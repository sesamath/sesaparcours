# Journal des changements de sesaparcours (versions 1.2.x)


## version 1.2.180

`* `[0edfe176a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0edfe176a) faut aussi gérer l'upgrade du param ayant eu le bug de typo (2025-03-04 10:07) <Daniel Caillibaud>  
`* `[d64f48c25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d64f48c25) fix lettre manquante dans un énoncé (2025-03-04 10:02) <Daniel Caillibaud>  

## version 1.2.179

`* `[e4a5832f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4a5832f2) fix upgradeParametres (HOMOTETHIE => HOMOTHETIE) (2025-02-28 21:12) <Daniel Caillibaud>  
`*   `[bec3cdfdf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bec3cdfdf) Merge branch 'reaccent' into 'main' (2025-02-27 16:44) <Tommy Barroy>  
`|\  `  
`| * `[8cc31ffff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cc31ffff) pour pas avoir ${} (2025-02-27 17:42) <Tommy Barroy>  
`|/  `  
`*   `[1c69f678c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c69f678c) Merge branch 'pbaccents' into 'main' (2025-02-26 12:59) <Tommy Barroy>  
`|\  `  
`| * `[1ff931893](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ff931893) [fix #372](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/372) (2025-02-26 13:58) <Tommy Barroy>  
`| * `[d8b2b445d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8b2b445d) [fix #372](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/372) (2025-02-26 13:54) <Tommy Barroy>  
`|/  `  
`* `[32337fc59](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32337fc59) faut approuver les builds dont on a besoin (2025-02-25 11:48) <bot-sesaparcours-preprod>  
`* `[d46adcb88](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d46adcb88) fix ignores eslint (2025-02-25 11:28) <Daniel Caillibaud>  
`* `[b1195aad8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1195aad8) eslint --fix (2025-02-25 11:27) <Daniel Caillibaud>  
`* `[678cd7237](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/678cd7237) eslint-config-standard => neostandard (2025-02-25 11:16) <Daniel Caillibaud>  
`* `[b7266cb54](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7266cb54) màj deps et passage à pnpm 10 (2025-02-25 10:48) <Daniel Caillibaud>  

## version 1.2.178

`*   `[7bb9f8104](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7bb9f8104) Merge branch 'chtitre' into 'main' (2025-02-21 15:45) <Tommy Barroy>  
`|\  `  
`| * `[2e682233e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e682233e) pas bon titre (2025-02-21 16:42) <Tommy Barroy>  
`|/  `  

## version 1.2.177

`*   `[11e82728b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/11e82728b) Merge branch 'Correction_Correction_Devel_(x-b)²' into 'main' (2025-02-16 13:12) <yves biton>  
`|\  `  
`| * `[ce441a1d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce441a1d8) Correction de la correction d'un exercice suite à rapport LaboMep [fix #370](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/370) (2025-02-16 14:10) <Yves Biton>  
`|/  `  

## version 1.2.176

`* `[aa79a0c51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa79a0c51) blindage editgraphe (fix busgnag 67010f9cb9c85341d8e25be5) (2025-02-04 12:03) <Daniel Caillibaud>  
`* `[5f208b1e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f208b1e2) modif du nom des chunks de src/vendors (pour que bugsnag les distingue) (2025-02-04 12:03) <Daniel Caillibaud>  
`*   `[73e14678e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/73e14678e) Merge remote-tracking branch 'origin/main' (2025-02-04 10:47) <Yves Biton>  
`|\  `  
`| *   `[acba0b7ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/acba0b7ce) Merge branch 'pbLectureGraphique' into 'main' (2025-02-02 17:06) <Remi Deniaud>  
`| |\  `  
`| | * `[28e5078af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28e5078af) modif du cas entier dans espace_repere7 (2025-02-02 18:04) <Rémi Deniaud>  
`| | * `[9f26d7577](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f26d7577) nettoyage code dans imageAntecedents (2025-01-26 17:44) <Rémi Deniaud>  
`| | * `[323d009be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/323d009be) une petite erreur a été corrigée, je ne vois pas l'incidence avec le souci remarqué, mais je ne suis jamais retombé dessus dans imageAntecedents (2025-01-26 17:37) <Rémi Deniaud>  
`| * `[3c14895ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c14895ab) Aj de l'id du nœud dans la réponse partielle (en cas d'abandon avant la fin d'un nœud) (2025-01-31 19:18) <Daniel Caillibaud>  
`| * `[1aa7c25eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1aa7c25eb) Ajout de l'envoi d'un résultat partiel si on quitte la page avant d'avoir fini (2025-01-31 18:29) <Daniel Caillibaud>  
`* | `[ec547c027](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec547c027) Correction de la correction d'un exercice suite à rapport LaboMep [fix #369](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/369) (2025-02-04 10:46) <Yves Biton>  
`|/  `  
`* `[4515fcc67](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4515fcc67) Amélioration de la correction d'un exercice (2025-01-31 16:51) <Yves Biton>  
`* `[b479d4067](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b479d4067) Amélioration de la correction d'un exercice (2025-01-31 16:49) <Yves Biton>  
`* `[28cf1d309](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28cf1d309) modif phrase d'aide (2025-01-30 19:48) <Daniel Caillibaud>  
`* `[611d964cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/611d964cb) hhhypoténuse => hippoténuze (pour éviter les signalemments de faute de frappe sur cet exemple de mauvaise réponse…) (2025-01-30 10:03) <Daniel Caillibaud>  
`* `[fd8867c38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd8867c38) aj d'un script build:local (2025-01-30 10:03) <Daniel Caillibaud>  

## version 1.2.175

`*   `[6dffa7e09](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6dffa7e09) Merge branch 'tofix' into 'main' (2025-01-29 19:46) <Tommy Barroy>  
`|\  `  
`| * `[9ddeb4110](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ddeb4110) change rotation regle (2025-01-29 20:45) <Tommy Barroy>  
`| * `[554b45043](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/554b45043) [fix #367](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/367) (2025-01-29 19:59) <Tommy Barroy>  
`|/  `  
`*   `[3f7733395](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f7733395) Merge branch 'Correction_Ex_Ineq_Log' into 'main' (2025-01-29 17:56) <yves biton>  
`|\  `  
`| * `[0b6bdd7ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b6bdd7ae) Correction de la correction d'un exercice sur les inéquations avec fonction ln. [fix #366](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/366) (2025-01-29 18:54) <Yves Biton>  
`* `[54e0798d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54e0798d5) correction orthographique (2025-01-28 13:37) <Daniel Caillibaud>  

## version 1.2.174

`* `[c20bbad28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c20bbad28) fix resultat.contenu (2025-01-27 19:42) <Daniel Caillibaud>  
`*   `[9f12c267f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f12c267f) Merge branch 'cacheco' into 'main' (2025-01-26 10:39) <Tommy Barroy>  
`|\  `  
`| * `[a38b2c4e3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a38b2c4e3) cache co dess et tab pas truc (2025-01-26 11:37) <Tommy Barroy>  
`|/  `  
`*   `[1683118ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1683118ed) Merge branch 'trypourvire' into 'main' (2025-01-25 13:10) <Tommy Barroy>  
`|\  `  
`| * `[3b2714c74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b2714c74) ajoute un try pour pas bug (2025-01-25 14:09) <Tommy Barroy>  
`|/  `  
`* `[fe9e9f37f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe9e9f37f) fix squelettemtg32_NbAntecedents_CaptK.test.js (2025-01-24 19:58) <Daniel Caillibaud>  
`* `[697629121](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/697629121) fix blokmtg01.test.js (2025-01-24 19:58) <Daniel Caillibaud>  
`* `[2751a7c62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2751a7c62) fix blokfigure.test (2025-01-24 19:58) <Daniel Caillibaud>  
`* `[8d2ad3e8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d2ad3e8c) fix jsdoc (2025-01-24 19:58) <Daniel Caillibaud>  

## version 1.2.173

`* `[223a49d95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/223a49d95) Ajout de _sendInitResult (pour appeler resultatCallback dès la fin du premier énoncé, pour incrémenter nbEssais dans Labomep au démarrage de l'exo et pas à la fin du premier nœud) (2025-01-24 13:52) <Daniel Caillibaud>  
`* `[6f2e07011](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f2e07011) Ajout de fcts utilitaires isArrayOfXxx (2025-01-24 13:50) <Daniel Caillibaud>  

## version 1.2.172

`*   `[0b750ea5b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b750ea5b) Merge branch 'mainpourtik' into 'main' (2025-01-22 18:25) <Tommy Barroy>  
`|\  `  
`| * `[9f9feffa1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f9feffa1) [fix #363](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/363) (2025-01-22 19:23) <Tommy Barroy>  

## version 1.2.171

`*   `[b00ec02c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b00ec02c5) Merge branch 'Amelioration_Section_Multi_Edit' into 'main' (2025-01-22 15:07) <yves biton>  
`|\  `  
`| * `[cf8d086f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf8d086f2) A laisse un padding-top de 10 pixels pour le div contenant la solution pour éviter des chevauchements. (2025-01-22 15:57) <Yves Biton>  
`|/  `  

## version 1.2.170

`*   `[e9bb53f91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9bb53f91) Merge branch 'tubig78' into 'main' (2025-01-15 13:06) <Tommy Barroy>  
`|\  `  
`| * `[c1b7371f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c1b7371f1) eslint eslint (2025-01-15 14:03) <Tommy Barroy>  
`| * `[0305a57f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0305a57f1) cherhche plus egalite exacte (2025-01-15 14:03) <Tommy Barroy>  
`| * `[2f4dcf8f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f4dcf8f0) this était pas reconnu (2025-01-15 11:45) <Tommy Barroy>  
`| * `[b647c6596](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b647c6596) this était pas reconnu (2025-01-15 11:44) <Tommy Barroy>  
`* `[7ff552d13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ff552d13) cosmonaute => spationaute et au-dessus => sur (entrainait une confusion entre même case et case en y+1) (2025-01-15 11:04) <Daniel Caillibaud>  

## version 1.2.169

`* `[7c459d6f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c459d6f0) fix focus au démarrage et enchaînement à la correction (2025-01-10 15:07) <Daniel Caillibaud>  
`* `[4c0431a57](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c0431a57) amélioration formateNombre avec ajout de tests unitaires (2025-01-09 18:00) <Daniel Caillibaud>  
`* `[b7b76716e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7b76716e) refonte verifNombreBienEcrit avec ajout de tests unitaires (2025-01-09 17:42) <Daniel Caillibaud>  
`* `[33b4d6137](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33b4d6137) simplification (notamment le case correction) (2025-01-09 13:57) <Daniel Caillibaud>  
`* `[c87530af5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c87530af5) simplification (on remplace les fins insécables si y'en a au départ) (2025-01-09 13:56) <Daniel Caillibaud>  
`* `[6a8f01df6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a8f01df6) code inutile viré (pas la peine de passer en revue tous les cas d'erreur pour reconstruire un message déjà fourni) (2025-01-09 13:55) <Daniel Caillibaud>  
`* `[f08f61752](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f08f61752) nombreBienEcritEspace => verifNombreBienEcrit et nombreBienEcrit => formateNombre (2025-01-09 13:20) <Daniel Caillibaud>  
`* `[713d214e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/713d214e9) externalisation de nombreBienEcritEspace et nombreBienEcrit dans un module séparé (2025-01-09 13:13) <Daniel Caillibaud>  

## version 1.2.168

`* `[5686cd164](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5686cd164) faut laisser le plugin am gérer la taille et le resize des swf (2025-01-08 11:00) <Daniel Caillibaud>  
`* `[e6ba1cdd8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e6ba1cdd8) fix incrément boucle (avec autobranchement et max=2 on ne retournait jamais dans le nœud) (2025-01-08 10:51) <Daniel Caillibaud>  
`* `[f5a47ad2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5a47ad2b) faut laisser le plugin em gérer la taille et le resize des swf (2025-01-08 10:03) <Daniel Caillibaud>  
`* `[d8629c5fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8629c5fb) fix n° question après la dernière (faut plus incrémenter) (2025-01-08 09:24) <Daniel Caillibaud>  

## version 1.2.167

`* `[04fcd1e45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04fcd1e45) màj deps (2025-01-07 11:55) <Daniel Caillibaud>  
`*   `[6720a66d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6720a66d9) Merge branch 'Compatibilite_Version_8.4.0_MathGraph32' into 'main' (2025-01-06 10:45) <yves biton>  
`|\  `  
`| * `[cbd5031e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cbd5031e2) Modification de ces deux sections pour qu'elles coninuent à fonctionner lors du déploiement de la version 8.4 de MathGraph32 dans laquelle le svg de la figure est encapsulé dans une autre svg (2025-01-06 11:41) <Yves Biton>  
`|/  `  
`* `[89ea6c241](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89ea6c241) split CHANGELOG (2025-01-03 17:51) <Daniel Caillibaud>  
`* `[405cf63f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/405cf63f6) baseId bibliloca et communloca (à la place de biblilocal3001 et communlocal3002) (2025-01-03 16:37) <Daniel Caillibaud>  

## version 1.2.166

`* `[6cdfeec65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cdfeec65) [fix #357](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/357) max sur autobranchement (marchait pas depuis très longtemps) (2025-01-03 12:41) <Daniel Caillibaud>  
`* `[ed9b4717b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed9b4717b) imports statiques sur une seule ligne (2025-01-03 12:12) <Daniel Caillibaud>  
`* `[fd2c3b13d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd2c3b13d) Le timer avait été viré dans bac42d09 (2025-01-03 11:25) <Daniel Caillibaud>  
`* `[3370003bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3370003bc) màj deps (2025-01-03 11:21) <Daniel Caillibaud>  
`* `[37e3e2393](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37e3e2393) imports sur une seule ligne (2025-01-03 11:17) <Daniel Caillibaud>  
`* `[ff475c4ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff475c4ee) aj eslint-plugin-import-newlines (2025-01-03 11:17) <Daniel Caillibaud>  
`* `[02253fb10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02253fb10) baseId peut contenir des chiffres (pour les biblis locales) (2025-01-03 10:59) <Daniel Caillibaud>  
`* `[82e1fd63b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/82e1fd63b) Ajout des biblis locales (2025-01-03 10:58) <Daniel Caillibaud>  
`* `[6d20ee7c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d20ee7c1) espaces virés avec les doubles ^^ (2025-01-03 10:58) <Daniel Caillibaud>  
`* `[46d5d4d0a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46d5d4d0a) simplifications mineures (2025-01-03 10:57) <Daniel Caillibaud>  

## version 1.2.165

`*   `[54386b6f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54386b6f6) Merge branch 'jesapse' into 'main' (2024-12-29 23:06) <Tommy Barroy>  
`|\  `  
`| * `[2777ec9f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2777ec9f7) set api a l'envers (2024-12-30 00:01) <Tommy Barroy>  
`|/  `  

## version 1.2.164

`*   `[b00dadcb7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b00dadcb7) Merge branch 'cobugsn' into 'main' (2024-12-29 18:17) <Tommy Barroy>  
`|\  `  
`| * `[b531e30cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b531e30cd) eslint eslint (2024-12-29 19:09) <Tommy Barroy>  
`| * `[07d4ec828](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07d4ec828) un bug quand pas de point (2024-12-29 19:03) <Tommy Barroy>  
`|/  `  

## version 1.2.163

`*   `[1cce5d95e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1cce5d95e) Merge branch 'amelioco' into 'main' (2024-12-26 07:32) <Tommy Barroy>  
`|\  `  
`| * `[77f1a80e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77f1a80e8) bug sur justeune longueur et place equerre quand vient d'angle a 90 (2024-12-26 08:30) <Tommy Barroy>  

## version 1.2.162

`*   `[86ae41073](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86ae41073) Merge branch 'boutonexpli' into 'main' (2024-12-25 22:46) <Tommy Barroy>  
`|\  `  
`| * `[576b745d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/576b745d9) précise utilisation des boutons (2024-12-25 23:44) <Tommy Barroy>  
`|/  `  
`* `[d27e9cd68](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d27e9cd68) Ajout de la génération de csv (2024-12-24 14:25) <Daniel Caillibaud>  
`* `[4ddc1eed4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ddc1eed4) Amélioration checkNiveaux pour traiter les ressources qui ne sont que dans l'arbre de l'éditeur de graphe (2024-12-24 13:24) <Daniel Caillibaud>  
`* `[52cc3e857](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52cc3e857) Amélioration checkNiveaux pour traiter les ressources qui ne sont que dans l'arbre de l'éditeur de graphe (2024-12-24 13:12) <Daniel Caillibaud>  

## version 1.2.161

`*   `[01fc4a51c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01fc4a51c) Merge branch 'pass6e' into 'main' (2024-12-24 09:09) <Tommy Barroy>  
`|\  `  
`| * `[eda9dda15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eda9dda15) aire triangle quelqconque en 6e (2024-12-24 10:07) <Tommy Barroy>  
`|/  `  

## version 1.2.160

`*   `[49cb370b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49cb370b7) Merge branch 'fondblanc' into 'main' (2024-12-23 10:00) <Tommy Barroy>  
`|\  `  
`| * `[a0c7d8fba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0c7d8fba) met un fond blanc cest plus joli (2024-12-23 10:59) <Tommy Barroy>  
`|/  `  

## version 1.2.159

`*   `[dabcec1ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dabcec1ad) Merge branch 'divegalite' into 'main' (2024-12-22 21:07) <Tommy Barroy>  
`|\  `  
`| * `[b36fb60f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b36fb60f8) Divegalite (2024-12-22 21:07) <Tommy Barroy>  
`|/  `  

## version 1.2.158

`*   `[f044d87a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f044d87a3) Merge branch 'Gain_Place_Section_Yves' into 'main' (2024-12-20 12:10) <yves biton>  
`|\  `  
`| *   `[9d54e1dab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d54e1dab) Merge branch 'main' into 'Gain_Place_Section_Yves' (2024-12-20 12:10) <yves biton>  
`| |\  `  
`| |/  `  
`|/|   `  
`* | `[70c12b73f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70c12b73f) Fix recensement des orphelines (2024-12-20 12:05) <Daniel Caillibaud>  
`| * `[51dcfc59c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51dcfc59c) Pour quasi toutes les sections de Yves on appelle structurePage avec comme paramètre 'presentation1bis' pour gagner de la place en hauteur à gauche. (2024-12-20 12:44) <Yves Biton>  
`| * `[d5b9a6a53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5b9a6a53) Fix recensement des orphelines (2024-12-20 10:50) <Daniel Caillibaud>  
`|/  `  
`* `[c74208a11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c74208a11) Aj de la liste des ressources sans niveau (2024-12-20 08:26) <Daniel Caillibaud>  
`* `[373403310](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/373403310) plus besoin de légende (on a les niveaux en clair) (2024-12-20 07:55) <Daniel Caillibaud>  
`* `[a7edf6d88](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7edf6d88) label des niveaux plutôt que leur code (dans les warnings) (2024-12-20 07:54) <Daniel Caillibaud>  
`* `[b346143bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b346143bd) aj légende (2024-12-20 07:44) <Daniel Caillibaud>  
`* `[f4bca599f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f4bca599f) amélioration cosmétique (2024-12-20 07:30) <Daniel Caillibaud>  
`* `[6b7857599](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b7857599) modif liens des ressources (vers décrire) (2024-12-20 07:20) <Daniel Caillibaud>  
`* `[921c7c749](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/921c7c749) aj rmq (2024-12-19 20:44) <Daniel Caillibaud>  
`* `[5ae080952](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ae080952) amélioration affichage date (2024-12-19 20:32) <Daniel Caillibaud>  
`* `[86fc449d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86fc449d2) amélioration pliage (2024-12-19 20:23) <Daniel Caillibaud>  
`* `[cfdee5f98](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cfdee5f98) amélioration warning dossier vide (2024-12-19 20:23) <Daniel Caillibaud>  
`* `[42f356036](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42f356036) rectif cron (2024-12-19 20:04) <Daniel Caillibaud>  
`* `[ea1d6c5b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea1d6c5b1) Aj d'un script cron pour cumuler refreshUsage et checkNiveaux + ajout de la page pour lister les anomalies de niveaux (2024-12-19 19:46) <Daniel Caillibaud>  

## version 1.2.157

`* `[f497ef012](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f497ef012) L'option preserveDom est devenue inutile (seul exo_mep l'utilisait) (2024-12-19 11:52) <Daniel Caillibaud>  
`* `[c335a78e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c335a78e7) refonte des sections exo_mep (en flash), fusionnées pour l'occasion (2024-12-19 11:52) <Daniel Caillibaud>  
`* `[bf3984c10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf3984c10) on simplifie encore (le tableau parametres_non_affiches devient un objet nommé parametresMasques) (2024-12-19 11:42) <Daniel Caillibaud>  
`* `[1c4ed10db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c4ed10db) simplification de code (2024-12-19 11:42) <Daniel Caillibaud>  
`* `[d9a788616](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9a788616) Ajout de commentaires (on ne peut pas modifier un id mep ou ato et c'est voulu…) (2024-12-19 11:42) <Daniel Caillibaud>  

## version 1.2.156

`*   `[bb6ae3c7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb6ae3c7e) Merge branch 'blindeprogdedep' into 'main' (2024-12-18 19:41) <Tommy Barroy>  
`|\  `  
`| * `[70889f479](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70889f479) empeche d'intercaler des blocs dans le prog de départ (2024-12-18 20:38) <Tommy Barroy>  
`* | `[ff8c96d0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff8c96d0e) Merge branch 'blindeprogdedep' into 'main' (2024-12-18 19:37) <Tommy Barroy>  
`|\| `  
`| * `[e4a32863b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4a32863b) empeche d'intercaler des blocs dans le prog de départ (2024-12-18 20:34) <Tommy Barroy>  
`|/  `  
`*   `[76e65b2d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76e65b2d8) Merge branch 'efface_sur_sectionsuivante' into 'main' (2024-12-16 11:38) <Tommy Barroy>  
`|\  `  
`| * `[efb8fc9ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/efb8fc9ed) efface sur section suivante , pour empecher de lancer la correction (2024-12-16 12:36) <Tommy Barroy>  

## version 1.2.155

`*   `[59ccd5101](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/59ccd5101) Merge branch 'ameliocond' into 'main' (2024-12-15 20:23) <Tommy Barroy>  
`|\  `  
`| * `[588850a44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/588850a44) Ameliocond (2024-12-15 20:23) <Tommy Barroy>  
`|/  `  

## version 1.2.154

`* `[64836e4ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64836e4ad) Merge branch 'sauvConstMtg' into 'main' (2024-12-15 19:20) <Tommy Barroy>  
`* `[71a1929f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/71a1929f3) Sauv const mtg (2024-12-15 19:20) <Tommy Barroy>  

## version 1.2.153

`*   `[bfa2ea235](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bfa2ea235) Merge branch 'corrogMUlpourfix' into 'main' (2024-12-15 18:38) <Tommy Barroy>  
`|\  `  
`| * `[30c6880b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30c6880b4) [fix #354](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/354) (2024-12-15 19:37) <Tommy Barroy>  
`|/  `  

## version 1.2.152

`*   `[fa00a0a8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa00a0a8b) Merge branch 'virecache' into 'main' (2024-12-14 14:04) <Tommy Barroy>  
`|\  `  
`| * `[61e4def20](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61e4def20) met qu'une fois le cahce pour pour vire vien apres (2024-12-14 15:03) <Tommy Barroy>  
`|/  `  
`*   `[0acfce768](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0acfce768) Merge branch 'fais_bien_case_tableau' into 'main' (2024-12-13 09:47) <Tommy Barroy>  
`|\  `  
`| * `[d0223897c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0223897c) fais bien vire bord pour tableau (2024-12-13 10:45) <Tommy Barroy>  
`|/  `  

## version 1.2.151

`*   `[bd40eceb5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd40eceb5) Merge branch 'refaisebc' into 'main' (2024-12-12 19:36) <Tommy Barroy>  
`|\  `  
`| * `[bdf6eb29d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bdf6eb29d) eslint eslint (2024-12-12 20:33) <Tommy Barroy>  
`* | `[22ba6a096](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22ba6a096) Merge branch 'refaisebc' into 'main' (2024-12-12 19:31) <Tommy Barroy>  
`|\| `  
`| * `[7bb3df531](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7bb3df531) vieille def pas prise en compte (2024-12-12 20:28) <Tommy Barroy>  
`|/  `  
`*   `[e47b31a26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e47b31a26) Merge branch 'notifpourcapte' into 'main' (2024-12-12 18:58) <Tommy Barroy>  
`|\  `  
`| * `[6d19c2b7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d19c2b7e) notif pour comprendre (2024-12-12 19:56) <Tommy Barroy>  
`| * `[afdaaf611](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/afdaaf611) notif pour comprendre (2024-12-12 19:55) <Tommy Barroy>  
`|/  `  
`*   `[644a8096d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/644a8096d) Merge branch 'maincacheboutraz' into 'main' (2024-12-12 18:43) <Tommy Barroy>  
`|\  `  
`| * `[cb8c10b3c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb8c10b3c) cache bouton reinit prog quand ca tourne (2024-12-12 19:42) <Tommy Barroy>  
`|/  `  

## version 1.2.150

`* `[0e4e764ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e4e764ae) Fix taille d'affichage des aides mep (2024-12-12 12:41) <Daniel Caillibaud>  
`* `[621af7c0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/621af7c0c) modif liste des biblis (pour les versions locales) (2024-12-12 12:40) <Daniel Caillibaud>  
`* `[5e20ced22](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e20ced22) aj d'un script checkNiveaux, pour vérifier les arbres de niveau (que les ressources qu'ils contiennent sont bien du niveau en question) et que toutes les ressources de sesabibli sont dans un de ces arbres (2024-12-11 20:33) <Daniel Caillibaud>  
`* `[d81759be3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d81759be3) Merge branch 'pbinit' into 'main' (2024-12-11 09:18) <Tommy Barroy>  
`* `[b06c53d60](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b06c53d60) explicite param conclusiondonnee (2024-12-11 10:16) <Tommy Barroy>  

## version 1.2.149

`*   `[73f3943c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/73f3943c4) Merge branch 'pbinit' into 'main' (2024-12-11 09:09) <Tommy Barroy>  
`|\  `  
`| * `[b5e200593](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5e200593) debloquage de nbchances (2024-12-11 10:08) <Tommy Barroy>  
`|/  `  
`*   `[4371562b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4371562b1) Merge branch 'simplif' into 'main' (2024-12-10 15:25) <Tommy Barroy>  
`|\  `  
`| * `[58c3b1910](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58c3b1910) simplif et corrige se termine par [fix #351](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/351) (2024-12-10 16:24) <Tommy Barroy>  

## version 1.2.148

`* `[4e9d82ffc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e9d82ffc) Aj d'option scoreIndulgent (2024-12-10 12:47) <Daniel Caillibaud>  
`* `[8ae1fad70](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8ae1fad70) Ajout d'une remarque pour signaler que 0.5 est déjà un bon score sur cet exo (2024-12-10 12:47) <Daniel Caillibaud>  
`* `[c4ff99142](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4ff99142) aj inputmode="none" sur l'input texte de la calculactrice ([fix #349](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/349)) (2024-12-10 12:47) <Daniel Caillibaud>  
`*   `[b649972c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b649972c5) Merge branch 'corrigeetoilefois' into 'main' (2024-12-10 11:25) <Tommy Barroy>  
`|\  `  
`| * `[f93f8a721](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f93f8a721) remet raz sur bouton suite et suivante (2024-12-10 12:23) <Tommy Barroy>  

## version 1.2.147

`*   `[b3028e5fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3028e5fe) Merge branch 'pd_deraz' into 'main' (2024-12-10 10:30) <Tommy Barroy>  
`|\  `  
`| * `[8db60d4e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8db60d4e8) remet raz sur bouton suite et suivante (2024-12-10 11:29) <Tommy Barroy>  
`|/  `  
`*   `[2e4ea48af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e4ea48af) Merge branch 'bugcopasunpoint' into 'main' (2024-12-06 12:52) <Tommy Barroy>  
`|\  `  
`| * `[a23ec91b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a23ec91b3) si l eleve cree un objet avec le nom d'un point au lieu d'un point (2024-12-06 13:48) <Tommy Barroy>  
`|/  `  
`*   `[41afb7a84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41afb7a84) Merge branch 'vire_quand_0' into 'main' (2024-12-06 12:18) <Tommy Barroy>  
`|\  `  
`| * `[04402211d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04402211d) eslint eslint (2024-12-06 13:16) <Tommy Barroy>  
`| * `[fff93b73d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fff93b73d) bug quand dx ou dy = 0 (2024-12-06 13:14) <Tommy Barroy>  
`|/  `  
`*   `[4dcd8822f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4dcd8822f) Merge branch 'ortho' into 'main' (2024-12-06 12:04) <Tommy Barroy>  
`|\  `  
`| * `[25cfaaa4c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25cfaaa4c) remplace 'de le' par 'du' dans énoncé (2024-12-06 13:02) <Tommy Barroy>  
`|/  `  

## version 1.2.146

`* `[b2ae3f743](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2ae3f743) Merge branch 'hlhjk' into 'main' (2024-12-05 21:21) <Tommy Barroy>  
`* `[d3f068ed5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3f068ed5) Hlhjk (2024-12-05 21:21) <Tommy Barroy>  

## version 1.2.145

`*   `[f9fa22bb1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f9fa22bb1) Merge branch 'faceopak' into 'main' (2024-12-05 20:37) <Tommy Barroy>  
`|\  `  
`| * `[d1983ad6d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1983ad6d) opacite a regler (2024-12-05 21:35) <Tommy Barroy>  
`|/  `  
`*   `[4e74a163d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e74a163d) Merge branch 'paslebonraz' into 'main' (2024-12-05 16:40) <Tommy Barroy>  
`|\  `  
`| * `[dfd18f587](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfd18f587) mauvaise variable (2024-12-05 17:38) <Tommy Barroy>  
`|/  `  
`* `[5a38608e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a38608e2) blindage des tests playwright (2024-12-03 10:36) <Daniel Caillibaud>  
`*   `[f34e041af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f34e041af) Merge branch 'direj' into 'main' (2024-12-02 20:56) <Tommy Barroy>  
`|\  `  
`| * `[6978a2c42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6978a2c42) dire petit scratch et variables dans editeur (2024-12-02 21:55) <Tommy Barroy>  
`|/  `  

## version 1.2.144

`*   `[9dfec1763](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9dfec1763) Merge branch 'petitsbugs' into 'main' (2024-11-30 14:42) <Tommy Barroy>  
`|\  `  
`| * `[f1306401e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1306401e) petit bugs sur plein ecran (2024-11-30 15:40) <Tommy Barroy>  
`|/  `  
`*   `[fe1ae04dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe1ae04dd) Merge branch 'hshfg' into 'main' (2024-11-30 13:49) <Tommy Barroy>  
`|\  `  
`| * `[0ed4c0f24](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ed4c0f24) remet stor.raz sur equerre01 (2024-11-30 14:43) <Tommy Barroy>  
`| * `[fb9b4489a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb9b4489a) pour test apres jefface (2024-11-30 10:48) <Tommy Barroy>  
`* | `[697c12269](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/697c12269) Merge branch 'hshfg' into 'main' (2024-11-30 07:34) <Tommy Barroy>  
`|\| `  
`| * `[41de2cd38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41de2cd38) pour test apres jefface (2024-11-30 08:33) <Tommy Barroy>  
`| * `[dc3197cb4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc3197cb4) pour test apres jefface (2024-11-30 08:33) <Tommy Barroy>  
`* | `[e1d7cc126](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1d7cc126) Merge branch 'hshfg' into 'main' (2024-11-30 07:32) <Tommy Barroy>  
`|\| `  
`| * `[d28138f2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d28138f2b) pour test apres jefface (2024-11-30 08:31) <Tommy Barroy>  
`* | `[134062de4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/134062de4) Merge branch 'hshfg' into 'main' (2024-11-30 07:29) <Tommy Barroy>  
`|\| `  
`| * `[59fbcb200](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/59fbcb200) pour test apres jefface (2024-11-30 08:27) <Tommy Barroy>  
`|/  `  
`* `[f06e0c565](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f06e0c565) Merge branch 'remetraz' into 'main' (2024-11-29 21:42) <Tommy Barroy>  
`* `[7aae056da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7aae056da) remet stor.raz sur sectionsuivante (2024-11-29 22:40) <Tommy Barroy>  

## version 1.2.143

`*   `[ed6b424dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed6b424dc) Merge branch 'notifnotif' into 'main' (2024-11-29 21:31) <Tommy Barroy>  
`|\  `  
`| * `[c4703c579](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4703c579) re notif je capte tjrs pas (2024-11-29 21:05) <Tommy Barroy>  
`|/  `  
`*   `[f49e760b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f49e760b7) Merge branch 'equ1opa' into 'main' (2024-11-29 19:16) <Tommy Barroy>  
`|\  `  
`| * `[31d3a313e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31d3a313e) force opa dans recon01 (2024-11-29 20:14) <Tommy Barroy>  
`| * `[6c73e41ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c73e41ad) force opa dans angle04 (2024-11-29 20:02) <Tommy Barroy>  
`| * `[9541a9d7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9541a9d7a) force opa dans resMake2 (2024-11-29 19:46) <Tommy Barroy>  
`| * `[6497e062e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6497e062e) force opa (2024-11-29 18:23) <Tommy Barroy>  
`|/  `  
`*   `[a0fa06c0a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0fa06c0a) Merge branch 'amelioRond' into 'main' (2024-11-28 19:59) <Tommy Barroy>  
`|\  `  
`| * `[3b6d9453f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b6d9453f) pour amelio le petit rond (2024-11-28 20:58) <Tommy Barroy>  
`|/  `  
`* `[bb3ff3308](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb3ff3308) Merge branch 'oubraz' into 'main' (2024-11-28 18:51) <Tommy Barroy>  
`* `[6b40c299d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b40c299d) manquait raz sur les boutons suites (2024-11-28 13:39) <Tommy Barroy>  

## version 1.2.142

`*   `[adb316245](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/adb316245) Merge branch 'Amélioration_Exerices_Vecteurs' into 'main' (2024-11-28 18:16) <yves biton>  
`|\  `  
`| * `[05df1fe8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05df1fe8e) Dans les fichiers annexes de Yves on ON met une couleur plusfoncée pour la quadrillage des ces deux exercices et on remplace dans l'exercice avec aide les points libres par des points à coordonnées entières. (2024-11-28 19:12) <Yves Biton>  
`|/  `  

## version 1.2.141

`*   `[c0d65bd75](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0d65bd75) Merge branch 'pourYv' into 'main' (2024-11-27 21:13) <Tommy Barroy>  
`|\  `  
`| * `[75ea48a7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75ea48a7e) pour setTag (2024-11-27 22:12) <Tommy Barroy>  
`| * `[2c17def99](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c17def99) pour setTag (2024-11-27 21:32) <Tommy Barroy>  
`|/  `  
`*   `[4d099e2b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d099e2b0) Merge branch 'bugopcaite' into 'main' (2024-11-27 19:59) <Tommy Barroy>  
`|\  `  
`| * `[f4e082cbb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f4e082cbb) pb sur opacite, faut pas faire update je sais pas pourquoi (2024-11-27 20:57) <Tommy Barroy>  
`|/  `  

## version 1.2.140

`*   `[da9375582](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da9375582) Merge branch 'cachefulll_quand_trop_cieux' into 'main' (2024-11-27 14:19) <Tommy Barroy>  
`|\  `  
`| * `[b7ebb6a47](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7ebb6a47) pas sur mais on essaye (2024-11-27 15:11) <Tommy Barroy>  
`|/  `  
`*   `[666668e79](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/666668e79) Merge branch 'init_de_workspace' into 'main' (2024-11-27 14:06) <Tommy Barroy>  
`|\  `  
`| * `[2190f4fda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2190f4fda) pas sur mais on essaye (2024-11-27 15:04) <Tommy Barroy>  
`|/  `  
`*   `[01e0cbba0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01e0cbba0) Merge branch 'ss1' into 'main' (2024-11-27 13:54) <Tommy Barroy>  
`|\  `  
`| * `[de74b8f20](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de74b8f20) me manque des vieux tags (2024-11-27 14:52) <Tommy Barroy>  
`|/  `  
`*   `[239a724c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/239a724c6) Merge branch 'angle04d' into 'main' (2024-11-27 13:45) <Tommy Barroy>  
`|\  `  
`| * `[b9277a762](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9277a762) correcteur se lance parfois trop tot (2024-11-27 14:42) <Tommy Barroy>  
`|/  `  
`*   `[f59a3fb16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f59a3fb16) Merge branch 'mainrenot' into 'main' (2024-11-26 15:56) <Tommy Barroy>  
`|\  `  
`| * `[82bd95d95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/82bd95d95) modif notify pour comprendre (2024-11-25 21:06) <Tommy Barroy>  
`*   `[1769261fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1769261fe) Merge branch 'jklui' into 'main' (2024-11-26 15:38) <Tommy Barroy>  
`|\  `  
`| * `[aa9e6f9ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa9e6f9ee) erreur dans la correction (2024-11-26 16:36) <Tommy Barroy>  
`|/  `  

## version 1.2.139

`* `[a6349bbaf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6349bbaf) Ajout de la détection de l'usage d'espace pour indenter, avec message d'erreur explicite (+ simplification du code) (2024-11-26 12:51) <Daniel Caillibaud>  
`* `[e54e62029](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e54e62029) qq changement d'import statique => dynamique (2024-11-26 12:35) <Daniel Caillibaud>  
`* `[07e2ef6d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07e2ef6d5) aj d'une options pour ne plus avoir de warning sur le module roboto qui fait de l'import scss (2024-11-26 12:23) <Daniel Caillibaud>  
`* `[31ffb01c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31ffb01c7) on passe "pnpx sass-migrator module --migrate-deps fichier.scss" sur tous nos fichiers scss (+aj d'une règle vite pour utilise la nouvelle api de sass afin qu'il ne râle plus) (2024-11-26 12:20) <Daniel Caillibaud>  
`* `[ef0a49d25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef0a49d25) fix orthographe (2024-11-26 10:21) <Daniel Caillibaud>  
`* `[dd2233a0f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd2233a0f) [fix #342](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/342) (pb de couleur à la correction) (2024-11-26 10:12) <Daniel Caillibaud>  

## version 1.2.138

`*   `[5b99fc9a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b99fc9a4) Merge branch 'nbchandansedit' into 'main' (2024-11-24 21:08) <Tommy Barroy>  
`|\  `  
`| * `[7a0e5b5fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a0e5b5fe) donne info en fin de test prog dans editeur moha et mtg (2024-11-24 22:07) <Tommy Barroy>  
`| * `[aad11de8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aad11de8b) nbchance mal init dans editeur moha mtg (2024-11-24 21:44) <Tommy Barroy>  
`|/  `  

## version 1.2.137

`*   `[77541cbf9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77541cbf9) Merge branch 'bloksisimple' into 'main' (2024-11-24 18:56) <Tommy Barroy>  
`|\  `  
`| * `[4db2be65f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4db2be65f) eslint esint (2024-11-24 19:55) <Tommy Barroy>  
`| * `[b35f7ed1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b35f7ed1a) blind si leleve supprime le prog de deb ( je ne sais pas comment il fait dailleurs ) (2024-11-24 19:54) <Tommy Barroy>  
`|/  `  

## version 1.2.136

`*   `[11cc7fee6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/11cc7fee6) Merge branch 'notifpourcapte' into 'main' (2024-11-24 09:53) <Tommy Barroy>  
`|\  `  
`| * `[211f4b1bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/211f4b1bd) enregistre pour notify pour compendre (2024-11-24 10:51) <Tommy Barroy>  
`|/  `  

## version 1.2.135

`*   `[0d77aed8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d77aed8d) Merge branch 'pleinecrres2' into 'main' (2024-11-23 18:35) <Tommy Barroy>  
`|\  `  
`| * `[748ffa236](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/748ffa236) Pleinecrres2 (2024-11-23 18:35) <Tommy Barroy>  
`|/  `  
`* `[3925ba33d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3925ba33d) màj mathgraph.d.ts (2024-11-21 18:02) <Daniel Caillibaud>  

## version 1.2.134

`*   `[eb0ae1d27](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb0ae1d27) Merge branch 'finprog' into 'main' (2024-11-21 16:03) <Tommy Barroy>  
`|\  `  
`| * `[89d0e5a2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89d0e5a2e) force fin du prog à la navigation (2024-11-21 16:56) <Tommy Barroy>  
`|/  `  
`* `[2d66b34e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d66b34e4) typedoc avec --logLevel Error par défaut (bcp trop de warnings) (2024-11-20 18:19) <Daniel Caillibaud>  

## version 1.2.133

`*   `[d4891e3eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4891e3eb) Merge branch 'mainmg' into 'main' (2024-11-20 15:53) <Tommy Barroy>  
`|\  `  
`| * `[6e35f9145](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e35f9145) tuouch et mouse declenchent le mm event (2024-11-20 16:51) <Tommy Barroy>  

## version 1.2.132

`*   `[b5dfaef42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5dfaef42) Merge branch 'mauvaisintides_cases' into 'main' (2024-11-20 14:47) <Tommy Barroy>  
`|\  `  
`| * `[75d7a87b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75d7a87b3) pb a init des cases et sur test entrée boucle jusqau et sur petitsctarch en mode cachetest ou modecompare (2024-11-20 15:46) <Tommy Barroy>  
`|/  `  
`*   `[c505b1ebd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c505b1ebd) Merge branch 'bloksim' into 'main' (2024-11-20 08:47) <Tommy Barroy>  
`|\  `  
`| * `[a2f0159d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2f0159d7) pb avec nb alea 0 (2024-11-20 09:46) <Tommy Barroy>  

## version 1.2.131

`*   `[6e5dc61d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e5dc61d8) Merge branch 'pour_ticket' into 'main' (2024-11-19 23:02) <Tommy Barroy>  
`|\  `  
`| * `[0d75eff28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d75eff28) correction a l'envers (2024-11-20 00:01) <Tommy Barroy>  
`|/  `  

## version 1.2.130

`*   `[779a367b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/779a367b2) Merge branch 'ticket' into 'main' (2024-11-19 16:26) <Tommy Barroy>  
`|\  `  
`| * `[122794437](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/122794437) prtits changments mais je sais pas torp (2024-11-19 17:25) <Tommy Barroy>  
`| * `[078111e2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/078111e2b) cetait peut etre linit de i avec var (2024-11-19 16:47) <Tommy Barroy>  
`| * `[af42d3df8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af42d3df8) j3pNotify pour comprendre (2024-11-19 16:43) <Tommy Barroy>  
`| * `[243ec3313](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/243ec3313) eslint eslint (2024-11-19 16:40) <Tommy Barroy>  
`| * `[2125dffae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2125dffae) mauvaise init de nombre alea (2024-11-19 16:40) <Tommy Barroy>  
`| * `[505604fda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/505604fda) eslint eslint (2024-11-19 16:11) <Tommy Barroy>  
`| * `[6313d8e2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6313d8e2e) [fix #340](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/340) (2024-11-19 16:09) <Tommy Barroy>  
`|/  `  

## version 1.2.129

`*   `[3300631ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3300631ab) Merge branch 'fullscretik' into 'main' (2024-11-18 20:06) <Tommy Barroy>  
`|\  `  
`| * `[39370e061](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39370e061) ajoute vullscrenn etikAtome (2024-11-18 21:05) <Tommy Barroy>  
`|/  `  

## version 1.2.128

`*   `[a602e05d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a602e05d9) Merge branch 'notif' into 'main' (2024-11-18 12:14) <Tommy Barroy>  
`|\  `  
`| * `[5c7da7bae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c7da7bae) eslint eslint (2024-11-18 13:12) <Tommy Barroy>  
`| * `[dd3f60eda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd3f60eda) j3pnotify pour comprendre (2024-11-18 13:12) <Tommy Barroy>  
`|/  `  
`*   `[7288c2f87](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7288c2f87) Merge branch 'bonbouton' into 'main' (2024-11-18 12:02) <Tommy Barroy>  
`|\  `  
`| * `[4e1ce8a58](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e1ce8a58) ca cachait pas le bon bouton (2024-11-18 13:00) <Tommy Barroy>  
`|/  `  
`*   `[e1db142ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1db142ad) Merge branch 'testFullScreen' into 'main' (2024-11-17 20:46) <Tommy Barroy>  
`|\  `  
`| * `[aace850ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aace850ea) ajoute un bouton plein ecran (2024-11-17 21:45) <Tommy Barroy>  
`* | `[f5a00a04d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5a00a04d) Merge branch 'testFullScreen' into 'main' (2024-11-17 20:44) <Tommy Barroy>  
`|\| `  
`| * `[3296f92d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3296f92d4) ajoute un bouton plein ecran (2024-11-17 21:43) <Tommy Barroy>  
`|/  `  

## version 1.2.127

`*   `[00aefdd03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00aefdd03) Merge branch 'correcbuf_sur_comparealea' into 'main' (2024-11-17 16:04) <Tommy Barroy>  
`|\  `  
`| * `[4557970e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4557970e8) pas la bonne sortie en correction (2024-11-17 17:03) <Tommy Barroy>  
`* | `[9698c228d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9698c228d) Merge branch 'correcbuf_sur_comparealea' into 'main' (2024-11-17 15:50) <Tommy Barroy>  
`|\| `  
`| * `[8716f7154](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8716f7154) yavait un bug dans l'ordre de tirage des val aléatoire (2024-11-17 16:48) <Tommy Barroy>  

## version 1.2.126

`*   `[bab2854fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bab2854fc) Merge branch 'jusqua' into 'main' (2024-11-16 20:07) <Tommy Barroy>  
`|\  `  
`| * `[4d3264d8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d3264d8b) Jusqua (2024-11-16 20:07) <Tommy Barroy>  
`|/  `  
`*   `[e8f291546](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8f291546) Merge branch 'lancefle' into 'main' (2024-11-15 17:24) <Tommy Barroy>  
`|\  `  
`| * `[fa016978a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa016978a) un bug doubli (2024-11-15 18:23) <Tommy Barroy>  
`|/  `  
`*   `[d1b8ce574](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1b8ce574) Merge branch 'bugsnagthales' into 'main' (2024-11-15 17:17) <Tommy Barroy>  
`|\  `  
`| * `[46c88c1df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46c88c1df) un bug doubli (2024-11-15 18:15) <Tommy Barroy>  
`|/  `  

## version 1.2.125

`*   `[458442d76](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/458442d76) Merge branch 'pourticket' into 'main' (2024-11-14 20:05) <Tommy Barroy>  
`|\  `  
`| * `[4e390cff0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e390cff0) [fix #338](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/338) (2024-11-14 21:03) <Tommy Barroy>  
`|/  `  
`*   `[c1e92cc45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c1e92cc45) Merge branch 'chg_tetemodale' into 'main' (2024-11-14 19:43) <Tommy Barroy>  
`|\  `  
`| * `[12f1a1738](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12f1a1738) chg tete de charlie scratch (2024-11-14 20:41) <Tommy Barroy>  
`|/  `  
`*   `[30ba82aa9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30ba82aa9) Merge branch 'amelco' into 'main' (2024-11-14 15:17) <Tommy Barroy>  
`|\  `  
`| * `[50a9a219d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50a9a219d) met modale sur fin  mtglibre (2024-11-14 16:16) <Tommy Barroy>  
`|/  `  
`*   `[d927a3cd5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d927a3cd5) Merge branch 'scratchprog' into 'main' (2024-11-14 12:39) <Tommy Barroy>  
`|\  `  
`| *   `[279e9efcd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/279e9efcd) Merge branch 'main' into 'scratchprog' (2024-11-14 12:38) <Tommy Barroy>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[c615091e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c615091e9) Merge remote-tracking branch 'origin/main' (2024-11-12 12:59) <Yves Biton>  
`|\ \  `  
`| * | `[bbc484fb0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bbc484fb0) ajout de commentaires (2024-11-12 12:21) <Daniel Caillibaud>  
`| * | `[be0702747](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be0702747) contournement d'un bug mathlive (qui vire les espaces autours des tags qui sont au même niveau qu'une expression entre $ => j3pAddContent ajoute des span autour de ces expressions) (2024-11-12 11:54) <Daniel Caillibaud>  
`| * | `[9ebacbec7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ebacbec7) màj deps (2024-11-12 06:50) <Daniel Caillibaud>  
`| * | `[93da24550](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93da24550) passage à eslint9 (2024-11-12 06:50) <Daniel Caillibaud>  
`|  /  `  
`* / `[ae923a214](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae923a214) Dans les fichiers annexes de Yves on remplace tous les <i>lettre</> par des $lettre$ (2024-11-12 12:48) <Yves Biton>  
` /  `  
`* `[f6d902e03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6d902e03) pour fiish (2024-11-14 13:34) <Tommy Barroy>  
`* `[e8f5fd7f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8f5fd7f7) pour sauv (2024-11-13 23:15) <Tommy Barroy>  
`* `[c5d0545a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c5d0545a2) pour sauv (2024-11-13 21:15) <Tommy Barroy>  
`* `[1d58cb324](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d58cb324) pour sauv (2024-11-13 17:53) <Tommy Barroy>  
`* `[dfb028420](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfb028420) pour sauv (2024-11-13 16:06) <Tommy Barroy>  
`* `[07c10a37b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07c10a37b) pour sauv (2024-11-13 14:55) <Tommy Barroy>  
`* `[c000873c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c000873c4) pour sauv (2024-11-13 12:14) <Tommy Barroy>  
`* `[b2d9a6062](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2d9a6062) pour sauv (2024-11-13 10:38) <Tommy Barroy>  
`* `[7249d4d3b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7249d4d3b) pour sauv (2024-11-13 07:29) <Tommy Barroy>  
`* `[9861e883d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9861e883d) pour sauv (2024-11-12 16:56) <Tommy Barroy>  
`* `[aa34e81b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa34e81b3) pour sauv (2024-11-12 00:31) <Tommy Barroy>  
`* `[048384783](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/048384783) pour sauv (2024-11-11 21:36) <Tommy Barroy>  
`* `[c3e94b92e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c3e94b92e) pour sauv (2024-11-11 21:02) <Tommy Barroy>  
`* `[f6c7943d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6c7943d9) peut etre un videleszones qui ne se faisait pas ( pas réussi à reproduire ) (2024-11-10 19:09) <Tommy Barroy>  
`* `[793fe3192](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/793fe3192) peut etre un videleszones qui ne se faisait pas ( pas réussi à reproduire ) (2024-11-09 15:09) <Tommy Barroy>  
`* `[c669a8f55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c669a8f55) erreur dans crorrcrion tri 4 (2024-11-09 11:03) <Tommy Barroy>  
`* `[65cf09bcb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65cf09bcb) pour sauv (2024-11-08 16:04) <Tommy Barroy>  
`* `[6232735cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6232735cf) pour sauv (2024-11-08 14:15) <Tommy Barroy>  
`* `[4545e2cdf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4545e2cdf) pour sauv (2024-11-04 21:09) <Tommy Barroy>  
`* `[975ad743f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/975ad743f) pour sauv (2024-11-04 20:58) <Tommy Barroy>  
`* `[50bf2fd79](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50bf2fd79) ajout boucle jusqua (2024-11-03 22:20) <Tommy Barroy>  
`* `[badb3a983](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/badb3a983) en cours (2024-11-03 14:40) <Tommy Barroy>  
`* `[2aa3a829c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2aa3a829c) en cours (2024-11-02 21:45) <Tommy Barroy>  
`* `[46771a360](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46771a360) commence (2024-10-31 14:31) <Tommy Barroy>  

## version 1.2.124

`*   `[48210c3ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48210c3ef) Merge branch 'Amelioration_Param_Translatable_Multi_Edit' into 'main' (2024-11-09 16:35) <yves biton>  
`|\  `  
`| * `[8a6636385](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a6636385) Pour cette section on rajoute un paramètre figureGlissable pour autoriser ou non de faire glisser la figure avec la souris. On diminue la hauteur du bas pour avoir plus de place pour la figure quand elle est présente. (2024-11-09 17:34) <Yves Biton>  
`|/  `  

## version 1.2.123

`*   `[50f847cc1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50f847cc1) Merge branch 'Squelettes_Const_Non_translatables' into 'main' (2024-11-09 12:29) <yves biton>  
`|\  `  
`| * `[2c1cfeeed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c1cfeeed) Pour ces squelettes de constructions on donne à MtgOptions la propriété translatable = false pour qu'on en puisse pas faire glisser la figure. (2024-11-09 13:28) <Yves Biton>  

## version 1.2.122

`*   `[416e23d32](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/416e23d32) Merge branch 'un_videles_znes' into 'main' (2024-11-09 10:50) <Tommy Barroy>  
`|\  `  
`| * `[d0a793397](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0a793397) peut etre un videleszones qui ne se faisait pas ( pas réussi à reproduire ) (2024-11-09 11:49) <Tommy Barroy>  
`|/  `  
`*   `[39214eef3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39214eef3) Merge branch 'bug_sur_touch' into 'main' (2024-11-09 10:24) <Tommy Barroy>  
`|\  `  
`| * `[2ad27bca8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ad27bca8) des fois touchmov sans point tag, et rapporteur restait collé en touchmove (2024-11-09 11:21) <Tommy Barroy>  
`|/  `  

## version 1.2.121

`*   `[a0dbe9bd2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0dbe9bd2) Merge branch 'erreur_co_tri_4' into 'main' (2024-11-08 15:39) <Tommy Barroy>  
`|\  `  
`| * `[9490df6b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9490df6b8) erreur dans crorrcrion tri 4 (2024-11-08 16:38) <Tommy Barroy>  
`|/  `  
`* `[1a49aa26f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a49aa26f) fix plus concis du cas "1 minute" (2024-11-06 10:43) <Daniel Caillibaud>  
`*   `[06866e4b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06866e4b0) Merge branch 'timer_bug_sur_1_minute' into 'main' (2024-11-06 09:41) <Daniel Caillibaud>  
`|\  `  
`| * `[7e87ccc1b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e87ccc1b) affichait pas 1 minute (2024-11-02 00:33) <Tommy Barroy>  
`*   `[5fd60241c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5fd60241c) Merge branch 'et_un_autre' into 'main' (2024-11-05 18:14) <Tommy Barroy>  
`|\  `  
`| * `[4790d719f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4790d719f) bug dans co mtg (2024-11-05 19:12) <Tommy Barroy>  
`|/  `  
`*   `[c8ea90dcd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c8ea90dcd) Merge branch 'rajoute_pas_test' into 'main' (2024-11-05 16:59) <Tommy Barroy>  
`|\  `  
`| * `[32bc0d152](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32bc0d152) pb sur mod def (2024-11-05 17:56) <Tommy Barroy>  
`|/  `  
`*   `[a22b842af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a22b842af) Merge branch 'Amelioration_Outils_Certains_Exos_Construction' into 'main' (2024-11-05 08:27) <yves biton>  
`|\  `  
`| * `[75c0b7a06](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75c0b7a06) Amélioration des outils disponibles de certains exos de constructions suite à des modifs version 8.2.1 de MathGraph32 (2024-11-05 09:26) <Yves Biton>  
`*   `[9fe27757e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9fe27757e) Merge branch 'ajout_boucleU' into 'main' (2024-11-03 14:05) <Tommy Barroy>  
`|\  `  
`| * `[ffa0b17ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffa0b17ee) ajout boucle jusqua (2024-11-03 14:59) <Tommy Barroy>  
`|/  `  

## version 1.2.120

`*   `[f09e7af8a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f09e7af8a) Merge branch 'oubli_dun_ds' into 'main' (2024-11-02 15:22) <Tommy Barroy>  
`|\  `  
`| * `[955194d5e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/955194d5e) oubli dun ds.textes dans nettoyage (2024-11-02 16:20) <Tommy Barroy>  
`|/  `  
`*   `[2c2d2c443](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c2d2c443) Merge branch 'adaptetaille_cases' into 'main' (2024-11-02 14:22) <Tommy Barroy>  
`|\  `  
`| * `[3b6e149c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b6e149c1) fin adapte taille (2024-11-02 15:19) <Tommy Barroy>  
`| * `[7605c4f28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7605c4f28) un point a pas vire (2024-11-01 02:09) <Tommy Barroy>  
`* |   `[5574a575b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5574a575b) Merge branch 'newNettoyage' into 'main' (2024-11-02 07:55) <Remi Deniaud>  
`|\ \  `  
`| * | `[5f196e78d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f196e78d) nettoyage de 16 nouvelles sections (2024-11-02 08:53) <Rémi Deniaud>  
`|/ /  `  
`* |   `[62180b13e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/62180b13e) Merge branch 'nettoyageGetDonnees' into 'main' (2024-11-01 17:36) <Remi Deniaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[0e0af41e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e0af41e6) nettoyage ProduitScalaire (2024-11-01 18:24) <Rémi Deniaud>  
`| * `[813d90673](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/813d90673) un oubli sur tabvar_trinome (2024-10-31 18:54) <Rémi Deniaud>  
`| * `[dc1beaa0b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc1beaa0b) nettoyage dossier seconddegre (2024-10-31 18:53) <Rémi Deniaud>  
`| * `[94a698768](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94a698768) nettoyage dossier statistiques y compris jquery (2024-10-31 14:13) <Rémi Deniaud>  
`| * `[7e52238cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e52238cd) nettoyage dossier suites + suppression de quasiment tous les appels jquery dans ce dossier et les suivants (2024-10-31 11:41) <Rémi Deniaud>  
`| * `[23050321a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23050321a) nettoyage tableauSignes (2024-10-30 16:21) <Rémi Deniaud>  
`| * `[81af6d1f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81af6d1f5) nettoyage tableauvariations et dossiers suivants (2024-10-30 15:08) <Rémi Deniaud>  
`| * `[991479674](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/991479674) nettoyage dossier taux_evolution (2024-10-30 14:32) <Rémi Deniaud>  
`| * `[feed85e61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/feed85e61) nettoyage evolutionTableur (2024-10-30 13:58) <Rémi Deniaud>  
`| * `[98c10be2a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98c10be2a) nettoyage sections sur les vecteurs (2024-10-30 12:32) <Rémi Deniaud>  
`| * `[42c09fde3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42c09fde3) nettoyage propDeProportion (2024-10-29 22:26) <Rémi Deniaud>  
`| * `[c4192b9e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4192b9e9) nettoyage homographique_expression (2024-10-29 22:22) <Rémi Deniaud>  
`| * `[f19f3a29c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f19f3a29c) nettoyage signeInegalites (2024-10-29 22:17) <Rémi Deniaud>  
`| * `[72fb4a823](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72fb4a823) nettoyage critereDivisibilite (2024-10-29 22:01) <Rémi Deniaud>  
`| * `[9f804a7b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f804a7b6) nettoyage distance_valabs (2024-10-29 21:49) <Rémi Deniaud>  

## version 1.2.119

`*   `[a2f58e7f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2f58e7f0) Merge branch 'reduis_taille' into 'main' (2024-10-31 13:37) <Tommy Barroy>  
`|\  `  
`| * `[06ce7aed8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06ce7aed8) un point a pas vire (2024-10-31 14:34) <Tommy Barroy>  
`| * `[db8a1bf6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db8a1bf6a) reduis la taille (2024-10-31 14:32) <Tommy Barroy>  
`|/  `  

## version 1.2.118

`*   `[bd65d6fbd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd65d6fbd) Merge branch 'amelio_place_etik' into 'main' (2024-10-31 12:25) <Tommy Barroy>  
`|\  `  
`| * `[b47c639c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b47c639c7) amelio place etik (2024-10-31 13:24) <Tommy Barroy>  
`|/  `  

## version 1.2.117

`*   `[d760cf8e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d760cf8e1) Merge branch 'affine_atome' into 'main' (2024-10-31 08:05) <Tommy Barroy>  
`|\  `  
`| * `[efaac0691](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/efaac0691) decale un peu un point (2024-10-31 08:55) <Tommy Barroy>  
`|/  `  
`*   `[f0f099873](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0f099873) Merge branch 'modifavoir2' into 'main' (2024-10-30 18:44) <Tommy Barroy>  
`|\  `  
`| * `[974cc80a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/974cc80a1) Le message de bounce n'est affiché qu'une seule fois (2024-10-30 19:22) <Daniel Caillibaud>  
`| * `[a0b805c89](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0b805c89) aj jsdoc sur les fcts dont la signature a été modifiée (+annulation de l'interdiction d'afficher 2 messages d'erreur) (2024-10-30 19:09) <Daniel Caillibaud>  
`| * `[5737afac2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5737afac2) ajoute un timer pour ce message (2024-10-30 18:42) <Tommy Barroy>  
`| * `[37b5a721f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37b5a721f) modif j3pShowwError pour pas afficher 1000 fois la même ca arrive quand un éléve clique 1001 fois sur OK sans lire (2024-10-30 18:42) <Tommy Barroy>  
`|/  `  

## version 1.2.116

`*   `[4e946efd7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e946efd7) Merge branch 'ajsauv' into 'main' (2024-10-30 16:23) <Tommy Barroy>  
`|\  `  
`| * `[93fe92e06](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93fe92e06) effacer sauve par erreur (2024-10-30 17:22) <Tommy Barroy>  
`|/  `  
`* `[dc05d772c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc05d772c) fix getPe (2024-10-30 15:49) <Daniel Caillibaud>  
`* `[80c69127c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80c69127c) simplification du case correction (appels directs de sectionCourante() virés) (2024-10-30 15:43) <Daniel Caillibaud>  
`* `[f43632162](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f43632162) la modale d'avertissement du temps limité rend le focus là où il était (2024-10-30 15:43) <Daniel Caillibaud>  
`* `[638b977c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/638b977c8) simplification des setTimeout (inutile de recréer une fct qui ne fait qu'en appeler une autre) (2024-10-30 15:43) <Daniel Caillibaud>  
`* `[37a49c1e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37a49c1e2) fix _startTimer appelé en double à la fermeture de la modale (2024-10-30 15:43) <Daniel Caillibaud>  
`* `[d52b593e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d52b593e5) Merge branch 'pb_bug_valid_scratchprog' into 'main' (2024-10-30 14:08) <Tommy Barroy>  
`* `[4d1f8792f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d1f8792f) pb valid ok ou pas scratchprog (2024-10-30 15:07) <Tommy Barroy>  

## version 1.2.115

`*   `[f9718884c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f9718884c) Merge branch 'atmeti' into 'main' (2024-10-30 13:17) <Tommy Barroy>  
`|\  `  
`| * `[529819745](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/529819745) ajoute une nvel ressource modele (2024-10-30 14:16) <Tommy Barroy>  
`| * `[1299eaa11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1299eaa11) aj sec et sauv (2024-10-29 12:07) <Tommy Barroy>  
`*   `[ec9957aad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec9957aad) Merge branch 'Amelioration_Section_excalcmathquill' into 'main' (2024-10-30 12:09) <yves biton>  
`|\  `  
`| * `[e2bd74860](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2bd74860) Amélioration de la section excalcmathquill : on peut maintenant choisir en paramètres la taille de police de l'éditeur, des réponses successives de l'élève et de la correction. (2024-10-30 13:06) <Yves Biton>  
`* | `[61598e500](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61598e500) Ajout d'une modale pour prévenir en cas de temps limité (2024-10-30 13:02) <Daniel Caillibaud>  
`* | `[1308a4b12](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1308a4b12) getCssDimension accepte (min|max|fit)-content (2024-10-30 12:58) <Daniel Caillibaud>  
`* | `[4a446d2c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a446d2c3) fix Modal title (2024-10-30 12:57) <Daniel Caillibaud>  
`|/  `  

## version 1.2.114

`* `[134475360](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/134475360) aj normalizeMtgOptions pour mettre decimalDot à false par défaut (2024-10-30 09:45) <Daniel Caillibaud>  
`* `[5272d7aa0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5272d7aa0) reponseIncomplete pris dans les textes génériques pour une centaine de sections qui l'avaient en dur. (2024-10-29 18:02) <Daniel Caillibaud>  
`* `[f163e837e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f163e837e) console de debug virés (2024-10-29 18:02) <Daniel Caillibaud>  
`*   `[ced35567a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ced35567a) Merge branch 'ajoutIndices' into 'main' (2024-10-29 16:25) <Remi Deniaud>  
`|\  `  
`| * `[294ab8601](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/294ab8601) ajout d'une ligne dans les tableau de tableauIndices (2024-10-29 17:23) <Rémi Deniaud>  
`* `[c7713de90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7713de90) getDonnees viré (volumecompt) (2024-10-29 14:12) <Daniel Caillibaud>  
`* `[08c31919d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/08c31919d) textes virés car déclarés et jamais utilisés (2024-10-29 14:12) <Daniel Caillibaud>  
`* `[ab9388b6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab9388b6a) textes et pe virée (déclarés et jamais utilisés) (2024-10-29 14:12) <Daniel Caillibaud>  
`* `[14af7ddca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14af7ddca) reponseIncomplete prise dans les textes génériques (2024-10-29 14:12) <Daniel Caillibaud>  
`* `[e3ff85647](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e3ff85647) inutile de calculer questionCourante modulo nbetapes, etapeCourante est là pour ça (2024-10-29 14:08) <Daniel Caillibaud>  
`* `[f4db33825](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f4db33825) nettoyage et aj de FIXME sur du comportement peu orthodoxe (2024-10-29 14:08) <Daniel Caillibaud>  
`* `[0972f7603](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0972f7603) un peu de clarification (2024-10-29 14:08) <Daniel Caillibaud>  
`* `[ade72b635](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ade72b635) inutile de mélanger plusieurs fois la même liste… (2024-10-29 14:08) <Daniel Caillibaud>  
`* `[cb16e025c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb16e025c) inutile d'appeler surcharge() quand il n'y a rien à surcharger mais il faut le faire pour imposer nbetapes|nbchances|autre param du graphe (2024-10-29 14:08) <Daniel Caillibaud>  
`* `[f8f77fb44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8f77fb44) code inutile viré (formules) (2024-10-29 14:08) <Daniel Caillibaud>  
`*   `[9e4a8bf82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e4a8bf82) Merge branch 'cleanDecompose' into 'main' (2024-10-29 12:25) <Tommy Barroy>  
`|\  `  
`| * `[d9f2724a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9f2724a2) ti bug (2024-10-29 13:22) <Tommy Barroy>  
`| * `[29acf76be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/29acf76be) un peu de clarification (2024-10-29 11:55) <Daniel Caillibaud>  
`| * `[81eb3439f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81eb3439f) classe Facteur externalisée (2024-10-29 11:47) <Daniel Caillibaud>  
`| * `[18bc4bb7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/18bc4bb7e) mutualisation des textes (2024-10-29 11:40) <Daniel Caillibaud>  
`| * `[9c3ea3d46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c3ea3d46) mutualisation de code dupliqué (2024-10-29 11:35) <Daniel Caillibaud>  
`| * `[971589ce3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/971589ce3) textes déclarés et jamais utilisés virés (decompose01) (2024-10-29 11:10) <Daniel Caillibaud>  
`|/  `  
`* `[75f54af10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75f54af10) code inutile viré (trigo01) (2024-10-29 11:04) <Daniel Caillibaud>  
`* `[2c54ddd6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c54ddd6e) code inutile viré (2024-10-29 10:51) <Daniel Caillibaud>  
`* `[e6a6d1b5c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e6a6d1b5c) code inutile viré (2024-10-29 10:49) <Daniel Caillibaud>  
`* `[f101e8c8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f101e8c8c) textes déclarés mais jamais utilisés (virés) (2024-10-29 10:47) <Daniel Caillibaud>  
`* `[a7845a470](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7845a470) code inutile viré (2024-10-29 10:42) <Daniel Caillibaud>  
`* `[9dd96c722](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9dd96c722) nettoyage et simplification pyth01 (reste deux FIXME) (2024-10-29 10:36) <Daniel Caillibaud>  
`* `[2d018bedb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d018bedb) fix initCptPe (2024-10-29 10:31) <Daniel Caillibaud>  
`* `[35d23a523](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35d23a523) Correction de titre d'exercice (2024-10-29 09:24) <Yves Biton>  
`* `[5ec0cc26b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ec0cc26b) Correction de faute d'orthographe (2024-10-29 09:16) <Yves Biton>  
`* `[ce7305de3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce7305de3) Corrcetion de la correction de cet exercice. [fix #322](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/322) (2024-10-29 09:14) <Yves Biton>  
`*   `[2e1713f08](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e1713f08) Merge branch 'clean' into 'main' (2024-10-28 20:58) <Tommy Barroy>  
`|\  `  
`| * `[dc57ef004](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc57ef004) vire indication (2024-10-28 21:52) <Tommy Barroy>  
`| * `[3a08b0412](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a08b0412) vire indication (2024-10-28 21:51) <Tommy Barroy>  
`| * `[8b7a05049](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b7a05049) vire indication (2024-10-28 21:49) <Tommy Barroy>  
`| * `[11f642c54](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/11f642c54) vire indication (2024-10-28 21:48) <Tommy Barroy>  
`| * `[edba170a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edba170a0) clean ordredegrandeur (2024-10-28 21:47) <Tommy Barroy>  
`| * `[cb8fa4176](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb8fa4176) clean propor03 (2024-10-28 21:38) <Tommy Barroy>  
`| * `[415553e02](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/415553e02) clean propor03 (2024-10-28 21:37) <Tommy Barroy>  
`| * `[42a85e783](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42a85e783) clean priorite01 (2024-10-28 21:34) <Tommy Barroy>  
`|/  `  
`*   `[f5a983c1b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5a983c1b) Merge branch 'mainpush' into 'main' (2024-10-28 20:10) <Tommy Barroy>  
`|\  `  
`| * `[c7a9b5021](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7a9b5021) clean trigo01 (2024-10-28 21:08) <Tommy Barroy>  
`| * `[b2d651918](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2d651918) clean decompose01 (2024-10-28 21:02) <Tommy Barroy>  
`| * `[4f18f29c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f18f29c3) clean decompose02 (2024-10-28 20:57) <Tommy Barroy>  
`| * `[7367d1090](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7367d1090) clean formules (2024-10-28 20:51) <Tommy Barroy>  
`| * `[d993cbe07](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d993cbe07) clean stat03 (2024-10-28 20:45) <Tommy Barroy>  

## version 1.2.113

`*   `[b390947b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b390947b2) Merge branch 'zarbug' into 'main' (2024-10-28 18:59) <Tommy Barroy>  
`|\  `  
`| * `[a15fca51b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a15fca51b) pb a linit je sais pas dou ca vient (2024-10-28 19:58) <Tommy Barroy>  

## version 1.2.112

`*   `[3a0112a0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a0112a0c) Merge branch 'newIndices' into 'main' (2024-10-28 15:13) <Remi Deniaud>  
`|\  `  
`| * `[43efd1502](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43efd1502) ajout de la section utilisationIndices (2024-10-28 16:09) <Rémi Deniaud>  
`*   `[f2a27576e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2a27576e) Merge branch 'pasdepe' into 'main' (2024-10-28 14:55) <Tommy Barroy>  
`|\  `  
`| * `[f6809d28f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6809d28f) pe sur angle03 et 04 (2024-10-28 15:49) <Tommy Barroy>  
`* | `[a42986eff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a42986eff) Merge branch 'pasdepe' into 'main' (2024-10-28 13:35) <Tommy Barroy>  
`|\| `  
`| * `[d44284a6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d44284a6a) y'avait pas de pe (2024-10-28 14:33) <Tommy Barroy>  
`|/  `  

## version 1.2.111

`*   `[aed32873e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aed32873e) Merge branch 'pbstorage_a_cause_de_getdonnes' into 'main' (2024-10-28 11:58) <Tommy Barroy>  
`|\  `  
`| * `[57022930d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57022930d) erreur de storage effacer par getdonnes dans agrand01 (2024-10-28 12:51) <Tommy Barroy>  
`| * `[a96901eae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a96901eae) erreur de storage effacer par getdonnes dans triegau4 (2024-10-28 12:43) <Tommy Barroy>  
`|/  `  

## version 1.2.110

`*   `[ef88e1090](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef88e1090) Merge branch 'pe_1' into 'main' (2024-10-28 08:59) <Tommy Barroy>  
`|\  `  
`| * `[e4f8489c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4f8489c9) aj explik zone de saisie (2024-10-28 09:55) <Tommy Barroy>  
`* | `[ccb2209a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ccb2209a1) Merge branch 'pe_1' into 'main' (2024-10-27 20:29) <Tommy Barroy>  
`|\| `  
`| * `[1d4104ee4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d4104ee4) vire pe de prefix (2024-10-27 21:20) <Tommy Barroy>  
`| * `[f8f6a3275](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8f6a3275) vire pe de proporgraphe (2024-10-27 21:18) <Tommy Barroy>  
`|/  `  

## version 1.2.109

`*   `[5afc9c4ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5afc9c4ab) Merge branch 'mohaint' into 'main' (2024-10-27 17:12) <Tommy Barroy>  
`|\  `  
`| * `[56f37392f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/56f37392f) chg titre dialog et bug quand prog eleve bug en mode aléatoire multitests (2024-10-27 18:10) <Tommy Barroy>  
`|/  `  
`*   `[483fd22e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/483fd22e5) Merge branch 'pourpush' into 'main' (2024-10-27 09:59) <Tommy Barroy>  
`|\  `  
`| * `[8e1663989](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e1663989) javais pas mis lobjet ou il fallait (2024-10-27 10:57) <Tommy Barroy>  
`|/  `  

## version 1.2.108

`*   `[b8267f10b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8267f10b) Merge branch 'amelioreglemoha' into 'main' (2024-10-26 18:46) <Tommy Barroy>  
`|\  `  
`| * `[38bb398bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38bb398bb) change le point de rotation de la règle (2024-10-26 20:44) <Tommy Barroy>  
`|/  `  

## version 1.2.107

`*   `[c7a4f9992](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7a4f9992) Merge branch 'notif_pour_capt' into 'main' (2024-10-26 12:06) <Tommy Barroy>  
`|\  `  
`| * `[8b3d77c49](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b3d77c49) nettoie un peu et notif quand ya pas storage ( je capte pas pk ) (2024-10-26 14:05) <Tommy Barroy>  
`|/  `  

## version 1.2.106

`*   `[ca7771d02](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca7771d02) Merge branch 'cacheerreur' into 'main' (2024-10-26 09:11) <Tommy Barroy>  
`|\  `  
`| * `[acb61aa65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/acb61aa65) met un try catch pour pas bloquer la ressource (2024-10-26 11:08) <Tommy Barroy>  
`|/  `  
`*   `[d4342e148](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4342e148) Merge branch 'ajimage' into 'main' (2024-10-26 08:01) <Tommy Barroy>  
`|\  `  
`| * `[4f97b429b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f97b429b) ajout image sectionsMake (2024-10-26 10:00) <Tommy Barroy>  

## version 1.2.105

`*   `[55bfbd97b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55bfbd97b) Merge branch 'erreur_sur_pe' into 'main' (2024-10-26 01:21) <Tommy Barroy>  
`|\  `  
`| * `[d614550b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d614550b3) curseur texte (2024-10-26 03:20) <Tommy Barroy>  
`| * `[4ae3ef110](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ae3ef110) un event sans nom et force bien nb essai illimite (2024-10-26 03:10) <Tommy Barroy>  

## version 1.2.104

`*   `[4c80202a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c80202a2) Merge branch 'erreur_sur_pe' into 'main' (2024-10-26 00:19) <Tommy Barroy>  
`|\  `  
`| * `[5749fd501](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5749fd501) dautres trucs de pe (2024-10-26 02:17) <Tommy Barroy>  
`| * `[c8e5e8115](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c8e5e8115) de pe pas au bon endroit peut etre (2024-10-26 02:01) <Tommy Barroy>  
`|/  `  
`*   `[08c38b4ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/08c38b4ed) Merge branch 'label_dans_image_Clck' into 'main' (2024-10-25 20:06) <Tommy Barroy>  
`|\  `  
`| * `[2562c5bc2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2562c5bc2) des petits bug, et ajoute label et cache dans mage clic (2024-10-25 22:04) <Tommy Barroy>  
`* | `[a6802a1ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6802a1ef) aj jsdoc (2024-10-25 20:19) <Daniel Caillibaud>  
`* | `[cbb874623](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cbb874623) Factorisation du code de gestion des pe (des sections de Tommy) (2024-10-25 20:16) <Daniel Caillibaud>  
`* | `[f642f132a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f642f132a) calcul de pe viré (y'a pas de pe dans ces sections) (2024-10-25 19:35) <Daniel Caillibaud>  
`* | `[5799cb8a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5799cb8a5) pe supprimées car déclarées mais jamais utilisées (priorite01) (2024-10-25 19:28) <Daniel Caillibaud>  
`* | `[ff2712eab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff2712eab) pe supprimées car déclarées mais jamais utilisées (multdiv10base) (2024-10-25 19:28) <Daniel Caillibaud>  
`* | `[a0b0710e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0b0710e5) pe supprimées car déclarées mais jamais utilisées (multiplevoc) (2024-10-25 19:27) <Daniel Caillibaud>  
`* | `[b80845a36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b80845a36) pe supprimées car déclarées mais jamais utilisées (fraction13) (2024-10-25 19:26) <Daniel Caillibaud>  
`* | `[e2686ec98](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2686ec98) pe supprimées car déclarées mais jamais utilisées (fraction12) (2024-10-25 19:26) <Daniel Caillibaud>  
`* | `[b407ea5af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b407ea5af) pe supprimées car déclarées mais jamais utilisées (fraction11) (2024-10-25 19:25) <Daniel Caillibaud>  
`* | `[31b073308](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31b073308) pe supprimées car déclarées mais jamais utilisées (fraction10) (2024-10-25 19:25) <Daniel Caillibaud>  
`* | `[76124559f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76124559f) pe supprimées car déclarées mais jamais utilisées (fraction08) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[4fd011507](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fd011507) pe supprimées car déclarées mais jamais utilisées (fraction05) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[8380b60a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8380b60a5) pe supprimées car déclarées mais jamais utilisées (fraction04) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[917251ad3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/917251ad3) pe supprimées car déclarées mais jamais utilisées (fraction03) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[cdb4a5f40](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cdb4a5f40) pe supprimées car déclarées mais jamais utilisées (angle02) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[7b3e54e95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b3e54e95) pe supprimées car déclarées mais jamais utilisées (angle01) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[3455b6d0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3455b6d0c) pe supprimées car déclarées mais jamais utilisées (compose01) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[edfbf81c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edfbf81c8) pe supprimées car déclarées mais jamais utilisées (med01) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[4a1b07c0f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a1b07c0f) pe supprimées car déclarées mais jamais utilisées (distptdrt) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[fb36cf091](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb36cf091) pe supprimées car déclarées mais jamais utilisées (recon01) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[acce144e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/acce144e4) pe supprimées car déclarées mais jamais utilisées (conserve01) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[396e63d20](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/396e63d20) pe supprimées car déclarées mais jamais utilisées (compose01) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[99e5e1c3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/99e5e1c3f) pe supprimées car déclarées mais jamais utilisées (fraction09) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[5676be0a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5676be0a9) pe supprimées car déclarées mais jamais utilisées (factorise01) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[b3bb27438](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3bb27438) pe supprimées car déclarées mais jamais utilisées (puissancesdef) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[f5c2c9a4e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5c2c9a4e) pe supprimées car déclarées mais jamais utilisées (testeq) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[43c7e241c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43c7e241c) pe supprimées car déclarées mais jamais utilisées (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[f1d17442a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1d17442a) mutualisation de getPe (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[07fa39eef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07fa39eef) mutualisation des params entre thales01 et thales02, ajout d'une fct getPe pour factoriser du code copié dans toutes les sections de Tommy avec pe (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[d65d181b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d65d181b7) encore du nettoyage (thales01) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[74995ec2c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/74995ec2c) getDonnees et autre code inutile viré (thales02) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[5673ffc66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5673ffc66) getDonnees et autre code inutile viré (repere01) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[7bd158250](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7bd158250) getDonnees viré (ReciproquePythagore) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[730214463](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/730214463) getDonnees et autre code inutile viré (puissancesSimple) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[b1ef49393](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1ef49393) getDonnees et autre code inutile viré (puissancesdef10) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[94f016aa1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94f016aa1) getDonnees et autre code inutile viré (pyth01) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[c8cb11be0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c8cb11be0) getDonnees et autre code inutile viré (puissancesdef) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[a86755cc2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a86755cc2) getDonnees et autre code inutile viré (proporgraph) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[9a4f1b972](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a4f1b972) getDonnees et autre code inutile viré (prefixes01) (2024-10-25 18:40) <Daniel Caillibaud>  
`* | `[856837f31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/856837f31) getDonnees viré (nature), avec pas mal de code inutile (2024-10-25 18:40) <Daniel Caillibaud>  
`|/  `  
`*   `[11ae12790](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/11ae12790) Merge branch 'relabranche' into 'main' (2024-10-25 10:14) <Tommy Barroy>  
`|\  `  
`| * `[763adcbad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/763adcbad) essai lfs (2024-10-25 12:13) <Tommy Barroy>  
`|/  `  
`*   `[68421a477](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68421a477) Merge branch 'laBranche' into 'main' (2024-10-25 10:13) <Tommy Barroy>  
`|\  `  
`| * `[2a5d69771](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a5d69771) essai lfs (2024-10-25 12:09) <Tommy Barroy>  
`|/  `  
`* `[f0b932037](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0b932037) Merge branch 'cache_old_rep' into 'main' (2024-10-25 10:06) <Tommy Barroy>  
`* `[69e3a3eb5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69e3a3eb5) cache ancienne réponse (2024-10-25 12:05) <Tommy Barroy>  

## version 1.2.103

`*   `[35be47a09](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35be47a09) Merge branch 'modifcss' into 'main' (2024-10-25 09:48) <Tommy Barroy>  
`|\  `  
`| * `[c6808a049](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6808a049) vire css (2024-10-25 11:47) <Tommy Barroy>  
`|/  `  
`*   `[e187ccff0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e187ccff0) Merge branch 'ajout_label' into 'main' (2024-10-25 09:45) <Tommy Barroy>  
`|\  `  
`| * `[b7292965c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7292965c) fini label et amelio co (2024-10-25 11:20) <Tommy Barroy>  
`| * `[421e50aa4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/421e50aa4) ajout label (2024-10-25 03:53) <Tommy Barroy>  
`|/  `  

## version 1.2.102

`*   `[5ed483b51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ed483b51) Merge branch 'maingfd' into 'main' (2024-10-23 22:07) <Tommy Barroy>  
`|\  `  
`| * `[6a7c8081e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a7c8081e) pb de variables aléatoire sur le programme vérif, et cahcer drapeau et réinit quand prog démo, et vice versa (2024-10-24 00:06) <Tommy Barroy>  
`|/  `  
`* `[8129eef2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8129eef2d) Merge branch 'ajangle_dans_mtglibre' into 'main' (2024-10-23 16:23) <Tommy Barroy>  
`* `[77594c3f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77594c3f1) met point crée dans liste pour pouvoir l'effacer mtglibre (2024-10-23 18:21) <Tommy Barroy>  

## version 1.2.101

`*   `[f81f3c2d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f81f3c2d1) Merge branch 'amelio' into 'main' (2024-10-23 15:51) <Tommy Barroy>  
`|\  `  
`| * `[64492ef61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64492ef61) mais plus faux quand temps dépassé dans vignettes (2024-10-23 17:50) <Tommy Barroy>  
`| * `[8d8516c82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d8516c82) ajoute des plygones a n cote dans resMake 2 (2024-10-23 17:41) <Tommy Barroy>  
`| * `[bf6c4f94e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf6c4f94e) modif Aide et propose zone de saisie encadrée ou non (2024-10-23 15:43) <Tommy Barroy>  
`|/  `  

## version 1.2.100

`*   `[f509302da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f509302da) Merge branch 'pourfin' into 'main' (2024-10-23 09:24) <Tommy Barroy>  
`|\  `  
`| * `[37b1a963b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37b1a963b) ameliore place image de fond et vire console et amélio test resMAke2 (2024-10-23 11:22) <Tommy Barroy>  
`|/  `  

## version 1.2.99

`*   `[db919d7e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db919d7e4) Merge branch 'rapprocheJL' into 'main' (2024-10-22 22:30) <Tommy Barroy>  
`|\  `  
`| * `[4c8a28cc3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c8a28cc3) chg titre (2024-10-23 00:29) <Tommy Barroy>  
`| * `[e3541b7e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e3541b7e4) vire nbchances (2024-10-23 00:27) <Tommy Barroy>  
`| * `[72d4263c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72d4263c4) prend en compte la correction (2024-10-23 00:26) <Tommy Barroy>  
`* | `[3c7cc4ea1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c7cc4ea1) Merge branch 'rapprocheJL' into 'main' (2024-10-22 22:15) <Tommy Barroy>  
`|/  `  
`* `[1fd8e9b24](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1fd8e9b24) rapproche les etiquette quand cest possible (2024-10-23 00:12) <Tommy Barroy>  

## version 1.2.98

`*   `[cc46e8f96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc46e8f96) Merge branch 'editpk' into 'main' (2024-10-22 21:55) <Tommy Barroy>  
`|\  `  
`| * `[3614780fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3614780fb) ajout section resAtome (2024-10-22 23:53) <Tommy Barroy>  
`|/  `  
`*   `[d1e37a47d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1e37a47d) Merge branch 'secTAtome' into 'main' (2024-10-22 20:37) <Tommy Barroy>  
`|\  `  
`| * `[c727e42e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c727e42e6) ajout section resAtome (2024-10-22 18:59) <Tommy Barroy>  
`* | `[77523bddd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77523bddd) ajout du cas diapo.php des manuels Sésamath dans guessImgUrl (2024-10-22 19:44) <Daniel Caillibaud>  
`|/  `  
`* `[7f896d035](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f896d035) Merge branch 'co_correc_contrai' into 'main' (2024-10-22 10:33) <Tommy Barroy>  
`* `[5766f6584](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5766f6584) bug a cause pe dans contraire01 (2024-10-22 12:32) <Tommy Barroy>  

## version 1.2.97

`*   `[86b197d2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86b197d2b) Merge branch 'latex' into 'main' (2024-10-22 10:01) <Tommy Barroy>  
`|\  `  
`| * `[11c6c3981](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/11c6c3981) afiche avert en mathquill aussi dans resMake2 (2024-10-22 11:38) <Tommy Barroy>  
`|/  `  
`* `[df6873a44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df6873a44) Merge branch 'modif_ordre_exo' into 'main' (2024-10-22 09:11) <Tommy Barroy>  
`* `[2990bf8f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2990bf8f2) modif ale ordre niv dans resMake aussi, et taille fixe de img dans resMake2 (2024-10-22 11:09) <Tommy Barroy>  

## version 1.2.96

`*   `[e667f65ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e667f65ac) Merge branch 'modif_ordre_exo' into 'main' (2024-10-22 08:22) <Tommy Barroy>  
`|\  `  
`| * `[b745c8a0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b745c8a0c) possib de modif place dep de grand scratch (2024-10-22 10:20) <Tommy Barroy>  
`| * `[a9eae480a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9eae480a) ch orde quand oui par niv ou non par niv (2024-10-22 10:08) <Tommy Barroy>  
`|/  `  
`*   `[7b588407d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b588407d) Merge branch 'vireindic' into 'main' (2024-10-21 22:18) <Tommy Barroy>  
`|\  `  
`| * `[fd1099772](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd1099772) vire une indication ya plus deplace (2024-10-22 00:11) <Tommy Barroy>  
`|/  `  
`*   `[78ff6e85b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/78ff6e85b) Merge branch 'termine_wen_svg' into 'main' (2024-10-21 22:02) <Tommy Barroy>  
`|\  `  
`| * `[95506f368](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95506f368) png plus proppres (2024-10-22 00:00) <Tommy Barroy>  
`|/  `  
`* `[6fb103857](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6fb103857) Merge branch 'ortho' into 'main' (2024-10-21 21:04) <Tommy Barroy>  
`* `[eb66f1db7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb66f1db7) ortho ortho ortho (2024-10-21 23:03) <Tommy Barroy>  

## version 1.2.95

`*   `[c8d42809b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c8d42809b) Merge branch 'cleanFractionCalcul' into 'main' (2024-10-21 20:40) <Tommy Barroy>  
`|\  `  
`| * `[cd634bbbf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd634bbbf) getDonnees viré (fractioncalculs) + modifs params pour mettre nbTentatives à la place de nbchances (qui était ensuite récupéré et remis à 1) (2024-10-21 18:27) <Daniel Caillibaud>  
`* |   `[b96d8b909](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b96d8b909) Merge branch 'virecons' into 'main' (2024-10-21 20:35) <Tommy Barroy>  
`|\ \  `  
`| * | `[05623f32d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05623f32d) vire les ocnsoles (2024-10-21 22:34) <Tommy Barroy>  
`|/ /  `  
`* |   `[b20b21a87](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b20b21a87) Merge branch 'cleanTestEq' into 'main' (2024-10-21 20:31) <Tommy Barroy>  
`|\ \  `  
`| * \   `[e42c3b23b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e42c3b23b) Merge branch 'main' into 'cleanTestEq' (2024-10-21 20:29) <Tommy Barroy>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | |   `[1100e0fa1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1100e0fa1) Merge branch 'co_blokmoha_gd_nombre' into 'main' (2024-10-21 16:30) <Tommy Barroy>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[b12403775](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b12403775) bug sur gd nb de tests (2024-10-21 18:28) <Tommy Barroy>  
`* | | `[d7dfa7810](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7dfa7810) getDonnees viré (contraire01) (2024-10-21 18:26) <Daniel Caillibaud>  
`* | | `[35c603423](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35c603423) getDonnees viré (stat02) (2024-10-21 17:33) <Daniel Caillibaud>  
`* | | `[c35ffb061](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c35ffb061) getDonnees viré (sommealgebrique) (2024-10-21 17:27) <Daniel Caillibaud>  
`* | | `[b6313c27e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6313c27e) getDonnees viré (relatifsuneop) (2024-10-21 17:27) <Daniel Caillibaud>  
`* | | `[ca1efd143](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca1efd143) getDonnees viré (parentheses) (2024-10-21 17:27) <Daniel Caillibaud>  
`* | | `[fa56ff75a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa56ff75a) var => const (2024-10-21 17:27) <Daniel Caillibaud>  
`* | | `[0223bbf94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0223bbf94) getDonnees viré (distance01) (2024-10-21 17:27) <Daniel Caillibaud>  
`* | | `[4ba7d079f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ba7d079f) getDonnees viré (comparerelatifs) (2024-10-21 17:27) <Daniel Caillibaud>  
`* | | `[66dc95d12](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66dc95d12) getDonnees viré (quad03) (2024-10-21 17:27) <Daniel Caillibaud>  
`* | | `[f2a46afdc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2a46afdc) getDonnees viré (factorise01) (2024-10-21 17:27) <Daniel Caillibaud>  
`* | | `[8459ae33f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8459ae33f) getDonnees viré (ineg01) (2024-10-21 17:27) <Daniel Caillibaud>  
`* | | `[d5c8e83d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5c8e83d9) code inutile viré (2024-10-21 17:27) <Daniel Caillibaud>  
`* | |   `[57d7bc621](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57d7bc621) Merge branch 'indices' into 'main' (2024-10-21 14:23) <Remi Deniaud>  
`|\ \ \  `  
`| * | | `[bfec12edc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bfec12edc) petite modif dans tableauIndices (2024-10-21 16:22) <Rémi Deniaud>  
`| * | | `[a474ebffa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a474ebffa) "ajout de la section tableauIndices" (2024-10-21 12:13) <Rémi Deniaud>  
`|  / /  `  
`| | * `[469fb206b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/469fb206b) remet les sectioncourantes (2024-10-21 22:26) <Tommy Barroy>  
`| | * `[7e13a7cf6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e13a7cf6) nettoyage (sectionCourante => finCorrection, var virés, etc.) (2024-10-21 16:46) <Daniel Caillibaud>  
`| | * `[36b56b008](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/36b56b008) getDonnees viré (testeq) (2024-10-21 16:18) <Daniel Caillibaud>  
`| |/  `  
`|/|   `  
`* | `[bfdf09aad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bfdf09aad) getDonnees viré (rezeq) (2024-10-21 15:51) <Daniel Caillibaud>  
`* | `[bd5a06650](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd5a06650) getDonnees viré (equation_dichotomie) (2024-10-21 15:51) <Daniel Caillibaud>  
`* | `[159791346](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/159791346) type fix (2024-10-21 15:51) <Daniel Caillibaud>  
`* | `[87469662b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87469662b) getDonnees viré (excalc) (2024-10-21 15:51) <Daniel Caillibaud>  
`* | `[2d8fbe3c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d8fbe3c2) getDonnees viré (demanglecorres01) (2024-10-21 15:51) <Daniel Caillibaud>  
`* | `[954dbf76a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/954dbf76a) getDonnees viré (blok08) (2024-10-21 15:51) <Daniel Caillibaud>  
`|/  `  
`*   `[5205d7aae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5205d7aae) Merge branch 'repare_etikMake' into 'main' (2024-10-21 13:23) <Tommy Barroy>  
`|\  `  
`| * `[9709a9661](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9709a9661) moins les consoles (2024-10-21 15:21) <Tommy Barroy>  
`| * `[dab2b9981](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dab2b9981) etikMake enregistre mal les clones (2024-10-21 15:20) <Tommy Barroy>  
`* | `[43ac1b131](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43ac1b131) getDonnees viré (blokvar) (2024-10-21 15:19) <Daniel Caillibaud>  
`* | `[906204e43](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/906204e43) getDonnees viré (blokcond) (2024-10-21 15:19) <Daniel Caillibaud>  
`* | `[4a504958a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a504958a) getDonnees viré (blokcomparealea) (2024-10-21 15:19) <Daniel Caillibaud>  
`* |   `[1d6b9dc9f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d6b9dc9f) Merge branch 'Correction_Ex_Factorisation' into 'main' (2024-10-21 13:06) <yves biton>  
`|\ \  `  
`| * | `[2edb29419](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2edb29419) Cette section ne distinguait pas entre réponse exacte et factorisée (mais nin finale) et réponse exacte et non factorisée. (2024-10-21 15:04) <Yves Biton>  
`|  /  `  
`* | `[266f3672c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/266f3672c) getDonnees viré (blokmohascratch, code mort viré au passage) (2024-10-21 15:05) <Daniel Caillibaud>  
`* | `[67bdae8e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67bdae8e4) modif énoncé (2024-10-21 14:48) <Daniel Caillibaud>  
`* | `[8b45acf6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b45acf6a) getDonnees viré (blokformules) (2024-10-21 14:41) <Daniel Caillibaud>  
`* | `[6bd968703](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6bd968703) aj jsdoc (2024-10-21 14:35) <Daniel Caillibaud>  
`* | `[2e41b15f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e41b15f1) loadSection() accepte que le chemin soit dans la section (format 'dir/subdir/sectionName') (2024-10-21 14:35) <Daniel Caillibaud>  
`|/  `  

## version 1.2.94

`*   `[339a859a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/339a859a3) Merge branch 'petits_bug' into 'main' (2024-10-21 09:41) <Tommy Barroy>  
`|\  `  
`| * `[b1c1ef225](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1c1ef225) double fleche pour agrandir compas (2024-10-21 11:37) <Tommy Barroy>  
`| * `[cc6de2813](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc6de2813) ajoute modif niveau dans editor etik Make (2024-10-21 11:04) <Tommy Barroy>  
`| * `[7435ca1bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7435ca1bd) met en opcity 0.5 les couleurs de correction (2024-10-21 10:48) <Tommy Barroy>  
`|/  `  
`*   `[968de894b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/968de894b) Merge branch 'modifModalFullScreen2' into 'main' (2024-10-21 07:22) <Daniel Caillibaud>  
`|\  `  
`| * `[80f9aaa6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80f9aaa6e) avec un param fullScreenIframe dans modal.ts (2024-10-16 21:18) <Tommy Barroy>  

## version 1.2.93

`*   `[4fa0acab9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fa0acab9) Merge branch 'ajout_quotient' into 'main' (2024-10-20 15:47) <Tommy Barroy>  
`|\  `  
`| * `[849827bbf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/849827bbf) ajout quotient reste dans blokmohascratch (2024-10-20 17:45) <Tommy Barroy>  
`| * `[524d0e294](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/524d0e294) ajout quotient reste dans blokmohascratch (2024-10-20 17:44) <Tommy Barroy>  
`|/  `  
`*   `[a4f80e86e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a4f80e86e) Merge branch 'rafraich' into 'main' (2024-10-20 14:57) <Tommy Barroy>  
`|\  `  
`| * `[1ff70552e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ff70552e) vire console et un # (2024-10-20 16:56) <Tommy Barroy>  
`* | `[f6cc2abe0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6cc2abe0) Merge branch 'rafraich' into 'main' (2024-10-20 14:51) <Tommy Barroy>  
`|\| `  
`| * `[2a4f2a4ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a4f2a4ab) fais plus beau tout (2024-10-20 16:48) <Tommy Barroy>  
`*   `[702170c48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/702170c48) Merge branch 'Suppresion_Boutons_Inutiles' into 'main' (2024-10-20 13:15) <yves biton>  
`|\  `  
`| * `[ce410df8a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce410df8a) Suppression de boutons inutiles pour la ressource Frabçais vers calculs sans carrés (2024-10-20 15:13) <Yves Biton>  

## version 1.2.92

`*   `[cb42e430a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb42e430a) Merge branch 'rafraich' into 'main' (2024-10-19 22:05) <Tommy Barroy>  
`|\  `  
`| * `[413101a3e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/413101a3e) chg tete compas (2024-10-20 00:04) <Tommy Barroy>  
`|/  `  

## version 1.2.91

`*   `[33f54b583](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33f54b583) Merge branch 'termine' into 'main' (2024-10-19 16:44) <Tommy Barroy>  
`|\  `  
`| * `[6257cf74e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6257cf74e) c'est mieux mnt (2024-10-19 18:42) <Tommy Barroy>  

## version 1.2.90

`*   `[8cd73283b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cd73283b) Merge branch 'termine' into 'main' (2024-10-19 14:50) <Tommy Barroy>  
`|\  `  
`| * `[7f4e881bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f4e881bf) remet bien les bons lutins scratch et main, montre correction bonne ou fausse en cas d'aléa (2024-10-19 16:46) <Tommy Barroy>  
`|/  `  
`*   `[b0bbaff20](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0bbaff20) Merge branch 'gjgjk' into 'main' (2024-10-19 12:51) <Tommy Barroy>  
`|\  `  
`| * `[9dcfcf879](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9dcfcf879) montre le dernier quand tout est bon dans mohascratch avec aléa (2024-10-19 14:50) <Tommy Barroy>  
`|/  `  
`*   `[3311b50c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3311b50c4) Merge branch 'pbProportion' into 'main' (2024-10-19 08:57) <Remi Deniaud>  
`|\  `  
`| * `[259089364](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/259089364) correction erreur section proportion (2024-10-19 10:53) <Rémi Deniaud>  
`|/  `  
`*   `[d37fe4eab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d37fe4eab) Merge branch 'pbCanoniqueTabVar' into 'main' (2024-10-19 08:19) <Remi Deniaud>  
`|\  `  
`| * `[d53835643](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d53835643) correction du pb de coefAlpha nul dans canonique_tabvar (2024-10-19 10:18) <Rémi Deniaud>  
`|/  `  
`*   `[5f528e92d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f528e92d) Merge branch 'pbValAbs' into 'main' (2024-10-19 07:48) <Remi Deniaud>  
`|\  `  
`| * `[1b3e0d5d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b3e0d5d1) fin correction resolution_valabs (2024-10-19 09:42) <Rémi Deniaud>  
`| * `[16c8784d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16c8784d5) nettoyage section resolution_valabs + correction petit oubli lorsque type_eq vaut 2 (2024-10-19 09:24) <Rémi Deniaud>  
`* `[cc83f4339](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc83f4339) Merge branch 'modif_la_co_quand_alea' into 'main' (2024-10-19 01:43) <Tommy Barroy>  
`* `[4634385b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4634385b4) javais raté la liste de nv de cas (2024-10-19 03:42) <Tommy Barroy>  

## version 1.2.89

`*   `[ced4a3df2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ced4a3df2) Merge branch 'modif_la_co_quand_alea' into 'main' (2024-10-19 01:24) <Tommy Barroy>  
`|\  `  
`| * `[39390d6d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39390d6d4) amelio quand plusieurs aléa et garde modif varia dans éditor (2024-10-19 03:23) <Tommy Barroy>  
`| * `[6979e726d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6979e726d) affiche bien les variabes en co, et supprime cache co en mode multi infini, et gere double, tripe, ... alea en debut de prog (2024-10-19 02:42) <Tommy Barroy>  
`* | `[86fb0668d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86fb0668d) Merge branch 'modif_la_co_quand_alea' into 'main' (2024-10-18 23:16) <Tommy Barroy>  
`|\| `  
`| * `[45aa7f5cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45aa7f5cc) teste toutes les valeurs quand ya alea (2024-10-19 01:15) <Tommy Barroy>  

## version 1.2.88

`*   `[fd9780b61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd9780b61) Merge branch 'bug_sur_oldmod' into 'main' (2024-10-18 14:49) <Tommy Barroy>  
`|\  `  
`| * `[3b25d592f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b25d592f) oldmod ne se mettait pas à jour (2024-10-18 16:48) <Tommy Barroy>  
`|/  `  

## version 1.2.87

`* `[86b3943fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86b3943fd) Merge branch 'corrige_text_non_reconnu' into 'main' (2024-10-18 13:29) <Tommy Barroy>  
`* `[b5f505486](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5f505486) yavait un souci sur bloc text et prog lancé en rapide (2024-10-18 15:27) <Tommy Barroy>  

## version 1.2.86

`*   `[f1161b24d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1161b24d) Merge branch 'ticket' into 'main' (2024-10-18 13:00) <Tommy Barroy>  
`|\  `  
`| * `[61e645ed9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61e645ed9) [fix #336](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/336) (2024-10-18 14:58) <Tommy Barroy>  

## version 1.2.85

`*   `[97de0e701](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97de0e701) Merge branch 'ajdeplactitescases' into 'main' (2024-10-18 12:07) <Tommy Barroy>  
`|\  `  
`| * `[e18613c8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e18613c8b) et les images of courses (2024-10-18 14:06) <Tommy Barroy>  
`| * `[b8e56f115](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8e56f115) fini les images de blocs (2024-10-18 14:04) <Tommy Barroy>  
`| * `[45259914b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45259914b) presque fini les images de blocs ( mais faut que je réponde à Daniel ) (2024-10-18 13:43) <Tommy Barroy>  
`| * `[066af0b48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/066af0b48) corrige 2 blocs coulurCases et cachceco en gorrigeGood ( décalage ) (2024-10-18 13:15) <Tommy Barroy>  
`| * `[c04997d73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c04997d73) termine blocs par cases (2024-10-18 12:45) <Tommy Barroy>  
`| * `[76e8f0d3b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76e8f0d3b) manquait des images (2024-10-18 12:20) <Tommy Barroy>  
`| * `[8c7ac0ca6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c7ac0ca6) met bien les cache co en relative (2024-10-18 12:09) <Tommy Barroy>  
`| * `[81b62d5ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81b62d5ec) ajout dans correction les couleurs de cases (2024-10-18 11:28) <Tommy Barroy>  
`| * `[32bfbec04](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32bfbec04) ajout init x y petit scratch (2024-10-18 06:48) <Tommy Barroy>  
`| * `[2b3894d59](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b3894d59) vire les console.error (2024-10-17 17:33) <Tommy Barroy>  
`| * `[90d27880f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90d27880f) finis les block dessins dans editor( sauf petit scractch ) , ajout des cases colorées aléatoires dans fond petit scratch (2024-10-17 16:27) <Tommy Barroy>  
`| * `[0ee47c179](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ee47c179) des images pour blocs et ptites cases (2024-10-17 00:42) <Tommy Barroy>  
`| * `[40793c0a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/40793c0a3) des images pour blocs et ptites cases (2024-10-17 00:41) <Tommy Barroy>  
`* `[ec09a838d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec09a838d) fct getDonnees inutile virée dans sokoban (2024-10-18 13:36) <Daniel Caillibaud>  
`* `[f61bdce16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f61bdce16) fct getDonnees inutile virée dans 501 (2024-10-18 13:36) <Daniel Caillibaud>  
`* `[c1d428e83](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c1d428e83) fct getDonnees inutile virée dans exValidationZones (2024-10-18 13:36) <Daniel Caillibaud>  
`* `[cc2912b46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc2912b46) fct getDonnees inutile virée dans Tables (2024-10-18 13:36) <Daniel Caillibaud>  
`* `[f5aae248a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5aae248a) fct getDonnees inutile virée dans MultiplierPar10 (2024-10-18 13:36) <Daniel Caillibaud>  

## version 1.2.84

`* `[8c0cb5151](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c0cb5151) [fix #335](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/335) (2024-10-18 10:54) <Daniel Caillibaud>  
`* `[d03ff54ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d03ff54ce) simplification du code (2024-10-18 10:54) <Daniel Caillibaud>  
`* `[046e4106f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/046e4106f) nettoyage (getDonnees viré) (2024-10-18 10:54) <Daniel Caillibaud>  
`*   `[6bf851968](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6bf851968) Merge branch 'modif_bouton_du' into 'main' (2024-10-15 15:26) <Tommy Barroy>  
`|\  `  
`| * `[e710082ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e710082ad) modif bouton dupliquer dans formuleMakeEditor (2024-10-15 17:25) <Tommy Barroy>  
`|/  `  
`*   `[1fc13c342](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1fc13c342) Merge branch 'aj_ordonne' into 'main' (2024-10-15 14:55) <Tommy Barroy>  
`|\  `  
`| * `[14faf7b7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14faf7b7a) met bien whitepsace sur consigne formuleMake (2024-10-15 16:53) <Tommy Barroy>  
`| * `[3308e5b9d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3308e5b9d) met bien whitepsace sur consigne resmake2 (2024-10-15 16:51) <Tommy Barroy>  
`| * `[5a799ecf5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a799ecf5) met bien whitepsace sur consigne resmake (2024-10-15 16:48) <Tommy Barroy>  
`| * `[03feac340](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03feac340) mais ale et ordonne par niveau dans etikMake (2024-10-15 16:26) <Tommy Barroy>  
`|/  `  

## version 1.2.83

`*   `[03c73c7a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03c73c7a4) Merge branch 'prog_co_pas_co' into 'main' (2024-10-14 21:02) <Tommy Barroy>  
`|\  `  
`| * `[5f2689fee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f2689fee) javais oublié de reinit coractif (2024-10-14 23:00) <Tommy Barroy>  
`|/  `  

## version 1.2.82

`*   `[a8392e700](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8392e700) Merge branch 'erreur_co_pos' into 'main' (2024-10-14 14:00) <Tommy Barroy>  
`|\  `  
`| * `[402d4805d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/402d4805d) erreur dans correction de l'angle (2024-10-14 15:59) <Tommy Barroy>  
`|/  `  

## version 1.2.81

`*   `[27931dcad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27931dcad) Merge branch 'essaiedepasserquandmm' into 'main' (2024-10-13 08:40) <Tommy Barroy>  
`|\  `  
`| * `[26e1d2595](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/26e1d2595) essaie de faire passer (2024-10-13 10:39) <Tommy Barroy>  
`| * `[64e5f8b61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64e5f8b61) essaie de faire passer (2024-10-13 10:31) <Tommy Barroy>  
`* | `[d7d57f7b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7d57f7b0) Merge branch 'essaiedepasserquandmm' into 'main' (2024-10-13 08:26) <Tommy Barroy>  
`|\| `  
`| * `[9e2731f13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e2731f13) essaie de faire passer (2024-10-13 10:26) <Tommy Barroy>  
`|/  `  
`*   `[604f1e77b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/604f1e77b) Merge branch 'placebien' into 'main' (2024-10-13 06:04) <Tommy Barroy>  
`|\  `  
`| * `[cd50ffdff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd50ffdff) la page défilait tout en bas (2024-10-13 08:00) <Tommy Barroy>  
`| * `[7ede6aecd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ede6aecd) la page défilait tout en bas (2024-10-13 07:52) <Tommy Barroy>  
`|/  `  
`*   `[145abaf33](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/145abaf33) Merge branch 'fullscreen' into 'main' (2024-10-13 05:30) <Tommy Barroy>  
`|\  `  
`| * `[996c4ac92](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/996c4ac92) le full screen sur le parent node 2 fois pour avoir le conteneur avec l'enfant ( MG32 et blockly mettent leur popup derrière sinon ) (2024-10-13 07:29) <Tommy Barroy>  
`|/  `  

## version 1.2.80

`*   `[39c3187b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39c3187b0) Merge branch 'pourPush' into 'main' (2024-10-12 12:44) <Tommy Barroy>  
`|\  `  
`| * `[8871676a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8871676a8) chg copier par dupliquer (2024-10-12 14:42) <Tommy Barroy>  
`| * `[ef8dcbbf4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef8dcbbf4) ajoute niv pour série aléatoire par niv (2024-10-12 14:34) <Tommy Barroy>  
`|/  `  

## version 1.2.79

`*   `[ca3df9610](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca3df9610) Merge branch 'notif_pour_comprenre' into 'main' (2024-10-11 15:17) <Tommy Barroy>  
`|\  `  
`| * `[4b50db62b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b50db62b) rajoute un ? pour pas que ca plante (2024-10-11 17:16) <Tommy Barroy>  
`| * `[c39ac4378](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c39ac4378) j3pnotif pour comprendre (2024-10-11 17:07) <Tommy Barroy>  
`|/  `  

## version 1.2.78

`*   `[fe10591bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe10591bf) Merge branch 'modif_sim_parenth' into 'main' (2024-10-10 10:59) <Tommy Barroy>  
`|\  `  
`| * `[c1f82969e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c1f82969e) met 1 case par terme pour simplifier la vie (2024-10-10 12:57) <Tommy Barroy>  
`|/  `  

## version 1.2.77

`*   `[14d3c47d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14d3c47d3) Merge branch 'gardeComp' into 'main' (2024-10-09 16:19) <Tommy Barroy>  
`|\  `  
`| * `[27dfff12c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27dfff12c) laisse compas visible si il sert en 2d juste après (2024-10-09 18:17) <Tommy Barroy>  
`|/  `  

## version 1.2.76

`*   `[3740efbbe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3740efbbe) Merge branch 'notif_pour_comprendre' into 'main' (2024-10-08 15:03) <Tommy Barroy>  
`|\  `  
`| * `[c4aba00b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4aba00b6) un notif pour comprendre (2024-10-08 17:01) <Tommy Barroy>  
`|/  `  
`*   `[f9fa95f37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f9fa95f37) Merge branch 'parampasajour' into 'main' (2024-10-08 14:51) <Tommy Barroy>  
`|\  `  
`| * `[6ab56ea00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ab56ea00) param pas a jour (2024-10-08 16:50) <Tommy Barroy>  
`|/  `  

## version 1.2.75

`*   `[4a8b94cc9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a8b94cc9) Merge branch 'ajcopiecollevignette' into 'main' (2024-10-08 04:55) <Tommy Barroy>  
`|\  `  
`| * `[5cebf3e14](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5cebf3e14) insere aussi les avert (2024-10-08 06:53) <Tommy Barroy>  

## version 1.2.74

`*   `[0290f2a2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0290f2a2f) Merge branch 'ajcopiecollevignette' into 'main' (2024-10-07 20:19) <Tommy Barroy>  
`|\  `  
`| * `[f37fcd7b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f37fcd7b6) ajoute copie colle une vignette (2024-10-07 22:17) <Tommy Barroy>  
`|/  `  

## version 1.2.73

`* `[57d452a47](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57d452a47) fix test d'un nœud dans l'éditeur v1 ([fix #329](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/329)) (2024-10-07 19:56) <Daniel Caillibaud>  
`* `[4c62dc486](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c62dc486) prod1.html accepte un graphe v1 dans le hash (pas seulement un rid) (2024-10-07 19:54) <Daniel Caillibaud>  
`* `[e628ce1c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e628ce1c8) simplification en utilisant me.etapeCourante (2024-10-07 19:16) <Daniel Caillibaud>  
`* `[550b44952](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/550b44952) fix nbetapes imposé à 1 ou 2 suivant contexte (2024-10-07 19:16) <Daniel Caillibaud>  
`* `[44af869b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44af869b0) fix indication toujours undefined (2024-10-07 19:15) <Daniel Caillibaud>  
`* `[9838e05be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9838e05be) background du virtualKeyboard plus opaque (suivant ce qu'il y avait derrière on pouvait ne plus trop distinguer les boutons) (2024-10-07 19:11) <Daniel Caillibaud>  
`* `[3962b7540](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3962b7540) factorisation de code (traiteMathQuillSansMultImplicites en 4 exemplaires) (2024-10-07 18:38) <Daniel Caillibaud>  
`* `[4fc5ae9df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fc5ae9df) faut remettre l'aléatoire (qui avait été viré pour débug) (2024-10-07 18:38) <Daniel Caillibaud>  
`*   `[6cae00664](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cae00664) Merge branch 'ticketss' into 'main' (2024-10-07 15:05) <Tommy Barroy>  
`|\  `  
`| * `[c33148bff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c33148bff) [fix #332](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/332) (2024-10-07 17:03) <Tommy Barroy>  
`| * `[ac3394047](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac3394047) [fix #334](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/334) (2024-10-07 16:05) <Tommy Barroy>  
`|/  `  
`*   `[9a429934a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a429934a) Merge branch 'Correction_Sections_Suites' into 'main' (2024-10-07 10:44) <yves biton>  
`|\  `  
`| * `[92e2d30c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/92e2d30c4) Correction de cette section qui ne fonctionnait pas quand on demandait u_n+1 en fonction de u_n qu'on rentrait u_n*(-3) [fix #333](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/333) (2024-10-07 12:42) <Yves Biton>  
`|/  `  
`* `[5aff21bc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5aff21bc7) Merge branch 'dvptverscanonque2' into 'main' (2024-10-06 16:35) <Remi Deniaud>  
`* `[cf499e5a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf499e5a0) nettoyage du code de la section prodquot_affine (2024-10-06 18:32) <Rémi Deniaud>  
`* `[bf4f28a6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf4f28a6a) nettoyage du code de la section dvptverscanonique2 (2024-10-06 18:21) <Rémi Deniaud>  
`* `[540468b39](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/540468b39) autorisation du facteur -1 devant la parenthèse dans dvptverscanonique2 (2024-10-06 18:08) <Rémi Deniaud>  

## version 1.2.72

`*   `[bb194471e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb194471e) Merge branch 'unautrebugsnag' into 'main' (2024-10-06 16:03) <Tommy Barroy>  
`|\  `  
`| * `[c25469270](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c25469270) ne trouvait plus le bon objet a reconsturire (2024-10-06 18:00) <Tommy Barroy>  
`|/  `  
`*   `[fe7a7464a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe7a7464a) Merge branch 'prodquot_affine' into 'main' (2024-10-06 15:31) <Remi Deniaud>  
`|\  `  
`| * `[daae4d554](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/daae4d554) une petite erreur trainait dans la fonction tableauSigneRep (2024-10-06 17:29) <Rémi Deniaud>  
`|/  `  

## version 1.2.71

`*   `[e26770b15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e26770b15) Merge branch 'ajcalculat' into 'main' (2024-10-06 13:42) <Tommy Barroy>  
`|\  `  
`| * `[e9b4cd2cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9b4cd2cb) ajoute une calculatrice en option (2024-10-06 15:22) <Tommy Barroy>  
`|/  `  

## version 1.2.70

`*   `[6608c984e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6608c984e) Merge branch 'bugqq' into 'main' (2024-10-04 23:24) <Tommy Barroy>  
`|\  `  
`| * `[279ecc41c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/279ecc41c) justune longueur marchait pas avec les faux points (2024-10-05 01:22) <Tommy Barroy>  
`|/  `  

## version 1.2.69

`*   `[e0775a14f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e0775a14f) Merge branch 'encore' into 'main' (2024-10-04 18:48) <Tommy Barroy>  
`|\  `  
`| * `[620e6ca96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/620e6ca96) oubli un cas quand mult pour compas (2024-10-04 20:46) <Tommy Barroy>  
`|/  `  

## version 1.2.68

`*   `[94c6d4cce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94c6d4cce) Merge branch 'ajoutplus' into 'main' (2024-10-04 17:01) <Tommy Barroy>  
`|\  `  
`| * `[5ded1f658](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ded1f658) liste mal config (2024-10-04 19:00) <Tommy Barroy>  

## version 1.2.67

`*   `[b7ed85c3b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7ed85c3b) Merge branch 'notif_pour_comprendre' into 'main' (2024-10-04 15:14) <Tommy Barroy>  
`|\  `  
`| * `[0367ae323](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0367ae323) ger les multiplications de longueurs avec compas, et affichage cadres invisibles (2024-10-04 17:13) <Tommy Barroy>  
`| * `[96c764c7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/96c764c7a) des modifs pour correction de animerep (2024-10-04 15:34) <Tommy Barroy>  
`| * `[860fa9c3b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/860fa9c3b) notify pour compendre bug (2024-10-03 21:25) <Tommy Barroy>  
`| * `[c92375c86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c92375c86) notify pour compendre bug (2024-10-03 20:25) <Tommy Barroy>  
`| * `[d02d2f093](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d02d2f093) notify pour compendre bug (2024-10-03 20:10) <Tommy Barroy>  
`|/  `  

## version 1.2.66

`*   `[79a94e417](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79a94e417) Merge branch 'prendcompte_prox' into 'main' (2024-09-29 20:42) <Tommy Barroy>  
`|\  `  
`| * `[e1e737d64](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1e737d64) oubli de remettre mode co à la correction (2024-09-29 22:40) <Tommy Barroy>  
`| * `[50fa728c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50fa728c0) prend en compte prox noprox pour deux apartient dans la correction (2024-09-29 20:33) <Tommy Barroy>  
`|/  `  

## version 1.2.65

`*   `[0748493f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0748493f4) Merge branch 'verif_nom_existe_avant' into 'main' (2024-09-28 11:47) <Tommy Barroy>  
`|\  `  
`| * `[0018b87cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0018b87cd) verif nom existe avant de travailler dessus (2024-09-28 13:44) <Tommy Barroy>  
`|/  `  

## version 1.2.64

`*   `[b1bbaab08](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1bbaab08) Merge branch 'oubli_dun_truc' into 'main' (2024-09-27 19:44) <Tommy Barroy>  
`|\  `  
`| * `[a28a0b710](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a28a0b710) là j'ai corrigé un truc mais je sais plus quoi (2024-09-27 21:41) <Tommy Barroy>  
`|/  `  
`*   `[a52b50ca7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a52b50ca7) Merge branch 'gere_touchcancel' into 'main' (2024-09-27 10:37) <Tommy Barroy>  
`|\  `  
`| * `[416b60574](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/416b60574) gerecancel etait pas géré bien (2024-09-27 12:33) <Tommy Barroy>  
`|/  `  
`*   `[7c582bea9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c582bea9) Merge branch 'bug_correc_sommet' into 'main' (2024-09-27 10:17) <Tommy Barroy>  
`|\  `  
`| * `[1f866fc12](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f866fc12) oublie de renvoyer faux quand le sommmet angle droit est faux (2024-09-27 12:12) <Tommy Barroy>  
`|/  `  

## version 1.2.63

`*   `[51795d7d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51795d7d0) Merge branch 'bugdnag_ddroite' into 'main' (2024-09-26 21:36) <Tommy Barroy>  
`|\  `  
`| * `[e846b0de0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e846b0de0) vieille def de demi droite plus gérée pareil mais ailleurs (2024-09-26 23:33) <Tommy Barroy>  
`|/  `  

## version 1.2.62

`*   `[bca0a0665](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bca0a0665) Merge branch 'notif_pour_capt' into 'main' (2024-09-25 18:54) <Tommy Barroy>  
`|\  `  
`| * `[5694b0bed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5694b0bed) vieille def de demi droite plus gérée (2024-09-25 20:48) <Tommy Barroy>  
`| * `[690a84c8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/690a84c8c) j3pnotifiy pour comprende un bug (2024-09-25 20:08) <Tommy Barroy>  
`|/  `  

## version 1.2.61

`*   `[6324338b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6324338b5) Merge branch 'Correction_Multi_Etapes_Inequation_Ensemble_Vide' into 'main' (2024-09-23 21:19) <yves biton>  
`|\  `  
`| * `[14e7678a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14e7678a5) Pour le multi étapes, un ensemble vide comme ensemble des solutions était toujours accepté comme bon. [fix #327](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/327) (2024-09-23 23:17) <Yves Biton>  
`|/  `  
`* `[23bb5c01e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23bb5c01e) blindage (fix erreur bugsnag 66ec28a39478570dd664d47a) (2024-09-23 15:19) <Daniel Caillibaud>  
`* `[1fec78a3e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1fec78a3e) blindage (fix erreur bugsnag 66e0bd743e88313d20e2c3c6) (2024-09-23 15:13) <Daniel Caillibaud>  
`* `[d14a45df4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d14a45df4) blindage (fix erreur bugsnag 66dc8e171b1f7f5fd8a5537a) (2024-09-23 15:11) <Daniel Caillibaud>  
`* `[05eebc050](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05eebc050) blindage et simplification (on utilise l'élément plutôt que son id) (2024-09-23 15:07) <Daniel Caillibaud>  
`*   `[7d115e09a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d115e09a) Merge branch 'aleadansrot' into 'main' (2024-09-22 19:24) <Tommy Barroy>  
`|\  `  
`| * `[5bd282ec9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5bd282ec9) bug sur place compas, modif point rot, corrige droite avec point a... (2024-09-22 19:24) <Tommy Barroy>  
`|/  `  
`*   `[1c0117c7c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c0117c7c) Merge branch 'aleadansrot' into 'main' (2024-09-22 00:20) <Tommy Barroy>  
`|\  `  
`| * `[4dc3f9841](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4dc3f9841) alea dans point rot deb (2024-09-22 02:17) <Tommy Barroy>  
`|/  `  
`* `[0377eec80](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0377eec80) Merge branch 'Correction_Exo_Coordonnes' into 'main' (2024-09-21 21:18) <yves biton>  
`* `[7dac27845](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7dac27845) Corrige le code base 64 de la figure de cet exercice non compatible à cause d'une erreur dans la version 8.0 [fix #326](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/326) (2024-09-21 23:16) <Yves Biton>  
`* `[8467f49ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8467f49ea) Corrige le code base 64 de la figure de cet exercice non compatble à cause d'une erreur dabs la version 8.0 [fix #326](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/326) (2024-09-21 16:20) <Yves Biton>  

## version 1.2.60

`*   `[c22428bdb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c22428bdb) Merge branch 'niv_dans_formuleMake' into 'main' (2024-09-21 21:11) <Tommy Barroy>  
`|\  `  
`| * `[6ab332a75](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ab332a75) mauvais cercle par défaut (2024-09-21 23:09) <Tommy Barroy>  
`|/  `  

## version 1.2.59

`*   `[da486269b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da486269b) Merge branch 'rend_instrument_vraiment_transparents' into 'main' (2024-09-20 20:20) <Tommy Barroy>  
`|\  `  
`| * `[04912ffbf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04912ffbf) utilise opacité 8.0 pour les instruments (2024-09-20 22:19) <Tommy Barroy>  
`|/  `  
`*   `[70848626f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70848626f) Merge branch 'manqupl' into 'main' (2024-09-20 19:42) <Tommy Barroy>  
`|\  `  
`| * `[6f4910aef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f4910aef) il manquaeit point longueur dans crea new (2024-09-20 21:40) <Tommy Barroy>  
`|/  `  
`*   `[a553c4d38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a553c4d38) Merge branch 'scratchmtg' into 'main' (2024-09-20 13:56) <Tommy Barroy>  
`|\  `  
`| * `[d32fcba68](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d32fcba68) Scratchmtg (2024-09-20 13:56) <Tommy Barroy>  
`|/  `  
`*   `[a2e67159a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2e67159a) Merge branch 'hgfdf' into 'main' (2024-09-20 12:21) <Tommy Barroy>  
`|\  `  
`| * `[f19036d17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f19036d17) essaie de recupe main (2024-09-20 14:20) <Tommy Barroy>  
`| * `[ed4f13d45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed4f13d45) chg menu pour editeur blocs (2024-09-20 14:10) <Tommy Barroy>  
`|/  `  
`*   `[86cd44664](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86cd44664) Merge branch 'gsffsgfds' into 'main' (2024-09-20 12:09) <Tommy Barroy>  
`|\  `  
`| * `[c298fb292](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c298fb292) Gsffsgfds (2024-09-20 12:09) <Tommy Barroy>  
`|/  `  

## version 1.2.58

`* `[686188ead](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/686188ead) Merge branch 'buggetpointcoor_' into 'main' (2024-09-19 18:59) <Tommy Barroy>  
`* `[ca94dfa38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca94dfa38) y'aait pas abscoord sur 3 (2024-09-19 20:58) <Tommy Barroy>  

## version 1.2.57

`*   `[480e3136c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/480e3136c) Merge branch 'bugsnag_nom_avec_dollar' into 'main' (2024-09-19 18:43) <Tommy Barroy>  
`|\  `  
`| * `[03c8cbbe0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03c8cbbe0) reparer un bug et améliore la correction pour que ca saute pas tout le temps (2024-09-19 20:41) <Tommy Barroy>  
`|/  `  

## version 1.2.56

`*   `[6464e9393](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6464e9393) Merge branch 'Correction_sectionexcalcmathquill' into 'main' (2024-09-19 16:25) <yves biton>  
`|\  `  
`| * `[968205a70](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/968205a70) Modification de cette section qui, depuis le passage aux éditeurs MathQuill ne gérait plus le paramètre acceptMult1 Si la réponse est bonne on l'affiche en vert et pas en bleu. [fix #323](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/323) (2024-09-19 18:24) <Yves Biton>  
`|/  `  
`*   `[2713dad09](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2713dad09) Merge branch 'pourticket' into 'main' (2024-09-19 14:45) <Tommy Barroy>  
`|\  `  
`| * `[6fd5727fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6fd5727fe) #324 pour fermer un ticket (2024-09-19 16:44) <Tommy Barroy>  
`|/  `  
`* `[28f9e57dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28f9e57dc) nettoyage de la section et ajout d'un max de questions (en cas d'erreur on n'ajoute plus de questions ad vitæm æternam) [fix #325](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/325) (2024-09-19 14:04) <Daniel Caillibaud>  

## version 1.2.55

`*   `[2986f3ca7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2986f3ca7) Merge branch 'bugsnagreopen' into 'main' (2024-09-18 17:46) <Tommy Barroy>  
`|\  `  
`| * `[4c130aa3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c130aa3d) faut appeler [0] du tab pour point (2024-09-18 19:45) <Tommy Barroy>  
`|/  `  

## version 1.2.54

`*   `[412966129](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/412966129) Merge branch 'pourJL' into 'main' (2024-09-18 11:38) <Tommy Barroy>  
`|\  `  
`| * `[b9d6b242a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9d6b242a) vire bbloque du bouton OK (2024-09-18 13:23) <Tommy Barroy>  
`|/  `  

## version 1.2.53

`*   `[cc37b4bba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc37b4bba) Merge branch 'bugsnagencore' into 'main' (2024-09-18 06:53) <Tommy Barroy>  
`|\  `  
`| * `[ce1f5659e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce1f5659e) remodifie l intervalle quand ya pas de limite donnée (2024-09-18 08:52) <Tommy Barroy>  
`|/  `  

## version 1.2.52

`*   `[38639ac36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38639ac36) Merge branch 'bugsnag2pourdorite' into 'main' (2024-09-17 21:36) <Tommy Barroy>  
`|\  `  
`| * `[c914d1e01](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c914d1e01) une droite a créer vraiment bien cachée (2024-09-17 23:34) <Tommy Barroy>  
`|/  `  
`*   `[b9faca35c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9faca35c) Merge branch 'bugsnag_nom' into 'main' (2024-09-17 16:58) <Tommy Barroy>  
`|\  `  
`| * `[babc617e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/babc617e7) une erreur sur nom des segments qui n existe pas toujours (2024-09-17 18:57) <Tommy Barroy>  
`|/  `  
`*   `[5dba33b6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5dba33b6f) Merge branch 'bugsnag_propo2' into 'main' (2024-09-17 14:46) <Tommy Barroy>  
`|\  `  
`| * `[08573ff24](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/08573ff24) vire les restrictions pour test (2024-09-17 16:44) <Tommy Barroy>  
`| * `[4584f516b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4584f516b) augente l'interval pour trouver un nb de temps en temps (2024-09-17 16:43) <Tommy Barroy>  
`|/  `  

## version 1.2.51

`*   `[cf8141212](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf8141212) Merge branch 'Mooification_Pour_Equations_Avec_Inconnue_Complexe' into 'main' (2024-09-17 06:40) <yves biton>  
`|\  `  
`| * `[5b6bd133a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b6bd133a) Modification des sections multi edit et multi étapes pour pouvoir gérer des équaations complexes dont l'inconnue est le conjugué de z. (2024-09-17 08:38) <Yves Biton>  

## version 1.2.50

`*   `[7640e6d52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7640e6d52) Merge branch 'pas_bon_co' into 'main' (2024-09-16 20:10) <Tommy Barroy>  
`|\  `  
`| * `[b33ee97d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b33ee97d4) pas le bon nombre affiché (2024-09-16 22:09) <Tommy Barroy>  
`|/  `  

## version 1.2.49

`*   `[075606afa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/075606afa) Merge branch 'Correction_Multi_Etapes_Desactivation_Editeurs' into 'main' (2024-09-16 15:39) <yves biton>  
`|\  `  
`| * `[e5f0e92c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5f0e92c6) Dans les exercices multi-étapes, quand on passait à l'étape suivante, les éditeurs internes éventuels contenus dans la figure mtg32 étaient désactivés dans la fonction desactiveEditeurs. Ce problème est résolu. (2024-09-16 17:04) <Yves Biton>  
`|/  `  
`*   `[89a5209f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89a5209f1) Merge branch 'Correction_Exo_Quotient_Complexes' into 'main' (2024-09-15 11:36) <yves biton>  
`|\  `  
`| * `[6d48bd24f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d48bd24f) Correction de cet exercice pour éviter le cas où le résultat du quotient est 1 ou -1 (2024-09-15 13:34) <Yves Biton>  

## version 1.2.48

`*   `[ec9505e66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec9505e66) Merge branch 'passupquandmodif' into 'main' (2024-09-13 20:48) <Tommy Barroy>  
`|\  `  
`| * `[e08436e2c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e08436e2c) oblige pas a effacer tous le programme quand y'a un chgt en amont (2024-09-13 22:45) <Tommy Barroy>  
`|/  `  

## version 1.2.47

`*   `[213a79234](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/213a79234) Merge branch 'nvieuxbug' into 'main' (2024-09-13 14:08) <Tommy Barroy>  
`|\  `  
`| * `[4a4a9f685](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a4a9f685) corrige un bug introduit en 2022... Soit personne se trompe, soit personne regarde la correction, soit personne s'en sert (2024-09-13 16:06) <Tommy Barroy>  

## version 1.2.46

`* `[01aef4c39](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01aef4c39) sectionSuivante doit être appelé dans un setTimeout(0), pour que l'appel en cours de sectionCourante se termine avant de passer à la suivante (+rectifs jsdoc) (2024-09-13 01:18) <Daniel Caillibaud>  
`* `[c168531cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c168531cb) plus de getDonnees dans les sections de videoProjection (2024-09-12 18:32) <Daniel Caillibaud>  
`* `[c1ae7f95e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c1ae7f95e) Suppression de la fct transformelettres (buggée) pour utiliser nombreEnMots (de lib/outils/conversion/nombreEnMots), [fix #319](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/319) (2024-09-12 17:35) <Daniel Caillibaud>  
`*   `[b19e18a16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b19e18a16) Merge branch 'refactoSectionsPassives' into 'main' (2024-09-12 15:11) <Daniel Caillibaud>  
`|\  `  
`| * `[2150c937b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2150c937b) Refacto de la gestion des sections sans score, ajout d'une méthode setPassive() (c'est alors le modèle qui gère le passage à la section suivante directement), [fix #316](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/316) (2024-09-12 17:10) <Daniel Caillibaud>  
`|/  `  
`* `[1fe8a3d42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1fe8a3d42) [fix #318](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/318) (détection du clic sur les flèches dans le tableau de variations) (2024-09-12 17:10) <Daniel Caillibaud>  

## version 1.2.45

`*   `[9025990fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9025990fe) Merge branch 'pourfix' into 'main' (2024-09-12 12:04) <Tommy Barroy>  
`|\  `  
`| * `[a7d7b9944](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7d7b9944) [fix #317](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/317) (2024-09-12 14:03) <Tommy Barroy>  
`|/  `  
`*   `[8056bb5b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8056bb5b8) Merge branch 'vire_un_setcolor' into 'main' (2024-09-11 20:16) <Tommy Barroy>  
`|\  `  
`| * `[6c1d518b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c1d518b2) augmente une limite pour aider (2024-09-11 22:15) <Tommy Barroy>  
`| * `[6a93b8962](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a93b8962) un setcolor pas indsipnsable jespere (2024-09-11 22:07) <Tommy Barroy>  
`|/  `  
`*   `[a14c29dc9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a14c29dc9) Merge branch 'notifbuscr' into 'main' (2024-09-11 20:00) <Tommy Barroy>  
`|\  `  
`| * `[69916b5cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69916b5cb) un notif pour quand ya des variables au nom inconnu (2024-09-11 21:58) <Tommy Barroy>  
`|/  `  

## version 1.2.44

`*   `[904b79bd8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/904b79bd8) Merge branch 'repace_nom_plus_cercle_aussi' into 'main' (2024-09-11 15:04) <Tommy Barroy>  
`|\  `  
`| * `[23577fe23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23577fe23) Repace nom plus cercle aussi (2024-09-11 15:04) <Tommy Barroy>  
`|/  `  
`*   `[8c571cac1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c571cac1) Merge branch 'notifpour_bugsnag' into 'main' (2024-09-11 10:49) <Tommy Barroy>  
`|\  `  
`| * `[c719b7773](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c719b7773) ajoute un notify pour bugs (2024-09-11 12:25) <Tommy Barroy>  
`* | `[9775beec2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9775beec2) Merge branch 'notifpour_bugsnag' into 'main' (2024-09-11 09:47) <Tommy Barroy>  
`|\| `  
`| * `[dcbd366ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dcbd366ef) ajoute un notify pour bugs (2024-09-11 11:46) <Tommy Barroy>  
`|/  `  

## version 1.2.43

`*   `[0717e0493](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0717e0493) Merge branch 'blok_supprime_sauvage' into 'main' (2024-09-11 08:16) <Tommy Barroy>  
`|\  `  
`| * `[2b9da2ad8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b9da2ad8) supprime toutes le constructions qui suivent une construction modifiée ou supprimée (2024-09-11 10:15) <Tommy Barroy>  
`|/  `  
`*   `[2c2c69e10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c2c69e10) Merge branch 'initbiencoef' into 'main' (2024-09-10 16:12) <Tommy Barroy>  
`|\  `  
`| * `[ca43faf9c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca43faf9c) pb de placement des noms des points dans editor (2024-09-10 17:55) <Tommy Barroy>  
`| * `[148b9d8a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/148b9d8a5) initialisation de pt au hasard (2024-09-10 16:24) <Tommy Barroy>  
`* `[ff155c2cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff155c2cf) genChangelog.sh commit les modifs du CHANGELOG.md (2024-09-10 07:21) <Daniel Caillibaud>  
`* `[c5b3a6c4d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c5b3a6c4d) màj deps (aj ts-node pour scripts/refreshUsage.js) (2024-09-10 07:01) <Daniel Caillibaud>  
`* `[f9ea3734c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f9ea3734c) rmq sur le pb d'incompatibilité vite avec pnpm9 (2024-09-10 07:01) <Daniel Caillibaud>  

## version 1.2.42

`*   `[5ec09c371](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ec09c371) Merge branch 'bug_ch_cercle' into 'main' (2024-09-09 22:11) <Tommy Barroy>  
`|\  `  
`| * `[9c149dcbe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c149dcbe) placement sur objet était décalé (2024-09-10 00:09) <Tommy Barroy>  
`| * `[a2d29ee29](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2d29ee29) obj doit etre visible et coloré pour intersec (2024-09-09 23:27) <Tommy Barroy>  
`| * `[a73b82b22](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a73b82b22) le choix cercle mal réglé quand y'a qu'un point (2024-09-09 23:04) <Tommy Barroy>  
`|/  `  
`*   `[f5d79a50c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5d79a50c) Merge branch 'vitesse_corrige' into 'main' (2024-09-09 20:56) <Tommy Barroy>  
`|\  `  
`| * `[53907f38c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53907f38c) change vitesse correction et reinit programme en cas de modif / suppres figure de départ (2024-09-09 22:55) <Tommy Barroy>  
`|/  `  
`*   `[76c28034b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76c28034b) Merge branch 'termine_param' into 'main' (2024-09-09 14:11) <Tommy Barroy>  
`|\  `  
`| * `[6644ee804](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6644ee804) Termine param (2024-09-09 14:11) <Tommy Barroy>  
`|/  `  
`*   `[b6baa65b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6baa65b0) Merge branch 'apos2' into 'main' (2024-09-08 20:08) <Tommy Barroy>  
`|\  `  
`| * `[3e8d64d7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e8d64d7e) un soucis avec apostrphe dans json encore mais ailleurs (2024-09-08 21:52) <Tommy Barroy>  
`|/  `  
`* `[081aaa50a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/081aaa50a) Merge branch 'gfdgd' into 'main' (2024-09-08 19:08) <Tommy Barroy>  
`* `[75c7a1660](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75c7a1660) un soucis avec apostrphe dans json (2024-09-08 21:06) <Tommy Barroy>  

## version 1.2.41

`* `[d659079c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d659079c2) Merge branch 'ajoute_ordre_efface_pour_trait_de_construction' into 'main' (2024-09-08 18:54) <Tommy Barroy>  
`* `[c83d5a615](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c83d5a615) possibilite oblige efface traits de construction (2024-09-08 18:54) <Tommy Barroy>  

## version 1.2.40

`* `[8eb68d12b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8eb68d12b) Merge branch 'poursuis_modif' into 'main' (2024-09-07 23:42) <Tommy Barroy>  
`* `[cc380144a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc380144a) Poursuis modif (2024-09-07 23:41) <Tommy Barroy>  

## version 1.2.39

`*   `[2dc0b25d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2dc0b25d6) Merge branch 'oubli_de_dte_rapide' into 'main' (2024-09-07 21:53) <Tommy Barroy>  
`|\  `  
`| * `[f79f0945a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f79f0945a) bug sur modif dte quand pas le choix du type (2024-09-07 23:52) <Tommy Barroy>  
`|/  `  
`* `[3d0f2be37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d0f2be37) Merge branch 'corrige_modif' into 'main' (2024-09-07 21:43) <Tommy Barroy>  
`* `[dd3b5875a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd3b5875a) Corrige modif (2024-09-07 21:43) <Tommy Barroy>  

## version 1.2.38

`* `[33ed9dceb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33ed9dceb) Merge branch 'ortho' into 'main' (2024-09-07 16:24) <Tommy Barroy>  
`* `[b90f8750e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b90f8750e) fote d'ortaugraf (2024-09-07 18:23) <Tommy Barroy>  

## version 1.2.37

`* `[df1e38efa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df1e38efa) Merge branch 'pour_ticket' into 'main' (2024-09-06 22:13) <Tommy Barroy>  
`* `[bf160361f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf160361f) [fix #314](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/314) (2024-09-07 00:12) <Tommy Barroy>  

## version 1.2.36

`* `[f15ca74f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f15ca74f1) Merge branch 'petits_bug' into 'main' (2024-09-06 20:28) <Tommy Barroy>  
`* `[92ed00221](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/92ed00221) eneve bouton crayon quand pas besoin (2024-09-06 22:22) <Tommy Barroy>  
`* `[2b292ed2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b292ed2d) retrouve une fonction manquante et vire bien le rapporteur (2024-09-06 22:19) <Tommy Barroy>  
`* `[a73722cf0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a73722cf0) ? mal placé dans les détails attendus (2024-09-06 21:42) <Tommy Barroy>  

## version 1.2.35

`*   `[2e9b710db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e9b710db) Merge branch 'editeurAirePerim' into 'main' (2024-09-04 09:48) <Tommy Barroy>  
`|\  `  
`| * `[fa7c6ad86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa7c6ad86) ajout param rapide et corrige boolean initialisation dans boitedialogie (2024-09-04 11:47) <Tommy Barroy>  
`| * `[58ea45165](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58ea45165) ajout param rapide et corrige boolean initialisation dans boitedialogie (2024-09-04 11:46) <Tommy Barroy>  
`| * `[5816dc5e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5816dc5e0) sauv (2024-09-04 10:59) <Tommy Barroy>  
`| * `[48514f3c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48514f3c2) sauv (2024-09-04 07:41) <Tommy Barroy>  
`| * `[444a4f927](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/444a4f927) sauv (2024-09-04 07:29) <Tommy Barroy>  
`| * `[8c04b585e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c04b585e) sauc (2024-09-03 23:14) <Tommy Barroy>  
`| * `[62e5aa848](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/62e5aa848) sauc (2024-09-03 17:59) <Tommy Barroy>  
`| * `[310c88510](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/310c88510) sauc (2024-09-03 17:39) <Tommy Barroy>  
`| * `[0266dc4e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0266dc4e8) sauc (2024-09-03 17:09) <Tommy Barroy>  
`| * `[d3c7a4d2c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3c7a4d2c) sauc (2024-09-03 16:26) <Tommy Barroy>  
`| * `[26e1c1b37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/26e1c1b37) sauc (2024-09-03 16:14) <Tommy Barroy>  
`| * `[c551f61c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c551f61c6) sauc (2024-09-03 08:07) <Tommy Barroy>  
`| * `[93fbaf426](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93fbaf426) sauv (2024-09-02 13:37) <Tommy Barroy>  
`| * `[0c446b99e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c446b99e) sauv (2024-09-02 02:19) <Tommy Barroy>  
`| * `[c6287ef43](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6287ef43) sauv (2024-09-01 22:09) <Tommy Barroy>  
`* `[a30ab29ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a30ab29ff) Merge branch 'petitug_intersec_de_truc_pas_visibles' into 'main' (2024-09-02 20:14) <Tommy Barroy>  
`* `[767e6da9b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/767e6da9b) Y'avait un soucis sur intersection de droite pas visibles (2024-09-02 22:10) <Tommy Barroy>  

## version 1.2.34

`* `[a9ac9d6d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9ac9d6d7) Merge branch 'explik_editor' into 'main' (2024-09-02 18:49) <Tommy Barroy>  
`* `[70aeefccd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70aeefccd) le correcteur retrouvait pas les lignes sans nom (2024-09-02 20:41) <Tommy Barroy>  

## version 1.2.33

`* `[22422b1bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22422b1bb) Merge branch 'bugsurnvpoint' into 'main' (2024-09-02 13:54) <Tommy Barroy>  
`* `[4a3378364](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a3378364) le correcteur retrouvait pas les lignes sans nom (2024-09-02 15:52) <Tommy Barroy>  

## version 1.2.32

`* `[ed0d0d166](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed0d0d166) Merge branch 'boolean_multiedit' into 'main' (2024-09-01 15:01) <Tommy Barroy>  
`* `[d062ea4fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d062ea4fe) petite amélioration sur le type boolean du multieditor (2024-09-01 15:27) <Daniel Caillibaud>  
`* `[3f51390b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f51390b2) bug dans la gestion boolean du multieditor ajout des param pas pris en compte (2024-08-31 22:30) <Tommy Barroy>  

## version 1.2.31

`*   `[6cc319d8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cc319d8c) Merge branch 'pblongtext' into 'main' (2024-08-31 17:15) <Tommy Barroy>  
`|\  `  
`| * `[c9b3007e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9b3007e5) les longueurs ne s'affichent pas toujours dans la consigne (2024-08-31 19:14) <Tommy Barroy>  
`|/  `  
`* `[ff24a15b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff24a15b4) Merge branch 'amel_mtglibre' into 'main' (2024-08-31 08:34) <Tommy Barroy>  
`* `[e93b7f3ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e93b7f3ae) les ocones en haut a droite se placaient pas bien (2024-08-31 10:33) <Tommy Barroy>  

## version 1.2.30

`* `[cf3e3eff0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf3e3eff0) Merge branch 'ameledit' into 'main' (2024-08-30 22:58) <Tommy Barroy>  
`* `[a37b03f70](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a37b03f70) mets les infos bulle dans un div, ils se placaient mal dans l'éditeur (2024-08-31 00:56) <Tommy Barroy>  
`* `[518f54421](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/518f54421) mets les infos bulle dans un div, ils se placaient mal dans l'éditeur (2024-08-31 00:56) <Tommy Barroy>  

## version 1.2.29

`*   `[af4cb7a89](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af4cb7a89) Merge branch 'aj_boutons_pour_message_derreurs' into 'main' (2024-08-30 15:26) <Tommy Barroy>  
`|\  `  
`| * `[fb4ea696f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb4ea696f) ajoute des boutons pour les messages d'erreurs (2024-08-30 17:25) <Tommy Barroy>  
`|/  `  
`*   `[fd8867529](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd8867529) Merge branch 'editoconst' into 'main' (2024-08-29 22:35) <Tommy Barroy>  
`|\  `  
`| * `[8cd75f797](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cd75f797) modifie les boutons dispos pour les textes (2024-08-30 00:34) <Tommy Barroy>  
`|/  `  
`* `[da93b271c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da93b271c) Merge branch 'corrig_long_dans_text' into 'main' (2024-08-29 16:35) <Tommy Barroy>  
`* `[5cc493c2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5cc493c2e) modifie les longueurs dans text étape (2024-08-29 18:33) <Tommy Barroy>  

## version 1.2.28

`*   `[5d20cfadc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d20cfadc) Merge branch 'pasmain' into 'main' (2024-08-29 10:49) <Tommy Barroy>  
`|\  `  
`| * `[c27fde7aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c27fde7aa) ameliore placement modale2 (2024-08-29 12:46) <Tommy Barroy>  
`|/  `  
`* `[338c5c86e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/338c5c86e) Merge branch 'verifpasexo_encours_dans_2_editeur' into 'main' (2024-08-29 09:55) <Tommy Barroy>  
`* `[60ccc7f78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/60ccc7f78) verif pas d'exo en cours de modifs dans 2 editeurs (2024-08-29 11:47) <Tommy Barroy>  

## version 1.2.27

`* `[a22047141](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a22047141) Merge branch 'progConstEdit' into 'main' (2024-08-29 09:29) <Tommy Barroy>  
`* `[3b0c78027](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b0c78027) pb placement divbulle (2024-08-29 11:27) <Tommy Barroy>  
`* `[6d76c562e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d76c562e) modif position de modale 2 pour etre tjrs visible (2024-08-29 11:15) <Tommy Barroy>  
`* `[8d6f0fc8f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d6f0fc8f) vire fullscrenne pour voir avertissement (2024-08-29 10:34) <Tommy Barroy>  
`* `[f4b23877a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f4b23877a) précise à pour val aléatoire dans rayon cercle (2024-08-29 10:25) <Tommy Barroy>  

## version 1.2.26

`* `[54cc56806](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54cc56806) Merge branch 'progconst2' into 'main' (2024-08-28 23:35) <Tommy Barroy>  
`* `[e6b5219ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e6b5219ae) bug pour calculs de longueurs quand les points existent pas (2024-08-29 01:33) <Tommy Barroy>  

## version 1.2.25

`*   `[976463c0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/976463c0e) Merge branch 'Optimisation_Inequations' into 'main' (2024-08-25 13:37) <yves biton>  
`|\  `  
`| * `[e4c062c44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4c062c44) Optimisation du traitement de la réponse pour les inéquations dans la sectionmulti étapes et la section de résolution d'inéquation. (2024-08-25 15:34) <Yves Biton>  
`|/  `  
`* `[7412a373e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7412a373e) Merge branch 'Amelioration_Multi_Etapes_Inequations' into 'main' (2024-08-25 12:46) <yves biton>  
`* `[03cc5bd62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03cc5bd62) Modification de la section multi étapes et de résolution d'équation pour que, si, par exemple, un ensemble des solutions d'une inéquation est [0;5], la réponse [0;1[U{1}U]1;5] soit acceptée (2024-08-25 14:44) <Yves Biton>  

## version 1.2.24

`* `[d5c9c0958](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5c9c0958) Merge branch 'Correction_Multi_Etapes_Pour_Inequations' into 'main' (2024-08-24 13:11) <yves biton>  
`* `[e3e2bc22c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e3e2bc22c) Modification de la section de resolution d'inéquation pour bien gérer  le cas des solutions isolées. Il y avait une confusion entre borne isolée et solution isolée. Ainsi nbBornesIsolees (suivi du numéro d'étape pour le mult étapes) doit s'appeler nbSolIsolees et il faut un calcul nommé est solIsolee (suivi du numéro d'étape pour le mult étapes) (2024-08-24 15:06) <Yves Biton>  

## version 1.2.23

`* `[e08087d84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e08087d84) Merge branch 'editMoha' into 'main' (2024-08-23 20:49) <Tommy Barroy>  
`* `[daa1587c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/daa1587c8) modifie bulle aide quand y'a événement (2024-08-23 22:47) <Tommy Barroy>  
`* `[8aa258ddf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8aa258ddf) callback sur nbChances mal nommée (2024-08-23 22:42) <Tommy Barroy>  

## version 1.2.22

`* `[39f34d837](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39f34d837) Merge branch 'pbquand_pas_de_valeur_donnée' into 'main' (2024-08-22 14:09) <Tommy Barroy>  
`* `[61d88e87d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61d88e87d) un objet vide dans scratch prog quand pas de bornes dans nombre aléatoire (2024-08-22 16:08) <Tommy Barroy>  

## version 1.2.21

`* `[3dec57fdc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3dec57fdc) Merge branch 'consoleerrorfaitplanter' into 'main' (2024-08-22 13:01) <Tommy Barroy>  
`* `[a40dcb983](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a40dcb983) console.error faisait planter (2024-08-22 14:56) <Tommy Barroy>  

## version 1.2.20

`*   `[bd4c8a491](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd4c8a491) Merge branch 'ajoutimage' into 'main' (2024-08-22 12:34) <Tommy Barroy>  
`|\  `  
`| * `[0ddbb2301](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ddbb2301) supprime une image mal rangée (2024-08-22 14:33) <Tommy Barroy>  
`| * `[2f2d4fdfd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f2d4fdfd) ajout d'images pour la section "programmer un tracé ( segment ) " (2024-08-22 14:32) <Tommy Barroy>  
`|/  `  
`* `[99afc63ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/99afc63ab) fix checkImgUrl lorsque l'url est celle d'une image sur un site tiers sans header Access-Control-Allow-Origin permettant de faire du fetch dessus (2024-08-22 12:34) <Daniel Caillibaud>  
`* `[91d1efc18](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/91d1efc18) mod gestion du try / catch (2024-08-22 10:05) <Daniel Caillibaud>  
`* `[687c77bfd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/687c77bfd) Merge branch 'avecuneimageenplus' into 'main' (2024-08-21 21:13) <Tommy Barroy>  
`* `[959af4561](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/959af4561) pour réparer pnpm (2024-08-21 23:12) <Tommy Barroy>  

## version 1.2.19

`* `[2103cf5af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2103cf5af) fix preBuild, ça doit continuer en cas de plantage du build précédent (2024-08-21 09:07) <Daniel Caillibaud>  
`* `[ee457d15a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee457d15a) faut remettre jquery-ui en 1.13.3 car safe-active-element a disparu en 1.14 (2024-08-21 08:17) <Daniel Caillibaud>  
`*   `[d01f48bf2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d01f48bf2) Merge branch 'bug_a_cause_de_evt' into 'main' (2024-08-21 05:32) <Tommy Barroy>  
`|\  `  
`| * `[3dc810a5f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3dc810a5f) bug a cause des nvt evts (2024-08-21 07:30) <Tommy Barroy>  
`* | `[81a1e666f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81a1e666f) màj deps (2024-08-21 06:18) <Daniel Caillibaud>  
`|/  `  
`*   `[51f2ae8f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51f2ae8f7) Merge branch 'errdanspointinet' into 'main' (2024-08-21 02:55) <Tommy Barroy>  
`|\  `  
`| * `[3a5f93c20](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a5f93c20) efface un point trop tot (2024-08-21 04:55) <Tommy Barroy>  
`|/  `  
`*   `[5535ebcb2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5535ebcb2) Merge branch 'ajtabl0' into 'main' (2024-08-21 02:22) <Tommy Barroy>  
`|\  `  
`| * `[071b61280](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/071b61280) aj 0 dans les tbles et met 10 en gris (2024-08-21 04:21) <Tommy Barroy>  
`|/  `  
`*   `[197efdc60](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/197efdc60) Merge branch 'desbugsnags' into 'main' (2024-08-21 02:10) <Tommy Barroy>  
`|\  `  
`| * `[befe3d28a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/befe3d28a) message d'erreur quand le json a été modifié à la main (2024-08-21 04:02) <Tommy Barroy>  
`| * `[120128bc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/120128bc4) bloque le bouton 'Valider' total si ya un exo en cours de modif (2024-08-21 03:49) <Tommy Barroy>  
`| * `[d90308d61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d90308d61) force un container à body quand on est en éditeur (2024-08-21 03:04) <Tommy Barroy>  
`| * `[e87a70010](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e87a70010) affiche un vide quand ya pas de titre (2024-08-21 02:41) <Tommy Barroy>  
`| * `[5de3f19b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5de3f19b7) l'équerre se placait pas bien, et une mise en page foireuse en aide (2024-08-21 02:31) <Tommy Barroy>  
`|/  `  
`*   `[9e4753c1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e4753c1a) Merge branch 'chgtBase' into 'main' (2024-08-15 07:59) <Remi Deniaud>  
`|\  `  
`| * `[d55475407](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d55475407) Ajout des corrections pour les différents cas de figure (2024-08-15 09:56) <Rémi Deniaud>  
`* `[b0c4fc6b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0c4fc6b5) Merge branch 'Amelioration_Section_Multi_Edit_Pour_Solutions_Isolees' into 'main' (2024-08-13 12:03) <yves biton>  
`* `[58fac2140](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58fac2140) Modification de la section de resolution d'inéquation pour bien gérer  le cas des solutions isolées. Si la solution contient des solutions isolées, la figure doit contenir un calcul nommé 'nbBornesIsolees' contenant le nombre de telles solutions. (2024-08-13 14:01) <Yves Biton>  

## version 1.2.18

`*   `[d218033df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d218033df) Merge branch 'Correction_Solutions_Isolees_Multi_Etape' into 'main' (2024-08-13 10:49) <yves biton>  
`|\  `  
`| * `[d35ee1461](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d35ee1461) Simplification d'appels de giveFormula2 et calculate (2024-08-13 10:30) <Yves Biton>  
`| * `[ebeeb4f01](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ebeeb4f01) Simplification d'appels de valueOf (2024-08-13 10:19) <Yves Biton>  
`| * `[d7bf8e850](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7bf8e850) Petite modif commentaire (2024-08-13 09:47) <Daniel Caillibaud>  
`| * `[3353dfbb3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3353dfbb3) Correction de code (2024-08-13 09:31) <Yves Biton>  
`| * `[4c59b94f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c59b94f9) Pour les étapes où on doit donner un ensemble des solutions, si l'ensemble des solutions était par exemple {0} U ]1; +inf[ et qu'on entrait ]1;+inf[ la solution était acceptée comme bonne mais non finie alors qu'elle est fausse. Pour une figure comportant des solutions isolées, la figure doit contenir un calcul nommé nbBornesIsolees suivi du numéro de l'étape renvyant le nombre de solutions isolées de l'ensemble des solutions. (2024-08-13 09:31) <Yves Biton>  
`* |   `[c69fdce94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c69fdce94) Merge branch 'chgtBase' into 'main' (2024-08-13 09:35) <Remi Deniaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| *   `[e9f0998ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9f0998ca) Merge branch 'main' into 'chgtBase' (2024-08-13 09:34) <Remi Deniaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* | `[be65fa31a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be65fa31a) rectif erreur ts (2024-08-13 09:03) <Daniel Caillibaud>  
`* |   `[617423e1d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/617423e1d) Merge branch 'chgtBase' into 'main' (2024-08-12 10:32) <Remi Deniaud>  
`|\ \  `  
`* \ \   `[19b025b3b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19b025b3b) Merge branch 'aj_even' into 'main' (2024-08-09 15:43) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[ba13478e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba13478e8) aj gestion evenement dans mohascratch (2024-08-09 17:43) <Tommy Barroy>  
`| * | | `[5c10959dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c10959dc) aj gestion evenement dans mohascratch (2024-08-09 17:39) <Tommy Barroy>  
`| * | | `[cb4e6afc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb4e6afc4) plud de modif taille apres la correction (2024-08-09 11:51) <Tommy Barroy>  
`| * | | `[62e0cbeff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/62e0cbeff) plud de modif taille apres la correction (2024-08-09 11:15) <Tommy Barroy>  
`| * | | `[c2dbe8e6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2dbe8e6e) plud de modif taille apres la correction (2024-08-08 18:19) <Tommy Barroy>  
`| * | | `[5b1880a75](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b1880a75) message qui sert a rien merci daile (2024-08-08 13:33) <Tommy Barroy>  
`| * | | `[1f180ef36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f180ef36) message qui sert a rien merci daile (2024-08-06 19:06) <Tommy Barroy>  
`| * | | `[f20b993e3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f20b993e3) message qui sert a rien merci daile (2024-08-06 19:06) <Tommy Barroy>  
`| * | | `[f1233decf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1233decf) pour sauv (2024-07-31 18:02) <Tommy Barroy>  
`|  / /  `  
`* | |   `[2c4688b28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c4688b28) Merge branch 'pb_place_co' into 'main' (2024-08-08 11:38) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[0414d17f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0414d17f0) plud de modif taille apres la correction (2024-08-08 13:37) <Tommy Barroy>  
`|/ / /  `  
`| | * `[9902f3859](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9902f3859) correction dans ChangementBase (2024-08-13 11:29) <Rémi Deniaud>  
`| | * `[626998ea4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/626998ea4) correction dans ChangementBase (2024-08-13 11:25) <Rémi Deniaud>  
`| |/  `  
`| * `[41ffcbd76](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41ffcbd76) quelques modifs dans chgtBase suite (2024-08-12 12:09) <Rémi Deniaud>  
`| * `[c78fa43c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c78fa43c8) "modif dans displayWork pour créer un bloc" (2024-08-12 12:04) <Rémi Deniaud>  
`| * `[117d4c87f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/117d4c87f) petites améliorations (2024-08-07 19:13) <Daniel Caillibaud>  
`| * `[b52d5ef00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b52d5ef00) Question dans un 2e block de l'énoncé (2024-08-07 19:02) <Daniel Caillibaud>  
`| * `[531f5199f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/531f5199f) check des bases fournies à l'init aussi (2024-08-07 19:02) <Daniel Caillibaud>  
`| * `[de7e70b38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de7e70b38) section chgtBase => ChangementBase (2024-08-07 19:02) <Daniel Caillibaud>  
`| * `[ddfd65bae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ddfd65bae) Ajout de validateParameters (2024-08-07 19:02) <Daniel Caillibaud>  
`| * `[5b8d7c93e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b8d7c93e) Ajout de listes contrôlées pour les params (2024-08-07 19:02) <Daniel Caillibaud>  
`| * `[9231057d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9231057d3) simplification (plus de listeNombres2 et listeNombres16, on convertit quand y'en a besoin) (2024-08-07 19:02) <Daniel Caillibaud>  
`| * `[baaa2d31e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/baaa2d31e) chgtBase sans erreur cette fois-ci, je peux aussi lancer l'exo (2024-08-07 19:02) <Rémi Deniaud>  
`| * `[d4073bb3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4073bb3a) "commit de la section chgtBase en ts" (2024-08-07 19:02) <Rémi Deniaud>  
`|/  `  
`* `[b26fb5717](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b26fb5717) bugfix restriction sur les inputs (qui marchait pas du tout, le replaceAll faisait l'inverse, heureusement il plantait sans flag g sur restriction et ça ne faisait rien) (2024-08-07 19:01) <Daniel Caillibaud>  
`* `[965da0ce3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/965da0ce3) ButtonBackSpace n'utilise plus resetInputSize (pas de raison de changer la taille de l'input si on efface un caractère, et ça réclame jQuery.textWidth dont on voudrait se passer) (2024-08-07 18:59) <Daniel Caillibaud>  
`* `[5dbe44822](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5dbe44822) si on passe une restriction globale à VirtualKeyboard on râle et vire le flag g (sinon ça déconne complètement) (2024-08-07 18:56) <Daniel Caillibaud>  
`* `[6af690e08](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6af690e08) faut propager le changement de parent en cas d'option asBlock (2024-08-07 17:33) <Daniel Caillibaud>  
`* `[05757c27a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05757c27a) bugfix (si on passe une restriction faut s'en servir) (2024-08-07 17:33) <Daniel Caillibaud>  
`* `[72e6086fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72e6086fa) aucune raison pour que inputProps soit obligatoire, mais restriction doit l'être (2024-08-07 17:33) <Daniel Caillibaud>  
`* `[2aef79fc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2aef79fc7) aj option asBlock pour les méthodes display (2024-08-07 16:45) <Daniel Caillibaud>  
`* `[2cdb2df9f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2cdb2df9f) En cas de def manquante ça affiche la string sans substitution et un message d'erreur plutôt que de planter tout l'exo. (2024-08-07 16:41) <Daniel Caillibaud>  
`* `[db575c509](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db575c509) refacto édition de node, implémentation du validate (reste à faire pour le cas multiple) (2024-08-07 16:41) <Daniel Caillibaud>  
`* `[8b1818f9a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b1818f9a) Implémentation de validateParameters dans l'éditeur (2024-08-07 11:50) <Daniel Caillibaud>  
`* `[76d4ad2a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76d4ad2a7) bugfix editor (selectedIndex pour les listes de choix unique) (2024-08-07 11:50) <Daniel Caillibaud>  
`* `[6a2f45211](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a2f45211) rectif scss (pas de règle css après des règles imbriquées) (2024-08-05 10:21) <Daniel Caillibaud>  
`* `[8a7e63300](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a7e63300) Merge branch 'docSection' into 'main' (2024-08-04 14:34) <Remi Deniaud>  
`* `[95a63164f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95a63164f) quelques corrections dans sectionsV2.md (2024-08-04 16:25) <Rémi Deniaud>  

## version 1.2.17

`* `[44ba4ba85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44ba4ba85) Merge branch 'Correction_CSS_Multi_Edit_Multi_Etapes' into 'main' (2024-08-03 10:32) <yves biton>  
`* `[c48513114](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c48513114) Les modifs précédentes ne fonctionnaient pas car les attributs devaient être en css. (2024-08-03 12:31) <Yves Biton>  

## version 1.2.16

`*   `[658c93106](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/658c93106) Merge branch 'Amelioration_CSS_Multi_Edit_Multi_Etapes' into 'main' (2024-08-03 08:52) <yves biton>  
`|\  `  
`| * `[063ab0cca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/063ab0cca) Pour que les éditeurs internes s'affichent correctement il faut imposer au div princiapl d'id divmtg32 les mêmes dimensions que celles du SVG qu'il contient. (2024-08-03 10:46) <Yves Biton>  
`* `[9848ff7bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9848ff7bb) fix bouton pour sortir de la modale d'édition pas cliquable (masqué par le contenu) (2024-08-01 15:46) <Daniel Caillibaud>  
`* `[ed87f1ec6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed87f1ec6) màj commande prepare pour husky (2024-08-01 10:31) <Daniel Caillibaud>  
`* `[f7111dc80](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7111dc80) fix picto fullscreen (qui doit se mettre à jour même si on sort avec Esc) (2024-07-31 20:42) <Daniel Caillibaud>  

## version 1.2.15

`* `[5adc2b477](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5adc2b477) pour préparer la compatibilité husky v10 (2024-07-31 20:19) <Daniel Caillibaud>  
`* `[d82f29403](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d82f29403) màj deps (et fix pnpm-lock.yaml qui était au format pnpm9) (2024-07-31 20:18) <Daniel Caillibaud>  
`*   `[f92ef23c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f92ef23c3) Merge branch 'gridAsBigIntArray' into 'main' (2024-07-31 18:16) <Daniel Caillibaud>  
`|\  `  
`| * `[75c268520](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75c268520) Ajout de marge aux nodes sur la grille (2024-07-31 20:14) <Daniel Caillibaud>  
`| * `[ab79e94a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab79e94a7) La sortie en string d'une grille contient désormais par défaut les n° de lignes et colonnes (2024-07-31 20:13) <Daniel Caillibaud>  
`| * `[dc0cfd577](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc0cfd577) La grille devient un array de bigint, pour utiliser les opérations binaires pour les tests de collisions (avec aj de test unitaire dessus) (2024-07-31 16:52) <Daniel Caillibaud>  
`* |   `[177cf998c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/177cf998c) Merge branch 'pour_bugPoints' into 'main' (2024-07-31 16:28) <Tommy Barroy>  
`|\ \  `  
`| * | `[6b91271f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b91271f0) lespoints pas tojours def (2024-07-31 18:10) <Tommy Barroy>  
`| |/  `  
`* |   `[a5a5ac493](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5a5ac493) Merge branch 'Correction_Inequation_Multi_Etapes' into 'main' (2024-07-31 16:03) <yves biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[69e1e5abb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69e1e5abb) Correction de la section multi étapes pour les résolutions d'inéquations (2024-07-31 14:15) <Yves Biton>  
`* `[82c9d5c58](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/82c9d5c58) rectif ordre surcharge eslint (2024-07-31 13:34) <Daniel Caillibaud>  
`* `[132195b0a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/132195b0a) plus de max-width: 100% d'office sur les images / video / svg (2024-07-31 13:29) <Daniel Caillibaud>  
`* `[d3a7b29fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3a7b29fc) Merge branch 'aligne_2_drapeaux' into 'main' (2024-07-30 13:51) <Tommy Barroy>  
`* `[638895375](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/638895375) 2 drapeaux qui voulaient pas s'aligner, dsl Daniel j'ai mis un tableau (2024-07-30 15:42) <Tommy Barroy>  

## version 1.2.14

`*   `[19df44674](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19df44674) Merge branch 'bugAffichage' into 'main' (2024-07-30 13:19) <Tommy Barroy>  
`|\  `  
`| * `[8044eddc2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8044eddc2) Y'avait un gros bug affichage, le td de droite de s'affichait plus (2024-07-30 15:17) <Tommy Barroy>  
`|/  `  
`* `[2f74becb1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f74becb1) Merge branch 'vireconsole' into 'main' (2024-07-29 13:15) <Tommy Barroy>  
`* `[c49a0e5ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c49a0e5ba) confusion dans nom sef et dte dans les vielles ressources (2024-07-29 15:13) <Tommy Barroy>  

## version 1.2.13

`* `[37e4428d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37e4428d7) Merge branch 'tibug' into 'main' (2024-07-29 13:05) <Tommy Barroy>  
`* `[7a7487e69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a7487e69) confusion dans nom sef et dte dans les vielles ressources (2024-07-29 15:02) <Tommy Barroy>  

## version 1.2.12

`* `[c9f83e222](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9f83e222) Merge branch 'amelo_edit' into 'main' (2024-07-29 08:12) <Tommy Barroy>  
`* `[c35bc6322](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c35bc6322) amelioration editeur progconstruction (2024-07-29 09:44) <Tommy Barroy>  

## version 1.2.11

`*   `[e318c5917](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e318c5917) Merge branch 'oubli_valmulpro' into 'main' (2024-07-21 07:05) <Tommy Barroy>  
`|\  `  
`| * `[833eb6dce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/833eb6dce) augment le nb de valeurs dispo pour tomber entre lim haute et basse (2024-07-21 09:03) <Tommy Barroy>  
`|/  `  
`*   `[0898eee73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0898eee73) Merge branch 'notifencoreplus_pour_comprendre' into 'main' (2024-07-21 06:55) <Tommy Barroy>  
`|\  `  
`| * `[90a579016](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90a579016) notif pour comprendre (2024-07-21 08:55) <Tommy Barroy>  
`|/  `  
`*   `[551aaddbb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/551aaddbb) Merge branch 'pb_dist_pt_dte' into 'main' (2024-07-21 06:47) <Tommy Barroy>  
`|\  `  
`| * `[347040f65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/347040f65) distPtDteTag dte marchait pas avec une droite pas déclarée avant (2024-07-21 08:46) <Tommy Barroy>  
`|/  `  
`*   `[242b88080](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/242b88080) Merge branch 'bug_dur_regle_et_point_cahés' into 'main' (2024-07-21 05:25) <Tommy Barroy>  
`|\  `  
`| * `[af7da8bf9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af7da8bf9) bug sur regle et points cachés au départ (2024-07-21 07:23) <Tommy Barroy>  
`|/  `  
`* `[ac9c7dc84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac9c7dc84) deps inutiles virées (2024-07-17 20:17) <Daniel Caillibaud>  
`* `[77f20f02f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77f20f02f) rectif url, sesajstools pas sur la forge (2024-07-16 20:07) <Daniel Caillibaud>  
`* `[2fab1112f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2fab1112f) réorganisations des exemples (2024-07-12 17:06) <Daniel Caillibaud>  
`* `[0d5ba6910](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d5ba6910) màj url vers forge.apps.education.fr (2024-07-12 13:07) <Daniel Caillibaud>  
`* `[66f5fc3bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66f5fc3bf) Rename test jc (2024-07-12 12:48) <Jean-Claude Lhote>  
`*   `[ac3476f91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac3476f91) Merge branch 'testTom' into 'main' (2024-07-12 10:34) <Tommy Barroy>  
`|\  `  
`| * `[76538b2a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76538b2a4) commentaire pour test (2024-07-12 12:32) <Tommy Barroy>  
`*   `[5284bc35f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5284bc35f) Merge branch 'Amelioration_Exo_Quotient_Complexes' into 'main' (2024-07-12 10:19) <Yves Biton>  
`|\  `  
`| * `[a3cb8acee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3cb8acee) Amélioration de cet exercice pour éviter le cas où le dénominateur et égal au numérateur ou est son opposé (2024-07-12 10:18) <Yves Biton>  
`* `[84535d66f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84535d66f) pas de type pour les jsdoc dans les fichiers ts (2024-07-12 09:34) <Daniel Caillibaud>  
`* `[fec502b4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fec502b4b) renommage param pasDeCroixClose => withoutClose et ajout de jsdoc (2024-07-12 09:34) <Daniel Caillibaud>  
`* `[662f32226](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/662f32226) Aj de magnetPitchX & Y pour le changer plus simplement (2024-07-12 09:34) <Daniel Caillibaud>  
`* `[1bb905566](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1bb905566) Changement picto fullscreen (2024-07-12 09:34) <Daniel Caillibaud>  
`* `[e034060fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e034060fb) aj commentaires (2024-07-12 09:34) <Daniel Caillibaud>  
`* `[ebf1182ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ebf1182ce) Merge branch 'fixFormReprise1234V2' into 'main' (2024-07-12 09:26) <Jean-Claude Lhote>  
`* `[53e5a25c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53e5a25c2) fix : on ne passait pas le graph original pour la reprise 1234 (2024-07-12 09:24) <Jean-claude Lhote>  

## version 1.2.10

`* `[2ebec357a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ebec357a) Merge branch 'oubli_init' into 'main' (2024-07-11 23:18) <Tommy Barroy>  
`* `[e8e912326](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8e912326) ajoute param pour pas de croix dans la modale, et fullscreen direct pour certains éditeurs (2024-07-11 23:16) <Tommy Barroy>  

## version 1.2.9

`*   `[525074bef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/525074bef) Merge branch 'oubli_init' into 'main' (2024-07-11 22:43) <Tommy Barroy>  
`|\  `  
`| * `[936bb3bfb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/936bb3bfb) oubli d initialiser un tableau (2024-07-11 22:41) <Tommy Barroy>  
`|/  `  
`* `[434b0c34c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/434b0c34c) Merge branch 'notify_encore' into 'main' (2024-07-11 21:37) <Tommy Barroy>  
`* `[22497934c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22497934c) notify pour capter cest quoi ce nom (2024-07-11 21:35) <Tommy Barroy>  

## version 1.2.8

`*   `[5e85cf16b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e85cf16b) Merge branch 'Corrcetion_Section_Multi_Edit' into 'main' (2024-07-11 14:03) <Yves Biton>  
`|\  `  
`| * `[5d80a778e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d80a778e) Modification de la sections multi edit pour que dans certains exercices sur les complexes ont puisse utiliser une fonction réelle de deux variables z et I dont la formule correspond à la formule complexe entrée par l'élève dans laquelle le nombre complexe i est remplacé par la variable I ce qui permet d'utiliser des dérivées partielles réelles pour contrôler la validité des calculs. (2024-07-11 14:01) <Yves Biton>  
`*   `[f8c798255](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8c798255) Merge branch 'fixTests' into 'main' (2024-07-11 10:52) <Jean-Claude Lhote>  
`|\  `  
`| * `[fe84c275d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe84c275d) Fix tests (2024-07-11 10:52) <Jean-Claude Lhote>  
`|/  `  
`* `[8c8a5540c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c8a5540c) plus de check typo sur le code, trop pénible d'ajouter tous les mots sans leur accent (2024-07-11 10:10) <Daniel Caillibaud>  
`*   `[87e7c3492](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87e7c3492) Merge branch 'un_bugsnag_et_deux_trux' into 'main' (2024-07-11 00:54) <Tommy Barroy>  
`|\  `  
`| * `[fa57f7bd6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa57f7bd6) un bgsng et deux trucs (2024-07-11 00:52) <Tommy Barroy>  
`* | `[dfb48965d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfb48965d) fix occupation de la grille en double (2024-07-10 21:19) <Daniel Caillibaud>  
`* | `[35554e6b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35554e6b4) libère la grille en supprimant un node (2024-07-10 21:06) <Daniel Caillibaud>  
`* | `[389f6f53f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/389f6f53f) listener EVENT_DRAG_START viré, il ne servait qu'à vider la grille, on fait tout au EVENT_DRAG_STOP (vider + remplir à coté + dispatch) (2024-07-10 20:43) <Daniel Caillibaud>  
`* | `[77bc744bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77bc744bf) majPosition râle mais ne plante plus si l'élément n'est pas dans le DOM (2024-07-10 20:12) <Daniel Caillibaud>  
`* | `[3e408aec5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e408aec5) checkStartingId fixe toujours un startingId s'il y a des nœuds non-fin (il ajoute le bon message) (2024-07-10 20:12) <Daniel Caillibaud>  
`* | `[5203a122f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5203a122f) rectif, pas de throw si emptyAllowed (2024-07-10 20:12) <Daniel Caillibaud>  
`* |   `[69cf0cc58](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69cf0cc58) Merge branch 'checkPositionsSuite' into 'main' (2024-07-10 20:12) <Daniel Caillibaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[3b952a235](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b952a235) modif commentaire (2024-07-10 20:09) <Daniel Caillibaud>  
`| * `[3cfa61697](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3cfa61697) amélioration de la gestion des positionnements (2024-07-10 20:01) <Daniel Caillibaud>  
`| * `[20d1617bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20d1617bd) fix throw dans un listener sans try/catch (2024-07-10 19:48) <Daniel Caillibaud>  
`| * `[8d845787d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d845787d) fix positions au démarrage (2024-07-10 19:02) <Daniel Caillibaud>  
`| * `[da7118433](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da7118433) simplifications et modif de formes (2024-07-10 19:02) <Daniel Caillibaud>  
`| * `[95dbcba60](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95dbcba60) bugfix editeur qui plante à l'init (2024-07-10 18:56) <Daniel Caillibaud>  
`| * `[28360e1d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28360e1d1) aj de Grid.dump() pour debug (2024-07-10 18:53) <Daniel Caillibaud>  
`| * `[d8e67f347](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8e67f347) clarification (`const line = this.values[lig] ?? []` laisse penser que la modif de line pourrait ne pas modifier values) (2024-07-10 17:17) <Daniel Caillibaud>  
`| * `[84e18e569](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84e18e569) le "menu" de l'éditeur devient la classe EditorTree (2024-07-10 14:53) <Daniel Caillibaud>  
`| * `[da1bb234e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da1bb234e) fix JsDoc (2024-07-10 13:34) <Jean-claude Lhote>  
`| * `[bf018df18](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf018df18) WIP : nettoyage des todo de positions et ajout de sceneUI dans le dropListener du menu + checkPositions dans addItemToScene (2024-07-10 13:32) <Jean-claude Lhote>  
`* |   `[9060b3b82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9060b3b82) Merge branch 'bugnsag_notif' into 'main' (2024-07-10 16:32) <Tommy Barroy>  
`|\ \  `  
`| * | `[d57945068](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d57945068) notify pour comprendre (2024-07-10 16:30) <Tommy Barroy>  
`* | | `[8e47d7a2c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e47d7a2c) Harmonisation du validate (qui retourne toujours du CheckResults) (2024-07-10 13:11) <Daniel Caillibaud>  
`* | | `[9bbe695a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9bbe695a1) cosmétique (transition à l'affichage de la modale, ça reste ≠ entre édition et ajout) (2024-07-10 13:11) <Daniel Caillibaud>  
`| |/  `  
`|/|   `  
`* |   `[10eda467e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10eda467e) Merge branch 'fixMovedNodesList' into 'main' (2024-07-10 12:48) <Jean-Claude Lhote>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[a80ec57df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a80ec57df) pas de wrap dans les imports (2024-07-10 12:44) <Jean-claude Lhote>  
`| * `[69357de3e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69357de3e) Plus de dispatch dans onStateChange(). Le check des positions est effectué en amont donc les positions sont bonnes (2024-07-10 12:42) <Jean-claude Lhote>  
`* |   `[341bf2b59](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/341bf2b59) Merge branch 'notify_pour_comprendre' into 'main' (2024-07-10 11:18) <Tommy Barroy>  
`|\ \  `  
`| * | `[1d7cb3616](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d7cb3616) notify pour comprendre (2024-07-10 11:17) <Tommy Barroy>  
`|/ /  `  
`* |   `[4ee5dee40](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ee5dee40) Merge branch 'fyuuy' into 'main' (2024-07-09 16:59) <Tommy Barroy>  
`|\ \  `  
`| * | `[76dcd7b4e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76dcd7b4e) pour sauv test (2024-07-09 15:16) <Tommy Barroy>  
`| * | `[521eb78c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/521eb78c4) pour sauv (2024-07-09 01:28) <Tommy Barroy>  
`| * | `[a1189ded2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1189ded2) pour sauv (2024-07-09 00:23) <Tommy Barroy>  
`| * | `[d9d3bcbdd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9d3bcbdd) pour sauv (2024-07-08 18:15) <Tommy Barroy>  
`| * | `[22e5ae642](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22e5ae642) pour sauv (2024-07-05 09:45) <Tommy Barroy>  
`| * | `[a27f2d6cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a27f2d6cd) pour sauv (2024-07-05 09:22) <Tommy Barroy>  
`| * | `[57cd0c364](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57cd0c364) pour sauv (2024-07-02 15:49) <Tommy Barroy>  
`| * | `[522133d4e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/522133d4e) peut etre fin du progconst bug (2024-07-01 22:54) <Tommy Barroy>  
`| * | `[25844538c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25844538c) notify sur limite haute/basse (2024-07-01 18:38) <Tommy Barroy>  
`| * | `[848429c84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/848429c84) appel de .cond qui n'existe pas tjrs (2024-06-25 15:41) <Tommy Barroy>  
`| * | `[362e106eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/362e106eb) appel de .cond qui n'existe pas tjrs (2024-06-24 12:31) <Tommy Barroy>  
`| * | `[38ddc91ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38ddc91ae) appel de .cond qui n'existe pas tjrs (2024-06-24 00:42) <Tommy Barroy>  
`|  /  `  
`* / `[2c78ee468](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c78ee468) Merge branch 'fixMovedNodesList' into 'main' (2024-07-09 16:51) <Jean-Claude Lhote>  
`|/  `  
`* `[87851663f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87851663f) Les positions des noeuds automatiquement replacés n'étaient plus sauvegardés dans le state. C'est de nouveau opérationnel. (2024-07-09 16:49) <Jean-claude Lhote>  

## version 1.2.7

`*   `[61d0c5e75](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61d0c5e75) Merge branch 'Amelioration_Multi_Etapes_Equations_Complexes' into 'main' (2024-07-09 11:09) <Yves Biton>  
`|\  `  
`| * `[712583509](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/712583509) Modification de sections multi edit et multi étapes pour que dans certains exercices sur les complexes ont puisse utiliser une fonction réelle de deux variables z et I dont la formule correspond à la formule complexe entrée par l'élève dans laquelle le nombre complexe i est remplacé par la variable I ce qui permet d'utiliser des dérivées partielles réelles pour contrôler la validité des calculs. (2024-07-09 11:07) <Yves Biton>  
`* `[5cbcfcc70](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5cbcfcc70) Ajouter un nœud le paramètre en même temps (+ mutualisation de code) (2024-07-09 06:10) <Daniel Caillibaud>  
`*   `[d880f93ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d880f93ed) Merge branch '308-ajouter-l-action-definir-comme-premier-noeud' into 'main' (2024-07-08 11:46) <Jean-Claude Lhote>  
`|\  `  
`| * `[e1c3139a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1c3139a5) ajout de l'action setStartingNode et de l'item correspondant dans NodeContextMEnu (2024-07-08 11:45) <Jean-claude Lhote>  
`|/  `  
`* `[f6c213726](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6c213726) message de debug viré (2024-07-06 10:16) <Daniel Caillibaud>  
`* `[2b384b9b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b384b9b6) mod mineure spValidate.ts (2024-07-06 08:25) <Daniel Caillibaud>  
`*   `[791343d62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/791343d62) Merge branch 'checkPositionNode' into 'main' (2024-07-06 07:27) <Jean-Claude Lhote>  
`|\  `  
`| * `[ab22bebb5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab22bebb5) ajout de setStartingId (2024-07-06 07:26) <Jean-claude Lhote>  
`* | `[3931b9468](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3931b9468) Merge branch 'checkPositionNode' into 'main' (2024-07-06 06:44) <Jean-Claude Lhote>  
`|\| `  
`| * `[16dd64d52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16dd64d52) nettoyage du scene manager : il ne s'occupe plus de la grille ni des boutons, c'est SceneUI qui gère + correction orthographe dans les scss (2024-07-06 06:41) <Jean-claude Lhote>  
`| * `[f395efbd5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f395efbd5) wip ajout de checkPosition au SceneContextMenu (2024-07-05 22:04) <Jean-claude Lhote>  
`| * `[3556d6a9e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3556d6a9e) wip refacto de SceneUi et SceneManager (2024-07-05 20:21) <Jean-claude Lhote>  
`* `[4a0d03759](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a0d03759) blindage spEdit (2024-07-05 20:43) <Daniel Caillibaud>  
`* `[de3d0e86f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de3d0e86f) spEditGraph => spEdit & spShowPathway => spView (par cohérence) (2024-07-05 20:43) <Daniel Caillibaud>  
`* `[676b3b2b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/676b3b2b2) appels de sectionCourante() en double (2024-07-05 20:43) <Daniel Caillibaud>  
`* `[a21c64ec5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a21c64ec5) oubli mineur (2024-07-05 20:43) <Daniel Caillibaud>  
`* `[e5ab6be22](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5ab6be22) nettoyage pixelMake (2024-07-05 20:43) <Daniel Caillibaud>  
`* `[3e116d443](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e116d443) nettoyage etikMake (2024-07-05 20:43) <Daniel Caillibaud>  
`* `[a00b58137](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a00b58137) nettoyage ratio01 (2024-07-05 20:43) <Daniel Caillibaud>  
`* `[7273d1467](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7273d1467) nettoyage prog01 (2024-07-05 20:43) <Daniel Caillibaud>  
`* `[27eec858b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27eec858b) premier nettoyage de remplace (il reste du boulot) (2024-07-05 20:43) <Daniel Caillibaud>  

## version 1.2.6

`* `[a1a67db6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1a67db6f) fix bugsnag 66619d0cc43dd80008117837 (2024-07-05 14:50) <Daniel Caillibaud>  
`* `[54f54b093](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54f54b093) fix bugsnag 664e0bdffc861f0008c99f3d (2024-07-05 14:50) <Daniel Caillibaud>  
`* `[1a0e733a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a0e733a0) fix appel de sectionCourante() avec changement d'état avant d'avoir appelé finCorrection (ça peut déclencher des bugs de timer) (2024-07-05 14:50) <Daniel Caillibaud>  
`* `[555673795](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/555673795) blindage _tickTimer contre les rigolos qui modifient l'horloge (cf erreur bugsnag 668154be439f7b0008a006f3) (2024-07-05 14:50) <Daniel Caillibaud>  
`*   `[73a14b06e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/73a14b06e) Merge branch 'fixSupprNode' into 'main' (2024-07-05 13:58) <Jean-Claude Lhote>  
`|\  `  
`| * `[00d98c2b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00d98c2b0) La suppression d'un noeud était buguée depuis la dernière modification, c'est réglé. (2024-07-05 12:36) <Jean-claude Lhote>  
`* | `[cc279768b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc279768b) onChangeParameter et validateParameters doivent retourner Promise<CheckResults> (2024-07-05 12:43) <Daniel Caillibaud>  
`* | `[1b9564e21](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b9564e21) simplification tableauSignesVariations (2024-07-05 12:43) <Daniel Caillibaud>  
`* | `[449ebd96c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/449ebd96c) fix apostrophe courbe dans le code python (2024-07-05 12:43) <Daniel Caillibaud>  
`|/  `  
`*   `[254bcd2d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/254bcd2d5) Merge branch 'addConnectorFeedback' into 'main' (2024-07-05 11:22) <Jean-Claude Lhote>  
`|\  `  
`| * `[29a7a8d83](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/29a7a8d83) ajoute la saisie du feedback sur un connecteur (2024-07-05 11:21) <Jean-claude Lhote>  
`|/  `  
`*   `[df4edd9da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df4edd9da) Merge branch 'bugsnag_undefind' into 'main' (2024-07-05 10:50) <Tommy Barroy>  
`|\  `  
`| * `[1b2d5aa66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b2d5aa66) pas assez de multiplicateur possible (2024-07-05 10:48) <Tommy Barroy>  
`|/  `  
`*   `[e60ea6da5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e60ea6da5) Merge branch 'bugsnag_correc' into 'main' (2024-07-05 10:37) <Tommy Barroy>  
`|\  `  
`| * `[bc3d52032](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bc3d52032) bug correc (2024-07-05 10:35) <Tommy Barroy>  
`* `[df23cd86b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df23cd86b) Merge branch 'fixTabInTable' into 'main' (2024-07-05 10:17) <Jean-Claude Lhote>  
`* `[51a602269](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51a602269) fixe le problème du tab dans les tables qui font du 'change' et validaient la réponse (2024-07-05 10:15) <Jean-claude Lhote>  

## version 1.2.5

`* `[237b4da82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/237b4da82) faut charger jQuery dans lazyLoader sinon ça plante dans loadJqueryDialog (seulement en mode build, faudra trouver pourquoi) (2024-07-04 20:09) <Daniel Caillibaud>  
`* `[95d4bc4cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95d4bc4cb) nettoyage (quasiment plus d'id, j3pDiv viré) (2024-07-04 20:09) <Daniel Caillibaud>  
`* `[39fce8081](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39fce8081) blindage (on arrête avec un message clair si formulaire2 n'est pas dans la figure) (2024-07-04 20:09) <Daniel Caillibaud>  
`* `[4bf7c1311](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4bf7c1311) blindage j3pAjouteBouton (ajout d'un try/catch autour du listener) (2024-07-04 20:09) <Daniel Caillibaud>  
`* `[ea529b8fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea529b8fd) [fix #304](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/304) (appel d'upgradeParametres | upgradeParameters par l'éditeur et le player v2) (2024-07-04 20:09) <Daniel Caillibaud>  
`* `[b592731b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b592731b8) graph.complete() en double (2024-07-04 20:09) <Daniel Caillibaud>  
`* `[3a3f264f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a3f264f8) fix upgradeParametres (modifParams était foireux) (2024-07-04 20:09) <Daniel Caillibaud>  
`*   `[f1078aea8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1078aea8) Merge branch 'AjouteNoeudModale' into 'main' (2024-07-04 17:28) <Jean-Claude Lhote>  
`|\  `  
`| * `[25f4dfbd7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25f4dfbd7) fixe le problème de contextMenu.close() (2024-07-04 17:27) <Jean-claude Lhote>  
`* | `[6c27af6b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c27af6b3) Merge branch 'AjouteNoeudModale' into 'main' (2024-07-04 17:03) <Jean-Claude Lhote>  
`|\| `  
`| * `[0a6571a6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a6571a6e) Ajouter un noeud ouvre une modale (2024-07-04 17:01) <Jean-claude Lhote>  
`* |   `[0579dc3e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0579dc3e7) Merge branch 'fixCalcMultiEditApresIntermV2' into 'main' (2024-07-04 16:54) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[843e6a01f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/843e6a01f) si les affichages n'existent pas on le signale et on continue (mais ça règle pas tous les pbs) (2024-07-04 14:42) <Daniel Caillibaud>  
`| * `[c284f47f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c284f47f7) hotfix upgradeParametres avec this null dans l'éditeur v1 (2024-07-04 14:41) <Daniel Caillibaud>  
`|/  `  
`* `[a27885281](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a27885281) Le player gère mieux le clic sur section suivante des v1 (2024-07-04 08:52) <Daniel Caillibaud>  
`* `[90dd7835f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90dd7835f) code en double (getElement) (2024-07-04 08:50) <Daniel Caillibaud>  
`* `[3d275f65f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d275f65f) bugfix : upgradeParametres des sections v1 n'était jamais appelé par le player v2 (pas de SectionContext pour elles) (2024-07-04 07:16) <Daniel Caillibaud>  
`* `[df36ee180](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df36ee180) next est facultatif dans spEditGraph (2024-07-04 05:22) <Daniel Caillibaud>  
`* `[6f9c55b44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f9c55b44) on remet id (2024-07-04 05:07) <Daniel Caillibaud>  
`* `[adcd812db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/adcd812db) largeurs en em (pour suivre le grossissement de la police au ctrl+) (2024-07-04 05:06) <Daniel Caillibaud>  
`* `[d066f2ea1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d066f2ea1) phrase d’état => évaluation qualitative (2024-07-04 05:01) <Daniel Caillibaud>  
`*   `[2ae4f401f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ae4f401f) Merge branch 'NodeContent' into 'main' (2024-07-03 19:43) <Jean-Claude Lhote>  
`|\  `  
`| * `[ace66ad32](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ace66ad32) Modification Présentation Id (2024-07-03 18:58) <Jean-claude Lhote>  
`| * `[1fa37d0ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1fa37d0ab) suppression du confirmMsg en cas de suppression du noeud et retour avec Undo (2024-07-03 18:57) <Jean-claude Lhote>  
`| * `[595545ffa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/595545ffa) fixe la largeur max d'un noeud (2024-07-03 18:13) <Jean-claude Lhote>  
`* |   `[eee4b997e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eee4b997e) Merge branch 'modifEndPoint' into 'main' (2024-07-03 19:42) <Jean-Claude Lhote>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[5c8dbe047](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c8dbe047) change la forme et la couleur des endPoints. (2024-07-03 16:58) <Jean-claude Lhote>  
`|/  `  
`* `[e7d598b9a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e7d598b9a) améliorations cosmétiques suite à visio (reste des couleurs à affiner) (2024-07-02 21:04) <Daniel Caillibaud>  
`* `[4c99b42ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c99b42ac) Amélioration des textes de condition affichés en hover (2024-07-02 21:04) <Daniel Caillibaud>  
`* `[9fbeb77fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9fbeb77fb) connecteur sans condition avec border d'étiquette un peu plus épais (2024-07-02 21:03) <Daniel Caillibaud>  
`* `[860dce22e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/860dce22e) Fix texte des connexions sans condition (2024-07-02 20:15) <Daniel Caillibaud>  
`* `[bc94d7d71](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bc94d7d71) option clean sur spValidate (2024-07-02 17:43) <Daniel Caillibaud>  
`* `[a96ed58e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a96ed58e9) rectif après cherry-picking (2024-07-02 17:42) <Daniel Caillibaud>  
`* `[190291590](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/190291590) évolution du chargement (aj spLoading pour savoir où on en est) (2024-07-02 17:41) <Daniel Caillibaud>  
`* `[af240971e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af240971e) ajout option emptyAllowed + complete sur getGraph + SceneUI.notif() (2024-07-02 17:40) <Daniel Caillibaud>  
`* `[a7e4e4465](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7e4e4465) modifs mineures (2024-07-02 17:40) <Daniel Caillibaud>  
`* `[e480c1f5b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e480c1f5b) Aj du spValidate (2024-07-02 17:40) <Daniel Caillibaud>  
`* `[3d68d84e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d68d84e2) Merge branch 'fixPositionMenu' into 'main' (2024-07-02 17:23) <Jean-Claude Lhote>  
`* `[c12b20a33](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c12b20a33) fixe le soucis des menus qui s'ouvrent au mauvais endroit (2024-07-02 17:20) <Jean-claude Lhote>  
`* `[c730a488f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c730a488f) position menu (2024-07-02 17:20) <Jean-claude Lhote>  
`* `[8100a219d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8100a219d) majLabel fonctionne à nouveau (2024-07-02 17:20) <Jean-claude Lhote>  

## version 1.2.4

`* `[e20c66515](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e20c66515) [fix #301](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/301) (tableau de variations) (2024-07-02 15:35) <Daniel Caillibaud>  
`*   `[ec124f74c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec124f74c) Merge branch 'bugsnag2' into 'main' (2024-07-01 20:16) <Tommy Barroy>  
`|\  `  
`| * `[135ed8111](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/135ed8111) peut etre fin du progconst bug (2024-07-01 20:14) <Tommy Barroy>  
`|/  `  
`*   `[846ac6376](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/846ac6376) Merge branch 'notif_pour_max_call_size' into 'main' (2024-07-01 19:17) <Tommy Barroy>  
`|\  `  
`| * `[bdf83048f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bdf83048f) notify pour max call size (2024-07-01 19:16) <Tommy Barroy>  
`|/  `  
`*   `[a069e06b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a069e06b3) Merge branch 'bugsnag1' into 'main' (2024-07-01 18:45) <Tommy Barroy>  
`|\  `  
`| * `[1674012bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1674012bc) oubli de virer l'anime sur section suivante (2024-07-01 18:44) <Tommy Barroy>  
`|/  `  
`*   `[01270e10c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01270e10c) Merge branch 'clicDroit' into 'main' (2024-07-01 12:42) <Jean-Claude Lhote>  
`|\  `  
`| * `[0add61869](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0add61869) Placement des noeuds créés et problème du form cumulatif dans le menu contextuel. (2024-07-01 12:40) <Jean-claude Lhote>  
`| * `[e4e2c8e15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4e2c8e15) click droit et cosmétique (2024-06-30 12:04) <Jean-claude Lhote>  
`|/  `  
`* `[ab6028719](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab6028719) cosmétique (croix et menu du nœud dans l'éditeur) (2024-06-29 18:31) <Daniel Caillibaud>  
`* `[59c2d2039](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/59c2d2039) amélioration typage avec distinction RessourceJ3p (v2) et RessourceJ3pBibli (v1 ou v2) (2024-06-29 18:31) <Daniel Caillibaud>  
`* `[8a2497568](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a2497568) moins de bruit en console (2024-06-28 23:00) <Daniel Caillibaud>  
`* `[dd065f6e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd065f6e8) Ajout de graph.checkStartingId (2024-06-28 23:00) <Daniel Caillibaud>  
`* `[b45742fe1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b45742fe1) amélioration des boutons pour editor (mais il restent collés) (2024-06-28 23:00) <Daniel Caillibaud>  
`* `[565e9b01a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/565e9b01a) sourceTreeRid est passé en paramètre et doit être géré (2024-06-28 23:00) <Daniel Caillibaud>  
`* `[513bb5c28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/513bb5c28) convertGraph doit accepter les graph vides (2024-06-28 23:00) <Daniel Caillibaud>  
`* `[06770a03d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06770a03d) 100 max pour un fetch sur la bibli (2024-06-28 23:00) <Daniel Caillibaud>  
`*   `[24af68870](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24af68870) Merge branch 'fixNodeContextMenu' into 'main' (2024-06-28 17:07) <Jean-Claude Lhote>  
`|\  `  
`| * `[eb00bc32b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb00bc32b) Fix node context menu (2024-06-28 17:07) <Jean-Claude Lhote>  
`|/  `  
`*   `[d8eeafe57](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8eeafe57) Merge branch 'notify_quand_erreur' into 'main' (2024-06-28 14:26) <Tommy Barroy>  
`|\  `  
`| * `[4392eeae9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4392eeae9) notify sur limite haute/basse (2024-06-28 14:25) <Tommy Barroy>  
`* |   `[6ebf30c73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ebf30c73) Merge branch 'fixAlerts' into 'main' (2024-06-28 14:23) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[20f26e599](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20f26e599) Suppression du notie sur suppression de noeud (2024-06-28 14:22) <Jean-claude Lhote>  
`* | | `[68524c1e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68524c1e7) Merge branch 'fixAlerts' into 'main' (2024-06-28 14:21) <Jean-Claude Lhote>  
`|\| | `  
`| |/  `  
`|/|   `  
`| * `[5f6a9890a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f6a9890a) Suppression du div de confirmation sur réponse négative à supprimer le noeud (2024-06-28 14:20) <Jean-claude Lhote>  
`|/  `  
`* `[5ec5ab461](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ec5ab461) la création du menu passe dans un SceneManager.create, ajout du bouton fullscreen (2024-06-28 09:57) <Daniel Caillibaud>  
`* `[b9d874cd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9d874cd0) Aj du bouton fullscreen dans l'éditeur (2024-06-28 09:57) <Daniel Caillibaud>  
`* `[31e28e893](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31e28e893) nettoyage des scopes (2024-06-28 09:57) <Daniel Caillibaud>  

## version 1.2.3

`* `[0721c938f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0721c938f) Merge branch 'fixTabVar' (2024-06-27 19:14) <Daniel Caillibaud>  
`* `[b8bf1b7cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8bf1b7cb) le div doit exister dans le DOM (2024-06-27 19:14) <Daniel Caillibaud>  
`* `[0dc009731](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0dc009731) faut pas détruire un div et le réutiliser ensuite (2024-06-27 19:14) <Daniel Caillibaud>  
`* `[c359fb959](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c359fb959) couleur => color et tailletexte => fontsize pour les styles prédéfinis (2024-06-27 19:09) <Daniel Caillibaud>  

## version 1.2.2

`* `[496053c85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/496053c85) nettoyage et simplification (2024-06-27 17:14) <Daniel Caillibaud>  
`* `[4fe581ba3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fe581ba3) modif message d'erreur (chargement mtg) (2024-06-27 17:14) <Daniel Caillibaud>  
`*   `[974226fb9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/974226fb9) Merge branch 'fixTestBlokMtg' into 'main' (2024-06-27 16:09) <Jean-Claude Lhote>  
`|\  `  
`| * `[29c3e3536](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/29c3e3536) blokmtg01 test, on ne fait plus de toggleCategory pour cette section. car je... (2024-06-27 16:09) <Jean-Claude Lhote>  
`|/  `  
`* `[037639904](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/037639904) blindage pour les navigateurs qui ne supportent pas le rendu WebGL alors que la section l'utilise (2024-06-27 15:47) <Daniel Caillibaud>  
`* `[5d5d44fd1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d5d44fd1) section echantillons virée (2024-06-27 15:47) <Daniel Caillibaud>  
`* `[093eff7f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/093eff7f1) modif messsage d'erreur quand la date de départ est dans le futur (2024-06-27 15:47) <Daniel Caillibaud>  
`*   `[56a93ca99](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/56a93ca99) Merge branch 'vireNumeroEssai' into 'main' (2024-06-27 13:52) <Daniel Caillibaud>  
`|\  `  
`| * `[fe4036991](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe4036991) simplification (getDonnees et autre code inutile viré) (2024-06-27 13:46) <Daniel Caillibaud>  
`| *   `[7dd2e04ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7dd2e04ff) Merge branch 'main' into vireNumeroEssai (2024-06-27 10:28) <Daniel Caillibaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[0bdb98d59](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0bdb98d59) Merge branch 'fixModaleEdition' into 'main' (2024-06-26 20:42) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[7fd9985a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7fd9985a9) refacto édition de node + refacto tooltip (simplification et ajout de flip() pour changer de coté quand ça rentre pas) (2024-06-26 20:37) <Daniel Caillibaud>  
`| * | `[a5d0d6ddd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5d0d6ddd) amélioration de la Modale pour l'édition des nœuds (2024-06-26 15:28) <Daniel Caillibaud>  
`| * | `[464d7d0f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/464d7d0f9) getCssDimension accepte inherit | unset | initial (2024-06-26 15:28) <Daniel Caillibaud>  
`|/ /  `  
`* | `[4568a2b5d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4568a2b5d) jsdoc et modif mineure msg (2024-06-26 15:27) <Daniel Caillibaud>  
`* |   `[f301d56a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f301d56a6) Merge branch 'ticket' into 'main' (2024-06-26 15:03) <Tommy Barroy>  
`|\ \  `  
`| * | `[7b970af75](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b970af75) [fix #294](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/294) (2024-06-26 15:01) <Tommy Barroy>  
`* | | `[3faf23710](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3faf23710) renommage plus clair des paramètres de conserve01 (2024-06-26 14:59) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[b425b7c86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b425b7c86) Merge branch 'fixTestSqueletteMG32Val' into 'main' (2024-06-25 17:37) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[40457cb40](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/40457cb40) L'apostrophe courbe a été remplacée par ' (2024-06-25 17:36) <Jean-claude Lhote>  
`|  /  `  
`* | `[a39fc6d94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a39fc6d94) Merge branch 'TraductionsDansTextes' into 'main' (2024-06-25 17:17) <Daniel Caillibaud>  
`* | `[001cca6f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/001cca6f4) Traductions dans textes (2024-06-25 17:17) <Jean-Claude Lhote>  
` /  `  
`* `[3fcbaa271](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3fcbaa271) numeroessai viré (on a déjà essaiCourant), avec ajout de FIXME (dernier essai imposé dès l'énoncé si encours = g) (2024-06-19 15:14) <Daniel Caillibaud>  
`* `[f35b50d02](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f35b50d02) numeroessai viré (on a déjà essaiCourant) (2024-06-19 15:11) <Daniel Caillibaud>  
`* `[f741b7727](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f741b7727) eslint fix (2024-06-19 15:07) <Daniel Caillibaud>  
`* `[be6bee634](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be6bee634) numeroessai viré (on a déjà essaiCourant) (2024-06-19 15:00) <Daniel Caillibaud>  
`* `[e0cb641ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e0cb641ef) sq.numeroessai => sq.numEssai (pour éviter toute confusion avec donneesSection.numeroessai voué à disparaître) (2024-06-19 14:27) <Daniel Caillibaud>  
`* `[9576a21a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9576a21a3) sq.numeroessai => sq.numEssai (pour éviter toute confusion avec donneesSection.numeroessai voué à disparaître) (2024-06-19 14:26) <Daniel Caillibaud>  
`* `[4aa80bab2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4aa80bab2) ménage de printemps (2024-06-19 14:21) <Daniel Caillibaud>  
`* `[309ada868](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/309ada868) ds.numeroessai => stor.numEssai (gestion particulière) (2024-06-19 14:13) <Daniel Caillibaud>  
`* `[86fcc62b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86fcc62b9) nettoyage (getDonnees et numeroessai virés) (2024-06-19 13:55) <Daniel Caillibaud>  
`* `[973996129](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/973996129) sq.numeroessai => sq.numEssai (pour éviter toute confusion avec donneesSection.numeroessai voué à disparaître) (2024-06-19 12:43) <Daniel Caillibaud>  
`* `[321917bb1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/321917bb1) sq doit être storage (Object) et pas stockage (Array) (2024-06-19 12:41) <Daniel Caillibaud>  

## version 1.2.1

`* `[cc0c19f34](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc0c19f34) nettoyage fraction03 (2024-06-25 16:35) <Daniel Caillibaud>  
`* `[e76459a16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e76459a16) fix délai en s (2024-06-25 16:29) <Daniel Caillibaud>  
`* `[728899d62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/728899d62) StylesJ3p.etendre() ne gère plus les propriétés exotiques taillepolice et couleur (uniquement fontSize et color), nettoyage également des appels de j3pAffiche (2024-06-25 16:04) <Daniel Caillibaud>  
`* `[afacf5430](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/afacf5430) fix touche entrée à la 2e question (et méthode desactiveReturn virée, elle ne faisait rien depuis très longtemps) (2024-06-25 11:18) <Daniel Caillibaud>  
`*   `[de3680a10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de3680a10) Merge branch 'tooltips' into 'main' (2024-06-24 16:05) <Jean-Claude Lhote>  
`|\  `  
`| * `[e5139855c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5139855c) Mise en place d'un système de substitution sur les labels du form du NodeEditor... nbSteps->nbÉtapes, maxTries->nbChances. (2024-06-24 16:04) <Jean-claude Lhote>  
`| * `[82162dc70](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/82162dc70) Modification tooltip MaxRuns (2024-06-24 15:50) <Jean-claude Lhote>  
`|/  `  
`*   `[5de957d7b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5de957d7b) Merge branch 'TheGrid' into 'main' (2024-06-24 15:32) <Jean-Claude Lhote>  
`|\  `  
`| * `[95bfa68b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95bfa68b4) Le déplacement des noeuds dans l'éditeur est lisse au pixel près. Mais le... (2024-06-24 15:32) <Jean-Claude Lhote>  
`|/  `  
`*   `[981b78dae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/981b78dae) Merge branch 'TitleVsTitre' into 'main' (2024-06-24 15:11) <Jean-Claude Lhote>  
`|\  `  
`| * `[5f30b50ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f30b50ef) titre devient title dans les GraphNode V2 et les SectionParameters V2. maxRuns... (2024-06-24 15:11) <Jean-Claude Lhote>  
`|/  `  
`*   `[2c0079208](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c0079208) Merge branch 'editprog_bug' into 'main' (2024-06-23 14:58) <Tommy Barroy>  
`|\  `  
`| * `[777ab5e4f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/777ab5e4f) appel de .cond qui n'existe pas tjrs (2024-06-23 14:57) <Tommy Barroy>  
`|/  `  
`*   `[1c0980691](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c0980691) Merge branch 'vire_les_noms_rpere' into 'main' (2024-06-22 10:51) <Tommy Barroy>  
`|\  `  
`| * `[be8065ef9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be8065ef9) vire les noms (2024-06-22 10:50) <Tommy Barroy>  
`* `[9bca2fb9e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9bca2fb9e) amélioration du typage dom & css (2024-06-21 20:40) <Daniel Caillibaud>  
`* `[bfa19644f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bfa19644f) mod label de debug (2024-06-21 18:15) <Daniel Caillibaud>  
`* `[b456f04b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b456f04b5) blindage correctArrayLignes (2024-06-21 17:56) <Daniel Caillibaud>  

## version 1.2.0

`* `[511320c16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/511320c16) mod scope (2024-06-21 17:34) <Daniel Caillibaud>  
`* `[6c45aadb0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c45aadb0) fix resize qui perdait ses styles (2024-06-21 17:34) <Daniel Caillibaud>  
`*   `[7cf38b28a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7cf38b28a) Merge branch 'reporte_error' into 'main' (2024-06-21 16:37) <Tommy Barroy>  
`|\  `  
`| * `[3d06b4de1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d06b4de1) reporte erreur (2024-06-21 16:36) <Tommy Barroy>  
`|/  `  
`* `[4cabe58cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4cabe58cd) fix détection de l'url de l'image pour pcloud (2024-06-21 16:01) <Daniel Caillibaud>  
`*   `[ff8b67fc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff8b67fc4) Merge branch 'noPeRefToEditor' into 'main' (2024-06-21 12:27) <Jean-Claude Lhote>  
`|\  `  
`| * `[551d96805](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/551d96805) n'affiche pas le bouton radio si il n'y a pas de phrases d'état. (2024-06-21 12:24) <Jean-claude Lhote>  
`|/  `  
`*   `[d9ba13439](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9ba13439) Merge branch 'modaleOpaque' into 'main' (2024-06-21 12:06) <Jean-Claude Lhote>  
`|\  `  
`| * `[77009d37b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77009d37b) Passe l'opacité de la modale à 1 au lieu de 0.8 (sont container est toujours à 0.5 de noir). (2024-06-21 12:06) <Jean-claude Lhote>  
`|/  `  
`* `[fb41ccac9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb41ccac9) rectifications modifs trop laxistes (2024-06-21 12:00) <Daniel Caillibaud>  
`* `[a8a53fd8f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8a53fd8f) blindage afficheMathliveDans lorsque ch n'est pas une string (2024-06-21 11:22) <Daniel Caillibaud>  
`* `[4d38459b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d38459b8) Aj de FIXME (2024-06-21 10:55) <Daniel Caillibaud>  
`* `[b1d293bc1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1d293bc1) Repere accepte des objets sans nom (il en génère alors un) (2024-06-21 10:55) <Daniel Caillibaud>  
`* `[22597ffb6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22597ffb6) nettoyage (getDonnees viré, ds.TypeValApp viré car doublon de ds.ApproximationDecimale) (2024-06-21 10:55) <Daniel Caillibaud>  
`* `[dd5e55e99](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd5e55e99) moins de log au chargement (2024-06-21 10:55) <Daniel Caillibaud>  
`*   `[4861883bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4861883bc) Merge branch 'dropNoeudPropre' into 'main' (2024-06-21 09:58) <Jean-Claude Lhote>  
`|\  `  
`| * `[b496c1796](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b496c1796) fixe le problème du 'saut' des noeuds après le drop. (2024-06-21 09:54) <Jean-claude Lhote>  
`|/  `  
`*   `[9cdb40098](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9cdb40098) Merge branch 'pour_squizz_j3pConteneur' into 'main' (2024-06-20 20:46) <Tommy Barroy>  
`|\  `  
`| * `[42b5ab9fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42b5ab9fc) pour inventer parent (2024-06-20 20:44) <Tommy Barroy>  
`* |   `[5386e3b9b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5386e3b9b) Merge branch 'pour_stat' into 'main' (2024-06-20 20:45) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[3ee449ab4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ee449ab4) stat e liste (2024-06-20 20:38) <Tommy Barroy>  
`|/  `  
`*   `[e43b9e8e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e43b9e8e8) Merge branch 'bb2' into 'main' (2024-06-20 12:59) <Tommy Barroy>  
`|\  `  
`| * `[138cdf68a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/138cdf68a) pb sur premsavec (2024-06-20 12:57) <Tommy Barroy>  
`|/  `  
`*   `[91f2664a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/91f2664a0) Merge branch 'oubli_correction_en_tps_limite' into 'main' (2024-06-20 12:20) <Tommy Barroy>  
`|\  `  
`| * `[50ada5be0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50ada5be0) oubli co en temps lmite (2024-06-20 12:19) <Tommy Barroy>  
`|/  `  
`* `[54188a9f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54188a9f0) grille de 20×10 (2024-06-20 00:53) <Daniel Caillibaud>  
`* `[d38c920f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d38c920f6) aj de mots au dico (2024-06-19 16:36) <Daniel Caillibaud>  
`* `[2cf92e5f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2cf92e5f7) modifs de forme (2024-06-19 16:30) <Daniel Caillibaud>  
`* `[287b462b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/287b462b5) blindage (fix bugsnag 667282d21de2660008a84ccf) (2024-06-19 16:05) <Daniel Caillibaud>  
`* `[47b980082](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47b980082) amélioration typage (2024-06-19 15:53) <Daniel Caillibaud>  
`* `[dd12de75f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd12de75f) getFirstParent retourne un Element s'il retourne qqchose (et pas un Node), fix erreur bugsnag 6671f7fcb6fc9e0008290489 (2024-06-19 15:52) <Daniel Caillibaud>  
`* `[1371520ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1371520ad) nettoyages (getDonnees viré, avec une tonne de textes qui n'étaient jamais utilisés) (2024-06-19 15:21) <Daniel Caillibaud>  
`*   `[d61d23cd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d61d23cd0) Merge branch 'noGrille' into 'main' (2024-06-18 19:26) <Jean-Claude Lhote>  
`|\  `  
`| * `[b405052a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b405052a3) Je change le pas de la grille à (1;1) pour voir... (2024-06-18 19:24) <Jean-claude Lhote>  
`|/  `  
`*   `[c81b80565](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c81b80565) Merge branch 'pesListWithOnlyOnePe' into 'main' (2024-06-18 18:40) <Jean-Claude Lhote>  
`|\  `  
`| * `[8f246f5e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f246f5e9) Si une seule pe est sélectionnée, on ne doit pas faire de join('<br>-') (2024-06-18 18:39) <Jean-claude Lhote>  
`|/  `  
`* `[bd7357b8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd7357b8b) build.prod et pas build.old (2024-06-18 17:28) <Daniel Caillibaud>  
`* `[dd61716f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd61716f3) docroot => public (pour régler les pbs de fichiers statiques au start) (2024-06-18 17:28) <Daniel Caillibaud>  
`*   `[e4a0789da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4a0789da) Merge branch 'fixEvaluationsNonConformes' into 'main' (2024-06-18 16:48) <Jean-Claude Lhote>  
`|\  `  
`| * `[9543a9280](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9543a9280) ne pas mettre evaluation: '' quand c'est pas dans les parametres (2024-06-18 16:46) <Jean-claude Lhote>  
`|/  `  
`*   `[0c7034d56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c7034d56) Merge branch 'utilise_net_ts' into 'main' (2024-06-18 16:32) <Tommy Barroy>  
`|\  `  
`| * `[065228a6b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/065228a6b) ajoute pcloud mais Daniel va rigoler, utilise net.ts (2024-06-18 16:31) <Tommy Barroy>  
`* | `[3ab615da6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ab615da6) modif de forme (2024-06-18 16:31) <Daniel Caillibaud>  
`* | `[b666a2a79](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b666a2a79) fix passage au nœud suivant (2024-06-18 16:22) <Daniel Caillibaud>  
`* | `[86d5cac22](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86d5cac22) simplification split (2024-06-18 16:22) <Daniel Caillibaud>  
`* |   `[6afb10a6c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6afb10a6c) Merge branch 'modBtnSuppr' into 'main' (2024-06-18 16:04) <Jean-Claude Lhote>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[985c2579b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/985c2579b) Suppression de code devenu inutile (2024-06-18 16:02) <Jean-claude Lhote>  
`| * `[eecfc2fdc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eecfc2fdc) rectifications : Le bouton ajouter un élément reprend sa place à la fin. Le bouton retirer un élément fonctionne à nouveau normalement et les numéros sont dans l'ordre (2024-06-18 16:02) <Jean-claude Lhote>  
`| * `[9197ee613](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9197ee613) WIP fix code dupliqué et simplification (reste à vérifier la gestion des br) (2024-06-18 16:02) <Daniel Caillibaud>  
`|/  `  
`* `[4c1509a31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c1509a31) on affiche le titre que s'il y en a un (2024-06-18 14:32) <Daniel Caillibaud>  
`* `[a6c2257fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6c2257fc) fix split du titre (2024-06-18 14:25) <Daniel Caillibaud>  
`* `[2127aa64f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2127aa64f) fix typo (2024-06-18 13:17) <Daniel Caillibaud>  
`* `[ca650ff1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca650ff1a) fix code dupliqué (et tous les pbs signalés par webstorm sur ce fichier) (2024-06-18 12:42) <Daniel Caillibaud>  
`* `[181a1bc1f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/181a1bc1f) fix délimiteur ' (au lieu de ") suite au changement ' => ’ précédent (2024-06-18 12:22) <Daniel Caillibaud>  
`* `[0a97341ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a97341ec) ' => ’ dans les phrases (2024-06-18 12:22) <Daniel Caillibaud>  
`*   `[ce67c7663](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce67c7663) Merge branch 'Correction_Enonce_Ex_Sixieme' into 'main' (2024-06-18 12:15) <Yves Biton>  
`|\  `  
`| * `[098ba3f82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/098ba3f82) Correction : il y a avait triangle au lieu de point dans l'énonce [fix #293](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/293) (2024-06-18 12:14) <Yves Biton>  
`|/  `  
`*   `[ce279e7d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce279e7d2) Merge branch 'fixSectionSuivante' into 'main' (2024-06-18 12:05) <Daniel Caillibaud>  
`|\  `  
`| * `[29ab7230b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/29ab7230b) getDelaySec ne plante plus mais signale le pb en console si le délai est > 10_000 secondes (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[c7ea040d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7ea040d5) fix getDelaySec (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[9632c8f33](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9632c8f33) spPlay gère aussi une éventuelle callback de chargement (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[6565dbbd7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6565dbbd7) spPlay retourne une promesse (de chargement) (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[b84630b17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b84630b17) resultatListener => resultatCallback (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[75d83e521](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75d83e521) fix des appels de sectionSuivante() dans moteur v2 (y'a des sections v1 qui le font) (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[f55d1f59f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f55d1f59f) fix hook commit-msg (si le message contient "fix" et un chiffre plus loin") (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[55abaad42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55abaad42) modif hook pre-commit (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[b3f4842ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3f4842ea) suite typage Parcours.ts (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[781cef744](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/781cef744) j3pIsInteger viré (remplacé par Number.isInteger) (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[ba5e8b626](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba5e8b626) Draft Parcours.js => Parcours.ts (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[b80d79e07](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b80d79e07) déclaration d’outils inutilisés virée (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[042793a52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/042793a52) modifs de forme (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[9879ef9a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9879ef9a1) parcours.donnees viré (parcours.donnees.resultatCallback => parcours.resultatCallback & parcours.donnees.editgraphes => parcours.editgraphes) (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[248a9870f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/248a9870f) Fix typage du paramètre donneesPrecedentes (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[ce1df7acf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce1df7acf) parcours._parametres viré (code mort) (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[98c9cb463](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98c9cb463) Parcours.parcours.donnees et Parcours.SectionTruc.donnees_parcours virés (code mort) (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[2a7e1ba33](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a7e1ba33) Parcours.ajouteEtape viré (code mort) (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[4128789a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4128789a6) Parcours.getLastScore viré (code mort) (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[cbf3ccf72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cbf3ccf72) Parcours.getNbPassages viré (code mort) (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[45b6cff1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45b6cff1a) contrôle de la validité de l'évaluation retournée par un check (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[1eb09a584](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1eb09a584) code mort viré dans le case navigation (il en reste bcp) (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[3312c6a07](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3312c6a07) fix prop pe foireuse (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[e6067bee8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e6067bee8) code mort viré (affectation de pe avec un number) (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[a799dd3a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a799dd3a0) code mort viré (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[1f57fd6c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f57fd6c9) outil sokoban plus déclaré dans les sections v1 (2024-06-18 11:56) <Daniel Caillibaud>  
`| * `[96aa8e0a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/96aa8e0a0) Aj getDelayMs et getDelaySec (et renommages, startedAt pour les dates et startedAtMs pour les timestamps) (2024-06-18 11:56) <Daniel Caillibaud>  
`|/  `  
`* `[438afa32c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/438afa32c) setOverlayLabels devient sync (2024-06-18 11:55) <Daniel Caillibaud>  
`* `[092518ddf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/092518ddf) simplification getCondition (et amélioration message d'erreur) (2024-06-18 11:55) <Daniel Caillibaud>  
`* `[77712348d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77712348d) fix pesList (il faut retourner les valeurs des pe dans l'ordre des peRefs demandée) (2024-06-18 11:34) <Daniel Caillibaud>  
`* `[48b90e989](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48b90e989) pas de length sur un object (2024-06-18 11:27) <Jean-claude Lhote>  
`*   `[16d3d208d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16d3d208d) Merge branch 'ticket_trim_et_expli_valide' into 'main' (2024-06-17 22:46) <Tommy Barroy>  
`|\  `  
`| * `[16b9f3ce1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16b9f3ce1) [fix #278](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/278) (2024-06-17 22:41) <Tommy Barroy>  
`|/  `  
`*   `[516947d1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/516947d1a) Merge branch 'angle_hors_cadre' into 'main' (2024-06-17 21:45) <Tommy Barroy>  
`|\  `  
`| * `[9d28ba235](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d28ba235) [fix #231](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/231) (2024-06-17 21:44) <Tommy Barroy>  
`* |   `[ee77f1b1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee77f1b1a) Merge branch 'mathliveCss' into 'main' (2024-06-17 17:09) <Jean-Claude Lhote>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[a79c85919](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a79c85919) uniformise la largeur des math-fields avec la V1. suppression de mathlive.scss la déclaration va dans playground.scss (2024-06-17 17:08) <Jean-claude Lhote>  
`|/  `  
`*   `[048d0f10a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/048d0f10a) Merge branch 'co_complete01' into 'main' (2024-06-16 23:22) <Tommy Barroy>  
`|\  `  
`| * `[c3bf0415c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c3bf0415c) [fix #212](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/212) (2024-06-16 23:20) <Tommy Barroy>  
`|/  `  
`*   `[fade59d59](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fade59d59) Merge branch 'chiffre_et_nombre' into 'main' (2024-06-16 22:50) <Tommy Barroy>  
`|\  `  
`| * `[72ecdd9f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72ecdd9f9) chiffres et pas nombres par 3 (2024-06-16 22:48) <Tommy Barroy>  
`|/  `  
`*   `[41410f7a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41410f7a0) Merge branch 'essaie_de_mettre_des_nombres_pas_trop_halucinant' into 'main' (2024-06-16 22:25) <Tommy Barroy>  
`|\  `  
`| * `[df110bda1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df110bda1) essai de mettre des nombres plus réalistes (2024-06-16 22:23) <Tommy Barroy>  
`* | `[1edaddd68](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1edaddd68) docroot.tmp exclus des scopes (2024-06-16 16:42) <Daniel Caillibaud>  
`* | `[94ab43345](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94ab43345) fix pb de fichiers statiques au pnpm start (2024-06-16 14:48) <Daniel Caillibaud>  
`* |   `[193dc07a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/193dc07a3) Merge branch 'pb750' into 'main' (2024-06-16 10:43) <Rémi Deniaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[94dd9b9dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94dd9b9dd) correction pb section 750 sur la gestion de e^{} à la validation (2024-06-16 10:40) <Rémi Deniaud>  
`|/  `  
`*   `[e8fb72b74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8fb72b74) Merge branch 'inclus_google_drive' into 'main' (2024-06-14 14:49) <Tommy Barroy>  
`|\  `  
`| * `[7008c0093](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7008c0093) iclus google drive (2024-06-14 14:47) <Tommy Barroy>  
`* | `[de2fc0ff9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de2fc0ff9) fix typo dans msg et commentaires (2024-06-14 13:17) <Daniel Caillibaud>  
`* | `[f3572205a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3572205a) checkImgUrl retourne toujours une string (2024-06-14 13:08) <Daniel Caillibaud>  
`* | `[0b6046563](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b6046563) WIP aj d'un brouillon pour rechercher l'url d'une image dans une page web (2024-06-14 13:03) <Daniel Caillibaud>  
`* |   `[a9a3f764e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9a3f764e) Merge branch 'focuses' into 'main' (2024-06-14 11:50) <Jean-Claude Lhote>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[d5466a9ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5466a9ba) mettre le focus sur le premier placeholder d'un math-field 'fillInTheBlank'... (2024-06-14 11:50) <Jean-Claude Lhote>  
`|/  `  
`*   `[7cb43bcb9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7cb43bcb9) Merge branch 'ajoute_un_autre_axe' into 'main' (2024-06-14 11:00) <Tommy Barroy>  
`|\  `  
`| * `[c2ab1b8ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2ab1b8ea) ajoute un axe fixe plus joli dans mohascratch (2024-06-14 10:59) <Tommy Barroy>  
`| * `[897a53dee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/897a53dee) ajoute un axe fixe plus joli dans mohascratch (2024-06-14 10:58) <Tommy Barroy>  
`|/  `  
`*   `[0d6b9f951](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d6b9f951) Merge branch 'moins_de_depot' into 'main' (2024-06-14 09:55) <Tommy Barroy>  
`|\  `  
`| * `[e4fa665fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4fa665fc) ajoute choix serveur perso (2024-06-14 09:53) <Tommy Barroy>  
`| * `[98c72b407](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98c72b407) propose que les deux depots que je sais utiliser (2024-06-14 09:44) <Tommy Barroy>  
`|/  `  
`*   `[b51e378e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b51e378e9) Merge branch 'amelio_choix_exo_quand_plusieurs_niveau_et_agrandit_taille_possible_énnce_dans_blokmoha' into 'main' (2024-06-14 08:35) <Tommy Barroy>  
`|\  `  
`| *   `[ef86dece7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef86dece7) Merge branch 'main' into 'amelio_choix_exo_quand_plusieurs_niveau_et_agrandit_taille_possible_énnce_dans_blokmoha' (2024-06-14 08:34) <Tommy Barroy>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[d2ee873d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2ee873d2) Merge branch 'BrancheRebasee' into 'main' (2024-06-13 21:09) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[c53077ff0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c53077ff0) sectionExemple4 itemStart ajouté dans question 1 pour vérifier le setFocus() des tableaux Mathlive. (2024-06-13 21:07) <Jean-claude Lhote>  
`| * | `[c82683c2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c82683c2d) Ajout d'une fonction pour trouver le Latex dans les tables... et utilisation dans display() pour modifier latexContent (2024-06-13 21:07) <Jean-claude Lhote>  
`| * | `[63bd96b27](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63bd96b27) rebase et fixe conflits (2024-06-13 21:07) <Jean-claude Lhote>  
`| * | `[e6475b4c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e6475b4c6) ajout de la propriété element sur DomElt + setFocus() (2024-06-13 21:07) <Jean-claude Lhote>  
`| * | `[66d287611](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66d287611) Les tooltips s'affichent correctement. (2024-06-13 20:57) <Jean-claude Lhote>  
`| * | `[148f44162](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/148f44162) à priori plus de todo dans SceneUI, ça semble au point (2024-06-13 20:52) <Jean-claude Lhote>  
`|/ /  `  
`* | `[5b5d1342b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b5d1342b) amélioration du typage ace (2024-06-13 20:04) <Daniel Caillibaud>  
`* | `[ec34e30da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec34e30da) burger menu plus visible dans le coin des nœuds (dans l’éditeur) (2024-06-13 19:52) <Daniel Caillibaud>  
`* | `[63be79361](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63be79361) apostrophe ' => ’ (dans une phrase) (2024-06-13 19:52) <Daniel Caillibaud>  
`* | `[3b7195b38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b7195b38) modifs de forme (2024-06-13 19:02) <Daniel Caillibaud>  
`* | `[a02a4aad7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a02a4aad7) Player.ts:getLabelMinutesSecondes => number.ts:getMinutesSecondesLabel (2024-06-13 19:01) <Daniel Caillibaud>  
`* | `[ce67fe13e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce67fe13e) modif ordre imports (2024-06-13 18:41) <Daniel Caillibaud>  
`* | `[2a3e680e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a3e680e2) fix dialog() : option withCancel virée (pas de sens sur une Modale et jamais utilisé), title utilisé même avec un message vide et promesse résolue même si on ne trouve pas le bouton de fermeture de la modale. (2024-06-13 18:39) <Daniel Caillibaud>  
`* | `[999dc7fa0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/999dc7fa0) fix Modal (qui pouvait ne jamais avoir de bouton de fermeture), avec param noClose supprimé et param closeWithOk ajouté (2024-06-13 18:36) <Daniel Caillibaud>  
`* | `[ba1967248](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba1967248) console.error pour debug viré ! (2024-06-13 17:42) <Daniel Caillibaud>  
`* | `[4fcebb7f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fcebb7f8) Aj du todo pour la comparaison de graphe à l'init de Pathway (il faudra mettre le graphe sous forme d'un hash par node) (2024-06-13 17:24) <Daniel Caillibaud>  
`* |   `[e9df4b329](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9df4b329) Merge branch 'refactoLimiteV2' into 'main' (2024-06-13 16:59) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[f51a2a4a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f51a2a4a7) nouvelle syntaxe chai v5 (2024-06-13 16:57) <Daniel Caillibaud>  
`| * | `[bac42d09d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bac42d09d) refacto gestion de la limite de temps en v2 (2024-06-13 16:56) <Daniel Caillibaud>  
`| * | `[472498373](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/472498373) optimisation display (un seul renderMath par display) (2024-06-13 16:25) <Daniel Caillibaud>  
`| * | `[8cb415279](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cb415279) Précision du type de SectionContext.params (2024-06-13 16:25) <Daniel Caillibaud>  
`| * | `[dba558d6c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dba558d6c) Fix spelling (2024-06-13 16:25) <Daniel Caillibaud>  
`| * | `[5cbfa549f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5cbfa549f) Aj du type LegacyParcoursProp (2024-06-13 16:24) <Daniel Caillibaud>  
`| * | `[f95fa4c13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f95fa4c13) Fix tous les pbs remontés par webstorm (typo, typage, etc.) (2024-06-13 16:24) <Daniel Caillibaud>  
`| * | `[aa74c19aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa74c19aa) Ajout des dicos pour ne plus avoir de typo|mispelling warnings sur le français dans les commentaires ou les noms de variable (2024-06-13 16:24) <Daniel Caillibaud>  
`| * | `[442022a9f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/442022a9f) màj deps (2024-06-13 16:24) <Daniel Caillibaud>  
`* | |   `[c7159ed61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7159ed61) Merge branch 'TesterUnNoeud' into 'main' (2024-06-13 13:53) <Jean-Claude Lhote>  
`|\ \ \  `  
`| * | | `[b662010cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b662010cb) Mise en place de testRessource dans l'éditeur (2024-06-13 13:52) <Jean-claude Lhote>  
`| | | * `[da389471a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da389471a) taille enonce et choix exo (2024-06-14 08:20) <Tommy Barroy>  
`| |_|/  `  
`|/| |   `  
`* | |   `[18a279b2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/18a279b2f) Merge branch 'retente_editeur_progconstruction2updated' into 'main' (2024-06-12 23:35) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * |   `[4cc8ea924](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4cc8ea924) Merge branch 'main' into retente_editeur_progconstruction2updated (2024-06-12 22:57) <Tommy Barroy>  
`| |\ \  `  
`| * | | `[107419ca9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/107419ca9) fait pareil avec son pour url (2024-06-12 22:52) <Tommy Barroy>  
`| * | | `[9287c3565](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9287c3565) message dans éditeur pour la vieilles ressources pas éditables avec lui (2024-06-12 15:03) <Tommy Barroy>  
`| * | | `[811c1f6c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/811c1f6c5) cache aide à la correction et descend dans le div pour voir la correction (2024-06-12 07:46) <Tommy Barroy>  
`| * | | `[39aaec598](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39aaec598) déplace la liste des cloud pour l'utiliser dans son et video (2024-06-12 06:48) <Tommy Barroy>  
`| * | | `[88ed8d777](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88ed8d777) détache la fenetre (2024-06-12 06:45) <Tommy Barroy>  
`| * | | `[548290893](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/548290893) oblige de passer par URL (2024-06-11 06:34) <Tommy Barroy>  
`| * | | `[64aa4a870](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64aa4a870) en cours (2024-06-05 22:39) <Tommy Barroy>  
`| * | |   `[ae9556a63](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae9556a63) Merge branch 'main' into retente_editeur_progconstruction2 (2024-06-04 16:54) <Daniel Caillibaud>  
`| |\ \ \  `  
`| | * \ \   `[f0a773994](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0a773994) Merge branch 'fixScratch' into 'main' (2024-06-04 16:02) <Tommy Barroy>  
`| | |\ \ \  `  
`| | | * | | `[cdb31e7f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cdb31e7f3) fixe le test cassé sur blokfigure en remplaçant l'event 'click' par l'event 'mousedown' (bouton scratchReset) (2024-06-04 15:09) <Jean-claude Lhote>  
`| | |/ / /  `  
`| * | | | `[d2e8fe779](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2e8fe779) tyztr (2024-05-28 15:54) <Tommy Barroy>  
`| * | | | `[b8de01b8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8de01b8c) tyztr (2024-05-27 22:01) <Tommy Barroy>  
`| * | | | `[cb0c96c0a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb0c96c0a) liminA peut etre undefined (2024-05-20 21:06) <Tommy Barroy>  
`| * | | | `[4b762572a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b762572a) liminA peut etre undefined (2024-05-11 14:52) <Tommy Barroy>  
`| * | | | `[1ef1b0442](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ef1b0442) liminA peut etre undefined (2024-05-10 14:56) <Tommy Barroy>  
`| * | | | `[b447a1c95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b447a1c95) liminA peut etre undefined (2024-05-08 22:50) <Tommy Barroy>  
`| * | | | `[da815ff5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da815ff5a) liminA peut etre undefined (2024-05-08 22:49) <Tommy Barroy>  
`| * | | | `[f9eb715a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f9eb715a8) co deb en cours (2024-05-07 13:14) <Tommy Barroy>  
`| * | | | `[98cca9ee2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98cca9ee2) co deb en cours (2024-05-07 12:13) <Tommy Barroy>  
`| * | | | `[7e82bef31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e82bef31) co deb en cours (2024-05-07 09:34) <Tommy Barroy>  
`| * | | | `[c7573a47a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7573a47a) co deb en cours (2024-05-06 14:04) <Tommy Barroy>  
`| * | | | `[f75c29ac3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f75c29ac3) co deb en cours (2024-05-06 13:17) <Tommy Barroy>  
`| * | | | `[34b1e2c36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34b1e2c36) co deb en cours (2024-05-02 14:29) <Tommy Barroy>  
`| * | | | `[425e2d2d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/425e2d2d5) co deb en cours (2024-04-30 02:31) <Tommy Barroy>  
`| * | | | `[4c7e5d7cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c7e5d7cf) co deb en cours (2024-04-29 23:00) <Tommy Barroy>  
`| * | | | `[2546eddc0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2546eddc0) co deb en cours (2024-04-29 20:57) <Tommy Barroy>  
`| * | | | `[00ab02046](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00ab02046) co deb en cours (2024-04-26 21:29) <Tommy Barroy>  
`| * | | | `[ef41bc753](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef41bc753) co deb en cours (2024-04-26 20:09) <Tommy Barroy>  
`| * | | | `[995838011](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/995838011) co deb en cours (2024-04-26 15:55) <Tommy Barroy>  
`| * | | | `[b893da861](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b893da861) co deb en cours (2024-04-25 22:24) <Tommy Barroy>  
`| * | | | `[1a5339240](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a5339240) co deb en cours (2024-04-25 21:21) <Tommy Barroy>  
`| * | | | `[2114e91eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2114e91eb) co deb en cours (2024-04-24 23:44) <Tommy Barroy>  
`| * | | | `[79e280563](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79e280563) co deb en cours (2024-04-24 21:59) <Tommy Barroy>  
`| * | | | `[94552085d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94552085d) co deb en cours (2024-04-23 14:56) <Tommy Barroy>  
`| * | | | `[a700ad7fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a700ad7fa) co deb en cours (2024-04-22 23:00) <Tommy Barroy>  
`| * | | | `[6f6e02065](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f6e02065) co deb en cours (2024-04-22 22:45) <Tommy Barroy>  
`| * | | | `[dee51ec2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dee51ec2f) co deb en cours (2024-04-22 22:33) <Tommy Barroy>  
`| * | | | `[4258948c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4258948c0) co deb en cours (2024-04-22 15:20) <Tommy Barroy>  
`| * | | | `[35f58a6ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35f58a6ba) co deb en cours (2024-04-22 10:08) <Tommy Barroy>  
`| * | | | `[90607b4a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90607b4a1) co deb en cours (2024-04-21 21:44) <Tommy Barroy>  
`| * | | | `[314fc8066](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/314fc8066) co deb en cours (2024-04-21 16:23) <Tommy Barroy>  
`| * | | | `[c3bf83178](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c3bf83178) co deb en cours (2024-04-21 10:30) <Tommy Barroy>  
`| * | | | `[8fc709ea2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8fc709ea2) co deb en cours (2024-04-21 09:56) <Tommy Barroy>  
`| * | | | `[e5b9f8193](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5b9f8193) co deb en cours (2024-04-20 22:04) <Tommy Barroy>  
`| * | | | `[3ae966740](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ae966740) co deb en cours (2024-04-20 21:33) <Tommy Barroy>  
`| * | | | `[ca4e7c530](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca4e7c530) co deb en cours (2024-04-20 11:10) <Tommy Barroy>  
`| * | | | `[f2dc1389a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2dc1389a) co deb en cours (2024-04-20 10:42) <Tommy Barroy>  
`| * | | | `[1ab398ec3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ab398ec3) co deb en cours (2024-04-19 17:53) <Tommy Barroy>  
`| * | | | `[d7ca7c3d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7ca7c3d4) co deb en cours (2024-04-18 21:27) <Tommy Barroy>  
`| * | | | `[8c10fad42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c10fad42) co deb en cours (2024-04-18 14:04) <Tommy Barroy>  
`| * | | | `[c07cbe8e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c07cbe8e9) co deb en cours (2024-04-17 20:30) <Tommy Barroy>  
`| * | | | `[c00b272b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c00b272b4) co deb en cours (2024-04-17 01:10) <Tommy Barroy>  
`| * | | | `[685349cb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/685349cb8) co deb en cours (2024-04-14 21:47) <Tommy Barroy>  
`| * | | | `[105799432](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/105799432) co deb en cours (2024-04-13 16:37) <Tommy Barroy>  
`| * | | | `[2903c9fd4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2903c9fd4) il manquait des cas dans aléatoire (2024-04-10 20:20) <Tommy Barroy>  
`| * | | | `[92ec703a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/92ec703a8) il manquait des cas dans aléatoire (2024-04-10 16:10) <Tommy Barroy>  
`| * | | | `[dbb46990c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dbb46990c) Revert "bugsnag" (2024-04-09 12:30) <Tommy Barroy>  
`| * | | | `[62bf2a472](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/62bf2a472) bugsnag (2024-04-09 09:57) <Tommy Barroy>  
`| * | | | `[44b66fbf9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44b66fbf9) bugsnag (2024-04-09 09:57) <Tommy Barroy>  
`| * | | | `[c666da5b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c666da5b9) bugsnag (2024-04-09 09:57) <Tommy Barroy>  
`| * | | | `[17b984d5e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17b984d5e) test pour ticket sur ipad (2024-04-09 09:57) <Tommy Barroy>  
`| * | | | `[8886974bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8886974bb) bugsnag somme angle (2024-04-09 09:57) <Tommy Barroy>  
`| * | | | `[e423c00a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e423c00a1) bugsnag somme angle (2024-04-09 09:55) <Tommy Barroy>  
`| * | | | `[35ee01573](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35ee01573) bugsnag somme angle (2024-04-09 09:55) <Tommy Barroy>  
`| * | | | `[0dbbd535a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0dbbd535a) bugsnag somme angle (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[58507d857](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58507d857) bugsnag somme angle (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[cf0762f8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf0762f8c) bugsnag somme angle (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[adee10716](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/adee10716) bugsnag somme angle (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[4d48b3a76](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d48b3a76) bugsnag somme angle (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[f6b830b1b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6b830b1b) bugsnag somme angle (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[929753e2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/929753e2f) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[0992075d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0992075d2) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[6b6a3e10a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b6a3e10a) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[90ae7db1d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90ae7db1d) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[c4e798d8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4e798d8b) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[26551f14a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/26551f14a) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[490e0a7d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/490e0a7d2) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[a6452e72c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6452e72c) vire anime (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[4bf89b8dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4bf89b8dc) vire anime (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[dfe78f19e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfe78f19e) vire anime (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[1bdbd9d4d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1bdbd9d4d) remis un bloc enlevé (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[8243494d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8243494d0) remis un bloc enlevé (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[10d41f9d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10d41f9d3) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[0ffc57210](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ffc57210) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[f59b770de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f59b770de) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[69cec2f2a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69cec2f2a) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[d2633eb74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2633eb74) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[283bffbb4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/283bffbb4) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[a86dd0b75](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a86dd0b75) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[abe0168d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/abe0168d9) bugsnag (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[93c8ffb10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93c8ffb10) remet variables deans editeur (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[834e805cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/834e805cc) remet variables deans editeur (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[7410bd287](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7410bd287) [fix #239](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/239) (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[2599d3ae7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2599d3ae7) ya reponse fonctionnait 1 fois sur 4 (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[f99d7b48f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f99d7b48f) ya reponse fonctionnait 1 fois sur 4 (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[86a788ebe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86a788ebe) ya reponse fonctionnait 1 fois sur 4 (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[20f9bcac6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20f9bcac6) enregistre pas 2 fois les trucs lourds en double dans la dernière. Reste plus qu'à compresser/décompresser et trouver une solution simple pour hebergement depuis éditeur (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[3a60a9db2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a60a9db2) [fix #237](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/237) (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[cb35de7f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb35de7f3) [fix #237](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/237) (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[0f090b8af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f090b8af) pour sauv (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[440a96e96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/440a96e96) pour sauv (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[7ad01ee71](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ad01ee71) sauv (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[d04545ae7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d04545ae7) pour sauv (2024-04-09 09:47) <Tommy Barroy>  
`| * | | | `[108a0b328](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/108a0b328) vire donnees et chnge place bouton (2024-04-09 09:33) <Tommy Barroy>  
`| * | | | `[82b004a29](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/82b004a29) pour sauv (2024-04-09 09:33) <Tommy Barroy>  
`| * | | | `[6341e5004](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6341e5004) pour sauv (2024-04-09 09:33) <Tommy Barroy>  
`| * | | | `[604956ede](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/604956ede) pour sauv (2024-04-09 09:33) <Tommy Barroy>  
`| * | | | `[775d5cb8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/775d5cb8c) pour sauv (2024-04-09 09:33) <Tommy>  
`* | | | |   `[16a0fd95f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16a0fd95f) Merge branch 'fixTodo2' into 'main' (2024-06-12 20:51) <Jean-Claude Lhote>  
`|\ \ \ \ \  `  
`| |_|_|_|/  `  
`|/| | | |   `  
`| * | | | `[17abfa1bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17abfa1bf) fixe l'éditeur : permet de changer label du noeud et titre de la section (2024-06-12 20:49) <Jean-claude Lhote>  
`|/ / / /  `  
`* | | |   `[84e59c342](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84e59c342) Merge branch 'fixTodo' into 'main' (2024-06-11 20:35) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| * | | | `[0ee8a07b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ee8a07b9) retrait de todo + fixe deux oublis dans convertParameters (2024-06-11 20:34) <Jean-claude Lhote>  
`|/ / / /  `  
`* | | | `[2a1ec92cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a1ec92cb) précision sur un todo (2024-06-11 19:59) <Daniel Caillibaud>  
`* | | | `[b3e2d1ed6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3e2d1ed6) Aj d'une option kbRestriction pour gérer les cas où restriction n'est pas une RegExp (2024-06-11 19:34) <Daniel Caillibaud>  
`* | | |   `[a1d75c777](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1d75c777) Merge branch 'fixVirtualKeyboard' into 'main' (2024-06-11 17:53) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[1012e71f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1012e71f1) On remplace qq isDomElement par isHtmlElement (pour avoir aussi la présence dans le DOM) (2024-06-11 17:39) <Daniel Caillibaud>  
`| * | | | `[ca84acf85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca84acf85) Fix VirtualKeyboard (marchait pas si quand il y avait une section précédente qui l'avait déjà instancié). [fix #290](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/290) (2024-06-11 17:26) <Daniel Caillibaud>  
`| * | | | `[d7610e547](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7610e547) isHtmlElement et isSvgElement vérifient aussi que l'élément est dans le DOM (2024-06-11 17:23) <Daniel Caillibaud>  
`| * | | | `[f0e9c322d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0e9c322d) Ajout des options mustBeInDom et warnIfNotInDom à isDomElement (2024-06-11 17:21) <Daniel Caillibaud>  
`* | | | |   `[d0f874e4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0f874e4b) Merge branch 'Correction_Squelette_Somme_Frac' into 'main' (2024-06-11 12:22) <Yves Biton>  
`|\ \ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`| * | | | `[57a0fba91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57a0fba91) Correction mineure (2024-06-11 12:20) <Yves Biton>  
`* | | | | `[307cbc29c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/307cbc29c) aj meta charset (2024-06-11 11:47) <Daniel Caillibaud>  
`* | | | | `[78fdd8075](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/78fdd8075) outil bulleAide n'existe plus (c'est un module js) (2024-06-11 11:47) <Daniel Caillibaud>  
`* | | | | `[cd0273cc6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd0273cc6) outil boulier n'existe plus (2024-06-11 11:47) <Daniel Caillibaud>  
`| |_|_|/  `  
`|/| | |   `  
`* | | |   `[c20320494](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c20320494) Merge branch 'modifs_mohascratchSuite' into 'main' (2024-06-10 19:57) <Tommy Barroy>  
`|\ \ \ \  `  
`| * \ \ \   `[8237fef77](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8237fef77) Merge branch 'main' into modifs_mohascratchSuite (2024-06-10 19:55) <Tommy Barroy>  
`| |\ \ \ \  `  
`| * | | | | `[433d26e87](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/433d26e87) modifie la comparaison des tracés pour plus de souplesse (2024-06-10 19:53) <Tommy Barroy>  
`| * | | | | `[e2c124876](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2c124876) pour me des fois undefined dans editor (2024-06-10 14:14) <Tommy Barroy>  
`| * | | | | `[ec28cae92](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec28cae92) reteste comparator dessin (2024-06-10 05:50) <Tommy Barroy>  
`* | | | | |   `[c0f37ab31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0f37ab31) Merge branch 'V2JC' into 'main' (2024-06-10 19:44) <Jean-Claude Lhote>  
`|\ \ \ \ \ \  `  
`| |_|/ / / /  `  
`|/| | | | |   `  
`| * | | | | `[ffe0944ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffe0944ba) fixe le problème du 'Enter' intercepté par les MathfieldElements qui le convertissent en event 'change' (2024-06-10 19:42) <Jean-claude Lhote>  
`| * | | | | `[44e64455f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44e64455f) fix jsdoc (2024-06-10 18:36) <Jean-claude Lhote>  
`| * | | | | `[4aba854ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4aba854ec) ajout de la durée aux résultats V2 (un oubli !) et donc affichage dans les bilans. (2024-06-10 18:36) <Jean-claude Lhote>  
`| * | | | | `[bf77dfaa1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf77dfaa1) Maj migration.md (2024-06-10 18:36) <Jean-claude Lhote>  
`|/ / / / /  `  
`* | | | |   `[7c93eb1ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c93eb1ed) Merge branch 'modifs_mohascratch' into 'main' (2024-06-10 17:35) <Tommy Barroy>  
`|\ \ \ \ \  `  
`| * | | | | `[66c5d6e38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66c5d6e38) pour me des fois undefined dans editor (2024-06-10 05:54) <Tommy Barroy>  
`| |/ / / /  `  
`| * | | | `[b99132de8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b99132de8) la largeur du div blockly s'agrandi quand y'a la place y'avait un bug si le programme démo se lançait en début de section ( il était pas compté comme fini ) (2024-06-09 21:59) <Tommy Barroy>  
`* | | | | `[a3db55661](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3db55661) Aj du param PRODLIKE=2 pour loadAll afin de tester la migration (2024-06-09 10:44) <Daniel Caillibaud>  
`* | | | | `[24524d124](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24524d124) mod msg erreur (2024-06-09 10:35) <Daniel Caillibaud>  
`* | | | | `[b8031083c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8031083c) error => warning (2024-06-09 10:34) <Daniel Caillibaud>  
`* | | | | `[2de1cde39](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2de1cde39) commentaire fixme obsolete viré (2024-06-09 10:28) <Daniel Caillibaud>  
`|/ / / /  `  
`* | | |   `[de9423896](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de9423896) Merge branch 'aj_bouton_reset_dans_enonce' into 'main' (2024-06-09 09:58) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[0fabed983](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0fabed983) ajoute des codes pour avoir des images de boutons dans enonce (2024-06-09 09:55) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[0dda03e2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0dda03e2d) Merge branch 'notify' into 'main' (2024-06-09 00:11) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[229ba99e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/229ba99e1) notify pour comprendre (2024-06-09 00:07) <Tommy Barroy>  
`| * | | | `[e9e0df94c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9e0df94c) notify pour comprendre (2024-06-09 00:02) <Tommy Barroy>  
`| * | | | `[f8280b4b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8280b4b6) notify pour comprendre (2024-06-08 23:56) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[274e2bef0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/274e2bef0) Merge branch 'oubli_de_vire_le_blokage' into 'main' (2024-06-08 17:30) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[fa7de7811](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa7de7811) oubli de vire le blocage (2024-06-08 17:29) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[021c081c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/021c081c0) Merge branch 'pour_moha' into 'main' (2024-06-08 17:25) <Tommy Barroy>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[65485686b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65485686b) accepte 12,0 quand la précision est demandée au dixième (2024-06-08 17:24) <Tommy Barroy>  
`* | | |   `[f4c758562](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f4c758562) Merge branch 'Correction_Exo_Pythagore' into 'main' (2024-06-08 16:24) <Yves Biton>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[86a65f79f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86a65f79f) Correction d'un exercice sur Pythagore (2024-06-08 16:21) <Yves Biton>  
`* | | |   `[a3d389aff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3d389aff) Merge branch 'co_corec' into 'main' (2024-06-08 12:30) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[51c893a7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51c893a7d) pb correction tracés (2024-06-08 12:28) <Tommy Barroy>  
`* | | | |   `[22726505a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22726505a) Merge branch 'modVite' into 'main' (2024-06-08 12:29) <Daniel Caillibaud>  
`|\ \ \ \ \  `  
`| * | | | | `[335cb498f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/335cb498f) modif conf vite pour avoir le hot-reload sur tous les html (et pas seulement index.html), on passe par un doocroot.tmp (pour avoir publicDir ≠ outDir) fusionné en postBuild (2024-06-08 10:55) <Daniel Caillibaud>  
`* | | | | |   `[f8f2857d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8f2857d8) Merge branch 'repriseGrapheV2-suite' into 'main' (2024-06-08 11:53) <Jean-Claude Lhote>  
`|\ \ \ \ \ \  `  
`| |_|/ / / /  `  
`|/| | | | |   `  
`| * | | | | `[0495c3ded](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0495c3ded) checks des params des noeuds effectués pour la reprise de graphe (2024-06-08 11:53) <Jean-Claude Lhote>  
`|/ / / / /  `  
`* | | | |   `[15aaaaf53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15aaaaf53) Merge branch 'RepriseGraphV2' into 'main' (2024-06-08 11:23) <Jean-Claude Lhote>  
`|\ \ \ \ \  `  
`| * | | | | `[ad0107e6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad0107e6e) Reprise graph v2 (2024-06-08 11:23) <Jean-Claude Lhote>  
`|/ / / / /  `  
`* | | | |   `[7e7727865](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e7727865) Merge branch 'fallait_adapter_getpointposition' into 'main' (2024-06-08 11:07) <Tommy Barroy>  
`|\ \ \ \ \  `  
`| |_|/ / /  `  
`|/| | | |   `  
`| * | | | `[6f7ba35f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f7ba35f4) get point position pas  jour (2024-06-08 11:05) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[a43c531ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a43c531ff) Merge branch 'Correction_Multi_Etapes_Ineq' into 'main' (2024-06-07 22:37) <Yves Biton>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[238a2f726](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/238a2f726) La section multi étape ne fonctionnait pas bien pour un ensemble des solutions avec des valeurs isolées. (2024-06-07 22:35) <Yves Biton>  
`* | | | `[115ae5062](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/115ae5062) ajout d'une liste de liens en bas du form pour tester des sections en prod2 (2024-06-07 19:21) <Daniel Caillibaud>  
`* | | | `[8db3cf300](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8db3cf300) fix chargement jQuery pour les prodX.html (on passe par lazyLoader) (2024-06-07 17:59) <Daniel Caillibaud>  
`* | | | `[4639ea2ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4639ea2ac) manquait les deux html ajoutés précédemment dans le .gitignore (2024-06-07 14:22) <Daniel Caillibaud>  
`* | | | `[e15d56a69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e15d56a69) màj deps (2024-06-07 14:21) <Daniel Caillibaud>  
`* | | | `[97313c40d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97313c40d) fix init stats (2024-06-07 14:20) <Daniel Caillibaud>  
`* | | | `[0a9fb828a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a9fb828a) il ne faut pas d'extension pour le build vite avec les imports dynamiques de ts (2024-06-07 13:53) <Daniel Caillibaud>  
`* | | | `[2049e47a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2049e47a9) aj doc et typage (2024-06-07 13:35) <Daniel Caillibaud>  
`* | | | `[3d3fe53a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d3fe53a3) Ajout de deux liens pour tester "comme en prod" pour v1 ET v2 (2024-06-07 13:09) <Daniel Caillibaud>  
`* | | | `[1a0d6457a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a0d6457a) déplacement de fichiers pour le html (2024-06-07 13:09) <Daniel Caillibaud>  
`* | | | `[1e43e89ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e43e89ab) jsdoc mineur (2024-06-07 13:09) <Daniel Caillibaud>  
`* | | | `[3f0e1069f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f0e1069f) Ajout de isElt(elt, tag), très utile pour le narrowing ts (2024-06-07 13:09) <Daniel Caillibaud>  
`* | | |   `[68a2245a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68a2245a6) Merge branch 'MigrationMD' into 'main' (2024-06-07 11:20) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| * | | | `[57a32db85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57a32db85) ajout de commentaires pour la migration (2024-06-07 11:18) <Jean-claude Lhote>  
`|/ / / /  `  
`* | | | `[4023004cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4023004cd) rectif doc (2024-06-07 11:12) <Daniel Caillibaud>  
`* | | | `[5526d7630](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5526d7630) rmq doc (2024-06-07 09:56) <Daniel Caillibaud>  
`* | | |   `[2476f72cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2476f72cd) Merge branch 'RepriseDeGraphe' into 'main' (2024-06-06 18:46) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| * | | | `[d1066ae30](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1066ae30) On colle des résultats et on reprend le graphe ! (2024-06-06 18:45) <Jean-claude Lhote>  
`|/ / / /  `  
`* | | |   `[f5f266dd2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5f266dd2) Merge branch 'reticket' into 'main' (2024-06-06 16:24) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[6c8b56aea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c8b56aea) je la cache quand meme et elle saffiche normale si on sort du cadre (2024-06-06 16:23) <Tommy Barroy>  
`| * | | | `[0fa77ff11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0fa77ff11) remet souris au dessus de la croix, et affiche quand meme la souris pour voir quand elle sort du cadre (2024-06-06 16:14) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[d995f5ecd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d995f5ecd) Merge branch 'oubli_du_cylindre' into 'main' (2024-06-06 13:40) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[453856532](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/453856532) rajout du cylindre (2024-06-06 13:39) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[a816f97e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a816f97e7) Merge branch 'co_quand_pas_nb_essai' into 'main' (2024-06-05 23:05) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[8b89a3775](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b89a3775) un soucis (2024-06-05 23:03) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[5d1cba104](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d1cba104) Merge branch 'fixTestBlokfigure' into 'main' (2024-06-05 19:09) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[3a5589cb5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a5589cb5) fix appel de l'image toutes les seconde (on arrête de détruire et recréer le html en permanence), qui règle le pb du test qui plantait. (2024-06-05 19:07) <Daniel Caillibaud>  
`| * | | | `[c2ff1a593](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2ff1a593) on met en évidence le pb (le html reconstruit toutes les secondes) (2024-06-05 18:28) <Daniel Caillibaud>  
`* | | | |   `[3d2e980c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d2e980c8) Merge branch 'ajouteBienecrire' into 'main' (2024-06-05 18:49) <Jean-Claude Lhote>  
`|\ \ \ \ \  `  
`| * | | | | `[f78a380ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f78a380ac) Ajout d'un bouton pour coller un grapheV1 dans start.form.ts (2024-06-05 18:48) <Jean-claude Lhote>  
`|/ / / / /  `  
`* | | | |   `[5ba1b6da5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ba1b6da5) Merge branch 'fixstartForm' into 'main' (2024-06-05 18:14) <Jean-Claude Lhote>  
`|\ \ \ \ \  `  
`| * | | | | `[fa7e9bc8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa7e9bc8e) bug dans le formulaire sur l'import de graphe V1 résolu (2024-06-05 18:11) <Jean-claude Lhote>  
`|/ / / / /  `  
`* | | | |   `[b76d627fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b76d627fc) Merge branch 'destickets' into 'main' (2024-06-05 17:27) <Tommy Barroy>  
`|\ \ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`| * | | | `[eee0c3857](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eee0c3857) [fix #283](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/283) (2024-06-05 17:25) <Tommy Barroy>  
`| * | | | `[cc65deac4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc65deac4) [fix #286](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/286) (2024-06-05 17:07) <Tommy Barroy>  
`| * | | | `[72fbd6425](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72fbd6425) [fix #287](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/287) (2024-06-05 17:00) <Tommy Barroy>  
`|/ / / /  `  
`* | | | `[9cace16a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9cace16a2) aj commit-msg git hook (2024-06-05 10:47) <Daniel Caillibaud>  
`* | | | `[9c25ebdde](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c25ebdde) modif de forme (timeouts écrits avec séparateur de millier pour la lisibilité) (2024-06-05 09:19) <Daniel Caillibaud>  
`* | | | `[c6fff9971](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6fff9971) locator .scratchReset évalué à chaque usage (2024-06-05 09:17) <Daniel Caillibaud>  
`* | | | `[56447a85d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/56447a85d) simplification mineure (2024-06-05 09:14) <Daniel Caillibaud>  
`| |_|/  `  
`|/| |   `  
`* | |   `[82b2eb8f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/82b2eb8f9) Merge branch 'opacity_et_prof_dep_modifiable' into 'main' (2024-06-02 15:30) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[dabeead3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dabeead3a) remet opacity et ajouter prog de départ modifiable (2024-06-02 15:27) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[659292e2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/659292e2f) Merge branch 'pour_fix' into 'main' (2024-05-31 17:00) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[df98114c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df98114c5) [fix #282](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/282) [fix #284](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/284) (2024-05-31 16:59) <Tommy Barroy>  
`|/ /  `  
`* |   `[2f00ca687](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f00ca687) Merge branch 'Correction_Exo_Vecteurs' into 'main' (2024-05-31 09:38) <Yves Biton>  
`|\ \  `  
`| * | `[219f5994d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/219f5994d) Correction exo sur vecteurs [fix #285](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/285) (2024-05-31 09:37) <Yves Biton>  
`* | |   `[4ab0b9360](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ab0b9360) Merge branch 'too_much_recurs_devrait_plus_arriver' into 'main' (2024-05-30 21:21) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[e0680c460](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e0680c460) too much recurs peut etre fix (2024-05-30 21:19) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[97933ad66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97933ad66) Merge branch 're_fis' into 'main' (2024-05-30 20:56) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[604f17964](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/604f17964) j3pNotify pour voir (2024-05-30 20:53) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[93f62ff67](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93f62ff67) Merge branch 'bugsnag_blok_pas_défini' into 'main' (2024-05-30 20:31) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[bada0c396](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bada0c396) tyztr (2024-05-30 20:30) <Tommy Barroy>  
`| * | | `[2bfae3325](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2bfae3325) tyztr (2024-05-30 20:28) <Tommy Barroy>  
`* | | |   `[f6ec9243a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6ec9243a) Merge branch 'timer' into 'main' (2024-05-30 19:22) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| |_|/ /  `  
`|/| | |   `  
`| * | | `[48c870a1f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48c870a1f) timer ok + fix un problème sur les span qui étaient imbriqués dans des spans. (2024-05-30 19:20) <Jean-claude Lhote>  
`| * | | `[9c0bbdc34](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c0bbdc34) mise en place du timer 'limite' sur Playground.showStatus (sectionDeTests) (2024-05-30 14:47) <Jean-claude Lhote>  
`* | | |   `[3e6fae8db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e6fae8db) Merge branch 'Amelioration_Section_Multi_Etapes' into 'main' (2024-05-30 15:50) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[3cc333376](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3cc333376) Amélioration de la section multi étapes pour l'évaluation des réponses dans les éditeurs internes à la figure comme cela a déjà été fait pour la section multi-edit. (2024-05-30 15:49) <Yves Biton>  
`|/ / / /  `  
`* | | |   `[0d5398c2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d5398c2b) Merge branch 'Modification_Min_Width_Mathlive_Editor' into 'main' (2024-05-29 21:17) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[e41e0e84c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e41e0e84c) Passage de la largeur mini des éditeurs mathfield à 28 pixels au lieu de 30. (2024-05-29 21:16) <Yves Biton>  
`|/ / / /  `  
`* | | |   `[79aa7ec3b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79aa7ec3b) Merge branch 'mathifeld-plus-larges-en-V1' into 'main' (2024-05-29 18:35) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[f48112aa5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f48112aa5) largeur minimum d'un mathfield : 40px (2024-05-29 18:34) <Jean-claude Lhote>  
`|/ / /  `  
`* | |   `[47c511305](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47c511305) Merge branch 'doc' into 'main' (2024-05-29 17:28) <Jean-Claude Lhote>  
`|\ \ \  `  
`| * | | `[0a319d86c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a319d86c) Doc (2024-05-29 17:28) <Jean-Claude Lhote>  
`|/ / /  `  
`* | |   `[e7379828c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e7379828c) Merge branch 'suiteDoc' into 'main' (2024-05-29 17:01) <Jean-Claude Lhote>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[3365f9dca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3365f9dca) Suite doc (2024-05-29 17:01) <Jean-Claude Lhote>  
`|/ /  `  
`* |   `[825a3fc8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/825a3fc8e) Merge branch 'redacDoc' into 'main' (2024-05-27 18:55) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[e8089850c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8089850c) De la lecture dans le tuto sectionV2.md (2024-05-27 18:55) <Jean-Claude Lhote>  
`|/ /  `  
`* |   `[91ffac107](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/91ffac107) Merge branch 'Correction_Clavier_Virtuel_Clignotement_Show_Hide' into 'main' (2024-05-24 19:08) <Yves Biton>  
`|\ \  `  
`| * | `[d0553ef54](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0553ef54) Correction du clavier virtuel. Sur certains exos avec plusieurs éditeurs ou palce hoders, une fois validée la première réponse, ça se mettaient de clignoter des claviers virtuels à plusierus endroits. Le show du clavier virtuel n'apelle plus le focus sur l'éditeur (car le focus pouvait lui-même appeler show). (2024-05-24 19:06) <Yves Biton>  
`|/ /  `  
`* |   `[65cd5467e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65cd5467e) Merge branch 'Amelioration_Multi_Etape' into 'main' (2024-05-24 14:15) <Yves Biton>  
`|\ \  `  
`| * | `[84eb0aa21](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84eb0aa21) Amélioration de la section multi étapes pour qu'on puisse faire remarquer certaines fautes quand la figure contient un LaTeX de tag 'faute' suivi du numéro d'étape. (2024-05-24 14:13) <Yves Biton>  
`* | |   `[64baeea64](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64baeea64) Merge branch 'addMathlive' into 'main' (2024-05-24 10:26) <Jean-Claude Lhote>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[ad37a25f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad37a25f2) mise à jour des sections exemples V2 avec le nouvel item de type 'mathlive' (2024-05-24 10:25) <Jean-claude Lhote>  
`| * | `[8d15bec4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d15bec4b) Réorganisation de types (2024-05-23 20:24) <Jean-claude Lhote>  
`| * | `[57d2093e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57d2093e4) Changement DomEltPlaceholder.ts disparaît DomEltMathlive.ts arrive. Il gère aussi bien les mathfields standards que les fillInTheBlank. (2024-05-23 20:06) <Jean-claude Lhote>  
`* | |   `[e8640f0fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8640f0fc) Merge branch 'Amelioration_Section_Multi_Etapes' into 'main' (2024-05-23 15:15) <Yves Biton>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[241cf6709](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/241cf6709) Petite correction de cette section : quand on était dans une étape de résolution d'inéquation et que k'élève faisait une faute de parenthèse sur l'infini à la preière réponse, le bouton Reopeir la réponse précédente n'apparaissait pas. (2024-05-23 15:12) <Yves Biton>  
`* | |   `[b3e22d91f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3e22d91f) Merge branch 'ticketss' into 'main' (2024-05-21 15:48) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[efe98cfc0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/efe98cfc0) [fix #277](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/277) (2024-05-21 15:46) <Tommy Barroy>  
`| * | `[1ebf8d132](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ebf8d132) [fix #279](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/279) (2024-05-21 15:31) <Tommy Barroy>  
`* | |   `[bee0a4939](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bee0a4939) Merge branch 'Corrcetion_Toggle_Button_Clavir_Virtuel' into 'main' (2024-05-21 15:16) <Yves Biton>  
`|\ \ \  `  
`| * | | `[6428cbe46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6428cbe46) Correction du bouton pour "toggler" le clavier virtuel qui ne marchait pas correctement sur écran tactile (2024-05-21 15:14) <Yves Biton>  
`|/ / /  `  
`* | |   `[1678875ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1678875ed) Merge branch 'fixCheckBilan' into 'main' (2024-05-21 14:24) <Jean-Claude Lhote>  
`|\ \ \  `  
`| * | | `[937a765bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/937a765bf) ajout d'un arrondi semblable à celui de Parcours._addBilan dans checkBilan (2024-05-21 14:22) <Jean-claude Lhote>  
`|/ / /  `  
`* | |   `[9c23dde98](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c23dde98) Merge branch 'fixTest' into 'main' (2024-05-17 17:55) <Jean-Claude Lhote>  
`|\ \ \  `  
`| * | | `[88c732d6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88c732d6f) ajout de verbose dans checkBilan pour pister les calculs de % + ajout d'un await oublié. (2024-05-17 17:49) <Jean-claude Lhote>  
`|/ / /  `  
`* | |   `[4c43d419d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c43d419d) Merge branch 'Amelioration_Section_Multil_Edit' into 'main' (2024-05-15 09:22) <Yves Biton>  
`|\ \ \  `  
`| * | | `[17f116eb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17f116eb8) Lodification pour qu'une répo,nse globale puisse être attendue dans le cas de plusieurs éditeurs internes (2024-05-15 09:19) <Yves Biton>  
`* | | |   `[952210889](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/952210889) Merge branch 'mixV1V2' into 'main' (2024-05-14 18:15) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| * | | | `[4a3bddbb4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a3bddbb4) quelques fixes. L'enchainement mixV2/V1 semble fonctionner (2024-05-14 18:13) <Jean-claude Lhote>  
`| * | | | `[0429be0bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0429be0bb) Une fonction pour récupérer le mapping node.id => idV1 et donner le bon initialId au parcours. (2024-05-13 20:33) <Jean-claude Lhote>  
`|/ / / /  `  
`* | | |   `[bd515f246](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd515f246) Merge branch 'bilanFeedbackEtDialog' into 'main' (2024-05-13 19:41) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[9d288f82d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d288f82d) feedback + bilan + arrêt du spinner à la fin du parcours. (2024-05-13 19:33) <Jean-claude Lhote>  
`| * | | `[de6eca4e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de6eca4e5) showFeedback attends un clic. ajout de className aux DomEltSpan (2024-05-07 18:24) <Jean-claude Lhote>  
`* | | |   `[44fe16005](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44fe16005) Merge branch 'Correction_Focus_Clavier_Virtuel' into 'main' (2024-05-12 14:53) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[1a8fb80f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a8fb80f7) Les boutons pour faire apparaître le clavier prenaient le focus. Avec un editeur et des plahe holder le clavier virtuel n'apparaissait pas. Et le pire : Jean-Claude avait cassé ma section excalcMathQuill. Heureusement que je m'en suis aperçu avant le déploiement ... (2024-05-12 14:50) <Yves Biton>  
`|/ / / /  `  
`* | | |   `[d9164d52f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9164d52f) Merge branch 'Amelioration_Section_Multi_Edit' into 'main' (2024-05-10 20:56) <Yves Biton>  
`|\ \ \ \  `  
`| |_|/ /  `  
`|/| | |   `  
`| * | | `[06d497311](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06d497311) Amélioration de la désactivation deséditeurs internes pour que, en cas de réussite ils ne soient pas trop "palots" (2024-05-10 20:53) <Yves Biton>  
`* | | |   `[de16fef3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de16fef3d) Merge branch 'liminA_peut_etre_undefined' into 'main' (2024-05-07 15:43) <Tommy Barroy>  
`|\ \ \ \  `  
`| |_|/ /  `  
`|/| | |   `  
`| * | | `[be00eb5c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be00eb5c9) liminA peut etre undefined (2024-05-07 15:42) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[061cbc803](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/061cbc803) Merge branch 'AvecModale' into 'main' (2024-05-06 19:36) <Jean-Claude Lhote>  
`|\ \ \  `  
`| * \ \   `[8b9dc8c2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b9dc8c2f) Merge branch 'modaleSuite' into AvecModale (2024-05-06 19:31) <Jean-claude Lhote>  
`| |\ \ \  `  
`| | |/ /  `  
`| |/| |   `  
`| | * | `[b0b4bcbf8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0b4bcbf8) merge Clavier (2024-04-27 14:24) <Jean-claude Lhote>  
`| | * |   `[fd3205678](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd3205678) Merge branch 'clavierIsOk' into modaleSuite (2024-04-27 14:21) <Jean-claude Lhote>  
`| | |\ \  `  
`| | * | | `[a58f38ba8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a58f38ba8) problème avec clavier... (2024-04-27 14:19) <Jean-claude Lhote>  
`| | * | | `[7933b28bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7933b28bf) playground.dialog() attend... et clique ! (2024-04-27 13:17) <Jean-claude Lhote>  
`| | * | | `[95205586f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95205586f) on garde le délai mais pas le onClickOk c'est playground.dialog qui gère (2024-04-27 12:33) <Jean-claude Lhote>  
`| | * | | `[19f8d1d5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19f8d1d5a) ajout clearTimeout (2024-04-26 21:34) <Jean-claude Lhote>  
`| | * | |   `[944cd80c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/944cd80c7) Merge branch 'fixMixV1V2' into modaleSuite (2024-04-26 09:26) <Jean-claude Lhote>  
`| | |\ \ \  `  
`| | | * | | `[6cbab74a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cbab74a0) waitForClick('nextSection') sorti du runOneNode vers run (il faut envoyer le résultat avant ce clic, pour le sauvegarder au cas où le clic n'arrive jamais) (2024-04-24 12:56) <Daniel Caillibaud>  
`| | * | | | `[c4670b8cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4670b8cd) ajout de delay et onClickOK à DialogOptions et ModalOptions (2024-04-26 09:21) <Jean-claude Lhote>  
`* | | | | |   `[80c74f255](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80c74f255) Merge branch 'ToutNeuf' into 'main' (2024-05-06 19:29) <Jean-Claude Lhote>  
`|\ \ \ \ \ \  `  
`| |/ / / / /  `  
`|/| | | | |   `  
`| * | | | | `[a1a0b063e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1a0b063e) Merge branch 'clavierIsOk' into ToutNeuf (2024-05-06 19:27) <Jean-claude Lhote>  
`|/| | | | | `  
`| | |_|/ /  `  
`| |/| | |   `  
`| * | | | `[488d65490](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/488d65490) events passive (2024-04-24 16:13) <Jean-claude Lhote>  
`| * | | | `[a4d327455](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a4d327455) Le focusOut ne s'occupe plus des boutons. Les events sur le triggerBtn sont gérés par addEventListener() en capturing phase (2024-04-24 14:51) <Jean-claude Lhote>  
`| * | | | `[e0873c50f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e0873c50f) clavier refermable via bouton (2024-04-22 17:33) <Jean-claude Lhote>  
`| * | | | `[4e871de65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e871de65) passage de outils/mathlive/utils en typescript. (2024-04-22 14:46) <Jean-claude Lhote>  
`| * | | | `[bc0e402bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bc0e402bb) maj commentaires (2024-04-22 13:05) <Jean-claude Lhote>  
`| * | | | `[f75bfb1ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f75bfb1ba) maj commentaires (2024-04-22 13:02) <Jean-claude Lhote>  
`| * | | | `[f2815c8cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2815c8cb) problème du focusOut réglé à tester (2024-04-22 12:24) <Jean-claude Lhote>  
`| * | | | `[0cc67b716](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0cc67b716) ajout de l'option -o pour indiquer un fichier où écrire le graphe (2024-04-22 12:24) <Daniel Caillibaud>  
`| * | | | `[8e50fa5e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e50fa5e9) Touches du clavier ok en V1 (2024-04-19 12:54) <Jean-claude Lhote>  
`| * | | | `[1090c3a64](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1090c3a64) Bouton ok en V1 (2024-04-16 17:54) <Jean-claude Lhote>  
`| * | | | `[c6e3e5a0b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6e3e5a0b) Le bouton sera derrière l'éditeur quoi qu'il arrive (2024-04-16 17:42) <Jean-claude Lhote>  
`| * | | | `[fe8cc1e40](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe8cc1e40) VirtualKeyboard Ok (le triggerButton est un button) (2024-04-16 17:28) <Jean-claude Lhote>  
`| * | | | `[511ef8f30](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/511ef8f30) fix bug test sur le hide inopérant (2024-04-16 17:22) <Jean-claude Lhote>  
`| * | | | `[20785314f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20785314f) rename hideThis devient hide (2024-04-16 16:54) <Jean-claude Lhote>  
`| * | | | `[40757719f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/40757719f) rename showThis en show (2024-04-16 15:50) <Jean-claude Lhote>  
`| * | | | `[d8f4e0255](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8f4e0255) suppression de showKeyboard (2024-04-16 15:47) <Jean-claude Lhote>  
`| * | | | `[7ded7b8c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ded7b8c2) factrorisation Btn et listener suite (2024-04-16 15:02) <Jean-claude Lhote>  
`| * | | | `[c45eede94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c45eede94) Retour du hide (2024-04-14 16:33) <Jean-claude Lhote>  
`| * | | | `[972e65ee4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/972e65ee4) Clavier virtuel refactorisé et fonctionnel ? (2024-04-14 16:33) <Jean-claude Lhote>  
`| * | | | `[5bc1305f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5bc1305f0) Ouf ça bouge à nouveau (2024-04-14 16:33) <Jean-claude Lhote>  
`| * | | | `[0e4cb7ee3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e4cb7ee3) mise en place de deux claviers pour les tests (2024-04-14 16:33) <Jean-claude Lhote>  
`| * | | | `[ecce8d1f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecce8d1f3) clavier qui s'affiche... (2024-04-14 16:33) <Jean-claude Lhote>  
`| * | | | `[666eb2628](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/666eb2628) suppression variable devenue inutile (2024-04-14 16:33) <Jean-claude Lhote>  
`| * | | | `[b9ec71102](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9ec71102) draft: clavier refactorisé pas encore opérationnel (2024-04-14 16:33) <Jean-claude Lhote>  
`| * | | | `[27a6e2d48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27a6e2d48) suppression console.log() remplacé par un fixme : voir ce qui intercepte le keyCode 13 (2024-04-14 16:33) <Jean-claude Lhote>  
`| * | | | `[e2ca97735](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2ca97735) options mieux renseignées dans afficheMathliveDans() (2024-04-14 16:33) <Jean-claude Lhote>  
`| * | | | `[f55f9f262](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f55f9f262) draft: passage de VirtualKeyboard en typescript (2024-04-14 16:33) <Jean-claude Lhote>  
`* | | | |   `[20d44151d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20d44151d) Merge branch 'Amelioration_CleanLatexForMl_Puissances' into 'main' (2024-05-05 18:40) <Yves Biton>  
`|\ \ \ \ \  `  
`| * | | | | `[1eeb59aff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1eeb59aff) Amélioration de la désactivation deséditeurs internes pour que, en cas de réussite ils ne soient pas trop "palots" (2024-05-05 18:36) <Yves Biton>  
`* | | | | |   `[d61c3bf39](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d61c3bf39) Merge branch 'pbTauxVariation' into 'main' (2024-05-05 12:39) <Rémi Deniaud>  
`|\ \ \ \ \ \  `  
`| |/ / / / /  `  
`|/| | | | |   `  
`| * | | | | `[0ac8414d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ac8414d7) correction dans tauxEvolution (2024-05-05 12:35) <Rémi Deniaud>  
`| * | | | | `[36f81fac8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/36f81fac8) nettoyage tauxReciproque (2024-05-05 12:23) <Rémi Deniaud>  
`|/ / / / /  `  
`* | | | |   `[f8e79fddc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8e79fddc) Merge branch 'Amelioration_Multi_Etape' into 'main' (2024-05-05 10:33) <Yves Biton>  
`|\ \ \ \ \  `  
`| * | | | | `[3cca7bd70](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3cca7bd70) Amélioration de la désactivation deséditeurs internes pour que, en cas de réussite ils ne soient pas trop "palots" (2024-05-05 10:31) <Yves Biton>  
`|/ / / / /  `  
`* | | | |   `[0e89fe37c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e89fe37c) Merge branch 'Amelioration_Multi_Edit' into 'main' (2024-05-04 17:48) <Yves Biton>  
`|\ \ \ \ \  `  
`| * | | | | `[c071d3a2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c071d3a2d) Section multi edit modifiée pour bien désactiver les éditeurs lors de la correction Clavier virtuel modifié pour que si un éditeur mathfield a readOnly à true le clavie ne soit plus activable. (2024-05-04 17:46) <Yves Biton>  
`* | | | | | `[5d03518ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d03518ad) màj deps (2024-05-04 10:37) <Daniel Caillibaud>  
`|/ / / / /  `  
`* | | | |   `[71f1dd7cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/71f1dd7cc) Merge branch 'Changt_Couleur_Correction_Interne' into 'main' (2024-05-03 10:20) <Yves Biton>  
`|\ \ \ \ \  `  
`| * | | | | `[7f434fdac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f434fdac) On met un cert plus foncé pour les éditeurs internes quand on garde l'éditeur pour la correction car sinon le vert est trop pâle. (2024-05-03 10:19) <Yves Biton>  
`|/ / / / /  `  
`* | | | |   `[006c16c2a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/006c16c2a) Merge branch 'Correction_Multi_Edit_Corr_Interne' into 'main' (2024-05-02 19:12) <Yves Biton>  
`|\ \ \ \ \  `  
`| * | | | | `[10eedf57e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10eedf57e) Corrige la section multi étapes : il y avait plantage quand on gardait les réponses fausses sur la figure avec les éditeurs internes avec appel de j3pBarre (2024-05-02 19:10) <Yves Biton>  
`|/ / / / /  `  
`* | | | |   `[fd2381dab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd2381dab) Merge branch 'Correction_Multi_Etapes' into 'main' (2024-05-02 18:42) <Yves Biton>  
`|\ \ \ \ \  `  
`| * | | | | `[dc49b13c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc49b13c9) Corrige la section multi étapes : il y avait plantage quand on gardait les réponses fausses sur la figure avec les éditeurs internes avec appel de j3pBarre (2024-05-02 18:38) <Yves Biton>  
`|/ / / / /  `  
`* | | | |   `[ecfa98655](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecfa98655) Merge branch 'Correction_Boutons_Multi_Etapes' into 'main' (2024-05-02 13:21) <Yves Biton>  
`|\ \ \ \ \  `  
`| * | | | | `[1df9790fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1df9790fa) La solution était affichée en bas de la figure pour cet exo de trigi [fix #267](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/267) (2024-05-02 13:19) <Yves Biton>  
`* | | | | | `[291bc5fda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/291bc5fda) nettoyage (2024-04-25 11:18) <Daniel Caillibaud>  
`* | | | | | `[fcbfabe78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fcbfabe78) faut pas continuer en cas de problème d'énoncé (2024-04-25 11:12) <Daniel Caillibaud>  
`* | | | | |   `[705ac3023](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/705ac3023) Merge branch 'blinder-etudeFonction_limite' into 'main' (2024-04-25 10:29) <Jean-Claude Lhote>  
`|\ \ \ \ \ \  `  
`| |_|_|/ / /  `  
`|/| | | | |   `  
`| * | | | | `[9639b8619](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9639b8619) blindage de l'appel de fctsEtudeDetermineLimiteSurcharge dans sectionEtudeFonction_limite (2024-04-25 10:27) <Jean-claude Lhote>  
`|/ / / / /  `  
`* | | / / `[92267f13d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/92267f13d) [fix #263](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/263) (2024-04-24 17:59) <Daniel Caillibaud>  
`| |_|/ /  `  
`|/| | |   `  
`* | | |   `[523cc70f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/523cc70f2) Merge branch 'dialogJC' into 'main' (2024-04-23 21:06) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| * | | | `[f780ea7c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f780ea7c9) Playground.dialog() s'autodétruit au bout de 5s, ou en cliquant avant sur OK. (2024-04-23 20:22) <Jean-claude Lhote>  
`|/ / / /  `  
`* | | |   `[6d675b177](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d675b177) Merge branch 'extensionGrapheTest' into 'main' (2024-04-23 19:50) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| * | | | `[a35c2734b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a35c2734b) je rallonge le graphe avec données persistantes pour vérifier que le parcours incrémente bien indexProgression OK (2024-04-23 19:48) <Jean-claude Lhote>  
`|/ / / /  `  
`* | | | `[281952bdb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/281952bdb) msg de debug obsoletes virés (2024-04-23 17:09) <Daniel Caillibaud>  
`* | | |   `[449aed739](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/449aed739) Merge branch '262-pb-de-numerotation-des-sections-en-v2' into 'main' (2024-04-23 17:02) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| * | | | `[1e5feb532](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e5feb532) Le numéro de section dans Parcours (indexProgression) mis à jour par le LegacySectionRunner (2024-04-23 17:02) <Jean-Claude Lhote>  
`|/ / / /  `  
`* | | |   `[ce55c7440](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce55c7440) Merge branch 'vectColineaires' into 'main' (2024-04-23 15:45) <Rémi Deniaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[fd0da5889](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd0da5889) amélioration code colinearite (2024-04-23 15:43) <Rémi Deniaud>  
`| * | | `[b9f10b7be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9f10b7be) correction appliColinearite quand il n'y a qu'une chance (2024-04-23 15:37) <Rémi Deniaud>  
`|/ / /  `  
`* | | `[bf85ef2ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf85ef2ab) La solution était affichée en bas de la figure pour cet exo de trigi [fix #267](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/267) (2024-04-23 10:10) <Yves Biton>  
`* | | `[ffb18608d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffb18608d) ajout de l'option -o pour indiquer un fichier où écrire le graphe (2024-04-16 15:27) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[2a99682b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a99682b1) Merge branch 'petittruc' into 'main' (2024-04-12 21:05) <Tommy Barroy>  
`|\ \  `  
`| * | `[6dd69fc7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6dd69fc7d) return si this.pl pas initaialisé ( je sais pas pk ) (2024-04-12 21:04) <Tommy Barroy>  
`|/ /  `  
`* |   `[6dc88182a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6dc88182a) Merge branch 'fixChoisisTaFonction' into 'main' (2024-04-12 14:40) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[4493d9195](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4493d9195) fixe un bug dans la section choisisTaFonction (2024-04-12 14:38) <Jean-claude Lhote>  
`|/ /  `  
`* |   `[ee94202b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee94202b4) Merge branch 'Correction_Enonce_Vecteurs' into 'main' (2024-04-12 11:53) <Yves Biton>  
`|\ \  `  
`| * | `[c012eafe6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c012eafe6) Correction de vecteurs mal rendus dans un  énoncé [fix #264](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/264) (2024-04-12 11:51) <Yves Biton>  
`| * | `[b7cbdbc4d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7cbdbc4d) Correction de code Latex périmés sur les vecteurs depuis massage MathLive (2024-04-11 09:31) <Yves Biton>  
`* | | `[9198539fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9198539fc) modif label (2024-04-10 07:01) <Daniel Caillibaud>  
`|/ /  `  
`* | `[73f200bf4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/73f200bf4) propagation correcte de mtgUrl à l'iframe (2024-04-09 15:55) <Daniel Caillibaud>  
`* | `[187ef4e95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/187ef4e95) loadJs accepte l'option type (pour passer module) (2024-04-09 15:53) <Daniel Caillibaud>  
`* |   `[5f4f9da9e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f4f9da9e) Merge branch 'il_manquait_des_cas' into 'main' (2024-04-09 14:41) <Tommy Barroy>  
`|\ \  `  
`| * | `[5685078e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5685078e6) il manquait des cas dans aléatoire (2024-04-09 14:40) <Tommy Barroy>  
`|/ /  `  
`* |   `[b4ce7b310](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4ce7b310) Merge branch 'afficheBilan' into 'main' (2024-04-07 17:34) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[86bab98c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86bab98c4) ajout du bilan. Playground.dialog() prend du html comme message si on veut. (2024-04-07 17:16) <Jean-claude Lhote>  
`| * | `[e1086a6ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1086a6ba) ajout de parcours terminé. + fixe des awaits manquant (2024-04-06 21:47) <Jean-claude Lhote>  
`| * | `[4cac0f695](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4cac0f695) modifs Daniel (2024-04-06 19:25) <Jean-claude Lhote>  
`| * | `[f1219bb38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1219bb38) correction syntaxique des Graphes de exemples.ts (2024-04-06 19:25) <Jean-claude Lhote>  
`|/ /  `  
`* |   `[030c9acc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/030c9acc7) Merge branch 'fixeBlockFigure2' into 'main' (2024-04-04 14:47) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[fa86a7665](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa86a7665) Retour des deux essais dans blockFigure : bouton RAZ à nouveau opérationnel (2024-04-04 14:47) <Jean-Claude Lhote>  
`|/ /  `  
`* |   `[4f6f3efa9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f6f3efa9) Merge branch 'AFixerBlokFigure' into 'main' (2024-04-03 20:00) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[fecf4b734](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fecf4b734) le test passe avec le correctif de Scratch.js (2024-03-27 18:18) <Jean-claude Lhote>  
`| * | `[f6478fe27](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6478fe27) le test passe avec le correctif de Scratch.js (2024-03-26 21:24) <Jean-claude Lhote>  
`| * | `[9d3001969](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d3001969) fixme pour Tommy dans blokFigure (2024-03-26 20:40) <Jean-claude Lhote>  
`* | |   `[4bcac1ea2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4bcac1ea2) Merge branch 'Correction_Integrales_Pour_Contrer_Bug_MathLive' into 'main' (2024-04-03 11:12) <Yves Biton>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[d80ea3df9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d80ea3df9) Les erreurs rencontrées dans certains exercices sur les intégrales étaient dues à un bug mathlive sur les place holders après un \differential. On n'utilise plus de place holder et on attend un seul symbole après le symbole différentiel. La fonction traiteIntegrales a dû être revue partout où elle utilise MathLive. [fix #258](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/258) (2024-04-03 11:05) <Yves Biton>  
`* | |   `[6e6ecccd8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e6ecccd8) Merge branch 'bugsncomp' into 'main' (2024-03-29 23:26) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[177c9dd75](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/177c9dd75) bugsnag (2024-03-29 23:25) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[b3a5c0a1d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3a5c0a1d) Merge branch 'pour_ticket_encore' into 'main' (2024-03-29 23:08) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[72ee05889](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72ee05889) [fix #252](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/252) (2024-03-29 23:07) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[e65760adb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e65760adb) Merge branch 'pour_ticket' into 'main' (2024-03-29 22:09) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[f59de489f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f59de489f) [fix #256](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/256) (2024-03-29 22:08) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[2da743937](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2da743937) Merge branch 'repare_2_reponses' into 'main' (2024-03-28 23:08) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[e32f70cb1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e32f70cb1) reponse incomplete impromptu (2024-03-28 23:07) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[7a8c70b4d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a8c70b4d) Merge branch 'pour_ticket_correc' into 'main' (2024-03-28 17:25) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[b9024d9db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9024d9db) #fix 257 (2024-03-28 17:24) <Tommy Barroy>  
`|/ / /  `  
`* | | `[436d1eb9b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/436d1eb9b) fix commentaire foireux (2024-03-27 16:02) <Daniel Caillibaud>  
`* | | `[92ca7c857](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/92ca7c857) aj de config typedoc pour virer mathlive (et sa collection d'erreurs) de la doc, sans succès (2024-03-27 16:02) <Daniel Caillibaud>  
`* | | `[ae71bd839](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae71bd839) timeoutLocked viré car inutilisé (2024-03-27 16:02) <Daniel Caillibaud>  
`* | | `[476bd2d2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/476bd2d2e) aj doc testBrowser (2024-03-27 16:02) <Daniel Caillibaud>  
`* | | `[bfdd50a45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bfdd50a45) aj doc HEADLESS=1 (2024-03-27 16:02) <Daniel Caillibaud>  
`| |/  `  
`|/|   `  
`* |   `[b32124e4e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b32124e4e) Merge branch 'fixTest1' into 'main' (2024-03-26 16:21) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[4b82c0e4f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b82c0e4f) fixe le test etudeFonctionVariations (2024-03-26 16:20) <Jean-claude Lhote>  
`|/ /  `  
`* | `[85bcfa821](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85bcfa821) màj deps (2024-03-26 14:34) <Daniel Caillibaud>  
`* | `[83c289bf6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83c289bf6) fix lancement des tests de chargement (2024-03-26 13:30) <Daniel Caillibaud>  
`* | `[156eb211f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/156eb211f) ortograf (2024-03-26 13:16) <Daniel Caillibaud>  
`* | `[a4c81ddc8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a4c81ddc8) commentaire (2024-03-26 13:16) <Daniel Caillibaud>  
`|/  `  
`*   `[bcbb8709e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bcbb8709e) Merge branch 'Correction_Exo_Vecteurs' into 'main' (2024-03-26 11:22) <Yves Biton>  
`|\  `  
`| * `[28f1dd032](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28f1dd032) Correction exercices sur vecteur de seconde. [fix #253](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/253) (2024-03-26 11:20) <Yves Biton>  
`|/  `  
`*   `[1ef441b19](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ef441b19) Merge branch 'whatToDo' into 'main' (2024-03-25 19:06) <Jean-Claude Lhote>  
`|\  `  
`| * `[3c1366b96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c1366b96) modifications clavier : boutons du 2e div aggrandis + ajout de .mqBtnpi pour tests (2024-03-25 19:06) <Jean-Claude Lhote>  
`|/  `  
`*   `[2d2e2f249](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d2e2f249) Merge branch 'test_propor' into 'main' (2024-03-20 20:27) <Tommy Barroy>  
`|\  `  
`| * `[422752581](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/422752581) test pour ticket sur ipad (2024-03-20 20:25) <Tommy Barroy>  
`* |   `[1e0d0fb7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e0d0fb7e) Merge branch 'jc-fignole' into 'main' (2024-03-20 17:19) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[d0bc7e217](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0bc7e217) Le clavier s'affiche et se cache avec les inputs classiques (typés en InputWithKeyboard) (de classe inputWithKeyboard (2024-03-20 17:17) <Jean-claude Lhote>  
`|/ /  `  
`* |   `[db8dd3871](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db8dd3871) Merge branch 'clavier' into 'main' (2024-03-20 16:02) <Jean-Claude Lhote>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[078d4dab6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/078d4dab6) Clavier (2024-03-20 16:02) <Jean-Claude Lhote>  
`|/  `  
`*   `[48a9f8911](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48a9f8911) Merge branch 'co_correc' into 'main' (2024-03-20 14:28) <Tommy Barroy>  
`|\  `  
`| * `[98b14d0ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98b14d0ee) [fix #250](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/250) (2024-03-20 14:27) <Tommy Barroy>  
`|/  `  
`* `[fb2879cda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb2879cda) fix mathgraph.d.ts (faut un export pour que ts comprenne que c'est un module) (2024-03-20 12:45) <Daniel Caillibaud>  
`* `[23da2b100](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23da2b100) nettoyage section proportionnalite02 (2024-03-20 12:04) <Daniel Caillibaud>  
`* `[d4e364faf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4e364faf) factorisation (2024-03-20 11:56) <Daniel Caillibaud>  
`* `[a078aed03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a078aed03) modif préventive pour compatibilité ascendante (l'api mathgraph qui aura bientôt isPromiseMode=false par défaut) (2024-03-20 11:53) <Daniel Caillibaud>  
`* `[d45ad26fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d45ad26fd) Aj de l'option absCoord: true pour les appels de mtgApp.getPointPosition (ça revient au même si la figure n'a pas de repère mais c'est plus prudent de le mettre explicitement) (2024-03-19 19:45) <Daniel Caillibaud>  
`* `[a9ff7604d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9ff7604d) Aj de l'option absCoord: true pour les appels de mtgApp.addText (ça revient au même si la figure n'a pas de repère mais c'est plus prudent de le mettre explicitement) (2024-03-19 19:45) <Daniel Caillibaud>  
`* `[590889368](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/590889368) Aj de l'option absCoord: true pour les appels de mtgApp.addLinkedPointCircle et addLinkedPointLine (ça revient au même si la figure n'a pas de repère mais c'est plus prudent de le mettre explicitement) (2024-03-19 19:45) <Daniel Caillibaud>  
`* `[bac59a361](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bac59a361) Aj de l'option absCoord: true pour les appels de mtgApp.addFreePoint (ça revient au même si la figure n'a pas de repère mais c'est plus prudent de le mettre explicitement) (2024-03-19 19:45) <Daniel Caillibaud>  
`* `[a57c644c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a57c644c1) màj mathgraph.d.ts (pas mal d'ajouts, notamment l'option absCoord) (2024-03-19 19:45) <Daniel Caillibaud>  
`*   `[b5fbf2891](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5fbf2891) Merge branch 'listenEnterKey' into 'main' (2024-03-15 12:13) <Jean-Claude Lhote>  
`|\  `  
`| * `[17be45807](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17be45807) ajout d'un listener sur les inputs pour 'Enter' provoque click sur button Action (2024-03-15 12:10) <Jean-claude Lhote>  
`|/  `  
`*   `[1d43db012](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d43db012) Merge branch 'espaceDansEditor' into 'main' (2024-03-13 15:55) <Jean-Claude Lhote>  
`|\  `  
`| * `[d1f693e17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1f693e17) ajout d'offset sur la grille de placement de l'éditeur (2024-03-13 15:54) <Jean-claude Lhote>  
`|/  `  
`* `[07f268e18](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07f268e18) fix restriction decimal ([fix #248](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/248)) (2024-03-13 11:56) <Daniel Caillibaud>  
`* `[ff60c7f8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff60c7f8b) loadBibli ne retourne la propriété editgraphes que sur les graphes v1 (2024-03-13 11:26) <Daniel Caillibaud>  
`* `[95f23636b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95f23636b) Remplacement de LegacyNode[] par LegacyGraph (2024-03-13 11:25) <Daniel Caillibaud>  
`* `[63b2270c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63b2270c0) editGraphe utilise upgradeParametres quand il existe (2024-03-13 11:12) <Daniel Caillibaud>  
`* `[0325c9076](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0325c9076) l'upgradeParametres des suites met aussi à jour donneesPrecedentes en booléen (il est encore une string 'j3p.parcours.donnees[n]' dans de vieux graphes) (2024-03-13 11:11) <Daniel Caillibaud>  
`* `[c668ce15d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c668ce15d) test aussi les warnings en console (2024-03-13 08:35) <Daniel Caillibaud>  
`* `[10bf19393](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10bf19393) Amélioration des tests de conversion de graphe v1 <=> v2 (2024-03-13 08:29) <Daniel Caillibaud>  
`* `[75612754c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75612754c) Amélioration de la conversion de graphe v1 <=> v2 en gérant max et maxParcours (2024-03-13 08:29) <Daniel Caillibaud>  
`* `[a2fc03ba4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2fc03ba4) aj doc sur boucle / boucleGraphe et max / maxParcours (2024-03-13 08:29) <Daniel Caillibaud>  
`* `[ccc543126](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ccc543126) code obsolète viré (2024-03-13 08:29) <Daniel Caillibaud>  
`* `[04c2d0a6d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04c2d0a6d) fix message de warning (2024-03-13 08:29) <Daniel Caillibaud>  
`* `[cb50cd45b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb50cd45b) meilleur nommage (2024-03-13 08:29) <Daniel Caillibaud>  
`* `[17a054f24](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17a054f24) fix conversion 1=>2 (2024-03-13 08:29) <Daniel Caillibaud>  
`*   `[bb210dcbe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb210dcbe) Merge branch 'typescriptInLib' into 'main' (2024-03-12 16:38) <Jean-Claude Lhote>  
`|\  `  
`| *   `[ba12db601](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba12db601) Merge branch 'main' into 'typescriptInLib' (2024-03-12 16:37) <Jean-Claude Lhote>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[364819ba5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/364819ba5) Merge branch 'v2-convert1To2-lastResultats' into 'main' (2024-03-12 16:27) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[2e350a05f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e350a05f) V2 convert1 to2 last resultats (2024-03-12 16:27) <Jean-Claude Lhote>  
`|/ /  `  
`* | `[2a1768f00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a1768f00) il ne faut pas de ref à #MepMG dans le code générique v1|v2 (2024-03-12 14:30) <Daniel Caillibaud>  
`| * `[44a58bf28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44a58bf28) V2 convert1 to2 last resultats (2024-03-12 16:34) <Jean-Claude Lhote>  
`| * `[463210f16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/463210f16) commentaire sur getElement() (2024-03-12 16:22) <Jean-claude Lhote>  
`| * `[4933b0f48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4933b0f48) draft : ajout de getElement dans src/lib/utils/dom/main.ts et suppression des imports js de VirtualKeyboard.js (2024-03-12 15:53) <Jean-claude Lhote>  
`| * `[6a420e844](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a420e844) draft : ajout de getElement dans src/lib/utils/dom/main.ts et suppression des imports js de VirtualKeyboard.js (2024-03-12 15:30) <Jean-claude Lhote>  
`|/  `  
`*   `[9ce199916](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ce199916) Merge branch 'encoreDesTodos' into 'main' (2024-03-08 16:39) <Jean-Claude Lhote>  
`|\  `  
`| * `[f7d87dbc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7d87dbc7) Encore des todos (2024-03-08 16:39) <Jean-Claude Lhote>  
`|/  `  
`*   `[3aa2fbcea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3aa2fbcea) Merge branch 'bugsnag_proconst' into 'main' (2024-03-08 11:08) <Tommy Barroy>  
`|\  `  
`| * `[a563a96d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a563a96d3) bugsnag somme angle (2024-03-08 11:06) <Tommy Barroy>  
`| * `[247b90f1b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/247b90f1b) bugsnag progconst pl (2024-03-08 10:53) <Tommy Barroy>  
`|/  `  
`* `[30d11676b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30d11676b) fix interface dev avec rid (2024-03-08 08:47) <Daniel Caillibaud>  
`* `[b88cbb82e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b88cbb82e) fix error bugsnag 65c0a6fba90cb70008c905c8 (2024-03-08 08:47) <Daniel Caillibaud>  
`* `[49bdb07a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49bdb07a6) maquait un blindage des fcts mqXxx (2024-03-08 08:47) <Daniel Caillibaud>  
`*   `[6c333760f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c333760f) Merge branch 'bugsnag_1' into 'main' (2024-03-07 17:24) <Tommy Barroy>  
`|\  `  
`| * `[9180e81e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9180e81e1) bugsnag (2024-03-07 17:22) <Tommy Barroy>  
`| * `[67e3d0932](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67e3d0932) bugsnag (2024-03-07 17:20) <Tommy Barroy>  
`| * `[72ac7b6d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72ac7b6d5) bugsnag (2024-03-07 17:00) <Tommy Barroy>  
`|/  `  
`* `[4dde09d2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4dde09d2f) retag 1.2.0 (2024-03-07 12:37) <static@sesamath-lampdev>  
`*   `[18ba30644](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/18ba30644) Merge branch 'ameliorations' into 'main' (2024-03-06 23:46) <Daniel Caillibaud>  
`|\  `  
`| * `[ac924294b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac924294b) aj de liens sur la page de dev (2024-03-06 23:45) <Daniel Caillibaud>  
`| * `[b6dce4e8f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6dce4e8f) améliorations typedoc (2024-03-06 23:45) <Daniel Caillibaud>  
`| * `[664ec96cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/664ec96cf) Ajout fct addRichElt (2024-03-06 23:45) <Daniel Caillibaud>  
`|/  `  
`*   `[986a99f7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/986a99f7e) Merge branch 'fixTestVariations' into 'main' (2024-03-06 20:31) <Jean-Claude Lhote>  
`|\  `  
`| * `[72d00f181](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72d00f181) fix: modification du test sectionEtudeVariations pour coller avec les fractions qui ont le signe devant et plus au numérateur (2024-03-06 20:30) <Jean-claude Lhote>  
`|/  `  
`*   `[5b63be830](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b63be830) Merge branch 'fixSectionExemple4' into 'main' (2024-03-06 19:39) <Jean-Claude Lhote>  
`|\  `  
`| * `[984a1cd50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/984a1cd50) nettoyage de console.warn + corrections coquilles sectionExemple4 (2024-03-06 19:38) <Jean-claude Lhote>  
`|/  `  
`*   `[b9b5b4b6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9b5b4b6f) Merge branch 'fixConvertParams' into 'main' (2024-03-06 18:25) <Jean-Claude Lhote>  
`|\  `  
`| * `[cb2be56b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb2be56b4) fix conversion des params du graphe (renommage de certains params comme nbrepetitions qui changent de nom entre v1 et v2) et renforcement du typage de ces params. (2024-03-05 21:16) <Daniel Caillibaud>  
`|/  `  
`* `[0f78ec1a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f78ec1a5) légère amélioration des tooltips, ça déconne encore (2024-03-05 21:16) <Daniel Caillibaud>  
`* `[f041ccdf8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f041ccdf8) Fix z-index (les modales doivent rester au dessus de tout le reste) (2024-03-05 21:16) <Daniel Caillibaud>  
`* `[f513a7925](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f513a7925) aj de try/catch dans les listeners (2024-03-05 21:16) <Daniel Caillibaud>  
`* `[e0c55dfa9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e0c55dfa9) fix nommage (et simplification editNode) (2024-03-05 21:16) <Daniel Caillibaud>  
`*   `[f801c4cc2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f801c4cc2) Merge branch 'Augmentation_Taille_SVG_Exos_Const' into 'main' (2024-03-05 19:46) <Yves Biton>  
`|\  `  
`| * `[cbd273ef2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cbd273ef2) Augmentation de la hauteur du svg pour certains exercices de construction (2024-03-05 19:43) <Yves Biton>  
`|/  `  
`*   `[37dfdbddf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37dfdbddf) Merge branch 'JC-correction-de-coquilles' into 'main' (2024-03-05 15:44) <Jean-Claude Lhote>  
`|\  `  
`| * `[1f46d524a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f46d524a) Correction du player (step double incrémentation et initialisation à 1) (2024-03-05 15:43) <Jean-claude Lhote>  
`| * `[66a584e4c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66a584e4c) corrections dans sectionExemple1 et ajout des bons paramètres dans sectionExemple4. (2024-03-05 15:43) <Jean-claude Lhote>  
`|/  `  
`*   `[6639a97f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6639a97f3) Merge branch 'bugsnag_delete' into 'main' (2024-03-05 14:29) <Tommy Barroy>  
`|\  `  
`| * `[b32709190](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b32709190) bugsnag (2024-03-05 13:53) <Tommy Barroy>  
`| * `[9e2be4c0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e2be4c0c) bugsnag (2024-03-05 13:53) <Tommy Barroy>  
`| * `[4aafd52d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4aafd52d9) bugsnag (2024-03-05 13:53) <Tommy Barroy>  
`|/  `  
`* `[50d8e197b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50d8e197b) player.d.ts renommé en types.d.ts car windows s'emmêlait les pinceaux (il cherchait le contenu dans Player.ts) (2024-03-05 13:22) <Daniel Caillibaud>  
`* `[6d23af1a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d23af1a4) Aj d'une action init à l'éditeur v1, mais ça corrige pas le bug de l'éditeur HS en dev quand on change d'onglet et qu'on revient sur l'éditeur v1 (2024-03-05 13:18) <Daniel Caillibaud>  
`* `[db26f0d9e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db26f0d9e) aj d'un bind sur logIfDebug pour pouvoir l'utiliser directement (avec du { logIfDebug } = pathway) (2024-03-05 12:08) <Daniel Caillibaud>  
`*   `[dc7fd5750](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc7fd5750) Merge branch 'JC-todo' into 'main' (2024-03-05 11:47) <Jean-Claude Lhote>  
`|\  `  
`| * `[909fa1272](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/909fa1272) correction coquille dans start.form.ts + correction usage logIfDebug dans sectionExemple3 (2024-03-05 11:38) <Jean-claude Lhote>  
`| * `[f707ae234](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f707ae234) ajout du compteur de Sections (cumul des nbRuns +1) dans showStatus (2024-03-05 10:44) <Jean-claude Lhote>  
`| * `[ec0f4c0a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec0f4c0a7) quelques todo réglés + ajout d'un texte générique 'bug' pour le Player.run() en cas d'erreur. (2024-03-04 18:58) <Jean-claude Lhote>  
`|/  `  
`*   `[3eb8ae489](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3eb8ae489) Merge branch '246-probleme-dans-sectionetudefonction_derivee-la-presence-de-facteurs-negatif-pour-le-modele-3-est' into 'main' (2024-03-04 17:57) <Jean-Claude Lhote>  
`|\  `  
`| * `[999ea4faf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/999ea4faf) sectionEtudeFonction_derivee.js : case 3 : le test de la présence de facteurs négatif est corrigé. (2024-03-04 17:56) <Jean-claude Lhote>  
`|/  `  
`*   `[ec1bc2cb1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec1bc2cb1) Merge branch '241-noeud-fin-vu-comme-non-fait-dans-showpathway' into 'main' (2024-03-04 17:51) <Jean-Claude Lhote>  
`|\  `  
`| * `[1f763c441](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f763c441) Les noeuds 'fin' ne sont plus considérés comme non-faits (2024-03-04 17:49) <Jean-claude Lhote>  
`|/  `  
`*   `[c725fc672](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c725fc672) Merge branch 'playground-dialog' into 'main' (2024-03-04 16:51) <Jean-Claude Lhote>  
`|\  `  
`| * `[1e6f50eb2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e6f50eb2) implémentation Playground.dialog() (2024-03-04 16:43) <Jean-claude Lhote>  
`|/  `  
`*   `[277b1f519](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/277b1f519) Merge branch 'JC-fix-fractions-dans-variations' into 'main' (2024-03-04 15:27) <Jean-Claude Lhote>  
`|\  `  
`| * `[507d9849a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/507d9849a) #247 : un problème de comparaison de fractions qui sont pas écrites pareil... (2024-03-04 15:27) <Jean-Claude Lhote>  
`|/  `  
`* `[2598e64ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2598e64ce) fix lancement depuis l'éditeur (en dev) (2024-03-02 16:23) <Daniel Caillibaud>  
`* `[7a42f533e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a42f533e) blindage (fix bugsnag 65c6a454da7f4d0007c2118f) (2024-03-01 19:17) <Daniel Caillibaud>  
`* `[5c12991d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c12991d2) Amélioration éditeur (2024-03-01 19:06) <Daniel Caillibaud>  
`* `[c776a1265](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c776a1265) Editor : Fix clipboard copy et amélioration de ConnectorJson (pas besoin de source, seulement les propriétés pertinentes pour le type de connecteur sont exportées) (2024-03-01 18:29) <Daniel Caillibaud>  
`* `[1df279d88](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1df279d88) Ajout d'icônes aux menus contextuels de l'éditeur (2024-03-01 18:23) <Daniel Caillibaud>  
`* `[63a7122b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63a7122b6) L'éditeur de graphe v1 ne supporte pas les id de node non numériques, on s'adapte… (2024-03-01 16:35) <Daniel Caillibaud>  
`* `[2e1e2810d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e1e2810d) séparation jsdoc/tsdoc (2024-03-01 13:52) <Daniel Caillibaud>  
`* `[85480bfd9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85480bfd9) Ajout de la date (en string) à chaque Result (2024-03-01 13:52) <Daniel Caillibaud>  
`* `[98cb364af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98cb364af) distinction entre les type XxxSerialized et les XxxJson (les seconds n'ont pas d'id et ont des dates en string) - [fix #244](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/244) (2024-03-01 13:52) <Daniel Caillibaud>  
`* `[d4732170e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4732170e) retag 2.0.3 (2024-03-01 11:19) <static@sesamath-lampdev>  
`* `[8460a33c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8460a33c4) Le player v2 gère isDebug, et l'info est propagée depuis l'iframe (aussi vers le player v1) (2024-03-01 11:16) <Daniel Caillibaud>  
`* `[12e4223f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12e4223f6) Ajout d'une case à cocher pour la bascule du mode debug (2024-03-01 10:31) <Daniel Caillibaud>  
`* `[ab15be877](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab15be877) bla bla dans l'iframe (avant / après conteneur) seulement en mode debug (2024-03-01 10:02) <Daniel Caillibaud>  
`* `[ecd352437](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecd352437) Ajout du cas où la section veut gérer elle-même la validation de la question (pas de bouton OK mais un message disant le lire la consigne pour valider) + spinner d'attente dans zoneActions (2024-03-01 09:57) <Daniel Caillibaud>  
`* `[b9d443066](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9d443066) Ajout d'un divWaiting (2024-03-01 08:52) <Daniel Caillibaud>  
`* `[b05d5a94d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b05d5a94d) bugfix affichage du message pas de réponse (qui s'affichait toujours quand isAnswered était async) (2024-03-01 08:32) <Daniel Caillibaud>  
`* `[e811dbd74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e811dbd74) LegacySectionRunner n'a pas besoin du contexte complet (2024-03-01 07:51) <Daniel Caillibaud>  
`* `[39db9f466](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39db9f466) Récup boutons v1 (2024-03-01 07:33) <Daniel Caillibaud>  
`* `[9a1073c44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a1073c44) Ajout du logo dans le titre (2024-03-01 06:56) <Daniel Caillibaud>  
`* `[0a7c8d08c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a7c8d08c) màj .gitignore (aj html buildés) (2024-03-01 06:23) <Daniel Caillibaud>  
`* `[ac318480c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac318480c) retag 1.2.0 (2024-03-01 06:06) <Daniel Caillibaud>  
`* `[abc4c8d04](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/abc4c8d04) aj FIXME dans sectionequerre01 (2024-02-29 20:12) <Daniel Caillibaud>  
`*   `[e02187c5c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e02187c5c) Merge branch 'v2' (2024-02-29 20:08) <Daniel Caillibaud>  
`|\  `  
`| *   `[98fd8e4cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98fd8e4cc) Merge branch 'v2-betterDisplay' into 'v2' (2024-02-29 19:37) <Daniel Caillibaud>  
`| |\  `  
`| | * `[202901845](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/202901845) amélioration typage (2024-02-29 19:34) <Daniel Caillibaud>  
`| | * `[36cb4f08c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/36cb4f08c) il faut imposer nbQuestions à 1 (2024-02-29 19:33) <Daniel Caillibaud>  
`| | * `[7365a173c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7365a173c) Ajout de texte bidon dans l'iframe, avant et après notre conteneur. (2024-02-29 19:18) <Daniel Caillibaud>  
`| | * `[610b200b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/610b200b4) fix sectionChoisisTaFonction (ramenée par un rebase v2) qui passe pas les nouvelles règles (2024-02-29 19:04) <Daniel Caillibaud>  
`| | * `[57b38255c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57b38255c) On remet le feedback à coté du bouton en bas à droite (2024-02-29 18:49) <Daniel Caillibaud>  
`| | * `[d96bcf448](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d96bcf448) Le player utilise les textes génériques (2024-02-29 18:49) <Daniel Caillibaud>  
`| | * `[fffb5230c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fffb5230c) faut au minimum un titre aussi après conversion (2024-02-29 18:49) <Daniel Caillibaud>  
`| | * `[d9903782a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9903782a) feedback au-dessus du titre, qui disparait au 1er click (2024-02-29 18:49) <Daniel Caillibaud>  
`| | * `[0778cca7b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0778cca7b) factorisation renderMathInElement (2024-02-29 18:49) <Daniel Caillibaud>  
`| | * `[87a119ec3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87a119ec3) Le titre est un paramètre obligatoire (2024-02-29 18:49) <Daniel Caillibaud>  
`| | * `[0b0621c42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b0621c42) harmonisation signature section.init() (2024-02-29 18:49) <Daniel Caillibaud>  
`| | * `[ff5da3eab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff5da3eab) SectionContext.ts initialise les valeurs des paramètres en contrôlant leur type (en utilisant une màj de la section si y'en a une) (2024-02-29 18:49) <Daniel Caillibaud>  
`| | * `[c77818b6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c77818b6a) fix signature de upgradeParametres (2024-02-29 18:49) <Daniel Caillibaud>  
`| | * `[833109c59](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/833109c59) Le player doit passer par le constructeur de SectionContext (et lui déléguer le contrôle des valeurs) (2024-02-29 18:49) <Daniel Caillibaud>  
`| | * `[d66cfae9b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d66cfae9b) fix validate qui est une fct et pas une string (2024-02-29 18:48) <Daniel Caillibaud>  
`| | * `[654e06763](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/654e06763) Pathway.resultatListener passe en private (2024-02-29 18:48) <Daniel Caillibaud>  
`| | * `[f826a513d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f826a513d) fix showStatus (pb sur le n° de section à implémenter) (2024-02-29 18:48) <Daniel Caillibaud>  
`| | * `[91d63cc3b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/91d63cc3b) Playground.addDiv viré, les autres addXxx passent en private (2024-02-29 18:48) <Daniel Caillibaud>  
`| | * `[5c1153f31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c1153f31) aj lorem ipsum pour allonger la taille occupée (2024-02-29 18:48) <Daniel Caillibaud>  
`| | * `[5db2d19d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5db2d19d5) sass veut l'unité % même avec 0 (2024-02-29 18:48) <Daniel Caillibaud>  
`| | * `[80f7372f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80f7372f8) simplification des DomElt (avec ajout de l'interface générique Item) (2024-02-29 18:48) <Daniel Caillibaud>  
`| | * `[75273e8d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75273e8d3) ajout de l'option onChange à ListeDeroulante (2024-02-29 18:48) <Daniel Caillibaud>  
`| | * `[b89e83cd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b89e83cd0) amélioration css (2024-02-29 18:48) <Daniel Caillibaud>  
`| |/  `  
`| *   `[63f8c5f90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63f8c5f90) Merge branch 'v2-problemeDeLimites' into 'v2' (2024-02-27 19:53) <Jean-Claude Lhote>  
`| |\  `  
`| | * `[35b4233be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35b4233be) graphe2 Ok pour l'enchainement des etudes de fonctions (2024-02-27 19:53) <Jean-Claude Lhote>  
`| |/  `  
`| *   `[29a7b5c83](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/29a7b5c83) Merge branch 'v2-donneesPersistantes' into 'v2' (2024-02-27 13:00) <Jean-Claude Lhote>  
`| |\  `  
`| | * `[738903ce1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/738903ce1) Données persistantes OK jusqu'au bout du graphe. (2024-02-27 12:58) <Jean-claude Lhote>  
`| * |   `[75f0314d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75f0314d2) Merge branch 'v2-donneesPersistantes' into 'v2' (2024-02-27 10:57) <Jean-Claude Lhote>  
`| |\ \  `  
`| | * \   `[05cbb9829](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05cbb9829) Merge branch 'v2' into 'v2-donneesPersistantes' (2024-02-27 10:57) <Jean-Claude Lhote>  
`| | |\ \  `  
`| | |/ /  `  
`| |/| /   `  
`| | |/    `  
`| * | `[e624f7457](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e624f7457) fix affichage liste (2024-02-27 10:26) <Daniel Caillibaud>  
`| * | `[b410fb7ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b410fb7ec) fix NodeContextMenu qui marchait plus, et distinction entre GraphNodeActiveSerialized et GraphNodeEndSerialized (qui a moins de props) (2024-02-23 20:31) <Daniel Caillibaud>  
`| * | `[21c25d783](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/21c25d783) showError mutualisé (2024-02-23 18:58) <Daniel Caillibaud>  
`| * | `[2506eecf3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2506eecf3) [fix #243](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/243) (2024-02-23 15:53) <Daniel Caillibaud>  
`| * | `[48c9655cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48c9655cc) [fix #242](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/242) (2024-02-23 13:29) <Daniel Caillibaud>  
`| * | `[2ef92bc8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ef92bc8d) ajout typedoc (2024-02-23 13:03) <Daniel Caillibaud>  
`| * | `[3c8c4dfb5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c8c4dfb5) fix scope (2024-02-23 12:42) <Daniel Caillibaud>  
`| * | `[00812d226](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00812d226) fix HOST & PORT fixé à pnpm start si besoin (2024-02-23 12:42) <Daniel Caillibaud>  
`| | * `[1170df504](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1170df504) fix affichage liste (2024-02-27 10:50) <Daniel Caillibaud>  
`| | * `[0ab4b58f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ab4b58f9) Ajout de sectionEtudeFonction_limite... qui plante (2024-02-26 16:57) <Jean-claude Lhote>  
`| | * `[833fec47d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/833fec47d) Ajout des noeuds signederivee et variations au graph2. ça se passe pas bien pour l'enchaînement 3->4 (2024-02-26 16:14) <Jean-claude Lhote>  
`| | * `[6cc772111](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cc772111) Ajout commentaire JSdoc sur convertParametersBack (2024-02-26 15:23) <Jean-claude Lhote>  
`| | * `[7eb2d7d0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7eb2d7d0e) Ajout commentaire JSdoc sur LegacySectionRunner.setParams() (2024-02-26 15:19) <Jean-claude Lhote>  
`| | * `[d7f7bb52f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7f7bb52f) pour quelques warning en moins (2024-02-26 15:14) <Jean-claude Lhote>  
`| | * `[2e37ed626](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e37ed626) Des données persistantes sur le graphe2 (2024-02-26 15:09) <Jean-claude Lhote>  
`| | * `[d4f8c4008](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4f8c4008) draft: ajout de donneesPersistantes dans SectionContext (2024-02-26 10:17) <Jean-claude Lhote>  
`| | * `[a12311071](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a12311071) draft: ajout d'une section pour choisir une fonction parmi 3 possibles (modification du graphe2). (2024-02-26 09:55) <Jean-claude Lhote>  
`| | * `[2123eac5e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2123eac5e) fix NodeContextMenu qui marchait plus, et distinction entre GraphNodeActiveSerialized et GraphNodeEndSerialized (qui a moins de props) (2024-02-26 09:54) <Daniel Caillibaud>  
`| | * `[bbb3d0adc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bbb3d0adc) showError mutualisé (2024-02-26 09:53) <Daniel Caillibaud>  
`| | * `[03cb487dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03cb487dd) [fix #243](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/243) (2024-02-26 09:51) <Daniel Caillibaud>  
`| | * `[ba83eea0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba83eea0e) [fix #242](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/242) (2024-02-26 09:51) <Daniel Caillibaud>  
`| | * `[a8a1c2b9f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8a1c2b9f) ajout typedoc (2024-02-26 09:49) <Daniel Caillibaud>  
`| | * `[78e57e31b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/78e57e31b) fix scope (2024-02-26 09:49) <Daniel Caillibaud>  
`| | * `[bf352c826](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf352c826) fix HOST & PORT fixé à pnpm start si besoin (2024-02-26 09:49) <Daniel Caillibaud>  
`| | * `[8f4071530](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f4071530) draft: ajout d'une section pour choisir une fonction parmi 3 possibles (modification du graphe2). (2024-02-23 12:23) <Jean-claude Lhote>  
`| | * `[02c55a137](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02c55a137) draft: ajout d'une section pour choisir une fonction parmi 3 possibles (modification du graphe2). (2024-02-23 12:20) <Jean-claude Lhote>  
`| | * `[87456a0da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87456a0da) rectification N° exemple  des 2 items via lastResultat sur l'interface start.form.ts (2024-02-23 08:54) <Jean-claude Lhote>  
`| |/  `  
`| * `[491dfd353](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/491dfd353) suppression de @ts-expect-error et @ts-ignore (2024-02-22 20:22) <Daniel Caillibaud>  
`| *   `[2feaa433b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2feaa433b) Merge branch 'v2-feedbacks' into 'v2' (2024-02-22 19:02) <Jean-Claude Lhote>  
`| |\  `  
`| | *   `[6efe4932b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6efe4932b) Merge branch 'v2' into 'v2-feedbacks' (2024-02-22 19:00) <Jean-Claude Lhote>  
`| | |\  `  
`| | |/  `  
`| |/|   `  
`| * | `[2c3e010f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c3e010f4) fix splitNode (2024-02-22 16:48) <Daniel Caillibaud>  
`| * | `[550011655](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/550011655) noImplicitAny passe à true ! (2024-02-22 16:20) <Daniel Caillibaud>  
`| * | `[f31187255](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f31187255) amélioration typage dans basthon (2024-02-22 16:19) <Daniel Caillibaud>  
`| * | `[8909d0a6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8909d0a6e) refacto chargement des claviers mathlive (ça devient dynamique à la demande), fixes types au passage (2024-02-22 16:15) <Daniel Caillibaud>  
`| * | `[c407f1f90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c407f1f90) suite amélioration typage (2024-02-22 15:26) <Daniel Caillibaud>  
`| | * `[1d8f11979](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d8f11979) petits ajustements suite au rebase (2024-02-22 18:43) <Jean-claude Lhote>  
`| | * `[bbb453dc1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bbb453dc1) feedbacks individuels ajoutés sur DomEltRadios (exemple1 question 2) (2024-02-22 18:36) <Jean-claude Lhote>  
`| | * `[4fe9726a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fe9726a5) virer frozen des Playground.Elements (2024-02-22 18:36) <Jean-claude Lhote>  
`| | * `[4ee36886a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ee36886a) fix splitNode (2024-02-22 18:30) <Daniel Caillibaud>  
`| | * `[38bb592dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38bb592dc) noImplicitAny passe à true ! (2024-02-22 18:30) <Daniel Caillibaud>  
`| | * `[b9e740ade](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9e740ade) amélioration typage dans basthon (2024-02-22 18:30) <Daniel Caillibaud>  
`| | * `[d93e52c62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d93e52c62) refacto chargement des claviers mathlive (ça devient dynamique à la demande), fixes types au passage (2024-02-22 18:30) <Daniel Caillibaud>  
`| | * `[c69c9d795](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c69c9d795) suite amélioration typage (2024-02-22 18:22) <Daniel Caillibaud>  
`| | * `[010118825](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/010118825) feedbacks individuels ajoutés sur DomEltRadios (exemple1 question 2) (2024-02-22 16:08) <Jean-claude Lhote>  
`| | * `[89a0f66e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89a0f66e5) virer frozen des Playground.Elements (2024-02-22 15:26) <Jean-claude Lhote>  
`| | * `[eafba023f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eafba023f) virer frozen des DomElt, le freeze est géré par le player sur les éléments HTML (2024-02-22 14:41) <Jean-claude Lhote>  
`| |/  `  
`| * `[73e9430f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/73e9430f5) nombreux fixes de typage (2024-02-22 14:00) <Daniel Caillibaud>  
`| *   `[a83a72afb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a83a72afb) Merge branch 'v2-css' into v2 (2024-02-21 20:33) <Daniel Caillibaud>  
`| |\  `  
`| | * `[e35cc857e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e35cc857e) fix tsc (après rebase sur v2) (2024-02-21 20:25) <Daniel Caillibaud>  
`| | * `[b850df7d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b850df7d6) début de cosmétique sur le player (2024-02-21 20:16) <Daniel Caillibaud>  
`| | * `[68297832d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68297832d) modif cosmétique mineure (2024-02-21 20:16) <Daniel Caillibaud>  
`| | * `[6cc1a79fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cc1a79fb) aj de box-shadow sur l'éditeur (2024-02-21 20:16) <Daniel Caillibaud>  
`| | * `[086e499fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/086e499fe) normalisation des couleurs de spStart (peut mieux faire… mais au moins c'est centralisé dans base.scss) (2024-02-21 20:16) <Daniel Caillibaud>  
`| | * `[e1005ae55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1005ae55) fix séparation css entre editor & player (2024-02-21 20:16) <Daniel Caillibaud>  
`| | * `[7f24427c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f24427c0) normalisation des couleurs de l'éditeur (peut mieux faire… mais au moins c'est centralisé dans base.scss) (2024-02-21 20:16) <Daniel Caillibaud>  
`| | * `[660d720d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/660d720d4) fix indentation (2024-02-21 20:14) <Daniel Caillibaud>  
`| | * `[ecc2cfe68](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecc2cfe68) l'iframe prend toute la hauteur restante (2024-02-21 20:14) <Daniel Caillibaud>  
`| | * `[89a647ebf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89a647ebf) harmonisation des pictos de l'éditeur (2024-02-21 20:14) <Daniel Caillibaud>  
`| | * `[8590d1864](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8590d1864) amélioration cosmétique éditeur (plus de surcharge de style entre v1 et v2) (2024-02-21 20:14) <Daniel Caillibaud>  
`| | * `[45135cd5f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45135cd5f) ajout SceneUI.autosize() (pour éviter que les nodes en absolute ne sortent du conteneur) (2024-02-21 20:13) <Daniel Caillibaud>  
`| | * `[10b7c4557](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10b7c4557) 1er jet (2024-02-21 20:13) <Daniel Caillibaud>  
`| | * `[7de71177b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7de71177b) Aj math-field comme custom html tag dans la config webstorm (2024-02-21 20:13) <Daniel Caillibaud>  
`| |/  `  
`| * `[4542c3b71](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4542c3b71) fix plantage tsc (introduit dans le merge précédent) (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[3bfb0d9bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3bfb0d9bb) problème de descendance sur le build entre ContextMenu et SceneContextMenu (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[53dacae5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53dacae5a) problème de descendance sur le build entre ContextMenu et NodeContextMenu (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[ebc48b9a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ebc48b9a1) rectification du display de la figure Mtg32. (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[7a2c14ef8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a2c14ef8) noAny false pour push (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[a6bb66d47](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6bb66d47) typage LegacySectionRunner.ts (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[f66035d50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f66035d50) typage Player.ts (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[c6aaabf55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6aaabf55) typage Playground.ts (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[61e3744c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61e3744c2) typage InputMtg.ts et Playground.ts (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[fa6d4b6ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa6d4b6ae) typage lib/utils/array.ts (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[5026ca72e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5026ca72e) typage lib/utils/comparators.ts (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[cf4362f9d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf4362f9d) typage lib/utils/css.ts (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[657765be3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/657765be3) typage lib/utils/debug.ts (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[31f383a8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31f383a8e) typage lib/utils/dom/main.ts (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[4fed00ffd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fed00ffd) typage object.ts (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[c071545e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c071545e1) fix un opérateur ?? inutile dans checks.ts (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[dc19aa077](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc19aa077) Changes by daniel (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[b17912abc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b17912abc) Encore du typage (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[894df0b9a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/894df0b9a) Encore du typage. (2024-02-21 20:09) <Jean-claude Lhote>  
`| * `[436575812](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/436575812) fix testBrowser (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[cb1a05b5e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb1a05b5e) Fin refacto start (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[092dbbd69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/092dbbd69) plus d’avertissement pour les throw catch locally (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[6555ba2c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6555ba2c7) Draft toujours, mais ça avance… (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[c1caae14f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c1caae14f) instanceJsPlumb.draggable(nodeElt) plante parfois si on lui passe pas un objet vide d'options en 2e argument (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[fdcb0025b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fdcb0025b) Fix tests (les sections doivent désormais exister pour passer le validate) (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[3042d963b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3042d963b) Draft suite refacto start, échanges ok avec l'iframe (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[a497f9144](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a497f9144) modifs de forme pour ts (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[adbdd6a26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/adbdd6a26) fix imports (src/ qui manquaient) (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[34690817e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34690817e) aj du chargement de jQueryUi et des outils dans LegacySectionRunner.ts (idem j3pLoad) (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[221d07d4a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/221d07d4a) convertGraphBack devient async (il fait du graph.validate) (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[d89ff1547](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d89ff1547) bugfix graph.validate (il virait le startingId) (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[3b9381d55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b9381d55) Il faut imposer node.id dans le constructeur de Graph (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[b07bc7fb0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b07bc7fb0) startingId mis d'office dans les graphes à un seul nœud non fin (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[cef6e537e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cef6e537e) fix types (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[3a253684a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a253684a) Draft: premier jet de refacto de start (ex spStart ou j3pStart) (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[688f792e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/688f792e5) une version de start.runners.ts qui gère le param ?testBrowser=1 (à priori inutile car doublon avec ?wait) (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[43efc7f4f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43efc7f4f) améliorations code style ts pour webstorm (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[fec09b164](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fec09b164) fix imports auto qui démarraient pas par 'src/' (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[5dde2e4dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5dde2e4dd) fix import path (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[e9ed323bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9ed323bf) améliorations du type checking (2024-02-21 20:09) <Daniel Caillibaud>  
`| * `[c2b4a81fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2b4a81fd) Ajout des types StyleObject/StyleProp/StyleValue (2024-02-21 20:08) <Daniel Caillibaud>  
`| * `[d01e969d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d01e969d6) encore un type MathJson (relatif à compute-engine) viré (2024-02-21 20:08) <Daniel Caillibaud>  
`| * `[d6918bb2a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d6918bb2a) type MathJson (relatif à compute-engine) viré (2024-02-21 20:08) <Daniel Caillibaud>  
`| * `[20b5236a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20b5236a2) on déclare nos var globales (2024-02-21 20:08) <Daniel Caillibaud>  
`| * `[a873fcc96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a873fcc96) lintage resultatFormatter.ts, checks.ts Graph.ts et GraphNode.ts (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[00773c42d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00773c42d) on déclare nos var globales (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[7cf39e7f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7cf39e7f4) spEditGraph en ts (avec ajouts de types) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[8cc0a4832](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cc0a4832) aj @types/jqueryui (change rien aux warnings dans loadJqueryDialog.ts) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[b4517bace](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4517bace) bascule du noImpliciteAny à false pour pouvoir push. Problème avec convert1To2 sur le type de parameter.validate et parameter.multiEditor (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[0dc9be152](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0dc9be152) bascule du noImpliciteAny à false pour pouvoir push. (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[6627b59a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6627b59a5) lintage resultatFormatter.ts, checks.ts Graph.ts et GraphNode.ts (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[1bbdededb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1bbdededb) lintage displayForms.ts avec 2 ts-ignore :-( (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[98292859d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98292859d) lintage buildUsageTree.ts & domHelpers.ts (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[57f31b2fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57f31b2fd) noImplicitAny à true ... ça balance ! (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[0a46acd90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a46acd90) lintage buildStats.ts et checkGraphV1.ts (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[270d94996](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/270d94996) fix import paths (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[5f4260b01](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f4260b01) fix conflit (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[a3e492680](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3e492680) il vaut mieux faire le preventDefault() dans un listener sync (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[f9b8e69ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f9b8e69ba) Ajout d'un showPathway V2 à displayForm.ts (positionnement des noeuds) (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[7d9091bea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d9091bea) La sauvegarde des résultats se fait après la recherche du nextId pour que currentNodeId pointe sur le prochain noeud. (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[1a2b07b55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a2b07b55) ajouter j3pEmpty() dans j3pPlay() (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[cdd39f8d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cdd39f8d3) modifier le lien player(lastResutat) avec j3pStart bonne idée ? (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[f722b92a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f722b92a3) Incrémente le nbRuns[currentNodeId] dans Pathway.addResult() (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[29223a270](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/29223a270) fix typapge (notamment aj PathwaySerialized.startedAt) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[b02ffd1e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b02ffd1e0) typage j3pStart (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[b64f9eb75](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b64f9eb75) dom/index => dom/main (pour éviter que ça râle sur l'implicite ou pas) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[321984013](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/321984013) harmonisation des fonctions globales pour lancer sesaparcours (player/editor/showPathway) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[16bf34ef1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16bf34ef1) fix syntax error css (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[1eaee5d15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1eaee5d15) stats.html est désormais géré par vite (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[10cdb6ef7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10cdb6ef7) liste.html est désormais géré par vite (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[d8e1b6791](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8e1b6791) réorganisation des fcts exportées par ordre alphabétique (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[e596999fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e596999fd) normalisation du chargement (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[9a6ae6bf8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a6ae6bf8) Graph n'a qu'un export par défaut (plus d'export nommé) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[3141ea0b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3141ea0b7) Ajout de startedAt (string) dans PathwaySerialized (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[0b7afb2e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b7afb2e2) amélioration j3pStart (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[78360e1a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/78360e1a4) récup d'exemples oubliés en stash (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[b79ae0db2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b79ae0db2) fix lancement du graphe depuis l'éditeur (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[9078b1d2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9078b1d2d) implémentation de pathway.serialize() (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[5abc09407](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5abc09407) Draft: qq améliorations mais faut virer tous les appels de sectionSuivante() dans les sections. (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[a8ab720f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8ab720f2) fix syntax pb (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[87c8c4a47](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87c8c4a47) Result convertit un score nullish en -1 (pour les sections sans score qui retournent undefined) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[c3fff30bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c3fff30bb) Il faut préciser des choix à la section choix (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[0dd6862f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0dd6862f0) fix section Exemple foireuse (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[07754d022](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07754d022) Draft: ça charge une section v1, reste à enchaîner (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[383f17b39](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/383f17b39) .zoneKeyboard viré (plus utilisé) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[52db053c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52db053c2) Le #j3pContainer devient #j3pDevContainer (pour la home j3p) pour éviter toute confusion avec .j3pContainer (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[dfb89620e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfb89620e) 1er jet (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[01e875d1c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01e875d1c) simplification du test dans Graph._nextId (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[6f6bb1386](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f6bb1386) un graph.nodes.get pas encore transformé en graph.getNode dans menu.ts (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[7c3be7300](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c3be7300) transformation de Graph.nodes en Record<string,GraphNode> à la place de Map<string, GraphNode> (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[ea4d6ce84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea4d6ce84) Suppression d'un await click en trop Remplacement 'Suite' par 'Question suivante' (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[e4eb0a6f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4eb0a6f0) Correction Pathway.isFinished Remplacement dans Pathway de currentNode par currentNodeId Ajout de Pathway.getCurrentNode() (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[37cac8371](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37cac8371) manquait un alias (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[930e22c61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/930e22c61) player.run() ne retourne plus rien, il faut passer un resultatListener au play() pour récupérer les résultats (intermédiaires et final) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[eb75635f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb75635f0) fonctions de conversion 1to2 toujours chargées dynamiquement (pour ne pas les ajouter d'office au bundle js v2) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[e7978959f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e7978959f) exemple de resultat mis dans fixture (+doc) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[5851671d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5851671d6) rationalisation des tableMathliveXx (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[1710574f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1710574f5) import type mis pour tous les imports de type (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[79a405bff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79a405bff) persistentStorage ajouté au pathway (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[6cee42104](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cee42104) SectionContext devient une classe (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[114cdec7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/114cdec7d) Plus d'itemStart dans SectionContext, c'est la section qui fait du setFocus si elle veut (plutôt que de modifier SectionContext). (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[8fb411fb7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8fb411fb7) fix appel enonce à chaque étape (et pas seulement à chaque question) + rationalisation des types (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[a4495abe6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a4495abe6) simplification runOneStep (renommé en runSteps car il les joue tous) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[4f75eb9ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f75eb9ff) evaluation (ex pe) n'est pas un feedback à afficher (c'est une "note" qualitative qui n'est pas destinée à l'élève mais aux branchements) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[b2576e7b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2576e7b7) fix évaluation isAnswered (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[b0e58760f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0e58760f) mutualisation de texte avec v1 (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[79fb30a6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79fb30a6f) police mousememoirs virée (servait que pour le titre sans être systématique) (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[8cbccefb2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cbccefb2) fix: les noeuds fin ne sont plus 'paramétrables' ni 'testables' (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[fa5058825](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa5058825) màj deps (2024-02-21 20:07) <Daniel Caillibaud>  
`| * `[b8dc30916](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8dc30916) fix: lintage (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[ca750e2d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca750e2d7) fix: Score prend un 's' uniquement s'il y en a plusieurs (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[0541e57f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0541e57f8) fix: suppression de la flèche en trop sur les branchements (modification des scores pour apporter de la couleur) (2024-02-21 20:07) <Jean-claude Lhote>  
`| * `[bdeb940a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bdeb940a6) fusion de showParcours dans v2 (2024-02-21 20:07) <Jean-Claude Lhote>  
`| * `[31405e94b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31405e94b) Squash de tous les commits de la branche v2 antérieurs à 2023-01-30 (2024-02-21 20:07) <Daniel Caillibaud>  
`* | `[4a0b83e57](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a0b83e57) scripts/genGraphAlea.js prend désormais un fichier en argument (plus simple à éditer dans un json, avec messages d'erreur plus détaillés (2024-02-27 09:45) <Daniel Caillibaud>  
`* | `[0748feaa4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0748feaa4) aj message d'erreur si un seul node fourni (2024-02-26 16:14) <Daniel Caillibaud>  
`* |   `[0a2e13199](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a2e13199) Merge branch 'vire_anime' into 'main' (2024-02-26 14:48) <Tommy Barroy>  
`|\ \  `  
`| * | `[1a2520836](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a2520836) vire anime (2024-02-26 14:47) <Tommy Barroy>  
`* | | `[1daff251d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1daff251d) retag 2.0.3 (2024-02-25 08:38) <static@sesamath-lampdev>  
`* | |   `[49dc4f815](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49dc4f815) Merge branch 'Modif_Mineure_Section_Construction' into 'main' (2024-02-24 22:54) <Yves Biton>  
`|\ \ \  `  
`| * | | `[b09fe8431](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b09fe8431) Correction suite rapport d'erreur LaboMep. Le symbole de réunion n'était pas disponible même si dans les paramètres (2024-02-24 22:51) <Yves Biton>  
`* | | | `[edea55d15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edea55d15) retag 2.0.3 (2024-02-24 21:55) <static@sesamath-lampdev>  
`| |/ /  `  
`|/| |   `  
`* | |   `[5c135acf9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c135acf9) Merge branch 're_ellever_un_bloc' into 'main' (2024-02-23 06:44) <Tommy Barroy>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[df1e07e8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df1e07e8e) remis un bloc enlevé (2024-02-23 06:43) <Tommy Barroy>  
`* | | `[bd552b059](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd552b059) retag 2.0.3 (2024-02-20 18:00) <static@sesamath-lampdev>  
`| |/  `  
`|/|   `  
`* |   `[a657031a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a657031a1) Merge branch 'Correction_Multi_Etapes_Symbole_Union' into 'main' (2024-02-20 17:25) <Yves Biton>  
`|\ \  `  
`| * | `[cce5204bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cce5204bf) Correction suite rapport d'erreur LaboMep. Le symbole de réunion n'était pas disponible même si dans les paramètres (2024-02-20 17:20) <Yves Biton>  
`* | | `[fea986d94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fea986d94) fix scripts/genGraphAlea.js (import vs require) (2024-02-20 07:53) <Daniel Caillibaud>  
`* | | `[e2ef32365](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2ef32365) retag 2.0.3 (2024-02-19 10:19) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[3f36764f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f36764f0) Merge branch 'Correction_Squelette_Multi_Etapes' into 'main' (2024-02-19 09:49) <Yves Biton>  
`|\ \  `  
`| * | `[8f4224f11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f4224f11) Correction suite rapport d'erreur LaboMep. Ce squelette ne fonctionnait plus suite à modifs Daniel je pense. (2024-02-19 09:47) <Yves Biton>  
`* | | `[738157520](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/738157520) retag 2.0.3 (2024-02-19 07:16) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[f330a4cb5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f330a4cb5) Merge branch 'Correction_Exo_Primitive' into 'main' (2024-02-18 23:29) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[215567186](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/215567186) Correction suite rapport d'erreur LaboMep. Cet exercice ne fonctionnait plus. (2024-02-18 23:27) <Yves Biton>  
`* |   `[573a58e78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/573a58e78) Merge branch 'bugsnag_A_undefined' into 'main' (2024-02-17 15:50) <Tommy Barroy>  
`|\ \  `  
`| * | `[f1408146b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1408146b) bugsnag (2024-02-17 15:47) <Tommy Barroy>  
`* | | `[6eb5bc757](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6eb5bc757) retag 2.0.3 (2024-02-16 13:19) <static@sesamath-lampdev>  
`| |/  `  
`|/|   `  
`* |   `[a96410718](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a96410718) Merge branch 'Correction_ModeDys' into 'main' (2024-02-16 12:34) <Yves Biton>  
`|\ \  `  
`| * | `[4b4e46262](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b4e46262) minuscule au lieu de majuscule pour le paramètre modeDys (2024-02-16 12:32) <Yves Biton>  
`* | | `[e1fe9ef9e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1fe9ef9e) retag 2.0.3 (2024-02-16 12:09) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[584c967eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/584c967eb) Merge branch 'Correction_Squelette_Ex_Const' into 'main' (2024-02-16 11:23) <Yves Biton>  
`|\ \  `  
`| * | `[b3c79fa29](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3c79fa29) Suppression de l'appel de surcharge dans ces deux sections (2024-02-16 11:21) <Yves Biton>  
`* | | `[03194b948](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03194b948) retag 2.0.3 (2024-02-16 09:37) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[558d31d42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/558d31d42) Merge branch 'Ajout_Mode_Dys_Exos_Construction' into 'main' (2024-02-16 08:59) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[d87dbb64c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d87dbb64c) Pour toutes les sections d'exercice de construction, on rajoute un mode "dys" permettant d'utiliser MathGraph32 en mode "dys" (2024-02-16 08:51) <Yves Biton>  
`* |   `[e554077ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e554077ec) Merge branch 'fsgf' into 'main' (2024-02-16 08:27) <Tommy Barroy>  
`|\ \  `  
`| * | `[e8ad25f34](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8ad25f34) bugsnag (2024-02-16 08:26) <Tommy Barroy>  
`* | | `[0fb9b1f72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0fb9b1f72) Merge branch 'fsgf' into 'main' (2024-02-16 08:25) <Tommy Barroy>  
`|\| | `  
`| * | `[eabb15f74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eabb15f74) bugsnag (2024-02-16 08:24) <Tommy Barroy>  
`|/ /  `  
`* | `[224c16d0b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/224c16d0b) retag 2.0.3 (2024-02-14 07:53) <static@sesamath-lampdev>  
`* |   `[604b4d810](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/604b4d810) Merge branch 'remet_variables' into 'main' (2024-02-14 07:23) <Tommy Barroy>  
`|\ \  `  
`| * | `[743b2ea24](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/743b2ea24) remet variables deans editeur (2024-02-14 07:22) <Tommy Barroy>  
`|/ /  `  
`* | `[5285a1118](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5285a1118) retag 2.0.3 (2024-02-13 12:21) <static@sesamath-lampdev>  
`* |   `[fd58c223f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd58c223f) Merge branch 'ajoute_bloc_perso' into 'main' (2024-02-13 11:46) <Tommy Barroy>  
`|\ \  `  
`| * | `[e412af123](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e412af123) met des ciseaux (2024-02-13 11:43) <Tommy Barroy>  
`| * | `[dce3ae53e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dce3ae53e) ajoute bloc perso dans mohascratch (2024-02-13 11:38) <Tommy Barroy>  
`|/ /  `  
`* | `[8b74d1b2c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b74d1b2c) retag 2.0.3 (2024-02-12 23:43) <static@sesamath-lampdev>  
`* |   `[f5c5b8c31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f5c5b8c31) Merge branch 'ajoute_demande' into 'main' (2024-02-12 23:10) <Tommy Barroy>  
`|\ \  `  
`| * | `[676a0ae79](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/676a0ae79) ajoute demander , attendre et reponse dans mohascratch (2024-02-12 23:08) <Tommy Barroy>  
`|/ /  `  
`* |   `[2a1cdf455](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a1cdf455) Merge branch 'onresize' into 'main' (2024-02-11 22:25) <Tommy Barroy>  
`|\ \  `  
`| * | `[9545e3157](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9545e3157) [fix #239](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/239) (2024-02-11 22:23) <Tommy Barroy>  
`* | | `[b03c3cce3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b03c3cce3) retag 2.0.3 (2024-02-11 19:53) <static@sesamath-lampdev>  
`| |/  `  
`|/|   `  
`* |   `[e315ff84f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e315ff84f) Merge branch 'Correction_Consigne' into 'main' (2024-02-11 19:17) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[fecdcb066](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fecdcb066) Modif de consigne suite à signalement de Olivier (2024-02-11 19:15) <Yves Biton>  
`* |   `[35271f6d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35271f6d7) Merge branch 'bug_sur_yareponse' into 'main' (2024-02-09 16:10) <Tommy Barroy>  
`|\ \  `  
`| * | `[4853935aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4853935aa) ya reponse fonctionnait 1 fois sur 4 (2024-02-09 16:09) <Tommy Barroy>  
`|/ /  `  
`* | `[6fccffaeb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6fccffaeb) j3pLoad exporte getPhrasesEtat (qui transforme la liste de pe en objet phrasesEtat) (2024-02-09 13:19) <Daniel Caillibaud>  
`* | `[8a917e40c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a917e40c) manquait l'import jquery dans Parcours.js (2024-02-09 13:19) <Daniel Caillibaud>  
`* | `[253c99347](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/253c99347) retag 2.0.3 (2024-02-09 13:10) <static@sesamath-lampdev>  
`|/  `  
`*   `[7f68832e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f68832e2) Merge branch 'Amelioration_Exos_Sur_Derivees' into 'main' (2024-02-09 12:12) <Yves Biton>  
`|\  `  
`| * `[28bddd04b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28bddd04b) Petites modifs sur exos sur dérivées et on laisse un peu de plave sous les réponses dans la sectionexcalcmathquill (2024-02-09 12:10) <Yves Biton>  
`* | `[179193fec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/179193fec) retag 2.0.3 (2024-02-08 14:47) <static@sesamath-lampdev>  
`* | `[15b5627d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15b5627d5) retag 2.0.3 (2024-02-08 14:07) <static@sesamath-lampdev>  
`|/  `  
`*   `[590c268eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/590c268eb) Merge branch 'Correction_Couleur_excalc' into 'main' (2024-02-08 14:06) <Yves Biton>  
`|\  `  
`| * `[09139b871](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09139b871) Correction de la couleur des affichages donnant le nombre d'essais restants (2024-02-08 14:04) <Yves Biton>  
`* |   `[e126c0ac5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e126c0ac5) Merge branch 'pas_deux_fois' into 'main' (2024-02-08 13:25) <Tommy Barroy>  
`|\ \  `  
`| * | `[d5dc3367f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5dc3367f) enregistre pas 2 fois les trucs lourds en double dans la dernière. Reste plus qu'à compresser/décompresser et trouver une solution simple pour hebergement depuis éditeur (2024-02-08 13:23) <Tommy Barroy>  
`* | | `[19b538cd4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19b538cd4) précisions de codeStyle (2024-02-08 13:18) <Daniel Caillibaud>  
`|/ /  `  
`* | `[ad1e76aba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad1e76aba) aj .idea/codeStyles/Project.xml qui n'y était plus (2024-02-08 10:59) <Daniel Caillibaud>  
`* |   `[39b4c8ea8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39b4c8ea8) Merge branch 'pour_pas_enregistrer_2_fois_mm' into 'main' (2024-02-07 21:03) <Tommy Barroy>  
`|\ \  `  
`| * | `[4d861b48e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d861b48e) enregistre pas 2 fois les trucs lourds en double (2024-02-07 21:01) <Tommy Barroy>  
`|/ /  `  
`* |   `[217ab0880](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/217ab0880) Merge branch 'pourbugsnag' into 'main' (2024-02-07 20:27) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[012b73743](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/012b73743) ev sur touchstart est pas le mm (2024-02-07 20:23) <Tommy Barroy>  
`|/  `  
`*   `[908397aa1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/908397aa1) Merge branch 'Amelioration_Gestion_Intervalles' into 'main' (2024-02-07 18:06) <Yves Biton>  
`|\  `  
`| * `[55bf0fa6b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55bf0fa6b) Ajout d'un paramètre à la section multi étapes pour que, dans le cas d'une étape de résolution d'inéquation, on compte comme une faute un intervalle fermé sur l'infini (et pas comme une faute de syntaxe). (2024-02-07 18:05) <Yves Biton>  
`|/  `  
`* `[7bcbe27ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7bcbe27ea) nettoyage (getDonnees viré) (2024-02-07 15:17) <Daniel Caillibaud>  
`*   `[d2526bcbb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2526bcbb) Merge branch 'recupVieuxTrucs' (2024-02-07 15:10) <Daniel Caillibaud>  
`|\  `  
`| * `[e9b2b6dd9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9b2b6dd9) aj hasScore false (2024-02-07 15:01) <Daniel Caillibaud>  
`| * `[9e324b089](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e324b089) aj d'un titre en option (2024-02-07 14:56) <Daniel Caillibaud>  
`|/  `  
`* `[3cff55794](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3cff55794) mutualisation de la création d'une page playwright (2024-02-07 13:14) <Daniel Caillibaud>  
`* `[a3243752b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3243752b) amélioration du fileLogger pour avoir les traces des erreurs (2024-02-07 13:14) <Daniel Caillibaud>  
`* `[362ef4070](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/362ef4070) retag 2.0.3 (2024-02-07 13:06) <static@sesamath-lampdev>  
`* `[3aa9ad3bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3aa9ad3bc) ne râle plus sur les pbs de requêtes HEAD qui plante si on est pas en mode verbose (2024-02-07 12:04) <Daniel Caillibaud>  
`* `[ffe4c7ebd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffe4c7ebd) code devenu inutile viré (2024-02-07 12:01) <Daniel Caillibaud>  
`* `[6cf4e707d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cf4e707d) amélioration init prefs (2024-02-07 12:01) <Daniel Caillibaud>  
`*   `[2d9249d59](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d9249d59) Merge branch 'Correction_Focus_Dans_Sections_Yves' into 'main' (2024-02-07 12:01) <Daniel Caillibaud>  
`|\  `  
`| * `[48db336bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48db336bc) fix inputId (2024-02-07 11:18) <Daniel Caillibaud>  
`| * `[f7413b6ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7413b6ad) renommage setFocusToFirstPlaceHolder => setFocusToFirstInput (2024-02-07 10:08) <Daniel Caillibaud>  
`| * `[f9480f79e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f9480f79e) fct getFirstCharIndex dupliquées remplacées par le getFirstIndexParExcluded générique (2024-02-06 21:01) <Daniel Caillibaud>  
`| * `[86b4f7977](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86b4f7977) getFirstStringIndex dupliquées remplacées par le getFirstIndexParExcluded générique (fix au passage le bug qui retournait la taille de la string quand searched n'existait pas) (2024-02-06 20:53) <Daniel Caillibaud>  
`| * `[907792767](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/907792767) parFer dupliquées remplacées par le getIndexFermant générique (2024-02-06 20:47) <Daniel Caillibaud>  
`| * `[7848ba03d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7848ba03d) encore un peu de factorisation avec le focusPlaceholder générique (2024-02-06 20:34) <Daniel Caillibaud>  
`| * `[b9492a735](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9492a735) mutualisation des autres focusEditeur() (avec la fct générique focusIfExists) (2024-02-06 20:24) <Daniel Caillibaud>  
`| * `[9141a07cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9141a07cb) mutualisation setFocusToFirstPlaceHolder (en général à la place de focusPremierEditeur) (2024-02-06 19:55) <Daniel Caillibaud>  
`| * `[4114aa2f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4114aa2f3) factorisation de setFocusById (2024-02-06 19:19) <Daniel Caillibaud>  
`| * `[d23da69c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d23da69c5) Ajout focusPlaceholder (2024-02-06 19:14) <Daniel Caillibaud>  
`| * `[8cdcfa57b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cdcfa57b) focus => focusIfExists pour ne pas confondre avec la fct globale window.focus() (2024-02-06 18:36) <Daniel Caillibaud>  
`| * `[cf368ef8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf368ef8d) Aj fct générique focus qui le lance dans un setTimeout (et tolère que l'élément n'existe pas ou plus dans le dom) (2024-02-06 18:22) <Daniel Caillibaud>  
`| * `[19de681e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19de681e2) Quand le focus est appleé via un setTimeout on vériie que l'éditeur est toujours présente avant d'appeler focus sur cet éditeur (2024-02-06 17:49) <Yves Biton>  
`|/  `  
`* `[d8fb4a47d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8fb4a47d) aj test unitaire pour addMaj (2024-02-06 17:48) <Daniel Caillibaud>  
`* `[d61c35ea9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d61c35ea9) Aj getFirstIndexParExcluded générique (avec son test unitaire) (2024-02-06 17:23) <Daniel Caillibaud>  
`* `[411184676](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/411184676) commentaires (2024-02-06 16:48) <Daniel Caillibaud>  
`* `[3eb54a95b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3eb54a95b) afficheMathliveDans gère aussi les params en number (2024-02-06 16:24) <Daniel Caillibaud>  
`* `[11412da65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/11412da65) revert partiel de cf8188c4a (faut préciser feedback false, cf erreur bugsnag 65c14b4d5f0cf200082d21cc) (2024-02-06 16:24) <Daniel Caillibaud>  
`*   `[1baee6d07](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1baee6d07) Merge branch 'Correction_Squelette_Reduc_Meme_Denom' into 'main' (2024-02-06 16:18) <Yves Biton>  
`|\  `  
`| * `[c25386c8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c25386c8d) Dans ceratines sections j'avais oublié de passer le paramètre e  comme String sinon Mathlive n'en veyt pas. [fix #238](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/238) (2024-02-06 16:16) <Yves Biton>  
`|/  `  
`*   `[e1167b722](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1167b722) Merge branch 'Correction_Inequation_Deg1_Et_Multi_Etapes' into 'main' (2024-02-06 16:07) <Yves Biton>  
`|\  `  
`| * `[709e7b9ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/709e7b9ac) Amélioration squelettes et width et height mis à 0 si possible dans certaines fichiers annexes. (2024-02-06 16:04) <Yves Biton>  
`* |   `[126ca8ce9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/126ca8ce9) Merge branch 'remet_phrases' into 'main' (2024-02-06 15:42) <Tommy Barroy>  
`|\ \  `  
`| * | `[00b3a23e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00b3a23e0) nettoyage (2024-02-06 08:44) <Daniel Caillibaud>  
`| * | `[ccb4ac95d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ccb4ac95d) oubli de phrases (2024-02-06 08:42) <Tommy Barroy>  
`* | |   `[f97a4b17b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f97a4b17b) Merge branch 'bug_dans_copie_d'un_tableau' into 'main' (2024-02-06 15:39) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[b8a92c545](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8a92c545) bug dans la copie d'un tabeleau (2024-02-06 15:38) <Tommy Barroy>  
`| * | `[4ee187f06](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ee187f06) bug dans la copie d'un tabeleau (2024-02-06 15:36) <Tommy Barroy>  
`|/ /  `  
`* |   `[57f65dd7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57f65dd7a) Merge branch 'doubleform' into 'main' (2024-02-05 21:27) <Tommy Barroy>  
`|\ \  `  
`| * | `[2d7957761](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d7957761) une formule en double n'est enregistrée qu'une fois (2024-02-05 21:26) <Tommy Barroy>  
`|/ /  `  
`* |   `[e91a3030f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e91a3030f) Merge branch 'double' into 'main' (2024-02-05 17:17) <Tommy Barroy>  
`|\ \  `  
`| * | `[4ef8f3045](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ef8f3045) une etiquette en double n'est enregistrée qu'une fois (2024-02-05 17:11) <Tommy Barroy>  
`* | | `[2461087d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2461087d7) code inutile viré (2024-02-05 15:59) <Daniel Caillibaud>  
`|/ /  `  
`* / `[2fa3563ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2fa3563ce) retag 2.0.3 (2024-02-04 22:54) <static@sesamath-lampdev>  
`|/  `  
`*   `[277adcfd2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/277adcfd2) Merge branch 'Amelioration_Squelette_Multi_EDit_Bloc' into 'main' (2024-02-04 20:00) <Yves Biton>  
`|\  `  
`| * `[25d1f1f41](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25d1f1f41) Amélioration squelettes et width et height mis à 0 si possible dans certaines fichiers annexes. (2024-02-04 19:55) <Yves Biton>  
`|/  `  
`*   `[ca1fe59fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca1fe59fa) Merge branch 'Amelioration_Squelette_Deriv_Param' into 'main' (2024-02-04 10:53) <Yves Biton>  
`|\  `  
`| * `[cf8188c4a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf8188c4a) Amélioration squelette _Deriv_Param_Mathquill et de certains fichiers annexes (2024-02-04 10:51) <Yves Biton>  
`* | `[d90ec77ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d90ec77ae) retag 2.0.3 (2024-02-03 23:08) <static@sesamath-lampdev>  
`* | `[34deb1e3e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34deb1e3e) retag 2.0.3 (2024-02-03 20:42) <static@sesamath-lampdev>  
`|/  `  
`*   `[69644cfc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69644cfc7) Merge branch 'Amelioration_Squelette_Et_Exos' into 'main' (2024-02-03 20:41) <Yves Biton>  
`|\  `  
`| * `[edd8ab8b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edd8ab8b3) Amélioration squelette _Deriv_Param_Mathquill et suppression des svg dans tous les exercices dans les fichiers annexes associés. (2024-02-03 20:35) <Yves Biton>  
`* |   `[9856f6712](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9856f6712) Merge branch 'pour_ticket' into 'main' (2024-02-03 16:59) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[4790b8847](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4790b8847) [fix #237](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/237) (2024-02-03 16:56) <Tommy Barroy>  
`|/  `  
`*   `[a3bc547c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3bc547c1) Merge branch 'fixGetMathliveValue' into 'main' (2024-02-03 15:16) <Yves Biton>  
`|\  `  
`| * `[51f68efda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51f68efda) Correction de squelette et amélioration des écouteurs d'événements dans le multi edit (2024-02-03 15:05) <Yves Biton>  
`| * `[f3cb808e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3cb808e9) Aj de l'option raw: true pour fonctionner comme avant la mutualisation de getMathliveValue (2024-02-02 20:27) <Daniel Caillibaud>  
`| * `[1ade99562](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ade99562) fix appel particulier de getMathliveValue (2024-02-02 20:26) <Daniel Caillibaud>  
`| * `[73548ef69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/73548ef69) màj deps (2024-02-02 20:01) <Daniel Caillibaud>  
`| * `[ebd798612](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ebd798612) surtout pas de feedback sonore (ça fait du bruit et génère des milliers de rapport bugsnag avec "Can't find variable: AudioContext") (2024-02-02 19:59) <Daniel Caillibaud>  
`| * `[60ce7fcf4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/60ce7fcf4) blindage cleanLatexForMl qui retourne toujours une string (vide en cas d'appel foireux) (2024-02-02 19:36) <Daniel Caillibaud>  
`| * `[d1e534f92](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1e534f92) ajout d'une option raw à getMathliveValue pour ne pas utiliser cleanLatexForMl (ex_Calc_Multi_Edit faisait ça avant, on remet à l'identique) (2024-02-02 19:35) <Daniel Caillibaud>  
`| * `[3588ab2a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3588ab2a4) fix appel particulier de getMathliveValue (2024-02-02 19:10) <Daniel Caillibaud>  
`| * `[4f007f901](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f007f901) mutualisation de getMathliveValue (qui retourne toujours une string, vide en cas de pb) (2024-02-02 19:10) <Daniel Caillibaud>  
`* |   `[2b595f49e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b595f49e) Merge branch 'verif_pas_mm_points' into 'main' (2024-02-03 13:45) <Tommy Barroy>  
`|\ \  `  
`| * | `[33defe6f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33defe6f6) pour bugsnag (2024-02-03 13:44) <Tommy Barroy>  
`|/ /  `  
`* |   `[4e73259a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e73259a3) Merge branch 'dfjskl' into 'main' (2024-02-02 21:06) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[9bee36e63](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9bee36e63) pour bugsnag (2024-02-02 21:03) <Tommy Barroy>  
`|/  `  
`* `[d1d18c1cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1d18c1cc) mod build avec mathlive dans un chunk séparé (2024-02-02 18:36) <Daniel Caillibaud>  
`* `[e6523fc7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e6523fc7a) code viré (temporaire devenu inutile) (2024-02-02 18:36) <Daniel Caillibaud>  
`*   `[f2869e358](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2869e358) Merge branch 'Correction_Squelette_Calc_Param_Multi_Edit' into 'main' (2024-02-02 17:38) <Yves Biton>  
`|\  `  
`| * `[dbabc8440](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dbabc8440) Correction suite rapport Bugsnag (2024-02-02 17:37) <Yves Biton>  
`|/  `  
`*   `[1298bfdfb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1298bfdfb) Merge branch 'Correction_Squelette_Calc_Multi_Edit' into 'main' (2024-02-02 17:29) <Yves Biton>  
`|\  `  
`| * `[8f1a77355](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f1a77355) Correction suite rapport Bugsnag (2024-02-02 17:28) <Yves Biton>  
`* |   `[0b0570525](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b0570525) Merge branch 'Correction_squelette_Calc_Param_2Edit' into 'main' (2024-02-02 17:00) <Yves Biton>  
`|\ \  `  
`| * | `[d32fc206e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d32fc206e) Correction suite rapport Bugsnag erreurs dans marqueEditeurPourErreur et demarqueEditeurPourErreur. (2024-02-02 16:59) <Yves Biton>  
`| |/  `  
`* |   `[12b0ca60b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12b0ca60b) Merge branch 'Correction_Multi_Etapes_Suote_Bugsnag' into 'main' (2024-02-02 15:53) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[3ea53bc6c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ea53bc6c) Correction suite rapport Bugsnag car appel de setFocusById alors que l'éditeur a disparu. (2024-02-02 15:52) <Yves Biton>  
`|/  `  
`* `[1ae1c330d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ae1c330d) vitest-env jsdom viré (on utilise happy-dom quand y'a besoin) (2024-02-02 15:11) <Daniel Caillibaud>  
`* `[6252db0c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6252db0c8) fix init rexams (2024-02-02 14:26) <Daniel Caillibaud>  
`*   `[f6a75a466](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6a75a466) Merge branch 'correction_Section_Diviseurs_Entier' (2024-02-02 14:02) <Daniel Caillibaud>  
`|\  `  
`| * `[f6bc5a554](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6bc5a554) Un paramètre de type float remplacé par number (2024-02-02 13:55) <Yves Biton>  
`|/  `  
`*   `[429c64685](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/429c64685) Merge branch 'Amelioration_Multi_Etapes_Inequations' into 'main' (2024-02-02 12:41) <Yves Biton>  
`|\  `  
`| * `[679f1f392](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/679f1f392) Amélioration de la gestion des inéquations dans le multi étapes (2024-02-02 12:39) <Yves Biton>  
`* | `[28b49cfe3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28b49cfe3) fix rexams qui utilisait encore j3pAjouteCoche (et nettoyage section PresentationCalculs, la seule à utiliser rexams) (2024-02-02 12:31) <Daniel Caillibaud>  
`* |   `[c091d26fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c091d26fe) Merge branch 'nettoyage' into 'main' (2024-02-02 11:44) <Daniel Caillibaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[3fa1aa41e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3fa1aa41e) commentaire obsolète viré (ref à _Donnees) (2024-02-02 11:37) <Daniel Caillibaud>  
`| * `[21404c409](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/21404c409) nettoyage lecteurmtg32 (constructeur _Donnees viré) (2024-02-02 11:32) <Daniel Caillibaud>  
`| * `[eb9f5d2f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb9f5d2f4) nettoyage inequationFctRef (constructeur _Donnees viré) (2024-02-02 11:25) <Daniel Caillibaud>  
`| * `[4a585afd7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a585afd7) constructeur _Donnees viré (mais section toujours HS) (2024-02-02 11:17) <Daniel Caillibaud>  
`| * `[535586ed2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/535586ed2) nettoyage diviseurs_Entier (2024-02-02 11:12) <Daniel Caillibaud>  
`| * `[055daef15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/055daef15) fix this.essaiCourant < ds.nbchances (et pas this.essaiCourant <= ds.nbchances) (2024-02-02 11:01) <Daniel Caillibaud>  
`| * `[ef7d1aa51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef7d1aa51) nettoyage compositionLibre (_Donnees viré) (2024-02-02 11:00) <Daniel Caillibaud>  
`| * `[8d9e0059b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d9e0059b) nettoyage, mais il reste pas mal de bugs (correction affichée loufoque) (2024-02-02 10:58) <Daniel Caillibaud>  
`| * `[22661aa8a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22661aa8a) constructeur _Donnees viré (2024-02-01 20:13) <Daniel Caillibaud>  
`| * `[7d3b74860](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d3b74860) refacto section compteestbon (bugfixes, mais il reste qq défauts) (2024-02-01 20:09) <Daniel Caillibaud>  
`| * `[0a04627f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a04627f9) LegacySectionWrapper utilise le préfixe Mep comme j3pLoad (sinon ça risque de casser des sections qui l'ont en dur) (2024-02-01 18:02) <Daniel Caillibaud>  
`| * `[ecf84d5dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecf84d5dd) Parcours.nom viré (personne ne l'utilisait) (2024-02-01 17:59) <Daniel Caillibaud>  
`| * `[ad0191270](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad0191270) div #Mepact_ viré, plus aucun usage de la var j3p globale dans les fcts j3p* (2024-02-01 17:58) <Daniel Caillibaud>  
`| * `[7f88a27ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f88a27ff) j3pAjouteCoche virée (et refacto espace_repere7 pour ne plus l'utiliser) (2024-02-01 17:43) <Daniel Caillibaud>  
`| * `[3a9243cc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a9243cc4) structures inutilisées virées (presentation1quater et les tableauX) (2024-02-01 16:25) <Daniel Caillibaud>  
`|/  `  
`* `[9b3570a7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b3570a7a) modif ordre entre * et / pour checkBilan (pour coller à Parcours.js et éviter une ≠ d'arrondi) (2024-02-01 16:05) <Daniel Caillibaud>  
`* `[c710d45a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c710d45a6) retag 2.0.3 (2024-01-31 19:42) <static@sesamath-lampdev>  
`* `[1c0bff2f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c0bff2f8) fix affectation de donneesPersistantes (typeSuite est un entier dans cette section, faut pas propager ça aux autres qui comprendraient pas) (2024-01-31 19:03) <Daniel Caillibaud>  
`* `[c2950c18a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2950c18a) fix upgradeParametres manquant (2024-01-31 19:02) <Daniel Caillibaud>  
`* `[20b77ea08](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20b77ea08) load & loadGraphe font une purge et recommencent une fois en cas de pb de chargement (2024-01-31 19:01) <Daniel Caillibaud>  
`* `[58a72ea62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58a72ea62) amélioration requestfailedListener (2024-01-31 17:33) <Daniel Caillibaud>  
`* `[1de88b3f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1de88b3f7) fix this vs me (2024-01-31 17:30) <Daniel Caillibaud>  
`*   `[62418031f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/62418031f) Merge branch 'Uniformisation_Noms_Calculs_Ineq' into 'main' (2024-01-31 16:44) <Yves Biton>  
`|\  `  
`| * `[1a338233f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a338233f) Dans la section ineq remplace le nom de calcul attendu estBorne par estBorneFermee pour uniformisation. Modification de traiteMathlive dans les sections de résolution d'inéquations pour que si on tape une { au clavier au lieu d'utiliser le bouton Mathquill le résultat soit quand même accepté. (2024-01-31 16:43) <Yves Biton>  
`* | `[7823ceb88](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7823ceb88) fix checkFeedback (2024-01-31 16:42) <Daniel Caillibaud>  
`* | `[b0c0159b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0c0159b6) fct perdue commentée (2024-01-31 15:44) <Daniel Caillibaud>  
`* | `[d5122cdcf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5122cdcf) fix pgcd de nb parfois négatif (2024-01-31 15:43) <Daniel Caillibaud>  
`* | `[f65083cf5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f65083cf5) fix création de bulle HS (2024-01-31 15:43) <Daniel Caillibaud>  
`* |   `[c3ab0114c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c3ab0114c) Merge branch 'groupTexts' into 'main' (2024-01-31 15:42) <Daniel Caillibaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[ea4e93cb6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea4e93cb6) testsBrowser utilise lib/core/textes (2024-01-31 15:23) <Daniel Caillibaud>  
`| * `[31e2cd567](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31e2cd567) Tous les textes qui étaient dans parcours.textes sont désormais dans un module js (pour être i18n compliant) (2024-01-31 15:04) <Daniel Caillibaud>  
`* |   `[8bffe8ec8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8bffe8ec8) Merge branch 'Amelioration_Multi_Etapes_Boutons_Ineq' into 'main' (2024-01-31 10:15) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[83ce2f89e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83ce2f89e) Quand une étape est une étape de résolution d'inéquation on rajoute à la palette Mathquill les deux crochets et le ; qui ne peuvent pas être dans le clavier virtuel. (2024-01-31 10:13) <Yves Biton>  
`|/  `  
`* `[4ce8d769a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ce8d769a) retag 2.0.3 (2024-01-30 21:44) <static@sesamath-lampdev>  
`* `[f2663e0d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2663e0d9) retag 2.0.3 (2024-01-30 20:49) <static@sesamath-lampdev>  
`*   `[4d8709635](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d8709635) Merge branch 'Passage_Mathlive_Squelette_Eq_Param_Sol_Mult_Apres_Ensdef' into 'main' (2024-01-30 20:47) <Yves Biton>  
`|\  `  
`| * `[2ef77ccea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ef77ccea) Passage sous Mathlive HTML de la section eq_param_Sol_Mult_Apres_Ens_Def et affichage de la solution via Mathlive en HTML pour les fichers annexes associés. (2024-01-30 20:46) <Yves Biton>  
`* |   `[a629a01d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a629a01d0) Merge branch 'factoriseTypeSuite' into 'main' (2024-01-30 14:10) <Daniel Caillibaud>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[54c3c29ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54c3c29ea) upgradeParametres ne prend que ceux du graphe (pas encore eu la surcharge, il peut en manquer) (2024-01-30 13:00) <Daniel Caillibaud>  
`| * `[3e311d12d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e311d12d) fix gestion des variantes (2024-01-30 13:00) <Daniel Caillibaud>  
`| * `[20a5964ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20a5964ba) aj gestion env BASE_URL (2024-01-30 12:59) <Daniel Caillibaud>  
`| * `[ab8597da7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab8597da7) valideUneZone accepte une réponse string|number comme la doc l'indique (c'était pas géré par le code) (2024-01-30 12:24) <Daniel Caillibaud>  
`| * `[ea5beb172](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea5beb172) généralisation de typeSuite en constante (à la place de string littérale) (2024-01-27 15:18) <Daniel Caillibaud>  
`* |   `[464c6302e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/464c6302e) Merge branch 'Passage_Mathlive_Correction_Fichiers_Annexes' into 'main' (2024-01-30 13:23) <Yves Biton>  
`|\ \  `  
`| * | `[764259e2a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/764259e2a) Passage sousMathlive HTML pour les ficheirs annexes associés au suelette Calc_Param_MathQuill_Sans_Entete (2024-01-30 13:21) <Yves Biton>  
`|/ /  `  
`* | `[09e59c645](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09e59c645) retag 2.0.3 (2024-01-29 18:53) <static@sesamath-lampdev>  
`* |   `[285a77dcc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/285a77dcc) Merge branch 'Passage_Mathlive_Squelette_Ecriture_Exponentielle' into 'main' (2024-01-29 17:52) <Yves Biton>  
`|\ \  `  
`| * | `[18f0a3fac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/18f0a3fac) Passage sousMathlive HTML pour ce squelette et sonseul fichier annexe associé (2024-01-29 17:49) <Yves Biton>  
`* | | `[7460517d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7460517d7) retag 2.0.3 (2024-01-29 13:54) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[f43b5d98d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f43b5d98d) Merge branch 'Passahe_Mathlive_Section_Calc_Param_2_Edit' into 'main' (2024-01-29 13:08) <Yves Biton>  
`|\ \  `  
`| * | `[2080c07d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2080c07d2) On passe en mode Mathlive HTML pour lce squelette et deux exercices associés en correction HTML (2024-01-29 13:07) <Yves Biton>  
`* | | `[e06996081](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e06996081) retag 2.0.3 (2024-01-29 11:53) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[4fdc4a0f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fdc4a0f7) Merge branch 'Passage_Mode_HTML_Correction_Fichiers_Annexes' into 'main' (2024-01-29 11:11) <Yves Biton>  
`|\ \  `  
`| * | `[d46596452](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d46596452) On passe en mode Mathlive HTML pour la correction des exercices associés à ces fichiers annexes (2024-01-29 11:08) <Yves Biton>  
`* | | `[a2ce5a614](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2ce5a614) il ne faut pas de suffixe sur les imports de js (2024-01-27 15:40) <Daniel Caillibaud>  
`* | | `[943af0ebf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/943af0ebf) retag 2.0.3 (2024-01-27 10:48) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[5eb667b55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5eb667b55) Merge branch 'Passage_Mathlive_Squuelette_Calc_Param_1Edit' into 'main' (2024-01-27 09:58) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[3199b50ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3199b50ac) Passage sous Mathlive du squelette Calc_Param_1_Edit et passage sous Mathlive de la correctionde fochier annexes associés (à finir) (2024-01-27 09:55) <Yves Biton>  
`* | `[80f5e2d06](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80f5e2d06) aj rmq dans le form pour editgraphe (2024-01-25 16:22) <Daniel Caillibaud>  
`|/  `  
`*   `[df14cf96a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df14cf96a) Merge branch 'fixCheckCorrection' into 'main' (2024-01-25 15:18) <Jean-Claude Lhote>  
`|\  `  
`| * `[47fb3ca6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47fb3ca6f) On simplifie la regex pour le feedback pour ne pas s'embêter avec l'apostrophe (2024-01-25 15:17) <Jean-claude Lhote>  
`| *   `[929e27cb7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/929e27cb7) Merge remote-tracking branch 'origin/main' (2024-01-25 15:15) <Jean-claude Lhote>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[d254e6ca6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d254e6ca6) Merge branch 'Remplacement_Quote_Pour_Compatibilite_Test' into 'main' (2024-01-25 13:54) <Yves Biton>  
`|\ \  `  
`| * | `[7535bb737](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7535bb737) Remplacement du caractère ' par ’ pour compatibilité des tests automatiques. (2024-01-25 13:51) <Yves Biton>  
`| |/  `  
`* |   `[790c2e201](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/790c2e201) Merge branch 'fixTestMultiEdit' into 'main' (2024-01-25 13:49) <Jean-Claude Lhote>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[3e53b03fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e53b03fa) Modification de l'apostrophe qui faisait planter le test ex_Calc_Multi_Edit (2024-01-25 13:09) <Jean-claude Lhote>  
`|/  `  
`* `[bfdb0caa9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bfdb0caa9) fix init des prefs (2024-01-24 19:55) <Daniel Caillibaud>  
`* `[270c6d8a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/270c6d8a3) orthographe (2024-01-24 10:24) <Daniel Caillibaud>  
`* `[3ddbd02f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ddbd02f8) retag 2.0.3 (2024-01-24 09:20) <static@sesamath-lampdev>  
`*   `[34162ad07](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34162ad07) Merge branch 'Amelioration_Section_Calc_Multi_Edit_Apres_Interm' into 'main' (2024-01-23 17:38) <Yves Biton>  
`|\  `  
`| * `[ddc692522](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ddc692522) Modification de parcous pour avoir accès à storage dans la fonction upgradeParametres. (2024-01-23 17:37) <Yves Biton>  
`* | `[646e2e2a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/646e2e2a7) refacto en utilisant des variables plutôt que des strings littérales (pour régler les pbs d'accent) [fix #232](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/232) (2024-01-23 14:44) <Daniel Caillibaud>  
`* | `[27f106f2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27f106f2e) retag 2.0.3 (2024-01-22 09:20) <static@sesamath-lampdev>  
`|/  `  
`*   `[e8c75704d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8c75704d) Merge branch 'Passage_Mathlive_Section_Multi_Edit_Apres_Interm' into 'main' (2024-01-21 18:04) <Yves Biton>  
`|\  `  
`| * `[e73ac48cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e73ac48cf) Passage à Mathlive de la section calc Multi Edit après calculs intermédiaires (2024-01-21 18:03) <Yves Biton>  
`* | `[1c22d7b5b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c22d7b5b) retag 2.0.3 (2024-01-21 12:44) <static@sesamath-lampdev>  
`|/  `  
`*   `[44dad8ce2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44dad8ce2) Merge branch 'Ajout_Dispo_Car_Editeurs_Texte' into 'main' (2024-01-21 12:09) <Yves Biton>  
`|\  `  
`| * `[ea9fb491b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea9fb491b) Ajout de la possibilité de mettre d'autres caractères dans les champs textes à la demande de Yannick (tableur) (2024-01-21 12:06) <Yves Biton>  
`* | `[3db5868f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3db5868f3) retag 2.0.3 (2024-01-21 11:21) <static@sesamath-lampdev>  
`|/  `  
`*   `[44c9f35d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44c9f35d1) Merge branch 'Peaufinage_Section_Multi_Etapes' into 'main' (2024-01-21 10:35) <Yves Biton>  
`|\  `  
`| * `[366ac52d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/366ac52d4) Les chaines cbien et cfaux passent dans textes pour pouvoir angliciser facilement. Correction mineure de ex_Calc_Multi_Edit_Apres_Interm : quand on passait à l'étape 2 le bouton recopier était disponible au premier essai. (2024-01-21 10:34) <Yves Biton>  
`* | `[22733f0be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22733f0be) retag 2.0.3 (2024-01-20 18:24) <static@sesamath-lampdev>  
`|/  `  
`*   `[a05fafd81](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a05fafd81) Merge branch 'Possibilite_Caractere_Espace_Clavier_virtuel' into 'main' (2024-01-20 17:46) <Yves Biton>  
`|\  `  
`| * `[b2da53768](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2da53768) On donne la possibilité d'autoriser le carcatère espace mais seulement dans les éditeurs de texte. (2024-01-20 17:44) <Yves Biton>  
`|/  `  
`* `[d41a0a451](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d41a0a451) retag 2.0.3 (2024-01-20 11:43) <static@sesamath-lampdev>  
`*   `[3ce9b1e10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ce9b1e10) Merge branch 'Correction_Array_Lignes_Pour_Compatibilite_Mathlive' into 'main' (2024-01-20 11:10) <Yves Biton>  
`|\  `  
`| * `[894eee1c2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/894eee1c2) On corrige dans display la différence d'afficheage pour les array lignes créées avec MathGraph32 et Mathlive. (2024-01-20 11:05) <Yves Biton>  
`|/  `  
`* `[eb90d28a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb90d28a7) retag 2.0.3 (2024-01-19 15:23) <static@sesamath-lampdev>  
`*   `[4c4bf4846](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c4bf4846) Merge branch 're_remet' into 'main' (2024-01-19 14:39) <Tommy Barroy>  
`|\  `  
`| * `[39b3bac88](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39b3bac88) oubli de phrases (2024-01-18 21:12) <Tommy Barroy>  
`|/  `  
`*   `[dd14d7790](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd14d7790) Merge branch 'airevolagrand_bug' into 'main' (2024-01-18 11:37) <Tommy Barroy>  
`|\  `  
`| * `[aea84ce38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aea84ce38) y'avait aire d'un segment... (2024-01-18 11:33) <Tommy Barroy>  
`|/  `  
`* `[5f205dd31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f205dd31) retag 2.0.3 (2024-01-18 10:48) <static@sesamath-lampdev>  
`*   `[4fa1c062d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fa1c062d) Merge branch 're_melange' into 'main' (2024-01-18 10:12) <Tommy Barroy>  
`|\  `  
`| * `[d1b2479bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1b2479bf) re melange les vignettes ( je l'avais enlevé pour trouver un bug ) (2024-01-18 10:09) <Tommy Barroy>  
`|/  `  
`* `[b0f39c557](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0f39c557) retag 2.0.3 (2024-01-17 18:46) <static@sesamath-lampdev>  
`*   `[1781cd9c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1781cd9c5) Merge branch 'Correction_Multi_Edit_Etape_Construction' into 'main' (2024-01-17 17:03) <Yves Biton>  
`|\  `  
`| * `[15b47d1f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15b47d1f2) Correction du plantage dans afficheReponse dans le cas d'un exercice de construction (2024-01-17 17:02) <Yves Biton>  
`* | `[cbf2bb898](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cbf2bb898) retag 2.0.3 (2024-01-17 16:25) <static@sesamath-lampdev>  
`|/  `  
`*   `[c9491e6b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9491e6b5) Merge branch 'Amelioration_Multi_Edit_Etapes_j3pBarre' into 'main' (2024-01-17 15:52) <Yves Biton>  
`|\  `  
`| * `[bdc791ba8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bdc791ba8) Amélioration des étapes avec éditeurs internes : so correctionInterne est présent et vauxt 0, on barre les solutions fausses au lieu de seulement les mettre en rouge. (2024-01-17 15:51) <Yves Biton>  
`| * `[5ec5d1cc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ec5d1cc7) Correction pour les cas d'éditeurs internes et on barre les réponses fausses si correctionInterne vaut 0 (2024-01-17 12:41) <Yves Biton>  
`* |   `[5487b9401](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5487b9401) Merge branch 'aj_possib' into 'main' (2024-01-17 14:21) <Tommy Barroy>  
`|\ \  `  
`| * | `[10d8ee7f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10d8ee7f0) ajoute "possibles" (2024-01-17 14:20) <Tommy Barroy>  
`|/ /  `  
`* | `[e87545e45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e87545e45) retag 2.0.3 (2024-01-17 13:31) <static@sesamath-lampdev>  
`* |   `[669514a0d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/669514a0d) Merge branch 'Correction_Exos_Vecteurs' into 'main' (2024-01-17 12:58) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[5d4033995](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d4033995) Correction de la consigne de ces deux exos où on voyait des [em] incongrus davant la deuxième coordonnée des vecteurs. (2024-01-17 12:56) <Yves Biton>  
`|/  `  
`*   `[64a0751bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64a0751bd) Merge branch 'Amelioration_Doc_Formula_Comparator' into 'main' (2024-01-16 15:27) <Yves Biton>  
`|\  `  
`| * `[52075c52f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52075c52f) Correction du FormulaComparator et de sa doc (2024-01-16 15:25) <Yves Biton>  
`|/  `  
`*   `[cbfebbec6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cbfebbec6) Merge branch 'force_un_peu_aleatoire_pour_moins_de_doublons' into 'main' (2024-01-16 12:33) <Tommy Barroy>  
`|\  `  
`| * `[79435d5f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79435d5f1) force un peu aléatoire pour moins de doublons (2024-01-16 12:32) <Tommy Barroy>  
`* | `[d502919b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d502919b6) Merge branch 'force_un_peu_aleatoire_pour_moins_de_doublons' into 'main' (2024-01-15 21:43) <Tommy Barroy>  
`|\| `  
`| * `[471e1166f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/471e1166f) force un peu aléatoire pour moins de doublons (2024-01-15 21:42) <Tommy Barroy>  
`|/  `  
`*   `[3be900b90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3be900b90) Merge branch 'retire_cadre' into 'main' (2024-01-15 17:10) <Tommy Barroy>  
`|\  `  
`| * `[c634d113b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c634d113b) retie cadre (2024-01-15 17:09) <Tommy Barroy>  
`|/  `  
`* `[c6874a77f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6874a77f) retag 2.0.3 (2024-01-15 14:38) <static@sesamath-lampdev>  
`*   `[77d8850a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77d8850a5) Merge branch 'Correction_Section_Multi_Edit' into 'main' (2024-01-15 13:49) <Yves Biton>  
`|\  `  
`| * `[6bd454ed4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6bd454ed4) Modification de display pour qu'on puisse passer du html (span) avec un contenu texte et une couleur donnée avec un option cleanHtml à false. Modification de l'affichage de la réponse dans des éditeurs de texte à cause d'un bug de mathlive qui part exemple ne rend pas bien \textcolor{green}{\text{8*a}} (2024-01-15 13:47) <Yves Biton>  
`* |   `[05b80897a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05b80897a) Merge branch 'Exclure_des_solutions_possibles_dans_formuleMake' into 'main' (2024-01-14 22:24) <Tommy Barroy>  
`|\ \  `  
`| * | `[8a37e2854](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a37e2854) bouton en double au mauvais endroit (2024-01-14 22:23) <Tommy Barroy>  
`| * | `[d70bb03d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d70bb03d4) possibilité de mettre en pièges des équivalences de vignettes (2024-01-14 22:09) <Tommy Barroy>  
`|/ /  `  
`* / `[0645bb408](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0645bb408) retag 2.0.3 (2024-01-14 14:36) <static@sesamath-lampdev>  
`|/  `  
`*   `[c0c3b1a5f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0c3b1a5f) Merge branch 'Amelioration_Section_Multi_Edit' into 'main' (2024-01-14 13:26) <Yves Biton>  
`|\  `  
`| * `[8a1ddc73f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a1ddc73f) Pour ces deux sections les tag affectés aux Latex n'étaient pas tous insensibles à la casse. (2024-01-14 13:24) <Yves Biton>  
`* | `[e4db44262](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4db44262) retag 2.0.3 (2024-01-13 14:10) <static@sesamath-lampdev>  
`* |   `[9656c55f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9656c55f8) Merge branch 'oblig_vois_prog' into 'main' (2024-01-13 13:11) <Tommy Barroy>  
`|\ \  `  
`| * | `[c2f04ab8a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2f04ab8a) oblige voir prog rep (2024-01-13 13:11) <Tommy Barroy>  
`|/ /  `  
`* |   `[6f5c20f56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f5c20f56) Merge branch 'oubli_test_dans_yareponse' into 'main' (2024-01-13 11:33) <Tommy Barroy>  
`|\ \  `  
`| * | `[1eb3135cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1eb3135cf) oubli test dans yareponse (2024-01-13 11:32) <Tommy Barroy>  
`|/ /  `  
`* / `[c04841724](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c04841724) retag 2.0.3 (2024-01-12 18:39) <static@sesamath-lampdev>  
`|/  `  
`*   `[5aec0b152](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5aec0b152) Merge branch 'Correction_Validation_Textes_Ex_Calc_Multi_Edit' into 'main' (2024-01-12 18:00) <Yves Biton>  
`|\  `  
`| * `[d94ebb0c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d94ebb0c6) Correction de la gestion des réponses textes quand on ne tien pas compte de la casse dans la réponse. (2024-01-12 17:59) <Yves Biton>  
`|/  `  
`*   `[ae513d52f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae513d52f) Merge branch 'repareTests1' into 'main' (2024-01-12 13:04) <Jean-Claude Lhote>  
`|\  `  
`| * `[28c4909d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28c4909d9) fix: test ex_Calc_Multi_Edit.js réparé (2024-01-12 13:02) <Jean-claude Lhote>  
`* | `[3231416c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3231416c4) retag 2.0.3 (2024-01-11 13:44) <static@sesamath-lampdev>  
`* | `[36e0dc9f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/36e0dc9f0) retag 2.0.3 (2024-01-11 13:16) <static@sesamath-lampdev>  
`* |   `[0af480373](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0af480373) Merge branch 'Correction_Listes_Multi_Edit' into 'main' (2024-01-11 13:14) <Yves Biton>  
`|\ \  `  
`| * | `[c4facc55f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4facc55f) Correction de la gestion des listes déroulantes dans le cas d'éditeurs internes (2024-01-11 13:13) <Yves Biton>  
`* | |   `[1d4bebf93](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d4bebf93) Merge branch 'pas_de_correction_dans_editeur' into 'main' (2024-01-10 20:26) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[f362c6420](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f362c6420) oubli correction dans test de editeur (2024-01-10 20:26) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[3d5703918](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d5703918) Merge branch 'alea_sur_resMake2_et_etikMake' into 'main' (2024-01-10 20:04) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[e416903ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e416903ad) mm pb que resMake (2024-01-10 20:04) <Tommy Barroy>  
`|/ / /  `  
`* / / `[eb756317f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb756317f) retag 2.0.3 (2024-01-10 16:11) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[f740898ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f740898ec) Merge branch 'Correction_Squelette_Eq_Sol_Mult_1_Edit' into 'main' (2024-01-10 15:32) <Yves Biton>  
`|\ \  `  
`| * | `[63a9c3015](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63a9c3015) Correction d'un défaut signalé par Sébastien. (2024-01-10 15:30) <Yves Biton>  
`|/ /  `  
`* |   `[7d304847e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d304847e) Merge branch 'Amelioration_Restriction_Mathlive_Input' into 'main' (2024-01-10 14:44) <Yves Biton>  
`|\ \  `  
`| * | `[cba21acca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cba21acca) Amélioration de la gestion du filtrage des caractères clavier dans les éditeurs mathlive : le caractère de multiplication apparaissait même quand il était interdit par le charset (2024-01-10 14:42) <Yves Biton>  
`* | | `[e2dfe9015](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2dfe9015) retag 2.0.3 (2024-01-10 12:52) <static@sesamath-lampdev>  
`* | |   `[91603caa1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/91603caa1) Merge branch 'pb_alea_sur_resMake' into 'main' (2024-01-10 11:36) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[c38ee908d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c38ee908d) pb alea sur resMake (2024-01-10 11:35) <Tommy Barroy>  
`|/ /  `  
`* |   `[46e180414](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46e180414) Merge branch 'Amelioration_Controle_Section_Multi_Etapes' into 'main' (2024-01-10 11:02) <Yves Biton>  
`|\ \  `  
`| * | `[997e25d6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/997e25d6e) Amélioration des sections multi édit et multi étapes pour la vérification des objets qui doivent être présents dans la figure. (2024-01-10 11:01) <Yves Biton>  
`|/ /  `  
`* | `[97440f398](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97440f398) retag 2.0.3 (2024-01-09 18:55) <static@sesamath-lampdev>  
`* |   `[0e2d419a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e2d419a4) Merge branch 'Adaptation_Multi_Edit_Editeurs_Internes' into 'main' (2024-01-09 17:42) <Yves Biton>  
`|\ \  `  
`| * | `[94e9659cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94e9659cb) Amélioration des sections multi édit et multi étapes. La section multi édit permet maintenant de faire des exos avec des éditeurs internes à une figure Mathgraph32. Modification du clavier virtuel pour qu'il ne puisse pas apparaîre si l'éditeur a été désactivé via editeur.disabled = true Blindage des vérifications de la figure pour l'exercice multi édit. Reste à faire pour le multi étapes. (2024-01-09 17:41) <Yves Biton>  
`* | | `[dfd456c7f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfd456c7f) retag 2.0.3 (2024-01-08 16:29) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[03c116241](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03c116241) Merge branch 'Amelioration_Multi_Etapes' into 'main' (2024-01-08 15:48) <Yves Biton>  
`|\ \  `  
`| * | `[ab4e90019](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab4e90019) Amélioration de la section multi-étapes. On garde la couleur origine des éditeurs internes pour la restituer lors de la correction. On donne la possibilité via un calcul nommé correctionInterne suivi du numéro d'étape de valeur 0 de ne pas remplacer les réponses fausse de l'élève sur la figure lors de la correction. (2024-01-08 15:45) <Yves Biton>  
`* | | `[6d2e2861d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d2e2861d) retag 2.0.3 (2024-01-08 11:28) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[ab6845bd1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab6845bd1) Merge branch 'Correction_Section_Multi_Etapes' into 'main' (2024-01-08 10:48) <Yves Biton>  
`|\ \  `  
`| * | `[0b1898cdd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b1898cdd) Correction de la section multi-étapes dans le cas où elle contenait une liste déroulante dans un éditeur interne. (2024-01-08 10:46) <Yves Biton>  
`* | | `[d79d4fe11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d79d4fe11) retag 2.0.3 (2024-01-07 20:57) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[fec3212a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fec3212a8) Merge branch 'Adaptation_section_Multi_Edit_Old_Matrices' into 'main' (2024-01-07 20:20) <Yves Biton>  
`|\ \  `  
`| * | `[86c281d46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86c281d46) Modification de la section multi étapes pour que les exos avec anciens codes Mathquill pour les matrices fonctionnent (2024-01-07 20:18) <Yves Biton>  
`* | | `[ba784d382](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba784d382) retag 2.0.3 (2024-01-07 16:44) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[ac545ebc3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac545ebc3) Merge branch 'Passage_Mathlive_Squelette_Calc_Affine' into 'main' (2024-01-07 14:22) <Yves Biton>  
`|\ \  `  
`| * | `[ef008061f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef008061f) Passage sous Mathlive de la section Calc_Param_Affine et amélioration de la correction fichiers annexes avec correction par Mathlive (2024-01-07 14:20) <Yves Biton>  
`* | | `[f158d2375](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f158d2375) retag 2.0.3 (2024-01-06 19:57) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[545489830](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/545489830) Merge branch 'Passage_Mathlive_Section_eq_Sol_Mult_1_Edit' into 'main' (2024-01-06 19:09) <Yves Biton>  
`|\ \  `  
`| * | `[19ab89a35](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19ab89a35) Passage sous Mathlive de la section eq_Sol_Mult_1_Edit (2024-01-06 19:08) <Yves Biton>  
`* | |   `[59f30a795](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/59f30a795) Merge branch 'co_etik_support' into 'main' (2024-01-06 17:35) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[ad325059a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad325059a) support disparaition étiquette (2024-01-06 17:34) <Tommy Barroy>  
`* | |   `[49884ce2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49884ce2b) Merge branch 'Passage_Mathlive_Section_exconst' into 'main' (2024-01-06 17:20) <Yves Biton>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[657c1f9a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/657c1f9a8) Passage sous Mathlive de la section exconst (2024-01-06 17:19) <Yves Biton>  
`|/ /  `  
`* |   `[64169e13e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64169e13e) Merge branch 'Passage_Mathlive_exconstparam' into 'main' (2024-01-06 16:45) <Yves Biton>  
`|\ \  `  
`| * | `[64ac27f9a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64ac27f9a) Passage sous Mathlive de la section exconstparam (2024-01-06 16:44) <Yves Biton>  
`|/ /  `  
`* |   `[c91061570](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c91061570) Merge branch 'Passage_Mathlive_Section_Validation_Interne' into 'main' (2024-01-06 16:14) <Yves Biton>  
`|\ \  `  
`| * | `[acabecc0a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/acabecc0a) Passage sous Mathlive de la section ex_validation_Interne (2024-01-06 16:13) <Yves Biton>  
`|/ /  `  
`* |   `[a2e7954ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2e7954ea) Merge branch 'Passage_Mathlive_Section_Ineq' into 'main' (2024-01-06 14:22) <Yves Biton>  
`|\ \  `  
`| * | `[543a42d3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/543a42d3a) Passage sous Mathlive de la section ineq hélas quasi inutilisée (2024-01-06 14:21) <Yves Biton>  
`|/ /  `  
`* |   `[ffcc57fdf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffcc57fdf) Merge branch 'Ameioration_Dsisplay_Mat_Array' into 'main' (2024-01-06 11:56) <Yves Biton>  
`|\ \  `  
`| * | `[49e1bc0d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49e1bc0d0) Ammélioration de l'espacement entre les lignes des matrix et array en particulier pour les systèmes d'équations (2024-01-06 11:54) <Yves Biton>  
`* | | `[9380e83d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9380e83d5) retag 2.0.3 (2024-01-06 11:19) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[0df1f7d69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0df1f7d69) Merge branch 'Amelioration_Squelettes_Systemes' into 'main' (2024-01-06 10:40) <Yves Biton>  
`|\ \  `  
`| * | `[384611690](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/384611690) Ammélioration de l'espacement entre les lignes des matrix et array en particulier pour les systèmes d'équations (2024-01-06 10:37) <Yves Biton>  
`* | | `[ceeb9db2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ceeb9db2d) retag 2.0.3 (2024-01-05 22:44) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[4bc33e2c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4bc33e2c7) Merge branch 'Nettoyage_Sections_Mathlive' into 'main' (2024-01-05 18:17) <Yves Biton>  
`|\ \  `  
`| * | `[4807fe4d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4807fe4d9) Passage sous Mathlive du squeleyte de résolution de 3 équations linéaires et correction dy squelette de résolution de systèmes de deux équations. Passage sous Mathlive du squelette placer point dans rep. Nettoyage de code. (2024-01-05 18:14) <Yves Biton>  
`* | | `[1413a8a7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1413a8a7a) retag 2.0.3 (2024-01-05 13:15) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[ecbfad60e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecbfad60e) Merge branch 'Passage_Mathlive_Squelette_Calc_Param_Multi_Etapes' into 'main' (2024-01-05 12:40) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[632d41d37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/632d41d37) Passage sous Mathlive du squelette Para_Multi_Etapes et amélioration des ressources qui l'utilisent (affichage des solutions via Mathlive). (2024-01-05 12:13) <Yves Biton>  
`* | `[0aaddf2da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0aaddf2da) retag 2.0.3 (2024-01-04 13:44) <static@sesamath-lampdev>  
`|/  `  
`*   `[f135702e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f135702e4) Merge branch 'Correction_Multi_Etapes_Matrices' into 'main' (2024-01-04 13:05) <Yves Biton>  
`|\  `  
`| * `[61d7b76c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61d7b76c1) Correction de cette section qui ne marchait pas bien avec les matrices et qui n'acceptait pas R ou l'ensemble vide comme ensemble des solutions d'une inéquation. (2024-01-04 13:03) <Yves Biton>  
`|/  `  
`*   `[46f31ebfb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46f31ebfb) Merge branch 'Passage_Mathlive_Sqelette_Multi_Etapes' into 'main' (2024-01-04 10:56) <Yves Biton>  
`|\  `  
`| * `[66987476a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66987476a) Passage de ce squelette sous <Mathlive. Une seule ressource est concernée (2024-01-04 10:55) <Yves Biton>  
`* |   `[786276cd8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/786276cd8) Merge branch 'ajVid' into 'main' (2024-01-03 16:59) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[04f367613](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04f367613) aj vid dans 2 et refais curseur (2024-01-03 16:57) <Tommy Barroy>  
`| * `[6b2e93e0f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b2e93e0f) vire donnees et chnge place bouton (2024-01-02 20:38) <Tommy Barroy>  
`* |   `[cc457b092](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc457b092) Merge branch 'Ajout_Choix_Taille_Poice_Solution' into 'main' (2024-01-03 16:59) <Yves Biton>  
`|\ \  `  
`| * | `[a97646a98](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a97646a98) Possibilité pour ces deux sections de choisir une taille de police pour la solution (2024-01-03 16:57) <Yves Biton>  
`|/ /  `  
`* |   `[c8d51b120](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c8d51b120) Merge branch 'Amelioration_Gestion_Taille_Police' into 'main' (2024-01-03 16:09) <Yves Biton>  
`|\ \  `  
`| * | `[2172363eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2172363eb) Amlioration de ala gestion de l'ancien paramètre bigSize pour ces deux sections. Pour la section muti edit on peut maintenant mettre des tailles de olice en paramètres. (2024-01-03 16:07) <Yves Biton>  
`|/ /  `  
`* |   `[2f62eb99d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f62eb99d) Merge branch 'Amelioration_Multi_Etapes_Pour_Firefox' into 'main' (2024-01-03 14:28) <Yves Biton>  
`|\ \  `  
`| * | `[2d0f17f29](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d0f17f29) Petite modif de la section multi édit multi étapes pour que le clavier virtuel disparaisse lors de l'affichage de la solution. (2024-01-03 14:27) <Yves Biton>  
`* | | `[3031b4642](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3031b4642) retag 2.0.3 (2024-01-03 14:05) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[9a0d6611d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a0d6611d) Merge branch 'Passage_Mathlive_Section_Multi_Etapes' into 'main' (2024-01-03 13:31) <Yves Biton>  
`|\ \  `  
`| * | `[2fcd817ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2fcd817ba) La section multi édit multi étapes passe sous Mathlive (2024-01-03 13:29) <Yves Biton>  
`| * | `[f2e59e107](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2e59e107) Améliorations diverses. (2024-01-03 09:00) <Yves Biton>  
`| * | `[1a664341a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a664341a) La secttion multi étapes commence à bien fonctionner. Travail à finir. (2024-01-02 14:40) <Yves Biton>  
`* | | `[0df71c8a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0df71c8a8) retag 2.0.3 (2024-01-03 09:42) <static@sesamath-lampdev>  
`* | |   `[df1701767](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df1701767) Merge branch 'Correction_Keyboard_Shortcuts_Display_Mathlive' into 'main' (2024-01-03 09:03) <Yves Biton>  
`|\ \ \  `  
`| * | | `[2aa765ce4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2aa765ce4) Rajout de shortcuts oubliés (2024-01-03 09:02) <Yves Biton>  
`* | | | `[f3864ade8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3864ade8) retag 2.0.3 (2024-01-02 15:57) <static@sesamath-lampdev>  
`|/ / /  `  
`* | |   `[a1d9c55fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1d9c55fc) Merge branch 'Correction_Exo_Vecteurs' into 'main' (2024-01-02 14:44) <Yves Biton>  
`|\ \ \  `  
`| * | | `[aa2078c73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa2078c73) Correction de la correction dan le cas du vecteur JD (2024-01-02 14:43) <Yves Biton>  
`| |/ /  `  
`* / / `[6041cafac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6041cafac) retag 2.0.3 (2023-12-31 23:18) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[add780d18](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/add780d18) Merge branch 'Passage_Mathlive_ex_Calc_Vect' into 'main' (2023-12-31 22:16) <Yves Biton>  
`|\ \  `  
`| * | `[d1a9e022f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1a9e022f) Passage de la section ex_Calc_Vect sous Mathlive (2023-12-31 22:15) <Yves Biton>  
`|/ /  `  
`* |   `[6cb2fcf1d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cb2fcf1d) Merge branch 'Passage_Mathlive_Squelette_Question_Oui_Non' into 'main' (2023-12-31 17:36) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[e5a2c6393](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5a2c6393) Passage dous Mathlive du squelette question oui non. Passage en mode Mathlive de la correcton des exos associés. Rétablissment du focntionnement de l'i=option replacefracdfrac de afficheMathlive (2023-12-31 17:34) <Yves Biton>  
`* | `[6af010bf1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6af010bf1) retag 2.0.3 (2023-12-31 13:40) <static@sesamath-lampdev>  
`|/  `  
`*   `[1bd4ad14e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1bd4ad14e) Merge branch 'Passage_Mathlive_Squelette_Prod_Scalaire' into 'main' (2023-12-31 12:32) <Yves Biton>  
`|\  `  
`| * `[c7484fffd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7484fffd) Le squelette de calcul de produit scalaire passe sous Mathlive. Amélioration des exercices associés (suppression affichage figure dans SVG quand inutile) (2023-12-31 12:30) <Yves Biton>  
`* | `[f12db6b02](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f12db6b02) retag 2.0.3 (2023-12-30 20:04) <static@sesamath-lampdev>  
`|/  `  
`*   `[077cc8acd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/077cc8acd) Merge branch 'Correction_Squelette_Eq_Vect_Param' into 'main' (2023-12-30 19:29) <Yves Biton>  
`|\  `  
`| * `[6fa5cb85f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6fa5cb85f) Correction de traiteMathlive pour la section eq_Param_Vect Dans display, on n'enrichit pas les keyshortcuts déjà définis, mais on ne garde que les deux qui nous sont utiles (2023-12-30 19:27) <Yves Biton>  
`* | `[9741ecfd6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9741ecfd6) retag 2.0.3 (2023-12-30 18:41) <static@sesamath-lampdev>  
`|/  `  
`*   `[b8c17bb34](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8c17bb34) Merge branch 'Suppression_Affichage_Fig_Exos_Calcul' into 'main' (2023-12-30 18:07) <Yves Biton>  
`|\  `  
`| * `[c469a5f1f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c469a5f1f) La correction de tous les exercices concernés par ces fichiers annexes passe sous Mathlive. (2023-12-30 18:05) <Yves Biton>  
`|/  `  
`*   `[b52a534bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b52a534bd) Merge branch 'Amelioration_Exos_Calcul' into 'main' (2023-12-30 12:38) <Yves Biton>  
`|\  `  
`| * `[d5802dc65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5802dc65) Suppression de l'afichage de figure inutile dans des exercices de développement (2023-12-30 12:35) <Yves Biton>  
`* |   `[0d6d073c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d6d073c1) Merge branch 'vire_donnes' into 'main' (2023-12-30 12:20) <Tommy Barroy>  
`|\ \  `  
`| * | `[5ceb6e950](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ceb6e950) vire donnees et chnge place bouton (2023-12-30 12:19) <Tommy Barroy>  
`* | | `[31ffb49c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31ffb49c6) retag 2.0.3 (2023-12-30 12:15) <static@sesamath-lampdev>  
`| |/  `  
`|/|   `  
`* |   `[f8783d6bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8783d6bc) Merge branch 'Passage_Mathlive_Squelette_Calc_vect' into 'main' (2023-12-30 11:19) <Yves Biton>  
`|\ \  `  
`| * | `[b1da034ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1da034ee) Passage sous Mathlive du squelette de calcul vectoriel et amélioraton des fichiers annexes associés. (2023-12-30 11:16) <Yves Biton>  
`| * | `[04bf4e329](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04bf4e329) Amélioration de la geston des affichages des fractions par afficheMathlive. Modification du focus pour deux squelettes. (2023-12-29 17:12) <Yves Biton>  
`* | | `[2a44b8709](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a44b8709) retag 2.0.3 (2023-12-28 18:03) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[642844924](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/642844924) Merge branch 'Passage_Mathlvie_Squelette_Systeme_Param_Remanie' into 'main' (2023-12-28 16:58) <Yves Biton>  
`|\ \  `  
`| * | `[72c7585e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72c7585e1) Passage du squelette de résolution de système 2x2 à mathlive. et les exercices ont leur correction en mode Mathlive. (2023-12-28 16:56) <Yves Biton>  
`| * | `[2f44c42d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f44c42d1) Passage du squelette de résolution de système 2x2 à mathlive. (2023-12-28 11:57) <Yves Biton>  
`* | | `[d9e6601ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9e6601ce) retag 2.0.3 (2023-12-28 09:18) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[ac5acbfb0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac5acbfb0) Merge branch 'Correction_Exo_Eq_Droite' into 'main' (2023-12-27 23:49) <Yves Biton>  
`|\ \  `  
`| * | `[c5bf74211](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c5bf74211) Correction de la correction de cet exercice (dernière ligne) (2023-12-27 23:47) <Yves Biton>  
`* | | `[e42b1fc63](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e42b1fc63) retag 2.0.3 (2023-12-27 23:32) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[f0134836b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0134836b) Merge branch 'Passage_Mathlive_Squelette_Reduc_Eq_Droite' into 'main' (2023-12-27 21:49) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[188fb73ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/188fb73ea) Passage du squelette de réduction d'équation de droite Mathlive. Exercices l'utilisant améliorés avec affichage de la correction via Mathlive. Amélioration du squelette de réduction au même dénominateur et de ses exercices. (2023-12-27 21:45) <Yves Biton>  
`|/  `  
`*   `[c35206c48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c35206c48) Merge branch 'vire_console' into 'main' (2023-12-27 17:31) <Tommy Barroy>  
`|\  `  
`| * `[a3e427654](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3e427654) vire console (2023-12-27 17:31) <Tommy Barroy>  
`* | `[79e140e35](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79e140e35) retag 2.0.3 (2023-12-27 17:31) <static@sesamath-lampdev>  
`* | `[f0dae0101](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0dae0101) retag 2.0.3 (2023-12-27 16:49) <static@sesamath-lampdev>  
`|/  `  
`*   `[f423960dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f423960dc) Merge branch 'là' into 'main' (2023-12-27 16:45) <Tommy Barroy>  
`|\  `  
`| * `[8288f4a6b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8288f4a6b) bugsnag, et oubli coordonnées en double (2023-12-27 16:44) <Tommy Barroy>  
`* |   `[ef1c19012](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef1c19012) Merge branch 'Adaptation_Mathlive_Squelette_Somme_Frac' into 'main' (2023-12-27 14:17) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[150e6f17f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/150e6f17f) Passage du squelette d'addition de frcations avec Mathlive. Exercices l'utilisant améliorés. (2023-12-27 14:15) <Yves Biton>  
`* |   `[f72603694](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f72603694) Merge branch 're_re_re_mohascratch' into 'main' (2023-12-27 13:44) <Tommy Barroy>  
`|\ \  `  
`| * | `[fc6f6a75f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fc6f6a75f) alea pas bien reglé, affine position y sur axe (2023-12-27 13:43) <Tommy Barroy>  
`* | | `[4471abb94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4471abb94) retag 2.0.3 (2023-12-27 13:15) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[f4bbd82aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f4bbd82aa) Merge branch 're_finish_mohascratch' into 'main' (2023-12-27 12:43) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[ebb28b8a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ebb28b8a4) fais bien axe (2023-12-27 12:42) <Tommy Barroy>  
`* |   `[deaa0159c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/deaa0159c) Merge branch 'Correction_RenderMathInElement_Inutiles' into 'main' (2023-12-27 10:15) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[6449b7e1c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6449b7e1c) Suppression des renderMathInElement inutiles dans la fonction recopierREponse. Correction de l'affichage de la réponse dans le squelette de réduction de fraction. (2023-12-27 10:14) <Yves Biton>  
`* |   `[3bda392e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3bda392e4) Merge branch 'fixBugDecembre' into 'main' (2023-12-27 09:11) <Rémi Deniaud>  
`|\ \  `  
`| * | `[5ffdf0b95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ffdf0b95) contournement d'un pb de paramétrage dans naturesuite et SuiteTermeGeneral (2023-12-27 09:09) <Rémi Deniaud>  
`| * | `[4aa26649e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4aa26649e) modifs dans les fonctions sur les tableaux de variations (2023-12-26 19:50) <Rémi Deniaud>  
`* | | `[257accb64](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/257accb64) retag 2.0.3 (2023-12-27 02:21) <static@sesamath-lampdev>  
`* | |   `[23803d346](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23803d346) Merge branch 'pti_buf_main' into 'main' (2023-12-27 01:11) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[cc6608a2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc6608a2d) pti big main (2023-12-27 01:11) <Tommy Barroy>  
`* | | | `[9c8b14062](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c8b14062) retag 2.0.3 (2023-12-26 19:47) <static@sesamath-lampdev>  
`| |_|/  `  
`|/| |   `  
`* | |   `[c4f11c8d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4f11c8d6) Merge branch 'Passage_Mathlive_Squelette_Op_Matrices' into 'main' (2023-12-26 19:15) <Yves Biton>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[38c4d99b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38c4d99b8) Les squelette de calcul matriciel passe à Mathlive et les matrices qui contiennent des fractions les affichent toutes avec des dfrac. Ma correctionde tous les exercices associés est maintenant affchée par Mathlive. (2023-12-26 19:13) <Yves Biton>  
`| * | `[66011658a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66011658a) Première version qui fonctionne avec le squelette de calcul sur les matrices. (2023-12-25 18:45) <Yves Biton>  
`* | |   `[588ec8637](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/588ec8637) Merge branch 'dvptcanonique' into 'main' (2023-12-26 19:09) <Rémi Deniaud>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[26e2edc25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/26e2edc25) ajout de la possibilité d'imposer les valeurs 1 ou -1 dans dvptverscanonique (2023-12-26 19:06) <Rémi Deniaud>  
`|/ /  `  
`* | `[fd4df7696](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd4df7696) retag 2.0.3 (2023-12-26 14:59) <static@sesamath-lampdev>  
`* |   `[6351a0191](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6351a0191) Merge branch 'met_url_dans_image' into 'main' (2023-12-26 13:09) <Tommy Barroy>  
`|\ \  `  
`| * | `[8a4d87715](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a4d87715) met url dans images (2023-12-26 13:07) <Tommy Barroy>  
`|/ /  `  
`* |   `[da5369efc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da5369efc) Merge branch 'finish_moha' into 'main' (2023-12-26 12:30) <Tommy Barroy>  
`|\ \  `  
`| * | `[fd1d4e1e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd1d4e1e0) corrige bug main et cache plus le résultat prog élève pour la correction (2023-12-26 12:28) <Tommy Barroy>  
`|/ /  `  
`* |   `[1087e075e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1087e075e) Merge branch 'amelio_mohascracth' into 'main' (2023-12-24 00:37) <Tommy Barroy>  
`|\ \  `  
`| * | `[fc8b11549](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fc8b11549) ajoute image , axe et lutin (2023-12-24 00:36) <Tommy Barroy>  
`* | | `[6d3fcba18](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d3fcba18) retag 2.0.3 (2023-12-23 22:11) <static@sesamath-lampdev>  
`* | | `[8412a17d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8412a17d1) retag 2.0.3 (2023-12-23 21:28) <static@sesamath-lampdev>  
`| |/  `  
`|/|   `  
`* |   `[741eca079](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/741eca079) Merge branch 'Pour_Cacher_le_clavier_virtuel_Mathlive' into 'main' (2023-12-23 21:26) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[cb81a7cbc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb81a7cbc) Pour que le clavier virtuel mathlive n'apparaisse jamais (avait été supprimé par erreur) (2023-12-23 21:24) <Yves Biton>  
`* |   `[f9030fae0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f9030fae0) Merge branch 'aj_url_son' into 'main' (2023-12-23 15:18) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[c31a5f226](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c31a5f226) ajoute URL pour sons (2023-12-23 15:17) <Tommy Barroy>  
`* |   `[9e2947236](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e2947236) Merge branch 'Passage_Mathlive_Squelette_Ineq_1_Edit' into 'main' (2023-12-23 14:47) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[ceb20f3c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ceb20f3c1) Le squelette Ineq_Param_1_Edit et la correction des exercices associés est maintenant affichée par Mathlive (2023-12-23 14:45) <Yves Biton>  
`* |   `[bfc569182](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bfc569182) Merge branch 'bug_blokmtg' into 'main' (2023-12-22 16:27) <Tommy Barroy>  
`|\ \  `  
`| * | `[295cc37ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/295cc37ca) les arcs , angles orientés et rotation doivent empecher la symétrie (2023-12-22 16:24) <Tommy Barroy>  
`* | | `[377b0c73f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/377b0c73f) retag 2.0.3 (2023-12-22 15:25) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[8b316db67](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b316db67) Merge branch 'amelio_editeur_scratch' into 'main' (2023-12-22 14:54) <Tommy Barroy>  
`|\ \  `  
`| * | `[7bda94907](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7bda94907) amelio moha scratch editeur (2023-12-22 14:52) <Tommy Barroy>  
`|/ /  `  
`* | `[9f143689d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f143689d) retag 2.0.3 (2023-12-22 11:10) <static@sesamath-lampdev>  
`* |   `[c15f764dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c15f764dc) Merge branch 'amelio_mtg' into 'main' (2023-12-22 08:48) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[16fd0d5bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16fd0d5bd) amelio lettres simpl (2023-12-22 08:47) <Tommy Barroy>  
`* |   `[291167bbb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/291167bbb) Merge branch 'Passage_Mathlive_SqueletteIneq_Apres_Ens_Def' into 'main' (2023-12-22 08:45) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[6524b67d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6524b67d4) Le squelette Ineq_Param_Apres_Ens_Def passe sous Mathlive (2023-12-22 08:43) <Yves Biton>  
`* | `[b7bbd1dfe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7bbd1dfe) retag 2.0.3 (2023-12-20 22:29) <static@sesamath-lampdev>  
`|/  `  
`*   `[bc1895aad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bc1895aad) Merge branch 'Passage_Mathlive_Squelette_Reduc_Meme_Denom' into 'main' (2023-12-20 21:14) <Yves Biton>  
`|\  `  
`| * `[2737af99f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2737af99f) Le squelette Reduc_Meme_Denom_Param passe sous Mathlive (2023-12-20 21:13) <Yves Biton>  
`* | `[0230d325f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0230d325f) retag 2.0.3 (2023-12-20 17:12) <static@sesamath-lampdev>  
`|/  `  
`*   `[1d1c41635](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d1c41635) Merge branch 'Passage_Mathlive_Squelette_Multi_Edit_2_Etapes' into 'main' (2023-12-20 15:15) <Yves Biton>  
`|\  `  
`| * `[22702b415](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22702b415) Le squelette Calc_Param_Multi_Edit_2_Etapes passe sous Mathlive et n'affiche plus de figure mtg32 car ttes les corrections passent par Mathlive (2023-12-20 15:14) <Yves Biton>  
`| * `[c056f4142](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c056f4142) Modifié dasn main par erreur (2023-12-20 13:08) <Yves Biton>  
`* |   `[6a1835c4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a1835c4b) Merge branch 'oubli_modif_rot_et_sym' into 'main' (2023-12-20 14:57) <Tommy Barroy>  
`|\ \  `  
`| * | `[de0f5b3b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de0f5b3b8) oubli modif rot sym, et - dans angle (2023-12-20 14:56) <Tommy Barroy>  
`|/ /  `  
`* / `[7fefc14c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7fefc14c6) retag 2.0.3 (2023-12-20 10:09) <static@sesamath-lampdev>  
`|/  `  
`*   `[a750044dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a750044dc) Merge branch 'arc_dans_co' into 'main' (2023-12-20 09:38) <Tommy Barroy>  
`|\  `  
`| * `[b415d4e8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b415d4e8c) pti bug de nom et arc dans correction (2023-12-20 09:35) <Tommy Barroy>  
`* |   `[9672f3674](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9672f3674) Merge branch 'Amelioration_Suites_Pour_Olivier_Jacc' into 'main' (2023-12-20 09:02) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[47f3bb029](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47f3bb029) On autorise les parenthèses quand on demande une formule (2023-12-20 08:59) <Yves Biton>  
`* |   `[5bc8603a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5bc8603a0) Merge branch 'remet_visible' into 'main' (2023-12-20 01:28) <Tommy Barroy>  
`|\ \  `  
`| * | `[ad78c5f14](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad78c5f14) pti bug de nom (2023-12-20 01:27) <Tommy Barroy>  
`* | | `[12e406e9f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12e406e9f) Merge branch 'remet_visible' into 'main' (2023-12-20 00:42) <Tommy Barroy>  
`|\| | `  
`| * | `[05eee8624](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05eee8624) remet visible une ancienne étiquette (2023-12-20 00:41) <Tommy Barroy>  
`* | | `[ec109e1bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec109e1bf) retag 2.0.3 (2023-12-20 00:29) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[b359fd8cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b359fd8cc) Merge branch 'ajoute_arc' into 'main' (2023-12-19 23:04) <Tommy Barroy>  
`|\ \  `  
`| * | `[334d0a543](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/334d0a543) pb dans affiche nom points (2023-12-19 23:03) <Tommy Barroy>  
`* | | `[4c4628d1d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c4628d1d) Merge branch 'ajoute_arc' into 'main' (2023-12-19 22:36) <Tommy Barroy>  
`|\| | `  
`| * | `[4f84a777c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f84a777c) aj arc dans fig dep (2023-12-19 22:20) <Tommy Barroy>  
`|/ /  `  
`* |   `[5e8d3d056](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e8d3d056) Merge branch 'amelio_un_peu_terrestre' into 'main' (2023-12-19 20:30) <Tommy Barroy>  
`|\ \  `  
`| * | `[40f9744d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/40f9744d4) bloque Nord Sud quand sphere terrestre (2023-12-19 20:30) <Tommy Barroy>  
`* | | `[d4f8bea48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4f8bea48) retag 2.0.3 (2023-12-19 17:40) <static@sesamath-lampdev>  
`| |/  `  
`|/|   `  
`* |   `[79b979a71](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79b979a71) Merge branch 'Passage_Mathlive_Squelette_Multi_Edit_Apres_Interm' into 'main' (2023-12-19 16:54) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[72665d99c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72665d99c) Passage de la section calc_Param_Edit_Apres_Interm sous mathlive avec correction possible en HTML via mathlive. Correction de marquePourErreur et demarquePourErreur de deux autres sections. (2023-12-19 16:52) <Yves Biton>  
`* |   `[afc49f8af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/afc49f8af) Merge branch 're_edit' into 'main' (2023-12-19 16:01) <Tommy Barroy>  
`|\ \  `  
`| * | `[7b153c65b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b153c65b) aj point rotatio pour editoeur (2023-12-19 16:00) <Tommy Barroy>  
`|/ /  `  
`* |   `[fdb33b89c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fdb33b89c) Merge branch 'aj_point_rot' into 'main' (2023-12-19 15:55) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[d92c0f66f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d92c0f66f) aj point rotatio pour editoeur (2023-12-19 15:54) <Tommy Barroy>  
`* |   `[194d1cb73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/194d1cb73) Merge branch 'Passage_Mathlive_Squelette_Multi_Edt_Bloc' into 'main' (2023-12-19 13:35) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[f28e0f533](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f28e0f533) Passage de la section calc_Param_Edit_Bloc sous mathlive avec correction possible en HTML via mathlive (2023-12-19 13:34) <Yves Biton>  
`* |   `[85e60ccc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85e60ccc7) Merge branch 'am_blmtg' into 'main' (2023-12-19 12:42) <Tommy Barroy>  
`|\ \  `  
`| * | `[c6252e246](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6252e246) pour sauv (2023-12-19 12:41) <Tommy Barroy>  
`| * | `[b6873b25e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6873b25e) pour sauv (2023-12-19 09:46) <Tommy Barroy>  
`| * | `[b62ef4423](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b62ef4423) facto nom point try (2023-12-18 21:11) <Tommy Barroy>  
`| * | `[5ebc99d4a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ebc99d4a) amelio et ajute modif (2023-12-17 18:58) <Tommy Barroy>  
`* | | `[365b4fefb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/365b4fefb) retag 2.0.3 (2023-12-19 12:23) <static@sesamath-lampdev>  
`| |/  `  
`|/|   `  
`* |   `[d2ae12bd4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2ae12bd4) Merge branch 'passage_Mathlive_Squelette_Multi_Edit_Param' into 'main' (2023-12-19 10:55) <Yves Biton>  
`|\ \  `  
`| * | `[e56f3fdfd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e56f3fdfd) Ajout de resetKeyboardPosition oublié (2023-12-19 10:53) <Yves Biton>  
`| * | `[a03e778a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a03e778a8) Passage du squelette Calc_Multi_Edit_Param sous Mathlive. Amélioration de MathliveAffiche pour que le premier affichage se fasse dans un <span> et pas dans un <p>. Quelques fichiers annexes améliorés pour la présentation. (2023-12-19 10:49) <Yves Biton>  
`* | |   `[f8df9fb19](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8df9fb19) Merge branch 'bugsnag2' into 'main' (2023-12-18 21:22) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[90146fb0d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90146fb0d) erreur nom obj (2023-12-18 21:21) <Tommy Barroy>  
`|/ / /  `  
`* / / `[0693bc211](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0693bc211) retag 2.0.3 (2023-12-18 20:31) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[954b2e20f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/954b2e20f) Merge branch 'Passage_Mathlve_Squelette_Multi_Edit' into 'main' (2023-12-18 18:24) <Yves Biton>  
`|\ \  `  
`| * | `[b30045090](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b30045090) Passage du squelette calc_Multi_Edit sous Mathlive. Correction mineure de ex_Calc_Multi_Edit Ajout de paramètres oubliès à renderMathInDocument() Améliorationde afficheMathliveDans pour éviter des avertissents en console pour des id multiples (2023-12-18 18:22) <Yves Biton>  
`* | | `[5faf5c767](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5faf5c767) retag 2.0.3 (2023-12-18 12:05) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[12516eecf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12516eecf) Merge branch 'Passage_Mathlive_Squelette_Deriv_Param' into 'main' (2023-12-18 11:24) <Yves Biton>  
`|\ \  `  
`| * | `[3d6d35db5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d6d35db5) Passage du squelette de calcul de dérivées sous Mathlive (2023-12-18 11:14) <Yves Biton>  
`|/ /  `  
`* |   `[6edf4e5c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6edf4e5c3) Merge branch 'Ajout_Filtrage_HTML_afficheMathlive' into 'main' (2023-12-18 10:35) <Yves Biton>  
`|\ \  `  
`| * | `[ade47ab51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ade47ab51) ON filtre dans afficheMathquill les balises indésirables et autres comme dans j3pAffiche (2023-12-18 10:34) <Yves Biton>  
`* | | `[99b161199](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/99b161199) retag 2.0.3 (2023-12-17 21:43) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[9e67b6d85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e67b6d85) Merge branch 'Optimisation_Positionnement_Clavier_Virtuel' into 'main' (2023-12-17 21:03) <Yves Biton>  
`|\ \  `  
`| * | `[efd7cb5f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/efd7cb5f6) Aélioration du positionnement du clavier virtuel (2023-12-17 21:01) <Yves Biton>  
`* | | `[0673c2246](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0673c2246) retag 2.0.3 (2023-12-17 19:37) <static@sesamath-lampdev>  
`* | |   `[3f93cd5f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f93cd5f9) Merge branch 'support' into 'main' (2023-12-17 19:07) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[0330a0a0a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0330a0a0a) support (2023-12-17 19:06) <Tommy Barroy>  
`|/ /  `  
`* |   `[7602451c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7602451c7) Merge branch 'Ajout_Appel_setMathliveCss' into 'main' (2023-12-17 10:30) <Yves Biton>  
`|\ \  `  
`| * | `[fdcd1f936](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fdcd1f936) Ajout de setMathlivecss dans les sections où c'était oublié Suppression de la fonction setKeyboardcss qui fait doublon. (2023-12-17 10:29) <Yves Biton>  
`* | | `[bddbe9967](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bddbe9967) retag 2.0.3 (2023-12-17 09:40) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[152f6e780](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/152f6e780) Merge branch 'Rajout_Appel_Css_Oublie' into 'main' (2023-12-17 08:52) <Yves Biton>  
`|\ \  `  
`| * | `[b99f2e219](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b99f2e219) Le squelette de réduction progressive de fraction passe sous Mathlive. Augmentation du padding haut et bas utilisé pour les paragraphes affichés par afficheMathliveDans. (2023-12-17 08:51) <Yves Biton>  
`* | | `[038902d94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/038902d94) retag 2.0.3 (2023-12-16 17:33) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[07477409f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07477409f) Merge branch 'Passage_Mathlive_Squelette_Reduc_Frac' into 'main' (2023-12-16 17:00) <Yves Biton>  
`|\ \  `  
`| * | `[4ba1e2185](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ba1e2185) Le squelette de réduction progressive de fraction passe sous Mathlive. Augmentation du padding haut et bas utilisé pour les paragraphes affichés par afficheMathliveDans. (2023-12-16 16:58) <Yves Biton>  
`* | | `[fecabaf9c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fecabaf9c) retag 2.0.3 (2023-12-16 14:36) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[15474a33b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15474a33b) Merge branch 'Correction_Touches_Inegalite' into 'main' (2023-12-16 14:03) <Yves Biton>  
`|\ \  `  
`| * | `[960d72668](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/960d72668) Il faut supprimer le raccourci par défaut pour la touche < car sinon tout déconne dès qu'on a tapé la touche < (2023-12-16 14:02) <Yves Biton>  
`| * | `[576edd2e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/576edd2e5) Il faut supprimer le raccourci par défaut pour la touche < car sinon tout déconne dès qu'on a tapé la touche < (2023-12-16 14:01) <Yves Biton>  
`|/ /  `  
`* |   `[519fc96dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/519fc96dd) Merge branch 'Correction_RecopierReponse' into 'main' (2023-12-16 13:17) <Yves Biton>  
`|\ \  `  
`| * | `[ad1b5525a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad1b5525a) Je remets les renderMathInElement dans recopierReponse car ça ne fonctionne plus bien (2023-12-16 13:15) <Yves Biton>  
`|/ /  `  
`* |   `[4a213266a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a213266a) Merge branch 'Passage_Mathlive_Squelette_Ineq' into 'main' (2023-12-16 12:18) <Yves Biton>  
`|\ \  `  
`| * | `[4f5a07c6b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f5a07c6b) Passage du squelette Ineq_Param sous Mathlive. Suppression d'un renderMathInElement inutile dans toutes les fonctions recopier. Suppression de paramètres inutiles dans les appels à mathliveRestrict Pas de focus donné à l'éditeur dans une fonction validation car cela pose des problèmes de focus à la deuxième étape. Petite correction des énoncés d'exercices de résolution d'inéquation. (2023-12-16 12:15) <Yves Biton>  
`|/ /  `  
`* | `[f1c1955e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1c1955e7) retag 2.0.3 (2023-12-15 23:19) <static@sesamath-lampdev>  
`* |   `[e9640ee2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9640ee2e) Merge branch 'passage_Mathlive_Squelette_Eq_Param_2I_nc' into 'main' (2023-12-15 22:48) <Yves Biton>  
`|\ \  `  
`| * | `[73fe0c014](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/73fe0c014) Passage du squelette Eq_Param_2_Inc sous Mathlive (2023-12-15 22:46) <Yves Biton>  
`* | |   `[fb7f2e932](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb7f2e932) Merge branch 'fixTestExCalcMultiEdit' into 'main' (2023-12-15 19:51) <Jean-Claude Lhote>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[6bea434e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6bea434e9) réparer le test qui dysfonctionnait après le passage à Mathlive (2023-12-15 19:48) <Jean-claude Lhote>  
`| * |   `[f26fbffff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f26fbffff) Merge remote-tracking branch 'origin/main' (2023-12-15 19:47) <Jean-claude Lhote>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | |   `[da8683828](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da8683828) Merge branch 'Passage_Mathlive_Squelette_eq_Vect' into 'main' (2023-12-15 14:38) <Yves Biton>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[1bd69e1ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1bd69e1ca) Passage du squelette d'équation vectorielle sous MathQuill (2023-12-15 14:36) <Yves Biton>  
`|/ /  `  
`* | `[545b3079d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/545b3079d) retag 2.0.3 (2023-12-15 13:49) <static@sesamath-lampdev>  
`* |   `[5e8e9243a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e8e9243a) Merge branch 'support' into 'main' (2023-12-15 13:20) <Tommy Barroy>  
`|\ \  `  
`| * | `[d9038a8fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9038a8fe) revert des commit (2023-12-15 13:19) <Tommy Barroy>  
`| * | `[2e19b0a4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e19b0a4b) Revert "refait une string" (2023-12-15 13:07) <Tommy Barroy>  
`|/ /  `  
`* | `[034e8572c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/034e8572c) retag 2.0.3 (2023-12-15 11:57) <static@sesamath-lampdev>  
`* | `[d94cd53f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d94cd53f0) retag 2.0.3 (2023-12-15 11:16) <static@sesamath-lampdev>  
`* |   `[277407fdf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/277407fdf) Merge branch 'aj_avertFond' into 'main' (2023-12-15 11:15) <Tommy Barroy>  
`|\ \  `  
`| * | `[6f52b57e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f52b57e5) oubli de sauvegarde message fond (2023-12-15 11:14) <Tommy Barroy>  
`|/ /  `  
`* |   `[02d9b657b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02d9b657b) Merge branch 'co_poseOp' into 'main' (2023-12-15 10:52) <Tommy Barroy>  
`|\ \  `  
`| * | `[51331235d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51331235d) refait une string (2023-12-15 10:51) <Tommy Barroy>  
`|/ /  `  
`* |   `[d5273e0ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5273e0ba) Merge branch 'amelio_image_clic_editor' into 'main' (2023-12-15 10:13) <Tommy Barroy>  
`|\ \  `  
`| * | `[b7a7ab626](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7a7ab626) Les zones se créent au click sur figure (2023-12-15 10:12) <Tommy Barroy>  
`* | |   `[340d7a650](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/340d7a650) Merge branch 'Correction_unlatexify_Balises_U' into 'main' (2023-12-15 09:28) <Yves Biton>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[496ad76e3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/496ad76e3) Correction pour ne pas supprimer les balises <u> (2023-12-15 09:26) <Yves Biton>  
`|/ /  `  
`* / `[9e334037d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e334037d) Pour me retrouver avec les identations de 2 caractères (2023-12-15 08:51) <Yves Biton>  
`|/  `  
`*   `[6511b8349](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6511b8349) Merge branch 'fixExCalcMultiEdit' into 'main' (2023-12-15 07:34) <Jean-Claude Lhote>  
`|\  `  
`| * `[7ebe438b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ebe438b8) supprimer l'accessibleContent des Mathlive pour éviter d'avoir le texte en double dans les pages ce qui fait planter les tests (2023-12-15 07:31) <Jean-claude Lhote>  
`| *   `[57c0a85c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57c0a85c5) Merge remote-tracking branch 'origin/main' (2023-12-15 04:28) <Jean-claude Lhote>  
`| |\  `  
`| |/  `  
`|/|   `  
`* | `[bbade93a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bbade93a8) retag 2.0.3 (2023-12-14 22:27) <static@sesamath-lampdev>  
`* |   `[06c0e7c61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06c0e7c61) Merge branch 'co_editor_resMake2' into 'main' (2023-12-14 21:40) <Tommy Barroy>  
`|\ \  `  
`| * | `[5ae776b26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ae776b26) co newExo dns editor (2023-12-14 21:39) <Tommy Barroy>  
`|/ /  `  
`* | `[2a1ef041f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a1ef041f) retag 2.0.3 (2023-12-14 20:37) <static@sesamath-lampdev>  
`* |   `[2897e979e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2897e979e) Merge branch 'aj_survol' into 'main' (2023-12-14 19:55) <Tommy Barroy>  
`|\ \  `  
`| * | `[a976fea40](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a976fea40) ajout option visible au survol (2023-12-14 19:54) <Tommy Barroy>  
`* | |   `[026888b98](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/026888b98) Merge branch 'blindageBlokmtg01' into 'main' (2023-12-14 19:23) <Jean-Claude Lhote>  
`|\ \ \  `  
`| * | | `[07407a7e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07407a7e1) ajout d'optionnal chaining pour éviter l'erreur bloquant le test. (2023-12-14 19:23) <Jean-Claude Lhote>  
`|/ / /  `  
`* | | `[645767089](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/645767089) retag 2.0.3 (2023-12-14 18:36) <static@sesamath-lampdev>  
`* | |   `[5492df423](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5492df423) Merge branch 'Passage_Mathlive_squelette_Eq_Sol_Mult' into 'main' (2023-12-14 17:29) <Yves Biton>  
`|\ \ \  `  
`| * | | `[a2cc8fc3c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2cc8fc3c) Le squelette Eq-Param_Sol_Mult_Passe sous Mathlive Correction du squelette Eq_Param_Mathquill (2023-12-14 17:28) <Yves Biton>  
`* | | | `[ebd71824d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ebd71824d) retag 2.0.3 (2023-12-14 16:38) <static@sesamath-lampdev>  
`| |/ /  `  
`|/| |   `  
`* | |   `[5e7b2faab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e7b2faab) Merge branch 'explication' into 'main' (2023-12-14 16:11) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[16ce8f109](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16ce8f109) augmente choix type img (2023-12-14 16:09) <Tommy Barroy>  
`| * | | `[0a9d0c194](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a9d0c194) ajoute des explications (2023-12-14 15:29) <Tommy Barroy>  
`|/ / /  `  
`* | | `[c4485259f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4485259f) retag 2.0.3 (2023-12-14 14:23) <static@sesamath-lampdev>  
`* | | `[90413cca9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90413cca9) retag 2.0.3 (2023-12-14 13:52) <static@sesamath-lampdev>  
`* | |   `[d553efb76](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d553efb76) Merge branch 'fait_pas_enregistrer_son_et_image_inutiles' into 'main' (2023-12-14 13:51) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[3caa6a40b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3caa6a40b) pour pas enregistrer de fichiers inutiles (2023-12-14 13:51) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[030febe85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/030febe85) Merge branch 'fixPoseOperation' into 'main' (2023-12-14 12:52) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[8f550438b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f550438b) ca a l'air d'etre bon (2023-11-30 23:22) <Tommy Barroy>  
`| * | | `[6992e9a15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6992e9a15) rajoute '$' pour mathquill affiche (2023-11-30 16:30) <Tommy Barroy>  
`| * | | `[4bf44d6ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4bf44d6ec) Affiche correctement les nombres (avec séparateur de milliers) (2023-11-30 15:31) <Daniel Caillibaud>  
`* | | |   `[2aa0a56c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2aa0a56c8) Merge branch 'fixMultipleVoc' into 'main' (2023-12-14 12:50) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[35fffe723](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35fffe723) pour support (2023-12-14 12:48) <Tommy Barroy>  
`| * | | | `[50ba93dca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50ba93dca) change correction pour rappeler se terminent pas 0, 2, 4, ou 5 (2023-11-29 21:39) <Tommy Barroy>  
`| * | | | `[e5eef21d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5eef21d2) getDonnees inutile (+ surcharge + une tonne de textes inutilisés) virés (2023-11-28 10:32) <Daniel Caillibaud>  
`| * | | | `[0dbdbe6d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0dbdbe6d4) grammaire (nb termine par => nb se termine par) (2023-11-28 10:30) <Daniel Caillibaud>  
`* | | | |   `[aaec8a45e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aaec8a45e) Merge branch 'amelio_Zone' into 'main' (2023-12-14 12:19) <Tommy Barroy>  
`|\ \ \ \ \  `  
`| |_|_|/ /  `  
`|/| | | |   `  
`| * | | | `[572c0277e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/572c0277e) zones plus jolies (2023-12-14 12:17) <Tommy Barroy>  
`| * | | | `[2cf3946b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2cf3946b1) zones plus jolies (2023-12-14 12:16) <Tommy Barroy>  
`* | | | |   `[6a8784275](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a8784275) Merge branch 'Passage_Mathlive_Squelette_Eq_1_Edit' into 'main' (2023-12-14 12:17) <Yves Biton>  
`|\ \ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`| * | | | `[5b13f4af3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b13f4af3) La correction est maintenant en mode html mathlive. Dans l'énoncé, les coordonnées des vecteurs sont en colonne. (2023-12-14 12:16) <Yves Biton>  
`* | | | |   `[5683811b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5683811b7) Merge branch 'facto_image' into 'main' (2023-12-14 09:37) <Tommy Barroy>  
`|\ \ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`| * | | | `[8b672e934](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b672e934) facto image (2023-12-14 09:18) <Tommy Barroy>  
`* | | | |   `[d842b285a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d842b285a) Merge branch 'Amelioration_Ex_Eq_Cartesienne' into 'main' (2023-12-14 09:16) <Yves Biton>  
`|\ \ \ \ \  `  
`| |/ / / /  `  
`|/| | | |   `  
`| * | | | `[853776e61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/853776e61) La correction est maintenant en mode html mathlive. Dans l'énoncé, les coordonnées des vecteurs sont en colonne. (2023-12-14 09:13) <Yves Biton>  
`| | |_|/  `  
`| |/| |   `  
`* | | |   `[7c36db41f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c36db41f) Merge branch 'test_deporte_son_sur_serveur_disatnt' into 'main' (2023-12-13 23:01) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[722c40225](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/722c40225) finish (2023-12-13 22:59) <Tommy Barroy>  
`| * | | | `[25e861d40](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25e861d40) finish (2023-12-13 22:36) <Tommy Barroy>  
`| * | | | `[83e808869](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83e808869) pour sauv (2023-12-13 21:26) <Tommy Barroy>  
`* | | | | `[8168ae453](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8168ae453) retag 2.0.3 (2023-12-13 19:07) <static@sesamath-lampdev>  
`| |/ / /  `  
`|/| | |   `  
`* | | |   `[9a565d5af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a565d5af) Merge branch 'Adaptation_Squelette_Eq_Param_Mathlive' into 'main' (2023-12-13 17:32) <Yves Biton>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[8414280ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8414280ed) La section squelette ex_Eq_Param_MathQuil utilise maitenant Mathlive (2023-12-13 17:31) <Yves Biton>  
`|/ / /  `  
`* | |   `[e2a58543f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2a58543f) Merge branch 'plus_joli_editeur_blokmtg' into 'main' (2023-12-12 20:54) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[2000c6b61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2000c6b61) new tete (2023-12-12 20:52) <Tommy Barroy>  
`| * | | `[b7c1cac4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7c1cac4b) pour signalement et un bugsnag (2023-12-12 20:51) <Tommy Barroy>  
`|/ / /  `  
`* | | `[9f96ff463](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f96ff463) retag 2.0.3 (2023-12-12 18:10) <static@sesamath-lampdev>  
`* | |   `[84d41e2df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84d41e2df) Merge branch 'co_stat_bug_signalement' into 'main' (2023-12-12 17:41) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[302a65a5f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/302a65a5f) pour signalement et un bugsnag (2023-12-12 17:37) <Tommy Barroy>  
`|/ / /  `  
`* | | `[792d4bb21](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/792d4bb21) retag 2.0.3 (2023-12-11 13:38) <static@sesamath-lampdev>  
`* | |   `[eb8e79caa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb8e79caa) Merge branch 'Amelioration_squelette_Calc_Sans_Entete' into 'main' (2023-12-11 13:05) <Yves Biton>  
`|\ \ \  `  
`| * | | `[a709e4d49](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a709e4d49) La section squelette ex_Calc_Param_MathQuill.Sans_Entete peut maintenant avoir sa correction donnée dans un Latex de tag solution (2023-12-11 13:04) <Yves Biton>  
`|/ / /  `  
`* | | `[6cf9fc111](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cf9fc111) retag 2.0.3 (2023-12-11 10:55) <static@sesamath-lampdev>  
`* | |   `[74323c8d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/74323c8d2) Merge branch 'Correction_Mathlive_Display_Plusieurs_Lignes' into 'main' (2023-12-11 10:23) <Yves Biton>  
`|\ \ \  `  
`| * | | `[ece27b256](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ece27b256) Correction de display qui ne marchait pas bien dans le vas de plusieurs lignes (icônes clavier Mathlive et menu Mathlive à partir de la deuxième) (2023-12-11 10:22) <Yves Biton>  
`|/ / /  `  
`* | | `[39eb51b55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39eb51b55) retag 2.0.3 (2023-12-11 06:40) <static@sesamath-lampdev>  
`* | |   `[cf1f5c303](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf1f5c303) Merge branch 'petit_bug_dans_test_exo_de_l_editeur' into 'main' (2023-12-10 21:38) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[b6b9b401f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6b9b401f) vire les bulles d'aide dans l'éditeur et met phrase entiere dans test exo (2023-12-10 21:31) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[4fbf3c3e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fbf3c3e2) Merge branch 'finish_facto_son' into 'main' (2023-12-10 20:55) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[fe7947ebf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe7947ebf) facto son (2023-12-10 20:53) <Tommy Barroy>  
`* | | | `[64ffe60e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64ffe60e0) retag 2.0.3 (2023-12-10 15:59) <static@sesamath-lampdev>  
`* | | |   `[68d95260e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68d95260e) Merge branch 'Adaptation_Squelette_CalcSansEnTete_Mathlive' into 'main' (2023-12-10 15:15) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[92498e059](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/92498e059) La suelette Calc_Param_MatQuill_Sans_Entete pass à mathlive. Correction mineure sur deux autres squelettes (pas d'erreur de syntaxe si réponse vide) (2023-12-10 15:14) <Yves Biton>  
`|/ / / /  `  
`* | | |   `[cf5bcb2a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf5bcb2a8) Merge branch 'adaptation_Squelette_Calcul_Mathlive' into 'main' (2023-12-10 12:49) <Yves Biton>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[52114f30e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52114f30e) Correction de le gestion du conteneur de haut niveau du clavier virtuel et la section squelettemtg32_Calc_Param_Mathquill passe sous Mathlive (2023-12-10 12:47) <Yves Biton>  
`* | | |   `[6a0904393](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a0904393) Merge branch 'focto_son' into 'main' (2023-12-10 10:57) <Tommy Barroy>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[7b2577471](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b2577471) facto son (2023-12-10 10:56) <Tommy Barroy>  
`* | | |   `[13e7817d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13e7817d8) Merge branch 'orrection_Squelette_Factorisation' into 'main' (2023-12-09 20:26) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[02e118379](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02e118379) Le squelette de factorisation ne répondait plus au clavier à la deuxième répétition (2023-12-09 20:24) <Yves Biton>  
`|/ / / /  `  
`* | | |   `[248582737](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/248582737) Merge branch 'Correction_Erreurs_Mathlive_Console' into 'main' (2023-12-09 19:29) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[d149fb464](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d149fb464) ON abandonne de mettre un \; quand l'éditeur mathlive est vide cela n'apporte que des emmerdes et des erreurs en conole si on vveut déplacer le curseur (2023-12-09 19:28) <Yves Biton>  
`|/ / / /  `  
`* | | |   `[002bd7d1f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/002bd7d1f) Merge branch 'Correction_Virtual_Keyboard' into 'main' (2023-12-09 18:31) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[ed0400b77](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed0400b77) Le squelette de factorisation passe sous MathLive. Amélioration des éditeurs Mathlive. (2023-12-09 18:29) <Yves Biton>  
`|/ / / /  `  
`* | | |   `[74edae992](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/74edae992) Merge branch 'Passage_Squelette_Factorisation_Mathlive' into 'main' (2023-12-09 17:42) <Yves Biton>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[55dc7430a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55dc7430a) Le squelette de factorisation passe sous MathLive. Amélioration des éditeurs Mathlive. (2023-12-09 17:41) <Yves Biton>  
`| * | | `[bba9341db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bba9341db) Début d'adaptation_squlelette_Factorisation_Mathlive (2023-12-09 11:39) <Yves Biton>  
`* | | |   `[fee3435ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fee3435ff) Merge branch 'essai_sans_couleur' into 'main' (2023-12-09 15:09) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[7ff3247a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ff3247a0) enleve les couleurs , ca faisait sapin de noel (2023-12-09 14:50) <Tommy Barroy>  
`* | | | | `[f54925eba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f54925eba) retag 2.0.3 (2023-12-09 14:56) <static@sesamath-lampdev>  
`|/ / / /  `  
`* | | |   `[85945605f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85945605f) Merge branch 'test_pour_redev' into 'main' (2023-12-09 11:03) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[ef592e553](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef592e553) met ordre dans exos (2023-12-09 11:02) <Tommy Barroy>  
`|/ / / /  `  
`* / / / `[b0448dd83](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0448dd83) retag 2.0.3 (2023-12-09 09:47) <static@sesamath-lampdev>  
`|/ / /  `  
`* | |   `[822b143f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/822b143f4) Merge branch 'Version_Mathlive_0.98.4' into 'main' (2023-12-09 08:18) <Yves Biton>  
`|\ \ \  `  
`| * | | `[6637d90b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6637d90b9) Version de Mathlive -> 0.98.4 (2023-12-09 08:16) <Yves Biton>  
`|/ / /  `  
`* | |   `[c630d8ded](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c630d8ded) Merge branch 'met_ordre_dans_exos' into 'main' (2023-12-09 01:34) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[4a72b4460](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a72b4460) met ordre dans exos (2023-12-09 01:33) <Tommy Barroy>  
`* | | | `[806eda5b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/806eda5b0) retag 2.0.3 (2023-12-09 00:10) <static@sesamath-lampdev>  
`|/ / /  `  
`* | |   `[0cebdb1bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0cebdb1bf) Merge branch 'try_pour_jc' into 'main' (2023-12-09 00:07) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[51e6a6ac2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51e6a6ac2) pour virer le bug, mais je capte pas (2023-12-09 00:06) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[3a758686f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a758686f) Merge branch 'etiMak' into 'main' (2023-12-08 23:38) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[0d7c3e0af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d7c3e0af) terminé (2023-12-08 23:26) <Tommy Barroy>  
`| * | | `[63a66a4c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63a66a4c1) sauv (2023-12-08 05:48) <Tommy Barroy>  
`| * | | `[02730dcf9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02730dcf9) sauv (2023-12-07 14:10) <Tommy Barroy>  
`| * | | `[73d550838](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/73d550838) sauv (2023-12-07 13:03) <Tommy Barroy>  
`| * | | `[491378989](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/491378989) sauv (2023-12-06 23:25) <Tommy Barroy>  
`| * | | `[214c9696b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/214c9696b) sauv (2023-12-06 17:06) <Tommy Barroy>  
`| * | | `[7d9a69ad7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d9a69ad7) sauv (2023-12-05 21:23) <Tommy Barroy>  
`| * | | `[2882cf53d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2882cf53d) suv (2023-12-05 12:44) <Tommy Barroy>  
`| * | | `[a057a74c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a057a74c3) sauv (2023-12-05 09:13) <Tommy Barroy>  
`| * | | `[bf174665b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf174665b) sauv (2023-12-05 09:13) <Tommy Barroy>  
`| * | | `[24e88f614](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24e88f614) sauv (2023-12-05 09:13) <Tommy Barroy>  
`* | | |   `[723c53c38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/723c53c38) Merge branch 'Correcion_Clavier_Virtuel' into 'main' (2023-12-08 23:30) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[9ba159eb0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ba159eb0) Corrige le dyysfonctionnement du clavier virtuel sur ordinateur sans écran tactile (2023-12-08 23:28) <Yves Biton>  
`|/ / / /  `  
`* | | | `[244d5d736](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/244d5d736) retag 2.0.3 (2023-12-08 17:50) <static@sesamath-lampdev>  
`* | | |   `[62bab1521](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/62bab1521) Merge branch 'Amelioration_Ergonomie_Clavier_Virtuel' into 'main' (2023-12-08 17:09) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[ce8dd8847](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce8dd8847) Le bouton standard mathquill est maintenant remplacé comme avant pas des flèches. Les éditeurs de texte ont aussi leur flèche. Meilleure gestion du clavier virtue: en cas d'erreur de syntaxe, le clavier virtuel enlève aussi le marquage pour erreur. (2023-12-08 17:06) <Yves Biton>  
`|/ / / /  `  
`* | | | `[deb652296](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/deb652296) retag 2.0.3 (2023-12-06 15:59) <static@sesamath-lampdev>  
`* | | |   `[a8470a716](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8470a716) Merge branch 'Factorisation_Code_Mathlive' into 'main' (2023-12-06 15:22) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[6b60e4c19](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b60e4c19) Version où les fonctions d'affichages Mathlive sont mutualisées (2023-12-06 15:21) <Yves Biton>  
`* | | | | `[c24237a58](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c24237a58) retag 2.0.3 (2023-12-06 09:40) <static@sesamath-lampdev>  
`|/ / / /  `  
`* | | |   `[12e9c1cc5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12e9c1cc5) Merge branch 'erreur_sur_nb_essais_resMake' into 'main' (2023-12-06 02:58) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[44c5be4d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44c5be4d0) sauv (2023-12-06 02:57) <Tommy Barroy>  
`|/ / / /  `  
`* | | | `[c7d1d7356](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7d1d7356) retag 2.0.3 (2023-12-05 19:46) <static@sesamath-lampdev>  
`* | | |   `[776300fe3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/776300fe3) Merge branch 'Correction_Double_Appui_x_Mathlive' into 'main' (2023-12-05 19:01) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[97e3891dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97e3891dd) Modif pour qu'un double appui de la touche x dans les éditeurs mathquill ne donne pas un signe de multiplication. (2023-12-05 19:00) <Yves Biton>  
`|/ / / /  `  
`* | | |   `[ee5b1d11b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee5b1d11b) Merge branch 'testBlokMtg01SurLaTouche' into 'main' (2023-12-05 18:00) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| * | | | `[08216bff3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/08216bff3) Je désactive le test blokmtg01 en attendant réparation de la section. (2023-12-05 17:59) <Jean-claude Lhote>  
`* | | | |   `[823069075](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/823069075) Merge branch 'Remplacement_Clavier_Mathlive_Dans_Multi_Edit' into 'main' (2023-12-05 17:18) <Yves Biton>  
`|\ \ \ \ \  `  
`| * | | | | `[9bb254d69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9bb254d69) Dans tous les exercices mulit-edit le clavier virtuel matquill est remplacé par le clavier virtuel mathlive. Optimisation de cleanLatexForML dans section excalmathquill (2023-12-05 17:16) <Yves Biton>  
`* | | | | |   `[d80b10589](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d80b10589) Merge branch 'egalite_formelle_dans_resMake' into 'main' (2023-12-05 16:39) <Tommy Barroy>  
`|\ \ \ \ \ \  `  
`| |_|/ / / /  `  
`|/| | | | |   `  
`| * | | | | `[201d94f15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/201d94f15) egalite formelle dans resMake (2023-12-05 16:38) <Tommy Barroy>  
`| |/ / / /  `  
`* / / / / `[2e5e6c93b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e5e6c93b) retag 2.0.3 (2023-12-05 13:40) <static@sesamath-lampdev>  
`|/ / / /  `  
`* | | |   `[cdf2d4d02](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cdf2d4d02) Merge branch 'Correction_Verification_Conteneur_Clavier_Virtuel' into 'main' (2023-12-05 11:58) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[6f5df2e0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f5df2e0c) Correction de la création du conteneur du clavier virtuel qui ne vérifiait pas si le conteneur le contenait ou pas (2023-12-05 11:56) <Yves Biton>  
`|/ / / /  `  
`* | | | `[8d6374f0a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d6374f0a) retag 2.0.3 (2023-12-05 09:56) <static@sesamath-lampdev>  
`* | | |   `[55871e9a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55871e9a1) Merge branch 'Amelioration_Tablettes_Mathlive' into 'main' (2023-12-05 09:25) <Yves Biton>  
`|\ \ \ \  `  
`| * | | | `[efc76b1d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/efc76b1d1) Ajout d'un inputmode='none' aux éditeur sde texte pour qu'ils ne déclenchent plus le clavier virtuel des tablettes (2023-12-05 09:23) <Yves Biton>  
`|/ / / /  `  
`* | | | `[06750986b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06750986b) Commit de Project.xml. Je comprends pas pourquoi ce fichier a été ajouté au projet ... (2023-12-05 08:48) <Yves Biton>  
`* | | | `[0e2d92e8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e2d92e8d) retag 2.0.3 (2023-12-05 06:51) <static@sesamath-lampdev>  
`|/ / /  `  
`* | |   `[ca89ce948](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca89ce948) Merge branch 'bug_editor_resMake' into 'main' (2023-12-05 06:16) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[fd47fdf0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd47fdf0e) il n'y a plus d'editor.consigne (2023-12-05 06:15) <Tommy Barroy>  
`|/ / /  `  
`* | |   `[0942d21a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0942d21a6) Merge branch 'bugPix' into 'main' (2023-12-04 21:08) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[e4d24bac9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4d24bac9) bug figures mal enregistree pixelMake (2023-12-04 21:07) <Tommy Barroy>  
`|/ / /  `  
`* | | `[58edd37c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58edd37c0) retag 2.0.3 (2023-12-04 18:25) <static@sesamath-lampdev>  
`* | |   `[ecd8ea0d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecd8ea0d0) Merge branch 'Adaptation_MuliEdit_Mathlive' into 'main' (2023-12-04 17:50) <Yves Biton>  
`|\ \ \  `  
`| * \ \   `[ffb758a44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffb758a44) Merge branch 'main' into 'Adaptation_MuliEdit_Mathlive' (2023-12-04 17:48) <Yves Biton>  
`| |\ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`* | | | `[db54a2c1b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db54a2c1b) retag 2.0.3 (2023-12-03 23:04) <static@sesamath-lampdev>  
`* | | |   `[2236ebc93](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2236ebc93) Merge branch 'oubli8' into 'main' (2023-12-03 22:05) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[3f1148780](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f1148780) oubli89 (2023-12-03 22:04) <Tommy Barroy>  
`|/ / / /  `  
`* | | | `[925a56b3e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/925a56b3e) retag 2.0.3 (2023-12-03 21:44) <static@sesamath-lampdev>  
`* | | |   `[44959d6b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44959d6b3) Merge branch 'formule_dans_pixel' into 'main' (2023-12-03 21:14) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[94c5645ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94c5645ea) editeur dans le dernier et change couleur (2023-12-03 21:13) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[fb587ad51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb587ad51) Merge branch 'formule_dans_formule' into 'main' (2023-12-03 14:57) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[6f874e0e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f874e0e2) editeur de formule dans editeur de ressouce formuleMake (2023-12-03 14:56) <Tommy Barroy>  
`|/ / / /  `  
`* | | | `[db0fd70f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db0fd70f9) retag 2.0.3 (2023-12-03 00:43) <static@sesamath-lampdev>  
`* | | | `[cdc7b3f3e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cdc7b3f3e) retag 2.0.3 (2023-12-03 00:14) <static@sesamath-lampdev>  
`* | | | `[310efd65b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/310efd65b) retag 2.0.3 (2023-12-03 00:14) <static@sesamath-lampdev>  
`* | | |   `[e9ede4585](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9ede4585) Merge branch 'formulator' into 'main' (2023-12-03 00:12) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[50ab17ce7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50ab17ce7) modif editeur de formule dans editeur de ressouce Make (2023-12-03 00:11) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[434482ba0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/434482ba0) Merge branch 'taille_div_edit' into 'main' (2023-12-02 15:50) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[0e52a7ec9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e52a7ec9) adapte en largeur (2023-12-02 15:49) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[1892b5e66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1892b5e66) Merge branch 're_pixel' into 'main' (2023-12-01 16:39) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[1e7fbc02b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e7fbc02b) adapte taille div liste modèle et met des couleurs pour différencier les modèles (2023-12-01 16:38) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[ba9ba4b89](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba9ba4b89) Merge branch 'taille_div_editor' into 'main' (2023-12-01 16:27) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[ec9baf312](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec9baf312) adapte taille div vignette et mets des couleurs pour bien distinguer tout (2023-11-30 22:55) <Tommy Barroy>  
`* | | | | `[c7521378f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7521378f) aj commentaires (2023-12-01 15:25) <Daniel Caillibaud>  
`* | | | | `[60c83ca49](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/60c83ca49) fix loader (2023-11-30 23:48) <Daniel Caillibaud>  
`|/ / / /  `  
`* | | | `[e75630376](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e75630376) affichage des n° de ligne pour les erreurs d'imports statiques avec extension (2023-11-30 19:25) <Daniel Caillibaud>  
`* | | | `[a3934ca03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3934ca03) loadAll un peu moins verbeux (2023-11-30 19:08) <Daniel Caillibaud>  
`* | | |   `[33cc77d33](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33cc77d33) Merge branch 'editeur_en_full_screen' into 'main' (2023-11-30 18:30) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[02f0a0f3c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02f0a0f3c) addFullScreenBtn passe privé, fullScreen devient toggleFullscreen et passe privé (2023-11-30 18:29) <Daniel Caillibaud>  
`| * | | | `[af5425e21](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af5425e21) il vaut mieux sortir du fullscreen juste avant de résoudre la promesse (2023-11-30 18:29) <Daniel Caillibaud>  
`| * | | | `[77283d1f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77283d1f2) vire les ? qui veulent dire required apparemment (2023-11-30 18:29) <Tommy Barroy>  
`| * | | | `[586218ace](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/586218ace) bouton full screen dans les editeurs de params (2023-11-30 18:29) <Tommy Barroy>  
`|/ / / /  `  
`* | | |   `[3ab55851a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ab55851a) Merge branch 'vitestBrowser' into 'main' (2023-11-30 18:10) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| |_|_|/  `  
`|/| | |   `  
`| * | | `[476422a42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/476422a42) MathfieldElement est undefined dans les tests happy-dom (2023-11-30 18:08) <Daniel Caillibaud>  
`| * | | `[219972dfe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/219972dfe) commentaires (2023-11-30 18:06) <Daniel Caillibaud>  
`| * | | `[5b3b86fdf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b3b86fdf) images importées (2023-11-30 18:06) <Daniel Caillibaud>  
`| * | | `[26fd00f7b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/26fd00f7b) on râle plus si la purge http plante sur un autre site que sesamath (2023-11-30 18:06) <Daniel Caillibaud>  
`| * | | `[ee93f9c90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee93f9c90) rectif nom du log (2023-11-30 18:06) <Daniel Caillibaud>  
`| * | | `[d70b112e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d70b112e5) sections.usage.json pris en dev (2023-11-30 18:06) <Daniel Caillibaud>  
`| * | | `[2a7ca98bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a7ca98bd) harmonisation nom de fichier et màj doc (2023-11-30 18:06) <Daniel Caillibaud>  
`| * | | `[5f873383c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f873383c) prefs webstorm pour imports sans extension (2023-11-30 18:06) <Daniel Caillibaud>  
`| * | | `[ca572e965](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca572e965) Refonte complète de testBrowser, qui utilise désormais vitest (pour avoir la même méthode de chargement / résolution des modules js/ts que pour le build ou pnpm start) (2023-11-30 18:06) <Daniel Caillibaud>  
`| * | | `[ae8a2e62f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae8a2e62f) alert de debug viré (2023-11-30 18:06) <Daniel Caillibaud>  
`| * | | `[b6aa97a29](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6aa97a29) il ne faut plus préciser d'extension dans les imports statiques (2023-11-30 18:05) <Daniel Caillibaud>  
`| * | | `[3b1120859](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b1120859) conf eslint en doublon virée (elle est dans eslintrc.cjs) (2023-11-30 18:04) <Daniel Caillibaud>  
`| * | | `[f30704905](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f30704905) fix test calculTerme (2023-11-30 18:04) <Daniel Caillibaud>  
`| * | | `[b892a4d56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b892a4d56) fix check des exports de section (2023-11-30 18:04) <Daniel Caillibaud>  
`|/ / /  `  
`* | | `[d267c6586](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d267c6586) Ajout de la fct formatNumber (pour améliorer j3pNombreBienEcrit) (2023-11-30 15:29) <Daniel Caillibaud>  
`* | | `[7e78301a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e78301a3) bugfix j3pVirgule sur les 0 non significatifs (2023-11-30 10:20) <Daniel Caillibaud>  
`* | |   `[08cd04ca9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/08cd04ca9) Merge branch 'ticket2' into 'main' (2023-11-29 21:31) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[0f999491d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f999491d) [fix #228](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/228) (2023-11-29 21:20) <Tommy Barroy>  
`| * | | `[8396a5fd1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8396a5fd1) [fix #229](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/229) (2023-11-29 20:59) <Tommy Barroy>  
`| * | | `[cf8e08187](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf8e08187) [fix #220](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/220) (2023-11-29 20:51) <Tommy Barroy>  
`|/ / /  `  
`* | | `[ebe825ba8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ebe825ba8) orthographe (2023-11-29 19:45) <Daniel Caillibaud>  
`* | |   `[5afd82467](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5afd82467) Merge branch 'editeur_fonction' into 'main' (2023-11-29 14:12) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[6358dbd23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6358dbd23) met bouton formle dans editeur consigne (2023-11-29 14:10) <Tommy Barroy>  
`|/ / /  `  
`* | | `[0068bbedf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0068bbedf) police icomoon virée (ne servait que pour le doigt devant le titre, viré par ailleurs) (2023-11-28 20:58) <Daniel Caillibaud>  
`* | |   `[1b03a78fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b03a78fb) Merge branch 'amelio_editor_encore' into 'main' (2023-11-28 20:09) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[9cfc0a766](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9cfc0a766) ameliore rendu et taille input (2023-11-28 20:08) <Tommy Barroy>  
`| | * |   `[38097c797](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38097c797) Merge remote-tracking branch 'origin/main' into Adaptation_MuliEdit_Mathlive (2023-12-04 16:24) <Yves Biton>  
`| | |\ \  `  
`| |_|/ /  `  
`|/| | |   `  
`* | | |   `[c0e688176](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0e688176) Merge branch 'Retire_la_main_en_début_de_titre' into 'main' (2023-11-28 19:19) <Yves Biton>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[6037ffa62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6037ffa62) Retire la main du début du titre (2023-11-28 19:15) <Yves Biton>  
`* | | | `[5305d4ad2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5305d4ad2) modif message affiché (2023-11-28 13:48) <Daniel Caillibaud>  
`|/ / /  `  
`| * | `[fb3961cac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb3961cac) Pour la secton excalcmathquill on revient  à la méthode du fond rouge pour nune faute de syntaxe (2023-12-04 16:21) <Yves Biton>  
`| * | `[cd87cbc7b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd87cbc7b) Version multi edit qui fonctionne bien avec Mathlive avec compatibilité commandes Latex pour mathquill. Petit changement dans le css (2023-12-04 15:49) <Yves Biton>  
`| * | `[146f1d38a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/146f1d38a) Version multi edit qui fonctionne bien avec Mathlive avec compatibilité commandes Latex pour mathquill (2023-12-04 15:31) <Yves Biton>  
`| * | `[7e94ef850](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e94ef850) Première version avec un clavier virtuel disponible aussi pour les éditeurs de texte. (2023-12-01 22:40) <Yves Biton>  
`| * | `[657c2d1b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/657c2d1b8) Lorsqu'on a une erreur de syntaxe dans un éditeur Mathfield ne comprenant pas de placeholder, on le met sur fonc rouge clair. (2023-12-01 11:14) <Yves Biton>  
`| * | `[b236874df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b236874df) Couleur vraiment rouge pour un math-field contenant une réponse faute. (2023-11-30 21:00) <Yves Biton>  
`| * | `[398fe1aeb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/398fe1aeb) Première version prenant en charge les listes déroulantes (2023-11-30 20:08) <Yves Biton>  
`| * | `[61ffe3a8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61ffe3a8b) Quand on recopie, on donne le focus au premier placeHolder en récrivant dedans son contenu (2023-11-29 17:50) <Yves Biton>  
`| * | `[ff9bfed95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff9bfed95) Quand on recopie, on donne le focus au premier placeHolder en récrivant dedans son contenu (2023-11-29 17:46) <Yves Biton>  
`| * | `[4e633f423](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e633f423) Première version du multi edit utilisant mathlive avec gestion des \editable{} (2023-11-29 17:33) <Yves Biton>  
`| * | `[0dba4cfdf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0dba4cfdf) Debut d'adaptation à Mathlive des ressources multi edit (2023-11-28 19:07) <Yves Biton>  
`|/ /  `  
`* |   `[ab3082e37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab3082e37) Merge branch 'Correction_AfficheMathlive' into 'main' (2023-11-28 13:30) <Yves Biton>  
`|\ \  `  
`| * | `[721ffcc5f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/721ffcc5f) Correction de cleanLatexForML dans lequel certains $$ vides faisaient déconner Mathlive (2023-11-28 13:29) <Yves Biton>  
`|/ /  `  
`* | `[0c68f90f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c68f90f9) fix refreshUsage (faut les suffixe pour un chargement par node) (2023-11-28 12:28) <Daniel Caillibaud>  
`* |   `[3a0817ef7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a0817ef7) Merge branch 'excalcmathquil_utilise_mathlive' into 'main' (2023-11-28 12:27) <Yves Biton>  
`|\ \  `  
`| * | `[9d5c98e90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d5c98e90) Les sections excalcmathlive et excalcmathlivecustomkbd sont supprimées. Le contenu de excalcmathlivecustomkbd est mis dans excalcmathquill (2023-11-28 12:04) <Yves Biton>  
`| |/  `  
`* / `[77036f9d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77036f9d8) retag 2.0.3 (2023-11-28 10:49) <static@sesamath-lampdev>  
`|/  `  
`*   `[83d9ef191](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83d9ef191) Merge branch 'Amelioration_Polices_Clavier_Virtuel_Mathlive' into 'main' (2023-11-28 10:07) <Yves Biton>  
`|\  `  
`| * `[dc67bffa2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc67bffa2) Le clavier virtuel ne se déplaçait plus automatiquement. Les lettres minuscules du clavier virtuel passent en italiques? (2023-11-28 10:06) <Yves Biton>  
`* |   `[75c86b5af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75c86b5af) Merge branch 'ref' into 'main' (2023-11-27 21:58) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[5618a87b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5618a87b3) ameliore rendu et taille input (2023-11-27 21:57) <Tommy Barroy>  
`| * `[8a6df8d91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a6df8d91) sauv (2023-11-27 21:52) <Tommy Barroy>  
`|/  `  
`*   `[50e4c4dd5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50e4c4dd5) Merge branch 'Consolidation_Code_Clavier_Virtuel' into 'main' (2023-11-27 17:58) <Yves Biton>  
`|\  `  
`| * `[162a3d20d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/162a3d20d) Amélioration de la doc du clavier virtuel. Amélioration de l'icône pour la puissance d'un nombre (2023-11-27 17:56) <Yves Biton>  
`* | `[b2f7cbf62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2f7cbf62) retag 2.0.3 (2023-11-27 16:22) <static@sesamath-lampdev>  
`|/  `  
`*   `[1e067b86f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e067b86f) Merge branch 'Amelioration_Shox_Clavier_Virtuel_Mathlive' into 'main' (2023-11-27 15:51) <Yves Biton>  
`|\  `  
`| * `[200d57319](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/200d57319) Au focus, le clavier virtuel ne devrait apparâitre que sur périphériques avec écran tactile (2023-11-27 15:50) <Yves Biton>  
`|/  `  
`* `[d927e200e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d927e200e) retag 2.0.3 (2023-11-27 11:47) <static@sesamath-lampdev>  
`*   `[def88b253](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/def88b253) Merge branch 'Amelioration_Clavier_Virtuel_Mathlive' into 'main' (2023-11-27 11:07) <Yves Biton>  
`|\  `  
`| * `[629ca7781](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/629ca7781) Ajout de boutons annuler-refaire au clavier virtuel mathquill (2023-11-27 11:05) <Yves Biton>  
`|/  `  
`*   `[34209ffac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34209ffac) Merge branch 'Correction_Cedille_Ex_Fact' into 'main' (2023-11-26 22:53) <Yves Biton>  
`|\  `  
`| * `[e2dc21bec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2dc21bec) Correction de cédille manquante (2023-11-26 22:50) <Yves Biton>  
`* |   `[2396af45f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2396af45f) Merge branch 'test_modale_hors_iframe' into 'main' (2023-11-26 20:42) <Tommy Barroy>  
`|\ \  `  
`| * | `[d7e49e141](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7e49e141) test , je commit, je push, je deploy dev, puis je decommit, repush redeploy dev (2023-11-26 20:41) <Tommy Barroy>  
`* | | `[e665c0bc6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e665c0bc6) Merge branch 'test_modale_hors_iframe' into 'main' (2023-11-26 20:38) <Tommy Barroy>  
`|\| | `  
`| * | `[235b620f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/235b620f5) test , je commit, je push, je deploy dev, puis je decommit, repush redeploy dev (2023-11-26 20:37) <Tommy Barroy>  
`* | | `[75ede231f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75ede231f) Merge branch 'test_modale_hors_iframe' into 'main' (2023-11-26 20:36) <Tommy Barroy>  
`|\| | `  
`| * | `[591f57f50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/591f57f50) test , je commit, je push, je deploy dev, puis je decommit, repush redeploy dev (2023-11-26 20:35) <Tommy Barroy>  
`|/ /  `  
`* / `[8201456e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8201456e9) retag 2.0.3 (2023-11-26 19:10) <static@sesamath-lampdev>  
`|/  `  
`* `[63c2fbffb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63c2fbffb) Correction de AfficheMathLive dans (2023-11-26 18:35) <Yves Biton>  
`*   `[146306bd3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/146306bd3) Merge branch 'erreur_type_dans_editeur_memo' into 'main' (2023-11-26 18:11) <Tommy Barroy>  
`|\  `  
`| * `[302a65dbe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/302a65dbe) les anciennes ressources ont pas routes un type defini pour mg (2023-11-26 18:10) <Tommy Barroy>  
`|/  `  
`* `[70f03a01b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70f03a01b) retag 2.0.3 (2023-11-26 13:05) <static@sesamath-lampdev>  
`*   `[126b3b753](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/126b3b753) Merge branch 'Correction_Clavier_Mathlive_Affichage_Solution' into 'main' (2023-11-26 12:07) <Yves Biton>  
`|\  `  
`| * `[d3a2676dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3a2676dd) Correction de la gestion de la touche Entrée pour le clavier virtuel adapté à Mathlive (2023-11-26 12:06) <Yves Biton>  
`|/  `  
`*   `[be21de788](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be21de788) Merge branch 'Ajout_Clavier_Virtuel_Pour_MathLive' into 'main' (2023-11-26 10:26) <Yves Biton>  
`|\  `  
`| * `[f67441979](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f67441979) Première version avec un clavier virtuel ayant le même look que celui habituel mais fonctionnant avec Mathlive (2023-11-26 10:24) <Yves Biton>  
`| * `[e64c28081](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e64c28081) Début d'ajout de fichiers pour créer un fichier ancien look marchant avec MathLive (2023-11-24 14:30) <Yves Biton>  
`* |   `[559fbe154](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/559fbe154) Merge branch 'pbNaN' into 'main' (2023-11-26 09:33) <Rémi Deniaud>  
`|\ \  `  
`| * | `[07121d0de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07121d0de) nettoyage et correction du pb où des vecteurs pouvaient être égaux dans produitscalaire [fix #225](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/225) (2023-11-25 17:42) <Rémi Deniaud>  
`* | | `[52f406ccc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52f406ccc) retag 2.0.3 (2023-11-26 01:11) <static@sesamath-lampdev>  
`* | |   `[4f4eaa10b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f4eaa10b) Merge branch 'oubli_type_pieges' into 'main' (2023-11-25 23:55) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[ed3ebb0e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed3ebb0e1) oublier de modif type pièges (2023-11-25 23:54) <Tommy Barroy>  
`|/ / /  `  
`* | | `[4b16ccc91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b16ccc91) retag 2.0.3 (2023-11-25 18:42) <static@sesamath-lampdev>  
`* | | `[a66c76ff0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a66c76ff0) retag 2.0.3 (2023-11-25 18:12) <static@sesamath-lampdev>  
`* | |   `[46416023a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46416023a) Merge branch 'bug_sur_tye_enregistre_dans_formuleMakeEditor' into 'main' (2023-11-25 18:11) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[cc82ee6f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc82ee6f9) add record et reduit taille son max (2023-11-25 18:10) <Tommy Barroy>  
`|/ /  `  
`* | `[4ab8eb504](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ab8eb504) chargement_j3p viré (plus personne ne s'en servait) (2023-11-24 16:59) <Daniel Caillibaud>  
`* |   `[4c0a8f467](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c0a8f467) Merge branch 'add_record' into 'main' (2023-11-24 16:23) <Tommy Barroy>  
`|\ \  `  
`| * \   `[242dde879](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/242dde879) Merge branch 'main' into 'add_record' (2023-11-24 16:23) <Tommy Barroy>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | | `[f2ec7a219](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2ec7a219) [fix #226](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/226) (2023-11-24 14:51) <Daniel Caillibaud>  
`* | | `[6daf09a2c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6daf09a2c) Ajout de tests unitaires pour j3pNombre et j3pVirgule (2023-11-24 14:45) <Daniel Caillibaud>  
`* | | `[a0aba9cd2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0aba9cd2) extensions virées dans les imports statiques (2023-11-24 12:34) <Daniel Caillibaud>  
`* | | `[0f374117e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f374117e) fcts utilisant Tarbre sorties de functions.js vers functionsTarbre.js (2023-11-24 12:30) <Daniel Caillibaud>  
`* | | `[07890af46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07890af46) Aj de regex statiques j3pRestriction.xxx (2023-11-24 11:51) <Daniel Caillibaud>  
`* | | `[240cde5fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/240cde5fa) Fix orthographe (Schrek => Shrek) (2023-11-24 11:11) <Daniel Caillibaud>  
`| * | `[1f882662a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f882662a) add record et reduit taille son max (2023-11-24 16:20) <Tommy Barroy>  
`|/ /  `  
`* | `[20ddfade9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20ddfade9) retag 2.0.3 (2023-11-23 22:27) <static@sesamath-lampdev>  
`* |   `[903cc932c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/903cc932c) Merge branch 'met_image_son_dans_memoMake' into 'main' (2023-11-23 21:59) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[216fdee0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/216fdee0e) en cours (2023-11-23 21:57) <Tommy Barroy>  
`| * `[88a4e3bd7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88a4e3bd7) en cours (2023-11-23 16:35) <Tommy Barroy>  
`| * `[371b19cfa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/371b19cfa) rebase main (2023-11-23 16:04) <Tommy Barroy>  
`* | `[b722f82fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b722f82fd) retag 2.0.3 (2023-11-23 17:17) <static@sesamath-lampdev>  
`* |   `[d6b68f5a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d6b68f5a5) Merge branch 'Amelioration_Position_Clavier_Virtuel_Mathlive' into 'main' (2023-11-23 16:45) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[f017408b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f017408b3) Positionnement par défaut du clavier virtuel mathlive à droite de l'éditeur et plus haut. (2023-11-23 16:44) <Yves Biton>  
`|/  `  
`* `[9ee37ee6d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ee37ee6d) retag 2.0.3 (2023-11-23 15:17) <static@sesamath-lampdev>  
`* `[79fd79b48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79fd79b48) On peut mantenant capturer le haut du clavier virtuel à la souris (2023-11-23 14:43) <Yves Biton>  
`* `[e5f3037e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5f3037e9) màj sesatheque-client (2023-11-23 12:28) <Daniel Caillibaud>  
`*   `[d9d80c8b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9d80c8b3) Merge branch 'correction_Hide_Virtual_Keyboard_Mathlive' into 'main' (2023-11-22 21:21) <Yves Biton>  
`|\  `  
`| * `[923689f73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/923689f73) J'avais oublié de cacher la barre du haut du clavier virtuel quand j'affiche la solution dans excalcmathlive (2023-11-22 21:20) <Yves Biton>  
`* |   `[6831d7a85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6831d7a85) Merge branch 'met_image_et_son_dans_pixel' into 'main' (2023-11-22 21:18) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[2d42edcda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d42edcda) met image/son dans pixelMake (2023-11-22 21:16) <Tommy>  
`| * `[b70770168](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b70770168) vire console (2023-11-22 09:51) <Tommy>  
`* |   `[ce8a76d1b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce8a76d1b) Merge branch 'Correction_Bubbles_Sur_Evt_Clavier_MathLive' into 'main' (2023-11-22 20:41) <Yves Biton>  
`|\ \  `  
`| * | `[79eaca492](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79eaca492) On rajoute un bubbles à un événement clavier lancé par l'éditeur mathlive (2023-11-22 20:40) <Yves Biton>  
`* | | `[ec67af80b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec67af80b) static/mathlive exclu du déploiement (c'est le post_deploy qui récupère les polices) (2023-11-22 19:03) <Daniel Caillibaud>  
`|/ /  `  
`* | `[0542b0af6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0542b0af6) retag 2.0.3 (2023-11-22 18:38) <static@sesamath-lampdev>  
`* | `[4f7e40493](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f7e40493) retag 2.0.3 (2023-11-22 18:10) <static@sesamath-lampdev>  
`* | `[3be0db1cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3be0db1cd) j3pConteneur => j3pContainer (2023-11-22 18:10) <Daniel Caillibaud>  
`* | `[253de767e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/253de767e) sesabiblidev=>sesabidev et sesacommundev => sesacomdev (pour avoir le même nb de lettre que la prod et ne pas casser les sources.map) (2023-11-22 18:10) <Daniel Caillibaud>  
`* |   `[d40948856](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d40948856) Merge branch 'On_Vire_Les_Menus_Des_Editeurs_Mathlive' into 'main' (2023-11-22 18:08) <Yves Biton>  
`|\ \  `  
`| * | `[909c2e986](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/909c2e986) On vire l'icône menu du clavier Mathlive (2023-11-22 18:07) <Yves Biton>  
`* | |   `[1c39b6a9a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c39b6a9a) Merge branch 'develop2' into 'main' (2023-11-22 15:23) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[fceeb3271](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fceeb3271) develop2 (2023-11-22 15:21) <Tommy>  
`|/ /  `  
`* | `[ea52b9622](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea52b9622) Je clone excalcmathquill ds excalcmathlive (2023-11-22 14:34) <Yves Biton>  
`* | `[46fd16a35](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46fd16a35) On remet sectionexcalcmathquill comme il était avant le merge de test_MathLive_Yves (2023-11-22 14:31) <Daniel Caillibaud>  
`* | `[522ea0847](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/522ea0847) màj mathlive (0.97.2) (2023-11-22 14:12) <Daniel Caillibaud>  
`* | `[9500f96a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9500f96a5) fix sélection du conteneur pour les css mathlive (2023-11-22 14:04) <Daniel Caillibaud>  
`* |   `[1ef104530](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ef104530) Merge branch 'Changement_Conteneur_Pour_Surcharge_CSS' into 'main' (2023-11-22 12:37) <Yves Biton>  
`|\ \  `  
`| * | `[41b03d9ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41b03d9ca) Le conteneur pour la surcharge css est défini par la classe j3pConteneur (2023-11-22 12:35) <Yves Biton>  
`|/ /  `  
`* |   `[dc0c44dfe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc0c44dfe) Merge branch 'Correction_j3pContainer' into 'main' (2023-11-22 12:02) <Yves Biton>  
`|\ \  `  
`| * | `[1e5e2c564](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e5e2c564) Correction de l'id utilsée pour surcharge du css (2023-11-22 11:59) <Yves Biton>  
`|/ /  `  
`* |   `[f7f00c4ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7f00c4ad) Merge branch 'test_MathLive_Yves' into 'main' (2023-11-22 11:19) <Yves Biton>  
`|\ \  `  
`| * \   `[46945f60c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46945f60c) Merge remote-tracking branch 'origin/main' into test_MathLive_Yves (2023-11-22 11:03) <Yves Biton>  
`| |\ \  `  
`| * | | `[b418e7e8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b418e7e8c) Ajout de la touche Shift au clavier virtuel (2023-11-22 10:42) <Yves Biton>  
`| * | | `[b05090b89](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b05090b89) Correction due au fait que MathLive ne met pas d'accolades pour une fraction dont le numérateur et le dénominateur sont des chiffres. (2023-11-21 21:37) <Yves Biton>  
`| * | | `[dbfa6d51f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dbfa6d51f) Suppsresssion d'une ligne du clavier quand elle est vide (2023-11-21 20:43) <Yves Biton>  
`| * | | `[e045e2de4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e045e2de4) La touche Entrée du clavier virtuel fonctionne maintenant en simulant un événement clavier keyup de la touche Entrée. (2023-11-21 20:20) <Yves Biton>  
`| * | | `[362a74c25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/362a74c25) sectionexcalcmathquill et sectionexcalcmathlive sont maintenant identiques pour tester les ressources en ligne. La touche Entrée du clavier virtuel ne fonctionn e pas comme prévu : ne semble pas engendrer un événement d'appui de la touche Entrée. (2023-11-21 18:24) <Yves Biton>  
`| * | | `[b92257250](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b92257250) Diminution de la taille de police des boutons. (2023-11-21 11:00) <Yves Biton>  
`| * | | `[5b9c307fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b9c307fa) Amélioration du positionnement du clavier. Problème de positionnement du clavier via event 'input' résolu (2023-11-21 10:46) <Yves Biton>  
`| * | | `[a6c3de261](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6c3de261) Amélioration du positionnement du clavier. Problème de positionnement du clavier via event 'inpu' résolu (2023-11-21 10:44) <Yves Biton>  
`| * | | `[c31646837](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c31646837) Amélioration du positionnement du clavier. Un problème reste à régler quand on utilise les boutons du clavier virtuel (sans doute bug de MathLive) (2023-11-21 09:23) <Yves Biton>  
`| * | | `[fbdb9e7f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fbdb9e7f1) Amélioration du positionnement du clavier (2023-11-20 10:24) <Yves Biton>  
`| * | | `[3d069ae00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d069ae00) La touche * renvoie un signe de multiplication (2023-11-20 09:14) <Yves Biton>  
`| * | | `[aa41be68c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa41be68c) Clavier virtuel mathlive bien positionné sous l'éditeur (2023-11-20 08:24) <Yves Biton>  
`| * | | `[c14072f25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c14072f25) Clavier virtuel mathlive avec possibilité d'avoir les équivalents des boutons MathQuill (2023-11-17 11:32) <Yves Biton>  
`| * | | `[a147c4e63](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a147c4e63) Version du clavier virtuel avec ajout en bas de boutons correspondant aux boutons MathQuill (2023-11-16 18:23) <Yves Biton>  
`| * | | `[cde641637](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cde641637) On intercepte l'événement virtual-keyboard-toggle pour montrer ou masquer le clavier virtuel (2023-11-14 09:36) <Yves Biton>  
`| * | | `[dcd7b139f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dcd7b139f) On intercepte l'événement virtual-keyboard-toggle pour montrer ou masquer le clavier virtuel (2023-11-14 09:24) <Yves Biton>  
`| * | | `[a0c9525e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0c9525e2) En attente que l'événement geometrychange marrche correctement quand on clique sur le bouton pour fermer le clavier virtuel. (2023-11-08 09:34) <Yves Biton>  
`| * | | `[37dc07fe0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37dc07fe0) Première version de exerciceCalcmathquill qui marche avec mathlive. Version où on peut déplacer le clavier virtuel. (2023-11-08 09:34) <Yves Biton>  
`| * | | `[2090ed2bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2090ed2bf) Première version de exerciceCalcmathquill qui marche avec mathlive. Version où on peut déplacer le clavier virtuel. (2023-11-08 09:34) <Yves Biton>  
`| * | | `[13e087491](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13e087491) Première version de exerciceCalcmathquill qui marche avec mathlive. Première version où on peut déplacer le clavier virtuek. (2023-11-08 09:34) <Yves Biton>  
`* | | | `[743d45ce4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/743d45ce4) Depuis l'éditeur, "Lancer ce graphe" ouvre une nouvelle fenêtre (2023-11-22 11:11) <Daniel Caillibaud>  
`| |_|/  `  
`|/| |   `  
`* | |   `[617f64050](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/617f64050) Merge branch 'vire_console' into 'main' (2023-11-21 22:54) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[c48a37ce4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c48a37ce4) vire console (2023-11-21 22:48) <Tommy>  
`|/ / /  `  
`* | |   `[6a8f9401f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a8f9401f) Merge branch 'fixEditorFullscreen' into 'main' (2023-11-21 22:16) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[a249d5dfc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a249d5dfc) amélioration testIframe.html (load listener) (2023-11-21 18:50) <Daniel Caillibaud>  
`| * | | `[142470dec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/142470dec) plus de fullscreen (blockly & mtg HS) mais une modale (2023-11-21 18:13) <Daniel Caillibaud>  
`| * | | `[294196954](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/294196954) blindage (2023-11-21 18:12) <Daniel Caillibaud>  
`| * | | `[2673df389](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2673df389) Ajout de l'option zIndex (à mettre sur la modale) (2023-11-21 18:12) <Daniel Caillibaud>  
`| * | | `[af3b85895](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af3b85895) aj d'un html pour tester j3p en iframe (en local) (2023-11-21 17:24) <Daniel Caillibaud>  
`* | | |   `[11e5ed12b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/11e5ed12b) Merge branch 'rajoute_fichier_image_je_crois' into 'main' (2023-11-21 22:13) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[df25a3d26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df25a3d26) j'ai mis que dans resMake, c'est long (2023-11-21 22:13) <Tommy>  
`|/ / / /  `  
`* | | |   `[2060527ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2060527ff) Merge branch 'generalise_son_image' into 'main' (2023-11-21 22:10) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[d992a9637](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d992a9637) j'ai mis que dans resMake, c'est long (2023-11-21 22:08) <Tommy>  
`|/ / / /  `  
`* | | |   `[14bfcfab8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14bfcfab8) Merge branch 'pb_copie_pixel_mal_fait' into 'main' (2023-11-21 17:07) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[e45d13d1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e45d13d1a) fix 223 (2023-11-21 17:06) <Tommy>  
`|/ / / /  `  
`* | | |   `[2a06692c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a06692c3) Merge branch 'ajout_im_et_son_formules_-_pix_-_memo' into 'main' (2023-11-21 15:55) <Tommy Barroy>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[a0e63fcbc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0e63fcbc) remet bien son et limite taille son (2023-11-21 15:53) <Tommy>  
`| * | | `[5596476fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5596476fe) revert des modifs de src/editGraphe/boiteDialogue.js (elles sont dans la branche editorModal) (2023-11-21 12:24) <Daniel Caillibaud>  
`| * | | `[6a99a2129](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a99a2129) ajoute pour son joli (2023-11-20 17:44) <Tommy>  
`| * | | `[f2732892a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2732892a) ajoute son et image dans formulesMake Modif appel editeur (2023-11-20 17:44) <Tommy>  
`| * | | `[9f0d6bbcc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f0d6bbcc) ajoute son et image dans formulesMake Modif appel editeur (2023-11-20 08:27) <Tommy>  
`| * | | `[c2298d96b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2298d96b) ajoute son et image dans formulesMake Modif appel editeur (2023-11-19 22:18) <Tommy>  
`|/ / /  `  
`* | |   `[e5054ec50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5054ec50) Merge branch 'pbTabVarCanonique' into 'main' (2023-11-18 18:47) <Rémi Deniaud>  
`|\ \ \  `  
`| * | | `[ee0d727df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee0d727df) correction canonique_tabvar [fix #221](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/221) (2023-11-18 18:41) <Rémi Deniaud>  
`|/ / /  `  
`* | |   `[6808fa096](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6808fa096) Merge branch 'reduireSimple_espace' into 'main' (2023-11-18 18:18) <Rémi Deniaud>  
`|\ \ \  `  
`| * | | `[2ca72e4b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ca72e4b1) j'ai identifié le pb dans espace_repere9, j'espère avoir réussi à le contourner car je ne sais pas quand l'erreur peut se produire (2023-11-18 18:15) <Rémi Deniaud>  
`| * | | `[d4bfb00bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4bfb00bd) petite modification dans reduirecomplexe (2023-11-15 19:49) <Rémi Deniaud>  
`| * | | `[7f86d34a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f86d34a1) fin de la correction de reduiresimple (2023-11-15 19:41) <Rémi Deniaud>  
`| * | | `[b705a6b65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b705a6b65) gros nettoyage de reduiresimple (2023-11-15 19:24) <Rémi Deniaud>  
`* | | | `[02efe5b6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02efe5b6e) retag 2.0.3 (2023-11-18 09:17) <static@sesamath-lampdev>  
`* | | |   `[a45d80cb6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a45d80cb6) Merge branch 'editeur_mtg' into 'main' (2023-11-18 08:50) <Tommy Barroy>  
`|\ \ \ \  `  
`| * | | | `[d2a0002d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2a0002d4) ajoute point sym pour figure de départ, met overflow dans éditeur plein écran (2023-11-18 08:48) <Tommy>  
`|/ / / /  `  
`* | | | `[e03e27d6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e03e27d6e) commentaires (2023-11-17 21:49) <Daniel Caillibaud>  
`* | | | `[5d8fbdd91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d8fbdd91) editor en fullscreen (2023-11-17 21:46) <Daniel Caillibaud>  
`* | | | `[52b9e5909](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52b9e5909) retag 2.0.3 (2023-11-15 21:56) <static@sesamath-lampdev>  
`* | | |   `[ae4fb3105](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae4fb3105) Merge branch 'bugsnag_tableur01_et_amelio_sa_tete' into 'main' (2023-11-15 21:26) <Tommy Barroy>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[aff496733](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aff496733) bugsnag et option sansbord pour zsm1 quand c'est dans un tableau (2023-11-15 21:25) <Tommy>  
`| * | | `[5c83b8134](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c83b8134) bugsnag et option sansbord pour zsm1 quand c'est dans un tableau (2023-11-15 21:24) <Tommy>  
`|/ / /  `  
`* | | `[cbde2433d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cbde2433d) fix url dans la doc (2023-11-15 14:58) <Daniel Caillibaud>  
`* | | `[01cf89913](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01cf89913) retag 2.0.3 (2023-11-14 23:35) <static@sesamath-lampdev>  
`* | |   `[b7fd3f1a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7fd3f1a0) Merge branch 'fini_explik_editeur' into 'main' (2023-11-14 22:58) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[7f7e5804b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f7e5804b) explications pour éditeur mémory et resMAke (2023-11-14 22:57) <Tommy>  
`|/ / /  `  
`* | | `[1f77521e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f77521e8) Appel de loadJqueryDialog systématique avant d'appeler Dialog (2023-11-14 20:55) <Daniel Caillibaud>  
`* | | `[13af638a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13af638a1) fix chargement de dialog (manquait keycode) (2023-11-14 20:54) <Daniel Caillibaud>  
`* | | `[30055e9d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30055e9d3) retag 2.0.3 (2023-11-14 15:58) <static@sesamath-lampdev>  
`| |/  `  
`|/|   `  
`* |   `[797aea4b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/797aea4b4) Merge branch 'ajoute_explik' into 'main' (2023-11-13 20:54) <Tommy Barroy>  
`|\ \  `  
`| * | `[1d77d308f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d77d308f) explications pour éditeur pixel Art (2023-11-13 20:52) <Tommy>  
`|/ /  `  
`* | `[10cde130c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10cde130c) retag 2.0.3 (2023-11-13 09:45) <static@sesamath-lampdev>  
`* |   `[ff508a259](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff508a259) Merge branch 'Correction_Unlatexify_Inegalites' into 'main' (2023-11-13 09:11) <Yves Biton>  
`|\ \  `  
`| * | `[f7a581ee9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7a581ee9) Les \le n'étaient plus remplacés par des <= dans unlatexify (2023-11-13 09:09) <Yves Biton>  
`|/ /  `  
`* | `[30d8b6c67](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30d8b6c67) retag 2.0.3 (2023-11-12 17:35) <static@sesamath-lampdev>  
`* |   `[6d9f71057](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d9f71057) Merge branch 'explik_formule' into 'main' (2023-11-12 16:15) <Tommy Barroy>  
`|\ \  `  
`| * | `[b9cf58227](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9cf58227) duplique .css ca marchait pas pour deploy (2023-11-12 16:14) <Tommy>  
`* | | `[512d70144](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/512d70144) Merge branch 'explik_formule' into 'main' (2023-11-12 16:11) <Tommy Barroy>  
`|\| | `  
`| * | `[818e3cc38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/818e3cc38) ajoute explications editeur de formulesMake ( qui s'appelle ressource Vignettes maintenant ) (2023-11-12 16:10) <Tommy>  
`|/ /  `  
`* | `[3b90758a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b90758a8) retag 2.0.3 (2023-11-12 15:31) <static@sesamath-lampdev>  
`* |   `[734164fc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/734164fc7) Merge branch 'Amelioration_Formula_Comparator' into 'main' (2023-11-12 11:50) <Yves Biton>  
`|\ \  `  
`| * | `[1e45b1771](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e45b1771) Amélioration du comparateur d'expressions. La paramètre otpions peut aussi contenur : simplify (true par défaut) ; on élimine avant la comparaison les divisions par 1, additions de 0 et multiplications par 1 (pour celles-ci sauf si le paramètre simplifyMult1 est false) simplifyMult1: si ce paramètre est false ue simplify est true, on supprime dans solution les division par 1 et sommes de 0 mais pas les multiplications par 1. (2023-11-12 11:44) <Yves Biton>  
`* | | `[606963c8b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/606963c8b) retag 2.0.3 (2023-11-11 16:44) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[8f4cc0e3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f4cc0e3d) Merge branch 'fait_aide' into 'main' (2023-11-11 14:34) <Tommy Barroy>  
`|\ \  `  
`| * | `[79033115f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79033115f) pour sauv (2023-11-11 11:03) <Tommy>  
`| * | `[549c6b80f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/549c6b80f) pour sauv (2023-11-10 20:00) <Tommy>  
`|/ /  `  
`* | `[5407d08f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5407d08f3) retag 2.0.3 (2023-11-10 16:10) <static@sesamath-lampdev>  
`* |   `[95bf2c6d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95bf2c6d7) Merge branch 'explik' into 'main' (2023-11-09 22:01) <Tommy Barroy>  
`|\ \  `  
`| * | `[831126e15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/831126e15) ajoute des explications pour l'éditeur blokmohascratch (2023-11-09 22:00) <Tommy>  
`|/ /  `  
`* | `[fb418886d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb418886d) retag 2.0.3 (2023-11-09 19:32) <static@sesamath-lampdev>  
`* |   `[884d1c7f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/884d1c7f6) Merge branch 'tiket' into 'main' (2023-11-09 19:02) <Tommy Barroy>  
`|\ \  `  
`| * | `[b323ff10b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b323ff10b) [fix #220](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/220) (2023-11-09 18:57) <Tommy>  
`|/ /  `  
`* |   `[fed1a3013](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fed1a3013) Merge branch 'bug_sur_obliges_toutes' into 'main' (2023-11-09 17:03) <Tommy Barroy>  
`|\ \  `  
`| * | `[c5588ed43](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c5588ed43) un biolean transformé en liste (2023-11-09 17:02) <Tommy>  
`* | | `[8390f72fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8390f72fe) retag 2.0.3 (2023-11-09 17:00) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[1f1ad1960](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f1ad1960) Merge branch 'repare_compas_2' into 'main' (2023-11-09 16:06) <Tommy Barroy>  
`|\ \  `  
`| * | `[5c3e5d628](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c3e5d628) enleve console (2023-11-09 16:02) <Tommy>  
`| * | `[537e1649c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/537e1649c) le compas restait actif (2023-11-09 16:02) <Tommy>  
`|/ /  `  
`* |   `[8d7ccd035](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d7ccd035) Merge branch 'Correction_keep_Text_Supprimes' into 'main' (2023-11-09 14:40) <Yves Biton>  
`|\ \  `  
`| * | `[4dbd48ef7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4dbd48ef7) On remet le keepText à true dans les appels de cleanLatexForMq qui l'utilisaient et modif de la fonction cleanLaTexForMq pour que le traitement soit complet quanr le LaTeX ne contient pas de \text{} On remet le traitement d'avant dans la section sectionexcalcmathlive (2023-11-09 14:03) <Yves Biton>  
`| * | `[e750c8a0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e750c8a0e) On remet le keepText à true dans les appels de cleanLatexForMq qui l'utilisaient et modif de la fonction cleanLaTexForMq pour que le traitement soit complet quanr le LaTeX ne contient pas de \text{} (2023-11-09 13:59) <Yves Biton>  
`* | | `[a21da7e03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a21da7e03) retag 2.0.3 (2023-11-09 14:05) <static@sesamath-lampdev>  
`* | |   `[2252cb771](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2252cb771) Merge branch 'repare_compas' into 'main' (2023-11-09 13:37) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[cc4439771](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc4439771) plein de bugsnag (2023-11-09 13:35) <Tommy>  
`* | | | `[a5c6d6349](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5c6d6349) retag 2.0.3 (2023-11-08 23:38) <static@sesamath-lampdev>  
`| |/ /  `  
`|/| |   `  
`* | |   `[788ebe4fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/788ebe4fc) Merge branch 'On_Retablit_CleanLatexForMq' into 'main' (2023-11-08 23:08) <Yves Biton>  
`|\ \ \  `  
`| * | | `[cfa176b17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cfa176b17) On a fait une grosse connerie en ne testant pas la modif sur cleanLaTeXForMq. Je remets comme avant en attendant mieux (2023-11-08 23:06) <Yves Biton>  
`* | | | `[e93591524](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e93591524) retag 2.0.3 (2023-11-08 20:08) <static@sesamath-lampdev>  
`* | | |   `[74b41afe6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/74b41afe6) Merge branch 'repare_compas' into 'main' (2023-11-08 18:56) <Tommy Barroy>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| / /   `  
`| |/ /    `  
`| * / `[b0c016e36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b0c016e36) le compas ne se bloquait plus avec le verrou (2023-11-08 18:55) <Tommy>  
`|/ /  `  
`* | `[ea411ace9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea411ace9) Merge branch 'test_MathLive_Yves' into 'main' (2023-11-08 15:38) <Daniel Caillibaud>  
`|\| `  
`| * `[6cee69a62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cee69a62) option keepText de cleanLatexForMq virée (buggée et inutile) (2023-11-08 09:34) <Daniel Caillibaud>  
`| * `[286a8edbd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/286a8edbd) fix import avec suffixe (2023-11-07 21:21) <Daniel Caillibaud>  
`| * `[722c14ee8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/722c14ee8) compare (de FormulaComparator) accepte comme argument réponse une string (syntaxe mathgraph) ou un input mathquill ou un input mathlive. (2023-11-07 21:11) <Daniel Caillibaud>  
`| * `[c972851b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c972851b5) Ajout de getMqValue (que j3pValeurde utilise) (2023-11-07 21:09) <Daniel Caillibaud>  
`| * `[b1342c67a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1342c67a) aj de deux commentaires fixme dans cleanLatexForMl (2023-11-07 20:34) <Daniel Caillibaud>  
`| * `[eb2e45ad5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb2e45ad5) bugfix cleanLatexForMl (récup de ce qui précède un éventuel \text) (2023-11-07 20:34) <Daniel Caillibaud>  
`| * `[3701d0d91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3701d0d91) Essai de créatio d'un comparateur de formule par mtg32 : ajout de la possibilité de changer le nom des variables utilisées pour la comparaison. Complétion des noms de variables si nécessaire (2023-11-07 19:47) <Yves Biton>  
`| * `[fe497a6e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe497a6e4) tabvar doit toujours contenir 6 caractères (2023-11-07 19:47) <Daniel Caillibaud>  
`| * `[841e1a47c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/841e1a47c) Essai de créatio d'un comparateur de formule par mtg32 : ajout de la possibilité de changer le nom des variables utilisées pour la comparaison. Correction de l'appel de verifieSyntaxe (2023-11-07 19:47) <Yves Biton>  
`| * `[2ef5a0173](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ef5a0173) jsdoc (2023-11-07 19:47) <Daniel Caillibaud>  
`| * `[822d80840](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/822d80840) retourne -2 en cas de pb de syntaxe sur la solution (2023-11-07 19:47) <Daniel Caillibaud>  
`| * `[f734fec91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f734fec91) retourne -1 en cas d'erreur de syntaxe (et plus de throw SyntaxError) (2023-11-07 19:47) <Daniel Caillibaud>  
`| * `[f014f85ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f014f85ec) Essai de créatio d'un comparateur de formule par mtg32 : ajout de la possibilité de changer le nom des variables utilisées pour la comparaison. Modif des test. (2023-11-07 19:47) <Yves Biton>  
`| * `[10c94f473](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10c94f473) refonte récup du formulaComparator et ajout du test unitaire (2023-11-07 19:47) <Daniel Caillibaud>  
`| * `[d5faca248](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5faca248) Essai de créatio d'un comparateur de formule par mtg32 : ajout de la possibilité de changer le nom des variables utilisées pour la comparaison (2023-11-07 19:47) <Yves Biton>  
`| * `[dde0ade76](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dde0ade76) Essai de créatio d'un comparateur de formule par mtg32 (2023-11-07 19:47) <Yves Biton>  
`| * `[a54d1a2ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a54d1a2ac) Première version de exerciceCalcmathquill qui marche avec mathlive. Version remaniée pour intégrales, primities. Reste un erreur de console sur le mathML (2023-11-07 19:47) <Yves Biton>  
`| * `[a60e97fc1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a60e97fc1) Première version de exerciceCalcmathquill qui marche avec mathlive. Version remaniée pour intégrales, primities. Reste un erreur de console sur le mathML (2023-11-07 19:47) <Yves Biton>  
`| * `[69dad7f14](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69dad7f14) Première version de exerciceCalcmathquill qui marche avec mathlive. Version qui marche avec un calcul d'intégrales (2023-11-07 19:47) <Yves Biton>  
`| * `[cdfbf8334](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cdfbf8334) Première version de exerciceCalcmathquill qui marche avec mathlive tentative de clavier virtuel personnalisé. Version qui marche avec un calcul d'intégrales (2023-11-07 19:47) <Yves Biton>  
`| * `[f711b6295](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f711b6295) Première version de exerciceCalcmathquill qui marche avec mathlive tentative de clavier virtuel personnalisé. Tout marche normalement sauf le clavier à perfectionner (2023-11-07 19:47) <Yves Biton>  
`| * `[c482d0bf5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c482d0bf5) fix chargement des polices mathlive (pnpm start only, faudra qqchose de plus pérenne pour le build comme dans draftPlayer) (2023-11-07 19:47) <Daniel Caillibaud>  
`| * `[a31a8b16b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a31a8b16b) aj des symlinks de docroot/static/mathlive (pour les polices mathlive) (2023-11-07 19:47) <Daniel Caillibaud>  
`| * `[2ddaff22d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ddaff22d) Première version de exerciceCalcmathquill qui marche avec mathlive tentative de clavier virtuel personnalisé. Test provisoire de boutons. Les polices Katex ne sont pas chargées avec vite. (2023-11-07 19:47) <Yves Biton>  
`| * `[ac99a682f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac99a682f) Première version de exerciceCalcmathquill qui marche avec mathlive tentative de clavier virtuel personnalisé. Le bouton puissance fonctionne (2023-11-07 19:47) <Yves Biton>  
`| * `[7ee3db099](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ee3db099) Première version de exerciceCalcmathquill qui marche avec mathlive tentative de clavier virtuel personnalisé. Le bouton recopier fonctionne avec Mathlive (2023-11-07 19:47) <Yves Biton>  
`| * `[d9c00297a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9c00297a) Première version de exerciceCalcmathquill qui marche avec mathlive tentative de clavier virtuel personnalisé. Tentaive de déplacer le clavier virtuel dans un conteneur (2023-11-07 19:47) <Yves Biton>  
`| * `[85bbc6e9e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85bbc6e9e) Première version de exerciceCalcmathquill qui marche avec mathlive tentative de clavier virtuel personnalisé. Modif package.json (2023-11-07 19:47) <Yves Biton>  
`| * `[a6c262fba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6c262fba) Première version de exerciceCalcmathquill qui marche avec mathlive tentative de clavier virtuel personnalisé. (2023-11-07 19:47) <Yves Biton>  
`| * `[c99acbcc6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c99acbcc6) Première version de exerciceCalcmathquill qui marche avec mathlive (2023-11-07 19:47) <Yves Biton>  
`| * `[505cf8be8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/505cf8be8) Première version de exerciceCalcmathquill où la correction est affichée par mathlive au lieu de MathQuill (2023-11-07 19:47) <Yves Biton>  
`* | `[7d245e580](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d245e580) retag 2.0.3 (2023-11-07 18:41) <static@sesamath-lampdev>  
`* | `[451234298](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/451234298) retag 2.0.3 (2023-11-07 18:04) <static@sesamath-lampdev>  
`* |   `[2d1ec969f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d1ec969f) Merge branch 'pb_de_place_nom' into 'main' (2023-11-07 18:02) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[d681c5d8f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d681c5d8f) api bug quand point lié pas visible à la construction (2023-11-07 18:01) <Tommy>  
`|/  `  
`* `[4e825bbd8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e825bbd8) timerId doit être déclaré plus tôt (sinon ça plante dans les tests happy-dom) (2023-11-07 16:04) <Daniel Caillibaud>  
`*   `[44bc97b94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44bc97b94) Merge branch 'vire_console_et_corrige_touch' into 'main' (2023-11-07 15:29) <Tommy Barroy>  
`|\  `  
`| * `[494837540](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/494837540) une console qui traine et bug sur touch move ( qui doit dater de longtemps.. ) (2023-11-07 15:27) <Tommy>  
`|/  `  
`* `[3f2641b49](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f2641b49) retag 2.0.3 (2023-11-07 14:33) <static@sesamath-lampdev>  
`*   `[c039201a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c039201a3) Merge branch 'allege_figlibre' into 'main' (2023-11-07 12:54) <Tommy Barroy>  
`|\  `  
`| * `[ca68052a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca68052a0) plein de bugsnag (2023-11-07 12:53) <Tommy>  
`| * `[9a9f95e58](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a9f95e58) alege figlibre (2023-11-07 09:37) <Tommy>  
`|/  `  
`* `[0cdefc5f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0cdefc5f7) retag 2.0.3 (2023-11-07 08:27) <static@sesamath-lampdev>  
`*   `[e96f65d64](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e96f65d64) Merge branch 'tryProgConst' into 'main' (2023-11-06 22:44) <Tommy Barroy>  
`|\  `  
`| * `[0f3875a3b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f3875a3b) finish 2 (2023-11-06 22:42) <Tommy>  
`| * `[0c4e5e8ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c4e5e8ee) finish 1 (2023-11-06 22:37) <Tommy>  
`| * `[71961bfcc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/71961bfcc) pour sauv (2023-11-06 22:07) <Tommy>  
`| * `[eec4ea74f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eec4ea74f) pour sauv (2023-11-06 21:58) <Tommy>  
`| * `[03999c6da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03999c6da) pour sauv (2023-11-06 19:21) <Tommy>  
`| * `[595be099a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/595be099a) pour sauv (2023-11-05 22:01) <Tommy>  
`| * `[91bf49f46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/91bf49f46) pour sauv (2023-11-05 22:00) <Tommy>  
`| * `[628c10f57](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/628c10f57) regle couleur (2023-11-04 20:33) <Tommy>  
`| * `[6b09a833f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b09a833f) pou test Yves (2023-11-04 16:05) <Tommy>  
`| * `[2ad08b466](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ad08b466) pour test Yves (2023-11-04 14:43) <Tommy>  
`| * `[2a4aabb6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a4aabb6f) pour test Yves (2023-11-02 20:29) <Tommy>  
`| * `[d92b4b174](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d92b4b174) sauv (2023-11-02 18:28) <Tommy>  
`| * `[b4ba712a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4ba712a2) sauv (2023-11-02 12:09) <Tommy>  
`| * `[64d96b570](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64d96b570) sauv (2023-11-02 02:50) <Tommy>  
`| * `[23a63f7a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23a63f7a6) pour sauv (2023-11-01 12:29) <Tommy>  
`| * `[314e1b9e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/314e1b9e7) pour sauv (2023-10-31 18:22) <Tommy>  
`* |   `[14ac60e6b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14ac60e6b) Merge branch 'reduireComplexe' into 'main' (2023-11-05 22:39) <Rémi Deniaud>  
`|\ \  `  
`| * | `[d2416edf3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2416edf3) correction + nettoyage de reduirecomplexe (2023-11-05 22:38) <Rémi Deniaud>  
`|/ /  `  
`* |   `[a9d873c1f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9d873c1f) Merge branch 'modifInegalitePhrase' into 'main' (2023-11-05 21:43) <Rémi Deniaud>  
`|\ \  `  
`| * | `[fce70ee20](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fce70ee20) petite modif dans inegalitePhrase (2023-11-05 21:42) <Rémi Deniaud>  
`* | | `[a0ec5b00b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0ec5b00b) retag 2.0.3 (2023-11-05 15:34) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[a9e7c5a8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9e7c5a8e) Merge branch 'inegalitePhraseV2' into 'main' (2023-11-03 11:39) <Rémi Deniaud>  
`|\ \  `  
`| * | `[9899cb999](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9899cb999) ajout de cas de figure dans inegalitePhrase (2023-11-03 11:38) <Rémi Deniaud>  
`|/ /  `  
`* | `[14b00701e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14b00701e) retag 2.0.3 (2023-11-02 20:10) <static@sesamath-lampdev>  
`* |   `[fb38e2f2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb38e2f2f) Merge branch 'co_recipThal' into 'main' (2023-11-02 19:10) <Tommy Barroy>  
`|\ \  `  
`| * | `[15a1bdf39](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15a1bdf39) bug sur correction, oubli de desactive brouillon, Deux cas à virer ( quotient sur les parallèles ) et , à accepter dans les réponses (2023-11-02 19:09) <Tommy>  
`* | | `[b1babf231](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1babf231) Merge branch 'co_recipThal' into 'main' (2023-11-02 19:08) <Tommy Barroy>  
`|\| | `  
`| * | `[b07704590](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b07704590) bug sur correction, oubli de desactive brouillon, Deux cas à virer ( quotient sur les parallèles ) et , à accepter dans les réponses (2023-11-02 19:07) <Tommy>  
`|/ /  `  
`* | `[5f8fe8a44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f8fe8a44) retag 2.0.3 (2023-11-01 14:00) <static@sesamath-lampdev>  
`* |   `[08bc97332](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/08bc97332) Merge branch 'modifnbChoix' into 'main' (2023-11-01 12:36) <Tommy Barroy>  
`|\ \  `  
`| * | `[b652eb2e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b652eb2e0) Descend à 3 le nb de choix possibles (2023-11-01 12:33) <Tommy>  
`|/ /  `  
`* | `[dd774052b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd774052b) retag 2.0.3 (2023-10-31 19:58) <static@sesamath-lampdev>  
`* |   `[4c3cbd4ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c3cbd4ad) Merge branch 'remetPoint' into 'main' (2023-10-31 18:38) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[4229ca836](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4229ca836) oubPoint (2023-10-31 18:36) <Tommy>  
`|/  `  
`* `[cad6c9ef2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cad6c9ef2) retag 2.0.3 (2023-10-30 21:04) <static@sesamath-lampdev>  
`*   `[646364aa0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/646364aa0) Merge branch 'avec_avert_sur_piege' into 'main' (2023-10-30 19:08) <Tommy Barroy>  
`|\  `  
`| * `[5770e78e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5770e78e1) sauve sur suppreim un elem (2023-10-30 17:52) <Tommy>  
`* | `[cd5074bd3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd5074bd3) retag 2.0.3 (2023-10-30 17:38) <static@sesamath-lampdev>  
`* | `[0bcc217fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0bcc217fb) Merge branch 'avec_avert_sur_piege' into 'main' (2023-10-30 16:45) <Tommy Barroy>  
`|\| `  
`| * `[1427bdd97](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1427bdd97) oubli sauv mg32 pieges (2023-10-30 16:38) <Tommy>  
`* | `[a11eb21d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a11eb21d4) retag 2.0.3 (2023-10-29 13:34) <static@sesamath-lampdev>  
`* | `[e888de253](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e888de253) Merge branch 'avec_avert_sur_piege' into 'main' (2023-10-29 13:04) <Tommy Barroy>  
`|\| `  
`| * `[019a2810a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/019a2810a) ajout avert su r piege ( pour la correcrion ) (2023-10-29 12:55) <Tommy>  
`|/  `  
`* `[30ee0828c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30ee0828c) retag 2.0.3 (2023-10-29 00:54) <static@sesamath-lampdev>  
`*   `[dcb47aacb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dcb47aacb) Merge branch 'co_toutes_rep' into 'main' (2023-10-29 00:21) <Tommy Barroy>  
`|\  `  
`| * `[165c82f21](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/165c82f21) prenait pas en compte toutes les réponses avec Id (2023-10-29 00:20) <Tommy>  
`|/  `  
`* `[21c9ce19b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/21c9ce19b) retag 2.0.3 (2023-10-28 21:10) <static@sesamath-lampdev>  
`*   `[3f7601722](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f7601722) Merge branch 'co_eqprod' into 'main' (2023-10-28 20:36) <Tommy Barroy>  
`|\  `  
`| * `[8de1b07fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8de1b07fc) pb score ( bugsnag ) et nb trop grands, plus orthigraphe (2023-10-28 20:35) <Tommy>  
`|/  `  
`* `[03c7522ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03c7522ef) retag 2.0.3 (2023-10-28 15:42) <static@sesamath-lampdev>  
`*   `[100b58416](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/100b58416) Merge branch 'formule_que_gauche' into 'main' (2023-10-28 15:17) <Tommy Barroy>  
`|\  `  
`| * `[4728964bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4728964bd) demande que à gauche les cartes en propose et et soluce (2023-10-28 15:17) <Tommy>  
`| * `[bf1f8adce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf1f8adce) demande que à gauche les cartes en propose et et soluce (2023-10-28 14:31) <Tommy>  
`|/  `  
`*   `[84d39532b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84d39532b) Merge branch 'simpli_eqprod' into 'main' (2023-10-28 12:25) <Tommy Barroy>  
`|\  `  
`| * `[52b33976b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52b33976b) demande sipli en q3 et corrige solution double en fraction (2023-10-28 12:18) <Tommy>  
`|/  `  
`* `[2d0b0ed63](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d0b0ed63) retag 2.0.3 (2023-10-27 18:16) <static@sesamath-lampdev>  
`*   `[4ed19295a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ed19295a) Merge branch 'Amelioration_Traitement_Produits_Par_1' into 'main' (2023-10-27 17:35) <Yves Biton>  
`|\  `  
`| * `[0fd490a89](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0fd490a89) On accepte les *1 (2023-10-27 17:34) <Yves Biton>  
`* | `[a7d4513d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7d4513d6) retag 2.0.3 (2023-10-27 16:49) <static@sesamath-lampdev>  
`* |   `[e1e96c819](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1e96c819) Merge branch 'amelio_editeurs' into 'main' (2023-10-27 16:08) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[7b1eea3b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b1eea3b4) ajout MG32 dans formuleMake ameliore duplik exo dans blokmtg et blokmohascratch editeurs (2023-10-27 16:06) <Tommy>  
`|/  `  
`*   `[d5f1ee07c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5f1ee07c) Merge branch 'Correction_Section_ExCalc' into 'main' (2023-10-27 15:40) <Yves Biton>  
`|\  `  
`| * `[327cc1778](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/327cc1778) Ne pas mettre en vert une bonne réponse en cas de non validation auto (2023-10-27 15:38) <Yves Biton>  
`|/  `  
`*   `[8f080528d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f080528d) Merge branch 'Amelioration_Section_Calcul_Mult1' into 'main' (2023-10-27 14:30) <Yves Biton>  
`|\  `  
`| * `[8290f5e32](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8290f5e32) Ajout d'un paramètre acceptMult1 qu'on met à true pour accepter les multiplications par 1 inutiles (faux par défaut). On met la couluer en vert pour une bonne réponse. (2023-10-27 14:28) <Yves Biton>  
`|/  `  
`* `[fee9908c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fee9908c0) retag 2.0.3 (2023-10-25 07:13) <static@sesamath-lampdev>  
`*   `[328935b07](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/328935b07) Merge branch 'mod' into 'main' (2023-10-25 02:37) <Tommy Barroy>  
`|\  `  
`| * `[368a3b4b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/368a3b4b8) supprime explique de la reponse précédente (2023-10-25 02:34) <Tommy>  
`|/  `  
`* `[3388d1c53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3388d1c53) retag 2.0.3 (2023-10-25 02:22) <static@sesamath-lampdev>  
`*   `[2dd9a2b13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2dd9a2b13) Merge branch 'finish_32' into 'main' (2023-10-25 00:27) <Tommy Barroy>  
`|\  `  
`| * `[5ea408e63](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ea408e63) duplik exo un peu sensé et aide pour comprendre comment ca fonctionne (2023-10-25 00:26) <Tommy>  
`|/  `  
`*   `[7c932e06f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c932e06f) Merge branch 'aie' into 'main' (2023-10-24 16:18) <Tommy Barroy>  
`|\  `  
`| * `[4f51a6aea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f51a6aea) utilise triatematquill pour transforme latex en utilisable pour mg32 (2023-10-24 16:17) <Tommy>  
`| * `[0f399884a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f399884a) sauve (2023-10-24 15:41) <Tommy>  
`|/  `  
`*   `[aaa75e850](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aaa75e850) Merge branch 'amelio_edit' into 'main' (2023-10-24 14:36) <Tommy Barroy>  
`|\  `  
`| * `[7e0bfe49c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e0bfe49c) amelio editeur (2023-10-24 14:26) <Tommy>  
`* | `[866db8de8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/866db8de8) retag 2.0.3 (2023-10-24 13:56) <static@sesamath-lampdev>  
`* | `[0e6b658f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e6b658f1) Merge branch 'amelio_edit' into 'main' (2023-10-24 12:33) <Tommy Barroy>  
`|\| `  
`| * `[624202d47](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/624202d47) amelio editeur (2023-10-24 12:33) <Tommy>  
`|/  `  
`* `[471f1e951](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/471f1e951) retag 2.0.3 (2023-10-24 10:49) <static@sesamath-lampdev>  
`*   `[bdd2105d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bdd2105d6) Merge branch 'ajoute_test' into 'main' (2023-10-24 10:15) <Tommy Barroy>  
`|\  `  
`| * `[ff12b8148](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff12b8148) ajoute text apres, et xx compté faux pour x² (2023-10-24 10:08) <Tommy>  
`* | `[01f1eaecb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01f1eaecb) retag 2.0.3 (2023-10-24 09:11) <static@sesamath-lampdev>  
`* | `[676f190b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/676f190b1) Merge branch 'ajoute_test' into 'main' (2023-10-24 07:40) <Tommy Barroy>  
`|\| `  
`| * `[d0ce1bbbf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0ce1bbbf) finish (2023-10-24 07:40) <Tommy>  
`* | `[21009d650](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/21009d650) retag 2.0.3 (2023-10-24 07:11) <static@sesamath-lampdev>  
`* | `[eb46994bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb46994bd) Merge branch 'ajoute_test' into 'main' (2023-10-23 23:49) <Tommy Barroy>  
`|\| `  
`| * `[e14305748](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e14305748) ajoute test d'un exo dans resMake (2023-10-23 23:48) <Tommy>  
`|/  `  
`* `[13d62eb35](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13d62eb35) retag 2.0.3 (2023-10-23 19:05) <static@sesamath-lampdev>  
`*   `[e8f6c2433](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8f6c2433) Merge branch 'enleve_deux_points' into 'main' (2023-10-23 17:22) <Tommy Barroy>  
`|\  `  
`| * `[b8c6b1298](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8c6b1298) enlve deux points apres consignes (2023-10-23 17:22) <Tommy>  
`|/  `  
`* `[da288cada](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da288cada) retag 2.0.3 (2023-10-23 17:02) <static@sesamath-lampdev>  
`*   `[1378a545a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1378a545a) Merge branch 'bug_supprimer' into 'main' (2023-10-23 16:35) <Tommy Barroy>  
`|\  `  
`| * `[23b7924e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23b7924e0) supprime exo bug (2023-10-23 16:34) <Tommy>  
`|/  `  
`* `[979fbde25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/979fbde25) retag 2.0.3 (2023-10-23 16:27) <static@sesamath-lampdev>  
`* `[e04f4ef82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e04f4ef82) retag 2.0.3 (2023-10-23 16:00) <static@sesamath-lampdev>  
`* `[748b4eaff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/748b4eaff) retag 2.0.3 (2023-10-23 16:00) <static@sesamath-lampdev>  
`*   `[908de6099](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/908de6099) Merge branch 'ortho' into 'main' (2023-10-23 15:59) <Tommy Barroy>  
`|\  `  
`| * `[dd3438311](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd3438311) export buggait (2023-10-23 15:58) <Tommy>  
`* | `[291fbcda1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/291fbcda1) fix test local avec les gros paramètres (2023-10-23 15:31) <Daniel Caillibaud>  
`* | `[e47a1868c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e47a1868c) màj deps (2023-10-23 15:31) <Daniel Caillibaud>  
`* | `[0aa9a892b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0aa9a892b) Merge branch 'ortho' into 'main' (2023-10-23 15:19) <Tommy Barroy>  
`|\| `  
`| * `[b46c1ea47](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b46c1ea47) vire console (2023-10-23 15:19) <Tommy>  
`* | `[b64f08e16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b64f08e16) Merge branch 'ortho' into 'main' (2023-10-23 15:04) <Tommy Barroy>  
`|\| `  
`| * `[a1f462eeb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1f462eeb) pb reponses prévues (2023-10-23 15:03) <Tommy>  
`* | `[9c300b5f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c300b5f8) ajout d'une requête purge avant chaque chargement de page (2023-10-23 14:37) <Daniel Caillibaud>  
`* | `[7607dfc85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7607dfc85) Merge branch 'ortho' into 'main' (2023-10-23 14:31) <Tommy Barroy>  
`|\| `  
`| * `[f00287f09](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f00287f09) pb couleur (2023-10-23 14:30) <Tommy>  
`* | `[0b6a73fab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b6a73fab) Merge branch 'ortho' into 'main' (2023-10-23 14:15) <Tommy Barroy>  
`|\| `  
`| * `[58056440e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58056440e) orthographe (2023-10-23 14:14) <Tommy>  
`|/  `  
`*   `[bf935fa10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf935fa10) Merge branch 'inegalites' into 'main' (2023-10-23 11:55) <Rémi Deniaud>  
`|\  `  
`| * `[b76733a8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b76733a8c) dans inegalitePhrase, gestion de nbchances (2023-10-23 11:54) <Rémi Deniaud>  
`* |   `[e3caf8626](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e3caf8626) Merge branch 'amelio_resmk' into 'main' (2023-10-23 11:49) <Tommy Barroy>  
`|\ \  `  
`| * | `[2490cd67c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2490cd67c) agrandi taille input editeur formule (2023-10-23 11:47) <Tommy>  
`| * | `[f744c1c2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f744c1c2d) pb sauve param (2023-10-23 11:35) <Tommy>  
`* | | `[0e8554208](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e8554208) Merge branch 'amelio_resmk' into 'main' (2023-10-23 11:04) <Tommy Barroy>  
`|\| | `  
`| * | `[52794d4ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/52794d4ff) agmente nb rep attendues (2023-10-23 11:03) <Tommy>  
`|/ /  `  
`* |   `[332e80f67](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/332e80f67) Merge branch 'Correction_Exo_Inequation' into 'main' (2023-10-23 09:45) <Yves Biton>  
`|\ \  `  
`| * | `[0c63098b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c63098b2) Correction de la correction de cet exercice (2023-10-23 09:36) <Yves Biton>  
`* | |   `[d0ee43eb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0ee43eb8) Merge branch 'inegalites' into 'main' (2023-10-23 09:21) <Rémi Deniaud>  
`|\ \ \  `  
`| | |/  `  
`| |/|   `  
`| * | `[0548105d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0548105d9) ajout de la section inegalitePhrase (2023-10-23 09:13) <Rémi Deniaud>  
`* | | `[8b0676459](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b0676459) retag 2.0.3 (2023-10-23 09:09) <static@sesamath-lampdev>  
`* | |   `[387ed7563](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/387ed7563) Merge branch 'ghj' into 'main' (2023-10-22 23:44) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[829d098d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/829d098d3) alert a virer (2023-10-22 23:43) <Tommy>  
`|/ / /  `  
`* | |   `[83389ad34](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83389ad34) Merge branch 'poursuis_res' into 'main' (2023-10-22 23:22) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[88ef70189](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/88ef70189) ortho (2023-10-22 23:21) <Tommy>  
`| * | | `[10f6d62e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10f6d62e2) resMake tests (2023-10-22 23:20) <Tommy>  
`|/ / /  `  
`* | |   `[d1dc1298a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1dc1298a) Merge branch 'parent' into 'main' (2023-10-22 17:52) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[a5ce2326b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5ce2326b) aggrandi les parenthèses (2023-10-22 17:51) <Tommy>  
`|/ / /  `  
`* | |   `[546d1ecd2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/546d1ecd2) Merge branch 'bloque_point' into 'main' (2023-10-22 17:27) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[f096d3711](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f096d3711) bloque le centre de la sphère (2023-10-22 17:27) <Tommy>  
`|/ / /  `  
`* | |   `[065ad7fc8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/065ad7fc8) Merge branch 'ressMake' into 'main' (2023-10-22 17:23) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[6bd9f8ec7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6bd9f8ec7) test (2023-10-22 17:23) <Tommy>  
`* | | | `[5a8358875](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a8358875) retag 2.0.3 (2023-10-22 17:13) <static@sesamath-lampdev>  
`* | | | `[c0fc3704b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0fc3704b) Merge branch 'ressMake' into 'main' (2023-10-22 16:42) <Tommy Barroy>  
`|\| | | `  
`| * | | `[6edadfb38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6edadfb38) test (2023-10-22 16:41) <Tommy>  
`* | | | `[68d6b843f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68d6b843f) retag 2.0.3 (2023-10-22 15:52) <static@sesamath-lampdev>  
`| |/ /  `  
`|/| |   `  
`* | |   `[3b856b172](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b856b172) Merge branch 'sommeTermesSuite' into 'main' (2023-10-22 15:12) <Rémi Deniaud>  
`|\ \ \  `  
`| * | | `[219efdb0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/219efdb0c) correction petite erreur de sommeTermesSuite [fix #179](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/179) (2023-10-22 15:11) <Rémi Deniaud>  
`* | | |   `[f6b5ccad1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6b5ccad1) Merge branch 'egaliteFcts' into 'main' (2023-10-22 12:37) <Rémi Deniaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[bd0f28f94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd0f28f94) juste une petite modif dans egalite_fonctions (2023-10-22 12:36) <Rémi Deniaud>  
`|/ / /  `  
`* | |   `[8246564ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8246564ab) Merge branch 'tabVariation' into 'main' (2023-10-22 09:56) <Rémi Deniaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[622ea198c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/622ea198c) gestion de la forme factorisée dans canonique_tabvar pour éviter d'avoir un radical (2023-10-22 09:54) <Rémi Deniaud>  
`* | |   `[f83ad1c8a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f83ad1c8a) Merge branch 'amelio_angle_section01' into 'main' (2023-10-22 09:31) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[8ba2191a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8ba2191a2) angle plan cone (2023-10-22 09:29) <Tommy>  
`| * | | `[bd034eb76](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd034eb76) angle plan sphere (2023-10-22 09:16) <Tommy>  
`|/ / /  `  
`* | |   `[009e4e16f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/009e4e16f) Merge branch 'angle_sec' into 'main' (2023-10-21 15:35) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[db38ec658](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/db38ec658) angle plan sphere (2023-10-21 15:34) <Tommy>  
`* | |   `[598f96a61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/598f96a61) Merge branch 'fixThComparaison' into 'main' (2023-10-21 12:23) <Rémi Deniaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[6f614ee83](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f614ee83) correction de thComparaison sur la validation et les commentaires de l'étape 2 (2023-10-21 11:25) <Rémi Deniaud>  
`|/ /  `  
`* |   `[3eaef2344](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3eaef2344) Merge branch 'co_correc_eq_prod' into 'main' (2023-10-20 16:52) <Tommy Barroy>  
`|\ \  `  
`| * | `[da75ec105](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/da75ec105) finish (2023-10-20 16:51) <Tommy>  
`|/ /  `  
`* | `[355f3b4ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/355f3b4ed) retag 2.0.3 (2023-10-20 16:35) <static@sesamath-lampdev>  
`* |   `[15d91c445](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/15d91c445) Merge branch 'fixLimiteComparaison' into 'main' (2023-10-20 14:58) <Rémi Deniaud>  
`|\ \  `  
`| * | `[aa1c2c6a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa1c2c6a4) déplacement affectation debug (2023-10-17 09:03) <Daniel Caillibaud>  
`| * | `[36a1e955c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/36a1e955c) \' => ’ (2023-10-17 09:00) <Daniel Caillibaud>  
`| * | `[d1fbe49cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1fbe49cf) [fix #208](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/208) : il y avait un souci dans thComparaison lorsque seuls les 2 signes d'inégalités étaient inversés (2023-10-14 17:29) <Rémi Deniaud>  
`| * | `[6e0ff7af7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e0ff7af7) debug n'était plus pris en compte avec l'évolution de surcharge (2023-10-14 16:18) <Rémi Deniaud>  
`| * | `[3eb632530](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3eb632530) fix modif qui n'était pas possible en 2e chance (2023-10-04 12:35) <Daniel Caillibaud>  
`| * | `[921ec8cff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/921ec8cff) nettoyage et simplification (2023-10-04 12:34) <Daniel Caillibaud>  
`* | |   `[006c7105d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/006c7105d) Merge branch 'fin_eq_prod' into 'main' (2023-10-19 22:36) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[1c8cfe933](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c8cfe933) finish (2023-10-19 22:35) <Tommy>  
`|/ / /  `  
`* | |   `[cd99632b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd99632b2) Merge branch 'co_ticket' into 'main' (2023-10-18 20:59) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[26afefbad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/26afefbad) co bloksim sur execution du prog de correction et retreint fenetre des points aléatoires dans blokmtg01 ( pour que le nom puisse s'afficher dans la fenetre totale ) (2023-10-18 20:58) <Tommy>  
`* | | | `[962222265](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/962222265) Merge branch 'co_ticket' into 'main' (2023-10-17 22:14) <Tommy Barroy>  
`|\| | | `  
`| * | | `[d3abba033](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3abba033) ticket 1 (2023-10-17 22:12) <Tommy>  
`|/ / /  `  
`* | | `[25fada818](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25fada818) retag 2.0.3 (2023-10-17 16:33) <static@sesamath-lampdev>  
`* | |   `[e2df9efaa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2df9efaa) Merge branch 'eqprod' into 'main' (2023-10-17 15:59) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[d029be23a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d029be23a) un nvelle pas finie , plus vire le 3 dans block cercle-rayon, plus 2 correction de yareponse (2023-10-17 15:58) <Tommy>  
`| | |/  `  
`| |/|   `  
`* / | `[5a2c4669c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a2c4669c) build exclu dans webstorm (2023-10-17 15:02) <Daniel Caillibaud>  
`|/ /  `  
`* | `[2e205729b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e205729b) retag 2.0.3 (2023-10-16 13:22) <static@sesamath-lampdev>  
`* |   `[ae3682dde](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae3682dde) Merge branch 'pb_duplique_cercle_avec_longueur' into 'main' (2023-10-16 12:52) <Tommy Barroy>  
`|\ \  `  
`| * | `[be68d9a28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be68d9a28) bug dans correction cerle avec longueur (2023-10-16 12:50) <Tommy>  
`|/ /  `  
`* |   `[3d1239189](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d1239189) Merge branch 'co_section' into 'main' (2023-10-16 10:45) <Tommy Barroy>  
`|\ \  `  
`| * | `[7e8e14ea2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e8e14ea2) difficile de trouver une réponse qui convienne a tout le monde (2023-10-16 10:44) <Tommy>  
`|/ /  `  
`* |   `[bea5175eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bea5175eb) Merge branch 'coolSim' into 'main' (2023-10-15 21:17) <Tommy Barroy>  
`|\ \  `  
`| * | `[4521014e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4521014e0) ajout tolerance (2023-10-15 21:13) <Tommy>  
`|/ /  `  
`* | `[64b80196b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64b80196b) retag 2.0.3 (2023-10-15 15:02) <static@sesamath-lampdev>  
`* |   `[f1b22246d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1b22246d) Merge branch 'sim01' into 'main' (2023-10-15 14:33) <Tommy Barroy>  
`|\ \  `  
`| * | `[27f0f9d13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27f0f9d13) ajout difficulte et corrige bug correction (2023-10-15 14:33) <Tommy>  
`| * | `[9928dc685](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9928dc685) change identique en superposable (2023-10-15 14:20) <Tommy>  
`| * | `[5139d24fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5139d24fc) pb correction quand y'a des marques de segments (2023-10-15 14:18) <Tommy>  
`| * | `[b9c6f33ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9c6f33ed) finish sim01 (2023-10-15 13:52) <Tommy>  
`| * | `[d5d1ff850](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5d1ff850) sauve en cours (2023-10-14 19:07) <Tommy>  
`* | | `[d735006e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d735006e1) retag 2.0.3 (2023-10-14 21:52) <static@sesamath-lampdev>  
`* | |   `[28f04ce25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28f04ce25) Merge branch 'co_se_l_a_co' into 'main' (2023-10-14 19:12) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[b9fe65229](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9fe65229) oublié de modif celle la (2023-10-14 19:10) <Tommy>  
`|/ /  `  
`* | `[9083349c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9083349c5) retag 2.0.3 (2023-10-14 10:40) <static@sesamath-lampdev>  
`* |   `[f718db147](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f718db147) Merge branch 'cor_sec02' into 'main' (2023-10-14 10:12) <Tommy Barroy>  
`|\ \  `  
`| * | `[5ad2bf10d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ad2bf10d) manquait un point dans la pyramide et ajout aide suplémentaire (2023-10-14 10:12) <Tommy>  
`|/ /  `  
`* | `[cf0923ad2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf0923ad2) retag 2.0.3 (2023-10-14 05:35) <static@sesamath-lampdev>  
`* |   `[31c21858f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/31c21858f) Merge branch 'modif_shere' into 'main' (2023-10-14 00:41) <Tommy Barroy>  
`|\ \  `  
`| * | `[99301d0c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/99301d0c0) vire les fleches de gauches a la correction (2023-10-14 00:40) <Tommy>  
`* | | `[1a66af3fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a66af3fe) Merge branch 'modif_shere' into 'main' (2023-10-14 00:21) <Tommy Barroy>  
`|\| | `  
`| * | `[43d549081](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43d549081) ti change (2023-10-14 00:20) <Tommy>  
`|/ /  `  
`* | `[084a855b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/084a855b3) retag 2.0.3 (2023-10-13 16:39) <static@sesamath-lampdev>  
`* | `[3baf23785](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3baf23785) retag 2.0.3 (2023-10-13 16:13) <static@sesamath-lampdev>  
`* | `[0288c4d0b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0288c4d0b) retag 2.0.3 (2023-10-13 16:13) <static@sesamath-lampdev>  
`* |   `[e89d4b4ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e89d4b4ca) Merge branch 'change_w_en_o' into 'main' (2023-10-13 15:59) <Tommy Barroy>  
`|\ \  `  
`| * | `[f3bf2dfe3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3bf2dfe3) remplace West par Ouest (2023-10-13 15:57) <Tommy>  
`* | | `[a0a5b931b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a0a5b931b) Merge branch 'change_w_en_o' into 'main' (2023-10-13 15:51) <Tommy Barroy>  
`|\| | `  
`| * | `[63019ff36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63019ff36) remplace West par Ouest (2023-10-13 15:50) <Tommy>  
`|/ /  `  
`* |   `[8d3cb6fa1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d3cb6fa1) Merge branch 'tite_modif_sur_2' into 'main' (2023-10-13 15:28) <Tommy Barroy>  
`|\ \  `  
`| * | `[64946698f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64946698f) vire console (2023-10-13 15:18) <Tommy>  
`* | | `[a4d715eae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a4d715eae) Merge branch 'tite_modif_sur_2' into 'main' (2023-10-13 15:16) <Tommy Barroy>  
`|\| | `  
`| * | `[7a2d51be6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a2d51be6) amelio sphere detail et bug sur efface a cause longueur dans mtgblo01 (2023-10-13 15:15) <Tommy>  
`| * | `[58bf2a7e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58bf2a7e1) amelio sphere detail et bug sur efface a cause longueur dans mtgblo01 (2023-10-13 15:11) <Tommy>  
`|/ /  `  
`* |   `[ad6b006c9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad6b006c9) Merge branch 'Correction_Exi_Placer_M_Par_Affixe' into 'main' (2023-10-13 13:08) <Yves Biton>  
`|\ \  `  
`| * | `[0f337e1b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f337e1b0) On cache les 2 points qui donnaient une indication sur la solution (2023-10-13 13:07) <Yves Biton>  
`|/ /  `  
`* |   `[afbd33da5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/afbd33da5) Merge branch 'Correction_Exo_Thales3' into 'main' (2023-10-13 13:00) <Yves Biton>  
`|\ \  `  
`| * | `[a628f3fc8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a628f3fc8) Correction de la correction de Thales3 [fix #204](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/204) (2023-10-13 12:59) <Yves Biton>  
`|/ /  `  
`* | `[14d829171](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14d829171) màj src/types/mathgraph.d.ts (2023-10-13 11:16) <Daniel Caillibaud>  
`* |   `[1d10c7bc0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d10c7bc0) Merge branch 'finish_terre' into 'main' (2023-10-13 11:04) <Tommy Barroy>  
`|\ \  `  
`| * | `[65362a6e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65362a6e4) fini spher (2023-10-13 11:03) <Tommy>  
`|/ /  `  
`* | `[a52d37751](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a52d37751) retag 2.0.3 (2023-10-13 07:09) <static@sesamath-lampdev>  
`* |   `[818891ce7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/818891ce7) Merge branch 'amelio_angle' into 'main' (2023-10-13 02:04) <Tommy Barroy>  
`|\ \  `  
`| * | `[247375aa6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/247375aa6) amelio spher (2023-10-13 02:03) <Tommy>  
`* | | `[7f51dd096](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f51dd096) màj src/types/mathgraph.d.ts (2023-10-12 22:18) <Daniel Caillibaud>  
`* | | `[80a90de79](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80a90de79) màj mathgraph.d.ts (2023-10-12 19:50) <Daniel Caillibaud>  
`* | | `[0f8aeaa5c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f8aeaa5c) jsdoc (2023-10-12 19:49) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[f4402b0e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f4402b0e1) Merge branch 'possib_modif_point_dans_editeur' into 'main' (2023-10-12 16:51) <Tommy Barroy>  
`|\ \  `  
`| * | `[04bcfa5d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04bcfa5d6) ameliore marque (2023-10-12 16:50) <Tommy>  
`* | | `[a9aa4ebb0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9aa4ebb0) retag 2.0.3 (2023-10-12 16:31) <static@sesamath-lampdev>  
`* | | `[46aed3bba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46aed3bba) Merge branch 'possib_modif_point_dans_editeur' into 'main' (2023-10-12 16:02) <Tommy Barroy>  
`|\| | `  
`| * | `[dd73e4f62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd73e4f62) possibilite de modifier place d'un point fix dans l'éditeur (2023-10-12 16:01) <Tommy>  
`|/ /  `  
`* |   `[d3116c938](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3116c938) Merge branch 'annule_clic' into 'main' (2023-10-12 15:40) <Tommy Barroy>  
`|\ \  `  
`| * | `[86f96d410](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86f96d410) mais pas en mode memory (2023-10-12 15:39) <Tommy>  
`| * | `[06f58f92a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06f58f92a) possibilité d'Annuler clic (2023-10-12 15:38) <Tommy>  
`|/ /  `  
`* | `[c1ba6e74a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c1ba6e74a) ajSegTerre découpe le segment en tranches de 10° de latitude max (pour éviter de petits traits bleus dans les continents sur l'horizon E/W) (2023-10-12 10:12) <Daniel Caillibaud>  
`* | `[1943f75d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1943f75d3) fix inversion lat/long dans le nommage des variables de ajSegTerre (2023-10-12 09:52) <Daniel Caillibaud>  
`* | `[01a1a7e06](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01a1a7e06) ajout fct ajVilles (et simplification de son code, int2 devient facultatif) (2023-10-12 09:36) <Daniel Caillibaud>  
`* |   `[1172e32cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1172e32cd) Merge branch 'Correction_Squelette_Calc_Param_MathQuill' into 'main' (2023-10-12 08:41) <Yves Biton>  
`|\ \  `  
`| * | `[d89f33e26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d89f33e26) Le paramètre variables n'est jamais utilisé dans les fichiers annexes (ne servirait que pour certains calculs d'intégrales) (2023-10-12 08:38) <Yves Biton>  
`| * | `[43803452b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43803452b) simplification boucles sur les lettres (2023-10-11 10:53) <Daniel Caillibaud>  
`| * | `[4b8326b95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b8326b95) constructeur _Donnees viré (2023-10-11 10:53) <Daniel Caillibaud>  
`| * | `[c99305e7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c99305e7d) nettoyage correction (2023-10-11 10:44) <Daniel Caillibaud>  
`| * | `[70862b3a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70862b3a0) On vire les afficheBouton et desactiveReturn de cette section (2023-10-11 10:30) <Yves Biton>  
`* | | `[4db87ae95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4db87ae95) retag 2.0.3 (2023-10-12 00:11) <static@sesamath-lampdev>  
`* | |   `[ac1564d5e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac1564d5e) Merge branch 'modif_un_peu_sphere' into 'main' (2023-10-11 22:31) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[daf8d419c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/daf8d419c) coupe l amerique du nord en 2 pour virer bug à 56° (2023-10-11 22:29) <Tommy>  
`|/ / /  `  
`* | |   `[76f9b3d4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76f9b3d4b) Merge branch 'amelio_section01' into 'main' (2023-10-11 21:50) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[2b43eceab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b43eceab) ecris cos(64°) au lieu de cos(64) (2023-10-11 21:48) <Tommy>  
`|/ / /  `  
`* | | `[1273ce340](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1273ce340) [fix #213](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/213) (2023-10-11 20:49) <Daniel Caillibaud>  
`* | | `[06f90d497](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06f90d497) màj mathgraph.d.ts (2023-10-11 15:53) <Daniel Caillibaud>  
`* | | `[6867ce824](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6867ce824) boost ×4 en traçant moins de segments (sans voir de différence visuelle) (2023-10-11 15:28) <Daniel Caillibaud>  
`* | | `[3c00d88cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c00d88cc) Ajout d’un html + js séparé pour construire la figure mathgraph de la mappemonde (2023-10-11 14:48) <Daniel Caillibaud>  
`* | | `[571a8224e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/571a8224e) màj src/types/mathgraph.d.ts (2023-10-11 13:02) <Daniel Caillibaud>  
`* | | `[66758d683](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66758d683) typofixes dans les parenthèses (des textes) (2023-10-11 11:21) <Daniel Caillibaud>  
`* | |   `[b2cac474c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b2cac474c) Merge branch 'chiffres_pour_nombres_et_espace' into 'main' (2023-10-11 11:03) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[7a4e48b6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a4e48b6a) nombre à la place de chiffres ( je sais c'est pas bien ) (2023-10-11 11:02) <Tommy>  
`|/ / /  `  
`* | |   `[c9ff30c17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9ff30c17) Merge branch 'corrige_points' into 'main' (2023-10-11 10:47) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[2aa53e7fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2aa53e7fe) modif kval || Daniel (2023-10-11 10:46) <Tommy>  
`| * | | `[9f8aabf5c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f8aabf5c) points correction (2023-10-11 10:39) <Tommy>  
`|/ / /  `  
`* | |   `[32ebc94b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32ebc94b0) Merge branch 'amelio_sphe' into 'main' (2023-10-11 10:26) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[0fcde77d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0fcde77d4) mise en page et amelio correc (2023-10-11 10:25) <Tommy>  
`|/ /  `  
`* |   `[ba984165a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba984165a) Merge branch 'erreur_param_lineaire' into 'main' (2023-10-11 04:32) <Tommy Barroy>  
`|\ \  `  
`| * | `[bb5e0a974](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb5e0a974) erreur params de départ (2023-10-11 04:31) <Tommy>  
`|/ /  `  
`* |   `[af06399d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af06399d8) Merge branch 'mise_en_page' into 'main' (2023-10-11 04:08) <Tommy Barroy>  
`|\ \  `  
`| * | `[a7beda1ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7beda1ea) mise en page (2023-10-11 04:08) <Tommy>  
`|/ /  `  
`* |   `[38aafd9b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38aafd9b4) Merge branch 'co_sec01' into 'main' (2023-10-11 03:44) <Tommy Barroy>  
`|\ \  `  
`| * | `[d5c933220](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5c933220) correction fausse (2023-10-11 03:42) <Tommy>  
`|/ /  `  
`* |   `[f7da12538](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7da12538) Merge branch 'fin_terre' into 'main' (2023-10-11 03:20) <Tommy Barroy>  
`|\ \  `  
`| * | `[dbb8b73a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dbb8b73a5) longueur (2023-10-11 03:19) <Tommy>  
`|/ /  `  
`* |   `[71279675c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/71279675c) Merge branch 'Changement_Titre_Exercices' into 'main' (2023-10-10 23:24) <Yves Biton>  
`|\ \  `  
`| * | `[b30c759d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b30c759d1) Rectification du titre de deux exercices (2023-10-10 23:22) <Yves Biton>  
`|/ /  `  
`* | `[4ffece23c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ffece23c) retag 2.0.3 (2023-10-10 23:17) <static@sesamath-lampdev>  
`* |   `[1596ed612](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1596ed612) Merge branch 'Amelioration_Squelette_Calc_Param_Apres_Oui_Non' into 'main' (2023-10-10 21:46) <Yves Biton>  
`|\ \  `  
`| * | `[27564d55e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27564d55e) Amélioration de l'exercice du français vers la formule sans racine carrée Amélioration du squelette Calc_Param_Apres_Oui_Non et des exercices qui lui sont associés (2023-10-10 21:40) <Yves Biton>  
`|/ /  `  
`* | `[9d7bd822f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d7bd822f) retag 2.0.3 (2023-10-10 15:45) <static@sesamath-lampdev>  
`* |   `[c72b89a31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c72b89a31) Merge branch 'Ajout_Exercice_Francais_Vers_Calcul' into 'main' (2023-10-10 15:12) <Yves Biton>  
`|\ \  `  
`| * | `[04d82b99b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04d82b99b) Création d'un nouvel exercice en 5 ième français vers formule et passage des 3 exos de 5 ième en mode MathQuill pour la correction. Pour cela, modif du squelette squlettemtg32_Calc_Param_Multi_Etapes (2023-10-10 15:11) <Yves Biton>  
`|/ /  `  
`* |   `[9dfa27c32](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9dfa27c32) Merge branch 'tent_long_dans_mtgblo01_et_mark_longueur' into 'main' (2023-10-09 21:48) <Tommy Barroy>  
`|\ \  `  
`| * | `[4a6e4b5d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a6e4b5d2) longueur (2023-10-09 20:02) <Tommy>  
`| * | `[e2e58412e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e2e58412e) longueur (2023-10-08 23:56) <Tommy>  
`| * | `[5b04b16a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b04b16a8) longueur (2023-10-08 22:59) <Tommy>  
`* | |   `[091b5a7d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/091b5a7d0) Merge branch 'pourSuis_terre' into 'main' (2023-10-09 21:44) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[cb08eebce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb08eebce) longueur (2023-10-09 21:42) <Tommy>  
`| * | `[cd014005f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd014005f) en attendant (2023-10-07 17:29) <Tommy>  
`* | | `[48c6b6a66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48c6b6a66) retag 2.0.3 (2023-10-07 23:33) <static@sesamath-lampdev>  
`* | |   `[ae3046d46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae3046d46) Merge branch 'co_intersec' into 'main' (2023-10-07 18:49) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[ab4d2a63c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab4d2a63c) la suite (2023-10-07 18:48) <Tommy>  
`* | | | `[7e44120a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e44120a7) retag 2.0.3 (2023-10-07 18:36) <static@sesamath-lampdev>  
`* | | | `[435e8372c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/435e8372c) Merge branch 'co_intersec' into 'main' (2023-10-07 18:06) <Tommy Barroy>  
`|\| | | `  
`| * | | `[ea0c3634f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea0c3634f) pour 2e point intersec (2023-10-07 18:05) <Tommy>  
`* | | |   `[bd0d817c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd0d817c0) Merge branch 'egaliteFcts' into 'main' (2023-10-07 18:01) <Rémi Deniaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[0d07ede42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d07ede42) améliorations egalite_fonctions (2023-10-07 17:59) <Rémi Deniaud>  
`|/ / /  `  
`* | |   `[b703a493b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b703a493b) Merge branch 'interUnion' into 'main' (2023-10-07 16:41) <Rémi Deniaud>  
`|\ \ \  `  
`| * | | `[c308a6b53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c308a6b53) tentative correction intervallesUnionInter (2023-10-07 16:40) <Rémi Deniaud>  
`|/ / /  `  
`* / / `[c887b83b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c887b83b1) retag 2.0.3 (2023-10-06 17:12) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[a853033b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a853033b0) Merge branch 'repare' into 'main' (2023-10-06 15:31) <Tommy Barroy>  
`|\ \  `  
`| * | `[dd4c692fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd4c692fa) corrige vert hori et correc (2023-10-06 15:30) <Tommy>  
`|/ /  `  
`* |   `[b359cfd46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b359cfd46) Merge branch 'deux_en_mm_temps' into 'main' (2023-10-06 15:26) <Tommy Barroy>  
`|\ \  `  
`| * | `[3dc256c03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3dc256c03) corrige vert hori et correc (2023-10-06 15:25) <Tommy>  
`| * | `[0020036ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0020036ae) en cours pour Yves (2023-10-06 14:42) <Tommy>  
`|/ /  `  
`* |   `[e371c2db8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e371c2db8) Merge branch 'new_terre' into 'main' (2023-10-05 15:39) <Tommy Barroy>  
`|\ \  `  
`| * | `[f4d1762f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f4d1762f0) en cours pour Yves (2023-10-05 15:38) <Tommy>  
`| * | `[629397238](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/629397238) en cours pour Yves (2023-10-05 12:20) <Tommy>  
`* | |   `[fb44f9495](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb44f9495) Merge branch 'Amelioration_Squelette_Calc_Vec_Multi_Edit' into 'main' (2023-10-05 14:44) <Yves Biton>  
`|\ \ \  `  
`| * | | `[dea17983c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dea17983c) Passage de la xxorrection des exercices du squelette Calc_Vec_Multi_Edit en mode MathQuill. (2023-10-05 14:42) <Yves Biton>  
`|/ / /  `  
`* | |   `[0bc18fa5a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0bc18fa5a) Merge branch 'Amelioration_Squelette_Calc_Param_Vect_Prosca' into 'main' (2023-10-05 11:02) <Yves Biton>  
`|\ \ \  `  
`| * | | `[d99361705](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d99361705) Passage de la correction des exercices de calcul sur produit scalaire en première en mode MathQuiil. (2023-10-05 11:00) <Yves Biton>  
`* | | |   `[215642c33](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/215642c33) Merge branch 'new_terre' into 'main' (2023-10-04 21:38) <Tommy Barroy>  
`|\ \ \ \  `  
`| | |/ /  `  
`| |/| |   `  
`| * | | `[69936be97](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/69936be97) en cours pour Yves (2023-10-04 21:35) <Tommy>  
`* | | |   `[14587ebad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14587ebad) Merge branch 'fixTestEtudeFonctionVariation' into 'main' (2023-10-04 16:44) <Jean-Claude Lhote>  
`|\ \ \ \  `  
`| * | | | `[33f17c0a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33f17c0a3) fix: Les flèches ont changé de style (le stroke-width vaut 9 et plus 9px) du coup le Locator était bredouille (2023-10-04 16:43) <Jean-claude Lhote>  
`|/ / / /  `  
`* | / / `[46df31adb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46df31adb) retag 2.0.3 (2023-10-04 13:39) <static@sesamath-lampdev>  
`| |/ /  `  
`|/| |   `  
`* | |   `[00a5e5451](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00a5e5451) Merge branch 'Amelioration_Exos_Calcul_Vectoriel' into 'main' (2023-10-04 13:10) <Yves Biton>  
`|\ \ \  `  
`| * | | `[eec4afbeb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eec4afbeb) Passage de la correction des exercices de calcul sur les vecteurs en mode MathQuiil. (2023-10-04 13:05) <Yves Biton>  
`* | | | `[aa205efbd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa205efbd) retag 2.0.3 (2023-10-04 11:35) <static@sesamath-lampdev>  
`| |_|/  `  
`|/| |   `  
`* | | `[dfe43030b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfe43030b) nettoyage (2023-10-04 10:34) <Daniel Caillibaud>  
`* | | `[e72e2ffd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e72e2ffd0) fix input de TableauConversion (on ne pouvait rien saisir dedans) (2023-10-04 10:34) <Daniel Caillibaud>  
`* | |   `[e4a6f29be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4a6f29be) Merge branch 'new_terre' into 'main' (2023-10-04 01:44) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| /   `  
`| |/    `  
`| * `[d433f3359](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d433f3359) new mais je suis loin (2023-10-04 01:43) <Tommy>  
`| * `[7ef870dac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ef870dac) new mais je suis loin (2023-10-04 01:22) <Tommy>  
`| * `[453cc967c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/453cc967c) new mais je suis loin (2023-10-04 01:20) <Tommy>  
`|/  `  
`*   `[2272ed9a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2272ed9a8) Merge branch 'Amelioration_Squelette_Eq_Param_MathQuill' into 'main' (2023-10-03 16:38) <Yves Biton>  
`|\  `  
`| * `[8d0434df0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d0434df0) Correction d'une erreur d'étourdeire sur squelette Calc_Param_Vect et amélioration squelette Eq_Param_Vect (2023-10-03 16:37) <Yves Biton>  
`|/  `  
`* `[c2d019907](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2d019907) Amélioration du squelette squelettemtg32_Eq_Param_Vect et passage en mode MathQuill des ressources qui l'utilisent. (2023-10-03 16:15) <Yves Biton>  
`*   `[f36e37614](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f36e37614) Merge branch 'Amelioration_Exos_Bases_Squelette_Sol_Mult_1Edit' into 'main' (2023-10-03 12:56) <Yves Biton>  
`|\  `  
`| * `[5d69045ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d69045ec) Amélioration du squelette squelettemtg32_Eq_Param_Sol_Multè1Edit et passage en mode MathQuill des ressources qui l'utilisent. (2023-10-03 12:53) <Yves Biton>  
`* | `[ac8c96132](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac8c96132) retag 2.0.3 (2023-10-02 22:10) <static@sesamath-lampdev>  
`* |   `[d552d02dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d552d02dc) Merge branch 'bug_vertical' into 'main' (2023-10-02 21:43) <Tommy Barroy>  
`|\ \  `  
`| * | `[f35d10397](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f35d10397) bug vert (2023-10-02 21:42) <Tommy>  
`* | | `[d7af7fc1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7af7fc1a) retag 2.0.3 (2023-10-02 09:23) <static@sesamath-lampdev>  
`| |/  `  
`|/|   `  
`* | `[1e710ebac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e710ebac) Amélioration du squelette squelettemtg32_Eq_Param_Sol_Mult et passage en mode MathQuill des ressources qui l'utilisent. (2023-10-02 08:32) <Yves Biton>  
`* | `[dceed89f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dceed89f9) Merge branch 'bug_vertical' into 'main' (2023-10-02 00:47) <Tommy Barroy>  
`|\| `  
`| * `[a5c58afc3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5c58afc3) bug equerre verticale (2023-10-02 00:46) <Tommy>  
`|/  `  
`* `[c8102acaf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c8102acaf) retag 2.0.3 (2023-10-01 22:38) <static@sesamath-lampdev>  
`*   `[21fe6b60a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/21fe6b60a) Merge branch 'NEw_sec' into 'main' (2023-10-01 22:09) <Tommy Barroy>  
`|\  `  
`| * `[b1adb9236](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1adb9236) new sec (2023-10-01 22:08) <Tommy>  
`* | `[5dc80896d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5dc80896d) retag 2.0.3 (2023-10-01 10:54) <static@sesamath-lampdev>  
`* |   `[ffe22a06e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffe22a06e) Merge branch 'Amelioration_Exo_squelette_Eq_Param_MathQuill' into 'main' (2023-10-01 09:20) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[aaabe6473](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aaabe6473) Passage en mode MathQuill de la correction de certains exos basés sur le squelette squelettemtg32_Eq_Param_MathQuill (2023-10-01 09:18) <Yves Biton>  
`* | `[2314a10e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2314a10e4) retag 2.0.3 (2023-09-30 15:10) <static@sesamath-lampdev>  
`|/  `  
`*   `[d6458b1d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d6458b1d0) Merge branch 'Amelioration_Squelette_Reduc_Meme_Denom' into 'main' (2023-09-30 13:05) <Yves Biton>  
`|\  `  
`| * `[ef3fdb8ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef3fdb8ec) Passage en mode MathQuill de la correction des exercices de réduction au même dénominateur formelle (2023-09-30 13:03) <Yves Biton>  
`* |   `[a6e584fbd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6e584fbd) Merge branch 'ptite_co' into 'main' (2023-09-30 12:05) <Tommy Barroy>  
`|\ \  `  
`| * | `[28b48ad3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28b48ad3d) fais de la place (2023-09-30 12:04) <Tommy>  
`* | | `[6da797751](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6da797751) Merge branch 'ptite_co' into 'main' (2023-09-30 11:58) <Tommy Barroy>  
`|\| | `  
`| * | `[2bdaf312d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2bdaf312d) arret anim en 2e question, correction mm quand c'est bon (2023-09-30 11:57) <Tommy>  
`|/ /  `  
`* |   `[0585574ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0585574ba) Merge branch 'fais_de_la_place' into 'main' (2023-09-30 11:30) <Tommy Barroy>  
`|\ \  `  
`| * | `[4ed9d7d31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ed9d7d31) pad assez de place en presentation 01 (2023-09-30 11:29) <Tommy>  
`|/ /  `  
`* / `[5fe67cbbf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5fe67cbbf) retag 2.0.3 (2023-09-30 10:22) <static@sesamath-lampdev>  
`|/  `  
`*   `[e371a1d60](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e371a1d60) Merge branch 'Amelioration_Squelette_Calc_Frac' into 'main' (2023-09-30 09:28) <Yves Biton>  
`|\  `  
`| * `[35e598794](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35e598794) Passage en mode MathQuill de la correction des exercices de calcul sur les fractions (2023-09-30 09:25) <Yves Biton>  
`* | `[ed0f9aec6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ed0f9aec6) retag 2.0.3 (2023-09-29 21:06) <static@sesamath-lampdev>  
`* |   `[a63bdd752](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a63bdd752) Merge branch 're_bug_cool' into 'main' (2023-09-29 20:31) <Tommy Barroy>  
`|\ \  `  
`| * | `[c7515744c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7515744c) un truc pas css rangé au mauvais endroit (2023-09-29 20:30) <Tommy>  
`|/ /  `  
`* |   `[19263283e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19263283e) Merge branch 'test_bug_couleur' into 'main' (2023-09-29 19:54) <Tommy Barroy>  
`|\ \  `  
`| * | `[0c1d18026](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c1d18026) un bug que je trouve pas , ptet a cause d'une reformulation interne des couleurs en background (2023-09-29 19:54) <Tommy>  
`|/ /  `  
`* |   `[9d0329040](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d0329040) Merge branch 'ne_spsec' into 'main' (2023-09-29 16:53) <Tommy Barroy>  
`|\ \  `  
`| * | `[ab556dd09](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ab556dd09) finish (2023-09-29 16:52) <Tommy>  
`| * | `[09e0069ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09e0069ee) en cours (2023-09-28 23:25) <Tommy>  
`| * | `[bd49e6095](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd49e6095) en cours (2023-09-28 19:55) <Tommy>  
`| * | `[122ad7c7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/122ad7c7e) EN COURS (2023-09-28 15:44) <Tommy>  
`| * | `[368983fb4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/368983fb4) new (2023-09-27 13:17) <Tommy>  
`* | | `[d167664c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d167664c7) retag 2.0.3 (2023-09-28 23:55) <static@sesamath-lampdev>  
`| |/  `  
`|/|   `  
`* |   `[ef0a97655](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef0a97655) Merge branch 'Amelioration_Squelette_Calc_Param_Multi_Edit' into 'main' (2023-09-28 20:51) <Yves Biton>  
`|\ \  `  
`| * | `[4e153fff9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e153fff9) Passage en mode MathQuill de la correction des exercices utilisant le squelette multi edit (ceux pour lesquels c'est possible) (2023-09-28 20:46) <Yves Biton>  
`* | |   `[126b09f5b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/126b09f5b) Merge branch 'virealert' into 'main' (2023-09-28 20:46) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[65691a048](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65691a048) en cours (2023-09-28 20:45) <Tommy>  
`|/ / /  `  
`* | |   `[a02b1cf4a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a02b1cf4a) Merge branch 'bug_grands_cercle' into 'main' (2023-09-28 16:06) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[5f110ac4a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f110ac4a) bug grand crcle correction et réduit taille correction (2023-09-28 16:05) <Tommy>  
`|/ / /  `  
`* / / `[4410ffec7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4410ffec7) retag 2.0.3 (2023-09-27 22:49) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[d64856b7f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d64856b7f) Merge branch 'Amelioration_Exos_Derivation' into 'main' (2023-09-27 21:04) <Yves Biton>  
`|\ \  `  
`| * | `[5e582c711](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e582c711) Passage en mode MathQuill de la correction de tous les exercices de calcul de dérivées (2023-09-27 20:58) <Yves Biton>  
`* | | `[2b2f3c4bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b2f3c4bf) retag 2.0.3 (2023-09-27 13:49) <static@sesamath-lampdev>  
`* | |   `[96f3dadbf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/96f3dadbf) Merge branch 'tjrs_mm_bug_ailleurs' into 'main' (2023-09-27 13:23) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[8e81330b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e81330b9) le mm (2023-09-27 13:22) <Tommy>  
`| | |/  `  
`| |/|   `  
`* / | `[8d4ddbf48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8d4ddbf48) retag 2.0.3 (2023-09-27 13:13) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[2bd84a373](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2bd84a373) Merge branch 'rebug_a_cause_de_la_correction' into 'main' (2023-09-27 12:47) <Tommy Barroy>  
`|\ \  `  
`| * | `[4f3196153](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f3196153) note mieux memory (2023-09-27 12:46) <Tommy>  
`|/ /  `  
`* | `[41491f4f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41491f4f2) retag 2.0.3 (2023-09-27 07:11) <static@sesamath-lampdev>  
`* |   `[0f3126592](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f3126592) Merge branch 'deux_fois_plus_cool_memory_note' into 'main' (2023-09-27 00:12) <Tommy Barroy>  
`|\ \  `  
`| * | `[edc6ace90](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edc6ace90) note mieux memory (2023-09-26 22:58) <Tommy>  
`|/ /  `  
`* | `[c9c1acabc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9c1acabc) retag 2.0.3 (2023-09-26 22:10) <static@sesamath-lampdev>  
`* |   `[fa9e650d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fa9e650d8) Merge branch 'bug_vertical_numero_8' into 'main' (2023-09-26 21:19) <Tommy Barroy>  
`|\ \  `  
`| * | `[e4cdb6488](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4cdb6488) le mm bug mais encore a d'autres endroits (2023-09-26 21:18) <Tommy>  
`|/ /  `  
`* | `[0fd58914a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0fd58914a) retag 2.0.3 (2023-09-25 20:00) <static@sesamath-lampdev>  
`* |   `[d709cc710](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d709cc710) Merge branch 're_bug_equerre_point_regle' into 'main' (2023-09-25 19:31) <Tommy Barroy>  
`|\ \  `  
`| * | `[41ff1b85c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41ff1b85c) bug colle equerre point (2023-09-25 19:29) <Tommy>  
`|/ /  `  
`* | `[78063e220](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/78063e220) retag 2.0.3 (2023-09-25 18:28) <static@sesamath-lampdev>  
`* |   `[a71efee34](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a71efee34) Merge branch 'pb_x_et_parenthèses_sur_fonction_num_30_et_29' into 'main' (2023-09-25 18:00) <Tommy Barroy>  
`|\ \  `  
`| * | `[22b8142ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22b8142ec) vire console (2023-09-25 17:59) <Tommy>  
`| * | `[209ceb4f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/209ceb4f4) oubli () et x sur 2 fonc (2023-09-25 17:59) <Tommy>  
`|/ /  `  
`* |   `[14fcad8dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14fcad8dd) Merge branch 'extremumCanonique' into 'main' (2023-09-24 16:27) <Rémi Deniaud>  
`|\ \  `  
`| * | `[266daec95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/266daec95) gestion du signe du coef devant a (2023-09-24 16:23) <Rémi Deniaud>  
`* | | `[1aebebbfb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1aebebbfb) retag 2.0.3 (2023-09-24 11:51) <static@sesamath-lampdev>  
`| |/  `  
`|/|   `  
`* |   `[fe66fd501](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe66fd501) Merge branch 'Amelioration_Exo_Developpement' into 'main' (2023-09-24 11:22) <Yves Biton>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[336bb3253](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/336bb3253) Passage en mode MathQuill de la correction des exercices de développement (2023-09-24 11:19) <Yves Biton>  
`|/  `  
`* `[2b38cfb2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b38cfb2f) retag 2.0.3 (2023-09-22 15:15) <static@sesamath-lampdev>  
`*   `[24a8ab24b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24a8ab24b) Merge branch 'Correcrion_Exo_Factorisstion' into 'main' (2023-09-22 14:35) <Yves Biton>  
`|\  `  
`| * `[6c7ae46d0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c7ae46d0) Correction d'un exo de factorisation (2023-09-22 14:33) <Yves Biton>  
`|/  `  
`* `[a4da40b64](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a4da40b64) retag 2.0.3 (2023-09-22 12:38) <static@sesamath-lampdev>  
`*   `[0971fa01f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0971fa01f) Merge branch 'Amelioration_Exos_Fact' into 'main' (2023-09-22 11:36) <Yves Biton>  
`|\  `  
`| * `[f1f5f1e5c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1f5f1e5c) Passage exos de factorisation en mode MathQuill pour la correction. (2023-09-22 10:36) <Yves Biton>  
`* |   `[0f6c67490](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f6c67490) Merge branch 'calcul_espace' into 'main' (2023-09-21 23:17) <Tommy Barroy>  
`|\ \  `  
`| * | `[362277a05](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/362277a05) un texte oublié (2023-09-21 23:11) <Tommy>  
`| * | `[220de7da8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/220de7da8) vire un getdonnees (2023-09-21 23:06) <Tommy>  
`|/ /  `  
`* |   `[1c50c65fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c50c65fe) Merge branch 'fixPremier01' into 'main' (2023-09-21 19:17) <Tommy Barroy>  
`|\ \  `  
`| * | `[9e106936e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e106936e) fix vérif des paramètres et du tirage aléatoire des nombres de la section premier01 (2023-09-15 18:55) <Daniel Caillibaud>  
`* | | `[b7368528d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7368528d) fix préfixe des logs (2023-09-21 11:07) <Daniel Caillibaud>  
`* | | `[1160c54bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1160c54bd) Ajout police Noto-emoji en fallback (pour les emoji utf8) (2023-09-21 10:41) <Daniel Caillibaud>  
`* | | `[d5e2069f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5e2069f2) retag 2.0.3 (2023-09-20 18:47) <static@sesamath-lampdev>  
`* | |   `[ee240f277](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee240f277) Merge branch 'reco_equuerr_+_regle' into 'main' (2023-09-20 17:26) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[983e3bc50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/983e3bc50) reco place equerre regle (2023-09-20 17:24) <Tommy>  
`|/ / /  `  
`* | |   `[ee591983f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee591983f) Merge branch 'refais_png' into 'main' (2023-09-20 15:21) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[ac06d322d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac06d322d) bizarre je l'avais mise (2023-09-20 15:20) <Tommy>  
`|/ / /  `  
`* | |   `[9416e2864](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9416e2864) Merge branch 'recine_def' into 'main' (2023-09-20 15:18) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[093495336](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/093495336) nouvelle sur racine carrée (2023-09-20 15:16) <Tommy>  
`|/ / /  `  
`* | | `[46e1b8a8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46e1b8a8e) retag 2.0.3 (2023-09-19 16:53) <static@sesamath-lampdev>  
`* | |   `[49bfd7f8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49bfd7f8c) Merge branch 'bug_equerre_verticale' into 'main' (2023-09-19 15:56) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[dfda5d52f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfda5d52f) bug sur equerre verticale (2023-09-19 15:54) <Tommy>  
`* | | | `[151cd8300](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/151cd8300) retag 2.0.3 (2023-09-17 09:49) <static@sesamath-lampdev>  
`| |_|/  `  
`|/| |   `  
`* | |   `[65e66fc13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65e66fc13) Merge branch 'Amelioration_Exo_Fact_ax²+kx' into 'main' (2023-09-17 09:19) <Yves Biton>  
`|\ \ \  `  
`| * | | `[51ddeb13a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51ddeb13a) Passage exo factorisation en mode MathQuill pour factorisation (2023-09-17 09:17) <Yves Biton>  
`|/ / /  `  
`* | | `[c9fdc1c15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9fdc1c15) retag 2.0.3 (2023-09-16 17:52) <static@sesamath-lampdev>  
`* | |   `[f210b1f2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f210b1f2f) Merge branch 'Correction_Consigne_Exo_Factorisation' into 'main' (2023-09-16 16:49) <Yves Biton>  
`|\ \ \  `  
`| * | | `[cb053bb88](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb053bb88) Correction consigne exo factorisation (2023-09-16 16:49) <Yves Biton>  
`|/ / /  `  
`* | |   `[fb498107f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb498107f) Merge branch 'Amelioration_Squelette_Factorisation' into 'main' (2023-09-16 16:43) <Yves Biton>  
`|\ \ \  `  
`| |_|/  `  
`|/| |   `  
`| * | `[d065eb924](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d065eb924) Amélioration dusequelette de factorisation pour pouvoir afficher la solution en mode texte. Amélioration de deux exercices de factorisation. (2023-09-16 16:33) <Yves Biton>  
`* | | `[2d8a15c8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d8a15c8d) retag 2.0.3 (2023-09-15 17:23) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[9f9eb441d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f9eb441d) Merge branch 'Amelioration_Consigne_Exo_Factorisation' into 'main' (2023-09-15 16:54) <Yves Biton>  
`|\ \  `  
`| * | `[444247a2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/444247a2e) Amélioration de la consigne d'un exo de factorisation (2023-09-15 16:53) <Yves Biton>  
`* | | `[6df797c70](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6df797c70) retag 2.0.3 (2023-09-15 16:04) <static@sesamath-lampdev>  
`|/ /  `  
`* | `[d997716fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d997716fd) Ajout d'une exercice de factorisation (2023-09-15 15:22) <Yves Biton>  
`* | `[47235942a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/47235942a) retag 2.0.3 (2023-09-15 11:07) <static@sesamath-lampdev>  
`|/  `  
`*   `[05951ae23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05951ae23) Merge branch 'co_edit_mtg_nom_des_points_alea' into 'main' (2023-09-15 10:16) <Tommy Barroy>  
`|\  `  
`| * `[234a3e8e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/234a3e8e2) bug sur nom des points alea en editeur (2023-09-15 10:15) <Tommy>  
`|/  `  
`*   `[2cbe1027c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2cbe1027c) Merge branch 'amelio_rezeq02_init' into 'main' (2023-09-15 09:42) <Tommy Barroy>  
`|\  `  
`| * `[dc459cb52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc459cb52) finish bis (2023-09-15 09:39) <Tommy>  
`|/  `  
`* `[339a71eff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/339a71eff) retag 2.0.3 (2023-09-15 09:16) <static@sesamath-lampdev>  
`*   `[b069716a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b069716a5) Merge branch 'new_carre' into 'main' (2023-09-14 21:56) <Tommy Barroy>  
`|\  `  
`| * `[eb96c513f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb96c513f) finish ? (2023-09-14 21:55) <Tommy>  
`| * `[f52c744ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f52c744ac) para a droite quand y'a seg ok (2023-09-14 21:10) <Tommy>  
`| * `[a6aae5e44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a6aae5e44) pour sauv (2023-09-13 21:02) <Tommy>  
`| * `[e44c47f37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e44c47f37) sauv (2023-09-11 21:20) <Tommy>  
`| * `[9b82fe1c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b82fe1c5) cache le bouton commencer (2023-09-11 17:27) <Tommy>  
`* | `[8f8d7afae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f8d7afae) retag 2.0.3 (2023-09-14 18:02) <static@sesamath-lampdev>  
`* |   `[e0fcbe8d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e0fcbe8d6) Merge branch 'co_para_seg_droite' into 'main' (2023-09-14 17:36) <Tommy Barroy>  
`|\ \  `  
`| * | `[415281c88](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/415281c88) para a droite quand y'a seg ok (2023-09-14 17:34) <Tommy>  
`|/ /  `  
`* | `[7a151c235](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a151c235) màj mathgraph.d.ts (2023-09-12 08:58) <Daniel Caillibaud>  
`* |   `[5d1e0b164](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5d1e0b164) Merge branch 'bugsnag_test' into 'main' (2023-09-11 20:04) <Tommy Barroy>  
`|\ \  `  
`| * | `[377c3ae1c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/377c3ae1c) pour bugsnag (2023-09-11 20:03) <Tommy>  
`|/ /  `  
`* | `[efa719ae4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/efa719ae4) retag 2.0.3 (2023-09-11 17:59) <static@sesamath-lampdev>  
`* |   `[0a6ebbdef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a6ebbdef) Merge branch 'co_mode_programme' into 'main' (2023-09-11 17:33) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[5338d7fef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5338d7fef) erreur avec programme en menu (2023-09-11 17:32) <Tommy>  
`|/  `  
`* `[b4e8bf7ad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4e8bf7ad) retag 2.0.3 (2023-09-08 22:51) <static@sesamath-lampdev>  
`*   `[54f5a806f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54f5a806f) Merge branch 'vireBoutonCommencer' into 'main' (2023-09-08 22:21) <Tommy Barroy>  
`|\  `  
`| * `[12924e933](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12924e933) cache le bouton commencer (2023-09-08 22:21) <Tommy>  
`|/  `  
`* `[f32e9f173](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f32e9f173) retag 2.0.3 (2023-09-08 17:07) <static@sesamath-lampdev>  
`*   `[a5287b105](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5287b105) Merge branch 'cacheBouton' into 'main' (2023-09-08 16:28) <Tommy Barroy>  
`|\  `  
`| * `[d49d8ef46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d49d8ef46) cache le bouton valider dans tous les cas (2023-09-08 16:27) <Tommy>  
`|/  `  
`* `[d03d97d03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d03d97d03) on exclu tous les build* (2023-09-08 13:50) <Daniel Caillibaud>  
`* `[9edc7d80b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9edc7d80b) alert sur import.meta inconnu (2023-09-08 13:49) <Daniel Caillibaud>  
`* `[19ca7a4fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19ca7a4fc) retag 2.0.3 (2023-09-07 23:46) <static@sesamath-lampdev>  
`*   `[ac931feb0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac931feb0) Merge branch 'recipThales' into 'main' (2023-09-07 22:55) <Tommy Barroy>  
`|\  `  
`| * `[120cb657e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/120cb657e) recip new (2023-09-07 22:54) <Tommy>  
`| * `[350c9d07e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/350c9d07e) recip new (2023-09-06 14:57) <Tommy>  
`|/  `  
`* `[61a3c337b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61a3c337b) retag 2.0.3 (2023-09-05 22:18) <static@sesamath-lampdev>  
`*   `[1d9656f7b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d9656f7b) Merge branch 'parent_sur_puissimple' into 'main' (2023-09-05 21:52) <Tommy Barroy>  
`|\  `  
`| * `[c7dcfe676](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7dcfe676) met des grnades parenthèses (2023-09-05 21:51) <Tommy>  
`|/  `  
`* `[1d4508fe0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d4508fe0) retag 2.0.3 (2023-09-05 18:30) <static@sesamath-lampdev>  
`*   `[906241ad2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/906241ad2) Merge branch 'co_pointill_sans' into 'main' (2023-09-05 18:02) <Tommy Barroy>  
`|\  `  
`| * `[d66625a16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d66625a16) fonction a declarer vide si elle existe pas (2023-09-05 18:00) <Tommy>  
`|/  `  
`*   `[e46caff57](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e46caff57) Merge branch 'pointill_dans_progcontruction' into 'main' (2023-09-05 17:11) <Tommy Barroy>  
`|\  `  
`| * `[34ebbe370](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34ebbe370) bouton pointill dans fig mg32 (2023-09-05 17:10) <Tommy>  
`| * `[b268f1bf0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b268f1bf0) bouton pointill dans fig mg32 (2023-09-04 12:23) <Tommy>  
`|/  `  
`* `[2e824290a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e824290a) retag 2.0.3 (2023-09-04 12:07) <static@sesamath-lampdev>  
`*   `[ac7240995](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac7240995) Merge branch 'mod_correct_pour_efface' into 'main' (2023-09-04 11:31) <Tommy Barroy>  
`|\  `  
`| * `[0ecc48268](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ecc48268) ajoute contient dans obj de départ et fait correction et avertissement pour traits de construction (2023-09-04 11:28) <Tommy>  
`* | `[b46d1e90f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b46d1e90f) retag 2.0.3 (2023-09-04 10:17) <static@sesamath-lampdev>  
`* | `[a5a7f0b55](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5a7f0b55) Merge branch 'mod_correct_pour_efface' into 'main' (2023-09-04 09:44) <Tommy Barroy>  
`|\| `  
`| * `[1f73c99c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f73c99c8) oubli num (2023-09-04 09:42) <Tommy>  
`|/  `  
`* `[b92aea223](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b92aea223) retag 2.0.3 (2023-09-03 23:10) <static@sesamath-lampdev>  
`*   `[fe5962976](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe5962976) Merge branch 'inter_big_num' into 'main' (2023-09-03 22:44) <Tommy Barroy>  
`|\  `  
`| * `[7fd310bce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7fd310bce) oubli num (2023-09-03 22:44) <Tommy>  
`|/  `  
`* `[4733a8c63](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4733a8c63) retag 2.0.3 (2023-09-03 14:38) <static@sesamath-lampdev>  
`*   `[e1f937299](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1f937299) Merge branch 'point_et_efface' into 'main' (2023-09-03 14:11) <Tommy Barroy>  
`|\  `  
`| * `[1b5aaf7f6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b5aaf7f6) co inter dep et pointille dans qqchose (2023-09-03 14:11) <Tommy>  
`* | `[0c7afe23f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c7afe23f) retag 2.0.3 (2023-09-03 10:31) <static@sesamath-lampdev>  
`|/  `  
`*   `[109799e5c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/109799e5c) Merge branch 'aj_fig_dep' into 'main' (2023-09-03 10:04) <Tommy Barroy>  
`|\  `  
`| *   `[51cc8e155](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51cc8e155) Merge branch 'main' into 'aj_fig_dep' (2023-09-03 08:04) <Tommy Barroy>  
`| |\  `  
`| |/  `  
`|/|   `  
`* | `[9704a5769](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9704a5769) retag 2.0.3 (2023-09-02 18:04) <static@sesamath-lampdev>  
`* |   `[e87af5931](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e87af5931) Merge branch 'Correction_Section_Reduc' into 'main' (2023-09-02 17:28) <Yves Biton>  
`|\ \  `  
`| * | `[576a7634c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/576a7634c) Correction d'une balise </i> en trop dans l'énoncé (2023-09-02 17:26) <Yves Biton>  
`* | | `[bd41b7b45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd41b7b45) retag 2.0.3 (2023-09-01 23:23) <static@sesamath-lampdev>  
`* | |   `[3435f2a7b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3435f2a7b) Merge branch 're_re_re' into 'main' (2023-09-01 23:08) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[981e43e0e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/981e43e0e) ajoute point sue segment dan sfig de départ (2023-09-01 23:08) <Tommy>  
`* | | | `[5dcd3e7e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5dcd3e7e8) Merge branch 're_re_re' into 'main' (2023-09-01 22:58) <Tommy Barroy>  
`|\| | | `  
`| * | | `[09ff72c45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09ff72c45) ajoute point sue segment dan sfig de départ (2023-09-01 22:57) <Tommy>  
`| * | | `[9f97a035f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f97a035f) ajoute point sue segment dan sfig de départ (2023-09-01 22:52) <Tommy>  
`|/ / /  `  
`| | * `[01043d72f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01043d72f) aj plein trucs de départ (2023-09-03 10:01) <Tommy>  
`| |/  `  
`|/|   `  
`* |   `[f0c5deb2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0c5deb2b) Merge branch 're_point_sur' into 'main' (2023-09-01 22:42) <Tommy Barroy>  
`|\ \  `  
`| * | `[d34e2884d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d34e2884d) ajoute point sue segment dan sfig de départ (2023-09-01 22:41) <Tommy>  
`|/ /  `  
`* | `[e0810e679](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e0810e679) retag 2.0.3 (2023-09-01 22:30) <static@sesamath-lampdev>  
`* |   `[79a4bcf6c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79a4bcf6c) Merge branch 'aj_point_sur_seg_deb' into 'main' (2023-09-01 22:05) <Tommy Barroy>  
`|\ \  `  
`| * | `[2357102a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2357102a5) ajoute point sue segment dan sfig de départ (2023-09-01 22:03) <Tommy>  
`|/ /  `  
`* |   `[5ca959302](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ca959302) Merge branch 'cont_mtg' into 'main' (2023-09-01 02:38) <Tommy Barroy>  
`|\ \  `  
`| * | `[5fe24cc91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5fe24cc91) bug dans le pointille (2023-09-01 02:37) <Tommy>  
`* | | `[9c0876080](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c0876080) retag 2.0.3 (2023-09-01 02:10) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[e065eddbb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e065eddbb) Merge branch 'co_mtg' into 'main' (2023-09-01 01:30) <Tommy Barroy>  
`|\ \  `  
`| * | `[8b616fd4a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b616fd4a) bug dans le pointille (2023-09-01 01:29) <Tommy>  
`|/ /  `  
`* | `[192195c8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/192195c8d) retag 2.0.3 (2023-09-01 01:20) <static@sesamath-lampdev>  
`* |   `[9d61fc8a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d61fc8a8) Merge branch 'aj_seg_mtg' into 'main' (2023-09-01 00:55) <Tommy Barroy>  
`|\ \  `  
`| * | `[2d0d22154](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d0d22154) ajoute segments dans figure depart et bloc pointillés (2023-09-01 00:54) <Tommy>  
`| * | `[aa37ba6b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa37ba6b6) ajoute segments dans figure depart et bloc pointillés (2023-09-01 00:53) <Tommy>  
`|/ /  `  
`* | `[d4948c719](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4948c719) retag 2.0.3 (2023-08-31 11:26) <static@sesamath-lampdev>  
`* |   `[b58594679](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b58594679) Merge branch 'mod_memo' into 'main' (2023-08-31 10:21) <Tommy Barroy>  
`|\ \  `  
`| * | `[bd8907098](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd8907098) constenu ecris sur fond je sais plus pourquoi (2023-08-31 10:21) <Tommy>  
`|/ /  `  
`* | `[b4c590be4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4c590be4) retag 2.0.3 (2023-08-30 21:51) <static@sesamath-lampdev>  
`* |   `[56fff49c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/56fff49c1) Merge branch 'mod_JL' into 'main' (2023-08-30 21:18) <Tommy Barroy>  
`|\ \  `  
`| * | `[4df5f3d48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4df5f3d48) chois ax + b viré de linéaire (2023-08-30 21:11) <Tommy>  
`|/ /  `  
`* | `[aaded50a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aaded50a8) retag 2.0.3 (2023-08-30 14:58) <static@sesamath-lampdev>  
`* |   `[edf013e65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edf013e65) Merge branch 'orthobis' into 'main' (2023-08-30 14:28) <Tommy Barroy>  
`|\ \  `  
`| * | `[e15eff1b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e15eff1b6) orthographe (2023-08-30 14:28) <Tommy>  
`|/ /  `  
`* | `[4ff6fa3d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ff6fa3d1) retag 2.0.3 (2023-08-29 21:27) <static@sesamath-lampdev>  
`* |   `[f7cd07162](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7cd07162) Merge branch 'le_e' into 'main' (2023-08-29 21:02) <Tommy Barroy>  
`|\ \  `  
`| * | `[dc97165b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc97165b8) orthographe (2023-08-29 21:01) <Tommy>  
`* | | `[bf0e5bfe9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf0e5bfe9) retag 2.0.3 (2023-08-29 20:59) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[3f82dc03f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f82dc03f) Merge branch 'chg_parent' into 'main' (2023-08-29 20:04) <Tommy Barroy>  
`|\ \  `  
`| * | `[2ecaae46c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ecaae46c) met des parenthèses plus grandes (2023-08-29 20:03) <Tommy>  
`|/ /  `  
`* | `[f3c125a00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3c125a00) retag 2.0.3 (2023-08-29 04:01) <static@sesamath-lampdev>  
`* |   `[9a3ca9f97](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a3ca9f97) Merge branch 'co_editor_memo' into 'main' (2023-08-29 03:36) <Tommy Barroy>  
`|\ \  `  
`| * | `[7341bd2c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7341bd2c4) editeur memory bug (2023-08-29 03:35) <Tommy>  
`|/ /  `  
`* | `[8dd5f26cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8dd5f26cb) retag 2.0.3 (2023-08-29 01:03) <static@sesamath-lampdev>  
`* |   `[9ff99fb4c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ff99fb4c) Merge branch 'ratio' into 'main' (2023-08-29 00:27) <Tommy Barroy>  
`|\ \  `  
`| * | `[8416cc618](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8416cc618) avec 2 question et corrige represnomb mode memory (2023-08-29 00:25) <Tommy>  
`| * | `[927f8a237](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/927f8a237) pour sauve (2023-08-28 19:49) <Tommy>  
`* | | `[3267c7034](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3267c7034) retag 2.0.3 (2023-08-28 22:29) <static@sesamath-lampdev>  
`* | |   `[89b471a9d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89b471a9d) Merge branch 'point_inter_cach' into 'main' (2023-08-28 21:50) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[2a7675b04](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a7675b04) bug sur perp sans point (2023-08-28 21:49) <Tommy>  
`| * | `[9f1d90cb9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f1d90cb9) bug sur point inter d'objet caché (2023-08-28 21:29) <Tommy>  
`|/ /  `  
`* | `[1b5f936ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b5f936ec) retag 2.0.3 (2023-08-28 02:04) <static@sesamath-lampdev>  
`* |   `[e1d75e035](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e1d75e035) Merge branch 'refaitMemory' into 'main' (2023-08-28 01:11) <Tommy Barroy>  
`|\ \  `  
`| * | `[d66dcf9d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d66dcf9d5) vire une console (2023-08-28 01:09) <Tommy>  
`| * | `[0af770877](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0af770877) modifie memory, cartes tjrs visibles au début Pas de temps limite pour voir comptage tjrs au clic (2023-08-28 01:08) <Tommy>  
`|/ /  `  
`* | `[bed9f1f84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bed9f1f84) retag 2.0.3 (2023-08-27 23:28) <static@sesamath-lampdev>  
`* |   `[358d6f8bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/358d6f8bc) Merge branch 'fonc05_co_correc' into 'main' (2023-08-27 23:00) <Tommy Barroy>  
`|\ \  `  
`| * | `[2aecdff7b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2aecdff7b) met a et b sur graphe (2023-08-27 22:59) <Tommy>  
`|/ /  `  
`* | `[7428fccad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7428fccad) retag 2.0.3 (2023-08-27 02:37) <static@sesamath-lampdev>  
`* |   `[006610942](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/006610942) Merge branch 'fon05' into 'main' (2023-08-27 02:05) <Tommy Barroy>  
`|\ \  `  
`| * | `[fd608a9a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd608a9a3) nee (2023-08-27 02:04) <Tommy>  
`| * | `[024505be7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/024505be7) correction antécédent 4 ou plus (2023-08-26 15:52) <Tommy>  
`* | | `[719120ab1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/719120ab1) retag 2.0.3 (2023-08-26 15:35) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[2c995cc6b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c995cc6b) Merge branch 'co_correc_fontion_antecedent_4_ou_plus' into 'main' (2023-08-26 15:09) <Tommy Barroy>  
`|\ \  `  
`| * | `[3ad2c17dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ad2c17dc) correction antécédent 4 ou plus (2023-08-26 15:09) <Tommy>  
`* | | `[af22c6394](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af22c6394) retag 2.0.3 (2023-08-26 01:10) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[81136509f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81136509f) Merge branch 'remod' into 'main' (2023-08-26 00:07) <Tommy Barroy>  
`|\ \  `  
`| * | `[493b7e67f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/493b7e67f) 'case' à la place de 'carré' (2023-08-26 00:05) <Tommy>  
`|/ /  `  
`* | `[aac60f063](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aac60f063) retag 2.0.3 (2023-08-25 16:53) <static@sesamath-lampdev>  
`* |   `[f3acdc329](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3acdc329) Merge branch 'co_nomme02_sphere' into 'main' (2023-08-25 16:23) <Tommy Barroy>  
`|\ \  `  
`| * | `[9e89f3029](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e89f3029) 2 erreur avec equateur et meridien (2023-08-25 16:22) <Tommy>  
`|/ /  `  
`* | `[b5d8bdef9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5d8bdef9) retag 2.0.3 (2023-08-22 01:51) <static@sesamath-lampdev>  
`* |   `[776123cc0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/776123cc0) Merge branch 'corrige_inter_dans_verif' into 'main' (2023-08-22 01:19) <Tommy Barroy>  
`|\ \  `  
`| * | `[12ab71857](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/12ab71857) passe outre l'erreur d intersection plusieurs fois (2023-08-22 01:18) <Tommy>  
`|/ /  `  
`* | `[a13d82601](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a13d82601) retag 2.0.3 (2023-08-21 21:35) <static@sesamath-lampdev>  
`* | `[1ae5b52c1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ae5b52c1) retag 2.0.3 (2023-08-21 21:04) <static@sesamath-lampdev>  
`* |   `[3090825ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3090825ae) Merge branch 'geom' into 'main' (2023-08-21 21:03) <Tommy Barroy>  
`|\ \  `  
`| * | `[6e502f2fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e502f2fa) une new (2023-08-21 20:56) <Tommy>  
`|/ /  `  
`* |   `[66aefd3ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66aefd3ef) Merge branch 'vire_console' into 'main' (2023-08-19 11:50) <Tommy Barroy>  
`|\ \  `  
`| * | `[9bc98c46d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9bc98c46d) regle pb point Inter (2023-08-19 11:49) <Tommy>  
`|/ /  `  
`* |   `[321f62c60](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/321f62c60) Merge branch 'coInter' into 'main' (2023-08-19 11:48) <Tommy Barroy>  
`|\ \  `  
`| * | `[28983cf42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28983cf42) regle pb point Inter (2023-08-19 11:47) <Tommy>  
`|/ /  `  
`* | `[d12c575ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d12c575ec) retag 2.0.3 (2023-08-19 05:29) <static@sesamath-lampdev>  
`* |   `[2e2103dc6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e2103dc6) Merge branch 'amelio_miseenpage_et_ajout_mode_fonction' into 'main' (2023-08-19 05:02) <Tommy Barroy>  
`|\ \  `  
`| * | `[9ad0210b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ad0210b3) mise en page et param fonction (2023-08-19 05:01) <Tommy>  
`|/ /  `  
`* | `[9e54b2bf6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e54b2bf6) retag 2.0.3 (2023-08-18 23:03) <static@sesamath-lampdev>  
`* |   `[4307aebde](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4307aebde) Merge branch 'remet_mathquill_dans_blocsi' into 'main' (2023-08-18 22:35) <Tommy Barroy>  
`|\ \  `  
`| * | `[6f65dc8e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f65dc8e6) oubli de outils mathquill (2023-08-18 22:29) <Tommy>  
`* | | `[21b488b37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/21b488b37) retag 2.0.3 (2023-08-18 22:28) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[5ccf58585](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ccf58585) Merge branch 'replae_nom' into 'main' (2023-08-18 22:01) <Tommy Barroy>  
`|\ \  `  
`| * | `[87f78ea4f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87f78ea4f) essai de place des noms plus lisibles (2023-08-18 21:59) <Tommy>  
`| * | `[684ec2ea5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/684ec2ea5) essai de place des noms plus lisibles (2023-08-18 21:40) <Tommy>  
`|/ /  `  
`* | `[1c596a290](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c596a290) retag 2.0.3 (2023-08-17 16:10) <static@sesamath-lampdev>  
`* |   `[171fe8eea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/171fe8eea) Merge branch 'ajtemps' into 'main' (2023-08-17 15:41) <Tommy Barroy>  
`|\ \  `  
`| * | `[1ddaa9c3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ddaa9c3f) temps de visibilité des cartes en mode memory en paramétrable (2023-08-17 15:40) <Tommy>  
`|/ /  `  
`* | `[6d813c677](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d813c677) retag 2.0.3 (2023-08-17 13:38) <static@sesamath-lampdev>  
`* |   `[8daca20fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8daca20fc) Merge branch 'rajformleMake' into 'main' (2023-08-17 13:00) <Tommy Barroy>  
`|\ \  `  
`| * | `[742f4fdb5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/742f4fdb5) rajformuleMake (2023-08-17 12:57) <Tommy>  
`|/ /  `  
`* |   `[83f394f44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83f394f44) Merge branch 'factorisationEtudeFonction' into 'main' (2023-08-09 20:08) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[e310b1adc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e310b1adc) suppression de l'appel aux fonctions du fichier commun dans EtudeEquaDiff (2023-08-09 17:37) <Rémi Deniaud>  
`| * | `[690dcb5b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/690dcb5b0) code factorisé dans common.js pour l'init (2023-08-02 11:28) <Daniel Caillibaud>  
`* | |   `[e504b0ac2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e504b0ac2) Merge branch 'testOk' into 'main' (2023-08-02 10:41) <Jean-Claude Lhote>  
`|\ \ \  `  
`| * | | `[5c2dae268](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c2dae268) Verification test allScenarios Ok avec modèle 8 de fonction (2023-08-02 10:40) <Jean-claude Lhote>  
`|/ / /  `  
`* | |   `[78626905d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/78626905d) Merge branch 'pbLimiteEtudeFct' into 'main' (2023-08-02 10:21) <Rémi Deniaud>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[43848d20a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43848d20a) correction des limites du modele 8 dans EtudeFonction (2023-08-02 10:17) <Rémi Deniaud>  
`* | | `[c124a80cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c124a80cd) tabModeles et aideModeles mutualisés dans un module js commun aux sections d'étude de fonctions. (2023-08-02 10:20) <Daniel Caillibaud>  
`|/ /  `  
`* |   `[c305c49ba](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c305c49ba) Merge branch 'ajouteModeleFonctionTests' into 'main' (2023-08-02 10:05) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[aca1e1592](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aca1e1592) Ajout du modèle 8 aux tests (2023-08-02 10:05) <Jean-Claude Lhote>  
`|/ /  `  
`* |   `[27c0c5c4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27c0c5c4b) Merge branch 'etudeFonction' into 'main' (2023-07-30 19:37) <Rémi Deniaud>  
`|\ \  `  
`| * | `[853651568](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/853651568) ajout de l'étude d'une fonction polynôme de degré 2 dans les sections EtudeFonction (2023-07-30 19:34) <Rémi Deniaud>  
`| * | `[24f126532](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/24f126532) modif des commentaires sur le paramètre modele + suppression de getDonnees dans toutes les sections EtudeFonction (2023-07-30 16:55) <Rémi Deniaud>  
`|/ /  `  
`* | `[6dc778b45](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6dc778b45) retag 2.0.3 (2023-07-28 10:28) <static@sesamath-lampdev>  
`* |   `[56b1c051c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/56b1c051c) Merge branch 'nettoy' into 'main' (2023-07-28 10:00) <Tommy Barroy>  
`|\ \  `  
`| * | `[2c831df30](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2c831df30) nettoyage et corrige affiche bouton ok au début (2023-07-28 09:59) <Tommy>  
`| * | `[e803c2a38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e803c2a38) nettoy (2023-07-26 21:35) <Tommy>  
`* | |   `[75e2933e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75e2933e7) Merge branch 'miseenpage_fonction' into 'main' (2023-07-28 08:43) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[d12cae5f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d12cae5f1) pas de duppliqué aux points effacés (2023-07-28 08:42) <Tommy>  
`|/ / /  `  
`* | |   `[3ebb62148](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ebb62148) Merge branch 'flute' into 'main' (2023-07-28 08:06) <Tommy Barroy>  
`|\ \ \  `  
`| * | | `[d05486512](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d05486512) pas de duppliqué aux points effacés (2023-07-28 07:56) <Tommy>  
`|/ / /  `  
`* / / `[f89e15af2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f89e15af2) retag 2.0.3 (2023-07-27 10:55) <static@sesamath-lampdev>  
`|/ /  `  
`* |   `[4ec23345c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ec23345c) Merge branch 'amelio_placedep' into 'main' (2023-07-26 19:46) <Tommy Barroy>  
`|\ \  `  
`| * | `[0cfebd5dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0cfebd5dd) amelio placedep (2023-07-26 19:45) <Tommy>  
`|/ /  `  
`* | `[b00cff02a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b00cff02a) màj types mathgraph (2023-07-26 14:40) <Daniel Caillibaud>  
`* |   `[72163e5e5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72163e5e5) Merge branch 'remet_unite_dans_blokmtg01' into 'main' (2023-07-26 13:54) <Jean-Claude Lhote>  
`|\ \  `  
`| * | `[67be1a597](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67be1a597) fix: test blockmtg01 mis à jour par rapport aux modifs de la section (2023-07-26 13:54) <Jean-claude Lhote>  
`| * | `[c72ada3e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c72ada3e4) cache point inutile (2023-07-25 21:00) <Tommy>  
`* | |   `[16cb8a838](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16cb8a838) Merge branch 'newto' into 'main' (2023-07-25 23:39) <Tommy Barroy>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[feded13b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/feded13b1) finish (2023-07-25 23:38) <Tommy>  
`| * | `[17b5b194c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/17b5b194c) cache point inutile (2023-07-25 21:01) <Tommy>  
`|/ /  `  
`* |   `[c420db2bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c420db2bf) Merge branch 'fixTestBlokmtg01' into 'main' (2023-07-25 19:58) <Jean-Claude Lhote>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[878b97881](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/878b97881) fix: test blockmtg01 mis à jour par rapport aux modifs de la section (2023-07-25 19:57) <Jean-claude Lhote>  
`* |   `[10d9e38af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10d9e38af) Merge branch 'ajout_tag_dans_seg_pour_para_perp' into 'main' (2023-07-25 09:20) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[761bfc347](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/761bfc347) cache point inutile (2023-07-25 09:16) <Tommy>  
`| * `[03de5c303](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03de5c303) ajoute equi (2023-07-25 03:32) <Tommy>  
`| * `[cdfe77ac8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cdfe77ac8) ajoute equi (2023-07-25 02:39) <Tommy>  
`| * `[918be32a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/918be32a9) rajoute efface fin prg (2023-07-25 01:49) <Tommy>  
`| * `[e114eb4ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e114eb4ea) cache le milieu de médiatrice (2023-07-24 15:12) <Tommy>  
`* | `[33171f2ec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/33171f2ec) Merge branch 'ajout_tag_dans_seg_pour_para_perp' into 'main' (2023-07-24 15:03) <Tommy Barroy>  
`|\| `  
`| * `[90a7db762](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90a7db762) un tag foireux (2023-07-24 15:02) <Tommy>  
`* | `[53d8fff5c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53d8fff5c) Merge branch 'ajout_tag_dans_seg_pour_para_perp' into 'main' (2023-07-24 13:51) <Tommy Barroy>  
`|\| `  
`| * `[bb102393f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb102393f) manquait tag1 et 2 dans segments (2023-07-24 13:50) <Tommy>  
`|/  `  
`*   `[2d4f0ac93](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d4f0ac93) Merge branch 'varia' into 'main' (2023-07-24 13:28) <Tommy Barroy>  
`|\  `  
`| * `[64bab417a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64bab417a) nettoyage (2023-07-24 13:08) <Daniel Caillibaud>  
`| * `[a7310486d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7310486d) propor => proportionnalite (2023-07-24 13:01) <Daniel Caillibaud>  
`| * `[a193a0d04](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a193a0d04) calcul variation en pourcentage (2023-07-23 23:21) <Tommy>  
`* |   `[41c766865](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41c766865) Merge branch 'rezoneco' into 'main' (2023-07-24 13:28) <Tommy Barroy>  
`|\ \  `  
`| * | `[4b48dbe0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b48dbe0c) remet image pour pas de couleur et fonction sur mousemove et carré selectionné (2023-07-23 00:41) <Tommy>  
`| * | `[75e02cd96](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75e02cd96) remet image pour pas de couleur et fonction sur mousemove et carré selectionné (2023-07-23 00:41) <Tommy>  
`| |/  `  
`* |   `[8f6e15b1c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f6e15b1c) Merge branch 'imante' into 'main' (2023-07-24 13:27) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[f7e4fdb63](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7e4fdb63) code mort viré, simplification (2023-07-24 12:52) <Daniel Caillibaud>  
`| * `[f8d9eae16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8d9eae16) externalisation des figures (2023-07-24 12:52) <Daniel Caillibaud>  
`| * `[dd4845406](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd4845406) finish (2023-07-22 23:57) <Tommy>  
`| * `[486f31bad](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/486f31bad) newone (2023-07-21 00:16) <Tommy>  
`| * `[1e4effa16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e4effa16) duplicated en fin (2023-07-20 23:12) <Tommy>  
`* |   `[4714d40a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4714d40a9) Merge branch 'contenu' into 'main' (2023-07-22 01:55) <Tommy Barroy>  
`|\ \  `  
`| * | `[68ff45783](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68ff45783) change des en du (2023-07-22 01:53) <Tommy>  
`|/ /  `  
`* |   `[4b790bd10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b790bd10) Merge branch 'avec_duplicated' into 'main' (2023-07-22 01:45) <Tommy Barroy>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[8ac83aa50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8ac83aa50) finish duplicated (2023-07-22 01:42) <Tommy>  
`| * `[bae77147f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bae77147f) duplicated en fin (2023-07-20 23:14) <Tommy>  
`| * `[5e7a47d5e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e7a47d5e) duplicated en fin (2023-07-20 20:29) <Tommy>  
`|/  `  
`*   `[ce423ac4f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce423ac4f) Merge branch 'images_et_triangle' into 'main' (2023-07-19 21:32) <Tommy Barroy>  
`|\  `  
`| * `[b16b46362](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b16b46362) vire console (2023-07-19 21:31) <Tommy>  
`|/  `  
`* `[7c514d907](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c514d907) jsdoc fix (2023-07-19 19:38) <Daniel Caillibaud>  
`* `[7ae13b656](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ae13b656) j3pGetBaseUrl() virée (remplacée par la constante j3pBaseUrl) (2023-07-19 19:36) <Daniel Caillibaud>  
`* `[44ddbb23f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44ddbb23f) les scripts passent en esm (2023-07-19 19:36) <Daniel Caillibaud>  
`*   `[980dd7b19](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/980dd7b19) Merge branch 'corrige_effacer_dans_editeur' into 'main' (2023-07-19 19:03) <Tommy Barroy>  
`|\  `  
`| * `[bf1fb4cfc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf1fb4cfc) vire console (2023-07-19 19:02) <Tommy>  
`| * `[3b946493e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b946493e) corrige effacer dans editeur blokmg (2023-07-19 19:01) <Tommy>  
`|/  `  
`* `[0ce4fb4d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ce4fb4d2) fix import ZoneColoriage.js (2023-07-19 17:35) <Daniel Caillibaud>  
`* `[a48c46d99](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a48c46d99) en dev local on peut choisir editGraphe v1 ou v2 (2023-07-19 17:12) <Daniel Caillibaud>  
`* `[b95cc031f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b95cc031f) fix imports ZoneColoriage (2023-07-19 14:40) <Daniel Caillibaud>  
`*   `[50d734d4c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50d734d4c) Merge branch 'rectifColoriage' into 'main' (2023-07-19 14:16) <Daniel Caillibaud>  
`|\  `  
`| * `[d18e7787a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d18e7787a) rectifs depuis v2 (2023-07-19 14:15) <Daniel Caillibaud>  
`| * `[2f803babf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f803babf) liste des modèles externalisés (2023-07-19 14:11) <Daniel Caillibaud>  
`| * `[811a3b948](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/811a3b948) normalisation ZoneColoriage (2023-07-19 14:11) <Daniel Caillibaud>  
`|/  `  
`*   `[fcd41e315](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fcd41e315) Merge branch 'fixDoc' into 'main' (2023-07-19 14:06) <Daniel Caillibaud>  
`|\  `  
`| * `[f9a8a91fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f9a8a91fc) meilleure gestion d'erreur (2023-07-19 14:06) <Daniel Caillibaud>  
`| * `[85de0c0dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85de0c0dd) eslint fixes (2023-07-19 14:05) <Daniel Caillibaud>  
`| * `[f8ff20876](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8ff20876) plus de fichier avec un point dans le nom (jsdoc n'aime pas du tout) (2023-07-19 13:53) <Daniel Caillibaud>  
`| * `[d5b9d3e25](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5b9d3e25) amélioration jsdoc (@module ajoutés) (2023-07-19 13:43) <Daniel Caillibaud>  
`| * `[de1ef1df8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de1ef1df8) màj deps (2023-07-19 13:43) <Daniel Caillibaud>  
`|/  `  
`* `[d2c1b8fd0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2c1b8fd0) correction baseUrl fixLoader pour dev (2023-07-19 11:01) <Daniel Caillibaud>  
`*   `[5cf42834e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5cf42834e) Merge branch 'noms_cercles_rayon_et_point_sur_droite' into 'main' (2023-07-19 01:14) <Tommy Barroy>  
`|\  `  
`| * `[2da8a2995](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2da8a2995) ajout postmessage pour mtglibre (2023-07-19 01:14) <Tommy>  
`| * `[f95c50e87](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f95c50e87) ajout postmessage pour mtglibre (2023-07-19 01:14) <Tommy>  
`* | `[6a3c8d568](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a3c8d568) Merge branch 'noms_cercles_rayon_et_point_sur_droite' into 'main' (2023-07-19 01:12) <Tommy Barroy>  
`|\| `  
`| * `[28a220055](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28a220055) les trucs effacés sont plus recherchés en correction (2023-07-19 01:11) <Tommy>  
`* | `[85f0d9735](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85f0d9735) Merge branch 'noms_cercles_rayon_et_point_sur_droite' into 'main' (2023-07-19 00:40) <Tommy Barroy>  
`|\| `  
`| * `[8b56c3b03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8b56c3b03) 2 ptits bugs (2023-07-19 00:39) <Tommy>  
`|/  `  
`* `[467522d87](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/467522d87) Merge branch 'v2' into 'main' (2023-07-18 20:45) <Daniel Caillibaud>  
`* `[cd7cc95be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd7cc95be) Merge branch 'main' into v2 (2023-07-18 20:44) <Daniel Caillibaud>  
`*   `[4c48505de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c48505de) Merge branch 'fixGdPrixSym' into 'v2' (2023-07-18 20:40) <Daniel Caillibaud>  
`|\  `  
`| * `[5f956c6d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f956c6d3) Enfin un GdPrixSym qui fonctionne… (2023-07-18 20:38) <Daniel Caillibaud>  
`| * `[860dad140](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/860dad140) classes GdPrixSym externalisées et init superflu de donneesSection viré (2023-07-17 11:54) <Daniel Caillibaud>  
`* |   `[4dc804f14](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4dc804f14) Merge branch 'fixTestV2' into 'v2' (2023-07-17 17:51) <Jean-Claude Lhote>  
`|\ \  `  
`| |/  `  
`|/|   `  
`| * `[c638d4437](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c638d4437) fix: problème avec modèle 7 réglé (2023-07-17 17:31) <Jean-claude Lhote>  
`|/  `  
`* `[e8c9a75e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8c9a75e6) Merge branch 'main' into v2 (2023-07-12 19:12) <Daniel Caillibaud>  
`* `[2ae7ac72a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ae7ac72a) le code du loader passe dans lazyLoader, et loader devient un preloader qui fonctionne en script ordinaire (pas forcément chargé comme module) (2023-07-12 19:07) <Daniel Caillibaud>  
`* `[7d6479af8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d6479af8) fix import/export de ex_Calc_Multi_Edit-reprise_datas0 (2023-07-07 10:30) <Daniel Caillibaud>  
`*   `[cd3e32212](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd3e32212) Merge branch 'testBrowserEsm' into 'v2' (2023-07-07 10:04) <Daniel Caillibaud>  
`|\  `  
`| * `[8e12715a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e12715a3) fix: encore des modifs...Reste le scenario multiEdit_reprise qui marche pas. (2023-07-05 17:51) <Jean-claude Lhote>  
`| * `[1c2595a28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c2595a28) un peu de refacto dans les testBrowser, allScenarios ok, --verbose et autres options correctement prises en compte (2023-07-05 17:45) <Daniel Caillibaud>  
`| * `[d195277dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d195277dd) on revient à ace-builds 1.20.0 sinon ts n'est pas content (@todo voir pourquoi et rectifier avec une version actuelle de ace-build) (2023-07-05 11:51) <Daniel Caillibaud>  
`| * `[4be11754d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4be11754d) common.js => _common.js pour que runAllSections ne le prenne pas pour un test de section (2023-07-05 11:50) <Daniel Caillibaud>  
`| * `[51156479e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51156479e) fix imports (2023-07-05 11:48) <Daniel Caillibaud>  
`| * `[530a2e891](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/530a2e891) fix enforceStyleProp (getComputedStyle ne retourne pas toutes les propriétés css valides) (2023-07-05 11:48) <Daniel Caillibaud>  
`| * `[425b53390](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/425b53390) màj deps (2023-07-05 10:56) <Daniel Caillibaud>  
`| * `[0e2b886e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0e2b886e0) fix import de fonctions communes à plusieurs tests (mises à part pour l'occasion) (2023-07-05 10:54) <Daniel Caillibaud>  
`| * `[c2cb77c11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2cb77c11) code en double viré (pickRandom et getPGCD) (2023-07-05 10:53) <Daniel Caillibaud>  
`| * `[07b9bc7dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/07b9bc7dd) fct aléatoires mises dans lib/utils/random.ts et code en double viré (2023-07-05 10:32) <Daniel Caillibaud>  
`| * `[e3a386954](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e3a386954) graphe.graphe qui restait (2023-07-05 10:11) <Daniel Caillibaud>  
`| * `[01cd50d82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01cd50d82) webpack.config.js viré (2023-07-05 10:11) <Daniel Caillibaud>  
`| * `[7be09acd4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7be09acd4) CHANGEMENT IMPORTANT : tous les imports de notre code commencent désormais par src|test|testBrowser (2023-07-05 09:40) <Daniel Caillibaud>  
`| * `[bf579d392](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bf579d392) msg de debug dans pre-commit viré (2023-07-05 08:45) <Daniel Caillibaud>  
`| * `[3f2697e3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f2697e3a) typofix (2023-07-05 08:45) <Daniel Caillibaud>  
`| * `[2bf67b985](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2bf67b985) fix test etudeFonctionVariations (y'avait souvent des plantages sur des pgcd de nb négatifs) (2023-07-05 08:39) <Daniel Caillibaud>  
`| * `[7a9e85753](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a9e85753) amélioration hook pre-commit (2023-07-05 08:36) <Daniel Caillibaud>  
`| * `[c0bf0846f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0bf0846f) restait deux imports de ts (2023-07-05 07:53) <Daniel Caillibaud>  
`| * `[4f30321db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f30321db) revert de a66905d29 pour l'ajout de string.js et number.js dans helpers (ils sont déjà dans lib/utils) (2023-07-05 07:52) <Daniel Caillibaud>  
`| * `[0b16850b5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b16850b5) typofix (2023-07-05 07:44) <Daniel Caillibaud>  
`| * `[1b8a55092](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b8a55092) gitc fix import foireux (from 'helpers') (2023-07-04 21:34) <Daniel Caillibaud>  
`| * `[23c0b766a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23c0b766a) restait un graphe.graphe… (2023-07-04 21:27) <Daniel Caillibaud>  
`| * `[5cf030776](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5cf030776) normalisation export des datas (2023-07-04 21:12) <Daniel Caillibaud>  
`| * `[d14a176c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d14a176c7) revert de a66905d29 pour airevolagrand (+aj de l'option --loader ts-node/esm dans les scripts de package.json qui pourraient en avoir besoin) (2023-07-04 20:34) <Daniel Caillibaud>  
`| * `[cb1ef654a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb1ef654a) revert de a66905d29 pour puissancesdef10 (2023-07-04 20:13) <Daniel Caillibaud>  
`| * `[c9bd16c8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9bd16c8e) fix: encore des modifs... (2023-07-04 15:27) <Jean-claude Lhote>  
`| * `[a66905d29](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a66905d29) fix: modifications de l'import des graphes + ajout de fichiers .js dans helpers (number.ts et string.ts) (2023-07-04 15:07) <Jean-claude Lhote>  
`| * `[481286127](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/481286127) fix: extension dans package.json (2023-07-04 13:18) <Jean-claude Lhote>  
`| * `[2af34a90a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2af34a90a) ça marche déjà mieux (2023-06-30 17:52) <Daniel Caillibaud>  
`| * `[1e8c02981](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1e8c02981) WIP déjà pas mal de remplacement (2023-06-30 16:19) <Daniel Caillibaud>  
`|/  `  
`* `[6de070c65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6de070c65) aj architecture.md (2023-06-29 11:24) <Daniel Caillibaud>  
`* `[781570cb4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/781570cb4) Merge branch 'main' into v2 (2023-06-29 09:31) <Daniel Caillibaud>  
`* `[e4a1b1e37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4a1b1e37) Merge branch 'main' into v2 (2023-06-28 12:51) <Daniel Caillibaud>  
`* `[0ecd76898](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ecd76898) j3pSetBaseUrl viré (inutile avec vite et import.meta) (2023-06-27 17:23) <Daniel Caillibaud>  
`* `[a3821a4db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3821a4db) eval => safeEval (2023-06-27 17:19) <Daniel Caillibaud>  
`* `[19ec4b52e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19ec4b52e) manquait safeEval dans object.ts (2023-06-27 12:29) <Daniel Caillibaud>  
`* `[37818591d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37818591d) rectifs de console (2023-06-27 12:11) <Daniel Caillibaud>  
`* `[cfcc9d5ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cfcc9d5ae) Merge branch 'main' into v2 (2023-06-27 12:03) <Daniel Caillibaud>  
`* `[f26080af5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f26080af5) Merge branch 'main' into v2 (2023-05-23 15:16) <Daniel Caillibaud>  
`* `[cd31a0a26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cd31a0a26) Merge branch 'main' into v2 (2023-05-23 15:15) <Daniel Caillibaud>  
`* `[4e9b0ae85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4e9b0ae85) config différente pour vite start|build (2023-05-23 15:13) <Daniel Caillibaud>  
`* `[f09cc33be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f09cc33be) fix "$.widget is not a function" (2023-05-11 16:10) <Daniel Caillibaud>  
`* `[8cba8f3d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cba8f3d1) passage à TS 5 (2023-05-11 16:04) <Daniel Caillibaud>  
`* `[0a3d08a7b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a3d08a7b) fix import getJ3pConteneur (2023-05-11 15:46) <Daniel Caillibaud>  
`* `[22cfdcb8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/22cfdcb8e) Ajout de 5 sections (2023-05-11 15:35) <Daniel Caillibaud>  
`* `[944f37470](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/944f37470) fichier viré de main qui était toujours en v2 (2023-05-11 15:14) <Daniel Caillibaud>  
`* `[cac0a035f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cac0a035f) fix renommage non fait au merge précédent (2023-05-11 14:18) <Daniel Caillibaud>  
`* `[bd1ad056d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bd1ad056d) fix timeout en secondes dans loadJs (comme l'ancien load) (2023-05-11 14:18) <Daniel Caillibaud>  
`* `[20645ab3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20645ab3a) Merge branch 'refactoChargementScratchBlockly' into v2 (2023-05-11 11:33) <Daniel Caillibaud>  
`* `[2b4e19b64](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b4e19b64) Merge branch 'refactoChargementScratchBlockly' into v2 (2023-05-11 11:24) <Daniel Caillibaud>  
`* `[3d4b6469f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d4b6469f) màj deps (2023-05-04 13:17) <Daniel Caillibaud>  
`* `[43c2f321e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/43c2f321e) plus de mocha (2023-05-04 13:06) <Daniel Caillibaud>  
`* `[3abf03866](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3abf03866) on répercute les versions de main (2023-05-04 13:05) <Daniel Caillibaud>  
`* `[4641f64fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4641f64fe) fix aj des paramètres au loading qui n'avait pas été récupéré de main (2023-05-04 11:42) <Daniel Caillibaud>  
`* `[f425e1c2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f425e1c2e) fix import getZoneParente (2023-05-04 11:35) <Daniel Caillibaud>  
`* `[7a92aa469](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a92aa469) src/lib/utils/number est en ts dans v2 (2023-05-04 11:35) <Daniel Caillibaud>  
`* `[4189bd956](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4189bd956) Merge remote-tracking branch 'origin/main' into v2 (2023-05-04 11:31) <Daniel Caillibaud>  
`* `[57f247eb2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57f247eb2) aj typescript.md (2023-05-04 11:23) <Daniel Caillibaud>  
`* `[9575e3dd9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9575e3dd9) aj rule eslint (2023-05-04 11:20) <Daniel Caillibaud>  
`* `[050ef080e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/050ef080e) fix import sans extension (2023-05-04 11:20) <Daniel Caillibaud>  
`* `[5944228df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5944228df) aj commentaires (2023-05-04 11:20) <Daniel Caillibaud>  
`* `[f0e35096b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0e35096b) Merge branch 'main' into v2 (2023-04-17 15:44) <Daniel Caillibaud>  
`* `[212fb137f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/212fb137f) fix pbs imports (2023-03-29 09:31) <Daniel Caillibaud>  
`* `[724836464](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/724836464) Merge branch 'main' into v2 (2023-03-29 08:46) <Daniel Caillibaud>  
`* `[5a69a1cd7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a69a1cd7) Merge branch 'main' into v2 (2023-03-29 08:13) <Daniel Caillibaud>  
`*   `[5511f0897](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5511f0897) Merge branch 'nouveauTest' into 'v2' (2023-03-28 20:36) <Jean-Claude Lhote>  
`|\  `  
`| * `[3e7790103](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e7790103) Nouveau test proportionnalite01 (2023-03-28 20:36) <Jean-Claude Lhote>  
`|/  `  
`*   `[1ec0b3ea1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ec0b3ea1) Merge branch 'newTestPriorite' into 'v2' (2023-03-27 10:28) <Jean-Claude Lhote>  
`|\  `  
`| * `[7f629a0e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f629a0e6) New test priorite01 (2023-03-27 10:28) <Jean-Claude Lhote>  
`|/  `  
`* `[6d7f8324b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6d7f8324b) ajout getFirstDelimitedContent (2023-03-23 17:36) <Daniel Caillibaud>  
`* `[129516ba3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/129516ba3) fix import (2023-03-22 19:51) <Daniel Caillibaud>  
`* `[e50794fd7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e50794fd7) déclaration en double après merge (2023-03-22 19:47) <Daniel Caillibaud>  
`*   `[a2193e9d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2193e9d7) Merge remote-tracking branch 'origin/v2' into v2 (2023-03-22 19:46) <Daniel Caillibaud>  
`|\  `  
`| *   `[aeb15825e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aeb15825e) Merge branch 'ConnectorEditorValidate' into 'v2' (2023-03-22 17:38) <Jean-Claude Lhote>  
`| |\  `  
`| | * `[41c764643](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41c764643) Connector.getError() et Connector.validate() (2023-03-22 17:38) <Jean-Claude Lhote>  
`| |/  `  
`| *   `[7d7518c4f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7d7518c4f) Merge branch 'editor2' into 'v2' (2023-03-20 11:14) <Jean-Claude Lhote>  
`| |\  `  
`| | * `[1edc80caa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1edc80caa) draft: ajout Connector.getErrors() (2023-03-20 11:10) <Jean-claude Lhote>  
`| | * `[b6ea0fdfb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6ea0fdfb) notifs ajoutées : voir pourquoi le pasteGraph() catch une erreur avec le graphe copié dans le clipboard... (2023-03-20 10:56) <Jean-claude Lhote>  
`| | * `[0a5736d49](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a5736d49) Modification de ContextMenu, SceneContextMenu et NodeContextMenu (2023-03-20 10:56) <Jean-claude Lhote>  
`| | * `[faf77d1a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/faf77d1a5) ajout de Jsdoc (2023-03-20 10:56) <Jean-claude Lhote>  
`| | * `[ad43c5aef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad43c5aef) suppression cssClass test-custom (2023-03-20 10:56) <Jean-claude Lhote>  
`| | * `[cf4e02751](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf4e02751) Ovelrays Default et Hover Ok (2023-03-20 10:56) <Jean-claude Lhote>  
`| | * `[4cb7358c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4cb7358c0) Suppression de notie.alert() pour score>=0 (2023-03-20 10:56) <Jean-claude Lhote>  
`| | * `[cba441ad2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cba441ad2) le css met à jour le label au survol todo : récupérer la condition à afficher (2023-03-20 10:56) <Jean-claude Lhote>  
`| | * `[555a65c3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/555a65c3f) On a deux contenant dans le custom label (2023-03-20 10:55) <Jean-claude Lhote>  
`| | * `[dd26d48d9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd26d48d9) utilisation du module uuid à la place de jsplumb.util.uuid (2023-03-20 10:54) <Jean-claude Lhote>  
`| | * `[203721cf3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/203721cf3) check source des connecteurs (2023-03-20 10:54) <Jean-claude Lhote>  
`| | * `[05e579671](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/05e579671) Les labels changent au survol (2023-03-20 10:54) <Jean-claude Lhote>  
`| | * `[676b4f3b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/676b4f3b9) aj de l'action ajouter un graphe d'après son rid + factorisation de code de ContextMenu (à poursuivre pour NodeContextMenu) (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[95262e6c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/95262e6c6) Ajout de notie à la création d'une connexion existante (2023-03-20 10:54) <Jean-claude Lhote>  
`| | * `[d100d8ac1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d100d8ac1) fix de 'Lancer ce graphe' réparé. (2023-03-20 10:54) <Jean-claude Lhote>  
`| | * `[8f6c260c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f6c260c7) fix du more-vertical de scène + ajout de Undo/redo à côté... (2023-03-20 10:54) <Jean-claude Lhote>  
`| | * `[6af31f395](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6af31f395) on cherche d'abord dessous puis à droite, pour privilégier le scroll vers le bas (plus facile avec la roulette de souris) (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[fedef8782](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fedef8782) refacto méthodes de Grid pour travailler en coord et éviter trop de conversions px<=>coord (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[44b61b5cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44b61b5cf) la grille n'a plus de taille max, elle s'agrandit au fil des besoins (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[c0d2706a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0d2706a4) premierMultipleSuperieur déplacé dans lib/utils/number (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[f09d62346](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f09d62346) jsdoc (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[b02488af1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b02488af1) refacto copy/paste (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[51648d67a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51648d67a) Ajout d'actions qui ne sont pas annulables (init du graph et positionnement auto) (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[7630a757a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7630a757a) annuler/refaire ne sont proposés que s'il y a qqchose à annuler / refaire (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[34196dd40](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34196dd40) dernières ref au milestone virées (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[704dd4dbd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/704dd4dbd) réordonnancement imports et rectif commentaires & jsdoc (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[ee6f50884](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee6f50884) réordonnancement imports (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[9258dcc74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9258dcc74) faut pas modifier les scripts existants ;-) (2023-03-20 10:54) <Daniel Caillibaud>  
`| | * `[ac47d07f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ac47d07f7) Ajout de bouton Valider et Annuler sur l'ajout de nœud simple (2023-03-20 10:54) <Jean-claude Lhote>  
`| | * `[199c99a2e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/199c99a2e) Révision des types dans reducers.ts (2023-03-20 10:54) <Jean-claude Lhote>  
`| | * `[709140dab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/709140dab) Suppression de milestones (2023-03-20 10:54) <Jean-claude Lhote>  
`| | * `[ad5690471](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ad5690471) fix: mise en place des reducer ehancers undo() et redo() (2023-03-20 10:54) <Jean-claude Lhote>  
`| | * `[6535756cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6535756cb) retour de l'alignement sur la grille des noeuds déplacés (normalement jsPlumb s'en occupe en amont) (2023-03-20 10:53) <Jean-claude Lhote>  
`| | * `[796865222](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/796865222) fix: problème des connexions non réactualisées réglé (2023-03-20 10:53) <Jean-claude Lhote>  
`| | * `[6babaa0be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6babaa0be) draft: plus de problème de deferredConnector restant mais un soucis avec les connexions lors de l'undo changeNodesPositions (2023-03-20 10:53) <Jean-claude Lhote>  
`| | * `[e9e02ec7f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e9e02ec7f) fixe le problème des nœuds en double et des nœuds target supprimés pour le undo (2023-03-20 10:53) <Jean-claude Lhote>  
`| | * `[26934f068](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/26934f068) Mise en place du state undoable. (2023-03-20 10:53) <Jean-claude Lhote>  
`| | * `[5bda6b2d7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5bda6b2d7) draft: ajout de copier-coller dans la scène pas au point (une bonne idée ?) (2023-03-20 10:52) <Jean-claude Lhote>  
`| | * `[32fd864da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32fd864da) draft: sélection du nom de la section dans une liste lors de l'ajout d'un nœud simple (2023-03-20 10:50) <Jean-claude Lhote>  
`| | * `[1fc2f95b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1fc2f95b1) draft: ajout de copier-coller dans la scène pas au point (une bonne idée ?) (2023-03-20 10:50) <Jean-claude Lhote>  
`| |/  `  
`* | `[199f5d554](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/199f5d554) màj deps (2023-03-22 19:44) <Daniel Caillibaud>  
`* | `[55fe0fb7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55fe0fb7d) Merge branch 'main' into v2 (2023-03-22 19:43) <Daniel Caillibaud>  
`* | `[572e75e26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/572e75e26) recup de testBrowser from main (2023-03-22 19:13) <Daniel Caillibaud>  
`* | `[811bf869c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/811bf869c) mocha.opts viré (plus de mocha) (2023-03-16 11:04) <Daniel Caillibaud>  
`* | `[fc9e5dc73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fc9e5dc73) task loadAllLocal virée (désormais dans j3p_export, pour les exports Magnard) (2023-03-16 11:02) <Daniel Caillibaud>  
`* | `[4d3871bc9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d3871bc9) fix testBrowser list (cjs) (2023-03-16 10:59) <Daniel Caillibaud>  
`|/  `  
`* `[a073ba378](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a073ba378) Merge branch 'main' into v2 (2023-03-15 09:44) <Daniel Caillibaud>  
`* `[34dd63213](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34dd63213) xcas doit être importé (2023-03-15 09:41) <Daniel Caillibaud>  
`* `[ae214fda7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae214fda7) fix xcasBaseUrl (2023-03-15 09:40) <Daniel Caillibaud>  
`* `[cb7d35de8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb7d35de8) outil webxcas pas utilisé (2023-03-15 09:39) <Daniel Caillibaud>  
`* `[8126c3988](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8126c3988) fix import annexe (2023-03-15 09:34) <Daniel Caillibaud>  
`* `[5774c688c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5774c688c) Merge branch 'main' into v2 (2023-03-13 18:18) <Daniel Caillibaud>  
`* `[5984f1f6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5984f1f6e) fix imports (2023-03-13 18:03) <Daniel Caillibaud>  
`* `[91cea9546](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/91cea9546) Merge branch 'nettoyage' into v2 (2023-03-13 17:59) <Daniel Caillibaud>  
`* `[948ffc6a2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/948ffc6a2) fix pb async depuis usage de getNewScratch (2023-03-13 17:50) <Daniel Caillibaud>  
`* `[768c3ca72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/768c3ca72) Merge branch 'nettoyage' into v2 (2023-03-09 22:21) <Daniel Caillibaud>  
`* `[6cdefda15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cdefda15) Merge branch 'main' into v2 (2023-03-09 18:41) <Daniel Caillibaud>  
`* `[aa0470bb4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa0470bb4) tentative de fix du pre-commit qui sort à la première erreur eslint (2023-03-09 18:40) <Daniel Caillibaud>  
`* `[e0aa2be6d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e0aa2be6d) refacto du chargement de Blockly et Scratch, pour compatibilité vite (2023-03-09 18:39) <Daniel Caillibaud>  
`* `[4b67ed1a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b67ed1a0) update scratch-blocks (2023-03-09 18:27) <Daniel Caillibaud>  
`* `[b4d78bd37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4d78bd37) fix appel de j3pAffiche avec attr style interdit (remplacé par une classe css) (2023-03-09 18:26) <Daniel Caillibaud>  
`* `[53120ea13](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53120ea13) fix appel de j3pAffiche avec attr style interdit (2023-03-09 18:26) <Daniel Caillibaud>  
`* `[82de37ef1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/82de37ef1) aj diviseursdirect et GdPrixSym + async/await inutiles virés (2023-03-09 15:42) <Daniel Caillibaud>  
`* `[30add9f7c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30add9f7c) fichiers js ajoutés par le merge main mais inutile en v2 (2023-03-09 15:37) <Daniel Caillibaud>  
`* `[ff293c6bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ff293c6bf) bugfix getFirstParent (2023-03-09 15:37) <Daniel Caillibaud>  
`* `[6e98f32da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e98f32da) fix testBrowser/start.cjs (2023-03-09 15:17) <Daniel Caillibaud>  
`* `[703c49bda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/703c49bda) fix path j3pImporteAnnexe pour deux sections oubliées (2023-03-09 15:17) <Daniel Caillibaud>  
`* `[b63c4aa8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b63c4aa8d) fix (modif main mal mergée) (2023-03-09 13:46) <Daniel Caillibaud>  
`* `[32f8cd2e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32f8cd2e0) fix enforceShape (2023-03-09 13:43) <Daniel Caillibaud>  
`* `[1223d7cef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1223d7cef) fix tests des exports (2023-03-09 11:43) <Daniel Caillibaud>  
`* `[40d39fd9b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/40d39fd9b) Merge branch 'main' into v2 (2023-03-09 11:20) <Daniel Caillibaud>  
`* `[a7abf92a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7abf92a1) aj d'un script pour fixer build/loader.js (pour usage en crossdomain) (2023-03-09 11:11) <Daniel Caillibaud>  
`* `[281365cc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/281365cc4) try/catch inutile viré (2023-03-09 11:09) <Daniel Caillibaud>  
`* `[03ce332b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03ce332b9) modif main du package.json (2023-03-09 10:21) <Daniel Caillibaud>  
`* `[8350d3184](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8350d3184) build ok avec 4Go de RAM mais pas avec le plugin legacy (2023-03-09 10:06) <Daniel Caillibaud>  
`* `[6350b17df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6350b17df) on split un peu plus vendor-* (2023-03-09 09:54) <Daniel Caillibaud>  
`* `[276758050](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/276758050) import de jquery.terminal ok (2023-03-09 09:52) <Daniel Caillibaud>  
`* `[3a47a60f8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a47a60f8) conf webpack inutile virée (2023-03-09 09:50) <Daniel Caillibaud>  
`* `[423f6b5aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/423f6b5aa) aj de j3pGetBaseUrl pour compatibilité ascendante (2023-03-09 09:47) <Daniel Caillibaud>  
`* `[f6bd0e930](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f6bd0e930) fix j3pBaseUrl (au build le new URL('/', import.meta.url) nous remonte du js minifié en base64…) (2023-03-09 09:46) <Daniel Caillibaud>  
`* `[4444456bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4444456bb) on vire patch-package des dépendances (c'était utilisé seulement pour jquery.terminal pour basthon, inutile depuis qu'on charge la version minifiée en externals) (2023-03-09 09:44) <Daniel Caillibaud>  
`* `[77badb5f4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/77badb5f4) application du patch basthon à jquery.terminal.min.js (2023-03-09 09:34) <Daniel Caillibaud>  
`* `[d2a455a11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2a455a11) jquery.terminal déplacé dans docroot/externals/jquery.terminal pour que basthon fonctionne dans sa version build (2023-03-09 09:31) <Daniel Caillibaud>  
`* `[ecca2755b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecca2755b) fix serve du static en dev (2023-03-08 17:47) <Daniel Caillibaud>  
`* `[93edd4730](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/93edd4730) fix sourcemap (2023-03-08 17:45) <Daniel Caillibaud>  
`* `[cc31cb726](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc31cb726) commentaires (2023-03-08 17:08) <Daniel Caillibaud>  
`*   `[d9d75dc28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9d75dc28) Merge branch 'editor2' into v2 (2023-03-08 16:39) <Daniel Caillibaud>  
`|\  `  
`| * `[19f7df3af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19f7df3af) draft: ajout de copier-coller dans la scène pas au point (une bonne idée ?) (2023-03-07 18:48) <Jean-claude Lhote>  
`| * `[74b85990b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/74b85990b) ajout de commentaires (2023-03-07 12:12) <Jean-claude Lhote>  
`| * `[e889684c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e889684c7) editor: dispatch(changeNodesPositions()) ajouté (2023-03-07 11:56) <Jean-claude Lhote>  
`* | `[121290037](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/121290037) fichiers obsolètes virés (2023-03-08 16:33) <Daniel Caillibaud>  
`* | `[c246df710](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c246df710) une version qui build avec 3Go de RAM et jQuery ok (2023-03-08 16:29) <Daniel Caillibaud>  
`* | `[ee10c2239](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee10c2239) dynamic import de jquery (2023-03-08 15:45) <Daniel Caillibaud>  
`* | `[ecd90cb11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ecd90cb11) l'autoload importe loader (2023-03-08 15:44) <Daniel Caillibaud>  
`* | `[597963375](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/597963375) lancement du timeout décalé (2023-03-08 15:44) <Daniel Caillibaud>  
`* | `[48c4778e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48c4778e1) fix pb de chargement de jquery-ui/ui/widgets/sortable (2023-03-08 15:28) <Daniel Caillibaud>  
`* | `[666289a15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/666289a15) faut fixer la même version de basthon pour tout le monde (2023-03-08 14:47) <Daniel Caillibaud>  
`* | `[6dfe6459a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6dfe6459a) màj deps en virant webpack (inutile depuis qu'on ne build plus ace) (2023-03-08 13:31) <Daniel Caillibaud>  
`* | `[8cee9a4e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8cee9a4e8) import inutile viré (2023-03-08 13:03) <Daniel Caillibaud>  
`* | `[d95dbb9f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d95dbb9f1) Merge branch 'main' into v2 (2023-03-06 16:29) <Daniel Caillibaud>  
`* | `[21ed0f4f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/21ed0f4f9) console.debug autorisé (2023-03-06 16:28) <Daniel Caillibaud>  
`* | `[f8d86a19d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8d86a19d) Merge branch 'main' into v2 (2023-03-06 16:23) <Daniel Caillibaud>  
`* | `[e36088454](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e36088454) Merge branch 'editor2' into v2 (2023-03-06 11:39) <Daniel Caillibaud>  
`|\| `  
`| * `[3b19bedcc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b19bedcc) editor: grille de placement automatique fonctionnelle (2023-03-05 19:45) <Jean-claude Lhote>  
`| * `[e540079a8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e540079a8) editor: amélioration des fonctions de placement auto. Todo : gérer la grille dans les reducers (2023-03-03 21:54) <Jean-claude Lhote>  
`* | `[8fc102564](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8fc102564) Merge branch 'main' into v2 (2023-03-06 11:38) <Daniel Caillibaud>  
`* | `[13bd19198](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/13bd19198) Aj loadSkulpt (2023-03-06 11:35) <Daniel Caillibaud>  
`* | `[713dabdc0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/713dabdc0) il restait qq outilsexternes => externals (2023-03-06 11:35) <Daniel Caillibaud>  
`* | `[7064510af](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7064510af) outil basthon plus utilisé (2023-03-06 10:52) <Daniel Caillibaud>  
`* | `[3c8b7e878](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c8b7e878) scratch/legacy viré (2023-03-06 10:50) <Daniel Caillibaud>  
`* | `[8324621e8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8324621e8) build ok avec 4Go (2023-03-04 14:59) <Daniel Caillibaud>  
`* | `[3ad55ee10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ad55ee10) on sort ace du build (dans externals/editeurs/ace) (2023-03-04 14:40) <Daniel Caillibaud>  
`* | `[b9e2e6f15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b9e2e6f15) fix params de l'exemple 4 (2023-03-04 10:51) <Daniel Caillibaud>  
`|/  `  
`* `[49c2fc9a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49c2fc9a1) faut surtout pas préciser de http://localhost:8081/ en dur (2023-03-02 13:04) <Daniel Caillibaud>  
`* `[c5fe39d09](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c5fe39d09) faut préciser from 'lib/utils/dom/index' sinon le build passe pas (2023-03-02 12:58) <Daniel Caillibaud>  
`* `[f55735344](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f55735344) fix no-console (2023-03-02 12:40) <Daniel Caillibaud>  
`*   `[81656d03f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/81656d03f) Merge remote-tracking branch 'origin/editor2' into v2 (2023-03-02 12:39) <Daniel Caillibaud>  
`|\  `  
`| * `[1a5fbc4a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a5fbc4a0) editor: SceneContextMenu.ts ajouté, tooltipManager.ts ajouté, mise en place d'une grille de placement (2023-03-02 10:56) <Jean-claude Lhote>  
`| * `[dd2282aee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dd2282aee) draft: On peut ajouter des noeuds fin en quelques clics... (2023-03-01 20:46) <Jean-claude Lhote>  
`| * `[cfeb5e762](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cfeb5e762) draft: ajout de boutons valider/annuler pour le renommage des noeuds (2023-03-01 20:28) <Jean-claude Lhote>  
`| * `[d76e0db18](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d76e0db18) draft: ajout de noeud fin ou single sur la scène via clic droit ou picto... à fignoler. (2023-03-01 19:54) <Jean-claude Lhote>  
`| *   `[511ca5757](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/511ca5757) Merge branch 'connectorEditorNew' into 'editor2' (2023-02-28 21:36) <Jean-Claude Lhote>  
`| |\  `  
`| | * `[670291311](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/670291311) Connector editor new (2023-02-28 21:36) <Jean-Claude Lhote>  
`| |/  `  
`* | `[1766d2c68](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1766d2c68) modifs qui auraient dû être dans le merge précédent (2023-03-02 12:28) <Daniel Caillibaud>  
`* | `[b3a300682](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3a300682) Merge branch 'main' into v2 (2023-03-02 12:28) <Daniel Caillibaud>  
`* | `[53c736c6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53c736c6e) suite du nettoyage pre-merge (2023-03-01 20:53) <Daniel Caillibaud>  
`* | `[d8db9d624](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8db9d624) récup de fichiers de main (2023-03-01 20:50) <Daniel Caillibaud>  
`* | `[5f764a090](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f764a090) fichiers virés de main à virer avant fusion (2023-03-01 20:44) <Daniel Caillibaud>  
`* | `[f2fbc0858](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2fbc0858) Merge branch 'editor2' into v2 (2023-03-01 19:33) <Daniel Caillibaud>  
`|\| `  
`| * `[8f744aac1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8f744aac1) NodeContextMenu opérationnel (css à fignoler pour les erreurs) (2023-02-23 10:52) <Jean-claude Lhote>  
`| * `[f4b267906](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f4b267906) NodeContextMenu opérationnel (css à fignoler pour les erreurs) (2023-02-23 10:46) <Jean-claude Lhote>  
`| * `[29b6e309c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/29b6e309c) aj getFirstParentByTag (2023-02-23 10:13) <Daniel Caillibaud>  
`| * `[191c22902](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/191c22902) draft: modif mineure (2023-02-23 10:11) <Jean-claude Lhote>  
`| * `[c93611888](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c93611888) Draft: Formulaire des Nodes OK ? (à tester) (2023-02-22 15:41) <Jean-claude Lhote>  
`| * `[00ba67b6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00ba67b6f) WIP Le bouton '-' supprime un champ sauf si c'est le dernier... (2023-02-22 09:31) <Jean-claude Lhote>  
`| * `[5a8242dbc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5a8242dbc) fix: quelques petites corrections (2023-02-21 20:04) <Jean-claude Lhote>  
`| * `[5162aaec2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5162aaec2) ajouts (2023-02-20 21:00) <Jean-claude Lhote>  
`| * `[9cdaf5211](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9cdaf5211) Wip: amélioration de la saisie libre des listes (2023-02-20 20:58) <Jean-claude Lhote>  
`* |   `[32189bf84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32189bf84) Merge branch 'splitBuild' into v2 (2023-03-01 19:28) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[49f349971](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49f349971) tsc n'a plus besoin de 4Go de RAM (2023-03-01 19:06) <Daniel Caillibaud>  
`| * | `[4d9f1488e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d9f1488e) plus de @vite-ignore (2023-02-28 20:41) <Daniel Caillibaud>  
`| * | `[30b87fbf8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30b87fbf8) fix hook pre-commit (2023-02-28 20:41) <Daniel Caillibaud>  
`| * | `[4ff919348](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ff919348) mathquill chargé par la section (2023-02-28 19:56) <Daniel Caillibaud>  
`| * | `[154b2e6b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/154b2e6b6) modifs mineures (2023-02-28 19:50) <Daniel Caillibaud>  
`| * | `[98790c05f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98790c05f) config eslint simplifiée, sans type checking (délégué à tsc, exécuté en hook pre-commit) (2023-02-28 19:10) <Daniel Caillibaud>  
`| * | `[9197caceb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9197caceb) un essai avec de l'import natif (2023-02-23 21:10) <Daniel Caillibaud>  
`| * | `[7b53eb68b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b53eb68b) fix Blockly (2023-02-20 17:44) <Daniel Caillibaud>  
`| * | `[1c820ea0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c820ea0c) aj de sections (ajoutées à adresses.js dans main) (2023-02-20 17:43) <Daniel Caillibaud>  
`| * | `[0ad252d67](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ad252d67) fix import path pour getJ3pConteneur (2023-02-20 17:43) <Daniel Caillibaud>  
`| * | `[9c6932949](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c6932949) les apostrophes dans les noms de fichiers annexes sont remplacées par Prim (ça plante le plugin dynamic-import) (2023-02-20 16:46) <Daniel Caillibaud>  
`| * | `[6fa2b4f73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6fa2b4f73) manquait Blockly (2023-02-20 16:39) <Daniel Caillibaud>  
`* | | `[6f29ff57c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f29ff57c) on build sans le plugin legacy tant qu'on a pas réglé le pb de RAM (2023-02-23 17:12) <Daniel Caillibaud>  
`* | | `[144cad917](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/144cad917) aj scratch dans vendors (2023-02-23 17:12) <Daniel Caillibaud>  
`|/ /  `  
`* | `[8817f7ed9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8817f7ed9) on vire les essais avec les plugins webpack pour charger scratch-blocks (fonctionnait pas) (2023-02-20 15:36) <Daniel Caillibaud>  
`* | `[ae317c83b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae317c83b) build ok (avec 8Go RAM) (2023-02-20 15:22) <Daniel Caillibaud>  
`* | `[20e5670f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20e5670f9) màj Basthon 0.50.10 => 0.50.16 (2023-02-20 12:03) <Daniel Caillibaud>  
`* | `[4aff5d931](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4aff5d931) Merge branch 'editor2' into v2 (2023-02-20 11:02) <Daniel Caillibaud>  
`|\| `  
`| * `[8804a5c69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8804a5c69) fix pbs de type (2023-02-20 11:01) <Daniel Caillibaud>  
`| * `[311842bc8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/311842bc8) section basthonConsole désactivée en attendant de régler ses pbs ts (2023-02-20 11:00) <Daniel Caillibaud>  
`| * `[094cbd9c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/094cbd9c6) Wip: NodeContextMenu (il reste à faire la saisie de booléens multiples) (2023-02-20 10:51) <Jean-claude Lhote>  
`| * `[d4decb96b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4decb96b) querySelector(param) pour signaler erreur rectifié (2023-02-18 08:27) <Jean-claude Lhote>  
`* | `[66790a367](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/66790a367) Merge branch 'main' into v2 (2023-02-20 10:54) <Daniel Caillibaud>  
`* | `[f27015a9e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f27015a9e) Merge branch 'editor2' into v2 (2023-02-17 16:14) <Daniel Caillibaud>  
`|\| `  
`| * `[8a3f711b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8a3f711b9) link virés (2023-02-17 16:13) <Daniel Caillibaud>  
`| * `[516fe7bfb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/516fe7bfb) aj exclusions eslint (2023-02-17 16:13) <Daniel Caillibaud>  
`| * `[aec12e5bf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aec12e5bf) simplification édition des params du node (2023-02-17 16:12) <Daniel Caillibaud>  
`| * `[3e04f552e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e04f552e) eslint fixes (2023-02-17 16:11) <Daniel Caillibaud>  
`| * `[3b341f96d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b341f96d) WIP : Mise en place du validate au submit (problème de type à régler) (2023-02-17 13:05) <Jean-claude Lhote>  
`| * `[71a95f7dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/71a95f7dc) Retour des valeurs par défaut + initialisation des listes avec lesvaleurs déjà saisies. (2023-02-17 13:05) <Jean-claude Lhote>  
`| * `[fe0f4f1b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe0f4f1b4) fix j3pLoad pour charger les sectionsV1 (2023-02-17 12:56) <Daniel Caillibaud>  
`| * `[6af5b2e69](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6af5b2e69) manquait une fct ajoutée dans le js de main (2023-02-17 12:56) <Daniel Caillibaud>  
`| * `[403ac8a4d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/403ac8a4d) Mise au point du submit du formulaire NodeContextMenu (2023-02-17 11:14) <Jean-claude Lhote>  
`| * `[39297e5cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39297e5cd) Mise en place des cas multi avec liste fournie au submit du formulaire NodeContextMenu (2023-02-17 11:14) <Jean-claude Lhote>  
`| * `[cdb7a7c1e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cdb7a7c1e) aj fct addCheckBoxes (2023-02-17 11:14) <Daniel Caillibaud>  
`| * `[76a0f6339](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76a0f6339) Le test des exports de sections passe désormais pour toutes sauf 2 (2023-02-17 11:14) <Daniel Caillibaud>  
`| * `[35f5809c3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/35f5809c3) Aj des exports possibles onChangeParameter et validateParameter (2023-02-17 11:12) <Daniel Caillibaud>  
`| * `[f78d1ffc9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f78d1ffc9) rectif conversion des params pour array, fix des tests (2023-02-17 11:12) <Daniel Caillibaud>  
`| * `[5c2b9f821](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c2b9f821) modification du format des paramètres, ajout d'un check dessus pour détecter d'éventuelles incohérences (2023-02-17 11:12) <Daniel Caillibaud>  
`| * `[5971829b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5971829b7) màj deps (2023-02-17 11:12) <Daniel Caillibaud>  
`| * `[d3f70afb3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3f70afb3) aj alias sections (2023-02-17 11:12) <Daniel Caillibaud>  
`| * `[697ff417a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/697ff417a) Aj sectionExemple (v2) dans loadSection (2023-02-17 11:11) <Daniel Caillibaud>  
`| * `[3457dbf71](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3457dbf71) Mise en place de tests de validation (2023-02-17 11:11) <Jean-claude Lhote>  
`| * `[419aa48e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/419aa48e1) Mise en place de tests de validation (2023-02-17 11:11) <Jean-claude Lhote>  
`| * `[89a83a49d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89a83a49d) Saisie de listes dans les formulaires => Array (2023-02-17 11:11) <Jean-claude Lhote>  
`| * `[3636a42cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3636a42cc) Mise en place du NodeContextMenu, des tooltips ... (2023-02-17 11:11) <Jean-claude Lhote>  
`| * `[58a32def7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58a32def7) testRessource alert (à implémenter) + fix coquilles dans ConnectorEditor.ts (2023-02-17 11:11) <Jean-claude Lhote>  
`| * `[ce0ab3632](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce0ab3632) fix tests (2023-02-17 11:11) <Daniel Caillibaud>  
`| * `[75aa8a74f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75aa8a74f) DRAFT On commence à wrapper tous les objets legacy pour ne travailler que sur du format v2 (2023-02-17 11:10) <Daniel Caillibaud>  
`| * `[68126fd78](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68126fd78) Ajout nodeMapping (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[5687b0756](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5687b0756) On supprime le curviness variable avec le rang (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[353443b56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/353443b56) on augmente curviness avec le rang (pour limiter les chevauchements, mais ça change pas grand chose…) (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[09181a4de](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09181a4de) on décale les labels d'après le rang (pour limiter les chevauchements) (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[108306d16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/108306d16) Menu contextuel positionné (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[4cced761e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4cced761e) Renommer et supprimer node opérationnels (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[3488ff558](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3488ff558) targetIfMax viré et nbRuns ajouté sur les Connector (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[acb619b85](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/acb619b85) refactorisation : isolation des logiques entre SceneUI et SceneManager (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[a2a04863b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a2a04863b) type de condition en bouton radio et affichage conditionnel en conséquence (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[38f23ebf5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38f23ebf5) addPesToEditor devient plus clairement async (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[916e981ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/916e981ee) simplification (plus besoin de pesList, ref mise dans checkbox.value) (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[0fa283a1d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0fa283a1d) Aj Ex3 pour tester l'édition de pe (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[be8259c10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be8259c10) ajustements phrases d'état (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[d444465bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d444465bb) ajustements phrases d'état (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[67c8d326c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/67c8d326c) ajustements phrases d'état (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[b38e2b1fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b38e2b1fa) Aj de fetchPes (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[6238be794](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6238be794) interface en doublon virée (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[639eb47df](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/639eb47df) aj du titre sur les nodes (≠ label) (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[63a9a7305](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63a9a7305) Ajout des phrases d'états (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[39f0bb7ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39f0bb7ab) Ajout de boutons radio dans le ConnectorEditor (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[079f5a8a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/079f5a8a7) modification du visuel (bis) (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[0a903f315](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a903f315) modification du visuel (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[9d68eb85e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d68eb85e) Bug des listeners fixé : suppression opérationnelle... (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[7be6a7644](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7be6a7644) Divers ajouts... bug de la suppression de connector à régler. (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[9e7b8b3dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e7b8b3dd) amélioration css de l'éditeur (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[5e34600bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5e34600bc) aj de deux lien de test de l'éditeur (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[10c7c0514](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10c7c0514) Simplification du Mapping + Modification du rang possible jspByConnector associe : connectorId => connectionJsp connectorByJsp associe : jsp.id => connector (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[7457990ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7457990ed) Pb des connecteurs sans source fixé (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[0b3c57ce0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b3c57ce0) function showError ajouté dans main.ts (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[3fa0ba229](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3fa0ba229) listener mouseleave ajouté (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[36d281ae1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/36d281ae1) refadctorisation du nodeEditor (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[42891f7c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/42891f7c8) modifs mineures (2023-02-17 11:09) <Daniel Caillibaud>  
`| * `[9bdbbc304](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9bdbbc304) Ajout de commentaires pour relecture (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[769a70c3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/769a70c3f) menu contextuels sur les nodes (à finir) (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[c73049647](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c73049647) Noeuds déplaçables, endpoint source draggable pour créer les connexions (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[27a3915e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/27a3915e1) utilisation de la librairie uuid (avec la bonne fonction) (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[b72aec517](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b72aec517) utilisation de la librairie uuid (2023-02-17 11:09) <Jean-claude Lhote>  
`| * `[0045bcc00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0045bcc00) nettoyage : la mise à jour du mapping se fait dans le onStateChange() (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[eba1d2c56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eba1d2c56) l'ajout d'un connecteur à la souris fonctionne. Pb: l'intercept_before_drop n'empêche pas la connexion de se faire. (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[87138e79f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87138e79f) refonte des connector avec la propriété source (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[a5e59de21](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a5e59de21) restructuration du sceneManager. connexionMapping opérationnel (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[85042b50f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/85042b50f) drawConnexions() implémentée. maj connexionMapping (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[2d8ccbd00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d8ccbd00) on continue d'isoler les logiques (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[cecc1bf66](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cecc1bf66) refacto edit connector (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[d6b700de4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d6b700de4) jsdoc (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[eb199bb3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb199bb3d) modale, connexionMapping déménagent dans le SceneMAnager (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[60e28e2aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/60e28e2aa) construction modale (pas fini) (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[4ecc32a49](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ecc32a49) Les connexions JsPlumb utilisent enfin les paramètres par défaut de l'instance. (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[ce291324e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ce291324e) SceneUI.drawNode() renseigne l'inventaire connexionMapping (pas fini) (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[5c9eac3c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c9eac3c0) Ajout ConnectionMapping (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[aa9a1b67b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa9a1b67b) scene.ts devient une classe SceneManager.ts (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[d9408fe11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d9408fe11) Draft: ajout de l'inventory sur la scène. (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[3dff49d07](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3dff49d07) Ajout du ranking des connexions en cours (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[53e05be8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/53e05be8e) ajout de paintSyle et hoverPainStyle (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[a885d58b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a885d58b2) gestion des deferredConnectors (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[c6b964811](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c6b964811) ts est pénible… (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[f603a3036](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f603a3036) mise à jour des sections à charger d'après les évolutions de adresse.js (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[4b11d594e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4b11d594e) Ajout fct haveSameValues (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[9cabf33ee](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9cabf33ee) SceneUi.addNode() renommée en SceneUI.drawNode + suppression du cadre autour de la scène (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[b3b5642a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b3b5642a7) Ajustements (noms de variables) faits. (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[84bda460c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/84bda460c) label automatique pour les nodes dont l'id est modifié + offset relatif pour les graphes. (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[8e284bf44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e284bf44) fix import Modal (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[5b90ecbb7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5b90ecbb7) Modal déplacé dans utils/dom/ (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[e305efb22](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e305efb22) function showError ajouté dans main.ts (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[9e705e555](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e705e555) Modifications mineures. (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[9f02b039c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f02b039c) modif options par défaut des endpoint jsPlumb (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[d4a5e69ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4a5e69ac) drop d'un item dans la scène à la position de la souris... (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[7cf78f3be](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7cf78f3be) reducer.addNode modifié : modification des Id en cas de doublons (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[23a7e748b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/23a7e748b) Renommage getValue() en serialize() et ajout dans certaine classes (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[eb6bd6899](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb6bd6899) fix: suppression des console.log() concernant les @@redux.type... (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[0c822688e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c822688e) aj commentaires (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[cc21dbb58](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc21dbb58) Draft: ajout d'une classe SceneUI pour gérer l'interface, et la connecter à jsPlumb, ça réagit aux changement du state redux (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[9d193cfb9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d193cfb9) Ajout du store redux et de quelques actions, avec @reduxjs/toolkit (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[f0bdeaf07](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0bdeaf07) function showError ajouté dans main.ts (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[cf2333452](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf2333452) feat: premier jet du nouvel éditeur (travail en cours) (2023-02-17 11:08) <Jean-claude Lhote>  
`| * `[cf2be7c4c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf2be7c4c) Ajout exemple dans le jsdoc de Modal (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[41277a5a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/41277a5a4) Ajout Modal (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[4eeeb9c93](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4eeeb9c93) On branche le nouvel éditeur sur le j3pStart actuel, pour le tester en local (2023-02-17 11:08) <Daniel Caillibaud>  
`| * `[39520dbf2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39520dbf2) Draft: structure principale du nouvel éditeur de graphe (2023-02-17 11:08) <Daniel Caillibaud>  
`|/  `  
`* `[f7dd59731](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f7dd59731) Merge branch 'main' into v2 (2023-02-17 11:04) <Daniel Caillibaud>  
`* `[e85a89fe2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e85a89fe2) qq fixes (2023-02-14 16:37) <Daniel Caillibaud>  
`* `[9c165e729](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c165e729) fix pb de merge (2023-02-14 12:48) <Daniel Caillibaud>  
`* `[eea91c3b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eea91c3b7) Merge branch 'main' into v2 (2023-02-14 12:32) <Daniel Caillibaud>  
`* `[f65643b8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f65643b8d) fix tests (2023-02-14 12:28) <Daniel Caillibaud>  
`* `[59f2930b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/59f2930b0) doublon viré (2023-02-14 12:27) <Daniel Caillibaud>  
`* `[121ac4d95](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/121ac4d95) testBrowser ok en cjs (2023-02-13 19:13) <Daniel Caillibaud>  
`* `[d2c8906e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2c8906e4) j3pGetBaseUrl => j3pBaseUrl (2023-02-13 17:50) <Daniel Caillibaud>  
`*   `[503f02fd7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/503f02fd7) Merge branch 'moveExternals' into v2 (2023-01-19 18:52) <Daniel Caillibaud>  
`|\  `  
`| * `[e5f1f0b3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5f1f0b3f) src/legacy/outilsexternes => docroot/externals (2023-01-19 13:39) <Daniel Caillibaud>  
`* `[0a1511d52](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a1511d52) on vire les link (2023-01-19 13:40) <Daniel Caillibaud>  
`* `[65d75ec26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65d75ec26) l'entrée browser du package.json plante vite si on a pas fait de build, on laisse tomber pour le moment (2022-12-13 12:10) <Daniel Caillibaud>  
`* `[7604b47fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7604b47fa) update deps (2022-12-13 12:02) <Daniel Caillibaud>  
`* `[d96eab2b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d96eab2b8) qq fixes eslint (2022-12-02 18:13) <Daniel Caillibaud>  
`* `[228838c22](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/228838c22) màj fixture (2022-12-02 17:02) <Daniel Caillibaud>  
`* `[49875f426](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49875f426) aj infos sur test qui plante (2022-12-02 17:01) <Daniel Caillibaud>  
`* `[df42063f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df42063f9) màj deps (2022-12-02 16:54) <Daniel Caillibaud>  
`* `[a7a343331](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7a343331) Merge branch 'main' into v2 (2022-12-02 15:48) <Daniel Caillibaud>  
`* `[871e0d03b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/871e0d03b) aj des peer dependencies (2022-12-02 15:26) <Daniel Caillibaud>  
`* `[c4b57b6b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4b57b6b3) harmonisation (2022-11-25 18:40) <Daniel Caillibaud>  
`* `[20605ca0a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20605ca0a) Merge branch 'main' into v2 (2022-11-25 18:05) <Daniel Caillibaud>  
`* `[054d8451e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/054d8451e) Merge branch 'main' into v2 (2022-11-24 14:02) <Daniel Caillibaud>  
`* `[63f4cc0dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/63f4cc0dd) mise à jour des sections à charger d'après les évolutions de adresse.js (2022-11-24 13:54) <Daniel Caillibaud>  
`* `[898eb1387](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/898eb1387) fix tomblok images (2022-11-24 10:11) <Daniel Caillibaud>  
`* `[d0c1dffc1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0c1dffc1) fix pb de merge pour tomblok (2022-11-23 19:37) <Daniel Caillibaud>  
`* `[1817d425a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1817d425a) scratchDessin sert plus, on vire tout ce qui y fait référence (2022-11-23 19:29) <Daniel Caillibaud>  
`* `[6ceb4af05](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ceb4af05) finalisation du merge précédent (2022-11-23 19:24) <Daniel Caillibaud>  
`* `[0a6766d7c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0a6766d7c) Merge branch 'main' into v2 (2022-11-23 18:33) <Daniel Caillibaud>  
`*   `[d0dfb01b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0dfb01b1) Merge branch 'vite' into 'v2' (2022-11-23 18:19) <Daniel Caillibaud>  
`|\  `  
`| *   `[065a1fd8d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/065a1fd8d) Merge branch 'fixProdBuild' into 'vite' (2022-11-23 18:15) <Daniel Caillibaud>  
`| |\  `  
`| | * `[70aad6402](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/70aad6402) trucs obsolètes virés (2022-11-23 18:09) <Daniel Caillibaud>  
`| | * `[34c104d59](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34c104d59) aj import Blockly (2022-11-23 18:00) <Daniel Caillibaud>  
`| | * `[2cbb93fb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2cbb93fb8) /docroot/index.html ignoré (2022-11-23 18:00) <Daniel Caillibaud>  
`| | * `[9907b8f11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9907b8f11) on vire docroot/index.html de git (généré au build) (2022-11-23 17:59) <Daniel Caillibaud>  
`| | * `[7afa83ca2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7afa83ca2) aj du plugin vite splitVendorChunkPlugin, réduction du maxParallelFileOps à 10, ça build avec 10Go… (2022-11-23 17:59) <Daniel Caillibaud>  
`| | * `[fd765549b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fd765549b) legacy/outilsexternes déplacé dans docroot/externals, pour le sortir de src et tenter de résoudre les pbs de RAM du build, mais ça suffit pas (2022-11-23 16:32) <Daniel Caillibaud>  
`| | * `[d508c013d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d508c013d) aj de commentaires sur les paquets installés (2022-11-23 16:20) <Daniel Caillibaud>  
`| | * `[beb283f61](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/beb283f61) fix syntax error dans jquery.terminal.css (2022-11-21 15:34) <Daniel Caillibaud>  
`| | * `[a3b1e5216](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3b1e5216) aj de jquery.terminal.css dans notre code, l'original contenant des erreurs qui plaisent pas au build (2022-11-21 15:32) <Daniel Caillibaud>  
`| | * `[32d9992c5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32d9992c5) polyfills listés en dur (2022-11-21 15:28) <Daniel Caillibaud>  
`| | * `[d857ec9bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d857ec9bc) fix scripts/rmBuild.sh si build existe pas encore (2022-11-21 13:51) <Daniel Caillibaud>  
`| | * `[b10717bf7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b10717bf7) backup du build précédent avant d'en refaire un (2022-11-21 13:36) <Daniel Caillibaud>  
`| | * `[00247c2d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00247c2d5) 1er build de prod qui fonctionne (2022-11-21 13:15) <Daniel Caillibaud>  
`| | * `[6aa3c7730](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6aa3c7730) aj de NODE_OPTIONS pour le vite build qui manque de RAM (2022-11-16 09:25) <Daniel Caillibaud>  
`| |/  `  
`| *   `[871417600](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/871417600) Merge branch 'convert2to1' into 'vite' (2022-11-09 15:39) <Jean-Claude Lhote>  
`| |\  `  
`| | * `[39f633e43](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39f633e43) fix message de retour de test (2022-11-09 15:21) <Daniel Caillibaud>  
`| | * `[bbf1912a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bbf1912a0) rectif des extensions de fichiers ajoutées automatiquement avec les déplacements précédents (2022-11-08 19:50) <Daniel Caillibaud>  
`| | * `[5ae8e9bb3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ae8e9bb3) fichiers de test déplacés (2022-11-08 19:47) <Daniel Caillibaud>  
`| | * `[c0e1bd6b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c0e1bd6b2) aj de GraphV1.test (2022-11-08 19:47) <Daniel Caillibaud>  
`| | * `[1bf4e48a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1bf4e48a9) précision de type (2022-11-08 19:46) <Daniel Caillibaud>  
`| | * `[80cb2154e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/80cb2154e) checkGraphV1 utilise checkRessource (2022-11-08 19:45) <Daniel Caillibaud>  
`| | * `[51b31300f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51b31300f) aj de checkRessource (2022-11-08 19:43) <Daniel Caillibaud>  
`| | * `[4bc57112a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4bc57112a) convert déplacé dans lib/core (2022-11-08 17:51) <Daniel Caillibaud>  
`| | * `[1a8a5ffc0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a8a5ffc0) Enfin tous les tests qui passent (notamment la conversion v1=>v2=>v1 (2022-11-08 17:46) <Daniel Caillibaud>  
`| | * `[74c79e48c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/74c79e48c) test etoffé (2022-11-08 12:13) <Daniel Caillibaud>  
`| | * `[0c477d8ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c477d8ea) Lorsqu'il n'y avait pas de dernier connecteur sans condition sur un nœud, on en ajoutait un vers un nœud fin, ce n'est plus le cas (désormais on le signale sans le faire) (2022-11-08 11:25) <Daniel Caillibaud>  
`| | * `[799c899fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/799c899fa) qq améliorations mais le test passe toujours pas (2022-11-07 19:17) <Daniel Caillibaud>  
`| | * `[5ad3bebc3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ad3bebc3) aj du Graph1.validate pour valider un graphe v1 en vérifiant que les éventuelles pe existent (2022-11-07 19:01) <Daniel Caillibaud>  
`| | * `[b01901051](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b01901051) Aj du test de GraphV1 (2022-11-07 17:51) <Daniel Caillibaud>  
`| | * `[9db2cba28](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9db2cba28) aj d'un test pour comparators (2022-11-07 17:27) <Daniel Caillibaud>  
`| | * `[7fc21ae75](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7fc21ae75) fix: Il semble qu'ESlint ait retrouvé ses esprit. On enlève le String() lol (2022-11-07 10:47) <Jean-claude Lhote>  
`| | * `[e61f9d64f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e61f9d64f) fix: Eslint de convert1to2.ts (2022-11-07 09:10) <Jean-claude Lhote>  
`| | * `[45f7c5b2a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45f7c5b2a) on fait un vrai test, mais il ne passe pas encore… (2022-11-04 19:54) <Daniel Caillibaud>  
`| | * `[af9c5a01d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af9c5a01d) types regroupés, un peu de refacto (2022-11-04 18:20) <Daniel Caillibaud>  
`| | * `[c1d1dee02](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c1d1dee02) jsdoc (2022-11-04 15:11) <Daniel Caillibaud>  
`| | * `[212683fca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/212683fca) aj de l'option --limitBibli (2022-11-04 15:06) <Daniel Caillibaud>  
`| | * `[e589971d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e589971d1) écriture de la méthode GraphV2.isSame() et des fonctions sous-jacentes (2022-11-04 15:03) <Jean-claude Lhote>  
`| | * `[4026f68e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4026f68e9) renommage is… => are… (plus pertinent) (2022-11-04 14:34) <Daniel Caillibaud>  
`| | * `[b1d38444a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1d38444a) Aj des comparateurs (isSameArray, isSameObject, isSameValue) (2022-11-04 14:27) <Daniel Caillibaud>  
`| | * `[779ec8b62](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/779ec8b62) On va chercher sections.usage.json en ligne s'il est pas présent localement (ou trop vieux) (2022-11-04 14:00) <Daniel Caillibaud>  
`| | * `[75e77844d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75e77844d) Aj d'une durée de conservation en cache, + récap des erreurs à la fin (2022-11-04 13:26) <Daniel Caillibaud>  
`| | * `[dca4ffe7f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dca4ffe7f) aj des options --offset, --limit et --rids (2022-11-04 12:59) <Daniel Caillibaud>  
`| | * `[ee67a62e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee67a62e2) Ajout du script checkGraphV1 pour aller chercher les ressources j3p de la bibli et les passer au constructeur graphV1 (2022-11-04 12:38) <Daniel Caillibaud>  
`| | * `[7f683f6bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f683f6bb) aj GraphV1 (2022-11-03 20:03) <Daniel Caillibaud>  
`| | * `[ae4063ab0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae4063ab0) rectif de pbs liés au rebase vite (2022-11-02 14:13) <Daniel Caillibaud>  
`| | * `[cfda2ad88](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cfda2ad88) appel direct de pyodide remplacé par l'usage de Gui:eval(), qui retourne désormais des strings (2022-11-02 14:13) <Daniel Caillibaud>  
`| | * `[ca943cbf3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca943cbf3) getDiagMessage utilise une nouvelle méthode gui.eval (2022-11-02 14:13) <Daniel Caillibaud>  
`| | *   `[10f74a322](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10f74a322) Merge branch 'suiteBasthonVite' into 'vite' (2022-11-02 14:13) <Daniel Caillibaud>  
`| | |\  `  
`| | | * `[6f033fe86](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f033fe86) tsc arrête enfin de râler (2022-11-02 14:13) <Daniel Caillibaud>  
`| | | * `[610444a17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/610444a17) faut pas de noResolve à true, sinon ts ne retrouve pas tous ses petits (notamment avec vitest et basthon) (2022-11-02 13:28) <Daniel Caillibaud>  
`| | | * `[7dc6451a9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7dc6451a9) aj d'une option strict, tests de conversion améliorés (2022-11-02 12:05) <Daniel Caillibaud>  
`| | | * `[45c650693](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45c650693) réécriture du test convert2to1 en prenant une ressource dans les fixtures (2022-11-02 10:24) <Daniel Caillibaud>  
`| | | * `[848416585](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/848416585) fix test (qui passe pas car il reste des pbs de conversion sur l'exemple pris) (2022-11-02 10:24) <Daniel Caillibaud>  
`| | | * `[00739f0f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00739f0f5) Amélioration loadBibli (2022-11-02 10:24) <Daniel Caillibaud>  
`| | | * `[9e3867128](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e3867128) tests OK, mais pas certain que ça fasse ce qu'il faut... (2022-11-02 09:58) <Jean-claude Lhote>  
`| | | * `[6ad2fbabf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6ad2fbabf) fix: renommage bibli.js -> bibli.ts dans convert2To1.test.js (2022-11-02 08:58) <Jean-claude Lhote>  
`| | | * `[c4671e5a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c4671e5a0) Transfert loadBibli & relecture sémantique des variables V1/V2. tests non fonctionnels (2022-11-02 08:40) <Jean-claude Lhote>  
`| | | * `[7b0aa1c89](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b0aa1c89) fix: export manquants pour le test convert2to1 (2022-10-28 17:50) <Jean-claude Lhote>  
`| | | * `[98e31b0ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98e31b0ae) feat: convert2to1 avec convertConnectorBack, convertNodeBack, convertGraphBack (2022-10-28 17:03) <Jean-claude Lhote>  
`| * | |   `[c7ce9c6bd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7ce9c6bd) Merge branch 'suiteBasthonVite' into 'vite' (2022-11-02 13:56) <Daniel Caillibaud>  
`| |\ \ \  `  
`| | |/ /  `  
`| |/| |   `  
`| | * | `[64dd1e905](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64dd1e905) tsc arrête enfin de râler (2022-11-02 13:54) <Daniel Caillibaud>  
`| | * | `[7ce341dfe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7ce341dfe) faut pas de noResolve à true, sinon ts ne retrouve pas tous ses petits (notamment avec vitest et basthon) (2022-11-02 13:31) <Daniel Caillibaud>  
`| | |/  `  
`| | * `[46e55e6b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46e55e6b8) fix: Editor.ts import Ace résolu, mais pas KernelBase (2022-10-27 09:30) <Jean-claude Lhote>  
`| | * `[6cc634ecd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cc634ecd) fix chargement de basthon (2022-10-21 16:13) <Daniel Caillibaud>  
`| | * `[02b3f923a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02b3f923a) fix appels de resize qui plantent dans jquery.terminal (2022-10-21 16:10) <Daniel Caillibaud>  
`| | * `[bcf3da02c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bcf3da02c) ça avance, mais il a fallu mettre jquery.terminal dans nos sources pour qu'il prenne le bon jQuery (2022-10-21 13:07) <Daniel Caillibaud>  
`| | * `[5ea90342c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ea90342c) eslint fixes (2022-10-21 12:18) <Daniel Caillibaud>  
`| | * `[57b5eb223](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57b5eb223) On récupère les types de basthon dans l'outil, reste le pb de GUIBase… (2022-10-20 22:05) <Daniel Caillibaud>  
`| | * `[dea4bc56e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dea4bc56e) qq modifs mineures, mais TS trouve toujours pas @basthon/gui-base :-( (2022-10-20 21:36) <Daniel Caillibaud>  
`| | * `[39c4e6be2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/39c4e6be2) Merge branch 'main' into suiteBasthonVite (2022-10-20 21:29) <Daniel Caillibaud>  
`| | * `[50980bf7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/50980bf7d) fix import getJ3pConteneur (2022-10-20 19:29) <Daniel Caillibaud>  
`| | * `[ba4fb7afe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba4fb7afe) on récupère le patch de jquery.terminal présent dans baston-console (2022-10-20 18:03) <Daniel Caillibaud>  
`| | * `[00f261ad3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/00f261ad3) version ou l'analyse vire les affectation avant de les refaire avec ses valeur (plutôt que de virer des inputs) (2022-10-19 16:33) <Daniel Caillibaud>  
`| | * `[65384a0d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65384a0d2) aj jsdoc (2022-10-19 16:33) <Daniel Caillibaud>  
`| | * `[03b3031ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/03b3031ab) aj d'une méthode disable sur l'éditeur basthon (2022-10-19 16:33) <Daniel Caillibaud>  
`| | * `[1a1e2361f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1a1e2361f) fix gestion touche entrée (2022-10-19 16:33) <Daniel Caillibaud>  
`| | * `[274fa130c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/274fa130c) appel direct de pyodide remplacé par l'usage de Gui:eval(), qui retourne désormais des strings (2022-10-19 16:33) <Daniel Caillibaud>  
`| | * `[2439e8432](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2439e8432) aj option withoutBanner à Shell:clear() (2022-10-19 16:33) <Daniel Caillibaud>  
`| | * `[732f913ff](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/732f913ff) getDiagMessage utilise une nouvelle méthode gui.eval (2022-10-19 16:33) <Daniel Caillibaud>  
`| | * `[d3a7654e6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3a7654e6) on affiche le résultat du code testé dans la console (2022-10-19 16:33) <Daniel Caillibaud>  
`| | * `[68888309f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68888309f) première version finalisée à tester (avec usage direct de pyodide.runPython chargé par basthon) (2022-10-19 16:33) <Daniel Caillibaud>  
`| | * `[6b9e47da2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b9e47da2) simplification du code, verif ne modifie plus le dom mais retourne la chaîne à afficher (et c'est l'appelant qui gère son dom), elle devient getDiagMessage au passage (2022-10-19 16:33) <Daniel Caillibaud>  
`| | * `[eb5429bc4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb5429bc4) fix eslint (2022-10-19 16:33) <Daniel Caillibaud>  
`| | * `[e98b4de87](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e98b4de87) debut de la branche avanceeBasthon pour intégration fonction de vérification de code (2022-10-19 16:33) <Alexis Lecomte>  
`| |/  `  
`| * `[d13b5f2e7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d13b5f2e7) pas forcément de decription pour tous les params (2022-10-19 16:32) <Daniel Caillibaud>  
`| * `[76683cae0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/76683cae0) fix baseUrl (2022-10-19 16:32) <Daniel Caillibaud>  
`| * `[2bd4f8363](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2bd4f8363) on préfère planter si le port 8081 est déjà occupé (2022-10-19 16:30) <Daniel Caillibaud>  
`| * `[df90c038f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df90c038f) config vite en js (on gère pas de ts dans node) (2022-10-19 16:30) <Daniel Caillibaud>  
`| * `[65f8b8c8a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65f8b8c8a) eslint fixes (2022-10-19 15:31) <Daniel Caillibaud>  
`| * `[65a837148](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/65a837148) fix import getJ3pConteneur (qui a changé de place depuis main) (2022-10-19 15:17) <Daniel Caillibaud>  
`| * `[c785cb8e2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c785cb8e2) màj deps (et deps inutiles virées) (2022-10-19 15:15) <Daniel Caillibaud>  
`| * `[f553c3f16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f553c3f16) màj doc (2022-10-19 13:39) <Daniel Caillibaud>  
`| * `[755962dda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/755962dda) fix pb de types (2022-10-19 13:11) <Daniel Caillibaud>  
`| * `[bdd383933](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bdd383933) nom de fichier de doc en majuscules (2022-10-19 10:34) <Daniel Caillibaud>  
`| * `[3ae73cf23](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3ae73cf23) fix passage à ts (2022-10-19 10:30) <Daniel Caillibaud>  
`| * `[e4dd3b60b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e4dd3b60b) commentaires (2022-10-19 10:09) <Daniel Caillibaud>  
`| * `[109341452](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/109341452) fix ts (2022-10-19 10:05) <Daniel Caillibaud>  
`| * `[30cc54caf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30cc54caf) Merge branch 'main' into vite (2022-10-18 19:07) <Daniel Caillibaud>  
`| * `[c64dee9d3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c64dee9d3) Merge branch 'main' into vite (2022-10-07 17:45) <Daniel Caillibaud>  
`| * `[504d09b44](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/504d09b44) Merge branch 'main' into vite (2022-10-07 12:11) <Daniel Caillibaud>  
`| *   `[d6880f227](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d6880f227) Merge branch 'vitest' into 'vite' (2022-10-07 10:56) <Daniel Caillibaud>  
`| |\  `  
`| | * `[58c5600b9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58c5600b9) fix récupération de xml (avec fetch) (2022-10-06 12:47) <Daniel Caillibaud>  
`| | * `[72104cd34](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/72104cd34) suppression de j3pCreeCubique1, j3pCreeRectangleNoms et j3pAssign, qq bugfixes, nettoyage de code, jsdoc (2022-10-06 12:46) <Daniel Caillibaud>  
`| | * `[dc9a70564](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dc9a70564) fix 2 appels de j3pShowError (2022-10-06 08:06) <Daniel Caillibaud>  
`| | * `[a76fc1e6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a76fc1e6f) fix petit pb de syntaxe (2022-10-05 20:28) <Daniel Caillibaud>  
`| | * `[7e3eb9e0b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7e3eb9e0b) on exclu vendors et outilsexternes (2022-10-05 20:26) <Daniel Caillibaud>  
`| | * `[3694b21f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3694b21f0) fonctions inutilisées virées (j3pIntersectionDroiteSegment, j3pPointDansSegment, j3pEstparalleles, j3pEstperpendiculaire, j3pInclude, j3pGetRandomChar, j3pChargeJs, j3pEnt, j3pAffichematrice, j3pDesactiveMQ, j3pLatexToMq, j3pFraction, j3pSupprimeChaine, j3pSupprimeChar, j3pGetCurseur, j3pEgaliteFormelle, j3pEgaliteFormelleWebxcas, j3pAjouteVideo, j3pDonneesParcours, j3pPick, j3pTranslate) (2022-10-05 16:56) <Daniel Caillibaud>  
`| | * `[6c729a5b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c729a5b2) eslint fixes (2022-10-05 16:32) <Daniel Caillibaud>  
`| | * `[4abdd6e2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4abdd6e2b) eslint un peu moins lent (2022-10-05 16:32) <Daniel Caillibaud>  
`| | * `[e90370307](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e90370307) commentaires sur les paths d’alias (2022-10-05 11:26) <Daniel Caillibaud>  
`| | * `[b1ed87578](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1ed87578) fix eslint (2022-10-05 11:23) <Daniel Caillibaud>  
`| | * `[c8187e8eb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c8187e8eb) config eslint externalisée (2022-10-05 10:52) <Daniel Caillibaud>  
`| | * `[dcedfe03c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dcedfe03c) fix test qui passait plus (2022-10-05 09:37) <Daniel Caillibaud>  
`| | * `[37eace6a1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/37eace6a1) Un peu de nettoyage + vite.loader remis comme avant (plus proche de celui de main) (2022-10-04 17:28) <Daniel Caillibaud>  
`| | * `[5be293db5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5be293db5) on remplace mocha par vitest => pnpm test et pnpm start fonctionnent tous les deux (2022-10-04 17:00) <Daniel Caillibaud>  
`| | * `[a9a0e5d1e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9a0e5d1e) On vire les extensions .js des imports et vite fonctionne, mais plus mocha… (2022-10-04 16:13) <Daniel Caillibaud>  
`| | * `[0ce5e578a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ce5e578a) on simplifie le pb avec un loader vite minimaliste, c'est l'import de ts depuis du js qui casse le lancement via vite (2022-10-04 16:05) <Daniel Caillibaud>  
`| | * `[d5befe882](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d5befe882) simplification du loader vite, on se limite à j3pStart (2022-10-04 15:49) <Daniel Caillibaud>  
`| | * `[9831a033c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9831a033c) plus de ./ dans les paths de tsconfig (2022-10-04 15:49) <Daniel Caillibaud>  
`| | * `[c004ba389](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c004ba389) on vire vite-tsconfig-paths pour remettre les alias à la main dans la config de vite, il plante plus directement mais ne trouve pas les js importés (qui sont des ts) (2022-10-04 14:48) <Daniel Caillibaud>  
`| | * `[f93cdcd31](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f93cdcd31) Tentative avec vite-tsconfig-paths pour éviter de re-lister les alias dans vite => vite HS (2022-10-04 14:45) <Daniel Caillibaud>  
`| | * `[5c91f258f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c91f258f) ts-mocha et tsconfig-paths virés car pas utilisés (2022-10-04 14:39) <Daniel Caillibaud>  
`| | * `[a9fde743b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a9fde743b) màj deps (2022-10-04 14:02) <Daniel Caillibaud>  
`| | * `[32923881d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32923881d) plus besoin du plugin vite-plugin-dynamic-import (2022-10-04 13:50) <Daniel Caillibaud>  
`| | * `[91c6144b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/91c6144b2) une solution pour mocha… qui casse vite :-/ (2022-10-04 13:50) <Daniel Caillibaud>  
`| | *   `[b1271f496](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1271f496) Merge branch 'main' into viteMocha (2022-10-04 13:45) <Daniel Caillibaud>  
`| | |\  `  
`| | | * `[1c55d36f3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c55d36f3) aj option --rids pour loadAll (2022-10-03 15:04) <Daniel Caillibaud>  
`| | | * `[6faf33af1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6faf33af1) aj exclusion des logs (2022-10-03 12:27) <Daniel Caillibaud>  
`| | | * `[20560fd6e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20560fd6e) fix jsdoc (2022-10-03 12:17) <Daniel Caillibaud>  
`| | * `[16a294cd7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16a294cd7) Aj du test avec tsconfig-paths/register (2022-10-03 10:07) <Daniel Caillibaud>  
`| | * `[9c46b807a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c46b807a) Ajout d'un test/README.md pour récapituler toutes les tentatives (2022-09-30 17:21) <Daniel Caillibaud>  
`| | * `[ee7375a4e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee7375a4e) test avec un resolver maison, toujours HS (2022-09-30 17:21) <Daniel Caillibaud>  
`| | * `[0dc3f81c4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0dc3f81c4) tests en ajoutant tsconfig-paths dans initMocha.cjs => toujours HS (2022-09-30 17:03) <Daniel Caillibaud>  
`| | * `[eecddf6ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eecddf6ef) tests avec ts-node/register + ts-node/loader, HS (2022-09-30 17:01) <Daniel Caillibaud>  
`| | * `[406b8a46e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/406b8a46e) param inutilisé viré (2022-09-30 16:59) <Daniel Caillibaud>  
`| | * `[9f250c9b4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f250c9b4) tests d'imports (2022-09-30 14:56) <Jean-claude Lhote>  
`| |/  `  
`| * `[90490d057](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/90490d057) fix pnpm start (2022-09-28 11:01) <Daniel Caillibaud>  
`| * `[4645c8710](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4645c8710) vite sur le port 8081 (2022-09-28 11:00) <Daniel Caillibaud>  
`| * `[48b69015a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48b69015a) fix build (2022-09-23 23:29) <Daniel Caillibaud>  
`| * `[2bc662b59](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2bc662b59) on utilise les imports pour le js (2022-09-23 19:55) <Daniel Caillibaud>  
`| * `[1c9f4035c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c9f4035c) on utilise les imports pour le js (2022-09-23 19:54) <Daniel Caillibaud>  
`| * `[0ec95a3b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ec95a3b0) un peu de nettoyage pour que ça compile… (2022-09-23 19:52) <Daniel Caillibaud>  
`| * `[afc2d9fe8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/afc2d9fe8) faut remettre j3pImporteAnnexe pour les sections dont le dossier de sectionsAnnexes a été renommé (c'est j3pImporteAnnexe qui remplace les / par des -) (2022-09-23 17:31) <Daniel Caillibaud>  
`| * `[2f9a7b53b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f9a7b53b) j3pImporteAnnexe remplacé par un import standard pour les js (2022-09-23 17:10) <Daniel Caillibaud>  
`| * `[6621216a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6621216a0) factorisation fetchXml (2022-09-23 17:04) <Daniel Caillibaud>  
`| * `[b5b637708](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5b637708) j3pGetBaseUrl() remplacé par j3pBaseUrl, calculé dynamiquement (2022-09-23 16:20) <Daniel Caillibaud>  
`| * `[6c0162a30](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c0162a30) plus besoin de legacyImport (2022-09-23 16:17) <Daniel Caillibaud>  
`| * `[9130f0664](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9130f0664) fix récupération de xml (avec fetch) (2022-09-23 16:14) <Daniel Caillibaud>  
`| * `[c5e0fada4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c5e0fada4) aj de docroot en publicDir (2022-09-23 16:09) <Daniel Caillibaud>  
`| * `[60a58103d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/60a58103d) js exclus (2022-09-23 15:06) <Daniel Caillibaud>  
`| * `[435da659f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/435da659f) modifs mineures (2022-09-23 14:49) <Daniel Caillibaud>  
`| * `[1b2456872](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1b2456872) nettoyage (2022-09-23 14:49) <Daniel Caillibaud>  
`| * `[3c4fa06ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3c4fa06ac) on ne masque plus la croix sur la calculatrice (2022-09-23 14:22) <Daniel Caillibaud>  
`| * `[527132bbf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/527132bbf) on ne log plus le graphe et les options (2022-09-23 14:22) <Daniel Caillibaud>  
`| * `[0f3730b49](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0f3730b49) j3pLoad utilise loadJqueryDialog (2022-09-23 13:22) <Daniel Caillibaud>  
`| * `[d256f278c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d256f278c) Parcours charge dialog, sauf si tous les nœuds n'en veulent pas (2022-09-23 13:20) <Daniel Caillibaud>  
`| * `[7255702f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7255702f9) fix chargement de jquery-ui (2022-09-23 12:52) <Daniel Caillibaud>  
`| * `[7f1e89c37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7f1e89c37) WIP Un seul niveau de dossier dans sectionsAnnexes (pour les imports dynamiques de j3pImporteAnnexe) (2022-09-21 15:43) <Daniel Caillibaud>  
`| * `[243c1487b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/243c1487b) WIP premier passage à vite qui compile (reste pas mal de pbs, notamment jQuery.dialog HS, et les imports de fichiers annexe à tester) (2022-09-21 14:30) <Daniel Caillibaud>  
`| * `[092fc3f88](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/092fc3f88) bugfix init des styles (2022-09-21 11:10) <Daniel Caillibaud>  
`|/  `  
`*   `[ee727d5d5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ee727d5d5) Merge branch 'fixTsBuild' into 'v2' (2022-09-20 16:04) <Daniel Caillibaud>  
`|\  `  
`| * `[d7683d037](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d7683d037) fix plantage du build (on élimine @basthon/kernel-sql) (2022-09-20 14:07) <Daniel Caillibaud>  
`| * `[edd82939c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/edd82939c) fix pbs ts & eslint (2022-09-20 14:06) <Daniel Caillibaud>  
`| * `[6bd4cf9ed](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6bd4cf9ed) fix import load => loadJs (2022-09-20 13:58) <Daniel Caillibaud>  
`|/  `  
`*   `[68492377e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/68492377e) Merge branch 'libUtilsToTs' into 'v2' (2022-09-20 11:53) <Daniel Caillibaud>  
`|\  `  
`| * `[8e3f9d6a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e3f9d6a3) typage css.ts rectifié (2022-09-20 11:36) <Jean-claude Lhote>  
`| * `[a770d2d87](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a770d2d87) régler des problèmes au pnpm start + systemEquation/index.js->ts (2022-09-20 11:36) <Jean-claude Lhote>  
`* | `[cc5463cf3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc5463cf3) src/legacy/css/j3pcss/fonts/MouseMemoirs-Regular.ttf,src/legacy/css/j3pcss/fonts/icomoon.eot,src/legacy/css/j3pcss/fonts/icomoon.ttf,src/legacy/css/j3pcss/fonts/icomoon.woff,src/legacy/css/j3pcss/fonts/mousememoirs-regular-webfont.eot,src/legacy/css/j3pcss/fonts/mousememoirs-regular-webfont.ttf,src/legacy/css/j3pcss/fonts/mousememoirs-regular-webfont.woff,src/legacy/css/j3pcss/icomoon.eot,src/legacy/css/j3pcss/icomoon.ttf,src/legacy/css/j3pcss/icomoon.woff,src/legacy/outils/sokoban/assets/bravododie.mp3,src/legacy/outils/sokoban/assets/miaou.wav,src/legacy/outils/sokoban/assets/porte.wav,src/legacy/outils/xcas/doc/algo.pdf,src/legacy/outils/xcas/doc/bernoulli.pdf,src/legacy/outils/xcas/doc/graphProb.pdf,src/legacy/outils/xcas/doc/lisezMoi.pdf,src/legacy/outils/xcas/doc/spirale.pdf,src/legacy/outilsexternes/blockly/media/click.mp3,src/legacy/outilsexternes/blockly/media/click.ogg,src/legacy/outilsexternes/blockly/media/click.wav,src/legacy/outilsexternes/blockly/media/delete.mp3,src/legacy/outilsexternes/blockly/media/delete.ogg,src/legacy/outilsexternes/blockly/media/delete.wav,src/legacy/outilsexternes/blockly/media/disconnect.mp3,src/legacy/outilsexternes/blockly/media/disconnect.ogg: convert to Git LFS (2022-09-20 11:47) <Daniel Caillibaud>  
`* | `[0436b04f9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0436b04f9) Merge branch 'main' into v2 (2022-09-20 11:38) <Daniel Caillibaud>  
`* | `[455b0a63f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/455b0a63f) récup branche startingTs (2022-09-20 11:32) <Daniel Caillibaud>  
`|/  `  
`* `[9081ab75c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9081ab75c) fix typage destroy (2022-09-19 11:57) <Daniel Caillibaud>  
`* `[57874862e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/57874862e) pgcdMulti accepte [0, 0, 0, 5] sans l'option acceptOnlyZeros (2022-09-19 11:35) <Daniel Caillibaud>  
`* `[9c395e56c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c395e56c) fix pgcd avec option acceptOnlyZeros (2022-09-19 10:45) <Daniel Caillibaud>  
`* `[d8980a9b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d8980a9b0) lintage ? (2022-09-19 08:32) <Jean-claude Lhote>  
`* `[04d8a3135](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/04d8a3135) quelques modifications de types pour éviter les any... (2022-09-19 08:30) <Jean-claude Lhote>  
`* `[bc70a30dd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bc70a30dd) test de lintage (2022-09-16 11:55) <Jean-claude Lhote>  
`* `[466b33b9b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/466b33b9b) modification du test pour pgcdMulti en accord avec la nouvelle fonction (2022-09-16 11:34) <Jean-claude Lhote>  
`* `[ca9078a14](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca9078a14) lintage (2022-09-16 11:07) <Jean-claude Lhote>  
`* `[449c402ef](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/449c402ef) rectification Jsdoc de number.ts + pb ts dans string.ts résolu (2022-09-16 10:34) <Jean-claude Lhote>  
`* `[25c6cd0f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25c6cd0f5) NoUncheckedIndex = true et corrections associées (2022-09-16 10:32) <Jean-claude Lhote>  
`* `[0ded49146](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ded49146) fix remplacement de chemin foireux (2022-09-16 07:50) <Daniel Caillibaud>  
`* `[fedbcfa2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fedbcfa2b) modifs oubliées (2022-09-16 07:48) <Daniel Caillibaud>  
`* `[6e8597a99](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6e8597a99) Ajout des types inline dans css.ts (2022-09-15 20:24) <Jean-claude Lhote>  
`* `[cdeef481e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cdeef481e) fix pbs TS (2022-09-15 17:49) <Daniel Caillibaud>  
`* `[097753c50](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/097753c50) manquait un fichier dans le commit précédent (2022-09-15 17:49) <Daniel Caillibaud>  
`* `[e334e03cb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e334e03cb) refacto fcts de lib/utils/dom (générique) & lib/domBuilder (spécifique j3p), avec màj pour TS (2022-09-15 17:36) <Daniel Caillibaud>  
`* `[6940a6267](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6940a6267) fix pb ts (2022-09-15 15:18) <Daniel Caillibaud>  
`* `[2b3f75374](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2b3f75374) aj commentaire (2022-09-15 14:51) <Daniel Caillibaud>  
`* `[f96604e37](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f96604e37) fix pbs ts + getErrorMessage déplacé de debug vers string (2022-09-15 14:50) <Daniel Caillibaud>  
`* `[b5b07ad3f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5b07ad3f) récup des jsdoc supprimés, rectifs des options facultatives, fix des pbs ts (pas tous, il en reste) (2022-09-15 12:36) <Daniel Caillibaud>  
`* `[f2d1ba238](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f2d1ba238) récup des jsdoc supprimés, rectifs des options facultatives, fix des pbs ts (2022-09-15 11:49) <Daniel Caillibaud>  
`* `[c95304b91](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c95304b91) restauration de src/legacy/core/functions.js idem main (2022-09-15 11:22) <Daniel Caillibaud>  
`* `[1be4119f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1be4119f2) faut filer 4G de RAM à eslint sinon depuis le passage à ts ça plante (et bloque le hook pre-commit) (2022-09-15 11:21) <Daniel Caillibaud>  
`* `[28af0d1a7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/28af0d1a7) fix pbs ts (2022-09-15 11:20) <Daniel Caillibaud>  
`* `[45d1daf10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45d1daf10) Suppression extension .js dans les imports de Connector.ts modifié :         src/lib/entities/Connector.ts (2022-09-15 08:32) <Jean-claude Lhote>  
`* `[5784c02ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5784c02ca) Migration Ts de lib/utils Pb avec object.ts 	modifié :         src/legacy/core/functions.js 	renommé :         src/lib/utils/Queue.js -> src/lib/utils/Queue.ts 	renommé :         src/lib/utils/css.js -> src/lib/utils/css.ts 	renommé :         src/lib/utils/debug.js -> src/lib/utils/debug.ts 	renommé :         src/lib/utils/dom.js -> src/lib/utils/dom.ts 	renommé :         src/lib/utils/number.js -> src/lib/utils/number.ts 	renommé :         src/lib/utils/object.js -> src/lib/utils/object.ts 	supprimé :        src/lib/utils/regexp.js 	nouveau fichier : src/lib/utils/regexp.ts 	renommé :         src/lib/utils/string.js -> src/lib/utils/string.ts 	renommé :         src/lib/utils/validators.js -> src/lib/utils/validators.ts (2022-09-14 22:33) <Jean-claude Lhote>  
`* `[a8d265bbe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a8d265bbe) passage en ts de convert1to2 (2022-09-14 17:10) <Daniel Caillibaud>  
`* `[2d9e63d36](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d9e63d36) faut lui préciser la liste dans path… (2022-09-14 17:10) <Daniel Caillibaud>  
`* `[0449ae0cd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0449ae0cd) fix résolution de modules pour le js (ajouter un jsconfig sert à rien s'il y a déjà un tsconfig.json, il fallait juste ajouter les js dans le tsconfig) (2022-09-14 14:38) <Daniel Caillibaud>  
`* `[ba23eb2f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba23eb2f1) refacto aj auto du nœud fin, remise de Position pour contrôler number fini (2022-09-14 12:39) <Daniel Caillibaud>  
`* `[c190ff4f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c190ff4f5) modif de forme (console.error et throw remis sur une ligne) (2022-09-14 12:34) <Daniel Caillibaud>  
`* `[9a2ecc3b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9a2ecc3b2) aj doc (2022-09-14 09:42) <Daniel Caillibaud>  
`* `[49fcf5b6a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49fcf5b6a) J'ai du oublié d'indexer mes changements dans le précédent commit (2022-09-13 21:35) <Jean-claude Lhote>  
`* `[ebfa6d08c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ebfa6d08c) intégration des positions à (0,0) dans convert1to2 (2022-09-13 21:35) <Jean-claude Lhote>  
`* `[d56addcab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d56addcab) Déplacement des positions à l'intérieur des graphNodes (2022-09-13 21:35) <Jean-claude Lhote>  
`* `[809375d4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/809375d4b) màj deps (2022-09-13 14:02) <Daniel Caillibaud>  
`* `[1fbec1c3d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1fbec1c3d) fix documentation des fichiers ts (avec better-docs) (2022-09-13 13:45) <Daniel Caillibaud>  
`* `[ae2e4a5c8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae2e4a5c8) jsdoc (2022-09-13 13:41) <Daniel Caillibaud>  
`* `[58c937e10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58c937e10) fix jsdoc (2022-09-13 13:41) <Daniel Caillibaud>  
`* `[d2f1e21a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2f1e21a3) fix eslint errors (2022-09-13 13:03) <Daniel Caillibaud>  
`* `[1da12826d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1da12826d) aj de /.vscode/settings.json à .gitignore (2022-09-13 12:28) <Daniel Caillibaud>  
`* `[6cfad200b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6cfad200b) fix jsdoc (2022-09-13 12:26) <Daniel Caillibaud>  
`* `[26bc6983f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/26bc6983f) fix precommit sur ts (2022-09-13 09:55) <Daniel Caillibaud>  
`* `[be8c64d27](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be8c64d27) Aj doc & commentaires (2022-09-13 09:46) <Daniel Caillibaud>  
`* `[9d2bde10b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d2bde10b) on passe le checkJs à false dans tsconfig, sinon on est pas près de réussir un build (2022-09-11 15:35) <Daniel Caillibaud>  
`* `[f47c28f54](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f47c28f54) mocha enfin OK avec TS (après màj deps et modifs du test convert1to2) (2022-09-11 15:19) <Daniel Caillibaud>  
`* `[8753eb54f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8753eb54f) Merge branch 'main' into startingTs (2022-09-11 11:07) <Daniel Caillibaud>  
`* `[160c800f0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/160c800f0) src/legacy/sections/EssaisXavier/Cycle_3/Resoudre_un_probleme/Pourcentage.docx,src/legacy/sectionsAnnexes/sons/01.wav,src/legacy/sectionsAnnexes/sons/02.wav,src/legacy/sectionsAnnexes/sons/03.wav,src/legacy/sectionsAnnexes/sons/04.wav,src/legacy/sectionsAnnexes/sons/05.wav,src/legacy/sectionsAnnexes/sons/06.wav,src/legacy/sectionsAnnexes/sons/07.wav,src/legacy/sectionsAnnexes/sons/08.wav,src/legacy/sectionsAnnexes/sons/09.wav,src/legacy/sectionsAnnexes/sons/10.wav,src/legacy/sectionsAnnexes/sons/100.wav,src/legacy/sectionsAnnexes/sons/1000.wav,src/legacy/sectionsAnnexes/sons/1000000.wav,src/legacy/sectionsAnnexes/sons/1000000000.wav,src/legacy/sectionsAnnexes/sons/11.wav,src/legacy/sectionsAnnexes/sons/12.wav,src/legacy/sectionsAnnexes/sons/13.wav,src/legacy/sectionsAnnexes/sons/14.wav,src/legacy/sectionsAnnexes/sons/15.wav,src/legacy/sectionsAnnexes/sons/16.wav,src/legacy/sectionsAnnexes/sons/17.wav,src/legacy/sectionsAnnexes/sons/18.wav,src/legacy/sectionsAnnexes/sons/19.wav,src/legacy/sectionsAnnexes/sons/20.wav,src/legacy/sectionsAnnexes/sons/21.wav,src/legacy/sectionsAnnexes/sons/22.wav,src/legacy/sectionsAnnexes/sons/23.wav,src/legacy/sectionsAnnexes/sons/24.wav,src/legacy/sectionsAnnexes/sons/25.wav,src/legacy/sectionsAnnexes/sons/26.wav,src/legacy/sectionsAnnexes/sons/27.wav,src/legacy/sectionsAnnexes/sons/28.wav,src/legacy/sectionsAnnexes/sons/29.wav,src/legacy/sectionsAnnexes/sons/30.wav,src/legacy/sectionsAnnexes/sons/31.wav,src/legacy/sectionsAnnexes/sons/32.wav,src/legacy/sectionsAnnexes/sons/33.wav,src/legacy/sectionsAnnexes/sons/34.wav,src/legacy/sectionsAnnexes/sons/35.wav,src/legacy/sectionsAnnexes/sons/36.wav,src/legacy/sectionsAnnexes/sons/37.wav,src/legacy/sectionsAnnexes/sons/38.wav,src/legacy/sectionsAnnexes/sons/39.wav,src/legacy/sectionsAnnexes/sons/40.wav,src/legacy/sectionsAnnexes/sons/41.wav,src/legacy/sectionsAnnexes/sons/42.wav,src/legacy/sectionsAnnexes/sons/43.wav,src/legacy/sectionsAnnexes/sons/44.wav,src/legacy/sectionsAnnexes/sons/45.wav,src/legacy/sectionsAnnexes/sons/46.wav,src/legacy/sectionsAnnexes/sons/47.wav,src/legacy/sectionsAnnexes/sons/48.wav,src/legacy/sectionsAnnexes/sons/49.wav,src/legacy/sectionsAnnexes/sons/50.wav,src/legacy/sectionsAnnexes/sons/bravo.wav,src/legacy/sectionsAnnexes/sons/est_egal_a.wav,src/legacy/sectionsAnnexes/sons/moins.wav,src/legacy/sectionsAnnexes/sons/moins2.wav,src/legacy/sectionsAnnexes/sons/non_trompe.wav,src/legacy/sectionsAnnexes/sons/perdu.wav,src/legacy/sectionsAnnexes/sons/plus.wav: convert to Git LFS (2022-09-11 11:05) <Daniel Caillibaud>  
`* `[ba5375833](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba5375833) src/legacy/sectionsAnnexes/sons/plus2.wav: convert to Git LFS (2022-09-11 10:59) <Daniel Caillibaud>  
`* `[7cc76c915](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7cc76c915) graph.positions aussi (2022-09-09 21:05) <Daniel Caillibaud>  
`* `[754888dcc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/754888dcc) graph.nodes est désormais un objet Map (et plus un Object) (2022-09-09 21:04) <Daniel Caillibaud>  
`* `[34fe2c80d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/34fe2c80d) màj deps (2022-09-09 20:44) <Daniel Caillibaud>  
`* `[ba4410703](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba4410703) toutes les entities en ts (2022-09-09 20:44) <Daniel Caillibaud>  
`* `[fcd139116](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fcd139116) qq progrès coté mocha, mais c'est pas gagné (il veut pas d'import de ts et trouve pas si on importe js) (2022-09-08 21:00) <Daniel Caillibaud>  
`* `[3913bec92](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3913bec92) enfin une version où mocha se lance (mais les tests plantent parce que ts parse tout le js) (2022-09-08 21:00) <Daniel Caillibaud>  
`* `[02b86ed2d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/02b86ed2d) aj des extensions dans les imports (2022-09-08 21:00) <Daniel Caillibaud>  
`* `[89a2fb98e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89a2fb98e) eslint ok (2022-09-08 21:00) <Daniel Caillibaud>  
`* `[df974b4c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df974b4c0) On passe à eslint-config-standard-with-typescript, mais ça génère pas mal d'erreurs eslint qui restent à régler (2022-09-08 21:00) <Daniel Caillibaud>  
`* `[566fc3fc0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/566fc3fc0) fix des pbs de type sur Connector, ajout enum pour les comparateurs possibles (2022-09-08 21:00) <Daniel Caillibaud>  
`* `[48f5ef830](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48f5ef830) correction référence valeurs static (2022-09-08 21:00) <Jean-claude Lhote>  
`* `[01b2e9301](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01b2e9301) Début de typage Connector.ts (2022-09-08 21:00) <Jean-claude Lhote>  
