jsdoc
=====

Depuis l’ajout de typescript la génération de la doc devient plus compliquée…

Il y a visiblement un plugin fait pour `jsdoc-plugin-typescript` mais il ne fonctionne pas du tout (une tonne d’erreur, dont des trucs comme `Unexpected reserved word 'interface'.`…)

Avec [better-docs](https://github.com/SoftwareBrothers/better-docs) ça fonctionne un peu mieux, mais il veut pleins de packages react dans ses peer-dependencies (à cause de ses templates), on peut s'en passer si on utilise pas ses templates, mais chaque `pnpm i` se met à râler.
