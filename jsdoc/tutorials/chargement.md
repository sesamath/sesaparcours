Chargement
==========

Notice destinée aux développpeurs, pour comprendre le fonctionnement du chargement des différents modes.

Il y a plusieurs chargeurs j3p
- j3pLoad : le player v1, charge un graphe v1 pour l’afficher, utilisé par les sesathèques, mais aussi editGraphe (aperçu) et spStart
- editGraphe : éditeur de graphe v1, peut appeler j3pLoad quand on demande un test du graphe, en 2021 seul Labomep l’utilise (et spStart sur le site j3p pour tester le fonctionnement)
- showParcours : affiche un parcours, à partir d’un résultat renvoyé par le player, utilisé par Labomep ou mathenpoche quand on clique pour voir les détails d’un résultat.
- spEdit (editeur v2 de graphes v1|v2)
- spPlay (player v2 de graphes v1|v2)
- spView (afficheur de parcours v2, pour des résultats v1|v2)
- spValidate (validateur de ressource v2)
- spStart : il gère le formulaire de https://j3p.sesamath.net/, et suivant les choix faits appelle un des autres loaders. Il n’est pas utilisé par les sésathèques (donc pas par Labomep ni mathenpoche ni aucun autre site externe à j3p).

Build et polyfills
------------------

On réalise 2 builds :
- un pour les vieux navigateurs (tout le code source est transpilé en es5, avec ajout de tous les polyfills nécessaires)
- un pour les plus récents (<5ans) qui savent gérer les modules js (es modules aka esm, à base d’import/export, à ne pas confondre avec les modules commonJs aka cjs à base de require module.exports de nodeJs). Il y a quand même de la transpilation (pour que des navigateurs de 2018 puissent exécuter du code source qui utilise de l’es2020) et des polyfills

C'est preload.js qui détecte les capacités du navigateur et oriente vers l’un des deux builds, et ensuite core-js et regenerator-runtime/runtime ajoutent les polyfills nécessaires au runtime (pas au build).

Il faut donc 2 builds pour éviter de transpiler en es5 le code natif compris par 95% des navigateurs (voire 98%), et ensuite chaque loader dois inclure core-js. 

Mais core-js ne doit être inclus qu'une seule fois dans toute l’appli, donc vu que nos loaders s'appellent entre eux il faut un tronc commun en amont. 
=> on crée un globalLoader qui inclue core-js et chargera ensuite le loader voulu.

Et avant d’appeler ce globalLoader, il faut un preload pour orienter entre la version es5 ou la version module.

On a donc
- un preload minimaliste codé en es5, qui ne passe pas par babel et n’inclue pas de polyfill (sinon le test pour détecter les vieux navigateurs marchera plus), pour orienter es5 ou module
- un globalLoader, qui sera transpilé en 2 versions (es5 & module), l’unique point d’entrée qui incluera core-js
- les 4 loaders
