Utiliser git
============

Git est un gestionnaire de version, que l’on utilise pour archiver toutes les versions du code.

Les bases
---------

Chaque fois que l’on veut enregistrer un jalon dans l’état d’avancement du code, on fait un commit, il faut le considérer comme un instantané du dépôt à ce moment là.

Git ne stocke évidemment pas tout le dépôt dans chaque commit, seulement les différences avec son parent, c'est ce qui permet de se déplacer rapidement d’un endroit à l’autre de l'historique, de pouvoir créer facilement plein de branches, les fusionner, etc.

* https://learngitbranching.js.org/?locale=fr_FR est un tutoriel interactif permettant de bien comprendre les bases, et même des fonctionnalités assez avancées. Les niveaux sont répartis entre main (dépôt local uniquement) et remote (partage de code avec d’autres), ce n’est pas la peine de faire tous les niveaux de main pour passer à remote (introduction + montée en puissance suffisent largement).

* http://ndpsoftware.com/git-cheatsheet.html (cliquer sur fr pour avoir les explications en français) est un très bon aide-mémoire, et permet de bien visualiser les "zones" de git (différence entre le répertoire de travail, l’index et le dépôt local)

Plus de liens et d’explications sur https://wiki.sesamath.net/doku.php?id=public:dev:git:start
