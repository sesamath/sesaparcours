# Playground l’objet qui gère la zone d’affichage des sections V2 #

Playground est une classe javascript qui permet de créer l’objet permettant d’interagir avec le conteneur HTML qui va
contenir les différents éléments participant au déroulement d’une section V2.  
Une instance de Playground est créée dés le chargement d’un graphe, dans la suite de cette documentation, j'utiliserai '
playground' pour désigner cette instance.

1. *Les méthodes publiques de l’instance playground*
    1. Les méthodes utilisées par les sections pour l’affichage
        1. `await playground.displayWork(content, params, options)` (async)  
           C'est sans doute la méthode la plus utilisée, car elle permet de produire les affichages dans la zone de
           travail.  
           Qu'ils soient statiques, dynamiques ou interactifs, tout affichage devrait passer par cette fonction.  
           La fonction prend comme premier argument une chaine de caractères.  
           Le latex 'inline' y est encadré par un $ de chaque côté (deux pour le mode bloc).  
           Les emplacements des différents inputs sont matérialisés par %{XXX} où XXX est le nom de la variable associée
           dans le
           playground (nous reviendrons dessus dans une prochaine partie)
           La fonction prend un deuxième argument (uniquement si le premier contient des inputs) qui permet de
           paramétrer les
           variables introduites par le premier argument.
           Enfin, un troisième argument optionnel permet de passer des options à travers un objet voir IDisplayOptions.
            - Exemple d’affichage statique (avec latex in line) :
            ```js
            await playground.displayWork('Etudions la fonction $f$ définie pour tout $x\\in\\Reals$ :')
            ```  
            - Exemple d’affichage avec une zone d’interactivité pour récupérer une réponse :
            ```typescript
            await playground.displayWork('Quelle est la forme irréductible de $\\dfrac{18}{36}$ ? : %{fraction}',
            {
                fraction: {
                    type: 'input',
                    label: '',
                    inputProps: {
                    type: 'number'
                    }
                }
            })
            ```  
           Je détaillerai davantage les types d’input dans une autre partie de cette documentation.
        2. `await playground.displaySolution(content, params, options)` (async)    
           À l’instar de playground.displayWork(), playground.displaySolution gère l’affichage dans la zône 'solutions'.
           Elle sera
           donc appelée dans la fonction solution() de la section V2.    
           Les arguments sont les mêmes que pour playground.displayWork() mais il est à noter que l’on ne peut pas
           demander à playground.displaySolution() d’afficher des inputs (ce n’est pas fait pour ça).  
           La présence de tels éléments dans les arguments provoquera un message d’erreur.    
           Les éléments autorisés sont :
            - le texte (y compris avec du latex)
            - les figures MG32 (qui devraient être figées dans ce contexte de correction)
                - les tables (qui doivent être totalement remplies dans ce contexte de correction)
                  Un exemple d’utilisation de playground.solution() :
            ```typescript
            export async function solution ({ storage, playground, question }: SectionContext): Promise<void> {
                const exposants = storage.exposants as number[]
                const n = exposants[question - 1]
                const corrige = `$x^{${ n }}+x^{${ n }}=2x^{${ n }}$`
                await playground.displaySolution(`La correction de la question ${ question } pour l’exposant ${ n }`, {}, {})
                await playground.displaySolution(corrige, {}, {})
                }
            ```

    2. `await playground.getItemValue(name)` (async)   
       La fonction qui récupère la valeur d’un input.
    3. `await playground.setFocus(name)` (async)    
       Mettre le focus sur l’un des inputs du playground.

    4. `await playground.dialog(message, title, options)` (async)  
       Permet d’afficher une boite de dialogue.

    5. `await playground.setFeedbackItem(item, isOk, feedback, index)` (async)  
       Met un feedback sur un input (ou une partie d’input par exemple pour les cases à cocher).

2. Les méthodes utilisées par le Player ou une autre partie du moteur V2  
   *Ces méthodes n’ont pas vocation à être utilisées dans les sections.*
    1. `await playground.create()` (async)    
       Le constructeur étant privé, c'est par cette méthode que l’instance playground est créée.
    2. `await playground.displayBilan()` (async)  
       Fonction utilisée par le Player pour afficher le bilan d’un graphe.
    3. `playground.getContaier(zone)`  
       Fonction permettant de récupérer l’élément HTML (un div) contenant telle ou telle zône.  
       Attention : il est préférable de ne pas utiliser ce biais pour manipuler le DOM mais de passer par les méthodes
       display
       du playground.
    4. `await playground.isAnswered()` (async)  
       Pour que le Player vérifie que l’élève a répondu avant de passer le relais à la fonction check() de la section.
    5. `playground.reset(options)`    
       Pour réinitialiser complètement ou partiellement le playground. Tâche effectuée par le Player en fonction des
       options (persistance ou pas des éléments).
    6. `playground.setTitle(title)`  
       Afficher le titre de l’exo.
    7. `await playground.showFeedback()` (async)    
       Affichage de feedback par le Player.
    8. `await playground.showStatus()` (async)    
       Affiche le status dans la zone dédiée du playground (c'est fait dans la boucle principale du Player)
    9. `playground.freeze()` et `playground.unfreeze()`  
       Utilisée par le Player pour empêcher ou autoriser la saisie.
    10. `playground.showWaiting()` et `playground.hideWaiting()`  
        Utilisées par le Player pour mettre en place le spinner de chargement ou le cacher.
