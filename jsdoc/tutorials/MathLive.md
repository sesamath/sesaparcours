### Utilisation de Mathlive dans les sections V2 ###

## Introduction ##

Mathlive est une alternative à MathQuill qui n’utilise plus JQuery et qui connaît depuis quelques années une forte
progression en terme d’installations. Son développement est très actif/réactif contrairement à celui de MathQuill qui
est pratiquement à l’arrêt.
Mathlive propose un moyen simple pour intégrer des formules mathématiques dans une page Web. Tout comme Mathquill, il
permet aussi d’éditer ces formules et de saisir du contenu mathématique qui sera traîté par la section.
Mathlive propose bien sûr une gestion des claviers virtuels configurable à souhait et responsive.

## Latex pour des formules statiques uniquement ##

Pour intégrer des formules latex dans un énoncé ou une correction, il suffit de l’écrire dans le texte de l’énoncé,
simplement en l’encadrant par des délimiteurs :

- '$' et '$' pour du latex en ligne (notez que le caractère \ est doublé car c'est le caractère d’échappement en js)
- '$$' et '$$]' pour du latex en bloc.  
  (ce latex sera alors converti en MathfieldElement par la fonction renderMathInDocument() de Mathlive)  
  par exemple : 'Quelle est la valeur décimale de $\\frac{1}{4}$ ?' produira la question avec la fraction 'un quart'
  correctement rendue.

## Latex éditable ##

Pour insérer une formule éditable ou simplement un champ de saisie mathématique, nous allons créer un input de type '
latex' avec la même syntaxe que pour les autres inputs (voir la documentation sectionsV2.md)  
Cela ajoute dans le DOM un élément de type MathfieldElement readOnly (c'est l’élément Mathlive qui contient le latex) au
sein duquel, nous allons avoir des zones éditables (les 'placeHolders')  
Ainsi, tout ce qui n’est pas dans des placeHolders n’est pas éditable, et tout ce qui est dans des placeHolders,
l’est.  
Le clavier est le même pour tous les placeHolders (voir la section claviers virtuels pour adapter les claviers au
besoins de la formule).

Un exemple :

```
await playground.displayWork('Compléter l\'intégrale suivante afin qu\'elle soit juste : %{integrale}', {
        integrale: {
          type: 'mathlive',
          value: '\\int_%{borneinf}^%{bornesup}2x = 1',
          commands: ['pi'],
          restrictions: /[^a-zA-Z]/
        }
      }, {})
```

Ici, 'integrale' est un nom donné à l’élément MathfieldElement, les items dont on pourra récupérer les valeurs (avec
un `await playground.getItemValue('borneinf')) sont borneinf et bornesup (ils correspondent aux placeHolders intégrés
dans le MathfieldElement).
On ajoute la commande 'pi' au clavier et on filtre les lettres minuscules et majuscules

## claviers virtuels ##

Les claviers virtuels proposés par cortex-js ont été remplacés par une solution 'maison' codée par Yves Biton.
Ils sont mis en place sur chaque mathfield ou input inséré dans l’énoncé d’une section en utilisant la méthode adhoc (
voir la documentation sectionsV2.md). Leur 'customisation' fera l’objet d’une documentation à part (voir
MlVirtualKeyboard.md)
