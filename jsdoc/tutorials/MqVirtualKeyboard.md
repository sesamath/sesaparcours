# Pour rendre le clavier virtuel disponible dans une section

Le clavier virtuel {@link MqVirtualKeyboard} ne peut être associé qu'à des champs d’édition Mathquill, pas à des champs d’édition de texte.

Pour adapter une section au clavier virtuel, il faut :

* Remplacer les appels à {@link module:j3pFunctions.j3pRestriction j3pRestriction} par des appels à {@link module:mqFunctions.mqRestriction mqRestriction}
* Ajouter une option boundingContainer pour lui indiquer dans quel conteneur il doit rester (il se décale alors automatiquement s'il est trop près du bord, pour éviter du scroll)
* Ajouter éventuellement une option commandes pour une liste de boutons "fonctionnels" (racine, exposant, etc.)
* Ne pas donner directement le focus à l’éditeur MathQuill associé (via par exemple $('#mqedit').focus() ou $('#mqedit').mathquill('focus')) mais uniquement via j3pFocus (donc par exemple un j3pFocus('mqedit'))

## syntaxe de mqRestriction avec un exemple

      mqRestriction('prefixeinputmq1', charList, {
        commandes: sq.listeBoutons,
        boundingContainer: parcours.zonesElts.MG
      })

Ici `prefixeinputmq1` est l’id du champ Mathquill associé mais on peut aussi passer comme paramètre directement l’élément (et pas son id)

`charList` est une regexp ou une chaîne de caractères donnant la liste des caractères autorisés (comme dans j3prestriction)

Attention : le - sera considéré comme le caractère de soustraction, pas comme un intervalle (comme dans [0-9]), utilisez la syntaxe regexp pour spécifier exactement vos intervalles
Vous pouvez utiliser `\\d` pour représenter les chiffres (équivalent à `0123456789`)
    
    Par exemple `charList = '\\d+-*/^'` est équivalent à `0123456789+-*/^`

Le troisième paramètre est un objet facultatif, mais il vaut mieux renseigner au moins boundingContainer pour que le clavier se positionne automatiquement dedans (sans scroll si possible)
* `commandes` est un tableau de string contenant les boutons autorisés (cf la constante exportée `mqCommandes` de `src/lib/mathquill/functions.js`), par exemple `commandes : ['racine', 'frac']`
* `boundingContainer` est un parent dans lequel il faut essayer de rester (pour que le clavier ne déborde pas)
