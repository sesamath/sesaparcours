Chaînage des nœuds
==================

Une section peut récupérer des données d’une section précédente, par ex la ressource
https://bibliotheque.sesamath.net/public/voir/91400, où il y a d’abord la
nature de la suite, puis le terme général et enfin la somme des premiers
termes.

Peu de sections permettent ce chaînage :
* celles sur les études de fonctions ;
* plusieurs sur les suites ;
* celles sur la loi exponentielle

L’objet permettant cet échange est `me.donneesPersistantes`, chaque section peut lire la propriété qui l’intéresse (donc provenant d’une section précédente) ou écrire dedans (pour l’initialiser ou écraser ce qui aurait été mis par une section précédente, à destination d’une section suivante).

On a donc un couplage entre les sections, avec collision possible sur les noms des propriétés.

En février 2022 on a les propriétés 
* `suites` pour les sections sur les suites qui l’utilisent (on récupère dans l’objet me.donneesPersistantes.suites les éléments définissant la suite à réutiliser dans la section suivante). 
* `etudeFonctions` pour les sections d’étude de fonctions (récupérer la fonction initiale pour ensuite du calcul de dérivée ou tableau de signe)
* `loiExpo` pour les sections traitant d’une loi exponentielle de sorte de récupérer la même v.a. d’une section à l’autre

Dans chacun des cas, un paramètre booléen `donneesPrecedentes` permet de demander à une section d’un graphe de récupérer les données du nœud précédent en allant chercher dans `me.donneesPersistantes`.

Pour etudeFonctions, les anciens graphes qui utilisaient `imposer_fct` à la fois pour imposer une fonction dans un modèle ou pour récupérer les données du nœud précédent fonctionnent toujours.
