Quelques remarques et découvertes à propos de JsPlumb

JsPlumb tient à jour une liste des éléments qu'il gère.
Pour faire partie des éléments gérés, il suffit d’ajouter un endPoint à l’élément grâce à la méthode :
instance.addEndPoint(element, {endpoint: type})
Dés lors, l’élément dispose des listeners jsPlumb permettant selon le paramétrage de l’instance de déplacer l’élément avec ses connexions, d’ajouter des connexions par drag & drop...
Une autre façon d’ajouter un élément à la liste des éléments gérés est de s'en servir pour créer une connexion à l’aide de la méthode :
instance.connect({source: element1,target: element2})
dés lors, les deux éléments sont gérés.

Un élément géré par JsPlumb récupère dans son dataset un attribut jtkManaged qui est une id unique... ça peut être intéressant pour l’identifier lorsqu'un événement comme EVENT_MANAGE_ELEMENT est déclenché.

à propos des événements que nous utilisons, il y a :

INTERCEPT_BEFORE_DROP : déclenché lorsqu'on drop une connexion sur un nœud. on l’utilise pour créer les nouveaux connecteurs à la souris.
EVENT_DRAG_STOP : déclenché lorsqu'un élément managé est déplacé et que le déplacement cesse. Je l’utilise pour mettre à jour la position du nœud est la grille de positionnement.
