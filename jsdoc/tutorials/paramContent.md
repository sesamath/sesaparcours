<!-- 
Faut pas mettre ici de titre de niveau 1 (souligné avec ====)
 car le template docdash ajoute le sien au-dessus
=> on passe par un fichier json pour préciser le titre voulu  
-->

Dans les `string` passées à plusieurs fonctions (dont J3PAffiche), vous pouvez utiliser des caractères spéciaux :
* `£` : pour une variable, `£x` (ou `£{truc}` si le nom de la variable a plus d’une lettre)
* `$` : pour une chaîne LaTeX, par exemple `$\sqrt{2}$` pour une racine de 2, ou `$\frac{1}{2}$` pour ½ ou `$e^x$` ou … (Attention, pour une chaîne dans du code js il faut doubler le \ pour signifier qu'on veut un caractère ` \ `, par ex `var str = '$\\sqrt{$2}$'`)
* `@n@` : input n° n (n > 9 possible ?)
* `&n&` : input mathquill n° n (n > 9 possible ?)

Suivant ce que l’on utilise, il faut préciser des paramètres

Substitution simple `£`
-----------------------
 Avec un `var content = 'La réponse est £x, £{foo}'` il faudra passer un 
```js
var params = {
  x: 42,
  foo: 'mais quelle était la question ?'
}
```  
pour que le rendu devienne `La réponse est 42, mais quelle était la question ?`

Rendu Latex
-----------

Passer tout ce qui doit être rendu en LaTeX entre `$`

Si vous voulez afficher un symbole `$`, il faudra utiliser une fonction qui n’interprète pas le contenu (par ex `J3PAddText`)

Input simple
------------

`J3PAffiche(container, '', 'Entrez un nombre @1@')` va afficher une zone de saisie standard (que l’on récupère avec ???)

Input Mathquill
---------------

`J3PAffiche(container, '', 'Entrez un nombre &1&')` va afficher une zone de saisie  mathquill (sans boutons)…

Pour afficher une zone avec des boutons il faut utiliser :

```js
// on affiche une zone et la palette de boutons 

```
