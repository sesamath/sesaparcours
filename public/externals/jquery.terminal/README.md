jquery.terminal
===============

Utilisé par le Shell de Basthon.

Comme toutes les extensions jquery, c'est particulièrement pénible à utiliser en esm, surtout avec pnpm/vite (plus stricts que npm/webpack).

js
--

Avant 2023-03, on avait trouvé un moyen de contournement en copiant une version légèrement modifiée dans le dossier basthon pour pouvoir l'importer sans avoir de `.terminal is not a function`. Mais ça ne fonctionnait qu'avec `pnpm start`, pas avec `pnpm run build` :-/

On a donc finalement résolu le pb en copiant `node_modules/jquery.terminal/js/jquery.terminal.min.js` dans `docroot/externals/jquery.terminal/jquery.terminal.min.js`

Il reste cependant à appliquer le patch basthon à ce fichier minifié (le paquet `patch-package` applique `patches/jquery.terminal+2.33.3.patch` à `node_modules/jquery.terminal/js/jquery.terminal.js` mais pas `node_modules/jquery.terminal/js/jquery.terminal.min.js`).
On vire donc patch-package de nos dépendances, et on va voir le patch d'origine sur https://framagit.org/basthon/basthon-console/-/blob/master/patches/jquery.terminal%2B2.33.3.patch

Au 09/03/2923 il s'agit de remplacer

`var format_exec_split_re=/(\[\[(?:-?[@!gbiuso])*;[^\]]+\](?:\\[[\]]|[^\]])*\]|\[\[[\s\S]+?\]\])/;var format_exec_re=/(\[\[[\s\S]+?\]\])/;` 

par 

`var format_exec_split_re=/(\[\[(?:[^\][]|\\\])+\]\])/;var format_exec_re=/(\[\[(?:[^\][]|\\\])+\]\])/;`

css
---

Concernant la css, il y a une erreur de syntaxe dans le css original, un `let` à la place de `left`, on copie donc `node_modules/jquery.terminal/css/jquery.terminal.css` dans `src/lib/outils/basthon/jquery.terminal.css` que l'on importe normalement dans Shell.ts (pour que ce soit minifié avec le reste)
