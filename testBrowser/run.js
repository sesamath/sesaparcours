import { basename } from 'node:path'

import prefs from './prefs'
import { getDefaultPage } from './helpers/browser'
import { logError } from './helpers/log'
import { fileURLToPath } from 'node:url'
import { afterEach, beforeEach, describe, it } from 'vitest'

/**
 * Ajoute un describe() contenant un it() par browser à tester
 * @param {function} test La fonction à exécuter dans chaque it()
 * @param meta il faut passer import.meta depuis le fichier appelant pour savoir lequel c’est (et l’indiquer dans le log)
 */
export function runTest (test, meta) {
  if (test.length < 1) throw Error('la fonction de test doit avoir un argument (un objet Page)')
  const filename = fileURLToPath(meta.url)
  // le chemin relatif à testBrowser/sections ou testBrowser/scenarios
  const path = filename.replace(/.*testBrowser\/(sections|scenarios)\//, '')
  if (!filename || path === filename) {
    throw Error('Il faut passer un fichier de testBrowser/(sections|scenarios)/ et son import.meta')
  }
  const isSection = filename.includes('testBrowser/sections')
  const label = (isSection ? 'section ' : 'scenario ') + basename(path, '.js').replace('.test', '')
  describe(label, async () => {
    let page, result
    const { continueOnError } = prefs
    let stop = false

    beforeEach(({ skip, task }) => {
      if (stop) skip()
    })

    // cf https://vitest.dev/guide/test-context.html pour l’argument passé
    afterEach(async () => {
      if (continueOnError) return
      // on s’arrête sur le 1er test qui plante pour creuser ça
      if (!result && page && !prefs.headless) {
        await page.pause()
        stop = true
      }
    })

    for (const browserName of prefs.browsers) {
      it(`fonctionne correctement avec ${browserName}`, async ({ skip, task }) => {
        if (stop) return skip()
        try {
          result = null
          page = await getDefaultPage({ browserName })
          const promise = test(page)
          if (!(promise instanceof Promise)) throw Error(`${filename} doit exporter une fonction test qui prend une page et retourne une promesse`)
          result = await promise
        } catch (error) {
          result = false
          // faut attendre que l’écriture se termine (sinon on se retrouve en pause avant
          // d’avoir le message d’erreur et on sait pas pourquoi ça a planté)
          await logError(error)
          throw error
        }
        if (!result) {
          const msg = `test ${label} KO avec ${browserName}`
          await logError(msg)
          throw Error(msg)
        }
      }) // it
    } // boucle browsers
  })
}
