import { clickSectionSuivante, clickSuite } from 'testBrowser/helpers/actions'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { getAngleDegFromVect } from 'testBrowser/helpers/math'
import ScratchHandler from 'testBrowser/helpers/ScratchHandler'
import { getMiddleOfLine } from 'testBrowser/helpers/svg'

/**
 * retourne la construction à réaliser pour dessiner la <line>
 * @param {x: number, y: number} debutLigne le point correspondant à (x1,y1) de la <line> concernée
 * @param {x: number, y: number} finLigne le point correspondant à (x2,y2) de la <line> concernée
 * @param {position: {x: number, y: number},locator: Locator, nom: string } Points les points de la figure dont on va utiliser les coordonnées
 * @returns {{type: string, point1: null, point2: null}} type = 'Droite', 'Segment' ou 'Demi-droite', point1 = l’origine de la demi-droite, ou une extremité du segment, ou un des points de la droite, point2 = l’autre point.
 */
function getTraceFromPoints (debutLigne, finLigne, Points) {
  const construction = { type: '', point1: null, point2: null }
  const extremites = []
  const marge = 5
  let extremite = ''
  for (const point of Points) {
    // Si point coïncide avec le début de la ligne, alors c’est soit une demi-droite soit un segment
    // dans le premier cas, il n’y aura pas d’autres extrémités donc extremites[0] uniquement et extremite = 'début' ou extremite='fin'
    // dans le deuxième cas, il y aura une autre extremité extremites[1] qui coïncidera avec la fin de la ligne donc extremite = 'début'+'fin' ou 'fin'+'début' mais peu importe, c’est un segment.

    if (Math.abs(point.position.x - debutLigne.x) < marge && Math.abs(point.position.y - debutLigne.y) < marge) {
      extremites.push(point)
      extremite += 'début'
    }
    if (Math.abs(point.position.x - finLigne.x) < marge && Math.abs(point.position.y - finLigne.y) < marge) {
      extremites.push(point)
      extremite += 'fin'
    }
  }
  if (extremites.length === 2) {
    construction.type = 'Segment'
  } else if (extremites.length === 1) {
    construction.type = 'Demi-droite'
    for (const autrePoint of Points) {
      if (autrePoint.nom !== extremites[0].nom) {
        const v1 = [autrePoint.position.x - extremites[0].position.x, autrePoint.position.y - extremites[0].position.y]
        const v2 = [(extremite === 'début' ? 1 : -1) * finLigne.x - debutLigne.x, (extremite === 'début' ? 1 : -1) * finLigne.y - debutLigne.y]
        if (Math.abs(getAngleDegFromVect(v1, v2)) < 1) {
          extremites[1] = autrePoint
          break
        }
      }
    }
  } else {
    construction.type = 'Droite'
    for (const autrePoint of Points) {
      // autrePoint est-il entre finLigne et debutLigne ?
      const v1 = [autrePoint.position.x - debutLigne.x, autrePoint.position.y - debutLigne.y]
      const v2 = [finLigne.x - autrePoint.position.x, finLigne.y - autrePoint.position.y]
      if (Math.abs(getAngleDegFromVect(v1, v2)) < 1) {
        extremites.push(autrePoint)
      }
    }
  }
  construction.point1 = extremites[0]
  construction.point2 = extremites[1]
  return construction
}

/**
 *
 * @param page
 * @param {ScratchHandler} editor
 * @param {boolean} rep Si on veut avoir juste, on met true, cela modifiera la conclusion pour que la correction fonctionne
 * @param {object[]} constructions Les constructions définies par la fonction getTraceFromPoint()
 * @returns {Promise<*>}
 */
async function answer (page, editor, rep, constructions) {
  if (!rep) {
    await logIfVerbose('Je répond faux')
    let blokLocator
    for (const construction of constructions) {
      let category
      switch (construction.type) {
        case 'Droite':
          await logIfVerbose('On veut une droite, je fais un segment !')
          blokLocator = await editor.getBlockFrom({ container: page, dataId: 'segmentAB' })
          await editor.clickAndType(blokLocator, 'SEGMENTS', `${construction.point1.nom}${construction.point2.nom}`, 0)
          category = 'SEGMENTS'
          break
        case 'Demi-droite':
          await logIfVerbose('On veut une demi-droite, je fais une droite !')
          blokLocator = await editor.getBlockFrom({ container: page, dataId: 'droitept' })
          await editor.clickAndType(blokLocator, 'DROITES', `${construction.point1.nom}${construction.point2.nom}`, 0)
          category = 'DROITES'
          break
        case 'Segment':
          await logIfVerbose('On veut un segment, je fais une demi-droite !')
          blokLocator = await editor.getBlockFrom({ container: page, dataId: 'demidroitept' })
          await editor.clickAndType(blokLocator, 'DEMI-DROITES', `${construction.point1.nom}${construction.point2.nom}`, 0)
          category = 'DEMI-DROITES'
          break
      }
      logIfVerbose(`catégorie visée : ${category}, block cherché : ${blokLocator}`)
      await editor.dragFromTo({
        category,
        source: blokLocator,
        target: editor.injectionDiv,
        targetPosition: editor.nextPosition
      })
      logIfVerbose(`Prochaine position : x = ${editor.nextPosition.x} et y = ${editor.nextPosition.y}`)
    }
  } else {
    logIfVerbose('Je répond sans erreur')
    let blokLocator
    for (const construction of constructions) {
      switch (construction.type) {
        case 'Segment':
          await logIfVerbose('On veut un segment, je fais un segment !')
          blokLocator = await editor.getBlockFrom({ container: page, dataId: 'segmentAB' })
          await editor.clickAndType(blokLocator, 'SEGMENTS', `${construction.point1.nom}${construction.point2.nom}`, 0)
          break
        case 'Droite':
          await logIfVerbose('On veut une droite, je fais une droite !')
          blokLocator = await editor.getBlockFrom({ container: page, dataId: 'droitept' })
          await editor.clickAndType(blokLocator, 'DROITES', `${construction.point1.nom}${construction.point2.nom}`, 0)
          break
        case 'Demi-droite':
          await logIfVerbose('On veut une demi-droite, je fais une demi-droite !')
          blokLocator = await editor.getBlockFrom({ container: page, dataId: 'demidroitept' })
          await editor.clickAndType(blokLocator, 'DEMI-DROITES', `${construction.point1.nom}${construction.point2.nom}`, 0)
          break
      }
      const category = construction.type.toUpperCase() + 'S'
      logIfVerbose(`catégorie visée : ${category}, block cherché : ${blokLocator}`)
      await editor.dragFromTo({
        category,
        source: blokLocator,
        target: editor.injectionDiv,
        targetPosition: editor.nextPosition
      })
      logIfVerbose(`Prochaine position : x = ${editor.nextPosition.x} et y = ${editor.nextPosition.y}`)
    }
  }
  await editor.resetDropZone()
  // on lance le programme
  await editor.drapeau.click()
  await page.waitForSelector('.boutonFermerMonProgramme:visible', { timeout: 10_004 })
  await page.locator('.boutonFermerMonProgramme').click({ force: true })
  await page.waitForSelector('.ScratchFeedback', { timeout: 10_005 })
  return rep
}

/**
 * Joue un essai pour une répétition (si il y a des blocks présents, drop tous les blocs, puis code un programme avant de cliquer sur le drapeau vert
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {ScratchHandler} editor Le handler de la 'zone' Scratch
 * @param {boolean} rep si true on fournit la bonne réponse
 * @param {object[]} constructions les constructions à réaliser (voir getTraceFromPoints())
 * @return {Promise<boolean>}
 */
export async function runOne (page, editor, rep, constructions) {
  // answer retourne rep, donc il n’y a aucun interêt à checker result
  const result = await answer(page, editor, rep, constructions)
  logIfVerbose(`Ce que retourne answer : ${result}`)
  if (rep === true && !result) throw Error('On voulait la saisie d’une bonne réponse mais ça n’a pas été le cas')
  if (rep === false && result) throw Error('On voulait la saisie d’une mauvaise réponse mais ça n’a pas été le cas')
  if (result) {
    await checkFeedbackCorrection(page, result, '.ScratchFeedback')
  } else {
    await checkFeedbackCorrection(page, /(Objectif non atteint|C’est faux|je ne trouve pas)/is, '.ScratchFeedback')
  }
  return result
}

/**
 * Teste toutes les répétitions de la section blokmtg01 avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {string} [options.id] id du nœud dans le graphe
 *
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { id } = {}) {
  // on génère de l’aléatoire
  const reponses = []
  while (reponses.length < params.nbrepetitions) {
    reponses.push([Math.random() < 0.5, Math.random() < 0.5])
  }

  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  await page.waitForSelector('.enonce')
  const nbChances = params.nbchances ?? 1 // l’auteur de cette section n’a pas prévu de fixer ce paramètre, je l’ai fait dans les fichier de test à 1
  // un raccourci (async)
  const myCheckEtat = () => checkEtat(page, { numQuestion, score, total })
  // et on y va, boucle questions
  while (numQuestion <= total) {
    // initialisation du handler
    const editor = new ScratchHandler(page, 42, ['SEGMENTS', 'DROITES', 'DEMI-DROITES']) // le deuxième paramètre est l’increment en pixel pour le scrolling et le troisième, la liste des catégories du menu tools
    await editor.init()
    let numEssais = 1
    // boucle essais
    // on doit récupérer les constructions à faire avant de lancer les runOne
    await page.waitForSelector('.boutonFermerFigureDepart:visible', { timeout: 10_006 })
    await page.waitForSelector('.boutonFermerModele:visible', { timeout: 10_007 })
    const boutonFermerFigureDepart = page.locator('.boutonFermerFigureDepart:visible')
    const boutonFermerModele = page.locator('.boutonFermerModele:visible')
    const svgModele = page.locator('#modele_0')
    // const svgFigureDepart = page.locator('#modelebase_0') Ne sert à rien ?
    // Il faut trouver les différences entre le modèle et la figure de départ et mettre tout ça dans un objet pour qu’il soit traité par answer ...
    // inutile ce sont les 3 segment de l’unité const linesDepart = await svgFigureDepart.locator('>line').all()
    // Les locators des points
    const pointsModele = await svgModele.locator('>g', { has: page.locator('line') }).all()
    // depuis une modification récente, tous les points sont en double...
    pointsModele.splice(0, pointsModele.length / 2)
    // les locators des noms des points (celui d’index 0 est le '1' de l’unité...
    const nomsPointsModele = await svgModele.locator('>g', { has: page.locator('text') }).all()
    const Points = []
    const constructions = []
    for (let i = 1; i <= pointsModele.length; i++) {
      Points.push({
        nom: await nomsPointsModele[i].textContent(),
        locator: pointsModele[i - 1],
        position: await getMiddleOfLine(pointsModele[i - 1].locator('line').first())
      })
    }
    const linesFigure = await svgModele.locator('>line').all()
    // On a trois lignes à zapper qui participent au segment unité
    for (let l = 3; l < linesFigure.length; l++) {
      const lineFigure = linesFigure[l]
      const debutLigne = { x: await lineFigure.getAttribute('x1'), y: await lineFigure.getAttribute('y1') }
      const finLigne = { x: await lineFigure.getAttribute('x2'), y: await lineFigure.getAttribute('y2') }
      // on contruit la liste des Points (objets avec le nom, le locator et la position dans le svg)

      // on va chercher ce qu’il faut construire (droite, demi-droite ou segment, et les 2 points concernés issus de Points
      constructions[l - 3] = getTraceFromPoints(debutLigne, finLigne, Points)
      // on rends compte de ce qu’on a trouvé
      logIfVerbose(`Construction trouvée : ${constructions[l - 3].type} avec les points : ${constructions[l - 3].point1.nom} et ${constructions[l - 3].point2.nom}`)
    }

    // On peut faire disparaître les dialogues présentant les figures pour passer à Scratch
    await boutonFermerFigureDepart.click()
    await boutonFermerModele.click()

    while (numEssais <= nbChances) {
      // attend l’énoncé
      await myCheckEtat()
      const rep = reponses[numQuestion - 1][numEssais - 1]
      const ok = await runOne(page, editor, rep, constructions)
      if (!ok && numEssais < nbChances) {
        // On nettoie la zone de drop pour une nouvelle tentative (si le bouton fermer et réinitialiser faisait son job, je n’aurait pas à le faire)
        await page.locator('div.modale').locator('input.MepBoutons2', { hasText: 'Fermer' }).click()
        await editor.resetDropZone()
        await editor.clearDropZone()
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await myCheckEtat()
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe blokmtg01_datas0.js (que l’on charge ici)
 */
export async function test (page) {
  const { graphe } = await import('./blokmtg01_datas0.js')
  await loadGraphe(page, { graphe })
  const { params, id } = getNodeInfos(graphe, 'blokmtg01')
  const score = await runAll(page, params)
  await checkBilan(page, [{ id, score, nbRepetitions: params.nbrepetitions }])
  return true
}
