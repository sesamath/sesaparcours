import { clickSectionSuivante, clickSuite } from 'testBrowser/helpers/actions'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import ScratchHandler from 'testBrowser/helpers/ScratchHandler'
import { getTextsFromSvg } from 'testBrowser/helpers/svg'

/**
 * Retourne la position y du cercle rouge du point de départ afin de déterminer le sens de rotation par la suite
 * @param figure
 * @returns {Promise<number>}
 */
async function getStartPosition (figure) {
  const circles = await figure.locator('circle').all()
  let higherY = 1000
  for (const circle of circles) {
    higherY = Math.min(higherY, Math.round(Number(await circle.getAttribute('cy'))))
  }
  return higherY
}

async function getMaxOrdLine (figure) {
  const lines = await figure.locator('line').all()
  let higherY = 1000
  for (const line of lines) {
    higherY = Math.min(higherY, Math.round(Number(await line.getAttribute('y1'))), Math.round(Number(await line.getAttribute('y2'))))
  }
  return higherY
}

/**
 *
 * @param page
 * @param {boolean} rep Si on veut avoir juste, on met true, cela modifiera la conclusion pour que la correction fonctionne
 * @param {string} figure 'rectangle' |'carré' | 'triangle' | 'hexagone' | 'parallélogramme'
 * @param {boolean} boucle true si boucle obligatoire
 * @returns {Promise<*>}
 */
async function answer (page, editor, rep, figure, boucle) {
  let erreur = ''
  if (!rep) {
    if (Math.random() < 0.3) erreur = 'sens'
    else if (Math.random() < 0.5) erreur = 'longueur'
    else erreur = 'angle'
    logIfVerbose('Type d’erreur : ', erreur)
  } else {
    logIfVerbose('Je répond sans erreur')
  }

  // await editor.chapeau.highlight() // une méthode PlayWright pour identifier un Locator à l’écran...
  // on récupère les données de la figure (à priori au moins une longueur et un angle... mais ça dépend des figures
  const modeleSvg = editor.modele.locator('>svg')
  const startY = await getStartPosition(modeleSvg)
  const maxY = await getMaxOrdLine(modeleSvg)
  const modelTexts = await getTextsFromSvg(modeleSvg)
  const boucleNbFois = figure === 'carré' ? '4' : (figure === 'triangle' ? '3' : (figure === 'hexagone' ? '6' : '2'))

  await editor.toggleCategory('STYLO')
  const stylo = await editor.getBlockFrom({ dataId: 'poserstylo', classList: '.blocklyDraggable' })
  // La méthode ScratchHandler.dragFromTo sert à déplacer un block dans la zone de travail la target (facultative) est par défaut ScratchHandler.zoneDeTravail
  await editor.dragFromTo({ category: 'STYLO', source: stylo, targetPosition: { x: 5, y: 5 } })
  // pose le bloc poserstylo sous le chapeau 'quand 'greenFlag' est pressé
  if (boucle) {
    await editor.toggleCategory('CONTROLES')
    const repeter = await editor.getBlockFrom({ dataId: 'control_repeat', classList: '.blocklyDraggable' })
    await editor.clickAndType(repeter, 'CONTROLES', boucleNbFois)
    await editor.dragFromTo({ category: 'CONTROLES', source: repeter, targetPosition: editor.nextPosition })
  }
  // pour la mauvaise réponse, on va simplement tourner dans le mauvais sens.
  const sensDeRotation = startY === maxY
    ? (erreur === 'sens' ? 'direct' : 'indirect')
    : (erreur === 'sens' ? 'indirect' : 'direct')
  const tourner = await editor.getBlockFrom({
    dataId: `motion_turn${sensDeRotation === 'indirect' ? 'right' : 'left'}`,
    classList: '.blocklyDraggable'
  })
  // Il faut activer la catégorie puis chercher les blocks, parce que s’ils ne sont pas visibles, on risque de ne pas pouvoir modifier les valeurs et les déplacer
  await editor.toggleCategory('DEPLACEMENTS')
  const avancer = await editor.getBlockFrom({ dataId: 'motion_movesteps', classList: '.blocklyDraggable' })
  switch (figure) {
    case 'triangle':
    case 'hexagone':
    case 'carré': {
      let longueurCote = modelTexts.filter((el) => el !== 'Modèle' && el.charAt(el.length - 1) !== '°')[0] // c’est un string !
      let unAngle = modelTexts.filter((el) => el.charAt(el.length - 1) === '°')[0] // c’est un string !
      unAngle = unAngle.substring(0, unAngle.length - 1) // on enlève le '°'
      if (erreur === 'longueur') longueurCote = (Number(longueurCote) + 1).toString()
      if (erreur === 'angle') unAngle = (Number(unAngle) + 10).toString()
      const angleDeRotation = (180 - Number(unAngle)).toString()
      if (boucle) {
        // ScratchHandler.clickAndType() sert à saisir des valeurs dans les inputs des blocs, un quatrième paramètre (index) sert si il y a plusieurs input pour atteindre les suivants
        await editor.clickAndType(avancer, 'DEPLACEMENTS', longueurCote)
        await editor.dragFromTo({
          category: 'DEPLACEMENTS',
          source: avancer,
          targetPosition: { x: editor.nextPosition.x + 40, y: editor.nextPosition.y - 60 } // il faut se mettre à l’intérieur du bloc répéter
        })
        await editor.clickAndType(tourner, 'DEPLACEMENTS', angleDeRotation)
        await editor.dragFromTo({
          category: 'DEPLACEMENTS',
          source: tourner,
          targetPosition: editor.nextPosition
        })
      } else {
        for (let i = 0; i < boucleNbFois; i++) {
          await editor.clickAndType(avancer, 'DEPLACEMENTS', longueurCote)
          await editor.dragFromTo({
            category: 'DEPLACEMENTS',
            source: avancer,
            // sourcePosition: { x: 10, y: 5 },
            targetPosition: editor.nextPosition
          })
          await editor.clickAndType(tourner, 'DEPLACEMENTS', angleDeRotation)
          await editor.dragFromTo({
            category: 'DEPLACEMENTS',
            source: tourner,
            //     sourcePosition: { x: 10, y: 5 },
            targetPosition: editor.nextPosition
          })
        }
      }
    }
      break
    case 'parallélogramme':
    case
      'rectangle'
      : {
        const longueursCotes = modelTexts.filter((el) => el !== 'Modèle' && el.charAt(el.length - 1) !== '°') // c’est un string[] !
        const quatreAngles = modelTexts.filter((el) => el.charAt(el.length - 1) === '°').map((el) => el.substring(0, el.length - 1))// c’est un string[] !
        let premierAngle = Number(quatreAngles[1])
        if (erreur === 'angle') premierAngle += 10
        const deuxiemeAngle = (180 - premierAngle).toString()
        premierAngle = premierAngle.toString()
        const premiereLongueur = (Number(longueursCotes[0]) + (erreur === 'longueur' ? 1 : 0)).toString()
        const deuxiemeLongueur = (Number(longueursCotes[1]) + (erreur === 'longueur' ? 1 : 0)).toString()
        if (boucle) {
          await editor.clickAndType(avancer, 'DEPLACEMENTS', premiereLongueur)
          await editor.dragFromTo({
            category: 'DEPLACEMENTS',
            source: avancer,
            targetPosition: { x: editor.nextPosition.x + 40, y: editor.nextPosition.y - 60 }
          })
          await editor.clickAndType(tourner, 'DEPLACEMENTS', premierAngle)
          await editor.dragFromTo({ category: 'DEPLACEMENTS', source: tourner, targetPosition: editor.nextPosition })
          await editor.clickAndType(avancer, 'DEPLACEMENTS', deuxiemeLongueur)
          await editor.dragFromTo({ category: 'DEPLACEMENTS', source: avancer, targetPosition: editor.nextPosition })
          await editor.clickAndType(tourner, 'DEPLACEMENTS', deuxiemeAngle)
          await editor.dragFromTo({ category: 'DEPLACEMENTS', source: tourner, targetPosition: editor.nextPosition })
        } else {
          for (let i = 0; i < boucleNbFois; i++) {
            await editor.clickAndType(avancer, 'DEPLACEMENTS', premiereLongueur)
            await editor.dragFromTo({ category: 'DEPLACEMENTS', source: avancer, targetPosition: editor.nextPosition })
            await editor.clickAndType(tourner, 'DEPLACEMENTS', premierAngle)
            await editor.dragFromTo({ category: 'DEPLACEMENTS', source: tourner, targetPosition: editor.nextPosition })
            await editor.clickAndType(avancer, 'DEPLACEMENTS', deuxiemeLongueur)
            await editor.dragFromTo({ category: 'DEPLACEMENTS', source: avancer, targetPosition: editor.nextPosition })
            await editor.clickAndType(tourner, 'DEPLACEMENTS', deuxiemeAngle)
            await editor.dragFromTo({ category: 'DEPLACEMENTS', source: tourner, targetPosition: editor.nextPosition })
          }
        }
      }
      break
  }
  // On remet le code en position de départ pour que la position du premier bloc coincide en cas de nettoyage
  await editor.resetDropZone()
  // on lance le programme
  await editor.drapeau.click()
  await page.waitForSelector('.ScratchFeedback', { timeout: 10_003 })
  return rep
}

/**
 * Joue un essai pour une répétition (si il y a des blocks présents, drop tous les blocs, puis code un programme avant de cliquer sur le drapeau vert
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {ScratchHandler} editor Le handler de la 'zone' Scratch
 * @param {boolean} rep si true on fournit la bonne réponse
 * @param {string} figure
 * @param {boolean} boucle true si boucle obligatoire
 * @return {Promise<boolean>}
 */
export async function runOne (page, editor, rep, figure, boucle) {
  // answer retourne rep, donc il n’y a aucun interêt à checker result
  const result = await answer(page, editor, rep, figure, boucle)
  logIfVerbose(`Ce que retourne answer : ${result}`)
  if (rep === true && !result) throw Error('On voulait la saisie d’une bonne réponse mais ça n’a pas été le cas')
  if (rep === false && result) throw Error('On voulait la saisie d’une mauvaise réponse mais ça n’a pas été le cas')
  if (result) {
    await checkFeedbackCorrection(page, result, '.ScratchFeedback')
  } else {
    await checkFeedbackCorrection(page, /(Objectif non atteint|C’est faux|ne correspond pas au modèle)/is, '.ScratchFeedback')
  }
  return result
}

/**
 * Teste toutes les répétitions de la section blokfigure avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {string} [options.id] id du nœud dans le graphe
 *
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { id } = {}) {
  // on génère de l’aléatoire
  const reponses = []
  while (reponses.length < params.nbrepetitions) {
    reponses.push([Math.random() < 0.5, Math.random() < 0.5, Math.random() < 0.5, Math.random() < 0.5])
  }

  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  await page.waitForSelector('.enonce')
  // si c’est pas précisé dans les params, on prend la valeur par défaut fixée par le modèle, 1
  const nbChances = params.nbchances ?? 1
  // un raccourci (async)
  const myCheckEtat = () => checkEtat(page, { numQuestion, score, total })
  // et on y va, boucle questions
  while (numQuestion <= total) {
    // initialisation du handler
    const scratchHandler = new ScratchHandler(page, 42) // le deuxième paramètre est l’increment en pixel pour le scrolling
    await scratchHandler.init()
    // on récupère la position du drop du bloc stylo (position qu’on devra fournir pour nettoyer la zone de drop)
    const styloPosition = scratchHandler.nextPosition
    let numEssais = 1
    // boucle essais
    while (numEssais <= nbChances) {
      // attend l’énoncé
      const enonce = await scratchHandler.enonce.innerText()
      const figure = enonce.match(/rectangle|parallélogramme|hexagone|triangle|carré/)[0]
      await myCheckEtat()
      const rep = reponses[numQuestion - 1][numEssais - 1]
      const ok = await runOne(page, scratchHandler, rep, figure, params.Boucle)
      if (!ok && numEssais < nbChances) {
        const fermer = page.locator('.MepBoutons2', { hasText: 'Fermer' })
        await fermer.click()
        // On nettoie la zone de drop pour une nouvelle tentative (si le bouton fermer et réinitialiser faisait son job, je n’aurait pas à le faire)
        await scratchHandler.clearDropZone(styloPosition)
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await myCheckEtat()
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe blokfigure_datas0.js (que l’on charge ici)
 */
export async function test (page) {
  page.setDefaultTimeout(10_000)
  const { graphe } = await import('./blokfigure_datas0.js')
  await loadGraphe(page, { graphe })
  const { params, id } = getNodeInfos(graphe, 'blokfigure')
  const score = await runAll(page, params)
  await checkBilan(page, [{ id, score, nbRepetitions: params.nbrepetitions }])
  const { graphe: graphe2 } = await import('./blokfigure_datas1.js')
  await loadGraphe(page, { graphe: graphe2 })
  const { params: params2, id: id2 } = getNodeInfos(graphe2, 'blokfigure')
  const score2 = await runAll(page, params2)
  await checkBilan(page, [{ id: id2, score: score2, nbRepetitions: params2.nbrepetitions }])

  return true
}
