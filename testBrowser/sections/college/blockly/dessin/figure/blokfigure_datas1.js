export const graphe = [
  [
    '1',
    'blokfigure',
    [
      {
        nn: '2',
        score: 'sans+condition',
        conclusion: 'Fin de l‘activité'
      },
      {
        nbrepetitions: 10,
        limite: 0,
        MiseEnPagePlusClaire: false,
        indication: '',
        Travail: 'Espaces',
        Partie: 'Entière',
        Mode: 'Click',
        MortSubite: true,
        nbchances: 2,
        PasAPas: false,
        Sequence: false,
        difficultemin: 1,
        difficultemax: 3,
        difficulte: 'Progressive',
        angle: 'Rendu',
        tolerance: 0,
        Carre: true,
        TriangleEquilateral: true,
        Rectangle: true,
        Parallelogramme: true,
        Hexagone: true,
        Boucle: true,
        AnglesAffiches: true
      }
    ]
  ],
  [
    '2',
    'fin',
    [
      null
    ]
  ]
]
