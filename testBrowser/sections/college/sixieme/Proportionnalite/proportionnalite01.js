import ListeDeroulanteHandler from 'testBrowser/helpers/ListeDeroulanteHandler'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkFeedbackCorrection, checkEtat } from 'testBrowser/helpers/checks'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { clickSuite, clickSectionSuivante, clickOk } from 'testBrowser/helpers/actions'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { randomBool, randomElement } from 'src/lib/utils/random'

/**
 *
 * @param page
 * @param {number} coefficient Le coefficient de proportionnalité entier entre les valeurs de la 1ère grandeur
 * @param {string} operation Le choix qu’on fera dans la liste entre multiplie, divise, ajoute ou soustrais
 * @param {boolean} concordance Si on utilise l’affirmative dans le deuxième champ
 * @param {boolean} rep Si on veut avoir juste, on met true, cela modifiera la conclusion pour que la correction fonctionne
 * @returns {Promise<*>}
 */
async function answer (page, coefficient, operation, concordance, rep) {
  const maListe1 = new ListeDeroulanteHandler(page)
  const choices1 = await maListe1.getTexts()
  await maListe1.selectText(choices1.find(elt => elt.includes(operation)))
  const coeffSelector = await page.locator('.zoneS3BAse').first()
  await coeffSelector.click()
  await coeffSelector.locator('.zsmMqFocusedCursor').first().type(coefficient.toString())
  const maListe2 = new ListeDeroulanteHandler(page, { index: 1 })
  const choices2 = await maListe2.getTexts()
  await maListe2.selectText(concordance ? choices2.find(elt => elt.includes('aussi')) : choices2.find(elt => elt.includes('pas')))
  const maListe3 = new ListeDeroulanteHandler(page, { index: 2 })
  const choices3 = await maListe3.getTexts()
  await maListe3.selectText((rep && !concordance) || (!rep && concordance)
    ? choices3.find(elt => elt.includes('Il ne s’agit pas'))
    : choices3.find(elt => elt.includes('Il s’agit')))
  await clickOk(page)
  return rep
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {string} operation passer 'multiplie' ou 'divise' ou 'ajoute' ou 'soustrais' pour choisir le champ choisir
 * @param {boolean} concordance si true on utilise la première phrase dans le 2e champ choisir qui affirme false si on choisit la négation
 * La liste des valeurs de operation et concordance est établie par runAll pour faire varier les plaisirs.
 * @return {Promise<boolean>}
 */
export async function runOne (page, operation, concordance) {
  const phrase1 = await page.locator('.enonce > span').first().innerText()
  const phrase2 = await page.locator('.enonce > span').nth(1).innerText()
  const donneesPhrase1 = phrase1.match(/[0-9]{1,3}/g)
  const donneesPhrase2 = phrase2.match(/[0-9]{1,3}/g)
  const isProportionnel = donneesPhrase1[0] * donneesPhrase2[1] - donneesPhrase1[1] * donneesPhrase2[0] === 0
  const coefficient = Number.isInteger(donneesPhrase2[0] / donneesPhrase1[0])
    ? donneesPhrase2[0] / donneesPhrase1[0]
    : donneesPhrase1[0] / donneesPhrase2[0]
  if (!Number.isInteger(coefficient)) throw Error('Le coefficient calculé sur la première grandeur n’est pas entier')
  // La réponse est bonne si on choisit l’une des opérations multiplie ou divise, si il y proportionnalité et si on choisit la concordance des 'choisir'
  // ou bien si on choisit la bonne opération, qu’il n’y a pas proportionnalité et non concordance des 'choisir'
  const rep = (isProportionnel && ['multiplie', 'divise'].includes(operation) && concordance) || (!isProportionnel && ['multiplie', 'divise'].includes(operation) && !concordance)
  logIfVerbose(`Les 2 premiers nombres : ${donneesPhrase1[0]} et ${donneesPhrase2[0]} pour la première grandeur`)
  logIfVerbose(`Les 2 autres nombres : ${donneesPhrase1[1]} et ${donneesPhrase2[1]} pour la deuxième grandeur`)
  logIfVerbose(`Opération choisie : ${operation} ; concordance des champs : ${concordance ? 'Oui' : 'Non'} ; y a-t-il proportionnalité : ${isProportionnel ? 'Oui' : 'Non'} ; enfin le résultat attendu : ${rep ? 'C’estbien' : 'Raisonnement faux ...'}`)
  const result = await answer(page, coefficient, operation, concordance, rep)
  logIfVerbose(`Ce que retourne answer : ${result}`)
  if (rep === true && !result) throw Error('On voulait la saisie d’une bonne réponse mais ça n’a pas été le cas')
  if (rep === false && result) throw Error('On voulait la saisie d’une mauvaise réponse mais ça n’a pas été le cas')
  // on vérifie le feedback ok/ko cohérent avec ce qu’on a saisi
  if (result) {
    await checkFeedbackCorrection(page, result, '#MepMD')
  } else {
    await checkFeedbackCorrection(page, /(Raisonnement faux !)|(C’est faux)/, '#MepMD')
  }
  return result
}

/**
 * Teste toutes les répétitions de la section proportionnalite01 avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {string} [options.id] id du nœud dans le graphe
 *
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { id } = {}) {
  // on génère de l’aléatoire
  const responses = []
  while (responses.length < params.nbrepetitions) {
    const operation = randomElement(['divise', 'multiplie', 'multiplie', 'divise', 'ajoute', 'soustrais'])
    const concordance = randomBool()
    responses.push({ operation, concordance })
  }
  // attend l’énoncé
  await page.waitForSelector('.enonce')
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  let nbChances = params.nbchances
  if (typeof nbChances !== 'number' || nbChances < 1) {
    logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // un raccourci (async)
  const myCheckEtat = () => checkEtat(page, { numQuestion, score, total, numProgression: id })
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    const { operation, concordance } = responses[numQuestion - 1]
    // boucle essais
    while (numEssais <= nbChances) {
      await myCheckEtat()
      const ok = await runOne(page, operation, concordance)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await myCheckEtat()
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe proportionnalite01_data0.js (que l’on charge ici)
 */
export async function test (page) {
  const { graphe } = await import('./proportionnalite01_datas0.js')

  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })

  // et on lance la batterie de tests par défaut
  const { params, id } = getNodeInfos(graphe, 'proportionnalite01')
  const score = await runAll(page, params)
  await checkBilan(page, [{ id, score, nbRepetitions: params.nbrepetitions }])
  // on clique pas sur suivant car y’en a pas
  return true
}
