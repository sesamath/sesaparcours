import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkFeedbackCorrection, checkEtat } from 'testBrowser/helpers/checks'
import { dropLatex } from 'testBrowser/helpers/text'
import { randomBool, randomElement } from 'src/lib/utils/random'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { clickSuite, clickSectionSuivante } from 'testBrowser/helpers/actions'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { getFirstDelimitedContent } from 'src/lib/utils/string'

const enforceRep = (rep) => (typeof rep === 'boolean' ? rep : false)

/**
 *
 * @param {Page} page
 * @param {Locator} locator
 * @returns {Promise<void>}
 */
const shortTimeoutOption = { timeout: 3000 }
async function clickOpe (page, locator) {
  await locator.waitFor(shortTimeoutOption)
  await locator.click()
}

async function answer (page, ope, operationsPossibles, ok) {
  const span = await page.locator(`.mq-root-block > span:has-text("${ope}")`).first()
  let span2
  let notOpe
  let compteur = 0
  do {
    notOpe = randomElement(operationsPossibles)
    span2 = await page.locator(`.mq-root-block > span:has-text("${notOpe}")`).first()
    compteur++
  } while (span2 == null && compteur < 10)
  if (compteur === 10) throw Error(`Playwright n’a pas trouvé l’opérateur à cliquer : $${notOpe}$`)
  if (ok) {
    if (span == null) throw Error(`Playwright n’a pas trouvé l’opérateur à cliquer : $${ope}$`)
    await clickOpe(page, span)
  } else {
    await clickOpe(page, span2)
  }
  return ok
}

const opePriOfExp = (expr) => {
  const expression = getFirstDelimitedContent(expr, '(')
  if (expression !== '') {
    const ope = expression.match(/[+−]/)
    if (ope == null) {
      logIfVerbose(`L’expression ${expression} ne contient ni + ni − alors que ça devrait`)
      throw Error(`L’expression entre parenthèses ne contient ni + ni − : ${expression}`)
    }
    logIfVerbose(`L’opération prioritaire de l’expression ${expression} est ${ope[0]}`)
    return ope[0]
  }
  // l’expression n’a pas de parenthèses... on cherche la première division ou multiplication
  const ope = expr.match(/[×÷]/)
  if (ope == null) {
    logIfVerbose(`L’expression sans parenthèses ${expr} n’a pas de × ni de ÷, on retourne donc expr.match(/[+−]/)`)
    return expr.match(/[+−]/)[0]
  }
  logIfVerbose(`L’opération prioritaire de l’expression ${expression} est ${ope[0]}`)
  return ope[0]
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean|string} rep passer true pour une bonne réponse et false|string pour une mauvaise
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep) {
  const eltEnonce = await page.$('.travail')
  const text = await eltEnonce.innerText()

  const enonce = dropLatex(text).replace(/\s+/g, ' ') // vire les espaces consécutifs (ou mélange d’espaces avec insécables ou \t ou \r ou \n) et remplace \t en espace simple
  const ope = opePriOfExp(enonce)
  const operationsPossibles = enonce.match(/[+−×÷]/g).map(elt => elt[0] !== ope ? elt[0] : null).filter(el => el != null)
  const result = await answer(page, ope, operationsPossibles, rep)
  if (rep === true && !result) throw Error('On voulait la saisie d’une bonne réponse mais ça n’a pas été le cas')
  if (rep === false && result) throw Error('On voulait la saisie d’une mauvaise réponse mais ça n’a pas été le cas')
  // on vérifie le feedback ok/ko cohérent avec ce qu’on a saisi
  await checkFeedbackCorrection(page, result, '#MepMD')
  // y’a eu le click OK (dans answer), pas le click suite
  return result
}

/**
 * Teste toutes les répétitions de la section priorite01 avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {string} [options.id] id du nœud dans le graphe
 * @param {boolean[]} [options.responses] Passer un tableau de booléens pour les réponses à mettre, un élément peut être un tableau de 2 booléens (dont le 1er est false et le 2e la réponse à donner au 2e, jusqu’au dernier si c’est false et qu’il y a plus de 2 essais)
 *
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { id, responses } = {}) {
  if (responses) {
    // on normalise en systématisant les array
    responses = responses.map(rep => {
      if (Array.isArray(rep)) return rep.map(enforceRep)
      return [enforceRep(rep)]
    })
  } else {
    // on génère de l’aléatoire
    responses = []
    while (responses.length < params.nbrepetitions) {
      const rep = randomBool()
      // si c’est faux on tire le 2e essai au hasard, on devrait donc avoir une moyenne de score de 0.5 si y’a un essai et 0.75 si y’en a deux
      responses.push(rep ? [rep] : [rep, randomBool()])
    }
  }
  // attend l’énoncé
  await page.waitForSelector('.enonce')
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  let nbChances = params.nbchances
  if (typeof nbChances !== 'number' || nbChances < 1) {
    logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // un raccourci (async)
  const myCheckEtat = () => checkEtat(page, { numQuestion, score, total, numProgression: id })
  // et on y va, boucle questions
  while (numQuestion <= total) {
    // on vérifie que l’on a un bouton équerre si c’est dans les params
    // const boutonEquerre = await page.$('input.MepBoutons text=Utiliser une équerre')
    // if (params.Equerre && !boutonEquerre) throw Error('On voulait le bouton équerre et il n’y est pas')
    // else if (!params.Equerre && boutonEquerre) throw Error('On ne voulait pas le bouton équerre et il y est')
    let numEssais = 1
    const [rep1, rep2] = responses[numQuestion - 1]
    // boucle essais
    while (numEssais <= nbChances) {
      await myCheckEtat()
      const rep = numEssais < 2 ? rep1 : rep2
      const ok = await runOne(page, rep)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await myCheckEtat()
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe priorite01_data0.js (que l’on charge ici)
 */
export async function test (page) {
  const datas = await import('./priorite01_datas0.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, datas)

  // et on lance la batterie de tests par défaut
  const { params, id } = getNodeInfos(datas.graphe, 'priorite01')
  const score = await runAll(page, params)
  await checkBilan(page, [{ id, score, nbRepetitions: params.nbrepetitions }])
  // on clique pas sur suivant car y’en a pas
  return true
}
