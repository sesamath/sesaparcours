import { randomBool } from 'src/lib/utils/random'
import { clickOk, clickSectionSuivante, clickSuite } from 'testBrowser/helpers/actions'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import ListeDeroulanteHandler from 'testBrowser/helpers/ListeDeroulanteHandler'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { getAngleDegFromVect } from 'testBrowser/helpers/math'
import { lineToVect } from 'testBrowser/helpers/svg'
import { dropLatex } from 'testBrowser/helpers/text'

// liste des options possibles
let possibles = new Set('nul.', 'aigu.', 'droit.', 'obtus.', 'plat.')
// s’assure que rep est une réponse possible
const enforceRep = (rep) => (typeof rep === 'boolean' || possibles.has(rep)) ? rep : false

// réitinialise la liste des possibles d’après les params
const resetPossibles = (params) => {
  possibles = new Set(['aigu.', 'obtus.'])
  if (params.Nul) possibles.add('nul.')
  if (params.Droit) possibles.add('droit.')
  if (params.Plat) possibles.add('plat.')
}
/**
 * var globale (à ce fichier) affectée par runOne
 * @type {ListeDeroulanteHandler}
 */
let liste

function getGoodChoice (angle) {
  if (typeof angle === 'string') angle = Number(angle)
  if (typeof angle !== 'number') throw Error('angle doit être un number')
  switch (angle) {
    case 0: return 'nul.'
    case 90: return 'droit.'
    case 180: return 'plat.'
    default: return angle < 90 ? 'aigu.' : 'obtus.'
  }
}

/**
 * Analyse l’énoncé pour trouver les solutions à mettre dans les input de la page
 * @param {Page} page
 * @param {boolean|string} ok passer true pour une bonne réponse, false pour une mauvaise ou une string pour la choisir précisément (si la string est pas dans la liste des possibles ce sera une mauvaise réponse)
 * @return {Promise<boolean>} true si on a mis la bonne réponse
 */
async function readSvgAndAnswer (page, ok) {
  // faut attendre que mtg soit chargé et ait fait son rendu
  await page.waitForSelector('.travail svg')
  // on cherche les 2 premiers tags line
  const [seg1, seg2] = await page.$$('.travail svg > line')
  if (!seg1 || !seg2) throw Error('la figure n’est pas celle attendue')
  const v1 = await lineToVect(seg1)
  const v2 = await lineToVect(seg2)
  const angle = getAngleDegFromVect(v1, v2)
  logIfVerbose(`trouvé dans la figure l’angle ${angle}° entre les vecteurs`, v1, v2)
  return answer(page, angle, ok)
}

/**
 * Analyse l’énoncé
 * @param page
 * @param angle
 * @param {boolean|string} ok passer true pour une bonne réponse, false pour une mauvaise ou une string pour la choisir précisément (si la string est pas dans la liste des possibles ce sera une mauvaise réponse)
 * @return {Promise<boolean>} true si on a mis la bonne réponse
 */
async function answer (page, angle, ok) {
  // on vérifie que la liste du select correspond bien à la liste des possibles
  const choices = await liste.getTexts()
  if (choices.length !== possibles.size || !choices.every(choice => possibles.has(choice))) {
    throw Error(`La page propose les choix (${choices.join('|')}) alors qu’on devrait avoir (${Array.from(possibles).join('|')})`)
  }
  // et on répond
  const goodChoice = getGoodChoice(angle)
  const choice = (ok === true)
    ? goodChoice
    : (possibles.has(ok))
        ? ok
        : (goodChoice === 'obtus.')
            ? 'aigu.'
            : 'obtus.'
  logIfVerbose(`Pour l’angle ${angle} la bonne réponse serait ${goodChoice} et on va répondre ${choice}`)
  await liste.selectText(choice)
  await clickOk(page)
  return choice === goodChoice
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean|string} rep passer true pour une bonne réponse et false|string pour une mauvaise
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep) {
  const eltEnonce = await page.$('.enonce')
  const enonce = dropLatex(await eltEnonce.innerText())
  liste = new ListeDeroulanteHandler(page)
  // on regarde si l’angle est dans l’énoncé, ou s’il faut le lire sur le svg
  const chunks = /([A-Z]+)\s*=\s*([0-9]+).?°/.exec(enonce) // le .? c’est à cause d’un caractère bizarre entre le dernier chiffre et °
  const result = chunks
    ? await answer(page, chunks[2], rep)
    : await readSvgAndAnswer(page, rep)
  if (rep === true && !result) throw Error('On voulait la saisie d’une bonne réponse mais ça n’a pas été le cas')
  if (rep === false && result) throw Error('On voulait la saisie d’une mauvaise réponse mais ça n’a pas été le cas')
  // on vérifie le feedback ok/ko cohérent avec ce qu’on a saisi
  await checkFeedbackCorrection(page, result, '#MepMD')
  // y’a eu le click OK (dans answer), pas le click suite
  return result
}

/**
 * Teste toutes les répétitions de la section angle02 avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {string} [options.id] id du nœud dans le graphe
 * @param {boolean[]} [options.responses] Passer un tableau de booléens pour les réponses à mettre, un élément peut être un tableau de 2 booléens (dont le 1er est false et le 2e la réponse à donner au 2e, jusqu’au dernier si c’est false et qu’il y a plus de 2 essais)
 *
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { id, responses } = {}) {
  resetPossibles(params)
  // normalise les réponses (tableau de booléens, pour chaque réponse c’est [true] ou [false, true] ou [false, false], suivant qu’on veut répondre bon ou faux au 1er ou 2e essai)
  if (responses) {
    // on normalise en systématisant les array
    responses = responses.map(rep => {
      if (Array.isArray(rep)) return rep.map(enforceRep)
      return [enforceRep(rep)]
    })
  } else {
    // on génère de l’aléatoire
    responses = []
    while (responses.length < params.nbrepetitions) {
      const rep = randomBool()
      // si c’est faux on tire le 2e essai au hasard, on devrait donc avoir une moyenne de score de 0.5 si y’a un essai et 0.75 si y’en a deux
      responses.push(rep ? [rep] : [rep, randomBool()])
    }
  }
  // attend l’énoncé
  await page.waitForSelector('.enonce')
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  let nbChances = params.nbchances
  if (typeof nbChances !== 'number' || nbChances < 1) {
    logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // un raccourci (async)
  const myCheckEtat = () => checkEtat(page, { numQuestion, score, total, numProgression: id })
  // et on y va, boucle questions
  while (numQuestion <= total) {
    // on vérifie que l’on a un bouton équerre si c’est dans les params
    // const boutonEquerre = await page.$('input.MepBoutons text=Utiliser une équerre')
    // if (params.Equerre && !boutonEquerre) throw Error('On voulait le bouton équerre et il n’y est pas')
    // else if (!params.Equerre && boutonEquerre) throw Error('On ne voulait pas le bouton équerre et il y est')
    let numEssais = 1
    const [rep1, rep2] = responses[numQuestion - 1]
    // boucle essais
    while (numEssais <= nbChances) {
      await myCheckEtat()
      const rep = numEssais < 2 ? rep1 : rep2
      const ok = await runOne(page, rep)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await myCheckEtat()
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe angle02_data0.js (que l’on charge ici)
 */
export async function test (page) {
  const { graphe } = await import('./angle02_datas0.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })

  // et on lance la batterie de tests par défaut
  const { params, id } = getNodeInfos(graphe, 'angle02')
  const score = await runAll(page, params)
  await checkBilan(page, [{ id, score, nbRepetitions: params.nbrepetitions }])
  // on clique pas sur suivant car y’en a pas
  return true
}
