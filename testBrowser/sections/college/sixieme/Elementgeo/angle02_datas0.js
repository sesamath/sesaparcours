export const graphe = [
  [
    '1',
    'angle02',
    [
      {
        nn: '2',
        score: 'sans+condition',
        conclusion: 'Fin de l‘activité'
      },
      {
        nbrepetitions: 9,
        nbchances: 1,
        limite: '0',
        Nul: true,
        Droit: true,
        Plat: true,
        // ['Figure', 'Mesure', 'les deux'] les deux testant à la fois Figure et Mesure, il n’y a besoin que d’un fichier data, j’ai donc supprimé l’autre
        Donnees: 'les deux',
        Equerre: true
      }
    ]
  ],
  [
    '2',
    'fin',
    [
      null
    ]
  ]
]
