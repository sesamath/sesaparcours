// avec justification
export const graphe = [
  [
    '1',
    'airevolagrand',
    [
      {
        nn: '2',
        score: 'sans+condition',
        conclusion: 'Fin de l‘activité'
      }, {
        nbrepetitions: 5,
        nbchances: 1,
        longueur: true,
        aire: true,
        volume: true,
        entiers: true,
        decimaux: true,
        fractions: true,
        justifie: true,
        Calculatrice: true,
        theme: 'standard'
      }
    ]
  ],
  [
    '2',
    'fin',
    [
      {}
    ]
  ]
]
