import { fixArrondi } from 'src/lib/utils/number'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkFeedbackCorrection, checkEtat } from 'testBrowser/helpers/checks'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { clickSuite, clickSectionSuivante, clickOk } from 'testBrowser/helpers/actions'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { getMqChunks } from 'testBrowser/helpers/text'
import ListeDeroulanteHandler from 'testBrowser/helpers/ListeDeroulanteHandler'
import ZsmHandler from 'testBrowser/helpers/ZsmHandler'

/**
 *
 * @param page
 * @param {boolean} justifie si true, il y a une liste déroulante pour choisir le mode de calcul du coefficient final
 * @param {string} stade 'a' ou 'b' pour le cas ou une justification est demandée, il y a la question na et la question nb
 * @param {boolean} isFraction true si le rapport de longueur est fractionnaire
 * @param {string} typeGrandeur 'Volume', 'Aire' ou autre chose => longueur
 * @param {string | string[]} valeurRapport le coefficient initial ou [num,den] en string avec , comme séparateur
 * @param {string | string[]} valeurMesure idem pour l’aire le volume ou la grandeur initiale
 * @param {boolean} rep true si on veut une réponse correcte
 * @returns {Promise<*>}
 */
async function answer (page, { justifie, stade, isFraction, typeGrandeur, valeurRapport, valeurMesure, rep }) {
  let rapportFinalString, num, den, resultString, rapportSimple, mesureString
  const exposantParType = typeGrandeur === 'Volume' ? 3 : typeGrandeur === 'Aire' ? 2 : 1
  if (rep) {
    if (isFraction) {
      num = fixArrondi(Number(valeurRapport[0].replace(',', '.')) ** exposantParType)
      den = fixArrondi(Number(valeurRapport[1].replace(',', '.')) ** exposantParType)
    } else {
      rapportSimple = fixArrondi(Number(valeurRapport.replace(',', '.')) ** exposantParType)
    }
  } else {
    // On prépare une réponse fausse
    if (isFraction) {
      num = fixArrondi(Number(valeurRapport[0].replace(',', '.')))
      den = fixArrondi(Number(valeurRapport[1].replace(',', '.')) ** ((exposantParType + 1) % 3))
    } else {
      rapportSimple = fixArrondi(Number(valeurRapport.replace(',', '.')) + 1)
    }
  }
  if (isFraction) {
    rapportFinalString = num.toString().replace('.', ',') + '/' + den.toString().replace('.', ',')
  } else {
    rapportFinalString = rapportSimple.toString().replace('.', ',')
  }
  if (Array.isArray(valeurMesure)) {
    mesureString = valeurMesure[0] + '/' + valeurMesure[1]
    resultString = fixArrondi((num ?? rapportSimple) * Number(valeurMesure[0].replace(',', '.'))).toString() + '/' + fixArrondi((den ?? 1) * Number(valeurMesure[1].replace(',', '.'))).toString().replace(',', '.')
  } else {
    mesureString = valeurMesure
    if (isFraction) {
      resultString = fixArrondi((num ?? rapportSimple) * Number(valeurMesure.replace(',', '.'))).toString().replace('.', ',') + '/' + den.toString().replace('.', ',')
    } else {
      resultString = fixArrondi((Number(valeurMesure.replace(',', '.')) * rapportSimple)).toString().replace('.', ',')
    }
  }
  logIfVerbose(`${justifie ? 'Au stade ' + stade : ''}`)
  logIfVerbose(`coefficient initial et rep : ${isFraction ? valeurRapport[0] + '/' + valeurRapport[1] : valeurRapport} et ${rep ? 'réponse correcte attendue' : 'réponse fausse attendue'}`)
  logIfVerbose(`Question sur le ${typeGrandeur === 'Volume' || typeGrandeur === 'Aire' ? typeGrandeur : 'Longueur'}`)
  logIfVerbose(`resultat = coeff x mesure, ${resultString}=${rapportFinalString} x ${mesureString}`)
  if (justifie && stade === 'a') {
    const liste = new ListeDeroulanteHandler(page)
    const choices = await liste.getTexts()
    await liste.selectText(choices[rep ? exposantParType - 1 : (exposantParType + 1) % 3])
  } else {
    // On chosit le type de frappe : clavier ou clavier virtuel pour chaque zone
    const typeSaisies = [0, 1, 2].map(() =>
      Math.random() < 0.5 ? 'type' : 'typeClavier'
    )
    const operator = new ZsmHandler(page, { index: 0 })
    await operator.clear()
    if (typeSaisies[0] === 'type') await operator.type(rapportFinalString)
    else await operator.typeClavier(Array.from(rapportFinalString))
    const mesure = new ZsmHandler(page, { index: 1 })
    await mesure.clear()
    if (typeSaisies[1] === 'type') await mesure.type(mesureString)
    else await mesure.typeClavier(Array.from(mesureString))
    const resultat = new ZsmHandler(page, { index: 2 })
    await resultat.clear()
    if (typeSaisies[2] === 'type') await resultat.type(resultString)
    else await resultat.typeClavier(Array.from(resultString))
  }
  await clickOk(page)
  return rep
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean} justifie true si on a l’étape de justification avant de compléter le champ réponses
 * @param {string} stade 'a' ou 'b' pour le cas ou une justification est demandée, il y a la question na et la question nb
 * @param {boolean} rep true si on veut formuler la bonne réponse
 * @return {Promise<boolean>}
 */
export async function runOne (page, { justifie, stade, rep }) {
  const phrase1 = await page.locator('.enonce > span').first().innerText()
  const phrase2 = await page.locator('.enonce > span').nth(1).innerText()
  // const phrase3 = await page.locator('.enonce > span').nth(2).innerText()
  const typeGrandeur = phrase2.split(' ')[0]
  let valeurRapportLatex = getMqChunks(phrase1)[0]

  valeurRapportLatex = valeurRapportLatex.replaceAll('\\text{ }', '')
  const valeurRapport = valeurRapportLatex.includes('frac')
    ? valeurRapportLatex.match(/[0-9,]{1,4}/g).map(str => str)
    : valeurRapportLatex
  const isFraction = Array.isArray(valeurRapport)
  let valeurMesureLatex = getMqChunks(phrase2)[0]
  valeurMesureLatex = valeurMesureLatex.replaceAll('\\text{ }', '').replace('=', '')
  const valeurMesure = valeurMesureLatex.includes('frac')
    ? valeurMesureLatex.match(/[0-9,]{1,6}/g).map(str => str)
    : valeurMesureLatex
  const result = await answer(page, { justifie, stade, isFraction, typeGrandeur, valeurRapport, valeurMesure, rep })

  logIfVerbose(`Ce que retourne answer : ${result}`)
  if (rep === true && !result) throw Error('On voulait la saisie d’une bonne réponse mais ça n’a pas été le cas')
  if (rep === false && result) throw Error('On voulait la saisie d’une mauvaise réponse mais ça n’a pas été le cas')
  // on vérifie le feedback ok/ko cohérent avec ce qu’on a saisi
  if (result) {
    await checkFeedbackCorrection(page, result, '#MepMD')
  } else {
    await checkFeedbackCorrection(page, /(C’est faux !)/, '#MepMD')
  }
  return result
}

/**
 * Teste toutes les répétitions de la section airevolagrand avec les params fournis
 * @param {Page} page
 * @param {object} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {string} [options.id] id du nœud dans le graphe
 *
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { id } = {}) {
  // on génère de l’aléatoire
  const justifie = params.justifie
  const responses = []
  while (responses.length < params.nbrepetitions) {
    responses.push(Math.random() < 0.5)
  }
  // attend l’énoncé
  await page.waitForSelector('.enonce')
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  const totalScore = justifie ? total * 2 : total
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  let nbChances = params.nbchances
  if (typeof nbChances !== 'number' || nbChances < 1) {
    logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // et on y va, boucle questions
  let numQuestionPlus = 'a'
  while (numQuestion <= total) {
    let numEssais = 1
    const rep = responses[numQuestion - 1]
    // boucle essais

    while (numEssais <= nbChances) {
      if (justifie) {
        if (numQuestionPlus === 'a') {
          await checkEtat(page, { numQuestion: numQuestion.toString() + 'a', score, total, totalScore, numProgression: id })
        } else {
          await checkEtat(page, { numQuestion: numQuestion.toString() + 'b', score, total, totalScore, numProgression: id })
        }
      } else {
        await checkEtat(page, { numQuestion, score, total, totalScore, numProgression: id })
      }
      const ok = await runOne(page, { justifie, stade: numQuestionPlus, rep })
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      if (justifie) {
        await checkEtat(page, { numQuestion: numQuestion.toString() + numQuestionPlus, score, total, totalScore, numProgression: id })
      } else {
        await checkEtat(page, { numQuestion, score, total, totalScore, numProgression: id })
      }

      if (justifie && numQuestionPlus === 'a') {
        numQuestionPlus = 'b'
      } else {
        numQuestion++
        numQuestionPlus = 'a'
      }

      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec les graphe airevolagrand_data0.js et airevolagrand_data1.js
 */
export async function test (page) {
  const datas1 = await import('./airevolagrand_datas1.js')
  await loadGraphe(page, datas1)
  const { params: params1, id: id1 } = getNodeInfos(datas1.graphe, 'airevolagrand')
  const score2 = await runAll(page, params1)
  await checkBilan(page, [{ id: id1, score: score2, nbRepetitions: params1.nbrepetitions * 2 }]) // Ici, avec la justification, on a deux fois plus de points, alors pour que le % soit correct on double le nombre de répétitions
  // on charge ce graphe dans la page courante
  const datas2 = await import('./airevolagrand_datas2.js')
  await loadGraphe(page, datas2)
  // et on lance la batterie de tests par défaut
  const { params: params2, id: id2 } = getNodeInfos(datas2.graphe, 'airevolagrand')
  const score1 = await runAll(page, params2)
  await checkBilan(page, [{ id: id2, score: score1, nbRepetitions: params2.nbrepetitions }])
  // on passe au deuxième test avec justification

  return true
}
