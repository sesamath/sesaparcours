export const graphe = [
  [
    '1',
    'puissancesdef10',
    [
      {
        nn: '2',
        score: 'sans+condition',
        conclusion: 'Fin de l‘activité'
      },
      {
        nbrepetitions: 10,
        nbchances: 2,
        Question: 'les deux',
        Exp_Positifs: 'parfois',
        Espaces: false,
        MiseEnPagePlusClaire: false
      }
    ]
  ],
  [
    '2',
    'fin',
    [
      null
    ]
  ]
]
