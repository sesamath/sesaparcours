import ZsmHandler from 'testBrowser/helpers/ZsmHandler'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkFeedbackCorrection, checkEtat } from 'testBrowser/helpers/checks'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { clickSuite, clickSectionSuivante, clickOk } from 'testBrowser/helpers/actions'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { dropLatex } from 'testBrowser/helpers/text'

const arrondiString = (nbString) => {
  return nbString.replace(/0(9){1,20}/, '1')
}
/**
 *
 * @param page
 * @param {string} consigne 'Calcule' ou 'Écris'
 * @param {string} nombre Le nombre tel qu’il est donné ou attendu
 * @param {string} puissance la puissance de 10 telle qu’elle est donnée ou attendue
 * @param {boolean} rep Si on veut avoir juste, on met true, cela modifiera la conclusion pour que la correction fonctionne
 * @returns {Promise<*>}
 */
async function answer (page, consigne, nombre, puissance, rep) {
  const zsm3 = new ZsmHandler(page)
  await zsm3.clear()
  const unSurDeux = Math.random() < 0.5
  if (rep) {
    const saisie = consigne === 'Calcule' ? nombre : puissance
    if (unSurDeux) {
      logIfVerbose('On choisit de cliquer sur le clavier virtuel')
      await zsm3.typeClavier(Array.from(saisie))
    } else {
      logIfVerbose(('On choisit la frappe clavier normal'))
      await zsm3.type(saisie)
    }
    logIfVerbose(`Bonne réponse attendue : On demande ${consigne === 'Calcule' ? 'le nombre' : 'l’exposant'} et je saisis ${saisie} pour ${nombre}=10^${puissance}`)
  } else { // on fabrique une mauvaise réponse
    if (consigne === 'Calcule') {
      const unOuMoinsUn = Math.random() < 0.5 ? -1 : 1
      const de1a3 = Math.ceil(Math.random() * 3)
      const badNumber = arrondiString(Intl.NumberFormat('fr-FR', { maximumFractionDigits: 14, maximumSignificantDigits: 15, useGrouping: false }).format(Number(nombre.replace(',', '.')) * (10 ** (de1a3 * unOuMoinsUn))))
      if (unSurDeux) {
        logIfVerbose('On choisit de cliquer sur le clavier virtuel')
        await zsm3.typeClavier(Array.from(badNumber))
      } else {
        logIfVerbose(('On choisit la frappe clavier normal'))
        await zsm3.type(badNumber)
      }
      logIfVerbose(`Pour le résultat la bonne réponse serait ${nombre} et on saisit ${badNumber}`)
    } else {
      const badExpo = (Number(puissance) + (Math.ceil(Math.random() * 3) * (Math.random() < 0.5 ? -1 : 1))).toString()
      if (unSurDeux) await zsm3.typeClavier(Array.from(badExpo), { index: 0 })
      else await zsm3.type(badExpo)
      logIfVerbose(`Pour l’exposant la bonne réponse serait ${puissance} et on saisit ${badExpo}`)
    }
  }
  await clickOk(page)
  return rep
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean} rep si true on fournit la bonne réponse
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep) {
  const phrase1 = await page.locator('.enonce').locator('td').first().innerText()
  const phrase2 = await page.locator('.enonce').locator('td').nth(1).innerText()
  const consigne = phrase1.match(/[ÉC][a-z]{1,6}/g)[0]
  let nombre, puissance
  if (consigne === 'Calcule') {
    puissance = dropLatex(phrase2.split('$')[1].split('=')[1]).match(/[0-9-]+/g)
    nombre = arrondiString(Intl.NumberFormat('fr-FR', { maximumFractionDigits: 16, maximumSignificantDigits: 17, useGrouping: false }).format(10 ** Number(puissance[1])))
    logIfVerbose(`Puissance à calculer : 10^${puissance[1]}`)
    logIfVerbose(`Nombre résultat : ${nombre}`)
  } else {
    nombre = dropLatex(phrase2.split('=')[1]).replaceAll(' ', '')
    puissance = ['10', Math.log10(Number(nombre.replace(',', '.'))).toString()]
    logIfVerbose(`Nombre donné : ${nombre}`)
    logIfVerbose(`Puissance attendue : ${puissance[1]}`)
  }
  if (puissance[0] !== '10') throw Error(`Un problème avec la mantisse de la puissance : ${puissance[0]}`)
  // answer retourne rep, donc il n’y a aucun interêt à checker result
  const result = await answer(page, consigne, nombre, puissance[1], rep)
  logIfVerbose(`Ce que retourne answer : ${result}`)
  if (rep === true && !result) throw Error('On voulait la saisie d’une bonne réponse mais ça n’a pas été le cas')
  if (rep === false && result) throw Error('On voulait la saisie d’une mauvaise réponse mais ça n’a pas été le cas')
  if (result) {
    await checkFeedbackCorrection(page, result, '#MepMD')
  } else {
    await checkFeedbackCorrection(page, /c.est faux.*essaie encore.*|c.est faux.*/is, '#MepMD')
  }
  await checkFeedbackCorrection(page, result, '#MepMD')
  return result
}

/**
 * Teste toutes les répétitions de la section puissancesdef10 avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {string} [options.id] id du nœud dans le graphe
 *
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { id } = {}) {
  // on génère de l’aléatoire
  const reponses = []
  while (reponses.length < params.nbrepetitions) {
    reponses.push([Math.random() < 0.5, Math.random() < 0.5])
  }
  // attend l’énoncé
  await page.waitForSelector('.enonce')
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  let nbChances = params.nbchances
  if (typeof nbChances !== 'number' || nbChances < 1) {
    logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // un raccourci (async)
  const myCheckEtat = () => checkEtat(page, { numQuestion, score, total, numProgression: id })
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    // boucle essais
    while (numEssais <= nbChances) {
      await myCheckEtat()
      const rep = reponses[numQuestion - 1][numEssais - 1]
      const ok = await runOne(page, rep)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await myCheckEtat()
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe puissancesdef10_datas0.js (que l’on charge ici)
 */
export async function test (page) {
  const { graphe } = await import('./puissancesdef10_datas0.js')

  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })

  // et on lance la batterie de tests par défaut
  const { params, id } = getNodeInfos(graphe, 'puissancesdef10')
  const score = await runAll(page, params)
  await checkBilan(page, [{ id, score, nbRepetitions: params.nbrepetitions }])
  // on clique pas sur suivant car y’en a pas
  return true
}
