import { randomBool } from 'src/lib/utils/random'
import { clickFirstDialogOk, clickOk, clickSectionSuivante, clickSuite } from 'testBrowser/helpers/actions'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import MtgHandler from 'testBrowser/helpers/MtgHandler'
import { getMqChunks } from 'testBrowser/helpers/text'

/**
 * Analyse l’énoncé
 * @param page
 * @param {number} abscisse
 * @param {boolean} rep passer true pour une bonne réponse, false pour une mauvaise
 * @param {MtgHandler} figure
 * @return {Promise<boolean>} true si on a mis la bonne réponse
 */
async function answer (page, abscisse, rep, figure) {
  // le point mobile à placer sur l’axe
  const pointMobile = figure.mtg32svg.locator('circle[r="3.5"]').first()
  if (!rep) {
    if (randomBool()) {
      if (abscisse !== figure.axe.max / 2) {
        abscisse = figure.axe.max - abscisse
      } else {
        abscisse = figure.axe.max / 2 + (randomBool() ? 1 : (-1))
      }
    } else {
      abscisse = abscisse < 1 ? abscisse + 1 : abscisse - 1
    }
    await logIfVerbose(`On veut une mauvaise réponse, on choisit ${abscisse} comme abscisse.`)
  } else {
    await logIfVerbose(`On veut une bonne réponse, on a ${abscisse} comme abscisse.`)
  }
  // l’abscisse de la graduation d’arrivée (abscisse peut être correcte ou non)
  await figure.updateGraduations()
  const maGraduation = await figure.getGraduation(abscisse)
  // on déplace le point mobile
  if (typeof maGraduation !== 'string') {
    await pointMobile.dragTo(maGraduation, { force: true })
  } else {
    await logIfVerbose(`Le type de graduation cherchée était ${figure.axe.lineGraduation ? 'line' : 'circle'}`)
    await logIfVerbose(`voilà ce qui est chérché et pas trouvé : ${maGraduation}`)
    await logIfVerbose(`On n’a pas trouvé sur l’axe la graduation correspondant à l’abscisse ${abscisse} !!!`)
  }

  // c’est fini...
  await clickOk(page)
  return rep
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean|string} rep passer true pour une bonne réponse et false|string pour une mauvaise
 * @param {MtgHandler} figure
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep, figure) {
  await figure.init() // initialise essentiellement l’axe avec les bonnes valeurs pour la question...
  const eltEnonce = await page.locator('#divEnonce')
  // le latex qui contient l’abscisse à trouver
  const nombre = await eltEnonce.locator('.mq-selectable').innerText()
  await logIfVerbose(`Nombre trouvé dans l’énoncé : ${nombre}`)
  const chunks = getMqChunks(nombre)[0].replace(',', '.')
  let abscisse, numerateur
  let denominateur
  const divisions = await figure.getDivisionNumber()
  await logIfVerbose(`on compte à l’affichage ${divisions} par unité ...`)
  const boutonAugmenter = page.locator('g', { has: page.locator('text>tspan', { hasText: '⇒Augmenter le nombre d\'intervalles par unité' }) })
  const boutonDiminuer = page.locator('g', { has: page.locator('text>tspan', { hasText: '⇒Diminuer le nombre d\'intervalles par unité' }) })
  const isPossibleToGrow = await boutonAugmenter.isVisible()
  const isPossibleToDecrease = await boutonDiminuer.isVisible()
  const modificationPossible = isPossibleToDecrease && isPossibleToGrow
  if (chunks.includes('\\frac')) {
    numerateur = Number(chunks.match(/[0-9]+/g)[0])
    denominateur = Number(chunks.match(/[0-9]+/g)[1])
    abscisse = numerateur / denominateur
    for (const diviseur of [4, 3, 2]) { // on simplifie les 8, 9, 6, 4 éventuels
      if (denominateur % diviseur === 0 && numerateur % diviseur === 0) {
        numerateur = Math.round(numerateur / diviseur)
        denominateur = Math.round(denominateur / diviseur)
      }
    }
    if (denominateur === 1) {
      denominateur = 2 // on ne partage pas en 1 morceau
    }

    // il faut modifier le nombre d’intervalles par unité !
  } else {
    await logIfVerbose('donné sous la forme de decimal : ', chunks)
    abscisse = Number(chunks)
    denominateur = 10
    // numerateur = Math.round(abscisse * 10)
    const denDecimaux = {
      1: 10,
      2: 5,
      3: 10,
      4: 5,
      5: 2,
      6: 5,
      7: 10,
      8: 5,
      9: 10,
      75: 4,
      25: 4,
      333: 3,
      666: 3,
      125: 8,
      375: 8,
      625: 8,
      875: 8
    }
    if (modificationPossible) {
      const troncature = (Math.floor(abscisse * 1000) / 1000).toFixed(3).match(/\.[1-9]+/)[0] // on tronque à au plus trois chiffres après la virgule

      const partieDecimale = troncature.replace('.', '')
      if (partieDecimale == null) { // c’est un entier
        denominateur = 2
      } else {
        denominateur = Object.entries(denDecimaux).find(([key, value]) => key === partieDecimale.toString() ? value : false)[1]
        if (denominateur == null) {
          denominateur = 10
        }
      }
    }
  }
  await logIfVerbose(`... et on en veut ${denominateur}... `)
  let click = 0
  if (denominateur !== divisions && modificationPossible) {
    if (divisions < denominateur) {
      for (let i = divisions; i < denominateur; i++) {
        await logIfVerbose('On est passé dans la boucle pour augmenter les graduation pour atteindre le dénominateur', denominateur, ` repetition ${i - divisions}`)
        await boutonAugmenter.click({ timeout: 1000, force: true })
        click++
      }
    } else if (divisions > denominateur && modificationPossible) {
      for (let i = divisions; i > denominateur; i--) {
        await logIfVerbose('On est passé dans la boucle pour diminuer les graduation pour atteindre le dénominateur', denominateur, ` repetition ${i - divisions}`)
        await boutonDiminuer.click({ timeout: 1000, force: true })
      }
    }
    await logIfVerbose(`On a donc effectué ${click} clics`)
  }

  const result = await answer(page, abscisse, rep, figure)
  if (rep === true && !result) throw Error('On voulait la saisie d’une bonne réponse mais ça n’a pas été le cas')
  if (rep === false && result) throw Error('On voulait la saisie d’une mauvaise réponse mais ça n’a pas été le cas')
  // on vérifie le feedback ok/ko cohérent avec ce qu’on a saisi
  await checkFeedbackCorrection(page, result, '#MepMD')
  // y’a eu le click OK (dans answer), pas le click suite
  return result
}

/**
 * Teste toutes les répétitions de la section squelettemtg32_Validation_Interne_Param avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {string} id id du nœud dans le graphe
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, id) {
  await logIfVerbose('id dans runAll : ', id == null ? 'aucune' : id)
  // on enregistre les réponses true ou false à 50/50 dans des arrays contenant autant de valeurs que d’essais
  const reponses = []
  while (reponses.length < params.nbrepetitions) {
    const reps = []
    for (let i = 0; i < params.nbEssais; i++) {
      reps.push(randomBool())
    }
    reponses.push(reps)
  }
  // attend l’énoncé
  await page.waitForSelector('#divEnonce', { timeout: 5000 })
  // le svg de la figure mtg32
  const figure = new MtgHandler(page)
  // await figure.init() init() doit être fait à chaque nouvelle question car l’axe peut avoir changé d’une question à l’autre !
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  let nbChances = params.nbEssais
  if (typeof nbChances !== 'number' || nbChances < 1) {
    logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    const reps = reponses[numQuestion - 1]
    // boucle essais
    while (numEssais <= nbChances) {
      await logIfVerbose(`Question ${numQuestion} sur ${total}${id != null ? ' (section ' + id + ')' : ''}`)
      await checkEtat(page, { numQuestion, score, total, numProgression: id })
      const rep = reps[numEssais - 1]
      const ok = await runOne(page, rep, figure)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await checkEtat(page, { numQuestion, score, total, numProgression: id })
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe squelettemtg32ValidationInterneParam_data0.js (que l’on charge ici)
 */
export async function test (page) {
  page.setDefaultTimeout(5000)
  // d’abord un graphe à 1 seul noeud non fin
  const { graphe } = await import('./squelettemtg32ValidationInterneParam_datas0.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })
  // et on lance la batterie de tests par défaut
  const { params, id } = getNodeInfos(graphe, 'squelettemtg32_Validation_Interne_Param')
  const score = await runAll(page, params) // ne pas mettre l’id ici s’il n’y a qu’une section !!!
  await checkBilan(page, [{ id, score, nbRepetitions: params.nbrepetitions }])

  // Un autre graphe avec multiple noeuds non fin (plusieurs sections)
  const { graphe: graphe1 } = await import('./squelettemtg32ValidationInterneParam_datas1.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe: graphe1 })
  // et on lance la batterie de tests par défaut
  const { params: params1, id: id1 } = getNodeInfos(graphe1, 'squelettemtg32_Validation_Interne_Param', '1')
  await runAll(page, params1, id1)
  await clickFirstDialogOk(page)
  // on passe à la 2e section
  const { params: params2, id: id2 } = getNodeInfos(graphe1, 'squelettemtg32_Validation_Interne_Param', '2')
  await runAll(page, params2, id2)
  await clickFirstDialogOk(page)
  // on passe à la 3e section
  const { params: params3, id: id3 } = getNodeInfos(graphe1, 'squelettemtg32_Validation_Interne_Param', '3')
  const score3 = await runAll(page, params3, id3)
  await checkBilan(page, [
    { id: id3, score: score3, nbRepetitions: params3.nbrepetitions }
  ])
  return true
}
