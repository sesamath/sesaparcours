export const graphe = [
  [
    '1',
    'squelettemtg32_Validation_Interne_Param',
    [
      {
        nn: '2',
        score: 'sans+condition',
        conclusion: 'fin'
      },
      {
        ex: 'primaire/Primaire_Placer_Decimal',
        nbrepetitions: 1,
        limite: '',
        nbEssais: 1,
        a: '0',
        b: 'random',
        c: 'random',
        d: 'random',
        e: 'random',
        f: 'random',
        g: 'random',
        h: 'random',
        j: 'random',
        k: 'random',
        l: 'random',
        m: 'random',
        n: 'random',
        p: 'random',
        q: 'random',
        r: 'random',
        x: 'random',
        y: 'random',
        infoParam: 'a: valeur du premier nombre de la graduation, le dernier étant a+10, b:abscisse du point à placer'
      }
    ]
  ],
  [
    '2',
    'fin',
    [
      null
    ]
  ]
]
