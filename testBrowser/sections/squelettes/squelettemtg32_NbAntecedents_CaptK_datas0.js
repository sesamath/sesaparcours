export const graphe = [
  [
    '1',
    'squelettemtg32_NbAntecedents_Captk',
    [
      {
        pe: '>=0',
        nn: '2'
      },
      {
        f: 'NbAntecedents_Captk1',
        nbchances: 1,
        nbrepetitions: 1

      }]],
  [
    '2',
    'squelettemtg32_NbAntecedents_Captk',
    [{
      pe: '>=0',
      nn: '3'
    },
    {
      f: 'NbAntecedents_Captk2',
      nbchances: 1,
      nbrepetitions: 1
    }]],
  [
    '3',
    'squelettemtg32_NbAntecedents_Captk',
    [{
      pe: '>=0',
      nn: '4'
    },
    {
      f: 'NbAntecedents_Captk3',
      nbchances: 1,
      nbrepetitions: 1
    }]],
  [
    '4',
    'squelettemtg32_NbAntecedents_Captk',
    [{
      pe: '>=0',
      nn: '5'
    },
    {
      f: 'NbAntecedents_Captk4',
      nbchances: 1,
      nbrepetitions: 1
    }]],
  [
    '5',
    'squelettemtg32_NbAntecedents_Captk',
    [{
      pe: '>=0',
      nn: '6'
    },
    {
      f: 'NbAntecedents_Captk5',
      nbchances: 1,
      nbrepetitions: 1
    }]],
  [
    '6',
    'squelettemtg32_NbAntecedents_Captk',
    [{
      pe: '>=0',
      nn: 'fin',
      conclusion: 'fin'
    },
    { f: 'NbAntecedents_Captk6' }]]
]
