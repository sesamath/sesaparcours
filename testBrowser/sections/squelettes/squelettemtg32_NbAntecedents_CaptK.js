import { clickOk, clickSectionSuivante, clickSuite } from 'testBrowser/helpers/actions'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { randomBool } from 'src/lib/utils/random'
import MtgHandler from 'testBrowser/helpers/MtgHandler'

/**
 * cherche des polylines dans figure.mtg32svg et en fait un array de coordonnées (dans cette section, la courbe est formée de polylines successives)
 * @param page
 * @param {MtgHandler} figure
 * @returns {Promise<null|*[]>}
 */
async function getSpline (page, figure) {
  const polylines = await figure.mtg32svg.locator('polyline').all()
  if (polylines.length === 0) return null
  const spline = []
  for (const polyline of polylines) { // en espérant que dans le dom, les polylines successives se rattache bien dans l’ordre...
    const points = await polyline.getAttribute('points')
    const chunks = points.split(' ')
    chunks.pop() // Le dernier élément du split est une chaine vide... on n’en veux pas !
    for (const chunk of chunks) {
      const [x, y] = chunk.split(',').map(Number)
      if (!isNaN(x) && !isNaN(y)) {
        if (spline.length === 0 || (spline.length > 0 && spline[spline.length - 1][0] !== x)) spline.push([x, y]) // il y a deux morceaux et le dernier point du premier est le même que le premier point du deuxième, donc on évite le doublon ici
      } else {
        logIfVerbose(`L’extraction de coordonnées des polylines a donné un truc bizarre avec x = ${x} et y = ${y}`)
      }
    }
  }
  return spline
}

/**
 * retourne true si y est entre y0 et y1 inclus
 * @param y
 * @param y0
 * @param y1
 * @returns {boolean}
 */
function dansIntervalle (y, y0, y1) {
  if (y0 < y1) return y >= y0 && y <= y1
  return y >= y1 && y <= y0
}

function cleanSpline (courbe) {
  const courbeOrdonnee = courbe.sort((element1, element2) => element1[0] < element2[0] ? -1 : element1[0] > element2[0] ? 1 : 0)
  const courbeNettoyee = new Set(courbeOrdonnee)
  return Array.from(courbeNettoyee)
}
/**
 * Cherche le nombre d’intersection d’une spline (voir la fonction getSpline ci-dessus) avec la droite horizontale d’équation y=ordonnee
 * @param {[number,number][]} courbe
 * @param {number} ordonnee
 * @returns {number}
 */
function getIntersections (courbe, ordonnee) {
  const candidats = []
  let nbIntersections = 0
  const tolerance = 0.1
  const delta = 15
  for (let index = 0; index < courbe.length - 1; index++) {
    if (Math.abs(courbe[index][1] - ordonnee) < tolerance || dansIntervalle(ordonnee, courbe[index][1], courbe[index + 1][1])) candidats.push(courbe[index][0])
  }
  for (let index = 0; index < candidats.length;) {
    const x0 = candidats[index]
    nbIntersections++
    do {
      index++
    } while (candidats[index] - x0 < delta)
  }
  return nbIntersections
}

/**
 * Cherche la première ordonnée pour laquelle la fonction f dont la courbe est donnée est telle que f(x)=ordonnee admet nbSolutions par dichotomie dans la hauteur (425 ici) de l’axe
 * @param courbe
 * @param nbSolutions
 * @returns {number|null}
 */
function getOrdonnee (courbe, nbSolutions) {
  const hauteur = 425 // y2 de l’axe des ordonnées (voir si c’est toujours le même ou pas)
  const checked = (new Array(hauteur)).fill(false)
  let pas = 32
  do {
    for (let k = 0; k <= hauteur; k += pas) {
      if (!checked[k]) {
        checked[k] = true
        if (getIntersections(courbe, k) === nbSolutions) {
          logIfVerbose(`On a ${nbSolutions} pour k = ${k}`)
          return k
        }
      }
    }
    pas = pas / 2
  } while (pas >= 1)
  logIfVerbose(`getOrdonnee() n’a pas trouvé de solution pour ${nbSolutions} solutions`)
  return null
}

/**
 * Retourne le nombre maximum de solutions de l’équation f(x)=a à partir de la courbe (a variant sur la hauteur de l’axe des ordonnées)
 * ça ne fonctionne pas à cause de getIntersections() pas assez fiable :-(
 * {[number,number][]} courbe
 */
function getMaxNbSolutions (courbe) {
  let ordonnee
  let nbSolutions = 2 // il y a toujours au moins une solution donc on commence à 2 d’autant qu’un max que la fonction getOrdonnee() ne détecte pas les max (qui sont des min pour y dans le svg)
  do {
    ordonnee = getOrdonnee(courbe, nbSolutions)
    if (ordonnee != null) nbSolutions++
  } while (ordonnee != null)
  return nbSolutions - 1 // on a incrémenté une fois de trop !
}
/**
 * Analyse l’énoncé
 * @param page
 * @param {number} nombreDeSolutions
 * @param {boolean} rep passer true pour une bonne réponse, false pour une mauvaise
 * @param {MtgHandler} figure
 * @return {Promise<boolean>} true si on a mis la bonne réponse
 */
async function answer (page, nombreDeSolutions, rep, figure) {
  const pointMobile = figure.mtg32svg.locator('circle[style="stroke:rgba(255,0,0,1);opacity:1;stroke-width:1;fill:rgba(255,0,0,1);"]').first()
  const abs0 = Number(await pointMobile.getAttribute('cx'))
  const courbe = cleanSpline(await getSpline(page, figure))
  const nbMaxSolutions = getMaxNbSolutions(courbe)
  // const axeOrdonnees = page.locator('line[style="stroke-width:1;stroke:rgb(0,0,0);opacity:1;"]').first()
  if (!rep) {
    logIfVerbose(`On veut une mauvaise réponse, une position de droite qui ne coupe pas la courbe en ${nombreDeSolutions} points...`)
    const mauvaisNombreDeSolution = (nbMaxSolutions - nombreDeSolutions === nombreDeSolutions) ? nombreDeSolutions - 1 : nbMaxSolutions - nombreDeSolutions
    const ordonnee = getOrdonnee(courbe, mauvaisNombreDeSolution) // Y a-t-il des questions avec '0 solutions demandées ?'
    await pointMobile.dragTo(figure.mtg32svg, { sourcePosition: { x: 1, y: 1 }, targetPosition: { x: abs0, y: ordonnee }, force: true })
  } else {
    logIfVerbose(`On veut une bonne réponse, une position de droite qui coupe la courbe en ${nombreDeSolutions} points...`)
    const ordonnee = getOrdonnee(courbe, nombreDeSolutions)
    await pointMobile.dragTo(figure.mtg32svg, { sourcePosition: { x: 1, y: 1 }, targetPosition: { x: abs0, y: ordonnee }, force: true })
  }

  // c’est fini...
  await clickOk(page)
  return rep
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean|string} rep passer true pour une bonne réponse et false|string pour une mauvaise
 * @param {MtgHandler} figure
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep, figure) {
  await figure.init() // initialise essentiellement l’axe avec les bonnes valeurs pour la question...
  const eltEnonce = page.locator('#enonce')
  // le latex qui contient l’abscisse à trouver
  const numberString = (await eltEnonce.innerText()).match(/[0-9]+|(une seule)/)[0]
  const nombreDeSolutions = Number(numberString === 'une seule' ? 1 : numberString)
  logIfVerbose(`Nombre trouvé dans l’énoncé : ${nombreDeSolutions}`)

  const result = await answer(page, nombreDeSolutions, rep, figure)
  if (rep === true && !result) throw Error('On voulait la saisie d’une bonne réponse mais ça n’a pas été le cas')
  if (rep === false && result) throw Error('On voulait la saisie d’une mauvaise réponse mais ça n’a pas été le cas')
  // on vérifie le feedback ok/ko cohérent avec ce qu’on a saisi
  await checkFeedbackCorrection(page, result, '#MepMD')
  // y’a eu le click OK (dans answer), pas le click suite
  return result
}

/**
 * Teste toutes les répétitions de la section squelettemtg32_NbAntecedents_Captk avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {string} id id du nœud dans le graphe
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, id) {
  logIfVerbose('id dans runAll : ', id == null ? 'aucune' : id)
  // on enregistre les réponses true ou false à 50/50 dans des arrays contenant autant de valeurs que d’essais
  const reponses = []
  while (reponses.length < (params.nbrepetitions ?? 1)) {
    const reps = []
    for (let i = 0; i < (params.nbEssais ?? 1); i++) {
      // pour l’instant je fais que de la bonne réponse...
      reps.push(randomBool())
    }
    reponses.push(reps)
  }
  // attend l’énoncé
  await page.locator('#enonce').waitFor({ timeout: 5000 })
  // le svg de la figure mtg32
  const figure = new MtgHandler(page)
  // await figure.init() init() doit être fait à chaque nouvelle question car l’axe peut avoir changé d’une question à l’autre !
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 1
  }
  let nbChances = params.nbchances
  if (typeof nbChances !== 'number' || nbChances < 1) {
    logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    const reps = reponses[numQuestion - 1]
    // boucle essais
    while (numEssais <= nbChances) {
      logIfVerbose(`Question ${numQuestion} sur ${total}${id != null ? ' (section ' + id + ')' : ''}`)
      await checkEtat(page, { numQuestion, score, total, numProgression: id })
      const rep = reps[numEssais - 1]
      const ok = await runOne(page, rep, figure)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await checkEtat(page, { numQuestion, score, total, numProgression: id })
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe squelettemtg32_NbAntecedents_Captk_datas0.js (que l’on charge ici)
 *  (6 figures différentes)
 */
export async function test (page) {
  page.setDefaultTimeout(5000)
  // d’abord un graphe à 1 seul noeud non fin
  const { graphe } = await import('./squelettemtg32_NbAntecedents_CaptK_datas0.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })
  // et on lance la batterie de tests par défaut
  const { params, id } = getNodeInfos(graphe, 'squelettemtg32_NbAntecedents_Captk', '1')
  let score = await runAll(page, params, id)

  const { params: params2, id: id2 } = getNodeInfos(graphe, 'squelettemtg32_NbAntecedents_Captk', '2')
  score += await runAll(page, params2, id2)

  const { params: params3, id: id3 } = getNodeInfos(graphe, 'squelettemtg32_NbAntecedents_Captk', '3')
  score += await runAll(page, params3, id3)

  const { params: params4, id: id4 } = getNodeInfos(graphe, 'squelettemtg32_NbAntecedents_Captk', '4')
  score += await runAll(page, params4, id4)

  const { params: params5, id: id5 } = getNodeInfos(graphe, 'squelettemtg32_NbAntecedents_Captk', '5')
  score += await runAll(page, params5, id5)

  const { params: params6, id: id6 } = getNodeInfos(graphe, 'squelettemtg32_NbAntecedents_Captk', '6')
  score += await runAll(page, params6, id6)
  await checkBilan(page, [{ id: id6, score, nbRepetitions: 6 }])

  return true
}
