export const graphe = [
  [
    '1',
    'squelettemtg32_Validation_Interne_Param',
    [
      {
        nn: '2',
        score: 'sans+condition',
        conclusion: 'Fin de l‘activité'
      },
      {
        ex: 'College_Placer_Nombre_Sur_Axe_Niv1',
        nbrepetitions: 10,
        limite: '',
        nbEssais: 2,
        a: 'random',
        b: 'random',
        c: 'random',
        d: 'random',
        e: 'random',
        f: 'random',
        g: 'random',
        h: 'random',
        j: 'random',
        k: 'random',
        l: 'random',
        m: 'random',
        n: 'random',
        p: 'random',
        q: 'random',
        r: 'random',
        x: 'random',
        y: 'random'
      }
    ]
  ],
  ['2',
    'squelettemtg32_Validation_Interne_Param',
    [
      {
        nn: '3',
        score: 'sans+condition',
        conclusion: 'Fin de l‘activité'
      },
      {
        ex: 'College_Placer_Nombre_Sur_Axe_Niv2',
        nbrepetitions: 10,
        limite: '',
        nbEssais: 2,
        a: 'random',
        b: 'random',
        c: 'random',
        d: 'random',
        e: 'random',
        f: 'random',
        g: 'random',
        h: 'random',
        j: 'random',
        k: 'random',
        l: 'random',
        m: 'random',
        n: 'random',
        p: 'random',
        q: 'random',
        r: 'random',
        x: 'random',
        y: 'random'
      }
    ]
  ],
  [
    '3',
    'squelettemtg32_Validation_Interne_Param',
    [
      {
        nn: '4',
        score: 'sans+condition',
        conclusion: 'Fin de l‘activité'
      },
      {
        ex: 'College_Placer_Nombre_Sur_Axe_Niv3',
        nbrepetitions: 10,
        limite: '',
        nbEssais: 4,
        a: 'random',
        b: 'random',
        c: 'random',
        d: 'random',
        e: 'random',
        f: 'random',
        g: 'random',
        h: 'random',
        j: 'random',
        k: 'random',
        l: 'random',
        m: 'random',
        n: 'random',
        p: 'random',
        q: 'random',
        r: 'random',
        x: 'random',
        y: 'random'
      }
    ]
  ],
  [
    '4',
    'fin',
    [
      null
    ]
  ]
]
