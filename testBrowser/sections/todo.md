# ressources populaires

Exemple d’après les logs, ici le 5/3/2021
```shell
zcat /backup/agarder/weblogs/front3/front3.2021-03-05.varnishncsa.gz|sed -nre 's@.*GET http://bibliotheque.sesamath.net/public/voir/([^ ?]+).*@\1@p'|uniq -c|sort -k1 -g|tail-20
# ça sort :
    239 5e7414e9aa97764c7c2c243a
    243 268
    245 5c88094d2337d46c42223357
    248 5c97f91d4bb1527df9236b2b
    249 5c978426d9e4237dfac484f1
    272 5c4197db39510b6985be8f71
    280 5c45cf236bcd6269890711cc
    280 5f7603fb8680fc0aeec0b0f1
    286 5f82db7f8680fc0aeec0b171
    287 5b9805c52476a6514499be42
    294 5cb4ea724bb1527df9236bdf
    295 49574
    300 599bd690b4649c7d956f6167
    305 5bb08bd488a6655edd3631af
    325 5f848f118680fc0aeec0b191
    338 5d8be11d2e4d8121a8ea504d
    342 5b9e6062c0cfab652e544a96
    464 5cc564864bb1527df9236c2a
    478 5bb6024b6536ba5ee36a929e
    905 5eae13fc831fab76b79960f0
```
https://bibliotheque.sesamath.net/ressource/voir/5eae13fc831fab76b79960f0
fractioncalculs (calculer une somme de fractions pas à pas), les fractions sont dans des td, coder la résolution automatique va être coton car y’a plein de cas…

https://bibliotheque.sesamath.net/public/voir/5bb6024b6536ba5ee36a929e
squelettemtg32_Reduc_Frac.js (simplifier une fraction)

https://bibliotheque.sesamath.net/public/voir/5cc564864bb1527df9236c2a
squelettemtg32_Validation_Interne_Param (savoir placer un point sur un axe), il faut cliquer pour augmenter le nb d’intervalles par unité puis déplacer le point sur la bonne fraction

https://bibliotheque.sesamath.net/public/voir/5b9e6062c0cfab652e544a96
proportionnalite01 (Déterminer si une situation est de proportionnalité) avec listeDeroulante

https://bibliotheque.sesamath.net/public/voir/5d8be11d2e4d8121a8ea504d
proportionnalite02 (Utiliser la proportionnalité) (il faut cliquer sur les ? pour choisir une opération, puis à la question suivante remplir les cases)

https://bibliotheque.sesamath.net/public/voir/5f848f118680fc0aeec0b191
angle02 (Nature d’un angle) D’après la figure mtg affichée il faut choisir ce qui semble la nature de l’angle

# Sections populaires
