export const graphe = [
  [
    '1',
    'affineounon',
    [
      {
        pe: 'sans condition',
        nn: 'fin',
        conclusion: 'Fin'
      },
      {
        limite: '',
        indication: '',
        nbchances: 2,
        tab_ex: [1, 2, 3, 4, 5, 6],
        nb_questions: 19
      }
    ]
  ]
]
