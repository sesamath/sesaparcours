import RadioHandler from 'testBrowser/helpers/RadioHandler'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkFeedbackCorrection, checkEtat } from 'testBrowser/helpers/checks'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { clickSuite, clickSectionSuivante, clickOk } from 'testBrowser/helpers/actions'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { InputMqHandler } from 'testBrowser/helpers/InputMqHandler'
import { getMqChunks } from 'testBrowser/helpers/text'

/**
 *
 * @param page
 * @param {object} reponse Contient ouiOuNon un booléen pour la case à cocher lors du premier essai, coeffJustes un booléen pour modifier ou pas les coefficients proposés et justeSiEssai2 un booléen pour le deuxième essai.
 * @param {number} numEssais 1 ou 2
 * @param {string} A le coefficient a pour l’instant juste
 * @param {string} B idem pour le coefficient b
 * @param {boolean} isNotAffine false si la fonction est affine.
 * @returns {Promise<*>}
 */
async function answer (page, reponse, numEssais, A, B, isNotAffine) {
  const { ouiOuNon, coeffJustes, justeSiEssai2 } = reponse
  const radio1 = new RadioHandler(page)
  if ((!ouiOuNon && numEssais === 1) || (isNotAffine && ouiOuNon && numEssais === 2 && justeSiEssai2)) { // on a choisi de répondre non au premier essai ou alors, on a répondu oui sur une fonction non affine au premier et on revient pour répondre non au 2e essai
    await radio1.choose('non')
  } else { // ici, on a choisi de répondre oui à tort ou à raison on peut y revenir plusieurs fois
    if (numEssais === 1) { // c’est la première fois, on clique sur oui => ça déroule la question suivante avec le focus sur la zone de saisie de a
      await radio1.choose('oui')
      if (!coeffJustes) { // on bricole les coefficients pour qu’ils soient faux
        A += '42'
        B += '42'
      }
    } else { // on y retourne pour un deuxième essai voir plus pas besoin de cliquer sur 'oui' à nouveau...
      if (coeffJustes) { // on a déjà répondu juste, on n’aurait pas du y revenir
        logIfVerbose('Pourquoi retourner ici pour un deuxième essai puisque la réponse est déjà donnée ?')
        throw Error('On aurait du passer à la question suivante avec un Ok')
      } else {
        if (!justeSiEssai2) { // On répond faux une deuxième fois
          A += '42'
          B += '42'
        }
      }
    }
    const textAreaHandlerA = new InputMqHandler(page, { index: 0 })
    const textAreaHandlerB = new InputMqHandler(page, { index: 1 })
    await textAreaHandlerA.clear() // On nettoie les champs pour répondre juste (ou faux à nouveau)
    if (Math.random() < 0.5) {
      await textAreaHandlerA.typeClavier(Array.from(A))
    } else {
      await textAreaHandlerA.type(A)
    }
    await textAreaHandlerB.clear()
    if (Math.random() < 0.5) {
      await textAreaHandlerB.typeClavier(Array.from(B))
    } else {
      await textAreaHandlerB.type(B)
    }
  }
  await clickOk(page)
  if (numEssais === 2) {
    return justeSiEssai2
  }
  // ici on est à l’essai 1
  if (isNotAffine) {
    return !ouiOuNon
  }
  // ici la fonction est affine
  if (!ouiOuNon) return false // on a choisi de répondre non à tort
  return coeffJustes
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param page
 * @param {object} reponse Contient ouiOuNon un booléen pour la case à cocher lors du premier essai, coeffJustes un booléen pour modifier ou pas les coefficients proposés et justeSiEssai2 un booléen pour le deuxième essai.
 * @param {number} numEssais 1 ou 2
 * @param {string} A le coefficient a pour l’instant juste
 * @param {string} B idem pour le coefficient b
 * @param {boolean} isNotAffine false si la fonction est affine. * @return {Promise<boolean>}
 */
export async function runOne (page, reponse, numEssais, A, B, isNotAffine) {
  logIfVerbose('On en est à l’essai : ', numEssais)
  const result = await answer(page, reponse, numEssais, A, B, isNotAffine)
  if (result) {
    await checkFeedbackCorrection(page, result, '#MepMD')
  } else {
    if (numEssais === 1 && reponse.ouiOuNon) {
      await checkFeedbackCorrection(page, /c.est faux.*essaie encore.*./is, '#MepMD')
    } else {
      await checkFeedbackCorrection(page, /c.est faux.*Regarde la correction.*./is, '#MepMD')
    }
  }
  return result
}

/**
 * Teste toutes les répétitions de la section affineounon avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {string} [options.id] id du nœud dans le graphe
 *
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { id } = {}) {
  // on génère de l’aléatoire
  const reponses = []
  while (reponses.length < params.nb_questions) {
    reponses.push({ ouiOuNon: Math.random() < 0.5, coeffJustes: Math.random() < 0.5, justeSiEssai2: Math.random() < 0.5 })
  }
  // attend l’énoncé
  await page.waitForSelector('#MepMG:visible')
  let numQuestion = 1
  let score = 0
  let total = params.nb_questions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  let nbChances = params.nbchances
  if (typeof nbChances !== 'number' || nbChances < 1) {
    logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // un raccourci (async)
  const myCheckEtat = () => checkEtat(page, { numQuestion, score, total, numProgression: id })
  // et on y va, boucle questions
  while (numQuestion <= total) {
    // on va chercher la fonction et on définit des coefficients A et B
    const fonction = getMqChunks(await page.locator('#MepMG').locator('span').first().innerText())[1].split('=')[1]
    const isNotAffine = fonction.includes('}{x}') || (fonction.match(/sqrt\{\d?x}/) != null)
    let A, B
    if (isNotAffine) { // on détermine des a et b stupides de toute façon, la réponse est fausse si on choisit de cliquer sur oui
      A = '42'
      B = '42'
      // quoi qu’on fasse, on ne pourra pas donner des coefficient justes !
      // Mais on peut répondre non au deuxième tour si justeSiEssai2
      reponses[numQuestion - 1].coeffJustes = false
    } else { // là c’est affine donc il faut calculer a et b, vraiment !
      if (fonction.includes('^2')) { // on est dans le cas 6 :(ax+b)²-(c-ax)². A=2a(b+c) et B=b²-c²
        const nombres = fonction.match(/(-?\d)/g)
        if (nombres[0] !== nombres[4]) throw Error(`il y a un problème avec la regex sur ${fonction} avec a = ${nombres[0]} et a = ${nombres[3]}`)
        const a = nombres[0]
        const b = nombres[1]
        const c = nombres[3]
        A = (2 * Number(a) * (Number(b) - Number(c))).toString()
        B = (Number(b) ** 2 - Number(c) ** 2).toString()
      } else if (fonction.includes('sqrt')) {
        const nombres = fonction.match(/-?\d/g)
        A = 'V' + nombres[0]
        B = nombres[1] ?? '0'
      } else if (fonction.includes('frac')) {
        if (fonction.match(/\{-?\d?x-?\+?\d}/)) {
          const numAndDen = fonction.match(/\{-?\d?x-?\+?\d}|\{\d}/g)
          const numerateur = numAndDen[0].substring(1, numAndDen[0].length - 1)
          const coeffsNum = numerateur.split('x')
          const numA = coeffsNum[0].length === 0 ? 1 : (coeffsNum[0] === '-' ? -1 : Number(coeffsNum[0]))
          const numB = Number(coeffsNum[1])
          const denominateur = Number(/\d/.exec(numAndDen[1]))
          A = numA.toString() + '/' + denominateur.toString()
          B = numB.toString() + '/' + denominateur.toString()
        } else {
          const numAndDen = fonction.match(/\{-?\d?x}|\{\d}/g)
          const nombreA = numAndDen[0].substring(1, numAndDen[0].length - 2)
          const num = nombreA.length === 0 ? 1 : (nombreA === '-' ? -1 : Number(nombreA))
          const den = Number(/\d/.exec(numAndDen[1]))
          A = num.toString() + '/' + den.toString()
          const nombres = fonction.match(/-?\d/g)
          B = nombres[nombres.length - 1]
        }
      }
    }

    let numEssais = 1
    // boucle essais
    while (numEssais <= nbChances) {
      await myCheckEtat()
      const ok = await runOne(page, reponses[numQuestion - 1], numEssais, A, B, isNotAffine)
      if (numEssais === 1 && !reponses[numQuestion - 1].ouiOuNon) numEssais++ // On dit que la fonction n’est pas affine : il n’y aura pas deux essais ! soit c’est une fonction affine et cela a été dit et corrigé, soit on a eu raison et c’est juste et terminé
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await myCheckEtat()
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe priorite01_data0.js (que l’on charge ici)
 */
export async function test (page) {
  const { graphe } = await import('./affineounon_datas0.js')

  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })

  // et on lance la batterie de tests par défaut
  const { params, id } = getNodeInfos(graphe, 'affineounon')
  const score = await runAll(page, params)
  await checkBilan(page, [{ id, score, nbRepetitions: params.nb_questions }])
  // on clique pas sur suivant car y’en a pas
  return true
}
