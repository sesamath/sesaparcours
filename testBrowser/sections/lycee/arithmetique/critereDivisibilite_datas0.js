export const graphe = [
  [
    '1',
    'critereDivisibilite',
    [
      {
        pe: 'sans condition',
        nn: 'fin',
        conclusion: 'Fin'
      },
      {
        nbrepetitions: 6,
        nbchances: 1,
        listeDiviseurs: [2, 3, 5, 9],
        avecProduit: false
      }
    ]
  ]
]
