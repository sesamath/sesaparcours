import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkFeedbackCorrection, checkEtat } from 'testBrowser/helpers/checks'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { clickSuite, clickSectionSuivante, clickOk } from 'testBrowser/helpers/actions'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import CheckboxesHandler from 'testBrowser/helpers/CheckboxesHandler'
import { runTest } from 'testBrowser/run'

/**
 *
 * @param page
 * @param {boolean} rep Si on veut avoir juste, on met true, cela modifiera la conclusion pour que la correction fonctionne
 * @param {number} diviseur
 * @returns {Promise<*>}
 */
async function answer (page, rep, diviseur) {
  const listeOfNumbers = new CheckboxesHandler(page)
  const listOfChoice = await listeOfNumbers.getChoiceList()
  let nbCheckedItems = 0
  for (const option of listOfChoice) {
    if (await listeOfNumbers.isChecked(option.label)) nbCheckedItems++
  }
  if (rep) { // on doit sélectionner tous les multiples du nombre...
    for (const option of listOfChoice) {
      if (Number(option.label) % diviseur === 0) {
        if (!(await listeOfNumbers.isChecked(option.label))) {
          await listeOfNumbers.check(option.label)
          nbCheckedItems++
        }
      }
    }
  } else { // on fabrique une mauvaise réponse
    // point d’attention : il faut au moins un élément coché !
    const alea = Math.random()
    if (alea < 0.5) { // on fait l’inverse de ce qu’il faudrait Donc, là, on est certain d’avoir un élément coché sauf si tous les nombres sont des multiples du diviseur.
      for (const option of listOfChoice) {
        if (Number(option.label) % diviseur === 0) {
          if (await listeOfNumbers.isChecked()) {
            await listeOfNumbers.unCheck(option.label)
            nbCheckedItems--
          }
        } else {
          if (!(await listeOfNumbers.isChecked())) {
            await listeOfNumbers.check(option.label)
            nbCheckedItems++
          }
        }
      }
    } else { // on coche le premier non diviseur et si il n’y en a pas, de toute façon, on ne coche pas non plus les diviseurs)
      for (const option of listOfChoice) {
        if (Number(option.label) % diviseur !== 0) {
          if (!(await listeOfNumbers.isChecked())) {
            await listeOfNumbers.check(option.label)
            nbCheckedItems++
            break
          }
        }
      }
    }
    if (nbCheckedItems === 0) { // On doit faire une mauvaise réponse, mais on a rien coché ! il faut impérativement cocher une réponse juste !
      for (const option of listOfChoice) {
        if (Number(option.label) % diviseur === 0) {
          await listeOfNumbers.check(option.label)
          break
        }
      }
    }
  }
  await clickOk(page)
  return rep
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean} rep si true on fournit la bonne réponse
 * @param {number} diviseur
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep, diviseur) {
  // answer retourne rep, donc il n’y a aucun interêt à checker result
  const result = await answer(page, rep, diviseur)
  logIfVerbose(`Ce que retourne answer : ${result}`)
  if (rep === true && !result) throw Error('On voulait la saisie d’une bonne réponse mais ça n’a pas été le cas')
  if (rep === false && result) throw Error('On voulait la saisie d’une mauvaise réponse mais ça n’a pas été le cas')
  if (result) {
    await checkFeedbackCorrection(page, result, '#MepMD')
  } else {
    await checkFeedbackCorrection(page, /c.est faux.*essaie encore.*|c.est faux.*/is, '#MepMD')
  }
  await checkFeedbackCorrection(page, result, '#MepMD')
  return result
}

/**
 * Teste toutes les répétitions de la section critereDivisibilite avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {string} [options.id] id du nœud dans le graphe
 *
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { id } = {}) {
  // on génère de l’aléatoire
  const reponses = []
  while (reponses.length < params.nbrepetitions) {
    reponses.push([Math.random() < 0.5, Math.random() < 0.5])
  }

  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  const nbChances = params.nbchances ?? 1 // l’auteur de cette section n’a pas prévu de fixer ce paramètre, je l’ai fait dans les fichier de test à 1
  // un raccourci (async)
  const myCheckEtat = () => checkEtat(page, { numQuestion, score, total })
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    // boucle essais
    while (numEssais <= nbChances) {
      // attend l’énoncé
      await page.waitForSelector('#affiche_0')
      const enonce = await page.locator('#affiche_0').innerText()
      const diviseurStr = enonce.match(/\d+/)[0]
      const diviseur = Number(diviseurStr)
      await myCheckEtat()
      const rep = reponses[numQuestion - 1][numEssais - 1]
      const ok = await runOne(page, rep, diviseur)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await myCheckEtat()
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe criterDivisibilite_datas0.js (que l’on charge ici)
 */
export async function test (page) {
  const { graphe } = await import('./critereDivisibilite_datas0.js')
  await loadGraphe(page, { graphe })
  const { params, id } = getNodeInfos(graphe, 'critereDivisibilite')
  const score = await runAll(page, params)
  await checkBilan(page, [{ id, score, nbRepetitions: params.nbrepetitions }])
  // passons au deuxième graphe (avec produits)
  const { graphe: graphe1 } = await import('./critereDivisibilite_datas1.js')
  await loadGraphe(page, { graphe: graphe1 })
  // et on lance la batterie de tests par défaut
  const { params: params1, id: id1 } = getNodeInfos(graphe1, 'critereDivisibilite')
  const score1 = await runAll(page, params1)
  await checkBilan(page, [{ id: id1, score: score1, nbRepetitions: params1.nbrepetitions }])
  return true
}

runTest(test, import.meta)
