
export const graphe = [
  [
    '1',
    'signeCourbe',
    [
      {
        nn: '2',
        score: '>=0.8', // attention, ce 0.8 est en dur dans le fichier de test
        conclusion: 'Un dernier exemple'
      },
      {
        nn: '3',
        score: 'sans condition',
        conclusion: 'Fin'
      },
      {
        nbrepetitions: 3,
        nbchances: 2
      }
    ]
  ],
  [
    '2',
    'signeCourbe',
    [
      {
        nn: '3',
        pe: 'sans+condition',
        conclusion: 'Fin'
      },
      {
        zeroSansChgtSigne: true,
        nbrepetitions: 1,
        indication: '',
        limite: '',
        nbchances: 2,
        nbzero: [
          'alea',
          'alea',
          'alea'
        ]
      }
    ]
  ],
  [
    '3',
    'fin'
  ]
]
