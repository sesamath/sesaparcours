import { clickOk, clickSectionSuivante, clickSuite, clickFirstDialogOk } from 'testBrowser/helpers/actions'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { logError, logIfVerbose, logWarning } from 'testBrowser/helpers/log'
import { randomBool } from 'src/lib/utils/random'
import MtgHandler from 'testBrowser/helpers/MtgHandler'
import { InputMqHandler } from 'testBrowser/helpers/InputMqHandler'

/**
 * cherche des polylines dans figure.mtg32svg et en fait un array de coordonnées (dans cette section, la courbe est formée de polylines successives)
 * @param page
 * @param {MtgHandler} figure
 * @returns {Promise<null|*[]>}
 */
async function getSpline (page, figure) {
  const polylines = await figure.mtg32svg.locator('polyline').all()
  if (polylines.length === 0) return null
  const spline = []
  for (const polyline of polylines) { // en espérant que dans le dom, les polylines successives se rattache bien dans l’ordre...
    const points = await polyline.getAttribute('points')
    const chunks = points.split(' ')
    chunks.pop() // Le dernier élément du split est une chaine vide... on n’en veux pas !
    for (const chunk of chunks) {
      const coords = chunk.split(',')
      const x = Number(coords[0])
      const y = Number(coords[1])
      if (!isNaN(x) && !isNaN(y)) {
        if (spline.length === 0 || (spline.length > 0 && spline[spline.length - 1][0] !== x)) spline.push([x, y]) // il y a deux morceaux et le dernier point du premier est le même que le premier point du deuxième, donc on évite le doublon ici
      } else {
        logWarning(`L’extraction de coordonnées des polylines a donné un truc bizarre avec x = ${x} et y = ${y}`)
      }
    }
  }
  return spline
}

function cleanSpline (courbe) {
  const courbeOrdonnee = courbe.sort((element1, element2) => element1[0] < element2[0] ? -1 : element1[0] > element2[0] ? 1 : 0)
  const courbeNettoyee = new Set(courbeOrdonnee)
  return Array.from(courbeNettoyee)
}
/**
 * Cherche les solutions de l’équation f(x) = ordonnee d’une spline (voir la fonction getSpline ci-dessus)
 * @param {[number,number][]} courbe
 * @param {number} ordonnee
 * @returns {number[]}
 */
function getSolutions (courbe, ordonnee) {
  const candidats = []
  const tolerance = 0.01
  const delta = 4
  for (let index = 0; index < courbe.length - 1; index++) {
    if (Math.abs(courbe[index][1] - ordonnee) < tolerance) candidats.push(courbe[index][0])
  }
  const solutions = []
  for (let index = 0; index < candidats.length;) {
    // le premier peut-être d’une série qui correspond à la même solution
    const x0 = candidats[index]
    do {
      index++
    } while (candidats[index] - x0 < delta)
    const x1 = candidats[index - 1]
    // on prend le dernier qui match et on fait la moyenne avec le premier.
    solutions.push(Math.round((x0 + x1) / 2))
  }
  return solutions
}

async function completeTableau (page, valeurs, signes) {
  const boutonPlus = page.locator('input.MepBoutons2').first()
  for (let i = 0; i < valeurs.length - 2; i++) {
    await boutonPlus.click()
  }
  const nbTexteAreas = 2 * valeurs.length - 1
  for (let i = 0; i < nbTexteAreas; i++) {
    if (i < valeurs.length) {
      const texteArea = new InputMqHandler(page, { index: i })
      await texteArea.clear()
      await texteArea.triggerClavier(false)
      await texteArea.type(valeurs[i].toString())
    } else {
      const texteArea = new InputMqHandler(page, { index: i })
      await texteArea.clear()
      await texteArea.triggerClavier(false)
      await texteArea.type(signes[i - valeurs.length])
    }
  }
  const zerosZone = await page.locator('span.liste1').all()
  for (let i = 0; i < zerosZone.length; i++) {
    const zeroZoneLocator = page.locator('span.liste1>span>form>select').nth(i)
    await zeroZoneLocator.highlight()
    await zeroZoneLocator.selectOption('0')
  }
}
/**
 * Réponds (rempli les champs) et clic sur Ok
 * @param page
 * @param {boolean} rep passer true pour une bonne réponse, false pour une mauvaise
 * @param {number[]} valeurs (n valeurs)
 * @param {string[]} signes (n-1 signes)
 * @return {Promise<boolean>} true si on a mis la bonne réponse
 */
async function answer (page, rep, valeurs, signes) {
  if (rep) {
    await completeTableau(page, valeurs, signes)
    await clickOk(page)
    return true
  }
  // on met une mauvaise réponse
  const texteAreaHandlerXMin = new InputMqHandler(page, { index: 0 })
  const texteAreaHandlerXMax = new InputMqHandler(page, { index: 1 })
  const texteAreaHandlerSigne = new InputMqHandler(page, { index: 2 })
  await texteAreaHandlerXMin.clear()
  await texteAreaHandlerXMax.clear()
  await texteAreaHandlerSigne.clear()
  await texteAreaHandlerXMin.type(valeurs[1].toString())
  await texteAreaHandlerXMax.type((valeurs[2] ?? 8).toString())
  await texteAreaHandlerSigne.type(signes[0])
  await clickOk(page) // c’est pas grave, on a une deuxième chance.
  // await checkFeedbackCorrection(page, /C’est faux/, '#MepMD')
  return false
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean} rep passer true pour une bonne réponse et false pour une mauvaise
 * @param {MtgHandler} figure
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep, figure) {
  await figure.init() // initialise essentiellement l’axe avec les bonnes valeurs pour la question...
  const axeHorizontal = await figure.chercheAxeHorizontal()
  const axeVertical = await figure.chercheAxeVertical()
  // on va récupérer les points présents sur l’axe des abscisses avec abscisse svg, valeur sur l’axe, locator du point
  const abscissesPositivesGroupe = figure.mtg32svg.locator('g', { has: page.locator('g>text') }).first()
  const abscissesNegativesGroupe = figure.mtg32svg.locator('g', { has: page.locator('g>text') }).nth(1)
  const abscisses = []
  const abscissesGroupes = (await abscissesPositivesGroupe.locator('g').all()).concat(await abscissesNegativesGroupe.locator('g').all())
  for (const groupe of abscissesGroupes) {
    const x = Math.round(Number((await groupe.getAttribute('transform')).match(/[0-9.-]+/g)[0]))
    const valeur = Number((await groupe.locator('text').textContent()).replace(/\s/g, ''))
    const element = axeHorizontal.graduations.find((element) => x === element.abscisse)
    if (element != null) { // il y a des groupes avec des 'text' en dehors de l’axe qui ne matchent avec aucun point
      const locator = element.locator
      abscisses.push({ x, valeur, locator })
    }
  }
  abscisses.push({ x: axeVertical.abscisseAxe, valeur: 0, locator: figure.mtg32svg.locator('circle').first() })
  const locatorsWithValeurEtX = abscisses.filter((element) => element.x > 0 && element.x < figure.width).sort((element1, element2) => element1.x < element2.x ? -1 : element1.x > element2.x ? 1 : 0)
  // maintenant qu’on a les locatorsWithValeurEtX on va chercher les solutions de f(x)=0
  const spline = cleanSpline(await getSpline(page, figure))
  const solutions = getSolutions(spline, axeHorizontal.ordonneeAxe)
  logIfVerbose(`Les solutions de f(x) = 0 en coordonnées svg ${solutions}`)
  // l’abscisse svg du premier point de la courbe (arrondi à l’unité)
  const xMin = Math.round(spline[0][0])
  // l’abscisse svg du dernier point de la courbe (arrondi à l’unité)
  const xMax = Math.round(spline[spline.length - 1][0])
  // les valeurs de la première ligne du tableau
  logIfVerbose(`L’intervalle de définition de f : [${xMin};${xMax}]`)
  const valeurs = [locatorsWithValeurEtX.find((element) => element.x === xMin).valeur]
  const valeursSvgX = [xMin]
  for (const xi of solutions) {
    // on teste à 1 pixel près à cause des arraondis.
    const locatorWithValeur = locatorsWithValeurEtX.find((element) => Math.abs(element.x - xi) < 2)
    if (locatorWithValeur != null) {
      valeurs.push(locatorWithValeur.valeur)
      valeursSvgX.push(xi)
    } else {
      throw Error(`On n’a pas trouvé de point sur l’axe pour l’abscisse svg ${xi} d’une des solutions`)
    }
  }
  valeurs.push(locatorsWithValeurEtX.find((element) => element.x === xMax).valeur)
  valeursSvgX.push(xMax)
  const signes = []
  for (let i = 0; i < valeursSvgX.length - 1; i++) {
    const premierPointApres = spline.find((element) => element[0] - valeursSvgX[i] > 5)
    if (premierPointApres != null) {
      const ordonneePointApres = premierPointApres[1]
      if (ordonneePointApres > axeHorizontal.ordonneeAxe) signes.push('-')
      else signes.push('+')
    }
  }
  logIfVerbose(`Les solutions de f(x)=0 : ${solutions} et valeurs : ${valeurs}`)
  logIfVerbose(`Les signes successifs : ${signes.join(' | ')}`)
  const result = await answer(page, rep, valeurs, signes)
  // on vérifie le feedback ok/ko cohérent avec ce qu’on a saisi
  await checkFeedbackCorrection(page, result, '#MepMD')
  // y’a eu le click OK (dans answer), pas le click suite
  return result
}

/**
 * Teste toutes les répétitions de la section squelettemtg32_NbAntecedents_Captk avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {object} options
 * @param {string} [options.id] id du nœud dans le graphe
 * @param {number} [options.numProgression] Le n° qui indique la progression dans le graphe (noté section N après le numéro de question)
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { id, numProgression }) {
  logIfVerbose('id dans runAll : ', id == null ? 'aucune' : id)
  // on enregistre les réponses true ou false à 50/50 dans des arrays contenant autant de valeurs que d’essais
  const reponses = []
  while (reponses.length < (params.nbrepetitions ?? 1)) {
    for (let i = 0; i < (params.nbchances ?? 1); i++) {
      reponses.push([randomBool(), randomBool()])
    }
  }
  // attend l’énoncé
  const figure = new MtgHandler(page)
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 1
  }
  let nbChances = params.nbchances
  if (typeof nbChances !== 'number' || nbChances < 1) {
    logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    // boucle essais
    while (numEssais <= nbChances) {
      logIfVerbose(`Question ${numQuestion} sur ${total}${id != null ? ' (section ' + numProgression + ')' : ''}`)
      await checkEtat(page, { numQuestion, score, total, numProgression })
      const rep = reponses[numQuestion - 1][numEssais - 1]
      const ok = await runOne(page, rep, figure)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await checkEtat(page, { numQuestion, score, total, numProgression })
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe signeCourbe_datas0.js (que l’on charge ici)
 *  (6 figures différentes)
 */
export async function test (page) {
  page.setDefaultTimeout(5000)
  // d’abord un graphe à 1 seul noeud non fin
  const { graphe } = await import('./signeCourbe_datas0.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })
  // et on lance la batterie de tests par défaut
  const { params, id } = getNodeInfos(graphe, 'signeCourbe', '1')
  const score = await runAll(page, params, { id, numProgression: 1 })
  await clickFirstDialogOk(page)
  const nbRepetitions = params.nbrepetitions
  const nodeScores = [{ id, score, nbRepetitions }]
  // attention, y’a un 2e exo seulement si le score est >= 0.8
  if (score / nbRepetitions >= 0.8) {
    logIfVerbose(`y’a un 2e exo car on a eu un score de ${score} sur ${nbRepetitions} (${score / nbRepetitions} >= 0.8)`)
    const { params, id } = getNodeInfos(graphe, 'signeCourbe', '2')
    const score2 = await runAll(page, params, { id, numProgression: 2 })
    nodeScores.push({ id, score: score2, nbRepetitions: params.nbrepetitions })
  }
  // await checkBilan(page, nodeScores)

  return true
}
