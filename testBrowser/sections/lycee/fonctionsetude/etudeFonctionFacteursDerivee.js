import textesGeneriques from 'src/lib/core/textes'
import { randomBool } from 'src/lib/utils/random'

import { clickOk, clickSectionSuivante, clickSuite } from 'testBrowser/helpers/actions'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { exp, ln, InputMqHandler } from 'testBrowser/helpers/InputMqHandler'
import { getMqChunks } from 'testBrowser/helpers/text'
import { logIfVerbose } from 'testBrowser/helpers/log'

import { getModele, solveDegre2 } from './_common'

const { cBien, cFaux } = textesGeneriques

async function answer (page, rep, nbFacteursPositifs, facteurs) {
  if (nbFacteursPositifs == null) throw Error('Il faut préciser le nombre de facteurs strictement positifs !')
  if (!rep) nbFacteursPositifs++
  const divQuestion = page.locator('div', { hasText: 'Précise' })
  await divQuestion.locator('input').first().type((nbFacteursPositifs ?? 0).toString())
  for (let i = 0; i < nbFacteursPositifs; i++) {
    const factorInput = new InputMqHandler(page, { index: i })
    await factorInput.clear()
    await factorInput.typeClavier(facteurs[i])
  }
  // en cas de réponse fausse, on écrit ad+bc au numérateur au lieu de ad-bc
  await clickOk(page) // c’est pas grave, on a une deuxième chance.
  const validator = rep
    ? (feedback) => ['C’est noté !', cBien].includes(feedback)
    : (feedback) => feedback === cFaux
  await checkFeedbackCorrection(page, validator, '#MepMD')
  return rep
}

/**
 * récupère les paramètres a et b d’une chaine contenant ax+b. Ex: '-3' retournera [0,-3] et '-2x' retournera [-2,0]
 * @param texte
 * @returns {(number|number)[]|(number|number)[]}
 */
function extraireCoeffAffine (texte) {
  const [, , c, d] = extraireCoeffDeg3(texte)
  return [c, d]
}

/**
 * récupère les 4 coefficients de ax³+bx²+cx+d
 * @param texte La chaine de caractère contenant l’expression ex : '-5x^3+\frac{3}{2}x^2-6x' retournera [-5,{signe:1,num:3,den:2},-6,0]
 * @returns {(number|{num: number, den: number, signe: number}|{num: number, den: number, signe: number}|{num: number, den: number, signe: number}|number)[]}
 */
function extraireCoeffDeg3 (texte) {
  texte = texte.replaceAll(/\s/g, '')
  let a, b, c, d
  // Normalement il y a du degré 3, on commence par chercher le coefficient
  const deg3 = texte.match(/-?(\\frac)?[{}\-0-9]*x\^3/)
  let fonctionDegre2
  if (deg3 != null) {
    let monome3 = deg3[0]
    fonctionDegre2 = texte.slice(monome3.length)
    let signeA
    if (monome3[0] === '-') { // ça commence par un - donc a est négatif
      signeA = -1
      monome3 = monome3.slice(1)
    } else {
      signeA = 1
    }
    if (monome3[0] === 'x') { // coeff 1 ou -1 non écrit
      a = signeA
    } else {
      if (monome3.includes('frac')) {
        const [num, den] = monome3.match(/[0-9]+/g)
        a = { signe: signeA, num: Number(num), den: Number(den) }
      } else {
        a = signeA * monome3.match(/[0-9]/g)[0]
      }
    }
  } else {
    a = 0
    fonctionDegre2 = texte
  }
  // on passe à la suite de la fonction y a-t-il un coefficient de degré 2 ?
  const deg2 = fonctionDegre2.match(/-?\+?(\\frac)?[{}\-0-9]*x\^2/)
  let fonctionAffine
  if (deg2 != null) {
    let monome2 = deg2[0]
    fonctionAffine = fonctionDegre2.slice(monome2.length)
    let signeB
    if (monome2[0] === '-') { // ça commence par - donc b est négatif
      signeB = -1
      monome2 = monome2.slice(1)
    } else if (monome2[0] === 'x' || monome2[0] === '+' || monome2[0].match(/^[0-9]/g).length > 0) { // si pas de x³ et que b=1, alors ça commence par x sinon +
      signeB = 1
    }
    if (monome2[0] === 'x') { // coeff 1 ou -1 non écrit
      b = signeB
    } else {
      if (monome2[0] === '+') {
        monome2 = monome2.slice(1)
      }
      if (monome2.includes('frac')) {
        const [num, den] = monome2.match(/[0-9]+/g)
        b = { signe: signeB, num: Number(num), den: Number(den) }
      } else {
        b = monome2 === 'x^2' ? signeB : signeB * monome2.match(/[0-9]+/g)[0]
      }
    }
  } else {
    b = 0
    fonctionAffine = fonctionDegre2
  }
  // maintenant on passe au coefficient de degré 1
  const deg1 = fonctionAffine.match(/-?\+?(\\frac)?[{}0-9]*x/)
  let fonctionConstante
  if (deg1 != null) {
    let monome1 = deg1[0]
    fonctionConstante = fonctionAffine.slice(monome1.length)
    let signeC
    if (monome1[0] === '-') { // ça commence par - donc c est négatif
      signeC = -1
      monome1 = monome1.slice(1)
    } else if (monome1[0] === 'x' || monome1[0] === '+' || monome1[0].match(/^[0-9]/g).length > 0) { // si pas de x³ et que b=1, alors ça commence par x sinon +
      signeC = 1
    }
    if (monome1[0] === 'x') { // coeff 1 ou -1 non écrit
      c = signeC
    } else {
      if (monome1[0] === '+') {
        monome1 = monome1.slice(1)
      }
      if (monome1.includes('frac')) {
        const [num, den] = monome1.match(/[0-9]+/g)
        c = { signe: signeC, num: Number(num), den: Number(den) }
      } else {
        c = monome1 === 'x' ? signeC : signeC * Number(monome1.match(/[0-9]+/g)[0])
      }
    }
  } else {
    c = 0
    fonctionConstante = fonctionAffine
  }
  if (fonctionConstante.length === 0) {
    d = 0
  } else { // à priori d n’est pas une fraction... à vérifier !
    d = Number(fonctionConstante)
  }
  return [a, b, c, d]
}
/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean|string} rep passer true pour une bonne réponse et false|string pour une mauvaise
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep) {
  let nbFacteursPositifs, facteurs, repDerivee
  const modele = await getModele(page)
  switch (modele) {
    case 1: // (ax+b)/exp(x)
      nbFacteursPositifs = 1
      facteurs = [`${exp}x`, '3x+4']
      break
    case 2: { // ax³+bx²+cx+d
      const fonction = (await page.locator('span#affiche_1_2').innerText()).split('=')[1].split('$')[0]
      // a et b peuvent être fractionnaires ! b, c et d peuvent être nuls...
      const [, aPrime, bPrime, cPrime] = extraireCoeffDeg3(fonction)
      const [valeursInter,,] = solveDegre2(aPrime, bPrime, cPrime, true)
      if (valeursInter == null && aPrime > 0) {
        nbFacteursPositifs = 1
        facteurs = [fonction.replace('^2', '^2→'), '3x+2']
      } else {
        nbFacteursPositifs = 0
        facteurs = [fonction.replace('^2', '^2→')]
      }
    }
      break
    case 3: { // (ax+b)/(cx+d)
      const fonction = (await page.locator('span#affiche_1_2').innerText()).split('=')[1].split('$')[0].match(/(f\(x\)=\\frac{)|[\dx\-+]+|(\(-?\d?x-?\+?\d*\)\^2)/g)
      const num = fonction[0]
      const den = fonction[1]
      const [a, b] = extraireCoeffAffine(num)
      // le denominateur s’annule ! donc il n’est pas 'strictement positif'
      if (a === 0 && b > 0) {
        nbFacteursPositifs = 2
        facteurs = [num, den, '2x+5']
      } else {
        nbFacteursPositifs = 1
        facteurs = [den, '2x+5']
      }
    }
      break
    case 4: { // (ax+b)/(cx^2+d)
      const fonction = (await page.locator('span#affiche_1_2').innerText()).split('=')[1].split('$')[0].split('\\frac{')[1].split('}{')
      const num = fonction[0]
      const den = fonction[1].slice(0, -1)
      const [, a, b, c] = extraireCoeffDeg3(num)
      const [valeursInter, ,] = solveDegre2(a, b, c, true)
      if (valeursInter == null && a > 0) {
        nbFacteursPositifs = 2
        facteurs = [num.replace('^2', '^2→'), den, '3x-7']
      } else {
        nbFacteursPositifs = 1
        facteurs = [den.replace('^2', '^2→').replace(')', '→'), '3x-7']
      }
    }
      break
    case 5: { // (ax+b)exp(cx+d)
      const fonction = await page.locator('span#affiche_1_2').innerText()
      const facteurAffine = fonction.match(/=(\\left\()?-?\d*x-?\+?\d*/g)[0].replace('=', '').replace('\\left(', '')
      const exposant = fonction.match(/\{-?\d*x-?\+?\d*/g)[0].slice(1)
      nbFacteursPositifs = 1
      facteurs = [`${exp}${exposant}`, facteurAffine]
    }
      break
    case 6: { // (a+bln(x))/x^c
      const fonction = getMqChunks(await page.locator('span#affiche_1_2').innerText())[0].replaceAll(/\s/g, '')
      const [num, den] = fonction.match(/\{-?\d*\+?-?\d*\\ln\(?x\)?}|\{x\^?\d?}/g)
      const text = num.split('x')[0].slice(1).replace('\\ln', `${ln}`)
      const denPropre = den.slice(1, -1)
      const n = denPropre.includes('^') // en cas de réponse fausse on modifie l’exposant.
        ? Number(denPropre.split('^')[1])
        : 1
      nbFacteursPositifs = n % 2 === 0 ? 1 : 0
      facteurs = n % 2 === 0 ? [denPropre, text] : [text]
    }
      break
    case 7: { // ax+b+cln(dx+e)
      const fonction = (await page.locator('span#affiche_1_2').innerText()).match(/(f\(x\)=\\frac{)|[\dx\-+]+/g)
      const num = fonction[1]
      const [a, b] = extraireCoeffAffine(num)
      const den = fonction[2]
      const [d, e] = extraireCoeffAffine(den)

      if (a === 0 && b > 0) {
        nbFacteursPositifs = 2
        facteurs = [den, num, '3x+3']
      } else {
        if (d > 0) { // den est strictement positif le domaine est  ]-e/d;+inf[
          if (a > 0 && -b / a < -e / d) { // num est strictement positif
            nbFacteursPositifs = 2
            facteurs = [den, num, '3x+3']
          } else { // num n’est pas strictement positif
            nbFacteursPositifs = 1
            facteurs = [den, num]
          }
        } else { // den est strictement positif le domaine est ]-inf;-d/e[
          if (a < 0 && -b / a > -e / d) { // num est strictement positif
            nbFacteursPositifs = 2
            facteurs = [den, num, '3x+3']
          } else { // num n’est pas strictement positif
            nbFacteursPositifs = 1
            facteurs = [den, num]
          }
        }
      }
    }
      break
    case 8: { // ax²+bx+c
      const fonction = (await page.locator('span#affiche_1_2').innerText()).split('=')[1].split('$')[0]
      // La dérivée est du type ax+b, la valeur intermédiaire est -b/a
      const [aPrime, bPrime] = extraireCoeffAffine(fonction)
      const valeursInter = aPrime !== 0 ? { num: bPrime, den: aPrime, signe: -1 } : null
      if (valeursInter == null && bPrime > 0) {
        nbFacteursPositifs = 1
        facteurs = [bPrime.toString(), '3x+2']
      } else {
        nbFacteursPositifs = 0
        facteurs = ['3x+2']
      }
    }
      break
  }
  logIfVerbose(repDerivee)
  return await answer(page, rep, nbFacteursPositifs, facteurs)
}

/**
 * Teste toutes les répétitions de la section squelettemtg32_NbAntecedents_Captk avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {object} options
 * @param {string} options.numProgression N° de la section dans le graphe
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params) {
  // on enregistre les réponses true ou false à 50/50 dans des arrays contenant autant de valeurs que d’essais
  const reponses = []
  while (reponses.length < (params.nbrepetitions ?? 1)) {
    const reps = []
    for (let i = 0; i < params.nbchances ?? 2; i++) { // nbchances est à 2 pour toutes les sections
      reps.push(randomBool())
    }
    reponses.push(reps)
  }
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (!total) {
    // faut aller chercher ça dans la valeur par défaut exportée par la section
    // const { sectionParams } = await import(`${dir}section${section}.js`)
    // const nbRepetitionsParam = sectionParams.parametres.find(([nom]) => nom === 'nbrepetitions')
    // // si la section ne l’exporte pas le modèle fixe ça à 1
    // total = nbRepetitionsParam ? nbRepetitionsParam[1] : 1
    // ce qui précède fait planter playwright sur du js récent utilisé dans le code j3p, probablement à cause du wrapper esm
    // car ce js récent est largement assez vieux pour être compris par le chromium et le node utilisé
    // en attendant de passer nos tests en esm dans la v2, on force ici
    total = 1
  }
  let nbChances = params.nbchances
  if (!nbChances) {
    // idem, faut aller voir les params exportés par la section, on a du bol nos 3 sections ont la même valeur, on le met en dur ici
    // eh bien, c’est à creuser, car il semblerait que EtudeFonction_variations ne donne pas de deuxième chance :-(
    nbChances = 2
  }
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    const reps = reponses[numQuestion - 1]
    // boucle essais
    while (numEssais <= nbChances) {
      await checkEtat(page, { numQuestion, score, total })
      const rep = reps[numEssais - 1]
      const ok = await runOne(page, rep)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await checkEtat(page, { numQuestion, score, total })
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe etudeFonctionFacteursDerivee_datas0.js (que l’on charge ici)
 *  (7 fonctions différentes)
 */
export async function test (page) {
  page.setDefaultTimeout(5000)

  const { graphe } = await import('./etudeFonctionFacteursDerivee_datas0.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })
  // et on lance la batterie de tests par défaut
  const nodeScores = []
  for (const [id, section, branchements] of graphe) {
    const params = branchements[1] // le troisième élément de chaque noeud est un array avec un branchement et ensuite les params...
    if (section === 'EtudeFonction_facteursderivee') { // ne pas éxécuter de runAll sur le noeud 'Fin'...
      nodeScores.push({ id, score: await runAll(page, params), nbRepetitions: params.nbrepetitions })
    }
  }
  await checkBilan(page, nodeScores)
  return true
}
