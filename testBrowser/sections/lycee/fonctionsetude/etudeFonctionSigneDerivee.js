import { randomBool } from 'src/lib/utils/random'
import { clickFirstDialogOk, clickOk, clickSectionSuivante, clickSuite } from 'testBrowser/helpers/actions'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { exp, InputMqHandler } from 'testBrowser/helpers/InputMqHandler'
import { getMqChunks } from 'testBrowser/helpers/text'
import { getModele, solveDegre2 } from './_common'
import { extraireCoeffAffine, extraireCoeffDeg3 } from './etudeFonctionDerivee'

/**
 * Complète le tableau de signes
 * @param page
 * @param valeursEtSignes
 * @param zeros
 * @returns {Promise<void>}
 */
async function completeTableau (page, valeursEtSignes, zeros) {
  const nbInputsAttendus = valeursEtSignes.length
  // 3 valeurs sur la première ligne et ensuite 3 lignes avec 2 signes à chaque fois.
  let nbTexteAreas = (await page.locator('div.kbdContainer').all()).length
  const boutonPlus = page.locator('input.MepBoutons2').first()
  // ici on diminue le nombre d’input (quand une précédente  mauvaise réponse a créé des inputs inutiles il faut les supprimer
  if (nbTexteAreas > nbInputsAttendus) {
    // Le bouton moins n’est là que si il y a des valeurs qui ont été ajouté sur la ligne
    const boutonMoins = page.locator('input.MepBoutons2').nth(1)
    while (nbTexteAreas > nbInputsAttendus) {
      await boutonMoins.click()
      nbTexteAreas = (await page.locator('div.kbdContainer').all()).length
    }
  }
  // on augmente ici le nombre de valeurs présentes sur la première ligne afin de pouvoir compléter correctement le tableau
  while (nbTexteAreas < nbInputsAttendus) {
    await boutonPlus.click()
    nbTexteAreas = (await page.locator('div.kbdContainer').all()).length
  }
  await page.locator('div#MepHG').click()
  for (let i = 0; i < nbTexteAreas; i++) {
    const texteArea = new InputMqHandler(page, { index: i })
    // Le signe - utilisé sur le clavier et celui qui est affiché n’ont pas le même charCode, il faut donc remplacer celui-ci avant d’effectuer la comparaison.
    const contenuPrecedent = (await texteArea.locator.innerText()).replace(String.fromCharCode(8722), '-')
    if (contenuPrecedent !== valeursEtSignes[i]) { // si la valeur est déjà dedans, de toute façon, le champ ne permet pas la saisie
      await texteArea.clear()
      if (valeursEtSignes[i].includes('∞')) {
        await texteArea.typeClavier(valeursEtSignes[i])
      } else {
        await texteArea.typeClavier(valeursEtSignes[i])
      }
    }
  }
  for (let i = 0; i < zeros.length; i++) {
    const zero = page.locator('span.liste1>span>form>select').nth(i)
    if (zeros[i].actif) {
      await zero.selectOption(zeros[i].option)
    }
  }
}

async function answer (page, rep, valeurs, signes, zeros) {
  await completeTableau(page, valeurs.concat(signes), zeros)
  await clickOk(page) // c’est pas grave, on a une deuxième chance.
  await checkFeedbackCorrection(page, rep, '#MepMD')
  return rep
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean|string} rep passer true pour une bonne réponse et false|string pour une mauvaise
 * @return {Promise<boolean>}
 */
async function runOne (page, rep) {
  let valeurs = []
  let signes = []
  let zeros = []
  const modele = await getModele(page)
  switch (modele) {
    case 1: { // (ax+b)/exp(x)
      const fonction = (await page.locator('span#affiche_0_2').innerText()).match(/(f\(x\)=)|[\dx\-+]+/g)
      const [a, b] = extraireCoeffAffine(fonction[1])
      const aPlusB = rep ? Number(a) + Number(b) : Number(a) + Number(b) + 1 // on change la valeur du zéro
      const signe = -aPlusB / a > 0 ? '' : '-'
      const zero = Number.isInteger(-aPlusB / a) ? `${(-aPlusB / a).toString()}` : `${signe}${Math.abs(-aPlusB).toString()}/${Math.abs(a).toString()}`
      valeurs = ['-∞', zero, '+∞']
      signes = a > 0 ? ['-', '+', '+', '+', '-', '+'] : ['+', '-', '+', '+', '+', '-']
      zeros = [{ actif: true, option: '0' }, { actif: true, option: '|' }, { actif: true, option: '0' }]
    }
      break
    case 2: { // ax³+bx²+cx+d
      // pour la réponse fausse on ne trafique pas les coefficients de la dérivée, c’est trop dangereux, on faussera la résolution de l’équation du second degré.
      const fonction = (await page.locator('span#affiche_1_2').innerText()).split('=')[1].split('$')[0]
      // a et b peuvent être fractionnaires ! b, c et d peuvent être nuls...
      const [, aPrime, bPrime, cPrime] = extraireCoeffDeg3(fonction)
      // étudions le signe de la dérivée
      const [valeursInter, signesInter, zerosInter] = solveDegre2(aPrime, bPrime, cPrime, rep)
      if (valeursInter != null) {
        valeurs = ['-∞', ...valeursInter, '+∞']
      } else {
        valeurs = ['-∞', '+∞']
      }
      signes = [...signesInter]
      if (zerosInter == null) {
        zeros = []
      } else {
        zeros = [...zerosInter]
      }
    }
      break
    case 3: { // (ax+b)/(cx+d)
      const fonction = (await page.locator('span#affiche_0_2').innerText()).match(/(f\(x\)=\\frac{)|[\dx\-+]+/g)
      const num = fonction[1]
      const den = fonction[2]
      const [a, b] = extraireCoeffAffine(num)
      const [c, d] = extraireCoeffAffine(den)
      let numOk, signesOk
      if (rep) {
        numOk = true
        signesOk = true
      } else { // si c’est faux, soit on trafique le numérateur, soit on trafique les signes, mais pas les deux sinon deux erreurs pourraient rendre juste le tableau !
        if (Math.random() < 0.5) {
          numOk = false
          signesOk = true
        } else {
          numOk = true
          signesOk = false
        }
      }
      const numDerivee = numOk ? a * d - b * c : b * c - a * d // On change le signe du numérateur afin de changer le signe de la dérivée
      let zeroDen
      if (Number.isInteger(d / c)) {
        zeroDen = (-d / c).toString()
      } else {
        zeroDen = d / c > 0 ? `-${Math.abs(d).toString()}/${Math.abs(c).toString()}` : `${Math.abs(d).toString()}/${Math.abs(c).toString()}`
      }

      signes = signesOk ? (numDerivee > 0 ? ['+', '+', '+', '+', '+', '+'] : ['-', '-', '+', '+', '-', '-']) : (numDerivee > 0 ? ['-', '-', '+', '+', '-', '-'] : ['+', '+', '+', '+', '+', '+'])
      valeurs = ['-∞', zeroDen, '+∞']
      zeros = [{ actif: true, option: '|' }, { actif: true, option: '0' }, { actif: true, option: '||' }]
    }
      break
    case 4: { // (ax+b)/(cx^2+d) // il faudra peut-être gérer la discontinuité en sqrt(-d/c) si -d/c peut être nul ! à priori, ce n’est pas le cas, ça voudrait dire que d est nul
      const fonction = (await page.locator('span#affiche_0_2').innerText()).match(/{-?\d*x\^?\d?-?\+?\d*}/g)
      const num = fonction[0].slice(1).slice(0, -1)
      const den = fonction[1].slice(1).slice(0, -1)
      const [a, b] = extraireCoeffAffine(num)
      const [, c, d, e] = extraireCoeffDeg3(den)
      const aPrime = (-a * c)
      const bPrime = rep ? -2 * b * c : -2 * b * c + 1
      const cPrime = rep ? a * e - b * d : a * e - b * d + 1
      const [valeursNum, signesNum, zerosNum] = solveDegre2(aPrime, bPrime, cPrime, rep)
      if (valeursNum != null) {
        valeurs = ['-∞', ...valeursNum, '+∞']
      } else {
        valeurs = ['-∞', '+∞']
      }
      signes = []
      signes = signes.concat(signesNum).concat((new Array(signesNum.length)).fill('+')).concat(signesNum)
      zeros = []
      zeros = (zeros.concat(zerosNum).concat((new Array(zerosNum.length)).fill({
        actif: true,
        option: '|'
      })).concat(zerosNum))
    }
      break
    case 5: { // (ax+b)exp(cx+d)
      const fonction = (await page.locator('span#affiche_1_2').innerText()).split('=')[2].replaceAll(/\s/g, '').replaceAll('−', '-')
      const chunk = fonction.match(/-?\d*x-?\+?\d*/g)[0]
      let [aPrime, bPrime] = extraireCoeffAffine(chunk)
      aPrime = rep ? aPrime : aPrime * 7
      bPrime = rep ? bPrime : (bPrime === 0 ? 1 : 0)
      const signe = -bPrime / aPrime > 0 ? '' : '-'
      const zero = Number.isInteger(-bPrime / aPrime) ? `${(-bPrime / aPrime).toString()}` : `${signe}${Math.abs(bPrime).toString()}/${Math.abs(aPrime).toString()}`
      valeurs = ['-∞', zero, '+∞']
      signes = aPrime > 0 ? ['-', '+', '+', '+', '-', '+'] : ['+', '-', '+', '+', '+', '-']
      zeros = [{ actif: true, option: '0' }, { actif: true, option: '|' }, { actif: true, option: '0' }]
    }
      break
    case 6: { // (a+bln(x))/x^c dérivable sur [0, +infty[ seulement
      const fonction = getMqChunks(await page.locator('span#affiche_1_2').innerText())[0].replaceAll(/\s/g, '')
      const [num] = fonction.match(/\{-?\d*\+?-?\d*\\ln\(?x\)?}|\{x\^?\d?}/g)
      const text = num.split('\\ln')[0].slice(1)
      const coeffs = text.match(/-?\+?\d*/g)
      let a, b
      if (coeffs.length > 2) {
        a = Number(coeffs[0])
        if (coeffs[1] === '-') b = -1
        else if (coeffs[1] === '+') b = 1
        else b = Number(coeffs[1])
      } else if (coeffs.length !== 0) {
        a = 0
        if (coeffs[0] === '-') b = -1
        else if (coeffs[0] === '+') b = 1
        else b = Number(coeffs[0])
      } else {
        a = 0
        b = 1
      }
      // la dérivée est : (a+b*lnx)/x^n
      // comme x>0, x^(n+1)>0 sur tout l’intervalle
      // on doit déterminer le zéro du numérateur à savoir : exp(-a/b)
      const signeExp = -a / b > 0 ? '' : '-'
      const zero = a === 0
        ? '1'
        : Number.isInteger(a / b) // il est impératif de simplifier la valeur de l’exponentielle si possible
          ? `${exp}${(-a / b).toString()}`
          : `${exp}${signeExp}${Math.abs(a).toString()}/${Math.abs(b).toString()}`
      valeurs = ['0', zero, '+∞']
      zeros = [{ actif: true, option: '0' }, { actif: true, option: '|' }, { actif: true, option: '0' }]
      signes = b > 0
        ? rep ? ['-', '+', '+', '+', '-', '+'] : ['+', '-', '+', '+', '+', '-']
        : rep ? ['+', '-', '+', '+', '+', '-'] : ['-', '+', '+', '+', '-', '+']
    }
      break
    case 7: { // ax+b+cln(dx+e) On a un domaine de définition à trouver !!! il faut que le dénominateur de la dérivée soit positif car il est à l’interieur du ln() dans la fonction !
      const fonction = (await page.locator('span#affiche_1_2').innerText()).match(/(f.\(x\)=\\frac{)|[\dx\-+]+/g)
      const num = fonction[1]
      const den = fonction[2]
      const [a, b] = extraireCoeffAffine(num)
      const [c, d] = extraireCoeffAffine(den)
      const x1 = -b / a
      const x2 = -d / c
      // on définit les valeurs de la première ligne...
      let signeNum
      if (isNaN(x2)) {
        if (d < 0) throw Error(`C’est impossible, la fonction n’est pas définie car ln(${d}) n’existe pas !`)
        // Je pense qu’on ne passera jamais ici si la section évite un ln(constante)...
        if (!isNaN(x1)) {
          valeurs = ['-∞', Number.isInteger(x1) ? x1.toString() : (x1 > 0 ? '' : '-') + Math.abs(d).toString() + '/' + Math.abs(c).toString(), '+∞']
        } else { // ah non, il n’y en a pas ??? ça veut dire que f'(x) = b/d une constante !
          valeurs = ['-∞', '+∞']
        }
        // par contre les choses sérieuses commencent ici !
      } else { // on doit trouver le domaine de définition et vérifier si x1 est dedans !!!
        const moinsDSurC = `${-d / c > 0 ? '' : '-'}${Number.isInteger(d / c) ? Math.abs(d / c).toString() : Math.abs(d).toString() + '/' + Math.abs(c).toString()}`
        if (c > 0) { // cx+d n’est positif qu'à partir de -d/c
          if (!isNaN(x1)) {
            if (x1 > -d / c) {
              valeurs = [moinsDSurC, Number.isInteger(x1) ? x1.toString() : (x1 > 0 ? '' : '-') + Math.abs(b).toString() + '/' + Math.abs(a).toString(), '+∞']
            } else {
              signeNum = a > 0 ? '+' : '-'
              valeurs = [moinsDSurC, '+∞']
            }
          } else { // pas de x1, ça veut dire que le numérateur est constant et ne s’annule pas
            signeNum = b > 0 ? '+' : '-'
            valeurs = [moinsDSurC, '+∞']
          }
        } else { // cx+d n’est positif qu’avant -d/c
          if (!isNaN(x1)) {
            if (x1 < -d / c) {
              valeurs = ['-∞', Number.isInteger(x1) ? x1.toString() : (x1 > 0 ? '' : '-') + Math.abs(b).toString() + '/' + Math.abs(a).toString(), moinsDSurC]
            } else {
              valeurs = ['-∞', moinsDSurC]
              signeNum = a > 0 ? '-' : '+'
            }
          } else { // pas de x1, ça veut dire que le numérateur est constant et ne s’annule pas
            valeurs = ['-∞', moinsDSurC]
            signeNum = b < 0 ? '-' : '+'
          }
        }
      }
      // ensuite les signes
      if (valeurs.length === 3) { // le numérateur s’annule en x1 le dénominateur est toujours positif !
        signes = rep ? [a > 0 ? '-' : '+', a > 0 ? '+' : '-'] : [a < 0 ? '-' : '+', a < 0 ? '+' : '-']
        signes = signes.concat(['+', '+'])// on va mettre les bons signes pour le dénominateur, de toute façon, ceux du dessus sont faux
        signes = signes.concat(rep ? [a > 0 ? '-' : '+', a > 0 ? '+' : '-'] : [a < 0 ? '-' : '+', a < 0 ? '+' : '-'])
        zeros = [{ actif: true, option: '0' }, { actif: true, option: '|' }, { actif: true, option: '0' }]
      } else { // x1 n’est pas dans l’intervalle, la fonction est du signe du numérateur sur l’intervalle.
        signes = [signeNum, rep ? '+' : '-', signeNum]
        zeros = []
      }
    }
      break
    case 8: { // ax²+bx+c
      const fonction = (await page.locator('span#affiche_1_2').innerText()).split('=')[1].split('$')[0]
      // a et b peuvent être fractionnaires ! b, c et d peuvent être nuls...
      let [aPrime, bPrime] = extraireCoeffAffine(fonction)
      // étudions le signe de la dérivée
      let numOk, signesOk
      if (rep) {
        numOk = true
        signesOk = true
      } else { // si c’est faux, soit on trafique le numérateur, soit on trafique les signes, mais pas les deux sinon deux erreurs pourraient rendre juste le tableau !
        if (Math.random() < 0.5) {
          numOk = false
          signesOk = true
        } else {
          numOk = true
          signesOk = false
        }
      }
      // une fois sur 2 on trafique la valeur de bPrime et pas les signes
      bPrime = numOk ? bPrime : bPrime + aPrime
      const signe = -bPrime / aPrime > 0 ? '' : '-'
      const zero = Number.isInteger(-bPrime / aPrime) ? `${(-bPrime / aPrime).toString()}` : `${signe}${Math.abs(bPrime).toString()}/${Math.abs(aPrime).toString()}`
      valeurs = ['-∞', zero, '+∞']
      // une fois sur 2 on trafique les signes
      signes = aPrime > 0
        ? signesOk
          ? ['-', '+']
          : ['+', '-']
        : signesOk
          ? ['+', '-']
          : ['-', '+']
      zeros = [{ actif: true, option: '0' }]
    }
      break
  }
  return await answer(page, rep, valeurs, signes, zeros)
}

/**
 * joue la(les) répétition(s) et retourne le résultat et la fonction étudiée lors de la dernière répétition, donc à n’utiliser qu’avec une seule répétition pour enchainer une autre section avec la même fonction
 * @param page
 * @param params
 * @param numProgression
 * @returns {Promise<(number|*)[]>}
 */
export async function runAllAvecFonction (page, params, { numProgression }) {
  return [await runAll(page, params, { numProgression }), await page.locator('span#affiche_0_2').innerText()]
}

/**
 * Teste toutes les répétitions de la section squelettemtg32_NbAntecedents_Captk avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {object} options
 * @param {string} options.numProgression N° de la section dans le graphe
 * @return {Promise<number>} Le score
 */
async function runAll (page, params, { numProgression }) {
  // on enregistre les réponses true ou false à 50/50 dans des arrays contenant autant de valeurs que d’essais
  const reponses = []
  while (reponses.length < (params.nbrepetitions ?? 1)) {
    const reps = []
    for (let i = 0; i < params.nbchances ?? 2; i++) { // nbchances est à 2 pour toutes les sections
      reps.push(randomBool())
    }
    reponses.push(reps)
  }
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (!total) {
    // faut aller chercher ça dans la valeur par défaut exportée par la section
    // const { sectionParams } = await import(`${dir}section${section}.js`)
    // const nbRepetitionsParam = sectionParams.parametres.find(([nom]) => nom === 'nbrepetitions')
    // // si la section ne l’exporte pas le modèle fixe ça à 1
    // total = nbRepetitionsParam ? nbRepetitionsParam[1] : 1
    // ce qui précède fait planter playwright sur du js récent utilisé dans le code j3p, probablement à cause du wrapper esm
    // car ce js récent est largement assez vieux pour être compris par le chromium et le node utilisé
    // en attendant de passer nos tests en esm dans la v2, on force ici
    total = 1
  }
  let nbChances = params.nbchances
  if (!nbChances) {
    // idem, faut aller voir les params exportés par la section, on a du bol nos 3 sections ont la même valeur, on le met en dur ici
    // eh bien, c’est à creuser, car il semblerait que EtudeFonction_variations ne donne pas de deuxième chance :-(
    nbChances = 2
  }
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    const reps = reponses[numQuestion - 1]
    // boucle essais
    while (numEssais <= nbChances) {
      await checkEtat(page, { numQuestion, score, total, numProgression })
      const rep = reps[numEssais - 1]
      const ok = await runOne(page, rep)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await checkEtat(page, { numQuestion, score, total, numProgression })
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe etudeFonctionSigneDerivee_datas0.js (que l’on charge ici)
 *  (7 fonctions différentes)
 */
export async function test (page) {
  page.setDefaultTimeout(5000)

  const { graphe } = await import('./etudeFonctionSigneDerivee_datas0.js')
  const numProgressionMax = 8

  // on charge ce graphe dans la page couranteœ
  await loadGraphe(page, { graphe })
  // et on lance la batterie de tests par défaut
  const nodeScores = []
  let numProgression = 1
  for (const [id, section, branchements] of graphe) {
    const params = branchements[1] // le troisième élément de chaque noeud est un array avec un branchement et ensuite les params...
    if (section === 'EtudeFonction_signederivee') { // ne pas éxécuter de runAll sur le noeud 'Fin'...
      nodeScores.push({
        id,
        score: await runAll(page, params, { numProgression }),
        nbRepetitions: params.nbrepetitions
      })
      numProgression++
      if (numProgression <= numProgressionMax) {
        await clickFirstDialogOk(page)
      }
    }
  }
  await checkBilan(page, nodeScores)
  return true
}
