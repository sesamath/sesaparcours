import { pgcd } from 'src/lib/utils/number'

/**
 * prends les coefficients d’un polynome de degré 2 (ou moins), et retourne un objet contenant les racines, les signes successifs et ce qu’il faut saisir comme valeur en fonction de rep (inversion des signes si false)
 * @param {number|{signe: number, num: number, den:number}} a
 * @param {number|{signe: number, num: number, den:number}} b
 * @param {number|{signe: number, num: number, den:number}} c
 * @param {boolean} rep
 * @returns {(*[]|string[])[]}
 */
export function solveDegre2 (a, b, c, rep) {
  let result
  if (a !== 0) {
    let delta
    if (typeof a === 'number' && typeof b === 'number' && typeof c === 'number') {
      delta = b ** 2 - 4 * a * c
    } else {
      // On uniformise les coeffs en fraction et on calcule delta... en espérant un résultat entier !
      const bCarreFrac = typeof b === 'number' ? { num: b * b, den: 1 } : { num: b.num * b.num, den: b.den * b.den }
      const aFrac = typeof a === 'number' ? { num: a, den: 1 } : { num: a.signe * a.num, den: a.den }
      const cFrac = typeof c === 'number' ? { num: c, den: 1 } : { num: c.signe * c.num, den: c.den }
      const gcd = pgcd(Math.abs(4 * aFrac.num * cFrac.num), Math.abs(aFrac.den * cFrac.den))
      const moins4ACFrac = { num: (-4 * aFrac.num * cFrac.num) / gcd, den: (aFrac.den * cFrac.den) / gcd }
      const denComm = bCarreFrac.den ** 2 * moins4ACFrac.den
      const numComm = bCarreFrac.num * moins4ACFrac.den - bCarreFrac.den * moins4ACFrac.num
      const gcd2 = pgcd(Math.abs(numComm), Math.abs(denComm))
      const deltaFrac = { num: numComm / gcd2, den: denComm / gcd2 }
      delta = deltaFrac.den === 1 ? deltaFrac.num : deltaFrac.num / deltaFrac.den
    }
    // en cas de réponse fausse on inverse les signes
    if (delta < 0) { // pas de solution qui annule la dérivée, elle est monotone du signe de a
      result = [null, [a > 0 ? (rep ? '+' : '-') : (rep ? '-' : '+')], null]
    } else if (delta > 0) {
      // Pour l’instant et jusqu'à preuve du contraire, je pense que les racines sont entières...
      const x1 = Math.round((-b - Math.sqrt(delta)) / (2 * a))
      const x2 = Math.round((-b + Math.sqrt(delta)) / (2 * a))
      result = [[Math.min(x1, x2).toString(), Math.max(x1, x2).toString()], a < 0 ? (rep ? ['-', '+', '-'] : ['+', '-', '+']) : (rep ? ['+', '-', '+'] : ['-', '+', '-']), [{
        actif: true,
        option: '0'
      }, { actif: true, option: '0' }]]
    } else {
      const x1 = Math.round((-b - Math.sqrt(delta)) / (2 * a))
      result = [[x1.toString()], a < 0 ? (rep ? ['-', '+'] : ['+', '-']) : (rep ? ['+', '-'] : ['-', '+']), [{
        actif: true,
        option: '0'
      }]]
    }
  } else { // l’équation du second degré est du premier degré voir moins !
    if (typeof b === 'number') {
      if (b !== 0) {
        result = [[(-c / b).toString()], b < 0 ? (rep ? ['+', '-'] : ['-', '+']) : (rep ? ['-', '+'] : ['+', '-']), [{
          actif: true,
          option: '0'
        }]]
      } else { // c’est une fonction constante
        result = [null, c < 0 ? (rep ? ['-'] : ['+']) : (rep ? ['+'] : ['-']), null]
      }
    } else { // b est rationnel !
      if (typeof c === 'number') {
        result = [[(-c * b.den * b.signe / b.num).toString()], b.signe < 0 ? (rep ? ['+', '-'] : ['-', '+']) : (rep ? ['-', '+'] : ['+', '-']), [{
          actif: true,
          option: '0'
        }]]
      } else { // b et c rationnels
        result = [[(-c.num * c.signe * b.den * b.signe / b.num / c.den).toString()], b.signe < 0 ? (rep ? ['+', '-'] : ['-', '+']) : (rep ? ['-', '+'] : ['+', '-']), [{
          actif: true,
          option: '0'
        }]]
      }
    }
  }
  return result
}

export async function getModele (page) {
  const fonction = (await page.locator('span#affiche_0_2').innerText()).split('=')[1]
  if (fonction.includes('ln')) {
    if (fonction.includes('frac')) return 6
    else return 7
  } else if (fonction.includes('e')) {
    const chunks = fonction.split('{e}')
    if (chunks[1].includes('^x$')) return 1
    else return 5
  } else if (fonction.includes('^3')) return 2
  else {
    if (fonction.includes('^2')) {
      if (fonction.includes('frac')) return 4
      else return 8
    } else return 3
  }
}
