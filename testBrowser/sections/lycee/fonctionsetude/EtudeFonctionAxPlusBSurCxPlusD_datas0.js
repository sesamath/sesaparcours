export const graphe = [
  [
    '1',
    'EtudeFonction_derivee',
    [
      {
        nn: '2',
        pe: 'sans condition',
        conclusion: 'Etudions alors le signe de la dérivée'
      },
      {
        imposer_fct: '',
        modele: [3],
        nbrepetitions: 1,
        nbchances: 2
      }
    ]
  ],
  [
    '2',
    'EtudeFonction_signederivee',
    [
      {
        nn: '3',
        pe: 'sans condition',
        conclusion: 'Passons aux variations de la fonction'
      },
      {
        imposer_fct: 'j3p.parcours.donnees[' + '1]',
        nbchances: 2
      }
    ]
  ],
  [
    '3',
    'EtudeFonction_variations',
    [
      {

        nn: '4',
        pe: 'sans condition',
        conclusion: 'Fin de l’exercice'
      },
      {
        imposer_fct: 'j3p.parcours.donnees[' + '1]',
        limites_a_trouver: false,
        nbchances: 1
      }
    ]
  ],
  [
    '4',
    'fin',
    [
      null]
  ]
]
