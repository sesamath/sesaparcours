import { clickOk, clickSectionSuivante, clickSuite, clickFirstDialogOk } from 'testBrowser/helpers/actions'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { InputMqHandler } from 'testBrowser/helpers/InputMqHandler'
import { randomBool } from 'src/lib/utils/random'

// toutes nos sections sont là-dedans, cette constante évite de passer par adresses.js
// const dir = 'legacy/sections/lycee/fonctionsetude/'

async function completeTableau (page, valeurs, signes) {
  let deuxiemeEssai = false
  // 3 valeurs sur la première ligne et ensuite 3 lignes avec 2 signes à chaque fois.
  let nbTexteAreas = (await page.locator('div.kbdContainer').all()).length
  if (nbTexteAreas !== 9) { // au départ il n’y a pas de valeur intermédiaire sur la première ligne, on l’ajoute alors
    const boutonPlus = page.locator('input.MepBoutons2').first()
    for (let i = 0; i < valeurs.length - 2; i++) {
      await boutonPlus.click()
    }
  } else { // ça veut dire qu’on est sur un deuxième essai et que le nombre de zone de saisie est correct, mais il faut enlever le focus de la zone de saisie
    await page.locator('div#MepHG').click()
    deuxiemeEssai = true
  }
  // on recompte combien il y en a
  nbTexteAreas = (await page.locator('div.kbdContainer').all()).length
  if (nbTexteAreas !== 9) {
    logError(`On devrait avoir 9 texteAreas et on n’en a que ${nbTexteAreas} !`)
  }
  for (let i = 0; i < nbTexteAreas; i++) {
    if (i < valeurs.length) {
      if (!deuxiemeEssai) { // lors du deuxième essai, ces valeurs sont désactivées
        const texteArea = new InputMqHandler(page, { index: i })
        await texteArea.clear()
        await texteArea.triggerClavier(true)
        await texteArea.typeClavier(valeurs[i])
      }
    } else {
      const texteArea = new InputMqHandler(page, { index: i })
      if ((i % 4) % 3 !== 0) { // signes du dénominateur au carré ça correspond au zone N° 5 et 6 : on exclu les zones 3,4,7,8 qui dépendent du signe du numérateur
        if (!deuxiemeEssai) { // si c’est le deuxième essai, ces zones sont désactivées car on a répondu correctement dedans
          await texteArea.clear()
          await texteArea.type('+')
        }
      } else {
        await texteArea.clear()
        await texteArea.type(signes[0]) // c’est monotone, donc c’est tout le temps le signe du numérateur
      }
    }
  }
  const zero = page.locator('span.liste1>span>form>select').nth(1)
  const nonZeroNum = page.locator('span.liste1>span>form>select').nth(0)
  const nonZeroQuotient = page.locator('span.liste1>span>form>select').nth(2)
  if (!deuxiemeEssai) await zero.selectOption('0') // ça aussi c’est désactivé au deuxième essai
  await nonZeroNum.selectOption('|')
  await nonZeroQuotient.selectOption('||')
}
// variables globales car la fonction est reprise lors des trois étapes (trois sections différentes)
let a, b, c, d, numDerivee, zeroDen

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean|string} rep passer true pour une bonne réponse et false|string pour une mauvaise
 * @param {'derivee' | 'signes' | 'variations'} etape
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep, etape) {
  if (etape === 'derivee') {
    const fonction = (await page.locator('span#affiche_0_2').innerText()).match(/(f\(x\)=\\frac{)|[\dx\-+]+/g)
    const num = fonction[1]
    const den = fonction[2]
    if (!num.includes('x')) {
      a = 0
      b = Number(num)
    } else {
      const aEtB = num.split('x')
      if (aEtB[0] === '') {
        a = 1
      } else if (aEtB[0] === '-') {
        a = -1
      } else {
        a = Number(aEtB[0])
      }
      if (aEtB[1] === '') {
        b = 0
      } else {
        b = Number(aEtB[1])
      }
    }
    if (!den.includes('x')) {
      c = 0
      d = Number(den)
    } else {
      const cEtD = den.split('x')
      if (cEtD[0] === '') {
        c = 1
      } else if (cEtD[0] === '-') {
        c = -1
      } else {
        c = Number(cEtD[0])
      }
      if (cEtD[1] === '') {
        d = 0
      } else {
        d = Number(cEtD[1])
      }
    }

    numDerivee = a * d - b * c
    // denDerivee ne sert qu'à l’écriture de la dérivée, de toute façon, c’est un carré donc toujours positif.
    const denDerivee = `(${c === 0 ? '' : c === '1' ? 'x' : c === '-1' ? '-x' : c.toString() + 'x'}${d === 0 ? '' : d > 0 ? '+' + d.toString() : d.toString()})^2`
    const texteArea = new InputMqHandler(page, { index: 0 })
    await texteArea.clear()
    // en cas de réponse fausse, on écrit ad+bc au numérateur au lieu de ad-bc
    await texteArea.type((rep ? numDerivee.toString() : (numDerivee + 2 * b * c).toString()) + '/' + denDerivee)
    await clickOk(page) // c’est pas grave, on a une deuxième chance.
    await checkFeedbackCorrection(page, rep, '#MepMD')
  } else if (etape === 'signes') {
    const signes = rep ? (numDerivee > 0 ? ['+', '+'] : ['-', '-']) : (numDerivee > 0 ? ['-', '-'] : ['+', '+'])
    zeroDen = c / d > 0 ? `-${Math.abs(d).toString()}/${Math.abs(c).toString()}` : `${Math.abs(d).toString()}/${Math.abs(c).toString()}`
    const valeurs = ['-∞', zeroDen, '+∞']
    await completeTableau(page, valeurs, signes)
    await clickOk(page) // c’est pas grave, on a une deuxième chance.
    await checkFeedbackCorrection(page, rep, '#MepMD')
  } else {
    await page.waitForSelector('span#affiche_1_2', { hasText: '(clique sur les flèches du tableau pour modifier leur direction)' })
    // parfois, les flèches ont cursor: pointer dans leur style, et parfois pas...
    const lines = await page.locator('svg>line[style="stroke: rgb(255, 0, 0); stroke-opacity: 0; stroke-width: 9; cursor: pointer;"]').all()
    // Si on n’a pas les deux flèches, on cherche celle qui n’ont pas le style cursor: pointer
    if (lines.length < 2) {
      lines.push(...(await page.locator('svg>line[style="stroke: rgb(255, 0, 0); stroke-opacity: 0; stroke-width: 9;"]').all()))
    }
    let fleche1 = lines[0]
    let fleche2 = lines[1]
    if (fleche1 === undefined || fleche2 === undefined) throw Error(`On n’a pas localisé les flèches du tableau de variations : ${JSON.stringify(lines)} sont les lines correspondantes au style "stroke: rgb(255, 0, 0); stroke-opacity: 0; stroke-width: 9;"`)
    const fleche1Id = await fleche1.getAttribute('id') // On a besoin de l’id parce que le style va changer lorsque l’on cliquera dessus, donc le locator sera inopérant !
    const fleche2Id = await fleche2.getAttribute('id') // idem pour la deuxième flèche
    fleche1 = page.locator('svg>line#' + fleche1Id)
    fleche2 = page.locator('svg>line#' + fleche2Id)
    const nbClicks = 2 - (rep ? (numDerivee > 0 ? 1 : 0) : (numDerivee > 0 ? 0 : 1))
    for (let i = 0; i < nbClicks; i++) {
      await fleche1.click({ position: { x: 110, y: i === 0 ? 4 : 24 }, force: true })
      await fleche2.click({ position: { x: 110, y: i === 0 ? 4 : 24 }, force: true })
    }
    await clickOk(page) // c’est pas grave, on a une deuxième chance.
    await checkFeedbackCorrection(page, rep, '#MepMD')
  }
  const result = rep
  if (rep === true && !result) throw Error('On voulait la saisie d’une bonne réponse mais ça n’a pas été le cas')
  if (rep === false && result) throw Error('On voulait la saisie d’une mauvaise réponse mais ça n’a pas été le cas')
  // on vérifie le feedback ok/ko cohérent avec ce qu’on a saisi
  await checkFeedbackCorrection(page, result, '#MepMD')
  // y’a eu le click OK (dans answer), pas le click suite
  return result
}

/**
 * Teste toutes les répétitions de la section squelettemtg32_NbAntecedents_Captk avec les params fournis
 * @param {Page} page
 * @param {object} options
 * @param {SectionParams} options.params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {string} options.section
 * @param {string} options.id id du nœud dans le graphe
 * @return {Promise<number>} Le score
 */
export async function runAll (page, { id, section, params }) {
  // on enregistre les réponses true ou false à 50/50 dans des arrays contenant autant de valeurs que d’essais
  const reponses = []
  while (reponses.length < (params.nbrepetitions ?? 1)) {
    const reps = []
    for (let i = 0; i < params.nbchances ?? 2; i++) { // nbchances est à 2 pour toutes les sections
      reps.push(randomBool())
    }
    reponses.push(reps)
  }
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (!total) {
    // faut aller chercher ça dans la valeur par défaut exportée par la section
    // const { sectionParams } = await import(`${dir}section${section}.js`)
    // const nbRepetitionsParam = sectionParams.parametres.find(([nom]) => nom === 'nbrepetitions')
    // // si la section ne l’exporte pas le modèle fixe ça à 1
    // total = nbRepetitionsParam ? nbRepetitionsParam[1] : 1
    // ce qui précède fait planter playwright sur du js récent utilisé dans le code j3p, probablement à cause du wrapper esm
    // car ce js récent est largement assez vieux pour être compris par le chromium et le node utilisé
    // en attendant de passer nos tests en esm dans la v2, on force ici
    total = 1
  }
  let nbChances = params.nbchances
  if (!nbChances) {
    // idem, faut aller voir les params exportés par la section, on a du bol nos 3 sections ont la même valeur, on le met en dur ici
    // eh bien, c’est à creuser, car il semblerait que EtudeFonction_variations ne donne pas de deuxième chance :-(
    nbChances = 2
  }
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    const reps = reponses[numQuestion - 1]
    // boucle essais
    while (numEssais <= nbChances) {
      // ici c’est un graphe linéaire et sans condition, numProgression correspond à l’id
      logIfVerbose(`Question ${numQuestion} sur ${total}${id != null ? ' (section ' + id + ')' : ''}`)
      await checkEtat(page, { numQuestion, score, total, numProgression: id })
      const rep = reps[numEssais - 1]
      const ok = await runOne(page, rep, id === '1' ? 'derivee' : id === '2' ? 'signes' : 'variations')
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await checkEtat(page, { numQuestion, score, total, numProgression: id })
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) {
        await clickSuite(page)
      }
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  if (id !== '3') {
    await clickFirstDialogOk(page)
  } // fermer la boite de dialogue
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe signeCourbe_datas0.js (que l’on charge ici)
 *  (6 figures différentes)
 */
export async function test (page) {
  page.setDefaultTimeout(5000)
  // d’abord un graphe à 1 seul noeud non fin
  const { graphe } = await import('./EtudeFonctionAxPlusBSurCxPlusD_datas0.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })
  // et on lance la batterie de tests par défaut
  const nodeScores = []
  for (const [id, section, branchements] of graphe) {
    const params = branchements[1] // le troisième élément de chaque noeud est un array avec un branchement et ensuite les params...
    if (section !== 'fin' && params != null) { // ne pas éxécuter de runAll sur le noeud 'Fin'...
      const score = await runAll(page, { id, section, params })
      nodeScores.push({ id, score, nbRepetitions: 1 })
    }
  }
  await checkBilan(page, nodeScores)

  return true
}
