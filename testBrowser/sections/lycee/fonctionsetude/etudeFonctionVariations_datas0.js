export const graphe = [
  [
    '1',
    'EtudeFonction_variations',
    [
      {
        nn: '2',
        pe: 'sans condition',
        conclusion: 'Etudions les variations d’une autre fonction'
      },
      {
        imposer_fct: '',
        modele: [1],
        nbrepetitions: 3,
        nbchances: 1
      }
    ]
  ],
  [
    '2',
    'EtudeFonction_variations',
    [
      {
        nn: '3',
        pe: 'sans condition',
        conclusion: 'Etudions les variations d’une autre fonction'
      },
      {
        imposer_fct: '',
        modele: [2],
        nbrepetitions: 3,
        nbchances: 1
      }
    ]
  ],
  [
    '3',
    'EtudeFonction_variations',
    [
      {
        nn: '4',
        pe: 'sans condition',
        conclusion: 'Etudions les variations d’une autre fonction'
      },
      {
        imposer_fct: '',
        modele: [3],
        nbrepetitions: 3,
        nbchances: 1
      }
    ]
  ],
  [
    '4',
    'EtudeFonction_variations',
    [
      {
        nn: '5',
        pe: 'sans condition',
        conclusion: 'Etudions les variations d’une autre fonction'
      },
      {
        imposer_fct: '',
        modele: [4],
        nbrepetitions: 3,
        nbchances: 1
      }
    ]
  ],
  [
    '5',
    'EtudeFonction_variations',
    [
      {
        nn: '6',
        pe: 'sans condition',
        conclusion: 'Etudions les variations d’une autre fonction'
      },
      {
        imposer_fct: '',
        modele: [5],
        nbrepetitions: 3,
        nbchances: 1
      }
    ]
  ],
  [
    '6',
    'EtudeFonction_variations',
    [
      {
        nn: '7',
        pe: 'sans condition',
        conclusion: 'Etudions les variations d’une autre fonction'
      },
      {
        imposer_fct: '',
        modele: [6],
        nbrepetitions: 3,
        nbchances: 1
      }
    ]
  ],
  [
    '7',
    'EtudeFonction_variations',
    [
      {
        nn: '8',
        pe: 'sans condition',
        conclusion: 'Etudions les variations d’une autre fonction'
      },
      {
        imposer_fct: '',
        modele: [7],
        nbrepetitions: 3,
        nbchances: 1
      }
    ]
  ],
  [
    '8',
    'EtudeFonction_variations',
    [
      {
        nn: '9',
        pe: 'sans condition',
        conclusion: 'Etudions les variations d’une autre fonction'
      },
      {
        imposer_fct: '',
        modele: [8],
        nbrepetitions: 3,
        nbchances: 1
      }
    ]
  ],
  [
    '9',
    'fin',
    [
      null]
  ]
]
