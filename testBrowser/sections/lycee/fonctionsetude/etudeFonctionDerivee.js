import { clickFirstDialogOk, clickOk, clickSectionSuivante, clickSuite } from 'testBrowser/helpers/actions'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { exp, InputMqHandler, ln } from 'testBrowser/helpers/InputMqHandler'
import { randomBool } from 'src/lib/utils/random'
import { getMqChunks } from 'testBrowser/helpers/text'
import { logIfVerbose } from 'testBrowser/helpers/log'
import { getModele } from './_common'

async function answer (page, rep, repDerivee) {
  const texteArea = new InputMqHandler(page, { index: 0 })
  await texteArea.clear()
  // en cas de réponse fausse, on écrit ad+bc au numérateur au lieu de ad-bc
  await texteArea.typeClavier(repDerivee)
  await clickOk(page) // c’est pas grave, on a une deuxième chance.
  await checkFeedbackCorrection(page, rep, '#MepMD')
  return rep
}

/**
 * récupère les paramètres a et b d’une chaine contenant ax+b. Ex: '-3' retournera [0,-3] et '-2x' retournera [-2,0]
 * @param texte
 * @returns {(number|number)[]|(number|number)[]}
 */
export function extraireCoeffAffine (texte) {
  const [, , c, d] = extraireCoeffDeg3(texte)
  return [c, d]
}

/**
 * récupère les 4 coefficients de ax³+bx²+cx+d
 * @param texte La chaine de caractère contenant l’expression ex : '-5x^3+\frac{3}{2}x^2-6x' retournera [-5,{signe:1,num:3,den:2},-6,0]
 * @returns {(number|{num: number, den: number, signe: number}|{num: number, den: number, signe: number}|{num: number, den: number, signe: number}|number)[]}
 */
export function extraireCoeffDeg3 (texte) {
  texte = texte.replaceAll(/\s/g, '').replaceAll('−', '-')
  let a, b, c, d
  // Normalement il y a du degré 3, on commence par chercher le coefficient
  const deg3 = texte.match(/-?(\\frac)?[{}\-0-9]*x\^3/)
  let fonctionDegre2
  if (deg3 != null) {
    let monome3 = deg3[0]
    fonctionDegre2 = texte.slice(monome3.length)
    let signeA
    if (monome3[0] === '-') { // ça commence par un - donc a est négatif
      signeA = -1
      monome3 = monome3.slice(1)
    } else {
      signeA = 1
    }
    if (monome3[0] === 'x') { // coeff 1 ou -1 non écrit
      a = signeA
    } else {
      if (monome3.includes('frac')) {
        const [num, den] = monome3.match(/[0-9]+/g)
        a = { signe: signeA, num: Number(num), den: Number(den) }
      } else {
        a = signeA * monome3.match(/[0-9]/g)[0]
      }
    }
  } else {
    a = 0
    fonctionDegre2 = texte
  }
  // on passe à la suite de la fonction y a-t-il un coefficient de degré 2 ?
  const deg2 = fonctionDegre2.match(/-?\+?(\\frac)?[{}\-0-9]*x\^2/)
  let fonctionAffine
  if (deg2 != null) {
    let monome2 = deg2[0]
    fonctionAffine = fonctionDegre2.slice(monome2.length)
    let signeB
    if (monome2[0] === '-') { // ça commence par - donc b est négatif
      signeB = -1
      monome2 = monome2.slice(1)
    } else if (monome2[0] === 'x' || monome2[0] === '+' || monome2[0].match(/^[0-9]/g).length > 0) { // si pas de x³ et que b=1, alors ça commence par x sinon +
      signeB = 1
    }
    if (monome2[0] === 'x') { // coeff 1 ou -1 non écrit
      b = signeB
    } else {
      if (monome2[0] === '+') {
        monome2 = monome2.slice(1)
      }
      if (monome2.includes('frac')) {
        const [num, den] = monome2.match(/[0-9]+/g)
        b = { signe: signeB, num: Number(num), den: Number(den) }
      } else {
        b = monome2 === 'x^2' ? signeB : signeB * monome2.match(/[0-9]+/g)[0]
      }
    }
  } else {
    b = 0
    fonctionAffine = fonctionDegre2
  }
  // maintenant on passe au coefficient de degré 1
  const deg1 = fonctionAffine.match(/-?\+?(\\frac)?[{}0-9]*x/)
  let fonctionConstante
  if (deg1 != null) {
    let monome1 = deg1[0]
    fonctionConstante = fonctionAffine.slice(monome1.length)
    let signeC
    if (monome1[0] === '-') { // ça commence par - donc c est négatif
      signeC = -1
      monome1 = monome1.slice(1)
    } else if (monome1[0] === 'x' || monome1[0] === '+' || monome1[0].match(/^[0-9]/g).length > 0) { // si pas de x³ et que b=1, alors ça commence par x sinon +
      signeC = 1
    }
    if (monome1[0] === 'x') { // coeff 1 ou -1 non écrit
      c = signeC
    } else {
      if (monome1[0] === '+') {
        monome1 = monome1.slice(1)
      }
      if (monome1.includes('frac')) {
        const [num, den] = monome1.match(/[0-9]+/g)
        c = { signe: signeC, num: Number(num), den: Number(den) }
      } else {
        c = monome1 === 'x' ? signeC : signeC * Number(monome1.match(/[0-9]+/g)[0])
      }
    }
  } else {
    c = 0
    fonctionConstante = fonctionAffine
  }
  if (fonctionConstante.length === 0) {
    d = 0
  } else { // à priori d n’est pas une fraction... à vérifier !
    d = Number(fonctionConstante)
  }
  return [a, b, c, d]
}

/**
 * joue la(les) répétition(s) et retourne le résultat et la fonction étudiée lors de la dernière répétition, donc à n’utiliser qu’avec une seule répétition pour enchainer une autre section avec la même fonction
 * @param page
 * @param params
 * @param numProgression
 * @returns {Promise<(number|*)[]>}
 */
export async function runAllAvecFonction (page, params, { numProgression }) {
  return [await runAll(page, params, { numProgression }), await page.locator('span#affiche_0_2').innerText()]
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean|string} rep passer true pour une bonne réponse et false|string pour une mauvaise
 * @param {number} modele le modèle de fonction (voir sectionEtudeFonction_derivee)
 * @return {Promise<boolean>}
 */
async function runOne (page, rep) {
  let repDerivee = ''
  const modele = await getModele(page)
  switch (modele) {
    case 1: { // (ax+b)/exp(x)
      const fonction = (await page.locator('span#affiche_0_2').innerText()).match(/(f\(x\)=)|[\dx\-+]+/g)
      const [a, b] = extraireCoeffAffine(fonction[1])
      logIfVerbose('On est dans le modèle 1 ici !!!')
      const aPlusB = rep ? Number(a) + Number(b) : Number(a) - Number(b)
      repDerivee = `${aPlusB === 0 ? '' : '('}${a === 1 ? '' : a === -1 ? '-' : a.toString()}x${aPlusB > 0 ? '+' + aPlusB.toString() + ')' : aPlusB === 0 ? '' : aPlusB.toString() + ')'}${exp}x`
    }
      break
    case 2: { // ax³+bx²+cx+d
      const fonction = (await page.locator('span#affiche_0_2').innerText()).split('=')[1].split('$')[0]
      // a et b peuvent être fractionnaires ! b, c et d peuvent être nuls...
      const [a, b, c] = extraireCoeffDeg3(fonction)
      // pour dériver on n’a pas besoin de cette constante... on garde le code sous la main pour d’autres sections
      // on a a,b et c... on dérive !
      const coeffDerivee = []
      let exposant = 3
      for (const coeff of [a, b, c]) {
        if (typeof coeff === 'number') {
          coeffDerivee.push(coeff * (rep ? exposant : exposant + 1))
        } else {
          if (coeff.num * exposant / coeff.den === Math.round(coeff.num * exposant / coeff.den)) {
            coeffDerivee.push(rep ? coeff.signe * Math.round(coeff.num * exposant / coeff.den) : exposant)
          } else {
            coeffDerivee.push({ signe: coeff.signe, num: coeff.num * (rep ? exposant : exposant + 1), den: coeff.den })
          }
        }
        exposant--
      }
      // on peut écrire la dérivée
      const [aPrime, bPrime, cPrime] = coeffDerivee
      if (aPrime !== 0) {
        repDerivee = `${typeof aPrime === 'number' ? (aPrime === 1 ? '' : aPrime === -1 ? '-' : aPrime.toString()) : (aPrime.signe * aPrime.num).toString() + '/' + aPrime.den.toString() + '→'}` + 'x^2→'
        // si il y a un terme en x², il faut mettre le + du terme en x si celui-ci est positif
        // pour le cas où c’est négatif, le signe sera là avec le nombre.
        // le cas bPrime === 0 ou bPrime négatif n’est donc pas traité.
        if (typeof bPrime === 'number' && bPrime > 0) repDerivee += '+'
        else if (bPrime.signe === 1) repDerivee += '+'
      }
      if (bPrime !== 0) { // il y a un terme en x fractionnaire ou pas
        repDerivee += `${typeof bPrime === 'number' ? (bPrime === 1 ? '' : bPrime === -1 ? '-' : bPrime.toString()) : (bPrime.signe * bPrime.num).toString() + '/' + bPrime.den.toString() + '→'}` + 'x'
      }
      if (cPrime !== 0) {
        if ((typeof cPrime === 'number' && cPrime > 0) || (typeof cPrime !== 'number' && cPrime.signe === 1)) repDerivee += '+'
        repDerivee += `${typeof cPrime === 'number' ? cPrime.toString() : (cPrime.signe * cPrime.num).toString() + '/' + cPrime.den.toString() + '→'}`
      }
    }
      break
    case 3: { // (ax+b)/(cx+d)
      const fonction = (await page.locator('span#affiche_0_2').innerText()).match(/(f\(x\)=\\frac{)|[\dx\-+]+/g)
      const num = fonction[1]
      const den = fonction[2]
      const [a, b] = extraireCoeffAffine(num)
      const [c, d] = extraireCoeffAffine(den)
      const numDerivee = rep ? a * d - b * c : a * d + b * c + 1
      // denDerivee ne sert qu'à l’écriture de la dérivée, de toute façon, c’est un carré donc toujours positif.
      const denDerivee = `(${c === 0 ? '' : c === 1 ? 'x' : c === -1 ? '-x' : c.toString() + 'x'}${d === 0 ? '' : d > 0 ? '+' + d.toString() : d.toString()})^2`
      repDerivee = numDerivee.toString() + '/' + denDerivee
    }
      break
    case 4: { // (ax+b)/(cx^2+d)
      const fonction = (await page.locator('span#affiche_0_2').innerText()).match(/{-?\d*x\^?\d?-?\+?\d*}/g)
      const num = fonction[0].slice(1).slice(0, -1)
      const den = fonction[1].slice(1).slice(0, -1)
      const [denPartA, denPartB] = den.split('^2')
      const [a, b] = extraireCoeffAffine(num)
      const [, c, d, e] = extraireCoeffDeg3(den)
      // on a les coeffs, on dérive
      const deriveeDen = denPartB != null
        ? `(${denPartA}^2→${denPartB}→^2`
        : `(${denPartA}→^2`
      const aPrime = (-a * c)
      const bPrime = rep ? -2 * b * c : -2 * b * c + 1
      const cPrime = rep
        ? a * e - b * d
        : a * e - b * d + 1
      repDerivee = '/'
      repDerivee += aPrime === 0 ? '' : aPrime === 1 ? 'x^2→' : aPrime === -1 ? '-x^2→' : aPrime.toString() + 'x^2→'
      repDerivee += bPrime === 0
        ? ''
        : bPrime > 0
          ? repDerivee !== ''
            ? '+' + (bPrime !== 1 ? bPrime.toString() : '') + 'x'
            : bPrime.toString() + 'x'
          : bPrime === -1
            ? '-x'
            : bPrime.toString() + 'x'
      repDerivee += cPrime === 0
        ? ''
        : cPrime > 0
          ? '+' + cPrime.toString()
          : cPrime.toString()
      repDerivee = repDerivee + `↓${deriveeDen}`
    }
      break
    case 5: { // (ax+b)exp(cx+d)
      const fonction = await page.locator('span#affiche_0_2').innerText()
      const facteurAffine = fonction.match(/=(\\left\()?-?\d*x-?\+?\d*/g)[0].replace('=', '').replace('\\left(', '')
      const exposant = fonction.match(/\{-?\d*x-?\+?\d*/g)[0].slice(1)
      logIfVerbose(`La fonction Affine : ${facteurAffine} et l’exponentielle : ${exposant}`)
      const [a, b] = extraireCoeffAffine(facteurAffine)
      const [c] = extraireCoeffAffine(exposant)
      // on a les coeffs, on dérive
      const aPrime = rep ? a * c : a * c * 2
      const bPrime = rep ? b * c + a : b * c + a + 1
      if (aPrime === 0 && bPrime === 0) { // ça ne devrait pas arriver, sinon on parle d’une fonction constante
        repDerivee = '0'
      } else {
        if (bPrime !== 0 && aPrime !== 0) repDerivee = '('
        repDerivee += aPrime === 1
          ? 'x'
          : aPrime === -1
            ? '-x'
            : aPrime === 0
              ? ''
              : aPrime.toString() + 'x'
        repDerivee += bPrime === 0
          ? ''
          : bPrime > 0
            ? (aPrime === 0 ? bPrime.toString() : '+' + bPrime.toString())
            : bPrime.toString()
        if (bPrime !== 0 && aPrime !== 0) repDerivee += ')'
        repDerivee += `${exp}${exposant}`
      }
    }
      break
    case 6: { // (a+bln(x))/x^c
      const fonction = getMqChunks(await page.locator('span#affiche_0_2').innerText())[0].replaceAll(/\s/g, '')
      const [num, den] = fonction.match(/\{-?\d*\+?-?\d*\\ln\(?x\)?}|\{x\^?\d?}/g)
      const text = num.split('\\ln')[0].slice(1)
      const coeffs = text.match(/-?\+?\d*/g)
      let a, b
      if (coeffs.length > 2) {
        a = Number(coeffs[0])
        if (coeffs[1] === '-') b = -1
        else if (coeffs[1] === '+') b = 1
        else b = Number(coeffs[1])
      } else {
        a = 0
        if (coeffs[0] === '-') b = -1
        else if (coeffs[0] === '+') b = 1
        else b = Number(coeffs[0])
      }
      const n = den.includes('^') // en cas de réponse fausse on modifie l’exposant.
        ? Number(den.split('^')[1].replace('}', '')) + (rep ? 0 : 1)
        : (rep ? 1 : 2)
      // on a les coeffs, on dérive (tout dépend de n donc si rep est false, tout est faux
      const aPrime = b - n * a
      const bPrime = -n * b
      let numDerivee = ''
      let denDerivee
      if (aPrime === 0 && bPrime === 0) { // ça ne devrait pas arriver, sinon on parle d’une fonction constante
        repDerivee = '0'
      } else {
        if (n === 1) { // pas de puissance au numérateur et x^2 au dénominateur
          numDerivee = `${aPrime === 0
              ? ''
              : aPrime.toString()}${bPrime === 0
              ? ''
              : bPrime < 0
                  ? (bPrime === -1
                      ? '-' + ln + 'x'
                      : bPrime.toString() + ln + 'x')
                  : (aPrime !== 0
                      ? '+' + bPrime.toString() + ln + 'x'
                      : bPrime.toString() + ln + 'x')}`
          denDerivee = 'x^2→'
        } else {
          if (aPrime !== 0 && bPrime !== 0) {
            numDerivee = '('
          }
          numDerivee += `${aPrime === 0 ? '' : aPrime.toString()}${bPrime === 0 ? '' : bPrime < 0 ? bPrime.toString() + ln + 'x)' : aPrime !== 0 ? '+' + bPrime.toString() + ln + 'x)' : bPrime.toString() + ln + 'x)'}`
          if (aPrime !== 0 && bPrime !== 0) {
            numDerivee += '→'
          }
          numDerivee += `x^${n - 1}→`
          denDerivee = `x^${2 * n}→`
        }
        repDerivee = `/${numDerivee}↓${denDerivee}`
      }
    }
      break
    case 7: { // ax+b+cln(dx+e)
      const fonction = getMqChunks(await page.locator('span#affiche_0_2').innerText())[0].replaceAll(/\s/g, '')
      const defFonction = fonction.split('=')[1].replaceAll(/\s/g, '')
      const text = defFonction.split('\\ln')
      let c
      if (text[0][text[0].length - 1] === '-') {
        c = -1
        text[0] = text[0].slice(0, -1)
      } else if (text[0][text[0].length - 1] === '+') {
        c = 1
        text[0] = text[0].slice(0, -1)
      } else {
        const cString = text[0].match(/(-?\+?\d+)$/)[0]
        text[0] = text[0].slice(0, -cString.length)
        c = Number(cString)
      }
      if (!rep) c++ // en cas de mauvaise réponse, on augment c
      const [a] = extraireCoeffAffine(text[0])
      const lnContent = text[1].replace('(', '').replace(')', '').replace('\\left', '').replace('\\right', '')
      const [d] = extraireCoeffAffine(lnContent)
      // on a les coeffs, on dérive (tout dépend de n donc si rep est false, tout est faux
      repDerivee = `${a === 0 ? '' : a.toString()}${c * d > 0 ? '+' : ''}${(c * d).toString()}/${lnContent}`
    }
      break
    case 8: { // ax²+bx+c
      const fonction = (await page.locator('span#affiche_0_2').innerText()).split('=')[1].split('$')[0]
      // a et b peuvent être fractionnaires ! b, c et d peuvent être nuls...
      const [, a, b] = extraireCoeffDeg3(fonction)
      // on a a,b ... on dérive !
      const coeffDerivee = []
      let exposant = 2
      for (const coeff of [a, b]) {
        if (typeof coeff === 'number') {
          coeffDerivee.push(coeff * (rep ? exposant : exposant + 1))
        } else {
          if (coeff.num * exposant / coeff.den === Math.round(coeff.num * exposant / coeff.den)) {
            coeffDerivee.push(rep ? coeff.signe * Math.round(coeff.num * exposant / coeff.den) : exposant)
          } else {
            coeffDerivee.push({ signe: coeff.signe, num: coeff.num * (rep ? exposant : exposant + 1), den: coeff.den })
          }
        }
        exposant--
      }
      // on peut écrire la dérivée
      const [aPrime, bPrime] = coeffDerivee
      if (aPrime !== 0) {
        repDerivee = `${typeof aPrime === 'number' ? (aPrime === 1 ? '' : aPrime === -1 ? '-' : aPrime.toString()) : (aPrime.signe * aPrime.num).toString() + '/' + aPrime.den.toString() + '→'}` + 'x'
        // si il y a un terme en x, il faut mettre le + du terme constant si celui-ci est positif
        // pour le cas où c’est négatif, le signe sera là avec le nombre.
        // le cas bPrime === 0 ou bPrime négatif n’est donc pas traité.
        if (typeof bPrime === 'number' && bPrime > 0) repDerivee += '+'
        else if (bPrime.signe === 1) repDerivee += '+'
      }
      if (bPrime !== 0) { // il y a un terme constant fractionnaire ou pas
        repDerivee += `${typeof bPrime === 'number' ? bPrime.toString() : (bPrime.signe * bPrime.num).toString() + '/' + bPrime.den.toString()}`
      }
    }
      break
  }
  return await answer(page, rep, repDerivee)
}

/**
 * Teste toutes les répétitions de la section squelettemtg32_NbAntecedents_Captk avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {object} options
 * @param {string} options.numProgression N° de la section dans le graphe
 * @return {Promise<number>} Le score
 */
async function runAll (page, params, { numProgression }) {
  // on enregistre les réponses true ou false à 50/50 dans des arrays contenant autant de valeurs que d’essais
  const reponses = []
  while (reponses.length < (params.nbrepetitions ?? 1)) {
    const reps = []
    for (let i = 0; i < params.nbchances ?? 2; i++) { // nbchances est à 2 pour toutes les sections
      reps.push(randomBool())
    }
    reponses.push(reps)
  }
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (!total) {
    // faut aller chercher ça dans la valeur par défaut exportée par la section
    // const { sectionParams } = await import(`${dir}section${section}.js`)
    // const nbRepetitionsParam = sectionParams.parametres.find(([nom]) => nom === 'nbrepetitions')
    // // si la section ne l’exporte pas le modèle fixe ça à 1
    // total = nbRepetitionsParam ? nbRepetitionsParam[1] : 1
    // ce qui précède fait planter playwright sur du js récent utilisé dans le code j3p, probablement à cause du wrapper esm
    // car ce js récent est largement assez vieux pour être compris par le chromium et le node utilisé
    // en attendant de passer nos tests en esm dans la v2, on force ici
    total = 1
  }
  let nbChances = params.nbchances
  if (!nbChances) {
    // idem, faut aller voir les params exportés par la section, on a du bol nos 3 sections ont la même valeur, on le met en dur ici
    // eh bien, c’est à creuser, car il semblerait que EtudeFonction_variations ne donne pas de deuxième chance :-(
    nbChances = 2
  }
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    const reps = reponses[numQuestion - 1]
    // boucle essais
    while (numEssais <= nbChances) {
      await checkEtat(page, { numQuestion, score, total, numProgression })
      const rep = reps[numEssais - 1]
      const ok = await runOne(page, rep)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await checkEtat(page, { numQuestion, score, total, numProgression })
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe etudeFonctionDerivee_datas0.js (que l’on charge ici)
 *  (7 fonctions différentes)
 */
export async function test (page) {
  page.setDefaultTimeout(5000)

  const { graphe } = await import('./etudeFonctionDerivee_datas0.js')
  const numProgressionMax = 8
  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })
  // et on lance la batterie de tests par défaut
  const nodeScores = []
  let numProgression = 1
  for (const [id, section, branchements] of graphe) {
    const params = branchements[1] // le troisième élément de chaque noeud est un array avec un branchement et ensuite les params...
    if (section === 'EtudeFonction_derivee') { // ne pas éxécuter de runAll sur le noeud 'Fin'...
      nodeScores.push({ id, score: await runAll(page, params, { numProgression }), nbRepetitions: params.nbrepetitions })
      numProgression++
      if (numProgression <= numProgressionMax) {
        await clickFirstDialogOk(page)
      }
    }
  }
  await checkBilan(page, nodeScores)
  return true
}
