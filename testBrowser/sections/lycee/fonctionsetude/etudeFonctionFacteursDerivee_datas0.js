import { tabModeles } from 'src/legacy/sections/lycee/fonctionsetude/common'
export const graphe = [
  [
    '1',
    'EtudeFonction_facteursderivee',
    [
      {
        nn: '2',
        pe: 'sans condition',
        conclusion: 'Fini',
        label: 'Sans condition'
      },
      {
        imposer_fct: '',
        modele: tabModeles,
        nbrepetitions: 8,
        indication: '',
        limite: '',
        nbchances: 1,
        imposer_domaine: []
      }
    ]
  ],
  [
    '2',
    'fin',
    [
      null
    ]
  ]
]
