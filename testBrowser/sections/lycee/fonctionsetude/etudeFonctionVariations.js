import { pgcd } from 'src/lib/utils/number'
import { randomBool } from 'src/lib/utils/random'
import { clickFirstDialogOk, clickOk, clickSectionSuivante, clickSuite } from 'testBrowser/helpers/actions'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { exp, InputMqHandler, ln } from 'testBrowser/helpers/InputMqHandler'
import { logIfVerbose } from 'testBrowser/helpers/log'
import { getMqChunks } from 'testBrowser/helpers/text'
import { getModele } from './_common'
import { extraireCoeffAffine, extraireCoeffDeg3 } from './etudeFonctionDerivee'

// on tombe souvent sur des négatifs…
const pgcdn = (n, m) => pgcd(n, m, { negativesAllowed: true })

function calculNumDenReduit (valeur, fctNum, fctDen) {
  const [num, den] = valeur.split('/').map((elt) => Number(elt))
  const num1 = fctNum(num, den) // a * num + b * den
  const den1 = fctDen(num, den)
  const gcd1 = pgcdn(num1, den1)
  return [num1 / gcd1, den1 / gcd1]
}

function normaliseFraction (element) {
  if (typeof element === 'number') {
    return { num: element, den: 1 }
  } else {
    return { num: element.signe * element.num, den: element.den }
  }
}

async function trouverValeursEtSignes (page, rep) {
  const valeurs = []
  let signes = []
  await page.waitForSelector('div#tableau1_0_0')
  const divTableau = page.locator('div#tableau1_0_0')
  const divsDuTableau = await divTableau.locator('div').all()
  for (const div of divsDuTableau) {
    const id = await div.getAttribute('id')
    if (id != null) {
      if (id.includes('zone_nulle')) {
        const text = getMqChunks(await div.innerText())[0].replace('\\infty', '∞')
        if (text.includes('frac')) {
          const [num, den] = text.match(/-?\d+/g)
          const negatif = text[0] === '-'
          valeurs.push(`${negatif ? String(-Number(num)) : num}/${den}`)
        } else {
          valeurs.push(text.replace('−', '-'))
        }
      } else if (id.includes('signe_fct')) {
        const textBrut = await div.innerText()
        const text = getMqChunks(textBrut)[0]
        if (text) {
          signes.push(text.replace('−', '-'))
        }
      }
    }
  }
  if (!rep) {
    await logIfVerbose(`reponse fausse ${signes} avant`)
    signes = signes.map(signe => signe === '+' ? '-' : '+')
    await logIfVerbose(`reponse fausse ${signes} après`)
  }
  return [valeurs, signes]
}

async function changeFleches (page, signes) {
  const lines = await page.locator('svg>line[style="stroke: rgb(255, 0, 0); stroke-opacity: 0; stroke-width: 9;"]').all()
  const flechesId = []
  const fleches = []
  for (let i = 0; i < signes.length; i++) {
    flechesId[i] = await lines[i].getAttribute('id')
    fleches[i] = page.locator('svg>line#' + flechesId[i])
  }
  // on trouve déjà les locators sur les flèches horizontales avant de cliquer...
  for (let i = 0; i < signes.length; i++) {
    await fleches[i].click({ force: true })// à minima la flèche doit être mis en position croissante avec un clic
    if (signes[i] === '-') {
      await fleches[i].click({ force: true })
    }
  }
}

async function renseigneImages (page, images) {
  for (let i = 0; i < images.length; i++) {
    const texteArea = new InputMqHandler(page, { index: i })
    await texteArea.clear()
    // en cas de réponse fausse, on écrit ad+bc au numérateur au lieu de ad-bc
    await texteArea.typeClavier(images[i])
  }
}

async function answer (page, rep) {
  await clickOk(page) // c’est pas grave, on a une deuxième chance.
  await checkFeedbackCorrection(page, rep, '#MepMD')
  return rep
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean|string} rep passer true pour une bonne réponse et false|string pour une mauvaise
 * @return {Promise<boolean>}
 */
async function runOne (page, rep) {
  await page.waitForSelector('rect#tableau1_0_0cadre')
  let images = []
  let signes = []
  let valeurs = []
  // on doit repérer les valeurs intermédiaires dans la première ligne
  // on doit repérer les limites aux bornes
  // on doit calculer les images
  // on doit cliquer sur les flèches.
  const modele = await getModele(page)
  switch (modele) {
    case 1: { // (ax+b)/exp(x) valeur intermédiaire -(a+b)/a image : -aexp(-(a+b)/a) signes : signe de -a puis signe de a
      const fonction = (await page.locator('span#affiche_0_2').innerText()).match(/(f\(x\)=)|[\dx\-+]+/g)
      const [a, b] = extraireCoeffAffine(fonction[1])
      const aPlusB = rep
        ? Number(a) + Number(b)
        : (b === 0 ? -Number(a) : Number(a) - Number(b))
      // calculons l’image
      let image
      if (Number.isInteger(aPlusB / a)) {
        if (-aPlusB / a === 1) {
          image = `${(-a).toString()}e`
        } else if (-aPlusB / a === 0) {
          image = `${(-a).toString()}`
        } else if (-aPlusB / a === -1) {
          image = `${(-a).toString()}/e}`
        } else {
          image = `${(-a).toString()}${exp}${(-aPlusB / a).toString()}`
        }
      } else {
        image = `${(-a).toString()}${exp}${-aPlusB / a > 0 ? '' : '-'}${(Math.abs(aPlusB)).toString()}/${Math.abs(a)}`
      }
      // l’exponentielle étant ce qu’elle est : les limites sont toujours 0 et +infty
      [valeurs, signes] = await trouverValeursEtSignes(page, rep)
      images = ['0', image, a > 0 ? '+∞' : '-∞']
      // si rep est false, l’image est déjà faussée et on inverse les signes
      signes = rep ? (a > 0 ? ['-', '+'] : ['+', '-']) : (a < 0 ? ['-', '+'] : ['+', '-'])
    }
      break
    case 2: { // ax³+bx²+cx+d dérivée : 3ax²+2bx+c il peut y avoir 0, 1 ou 2 valeurs intermédiaires
      // on a besoin des valeurs de a,b,c et d pour calculer les images là où la dérivée s’annule
      const fonction = (await page.locator('span#affiche_0_2').innerText()).split('=')[1].split('$')[0]
      // a et b peuvent être fractionnaires ! b, c et d peuvent être nuls...
      let [a, b, c, d] = extraireCoeffDeg3(fonction)
      a = normaliseFraction(a)
      b = normaliseFraction(b)
      c = normaliseFraction(c)
      d = normaliseFraction(d);
      [valeurs, signes] = await trouverValeursEtSignes(page, rep)
      // On va calculer les images
      images[0] = a.num > 0 ? '-∞' : '+∞'
      for (let i = 1; i < valeurs.length - 1; i++) {
        let numerateur, denominateur, gcd, numReduit, denReduit
        if (Number.isNaN(valeurs[i])) { // c’est pas juste un entier relatif
          const [num, den] = valeurs[i].split('/').map((elt) => Number(elt))
          numerateur = a.num * b.den * c.den * d.den * num ** 3 + b.num * a.den * c.den * d.den * den * num ** 2 + c.num * a.den * b.den * d.den * num * den ** 2 + d.num * a.den * b.den * c.den * den ** 3
          denominateur = a.den * b.den * c.den * d.den * den ** 3
          gcd = pgcdn(numerateur, denominateur)
          numReduit = numerateur / gcd
          denReduit = denominateur / gcd
        } else {
          const antecedent = Number(valeurs[i])
          numerateur = a.num * b.den * c.den * d.den * antecedent ** 3 + b.num * a.den * c.den * d.den * antecedent ** 2 + c.num * a.den * b.den * d.den * antecedent + d.num * a.den * b.den * c.den
          denominateur = a.den * b.den * c.den * d.den
          gcd = pgcdn(numerateur, denominateur)
          numReduit = numerateur / gcd
          denReduit = denominateur / gcd
        }
        if (Number.isInteger(numReduit / denReduit)) {
          images[i] = (numReduit / denReduit).toString()
        } else {
          images[i] = denReduit > 0
            ? numReduit.toString() + '/' + denReduit.toString()
            : (-numReduit).toString() + '/' + (-denReduit).toString()
        }
      }
      images.push(a.num > 0 ? '+∞' : '-∞')
    }
      break
    case 3: { // (ax+b)/(cx+d)
      const fonction = (await page.locator('span#affiche_0_2').innerText()).match(/(f\(x\)=\\frac{)|[\dx\-+]+/g)
      const num = fonction[1]
      const den = fonction[2]
      const [a, b] = extraireCoeffAffine(num)
      const [c, d] = extraireCoeffAffine(den)
      const valeurNumEnMoinsDSurC = -a * d / c + b;
      // à priori a,b,c et d sont des entiers, donc on ne transforme pas en fraction.
      [valeurs, signes] = await trouverValeursEtSignes(page, rep)
      // On va calculer les images
      images[0] = Number.isInteger(a / c)
        ? (a / c).toString()
        : `${c > 0
          ? a.toString() + '/' + c.toString()
          : (-a).toString() + '/' + (-c).toString()}`
      for (let i = 1; i < valeurs.length - 1; i++) {
        let numerateur, denominateur
        if (isNaN(valeurs[i])) { // c’est pas juste un entier relatif
          const [num, den] = valeurs[i].split('/').map((elt) => Number(elt))
          numerateur = a * num + b * den
          denominateur = c * num + d * den
        } else {
          const antecedent = Number(valeurs[i])
          numerateur = a * antecedent + b
          denominateur = c * antecedent + d
        }
        if (denominateur === 0) {
          if ((c > 0 && valeurNumEnMoinsDSurC > 0) || (c < 0 && valeurNumEnMoinsDSurC < 0)) images.push('-∞', '+∞')
          else images.push('+∞', '-∞')
        } else {
          const gcd = pgcdn(numerateur, denominateur)
          const numReduit = numerateur / gcd
          const denReduit = denominateur / gcd
          if (Number.isInteger(numReduit / denReduit)) {
            images.push((numReduit / denReduit).toString())
          } else {
            images.push(denReduit > 0
              ? numReduit.toString() + '/' + denReduit.toString()
              : (-numReduit).toString() + '/' + (-denReduit).toString())
          }
        }
      }
      images.push(Number.isInteger(a / c)
        ? (a / c).toString()
        : `${c > 0
          ? a.toString() + '/' + c.toString()
          : (-a).toString() + '/' + (-c).toString()}`)
    }
      break
    case 4: { // (ax+b)/(cx^2+d)
      const fonction = (await page.locator('span#affiche_0_2').innerText()).match(/{-?\d*x\^?\d?-?\+?\d*}/g)
      const num = fonction[0].slice(1).slice(0, -1)
      const den = fonction[1].slice(1).slice(0, -1)
      const [a, b] = extraireCoeffAffine(num)
      const [, c, d, e] = extraireCoeffDeg3(den);
      [valeurs, signes] = await trouverValeursEtSignes(page, rep)
      // On va calculer les images
      images[0] = '0'
      for (let i = 1; i < valeurs.length - 1; i++) {
        let numerateur, denominateur
        if (isNaN(valeurs[i])) { // c’est pas juste un entier relatif
          const [num, den] = valeurs[i].split('/').map((elt) => Number(elt))
          numerateur = (a * num + b * den) * den
          denominateur = c * num ** 2 + d * num * den + e * den ** 2
        } else {
          const antecedent = Number(valeurs[i])
          numerateur = a * antecedent + b
          denominateur = c * antecedent ** 2 + d * antecedent + e
        }
        if (denominateur === 0) {
          await logIfVerbose('Là on a un problème ! le dénominateur s’annule, alors ça tend vers l’infini !')
        } else {
          const gcd = pgcdn(numerateur, denominateur)
          const numReduit = numerateur / gcd
          const denReduit = denominateur / gcd
          if (Number.isInteger(numReduit / denReduit)) {
            images.push((numReduit / denReduit).toString())
          } else {
            images.push(denReduit > 0
              ? numReduit.toString() + '/' + denReduit.toString()
              : (-numReduit).toString() + '/' + (-denReduit).toString())
          }
        }
      }
      images.push('0')
    }
      break
    case 5: { // (ax+b)exp(cx+d)
      const fonction = await page.locator('span#affiche_0_2').innerText()
      const facteurAffine = fonction.match(/=(\\left\()?-?\d*x-?\+?\d*/g)[0].replace('=', '').replace('\\left(', '')
      const exposant = fonction.match(/\{-?\d*x-?\+?\d*/g)[0].slice(1)
      const [a, b] = extraireCoeffAffine(facteurAffine)
      const [c, d] = extraireCoeffAffine(exposant);
      [valeurs, signes] = await trouverValeursEtSignes(page, rep)
      images[0] = c > 0 ? '0' : a > 0 ? '-∞' : '+∞'
      let num2
      if (isNaN(valeurs[1])) { // Ce ne sont pas les mêmes - !!! la conversion de -1 à l’affichage donne NaN !
        num2 = -a - b * c + d * a // den2 = a*c
        const gcd = pgcdn(num2, a)
        num2 = num2 / gcd
        const den2 = a / gcd
        if (Number.isInteger(-a / c)) {
          if (-a === c) {
            images[1] = ''
          } else if (a === c) {
            images[1] = '-'
          } else {
            images[1] = (-a / c).toString()
          }
        } else {
          images[1] = c > 0
            ? (-a).toString() + '/' + c.toString()
            : a.toString() + '/' + (-c).toString()
          images[1] += '→'
        }
        images[1] += exp
        if (Number.isInteger(num2 / den2)) {
          images[1] += (num2 / den2).toString()
        } else {
          images[1] += den2 > 0
            ? num2.toString() + '/' + den2.toString()
            : (-num2).toString() + '/' + (-den2).toString()
        }
      } else {
        const antecedent = Number(valeurs[1])
        images[1] = `${(a * antecedent + b).toString()}${exp}${(c * antecedent + d).toString()}`
      }
      images[2] = c > 0 ? (a > 0 ? '+∞' : '-∞') : '0'
    }
      break
    case 6: { // (a+bln(x))/x^c
      const fonction = getMqChunks(await page.locator('span#affiche_0_2').innerText())[0].replaceAll(/\s/g, '')
      const [num, den] = fonction.match(/\{-?\d*\+?-?\d*\\ln\(?x\)?}|\{x\^?\d?}/g)
      const text = num.split('\\ln')[0].slice(1)
      const coeffs = text.match(/-?\+?\d*/g)
      let a, b
      if (coeffs.length > 2) {
        a = Number(coeffs[0])
        if (coeffs[1] === '-') b = -1
        else if (coeffs[1] === '+') b = 1
        else b = Number(coeffs[1])
      } else {
        a = 0
        if (coeffs[0] === '-') b = -1
        else if (coeffs[0] === '+') b = 1
        else b = Number(coeffs[0])
      }
      const n = den.includes('^') // en cas de réponse fausse on modifie l’exposant.
        ? Number(den.split('^')[1].replace('}', '')) + (rep ? 0 : 1)
        : (rep ? 1 : 2);
      [valeurs, signes] = await trouverValeursEtSignes(page, rep)
      images[0] = b > 0 ? '-∞' : '+∞'
      const valeur = Number(valeurs[1])
      if (!isNaN(valeur) && Number.isInteger(valeur)) {
        images[1] = `${a.toString()}${b > 1
          ? '+' + b.toString()
          : b < -1
            ? b.toString()
            : b === -1
              ? '-'
              : '+'
        }${ln}${valeurs[1]}→${valeur !== 1
          ? '/' + (valeur ** n).toString()
          : ''
        }`
      } else { // c’est une exponentielle !
        const exposant = valeurs[1].match(/-?\d+/g)
        if (exposant == null) {
          images[1] = `${(a + b).toString()}/${exp}${n}`
        } else if (exposant.length === 1) { // l’exposant est entier
          const expEnt = Number(exposant[0])
          images[1] = `${(a + b * expEnt).toString()}/${exp}${expEnt * n}`
        } else { // l’exposant est rationnel
          const numExp = Number(exposant[0])
          const denExp = Number(exposant[1])
          let num1 = a * denExp + b * numExp
          const gcd1 = pgcdn(num1, denExp)
          num1 = num1 / gcd1
          const den1 = denExp / gcd1
          let num2 = n * numExp
          const gcd2 = pgcdn(num2, denExp)
          num2 = num2 / gcd2
          const den2 = denExp / gcd2
          images[1] = num1.toString()
          if (den1 === 1) { // numérateur entier
            images[1] += '/'
          } else { // numérateur rationnel
            images[1] += `/${num1.toString()}`
          }
          images[1] += `${exp}${den2 === 1 ? num2.toString : num2.toString() + '/' + den2.toString()}`
        }
      }
      images[2] = '0'
    }
      break
    case 7: { // ax+b+cln(dx+e)
      const fonction = getMqChunks(await page.locator('span#affiche_0_2').innerText())[0].replaceAll(/\s/g, '')
      const defFonction = fonction.split('=')[1].replaceAll(/\s/g, '')
      const text = defFonction.split('\\ln')
      let c
      if (text[0][text[0].length - 1] === '-') {
        c = -1
        text[0] = text[0].slice(0, -1)
      } else if (text[0][text[0].length - 1] === '+') {
        c = 1
        text[0] = text[0].slice(0, -1)
      } else {
        const cString = text[0].match(/(-?\+?\d+)$/)[0]
        text[0] = text[0].slice(0, -cString.length)
        c = Number(cString)
      }
      const [a, b] = extraireCoeffAffine(text[0])
      const lnContent = text[1].replace('(', '').replace(')', '').replace('\\left', '').replace('\\right', '')
      const [d, e] = extraireCoeffAffine(lnContent);
      [valeurs, signes] = await trouverValeursEtSignes(page, rep)
      // plein de cas à gérer
      if (d > 0) { // il faut que dx+e soit positif -e/d est la borne gauche et +∞ la borne droite
        images[0] = signes[0] === '-' ? '+∞' : '-∞'
        // on vérifie si il y a une valeur intermédiaire
        if (valeurs.length !== 2) { // la dérivée change de signe en(-ae-cd)/ad c’est la valeurs[1]
          // il faut donc mettre l’image de valeurs[1]
          if (isNaN(valeurs[1])) { // c’est pas juste un entier relatif
            const [num1, den1] = calculNumDenReduit(valeurs[1], (num, den) => a * num + b * den, (num, den) => den)
            const [num2, den2] = calculNumDenReduit(valeurs[1], (num, den) => d * num + e * den, (num, den) => den)
            images[1] = `${den1 === 1 ? num1.toString() : num1.toString() + '/' + den1.toString() + '→'}`
            images[1] += `${c > 1 ? '+' + c.toString() : c < -1 ? c.toString() : c === -1 ? '-' : '+'}`
            images[1] += ln + `${den2 === 1 ? num2.toString() : num2.toString() + '/' + den2.toString()}`
            images[1] += '→'
          } else {
            const antecedent = Number(valeurs[1])
            images[1] = `${(a * antecedent + b).toString()}`
            images[1] += `${c > 1 ? '+' + c.toString() : c < -1 ? c.toString() : c === -1 ? '-' : '+'}`
            images[1] += ln + `${(d * antecedent + e).toString()}`
            images[1] += '→'
          }
        }
        // c’est le signe de a qui compte
        images.push(signes[signes.length - 1] === '+' ? '+∞' : '-∞')
      } else { // d < 0 -∞ est la borne gauche et -e/d est la borne droite
        images[0] = signes[0] === '-' ? '+∞' : '-∞'
        if (valeurs.length !== 2) { // la dérivée change de signe en(-ae-cd)/ad c’est la valeurs[1]
          // il faut donc mettre l’image de valeurs[1]
          if (isNaN(valeurs[1])) { // c’est pas juste un entier relatif
            const [num1, den1] = calculNumDenReduit(valeurs[1], (num, den) => a * num + b * den, (num, den) => den)
            const [num2, den2] = calculNumDenReduit(valeurs[1], (num, den) => d * num + e * den, (num, den) => den)
            images[1] = `${den1 === 1 ? num1.toString() : num1.toString() + '/' + den1.toString() + '→'}`
            images[1] += `${c > 1 ? '+' + c.toString() : c < -1 ? c.toString() : c === -1 ? '-' : '+'}`
            images[1] += ln + `${den2 === 1 ? num2.toString() : num2.toString() + '/' + den2.toString()}`
            images[1] += '→'
          } else {
            const antecedent = Number(valeurs[1])
            images[1] = `${(a * antecedent + b).toString()}`
            images[1] += `${c > 1 ? '+' + c.toString() : c < -1 ? c.toString() : c === -1 ? '-' : '+'}`
            images[1] += ln + `${(d * antecedent + e).toString()}`
            images[1] += '→'
          }
        }
        images.push(signes[signes.length - 1] === '+' ? '+∞' : '-∞')
      }
    }
      break
    case 8: { // ax²+bx+c dérivée : 2ax+b il y a 1 valeur intermédiaire : -b/2a
      const fonction = (await page.locator('span#affiche_0_2').innerText()).split('=')[1].split('$')[0]
      // a et b peuvent être fractionnaires ! b, c et d peuvent être nuls...
      let [, a, b, c] = extraireCoeffDeg3(fonction)
      a = normaliseFraction(a)
      b = normaliseFraction(b)
      c = normaliseFraction(c);
      [valeurs, signes] = await trouverValeursEtSignes(page, rep)
      // On va calculer les images
      images[0] = a.num > 0 ? '+∞' : '-∞'
      for (let i = 1; i < valeurs.length - 1; i++) {
        let numerateur, denominateur, gcd, numReduit, denReduit
        if (isNaN(valeurs[i])) { // c’est pas juste un entier relatif
          const [num, den] = valeurs[i].split('/').map((elt) => Number(elt))
          numerateur = a.num * b.den * c.den * num ** 2 + b.num * a.den * c.den * den * num + c.num * a.den * b.den * den ** 2
          denominateur = a.den * b.den * c.den * den ** 2
          gcd = pgcdn(numerateur, denominateur)
          numReduit = numerateur / gcd
          denReduit = denominateur / gcd
        } else {
          const antecedent = Number(valeurs[i])
          numerateur = a.num * b.den * c.den * antecedent ** 2 + b.num * a.den * c.den * antecedent + c.num * a.den * b.den
          denominateur = a.den * b.den * c.den
          gcd = pgcdn(numerateur, denominateur)
          numReduit = numerateur / gcd
          denReduit = denominateur / gcd
        }
        if (Number.isInteger(numReduit / denReduit)) {
          images[i] = (numReduit / denReduit).toString()
        } else {
          images[i] = denReduit > 0
            ? numReduit.toString() + '/' + denReduit.toString()
            : (-numReduit).toString() + '/' + (-denReduit).toString()
        }
      }
      images.push(a.num > 0 ? '+∞' : '-∞')
    }
      break
  }
  await changeFleches(page, signes)
  await renseigneImages(page, images)
  return await answer(page, rep)
}

/**
 * Teste toutes les répétitions de la section squelettemtg32_NbAntecedents_Captk avec les params fournis
 * @param {Page} page
 * @param {object} options
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {string} options.numProgression N° de la section dans le graphe
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { numProgression }) {
  // on enregistre les réponses true ou false à 50/50 dans des arrays contenant autant de valeurs que d’essais
  const reponses = []
  while (reponses.length < (params.nbrepetitions ?? 1)) {
    const reps = []
    for (let i = 0; i < params.nbchances ?? 2; i++) { // nbchances est à 2 pour toutes les sections
      reps.push(randomBool())
    }
    reponses.push(reps)
  }
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (!total) {
    // faut aller chercher ça dans la valeur par défaut exportée par la section
    // const { sectionParams } = await import(`${dir}section${section}.js`)
    // const nbRepetitionsParam = sectionParams.parametres.find(([nom]) => nom === 'nbrepetitions')
    // // si la section ne l’exporte pas le modèle fixe ça à 1
    // total = nbRepetitionsParam ? nbRepetitionsParam[1] : 1
    // ce qui précède fait planter playwright sur du js récent utilisé dans le code j3p, probablement à cause du wrapper esm
    // car ce js récent est largement assez vieux pour être compris par le chromium et le node utilisé
    // en attendant de passer nos tests en esm dans la v2, on force ici
    total = 1
  }
  let nbChances = params.nbchances
  if (!nbChances) {
    // idem, faut aller voir les params exportés par la section, on a du bol nos 3 sections ont la même valeur, on le met en dur ici
    // eh bien, c’est à creuser, car il semblerait que EtudeFonction_variations ne donne pas de deuxième chance :-(
    nbChances = 2
  }
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    const reps = reponses[numQuestion - 1]
    // boucle essais
    while (numEssais <= nbChances) {
      await checkEtat(page, { numQuestion, score, total, numProgression })
      const rep = reps[numEssais - 1]
      const ok = await runOne(page, rep)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await checkEtat(page, { numQuestion, score, total, numProgression })
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe etudeFonctionVariations_datas0.js (que l’on charge ici)
 *  (7 fonctions différentes)
 */
export async function test (page) {
  page.setDefaultTimeout(5000)

  const { graphe } = await import('./etudeFonctionVariations_datas0.js')
  const numProgressionMax = 8
  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })
  // et on lance la batterie de tests par défaut
  const nodeScores = []
  let numProgression = 1
  for (const [id, section, branchements] of graphe) {
    const params = branchements[1] // le troisième élément de chaque noeud est un array avec un branchement et ensuite les params...
    if (section === 'EtudeFonction_variations') { // ne pas éxécuter de runAll sur le noeud 'Fin'...
      nodeScores.push({ id, score: await runAll(page, params, { numProgression }), nbRepetitions: params.nbrepetitions })
      numProgression++
      if (numProgression <= numProgressionMax) {
        await clickFirstDialogOk(page)
      }
    }
  }
  await checkBilan(page, nodeScores)
  return true
}
