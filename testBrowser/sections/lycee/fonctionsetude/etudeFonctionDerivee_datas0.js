export const graphe = [
  [
    '1',
    'EtudeFonction_derivee',
    [
      {
        nn: '2',
        pe: 'sans condition',
        conclusion: 'Un autre type de fonction',
        label: 'Sans condition'
      },
      {
        imposer_fct: '',
        modele: [1],
        nbrepetitions: 3,
        indication: '',
        limite: '',
        nbchances: 2,
        imposer_domaine: []
      }
    ]
  ],
  [
    '2',
    'EtudeFonction_derivee',
    [
      {
        nn: '3',
        pe: 'sans condition',
        conclusion: 'Un autre type de fonction',
        label: 'Sans condition'
      },
      {
        imposer_fct: '',
        modele: [2],
        nbrepetitions: 3,
        indication: '',
        limite: '',
        nbchances: 2,
        imposer_domaine: []
      }
    ]
  ],
  [
    '3',
    'EtudeFonction_derivee',
    [
      {
        nn: '4',
        pe: 'sans condition',
        conclusion: 'Un autre type de fonction',
        label: 'Sans condition'
      },
      {
        imposer_fct: '',
        modele: [3],
        nbrepetitions: 3,
        indication: '',
        limite: '',
        nbchances: 2,
        imposer_domaine: []
      }
    ]
  ],
  [
    '4',
    'EtudeFonction_derivee',
    [
      {
        nn: '5',
        pe: 'sans condition',
        conclusion: 'Un autre type de fonction',
        label: 'Sans condition'
      },
      {
        imposer_fct: '',
        modele: [4],
        nbrepetitions: 3,
        indication: '',
        limite: '',
        nbchances: 2,
        imposer_domaine: []
      }
    ]
  ],
  [
    '5',
    'EtudeFonction_derivee',
    [
      {
        nn: '6',
        pe: 'sans condition',
        conclusion: 'Un autre type de fonction',
        label: 'Sans condition'
      },
      {
        imposer_fct: '',
        modele: [5],
        nbrepetitions: 3,
        indication: '',
        limite: '',
        nbchances: 2,
        imposer_domaine: []
      }
    ]
  ],
  [
    '6',
    'EtudeFonction_derivee',
    [
      {
        nn: '7',
        pe: 'sans condition',
        conclusion: 'Un autre type de fonction',
        label: 'Sans condition'
      },
      {
        imposer_fct: '',
        modele: [6],
        nbrepetitions: 3,
        indication: '',
        limite: '',
        nbchances: 2,
        imposer_domaine: []
      }
    ]
  ],
  [
    '7',
    'EtudeFonction_derivee',
    [
      {
        nn: '8',
        pe: 'sans condition',
        conclusion: 'Un autre type de fonction',
        label: 'Sans condition'
      },
      {
        imposer_fct: '',
        modele: [7],
        nbrepetitions: 3,
        indication: '',
        limite: '',
        nbchances: 2,
        imposer_domaine: []
      }
    ]
  ],
  [
    '8',
    'EtudeFonction_derivee',
    [
      {
        nn: '9',
        pe: 'sans condition',
        conclusion: 'Un autre type de fonction',
        label: 'Sans condition'
      },
      {
        imposer_fct: '',
        modele: [8],
        nbrepetitions: 3,
        indication: '',
        limite: '',
        nbchances: 2,
        imposer_domaine: []
      }
    ]
  ],
  [
    '9',
    'fin',
    [
      null
    ]
  ]
]
