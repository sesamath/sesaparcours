export const graphe = [
  [
    '1',
    'calculTerme',
    [
      {
        nn: '2',
        score: 'sans+condition',
        conclusion: ''
      },
      {
        typeSuite: 'arithmetique', //, 'geometrique', // 'arithmeticoGeometrique' pas fait pour l’instant
        nbchances: 2,
        nbrepetitions: 5
      }
    ]
  ],
  [
    '2',
    'calculTerme',
    [
      {
        nn: '3',
        score: 'sans+condition',
        conclusion: 'Fin'
      },
      {
        typeSuite: 'geometrique', //, 'geometrique', // 'arithmeticoGeometrique' pas fait pour l’instant
        nbchances: 2,
        nbrepetitions: 5
      }
    ]
  ],
  [
    '3',
    'calculTerme',
    [
      {
        nn: '4',
        score: 'sans+condition',
        conclusion: 'Fin'
      },
      {
        typeSuite: 'arithmeticoGeometrique', //, 'geometrique', // 'arithmeticoGeometrique' pas fait pour l’instant
        nbchances: 2,
        nbrepetitions: 5
      }
    ]
  ], [
    '4',
    'fin',
    [
      null
    ]
  ]
]
