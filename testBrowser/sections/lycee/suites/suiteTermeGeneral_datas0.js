export const graphe = [
  [
    '1',
    'SuiteTermeGeneral',
    [
      {
        nn: '2',
        score: 'sans+condition',
        conclusion: 'Fin'
      },
      {
        nbrepetitions: 6,
        nbchances: 2,
        typeSuite: 'Arithmetique',
        afficherPhrase: true
      }
    ]
  ],
  [
    '2',
    'SuiteTermeGeneral',
    [
      {
        nn: '3',
        score: 'sans+condition',
        conclusion: 'Fin'
      },
      {
        nbrepetitions: 6,
        nbchances: 2,
        typeSuite: 'Geometrique',
        afficherPhrase: true
      }
    ]
  ],
  [
    '3',
    'SuiteTermeGeneral',
    [
      {
        nn: '4',
        score: 'sans+condition',
        conclusion: 'Fin'
      },
      {
        nbrepetitions: 6,
        nbchances: 2,
        typeSuite: 'arithmeticogeometrique',
        afficherPhrase: true
      }
    ]
  ],
  [
    '4',
    'fin',
    [null]
  ]
]
