export const graphe = [
  [
    '1',
    'natureSuite',
    [
      {
        nn: '2',
        score: 'sans+condition',
        conclusion: 'Fin de l‘activité'
      },
      {
        nbrepetitions: 4,
        niveau: 'TES',
        nbchances: 2
      }
    ]
  ],
  [
    '2',
    'fin',
    [
      null
    ]
  ]
]
