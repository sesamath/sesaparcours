import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkFeedbackCorrection, checkEtat } from 'testBrowser/helpers/checks'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { clickSuite, clickSectionSuivante, clickOk } from 'testBrowser/helpers/actions'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { InputMqHandler } from 'testBrowser/helpers/InputMqHandler'
import { getMqChunks } from 'testBrowser/helpers/text'

/**
 *
 * @param page
 * @param {string} repIndice l’indice du premier terme
 * @param {string} repPremierTerme sa valeur
 * @param {'géométrique'|'arithmétique'} nature la 'géométrique' ou 'arithmétique'
 * @param {string} repRaison la raison de la suite
 * @returns {Promise<*>}
 */
async function answer (page, repIndice, repPremierTerme, nature, repRaison) {
  const inputNature = page.locator('div#MepMG input')
  await inputNature.clear()
  await inputNature.type(nature)
  const inputRaison = new InputMqHandler(page, { index: 0 })
  const inputIndice = new InputMqHandler(page, { index: 1 })
  const inputPremierTerme = new InputMqHandler(page, { index: 2 })
  await inputRaison.clear()
  await inputRaison.type(repRaison)
  await inputIndice.clear()
  await inputIndice.type(repIndice)
  await inputPremierTerme.clear()
  await inputPremierTerme.type(repPremierTerme)
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean} rep si true on fournit la bonne réponse
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep) {
  const premierTermeInfo = await page.locator('span#affiche_0_2>span.mq-selectable').innerText()
  const recurenceInfo = await page.locator('span#affiche_0_6>span.mq-selectable').innerText()
  const recurenceFormule = getMqChunks(recurenceInfo)[0].split('=')[1]
  const chunks = recurenceFormule.match(/(-?[0-9]+)|[a-z]_n|(\+?[0-9]+)/g)
  let nature, raison
  if (chunks[0].includes('_n')) {
    nature = rep ? 'arithmétique' : 'géométrique'
    raison = chunks[1].replace('+', '')
  } else {
    nature = rep ? 'géométrique' : 'arithmétique'
    raison = chunks[0]
  }
  const indicePremierTerme = Number(premierTermeInfo.split('=')[0].charAt(3))
  const premierTerme = premierTermeInfo.match(/[-?0-9,]+/g)[1]
  logIfVerbose(`premier terme : U${indicePremierTerme}=${premierTerme} et recurrence : ${recurenceInfo}`)
  const repIndice = rep ? indicePremierTerme.toString() : (1 - indicePremierTerme).toString()
  const repPremierTerme = rep ? premierTerme : premierTerme + '1'
  await answer(page, repIndice, repPremierTerme, nature, raison)
  await clickOk(page)
  await checkFeedbackCorrection(page, rep, '#MepMD')
  return rep
}

/**
 * Teste toutes les répétitions de la section natureSuite avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {number} [options.numProgression] le n° de section dans le parcours
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { numProgression } = {}) {
  // on génère de l’aléatoire
  const reponses = []
  while (reponses.length < params.nbrepetitions) {
    reponses.push([Math.random() < 0.5, Math.random() < 0.5])
  }
  // attend l’énoncé
  await page.waitForSelector('span#affiche_1')
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  let nbChances = params.nbchances
  if (typeof nbChances !== 'number' || nbChances < 1) {
    logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // un raccourci (async)
  const myCheckEtat = () => checkEtat(page, { numQuestion, score, total, numProgression })
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    // boucle essais
    while (numEssais <= nbChances) {
      await myCheckEtat()
      const rep = reponses[numQuestion - 1][numEssais - 1]
      const ok = await runOne(page, rep)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await myCheckEtat()
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe natureSuite_datas0.js (que l’on charge ici)
 */
export async function test (page) {
  const { graphe } = await import('./natureSuite_datas0.js')

  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })

  // et on lance la batterie de tests par défaut
  const { params, id } = getNodeInfos(graphe, 'natureSuite')
  const score = await runAll(page, params)
  await checkBilan(page, [{ id, score, nbRepetitions: params.nbrepetitions }])
  // on clique pas sur suivant car y’en a pas
  return true
}
