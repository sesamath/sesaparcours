import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkFeedbackCorrection, checkEtat } from 'testBrowser/helpers/checks'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { clickSuite, clickSectionSuivante, clickOk, clickFirstDialogOk } from 'testBrowser/helpers/actions'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { InputMqHandler } from 'testBrowser/helpers/InputMqHandler'
import { getMqChunks } from 'testBrowser/helpers/text'
import { arrondiString } from './calculTerme'
/**
 *
 * @param page
 * @param {string} valeurTerme la valeur à taper dans la zone de saisie (correcte ou pas) séparateur décimal '.' ou ',' on s’en fiche l’input Mathquill convertit les . en ,
 * @returns {Promise<*>}
 */
async function answer (page, valeurTerme) {
  const inputValeur = new InputMqHandler(page, { index: 0 })
  await inputValeur.clear()
  await inputValeur.typeClavier(valeurTerme)
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean} rep si true on fournit la bonne réponse
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep) {
  const premierTermeInfo = await page.locator('span#affiche_0_2>span.mq-selectable').innerText()
  const recurenceInfo = await page.locator('span#affiche_0_6>span.mq-selectable').innerText()
  const recurenceFormule = getMqChunks(recurenceInfo)[0].split('=')[1]
  const chunks = recurenceFormule.match(/(-?[0-9,]+)|[a-z]_n|(\+?-?[0-9,]+)/g)
  let nature, raison, a
  if (chunks[0].includes('_n')) {
    nature = 'arithmétique'
    raison = Number(chunks[1].replace('+', '').replace(',', '.'))
  } else if (chunks.length === 2) {
    nature = 'géométrique'
    raison = Number(chunks[0].replace(',', '.'))
  } else {
    a = Number(chunks[0].replace(',', '.'))
  }
  const indicePremierTerme = Number(premierTermeInfo.split('=')[0].charAt(3))
  const premierTerme = Number(premierTermeInfo.match(/-?[0-9,]+/g)[1].replace(',', '.'))
  let repValeur, exposant
  if (indicePremierTerme === 0) { // cas le plus simple Un=U0 + kn ou Un = U0*k^n
    exposant = rep ? 'n' : '(n-1)'
  } else {
    exposant = rep ? '(n-1)' : 'n'
  }
  if (nature === 'géométrique') {
    logIfVerbose(`suite géométrique de raison ${raison}`)
    if (raison < 0) {
      repValeur = `${premierTerme === 1 ? '' : premierTerme === -1 ? '-' : premierTerme.toString() + '×'}(${raison.toString()})^${exposant}`
    } else {
      repValeur = `${premierTerme === 1 ? '' : premierTerme === -1 ? '-' : premierTerme.toString() + '×'}${raison.toString()}^${exposant}`
    }
  } else if (nature === 'arithmétique') {
    logIfVerbose(`suite arithmétique de raison ${raison}`)
    if (exposant === 'n') {
      repValeur = `${premierTerme === 0 ? '' : premierTerme.toString()}${raison < 0 ? '-' : '+'}${Math.abs(raison) === 1 ? '' : Math.abs(raison).toString()}n`
    } else {
      repValeur = `${premierTerme === 0 ? '' : arrondiString(premierTerme - raison, 3)}${raison < 0 ? '-' : '+'}${Math.abs(raison) === 1 ? '' : Math.abs(raison).toString()}n`
    }
  } else { // Y a-t-il des suites arithmético-géométriques ?
    const delta = getMqChunks(await page.locator('span#affiche_1_4').innerText())[0].split('_n')[2]
    const y0 = getMqChunks(await page.locator('span#affiche_2_4').innerText())[0].split('=')[1].replace('−', '-')
    let numY0, denY0
    if (y0.includes('frac')) {
      [numY0, denY0] = y0.match(/[-?0-9]+/g)
    } else {
      numY0 = y0.match(/[0-9,]+/g)[0]
      denY0 = 1
    }
    const y0Negatif = y0[0] === '-'
    let deltaNum, deltaDen
    if (delta.includes('frac')) {
      [deltaNum, deltaDen] = delta.match(/[0-9]+/g)
    } else {
      deltaNum = delta.match(/[0-9,]+/g)[0]
      deltaDen = '1'
    }
    const deltaNegatif = delta[0] === '-'
    repValeur = ''
    if (y0Negatif) {
      repValeur += '-'
    }
    // si le premier terme est 1 ou -1, le 1× ne doit pas s'écrire (simplification d’écriture)
    if (!(Math.abs(numY0) === 1 && denY0 === 1)) {
      repValeur += numY0
      repValeur += denY0 !== 1 ? '/' + denY0 : ''
      repValeur += '→×'
    }
    if (a < 0) {
      repValeur += `(${arrondiString(a, 3)})`
    } else {
      repValeur += `${arrondiString(a, 3)}`
    }
    repValeur += `^${rep ? (indicePremierTerme === 0 ? 'n' : 'n-1') : (indicePremierTerme === 0 ? 'n-1' : 'n')}→`
    repValeur += deltaNegatif ? '+' : '-'
    repValeur += deltaNum + (deltaDen === '1' ? '' : `/${deltaDen}→`)
  }
  logIfVerbose(`repValeur = ${repValeur}`)
  await answer(page, repValeur)
  await clickOk(page)
  await checkFeedbackCorrection(page, rep, '#MepMD')
  return rep
}

/**
 * Teste toutes les répétitions de la section calculTerme avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {string} [options.numProgression] id du nœud dans le graphe
 *
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { numProgression } = {}) {
  // on génère de l’aléatoire
  const reponses = []
  while (reponses.length < params.nbrepetitions ?? 1) {
    reponses.push([Math.random() < 0.5, Math.random() < 0.5])
  }
  // attend l’énoncé
  await page.waitForSelector('span#affiche_0')
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    await logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  let nbChances = params.nbchances
  if (typeof nbChances !== 'number' || nbChances < 1) {
    await logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // un raccourci (async)
  const myCheckEtat = () => checkEtat(page, { numQuestion, score, total, numProgression })
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    // boucle essais
    while (numEssais <= nbChances) {
      await myCheckEtat()
      const rep = reponses[numQuestion - 1][numEssais - 1]
      const ok = await runOne(page, rep)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await myCheckEtat()
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe suiteTermeGeneral_datas0.js (que l’on charge ici)
 */
export async function test (page) {
  const { graphe } = await import('./suiteTermeGeneral_datas0.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })
  // et on lance la batterie de tests par défaut
  const nodeScore = []
  for (const node of graphe) {
    if (node[0] === '4') break
    const { params, id } = getNodeInfos(graphe, 'SuiteTermeGeneral', node[0])
    nodeScore.push({ id, score: await runAll(page, params, { numProgression: id }), nbRepetitions: 6 })
    if (node[0] !== '3') {
      await clickFirstDialogOk(page)
    }
  }
  await checkBilan(page, nodeScore)
  // on clique pas sur suivant car y’en a pas
  return true
}
