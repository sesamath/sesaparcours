import { loadGraphe } from 'testBrowser/helpers/browser'
import { checkBilan, checkFeedbackCorrection, checkEtat } from 'testBrowser/helpers/checks'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'
import { clickSuite, clickSectionSuivante, clickOk } from 'testBrowser/helpers/actions'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { InputMqHandler } from 'testBrowser/helpers/InputMqHandler'
import { getMqChunks } from 'testBrowser/helpers/text'

/**
 * On a besoin d’arrondir les valeurs passées en flottant avec une précision donnée afin de les taper dans la zone de saisie sans les scories inhérentes aux flottants
 * @param nb
 * @param nbDecimales
 * @returns {string|*}
 */
export function arrondiString (nb, nbDecimales) {
  if (!Number.isInteger(nbDecimales)) throw Error('maxDecimales doit être entier dans j3pArrondi')
  if (nbDecimales > 0) {
    if (Number.isInteger(nb)) return nb.toString()
    const str = nb.toFixed(nbDecimales)
    return str.replace(/(\d+\.?\d*)/g, (p) => parseFloat(p))
  }
  if (nbDecimales === 0) return Math.round(nb).toString()
  throw Error('maxDecimales doit être positif ou nul dans j3pArrondi')
}

/**
 *
 * @param page
 * @param {string} valeurTerme la valeur à taper dans la zone de saisie (correcte ou pas) séparateur décimal '.' ou ',' on s’en fiche l’input Mathquill convertit les . en ,
 * @returns {Promise<*>}
 */
async function answer (page, valeurTerme) {
  const inputValeur = new InputMqHandler(page, { index: 0 })
  await inputValeur.clear()
  await inputValeur.type(valeurTerme)
}

/**
 * Joue un essai pour une répétition (entre une valeur, clique OK, vérifie le feedback, ne clique pas sur suite
 * (en sortie on est encore en correction s’il reste des essais, navigation sinon)
 * @param {Page} page
 * @param {boolean} rep si true on fournit la bonne réponse
 * @return {Promise<boolean>}
 */
export async function runOne (page, rep) {
  const premierTermeInfo = await page.locator('span#affiche_0_2>span.mq-selectable').innerText()
  const recurenceInfo = await page.locator('span#affiche_0_6>span.mq-selectable').innerText()
  const recurenceFormule = getMqChunks(recurenceInfo)[0].split('=')[1]
  const chunks = recurenceFormule.match(/(-?[0-9,]+)|[a-z]_n|(\+?-?[0-9,]+)/g)
  let nature, raison, a, b
  if (chunks[0].includes('_n')) {
    nature = 'arithmétique'
    raison = Number(chunks[1].replace('+', '').replace(',', '.'))
  } else if (chunks.length === 2) {
    nature = 'géométrique'
    raison = Number(chunks[0].replace(',', '.'))
  } else {
    nature = 'arithmético-géométrique'
    a = Number(chunks[0].replace(',', '.'))
    b = Number(chunks[2].replace(',', '.'))
  }
  const consigne = nature === 'arithmético-géométrique'
    ? (await page.locator('span#affiche_4').innerText()).replaceAll('\n', '')
    : (await page.locator('span#affiche_3').innerText()).replaceAll('\n', '')
  const indiceTerme = Number(consigne.match(/[0-9]+/)[0])
  let consigneArrondir = consigne.match(/\(.*\)/g)
  if (!consigneArrondir) {
    throw Error('Pas de consigne trouvée')
  }
  consigneArrondir = consigneArrondir[0]
  let arrondir
  if (consigneArrondir.includes('exacte')) {
    arrondir = null
    logIfVerbose('On n’arrondi pas')
  } else if (consigneArrondir.includes('unité')) {
    arrondir = 0
    logIfVerbose('On arrondi à l’unité')
  } else {
    try {
      arrondir = Number(getMqChunks(consigneArrondir)[0].match(/[0-9]+/g)[1])
      logIfVerbose(`On arrondi à 10⁻${arrondir}`)
    } catch (error) {
      logError('On a pas retrouvé ce que l’on attendait dans l’énoncé', error)
    }
  }

  const indicePremierTerme = Number(premierTermeInfo.split('=')[0].charAt(3))
  const premierTerme = Number(premierTermeInfo.match(/-?[0-9,]+/g)[1].replace(',', '.'))
  let valeurTerme
  const n = indiceTerme - indicePremierTerme
  if (nature === 'géométrique') {
    valeurTerme = premierTerme * (rep ? 1 : raison) * raison ** n
  } else if (nature === 'arithmétique') {
    valeurTerme = premierTerme + (rep ? 0 : raison) + raison * n
  } else {
    if (a === 1) {
      valeurTerme = premierTerme + (rep ? 0 : b) + b * n
    } else {
      const r = b / (1 - a)
      valeurTerme = a ** (rep ? n : n + 1) * (premierTerme - r) + r
    }
  }
  let repValeur
  if (arrondir !== null) {
    repValeur = arrondiString(valeurTerme, arrondir)
  } else {
    repValeur = arrondiString(valeurTerme, 8)// on veut une valeur exacte... mais si valeurTerme est un truc comme 3.799999999999999 à cause du format flottant, il faut qu’on arrondisse pour obtenir 3.800000000
  }
  logIfVerbose(`valeurTerme = ${valeurTerme} et repValeur = ${repValeur}`)
  await answer(page, repValeur)
  await clickOk(page)
  await checkFeedbackCorrection(page, rep, '#MepMD')
  return rep
}

/**
 * Teste toutes les répétitions de la section calculTerme avec les params fournis
 * @param {Page} page
 * @param {SectionParams} params Les paramètres de cette section déjà chargée dans le navigateur
 * @param {Object} [options]
 * @param {string} [options.numProgression] N° de la section dans le parcours
 *
 * @return {Promise<number>} Le score
 */
export async function runAll (page, params, { numProgression } = {}) {
  // on génère de l’aléatoire
  const reponses = []
  while (reponses.length < params.nbrepetitions ?? 1) {
    reponses.push([Math.random() < 0.5, Math.random() < 0.5])
  }
  // attend l’énoncé
  await page.waitForSelector('span#affiche_0')
  let numQuestion = 1
  let score = 0
  let total = params.nbrepetitions
  if (typeof total !== 'number' || total < 1) {
    logError('nbrepetitions invalide avec les paramètres', params)
    total = 3
  }
  let nbChances = params.nbchances
  if (typeof nbChances !== 'number' || nbChances < 1) {
    logError('nbchances invalide avec les paramètres', params)
    nbChances = 1
  }
  // un raccourci (async)
  const myCheckEtat = () => checkEtat(page, { numQuestion, score, total, numProgression })
  // et on y va, boucle questions
  while (numQuestion <= total) {
    let numEssais = 1
    // boucle essais
    while (numEssais <= nbChances) {
      await myCheckEtat()
      const rep = reponses[numQuestion - 1][numEssais - 1]
      const ok = await runOne(page, rep)
      if (!ok && numEssais < nbChances) {
        // on est encore en état correction, faut refaire un essai
        numEssais++
        continue
      }
      // dernier essai
      if (ok) score++
      await myCheckEtat()
      numQuestion++
      numEssais = nbChances + 1 // pour sortir de la boucle essais
      // on repasse en énoncé
      if (numQuestion <= total) await clickSuite(page)
    } // boucle essais
  } // boucle questions
  // on a fini toutes les répétitions, on clique sur section suivante
  await clickSectionSuivante(page)
  // et retourne le score final
  return score
}

/**
 * Un scénario par défaut avec le graphe calculTerme_datas0.js (que l’on charge ici)
 */
export async function test (page) {
  const { graphe } = await import('./calculTerme_datas0.js')

  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })
  // et on lance la batterie de tests par défaut
  const { params, id } = getNodeInfos(graphe, 'calculTerme', '1')
  let score = await runAll(page, params, { numProgression: id })
  const { params: params2, id: id2 } = getNodeInfos(graphe, 'calculTerme', '2')
  score += await runAll(page, params2, { numProgression: id2 })
  const { params: params3, id: id3 } = getNodeInfos(graphe, 'calculTerme', '3')
  score += await runAll(page, params3, { numProgression: id3 })

  await checkBilan(page, [{ id: id2, score, nbRepetitions: 15 }])
  // on clique pas sur suivant car y’en a pas
  return true
}
