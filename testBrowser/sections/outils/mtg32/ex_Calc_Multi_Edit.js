import { fixArrondi } from 'src/lib/utils/number'

import { clickOk, clickSectionSuivante, clickSuite } from 'testBrowser/helpers/actions'
import { getContentNormalized, loadGraphe } from 'testBrowser/helpers/browser'
import { assertEquals, checkBilan, checkEtat, checkFeedbackCorrection } from 'testBrowser/helpers/checks'
import { getNodeInfos } from 'testBrowser/helpers/j3p'
import { logIfVerbose } from 'testBrowser/helpers/log'
import { dropLatex } from 'testBrowser/helpers/text'

// les coefs à appliquer en prenant le litre comme ref
const coefs = {
  mm3: 0.000001,
  mL: 0.001,
  cm3: 0.001,
  cL: 0.01,
  dL: 0.1,
  L: 1,
  dm3: 1,
  daL: 10,
  hL: 100,
  m3: 1000
}

/**
 * Analyse l’énoncé pour trouver les solutions à mettre dans les input de la page
 * @param {Page} page
 * @return {Promise<Array<number>>}
 */
async function getSolutions (page) {
  let enonce = await page.innerText('#texte')
  // on nettoie le mathquill et vire les \n
  enonce = dropLatex(enonce).replace(/\s+/g, ' ')

  const chunks = /On souhaite convertir ([0-9,]+) ([^ ]+) en ([^ ]+)\./.exec(enonce)
  if (!chunks) throw Error(`On a pas trouvé ce qu’on cherchait dans l’énoncé : ${enonce}`)
  const [, nbStr, unitSrc, unitDst] = chunks
  const nbSrc = Number(nbStr.replace(',', '.'))
  logIfVerbose(`On a ${nbSrc} en ${unitSrc} à convertir en ${unitDst}`)
  const coefSrc = coefs[unitSrc]
  if (!coefSrc) throw Error(`unité ${unitSrc} inconnue, il faut venir l’ajouter dans ce test ${__filename}`)
  const litres = nbSrc * coefSrc
  // ça se corse, faut trouver tous les inputs et l’unité voulue pour chacun
  // on prend tous les spans enfants directs de #expression, et on ne garde que ceux qui suivent un .mq-editable
  const spans = await page.locator('#expression math-field ~ span').all()
  const result = []
  for (const span of spans) {
    await span.highlight()
    const unit = await span.innerText({ timeout: 10_000 })
    if (unit != null) result.push(unit)
  }
  const units = result.filter(s => s).map(u => dropLatex(u).replace(/\s/g, ''))
  const solutions = units.map(unit => {
    const coef = coefs[unit]
    if (!coef) throw Error(`unité ${unit} inconnue`)
    const solution = litres / coef
    // solution manque parfois d’arrondi, genre 2.9999999999999999
    return fixArrondi(solution)
  })
  return { solutions, units }
}

/**
 * Remplit les réponses et clique sur ok
 * @param {Page} page
 * @param {Array<string|number>}reponses
 * @return {Promise<void>}
 */
async function setReponses (page, reponses) {
  let i = 1
  reponses = reponses.map(reponse => {
    if (typeof reponse === 'number') reponse = String(fixArrondi(reponse)).replace('.', ',')
    if (typeof reponse !== 'string') throw TypeError(`Il faut number ou string, ${typeof reponse} fourni`)
    return reponse
  })
  logIfVerbose(`On va saisir les réponses ${reponses.join(' et ')}`)
  for (const reponse of reponses) {
    // faut du type pour déclencher les evts clavier, le fill ne déclenche pas le traitement mathquill
    await page.type(`#expressioninputmq${i++}`, reponse)
  }
  await clickOk(page)
}

async function checkCorrection (page, solutions, units) {
  // on vire tous les espaces avant comparaison (y’en a pas toujours entre le = et le nombre qui suit, ou entre le nombre et l’unité
  const phraseSolution = (await getContentNormalized(page, '#solution', true)).replace(/\s/g, '')
  const expected = solutions.map((s, i) => `${s} ${units[i]}`).join(' = ')
    .replace(/\./g, ',') // plus de points
    .replace(/\s/g, '') // espaces virés
    .replace(/([$^[\]()+*-])/g, '\\$1') // faut échapper tous ces caractères
    // faut une regexp car dans la solution y’a en plus l’énoncé, expected doit être la fin
  const re = new RegExp(expected + '$')
  assertEquals(phraseSolution, re)
}

/**
 * Joue une répétition
 * @param {Page} page
 * @param {boolean} ok Si true on saisi la bonne réponse, si false on fait une erreur ×10 sur un input aléatoire au 1er essai
 * @param {boolean} ok2 Si true on saisi la bonne réponse au 2e essai (ignoré si on avait ok)
 * @return {Promise<void>}
 */
export async function runOne (page, ok, ok2) {
  const { solutions, units } = await getSolutions(page)
  // si ko on prend un index au hasard pour lequel on va répondre 10 fois trop
  const indexKo = !ok && Math.floor(Math.random() * solutions.length)
  const reponses = ok ? solutions : solutions.map((s, i) => i === indexKo ? s * 10 : s)
  await setReponses(page, reponses)
  // ici y’a du mathquill
  await checkFeedbackCorrection(page, ok)
  if (!ok) {
    // on saisit des réponses au 2e essai
    await setReponses(page, ok2 ? solutions : reponses)
    await checkFeedbackCorrection(page, ok2)
  }
  // on vérifie la correction affichée (que l’on ait fait des erreurs ou pas c’est la même)
  await checkCorrection(page, solutions, units)
}

/**
 * Teste la section ex_Calc_Multi_Edit avec les données de la ressource 60391cb5491aff7ab7c25071
 * @param {Page} page
 * @param {SectionParams} params
 * @param {Object} [options]
 * @param {string} [options.id] Passer l’id de la section dans le graphe si elle n’est pas la seule (pour le check du score)
 * @return {Promise<boolean>}
 */
export async function runAll (page, params, { id } = {}) {
  const getEtatOpts = (numQuestion, score) => ({ numQuestion, score, total: params.nbrepetitions, numProgression: id })

  // faut attendre que mtg soit chargé et ait fait son rendu
  await page.waitForSelector('#expressioninputmq1')
  // on vérifie que l’init de l’état est ok
  await checkEtat(page, getEtatOpts(1, 0))
  await runOne(page, true)
  await clickSuite(page)
  await checkEtat(page, getEtatOpts(2, 1))
  // à la 2e question, on répond faux puis bon
  await runOne(page, false, true)
  await clickSuite(page)
  await checkEtat(page, getEtatOpts(3, 2))
  // on termine les 8 répétitions en se trompant sur les impairs (3 5 7 9)
  let numQuestion = 3
  let score = 2
  while (numQuestion <= params.nbrepetitions) {
    const ok = !(numQuestion % 2)
    await runOne(page, ok, ok)
    if (numQuestion < params.nbrepetitions) await clickSuite(page)
    if (ok) score++
    numQuestion++
    await checkEtat(page, getEtatOpts(Math.min(numQuestion, params.nbrepetitions), score))
  }
  // le clic sur section suivante doit pas être ici (le résultat du clic va dépendre des branchements qui partent de ce nœud
  return score
}

export async function test (page) {
  const { graphe } = await import('./ex_Calc_Multi_Edit_datas1.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, { graphe })

  // et on lance la batterie de tests par défaut
  const { params, id } = getNodeInfos(graphe, 'ex_Calc_Multi_Edit')
  const score = await runAll(page, params)
  // clic section suivante
  await clickSectionSuivante(page)
  await checkBilan(page, [{ id, score, nbRepetitions: params.nbrepetitions }])
  return true
}
