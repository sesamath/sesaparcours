# Tests fonctionnels automatisés

Il s'agit de piloter un navigateur avec du code pour vérifier que tout se passe comme attendu. On utilise [playwright](https://playwright.dev/) pour ça

`pnpm run` affiche la liste de tous les scripts, vous pouvez lancer `pnpm run|grep test:browser` pour voir la liste des tests playwright existants

Pour la liste des tests individuels : `find testBrowser -name '*.test.*'`

Pour lancer un seul test, c'est `vitest --run testBrowser/…/xxx.test.js`, avec éventuellement des options

Chaque test est un module *.test.js qui déclare une série de test avec `describe` (la série) et `it` (un test contenue dans une série). 

Les autres fichiers sont des modules annexes utilisés par les test.js.

## Utilisation

### paramètres

Pour tous ces tests, vous pouvez passer via l’environnement les paramètres définis dans prefs.js (certains ne sont pas pris en compte par tous les tests)

* `baseUrl=http://localhost:8081/` pour tester le code local (avec un `pnpm start` qui tourne dans un autre terminal), sinon ça teste sur https://j3p.sesamath.dev, attention il ne faut pas utiliser BASE_URL car vitest l’écrase avec '/'
* `CONTINUE_ON_ERROR=1` pour continuer même si une erreur survient
* `DEVTOOLS=1` pour ouvrir les devtools (chromium seulement)
* `HEADLESS=1` Pour lancer les tests en mode headless
* `IGNORE_HTTPS_ERROR=1` pour ne pas s'arrêter à des pbs de certificats (si vous testez sur une url https sans certificat valide)
* `QUIET=1` pour ne sortir que les erreurs
* `RELAX=1` pour ne pas s'arrêter sur une erreur dans le navigateur (mais ça va s'arrêter pour une erreur dans le test)
[//]: # (* `TIMEOUT=xxx` un timeout en ms)
* `VERBOSE=1` Afficher le détail en console (sinon uniquement dans le log)
* `PRODLIKE=1` ou `PRODLIKE=2` pour tester loadAll via prod1.html ou prod2.html

Ça donne alors quelque chose comme `PARAM1=xxx PARAM2=yyy vitest …` ou plutôt `PARAM1=xxx PARAM2=yyy pnpm test:browser <pattern>` pour avoir la config de vitest.config.browser.js
(pattern est un nom du dossier / fichier, pas besoin du chemin complet)


### allSections

Il s'agit de lancer tous les tests existants sur des sections : testBrowser/sections/**/*.test.(js|ts)

`pnpm test:browser:allSections` ou `PARAM1=xxx PARAM2=yyy pnpm test:browser:allSections`

### allScenarios

Il s'agit de lancer tous les scénarios existants, ça reprend des tests de sections mais avec des enchaînements prédéterminés (le chaînage dans un graphe, avec ou sans reprise de lastResultat, …) : testBrowser/scenarios/**/*.test.(js|ts)

`pnpm test:browser:allScenarios` ou `PARAM1=xxx PARAM2=yyy pnpm test:browser:allScenarios`

### loadAll

Il s'agit d’une tâche, définie dans testBrowser/tasks/loadAll.test.js

Cela charge chaque ressource / section et vérifie qu'il n’y a pas de plantage ni de console.error pendant le chargement (cela ne garanti donc pas que la section fonctionne mais au moins elle se charge sans erreur).

Vous pouvez la lancer avec `pnpm test:browser:all` avec éventuellement ces paramètres

* `USAGE=1` prendre une ressource existante en prod pour chaque section (plutôt que le graphe par défaut de la section)
* `RIDS=xxx,yyy` pour ne charger que xxx et yyy (autant que vous voulez dans la liste, séparateur virgule sans espace)
* `SKIP=x` et `LIMIT=y` sauter x ressource de la liste obtenue avec les autres params et se limiter à y ressources en tout


### tasks
Ce sont des tâches génériques, actuellement seulement loadAll

## Écrire un test

### format d’un test de section

En général, un fichier de test de section exporte 3 fonctions
* `test (page: Page): Promise<true>` lance le test de la section, avec les params qu'il a défini pour cette section (en général dans un fichier _datas séparé), rejeter la promesse en cas de pb.
* `runAll (page: Page, params: SectionParams, opts?): Promise<number>` Lance toutes les répétitions de la section (déjà chargée avec params), on peut lui préciser si on veut qu'il réponde juste ou faux à chaque répétition via les options. Doit résoudre la promesse avec le score obtenu (ou la rejeter en cas de pb)
* `runOne (page: Page, rep: boolean)` Joue une seule répétition dans la page déjà chargée, rep permet d’indiquer s'il doit mettre une bonne ou mauvaise réponse.

C'est la fonction `test` qui sera évaluée lors du test allSections (cf les fichiers test.js), en la passant à runTest (de ./run.js), les autres sont là à disposition de scenarios éventuels.

### lancer un test seul localement pour le voir à l'œuvre

On lance un `pnpm start` dans un terminal et en ouvre un autre pour lancer un test avec par exemple `DEVTOOLS=1 baseUrl=http://localhost:8081/ vitest --reporter=verbose --run testBrowser/sections/college/blockly/dessin/figure/blokfigure.test.js --testTimeout=3600000 --hookTimeout=3600000`

* `DEVTOOLS=1` pour lancer le chromium de playwright avec ses devTools ouverts
* `baseUrl=http://localhost:8081/` pour tester le code local (sinon ça teste sur https://j3p.sesamath.dev), attention il ne faut pas utiliser BASE_URL car vitest l’écrase avec '/'
* `--reporter=verbose` pour avoir les traces complètes (mais quand c'est vitest qui coupe pour timeout on a pas plus d’info sur ce qu'il faisait au moment du timeout)
* `--run testBrowser/…` pour préciser le test à lancer
* `--testTimeout=3600000` pour que le timeout d’un test soit très long (sinon on peut toujours faire du `page.pause()` dans le code du test on a que très peu de temps pour aller voir, là on a 1h)
* `--hookTimeout=3600000` idem pour le timeout des hook (afterEach par ex)

## Pbs connus

Pour que playwright fonctionne sur une debian bullseye sans chromium (ni aucun serveur graphique), il faut notamment installer les paquets `ffmpeg libnss3 libatk-bridge2.0-0 libxcomposite1` (ffmpeg n’est pas forcément obligatoire mais beaucoup de ses dépendances le sont pour faire tourner chromium en headless) 
