/**
 * Teste le chargement de toutes les sections et log ce qui sort dans le console.error du navigateur pour chaque section
 * À lancer avec par ex
 * `VERBOSE=1 vitest --config vitest.config.browser.js --run tasks/loadAll`
 * ou pour charger par rid de prod (un par section étant la première dans un graphe)
 * `VERBOSE=1 USAGE=1 vitest --config vitest.config.browser.js --run tasks/loadAll`
 * ou pour charger seulement qq rids
 * `RIDS=sesabibli/xxx,sesabibli/yyy vitest --config vitest.config.browser.js --run tasks/loadAll`
 * @module loadAll
 */
import { existsSync, mkdirSync } from 'node:fs'
import { basename, dirname, join, resolve } from 'node:path'
import { fileURLToPath } from 'node:url'

import { afterAll, afterEach, beforeAll, describe, it } from 'vitest'

import prefs from '../prefs'
import { getFileLogger } from 'testBrowser/helpers/log'
import { getDefaultPage, initCurrentBrowser, MAX_TRIES, purge } from 'testBrowser/helpers/browser'
import { getAdresses, getUsageRids } from 'testBrowser/helpers/j3p'
import { waitMs } from 'testBrowser/helpers/promise'

import { plural } from 'testBrowser/helpers/text'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

const rootDir = join(__dirname, '..', '..')
const logDir = join(rootDir, 'log')
if (!existsSync(logDir)) mkdirSync(logDir, { mode: 0o755 })
const logName = basename(__filename, '.test.js')
const fileLogger = getFileLogger(logName)
const logFile = resolve(logDir, `${logName}.log`)

let adresses = {}
// index dans la liste, pour le log
let current

/**
 * Charge une section ou une ressource (recommence une fois en cas d’erreur avec des failed urls)
 * @param {string} item nomSection ou rid
 * @param {number} [nbTries=0] Passer 1 si on a déjà fait un essai de chargement de cet item
 * @return {Promise<{errors: number, warnings: number}|null>}
 */
async function load (item, nbTries = 0) {
  const failedUrls = []
  // pour recommencer ce chargement
  const retry = async () => {
    const msg = plural(failedUrls.length, { tpl: 'load a planté avec %d pb%s de chargement, on recommence une fois ' }) +
      plural(failedUrls.length, { tpl: '(après purge de %s en erreur)', singular: 'cette url', plural: 'ces urls' })
    await fileLogger(item, msg)
    for (const url of failedUrls) await purge(url)
    await initCurrentBrowser()
    return load(item, nbTries)
  }

  nbTries++

  try {
    const pathSection = adresses[item]
    let url = prefs.baseUrl
    if (pathSection) url += `?testBrowser=1&graphe=[1,"${item}",[{pe:"sans%20condition",nn:"fin",conclusion:"Fin"}]]`
    // sinon, on suppose que c’est un rid, on regarde si on veut un test particulier
    // PRODLIKE et pas PROD car DEV|PROD est init par vite
    else if (process.env.PRODLIKE) url += `prod${process.env.PRODLIKE}.html#${item}`
    else url += `?testBrowser=1&rid=${item}` // on suppose que c’est un rid
    let nbConsoleErrorCalls = 0
    let nbConsoleWarnCalls = 0
    current++
    if (!prefs.quiet) await fileLogger(`Chargement de ${item} [${current}]${pathSection ? ` (dans ${pathSection})` : ''} via ${url}`)

    // gestion du log
    const logWithItem = (type, ...args) => fileLogger(`${item} ${type} :`, ...args)

    // on ajoute le listeners sur les messages pour récupérer les console.error dans le browser
    const consoleListener = (message) => {
      // on a déjà eu du `TypeError: message.type is not a function`
      if (typeof message?.type !== 'function') {
        fileLogger('console listener appelé avec un message sans type', message)
        return
      }
      // Cf https://playwright.dev/docs/api/class-consolemessage
      const type = message.type()
      if (!['warning', 'error'].includes(type)) return
      const isError = type === 'error'
      const args = message.args()
      if (isError) nbConsoleErrorCalls++
      else nbConsoleWarnCalls++
      if (!args.length) args.push(`console.${isError ? 'error' : 'warn'} appelé sans argument`)
      logWithItem(type, ...args)
    }
    // https://playwright.dev/docs/api/class-page#pageonconsole
    const page = await getDefaultPage({ consoleListener, logger: logWithItem })

    // on charge l’url
    await purge(url)
    const start = Date.now()
    await page.goto(url, { timeout: 60000 })
    await page.waitForLoadState('networkidle')
    await waitMs(20) // pour laisser à un éventuel log async le temps d’écrire toutes ses infos
    page.removeListener('console', consoleListener)
    await page.close()
    let conclusion
    if (nbConsoleErrorCalls) {
      if (nbTries < MAX_TRIES && failedUrls.length) {
        return retry()
      }
      conclusion = plural(nbConsoleErrorCalls, { tpl: 'KO avec %d appel%s de console.error' })
      if (nbConsoleWarnCalls) conclusion += plural(nbConsoleWarnCalls, { tpl: ' et %d appel%s de console.warn' })
    } else {
      conclusion = 'ok'
      if (nbConsoleWarnCalls) conclusion += plural(nbConsoleWarnCalls, { tpl: ' mais %d appel%s de console.warn' })
    }
    conclusion += ` (${Date.now() - start}ms)`
    if (nbConsoleErrorCalls || !prefs.quiet) await fileLogger(`${item} ${conclusion}`)
    return { errors: nbConsoleErrorCalls, warnings: nbConsoleWarnCalls }
  } catch (error) {
    if (nbTries < MAX_TRIES && failedUrls.length) {
      return retry()
    }
    await fileLogger(item, 'load a planté', error)
    return null
  }
}

describe('Chargement de toutes les sections', async () => {
  // on regarde si on doit charger adresses.js ou le sections.usage.json
  let liste
  if (process.env.RIDS) {
    liste = process.env.RIDS.split(/[,; ]+/g)
  } else if (['1', 'on', 'true'].includes(process.env.USAGE?.toLowerCase())) {
    liste = await getUsageRids()
  } else {
    adresses = await getAdresses()
    liste = Object.keys(adresses)
  }
  // on tronque si demandé
  const { skip, limit } = prefs
  current = skip // 0 par défaut
  if (limit) {
    liste = liste.slice(skip, skip + limit)
  } else if (skip) liste = liste.slice(skip)

  let nbRessources = 0
  let nbLoaded = 0
  let nbErrors = 0
  let nbWarnings = 0

  // init du browser au début
  beforeAll(() => initCurrentBrowser())

  // fermeture du browser à la fin
  afterAll(async () => {
    if (prefs.browserInstance) {
      await prefs.browserInstance.close()
      prefs.browserInstance = null
    }
    await fileLogger(`loadAll a fini, mis dans ${logFile} (${plural(nbLoaded, { tpl: '%d ressource%s chargée%s' })} sur ${nbRessources}, avec ${plural(nbErrors, { tpl: '%d erreur%s' })} et ${plural(nbWarnings, { tpl: '%d warning%s' })})`)
  })

  // on recrée une nouvelle session du browser toutes les 10 ressources (y’a visiblement des fuites mémoires)
  afterEach(() => async () => {
    if (nbRessources % 10 === 0) await initCurrentBrowser()
  })

  // les tests
  if (liste.length) {
    // et on lance tout ça en créant un test par ressource
    for (const item of liste) {
      it(item, async () => {
        nbRessources++
        const result = await load(item)
        if (result) {
          nbLoaded++
          const { errors, warnings } = result
          if (errors) {
            nbErrors++
            throw Error(`${item} KO avec ${errors} erreurs`)
          }
          if (warnings) nbWarnings++
        } else {
          throw Error(`${item} KO (cf ${logFile} pour les détails)`)
        }
      })
    }
  } else {
    // faut au moins un test
    it('Rien à tester', () => {
      throw Error('Liste de ressources vide')
    })
  }
})
