Liste de ressources
===================

Voici quelques ressources particulières

Avec reprise de données d’une section précédente
------------------------------------------------
* graphe linéaire sur les suites, node2 récupère les données de node1, node4 celles de node3 
http://localhost:8081/?rid=sesabibli/5e048081a726dc6681f2c4e7
* 4 nœuds et à l’issu du 4ème, je reviens au premier (sans condition). Pour arrêter, on utilise maxParcours. Pour les 4 premières questions, les données sont celles de q1 (idem pour les 4 autres qui reprennent les données de q5). Etude de fct : http://localhost:8081/?rid=sesabibli/606de69876b59260866d9537
 
Sans case correction
--------------------
Toutes les vidéoprojection avec corrections à la fin, déclenchement sur espace)
* V700  http://localhost:8081/?rid=sesabibli/32443

Sans case navigation
--------------------
* atome http://localhost:8081/?rid=sesabibli/6098077f125bb16c2faed37c (se tromper au 1er nœud)
* boulier http://localhost:8081/?rid=sesabibli/5fa510a78680fc0aeec0b278

Sans case correction ni navigation
----------------------------------
* Vboulier http://localhost:8081/?rid=sesabibli/5fa510a78680fc0aeec0b278

Autres cas particuliers
-----------------------
* nb d’étapes qui dépend du graphe
  * algo de Dijkstra http://localhost:8081/?rid=sesabibli/5ebfdcc3b0c6c95bf2d3530f
* enchaînement de ressources sans score
  * http://localhost:8081/?rid=sesabibli/5c50587cb2d5904842d941da
