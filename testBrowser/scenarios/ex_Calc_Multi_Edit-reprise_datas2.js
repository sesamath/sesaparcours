// pris dans le rapport bugsnag https://app.bugsnag.com/sesamath/j3p/errors/604376e1ac8f6e0018c73eb7?event_id=6051bab10073bf0cad8c0000

export const graphe = [
  [],
  [
    '1',
    'squelettemtg32_Fact_Param',
    [
      {
        nn: '2',
        score: 'sans+condition'
      },
      {
        ex: 'Fact_MoinsaxMoinsb_Carre_Plus_k_axPlusb_cxPlusd',
        a: 'random',
        b: 'random',
        c: 'random',
        d: 'random',
        e: 'random',
        f: 'random',
        k: '1',
        nbEssais: 8,
        factMax: true,
        nbchances: 2
      }
    ]
  ],
  [
    '2',
    'squelettemtg32_Fact_Param',
    [
      {
        nn: '3',
        score: 'sans+condition'
      },
      {
        ex: 'Fact_aCarrexCarre_Moins_bCarre_Plus_k_axPlusb_cxPlusd',
        a: 'random',
        b: 'random',
        c: 'random',
        d: 'random',
        k: '1',
        nbEssais: 8,
        factMax: true,
        nbchances: 2
      }
    ]
  ],
  [
    '3',
    'squelettemtg32_Fact_Param',
    [
      {
        nn: '4',
        score: 'sans+condition'
      },
      {
        ex: 'Fact_aCarrexCarre_Moins_bCarre_Plus_k_axPlusb_cxPlusd',
        a: 'random',
        b: 'random',
        c: 'random',
        d: 'random',
        k: 'int(rand(0)*3+2)',
        nbEssais: 8,
        factMax: true,
        nbchances: 2
      }
    ]
  ],
  [
    '4',
    'squelettemtg32_Fact_Param',
    [
      {
        nn: '5',
        conclusion: 'C’est fini !',
        score: 'sans+condition'
      },
      {
        ex: 'Fact_aCarrexCarre_Moins_bCarre_Moins_k_axPlusb_cxPlusd',
        a: 'random',
        b: 'random',
        c: 'random',
        d: 'random',
        k: 'random',
        nbEssais: 8,
        factMax: true,
        nbchances: 2
      }
    ]
  ],
  [
    '5',
    'fin'
  ]
]

export const lastResultat = {
  autonomie: false,
  checksum: '86c4ad',
  contenu: {
    noeuds: [
      '1',
      '2',
      '3',
      '4'
    ],
    pe: [
      0,
      0,
      0,
      0
    ],
    scores: [
      0,
      0,
      0,
      0
    ],
    ns: [
      2,
      3,
      4,
      'fin'
    ],
    boucle: [
      1,
      1,
      1,
      1
    ],
    nextId: '5',
    boucleGraphe: {
      1: [],
      2: [],
      3: [],
      4: []
    },
    editgraphes: {
      positionNodes: [
        [
          70,
          70
        ],
        [
          120,
          120
        ],
        [
          170,
          170
        ],
        [
          220,
          220
        ],
        [
          420,
          420
        ]
      ],
      titreNodes: [
        null,
        null,
        null,
        null,
        null
      ]
    },
    graphe: [
      [],
      [
        '1',
        'squelettemtg32_Fact_Param',
        [
          {
            nn: '2',
            score: 'sans+condition'
          },
          {
            ex: 'Fact_MoinsaxMoinsb_Carre_Plus_k_axPlusb_cxPlusd',
            a: 'random',
            b: 'random',
            c: 'random',
            d: 'random',
            e: 'random',
            f: 'random',
            k: '1',
            nbEssais: 8,
            factMax: true,
            nbchances: 2

          }
        ]
      ],
      [
        '2',
        'squelettemtg32_Fact_Param',
        [
          {
            nn: '3',
            score: 'sans+condition'
          },
          {
            ex: 'Fact_aCarrexCarre_Moins_bCarre_Plus_k_axPlusb_cxPlusd',
            a: 'random',
            b: 'random',
            c: 'random',
            d: 'random',
            k: '1',
            nbEssais: 8,
            factMax: true,
            nbchances: 2

          }
        ]
      ],
      [
        '3',
        'squelettemtg32_Fact_Param',
        [
          {
            nn: '4',
            score: 'sans+condition'
          },
          {
            ex: 'Fact_aCarrexCarre_Moins_bCarre_Plus_k_axPlusb_cxPlusd',
            a: 'random',
            b: 'random',
            c: 'random',
            d: 'random',
            k: 'int(rand(0)*3+2)',
            nbEssais: 8,
            factMax: true,
            nbchances: 2

          }
        ]
      ],
      [
        '4',
        'squelettemtg32_Fact_Param',
        [
          {
            nn: '5',
            conclusion: 'C’est fini !',
            score: 'sans+condition'
          },
          {
            ex: 'Fact_aCarrexCarre_Moins_bCarre_Moins_k_axPlusb_cxPlusd',
            a: 'random',
            b: 'random',
            c: 'random',
            d: 'random',
            k: 'random',
            nbEssais: 8,
            factMax: true,
            nbchances: 2
          }
        ]
      ],
      [
        '5',
        'fin'
      ]
    ],
    bilans: [
      {
        index: 0,
        id: '1',
        pe: 0,
        duree: 198,
        score: 0,
        fin: false,
        nextId: '2',
        ns: 2,
        boucle: 1
      },
      {
        index: 1,
        id: '2',
        pe: 0,
        duree: 4,
        score: 0,
        fin: false,
        nextId: '3',
        ns: 3,
        boucle: 1
      },
      {
        index: 2,
        id: '3',
        pe: 0,
        duree: 2,
        score: 0,
        fin: false,
        nextId: '4',
        ns: 4,
        boucle: 1
      },
      {
        index: 3,
        id: '4',
        pe: 0,
        duree: 3,
        score: 0,
        fin: true,
        nextId: '5',
        ns: 'fin',
        boucle: 1
      }
    ]
  },
  date: '2021-03-17T08:15:30.220Z',
  duree: 209,
  fin: true,
  oid: '6051ba9ac5c153687f0d9829',
  participants: [
    '5f434ac4d10a7418e701e1cc'
  ],
  reponse: '',
  ressource: {
    rid: 'sesabibli/599bf117b4649c7d956f6168',
    type: 'j3p',
    titre: 'Factorisation par ax+b : Niveau 4'
  },
  resultatId: '1615968240446',
  rid: 'sesabibli/599bf117b4649c7d956f6168',
  score: 0,
  sequence: '600d68912bdf02250d674e78',
  type: 'j3p'
}
