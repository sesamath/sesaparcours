import { clickFirstDialogOk, clickSectionSuivante } from 'testBrowser/helpers/actions'
import { assertEquals, checkBilan } from 'testBrowser/helpers/checks'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { logIfVerbose } from 'testBrowser/helpers/log'
import { getNodeInfos } from 'testBrowser/helpers/j3p'

import { runAll as exCalcMultiEdit } from 'testBrowser/sections/outils/mtg32/ex_Calc_Multi_Edit'
import { runAll as angle02 } from 'testBrowser/sections/college/sixieme/Elementgeo/angle02'

/**
 * Teste un graphe avec la section ex_Calc_Multi_Edit suivie de angle02 (cf *_datas issu de sesabibli/60391cb5491aff7ab7c25071)
 * @param {Page} page
 * @return {Promise<boolean>}
 */
export async function test (page) {
  const datas = await import('./ex_Calc-angle02_datas.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, datas)
  // on lance le test de la 1re section (qui doit être déjà affichée)
  const { params, id } = getNodeInfos(datas.graphe, 'ex_Calc_Multi_Edit')
  const score1 = await exCalcMultiEdit(page, params, { id })
  // on vérifie le contenu de la boite de dialogue
  await clickSectionSuivante(page)
  const dialogContent = await page.innerText('.ui-dialog-content')
  const expected = score1 >= 0.6 ? 'C’est bien, on passe à la suite' : 'C’est pas encore ça, on passe à la suite quand même'
  assertEquals(dialogContent, expected)
  logIfVerbose(`Boite de dialogue ok avec ${expected}`)

  // pour continuer y’a pas le bouton suivant, faut cliquer sur le ok de la boite de dialogue
  await clickFirstDialogOk(page)

  // on passe à la 2e section
  const { params: params2, id: id2 } = getNodeInfos(datas.graphe, 'angle02')
  const score2 = await angle02(page, params2, { id: id2 })
  await checkBilan(page, [
    { id, score: score1, nbRepetitions: params.nbrepetitions },
    { id: id2, score: score2, nbRepetitions: params2.nbrepetitions }
  ])
  // pas de suivant, c’est fini avec un écran vide (reste juste le score en haut à droite)
  return true
}
