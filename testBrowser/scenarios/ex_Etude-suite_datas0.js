// graphe de la ressource https://bibliotheque.sesamath.net/api/public/60391cb5491aff7ab7c25071
// faut exporter un objet
export const graphe = [
  [
    '1',
    'natureSuite',
    [
      {
        nn: '2',
        pe: 'sans condition',
        conclusion: 'Maintenant déterminons le terme général de la suite'
      },
      {
        nbrepetitions: 1,
        typeSuite: 'aleatoire',
        nbchances: 1
      }

    ]
  ],
  [
    '2',
    'SuiteTermeGeneral',
    [
      {
        nn: '3',
        pe: 'sans condition',
        conclusion: 'Et maintenant calculons un terme particulier de la suite'
      },
      {
        nbrepetitions: 1,
        donneesPrecedentes: true,
        indication: '',
        limite: '',
        // typeSuite: 'geometrique',
        nbchances: 2,
        a: '[-1.1;2.1]',
        b: '[-0.5;1]',
        termeInitial: [
          '0|[0.4;5]',
          '0|[-4;-1]',
          '1|[1;6]'
        ],
        UnDirect: true,
        afficherPhrase: false,
        niveau: 'TS'
      }

    ]
  ],
  [
    '3',
    'calculTerme',
    [
      {
        nn: '1',
        score: 'sans+condition',
        conclusion: 'Nouvel exemple',
        sconclusion: 'Dernier exemple',
        snn: '4',
        maxParcours: '3'
      },
      {
        nbrepetitions: 1,
        donneesPrecedentes: true,
        indication: '',
        limite: '',
        nbchances: 2,
        a: '[-1.1;2.1]',
        b: '[-0.5;1]',
        termeInitial: [
          '0|[0.4;5]',
          '0|[-4;-1]',
          '1|[1;6]'
        ],
        niveau: 'TS'
      }

    ]
  ],
  [
    '4',
    'natureSuite',
    [
      {
        nn: '5',
        pe: 'sans condition',
        conclusion: 'Maintenant déterminons le terme général de la suite'
      },
      {
        nbrepetitions: 1,
        nbchances: 2
      }

    ]
  ],
  [
    '5',
    'SuiteTermeGeneral',
    [
      {
        nn: '6',
        pe: 'sans condition',
        conclusion: 'Et maintenant calculons un terme particulier de la suite'
      },
      {
        nbrepetitions: 1,
        donneesPrecedentes: true,
        nbchances: 2
      }

    ]
  ],
  [
    '6',
    'calculTerme',
    [
      {
        nn: '7',
        pe: 'sans condition',
        conclusion: 'Fin'
      },
      {
        nbrepetitions: 1,
        donneesPrecedentes: true,
        nbchances: 2
      }
    ]
  ],
  [
    '7',
    'fin',
    [
      {}

    ]
  ]
]
