import { checkBilan } from 'testBrowser/helpers/checks'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { getNodeInfos } from 'testBrowser/helpers/j3p'

import { runAll as natureSuite } from 'testBrowser/sections/lycee/suites/natureSuite'
import { runAll as suiteTermeGeneral } from 'testBrowser/sections/lycee/suites/suiteTermeGeneral'
import { runAll as calculTerme } from 'testBrowser/sections/lycee/suites/calculTerme'
import { clickFirstDialogOk } from 'testBrowser/helpers/actions'

/**
 * Teste un graphe avec la section natureSuite suivie de SuiteTermeGeneral puis de calculTerme deux fois de suite (cf *_datas issu de sesabibli/91950)
 * @param {Page} page
 * @return {Promise<boolean>}
 */
export async function test (page) {
  const datas = await import('./ex_Etude-suite_datas0.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, datas)
  const { graphe } = datas
  // on lance le test de la 1re section (qui doit être déjà affichée)
  let nbLoop = 1
  let idNumber = 1
  const score = []
  const { params: natureSuiteParams } = getNodeInfos(graphe, 'natureSuite', '1')
  const { params: suiteTermeGeneralParams } = getNodeInfos(graphe, 'SuiteTermeGeneral', '2')
  const { params: calculTermeParams, branchements } = getNodeInfos(graphe, 'calculTerme', '3')
  const maxParcours = Number(branchements[0].maxParcours)

  do {
    score[idNumber] = await natureSuite(page, natureSuiteParams, { numProgression: idNumber.toString() })
    await clickFirstDialogOk(page)
    score[idNumber + 1] = await suiteTermeGeneral(page, suiteTermeGeneralParams, { numProgression: (idNumber + 1).toString() })
    await clickFirstDialogOk(page)
    score[idNumber + 2] = await calculTerme(page, calculTermeParams, { numProgression: (idNumber + 2).toString() })
    await clickFirstDialogOk(page)
    idNumber += 3
    nbLoop++
  } while (nbLoop <= maxParcours)
  const { params: natureSuite4Params } = getNodeInfos(graphe, 'natureSuite', '4')
  score[idNumber] = await natureSuite(page, natureSuite4Params, { numProgression: idNumber.toString() })
  await clickFirstDialogOk(page)
  const { params: suiteTermeGeneral5Params } = getNodeInfos(graphe, 'SuiteTermeGeneral', '5')
  score[idNumber + 1] = await suiteTermeGeneral(page, suiteTermeGeneral5Params, { numProgression: (idNumber + 1).toString() })
  await clickFirstDialogOk(page)
  const { params: calculTerme6Params } = getNodeInfos(graphe, 'calculTerme', '6')
  score[idNumber + 2] = await calculTerme(page, calculTerme6Params, { numProgression: (idNumber + 2).toString() })
  const nodeScores = []
  for (let i = 1; i < score.length; i++) {
    nodeScores.push({ id: (i < maxParcours * 3 + 1 ? (i - 1) % 3 + 1 : i - (maxParcours - 1) * 3).toString(), score: score[i], nbRepetitions: 1 })
  }
  await checkBilan(page, nodeScores)
  // pas de suivant, c’est fini avec un écran vide (reste juste le score en haut à droite)
  return true
}
