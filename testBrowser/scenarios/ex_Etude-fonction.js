import { checkBilan } from 'testBrowser/helpers/checks'
import { loadGraphe } from 'testBrowser/helpers/browser'
import { getNodeInfos } from 'testBrowser/helpers/j3p'

import { runAllAvecFonction as etudeDerivee } from 'testBrowser/sections/lycee/fonctionsetude/etudeFonctionDerivee'
import { runAllAvecFonction as etudeSigneDerivee } from 'testBrowser/sections/lycee/fonctionsetude/etudeFonctionSigneDerivee'
// import { runAll as etudeFacteursDerivee } from 'testBrowser/sections/lycee/fonctionsetude/etudeFonctionFacteursDerivee'
import { runAll as etudeVariations } from 'testBrowser/sections/lycee/fonctionsetude/etudeFonctionVariations'
import { clickFirstDialogOk } from 'testBrowser/helpers/actions'

/**
 * Teste un graphe avec la section natureSuite suivie de SuiteTermeGeneral puis de calculTerme deux fois de suite (cf *_datas issu de sesabibli/91950)
 * @param {Page} page
 * @return {Promise<boolean>}
 */
export async function test (page) {
  const datas = await import('./ex_Etude-fonction_datas0.js')
  // on charge ce graphe dans la page courante
  const nodeIndexMax = 25 //
  await loadGraphe(page, datas)
  const { graphe } = datas
  // on lance le test de la 1re section (qui doit être déjà affichée)
  let idNumber = 1
  const ids = []
  const score = []
  let nodeIndex = 0
  do {
    let nbLoop = 1
    const { params: etudeDeriveeParams, id: id1 } = getNodeInfos(graphe, 'EtudeFonction_derivee', graphe[nodeIndex][0])
    ids[nodeIndex] = id1
    const { params: etudeSigneDeriveeParams, id: id2 } = getNodeInfos(graphe, 'EtudeFonction_signederivee', graphe[nodeIndex + 1][0])
    ids[nodeIndex + 1] = id2
    const { params: etudeVariationsParams, branchements, id: id3 } = getNodeInfos(graphe, 'EtudeFonction_variations', graphe[nodeIndex + 2][0])
    ids[nodeIndex + 2] = id3
    const maxParcours = Number(branchements[0].maxParcours)
    let fonction, fonction2
    do {
      [score[idNumber - 1], fonction] = await etudeDerivee(page, etudeDeriveeParams, { numProgression: idNumber.toString() })
      await clickFirstDialogOk(page)
      // score[idNumber + 1] = await etudeFacteursDerivee(page, etudeFacteursDeriveeParams, { numProgression: (idNumber + 1).toString() })
      // on a choisi de zapper l’étude du nombre de facteurs positifs car cela modifiait le tableau de signe de la dérivée et cela aurait été trop coûteux de gérer les différents tableaux.
      // idem pour l’étude des limites...
      if (fonction !== await (await page.locator('span#affiche_0_2').innerText())) {
        throw Error('La fonction étudiée dans cette section n’est pas la même que la fonction de la section EtudeFonctionDerivee !')
      }
      [score[idNumber], fonction2] = await etudeSigneDerivee(page, etudeSigneDeriveeParams, { numProgression: (idNumber + 1).toString() })
      if (fonction !== fonction2 || fonction !== await (await page.locator('span#affiche_0_2').innerText())) {
        throw Error('La fonction étudiée dans cette section n’est pas la même que la fonction de la section EtudeFonctionDerivee !')
      }
      await clickFirstDialogOk(page)
      score[idNumber + 1] = await etudeVariations(page, etudeVariationsParams, { numProgression: (idNumber + 2).toString() })
      if (nodeIndex < nodeIndexMax - 4) {
        await clickFirstDialogOk(page)
      }
      idNumber += 3
      nbLoop++
    } while (nbLoop <= maxParcours)
    nodeIndex += 3
  } while (nodeIndex < nodeIndexMax - 1)
  const nodeScores = []
  for (let i = 0; i < score.length; i++) {
    nodeScores.push({ id: ids[i], score: score[i], nbRepetitions: 1 })
  }
  await checkBilan(page, nodeScores)
  // pas de suivant, c’est fini avec un écran vide (reste juste le score en haut à droite)
  return true
}
