import { assertEquals, checkEtat } from 'testBrowser/helpers/checks'
import { getContentNormalized, getDefaultPage, loadGraphe } from 'testBrowser/helpers/browser'
import { logError, logIfVerbose } from 'testBrowser/helpers/log'

// ça sert à rien de récupérer ça, c’est la même section mais avec des datas différentes, le résultat n’a rien à voir
// import { runAll: ex_Calc_Multi_Edit } from 'testBrowser/sections/outils/mtg32/ex_Calc_Multi_Edit'

/*
async function runOne (page) {
  await page.waitForSelector('#enonce')
  // ATTENTION c’est du debug, faut surtout pas merger ça dans main (p’tet que ça fait rien en headless, mais c’est pas une raison)
  // await page.pause()

  const latexChunks = await getLatexChunks(page, '#enonce')
  if (!latexChunks[0]) throw Error('Énoncé sans LaTeX')
  const exprChunks = /=([^-]+)-(.*)$/.exec(latexChunks[0])
  if (!exprChunks || exprChunks.length < 3) throw Error('Pas la string LaTeX attendue : ' + latexChunks[0])
  const a2 = exprChunks[1]
  const b2 = exprChunks[2]
  // @todo réécrire ce test qui marche pas toujours (on peut avoir du "81-(6x-5)^2" = 4(7-3x)(2+3x)
  const aChunks = /^([0-9]*)x\^2$/.exec(a2)
  if (!aChunks || aChunks.length < 2) throw Error(`1er terme pas celui attendu ${a2}`)
  let a = aChunks[1]
  if (a !== '') a = Math.sqrt(Number(a))
  const b = Math.sqrt(b2)
  // ATTENTION c’est du debug, faut surtout pas merger ça dans main (p’tet que ça fait rien en headless, mais c’est pas une raison)
  // await page.pause()
  // console.log('on a récupéré', latexChunks[0], `qui donne la différence de ${a2} et ${b2} soit les termes a=${a} et b=${b}`)
  // @todo gérer le cas ou ax+b ou ax-b est factorisable (a et b non premiers entre eux)
  await page.type('#expressioninputmq1', `(${a}x-${b})(${a}x+${b})`)
  await clickOk(page)
  const pg = Number.isInteger(b) ? pgcd(a || 1, b) : 1
  if (pg === 1) {
    await checkFeedbackCorrection(page, true)
  } else {
    await checkFeedbackCorrection(page, /La factorisation est bonne mais pas maximale./)
    let c = fixArrondi(a / pg)
    if (c === 1) c = ''
    const d = fixArrondi(b / pg)
    const coefGlobal = fixArrondi(pg * pg)
    await page.type('#expressioninputmq1', `${coefGlobal}(${c}x-${d})(${c}x+${d})`)
    await clickOk(page)
    await checkFeedbackCorrection(page, true)
  }
}
 */

async function reset (page, datas, { total, numProgression } = {}) {
  await page.close()
  page = await getDefaultPage({
    reject: (error) => {
      logError(`Plantage dans ${__filename}`)
      throw error
    }
  })
  await loadGraphe(page, datas)
  logIfVerbose('reset de la page avec un nouveau graphe|lastResultat ok')
  if (total) {
    // on doit être directement à la section numProgression
    await checkEtat(page, { numQuestion: 1, score: 0, total, numProgression })
    logIfVerbose(`Démarrage en section ${numProgression} ok`)
  }
  return page
}

/**
 * Teste la section ex_Calc_Multi_Edit avec les données de la ressource 60391cb5491aff7ab7c25071
 * @param {Page} page
 * @return {Promise<boolean>}
 */
export async function test (page) {
  let datas = await import('./ex_Calc_Multi_Edit-reprise_datas0.js')
  // on charge ce graphe dans la page courante
  await loadGraphe(page, datas)
  logIfVerbose('Chargement ok avec ex_Calc_Multi_Edit-reprise_datas')
  // on doit avoir un message comme quoi le parcours a déjà été terminé
  const dialogContent = await getContentNormalized(page, '#modalecontenu')
  assertEquals(dialogContent, 'Ce parcours a déjà été terminé. Si tu fermes cette boite de dialogue, il reprendra du début (sinon choisis un autre exercice).')
  logIfVerbose('Message parcours déjà fait ok')
  // si on cliques sur OK ça recommence
  await page.click('#modale div.croix')
  await page.waitForSelector('#enonce')
  const enonce = await getContentNormalized(page, '#enonce', true)
  assertEquals(enonce, /^On considère la fonction.*dont la courbe représentative est tracée ci-dessous\.$/)
  logIfVerbose('Après clic pour fermer la boite de dialogue, rechargement et énoncé ok')

  // on charge un autre lastResultat avec un autre graphe pour vérifier qu’on reprend bien en node 2
  datas = await import('./ex_Calc_Multi_Edit-reprise_datas1.js')
  datas = { ...datas } // sinon on peut pas lui ajouter de propriété plus bas
  await reset(page, datas, { total: 1, numProgression: 4 })
  return true
  /* @todo finir la suite
  await runOne(page)
  await checkResultats(page, { nbResultats: 0 })
  await clickSectionSuivante(page)
  // ici faut attendre un peu que la section envoie le résultat et recrée l’énoncé, sinon on se retrouve avec l’ancien
  await waitMs(100)
  await page.waitForSelector('#enonce')
  await checkResultats(page, { nbResultats: 1 })

  // section 2
  await runOne(page)
  await checkResultats(page, { nbResultats: 1 })
  await clickSectionSuivante(page)
  await waitMs(100)
  await page.waitForSelector('#enonce')
  await checkResultats(page, { nbResultats: 2 })
  // on arrête là pour le moment, ensuite l’analyse d’énoncé se corse, faudrait avoir mtg pour le calcul symbolique
  // pour lui demander de récupérer les termes de (-x-6)^2-4 par ex, puis simplifier (-x-6+2)

  // reset de la page
  const resultats = getResultats(page)
  datas.lastResultat = resultats[0]
  // page = await reset(page, datas, { total: 1, numProgression: 2 })
  page = await reset(page, datas)

  datas.lastResultat = resultats[1]
  page = await reset(page, datas, { total: 1, numProgression: 3 })

  // pas de suivant, c’est fini avec un écran vide (reste juste le score en haut à droite)
  return true
  /* */
}
