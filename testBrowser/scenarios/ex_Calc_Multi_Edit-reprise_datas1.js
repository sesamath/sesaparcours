export const graphe = [
  [],
  [
    '1',
    'squelettemtg32_Fact_Param',
    [
      {
        nn: '2',
        score: 'sans+condition'
      },
      {
        ex: 'Fact_ax_Carre_Moins_bCarre',
        a: '1',
        b: 'random',
        nbEssais: 2,
        factMax: true,
        nbchances: 1,
        nbrepetitions: 1,
        limite: '',
        c: 'random',
        d: 'random',
        e: 'random',
        f: 'random',
        g: 'random',
        h: 'random',
        j: 'random',
        k: 'random',
        l: 'random',
        m: 'random',
        n: 'random',
        p: 'random',
        q: 'random',
        r: 'random'
      }
    ]
  ],
  [
    '2',
    'squelettemtg32_Fact_Param',
    [
      {
        nn: '3',
        score: 'sans+condition',
        conclusion: ''
      },
      {
        ex: 'Fact_ax_Carre_Moins_bCarre',
        a: 'int(rand(0)*3+2)',
        b: 'random',
        nbEssais: 5,
        factMax: true,
        nbchances: 2
      }
    ]
  ],
  [
    '3',
    'squelettemtg32_Fact_Param',
    [
      {
        nn: '4',
        score: 'sans+condition',
        conclusion: ''
      },
      {
        ex: 'Fact_axPlusb_Carre_Moins_cCarre',
        a: 'random',
        b: 'random',
        c: 'random',
        nbEssais: 8,
        factMax: true,
        nbchances: 2,
        nbrepetitions: 1,
        limite: '',
        d: 'random',
        e: 'random',
        f: 'random',
        g: 'random',
        h: 'random',
        j: 'random',
        k: 'random',
        l: 'random',
        m: 'random',
        n: 'random',
        p: 'random',
        q: 'random',
        r: 'random'
      }
    ]
  ],
  [
    '4',
    'squelettemtg32_Fact_Param',
    [
      {
        nn: '5',
        score: 'sans+condition',
        conclusion: 'C’est fini'
      },
      {
        ex: 'Fact_cCarre_Moins_axPlusb_Carre',
        a: 'random',
        b: 'random',
        c: 'random',
        nbEssais: 8,
        factMax: true,
        nbchances: 2,
        nbrepetitions: 1,
        limite: '',
        d: 'random',
        e: 'random',
        f: 'random',
        g: 'random',
        h: 'random',
        j: 'random',
        k: 'random',
        l: 'random',
        m: 'random',
        n: 'random',
        p: 'random',
        q: 'random',
        r: 'random'
      }
    ]
  ],
  [
    '5',
    'fin'
  ]
]

export const lastResultat = {
  contenu: {
    noeuds: [
      '1',
      '2',
      '3'
    ],
    pe: [
      0,
      1,
      0,
      0
    ],
    scores: [
      0,
      1,
      0,
      0
    ],
    ns: [
      2,
      3,
      4
    ],
    boucle: [
      1,
      1,
      1,
      1
    ],
    nextId: '4',
    boucleGraphe: {
      1: [],
      2: [],
      3: [],
      4: []
    },
    editgraphes: {
      positionNodes: [
        [
          22,
          32
        ],
        [
          218,
          31
        ],
        [
          472,
          38
        ],
        [
          448,
          137
        ],
        [
          225,
          132
        ]
      ],
      titreNodes: [
        'x^2-a^2',
        '(ax)^2-b^2',
        '(ax+b)^2-c^2',
        'a^2-(bx+c)^2',
        null
      ]
    },
    graphe,
    bilans: [
      {
        index: 0,
        id: '1',
        pe: 0,
        duree: 143,
        score: 0,
        fin: false,
        nextId: '2',
        ns: 2,
        boucle: 1
      },
      {
        index: 1,
        id: '2',
        pe: 1,
        duree: 39,
        score: 1,
        fin: false,
        nextId: '3',
        ns: 3,
        boucle: 1
      },
      {
        index: 2,
        id: '3',
        pe: 0,
        duree: 246,
        score: 0,
        fin: false,
        nextId: '4',
        ns: 4,
        boucle: 1
      }
    ]
  }
}
