// graphe de la ressource https://bibliotheque.sesamath.net/api/public/60391cb5491aff7ab7c25071
// faut exporter un objet
export const graphe = [
  [
    '1',
    'EtudeFonction_derivee',
    [
      {
        nn: '2',
        pe: 'sans condition',
        conclusion: 'Etudions alors le signe de la dérivée en s’intéressant tout d’abord aux facteurs positifs'
      },
      {
        modele: [1],
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '2',
    'EtudeFonction_signederivee',
    [
      {
        nn: '3',
        pe: 'sans condition',
        conclusion: 'Passons aux variations de la fonction'
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        nbrepetitions: 1,
        nbchances: 1,
        imposer_domaine: []
      }
    ]
  ],
  [
    '3',
    'EtudeFonction_variations',
    [
      {
        nn: '1',
        pe: 'sans condition',
        conclusion: 'Fin de l’exercice',
        sconclusion: 'Fin',
        snn: '4',
        maxParcours: 1
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        limites_a_trouver: true,
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '4',
    'EtudeFonction_derivee',
    [
      {
        nn: '5',
        pe: 'sans condition',
        conclusion: 'Etudions alors le signe de la dérivée en s’intéressant tout d’abord aux facteurs positifs'
      },
      {
        modele: [2],
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '5',
    'EtudeFonction_signederivee',
    [
      {
        nn: '6',
        pe: 'sans condition',
        conclusion: 'Passons aux variations de la fonction'
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        nbrepetitions: 1,
        nbchances: 1,
        imposer_domaine: []
      }
    ]
  ],
  [
    '6',
    'EtudeFonction_variations',
    [
      {
        nn: '4',
        pe: 'sans condition',
        conclusion: 'Fin de l’exercice',
        sconclusion: 'Fin',
        snn: '7',
        maxParcours: 1
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        limites_a_trouver: true,
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '7',
    'EtudeFonction_derivee',
    [
      {
        nn: '8',
        pe: 'sans condition',
        conclusion: 'Etudions alors le signe de la dérivée en s’intéressant tout d’abord aux facteurs positifs'
      },
      {
        modele: [3],
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '8',
    'EtudeFonction_signederivee',
    [
      {
        nn: '9',
        pe: 'sans condition',
        conclusion: 'Passons aux variations de la fonction'
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        nbrepetitions: 1,
        nbchances: 1,
        imposer_domaine: []
      }
    ]
  ],
  [
    '9',
    'EtudeFonction_variations',
    [
      {
        nn: '7',
        pe: 'sans condition',
        conclusion: 'Fin de l’exercice',
        sconclusion: 'Fin',
        snn: '10',
        maxParcours: 1
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        limites_a_trouver: true,
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '10',
    'EtudeFonction_derivee',
    [
      {
        nn: '11',
        pe: 'sans condition',
        conclusion: 'Etudions alors le signe de la dérivée en s’intéressant tout d’abord aux facteurs positifs'
      },
      {
        modele: [4],
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '11',
    'EtudeFonction_signederivee',
    [
      {
        nn: '12',
        pe: 'sans condition',
        conclusion: 'Passons aux variations de la fonction'
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        nbrepetitions: 1,
        nbchances: 1,
        imposer_domaine: []
      }
    ]
  ],
  [
    '12',
    'EtudeFonction_variations',
    [
      {
        nn: '10',
        pe: 'sans condition',
        conclusion: 'Fin de l’exercice',
        sconclusion: 'Fin',
        snn: '13',
        maxParcours: 1
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        limites_a_trouver: true,
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '13',
    'EtudeFonction_derivee',
    [
      {
        nn: '14',
        pe: 'sans condition',
        conclusion: 'Etudions alors le signe de la dérivée en s’intéressant tout d’abord aux facteurs positifs'
      },
      {
        modele: [5],
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '14',
    'EtudeFonction_signederivee',
    [
      {
        nn: '15',
        pe: 'sans condition',
        conclusion: 'Passons aux variations de la fonction'
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        nbrepetitions: 1,
        nbchances: 1,
        imposer_domaine: []
      }
    ]
  ],
  [
    '15',
    'EtudeFonction_variations',
    [
      {
        nn: '13',
        pe: 'sans condition',
        conclusion: 'Fin de l’exercice',
        sconclusion: 'Fin',
        snn: '16',
        maxParcours: 1
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        limites_a_trouver: true,
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '16',
    'EtudeFonction_derivee',
    [
      {
        nn: '17',
        pe: 'sans condition',
        conclusion: 'Etudions alors le signe de la dérivée en s’intéressant tout d’abord aux facteurs positifs'
      },
      {
        modele: [6],
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '17',
    'EtudeFonction_signederivee',
    [
      {
        nn: '18',
        pe: 'sans condition',
        conclusion: 'Passons aux variations de la fonction'
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        nbrepetitions: 1,
        nbchances: 1,
        imposer_domaine: []
      }
    ]
  ],
  [
    '18',
    'EtudeFonction_variations',
    [
      {
        nn: '16',
        pe: 'sans condition',
        conclusion: 'Fin de l’exercice',
        sconclusion: 'Fin',
        snn: '19',
        maxParcours: 1
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        limites_a_trouver: true,
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '19',
    'EtudeFonction_derivee',
    [
      {
        nn: '20',
        pe: 'sans condition',
        conclusion: 'Etudions alors le signe de la dérivée en s’intéressant tout d’abord aux facteurs positifs'
      },
      {
        modele: [7],
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '20',
    'EtudeFonction_signederivee',
    [
      {
        nn: '21',
        pe: 'sans condition',
        conclusion: 'Passons aux variations de la fonction'
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        nbrepetitions: 1,
        nbchances: 1,
        imposer_domaine: []
      }
    ]
  ],
  [
    '21',
    'EtudeFonction_variations',
    [
      {
        nn: '19',
        pe: 'sans condition',
        conclusion: 'Fin de l’exercice',
        sconclusion: 'Fin',
        snn: '22',
        maxParcours: 1
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        limites_a_trouver: true,
        nbrepetitions: 1,
        nbchances: 1,
        boitedialogue: true
      }
    ]
  ],
  [
    '22',
    'EtudeFonction_derivee',
    [
      {
        nn: '23',
        pe: 'sans condition',
        conclusion: 'Etudions alors le signe de la dérivée en s’intéressant tout d’abord aux facteurs positifs'
      },
      {
        modele: [8],
        nbrepetitions: 1,
        nbchances: 1
      }
    ]
  ],
  [
    '23',
    'EtudeFonction_signederivee',
    [
      {
        nn: '24',
        pe: 'sans condition',
        conclusion: 'Passons aux variations de la fonction'
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        nbrepetitions: 1,
        nbchances: 1,
        imposer_domaine: []
      }
    ]
  ],
  [
    '24',
    'EtudeFonction_variations',
    [
      {
        nn: '22',
        pe: 'sans condition',
        conclusion: 'Fin de l’exercice',
        sconclusion: 'Fin',
        snn: '25',
        maxParcours: 1
      },
      {
        donneesPrecedentes: true,
        imposer_fct: 'j3p.parcours.donnees[1]',
        limites_a_trouver: true,
        nbrepetitions: 1,
        nbchances: 1,
        boitedialogue: true
      }
    ]
  ],
  ['25', 'fin', [null]]
]
