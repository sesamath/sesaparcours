import textesGeneriques from 'src/lib/core/textes'

import { clickFirstDialogOk } from 'testBrowser/helpers/actions'
import { getContentNormalized, getResultats } from 'testBrowser/helpers/browser'
import { logIfVerbose } from 'testBrowser/helpers/log'
import { normalize } from 'testBrowser/helpers/text'

const { cBien, cFaux } = textesGeneriques

/**
 * Vérifie que result est égal à expected (aux espaces près, sauf si strict), la validation passe en cas d’égalité js stricte, indépendamment du type
 * @param {number|string} result
 * @param {number|string|RegExp} expected
 * @param {boolean} strict
 */
export function assertEquals (result, expected, strict) {
  // si c’est strictement égal on fait rien, indépendamment du type, inutile de se casser la tête
  if (result === expected) return
  // on compare des string, sauf si on nous donne deux numbers
  if (typeof result === 'number' && typeof expected === 'number') {
    if (result !== expected) throw Error(`Nombre erroné, on attendait ${expected} et on a eu ${result}`)
    return
  }
  if (typeof result === 'number') result = String(result)
  if (typeof expected === 'number') expected = String(expected)
  if (typeof result !== 'string') throw TypeError(`result n’est ni un number ni une string ${typeof result}`)
  if (!strict) result = normalize(result)
  if (expected instanceof RegExp) {
    if (expected.test(result)) return
    throw Error(`Le résultat "${result}" ne match pas ${expected}`)
  }
  if (typeof expected !== 'string') throw TypeError(`expected n’est ni un number ni une regex ni une string ${typeof expected}`)
  if (!strict) expected = normalize(expected)
  if (result !== expected) throw Error(`On attendait "${expected}" et on a eu "${result}"`)
}

/**
 * Vérifie que le bilan est affiché dans sa boite de dialogue puis clique sur ok
 * @param page
 * @param {NodeScore[]} nodeScores
 * @return {Promise<void>}
 */
export async function checkBilan (page, nodeScores) {
  const nbScores = nodeScores.length
  // on check le score qui doit être dans une boite de dialogue, faut le passer en %
  const bilan = await getContentNormalized(page, 'div.ui-dialog-content:visible')
  let scoreCumul = 0
  for (const nodeScore of nodeScores) {
    const { id, score, nbRepetitions } = nodeScore
    const scorePc = Math.round(score * 100 / nbRepetitions) // cf avant 2023-03-21 pour arrondi au 10e
    scoreCumul += Number((score / nbRepetitions).toFixed(3))
    const expected = new RegExp(`Nœud : *${id}.*Temps passé[^0-9]+([0-9]+ minutes? )?[0-9]+ secondes?.*Score( global)? : ${scorePc}%`)
    assertEquals(bilan, expected)
  }
  if (nbScores > 1) {
    // vérif du score global (ATTENTION à faire la même opération que Parcours.js score/nb*100 et pas score*100/nb car ça peut changer l’arrondi
    await logIfVerbose(`Score global calculé par checkBilan : ${Math.round(scoreCumul / nbScores * 100)}%`)
    await logIfVerbose(`Bilan fourni par la modale de showBilan : ${bilan.match(/.*Score( global)? : (\d*%)/)[2]}`)
    assertEquals(bilan, new RegExp(`Score global : ${Math.round(scoreCumul / nbScores * 100)}%`))
  }
  await logIfVerbose(`Bilan ok : ${bilan}`)
  await clickFirstDialogOk(page)
}

/**
 * Enchaîne checkQuestion et checkScore
 * @param {Page} page
 * @param {object} options
 * @param {number|string} options.numQuestion
 * @param {number|string} options.score
 * @param {number|string} options.total
 * @param {number|string} [options.totalScore]
 * @param {number|string} [options.numProgression] Passer le numéro de la section dans le graphe si elle n’est pas la seule (pour le check de `question x sur y (section n)`)
 * @return {Promise<void>}
 */
export async function checkEtat (page, { numQuestion, score, total, totalScore, numProgression }) {
  if (totalScore == null) totalScore = total
  await checkQuestion(page, numQuestion, total, numProgression)
  await checkScore(page, score, totalScore)
}

/**
 * Vérifie que le message affiché dans correction est le bon
 * @param {Page} page
 * @param {RegExp|string|boolean|function} [expected=true] passer un booléen pour tester le classique "C’est bien|faux", ou une string ou une Regexp ou une fonction
 * @param {string} [selector=#correction]
 * @return {Promise<void>}
 */
export async function checkFeedbackCorrection (page, expected = true, selector = '#correction') {
  const feedback = await page.innerText(selector)
  if (typeof expected === 'boolean') {
    expected = new RegExp(expected ? cBien : cFaux)
  } else if (typeof expected === 'number') {
    expected = String(expected)
  }

  if (typeof expected === 'string') {
    if (feedback !== expected) throw Error(`Le message de correction n’est pas celui attendu, on a "${feedback}" et on attendait "${expected}"`)
  } else if (expected instanceof RegExp) {
    if (!expected.test(feedback)) throw Error(`Le message de correction n’est pas celui attendu, on a "${feedback}" qui ne match pas "${expected}"`)
  } else if (typeof expected === 'function') {
    if (!expected(feedback)) throw Error(`Le message de correction n’est pas celui attendu, "${feedback}" ne passe pas la validation`)
  } else {
    throw TypeError(`string ou RegExp attendu, on a eu ${typeof expected} ${expected}`)
  }
  logIfVerbose(`Correction OK avec "${normalize(feedback)}"`) // ici on met du normalize pour avoir une seule ligne de log
}

/**
 * Vérifie que l’on a bien le bon numéro de question affiché en haut à droite
 * @param {Page} page
 * @param {number|string} numQuestion
 * @param {number|string} total
 * @param {number|string} numProgression
 * @return {Promise<void>}
 */
export async function checkQuestion (page, numQuestion, total, numProgression) {
  // on vérifie qu’on est dans la bonne section si il y en a une
// je ne sais pas pourquoi le test textSection !== expected foire il reste sur la question+section précédente précédente (sans doute, l’étatQuestion n’est-il pas actualisé à temps ???
  if (numProgression) {
    await page.locator('#etatQuestion>span.section', { hasText: `${numProgression}` }).waitFor({ timeout: 10_010 })
  }
  // on vérifie qu’on en est à la bonne question
  const text = await page.innerText('#etatQuestion')
  const expected = `Question : ${numQuestion} sur ${total}` + (numProgression ? ` (section ${numProgression})` : '') // eslint-disable-line no-irregular-whitespace
  if (text !== expected) throw Error(`Numéro de question HS, on voulait "${expected}" et on a "${text}"`)
  logIfVerbose(`Question OK avec "${text}"`)
}

/**
 * Vérifie que les résultats retournés précédemment correspondent
 * @param {Page} page
 * @param {Object} options
 * @param {number} options.nbResultats
 * @return {Promise<void>}
 */
export async function checkResultats (page, { nbResultats }) {
  const resultats = await getResultats(page)
  if (resultats.length !== nbResultats) throw Error(`On voulait ${nbResultats} résultat${nbResultats > 1 ? 's' : ''} et on en a ${resultats.length}`)
  logIfVerbose(`Nb de résultats ok (${nbResultats})`)
}

/**
 * Vérifie que l’on a bien le bon score affiché en haut à droite
 * @param {Page} page
 * @param {number|string} score
 * @param {number|string} total
 * @return {Promise<void>}
 */
export async function checkScore (page, score, total) {
  const text = await page.innerText('#etatScore')
  const expected = `Score : ${score} sur ${total}`
  if (text !== expected) throw Error(`Score HS, on voulait "${expected}" et on a "${text}"`)
  logIfVerbose(`Score OK avec "${expected}"`)
}

/**
 * @typedef NodeScore
 * @type {Object}
 * @property {string} id
 * @property {number} score entier entre 0 et nbRepetitions
 * @property {number} nbRepetitions
 */
