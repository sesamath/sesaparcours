/**
 * Pilotage d’un input Mathquill dans le dom
 * Attention : En cas de double édition, penser à faire un clear() pour supprimer le contenu avant de saisir à nouveau des choses.
 * @class
 */
class ZsmHandler {
  /**
   *
   * @param {Page} page
   * @param {string} selector
   * @param {number} index
   * @param {Locator} has
   * @param {string} hasText
   */
  constructor (page, { selector = '', index = 0, has = null, hasText = '' } = {}) {
    this.page = page
    this.zsmMqFocusedCursor = null
    this.clavier = null
    this.zsmPictoClavier = null
    this.index = index
    page.setDefaultTimeout(3000)
    this.locator = null
    // on cherche notre input dans le dom
    if (selector) selector += ' '
    selector += '.zoneS3BAse'
    if (has || hasText) {
      /**
       * Le conteneur de l’input
       * @type {Locator}
       */
      this.locator = this.page.locator(selector, { has, hasText }).first()
    } else {
      this.locator = this.page.locator(selector).nth(index)
    }
    this.focused = false
    this.clavierActif = false
  }

  /**
   * Met le focus sur la zone Mathquill
   * @returns {Promise<void>}
   */
  async focus () {
    if (this.focused) return
    await this.locator.click()
    this.zsmMqFocusedCursor = this.locator.locator('td.zsmMqFocused:visible>div')
    this.focused = true
  }

  /**
   * Retire le focus de l’input
   * @returns {Promise<void>}
   */
  async blur () {
    if (!this.focused) return
    // Si le clavier est actif, alors on le désactive (ça retire aussi le focus)
    if (this.clavierActif) await this.desactiveClavier()
    // sinon, on blur
    else await this.locator.blur()
    this.focused = false
  }

  /**
   * s’assure que la zone Mathquill a le focus puis clique sur le picto Clavier pour l’ouvrir
   * @returns {Promise<void>}
   */
  async activeClavier () {
    if (!this.focused) {
      await this.focus()
    }
    if (!this.clavierActif) {
      this.zsmPictoClavier = this.locator.locator('.zsmPictoClavier').first()
      await this.zsmPictoClavier.click()
      this.clavierActif = true
    }
    this.clavier = this.page.locator('.zsmClavier:visible')
  }

  /**
   * s’assure que le clavier est actif et si oui, clique sur le clavier barré
   * le clavier disparaît et le focus est retiré de la zone
   * @returns {Promise<void>}
   */
  async desactiveClavier () {
    if (!this.clavierActif) return
    const pictoClavierBarre = this.locator.locator('.zsmPictoClavierBarre:visible')
    await pictoClavierBarre.click()
    this.clavierActif = false
    // cliquer sur le clavier barré, enlève le focus aussi !
    this.focused = false
  }

  /**
   * Tape text dans l’input (clavier physique)
   * @param {string} text
   * @returns {Promise<void>}
   */
  async type (text) {
    if (!this.focused) await this.focus()
    await this.zsmMqFocusedCursor.type(text)
    await this.blur()
  }

  /**
   * active le clavier si besoin, fait la frappe demandée et fait disparaître le clavier (retire le focus ne la zone)
   * @param {string[]} textArray il faut passer des strings pour chaque touche car il peut y avoir des Del ou Sup
   * @returns {Promise<void>}
   */
  async typeClavier (textArray) {
    if (!this.clavierActif) {
      await this.activeClavier()
    }
    for (const key of textArray) {
      await this.clavier.locator(`.zsmBtn:has-text("${key}")`).click()
    }
    await this.desactiveClavier()
  }

  /**
   * Nettoie la zone de saisie grâce au clavier virtuel retire le focus de la zone à l’issue du nettoyage
   * @returns {Promise<void>}
   */
  async clear () {
    if (!this.clavierActif) await this.activeClavier()
    let locators
    do {
      await this.clavier.press('Backspace')
      await this.clavier.press('Delete')
      locators = await this.page.locator('.zsmMqFocused').locator('.mq-selectable').all()
    } while (locators.length > 0)
    await this.desactiveClavier()
  }
}

export default ZsmHandler
