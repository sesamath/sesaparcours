import { fixArrondi } from 'src/lib/utils/number'

export async function areColineaires (v1, v2) {
  if (v1[0] === 0 && v2[0] === 0) return true
  // ok si v2 = k . v1
  return Math.abs(v2[0] / v1[0] - v2[1] / v1[1]) < 1e-9
}
export async function areColineairesOpposes (v1, v2) {
  return (180 - getAngleDegFromVect(v1, v2)) < 1e-9
}
export async function areColineairesMemeDirection (v1, v2) {
  return getAngleDegFromVect(v1, v2) < 1e-9
}

/**
 * Retourne l’angle en ° entre v1 et v2
 * @param {Vecteur} v1
 * @param {Vecteur} v2
 * @return {number} entre 0 et 180 (NaN si l’un des deux est le vecteur nul)
 */
export function getAngleDegFromVect (v1, v2) {
  const prodNormes = norme(v1) * norme(v2)
  if (prodNormes === 0) return Number.NaN
  const cosinus = fixArrondi(produitScalaire(v1, v2) / prodNormes)
  return fixArrondi(Math.acos(cosinus) * 180 / Math.PI)
}

/**
 * @param {Vecteur} v
 * @return {number}
 */
export const norme = (v) => fixArrondi(Math.sqrt(v.reduce((acc, coord) => acc + coord * coord, 0)))
/**
 * @param {Vecteur} v1
 * @param {Vecteur} v2
 * @return {number}
 */
export const produitScalaire = (v1, v2) => fixArrondi(v1.reduce((acc, coord, index) => acc + coord * v2[index], 0))

/**
 * @typedef Vecteur
 * @type {number[]}
 */
