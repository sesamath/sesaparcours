/**
 * Pilotage d’un input Mathquill dans le dom
 * Attention : En cas de double édition, penser à faire un clear() pour supprimer le contenu avant de saisir à nouveau des choses.
 * @class
 */
export const exp = String.fromCharCode(0x00002147)
export const ln = String.fromCharCode(0x000033D1)

export class InputMqHandler {
  /**
   *
   * @param {Page} page
   * @param {string} selector
   * @param {number} index
   * @param {Locator} has
   * @param {string} hasText
   */
  constructor (page, { selector = '', index = 0, has = null, hasText = '' } = {}) {
    this.page = page
    this.clavier = null
    this.triggerButton = null
    this.index = index
    page.setDefaultTimeout(3000)
    this.locator = null
    // on cherche notre input dans le dom
    if (selector) selector += ' '
    selector += 'div.kbdContainer'
    if (has || hasText) {
      /**
       * Le conteneur de l’input
       * @type {Locator}
       */
      this.locator = this.page.locator(selector, { has, hasText }).first()
    } else {
      this.locator = this.page.locator(selector).nth(index)
    }
    this.focused = false
    this.triggerButton = this.locator.locator('.triggerButton').first()
  }

  /**
   * Affiche/Masque le clavier virtuel
   * @param {boolean} show Passer true pour afficher et false pour masquer
   * @returns {Promise<void>}
   */
  async triggerClavier (show) {
    const clavier = await this.locator.locator('.virtualKeyboard')
    const isOpen = await clavier.isVisible()
    if (isOpen !== show) {
      await this.triggerButton.click()
      this.focused = show // activer le clavier donne le focus au textarea, le masquer l’enlève
    }
  }

  /**
   * Met le focus sur la zone Mathquill
   * @returns {Promise<void>}
   */
  async focus () {
    if (this.focused) return
    await this.locator.locator('span.mq-editable-field').click()
    this.focused = true
    // le focus active le clavier s’il était ouvert sur un autre input
  }

  /**
   * Retire le focus de l’input (masque le clavier s’il était affiché)
   * @returns {Promise<void>}
   */
  async blur () {
    if (!this.focused) return
    // Si le clavier est affiché, alors on le masque (ça retire aussi le focus du textarea)
    if (this.clavier) await this.desactiveClavier()
    // sinon on blur seulement
    else await this.locator.blur()
    this.focused = false
  }

  /**
   * Clique sur le picto Clavier pour l’ouvrir
   * @returns {Promise<void>}
   */
  async activeClavier () {
    await this.triggerClavier(true)
    this.clavier = this.page.locator('div.virtualKeyboard:visible').first()
  }

  /**
   * s’assure que le clavier est actif et si oui, clique sur le clavier barré
   * le clavier disparaît et le focus est retiré de la zone
   * @returns {Promise<void>}
   */
  async desactiveClavier () {
    await this.triggerClavier(false)
    this.clavier = null
  }

  async type (text) {
    if (!this.focused) await this.focus()

    if (text.substring(0, 1) === 'V') {
      await this.typeClavier(['V'], false)
      text = text.slice(1)
    }
    await this.locator.type(text)
    await this.blur()
  }

  /**
   * active le clavier si besoin, fait la frappe demandée et fait disparaître le clavier (retire le focus de la zone)
   * @param {string[]} texts il faut passer des strings pour chaque touche car il peut y avoir des Del ou Sup
   * @param {boolean} [desactivation=true] Si true, on désactive le clavier après la frappe, sinon on le laisse actif pour que le focus reste dans la zone (utilisé pour taper des racines carrées avec la méthode type()
   * @returns {Promise<void>}
   */
  async typeClavier (texts, desactivation = true) {
    await this.activeClavier()
    let bouton
    for (const key of texts) {
      if (key === 'V') {
        bouton = this.clavier.locator('.mqBtnracine')
      } else if (key === '∞') {
        bouton = this.clavier.locator('.mqBtninf')
      } else if (key === '^') {
        bouton = this.clavier.locator('.mqBtnpuissance')
      } else if (key === exp) {
        bouton = this.clavier.locator('.mqBtnexp')
      } else if (key === ln) {
        bouton = this.clavier.locator('.mqButton.mqFunction', { hasText: 'ln' })
      } else {
        bouton = this.clavier.locator(`div >  button:has-text("${key.replace('/', '÷').replace('−', '-')}")`)
      }
      await bouton.click()
    }
    if (desactivation) await this.desactiveClavier()
  }

  /**
   * Nettoie la zone de saisie grâce au clavier virtuel retire le focus de la zone à l’issue du nettoyage
   * @returns {Promise<void>}
   */
  async clear () {
    await this.activeClavier()
    const rootBlock = this.locator.locator('.mq-root-block')
    let spans = []
    do {
      await this.typeClavier(['→', '↤', '↤'])
      spans = await rootBlock.locator('span').allInnerTexts()
    } while (spans.length > 0)
    await this.desactiveClavier()
  }
}
