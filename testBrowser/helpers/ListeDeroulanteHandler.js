/**
 * Pilotage d’une liste déroulante dans le dom
 * @class
 */
class ListeDeroulanteHandler {
  /**
   * @param page
   * @param {Object} [options]
   * @param {string} [options.selector=''] Un éventuel sélecteur dans lequel chercher notre .listeDeroulante
   * @param {number} [options.index=0] L’index de la liste déroulante dans le retour de querySelectorAll('.listeDeroulante')
   * @param {Locator} [options.has] Un objet [Locator]{@link https://playwright.dev/docs/api/class-locator} que la liste devra contenir (ignoré si index fourni)
   * @param {string} [options.hasText=''] Un texte que la liste devra contenir (ignoré si index fourni)
   * @constructor
   */
  constructor (page, { selector = '', index = 0, has = null, hasText = '' } = {}) {
    /** @type {Page} */
    this.page = page
    this.locator = null
    /**
     * Le span contenant le choix courant (cliquer dessus pour toggle la liste)
     * @type {Locator}
     */
    this.currentChoice = null
    /**
     * @type {Locator[]}
     */
    this.choices = null
    /**
     * @type {Locator}
     */
    this.ul = null
    /**
     * Les choix (innerText des ≠ choices)
     * @type {string[]}
     */
    this.texts = null
    // on supprime la ref à une page fermée
    page.on('close', () => {
      this.page = null
      this.container = null
    })
    // on cherche notre liste dans le dom
    if (selector) selector += ' '
    selector += 'span.listeDeroulante'
    if (has || hasText) {
      /**
       * Le conteneur de la liste déroulante
       * @type {Locator}
       */
      this.locator = this.page.locator(selector, { has, hasText }).first()
    } else {
      this.locator = this.page.locator(selector).nth(index)
    }
  }

  async _init () {
    if (this.currentChoice) return // déjà passé dans _init
    this.currentChoice = await this.locator.locator('.currentChoice')
    this.ul = await this.locator.locator('ul').first()
    this.choices = await this.locator.locator('li').all()
  }

  /**
   * @return {Promise<boolean|undefined>}
   */
  async isOpen () {
    await this._init()
    return this.ul.isVisible()
  }

  /**
   * Déroule la liste déroulante
   * @return {Promise<boolean>}
   */
  async show () {
    const isOpen = await this.isOpen()
    if (isOpen) return true
    await this.currentChoice.click()
    return this.isOpen()
  }

  /**
   * Retourne les textes des ≠ choix possible (sans le 1er "Choisir" s’il n’est pas sélectionnable)
   * @return {Promise<string[]>}
   */
  async getTexts () {
    if (!this.texts) {
      await this._init()
      this.texts = await Promise.all(this.choices.map(c => c.innerText()))
    }
    return this.texts
  }

  /**
   * Déroule la liste et sélectionne text
   * @param {string} text
   * @return {Promise<boolean>}
   */
  async selectText (text) {
    const texts = await this.getTexts()
    const index = texts.findIndex(t => t.includes(text))
    if (index === -1) {
      console.error(Error(`Pas trouvé de choix contenant ${text}`))
      return false
    }
    if (await this.show()) {
      await this.choices[index].click({ timeout: 500 })
      return true
    }
    console.error(Error(`Pas moyen d’afficher la liste contenant ${text}`))
    return false
  }
}

export default ListeDeroulanteHandler
