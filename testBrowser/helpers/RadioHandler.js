/**
 * Pilotage d’un input Mathquill dans le dom
 * Attention : En cas de double édition, penser à faire un clear() pour supprimer le contenu avant de saisir à nouveau des choses.
 * @class
 */
class RadioHandler {
  /**
   *
   * @param {Page} page
   * @param {string} selector
   * @param {number} index
   * @param {Locator} has
   * @param {string} hasText
   */
  constructor (page, { listeChoix = ['oui', 'non'], selector = '', index = 0, has = null, hasText = '' } = {}) {
    this.page = page
    this.index = index
    page.setDefaultTimeout(3000)
    this.locator = null
    // on cherche notre input dans le dom
    /**
       * Le conteneur de l’input
       * @type {Locator}
       */
    this.locator = this.page.locator('input ~ label')
  }

  /**
   * Met le focus sur la zone Mathquill
   * @returns {Promise<void>}
   */
  async choose (text) {
    await this.locator.filter({ hasText: text }).click()
  }
}

export default RadioHandler
