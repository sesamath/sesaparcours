/**
 * Les fonctions spécifiques à j3p, pour manipuler un graphe, récupérer adresses, etc.
 * @module j3p
 */

import { logError } from './log'

// toujours en dev, c’est là qu’il est construit (à partir des usages de prod)
const usageUrl = 'https://j3p.sesamath.dev/sections.usage.json'

const sectionsExcluded = [
  'exo_mep', // flash,
  'ancienexo_mep', // flash aussi,
  'aide_mep', // ruffle, déconne avec playwright
  'squelettepsylvia' // section HS depuis toujours…
]

const sectionsForced = {
  alea: 'sesacommun/646f0f40f9aba80df3b45f42', // pas mal de brouillons qui déconnent, celui-là fonctionne
  excalc: 'sesacommun/5a045859f3429166869fb668',
  lecteuriep: 'sesacommun/637260b500133935f6f48208'
}

const ridExcluded = [
  // Cette ressource doit être personnalisée :
  'sesabibli/59b7d0520cc066253e483846',
  'sesabibli/5a009442457caa4e14f3edff',
  'sesabibli/5b0eedca96682d3747a7222d',
  'sesabibli/5b9a51bac0cfab652e544a52',
  'sesabibli/5c49b17c80dba5090554cc74',
  'sesabibli/5c7b8cbf2337d46c42223307',
  'sesabibli/5efc938762855041607be85e',
  'sesabibli/5feca226d20af72192fe8d86',
  'sesabibli/60267ea73289ea68b711bdde',
  'sesabibli/6055c12b491aff7ab7c250e5',
  'sesabibli/5c49b17c80dba5090554cc74'
]
const setRidExcluded = new Set(ridExcluded)

// fcts privées
function _getBranchementsParams (section) {
  const liste = section[2] // branchements + params
  if (!Array.isArray(liste) || !liste.length) {
    logError('section sans branchements ni params', section)
    return []
  }
  return liste
}

// fcts exportées
export async function getAdresses () {
  const { getAdresses } = await import('../../src/legacy/core/adresses.js')
  return getAdresses()
}

/**
 * Retourne le nœud du graphe correspondant à cette section ou cet id
 * @param {Node[]} graphe
 * @param {string} [nomSection] Le nom de la section
 * @param {string} [id] Passer l’id du nœud voulu dans le graphe (si cette section existe plusieurs fois  dans le graphe et qu’on ne veut pas la 1re occurence)
 * @return {Node|undefined}
 */
export function getNode (graphe, nomSection, id) {
  if (nomSection) return graphe.find(elt => elt[1] === nomSection && (!id || id === elt[0]))
  if (!id) throw Error('Il faut fournir nomSection ou id')
  return graphe.find(elt => id === elt[0])
}

/**
 * Retourne la liste des branchement de cette section
 * @param {Node} section
 * @return {Branchement}
 */
export function getNodeBranchements (section) {
  return _getBranchementsParams(section).filter(item => item.nn)
}

/**
 * Retourne le numéro du nœud de cette section
 * @param {Node} section
 * @return {string}
 */
export function getNodeId (section) {
  return section[0]
}

/**
 * Retourne les infos du nœud dans le graphe
 * @param graphe
 * @param nomSection
 * @param id
 * @return {{branchements: Branchement[], id: string, node: (Node|undefined), params: Object}}
 */
export function getNodeInfos (graphe, nomSection, id) {
  const node = getNode(graphe, nomSection, id)
  if (node == null) { throw Error(`getNodeInfos : node inconnu avec nomSection = ${nomSection} et id = ${id}`) }
  return {
    node,
    branchements: getNodeBranchements(node),
    params: getNodeParams(node),
    id: getNodeId(node)
  }
}

/**
 * Retourne les paramètres du node
 * @param node
 * @return {*}
 */
export function getNodeParams (node) {
  // faut retourner le dernier élément
  const liste = _getBranchementsParams(node)
  if (liste.length < 1) return {}
  return liste[liste.length - 1]
}

/**
 * Retourne une liste de rids, un par sectionsFirst (vire les sections exo_mep et ancien_exo_mep qui sont des contenus flash)
 * @return {Promise<string[]>}
 */
export async function getUsageRids () {
  const { default: fetch } = await import('node-fetch')
  const response = await fetch(usageUrl)
  if (!response.ok) throw Error(`${usageUrl} retourne ${response.status} ${response.statusText}`)
  const { sectionsFirst } = await response.json()
  // pour chaque valeur de sectionsFirst (la clé est le nom de la section, la valeur un array de rids)
  // on prend le premier rid de chaque (il est sur bibli, sauf si y’en a aucun sur bibli)
  return Object.entries(sectionsFirst)
    .map(([section, rids]) => {
      if (sectionsExcluded.includes(section)) return null
      if (sectionsForced[section]) return sectionsForced[section]
      const bibliRid = rids.find(rid => !setRidExcluded.has(rid) && /^sesabibli/.test(rid))
      // if (!bibliRid) console.log('pas trouvé de bibliRid pour', section, 'avec', rids.join('\n'))
      return bibliRid ?? rids[0]
    })
    .filter(rid => rid) // pour virer les null mis dans le map
}

/**
 * @typedef Node
 * @type {Array}
 * @property {string} 0 le numéro du nœud dans le graphe, en string
 * @property {string} 1 le nom de la section
 * @property {SectionParams} 2 Les paramètres du nœud pour cette section
 */
/**
 * @typedef Branchement
 * @property {string} nn Le nœud suivant si condition ok
 * @property {string} pe une condition sur la pe renvoyée par le nœud
 * @property {string} score une condition sur le score
 * @property {string} snn Le nœud suivant si la condition n’est pas remplie
 * @property {number} max Le nb max de passage dans ce branchement
 */
