import { logIfVerbose } from './log'

/**
 * Pilotage d’une fenêtre d’édition Scratch dans le dom
 * @class
 */

class MtgHandler {
  /**
   * // On n’a besoin que de la page, il y a peu de chances qu’on ait plusieurs fenêtres scratch à piloter !
   * @param {Page,id = 'mtg32svg'} page
   */
  constructor (page, id = 'mtg32svg') {
    this.page = page
    this.id = id
    page.setDefaultTimeout(3000)
    // le conteneur de base
    this.mtg32svg = page.locator(`svg#${id}`)
    this.axeAbscisses = null
    this.axeOrdonnees = null
  }

  /**
   * tente de trouver un axe (pour l’instant horizontal) et renseigne la propriété axe :
   * axe.graduations contient la liste des locators des graduations
   * axe.max est la valeur de la graduation maximale
   * axe.lineGraduation est un booléen qui renseigne sur la nature des graduations (true = line ; false = circle)
   * axe.absOrigine, axe.absUnite et axe.absMax sont les abscisses en coordonnées svg des différent points correspondants
   * axe.longueurUnite est la distance en coordonnée svg pour faire une unité
   * @returns {Promise<void>}
   */
  async init () {
    // on renseigne la largeur du svg
    this.width = Number(await this.mtg32svg.getAttribute('width'))
    this.height = Number(await this.mtg32svg.getAttribute('height'))

    // on cherche des axes...
    let axeId = null
    const maxId = Number((await this.page.locator('g').last().getAttribute('id')).split('#')[1])
    const axePossibles = await this.page.locator('line[style="stroke-width:1;stroke:rgb(0,0,0);opacity:1;"]').all()
    for (const line of axePossibles) {
      const x1 = await line.getAttribute('x1')
      const x2 = await line.getAttribute('x2')
      // const y1 = await line.getAttribute('y1')
      // const y2 = await line.getAttribute('y2')
      if (x2 - x1 > 500) { // on tient un axe horizontal peut-être
        axeId = Number((await line.getAttribute('id')).split('#')[1])
      }
      if (axeId) break
    }
    let graduationsId
    let graduations = []
    let idNumber = axeId + 1
    let lineGraduation
    let locator
    do {
      lineGraduation = false
      graduationsId = `${this.id}\\#${idNumber.toString()}`
      locator = this.page.locator(`g#${graduationsId}`)
      graduations = await locator.locator('circle').all()
      if (graduations.length < 5) {
        lineGraduation = !lineGraduation
        graduations = await locator.locator('line').all()
      }
      idNumber++
    } while (graduations.length < 5 && idNumber < maxId)
    if (graduations.length < 5) {
      this.axe = null
    } else {
      // l’origine de l’axe
      const origine = this.mtg32svg.locator('circle[r="1.5"]').first()
      // le point d’abscisse 1
      const unite = this.mtg32svg.locator('circle[r="1.5"]').nth(1)
      // l’abscisse du point unité dans le svg
      const absUnite = Number(await unite.getAttribute('cx'))
      // l’abscisse de l’origine dans le svg
      const absOrigine = Number(await origine.getAttribute('cx'))
      // distance unité dans le svg
      const longueurUnite = absUnite - absOrigine
      const absMax = Number(await graduations[graduations.length - 1].getAttribute(lineGraduation ? 'x1' : 'cx'))
      const max = Math.round((absMax - absOrigine) / longueurUnite)
      this.axe = {
        lineGraduation,
        graduations,
        absOrigine,
        absUnite,
        longueurUnite,
        absMax,
        max
      }
      this.graduationGroupe = locator
    }
  }

  async chercheAxeHorizontal () {
    const axePossibles = await this.page.locator('line').all()
    let axeId, ordonneeAxe
    for (const line of axePossibles) {
      const x1 = await line.getAttribute('x1')
      const x2 = await line.getAttribute('x2')
      const y1 = await line.getAttribute('y1')
      const y2 = await line.getAttribute('y2')
      if (Math.abs(x2 - x1) > 500 && y1 === y2) { // on tient un axe horizontal peut-être
        ordonneeAxe = Number(y1)
        axeId = Number((await line.getAttribute('id')).split('#')[1])
      }
      if (axeId) break
    }
    let graduations
    // on va chercher des graduations sous forme de circle d’abord
    const allCircles = await this.page.locator('circle').all()
    const cercleSurAxe = []
    let abscissesTrouvees = []
    for (const circle of allCircles) {
      if (ordonneeAxe === Number(await circle.getAttribute('cy'))) {
        const cx = Math.round(Number(await circle.getAttribute('cx')))
        if (!abscissesTrouvees.includes(cx)) {
          abscissesTrouvees.push(cx)
          cercleSurAxe.push(circle)
        }
      }
    }
    const locatorsCerclesAvecAbscisses = []
    for (let i = 0; i < abscissesTrouvees.length; i++) {
      locatorsCerclesAvecAbscisses.push({ abscisse: abscissesTrouvees[i], locator: cercleSurAxe[i] })
    }
    const cerclesSurAxeFiltresTries = locatorsCerclesAvecAbscisses.sort((element1, element2) => element1.abscisse < element2.abscisse ? -1 : element1.abscisse > element2.abscisse ? 1 : 0)
    // On cherche ensuite des graduations sous forme de line
    const allLines = await this.page.locator('line').all()
    const lineSurAxes = []
    abscissesTrouvees = []
    for (const line of allLines) {
      if (ordonneeAxe === (Number(await line.getAttribute('y1')) + Number(await line.getAttribute('y2'))) / 2) {
        const x1 = Math.round(Number(await line.getAttribute('x1')))
        if (!abscissesTrouvees.includes(x1)) {
          abscissesTrouvees.push(x1)
          lineSurAxes.push(line)
        }
      }
    }
    const locatorsLinesAvecAbscisses = []
    for (let i = 0; i < abscissesTrouvees.length; i++) {
      locatorsLinesAvecAbscisses.push({ abscisse: abscissesTrouvees[i], locator: lineSurAxes[i] })
    }
    const linesSurAxeFiltresTries = locatorsLinesAvecAbscisses.sort((element1, element2) => element1.abscisse < element2.abscisse ? -1 : element1.abscisse > element2.abscisse ? 1 : 0)

    const lineGraduation = lineSurAxes.length > 5
    if (cercleSurAxe.length > 5) {
      graduations = cerclesSurAxeFiltresTries
    } else if (lineSurAxes.length > 5) {
      graduations = linesSurAxeFiltresTries
    }
    if (graduations.length > 5) {
      return {
        ordonneeAxe,
        lineGraduation,
        graduations
      }
    }
  }

  async chercheAxeVertical () {
    const axePossibles = await this.page.locator('line').all()
    let axeId, abscisseAxe
    for (const line of axePossibles) {
      const x1 = await line.getAttribute('x1')
      const x2 = await line.getAttribute('x2')
      const y1 = await line.getAttribute('y1')
      const y2 = await line.getAttribute('y2')
      if (Math.abs(y2 - y1) > 300 && x1 === x2) { // on tient un axe vertical peut-être
        abscisseAxe = Number(x1)
        axeId = Number((await line.getAttribute('id')).split('#')[1])
      }
      if (axeId) break
    }
    let graduations = []
    const allCircles = await this.page.locator('circle').all()
    const cercleSurAxe = []
    let ordonneesTrouvees = []
    for (const circle of allCircles) {
      if (abscisseAxe === Number(await circle.getAttribute('cx'))) {
        const cy = Math.round(Number(await circle.getAttribute('cy')))
        if (!ordonneesTrouvees.includes(cy)) {
          ordonneesTrouvees.push(cy)
          cercleSurAxe.push(circle)
        }
      }
    }
    let locatorsAvecOrdonnees = []
    for (let i = 0; i < ordonneesTrouvees.length; i++) {
      locatorsAvecOrdonnees.push({ ordonnee: ordonneesTrouvees[i], locator: cercleSurAxe[i] })
    }
    const cerclesSurAxeFiltresTries = locatorsAvecOrdonnees.sort((element1, element2) => element1.ordonnee > element2.ordonnee ? -1 : element1.ordonnee < element2.ordonnee ? 1 : 0)
    // On cherche ensuite des graduations sous forme de line
    const allLines = await this.page.locator('line').all()
    const linesSurAxes = []
    ordonneesTrouvees = []
    for (const line of allLines) {
      if (abscisseAxe === (Number(await line.getAttribute('x1')) + Number(await line.getAttribute('x2'))) / 2) {
        const y1 = Math.round(Number(await line.getAttribute('y1')))
        if (!ordonneesTrouvees.includes(y1)) {
          ordonneesTrouvees.push(y1)
          linesSurAxes.push(line)
        }
      }
    }
    locatorsAvecOrdonnees = []
    for (let i = 0; i < ordonneesTrouvees.length; i++) {
      locatorsAvecOrdonnees.push({ abscisse: ordonneesTrouvees[i], locator: linesSurAxes[i] })
    }
    const linesSurAxeFiltresTries = locatorsAvecOrdonnees.sort((element1, element2) => element1.abscisse < element2.abscisse ? -1 : element1.abscisse > element2.abscisse ? 1 : 0)

    // un booléen pour conserver le type de graduations
    const lineGraduation = linesSurAxeFiltresTries.length > 5
    if (cerclesSurAxeFiltresTries.length > 5) {
      graduations = cerclesSurAxeFiltresTries
    } else if (linesSurAxeFiltresTries.length > 5) {
      graduations = linesSurAxeFiltresTries // les y du svg sont croissants vers les ordonnées négatives, on les classe donc du plus grand au plus petit (dans l’ordre croissant des ordonnées du repère)
    }
    if (graduations.length > 5) {
      return {
        abscisseAxe,
        lineGraduation,
        graduations
      }
    }
  }

  /**
   * modifie la liste des graduations (lorsqu’on a aumenté ou diminué leur nombre)
   * @returns {Promise<void>}
   */
  async updateGraduations () {
    const selector = this.axe.lineGraduation ? 'line' : 'circle'
    this.axe.graduations = await this.graduationGroupe.locator(selector).all()
  }

  /**
   * Trouve la division de l’unité à partir des graduations et la retourne
   * @returns {Promise<number>}
   */
  async getDivisionNumber () {
    const text = await this.page.evaluate(() => Object.values(document.querySelectorAll('g>text>tspan'))
      .find((el) => el.textContent.includes('Nombre de graduations par unité'))
      ?.textContent)
    let division; let presence = false
    if (text != null) {
      division = text.match(/[0-9]+/)[0]
      presence = true
    }
    if (presence) {
      logIfVerbose('Le nombre de divisions est présent : ' + division)
      return Number(division)
    } else {
      await this.updateGraduations()
      const unites = Math.round((this.axe.absMax - this.axe.absOrigine) / this.axe.longueurUnite)
      logIfVerbose(`Le nombre de divisions n’était pas présent, je l’ai détermine : ${Math.round((this.axe.graduations.length - 1) / unites)}`)
      return Math.round((this.axe.graduations.length - 1) / unites)
    }
  }

  /**
   * retourne le locator de la graduation correspondant à abscisse ou undefined s’il ne l’a pas trouvé
   * @param {number} abscisse
   * @returns {Promise<*>}
   */
  async getGraduation (abscisse) {
    const absM = this.axe.absOrigine + this.axe.longueurUnite * abscisse
    // Je n’ai pas trouvé plus simple pour localiser la graduation d’arrivée
    let maGraduation
    let trouve = false
    for (const lineOrCircle of this.axe.graduations) {
      const x = await lineOrCircle.getAttribute(this.axe.lineGraduation ? 'x1' : 'cx')
      if (Math.abs(absM - Number(x)) < 1) {
        maGraduation = lineOrCircle
        trouve = true
        break
      }
    }
    if (trouve) return maGraduation
    else {
      logIfVerbose(`Les paramètres de l’axe sont : ${JSON.stringify(this.axe)}`)
      return `Je n’ai pas trouvé de graduations avec ${this.axe.lineGraduation ? 'x1' : 'cx'} = ${absM}`
    }
  }
}
export default MtgHandler
