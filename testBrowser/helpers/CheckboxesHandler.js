/**
 * Pilotage d’une liste de checkbox
 * @class
 */
class CheckboxesHandler {
  /**
   *
   * @param {Page} page
   * @param {string} selector
   * @param {number} index
   * @param {Locator} has
   * @param {string} hasText
   */
  constructor (page, { selector = '', index = 0, has = null, hasText = '' } = {}) {
    this.page = page
    this.index = index
    page.setDefaultTimeout(3000)
    // La liste des options checkable
    this.listeChoix = null
    // on cherche notre input dans le dom
    /**
       * Le conteneur de l’input
       * @type {Locator}
       */
    this.locator = this.page.locator('div').filter({ has: this.page.locator('label > input ') })
  }

  /**
   * renseigne la liste des options
   * @returns {Promise<void>}
   */
  async _init () {
    if (this.listeChoix != null) return
    this.listeChoix = []
    const locators = await this.locator.locator('label').all()
    for (const labelLocator of locators) {
      const label = await labelLocator.innerText()
      this.listeChoix.push({ label, locator: labelLocator })
    }
  }

  async getChoiceList () {
    if (this.listeChoix == null) await this._init()
    return this.listeChoix
  }

  /**
   * rendre la case cochée
   * @param {string} label // le texte de la case à cocher
   * @returns {Promise<void>}
   */
  async check (label) {
    if (this.listeChoix == null) await this._init()
    const labelElement = this.listeChoix.find((el) => el.label === label)
    if (labelElement != null) {
      const checkbox = labelElement.locator.filter(this.page.getByRole('input'))
      await checkbox.check()
    }
  }

  /**
   *  rendre la case décochée
   *  @param {string} label// le texte de la case à cocher
   * @returns {Promise<void>}
   */
  async unCheck (label) {
    if (this.listeChoix == null) await this._init()
    const labelElement = this.listeChoix.find((el) => el.label === label)
    if (labelElement != null) {
      const checkbox = labelElement.locator.filter(this.page.getByRole('input'))
      await checkbox.uncheck()
    }
  }

  /**
   * retourne l’état de la case à cocher
   * @param label
   * @returns {Promise<boolean>}
   */
  async isChecked (label) {
    if (this.listeChoix == null) await this._init()
    const labelElement = this.listeChoix.find((el) => el.label === label)
    if (labelElement != null) {
      const checkbox = labelElement.locator.filter(this.page.getByRole('input'))
      return await checkbox.isChecked()
    }
  }
}

export default CheckboxesHandler
