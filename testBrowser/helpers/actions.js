import { waitMs } from './promise'
import { logIfVerbose } from './log'

const shortTimeoutOption = { timeout: 1000 }

/**
 * Clique sur le bouton OK de la première boite de dialogue ouverte
 * @param page
 * @return {Promise<void>}
 */
export async function clickFirstDialogOk (page) {
  const selector = '.ui-button:visible:text("OK")'
  const elts = await page.$$(selector)
  if (!elts.length) await page.waitForSelector(selector)
  await page.click(selector)
}

/**
 * Clique sur le bouton OK
 * @param page
 * @return {Promise<void>}
 */
export async function clickOk (page) {
  const locator = page.locator('#Mepboutonvalider')
  await locator.waitFor(shortTimeoutOption)
  await locator.click()
}

/**
 * Reclique sur le bouton jusqu'à ce qu’il disparaisse (max 10×, throw si toujours là )
 * @param {Locator} locator
 * @return {Promise<void>}
 * @throws {Error} si le bouton est toujours visible après 10 clics
 */
async function checkClickSuivant (locator) {
  const label = await locator.getAttribute('value')
  let nbClicks = 1
  while (await locator.isVisible() && nbClicks < 10) {
    await waitMs(100)
    await locator.click()
    nbClicks++
  }
  if (nbClicks > 1) {
    if (await locator.isVisible()) throw Error(`Après ${nbClicks} clics sur ${label} il est toujours visible`)
    else logIfVerbose(`Il a fallu ${nbClicks} clics sur ${label} pour qu’il disparaisse`)
  }
}

/**
 * Clique sur le bouton section suivante
 * @param {Page} page
 * @return {Promise<void>}
 */
export async function clickSectionSuivante (page) {
  const locator = page.locator('#Mepsectioncontinuer')
  await locator.waitFor(shortTimeoutOption)
  await locator.click()
  await checkClickSuivant(locator)
}
/**
 * Clique sur le bouton suite
 * @param {Page} page
 * @return {Promise<void>}
 */
export async function clickSuite (page) {
  const locator = page.locator('#Mepboutoncontinuer')
  await locator.waitFor(shortTimeoutOption)
  await locator.click()
  // parfois ce clic ne fait rien, comme s’il n’avait jamais eu lieu
  // (testé avec un console.warn dans le listener du bouton, dans Parcours.js,
  // et un console.log ici => l’appel de cette fonction ne déclenche pas le listener de l’appli)
  await checkClickSuivant(locator)
}

// cf d4f4d5fa3 pour clickSuivant, finalement plus utilisé
