import fetch from 'node-fetch'
/**
 * Regroupe les méthodes qui manipulent le browser playwright
 * @module browser
 */
import playwright from 'playwright'

import prefs from '../prefs'
import { log, logError, logIfVerbose } from './log'
import { waitMs } from './promise'
import { dropLatex, getMqChunks, normalize } from './text'

export const MAX_TRIES = 2

async function getBrowserPage (browserName) {
  let browser = prefs.browserInstance
  if (!browser || browserName) {
    browser = await initCurrentBrowser(browserName)
  }
  try {
    const [context] = browser.contexts()
    const page = await context.newPage()
    return page
  } catch (error) {
    if (error.message.includes('closed')) {
      if (browser) await browser.close()
      browser = await initCurrentBrowser(browserName)
      const [context] = browser.contexts()
      return context.newPage()
    } else {
      throw error
    }
  }
}

/**
 * Affiche le html de la page courante en console (de node)
 * @param {Page} page
 * @return {Promise<void>}
 */
export async function dump (page) {
  const html = await page.$eval('body', (body) => body.parentNode.innerHTML)
  console.info('La page contient : \n', html)
}

/**
 * Retourne le contenu texte ou html d’un élément
 * @param {Page} page
 * @param {string} selector
 * @param {boolean} [fullHtml=false] Passer true pour récupérer le innerHTML plutôt que le innerText
 * @return {Promise<string>}
 */
export async function getContent (page, selector, fullHtml = false) {
  // avec un selector qui match plusieurs éléments dont le premier est invisible, faut pas faire de waitForSelector sinon il attend que le 1er devienne visible)
  const elts = await page.$$(selector)
  if (!elts.length) await page.waitForSelector(selector)
  const elt = await page.$(selector)
  if (!elt) return null
  // cf https://playwright.dev/docs/api/class-elementhandle#elementhandleinnertext
  return fullHtml ? await elt.innerHTML() : await elt.innerText()
}

/**
 * Retourne le innerText "normalisé" (sans saut de ligne, espaces en doubles virés, tabulations remplacées, etc.)
 * @param {Page} page
 * @param {string} selector Sélecteur playwright ({@link https://playwright.dev/docs/selectors/}
 * @param {boolean} [doDropLatex=false] passer true pour virer aussi le LaTeX
 * @return {Promise<void>}
 */
export async function getContentNormalized (page, selector, doDropLatex) {
  let text = await getContent(page, selector)
  if (doDropLatex) text = dropLatex(text)
  return normalize(text)
}

/**
 * Retourne une nouvelle page sur le browser courant
 * @param {Object} [options]
 * @param {string} [options.browserName]
 * @param {function} [options.consoleListener] Si fourni ça remplacera notre consoleListener par défaut (qui log les error, les warning si verbose et le reste si debug)
 * @param {Logger} [options.logger]
 * @param {boolean} [options.logResponse=false] passer true pour logguer toutes les réponses
 * @param {function} [options.reject] Une fonction à appeler en cas de console.error dans le navigateur (ignoré si prefs.relax), ou si le navigateur crash
 * (mais ce sera probablement à cause d’un pb de RAM et ce script sera peut-être dégagé aussi sans préavis)
 * @return {Promise<Page>}
 */
export async function getDefaultPage ({ browserName, consoleListener, logger, logResponse, reject } = {}) {
  const page = await getBrowserPage(browserName)

  let errorLogger
  if (logger) {
    errorLogger = (...args) => logger('ERROR', ...args)
  } else {
    logger = log
    errorLogger = logError
  }

  // on ajoute le listener sur les messages pour récupérer les console.error
  if (!consoleListener) {
    consoleListener = (message) => {
      // on a déjà eu du `TypeError: message.type is not a function`
      if (typeof message?.type !== 'function') {
        errorLogger('console listener appelé avec un message sans type', message)
        return
      }
      // Cf https://playwright.dev/docs/api/class-consolemessage
      const type = message.type()
      const args = message.args()
      if (type === 'error') {
        if (!args.length) return errorLogger('console.error du navigateur appelé sans argument')
        errorLogger('[Browser error]', ...args)
        if (!prefs.relax) {
          const error = Error(`Erreur dans la console du navigateur => ABANDON\nSur ${page.url()} on a eu\n${args.join('\n')}`)
          if (typeof reject === 'function') reject(error)

          // on throw dans qq ms pour laisser le temps aux fcts async du logger d’écrire en console
          // le pb est qu’on récupèrera pas ça dans le catch de l’appelant…
          // mais sans timeout on le récupère pas non plus…
          // if (!continueOnErrors) throw new Error(`Erreur dans la console du navigateur => ABANDON\nSur ${page.url()} on a eu\n${args.join('\n')}`)
          // => il faut passer une fct reject
          else setTimeout(() => { throw error }, 200)
        }
      } else if (prefs.debug || (prefs.verbose && type === 'warning')) {
        logger(`[Browser ${type}]`, ...args)
      }
    }
  }
  // https://playwright.dev/docs/api/class-page#page-event-console
  page.on('console', consoleListener)
  // https://playwright.dev/docs/api/class-page#page-event-page-error
  page.on('pageerror', reject ?? consoleListener)
  // https://playwright.dev/docs/api/class-page#page-event-crash
  page.on('crash', reject ?? consoleListener)

  page.failedUrls = []
  // on ajoute toujours un listener sur les requests failed
  // https://playwright.dev/docs/api/class-page#pageonrequestfailed
  page.on('requestfailed', (request) => {
    // régulièrement des pbs sur du HEAD (mathlive), qui ne semble pas bloquer la suite…
    const method = request.method()
    if (method === 'HEAD' && !prefs.verbose) return
    const url = request.url()
    page.failedUrls.push(url)
    logger(`request failure on ${method} ${url} ${request.failure().errorText}`)
  })

  // et sur toutes les réponses si on le demande
  if (logResponse) {
    page.on('response', (res) => {
      const req = res.request()
      logger(`${req.method()} ${req.url()} ${res.status()} ${res.statusText()} => ${res.ok() ? 'ok' : 'KO'}`)
      if (prefs.debug) {
        // on affiche aussi le contenu de la réponse json
        const headers = res.headers()
        if (/json/.test(headers['content-type'])) {
          // pas de await ici car on est pas async, on log en tâche de fond
          res.json()
            .then(resJson => logger(`réponse json de ${req.url()} :\n`, resJson))
            .catch((error) => errorLogger(`Réponse json KO sur ${req.url()} : `, error))
        }
      }
    })
  }

  return page
}

/**
 * Retourne les strings LaTeX (sans les $) contenues dans un sélecteur (tableau vide si y’en avait pas)
 * @param {Page} page
 * @param {string} selector
 * @return {Promise<string[]>}
 */
export async function getLatexChunks (page, selector) {
  const text = await getContent(page, selector)
  return getMqChunks(text)
}

/**
 * Retourne les résultats déjà envoyés pour ce graphe
 * @param {Page} page
 * @param {boolean} [doReset=false] Passer true pour remettre à 0 les résultats (mis en global dans la page)
 * @return {Promise<Resultat[]>}
 */
export async function getResultats (page, doReset = false) {
  const resultats = await page.evaluate(() => window.j3pResultats)
  if (doReset) await page.evaluate(() => { window.j3pResultats = [] })
  return resultats
}

/**
 * Réinitialise l’instance courante du browser
 * @param {string} browserName
 * @param {Object} [browserOptions] Les options à passer au lancement du browser ({@link https://playwright.dev/docs/api/class-browsertype?_highlight=launch#browsertypelaunchoptions})
 * @param {Object} [contextOptions] options de contexte éventuel {@link https://playwright.dev/docs/api/class-browser#browsernewcontextoptions}
 * @return {Promise<Protocol.Browser>}
 */
export async function initCurrentBrowser (browserName, browserOptions = {}, contextOptions = {}) {
  logIfVerbose(`init browser ${browserName}`)
  const options = { ...browserOptions } // faut cloner pour que nos affectations ne modifient pas l’objet d’origine
  if (prefs.browserInstance) await prefs.browserInstance.close()
  prefs.browserInstance = null
  if (browserName) {
    if (!prefs.browsers.includes(browserName)) throw Error(`browser ${browserName} invalide (pas dans la liste des browsers autorisés : ${prefs.browsers.join(' ou ')}`)
  } else {
    browserName = prefs.browsers[0]
  }
  const pwBrowser = playwright[browserName]
  options.headless = prefs.headless
  // on ajoute les devtools pour chromium, sauf si on nous demande de pas le faire (ou headless car ça n’aurait pas vraiment d’intérêt)
  if (!prefs.headless && browserName === 'chromium' && prefs.devtools) options.devtools = true
  // chromium plante en headless chez moi, on tente ce workaround https://github.com/microsoft/playwright/issues/4761
  if (prefs.headless) options.args = ['--disable-gpu']
  if (prefs.slow) {
    console.info('slow mis en option du browser', prefs.slow)
    options.slowMo = prefs.slow
  }
  const browser = await pwBrowser.launch(options)
  // on lui crée son contexte
  if (prefs.ignoreHttpsErrors) contextOptions.ignoreHTTPSErrors = true
  await browser.newContext(contextOptions)
  prefs.browserInstance = browser
  logIfVerbose(`browser ${browserName} instancié`)
  return browser
}

/**
 * Charge le graphe par défaut de la section dans le navigateur (via j3p.html)
 * @param {Page} page
 * @param {string} section
 * @return {Promise<void>}
 */
export async function loadDefaultGraphe (page, section) {
  const graphe = [
    1,
    section,
    [{ pe: 'sans%20condition', nn: 'fin', conclusion: 'Fin' }]
  ]
  await loadGraphe(page, { graphe })
}

/**
 * Charge le graphe dans le navigateur (via j3p.html) et ajoute une callback pour mettre les résultats dans un Array global j3pResultats
 * @param {Page} page
 * @param {Object} datas Données initiales à charger via j3pTest
 * @param {Nodes[]} datas.graphe
 * @param {Object} [datas.lastResultat]
 * @param {Object} [datas.editgraphe]
 * @return {Promise<void>>}
 */
export async function loadGraphe (page, datas) {
  if (!datas || !datas.graphe || !Array.isArray(datas.graphe)) throw Error('graphe invalide (pas un tableau)')
  prefs.currentGraphe = null
  // on peut pas affecter directement datas.baseUrl si datas provient d’un import (object is not extensible)
  datas = Object.assign({}, datas, { baseUrl: prefs.baseUrl })
  if (typeof datas.nbTries !== 'number') datas.nbTries = 0
  datas.nbTries++

  // on va démarrer un nouveau graphe, reset des pbs
  page.failedUrls = []

  // on passe pas toujours le graphe complet dans l’url, car s’il est gros ça peut donner du 414 Request-URI Too Large
  let url = prefs.baseUrl + '?testBrowser=1&wait'
  if (prefs.debug) url += '&debug'
  // si le graphe est pas trop gros, on le passe quand même aussi dans l’url, bien pratique pour le debug (et avoir ça dans le log d’erreur)
  // à priori une url < 4k doit toujours passer
  const grapheSerialized = JSON.stringify(datas.graphe)
  if (grapheSerialized.length < 4000 - url.length) url += `&graphe=${grapheSerialized}`

  await purge(url)

  // on charge
  try {
    await page.goto(url, { waitUntil: 'networkidle' }) // il faut networkidle pour que le code qui suit soit exécuté après le chargement des module js async
  } catch (error) {
    if (datas.nbTries < MAX_TRIES && page.failedUrls.length) {
      for (const url of page.failedUrls) await purge(url)
      return loadGraphe(page, datas)
    }
    throw error
  }

  await page.evaluate((datas) => {
    // notre code peut tester ça (pour éviter d’afficher des warnings si ça clique trop vite par ex)
    window.isPlaywright = true
    // pour récupérer les résultats
    window.j3pResultats = []
    datas.resultatCallback = (resultat) => {
      let fixedResultat
      try {
        // on clone tout
        fixedResultat = JSON.parse(JSON.stringify(resultat))
      } catch (error) {
        console.error(error)
        fixedResultat = resultat
      }
      window.j3pResultats.push(fixedResultat)
    }
    window.j3pLoad('playContainer', datas, (error) => {
      if (error) console.error(error)
    })
  }, datas)
  prefs.currentGraphe = datas.graphe
}

/**
 * Reset page puis charge url, recommence si on a des failures (ça arrive souvent, pas trouvé pourquoi)
 * @param {Page} page
 * @param {string} url
 * @param {number} [maxTries=3] Nb max d’essais
 * @return {Promise<number>} Le nb d’essais réalisés
 */
export async function loadUrl (page, url, maxTries = 3) {
  let tries = 1
  resetPage(page)
  await purge(url)
  await page.goto(url)
  while (page.failures.length && tries < maxTries) {
    tries++
    resetPage(page)
    // petit délai pour le laisser reprendre ses esprits (ça marche mieux, pas cherché pourquoi)
    await waitMs(tries * 100)
    await purge(url)
    await page.goto(url)
  }
  return tries
}

/**
 * Retournent les contenus des éléments qui matchent un pattern
 * @param {Page} page
 * @param {string|JSHandle[]} selector
 * @param {RegExp} pattern
 * @return {Promise<JSHandle[]>} La liste des éléments dont le contenu match le pattern
 */
export async function patternFilter (page, selector, pattern) {
  if (!(pattern instanceof RegExp)) return Promise.reject(Error('patternFilter veut un pattern en RexExp'))
  const elts = typeof selector === 'string' ? await page.$$(selector) : selector
  return Promise.all(elts.map(elt => getContent(page, elt, { doNotDispose: true }))).then(contents => {
    const filteredElements = []
    contents.forEach((content, i) => {
      if (pattern.test(content)) filteredElements.push(elts[i])
    })
    return filteredElements
  })
}

export async function purge (url) {
  try {
    const response = await fetch(url, { method: 'PURGE' })
    if (!response.ok) {
      // si on est pas sur un serveur sésamath varnish n’est pas forcément là, on dit rien
      if (!/\.sesamath\.(dev|net)/.test(url)) return
      // sinon c’est pas normal, on le signale
      console.error(`purge ${url} retourne ${response.status} ${response.statusText}`)
    }
  } catch (error) {
    console.error(`purge ${url} KO : ${error}`)
  }
}

/**
 * Vide les propriétés errors/warnings/failures de l’objet page (retourné par getPage)
 * @param {Page} page
 */
export function resetPage (page) {
  page.errors = []
  page.warnings = []
  page.failures = []
}
