/**
 * Retourne le vecteur correspondant au tag svg line fourni
 * @param {JSElement} line
 * @return {Promise<Vecteur>}
 */
export async function lineToVect (line) {
  const v = []
  v.push(Number(await line.getAttribute('x2')) - Number(await line.getAttribute('x1')))
  v.push(Number(await line.getAttribute('y2')) - Number(await line.getAttribute('y1')))
  if (v.some(n => Number.isNaN(n))) throw Error('elt line invalide')
  return v
}

/**
 * récupère dans un array les texts contenus dans les svg contenus dans conteneur
 * @param conteneur
 * @returns {Promise<void>}
 */
export async function getTextsFromSvg (conteneur, { index = 0 } = {}) {
  const textsLocators = await conteneur.locator('text').all()
  const texts = []
  for (const text of await textsLocators) {
    texts.push(await text.textContent())
  }
  return texts
}

export async function getMiddleOfLine (locator) {
  return { x: (Number(await locator.getAttribute('x1')) + Number(await locator.getAttribute('x2'))) / 2, y: (Number(await locator.getAttribute('y1')) + Number(await locator.getAttribute('y2'))) / 2 }
}
