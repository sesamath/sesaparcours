import { logIfVerbose } from 'testBrowser/helpers/log'

/**
 * Pilotage d’une fenêtre d’édition Scratch dans le dom
 * @class
 */
class ScratchHandler {
  /**
   * // On n’a besoin que de la page, il y a peu de chances qu’on ait plusieurs fenêtres scratch à piloter !
   * @param {Page} page
   * @param {number} scrollingIncrement nombre de pixels ajoutés avec la molette de la souris lors d’un pas de scolling (42 par défaut, la hauteur d’un bloc)
   * @param {string[]} categories les categories de tools.
   */
  constructor (page, scrollingIncrement = 42, categories = ['STYLO', 'DEPLACEMENTS', 'CONTROLES']) {
    this.page = page
    page.setDefaultTimeout(3000)
    // le conteneur de base
    // le conteneur du drapeau vert pour lancer l’exécution
    this.drapeau = page.locator('.ScratchDrapeau')
    // idem pour le bouton Stop
    this.stop = page.locator('.ScratchStop')
    // la zone de l’énoncé
    this.enonce = page.locator('td.enonce')
    // la zone contenant le pas à pas et le nombre de blocs utilisés
    this.bandeau = page.locator('.ScratchBandeau')
    // la zone contenant la bibliothèque blockly et la zone du programme
    this.zoneDeTravail = page.locator('.ScratchBlockly')
    // La zone de sortie qui contient le modèle et la zone de rendu
    this.modele = page.locator('.ScratchSortie')
    // le div qui contient le workspace
    this.injectionDiv = page.locator('div .injectionDiv')
    this.blocklyWorkspace = page.locator('.blocklyWorkspace').nth(1)
    this.blocklyFlyout = page.locator('.blocklyFlyout')
    this.chapeau = this.zoneDeTravail.locator('g[data-shapes="hat"]').locator('text .blocklyText')
    this.dropZone = this.zoneDeTravail.locator('g.blocklyBlockCanvas').first()
    this.categoryMenu = []
    // sert à determiner les coordonnées de dropFromTo
    this.offsetDropZone = null
    // La position relative du prochain drop
    this.nextPosition = null
    this.scrolling = scrollingIncrement
    this.scrollingIncrement = scrollingIncrement
    this.categories = categories
  }

  async init () {
    this.nextPosition = { x: 0, y: 0 } // comme on va scroller, on a besoin de ces valeurs initiales
    // on se laisse 2 blocs de marge pour le scrolling qui sinon est bloqué vers le haut
    await this.scrollDropZone(-this.scrollingIncrement)
    const dropZoneBoundingBox = await this.dropZone.boundingBox()
    const flyoutBoundingBox = await this.blocklyFlyout.boundingBox()
    const toolboxBoundingBox = await this.page.locator('.blocklyToolboxDiv').boundingBox()
    this.offsetDropZone = {
      x: toolboxBoundingBox.width + flyoutBoundingBox.width,
      y: dropZoneBoundingBox.height + this.scrollingIncrement
    }
    this.nextPosition = { x: 10, y: 10 } // on peut se servir de ces valeurs pour poser le premier block
  }

  /**
   * Pour trouver les locators des blocks de façon pratique
   * @param {Locator} container Le conteneur dans lequel chercher : par défaut le blocklyWorkspace
   * @param {string?} dataId si l’élément possède un data-id par exemple : "motion_turnright"
   * @param {string?} dataShapes si l’élément possède un data-shapes par exemple ! "hat"
   * @param {boolean} hasTextEditable true si l’élément doit contenir un texte éditable
   * @param {string} classList ajouter la liste des classes permettant de cibler l’élément par exemple : ".blocklyText" ou ".blocklyEditableText"
   * @returns {Locator}
   */
  async getBlockFrom ({ container = this.page, dataId, dataShapes, hasTextEditable = false, classList = '' } = {}) {
    let locator
    if (dataId != null) {
      if (hasTextEditable) {
        const locatorTextInput = this.page.locator('.blocklyEditableText')
        locator = container.locator(`g.blocklyDraggable[data-id="${dataId}"]${classList}`, { has: locatorTextInput })
      } else {
        locator = container.locator(`g.blocklyDraggable[data-id="${dataId}"]${classList}`)
      }
    } else if (dataShapes != null) {
      if (hasTextEditable) {
        const locatorTextInput = this.page.locator('.blocklyEditableText')
        locator = container.locator(`g.blocklyDraggable[data-shapes="${dataShapes}"]${classList}`, { has: locatorTextInput })
      } else {
        locator = container.locator(`g.blocklyDraggable[data-shapes="${dataShapes}"]${classList}`)
      }
    } else {
      locator = container.locator(`g.blocklyDraggable${classList}`).first()
    }
    await locator.waitFor('visible', { timeout: 5000 })
    return locator
  }

  /**
   * mets le focus sur une catégorie de blocs pour éviter le scrolling
   * @param {string} category c’est le label qui apparaît en entête de la liste de blocks concernés ('STYLO', 'DEPLACEMENTS', 'CONTROLES' par exemple).
   * @param {Locator} blockLocator Si renseigné, on s’assure que le bloc est visible, sinon on scrolle vers le bas pour qu’il soit visible
   * @returns {Promise<void>}
   */
  async toggleCategory (categorie = 'DEPLACEMENTS', blockLocator = null) {
    // pour relier les catégories au data-id...
    const scrollBackground = this.injectionDiv.locator('.blocklyScrollbarVertical')
    if (categorie != null && !this.categories.includes(categorie)) throw Error(`Catégorie de blocks non encore incluse ou erreur de syntaxe : ${categorie} avec valeurs possibles : ${this.categories.join(' ; ')}`)
    const locators = await this.injectionDiv.locator('div.scratchCategoryMenuItem', { hasText: categorie }).all()
    let locator
    if (locators.length > 1) {
      for (const loc of locators) {
        const content = await loc.textContent()
        if (content === categorie) {
          locator = loc
          break
        }
      }
      if (locator == null) throw Error(`On n'a pas trouvé le locator associé à ${categorie}`)
    } else locator = locators[0]

    await locator.click()
    if (blockLocator != null) {
      while (!(await blockLocator.isVisible())) {
        await scrollBackground.click({ position: { x: 5, y: 400 } })
      }
    }
    await this.page.waitForTimeout(20)
  }

  /**
   * retourne un array d’objets contenant les locators des différentes catégories de blocks
   * @returns {Promise<[]>}
   */
  async getMenuCategories () {
    const locators = await this.injectionDiv.locator('div.scratchCategoryMenuItem>div.scratchCategoryMenuItemLabel').all()
    for (const locator of locators) {
      this.categoryMenu.push(Object.defineProperty({}, `${await locator.innerText()}`, { value: locator }))
    }
    return this.categoryMenu
  }

  /**
   *
   * @param {string} categorie pour activer la catégorie et ainsi limiter la liste des blocks visibles
   * @param {Locator} source l’élément à déplacer
   * @param {Locator} target l’élément dans lequel déposer
   * @param {x: number, y:number} sourcePosition l’endroit où cliquer dans la source (facultatif, mais parfois nécessaire pour éviter des scrolls intempestifs
   * @param {x: number, y:number} targetPosition obligatoire et doit être précis pour assurer un bon positionnement du bloc lors du dépot
   * @returns {Promise<void>}
   */
  async dragFromTo ({
    category = 'DEPLACEMENTS',
    source,
    target = this.zoneDeTravail,
    targetPosition = { x: 10, y: 10 }
  } = {}) {
    const sourcePosition = { x: 5, y: 5 }
    // on renseigne l’offset de la dropZone lors du premier dragFromTo
    if (targetPosition.y > 300) {
      await this.scrollDropZone(this.scrollingIncrement)
      //  targetPosition.y -= this.scrollingIncrement
    }
    if (!this.categories.includes(category)) throw Error(`Catégorie de blocks non encore incluse ou erreur de syntaxe : ${category} avec valeurs possibles : ${this.categories.join(' ; ')}`)
    if (!['DROITES', 'SEGMENTS', 'DEMI-DROITES'].includes(category)) await this.toggleCategory(category)
    // il semble que Locator ne puisse pas servir de class pour ce test...
    // if (!(source instanceof Locator) && !(target instanceof Locator)) throw Error('La méthode dragFormTo() du ScratchHandler prends comme source et target des Locators !')
    // Il faudrait mettre en place un système pour scroller dans la catégorie jusqu'à ce que le block source soit visible...
    // Que se passe-t-il si les coordonnées de target entraine un scrolling ?

    await source.waitFor('visible', { timeout: 10000 })// normalement, il ne devrait pas avoir besoin de tout ce temps là mais avec la valeur par défaut, des fois, c’est trop juste.
    await source.highlight()
    // await this.page.waitForTimeout(3000)
    await source.dragTo(target, {
      sourcePosition,
      targetPosition: { x: this.offsetDropZone.x + targetPosition.x, y: this.offsetDropZone.y + targetPosition.y }
    })

    await this.page.locator('.blocklySelected').waitFor('visible', { timeout: 10000 }) // j’ai du ajouter un gros timeout car avec 3000 par défaut, des fois, il ne le trouvait pas (sans doute le temps que le drag&drop ait eu de l’effet, que le bloc soit intégré et que sa class blocklySelected soit ajoutée).
    const droppedBlock = await this.page.locator('.blocklySelected').boundingBox()
    const centrage = 8
    this.nextPosition = { x: targetPosition.x, y: targetPosition.y + droppedBlock.height - centrage }
    logIfVerbose(`next position : x=${this.nextPosition.x} ; y=${this.nextPosition.y}`)
  }

  /**
   *
   * @param {Locator} locator Le locator du block à atteindre
   * @param {string} category sa catégorie pour activer la liste de cette catégorie
   * @returns {Promise<void>}
   */
  async fetchBlock (locator, category) {
    if (!['DROITES', 'SEGMENTS', 'DEMI-DROITES'].includes(category)) await this.toggleCategory(category)
    const scrollBackground = this.injectionDiv.locator('.blocklyScrollbarVertical')
    if (locator) {
      let compteur = 0
      let trouve = false
      do {
        if (!(await locator.isVisible())) {
          await scrollBackground.click({ position: { x: 5, y: 400 } })
        } else {
          trouve = true
        }
        compteur++
      } while (compteur < 5 && !trouve) // après 5 scroll, on devrait avoir trouvé le bloc
      if (compteur === 5) throw Error(`On a cherché ${locator} dans la catégorie ${category} et on n’a pas trouvé après 5 scrolls`)
    }
  }

  /**
   * rempli le champ input du blockLocator avec text après avoir mis le focus dessus
   * @param {Locator} blockLocator Le block contenant l’input à remplir
   * @param {string} category La catégorie du block pour réduire à la sous-liste
   * @param {string} text le texte à saisir
   * @param {number} index 0 pour le premier input, 1 pour le deuxième ...
   * @returns {Promise<void>}
   */
  async clickAndType (blockLocator, category, text, index = 0) {
    await this.fetchBlock(blockLocator, category)
    const inputZone = blockLocator.locator('.blocklyEditableText').nth(index)
    await inputZone.click()
    await inputZone.type(text)
  }

  /**
   * utilisée pour nettoyer la zone de drop : on réaliste un dragOut de l’objet situé à positions dans la dropZone
   * @param {x: number, y:number} positions la position à laquelle on va commencer le drag&Drop relativement à la dropZone : il peut être interessant de récupérer pour cela this.nextPosition juste après this.init()
   * @returns {Promise<void>}
   */
  async clearDropZone (positions) {
    await this.scrollDropZone(-this.scrolling)
    // faut d'abord cliquer sur un bouton pour pouvoir réinitialiser ensuite en cliquant sur l'image
    const btnReset = this.page.locator('input:has-text("Réinitialiser")')
    await btnReset.click()
    // l'image pour réinitialiser a changé
    const scratchReset = this.page.locator('img.scratchReset')
    await scratchReset.click({ force: true, position: { x: 1, y: 5 } })
    // await scratchReset.highlight()
    // await this.page.pause()
    this.nextPosition = { x: 1, y: 5 }
  }

  /**
   * @param {number} deltaY incrément en pixels pour le scrolling vertical
   * @returns {Promise<void>}
   */
  async scrollDropZone (deltaY) {
    if (deltaY === 0) return
    // des marges pour situer la zone de travail dans la page
    const margeX = 400
    const margeY = 400
    await this.page.mouse.move(margeX, margeY)
    await this.page.mouse.wheel(0, deltaY)
    this.nextPosition.y -= deltaY
    this.scrolling += deltaY
  }

  /**
   * remet le code en position de départ (annule les éventuels scrolling) afin de préparer un nouvel essai et afin que le premier bloc soit au bon endroit pour le clearDropZone
   * @returns {Promise<void>}
   */
  async resetDropZone () {
    if (this.scrolling > this.scrollingIncrement) await this.scrollDropZone(-this.scrolling + this.scrollingIncrement)
  }
}

export default ScratchHandler
