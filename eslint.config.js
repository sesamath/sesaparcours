// Cf https://eslint.org/docs/latest/use/configure/
// et https://eslint.org/docs/latest/use/configure/migration-guide

// pour forcer les imports sur une seule ligne (pour rechercher avec regex tous les fichiers qui utilisent une fct)
// cf https://github.com/SeinopSys/eslint-plugin-import-newlines
import importNewlines from 'eslint-plugin-import-newlines'
import globals from 'globals'
import neostandard from 'neostandard'

export default [
  // les fichiers à ignorer (https://eslint.org/docs/latest/use/configure/ignore#ignoring-files)
  // ATTENTION : `only global ignores patterns can match directories`
  // => ça doit être précisé ici dans le premier objet de la config
  {
    plugins: { importNewlines },
    rules: {
      // https://github.com/SeinopSys/eslint-plugin-import-newlines#readme
      'importNewlines/enforce': [
        'error',
        {
          items: 1000,
          forceSingleLine: true,
          semi: false
        }
      ]
    },
  },

  // la conf standard, cf https://github.com/neostandard/neostandard?tab=readme-ov-file#configuration-options
  ...neostandard({
    ts: true,
    globals: {
      ...globals.browser
    },
    ignores: [
      // rien dans les node_modules
      'node_modules/',
      // ni le statique ou le code généré
      'docroot/',
      // ni dans du code pas à nous
      'src/vendors/',
      // lui est construit dynamiquement dans mathgraph, on y touche pas
      'src/types/mathgraph.d.ts',
      // ni les fichiers d’un dossier qui commence par un point (mais les .xxx.js seront pris en compte)
      '**/.*/**/*',
      // et qq autres trucs
      'src/lib/outils/basthon/jquery.terminal.js',
      'src/lib/outils/basthon/jquery.terminal.d.ts'
    ]
  }),

  // options communes js & ts
  {
    files: ['**/*.{js,ts}'],
    languageOptions: {
      ecmaVersion: 'latest',
      sourceType: 'module',
    },
    // avec qq ajouts
    rules: {
      // un alert() doit lancer une erreur
      'no-alert': 'error',
      // et un console.log aussi (on autorise console.error, warn, …)
      'no-console': ['error', { allow: ['error', 'warn', 'info', 'debug'] }]
    }
  },

  // moins strict dans legacy (sinon y’a pas grand chose qui passe)
  {
    files: ['src/legacy/**/*.js'],
    rules: {
      camelcase: 'warn',
      eqeqeq: 'warn'
    }
  },

  // loader en es5 (copié directement dans build)
  {
    files: ['src/loader.js'],
    rules: {
      'no-var': 'off',
      'prefer-const': 'off'
    },
    languageOptions: {
      ecmaVersion: 5,
      sourceType: 'script'
    }
  },

  // les scripts node
  {
    files: ['scripts/**/*'],
    languageOptions: {
      globals: {
        ...globals.node
      }
    }
  },

  // les tests
  {
    files: ['test/**/*.test.{ts,js}'],
    rules: {
      // ça c’est pour les trucs du genre `expect(value).to.be.true`
      'no-unused-expressions': 'off'
    },
    languageOptions: {
      globals: {
        ...globals.mocha,
        ...globals.chai
      }
    }
  }
]
