# Journal des changements de sesaparcours (versions 0.x)

`* `[b60c5446b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b60c5446b) retag 0.0.21 (2021-07-24 11:36) <Daniel Caillibaud>  
`* `[aaacf6e58](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aaacf6e58) màj j3p (2021-07-24 11:36) <Daniel Caillibaud>  
`* `[ca677f906](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca677f906) aj option mtgUrl (2021-07-24 11:36) <Daniel Caillibaud>  
`* `[eec5d6462](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eec5d6462) màj sections/lycee/statistiques/sectionstatsDeuxVariables.js (2021-07-23 20:26) <Daniel Caillibaud>  
`* `[697945f94](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/697945f94) import fix sectionApproche (2021-07-23 20:24) <Daniel Caillibaud>  
`* `[131dd78a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/131dd78a0) fix pour imposer la surcharge de Rémi sur la couleur des flèches de vecteur (2021-07-23 14:06) <Daniel Caillibaud>  
`* `[605455076](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/605455076) màj j3p (2021-07-23 13:47) <Daniel Caillibaud>  
`* `[0d793d678](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d793d678) On remet le css de mathquill comme il était et déplace la modif de Rémi dans surcharge.css (2021-07-23 11:24) <Daniel Caillibaud>  
`* `[465136061](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/465136061) résolution de conflit oubliée (2021-07-23 11:19) <Daniel Caillibaud>  
`*   `[b24394c21](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b24394c21) Merge branch 'cssMathquill' (2021-07-23 11:18) <Daniel Caillibaud>  
`|\  `  
`| * `[ea90bd521](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ea90bd521) On remet le css de mathquill comme il était et déplace la modif de Rémi dans surcharge.css (2021-07-23 11:18) <Daniel Caillibaud>  
`| *   `[0d5c86ffd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0d5c86ffd) Merge branch 'master' into Correction_Binomial (2021-07-23 11:15) <Daniel Caillibaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[1c51ed8b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1c51ed8b1) Merge branch 'Resolve_Vecteur_Command' into 'master' (2021-07-23 09:08) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[ca1276a46](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca1276a46) Modification de la commande pour les vecteur squi doit être \vecteur et pas \overrightarrow (2021-07-18 23:34) <Yves Biton>  
`| * | `[842491f00](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/842491f00) Retour en arrière (2021-07-18 23:28) <Yves Biton>  
`| * | `[a30389cb8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a30389cb8) Modification de la oomande vecteur qui doit utiliser \vecteur et non \overrightarrow (2021-07-18 23:02) <Yves Biton>  
`| * |   `[64950b96c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64950b96c) Merge branch 'master' of framagit.org:Sesamath/sesaparcours (2021-07-18 19:43) <Yves Biton>  
`| |\ \  `  
`| * | | `[86ecbd691](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/86ecbd691) Ajout de la commande Latex pour matrice six-six. Correction de widehat pour le LaTeX (2021-06-24 17:59) <Yves Biton>  
`* | | |   `[c20da6b48](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c20da6b48) Merge branch 'cssMathquill' into 'master' (2021-07-23 09:02) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[0c570af6b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c570af6b) aj du css d'origine (2021-07-23 11:01) <Daniel Caillibaud>  
`| * | | | `[af1cf0755](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/af1cf0755) modif css class mq-overarrow pour la couleur (2021-07-22 16:05) <Rémi Deniaud>  
`|/ / / /  `  
`* | | | `[55a60e2fb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/55a60e2fb) retag 0.0.20 | màj j3p (2021-07-19 09:19) <Daniel Caillibaud>  
`* | | | `[ec0328500](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ec0328500) màj liste sections (2021-07-19 09:17) <Daniel Caillibaud>  
`* | | | `[46d49bb4d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/46d49bb4d) màj readme (2021-07-19 09:10) <Daniel Caillibaud>  
`* | | | `[6a220d0b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a220d0b2) fix import dialog (reste pb css) (2021-07-19 09:10) <Daniel Caillibaud>  
`| |/ /  `  
`|/| |   `  
`* | | `[2d6c2e821](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d6c2e821) fix iepLoader => iepLoad (2021-07-17 11:30) <Daniel Caillibaud>  
`* | | `[5ab623f3c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ab623f3c) màj j3p (2021-07-17 11:03) <Daniel Caillibaud>  
`* | | `[60ff4068f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/60ff4068f) fix loading skulpt (2021-07-16 18:25) <Daniel Caillibaud>  
`* | | `[40855eac5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/40855eac5) fix messages en anglais pour blockly (2021-07-16 17:50) <Daniel Caillibaud>  
`* | | `[bb7d8c1e9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb7d8c1e9) fix etiquette (2021-07-16 09:49) <Daniel Caillibaud>  
`* | |   `[8591794e1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8591794e1) Merge branch 'fixSkulpt' (2021-07-16 09:22) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[3d2b9c23c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3d2b9c23c) fix outil skulpt (2021-07-16 09:22) <Daniel Caillibaud>  
`| * | | `[4a469cc68](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4a469cc68) début d'ajout d'outils python/ace/editeurs avec les modules npm, marche pas avec les anciennes sections car la syntaxe skulpt a changé (2021-07-16 09:13) <Daniel Caillibaud>  
`|/ / /  `  
`* | | `[c076f11bb](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c076f11bb) aj sectionblokcond (2021-07-15 10:13) <Daniel Caillibaud>  
`* | |   `[604e37db5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/604e37db5) Merge branch 'fixBlockly' (2021-07-13 13:38) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[5ff06d750](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5ff06d750) màj j3p avec blockly / scratch ok (2021-07-13 13:35) <Daniel Caillibaud>  
`| * | | `[44bd59d3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44bd59d3a) scratch OK (2021-07-13 13:21) <Daniel Caillibaud>  
`| * | | `[21ba26a5d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/21ba26a5d) aj dependencies blockly et scratch-blocks (2021-07-13 12:30) <Daniel Caillibaud>  
`| * | | `[9e5652fca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e5652fca) modif pour tomblok (2021-07-13 12:30) <Daniel Caillibaud>  
`| * | | `[f3e1002a5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3e1002a5) nouvelle tentative de récupération de l'ancien outil scratch (2021-07-13 12:10) <Daniel Caillibaud>  
`* | | |   `[44e1360a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/44e1360a6) Merge branch 'desactiveListe' into 'master' (2021-07-13 10:35) <Daniel Caillibaud>  
`|\ \ \ \  `  
`| * | | | `[0796734b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0796734b7) j3pDesactive pour liste déroulante (2021-07-13 11:30) <Rémi Deniaud>  
`|/ / / /  `  
`* | | |   `[f3c4646e4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3c4646e4) Merge branch 'btnPalette' into 'master' (2021-07-09 13:03) <Rémi Deniaud>  
`|\ \ \ \  `  
`| |/ / /  `  
`|/| | |   `  
`| * | | `[9b2e10942](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b2e10942) Btn palette (2021-07-09 13:03) <Rémi Deniaud>  
`|/ / /  `  
`* | | `[b1cd78e1d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1cd78e1d) retag 0.0.19 | réimport j3p pour les sections blockly (2021-07-08 14:35) <Daniel Caillibaud>  
`* | | `[456c5bfb5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/456c5bfb5) retag 0.0.19 | version bancale pour tenter de régler les pbs blockly/scratch/skulp, mais c'est pas encore ça (2021-07-08 13:47) <Daniel Caillibaud>  
`* | | `[6b164c14a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b164c14a) retag 0.0.18 | fix outils Tableauconversion, BulleAide, brouillon (2021-07-08 08:55) <Daniel Caillibaud>  
`* | | `[dbac83f2f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dbac83f2f) retag 0.0.18 | fix pb j3pCallback (et pas j3pcallback) + fix baseUrl depuis qu'on est plus sur j3p.html (2021-07-07 20:26) <Daniel Caillibaud>  
`* | | `[624544ec0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/624544ec0) màj j3p (2021-07-07 16:00) <Daniel Caillibaud>  
`* | | `[64e7514a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/64e7514a6) retag 0.0.17 | compactage de l'url de test construite avec graphe= (2021-07-07 14:37) <Daniel Caillibaud>  
`* | | `[8ae9fa5b2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8ae9fa5b2) fix export de j3pTest en global, webpack fait encore des siennes (2021-07-07 14:27) <Daniel Caillibaud>  
`* | | `[01ab2252d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01ab2252d) Branch => Connector (2021-07-07 14:13) <Daniel Caillibaud>  
`* | | `[030ece14a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/030ece14a) aj conversion graphe v1 => v2 (2021-07-07 13:35) <Daniel Caillibaud>  
`* | | `[df0b89883](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df0b89883) Un premier modèle de graphe v2 (2021-07-07 13:34) <Daniel Caillibaud>  
`* | | `[f48f4a8bc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f48f4a8bc) retag 0.0.17 | màj j3p (2021-07-07 13:31) <Daniel Caillibaud>  
`* | | `[6aa7d34cf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6aa7d34cf) stringify formate correctement les Error (2021-07-07 11:53) <Daniel Caillibaud>  
`* | | `[ae4695fa1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae4695fa1) aj tuto git (2021-07-07 10:10) <Daniel Caillibaud>  
`* | | `[d3c1cf5e3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3c1cf5e3) màj j3p (2021-07-01 10:11) <Daniel Caillibaud>  
`* | | `[d3658da65](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3658da65) màj view.html pour redirection correcte (2021-06-30 10:15) <Daniel Caillibaud>  
`* | | `[4f4319ec1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4f4319ec1) retag 0.0.16 | màj j3p avec import de j3pDesactive pris dans lib/mathquill/functions (2021-06-29 12:56) <Daniel Caillibaud>  
`* | |   `[f8d8166a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f8d8166a3) Merge branch 'pbDesactive' into 'master' (2021-06-29 06:09) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[3aa26585d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3aa26585d) j3pDesactive différente de J3P (2021-06-28 20:55) <Rémi Deniaud>  
`* | | | `[3dbd1fdfd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3dbd1fdfd) fix import StylesJ3p & j3pDesactive (2021-06-28 16:24) <Daniel Caillibaud>  
`* | | | `[fe3b50b0a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fe3b50b0a) fix devServer (2021-06-28 16:23) <Daniel Caillibaud>  
`* | | | `[b7873eefc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7873eefc) Merge branch 'determinant' into 'master' (2021-06-26 07:01) <Rémi Deniaud>  
`|\| | | `  
`| * | | `[38d1623f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/38d1623f2) ajout de déterminant 2*2 (2021-06-25 18:21) <Rémi Deniaud>  
`|/ / /  `  
`* | | `[6b9a41e02](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6b9a41e02) retag 0.0.15 | j3pDesactive mis dans src/lib/mathquill/functions.js (faudra gérer les mises à jour à la main) (2021-06-24 21:02) <Daniel Caillibaud>  
`* | | `[9690944dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9690944dc) nouvel import après aj du sortable (2021-06-24 20:57) <Daniel Caillibaud>  
`* | | `[599563fb7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/599563fb7) aj gestion du jqueryUI sortable (2021-06-24 18:44) <Daniel Caillibaud>  
`* | | `[802bcad2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/802bcad2b) modif mineure de doc (2021-06-24 18:12) <Daniel Caillibaud>  
`* | | `[f4a6bf062](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f4a6bf062) maxUse doit pas être nul (2021-06-24 18:01) <Daniel Caillibaud>  
`* | |   `[d4e8e0b16](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d4e8e0b16) Merge branch 'MatrixSixSix' into 'master' (2021-06-24 15:56) <Daniel Caillibaud>  
`|\ \ \  `  
`| * | | `[ddf6b1dc8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ddf6b1dc8) Ajout de la commande Latex pour matrice six-six. Correction de widehat pour le LaTeX (2020-06-12 14:04) <Yves Biton>  
`* | | | `[98453a379](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/98453a379) màj j3pAffiche (répercussion des évolutions de j3p) (2021-06-24 08:52) <Daniel Caillibaud>  
`* | | | `[6a8ace2aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a8ace2aa) màj j3p suite à nettoyage (2021-06-23 20:43) <Daniel Caillibaud>  
`* | | | `[4ed860bdc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4ed860bdc) des modifs pour la doc, avec nouvel import (2021-06-23 19:43) <Daniel Caillibaud>  
`* | | | `[1ab9734fc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1ab9734fc) retag 0.0.13 (2021-06-22 20:09) <Daniel Caillibaud>  
`* | | | `[2bc3f5a9a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2bc3f5a9a) fix pnpm start, on démarre en devServer avec le même html que pour le site, la page de base est l'ancien j3p.html, un peu amélioré pour l'occasion (2021-06-22 20:09) <Daniel Caillibaud>  
`* | | | `[54c9c76ac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/54c9c76ac) Début de structuration des objets que l'on manipule lors d'un parcours (2021-06-22 20:08) <Daniel Caillibaud>  
`* | | | `[0c28699da](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0c28699da) fix code retour import (2021-06-22 11:06) <Daniel Caillibaud>  
`* | | | `[c2eb978fd](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2eb978fd) nouvel import j3p et adaptation testsBrowser (2021-06-22 10:48) <Daniel Caillibaud>  
`| |/ /  `  
`|/| |   `  
`* | | `[433ded359](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/433ded359) màj j3p (2021-06-18 11:11) <Daniel Caillibaud>  
`* | | `[b6fa81085](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b6fa81085) fix pb import ZoneFormuleStyleMathquill (2021-06-18 11:08) <Daniel Caillibaud>  
`* | | `[c27d75534](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c27d75534) màj j3p (2021-06-17 10:52) <Daniel Caillibaud>  
`* | | `[ba0cdbc58](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ba0cdbc58) Aj à l'import des outils algebrite, fctsGestionParametres, rexams, three, zoneStyleMathquill, zoneStyleMathquill2; modif J3PEstDansLabomep=>j3pBase, J3PInit_section=>J3Pcallback (2021-06-17 10:48) <Daniel Caillibaud>  
`* | | `[6f0cd1c26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f0cd1c26) retag 0.0.11 | encore qq sections rectifiées (2021-05-28 18:37) <Daniel Caillibaud>  
`* | | `[2e9bc18ce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e9bc18ce) retag 0.0.11 | màj j3p et fix sokoban (2021-05-28 18:02) <Daniel Caillibaud>  
`* | | `[ffed1788b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffed1788b) retag 0.0.11 | màj j3p (2021-05-28 14:55) <Daniel Caillibaud>  
`* | | `[1d36d3901](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1d36d3901) màj usage (2021-05-28 11:07) <Daniel Caillibaud>  
`* | | `[daf570467](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/daf570467) retag 0.0.10 | màj j3p pour boulier (2021-05-04 15:21) <Daniel Caillibaud>  
`* | | `[d3bf2e56b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3bf2e56b) màj j3p (2021-05-04 12:43) <Daniel Caillibaud>  
`* | | `[470cfd32a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/470cfd32a) màj deps (2021-05-04 12:31) <Daniel Caillibaud>  
`* | | `[2f4047e21](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2f4047e21) aj commonNav (2021-05-04 12:30) <Daniel Caillibaud>  
`* | | `[20d0ee203](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20d0ee203) màj usage (2021-05-04 12:29) <Daniel Caillibaud>  
`* | | `[d468987c6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d468987c6) blindage (2021-05-04 12:29) <Daniel Caillibaud>  
`* | | `[b8d9e8eb4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b8d9e8eb4) màj j3p (2020-12-23 18:56) <Daniel Caillibaud>  
`* | | `[df482827f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df482827f) retag 0.0.10 | màj j3p (2020-11-03 16:54) <Daniel Caillibaud>  
`* | | `[d2f59ccce](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d2f59ccce) retag 0.0.9 | màj usage (2020-11-03 16:52) <Daniel Caillibaud>  
`* | | `[c2290bada](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2290bada) retag 0.0.8 | màj j3p (2020-11-03 15:59) <Daniel Caillibaud>  
`* | | `[d0787536e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0787536e) retag 0.0.7 (2020-10-21 11:51) <Daniel Caillibaud>  
`* | | `[cecddddaf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cecddddaf) Ajout lien vers l'utilisation de git/framagit (2020-07-27 11:28) <Daniel Caillibaud>  
`* | | `[7544e1c56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7544e1c56) retag 0.0.6 | màj j3p (2020-07-15 15:26) <Daniel Caillibaud>  
`* | | `[425637205](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/425637205) doc déplacée dans www/doc (2020-06-15 12:48) <Daniel Caillibaud>  
`* | | `[c178d0154](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c178d0154) màj j3p (2020-06-15 12:20) <Daniel Caillibaud>  
`* | | `[103cf717f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/103cf717f) màj deps et ajout jsdoc (2020-06-15 12:19) <Daniel Caillibaud>  
`|/ /  `  
`* | `[c1e256590](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c1e256590) retag 0.0.5 | màj j3p (2020-05-20 19:09) <Daniel Caillibaud>  
`* | `[f59a7a24a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f59a7a24a) retag 0.0.5 | màj j3p (2020-05-20 18:40) <Daniel Caillibaud>  
`* | `[346a97d6f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/346a97d6f) blindage j3pAffiche (2020-05-20 17:38) <Daniel Caillibaud>  
`* | `[9081e2d1c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9081e2d1c) retag 0.0.5 | màj j3p avec les sections qui ne sont jamais en 1er dans un graphe (2020-05-20 17:16) <Daniel Caillibaud>  
`* | `[4c2d5be97](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4c2d5be97) retag 0.0.5 | màj j3p (2020-05-20 15:36) <Daniel Caillibaud>  
`* | `[c63e12e3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c63e12e3a) màj usage (2020-05-19 11:26) <Daniel Caillibaud>  
`* | `[45cc65285](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45cc65285) màj modele (2020-05-18 19:25) <Daniel Caillibaud>  
`* | `[a1c0e5882](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a1c0e5882) retag 0.0.4 | fix chargement de fcts par headJs (depuis l'ajout des outils fctsEtude, fctsGestionParametres et fctsTableauSignes) (2020-05-18 15:32) <Daniel Caillibaud>  
`* | `[d38fd66f1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d38fd66f1) retag 0.0.3 (2020-05-15 17:54) <Daniel Caillibaud>  
`* | `[14aa5b641](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/14aa5b641) récup modifs css qui étaient restées dans une branche Correction_Binomial (2020-05-15 17:54) <Daniel Caillibaud>  
`* |   `[c2dae91f2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c2dae91f2) Merge branch 'fixTableur' (2020-05-15 17:50) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[392a06945](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/392a06945) modif pour gestion de l'outil Tableur avec import (2020-05-15 17:50) <Daniel Caillibaud>  
`|/ /  `  
`| *   `[fdf542d7d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fdf542d7d) Merge branch 'master' into Correction_Binomial (2020-05-15 16:54) <Daniel Caillibaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[0026eac9c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0026eac9c) Merge branch 'Ajoute_Conj' (2020-05-15 16:49) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[6677a4345](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6677a4345) LatexCmds.binomial était défini en double ... (2020-05-15 15:51) <Yves Biton>  
`| * | `[87f357160](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87f357160) nouvele màj j3p (2020-05-07 08:40) <Daniel Caillibaud>  
`| * | `[49949eaa0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/49949eaa0) nouvele màj j3p (2020-05-06 11:51) <Daniel Caillibaud>  
`| * | `[f1b2a2a7a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f1b2a2a7a) fix j3pAjoute_CONJ (et pas j3p_AJOUTE_CONJ) (2020-05-06 11:48) <Daniel Caillibaud>  
`| * | `[9cea772aa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9cea772aa) Correction du nom de J3PAjoute_CONJ encore ... (2020-05-06 11:43) <Yves Biton>  
`| * | `[2a18c97b8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a18c97b8) Correction du nom de J3PAjoute_CONJ (2020-05-06 11:40) <Yves Biton>  
`| * | `[3faa5820a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3faa5820a) Nouvelle branche pour gérer le bouton de conjugaison d'un complexe. Modification de scripts/legacyImport.zsh (2020-05-06 09:46) <Yves Biton>  
`| * | `[1caaf2453](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1caaf2453) Nouvelle branche pour gérer le bouton de conjugaison d'un complexe. (2020-05-06 09:17) <Yves Biton>  
`|/ /  `  
`* | `[e77b49644](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e77b49644) màj loader (2020-05-05 17:40) <Daniel Caillibaud>  
`* | `[20e50a1d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/20e50a1d1) manquait ces nouveaux fichiers pour l'import précédent (2020-05-05 15:51) <Daniel Caillibaud>  
`* | `[c7d9f7fb5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c7d9f7fb5) retag 0.0.2 | import j3p (2020-05-05 15:09) <Daniel Caillibaud>  
`* | `[7c0be70d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c0be70d1) modif pour laisser le comportement de défilement du navigateur si c'est un hash qui correspond à un id (2020-04-23 16:20) <Daniel Caillibaud>  
`* | `[9e449562a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9e449562a) fusion de #json et #jsonFull (2020-04-21 20:16) <Daniel Caillibaud>  
`* | `[30a1b830b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/30a1b830b) aj de la gestion du hash #jsonFull: pour charger graphe & lastResultat & editgraphes (2020-04-21 20:04) <Daniel Caillibaud>  
`| * `[dfcbcfc74](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfcbcfc74) Petite modif css suite conseil Daniel. (2020-04-24 13:12) <Yves Biton>  
`| * `[193d37806](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/193d37806) Modification de css et de MathQuill pour le \binomial. (2020-04-23 15:54) <Yves Biton>  
`|/  `  
`* `[dfb515a9e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/dfb515a9e) màj j3p (2020-04-17 19:35) <Daniel Caillibaud>  
`*   `[2d61e38c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d61e38c7) Merge branch 'fixDialog' into 'master' (2020-04-17 19:16) <Daniel Caillibaud>  
`|\  `  
`| * `[d0f31a84b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d0f31a84b) un résidu de $(this).css() inutile viré (2020-04-17 18:48) <Daniel Caillibaud>  
`| * `[2ce8ba923](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ce8ba923) fix j3psetProps (sinon pas de name sur un div) (2020-04-17 18:45) <Daniel Caillibaud>  
`| *   `[5aeb7c0a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5aeb7c0a4) Merge branch 'master' into fixDialog (2020-04-17 18:14) <Daniel Caillibaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* | `[94d8cf381](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94d8cf381) répercussions modifs Yves oubliées (2020-04-15 11:24) <Daniel Caillibaud>  
`* |   `[c66ec4d35](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c66ec4d35) Merge branch 'Resolution_Decalage_MathQuill' into 'master' (2020-04-15 11:07) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[3e6c90df0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3e6c90df0) Version qui modifie la fonction j3PAffiche pour qu'elle soit compatible avec les versions que j'ai faites dans j3p qui rendront compatibles avec Sesaparcours des sections qui utilisaient des ediable MathQuill sans passer par des champs mq. (2020-04-15 11:07) <Yves Biton>  
`|/ /  `  
`| * `[e75fedac7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e75fedac7) nettoyage et simplifications (une seule boucle sur les fenêtres au lieu de 5|6 (2020-04-17 17:59) <Daniel Caillibaud>  
`| * `[228753f0c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/228753f0c) Modification de J3PAffiche et de J3PCreeFenetres. (2020-04-17 16:23) <Yves Biton>  
`| * `[0210995fa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0210995fa) aj fichiers manquants (2020-04-17 10:07) <Daniel Caillibaud>  
`| * `[fc8638e82](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fc8638e82) fix import j3pCreefenetres séparé + màj j3p (2020-04-16 18:45) <Daniel Caillibaud>  
`|/  `  
`*   `[722da3fea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/722da3fea) Merge branch 'revert-7b374c23' into 'master' (2020-04-10 20:12) <Daniel Caillibaud>  
`|\  `  
`| * `[df1680fdf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/df1680fdf) Revert "Merge branch 'fixRootSections' into 'master'" (2020-04-10 20:11) <Daniel Caillibaud>  
`|/  `  
`*   `[7b374c234](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b374c234) Merge branch 'fixRootSections' into 'master' (2020-04-10 19:57) <Daniel Caillibaud>  
`|\  `  
`| * `[ef1fa633f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef1fa633f) màj j3p avec les sections à la racine de sections/ (2020-04-10 19:55) <Daniel Caillibaud>  
`| *   `[8c91e6271](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8c91e6271) Merge branch 'modifDialog' into fixRootSections (2020-04-10 19:15) <Daniel Caillibaud>  
`| |\  `  
`| | * `[5c8829909](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5c8829909) nouvel import j3p (2020-04-10 18:27) <Daniel Caillibaud>  
`| | * `[7b83bee51](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7b83bee51) gestion de j3pCreefenetres hors legacy (2020-04-10 18:27) <Daniel Caillibaud>  
`| * |   `[b1bec667d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b1bec667d) Merge branch 'master' into fixRootSections (2020-04-10 19:14) <Daniel Caillibaud>  
`| |\ \  `  
`| |/ /  `  
`|/| |   `  
`* | |   `[c173d42a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c173d42a4) Merge branch 'SigneMult' into 'master' (2020-04-10 16:31) <Yves Biton>  
`|\ \ \  `  
`| | |/  `  
`| |/|   `  
`| * | `[26c2a6ac9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/26c2a6ac9) modif de forme (2020-04-10 14:13) <Daniel Caillibaud>  
`| * | `[923eba15e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/923eba15e) Modifié pour redéfinir la fonction jquery focus. Si le champ est un editable mathfield, traitement avec notre mathquill sinon on utilise la fonctio focus de jquery. Et ça marche ! (2020-04-10 13:45) <Yves Biton>  
`| * | `[0b619b26e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b619b26e) Modifié pour qu'un appui sur la touche * donne une signe de multiplication. (2020-04-10 13:13) <Yves Biton>  
`|/ /  `  
`* |   `[f0cc776f5](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f0cc776f5) Merge branch 'fixDecompose01' into 'master' (2020-04-10 12:32) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[32a383251](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/32a383251) manquait cette modif dans le merge précédent (2020-04-10 12:31) <Daniel Caillibaud>  
`| * |   `[21a7e7b1b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/21a7e7b1b) Merge remote-tracking branch 'origin/master' into fixDecompose01 (2020-04-10 12:30) <Daniel Caillibaud>  
`| |\ \  `  
`| |/ /  `  
`|/| /   `  
`| |/    `  
`* |   `[9b43fbd84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b43fbd84) Merge branch 'ajDevServer' into 'master' (2020-04-10 12:12) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[744bfeab6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/744bfeab6) Amélioration des cas gérés par devServer (aj de json dans l'url ou via un fichier) + aj du menu explicatif & README (2020-04-09 19:56) <Daniel Caillibaud>  
`* | |   `[06507b1c7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/06507b1c7) Merge branch 'TraiteRevert' into 'master' (2020-04-09 17:41) <Yves Biton>  
`|\ \ \  `  
`| |/ /  `  
`|/| |   `  
`| * | `[417e674d4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/417e674d4) Modifié pour obtenir le latex d'un chap, éditable qui a été désactivé. + Commentaire (2020-04-09 17:41) <Yves Biton>  
`| | * `[d6b0ecb73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d6b0ecb73) fix mepcalculatrice en global (2020-04-10 11:19) <Daniel Caillibaud>  
`| | * `[61f6ac34c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/61f6ac34c) fix css chargés avec j3pCSSinclude (2020-04-10 11:17) <Daniel Caillibaud>  
`| | * `[bb595bff9](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bb595bff9) fix loading mathquill (2020-04-10 09:50) <Daniel Caillibaud>  
`| |/  `  
`|/|   `  
`* | `[f00aeba53](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f00aeba53) màj j3p (2020-04-09 16:32) <Daniel Caillibaud>  
`|/  `  
`* `[89b52bc97](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89b52bc97) màj j3p (2020-04-09 11:38) <Daniel Caillibaud>  
`*   `[d53555c83](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d53555c83) Merge branch 'ModifMathQuillRevert' into 'master' (2020-04-09 10:52) <Daniel Caillibaud>  
`|\  `  
`| * `[1bef46845](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1bef46845) Modifié le revert de notre MathQuill pour rendre le champ non éditable. (2020-04-09 10:35) <Yves Biton>  
`| * `[e6f6f5ca4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e6f6f5ca4) mathquill.js modifié pour remplacer àa la volée le point déciaml apr une virgule. (2020-04-08 22:51) <Yves Biton>  
`* | `[e8532f369](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8532f369) màj résultat en console dans tous les cas (2020-04-08 17:45) <Daniel Caillibaud>  
`* | `[eb06d4891](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/eb06d4891) màj section751 (2020-04-08 17:15) <Daniel Caillibaud>  
`* |   `[cc38ca307](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cc38ca307) Merge branch 'mathquillNew' into 'master' (2020-04-08 15:58) <Daniel Caillibaud>  
`|\ \  `  
`| * \   `[79c431c79](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/79c431c79) Merge branch 'master' into mathquillNew (2020-04-08 15:57) <Daniel Caillibaud>  
`| |\ \  `  
`| |/ /  `  
`|/| /   `  
`| |/    `  
`* |   `[60a4691d6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/60a4691d6) Merge branch 'doc' into 'master' (2020-04-07 14:40) <Alexis Lecomte>  
`|\ \  `  
`| * | `[9ab6b8fa4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9ab6b8fa4) Doc (2020-04-07 14:40) <Daniel Caillibaud>  
`|/ /  `  
`| * `[16f02f825](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/16f02f825) Des return $elt pour revert et redraw (2020-04-08 15:54) <Yves Biton>  
`| * `[b89e173a3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b89e173a3) Modification du mathquill.js pour qu'il renvoie le sélecteur sauf pour les cas où on appelle .mathquill('latex') pour obtenir le contenu d'un champ MathQuill. Modifications correspondantes pour les fonctions mqAjouteXXX (2020-04-08 14:55) <Yves Biton>  
`| *   `[a3f8b3fac](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3f8b3fac) Merge branch 'mathquillNew' of framagit.org:Sesamath/sesaparcours into mathquillNew (2020-04-08 14:10) <Yves Biton>  
`| |\  `  
`| | * `[d34a29383](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d34a29383) màj sectionappartenancecercle & sectiondecompose01 (2020-04-08 13:19) <Daniel Caillibaud>  
`| | * `[4fa946199](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4fa946199) réimport de 4 sections de Tommy récemment modifiées (2020-04-08 09:26) <Daniel Caillibaud>  
`| | * `[3b314df84](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3b314df84) nouvel import avec remplacement de classes css mathquill (2020-04-08 09:24) <Daniel Caillibaud>  
`| * | `[c9b2ba89b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c9b2ba89b) Modification de mqAjouteExp et mathquill. (2020-04-08 14:08) <Yves Biton>  
`| |/  `  
`| * `[3850753c0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3850753c0) nouvel import j3p pour fix jQueryUi (css dans les css) (2020-04-07 18:07) <Daniel Caillibaud>  
`| * `[a84e33d71](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a84e33d71) fix renommage des fcts mq à l'import (2020-04-07 17:29) <Daniel Caillibaud>  
`| * `[5f0d6bc92](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5f0d6bc92) fcts j3pAffiche et j3pMathsAjouteDans déplacées dans lib/mathquill/functions => nouvel import (2020-04-07 15:38) <Daniel Caillibaud>  
`| * `[381e489cc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/381e489cc) modif module-resolver pour voir si ça règle un pb windows (2020-04-07 14:37) <Daniel Caillibaud>  
`| * `[83e5eb2f7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/83e5eb2f7) simplification path import (2020-04-07 14:34) <Daniel Caillibaud>  
`| * `[58274bc11](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/58274bc11) fix import (2020-04-07 14:10) <Daniel Caillibaud>  
`| *   `[19244e33c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/19244e33c) Merge branch 'mathquillNew' of framagit.org:Sesamath/sesaparcours into mathquillNew (2020-04-07 11:35) <Yves Biton>  
`| |\  `  
`| | * `[429d91149](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/429d91149) fichiers oubliés (2020-04-07 11:34) <Daniel Caillibaud>  
`| * | `[c692efd3e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c692efd3e) Ajout de j3pAffiche et j3pMathsAjouteDans à funcitons ds MathQuill (2020-04-07 11:31) <Yves Biton>  
`| |/  `  
`| * `[695905155](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/695905155) retag 0.0.2 | modifs import pour les fcts mathquill spécial 0.10 isolées + màj j3p (2020-04-07 10:19) <Daniel Caillibaud>  
`| * `[b4694a1ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b4694a1ca) fonctions mathquill J3PAjoute_XX mises dans src/lib/mathquill/functions.js (pour surcharge par rapport à j3p) et renommées en mqAjouteXx (2020-04-06 19:01) <Daniel Caillibaud>  
`| *   `[2d259d0a4](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2d259d0a4) Merge branch 'mathquillNew' of framagit.org:Sesamath/sesaparcours into mathquillNew (2020-04-06 18:26) <Yves Biton>  
`| |\  `  
`| | * `[2a1296c27](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2a1296c27) Tarbre n'a plus besoin de j3pElement (import cyclique) (2020-04-06 17:55) <Daniel Caillibaud>  
`| | *   `[8800d4724](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8800d4724) Merge branch 'doc' into mathquillNew (2020-04-06 17:32) <Daniel Caillibaud>  
`| | |\  `  
`| | | * `[45954d6db](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/45954d6db) Ajout install & utilisation (2020-04-06 17:32) <Daniel Caillibaud>  
`| |_|/  `  
`|/| |   `  
`| | *   `[a7e4f882c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a7e4f882c) Merge branch 'master' into mathquillNew (2020-04-06 17:27) <Daniel Caillibaud>  
`| | |\  `  
`| |_|/  `  
`|/| |   `  
`* | | `[6f400ee9f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6f400ee9f) mod README (2020-04-03 16:31) <Daniel Caillibaud>  
`| | * `[421a0c1fe](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/421a0c1fe) aj commentaire (2020-04-06 17:26) <Daniel Caillibaud>  
`| | * `[d3434ac8c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3434ac8c) import surcharge css + modifs de forme (2020-04-06 17:26) <Daniel Caillibaud>  
`| | * `[99b7b1973](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/99b7b1973) code commenté viré (2020-04-06 17:21) <Daniel Caillibaud>  
`| | * `[09365f062](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/09365f062) aj import Tarbre au début de src/legacy/core/functions.js (2020-04-06 17:21) <Daniel Caillibaud>  
`| * | `[e629a8081](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e629a8081) Modifs de MathQuill. (2020-04-06 17:54) <Yves Biton>  
`| |/  `  
`| * `[d3fede73d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d3fede73d) Modifs Yves pour que les boîtes de dialogues se positionnent comme avant. (2020-04-06 10:12) <Yves Biton>  
`| * `[de765cd43](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/de765cd43) J'ai trouvé pourquoi les boîtes de dialogues ne s'affichaient pas bien : il manquait un         import('jquery-ui/themes/base/theme.css') dans loader.js (2020-04-05 22:38) <Yves Biton>  
`| * `[c378a81b7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c378a81b7) Correction de j3pFocus (2020-04-04 17:59) <Yves Biton>  
`| * `[fb030ef30](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fb030ef30) Ajout de la commenade \vecteur à MathQuill (2020-04-04 17:25) <Yves Biton>  
`| * `[51f096e17](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/51f096e17) commentaire (2020-04-04 15:34) <Yves Biton>  
`| * `[be9f36fec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/be9f36fec) Ajout d'une ligne audébut de functions pour importer Tarbre. A revoir ... (2020-04-04 15:32) <Yves Biton>  
`| * `[2997a46ab](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2997a46ab) Mosdification de mathquill pour que lorsqu'on tape un crochet ouvrant ou fermant l'autre crochet n'apparaisse pas. (2020-04-04 15:29) <Yves Biton>  
`| * `[a3e35d13a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a3e35d13a) commentaire (2020-04-04 13:59) <Yves Biton>  
`| * `[e061bbed6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e061bbed6) Ajout des matrices et systèmes à mathquill.css (2020-04-04 09:52) <Yves Biton>  
`| * `[ca49ee460](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ca49ee460) commentaire (2020-04-04 09:10) <Yves Biton>  
`| * `[2cc824538](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2cc824538) commentaire (2020-04-03 23:09) <Yves Biton>  
`| * `[9b4aa8e56](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9b4aa8e56) Le squelette sur les matrices fonctionne. Commit avant de changer la façon d'utiliser MathQuill. (2020-04-03 20:04) <Yves Biton>  
`| *   `[87f161a3a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/87f161a3a) Merge remote-tracking branch 'origin/mathquillNew' into mathquillNew (2020-04-03 16:30) <Yves Biton>  
`| |\  `  
`| | * `[1f4aa8c26](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/1f4aa8c26) mod README (2020-04-03 16:17) <Daniel Caillibaud>  
`| * | `[e7928bda3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e7928bda3) Nouvelles modifs mais problème au démarrage que je comprends pas ... (2020-04-03 16:29) <Yves Biton>  
`| |/  `  
`| * `[25b1d411e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/25b1d411e) Ajout du squelette sur les matrices mais problème au démarrage que je comprends pas ... (2020-04-03 16:00) <Yves Biton>  
`| * `[cb44aec14](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cb44aec14) Amélioration du css. Modification dde j3PFocus pour ne plus faire de différence avec FireFox (à tester plus tard dans LaboMep). (2020-04-03 11:32) <Yves Biton>  
`| * `[01c8c7e7e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01c8c7e7e) Amélioration du css pour les intégrales (2020-04-02 23:36) <Yves Biton>  
`| * `[0001f188e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0001f188e) commentaire (2020-04-02 20:48) <Yves Biton>  
`| * `[bad7375dc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bad7375dc) Rectification de la figure de test pour le squelette sectionsquelettemtg32_Calc_Param_1_Edit (2020-04-02 09:47) <Yves Biton>  
`| * `[94dd33615](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/94dd33615) 1er test HS de mathquill 0.10 (2020-04-02 00:05) <Daniel Caillibaud>  
`|/  `  
`* `[48dd11567](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/48dd11567) màj j3p (2020-03-31 16:36) <Daniel Caillibaud>  
`* `[2e82f87a0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e82f87a0) fix import (depuis la modif sectionsFirst et sectionsThen dans www/sections.usage.json) (2020-03-31 16:34) <Daniel Caillibaud>  
`* `[8e9712733](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8e9712733) Ajout de LICENSE (2020-03-24 12:51) <Daniel Caillibaud>  
`* `[11963cf01](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/11963cf01) On distingue les ressources où la section est en premier dans le graphe des autres (2020-03-14 01:58) <Daniel Caillibaud>  
`* `[b36937c15](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b36937c15) améliorations pour wdio (2020-03-13 14:49) <Daniel Caillibaud>  
`* `[9592811ae](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9592811ae) on vide le container avant chaque chargement de ressource (2020-03-13 14:49) <Daniel Caillibaud>  
`* `[804c2ec22](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/804c2ec22) màj j3p (2020-03-13 14:44) <Daniel Caillibaud>  
`* `[2e3de6430](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2e3de6430) terser moins verbeux (2020-03-13 14:32) <Daniel Caillibaud>  
`* `[fdea86544](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/fdea86544) màj import j3p (2020-03-13 13:09) <Daniel Caillibaud>  
`* `[caa5f8451](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/caa5f8451) aj d'options skip & limit pour le test wdio (2020-03-13 13:09) <Daniel Caillibaud>  
`* `[b5ffc9357](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b5ffc9357) refresh usage (2020-03-13 12:59) <Daniel Caillibaud>  
`* `[644357027](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/644357027) màj legacy (2020-03-10 05:38) <Daniel Caillibaud>  
`* `[ae0d86d2b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ae0d86d2b) aj conf eslint pour wdio (2020-03-10 05:38) <Daniel Caillibaud>  
`* `[014c8c362](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/014c8c362) un premier test automatisé avec chrome (2020-03-10 05:37) <Daniel Caillibaud>  
`* `[ef91a1f4b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ef91a1f4b) aj favicon (2020-03-10 05:35) <Daniel Caillibaud>  
`* `[9244bac27](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9244bac27) Ajout wdio (2020-03-10 03:50) <Daniel Caillibaud>  
`* `[3dd253fda](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3dd253fda) màj j3p (2020-02-14 19:48) <Daniel Caillibaud>  
`* `[8bf0f8f73](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8bf0f8f73) màj j3p (2020-02-14 17:36) <Daniel Caillibaud>  
`* `[7c8ff3382](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7c8ff3382) màj legacy, on prend toutes les fcts J3P trouvées et plus seulement les appels de fcts (car elles peuvent être passées en callback), donc on importe parfois des fct mentionnées seulement en /* commentaires */ (2020-02-14 13:51) <Daniel Caillibaud>  
`* `[2ac41a091](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ac41a091) renommage _cssGetDimension => j3pcssGetDimension dans les tests (2020-02-14 10:35) <Daniel Caillibaud>  
`* `[78b815417](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/78b815417) màj legacy (2020-02-14 10:31) <Daniel Caillibaud>  
`* `[faa53ccc7](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/faa53ccc7) màj listes (2020-01-29 17:20) <Daniel Caillibaud>  
`* `[3468eacdf](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3468eacdf) source-map avec devServer aussi (2020-01-29 17:20) <Daniel Caillibaud>  
`* `[030c3fe63](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/030c3fe63) Aj de l'option --logEach (2020-01-29 17:20) <Daniel Caillibaud>  
`* `[0b276584b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b276584b) aj de tests unitaires (2020-01-29 17:19) <Daniel Caillibaud>  
`* `[75ae0783d](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/75ae0783d) un peu moins verbeux (2020-01-29 17:18) <Daniel Caillibaud>  
`* `[d1533baec](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d1533baec) aj core-js (2020-01-29 17:18) <Daniel Caillibaud>  
`* `[d576ace38](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d576ace38) màj j3p legacy (2020-01-29 17:18) <Daniel Caillibaud>  
`* `[92345e444](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/92345e444) màj règles d'import (2020-01-29 17:17) <Daniel Caillibaud>  
`* `[2389f39b6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2389f39b6) fix gestion tabulations (2020-01-29 13:11) <Daniel Caillibaud>  
`* `[b7d81e049](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b7d81e049) àj www/index.html (2020-01-26 13:34) <Daniel Caillibaud>  
`* `[0b7c935d2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0b7c935d2) mod mineure (2020-01-25 19:05) <Daniel Caillibaud>  
`* `[536e715e0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/536e715e0) Aj view, rectif liste (2020-01-25 18:34) <Daniel Caillibaud>  
`* `[3a0ec48d8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3a0ec48d8) commit en vrac (2020-01-25 12:38) <Daniel Caillibaud>  
`* `[bc0a34bb6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/bc0a34bb6) màj import pour les iife dans les sections (2020-01-25 12:34) <Daniel Caillibaud>  
`* `[c10b6b58b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/c10b6b58b) màj j3p (2020-01-25 12:34) <Daniel Caillibaud>  
`* `[8fd47e92c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/8fd47e92c) fix husky pre-push (2020-01-25 10:30) <Daniel Caillibaud>  
`* `[3da9903a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3da9903a6) check browser dans populate (2020-01-25 10:29) <Daniel Caillibaud>  
`* `[0ecaabb1a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/0ecaabb1a) màj j3p (2020-01-17 20:29) <Daniel Caillibaud>  
`* `[01e09999f](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/01e09999f) fix build (2020-01-17 20:24) <Daniel Caillibaud>  
`* `[e14be7d33](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e14be7d33) aj détails usage (2020-01-16 17:07) <Daniel Caillibaud>  
`* `[676426653](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/676426653) aj test _cssToNb (2020-01-16 17:06) <Daniel Caillibaud>  
`* `[a747ba7d1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/a747ba7d1) màj core (2020-01-16 17:06) <Daniel Caillibaud>  
`* `[2db11b2b1](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2db11b2b1) màj usage (2020-01-16 16:58) <Daniel Caillibaud>  
`* `[e8b0a117a](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e8b0a117a) màj core (2020-01-16 15:37) <Daniel Caillibaud>  
`* `[d43f77550](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/d43f77550) commentaires + màj mineure de Parcours (2020-01-15 19:00) <Daniel Caillibaud>  
`* `[6a5b4c837](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6a5b4c837) fix récup de l'extension jQuery (2020-01-15 17:39) <Daniel Caillibaud>  
`* `[10ee2eacc](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/10ee2eacc) màj deps (2020-01-15 16:58) <Daniel Caillibaud>  
`* `[150409623](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/150409623) màj legacy & testBrowser (2020-01-15 16:35) <Daniel Caillibaud>  
`* `[97923590c](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/97923590c) on charge aussi du sesacommun (2020-01-14 18:01) <Daniel Caillibaud>  
`* `[9c8a2dcaa](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9c8a2dcaa) modif pour blockly et scratch (2020-01-14 18:00) <Daniel Caillibaud>  
`* `[6c0b13081](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c0b13081) modif refreshUsage + html pour lister tout (2020-01-14 17:15) <Daniel Caillibaud>  
`* `[e5a4e2b10](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/e5a4e2b10) màj legacy (2020-01-14 13:04) <Daniel Caillibaud>  
`* `[6c6374c03](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/6c6374c03) bricoles mineures (2019-12-20 20:04) <Daniel Caillibaud>  
`* `[ffd9aea72](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/ffd9aea72) fix mise en global de mtg32App pour l'outil mtg32 (2019-12-20 19:46) <Daniel Caillibaud>  
`* `[455dc26b0](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/455dc26b0) répercussion modif loader (2019-12-20 19:46) <Daniel Caillibaud>  
`*   `[7a7decf60](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/7a7decf60) Merge branch 'addTests' (il manquait un push qui n'avait pas terminé au moment du merge précédent) (2019-12-20 18:33) <Daniel Caillibaud>  
`|\  `  
`| * `[89dd8cf42](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/89dd8cf42) màj legacy (+2 fichiers de test oubliés au commit précédent) (2019-12-20 18:01) <Daniel Caillibaud>  
`* | `[2ebf73a33](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/2ebf73a33) Merge branch 'addTests' into 'master' (2019-12-20 18:03) <Daniel Caillibaud>  
`|\| `  
`| * `[b14999e8e](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b14999e8e) Aj de tests automatisés avec le browser zombie, c'est pas encore ça mais y'a une bonne base (2019-12-20 17:57) <Daniel Caillibaud>  
`| *   `[b56fe694b](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b56fe694b) Merge branch 'master' into addTests (2019-12-20 11:14) <Daniel Caillibaud>  
`| |\  `  
`| |/  `  
`|/|   `  
`* |   `[5bc02ff43](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/5bc02ff43) Merge branch 'transition' into 'master' (2019-12-20 11:10) <Daniel Caillibaud>  
`|\ \  `  
`| * | `[b54f35fca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b54f35fca) On n'importe que les sections utilisées en prod (et les outils qu'elles utilisent) (2019-12-20 11:08) <Daniel Caillibaud>  
`| * | `[4d9b44a09](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/4d9b44a09) version du jour, après modif de Calculatrice et Tarbre (2019-12-18 19:14) <Daniel Caillibaud>  
`| * | `[f3fca61ea](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/f3fca61ea) maj dépendencies (aj jquery & jquery-ui) (2019-12-17 16:14) <Daniel Caillibaud>  
`| | * `[cf95d90b3](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/cf95d90b3) 1re version de testBrowser (2019-12-20 09:13) <Daniel Caillibaud>  
`| | * `[9f3c3b2a6](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9f3c3b2a6) add mocha & chai & zombie (2019-12-19 10:21) <Daniel Caillibaud>  
`| |/  `  
`| * `[aa73dc769](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/aa73dc769) qq améliorations (ValidationZones) (2019-12-14 19:01) <Daniel Caillibaud>  
`| * `[564dbcab8](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/564dbcab8) qq fix d'import + pnpm start possible sans préciser de section (pour toutes) (2019-12-14 17:10) <Daniel Caillibaud>  
`| * `[3f8633fd2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/3f8633fd2) Ça commence à marcher… (2019-12-13 18:53) <Daniel Caillibaud>  
`| * `[9d1edf4ca](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/9d1edf4ca) WIP, 1er jet (2019-10-24 09:28) <Daniel Caillibaud>  
`|/  `  
`* `[b07577c49](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/b07577c49) aj .gitignore (2019-07-17 08:58) <Daniel Caillibaud>  
