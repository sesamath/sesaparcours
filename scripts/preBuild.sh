#!/bin/sh

ROOT_DIR="$(realpath "$(dirname "$0")/../")"

# si y'a un build.prod on le déplace à la racine avant le build, il sera remis dans docroot en postBuild
DOCROOT="$ROOT_DIR/docroot"
PROD="build.prod"

if [ -d "$DOCROOT/$PROD" ]; then
  if [ -d "$ROOT_DIR/$PROD" ]; then
    echo "avant de lancer un build on a à la fois $ROOT_DIR/$PROD et $DOCROOT/$PROD, on ne conserve que le second"
    rm -rf "$ROOT_DIR/$PROD"
  fi
  mv "$DOCROOT/$PROD" "$ROOT_DIR/"
fi
