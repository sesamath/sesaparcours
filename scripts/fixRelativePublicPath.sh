#!/bin/bash

# Corrige le public path dans les js généré par webpack (indispensable s'il est relatif)

set -u

rootDir="$(cd $(dirname $0); cd ..; pwd)"
publicPath="${1-build}"
# on vire un éventuel ./ au début (il ne sera pas dans les css)
publicPath="${publicPath#./*}"
# on ajoute le / de fin s'il manque
#[[ "$publicPath" =~ /$ ]] || publicPath="$publicPath/"
# on vire le slash éventuel de fin
publicPath="${publicPath#./*}"
base="$rootDir/docroot/build"
for d in module es5; do
  dst="$base/$d"
  echo "nettoyage des css de $dst"
  if [ -d "$dst" ]; then
    sed -i -e "s@url($publicPath/$d/@url(@g" "$dst"/*.css
    echo "nettoyage de url($publicPath/$d/ => url( fait dans $dst"
  else
    echo "$dst n’est pas un dossier"
  fi
done

echo "Attention à contrôler le chargement des dépendances des css après ces modifications (tout exo j3p doit charger l’image de fond par ex)"
