#!/usr/bin/env bash

set -u

[ ! -d .git ] && echo "Ce script doit être lancé depuis la racine du projet" && exit 1

#export VERSION=$(git tag --sort=-committerdate | head -1)
#export PREVIOUS_VERSION=$(git tag --sort=-committerdate | head -2 | awk '{split($0, tags, "\n")} END {print tags[1]}')
#export CHANGES=$(git log --graph --pretty="- %s" $VERSION...$PREVIOUS_VERSION)
#printf "# 🎁 Release notes (\`$VERSION\`)\n\n## Changes\n$CHANGES\n\n## Metadata\n\`\`\`\nThis version -------- $VERSION\nPrevious version ---- $PREVIOUS_VERSION\nTotal commits ------- $(echo "$CHANGES" | wc -l)\n\`\`\`\n"
#
#exit

version=''
CHANGELOG=CHANGELOG.md
# on ne prend dans le changelog que les dernières versions mineures
CURRENT=$(sed -nre 's/.*"version" *: *"([0-9]+\.[0-9]+)\..*/\1/p' package.json)

echo "# Journal des changements de sesaparcours (versions $CURRENT.x)
">$CHANGELOG

git tag|sort --reverse --version-sort|while read prev; do
  [ "$prev" = "v1" ] && continue
  [ -z "$version" ] && version=$prev && continue
  echo '
## version '$version'
' >> $CHANGELOG
  git log --graph --pretty='%h %s (%cd) <%an>' --date='format:%F %R' $prev..$version|sed -re '
    /incrément version/ d;
    /màj CHANGELOG/d;
    s@^([* \\|/]+)([a-z0-9]+) (.*)@`\1`[\2](https://forge.apps.education.fr/sesamath/sesaparcours/-/commit/\2) \3  @;
    s/^([^`].*)$/`\1`  /;
    s@(fix|Fix|FIX) *#([0-9]+)@[fix #\2](https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/\2)@g;
    ' >> $CHANGELOG
  version=$prev
  ! [[ "$prev" =~ ^$CURRENT ]] && break
done

git commit -m "màj CHANGELOG" "$CHANGELOG"
git push --no-verify
