// prévu pour tourner avec node, on ne met pas de shebang pour la compatibilité windows
// doit être exécuté via `node scripts/genGraphAlea⎄.js`

import minimist from 'minimist'
import { existsSync, readFileSync, writeFileSync } from 'node:fs'

const { h, help, c, compact, o, output } = minimist(process.argv.slice(2))

function usage () {
  console.info(`Ce script permet de générer un graphe de 2 à 4 sections qui seront chaînées dans un ordre aléatoire.
--help -h : affiche cette aide 
--compact -c : afficher le graphe construit sans indentation
--output -o graphe.json : mettre la sortie dans le fichier graphe.json 

Exemple: node scripts/genGraphAlea.js -o graphe.json ex.json
Avec ex.json qui contient :
{ 
  "nodes": [{
    "section": "angle02", 
    "params": {
      "Nul": false, 
      "Droit": false 
    }
  }, {
    "section": "bienecrire" 
  }, { 
    "section": "ordredegrandeur" 
  }]
}

Vous pouvez tester la validité du json par ex sur http://json.parser.online.fr/
`)
  process.exit()
}

function abort (message) {
  console.error(message)
  usage()
}

if (h || help) usage()
const file = process.argv.at(-1)
if (!file) abort('Il faut indiquer un fichier json en dernier argument')
if (!existsSync(file)) abort(`Le fichier ${file} n’existe pas`)
const content = readFileSync(file)
let obj
try {
  obj = JSON.parse(content)
} catch (error) {
  console.error(error)
  abort(`Le fichier ${file} ne contient pas du json valide`)
}
const { nodes } = obj
if (!Array.isArray(nodes)) abort('Le json doit contenir un objet avec une propriété nodes qui doit être un tableau')
for (const [i, node] of nodes.entries()) {
  if (typeof node !== 'object') abort(`Le node n°${i + 1} n’est pas un objet`)
  const { section, params } = node
  if (typeof section !== 'string') abort(`Le node n°${i + 1} n’a pas de proprété section)`)
  if (params && typeof params !== 'object') abort(`Le node n°${i + 1} est invalide (params n’est pas un objet`)
}
if (nodes.length < 2 || nodes.length > 4) throw Error('Il faut passer de 1 à 4 nodes')

let suffix = 1

const graphe = []
// pour s’y retrouver
const titreNodes = []

/**
   * Retourne un branchement vers
   * - un nœud fin (créé) si nodes est vide
   * - le nœud passé en argument s’il est unique
   * - un nouveau nœud alea qui renverra vers la liste des nodes passés en argument sinon
   * Ajoute au graphe tous les nœuds créés.
   * @param nodes
   * @return {[{nn: string, score: string}]}
   */
const getBranchement = (nodes) => {
  let nn
  const score = 'sans condition'
  // l’éditeur v1 veut des id numérique et toujours un array (même vide) en 3e arg…
  if (!nodes.length) {
    // faut simplement ajouter un nœud fin, on génère son id
    nn = `${suffix++}`
    graphe.push([nn, 'fin', []])
    titreNodes.push(`fin_${nn}`)
  } else if (nodes.length === 1) {
    // un seul nœud
    const { section, params } = nodes[0]
    nn = `${suffix++}`
    // faut lui ajouter un nœud fin
    const nnFin = `${suffix++}`
    const endNode = [nnFin, 'fin', []]
    const nextNode = [nn, section, [{ nn: nnFin, score }, params ?? {}]]
    graphe.push(nextNode)
    titreNodes.push(`section_${nn}`)
    graphe.push(endNode)
    titreNodes.push(`fin_${nnFin}`)
  } else {
    // plusieurs nœud, donc un nœud alea vers tous nos nodes
    nn = `${suffix++}`
    const aleaNodeArgs = []
    graphe.push([nn, 'alea', aleaNodeArgs])
    titreNodes.push(`alea_${nn}`)
    // on ajoute les branchements de cet aleaNode vers les sections, et ces nœuds destination
    for (const [i, { section, params }] of nodes.entries()) {
      // le nodeId destination
      const nn = `${suffix++}`
      // les nœuds qui restent (splice est mutatif et retourne les éléments supprimés, on veut le nouveau tableau sans modifier l’ancien => toSpliced)
      // const nextNodes = nodes.toSpliced(i, 1)
      // la ligne précédente ne fonctionne qu'à partir de nodeJs 20, on en revient à la méthode splice qui modifie le tableau
      const nextNodes = [...nodes]
      nextNodes.splice(i, 1)
      // ajout branchement vers ce nœud destination
      aleaNodeArgs.push({ pe: `Choix${i + 1}`, nn })
      // ajout du nœud destination
      graphe.push([nn, section, [getBranchement(nextNodes), params ?? {}]])
      titreNodes.push(`${section}_${nn}`)
    }
    // on lui ajoute aussi un branchement sans condition vers un nœud fin (pour que le modèle v2 ne râle pas, il veut toujours un dernier branchement sans condition)
    const nnFin = `${suffix++}`
    graphe.push([nnFin, 'fin', []])
    titreNodes.push(`fin_${nnFin}`)
    aleaNodeArgs.push({ nn: nnFin, score })
    // tous les branchements ont été ajoutés à cet aleaNode, il ne lui manque plus que ses params
    aleaNodeArgs.push({ nbAlea: nodes.length })
  }
  // on retourne le branchement (vers cet aleaNode, ou fin ou destination unique)
  return { nn, score }
}

// et pour démarrer le graphe, il suffit d’appeler getBranchement sans récupérer le branchement (le 1er nœud sera le nœud alea de départ)
getBranchement(nodes)

const ressParams = {
  g: graphe
}
const result = (c || compact)
  ? JSON.stringify(ressParams)
  : JSON.stringify(ressParams, null, 2)
console.info(result)
if (output || o) {
  const outputFile = output || o
  writeFileSync(outputFile, result)
  console.info(`\nCe graphe a été sauvegardé dans ${outputFile}`)
}
