#!/usr/bin/node --loader ts-node/esm
import fs from 'fs'
import path from 'path'
import { fetchGraphe, biblis } from './helpers/sesatheque'
import { stringify } from 'src/lib/utils/object'

const usage = (exitCode = 0) => {
  console.info(`Convertit un graphe v1 en graphe v2. Syntaxe :
1) avec un json local
  scripts/${path.basename(__filename)} chemin/vers/graphe_v1.json
ou (la ligne précédente marche peut-être pas sur votre plate-forme)
  node ${__filename} chemin/vers/graphe_v1.json
ou bien pour mettre le résultat dans un autre fichier
  node ${__filename} chemin/vers/graphe_v1.json > chemin/vers/graphe_v2.json 

2) avec une ressource
  scripts/${path.basename(__filename)} {baseId}/{idRessource}
par ex
  scripts/${path.basename(__filename)} sesabibli/101
`)
  process.exit(exitCode)
}

// une fct main async
async function main () {
  const lastArg = process.argv[process.argv.length - 1]
  if ((new RegExp(path.basename(__filename))).test(lastArg)) usage(1)
  if (['-h', '--help', '--usage'].includes(lastArg)) usage(0)
  let graphe
  if (Object.keys(biblis).some(baseId => lastArg.startsWith(baseId + '/'))) {
    // c’est un rid
    graphe = await fetchGraphe(lastArg)
  } else {
    // c’est un fichier json local
    const json = fs.readFileSync(lastArg, { encoding: 'utf8' })
    graphe = JSON.parse(json)
  }
  // on récupère tout ce qui peut être envoyé à la console, pour sortir du json valide
  const messages = {
    infos: [],
    warnings: [],
    errors: []
  }
  const { info, log, error, warn } = console
  const formatArgs = (args) => args.map(stringify)
  console.info = (...args) => messages.infos.push(formatArgs(args))
  // eslint-disable-next-line no-console
  console.log = (...args) => messages.infos.push(formatArgs(args))
  console.warn = (...args) => messages.warnings.push(formatArgs(args))
  console.error = (...args) => messages.errors.push(formatArgs(args))
  // conversion
  let grapheV2
  try {
    const { convertGraph } = await import('src/lib/core/convert1to2')
    grapheV2 = convertGraph(graphe)
  } catch (e) {
    error(e)
  }
  // on regarde s’il faut ajouter des messages
  if (Object.values(messages).some(values => values.length)) {
    grapheV2.messages = messages
  }
  // on remet la console
  // eslint-disable-next-line no-console
  console.log = log.bind(console)
  console.info = info.bind(console)
  console.error = error.bind(console)
  console.warn = warn.bind(console)
  // et on peut l’utiliser
  console.info('grapheV2', stringify(grapheV2, 2))
}

main()
  .catch((error) => {
    console.error(error)
    usage(1)
  })
