// prévu pour tourner avec node, on ne met pas de shebang pour la compatibilité windows

/**
 * Importe chaque section pour vérifier ses exports (notamment le format des params)
 * Doit être exécuté avec
 *   `ts-node --esm scripts/checkSectionsExports.ts`
 * ou
 *   `node --loader ts-node/esm scripts/checkSectionsExports.ts`
 * On peut ajouter l’option `--section xx,yy` pour limiter à une ou quelques sections
 * @description
 */
import minimist from 'minimist'

import { checkSectionExports } from 'src/lib/core/checks'
import { getSectionsList } from 'src/lib/core/loadSection'

const args = process.argv.slice(2)
const options = minimist(args)

try {
  const sections: string[] = ('sections' in options) ? options.sections.split(',') : getSectionsList()
  for (const section of sections) {
    const { warnings, errors } = await checkSectionExports(section).then()
    for (const warning of warnings) console.warn(`warning sur la section ${section} : ${warning}`)
    for (const error of errors) console.warn(`ERORR sur la section ${section} : ${error}`)
  }
} catch (error) {
  console.error(error)
}
