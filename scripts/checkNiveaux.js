// prévu pour tourner avec `node --loader ts-node/esm`, on ne met pas de shebang pour la compatibilité windows
// pour le lancer sur une bibli locale ce sera par ex
// NODE_TLS_REJECT_UNAUTHORIZED=0 NODE_NO_WARNINGS=1 node --loader ts-node/esm "scripts/checkNiveaux.js" --bibliId=biblilocal3001

import fs from 'node:fs'
import { basename, dirname, resolve } from 'node:path'
import { fileURLToPath } from 'node:url'

import minimist from 'minimist'

import stConfig from 'sesatheque-client/dist/server/config.js'
import getLogger from './helpers/logger.js'
import { fetchRessource, fillEnfants } from './helpers/sesatheque.js'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

const usageFile = resolve(__dirname, '..', 'public', 'sections.usage.json')
const levelFile = resolve(__dirname, '..', 'public', 'niveaux.usage.json')

const logger = getLogger(basename(__filename, '.js'))

const args = process.argv.slice(2)
const options = minimist(args)

// verbeux par défaut, passer -q ou --quiet en option pour le faire taire
const isQuiet = Boolean(options.quiet || options.q)

const bibliId = options.bibliId ?? 'sesabibli'

function log () {
  if (isQuiet) return
  logger.log.apply(null, arguments)
}
function logError () {
  logger.error.apply(null, arguments)
}

const { niveaux: levelNames } = stConfig
/** liste des arbres par niveau (à partir de https://bibliotheque.sesamath.net/ressource/apercevoir/50035) */
const treesByLevel = {
  // CM2
  7: ['67595ad4c9a4931b31609ba1', '5bd006a7129e5b1076e56666'],
  6: ['605b660685d4f6024ced4266', '5bcc75c98e5698107cb6c8d8'],
  5: ['605b66af0bc4ea0272aaeb19', '5bcc877e8e5698107cb6c8db'],
  4: ['605b68fc85d4f6024ced4268', '5bcc89268e5698107cb6c8dd'],
  3: ['605b672085d4f6024ced4267', '5bcc89a78e5698107cb6c8de'],
  2: ['605b6ac885d4f6024ced426a', '5bcc89c9129e5b1076e56615'],
  // 2de pro
  '2p': ['62b4099163fa0418db7e52b0'],
  1: ['67595ccb712299f42ea744ec', '5bcc89eb129e5b1076e56617'],
  // '1p': ['', ''],
  // tle
  12: ['605b6c5485d4f6024ced426b', '5bd006738e5698107cb6c921']
  // tle pro
  // '12p': ['', '']
}
const otherTrees = [
  '50045'
]

/**
 * @typedef RessTitTypNiv
 * @extends RessRidTitTyp
 * @property {string[]} niveaux
 */
/**
 * Les ressources parsées avec titre/type/niveaux
 * @type {RessTitTypNiv}
 */
const ressByOid = {}
/** Liste d'oids présent dans un arbre de niveaux */
const mentioned = new Set()
/**
 * Liste des arbres par niveau
 * @type {Object<string, Array<{oid: string, titre: string}>>}
 */
const levelTrees = {}
/** Liste de warnings par oid d’arbre */
const levelTreeWarnings = {}
/** Liste de ressources dans aucun arbre (ni niveau ni others) */
const levelOrphans = []
/** Liste de ressources dans aucun arbre de niveau mais dans others */
const levelOrphansInOthers = []
/**
 * Liste de ressources sans niveau
 * @type {Array<{oid: string, titre: string}>}
 */
const levelLess = []
/** Les titres des arbres par oid */
const treeTitles = {}

/**
 * Complète ressByOid d'après usageFile (construit par refreshUsage)
 * @returns {Promise<void>}
 */
async function initJ3pRessources () {
  log('Init des ressources déjà parsées par refreshUsage.js')
  if (!fs.existsSync(usageFile)) {
    logError(`Le fichier ${usageFile} n’existe pas, il faudrait lancer refreshUsage avant ce script (qui va être bcp plus long)`)
    return
  }
  const { default: { ressources } } = await import(usageFile, { assert: { type: 'json' } })
  for (const { rid, titre, niveaux } of Object.values(ressources)) {
    if (!rid.startsWith(bibliId)) continue
    const oid = rid.replace(`${bibliId}/`, '')
    ressByOid[oid] = { titre, niveaux, type: 'j3p' }
  }
}

async function fetchTree (treeOid) {
  const treeRid = `${bibliId}/${treeOid}`
  const tree = await fetchRessource(treeRid, 'arbre')
  // on note le titre
  treeTitles[treeOid] = tree.titre
  /** @type {RessRidTitTyp[]} */
  const enfants = []
  /** @type {Array<string|{oid: string, titre: string, warn: string}>} */
  const warnings = []
  await fillEnfants({ arbre: tree, enfants, warnings, wantedBaseId: bibliId })
  return { tree, enfants, warnings }
}

async function checkNiveaux () {
  log('Analyse des arbres de ressource par niveau')
  for (const [niveau, trees] of Object.entries(treesByLevel)) {
    for (const treeOid of trees) {
      try {
        const { tree, enfants, warnings } = await fetchTree(treeOid)
        const treeLabel = `${treeOid} (${tree.titre})`
        const { oid, titre, niveaux: nxTree } = tree
        if (!nxTree.includes(niveau)) {
          logError(`Arbre ${treeLabel} listé comme arbre de niveau ${niveau} mais il n’est pas de ce niveau`)
        }
        if (!levelTrees[niveau]) levelTrees[niveau] = []
        levelTrees[niveau].push({ oid, titre })

        for (const { rid, type, titre: titreEnfant } of enfants) {
          const [baseId, oid] = rid.split('/')
          if (baseId !== bibliId) {
            warnings.push(`L’arbre ${treeLabel} a un enfant qui n’est pas sur ${bibliId} : ${rid} (${titreEnfant})`)
          }
          if (!ressByOid[oid]) {
            if (type === 'j3p') {
              logError(`La ressource ${oid} est de type j3p mais n’était pas dans ${usageFile}`)
            }
            // faut récupérer ses niveaux
            try {
              const enfant = await fetchRessource(rid, '')
              if (enfant.type !== type) {
                logError(`Incohérence de type, ${oid} a le type ${enfant.type} alors qu’il était dans l’arbre ${treeLabel} avec le type ${type}`)
                continue
              }
              ressByOid[oid] = { niveaux: enfant.niveaux, titre: enfant.titre, type }
            } catch (error) {
              if (error.status === 404) {
                logError(`L’arbre ${treeLabel} contient une ressource ${rid} (${titreEnfant}) qui n’existe plus.`)
              } else {
                logError(`Pb pour récupérer la ressource ${rid} (${titreEnfant})`, error)
              }
              continue
            }
          }
          mentioned.add(oid)
          const { niveaux, titre } = ressByOid[oid]
          if (Array.isArray(niveaux) && niveaux.length) {
            // ok s'il contient le niveau de l'arbre
            if (niveaux.includes(niveau)) continue
            // sinon c'est un warning
            warnings.push({ oid, titre, warn: `n’a pas le niveau ${levelNames[niveau]} (${(niveaux ?? []).map(niv => levelNames[niv] ?? niv).join(', ')})` })
          } else {
            warnings.push({ oid, titre, warn: `n’a pas de niveaux, elle devrait avoir au moins "${levelNames[niveau]}" car elle est dans l’arbre ${treeLabel}` })
            levelLess.push({ oid, titre })
          }
        } // fin boucle enfants

        levelTreeWarnings[treeOid] = warnings
      } catch (error) {
        if (error.status === 404) {
          logError(`L’arbre ${treeOid} mentionné dans le niveau ${niveau} n’existe pas.`)
        } else {
          logError(`Erreur lors de l’analyse de l’arbre ${treeOid} mentionné dans le niveau ${niveau} `, error)
        }
      }
    } // fin boucle arbre
  } // fin boucle niveaux

  // on passe à others pour ajouter ses enfants à mentioned
  for (const oid of otherTrees) {
    try {
      const { enfants, warnings } = await fetchTree(oid)
      levelTreeWarnings[oid] = warnings
      for (const enfant of enfants) {
        const { rid, titre, type } = enfant
        if (type !== 'j3p') continue
        if (rid) {
          const [, oid] = rid.split('/')
          if (!mentioned.has(oid)) {
            levelOrphansInOthers.push({ oid, titre, type })
            mentioned.add(oid)
          }
        } else {
          logError(`Enfant sans rid dans l’arbre ${oid}`, enfant)
        }
      }
    } catch (error) {
      console.error('Pb dans l’analyse de l’arbre other', error)
    }
  }

  // on regarde les ressources qui n'ont été listées par personne
  log('Analyses des ressources qui ne sont dans aucun arbre de niveau')
  for (const oid of Object.keys(ressByOid)) {
    if (!mentioned.has(oid)) levelOrphans.push({ ...ressByOid[oid], oid })
  }
  // reste à écrire notre json
  const levelFilePrevious = levelFile.replace('.json', '.prev.json')
  if (fs.existsSync(levelFile)) {
    fs.copyFileSync(levelFile, levelFilePrevious)
    log(`${levelFile} précédent sauvegardé dans ${levelFilePrevious}`)
  }
  const data = {
    levelLess,
    levelOrphans,
    levelOrphansInOthers,
    levelTreeWarnings,
    treesByLevel,
    levelNames,
    treeTitles,
    date: new Date()
  }
  fs.writeFileSync(levelFile, JSON.stringify(data))
}

initJ3pRessources()
  .then(checkNiveaux)
  .catch(logError)
