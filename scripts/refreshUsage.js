// prévu pour tourner avec `node --loader ts-node/esm`, on ne met pas de shebang pour la compatibilité windows
// doit être exécuté via `pnpm run refreshUsage`

import fs from 'node:fs'
import { basename, dirname, resolve } from 'node:path'
import { fileURLToPath } from 'node:url'

import queryString from 'query-string'
import minimist from 'minimist'

// on tourne avec `node --loader ts-node/esm scripts/refreshUsage.js` faut lui mettre les suffixes
import { getAdresses } from 'src/legacy/core/adresses.js'
import { dedup } from 'src/lib/utils/array.ts'

import { fetchRessource, fetchList, getBibliBaseUrl } from './helpers/sesatheque.js'
import getLogger from './helpers/logger.js'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

const usageFile = resolve(__dirname, '..', 'public', 'sections.usage.json')

const logger = getLogger(basename(__filename, '.js'))

const args = process.argv.slice(2)
const options = minimist(args)

// verbeux par défaut, passer -q ou --quiet en option pour le faire taire
const isQuiet = Boolean(options.quiet || options.q)

const bibliId = options.bibliId ?? 'sesabibli'
const communId = options.communId ?? 'sesacommun'

const limit = 50

function log () {
  if (isQuiet) return
  logger.log.apply(null, arguments)
}
function logError () {
  logger.error.apply(null, arguments)
}

const adresses = getAdresses()
const knownSections = new Set(Object.keys(adresses))

/**
 * La liste de toutes les ressources et toutes les sections qu’elles utilisent
 */
const usage = {
  // la liste rid => aliasOf
  aliases: {},
  // la liste rid => [ridAlias1, ridAlias2, ...]
  clones: {},
  // la liste des ressources, clé rid
  ressources: {},
  // la liste des sections, clé sectionName et valeur liste des rid qui l’utilisent en premier dans le graphe
  sectionsFirst: {},
  // la liste des sections, clé sectionName et valeur liste des rid qui l’utilisent en 2e ou suivant
  sectionsThen: {},
  // la liste des sections inconnues, clé sectionName et valeur liste des rid qui l’utilisent
  sectionsUnknown: {}
}

/**
 * Retourne la liste des sections du graphe (nœuds fin enlevés, mais doublons laissés)
 * @param {Array[]} graphe
 * @return {string[]}
 */
const getSections = (graphe) => graphe
  .map(grapheNode => Array.isArray(grapheNode) && grapheNode[1])
  .filter(name => name && name.toLowerCase() !== 'fin')

/**
 * Retourne les éléments de liste en virant les doublons
 * @param {*[]} liste
 * @return {*[]}
 */
const filterUnique = (liste) => {
  // on passe par un Set pour gérer l’unicité
  // (plutôt que de faire le test uniqList.includes(item) puis push éventuel à chaque itération)
  const uniqList = new Set()
  liste.forEach(section => uniqList.add(section))
  return Array.from(uniqList)
}

/**
 * Peuple usage avec la ressource
 * @param {Ressource} ressource
 */
function parseRessource (ressource) {
  const { aliasOf, rid, titre, type, parametres, niveaux } = ressource

  // pbs qui auraient dû être filtrés en amont
  if (usage.ressources[rid]) {
    logError(`parseRessource appelée avec une ressource déjà traitée : ${rid}`)
    return
  }
  if (aliasOf) {
    logError(Error('parseRessource appelée avec un alias'), ressource)
    return
  }
  if (type !== 'j3p') {
    logError(Error(`parseRessource appelée avec une ressource de type ${type} (${rid})`))
    return
  }
  // graphe invalide
  if (!parametres || !Array.isArray(parametres.g)) {
    logError(`La ressource ${rid} n’a pas de graphe valide`)
    return
  }

  // on ajoute la ressource à usage
  usage.ressources[rid] = { rid, titre, niveaux }

  // et ses sections
  const sectionsList = getSections(ressource.parametres.g) // toutes, dans l’ordre, sans les nœuds fin
  const sections = filterUnique(sectionsList) // doublons virés
  if (!sections.length) {
    // on ajoute le graphe bizarre dans le json
    const error = `La ressource ${rid} a un graphe sans section`
    usage.ressources[rid].graphe = ressource.parametres.g
    usage.ressources[rid].error = error
    logError(error)
    return
  }
  let isFirst = true
  for (const sectionName of sections) {
    // on ajoute le rid à la liste des ressources qui utilisent cette section
    log(`Aj de ${rid} à la section ${sectionName}`)
    if (knownSections.has(sectionName)) {
      if (isFirst) {
        // idem si la ressource utilise cette section en premier
        if (!usage.sectionsFirst[sectionName]) usage.sectionsFirst[sectionName] = []
        usage.sectionsFirst[sectionName].push(rid)
      } else {
        if (!usage.sectionsThen[sectionName]) usage.sectionsThen[sectionName] = []
        usage.sectionsThen[sectionName].push(rid)
      }
    } else {
      logError(`section ${sectionName} inconnue (dans ${rid})`)
      if (!usage.sectionsUnknown[sectionName]) usage.sectionsUnknown[sectionName] = []
      usage.sectionsUnknown[sectionName].push(rid)
    }
    isFirst = false
  }
}

/**
 * Récupère toutes les ressources de type j3p sur la sesatheque baseId, et peuple la var globale usage
 * @param {string} baseId
 * @param {number} [skip=0]
 * @return {Promise<undefined>} Quand c’est fini
 */
async function fetchAndFillUsage (baseId, skip = 0) {
  const searchQuery = { type: 'j3p', skip, limit, orderBy: 'oid' }
  const { liste, queryOptions } = await fetchList(baseId, searchQuery)
  const rids = liste
    // vire les alias en les notant
    .filter(({ aliasOf, aliasRid }) => {
      if (!aliasRid) return true
      if (!usage.aliases[aliasOf]) usage.aliases[aliasOf] = []
      usage.aliases[aliasOf].push(aliasRid)
      return false
    })
    .map(({ aliasOf }) => aliasOf)
  const isFinished = liste.length < queryOptions.limit

  // on veut pas s’arrêter si un des fetch échoue, ce qui serait le cas avec la ligne suivante
  // return Promise.all(rids.map(fetchJ3pRessource))
  while (rids.length) {
    const rid = rids.pop()
    try {
      if (usage.ressources[rid]) {
        logError(`La recherche sur ${getBibliBaseUrl(baseId)}api/liste?${queryString.stringify(searchQuery)} remonte ${rid} que l’on avait déjà eu précédemment !`)
        continue
      }
      const ressource = await fetchRessource(rid, 'j3p')
      parseRessource(ressource)
    } catch (error) {
      logError(`Pb avec la ressource ${rid} :`, error)
    }
  }
  // on a fini pour ce paquet, on passe au suivant si besoin
  if (!isFinished) return fetchAndFillUsage(baseId, skip + limit)
}

fetchAndFillUsage(bibliId)
  .then(() => {
    log(`fin de la récupération des graphes des ressources j3p de ${bibliId}`)
    return fetchAndFillUsage(communId)
  })
  .then(() => {
    log(`fin de la récupération des graphes des ressources j3p de ${communId}`)
    const sections = dedup(Object.keys(usage.sectionsFirst), Object.keys(usage.sectionsThen))
    const nbSections = sections.length
    const nbRessources = Object.keys(usage.ressources).length
    const nbAliases = Object.keys(usage.aliases).length
    log(`On a ${nbRessources} ressources utilisant ${nbSections} sections (et il y a ${nbAliases} aliases)`)
    const unknownSections = Object.keys(usage.sectionsUnknown)
    if (unknownSections.length) {
      const ridsUsingUnknown = new Set()
      for (const rids of Object.values(usage.sectionsUnknown)) {
        for (const rid of rids) ridsUsingUnknown.add(rid)
      }
      logError(`Il y a ${unknownSections.length} sections inconnues (${unknownSections.join(', ')}) utilisées dans ${ridsUsingUnknown.size} ressources (${Array.from(ridsUsingUnknown).join(', ')})`)
    }
    // on enregistre ça dans le fichier usage.json
    const usageFilePrevious = usageFile.replace('.json', '.prev.json')
    if (fs.existsSync(usageFile)) {
      fs.copyFileSync(usageFile, usageFilePrevious)
      log(`${usageFile} précédent sauvegardé dans ${usageFilePrevious}`)
    }
    usage.date = (new Date()).toLocaleDateString()
    fs.writeFileSync(usageFile, JSON.stringify(usage))
    // et la liste des sections seules
    const listFile = resolve(__dirname, '..', 'public', 'sections.list')
    const stream = fs.createWriteStream(listFile, { flags: 'w', mode: '0644' })
    sections.forEach(section => stream.write(`${section} ${adresses[section]}/section${section}.js\n`))
    stream.end()
    log(`${usageFile} sauvegardé`)
  })
  .catch(logError)
