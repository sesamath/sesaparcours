/**
 * This file is part of SesaParcours.
 *   Copyright 2014-2015, Association Sésamath
 *
 * SesaParcours is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * SesaParcours is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with SesaParcours (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de SesaParcours, créée par l’association Sésamath.
 *
 * SesaParcours est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * SesaParcours est distribué dans l’espoir qu’il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d’ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
import fs from 'node:fs'
import { dirname, resolve } from 'node:path'
import { fileURLToPath } from 'node:url'

import { formatDateTime } from 'sesajs-date'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

const logDir = process?.env?.LOGDIR ?? resolve(__dirname, '..', '..', 'log')

const levels = {
  debug: 0,
  notice: 1,
  warning: 2,
  error: 3
}
const prefixes = {
  debug: ' [debug] ',
  notice: '',
  warning: ' [warning]',
  error: ' [ERROR] '
}

// les loggers que l’on crée
const loggers = {}
// leurs writeStreams
const streams = {}
// on met leurs logLevel dans une variable privée ici, pour obliger l’appelant à passer par setLogLevel
const loggersLevel = {}

const getLogLevel = (logName) => {
  const level = loggersLevel[logName]
  if (level) {
    // on doit retourner la string (la clé, 1er elt de chaque entry)
    return Object.entries(levels).filter(([k, v]) => v === level)[0]
  }
}

const setLogLevel = (logName, level) => {
  if (!loggers[logName]) throw Error(`Le logger ${logName} n’a pas été créé`)
  if (levels[level]) loggersLevel[logName] = levels[level]
  else console.error(`${level} n’est pas un logLevel valide (${Object.keys(levels).join('|')})`)
}

/**
 * Retourne une writeStream sur le fichier passé en arguments (qui sera ouvert dans le dossier de log défini dans la conf)
 * @private
 * @param {string} logName
 * @returns {stream.Writable}
 */
const getLogStream = (logName) => {
  if (!/^[-a-zA-Z0-9_.]+$/.test(logName)) throw Error(`Nom de log invalide : ${logName}`)
  if (!fs.existsSync(logDir)) fs.mkdirSync(logDir)
  const options = { flags: 'a', mode: '0644' }
  return fs.createWriteStream(getFile(logName), options)
}

/**
 * Retourne le path complet du fichier
 * @property
 * @param {string} logName
 * @return {string}
 */
const getFile = (logName) => resolve(logDir, `${logName}.log`)

/**
 * Vide le log (ferme le logStream et le rouvre si y’en avait un ouvert)
 * @private
 * @param logName
 */
const reset = (logName) => {
  const isStreamOpen = Boolean(streams[logName])
  // on veut mettre de coté le log courant dans un .bak
  // on fait un rename et un nouvel open, si y’avait un stream ouvert
  // ça devrait fonctionner quand même (le stream est ouvert sur l’inode,
  // qui ne change pas avec le renommage),
  // pas besoin d’attendre l’event finish apres .end()
  const logFile = getFile(logName)
  const bakFile = logFile + '.bak'
  if (isStreamOpen) streams[logName].end()
  if (fs.existsSync(logFile)) {
    // on regarde sa taille, s’il est à 0 on tourne pas
    if (fs.lstatSync(logFile).size) {
      if (fs.existsSync(bakFile)) fs.unlinkSync(bakFile)
      fs.renameSync(logFile, bakFile)
    }
  }
  if (isStreamOpen) streams[logName] = getLogStream(logName)
}

/**
 * Écrit dans le log en préfixant par la date et le level
 * @private
 * @param {string} logName
 * @param {string} level
 * @param {string} message
 * @param {...*} [args]
 */
function log (logName, level, message) {
  // si c’est un logLevel trop bas on laisse tomber
  if (levels[level] < loggersLevel[level]) return
  let output = `[${formatDateTime()}]${prefixes[level]} `
  // on prend tous les arguments à partir de message
  Array.prototype.slice.call(arguments, 2).forEach(arg => {
    output += (typeof arg === 'object' ? JSON.stringify(arg, null, 2) : arg) + '\n'
  })
  // on ne crée la stream qu'à la 1re écriture
  if (!streams[logName]) streams[logName] = getLogStream(logName)
  streams[logName].write(output)
}

/**
 * Un logger
 * @typedef logger
 * @type {object}
 * @property {function} log
 * @property {function} debug
 * @property {function} warn
 * @property {function} error
 * @property {function} getLogLevel
 * @property {function} setLogLevel
 */

/**
 * Retourne un logger avec
 * 1) ces 4 "méthodes" qui écrivent dans le log si le logLevel est suffisant
 *   debug
 *   log
 *   warn
 *   error
 *  Chacune prend autant d’argument que l’on veut, les objets seront sérialisés en json
 *
 * 2) Ces méthodes pour lire ou changer le logLevel courant du logger
 *   getLogLevel
 *   setLogLevel
 *
 * Exemple :
 * ```
 * const logger = getLogger('sections') // va créer un log/sections.log (seulement si on écrit dedans ensuite)
 * logger.log('on récupère', unGrosObjet)
 * logger.warn('Attention y’a eu tel pb')
 * logger.debug('bla bla') // ne fait rien
 * logger.setLogLevel(logger.levels.debug)
 * logger.debug('bla bla') // écrit
 * @param {string} logName pour écrire dans log/{logName}.log
 * @param {object} [options]
 * @param {boolean} [options.reset] Passer true pour vider le log avant d'écrire dedans
 * @param {string} [options.level=notice] Le level du logger qui sera retourné (peut être modifié ensuite avec sa méthode setLogLevel)
 * @return {logger}
 * ```
 */
export default function getLogger (logName, options = {}) {
  const level = options.level || 'notice'
  if (options.reset) reset(logName)
  // si on l’a déjà créé on le retourne
  if (loggers[logName]) return loggers[logName]
  const logger = {
    // on utilise bind pour gérer le nb variable d’argument à partir du 3e
    debug: log.bind(null, logName, 'debug'),
    log: log.bind(null, logName, 'notice'),
    warn: log.bind(null, logName, 'warning'),
    error: log.bind(null, logName, 'error'),
    // accesseurs logLevel
    getLogLevel: () => getLogLevel(logName),
    setLogLevel: (level) => setLogLevel(logName, level)
  }
  loggers[logName] = logger
  setLogLevel(logName, level)
  return logger
}
