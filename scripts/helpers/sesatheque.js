// utilisé par scripts/refreshUsage.js

import queryString from 'query-string'

// on ne peut pas importer ça depuis src/lib/core/bibli dans un script node, on duplique…
export const biblis = {
  sesabibli: 'https://bibliotheque.sesamath.net/',
  sesacommun: 'https://commun.sesamath.net/',
  sesabidev: 'https://bibliotheque.sesamath.dev/',
  sesacomdev: 'https://commun.sesamath.dev/',
  bibliloca: 'https://bibliotheque.sesamath.local/',
  communloca: 'https://commun.sesamath.local/'
}

/**
 * Retourne la baseUrl de cette sésathèque (avec slash de fin)
 * @param {string} baseId
 * @returns {string}
 * @throws {Error} si baseId est une base inconnue
 */
export function getBibliBaseUrl (baseId) {
  if (!biblis[baseId]) throw Error(`Sésathèque ${baseId} inconnue`)
  return biblis[baseId]
}

// si on est dans un browser on prend son fetch
async function getFetch () {
  if (global.fetch) return global.fetch
  return import('node-fetch')
}

/**
 * Récupère le graphe d’une ressource j3p
 * @param {string} rid
 * @return {Promise<GrapheV1, Error>}
 */
export async function fetchGraphe (rid) {
  const { parametres: { g: graphe } } = await fetchRessource(rid)
  return graphe
}

/**
 * @typedef RessRidTitTyp
 * @property {string} rid
 * @property {string} titre
 * @property {string} type
 */
/**
 * Remplit les array enfants et warnings à partir des enfants de la ressource arbre (récursif)
 * @param {Object} args
 * @param {Ressource} args.arbre
 * @param {RessRidTitTyp[]} args.enfants
 * @param {string[]} args.warnings
 * @param {string} [args.wantedBaseId] si fourni ça ajoute un warning en cas d'enfant qui n'aurait pas cette baseId
 * @returns {Promise<void>}
 */
export async function fillEnfants ({ arbre, enfants, warnings, wantedBaseId }) {
  const treeLabel = `${arbre?.oid ?? ''} ${arbre?.titre}`
  if (arbre.type !== 'arbre') {
    warnings.push(`Ressource ${treeLabel} n’est pas un arbre`)
    return
  }
  if (!Array.isArray(arbre.enfants) || !arbre.enfants.length) {
    warnings.push(`${treeLabel} est un arbre vide`)
    return
  }
  for (const enfant of arbre.enfants) {
    if (enfant.type === 'arbre') {
      if (enfant.aliasOf) {
        const arbre2 = await fetchRessource(enfant.aliasOf)
        await fillEnfants({ arbre: arbre2, enfants, warnings, wantedBaseId })
        continue
      }
      if (Array.isArray(enfant.enfants) && enfant.enfants.length) {
        await fillEnfants({ arbre: enfant, enfants, warnings, wantedBaseId })
      } else {
        const warn = enfant.titre
          ? `Le dossier « ${enfant.titre} » est vide`
          : `${treeLabel} contient un dossier sans titre et vide`
        warnings.push(warn)
      }
    } else {
      const { aliasOf: rid, titre, type } = enfant
      if (wantedBaseId && rid) {
        const [baseId] = rid.split('/')
        if (baseId !== wantedBaseId) {
          warnings.push(`L’arbre ${treeLabel} a un enfant qui n’est pas sur ${wantedBaseId} : ${rid} (${titre})`)
        }
      }
      enfants.push({ rid, titre, type })
    }
  }
}

/**
 * Récupère une ressource
 * @param {string} rid
 * @param {string} [type] Si précisé, on throw si la ressource récupérée n'est pas de ce type
 * @return {Promise<Ressource>}
 */
export async function fetchRessource (rid, type) {
  const [baseId, id] = rid.split('/')
  const baseUrl = getBibliBaseUrl(baseId)
  const url = `${baseUrl}api/public/${id}`
  const fetch = await getFetch()
  const response = await fetch(url)
  if (!response.ok) {
    const error = Error(`HTTP error ${response.status} ${response.statusText}`)
    error.status = response.status
    throw error
  }
  const { data: ressource } = await response.json()
  if (type && ressource.type !== type) throw Error(`La ressource ${rid} n’est pas une ressource de type ${type} (type ${ressource.type})`)
  return ressource
}

/**
 * Retourne une liste de ressources publiques sur la sésathèque indiquée
 * @param {string} baseId
 * @param {Object} searchQuery cf doc query-string
 * @param {string|string[]} [searchQuery.type]
 * @param {number} [searchQuery.limit=25] Le nb d’objet à récupérer
 * @param {number} [searchQuery.skip=0] À indiquer pour récupérer les suivants
 * @return {Promise<{ query: Object, queryOptions: Object, total: number, liste: Object[]}>}
 */
export async function fetchList (baseId, searchQuery) {
  const baseUrl = getBibliBaseUrl(baseId)
  const url = `${baseUrl}api/liste?${queryString.stringify(searchQuery)}`
  const fetch = await getFetch()
  const response = await fetch(url)
  if (!response.ok) throw Error(`HTTP error ${response.status} ${response.statusText}`)
  const { data } = await response.json()
  return data
}
