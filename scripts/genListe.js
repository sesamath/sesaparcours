// script pour générer la 1re sortie de https://pad.sesamath.net/p/testParcours
const sectionsUsage = require('../docroot/sections.usage')
Object.entries(sectionsUsage.sectionsFirst).sort().forEach(([sectionName, rids]) => {
  console.info(`\t* ${sectionName} https://j3p.sesamath.dev/?rid=${rids[0]}`)
})
// on check que chaque section existante en prod a au moins un rid ou elle est en premier dans le graphe
const sectionsListedAsFirst = new Set(Object.keys(sectionsUsage.sectionsFirst))
const sectionsMissing = Object.keys(sectionsUsage.sectionsThen).filter(sectionName => !sectionsListedAsFirst.has(sectionName))
if (sectionsMissing.length) {
  console.error('Ces sections ne seront pas testées car aucune ressource ne l’utilise en premier dans son graphe', sectionsMissing)
}
