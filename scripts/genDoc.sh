#!/bin/sh
# le plugin "jsdoc-plugin-typescript" marche pas du tout comme attendu, cf commit af539ac1

# on va donc générer les js correspondants aux ts, lancer la doc puis effacer les js générés
set -u

clean () {
  rm -f tsconfig.jsdoc.json
  for f in $(find src/lib -name '*.ts'); do
    rm -f "$(dirname $f)/$(basename $f .ts).js"
  done
}

sed -re '/^ *\/.*/ d; /outDir/d; /removeComments/d; /sourceMap/d' < tsconfig.json > tsconfig.jsdoc.json \
  && ./node_modules/.bin/tsc -p tsconfig.jsdoc.json \
  && pnpm run doc

clean
