// prévu pour tourner avec node, on ne met pas de shebang pour la compatibilité windows

/**
 * Récupère tous les rids connus et vérifie que les graphe V1 sont corrects
 * Doit être exécuté avec
 *   `ts-node --esm scripts/checkGraphV1.ts`
 * ou
 *   `node --loader ts-node/esm scripts/checkGraphV1.ts`
 * On peut ajouter les options `--offset xx` ou `--limit yy` pour ne pas parcourir toutes les ressources de https://j3p.sesamath.dev/sections.usage.json
 * ou `--rids sesabibli/xxx,sesacomdev/yyy`,… pour préciser une liste de rids particuliers
 * ou `--limitBibli sesabibli` pour se limiter aux rids de sesabibli
 * @description
 */

import minimist from 'minimist'
import { access, mkdir, open, readFile, stat, writeFile } from 'node:fs/promises'
import { basename, dirname, resolve } from 'node:path'
import { fileURLToPath } from 'node:url'

import { loadBibli } from 'src/lib/core/bibli'
import { checkRessource } from 'src/lib/core/checks'

import type { PlainObject, RessourceJ3pBibli } from 'src/lib/types'
import { getDelayMs } from 'src/lib/utils/date'

interface NodeError extends Error {
  code?: string
}

const filename = fileURLToPath(import.meta.url)
const rootDir = resolve(dirname(filename), '..')
const docroot = resolve(rootDir, 'docroot')
const ressDir = resolve(docroot, 'tmp/ressources')
const logDir = resolve(rootDir, 'log')
const logFile = resolve(logDir, basename(filename, '.ts') + '.log')

const args = process.argv.slice(2)
const options = minimist(args)
const { offset = 0, limit = 0 } = options
if (!Number.isInteger(offset)) throw Error(`options --offset invalide : ${String(offset)} (${typeof offset})`)
if (!Number.isInteger(limit)) throw Error(`options --limit invalide : ${String(limit)} (${typeof limit})`)

// on garde en cache localement les ressources 7j
const maxAgeMs = 7 * 24 * 3600_000

const sectionUsageName = 'sections.usage.json'
const sectionUsageFile = resolve(docroot, sectionUsageName)
// on le prend en dev (construit le dimanche matin, mais déployé en prod avec le déploiement qui suivra, pas forcément tout de suite)
const sectionUsageUrl = `https://j3p.sesamath.dev/${sectionUsageName}`

try {
  await stat(logDir)
} catch (error) {
  if ((error as NodeError)?.code === 'ENOENT') {
    await mkdir(logDir, { mode: 0o755 })
  } else {
    throw error
  }
}

const logFileHandle = await open(logFile, 'a')

async function output (message: string): Promise<void> {
  await writeFile(logFileHandle, message + '\n', { encoding: 'utf-8', mode: 0o644 })
  // eslint-disable-next-line no-console
  console.log(message)
}

async function fetchSectionUsage (): Promise<PlainObject> {
  const response = await fetch(sectionUsageUrl)
  if (!response.ok) throw Error(`HTTP error ${response.status} ${response.statusText} on ${sectionUsageUrl}`)
  const json = await response.json()
  await writeFile(sectionUsageFile, JSON.stringify(json, null, 2), { encoding: 'utf-8', mode: 0o644 })
  await output(`${sectionUsageUrl} récupéré et mis dans ${sectionUsageFile}`)
  return json
}

async function fetchAllRids ({ offset = 0, limit = 0, limitBibli = null } = {}): Promise<string[]> {
  let json
  try {
    const fstat = await stat(sectionUsageFile)
    if (getDelayMs(fstat.ctimeMs) > maxAgeMs) json = await fetchSectionUsage()
    else json = JSON.parse(await readFile(sectionUsageFile, { encoding: 'utf-8' }))
  } catch (error) {
    if ((error as NodeError)?.code === 'ENOENT') {
      json = await fetchSectionUsage()
    }
  }
  const { ressources } = json
  if (ressources == null || typeof ressources !== 'object') throw Error(`${sectionUsageFile} n’est pas au format attendu (pas de propriété ressources)`)
  let rids = Object.keys(ressources)
  if (limitBibli != null) rids = rids.filter((rid) => rid.split('/')[0] === limitBibli)
  if (offset > 0) rids.splice(0, offset)
  if (limit > 0) rids.splice(limit)
  return rids
}

async function fetchOne (rid: string): Promise<RessourceJ3pBibli> {
  // faut aller chercher la ressource sur sa bibli
  const { ressource } = await loadBibli(rid)
  // on l’enregistre en local
  const file = resolve(ressDir, rid)
  try {
    await writeFile(file, JSON.stringify(ressource), { encoding: 'utf-8', mode: 0o644 })
  } catch (error) {
    if ((error as NodeError)?.code === 'ENOENT') {
      const dir = dirname(file)
      await mkdir(dir, { recursive: true, mode: 0o755 })
      await writeFile(file, JSON.stringify(ressource), { encoding: 'utf-8', mode: 0o644 })
    } else {
      throw error
    }
  }
  // et on la retourne
  return ressource
}

/**
 * Regarde si on trouve la ressource dans /docroot/tmp/ressources, si oui on la prend, sinon on va la chercher sur la bibli (et elle sera mise en cache à cet endroit)
 * @param rid
 */
async function loadOne (rid: string): Promise<RessourceJ3pBibli> {
  try {
    const file = resolve(ressDir, rid)
    const fstat = await stat(file)
    if (getDelayMs(fstat.ctimeMs) > maxAgeMs) return await fetchOne(rid)
    const json = await readFile(file, { encoding: 'utf-8' })
    return JSON.parse(json)
  } catch (error) {
    if ((error as NodeError)?.code === 'ENOENT') {
      return await fetchOne(rid)
    }
    // pb de json.parse ?
    throw error
  }
}

// on crée docroot/tmp/ressources s’il n’existe pas
try {
  await access(ressDir)
} catch (error) {
  if ((error as NodeError)?.code === 'ENOENT') {
    await mkdir(ressDir, { recursive: true, mode: 0o755 })
  }
}
// on init les rids
let rids: string[]
if (options.rids != null) {
  rids = options.rids.split(',').map((rid: string) => rid.trim())
  await output(`début du script ${filename} avec la vérif des ${rids.length} rids passés en argument`)
} else {
  // on restreint à une bibli si c’est demandé avec l’option --limitBibli
  rids = await fetchAllRids({ offset, limit, limitBibli: options.limitBibli })
  await output(`début du script ${filename} avec la vérif de ${rids.length} ressources (offset ${String(offset)} et limit ${String(limit)})`)
}

const errorsCumul: string[] = []
const warningsCumul: string[] = []
// on boucle dessus
for (const rid of rids) {
  const ressource = await loadOne(rid)
  const { errors, warnings } = await checkRessource(ressource)
  let message = rid
  if (errors.length > 0) {
    message += ` KO :\n${errors.join('\n')}`
  }
  if (warnings.length > 0) {
    message += (errors.length > 0) ? '\net ' : ' mais avec '
    message += `les warnings : ${warnings.join(', ')}`
  }
  if (errors.length > 0) errorsCumul.push(message)
  else if (warnings.length > 0) warnings.push(message)
  await output(message)
}

// récapitulatif
let message = `${rids.length} ressources passées au constructeur GraphV1`
message += (warningsCumul.length > 0) ? `\navec ${warningsCumul.length} warnings :\n${warningsCumul.join('\n')}` : ', sans warning'
message += (errorsCumul.length > 0) ? `\navec ${errorsCumul.length} erreur${errorsCumul.length > 1 ? 's' : ''} :\n${errorsCumul.join('\n')}` : ', sans erreur'
await output(`\n\n${message}\n\nfin du script ${filename}`)
