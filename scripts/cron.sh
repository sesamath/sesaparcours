#!/usr/bin/env sh

set -u

dirIni="$(pwd)"
rootDir="$(cd $(dirname $0); cd ..; pwd)"
scriptsDir="$rootDir/scripts"

fin() {
  cd "$dirIni" || return 1
}
trap fin EXIT

# il faut lancer ça depuis la racine sinon il ne trouve pas le loader
cd "$rootDir"

# il faut du NODE_NO_WARNINGS=1 pour ne pas avoir sur stderr les warnings
# (node:nnn) ExperimentalWarning: Custom ESM Loaders is an experimental feature and might change at any time
# (Use `node --trace-warnings ...` to show where the warning was created)
NODE_NO_WARNINGS=1 node --loader ts-node/esm "$scriptsDir/refreshUsage.js" -q
NODE_NO_WARNINGS=1 node --loader ts-node/esm "$scriptsDir/checkNiveaux.js" -q

# et on recopie dans docroot ces json
rsync -a "$rootDir"/public/*.json "$rootDir/docroot/"
