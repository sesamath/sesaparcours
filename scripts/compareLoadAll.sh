#!/bin/bash

abort () {
  echo "${1-ABANDON}" >&2
  exit 1
}

usage () {
  echo "Usage $0"
  echo "Compare un loadAll.log avec un autre"
  echo "  -h : affiche cette aide"
  echo "  -k : pour conserver les /tmp/loadAll.* en fin de script"
  echo "  -n : url ou chemin local du loadAll.log à comparer (https://j3p.sesamath.dev/build/loadAll.log par défaut)"
  echo "  -o : url ou chemin local du loadAll.old.log à prendre comme référence pour la comparaison (https://j3p.sesamath.dev/build/loadAll.old.log par défaut)"
  echo
  exit ${1-1}
}

KEEP=no
while getopts "hkn:o:" OPTION; do
  case $OPTION in
    h) usage 0;;
    k) KEEP=yes;;
    n) new="$OPTARG";;
    o) old="$OPTARG";;
  esac
done

[ -z "$new" ] && new=https://j3p.sesamath.dev/build/loadAll.log
[[ "$new" =~ ^http ]] && wget -q -O /tmp/loadAll.log "$new" && new=/tmp/loadAll.log
[ -f "$new" ] || abort "$new n’est pas un fichier"
[ -z "$old" ] && old=https://j3p.sesamath.dev/build/loadAll.old.log
[[ "$old" =~ ^http ]] && wget -q -O /tmp/loadAll.old.log "$old" && old=/tmp/loadAll.old.log
[ -f "$old" ] || abort "$old n’est pas un fichier"

srcNew=/tmp/loadAll.list
srcOld=/tmp/loadAll.old.list
# on extrait les infos intéressantes
sed -nre 's@^.*(sesa[a-z]+/[^ ]+ [oOkK]{2}[^(]+).*@\1@p' "$new"|sort > "$srcNew"
sed -nre 's@^.*(sesa[a-z]+/[^ ]+ [oOkK]{2}[^(]+).*@\1@p' "$old"|sort > "$srcOld"

pire=/tmp/loadAll.pire
mieux=/tmp/loadAll.mieux
:>$pire
:>$mieux

while read rid res rest; do
  if [ "$res" == "ok" ]; then
    lineOld="$(grep "$rid KO" "$srcOld")"
    [ -n "$lineOld" ] && echo "$rid n’a plus d’erreurs ($lineOld => $rid $res $rest)" >> "$mieux"
    continue
  fi
  nbErr=$(echo "$rest"|sed -nre 's/.* ([0-9]+) appels? de console.error.*/\1/p')
  # grep |read affecte les var dans le sous shell du pipe, faut mettre la suite du code dans le pipe, d’où le while
  grep "$rid" "$srcOld"|while read ridOld resOld restOld; do
    nbErrOld=$(echo "$restOld"|sed -nre 's/.* ([0-9]+) appels? de console.error.*/\1/p')
    if [ -z "$nbErr" ]; then
      echo "Pb dans le script $0, KO sans erreurs trouvée dans : $rest">&2
    elif [ -z "$nbErrOld" ] || [ "$nbErrOld" -lt "$nbErr" ]; then
      if [ -z "$resOld" ]; then
        echo "$rid a des erreurs et n’était pas dans le log précédent : $res $rest" >> "$pire"
      else
        echo -e "$rid a plus d’erreurs qu'avant\n  $resOld $restOld\n    est devenu\n  $res $rest#\n    avec le log complet :" >> "$pire"
        # faut échapper le slash
        # on ajoute le head au cas où le pattern de fin choperait rien
        sed -nre "/${rid//\//\\/}/,/${rid//\//\\/} KO/ p" "$new"|head -50 >> "$pire"
        echo >> "$pire"
      fi
    # else c'est KO et ça l’était déjà avec au moins autant d’erreurs, on laisse tomber
    fi
  done
done < "$srcNew"

[ -s "$pire" ] && echo -e "\\n\\nIl y a des aggravations depuis la dernière fois" && cat "$pire"
[ -s "$mieux" ] && echo -e "\\n\\nIl y a des améliorations depuis la dernière fois" && cat "$mieux"

[ "$KEEP" == 'no' ] && rm -f /tmp/loadAll.*
