#!/bin/sh

# vu que le build en lib ne fonctionne pas comme attendu, et que l’on ne peut pas utiliser le plugin legacy
# (consomme bcp trop de RAM, et très galère en usage cross-domain)
# on copie src/loader.js dans docroot/build/

ROOT_DIR="$(realpath "$(dirname "$0")/../")"

DOCROOT="$ROOT_DIR/docroot"
BUILD_DIR="$DOCROOT/build"

LOADER="$ROOT_DIR/src/loader.js"

# on peut passer une baseUrl en option
j3pBaseUrl=${1-}
if [ -z "$j3pBaseUrl" ]; then
  # avec du sh posix on peut pas utiliser [[ "$(hostname)" =~ dev ]] ni les substitutions ${var/str/}
  [ "$(hostname|sed -e 's/dev//')" = "$(hostname)" ] && j3pBaseUrl=https://j3p.sesamath.net/ || j3pBaseUrl=https://j3p.sesamath.dev/
fi

[ ! -d "$DOCROOT" ] && echo "$DOCROOT n’existe pas, abandon" >&2 && exit 1
[ ! -f "$LOADER" ] && echo "$LOADER n’existe pas">&2 && exit 1
[ ! -d "$BUILD_DIR" ] && echo "$BUILD_DIR n’existe pas (on doit être lancé juste après un build)">&2 && exit 1

version="$(sed -nre 's/^\s*"version"\s*:\s*"([^"]+)".*/\1/p' package.json)"
lazyLoader="$(find "$BUILD_DIR" -name "lazyLoader-*.js"|head -1)"
[ -z "$lazyLoader" ] && echo "Pas trouvé de $BUILD_DIR/lazyLoader-*.js">&2 && exit 1
lazyLoaderUrl="${j3pBaseUrl}build/$(basename "$lazyLoader")"
# marche pas si on est pas dans un module…
# s@loaderUrl = ''@loaderUrl = import.meta.url.replace("'/\\/[^/]*$/'", '') + '$lazyLoader'@;
if sed -re '
      /^\s*\/\*[*\s]*$/, /\*\/\s*$/ d;
      /^\s*\/\/.*/ d;
      /^\s*$/ d;'"
      s@loaderUrl = ''@loaderUrl = '$lazyLoaderUrl'@;
      s@window.j3pVersion = ''@window.j3pVersion = '$version'@;
    " < "$LOADER" > "$BUILD_DIR/loader.js"; then

  # on récupère le build.prod si y'en avait un
  # (utile pour garder en préprod le dernier build déployé en prod, la rotation est faite par le déploiement pré-prod => prod)
  if [ -d "$ROOT_DIR/build.prod" ]; then
    mv "$ROOT_DIR/build.prod" "$DOCROOT/"
  fi

  echo "$LOADER mis dans $BUILD_DIR pour usage en crossdomain (avec $lazyLoaderUrl)"

  exit 0
fi

echo "KO : quelque chose s'est mal passé (cf ci-dessus)">&2
exit 1
