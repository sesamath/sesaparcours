Architecture du code
====================

lib/loader
----------

Expose les fonctions globales de chargement
- j3pLoad (player v1 pour graphes v1)
- editGraphe (éditeur de graphe v1)
- showParcours (afficheur de parcours v1)
- spEdit (editeur v2 de graphes v1|v2)
- spPlay (player v2 de graphes v1|v2)
- spView (afficheur de parcours v2, pour des résultats v1|v2)
- spStart (gère la home de j3p.sesamath.net pour afficher le formulaire et utiliser player/graphEditor/showPathway)

Ensuite, suivant celle qui sera appelé ça chargera ce qu'il faut et l’utilisera

lib/player
----------

Remplace l’ancien j3pLoad, qui va lancer une section.

Il prend un graphe (le passe au format v2 si c'est pas le cas), éventuellement un lastResultat (passé aussi au format v2 si besoin), et détermine par où commencer (si y’a pas de lastResultat c'est startingId) pour charger la section et la lancer.

La section va alors utiliser des fonctions mises à sa disposition pour afficher l’énoncé, afficher du feedback, des indications.

section
-------

Cf le type Section dans src/lib/types.d.ts
