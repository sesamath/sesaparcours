Spelling
========

Voici les deux fichiers (pris sur https://github.com/LibreOffice/dictionaries/blob/master/fr_FR/) pour ajouter la langue française au spelling de votre IDE

Webstorm
--------

Cf https://www.jetbrains.com/help/idea/spellchecking.html#dictionaries
