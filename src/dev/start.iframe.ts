import type { Version } from 'src/dev/start.runners'
import { isLegacyGraph } from 'src/lib/core/checks'
import { convertGraph } from 'src/lib/core/convert1to2'
import { convertGraphBack } from 'src/lib/core/convert2To1'
import Graph, { type GraphJson } from 'src/lib/entities/Graph'
import type { LoaderName, LegacyGraph, LegacyResultat, ressourceJ3pParametresGetter, ressourceJ3pParametresGetterV1, Resultat } from 'src/lib/types'
import { addElement, empty, showError } from 'src/lib/utils/dom/main'

// pour déclarer nos 6 fcts en global
import 'src/lazyLoader'

/**
 * Ce code est exécuté dans iframe.html (l’iframe qui affiche play|edit|show en v1 ou v2)
 * @fileOverview
 */

// permet de choper des erreurs jsPlumb
window.addEventListener('unhandledrejection', (error) => {
  console.error('Unhandled rejection', error)
})

async function autostart () {
  window.addEventListener('message', (event) => {
    if (event.data?.action == null) return // pas pour nous
    const { action, version, arg, isDebug, mtgUrl } = event.data
    if (mtgUrl) window.mtgUrl = mtgUrl
    else delete window.mtgUrl
    if (action === 'ping') {
      if (isDebug) console.debug('iframe reçoit un ping')
      if (window.top == null) return showError(Error('Ce script doit être inclus dans une iframe'))
      return window.top.postMessage({ action: 'pong' })
    }
    let method: LoaderName
    switch (action) {
      case 'edit':
        method = version === '1' ? 'editGraphe' : 'spEdit'
        break
      case 'play':
        method = version === '1' ? 'j3pLoad' : 'spPlay'
        break
      case 'show':
        method = version === '1' ? 'showParcours' : 'spView'
        break
      default:
        // sinon rien, c'était pas pour nous, mathlive aussi fait du postMessage avec action
        return
    }
    // console.debug('iframe reçoit', event.data)
    try {
      if (typeof window[method] !== 'function') throw Error(`Le chargement a échoué (pas de fonction ${method})`)
      if (isDebug) console.debug(`appel de ${method} dans l’iframe avec`, arg)
      const globalMethod: Function = window[method] as unknown as Function
      let result
      empty(document.body)
      if (isDebug) {
        // on ajoute du contenu bidon pour que le conteneur ne soit pas tout seul dans son dom
        addElement(document.body, 'div', {
          textContent: 'En mode debug on ajoute un peu de contenu pour voir ce que ça donne quand le conteneur sesaparcours n’est pas tout seul dans sa page (qui est ici dans une iframe délimitée par le liseret noir).',
          style: { margin: '50px' }
        })
      }
      const id = method === 'editGraphe' ? 'edgRoot' : 'main'
      const main = addElement(document.body, 'div', { id })
      arg.isDebug = isDebug
      if (isDebug) {
        addElement(document.body, 'div', {
          textContent: 'Avec encore un peu de contenu après le conteneur sesaparcours.',
          style: { margin: '50px' }
        })
      }
      if (['editGraphe', 'spEdit'].includes(method)) {
        // faut gérer la callback, mais on ne peut pas passer de fonction via postMessage
        // on ajoute les boutons dans l’éditeur (donc l’iframe) et on passera l’objet à la fenêtre parente via postMessage
        result = globalMethod(main, arg, (error: Error, getter: ressourceJ3pParametresGetter) => {
          if (error != null) return showError(error)
          addFooterButtons(getter)
        })
      } else {
        if (method === 'j3pLoad') {
          // on a besoin de resultatCallback (pour remonter le résultat au parent et activer l’onglet show)
          arg.resultatCallback = (resultat: LegacyResultat) => window.top?.postMessage({ action: 'setResultat', resultat })
          // arg.loadCallback = (error, parcours) => { console.debug('j3pLoad chargé', error, parcours) }
        } else if (method === 'spPlay') {
          // on a besoin de resultatCallback (pour remonter le résultat au parent et activer l’onglet show)
          arg.resultatCallback = (resultat: Resultat) => window.top?.postMessage({ action: 'setResultat', resultat })
        }
        result = globalMethod(main, arg)
      }
      if (result instanceof Promise) {
        result.catch(showError)
      }
    } catch (error) {
      showError(error)
    }
  })
}

function addFooterButtons (getGraph: ressourceJ3pParametresGetter | ressourceJ3pParametresGetterV1) {
  const footer = document.getElementById('edgFooter') ?? document.querySelector('.editorFooter')
  if (footer == null) return console.error(Error('Pas de #edgFooter dans le document courant'))
  const divBtns = addElement(footer, 'div', {
    style: {
      margin: '3px auto',
      display: 'flex',
      width: '100%',
      justifyContent: 'space-evenly'
    }
  })
  const addButton = (textContent: string) => addElement(divBtns, 'button', { textContent, style: { flexGrow: 'auto', flexShrink: '1', flexBasis: 'fit-content' } })
  const getGraphOrG = (): GraphJson | LegacyGraph => {
    if (typeof getGraph !== 'function') throw Error('L’éditeur n’a pas retourné de fonction pour récupérer le graphe')
    const recup = getGraph()
    return 'graph' in recup ? recup.graph : recup.g
  }
  const postGraph = (version: Version) => {
    const graph = getGraphOrG()
    window.top?.postMessage({ action: 'play', version, graph })
  }
  addButton('Afficher le graphe en console')
    .addEventListener('click', async () => {
      let graph1: LegacyGraph
      let graph2: GraphJson
      const g = getGraphOrG()
      if (isLegacyGraph(g)) {
        graph1 = g
        graph2 = convertGraph(g).toJSON()
      } else {
        graph1 = (await convertGraphBack(new Graph(g))).g
        graph2 = g
      }
      console.info('Le graphe actuellement édité, au format v1', graph1, 'et v2', graph2)
    })
  addButton('Lancer ce graphe (v1)')
    .addEventListener('click', postGraph.bind(null, '1'))
  addButton('Lancer ce graphe (v2)')
    .addEventListener('click', postGraph.bind(null, '2'))
}

autostart().catch(showError)
