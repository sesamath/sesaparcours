// noinspection JSIgnoredPromiseFromCall,ES6MissingAwait

// ligne précédente parce qu’on fait exprès des appels de addTruc
// dont on ne veut pas attendre la résolution de la promesse (affichage du point)
// avant de passer à la création de l’élément suivant

import { getDelaySec } from 'src/lib/utils/date'

/**
 * Ce code js est chargé directement par buildMappemonde.html pour construire la figure mathgraph32 de la mappemonde
 * Il sert à générer le code base64 en utilisant l’api, ce code étant ensuite copié/collé dans la section (nettement plus rapide à charger)
 * Ouvrir mappemonde.html (à travers un serveur web, par ex dans webstorm avec un clic droit dessus puis run)
 * et attendre 1~2min pour avoir le résultat
 * @fileOverview
 */

// On ne peut pas importer de js qui font des imports absolus from 'src/…' => on s’en passe

// const radians = (h) => h * Math.PI / 180

let inc = 1
const newtag = () => 'T' + inc++

/**
 * @type {MtgAppLecteurApi}
 */
let mtgApp

// @todo virer stor
const stor = {}

// les couleurs
const colOcean = '#a6b8ff'
const colContinent = '#d3ca2f'
const colBordContinent = '#523703'
const colMain = '#ff0000'
const colText = '#000'
const colLong = '#0000ff'
const colLat = '#30b430'
const colLong2 = '#5776ec'
const colLat2 = '#6de76d'
// donner des noms plus parlant à ces couleurs
const col1 = '#fff'
const col2 = '#2a4d11'
const col3 = '#d91212'

/**
 * @typedef Ville
 * @property {string} nom
 * @property {number} lat
 * @property {string} ns hémisphère N|S
 * @property {number} long
 * @property {string} eo E|W
 * @property {number[]} int1 intervalle de valCurseurb (curseur Pôle Nord) dans lequel le point doit être affiché
 * @property {number[]} [int2] 2e intervalle éventuel de valCurseurb (curseur Pôle Nord) dans lequel le point doit être affiché
 */
/**
 * Les villes à afficher
 * @type {Ville[]}
 */
const listVille = [
  {
    nom: 'Paris',
    lat: 48,
    ns: 'N',
    long: 2,
    eo: 'E',
    int1: [90, 271]
  },
  {
    nom: 'Dallas',
    lat: 32,
    ns: 'N',
    long: 96,
    eo: 'W',
    int1: [186, 361]
  },
  {
    nom: 'Mexico',
    lat: 23,
    ns: 'N',
    long: 102,
    eo: 'W',
    int1: [-1, 20],
    int2: [190, 361]
  },
  {
    nom: 'Brasilia',
    lat: 14,
    ns: 'S',
    long: 51,
    eo: 'W',
    int1: [-1, 2],
    int2: [140, 321]
  },
  {
    nom: 'Rio',
    lat: 32,
    ns: 'S',
    long: 52,
    eo: 'W',
    int1: [143, 324],
    int2: [361, 361]
  },
  {
    nom: 'New Delhi',
    lat: 28,
    ns: 'N',
    long: 77,
    eo: 'E',
    int1: [13, 194],
    int2: [358, 361]
  },
  {
    nom: 'Prétoria',
    lat: 25,
    ns: 'S',
    long: 28,
    eo: 'E',
    int1: [60, 245],
    int2: [361, 361]
  },
  {
    nom: 'Bamako',
    lat: 12,
    ns: 'N',
    long: 8,
    eo: 'W',
    int1: [95, 281]
  },
  {
    nom: 'Bagdad',
    lat: 33,
    ns: 'N',
    long: 44,
    eo: 'E',
    int1: [45, 226]
  },
  {
    nom: 'Pékin',
    lat: 39,
    ns: 'N',
    long: 116,
    eo: 'E',
    int1: [-1, 155],
    int2: [333, 361]
  },
  {
    nom: 'Moscou',
    lat: 55,
    ns: 'N',
    long: 37,
    eo: 'E',
    int1: [50, 235],
    int2: [355, 361]
  },
  {
    nom: 'Sidney',
    lat: 32,
    ns: 'S',
    long: 140,
    eo: 'E',
    int1: [-1, 130],
    int2: [305, 361]
  }
]

// on peut pas utiliser le getMtgCore de sesaparcours ici (chargement de ce js directement dans le navigateur)
// => on charge le mtgLoad.min.js de prod dans le html (dans le <head> donc avant notre exécution)
// et on l’utilise directement ici
function getMtg () {
  return new Promise((resolve, reject) => {
    window.mtgLoad('', {}, { loadCoreOnly: true, loadApi: true, isPromiseMode: true }, function (error, mtgApp) {
      if (error) return reject(error)
      resolve(mtgApp)
    })
  })
}

async function build () {
  stor.acons = ['lacircleS', 'surfAfri', 'surfAm1', 'surfAm2', 'surfAm3', 'surfAm4', 'surfAm5', 'surfAm6', 'surfAm7', 'surfAm8', 'surfAm9', 'surfAm15']
  mtgApp = await getMtg()
  const start = Date.now()
  const svgId = 'mtgSvg'
  mtgApp.removeAllDoc()
  const wi = 400
  const lar = 400
  const marg = 50

  await mtgApp.addDoc(svgId, 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAAH#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj###############8=', true)
  mtgApp.setApiDoc(svgId)
  mtgApp.calculateFirstTime(svgId)

  // curseur
  mtgApp.addFreePoint({
    absCoord: true,
    x: marg,
    y: 15,
    name: 'C1',
    tag: 'C1',
    hidden: true,
    pointStyle: 'bigmult'
  })
  mtgApp.fixPoint({ a: 'C1' })
  mtgApp.addFreePoint({
    absCoord: true,
    x: wi - marg,
    y: 15,
    name: 'C2',
    tag: 'C2',
    hidden: true,
    pointStyle: 'bigmult'
  })
  mtgApp.fixPoint({ a: 'C2' })
  mtgApp.setUnity({ a: 'C1', b: 'C2' })
  mtgApp.addSegment({
    b: 'C1',
    a: 'C2',
    tag: 'segcurs',
    lineStyle: 'dashdash'
  })
  mtgApp.addLinkedPointLine({
    absCoord: true,
    d: 'segcurs',
    x: wi / 2 + 20,
    y: lar / 2,
    name: 'potCurs',
    tag: 'potCurs',
    color: 'white',
    hiddenName: true
  })
  mtgApp.addCircleOr({
    o: 'potCurs',
    r: 0.035,
    tag: 'peticurs',
    thickness: 3
  })
  mtgApp.addSurfaceCircle({
    c: 'peticurs',
    color: colText
  })
  mtgApp.addLengthMeasure({
    a: 'C1',
    b: 'potCurs'
  })
  mtgApp.addLengthMeasure({
    a: 'C1',
    b: 'C2'
  })
  mtgApp.addCalc({
    nameCalc: 'valCurseurb',
    formula: 'C1potCurs/C1C2*360'
  })
  mtgApp.addCalc({
    nameCalc: 'valCurseur',
    formula: 'si(valCurseurb=0,0.01,si(valCurseurb=-180,-179.9,si(valCurseurb=180,179.9,valCurseurb)))'
  })

  // potCurs C1potCurs  et C1C2
  mtgApp.addFreePoint({
    absCoord: true,
    x: 100,
    y: 1,
    name: 'PPP1',
    tag: 'PPP1',
    hidden: true,
    pointStyle: 'bigmult'
  })
  mtgApp.addFreePoint({
    absCoord: true,
    x: 200,
    y: 1,
    name: 'PPP2',
    tag: 'PPP2',
    hidden: true,
    pointStyle: 'bigmult'
  })
  mtgApp.addSegment({
    b: 'PPP1',
    a: 'PPP2',
    tag: 'PPPsegcurs',
    lineStyle: 'dashdash',
    hidden: true
  })
  mtgApp.addLinkedPointLine({
    absCoord: true,
    d: 'PPPsegcurs',
    x: 150,
    y: 1,
    name: 'ppppp',
    tag: 'ppppp',
    color: 'white',
    hiddenName: true,
    hidden: true
  })
  mtgApp.addLengthMeasure({
    a: 'PPP1',
    b: 'ppppp'
  })
  mtgApp.addLengthMeasure({
    a: 'PPP1',
    b: 'PPP2'
  })

  // curseur lattitude
  mtgApp.addFreePoint({
    absCoord: true,
    x: 2 * marg + 10,
    y: 380,
    name: 'L1',
    tag: 'L1',
    hidden: true,
    pointStyle: 'bigmult'
  })
  mtgApp.fixPoint({ a: 'L1' })
  mtgApp.addFreePoint({
    absCoord: true,
    x: wi - marg + 10,
    y: 380,
    name: 'L2',
    tag: 'L2',
    hidden: true,
    pointStyle: 'bigmult'
  })
  mtgApp.fixPoint({ a: 'L2' })
  mtgApp.addSegment({
    b: 'L1',
    a: 'L2',
    tag: 'segcursL',
    lineStyle: 'dashdash',
    color: colLat,
    thickness: 2
  })
  mtgApp.addFreePoint({
    absCoord: true,
    x: 300,
    y: lar / 2,
    name: 'POINTlat',
    tag: 'POINTlat',
    hidden: false,
    pointStyle: 'pixel',
    color: 'white',
    hiddenName: true
  })
  mtgApp.addLinePerp({
    d: 'segcursL',
    a: 'POINTlat',
    hidden: true,
    tag: 'PERPLat'
  })
  mtgApp.addIntLineLine({
    d: 'segcursL',
    d2: 'PERPLat',
    name: 'potCursL',
    tag: 'potCursL',
    color: 'white',
    hiddenName: true,
    hidden: true
  })
  mtgApp.addCircleOr({
    o: 'potCursL',
    r: 0.035,
    tag: 'peticursL',
    thickness: 3
  })
  mtgApp.addSurfaceCircle({
    c: 'peticursL',
    color: colLat,
    fillStyle: 'fill',
    tag: 'surface987'
  })
  mtgApp.addLengthMeasure({
    a: 'L1',
    b: 'potCursL'
  })
  mtgApp.addLengthMeasure({
    a: 'L1',
    b: 'L2'
  })
  mtgApp.addCalc({
    nameCalc: 'latiP',
    formula: '(L1potCursL/L1L2)*180-90'
  })
  mtgApp.addCalc({
    nameCalc: 'latiP2',
    formula: 'int(abs(latiP))'
  })

  // curseur longitude
  mtgApp.addFreePoint({
    absCoord: true,
    x: 2 * marg + 10,
    y: 350,
    name: 'Lo1',
    tag: 'Lo1',
    hidden: true,
    pointStyle: 'bigmult'
  })
  mtgApp.fixPoint({ a: 'Lo1' })
  mtgApp.addFreePoint({
    absCoord: true,
    x: wi - marg + 10,
    y: 350,
    name: 'Lo2',
    tag: 'Lo2',
    hidden: true,
    pointStyle: 'bigmult'
  })
  mtgApp.fixPoint({ a: 'Lo2' })
  mtgApp.addSegment({
    b: 'Lo1',
    a: 'Lo2',
    tag: 'segcursLo',
    lineStyle: 'dashdash',
    color: colLong,
    thickness: 2
  })
  mtgApp.addFreePoint({
    absCoord: true,
    x: 200,
    y: lar / 2,
    name: 'POINTlong',
    tag: 'POINTlong',
    hidden: false,
    pointStyle: 'pixel',
    color: 'white',
    hiddenName: true
  })
  mtgApp.addLinePerp({
    d: 'segcursLo',
    a: 'POINTlong',
    tag: 'PERPLong',
    hidden: true
  })
  mtgApp.addIntLineLine({
    d: 'segcursLo',
    d2: 'PERPLong',
    name: 'potCursLo',
    tag: 'potCursLo',
    color: 'white',
    hiddenName: true,
    hidden: true
  })
  mtgApp.addCircleOr({
    o: 'potCursLo',
    r: 0.035,
    tag: 'peticursLo',
    thickness: 3
  })
  mtgApp.addSurfaceCircle({
    c: 'peticursLo',
    color: colLong,
    fillStyle: 'fill',
    tag: 'surfacgfde987'
  })
  mtgApp.addLengthMeasure({
    a: 'Lo1',
    b: 'potCursLo'
  })
  mtgApp.addLengthMeasure({
    a: 'Lo1',
    b: 'Lo2'
  })
  mtgApp.addCalc({
    nameCalc: 'longiP',
    formula: '-((Lo1potCursLo/Lo1Lo2)*360-180)'
  })
  mtgApp.addCalc({
    nameCalc: 'longiP2',
    formula: 'int(abs(longiP))'
  })

  // sphere
  mtgApp.addFreePoint({
    absCoord: true,
    x: 700,
    y: 700,
    name: 'R',
    tag: 'R',
    hidden: false,
    pointStyle: 'bigmult'
  })
  mtgApp.addFreePoint({
    absCoord: true,
    x: wi / 2,
    y: (lar - marg) / 2,
    name: 'O',
    tag: 'O',
    hidden: false,
    pointStyle: 'pixel',
    color: 'white',
    hiddenName: true
  })
  mtgApp.addFreePoint({
    absCoord: true,
    x: wi / 2,
    y: (lar - marg) / 2,
    name: 'O2',
    tag: 'O2',
    hidden: false,
    pointStyle: 'pixel',
    color: 'white',
    hiddenName: true
  })
  mtgApp.addFreePoint({
    absCoord: true,
    x: wi / 2,
    y: marg,
    name: 'N',
    tag: 'N',
    pointStyle: 'bigmult',
    hiddenName: true
  })
  mtgApp.fixPoint({ a: 'N' })
  mtgApp.addFreePoint({
    absCoord: true,
    x: wi / 2,
    y: 300,
    name: 'S',
    tag: 'S',
    pointStyle: 'bigmult',
    hiddenName: true
  })
  mtgApp.fixPoint({ a: 'S' })
  mtgApp.addCircleOA({
    o: 'O',
    a: 'N',
    tag: 'lecircle'
  })
  mtgApp.addLengthMeasure({
    a: 'O',
    b: 'N'
  })
  // repere
  mtgApp.addFreePoint({
    absCoord: true,
    x: wi - marg * 1.5,
    y: (lar - marg) / 2,
    name: 'I',
    tag: 'I',
    hidden: true,
    pointStyle: 'bigmult'
  })
  mtgApp.addFreePoint({
    absCoord: true,
    x: marg * 1.5,
    y: (lar - marg) / 2,
    name: 'I7',
    tag: 'I7',
    hidden: true,
    pointStyle: 'bigmult'
  })
  mtgApp.addLengthMeasure({
    a: 'O',
    b: 'I'
  })
  mtgApp.addLengthMeasure({
    a: 'O',
    b: 'N'
  })
  mtgApp.addSegment({
    b: 'O',
    a: 'I',
    tag: 'axeab1',
    color: 'red',
    hidden: true
  })
  mtgApp.addSegment({
    b: 'O',
    a: 'N',
    tag: 'axeord1',
    color: 'red',
    hidden: true
  })
  // repoer o i n
  mtgApp.addSystemOfAxis({
    o: 'O',
    a: 'I',
    b: 'N',
    hidden: false,
    tag: 'Repere2'
  })

  // place I' et J'
  /*
     mtgApp.addCalc({
      nameCalc: 'abI',
      formula: 'cos(valCurseur)'
    })
     mtgApp.addCalc({
      nameCalc: 'ordI',
      formula: 'sin(valCurseur)*0.2'
    })
     mtgApp.addImPointDilation({
      o: 'O',
      a: 'I',
      x: 'abI',
      tag: 'ibuf',
      name: 'ibuf',
      hidden: true
    })
     mtgApp.addLinePar({
      d: 'axeord1',
      a: 'ibuf',
      tag: 'parIab',
      hidden: true,
      color: colLong
    })
     mtgApp.addImPointDilation({
      o: 'O',
      a: 'N',
      x: 'ordI',
      tag: 'ibuf2',
      name: 'ibuf2',
      hidden: true
    })
     mtgApp.addLinePar({
      d: 'axeab1',
      a: 'ibuf2',
      tag: 'parIord',
      hidden: true,
      color: colLong
    })
     mtgApp.addIntLineLine({
      d: 'parIord',
      d2: 'parIab',
      name: 'J1',
      tag: 'J1',
      hidden: true
    })
    */
  mtgApp.addPointXY({
    x: 'cos(valCurseur)',
    y: 'sin(valCurseur)*0.2',
    rep: 'Repere2',
    tag: 'J1',
    name: 'J1',
    hidden: true
  })
  /*
     mtgApp.addCalc({
      nameCalc: 'abJ',
      formula: 'sin(valCurseur)'
    })
     mtgApp.addCalc({
      nameCalc: 'ordJ',
      formula: '-cos(valCurseur)*0.2'
    })
     mtgApp.addImPointDilation({
      o: 'O',
      a: 'I',
      x: 'abJ',
      tag: 'jbuf',
      name: 'jbuf',
      hidden: true
    })
     mtgApp.addLinePar({
      d: 'axeord1',
      a: 'jbuf',
      tag: 'parJab',
      hidden: true,
      color: colLong
    })
     mtgApp.addImPointDilation({
      o: 'O',
      a: 'N',
      x: 'ordJ',
      tag: 'jbuf2',
      name: 'jbuf2',
      hidden: true
    })
     mtgApp.addLinePar({
      d: 'axeab1',
      a: 'jbuf2',
      tag: 'parJord',
      hidden: true,
      color: colLong
    })
     mtgApp.addIntLineLine({
      d: 'parJord',
      d2: 'parJab',
      name: 'I1',
      tag: 'I1',
      hidden: true
    })
    */
  mtgApp.addPointXY({
    x: 'sin(valCurseur)',
    y: '-cos(valCurseur)*0.2',
    rep: 'Repere2',
    tag: 'I1',
    name: 'I1',
    hidden: true
  })
  mtgApp.addPointXY({
    x: '-sin(valCurseur)',
    y: '+cos(valCurseur)*0.2',
    rep: 'Repere2',
    tag: 'I2',
    name: 'I2',
    hidden: true
  })

  for (let i = -90; i < 91; i++) {
    mtgApp.addPointXY({
      x: Math.cos(i * Math.PI / 180),
      y: Math.sin(i * Math.PI / 180),
      rep: 'Repere2',
      tag: 'vers' + i + '1',
      name: 'vers' + i + '1',
      hidden: true
    })
    mtgApp.addPointXY({
      x: -Math.cos(i * Math.PI / 180),
      y: Math.sin(i * Math.PI / 180),
      rep: 'Repere2',
      tag: 'vers' + i + '2',
      name: 'vers' + i + '2',
      hidden: true
    })
  }

  // textes
  mtgApp.addText({
    absCoord: true,
    text: 'Pôle Nord',
    x: 170,
    y: 27
  })
  mtgApp.addText({
    absCoord: true,
    text: 'Pôle Sud',
    x: 170,
    y: 310
  })
  mtgApp.addText({
    absCoord: true,
    text: 'Latitude:',
    x: 5,
    y: 370,
    color: colLat,
    tag: 'LatitudeText'
  })
  mtgApp.addText({
    absCoord: true,
    text: 'Longitude:',
    x: 5,
    y: 340,
    color: colLong,
    tag: 'LongitudeText'
  })

  // -> longi
  mtgApp.addFreePoint({
    absCoord: true,
    x: 380,
    y: 350,
    name: 'Ngfd2',
    tag: 'Ngfd2',
    pointStyle: 'bigmult',
    hidden: true
  })
  mtgApp.addCircleOr({
    o: 'Ngfd2',
    r: 0.033,
    tag: 'NgfdC2'
  })
  mtgApp.addSurfaceCircle({
    c: 'NgfdC2',
    color: colLong2,
    fillStyle: 'fill'
  })
  mtgApp.addText({
    absCoord: true,
    text: '→',
    x: 372,
    y: 340
  })

  // -> lati
  mtgApp.addFreePoint({
    absCoord: true,
    x: 380,
    y: 380,
    name: 'Ngfd',
    tag: 'Ngfd',
    pointStyle: 'bigmult',
    hidden: true
  })
  mtgApp.addCircleOr({
    o: 'Ngfd',
    r: 0.033,
    tag: 'NgfdC'
  })
  mtgApp.addSurfaceCircle({
    c: 'NgfdC',
    color: colLat2,
    fillStyle: 'fill'
  })
  mtgApp.addText({
    absCoord: true,
    text: '→',
    x: 372,
    y: 370
  })

  // <- longi
  mtgApp.addFreePoint({
    absCoord: true,
    x: 90,
    y: 350,
    name: 'Ngfd3',
    tag: 'Ngfd3',
    pointStyle: 'bigmult',
    hidden: true
  })
  mtgApp.addCircleOr({
    o: 'Ngfd3',
    r: 0.033,
    tag: 'NgfdC3'
  })
  mtgApp.addSurfaceCircle({
    c: 'NgfdC3',
    color: colLong2,
    fillStyle: 'fill',
    tag: 'NgfdC3S'
  })
  mtgApp.addText({
    absCoord: true,
    text: '←',
    x: 82,
    y: 340,
    tag: 'fhdsk'
  })

  // <- lati
  mtgApp.addFreePoint({
    absCoord: true,
    x: 90,
    y: 380,
    name: 'Ngfd4',
    tag: 'Ngfd4',
    pointStyle: 'bigmult',
    hidden: true
  })
  mtgApp.addCircleOr({
    o: 'Ngfd4',
    r: 0.033,
    tag: 'NgfdC4'
  })
  mtgApp.addSurfaceCircle({
    c: 'NgfdC4',
    color: colLat2,
    fillStyle: 'fill',
    tag: 'NgfdC4S'
  })
  mtgApp.addText({
    absCoord: true,
    text: '←',
    x: 82,
    y: 370,
    tag: 'fhdsk2'
  })

  // repoer o i1 J1
  mtgApp.addSystemOfAxis({
    o: 'O',
    a: 'I1',
    b: 'J1',
    hidden: false,
    tag: 'Repere1'
  })
  mtgApp.addPointXY({ x: 0.3, y: 0, rep: 'Repere1', name: 'Imil', tag: 'Imil', hidden: true })
  mtgApp.addPointXY({ x: 0, y: 0.3, rep: 'Repere1', name: 'Jmil', tag: 'Jmil', hidden: true })
  mtgApp.addSystemOfAxis({
    o: 'O',
    a: 'Imil',
    b: 'Jmil',
    hidden: false,
    tag: 'Repere1bis'
  })

  mtgApp.addPointXY({
    color: 'red',
    hidden: true,
    hiddenName: true,
    name: 'tagLo',
    rep: 'Repere1',
    tag: 'tagLo',
    x: '-cos(longiP)',
    y: 'sin(longiP)'
  })
  const tagRep = newtag()
  mtgApp.addSystemOfAxis({
    o: 'O',
    a: 'tagLo',
    b: 'N',
    hidden: false,
    tag: tagRep
  })
  const tagIII = newtag()
  mtgApp.addPointXY({
    x: 'cos(latiP)',
    y: 'sin(latiP)',
    rep: tagRep,
    tag: tagIII,
    name: tagIII,
    hidden: true,
    hiddenName: true,
    color: colLat
  })
  mtgApp.addImPointDilation({ o: 'O', a: 'tagLo', x: '0.3', name: 'Imil2', tag: 'Imil2', hidden: true, pointStyle: 'round', color: 'red' })
  mtgApp.addImPointDilation({ o: 'O', a: 'N', x: '0.3', name: 'Nmil', tag: 'Nmil', hidden: true, pointStyle: 'round', color: 'red' })
  mtgApp.addSystemOfAxis({
    o: 'O',
    a: 'Imil2',
    b: 'Nmil',
    hidden: false,
    tag: tagRep + 'bis'
  })

  mtgApp.addCalc({ nameCalc: 'pourV', formula: '(longiP+180-valCurseur)' })
  mtgApp.addCalc({ nameCalc: 'pourV2', formula: 'si(pourV<-180,pourV+360,si(pourV>180,pourV-360,pourV))' })
  const ff = 'si((pourV2)<-90,1000,si((pourV2)>90,1000,1))'

  const tagRet = newtag()
  mtgApp.addImPointDilation({
    o: 'O',
    a: tagIII,
    x: ff,
    tag: tagRet,
    name: tagRet,
    hidden: true,
    color: colText,
    hiddenName: true,
    pointStyle: 'biground'
  })

  mtgApp.addCalc({ nameCalc: 'miLn', formula: 'min(longiP,0)' })
  mtgApp.addCalc({ nameCalc: 'maLn', formula: 'max(longiP,0)' })
  mtgApp.addCalc({ nameCalc: 'dx', formula: 'miLn+(PPP1ppppp/PPP1PPP2)*(maLn-miLn)' })
  mtgApp.addCalc({ nameCalc: 'd1', formula: '-cos(dx)' })
  mtgApp.addCalc({ nameCalc: 'd2', formula: '+sin(dx)' })
  mtgApp.addCalc({ nameCalc: 'dx34', formula: 'miLn+0.5*(maLn-miLn)' })
  mtgApp.addCalc({ nameCalc: 'd134', formula: '-cos(dx34)*2' })
  mtgApp.addCalc({ nameCalc: 'd234', formula: '+sin(dx34)*2' })
  mtgApp.addCalc({ nameCalc: 'dx345', formula: 'maLn' })
  mtgApp.addCalc({ nameCalc: 'd1345', formula: '-cos(dx345)' })
  mtgApp.addCalc({ nameCalc: 'd2345', formula: '+sin(dx345)' })

  mtgApp.addCalc({ nameCalc: 'miLn2', formula: 'min(latiP,0)' })
  mtgApp.addCalc({ nameCalc: 'maLn2', formula: 'max(latiP,0)' })
  mtgApp.addCalc({ nameCalc: 'dx2', formula: 'miLn2+(PPP1ppppp/PPP1PPP2)*(maLn2-miLn2)' })
  mtgApp.addCalc({ nameCalc: 'd12', formula: 'cos(dx2)' })
  mtgApp.addCalc({ nameCalc: 'd22', formula: 'sin(dx2)' })
  mtgApp.addCalc({ nameCalc: 'dx26', formula: 'miLn2+0.5*(maLn2-miLn2)' })
  mtgApp.addCalc({ nameCalc: 'd126', formula: 'cos(dx26)*1.9' })
  mtgApp.addCalc({ nameCalc: 'd226', formula: 'sin(dx26)*1.9' })

  ajParaCache()
  ajMeridienCache()
  ajEquateurCache()
  ajGreenMerCache()

  mtgApp.addSegment({
    b: 'O',
    a: 'I2',
    tag: 'seg11',
    color: colText,
    hidden: false
  })
  mtgApp.addSegment({
    b: 'O',
    a: 'tagLo',
    tag: 'seg22',
    color: colText,
    hidden: false
  })
  mtgApp.addSegment({
    b: 'O',
    a: tagIII,
    tag: 'seg33',
    color: colText,
    hidden: false
  })

  // grand long
  mtgApp.addPointXY({
    x: 'd1',
    y: 'd2',
    rep: 'Repere1',
    tag: 'pLomp',
    name: 'pLomp',
    hidden: true,
    hiddenName: true,
    color: colText,
    pointStyle: 'biground'
  })
  mtgApp.addPointLocus({
    a: 'ppppp',
    b: 'pLomp',
    x: 200,
    color: colLat,
    thickness: 1,
    tag: 'jdslkjflskq'
  })

  // grand lat
  mtgApp.addPointXY({
    x: 'd12',
    y: 'd22',
    rep: tagRep,
    tag: 'pLomp2',
    name: 'pLomp2',
    hidden: true,
    hiddenName: true,
    color: colText,
    pointStyle: 'biground'
  })
  mtgApp.addPointLocus({
    a: 'ppppp',
    b: 'pLomp2',
    x: 200,
    color: colLong,
    thickness: 1,
    tag: 'jdslkjhgfhflskq'
  })

  // petit long
  mtgApp.addPointXY({
    x: 'd1',
    y: 'd2',
    rep: 'Repere1bis',
    tag: 'pLompbis',
    name: 'pLompbis',
    hidden: true,
    hiddenName: true,
    color: colText,
    pointStyle: 'biground'
  })
  mtgApp.addPointLocus({
    a: 'ppppp',
    b: 'pLompbis',
    x: 200,
    color: colLong,
    thickness: 1,
    tag: 'jdslkjflskqbis'
  })

  // petit lat
  mtgApp.addPointXY({
    x: 'd12',
    y: 'd22',
    rep: tagRep + 'bis',
    tag: 'pLompbis2',
    name: 'pLompbis2',
    hidden: true,
    hiddenName: true,
    color: colText,
    pointStyle: 'biground'
  })
  mtgApp.addPointLocus({
    a: 'ppppp',
    b: 'pLompbis2',
    x: 200,
    color: colLat,
    thickness: 2,
    tag: 'jdslkjflskqbis2'
  })

  mtgApp.addPointXY({
    x: 'd134',
    y: 'd234',
    rep: 'Repere1bis',
    tag: 'TextpLompbis',
    name: 'TextpLompbis',
    hidden: true,
    hiddenName: true,
    color: 'red',
    pointStyle: 'biground'
  })
  mtgApp.addPointXY({
    x: 'd1345',
    y: 'd2345',
    rep: 'Repere1bis',
    tag: 'TextpLompbis2',
    name: 'TextpLompbis2',
    hidden: true,
    hiddenName: true,
    color: 'green',
    pointStyle: 'biground'
  })
  mtgApp.addLinePerp({
    d: 'seg22',
    a: 'TextpLompbis2',
    tag: 'perpLong',
    hidden: true
  })
  mtgApp.addCircleOr({ o: 'TextpLompbis2', r: '0.04', tag: 'cercleLongText', hidden: true })
  mtgApp.addIntLineCircle({ d: 'perpLong', c: 'cercleLongText', name: 'intLon11', name2: 'intLon22', hidden: true })
  mtgApp.addCalc({ nameCalc: 'PlaceTextLong', formula: 'si(abs(longiP)<25,1,0)' })
  mtgApp.addImPointDilation({
    o: 'TextpLompbis',
    a: 'intLon22',
    x: 'PlaceTextLong',
    tag: 'textLongPlace',
    name: 'textLongPlace',
    hiddenName: true,
    hidden: true
  })
  mtgApp.addPointXY({
    x: 'd126',
    y: 'd226',
    rep: tagRep + 'bis',
    tag: 'Lt3p',
    name: 'Lt3p',
    hidden: true,
    hiddenName: true,
    color: 'red',
    pointStyle: 'biground'
  })

  mtgApp.addLinkedText({
    text: '#G#Val(latiP2)°',
    a: 'Lt3p',
    hAlign: 'center',
    vAlign: 'middle',
    color: colLat,
    tag: 'Lt3',
    backgroundColor: '#FFFFFF80'
  })
  mtgApp.addLinkedText({
    text: '#G#Val(longiP2)°',
    a: 'textLongPlace',
    hAlign: 'center',
    vAlign: 'middle',
    color: colLong,
    tag: 'Lt2',
    backgroundColor: '#FFFFFF80'
  })

  mtgApp.addSurfaceCircle({
    c: 'lecircle',
    color: colOcean,
    tag: 'lacircleS',
    fillStyle: 'fill'
  })

  dessineAfrique()
  dessineAmeriqueN()
  dessineAmeriqueS()
  dessineEurasie()
  dessineOceanie()
  ajEquateur()
  ajGreenMer()
  ajPara()
  ajMeridien()
  ajVilles()

  // pour cerif
  mtgApp.addLinkedText({
    text: '#G#Val(latiP2)°',
    a: stor.tagLatPt,
    offsetX: -40,
    offsetY: -5,
    color: colLat
  })
  mtgApp.addCircleOr({
    o: tagRet,
    r: 0.06,
    tag: 'cerPoP',
    hidden: true
  })
  mtgApp.addLinkedPointCircle({
    absCoord: true,
    c: 'cerPoP',
    x: 0,
    y: 0,
    name: tagRet + 'pp',
    pointStyle: 'pixel',
    hiddenName: true,
    color: colLong
  })
  // on await le dernier affichage
  await mtgApp.addLinkedText({
    text: '#G#Val(longiP2)°',
    a: tagRet + 'pp',
    hAlign: 'center',
    vAlign: 'middle',
    color: colLong,
    tag: 'Lt1',
    backgroundColor: '#FFFFFF80'
  })

  // et on affiche le code base64
  const statusElt = document.getElementById('status')
  statusElt.innerText = `Le code base64 de la figure construite (en ${getDelaySec(start)}s)`
  const fig = mtgApp.getBase64Code()
  const ct = document.getElementById('fig')
  // ct.appendChild(document.createTextNode(fig))
  ct.innerText = fig

  let fig2 = '[\'' + stor.acons[0] + '\''
  for (let i = 1; i < stor.acons.length; i++) {
    fig2 += ', \'' + stor.acons[i] + '\''
  }
  fig2 += ']'
  const ct2 = document.getElementById('acons')
  // ct.appendChild(document.createTextNode(fig))
  ct2.innerText = fig2

  const button = document.querySelector('button')
  button.addEventListener('click', async () => {
    try {
      await navigator.clipboard.writeText(fig)
      // eslint-disable-next-line no-alert
      alert('La figure est copiée dans le presse papier')
    } catch (error) {
      console.error(error)
    }
  })
  button.style.display = 'inline'

  mtgApp.addEltListener({
    elt: 'peticursL',
    eventName: 'mouseover',
    callBack: () => {
      cacheEtMontre()
    }
  })
  mtgApp.addEltListener({
    elt: 'peticursL',
    eventName: 'mouseout',
    callBack: () => {
      ontreEtCache()
    }
  })
  mtgApp.addEltListener({
    elt: 'peticursLo',
    eventName: 'mouseover',
    callBack: () => {
      cacheEtMontre()
    }
  })
  mtgApp.addEltListener({
    elt: 'peticursLo',
    eventName: 'mouseout',
    callBack: () => {
      ontreEtCache()
    }
  })

  function cacheEtMontre () {
    for (let i = 0; i < stor.acons.length; i++) {
      mtgApp.setHidden({ elt: stor.acons[i] })
    }
  }
  function ontreEtCache () {
    for (let i = 0; i < stor.acons.length; i++) {
      mtgApp.setVisible({ elt: stor.acons[i] })
    }
  }
}

function dessineAfrique () {
  mtgApp.addCalc({ nameCalc: 'kklmop', formula: 'si(valCurseurb>280,0,si(valCurseurb<20,0,1))' })
  const points = [
    ...ajSegTerre(30, 'S', 25, 'E', 20, 'S', 33, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(20, 'S', 33, 'E', 7, 'S', 38, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(7, 'S', 38, 'E', 1, 'N', 41, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(1, 'N', 41, 'E', 8, 'N', 46, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(8, 'N', 46, 'E', 18, 'N', 34, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(18, 'N', 34, 'E', 27, 'N', 27, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(27, 'N', 27, 'E', 31, 'N', 13, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(31, 'N', 13, 'E', 35, 'N', 2, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(35, 'N', 2, 'E', 26, 'N', 11, 'W', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(26, 'N', 11, 'W', 6, 'N', 8, 'W', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(6, 'N', 8, 'W', 7, 'N', 6, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(7, 'N', 6, 'E', 5, 'S', 13, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(5, 'S', 13, 'E', 18, 'S', 14, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(18, 'S', 14, 'E', 25, 'S', 17, 'E', colBordContinent, true, 'kklmop'),
    ...ajSegTerre(25, 'S', 17, 'E', 30, 'S', 25, 'E', colBordContinent, true, 'kklmop')
  ]
  mtgApp.addPolygon({
    points,
    tag: 'polaf',
    hidden: true
  })
  mtgApp.addSurfacePoly({
    poly: 'polaf',
    color: colContinent,
    hidden: false,
    fillStyle: 'fill',
    tag: 'surfAfri'
  })
}
function dessineAmeriqueN () {
  mtgApp.addCalc({ nameCalc: 'Zkklmop', formula: 'si(valCurseurb>160,1,0)' })
  let points = [
    ...ajSegTerre(79, 'N', 90, 'W', 65, 'N', 69, 'W', colBordContinent, true, 'Zkklmop'),
    ...ajSegTerre(65, 'N', 69, 'W', 47, 'N', 53, 'W', colBordContinent, true, 'Zkklmop'),
    ...ajSegTerre(47, 'N', 53, 'W', 42, 'N', 74, 'W', colBordContinent, true, 'Zkklmop'),
    ...ajSegTerre(42, 'N', 74, 'W', 31, 'N', 82, 'W', colBordContinent, true, 'Zkklmop'),
    ...ajSegTerre(31, 'N', 82, 'W', 22, 'N', 79, 'W', colBordContinent, true, 'Zkklmop'),
    ...ajSegTerre(22, 'N', 79, 'W', 29, 'N', 82, 'W', colBordContinent, true, 'Zkklmop'),
    ...ajSegTerre(29, 'N', 82, 'W', 28, 'N', 96, 'W', colBordContinent, true, 'Zkklmop'),
    ...ajSegTerre(28, 'N', 96, 'W', 17, 'N', 96, 'W', colBordContinent, true, 'Zkklmop'),
    ...ajSegTerre(17, 'N', 96, 'W', 79, 'N', 90, 'W', colBordContinent, true, 'Zkklmop')
  ]
  mtgApp.addPolygon({
    points,
    tag: 'polafee',
    hidden: true
  })
  mtgApp.addSurfacePoly({
    poly: 'polafee',
    color: colContinent,
    fillStyle: 'fill',
    tag: 'surfAm1'
  })

  mtgApp.addCalc({ nameCalc: 'fdsfsdkljfidos', formula: 'si(valCurseurb<160,si(valCurseurb<60,1,0),1)' })
  points = [
    ...ajSegTerre(17, 'N', 96, 'W', 25, 'N', 110, 'W', '#523703', true, 'fdsfsdkljfidos'),
    ...ajSegTerre(25, 'N', 110, 'W', 39, 'N', 123, 'W', '#523703', true, 'fdsfsdkljfidos'),
    ...ajSegTerre(39, 'N', 123, 'W', 49, 'N', 124, 'W', '#523703', true, 'fdsfsdkljfidos'),
    ...ajSegTerre(49, 'N', 124, 'W', 59, 'N', 160, 'W', '#523703', true, 'fdsfsdkljfidos'),
    ...ajSegTerre(59, 'N', 160, 'W', 69, 'N', 160, 'W', '#523703', true, 'fdsfsdkljfidos'),
    ...ajSegTerre(69, 'N', 160, 'W', 69, 'N', 134, 'W', '#523703', true, 'fdsfsdkljfidos'),
    ...ajSegTerre(69, 'N', 134, 'W', 80, 'N', 88, 'W', '#523703', true, 'fdsfsdkljfidos'),
    ...ajSegTerre(80, 'N', 88, 'W', 65, 'N', 69, 'W', '#523703', true, 'fdsfsdkljfidos'),
    ...ajSegTerre(65, 'N', 69, 'W', 17, 'N', 96, 'W', '#523703', true, 'fdsfsdkljfidos')
  ]
  mtgApp.addPolygon({
    points,
    tag: 'gfgfdgfdsgfd',
    hidden: true
  })
  mtgApp.addSurfacePoly({
    poly: 'gfgfdgfdsgfd',
    color: colContinent,
    fillStyle: 'fill',
    tag: 'surfAm15'
  })
}
function dessineOceanie () {
  mtgApp.addCalc({ nameCalc: 'Zkklmottp', formula: 'si(valCurseurb>150,si(valCurseurb<310,0,1),1)' })
  const points = [
    ...ajSegTerre(35, 'S', 117, 'E', 31, 'S', 133, 'E', colBordContinent, true, 'Zkklmottp'),
    ...ajSegTerre(31, 'S', 133, 'E', 43, 'S', 146, 'E', colBordContinent, true, 'Zkklmottp'),
    ...ajSegTerre(43, 'S', 146, 'E', 28, 'S', 152, 'E', colBordContinent, true, 'Zkklmottp'),
    ...ajSegTerre(28, 'S', 152, 'E', 11, 'S', 142, 'E', colBordContinent, true, 'Zkklmottp'),
    ...ajSegTerre(11, 'S', 142, 'E', 18, 'S', 140, 'E', colBordContinent, true, 'Zkklmottp'),
    ...ajSegTerre(18, 'S', 140, 'E', 13, 'S', 133, 'E', colBordContinent, true, 'Zkklmottp'),
    ...ajSegTerre(13, 'S', 133, 'E', 22, 'S', 114, 'E', colBordContinent, true, 'Zkklmottp'),
    ...ajSegTerre(22, 'S', 114, 'E', 35, 'S', 117, 'E', colBordContinent, true, 'Zkklmottp')
  ]
  mtgApp.addPolygon({
    points,
    tag: 'poladfsffee',
    hidden: true
  })
  mtgApp.addSurfacePoly({
    poly: 'poladfsffee',
    color: colContinent,
    fillStyle: 'fill',
    tag: 'surfAm2'
  })
}
function dessineAmeriqueS () {
  mtgApp.addCalc({ nameCalc: 'Zkklrmop', formula: 'si(valCurseurb<140,0,1)' })
  const points = [
    ...ajSegTerre(17, 'N', 96, 'W', 22, 'N', 87, 'W', colBordContinent, true, 'Zkklrmop'),
    ...ajSegTerre(22, 'N', 87, 'W', 10, 'N', 84, 'W', colBordContinent, true, 'Zkklrmop'),
    ...ajSegTerre(10, 'N', 84, 'W', 17, 'N', 96, 'W', colBordContinent, true, 'Zkklrmop'),
    ...ajSegTerre(10, 'N', 84, 'W', 6, 'N', 76, 'W', colBordContinent, true, 'Zkklrmop'),
    ...ajSegTerre(6, 'N', 76, 'W', 5, 'S', 79, 'W', colBordContinent, true, 'Zkklrmop'),
    ...ajSegTerre(5, 'S', 79, 'W', 18, 'S', 69, 'W', colBordContinent, true, 'Zkklrmop'),
    ...ajSegTerre(18, 'S', 69, 'W', 51, 'S', 72, 'W', colBordContinent, true, 'Zkklrmop'),
    ...ajSegTerre(51, 'S', 72, 'W', 20, 'S', 42, 'W', colBordContinent, true, 'Zkklrmop'),
    ...ajSegTerre(20, 'S', 42, 'W', 5, 'S', 36, 'W', colBordContinent, true, 'Zkklrmop'),
    ...ajSegTerre(5, 'S', 36, 'W', 10, 'N', 62, 'W', colBordContinent, true, 'Zkklrmop'),
    ...ajSegTerre(10, 'N', 62, 'W', 6, 'N', 76, 'W', colBordContinent, true, 'Zkklrmop')
  ]
  mtgApp.addPolygon({
    points,
    tag: 'ttpolafee',
    hidden: true
  })
  mtgApp.addSurfacePoly({
    poly: 'ttpolafee',
    color: colContinent,
    fillStyle: 'fill',
    tag: 'surfAm3'
  })
}
function dessineEurasie () {
  mtgApp.addCalc({ nameCalc: 'Zkklrggmop', formula: 'si(valCurseurb>182,si(valCurseurb>284,1,0),1)' })
  let points = [
    ...ajSegTerre(19, 'N', 105, 'E', 25, 'N', 116, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(25, 'N', 116, 'E', 31, 'N', 121, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(31, 'N', 121, 'E', 40, 'N', 119, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(40, 'N', 119, 'E', 39, 'N', 105, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(39, 'N', 105, 'E', 19, 'N', 105, 'E', colBordContinent, true, 'Zkklrggmop')
  ]
  mtgApp.addPolygon({
    points,
    tag: 'ttpffolafee',
    hidden: true
  })
  mtgApp.addSurfacePoly({
    poly: 'ttpffolafee',
    color: colContinent,
    fillStyle: 'fill',
    tag: 'surfAm4'
  })
  points = [
    ...ajSegTerre(40, 'N', 119, 'E', 37, 'N', 127, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(37, 'N', 127, 'E', 42, 'N', 127, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(42, 'N', 127, 'E', 45, 'N', 135, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(45, 'N', 135, 'E', 72, 'N', 150, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(72, 'N', 150, 'E', 76, 'N', 102, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(76, 'N', 102, 'E', 39, 'N', 105, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(39, 'N', 105, 'E', 40, 'N', 119, 'E', colBordContinent, true, 'Zkklrggmop')
  ]
  mtgApp.addPolygon({
    points,
    tag: 'ttpffola3fee',
    hidden: true
  })
  mtgApp.addSurfacePoly({
    poly: 'ttpffola3fee',
    color: colContinent,
    fillStyle: 'fill',
    tag: 'surfAm5'
  })
  points = [
    ...ajSegTerre(55, 'N', 133, 'E', 61, 'N', 148, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(61, 'N', 148, 'E', 63, 'N', 176, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(63, 'N', 176, 'E', 67, 'N', 173, 'E', colBordContinent, true, 'Zkklrggmop'),
    ...ajSegTerre(67, 'N', 173, 'E', 72, 'N', 150, 'E', colBordContinent, true, 'Zkklrggmop')
  ]
  mtgApp.addPolygon({
    points,
    tag: '3ttpffola3fee',
    hidden: true
  })
  mtgApp.addSurfacePoly({
    poly: '3ttpffola3fee',
    color: colContinent,
    fillStyle: 'fill',
    tag: 'surfAm6'
  })

  mtgApp.addCalc({ nameCalc: 'Zkklrggmop2', formula: 'si(valCurseurb>204,si(valCurseurb>340,1,0),1)' })
  points = [
    ...ajSegTerre(21, 'N', 73, 'E', 8, 'N', 77, 'E', colMain, true, 'Zkklrggmop2'),
    ...ajSegTerre(8, 'N', 77, 'E', 16, 'N', 80, 'E', colMain, true, 'Zkklrggmop2'),
    ...ajSegTerre(16, 'N', 80, 'E', 21, 'N', 85, 'E', colMain, true, 'Zkklrggmop2'),
    ...ajSegTerre(21, 'N', 85, 'E', 20, 'N', 94, 'E', colMain, true, 'Zkklrggmop2'),
    ...ajSegTerre(20, 'N', 94, 'E', 15, 'N', 95, 'E', colMain, true, 'Zkklrggmop2'),
    ...ajSegTerre(15, 'N', 95, 'E', 10, 'N', 104, 'E', colMain, true, 'Zkklrggmop2'),
    ...ajSegTerre(10, 'N', 104, 'E', 13, 'N', 108, 'E', colMain, true, 'Zkklrggmop2'),
    ...ajSegTerre(13, 'N', 108, 'E', 19, 'N', 105, 'E', colMain, true, 'Zkklrggmop2'),
    ...ajSegTerre(19, 'N', 105, 'E', 50, 'N', 115, 'E', colMain, true, 'Zkklrggmop2'),
    ...ajSegTerre(50, 'N', 115, 'E', 76, 'N', 102, 'E', colMain, true, 'Zkklrggmop2'),
    ...ajSegTerre(76, 'N', 102, 'E', 68, 'N', 66, 'E', colBordContinent, true, 'Zkklrggmop2'),
    ...ajSegTerre(68, 'N', 66, 'E', 21, 'N', 73, 'E', colBordContinent, true, 'Zkklrggmop2')
  ]
  mtgApp.addPolygon({
    points,
    tag: '3ttpffolaff3fee',
    hidden: true
  })
  mtgApp.addSurfacePoly({
    poly: '3ttpffolaff3fee',
    color: colContinent,
    fillStyle: 'fill',
    tag: 'surfAm7'
  })

  mtgApp.addCalc({ nameCalc: 'Zkklrggmop3', formula: 'si(valCurseurb>230,0,1)' })
  points = [
    ...ajSegTerre(68, 'N', 66, 'E', 63, 'N', 34, 'E', colBordContinent, true, 'Zkklrggmop3'),
    ...ajSegTerre(63, 'N', 34, 'E', 28, 'N', 47, 'E', colBordContinent, true, 'Zkklrggmop3'),
    ...ajSegTerre(28, 'N', 47, 'E', 21, 'N', 73, 'E', colMain, true, 'Zkklrggmop3'),
    ...ajSegTerre(21, 'N', 73, 'E', 50, 'N', 80, 'E', colMain, true, 'Zkklrggmop3'),
    ...ajSegTerre(50, 'N', 80, 'E', 68, 'N', 66, 'E', colMain, true, 'Zkklrggmop3')
  ]
  mtgApp.addPolygon({
    points,
    tag: '3ttp4ffolaff3fee',
    hidden: true
  })
  mtgApp.addSurfacePoly({
    poly: '3ttp4ffolaff3fee',
    color: colContinent,
    fillStyle: 'fill',
    tag: 'surfAm8'
  })

  mtgApp.addCalc({ nameCalc: 'Zkklrggmop4', formula: 'si(valCurseurb>270,0,si(valCurseurb<33,0,1))' })
  points = [
    ...ajSegTerre(63, 'N', 34, 'E', 70, 'N', 27, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(70, 'N', 27, 'E', 60, 'N', 4, 'E', col1, true, 'Zkklrggmop4'),
    ...ajSegTerre(60, 'N', 4, 'E', 53, 'N', 8, 'E', col2, true, 'Zkklrggmop4'),
    ...ajSegTerre(53, 'N', 8, 'E', 48, 'N', 0, 'W', colText, true, 'Zkklrggmop4'),

    ...ajSegTerre(48, 'N', 0, 'W', 48, 'N', 4, 'W', colMain, true, 'Zkklrggmop4'),
    ...ajSegTerre(48, 'N', 4, 'W', 47, 'N', 1, 'W', colMain, true, 'Zkklrggmop4'),
    ...ajSegTerre(47, 'N', 1, 'W', 43, 'N', 1, 'W', colMain, true, 'Zkklrggmop4'),
    ...ajSegTerre(43, 'N', 1, 'W', 43, 'N', 8, 'W', colMain, true, 'Zkklrggmop4'),
    ...ajSegTerre(43, 'N', 8, 'W', 37, 'N', 8, 'W', colLat, true, 'Zkklrggmop4'),
    ...ajSegTerre(37, 'N', 8, 'W', 36, 'N', 5, 'W', colLat, true, 'Zkklrggmop4'),
    ...ajSegTerre(36, 'N', 5, 'W', 37, 'N', 1, 'W', colLat, true, 'Zkklrggmop4'),
    ...ajSegTerre(37, 'N', 1, 'W', 45, 'N', 11, 'E', colLat, true, 'Zkklrggmop4'),

    ...ajSegTerre(45, 'N', 11, 'E', 38, 'N', 15, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(38, 'N', 15, 'E', 40, 'N', 18, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(40, 'N', 18, 'E', 42, 'N', 14, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(42, 'N', 14, 'E', 45, 'N', 13, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(45, 'N', 13, 'E', 45, 'N', 14, 'E', col3, true, 'Zkklrggmop4'),
    ...ajSegTerre(45, 'N', 14, 'E', 41, 'N', 19, 'E', col3, true, 'Zkklrggmop4'),
    ...ajSegTerre(41, 'N', 19, 'E', 40, 'N', 19, 'E', col3, true, 'Zkklrggmop4'),
    ...ajSegTerre(40, 'N', 19, 'E', 36, 'N', 22, 'E', col3, true, 'Zkklrggmop4'),
    ...ajSegTerre(36, 'N', 22, 'E', 40, 'N', 23, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(40, 'N', 23, 'E', 36, 'N', 28, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(36, 'N', 28, 'E', 37, 'N', 36, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(37, 'N', 36, 'E', 31, 'N', 34, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(31, 'N', 34, 'E', 29, 'N', 33, 'E', colText, true, 'Zkklrggmop4'),

    ...ajSegTerre(29, 'N', 33, 'E', 22, 'N', 40, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(22, 'N', 40, 'E', 15, 'N', 45, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(15, 'N', 45, 'E', 17, 'N', 51, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(17, 'N', 51, 'E', 21, 'N', 59, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(21, 'N', 59, 'E', 24, 'N', 55, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(24, 'N', 55, 'E', 23, 'N', 43, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(23, 'N', 43, 'E', 30, 'N', 50, 'E', colText, true, 'Zkklrggmop4'),
    ...ajSegTerre(30, 'N', 50, 'E', 63, 'N', 34, 'E', col1, true, 'Zkklrggmop4')
  ]
  mtgApp.addPolygon({
    points,
    tag: 'ttpffofdfdlafee2',
    hidden: true
  })
  mtgApp.addSurfacePoly({
    poly: 'ttpffofdfdlafee2',
    color: colContinent,
    fillStyle: 'fill',
    tag: 'surfAm9'
  })
}

function ajEquateur () {
  mtgApp.addPointXY({
    x: '-cos(PPP1ppppp/PPP1PPP2*180)',
    y: '-sin(PPP1ppppp/PPP1PPP2*180)*0.2',
    rep: 'Repere2',
    tag: 'PtMoEq',
    name: 'PtMoEq',
    hiddenName: true,
    pointStyle: 'biground',
    color: colMain,
    hidden: true
  })
  mtgApp.addPointLocus({
    a: 'ppppp',
    b: 'PtMoEq',
    x: 200,
    color: colLat,
    thickness: 6
  })
}
function ajGreenMer () {
  for (let i = -90; i < 90; i++) {
    const j = (i < 0) ? 'S' : 'N'
    const tag = ajPoint(Math.abs(i), j, 0, 'E', colLong, 'round')
    if (i === 0) {
      // @todo mettre ici un truc pour que la section puisse récupérer ce tag (le renommer ?)
      stor.tagLongPtOrigine = tag
    }
  }
}
function ajPara () {
  for (let i = 0; i < 180; i++) {
    const jj = ajPointF('latiP', 'N', i, 'E', colLat, 'littleround')
    if (i === 0) {
      // @todo mettre ici un truc pour que la section puisse récupérer ce tag (le renommer ?)
      stor.tagLatPt = jj
    }
  }
}
function ajMeridien () {
  for (let i = -90; i < 90; i++) {
    const j = (i < 0) ? 'S' : 'N'
    ajPoint(Math.abs(i), j, 'longiP', 'E', colLong, 'littleround')
  }
  mtgApp.addCircleOA({
    o: 'O',
    a: 'N',
    thickness: 4
  })
}

function ajEquateurCache () {
  mtgApp.addPointXY({
    x: 'cos(PPP1ppppp/PPP1PPP2*180)',
    y: 'sin(PPP1ppppp/PPP1PPP2*180)*0.2',
    rep: 'Repere2',
    tag: 'PtMoEq7',
    name: 'PtMoEq7',
    hiddenName: true,
    pointStyle: 'biground',
    color: colMain,
    hidden: true
  })
  mtgApp.addPointLocus({
    a: 'ppppp',
    b: 'PtMoEq7',
    x: 200,
    color: colLat,
    thickness: 6,
    lineStyle: 'dashdash',
    tag: 'equaAvire',
    name: 'equaAvire'
  })
}
function ajGreenMerCache () {
  for (let i = -90; i < 90; i += 4) {
    const j = (i < 0) ? 'S' : 'N'
    ajPointMe(Math.abs(i), j, 0, 'E', colLong, 'round')
  }
}
function ajParaCache () {
  for (let i = 0; i < 180; i += 4) {
    ajPointF('latiP', 'N', -i, 'E', colLat, 'littleround')
  }
}
function ajMeridienCache () {
  for (let i = -90; i < 90; i += 4) {
    const j = (i < 0) ? 'S' : 'N'
    ajPointMe(Math.abs(i), j, 'longiP', 'E', colLong, 'littleround')
  }
}

function ajVilles () {
  let i = 0
  for (const { nom, lat, ns, long, eo, int1: [lat1, lat2], int2 } of listVille) {
    i++
    // condition d’affichage du point
    // => valCurseurb doit être dans un des deux intervalles (int1 et int2)
    const condInt2 = int2
      ? `si(valCurseurb>${int2[0]},si(valCurseurb<${int2[1]},1,0),0)`
      : '0'
    const condition = `si(valCurseurb>${lat1},si(valCurseurb<${lat2},1,${condInt2}),0)`
    const a = ajPoint(lat, ns, long, eo, colText, 'biground', false, condition)
    mtgApp.addLinkedText({
      text: nom,
      a,
      offsetX: 16,
      offsetY: -10,
      color: colText,
      fontSize: 15,
      tag: 'Ville' + i
    })
    stor.acons.push('Ville' + i)
  }
}

function ajSegTerre (lat1, ns1, long1, eo1, lat2, ns2, long2, eo2, color, visible, condition) {
  const latA = (ns1 === 'N') ? lat1 : -lat1
  const longA = (eo1 === 'E') ? -long1 : long1
  const latB = (ns2 === 'N') ? lat2 : -lat2
  const longB = (eo2 === 'E') ? -long2 : long2

  // pour le dessin "de face", deux points suffisent par segment, mais sur l’horizon (E & W)
  // on peut avoir un artefact sur les grands segments en latitude (de la mer apparait dans les continents)
  // => on ne s’occupe que de la différence de latitude, on segmente tous les 10° (ça suffit pour faire disparaître l’artefact)
  const nbPas = Math.max(1, Math.ceil(Math.abs((latB - latA) / 10)))
  const pasLat = (latB - latA) / nbPas
  const pasLong = (longB - longA) / nbPas
  // nbPas peut être 1, dans ce cas on retourne seulement 2 points
  const tags = []
  for (let i = 0; i <= nbPas; i++) {
    tags.push(ajPoint(latA + pasLat * i, 'N', longA + pasLong * i, 'W', color, 'pixel', visible, condition))
  }

  return tags
}

/**
 * fct synchrone, l’affichage sera fait en async plus tard
 * @param lat1
 * @param ns
 * @param long1
 * @param eo
 * @param color
 * @param pointStyle
 * @param visible
 * @param condition
 * @return {string} Le tag du point créé
 */
function ajPoint (lat1, ns, long1, eo, color, pointStyle, visible, condition) {
  const long = (ns === 'N') ? lat1 : -lat1
  let lat, aa
  if (typeof long1 === 'string') {
    lat = long1
    aa = '(' + long1 + '+180-valCurseur)'
  } else {
    lat = (eo === 'E') ? -long1 : long1
    aa = '(' + (lat + 180) + '-valCurseur)'
  }
  const rem = '(si(' + aa + '<-180,' + aa + '+360,si(' + aa + '>180,' + aa + '-360,' + aa + ')))'
  /*
    const tag1 = newtag()
     mtgApp.addCalc({
      nameCalc: tag1,
      formula: '-cos(' + lat + ')'
    })
    const tag2 = newtag()
     mtgApp.addCalc({
      nameCalc: tag2,
      formula: 'sin(' + lat + ')'
    })
    */
  const tag3 = newtag()
  mtgApp.addPointXY({
    x: '-cos(' + lat + ')',
    y: 'sin(' + lat + ')',
    rep: 'Repere1',
    tag: tag3,
    name: tag3,
    hidden: true,
    hiddenName: true,
    color: colMain
  })
  const tagRep = newtag()
  mtgApp.addSystemOfAxis({
    o: 'O',
    a: tag3,
    b: 'N',
    hidden: false,
    tag: tagRep
  })
  /*
    const tagSeg = newtag()
     mtgApp.addSegment({
      a: tag3,
      b: 'O',
      tag: tagSeg,
      hidden: true
    })

    const tagAbI = newtag()
     mtgApp.addCalc({
      nameCalc: tagAbI,
      formula: 'cos(' + long + ')'
    })
    const tagOrdI = newtag()
     mtgApp.addCalc({
      nameCalc: tagOrdI,
      formula: 'sin(' + long + ')'
    })
    const tagBuf1 = newtag()
     mtgApp.addImPointDilation({
      o: 'O',
      a: tag3,
      x: tagAbI,
      tag: tagBuf1,
      name: tagBuf1,
      hidden: true
    })
    const tagparI1 = newtag()
     mtgApp.addLinePar({
      d: 'axeord1',
      a: tagBuf1,
      tag: tagparI1,
      hidden: true,
      color: colLong
    })
    const tagBuf2 = newtag()
     mtgApp.addImPointDilation({
      o: 'O',
      a: 'N',
      x: tagOrdI,
      tag: tagBuf2,
      name: tagBuf2,
      hidden: true
    })
    const tagparI2 = newtag()
     mtgApp.addLinePar({
      d: tagSeg,
      a: tagBuf2,
      tag: tagparI2,
      hidden: true,
      color: colLong
    })
    */
  const tagIII = newtag()
  mtgApp.addPointXY({
    x: 'cos(' + long + ')',
    y: 'sin(' + long + ')',
    rep: tagRep,
    tag: tagIII,
    name: tagIII,
    hidden: true,
    hiddenName: true,
    color: colLat
  })
  /*
     mtgApp.addIntLineLine({
      d: tagparI2,
      d2: tagparI1,
      name: tagIII,
      pointStyle: 'bigmult',
      color: colMain,
      hidden: true
    })
    const tagCalBuf = newtag()
     mtgApp.addCalc({
      nameCalc: tagCalBuf,
      formula: rem
    })
    const tagCal = newtag()
     mtgApp.addCalc({
      nameCalc: tagCal,
      formula: 'si(' + tagCalBuf + '<-90,1000,si(' + tagCalBuf + '>90,1000,1))'
    })
    */
  const ff = 'si(' + rem + '<-90,0,si(' + rem + '>90,0,1))'
  const ff2 = 'si(' + rem + '>90,0,1)'
  const tagRet = newtag()
  mtgApp.addImPointDilation({
    o: 'vers' + Math.round(long) + '1',
    a: tagIII,
    x: ff,
    tag: tagRet,
    name: tagRet,
    hidden: true,
    color,
    hiddenName: true,
    pointStyle
  })
  const tagRet2 = newtag()
  mtgApp.addImPointDilation({
    o: 'vers' + Math.round(long) + '2',
    a: tagRet,
    x: ff2,
    tag: tagRet2,
    name: tagRet2,
    hidden: visible || (condition),
    color,
    hiddenName: true,
    pointStyle
  })
  if (condition) {
    const tagRet3 = newtag()
    mtgApp.addImPointDilation({
      o: 'R',
      a: tagRet2,
      x: condition,
      tag: tagRet3,
      name: tagRet3,
      hidden: visible,
      color,
      hiddenName: true,
      pointStyle
    })
    return tagRet3
  }

  return tagRet2
  // O et O7 dependent de la lat
}
function ajPointF (lat1, ns, long1, eo, color, pointStyle) {
  const long = (ns === 'N') ? lat1 : -lat1
  const lat = (eo === 'E') ? -long1 : long1

  /*
    const tag1 = newtag()
     mtgApp.addCalc({
      nameCalc: tag1,
      formula: '-cos(' + lat + ')'
    })
    const tag2 = newtag()
     mtgApp.addCalc({
      nameCalc: tag2,
      formula: 'sin(' + lat + ')*0.2'
    })
    const tag3 = newtag()
     mtgApp.addImPointDilation({
      o: 'O',
      a: 'I',
      x: tag1,
      tag: tag3,
      name: tag3,
      hidden: true
    })
    const tag4 = newtag()
     mtgApp.addLinePar({
      d: 'axeord1',
      a: tag3,
      tag: tag4,
      hidden: true,
      color: colLong
    })
    const tag5 = newtag()
     mtgApp.addImPointDilation({
      o: 'O',
      a: 'N',
      x: tag2,
      tag: tag5,
      name: tag5,
      hidden: true
    })
    const tag6 = newtag()
     mtgApp.addLinePar({
      d: 'axeab1',
      a: tag5,
      tag: tag6,
      hidden: true,
      color: colLong
    })
    const tag7 = newtag()
     mtgApp.addIntLineLine({
      d: tag6,
      d2: tag4,
      name: tag7,
      // pointStyle: 'bigmult',
      color,
      hidden: true,
      hiddenName: true
    })
    const tagSeg = newtag()
     mtgApp.addSegment({
      a: tag7,
      b: 'O',
      tag: tagSeg,
      hidden: true
    })
    */

  const tag7 = newtag()
  mtgApp.addPointXY({
    x: '-cos(' + lat + ')',
    y: 'sin(' + lat + ')*0.2',
    rep: 'Repere2',
    tag: tag7,
    name: tag7,
    hidden: true,
    hiddenName: true
  })
  const tagRep = newtag()
  mtgApp.addSystemOfAxis({
    o: 'O',
    a: tag7,
    b: 'N',
    hidden: false,
    tag: tagRep
  })
  const tagIII = newtag()
  mtgApp.addPointXY({
    x: 'cos(' + long + ')',
    y: 'sin(' + long + ')',
    rep: tagRep,
    tag: tagIII,
    name: tagIII,
    pointStyle,
    color,
    hiddenName: true
  })
  return tagIII

  /*
    const tagAbI = newtag()
     mtgApp.addCalc({
      nameCalc: tagAbI,
      formula: 'cos(' + long + ')'
    })
    const tagOrdI = newtag()
     mtgApp.addCalc({
      nameCalc: tagOrdI,
      formula: 'sin(' + long + ')'
    })
    const tagBuf1 = newtag()
     mtgApp.addImPointDilation({
      o: 'O',
      a: tag7,
      x: tagAbI,
      tag: tagBuf1,
      name: tagBuf1,
      hidden: true,
      hiddenName: true
    })
    const tagparI1 = newtag()
     mtgApp.addLinePar({
      d: 'axeord1',
      a: tagBuf1,
      tag: tagparI1,
      hidden: true,
      color: colLong
    })
    const tagBuf2 = newtag()
     mtgApp.addImPointDilation({
      o: 'O',
      a: 'N',
      x: tagOrdI,
      tag: tagBuf2,
      name: tagBuf2,
      hidden: true,
      hiddenName: true
    })
    const tagparI2 = newtag()
     mtgApp.addLinePar({
      d: tagSeg,
      a: tagBuf2,
      tag: tagparI2,
      hidden: true,
      color: colLong
    })
    const tagIII = newtag()
     mtgApp.addIntLineLine({
      d: tagparI2,
      d2: tagparI1,
      name: tagIII,
      tag: tagIII,
      pointStyle,
      color,
      hidden: false,
      hiddenName: true
    })
    return tagIII
    */
}
function ajPointMe (lat1, ns, long1, eo, color, pointStyle, visible) {
  const long = (ns === 'N') ? lat1 : -lat1
  let lat
  if (typeof long1 === 'string') {
    lat = long1
  } else {
    lat = (eo === 'E') ? -long1 : long1
  }
  /*
    const tag1 = newtag()
     mtgApp.addCalc({
      nameCalc: tag1,
      formula: '-cos(' + lat + ')'
    })
    const tag2 = newtag()
     mtgApp.addCalc({
      nameCalc: tag2,
      formula: 'sin(' + lat + ')'
    })
    */
  const tag3 = newtag()
  mtgApp.addPointXY({
    x: '-cos(' + lat + ')',
    y: 'sin(' + lat + ')',
    rep: 'Repere1',
    tag: tag3,
    name: tag3,
    hidden: true,
    hiddenName: true,
    color: colMain
  })
  const tagRep = newtag()
  mtgApp.addSystemOfAxis({
    o: 'O',
    a: tag3,
    b: 'N',
    hidden: false,
    tag: tagRep
  })
  /*
    const tagSeg = newtag()
     mtgApp.addSegment({
      a: tag3,
      b: 'O',
      tag: tagSeg,
      hidden: true
    })

    const tagAbI = newtag()
     mtgApp.addCalc({
      nameCalc: tagAbI,
      formula: 'cos(' + long + ')'
    })
    const tagOrdI = newtag()
     mtgApp.addCalc({
      nameCalc: tagOrdI,
      formula: 'sin(' + long + ')'
    })
    const tagBuf1 = newtag()
     mtgApp.addImPointDilation({
      o: 'O',
      a: tag3,
      x: tagAbI,
      tag: tagBuf1,
      name: tagBuf1,
      hidden: true
    })
    const tagparI1 = newtag()
     mtgApp.addLinePar({
      d: 'axeord1',
      a: tagBuf1,
      tag: tagparI1,
      hidden: true,
      color: colLong
    })
    const tagBuf2 = newtag()
     mtgApp.addImPointDilation({
      o: 'O',
      a: 'N',
      x: tagOrdI,
      tag: tagBuf2,
      name: tagBuf2,
      hidden: true
    })
    const tagparI2 = newtag()
     mtgApp.addLinePar({
      d: tagSeg,
      a: tagBuf2,
      tag: tagparI2,
      hidden: true,
      color: colLong
    })
    */
  const tagIII = newtag()
  mtgApp.addPointXY({
    x: 'cos(' + long + ')',
    y: 'sin(' + long + ')',
    rep: tagRep,
    tag: tagIII,
    name: tagIII,
    hidden: visible,
    hiddenName: true,
    color,
    pointStyle
  })
  /*
     mtgApp.addIntLineLine({
      d: tagparI2,
      d2: tagparI1,
      name: tagIII,
      pointStyle: 'bigmult',
      color: colMain,
      hidden: true
    })
    const tagCalBuf = newtag()
     mtgApp.addCalc({
      nameCalc: tagCalBuf,
      formula: rem
    })
    const tagCal = newtag()
     mtgApp.addCalc({
      nameCalc: tagCal,
      formula: 'si(' + tagCalBuf + '<-90,1000,si(' + tagCalBuf + '>90,1000,1))'
    })
    */
  return tagIII
}

build()
