/**
 * fonctions communes à notre interface de dev
 * @fileOverview
 */

import { addElement, getElement } from 'src/lib/utils/dom/main'

/** une partie de RessourceJ3p */
export interface RessourceDesc {
  rid: string
  titre: string
}

export const biblis: Record<string, string> = {
  sesabibli: 'https://bibliotheque.sesamath.net/',
  sesacommun: 'https://commun.sesamath.net/'
}

/**
 * Ajoute une erreur sur la page
 * @param error
 * @private
 */
export function addError (error: Error | string): void {
  console.error(error)
  const eltError = getElement('errors')
  if (eltError && eltError instanceof HTMLElement) addElement(eltError, 'li', typeof error === 'string' ? error : error.message)
}

/**
 * Alias de document.getElementById (qui râle en console s’il ne trouve pas)
 * @param {string} id
 * @private
 */
export function get (id: string): HTMLElement | null {
  const elt = document.getElementById(id)
  if (!elt) console.error(Error(`#${id} non trouvé dans le dom`))
  return elt
}

interface GetRidUrlOptions {
  /** pour avoir l’url de l’api */
  api?: boolean,
  /** pour avoir l’url de visionnage en prod */
  j3p?: boolean,
  /** pour avoir l’url de visionnage en dev */
  j3pdev?: boolean,
  /** pour avoir l’url de visionnage en local */
  local?: boolean,
  /** pour avoir l’url describe en prod */
  describe?: boolean
}

/**
 * Retourne l’url demandée via les options (sans option ce sera celle de visionnage sur sa bibli)
 * @param rid
 * @param options
 */
export function getRidUrl (rid: string, { api, j3p, j3pdev, local, describe }: GetRidUrlOptions = {}): string {
  const [baseId, id] = rid.split('/')
  if (baseId && biblis[baseId]) {
    if (api) return `${biblis[baseId]}api/public/${id}`
    if (describe) return `${biblis[baseId]}ressource/decrire/${id}`
    if (j3p) return `https://j3p.sesamath.net/?rid=${rid}`
    if (j3pdev) return `https://j3p.sesamath.dev/?rid=${rid}`
    if (local) return `http://localhost:8081/?rid=${rid}`
    return `${biblis[baseId]}public/voir/${id}`
  }
  console.error(Error(`rid ${rid} invalide car sésathèque ${baseId} inconnue`))
  return ''
}

export function printRessourceLinks (container: HTMLElement, ressource: RessourceDesc, isJ3p = true): HTMLElement {
  const { rid } = ressource
  const child = addElement(container, 'li')
  const ressTitle = ressource.titre ?? 'titre inconnu'
  addElement(child, 'span', { textContent: ressTitle })
  // les liens ouvrent tous dans un nouvel onglet
  if (isJ3p) {
    addElement(child, 'span', { textContent: `(${rid})`, style: { paddingLeft: '2ch' } })
    const url = getRidUrl(rid)
    addElement(child, 'a', { textContent: 'original', href: url, target: 'original' })
    addElement(child, 'a', { textContent: 'json', href: getRidUrl(rid, { api: true }), target: 'json' })
    addElement(child, 'a', {
      textContent: 'description',
      href: url.replace('public/voir', 'ressource/decrire'),
      target: 'description'
    })
    addElement(child, 'a', { textContent: 'j3p prod', href: getRidUrl(rid, { j3p: true }), target: 'prod' })
    addElement(child, 'a', { textContent: 'j3p dev', href: getRidUrl(rid, { j3pdev: true }), target: 'dev' })
    addElement(child, 'a', { textContent: 'localhost', href: getRidUrl(rid, { local: true }), target: 'local' })
  } else {
    const url = getRidUrl(rid, { describe: true })
    addElement(child, 'a', { textContent: rid, href: url, target: 'description', style: { paddingLeft: '2ch' } })
  }
  return child
}
