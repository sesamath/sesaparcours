/**
 * Utilisé par niveaux.html pour peupler la liste des anomalies de niveaux
 */
import { addError, get, getRidUrl, printRessourceLinks } from 'src/dev/helpers'
import { addElement, addTextContent, empty } from 'src/lib/utils/dom/main'

interface RessTitre {
  oid: string
  titre: string
}
interface Orphan extends RessTitre {
  niveaux: string[]
}

interface Warning {
  oid: string
  titre: string
  warn: string
}

interface NiveauxUsage {
  levelLess: RessTitre[]
  levelOrphans: Orphan[]
  levelOrphansInOthers: RessTitre[]
  levelTreeWarnings: Record<string, (string | Warning)[]>
  /** levelCode => oid[] */
  treesByLevel: Record<string, string[]>
  /** oid => titre */
  treeTitles: Record<string, string>
  /** levelCode => levelName */
  levelNames: Record<string, string>
  date: string
}

// pour les niveaux on ne regarde que cette bibli
const baseId = 'sesabibli'

function addToggle (container: HTMLElement, trigger: HTMLElement, initClosed = true): void {
  container.classList.add('folder')
  // init fermé ?
  if (initClosed) {
    container.classList.add('closed')
  }
  // puis toggle au click
  trigger.addEventListener('click', () => {
    if (container.classList.contains('closed')) {
      container.classList.remove('closed')
    } else {
      container.classList.add('closed')
    }
  })
}

/**
 * Déplie/replie tout
 * @param {boolean} folded passer true pour tout replier, false pour déplier
 * @private
 */
function foldAll (folded: boolean): void {
  for (const ct of document.querySelectorAll('.folder')) {
    if (folded) ct.classList.add('closed')
    else ct.classList.remove('closed')
  }
}
function getCt (id: string): HTMLElement {
  const ct = get(id)
  if (ct == null) throw Error(`Impossible de trouver #${id} dans le DOM`)
  empty(ct)
  return ct
}

async function buildLevelWarningsCsv (link: HTMLAnchorElement, respJson: NiveauxUsage): Promise<void> {
  const { treesByLevel, levelTreeWarnings, treeTitles } = respJson
  const csv = ['arbreOid;arbreTitre;lien;titre;warning']
  for (const oids of Object.values(treesByLevel)) {
    for (const oid of oids) {
      const levelWarnings = (levelTreeWarnings[oid] ?? []).filter(w => typeof w === 'object' && w.warn?.includes('pas le niveau')) as Warning[]
      if (!levelWarnings.length) continue
      for (const warning of levelWarnings) {
        const { oid: ressOid, titre, warn } = warning
        const titreTree = treeTitles[oid]
        const ressUrl = getRidUrl(`${baseId}/${ressOid}`, { describe: true })
        csv.push(`${oid};${titreTree};${ressUrl};${titre};${warn}`)
      }
    }
  }
  const blob = new Blob([csv.join('\n')], { type: 'text/csv;charset=utf-8;' })
  const href = URL.createObjectURL(blob)
  link.setAttribute('href', href)
}

/**
 * Lance la construction des listes d'anomalies de niveaux (d’après niveaux.usage.json construit en dev)
 */
async function buildLevelTree (): Promise<void> {
  try {
    // le json est construit en dev, d'après les ressources de prod
    const response = await fetch('https://j3p.sesamath.dev/niveaux.usage.json')
    const respJson = await response.json() as NiveauxUsage
    const { date, levelOrphans, levelOrphansInOthers, levelLess, levelTreeWarnings, levelNames, treeTitles, treesByLevel } = respJson

    // la date
    let ct = getCt('infos')
    const dateStr = new Date(date).toLocaleDateString('fr-FR', { day: 'numeric', month: 'numeric', year: 'numeric', hour: '2-digit', minute: '2-digit' })
    addTextContent(ct, `Sur la base des anomalies relevées le ${dateStr}`)
    // le lien de téléchargement
    const p = addElement(ct, 'p', { content: 'Télécharger les anomalies de niveaux ' })
    const a = addElement(p, 'a', { content: 'au format csv', attrs: { href: '#', target: '_blank', download: 'anomaliesNiveaux.csv' } })
    // faut l'appeler une seule fois, le lien sera ensuite avec le bon href et pourra être cliqué autant de fois qu'on veut
    a.addEventListener('click', () => buildLevelWarningsCsv(a, respJson), { once: true })

    // on avait une légende dans le commit a7edf6d88 (2024-12-20), devenu inutile avec les niveaux en clair dans les warnings

    // les arbres de niveau
    ct = getCt('levelTreeWarnings')
    const h2 = addElement(ct, 'h2', { content: 'Anomalies par arbre de niveau' })
    addToggle(ct, h2, false)
    for (const [levelCode, oids] of Object.entries(treesByLevel)) {
      const ulLevel = addElement(ct, 'ul')
      addElement(ulLevel, 'h3', { content: `${levelNames[levelCode]}` })
      // boucle sur les arbres
      for (const oid of oids) {
        const liTree = printRessourceLinks(ulLevel, { rid: `${baseId}/${oid}`, titre: `arbre « ${treeTitles[oid]} »` }, false)
        const warnings = levelTreeWarnings[oid]
        if (!Array.isArray(warnings)) {
          addError(`L’arbre ${oid} n’est pas listé dans levelTreeWarnings`)
          continue
        }
        if (!warnings.length) {
          addTextContent(liTree, 'Aucune anomalie')
          continue
        }
        liTree.classList.add('levelTree')
        addTextContent(liTree, ` (${warnings.length} anomalies)`)
        // y'a des warnings sur cet arbre
        const ulWarnings = addElement(liTree, 'ul')
        const titreTree = liTree.querySelector('span')
        if (titreTree) {
          addToggle(liTree, titreTree)
        }
        for (const warning of warnings) {
          if (typeof warning === 'string') {
            const li = addElement(ulWarnings, 'li')
            addTextContent(li, warning)
          } else {
            const { oid, titre, warn } = warning
            const li = printRessourceLinks(ulWarnings, { rid: `${baseId}/${oid}`, titre }, false) // pas sûr que ce soit un arbre mais on veut pas des liens j3p
            addTextContent(li, warn)
          }
        }
      }
    }

    // ressources sans niveau
    ct = getCt('levelLess')
    if (levelLess.length) {
      const h2 = addElement(ct, 'h2', 'Liste des ressources sans niveau')
      addElement(h2, 'span', { content: ` (${levelLess.length} ressources sans niveau renseigné)`, style: { fontSize: '0.8em' } })
      addToggle(ct, h2)
      const ul = addElement(ct, 'ul')
      for (const { oid, titre } of levelLess) {
        printRessourceLinks(ul, { rid: `${baseId}/${oid}`, titre }, false)
      }
    } else {
      addTextContent(ct, 'Aucune ressource sans niveau')
    }

    // levelOrphans
    ct = getCt('levelOrphans')
    const h2Orphan = addElement(ct, 'h2', 'Liste des ressources orphelines')
    if (levelOrphans.length) {
      addElement(h2Orphan, 'span', { content: ` (${levelOrphans.length} qui ne sont listées ni dans un arbre de niveau ni dans l’arbre de l’éditeur de graphe)`, style: { fontSize: '0.8em' } })
      addToggle(ct, h2Orphan)
      const ul = addElement(ct, 'ul')
      for (const orphan of levelOrphans) {
        const { oid, titre, niveaux: nx } = orphan
        const li = printRessourceLinks(ul, { rid: `${baseId}/${oid}`, titre }, false)
        if (Array.isArray(nx)) {
          const niv = nx.map(level => levelNames[level]).join(', ')
          addTextContent(li, ` (${niv})`)
        } else {
          console.error('levelOrphans contient un élément sans niveaux', orphan)
        }
      }
    } else {
      addElement(h2Orphan, 'span', { content: ' => aucune :-)', style: { fontSize: '0.8em' } })
    }

    // levelOrphansInOthers
    ct = getCt('levelOrphansInOthers')
    const h2OrphanOthers = addElement(ct, 'h2', 'Pour info, ressources listées uniquement dans l’éditeur de graphe')
    addToggle(ct, h2OrphanOthers)
    const ul = addElement(ct, 'ul')
    // on met en 1er l'arbre complet
    const li = printRessourceLinks(ul, { rid: 'sesabibli/50045', titre: 'arbre complet' })
    li.style.listStyleType = 'none'
    li.style.marginLeft = '3rem'
    li.style.fontWeight = 'bold'
    if (levelOrphansInOthers.length) {
      addElement(h2OrphanOthers, 'span', { content: ` (${levelOrphansInOthers.length} ressources)`, style: { fontSize: '0.8em' } })
      for (const orphan of levelOrphansInOthers) {
        const { oid, titre } = orphan
        printRessourceLinks(ul, { rid: `${baseId}/${oid}`, titre }, false)
      }
    } else {
      addElement(h2OrphanOthers, 'span', { content: '(0 ressource)', style: { fontSize: '0.8em' } })
    }

    // et les actions
    const expandAllButton = get('expandAll')
    if (expandAllButton) expandAllButton.addEventListener('click', foldAll.bind(null, false))
    const foldAllButton = get('foldAll')
    if (foldAllButton) foldAllButton.addEventListener('click', foldAll.bind(null, true))
  } catch (error) {
    addError(error as Error)
  }
}

export default buildLevelTree
