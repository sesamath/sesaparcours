import { tab } from 'src/dev/start.runners'
import { isLegacyResultat } from 'src/lib/core/checks'
import { convertGraph } from 'src/lib/core/convert1to2'
import Graph, { type GraphJson } from 'src/lib/entities/Graph'
import type { LegacyResultat, Resultat } from 'src/lib/types'
import { addElement, addRichElt, addTextContent, empty, isElt, showError } from 'src/lib/utils/dom/main'

import { safeEval } from 'src/lib/utils/object'
import { legacyResultat1 } from 'test/fixtures/resultats/resultat1'
import { grapheBienEcrire, graphEx1, graphExFcts, graphMix, graphEx4, graphReprise, graphEx2, graphTest, lastResultat, resultatsGrapheV2NonFini, resultatV1NonFini, unResultatGraphFini } from './exemples'

const preventSubmit = (event: Event) => {
  event.preventDefault()
}

/**
 * Charge la liste de toutes les sections
 */
async function loadListe () {
  // on veut pas d’import statique car cette fct est rarement appelée, on importe donc à la demande
  const { getAdresses } = await import('src/legacy/core/adresses.js')
  // on veut les chemins pour exclure archive et bacasable, et trier dessus
  return Object.entries(getAdresses())
    .map(([section, path]) => {
      if (typeof path === 'string') {
        if (path) path += '/'
        if (!/^(archive|bacasable)/.test(path)) return `sections/${path}section${section}.js`
      } else {
        console.error(Error(`Pb dans adresses.js, le path de ${section} n’est pas une string`))
      }
      return ''
    })
    .filter(Boolean) // pour virer les string vides
    .sort() // pour trier par ordre alphabétique
}

function getObj (elt: HTMLTextAreaElement): Object | null {
  let obj: unknown
  try {
    obj = JSON.parse(elt.value)
  } catch (error) {
    try {
      obj = safeEval(elt.value)
    } catch (error) {
      showError(error)
    }
  }
  if (obj == null || typeof obj !== 'object') {
    console.error(Error('L’élément récupéré n’est pas un objet'), obj)
    return null
  }
  return obj
}

export default async function showForm () {
  // conteneur principal
  const parent = document.getElementById('formContainer')
  if (!parent) throw Error('Pas de conteneur #formContainer dans la page')
  empty(parent)
  tab('form')
  // on lui ajoute un div que l’on style (qui sera viré au chargement d’une ressource)
  const conteneur = addElement(parent, 'div')
  const addHr = () => addElement(conteneur, 'hr')

  // ajoute un bouton qui ne soumet pas le form
  const addButton = (parent: HTMLElement, text: string): HTMLButtonElement => addElement(parent, 'button', {
    textContent: text,
    type: 'button'
  })

  const labelProps = { style: { fontWeight: 'bold', verticalAlign: 'top' } }
  const textAreaProps = { rows: 20 }

  // une liste de liens
  const divLinks = addElement(conteneur, 'div', { className: 'rmq', style: { marginBottom: '20px' } })
  // des liens vers nos "outils" de dev
  addElement(divLinks, 'a', {
    textContent: 'Voir la liste des sections utilisées en production', href: 'liste.html', target: 'list'
  })
  addElement(divLinks, 'a', {
    textContent: 'Voir les stats Labomep',
    href: 'https://j3p.sesamath.net/stats.html',
    target: 'stats',
    style: { marginLeft: '2em' }
  })
  addElement(divLinks, 'a', {
    textContent: 'Voir la liste des dernier changements',
    href: 'https://forge.apps.education.fr/sesamath/sesaparcours/-/raw/main/CHANGELOG.md',
    target: 'changelog',
    style: { marginLeft: '2em' }
  })
  addElement(divLinks, 'a', {
    textContent: 'log du dernier déploiement en pré-prod',
    href: 'https://j3p.sesamath.dev/build/deploy.dev.log',
    target: 'log',
    style: { marginLeft: '2em' }
  })
  addElement(divLinks, 'a', {
    textContent: 'log test loadAll (pré-prod)',
    href: 'https://j3p.sesamath.dev/build/loadAll.log',
    target: 'log',
    style: { marginLeft: '2em' }
  })
  addElement(divLinks, 'a', {
    textContent: 'log du dernier déploiement en prod',
    href: 'https://j3p.sesamath.net/build/deploy.dev.log',
    target: 'log',
    style: { marginLeft: '2em' }
  })
  addElement(divLinks, 'a', {
    textContent: 'log test loadAll (prod)',
    href: 'https://j3p.sesamath.net/build/loadAll.log',
    target: 'log',
    style: { marginLeft: '2em' }
  })

  // un div pour le titre
  addElement(conteneur, 'h1', { textContent: 'Tester une ressource' })

  // des liens vers des exemples
  const exemples = addElement(conteneur, 'p', { className: 'rmq', textContent: 'Exemples v2 :' })
  const addExemple = (label: string, graph: GraphJson) => {
    addElement(exemples, 'a', {
      textContent: `edit ${label}`,
      href: '#action=edit&version=2&debug=1&graph=' + encodeURIComponent(JSON.stringify(graph))
    })
    addElement(exemples, 'a', {
      textContent: `play ${label}`,
      href: '#action=play&version=2&debug=1&graph=' + encodeURIComponent(JSON.stringify(graph))
    })
  }
  addExemple('test', graphTest)
  addExemple('ex1', graphEx1)
  addExemple('ex2', graphEx2)
  addExemple('ex4', graphEx4)
  // mix v1/v2
  addExemple('mix v1/v2', graphMix)
  // avec donneesPersistantes
  addExemple('v2 puis v1 avec données persistantes', graphExFcts)

  // avec lastResultat
  addTextContent(exemples, 'reprise avec un lastResultat (ex1234)')
  addElement(exemples, 'a', {
    textContent: 'play exReprise',
    href: '#action=play&version=2&graph=' + encodeURIComponent(JSON.stringify(graphReprise)) + '&resultat=' + encodeURIComponent(JSON.stringify(lastResultat))
  })

  addElement(exemples, 'a', {
    textContent: 'afficher un résultat',
    href: '#action=show&version=2&resultat=' + encodeURIComponent(JSON.stringify(legacyResultat1))
  })

  // fin des liens exemples

  addHr()

  // chargement ressource (par rid)
  const formRid = addElement(conteneur, 'form', { target: '#' })
  const labelRid = addElement(formRid, 'label')
  addElement(labelRid, 'strong', { textContent: 'Ressource' })
  const inputRid = addElement(labelRid, 'input', { style: { margin: '0 1em' } })
  const getRid = () => {
    const rid = inputRid.value.trim()
    return rid.includes('/') ? rid : `sesabibli/${rid}`
  }
  addButton(formRid, 'Play v1')
    .addEventListener('click', () => {
      const rid = getRid()
      if (rid) window.location.hash = `#action=play&version=1&rid=${rid}`
    })
  addButton(formRid, 'Play v2')
    .addEventListener('click', () => {
      const rid = getRid()
      if (rid) window.location.hash = `#action=play&version=2&rid=${rid}`
    })
  addButton(formRid, 'Edit v1')
    .addEventListener('click', () => {
      const rid = getRid()
      if (rid) window.location.hash = `#action=edit&version=1&rid=${rid}`
    })
  addButton(formRid, 'Edit v2')
    .addEventListener('click', () => {
      const rid = getRid()
      if (rid) window.location.hash = `#action=edit&version=2&rid=${rid}`
    })
  const linkProdListener = (event: MouseEvent): void => {
    if (!isElt(event.currentTarget, 'a')) return
    event.currentTarget.href = event.currentTarget.href.replace(/#.*/, '#') + getRid()
  }
  // les deux liens pour tester "comme en prod"
  addElement(formRid, 'a', {
    content: 'idem prod v1',
    href: 'prod1.html#',
    target: 'prod',
    style: { margin: '0 1rem' }
  })
    .addEventListener('click', linkProdListener)
  addElement(formRid, 'a', { content: 'idem prod v2', href: 'prod2.html#', target: 'prod' })
    .addEventListener('click', linkProdListener)

  addElement(formRid, 'br')
  const span = addElement(formRid, 'span', {
    className: 'rmq',
    textContent: '(Mettre un identifiant, préfixer avec sesacommun/ pour les ressources de '
  })
  addElement(span, 'a', { href: 'https://commun.sesamath.net', textContent: 'commun.sesamath.net', target: 'commun' })
  addTextContent(span, ', sinon c’est sesabibli/ par défaut pour ')
  addElement(span, 'a', { href: 'https://bibliotheque.sesamath.net', textContent: 'bibliotheque.sesamath.net', target: 'bibli' })
  addTextContent(span, ')')
  // le form peut quand même être soumis avec la touche entrée
  formRid.addEventListener('submit', (event) => {
    // on veut pas que le navigateur POST ou GET, on change l’url nous-même ci-dessous
    event.preventDefault()
  })

  addHr()

  // chargement section
  const formSection = addElement(conteneur, 'form')
  const labelSection = addElement(formSection, 'label', { textContent: 'Section', style: { fontWeight: 'bold' } })
  const inputSection = addElement(labelSection, 'input', { style: { margin: '1em' } })
  inputSection.focus()
  const getGraphStr = () => {
    const section = inputSection.value.trim()
    return section ? JSON.stringify({ nodes: { n1: { section } } }) : ''
  }
  addButton(formSection, 'Play v1')
    .addEventListener('click', () => {
      const g = getGraphStr()
      if (g) window.location.hash = `#action=play&version=1&graph=${g}`
    })
  addButton(formSection, 'Play v2')
    .addEventListener('click', () => {
      const g = getGraphStr()
      if (g) window.location.hash = `#action=play&version=2graph=${g}`
    })
  addButton(formSection, 'Edit v1')
    .addEventListener('click', () => {
      const g = getGraphStr()
      if (g) window.location.hash = `#action=edit&version=1&graph=${g}`
    })
  addButton(formSection, 'Edit v2')
    .addEventListener('click', () => {
      const g = getGraphStr()
      if (g) window.location.hash = `#action=edit&version=2&graph=${g}`
    })
  // ça c’est pour réagir à la touche entrée
  formSection.addEventListener('submit', (event) => {
    event.preventDefault()
  })

  const boutonListeSections = addButton(conteneur, 'Liste des sections')
  // au clic on affiche ou masque la liste (et change le label du bouton)
  boutonListeSections.addEventListener('click', () => {
    if (divsections.style.display === 'none') {
      divsections.style.display = 'block'
      boutonListeSections.innerText = 'Masquer liste'
    } else {
      divsections.style.display = 'none'
      boutonListeSections.innerText = 'Afficher la liste'
    }
    if (!divsections.childNodes.length) {
      // 1er clic, on charge la liste
      const waitMsg = addElement(divsections, 'span', 'Chargement en cours…')
      loadListe().then(files => {
        // on vire le message de chargement
        divsections.removeChild(waitMsg)

        // input pour filtrer
        const filterContainer = addElement(divsections, 'p', {
          textContent: 'filtrer avec : ', style: { fontSize: '0.8em' }
        })
        const filterInput = addElement(filterContainer, 'input')
        addElement(filterContainer, 'span', {
          textContent: ' (plusieurs possibles, 3 car min, séparer avec espace ou , ou ;)',
          style: { fontSize: '0.8em' }
        })
        // son listener
        let timerId: number | undefined
        const onChange = () => {
          const filters = filterInput.value.trim().split(/[\s,;]/).filter(f => f.length > 2)
          divsections.querySelectorAll('div').forEach(div => {
            div.style.display = (!filters.length || filters.every(filter => div.innerText.includes(filter))) ? 'block' : 'none'
          })
        }
        const listener = () => {
          if (timerId) clearTimeout(timerId)
          // avec ts faut utiliser window.setTimeout sinon il râle avec
          // TS2322: Type 'Timeout' is not assignable to type 'number'
          // et il faudrait définir `type Timeout = ReturnType<typeof setTimeout>`
          // cf https://github.com/Microsoft/TypeScript/issues/30128
          timerId = window.setTimeout(onChange, 200)
        }
        filterInput.addEventListener('change', listener)
        filterInput.addEventListener('keydown', listener)

        // liste des sections, avec le chemin complet (nom de la section en gras)
        for (const file of files) {
          const chunks = /^(.*section)([^.]+)\.js$/.exec(file)
          if (chunks) {
            const [, startFile, section] = chunks
            if (section != null) {
              const div = addElement(divsections, 'div', startFile)
              addElement(div, 'strong', section)
              div.appendChild(document.createTextNode('.js'))
              // on ajoute la section en propriété, pour les listener
              div.addEventListener('mouseover', () => {
                div.style.color = '#F00'
                div.style.cursor = 'pointer'
              }, false)
              div.addEventListener('mouseout', () => {
                div.style.color = '#000'
              }, false)
              div.addEventListener('click', () => {
                inputSection.value = section
              }, false)
            }
          }
        }
      }).catch(showError)
    }
  }, false)

  // contiendra la liste des noms des sections, si on la charge
  const divsections = addElement(conteneur, 'div', {
    style: {
      marginTop: '20px',
      marginBottom: '10px',
      border: '2px #FFF solid',
      padding: '10px',
      background: '#FFF',
      width: '900px',
      height: '200px',
      overflow: 'auto',
      display: 'none' // ce div des noms des sections n’est pas affiché par défaut
    }
  })

  addHr()

  // textarea du graphe
  const formGraphe = addElement(conteneur, 'form')
  formGraphe.addEventListener('submit', preventSubmit)
  const labelGraphe = addElement(formGraphe, 'label', { textContent: 'Graphe', ...labelProps })
  addRichElt(labelGraphe, {
    tag: 'span',
    content: 'Un graphe, au format %{v1} ou %{v2}, par exemple : ',
    contentOptions: {
      v1: {
        tag: 'a',
        content: 'v1',
        attrs: { href: 'https://j3p.sesamath.dev/tsdoc/types/lib_types.LegacyGraph.html', target: 'doc' }
      },
      v2: {
        tag: 'a',
        content: 'v2',
        attrs: { href: 'https://j3p.sesamath.dev/tsdoc/interfaces/lib_types._internal_.GraphJson.html', target: 'doc' }
      }

    },
    props: { className: 'rmq', style: { marginLeft: '2rem' } }
  })
  const pasteBienEcrire = addElement(labelGraphe, 'button', {
    textContent: 'bienEcrire'
  })
  pasteBienEcrire.addEventListener('click', async () => {
    grapheElt.value = JSON.stringify(grapheBienEcrire)
  })
  addElement(labelGraphe, 'br')
  const grapheElt = addElement(labelGraphe, 'textarea', textAreaProps)
  const graphToHash = (action: string, version: string): void => {
    try {
      const graphValues = getObj(grapheElt)
      if (graphValues) {
        const graph = Array.isArray(graphValues) ? new Graph(convertGraph(graphValues)) : new Graph(graphValues)
        window.location.hash = `#action=${action}&version=${version}&graph=${encodeURIComponent(JSON.stringify(graph.serialize()))}`
      }
    } catch (error) {
      console.error(error)
      showError('Graphe invalide')
    }
  }
  addElement(formGraphe, 'br')
  addButton(formGraphe, 'Play v1')
    .addEventListener('click', graphToHash.bind(null, 'play', '1'))
  addButton(formGraphe, 'Play v2')
    .addEventListener('click', graphToHash.bind(null, 'play', '2'))
  addButton(formGraphe, 'Edit v1')
    .addEventListener('click', graphToHash.bind(null, 'edit', '1'))
  addButton(formGraphe, 'Edit v2')
    .addEventListener('click', graphToHash.bind(null, 'edit', '2'))

  addHr()

  // lastResultat
  const formLastResultat = addElement(conteneur, 'form')
  formLastResultat.addEventListener('submit', preventSubmit)
  const labelLastResultat = addElement(formLastResultat, 'label', { textContent: 'Résultat', ...labelProps })
  addElement(labelLastResultat, 'span', {
    content: 'Un résultat précédent renvoyé par Sésaparcours (objet avec une propriété contenu), pour tester une reprise de graphe ou voir l’affichage du parcours. par exemple celui-ci :',
    className: 'rmq',
    style: { marginLeft: '2rem' }
  })
  const pasteResultatV1 = addElement(labelLastResultat, 'button', { textContent: 'Un résultat avec sections V1' })
  pasteResultatV1.addEventListener('click', () => {
    lastResultatElt.value = JSON.stringify(resultatV1NonFini)
  })
  addElement(labelLastResultat, 'span', {
    content: ' ou celui-là : ',
    className: 'rmq',
    style: { marginLeft: '2rem' }
  })
  const pasteResultatV2 = addElement(labelLastResultat, 'button', { textContent: 'Un résultat avec sections V2' })
  pasteResultatV2.addEventListener('click', () => {
    lastResultatElt.value = JSON.stringify(resultatsGrapheV2NonFini)
  })
  addElement(labelLastResultat, 'span', {
    content: ' ou un résultat de parcours terminé : ',
    className: 'rmq',
    style: { marginLeft: '2rem' }
  })
  const pasteResultatFini = addElement(labelLastResultat, 'button', { textContent: 'une section V1 fini' })
  pasteResultatFini.addEventListener('click', () => {
    lastResultatElt.value = JSON.stringify(unResultatGraphFini)
  })
  const lastResultatElt = addElement(labelLastResultat, 'textarea', textAreaProps)

  const resultToHash = async (action: string, version: string): Promise<void> => {
    try {
      const resultat = getObj(lastResultatElt) as Resultat | LegacyResultat
      if (resultat) {
        const graphValues = isLegacyResultat(resultat)
          ? resultat.contenu.graphe
          : resultat.contenu.pathway.graph
        window.location.hash = `#action=${action}&version=${version}&graph=${encodeURIComponent(JSON.stringify(graphValues))}&resultat=${encodeURIComponent(JSON.stringify(resultat))}`
      }
    } catch (error) {
      console.error(error)
      showError('Résultat invalide')
    }
  }
  addElement(formLastResultat, 'br')
  addButton(formLastResultat, 'Play v1')
    .addEventListener('click', resultToHash.bind(null, 'play', '1'))
  addButton(formLastResultat, 'Play v2')
    .addEventListener('click', resultToHash.bind(null, 'play', '2'))
  addButton(formLastResultat, 'Show1')
    .addEventListener('click', resultToHash.bind(null, 'show', '1'))
  addButton(formLastResultat, 'Show2')
    .addEventListener('click', resultToHash.bind(null, 'show', '2'))

  // pour les tests de migration v2
  addHr()
  const ulMig = addElement(conteneur, 'ul')
  addElement(ulMig, 'h3', { content: 'Quelques tests pour la migration v2' })
  // on pioche parmi les sections populaires de https://j3p.sesamath.net/stats.html
  // et on prend ensuite un rid dans http://localhost:8081/liste.html#{section}
  const tests = {
    affineounon: 'sesabibli/49576',
    probabiliteCalculSimple: 'sesabibli/5cbd6af84bb1527df9236c07',
    squelettemtg32_Calc_Param_1_Edit: 'sesabibli/49574',
    ex_Calc_Multi_Edit: 'sesabibli/600d8ed72eb94c218b3657ce',
    // squelettemtg32_Calc_Param_MathQuill:'',
    // squelettemtg32_Deriv_Param:'',
    // ex_Calc_Multi_Edit_Multi_Etapes:'',
    squelettemtg32_Calc_Param_Multi_Edit: 'sesabibli/32532',
    // squelettemtg32_Fact_Param:'',
    squelettemtg32_Eq_Param_MathQuill: 'sesabibli/5c9758b4d9e4237dfac484ef',
    // squelettemtg32_Calc_Param_Affine_MathQuill:'',
    formules02: 'sesabibli/5ba62178d31cfd05a7eeb140',
    nomme03: 'sesabibli/5c97d5764bb1527df9236b29',
    memoMake: 'sesabibli/64880ca1cf55a4695ca1bb56',
    squelettemtg32_Tracer_Droite1: 'sesabibli/5b3bd68256755e345690d279',
    nomme02: 'sesabibli/5cefbf04a41967021bb68ca8',
    formuleMake: 'sesabibli/64824bd3cf55a4695ca1bb40',
    squelettemtg32_Calc_Param_MathQuill_Sans_Entete: '',
    squelettemtg32_Ineq_Param: '',
    excalcmathquill: '',
    squelettemtg32_Reduire_Eq_Droite_Param: '',
    EtudeFonction_derivee: 'sesabibli/53873',
    conserve01: 'sesabibli/5efe0c6962855041607be86a',
    conversion: 'sesabibli/5c5d51f3cda5e037fe314be6',
    squelettemtg32_Validation_Interne_Param: 'sesabibli/5cc564864bb1527df9236c2a',
    trigo01: 'sesabibli/5e892f792bf01563d3a238ee',
    fractioncalculs: 'sesabibli/5e8064c578fc2263d99415cd',
    fraction06: 'sesabibli/5f7631027166a20ae7ae8e36',
    stat01: 'sesabibli/5de6ca9bcb679c42a81ef902',
    SuiteTermeGeneral: 'sesabibli/91392',
    // FIXME lui a un pb, il apparaît dans stats.html mais on le retrouve pas dans liste.html
    // EtudeFonction_signederivee: '',
    // symetriequad: '',
    // puissancesSimple: '',
    natureSuite: 'sesabibli/91400',
    // pixelMake: '',
    // EtudeFonction_variations: '',
    // poseoperation: '',
    // multiplevoc: '',
    501: 'sesabibli/32455'
    // squelettemtg32_Calc_Frac: '',
    // proportionnalite02: '',
    // squelettemtg32_Tracer_Droite2: ''
  }
  for (const [section, rid] of Object.entries(tests)) {
    const li = addElement(ulMig, 'li', { content: section })
    addElement(li, 'a', { href: `prod2.html#${rid}`, content: rid, target: 'prod', style: { marginLeft: '1rem' } })
  }
}
