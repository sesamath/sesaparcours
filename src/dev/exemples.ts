import Graph, { type GraphJson } from 'src/lib/entities/Graph'
import type { ParamsValues } from 'src/lib/entities/GraphNode'
import type { LegacyGraph, Resultat } from 'src/lib/types'

export const graphReprise: GraphJson = {
  nodes: {
    1: {
      section: 'Exemple1',
      label: 'Nœud 1',
      title: '',
      maxRuns: 3,
      connectors: [
        {
          target: '2',
          feedback: '',
          label: '',
          typeCondition: 'none'
        }
      ],
      params: {
        unCarre: 25,
        desCarres: [
          1,
          4,
          9
        ],
        quadrilateres: [
          'carré',
          'rectangle',
          'losange'
        ],
        nbQuestions: 5,
        maxTries: 1,
        nbSteps: 1
      },
      position: {
        x: 210,
        y: 50
      }
    },
    2: {
      section: 'Exemple3',
      label: 'Nœud 2',
      title: '',
      maxRuns: 3,
      connectors: [
        {
          target: '3',
          feedback: '',
          label: '',
          typeCondition: 'none'
        }
      ],
      params: {
        deuxPairesDeDes: [
          4,
          12,
          8,
          6
        ],
        nbQuestions: 1,
        maxTries: 1,
        nbSteps: 1
      },
      position: {
        x: 0,
        y: 150
      }
    },
    3: {
      section: 'Exemple4',
      label: 'Nœud 3',
      title: '',
      maxRuns: 3,
      connectors: [
        {
          target: '4',
          feedback: '',
          label: '',
          typeCondition: 'none'
        }
      ],
      params: {
        maxTries: 1,
        nbQuestions: 3,
        nbrepetitions: 1,
        nbSteps: 2
      },
      position: {
        x: 210,
        y: 200
      }
    },
    4: {
      section: 'Exemple3',
      label: 'Nœud 4',
      title: '',
      maxRuns: 3,
      connectors: [
        {
          target: '5',
          feedback: '',
          label: '',
          typeCondition: 'none'
        }
      ],
      params: {
        deuxPairesDeDes: [
          4,
          12,
          8,
          6
        ],
        nbQuestions: 1,
        maxTries: 1,
        nbSteps: 1
      },
      position: {
        x: 0,
        y: 300
      }
    },
    5: {
      section: 'Exemple4',
      label: 'Nœud 5',
      title: '',
      maxRuns: 3,
      connectors: [
        {
          target: '6',
          feedback: '',
          label: '',
          typeCondition: 'none'
        }
      ],
      params: {
        maxTries: 1,
        nbQuestions: 3,
        nbrepetitions: 1,
        nbSteps: 2
      },
      position: {
        x: 210,
        y: 350
      }
    },
    6: {
      section: 'Exemple3',
      label: 'Nœud 6',
      title: '',
      maxRuns: 3,
      connectors: [
        {
          target: 'fini',
          feedback: '',
          label: '',
          typeCondition: 'none'
        }
      ],
      params: {
        deuxPairesDeDes: [
          4,
          12,
          8,
          6
        ],
        nbQuestions: 1,
        maxTries: 1,
        nbSteps: 1
      },
      position: {
        x: 420,
        y: 300
      }
    },
    fini: {
      section: '',
      label: 'Nœud 7',
      title: '',
      maxRuns: 3,
      connectors: [],
      params: {},
      position: {
        x: 0,
        y: 450
      }
    }
  },
  startingId: '1'
}

const params1 = {
  unCarre: 25,
  desCarres: [1, 4, 9],
  quadrilateres: ['carré', 'rectangle', 'losange'],
  nbQuestions: 5,
  maxTries: 1,
  nbSteps: 1
}

export const grapheBienEcrire: LegacyGraph = [
  [
    '1',
    'bienecrire',
    [
      {
        nn: '3',
        score: '>=0.6',
        conclusion: 'Bon travail , on passe au chronomètre !'
      },
      {
        nn: '4',
        pe: 'pe_7,pe_10',
        conclusion: 'Il faut retravailler les espaces',
        sconclusion: 'Graphe terminé. Pense aux groupes de 3.',
        snn: '2',
        maxParcours: 2
      },
      {
        nn: '5',
        pe: 'pe_6,pe_9',
        conclusion: 'Il faut faire attention aux zéros inutiles',
        sconclusion: 'Graphe terminé. Pense aux zéros inutiles',
        snn: '2',
        maxParcours: 2
      },
      {
        nn: '1',
        score: '<=0.6',
        conclusion: 'Il faut encore s’entraîner',
        sconclusion: 'Travail terminé. Il faudra encore s’entraîner',
        snn: '2',
        max: 3
      },
      {
        nn: '2',
        score: 'sans condition',
        conclusion: ''
      },
      {
        nbrepetitions: 3,
        limite: 0,
        m: true,
        L: false,
        g: false,
        a: false,
        mcarre: false,
        mcube: false,
        Changement: 'jamais',
        Tableau: 'Virgule_Fleches',
        ZerosInutiles: 'Non_Acceptés',
        Espace: false,
        MiseEnPagePlusClaire: false,
        indication: '',
        nbchances: 2,
        Travail: 'Espaces_Zéros',
        Partie: 'Entière',
        Mode: 'Clavier',
        MortSubite: true
      }
    ]
  ],
  [
    '2',
    'fin'
  ],
  [
    '3',
    'bienecrire',
    [
      {
        nn: '2',
        score: '>=0.6',
        conclusion: 'Travail terminé, tu es parvenu au dernier niveau !'
      },
      {
        nn: '6',
        pe: 'pe_7,pe_10',
        conclusion: 'Il faut retravailler les espaces en chaque groupe de 3',
        sconclusion: 'Travail terminé, tu es parvenu au dernier niveau !',
        snn: '2',
        maxParcours: 2
      },
      {
        nn: '7',
        pe: 'pe_6,pe_9',
        conclusion: 'Il faut surveiller les zéros inutiles',
        sconclusion: 'Travail terminé, tu es parvenu au dernier niveau !',
        snn: '2',
        maxParcours: 2
      },
      {
        nn: '3',
        score: '<=0.6',
        conclusion: 'Il faut encore d’entrainer',
        sconclusion: 'Travail terminé, tu es parvenu au dernier niveau !',
        snn: '2',
        max: 3
      },
      {
        nn: '2',
        score: 'sans condition',
        conclusion: ''
      },
      {
        nbrepetitions: 3,
        indication: '',
        limite: 20,
        nbchances: 2,
        entier: '[2;100]',
        b_accolades: false,
        b_avecaide: true,
        b_avecpremiers: true,
        Travail: 'Espaces_Zéros',
        Partie: 'Entière',
        Mode: 'Click',
        MortSubite: true,
        MiseEnPagePlusClaire: false
      }
    ]
  ],
  [
    '4',
    'bienecrire',
    [
      {
        nn: '1',
        score: '>=0.6',
        conclusion: 'Bien , réessayons plus dur'
      },
      {
        nn: '4',
        score: '<=0.6',
        conclusion: 'Essaye encore',
        sconclusion: 'Travail terminé, Il faudra encore s’entrainer !',
        snn: '2',
        max: 2
      },
      {
        nn: '2',
        score: 'sans condition',
        conclusion: ''
      },
      {
        nbrepetitions: 3,
        indication: '',
        limite: 0,
        nbchances: 2,
        b_accolades: false,
        Travail: 'Espaces',
        Partie: 'Entière',
        Mode: 'Click',
        MortSubite: true,
        MiseEnPagePlusClaire: false
      }
    ]
  ],
  [
    '5',
    'bienecrire',
    [
      {
        nn: '1',
        score: '>=0.6',
        conclusion: 'Bien , réessayons plus dur'
      },
      {
        nn: '5',
        score: '<=0.6',
        conclusion: 'Essayons une dernière fois',
        sconclusion: 'Travail terminé, Il faudra encore s’entrainer !',
        snn: '2',
        max: 2
      },
      {
        nn: '2',
        score: 'sans condition',
        conclusion: ''
      },
      {
        nbrepetitions: 3,
        indication: '',
        limite: 0,
        nbchances: 2,
        entier: '[2;100]',
        Travail: 'Zéros',
        Partie: 'Entière',
        Mode: 'Click',
        MortSubite: true,
        MiseEnPagePlusClaire: false
      }
    ]
  ],
  [
    '6',
    'bienecrire',
    [
      {
        nn: '3',
        score: '>=0.6',
        conclusion: 'Bien , réessayons plus dur'
      },
      {
        nn: '6',
        score: '<=0.6',
        conclusion: 'Essaye encore',
        sconclusion: 'Travail terminé, tu es parvenu au dernier niveau !',
        snn: '2',
        max: 2
      },
      {
        nn: '2',
        score: 'sans condition',
        conclusion: ''
      },
      {
        nbrepetitions: 3,
        indication: '',
        limite: 0,
        nbchances: 2,
        Travail: 'Espaces',
        Partie: 'Entière',
        Mode: 'Click',
        MortSubite: true,
        MiseEnPagePlusClaire: false
      }
    ]
  ],
  [
    '7',
    'bienecrire',
    [
      {
        nn: '3',
        score: '>=0.6',
        conclusion: 'Essayons plus dur.'
      },
      {
        nn: '7',
        score: '<=0.6',
        conclusion: 'Essaye encore',
        sconclusion: 'Travail terminé, tu es parvenu au dernier niveau !',
        snn: '2',
        max: 2
      },
      {
        nn: '2',
        score: 'sans condition',
        conclusion: ''
      },
      {
        nbrepetitions: 3,
        indication: '',
        limite: 0,
        nbchances: 2,
        entier: '[2;100]',
        Travail: 'Zéros',
        Partie: 'Entière',
        Mode: 'Click',
        MortSubite: true,
        MiseEnPagePlusClaire: false
      }
    ]
  ]
]

export const graphEx1: GraphJson = {
  nodes: {
    n1: {
      section: 'Exemple1',
      label: 'exemple 1',
      title: 'exemple 1',
      maxRuns: 1,
      params: params1,
      connectors: [
        {
          target: 'Fin',
          label: 'Rang 1',
          typeCondition: 'none',
          feedback: 'C\'est parti'
        }
      ]
    },
    Fin: {
      section: '',
      label: 'Fin'
    }
  },
  startingId: 'n1'
}

export const graphEx2: GraphJson = {
  nodes: {
    1: {
      section: 'Exemple2',
      params: {},
      label: 'Un exemple de section V2',
      maxRuns: 1,
      title: 'exemple 2',
      connectors: [{
        target: '2',
        label: 'Rang 1',
        feedback: 'Terminé !',
        typeCondition: 'none'
      }]
    },
    2: {
      label: 'Fin',
      section: ''
    }
  },
  startingId: '1'
}

export const graphTest: GraphJson = {
  nodes: {
    1: {
      section: 'TestMathlive',
      params: { limite: 20 },
      label: 'Test de clavier',
      maxRuns: 1,
      title: 'Tests à JC',
      connectors: [{
        target: '2',
        label: 'Rang 1',
        feedback: 'Passons à l’exercice suivant',
        typeCondition: 'none'
      }],
      position: { x: 80, y: 150 }
    },
    2: {
      section: 'Exemple4',
      title: '',
      maxRuns: 1,
      label: 'Exemple4',
      params: {
        nbQuestions: 1,
        maxTries: 1,
        nbSteps: 1,
        maxRuns: 1
      },
      connectors: [{
        target: '3',
        label: 'Rang 1',
        feedback: '',
        typeCondition: 'none'
      }],
      position: { x: 200, y: 300 }
    },
    3: {
      label: 'Fin',
      section: '',
      position: { x: 400, y: 150 }
    }
  },
  startingId: '1'
}

const params2: ParamsValues = {
  nbQuestions: 1,
  maxTries: 1,
  nbSteps: 1,
  donneesPrecedentes: true
}

const params3: ParamsValues = {
  maxTries: 1,
  nbQuestions: 1,
  nbStep: 1
}

// En premier noeud une section V2 pour choisir une fonction et en deuxième noeud EtudeFonction_derivee qui reprend la fonction choisie.
export const graphExFcts: GraphJson = {
  nodes: {
    1: {
      section: 'ChoisisTaFonction',
      label: 'ChoisisTaFonction',
      title: 'ChoisisTaFonction',
      maxRuns: 6,
      params: params3,
      connectors: [{
        target: '2',
        label: 'Rang 1',
        feedback: 'Passons à l’étude de cette fonction',
        typeCondition: 'none'
      }]
    },
    2: {
      section: 'EtudeFonction_derivee',
      label: 'EtudeFonction_derivee',
      title: 'EtudeFonction_derivee',
      maxRuns: 6,
      params: params2,
      connectors: [{
        target: '3',
        label: 'Rang 1',
        feedback: 'Passons à la question suivante',
        typeCondition: 'none'
      }]
    },
    3: {
      section: 'EtudeFonction_signederivee',
      label: 'EtudeFonction_signederivee',
      title: 'EtudeFonction_signederivee',
      maxRuns: 6,
      params: params2,
      connectors: [{
        target: '4',
        label: 'Rang 1',
        feedback: 'Passons à la question suivante',
        typeCondition: 'none'
      }]
    },
    4: {
      section: 'EtudeFonction_limite',
      label: 'EtudeFonction_limite',
      title: 'EtudeFonction_limite',
      maxRuns: 6,
      params: params2,
      connectors: [{
        target: '5',
        label: 'Rang 1',
        feedback: 'Passons à la question suivante',
        typeCondition: 'none'
      }]
    },
    5: {
      section: 'EtudeFonction_variations',
      label: 'EtudeFonction_variations',
      title: 'EtudeFonction_variations',
      maxRuns: 6,
      params: params2,
      connectors: [{
        target: '6',
        label: 'Rang 1',
        feedback: 'Passons à la question suivante',
        typeCondition: 'none'
      }]
    },
    6: {
      section: 'ChoisisTaFonction',
      label: 'ChoisisTaFonction',
      title: 'ChoisisTaFonction',
      maxRuns: 6,
      params: params3,
      connectors: [{
        target: '7',
        label: 'Rang 1',
        feedback: 'Passons à l’étude de cette fonction',
        typeCondition: 'none'
      }]
    },
    7: {
      section: 'EtudeFonction_derivee',
      label: 'EtudeFonction_derivee',
      title: 'EtudeFonction_derivee',
      maxRuns: 6,
      params: params2,
      connectors: [{
        target: 'fini',
        label: 'Rang 1',
        feedback: 'Terminé',
        typeCondition: 'none'
      }]
    },
    fini: {
      section: '',
      label: 'Fin'
    }
  },
  startingId: '1'
}

export const graphMix: GraphJson = {
  nodes: {
    // section v1
    n1: {
      section: 'Exemple',
      label: 'Exemple',
      title: 'Nœud 1',
      maxRuns: 1,
      params: params3,
      connectors: [{
        target: 'n2',
        label: 'Rang 1',
        feedback: 'Passons à l’exercice suivant',
        typeCondition: 'none'
      }]
    },
    // section v2
    n2: {
      section: 'Exemple3',
      label: 'Exemple3',
      title: 'Nœud 2',
      maxRuns: 1,
      params: params3,
      connectors: [{
        feedback: 'C’est déjà terminé',
        label: 'Label qcq',
        typeCondition: 'none',
        target: 'fin'
      }]
    },
    fin: {
      section: '',
      label: 'Fin'
    }
  },
  startingId: 'n1'
}

export const graphEx4: GraphJson = {
  nodes: {
    n1: {
      section: 'Exemple4',
      label: 'Exemple4',
      title: '',
      maxRuns: 1,
      params: {
        maxTries: 1,
        nbQuestions: 3,
        nbSteps: 2
      },
      connectors: [{
        typeCondition: 'none',
        target: 'fin',
        label: '',
        feedback: ''
      }]
    },
    fin: {
      section: '',
      label: 'fin'
    }
  },
  startingId: 'n1'
}

// On prend le graphMix pour produire un pathway avec des résultats bidons pour tester viewer
const graphForPathway = new Graph(graphMix)
export const pathwayEx1 = {
  graph: graphForPathway,
  currentNode: graphForPathway.getNode('n2'),
  /** Les résultats des nœuds précédents */
  results: [
    {
      id: 'n2',
      score: 0.7,
      evaluation: 'Erreur de virgule'
    },
    {
      id: '1',
      score: 0.1,
      evaluation: ''
    }
  ],
  /** La liste des branchements utilisés, par id du nœud de départ */
  nbRuns: { 2: 1, 3: 1 }
}
export const partialPathway =
  {
    graph: { nodes: {}, startingId: '1' },
    startedAt: '2024-02-07T16:19:55.402Z',
    currentNode: {
      id: '4',
      section: 'Exemple3',
      label: 'Nœud 4',
      title: '',
      maxRuns: 3,
      connectors: [{
        target: '5',
        feedback: '',
        label: '',
        scoreComparator: '>=',
        _scoreRef: 0,
        _typeCondition: 'none',
        peRefs: [],
        nbRuns: 3,
        source: '4'
      }],
      params: { deuxPairesDeDes: [4, 12, 8, 6], nbQuestions: 1, maxTries: 1, nbSteps: 1 },
      position: { x: 0, y: 0 }
    },
    results: [
      { id: '1', score: 1, evaluation: '' },
      { id: '2', score: 1, evaluation: 'parfait' },
      { id: '3', score: 1, evaluation: 'parfait' }],
    nbRuns: {}
  }

export const lastResultat: Resultat = {
  contenu: {
    pathway: {
      graph: graphReprise,
      startedAt: '2024-02-12T16:21:43.456Z',
      currentNodeId: '4',
      results: [
        {
          id: '1',
          score: 1,
          evaluation: '',
          duree: 3
        },
        {
          id: '2',
          score: 0,
          evaluation: 'faux',
          duree: 2
        },
        {
          id: '3',
          score: 1,
          evaluation: 'parfait',
          duree: 4
        }
      ],
      nbRuns: {},
      persistentStorage: {}
    }
  },
  date: '2024-02-12T16:21:43.456Z',
  duree: 71,
  fin: false,
  score: 0.6666666666666666,
  reponse: '',
  type: 'j3p'
}

/**
 * Un résultat v1 qui existe réellement en base
 * On ne peut pas le typer en LegacyResultat à cause des pe numérique et du premier nœud qui est un array vide
 */
export const resultatV1NonFini = {
  rid: 'sesabibli/599bd690b4649c7d956f6167',
  type: 'j3p',
  date: '17/12/2023 11:52',
  duree: 865,
  fin: false,
  score: 0.6666666666666666,
  reponse: '',
  contenu: {
    noeuds: [
      '1',
      '2',
      '3',
      '4',
      '5',
      '6'
    ],
    pe: [
      0,
      1,
      1,
      1,
      1,
      0
    ],
    scores: [
      0,
      1,
      1,
      1,
      1,
      0
    ],
    ns: [
      2,
      3,
      4,
      5,
      6,
      7
    ],
    boucle: [
      1,
      1,
      1,
      1,
      1,
      1
    ],
    nextId: '7',
    boucleGraphe: {
      1: [],
      2: [],
      3: [],
      4: [],
      5: [],
      6: []
    },
    editgraphes: {
      positionNodes: [
        [
          70,
          70
        ],
        [
          210,
          300
        ],
        [
          420,
          70
        ],
        [
          560,
          300
        ],
        [
          700,
          70
        ],
        [
          840,
          300
        ],
        [
          980,
          70
        ],
        [
          1120,
          300
        ]
      ],
      titreNodes: [
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
      ]
    },
    graphe: [
      [],
      [
        '1',
        'squelettemtg32_Fact_Param',
        [
          {
            nn: '2',
            score: 'sans+condition'
          },
          {
            ex: 'Fact_a_xCarre_Plus_kx',
            a: 'int(rand(0)*12+1)',
            k: 'int(rand(0)*12+2)',
            nbEssais: 5,
            factMax: false,
            nbchances: 2,
            boitedialogue: 'non'
          }
        ]
      ],
      [
        '2',
        'squelettemtg32_Fact_Param',
        [
          {
            nn: '3',
            score: 'sans+condition'
          },
          {
            ex: 'Fact_a_xCarre_Plus_kx',
            a: 'int(rand(0)*12+1)',
            k: '-int(rand(0)*12+2)',
            nbEssais: 5,
            factMax: true,
            nbchances: 2,
            boitedialogue: 'non'
          }
        ]
      ],
      [
        '3',
        'squelettemtg32_Fact_Param',
        [
          {
            nn: '4',
            score: 'sans+condition'
          },
          {
            ex: 'Fact_a_xCarre_Plus_kx',
            a: 'random',
            k: '-1',
            nbEssais: 4,
            factMax: true,
            nbchances: 2,
            boitedialogue: 'non'
          }
        ]
      ],
      [
        '4',
        'squelettemtg32_Fact_Param',
        [
          {
            nn: '5',
            score: 'sans+condition'
          },
          {
            ex: 'Fact_a_xCarre_Plus_kx',
            a: 'int(rand(0)*4+2)',
            k: 'a*(2*int(rand(0)*2)-1)*int(rand(0)*5+1)',
            nbEssais: 5,
            factMax: true,
            nbchances: 2,
            boitedialogue: 'non'
          }
        ]
      ],
      [
        '5',
        'squelettemtg32_Fact_Param',
        [
          {
            nn: '6',
            score: 'sans+condition'
          },
          {
            ex: 'Fact_cxPlusd_axPlusb_Plus_k_axPlusb',
            a: 'int(rand(0)*6+1)',
            b: 'int(rand(0)*6+1)',
            c: 'random',
            d: '0',
            k: '1',
            nbEssais: 6,
            factMax: false,
            nbchances: 2,
            boitedialogue: 'non'
          }
        ]
      ],
      [
        '6',
        'squelettemtg32_Fact_Param',
        [
          {
            nn: '7',
            score: 'sans+condition'
          },
          {
            ex: 'Fact_cxPlusd_axPlusb_Plus_k_axPlusb',
            a: '1',
            b: 'random',
            c: 'random',
            d: 'random',
            k: 'int(rand(0)*7+2)',
            nbEssais: 6,
            factMax: true,
            nbchances: 2,
            boitedialogue: 'non'
          }
        ]
      ],
      [
        '7',
        'squelettemtg32_Fact_Param',
        [
          {
            nn: '8',
            conclusion: 'fin',
            score: 'sans+condition'
          },
          {
            ex: 'Fact_cxPlusd_axPlusb_Moins_k_axPlusb',
            a: 'random',
            b: 'random',
            c: 'random',
            d: 'random',
            k: 'random',
            nbEssais: 6,
            factMax: true,
            nbchances: 2
          }
        ]
      ],
      [
        '8',
        'fin'
      ]
    ],
    bilans: [
      {
        index: 0,
        id: '1',
        pe: 0,
        duree: 17,
        score: 0,
        fin: false,
        nextId: '2',
        ns: 2,
        boucle: 1
      },
      {
        index: 1,
        id: '2',
        pe: 1,
        duree: 21,
        score: 1,
        fin: false,
        nextId: '3',
        ns: 3,
        boucle: 1
      },
      {
        index: 2,
        id: '3',
        pe: 1,
        duree: 215,
        score: 1,
        fin: false,
        nextId: '4',
        ns: 4,
        boucle: 1
      },
      {
        index: 3,
        id: '4',
        pe: 1,
        duree: 60,
        score: 1,
        fin: false,
        nextId: '5',
        ns: 5,
        boucle: 1
      },
      {
        index: 4,
        id: '5',
        pe: 1,
        duree: 127,
        score: 1,
        fin: false,
        nextId: '6',
        ns: 6,
        boucle: 1
      },
      {
        index: 5,
        id: '6',
        pe: 0,
        duree: 386,
        score: 0,
        fin: false,
        nextId: '7',
        ns: 7,
        boucle: 1
      }
    ],
    donneesPersistantes: {}
  },
  resultatId: '1702809508135',
  checksum: '73a1b5',
  sequence: '657694963103a703a6db7189',
  participants: [
    '6564549712a3ab4961ec5969'
  ],
  ressource: {
    rid: 'sesabibli/599bd690b4649c7d956f6167',
    type: 'j3p',
    titre: 'Factorisation par ax+b : Niveau 1'
  },
  autonomie: false,
  oid: '657ecfb512a3ab4961f6bf81',
  $loadState: {},
  $indexRessource: 1,
  $indexSousSequence: 1,
  $reachLast: false,
  $sousSequenceName: 'factoriser',
  $date: '2023-12-17T10:52:53.575Z',
  csvDate: '17/12/2023 11:52'
}

/** un résultat v2 construit manuellement à partir de ce qu'à retourné le player v2 en console */
export const resultatsGrapheV2NonFini = {
  contenu: {
    pathway: {
      graph: {
        nodes: {
          1: {
            section: 'TestMathlive',
            label: 'Test de clavier',
            title: 'Tests à JC',
            maxRuns: 1,
            connectors: [
              {
                feedback: 'Passons à l’exercice suivant',
                typeCondition: 'none',
                label: 'Rang 1',
                target: '2',
                source: '1'
              }
            ],
            params: {},
            position: {
              x: 50,
              y: 50
            }
          },
          2: {
            section: 'Exemple4',
            label: 'Exemple4',
            title: '',
            maxRuns: 1,
            connectors: [
              {
                feedback: '',
                typeCondition: 'none',
                label: 'Rang 1',
                target: '3',
                source: '2'
              }
            ],
            params: {
              nbQuestions: 1,
              maxTries: 1,
              nbSteps: 1,
              maxRuns: 1
            },
            position: {
              x: 200,
              y: 200
            }
          },
          3: {
            section: '',
            label: 'Fin',
            position: {
              x: 400,
              y: 100
            }
          }
        },
        startingId: '1'
      },
      currentNodeId: '2',
      results: [
        {
          id: '1',
          score: 0.5,
          evaluation: '',
          date: '2024-06-06T16:33:08.707Z'
        }
      ],
      nbRuns: {
        1: 1
      },
      startedAt: '"2024-06-06T16:32:52.812Z"',
      persistentStorage: {}
    }
  },
  date: '"2024-06-06T16:32:52.812Z"',
  duree: 16,
  fin: false,
  score: 0.5,
  reponse: '',
  type: 'j3p'
}

export const unGraphA1Node = {
  nodes: {
    1: {
      section: 'decompose01',
      label: 'Nœud 1',
      title: '',
      maxRuns: 3,
      connectors: [
        {
          feedback: 'Fin de l‘activité',
          typeCondition: 'none',
          label: '',
          target: '2'
        }
      ],
      params: {},
      position: {
        x: 70,
        y: 70
      }
    },
    2: {
      section: '',
      label: 'Nœud 2',
      position: {
        x: 250,
        y: 130
      }
    }
  },
  startingId: '1'
}

export const unResultatGraphFini = {
  contenu: {
    pathway: {
      graph: {
        nodes: {
          1: {
            section: 'decompose01',
            label: 'Nœud 1',
            title: '',
            maxRuns: 3,
            connectors: [
              {
                feedback: 'Fin de l‘activité',
                typeCondition: 'none',
                label: '',
                target: '2',
                source: '1'
              }
            ],
            params: {},
            position: {
              x: 70,
              y: 70
            }
          },
          2: {
            section: '',
            label: 'Nœud 2',
            position: {
              x: 250,
              y: 130
            }
          }
        },
        startingId: '1'
      },
      currentNodeId: '2',
      results: [
        {
          id: '1',
          score: 1,
          evaluation: '',
          date: '2024-06-07T08:12:51.239Z'
        }
      ],
      nbRuns: {
        1: 1
      },
      startedAt: '"2024-06-07T08:04:21.288Z"',
      persistentStorage: {}
    }
  },
  date: '"2024-06-07T08:04:21.288Z"',
  duree: 510,
  fin: true,
  score: 1,
  reponse: '',
  type: 'j3p',
  isDebug: false
}
