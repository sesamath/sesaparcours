// le script js chargé par niveaux.html
import('../buildLevelTree')
  .then(({ default: buildLevelTree }) => {
    buildLevelTree()
  })
  .catch(error => {
    console.error(error)
  })
