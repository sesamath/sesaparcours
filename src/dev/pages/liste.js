// le script js chargé par liste.html
import('../buildUsageTree')
  .then(({ default: buildUsageTree }) => {
    buildUsageTree()
  })
  .catch(error => {
    console.error(error)
  })
