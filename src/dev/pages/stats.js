// le script js chargé par stats.html
import('../buildStats')
  .then(({ default: buildStats }) => {
    buildStats()
  })
  .catch(error => {
    console.error(error)
  })
