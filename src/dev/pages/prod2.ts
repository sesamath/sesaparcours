// on est chargé par prod1.html pour être au plus près d’un affichage j3p de prod v1
import { loadBibli } from 'src/lib/core/bibli'
import { isLegacyGraph } from 'src/lib/core/checks'
import { addElement, showError } from 'src/lib/utils/dom/main'

import type Graph from 'src/lib/entities/Graph'
import type { GraphJson } from 'src/lib/entities/Graph'
import type { LegacyResultat, RessourceJ3pParametresV1, Resultat } from 'src/lib/types'

const container = addElement(document.body, 'div')

function resultatCallback (resultat: Resultat | LegacyResultat): void {
  console.info('La ressource retourne le résultat', resultat)
}

async function restart (): Promise<void> {
  if (window.location.hash.length < 4) return
  try {
    const rid = window.location.hash.substring(1)
    if (!/^[a-z0-9]+\/[a-z0-9]+$/.test(rid)) {
      return console.info(`hash ${rid} n’est pas un rid valide`)
    }
    const { ressource, graphe } = await loadBibli(rid)
    let graph: Graph | GraphJson
    if (isLegacyGraph(graphe)) {
      const params = ressource.parametres as RessourceJ3pParametresV1
      const { convertGraph } = await import('src/lib/core/convert1to2')
      graph = convertGraph(graphe, params.editgraphes)
    } else {
      graph = graphe
    }
    if (typeof window.spPlay !== 'function') throw Error('Le chargement a échoué')
    window.spPlay(container, { graph, resultatCallback })
  } catch (error) {
    showError(error)
  }
}

// @ts-ignore il exporte rien mais on l’importe en dynamique quand même !
import('src/lazyLoader')
  .then(() => {
    window.addEventListener('hashchange', restart)
    // et on démarre
    return restart()
  })
  .catch(showError)
