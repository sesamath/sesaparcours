// on est chargé par prod1.html pour être au plus près d’un affichage j3p de prod v1
import { loadBibli } from 'src/lib/core/bibli'
import { isLegacyGraph } from 'src/lib/core/checks'
import { addElement, showError } from 'src/lib/utils/dom/main'

import type { LegacyResultat, Resultat } from 'src/lib/types'

const container = addElement(document.body, 'div')

function resultatCallback (resultat: Resultat | LegacyResultat): void {
  console.info('La ressource retourne le résultat', resultat)
}

async function restart (): Promise<void> {
  if (window.location.hash.length < 4) return
  try {
    const rid = decodeURIComponent(window.location.hash.substring(1))
    let graphe
    if (rid.startsWith('[[')) {
      // c'est un graphe
      try {
        graphe = JSON.parse(rid)
      } catch (error) {
        console.error(error)
        throw Error(`Graphe invalide : ${rid}`)
      }
    } else {
      // c'est un rid par défaut
      if (!/^[a-z0-9]+\/[a-z0-9]+$/.test(rid)) {
        return console.info(`hash ${rid} n’est pas un rid valide`)
      }
      const ressource = await loadBibli(rid)
      graphe = ressource.graphe
    }
    if (!isLegacyGraph(graphe)) throw Error(`La ressource ${rid} n’est pas une ressource v1`)
    if (typeof window.j3pLoad !== 'function') throw Error('Le chargement a échoué')
    window.j3pLoad(container, { graphe, resultatCallback })
  } catch (error) {
    showError(error)
  }
}

// @ts-ignore il exporte rien mais on l’importe en dynamique quand même !
import('src/lazyLoader')
  .then(() => {
    window.addEventListener('hashchange', restart)
    // et on démarre
    return restart()
  })
  .catch(showError)
