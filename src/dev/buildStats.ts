import { addElement } from 'src/lib/utils/dom/main'

/**
 * Utilisé par stats.html pour compiler les stats dispo sur https://labomep.sesamath.net/stats/resultats_YYYY-MM.json
 * @module dev/spBuildStats
 */
type StatsResultatsForType = {
  total: number, rids: Record<string, number>
}

interface StatsResultatsForTypeJ3p extends StatsResultatsForType {
  sections: Record<string, number>
}

interface StatsResultats extends StatsResultatsForTypeJ3p {
  total: number,
  periode: string,
  firstDate: string,
  lastDate: string,
  ecjs: StatsResultatsForType,
  j3p: StatsResultatsForTypeJ3p,
  qcm: StatsResultatsForType
}

/**
 * @typedef StatsResultats
 * @property {number} total Le nb de résultats analysés
 * @property {string} periode Le mois analysé sous la forme YYYY-MM
 * @property {string} firstDate Date du premier résultat
 * @property {string} lastDate Date du dernier resultat
 * @property {StatsResultatsForType} ecjs
 * @property {StatsResultatsForTypeJ3p} j3p
 * @property {StatsResultatsForType} qcm
 */
/**
 * @typedef StatsResultatsForType
 * @property {number} total Le nb de résultat pour ce type
 * @property {Object} rids Objet dont les clés sont des rids et les valeurs le nb de résultats de ce rid
 */

/**
 * @typedef StatsResultatsForTypeJ3p
 * @extends StatsResultatsForType
 * @property {Object} sections Objet dont les clés sont des noms de section et les valeurs le nb de résultats pour
 *     cette section
 */

/**
 * Ajoute une erreur sur la page
 * @param error
 * @private
 */
export function addError (error: unknown): void {
  console.error(error)
  const err: Error = error instanceof Error
    ? error
    : Error(String(error))
  const errorContainer = document.getElementById('errors')
  if (errorContainer) addElement(errorContainer, 'li', err.message)
}

/**
 * Alias de document.getElementById (qui râle en console s’il ne trouve pas)
 * @param {string} id
 * @param {HTMLElement|null}
 * @private
 */
function get (id: string): HTMLElement | null {
  const elt: HTMLElement | null = document.getElementById(id)
  if (!elt) console.error(Error(`#${id} non trouvé dans le dom`))
  return elt
}

interface GetRidUrlOptions {
  api?: boolean,
  j3p?: boolean,
  j3pdev?: boolean,
  local?: boolean
}

/**
 * Retourne l’url demandée pour cette ressource (voir si aucune option demandée)
 * @param {string} rid
 * @param {GetRidUrlOptions} options
 * @return {string}
 */
export function getRidUrl (rid: string, options?: GetRidUrlOptions): string {
  const [baseId, id] = rid.split('/')
  if (baseId && biblis[baseId]) {
    if (options) {
      if (options.api) return `${biblis[baseId]}api/public/${id}`
      if (options.j3p) return `https://j3p.sesamath.net/?rid=${rid}`
      if (options.j3pdev) return `https://j3p.sesamath.dev/?rid=${rid}`
      if (options.local) return `http://localhost:8081/?rid=${rid}`
    }
    return `${biblis[baseId]}public/voir/${id}`
  }
  console.error(Error(`rid ${rid} invalide`))
  return ''
}

/**
 * Affiche les liens de la ressource
 * @param {HTMLElement} container
 * @param {string} rid
 * @param {number} total
 */
function printRessourceLinks (container: HTMLElement, rid: string, total: number): void {
  const child = addElement(container, 'li')
  const href = getRidUrl(rid)
  if (href != null) {
    addElement(child, 'span', `${rid} : ${String(total)}`)
    // les liens ouvrent tous dans un nouvel onglet
    addElement(child, 'a', { textContent: 'original', href, target: 'original' })
    addElement(child, 'a', { textContent: 'json', href: getRidUrl(rid, { api: true }), target: 'json' })
    addElement(child, 'a', {
      textContent: 'description',
      href: href.replace('public/voir', 'ressource/decrire'),
      target: 'description'
    })
    addElement(child, 'a', { textContent: 'j3p prod', href: getRidUrl(rid, { j3p: true }), target: 'prod' })
    addElement(child, 'a', { textContent: 'j3p dev', href: getRidUrl(rid, { j3pdev: true }), target: 'dev' })
    addElement(child, 'a', { textContent: 'localhost', href: getRidUrl(rid, { local: true }), target: 'local' })
  }
}

const biblis: Record<string, string> = {
  sesabibli: 'https://bibliotheque.sesamath.net/',
  sesacommun: 'https://commun.sesamath.net/'
}
const moisCt = get('mois')

/** @type {StatsResultats} */
let cumul: StatsResultats
const step = 10
let lastSectionsIndex = 0
let lastRidsIndex = 0

/*
 const formatDay = num => String(num).padStart(2, '0')
 /**
 * Retourne la date en YYYY-MM-DD
 * @param {Date} date
 * @return {string}
 * /
 const formatDate = (date) => `${date.getFullYear()}-${formatDay(date.getMonth() + 1)}-${formatDay(date.getDate())}`
 */

/**
 *
 * @param {number} year
 * @param {number} month
 * @param {StatsResultatsForTypeJ3p} stats
 */
function addStats (year: number, month: number, stats: StatsResultats): void {
  const periode = getPeriod(year, month)
  if (moisCt == null) throw Error('Il n’y a pas de conteneur pour le mois courant dans le DOM')
  const li = addElement(moisCt, 'li')
  addElement(li, 'a', { textContent: periode, href: getJsonUrl(periode) })
  li.appendChild(document.createTextNode(' '))

  const { firstDate, lastDate, total, j3p } = stats
  const totalJ3p = j3p?.total ?? 0
  addElement(li, 'span', `${total} résultats (dont ${totalJ3p} j3p) du ${firstDate.substring(0, 10)} au ${lastDate.substring(0, 10)}`)
  if (cumul) {
    if (stats.lastDate < cumul.firstDate) {
      // console.log('on avait le cumul', cumul, 'et on ajoute', stats)
      cumul.firstDate = stats.firstDate
      if (j3p.total) {
        cumul.total += j3p.total
        if (!cumul.rids) cumul.rids = {}
        if (!cumul.sections) cumul.sections = {}
        for (const [rid, nb] of Object.entries(j3p.rids)) {
          if (!cumul.rids[rid]) cumul.rids[rid] = 0
          cumul.rids[rid] += nb
        }
        for (const [section, nb] of Object.entries(j3p.sections)) {
          if (!cumul.sections[section]) cumul.sections[section] = 0
          cumul.sections[section] += nb
        }
      }
    }
  } else {
    cumul = JSON.parse(JSON.stringify(stats))
    // faut remonter les infos j3p à la racine
    cumul.sections = cumul.j3p.sections
    cumul.rids = cumul.j3p.rids
  }

  // le bouton pour récupérer la période précédente
  const moreMonths = get('moreMonths')
  const prevPeriod = getPrevPeriod(year, month)
  if (moreMonths) {
    moreMonths.innerHTML = `Ajouter la période précédente ${prevPeriod}`
    moreMonths.onclick = async () => {
      const prevStats = await fetchOne(prevPeriod)
      if (prevStats) {
        const [prevYear, prevMonth] = prevPeriod.split('-').map(Number)
        if (prevYear && prevMonth) addStats(prevYear, prevMonth, prevStats)
      } else {
        moreMonths.innerHTML = `Plus de stats pour ${prevPeriod}`
        moreMonths.onclick = () => undefined
      }
    }
  }
  displayStats()
}

function displayStats (): void {
  // console.log('displayStats avec le cumul', cumul)
  if (!cumul.sections) {
    console.error(Error('objet cumul sans sections'), cumul)
    cumul.sections = {}
  }
  lastSectionsIndex = 0
  lastRidsIndex = 0
  const sectionsList = get('sectionsList')
  const ridsList = get('ridsList')
  const moreSectionsBtn = get('moreSections')
  const allSectionsBtn = get('allSections')
  const moreRidsBtn = get('moreRids')
  const allRidsBtn = get('allRids')
  if (!sectionsList) throw Error('Pas trouvé de conteneur #sectionsList')
  sectionsList.innerHTML = '<span>Sections populaires</span>'
  if (ridsList) ridsList.innerHTML = '<span>Ressources populaires</span>'

  // la liste des sections triées par nb
  const sectionsPopulaires = Object.keys(cumul.sections).sort((a, b) => Number(cumul.sections[b]) - Number(cumul.sections[a]))

  const displayMoreSections = () => {
    for (const section of sectionsPopulaires.slice(lastSectionsIndex, lastSectionsIndex + step)) {
      const li = addElement(sectionsList, 'li', { textContent: `${section} : ${cumul.sections[section]}` })
      addElement(li, 'a', { textContent: 'liste de ressources', target: 'liste', href: `liste.html#${section}` })
      lastSectionsIndex++
    }
    if (moreSectionsBtn) {
      if (lastSectionsIndex >= sectionsPopulaires.length - 1) {
        moreSectionsBtn.innerText = 'Tout est déjà affiché'
        moreSectionsBtn.onclick = () => undefined
      } else {
        moreSectionsBtn.innerText = `Afficher 10 sections de plus (${lastSectionsIndex + 1} - ${Math.min(lastSectionsIndex + 1 + step, sectionsPopulaires.length)})`
        moreSectionsBtn.onclick = displayMoreSections
      }
    }
  }
  // si c’est le 1er appel on affiche les 10 premières
  if (!lastSectionsIndex && cumul.total) {
    displayMoreSections()
    if (allSectionsBtn) {
      allSectionsBtn.innerHTML = 'Afficher tout'
      allSectionsBtn.onclick = () => {
        while (lastSectionsIndex < sectionsPopulaires.length - 1) displayMoreSections()
      }
    }
  }

  // idem pour les rids
  const ridsPopulaires = Object.keys(cumul.rids).sort((a, b) => Number(cumul.rids[a]) < Number(cumul.rids[b]) ? 1 : -1)

  const displayMoreRids = () => {
    if (ridsList) {
      for (const rid of ridsPopulaires.slice(lastRidsIndex, lastRidsIndex + step)) {
        printRessourceLinks(ridsList, rid, Number(cumul.rids[rid]))
        lastRidsIndex++
      }
    }
    if (moreRidsBtn) {
      if (lastRidsIndex >= ridsPopulaires.length - 1) {
        moreRidsBtn.innerText = 'Tout est déjà affiché'
        moreRidsBtn.onclick = () => undefined
      } else {
        moreRidsBtn.innerText = `Afficher 10 ressources de plus (${lastRidsIndex + 1} - ${Math.min(lastRidsIndex + 1 + step, ridsPopulaires.length)})`
        moreRidsBtn.onclick = displayMoreRids
      }
    }
  }

  // si c’est le 1er appel on affiche les 10 premières
  if (!lastRidsIndex && cumul.total) {
    displayMoreRids()
    if (allRidsBtn) {
      allRidsBtn.innerHTML = 'Afficher tout'
      allRidsBtn.onclick = () => {
        while (lastRidsIndex < ridsPopulaires.length - 1) displayMoreRids()
      }
    }
  }
}

/**
 * Retourne le json récupéré sur labomep pour la période demandée (undefined si ça n’existe pas)
 * @param {string} periode
 * @return {Promise<StatsResultats|undefined>}
 */
async function fetchOne (periode: string): Promise<StatsResultats | void> {
  const url = getJsonUrl(periode)
  try {
    const response = await fetch(url, { mode: 'cors' })
    if (!response.ok) return
    const stats = await response.json()
    if (stats.periode !== periode) throw Error(`${url} ne retourne pas le json attendu`)
    return stats
  } catch (error) {
    console.error(error, `Pas de stats pour ${periode}`)
  }
}

function getPeriod (year: number, month: number): string {
  return `${year}-${String(month).padStart(2, '0')}`
}

function getPrevPeriod (year: number, month: number): string {
  if (month > 1) return getPeriod(year, month - 1)
  return `${year - 1}-12`
}

const getJsonUrl = (periode: string): string => `https://j3p.sesamath.net/tmp/resultats_${periode}.json`

// const getJsonUrl = (periode) => `tmp/resultats_${periode}.json`

/**
 * Lance la compilation des stats
 */
async function buildStats (): Promise<void> {
  try {
    if (!moisCt) throw Error('Pas trouvé de conteneur #mois, la page n’est pas correctement initialisée')
    const today = new Date()
    const year = today.getFullYear()
    const month = today.getMonth() + 1
    let periode = getPeriod(year, month)
    // si la période en cours n’existe pas on tente la précédente
    let stats = await fetchOne(`${year}-${String(month).padStart(2, '0')}`)
    moisCt.innerHTML = ''
    if (!stats) {
      const mois = get('mois')
      if (mois) mois.innerText = `Aucune stats pour ${periode}`
      periode = getPrevPeriod(year, month)
      stats = await fetchOne(periode)
      if (!stats) {
        moisCt.innerText += ` ni pour ${periode}, on abandonne`
        return
      }
    }
    // on a une première stat, on affiche ça
    moisCt.innerHTML = '<span>Périodes analysées</span>'
    addStats(year, month, stats)
  } catch (error) {
    addError(error)
  }
}

export default buildStats
