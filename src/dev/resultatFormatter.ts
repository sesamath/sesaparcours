// Ce fichier est une copie de sesatheque-client:src/resultatFormatters/j3p.js pour debug
// adapté en ts

import { isLegacyResultat, normalizeResultat } from 'src/lib/core/checks'
import { convertResultat } from 'src/lib/core/convert1to2'
import type { PathwayJson } from 'src/lib/entities/Pathway'
import type { LegacyResultat, Resultat } from 'src/lib/types'

/**
 * Pour avoir l’équivalent de LegacyResultat.contenu.scores en V2.
 * C’est à dire, l’ensemble des scores dans une liste.
 */
function getScores (pathway: PathwayJson): number[] {
  return pathway.results.map(el => el.score).filter(score => score !== -1)
}

function getBaseUrlFromResultat (laxResultat: LegacyResultat | Resultat): string {
  const resultat = normalizeResultat(laxResultat)

  if (!resultat?.rid) throw Error('Résultat sans rid')
  const parsedResult = /^([^/]+)\//.exec(resultat.rid)
  if (parsedResult != null) {
    const [, baseId] = parsedResult
    switch (baseId) {
      case 'sesabibli':
        return 'https://bibliotheque.sesamath.net'
      case 'sesabidev':
        return 'https://bibliotheque.sesamath.dev'
      case 'sesacommun':
        return 'https://commun.sesamath.net'
      case 'sesacomdev':
        return 'https://commun.sesamath.dev'
      default:
        throw Error(`baseId ${baseId} inconnue`)
    }
  }
  throw Error(`rid ${resultat.rid} invalide`)
}

let i = 0

function getScoresByNode (contenu: { pathway: PathwayJson }): Record<string, number> {
  // Pour le score global, on veut un seul score par nœud (le meilleur si y’en a plusieurs)
  const scoresByNode: Record<string, number> = {}
  for (const { id, score } of contenu.pathway.results) {
    const node = contenu.pathway.graph.nodes[id]
    if (!node) {
      console.warn(`résultat incohérent avec un score sans nœud ${id} correspondant`, contenu)
      continue
    }
    if (typeof score !== 'number') continue
    // y’a un score, on affecte s’il est meilleur
    const nodeScore = scoresByNode[id]
    if (!nodeScore || typeof nodeScore !== 'number' || nodeScore < score) scoresByNode[id] = score
  }
  return scoresByNode
}

/**
 * Retourne le score d’un résultat (ce code devrait être dans j3p, mis là en attendant que les pbs de scores soient
 * réglés dans j3p pour recalcul à chaque affichage)
 * @param resultat
 * @return {number|undefined}
 */
function getRealScore (laxResultat: LegacyResultat | Resultat): number | undefined {
  // tant que c’est pas fini c’est undefined
  const resultat = isLegacyResultat(laxResultat) ? convertResultat(laxResultat) : laxResultat as Resultat
  if (!resultat.fin) return undefined
  const scores = getScores(resultat.contenu.pathway)
  if (resultat.contenu && scores && scores.length) {
    let total = 0
    let nbNodes = 0
    if (resultat.contenu.pathway.currentNodeId) {
      // on moyenne sur tous les nœuds avec scores
      scores.forEach(score => {
        if (typeof score === 'number' && score >= 0 && score <= 1) {
          total += score
          nbNodes++
        }
      })
    } else {
      // avant l’ajout de nextId dans le résultat, on avait tous les scores concaténés dans le même résultat,
      // faut prendre le meilleur par node
      // Mais ça, c'était avant. Maintenant, on convertit en Resultat, donc on va chercher dans les results.
      const scoresByNode = getScoresByNode(resultat.contenu)
      Object.entries(scoresByNode).forEach(([node, score]) => {
        if (typeof score === 'number' && score >= 0 && score <= 1) {
          nbNodes++
          total += score
        } else {
          console.warn(`score ${score} du nœud ${node} incohérent dans ce résultat`, resultat)
        }
      })
    }
    if (nbNodes) return total / nbNodes
    // sinon ça reste undefined
  }
  return undefined
}

function getReponse (laxResultat: Resultat | LegacyResultat, isTxt: boolean): string {
  const resultat = normalizeResultat(laxResultat)

  const separator = isTxt ? '\n' : '<br />'
  if (resultat.contenu.pathway.results.length) {
    return resultat.contenu.pathway.results.map(({
      id,
      score
    }) => `Nœud ${id} : ${Math.round(100 * score)}%`).join(separator)
    // à une époque noeuds pouvait avoir un élément de plus
    // Mais cette époque est révolue si normalizeResultat a fait correctement son job, tout est dans results.
    /*
    if (resultat.contenu.noeuds.length >= nbScores) {
      const results: string[] = []
      resultat.contenu.scores.forEach((score, index) => {
        const noeud = resultat.contenu.noeuds[index]
        if (noeud) {
          if (typeof score === 'number' && score >= 0 && score <= 1) results.push(`Nœud ${noeud} : ${Math.round(100 * score)}%`)
        } else {
          console.error(`score d’index ${index} sans nœud correspondant dans ce résultat`, resultat)
        }
      })
      if (results.length) return results.join(separator)
    } else {
      // bizarre on prend le meilleur score par nœud
      console.warn('resultat bizarre, avec noeuds', resultat.contenu.noeuds, 'et scores', resultat.contenu.scores)
      const scoresByNode = getScoresByNode(resultat.contenu)
      return Object.entries(scoresByNode)
        .map(([node, score]) => `Nœud ${node} : ${Math.round(100 * Number(score))}%`)
        .join(separator)
    }
     */
  }
  return 'Pas d’information enregistrée'
}

/**
 * Exo j3p (graphe d’exercices en js), avec getHtmlFullReponse dispo
 * @type TypeFormatters
 * @memberOf resultatFormatters
 */
export const j3p = {
  getScore: (laxResultat: Resultat | LegacyResultat): number | undefined => {
    const resultat = normalizeResultat(laxResultat)
    const score = getRealScore(resultat)
    if (resultat.fin && typeof score !== 'number') {
      console.error(`Pas de score sur un parcours terminé (1 imposé pour permettre de passer à la suite en cas de condition de réussite), résultat ${resultat.rid}`)
      return 1
    }
    return score
  },

  /**
   * Retourne le score à afficher sur la page html des bilans (pas forcément un nombre)
   * @param {Resultat} resultat
   * @return {string}
   */
  getHtmlScore: (laxResultat: Resultat | LegacyResultat): string => {
    const resultat = normalizeResultat(laxResultat)
    // pour j3p on s’attend à avoir
    if (!resultat.fin) return 'Parcours non terminé'
    if (resultat.score === undefined) console.error('parcours terminé sans score', resultat)
    // y’a eu un gros bug dans j3p, un résultat enregistré plusieurs fois avec le même oid, avec un tableau scores
    // qui augmente mais un score du resultat jamais mis à jour ex
    // {"noeuds":["1","1","1","1","1","1","1","1"],"pe":[0.75,1,1,1],"scores":[0.75,1,1,1],"ns":["fin","fin","fin","fin"],"boucle":[1,1,1,1],"graphe":[[],["1","squelettemtg32_Calc_Ecriture_Scientifique",[{"pe":">=0","nn":"fin","conclusion":"fin"},{"ex":"Lycee_Fraction_Reduction_1","limite":"","nbEssais":7,"nbchances":2,"nbrepetitions":4,"indicationfaute":false,"simplifier":true,"a":"random","b":"random","c":"random","d":"random","e":"random","f":"random","g":"random","h":"random","j":"random","k":"random","l":"random","m":"random","n":"random","r":"random","p":"random","q":"random"}]]]}
    // on recalcule score à chaque affichage
    const oldScore = resultat.score
    let score = getRealScore(resultat)
    if (typeof score === 'number' && typeof oldScore === 'number' && Math.abs(oldScore - score) > 0.005) {
      console.warn(`le score enregistré ${oldScore} ne correspond pas à celui calculé ${score}`, resultat)
      if (score < oldScore && oldScore <= 1) score = oldScore // on baisse pas un score déjà attribué, même s’il
      // est foireux…
    }

    if (typeof score === 'number') return Math.round(score * 100) + '%'
    return 'pas de score pour ce parcours (mis à 100% pour permettre le passage au suivant en cas de minimum de réussite exigé)'
  },

  /**
   * Retourne la réponse à insérer sur la page html des bilans
   * @param {Resultat} resultat
   * @param {FormatterOptions} [options] isTxt géré
   * @return {string}
   */
  getHtmlReponse: (laxResultat: Resultat | LegacyResultat): string => getReponse(normalizeResultat(laxResultat), false),

  /**
   * Retourne la réponse à insérer dans un csv
   * @param {Resultat} resultat
   * @return {string}
   */
  getTxtReponse: (laxResultat: Resultat | LegacyResultat): string => getReponse(normalizeResultat(laxResultat), true),

  /**
   * Réponse en pleine page
   * @param {Resultat} resultat
   * @return {string}
   */
  getHtmlFullReponse: function getHtmlFullReponse (laxResultat: Resultat | LegacyResultat): string {
    const resultat = normalizeResultat(laxResultat)

    // avec un seul node on ne répond rien
    if (resultat.contenu.pathway.results.length < 2) return ''

    // on a un bug qq part qui donnait par ex du
    // noeuds: ['1', '1'], 'pe': [1]
    if (resultat.contenu.pathway.results.every(({ id }) => id === '1')) {
      console.warn('graphe à un seul nœud bizarrement multiple', JSON.stringify(resultat.contenu))
      return ''
    }

    // faut virer les nœuds fin éventuels (ils ne sont plus dans les résultats récents mais toujours dans les
    // anciens)
    // Je vire les noeuds 'fin', à priori, ceux qui ont comme section ''
    const noeudsActifs = Object.entries(resultat.contenu.pathway.graph.nodes).filter(([id, node]) => id && node.section !== '')
    if (noeudsActifs.length < 2) return ''

    // on gère l’unicité des ids, même si à priori on est tout seul dans une iframe
    const myId = 'j3pParcours' + (i++)
    const baseUrl = getBaseUrlFromResultat(resultat) as string
    const { hostname } = new URL(baseUrl)
    const isDev = /(sesamath\.dev|localhost|\.local)$/.test(hostname)
    const jsUrl = `https://j3p.sesamath.${isDev ? 'dev' : 'net'}/build/loader.js`
    const contenuResultat = JSON.stringify(resultat.contenu)
    // dans ce qui suit, on peut mettre de l’es6 dans les ${} mais pas en dehors
    // normalement le js expose en global une fct stshowParcours, mais avec un pb de bundler on peut se
    // retrouver avec le module complet et pas son export par défaut, et vu que c’est arrivé (avec webpack) et
    // resté comme ça pendant des mois… …on traite les deux cas
    return `
<div id="${myId}"></div>
<script type="application/javascript">
  (function () {
    'use strict'
    var conteneur = document.getElementById("${myId}")
    if (typeof showPathway === 'function') return showPathway(conteneur, ${contenuResultat})
    // sinon c’est le premier appel, faut le charger
    var head = window.document.getElementsByTagName('head')[0]
    var body = window.document.getElementsByTagName('body')[0]
    if (!head || !body) throw Error('Page html malformée (head ou body manquant)')
    // ajout du js
    var scriptTag = document.createElement('script')
    scriptTag.type = 'application/javascript'
    scriptTag.src = '${jsUrl}'
    scriptTag.addEventListener('load', function () {
      if (typeof showPathway === 'function') return showPathway(conteneur, ${contenuResultat})
      console.error('Problème de chargement de viewer')
      conteneur.innerHTML = '<p style="color:"#d33">Impossible d’afficher le parcours réalisé (problème de chargement de l’afficheur)</p>'
    })
    body.appendChild(scriptTag)
  })()
</script>`
  }
}
