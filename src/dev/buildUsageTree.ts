/**
 * Utilisé par liste.html pour peupler la liste des sections
 * @module dev/spBuildUsageTree
 */

import { addError, get, printRessourceLinks, type RessourceDesc } from 'src/dev/helpers'
import { addElement } from 'src/lib/utils/dom/main'

type RessourcesList = Record<string, RessourceDesc>
type SectionsRids = Record<string, string[]>

/**
 * Déplie/replie tout
 * @param {boolean} folded passer true pour tout replier, false pour déplier
 * @private
 */
function foldAll (folded: boolean): void {
  const sectionList = get('sectionsList')
  if (sectionList) {
    for (const span of (sectionList.querySelectorAll('li > span'))) {
      toggleChilds(span as HTMLSpanElement, folded)
    }
  }
}

function populate (sectionsFirst: SectionsRids, sectionsThen: SectionsRids, ressources: RessourcesList, sectionsList: string[]): void {
  const ul = get('sectionsList')
  // on vide
  if (ul) {
    while (ul.lastChild) ul.removeChild(ul.lastChild)
    // et on reconstruit
    for (const section of sectionsList) {
      const li = addElement(ul, 'li')
      const sectionTitle = addElement(li, 'span', section)
      sectionTitle.addEventListener('click', toggleChilds)
      sectionTitle.style.fontWeight = 'bold'
      const children = addElement(li, 'ul')
      // replié par défaut
      children.style.display = 'none'
      let nb = 0
      // nb de ressources sur sesacommun
      let nbCommun = 0
      let rids = sectionsFirst[section]
      if (rids == null) {
        console.error(Error(`Aucune section ${section}`))
        continue
      }
      for (const rid of rids) {
        nb++
        if (/^sesacommun/.test(rid)) nbCommun++
        const ress = ressources[rid]
        if (ress == null) console.error(Error(`Pas de ressource ${rid}`))
        else printRessourceLinks(children, ress)
      }
      rids = sectionsThen[section]
      if (rids && rids.length) {
        const ul2 = addElement(children, 'ul', 'Ainsi que ces ressources où la section n’est pas en premier dans le graphe')
        for (const rid of rids) {
          nb++
          if (/^sesacommun/.test(rid)) nbCommun++
          const ress = ressources[rid]
          if (ress == null) console.error(Error(`Pas de ressource ${rid}`))
          else printRessourceLinks(ul2, ress)
        }
      }
      sectionTitle.appendChild(document.createTextNode(` (${nb}, dont ${nbCommun} sur commun)`))
    }
  }
}

/**
 * masque / démasque une liste de ressource
 * @param {Event|HTMLElement} ev
 * @param {boolean} [folded] passer true|false pour forcer fermé|ouvert (indépendamment de l’état actuel
 * @private
 */
function toggleChilds (ev: Event | HTMLElement, folded?: boolean): void {
  const elt = (ev instanceof Event ? ev.target : ev) as HTMLElement
  const parent = elt.parentNode
  if (parent) {
    const children = parent.querySelector('ul')
    if (children) {
      const shouldOpen = (typeof folded === 'boolean') ? !folded : children.style.display === 'none'
      if (shouldOpen) {
        children.style.display = 'block'
        elt.style.fontWeight = 'normal'
      } else {
        children.style.display = 'none'
        elt.style.fontWeight = 'bold'
      }
      // on ne râle plus car au chargement ça arrive pour tous les li, populate rend la main mais les ul ne sont
      // pas encore dans le dom… } else { console.error(Error('cette cible ne contient pas de <ul>'), elt)
    }
  }
}

/**
 * Lance la construction de l’arbre d’utilisation des sections dans le DOM (d’après sections.usage.json)
 */
async function buildUsageTree (): Promise<void> {
  try {
    const response = await fetch('https://j3p.sesamath.dev/sections.usage.json')
    const { sectionsFirst, sectionsThen, ressources, date } = await response.json()
    // on veut ça par ordre alphabétique
    const listByName = Object.keys(sectionsFirst).sort()
    const populateByName = () => populate(sectionsFirst, sectionsThen, ressources, listByName)
    // on ajoute total et date
    const nbSections = Object.keys(sectionsFirst).length
    const nbRess = Object.keys(ressources).length
    addElement(document.body, 'p', `${nbSections} sections utilisées dans ${nbRess} (généré avec les données du ${date})`)
    // et les actions
    const expandAllButton = get('expandAll')
    if (expandAllButton) expandAllButton.addEventListener('click', () => foldAll(false))
    const foldAllButton = get('foldAll')
    if (foldAllButton) foldAllButton.addEventListener('click', () => foldAll(true))
    const sortByNameButton = get('sortByName')
    if (sortByNameButton) sortByNameButton.addEventListener('click', populateByName)

    // cette liste sera calculée au 1er appel
    let listByUsage: string[]
    const sortByUsageButton = get('sortByUsage')
    if (sortByUsageButton) {
      sortByUsageButton.addEventListener('click', () => {
        if (!listByUsage) {
          const count = (section: string): number => ((sectionsFirst[section] && sectionsFirst[section].length) || 0) + ((sectionsThen[section] && sectionsThen[section].length) || 0)
          const compare = (sectionA: string, sectionB: string): number => count(sectionB) - count(sectionA)
          listByUsage = Object.keys(sectionsFirst).sort(compare)
        }
        populate(sectionsFirst, sectionsThen, ressources, listByUsage)
      })
    }

    // par défaut on affiche par nom au 1er affichage
    populateByName()

    // et on ajoute un listener sur le hashChange pour ne déplier qu’une seule section
    const onHashChange = () => {
      const section = window.location.hash.substring(1)
      if (listByName.includes(section)) {
        foldAll(true)
        for (const li of document.querySelectorAll('#sectionsList > li')) {
          if ((li as HTMLLIElement).innerText.startsWith(`${section} (`)) {
            const ul = li.querySelector('ul')
            if (ul) ul.style.display = 'block'
            li.scrollIntoView()
            return
          }
        }
      }
    }
    window.addEventListener('hashchange', onHashChange)
    // et au départ on le déclenche si on a un hash
    if (window.location.hash.length > 1) setTimeout(onHashChange, 0)
  } catch (error) {
    addError(error as Error)
  }
}

export default buildUsageTree
