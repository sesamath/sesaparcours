import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pClone, j3pEmpty, j3pIsHtmlElement } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import './editor.css'

class EditorProgrammeBase {
  constructor ({ container, getDefaultProgramme, values, scratch }) {
    if (!j3pIsHtmlElement(container)) throw Error('conteneur invalide')
    this.container = container
    this.ldOptions = { dansModale: true, speMod: 10, speMod2: 70, j3pCont: container }
    this.stor = {}
    this.getDefaultProgramme = getDefaultProgramme
    this.values = values
    this.menuChoixMin = [{
      type: 'catégorie',
      nom: 'POINTS',
      coul1: '#3230B8',
      coul2: '#3373CC',
      contenu: [{ type: 'pointLibreAvecNom', value: [{ nom: 'NOM', contenu: { valeur: 'A' } }] }]
    }]
    this.menuChoixMin2 = [{
      type: 'catégorie',
      nom: 'DEPLACEMENTS',
      coul1: '#4C97FF',
      coul2: '#3373CC',
      contenu: [
        { type: 'avancer', value: [{ nom: 'STEPS', contenu: { valeur: '20' } }], aff: false }
      ]
    }]
    this.scratch = scratch
  }

  ajouteExo () {
    this.stor.Programme.push(this.getDefaultProgrammeElement())
    this.menuExo()
  }

  dist (p, q) {
    return Math.sqrt((p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y))
  }

  getDefaultProgrammeElement () {
    const defaultProgramme = this.getDefaultProgramme()
    return defaultProgramme.Programme[0]
  }

  creeLesDiv () {
    this.stor.lesdivs = {}
    this.stor.lesdivs.conteneur = this.container
    this.stor.lesdivs.divUp = j3pAddElt(this.stor.lesdivs.conteneur, 'div', '', {
      style: {
        background: '#47ffff',
        border: '7px solid black',
        borderRadius: '10px',
        padding: '10px'
      }
    })
    this.stor.lesdivs.divUp.style.minWidth = '900px'
    this.stor.lesdivs.divExo = j3pAddElt(this.stor.lesdivs.conteneur, 'div', '', {
      style: {
        background: '#ccccff',
        border: '7px solid black',
        borderRadius: '10px',
        padding: '10px'
      }
    })
    this.stor.lesdivs.figDep = j3pAddElt(this.stor.lesdivs.conteneur, 'div')
    this.stor.lesdivs.Programme = j3pAddElt(this.stor.lesdivs.conteneur, 'div')
    this.stor.lesdivs.NouveauBlocs = j3pAddElt(this.stor.lesdivs.conteneur, 'div')
    this.stor.lesdivs.figDep.style.border = '4px solid black'
    this.stor.lesdivs.figDep.style.padding = '10px'
    this.stor.lesdivs.figDep.style.borderRadius = '10px'
    this.stor.lesdivs.figDep.style.backgroundImage = 'linear-gradient(to left, rgba(245, 169, 107, 0.8) 30%, rgba(245, 220, 187, 0.4))'
    this.stor.lesdivs.figDep.classList.add('pourFitContent')
    this.stor.lesdivs.Programme.style.border = '4px solid black'
    this.stor.lesdivs.Programme.style.padding = '10px'
    this.stor.lesdivs.Programme.style.borderRadius = '10px'
    this.stor.lesdivs.Programme.style.backgroundImage = 'linear-gradient(to right, rgba(187, 187, 187, 0.8) 30%, rgba(224, 224, 224, 0.5))'
    this.stor.lesdivs.NouveauBlocs.style.border = '4px solid black'
    this.stor.lesdivs.NouveauBlocs.style.background = '#22aa22'
  }

  importExo () {
    const aaj = j3pClone(this.stor.Programme)
    const nomsPris = []
    for (let i = 0; i < aaj.length; i++) {
      nomsPris.push(aaj[i].nom)
      // je sais Daniel y’a un truc pour faire ca mais je m’en rappelle plus
    }
    for (let i = 0; i < aaj.length; i++) {
      do {
        aaj[i].nom += '2'
      } while (nomsPris.indexOf(aaj[i].nom) !== -1)
      this.stor.Programme.push(j3pClone(aaj[i]))
      nomsPris.push(aaj[i].nom)
    }
    this.menuExo()
  }

  menuTotal () {
    // affectation de stor.Programme
    let parametres
    try {
      parametres = JSON.parse(this.values.Parametres)
    } catch (error) {
      console.error(error)
      parametres = this.getDefaultProgramme()
    }
    this.stor.Programme = Array.isArray(parametres.Programme) ? parametres.Programme : []
    const tab = addDefaultTable(this.stor.lesdivs.divUp, 7, 3)
    j3pAddContent(tab[0][0], '&nbsp;')
    j3pAddContent(tab[0][2], '&nbsp;')
    // nb exos
    const tab1 = addDefaultTable(tab[0][1], 1, 2)
    j3pAddContent(tab1[0][0], '<b><u>Nombre d’exercices proposés:</u></b>&nbsp;')

    this.stor.ParamnbExos = j3pAddElt(tab1[0][1], 'input', '', {
      type: 'number',
      value: String(this.values.nbrepetitions),
      size: 3,
      min: '1',
      max: '20'
    })
    this.stor.ParamnbExos.addEventListener('change', () => {
      const averif = this.stor.ParamnbExos.value
      if (isNaN(averif) || (averif < 1) || (averif > 20)) this.stor.ParamnbExos.value = '4'
    })
    // nb chances
    const tab2 = addDefaultTable(tab[1][1], 1, 4)
    j3pAddContent(tab2[0][0], '<b><u>Essais possibles:</u></b>&nbsp;')

    this.stor.inputNbChances = j3pAddElt(tab2[0][3], 'input', '', {
      type: 'number',
      value: this.values.nbchances,
      size: 2,
      min: '1',
      max: '10'
    })
    this.stor.inputNbChances.addEventListener('change', () => {
      const averif = this.stor.inputNbChances.value
      if (isNaN(averif) || (averif < 1) || (averif > 10)) this.stor.inputNbChances.value = '2'
    })
    this.stor.chronoCahc10 = tab2[0][2]
    this.stor.chronoCahc20 = tab2[0][3]

    const ldOptions4 = j3pClone(this.ldOptions)
    ldOptions4.choix0 = true
    ldOptions4.onChange = this.faisNbChances.bind(this)
    const ll = (Number(this.values.nbchances) === 0) ? ['Illimité', 'fixé'] : ['fixé', 'Illimité']
    this.stor.listeparamChance = ListeDeroulante.create(tab2[0][1], ll, ldOptions4)
    j3pAddContent(tab2[0][2], '&nbsp;à&nbsp;')
    this.stor.chronoCahc10.style.display = (this.stor.listeparamChance.reponse === 'fixé') ? '' : 'none'
    this.stor.chronoCahc20.style.display = (this.stor.listeparamChance.reponse === 'fixé') ? '' : 'none'

    // limite
    const tab3 = addDefaultTable(tab[2][1], 1, 4)
    j3pAddContent(tab3[0][0], '<b><u>Exercice chronométré:</u></b>&nbsp;')
    const ldOptions2 = j3pClone(this.ldOptions)
    ldOptions2.choix0 = true
    ldOptions2.afaire = this.faisLimite.bind(this)
    const ll2 = (this.values.limite === 0 || this.values.limite === '') ? ['Non', 'Oui'] : ['Oui', 'Non']
    this.stor.listeparamLimite = ListeDeroulante.create(tab3[0][1], ll2, ldOptions2)
    j3pAddContent(tab3[0][2], '&nbsp;Temps (en secondes):&nbsp;')

    this.stor.inputLimite = j3pAddElt(tab3[0][3], 'input', '', {
      type: 'number',
      value: (this.values.limite !== '' && this.values.limite !== 0) ? this.values.limite : 20,
      size: 3,
      min: '20',
      max: '120'
    })
    this.stor.inputLimite.addEventListener('change', () => {
      const averif = this.stor.inputLimite.value
      if (isNaN(averif) || (averif < 20) || (averif > 120)) this.stor.inputLimite.value = '30'
    })

    this.stor.chronoCahc1 = tab3[0][2]
    this.stor.chronoCahc2 = tab3[0][3]
    this.faisLimite()
    const tab4 = addDefaultTable(tab[3][1], 1, 2)
    j3pAddContent(tab4[0][0], '<b><u>Difficulté:</u></b>&nbsp;')
    const ldOptions3 = j3pClone(this.ldOptions)
    ldOptions3.choix0 = true
    const ll3 = (parametres.Difficulte === 'Progressive') ? ['Progressive', 'Aléatoire'] : ['Aléatoire', 'Progressive']
    this.stor.ParamDifficul = ListeDeroulante.create(tab4[0][1], ll3, ldOptions3)
    const tab5 = addDefaultTable(tab[4][1], 1, 2)
    j3pAddContent(tab5[0][0], '<b><u>Durée de 1 seconde entre chaque blocs:</u></b>&nbsp;')
    const ll4 = (parametres.Sequence) ? ['Oui', 'Non'] : ['Non', 'Oui']
    this.stor.ParamSequence = ListeDeroulante.create(tab5[0][1], ll4, ldOptions3)
    const tab6 = addDefaultTable(tab[5][1], 1, 2)
    j3pAddContent(tab6[0][0], '<b><u>Mode "pas à pas" disponible:</u></b>&nbsp;')
    const ll5 = (parametres.PasAPas) ? ['Oui', 'Non'] : ['Non', 'Oui']
    this.stor.ParamPasAPas = ListeDeroulante.create(tab6[0][1], ll5, ldOptions3)
    this.menuExo()
  }

  faisLimite () {
    const viz = (this.stor.listeparamLimite.reponse === 'Oui') ? '' : 'none'
    this.stor.chronoCahc1.style.display = viz
    this.stor.chronoCahc2.style.display = viz
  }

  faisNbChances () {
    const viz = (this.stor.listeparamChance.reponse === 'fixé') ? '' : 'none'
    this.stor.chronoCahc10.style.display = viz
    this.stor.chronoCahc20.style.display = viz
    if (viz === '') this.stor.inputNbChances.value = '1'
  }

  menuExo () {
    this.stor.yaBlok = false
    this.stor.lesdivs.figDep.style.display = 'none'
    this.stor.lesdivs.Programme.style.display = 'none'
    this.stor.lesdivs.NouveauBlocs.style.display = 'none'
    j3pEmpty(this.stor.lesdivs.figDep)
    j3pEmpty(this.stor.lesdivs.Programme)
    j3pEmpty(this.stor.lesdivs.NouveauBlocs)
    j3pEmpty(this.stor.lesdivs.divExo)
    const tab3 = addDefaultTable(this.stor.lesdivs.divExo, 3, 3)
    j3pAddContent(tab3[0][0], '&nbsp;')
    j3pAddContent(tab3[0][2], '&nbsp;')
    const tab4 = addDefaultTable(tab3[0][1], 1, 7)
    j3pAddContent(tab4[0][0], '<b>Liste des exercices disponibles</b>')
    tab3[0][0].style.textAlign = 'center'
    j3pAddContent(tab4[0][1], '&nbsp;&nbsp;')
    j3pAddContent(tab4[0][3], '&nbsp;&nbsp;')
    j3pAddContent(tab4[0][5], '&nbsp;&nbsp;')
    this.stor.lesdivs.divExoliste = tab3[1][1]
    j3pAjouteBouton(tab4[0][2], this.importExo.bind(this), { value: 'Dupliquer l’ensemble des exercices' })

    const tab = addDefaultTable(this.stor.lesdivs.divExoliste, this.stor.Programme.length + 2, 7)
    for (let i = 0; i < this.stor.Programme.length; i++) {
      tab[i + 1][0].style.borderBottom = '1px solid black'
      tab[i + 1][0].style.borderTop = '1px solid black'
      tab[i + 1][0].style.borderLeft = '1px solid black'
      tab[i + 1][1].style.borderBottom = '1px solid black'
      tab[i + 1][1].style.borderTop = '1px solid black'
      tab[i + 1][3].style.borderBottom = '1px solid black'
      tab[i + 1][3].style.borderTop = '1px solid black'
      tab[i + 1][2].style.borderBottom = '1px solid black'
      tab[i + 1][2].style.borderTop = '1px solid black'
      tab[i + 1][6].style.borderBottom = '1px solid black'
      tab[i + 1][6].style.borderTop = '1px solid black'
      tab[i + 1][6].style.borderRight = '1px solid black'
      tab[i + 1][2].style.padding = '5px 5px 5px 5px'
      tab[i + 1][6].style.padding = '5px 5px 5px 5px'
      tab[i + 1][4].style.padding = '5px 5px 5px 5px'
      tab[i + 1][4].style.borderBottom = '1px solid black'
      tab[i + 1][4].style.borderTop = '1px solid black'
      tab[i + 1][5].style.borderBottom = '1px solid black'
      tab[i + 1][5].style.borderTop = '1px solid black'
      j3pAddContent(tab[i + 1][1], '&nbsp;')
      j3pAddContent(tab[i + 1][3], '&nbsp;')
      j3pAddContent(tab[i + 1][5], '&nbsp;')
      const bufadd = (this.stor.Programme[i].nom) ? ' (' + this.stor.Programme[i].nom + ')' : ''
      j3pAffiche(tab[i + 1][0], null, '&nbsp;Exercice ' + (i + 1) + bufadd + ' niv: ' + this.stor.Programme[i].niv + '&nbsp;')
      j3pAjouteBouton(tab[i + 1][2], this.modifExo.bind(this, i), { value: 'Modifier' })
      j3pAjouteBouton(tab[i + 1][4], this.supExo.bind(this, i), { value: 'Supprimer' })
      j3pAjouteBouton(tab[i + 1][6], this.DupExo.bind(this, i), { value: 'Dupliquer' })
    }
    j3pAddContent(tab[tab.length - 2][0], '&nbsp;')
    j3pAjouteBouton(tab[tab.length - 1][0], this.ajouteExo.bind(this), { value: '+' })
  }

  modifExo (i) {
    this.stor.yaBlok = true
    this.stor.lesdivs.figDep.style.display = ''
    this.stor.lesdivs.Programme.style.display = ''
    this.stor.lesdivs.NouveauBlocs.style.display = ''
    j3pEmpty(this.stor.lesdivs.figDep)
    j3pEmpty(this.stor.lesdivs.Programme)
    j3pEmpty(this.stor.lesdivs.NouveauBlocs)
    this.stor.exoEncours = j3pClone(this.stor.Programme[i])
    j3pEmpty(this.stor.lesdivs.divExo)
    const tab = addDefaultTable(this.stor.lesdivs.divExo, 5, 3)
    j3pAddContent(tab[0][0], '&nbsp;')
    j3pAddContent(tab[2][2], '&nbsp;')
    j3pAddContent(tab[4][0], '&nbsp;')
    const tadd = addDefaultTable(tab[1][1], 1, 7)
    this.stor.cellModifExo = tadd[0][0]
    j3pAddContent(tadd[0][1], '&nbsp;&nbsp;&nbsp;&nbsp;')
    j3pAddContent(tadd[0][2], '<b>Changer le nom:</b>&nbsp;')
    j3pAddContent(tadd[0][4], '&nbsp;&nbsp;&nbsp;&nbsp;')
    j3pAddContent(tadd[0][5], 'niveau:&nbsp;')
    this.stor.inputNouveauNom = j3pAddElt(tadd[0][3], 'input', '', { value: this.stor.exoEncours.nom, size: 15 })
    this.stor.inputNouveauNom.addEventListener('input', this.chgNom.bind(this))
    this.stor.numExo = i + 1
    this.chgNom()
    this.stor.inputNiv = j3pAddElt(tadd[0][5], 'input', '', {
      type: 'number',
      value: String(this.stor.exoEncours.niv),
      size: 3,
      min: '0',
      max: '20'
    })
    this.stor.inputNiv.addEventListener('change', () => {
      const averif = this.stor.inputNiv.value
      if (isNaN(averif) || (averif < 0) || (averif > 20)) this.stor.inputNiv.value = '0'
      this.stor.exoEncours.niv = Number(this.stor.inputNiv.value)
    })

    const tabou = addDefaultTable(tab[3][1], 1, 3)
    j3pAddContent(tabou[0][1], '&nbsp;&nbsp;&nbsp;&nbsp;')
    this.stor.btnValiderNom = j3pAjouteBouton(tabou[0][0], this.valModifExo.bind(this, i), { value: 'Valider' })
    j3pAjouteBouton(tabou[0][2], this.menuExo.bind(this), { value: 'Annuler' })
    this.stor.exoEncoursi = i
  }

  DupExo (num) {
    const aaj = j3pClone(this.stor.Programme[num])
    const nomsPris = []
    for (let i = 0; i < this.stor.Programme.length; i++) {
      nomsPris.push(this.stor.Programme[i].nom)
      // je sais Daniel y’a un truc pour faire ca mais je m’en rappelle plus
    }
    do {
      aaj.nom += '2'
    } while (nomsPris.indexOf(aaj.nom) !== -1)
    this.stor.Programme.push(j3pClone(aaj))
    this.menuExo()
  }

  chgNom () {
    this.stor.exoEncours.nom = this.stor.inputNouveauNom.value
    const bufadd = (this.stor.exoEncours.nom) ? ' (' + this.stor.exoEncours.nom + ') ' : ''
    j3pEmpty(this.stor.cellModifExo)
    j3pAddContent(this.stor.cellModifExo, '<b>Modification de l’exercice ' + this.stor.numExo + bufadd + '</b>.')
  }

  supExo (i) {
    this.stor.Programme.splice(i, 1)
    this.menuExo()
  }

  afficheExplik (ou, txt) {
    const tomuch = addDefaultTable(ou, 1, 2)
    j3pAjouteBouton(tomuch[0][0], () => { this.faiv(tomuch[0][1]) }, { value: ' ? ' })
    tomuch[0][1].style.color = '#850593'
    tomuch[0][1].style.background = '#b0ee7f'
    tomuch[0][1].style.padding = '5px'
    tomuch[0][1].style.border = '1px solid black'
    j3pAddContent(tomuch[0][1], txt)
    tomuch[0][1].style.display = 'none'
  }

  faiv (el) {
    el.style.display = (el.style.display === '') ? 'none' : ''
  }
}

export default EditorProgrammeBase
