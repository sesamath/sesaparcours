import $ from 'jquery'

import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pAjouteCaseCoche, j3pClone, j3pDetruit, j3pEmpty } from 'src/legacy/core/functions'
import EditorProgrammeBase from 'src/legacy/outils/paramEditors/EditorProgrammeBase'
import { getListeBloc } from 'src/legacy/sections/college/blockly/scratchmake/defaultProgramme'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import { faitAreaFacile, importImage, j3pModale2 } from 'src/legacy/outils/zoneStyleMathquill/functions'
import axeIm from './axe.png'
import axeImFixe from './axecratch.png'
import chatImg from './chat.png'
import dessinleveImg from './dessinleve.png'
import blokAvancerImg from './images/blokAvancer.jpg'
import blokTourneDroiteImg from './images/blokTournerDroite.jpg'
import blokTourneGaucheImg from './images/blokTournerGauche.jpg'
import blokAllerAImg from './images/blokAllerA.jpg'
import blokAjouterXImg from './images/blokAjouterX.jpg'
import blokAjouterYImg from './images/blokAjouterY.jpg'
import blokMettreXImg from './images/blokMettreX.jpg'
import blokMettreYImg from './images/blokMettreY.jpg'
import blokDirectionImg from './images/blokDirection.jpg'
import blokOrienterImg from './images/blokOrienter.jpg'
import blokAbscisseImg from './images/blokAbscisse.jpg'
import blokOrdonneeImg from './images/blokOrdonne.jpg'
import blokDireImg from './images/blokDire.png'
import blokDrapoImg from './images/blokDrapo.png'
import blokFlecheGImg from './images/blokFecheG.png'
import blokFlecheDImg from './images/blokFecheD.png'
import blokFlecheHImg from './images/blokFecheH.png'
import blokFlecheBImg from './images/blokFecheB.png'
import blokFlecheAImg from './images/blokFecheA.png'
import blokFlecheESPImg from './images/blokFecheESP.png'
import blokFlecheSouImg from './images/blokFecheSOU.jpg'
import blokREPETEImg from './images/blokREPETE.png'
import blokFOREVERImg from './images/blokFOREVER.png'
import blokJUSQUAImg from './images/blokJUSQUA.png'
import blokIFImg from './images/blokIF.png'
import blokSTOPImg from './images/blokSTOP.png'
import blokIFELSEImg from './images/blokIFELSE.png'
import blokADDImg from './images/blokADD.png'
import blokSUBImg from './images/blokSUB.png'
import blokMULImg from './images/blokMUL.png'
import blokDIVImg from './images/blokDIV.png'
import blokMATHImg from './images/blokMATH.png'
import blokRANDImg from './images/blokRAND.png'
import blokEGALImg from './images/blokEGAL.png'
import blokPLUSGRANDImg from './images/blokPLUSGRAND.png'
import blokPLUSPETITImg from './images/blokPLUSPETIT.png'
import blokECRITImg from './images/blokECRIT.png'
import blokLEVEImg from './images/blokRELEV.png'
import blokGETUSImg from './images/blokGETUS.png'
import blokREPONSEImg from './images/blokREPONSE.png'
import blokMONBLOCImg from './images/blokMONBLOC.png'
import blokCaseAvanceImg from './images/blokCaseAvance.png'
import blokCaseReculeImg from './images/blokCaseRecule.png'
import blokCaseTourneGaucheImg from './images/blokCaseTourneGauche.png'
import blokCaseTourneDroiteImg from './images/blokCaseTourneDroite.png'
import blokCaseJauneImg from './images/blokCaseJaune.png'
import blokCaseBleueImg from './images/blokCaseBleue.png'
import blokCaseRougeImg from './images/blokCaseRouge.png'
import blokCaseVerteImg from './images/blokCaseVerte.png'
import blokCaseOrangeImg from './images/blokCaseOrange.png'
import blokCaseRoseImg from './images/blokCaseRose.png'
import blokCaseVioletteImg from './images/blokCaseViolette.png'
import blokCaseColoreImg from './images/blokCaseColore.png'
import blokCaseNotColoreImg from './images/blokCaseNotColore.png'
import blokCaseNoireImg from './images/blokCaseNoire.png'
import blokCaseColoreJauneImg from './images/blokCaseColoreJaune.png'
import blokCaseColoreBleueImg from './images/blokCaseColoreBleue.png'
import blokCaseColoreRougeImg from './images/blokCaseColoreRouge.png'
import blokCaseColoreVerteImg from './images/blokCaseColoreVerte.jpg'
import blokCaseColoreOrangeImg from './images/blokCaseColoreOrange.png'
import blokCaseColoreRoseImg from './images/blokCaseColoreRose.png'
import blokCaseColoreVioletteImg from './images/blokCaseColoreViolette.png'
import blokCaseoperatorresteImg from './images/blokReste.png'
import blokCaseoperatorquotientImg from './images/blokQuotient.png'

import caseIm from './images/fondcase.jpg'

const genIm = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAMAAAC5zwKfAAAAPFBMVEX///8QX3X1+fnY6+oGWG/G3t8ATWYxo5P8/f0jnYzo8vKdy8mx2dUydYhYtKduvrNEq52Gwb54pbFWjp6S6lfVAAAD+0lEQVRYw62Y2YKDIAxFEQQSQAX9/3+dBPcW12leplPt4cINESPE/wPETwNASPghjCLE8AuVIwxAdYhRAvxEnx+EjGgtpjsSTwclZW3wxvTCE9CiujXpEySIDq2q6gSyRbT+2hcQSp0g6XL0EDR90NipG7kD0I43wlHqLRfoLnlJBBHQoi0jYSaC1tMn5c+JAM5yHCAhNQ44mjr/Famq9SmRbkE7I/UeCaAbtpe/CoEvgeiNGc6AvNJ2DsQ9kuZXV5XRQtLKidTx1EXTO7hwxNojJMk39aBcNQgXEVkkyL1PXwL9lveJpN8PgZwdEkCiK6eskefsVyC2evs7GCmyDfRJdzJ/qU92wTkSNhuJ54Oe/+uNv3TkExnG/BOO0gmkbhSDAtcbrhWNhmtHvpDAlcFiK8lt0+SsSZ0EuO9IAUkVoaU86eskYbFfDapAnffIBVJ5Vibd7A4lTl9Rcsu7jnwgYxgpuiEk1xyQjakrEx448sGMgW0IjaJHQMv7uTdVVVfuC5hL+h0iRp/nFzBS0tAPm9pUvXvmyKdKL/MSRS3IIW8GVajD9klgJF+Vl65tuTh+b8A7juyJeSPzjuYC+103bzqyJeZnvIydKmQ2eR/t04hSJi/cmz1yIDHJuimXr6eOzKGU064M7PAFj6pEagEOHpzvAktPvFeOzBrLAtNrgaXj4VtHcuaUBbY/FvjaEVs6vv7DEYu+KDC9F1hI6v84Ujxenz04L6Mo8L0jJYF0DHjtCJWGEvC9I9PR679legMsCfyHI2WB4R8CC3WLjjLvBRbrlvPtWyQevdaqRGfvXwkcz2MydBbxVwLHJz5N/SEyHr+Cwvj6px9NHf3Jewm/FD2depTHM5ZN0/MBnpnK35N5LJB4g6lNPTFJpr4l0x0A+UXL0GG2WpjsULhKzsO+Bc0y82pCbplwlZzHbQvm1RlZ73WeOVQqC9POo/XL6qq6oFMdJuehQN0PDSHMgs0yV2Y5OY8Fjiq1X7HVpHZlFhzC4w7DcjSGAnZhfjiE7UVXZT1wf2IX5s4h1NdtGu+DcgVsPc19WzmP6tb+OUDDxzZ5raQoqDUjc5w6XvULlyYDZmyXgnYLVo7YemaGDqO8FNjtuyoTdl2DEdtwDZHcZbsS6HEMu+fSGnTbNRBSBe+1vHQE6LaUuq5t2xhnjWvkpZ3WgF/db/Rw126MlM4ppbQO0yA0Styodfc6wjDGUbnMo+QxQtDqSfsWQGt603LlWT1vBOcuGe86srMZKPo+Jcp4zcPQONNIcJ+npj3MYbZB39MIPZsDTybcmAXESlkmiWSJd93YC+xNlee6MHKba53j6Bw8cETNOw7WVuEcb9v9OUukXLPoIekPRXxs+n14NQQAAAAASUVORK5CYII='

class EditorProgrammeScratchMake extends EditorProgrammeBase {
  affichelisteBlocs () {
    function makefuncBout (i, cmpt) {
      return function () {
        this.stor.listeBloc[i].aff = !this.stor.listeBloc[i].aff
        for (let j = 0; j < this.stor.listeBloc[i].contenu.length; j++) {
          this.stor.tabTot[cmpt + j + 1][0].style.display = (this.stor.listeBloc[i].aff) ? '' : 'none'
        }
        this.stor.listeBoutoBloc[i].value = (this.stor.listeBloc[i].aff) ? '-' : '+'
      }
    }
    j3pEmpty(this.stor.lesdivs.listeBloc)
    let cmpt = 0
    for (let i = 0; i < this.stor.listeBloc.length; i++) {
      cmpt += (this.stor.listeBloc[i].contenu.length + 1)
    }
    this.stor.listeBoutoBloc = []
    this.stor.listeCocheBloc = []
    this.stor.tabTot = addDefaultTable(this.stor.lesdivs.listeBloc, cmpt, 1)
    cmpt = 0
    for (let i = 0; i < this.stor.listeBloc.length; i++) {
      this.stor.tabTot[cmpt][0].style.border = '1px solid black'
      const tabla = addDefaultTable(this.stor.tabTot[cmpt][0], 1, 3)
      tabla[0][0].style.padding = '5px'
      tabla[0][1].style.padding = '5px'
      tabla[0][1].style.width = '100%'
      tabla[0][2].style.padding = '5px'
      if (i !== this.stor.listeBloc.length - 1) this.stor.listeBloc[i].aff = false
      this.stor.tabTot[cmpt][0].style.background = this.stor.listeBloc[i].coul1
      j3pAddContent(tabla[0][0], this.stor.listeBloc[i].nom)
      this.stor.listeBoutoBloc[i] = j3pAjouteBouton(tabla[0][2], makefuncBout(i, cmpt).bind(this), { value: '+' })
      cmpt++
      this.stor.listeCocheBloc[i] = []
      for (let j = 0; j < this.stor.listeBloc[i].contenu.length; j++) {
        this.stor.tabTot[cmpt][0].style.borderLeft = '1px solid black'
        this.stor.tabTot[cmpt][0].style.borderRight = '1px solid black'
        this.stor.tabTot[cmpt][0].style.borderTop = '1px dashed black'
        this.stor.tabTot[cmpt][0].style.borderBottom = '1px dashed black'
        const tabjk = addDefaultTable(this.stor.tabTot[cmpt][0], 1, 4)
        this.stor.tabTot[cmpt][0].style.background = this.stor.listeBloc[i].coul1
        tabjk[0][0].style.padding = '5px'
        // tabjk[0][1].style.padding = '5px'
        tabjk[0][1].style.width = '100%'
        tabjk[0][2].style.padding = '5px'
        tabjk[0][3].style.padding = '5px'
        const yaIm = this.fileIm(this.stor.listeBloc[i].contenu[j].type)
        if (!yaIm) {
          j3pAddContent(tabjk[0][0], this.stor.listeBloc[i].contenu[j].type)
        } else {
          j3pAddElt(tabjk[0][0], 'img', '', { src: yaIm.im, width: yaIm.x, height: yaIm.y })
        }
        j3pAddContent(tabjk[0][3], '&nbsp;')
        this.stor.listeCocheBloc[i][j] = j3pAjouteCaseCoche(tabjk[0][2])
        this.stor.tabTot[cmpt][0].style.display = 'none'
        this.stor.listeCocheBloc[i][j].checked = this.stor.listeBloc[i].contenu[j].aff
        this.stor.listeCocheBloc[i][j].onchange = this.majMenu.bind(this)
        cmpt++
      }
      if (i === this.stor.listeBloc.length - 1) {
        j3pEmpty(tabla[0][2])
        this.stor.listeCocheBlocVar = j3pAjouteCaseCoche(tabla[0][2])
        this.stor.listeCocheBlocVar.checked = this.stor.listeBloc[i].aff
        this.stor.listeCocheBlocVar.onchange = this.majMenu.bind(this)
      }
    }
  }

  fileIm (s) {
    switch (s) {
      case 'avancer': return { im: blokAvancerImg, x: 120, y: 50 }
      case 'tourne_droite': return { im: blokTourneDroiteImg, x: 150, y: 50 }
      case 'tourne_gauche': return { im: blokTourneGaucheImg, x: 150, y: 50 }
      case 'aller_a': return { im: blokAllerAImg, x: 130, y: 50 }
      case 'ajouter_x': return { im: blokAjouterXImg, x: 110, y: 50 }
      case 'mettre_x': return { im: blokMettreXImg, x: 105, y: 50 }
      case 'ajouter_y': return { im: blokAjouterYImg, x: 110, y: 50 }
      case 'mettre_y': return { im: blokMettreYImg, x: 105, y: 50 }
      case 'direction': return { im: blokDirectionImg, x: 100, y: 40 }
      case 'motion_pointindirection': return { im: blokOrienterImg, x: 100, y: 50 }
      case 'abscisse_x': return { im: blokAbscisseImg, x: 100, y: 40 }
      case 'ordonnee_y': return { im: blokOrdonneeImg, x: 100, y: 40 }
      case 'dire': return { im: blokDireImg, x: 170, y: 50 }
      case 'Drapo': return { im: blokDrapoImg, x: 120, y: 60 }
      case 'FLECHEG': return { im: blokFlecheGImg, x: 120, y: 60 }
      case 'FLECHED': return { im: blokFlecheDImg, x: 120, y: 60 }
      case 'FLECHEH': return { im: blokFlecheHImg, x: 120, y: 60 }
      case 'FLECHEB': return { im: blokFlecheBImg, x: 120, y: 60 }
      case 'FLECHEA': return { im: blokFlecheAImg, x: 120, y: 60 }
      case 'FLECHEESP': return { im: blokFlecheESPImg, x: 120, y: 60 }
      case 'EVENTSou': return { im: blokFlecheSouImg, x: 150, y: 60 }
      case 'control_repeat': return { im: blokREPETEImg, x: 150, y: 110 }
      case 'control_forever': return { im: blokFOREVERImg, x: 150, y: 110 }
      case 'control_repeat_until': return { im: blokJUSQUAImg, x: 150, y: 110 }
      case 'control_if': return { im: blokIFImg, x: 150, y: 110 }
      case 'control_if_else': return { im: blokIFELSEImg, x: 150, y: 130 }
      case 'control_stop': return { im: blokSTOPImg, x: 100, y: 60 }
      case 'operator_add': return { im: blokADDImg, x: 100, y: 40 }
      case 'operator_subtract': return { im: blokSUBImg, x: 100, y: 40 }
      case 'operator_multiply': return { im: blokMULImg, x: 100, y: 40 }
      case 'operator_divide': return { im: blokDIVImg, x: 100, y: 40 }
      case 'operator_random': return { im: blokRANDImg, x: 160, y: 40 }
      case 'operator_equals': return { im: blokEGALImg, x: 100, y: 40 }
      case 'operator_gt': return { im: blokPLUSGRANDImg, x: 100, y: 40 }
      case 'operator_lt': return { im: blokPLUSPETITImg, x: 100, y: 40 }
      case 'operator_mathop': return { im: blokMATHImg, x: 110, y: 40 }
      case 'lever_stylo': return { im: blokLEVEImg, x: 130, y: 50 }
      case 'poser_stylo': return { im: blokECRITImg, x: 160, y: 50 }
      case 'Get_us': return { im: blokGETUSImg, x: 160, y: 50 }
      case 'reponse': return { im: blokREPONSEImg, x: 100, y: 40 }
      case 'perso': return { im: blokMONBLOCImg, x: 120, y: 50 }
      case 'caseAvancer': return { im: blokCaseAvanceImg, x: 120, y: 50 }
      case 'caseReculer': return { im: blokCaseReculeImg, x: 120, y: 50 }
      case 'caseTournerDroite': return { im: blokCaseTourneDroiteImg, x: 140, y: 50 }
      case 'caseTournerGauche': return { im: blokCaseTourneGaucheImg, x: 140, y: 50 }
      case 'caseIsJaune': return { im: blokCaseJauneImg, x: 150, y: 40 }
      case 'caseIsBleue': return { im: blokCaseBleueImg, x: 150, y: 40 }
      case 'caseIsRouge': return { im: blokCaseRougeImg, x: 150, y: 40 }
      case 'caseIsVerte': return { im: blokCaseVerteImg, x: 150, y: 40 }
      case 'caseIsOrange': return { im: blokCaseOrangeImg, x: 150, y: 40 }
      case 'caseIsRose': return { im: blokCaseRoseImg, x: 150, y: 40 }
      case 'caseIsViolette': return { im: blokCaseVioletteImg, x: 150, y: 40 }
      case 'caseIsColore': return { im: blokCaseColoreImg, x: 160, y: 40 }
      case 'caseIsNotColore': return { im: blokCaseNotColoreImg, x: 160, y: 40 }
      case 'caseDevantNoire': return { im: blokCaseNoireImg, x: 160, y: 40 }
      case 'caseColoreJaune': return { im: blokCaseColoreJauneImg, x: 160, y: 50 }
      case 'caseColoreBleue': return { im: blokCaseColoreBleueImg, x: 160, y: 50 }
      case 'caseColoreRouge': return { im: blokCaseColoreRougeImg, x: 160, y: 50 }
      case 'caseColoreVerte': return { im: blokCaseColoreVerteImg, x: 160, y: 50 }
      case 'caseColoreOrange': return { im: blokCaseColoreOrangeImg, x: 160, y: 50 }
      case 'caseColoreRose': return { im: blokCaseColoreRoseImg, x: 160, y: 50 }
      case 'caseColoreViolette': return { im: blokCaseColoreVioletteImg, x: 160, y: 50 }
      case 'operator_reste': return { im: blokCaseoperatorresteImg, x: 140, y: 40 }
      case 'operator_quotient': return { im: blokCaseoperatorquotientImg, x: 160, y: 40 }
    }
    return null
  }

  ajBlocPerso () {
    const Blockly = this.scratch.Blockly
    const lxmlTrav = Blockly.Xml.workspaceToDom(this.scratch.params.scratchProgDonnees.workspace2)
    let travEnCours = Blockly.Xml.domToText(lxmlTrav)
    if (travEnCours.indexOf('MAkePerso') === -1) {
      travEnCours = travEnCours.replace('</variables>', '</variables><block type="MAkePerso" id="yU8K90S3w:18u?Wt?G}Y" x="200" y="20"></block>')
    } else {
      return
    }
    this.refaisProg($.parseXML(travEnCours), this.scratch.params.scratchProgDonnees.workspace2)
  }

  boutonGenere () {
    const Blockly = this.scratch.Blockly
    const lxml = Blockly.Xml.workspaceToDom(this.scratch.params.scratchProgDonnees.workspace2)
    this.stor.ProgDeb = Blockly.Xml.domToText(lxml)
    let araj = this.stor.ProgDeb

    const lxmlTrav = Blockly.Xml.workspaceToDom(this.scratch.params.scratchProgDonnees.workspace)
    let travEnCours = Blockly.Xml.domToText(lxmlTrav)
    travEnCours = travEnCours.replace('</xml>', '')
    travEnCours = travEnCours.replace('x="20"', 'x="250"')
    travEnCours = travEnCours.replace('x="50"', 'x="280"')

    araj = araj.replace('<xml xmlns="http://www.w3.org/1999/xhtml"><variables>', '')
    const yvar = araj.indexOf('</variables>')
    if (yvar !== 0) {
      const varxonc = araj.substring(0, araj)
      araj = araj.substring(yvar)
      travEnCours = '<xml xmlns="http://www.w3.org/1999/xhtml"><variables>' + varxonc + travEnCours.substring(travEnCours.indexOf('</variables>'))
    }
    araj = araj.replace('</variables>', '')
    travEnCours = travEnCours + araj
    travEnCours = this.nettoieId(travEnCours)
    this.refaisProg($.parseXML(travEnCours), this.scratch.params.scratchProgDonnees.workspace)
    window.scroll({
      top: 1000,
      left: 0,
      behavior: 'smooth'
    })
    j3pEmpty(this.stor.zoneAttText)
    j3pAddContent(this.stor.zoneAttText, '&nbsp;<u>Programme attendu</u> &nbsp&nbsp <i> n’oubliez pas de supprimer l’ancien drapreau </i> &nbsp&nbsp')
    this.afficheExplik(this.stor.zoneAttText, 'La correction compare le résulat du programme attendu avec celui de l’élève.<br> En cas de mauvaise réponse de l’élève,<br> ce <b>programme attendu</b> est donné comme solution de l’exercice.')
  }

  cestParti () {
    this.paspret = true
    const Blockly = this.scratch.Blockly
    this.scratch.initParams(null, false, { edit: true })
    this.scratch.params.j3pCont = this.container
    this.scratch.testFlag = () => {}
    this.scratch.testFlagFin = () => {
      this.stor.leboubput.style.display = ''
      return { ok: true }
    }
    this.scratch.finishCreaPerso = () => {
      j3pEmpty(this.scratch.params.ModaleCreasquizzEssai2)
      j3pEmpty(this.stor.lesdivs.DeDefPerso)
      this.affichelisteBlocs()
      this.scratch.params.ModaleCreasquizzEssai2.style.border = '0px'
      this.scratch.params.ModaleCreasquizzEssai2.style.background = ''
      this.refaisMenu()
    }
    this.scratch.params.lesBlocPerso = this.stor.exoEncours.blocPerso.map(el => el.nom)
    this.scratch.params.listeDef = {}

    this.creeFigDep()
    const xmlDoc = $.parseXML(this.stor.ProgDeb)
    const xmlDocTrav = $.parseXML(this.stor.ProgrammeAt)
    const xmll2 = this.faisMenu()
    const xxxml1 = this.faisMenuBase()

    const side = 'start'
    this.scratch.params.scratchProgDonnees.workspace2 = Blockly.inject(this.stor.lesdivs.progdeb, {
      comments: true,
      disable: false,
      collapse: false,
      media: j3pBaseUrl + 'externals/blockly/media/',
      readOnly: false,
      rtl: null,
      scrollbars: true,
      toolbox: xxxml1,
      toolboxPosition: side === 'top' || side === 'start' ? 'start' : 'end',
      horizontalLayout: side === 'top' || side === 'bottom',
      sounds: false,
      zoom: {
        controls: true,
        wheel: true,
        startScale: 0.6,
        maxScale: 2,
        minScale: 0.25,
        scaleSpeed: 1.1
      },
      colours: {
        fieldShadow: 'rgba(100, 100, 100, 0.3)',
        dragShadowOpacity: 0.6
      }
    })
    this.stor.lesdivs.progdeb.style.height = '600px'
    this.stor.lesdivs.progdeb.style.width = '600px'
    this.stor.lesdivs.progdeb.style.minWidth = '600px'
    Blockly.svgResize(this.scratch.params.scratchProgDonnees.workspace2)
    this.refaisProg(xmlDoc, this.scratch.params.scratchProgDonnees.workspace2)

    this.scratch.params.scratchProgDonnees.workspace = Blockly.inject(this.stor.lesdivs.programme, {
      comments: true,
      disable: false,
      collapse: false,
      media: '/externals/blockly/media/',
      readOnly: false,
      rtl: null,
      scrollbars: true,
      toolbox: xmll2,
      toolboxPosition: side === 'top' || side === 'start' ? 'start' : 'end',
      horizontalLayout: side === 'top' || side === 'bottom',
      sounds: false,
      zoom: {
        controls: true,
        wheel: true,
        startScale: 0.6,
        maxScale: 2,
        minScale: 0.25,
        scaleSpeed: 1.1
      },
      colours: {
        fieldShadow: 'rgba(100, 100, 100, 0.3)',
        dragShadowOpacity: 0.6
      }
    })
    this.stor.lesdivs.programme.style.height = '600px'
    this.stor.lesdivs.programme.style.width = '600px'
    this.stor.lesdivs.programme.style.minWidth = '600px'
    Blockly.svgResize(this.scratch.params.scratchProgDonnees.workspace)
    this.refaisProg(xmlDocTrav, this.scratch.params.scratchProgDonnees.workspace)
    this.paspret = false
    this.faisProg('drapeau')
  }

  creeFigDep () {
    const tabsup = addDefaultTable(this.stor.lesdivs.figDep, 1, 4)
    j3pAddContent(tabsup[0][1], '&nbsp;&nbsp;')
    j3pAddContent(tabsup[0][2], '&nbsp;&nbsp;')
    tabsup[0][3].style.verticalAlign = 'top'
    tabsup[0][0].style.verticalAlign = 'top'
    tabsup[0][2].style.borderLeft = '1px dashed black'
    j3pAddContent(tabsup[0][0], '&nbsp;<u>Programme de départ</u><br>')
    this.afficheExplik(tabsup[0][0], 'Le programme que l’élève devra compléter.<br>Ne pas oublier de cliquer sur le bouton dessous pour le copier dans le programme attendu.')
    j3pAddContent(tabsup[0][0], '\n')
    j3pAjouteBouton(tabsup[0][0], this.boutonGenere.bind(this), { value: 'Copier le programme de départ dans la zone "Programme attendu" (en dessous)' })
    this.afficheExplik(tabsup[0][0], 'Une fois ce programme copié, il faut effacer l’ancien.')
    j3pAddContent(tabsup[0][0], '\n')
    const tabAddText = addDefaultTable(tabsup[0][0], 1, 2)
    j3pAddContent(tabAddText[0][0], '&nbsp;Programme de départ modifiable.&nbsp;')
    const laCoche = j3pAjouteCaseCoche(tabAddText[0][1])
    laCoche.checked = this.stor.ProgrammeMod
    laCoche.onchange = () => {
      this.stor.ProgrammeMod = laCoche.checked
    }
    j3pAddContent(tabsup[0][0], '\n')
    const tabPlace = addDefaultTable(tabsup[0][0], 2, 1)
    j3pAddContent(tabPlace[1][0], '<br>')
    j3pAjouteBouton(tabPlace[1][0], this.ajBlocPerso.bind(this), { value: '+BlocPerso' })
    this.stor.lesdivs.progdeb = tabPlace[0][0]
    j3pAddContent(tabsup[0][1], '&nbsp;&nbsp;&nbsp;')

    tabsup[0][2].style.verticalAlign = 'top'
    const taben = addDefaultTable(tabsup[0][3], 16, 1)
    j3pAddContent(taben[0][0], '<u><b>Enoncé</b></u>')
    faitAreaFacile(taben[1][0], this.stor.exoEncours, 'enonce', this.container, 300)
    this.afficheExplik(taben[1][0], 'Indiquer ci-dessus la consigne de l\'exercice.')
    const petittab0 = addDefaultTable(taben[2][0], 1, 4)
    j3pAddTxt(petittab0[0][0], 'L’élève peut voir le rendu du programme attendu&nbsp;')
    this.stor.cocheRendu1 = j3pAjouteCaseCoche(petittab0[0][1])
    this.stor.cocheRendu1.checked = this.stor.Rendu1
    j3pAddTxt(petittab0[0][2], '&nbsp;...que la fin...&nbsp;')
    this.afficheExplik(taben[2][0], 'Quand ces cases sont cochées, l’élève peut<br> voir  l’éxécution / le résultat final du programme attendu.')

    this.stor.cocheRendu2 = j3pAjouteCaseCoche(petittab0[0][3])
    this.stor.cocheRendu2.checked = this.stor.Rendu2
    this.stor.garcoX1 = petittab0[0][2]
    this.stor.garcoX2 = petittab0[0][3]
    this.stor.garcoX1.style.display = (this.stor.cocheRendu1.checked) ? '' : 'none'
    this.stor.garcoX2.style.display = (this.stor.cocheRendu1.checked) ? '' : 'none'
    this.stor.cocheRendu1.onchange = () => {
      this.stor.garcoX1.style.display = (this.stor.cocheRendu1.checked) ? '' : 'none'
      this.stor.garcoX2.style.display = (this.stor.cocheRendu1.checked) ? '' : 'none'
    }

    j3pAddContent(taben[3][0], '&nbsp;')
    j3pAddContent(taben[4][0], '<u><b>Correction</b></u>')
    this.afficheExplik(taben[4][0], 'Indiquer ci-dessous les élèments pris en compte pour que <br>la ressource juge bonne ou fausse la réponse de l’élève.')
    const petittab1 = addDefaultTable(taben[5][0], 1, 2)
    j3pAddTxt(petittab1[0][0], 'La correction prend en compte la position du lutin&nbsp;')
    this.stor.cochePosition = j3pAjouteCaseCoche(petittab1[0][1])
    this.stor.cochePosition.checked = this.stor.comptepla
    const petittab2 = addDefaultTable(taben[6][0], 1, 4)
    j3pAddTxt(petittab2[0][0], 'La correction prend en compte ce que dit le lutin&nbsp;')
    this.stor.cocheDire = j3pAjouteCaseCoche(petittab2[0][1])
    this.stor.cocheDire.checked = this.stor.comptedire
    j3pAddTxt(petittab2[0][2], '&nbsp;...que la dernière phrase&nbsp;')
    this.stor.cocheDire2 = j3pAjouteCaseCoche(petittab2[0][3])
    this.stor.cocheDire2.checked = this.stor.comptedirfin
    this.stor.garco1 = petittab2[0][2]
    this.stor.garco2 = petittab2[0][3]
    this.stor.garco1.style.display = (this.stor.cocheDire.checked) ? '' : 'none'
    this.stor.garco2.style.display = (this.stor.cocheDire.checked) ? '' : 'none'
    this.stor.cocheDire.onchange = () => {
      this.stor.garco1.style.display = (this.stor.cocheDire.checked) ? '' : 'none'
      this.stor.garco2.style.display = (this.stor.cocheDire.checked) ? '' : 'none'
    }
    const petittab3 = addDefaultTable(taben[7][0], 1, 2)
    j3pAddTxt(petittab3[0][0], 'La correction prend en compte les traces de stylo&nbsp;')
    this.stor.cocheTra = j3pAjouteCaseCoche(petittab3[0][1])
    this.stor.cocheTra.checked = this.stor.comptetrace

    const petittab4 = addDefaultTable(taben[8][0], 1, 4)
    taben[8][0].style.verticalAlign = 'top'
    petittab4[0][0].style.verticalAlign = 'top'
    petittab4[0][1].style.verticalAlign = 'top'
    j3pAddTxt(petittab4[0][0], 'La correction prend en compte le contenu de variables&nbsp;')
    this.afficheExplik(taben[8][0], 'Le programme ne peut tester que les variables créées<br> dans le programme de départ.')
    this.stor.cocheVar = j3pAjouteCaseCoche(petittab4[0][1])
    this.stor.cocheVar.checked = this.stor.comptevar.length > 0
    this.stor.garco3 = petittab4[0][2]
    this.stor.garco4 = petittab4[0][3]
    j3pAddTxt(this.stor.garco3, '&nbsp;')
    this.stor.inputVar = j3pAddElt(this.stor.garco4, 'input', { size: 16 })
    for (let i = 0; i < this.stor.comptevar.length; i++) {
      this.stor.inputVar.value += this.stor.comptevar[i] + ' '
    }
    this.afficheExplik(this.stor.garco4, 'Indiquer ici les noms<br> des variables à tester.<br>( séparées pas une espace )')

    this.stor.garco3.style.display = (this.stor.cocheVar.checked) ? '' : 'none'
    this.stor.garco4.style.display = (this.stor.cocheVar.checked) ? '' : 'none'
    this.stor.cocheVar.onchange = () => {
      this.stor.garco3.style.display = (this.stor.cocheVar.checked) ? '' : 'none'
      this.stor.garco4.style.display = (this.stor.cocheVar.checked) ? '' : 'none'
    }

    const petittab5 = addDefaultTable(taben[9][0], 1, 4)
    j3pAddTxt(petittab5[0][0], 'La correction prend en compte le nombre de blocs&nbsp;')
    this.afficheExplik(taben[9][0], 'Le programme de l’élève devra avoir au plus<br>le même nombre de blocs que le programme attendu.')
    this.stor.cochenbBlok = j3pAjouteCaseCoche(petittab5[0][1])
    this.stor.cochenbBlok.checked = this.stor.comptebloc > 0
    this.stor.garco5 = petittab5[0][2]
    this.stor.garco6 = petittab5[0][3]
    j3pAddTxt(this.stor.garco5, '&nbsp;')
    function verifNbBloc () {
      const averif = this.stor.inputnbloc.value
      if (isNaN(averif) || (averif < 1) || (averif > 300)) this.stor.inputnbloc.value = '1'
    }
    this.stor.inputnbloc = j3pAddElt(this.stor.garco6, 'input', { type: 'number', min: '1', max: '300', size: 4, onchange: verifNbBloc.bind(this) })
    this.stor.inputnbloc.value = String(this.stor.comptebloc)
    this.stor.garco5.style.display = (this.stor.cochenbBlok.checked) ? '' : 'none'
    this.stor.garco6.style.display = (this.stor.cochenbBlok.checked) ? '' : 'none'
    this.stor.cochenbBlok.onchange = () => {
      this.stor.garco5.style.display = (this.stor.cochenbBlok.checked) ? '' : 'none'
      this.stor.garco6.style.display = (this.stor.cochenbBlok.checked) ? '' : 'none'
    }

    const divCorCase = addDefaultTable(taben[9][0], 2, 1)
    const tabVoisCaseCor = addDefaultTable(divCorCase[0][0], 1, 3)
    j3pAddContent(tabVoisCaseCor[0][0], 'Corrections pour Petit Scratch ( par Cases )')
    j3pAddContent(tabVoisCaseCor[0][1], '&nbsp;&nbsp;')
    j3pAddContent(tabVoisCaseCor[0][2], '<b>+</b>')
    tabVoisCaseCor[0][2].addEventListener('click', () => {
      if (divCorCase[1][0].style.display === 'none') {
        j3pEmpty(tabVoisCaseCor[0][2])
        j3pAddContent(tabVoisCaseCor[0][2], '<b>-</b>')
        divCorCase[1][0].style.display = ''
      } else {
        j3pEmpty(tabVoisCaseCor[0][2])
        j3pAddContent(tabVoisCaseCor[0][2], '<b>+</b>')
        divCorCase[1][0].style.display = 'none'
      }
    })
    tabVoisCaseCor[0][2].style.color = '#0665fa'
    tabVoisCaseCor[0][2].style.textAlign = 'center'
    tabVoisCaseCor[0][2].style.border = '1px solid #0665fa'
    tabVoisCaseCor[0][2].style.width = '20px'
    tabVoisCaseCor[0][2].style.cursor = 'pointer'
    const tabCorC = addDefaultTable(divCorCase[1][0], 3, 2)
    j3pAddContent(tabCorC[0][0], '&nbsp;&nbsp;')
    const tabCor1 = addDefaultTable(tabCorC[0][1], 1, 3)
    j3pAddContent(tabCor1[0][0], 'Couleur de la case finale de Scratch')
    j3pAddContent(tabCor1[0][1], '&nbsp;&nbsp;')
    const cacore = j3pAjouteCaseCoche(tabCor1[0][2])
    cacore.checked = this.stor.exoEncours.corCaseCoul
    cacore.onchange = () => {
      this.stor.exoEncours.corCaseCoul = cacore.checked
    }
    const tabCor2 = addDefaultTable(tabCorC[1][1], 1, 3)
    j3pAddContent(tabCor2[0][0], 'Couleurs de toutes les cases')
    j3pAddContent(tabCor2[0][1], '&nbsp;&nbsp;')
    const cacore2 = j3pAjouteCaseCoche(tabCor2[0][2])
    cacore2.checked = this.stor.exoEncours.corCaseCoulToutes
    cacore2.onchange = () => {
      this.stor.exoEncours.corCaseCoulToutes = cacore2.checked
    }

    // a change
    divCorCase[1][0].style.display = 'none'
    if (cacore2.checked || cacore.checked) {
      divCorCase[1][0].style.display = ''
      j3pEmpty(tabVoisCaseCor[0][2])
      j3pAddContent(tabVoisCaseCor[0][2], '<b>-</b>')
    }

    j3pAddContent(taben[10][0], '&nbsp;')
    j3pAddContent(taben[11][0], '&nbsp;(?) Pour rendre un champ de block éditatble, noter <i><b>A COMPLETER </b></i> dans le champ.')
    taben[11][0].style.color = '#154b02'

    j3pAddContent(taben[12][0], '&nbsp;')
    j3pAddContent(taben[13][0], 'Réponses proposées par le testeur si présence du bloc "Demander et attendre"')
    this.stor.tabValPos = taben[14][0]
    this.affValpos()
    this.afficheExplik(taben[15][0], 'Un utilisateur virtuel va tester le programme.<br>Il demandera à l’élève de rentrer une des valeurs ci-dessus pour son test.<br>Ne pas hésiter à enregistrer beaucoup de valeurs différentes.')

    const tabsupP = addDefaultTable(this.stor.lesdivs.Programme, 2, 3)
    const laligne = j3pAddElt(this.stor.lesdivs.Programme, 'div')
    laligne.style.borderBottom = '1px dashed black'
    laligne.style.marginTop = '5px'
    laligne.style.marginBottom = '5px'
    const tabsupP2 = addDefaultTable(this.stor.lesdivs.Programme, 2, 5)
    this.stor.lesdivs.Programme.classList.add('pourFitContent')
    j3pAddContent(tabsupP[0][0], '&nbsp;<u>Programme attendu</u>')
    this.afficheExplik(tabsupP[0][0], 'La correction compare le résulat du programme attendu avec celui de l’élève.<br> En cas de mauvaise réponse de l’élève,<br> ce <b>programme attendu</b> est donné comme solution de l’exercice.')

    tabsupP[0][0].style.verticalAlign = 'top'
    tabsupP[1][0].style.verticalAlign = 'top'
    tabsupP[0][2].style.verticalAlign = 'top'
    tabsupP2[0][0].style.verticalAlign = 'top'
    this.stor.zoneAttText = tabsupP[0][0]
    const divProg = j3pAddElt(tabsupP[1][0], 'div')
    this.stor.lesdivs.programme = divProg
    this.stor.lesdivs.programme.style.verticalAlign = 'top'
    this.stor.lesdivs.programme.style.border = '1px solid black'
    j3pAddContent(tabsupP[0][1], '&nbsp;&nbsp;&nbsp;')
    j3pAddContent(tabsupP[0][2], '&nbsp;<u>Blocs disponibles</u>')
    this.afficheExplik(tabsupP[0][2], 'Blocs disponibles <br>pour l’élève.')

    this.stor.leboubput = j3pAddElt(tabsupP2[0][0], 'div')
    j3pAjouteBouton(tabsupP2[0][0], () => { this.faisProg.bind(this)('drapeau') }, { value: '⚐' })
    j3pAjouteBouton(tabsupP2[0][0], () => { this.faisProg.bind(this)('◄') }, { value: '◄' })
    j3pAjouteBouton(tabsupP2[0][0], () => { this.faisProg.bind(this)('►') }, { value: '►' })
    j3pAjouteBouton(tabsupP2[0][0], () => { this.faisProg.bind(this)('▲') }, { value: '▲' })
    j3pAjouteBouton(tabsupP2[0][0], () => { this.faisProg.bind(this)('▼') }, { value: '▼' })
    j3pAjouteBouton(tabsupP2[0][0], () => { this.faisProg.bind(this)('ª') }, { value: 'a' })
    j3pAjouteBouton(tabsupP2[0][0], () => { this.faisProg.bind(this)('_') }, { value: '▂' })
    j3pAjouteBouton(tabsupP2[0][0], () => { this.faisProg.bind(this)('sou') }, { value: '🖱' })
    this.stor.lesdivs.listeBloc = tabsupP[1][2]
    this.stor.lesdivs.listeBloc.style.verticalAlign = 'top'
    tabsupP2[0][2].style.verticalAlign = 'top'
    const rr = addDefaultTable(tabsupP2[1][0], 4, 1)
    this.stor.mypre = rr[1][0]
    tabsupP2[1][0].style.verticalAlign = 'top'
    this.stor.doudi = rr[0][0]
    this.stor.doudemande = rr[2][0]
    const yy = addDefaultTable(rr[3][0], 1, 2)
    j3pAddContent(yy[0][0], '&nbsp;<u>nombre de blocs</u>&nbsp')
    this.stor.whereafnbbloc = yy[0][1]
    this.affichelisteBlocs()
    this.stor.doudi.style.border = '1px solid black'

    const tabTitreFond = j3pAddElt(tabsupP2[0][2], 'div')
    j3pAddContent(tabsupP2[0][1], '&nbsp;&nbsp;')
    j3pAddContent(tabsupP2[0][3], '&nbsp;&nbsp;')
    j3pAddContent(tabTitreFond, '<b>Fond</b>')
    j3pAddContent(tabTitreFond, '&nbsp;&nbsp;')
    const boutAucun = j3pAjouteBouton(tabTitreFond, aucunFond.bind(this), { value: 'Aucun' })
    j3pAddContent(tabTitreFond, '&nbsp;&nbsp;')
    const boutaxe = j3pAjouteBouton(tabTitreFond, axenFond.bind(this), { value: 'Axe' })
    j3pAddContent(tabTitreFond, '&nbsp;&nbsp;')
    const boutaxe2 = j3pAjouteBouton(tabTitreFond, axenFondFixe.bind(this), { value: 'Axe Fixe' })
    j3pAddContent(tabTitreFond, '&nbsp;&nbsp;')
    const boutcases = j3pAjouteBouton(tabTitreFond, caseFond.bind(this), { value: 'Case' })
    j3pAddContent(tabTitreFond, '&nbsp;&nbsp;')
    const boutImage = j3pAjouteBouton(tabTitreFond, imageFon.bind(this), { value: 'Image' })
    const tabFondImage = j3pAddElt(tabsupP2[1][2], 'div')
    tabFondImage.style.border = '1px solid black'
    const tabFondAxe = j3pAddElt(tabsupP2[1][2], 'div')
    tabFondAxe.style.border = '1px solid black'
    tabsupP2[1][2].style.verticalAlign = 'top'
    const _choixFond = choixFond.bind(this)
    const _afficheAxe = afficheAxe.bind(this)
    const _afficheCases = afficheCases.bind(this)
    function aucunFond () {
      this.stor.exoEncours.imageFond = { type: 'aucun' }
      _choixFond()
    }
    function axenFond () {
      this.stor.exoEncours.imageFond = { type: 'axe', points: [], txtFigure: axeIm }
      _choixFond()
    }
    function axenFondFixe () {
      this.stor.exoEncours.imageFond = { type: 'axe fixe', txtFigure: axeImFixe, width: 300, height: 300 }
      _choixFond()
    }
    function caseFond () {
      this.stor.exoEncours.imageFond = { type: 'cases', cases: [], txtFigure: caseIm, width: 300, height: 300 }
      _choixFond()
    }
    function imageFon () {
      this.stor.exoEncours.imageFond = { type: 'image', txtFigure: genIm, width: 300, height: 300 }
      _choixFond()
    }
    function afficheAxe () {
      j3pEmpty(tabFondAxe)
      for (let i = 0; i < this.stor.exoEncours.imageFond.points.length; i++) {
        const spanP = addDefaultTable(tabFondAxe, 1, 6)
        j3pAddContent(spanP[0][0], '( ' + this.stor.exoEncours.imageFond.points[i].x + ' ; ' + this.stor.exoEncours.imageFond.points[i].y + ' )&nbsp;&nbsp;')
        const bsens1 = j3pAjouteBouton(spanP[0][3], () => {
          this.stor.exoEncours.imageFond.points[i].sens1 = !this.stor.exoEncours.imageFond.points[i].sens1
          bsens1.value = (this.stor.exoEncours.imageFond.points[i].sens1) ? '⇒' : '⇐'
          this.montreDebProg()
        }, { value: (this.stor.exoEncours.imageFond.points[i].sens1) ? '⇒' : '⇐' })
        const bsens2 = j3pAjouteBouton(spanP[0][1], () => {
          this.stor.exoEncours.imageFond.points[i].sens2 = !this.stor.exoEncours.imageFond.points[i].sens2
          bsens2.value = (this.stor.exoEncours.imageFond.points[i].sens2) ? '⇑' : '⇓'
          this.montreDebProg()
        }, { value: (this.stor.exoEncours.imageFond.points[i].sens2) ? '⇑' : '⇓' })
        j3pAjouteBouton(spanP[0][5], () => {
          this.stor.exoEncours.imageFond.points.splice(i, 1)
          _afficheAxe()
        }, { value: 'Suppr' })
        j3pAddContent(spanP[0][2], '&nbsp;&nbsp;')
        j3pAddContent(spanP[0][4], '&nbsp;&nbsp;')
      }
      j3pAddContent(tabFondAxe, '<br><br>')
      const tabPlus = addDefaultTable(tabFondAxe, 1, 6)
      j3pAddContent(tabPlus[0][0], ' (&nbsp;')
      const inupx = j3pAddElt(tabPlus[0][1], 'input', {
        type: 'number',
        size: 4
      })
      j3pAddContent(tabPlus[0][2], ' &nbsp;;&nbsp;')
      const inupy = j3pAddElt(tabPlus[0][3], 'input', {
        type: 'number',
        size: 4
      })
      j3pAddContent(tabPlus[0][4], ' &nbsp;)&nbsp;')
      j3pAjouteBouton(tabPlus[0][5], () => {
        if (inupx.value === '') return
        if (inupy.value === '') return
        this.stor.exoEncours.imageFond.points.push({ x: inupx.value, y: inupy.value, sens1: false, sens2: false })
        _afficheAxe()
      }, { value: '+' })
      this.montreDebProg()
      // affiche liste points et dessine les
      // met ajoute et supprime poits
    }
    function afficheCases (ou) {
      j3pEmpty(ou)
      j3pAddContent(ou, '<i>A utiliser avec le Lutin "<b>Petit Scratch</b>",<br> et les blocs "<b>... PAR CASE</b>"</i><br>')
      for (let i = 0; i < this.stor.exoEncours.imageFond.cases.length; i++) {
        const spanP = addDefaultTable(ou, 1, 4)
        let coo = '( [ ' + this.stor.exoEncours.imageFond.cases[i].cox[0] + ' '
        for (let j = 1; j < this.stor.exoEncours.imageFond.cases[i].cox.length; j++) {
          coo += '; ' + this.stor.exoEncours.imageFond.cases[i].cox[j] + ' '
        }
        coo += '] , [ ' + this.stor.exoEncours.imageFond.cases[i].coy[0] + ' '
        for (let j = 1; j < this.stor.exoEncours.imageFond.cases[i].coy.length; j++) {
          coo += '; ' + this.stor.exoEncours.imageFond.cases[i].coy[j] + ' '
        }
        coo += '] )&nbsp;&nbsp;'
        j3pAddContent(spanP[0][0], coo)
        const tabCoul = addDefaultTable(spanP[0][1], 1, 2 + this.stor.exoEncours.imageFond.cases[i].coul.length)
        j3pAddContent(tabCoul[0][0], '[')
        j3pAddContent(tabCoul[0][1 + this.stor.exoEncours.imageFond.cases[i].coul.length], ']')
        for (let j = 0; j < this.stor.exoEncours.imageFond.cases[i].coul.length; j++) {
          const divc = j3pAddElt(tabCoul[0][j + 1], 'div')
          j3pAddContent(divc, '&nbsp;&nbsp;')
          divc.style.maxHeight = '25px'
          divc.style.border = '1px solid black'
          divc.style.background = this.scratch.fileCouleur(this.stor.exoEncours.imageFond.cases[i].coul[j])
        }
        j3pAddContent(spanP[0][2], '&nbsp;&nbsp;')
        j3pAjouteBouton(spanP[0][3], () => {
          this.stor.exoEncours.imageFond.cases.splice(i, 1)
          _afficheCases(ou)
        }, { value: 'Suppr' })
      }
      j3pAddContent(ou, '<br><br>')
      const tabPlus = addDefaultTable(ou, 1, 25)
      j3pAddContent(tabPlus[0][0], ' (&nbsp;[&nbsp;')
      const bx1 = this.faisPropre(tabPlus[0][1], '1')
      const bx2 = this.faisPropre(tabPlus[0][2], '2')
      const bx3 = this.faisPropre(tabPlus[0][3], '3')
      const bx4 = this.faisPropre(tabPlus[0][4], '4')
      const bx5 = this.faisPropre(tabPlus[0][5], '5')
      const bx6 = this.faisPropre(tabPlus[0][6], '6')
      j3pAddContent(tabPlus[0][7], '&nbsp;] ,[&nbsp;')
      const by1 = this.faisPropre(tabPlus[0][8], '1')
      const by2 = this.faisPropre(tabPlus[0][9], '2')
      const by3 = this.faisPropre(tabPlus[0][10], '3')
      const by4 = this.faisPropre(tabPlus[0][11], '4')
      const by5 = this.faisPropre(tabPlus[0][12], '5')
      const by6 = this.faisPropre(tabPlus[0][13], '6')
      j3pAddContent(tabPlus[0][14], '&nbsp;] )&nbsp;')
      const c1 = this.faisPropre(tabPlus[0][15], 'jaune')
      const c2 = this.faisPropre(tabPlus[0][16], 'bleue')
      const c3 = this.faisPropre(tabPlus[0][17], 'rouge')
      const c4 = this.faisPropre(tabPlus[0][18], 'verte')
      const c5 = this.faisPropre(tabPlus[0][19], 'noire')
      const c6 = this.faisPropre(tabPlus[0][20], 'orange')
      const c7 = this.faisPropre(tabPlus[0][21], 'rose')
      const c8 = this.faisPropre(tabPlus[0][22], 'violette')
      j3pAddContent(tabPlus[0][23], '&nbsp;;&nbsp;')
      j3pAjouteBouton(tabPlus[0][24], () => {
        const cox = []
        if (bx1.select) cox.push(1)
        if (bx2.select) cox.push(2)
        if (bx3.select) cox.push(3)
        if (bx4.select) cox.push(4)
        if (bx5.select) cox.push(5)
        if (bx6.select) cox.push(6)
        if (cox.length === 0) return
        const coy = []
        if (by1.select) coy.push(1)
        if (by2.select) coy.push(2)
        if (by3.select) coy.push(3)
        if (by4.select) coy.push(4)
        if (by5.select) coy.push(5)
        if (by6.select) coy.push(6)
        if (coy.length === 0) return
        const coul = []
        if (c1.select) coul.push('jaune')
        if (c2.select) coul.push('bleue')
        if (c3.select) coul.push('rouge')
        if (c4.select) coul.push('verte')
        if (c5.select) coul.push('noire')
        if (c6.select) coul.push('orange')
        if (c7.select) coul.push('rose')
        if (c8.select) coul.push('violette')
        if (coul.length === 0) return
        this.stor.exoEncours.imageFond.cases.push({ cox, coy, coul })
        _afficheCases(ou)
      }, { value: '+' })
      j3pAddContent(ou, '<br><br>')
      j3pAjouteBouton(ou, this.montreDebProg.bind(this), { value: 'actualise' })
      this.montreDebProg()
      // affiche liste points et dessine les
      // met ajoute et supprime poits
    }
    function choixFond () {
      switch (this.stor.exoEncours.imageFond.type) {
        case 'aucun':
          boutAucun.disabled = true
          boutaxe.disabled = false
          boutcases.disabled = false
          boutaxe2.disabled = false
          boutImage.disabled = false
          tabFondImage.style.display = 'none'
          tabFondAxe.style.display = 'none'
          break
        case 'axe fixe':
          boutcases.disabled = false
          boutAucun.disabled = false
          boutImage.disabled = false
          boutaxe.disabled = false
          boutaxe2.disabled = true
          tabFondImage.style.display = ''
          tabFondAxe.style.display = 'none'
          j3pEmpty(tabFondImage)
          j3pAddContent(tabFondImage, '<b>Remarque</b><br><br> Le décalage en abscisse de l\'axe <br>dans la zone de test ( à gauche)<br>disparait dans la ressource élève. ')
          break
        case 'image':
          boutcases.disabled = false
          boutAucun.disabled = false
          boutImage.disabled = true
          boutaxe.disabled = false
          boutaxe2.disabled = false
          tabFondImage.style.display = ''
          tabFondAxe.style.display = 'none'
          j3pEmpty(tabFondImage)
          importImage(tabFondImage, this.stor.exoEncours.imageFond, 'txtFigure', 'width', 'height', true, null, true)
          break
        case 'axe':
          boutcases.disabled = false
          boutAucun.disabled = false
          boutaxe.disabled = true
          boutImage.disabled = false
          boutaxe2.disabled = false
          tabFondImage.style.display = 'none'
          tabFondAxe.style.display = ''
          _afficheAxe()
          break
        case 'cases':
          boutcases.disabled = true
          boutAucun.disabled = false
          boutaxe.disabled = false
          boutImage.disabled = false
          boutaxe2.disabled = false
          tabFondImage.style.display = ''
          tabFondAxe.style.display = 'none'
          _afficheCases(tabFondImage)
          break
      }
      this.montreDebProg()
    }
    _choixFond()

    const tabTitreLutin = j3pAddElt(tabsupP2[0][4], 'div')
    const afLutin = j3pAddElt(tabsupP2[1][4], 'div')
    afLutin.style.border = '1px solid black'
    tabsupP2[1][4].style.verticalAlign = 'top'
    afLutin.style.padding = '5px'
    j3pAddContent(tabTitreLutin, '<b>Lutin</b>')
    j3pAddContent(tabTitreLutin, '&nbsp;&nbsp;')
    const boutScratch = j3pAjouteBouton(tabTitreLutin, () => {
      this.stor.exoEncours.lutinScratch = false
      _choixLut()
    }, { value: 'Scratch' })
    j3pAddContent(tabTitreLutin, '&nbsp;&nbsp;')
    const boutPetitScratch = j3pAjouteBouton(tabTitreLutin, () => {
      this.stor.exoEncours.lutinScratch = 'Petit Scratch'
      _choixLut()
    }, { value: 'Petit Scratch' })
    j3pAddContent(tabTitreLutin, '&nbsp;&nbsp;')
    const boutMain = j3pAjouteBouton(tabTitreLutin, () => {
      this.stor.exoEncours.lutinScratch = true
      _choixLut()
    }, { value: 'Main' })
    const _choixLut = choixLut.bind(this)
    function choixLut () {
      j3pEmpty(afLutin)
      if (this.stor.exoEncours.lutinScratch === false) {
        boutScratch.disabled = true
        boutMain.disabled = false
        boutPetitScratch.disabled = false
        const imm = j3pAddElt(afLutin, 'img', '', { src: chatImg, width: 50, height: 50 })
        imm.style.width = '100px'
        imm.style.height = '100px'

        const tabLutCoo = addDefaultTable(afLutin, 1, 5)
        j3pAddContent(tabLutCoo[0][0], 'Place:&nbsp;')
        if (!this.stor.exoEncours.coxLut) this.stor.exoEncours.coxLut = 3
        if (!this.stor.exoEncours.coyLut) this.stor.exoEncours.coyLut = 3
        const cox = j3pAddElt(tabLutCoo[0][1], 'input', { type: 'number', min: '1', max: '6', size: 4, onchange: verifCox.bind(this), value: String(this.stor.exoEncours.coxLut) })
        cox.addEventListener('input', verifCox.bind(this))
        function verifCox () {
          const averif = cox.value
          if (averif === '' || isNaN(averif) || Number(averif) < 1 || Number(averif) > 6) { cox.value = '3' }
          this.stor.exoEncours.coxLut = Number(cox.value)
          this.montreDebProg()
        }
        j3pAddContent(tabLutCoo[0][2], '&nbsp;;&nbsp;')
        const coy = j3pAddElt(tabLutCoo[0][3], 'input', { type: 'number', min: '1', max: '6', size: 4, onchange: verifCoy.bind(this), value: String(this.stor.exoEncours.coyLut) })
        coy.addEventListener('input', verifCoy.bind(this))
        function verifCoy () {
          const averif = coy.value
          if (averif === '' || isNaN(averif) || Number(averif) < 1 || Number(averif) > 6) { coy.value = '3' }
          this.stor.exoEncours.coyLut = Number(coy.value)
          this.montreDebProg()
        }
      } else if (this.stor.exoEncours.lutinScratch === true) {
        boutScratch.disabled = false
        boutMain.disabled = true
        boutPetitScratch.disabled = false
        const imm = j3pAddElt(afLutin, 'img', '', { src: dessinleveImg, width: 50, height: 50 })
        imm.style.width = '100px'
        imm.style.height = '100px'
      } else {
        boutScratch.disabled = false
        boutMain.disabled = false
        boutPetitScratch.disabled = true
        const imm = j3pAddElt(afLutin, 'img', '', { src: chatImg, width: 25, height: 25 })
        imm.style.width = '50px'
        imm.style.height = '50px'
        j3pAddContent(afLutin, '<i>A utiliser avec le fond "<b>Case</b>",<br> et les blocs "<b>... PAR CASE</b>"</i><br>')
        const tabLutCoo = addDefaultTable(afLutin, 1, 5)
        j3pAddContent(tabLutCoo[0][0], 'Case (&nbsp;')
        if (!this.stor.exoEncours.coxLut) this.stor.exoEncours.coxLut = 3
        if (!this.stor.exoEncours.coyLut) this.stor.exoEncours.coyLut = 3
        const cox = j3pAddElt(tabLutCoo[0][1], 'input', { type: 'number', min: '1', max: '6', size: 4, onchange: verifCox.bind(this), value: String(this.stor.exoEncours.coxLut) })
        cox.addEventListener('input', verifCox.bind(this))
        function verifCox () {
          const averif = cox.value
          if (averif === '' || isNaN(averif) || Number(averif) < 1 || Number(averif) > 6) { cox.value = '3' }
          this.stor.exoEncours.coxLut = Number(cox.value)
          this.montreDebProg()
        }
        j3pAddContent(tabLutCoo[0][2], '&nbsp;;&nbsp;')
        const coy = j3pAddElt(tabLutCoo[0][3], 'input', { type: 'number', min: '1', max: '6', size: 4, onchange: verifCoy.bind(this), value: String(this.stor.exoEncours.coyLut) })
        coy.addEventListener('input', verifCoy.bind(this))
        function verifCoy () {
          const averif = coy.value
          if (averif === '' || isNaN(averif) || Number(averif) < 1 || Number(averif) > 6) { coy.value = '3' }
          this.stor.exoEncours.coyLut = Number(coy.value)
          this.montreDebProg()
        }
        j3pAddContent(tabLutCoo[0][4], '&nbsp;)')
      }
      this.montreDebProg()
    }
    _choixLut()
  }

  montreDebProg () {
    this.scratch.params.grosStop = true
    const ffunc = () => {
      this.scratch.params.grosStop = false
      this.faisProg('queDeb')
    }
    setTimeout(ffunc.bind(this), 1001)
  }

  faisPropre (uu, n) {
    const ki = j3pAddElt(uu, 'div')
    uu.style.paddingLeft = '2px'
    uu.style.paddingRight = '2px'
    ki.style.border = '1px solid black'
    ki.style.cursor = 'pointer'
    ki.style.maxHeight = '25px'
    ki.select = false
    if (n.length === 1) {
      j3pAddContent(ki, n)
    } else {
      j3pAddContent(ki, '&nbsp;&nbsp;')
      ki.style.background = this.scratch.fileCouleur(n)
    }
    ki.addEventListener('mouseover', () => { ki.style.border = '1px solid grey' })
    ki.addEventListener('mouseout', () => {
      if (ki.select) {
        ki.style.border = '1px solid red'
      } else {
        ki.style.border = '1px solid black'
      }
    })
    ki.addEventListener('click', () => {
      ki.select = !ki.select
      if (ki.select) {
        ki.style.border = '1px solid red'
      } else {
        ki.style.border = '1px solid black'
      }
    })
    return ki
  }

  affValpos () {
    j3pEmpty(this.stor.tabValPos)
    if (!this.stor.exoEncours.listValPos) this.stor.exoEncours.listValPos = []
    for (let i = 0; i < this.stor.exoEncours.listValPos.length; i++) {
      const ouaf = j3pAddElt(this.stor.tabValPos, 'span')
      const inputVal = j3pAddElt(ouaf, 'input', {
        value: this.stor.exoEncours.listValPos[i],
        size: 9
      })
      inputVal.addEventListener('change', () => {
        this.stor.exoEncours.listValPos[i] = inputVal.value
      })
      j3pAddContent(ouaf, '&nbsp;')
      const boutCut = j3pAjouteBouton(ouaf, () => {
        this.stor.exoEncours.listValPos.splice(i, 1)
        this.affValpos()
      }, { value: '✄' })
      boutCut.addEventListener('mouseover', () => {
        inputVal.style.background = '#f00'
      })
      j3pAddContent(ouaf, '&nbsp;')
      boutCut.addEventListener('mouseout', () => {
        inputVal.style.background = ''
      })
      j3pAddContent(ouaf, '&nbsp;')
      if ((i + 1) % 4 === 0) j3pAddContent(ouaf, '<br>')
    }
    j3pAjouteBouton(this.stor.tabValPos, () => {
      this.stor.exoEncours.listValPos.push('')
      this.affValpos()
    }, { value: '+' })
  }

  faisMenu () {
    let montabMenu = []
    for (let i = 0; i < this.stor.listeBloc.length; i++) {
      let cmpt = 0
      for (let j = 0; j < this.stor.listeBloc[i].contenu.length; j++) {
        this.stor.listeBloc[i].contenu[j].aff = this.stor.listeCocheBloc[i][j].checked
        if (this.stor.listeCocheBloc[i][j].checked) cmpt++
      }
      if (cmpt !== 0) {
        montabMenu.push(j3pClone(this.stor.listeBloc[i]))
        for (let j = montabMenu[montabMenu.length - 1].contenu.length - 1; j > -1; j--) {
          if (!this.stor.listeCocheBloc[i][j].checked) montabMenu[montabMenu.length - 1].contenu.splice(j, 1)
        }
      }
      if (i === this.stor.listeBloc.length - 1) {
        this.stor.listeBloc[i].aff = this.stor.listeCocheBlocVar.checked
        if (this.stor.listeBloc[i].aff) montabMenu.push(j3pClone(this.stor.listeBloc[i]))
      }
    }
    if (montabMenu.length === 0) {
      montabMenu = j3pClone(this.menuChoixMin2) // pourquoi cloner ?
    }
    const xml2 = this.scratch.scratchCreeMenu(montabMenu, true)
    const xml2Doc = $.parseXML(xml2)
    let xmll2 = xml2Doc.firstChild
    while (xmll2.nodeType !== 1) {
      xmll2 = xmll2.nextSibling
    }
    return xmll2
  }

  faisMenuBase () {
    const xml2 = this.scratch.scratchCreeMenu(this.stor.listeBloc, true)
    const xml2Doc = $.parseXML(xml2)
    let xmll2 = xml2Doc.firstChild
    while (xmll2.nodeType !== 1) {
      xmll2 = xmll2.nextSibling
    }
    return xmll2
  }

  faisProg (koi) {
    if (this.paspret) return
    this.scratch.params.workspaceEl = this.scratch.params.scratchProgDonnees.workspace
    this.scratch.params.scratchProgDonnees.runable = true
    const nnnb = this.scratch.decode()
    this.scratch.params.scratchProgDonnees.tabcodeX = this.scratch.scratchRunBlockly(this.scratch.params.scratchProgDonnees.workspace)
    const trucAlance = []
    for (let i = 0; i < this.scratch.params.scratchProgDonnees.tabcodeX.length; i++) {
      if ((this.scratch.params.scratchProgDonnees.tabcodeX[i][0].type === 'event') && (this.scratch.params.scratchProgDonnees.tabcodeX[i][0].eventtype === koi)) {
        trucAlance.push(this.scratch.params.scratchProgDonnees.tabcodeX[i])
      }
    }
    if (trucAlance.length === 0 && (koi !== 'queDeb')) return
    this.stor.leboubput.style.display = 'none'
    this.scratch.lesdiv = {}
    this.scratch.params.mtgEditoText = this.stor.doudi
    j3pEmpty(this.stor.doudi)
    if (koi !== 'queDeb') {
      j3pAddContent(this.stor.doudi, 'Programme en cours d’éxécution<br><br>')
    }
    j3pEmpty(this.stor.whereafnbbloc)
    j3pAddTxt(this.stor.whereafnbbloc, String(nnnb))
    this.scratch.params.scratchProgDonnees.lesidprog = []
    this.scratch.params.scratchProgDonnees.progencours = []
    this.scratch.params.NbEssaiIllimite = true
    this.scratch.params.scratchProgDonnees.pause = false
    this.scratch.params.scratchProgDonnees.mypre = this.stor.mypre
    this.stor.mypre.style.background = '#fff'
    this.stor.mypre.style.border = '2px solid black'
    this.scratch.params.scratchProgDonnees.Lutins[0].coxbase = 100
    this.scratch.params.scratchProgDonnees.Lutins[0].coybase = 100
    this.scratch.params.ModeSequence = this.stor.ParamSequence.reponse === 'Oui'
    this.scratch.params.yaFond = this.stor.exoEncours.imageFond.type !== 'aucun'
    this.scratch.params.lefond = this.stor.exoEncours.imageFond.txtFigure
    this.scratch.params.figureAfaire = []
    if (this.stor.exoEncours.imageFond.type === 'axe') {
      for (let i = 0; i < this.stor.exoEncours.imageFond.points.length; i++) {
        this.scratch.params.figureAfaire.push({ type: 'croix', x: this.stor.exoEncours.imageFond.points[i].x, y: this.stor.exoEncours.imageFond.points[i].y, sens1: this.stor.exoEncours.imageFond.points[i].sens1, sens2: this.stor.exoEncours.imageFond.points[i].sens2 })
      }
    }
    if (this.stor.exoEncours.imageFond.type === 'cases') {
      for (let i = 0; i < this.stor.exoEncours.imageFond.cases.length; i++) {
        const Kool = []
        for (let j = 0; j < this.stor.exoEncours.imageFond.cases[i].coul.length; j++) {
          Kool.push(this.scratch.fileCouleur(this.stor.exoEncours.imageFond.cases[i].coul[j]))
        }
        this.scratch.params.figureAfaire.push({
          type: 'Cases',
          x: this.stor.exoEncours.imageFond.cases[i].cox,
          y: this.stor.exoEncours.imageFond.cases[i].coy,
          width: 50,
          height: 50,
          coul: Kool
        })
      }
    }
    this.scratch.params.scratchProgDonnees.Lutins[0].img = (!this.stor.exoEncours.lutinScratch || this.stor.exoEncours.lutinScratch === 'Petit Scratch') ? chatImg : dessinleveImg
    if (this.stor.exoEncours.lutinScratch === 'Petit Scratch') {
      this.scratch.params.scratchProgDonnees.Lutins[0].larg = 50
      this.scratch.params.scratchProgDonnees.Lutins[0].haut = 50
      this.scratch.params.scratchProgDonnees.Lutins[0].coxbase = (this.stor.exoEncours.coxLut - 1) * 50
      this.scratch.params.scratchProgDonnees.Lutins[0].coybase = (this.stor.exoEncours.coyLut - 1) * 50
    } else {
      this.scratch.params.scratchProgDonnees.Lutins[0].larg = 100
      this.scratch.params.scratchProgDonnees.Lutins[0].haut = 100
      if (!this.stor.exoEncours.lutinScratch) {
        this.scratch.params.scratchProgDonnees.Lutins[0].coxbase = (this.stor.exoEncours.coxLut - 1) * 40
        this.scratch.params.scratchProgDonnees.Lutins[0].coybase = 100 + (this.stor.exoEncours.coyLut - 1) * 20
      }
    }
    this.scratch.scratchReinitPartiel()
    this.scratch.lesdivs.sortie3 = this.stor.doudemande
    this.scratch.params.scratchProgDonnees.avertissements = []
    if (koi !== 'queDeb') {
      this.scratch.execProg2(trucAlance)
    } else {
      this.scratch.traceSortieBase({ sortie: this.stor.mypre })
    }
  }

  majMenu () {
    const xml = this.faisMenu()
    this.scratch.params.scratchProgDonnees.workspace.updateToolbox(xml)
  }

  modifExo (i) {
    super.modifExo(i)
    if (this.stor.Programme[i].imageFond) {
      this.stor.exoEncours.imageFond = j3pClone(this.stor.Programme[i].imageFond)
    } else {
      this.stor.exoEncours.imageFond = { type: 'aucun' }
    }
    this.stor.exoEncours.lutinScratch = (this.stor.Programme[i].lutinScratch) ? this.stor.Programme[i].lutinScratch : false
    this.stor.listeBloc = j3pClone(this.stor.exoEncours.listeBloc)
    const blovcsTot = getListeBloc()
    for (let i = 0; i < blovcsTot.length; i++) {
      let gtrouv = -1
      for (let j = 0; j < this.stor.listeBloc.length; j++) {
        if (blovcsTot[i].nom === this.stor.listeBloc[j].nom) gtrouv = i
      }
      if (gtrouv === -1) {
        this.stor.listeBloc.push(j3pClone(blovcsTot[i]))
      } else {
        for (let k = 0; k < blovcsTot[i].contenu.length; k++) {
          let gtrouv2 = false
          for (let u = 0; u < this.stor.listeBloc[gtrouv].contenu.length; u++) {
            if (blovcsTot[i].contenu[k].type === this.stor.listeBloc[gtrouv].contenu[u].type) gtrouv2 = true
          }
          if (!gtrouv2) {
            this.stor.listeBloc[gtrouv].contenu.push(j3pClone(blovcsTot[i].contenu[k]))
          }
        }
      }
    }
    this.stor.exoEncours.coxLut = this.stor.Programme[i].coxLut
    this.stor.exoEncours.coyLut = this.stor.Programme[i].coyLut
    this.stor.ProgDeb = j3pClone(this.stor.exoEncours.ProgDeb)
    this.stor.ProgrammeAt = j3pClone(this.stor.exoEncours.ProgrammeAt)
    this.stor.enonceEncours = this.stor.exoEncours.enonce
    this.stor.comptedire = this.stor.exoEncours.comptedire
    this.stor.comptedirfin = this.stor.exoEncours.comptedirfin
    this.stor.comptepla = this.stor.exoEncours.comptepla
    this.stor.comptetrace = this.stor.exoEncours.comptetrace
    this.stor.comptevar = j3pClone(this.stor.exoEncours.comptevar)
    this.stor.comptebloc = this.stor.exoEncours.comptebloc
    this.stor.Rendu1 = this.stor.exoEncours.Rendu1
    this.stor.Rendu2 = this.stor.exoEncours.Rendu2
    this.stor.ProgrammeMod = this.stor.exoEncours.ProgrammeMod
    this.stor.listValPos = j3pClone(this.stor.exoEncours.listValPos)
    this.stor.exoEncours.corCaseCoul = this.stor.Programme[i].corCaseCoul
    this.stor.exoEncours.corCaseCoulToutes = this.stor.Programme[i].corCaseCoulToutes
    this.cestParti()
  }

  nettoieId (x) {
    let compt = 0
    let ok = false
    let arnev = ''
    let id = 0
    do {
      const ouch = x.substring(compt)
      const where = ouch.indexOf('id="')
      if (where === -1) {
        arnev += ouch
        ok = true
      } else {
        arnev += ouch.substring(0, where + 4) + 'AA' + id + '"'
        const ouch2 = ouch.substring(where + 4)
        const dep = ouch2.indexOf('"')
        compt += where + 4 + dep + 1
        id++
      }
    } while (!ok)
    arnev = arnev.replace('x="20"', 'x="50"')
    return arnev
  }

  refaisMenu () {
    this.scratch.params.scratchProgDonnees.menu = []

    const lib = this.stor.exoEncours.listeBloc
    if (lib.pointLibreAvecNom || lib.pointIntersection || lib.pointSur || lib.pointMilieu) {
      this.scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'POINTS', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
      if (lib.pointLibreAvecNom) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointLibreAvecNom', value: [{ nom: 'NOM', contenu: { valeur: 'A' } }] })
      if (lib.pointIntersection) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointIntersection', value: [{ nom: 'NOM', contenu: { valeur: 'A' } }, { nom: 'CONT1', contenu: { valeur: ' ' } }, { nom: 'CONT2', contenu: { valeur: ' ' } }] })
      if (lib.pointSur) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointSur', value: [{ nom: 'NOM', contenu: { valeur: 'A' } }, { nom: 'CONT', contenu: { valeur: ' ' } }] })
      if (lib.pointMilieu) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointMilieu', value: [{ nom: 'NOM', contenu: { valeur: 'I' } }, { nom: 'CONT', contenu: { valeur: 'AB' } }] })
    }

    if (lib.segmentAB || lib.segmentlongA) {
      this.scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'SEGMENTS', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
      if (lib.segmentAB) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'segmentAB', value: [{ nom: 'EXT1', contenu: { valeur: 'AB' } }] })
      if (lib.segmentlongA) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'segmentlongA', value: [{ nom: 'EXT1', contenu: { valeur: 'AB' } }, { nom: 'LONG', contenu: { valeur: '3' } }] })
    }

    if (lib.droitept || lib.droitepara || lib.droiteperp) {
      this.scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DROITES', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
      if (lib.droitept) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'droitept', value: [{ nom: 'POINT1', contenu: { valeur: 'AB' } }] })
      if (lib.droitepara) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'droitepara', value: [{ nom: 'NOM', contenu: { valeur: '(d1)' } }, { nom: 'PARA', contenu: { valeur: '(AB)' } }, { nom: 'POINT', contenu: { valeur: 'C' } }] })
      if (lib.droiteperp) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'droiteperp', value: [{ nom: 'NOM', contenu: { valeur: '(d1)' } }, { nom: 'PERP', contenu: { valeur: '(AB)' } }, { nom: 'POINT', contenu: { valeur: 'C' } }] })
    }

    if (lib.demidroitept) {
      this.scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEMI-DROITES', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
      this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'demidroitept', value: [{ nom: 'ORIGINE', contenu: { valeur: 'AB' } }] })
    }

    if (lib.cercleCentrePoint || lib.cercleCentreRayon) {
      this.scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CERCLES', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
      if (lib.cercleCentrePoint) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'cercleCentrePoint', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'POINT', contenu: { valeur: 'A' } }, { nom: 'NOM', contenu: { valeur: 'C1' } }] })
      if (lib.cercleCentreRayon) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'cercleCentreRayon', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'RAYON', contenu: { valeur: '3' } }, { nom: 'NOM', contenu: { valeur: 'C1' } }] })
    }

    if (lib.arc1PointPasse || lib.arccentreplus || lib.arccentremoins) {
      this.scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'ARC DE CERCLES', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
      if (lib.arc1PointPasse) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'arc1PointPasse', value: [{ nom: 'POINT', contenu: { valeur: 'M' } }, { nom: 'NOM', contenu: { valeur: 'BC' } }] })
      if (lib.arccentreplus) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'arccentreplus', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'NOM', contenu: { valeur: 'AB' } }] })
      if (lib.arccentremoins) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'arccentremoins', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'NOM', contenu: { valeur: 'AB' } }] })
    }

    if (lib.effacer || lib.renommer || lib.regroupe || lib.ecriSeg || lib.ecriDroite || lib.ecriDDroite || lib.nomAleatoire) {
      this.scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'UTILITAIRES', coul1: '#B5E61D', coul2: '#3373CC', contenu: [] })
      if (lib.effacer) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'mtgEffacer', value: [{ nom: 'NOM', contenu: { valeur: ' ' } }] })
      if (lib.renommer) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'Renommer', value: [{ nom: 'NOM', contenu: { valeur: ' ' } }, { nom: 'NOM2', contenu: { valeur: ' ' } }] })
      if (lib.regroupe) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'Regroupe', value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }] })
      if (lib.ecriSeg) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'ecriSeg', value: [{ nom: 'A', contenu: { valeur: ' ' } }] })
      if (lib.ecriDroite) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'ecriDroite', value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }] })
      if (lib.ecriDDroite) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'ecriDDroite', value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }] })
      if (lib.nomAleatoire) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'NomAleatoire' })
    }

    if (this.scratch.params.lesBlocPerso.length > 0) {
      this.scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'MES BLOCS', coul1: '#D14C7D', coul2: '#B8436E', contenu: [] })
      for (let i = 0; i < this.scratch.params.lesBlocPerso.length; i++) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: this.scratch.params.lesBlocPerso[i] + 'b', value: this.scratch.params.listeDef[this.scratch.params.lesBlocPerso[i]].value })
      }
    }

    const x = this.scratch.scratchCreeMenu(this.scratch.params.scratchProgDonnees.menu, true)
    const xmlDoc = $.parseXML(x)
    let xml = xmlDoc.firstChild
    while (xml.nodeType !== 1) {
      xml = xml.nextSibling
    }
    this.scratch.params.scratchProgDonnees.workspace.updateToolbox(xml)
  }

  refaisProg (xmlDoc, ou) {
    let xml = xmlDoc.firstChild
    while (xml.nodeType !== 1) {
      xml = xml.nextSibling
    }
    ou.clear()
    this.scratch.Blockly.Xml.domToWorkspace(xml, ou)
  }

  valModifExo () {
    const Blockly = this.scratch.Blockly
    const lxml = Blockly.Xml.workspaceToDom(this.scratch.params.scratchProgDonnees.workspace2)
    const lxmlTrav = Blockly.Xml.workspaceToDom(this.scratch.params.scratchProgDonnees.workspace)
    const progd = Blockly.Xml.domToText(lxml)
    const progA = Blockly.Xml.domToText(lxmlTrav)
    // teste si le prof utilise get sans donner de valeurs de test
    if (progd.indexOf('Get_us') !== -1 || progA.indexOf('Get_us') !== -1) {
      if (this.stor.exoEncours.listValPos.length === 0) {
        const yy = j3pModale2({ titre: 'Erreur ', contenu: '', divparent: this.container })
        const stab1 = addDefaultTable(yy, 3, 1)
        j3pAddContent(stab1[0][0], 'Un de vos programmes contient le bloc "<b>Demander et attendre</b>",<br>')
        j3pAddContent(stab1[0][0], 'mais aucune valeur de test n\'est précisée.\n')
        j3pAddContent(stab1[1][0], 'Cliquez sur le bouton "+" en dessous de la phrase: \n')
        j3pAddContent(stab1[1][0], '<i>Réponses proposées par le testeur si présence du bloc "Demander et attendre"</i><br>')
        j3pAddContent(stab1[1][0], '( en bas à droite dans le troisième cadre ( couleur saumon ) qui \n contient le programme de départ. )')
        const elem = $('.croix')[0]
        j3pDetruit(elem)
        stab1[0][0].style.color = '#f00'
        j3pAjouteBouton(stab1[2][0], () => {
          j3pDetruit('modale2')
          j3pDetruit('j3pmasque2')
        }, { value: 'OK' })
        return
      }
    }
    this.stor.Programme[this.stor.exoEncoursi].ProgDeb = Blockly.Xml.domToText(lxml)
    this.stor.Programme[this.stor.exoEncoursi].ProgrammeAt = Blockly.Xml.domToText(lxmlTrav)
    this.stor.Programme[this.stor.exoEncoursi].enonce = this.stor.exoEncours.enonce
    this.stor.Programme[this.stor.exoEncoursi].listeBloc = j3pClone(this.stor.listeBloc)
    this.stor.Programme[this.stor.exoEncoursi].nom = this.stor.exoEncours.nom
    this.stor.Programme[this.stor.exoEncoursi].niv = this.stor.exoEncours.niv
    this.stor.Programme[this.stor.exoEncoursi].comptedire = this.stor.cocheDire.checked
    this.stor.Programme[this.stor.exoEncoursi].comptedirfin = this.stor.cocheDire2.checked
    this.stor.Programme[this.stor.exoEncoursi].comptepla = this.stor.cochePosition.checked
    this.stor.Programme[this.stor.exoEncoursi].Rendu1 = this.stor.cocheRendu1.checked
    this.stor.Programme[this.stor.exoEncoursi].Rendu2 = this.stor.cocheRendu2.checked
    this.stor.Programme[this.stor.exoEncoursi].ProgrammeMod = this.stor.ProgrammeMod
    this.stor.Programme[this.stor.exoEncoursi].comptetrace = this.stor.cocheTra.checked
    this.stor.Programme[this.stor.exoEncoursi].comptevar = (!this.stor.cocheVar.checked) ? [] : this.stor.inputVar.value.split(' ')
    this.stor.Programme[this.stor.exoEncoursi].comptevar = this.stor.Programme[this.stor.exoEncoursi].comptevar.filter(value => value !== '')
    this.stor.Programme[this.stor.exoEncoursi].comptebloc = (!this.stor.cochenbBlok.checked) ? 0 : Number(this.stor.inputnbloc.value)
    this.stor.Programme[this.stor.exoEncoursi].imageFond = j3pClone(this.stor.exoEncours.imageFond)
    this.stor.Programme[this.stor.exoEncoursi].lutinScratch = this.stor.exoEncours.lutinScratch
    this.stor.Programme[this.stor.exoEncoursi].listValPos = j3pClone(this.stor.exoEncours.listValPos)
    this.stor.Programme[this.stor.exoEncoursi].coxLut = this.stor.exoEncours.coxLut
    this.stor.Programme[this.stor.exoEncoursi].coyLut = this.stor.exoEncours.coyLut
    this.stor.Programme[this.stor.exoEncoursi].corCaseCoul = this.stor.exoEncours.corCaseCoul
    this.stor.Programme[this.stor.exoEncoursi].corCaseCoulToutes = this.stor.exoEncours.corCaseCoulToutes
    this.menuExo()
  }
}

export default EditorProgrammeScratchMake
