import $ from 'jquery'

import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pAjouteCaseCoche, j3pClone, j3pDetruit, j3pEmpty } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { InitSlider } from 'src/legacy/outils/slider/slider'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import EditorProgrammeBase from 'src/legacy/outils/paramEditors/EditorProgrammeBase'

// les fonctions communes avec scratchmake/editorProgramme.js sont exportées
class EditorProgrammeMtg extends EditorProgrammeBase {
  // pas besoin de définir de constructor, on a rien à ajouter à celui de la classe de base

  affichelisteBlocs () {
    j3pEmpty(this.scratch.lesdivs.divBlocPersoListe)
    this.stor.tabBoutSup = []
    this.stor.tabBoutMod = []
    const tab = addDefaultTable(this.scratch.lesdivs.divBlocPersoListe, this.scratch.params.lesBlocPerso.length + 2, 5)
    for (let i = 0; i < this.scratch.params.lesBlocPerso.length; i++) {
      tab[i + 1][0].style.border = '1px solid black'
      tab[i + 1][2].style.border = '1px solid black'
      tab[i + 1][4].style.border = '1px solid black'
      tab[i + 1][0].style.padding = '0px 5px 5px 5px'
      tab[i + 1][2].style.padding = '5px 5px 5px 5px'
      tab[i + 1][4].style.padding = '5px 5px 5px 5px'
      j3pAddContent(tab[i + 1][1], '&nbsp;')
      j3pAddContent(tab[i + 1][3], '&nbsp;')
      j3pAffiche(tab[i + 1][0], null, this.scratch.params.lesBlocPerso[i] + '&nbsp;')
      this.stor.tabBoutMod[i] = j3pAjouteBouton(tab[i + 1][2], this.modifDef.bind(this, i), { value: 'Modifier' })
      this.stor.tabBoutSup[i] = j3pAjouteBouton(tab[i + 1][4], this.supDef.bind(this, i), { value: 'Supprimer' })
    }
    j3pAddContent(tab[tab.length - 2][0], '&nbsp;')
    j3pAjouteBouton(tab[tab.length - 1][0], this.ajouteDef.bind(this), { value: '+' })
  }

  affichelistedep (bool) {
    j3pEmpty(this.stor.lesdivs.figdep)
    this.aret = undefined
    this.oldNom = undefined
    this.aretDep = undefined
    this.scratch.params.lettres = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    this.scratch.params.lettres = this.scratch.params.lettres.filter(el => { return this.scratch.params.zoneReserveLet.value.indexOf(el) === -1 })
    this.scratch.params.foraz = true
    j3pEmpty(this.divListRac)
    for (let i = 0; i < this.scratch.params.lettres.length; i++) {
      const sp = j3pAddElt(this.divListRac, 'span')
      const sp2 = j3pAddElt(this.divListRac, 'span')
      const hh = (i < 9) ? '.....' : '...'
      j3pAddContent(sp, '#' + (i + 1) + '#')
      j3pAddContent(sp2, hh + this.scratch.params.lettres[i] + '<br>')
      sp.addEventListener('click', this.faisSel)
    }
    this.stor.listeZonePris = []
    this.stor.listeZonePrisSeg = []
    // this.scratch doit être un scratchMathgraph, sinon ça va planter
    this.scratch.params.squizzEssai2 = true
    this.scratch.traceSortieBase()

    j3pEmpty(this.stor.lesdivs.modifDep)
    this.stor.lesdivs.modifDep.style.padding = '5px'
    this.stor.lesdivs.modifDep.style.width = '300px'
    this.stor.lesdivs.modifDep.style.verticalAlign = 'top'
    this.stor.tabBoutDep = []
    this.stor.cursoor = []
    this.stor.caseCoche = []
    this.stor.caseCoche2 = []
    const tab = addDefaultTable(this.stor.lesdivs.modifDep, this.scratch.params.MtgProg.figdep.length + 2, 7)
    this.letab = tab
    this.stor.boutAjDep = j3pAjouteBouton(tab[this.scratch.params.MtgProg.figdep.length + 1][0], this.ajouteDep.bind(this), { value: '+' })
    tab[this.scratch.params.MtgProg.figdep.length + 1][0].style.paddingTop = '20px'
    const titre = j3pAddElt(tab[0][0], 'span')
    j3pAddContent(titre, '<b><u>Eléments</u></b>')
    tab[0][0].style.padding = '0px 5px 10px 5px'
    this.stor.acache = [tab[0][3], tab[0][4]]
    let foblanc = false
    for (let i = 0; i < this.scratch.params.MtgProg.figdep.length; i++) {
      tab[i + 1][0].style.borderLeft = '1px solid black'
      tab[i + 1][0].style.borderTop = '1px solid black'
      tab[i + 1][0].style.borderBottom = '1px solid black'
      tab[i + 1][1].style.borderTop = '1px solid black'
      tab[i + 1][1].style.borderBottom = '1px solid black'
      tab[i + 1][2].style.borderTop = '1px solid black'
      tab[i + 1][2].style.borderBottom = '1px solid black'
      tab[i + 1][1].style.padding = '2px 5px 2px 5px'
      tab[i + 1][2].style.padding = '2px 5px 2px 5px'
      tab[i + 1][3].style.borderTop = '1px solid black'
      tab[i + 1][3].style.borderBottom = '1px solid black'
      tab[i + 1][3].style.padding = '2px 5px 2px 5px'
      tab[i + 1][4].style.borderTop = '1px solid black'
      tab[i + 1][4].style.borderBottom = '1px solid black'
      tab[i + 1][4].style.padding = '2px 5px 2px 5px'
      tab[i + 1][0].style.background = '#fff'
      tab[i + 1][1].style.background = '#fff'
      tab[i + 1][2].style.background = '#fff'
      tab[i + 1][3].style.background = '#fff'
      tab[i + 1][4].style.background = '#fff'
      tab[i + 1][5].style.background = '#fff'
      tab[i + 1][5].style.borderTop = '1px solid black'
      tab[i + 1][5].style.borderBottom = '1px solid black'
      tab[i + 1][5].style.borderRight = '1px solid black'
      tab[i + 1][5].style.padding = '2px 5px 2px 5px'
      const boutonSup = j3pAddElt(tab[i + 1][1], 'button')
      boutonSup.addEventListener('click', () => this.supprimeDep(i))
      j3pAddContent(boutonSup, '&nbsp;Suppr&nbsp;')
      if (i !== this.scratch.params.MtgProg.figdep.length - 1) {
        const tabContrainte = this.retrouveListeDep([i])
        tab[i + 1][0].addEventListener('mouseover', this.makeFuncAllume('#fd6969', tabContrainte, i))
        tab[i + 1][0].addEventListener('mouseout', this.makeFuncEteint(tabContrainte))
        boutonSup.addEventListener('mouseover', this.makeFuncAllume('#fd6969', tabContrainte, -1))
        boutonSup.addEventListener('mouseout', this.makeFuncEteint(tabContrainte))
        if (tabContrainte.indexOf(i + 1) === -1) j3pAjouteBouton(tab[i + 1][4], this.makeDown(i), { value: '↓' })
      } else {
        boutonSup.addEventListener('mouseover', this.makeFuncAllume('#fd6969', [i], -1))
        boutonSup.addEventListener('mouseout', this.makeFuncEteint([i]))
      }
      if (i !== 0) {
        const tabContrainte = this.retrouveContrainte([i])
        tab[i + 1][0].addEventListener('mouseover', this.makeFuncAllume('#b4f876', tabContrainte, i))
        tab[i + 1][0].addEventListener('mouseout', this.makeFuncEteint(tabContrainte, i))
        if (tabContrainte.indexOf(i - 1) === -1) j3pAjouteBouton(tab[i + 1][5], this.makeUp(i), { value: '↑' })
      }
      let bufaa
      const ffob = this.scratch.scratchMtgReplaceLettres(this.scratch.params.MtgProg.figdep[i])
      if (ffob.type === 'arc') {
        bufaa = 'arc ' + ffob.ext1 + ffob.ext2
      } else {
        bufaa = (ffob.type.indexOf('point') !== -1) ? 'Point ' + this.scratch.params.MtgProg.figdep[i].noms[0] : this.scratch.params.MtgProg.figdep[i].noms[0]
      }
      const buftrad = (this.scratch.params.MtgProg.figdep[i].noms[0].indexOf('#') !== -1) ? '&nbsp;&nbsp;~' + ffob.noms[0] : ''
      j3pAddContent(tab[i + 1][0], '&nbsp;' + bufaa + buftrad + '&nbsp;')
      this.stor.tabBoutDep[i] = tab[i + 1][1]
      if (ffob.type.indexOf('zone') === -1) {
        // j3pAjouteBouton(tab[i + 1][3], () => this.cacheDep(i), { value: (this.scratch.params.MtgProg.figdep[i].debCache) ? 'Montre' : 'Cache' })
        const boutonCache = j3pAddElt(tab[i + 1][3], 'button')
        boutonCache.addEventListener('click', () => this.cacheDep(i))
        j3pAddContent(boutonCache, '&nbsp;' + ((this.scratch.params.MtgProg.figdep[i].debCache) ? 'Montre' : 'Cache') + '&nbsp;')
      }
      if (this.scratch.params.MtgProg.figdep[i].type === 'zoneSeg') {
        const boutonMod = j3pAddElt(tab[i + 1][2], 'button')
        boutonMod.addEventListener('click', () => this.modiiefDepS(i))
        j3pAddContent(boutonMod, '&nbsp;Modif&nbsp;')
      }
      if (this.scratch.params.MtgProg.figdep[i].type === 'zone') {
        const boutonMod = j3pAddElt(tab[i + 1][2], 'button')
        boutonMod.addEventListener('click', () => this.modiiefDep(i))
        j3pAddContent(boutonMod, '&nbsp;Modif&nbsp;')
      }
      if (this.scratch.params.MtgProg.figdep[i].type === 'segment' || this.scratch.params.MtgProg.figdep[i].type === 'segmentlongA') {
        const boutonMod = j3pAddElt(tab[i + 1][2], 'button')
        boutonMod.addEventListener('click', () => this.modifSeg2(i))
        j3pAddContent(boutonMod, '&nbsp;Modif&nbsp;')
      }
      if (this.scratch.params.MtgProg.figdep[i].type === 'droitept' || this.scratch.params.MtgProg.figdep[i].type === 'droitepara' || this.scratch.params.MtgProg.figdep[i].type === 'droiteperp') {
        const boutonMod = j3pAddElt(tab[i + 1][2], 'button')
        boutonMod.addEventListener('click', () => this.modifDroite2(i))
        j3pAddContent(boutonMod, '&nbsp;Modif&nbsp;')
      }
      if (this.scratch.params.MtgProg.figdep[i].type === 'demidroitept') {
        const boutonMod = j3pAddElt(tab[i + 1][2], 'button')
        boutonMod.addEventListener('click', () => this.modifDemiDroite(i))
        j3pAddContent(boutonMod, '&nbsp;Modif&nbsp;')
      }
      if (this.scratch.params.MtgProg.figdep[i].type === 'cercleCentrePoint' || this.scratch.params.MtgProg.figdep[i].type === 'cercleCentreRayon') {
        const boutonMod = j3pAddElt(tab[i + 1][2], 'button')
        boutonMod.addEventListener('click', () => this.modifCercleles2(i))
        j3pAddContent(boutonMod, '&nbsp;Modif&nbsp;')
      }
      const tabPt = ['point', 'pointLibreAvecNom', 'pointSur', 'pointIntersection', 'pointMilieu', 'pointSym', 'pointRot']
      if (tabPt.indexOf(this.scratch.params.MtgProg.figdep[i].type) !== -1) {
        const boutonMod = j3pAddElt(tab[i + 1][2], 'button')
        boutonMod.addEventListener('click', () => this.modiPointDep(i))
        j3pAddContent(boutonMod, '&nbsp;Modif&nbsp;')
      }
      if (this.scratch.params.MtgProg.figdep[i].type === 'point' && this.scratch.params.MtgProg.figdep[i].sur) {
        this.stor.acache.push(tab[i + 1][6])
        tab[i + 1][6].style.border = '1px solid black'
        tab[i + 1][6].style.background = '#fff'
        foblanc = true
        this.stor.cursoor.push(InitSlider(tab[i + 1][6], this.scratch.params.MtgProg.figdep[i].coef, 100, this.getSliderFunction(i), this.mtgAppLecteur2))
      }
    }
    if (foblanc) {
      j3pAddContent(tab[1][6], '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')
    }
    if (!bool) this.stor.bouGenere.disabled = false
  }

  faisSel () {
    const range = new Range()
    range.setStart(this, 0)
    range.setEnd(this, 1)
    document.getSelection().addRange(range)
  }

  makeFuncAllume (couleur, tab, num) {
    return () => {
      for (let i = 0; i < tab.length; i++) {
        if (tab[i] === num) continue
        this.letab[tab[i] + 1][0].style.background = couleur
      }
    }
  }

  makeFuncEteint (tab) {
    return () => {
      for (let i = 0; i < tab.length; i++) {
        this.letab[tab[i] + 1][0].style.background = '#fff'
      }
    }
  }

  modifDroite2 (num) {
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’une droite</u></b><br><br>')
    if (this.nbPoints(num) > 1) {
      j3pAjouteBouton(this.stor.lesdivs.modifDep, () => this.modifDroite(num), { value: 'Droite 2 points' })
      j3pAddContent(this.stor.lesdivs.modifDep, '<br><br>')
    }
    if (this.nbLignes(num) > 0) {
      j3pAjouteBouton(this.stor.lesdivs.modifDep, () => this.modifDroitePara(num), { value: 'Droite parallèle' })
      j3pAddContent(this.stor.lesdivs.modifDep, '<br><br>')
      j3pAjouteBouton(this.stor.lesdivs.modifDep, () => this.modifDroitePerp(num), { value: 'Droite perpendiculaire' })
    }
  }

  modifSeg2 (num) {
    const nbPoints = this.nbPoints(num)
    if (nbPoints < 2) {
      this.modifSegLong(num)
      return
    }
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’un segment</u></b><br><br>')
    j3pAjouteBouton(this.stor.lesdivs.modifDep, () => this.modifSeg(num), { value: 'Segment 2 points' })
    j3pAddContent(this.stor.lesdivs.modifDep, '<br><br>')
    j3pAjouteBouton(this.stor.lesdivs.modifDep, () => this.modifSegLong(num), { value: 'Segment 1 point 1 longeur' })
  }

  modifCercleles2 (num) {
    const nbPoints = this.nbPoints(num)
    if (nbPoints < 2) {
      this.modifCercleRay(num)
      return
    }
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’un cercle</u></b><br><br>')
    j3pAjouteBouton(this.stor.lesdivs.modifDep, () => this.modifCercleRay(num), { value: 'Cercle centre rayon' })
    j3pAddContent(this.stor.lesdivs.modifDep, '<br><br>')
    j3pAjouteBouton(this.stor.lesdivs.modifDep, () => this.modifCerclePt(num), { value: 'Cercle centre point' })
  }

  makeUp (i) {
    return () => {
      const acop = j3pClone(this.scratch.params.MtgProg.figdep[i])
      this.scratch.params.MtgProg.figdep.splice(i, 1)
      this.scratch.params.MtgProg.figdep.splice(i - 1, 0, acop)
      this.affichelistedep()
    }
  }

  makeDown (i) {
    return () => {
      const acop = j3pClone(this.scratch.params.MtgProg.figdep[i])
      this.scratch.params.MtgProg.figdep.splice(i, 1)
      this.scratch.params.MtgProg.figdep.splice(i + 1, 0, acop)
      this.affichelistedep()
    }
  }

  retrouveContrainte (tab) {
    const aret = j3pClone(tab)
    for (let i = tab[0] - 1; i > -1; i--) {
      for (const a of aret) {
        for (let j = 0; j < this.scratch.params.MtgProg.figdep[a].dep.length; j++) {
          if (this.scratch.params.MtgProg.figdep[i].noms.includes(this.scratch.params.MtgProg.figdep[a].dep[j])) {
            aret.push(i)
            break
          }
        }
      }
    }
    return aret
  }

  ajouteDef () {
    j3pEmpty(this.scratch.lesdivs.divBlocPersoListe)
    j3pEmpty(this.scratch.params.ModaleCreaisEditing)
    this.scratch.Blockly.Procedures.externalProcedureDefCallback()
    this.scratch.params.ModaleCreaisEditing.style.border = '1px solid black'
    this.scratch.params.ModaleCreaisEditing.style.background = '#aaaaaa'
    window.scroll({
      top: 2000,
      left: 0,
      behavior: 'smooth'
    })
  }

  ajouteDep () {
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Nouvel Element</u></b>')
    this.stor.tabDepAct = addDefaultTable(this.stor.lesdivs.actDep, 8, 1)
    this.stor.tabDepAct0 = addDefaultTable(this.stor.tabDepAct[0][0], 1, 5)
    j3pAddContent(this.stor.tabDepAct0[0][0], 'Type:')
    j3pAddContent(this.stor.tabDepAct0[0][1], '&nbsp;')
    j3pAddContent(this.stor.tabDepAct0[0][3], '&nbsp;')
    const ll = ['Choisir', 'Zone', 'ZoneSeg', 'Point']
    if (this.nbPoints() > 1) ll.push('Segment')
    if (this.nbPoints() > 0) ll.push('SegmentLong')
    if (this.nbPoints() > 1) ll.push('Droite')
    if (this.nbPoints() > 0 && this.nbLignes() > 0) ll.push('DroitePara', 'DroitePerp')
    if (this.nbPoints() > 1) ll.push('Demi-droite')
    if (this.nbPoints() > 1) ll.push('Cercle point')
    if (this.nbPoints() > 0) ll.push('Cercle rayon')
    if (this.nbPoints() > 2) ll.push('Arc de cercle')
    if (this.nbSegments() > 0) ll.push('Marque long')
    this.stor.listeDepType = ListeDeroulante.create(this.stor.tabDepAct0[0][2], ll, this.ldOptions)
    this.stor.boutOKDep = j3pAjouteBouton(this.stor.tabDepAct0[0][4], this.okDep.bind(this), { value: 'OK' })
    this.genereOK(false, true)
    j3pAddContent(this.stor.tabDepAct[6][0], '&nbsp;')
    this.stor.listAnulle = []
    this.stor.annuleDep = j3pAjouteBouton(this.stor.tabDepAct[7][0], this.annuleDep.bind(this), { value: 'Annuler' })
  }

  okDep () {
    if (!this.stor.listeDepType.changed) return
    this.stor.listeDepType.disable()
    j3pEmpty(this.stor.tabDepAct[0][0])
    const tatab = addDefaultTable(this.stor.tabDepAct[0][0], 1, 5)
    j3pAffiche(tatab[0][0], null, '&nbsp;' + this.stor.listeDepType.reponse + '&nbsp;')
    if (this.stor.listeDepType.reponse === 'Zone') {
      const nn = this.ajZoneDep()
      j3pAffiche(tatab[0][1], null, nn)
      j3pAffiche(this.stor.tabDepAct[1][0], null, '<i>Clic <b>OK</b> quand la zone est bien placée</i>')
      this.stor.tabDepAct[1][0].style.color = '#2573b4'
      this.stor.boutOKDep = j3pAjouteBouton(this.stor.tabDepAct[2][0], this.okPlaceC.bind(this, nn), { value: 'OK' })
    }
    if (this.stor.listeDepType.reponse === 'ZoneSeg') {
      const nn = this.ajZoneDepSeg()
      j3pAffiche(tatab[0][1], null, nn)
      j3pAffiche(this.stor.tabDepAct[1][0], null, '<i>Clic <b>OK</b> quand la zone segment est bien placée</i>')
      this.stor.tabDepAct[1][0].style.color = '#2573b4'
      this.stor.boutOKDep = j3pAjouteBouton(this.stor.tabDepAct[2][0], this.okPlaceCSeg.bind(this, nn), { value: 'OK' })
    }
    if (this.stor.listeDepType.reponse === 'Point') {
      this.stor.zoneNom = new ZoneStyleMathquill1(tatab[0][1], {
        restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789\'',
        lim: 4,
        limite: 4,
        enter: this.okNomDeb.bind(this),
        j3pCont: this.container
      })
      j3pAjouteBouton(tatab[0][3], this.okNomDeb.bind(this), { value: 'OK' })
      j3pAjouteBouton(this.stor.tabDepAct[0][0], this.okNomAlea.bind(this), { value: 'Nom aléatoire' })
      this.stor.tabDepAct[1][0].style.color = '#2573b4'
      setTimeout(() => { this.stor.zoneNom.focus() }, 2)
      this.stor.aaasssuuup = tatab[0][3]
      this.stor.listAnulle = [this.stor.zoneNom]
    }
    if (this.stor.listeDepType.reponse === 'Segment') {
      j3pEmpty(tatab[0][0])
      const ll = this.fileMoiLesPoints()
      const tyu = addDefaultTable(tatab[0][0], 1, 5)
      this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
      this.stor.listeP2 = ListeDeroulante.create(tyu[0][3], ll, {})
      j3pAddContent(tyu[0][0], '[&nbsp;')
      j3pAddContent(tyu[0][2], '&nbsp;')
      j3pAddContent(tyu[0][4], '&nbsp;]')
      const tyu2 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
      this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
      j3pAjouteBouton(tatab[0][1], this.okSegment.bind(this), { value: 'OK' })
      this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3]
    }
    if (this.stor.listeDepType.reponse === 'SegmentLong') {
      j3pEmpty(tatab[0][0])
      const ll = this.fileMoiLesPoints()
      const tyu = addDefaultTable(tatab[0][0], 1, 2)
      const tyu3 = addDefaultTable(tatab[0][0], 1, 2)
      this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
      j3pAddContent(tyu[0][0], 'extrémité:&nbsp;')
      j3pAddContent(tyu3[0][0], 'rayon:&nbsp;')
      this.stor.listeP2 = j3pAddElt(tyu3[0][1], 'input', '', {
        size: 9
      })
      const tyu2 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
      this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
      j3pAjouteBouton(tatab[0][0], this.okSegmentLong.bind(this), { value: 'OK' })
      this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP3]
    }
    if (this.stor.listeDepType.reponse === 'Droite') {
      j3pEmpty(tatab[0][0])
      const ll = this.fileMoiLesPoints()
      const tyu = addDefaultTable(tatab[0][0], 1, 5)
      this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
      this.stor.listeP2 = ListeDeroulante.create(tyu[0][3], ll, {})
      j3pAddContent(tyu[0][0], '(&nbsp;')
      j3pAddContent(tyu[0][2], '&nbsp;')
      j3pAddContent(tyu[0][4], '&nbsp;)')
      const tyu2 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
      this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
      j3pAjouteBouton(tatab[0][1], this.okDroite.bind(this), { value: 'OK' })
      this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3]
    }
    if (this.stor.listeDepType.reponse === 'DroitePara') {
      j3pEmpty(tatab[0][0])
      const ll = this.fileMoiLesPoints()
      const ll2 = this.fileMoiLesLignes()
      const tyu = addDefaultTable(tatab[0][0], 1, 2)
      const tyu3 = addDefaultTable(tatab[0][0], 1, 2)
      this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
      j3pAddContent(tyu[0][0], 'passant par:&nbsp;')
      j3pAddContent(tyu3[0][0], 'parallèle à:&nbsp;')
      this.stor.listeP2 = ListeDeroulante.create(tyu3[0][1], ll2, {})
      const tyu2 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
      this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
      j3pAjouteBouton(tatab[0][1], this.okPara.bind(this), { value: 'OK' })
      this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3]
    }
    if (this.stor.listeDepType.reponse === 'DroitePerp') {
      j3pEmpty(tatab[0][0])
      const ll = this.fileMoiLesPoints()
      const ll2 = this.fileMoiLesLignes()
      const tyu = addDefaultTable(tatab[0][0], 1, 2)
      const tyu3 = addDefaultTable(tatab[0][0], 1, 2)
      this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
      j3pAddContent(tyu[0][0], 'passant par:&nbsp;')
      j3pAddContent(tyu3[0][0], 'perpendiculaire à:&nbsp;')
      this.stor.listeP2 = ListeDeroulante.create(tyu3[0][1], ll2, {})
      const tyu2 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
      this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
      j3pAjouteBouton(tatab[0][1], this.okPerp.bind(this), { value: 'OK' })
      this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3]
    }
    if (this.stor.listeDepType.reponse === 'Demi-droite') {
      j3pEmpty(tatab[0][0])
      const ll = this.fileMoiLesPoints()
      const tyu = addDefaultTable(tatab[0][0], 1, 5)
      this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
      this.stor.listeP2 = ListeDeroulante.create(tyu[0][3], ll, {})
      j3pAddContent(tyu[0][0], '[&nbsp;')
      j3pAddContent(tyu[0][2], '&nbsp;')
      j3pAddContent(tyu[0][4], '&nbsp;)')
      const tyu2 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
      this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
      j3pAjouteBouton(tatab[0][1], this.okDemiDroite.bind(this), { value: 'OK' })
      this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3]
    }
    if (this.stor.listeDepType.reponse === 'Cercle point') {
      j3pEmpty(tatab[0][0])
      const ll = this.fileMoiLesPoints()
      const tyu = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu[0][0], 'centre:&nbsp;')
      this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
      const tyu3 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu3[0][0], 'point:&nbsp;')
      this.stor.listeP2 = ListeDeroulante.create(tyu3[0][1], ll, {})
      const tyu2 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
      this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
      const tyu5 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu5[0][0], 'Nom:&nbsp;')
      this.stor.zoneNom = new ZoneStyleMathquill1(tyu5[0][1], {
        restric: '()\'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMONPQRSTUVWXYZ',
        lim: 4,
        limite: 4,
        enter: this.okNomDebPara.bind(this),
        j3pCont: this.container
      })
      const tyu4 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu4[0][0], 'Nom caché:&nbsp;')
      this.stor.listeP4 = ListeDeroulante.create(tyu4[0][0], ['Non', 'Oui'], { choix0: true })
      j3pAddContent(tatab[0][0], '<br>')
      j3pAjouteBouton(tatab[0][0], this.okCercle.bind(this), { value: 'OK' })
      this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3, this.stor.listeP4, this.stor.zoneNom]
    }
    if (this.stor.listeDepType.reponse === 'Cercle rayon') {
      j3pEmpty(tatab[0][0])
      const ll = this.fileMoiLesPoints()
      const tyu = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu[0][0], 'centre:&nbsp;')
      this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
      const tyu3 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu3[0][0], 'rayon:&nbsp;')
      this.stor.listeP2 = j3pAddElt(tyu3[0][1], 'input', '', {
        size: 10
      })
      this.stor.listeP2.addEventListener('change', () => {
        let niveau = Number(this.stor.listeP2.value)
        if (isNaN(this.stor.listeP2.value) || this.stor.listeP2.value === '') niveau = 3
        if (niveau < 1) {
          niveau = 1
        } else if (niveau > 10) {
          niveau = 10
        }
        this.stor.listeP2.value = String(niveau)
      })
      const tyu2 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
      this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
      const tyu5 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu5[0][0], 'Nom:&nbsp;')
      this.stor.zoneNom = new ZoneStyleMathquill1(tyu5[0][1], {
        restric: '()\'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMONPQRSTUVWXYZ',
        lim: 4,
        limite: 4,
        enter: this.okNomDebPara.bind(this),
        j3pCont: this.container
      })
      const tyu4 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu4[0][0], 'Nom caché:&nbsp;')
      this.stor.listeP4 = ListeDeroulante.create(tyu4[0][0], ['Non', 'Oui'], { choix0: true })
      j3pAddContent(tatab[0][0], '<br>')
      j3pAjouteBouton(tatab[0][0], this.okCercleRayon.bind(this), { value: 'OK' })
      this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP3, this.stor.listeP4, this.stor.zoneNom]
    }
    if (this.stor.listeDepType.reponse === 'Arc de cercle') {
      j3pEmpty(tatab[0][0])
      const ll = this.fileMoiLesPoints()
      const tyu = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu[0][0], 'centre:&nbsp;')
      this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
      const tyu3 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu3[0][0], 'extrémité:&nbsp;')
      this.stor.listeP2 = ListeDeroulante.create(tyu3[0][1], ll, {})
      const tyu4 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu4[0][0], 'point d’arêt:&nbsp;')
      this.stor.listeP4 = ListeDeroulante.create(tyu4[0][1], ll, {})
      const tyu5 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu5[0][0], 'sens:&nbsp;')
      this.stor.listeP3 = ListeDeroulante.create(tyu5[0][0], ['direct', 'indirect'], { choix0: true })
      const tyu2 = addDefaultTable(tatab[0][0], 1, 2)
      j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
      this.stor.listeP5 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
      j3pAddContent(tatab[0][0], '<br>')
      j3pAjouteBouton(tatab[0][0], this.okArc.bind(this), { value: 'OK' })
      this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3, this.stor.listeP4, this.stor.listeP5]
    }
    if (this.stor.listeDepType.reponse === 'Marque long') {
      this.vireBouts()
      const ll = ['Choisir']
      for (let i = 0; i < this.scratch.params.MtgProg.figdep.length; i++) {
        const ffob = this.scratch.scratchMtgReplaceLettres(this.scratch.params.MtgProg.figdep[i])
        if (ffob.type === 'segment' || ffob.type === 'segmentlongA') ll.push(ffob.noms[0])
      }
      const ttab = addDefaultTable(this.stor.tabDepAct[1][0], 1, 4)
      this.stor.tabDepAct[1][0].style.color = ''
      j3pAddTxt(ttab[0][0], 'sur le segment&nbsp;')
      this.stor.listeSur = ListeDeroulante.create(ttab[0][1], ll, this.ldOptions)
      j3pAddTxt(ttab[0][2], '&nbsp;')
      j3pAjouteBouton(ttab[0][3], this.choixMarque.bind(this), { value: 'OK' })
      this.stor.listAnulle = [this.stor.listeSur]
    }
  }

  okArc () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (!this.stor.listeP2.changed) {
      this.stor.listeP2.focus()
      return
    }
    if (!this.stor.listeP4.changed) {
      this.stor.listeP2.focus()
      return
    }
    if (this.stor.listeP1.reponse === this.stor.listeP2.reponse) {
      this.stor.listeP2.focus()
      return
    }
    if (this.stor.listeP1.reponse === this.stor.listeP4.reponse) {
      this.stor.listeP4.focus()
      return
    }
    if (this.stor.listeP2.reponse === this.stor.listeP4.reponse) {
      this.stor.listeP4.focus()
      return
    }
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    const p2 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    const p3 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP4.reponse)
    this.scratch.params.MtgProg.figdep.push(
      { type: 'arc', centre: p1, ext1: p2, ext2: p3, noms: [p2 + p3], sens: this.stor.listeP3.reponse !== 'direct', pointille: this.stor.listeP5.reponse === 'Oui', dep: [p1, p2, p3], contient: [p2, p3] }
    )
    this.stor.listeP2.disable()
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.stor.listeP4.disable()
    this.stor.listeP5.disable()
    this.affichelistedep()
  }

  modifDroitePara (num) {
    this.aret = num
    this.aretDep = this.retrouveListeDep([num])
    this.oldNom = j3pClone(this.scratch.params.MtgProg.figdep[num].noms)
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’une droite</u></b>')
    const ll = this.fileMoiLesPoints(num)
    const ll2 = this.fileMoiLesLignes(num)
    const tyu = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    const tyu3 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
    j3pAddContent(tyu[0][0], 'passant par:&nbsp;')
    j3pAddContent(tyu3[0][0], 'parallèle à:&nbsp;')
    this.stor.listeP2 = ListeDeroulante.create(tyu3[0][1], ll2, {})
    const tyu2 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
    this.stor.listeP3 = ListeDeroulante.create(tyu2[0][1], ['Non', 'Oui'], { choix0: true })
    const tyu9 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu9[0][0], 'Nom:&nbsp;')
    this.stor.zoneNom = new ZoneStyleMathquill1(tyu9[0][1], {
      restric: '()\'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMONPQRSTUVWXYZ',
      lim: 4,
      limite: 4,
      enter: this.okNomDebPara.bind(this),
      j3pCont: this.container
    })
    const tyu4 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu4[0][0], 'Nom Caché:&nbsp;')
    this.stor.listeP4 = ListeDeroulante.create(tyu4[0][1], ['Non', 'Oui'], { choix0: true })
    j3pAjouteBouton(this.stor.lesdivs.modifDep, this.okParaMod.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3, this.stor.listeP4, this.stor.zoneNom]
  }

  modifDroitePerp (num) {
    this.aret = num
    this.aretDep = this.retrouveListeDep([num])
    this.oldNom = j3pClone(this.scratch.params.MtgProg.figdep[num].noms)
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’une droite</u></b>')
    const ll = this.fileMoiLesPoints(num)
    const ll2 = this.fileMoiLesLignes(num)
    const tyu = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    const tyu3 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
    j3pAddContent(tyu[0][0], 'passant par:&nbsp;')
    j3pAddContent(tyu3[0][0], 'perpendiculaire à:&nbsp;')
    this.stor.listeP2 = ListeDeroulante.create(tyu3[0][1], ll2, {})
    const tyu2 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
    this.stor.listeP3 = ListeDeroulante.create(tyu2[0][1], ['Non', 'Oui'], { choix0: true })
    const tyu9 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu9[0][0], 'Nom:&nbsp;')
    this.stor.zoneNom = new ZoneStyleMathquill1(tyu9[0][1], {
      restric: '()\'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMONPQRSTUVWXYZ',
      lim: 4,
      limite: 4,
      enter: this.okNomDebPara.bind(this),
      j3pCont: this.container
    })
    const tyu4 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu4[0][0], 'Nom Caché:&nbsp;')
    this.stor.listeP4 = ListeDeroulante.create(tyu4[0][1], ['Non', 'Oui'], { choix0: true })
    j3pAjouteBouton(this.stor.lesdivs.modifDep, this.okPerpMod.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3, this.stor.listeP4, this.stor.zoneNom]
  }

  modifSeg (num) {
    this.aretDep = this.retrouveListeDep([num])
    this.oldNom = j3pClone(this.scratch.params.MtgProg.figdep[num].noms)
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’un segment</u></b>')
    j3pEmpty(this.stor.lesdivs.modifDep)
    const ll = this.fileMoiLesPoints(num)
    const tyu = addDefaultTable(this.stor.lesdivs.modifDep, 1, 5)
    this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
    this.stor.listeP2 = ListeDeroulante.create(tyu[0][3], ll, {})
    j3pAddContent(tyu[0][0], '[&nbsp;')
    j3pAddContent(tyu[0][2], '&nbsp;')
    j3pAddContent(tyu[0][4], '&nbsp;]')
    this.aret = num
    const tyu2 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
    this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
    j3pAjouteBouton(this.stor.lesdivs.modifDep, this.okSegment.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3]
  }

  modifSegLong (num) {
    this.aretDep = this.retrouveListeDep([num])
    this.oldNom = j3pClone(this.scratch.params.MtgProg.figdep[num].noms)
    this.aret = num
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’un segment</u></b><br><br>')
    const ll = this.fileMoiLesPoints(num)
    const tyu = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    const tyu3 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
    j3pAddContent(tyu[0][0], 'extrémité:&nbsp;')
    j3pAddContent(tyu3[0][0], 'rayon:&nbsp;')
    this.stor.listeP2 = j3pAddElt(this.stor.lesdivs.modifDep, 'input', '', {
      size: 9
    })
    const tyu2 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
    this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
    j3pAjouteBouton(this.stor.lesdivs.modifDep, this.okSegmentLongMod.bind(this), { value: 'OK' })
  }

  modifDemiDroite (num) {
    this.aretDep = this.retrouveListeDep([num])
    this.oldNom = j3pClone(this.scratch.params.MtgProg.figdep[num].noms)
    this.aret = num
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’une demi-droite</u></b><br><br>')
    const ll = this.fileMoiLesPoints(num)
    const tyu = addDefaultTable(this.stor.lesdivs.modifDep, 1, 5)
    this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
    this.stor.listeP2 = ListeDeroulante.create(tyu[0][3], ll, {})
    j3pAddContent(tyu[0][0], '[&nbsp;')
    j3pAddContent(tyu[0][2], '&nbsp;')
    j3pAddContent(tyu[0][4], '&nbsp;)')
    const tyu2 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
    this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
    j3pAjouteBouton(this.stor.lesdivs.modifDep, this.okDemiDroite.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3]
  }

  annuleDep () {
    for (let i = 0; i < this.stor.listAnulle; i++) {
      this.stor.listAnulle[i].disable()
    }
    j3pEmpty(this.stor.lesdivs.actDepTitre)
    j3pEmpty(this.stor.lesdivs.actDep)
    this.affichelistedep()
  }

  ajZoneDep () {
    this.scratch.params.mtgApplecteur.setApiDoc(this.scratch.params.svgId2)
    const tatag = this.scratch.ajZone({ noms: [] })
    const c = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'z' + tatag + 'c' })
    const p = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'z' + tatag + 'p' })
    this.scratch.params.mtgApplecteur.setVisible({ elt: 'z' + tatag + 'c' })
    this.scratch.params.mtgApplecteur.setVisible({ elt: 'z' + tatag + 'p' })
    this.scratch.params.MtgProg.figdep.push({ type: 'zone', noms: ['zone' + tatag], c, p, dep: [] })
    j3pEmpty(this.stor.tabDepAct[6][0])
    return tatag
  }

  ajZoneDepSeg () {
    this.scratch.params.mtgApplecteur.setApiDoc(this.scratch.params.svgId2)
    const tatag = this.scratch.ajZoneSeg({ noms: [] })
    const c = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'zs' + tatag + 'c' })
    const p = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'zs' + tatag + 'p' })
    this.scratch.params.mtgApplecteur.setVisible({ elt: 'zs' + tatag + 'c' })
    this.scratch.params.mtgApplecteur.setVisible({ elt: 'zs' + tatag + 'p' })
    this.scratch.params.MtgProg.figdep.push({ type: 'zoneSeg', noms: ['zoneSeg' + tatag], c, p, dep: [] })
    j3pEmpty(this.stor.tabDepAct[6][0])
    return tatag
  }

  newTagSeg () {
    let i = -1
    do {
      i++
      if (!this.stor.listeZonePrisSeg.includes(String(i))) {
        return String(i)
      }
    } while (i < 10000)
  }

  newTagZone () {
    let i = -1
    do {
      i++
      if (!this.stor.listeZonePris.includes(String(i))) {
        return String(i)
      }
    } while (i < 10000)
  }

  MakeZoneSeg (tatag) {
    this.scratch.params.mtgApplecteur.setApiDoc(this.scratch.params.svgId2)
    this.scratch.params.mtgApplecteur.addFreePoint({
      absCoord: true,
      name: 'zs' + tatag + 'c',
      tag: 'zs' + tatag + 'c',
      x: 100,
      y: 100,
      pointStyle: 'biground',
      hiddenName: true,
      color: this.color[Number(tatag) % this.color.length],
      hidden: true
    })
    this.scratch.params.mtgApplecteur.addFreePoint({
      absCoord: true,
      name: 'zs' + tatag + 'p',
      tag: 'zs' + tatag + 'p',
      x: 200,
      y: 200,
      pointStyle: 'biground',
      hiddenName: true,
      color: this.color[Number(tatag) % this.color.length],
      hidden: true
    })
    this.scratch.params.mtgApplecteur.addSegment({
      tag: 'zs' + tatag,
      a: 'zs' + tatag + 'p',
      b: 'zs' + tatag + 'c',
      thickness: 4,
      color: this.color[Number(tatag) % this.color.length]
    })
    this.scratch.params.mtgApplecteur.addLengthMeasure({ a: 'zs' + tatag + 'p', b: 'zs' + tatag + 'c' })
    this.scratch.params.mtgApplecteur.addCalc({ nameCalc: 'zscal' + tatag, formula: 'zs' + tatag + 'pzs' + tatag + 'c/2' })
    this.scratch.params.mtgApplecteur.addCircleOr({
      o: 'zs' + tatag + 'p',
      r: 'zscal' + tatag,
      tag: 'zs' + tatag + 'm',
      hidden: true
    })
    this.scratch.params.mtgApplecteur.addIntLineCircle({
      d: 'zs' + tatag,
      c: 'zs' + tatag + 'm',
      tag: 'zsmil' + tatag,
      name: 'zsmil' + tatag,
      hidden: true
    })
    this.scratch.params.mtgApplecteur.addLinkedText({
      text: 'Zs' + tatag,
      a: 'zsmil' + tatag,
      color: this.color[Number(tatag) % this.color.length],
      hidden: false,
      tag: 'zs' + tatag + 'a',
      hAlign: 'center',
      vAlign: 'middle',
      transparent: true
    })
  }

  valModifExo () {
    // fais nom , import export
    this.stor.Programme[this.stor.exoEncoursi] = j3pClone(this.scratch.params.MtgProg)
    const ybloc = []
    for (const property in this.scratch.params.listeDef) {
      ybloc.push(this.scratch.params.listeDef[property])
    }
    this.stor.Programme[this.stor.exoEncoursi].blocPerso = ybloc
    this.stor.Programme[this.stor.exoEncoursi].nom = this.stor.exoEncours.nom
    this.stor.Programme[this.stor.exoEncoursi].niv = this.stor.exoEncours.niv
    this.stor.Programme[this.stor.exoEncoursi].FoEfCo = this.scratch.params.tabConstEff.reponse === 'Oui'
    this.stor.Programme[this.stor.exoEncoursi].reservLettres = this.scratch.params.zoneReserveLet.value
    this.menuExo()
  }

  MakeZone (tatag) {
    this.scratch.params.mtgApplecteur.setApiDoc(this.scratch.params.svgId2)
    this.scratch.params.mtgApplecteur.addFreePoint({
      absCoord: true,
      name: 'z' + tatag + 'c',
      tag: 'z' + tatag + 'c',
      x: 200,
      y: 200,
      pointStyle: 'biground',
      hiddenName: true,
      color: this.color[Number(tatag) % this.color.length],
      hidden: true
    })
    this.scratch.params.mtgApplecteur.addFreePoint({
      absCoord: true,
      name: 'z' + tatag + 'p',
      tag: 'z' + tatag + 'p',
      x: 300,
      y: 300,
      pointStyle: 'biground',
      hiddenName: true,
      color: this.color[Number(tatag) % this.color.length],
      hidden: true
    })
    this.scratch.params.mtgApplecteur.addCircleOA({
      tag: 'zP' + tatag,
      o: 'z' + tatag + 'c',
      a: 'z' + tatag + 'p',
      thickness: 4,
      hidden: true
    })
    this.scratch.params.mtgApplecteur.addSurfaceCircle({
      c: 'zP' + tatag,
      tag: 'z' + tatag,
      color: this.color[Number(tatag) % this.color.length],
      transparancy: 0.5
    })
    this.scratch.params.mtgApplecteur.addLinkedText({
      text: 'Z' + tatag,
      a: 'z' + tatag + 'c',
      color: this.color[Number(tatag) % this.color.length],
      hidden: false,
      tag: 'z' + tatag + 'a',
      hAlign: 'center',
      vAlign: 'middle',
      transparent: true
    })
  }

  cestParti () {
    this.color = ['black', 'blue', 'red', 'orange', 'purple', 'green']
    const Blockly = this.scratch.Blockly
    this.scratch.initParams(null, true, { edit: true })
    this.scratch.ajZone = (ki) => {
      const tatag = (ki.noms[0]) ? ki.noms[0].replace('zone', '') : this.newTagZone()
      this.stor.listeZonePris.push(tatag)
      this.MakeZone(tatag)
      this.scratch.params.mtgApplecteur.setVisible({ elt: 'z' + tatag })
      this.scratch.params.mtgApplecteur.setVisible({ elt: 'z' + tatag + 'a' })
      if (ki.c) {
        this.scratch.params.mtgApplecteur.setPointPosition(this.scratch.params.svgId2, 'z' + tatag + 'c', ki.c.x, ki.c.y, true)
      }
      if (ki.p) {
        this.scratch.params.mtgApplecteur.setPointPosition(this.scratch.params.svgId2, 'z' + tatag + 'p', ki.p.x, ki.p.y, true)
      }
      return tatag
    }
    this.scratch.ajZoneSeg = (ki) => {
      const tatag = (ki.noms[0]) ? ki.noms[0].replace('zoneSeg', '') : this.newTagSeg()
      this.stor.listeZonePrisSeg.push(tatag)
      this.MakeZoneSeg(tatag)
      this.scratch.params.mtgApplecteur.setVisible({ elt: 'zs' + tatag })
      this.scratch.params.mtgApplecteur.setVisible({ elt: 'zs' + tatag + 'a' })
      if (ki.c) {
        this.scratch.params.mtgApplecteur.setPointPosition(this.scratch.params.svgId2, 'zs' + tatag + 'c', ki.c.x, ki.c.y, true)
      }
      if (ki.p) {
        this.scratch.params.mtgApplecteur.setPointPosition(this.scratch.params.svgId2, 'zs' + tatag + 'p', ki.p.x, ki.p.y, true)
      }
      return tatag
    }
    this.scratch.testFlag = () => {}
    this.scratch.testFlagFin = () => {
      return { ok: true }
    }
    this.scratch.finishCreaPerso = () => {
      j3pEmpty(this.scratch.params.ModaleCreaisEditing)
      j3pEmpty(this.stor.lesdivs.DeDefPerso)
      this.affichelisteBlocs()
      this.scratch.params.ModaleCreaisEditing.style.border = '0px'
      this.scratch.params.ModaleCreaisEditing.style.background = ''
      this.refaisMenu()
    }
    this.scratch.scratchAnnuleCrea = () => {
      j3pEmpty(this.scratch.params.ModaleCrea)
      this.scratch.params.ModaleCrea.style.border = ''
      this.scratch.params.ModaleCrea.style.background = ''
      j3pDetruit('j3pmasque')
      this.affichelisteBlocs()
    }
    this.scratch.scratchEditorMetPasOk = () => {
      this.genereOK(false)
    }
    this.scratch.params.j3pCont = this.container
    this.scratch.params.MtgProg = this.stor.exoEncours
    this.scratch.params.lesBlocPerso = this.stor.exoEncours.blocPerso.map(el => el.nom)
    this.scratch.params.listeDef = {}
    if (this.scratch.params.lesBlocPerso.length > 0) {
      const x2 = this.scratch.scratchCreeMenu([], true)
      const xmlDoc2 = $.parseXML(x2)
      let xml2 = xmlDoc2.firstChild
      while (xml2.nodeType !== 1) {
        xml2 = xml2.nextSibling
      }
      const aVire = addDefaultTable(this.stor.lesdivs.figDep, 1, 1)[0][0]
      for (let i = 0; i < this.scratch.params.MtgProg.blocPerso.length; i++) {
        if (i === 0) {
          this.scratch.params.scratchProgDonnees.menu.push({
            type: 'catégorie',
            nom: 'MES BLOCS',
            coul1: '#D14C7D',
            coul2: '#B8436E',
            contenu: []
          })
        }

        function urlImg (img, onlyRel) {
          if (onlyRel) return '/outils/scratch/' + img
          return j3pBaseUrl + 'outils/scratch/' + img
        }

        const nwBlo = this.scratch.params.MtgProg.blocPerso[i]
        const obdep = {
          message0: nwBlo.txt,
          args0: [],
          category: Blockly.Categories.motion,
          colour: 330,
          extensions: ['shape_statement']
        }
        const value = []
        let funX = () => { return '' }
        let funY = () => { return '' }
        let funZ = () => { return '' }
        if (nwBlo.nbVar > 0) {
          obdep.args0.push({ type: 'input_value', name: 'X', default: '' })
          funX = (block) => { return Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '') }
          value.push({ nom: 'X', contenu: { valeur: ' ' } })
        }
        if (nwBlo.nbVar > 1) {
          obdep.args0.push({ type: 'input_value', name: 'Y', default: '' })
          funY = (block) => { return Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '') }
          value.push({ nom: 'Y', contenu: { valeur: ' ' } })
        }
        if (nwBlo.nbVar > 2) {
          obdep.args0.push({ type: 'input_value', name: 'Z', default: '' })
          funZ = (block) => { return Blockly.JavaScript.valueToCode(block, 'Z', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '') }
          value.push({ nom: 'Z', contenu: { valeur: ' ' } })
        }
        obdep.message0 = nwBlo.txt
        let yaicone = false
        if (this.scratch.params.creaIconeChoice) {
          obdep.args0.splice(0, 0, { type: 'field_vertical_separator' })
          obdep.args0.splice(0, 0, {
            type: 'field_image',
            src: urlImg(this.scratch.params.creaIconeChoice),
            width: 30,
            height: 30
          })
          yaicone = true
        }
        const xml = $.parseXML(nwBlo.xmlText)
        this.scratch.params.listeDef[nwBlo.nom] = {
          nom: nwBlo.nom,
          nbVar: nwBlo.nbVar,
          txt: nwBlo.txt,
          yaicone,
          xmlText: nwBlo.xmlText,
          value
        }
        Blockly.Blocks[nwBlo.nom] = {
          init: function () {
            this.jsonInit({
              type: 'make',
              message0: 'Définir ' + nwBlo.nom,
              inputsInline: false,
              nextStatement: null,
              colour: 330,
              tooltip: '',
              helpUrl: ''
            })
          }
        }
        Blockly.JavaScript[nwBlo.nom] = function () {
          return { type: 'make', eventtype: 'make', nom: nwBlo.nom }
        }
        Blockly.Blocks[nwBlo.nom + 'b'] = {
          init: function () {
            this.jsonInit(obdep)
          }
        }
        Blockly.JavaScript[nwBlo.nom + 'b'] = function (block) {
          const retValx = function () {
            return funX(block)
          }
          const retvaly = function () {
            return funY(block)
          }
          const retvalz = function () {
            return funZ(block)
          }
          return { type: 'Mb', nom: nwBlo.nom + 'b', valx: retValx(), valy: retvaly(), valz: retvalz() }
        }

        const side = 'start'
        this.scratch.params.scratchProgDonnees.workspaceDef = Blockly.inject(aVire, {
          comments: true,
          disable: false,
          collapse: false,
          media: './externals/blockly/media/',
          readOnly: false,
          rtl: null,
          scrollbars: true,
          toolbox: xml2,
          toolboxPosition: side === 'top' || side === 'start' ? 'start' : 'end',
          horizontalLayout: side === 'top' || side === 'bottom',
          sounds: false,
          zoom: {
            controls: true,
            wheel: true,
            startScale: 0.6,
            maxScale: 2,
            minScale: 0.25,
            scaleSpeed: 1.1
          },
          colours: {
            fieldShadow: 'rgba(100, 100, 100, 0.3)',
            dragShadowOpacity: 0.6
          }
        })
        Blockly.svgResize(this.scratch.params.scratchProgDonnees.workspaceDef)
        // this.scratch.params.scratchProgDonnees.workspaceDef.clear()
        let x = xml.firstChild
        while (x.nodeType !== 1) {
          x = x.nextSibling
        }
        Blockly.Xml.domToWorkspace(x, this.scratch.params.scratchProgDonnees.workspaceDef)
        const ladef = Blockly.JavaScript.workspaceToCode(this.scratch.params.scratchProgDonnees.workspaceDef)
        ladef[0].splice(0, 1)
        this.scratch.params.listeDef[nwBlo.nom].def = ladef[0]
        j3pEmpty(aVire)
      }
      j3pDetruit(aVire)
    }

    this.scratch.params.scratchProgDonnees.progdeb = []
    this.scratch.params.scratchProgDonnees.progdeb.push({
      type: 'Drapo',
      editable: 'false',
      deletable: 'false',
      movable: 'false',
      x: '20',
      y: '20'
    })
    for (let i = 0; i < this.scratch.params.MtgProg.suite.length; i++) {
      const ordre = this.scratch.params.MtgProg.suite[i]
      switch (ordre.type) {
        case 'segment':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'segmentAB',
            value: [{ nom: 'EXT1', contenu: { valeur: ordre.noms[0].replace('[', '').replace(']', '') } }]
          })
          break
        case 'segmentlongA':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'segmentlongA',
            value: [{
              nom: 'EXT1',
              contenu: { valeur: ordre.noms[0].replace('[', '').replace(']', '') }
            }, { nom: 'LONG', contenu: { valeur: ordre.long } }]
          })
          break
        case 'pointLibreAvecNom':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'pointLibreAvecNom',
            value: [{ nom: 'NOM', contenu: { valeur: ordre.noms[0] } }]
          })
          break
        case 'pointIntersection':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'pointIntersection',
            value: [{ nom: 'NOM', contenu: { valeur: ordre.noms[0] } }, {
              nom: 'CONT1',
              contenu: { valeur: ordre.cont1 }
            }, { nom: 'CONT2', contenu: { valeur: ordre.cont2 } }]
          })
          break
        case 'pointSur':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'pointSur',
            value: [{ nom: 'NOM', contenu: { valeur: ordre.noms[0] } }, {
              nom: 'CONT',
              contenu: { valeur: ordre.cont }
            }]
          })
          break
        case 'pointMilieu':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'pointMilieu',
            value: [{ nom: 'NOM', contenu: { valeur: ordre.noms[0] } }, {
              nom: 'CONT',
              contenu: { valeur: ordre.cont.replace('[', '').replace(']', '') }
            }]
          })
          break
        case 'pointLong':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'pointLong',
            value: [{ nom: 'EXT1', contenu: { valeur: ordre.noms[0] } },
              { nom: 'LONG', contenu: { valeur: ordre.long } },
              { nom: 'P1', contenu: { valeur: ordre.p1 } }
            ]
          })
          break
        case 'droitept':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'droitept',
            value: [{ nom: 'POINT1', contenu: { valeur: ordre.noms[0].replace('(', '').replace(')', '') } }]
          })
          break
        case 'droitepara':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'droitepara',
            value: [
              { nom: 'NOM', contenu: { valeur: ordre.noms[0] } },
              { nom: 'PARA', contenu: { valeur: ordre.para } },
              { nom: 'POINT', contenu: { valeur: ordre.point } }]
          })
          break
        case 'droiteperp':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'droiteperp',
            value: [
              { nom: 'NOM', contenu: { valeur: ordre.noms[0] } },
              { nom: 'PERP', contenu: { valeur: ordre.perp } },
              { nom: 'POINT', contenu: { valeur: ordre.point } }]
          })
          break
        case 'demidroitept':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'demidroitept',
            value: [{ nom: 'ORIGINE', contenu: { valeur: ordre.noms[0].replace('[', '').replace(')', '') } }]
          })
          break
        case 'cercleCentrePoint':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'cercleCentrePoint',
            value: [{ nom: 'CENTRE', contenu: { valeur: ordre.centre } }, {
              nom: 'POINT',
              contenu: { valeur: ordre.point }
            }, { nom: 'NOM', contenu: { valeur: ordre.noms[0] } }]
          })
          break
        case 'cercleCentreRayon':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'cercleCentreRayon',
            value: [{ nom: 'CENTRE', contenu: { valeur: ordre.centre } }, {
              nom: 'RAYON',
              contenu: { valeur: ordre.rayon }
            }, { nom: 'NOM', contenu: { valeur: ordre.noms[0] } }]
          })
          break
        case 'arc1PointPasse':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'arc1PointPasse',
            value: [{ nom: 'POINT', contenu: { valeur: ordre.point } }, {
              nom: 'NOM',
              contenu: { valeur: ordre.noms[0] }
            }]
          })
          break
        case 'arccentreplus':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'arccentreplus',
            value: [{ nom: 'CENTRE', contenu: { valeur: ordre.centre } }, {
              nom: 'NOM',
              contenu: { valeur: ordre.noms[0] }
            }]
          })
          break
        case 'arccentremoins':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'arccentremoins',
            value: [{ nom: 'CENTRE', contenu: { valeur: ordre.centre } }, {
              nom: 'NOM',
              contenu: { valeur: ordre.noms[0] }
            }]
          })
          break
        case 'mtgEffacer':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'mtgEffacer',
            value: [{ nom: 'NOM', contenu: { valeur: ordre.noms[0] } }]
          })
          break
        case 'mtgPointille':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'mtgPointille',
            value: [{ nom: 'NOM', contenu: { valeur: ordre.noms[0] } }]
          })
          break
        case 'renommer':
          this.scratch.params.scratchProgDonnees.progdeb.push({
            type: 'Renommer',
            value: [{ nom: 'NOM', contenu: { valeur: ordre.nom } }, { nom: 'NOM2', contenu: { valeur: ordre.nom2 } }]
          })
          break
      }
    }
    this.creeFigDep()
    const side = 'start'
    this.scratch.params.scratchProgDonnees.workspace = Blockly.inject(this.scratch.params.scratchProgDonnees.outPutWork, {
      comments: true,
      disable: false,
      collapse: false,
      media: '/externals/blockly/media/',
      readOnly: false,
      rtl: null,
      scrollbars: true,
      // toolbox: xml2,
      toolboxPosition: side === 'top' || side === 'start' ? 'start' : 'end',
      horizontalLayout: side === 'top' || side === 'bottom',
      sounds: false,
      zoom: {
        controls: true,
        wheel: true,
        startScale: 0.6,
        maxScale: 2,
        minScale: 0.25,
        scaleSpeed: 1.1
      },
      colours: {
        fieldShadow: 'rgba(100, 100, 100, 0.3)',
        dragShadowOpacity: 0.6
      }
    })
    // this.scratch.params.ModaleDefi.style.top = '0px'
    // this.scratch.params.ModaleDefi.style.left = '0px'
    this.scratch.params.scratchProgDonnees.outPutWork.style.height = '600px'
    this.scratch.params.scratchProgDonnees.outPutWork.style.width = '800px'
    this.scratch.params.scratchProgDonnees.outPutWork.style.minWidth = '800px'
    Blockly.svgResize(this.scratch.params.scratchProgDonnees.workspace)
    // this.scratch.params.scratchProgDonnees.workspaceDef.clear()
    // Blockly.Xml.domToWorkspace(x, this.scratch.params.scratchProgDonnees.workspace)
    this.stor.progSouv = ''
    this.intervalId = setInterval(this.verifPasChange.bind(this), 2000)
    this.refaisMenu()
    this.refaisProg()
  }

  choixSur () {
    if (!this.stor.listeSur.changed) return
    this.stor.listeSur.disable()
    if (this.stor.listeSur.reponse.indexOf('[') !== -1 || this.stor.listeSur.reponse.indexOf('(') !== -1) {
      this.stor.newPoint.type = 'pointSur'
      this.stor.newPoint.sur = [this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeSur.reponse)]
      this.stor.newPoint.cont = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeSur.reponse)
      this.stor.newPoint.dep = [this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeSur.reponse)]
    } else {
      this.stor.newPoint.sur = [this.stor.listeSur.reponse]
      this.stor.newPoint.haz = true
      this.stor.newPoint.dep = [this.stor.listeSur.reponse]
    }
    this.finishPoint()
  }

  finishPoint () {
    if (this.stor.newPoint.remplace) {
      this.stor.newPoint.remplace = undefined
      this.aret = undefined
    } else {
      this.scratch.params.MtgProg.figdep.push(this.stor.newPoint)
    }
    j3pEmpty(this.stor.lesdivs.actDepTitre)
    j3pEmpty(this.stor.lesdivs.actDep)
    this.affichelistedep()
  }

  okPointSym () {
    if (!this.stor.listeP1.changed) return
    if (!this.stor.listeP2.changed) return
    this.stor.listeP1.disable()
    this.stor.listeP2.disable()
    this.stor.newPoint.type = 'pointSym'
    this.stor.newPoint.dep = [this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse), this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)]
    this.stor.newPoint.centre = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    this.stor.newPoint.point = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    this.finishPoint()
  }

  okPointRot () {
    if (!this.stor.listeP1.changed) return
    if (!this.stor.listeP2.changed) return
    if (this.stor.zoneAngle.reponse() === '') return
    this.stor.listeP1.disable()
    this.stor.listeP2.disable()
    this.stor.zoneAngle.disable()
    this.stor.newPoint.type = 'pointRot'
    this.stor.newPoint.centre = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    this.stor.newPoint.dep = [this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse), this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)]
    this.stor.newPoint.point = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    this.stor.newPoint.angle = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.zoneAngle.reponse())
    this.finishPoint()
  }

  choixMilieu () {
    if (!this.stor.listeSur.changed) return
    this.stor.listeSur.disable()
    this.stor.newPoint.type = 'pointMilieu'
    this.stor.newPoint.sur = [this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeSur.reponse)]
    this.stor.newPoint.dep = [this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeSur.reponse)]
    this.stor.newPoint.cont = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeSur.reponse).replace(']', '').replace('[', '')
    this.finishPoint()
  }

  choixMarque () {
    if (!this.stor.listeSur.changed) return
    this.stor.listeSur.disable()
    this.scratch.params.MtgProg.figdep.push({ noms: ['htsglsvnilqquinrqei'], type: 'Marque', sur: [this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeSur.reponse)] })
    j3pEmpty(this.stor.lesdivs.actDepTitre)
    j3pEmpty(this.stor.lesdivs.actDep)
    this.affichelistedep()
  }

  choixInter () {
    if (!this.stor.listint1.changed) return
    if (!this.stor.listint2.changed) return
    if (this.stor.listint1.reponse === this.stor.listint2.reponse) return
    this.stor.listint1.disable()
    this.stor.listint2.disable()
    this.stor.newPoint.type = 'pointIntersection'
    this.stor.newPoint.cont1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listint1.reponse)
    this.stor.newPoint.cont2 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listint2.reponse)
    this.stor.newPoint.dep = [this.stor.newPoint.cont1, this.stor.newPoint.cont2]
    for (let i = 0; i < this.scratch.params.MtgProg.figdep.length; i++) {
      for (let j = 0; j < this.scratch.params.MtgProg.figdep[i].noms.length; j++) {
        if (this.scratch.params.MtgProg.figdep[i].noms[j] === this.stor.newPoint.cont1) {
          if (this.scratch.params.MtgProg.figdep[i].contient) {
            this.scratch.params.MtgProg.figdep[i].contient.push(this.stor.newPoint.noms[0])
          } else {
            this.scratch.params.MtgProg.figdep[i].contient = [this.stor.newPoint.noms[0]]
          }
        }
        if (this.scratch.params.MtgProg.figdep[i].noms[j] === this.stor.newPoint.cont2) {
          if (this.scratch.params.MtgProg.figdep[i].contient) {
            this.scratch.params.MtgProg.figdep[i].contient.push(this.stor.newPoint.noms[0])
          } else {
            this.scratch.params.MtgProg.figdep[i].contient = [this.stor.newPoint.noms[0]]
          }
        }
      }
    }
    this.finishPoint()
    j3pEmpty(this.stor.lesdivs.actDepTitre)
    j3pEmpty(this.stor.lesdivs.actDep)
    this.affichelistedep()
  }

  creeFigDep () {
    this.stor.nomDep = []
    for (let i = 0; i < this.scratch.params.MtgProg.figdep.length; i++) {
      this.stor.nomDep.push(this.scratch.params.MtgProg.figdep[i].noms[0])
    }
    const tabsup = addDefaultTable(this.stor.lesdivs.figDep, 10, 3)
    j3pAddContent(tabsup[0][0], '&nbsp;')
    j3pAddContent(tabsup[4][0], '&nbsp;')
    const contTatab2Dep = j3pAddElt(tabsup[1][1], 'div')
    contTatab2Dep.style.background = '#e0b89b'
    contTatab2Dep.style.padding = '10px'
    contTatab2Dep.style.border = '2px solid black'
    contTatab2Dep.style.borderRadius = '5px'
    const tatab2fig = addDefaultTable(contTatab2Dep, 1, 2)
    tatab2fig[0][0].style.textAlign = 'center'

    const tabFigDep = addDefaultTable(tatab2fig[0][0], 1, 9)
    tabFigDep[0][1].style.width = '200px'
    tabFigDep[0][8].style.width = '10px'
    j3pAddContent(tabFigDep[0][1], '<b><u>Figure de départ</u></b>')
    this.afficheExplik(tabFigDep[0][1], 'L’élève doit compléter<br> cette figure pour <br>réaliser la figure Modèle')
    j3pAddContent(tabFigDep[0][0], '&nbsp;&nbsp;')
    tabFigDep[0][1].style.verticalAlign = 'top'
    tabFigDep[0][3].style.verticalAlign = 'top'
    tabFigDep[0][5].style.verticalAlign = 'top'
    tabFigDep[0][7].style.verticalAlign = 'top'
    j3pAddContent(tabFigDep[0][2], '&nbsp;')
    j3pAddContent(tabFigDep[0][4], '&nbsp;')
    j3pAddContent(tabFigDep[0][6], '&nbsp;')
    j3pAjouteBouton(tabFigDep[0][3], this.importFigDep.bind(this), { value: 'importer' })
    j3pAjouteBouton(tabFigDep[0][5], this.exportFigDep.bind(this), { value: 'exporter' })
    this.stor.inputImportExportFig = j3pAddElt(tabFigDep[0][7], 'input', '', { value: '', size: 14 })
    this.afficheExplik(tabFigDep[0][7], 'Importer / exporter la figure de départ permet<br> de la copier d’un exercice à un autre')

    tatab2fig[0][1].style.padding = '5px'
    tatab2fig[0][1].style.verticalAlign = 'top'
    this.afficheExplik(tatab2fig[0][1], 'Les élèments <b>Zone</b> et <b>Zoneseg</b> ne seront pas visibles,<br> ils permettent de "limiter les emplacements possibles" d’un point.<br><br>Un point placé sur une <b>zone</b> sera placé aléatoirement dans<br> la zone en début d’exercice.')
    this.afficheExplik(tatab2fig[0][1], 'Les "noms aléatoires" des points sont de la forme <b>#1#</b>, <b>#2#</b>, ...<br> ils devront être nommés ainsi dans le programme attendu.')

    const tabReserv = addDefaultTable(tabsup[3][1], 1, 3)
    j3pAddContent(tabReserv[0][0], '&nbsp;<u>lettres réservées</u>:')
    this.scratch.params.zoneReserveLet = j3pAddElt(tabReserv[0][2], 'input', '', {
      value: this.stor.exoEncours.reservLettres || '',
      maxLength: 26,
      minLength: 1,
      size: 40
    })
    this.scratch.params.zoneReserveLet.addEventListener('input', this.affichelistedep.bind(this))
    this.afficheExplik(tabsup[3][1], 'Si le programme attendu ( plus bas ) utilise un nouveau point nommé explicitement A ( respectivement B )<br>La lettre <b>A</b> ( repectivement <b>B</b> ) doit être réservée ci dessus.')

    const tabConstEff = addDefaultTable(tabsup[7][1], 1, 2)
    j3pAddContent(tabConstEff[0][0], '<u>Traits de construction à effacer</u>:&nbsp;')
    this.afficheExplik(tabsup[7][1], 'La ressource vérifiera qu’il n’y a pas d’éléments supplémentaires tracés.')
    const lltabConstEff = (this.scratch.params.MtgProg.FoEfCo) ? ['Oui', 'Non'] : ['Non', 'Oui']
    this.scratch.params.tabConstEff = ListeDeroulante.create(tabConstEff[0][1], lltabConstEff, { choix0: true })

    const contFigMod = j3pAddElt(tabsup[5][1], 'div')
    contFigMod.style.padding = '5px'
    contFigMod.style.background = '#e0b89b'
    contFigMod.style.border = '2px solid black'
    contFigMod.style.borderRadius = '5px'
    contFigMod.classList.add('pourFitContent')
    const tabtruc = addDefaultTable(contFigMod, 3, 2)
    tabtruc[1][0].style.padding = '5px'
    tabtruc[1][1].style.padding = '5px'
    tabtruc[1][1].style.border = '1px solid black'
    const tabMod = addDefaultTable(tatab2fig[0][0], 1, 3)
    j3pAddContent(tabMod[0][1], '&nbsp;')
    tabMod[0][1].style.width = '10px'
    this.scratch.params.Model = tabMod[0][0]
    this.stor.lesdivs.modifDep = tabMod[0][2]
    tabMod[0][2].style.verticalAlign = 'top'
    tabMod[0][2].style.border = '1px solid black'
    tabMod[0][2].style.background = '#eeeeee'
    tabMod[0][0].style.verticalAlign = 'top'
    this.stor.lesdivs.actDepTitre = tabMod[0][2]
    this.stor.lesdivs.actDep = tabMod[0][2]
    this.stor.lesdivs.actDep.style.verticalAlign = 'top'

    const tabGen = addDefaultTable(tabtruc[0][1], 1, 3)
    j3pAddContent(tabGen[0][1], '&nbsp;')
    tabGen[0][1].style.width = '100%'

    tabtruc[1][1].style.background = '#e8e8e8'
    tabtruc[1][1].style.width = '350px'
    tabtruc[1][1].style.verticalAlign = 'top'
    const tabTruc = addDefaultTable(tabtruc[1][1], 5, 1)
    j3pAddTxt(tabTruc[0][0], 'Déplace')
    this.stor.lacocheModele = j3pAjouteCaseCoche(tabTruc[0][0])
    this.stor.lacocheModele.checked = this.stor.exoEncours.placeDep
    this.stor.lacocheModele.onchange = () => {
      this.stor.exoEncours.placeDep = this.stor.lacocheModele.checked
    }
    this.afficheExplik(tabTruc[0][0], 'Quand cette case est cochée,<br> la <b>figure de départ</b> de l’élève <br>s’obtient par une rotation aléatoire<br> et / ou une symétrie axiale de<br> la figure de départ ci-dessus.<br><br>Cette option ne se voit pas dans cette éditeur.')
    j3pAddContent(tabTruc[1][0], '&nbsp;')
    j3pAddContent(tabTruc[3][0], '&nbsp;')
    this.stor.bouGenere = j3pAjouteBouton(tabTruc[2][0], this.funcBoutGenere.bind(this), { value: 'Générer' })
    this.afficheExplik(tabTruc[2][0], 'Ce bouton permet de créer la <b>figure Modèle</b><br> à partir du <b>Programme attendu</b> ( plus bas )<br> Si la génération pose un problème,<br> l’exercice ne peut être enregistré.')
    this.stor.lesdivs.GenerationOK = tabTruc[4][0]
    this.genereOK(false, true)

    const ttext = addDefaultTable(tabtruc[0][0], 2, 1)
    j3pAddContent(ttext[0][0], '<b><u>Figure Modèle</u></b>')
    ttext[0][0].style.textAlign = 'Left'
    this.afficheExplik(ttext[0][0], 'Figure que l’élève doit reproduire.<br>Elle se génère avec le bouton <b>Générer</b> à droite, <br> qui éxécute le programme attendu.<br>La ressource comptera bon un programme qui<br> réalise une figure similaire.')
    const ttext2 = addDefaultTable(ttext[1][0], 1, 3)
    ttext[1][0].style.padding = '5px'
    j3pAddContent(ttext2[0][0], '&nbsp;<u>Texte</u>:&nbsp;')
    this.stor.letext = j3pAddElt(ttext2[0][1], 'input', '', { value: this.scratch.params.MtgProg.text || '', size: 60 })
    this.stor.letext.addEventListener('input', () => {
      this.scratch.params.MtgProg.text = this.stor.letext.value
    })
    j3pAddContent(ttext2[0][1], '&nbsp;&nbsp;&nbsp;')
    this.afficheExplik(ttext[1][0], 'Texte qui sera affiché au dessus de la figure. <br> Pour faire référence à un point nommé aléatoirement, <br> écrire par exmple: le point #1# ..., ou le segment [#1##2#] ...')

    tabtruc[0][1].style.textAlign = 'center'
    tabtruc[0][1].style.height = '5px'
    const lasttab = addDefaultTable(tabGen[0][0], 2, 1)
    this.scratch.params.scratchProgDonnees.mypre = tabtruc[1][0]
    this.scratch.params.mtgEditoText = lasttab[0][0]

    const tatab2 = addDefaultTable(this.stor.lesdivs.Programme, 3, 7)
    this.stor.lesdivs.Programme.classList.add('pourFitContent')
    this.stor.lesdivs.Programme.style.padding = '5px'
    const tabFigProgramme = addDefaultTable(tatab2[0][1], 1, 7)
    tatab2[1][1].style.border = '1px solid black'
    tatab2[1][1].style.padding = '0px'
    tatab2[1][5].style.padding = '0px'
    tatab2[1][5].style.border = '1px solid black'
    j3pAddContent(tabFigProgramme[0][0], '<b><u>Programme attendu</u></b>')
    tabFigProgramme[0][0].style.width = '400px'
    this.afficheExplik(tabFigProgramme[0][0], 'Programme permmettant de construire la <b>figure modèle</b> <br>à partir de la <b>figure de départ</b>, et présenté comme<br>solution quand la répone de l’élève est jugée fausse.')
    tabFigProgramme[0][0].style.verticalAlign = 'top'
    tabFigProgramme[0][2].style.verticalAlign = 'top'
    tabFigProgramme[0][4].style.verticalAlign = 'top'
    tabFigProgramme[0][6].style.verticalAlign = 'top'
    j3pAddContent(tabFigProgramme[0][1], '&nbsp;')
    j3pAddContent(tabFigProgramme[0][3], '&nbsp;')
    j3pAddContent(tabFigProgramme[0][5], '&nbsp;')
    j3pAjouteBouton(tabFigProgramme[0][2], this.importProgramme.bind(this), { value: 'importer' })
    j3pAjouteBouton(tabFigProgramme[0][4], this.exportProgramme.bind(this), { value: 'exporter' })
    this.stor.importExportProgramme = j3pAddElt(tabFigProgramme[0][6], 'input', '', { value: '', size: 14 })
    this.afficheExplik(tabFigProgramme[0][6], 'Importer / exporter ce programme<br>permet de le copier d’un exercice<br> à un autre')

    tatab2[0][1].style.textAlign = 'center'
    j3pAddContent(tatab2[0][5], '<b><u>Blocs disponibles</u></b>')
    tatab2[0][5].style.verticalAlign = 'top'
    tatab2[0][5].style.textAlign = 'center'
    j3pAddContent(tatab2[0][2], '&nbsp;&nbsp;')
    this.divListRac = tatab2[1][3]
    j3pAddContent(tatab2[0][4], '&nbsp;&nbsp;')
    this.scratch.params.scratchProgDonnees.outPutWork = tatab2[1][1]
    tatab2[2][0].style.height = '5px'
    const tatab3 = addDefaultTable(this.stor.lesdivs.NouveauBlocs, 4, 3)
    j3pAddContent(tatab3[0][0], '&nbsp;')
    j3pAddContent(tatab3[3][2], '&nbsp;')
    const tabFigBlocs = addDefaultTable(tatab3[0][1], 1, 7)
    j3pAddContent(tabFigBlocs[0][0], '<b>block persos</b>')
    j3pAddContent(tabFigBlocs[0][1], '&nbsp;')
    j3pAddContent(tabFigBlocs[0][3], '&nbsp;')
    j3pAddContent(tabFigBlocs[0][5], '&nbsp;')
    j3pAjouteBouton(tabFigBlocs[0][2], this.importBlocs.bind(this), { value: 'importer' })
    j3pAjouteBouton(tabFigBlocs[0][4], this.exportBlocs.bind(this), { value: 'exporter' })
    this.stor.importExportBlocSpers = j3pAddElt(tabFigBlocs[0][6], 'input', '', { value: '', size: 20 })

    this.stor.lesdivs.DeDefPerso = tatab3[2][1]
    this.scratch.lesdivs = {}
    this.scratch.params.ModaleCreaisEditing = tatab3[2][1]
    this.scratch.lesdivs.divBlocPersoListe = tatab3[2][1]
    const tatab4 = addDefaultTable(tatab2[1][5], 25, 2)
    tatab2[1][5].style.background = '#ffdddd'
    tatab2[1][5].style.verticalAlign = 'top'
    j3pAddContent(tatab2[1][6], '&nbsp;&nbsp;')
    let i = 0
    this.stor.listeCocheBloc = []
    if (this.stor.exoEncours.listeBloc.pointille === undefined) this.stor.exoEncours.listeBloc.pointille = false
    for (const property in this.stor.exoEncours.listeBloc) {
      i++
      j3pAddContent(tatab4[i][0], `&nbsp;${property}`)
      this.stor.listeCocheBloc.push(j3pAjouteCaseCoche(tatab4[i][1]))
      this.stor.listeCocheBloc[i - 1].checked = this.stor.exoEncours.listeBloc[property]
      this.stor.listeCocheBloc[i - 1].onchange = this.getChangeBlocListener(i - 1, property)
    }
    this.affichelistedep()
    this.affichelisteBlocs()
  }

  exportBlocs () {
    this.stor.importExportBlocSpers.value = JSON.stringify(this.scratch.params.listeDef)
  }

  exportFigDep () {
    this.stor.inputImportExportFig.value = JSON.stringify(this.scratch.params.MtgProg.figdep)
  }

  exportProgramme () {
    const Blockly = this.scratch.Blockly
    const lxml = Blockly.Xml.workspaceToDom(this.scratch.params.scratchProgDonnees.workspace)
    this.stor.importExportProgramme.value = Blockly.Xml.domToText(lxml)
  }

  faisFauxImoFig () {
    this.stor.inputImportExportFig.value = 'JSON invalide'
  }

  funcBoutGenere () {
    this.scratch.params.NbEssaiIllimite = true
    j3pEmpty(this.scratch.params.mtgEditoText)
    this.scratch.params.mtgEditoText.style.background = '#ffaaff'
    j3pAddTxt(this.scratch.params.mtgEditoText, '... Génération en cours ...')
    this.scratch.params.scratchProgDonnees.pause = false
    this.scratch.params.MtgProg.suite = []
    this.genereOK(true)
    this.scratch.params.scratchProgDonnees.avertissements = []
    this.scratch.params.scratchProgDonnees.tabcodeX = this.scratch.scratchRunBlockly(this.scratch.params.scratchProgDonnees.workspace)
    this.scratch.execProg2(this.scratch.params.scratchProgDonnees.tabcodeX)
    this.stor.bouGenere.enabled = false
  }

  genereOK (b, pass) {
    const mes = (b) ? 'Génération OK' : 'Génération pas OK'
    j3pEmpty(this.stor.lesdivs.GenerationOK)
    j3pAddContent(this.stor.lesdivs.GenerationOK, mes)
    this.stor.lesdivs.GenerationOK.style.color = (b) ? '#00aa00' : '#aa0000'
    this.scratch.params.mtgProgOk = b
    this.stor.btnValiderNom.disabled = !b
    if (!pass) {
      setTimeout(() => {
        this.affichelistedep(b)
      }, 500)
    }
  }

  importBlocs () {
    try {
      const tt = JSON.parse(this.stor.importExportBlocSpers.value)
      this.scratch.params.BlocsAAj = j3pClone(tt)
      this.rajoutdefenvrai()
      this.stor.importExportBlocSpers.value = 'import OK'
      this.affichelisteBlocs()
      this.refaisMenu()
      this.genereOK(false)
    } catch (error) {
      this.stor.importExportBlocSpers.value = 'JSON Invalide'
    }
  }

  importFigDep () {
    try {
      const tt = JSON.parse(this.stor.inputImportExportFig.value)
      // TODO test si figdep valide
      /*
      for (let i = 0; i < tt.length; i++) {
        const ok = true
        if (!tt[i].noms) {
          faisFauxImoFig()
          return
        }
        if (Array.isArray(tt[i].noms)) {
          faisFauxImoFig()
          return
        }
        if (tt[i].noms.length < 1) {
          faisFauxImoFig()
          return
        }
        if (!tt[i].type) {
          faisFauxImoFig()
          return
        }
        if (tt[i].type === 'zone') {

        }
        if (tt[i].type === 'point') {

        }

        if (tt[i].type === 'zoneSeg') {

        }
      }
       */
      this.scratch.params.MtgProg.figdep = tt
      this.stor.inputImportExportFig.value = 'import OK'
      this.affichelistedep()
    } catch (error) {
      this.faisFauxImoFig()
    }
  }

  importProgramme () {
    try {
      const xmlDoc = $.parseXML(this.stor.importExportProgramme.value)
      let x = xmlDoc.firstChild
      while (x.nodeType !== 1) {
        x = x.nextSibling
      }
      this.scratch.params.scratchProgDonnees.workspace.clear()
      this.scratch.Blockly.Xml.domToWorkspace(x, this.scratch.params.scratchProgDonnees.workspace)
      this.genereOK(false)
    } catch (error) {
      this.stor.importExportProgramme.value = 'Programme invalide'
    }
  }

  getChangeBlocListener (i, property) {
    return () => {
      this.stor.exoEncours.listeBloc[property] = this.stor.listeCocheBloc[i].checked
      this.refaisMenu()
    }
  }

  getSliderFunction (numob) {
    return (val) => {
      const ob = this.scratch.params.MtgProg.figdep[numob]
      let don
      for (let i = 0; i < this.scratch.params.donnesMtg.length; i++) {
        if (this.scratch.params.donnesMtg[i].noms[0] === this.scratch.scratchMtgReplaceLettresInv(ob.noms[0])) don = this.scratch.params.donnesMtg[i]
      }
      if (ob.sur) {
        const surkoi = this.retrouveDep({ nom: ob.sur[0] }, 'objet')
        this.scratch.params.mtgApplecteur.setApiDoc(this.scratch.params.svgId2)
        switch (surkoi.type) {
          case 'zoneSeg': {
            const co1 = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'zs' + ob.sur[0].replace('zoneSeg', '') + 'c' })
            const co2 = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'zs' + ob.sur[0].replace('zoneSeg', '') + 'p' })
            const coef = val
            const x = (co1.x * coef + (100 - coef) * co2.x) / 100
            const y = (co1.y * coef + (100 - coef) * co2.y) / 100
            ob.coef = coef
            this.scratch.params.mtgApplecteur.setPointPosition(this.scratch.params.svgId2, don.tag, x, y, true)
            this.scratch.params.mtgApplecteur.setPointPosition(this.scratch.params.svgId, don.tag, x, y, true)
          }
            break
          case 'zone': {
            const co1 = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'z' + ob.sur[0].replace('zone', '') + 'c' })
            const co2 = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'z' + ob.sur[0].replace('zone', '') + 'p' })
            const coef = val
            const ladist = this.dist(co1, co2)
            const x = coef / 100 * ladist * Math.cos((10 * coef) / 180 * Math.PI) + co1.x
            const y = coef / 100 * ladist * Math.sin((10 * coef) / 180 * Math.PI) + co1.y
            ob.coef = coef
            this.scratch.params.mtgApplecteur.setPointPosition(this.scratch.params.svgId2, don.tag, x, y, true)
            this.scratch.params.mtgApplecteur.setPointPosition(this.scratch.params.svgId, don.tag, x, y, true)
          }
            break
        }
      }
    }
  }

  modifDef (i) {
    j3pEmpty(this.stor.lesdivs.DeDefPerso)
    j3pAddContent(this.stor.lesdivs.DeDefPerso, '<b>Définiton de <i>' + this.scratch.params.lesBlocPerso[i] + '</i></b>')
    j3pEmpty(this.scratch.lesdivs.divBlocPersoListe)
    this.scratch.scratchDemandeMenu({ label: this.scratch.params.lesBlocPerso[i] })
    window.scroll({
      top: 2000,
      left: 0,
      behavior: 'smooth'
    })
  }

  modifExo (i) {
    super.modifExo(i)
    this.stor.avertGen = j3pAddElt(this.stor.lesdivs.divExo, 'div')
    j3pAddContent(this.stor.avertGen, 'Pour valider l\'exercice, l\'éditeur a besoin de générer la figure modèle.<br><i>Cliquer sur le bouton </i><b>"Générer"</b><i> à droite de la </i><b>Figure Modèle.</b></i><br><br>')
    this.stor.avertGen.style.color = '#2cc42c'
    // on veut ajouter un input pour le niveau dans le tableau de modif de l’exo (de base ça modifie seulement le nom)
    this.cestParti()
  }

  newNomDiese () {
    const tabNon = []
    for (let i = 0; i < this.scratch.params.MtgProg.figdep.length; i++) {
      tabNon.push(this.scratch.params.MtgProg.figdep[i].noms[0])
      if (this.scratch.params.MtgProg.figdep[i].type === 'segmentlongA') tabNon.push(this.scratch.params.MtgProg.figdep[i].ext2)
    }
    for (let i = 1; i < 100; i++) {
      if (tabNon.indexOf('#' + i + '#') === -1) return '#' + i + '#'
    }
  }

  modifDepend (el, nName, oName) {
    if (el.dep) this.replceEl(el.dep, nName, oName)
    if (el.cont) el.cont = this.replceEl2(el.cont, nName, oName)
    if (el.sur) this.replceEl(el.sur, nName, oName)
  }

  replceEl2 (a, b, c) {
    for (let i = 0; i < c.length; i++) {
      if (a === c[i]) {
        return b[0]
      }
    }
    return a
  }

  replceEl (tab, n, o) {
    let ya = false
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < o.length; j++) {
        ya = ya || (tab[i] === o[j])
      }
    }
    if (!ya) return
    for (let i = tab.length - 1; i > -1; i--) {
      for (let j = 0; j < o.length; j++) {
        if (tab[i] === o[j]) {
          tab.splice(i, 1)
          break
        }
      }
    }
    for (let i = 0; i < n.length; i++) {
      tab.push(n[i])
    }
  }

  okDroite () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (!this.stor.listeP2.changed) {
      this.stor.listeP2.focus()
      return
    }
    if (this.stor.listeP1.reponse === this.stor.listeP2.reponse) {
      this.stor.listeP2.focus()
      return
    }
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    const p2 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    if (this.aret) {
      this.scratch.params.MtgProg.figdep[this.aret] = { type: 'droitept', ext1: p1, ext2: p2, noms: ['(' + p1 + p2 + ')', '(' + p2 + p1 + ')'], pointille: this.stor.listeP3.reponse === 'Oui', dep: [p1, p2], contient: [p1, p2] }
      for (let i = 0; i < this.aretDep.length; i++) {
        const el = this.scratch.params.MtgProg.figdep[this.aretDep[i]]
        this.modifDepend(el, ['(' + p1 + p2 + ')', '(' + p2 + p1 + ')'], this.oldNom)
      }
      this.aret = undefined
      this.oldNom = undefined
      this.aretDep = undefined
    } else {
      this.scratch.params.MtgProg.figdep.push(
        { type: 'droitept', ext1: p1, ext2: p2, noms: ['(' + p1 + p2 + ')', '(' + p2 + p1 + ')'], pointille: this.stor.listeP3.reponse === 'Oui', dep: [p1, p2], contient: [p1, p2] }
      )
    }
    this.stor.listeP2.disable()
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.affichelistedep()
  }

  okSegment () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (!this.stor.listeP2.changed) {
      this.stor.listeP2.focus()
      return
    }
    if (this.stor.listeP1.reponse === this.stor.listeP2.reponse) {
      this.stor.listeP2.focus()
      return
    }
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    const p2 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    if (this.aret) {
      this.scratch.params.MtgProg.figdep[this.aret] = { type: 'segment', ext1: p1, ext2: p2, noms: ['[' + p1 + p2 + ']', '[' + p2 + p1 + ']'], pointille: this.stor.listeP3.reponse === 'Oui', dep: [p1, p2], contient: [p1, p2] }
      for (let i = 0; i < this.aretDep.length; i++) {
        const el = this.scratch.params.MtgProg.figdep[this.aretDep[i]]
        this.modifDepend(el, ['[' + p1 + p2 + ']', '[' + p2 + p1 + ']'], this.oldNom)
      }
      this.aret = undefined
      this.oldNom = undefined
      this.aretDep = undefined
    } else {
      this.scratch.params.MtgProg.figdep.push(
        { type: 'segment', ext1: p1, ext2: p2, noms: ['[' + p1 + p2 + ']', '[' + p2 + p1 + ']'], pointille: this.stor.listeP3.reponse === 'Oui', dep: [p1, p2], contient: [p1, p2] }
      )
    }
    this.stor.listeP2.disable()
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.affichelistedep()
  }

  okDemiDroite () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (!this.stor.listeP2.changed) {
      this.stor.listeP2.focus()
      return
    }
    if (this.stor.listeP1.reponse === this.stor.listeP2.reponse) {
      this.stor.listeP2.focus()
      return
    }
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    const p2 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    if (this.aret) {
      this.scratch.params.MtgProg.figdep[this.aret] = { type: 'demidroitept', ext1: p1, ext2: p2, noms: ['[' + p1 + p2 + ')', '(' + p2 + p1 + ']'], pointille: this.stor.listeP3.reponse === 'Oui', dep: [p1, p2], contient: [p1, p2] }
      for (let i = 0; i < this.aretDep.length; i++) {
        const el = this.scratch.params.MtgProg.figdep[this.aretDep[i]]
        this.modifDepend(el, ['[' + p1 + p2 + ')', '(' + p2 + p1 + ']'], this.oldNom)
      }
      this.aret = undefined
      this.oldNom = undefined
      this.aretDep = undefined
    } else {
      this.scratch.params.MtgProg.figdep.push(
        { type: 'demidroitept', ext1: p1, ext2: p2, noms: ['[' + p1 + p2 + ')', '(' + p2 + p1 + ']'], pointille: this.stor.listeP3.reponse === 'Oui', dep: [p1, p2], contient: [p1, p2] }
      )
    }
    this.stor.listeP2.disable()
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.affichelistedep()
  }

  okCercle () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (!this.stor.listeP2.changed) {
      this.stor.listeP2.focus()
      return
    }
    if (this.stor.listeP1.reponse === this.stor.listeP2.reponse) {
      this.stor.listeP2.focus()
      return
    }
    const reponse = this.stor.zoneNom.reponse()
    if (reponse === '') {
      this.stor.zoneNom.focus()
      return
    }
    const nom = reponse
    if (this.ceNomEstPris(nom)) return
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    const p2 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    this.scratch.params.MtgProg.figdep.push(
      { type: 'cercleCentrePoint', centre: p1, point: p2, noms: [nom], pointille: this.stor.listeP3.reponse === 'Oui', nomCache: this.stor.listeP4.reponse === 'Oui', dep: [p1, p2], contient: [p2] }
    )
    this.stor.listeP2.disable()
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.stor.listeP4.disable()
    this.stor.zoneNom.disable()
    this.affichelistedep()
  }

  modifCerclePt (num) {
    this.aret = num
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’un cercle</u></b><br><br>')
    const ll = this.fileMoiLesPoints(num)
    const tyu = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu[0][0], 'centre:&nbsp;')
    this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
    const tyu3 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu3[0][0], 'point:&nbsp;')
    this.stor.listeP2 = ListeDeroulante.create(tyu3[0][1], ll, {})
    const tyu2 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
    this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
    const tyu4 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu4[0][0], 'Nom caché:&nbsp;')
    this.stor.listeP4 = ListeDeroulante.create(tyu4[0][0], ['Non', 'Oui'], { choix0: true })
    j3pAddContent(this.stor.lesdivs.modifDep, '<br>')
    j3pAjouteBouton(this.stor.lesdivs.modifDep, this.okCercleMod.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3, this.stor.listeP4, this.stor.zoneNom]
  }

  modifCercleRay (num) {
    this.aret = num
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’un cercle</u></b><br><br>')
    const ll = this.fileMoiLesPoints(num)
    const tyu = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu[0][0], 'centre:&nbsp;')
    this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
    const tyu3 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu3[0][0], 'rayon:&nbsp;')
    this.stor.listeP2 = j3pAddElt(this.stor.lesdivs.modifDep, 'input', '', {
      size: 10
    })
    this.stor.listeP2.addEventListener('change', () => {
      let niveau = Number(this.stor.listeP2.value)
      if (isNaN(this.stor.listeP2.value) || this.stor.listeP2.value === '') niveau = 3
      if (niveau < 1) {
        niveau = 1
      } else if (niveau > 10) {
        niveau = 10
      }
      this.stor.listeP2.value = String(niveau)
    })
    const tyu2 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
    this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
    const tyu4 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu4[0][0], 'Nom caché:&nbsp;')
    this.stor.listeP4 = ListeDeroulante.create(tyu4[0][0], ['Non', 'Oui'], { choix0: true })
    j3pAddContent(this.stor.lesdivs.modifDep, '<br>')
    j3pAjouteBouton(this.stor.lesdivs.modifDep, this.okCercleRayonMod.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP3, this.stor.listeP4]
  }

  okCercleMod () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (!this.stor.listeP2.changed) {
      this.stor.listeP2.focus()
      return
    }
    if (this.stor.listeP1.reponse === this.stor.listeP2.reponse) {
      this.stor.listeP2.focus()
      return
    }
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    const p2 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    this.scratch.params.MtgProg.figdep[this.aret].type = 'cercleCentrePoint'
    this.scratch.params.MtgProg.figdep[this.aret].centre = p1
    this.scratch.params.MtgProg.figdep[this.aret].point = p2
    this.scratch.params.MtgProg.figdep[this.aret].pointille = this.stor.listeP3.reponse === 'Oui'
    this.scratch.params.MtgProg.figdep[this.aret].nomCache = this.stor.listeP4.reponse === 'Oui'
    this.scratch.params.MtgProg.figdep[this.aret].dep = [p1, p2]
    this.scratch.params.MtgProg.figdep[this.aret].contient = [p2]
    this.stor.listeP2.disable()
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.stor.listeP4.disable()
    this.affichelistedep()
  }

  modifDroite (num) {
    this.aretDep = this.retrouveListeDep([num])
    this.oldNom = j3pClone(this.scratch.params.MtgProg.figdep[num].noms)
    this.aret = num
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’une droite</u></b><br><br>')
    const ll = this.fileMoiLesPoints(num)
    const tyu = addDefaultTable(this.stor.lesdivs.modifDep, 1, 5)
    this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
    this.stor.listeP2 = ListeDeroulante.create(tyu[0][3], ll, {})
    j3pAddContent(tyu[0][0], '(&nbsp;')
    j3pAddContent(tyu[0][2], '&nbsp;')
    j3pAddContent(tyu[0][4], '&nbsp;)')
    const tyu2 = addDefaultTable(this.stor.lesdivs.modifDep, 1, 2)
    j3pAddContent(tyu2[0][0], 'Pointillés:&nbsp;')
    this.stor.listeP3 = ListeDeroulante.create(tyu2[0][0], ['Non', 'Oui'], { choix0: true })
    j3pAjouteBouton(this.stor.lesdivs.modifDep, this.okDroite.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.listeP3]
  }

  okCercleRayon () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (this.stor.listeP2.value === '') return
    const reponse = this.stor.zoneNom.reponse()
    if (reponse === '') {
      this.stor.zoneNom.focus()
      return
    }
    const nom = reponse
    if (this.ceNomEstPris(nom)) return
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    this.scratch.params.MtgProg.figdep.push(
      { type: 'cercleCentreRayon', centre: p1, rayon: this.stor.listeP2.value, noms: [nom], pointille: this.stor.listeP3.reponse === 'Oui', nomCache: this.stor.listeP4.reponse === 'Oui', dep: [p1], contient: [] }
    )
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.stor.listeP4.disable()
    this.stor.zoneNom.disable()
    this.affichelistedep()
  }

  okCercleRayonMod () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (this.stor.listeP2.value === '') return
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    this.scratch.params.MtgProg.figdep[this.aret].type = 'cercleCentreRayon'
    this.scratch.params.MtgProg.figdep[this.aret].centre = p1
    this.scratch.params.MtgProg.figdep[this.aret].rayon = this.stor.listeP2.value
    this.scratch.params.MtgProg.figdep[this.aret].pointille = this.stor.listeP3.reponse === 'Oui'
    this.scratch.params.MtgProg.figdep[this.aret].nomCache = this.stor.listeP4.reponse === 'Oui'
    this.scratch.params.MtgProg.figdep[this.aret].dep = [p1]
    this.scratch.params.MtgProg.figdep[this.aret].contient = []
    this.aret = undefined
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.stor.listeP4.disable()
    this.affichelistedep()
  }

  okSegmentLong () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    this.stor.newSeg = { type: 'segmentlongA', ext1: p1, pointille: this.stor.listeP3.reponse === 'Oui', long: this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.value), dep: [p1], contient: [p1] }
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    j3pEmpty(this.stor.tabDepAct[0][0])
    const tyu = addDefaultTable(this.stor.tabDepAct[0][0], 1, 4)
    j3pAddContent(tyu[0][0], '2e extrémité:&nbsp;')
    this.stor.zoneNom = new ZoneStyleMathquill1(tyu[0][1], {
      restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789\'',
      lim: 4,
      limite: 4,
      enter: this.okNomDebSeg.bind(this),
      j3pCont: this.container
    })
    j3pAddContent(tyu[0][2], '&nbsp;')
    j3pAjouteBouton(tyu[0][3], this.okNomDebSeg.bind(this), { value: 'OK' })
    j3pAjouteBouton(this.stor.tabDepAct[0][0], this.okNomAleaSeg.bind(this), { value: 'Nom aléatoire' })
    this.stor.listAnulle = [this.stor.zoneNom]
  }

  okSegmentLongMod () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.scratch.params.MtgProg.figdep[this.aret].type = 'segmentlongA'
    this.scratch.params.MtgProg.figdep[this.aret].ext1 = p1
    this.scratch.params.MtgProg.figdep[this.aret].pointille = this.stor.listeP3.reponse === 'Oui'
    this.scratch.params.MtgProg.figdep[this.aret].long = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.value)
    const p2 = this.scratch.params.MtgProg.figdep[this.aret].ext2
    this.scratch.params.MtgProg.figdep[this.aret].dep = [p1, p2]
    this.scratch.params.MtgProg.figdep[this.aret].contient = [p1, p2]
    this.scratch.params.MtgProg.figdep[this.aret].noms = ['[' + p1 + p2 + ']', '[' + p2 + p1 + ']']
    for (let i = 0; i < this.aretDep.length; i++) {
      const el = this.scratch.params.MtgProg.figdep[this.aretDep[i]]
      this.modifDepend(el, ['[' + p1 + p2 + ']', '[' + p2 + p1 + ']'], this.oldNom)
    }
    this.aret = undefined
    this.oldNom = undefined
    this.aretDep = undefined
    this.affichelistedep()
  }

  okParaMod () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (!this.stor.listeP2.changed) {
      this.stor.listeP2.focus()
      return
    }
    const reponse = this.stor.zoneNom.reponse()
    if (reponse === '') return
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    const d1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    this.scratch.params.MtgProg.figdep[this.aret] = { type: 'droitepara', point: p1, para: d1, pointille: this.stor.listeP3.reponse === 'Oui', dep: [p1, d1], contient: [p1], noms: [reponse], nomCache: this.stor.listeP4.reponse === 'Oui' }
    for (let i = 0; i < this.aretDep.length; i++) {
      const el = this.scratch.params.MtgProg.figdep[this.aretDep[i]]
      this.modifDepend(el, [reponse], this.oldNom)
    }
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.stor.listeP4.disable()
    this.stor.listeP2.disable()
    this.stor.zoneNom.disable()
    j3pEmpty(this.stor.tabDepAct[0][0])
    this.aret = undefined
    this.aretDep = undefined
    this.oldNom = undefined
    this.affichelistedep()
  }

  okPerpMod () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (!this.stor.listeP2.changed) {
      this.stor.listeP2.focus()
      return
    }
    const reponse = this.stor.zoneNom.reponse()
    if (reponse === '') return
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    const d1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    this.scratch.params.MtgProg.figdep[this.aret] = { type: 'droiteperp', point: p1, perp: d1, pointille: this.stor.listeP3.reponse === 'Oui', dep: [p1, d1], contient: [p1], noms: [reponse], nomCache: this.stor.listeP4.reponse === 'Oui' }
    for (let i = 0; i < this.aretDep.length; i++) {
      const el = this.scratch.params.MtgProg.figdep[this.aretDep[i]]
      this.modifDepend(el, [reponse], this.oldNom)
    }
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.stor.listeP4.disable()
    this.stor.listeP2.disable()
    this.stor.zoneNom.disable()
    j3pEmpty(this.stor.tabDepAct[0][0])
    this.aret = undefined
    this.aretDep = undefined
    this.oldNom = undefined
    this.affichelistedep()
  }

  okPara () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (!this.stor.listeP2.changed) {
      this.stor.listeP2.focus()
      return
    }
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    const d1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    this.stor.newDte = { type: 'droitepara', point: p1, para: d1, pointille: this.stor.listeP3.reponse === 'Oui', dep: [p1, d1], contient: [p1] }
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.stor.listeP2.disable()
    j3pEmpty(this.stor.tabDepAct[0][0])
    const tyu = addDefaultTable(this.stor.tabDepAct[0][0], 1, 2)
    j3pAddContent(tyu[0][0], 'Nom:&nbsp;')
    this.stor.zoneNom = new ZoneStyleMathquill1(tyu[0][1], {
      restric: '()\'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMONPQRSTUVWXYZ',
      lim: 4,
      limite: 4,
      enter: this.okNomDebPara.bind(this),
      j3pCont: this.container
    })
    const tyu2 = addDefaultTable(this.stor.tabDepAct[0][0], 1, 2)
    j3pAddContent(tyu2[0][0], 'Nom caché:&nbsp;')
    this.stor.listeCache = ListeDeroulante.create(tyu2[0][1], ['Non', 'Oui'], { choix0: true })
    j3pAjouteBouton(this.stor.tabDepAct[0][0], this.okNomDebPara.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.zoneNom, this.stor.listeCache]
  }

  okPerp () {
    if (!this.stor.listeP1.changed) {
      this.stor.listeP1.focus()
      return
    }
    if (!this.stor.listeP2.changed) {
      this.stor.listeP2.focus()
      return
    }
    const p1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP1.reponse)
    const d1 = this.scratch.scratchMtgReplaceLettresInvVraiment(this.stor.listeP2.reponse)
    this.stor.newDte = { type: 'droiteperp', point: p1, perp: d1, pointille: this.stor.listeP3.reponse === 'Oui', dep: [p1, d1], contient: [p1] }
    this.stor.listeP1.disable()
    this.stor.listeP3.disable()
    this.stor.listeP2.disable()
    j3pEmpty(this.stor.tabDepAct[0][0])
    const tyu = addDefaultTable(this.stor.tabDepAct[0][0], 1, 2)
    j3pAddContent(tyu[0][0], 'Nom:&nbsp;')
    this.stor.zoneNom = new ZoneStyleMathquill1(tyu[0][1], {
      restric: '()\'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMONPQRSTUVWXYZ',
      lim: 4,
      limite: 4,
      enter: this.okNomDebPerp.bind(this),
      j3pCont: this.container
    })
    const tyu2 = addDefaultTable(this.stor.tabDepAct[0][0], 1, 2)
    j3pAddContent(tyu2[0][0], 'Nom caché:&nbsp;')
    this.stor.listeCache = ListeDeroulante.create(tyu2[0][1], ['Non', 'Oui'], { choix0: true })
    j3pAjouteBouton(this.stor.tabDepAct[0][0], this.okNomDebPerp.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.zoneNom, this.stor.listeCache]
  }

  okNomDebPara () {
    const reponse = this.stor.zoneNom.reponse()
    if (reponse === '') return
    const nom = reponse
    if (this.ceNomEstPris(nom)) return
    this.stor.zoneNom.disable()
    this.stor.listeCache.disable()
    this.stor.newDte.noms = [reponse]
    this.stor.newDte.nomCache = this.stor.listeCache.reponse === 'Oui'
    this.scratch.params.MtgProg.figdep.push(this.stor.newDte)
    this.affichelistedep()
  }

  okNomDebPerp () {
    const reponse = this.stor.zoneNom.reponse()
    if (reponse === '') return
    const nom = reponse
    if (this.ceNomEstPris(nom)) return
    this.stor.zoneNom.disable()
    this.stor.listeCache.disable()
    this.stor.newDte.noms = [reponse]
    this.stor.newDte.nomCache = this.stor.listeCache.reponse === 'Oui'
    this.scratch.params.MtgProg.figdep.push(this.stor.newDte)
    this.affichelistedep()
  }

  okNomAleaSeg () {
    const nom = this.newNomDiese()
    this.stor.zoneNom.disable()
    this.stor.newSeg.ext2 = nom
    this.stor.nomDep.push(nom)
    this.stor.newSeg.noms = ['[' + this.stor.newSeg.ext1 + nom + ']', '[' + nom + this.stor.newSeg.ext1 + ']']
    this.stor.newSeg.contient.push(nom)
    this.scratch.params.MtgProg.figdep.push(this.stor.newSeg)
    this.affichelistedep()
  }

  okNomDebSeg () {
    const reponse = this.stor.zoneNom.reponse()
    if (reponse === '') return
    const nom = reponse
    if (this.stor.nomDep.indexOf(nom) !== -1) return
    this.stor.nomDep.push(nom)
    this.stor.zoneNom.disable()
    this.stor.newSeg.ext2 = nom
    this.stor.newSeg.noms = ['[' + this.stor.newSeg.ext1 + nom + ']', '[' + nom + this.stor.newSeg.ext1 + ']']
    this.stor.newSeg.contient.push(nom)
    this.scratch.params.MtgProg.figdep.push(this.stor.newSeg)
    this.affichelistedep()
  }

  okNomAlea () {
    const nom = this.newNomDiese()
    this.stor.nomDep.push(nom)
    this.stor.newPoint = { type: 'point', noms: [nom] }
    this.stor.zoneNom.disable()
    j3pEmpty(this.stor.tabDepAct[0][0])
    j3pAddContent(this.stor.tabDepAct[0][0], 'Nom aléatoire')
    j3pEmpty(this.stor.aaasssuuup)
    j3pEmpty(this.stor.tabDepAct[1][0])
    const wt = addDefaultTable(this.stor.tabDepAct[1][0], 7, 1)
    j3pAjouteBouton(wt[0][0], this.pointFix.bind(this), { value: 'Point Fixe' })
    j3pAjouteBouton(wt[1][0], this.pointLibre.bind(this), { value: 'Point Libre' })
    if (this.yaob()) j3pAjouteBouton(wt[2][0], this.pointSur.bind(this), { value: 'Point sur objet' })
    if (this.yaob2) j3pAjouteBouton(wt[3][0], this.pointMilieu.bind(this), { value: 'Point milieu' })
    if (this.yaob3()) j3pAjouteBouton(wt[4][0], this.pointInter.bind(this), { value: 'Point d’intersection' })
    if (this.nbPoints() > 1) j3pAjouteBouton(wt[5][0], this.pointSym.bind(this), { value: 'Symétrique / point' })
    if (this.nbPoints() > 1) j3pAjouteBouton(wt[6][0], this.pointRot.bind(this), { value: 'Rotation / point' })
    this.stor.listAnulle = []
  }

  okNomDeb () {
    const reponse = this.stor.zoneNom.reponse()
    if (reponse === '') return
    const nom = reponse
    if (this.stor.nomDep.indexOf(nom) !== -1) return
    j3pEmpty(this.stor.tabDepAct[0][0])
    j3pAddContent(this.stor.tabDepAct[0][0], 'Point ' + nom)
    this.stor.nomDep.push(nom)
    this.stor.newPoint = { type: 'point', noms: [nom] }
    this.stor.zoneNom.disable()
    j3pEmpty(this.stor.aaasssuuup)
    j3pEmpty(this.stor.tabDepAct[1][0])
    const wt = addDefaultTable(this.stor.tabDepAct[1][0], 7, 1)
    j3pAjouteBouton(wt[0][0], this.pointFix.bind(this), { value: 'Point Fixe' })
    j3pAjouteBouton(wt[1][0], this.pointLibre.bind(this), { value: 'Point Libre' })
    if (this.yaob()) j3pAjouteBouton(wt[2][0], this.pointSur.bind(this), { value: 'Point sur objet' })
    if (this.yaob2) j3pAjouteBouton(wt[3][0], this.pointMilieu.bind(this), { value: 'Point milieu' })
    if (this.yaob3()) j3pAjouteBouton(wt[4][0], this.pointInter.bind(this), { value: 'Point d’intersection' })
    if (this.nbPoints() > 1) j3pAjouteBouton(wt[5][0], this.pointSym.bind(this), { value: 'Symétrique / point' })
    if (this.nbPoints() > 1) j3pAjouteBouton(wt[6][0], this.pointRot.bind(this), { value: 'Rotation / point' })
    this.stor.listAnulle = []
  }

  okPlace (tag) {
    this.scratch.params.mtgApplecteur.setApiDoc(this.scratch.params.svgId2)
    const amodif = this.retrouveDep({ nom: 'zone' + tag }, 'objet')
    const c = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'z' + tag + 'c' })
    const p = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'z' + tag + 'p' })
    amodif.c = c
    amodif.p = p
    this.scratch.params.mtgApplecteur.setHidden({ elt: 'z' + tag + 'c' })
    this.scratch.params.mtgApplecteur.setHidden({ elt: 'z' + tag + 'p' })
  }

  okPlaceC (tag) {
    this.okPlace(tag)
    j3pEmpty(this.stor.lesdivs.actDepTitre)
    j3pEmpty(this.stor.lesdivs.actDep)
    this.affichelistedep()
  }

  okPlaceCSeg (tag) {
    this.okPlaceSeg(tag)
    j3pEmpty(this.stor.lesdivs.actDepTitre)
    j3pEmpty(this.stor.lesdivs.actDep)
    this.affichelistedep()
  }

  okPlaceSeg (tag) {
    this.scratch.params.mtgApplecteur.setApiDoc(this.scratch.params.svgId2)
    const amodif = this.retrouveDep({ nom: 'zoneSeg' + tag }, 'objet')
    const c = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'zs' + tag + 'c' })
    const p = this.scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'zs' + tag + 'p' })
    amodif.c = c
    amodif.p = p
    this.scratch.params.mtgApplecteur.setHidden({ elt: 'zs' + tag + 'c' })
    this.scratch.params.mtgApplecteur.setHidden({ elt: 'zs' + tag + 'p' })
  }

  pointFix () {
    this.vireBouts()
    j3pAffiche(this.stor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le point')
    this.scratch.params.mtgApplecteur.getDoc(this.scratch.params.svgId2).defaultCursor = 'crosshair'
    this.scratch._mtgOnDown2 = (ev) => {
      const where = this.scratch.params.leSvg2.getBoundingClientRect()
      const x = ev.clientX - where.x
      const y = ev.clientY - where.y
      this.stor.newPoint.x = x
      this.stor.newPoint.y = y
      this.stor.funcDown = () => {}
      this.stor.newPoint.dep = []
      this.scratch._mtgOnDown2 = () => {}
      this.finishPoint()
    }
  }

  pointSur () {
    this.vireBouts()
    const ll = this.fileMoiLesLignes2(this.aret)
    for (let i = 0; i < this.scratch.params.MtgProg.figdep.length; i++) {
      if (this.scratch.params.MtgProg.figdep[i].type.indexOf('zone') !== -1) ll.push(this.scratch.params.MtgProg.figdep[i].noms[0])
    }
    const ttab = addDefaultTable(this.stor.tabDepAct[1][0], 1, 4)
    this.stor.tabDepAct[1][0].style.color = ''
    j3pAddTxt(ttab[0][0], 'point sur&nbsp;')
    this.stor.listeSur = ListeDeroulante.create(ttab[0][1], ll, this.ldOptions)
    j3pAddTxt(ttab[0][2], '&nbsp;')
    j3pAjouteBouton(ttab[0][3], this.choixSur.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listeSur]
  }

  pointLibre () {
    this.stor.newPoint.type = 'pointLibreAvecNom'
    this.stor.newPoint.dep = []
    this.finishPoint()
  }

  pointSym () {
    this.vireBouts()
    const ttab = addDefaultTable(this.stor.tabDepAct[1][0], 3, 1)
    const ll = this.fileMoiLesPoints(this.aret)
    const tyu = addDefaultTable(ttab[0][0], 1, 2)
    j3pAddContent(tyu[0][0], 'Centre symétrie:&nbsp;')
    this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
    const tyu2 = addDefaultTable(ttab[1][0], 1, 2)
    j3pAddContent(tyu2[0][0], 'symétrique de:&nbsp;')
    this.stor.listeP2 = ListeDeroulante.create(tyu2[0][1], ll, {})
    j3pAjouteBouton(ttab[2][0], this.okPointSym.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2]
  }

  pointRot () {
    this.vireBouts()
    const ttab = addDefaultTable(this.stor.tabDepAct[1][0], 3, 1)
    const ll = this.fileMoiLesPoints(this.aret)
    const tyu = addDefaultTable(ttab[0][0], 1, 2)
    j3pAddContent(tyu[0][0], 'Centre rotation:&nbsp;')
    this.stor.listeP1 = ListeDeroulante.create(tyu[0][1], ll, {})
    const tyu2 = addDefaultTable(ttab[1][0], 1, 2)
    j3pAddContent(tyu2[0][0], 'image de:&nbsp;')
    this.stor.listeP2 = ListeDeroulante.create(tyu2[0][1], ll, {})
    const tyu3 = addDefaultTable(ttab[1][0], 1, 2)
    j3pAddContent(tyu3[0][0], 'angle ( en ° sens direct ):&nbsp;')
    this.stor.zoneAngle = this.stor.zoneNom = new ZoneStyleMathquill1(tyu3[0][1], {
      restric: '-0123456789',
      lim: 4,
      limite: 4,
      j3pCont: this.container
    })
    j3pAjouteBouton(ttab[2][0], this.okPointRot.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listeP1, this.stor.listeP2, this.stor.zoneAngle]
  }

  pointMilieu () {
    this.vireBouts()
    const ll = ['Choisir']
    const max = this.aret || this.scratch.params.MtgProg.figdep.length
    for (let i = 0; i < max; i++) {
      const ffob = this.scratch.scratchMtgReplaceLettres(this.scratch.params.MtgProg.figdep[i])
      if (ffob.type === 'segment' || ffob.type === 'segmentLong') ll.push(ffob.noms[0])
    }
    const ttab = addDefaultTable(this.stor.tabDepAct[1][0], 1, 4)
    this.stor.tabDepAct[1][0].style.color = ''
    j3pAddTxt(ttab[0][0], 'milieu de&nbsp;')
    this.stor.listeSur = ListeDeroulante.create(ttab[0][1], ll, this.ldOptions)
    j3pAddTxt(ttab[0][2], '&nbsp;')
    j3pAjouteBouton(ttab[0][3], this.choixMilieu.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listeSur]
  }

  pointInter () {
    this.vireBouts()
    const ll = this.fileMoiLesLignes2(this.aret)
    const ttab = addDefaultTable(this.stor.tabDepAct[1][0], 1, 4)
    this.stor.tabDepAct[1][0].style.color = ''
    j3pAddContent(ttab[0][0], 'intersection de<br>')
    this.stor.listint1 = ListeDeroulante.create(ttab[0][0], ll, this.ldOptions)
    j3pAddContent(ttab[0][0], '<br>et de<br>')
    this.stor.listint2 = ListeDeroulante.create(ttab[0][0], ll, this.ldOptions)
    j3pAddContent(ttab[0][2], '&nbsp;')
    j3pAjouteBouton(ttab[0][3], this.choixInter.bind(this), { value: 'OK' })
    this.stor.listAnulle = [this.stor.listint1, this.stor.listint2]
  }

  rajoutdefenvrai () {
    const Blockly = this.scratch.Blockly
    const x2 = this.scratch.scratchCreeMenu([], true)
    const xmlDoc2 = $.parseXML(x2)
    let xml2 = xmlDoc2.firstChild
    while (xml2.nodeType !== 1) {
      xml2 = xml2.nextSibling
    }
    const aVire = addDefaultTable(this.container, 1, 1)[0][0]
    for (const property in this.scratch.params.BlocsAAj) {
      function urlImg (img, onlyRel) {
        if (onlyRel) return '/outils/scratch/' + img
        return j3pBaseUrl + 'outils/scratch/' + img
      }

      const nwBlo = this.scratch.params.BlocsAAj[property]
      const obdep = {
        message0: nwBlo.txt,
        args0: [],
        category: Blockly.Categories.motion,
        colour: nwBlo.colour,
        extensions: ['shape_statement']
      }
      const value = []
      let funX = () => { return '' }
      let funY = () => { return '' }
      let funZ = () => { return '' }
      if (nwBlo.nbVar > 0) {
        obdep.args0.push({ type: 'input_value', name: 'X', default: '' })
        funX = (block) => { return Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '') }
        value.push({ nom: 'X', contenu: { valeur: ' ' } })
      }
      if (nwBlo.nbVar > 1) {
        obdep.args0.push({ type: 'input_value', name: 'Y', default: '' })
        funY = (block) => { return Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '') }
        value.push({ nom: 'Y', contenu: { valeur: ' ' } })
      }
      if (nwBlo.nbVar > 2) {
        obdep.args0.push({ type: 'input_value', name: 'Z', default: '' })
        funZ = (block) => { return Blockly.JavaScript.valueToCode(block, 'Z', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '') }
        value.push({ nom: 'Z', contenu: { valeur: ' ' } })
      }
      obdep.message0 = nwBlo.txt
      let yaicone = false
      if (this.scratch.params.creaIconeChoice) {
        obdep.args0.splice(0, 0, { type: 'field_vertical_separator' })
        obdep.args0.splice(0, 0, {
          type: 'field_image',
          src: urlImg(this.scratch.params.creaIconeChoice),
          width: 30,
          height: 30
        })
        yaicone = true
      }
      const xml = $.parseXML(nwBlo.xmlText)
      this.scratch.params.lesBlocPerso.push(nwBlo.nom)
      this.scratch.params.listeDef[nwBlo.nom] = {
        nom: nwBlo.nom,
        nbVar: nwBlo.nbVar,
        xml,
        txt: nwBlo.txt,
        yaicone,
        xmlText: nwBlo.xmlText
      }
      Blockly.Blocks[nwBlo.nom] = {
        init: function () {
          this.jsonInit({
            type: 'make',
            message0: 'Définir ' + nwBlo.nom,
            inputsInline: false,
            nextStatement: null,
            colour: 330,
            tooltip: '',
            helpUrl: ''
          })
        }
      }
      Blockly.JavaScript[nwBlo.nom] = function () {
        return { type: 'make', eventtype: 'make', nom: nwBlo.nom }
      }
      Blockly.Blocks[nwBlo.nom + 'b'] = {
        init: function () {
          this.jsonInit(obdep)
        }
      }
      Blockly.JavaScript[nwBlo.nom + 'b'] = function (block) {
        const retValx = function () {
          return funX(block)
        }
        const retvaly = function () {
          return funY(block)
        }
        const retvalz = function () {
          return funZ(block)
        }
        return { type: 'Mb', nom: nwBlo.nom + 'b', valx: retValx(), valy: retvaly(), valz: retvalz() }
      }

      const side = 'start'
      this.scratch.params.scratchProgDonnees.workspaceDef = Blockly.inject(aVire, {
        comments: true,
        disable: false,
        collapse: false,
        media: j3pBaseUrl + 'externals/blockly/media/',
        readOnly: false,
        rtl: null,
        scrollbars: true,
        toolbox: xml2,
        toolboxPosition: side === 'top' || side === 'start' ? 'start' : 'end',
        horizontalLayout: side === 'top' || side === 'bottom',
        sounds: false,
        zoom: {
          controls: true,
          wheel: true,
          startScale: 0.6,
          maxScale: 2,
          minScale: 0.25,
          scaleSpeed: 1.1
        },
        colours: {
          fieldShadow: 'rgba(100, 100, 100, 0.3)',
          dragShadowOpacity: 0.6
        }
      })
      Blockly.svgResize(this.scratch.params.scratchProgDonnees.workspaceDef)
      // this.scratch.params.scratchProgDonnees.workspaceDef.clear()
      let x = xml.firstChild
      while (x.nodeType !== 1) {
        x = x.nextSibling
      }
      Blockly.Xml.domToWorkspace(x, this.scratch.params.scratchProgDonnees.workspaceDef)
      const ladef = Blockly.JavaScript.workspaceToCode(this.scratch.params.scratchProgDonnees.workspaceDef)
      ladef[0].splice(0, 1)
      this.scratch.params.listeDef[nwBlo.nom].def = ladef[0]
      this.scratch.params.listeDef[nwBlo.nom].value = value
      j3pEmpty(aVire)
    }
    j3pDetruit(aVire)
  }

  refaisMenu () {
    this.scratch.params.scratchProgDonnees.menu = []

    const lib = this.stor.exoEncours.listeBloc
    if (lib.pointLibreAvecNom || lib.pointIntersection || lib.pointSur || lib.pointMilieu || lib.pointLong) {
      this.scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'POINTS',
        coul1: '#3230B8',
        coul2: '#3373CC',
        contenu: []
      })
      if (lib.pointLibreAvecNom) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'pointLibreAvecNom',
          value: [{ nom: 'NOM', contenu: { valeur: 'A' } }]
        })
      }
      if (lib.pointIntersection) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'pointIntersection',
          value: [{ nom: 'NOM', contenu: { valeur: 'A' } }, { nom: 'CONT1', contenu: { valeur: ' ' } }, {
            nom: 'CONT2',
            contenu: { valeur: ' ' }
          }]
        })
      }
      if (lib.pointSur) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'pointSur',
          value: [{ nom: 'NOM', contenu: { valeur: 'A' } }, { nom: 'CONT', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.pointMilieu) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'pointMilieu',
          value: [{ nom: 'NOM', contenu: { valeur: 'I' } }, { nom: 'CONT', contenu: { valeur: 'AB' } }]
        })
      }
      if (lib.pointLong) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'pointLong',
          value: [{ nom: 'EXT1', contenu: { valeur: 'A' } }, { nom: 'LONG', contenu: { valeur: ' ' } }, { nom: 'P1', contenu: { valeur: ' ' } }]
        })
      }
    }

    if (lib.segmentAB || lib.segmentlongA) {
      this.scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'SEGMENTS',
        coul1: '#3230B8',
        coul2: '#3373CC',
        contenu: []
      })
      if (lib.segmentAB) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'segmentAB',
          value: [{ nom: 'EXT1', contenu: { valeur: 'AB' } }]
        })
      }
      if (lib.segmentlongA) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'segmentlongA',
          value: [{ nom: 'EXT1', contenu: { valeur: 'AB' } }, { nom: 'LONG', contenu: { valeur: '3' } }]
        })
      }
    }

    if (lib.droitept || lib.droitepara || lib.droiteperp) {
      this.scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'DROITES',
        coul1: '#3230B8',
        coul2: '#3373CC',
        contenu: []
      })
      if (lib.droitept) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'droitept',
          value: [{ nom: 'POINT1', contenu: { valeur: 'AB' } }]
        })
      }
      if (lib.droitepara) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'droitepara',
          value: [{ nom: 'NOM', contenu: { valeur: '(d1)' } }, {
            nom: 'PARA',
            contenu: { valeur: '(AB)' }
          }, { nom: 'POINT', contenu: { valeur: 'C' } }]
        })
      }
      if (lib.droiteperp) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'droiteperp',
          value: [{ nom: 'NOM', contenu: { valeur: '(d1)' } }, {
            nom: 'PERP',
            contenu: { valeur: '(AB)' }
          }, { nom: 'POINT', contenu: { valeur: 'C' } }]
        })
      }
    }

    if (lib.demidroitept) {
      this.scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'DEMI-DROITES',
        coul1: '#3230B8',
        coul2: '#3373CC',
        contenu: []
      })
      this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
        type: 'demidroitept',
        value: [{ nom: 'ORIGINE', contenu: { valeur: 'AB' } }]
      })
    }

    if (lib.cercleCentrePoint || lib.cercleCentreRayon) {
      this.scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'CERCLES',
        coul1: '#3230B8',
        coul2: '#3373CC',
        contenu: []
      })
      if (lib.cercleCentrePoint) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'cercleCentrePoint',
          value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'POINT', contenu: { valeur: 'A' } }, {
            nom: 'NOM',
            contenu: { valeur: 'C1' }
          }]
        })
      }
      if (lib.cercleCentreRayon) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'cercleCentreRayon',
          value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'RAYON', contenu: { valeur: '3' } }, {
            nom: 'NOM',
            contenu: { valeur: 'C1' }
          }]
        })
      }
    }

    if (lib.arc1PointPasse || lib.arccentreplus || lib.arccentremoins) {
      this.scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'ARC DE CERCLES',
        coul1: '#3230B8',
        coul2: '#3373CC',
        contenu: []
      })
      if (lib.arc1PointPasse) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'arc1PointPasse',
          value: [{ nom: 'POINT', contenu: { valeur: 'M' } }, { nom: 'NOM', contenu: { valeur: 'BC' } }]
        })
      }
      if (lib.arccentreplus) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'arccentreplus',
          value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'NOM', contenu: { valeur: 'AB' } }]
        })
      }
      if (lib.arccentremoins) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'arccentremoins',
          value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'NOM', contenu: { valeur: 'AB' } }]
        })
      }
    }

    if (lib.effacer || lib.renommer || lib.regroupe || lib.ecriSeg || lib.ecriDroite || lib.ecriDDroite || lib.nomAleatoire || lib.pointille) {
      this.scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'UTILITAIRES',
        coul1: '#B5E61D',
        coul2: '#3373CC',
        contenu: []
      })
      if (lib.effacer) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'mtgEffacer',
          value: [{ nom: 'NOM', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.pointille) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'mtgPointille',
          value: [{ nom: 'NOM', contenu: { valeur: '[AB]' } }]
        })
      }
      if (lib.renommer) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'Renommer',
          value: [{ nom: 'NOM', contenu: { valeur: ' ' } }, { nom: 'NOM2', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.regroupe) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'Regroupe',
          value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.ecriSeg) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'ecriSeg',
          value: [{ nom: 'A', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.ecriDroite) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'ecriDroite',
          value: [{ nom: 'A', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.ecriDDroite) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'ecriDDroite',
          value: [{ nom: 'A', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.nomAleatoire) this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'NomAleatoire' })
    }

    if (this.scratch.params.lesBlocPerso.length > 0) {
      this.scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'MES BLOCS',
        coul1: '#D14C7D',
        coul2: '#B8436E',
        contenu: []
      })
      for (let i = 0; i < this.scratch.params.lesBlocPerso.length; i++) {
        this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: this.scratch.params.lesBlocPerso[i] + 'b',
          value: this.scratch.params.listeDef[this.scratch.params.lesBlocPerso[i]].value
        })
      }
    }

    if (this.scratch.params.scratchProgDonnees.menu.length === 0) {
      this.scratch.params.scratchProgDonnees.menu = j3pClone(this.menuChoixMin) // pourquoi cloner ?
    }

    const x = this.scratch.scratchCreeMenu(this.scratch.params.scratchProgDonnees.menu, true)
    const xmlDoc = $.parseXML(x)
    let xml = xmlDoc.firstChild
    while (xml.nodeType !== 1) {
      xml = xml.nextSibling
    }
    this.scratch.params.scratchProgDonnees.workspace.updateToolbox(xml)
  }

  refaisProg () {
    let xml
    if (Array.isArray(this.scratch.params.scratchProgDonnees.progdeb[0])) {
      xml = this.scratch.scratchCreeProgCo2(this.scratch.params.scratchProgDonnees.progdeb[0], { kedeb: true })
      xml += this.scratch.scratchCreeProgCo2(this.scratch.params.scratchProgDonnees.progdeb[1], { kefin: true })
    } else {
      xml = this.scratch.scratchCreeProgCo2(this.scratch.params.scratchProgDonnees.progdeb, {})
    }
    const xmlDoc = $.parseXML(xml)
    let x = xmlDoc.firstChild
    while (x.nodeType !== 1) {
      x = x.nextSibling
    }
    this.scratch.params.scratchProgDonnees.workspace.clear()

    this.scratch.Blockly.Xml.domToWorkspace(x, this.scratch.params.scratchProgDonnees.workspace)
  }

  retrouveDep (ob, quoi) {
    for (let i = 0; i < this.scratch.params.MtgProg.figdep.length; i++) {
      let ok = true
      if (ob.nom) ok = ok && this.scratch.params.MtgProg.figdep[i].noms[0] === ob.nom
      if (ob.type) ok = ok && this.scratch.params.MtgProg.figdep[i].type === ob.type
      if (ok) {
        if (quoi === 'objet') return this.scratch.params.MtgProg.figdep[i]
        if (quoi === 'num') return i
      }
    }
  }

  retrouveListeDep (tab) {
    const aret = j3pClone(tab)
    for (let i = tab[0] + 1; i < this.scratch.params.MtgProg.figdep.length; i++) {
      for (const a of aret) {
        this.scratch.params.MtgProg.figdep[i].dep = this.scratch.params.MtgProg.figdep[i].dep || []
        if (this.scratch.params.MtgProg.figdep[i].dep.includes(this.scratch.params.MtgProg.figdep[a].noms[0])) {
          aret.push(i)
          break
        }
      }
    }
    return aret
  }

  supDef (i) {
    const avire = this.scratch.params.lesBlocPerso[i]
    Reflect.deleteProperty(this.scratch.params.listeDef, this.scratch.params.lesBlocPerso[i])
    this.scratch.params.lesBlocPerso.splice(i, 1)

    const cont = this.scratch.params.scratchProgDonnees.menu[this.scratch.params.scratchProgDonnees.menu.length - 2].contenu
    for (let i = 0; i < cont.length; i++) {
      if (cont[i].type === avire + 'b') {
        cont.splice(i, 1)
        break
      }
    }
    if (cont.length === 0) {
      this.scratch.params.scratchProgDonnees.menu.splice(this.scratch.params.scratchProgDonnees.menu.length - 2, 1)
    }
    // modif menu
    const x2 = this.scratch.scratchCreeMenu(this.scratch.params.scratchProgDonnees.menu, true)
    const xmlDoc2 = $.parseXML(x2)
    let xml2 = xmlDoc2.firstChild
    while (xml2.nodeType !== 1) {
      xml2 = xml2.nextSibling
    }
    this.scratch.params.scratchProgDonnees.workspace.updateToolbox(xml2)
    this.affichelisteBlocs()
    this.refaisMenu()
  }

  supprimeDep (num) {
    const nomAvire = this.scratch.params.MtgProg.figdep[num].noms[0]
    for (let i = 0; i < this.scratch.params.MtgProg.figdep.length; i++) {
      if (this.scratch.params.MtgProg.figdep[i].contient) {
        for (let j = this.scratch.params.MtgProg.figdep[i].contient.length - 1; j > -1; j--) {
          if (this.scratch.params.MtgProg.figdep[i].contient[j] === nomAvire) {
            this.scratch.params.MtgProg.figdep[i].contient.splice(j, 1)
          }
        }
      }
    }
    const avire = this.retrouveListeDep([num])
    for (let i = avire.length - 1; i > -1; i--) {
      this.scratch.params.MtgProg.figdep.splice(avire[i], 1)
    }
    this.stor.nomDep = this.fileMoiLesPoints().splice(0, 1)
    this.genereOK(false)
    this.affichelistedep()
  }

  cacheDep (num) {
    this.scratch.params.MtgProg.figdep[num].debCache = !this.scratch.params.MtgProg.figdep[num].debCache
    this.affichelistedep()
  }

  modiPointDep (num) {
    this.aret = num
    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAddContent(this.stor.lesdivs.actDepTitre, '<b><u>Modification d’un point</u></b><br><br>')
    this.stor.tabDepAct = addDefaultTable(this.stor.lesdivs.actDep, 8, 1)
    this.stor.tabDepAct0 = addDefaultTable(this.stor.tabDepAct[0][0], 1, 5)
    const nom = this.scratch.params.MtgProg.figdep[num].noms[0]
    j3pEmpty(this.stor.tabDepAct[0][0])
    j3pAddContent(this.stor.tabDepAct[0][0], 'Point ' + nom)
    this.stor.newPoint = { type: 'point', noms: [nom], remplace: true }
    this.scratch.params.MtgProg.figdep[num] = this.stor.newPoint
    const wt = addDefaultTable(this.stor.tabDepAct[1][0], 7, 1)
    j3pAjouteBouton(wt[0][0], this.pointFix.bind(this), { value: 'Point Fixe' })
    j3pAjouteBouton(wt[1][0], this.pointLibre.bind(this), { value: 'Point Libre' })
    if (this.yaob(num)) j3pAjouteBouton(wt[2][0], this.pointSur.bind(this), { value: 'Point sur objet' })
    if (this.yaob2(num)) j3pAjouteBouton(wt[3][0], this.pointMilieu.bind(this), { value: 'Point milieu' })
    if (this.yaob3(num)) j3pAjouteBouton(wt[4][0], this.pointInter.bind(this), { value: 'Point d’intersection' })
    if (this.nbPoints(num) > 1) j3pAjouteBouton(wt[5][0], this.pointSym.bind(this), { value: 'Symétrique / point' })
    if (this.nbPoints(num) > 1) j3pAjouteBouton(wt[6][0], this.pointRot.bind(this), { value: 'Rotation / point' })
    this.stor.listAnulle = []
  }

  modiiefDepS (num) {
    const nn = this.scratch.params.MtgProg.figdep[num].noms[0].replace('zoneSeg', '')
    this.scratch.params.mtgApplecteur.setApiDoc(this.scratch.params.svgId2)
    this.scratch.params.mtgApplecteur.setVisible({ elt: 'zs' + nn + 'c' })
    this.scratch.params.mtgApplecteur.setVisible({ elt: 'zs' + nn + 'p' })

    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAffiche(this.stor.lesdivs.modifDep, null, '<i>Clic <b>OK</b> quand <br>la zone segment est bien placée</i><br><br>')
    this.stor.lesdivs.modifDep.style.color = '#2573b4'
    this.stor.boutOKDep = j3pAjouteBouton(this.stor.lesdivs.modifDep, this.okPlaceCSeg.bind(this, nn), { value: 'OK' })
  }

  modiiefDep (num) {
    const nn = this.scratch.params.MtgProg.figdep[num].noms[0].replace('zone', '')
    this.scratch.params.mtgApplecteur.setApiDoc(this.scratch.params.svgId2)
    this.scratch.params.mtgApplecteur.setVisible({ elt: 'z' + nn + 'c' })
    this.scratch.params.mtgApplecteur.setVisible({ elt: 'z' + nn + 'p' })

    j3pEmpty(this.stor.lesdivs.modifDep)
    j3pAffiche(this.stor.lesdivs.modifDep, null, '<i>Clic <b>OK</b> quand <br>la zone est bien placée</i><br><br>')
    this.stor.lesdivs.modifDep.style.color = '#2573b4'
    this.stor.boutOKDep = j3pAjouteBouton(this.stor.lesdivs.modifDep, this.okPlaceC.bind(this, nn), { value: 'OK' })
  }

  verifPasChange () {
    const lxml = this.scratch.Blockly.Xml.workspaceToDom(this.scratch.params.scratchProgDonnees.workspace)
    const xmlText = this.scratch.Blockly.Xml.domToText(lxml)
    if (xmlText !== this.stor.progSouv) this.genereOK(false)
    this.stor.progSouv = xmlText
  }

  vireBouts () {
    j3pEmpty(this.stor.tabDepAct[1][0])
    j3pEmpty(this.stor.tabDepAct[2][0])
    j3pEmpty(this.stor.tabDepAct[3][0])
    j3pEmpty(this.stor.tabDepAct[4][0])
    j3pEmpty(this.stor.tabDepAct[5][0])
  }

  yaob (num) {
    const max = num || this.scratch.params.MtgProg.figdep.length
    for (let i = 0; i < max; i++) {
      if (this.scratch.params.MtgProg.figdep[i].type === 'segment' || this.scratch.params.MtgProg.figdep[i].type === 'segmentLong' || this.scratch.params.MtgProg.figdep[i].type === 'zoneSeg') return true
    }
    return false
  }

  yaob2 (num) {
    const max = num || this.scratch.params.MtgProg.figdep.length
    for (let i = 0; i < max; i++) {
      if (this.scratch.params.MtgProg.figdep[i].type === 'segment' || this.scratch.params.MtgProg.figdep[i].type === 'segmentLong') return true
    }
    return false
  }

  yaob3 (num) {
    let cmpt = 0
    const max = num || this.scratch.params.MtgProg.figdep.length
    for (let i = 0; i < max; i++) {
      const ff = this.scratch.params.MtgProg.figdep[i].type
      if (ff.indexOf('segment') !== -1 || ff.indexOf('droite') !== -1 || ff.indexOf('arc') !== -1 || ff.indexOf('cercle') !== -1) cmpt++
    }
    return cmpt > 1
  }

  nbPoints (num) {
    const max = num || this.scratch.params.MtgProg.figdep.length
    let cmpt = 0
    for (let i = 0; i < max; i++) {
      if (this.scratch.params.MtgProg.figdep[i].type === 'point') cmpt++
      if (this.scratch.params.MtgProg.figdep[i].type === 'pointLibreAvecNom') cmpt++
      if (this.scratch.params.MtgProg.figdep[i].type === 'pointMilieu') cmpt++
      if (this.scratch.params.MtgProg.figdep[i].type === 'pointSym') cmpt++
      if (this.scratch.params.MtgProg.figdep[i].type === 'pointRot') cmpt++
      if (this.scratch.params.MtgProg.figdep[i].type === 'pointIntersection') cmpt++
      if (this.scratch.params.MtgProg.figdep[i].type === 'segmentlongA') cmpt++
    }
    return cmpt
  }

  nbLignes (num) {
    const max = num || this.scratch.params.MtgProg.figdep.length
    let cmpt = 0
    for (let i = 0; i < max; i++) {
      if (this.scratch.params.MtgProg.figdep[i].type.indexOf('segment') !== -1) cmpt++
      if (this.scratch.params.MtgProg.figdep[i].type.indexOf('droite') !== -1) cmpt++
    }
    return cmpt
  }

  nbSegments () {
    let cmpt = 0
    for (let i = 0; i < this.scratch.params.MtgProg.figdep.length; i++) {
      if (this.scratch.params.MtgProg.figdep[i].type.indexOf('segment') !== -1) cmpt++
    }
    return cmpt
  }

  fileMoiLesPoints (lim) {
    const ll = ['---']
    const sup = lim || this.scratch.params.MtgProg.figdep.length
    for (let i = 0; i < sup; i++) {
      const ffob = this.scratch.scratchMtgReplaceLettres(this.scratch.params.MtgProg.figdep[i])
      if (ffob.type === 'point') ll.push(ffob.noms[0])
      if (ffob.type === 'pointSur') ll.push(ffob.noms[0])
      if (ffob.type === 'pointLibreAvecNom') ll.push(ffob.noms[0])
      if (ffob.type === 'pointMilieu') ll.push(ffob.noms[0])
      if (ffob.type === 'pointSym') ll.push(ffob.noms[0])
      if (ffob.type === 'pointRot') ll.push(ffob.noms[0])
      if (ffob.type === 'pointIntersection') ll.push(ffob.noms[0])
      if (ffob.type === 'segmentlongA') ll.push(ffob.ext2)
    }
    return ll
  }

  fileMoiLesLignes (num) {
    const ll = ['---']
    const max = num || this.scratch.params.MtgProg.figdep.length
    for (let i = 0; i < max; i++) {
      const ffob = this.scratch.scratchMtgReplaceLettres(this.scratch.params.MtgProg.figdep[i])
      if (ffob.type.indexOf('segment') !== -1) ll.push(ffob.noms[0])
      if (ffob.type.indexOf('droite') !== -1) ll.push(ffob.noms[0])
    }
    return ll
  }

  fileMoiLesLignes2 (num) {
    const ll = ['---']
    const max = num || this.scratch.params.MtgProg.figdep.length
    for (let i = 0; i < max; i++) {
      const ffob = this.scratch.scratchMtgReplaceLettres(this.scratch.params.MtgProg.figdep[i])
      if (ffob.type.indexOf('segment') !== -1) ll.push(ffob.noms[0])
      if (ffob.type.indexOf('droite') !== -1) ll.push(ffob.noms[0])
      if (ffob.type.indexOf('cercle') !== -1) ll.push(ffob.noms[0])
    }
    return ll
  }

  ceNomEstPris (n) {
    for (let i = 0; i < this.scratch.params.MtgProg.figdep.length; i++) {
      const ffob = this.scratch.scratchMtgReplaceLettres(this.scratch.params.MtgProg.figdep[i])
      if (ffob.noms.indexOf(n) !== -1) return true
    }
    return false
  }
}

export default EditorProgrammeMtg
