import $ from 'jquery'

import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pDetruit, j3pGetRandomInt, j3pModale, j3pNotify } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'

import arc3pImg from './images/arc3p.png'
import angleImg from './images/angle.png'
import pointilleImg from './images/pointille.png'
import aligneImg from './images/aligne.png'
import pointLong from './images/pointlong.png'
import diametreImg from './images/diametre.png'
import arcmilImg from './images/arcmil.png'
import mediatriceImg from './images/mediatrice.png'
import triangleImg from './images/triangle.png'
import bras2Img from './images/bras2.png'
import cercleptImg from './images/cerclept.png'
import cerclerayImg from './images/cercleray.png'
import cosmoImg from './images/cosmo.svg'
import demidroiteptImg from './images/demidroitept.png'
import dessinImg from './images/dessin.png'
import dessinleveImg from './images/dessinleve.png'
import drapeauImg from './images/drapeau.gif'
import drapeauFlg from './images/drapeauflg.jpg'
import drapeauFld from './images/drapeaufld.jpg'
import drapeauFlh from './images/drapeauflh.jpg'
import drapeauFlb from './images/drapeauflb.jpg'
import drapeauFla from './images/drapeaufla.jpg'
import drapeauFlesp from './images/drapeauesp.jpg'
import drapeaucoImg from './images/drapeauco.gif'
import droiteparaImg from './images/droitepara.png'
import droiteperpImg from './images/droiteperp.png'
import droiteptImg from './images/droitept.png'
import effacerImg from './images/effacer.png'
import fourcheImg from './images/fourche.gif'
import fuseeImg from './images/fusee.svg'
import pinceImg from './images/pince.png'
import caseImg from './images/petitScratch.png'
import planetrougeImg from './images/planetrouge.svg'
import pointinterImg from './images/pointinter.png'
import pointlibreImg from './images/pointlibre.png'
import pointmilieuImg from './images/pointmilieu.png'
import pointsurImg from './images/pointsur.png'
import segmentImg from './images/segment.png'
import segmentlongImg from './images/segmentlong.png'
import caseJauneImg from './images/caseJaune.png'
import caseBleueImg from './images/caseBleue.png'
import caseRougeImg from './images/caseRouge.png'
import caseVerteImg from './images/caseVerte.png'
import caseOrangeImg from './images/caseOrange.png'
import caseRoseImg from './images/caseRose.png'
import caseVioletteImg from './images/caseViolette.png'
import caseNoireImg from './images/caseNoire.png'
import { addDefaultTable } from 'src/legacy/themes/table'
import { InitSlider } from 'src/legacy/outils/slider/slider'

/**
 * Ajoute des propriétés / méthodes à l’objet Blockly pour le cas général
 * @param {Scratch} scratch
 */
export default function ajoutsBlockly (Blockly, scratch) {
  Blockly.JavaScript = new Blockly.Generator('JavaScript')

  // une fct utilisée par Blockly.Variables.createVariable
  function scratchAutorise (m) {
    if (scratch.params.isEditing) return true
    if (!scratch.params.leWork) scratch.params.leWork = scratch.params.scratchProgDonnees.workspace
    const mmm = scratch.params.leWork.getAllVariables()
    if (mmm.length >= scratch.params.me.donneesSection.nombre_de_variables) {
      if (!souv) {
        const vv = (scratch.params.me.donneesSection.nombre_de_variables > 1)
          ? 'variables'
          : 'variable'
        scratch.afficheModale('Dans cet exercice, <BR> tu ne peux pas créer plus de ' + scratch.params.me.donneesSection.nombre_de_variables + ' ' + vv + ' ! <br> (<i>Clic droit sur une variable pour la supprimer</i>i)', 'Message')
        return false
      }
    }
    souv = false
    if (scratch.params.NomsVariableAutorise.length === 0) return true
    if (scratch.params.NomsVariableAutorise.indexOf(m) !== -1) { return true }
    scratch.afficheModale('Ce nom n’est pas autorisé !', 'Message')
    return false
  }

  // une fct utilisée par Blockly.Variables.createVariable
  function scratchAutoriseType (m) {
    if (m === 'list') {
      scratch.afficheModale('Dans cet exercice, <BR> tu ne peux pas créer de liste !', 'Message')
      return false
    }

    return true
  }

  // une fct utilisée par Blockly.ContextMenu.blockDuplicateOption
  function scratchAutoriseContext (m) {
    let i
    if (m === null) return true
    const EstDisa = scratch.getDisabledDansMenu(m.type)
    if (EstDisa === true) return false
    let nbl = m.getNextBlock()
    if (scratchAutoriseContext(nbl) === false) return false
    nbl = m.getChildren()
    for (i = 0; i < nbl.length; i++) {
      if (scratchAutoriseContext(nbl[i]) === false) return false
    }
    return true
  }

  let souv = false

  // //////////////////////////////////////////////////
  // Le code qui était dans tomblok.redef.blockly.js
  // //////////////////////////////////////////////////
  Blockly.WorkspaceSvg.prototype.preloadAudio_ = function () {}

  Blockly.Generator.prototype.valueToCode = function (a, b, c) {
    // isNaN(c) && goog.asserts.fail('Expecting valid order from block "%s".', a.type)
    let d = a?.getInputTargetBlock(b)
    if (!d) return 'erreur block vide'
    b = this.blockToCode(d)
    if (b === '') return ''
    // goog.asserts.assertArray(b, 'Expecting tuple from value block "%s".', d.type)
    a = b[0]
    b = b[1]
    // isNaN(b) && goog.asserts.fail('Expecting valid order from value block "%s".', d.type)
    if (!a) return ''
    d = false
    let e = Math.floor(c)
    const f = Math.floor(b)

    if (e <= f && (e != f || (e != 0 && e != 99))) {
      for (d = true, e = 0; e < this.ORDER_OVERRIDES.length; e++) {
        if (this.ORDER_OVERRIDES[e][0] == c && this.ORDER_OVERRIDES[e][1] == b) {
          d = false
          break
        }
      }
    }
    if (d) a = '(' + a + ')'
    return a
  }

  Blockly.Constants.Data.DELETE_OPTION_CALLBACK_FACTORY = function (a, b) {
    return function () {
      if (!scratch.params.AutoriseSupprimeVariable) {
        scratch.afficheModale('Dans cet exercice , tu ne peux pas supprimer de variable.', 'Message')
        return
      }
      const c = a.workspace; const d = a.getField(b).getVariable()
      c.deleteVariableById(d.getId())
    }
  }

  Blockly.Constants.Data.RENAME_OPTION_CALLBACK_FACTORY = function (a, b) {
    return function () {
      if (!scratch.params.AutoriseRenomeVariable) {
        scratch.afficheModale('Dans cet exercice , tu ne peux pas renommer de variable.', 'Message')
        return
      }
      const c = a.workspace; const d = a.getField(b).getVariable()
      Blockly.Variables.renameVariable(c, d)
    }
  }

  Blockly.Workspace.prototype.variableIndexOf = function (a) {
    for (const [index, variable] of this.variableList.entries()) {
      if (Blockly.Names.equals(variable, a)) return index
    }
    return -1
  }

  // Blockly.Workspace.prototype.getWidth = function () { return 0 }

  Blockly.Workspace.prototype.newBlock = function (a, b) { return new Blockly.Block(this, a, b) }

  Blockly.Variables.renameVariable = function (a, b, c) {
    if (!scratch.params.AutoriseRenomeVariable) {
      scratch.afficheModale('Dans cet exercice , tu ne peux pas renommer de variable.', 'Message')
      return
    }
    souv = true
    const d = b.type
    if (d == Blockly.BROADCAST_MESSAGE_VARIABLE_TYPE) {
      console.warn('Unexpected attempt to rename a broadcast message with id: ' + b.getId() + ' and name: ' + b.name)
      return
    }
    let e, f
    if (d == Blockly.LIST_VARIABLE_TYPE) {
      e = Blockly.Msg.RENAME_LIST_TITLE
      f = Blockly.Msg.RENAME_LIST_MODAL_TITLE
    } else {
      e = Blockly.Msg.RENAME_VARIABLE_TITLE
      f = Blockly.Msg.RENAME_VARIABLE_MODAL_TITLE
    }
    const g = Blockly.Variables.nameValidator_.bind(null, d)
    e = e.replace('%1', b.name)
    let h = b.name
    b.isCloud && b.name.indexOf(Blockly.Variables.CLOUD_PREFIX) == 0 && (h = h.substring(Blockly.Variables.CLOUD_PREFIX.length))
    const fn = function (d, e) {
      if (!scratchAutorise(d)) return
      b.isCloud && d.length > 0 && d.indexOf(Blockly.Variables.CLOUD_PREFIX) == 0 && (d = d.substring(Blockly.Variables.CLOUD_PREFIX.length))
      e = e || []
      const arg3 = b.isLocal ? [] : e
      e = g(d, a, arg3, b.isCloud)
      if (e) {
        a.renameVariableById(b.getId(), e)
        if (c) c(d)
      } else {
        if (c) c(null)
      }
    }
    Blockly.prompt(e, h, fn, f, d)
  }

  // @todo fix Deprecated: Use Blockly.Variables.createVariableButtonHandler(..)
  Blockly.Variables.createVariable = function (a, b, c) {
    if (!scratchAutoriseType(c)) {
      return
    }
    let d, e
    if (c == Blockly.BROADCAST_MESSAGE_VARIABLE_TYPE) {
      d = Blockly.Msg.NEW_BROADCAST_MESSAGE_TITLE
      e = Blockly.Msg.BROADCAST_MODAL_TITLE
    } else {
      if (c == Blockly.LIST_VARIABLE_TYPE) {
        d = Blockly.Msg.NEW_LIST_TITLE
        e = Blockly.Msg.LIST_MODAL_TITLE
      } else {
        c = c || ''
        d = Blockly.Msg.NEW_VARIABLE_TITLE
        e = Blockly.Msg.VARIABLE_MODAL_TITLE
      }
    }
    const f = Blockly.Variables.nameValidator_.bind(null, c)
    const fn = function (d, e, k) {
      if (!scratchAutorise(d)) return
      k = k || {}
      let g = k.scope === 'local'
      k = k.isCloud || !1
      e = e || []
      const arg3 = g ? [] : e
      d = f(d, a, arg3, k, b)
      if (d) {
        let h
        a.getPotentialVariableMap() && c && (h = Blockly.Variables.realizePotentialVar(d, c, a, !1))
        h || (h = a.createVariable(d, c, null, g, k))
        g = a.isFlyout ? a : a.getFlyout()
        h = h.getId()
        g.setCheckboxState && g.setCheckboxState(h, !0)
        b && b(h)
      } else {
        b && b(null)
      }
    }
    Blockly.prompt(d, '', fn, e, c)
  }

  Blockly.JavaScript.blockToCode = function (a) {
    if (!a) return ''
    if (a.disabled) return this.blockToCode(a.getNextBlock())
    let b = this[a.type]
    // goog.asserts.assertFunction(b, 'Language "%s" does not know how to generate code for block type "%s".', this.name_, a.type)

    try {
      b = b.call(a, a)
    } catch (error) {
      console.error(a.type + ' fait planter blocktocode', error)
    }
    /*
    if (goog.isArray(b)) {
      // FIXME y’a un pb sur cette ligne, ce qui suit la virgule n’est pas exécuté
      // to fixme , j’en sais rien , je l’ai laissé là , j’ai pas cherché à comprendre

      // eslint-disable-next-line no-sequences
      return goog.asserts.assert(a.outputConnection, 'Expecting string from statement block "%s".', a.type), [this.scrub_(a, b[0]), b[1]]
    }
    */
    if ((typeof b === 'object' || typeof b === 'function') && (b !== null)) { b.id = a.id }
    b = [b]

    let ep = a.nextConnection && a.nextConnection.targetBlock()
    ep = Blockly.JavaScript.blockToCode(ep)

    return b.concat(ep)
  }

  Blockly.JavaScript.statementToCode = function (a, b) {
    const c = a.getInputTargetBlock(b)
    return this.blockToCode(c)
  }

  Blockly.Generator.prototype.workspaceToCode = function (a) {
    if (!a) {
      console.warn('No workspace specified in workspaceToCode call.  Guessing.')
      a = Blockly.getMainWorkspace()
    }
    const b = []
    a.variableList = []
    this.init(a)
    a = a.getTopBlocks(!0)
    for (const block of a) {
      b.push(this.blockToCode(block))
    }
    // b=b.join("\n");
    // b=this.finish(b);
    // b=b.replace(/^\s+\n/,"");
    // b=b.replace(/\n\s+$/,"\n");
    // return b=b.replace(/[ \t]+\n/g,"\n")
    return b
  }

  Blockly.ContextMenu.blockDuplicateOption = function (a, b) {
    if (scratchAutoriseContext(a)) {
      return {
        text: Blockly.Msg.DUPLICATE,
        enabled: true,
        callback: Blockly.scratchBlocksUtils.duplicateAndDragCallback(a, b)
      }
    }
    // sinon ça retourne undefined
  }

  Blockly.ContextMenu.blockCommentOption = function (a) {
    const b = {
      enabled: true
    //! goog.userAgent.IE
    }
    if (a.comment) {
      b.text = Blockly.Msg.REMOVE_COMMENT
      b.callback = function () { a.setCommentText(null) }
    } else {
      b.text = Blockly.Msg.ADD_COMMENT
      b.callback = function () { a.setCommentText(''); a.comment.focus() }
    }
    return b
  }

  Blockly.VariableMap.prototype.deleteVariableById = function (a) {
    if (!scratch.params.AutoriseSupprimeVariable) {
      scratch.afficheModale('Dans cet exercice , tu ne peux pas supprimer de variable.', 'Message')
      return
    }
    const b = this.getVariableById(a)
    if (b) {
      let c = b.name
      const d = this.getVariableUsesById(a)
      for (const e of d) {
        if (e.type == Blockly.PROCEDURES_DEFINITION_BLOCK_TYPE || e.type == 'procedures_defreturn') {
          a = e.getFieldValue('NAME'); c = Blockly.Msg.CANNOT_DELETE_VARIABLE_PROCEDURE.replace('%1', c).replace('%2', a); Blockly.alert(c); return
        }
      }
      const f = this
      if (d.length > 1) {
        Blockly.Msg.DELETE_VARIABLE_CONFIRMATION.replace('%1', String(d.length)).replace('%2', c)
        Blockly.confirm(c, function (a) { a && f.deleteVariableInternal_(b, d) })
      } else {
        f.deleteVariableInternal_(b, d)
      }
    } else {
      console.warn('Can’t delete non-existent variable: ' + a)
    }
  }

  // //////////////////////////////////////////////////
  // Le code qui était dans tomblok.js
  // //////////////////////////////////////////////////
  Blockly.Procedures.externalProcedureDefCallback = () => {
    const loption = { choix0: true, dansModale: true, onChange: scratch.scratchPropIcineCrea.bind(scratch) }
    if (!scratch.params.isEditing) {
      scratch.params.ModaleCrea = j3pModale({ titre: 'Création d’un nouveau bloc' })
      scratch.params.ModaleCrea.style.top = '0px'
      scratch.params.ModaleCrea.style.left = '0px'
    } else {
      loption.j3pCont = scratch.params.j3pCont
      loption.decalage = 80
      scratch.params.ModaleCrea = scratch.params.ModaleCreaisEditing
      scratch.params.ModaleCrea.style.border = '2px solid black'
      scratch.params.ModaleCrea.style.background = '#aaa'
    }

    const tabCont = addDefaultTable(scratch.params.ModaleCrea, 1, 2)
    const tab = addDefaultTable(tabCont[0][0], 6, 1)
    scratch.params.tabIcineCrea = tabCont[0][1]
    scratch.params.tabIcineCrea2 = tabCont[0][0]
    scratch.params.tabIcineCrea.style.border = '3px blue solid'
    const tab5 = addDefaultTable(scratch.params.tabIcineCrea, 2, 1)
    tab5[0][0].style.textAlign = 'center'
    scratch.params.tabIcineCrea.style.verticalAlign = 'top'
    scratch.params.creaDivClic = tab5[1][0]
    const lll = ['Choisir', 'Mathgraph', 'scratch', 'bras']
    scratch.params.listeCrea = ListeDeroulante.create(tab5[0][0], lll, loption)
    scratch.params.tabIcineCrea.style.display = 'none'
    const tab1 = addDefaultTable(tab[1][0], 1, 6)
    const elem = $('.croix')[0]
    j3pDetruit(elem)
    j3pAddContent(tab1[0][0], 'Nombre de variables:&nbsp;')
    scratch.params.nbVarCrea = 0
    scratch.params.nbVarCreaDiv = tab1[0][1]
    tab1[0][1].style.color = '#FF0000'
    j3pAddContent(scratch.params.nbVarCreaDiv, String(scratch.params.nbVarCrea))
    j3pAddContent(tab1[0][2], '&nbsp;')
    j3pAddContent(tab1[0][4], '&nbsp;')
    j3pAjouteBouton(tab1[0][3], scratch.faisPlus.bind(scratch), { value: '+' })
    j3pAjouteBouton(tab1[0][5], scratch.faisMoins.bind(scratch), { value: '-' })
    const tab0 = addDefaultTable(tab[0][0], 1, 2)
    j3pAddContent(tab0[0][0], 'Nom du bloc:&nbsp;')
    scratch.params.zoneNomBloc = j3pAddElt(tab0[0][1], 'input', '', { value: 'Bloc perso', maxLength: 40, minLength: 1, size: 40 })
    const tab2 = addDefaultTable(tab[2][0], 1, 2)
    j3pAddContent(tab2[0][0], 'Text:&nbsp;')
    scratch.params.divzoneTxtCreAef = tab2[0][1]
    scratch.params.divzoneTxtCre = addDefaultTable(tab2[0][1], 1, 7)
    scratch.params.zoneTxtCre = []
    scratch.params.zoneTxtCre[0] = j3pAddElt(scratch.params.divzoneTxtCre[0][0], 'input', '', { value: 'Texte à afficher', maxLength: 40, minLength: 1, size: 40 })
    const tab22 = addDefaultTable(tab[3][0], 1, 3)
    j3pAddContent(tab22[0][0], 'Icône:&nbsp;')
    j3pAjouteBouton(tab22[0][1], scratch.scratchChoixIcinePerso.bind(scratch), { value: 'Ajouter' })
    scratch.params.dd1 = tab22[0][1]
    scratch.params.dd2 = tab22[0][2]

    const tab12 = addDefaultTable(tab[4][0], 1, 4)
    j3pAddContent(tab12[0][0], '<u>Couleur</u>:&nbsp;')
    scratch.params.coolCrea = j3pAddElt(tab12[0][1], 'div')
    scratch.params.coolCrea.style.border = '2px black solid'
    scratch.params.coolCrea.style.borderRadius = '4px'
    scratch.params.coolCrea.style.background = '#5ffa32'
    j3pAddContent(scratch.params.coolCrea, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')
    j3pAddContent(tab12[0][2], '&nbsp;&nbsp;&nbsp;')
    scratch.params.valColor = 100
    scratch.params.ccurso = InitSlider(tab12[0][3], 100, 350, scratch.scratchColorCrea.bind(scratch), scratch.params.mtgApplecteur)
    const tab3 = addDefaultTable(tab[5][0], 1, 3)
    tab3[0][1].style.width = '100%'
    j3pAjouteBouton(tab3[0][0], scratch.scratchValideCrea.bind(scratch), { value: 'créer' })
    j3pAjouteBouton(tab3[0][2], scratch.scratchAnnuleCrea.bind(scratch), { value: 'Annuler' })
  }

  Blockly.HSV_SATURATION = 0.9
  Blockly.HSV_VALUE = 0.9
  // marche pas car Blockly.mainWorkspace.options n’existe pas encore
  // Blockly.mainWorkspace.options.pathToMedia = j3pBaseUrl + '/externals/blockly/media/'

  // bloks petit scratch ( case )
  Blockly.Blocks.caseAvancer = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Avancer',
        args0: [{
          type: 'field_image',
          src: caseImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.caseAvancer = () => {
    const code = () => {
      let lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
      let ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
      switch (scratch.params.scratchProgDonnees.Lutins[0].angle) {
        case 0 :
          scratch.params.scratchProgDonnees.Lutins[0].cox += 50
          lx += 1
          break
        case 90 :
          scratch.params.scratchProgDonnees.Lutins[0].coy += 50
          ly += 1
          break
        case 180 :
          scratch.params.scratchProgDonnees.Lutins[0].cox -= 50
          lx -= 1
          break
        case 270 :
          scratch.params.scratchProgDonnees.Lutins[0].coy -= 50
          ly -= 1
          break
      }
      if (lx > 5 || lx < 0) {
        return { result: 'prob', quoi: 'Scratch ne peut pas sortir de la carte !' }
      }
      if (ly > 5 || ly < 0) {
        return { result: 'prob', quoi: 'Scratch ne peut pas sortir de la carte !' }
      }
      if (scratch.params.scratchProgDonnees.carteCases[lx][ly] === 'Noire') {
        return { result: 'prob', quoi: 'Scratch ne peut pas marcher sur une case Noire !' }
      }
      scratch.scratchPetitScratch()

      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'caseAvancer' }
  }
  Blockly.Blocks.caseReculer = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Reculer',
        args0: [{
          type: 'field_image',
          src: caseImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.caseReculer = () => {
    const code = () => {
      let lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
      let ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
      switch (scratch.params.scratchProgDonnees.Lutins[0].angle) {
        case 0 :
          scratch.params.scratchProgDonnees.Lutins[0].cox -= 50
          lx -= 1
          break
        case 90 :
          scratch.params.scratchProgDonnees.Lutins[0].coy -= 50
          ly -= 1
          break
        case 180 :
          scratch.params.scratchProgDonnees.Lutins[0].cox += 50
          lx += 1
          break
        case 270 :
          scratch.params.scratchProgDonnees.Lutins[0].coy += 50
          ly += 1
          break
      }
      if (lx > 5 || lx < 0) {
        return { result: 'prob', quoi: 'Scratch ne peut pas sortir de la carte !' }
      }
      if (ly > 5 || ly < 0) {
        return { result: 'prob', quoi: 'Scratch ne peut pas sortir de la carte !' }
      }
      if (scratch.params.scratchProgDonnees.carteCases[lx][ly] === 'Noire') {
        return { result: 'prob', quoi: 'Scratch ne peut pas marcher sur une case Noire !' }
      }
      scratch.scratchPetitScratch()

      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'caseReculer' }
  }
  Blockly.Blocks.caseTournerDroite = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Tourner %3',
        args0: [{
          type: 'field_image',
          src: caseImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-right.svg',
          width: 24,
          height: 24
        }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.caseTournerDroite = () => {
    const code = () => {
      scratch.params.scratchProgDonnees.Lutins[0].angle += 90
      if (scratch.params.scratchProgDonnees.Lutins[0].angle === 360) scratch.params.scratchProgDonnees.Lutins[0].angle = 0
      scratch.scratchPetitScratch()

      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'caseTournerDroite' }
  }
  Blockly.Blocks.caseTournerGauche = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Tourner %3',
        args0: [{
          type: 'field_image',
          src: caseImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-left.svg',
          width: 24,
          height: 24
        }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.caseTournerGauche = () => {
    const code = () => {
      scratch.params.scratchProgDonnees.Lutins[0].angle -= 90
      if (scratch.params.scratchProgDonnees.Lutins[0].angle === -90) scratch.params.scratchProgDonnees.Lutins[0].angle = 270
      scratch.scratchPetitScratch()

      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'caseTournerDroite' }
  }
  Blockly.Blocks.caseIsJaune = {
    init: function () {
      this.jsonInit({
        message0: 'La case de Scratch est %1',
        args0: [{
          type: 'field_image',
          src: caseJauneImg,
          width: 30,
          height: 30
        }],
        tooltip: 'Renvoie VRAI si la case sous Scratch est jaune',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.caseIsJaune = () => {
    const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
    const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
    if (scratch.params.scratchProgDonnees.carteCases[lx][ly] === 'Jaune') {
      return 'vrai'
    }
    return 'faux'
  }
  Blockly.Blocks.caseIsBleue = {
    init: function () {
      this.jsonInit({
        message0: 'La case de Scratch est %1',
        args0: [{
          type: 'field_image',
          src: caseBleueImg,
          width: 30,
          height: 30
        }],
        tooltip: 'Renvoie VRAI si la case sous Scratch est bleue',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.caseIsBleue = () => {
    const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
    const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
    if (scratch.params.scratchProgDonnees.carteCases[lx][ly] === 'Bleue') {
      return 'vrai'
    }
    return 'faux'
  }
  Blockly.Blocks.caseIsRouge = {
    init: function () {
      this.jsonInit({
        message0: 'La case de Scratch est %1',
        args0: [{
          type: 'field_image',
          src: caseRougeImg,
          width: 30,
          height: 30
        }],
        tooltip: 'Renvoie VRAI si la case sous Scratch est rouge',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.caseIsRouge = () => {
    const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
    const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
    if (scratch.params.scratchProgDonnees.carteCases[lx][ly] === 'Rouge') {
      return 'vrai'
    }
    return 'faux'
  }
  Blockly.Blocks.caseIsVerte = {
    init: function () {
      this.jsonInit({
        message0: 'La case de Scratch est %1',
        args0: [{
          type: 'field_image',
          src: caseVerteImg,
          width: 30,
          height: 30
        }],
        tooltip: 'Renvoie VRAI si la case sous Scratch est verte',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.caseIsVerte = () => {
    const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
    const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
    if (scratch.params.scratchProgDonnees.carteCases[lx][ly] === 'Verte') {
      return 'vrai'
    }
    return 'faux'
  }
  Blockly.Blocks.caseIsOrange = {
    init: function () {
      this.jsonInit({
        message0: 'La case de Scratch est %1',
        args0: [{
          type: 'field_image',
          src: caseOrangeImg,
          width: 30,
          height: 30
        }],
        tooltip: 'Renvoie VRAI si la case sous Scratch est orange',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.caseIsOrange = () => {
    const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
    const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
    if (scratch.params.scratchProgDonnees.carteCases[lx][ly] === 'Orange') {
      return 'vrai'
    }
    return 'faux'
  }
  Blockly.Blocks.caseIsRose = {
    init: function () {
      this.jsonInit({
        message0: 'La case de Scratch est %1',
        args0: [{
          type: 'field_image',
          src: caseRoseImg,
          width: 30,
          height: 30
        }],
        tooltip: 'Renvoie VRAI si la case sous Scratch est rose',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.caseIsRose = () => {
    const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
    const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
    if (scratch.params.scratchProgDonnees.carteCases[lx][ly] === 'Rose') {
      return 'vrai'
    }
    return 'faux'
  }
  Blockly.Blocks.caseIsViolette = {
    init: function () {
      this.jsonInit({
        message0: 'La case de Scratch est %1',
        args0: [{
          type: 'field_image',
          src: caseVioletteImg,
          width: 30,
          height: 30
        }],
        tooltip: 'Renvoie VRAI si la case sous Scratch est violette',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.caseIsViolette = () => {
    const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
    const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
    if (scratch.params.scratchProgDonnees.carteCases[lx][ly] === 'Violette') {
      return 'vrai'
    }
    return 'faux'
  }
  Blockly.Blocks.caseIsColore = {
    init: function () {
      this.jsonInit({
        message0: 'La case de Scratch est colorée',
        args0: [],
        tooltip: 'Renvoie VRAI si Scratch est sur une case colorée',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.caseIsColore = () => {
    const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
    const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
    if (scratch.params.scratchProgDonnees.carteCases[lx][ly] !== 'vide') {
      return 'vrai'
    }
    return 'faux'
  }
  Blockly.Blocks.caseIsNotColore = {
    init: function () {
      this.jsonInit({
        message0: 'La case de Scratch est blanche',
        args0: [],
        tooltip: 'Renvoie VRAI si Scratch est sur une case blanche',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.caseIsNotColore = () => {
    const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
    const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
    if (scratch.params.scratchProgDonnees.carteCases[lx][ly] === 'vide') {
      return 'vrai'
    }
    return 'faux'
  }
  Blockly.Blocks.caseDevantNoire = {
    init: function () {
      this.jsonInit({
        message0: 'La case DEVANT Scratch est %1',
        args0: [{
          type: 'field_image',
          src: caseNoireImg,
          width: 30,
          height: 30
        }],
        tooltip: 'Renvoie VRAI si la case devant Scratch est noire',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.caseDevantNoire = () => {
    let lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
    let ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
    switch (scratch.params.scratchProgDonnees.Lutins[0].angle) {
      case 0 :
        lx += 1
        break
      case 90 :
        ly += 1
        break
      case 180 :
        lx -= 1
        break
      case 270 :
        ly -= 1
        break
    }
    if (scratch.params.scratchProgDonnees.carteCases[lx][ly] === 'Noire') {
      return 'vrai'
    }
    return 'faux'
  }
  Blockly.Blocks.caseColoreJaune = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Colorie en %3',
        args0: [{
          type: 'field_image',
          src: caseImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: caseJauneImg,
          width: 30,
          height: 30
        }],
        category: Blockly.Categories.motion,
        colour: '310',
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.JavaScript.caseColoreJaune = () => {
    const code = () => {
      const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
      const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
      if (scratch.params.scratchProgDonnees.carteCases[lx][ly] !== 'vide') {
        return { result: 'prob', quoi: 'Cette case est déjà coloriée !' }
      }
      scratch.scratchPetitScratchAjouteColoeur('jaune')
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'caseColoreJaune' }
  }
  Blockly.Blocks.caseColoreBleue = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Colorie en %3',
        args0: [{
          type: 'field_image',
          src: caseImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: caseBleueImg,
          width: 30,
          height: 30
        }],
        category: Blockly.Categories.motion,
        colour: '310',
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.JavaScript.caseColoreBleue = () => {
    const code = () => {
      const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
      const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
      if (scratch.params.scratchProgDonnees.carteCases[lx][ly] !== 'vide') {
        return { result: 'prob', quoi: 'Cette case est déjà coloriée !' }
      }
      scratch.scratchPetitScratchAjouteColoeur('bleue')
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'caseColoreBleue' }
  }
  Blockly.Blocks.caseColoreRouge = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Colorie en %3',
        args0: [{
          type: 'field_image',
          src: caseImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: caseRougeImg,
          width: 30,
          height: 30
        }],
        category: Blockly.Categories.motion,
        colour: '310',
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.JavaScript.caseColoreRouge = () => {
    const code = () => {
      const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
      const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
      if (scratch.params.scratchProgDonnees.carteCases[lx][ly] !== 'vide') {
        return { result: 'prob', quoi: 'Cette case est déjà coloriée !' }
      }
      scratch.scratchPetitScratchAjouteColoeur('rouge')
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'caseColoreRouge' }
  }

  Blockly.Blocks.caseColoreVerte = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Colorie en %3',
        args0: [{
          type: 'field_image',
          src: caseImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: caseVerteImg,
          width: 30,
          height: 30
        }],
        category: Blockly.Categories.motion,
        colour: '310',
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.JavaScript.caseColoreVerte = () => {
    const code = () => {
      const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
      const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
      if (scratch.params.scratchProgDonnees.carteCases[lx][ly] !== 'vide') {
        return { result: 'prob', quoi: 'Cette case est déjà coloriée !' }
      }
      scratch.scratchPetitScratchAjouteColoeur('verte')
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'caseColoreVerte' }
  }
  Blockly.Blocks.caseColoreOrange = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Colorie en %3',
        args0: [{
          type: 'field_image',
          src: caseImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: caseOrangeImg,
          width: 30,
          height: 30
        }],
        category: Blockly.Categories.motion,
        colour: '310',
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.JavaScript.caseColoreOrange = () => {
    const code = () => {
      const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
      const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
      if (scratch.params.scratchProgDonnees.carteCases[lx][ly] !== 'vide') {
        return { result: 'prob', quoi: 'Cette case est déjà coloriée !' }
      }
      scratch.scratchPetitScratchAjouteColoeur('orange')
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'caseColoreOrange' }
  }
  Blockly.Blocks.caseColoreRose = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Colorie en %3',
        args0: [{
          type: 'field_image',
          src: caseImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: caseRoseImg,
          width: 30,
          height: 30
        }],
        category: Blockly.Categories.motion,
        colour: '310',
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.JavaScript.caseColoreRose = () => {
    const code = () => {
      const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
      const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
      if (scratch.params.scratchProgDonnees.carteCases[lx][ly] !== 'vide') {
        return { result: 'prob', quoi: 'Cette case est déjà coloriée !' }
      }
      scratch.scratchPetitScratchAjouteColoeur('rose')
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'caseColoreRose' }
  }
  Blockly.Blocks.caseColoreViolette = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Colorie en %3',
        args0: [{
          type: 'field_image',
          src: caseImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: caseVioletteImg,
          width: 30,
          height: 30
        }],
        category: Blockly.Categories.motion,
        colour: '310',
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.JavaScript.caseColoreViolette = () => {
    const code = () => {
      const lx = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].cox / 50)
      const ly = Math.floor(scratch.params.scratchProgDonnees.Lutins[0].coy / 50)
      if (scratch.params.scratchProgDonnees.carteCases[lx][ly] !== 'vide') {
        return { result: 'prob', quoi: 'Cette case est déjà coloriée !' }
      }
      scratch.scratchPetitScratchAjouteColoeur('violette')
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'caseColoreViolette' }
  }

  // bloks mathgraphs
  Blockly.Blocks.NomAleatoire = {
    init: function () {
      this.jsonInit({
        message0: '?? %1 Nom aléatoire',
        args0: [{ type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'output_number']
      })
    }
  }
  Blockly.JavaScript.NomAleatoire = function () {
    return scratch.params.scratchProgDonnees.nomsAlea.pop()
  }
  Blockly.Blocks.Regroupe = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2',
        args0: [{ type: 'input_value', name: 'A', default: 'A' }, { type: 'input_value', name: 'B', default: 'B' }],
        category: Blockly.Categories.motion,
        colour: 100,
        extensions: ['output_number']
      })
    }
  }
  Blockly.JavaScript.Regroupe = function (block) {
    const ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'A', Blockly.JavaScript.ORDER_ATOMIC))
    const ext2 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'B', Blockly.JavaScript.ORDER_ATOMIC))
    return ext1 + ext2
  }
  Blockly.Blocks.Renommer = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Renommer %3 en %4',
        args0: [{
          type: 'field_image',
          src: pointlibreImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: 'A' }, { type: 'input_value', name: 'NOM2', default: 'A' }],
        category: Blockly.Categories.motion,
        colour: 100,
        extensions: ['shape_statement']
      })
    }
  }

  Blockly.Blocks.ecriSeg = {
    init: function () {
      this.jsonInit({
        message0: '[%1]',
        args0: [{ type: 'input_value', name: 'A', default: 'A' }],
        category: Blockly.Categories.motion,
        colour: 100,
        extensions: ['output_number']
      })
    }
  }
  Blockly.JavaScript.ecriSeg = function (block) {
    const ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'A', Blockly.JavaScript.ORDER_ATOMIC))
    return '[' + ext1 + ']'
  }
  Blockly.Blocks.ecriDroite = {
    init: function () {
      this.jsonInit({
        message0: '(%1)',
        args0: [{ type: 'input_value', name: 'A', default: 'A' }],
        category: Blockly.Categories.motion,
        colour: 100,
        extensions: ['output_number']
      })
    }
  }
  Blockly.JavaScript.ecriDroite = function (block) {
    const ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'A', Blockly.JavaScript.ORDER_ATOMIC))
    return '(' + ext1 + ')'
  }
  Blockly.Blocks.ecriDDroite = {
    init: function () {
      this.jsonInit({
        message0: '[%1)',
        args0: [{ type: 'input_value', name: 'A', default: 'A' }],
        category: Blockly.Categories.motion,
        colour: 100,
        extensions: ['output_number']
      })
    }
  }
  Blockly.JavaScript.ecriDDroite = function (block) {
    const ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'A', Blockly.JavaScript.ORDER_ATOMIC))
    return '[' + ext1 + ')'
  }
  // EFFACER
  Blockly.Blocks.mtgEffacer = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Effacer %3',
        args0: [{
          type: 'field_image',
          src: effacerImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: 'A' }],
        category: Blockly.Categories.motion,
        colour: 100,
        extensions: ['shape_statement']
      })
    }
  }

  // POINTILLE
  Blockly.Blocks.mtgPointille = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Mettre %3 en pointillés',
        args0: [{
          type: 'field_image',
          src: pointilleImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: '[AB]' }],
        category: Blockly.Categories.motion,
        colour: 100,
        extensions: ['shape_statement']
      })
    }
  }

  // POINTS
  Blockly.Blocks.pointLibreAvecNom = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Point %3',
        args0: [{
          type: 'field_image',
          src: pointlibreImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: 'A' }],
        category: Blockly.Categories.motion,
        colour: 230,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.pointIntersection = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Point %5, intersection de %3 et de %4',
        args0: [{
          type: 'field_image',
          src: pointinterImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'CONT1', default: 'A' }, { type: 'input_value', name: 'CONT2', default: 'A' }, { type: 'input_value', name: 'NOM', default: 'A' }],
        category: Blockly.Categories.motion,
        colour: 230,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.pointSur = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Point %3 sur %4',
        args0: [{
          type: 'field_image',
          src: pointsurImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: 'A' }, { type: 'input_value', name: 'CONT', default: 'A' }],
        category: Blockly.Categories.motion,
        colour: 230,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.pointMilieu = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Milieu %3 du segment [%4]',
        args0: [{
          type: 'field_image',
          src: pointmilieuImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: 'I' }, { type: 'input_value', name: 'CONT', default: '[AB]' }],
        category: Blockly.Categories.motion,
        colour: 230,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.pointAligne = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Point %3 aligné avec %4 et %5',
        args0: [{
          type: 'field_image',
          src: aligneImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: 'C' }, { type: 'input_value', name: 'P1', default: 'A' }, { type: 'input_value', name: 'P2', default: 'B' }],
        category: Blockly.Categories.motion,
        colour: 230,
        extensions: ['shape_statement']
      })
    }
  }

  Blockly.Blocks.pointLong = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Point %3 à %4 unités du point %5 .',
        args0: [{
          type: 'field_image',
          src: pointLong,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'EXT1', default: 'A' }, { type: 'input_value', name: 'LONG', default: ' ' }, { type: 'input_value', name: 'P1', default: ' ' }],
        category: Blockly.Categories.motion,
        colour: 230,
        extensions: ['shape_statement']
      })
    }
  }

  // SEGMENT
  Blockly.Blocks.segmentAB = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Segment [%3]',
        args0: [{
          type: 'field_image',
          src: segmentImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'EXT1', default: 'AB' }],
        category: Blockly.Categories.motion,
        colour: 360,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.segmentlongA = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Segment [%3] de longueur %4 unités',
        args0: [{
          type: 'field_image',
          src: segmentlongImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'EXT1', default: 'A' }, { type: 'input_value', name: 'LONG', default: '3' }],
        category: Blockly.Categories.motion,
        colour: 360,
        extensions: ['shape_statement']
      })
    }
  }

  // DROITES
  Blockly.Blocks.droitept = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Droite (%3)',
        args0: [{
          type: 'field_image',
          src: droiteptImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'POINT1', default: 'AB' }],
        category: Blockly.Categories.motion,
        colour: 280,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.droitepara = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Droite %3 parallèle à %4 passant par %5',
        args0: [{
          type: 'field_image',
          src: droiteparaImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: '(AB)' }, { type: 'input_value', name: 'PARA', default: '(AB)' }, { type: 'input_value', name: 'POINT', default: 'C' }],
        category: Blockly.Categories.motion,
        colour: 280,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.droiteperp = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Droite %3  perpendiculaire à %4 passant par %5',
        args0: [{
          type: 'field_image',
          src: droiteperpImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: '(d1)' }, { type: 'input_value', name: 'PERP', default: '(AB)' }, { type: 'input_value', name: 'POINT', default: 'C' }],
        category: Blockly.Categories.motion,
        colour: 280,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.mediatrice = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Médiatrice %3 du segment  [%4]',
        args0: [{
          type: 'field_image',
          src: mediatriceImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: '(d1)' }, { type: 'input_value', name: 'CONT', default: 'AB' }],
        category: Blockly.Categories.motion,
        colour: 280,
        extensions: ['shape_statement']
      })
    }
  }

  // DEMI DROITES
  Blockly.Blocks.demidroitept = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Demi-droite [%3)',
        args0: [{
          type: 'field_image',
          src: demidroiteptImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'ORIGINE', default: 'AB' }],
        category: Blockly.Categories.motion,
        colour: 330,
        extensions: ['shape_statement']
      })
    }
  }

  // CERCLE
  Blockly.Blocks.cercleCentrePoint = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Cercle %5 de centre %3 passant par le point %4',
        args0: [{
          type: 'field_image',
          src: cercleptImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'CENTRE', default: 'O' }, { type: 'input_value', name: 'POINT', default: 'A' }, { type: 'input_value', name: 'NOM', default: 'C1' }],
        category: Blockly.Categories.motion,
        colour: 20,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.cercleCentreRayon = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Cercle %5 de centre %3 et de rayon %4 unités',
        args0: [{
          type: 'field_image',
          src: cerclerayImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'CENTRE', default: 'O' }, { type: 'input_value', name: 'RAYON', default: '3' }, { type: 'input_value', name: 'NOM', default: 'C1' }],
        category: Blockly.Categories.motion,
        colour: 20,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.cercleDiametre = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Cercle %3 de diamètre [%4]',
        args0: [{
          type: 'field_image',
          src: diametreImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: 'C1' }, { type: 'input_value', name: 'DIAM', default: 'AB' }],
        category: Blockly.Categories.motion,
        colour: 20,
        extensions: ['shape_statement']
      })
    }
  }

  // ARC DE CERCLES
  Blockly.Blocks.arc1PointPasse = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Arc de cercle %4 passant par %3',
        args0: [{
          type: 'field_image',
          src: arc3pImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'POINT', default: 'A' }, { type: 'input_value', name: 'NOM', default: 'C1' }],
        category: Blockly.Categories.motion,
        colour: 30,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.arccentreplus = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 %3 Arc de cercle %5 de centre %4',
        args0: [{
          type: 'field_image',
          src: arcmilImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-left.svg',
          width: 24,
          height: 24
        }, { type: 'input_value', name: 'CENTRE', default: 'A' }, { type: 'input_value', name: 'NOM', default: 'C1' }],
        category: Blockly.Categories.motion,
        colour: 30,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.arccentremoins = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 %3 Arc de cercle %5 de centre %4',
        args0: [{
          type: 'field_image',
          src: arcmilImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-right.svg',
          width: 24,
          height: 24
        }, { type: 'input_value', name: 'CENTRE', default: 'A' }, { type: 'input_value', name: 'NOM', default: 'C1' }],
        category: Blockly.Categories.motion,
        colour: 30,
        extensions: ['shape_statement']
      })
    }
  }

  // ANGLES
  Blockly.Blocks.angleplus = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 %5 Angle %3 de mesure %4 ° ',
        args0: [{
          type: 'field_image',
          src: angleImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: 'ABC' }, { type: 'input_value', name: 'MESURE', default: '45' }, {
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-left.svg',
          width: 24,
          height: 24
        }],
        category: Blockly.Categories.motion,
        colour: 330,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.anglemoins = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 %5 Angle %3 de mesure %4 ° ',
        args0: [{
          type: 'field_image',
          src: angleImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: 'ABC' }, { type: 'input_value', name: 'MESURE', default: '45' }, {
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-right.svg',
          width: 24,
          height: 24
        }],
        category: Blockly.Categories.motion,
        colour: 330,
        extensions: ['shape_statement']
      })
    }
  }

  // TRIANGLES
  Blockly.Blocks.triangleQuelconque = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Triangle %3 ',
        args0: [{
          type: 'field_image',
          src: triangleImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: 'ABC' }],
        category: Blockly.Categories.motion,
        colour: 230,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.Blocks.triangleEquilateral = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 Triangle équilatéral %3 ',
        args0: [{
          type: 'field_image',
          src: triangleImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, { type: 'input_value', name: 'NOM', default: 'ABC' }],
        category: Blockly.Categories.motion,
        colour: 230,
        extensions: ['shape_statement']
      })
    }
  }

  // EVENEMENTS
  // DRAPO
  Blockly.Blocks.Drapo = {
    init: function () {
      this.appendDummyInput()
        .appendField('quand')
        .appendField(new Blockly.FieldImage(drapeauImg, 40, 20, '-'))
        .appendField('pressé')
      this.setNextStatement(true, null)
      this.setColour(47)
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }
  Blockly.JavaScript.Drapo = () => {
    return { type: 'event', eventtype: 'drapeau', nom: 'Drapeau' }
  }
  // DRAPOCO
  Blockly.Blocks.Drapoco = {
    init: function () {
      this.appendDummyInput()
        .appendField('quand')
        .appendField(new Blockly.FieldImage(drapeaucoImg, 50, 20, '-'))
        .appendField('pressé')
      this.setNextStatement(true, null)
      this.setColour(33)
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }
  Blockly.JavaScript.Drapoco = () => {
    return { type: 'event', eventtype: 'drapeauco', nom: 'Drapeauco' }
  }
  // FLECHEG
  Blockly.Blocks.FLECHEG = {
    init: function () {
      this.appendDummyInput()
        .appendField('quand')
        .appendField(new Blockly.FieldImage(drapeauFlg, 40, 20, '-'))
        .appendField('appuyé')
      this.setNextStatement(true, null)
      this.setColour(47)
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }
  Blockly.JavaScript.FLECHEG = () => {
    return { type: 'event', eventtype: '◄', nom: '◄' }
  }
  // FLECHED
  Blockly.Blocks.FLECHED = {
    init: function () {
      this.appendDummyInput()
        .appendField('quand')
        .appendField(new Blockly.FieldImage(drapeauFld, 40, 20, '-'))
        .appendField('appuyé')
      this.setNextStatement(true, null)
      this.setColour(47)
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }
  Blockly.JavaScript.FLECHED = () => {
    return { type: 'event', eventtype: '►', nom: '►' }
  }
  // FLECHEB
  Blockly.Blocks.FLECHEB = {
    init: function () {
      this.appendDummyInput()
        .appendField('quand')
        .appendField(new Blockly.FieldImage(drapeauFlb, 40, 20, '-'))
        .appendField('appuyé')
      this.setNextStatement(true, null)
      this.setColour(47)
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }
  Blockly.JavaScript.FLECHEB = () => {
    return { type: 'event', eventtype: '▼', nom: '▼' }
  }
  // FLECHEH
  Blockly.Blocks.FLECHEH = {
    init: function () {
      this.appendDummyInput()
        .appendField('quand')
        .appendField(new Blockly.FieldImage(drapeauFlh, 40, 20, '-'))
        .appendField('appuyé')
      this.setNextStatement(true, null)
      this.setColour(47)
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }
  Blockly.JavaScript.FLECHEH = () => {
    return { type: 'event', eventtype: '▲', nom: '▲' }
  }
  // FLECHE_ a
  Blockly.Blocks.FLECHEA = {
    init: function () {
      this.appendDummyInput()
        .appendField('quand')
        .appendField(new Blockly.FieldImage(drapeauFla, 40, 20, '-'))
        .appendField('appuyé')
      this.setNextStatement(true, null)
      this.setColour(47)
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }
  Blockly.JavaScript.FLECHEA = () => {
    return { type: 'event', eventtype: 'ª', nom: 'a' }
  }
  // FLECHE_ espace
  Blockly.Blocks.FLECHEESP = {
    init: function () {
      this.appendDummyInput()
        .appendField('quand')
        .appendField(new Blockly.FieldImage(drapeauFlesp, 40, 20, '-'))
        .appendField('appuyé')
      this.setNextStatement(true, null)
      this.setColour(47)
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }
  Blockly.JavaScript.FLECHEESP = () => {
    return { type: 'event', eventtype: '▂', nom: '▂' }
  }
  // Event souris
  Blockly.Blocks.EVENTSou = {
    init: function () {
      this.appendDummyInput()
        .appendField('quand ce Lutin est cliqué')
      this.setNextStatement(true, null)
      this.setColour(47)
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }
  Blockly.JavaScript.EVENTSou = () => {
    return { type: 'event', eventtype: 'sou', nom: 'sou' }
  }
  // REPETE
  Blockly.JavaScript.control_repeat = (block) => {
    const statementsName = Blockly.JavaScript.statementToCode(block, 'SUBSTACK')
    return { type: 'boucle', repete: block, boucle: statementsName, nom: 'Repete', id: block.id }
  }
  Blockly.Msg.CONTROL_FOREVER = 'Répéter indéfiniment'
  Blockly.Msg.CONTROL_REPEAT = 'Répéter %1 fois'
  Blockly.Msg.CONTROL_REPEATUNTIL = 'Répéter jusqu’à %1'
  Blockly.JavaScript.control_forever = (block) => {
    const statementsName = Blockly.JavaScript.statementToCode(block, 'SUBSTACK')
    return { type: 'boucleF', boucle: statementsName, nom: 'RepeteF' }
  }
  Blockly.JavaScript.control_repeat_until = (block) => {
    const statementsName = Blockly.JavaScript.statementToCode(block, 'SUBSTACK')
    return { type: 'boucleU', repete: block, boucle: statementsName, nom: 'RepeteU', id: block.id }
  }
  Blockly.JavaScript.control_stop = (block) => {
    return { type: 'control_stop', nom: 'control_stop' }
  }
  // OPERATEURS
  // somme
  Blockly.JavaScript.operator_add = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'NUM1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'NUM2', Blockly.JavaScript.ORDER_ATOMIC)

    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }
    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return '(' + valueNUM1 + ') + (' + valueNUM2 + ')'
    }
    const f = parseFloat(valueNUM1) + parseFloat(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(parseFloat(valueNUM1) + parseFloat(valueNUM2))
  }
  Blockly.Blocks.operator_add2 = { init: function () { this.jsonInit({ message0: Blockly.Msg.OPERATORS_ADD, args0: [{ type: 'input_value', name: 'NUM1' }, { type: 'field_number', name: 'NUM2' }], category: Blockly.Categories.operators, extensions: ['colours_operators', 'output_number'] }) } }
  Blockly.JavaScript.operator_add2 = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'NUM1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = block.getFieldValue('NUM2')

    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }

    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return '(' + valueNUM1 + ') + (' + valueNUM2 + ')'
    }
    const f = parseFloat(valueNUM1) + parseFloat(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(parseFloat(valueNUM1) + parseFloat(valueNUM2))
  }
  Blockly.Blocks.operator_reste = {
    init: function () {
      this.jsonInit({ message0: 'reste de %1 / %2', args0: [{ type: 'input_value', name: 'NUM1' }, { type: 'input_value', name: 'NUM2' }], category: Blockly.Categories.operators, extensions: ['colours_operators', 'output_number'] })
    }
  }
  Blockly.JavaScript.operator_reste = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'NUM1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'NUM2', Blockly.JavaScript.ORDER_ATOMIC)

    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }

    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return 'mod(' + valueNUM1 + ' , ' + valueNUM2 + ')'
    }
    const f = parseFloat(valueNUM1) % parseFloat(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(f)
  }
  Blockly.Blocks.operator_quotient = {
    init: function () {
      this.jsonInit({ message0: 'quotient de %1 / %2', args0: [{ type: 'input_value', name: 'NUM1' }, { type: 'input_value', name: 'NUM2' }], category: Blockly.Categories.operators, extensions: ['colours_operators', 'output_number'] })
    }
  }
  Blockly.JavaScript.operator_quotient = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'NUM1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'NUM2', Blockly.JavaScript.ORDER_ATOMIC)

    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }

    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return 'ent(' + valueNUM1 + ') / (' + valueNUM2 + ')'
    }
    const f = Math.floor(parseFloat(valueNUM1) / parseFloat(valueNUM2))
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(f)
  }
  // difference
  Blockly.JavaScript.operator_subtract = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'NUM1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'NUM2', Blockly.JavaScript.ORDER_ATOMIC)
    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if (valueNUM1 === 'erreur block vide' || valueNUM2 === 'erreur block vide') {
      return 'erreur block vide'
    }

    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return '(' + valueNUM1 + ') - (' + valueNUM2 + ')'
    }
    const f = parseFloat(valueNUM1) - parseFloat(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(parseFloat(valueNUM1) - parseFloat(valueNUM2))
  }
  Blockly.Blocks.operator_subtract2 = { init: function () { this.jsonInit({ message0: Blockly.Msg.OPERATORS_SUBTRACT, args0: [{ type: 'input_value', name: 'NUM1' }, { type: 'field_number', name: 'NUM2' }], category: Blockly.Categories.operators, extensions: ['colours_operators', 'output_number'] }) } }
  Blockly.JavaScript.operator_subtract2 = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'NUM1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = block.getFieldValue('NUM2')

    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }

    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return '(' + valueNUM1 + ') - (' + valueNUM2 + ')'
    }
    const f = parseFloat(valueNUM1) - parseFloat(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(parseFloat(valueNUM1) - parseFloat(valueNUM2))
  }
  Blockly.Blocks.operator_subtract3 = { init: function () { this.jsonInit({ message0: Blockly.Msg.OPERATORS_SUBTRACT, args0: [{ type: 'field_number', name: 'NUM1' }, { type: 'input_value', name: 'NUM2' }], category: Blockly.Categories.operators, extensions: ['colours_operators', 'output_number'] }) } }
  Blockly.JavaScript.operator_subtract3 = (block) => {
    let valueNUM1 = block.getFieldValue('NUM1')
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'NUM2', Blockly.JavaScript.ORDER_ATOMIC)

    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }

    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return '(' + valueNUM1 + ') - (' + valueNUM2 + ')'
    }
    const f = parseFloat(valueNUM1) - parseFloat(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(parseFloat(valueNUM1) - parseFloat(valueNUM2))
  }

  // produit
  Blockly.JavaScript.operator_multiply = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'NUM1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'NUM2', Blockly.JavaScript.ORDER_ATOMIC)
    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if (valueNUM1 === 'erreur block vide' || valueNUM2 === 'erreur block vide') {
      return 'erreur block vide'
    }
    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return '(' + valueNUM1 + ') * (' + valueNUM2 + ')'
    }
    const f = parseFloat(valueNUM1) * parseFloat(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(parseFloat(valueNUM1) * parseFloat(valueNUM2))
  }
  Blockly.Blocks.operator_multiply2 = { init: function () { this.jsonInit({ message0: Blockly.Msg.OPERATORS_MULTIPLY, args0: [{ type: 'input_value', name: 'NUM1' }, { type: 'field_number', name: 'NUM2' }], category: Blockly.Categories.operators, extensions: ['colours_operators', 'output_number'] }) } }
  Blockly.JavaScript.operator_multiply2 = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'NUM1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = block.getFieldValue('NUM2')

    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }

    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return '(' + valueNUM1 + ') * (' + valueNUM2 + ')'
    }
    const f = parseFloat(valueNUM1) * parseFloat(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(parseFloat(valueNUM1) * parseFloat(valueNUM2))
  }

  // quotient
  Blockly.JavaScript.operator_divide = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'NUM1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'NUM2', Blockly.JavaScript.ORDER_ATOMIC)
    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if (valueNUM1 === 'erreur block vide' || valueNUM2 === 'erreur block vide') {
      return 'erreur block vide'
    }
    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return '(' + valueNUM1 + ') / (' + valueNUM2 + ')'
    }
    const f = parseFloat(valueNUM1) / parseFloat(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(parseFloat(valueNUM1) / parseFloat(valueNUM2))
  }
  Blockly.Blocks.operator_divide2 = { init: function () { this.jsonInit({ message0: Blockly.Msg.OPERATORS_DIVIDE, args0: [{ type: 'input_value', name: 'NUM1' }, { type: 'field_number', name: 'NUM2' }], category: Blockly.Categories.operators, extensions: ['colours_operators', 'output_number'] }) } }
  Blockly.JavaScript.operator_divide2 = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'NUM1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = block.getFieldValue('NUM2')

    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }

    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return '(' + valueNUM1 + ') / (' + valueNUM2 + ')'
    }
    const f = parseFloat(valueNUM1) * parseFloat(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(parseFloat(valueNUM1) * parseFloat(valueNUM2))
  }
  Blockly.Blocks.operator_divide3 = { init: function () { this.jsonInit({ message0: Blockly.Msg.OPERATORS_DIVIDE, args0: [{ type: 'field_number', name: 'NUM1' }, { type: 'input_value', name: 'NUM2' }], category: Blockly.Categories.operators, extensions: ['colours_operators', 'output_number'] }) } }
  Blockly.JavaScript.operator_divide3 = (block) => {
    let valueNUM1 = block.getFieldValue('NUM1')
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'NUM2', Blockly.JavaScript.ORDER_ATOMIC)

    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }

    if (valueNUM1.indexOf('x@ù') !== -1 || valueNUM2.indexOf('x@ù') !== -1) {
      return '(' + valueNUM1 + ') / (' + valueNUM2 + ')'
    }
    const f = parseFloat(valueNUM1) * parseFloat(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM1)
    scratch.params.scratchProgDonnees.Valeursutilisees.push(valueNUM2)
    scratch.params.scratchProgDonnees.Valeursautorisees.push(f + '')
    return String(parseFloat(valueNUM1) * parseFloat(valueNUM2))
  }

  // dire pendant x sec
  Blockly.Blocks.dire = {
    init: function () {
      this.appendDummyInput()
        .appendField('dire')
      this.appendValueInput('DELTA')
        .setCheck()
      this.appendDummyInput()
        .appendField('pendant')
      this.appendValueInput('TEMPS')
        .setCheck()
      this.appendDummyInput()
        .appendField('secondes')
      this.setInputsInline(true)
      this.setPreviousStatement(true, null)
      this.setNextStatement(true, null)
      this.setColour(264)
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }
  Blockly.JavaScript.dire = (block) => {
    return {
      type: 'ordre',
      nom: 'dire',
      javascript: () => {
        let b = Blockly.JavaScript.valueToCode(block, 'DELTA', Blockly.JavaScript.ORDER_ATOMIC)
        let valueTemps = (Blockly.JavaScript.valueToCode(block, 'TEMPS', Blockly.JavaScript.ORDER_ATOMIC) + '')
        // test sur durée pas trop longue

        if (!valueTemps.length) return { result: 'prob', quoi: 'Je ne comprends pas cette durée !' }
        valueTemps = scratch.vireGuillemets(valueTemps)
        b = scratch.vireGuillemets(b).replace(/\\'/g, "'")
        if (isNaN(valueTemps)) {
          return { result: 'prob', quoi: 'Je ne comprends pas cette durée !' }
        }
        if (valueTemps > 10) {
          return { result: 'prob', quoi: 'Cette durée est bien trop longue !!' }
        }
        if (b === 'erreur block vide') {
          return { result: 'prob', quoi: 'Erreur block vide !' }
        }
        if (b === 'erreur valeur interdite') {
          return { result: 'prob', quoi: 'Le programme ne peut contenir que les nombres de l’énoncé !' }
        }
        scratch.scratchDire(b, valueTemps)
        return { result: 'ordrew' }
      }
    }
  }
  // VARIABLES
  // variables_get
  Blockly.Blocks.data_variable = { init: function () { this.jsonInit({ message0: '%1', lastDummyAlign0: 'CENTRE', args0: [{ type: 'field_variable_getter', text: '', name: 'VARIABLE', variableType: '' }], category: Blockly.Categories.data, extensions: ['contextMenu_getVariableBlock', 'colours_data', 'output_string'] }) } }
  Blockly.JavaScript.data_variable = (block) => {
    const v2 = block.getFieldValue('VARIABLE')
    let lavar
    if (!scratch.params.leWork) scratch.params.leWork = scratch.params.scratchProgDonnees.workspace
    const mmm = scratch.params.leWork.getAllVariables()
    lavar = ''
    for (let i = 0; i < mmm.length; i++) {
      if (mmm[i].id_ === v2) {
        lavar = mmm[i].name
      }
    }
    if (lavar === 'NaN') {
      console.error('data_variable: pas trouvé de variable')
      return 'NaN'
    }
    if (lavar === '') {
      lavar = v2
    }
    let ichoiz = -1
    for (let i = 0; i < scratch.params.scratchProgDonnees.variables.length; i++) {
      if (scratch.params.scratchProgDonnees.variables[i].name === lavar) {
        ichoiz = i
        break
      }
    }
    if (ichoiz === -1) {
      return 'NaN'
    }
    return scratch.params.scratchProgDonnees.variables[ichoiz].val + ''
  }
  // variables_set
  Blockly.Blocks.data_setvariableto = { init: function () { this.jsonInit({ message0: 'mettre %1 à %2', args0: [{ type: 'field_variable', name: 'VARIABLE' }, { type: 'input_value', name: 'VALUE' }], category: Blockly.Categories.data, extensions: ['colours_data', 'shape_statement'] }) } }
  Blockly.JavaScript.data_setvariableto = (block) => {
    const code = () => {
      const v2 = block.getFieldValue('VARIABLE')
      let lavar

      if (!scratch.params.leWork) scratch.params.leWork = scratch.params.scratchProgDonnees.workspace
      const mmm = scratch.params.leWork.getAllVariables()
      lavar = ''
      for (let i = 0; i < mmm.length; i++) {
        if (mmm[i].id_ === v2) {
          lavar = mmm[i].name
        }
      }

      if (lavar === 'NaN') {
        return 'NaN'
      }
      if (lavar === '') lavar = v2
      let b = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ATOMIC) + ''

      b = scratch.vireGuillemets(b)
      if (!lavar) {
        j3pNotify('pour tom dans ajoublosetval', { mmm, v2, leWork: scratch.params.leWork, block })
      }
      scratch.setVal(b, lavar)
      if (!scratch.params.MohaDireVite) scratch.scratchReplaceVar()
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'Set' }
  }
  /// ajoute a nb
  Blockly.JavaScript.data_changevariableby = (block) => {
    const code = () => {
      const v2 = block.getFieldValue('VARIABLE')
      if (!scratch.params.leWork) scratch.params.leWork = scratch.params.scratchProgDonnees.workspace
      const mmm = scratch.params.leWork.getAllVariables()
      let lavar = ''
      for (let i = 0; i < mmm.length; i++) {
        if (mmm[i].id_ === v2) {
          lavar = mmm[i].name
        }
      }
      if (lavar === 'NaN') {
        console.error('data_variable: pas trouvé de variable')
        return 'NaN'
      }

      let b = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ATOMIC)
      b = scratch.vireGuillemets(b)

      scratch.ajVal(b, lavar)
      scratch.scratchReplaceVar()
      scratch.params.scratchProgDonnees.Valeursutilisees.push(b)
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'Set' }
  }
  /// cache montre
  Blockly.JavaScript.data_showvariable = (block) => {
    const code = () => {
      const v2 = block.getFieldValue('VARIABLE')
      if (!scratch.params.leWork) scratch.params.leWork = scratch.params.scratchProgDonnees.workspace
      const mmm = scratch.params.leWork.getAllVariables()
      let lavar = ''
      for (let i = 0; i < mmm.length; i++) {
        if (mmm[i].id_ === v2) {
          lavar = mmm[i].name
        }
      }
      if (lavar === 'NaN') {
        console.error('data_showvariable: pas trouvé de variable')
        return 'NaN'
      }

      let i // on en a besoin en sortie de boucle
      for (i = 0; i < scratch.params.scratchProgDonnees.variables.length; i++) {
        if (scratch.params.scratchProgDonnees.variables[i].name === lavar) {
          break
        }
      }
      scratch.params.scratchProgDonnees.variables[i].visible = true
      scratch.scratchReplaceVar()
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'Set' }
  }
  Blockly.JavaScript.data_hidevariable = (block) => {
    const code = () => {
      const v2 = block.getFieldValue('VARIABLE')
      if (!scratch.params.leWork) scratch.params.leWork = scratch.params.scratchProgDonnees.workspace
      const mmm = scratch.params.leWork.getAllVariables()
      let lavar = ''
      for (let i = 0; i < mmm.length; i++) {
        if (mmm[i].id_ === v2) {
          lavar = mmm[i].name
        }
      }
      if (lavar === 'NaN') {
        console.error('data_showvariable: pas trouvé de variable')
        return 'NaN'
      }

      let i // on en a besoin en sortie de boucle
      for (i = 0; i < scratch.params.scratchProgDonnees.variables.length; i++) {
        if (scratch.params.scratchProgDonnees.variables[i].name === lavar) {
          break
        }
      }
      scratch.params.scratchProgDonnees.variables[i].visible = false
      scratch.scratchReplaceVar()
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'Set' }
  }

  // On avait ici un
  // Blockly.Variables.deleteVariable = function (a) { …
  // viré le 2021-12-11 car y’avait des variables non définies dedans, et qu’elle n’avait pas l’air de servir

  // INPUT ET REPONSE
  // get_us  pose une question et met dans reponse
  Blockly.Blocks.Get_us = {
    init: function () {
      this.jsonInit({
        message0: 'Demander %1 et attendre',
        args0: [{ type: 'input_value', name: 'STRING', default: 'Donne un nombre' }],
        category: Blockly.Categories.sensing,
        extensions: ['colours_sensing', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.Get_us = (block) => {
    const textNamee = Blockly.JavaScript.valueToCode(block, 'STRING', Blockly.JavaScript.ORDER_ATOMIC)
    const code = () => {
      scratch.scratchGetUs(scratch.vireGuillemets(textNamee).replace(/\\'/g, "'"))
      return { result: 'ordrew' }
    }

    return { type: 'ordre', javascript: code, nom: 'Getus' }
  }
  // reponse  (NEED decla varglobale scratch.params.meSectionblocktom...reponse)
  Blockly.Blocks.reponse = {
    init: function () {
      this.jsonInit({
        message0: 'réponse',
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'output_number']
      })
    }
  }
  Blockly.JavaScript.reponse = () => {
    return scratch.params.scratchProgDonnees.reponse
  }
  /// nombre aléatoire
  Blockly.Blocks.operator_random = {
    init: function () {
      this.jsonInit({
        message0: 'nombre aléatoire entre %1 et %2',
        args0: [{ type: 'input_value', name: 'NUM1' }, { type: 'input_value', name: 'NUM2' }],
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_number']
      })
    }
  }
  Blockly.JavaScript.operator_random = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'NUM1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'NUM2', Blockly.JavaScript.ORDER_ATOMIC)
    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }
    if ((isNaN(valueNUM1)) || (isNaN(valueNUM2))) {
      return 'erreur'
    }
    let val = j3pGetRandomInt(valueNUM1, valueNUM2)
    if (scratch.params.valAleoOblige.length > 0) {
      if (scratch.params.modeCompare) {
        val = scratch.params.valAleoOblige.shift()
      } else {
        for (let i = 0; i < scratch.params.valAleoOblige.length; i++) {
          if (scratch.params.valAleoOblige[i].idBlok === block.id) {
            val = scratch.params.valAleoOblige[i].val
          }
        }
      }
    }
    if (scratch.params.scratchProgDonnees.valAleaVerif) val = scratch.params.scratchProgDonnees.valAleaVerif
    val = String(val)
    scratch.params.scratchProgDonnees.valeursaleatoiressorties.push({ valeur: val, borne1: valueNUM1, borne2: valueNUM2 })
    return val
  }
  // CONDITIONS
  // égalite
  Blockly.JavaScript.operator_equals = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'OPERAND1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'OPERAND2', Blockly.JavaScript.ORDER_ATOMIC)
    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }
    if (!isNaN(valueNUM1) && !isNaN(valueNUM2)) {
      valueNUM1 = parseFloat(valueNUM1)
      valueNUM2 = parseFloat(valueNUM2)
      return (valueNUM1 === valueNUM2) ? 'vrai' : 'faux'
    } else { return (valueNUM1.trim() === valueNUM2.trim()) ? 'vrai' : 'faux' }
  }
  // plus grand
  Blockly.JavaScript.operator_gt = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'OPERAND1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'OPERAND2', Blockly.JavaScript.ORDER_ATOMIC)
    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }
    if (!isNaN(valueNUM1) && !isNaN(valueNUM2)) {
      valueNUM1 = parseFloat(valueNUM1)
      valueNUM2 = parseFloat(valueNUM2)
    }
    return (valueNUM1 > valueNUM2) ? 'vrai' : 'faux'
  }
  // plus petit
  Blockly.JavaScript.operator_lt = (block) => {
    let valueNUM1 = Blockly.JavaScript.valueToCode(block, 'OPERAND1', Blockly.JavaScript.ORDER_ATOMIC)
    let valueNUM2 = Blockly.JavaScript.valueToCode(block, 'OPERAND2', Blockly.JavaScript.ORDER_ATOMIC)
    if (!String(valueNUM1).length || !String(valueNUM2).length) return 'erreur block vide'
    valueNUM1 = scratch.vireGuillemets(valueNUM1)
    valueNUM2 = scratch.vireGuillemets(valueNUM2)

    if ((valueNUM1 === 'erreur block vide') || (valueNUM2 === 'erreur block vide')) {
      return 'erreur block vide'
    }
    if (!isNaN(valueNUM1) && !isNaN(valueNUM2)) {
      valueNUM1 = parseFloat(valueNUM1)
      valueNUM2 = parseFloat(valueNUM2)
    }
    return (valueNUM1 < valueNUM2) ? 'vrai' : 'faux'
  }
  // FUSEE
  // Avance
  Blockly.Blocks.avance_fusee = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 avancer',
        args0: [{
          type: 'field_image',
          src: fuseeImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.avance_fusee = () => {
    const code = () => {
      if (!scratch.params.scratchProgDonnees.Lutins[0].hauteur) {
        return {
          result: 'prob',
          quoi: 'La fusée ne peut pas se déplacer quand elle est posée !'
        }
      }
      switch (scratch.params.scratchProgDonnees.Lutins[0].angle) {
        case 0 :
          scratch.params.scratchProgDonnees.Lutins[0].coy--
          break
        case 90 :
          scratch.params.scratchProgDonnees.Lutins[0].cox++
          break
        case 180 :
          scratch.params.scratchProgDonnees.Lutins[0].coy++
          break
        case 270 :
          scratch.params.scratchProgDonnees.Lutins[0].cox--
          break
      }
      if ((scratch.params.scratchProgDonnees.Lutins[0].coy === scratch.params.scratchProgDonnees.nbcasefusee) || (scratch.params.scratchProgDonnees.Lutins[0].cox === scratch.params.scratchProgDonnees.nbcasefusee) || (scratch.params.scratchProgDonnees.Lutins[0].coy === -1) || (scratch.params.scratchProgDonnees.Lutins[0].cox === -1)) {
        return { result: 'prob', quoi: 'La fusée ne peut pas sortir de la carte !' }
      }
      const lend = scratch.params.scratchProgDonnees.cartefusee[scratch.params.scratchProgDonnees.Lutins[0].cox][scratch.params.scratchProgDonnees.Lutins[0].coy]
      if (lend === 2) { return { result: 'prob', quoi: 'La fusée ne peut pas passer sur un trou noir !' } }

      scratch.scratchVireRadar()
      scratch.scratchFusee()

      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'avance_fusee' }
  }
  // Droite
  Blockly.Blocks.droite_fusee = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 tourner %3',
        args0: [{
          type: 'field_image',
          src: fuseeImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-right.svg',
          width: 24,
          height: 24
        }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.droite_fusee = () => {
    const code = () => {
      if (!scratch.params.scratchProgDonnees.Lutins[0].hauteur) {
        return {
          result: 'prob',
          quoi: 'La fusée ne peut pas se déplacer quand elle est posée !'
        }
      }
      scratch.params.scratchProgDonnees.Lutins[0].angle += 90
      if (scratch.params.scratchProgDonnees.Lutins[0].angle === 360) { scratch.params.scratchProgDonnees.Lutins[0].angle = 0 }
      scratch.scratchFusee()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'droite_fusee' }
  }
  // Gauche
  Blockly.Blocks.gauche_fusee = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 tourner %3',
        args0: [{
          type: 'field_image',
          src: fuseeImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }, {
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-left.svg',
          width: 24,
          height: 24
        }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.gauche_fusee = (/* block */) => {
    const code = () => {
      if (!scratch.params.scratchProgDonnees.Lutins[0].hauteur) {
        return {
          result: 'prob',
          quoi: 'La fusée ne peut pas se déplacer quand elle est posée !'
        }
      }
      scratch.params.scratchProgDonnees.Lutins[0].angle -= 90
      if (scratch.params.scratchProgDonnees.Lutins[0].angle < 0) { scratch.params.scratchProgDonnees.Lutins[0].angle = 270 }
      scratch.scratchFusee()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'gauche_fusee' }
  }
  // Decolle
  Blockly.Blocks.decolle_fusee = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 décoller',
        args0: [{
          type: 'field_image',
          src: fuseeImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_sounds', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.decolle_fusee = (/* block */) => {
    return {
      type: 'ordre',
      nom: 'decolle_fusee',
      javascript: () => {
        if (scratch.params.scratchProgDonnees.Lutins[0].hauteur === 1) { return { result: 'prob', quoi: 'La fusée est déjà en vol !' } }
        scratch.params.scratchProgDonnees.Lutins[0].hauteur = 1
        scratch.scratchFusee()
        return { result: 'ordre' }
      }
    }
  }
  // Atteris
  Blockly.Blocks.atteri_fusee = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 atterrir',
        args0: [{
          type: 'field_image',
          src: fuseeImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_sounds', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.atteri_fusee = (/* block */) => {
    const code = () => {
      if (!scratch.params.scratchProgDonnees.Lutins[0].hauteur) return { result: 'prob', quoi: 'La fusée est déjà posée !' }
      const lend = scratch.params.scratchProgDonnees.cartefusee[scratch.params.scratchProgDonnees.Lutins[0].cox][scratch.params.scratchProgDonnees.Lutins[0].coy]
      if ((lend !== 1) && (lend !== 3)) {
        return {
          result: 'prob',
          quoi: 'La fusée ne peut se poser que sur une planète !'
        }
      }
      scratch.params.scratchProgDonnees.Lutins[0].hauteur = 0
      scratch.scratchFusee()

      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'atteri_fusee' }
  }
  // sauver
  Blockly.Blocks.recuperer_fusee = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 récupérer',
        args0: [{
          type: 'field_image',
          src: cosmoImg,
          width: 30,
          height: 30
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_sounds', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.recuperer_fusee = (/* block */) => {
    const code = () => {
      if (!scratch.params.scratchProgDonnees.Lutins[0].hauteur) return { result: 'prob', quoi: 'La fusée est déjà posée !' }
      const lend = scratch.params.scratchProgDonnees.cartefusee[scratch.params.scratchProgDonnees.Lutins[0].cox][scratch.params.scratchProgDonnees.Lutins[0].coy]
      if (lend !== 4) return { result: 'prob', quoi: 'Il n’y a pas de spationaute à sauver' }
      scratch.params.scratchProgDonnees.cartefusee[scratch.params.scratchProgDonnees.Lutins[0].cox][scratch.params.scratchProgDonnees.Lutins[0].coy] = 0
      for (let i = 0; i < scratch.params.idIm.length; i++) {
        if (scratch.params.idIm[i].id === scratch.params.scratchProgDonnees.Lutins[0].cox + 'cosmo' + scratch.params.scratchProgDonnees.Lutins[0].coy) {
          j3pDetruit(scratch.params.idIm[i].el)
        }
      }
      scratch.scratchFusee()

      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'recuperer_fusee' }
  }
  // conditions
  // planet
  Blockly.Blocks.operator_fusee_planet = {
    init: function () {
      this.jsonInit({
        message0: '%1',
        args0: [{
          type: 'field_image',
          src: planetrougeImg,
          width: 30,
          height: 30
        }],
        tooltip: 'Renvoie VRAI si la fusée est sur une planète rouge.',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.operator_fusee_planet = () => {
    if (scratch.params.scratchProgDonnees.cartefusee[scratch.params.scratchProgDonnees.Lutins[0].cox][scratch.params.scratchProgDonnees.Lutins[0].coy] === 3) {
      return 'vrai'
    }
    return 'faux'
  }
  // cosmo
  Blockly.Blocks.operator_fusee_cosmo = {
    init: function () {
      this.jsonInit({
        message0: '%1',
        args0: [{
          type: 'field_image',
          src: cosmoImg,
          width: 30,
          height: 30
        }],
        tooltip: 'Renvoie VRAI si la fusée est sur un spationaute.',
        category: Blockly.Categories.operators,
        extensions: ['colours_operators', 'output_boolean']
      })
    }
  }
  Blockly.JavaScript.operator_fusee_cosmo = () => {
    if (scratch.params.scratchProgDonnees.cartefusee[scratch.params.scratchProgDonnees.Lutins[0].cox][scratch.params.scratchProgDonnees.Lutins[0].coy] === 4) {
      return 'vrai'
    }
    return 'faux'
  }

  // BRAS
  // monter
  Blockly.Blocks.monter_bras = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 monter',
        args0: [{
          type: 'field_image',
          src: fourcheImg,
          width: 40,
          height: 40
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.monter_bras = () => {
    const code = () => {
      if (scratch.params.scratchProgDonnees.Lutins[0].coy === 1) return { result: 'prob', quoi: 'Le bras est déjà en hauteur !' }

      if (scratch.params.scratchProgDonnees.Lutins[0].angle === 1) {
        return {
          result: 'prob',
          quoi: 'Il faut réduire le bras avant tout déplacement !'
        }
      }

      scratch.params.scratchProgDonnees.Lutins[0].coy = 1

      scratch.scratchBras()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'monter_bras' }
  }
  // descendre
  Blockly.Blocks.descendre_bras = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 descendre',
        args0: [{
          type: 'field_image',
          src: fourcheImg,
          width: 40,
          height: 40
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.descendre_bras = () => {
    const code = () => {
      if (!scratch.params.scratchProgDonnees.Lutins[0].coy) return { result: 'prob', quoi: 'Le bras est déjà en bas !' }

      if (scratch.params.scratchProgDonnees.Lutins[0].angle === 1) {
        return {
          result: 'prob',
          quoi: 'Il faut réduire le bras avant tout déplacement !'
        }
      }

      scratch.params.scratchProgDonnees.Lutins[0].coy = 0

      scratch.scratchBras()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'descendre_bras' }
  }

  // tourner
  Blockly.Blocks.tourner_bras = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 tourner',
        args0: [{
          type: 'field_image',
          src: fourcheImg,
          width: 40,
          height: 40
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.tourner_bras = () => {
    const code = () => {
      if (scratch.params.scratchProgDonnees.Lutins[0].angle === 1) {
        return {
          result: 'prob',
          quoi: 'Il faut réduire le bras avant tout déplacement !'
        }
      }

      scratch.params.scratchProgDonnees.Lutins[0].cox = 1 - scratch.params.scratchProgDonnees.Lutins[0].cox

      scratch.scratchBras()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'tourner_bras' }
  }
  // agrandir
  Blockly.Blocks.agrandir_bras = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 agrandir',
        args0: [{
          type: 'field_image',
          src: bras2Img,
          width: 40,
          height: 40
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_sounds', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.agrandir_bras = () => {
    const code = () => {
      if (scratch.params.scratchProgDonnees.Lutins[0].angle === 1) {
        return {
          result: 'prob',
          quoi: 'Je ne peux pas agrandir davantage le bras !'
        }
      }

      scratch.params.scratchProgDonnees.Lutins[0].angle = 1

      scratch.scratchBras()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'agrandir_bras' }
  }

  // retrecir
  Blockly.Blocks.reduire_bras = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 réduire',
        args0: [{
          type: 'field_image',
          src: bras2Img,
          width: 40,
          height: 40
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_sounds', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.reduire_bras = () => {
    return {
      nom: 'reduire_bras',
      type: 'ordre',
      javascript: () => {
        if (scratch.params.scratchProgDonnees.Lutins[0].angle === 0) {
          return {
            result: 'prob',
            quoi: 'Je ne peux pas réduire davantage le bras !'
          }
        }

        scratch.params.scratchProgDonnees.Lutins[0].angle = 0

        scratch.scratchBras()
        return { result: 'ordre' }
      }
    }
  }
  // prendre
  Blockly.Blocks.prendre_bras = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 prendre',
        args0: [{
          type: 'field_image',
          src: pinceImg,
          width: 40,
          height: 40
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_operators', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.prendre_bras = () => {
    const code = () => {
      if (scratch.params.scratchProgDonnees.Lutins[0].angle === 0) {
        return {
          result: 'prob',
          quoi: 'Le bras doit être au dessus d’un tapis !'
        }
      }
      if (scratch.params.scratchProgDonnees.donneesbras.bras) {
        return {
          result: 'prob',
          quoi: 'Le bras ne peut prendre qu’un seul objet !'
        }
      }

      if (!scratch.params.scratchProgDonnees.Lutins[0].cox) {
        if (scratch.params.scratchProgDonnees.Lutins[0].coy === 1) {
          if (scratch.params.scratchProgDonnees.donneesbras.tapis2 === 0) {
            return {
              result: 'prob',
              quoi: 'Il n’y a pas d’objet à prendre !'
            }
          }
          scratch.params.scratchProgDonnees.donneesbras.bras = scratch.params.scratchProgDonnees.donneesbras.tapis2
          scratch.params.scratchProgDonnees.donneesbras.tapis2 = 0
        } else {
          if (scratch.params.scratchProgDonnees.donneesbras.tapis1 === 0) {
            return {
              result: 'prob',
              quoi: 'Il n’y a pas d’objet à prendre !'
            }
          }
          scratch.params.scratchProgDonnees.donneesbras.bras = scratch.params.scratchProgDonnees.donneesbras.tapis1
          scratch.params.scratchProgDonnees.donneesbras.tapis1 = 0
        }
      } else {
        if (scratch.params.scratchProgDonnees.Lutins[0].coy === 1) {
          if (scratch.params.scratchProgDonnees.donneesbras.tapis4 === 0) {
            return {
              result: 'prob',
              quoi: 'Il n’y a pas d’objet à prendre !'
            }
          }
          scratch.params.scratchProgDonnees.donneesbras.bras = scratch.params.scratchProgDonnees.donneesbras.tapis4
          scratch.params.scratchProgDonnees.donneesbras.tapis4 = 0
        } else {
          if (scratch.params.scratchProgDonnees.donneesbras.tapis3 === 0) {
            return {
              result: 'prob',
              quoi: 'Il n’y a pas d’objet à prendre !'
            }
          }
          scratch.params.scratchProgDonnees.donneesbras.bras = scratch.params.scratchProgDonnees.donneesbras.tapis3
          scratch.params.scratchProgDonnees.donneesbras.tapis3 = 0
        }
      }

      scratch.scratchBras()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'prendre_bras' }
  }
  // deposer
  Blockly.Blocks.deposer_bras = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 déposer',
        args0: [{
          type: 'field_image',
          src: pinceImg,
          width: 40,
          height: 40
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_operators', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.deposer_bras = () => {
    const code = () => {
      if (scratch.params.scratchProgDonnees.Lutins[0].angle === 0) {
        return {
          result: 'prob',
          quoi: 'Le bras doit être au dessus d’un tapis !'
        }
      }
      if (scratch.params.scratchProgDonnees.donneesbras.bras === 0) return { result: 'prob', quoi: 'Le bras n’a pas d’objet à déposer !' }

      if (scratch.params.scratchProgDonnees.Lutins[0].cox === 0) {
        if (scratch.params.scratchProgDonnees.Lutins[0].coy === 1) {
          if (scratch.params.scratchProgDonnees.donneesbras.tapis2 !== 0) {
            return {
              result: 'prob',
              quoi: 'Il n’y a pas d’objet à prendre !'
            }
          }
          scratch.params.scratchProgDonnees.donneesbras.tapis2 = scratch.params.scratchProgDonnees.donneesbras.bras
          scratch.params.scratchProgDonnees.donneesbras.bras = 0
        } else {
          if (scratch.params.scratchProgDonnees.donneesbras.tapis1 !== 0) {
            return {
              result: 'prob',
              quoi: 'Il n’y a pas d’objet à prendre !'
            }
          }
          scratch.params.scratchProgDonnees.donneesbras.tapis1 = scratch.params.scratchProgDonnees.donneesbras.bras
          scratch.params.scratchProgDonnees.donneesbras.bras = 0
        }
      } else {
        if (scratch.params.scratchProgDonnees.Lutins[0].coy === 1) {
          if (scratch.params.scratchProgDonnees.donneesbras.tapis4 !== 0) {
            return {
              result: 'prob',
              quoi: 'Il n’y a pas d’objet à prendre !'
            }
          }
          scratch.params.scratchProgDonnees.donneesbras.tapis4 = scratch.params.scratchProgDonnees.donneesbras.bras
          scratch.params.scratchProgDonnees.donneesbras.bras = 0
        } else {
          if (scratch.params.scratchProgDonnees.donneesbras.tapis3 !== 0) {
            return {
              result: 'prob',
              quoi: 'Il n’y a pas d’objet à prendre !'
            }
          }
          scratch.params.scratchProgDonnees.donneesbras.tapis3 = scratch.params.scratchProgDonnees.donneesbras.bras
          scratch.params.scratchProgDonnees.donneesbras.bras = 0
        }
      }

      scratch.scratchBras()
      return { result: 'ordre' }
    }

    return {
      type: 'ordre',
      javascript: code,
      nom: 'deposer_bras'
    }
  }
  // SI SIMPLE
  Blockly.JavaScript.control_if = (block) => {
    const statementsName = Blockly.JavaScript.statementToCode(block, 'SUBSTACK')
    return { type: 'si', sioui: statementsName, nom: 'SI', sinon: '', id: block.id }
  }
  // SI SINON
  Blockly.JavaScript.control_if_else = (block) => {
    const statementsName = Blockly.JavaScript.statementToCode(block, 'SUBSTACK')
    const statementsName2 = Blockly.JavaScript.statementToCode(block, 'SUBSTACK2')
    return { type: 'si', sioui: statementsName, nom: 'SI', sinon: statementsName2, id: block.id }
  }
  // PI
  Blockly.Blocks.pi = {
    init: function () {
      this.jsonInit({
        message0: 'π',
        category: Blockly.Categories.motion,
        extensions: ['colours_data', 'output_number']
      })
    }
  }
  Blockly.JavaScript.pi = () => {
    return Math.PI
  }
  // faux x
  Blockly.Blocks.fauxx = {
    init: function () {
      this.jsonInit({
        message0: 'réponse',
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'output_number']
      })
    }
  }
  Blockly.JavaScript.fauxx = () => {
    return 'x@ù'
  }

  Blockly.Blocks.Giv_pomme = {
    init: function () {
      this.jsonInit({
        message0: 'Transmettre la valeur de %1',
        args0: [{ type: 'input_value', name: 'VARIABLE' }],
        category: Blockly.Categories.sensing,
        extensions: ['colours_data', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.Giv_pomme = (block) => {
    const cose = () => {
      const v2 = Blockly.JavaScript.valueToCode(block, 'VARIABLE', Blockly.JavaScript.ORDER_ATOMIC)
      if (v2 === 'erreur block vide') {
        return { result: 'prob', quoi: 'erreur blok vide !' }
      }
      switch (scratch.params.onestou) {
        case 'Félix': scratch.params.scratchProgDonnees.mem[1] = v2
          break
        case 'Llyn': scratch.params.scratchProgDonnees.mem[0] = v2
          break
        case 'Charlie': scratch.params.scratchProgDonnees.mem[2] = v2
          break
        default:
          return { result: 'prob', quoi: 'Il n’y a personne ici !' }
      }
      scratch.scratchMajMem()
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: cose, nom: 'Giv_pomme' }
  }
  Blockly.Blocks.Get_pomme = {
    init: function () {
      this.jsonInit({
        message0: 'Mettre le nombre dans %1',
        args0: [{ type: 'field_variable', name: 'VARIABLE' }],
        category: Blockly.Categories.sensing,
        extensions: ['colours_data', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.Get_pomme = (block) => {
    const cose = () => {
      const v2 = block.getFieldValue('VARIABLE')
      let mmm, lavar
      if (scratch.params.scratchProgDonnees.progcolo) {
        mmm = scratch.params.scratchProgDonnees.LesVariablesCo
        lavar = ''
        for (let i = 0; i < mmm.length; i++) {
          if (mmm[i].id == v2) { // eslint-disable-line eqeqeq
            lavar = mmm[i].name
          }
        }
      } else {
        if (!scratch.params.leWork) scratch.params.leWork = scratch.params.scratchProgDonnees.workspace
        mmm = scratch.params.leWork.getAllVariables()
        lavar = ''
        for (let i = 0; i < mmm.length; i++) {
          if (mmm[i].id_ === v2) {
            lavar = mmm[i].name
          }
        }
      }

      if (lavar === 'NaN') {
        console.error('data_variable: pas trouvé de variable')
        return 'NaN'
      }
      switch (scratch.params.onestou) {
        case 'Félix': scratch.setVal(scratch.params.scratchProgDonnees.mem[1], lavar)
          break
        case 'Llyn': scratch.setVal(scratch.params.scratchProgDonnees.mem[0], lavar)
          break
        case 'Charlie': scratch.setVal(scratch.params.scratchProgDonnees.mem[2], lavar)
          break
        default:
          return { result: 'prob', quoi: 'Personne n’a d’information ici !' }
      }
      scratch.scratchReplaceVar()
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: cose, nom: 'Get_pomme' }
  }
  Blockly.Blocks.vaCharlie = {
    init: function () {
      this.jsonInit({
        message0: 'Aller chez Charlie',
        args0: [],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.vaCharlie = () => {
    const code = () => {
      scratch.params.scratchProgDonnees.Lutins[0].cox = 150
      scratch.params.scratchProgDonnees.Lutins[0].coy = 135
      scratch.params.imageLutin[0].y.baseVal.value = scratch.params.scratchProgDonnees.Lutins[0].coy
      scratch.params.imageLutin[0].x.baseVal.value = scratch.params.scratchProgDonnees.Lutins[0].cox
      scratch.params.onestou = 'Charlie'
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'vaCharlie' }
  }
  Blockly.Blocks.vaLlyn = {
    init: function () {
      this.jsonInit({
        message0: 'Aller chez Llyn',
        args0: [],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.vaLlyn = () => {
    const code = () => {
      scratch.params.scratchProgDonnees.Lutins[0].cox = 25
      scratch.params.scratchProgDonnees.Lutins[0].coy = 180
      scratch.params.imageLutin[0].y.baseVal.value = scratch.params.scratchProgDonnees.Lutins[0].coy
      scratch.params.imageLutin[0].x.baseVal.value = scratch.params.scratchProgDonnees.Lutins[0].cox
      scratch.params.onestou = 'Llyn'
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'vaLlyn' }
  }
  Blockly.Blocks.vaFelix = {
    init: function () {
      this.jsonInit({
        message0: 'Aller chez Félix',
        args0: [],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.vaFelix = () => {
    const code = () => {
      scratch.params.scratchProgDonnees.Lutins[0].cox = 25
      scratch.params.scratchProgDonnees.Lutins[0].coy = 80
      scratch.params.imageLutin[0].y.baseVal.value = scratch.params.scratchProgDonnees.Lutins[0].coy
      scratch.params.imageLutin[0].x.baseVal.value = scratch.params.scratchProgDonnees.Lutins[0].cox
      scratch.params.onestou = 'Félix'
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'vaFelix' }
  }

  Blockly.Blocks.MAkePerso = {
    init: function () {
      this.jsonInit({
        type: 'make',
        message0: 'Définir Mon block',
        inputsInline: false,
        nextStatement: null,
        colour: 330,
        tooltip: '',
        helpUrl: ''
      })
    }
  }
  Blockly.JavaScript.MAkePerso = () => {
    return { type: 'make', eventtype: 'make', nom: 'make' }
  }
  Blockly.Blocks.perso = {
    init: function () {
      this.jsonInit({
        message0: 'Mon block',
        args0: [],
        category: Blockly.Categories.motion,
        colour: 330,
        extensions: ['shape_statement']
      })
    }
  }
  Blockly.JavaScript.perso = () => {
    return { type: 'Mb', nom: 'Mb' }
  }

  // creat block Perso
  Blockly.Blocks.varx = {
    init: function () {
      this.jsonInit({
        message0: 'x',
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'output_number']
      })
    }
  }
  Blockly.JavaScript.varx = () => {
    return scratch.params.scratchProgDonnees.x
  }
  Blockly.Blocks.vary = {
    init: function () {
      this.jsonInit({
        message0: 'y',
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'output_number']
      })
    }
  }
  Blockly.JavaScript.vary = () => {
    return scratch.params.scratchProgDonnees.y
  }
  Blockly.Blocks.varz = {
    init: function () {
      this.jsonInit({
        message0: 'z',
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'output_number']
      })
    }
  }
  Blockly.JavaScript.varz = () => {
    return scratch.params.scratchProgDonnees.z
  }

  // DESSIN
  Blockly.Blocks.motion_movesteps = {
    init: function () {
      this.jsonInit({
        message0: 'Avancer de %1 pas',
        args0: [{ type: 'input_value', name: 'STEPS' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.motion_movesteps = (block) => {
    const code = () => {
      let b = Blockly.JavaScript.valueToCode(block, 'STEPS', Blockly.JavaScript.ORDER_ATOMIC)

      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }

      scratch.scratchMove(b)
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'move' }
  }
  // pointbleu
  Blockly.Blocks.motion_movesteps = {
    init: function () {
      this.jsonInit({
        message0: 'Avancer de %1 pas',
        args0: [{ type: 'input_value', name: 'STEPS' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.motion_movesteps = (block) => {
    const code = () => {
      let b = Blockly.JavaScript.valueToCode(block, 'STEPS', Blockly.JavaScript.ORDER_ATOMIC)

      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }

      scratch.scratchMove(b)
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'move' }
  }
  // TOURNE
  Blockly.Blocks.motion_turnright = {
    init: function () {
      this.jsonInit({
        message0: 'Tourner %1 de %2 degrés',
        args0: [{
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-right.svg',
          width: 24,
          height: 24
        }, { type: 'input_value', name: 'DEGREES' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.Blocks.motion_turnleft = {
    init: function () {
      this.jsonInit({
        message0: 'Tourner %1 de %2 degrés',
        args0: [{
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-left.svg',
          width: 24,
          height: 24
        }, { type: 'input_value', name: 'DEGREES' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.motion_turnright = (block) => {
    const code = () => {
      let b = Blockly.JavaScript.valueToCode(block, 'DEGREES', Blockly.JavaScript.ORDER_ATOMIC)

      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }

      scratch.scratchTurnRight(b)
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'tourneD' }
  }
  Blockly.JavaScript.motion_turnleft = (block) => {
    const code = () => {
      let b = Blockly.JavaScript.valueToCode(block, 'DEGREES', Blockly.JavaScript.ORDER_ATOMIC)

      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }

      scratch.scratchTurnLeft(b)
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'tourneG' }
  }
  // FIN TOURNE
  // STYLO
  // leve stylo
  Blockly.Blocks.levestylo = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 relever le stylo',
        args0: [{
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'extensions/pen-block-icon.svg',
          width: 40,
          height: 40
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_operators', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.levestylo = () => {
    const code = () => {
      scratch.params.scratchProgDonnees.Lutins[0].StyloPose = false
      scratch.params.scratchProgDonnees.Lutins[0].img = dessinleveImg
      scratch.scratchMain()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'move' }
  }
  // fin leve stylo
  Blockly.Blocks.pointB = {
    init: function () {
      this.jsonInit({
        message0: 'Aller au point bleu',
        args0: [],
        category: Blockly.Categories.motion,
        extensions: ['colours_operators', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.pointB = () => {
    const code = () => {
      if (scratch.params.scratchProgDonnees.Lutins[0].StyloPose) {
        scratch.scratchTrace(scratch.params.scratchProgDonnees.Lutins[0].cox + scratch.params.scratchProgDonnees.Lutins[0].larg - 1, scratch.params.scratchProgDonnees.Lutins[0].coy + 34, scratch.params.pointBx - (scratch.params.scratchProgDonnees.Lutins[0].cox + scratch.params.scratchProgDonnees.Lutins[0].larg - 1), scratch.params.pointBy - (scratch.params.scratchProgDonnees.Lutins[0].coy + 34))
      }
      scratch.params.scratchProgDonnees.Lutins[0].cox = scratch.params.pointBx - scratch.params.scratchProgDonnees.Lutins[0].larg + 1
      scratch.params.scratchProgDonnees.Lutins[0].coy = scratch.params.pointBy - 34
      scratch.params.scratchProgDonnees.Lutins[0].angle = 0
      scratch.scratchMain()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'pointB' }
  }
  Blockly.Blocks.pointV = {
    init: function () {
      this.jsonInit({
        message0: 'Aller au point vert',
        args0: [],
        category: Blockly.Categories.motion,
        extensions: ['colours_operators', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.pointV = () => {
    const code = () => {
      if (scratch.params.scratchProgDonnees.Lutins[0].StyloPose) {
        scratch.scratchTrace(scratch.params.scratchProgDonnees.Lutins[0].cox + scratch.params.scratchProgDonnees.Lutins[0].larg - 1, scratch.params.scratchProgDonnees.Lutins[0].coy + 34, scratch.params.pointVx - (scratch.params.scratchProgDonnees.Lutins[0].cox + scratch.params.scratchProgDonnees.Lutins[0].larg - 1), scratch.params.pointVy - (scratch.params.scratchProgDonnees.Lutins[0].coy + 34))
      }
      scratch.params.scratchProgDonnees.Lutins[0].cox = scratch.params.pointVx - scratch.params.scratchProgDonnees.Lutins[0].larg + 1
      scratch.params.scratchProgDonnees.Lutins[0].coy = scratch.params.pointVy - 34
      scratch.params.scratchProgDonnees.Lutins[0].angle = 0
      scratch.scratchMain()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'pointB' }
  }
  // pose stylo
  Blockly.Blocks.poserstylo = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 stylo en position d’écriture',
        args0: [{
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'extensions/pen-block-icon.svg',
          width: 40,
          height: 40
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_operators', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.poserstylo = () => {
    const code = () => {
      scratch.params.scratchProgDonnees.Lutins[0].StyloPose = true
      scratch.params.scratchProgDonnees.Lutins[0].img = dessinImg
      scratch.scratchMain()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'move' }
  }
  // FIN pose stylo
  // couleur stylo
  Blockly.Blocks.metcouleur = {
    init: function () {
      this.jsonInit({
        message0: 'Mettre couleur à %1',
        args0: [{ type: 'input_value', name: 'COLOR' }],
        category: Blockly.Categories.sensing,
        extensions: ['colours_operators', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.metcouleur = (block) => {
    return {
      type: 'ordre',
      nom: 'move',
      javascript: () => {
        let b = Blockly.JavaScript.valueToCode(block, 'COLOR', Blockly.JavaScript.ORDER_ATOMIC)
        b = scratch.vireGuillemets(b)
        scratch.params.scratchProgDonnees.Lutins[0].StyloCol = b
        return { result: 'ordre' }
      }
    }
  }

  Blockly.JavaScript.text = (block) => block.getFieldValue('TEXT')
  Blockly.JavaScript.math_number = (block) => block.getFieldValue('NUM')
  Blockly.Blocks.dire2 = Blockly.Blocks.dire
  Blockly.JavaScript.dire2 = Blockly.JavaScript.dire
  Blockly.Blocks.dire3 = Blockly.Blocks.dire
  Blockly.JavaScript.dire3 = Blockly.JavaScript.dire

  // mohaBlok
  Blockly.Blocks.motion_pointindirection = {
    init: function () {
      this.jsonInit({
        message0: 'S’orienter à %1',
        args0: [{ type: 'input_value', name: 'STEPS' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.motion_pointindirection = function (block) {
    const code = function () {
      let b = Blockly.JavaScript.valueToCode(block, 'STEPS', Blockly.JavaScript.ORDER_ATOMIC)
      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }
      scratch.scratchTourneMoha(b)
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'avancer' }
  }
  Blockly.Blocks.avancer = {
    init: function () {
      this.jsonInit({
        message0: 'Avancer de %1 pas',
        args0: [{ type: 'input_value', name: 'STEPS' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.avancer = function (block) {
    const code = function () {
      let b = Blockly.JavaScript.valueToCode(block, 'STEPS', Blockly.JavaScript.ORDER_ATOMIC)
      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }
      scratch.scratchMoveMoha(b)
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'avancer' }
  }
  Blockly.Blocks.aller_a = {
    init: function () {
      this.jsonInit({
        message0: 'Aller à x:%1 y: %2',
        args0: [{ type: 'input_value', name: 'X', default: '0' }, { type: 'input_value', name: 'Y', default: '0' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.aller_a = function (block) {
    const code = function () {
      const x = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_ATOMIC))
      const y = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_ATOMIC))
      if (isNaN(x)) {
        return { result: 'prob', quoi: 'Je ne comprends pas la valeur x !' }
      }
      if (isNaN(y)) {
        return { result: 'prob', quoi: 'Je ne comprends pas la valeur y !' }
      }
      scratch.scratchAllerAMoha(Number(x), Number(y))
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'aller_a' }
  }
  Blockly.Blocks.tourne_droite = {
    init: function () {
      this.jsonInit({
        message0: 'Tourner %1 de %2 degrés',
        args0: [{
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-right.svg',
          width: 24,
          height: 24
        }, { type: 'input_value', name: 'DEGREES' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.tourne_droite = function (block) {
    const code = function () {
      let b = Blockly.JavaScript.valueToCode(block, 'DEGREES', Blockly.JavaScript.ORDER_ATOMIC)

      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }

      scratch.scratchMoveMohaDroite(b)
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: code, nom: 'tourne_droite' }
  }
  Blockly.Blocks.tourne_gauche = {
    init: function () {
      this.jsonInit({
        message0: 'Tourner %1 de %2 degrés',
        args0: [{
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'rotate-left.svg',
          width: 24,
          height: 24
        }, { type: 'input_value', name: 'DEGREES' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.tourne_gauche = function (block) {
    const code = function () {
      let b = Blockly.JavaScript.valueToCode(block, 'DEGREES', Blockly.JavaScript.ORDER_ATOMIC)

      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }

      scratch.scratchMoveMohaGauche(b)
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'tourne_gauche' }
  }
  Blockly.Blocks.lever_stylo = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 relever le stylo',
        args0: [{
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'extensions/pen-block-icon.svg',
          width: 40,
          height: 40
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_operators', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.lever_stylo = function () {
    const code = function () {
      scratch.params.scratchProgDonnees.Lutins[0].StyloPose = false
      scratch.scratchMainMoha()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'lever_stylo' }
  }
  Blockly.Blocks.poser_stylo = {
    init: function () {
      this.jsonInit({
        message0: '%1 %2 stylo en position d’écriture',
        args0: [{
          type: 'field_image',
          src: Blockly.mainWorkspace.options.pathToMedia + 'extensions/pen-block-icon.svg',
          width: 40,
          height: 40
        }, { type: 'field_vertical_separator' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_operators', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.poser_stylo = function () {
    const code = function () {
      scratch.params.scratchProgDonnees.Lutins[0].StyloPose = true
      scratch.scratchMainMoha()
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'poser_stylo' }
  }
  Blockly.Blocks.abscisse_x = {
    init: function () {
      this.jsonInit({
        message0: 'abscisse x',
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'output_number']
      })
    }
  }
  Blockly.JavaScript.abscisse_x = () => {
    return String(j3pArrondi((scratch.params.scratchProgDonnees.Lutins[0].cox - 90) * 5 / 3, 1))
  }
  Blockly.Blocks.ordonnee_y = {
    init: function () {
      this.jsonInit({
        message0: 'ordonnée y',
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'output_number']
      })
    }
  }
  Blockly.JavaScript.ordonnee_y = () => {
    return String(j3pArrondi((100 - scratch.params.scratchProgDonnees.Lutins[0].coy) * 5 / 3, 1))
  }
  Blockly.Blocks.direction = {
    init: function () {
      this.jsonInit({
        message0: 'direction',
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'output_number']
      })
    }
  }
  Blockly.JavaScript.direction = () => {
    return String(Math.floor(scratch.params.scratchProgDonnees.Lutins[0].angle + 90) % 360)
  }
  Blockly.Blocks.ajouter_x = {
    init: function () {
      this.jsonInit({
        message0: 'Ajouter %1 à x',
        args0: [{ type: 'input_value', name: 'STEPS' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.ajouter_x = function (block) {
    const code = function () {
      let b = Blockly.JavaScript.valueToCode(block, 'STEPS', Blockly.JavaScript.ORDER_ATOMIC)
      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }
      scratch.scratchAJoutexMoha(b)
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'ajouter_x' }
  }
  Blockly.Blocks.mettre_x = {
    init: function () {
      this.jsonInit({
        message0: 'Mettre x à %1',
        args0: [{ type: 'input_value', name: 'STEPS' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.mettre_x = function (block) {
    const code = function () {
      let b = Blockly.JavaScript.valueToCode(block, 'STEPS', Blockly.JavaScript.ORDER_ATOMIC)
      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }
      scratch.scratchMetsxMoha(b)
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'mettre_x' }
  }
  Blockly.Blocks.mettre_y = {
    init: function () {
      this.jsonInit({
        message0: 'Mettre y à %1',
        args0: [{ type: 'input_value', name: 'STEPS' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.mettre_y = function (block) {
    const code = function () {
      let b = Blockly.JavaScript.valueToCode(block, 'STEPS', Blockly.JavaScript.ORDER_ATOMIC)
      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }
      scratch.scratchMetsyMoha(b)
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'mettre_y' }
  }
  Blockly.Blocks.ajouter_y = {
    init: function () {
      this.jsonInit({
        message0: 'Ajouter %1 à y',
        args0: [{ type: 'input_value', name: 'STEPS' }],
        category: Blockly.Categories.motion,
        extensions: ['colours_motion', 'shape_statement']
      })
    }
  }
  Blockly.JavaScript.ajouter_y = function (block) {
    const code = function () {
      let b = Blockly.JavaScript.valueToCode(block, 'STEPS', Blockly.JavaScript.ORDER_ATOMIC)
      b = scratch.vireGuillemets(b)
      if (isNaN(b)) {
        return { result: 'prob', quoi: 'Je ne comprends pas cette valeur !' }
      }
      scratch.scratchAJouteyMoha(b)
      return { result: 'ordre' }
    }

    return { type: 'ordre', javascript: code, nom: 'ajouter_y' }
  }
  Blockly.JavaScript.operator_mathop = function (block) {
    let b = Blockly.JavaScript.valueToCode(block, 'NUM', Blockly.JavaScript.ORDER_ATOMIC)
    b = scratch.vireGuillemets(b)
    if (isNaN(b)) {
      return 'valeur incorrecte'
    }
    b = Number(b)
    const op = block.getFieldValue('OPERATOR')
    switch (op) {
      case 'abs': return String(Math.abs(b))
      case 'plancher':
      case 'floor': return String(Math.floor(b))
      case 'plafond':
      case 'ceiling': return String(Math.ceil(b))
      case 'racine':
      case 'sqrt': return String(Math.sqrt(b))
      case 'sin': return String(Math.sin(b * Math.PI / 180))
      case 'cos': return String(Math.cos(b * Math.PI / 180))
      case 'tan': return String(Math.tan(b * Math.PI / 180))
      case 'asin': return String(180 / Math.PI * Math.asin(b))
      case 'acos': return String(180 / Math.PI * Math.acos(b))
      case 'atan': return String(180 / Math.PI * Math.atan(b))
      case 'ln': return String(Math.log(b))
      case 'log': return String(Math.log10(b))
      case 'e^':
      case 'e ^': return String(Math.exp(b))
      case '10^':
      case '10 ^': return String(Math.pow(10, b))
    }
  }
  // FIN DECLA BLOCS
  // ajoutsFaits = true
}
