import { j3pAddContent, j3pAddElt, j3pClone, j3pEmpty, j3pGetNewId, j3pGetRandomInt, j3pModale, j3pNotify, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
// une image qui vient de sections
import arc2Img from 'src/legacy/sections/college/sixieme/Elementgeo/images/arc2.png'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'

import ajoutsBlocklyMathgraph from './ajoutsBlocklyMathgraph'
import Scratch from './Scratch'

const dummyFn = () => undefined
const getTrue = () => true
const listMarkeBase = [
  { color: 'black', segmentMarkStyle: '---' },
  { color: 'black', segmentMarkStyle: '--' },
  { color: 'green', segmentMarkStyle: '-' },
  { color: 'red', segmentMarkStyle: '---' },
  { color: 'green', segmentMarkStyle: '--' },
  { color: 'red', segmentMarkStyle: '-' },
  { color: 'green', segmentMarkStyle: '--' },
  { color: 'black', segmentMarkStyle: '-' },
  { color: 'green', segmentMarkStyle: '---' },
  { color: 'red', segmentMarkStyle: '--' }
]

/**
 * Construit un gros objet plein de méthodes pour les sections scratch utilisant mathgraph
 * @class
 */
class ScratchMathgraph extends Scratch {
  /**
   * @constructor
   * @param Blockly
   */
  constructor (Blockly) {
    // on étend Scratch, donc on doit appeler son constructeur en premier
    super(Blockly)
    // pour la version Mathgraph y’a des trucs en plus
    /**
     * Flag mis à true si l’objet courant est une instance de ScratchMathgraph (pour éviter d’importer ScratchMathgraph juste pour faire un instanceof dessus)
     * @type {boolean}
     */
    this.isMtg = true
    ajoutsBlocklyMathgraph(this.Blockly, this)
  }

  calculeMG (s, num) {
    let svgId
    switch (num) {
      case 1:
        svgId = this.params.svgId
        break
      case 2:
        svgId = this.params.svgId2
        break
      case 3:
        svgId = this.params.svgId3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    const arentre = this.scratchTagPourLettre(s, num)
    try {
      const tatag5 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addCalc({ nameCalc: tatag5, formula: arentre })
      return tatag5
    } catch (e) {
      return 'pb'
    }
  }

  scratchTagPourLettre (s, num) {
    let donnesMtg
    switch (num) {
      case 1:
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        donnesMtg = this.params.donnesMtg3
        break
    }
    for (let i = 0; i < donnesMtg.length; i++) {
      if (donnesMtg[i].type === 'point' ||
        donnesMtg[i].type === 'pointMilieu' ||
        donnesMtg[i].type === 'pointSym' ||
        donnesMtg[i].type === 'pointRot' ||
        donnesMtg[i].type === 'pointSur' ||
        donnesMtg[i].type === 'pointIntersection' ||
        donnesMtg[i].type === 'pointLibreAvecNom') {
        const ar = new RegExp(donnesMtg[i].noms[0], 'g')
        s = s.replace(ar, donnesMtg[i].tag)
      }
    }
    return s
  }

  decode () {
    const cmpt = super.decode()
    // version mathgraph faut faire ça en plus
    if (!this.params.aiPrevenuNomArc && !this.params.isEditing) {
      const listeArc = this.scratchListeDesArcs(this.params.scratchProgDonnees.tabcodeX)
      if (listeArc.length === 1) {
        this.params.aiPrevenuNomArc = true
        const hh = j3pModale({ titre: 'Notation spéciale' })
        hh.style.left = '20px'
        hh.style.top = '20px'
        const untab = addDefaultTable(hh, 5, 1)
        const untabl1 = addDefaultTable(untab[0][0], 1, 3)
        j3pAffiche(untabl1[0][0], null, 'Tu viens de créer un arc de cercle qui se nomme&nbsp;')
        j3pAffiche(untabl1[0][1], null, 'BC')
        j3pAffiche(untabl1[0][2], null, '&nbsp;.')
        untabl1[0][1].style.background = 'url("' + arc2Img + '"), #fff'
        untabl1[0][1].style.backgroundRepeat = 'no-repeat'
        untabl1[0][1].style.backgroundSize = '100% 50%'
        j3pAffiche(untab[1][0], null, 'Pour le nommer dans les blocs suivants,')
        j3pAffiche(untab[2][0], null, 'utilises la notation <b>arc BC</b>.\n\n')
        const untabl2 = addDefaultTable(untab[3][0], 1, 3)
        j3pAffiche(untabl2[0][0], null, '<i>Si tu avais créé un arc de cercle&nbsp;</i>')
        j3pAffiche(untabl2[0][1], null, 'JK')
        j3pAffiche(untab[4][0], null, '<i>il faudrait par la suite écrire <b>arc JK</b></i>')
        untabl2[0][1].style.background = 'url("' + arc2Img + '"), #fff'
        untabl2[0][1].style.backgroundRepeat = 'no-repeat'
        untabl2[0][1].style.backgroundSize = '100% 50%'
      }
    }
    return cmpt
  }

  faitSuitSpe () {
    this._mtgSuite(1, false, null)
    this.params.mtgApplecteur.calculate(this.params.svgId)
  }

  _mtgSuite (num, force, ev) {
    if (!this.params.sauteSuite) {
      let svgId, donnesMtg
      switch (num) {
        case 1:
          svgId = this.params.svgId
          donnesMtg = this.params.donnesMtg
          break
        case 2:
          svgId = this.params.svgId2
          donnesMtg = this.params.donnesMtg2
          break
        case 3:
          svgId = this.params.svgId3
          donnesMtg = this.params.donnesMtg3
          break
      }
      this.params.mtgApplecteur.setApiDoc(svgId)
      if (this.params.mtgApplecteur.getDoc(svgId).pointCapture) {
        const lep = this.scratchMtgRetrouveBase(num, { tag: this.params.mtgApplecteur.getDoc(svgId).pointCapture.tag }, 'objet')
        if (lep) {
          if (lep.fix) {
            this.params.mtgApplecteur.setPointPosition(svgId, lep.tag, lep.cooGarde.x, lep.cooGarde.y, true)
          }
        }
      }
      if (force) {
        for (let i = 0; i < donnesMtg.length; i++) {
          if (donnesMtg[i].fix) {
            this.params.mtgApplecteur.setPointPosition(svgId, donnesMtg[i].tag, donnesMtg[i].cooGarde.x, donnesMtg[i].cooGarde.y, true)
          }
        }
      }
      for (let i = 0; i < donnesMtg.length; i++) {
        let izViz = donnesMtg[i].viz()
        for (let j = 0; j < donnesMtg[i].depend.length; j++) {
          izViz = izViz && donnesMtg[donnesMtg[i].depend[j]].izViz
        }
        donnesMtg[i].izViz = izViz
        if (donnesMtg[i].type.indexOf('arc') !== -1 && !donnesMtg[i].centre) {
          this.params.mtgApplecteur.calculate(svgId)
          const arcAvoir = donnesMtg[i].funcArc()
          for (let j = 0; j < donnesMtg[i].toShow.length; j++) {
            if (izViz && !donnesMtg[i].efface && arcAvoir === j) {
              this.params.mtgApplecteur.setVisible({ elt: donnesMtg[i].toShow[j] })
            } else {
              this.params.mtgApplecteur.setHidden({ elt: donnesMtg[i].toShow[j] })
            }
          }
        } else {
          for (let j = 0; j < donnesMtg[i].toShow.length; j++) {
            if (izViz && (!donnesMtg[i].efface) && !donnesMtg[i].speCache) {
              try {
                this.params.mtgApplecteur.setVisible({ elt: donnesMtg[i].toShow[j] })
              } catch (e) {
                // eslint-disable-next-line no-console
                console.log(e)
              }
            } else {
              this.params.mtgApplecteur.setHidden({ elt: donnesMtg[i].toShow[j] })
            }
          }
        }
      }
      if (ev === null) return
    }
    if (num === 1) this._mtgOnMove(ev)
  }

  scratchMtgMessage (j) {
    j3pEmpty(this.params.tabMtg[0][0])
    this.params.tabMtg[0][0].classList.add('MessageCli')
    j3pAddContent(this.params.tabMtg[0][0], '&nbsp;' + j)
  }

  scratchMtgClean () {
    j3pEmpty(this.params.tabMtg[0][0])
    this.params.tabMtg[0][0].classList.remove('MessageCli')
  }

  scratchMtgFinishOrdre () {
    if (this.params.scratchProgDonnees.yaErreurMtg) {
      setTimeout(() => { this.params.scratchProgDonnees.pause = false }, 1)
      return
    }
    if (this.params.afaire.length === 0) {
      setTimeout(() => { this.params.scratchProgDonnees.pause = false }, 1)
    } else {
      const af = this.params.afaire[0]
      this.params.afaire.splice(0, 1)
      af()
    }
  }

  scratchMtgEnDehors (pt) {
    if (pt.x < 1) return true
    if (pt.y < 1) return true
    if (this.params.squizzEssai) {
      if (pt.x > 1000) return true
      if (pt.y > 1000) return true
    } else {
      if (pt.x > 384) return true
      if (pt.y > 384) return true
    }
    return false
  }

  scatchMtgPointsProches (num, pt) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    const aret = []
    for (let i = 0; i < donnesMtg.length; i++) {
      if (donnesMtg[i].type === 'point' ||
        donnesMtg[i].type === 'pointMilieu' ||
        donnesMtg[i].type === 'pointSym' ||
        donnesMtg[i].type === 'pointRot' ||
        donnesMtg[i].type === 'pointSur' ||
        donnesMtg[i].type === 'pointIntersection' ||
        donnesMtg[i].type === 'pointLibreAvecNom') {
        const p2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: donnesMtg[i].tag })
        if (Math.sqrt((pt.x - p2.x) * (pt.x - p2.x) + (pt.y - p2.y) * (pt.y - p2.y)) < 0.01) aret.push(i)
      }
    }
    return aret
  }

  /**
   * réinitialise les lettres et marque foraz
   */
  resetMtg () {
    this.params.lettres = j3pShuffle(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'])
    this.params.foraz = true
  }

  /**
   * Affecte la figure base64 qui sera affichée par mathgraph dans traceSortieBase
   * @param {string} figure La figure mathgraph en base64
   */
  setFigure (figure, isLibre) {
    if (!figure || typeof figure !== 'string') throw Error('Figure invalide')
    /**
     * La figure base64 qui sera affichée dans traceSortieBase
     * @type {string}
     */
    this.figure = figure
    this.xMax = (isLibre) ? 1000 : 350
    this.yMax = (isLibre) ? 1000 : 350
  }

  /**
   * Affecte mtgAppLecteur aux params
   * @param {MtgAppLecteur} mtgAppLecteur
   */
  setMtg (mtgAppLecteur) {
    /** @type {MtgAppLecteur} */
    this.params.mtgApplecteur = mtgAppLecteur
    this.params.foraz = true
  }

  scratchMtgReplaceLettres (ob) {
    const aret = {}
    aret.type = ob.type
    aret.noms = []
    if (ob.noms) {
      for (let i = 0; i < ob.noms.length; i++) {
        aret.noms.push(this.scratchMtgReplaceLettres2(ob.noms[i]))
      }
    }
    aret.x = ob.x
    aret.y = ob.y
    if (ob.sur) {
      aret.sur = []
      for (let i = 0; i < ob.sur.length; i++) {
        aret.sur.push(this.scratchMtgReplaceLettres2(ob.sur[i]))
      }
    }
    aret.coef = ob.coef
    aret.tag = ob.tag
    aret.tag2 = ob.tag2
    aret.tag3 = ob.tag3
    aret.visible = ob.visible
    aret.coef = ob.coef
    aret.pointille = ob.pointille
    aret.angle = ob.angle
    aret.nomCache = ob.nomCache
    if (ob.cont1) aret.cont1 = this.scratchMtgReplaceLettres2(ob.cont1)
    if (ob.rayon) aret.rayon = this.scratchMtgReplaceLettres2(ob.rayon)
    if (ob.long) aret.long = this.scratchMtgReplaceLettres2(ob.long)
    if (ob.cont2) aret.cont2 = this.scratchMtgReplaceLettres2(ob.cont2)
    if (ob.centre) aret.centre = this.scratchMtgReplaceLettres2(ob.centre)
    if (ob.point) aret.point = this.scratchMtgReplaceLettres2(ob.point)
    if (ob.cont) aret.cont = this.scratchMtgReplaceLettres2(ob.cont)
    if (ob.para) aret.para = this.scratchMtgReplaceLettres2(ob.para)
    if (ob.perp) aret.perp = this.scratchMtgReplaceLettres2(ob.perp)
    if (ob.point) aret.point = this.scratchMtgReplaceLettres2(ob.point)
    if (ob.ext1) aret.ext1 = this.scratchMtgReplaceLettres2(ob.ext1)
    if (ob.ext2) aret.ext2 = this.scratchMtgReplaceLettres2(ob.ext2)
    aret.efface = ob.efface
    aret.affnomspe = ob.affnomspe
    return aret
  }

  scratchMtgNewTag (i) {
    let tab
    switch (i) {
      case 1: tab = this.params.tagList1
        break
      case 2: tab = this.params.tagList2
        break
      default: tab = this.params.tagList3
    }
    let j = 0
    let ntag
    do {
      ntag = 't' + j
      j++
    } while (tab.includes(ntag))
    tab.push(ntag)
    return ntag
  }

  scratchMtgReplaceLettres2 (t) {
    let aret = t
    for (let i = 1; i < 27; i++) {
      const ar = '#' + i + '#'
      while (aret.indexOf(ar) !== -1) { aret = aret.replace(ar, this.params.lettres[i - 1]) }
    }
    return aret
  }

  /**
   * traceSortieBase version mathgraph
   * @param options
   */
  traceSortieBase (options = {}) {
    if (options.demoMode === true) return this.traceSortieBaseDemo()

    if (this.params.foraz) {
      if (this.params.isEditing) {
        const obb = this
        this.params.lettres = this.params.lettres.filter(function (value) {
          return obb.params.zoneReserveLet.value.indexOf(value) === -1
        })
      }
      this.params.listMarke = j3pClone(listMarkeBase)
      this.params.listMarke2 = j3pClone(listMarkeBase)
      this.params.listMarke3 = j3pClone(listMarkeBase)
      j3pEmpty(this.params.scratchProgDonnees.mypre)
      this.params.tabMtg = addDefaultTable(this.params.scratchProgDonnees.mypre, 2, 1)
      this.params.svgId = j3pGetNewId()
      if (!this.params.isEditing) {
        this.params.tabMtg[0][0].style.height = '40px'
        this.params.tabMtg[0][0].style.border = '3px solid blue'
      }
      if (this.params.squizzEssai) {
        j3pEmpty(this.params.tabMtg[0][0])
        const getUp = j3pAddElt(this.params.tabMtg[0][0], 'a', { href: '#' + this.idTop })
        j3pAddContent(getUp, '^')
      }
      if (!this.params.squizzEssai) {
        this.params.leSvg = j3pCreeSVG(this.params.tabMtg[1][0], {
          id: this.params.svgId,
          width: 385,
          height: 385
        })
      } else {
        this.params.leSvg = j3pCreeSVG(this.params.tabMtg[1][0], {
          id: this.params.svgId,
          width: 1000,
          height: 1000
        })
      }
      this.params.lesMArque = []
      this.params.mtgApplecteur.removeAllDoc()
      this.params.mtgApplecteur.addDoc(this.params.svgId, this.figure, true)
      this.params.mtgApplecteur.setApiDoc(this.params.svgId)
      this.params.mtgApplecteur.calculateFirstTime(this.params.svgId)
      const p1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'A' })
      const p2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'B' })
      this.params.longUnit = this.dist(p1, p2)
      this.params.mtgApplecteur.addFreePoint({
        absCoord: true,
        x: p1.x,
        y: p1.y + 5,
        name: 'E',
        tag: 'E',
        hidden: true
      })
      this.params.mtgApplecteur.addFreePoint({
        absCoord: true,
        x: p1.x,
        y: p1.y - 5,
        name: 'H',
        tag: 'H',
        hidden: true
      })
      this.params.mtgApplecteur.addFreePoint({
        absCoord: true,
        x: p2.x,
        y: p1.y + 5,
        name: 'G',
        tag: 'G',
        hidden: true
      })
      this.params.mtgApplecteur.addFreePoint({
        absCoord: true,
        x: p2.x,
        y: p1.y - 5,
        name: 'F',
        tag: 'F',
        hidden: true
      })
      this.params.mtgApplecteur.addFreePoint({
        absCoord: true,
        x: (p2.x + p1.x) / 2,
        y: p1.y - 10,
        name: 'D',
        tag: 'D',
        hidden: true
      })
      this.params.mtgApplecteur.addSegment({
        a: 'A',
        b: 'B'
      })
      this.params.mtgApplecteur.addSegment({
        a: 'E',
        b: 'H'
      })
      this.params.mtgApplecteur.addSegment({
        a: 'G',
        b: 'F'
      })
      this.params.mtgApplecteur.addLinkedText({
        a: 'D',
        text: '1',
        hAlign: 'center',
        vAlign: 'middle',
        transparent: true,
        color: 'purple'
      })
      if (!this.params.squizzEssai) {
        this.params.lesMArque2 = []
        this.params.svgId2 = j3pGetNewId('modele')
        j3pEmpty(this.params.Model)
        this.params.leSvg2 = j3pCreeSVG(this.params.Model, {
          id: this.params.svgId2,
          width: 385,
          height: 385
        })
        this.params.mtgApplecteur.addDoc(this.params.svgId2, this.figure, true)
        this.params.mtgApplecteur.setApiDoc(this.params.svgId2)
        this.params.mtgApplecteur.calculateFirstTime(this.params.svgId2)
        this.params.mtgApplecteur.addFreePoint({
          absCoord: true,
          x: p1.x,
          y: p1.y + 5,
          name: 'E',
          tag: 'E',
          hidden: true
        })
        this.params.mtgApplecteur.addFreePoint({
          absCoord: true,
          x: p1.x,
          y: p1.y - 5,
          name: 'H',
          tag: 'H',
          hidden: true
        })
        this.params.mtgApplecteur.addFreePoint({
          absCoord: true,
          x: p2.x,
          y: p1.y + 5,
          name: 'G',
          tag: 'G',
          hidden: true
        })
        this.params.mtgApplecteur.addFreePoint({
          absCoord: true,
          x: p2.x,
          y: p1.y - 5,
          name: 'F',
          tag: 'F',
          hidden: true
        })
        this.params.mtgApplecteur.addFreePoint({
          absCoord: true,
          x: (p2.x + p1.x) / 2,
          y: p1.y - 10,
          name: 'D',
          tag: 'D',
          hidden: true
        })
        this.params.mtgApplecteur.addSegment({
          a: 'A',
          b: 'B'
        })
        this.params.mtgApplecteur.addSegment({
          a: 'E',
          b: 'H'
        })
        this.params.mtgApplecteur.addSegment({
          a: 'G',
          b: 'F'
        })
        this.params.mtgApplecteur.addLinkedText({
          a: 'D',
          text: '1',
          hAlign: 'center',
          vAlign: 'middle',
          transparent: true,
          color: 'purple'
        })
        this.params.donnesMtg2 = []
        this.params.tagList2 = []
        if (!this.params.isEditing) {
          this.params.lesMArque3 = []
          this.params.svgId3 = j3pGetNewId('modelebase')
          j3pEmpty(this.params.Base)
          this.params.leSvg3 = j3pCreeSVG(this.params.Base, {
            id: this.params.svgId3,
            width: 385,
            height: 385
          })
          this.params.mtgApplecteur.addDoc(this.params.svgId3, this.figure, true)
          this.params.mtgApplecteur.setApiDoc(this.params.svgId3)
          this.params.mtgApplecteur.calculateFirstTime(this.params.svgId3)
          this.params.mtgApplecteur.addFreePoint({
            absCoord: true,
            x: p1.x,
            y: p1.y + 5,
            name: 'E',
            tag: 'E',
            hidden: true
          })
          this.params.mtgApplecteur.addFreePoint({
            absCoord: true,
            x: p1.x,
            y: p1.y - 5,
            name: 'H',
            tag: 'H',
            hidden: true
          })
          this.params.mtgApplecteur.addFreePoint({
            absCoord: true,
            x: p2.x,
            y: p1.y + 5,
            name: 'G',
            tag: 'G',
            hidden: true
          })
          this.params.mtgApplecteur.addFreePoint({
            absCoord: true,
            x: p2.x,
            y: p1.y - 5,
            name: 'F',
            tag: 'F',
            hidden: true
          })
          this.params.mtgApplecteur.addFreePoint({
            absCoord: true,
            x: (p2.x + p1.x) / 2,
            y: p1.y - 10,
            name: 'D',
            tag: 'D',
            hidden: true
          })
          this.params.mtgApplecteur.addSegment({
            a: 'A',
            b: 'B'
          })
          this.params.mtgApplecteur.addSegment({
            a: 'E',
            b: 'H'
          })
          this.params.mtgApplecteur.addSegment({
            a: 'G',
            b: 'F'
          })
          this.params.mtgApplecteur.addLinkedText({
            a: 'D',
            text: '1',
            hAlign: 'center',
            vAlign: 'middle',
            transparent: true,
            color: 'purple'
          })
          this.params.donnesMtg3 = []
          this.params.tagList3 = []
        }
      }
      this.params.foraz = false
      this.params.donnesMtg = []
      this.params.tagList1 = []

      this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId, 'mousemove', this._mtgSuite.bind(this, 1, false))
      this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId, 'touchmove', this._mtgSuite.bind(this, 1, false))
      this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId, 'mouseup', this._mtgSuite.bind(this, 1, true))
      this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId, 'touchend', this._mtgSuite.bind(this, 1, true))
      // on ne peut pas mettre de bind ici car _mtgOnDown est affecté / retiré dynamiquement
      this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId, 'mousedown', (ev) => { this._mtgOnDown(ev) })
      this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId, 'touchstart', (ev) => {
        const ev2 = { clientX: ev.touches[0].clientX ?? undefined, clientY: ev.touches[0].clientY ?? undefined }
        if (ev2.clientX && ev2.clientY) this._mtgOnDown(ev2)
      })

      if (!this.params.squizzEssai) {
        const suiteListener = this._mtgSuite.bind(this, 2, false)
        this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId2, 'mousemove', suiteListener)
        this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId2, 'touchmove', suiteListener)
        // FIXME il manque pas un true en 2e param ? sinon expliquer pourquoi…
        this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId2, 'mouseup', suiteListener)
        this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId2, 'touchend', suiteListener)
        if (!this.params.isEditing) {
          const suiteListener = this._mtgSuite.bind(this, 3, false)
          this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId3, 'mousemove', suiteListener)
          this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId3, 'touchmove', suiteListener)
          this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId3, 'mouseup', suiteListener)
          this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId3, 'touchend', suiteListener)
        } else {
          // pas de bind car _mtgOnDown2 change dynamiquement
          this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId2, 'mousedown', (ev) => { this._mtgOnDown2(ev) })
          this.params.mtgApplecteur.addCallBackToSVGListener(this.params.svgId2, 'touchstart', (ev) => {
            const ev2 = { clientX: ev.touches[0].clientX ?? undefined, clientY: ev.touches[0].clientY ?? undefined }
            if (ev2.clientX && ev2.clientY) this._mtgOnDown2(ev2)
          })
        }
        const tabLettreAvire = []
        for (let i = 0; i < this.params.MtgProg.suite.length; i++) {
          if (this.params.MtgProg.suite[i].type === 'pointLibreAvecNom' ||
              this.params.MtgProg.suite[i].type === 'pointIntersection' ||
              this.params.MtgProg.suite[i].type === 'pointSym' ||
              this.params.MtgProg.suite[i].type === 'pointRot' ||
              this.params.MtgProg.suite[i].type === 'pointSur' ||
              this.params.MtgProg.suite[i].type === 'pointMilieu') {
            tabLettreAvire.push(this.params.MtgProg.suite[i].noms[0])
          }
        }
        this._nettoieLettres(tabLettreAvire)
      }
      // const p1 = this.params.mtgApplecteur.getPointPosition(this.params.svgId, '#PU1')
      // const p2 = this.params.mtgApplecteur.getPointPosition(this.params.svgId, '#PU2')
      // this.params.longUnit = Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y))
      // ces fonctions sont affectées / retirées dynamiquement
      this._mtgOnMove = dummyFn
      this._mtgOnDown = dummyFn
      this._mtgOnDown2 = dummyFn
      const tabAcache = []
      let yaAngleOuArc = false
      for (let i = 0; i < this.params.MtgProg.figdep.length; i++) {
        switch (this.params.MtgProg.figdep[i].type) {
          case 'zone':
            this.scratchFaisZone(1, this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisZone(2, this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisZone(3, this.params.MtgProg.figdep[i])
            break
          case 'zoneSeg':
            this.scratchFaisZoneSeg(1, this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisZoneSeg(2, this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisZoneSeg(3, this.params.MtgProg.figdep[i])
            break
          case 'point':
            if (!this.params.squizzEssai) this.scratchFaisPoint(2, this.params.MtgProg.figdep[i], true)
            this.scratchFaisPoint(1, this.params.MtgProg.figdep[i], true)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisPoint(3, this.params.MtgProg.figdep[i], true)
            break
          case 'segment': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisSegment(2, eel, true)
            this.scratchFaisSegment(1, eel, true)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisSegment(3, eel, true)
            break
          }
          case 'pointSur': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisPointPointSur(2, eel, true)
            this.scratchFaisPointPointSur(1, eel, true)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisPointPointSur(3, eel, true)
          }
            break
          case 'pointLibreAvecNom': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisPoint(2, eel, false)
            this.scratchFaisPoint(1, eel, false)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisPoint(3, eel, false)
          }
            break
          case 'pointMilieu': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisPointPointMilieu(2, eel, false)
            this.scratchFaisPointPointMilieu(1, eel, false)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisPointPointMilieu(3, eel, false)
          }
            break
          case 'pointSym': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisPointSym(2, eel, false)
            this.scratchFaisPointSym(1, eel, false)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisPointSym(3, eel, false)
          }
            break
          case 'pointRot': {
            yaAngleOuArc = true
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisPointRot(2, eel, false)
            this.scratchFaisPointRot(1, eel, false)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisPointRot(3, eel, false)
          }
            break
          case 'pointIntersection': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisPointPointInter(2, eel, false)
            this.scratchFaisPointPointInter(1, eel, false)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisPointPointInter(3, eel, false)
          }
            break
          case 'segmentlongA': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisSegmentLongA(2, eel, false)
            this.scratchFaisSegmentLongA(1, eel, false)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisSegmentLongA(3, eel, false)
          }
            break
          case 'droitept': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisDroitept(2, eel, true)
            this.scratchFaisDroitept(1, eel, true)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisDroitept(3, eel, true)
            break
          }
          case 'droitepara': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisDroitePara(2, eel, true)
            this.scratchFaisDroitePara(1, eel, true)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisDroitePara(3, eel, true)
            break
          }
          case 'droiteperp': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisDroitePerp(2, eel, true)
            this.scratchFaisDroitePerp(1, eel, true)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisDroitePerp(3, eel, true)
            break
          }
          case 'demidroitept': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisDemiDroitept(2, eel, true)
            this.scratchFaisDemiDroitept(1, eel, true)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisDemiDroitept(3, eel, true)
            break
          }
          case 'cercleCentrePoint': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisCercleCentrePoint(2, eel, true)
            this.scratchFaisCercleCentrePoint(1, eel, true)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisCercleCentrePoint(3, eel, true)
            break
          }
          case 'cercleCentreRayon': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisCercleCentreRayon(2, eel, true)
            this.scratchFaisCercleCentreRayon(1, eel, true)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisCercleCentreRayon(3, eel, true)
            break
          }
          case 'arc':
            {
              yaAngleOuArc = true
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
              if (!this.params.squizzEssai) this.scratchFaisArcCentre(2, eel, false)
              this.scratchFaisArcCentre(1, eel, false)
              if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisArcCentre(3, eel, false)
            }
            break
          case 'Marque': {
            const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.figdep[i])
            if (!this.params.squizzEssai) this.scratchFaisMarque(2, eel, true)
            this.scratchFaisMarque(1, eel, true)
            if (!this.params.squizzEssai && !this.params.isEditing) this.scratchFaisMarque(3, eel, true)
            break
          }
        }
        if (this.params.MtgProg.figdep[i].debCache) tabAcache.push(this.params.MtgProg.figdep[i])
      }
      for (let i = 0; i < tabAcache.length; i++) {
        if (!this.params.squizzEssai) {
          const numeel = this.scratchMtgRetrouveBase(2, { nom: this.scratchMtgReplaceLettres(tabAcache[i]).noms[0] }, 'num')
          this.mtgEffacer(2, numeel, false)
        }
        const numeel = this.scratchMtgRetrouveBase(1, { nom: this.scratchMtgReplaceLettres(tabAcache[i]).noms[0] }, 'num')
        this.mtgEffacer(1, numeel, false)
        if (!this.params.squizzEssai && !this.params.isEditing) {
          const numeel = this.scratchMtgRetrouveBase(3, { nom: this.scratchMtgReplaceLettres(tabAcache[i]).noms[0] }, 'num')
          this.mtgEffacer(3, numeel, false)
        }
      }
      if (!this.params.squizzEssai && !this.params.isEditing) {
        for (let i = 0; i < this.params.MtgProg.suite.length; i++) {
          switch (this.params.MtgProg.suite[i].type) {
            case 'pointLibreAvecNom':
            case 'point': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisPoint(2, eel, false)
            }
              break
            case 'pointMilieu': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisPointPointMilieu(2, eel)
            }
              break
            case 'pointIntersection':{
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisPointPointInter(2, eel)
            }
              break
            case 'pointSur':{
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisPointPointSur(2, eel)
            }
              break
            case 'segment': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisSegment(2, eel)
            }
              break
            case 'segmentlongA': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisSegmentLongA(2, eel)
            }
              break
            case 'droitept': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisDroitept(2, eel)
            }
              break
            case 'demidroitept': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisDemiDroitept(2, eel)
            }
              break
            case 'droitepara': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisDroitePara(2, eel)
            }
              break
            case 'droiteperp': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisDroitePerp(2, eel)
            }
              break
            case 'cercleCentrePoint': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisCercleCentrePoint(2, eel)
            }
              break
            case 'cercleCentreRayon': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisCercleCentreRayon(2, eel)
            }
              break
            case 'arc1PointPasse': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.scratchFaisArc1PointPasse(2, eel)
            }
              break
            case 'arccentreplus': {
              yaAngleOuArc = true
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              eel.sens = 1
              this.scratchFaisArcCentre(2, eel)
            }
              break
            case 'arccentremoins': {
              yaAngleOuArc = true
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              eel.sens = 0
              this.scratchFaisArcCentre(2, eel)
            }
              break
            case 'mtgEffacer': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.mtgEffacer(2, this.scratchMtgRetrouveBase(2, { nom: eel.noms[0] }, 'num'))
            }
              break
            case 'mtgPointille': {
              const eel = this.scratchMtgReplaceLettres(this.params.MtgProg.suite[i])
              this.chgStyle(2, this.scratchMtgRetrouveBase(2, { nom: eel.noms[0] }, 'num'))
            }
              break
          }
        }
        if (this.params.MtgProg.text) {
          const ttext = this.scratchMtgReplaceLettres2(this.params.MtgProg.text)
          j3pEmpty(this.params.ModelText)
          j3pAffiche(this.params.ModelText, null, ttext)
          this.params.ModelText.style.background = '#eee'
        }
      }
      if (this.params.MtgProg.placeDep && !this.params.isEditing) {
        this.params.mtgRotation = (this.params.mtgRotation) || j3pGetRandomInt(45, 325) * Math.PI / 180
        this.params.mtgSym = (this.params.mtgSym) || j3pGetRandomInt(1, 2)
        if (yaAngleOuArc) this.params.mtgSym = 2
        if (this.params.mtgSym === 1) {
          this.params.mtgApplecteur.setApiDoc(this.params.svgId)
          for (let i = 0; i < this.params.donnesMtg.length; i++) {
            if (this.params.donnesMtg[i].fix) {
              this.params.donnesMtg[i].cooGarde.x = 385 - this.params.donnesMtg[i].cooGarde.x
              this.params.mtgApplecteur.setPointPosition(this.params.svgId, this.params.donnesMtg[i].tag, this.params.donnesMtg[i].cooGarde.x, this.params.donnesMtg[i].cooGarde.y, true)
            }
          }
        }
        for (let i = 0; i < this.params.donnesMtg.length; i++) {
          if (this.params.donnesMtg[i].fix) {
            const xM = this.params.donnesMtg[i].cooGarde.x - 192.5
            const yM = this.params.donnesMtg[i].cooGarde.y - 192.5
            this.params.donnesMtg[i].cooGarde.x = xM * Math.cos(this.params.mtgRotation) + yM * Math.sin(this.params.mtgRotation) + 192.5
            this.params.donnesMtg[i].cooGarde.y = -xM * Math.sin(this.params.mtgRotation) + yM * Math.cos(this.params.mtgRotation) + 192.5
            this.params.mtgApplecteur.setPointPosition(this.params.svgId, this.params.donnesMtg[i].tag, this.params.donnesMtg[i].cooGarde.x, this.params.donnesMtg[i].cooGarde.y, true)
            this.params.donnesMtg3[i].cooGarde = j3pClone(this.params.donnesMtg[i].cooGarde)
          }
        }
      }
      if (this.params.donnesMtg2) {
        this.params.mtgApplecteur.setApiDoc(this.params.svgId2)
        for (let i = 0; i < this.params.donnesMtg2.length; i++) {
          if (this.params.donnesMtg2[i].type === 'point') {
            if (!this.params.donnesMtg2[i].efface) this.params.mtgApplecteur.addDuplicatedObject({ elt: this.params.donnesMtg2[i].tag })
          }
          this.verifMarque(this.params.donnesMtg2[i], 2)
        }
        this._mtgSuite(2, true, {})
      }
      if (this.params.donnesMtg3) {
        this.params.mtgApplecteur.setApiDoc(this.params.svgId3)
        for (let i = 0; i < this.params.donnesMtg3.length; i++) {
          if (this.params.donnesMtg3[i].type === 'point') {
            if (!this.params.donnesMtg3[i].efface) this.params.mtgApplecteur.addDuplicatedObject({ elt: this.params.donnesMtg3[i].tag })
          }
          this.verifMarque(this.params.donnesMtg3[i], 3)
        }
      }
      if (!options.blokDuplik) {
        this.params.mtgApplecteur.setApiDoc(this.params.svgId)
        for (let i = 0; i < this.params.donnesMtg.length; i++) {
          if (this.params.donnesMtg[i].type === 'point') {
            if (!this.params.donnesMtg[i].efface) this.params.mtgApplecteur.addDuplicatedObject({ elt: this.params.donnesMtg[i].tag })
          }
          this.verifMarque(this.params.donnesMtg[i], 1)
        }
      }
      this._mtgSuite(1, true, {})
      if (!this.params.squizzEssai && !this.params.isEditing) this._mtgSuite(3, true, {})
      if (!this.params.isEditing) this.scratchMenuPerso()
    }
  } // traceSortieBase

  verifMarque (ob, num) {
    let lesMArque, svgId
    switch (num) {
      case 1: lesMArque = this.params.lesMArque
        svgId = this.params.svgId
        break
      case 2: lesMArque = this.params.lesMArque2
        svgId = this.params.svgId2
        break
      case 3: lesMArque = this.params.lesMArque3
        svgId = this.params.svgId3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    if (ob.type.indexOf('segment') !== -1) {
      for (let j = 0; j < lesMArque.length; j++) {
        if (lesMArque[j].elt === ob.tag) continue
        const acomp1 = this.params.mtgApplecteur.valueOf(svgId, ob.longueur)
        const acomp2 = this.params.mtgApplecteur.valueOf(svgId, lesMArque[j].calcul)
        if (Math.abs(acomp1 - acomp2) < 0.00001) {
          const lam = lesMArque[j]
          this.params.mtgApplecteur.addSegmentMark({
            elt: ob.tag,
            color: lam.marque.color,
            segmentMarkStyle: lam.marque.segmentMarkStyle,
            thickness: 3
          })
          if (ob.toShow.length > 1) {
            this.params.mtgApplecteur.setText(svgId, '#' + ob.toShow.pop(), ' ')
          }
        }
      }
    }
    if (ob.type.indexOf('cercle') !== -1 && ob.rayon) {
      for (let j = 0; j < lesMArque.length; j++) {
        if (lesMArque[j].elt === ob.tag) continue
        const acomp1 = this.params.mtgApplecteur.valueOf(this.params.svgId2, ob.rayon)
        const acomp2 = this.params.mtgApplecteur.valueOf(this.params.svgId2, lesMArque[j].calcul)
        if (Math.abs(acomp1 - acomp2) < 0.00001) {
          const lam = lesMArque[j]
          this.params.mtgApplecteur.addSegmentMark({
            elt: ob.toShow[1],
            color: lam.marque.color,
            segmentMarkStyle: lam.marque.segmentMarkStyle,
            thickness: 3
          })
          this.params.mtgApplecteur.setText(svgId, '#' + ob.toShow[3], ' ')
        }
      }
    }
  }

  retrouveCooDans2 (don) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId2)
    const tag = this.scratchMtgRetrouveBase(2, { nom: don }, 'tag', true)
    const aret = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tag })
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    return aret
  }

  verifCestBien (newob) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    switch (newob.type) {
      case 'pointMilieu': {
        const tag1 = this.scratchMtgRetrouve({ nom: newob.noms[0] }, 'tag')
        const tag2 = this.scratchMtgRetrouve({ nom: newob.cont[0] }, 'tag')
        const tag3 = this.scratchMtgRetrouve({ nom: newob.cont[1] }, 'tag')
        const de1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tag1 })
        const de2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tag2 })
        const de3 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tag3 })
        const de4 = { x: (de2.x + de3.x) / 2, y: (de2.y + de3.y) / 2 }
        if (this.dist(de1, de4) > 0.01) return { ok: false, mess: newob.noms[0] + ' doit être le milieu de [' + newob.cont[0] + newob.cont[1] + '] !' }
      }
        break
      case 'pointSur': {
        const letypecont = this.scratchMtgRetrouve({ nom: newob.cont }, 'type')
        switch (letypecont) {
          case 'segment': {
            const lob = this.scratchMtgRetrouve({ nom: newob.noms[0] }, 'objet')
            if (lob.sur.length !== 1) return { ok: false, mess: newob.noms[0] + ' doit être un point quelconque de ' + newob.cont + ' !' }
            const surki = lob.sur[0]
            if (surki !== this.scratchMtgRetrouve({ nom: newob.cont }, 'num')) return { ok: false, mess: newob.noms[0] + ' doit être un point quelconque de ' + newob.cont + ' !' }
          }
            break
          case 'demidroite':
          case 'cercle':
          case 'droite': {
            const lob = this.scratchMtgRetrouve({ nom: newob.noms[0] }, 'objet')
            if (lob.sur.length !== 1) return { ok: false, mess: newob.noms[0] + ' doit être un point quelconque de ' + newob.cont + ' !' }
            const surki = this.params.donnesMtg[lob.sur[0]]
            if (surki.noms.indexOf(newob.cont) === -1) return { ok: false, mess: newob.noms[0] + ' doit être un point quelconque de ' + newob.cont + ' !' }
          }
            break
        }
      }
        break
      case 'pointIntersection': {
        const tag1 = this.scratchMtgRetrouve({ nom: newob.noms[0] }, 'tag')
        const de1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tag1 })
        const ob1 = this.scratchMtgRetrouveGrave2({ nom: newob.cont1 }, 'objet')
        const ob2 = this.scratchMtgRetrouveGrave2({ nom: newob.cont2 }, 'objet')
        const errSpe = (!ob1 || !ob2)
        const clone1 = this.dupplique(ob1)
        const clone2 = this.dupplique(ob2)

        const tatag = this.scratchMtgNewTag(1)
        const tatag2 = this.scratchMtgNewTag(1)
        const InterPos = []
        try {
          if (ob1.type === 'arc' || ob1.type === 'cercle') {
            if (ob2.type === 'arc' || ob2.type === 'cercle') {
              this.params.mtgApplecteur.addIntCircleCircle({
                c: clone1,
                c2: clone2,
                hidden: false,
                name: tatag,
                tag: tatag,
                name2: tatag2,
                tag2: tatag2,
                hiddenName: true,
                color: 'red',
                pointStyle: 'bigmult',
                smartIntersect: false
              })
            } else {
              this.params.mtgApplecteur.addIntLineCircle({
                c: clone1,
                d: clone2,
                hidden: false,
                name: tatag,
                tag: tatag,
                name2: tatag2,
                tag2: tatag2,
                hiddenName: true,
                color: 'red',
                pointStyle: 'bigmult',
                smartIntersect: false
              })
            }
            InterPos.push(tatag, tatag2)
          } else {
            if (ob2.type === 'arc' || ob2.type === 'cercle') {
              this.params.mtgApplecteur.addIntLineCircle({
                d: clone1,
                c: clone2,
                hidden: false,
                name: tatag,
                tag: tatag,
                name2: tatag2,
                tag2: tatag2,
                hiddenName: true,
                color: 'red',
                pointStyle: 'bigmult',
                smartIntersect: false
              })
              InterPos.push(tatag, tatag2)
            } else {
              this.params.mtgApplecteur.addIntLineLine({
                d: clone1,
                d2: clone2,
                hidden: false,
                name: tatag,
                tag: tatag,
                hiddenName: true,
                color: 'red',
                pointStyle: 'bigmult',
                smartIntersect: false
              })
              InterPos.push(tatag)
            }
          }
        } catch (e) {
          return { ok: false, mess: newob.noms[0] + ' doit appartenir à ' + newob.cont1 + ' et ' + newob.cont2 + ' !' }
        }

        let ok = false
        for (let i = InterPos.length - 1; i > -1; i--) {
          const place = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: InterPos[i] })
          if (this.dist(place, de1) < 0.0001) ok = true
          try {
            this.params.mtgApplecteur.deleteElt({ elt: InterPos[i] })
          } catch (e) {
            // eslint-disable-next-line no-console
            console.log(e)
          }
        }
        this.params.mtgApplecteur.deleteElt({ elt: clone1 })
        this.params.mtgApplecteur.deleteElt({ elt: clone2 })
        if (!ok) {
          if (!errSpe) {
            return { ok: false, mess: newob.noms[0] + ' doit appartenir à ' + newob.cont1 + ' et ' + newob.cont2 + ' !' }
          } else {
            return { ok: false, mess: 'Le point ' + newob.noms[0] + ' est mal placé !' }
          }
        }
      }
        break
      case 'pointLibreAvecNom':
        if (this.scratchMtgRetrouve({ nom: newob.noms[0] }, 'type') !== 'pointLibreAvecNom') return { ok: false, mess: newob.noms[0] + ' doit être un point quelconque !' }
        break
      case 'segmentlongA': {
        let an = newob.noms[0].replace('[', '').replace(']', '')
        an = this.scratchDecomposeEnPoints(an)
        const ext2 = an[1]
        const ext1 = an[0]
        const tag1 = this.scratchMtgRetrouve({ nom: ext1 }, 'tag')
        const tag2 = this.scratchMtgRetrouve({ nom: ext2 }, 'tag')
        const de1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tag1 })
        const de2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tag2 })
        const didi = this.dist(de1, de2)
        const dicompp = Number(newob.long)
        const p1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'A' })
        const p2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: 'B' })
        this.params.longUnit = this.dist(p1, p2)
        if (Math.abs(didi - dicompp * this.params.longUnit) > 0.001) return { ok: false, mess: 'La longueur ' + ext1 + ext2 + ' doit être égale à ' + newob.long + ' unités !' }
      }
        break
      case 'cercleCentrePoint': {
        const don = this.scratchMtgRetrouve({ nom: newob.noms[0] }, 'objet')
        let de1
        try {
          if (this.scratchMtgRetrouve({ nom: newob.centre }, 'tag') === undefined) {
            de1 = this.retrouveCooDans2(newob.centre)
          } else {
            de1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouve({ nom: newob.centre }, 'tag') })
          }

          const de2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouve({ nom: don.centre }, 'tag', true) })

          if (this.dist(de1, de2) > 0.001) return { ok: false, mess: 'Le centre du cercle ' + newob.noms[0] + ' est incorrect !' }

          if (don.point) {
            if (don.point !== newob.point) {
              let de4
              if (this.scratchMtgRetrouve({ nom: newob.point }, 'tag') === undefined) {
                de4 = this.retrouveCooDans2(newob.centre)
              } else {
                de4 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouve({ nom: newob.point }, 'tag') })
              }
              const de3 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouve({ nom: don.point }, 'tag') })
              if (Math.abs(this.dist(de1, de4) - this.dist(de1, de3)) > 0.0001) return { ok: false, mess: 'Le cercle ' + newob.noms[0] + ' doit passer par le point ' + newob.point + ' !' }
            }
          } else {
            if (don.rayon) {
              let de4
              if (this.scratchMtgRetrouve({ nom: newob.point }, 'tag') === undefined) {
                de4 = this.retrouveCooDans2(newob.centre)
              } else {
                de4 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouve({ nom: newob.point }, 'tag') })
              }
              const cal2Tag = this.calculeMG(don.rayon, 1)
              const cal2 = this.params.mtgApplecteur.valueOf(this.params.svgId, cal2Tag)
              if (Math.abs(this.dist(de1, de4) - cal2 * this.params.longUnit) > 0.0001) return { ok: false, mess: 'Le cercle ' + newob.noms[0] + ' doit passer par le point ' + newob.point + ' !' }
            } else {
              return { ok: false, mess: 'Il y a un problème avec un cercle.' }
            }
          }
        } catch (e) {
          j3pNotify(e, { don, newob })
          return { ok: false, mess: 'Il y a un problème avec un cercle.' }
        }
      }
        break
      case 'cercleCentreRayon': {
        const don = this.scratchMtgRetrouve({ nom: newob.noms[0] }, 'objet')
        if (don.centre !== newob.centre) return { ok: false, mess: newob.centre + ' doit être le centre du cercle ' + newob.noms[0] + ' !' }
        const cal1 = this.params.mtgApplecteur.valueOf(this.params.svgId, don.rayon)
        const cal2Tag = this.calculeMG(newob.rayon, 1)
        const cal2 = this.params.mtgApplecteur.valueOf(this.params.svgId, cal2Tag)
        if (cal1 !== cal2) return { ok: false, mess: 'Le rayon de ' + newob.noms[0] + ' doit être égal à ' + newob.rayon + ' !' }
      }
        break
      case 'droitepara': {
        const ob1 = this.scratchMtgRetrouve({ nom: newob.noms[0] }, 'objet')
        const obpara = this.scratchMtgRetrouve({ nom: newob.para }, 'objet')
        const de1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: ob1.tag1 })
        const de2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: ob1.tag2 })
        const de3 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: obpara.tag1 })
        const de4 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: obpara.tag2 })
        const v1 = { x: de2.x - de1.x, y: de2.y - de1.y }
        const v2 = { x: de4.x - de3.x, y: de4.y - de3.y }
        const pv = v1.x * v2.y - v2.x * v1.y
        if (Math.abs(pv) > 0.0001) return { ok: false, mess: 'Les droites ' + ob1.noms[0] + ' et ' + obpara.noms[0] + ' doivent être parallèles !' }
        const de5 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouve({ nom: newob.point }, 'tag') })
        const v3 = { x: de5.x - de1.x, y: de5.y - de1.y }
        const pv2 = v1.x * v3.y - v3.x * v1.y
        if (Math.abs(pv2) > 0.0001) return { ok: false, mess: 'La droite ' + ob1.noms[0] + ' doit passer par le point ' + newob.point + ' !' }
      }
        break
      case 'droiteperp': {
        const ob1 = this.scratchMtgRetrouve({ nom: newob.noms[0] }, 'objet')
        const obpara = this.scratchMtgRetrouve({ nom: newob.perp }, 'objet')
        const obpoint = this.scratchMtgRetrouve({ nom: newob.point }, 'tag')
        const errSpe = (!obpoint)
        let de5
        if (!obpoint) {
          const deyy = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: obpara.tag1 })
          const dezz = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: obpara.tag2 })
          de5 = { x: (deyy.x + dezz.x) / 2, y: (deyy.y + dezz.y) / 2 }
        } else {
          de5 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: obpoint })
        }
        const de1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: ob1.tag1 })
        const de2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: ob1.tag2 })
        const de3 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: obpara.tag1 })
        const de4 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: obpara.tag2 })
        const v1 = { x: de2.x - de1.x, y: de2.y - de1.y }
        const v2 = { x: de4.x - de3.x, y: de4.y - de3.y }
        const ps = v1.x * v2.x + v2.y * v1.y
        if (Math.abs(ps) > 0.0001) return { ok: false, mess: 'Les droites ' + ob1.noms[0] + ' et ' + obpara.noms[0] + ' doivent être perpendiculaires !' }
        const v3 = { x: de5.x - de1.x, y: de5.y - de1.y }
        const pv2 = v1.x * v3.y - v3.x * v1.y
        if (Math.abs(pv2) > 0.0001) {
          if (!errSpe) {
            return { ok: false, mess: 'La droite ' + ob1.noms[0] + ' doit passer par le point ' + newob.point + ' !' }
          } else {
            return { ok: false, mess: 'La droite ' + ob1.noms[0] + ' n’est pas correctement construite !' }
          }
        }
      }
        break
      case 'arc1PointPasse':
      case 'arccentreplus':
      case 'arccentremoins': {
        const don = this.scratchMtgRetrouve({ nom: 'arc ' + newob.noms[0] }, 'objet')
        if (don.centre !== newob.centre) return { ok: false, mess: newob.centre + ' doit être le centre de l’arc ' + newob.noms[0] + ' !' }
        let okEx = true
        if (don.ext1 !== newob.noms[0][0] && don.ext1 !== newob.noms[0][1]) okEx = false
        if (don.ext2 !== newob.noms[0][0] && don.ext2 !== newob.noms[0][1]) okEx = false
        if (!okEx) return { ok: false, mess: 'L’arc ' + newob.noms[0] + ' doit avoir ' + newob.noms[0][1] + ' et ' + newob.noms[0][0] + ' pour extrémitès !' }
        // verfi le bon
        const extPlus = this.metPlus(don)
        const extPlusNew = this.metPlusN(newob)
        if (extPlus !== extPlusNew) return { ok: false, mess: 'L’arc ' + newob.noms[0] + ' n’est pas tracé dans le bon sens !' }
      }
    }
    return { ok: true, mes: '' }
  }

  metPlus (ob) {
    return (ob.sens) ? ob.ext1 : ob.ext2
  }

  metPlusN (ob) {
    return (ob.type === 'arccentreplus') ? ob.noms[0][0] : ob.noms[0][1]
  }

  dupplique (ob) {
    const tagaRet = this.scratchMtgNewTag(1)
    switch (ob.type) {
      case 'segment':
        this.params.mtgApplecteur.addSegment({
          a: this.scratchMtgRetrouve({ nom: ob.ext1 }, 'tag'),
          b: this.scratchMtgRetrouve({ nom: ob.ext2 }, 'tag'),
          thickness: 2,
          tag: tagaRet,
          color: 'black'
        })
        break
      case 'droite':
        if (ob.point1) {
          this.params.mtgApplecteur.addLineAB({
            a: this.scratchMtgRetrouve({ nom: ob.point1 }, 'tag'),
            b: this.scratchMtgRetrouve({ nom: ob.point2 }, 'tag'),
            thickness: 2,
            tag: tagaRet,
            color: 'black'
          })
        }
        if (ob.para) {
          this.params.mtgApplecteur.addLinePar({
            d: this.scratchMtgRetrouve({ nom: ob.para }, 'tag'),
            a: this.scratchMtgRetrouve({ nom: ob.ext1 }, 'tag'),
            tag: tagaRet,
            hidden: true
          })
        }
        if (ob.perp) {
          this.params.mtgApplecteur.addLinePerp({
            d: this.scratchMtgRetrouve({ nom: ob.perp }, 'tag'),
            a: this.scratchMtgRetrouve({ nom: ob.ext1 }, 'tag'),
            tag: tagaRet,
            hidden: true
          })
        }
        break
      case 'demidroite':
        this.params.mtgApplecteur.addRay({
          o: this.scratchMtgRetrouve({ nom: ob.origine }, 'tag'),
          a: this.scratchMtgRetrouve({ nom: ob.point }, 'tag'),
          thickness: 2,
          tag: tagaRet,
          color: 'black'
        })
        break
      case 'cercle':
        if (ob.point) {
          this.params.mtgApplecteur.addCircleOA({
            o: this.scratchMtgRetrouve({ nom: ob.centre }, 'tag'),
            a: this.scratchMtgRetrouve({ nom: ob.point }, 'tag'),
            thickness: 2,
            tag: tagaRet,
            color: 'black'
          })
        } else {
          const recupe = this.params.mtgApplecteur.getFormula(this.params.svgId2, ob.rayon)
          this.params.mtgApplecteur.addCircleOr({
            o: this.scratchMtgRetrouve({ nom: ob.centre }, 'tag'),
            r: recupe,
            tag: tagaRet
          })
        }
        break
    }
    return tagaRet
  }

  scratchMtgReplaceLettresInv (n) {
    let aret = n
    for (let i = 0; i < this.params.lettres.length; i++) {
      const ar = this.params.lettres[i]
      aret = aret.replace('#' + (i + 1) + '#', ar)
    }
    return aret
  }

  ceDiesePris (n) {
    for (let i = 0; i < this.params.MtgProg.figdep.length; i++) {
      if (this.params.MtgProg.figdep[i].noms.indexOf('#' + (n + 1) + '#') !== -1) return true
    }
    return false
  }

  scratchMtgReplaceLettresInvVraiment (n) {
    let aret = n
    for (let i = 0; i < this.params.lettres.length; i++) {
      const ar = this.params.lettres[i]
      if (this.ceDiesePris(i)) aret = aret.replace(ar, '#' + (i + 1) + '#')
    }
    return aret
  }

  _nettoieLettres (tab) {
    for (let i = this.params.lettres.length - 1; i > -1; i--) {
      if (tab.indexOf(this.params.lettres[i]) !== -1) this.params.lettres.splice(i, 1)
    }
  }

  /**
   * Retourne une fonction qui renverra true si eel est visible au moment de l’appel
   * @param eel
   * @return {function(): boolean}
   */
  _getIsVisible (eel) {
    return () => eel.visible !== false
  }

  scratchMtgRetrouveFin (obj, quoi) {
    return this.scratchMtgRetrouve(obj, quoi)
  }

  scratchMtgRetrouve (obj, quoi, force) {
    return this.scratchMtgRetrouveBase(1, obj, quoi, force)
  }

  scratchMtgRetrouveGrave (obj, quoi) {
    return this.scratchMtgRetrouveBase(1, obj, quoi, true)
  }

  scratchMtgRetrouveGrave2 (obj, quoi) {
    return this.scratchMtgRetrouveBase(2, obj, quoi, true)
  }

  scratchMtgRetrouveBase (num, obj, quoi, force) {
    let donnesMtg
    switch (num) {
      case 1:
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        donnesMtg = this.params.donnesMtg3
        break
    }
    for (let i = 0; i < donnesMtg.length; i++) {
      if (!force && donnesMtg[i].efface) continue
      let ok = true
      ok = ok && (!obj.nom || (donnesMtg[i].noms.indexOf(obj.nom) !== -1))
      ok = ok && (!obj.centre || (donnesMtg[i].centre === obj.centre))
      ok = ok && (!obj.point || (donnesMtg[i].point === obj.point))
      ok = ok && (!obj.rayon || (donnesMtg[i].point === obj.rayon))
      ok = ok && (!obj.type || (donnesMtg[i].type === obj.type))
      ok = ok && (!obj.sens || (donnesMtg[i].sens === obj.sens))
      ok = ok && (!obj.tag || (donnesMtg[i].toShow[0] === obj.tag))
      /*
      if (obj.nom) {
        if (this.params.donnesMtg[i].noms.indexOf(obj.nom) === -1) ok = false
      }
      if (obj.centre) {
        if (this.params.donnesMtg[i].centre !== obj.centre) ok = false
      }
      */
      if (ok) {
        switch (quoi) {
          case 'objet': return j3pClone(donnesMtg[i])
          case 'tag': return donnesMtg[i].tag
          case 'num': return i
          case 'noms': return j3pClone(donnesMtg[i].noms)
          case 'type': return donnesMtg[i].type
        }
      }
    }
  }

  scratchTestExiste (num, tag) {
    let svgId
    switch (num) {
      case 1:
        svgId = this.params.svgId
        break
      case 2:
        svgId = this.params.svgId2
        break
      case 3:
        svgId = this.params.svgId3
        break
    }
    const lecal = this.params.mtgApplecteur.valueOf(svgId, tag)
    return (lecal !== -1)
  }

  scratchMtgPlaceOk (num, p, force) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    if (p.x < 2) return false
    if (p.y < 2) return false
    if (p.x > this.xMax - 2) return false
    if (p.y > this.yMax - 2) return false
    if (force) return true
    for (let i = 0; i < donnesMtg.length; i++) {
      if (donnesMtg[i].type === 'point' ||
        donnesMtg[i].type === 'pointMilieu' ||
        donnesMtg[i].type === 'pointSym' ||
        donnesMtg[i].type === 'pointRot' ||
        donnesMtg[i].type === 'pointSur' ||
        donnesMtg[i].type === 'pointIntersection' ||
        donnesMtg[i].type === 'pointLibreAvecNom') {
        const acomp = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: donnesMtg[i].tag })
        if (this.dist(p, acomp) < 20) return false
      }
    }
    return true
  }

  scratchChoisz (obj, func, param) {
    const nomaf = (this.params.scratchProgDonnees.nomsAleaBase.indexOf(obj.nom) === -1) ? 'Clique pour choisir ' + obj.nom : 'Clique pour choisir '
    this.scratchMtgMessage(nomaf)
    const adet = []
    const donnesMtg = this.params.donnesMtg
    for (let i = 0; i < donnesMtg.length; i++) {
      let ok = true
      ok = ok && (!obj.nom || (donnesMtg[i].noms.indexOf(obj.nom) !== -1))
      ok = ok && (!obj.centre || (donnesMtg[i].centre === obj.centre))
      ok = ok && (!obj.point || (donnesMtg[i].point === obj.point))
      ok = ok && (!obj.rayon || (donnesMtg[i].point === obj.rayon))
      ok = ok && (!obj.type || (donnesMtg[i].type === obj.type))
      /*
      if (obj.nom) {
        if (this.params.donnesMtg[i].noms.indexOf(obj.nom) === -1) ok = false
      }
      if (obj.centre) {
        if (this.params.donnesMtg[i].centre !== obj.centre) ok = false
      }
      */
      if (ok) adet.push(donnesMtg[i])
    }
    this.params.suitChoisz = {}
    this.params.funcAconsOver = {}
    this.params.funcAconsClique = {}
    this.params.funcAconsOut = {}
    adet.forEach(el => {
      this.params.suitChoisz[el.tag] = false
      this.params.funcAconsOver[el.tag] = () => {
        this.params.suitChoisz[el.tag] = true
        this.params.mtgApplecteur.setColor({ elt: el.tag, color: 'red' })
        this.params.mtgApplecteur.setThickness({ elt: el.tag, thickness: 5 })
      }
      this.params.funcAconsClique[el.tag] = () => {
        this.params.suitChoisz[el.tag] = true
        this.params.param = { ob1: el }
        adet.forEach(um => {
          this.params.mtgApplecteur.setColor({ elt: um.tag, color: 'black' })
          this.params.mtgApplecteur.setThickness({ elt: um.tag, thickness: 2 })
          this.params.mtgApplecteur.removeEltListener({ elt: um.tag, eventName: 'mouseover' })
          this.params.mtgApplecteur.removeEltListener({ elt: um.tag, eventName: 'mouseout' })
          this.params.mtgApplecteur.removeEltListener({ elt: um.tag, eventName: 'click' })
        })
        func()
        //
      }
      this.params.funcAconsOut[el.tag] = () => {
        this.params.suitChoisz[el.tag] = false
        this.params.mtgApplecteur.setColor({ elt: el.tag, color: 'black' })
        this.params.mtgApplecteur.setThickness({ elt: el.tag, thickness: 2 })
      }
      this.params.mtgApplecteur.addEltListener({ elt: el.tag, eventName: 'mouseover', callBack: this.params.funcAconsOver[el.tag] })
      this.params.mtgApplecteur.addEltListener({ elt: el.tag, eventName: 'click', callBack: this.params.funcAconsClique[el.tag] })
      this.params.mtgApplecteur.addEltListener({ elt: el.tag, eventName: 'mouseout', callBack: this.params.funcAconsOut[el.tag] })
    })
  }

  scracthOAegalOB (p1, p2, p3) {
    const de1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: p1 })
    const de2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: p2 })
    const de3 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: p3 })
    const d1 = this.dist(de1, de2)
    const d2 = this.dist(de1, de3)
    return Math.abs(d1 - d2) < 0.01
  }

  scratchMtgPointLibre (nom) {
    this.params.enreG.push('scratchMtgPointLibre avec ' + nom)
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
    while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
    if (!this.params.isEditing && !this.params.isSpeed) {
      const nomaf = (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) ? 'Clique pour placer le point ' + nom : 'Clique pour placer un point '
      this.scratchMtgMessage(nomaf)
      this.params.tabMtg[1][0].style.cursor = 'crosshair'
      this._mtgOnMove = (event) => {
        let lx = event.clientX
        let ly = event.clientY
        if (event.touches) {
          if (event.touches[0]) {
            lx = event.touches[0].clientX
            ly = event.touches[0].clientY
          }
        }
        if (!lx || !ly) return
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        this.params.mtgApplecteur.setPointPosition(this.params.svgId, '#PS', lx - cooSvgId.x, ly - cooSvgId.y, true)
      }
      this._mtgOnDown = (event) => {
        let lx = event.clientX
        let ly = event.clientY
        if (event.touches) {
          if (event.touches[0]) {
            lx = event.touches[0].clientX
            ly = event.touches[0].clientY
          }
        }
        if (!lx || !ly) return
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        const tag1 = this.scratchMtgNewTag(1)
        const tag2 = this.scratchMtgNewTag(1)
        const tag3 = this.scratchMtgNewTag(1)
        const tag4 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addFreePoint({
          absCoord: true,
          x: lx - cooSvgId.x,
          y: ly - cooSvgId.y,
          tag: tag1,
          name: tag1,
          pointStyle: 'bigmult',
          hiddenName: true
        })
        this._mtgOnMove = dummyFn
        this._mtgOnDown = dummyFn
        this.params.tabMtg[1][0].style.cursor = ''
        this.scratchMtgClean()
        const toShow = [tag1]
        console.error(this.params.enreG)
        if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
          try {
            this.tryPlaceNom(tag1, tag2, tag3, tag4, nom, cooSvgId, toShow)
          } catch (e) {
            j3pNotify('Pour tom ' + e, { tabcodex: this.params.scratchProgDonnees.tabcodeX, enreG: this.params.enreG })
          }
        }
        this.params.donnesMtg.push({ type: 'pointLibreAvecNom', tag: tag1, noms: [nom], depend: [], viz: getTrue, toShow, sur: [] })
        this.addPointMesur(tag1, 1)
        this.scratchMtgFinishOrdre()
      }
    } else {
      const cooPointLire = {}
      const tm = this.xMax
      do {
        cooPointLire.x = j3pGetRandomInt(15, tm)
        cooPointLire.y = j3pGetRandomInt(15, tm)
      } while (!this.scratchMtgPlaceOk(1, cooPointLire))
      const tag1 = this.scratchMtgNewTag(1)
      const tag2 = this.scratchMtgNewTag(1)
      const tag3 = this.scratchMtgNewTag(1)
      const tag4 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addFreePoint({
        absCoord: true,
        x: cooPointLire.x,
        y: cooPointLire.y,
        tag: tag1,
        name: tag1,
        pointStyle: 'bigmult',
        hiddenName: true
      })
      this.params.tabMtg[1][0].style.cursor = ''
      const toShow = [tag1]
      if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        this.tryPlaceNom(tag1, tag2, tag3, tag4, nom, cooSvgId, toShow)
      }
      this.params.donnesMtg.push({ type: 'point', tag: tag1, noms: [nom], depend: [], viz: getTrue, toShow, sur: [] })
      this.addPointMesur(tag1, 1)
      this.scratchMtgFinishOrdre()
    }
  }

  scratchMtgPointLong (ptariv, adonext1, long, nomLong) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const tatag2 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
      r: long,
      thickness: 6,
      color: 'green',
      tag: tatag2,
      hidden: true
    })
    if (!this.params.isEditing && !this.params.isSpeed) {
      this.scratchMtgMessage('Place le point ' + ptariv)
      const tatag4 = this.scratchMtgNewTag(1)
      const tatag6 = this.scratchMtgNewTag(1)
      const tatag3 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addRay({
        o: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
        a: 'I',
        tag: tatag3,
        hidden: true
      })
      this.params.mtgApplecteur.addIntLineCircle({
        d: tatag3,
        c: tatag2,
        name: tatag4,
        tag: tatag4,
        pointStyle: 'bigmult',
        color: 'black',
        hidden: true
      })
      this.params.mtgApplecteur.addCircleOr({
        o: tatag4,
        r: 0.5,
        thickness: 6,
        color: 'red',
        tag: tatag6
      })
      this._mtgOnMove = (event) => {
        let lx = event.clientX
        let ly = event.clientY
        if (event.touches) {
          if (event.touches[0]) {
            lx = event.touches[0].clientX
            ly = event.touches[0].clientY
          }
        }
        if (!lx || !ly) return
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        this.params.mtgApplecteur.setPointPosition(this.params.svgId, '#PS', lx - cooSvgId.x, ly - cooSvgId.y, true)
      }
      this._mtgOnDown = () => {
        this._mtgOnMove = dummyFn
        this._mtgOnDown = dummyFn
        this.scratchMtgClean()
        const tatagLa = this.scratchMtgNewTag(1)
        const tatagPoint = this.scratchMtgNewTag(1)
        const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag4 })
        try {
          this.params.mtgApplecteur.addLinkedPointCircle({
            absCoord: true,
            c: tatag2,
            x: loch.x,
            y: loch.y,
            pointStyle: 'bigmult',
            tag: tatagPoint,
            name: tatagPoint,
            hiddenName: true
          })
        } catch (e) {
          this.params.scratchProgDonnees.yaErreurMtg = 'Le point ' + ptariv + ' est en dehors de la figure !<br> Essaie avec une longueur plus petite. '
          this.scratchMtgFinishOrdre()
          return
        }
        this.params.mtgApplecteur.addSegment({
          b: tatagPoint,
          a: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
          thickness: 2,
          lineStyle: 'dashdash',
          color: 'green',
          tag: tatagLa
        })
        const toShow = [tatagPoint]
        this.params.mtgApplecteur.deleteElt({ elt: tatag3 })
        if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(ptariv) === -1) {
          const tag3 = this.scratchMtgNewTag(1)
          const tag4 = this.scratchMtgNewTag(1)
          const tag2 = this.scratchMtgNewTag(1)
          const cooSvgId = this.params.leSvg.getBoundingClientRect()
          this.tryPlaceNom(tatagPoint, tag2, tag3, tag4, ptariv, cooSvgId, toShow)
        }

        const tag1 = this.scratchMtgNewTag(1)
        const tag2 = this.scratchMtgNewTag(1)
        const tag3 = this.scratchMtgNewTag(1)
        const tag4 = this.scratchMtgNewTag(1)
        const tag5 = this.scratchMtgNewTag(1)
        const tag6 = this.scratchMtgNewTag(1)
        const tag7 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addCircleOA({
          o: tatagPoint,
          a: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
          hidden: true,
          tag: tag2
        })
        this.params.mtgApplecteur.addCircleOA({
          o: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
          a: tatagPoint,
          hidden: true,
          tag: tag3
        })
        this.params.mtgApplecteur.addIntCircleCircle({
          c: tag3,
          c2: tag2,
          pointStyle: 'bigmult',
          tag: tag5,
          name: tag5,
          tag2: tag6,
          name2: tag6,
          hidden: true
        })
        this.params.mtgApplecteur.addLineAB({
          a: tag5,
          b: tag6,
          thickness: 1,
          tag: tag4,
          color: 'green',
          hidden: true
        })
        this.params.mtgApplecteur.addIntLineLine({
          d: tag4,
          d2: tatagLa,
          tag: tag1,
          name: tag1,
          hiddenName: true,
          hidden: true
        })
        const ztag3 = this.scratchMtgNewTag(1)
        const ztag2 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addCircleOr({
          o: tag1,
          r: 0.6,
          thickness: 6,
          color: 'green',
          tag: ztag3,
          hidden: true
        })
        this.params.mtgApplecteur.addIntLineCircle({
          c: ztag3,
          d: tag4,
          pointStyle: 'bigmult',
          tag: ztag2,
          name: ztag2,
          hidden: true
        })
        this.params.mtgApplecteur.addLinkedText({
          text: nomLong,
          a: ztag2,
          y: 1,
          color: 'purple',
          hidden: false,
          tag: tag7,
          hAlign: 'center',
          vAlign: 'middle',
          transparent: true
        })
        const ames = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addCalc({ nameCalc: ames, formula: long })

        // TODO dans ajoute contient, on doit test les autres qui le contiennent aussi
        this.params.donnesMtg.push({ type: 'point', tag: tatagPoint, noms: [ptariv], depend: [this.scratchMtgRetrouve({ nom: adonext1 }, 'num'), this.params.donnesMtg.length - 1], viz: getTrue, toShow })
        this.addPointMesur(tatagPoint, 1)
        this.scratchMtgFinishOrdre()
      }
    } else {
      const tatagLa = this.scratchMtgNewTag(1)
      const tatagPoint = this.scratchMtgNewTag(1)
      const loch = (!this.params.squizzEssai) ? { x: 150, y: 150 } : { x: 500, y: 500 }
      try {
        this.params.mtgApplecteur.addLinkedPointCircle({
          absCoord: true,
          c: tatag2,
          x: loch.x,
          y: loch.y,
          pointStyle: 'bigmult',
          tag: tatagPoint,
          name: tatagPoint,
          hiddenName: true
        })
      } catch (e) {
        this.params.scratchProgDonnees.yaErreurMtg = 'Le point ' + ptariv + ' est en dehors de la figure !<br> Essaie avec une longueur plus petite. '
        this.scratchMtgFinishOrdre()
        return
      }
      this.params.mtgApplecteur.addSegment({
        b: tatagPoint,
        a: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
        thickness: 2,
        lineStyle: 'dashdash',
        color: 'green',
        tag: tatagLa
      })
      const toShow = [tatagPoint]
      if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(ptariv) === -1) {
        const tag3 = this.scratchMtgNewTag(1)
        const tag4 = this.scratchMtgNewTag(1)
        const tag2 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addCircleOr({
          o: tatagPoint,
          r: 1,
          hidden: true,
          tag: tag2
        })
        this.params.mtgApplecteur.addLinkedPointCircle({
          absCoord: true,
          c: tag2,
          x: 1,
          y: 1,
          pointStyle: 'pixel',
          color: 'white',
          hidden: false,
          tag: tag3,
          name: tag3,
          hiddenName: true
        })
        this.params.mtgApplecteur.addLinkedText({
          text: ptariv,
          a: tag3,
          color: 'blue',
          hidden: false,
          tag: tag4,
          hAlign: 'center',
          vAlign: 'middle',
          transparent: true
        })
        toShow.push(tag3, tag4)
      }

      const tag1 = this.scratchMtgNewTag(1)
      const tag2 = this.scratchMtgNewTag(1)
      const tag3 = this.scratchMtgNewTag(1)
      const tag4 = this.scratchMtgNewTag(1)
      const tag5 = this.scratchMtgNewTag(1)
      const tag6 = this.scratchMtgNewTag(1)
      const tag7 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOA({
        o: tatagPoint,
        a: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
        hidden: true,
        tag: tag2
      })
      this.params.mtgApplecteur.addCircleOA({
        o: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
        a: tatagPoint,
        hidden: true,
        tag: tag3
      })
      this.params.mtgApplecteur.addIntCircleCircle({
        c: tag3,
        c2: tag2,
        pointStyle: 'bigmult',
        tag: tag5,
        name: tag5,
        tag2: tag6,
        name2: tag6,
        hidden: true
      })
      this.params.mtgApplecteur.addLineAB({
        a: tag5,
        b: tag6,
        thickness: 1,
        tag: tag4,
        color: 'green',
        hidden: true
      })
      this.params.mtgApplecteur.addIntLineLine({
        d: tag4,
        d2: tatagLa,
        tag: tag1,
        name: tag1,
        hiddenName: true,
        hidden: true
      })
      const ztag3 = this.scratchMtgNewTag(1)
      const ztag2 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOr({
        o: tag1,
        r: 0.6,
        thickness: 6,
        color: 'green',
        tag: ztag3,
        hidden: true
      })
      this.params.mtgApplecteur.addIntLineCircle({
        c: ztag3,
        d: tag4,
        pointStyle: 'bigmult',
        tag: ztag2,
        name: ztag2,
        hidden: true
      })
      this.params.mtgApplecteur.addLinkedText({
        text: nomLong,
        a: ztag2,
        y: 1,
        color: 'purple',
        hidden: false,
        tag: tag7,
        hAlign: 'center',
        vAlign: 'middle',
        transparent: true
      })
      const ames = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCalc({ nameCalc: ames, formula: long })
      // TODO dans ajoute contient, on doit test les autres qui le contiennent aussi
      this.params.donnesMtg.push({ type: 'point', tag: tatagPoint, noms: [ptariv], depend: [this.scratchMtgRetrouve({ nom: adonext1 }, 'num'), this.params.donnesMtg.length - 1], viz: getTrue, toShow })
      this.addPointMesur(tatagPoint, 1)
      this.scratchMtgFinishOrdre()
    }
  }

  tryPlaceNom (tag1, tag2, tag3, tag4, nom, cooSvgId, toShow) {
    this.params.enreG.push('ON try place nom ', nom)
    let pasplace = false
    this.params.mtgApplecteur.addCircleOr({
      o: tag1,
      r: 1,
      hidden: true,
      tag: tag2
    })
    try {
      this.params.mtgApplecteur.addLinkedPointCircle({
        absCoord: true,
        c: tag2,
        x: cooSvgId.x + 1,
        y: cooSvgId.y + 1,
        pointStyle: 'pixel',
        color: 'white',
        hidden: false,
        tag: tag3,
        name: tag3,
        hiddenName: true
      })
    } catch (e) {
      try {
        this.params.mtgApplecteur.addLinkedPointCircle({
          absCoord: true,
          c: tag2,
          x: cooSvgId.x + 1,
          y: cooSvgId.y - 1,
          pointStyle: 'pixel',
          color: 'white',
          hidden: false,
          tag: tag3,
          name: tag3,
          hiddenName: true
        })
      } catch (e) {
        try {
          this.params.mtgApplecteur.addLinkedPointCircle({
            absCoord: true,
            c: tag2,
            x: cooSvgId.x - 1,
            y: cooSvgId.y - 1,
            pointStyle: 'pixel',
            color: 'white',
            hidden: false,
            tag: tag3,
            name: tag3,
            hiddenName: true
          })
        } catch (e) {
          try {
            this.params.mtgApplecteur.addLinkedPointCircle({
              absCoord: true,
              c: tag2,
              x: cooSvgId.x - 1,
              y: cooSvgId.y + 1,
              pointStyle: 'pixel',
              color: 'white',
              hidden: false,
              tag: tag3,
              name: tag3,
              hiddenName: true
            })
          } catch (e) {
            console.error('place nom impossible')
            pasplace = true
          }
        }
      }
    }
    if (!pasplace) {
      try {
        this.params.mtgApplecteur.addLinkedText({
          text: nom,
          a: tag3,
          color: 'blue',
          hidden: false,
          tag: tag4,
          hAlign: 'center',
          vAlign: 'middle',
          transparent: true
        })
        toShow.push(tag3, tag4)
      } catch (e) {
        j3pNotify('Pour tom ' + e, { tabcodex: this.params.scratchProgDonnees.tabcodeX, enreG: this.params.enreG })
      }
    }
  }

  addPointMesur (tag, num) {
    let donnesMtg
    switch (num) {
      case 1:
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        donnesMtg = this.params.donnesMtg3
        break
    }
    for (let i = 0; i < donnesMtg.length - 1; i++) {
      if (donnesMtg[i].type === 'point' ||
        donnesMtg[i].type === 'pointMilieu' ||
        donnesMtg[i].type === 'pointSym' ||
        donnesMtg[i].type === 'pointRot' ||
        donnesMtg[i].type === 'pointSur' ||
        donnesMtg[i].type === 'pointIntersection' ||
        donnesMtg[i].type === 'pointLibreAvecNom') {
        this.params.mtgApplecteur.addLengthMeasure({ a: tag, b: donnesMtg[i].tag })
      }
    }
  }

  scratchMtgPointInter (nom, cont1, cont2) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const ob1 = this.scratchMtgRetrouve({ nom: cont1 }, 'objet')
    const ob2 = this.scratchMtgRetrouve({ nom: cont2 }, 'objet')
    const InterPos = []
    const tatag = this.scratchMtgNewTag(1)
    const tatag2 = this.scratchMtgNewTag(1)
    try {
      if (ob1.type === 'arc' || ob1.type === 'cercle') {
        if (ob2.type === 'arc' || ob2.type === 'cercle') {
          this.params.mtgApplecteur.addIntCircleCircle({
            c: ob1.tag,
            c2: ob2.tag,
            hidden: false,
            name: tatag,
            tag: tatag,
            name2: tatag2,
            tag2: tatag2,
            hiddenName: true,
            color: 'red',
            pointStyle: 'bigmult',
            smartIntersect: false
          })
        } else {
          this.params.mtgApplecteur.addIntLineCircle({
            c: ob1.tag,
            d: ob2.tag,
            hidden: false,
            name: tatag,
            tag: tatag,
            name2: tatag2,
            tag2: tatag2,
            hiddenName: true,
            color: 'red',
            pointStyle: 'bigmult',
            smartIntersect: false
          })
        }
        InterPos.push(tatag, tatag2)
      } else {
        if (ob2.type === 'arc' || ob2.type === 'cercle') {
          this.params.mtgApplecteur.addIntLineCircle({
            d: ob1.tag,
            c: ob2.tag,
            hidden: false,
            name: tatag,
            tag: tatag,
            name2: tatag2,
            tag2: tatag2,
            hiddenName: true,
            color: 'red',
            pointStyle: 'bigmult',
            smartIntersect: false
          })
          InterPos.push(tatag, tatag2)
        } else {
          this.params.mtgApplecteur.addIntLineLine({
            d: ob1.tag,
            d2: ob2.tag,
            hidden: false,
            name: tatag,
            tag: tatag,
            hiddenName: true,
            color: 'red',
            pointStyle: 'bigmult',
            smartIntersect: false
          })
          InterPos.push(tatag)
        }
      }
    } catch (e) {
      this.scratchfautStop('Ce point d’intersection existe déjà !')
      return
    }
    const avire = []
    for (let i = InterPos.length - 1; i > -1; i--) {
      const isViz = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: InterPos[i] })
      if (isViz.x === undefined || isViz.y === undefined) {
        InterPos.splice(i, 1)
        if (InterPos.length === 0) {
          this.scratchfautStop('Pas de point d’intersection !')
          return
        }
        continue
      }
      const tropProches = this.scatchMtgPointsProches(1, isViz)
      if (tropProches.length > 0) {
        avire.push(InterPos[i])
        InterPos.splice(i, 1)
        if (InterPos.length === 0) {
          this.scratchfautStop('Le point existe déjà !')
          return
        }
        continue
      }
      if (this.scratchMtgEnDehors(isViz)) {
        avire.push(InterPos[i])
        InterPos.splice(i, 1)
        if (InterPos.length === 0) {
          this.scratchfautStop('Le point d’intersection n’est pas visible !')
          return
        }
      }
    }
    if (avire.length > 0) this.params.mtgApplecteur.deleteElt({ elt: avire[0] })
    if (this.params.isEditing || this.params.isSpeed) {
      if (InterPos.length > 1) {
        const iasup = j3pGetRandomInt(0, 1)
        const asup = InterPos[iasup]
        InterPos.splice(iasup, 1)
        this.params.mtgApplecteur.setHidden({ elt: asup })
        this.params.mtgApplecteur.deleteElt({ elt: asup })
      }
    }
    if (InterPos.length === 1) {
      this.scratchMtgFinishPointInter(InterPos[0], nom, ob1, ob2)
    } else {
      this.scratchMtgMessage('Sélectionne le point d’intersection')
      const tagop1 = this.scratchMtgNewTag(1)
      const tagop2 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOr({
        o: InterPos[0],
        r: 0.5,
        thickness: 6,
        color: 'red',
        tag: tagop1
      })
      this.params.mtgApplecteur.addCircleOr({
        o: InterPos[1],
        r: 0.5,
        thickness: 6,
        color: 'red',
        tag: tagop2
      })
      const co1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: InterPos[0] })
      const co2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: InterPos[1] })
      this._mtgOnMove = (event) => {
        let lx = event.clientX
        let ly = event.clientY
        if (event.touches) {
          if (event.touches[0]) {
            lx = event.touches[0].clientX
            ly = event.touches[0].clientY
          }
        }
        if (!lx || !ly) return
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        const loch1 = this.dist({ x: lx - cooSvgId.x, y: ly - cooSvgId.y }, co1)
        const loch2 = this.dist({ x: lx - cooSvgId.x, y: ly - cooSvgId.y }, co2)
        const cool2 = (loch1 > loch2) ? 'red' : 'black'
        const cool1 = (loch1 < loch2) ? 'red' : 'black'
        this.params.mtgApplecteur.setColor({ elt: tagop1, color: cool1 })
        this.params.mtgApplecteur.setColor({ elt: InterPos[0], color: cool1 })
        this.params.mtgApplecteur.setColor({ elt: tagop2, color: cool2 })
        this.params.mtgApplecteur.setColor({ elt: InterPos[1], color: cool2 })
      }
      this._mtgOnDown = (event) => {
        let lx = event.clientX
        let ly = event.clientY
        if (event.touches) {
          if (event.touches[0]) {
            lx = event.touches[0].clientX
            ly = event.touches[0].clientY
          }
        }
        if (!lx || !ly) return
        this._mtgOnMove = dummyFn
        this._mtgOnDown = dummyFn
        this.scratchMtgClean()
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        const loch1 = this.dist({ x: lx - cooSvgId.x, y: ly - cooSvgId.y }, co1)
        const loch2 = this.dist({ x: lx - cooSvgId.x, y: ly - cooSvgId.y }, co2)
        const tatagLa = (loch1 < loch2) ? InterPos[0] : InterPos[1]
        const tatagAef = (loch1 > loch2) ? InterPos[0] : InterPos[1]
        this.params.mtgApplecteur.deleteElt({ elt: tagop1 })
        this.params.mtgApplecteur.deleteElt({ elt: tagop2 })
        this.params.mtgApplecteur.deleteElt({ elt: tatagAef })
        this.params.mtgApplecteur.setColor({ elt: tatagLa, color: 'black' })
        const toShow = [tatagLa]
        if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
          const tag3 = this.scratchMtgNewTag(1)
          const tag2 = this.scratchMtgNewTag(1)
          const tag4 = this.scratchMtgNewTag(1)
          const cooSvgId = this.params.leSvg.getBoundingClientRect()
          this.tryPlaceNom(tatagLa, tag2, tag3, tag4, nom, cooSvgId, toShow)
        }
        this.params.donnesMtg.push({ type: 'point', tag: tatagLa, noms: [nom], depend: [this.scratchMtgRetrouve({ nom: ob1.nom }, 'num'), this.scratchMtgRetrouve({ nom: ob2.nom }, 'num')], viz: getTrue, toShow, sur: [this.scratchMtgRetrouve({ nom: ob1.nom }, 'num'), this.scratchMtgRetrouve({ nom: ob2.nom }, 'num')] })
        this.addPointMesur(tatagLa, 1)
        // TODO dans ajoute contient, on doit test les autres qui le contiennent aussi
        this.scratchAjouteContient(1, ob1.noms[0], nom)
        this.scratchAjouteContient(1, ob2.noms[0], nom)
        this.scratchMtgFinishOrdre()
      }
    }
  }

  scratchMtgFinishPointInter (inter, nom, ob1, ob2) {
    const toShow = [inter]
    this.params.mtgApplecteur.setColor({ elt: inter, color: 'black' })
    if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
      const tag3 = this.scratchMtgNewTag(1)
      const tag4 = this.scratchMtgNewTag(1)
      const tag2 = this.scratchMtgNewTag(1)
      const cooSvgId = this.params.leSvg.getBoundingClientRect()
      this.tryPlaceNom(inter, tag2, tag3, tag4, nom, cooSvgId, toShow)
    }
    this.params.donnesMtg.push({ type: 'pointIntersection', tag: inter, noms: [nom], depend: [this.scratchMtgRetrouve({ nom: ob1.nom }, 'num'), this.scratchMtgRetrouve({ nom: ob2.nom }, 'num')], viz: getTrue, toShow, sur: [this.scratchMtgRetrouve({ nom: ob1.nom }, 'num'), this.scratchMtgRetrouve({ nom: ob2.nom }, 'num')] })
    this.addPointMesur(inter, 1)
    this.scratchAjouteContient(1, ob1.noms[0], nom)
    this.scratchAjouteContient(1, ob2.noms[0], nom)
    this.scratchMtgFinishOrdre()
  }

  scratchMtgPointSur (nom, cont) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const obDeb = this.scratchMtgRetrouve({ nom: cont }, 'objet')
    if (!this.params.isEditing && !this.params.isSpeed) {
      const nomaf = (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) ? 'Clique pour placer le point ' + nom : 'Clique pour placer un point '
      const maFunc = (ob1) => {
        if (ob1 === undefined) {
          ob1 = this.params.param.ob1
        }
        const asuivre = []
        const tag1 = this.scratchMtgNewTag(1)
        const tatag4 = this.scratchMtgNewTag(1)
        this.scratchMtgMessage(nomaf)
        if (ob1.type === 'cercle' || ob1.type === 'arc') {
          this.params.mtgApplecteur.addRay({
            o: this.scratchMtgRetrouve({ nom: ob1.centre }, 'tag'),
            a: 'I',
            tag: tag1,
            hidden: true
          })
          this.params.mtgApplecteur.addIntLineCircle({
            d: tag1,
            c: ob1.tag,
            hidden: false,
            name: tatag4,
            tag: tatag4,
            hiddenName: true,
            color: 'red',
            pointStyle: 'biground'
          })
          asuivre.push(tatag4)
          const tatag5 = this.scratchMtgNewTag(1)
          this.params.mtgApplecteur.addCircleOr({
            o: tatag4,
            r: 0.8,
            hidden: false,
            tag: tatag5,
            thickness: 5,
            color: 'red'
          })
        }
        if (ob1.type === 'segment' || ob1.type === 'droite' || ob1.type === 'demidroite') {
          this.params.mtgApplecteur.addLinePerp({
            d: ob1.tag,
            a: 'I',
            tag: tag1,
            hidden: true
          })
          this.params.mtgApplecteur.addIntLineLine({
            d: tag1,
            d2: ob1.tag,
            hidden: false,
            name: tatag4,
            tag: tatag4,
            hiddenName: true,
            color: 'red',
            pointStyle: 'biground'
          })
          asuivre.push(tatag4)
          const tatag5 = this.scratchMtgNewTag(1)
          this.params.mtgApplecteur.addCircleOr({
            o: tatag4,
            r: 0.5,
            hidden: false,
            tag: tatag5,
            thickness: 6,
            color: 'red'
          })
        }
        this.params.mtgApplecteur.addLengthMeasure({ a: asuivre[0], b: 'I' })
        const tatag5 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addCalc({ nameCalc: tatag5, formula: asuivre[0] + 'I' })
        this._mtgOnMove = (event) => {
          let lx = event.clientX
          let ly = event.clientY
          if (event.touches) {
            if (event.touches[0]) {
              lx = event.touches[0].clientX
              ly = event.touches[0].clientY
            }
          }
          if (!lx || !ly) return
          const cooSvgId = this.params.leSvg.getBoundingClientRect()
          this.params.mtgApplecteur.setPointPosition(this.params.svgId, '#PS', lx - cooSvgId.x, ly - cooSvgId.y, true)
          const apasoubli = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag4 })
          if (this.scratchMtgPlaceOk(1, apasoubli, true)) {
            this.apasoubli = apasoubli
          }
        }
        this._mtgOnDown = () => {
          const jkjk = this.apasoubli
          if (!jkjk) return
          if (jkjk.x === undefined) return
          if (!this.scratchTestExiste(1, tatag5)) return
          // this.params.mtgApplecteur.deleteElt({ elt: tatag5 })
          this._mtgOnMove = dummyFn
          this._mtgOnDown = dummyFn
          this.scratchMtgClean()
          const tagp1 = this.scratchMtgNewTag(1)
          if (ob1.type === 'cercle' || ob1.type === 'arc') {
            this.params.mtgApplecteur.addLinkedPointCircle({
              absCoord: true,
              c: ob1.tag,
              x: jkjk.x,
              y: jkjk.y,
              hidden: false,
              name: tagp1,
              tag: tagp1,
              hiddenName: true,
              color: 'black',
              pointStyle: 'bigmult'
            })
          }
          if (ob1.type === 'segment' || ob1.type === 'droite' || ob1.type === 'demidroite') {
            this.params.mtgApplecteur.addLinkedPointLine({
              absCoord: true,
              d: ob1.tag,
              x: jkjk.x,
              y: jkjk.y,
              hidden: false,
              name: tagp1,
              tag: tagp1,
              hiddenName: true,
              color: 'black',
              pointStyle: 'bigmult'
            })
          }
          this.params.mtgApplecteur.deleteElt({ elt: tag1 })
          const toShow = [tagp1]
          if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
            const tag2 = this.scratchMtgNewTag(1)
            const tag3 = this.scratchMtgNewTag(1)
            const tag4 = this.scratchMtgNewTag(1)
            const cooSvgId = this.params.leSvg.getBoundingClientRect()
            this.tryPlaceNom(tagp1, tag2, tag3, tag4, nom, cooSvgId, toShow)
          }
          this.params.donnesMtg.push({
            type: 'pointSur',
            tag: tagp1,
            noms: [nom],
            depend: [this.scratchMtgRetrouve({ nom: cont }, 'num')],
            viz: getTrue,
            toShow,
            sur: [this.scratchMtgRetrouve({ nom: cont }, 'num')]
          })
          this.addPointMesur(tagp1, 1)
          this.scratchAjouteContient(1, ob1.noms[0], nom)
          this.scratchMtgFinishOrdre()
        }
      }
      if (this.scratchYaPLusieurs({ nom: cont })) {
        this.scratchChoisz({ nom: cont }, maFunc, { choiz: 'ob1' })
      } else {
        maFunc(obDeb)
      }
    } else {
      const ob1 = obDeb

      const asuivre = []
      const tag1 = this.scratchMtgNewTag(1)
      let minx = 10
      let miny = 15
      let maxx = (!this.params.squizzEssai) ? 380 : 900
      let maxy = (!this.params.squizzEssai) ? 380 : 900
      let cooTantpis = { x: 100, y: 100 }
      if (ob1.type === 'cercle' || ob1.type === 'arc') {
        this.params.mtgApplecteur.addRay({
          o: this.scratchMtgRetrouve({ nom: ob1.centre }, 'tag'),
          a: 'I',
          tag: tag1,
          hidden: true
        })
        const tatag4 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addIntLineCircle({
          d: tag1,
          c: ob1.tag,
          hidden: true,
          name: tatag4,
          tag: tatag4,
          hiddenName: true,
          color: 'red',
          pointStyle: 'biground'
        })
        asuivre.push(tatag4)
      }
      if (ob1.type === 'segment' || ob1.type === 'droite' || ob1.type === 'demidroite') {
        this.params.mtgApplecteur.addLinePerp({
          d: ob1.tag,
          a: 'I',
          tag: tag1,
          hidden: true
        })
        const tatag4 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addIntLineLine({
          d: tag1,
          d2: ob1.tag,
          hidden: true,
          name: tatag4,
          tag: tatag4,
          hiddenName: true,
          color: 'red',
          pointStyle: 'biground'
        })
        asuivre.push(tatag4)
        if (ob1.type === 'segment') {
          const coa = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouve({ nom: ob1.ext1 }, 'tag') })
          const cob = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouve({ nom: ob1.ext2 }, 'tag') })
          minx = Math.min(coa.x, cob.x) - 10
          miny = Math.min(coa.y, cob.y) - 10
          maxx = Math.max(coa.x, cob.x) + 10
          maxy = Math.max(coa.x, cob.x) + 10
          cooTantpis = { x: (2 * coa.x + cob.x) / 3, y: (2 * coa.y + cob.y) / 3 }
        }
      }
      this.params.mtgApplecteur.addLengthMeasure({ a: asuivre[0], b: 'I' })
      const tatag5 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCalc({ nameCalc: tatag5, formula: asuivre[0] + 'I' })
      const cooPointLire = {}
      let ok, jkjk
      let cptcpt = 0
      do {
        cptcpt++
        ok = false
        cooPointLire.x = j3pGetRandomInt(minx, maxx)
        cooPointLire.y = j3pGetRandomInt(miny, maxy)
        this.params.mtgApplecteur.setPointPosition(this.params.svgId, '#PS', cooPointLire.x, cooPointLire.x, true)
        if (this.scratchTestExiste(1, tatag5)) {
          jkjk = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: asuivre[0] })
          ok = this.scratchMtgPlaceOk(1, jkjk)
        }
        if (cptcpt > 1000) ok = true
      } while (!ok)
      if (!jkjk) jkjk = cooTantpis
      const tagp1 = this.scratchMtgNewTag(1)
      if (ob1.type === 'cercle' || ob1.type === 'arc') {
        this.params.mtgApplecteur.addLinkedPointCircle({
          absCoord: true,
          c: ob1.tag,
          x: jkjk.x,
          y: jkjk.y,
          hidden: false,
          name: tagp1,
          tag: tagp1,
          hiddenName: true,
          color: 'black',
          pointStyle: 'bigmult'
        })
      }
      if (ob1.type === 'segment' || ob1.type === 'droite' || ob1.type === 'demidroite') {
        this.params.mtgApplecteur.addLinkedPointLine({
          absCoord: true,
          d: ob1.tag,
          x: jkjk.x,
          y: jkjk.y,
          hidden: false,
          name: tagp1,
          tag: tagp1,
          hiddenName: true,
          color: 'black',
          pointStyle: 'bigmult'
        })
      }
      this.params.mtgApplecteur.deleteElt({ elt: tag1 })
      const toShow = [tagp1]
      if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
        const tag2 = this.scratchMtgNewTag(1)
        const tag3 = this.scratchMtgNewTag(1)
        const tag4 = this.scratchMtgNewTag(1)
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        this.tryPlaceNom(tagp1, tag2, tag3, tag4, nom, cooSvgId, toShow)
      }
      this.params.donnesMtg.push({
        type: 'pointSur',
        tag: tagp1,
        noms: [nom],
        depend: [this.scratchMtgRetrouve({ nom: cont }, 'num')],
        viz: getTrue,
        toShow,
        sur: [this.scratchMtgRetrouve({ nom: cont }, 'num')]
      })
      this.addPointMesur(tagp1, 1)
      this.scratchAjouteContient(1, ob1.noms[0], nom)
      this.scratchMtgFinishOrdre()
    }
  }

  scratchMtgPointMilieu (nom, cont) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const ob = this.scratchMtgRetrouve({ nom: cont }, 'objet')
    const de1 = this.scratchMtgRetrouve({ nom: ob.ext1 }, 'tag')
    const de2 = this.scratchMtgRetrouve({ nom: ob.ext2 }, 'tag')
    const tatag = this.scratchMtgNewTag(1)
    const tatag2 = this.scratchMtgNewTag(1)
    const tatag3 = this.scratchMtgNewTag(1)
    const tatag4 = this.scratchMtgNewTag(1)
    const tatag5 = this.scratchMtgNewTag(1)
    const tatag6 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOA({
      o: de2,
      a: de1,
      hidden: true,
      tag: tatag2
    })
    this.params.mtgApplecteur.addCircleOA({
      o: de1,
      a: de2,
      hidden: true,
      tag: tatag3
    })
    this.params.mtgApplecteur.addIntCircleCircle({
      c: tatag3,
      c2: tatag2,
      pointStyle: 'bigmult',
      tag: tatag5,
      name: tatag5,
      tag2: tatag6,
      name2: tatag6,
      hidden: true
    })
    this.params.mtgApplecteur.addLineAB({
      a: tatag5,
      b: tatag6,
      thickness: 1,
      tag: tatag4,
      color: 'green',
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineLine({
      d: tatag4,
      d2: ob.tag,
      pointStyle: 'bigmult',
      tag: tatag,
      name: tatag,
      hiddenName: true
    })
    const toShow = [tatag]
    if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
      const tag2 = this.scratchMtgNewTag(1)
      const tag3 = this.scratchMtgNewTag(1)
      const tag4 = this.scratchMtgNewTag(1)
      const cooSvgId = this.params.leSvg.getBoundingClientRect()
      this.tryPlaceNom(tatag, tag2, tag3, tag4, nom, cooSvgId, toShow)
    }
    this.params.donnesMtg.push({
      type: 'pointMilieu',
      tag: tatag,
      noms: [nom],
      depend: [this.scratchMtgRetrouve({ nom: ob.noms[0] }, 'num')],
      viz: getTrue,
      toShow,
      sur: []
    })
    this.addPointMesur(tatag, 1)
    this.scratchAjouteContient(1, ob.noms[0], nom)
    this.scratchMtgFinishOrdre()
  }

  scratchMtgPointAligne (nom, p1, p2) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const de1 = this.scratchMtgRetrouve({ nom: p1 }, 'tag')
    const de2 = this.scratchMtgRetrouve({ nom: p2 }, 'tag')
    const tatag2 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addLineAB({
      a: de1,
      b: de2,
      thickness: 1,
      tag: tatag2,
      lineStyle: 'dashdash'
    })
    if (!this.params.isEditing && !this.params.isSpeed) {
      const nomaf = (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) ? 'Clique pour placer le point ' + nom : 'Clique pour placer un point '
      const tag1 = this.scratchMtgNewTag(1)
      this.scratchMtgMessage(nomaf)
      this.params.mtgApplecteur.addLinePerp({
        d: tatag2,
        a: 'I',
        tag: tag1,
        hidden: true
      })
      const tatag4 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addIntLineLine({
        d: tag1,
        d2: tatag2,
        hidden: false,
        name: tatag4,
        tag: tatag4,
        hiddenName: true,
        color: 'red',
        pointStyle: 'biground'
      })
      const tatag5 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOr({
        o: tatag4,
        r: 0.5,
        hidden: false,
        tag: tatag5,
        thickness: 6,
        color: 'red'
      })
      this.params.mtgApplecteur.addLengthMeasure({ a: tatag4, b: 'I' })
      const tatag6 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCalc({ nameCalc: tatag6, formula: tatag4 + 'I' })
      this._mtgOnMove = (event) => {
        let lx = event.clientX
        let ly = event.clientY
        if (event.touches) {
          if (event.touches[0]) {
            lx = event.touches[0].clientX
            ly = event.touches[0].clientY
          }
        }
        if (!lx || !ly) return
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        this.params.mtgApplecteur.setPointPosition(this.params.svgId, '#PS', lx - cooSvgId.x, ly - cooSvgId.y, true)
      }
      this._mtgOnDown = () => {
        const jkjk = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag4 })
        if (!this.scratchTestExiste(1, tatag6)) return
        // this.params.mtgApplecteur.deleteElt({ elt: tatag5 })
        this._mtgOnMove = dummyFn
        this._mtgOnDown = dummyFn
        this.scratchMtgClean()
        const tagp1 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addLinkedPointLine({
          absCoord: true,
          d: tatag2,
          x: jkjk.x,
          y: jkjk.y,
          hidden: false,
          name: tagp1,
          tag: tagp1,
          hiddenName: true,
          color: 'black',
          pointStyle: 'bigmult'
        })
        this.params.mtgApplecteur.deleteElt({ elt: tag1 })
        this.params.mtgApplecteur.deleteElt({ elt: tatag4 })
        const toShow = [tagp1]
        if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
          const tag2 = this.scratchMtgNewTag(1)
          const tag3 = this.scratchMtgNewTag(1)
          const tag4 = this.scratchMtgNewTag(1)
          const cooSvgId = this.params.leSvg.getBoundingClientRect()
          this.tryPlaceNom(tagp1, tag2, tag3, tag4, nom, cooSvgId, toShow)
        }
        this.params.donnesMtg.push({
          type: 'point',
          tag: tagp1,
          noms: [nom],
          depend: [this.scratchMtgRetrouve({ nom: p1 }, 'num'), this.scratchMtgRetrouve({ nom: p2 }, 'num')],
          viz: getTrue,
          toShow,
          sur: []
        })
        this.addPointMesur(tagp1, 1)
        this.scratchMtgFinishOrdre()
      }
    } else {
      const tag1 = this.scratchMtgNewTag(1)
      const minx = 10
      const miny = 15
      const maxx = (!this.params.squizzEssai) ? 380 : 900
      const maxy = (!this.params.squizzEssai) ? 380 : 900
      const loch1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: de1 })
      const loch2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: de2 })
      const cooTantpis = { x: (loch1.x + 2 * loch2.x) / 3, y: (loch1.y + 2 * loch2.y) / 3 }
      this.params.mtgApplecteur.addLinePerp({
        d: tatag2,
        a: 'I',
        tag: tag1,
        hidden: true
      })
      const tatag4 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addIntLineLine({
        d: tag1,
        d2: tatag2,
        hidden: true,
        name: tatag4,
        tag: tatag4,
        hiddenName: true,
        color: 'red',
        pointStyle: 'biground'
      })
      this.params.mtgApplecteur.addLengthMeasure({ a: tatag4, b: 'I' })
      const tatag5 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCalc({ nameCalc: tatag5, formula: tatag4 + 'I' })
      const cooPointLire = {}
      let ok, jkjk
      let cptcpt = 0
      do {
        cptcpt++
        ok = false
        cooPointLire.x = j3pGetRandomInt(minx, maxx)
        cooPointLire.y = j3pGetRandomInt(miny, maxy)
        this.params.mtgApplecteur.setPointPosition(this.params.svgId, '#PS', cooPointLire.x, cooPointLire.x, true)
        if (this.scratchTestExiste(1, tatag5)) {
          jkjk = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag4 })
          ok = this.scratchMtgPlaceOk(1, jkjk)
        }
        if (cptcpt > 1000) ok = true
      } while (!ok)
      if (!jkjk) jkjk = cooTantpis
      const tagp1 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLinkedPointLine({
        absCoord: true,
        d: tatag2,
        x: jkjk.x,
        y: jkjk.y,
        hidden: false,
        name: tagp1,
        tag: tagp1,
        hiddenName: true,
        color: 'black',
        pointStyle: 'bigmult'
      })
      this.params.mtgApplecteur.deleteElt({ elt: tag1 })
      const toShow = [tagp1]
      if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
        const tag2 = this.scratchMtgNewTag(1)
        const tag3 = this.scratchMtgNewTag(1)
        const tag4 = this.scratchMtgNewTag(1)
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        this.tryPlaceNom(tagp1, tag2, tag3, tag4, nom, cooSvgId, toShow)
      }
      this.params.donnesMtg.push({
        type: 'pointSur',
        tag: tagp1,
        noms: [nom],
        depend: [this.scratchMtgRetrouve({ nom: p1 }, 'num'), this.scratchMtgRetrouve({ nom: p2 }, 'num')],
        viz: getTrue,
        toShow,
        sur: []
      })
      this.addPointMesur(tagp1, 1)
      this.scratchMtgFinishOrdre()
    }
  }

  scratchMtgSegment (ext1, ext2, visible) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
    while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
    while (ext2.indexOf('\\\'') !== -1) { ext2 = ext2.replace('\\\'', '\'') }
    while (ext2.indexOf('\\"') !== -1) { ext2 = ext2.replace('\\"', '"') }
    const tatag = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addSegment({
      a: this.scratchMtgRetrouve({ nom: ext1 }, 'tag'),
      b: this.scratchMtgRetrouve({ nom: ext2 }, 'tag'),
      thickness: 2,
      tag: tatag,
      color: 'black'
    })
    const ames = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCalc({ nameCalc: ames, formula: this.scratchMtgRetrouveBase(1, { nom: ext1 }, 'tag') + this.scratchMtgRetrouveBase(1, { nom: ext2 }, 'tag') })

    this.params.donnesMtg.push({ tag1: this.scratchMtgRetrouve({ nom: ext1 }, 'tag'), tag2: this.scratchMtgRetrouve({ nom: ext2 }, 'tag'), type: 'segment', tag: tatag, ext1, ext2, noms: ['[' + ext1 + ext2 + ']', '[' + ext2 + ext1 + ']'], depend: [this.scratchMtgRetrouve({ nom: ext1 }, 'num'), this.scratchMtgRetrouve({ nom: ext2 }, 'num')], viz: this._getIsVisible({ visible: visible !== false }), toShow: [tatag], contient: [ext1, ext2], longueur: ames })
    this.scratchMtgFinishOrdre()
  }

  scratchMtgSegmentLong (adonext1, ptariv, long, nomLong) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const tatag2 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
      r: long,
      thickness: 6,
      color: 'green',
      tag: tatag2,
      hidden: true
    })
    if (!this.params.isEditing && !this.params.isSpeed) {
      this.scratchMtgMessage('Place le point ' + ptariv)
      const tatag4 = this.scratchMtgNewTag(1)
      const tatag6 = this.scratchMtgNewTag(1)
      const tatag3 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addRay({
        o: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
        a: 'I',
        tag: tatag3,
        hidden: true
      })
      this.params.mtgApplecteur.addIntLineCircle({
        d: tatag3,
        c: tatag2,
        name: tatag4,
        tag: tatag4,
        pointStyle: 'bigmult',
        color: 'black',
        hidden: true
      })
      this.params.mtgApplecteur.addCircleOr({
        o: tatag4,
        r: 0.5,
        thickness: 6,
        color: 'red',
        tag: tatag6
      })
      this._mtgOnMove = (event) => {
        let lx = event.clientX
        let ly = event.clientY
        if (event.touches) {
          if (event.touches[0]) {
            lx = event.touches[0].clientX
            ly = event.touches[0].clientY
          }
        }
        if (!lx || !ly) return
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        this.params.mtgApplecteur.setPointPosition(this.params.svgId, '#PS', lx - cooSvgId.x, ly - cooSvgId.y, true)
      }
      this._mtgOnDown = () => {
        this._mtgOnMove = dummyFn
        this._mtgOnDown = dummyFn
        this.scratchMtgClean()
        const tatagLa = this.scratchMtgNewTag(1)
        const tatagPoint = this.scratchMtgNewTag(1)
        const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag4 })
        try {
          this.params.mtgApplecteur.addLinkedPointCircle({
            absCoord: true,
            c: tatag2,
            x: loch.x,
            y: loch.y,
            pointStyle: 'bigmult',
            tag: tatagPoint,
            name: tatagPoint,
            hiddenName: true
          })
        } catch (e) {
          this.params.scratchProgDonnees.yaErreurMtg = 'Le point ' + ptariv + ' est en dehors de la figure !<br> Essaie avec une longueur plus petite. '
          this.scratchMtgFinishOrdre()
          return
        }
        this.params.mtgApplecteur.addSegment({
          b: tatagPoint,
          a: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
          thickness: 2,
          tag: tatagLa
        })
        const toShow = [tatagPoint]
        this.params.mtgApplecteur.deleteElt({ elt: tatag3 })
        if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(ptariv) === -1) {
          const tag3 = this.scratchMtgNewTag(1)
          const tag4 = this.scratchMtgNewTag(1)
          const tag2 = this.scratchMtgNewTag(1)
          const cooSvgId = this.params.leSvg.getBoundingClientRect()
          this.tryPlaceNom(tatagPoint, tag2, tag3, tag4, ptariv, cooSvgId, toShow)
        }

        const tag1 = this.scratchMtgNewTag(1)
        const tag2 = this.scratchMtgNewTag(1)
        const tag3 = this.scratchMtgNewTag(1)
        const tag4 = this.scratchMtgNewTag(1)
        const tag5 = this.scratchMtgNewTag(1)
        const tag6 = this.scratchMtgNewTag(1)
        const tag7 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addCircleOA({
          o: tatagPoint,
          a: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
          hidden: true,
          tag: tag2
        })
        this.params.mtgApplecteur.addCircleOA({
          o: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
          a: tatagPoint,
          hidden: true,
          tag: tag3
        })
        this.params.mtgApplecteur.addIntCircleCircle({
          c: tag3,
          c2: tag2,
          pointStyle: 'bigmult',
          tag: tag5,
          name: tag5,
          tag2: tag6,
          name2: tag6,
          hidden: true
        })
        this.params.mtgApplecteur.addLineAB({
          a: tag5,
          b: tag6,
          thickness: 1,
          tag: tag4,
          color: 'green',
          hidden: true
        })
        this.params.mtgApplecteur.addIntLineLine({
          d: tag4,
          d2: tatagLa,
          tag: tag1,
          name: tag1,
          hiddenName: true,
          hidden: true
        })
        const ztag3 = this.scratchMtgNewTag(1)
        const ztag2 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addCircleOr({
          o: tag1,
          r: 0.6,
          thickness: 6,
          color: 'green',
          tag: ztag3,
          hidden: true
        })
        this.params.mtgApplecteur.addIntLineCircle({
          c: ztag3,
          d: tag4,
          pointStyle: 'bigmult',
          tag: ztag2,
          name: ztag2,
          hidden: true
        })
        this.params.mtgApplecteur.addLinkedText({
          text: nomLong,
          a: ztag2,
          y: 1,
          color: 'purple',
          hidden: false,
          tag: tag7,
          hAlign: 'center',
          vAlign: 'middle',
          transparent: true
        })
        const ames = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addCalc({ nameCalc: ames, formula: long })

        // TODO dans ajoute contient, on doit test les autres qui le contiennent aussi
        this.params.donnesMtg.push({ type: 'point', tag: tatagPoint, noms: [ptariv], depend: [this.scratchMtgRetrouve({ nom: adonext1 }, 'num'), this.params.donnesMtg.length - 1], viz: getTrue, toShow })
        this.params.donnesMtg.push({ tag1: tatagPoint, tag2: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'), type: 'segment', tag: tatagLa, ext1: adonext1, ext2: ptariv, long, noms: ['[' + adonext1 + ptariv + ']', '[' + ptariv + adonext1 + ']'], depend: [this.scratchMtgRetrouve({ nom: adonext1 }, 'num')], viz: getTrue, toShow: [tatagLa, tag7], contient: [adonext1, ptariv], longueur: ames })
        this.addPointMesur(tatagPoint, 1)
        this.scratchMtgFinishOrdre()
      }
    } else {
      const tatagLa = this.scratchMtgNewTag(1)
      const tatagPoint = this.scratchMtgNewTag(1)
      const loch = (!this.params.squizzEssai) ? { x: 150, y: 150 } : { x: 500, y: 500 }
      try {
        this.params.mtgApplecteur.addLinkedPointCircle({
          absCoord: true,
          c: tatag2,
          x: loch.x,
          y: loch.y,
          pointStyle: 'bigmult',
          tag: tatagPoint,
          name: tatagPoint,
          hiddenName: true
        })
      } catch (e) {
        this.params.scratchProgDonnees.yaErreurMtg = 'Le point ' + ptariv + ' est en dehors de la figure !<br> Essaie avec une longueur plus petite. '
        this.scratchMtgFinishOrdre()
        return
      }
      this.params.mtgApplecteur.addSegment({
        b: tatagPoint,
        a: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
        thickness: 2,
        tag: tatagLa
      })
      const toShow = [tatagPoint]
      if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(ptariv) === -1) {
        const tag3 = this.scratchMtgNewTag(1)
        const tag4 = this.scratchMtgNewTag(1)
        const tag2 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addCircleOr({
          o: tatagPoint,
          r: 1,
          hidden: true,
          tag: tag2
        })
        this.params.mtgApplecteur.addLinkedPointCircle({
          absCoord: true,
          c: tag2,
          x: 1,
          y: 1,
          pointStyle: 'pixel',
          color: 'white',
          hidden: false,
          tag: tag3,
          name: tag3,
          hiddenName: true
        })
        this.params.mtgApplecteur.addLinkedText({
          text: ptariv,
          a: tag3,
          color: 'blue',
          hidden: false,
          tag: tag4,
          hAlign: 'center',
          vAlign: 'middle',
          transparent: true
        })
        toShow.push(tag3, tag4)
      }

      const tag1 = this.scratchMtgNewTag(1)
      const tag2 = this.scratchMtgNewTag(1)
      const tag3 = this.scratchMtgNewTag(1)
      const tag4 = this.scratchMtgNewTag(1)
      const tag5 = this.scratchMtgNewTag(1)
      const tag6 = this.scratchMtgNewTag(1)
      const tag7 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOA({
        o: tatagPoint,
        a: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
        hidden: true,
        tag: tag2
      })
      this.params.mtgApplecteur.addCircleOA({
        o: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'),
        a: tatagPoint,
        hidden: true,
        tag: tag3
      })
      this.params.mtgApplecteur.addIntCircleCircle({
        c: tag3,
        c2: tag2,
        pointStyle: 'bigmult',
        tag: tag5,
        name: tag5,
        tag2: tag6,
        name2: tag6,
        hidden: true
      })
      this.params.mtgApplecteur.addLineAB({
        a: tag5,
        b: tag6,
        thickness: 1,
        tag: tag4,
        color: 'green',
        hidden: true
      })
      this.params.mtgApplecteur.addIntLineLine({
        d: tag4,
        d2: tatagLa,
        tag: tag1,
        name: tag1,
        hiddenName: true,
        hidden: true
      })
      const ztag3 = this.scratchMtgNewTag(1)
      const ztag2 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOr({
        o: tag1,
        r: 0.6,
        thickness: 6,
        color: 'green',
        tag: ztag3,
        hidden: true
      })
      this.params.mtgApplecteur.addIntLineCircle({
        c: ztag3,
        d: tag4,
        pointStyle: 'bigmult',
        tag: ztag2,
        name: ztag2,
        hidden: true
      })
      this.params.mtgApplecteur.addLinkedText({
        text: nomLong,
        a: ztag2,
        y: 1,
        color: 'purple',
        hidden: false,
        tag: tag7,
        hAlign: 'center',
        vAlign: 'middle',
        transparent: true
      })
      const ames = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCalc({ nameCalc: ames, formula: long })
      // TODO dans ajoute contient, on doit test les autres qui le contiennent aussi
      this.params.donnesMtg.push({ type: 'point', tag: tatagPoint, noms: [ptariv], depend: [this.scratchMtgRetrouve({ nom: adonext1 }, 'num'), this.params.donnesMtg.length - 1], viz: getTrue, toShow })
      this.params.donnesMtg.push({ tag1: tatagPoint, tag2: this.scratchMtgRetrouve({ nom: adonext1 }, 'tag'), type: 'segment', tag: tatagLa, ext1: adonext1, ext2: ptariv, long, noms: ['[' + adonext1 + ptariv + ']', '[' + ptariv + adonext1 + ']'], depend: [this.scratchMtgRetrouve({ nom: adonext1 }, 'num')], viz: getTrue, toShow: [tatagLa, tag7], contient: [adonext1, ptariv], longueur: ames })
      this.addPointMesur(tatagPoint, 1)
      this.scratchMtgFinishOrdre()
    }
  }

  scratchMtgDroitePt (point1, point2) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    while (point1.indexOf('\\\'') !== -1) { point1 = point1.replace('\\\'', '\'') }
    while (point1.indexOf('\\"') !== -1) { point1 = point1.replace('\\"', '"') }
    while (point2.indexOf('\\\'') !== -1) { point2 = point2.replace('\\\'', '\'') }
    while (point2.indexOf('\\"') !== -1) { point2 = point2.replace('\\"', '"') }
    const tatag = this.scratchMtgNewTag(1)
    const toShow = [tatag]
    this.params.mtgApplecteur.addLineAB({
      a: this.scratchMtgRetrouve({ nom: point1 }, 'tag'),
      b: this.scratchMtgRetrouve({ nom: point2 }, 'tag'),
      thickness: 2,
      tag: tatag,
      color: 'black'
    })
    this.params.donnesMtg.push({ tag1: this.scratchMtgRetrouve({ nom: point1 }, 'tag'), tag2: this.scratchMtgRetrouve({ nom: point2 }, 'tag'), type: 'droite', tag: tatag, point1, point2, noms: ['(' + point1 + point2 + ')', '(' + point2 + point1 + ')'], depend: [this.scratchMtgRetrouve({ nom: point1 }, 'num'), this.scratchMtgRetrouve({ nom: point2 }, 'num')], viz: getTrue, toShow, contient: [point1, point2] })
    this.scratchMtgFinishOrdre()
  }

  scratchMtgDroitePara (nom, para, point) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
    while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
    while (para.indexOf('\\\'') !== -1) { para = para.replace('\\\'', '\'') }
    while (para.indexOf('\\"') !== -1) { para = para.replace('\\"', '"') }
    while (point.indexOf('\\\'') !== -1) { point = point.replace('\\\'', '\'') }
    while (point.indexOf('\\"') !== -1) { point = point.replace('\\"', '"') }
    const tatag = this.scratchMtgNewTag(1)
    const toShow = [tatag]
    this.params.mtgApplecteur.addLinePar({
      a: this.scratchMtgRetrouve({ nom: point }, 'tag'),
      d: this.scratchMtgRetrouve({ nom: para }, 'tag'),
      thickness: 2,
      tag: tatag,
      color: 'black',
      hiddenName: true
    })
    const tatagf = this.scratchMtgNewTag(1)
    const tatagf3 = this.scratchMtgNewTag(1)
    const tatagf2 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouve({ nom: point }, 'tag'),
      r: 2,
      tag: tatagf2,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatagf2,
      d: tatag,
      hidden: true,
      name: tatagf,
      tag: tatagf,
      name2: tatagf3,
      tag2: tatagf3
    })
    if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
      const tatag2 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOr({
        o: this.scratchMtgRetrouve({ nom: point }, 'tag'),
        r: 1,
        hidden: true,
        color: 'red',
        tag: tatag2
      })
      const tatag3 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLinePerp({
        a: this.scratchMtgRetrouve({ nom: point }, 'tag'),
        d: tatag,
        thickness: 2,
        tag: tatag3,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const tatag4 = this.scratchMtgNewTag(1)
      const tatag5 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addIntLineCircle({
        c: tatag2,
        d: tatag3,
        hidden: true,
        name: tatag4,
        name2: tatag5
      })
      const tatag7 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLinePerp({
        a: tatag5,
        d: tatag3,
        thickness: 2,
        tag: tatag7,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const dudeb = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatagf })
      const dudeb2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatagf3 })
      const tatag8 = this.scratchMtgNewTag(1)
      try {
        this.params.mtgApplecteur.addLinkedPointLine({
          absCoord: true,
          d: tatag7,
          color: 'white',
          name: tatag8,
          tag: tatag8,
          x: dudeb.x,
          y: dudeb.y,
          pointStyle: 'pixel',
          hiddenName: true
        })
      } catch (e) {
        this.params.mtgApplecteur.addLinkedPointLine({
          absCoord: true,
          d: tatag7,
          color: 'white',
          name: tatag8,
          tag: tatag8,
          x: dudeb2.x,
          y: dudeb2.y,
          pointStyle: 'pixel',
          hiddenName: true
        })
      }
      const tatag6 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLinkedText({
        text: nom,
        a: tatag8,
        color: '#7900c5',
        hidden: false,
        tag: tatag6,
        hAlign: 'center',
        vAlign: 'middle',
        transparent: true
      })
      toShow.push(tatag6, tatag8)
    }
    this.params.donnesMtg.push({ tag1: this.scratchMtgRetrouve({ nom: point }, 'tag'), tag2: tatagf, type: 'droite', tag: tatag, ext1: point, para, noms: [nom], depend: [this.scratchMtgRetrouve({ nom: point }, 'num'), this.scratchMtgRetrouve({ nom: para }, 'num')], viz: getTrue, toShow, contient: [point] })
    this.scratchMtgFinishOrdre()
  }

  scratchMtgDroitePerp (nom, perp, point) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const tatag = this.scratchMtgNewTag(1)
    const toShow = [tatag]
    try {
      this.params.mtgApplecteur.addLinePerp({
        a: this.scratchMtgRetrouve({ nom: point }, 'tag'),
        d: this.scratchMtgRetrouve({ nom: perp }, 'tag'),
        thickness: 2,
        tag: tatag,
        color: 'black',
        hiddenName: true
      })
    } catch (e) {
      const lxml = this.Blockly.Xml.workspaceToDom(this.params.scratchProgDonnees.workspace)
      j3pNotify('pour tom', {
        leprog: this.params.scratchProgDonnees.tabcodeX, Prog: this.Blockly.Xml.domToText(lxml), donnessMTG: this.params.donnesMtg
      })
      return
    }
    const tatagf = this.scratchMtgNewTag(1)
    const tatagf3 = this.scratchMtgNewTag(1)
    const tatagf2 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouve({ nom: point }, 'tag'),
      r: 2,
      tag: tatagf2,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatagf2,
      d: tatag,
      hidden: true,
      name: tatagf,
      tag: tatagf,
      name2: tatagf3,
      tag2: tatagf3
    })
    if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
      const tatag2 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOr({
        o: this.scratchMtgRetrouve({ nom: point }, 'tag'),
        r: 1,
        hidden: true,
        color: 'red',
        tag: tatag2
      })
      const tatag3 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLinePerp({
        a: this.scratchMtgRetrouve({ nom: point }, 'tag'),
        d: tatag,
        thickness: 2,
        tag: tatag3,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const tatag4 = this.scratchMtgNewTag(1)
      const tatag5 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addIntLineCircle({
        c: tatag2,
        d: tatag3,
        hidden: true,
        name: tatag4,
        name2: tatag5
      })
      const tatag7 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLinePerp({
        a: tatag5,
        d: tatag3,
        thickness: 2,
        tag: tatag7,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const dudeb = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatagf })
      const dudeb2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatagf3 })
      const tatag8 = this.scratchMtgNewTag(1)
      try {
        this.params.mtgApplecteur.addLinkedPointLine({
          absCoord: true,
          d: tatag7,
          color: 'white',
          name: tatag8,
          tag: tatag8,
          x: dudeb.x,
          y: dudeb.y,
          pointStyle: 'pixel',
          hiddenName: true
        })
      } catch (e) {
        this.params.mtgApplecteur.addLinkedPointLine({
          absCoord: true,
          d: tatag7,
          color: 'white',
          name: tatag8,
          tag: tatag8,
          x: dudeb2.x,
          y: dudeb2.y,
          pointStyle: 'pixel',
          hiddenName: true
        })
      }
      const tatag6 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLinkedText({
        text: nom,
        a: tatag8,
        color: '#7900c5',
        hidden: false,
        tag: tatag6,
        hAlign: 'center',
        vAlign: 'middle',
        transparent: true
      })
      toShow.push(tatag6, tatag8)
    }
    this.params.donnesMtg.push({ tag1: this.scratchMtgRetrouve({ nom: point }, 'tag'), tag2: tatagf, type: 'droite', tag: tatag, ext1: point, perp, noms: [nom], depend: [this.scratchMtgRetrouve({ nom: point }, 'num'), this.scratchMtgRetrouve({ nom: perp }, 'num')], viz: getTrue, toShow, contient: [point] })
    this.scratchMtgFinishOrdre()
  }

  scratchMtgMediatrice (nom, cont) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const ob = this.scratchMtgRetrouve({ nom: cont }, 'objet')
    const de1 = this.scratchMtgRetrouve({ nom: ob.ext1 }, 'tag')
    const de2 = this.scratchMtgRetrouve({ nom: ob.ext2 }, 'tag')
    const tatag = this.scratchMtgNewTag(1)
    const tatag2 = this.scratchMtgNewTag(1)
    const tatag3 = this.scratchMtgNewTag(1)
    const tatag4 = this.scratchMtgNewTag(1)
    const tatag5 = this.scratchMtgNewTag(1)
    const tatag6 = this.scratchMtgNewTag(1)
    const tatag7 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOA({
      o: de2,
      a: de1,
      hidden: true,
      tag: tatag2
    })
    this.params.mtgApplecteur.addCircleOA({
      o: de1,
      a: de2,
      hidden: true,
      tag: tatag3
    })
    this.params.mtgApplecteur.addIntCircleCircle({
      c: tatag3,
      c2: tatag2,
      pointStyle: 'bigmult',
      tag: tatag5,
      name: tatag5,
      tag2: tatag6,
      name2: tatag6,
      hidden: true
    })
    this.params.mtgApplecteur.addLineAB({
      a: tatag5,
      b: tatag6,
      thickness: 1,
      tag: tatag4,
      color: 'green',
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineLine({
      d: tatag4,
      d2: ob.tag,
      pointStyle: 'bigmult',
      tag: tatag7,
      name: tatag7,
      hiddenName: true,
      hidden: true
    })
    this.params.mtgApplecteur.addLinePerp({
      a: tatag7,
      d: this.scratchMtgRetrouve({ nom: cont }, 'tag'),
      thickness: 2,
      tag: tatag,
      color: 'black',
      hiddenName: true
    })
    const toShow = [tatag]
    const tatagf = this.scratchMtgNewTag(1)
    const tatagf2 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOr({
      o: tatag7,
      r: 2,
      tag: tatagf2,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatagf2,
      d: tatag,
      hidden: true,
      name: tatagf,
      tag: tatagf
    })
    if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
      const tatag2 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOr({
        o: tatag7,
        r: 1,
        hidden: true,
        color: 'red',
        tag: tatag2
      })
      const tatag3 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLinePerp({
        a: tatag7,
        d: tatag,
        thickness: 2,
        tag: tatag3,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const tatag4 = this.scratchMtgNewTag(1)
      const tatag5 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addIntLineCircle({
        c: tatag2,
        d: tatag3,
        hidden: true,
        name: tatag4,
        name2: tatag5
      })
      const tatag9 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLinePerp({
        a: tatag5,
        d: tatag3,
        thickness: 2,
        tag: tatag9,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const dudeb = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatagf })
      const tatag8 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLinkedPointLine({
        absCoord: true,
        d: tatag9,
        color: 'white',
        name: tatag8,
        tag: tatag8,
        x: dudeb.x,
        y: dudeb.y,
        pointStyle: 'pixel',
        hiddenName: true
      })
      const tatag6 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLinkedText({
        text: nom,
        a: tatag8,
        color: '#7900c5',
        hidden: false,
        tag: tatag6,
        hAlign: 'center',
        vAlign: 'middle',
        transparent: true
      })
      toShow.push(tatag6, tatag8)
    }
    this.params.donnesMtg.push({ tag1: tatag7, tag2: tatagf, type: 'droite', tag: tatag, noms: [nom], depend: [this.scratchMtgRetrouve({ nom: cont }, 'num')], viz: getTrue, toShow, contient: [] })
    this.scratchMtgFinishOrdre()
  }

  scratchMtgDemiDroitePt (origine, point, visible) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const tatag = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addRay({
      o: this.scratchMtgRetrouve({ nom: origine }, 'tag'),
      a: this.scratchMtgRetrouve({ nom: point }, 'tag'),
      thickness: 2,
      tag: tatag,
      color: 'black'
    })
    this.params.donnesMtg.push({ type: 'demidroite', tag: tatag, origine, point, noms: ['[' + origine + point + ')', '(' + point + origine + ']'], depend: [this.scratchMtgRetrouve({ nom: origine }, 'num'), this.scratchMtgRetrouve({ nom: point }, 'num')], viz: this._getIsVisible({ visible: visible !== false }), toShow: [tatag], contient: [origine, point] })
    this.scratchMtgFinishOrdre()
  }

  scratchMtgCercleCentrePoint (centre, point, nom, bool) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const tatag = this.scratchMtgNewTag(1)
    const toShow = [tatag]
    this.params.mtgApplecteur.addCircleOA({
      o: this.scratchMtgRetrouve({ nom: centre }, 'tag'),
      a: this.scratchMtgRetrouve({ nom: point }, 'tag'),
      thickness: 2,
      tag: tatag,
      color: 'black',
      hidden: Boolean(bool)
    })
    if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
      const tatag2 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLineAB({
        a: this.scratchMtgRetrouve({ nom: centre }, 'tag'),
        b: this.scratchMtgRetrouve({ nom: point }, 'tag'),
        hidden: true,
        tag: tatag2
      })
      const tatag3 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOr({
        o: this.scratchMtgRetrouve({ nom: point }, 'tag'),
        r: 1,
        hidden: true,
        color: 'red',
        tag: tatag3
      })
      const tatag4 = this.scratchMtgNewTag(1)
      const tatag5 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addIntLineCircle({
        d: tatag2,
        c: tatag3,
        hidden: true,
        name: tatag4,
        name2: tatag5
      })
      const tatag7 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOA({
        o: this.scratchMtgRetrouve({ nom: centre }, 'tag'),
        a: tatag4,
        thickness: 2,
        tag: tatag7,
        hidden: true,
        color: 'black'
      })
      const tatag8 = this.scratchMtgNewTag(1)
      const pla = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouve({ nom: centre }, 'tag') })
      let cpasplace
      let cmopt = 0
      do {
        cmopt++
        cpasplace = false
        try {
          this.params.mtgApplecteur.addLinkedPointCircle({
            absCoord: true,
            c: tatag7,
            color: 'white',
            name: tatag8,
            tag: tatag8,
            x: pla.x + Math.cos(cmopt * 180 / Math.PI),
            y: pla.y + Math.sin(cmopt * 180 / Math.PI),
            pointStyle: 'pixel',
            hiddenName: true
          })
        } catch (e) {
          cpasplace = true
        }
      } while (cpasplace && cmopt < 361)
      if (!cpasplace) {
        const tatag6 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addLinkedText({
          text: nom,
          a: tatag8,
          color: '#E55B16',
          hidden: false,
          tag: tatag6,
          hAlign: 'center',
          vAlign: 'middle',
          transparent: true
        })
        toShow.push(tatag6)
      }
    }
    this.params.donnesMtg.push({ type: 'cercle', tag: tatag, centre, point, noms: [nom], depend: [this.scratchMtgRetrouve({ nom: centre }, 'num'), this.scratchMtgRetrouve({ nom: point }, 'num')], viz: getTrue, toShow, contient: [point], speCache: bool })
    this.scratchMtgFinishOrdre()
  }

  scratchMtgCercleCentreRayon (centre, rayon, nom, nomrayon) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const tatag = this.scratchMtgNewTag(1)
    const tatag2 = this.scratchMtgNewTag(1)
    const tatag3 = this.scratchMtgNewTag(1)
    const tatag4 = this.scratchMtgNewTag(1)
    const tatag5 = this.scratchMtgNewTag(1)
    const tatag6 = this.scratchMtgNewTag(1)
    const tatag7 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouve({ nom: centre }, 'tag'),
      r: rayon,
      thickness: 2,
      tag: tatag,
      color: 'black'
    })
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouve({ nom: centre }, 'tag'),
      r: '(' + rayon + ')/2',
      thickness: 1,
      tag: tatag2,
      hidden: true
    })
    try {
      this.params.mtgApplecteur.addLinkedPointCircle({
        absCoord: true,
        c: tatag2,
        x: 192.5,
        y: 192.5,
        hidden: false,
        name: tatag3,
        tag: tatag3,
        hiddenName: true,
        color: 'white',
        pointStyle: 'pixel'
      })
    } catch (e) {
      this.params.scratchProgDonnees.yaErreurMtg = 'Le rayon de ce cercle est trop grand ! '
      this.scratchMtgFinishOrdre()
      return
    }
    this.params.mtgApplecteur.addLineAB({
      a: this.scratchMtgRetrouve({ nom: centre }, 'tag'),
      b: tatag3,
      thickness: 1,
      tag: tatag4,
      color: 'green',
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      d: tatag4,
      c: tatag,
      hidden: true,
      name: tatag5,
      tag: tatag5,
      hiddenName: true,
      color: 'red',
      pointStyle: 'biground'
    })
    this.params.mtgApplecteur.addSegment({
      a: this.scratchMtgRetrouve({ nom: centre }, 'tag'),
      b: tatag5,
      thickness: 2,
      tag: tatag6,
      color: 'green',
      lineStyle: 'dashdash'
    })
    const ztag1 = this.scratchMtgNewTag(1)
    const ztag2 = this.scratchMtgNewTag(1)
    const ztag3 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOr({
      o: tatag3,
      r: '0.6',
      thickness: 1,
      tag: ztag1,
      hidden: true
    })
    this.params.mtgApplecteur.addLinePerp({
      d: tatag4,
      a: tatag3,
      tag: ztag3,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      d: ztag3,
      c: ztag1,
      hidden: true,
      name: ztag2,
      tag: ztag2,
      hiddenName: true,
      color: 'red',
      pointStyle: 'biground'
    })
    this.params.mtgApplecteur.addLinkedText({
      text: nomrayon,
      a: ztag2,
      color: 'purple',
      hidden: false,
      tag: tatag7,
      hAlign: 'center',
      vAlign: 'middle',
      transparent: true
    })
    const toShow = [tatag, tatag6, tatag3, tatag7]
    if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
      const tatag2 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addLineAB({
        a: this.scratchMtgRetrouve({ nom: centre }, 'tag'),
        b: tatag5,
        hidden: true,
        tag: tatag2
      })
      const tatag3 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOr({
        o: tatag5,
        r: 1,
        hidden: true,
        color: 'red',
        tag: tatag3
      })
      const tatag4 = this.scratchMtgNewTag(1)
      const tag5 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addIntLineCircle({
        d: tatag2,
        c: tatag3,
        hidden: true,
        name: tatag4,
        name2: tag5
      })
      const tatag7 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOA({
        o: this.scratchMtgRetrouve({ nom: centre }, 'tag'),
        a: tatag4,
        thickness: 2,
        tag: tatag7,
        hidden: true,
        color: 'black'
      })
      const tatag8 = this.scratchMtgNewTag(1)

      const pla = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouve({ nom: centre }, 'tag') })
      let cpasplace
      let cmopt = 0
      do {
        cmopt++
        cpasplace = false
        try {
          this.params.mtgApplecteur.addLinkedPointCircle({
            absCoord: true,
            c: tatag7,
            color: 'white',
            name: tatag8,
            tag: tatag8,
            x: pla.x + Math.cos(cmopt * 180 / Math.PI),
            y: pla.y + Math.sin(cmopt * 180 / Math.PI),
            pointStyle: 'pixel',
            hiddenName: true
          })
        } catch (e) {
          cpasplace = true
        }
      } while (cpasplace && cmopt < 361)
      if (!cpasplace) {
        const tatag6 = this.scratchMtgNewTag(1)
        this.params.mtgApplecteur.addLinkedText({
          text: nom,
          a: tatag8,
          color: '#E55B16',
          hidden: false,
          tag: tatag6,
          hAlign: 'center',
          vAlign: 'middle',
          transparent: true
        })
        toShow.push(tatag6)
      }
    }
    this.params.donnesMtg.push({ type: 'cercle', tag: tatag, centre, rayon, noms: [nom], depend: [this.scratchMtgRetrouve({ nom: centre }, 'num')], viz: getTrue, toShow, contient: [] })
    this.scratchMtgFinishOrdre()
  }

  scratchMtgCercleDiametre (nom, cont) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const ob = this.scratchMtgRetrouve({ nom: cont }, 'objet')
    const de1 = this.scratchMtgRetrouve({ nom: ob.ext1 }, 'tag')
    const de2 = this.scratchMtgRetrouve({ nom: ob.ext2 }, 'tag')
    const tatag = this.scratchMtgNewTag(1)
    const tatag2 = this.scratchMtgNewTag(1)
    const tatag3 = this.scratchMtgNewTag(1)
    const tatag4 = this.scratchMtgNewTag(1)
    const tatag5 = this.scratchMtgNewTag(1)
    const tatag6 = this.scratchMtgNewTag(1)
    const tatag7 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOA({
      o: de2,
      a: de1,
      hidden: true,
      tag: tatag2
    })
    this.params.mtgApplecteur.addCircleOA({
      o: de1,
      a: de2,
      hidden: true,
      tag: tatag3
    })
    this.params.mtgApplecteur.addIntCircleCircle({
      c: tatag3,
      c2: tatag2,
      pointStyle: 'bigmult',
      tag: tatag5,
      name: tatag5,
      tag2: tatag6,
      name2: tatag6,
      hidden: true
    })
    this.params.mtgApplecteur.addLineAB({
      a: tatag5,
      b: tatag6,
      thickness: 1,
      tag: tatag4,
      color: 'green',
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineLine({
      d: tatag4,
      d2: ob.tag,
      pointStyle: 'bigmult',
      tag: tatag7,
      name: tatag7,
      hiddenName: true
    })
    this.params.mtgApplecteur.addCircleOA({
      o: tatag7,
      a: de1,
      hidden: false,
      tag: tatag,
      thickness: 2
    })

    const tag2 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addLineAB({
      a: tatag7,
      b: de1,
      hidden: true,
      tag: tag2
    })
    const tag3 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOr({
      o: de1,
      r: 1,
      hidden: true,
      color: 'red',
      tag: tag3
    })
    const tag4 = this.scratchMtgNewTag(1)
    const tag5 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addIntLineCircle({
      d: tag2,
      c: tag3,
      hidden: true,
      name: tag4,
      name2: tag5
    })
    const tag7 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOA({
      o: tatag7,
      a: tag4,
      thickness: 2,
      tag: tag7,
      hidden: true,
      color: 'black'
    })
    const tag8 = this.scratchMtgNewTag(1)
    const pouX = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: de1 })
    this.params.mtgApplecteur.addLinkedPointCircle({
      absCoord: true,
      c: tag7,
      color: 'white',
      name: tag8,
      tag: tag8,
      x: pouX.x + 20,
      y: pouX.y,
      pointStyle: 'pixel',
      hiddenName: true
    })
    const tag6 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addLinkedText({
      text: nom,
      a: tag8,
      color: '#E55B16',
      hidden: false,
      tag: tag6,
      hAlign: 'center',
      vAlign: 'middle',
      transparent: true
    })

    const toShow = [tatag, tag6]
    this.params.donnesMtg.push({
      type: 'cercle',
      tag: tatag,
      noms: [nom],
      depend: [this.scratchMtgRetrouve({ nom: ob.noms[0] }, 'num')],
      viz: getTrue,
      toShow,
      sur: []
    })
    this.scratchAjouteContient(1, ob.noms[0], nom)
    this.scratchMtgFinishOrdre()
  }

  scratchMtgarc1PointPasse (point, ext1, ext2) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const tatag = this.scratchMtgNewTag(1)
    const tatagbis = this.scratchMtgNewTag(1)
    const tatag1 = this.scratchMtgNewTag(1)
    const tatag2 = this.scratchMtgNewTag(1)
    const tatag3 = this.scratchMtgNewTag(1)
    const tatag4 = this.scratchMtgNewTag(1)
    const tatag5 = this.scratchMtgNewTag(1)
    const tatag6 = this.scratchMtgNewTag(1)
    const tatag7 = this.scratchMtgNewTag(1)
    const tatag8 = this.scratchMtgNewTag(1)
    const tatag9 = this.scratchMtgNewTag(1)
    const tatag10 = this.scratchMtgNewTag(1)
    const tatag11 = this.scratchMtgNewTag(1)
    const tatag12 = this.scratchMtgNewTag(1)

    const A = this.scratchMtgRetrouve({ nom: ext1 }, 'tag')
    const B = this.scratchMtgRetrouve({ nom: ext2 }, 'tag')
    const M = this.scratchMtgRetrouve({ nom: point }, 'tag')

    this.params.mtgApplecteur.addCircleOA({
      o: A,
      a: M,
      tag: tatag1,
      hidden: true
    })
    this.params.mtgApplecteur.addCircleOA({
      a: A,
      o: M,
      tag: tatag2,
      hidden: true
    })
    this.params.mtgApplecteur.addIntCircleCircle({
      c: tatag2,
      c2: tatag1,
      tag: tatag3,
      name: tatag3,
      tag2: tatag4,
      name2: tatag4,
      hidden: true
    })
    this.params.mtgApplecteur.addLineAB({
      a: tatag3,
      b: tatag4,
      tag: tatag9,
      hidden: true
    })

    this.params.mtgApplecteur.addCircleOA({
      o: B,
      a: M,
      tag: tatag5,
      hidden: true
    })
    this.params.mtgApplecteur.addCircleOA({
      a: B,
      o: M,
      tag: tatag6,
      hidden: true
    })
    this.params.mtgApplecteur.addIntCircleCircle({
      c: tatag5,
      c2: tatag6,
      tag: tatag7,
      name: tatag7,
      tag2: tatag8,
      name2: tatag8,
      hidden: true
    })
    this.params.mtgApplecteur.addLineAB({
      a: tatag7,
      b: tatag8,
      tag: tatag10,
      hidden: true
    })

    this.params.mtgApplecteur.addIntLineLine({
      d: tatag9,
      d2: tatag10,
      tag: tatag11,
      name: tatag11,
      hidden: true
    })

    this.params.mtgApplecteur.addArcOAB({
      o: tatag11,
      a: A,
      b: B,
      thickness: 2,
      tag: tatag
    })
    this.params.mtgApplecteur.addArcMajorOAB({
      o: tatag11,
      a: A,
      b: B,
      thickness: 2,
      tag: tatagbis
    })
    this.params.mtgApplecteur.addLengthMeasure({ a: A, b: B })
    this.params.mtgApplecteur.addLengthMeasure({ a: A, b: M })
    this.params.mtgApplecteur.addLengthMeasure({ a: B, b: M })
    this.params.mtgApplecteur.addCalc({ nameCalc: tatag12, formula: A + B + '*' + A + B + '-' + A + M + '*' + A + M + '-' + B + M + '*' + B + M })
    const ff = () => {
      const lecal = this.params.mtgApplecteur.valueOf(this.params.svgId, tatag12)
      if (lecal > 0) { return 0 } else { return 1 }
    }
    this.params.donnesMtg.push({ funcArc: ff, type: 'arc', ext1, ext2, tag: tatag, point, noms: ['arc ' + ext1 + ext2, 'arc ' + ext2 + ext1], depend: [this.scratchMtgRetrouve({ nom: point }, 'num'), this.scratchMtgRetrouve({ nom: ext1 }, 'num'), this.scratchMtgRetrouve({ nom: ext2 }, 'num')], viz: getTrue, toShow: [tatag, tatagbis], contient: [point, ext1, ext2] })
    this.scratchMtgFinishOrdre()
  }

  scratchMtgarccentre (centre, ext1, ext2, sens) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const tatag = this.scratchMtgNewTag(1)
    const lafuncAr = (sens) ? this.params.mtgApplecteur.addArcDirectOAB : this.params.mtgApplecteur.addArcIndirectOAB
    lafuncAr({
      o: this.scratchMtgRetrouve({ nom: centre }, 'tag'),
      a: this.scratchMtgRetrouve({ nom: ext1 }, 'tag'),
      b: this.scratchMtgRetrouve({ nom: ext2 }, 'tag'),
      color: 'black',
      hidden: false,
      tag: tatag,
      thickness: 2
    })
    this.params.donnesMtg.push({ type: 'arc', ext1, ext2, tag: tatag, centre, noms: ['arc ' + ext1 + ext2, 'arc ' + ext2 + ext1], depend: [this.scratchMtgRetrouve({ nom: centre }, 'num'), this.scratchMtgRetrouve({ nom: ext1 }, 'num'), this.scratchMtgRetrouve({ nom: ext2 }, 'num')], viz: getTrue, toShow: [tatag], contient: [ext1, ext2], sens })
    this.scratchMtgFinishOrdre()
  }

  scratchMtanglePlus (centre, ext1, ptariv, mesure, sens) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const tatag2 = this.scratchMtgNewTag(1)
    const tatag3 = this.scratchMtgNewTag(1)
    const tatag4 = this.scratchMtgNewTag(1)
    const tatag5 = this.scratchMtgNewTag(1)
    const tatag6 = this.scratchMtgNewTag(1)
    const tatag7 = this.scratchMtgNewTag(1)
    const tatag8 = this.scratchMtgNewTag(1)
    const tatag9 = this.scratchMtgNewTag(1)
    const tatag10 = this.scratchMtgNewTag(1)
    const tatag11 = this.scratchMtgNewTag(1)
    const tatag12 = this.scratchMtgNewTag(1)
    const tatag13 = this.scratchMtgNewTag(1)
    const tatag14 = this.scratchMtgNewTag(1)
    const tatag15 = this.scratchMtgNewTag(1)
    const tatag16 = this.scratchMtgNewTag(1)
    const tatag17 = this.scratchMtgNewTag(1)
    const tatag18 = this.scratchMtgNewTag(1)
    const tatag19 = this.scratchMtgNewTag(1)
    const tatag20 = this.scratchMtgNewTag(1)
    const tatag21 = this.scratchMtgNewTag(1)
    const tatag22 = this.scratchMtgNewTag(1)
    const tatag23 = this.scratchMtgNewTag(1)
    const tatag24 = this.scratchMtgNewTag(1)
    const tatag25 = this.scratchMtgNewTag(1)
    const tatag26 = this.scratchMtgNewTag(1)
    const tatag27 = this.scratchMtgNewTag(1)
    const tatag28 = this.scratchMtgNewTag(1)
    const tatag29 = this.scratchMtgNewTag(1)
    const tatag30 = this.scratchMtgNewTag(1)
    const tatag31 = this.scratchMtgNewTag(1)

    const o = this.scratchMtgRetrouve({ nom: centre }, 'tag')
    const a = this.scratchMtgRetrouve({ nom: ext1 }, 'tag')

    // todo r de circleor accepte pas un cacul
    this.params.mtgApplecteur.addLengthMeasure({ a: o, b: a })
    this.params.mtgApplecteur.addCalc({ nameCalc: tatag2, formula: '2*' + o + a + '*sin(' + mesure + '/2)' })
    this.params.mtgApplecteur.addCalc({ nameCalc: tatag3, formula: o + a })

    this.params.mtgApplecteur.addCircleOr({
      o,
      r: tatag3,
      color: 'red',
      tag: tatag4,
      hidden: true
    })
    this.params.mtgApplecteur.addCircleOr({
      o: a,
      r: tatag2,
      color: 'red',
      tag: tatag5,
      hidden: true
    })
    this.params.mtgApplecteur.addIntCircleCircle({
      c: tatag4,
      c2: tatag5,
      color: 'red',
      tag: tatag6,
      name: tatag6,
      tag2: tatag7,
      name2: tatag7,
      hidden: true
    })
    const tagGarde = (sens) ? tatag7 : tatag6
    this.params.mtgApplecteur.addRay({
      o,
      a: tagGarde,
      color: 'black',
      tag: tatag8,
      thickness: 2
    })
    const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tagGarde })
    try {
      this.params.mtgApplecteur.addLinkedPointLine({
        absCoord: true,
        d: tatag8,
        x: loch.x,
        y: loch.y,
        tag: tatag9,
        name: tatag9,
        pointStyle: 'bigmult',
        hiddenName: true
      })
    } catch (e) {
      this.params.scratchProgDonnees.yaErreurMtg = 'Le point ' + ptariv + ' est en dehors de la figure ! '
      this.scratchMtgFinishOrdre()
      return
    }
    const toShow = [tatag8, tatag30, tatag31, tatag20]
    if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(ptariv) === -1) {
      const tag2 = this.scratchMtgNewTag(1)
      const tag3 = this.scratchMtgNewTag(1)
      const tag4 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOr({
        o: tatag9,
        r: 1,
        hidden: true,
        tag: tag2
      })
      this.params.mtgApplecteur.addLinkedPointCircle({
        absCoord: true,
        c: tag2,
        x: 1,
        y: 1,
        pointStyle: 'pixel',
        color: 'white',
        hidden: false,
        tag: tag3,
        name: tag3,
        hiddenName: true
      })
      this.params.mtgApplecteur.addLinkedText({
        text: ptariv,
        a: tag3,
        color: 'blue',
        hidden: false,
        tag: tag4,
        hAlign: 'center',
        vAlign: 'middle',
        transparent: true
      })
      const toShow2 = [tatag9, tag4]
      this.params.donnesMtg.push({ type: 'point', tag: tag3, noms: [ptariv], depend: [this.scratchMtgRetrouve({ nom: centre }, 'num'), this.scratchMtgRetrouve({ nom: ext1 }, 'num')], viz: getTrue, toShow: toShow2, sur: [] })
    }

    this.params.mtgApplecteur.addCircleOr({
      o,
      r: 2,
      color: 'red',
      tag: tatag10,
      hidden: true
    })
    this.params.mtgApplecteur.addCircleOr({
      o,
      r: 5,
      color: 'red',
      tag: tatag11,
      hidden: true
    })
    this.params.mtgApplecteur.addLengthMeasure({ a: tagGarde, b: a })
    this.params.mtgApplecteur.addCalc({ nameCalc: tatag12, formula: tagGarde + a + '/2' })
    this.params.mtgApplecteur.addCircleOr({
      o: a,
      r: tatag12,
      color: 'green',
      tag: tatag13,
      hidden: true
    })
    this.params.mtgApplecteur.addSegment({
      a,
      b: tagGarde,
      color: 'green',
      tag: tatag14,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatag13,
      d: tatag14,
      color: 'green',
      tag: tatag15,
      name: tatag15,
      hidden: true
    })
    this.params.mtgApplecteur.addRay({
      o,
      a: tatag15,
      color: 'black',
      tag: tatag16,
      thickness: 5,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatag11,
      d: tatag16,
      color: 'green',
      tag: tatag17,
      name: tatag17,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatag10,
      d: tatag16,
      color: 'green',
      tag: tatag18,
      name: tatag18,
      hidden: true
    })
    this.params.mtgApplecteur.addSegment({
      a: tatag17,
      b: tatag18,
      color: 'green',
      tag: tatag19,
      hidden: true
    })
    const loch2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag17 })
    this.params.mtgApplecteur.addLinkedPointLine({
      absCoord: true,
      d: tatag19,
      x: loch2.x,
      y: loch2.y,
      tag: tatag20,
      name: tatag20,
      pointStyle: 'pixel',
      hiddenName: true,
      color: 'white'
    })
    this.params.mtgApplecteur.addCircleOr({
      o: tatag20,
      r: 1,
      color: 'green',
      tag: tatag21,
      hidden: true
    })
    this.params.mtgApplecteur.addCircleOr({
      o,
      r: 0.9,
      color: 'red',
      tag: tatag22,
      hidden: true
    })
    this.params.mtgApplecteur.addCircleOr({
      o,
      r: 4.1,
      color: 'red',
      tag: tatag23,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatag22,
      d: tatag16,
      color: 'green',
      tag: tatag24,
      name: tatag24,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatag23,
      d: tatag16,
      color: 'green',
      tag: tatag25,
      name: tatag25,
      hidden: true
    })
    this.params.mtgApplecteur.addSegment({
      a: tatag24,
      b: tatag25,
      color: 'green',
      tag: tatag26,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatag21,
      d: tatag26,
      color: 'green',
      tag2: tatag27,
      name2: tatag27,
      hidden: true
    })

    this.params.mtgApplecteur.addLinePerp({
      d: tatag16,
      a: tatag27,
      tag: tatag28,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineLine({
      d: tatag28,
      d2: tatag8,
      color: 'green',
      tag: tatag29,
      name: tatag29,
      hidden: true
    })
    this.params.mtgApplecteur.addArcOAB({
      o,
      a: tatag29,
      b: a,
      color: 'purple',
      tag: tatag30
    })
    this.params.mtgApplecteur.addLinkedText({
      a: tatag20,
      text: mesure + ' °',
      color: 'purple',
      tag: tatag31,
      vAlign: 'middle',
      hAlign: 'center',
      transparent: true
    })

    this.params.donnesMtg.push({ type: 'demidroite', tag: tatag8, origine: centre, point: ptariv, noms: ['[' + centre + ptariv + ')', '(' + ptariv + centre + ']'], depend: [this.scratchMtgRetrouve({ nom: centre }, 'num'), this.scratchMtgRetrouve({ nom: ext1 }, 'num')], viz: getTrue, toShow })
    this.scratchMtgFinishOrdre()
  }

  scratchMtgTriangleEquilateral (ext1, ext2, ext3, ptAplace) {
    this.params.mtgApplecteur.setApiDoc(this.params.svgId)
    const pdeb1 = (ext1 !== ptAplace) ? ext1 : ext3
    const pdeb2 = (ext2 !== ptAplace) ? ext2 : ext3
    const tatag2 = this.scratchMtgNewTag(1)
    const tatag3 = this.scratchMtgNewTag(1)
    const tatag4 = this.scratchMtgNewTag(1)
    const tatag5 = this.scratchMtgNewTag(1)
    this.params.mtgApplecteur.addCircleOA({
      o: this.scratchMtgRetrouve({ nom: pdeb1 }, 'tag'),
      a: this.scratchMtgRetrouve({ nom: pdeb2 }, 'tag'),
      thickness: 1,
      tag: tatag2,
      color: 'red',
      hidden: false
    })
    this.params.mtgApplecteur.addCircleOA({
      o: this.scratchMtgRetrouve({ nom: pdeb2 }, 'tag'),
      a: this.scratchMtgRetrouve({ nom: pdeb1 }, 'tag'),
      thickness: 1,
      tag: tatag3,
      color: 'red',
      hidden: false
    })
    this.params.mtgApplecteur.addIntCircleCircle({
      c: tatag2,
      c2: tatag3,
      color: 'red',
      tag: tatag4,
      name: tatag4,
      tag2: tatag5,
      name2: tatag5,
      hidden: false,
      hiddenName: true
    })

    const avire = []
    const InterPos = [tatag4, tatag5]
    for (let i = InterPos.length - 1; i > -1; i--) {
      const isViz = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: InterPos[i] })
      if (this.scratchMtgEnDehors(isViz)) {
        avire.push(InterPos[i])
        InterPos.splice(i, 1)
        if (InterPos.length === 0) {
          this.scratchfautStop('Le 3e point n’est pas visible !')
          return
        }
      }
    }
    if (avire.length > 0) this.params.mtgApplecteur.deleteElt({ elt: avire[0] })
    if (this.params.isEditing || this.params.isSpeed) {
      if (InterPos.length > 1) {
        const iasup = j3pGetRandomInt(0, 1)
        const asup = InterPos[iasup]
        InterPos.splice(iasup, 1)
        this.params.mtgApplecteur.setHidden({ elt: asup })
        this.params.mtgApplecteur.deleteElt({ elt: asup })
      }
    }
    if (InterPos.length === 1) {
      this.scratchMtgFinishTriangleEquilateral(InterPos[0], ptAplace, pdeb1, pdeb2)
    } else {
      this.scratchMtgMessage('Sélectionne le point ' + ptAplace)
      const tagop1 = this.scratchMtgNewTag(1)
      const tagop2 = this.scratchMtgNewTag(1)
      this.params.mtgApplecteur.addCircleOr({
        o: InterPos[0],
        r: 0.5,
        thickness: 6,
        color: 'red',
        tag: tagop1
      })
      this.params.mtgApplecteur.addCircleOr({
        o: InterPos[1],
        r: 0.5,
        thickness: 6,
        color: 'red',
        tag: tagop2
      })
      const co1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: InterPos[0] })
      const co2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: InterPos[1] })
      this._mtgOnMove = (event) => {
        let lx = event.clientX
        let ly = event.clientY
        if (event.touches) {
          if (event.touches[0]) {
            lx = event.touches[0].clientX
            ly = event.touches[0].clientY
          }
        }
        if (!lx || !ly) return
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        const loch1 = this.dist({ x: lx - cooSvgId.x, y: ly - cooSvgId.y }, co1)
        const loch2 = this.dist({ x: lx - cooSvgId.x, y: ly - cooSvgId.y }, co2)
        const cool2 = (loch1 > loch2) ? 'red' : 'black'
        const cool1 = (loch1 < loch2) ? 'red' : 'black'
        this.params.mtgApplecteur.setColor({ elt: tagop1, color: cool1 })
        this.params.mtgApplecteur.setColor({ elt: InterPos[0], color: cool1 })
        this.params.mtgApplecteur.setColor({ elt: tagop2, color: cool2 })
        this.params.mtgApplecteur.setColor({ elt: InterPos[1], color: cool2 })
      }
      this._mtgOnDown = (event) => {
        let lx = event.clientX
        let ly = event.clientY
        if (event.touches) {
          if (event.touches[0]) {
            lx = event.touches[0].clientX
            ly = event.touches[0].clientY
          }
        }
        if (!lx || !ly) return
        this._mtgOnMove = dummyFn
        this._mtgOnDown = dummyFn
        this.scratchMtgClean()
        const cooSvgId = this.params.leSvg.getBoundingClientRect()
        const loch1 = this.dist({ x: lx - cooSvgId.x, y: ly - cooSvgId.y }, co1)
        const loch2 = this.dist({ x: lx - cooSvgId.x, y: ly - cooSvgId.y }, co2)
        const tatagLa = (loch1 < loch2) ? InterPos[0] : InterPos[1]
        const tatagAef = (loch1 > loch2) ? InterPos[0] : InterPos[1]
        this.params.mtgApplecteur.deleteElt({ elt: tagop1 })
        this.params.mtgApplecteur.deleteElt({ elt: tagop2 })
        this.params.mtgApplecteur.deleteElt({ elt: tatagAef })
        this.params.mtgApplecteur.setColor({ elt: tatagLa, color: 'black' })
        const toShow = [tatagLa]
        if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(ptAplace) === -1) {
          const tag3 = this.scratchMtgNewTag(1)
          const tag2 = this.scratchMtgNewTag(1)
          const tag4 = this.scratchMtgNewTag(1)
          this.params.mtgApplecteur.addCircleOr({
            o: tatagLa,
            r: 1,
            hidden: true,
            tag: tag2
          })
          this.params.mtgApplecteur.addLinkedPointCircle({
            absCoord: true,
            c: tag2,
            x: 1,
            y: 1,
            pointStyle: 'pixel',
            color: 'white',
            hidden: false,
            tag: tag3,
            name: tag3,
            hiddenName: true
          })
          this.params.mtgApplecteur.addLinkedText({
            text: ptAplace,
            a: tag3,
            color: 'blue',
            hidden: false,
            tag: tag4,
            hAlign: 'center',
            vAlign: 'middle',
            transparent: true
          })
          toShow.push(tag3, tag4)
        }
        this.params.donnesMtg.push({ type: 'point', tag: tatagLa, noms: [ptAplace], depend: [this.scratchMtgRetrouve({ nom: pdeb1 }, 'num'), this.scratchMtgRetrouve({ nom: pdeb2 }, 'num')], viz: getTrue, toShow, sur: [] })
        // TODO dans ajoute contient, on doit test les autres qui le contiennent aussi
        this.scratchMtgFinishOrdre()
      }
    }
  }

  scratchMtgFinishTriangleEquilateral (inter, nom, ob1, ob2) {
    const toShow = [inter]
    this.params.mtgApplecteur.setColor({ elt: inter, color: 'black' })
    if (this.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
      const tag3 = this.scratchMtgNewTag(1)
      const tag4 = this.scratchMtgNewTag(1)
      const tag2 = this.scratchMtgNewTag(1)
      const cooSvgId = this.params.leSvg.getBoundingClientRect()
      this.tryPlaceNom(inter, tag2, tag3, tag4, nom, cooSvgId, toShow)
    }
    this.params.donnesMtg.push({ type: 'point', tag: inter, noms: [nom], depend: [this.scratchMtgRetrouve({ nom: ob1 }, 'num'), this.scratchMtgRetrouve({ nom: ob2 }, 'num')], viz: getTrue, toShow, sur: [] })
    this.scratchMtgFinishOrdre()
  }

  scratchFaisZone (num, eel) {
    let donnesMtg
    switch (num) {
      case 1:
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        donnesMtg = this.params.donnesMtg3
        break
    }
    donnesMtg.push({ type: 'zone', noms: [eel.noms[0]], depend: [], viz: getTrue, toShow: [], sur: [], c: eel.c, p: eel.p })
    if (num === 2 && this.params.isEditing) {
      this.ajZone(eel)
    }
  }

  scratchFaisZoneSeg (num, eel) {
    let donnesMtg
    switch (num) {
      case 1:
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        donnesMtg = this.params.donnesMtg3
        break
    }
    donnesMtg.push({ type: 'zoneSeg', noms: [eel.noms[0]], depend: [], viz: getTrue, toShow: [], sur: [], c: eel.c, p: eel.p })
    if (num === 2 && this.params.isEditing) {
      this.ajZoneSeg(eel)
    }
  }

  scratchFaisPoint (num, eel2, fix) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    const tatag = this.scratchMtgNewTag(num)
    const eel = this.scratchMtgReplaceLettres(eel2)
    this.params.mtgApplecteur.setApiDoc(svgId)
    if (eel.sur) {
      if (eel.sur[0].indexOf('zone') !== -1) {
        const ob1 = this.scratchMtgRetrouveBase(num, { nom: eel.sur[0] }, 'objet')
        const coef = (eel2.coef) ? eel2.coef : j3pGetRandomInt(0, 100)
        eel2.coef = coef
        if (eel.sur[0].indexOf('zoneSeg') !== -1) {
          eel.x = (coef * ob1.c.x + (100 - coef) * ob1.p.x) / 100
          eel.y = (coef * ob1.c.y + (100 - coef) * ob1.p.y) / 100
        } else {
          const ladist = this.dist(ob1.c, ob1.p)
          eel.x = coef / 100 * ladist * Math.cos((10 * coef) / 180 * Math.PI) + ob1.c.x
          eel.y = coef / 100 * ladist * Math.sin((10 * coef) / 180 * Math.PI) + ob1.c.y
        }
        this.params.mtgApplecteur.addFreePoint({
          absCoord: true,
          x: eel.x,
          y: eel.y,
          tag: tatag,
          name: tatag,
          pointStyle: 'bigmult',
          hiddenName: true,
          color: (fix) ? 'green' : 'black'
        })
        fix = true
        donnesMtg.push({ classe: 'l', type: 'point', tag: tatag, noms: eel.noms, depend: [], viz: this._getIsVisible(eel), toShow: [tatag], sur: [] })
        this.addPointMesur(tatag, num)
      } else {
        const ob1 = this.scratchMtgRetrouveBase(num, { nom: eel.sur[0] }, 'objet')
        let fg1, fg2
        if (ob1.para || ob1.perp) {
          fg1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: ob1.tag1 })
          fg2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: ob1.tag2 })
          eel.coef = 0.5
        } else if (ob1.type === 'cercle') {
          fg1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouveBase(num, { nom: ob1.centre }, 'tag') })
        } else {
          fg1 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouveBase(num, { nom: eel.sur[0][1] }, 'tag') })
          fg2 = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouveBase(num, { nom: eel.sur[0][2] }, 'tag') })
        }
        if (ob1.type === 'cercle' || ob1.type === 'arc') {
          eel.coef = eel.coef || j3pGetRandomInt(0, 360)
          let ok = false
          let cpt = 0
          while (!ok && cpt < 360) {
            try {
              this.params.mtgApplecteur.addLinkedPointCircle({
                absCoord: true,
                c: ob1.tag,
                x: fg1.x + Math.cos(eel.coef * 180 / Math.PI),
                y: fg1.y + Math.sin(eel.coef * 180 / Math.PI),
                pointStyle: 'bigmult',
                color: 'black',
                hidden: false,
                tag: tatag,
                name: tatag,
                hiddenName: true
              })
              ok = true
            } catch (e) {
              cpt++
              eel.coef++
            }
          }
        } else {
          this.params.mtgApplecteur.addLinkedPointLine({
            absCoord: true,
            d: ob1.tag,
            x: (eel.coef * fg1.x + (1 - eel.coef) * fg2.x),
            y: (eel.coef * fg1.y + (1 - eel.coef) * fg2.y),
            pointStyle: 'bigmult',
            color: 'black',
            hidden: false,
            hiddenName: true,
            tag: tatag,
            name: tatag
          })
        }
        donnesMtg.push({ classe: 'sl', type: 'point', tag: tatag, noms: [eel.noms[0]], depend: [this.scratchMtgRetrouveBase(num, { nom: eel.sur[0] }, 'num')], viz: this._getIsVisible(eel), toShow: [tatag], sur: [this.scratchMtgRetrouveBase(num, { nom: eel.sur[0] }, 'num')] })
        this.addPointMesur(tatag, num)
      }
    } else {
      if (!eel.x) eel.x = j3pGetRandomInt(20, 350)
      if (!eel.y) eel.y = j3pGetRandomInt(20, 350)
      this.params.mtgApplecteur.addFreePoint({
        absCoord: true,
        x: eel.x,
        y: eel.y,
        tag: tatag,
        name: tatag,
        pointStyle: 'bigmult',
        hiddenName: true,
        color: (fix) ? 'green' : 'black'
      })
      donnesMtg.push({ classe: 'l', type: 'point', tag: tatag, noms: eel.noms, depend: [], viz: this._getIsVisible(eel), toShow: [tatag], sur: [], efface: eel.efface })
      this.addPointMesur(tatag, num)
    }
    if (eel.efface !== true) {
      const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag })
      const tag2 = this.scratchMtgNewTag(num)
      const tag3 = this.scratchMtgNewTag(num)
      const tag4 = this.scratchMtgNewTag(num)
      this.tryPlaceNom(tatag, tag2, tag3, tag4, eel.noms[0], loch, donnesMtg[donnesMtg.length - 1].toShow)
      donnesMtg[donnesMtg.length - 1].fix = fix
      if (fix) { donnesMtg[donnesMtg.length - 1].cooGarde = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag }) }
    }
  }

  scratchFaisPointSym (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    const de1 = this.scratchMtgRetrouveBase(num, { nom: eel.centre }, 'tag')
    const de2 = this.scratchMtgRetrouveBase(num, { nom: eel.point }, 'tag')
    const tatag = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addImPointSymCent({
      o: de1,
      a: de2,
      pointStyle: 'bigmult',
      tag: tatag,
      name: tatag,
      hiddenName: true
    })
    const toShow = [tatag]
    const tag2 = this.scratchMtgNewTag(num)
    const tag3 = this.scratchMtgNewTag(num)
    const tag4 = this.scratchMtgNewTag(num)
    const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag })
    this.params.mtgApplecteur.addCircleOr({
      o: tatag,
      r: 1,
      hidden: true,
      tag: tag2
    })
    this.params.mtgApplecteur.addLinkedPointCircle({
      absCoord: true,
      c: tag2,
      x: loch.x + 1,
      y: loch.y - this.params.longUnit,
      pointStyle: 'pixel',
      color: 'white',
      hidden: false,
      tag: tag3,
      name: tag3,
      hiddenName: true
    })
    this.params.mtgApplecteur.addLinkedText({
      text: eel.noms[0],
      a: tag3,
      y: 1,
      color: 'blue',
      hidden: false,
      tag: tag4,
      hAlign: 'center',
      vAlign: 'middle',
      transparent: true
    })
    toShow.push(tag3, tag4)
    donnesMtg.push({ p1: de1, p2: de2, type: 'pointSym', tag: tatag, noms: [eel.noms[0]], depend: [this.scratchMtgRetrouveBase(num, { nom: eel.centre }, 'num'), this.scratchMtgRetrouveBase(num, { nom: eel.point }, 'num')], viz: getTrue, toShow, sur: [] })
    this.addPointMesur(tatag, num)
  }

  scratchFaisPointRot (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    const de1 = this.scratchMtgRetrouveBase(num, { nom: eel.centre }, 'tag')
    const de2 = this.scratchMtgRetrouveBase(num, { nom: eel.point }, 'tag')
    const tatag = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addImPointRotation({
      o: de1,
      a: de2,
      x: eel.angle,
      pointStyle: 'bigmult',
      tag: tatag,
      name: tatag,
      hiddenName: true
    })
    const toShow = [tatag]
    const tag2 = this.scratchMtgNewTag(num)
    const tag3 = this.scratchMtgNewTag(num)
    const tag4 = this.scratchMtgNewTag(num)
    const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag })
    this.params.mtgApplecteur.addCircleOr({
      o: tatag,
      r: 1,
      hidden: true,
      tag: tag2
    })
    this.params.mtgApplecteur.addLinkedPointCircle({
      absCoord: true,
      c: tag2,
      x: loch.x + 1,
      y: loch.y - this.params.longUnit,
      pointStyle: 'pixel',
      color: 'white',
      hidden: false,
      tag: tag3,
      name: tag3,
      hiddenName: true
    })
    this.params.mtgApplecteur.addLinkedText({
      text: eel.noms[0],
      a: tag3,
      y: 1,
      color: 'blue',
      hidden: false,
      tag: tag4,
      hAlign: 'center',
      vAlign: 'middle',
      transparent: true
    })
    toShow.push(tag3, tag4)
    donnesMtg.push({ p1: de1, p2: de2, type: 'pointSym', tag: tatag, noms: [eel.noms[0]], depend: [this.scratchMtgRetrouveBase(num, { nom: eel.centre }, 'num'), this.scratchMtgRetrouveBase(num, { nom: eel.point }, 'num')], viz: getTrue, toShow, sur: [] })
    this.addPointMesur(tatag, num)
  }

  scratchFaisPointPointMilieu (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    const ob = this.scratchMtgRetrouveBase(num, { nom: '[' + eel.cont + ']' }, 'objet')
    const de1 = this.scratchMtgRetrouveBase(num, { nom: ob.ext1 }, 'tag')
    const de2 = this.scratchMtgRetrouveBase(num, { nom: ob.ext2 }, 'tag')
    const tatag = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.setApiDoc(svgId)
    const tatag2 = this.scratchMtgNewTag(num)
    const tatag3 = this.scratchMtgNewTag(num)
    const tatag4 = this.scratchMtgNewTag(num)
    const tatag5 = this.scratchMtgNewTag(num)
    const tatag6 = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCircleOA({
      o: de2,
      a: de1,
      hidden: true,
      tag: tatag2
    })
    this.params.mtgApplecteur.addCircleOA({
      o: de1,
      a: de2,
      hidden: true,
      tag: tatag3
    })
    this.params.mtgApplecteur.addIntCircleCircle({
      c: tatag3,
      c2: tatag2,
      pointStyle: 'bigmult',
      tag: tatag5,
      name: tatag5,
      tag2: tatag6,
      name2: tatag6,
      hidden: true
    })
    this.params.mtgApplecteur.addLineAB({
      a: tatag5,
      b: tatag6,
      thickness: 1,
      tag: tatag4,
      color: 'green',
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineLine({
      d: tatag4,
      d2: ob.tag,
      pointStyle: 'bigmult',
      tag: tatag,
      name: tatag,
      hiddenName: true
    })
    const toShow = [tatag]
    const tag2 = this.scratchMtgNewTag(num)
    const tag3 = this.scratchMtgNewTag(num)
    const tag4 = this.scratchMtgNewTag(num)
    const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag })
    this.params.mtgApplecteur.addCircleOr({
      o: tatag,
      r: 1,
      hidden: true,
      tag: tag2
    })
    this.params.mtgApplecteur.addLinkedPointCircle({
      absCoord: true,
      c: tag2,
      x: loch.x + 1,
      y: loch.y - this.params.longUnit,
      pointStyle: 'pixel',
      color: 'white',
      hidden: false,
      tag: tag3,
      name: tag3,
      hiddenName: true
    })
    this.params.mtgApplecteur.addLinkedText({
      text: eel.noms[0],
      a: tag3,
      y: 1,
      color: 'blue',
      hidden: false,
      tag: tag4,
      hAlign: 'center',
      vAlign: 'middle',
      transparent: true
    })
    toShow.push(tag3, tag4)
    donnesMtg.push({ p1: ob.ext1, p2: ob.ext2, type: 'pointMilieu', tag: tatag, noms: [eel.noms[0]], depend: [this.scratchMtgRetrouveBase(num, { nom: ob.noms[0] }, 'num')], viz: getTrue, toShow, sur: [] })
    this.addPointMesur(tatag, num)
    this.scratchAjouteContient(num, ob.noms[0], eel.noms[0])
  }

  scratchFaisPointPointInter (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    const tatag = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.setApiDoc(svgId)
    const ob1 = this.scratchMtgRetrouveBase(num, { nom: eel.cont1 }, 'objet')
    const ob2 = this.scratchMtgRetrouveBase(num, { nom: eel.cont2 }, 'objet')
    const InterPos = []
    const tatag2 = this.scratchMtgNewTag(num)
    if (ob1.type === 'arc' || ob1.type === 'cercle') {
      if (ob2.type === 'arc' || ob2.type === 'cercle') {
        this.params.mtgApplecteur.addIntCircleCircle({
          c: ob1.tag,
          c2: ob2.tag,
          hidden: false,
          name: tatag,
          tag: tatag,
          name2: tatag2,
          tag2: tatag2,
          hiddenName: true,
          color: 'black',
          pointStyle: 'bigmult',
          smartIntersect: false
        })
      } else {
        this.params.mtgApplecteur.addIntLineCircle({
          c: ob1.tag,
          d: ob2.tag,
          hidden: true,
          name: tatag,
          tag: tatag,
          name2: tatag2,
          tag2: tatag2,
          hiddenName: true,
          pointStyle: 'bigmult',
          smartIntersect: false
        })
      }
      InterPos.push(tatag, tatag2)
    } else {
      if (ob2.type === 'arc' || ob2.type === 'cercle') {
        this.params.mtgApplecteur.addIntLineCircle({
          d: ob1.tag,
          c: ob2.tag,
          hidden: false,
          name: tatag,
          tag: tatag,
          name2: tatag2,
          tag2: tatag2,
          hiddenName: true,
          color: 'black',
          pointStyle: 'bigmult',
          smartIntersect: false
        })
        InterPos.push(tatag, tatag2)
      } else {
        this.params.mtgApplecteur.addIntLineLine({
          d: ob1.tag,
          d2: ob2.tag,
          hidden: false,
          name: tatag,
          tag: tatag,
          hiddenName: true,
          color: 'black',
          pointStyle: 'bigmult',
          smartIntersect: false
        })
        InterPos.push(tatag)
      }
    }
    for (let i = InterPos.length - 1; i > -1; i--) {
      const isViz = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: InterPos[i] })
      if (isViz.x === undefined) {
        InterPos.splice(i, 1)
        if (InterPos.length === 0) {
          this.scratchfautStop('Pas de point d’intersection !')
          return
        }
        continue
      }
      const tropProches = this.scatchMtgPointsProches(num, isViz)
      if (tropProches.length > 0) {
        try {
          this.params.mtgApplecteur.setHidden({ elt: InterPos[i] })
          this.params.mtgApplecteur.deleteElt({ elt: InterPos[i] })
        } catch (e) {
          // eslint-disable-next-line no-console
          console.log('peux pas effacer')
        }
        InterPos.splice(i, 1)
        if (InterPos.length === 0) {
          this.scratchfautStop('Le point existe déjà !')
          return
        }
        continue
      }
      if (this.scratchMtgEnDehors(isViz)) {
        InterPos.splice(i, 1)
        this.params.mtgApplecteur.setHidden({ elt: InterPos[i] })
        if (InterPos.length === 0) {
          this.scratchfautStop('Le point d’intersection n’est pas visible !')
          return
        }
      }
    }
    if (InterPos[1]) {
      try {
        this.params.mtgApplecteur.setHidden({ elt: InterPos[1] })
        this.params.mtgApplecteur.deleteElt({ elt: InterPos[1] })
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log('peux pas effacer')
      }
    }
    const inter = InterPos[0]
    const toShow = [inter]
    const tag3 = this.scratchMtgNewTag(num)
    const tag4 = this.scratchMtgNewTag(num)
    const tag2 = this.scratchMtgNewTag(num)
    const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: inter })
    this.params.mtgApplecteur.addCircleOr({
      o: inter,
      r: 1,
      hidden: true,
      tag: tag2
    })
    let oknom = true
    try {
      this.params.mtgApplecteur.addLinkedPointCircle({
        absCoord: true,
        c: tag2,
        color: 'white',
        name: tag3,
        tag: tag3,
        x: loch.x,
        y: loch.y - this.params.longUnit,
        pointStyle: 'pixel',
        hiddenName: true
      })
    } catch (e) {
      try {
        this.params.mtgApplecteur.addLinkedPointCircle({
          absCoord: true,
          c: tag2,
          color: 'white',
          name: tag3,
          tag: tag3,
          x: loch.x,
          y: loch.y + this.params.longUnit,
          pointStyle: 'pixel',
          hiddenName: true
        })
      } catch (e) {
        try {
          this.params.mtgApplecteur.addLinkedPointCircle({
            absCoord: true,
            c: tag2,
            color: 'white',
            name: tag3,
            tag: tag3,
            x: loch.x + this.params.longUnit,
            y: loch.y,
            pointStyle: 'pixel',
            hiddenName: true
          })
        } catch (e) {
          try {
            this.params.mtgApplecteur.addLinkedPointCircle({
              absCoord: true,
              c: tag2,
              color: 'white',
              name: tag3,
              tag: tag3,
              x: loch.x - this.params.longUnit,
              y: loch.y,
              pointStyle: 'pixel',
              hiddenName: true
            })
          } catch (e) {
            console.error('nom du point inter impossible à placer')
            oknom = false
          }
        }
      }
    }
    if (oknom) {
      this.params.mtgApplecteur.addLinkedText({
        text: eel.noms[0],
        a: tag3,
        color: 'blue',
        hidden: false,
        tag: tag4,
        hAlign: 'center',
        vAlign: 'middle',
        transparent: true
      })
      toShow.push(tag3, tag4)
    }
    donnesMtg.push({ type: 'pointIntersection', tag: inter, noms: [eel.noms[0]], depend: [this.scratchMtgRetrouveBase(num, { nom: eel.cont1 }, 'num'), this.scratchMtgRetrouveBase(num, { nom: eel.cont2 }, 'num')], viz: getTrue, toShow, sur: [this.scratchMtgRetrouveBase(num, { nom: eel.cont1 }, 'num'), this.scratchMtgRetrouveBase(num, { nom: eel.cont2 }, 'num')] })
    this.addPointMesur(inter, num)
    this.scratchAjouteContient(num, eel.cont1, eel.noms[0])
    this.scratchAjouteContient(num, eel.cont2, eel.noms[0])
  }

  scratchFaisPointPointSur (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    const tag1 = this.scratchMtgNewTag(num)
    const tatag4 = this.scratchMtgNewTag(num)
    const tatag5 = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.setApiDoc(svgId)
    const ob1 = this.scratchMtgRetrouveBase(num, { nom: eel.cont }, 'objet')
    const assuprapres = []
    if (ob1.type === 'cercle' || ob1.type === 'arc') {
      const tagf1 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addRay({
        o: this.scratchMtgRetrouveBase(num, { nom: ob1.centre }, 'tag'),
        a: 'I',
        tag: tagf1,
        hidden: true
      })
      this.params.mtgApplecteur.addIntLineCircle({
        d: tagf1,
        c: ob1.tag,
        hidden: true,
        name: tatag4,
        tag: tatag4,
        hiddenName: true,
        color: 'red',
        pointStyle: 'biground'
      })
      assuprapres.push(tagf1)
    }
    if (ob1.type === 'segment' || ob1.type === 'droite' || ob1.type === 'demidroite') {
      const tagf1 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinePerp({
        d: ob1.tag,
        a: 'I',
        tag: tagf1,
        hidden: true
      })
      this.params.mtgApplecteur.addIntLineLine({
        d: tagf1,
        d2: ob1.tag,
        hidden: true,
        name: tatag4,
        tag: tatag4,
        hiddenName: true,
        color: 'red',
        pointStyle: 'biground'
      })
      assuprapres.push(tagf1)
    }

    let cccpt = 0
    let jkjk
    this.params.mtgApplecteur.addLengthMeasure({ a: tatag4, b: 'I' })
    this.params.mtgApplecteur.addCalc({ nameCalc: tatag5, formula: tatag4 + 'I' })
    do {
      do {
        cccpt++
        this.params.mtgApplecteur.setPointPosition(svgId, '#PS', j3pGetRandomInt(15, 360), j3pGetRandomInt(15, 360), true)
      } while (!this.scratchTestExiste(num, tatag5) && cccpt < 1000)
      jkjk = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag4 })
    } while (!this.scratchMtgPlaceOk(num, jkjk) && cccpt < 1000)
    const toShow = [tag1]

    if (ob1.type === 'cercle' || ob1.type === 'arc') {
      this.params.mtgApplecteur.addLinkedPointCircle({
        absCoord: true,
        c: ob1.tag,
        x: jkjk.x,
        y: jkjk.y,
        hidden: false,
        name: tag1,
        tag: tag1,
        hiddenName: true,
        color: 'black',
        pointStyle: 'bigmult'
      })
    }
    if (ob1.type === 'segment' || ob1.type === 'droite' || ob1.type === 'demidroite') {
      this.params.mtgApplecteur.addLinkedPointLine({
        absCoord: true,
        d: ob1.tag,
        x: jkjk.x,
        y: jkjk.y,
        hidden: false,
        name: tag1,
        tag: tag1,
        hiddenName: true,
        color: 'black',
        pointStyle: 'bigmult'
      })
    }
    // TODO faut une removeElt
    assuprapres.forEach(elt => {
      this.params.mtgApplecteur.deleteElt({ elt })
    })
    const tag2 = this.scratchMtgNewTag(num)
    const tag3 = this.scratchMtgNewTag(num)
    const tag4 = this.scratchMtgNewTag(num)
    const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tag1 })
    this.params.mtgApplecteur.addCircleOr({
      o: tag1,
      r: 1,
      hidden: true,
      tag: tag2
    })
    this.params.mtgApplecteur.addLinkedPointCircle({
      absCoord: true,
      c: tag2,
      x: loch.x + 1,
      y: loch.y - this.params.longUnit,
      pointStyle: 'pixel',
      color: 'white',
      hidden: false,
      tag: tag3,
      name: tag3,
      hiddenName: true
    })
    this.params.mtgApplecteur.addLinkedText({
      text: eel.noms[0],
      a: tag3,
      color: 'blue',
      hidden: false,
      tag: tag4,
      hAlign: 'center',
      vAlign: 'middle',
      transparent: true
    })
    toShow.push(tag3, tag4)
    this.scratchAjouteContient(num, ob1.noms[0], eel.noms[0])
    donnesMtg.push({
      type: 'pointSur',
      tag: tag1,
      noms: [eel.noms[0]],
      depend: [this.scratchMtgRetrouveBase(num, { nom: eel.cont }, 'num')],
      viz: getTrue,
      toShow,
      sur: [this.scratchMtgRetrouveBase(num, { nom: eel.cont }, 'num')]
    })
    this.addPointMesur(tag1, num)
  }

  scratchFaisSegmentLongA (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    const e1 = this.scratchMtgRetrouveBase(num, { nom: eel.noms[0][1] }, 'objet')
    const e2 = this.scratchMtgRetrouveBase(num, { nom: eel.noms[0][2] }, 'objet')
    let adonext1 = eel.noms[0][1]
    let ptariv = eel.noms[0][2]
    if (!e1) {
      if (!e2) {
        this.scratchFaisPoint(num, { noms: [eel.noms[0][1]] }, false)
      } else {
        ptariv = eel.noms[0][1]
        adonext1 = eel.noms[0][2]
      }
    }

    const tatag3 = this.scratchMtgNewTag(num)
    const tatag2 = this.scratchMtgNewTag(num)
    const tag5 = this.scratchMtgNewTag(num)
    const tag7 = this.scratchMtgNewTag(num)
    const tatagLa = this.scratchMtgNewTag(num)
    const tatagPoint = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouveBase(num, { nom: adonext1 }, 'tag'),
      r: this.scratchTagPourLettre(eel.long, num),
      thickness: 6,
      color: 'green',
      tag: tatag2,
      hidden: true
    })
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouveBase(num, { nom: adonext1 }, 'tag'),
      r: '(' + this.scratchTagPourLettre(eel.long, num) + ')/2',
      thickness: 6,
      color: 'green',
      tag: tatag3,
      hidden: true
    })
    const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouveBase(num, { nom: adonext1 }, 'tag') })
    this.params.mtgApplecteur.addLinkedPointCircle({
      absCoord: true,
      c: tatag2,
      x: loch.x + 100,
      y: loch.y,
      pointStyle: 'bigmult',
      tag: tatagPoint,
      name: tatagPoint,
      hiddenName: true,
      color: 'black'
    })
    this.params.mtgApplecteur.addSegment({
      b: tatagPoint,
      a: this.scratchMtgRetrouveBase(num, { nom: adonext1 }, 'tag'),
      thickness: 2,
      tag: tatagLa,
      lineStyle: (eel.pointille) ? 'dashdash' : 'line',
      color: 'black'
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatag3,
      d: tatagLa,
      pointStyle: 'bigmult',
      tag: tag5,
      name: tag5,
      hidden: true
    })
    const ztag3 = this.scratchMtgNewTag(num)
    const ztag4 = this.scratchMtgNewTag(num)
    const ztag2 = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCircleOr({
      o: tag5,
      r: 0.6,
      thickness: 6,
      color: 'green',
      tag: ztag3,
      hidden: true
    })
    this.params.mtgApplecteur.addLinePerp({
      a: tag5,
      d: tatagLa,
      thickness: 2,
      tag: ztag4,
      hiddenName: true,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: ztag3,
      d: ztag4,
      pointStyle: 'bigmult',
      tag: ztag2,
      name: ztag2,
      hidden: true
    })
    this.params.mtgApplecteur.addLinkedText({
      text: eel.long,
      a: ztag2,
      y: 1,
      color: 'purple',
      hidden: false,
      tag: tag7,
      hAlign: 'center',
      vAlign: 'middle',
      transparent: true
    })

    const toShow = [tatagPoint]
    const tag3 = this.scratchMtgNewTag(num)
    const tag4 = this.scratchMtgNewTag(num)
    const tag2 = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCircleOr({
      o: tatagPoint,
      r: 1,
      hidden: true,
      tag: tag2
    })
    this.params.mtgApplecteur.addLinkedPointCircle({
      absCoord: true,
      c: tag2,
      x: 1,
      y: 1,
      pointStyle: 'bigmult',
      color: 'white',
      hidden: false,
      tag: tag3,
      name: tag3,
      hiddenName: true
    })
    this.params.mtgApplecteur.addLinkedText({
      text: ptariv,
      a: tag3,
      color: 'blue',
      hidden: false,
      tag: tag4,
      hAlign: 'center',
      vAlign: 'middle',
      transparent: true
    })
    toShow.push(tag3, tag4)
    const ames = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCalc({ nameCalc: ames, formula: this.scratchTagPourLettre(eel.long, num) })
    // TODO dans ajoute contient, on doit test les autres qui le contiennent aussi
    donnesMtg.push({ type: 'point', tag: tatagPoint, noms: [ptariv], depend: [this.scratchMtgRetrouveBase(num, { nom: adonext1 }, 'num'), donnesMtg.length - 1], viz: getTrue, toShow })
    donnesMtg.push({ tag1: this.scratchMtgRetrouveBase(num, { nom: adonext1 }, 'tag'), tag2: tatagPoint, type: 'segment', tag: tatagLa, ext1: adonext1, ext2: ptariv, long: eel.long, noms: ['[' + adonext1 + ptariv + ']', '[' + ptariv + adonext1 + ']'], depend: [this.scratchMtgRetrouveBase(num, { nom: adonext1 }, 'num')], viz: getTrue, toShow: [tatagLa, tag7], contient: [adonext1, ptariv], longueur: ames })
    this.addPointMesur(tatagPoint, num)
  }

  scratchFaisSegment (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    const tatag = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.setApiDoc(svgId)
    const ext1 = eel.noms[0][1]
    const ext2 = eel.noms[0][2]
    if (!this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag')) {
      this.scratchFaisPoint(num, { noms: [ext1] }, false)
    }
    if (!this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag')) {
      this.scratchFaisPoint(num, { noms: [ext2] }, false)
    }
    this.params.mtgApplecteur.addSegment({
      b: this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag'),
      a: this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag'),
      thickness: 2,
      lineStyle: (eel.pointille) ? 'dashdash' : 'line',
      tag: tatag
    })
    const ames = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCalc({ nameCalc: ames, formula: this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag') + this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag') })
    donnesMtg.push({ contient: [ext1, ext2], tag1: this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag'), tag2: this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag'), type: 'segment', tag: tatag, ext1, ext2, noms: ['[' + ext1 + ext2 + ']', '[' + ext2 + ext1 + ']'], depend: [this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'num'), this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'num')], viz: this._getIsVisible(eel), toShow: [tatag], longueur: ames })
  }

  scratchFaisDemiDroitept (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    const tatag = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.setApiDoc(svgId)
    const ext1 = eel.noms[0][1]
    const ext2 = eel.noms[0][2]
    if (!this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag')) {
      this.scratchFaisPoint(num, { noms: [ext1] }, false)
    }
    if (!this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag')) {
      this.scratchFaisPoint(num, { noms: [ext2] }, false)
    }
    this.params.mtgApplecteur.addRay({
      o: this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag'),
      a: this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag'),
      thickness: 2,
      lineStyle: (eel.pointille) ? 'dashdash' : 'line',
      tag: tatag
    })
    donnesMtg.push({ contient: [ext1, ext2], tag1: this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag'), tag2: this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag'), type: 'demidroite', tag: tatag, origine: ext1, point: ext2, noms: ['[' + ext1 + ext2 + ')', '(' + ext2 + ext1 + ']'], depend: [this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'num'), this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'num')], viz: this._getIsVisible(eel), toShow: [tatag] })
  }

  scratchFaisDroitept (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    const tatag = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.setApiDoc(svgId)
    const ext1 = eel.noms[0][1]
    const ext2 = eel.noms[0][2]
    if (!this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag')) {
      this.scratchFaisPoint(num, { noms: [ext1] }, false)
    }
    if (!this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag')) {
      this.scratchFaisPoint(num, { noms: [ext2] }, false)
    }
    this.params.mtgApplecteur.addLineAB({
      a: this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag'),
      b: this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag'),
      thickness: 2,
      lineStyle: (eel.pointille) ? 'dashdash' : 'line',
      tag: tatag
    })
    const toShow = [tatag]
    if (eel.affnomspe) {
      const lochA = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag') })
      const lochB = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag') })
      const vecAB = { x: lochA.x - lochB.x, y: lochA.y - lochB.y }
      const tatag2 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addCircleOr({
        o: this.scratchMtgRetrouve({ nom: ext1 }, 'tag'),
        r: 1,
        hidden: true,
        color: 'red',
        tag: tatag2
      })
      const tatag3 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinePerp({
        a: this.scratchMtgRetrouve({ nom: ext1 }, 'tag'),
        d: tatag,
        thickness: 2,
        tag: tatag3,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const tatag4 = this.scratchMtgNewTag(num)
      const tatag5 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addIntLineCircle({
        c: tatag2,
        d: tatag3,
        hidden: true,
        name: tatag4,
        name2: tatag5
      })
      const tatag7 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinePerp({
        a: tatag5,
        d: tatag3,
        thickness: 2,
        tag: tatag7,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const lochC = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatag4 })
      const t1 = (vecAB.x !== 0) ? (40 - lochC.x) / vecAB.x : 40
      const t2 = (vecAB.x !== 0) ? (360 - lochC.x) / vecAB.x : 360
      const t3 = (vecAB.y !== 0) ? (40 - lochC.y) / vecAB.y : 40
      const t4 = (vecAB.y !== 0) ? (360 - lochC.y) / vecAB.y : 360
      const y1 = (vecAB.x !== 0) ? (lochC.y + vecAB.y * t1) : 40
      const y2 = (vecAB.x !== 0) ? (lochC.y + vecAB.y * t2) : 360
      const x3 = (vecAB.y !== 0) ? (lochC.x + vecAB.x * t3) : 40
      const x4 = (vecAB.y !== 0) ? (lochC.x + vecAB.x * t4) : 360
      let lex = lochC.x
      let ley = lochC.y
      if (y1 > 39 && y1 < 361) {
        lex = 40
        ley = y1
      } else if (y2 > 39 && y2 < 361) {
        lex = 360
        ley = y2
      } else if (x3 > 39 && x3 < 361) {
        lex = x3
        ley = 40
      } else if (x4 > 39 && x4 < 361) {
        lex = x4
        ley = 360
      }
      const tatag8 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinkedPointLine({
        absCoord: true,
        d: tatag7,
        color: 'white',
        name: tatag8,
        tag: tatag8,
        x: lex,
        y: ley,
        pointStyle: 'pixel',
        hiddenName: true
      })
      const tatag6 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinkedText({
        text: eel.noms[1],
        a: tatag8,
        color: 'blue',
        hidden: false,
        tag: tatag6,
        hAlign: 'center',
        vAlign: 'middle',
        transparent: true
      })
      toShow.push(tatag6, tatag8)
    }
    donnesMtg.push({ contient: [ext1, ext2], tag1: this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag'), tag2: this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag'), type: 'droite', tag: tatag, point1: ext1, point2: ext2, noms: ['(' + ext1 + ext2 + ')', '(' + ext2 + ext1 + ')'], depend: [this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'num'), this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'num')], viz: this._getIsVisible(eel), toShow })
  }

  scratchFaisDroitePara (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    let nom = eel.noms[0]
    let para = eel.para
    let point = eel.point
    while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
    while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
    while (para.indexOf('\\\'') !== -1) { para = para.replace('\\\'', '\'') }
    while (para.indexOf('\\"') !== -1) { para = para.replace('\\"', '"') }
    while (point.indexOf('\\\'') !== -1) { point = point.replace('\\\'', '\'') }
    while (point.indexOf('\\"') !== -1) { point = point.replace('\\"', '"') }
    const yd = this.scratchMtgRetrouveBase(num, { nom: para }, 'tag')
    if (!yd) {
      let type = '?'
      if (para.indexOf('(') === 0 && para.indexOf(')') === para.length - 1) type = 'droite'
      if (para.indexOf('[') === 0 && para.indexOf(']') === para.length - 1) type = 'segment'
      if (para.indexOf('(') === 0 && para.indexOf(']') === para.length - 1) type = 'demidroite'
      if (para.indexOf('[') === 0 && para.indexOf(')') === para.length - 1) type = 'demidroite'
      if (type === '?') return { result: 'prob', quoi: 'Je ne trouve pas " ' + para + ' " !' }
      const base = para.replace('[', '').replace(']', '').replace('(', '').replace(')', '')
      const base2 = this.scratchDecomposeEnPoints(base)
      const ext2 = base2[1]
      const ext1 = base2[0]
      const para2 = this.scratchLigneContient([ext1, ext2], '', true)
      para = para2
    }
    const yy = this.scratchMtgRetrouveBase(num, { nom: point }, 'objet')
    if (!yy) {
      this.scratchFaisPoint(num, { noms: [point] }, false)
    }
    const tatag = this.scratchMtgNewTag(num)
    const toShow = [tatag]
    this.params.mtgApplecteur.addLinePar({
      a: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'),
      d: this.scratchMtgRetrouveBase(num, { nom: para }, 'tag'),
      thickness: 2,
      tag: tatag,
      color: 'black',
      lineStyle: (eel.pointille) ? 'dashdash' : 'line',
      hiddenName: true
    })
    const tatagf = this.scratchMtgNewTag(num)
    const tatagf2 = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'),
      r: 2,
      tag: tatagf2,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatagf2,
      d: tatag,
      hidden: true,
      name: tatagf,
      tag: tatagf
    })
    if (!eel.nomCache) {
      const lochA = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag') })
      const lochB = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatagf })
      const vecAB = { x: lochA.x - lochB.x, y: lochA.y - lochB.y }
      const tatag2 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addCircleOr({
        o: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'),
        r: 1,
        hidden: true,
        color: 'red',
        tag: tatag2
      })
      const tatag3 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinePerp({
        a: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'),
        d: tatag,
        thickness: 2,
        tag: tatag3,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const tatag4 = this.scratchMtgNewTag(num)
      const tatag5 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addIntLineCircle({
        c: tatag2,
        d: tatag3,
        hidden: true,
        name: tatag4,
        name2: tatag5
      })
      const tatag7 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinePerp({
        a: tatag5,
        d: tatag3,
        thickness: 2,
        tag: tatag7,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const tatag8 = this.scratchMtgNewTag(num)
      const t1 = (vecAB.x !== 0) ? (40 - lochA.x) / vecAB.x : 40
      const t2 = (vecAB.x !== 0) ? (360 - lochA.x) / vecAB.x : 360
      const t3 = (vecAB.y !== 0) ? (40 - lochA.y) / vecAB.y : 40
      const t4 = (vecAB.y !== 0) ? (360 - lochA.y) / vecAB.y : 360
      const y1 = (vecAB.x !== 0) ? (lochA.y + vecAB.y * t1) : 40
      const y2 = (vecAB.x !== 0) ? (lochA.y + vecAB.y * t2) : 360
      const x3 = (vecAB.y !== 0) ? (lochA.x + vecAB.x * t3) : 40
      const x4 = (vecAB.y !== 0) ? (lochA.x + vecAB.x * t4) : 360
      let lex = lochA.x
      let ley = lochA.y
      if (y1 > 39 && y1 < 361) {
        lex = 40
        ley = y1
      } else if (y2 > 39 && y2 < 361) {
        lex = 360
        ley = y2
      } else if (x3 > 39 && x3 < 361) {
        lex = x3
        ley = 40
      } else if (x4 > 39 && x4 < 361) {
        lex = x4
        ley = 360
      }
      this.params.mtgApplecteur.addLinkedPointLine({
        absCoord: true,
        d: tatag7,
        color: 'white',
        name: tatag8,
        tag: tatag8,
        x: lex,
        y: ley,
        pointStyle: 'pixel',
        hiddenName: true
      })
      const tatag6 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinkedText({
        text: nom,
        a: tatag8,
        color: '#7900c5',
        hidden: false,
        tag: tatag6,
        hAlign: 'center',
        vAlign: 'middle',
        transparent: true
      })
      toShow.push(tatag6, tatag8)
    }
    donnesMtg.push({ tag1: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'), tag2: tatagf, type: 'droite', tag: tatag, ext1: point, para, noms: [nom], depend: [this.scratchMtgRetrouveBase(num, { nom: point }, 'num'), this.scratchMtgRetrouveBase(num, { nom: para }, 'num')], viz: getTrue, toShow, contient: [point] })
  }

  scratchFaisDroitePerp (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    let nom = eel.noms[0]
    let para = eel.perp
    let point = eel.point
    while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
    while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
    while (para.indexOf('\\\'') !== -1) { para = para.replace('\\\'', '\'') }
    while (para.indexOf('\\"') !== -1) { para = para.replace('\\"', '"') }
    while (point.indexOf('\\\'') !== -1) { point = point.replace('\\\'', '\'') }
    while (point.indexOf('\\"') !== -1) { point = point.replace('\\"', '"') }
    const yd = this.scratchMtgRetrouveBase(num, { nom: para }, 'tag')
    if (!yd) {
      let type = '?'
      if (para.indexOf('(') === 0 && para.indexOf(')') === para.length - 1) type = 'droite'
      if (para.indexOf('[') === 0 && para.indexOf(']') === para.length - 1) type = 'segment'
      if (para.indexOf('(') === 0 && para.indexOf(']') === para.length - 1) type = 'demidroite'
      if (para.indexOf('[') === 0 && para.indexOf(')') === para.length - 1) type = 'demidroite'
      if (type === '?') return { result: 'prob', quoi: 'Je ne trouve pas " ' + para + ' " !' }
      const base = para.replace('[', '').replace(']', '').replace('(', '').replace(')', '')
      const base2 = this.scratchDecomposeEnPoints(base)
      const ext2 = base2[1]
      const ext1 = base2[0]
      const para2 = this.scratchLigneContient([ext1, ext2], '', true)
      para = para2
    }
    const yy = this.scratchMtgRetrouveBase(num, { nom: point }, 'objet')
    if (!yy) {
      this.scratchFaisPoint(num, { noms: [point] }, false)
    }
    const tatag = this.scratchMtgNewTag(num)
    const toShow = [tatag]
    this.params.mtgApplecteur.addLinePerp({
      a: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'),
      d: this.scratchMtgRetrouveBase(num, { nom: para }, 'tag'),
      thickness: 2,
      tag: tatag,
      color: 'black',
      lineStyle: (eel.pointille) ? 'dashdash' : 'line',
      hiddenName: true
    })
    const tatagf = this.scratchMtgNewTag(num)
    const tatagf2 = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'),
      r: 2,
      tag: tatagf2,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      c: tatagf2,
      d: tatag,
      hidden: true,
      name: tatagf,
      tag: tatagf
    })
    if (!eel.nomCache) {
      const lochA = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag') })
      const lochB = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: tatagf })
      const vecAB = {
        x: lochA.x - lochB.x,
        y: lochA.y - lochB.y
      }
      const tatag2 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addCircleOr({
        o: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'),
        r: 1,
        hidden: true,
        color: 'red',
        tag: tatag2
      })
      const tatag3 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinePerp({
        a: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'),
        d: tatag,
        thickness: 2,
        tag: tatag3,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const tatag4 = this.scratchMtgNewTag(num)
      const tatag5 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addIntLineCircle({
        c: tatag2,
        d: tatag3,
        hidden: true,
        name: tatag4,
        name2: tatag5
      })
      const tatag7 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinePerp({
        a: tatag5,
        d: tatag3,
        thickness: 2,
        tag: tatag7,
        color: 'black',
        hiddenName: true,
        hidden: true
      })
      const t1 = (vecAB.x !== 0) ? (40 - lochA.x) / vecAB.x : 40
      const t2 = (vecAB.x !== 0) ? (360 - lochA.x) / vecAB.x : 360
      const t3 = (vecAB.y !== 0) ? (40 - lochA.y) / vecAB.y : 40
      const t4 = (vecAB.y !== 0) ? (360 - lochA.y) / vecAB.y : 360
      const y1 = (vecAB.x !== 0) ? (lochA.y + vecAB.y * t1) : 40
      const y2 = (vecAB.x !== 0) ? (lochA.y + vecAB.y * t2) : 360
      const x3 = (vecAB.y !== 0) ? (lochA.x + vecAB.x * t3) : 40
      const x4 = (vecAB.y !== 0) ? (lochA.x + vecAB.x * t4) : 360
      let lex = lochA.x
      let ley = lochA.y
      if (y1 > 39 && y1 < 361) {
        lex = 40
        ley = y1
      } else if (y2 > 39 && y2 < 361) {
        lex = 360
        ley = y2
      } else if (x3 > 39 && x3 < 361) {
        lex = x3
        ley = 40
      } else if (x4 > 39 && x4 < 361) {
        lex = x4
        ley = 360
      }
      const tatag8 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinkedPointLine({
        absCoord: true,
        d: tatag7,
        color: 'white',
        name: tatag8,
        tag: tatag8,
        x: lex,
        y: ley,
        pointStyle: 'pixel',
        hiddenName: true
      })
      const tatag6 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLinkedText({
        text: nom,
        a: tatag8,
        color: '#7900c5',
        hidden: false,
        tag: tatag6,
        hAlign: 'center',
        vAlign: 'middle',
        transparent: true
      })
      toShow.push(tatag6, tatag8)
    }
    donnesMtg.push({ tag1: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'), tag2: tatagf, type: 'droite', tag: tatag, ext1: point, perp: para, noms: [nom], depend: [this.scratchMtgRetrouveBase(num, { nom: point }, 'num'), this.scratchMtgRetrouveBase(num, { nom: para }, 'num')], viz: getTrue, toShow, contient: [point] })
  }

  scratchFaisCercleCentrePoint (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    const centre = eel.centre
    const point = eel.point
    const nom = eel.noms[0]
    const tatag = this.scratchMtgNewTag(num)
    const toShow = [tatag]
    this.params.mtgApplecteur.addCircleOA({
      o: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag'),
      a: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'),
      thickness: 2,
      tag: tatag,
      lineStyle: (eel.pointille) ? 'dashdash' : 'line',
      color: 'black'
    })
    if (!eel.nomCache) {
      const tatag2 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLineAB({
        a: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag'),
        b: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'),
        hidden: true,
        tag: tatag2
      })
      const tatag3 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addCircleOr({
        o: this.scratchMtgRetrouveBase(num, { nom: point }, 'tag'),
        r: 0.6,
        hidden: true,
        color: 'red',
        tag: tatag3
      })
      const tatag4 = this.scratchMtgNewTag(num)
      const tatag5 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addIntLineCircle({
        d: tatag2,
        c: tatag3,
        hidden: true,
        name: tatag4,
        name2: tatag5
      })
      const tatag7 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addCircleOA({
        o: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag'),
        a: tatag4,
        thickness: 2,
        tag: tatag7,
        hidden: true,
        color: 'black'
      })
      const tatag8 = this.scratchMtgNewTag(num)
      const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag') })
      let oknom = true
      try {
        this.params.mtgApplecteur.addLinkedPointCircle({
          absCoord: true,
          c: tatag7,
          color: 'white',
          name: tatag8,
          tag: tatag8,
          x: loch.x,
          y: loch.y - this.params.longUnit,
          pointStyle: 'pixel',
          hiddenName: true
        })
      } catch (e) {
        try {
          this.params.mtgApplecteur.addLinkedPointCircle({
            absCoord: true,
            c: tatag7,
            color: 'white',
            name: tatag8,
            tag: tatag8,
            x: loch.x,
            y: loch.y + this.params.longUnit,
            pointStyle: 'pixel',
            hiddenName: true
          })
        } catch (e) {
          try {
            this.params.mtgApplecteur.addLinkedPointCircle({
              absCoord: true,
              c: tatag7,
              color: 'white',
              name: tatag8,
              tag: tatag8,
              x: loch.x + this.params.longUnit,
              y: loch.y,
              pointStyle: 'pixel',
              hiddenName: true
            })
          } catch (e) {
            try {
              this.params.mtgApplecteur.addLinkedPointCircle({
                absCoord: true,
                c: tatag7,
                color: 'white',
                name: tatag8,
                tag: tatag8,
                x: loch.x - this.params.longUnit,
                y: loch.y,
                pointStyle: 'pixel',
                hiddenName: true
              })
            } catch (e) {
              console.error('nom du cercle impossible à placer')
              oknom = false
            }
          }
        }
      }
      if (oknom) {
        const tatag6 = this.scratchMtgNewTag(num)
        this.params.mtgApplecteur.addLinkedText({
          text: nom,
          a: tatag8,
          color: '#E55B16',
          hidden: false,
          tag: tatag6,
          hAlign: 'center',
          vAlign: 'middle',
          transparent: true
        })
        toShow.push(tatag6, tatag8)
      }
    }
    donnesMtg.push({ type: 'cercle', tag: tatag, centre, point, noms: [nom], depend: [this.scratchMtgRetrouveBase(num, { nom: centre }, 'num'), this.scratchMtgRetrouveBase(num, { nom: point }, 'num')], viz: getTrue, toShow, contient: [point] })
  }

  scratchFaisCercleCentreRayon (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    const centre = eel.centre
    const rayon = eel.rayon
    const nom = eel.noms[0]
    this.params.mtgApplecteur.setApiDoc(svgId)
    const tatag = this.scratchMtgNewTag(num)
    const tatag2 = this.scratchMtgNewTag(num)
    const tatag3 = this.scratchMtgNewTag(num)
    const tatag4 = this.scratchMtgNewTag(num)
    const tatag5 = this.scratchMtgNewTag(num)
    const tatag6 = this.scratchMtgNewTag(num)
    const tatag7 = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag'),
      r: this.scratchTagPourLettre(rayon, num),
      thickness: 2,
      tag: tatag,
      lineStyle: (eel.pointille) ? 'dashdash' : 'line',
      color: 'black'
    })
    this.params.mtgApplecteur.addCircleOr({
      o: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag'),
      r: '(' + this.scratchTagPourLettre(rayon, num) + ')/2',
      thickness: 1,
      tag: tatag2,
      hidden: true
    })
    this.params.mtgApplecteur.addLinkedPointCircle({
      absCoord: true,
      c: tatag2,
      x: 192.5,
      y: 192.5,
      hidden: false,
      name: tatag3,
      tag: tatag3,
      hiddenName: true,
      color: 'white',
      pointStyle: 'pixel'
    })
    this.params.mtgApplecteur.addLineAB({
      a: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag'),
      b: tatag3,
      thickness: 1,
      tag: tatag4,
      color: 'green',
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      d: tatag4,
      c: tatag,
      hidden: true,
      name: tatag5,
      tag: tatag5,
      hiddenName: true,
      color: 'red',
      pointStyle: 'biground'
    })
    this.params.mtgApplecteur.addSegment({
      a: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag'),
      b: tatag5,
      thickness: 2,
      tag: tatag6,
      color: 'green',
      lineStyle: 'dashdash'
    })
    const ztag1 = this.scratchMtgNewTag(num)
    const ztag2 = this.scratchMtgNewTag(num)
    const ztag3 = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCircleOr({
      o: tatag3,
      r: '0.6',
      thickness: 1,
      tag: ztag1,
      hidden: true
    })
    this.params.mtgApplecteur.addLinePerp({
      d: tatag4,
      a: tatag3,
      tag: ztag3,
      hidden: true
    })
    this.params.mtgApplecteur.addIntLineCircle({
      d: ztag3,
      c: ztag1,
      hidden: true,
      name: ztag2,
      tag: ztag2,
      hiddenName: true,
      color: 'red',
      pointStyle: 'biground'
    })
    this.params.mtgApplecteur.addLinkedText({
      text: rayon,
      a: tatag3,
      color: 'purple',
      hidden: false,
      tag: tatag7,
      hAlign: 'center',
      vAlign: 'middle',
      transparent: true
    })
    const toShow = [tatag, tatag6, tatag3, tatag7]
    if (!eel.nomCache) {
      const tag2 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addLineAB({
        a: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag'),
        b: tatag5,
        hidden: true,
        tag: tag2
      })
      const tag3 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addCircleOr({
        o: tatag5,
        r: 0.5,
        hidden: true,
        color: 'red',
        tag: tag3
      })
      const tag4 = this.scratchMtgNewTag(num)
      const tag5 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addIntLineCircle({
        d: tag2,
        c: tag3,
        hidden: true,
        name: tag4,
        name2: tag5
      })
      const tag7 = this.scratchMtgNewTag(num)
      this.params.mtgApplecteur.addCircleOA({
        o: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag'),
        a: tag4,
        thickness: 2,
        tag: tag7,
        hidden: true,
        color: 'black'
      })
      const tatag8 = this.scratchMtgNewTag(num)
      const loch = this.params.mtgApplecteur.getPointPosition({ absCoord: true, a: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag') })
      let oknom = true
      try {
        this.params.mtgApplecteur.addLinkedPointCircle({
          absCoord: true,
          c: tag7,
          color: 'white',
          name: tatag8,
          tag: tatag8,
          x: loch.x,
          y: loch.y - this.params.longUnit,
          pointStyle: 'pixel',
          hiddenName: true
        })
      } catch (e) {
        try {
          this.params.mtgApplecteur.addLinkedPointCircle({
            absCoord: true,
            c: tag7,
            color: 'white',
            name: tatag8,
            tag: tatag8,
            x: loch.x,
            y: loch.y + this.params.longUnit,
            pointStyle: 'pixel',
            hiddenName: true
          })
        } catch (e) {
          try {
            this.params.mtgApplecteur.addLinkedPointCircle({
              absCoord: true,
              c: tag7,
              color: 'white',
              name: tatag8,
              tag: tatag8,
              x: loch.x + this.params.longUnit,
              y: loch.y,
              pointStyle: 'pixel',
              hiddenName: true
            })
          } catch (e) {
            try {
              this.params.mtgApplecteur.addLinkedPointCircle({
                absCoord: true,
                c: tag7,
                color: 'white',
                name: tatag8,
                tag: tatag8,
                x: loch.x - this.params.longUnit,
                y: loch.y,
                pointStyle: 'pixel',
                hiddenName: true
              })
            } catch (e) {
              console.error('nom du cercle impossible à placer')
              oknom = false
            }
          }
        }
      }
      if (oknom) {
        const tag6 = this.scratchMtgNewTag(num)
        this.params.mtgApplecteur.addLinkedText({
          text: nom,
          a: tatag8,
          color: '#E55B16',
          hidden: false,
          tag: tag6,
          hAlign: 'center',
          vAlign: 'middle',
          transparent: true
        })
        toShow.push(tag6, tatag8)
      }
    }
    const ames = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCalc({ nameCalc: ames, formula: this.scratchTagPourLettre(rayon, num) })
    donnesMtg.push({ type: 'cercle', tag: tatag, centre, rayon: ames, noms: [nom], depend: [this.scratchMtgRetrouveBase(num, { nom: centre }, 'num')], viz: getTrue, toShow, contient: [] })
  }

  scratchFaisMarque (num, eel) {
    let svgId, lesMArque, listMarke
    switch (num) {
      case 1:
        svgId = this.params.svgId
        lesMArque = this.params.lesMArque
        listMarke = this.params.listMarke
        break
      case 2:
        svgId = this.params.svgId2
        lesMArque = this.params.lesMArque2
        listMarke = this.params.listMarke2
        break
      case 3:
        svgId = this.params.svgId3
        lesMArque = this.params.lesMArque3
        listMarke = this.params.listMarke3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    const elt = this.scratchMtgRetrouveBase(num, { nom: eel.sur[0] }, 'objet')
    const lecal = this.scratchMtgNewTag(num)
    this.params.mtgApplecteur.addCalc({ nameCalc: lecal, formula: elt.longueur })
    lesMArque.push({ elt: elt.tag, marque: listMarke.pop(), calcul: lecal })
    const lam = lesMArque[lesMArque.length - 1]
    this.params.mtgApplecteur.addSegmentMark({
      elt: elt.tag,
      color: lam.marque.color,
      segmentMarkStyle: lam.marque.segmentMarkStyle,
      thickness: 3
    })
  }

  scratchFaisArc1PointPasse (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    const ext1 = eel.noms[0][0]
    const point = eel.point
    const ext2 = eel.noms[0][1]
    this.params.mtgApplecteur.setApiDoc(svgId)
    const tatag = this.scratchMtgNewTag(num)
    const tatagbis = this.scratchMtgNewTag(num)
    const tatag1 = this.scratchMtgNewTag(num)
    const tatag2 = this.scratchMtgNewTag(num)
    const tatag3 = this.scratchMtgNewTag(num)
    const tatag4 = this.scratchMtgNewTag(num)
    const tatag5 = this.scratchMtgNewTag(num)
    const tatag6 = this.scratchMtgNewTag(num)
    const tatag7 = this.scratchMtgNewTag(num)
    const tatag8 = this.scratchMtgNewTag(num)
    const tatag9 = this.scratchMtgNewTag(num)
    const tatag10 = this.scratchMtgNewTag(num)
    const tatag11 = this.scratchMtgNewTag(num)
    const tatag12 = this.scratchMtgNewTag(num)

    const A = this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag')
    const B = this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag')
    const M = this.scratchMtgRetrouveBase(num, { nom: point }, 'tag')

    this.params.mtgApplecteur.addCircleOA({
      o: A,
      a: M,
      tag: tatag1,
      hidden: true
    })
    this.params.mtgApplecteur.addCircleOA({
      a: A,
      o: M,
      tag: tatag2,
      hidden: true
    })
    this.params.mtgApplecteur.addIntCircleCircle({
      c: tatag2,
      c2: tatag1,
      tag: tatag3,
      name: tatag3,
      tag2: tatag4,
      name2: tatag4,
      hidden: true
    })
    this.params.mtgApplecteur.addLineAB({
      a: tatag3,
      b: tatag4,
      tag: tatag9,
      hidden: true
    })

    this.params.mtgApplecteur.addCircleOA({
      o: B,
      a: M,
      tag: tatag5,
      hidden: true
    })
    this.params.mtgApplecteur.addCircleOA({
      a: B,
      o: M,
      tag: tatag6,
      hidden: true
    })
    this.params.mtgApplecteur.addIntCircleCircle({
      c: tatag5,
      c2: tatag6,
      tag: tatag7,
      name: tatag7,
      tag2: tatag8,
      name2: tatag8,
      hidden: true
    })
    this.params.mtgApplecteur.addLineAB({
      a: tatag7,
      b: tatag8,
      tag: tatag10,
      hidden: true
    })

    this.params.mtgApplecteur.addIntLineLine({
      d: tatag9,
      d2: tatag10,
      tag: tatag11,
      name: tatag11,
      hidden: true
    })

    this.params.mtgApplecteur.addArcOAB({
      o: tatag11,
      a: A,
      b: B,
      thickness: 2,
      tag: tatag
    })
    this.params.mtgApplecteur.addArcMajorOAB({
      o: tatag11,
      a: A,
      b: B,
      thickness: 2,
      tag: tatagbis
    })
    this.params.mtgApplecteur.addLengthMeasure({ a: A, b: B })
    this.params.mtgApplecteur.addLengthMeasure({ a: A, b: M })
    this.params.mtgApplecteur.addLengthMeasure({ a: B, b: M })
    this.params.mtgApplecteur.addCalc({ nameCalc: tatag12, formula: A + B + '*' + A + B + '-' + A + M + '*' + A + M + '-' + B + M + '*' + B + M })
    const ff = () => {
      const lecal = this.params.mtgApplecteur.valueOf(svgId, tatag12)
      if (lecal > 0) { return 0 } else { return 1 }
    }
    donnesMtg.push({ funcArc: ff, type: 'arc', ext1, ext2, tag: tatag, point, noms: ['arc ' + ext1 + ext2, 'arc ' + ext2 + ext1], depend: [this.scratchMtgRetrouveBase(num, { nom: point }, 'num'), this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'num'), this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'num')], viz: getTrue, toShow: [tatag, tatagbis], contient: [point, ext1, ext2] })
  }

  scratchFaisArcCentre (num, eel) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    const ext1 = eel.noms[0][0]
    const centre = eel.centre
    const ext2 = eel.noms[0][1]
    const sens = eel.sens
    this.params.mtgApplecteur.setApiDoc(svgId)
    const tatag = this.scratchMtgNewTag(num)
    const lafuncAr = (sens) ? this.params.mtgApplecteur.addArcDirectOAB : this.params.mtgApplecteur.addArcIndirectOAB
    lafuncAr({
      o: this.scratchMtgRetrouveBase(num, { nom: centre }, 'tag'),
      a: this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'tag'),
      b: this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'tag'),
      color: 'black',
      hidden: false,
      tag: tatag,
      thickness: 2
    })
    donnesMtg.push({ type: 'arc', ext1, ext2, tag: tatag, centre, noms: ['arc ' + ext1 + ext2, 'arc ' + ext2 + ext1], depend: [this.scratchMtgRetrouveBase(num, { nom: centre }, 'num'), this.scratchMtgRetrouveBase(num, { nom: ext1 }, 'num'), this.scratchMtgRetrouveBase(num, { nom: ext2 }, 'num')], viz: getTrue, toShow: [tatag], contient: [ext1, ext2], sens })
  }

  mtgEffacer (num, num2) {
    let donnesMtg
    switch (num) {
      case 1:
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        donnesMtg = this.params.donnesMtg3
        break
    }
    donnesMtg[num2].efface = true
    this._mtgSuite(num, true, null)
  }

  chgStyle (num, num2) {
    let svgId, donnesMtg
    switch (num) {
      case 1:
        svgId = this.params.svgId
        donnesMtg = this.params.donnesMtg
        break
      case 2:
        svgId = this.params.svgId2
        donnesMtg = this.params.donnesMtg2
        break
      case 3:
        svgId = this.params.svgId3
        donnesMtg = this.params.donnesMtg3
        break
    }
    this.params.mtgApplecteur.setApiDoc(svgId)
    donnesMtg[num2].pointille = true
    for (let i = 0; i < donnesMtg[num2].toShow.length; i++) {
      try {
        this.params.mtgApplecteur.setLineStyle({ elt: donnesMtg[num2].toShow[i], lineStyle: 'dashdash' })
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e)
      }
    }
  }
}

export default ScratchMathgraph
