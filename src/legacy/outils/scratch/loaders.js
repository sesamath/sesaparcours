/** @module legacy/outils/scratch/loaders */

/**
 * Retourne l’objet Blockly généré dans vendors/scratch/Blockly par notre script
 * (pour régler les pbs de compilation avec vite, cf src/vendors/scratch/README.md)
 * @return Promise<Blockly>
 */
export function loadBlockly () {
  return import('src/vendors/scratch/Blockly').then(({ default: Blockly }) => Blockly)
}

/**
 * Retourne la classe Scratch
 * @return Promise<Scratch>
 */
export function loadScratch () {
  return import('./Scratch.js').then(({ default: Scratch }) => Scratch)
}

/**
 * Retourne la classe ScratchMathgraph
 * @return Promise<ScratchMathgraph>
 */
export function loadScratchMathgraph () {
  return import('./ScratchMathgraph.js').then(({ default: ScratchMathgraph }) => ScratchMathgraph)
}

/**
 * Charge Blockly et la classe Scratch et retourne un objet Scratch instancié
 * @returns {Promise<Scratch>}
 */
export function getNewScratch () {
  return Promise.all([loadScratch(), loadBlockly()])
    .then(([Scratch, Blockly]) => new Scratch(Blockly))
}

/**
 * Charge Blockly et la classe ScratchMathgraph et retourne un objet ScratchMathgraph instancié
 * @returns {Promise<ScratchMathgraph>}
 */
export function getNewScratchMathgraph () {
  return Promise.all([loadScratchMathgraph(), loadBlockly()])
    .then(([ScratchMathgraph, Blockly]) => new ScratchMathgraph(Blockly))
}
