function getLabelComplet (type) {
  switch (type) {
    case 'segment': return 'un segment'
    case 'droite': return 'une droite'
    case 'demidroite': return 'une demi-droite'
    case 'cercle': return 'un cercle'
    case 'arccercle': return 'un arc de cercle'
    case 'angle': return 'un angle'
  }
}

/**
 * Ajouts de méthodes à Blockly pour le cas ScratchMathgraph
 * @param Blockly
 * @param {ScratchMathgraph} scratch
 */
export default function ajoutsBlocklyMathgraph (Blockly, scratch) {
  if (!Blockly.JavaScript) throw Error('Il faut appeler ajoutBlockly avant ajoutsBlocklyMathgraph')

  Blockly.JavaScript.Renommer = function (block) {
    const cose = function () {
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      let ext2 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM2', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      while (ext2.indexOf('\\\'') !== -1) { ext2 = ext2.replace('\\\'', '\'') }
      while (ext2.indexOf('\\"') !== -1) { ext2 = ext2.replace('\\"', '"') }
      ext1 = ext1.trim()
      ext2 = ext2.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'Renommer', nom: ext1, nom2: ext2 })
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
        ext2 = scratch.scratchMtgReplaceLettres2(ext2)
      }
      if (scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')) return { result: 'prob', quoi: 'Il y a déjà un objet nommé ' + ext2 + ' !' }
      if (ext2 === '') return { result: 'prob', quoi: 'Il faut donner un nouveau nom !' }
      if (!scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')) return { result: 'prob', quoi: 'Je ne trouve pas l’objet ' + ext1 + ' !' }
      scratch.params.afaire = []
      const num = scratch.scratchMtgRetrouve({ nom: ext1 }, 'num')
      if (['segment', 'demidroite', 'arccercle', 'angle'].indexOf(scratch.params.donnesMtg[num].type) !== -1) return { result: 'prob', quoi: 'Je ne peux pas renommer ' + getLabelComplet(scratch.params.donnesMtg[num].type) + ' !' }
      if (!scratch.scratchIsNomCorecct(ext2, scratch.params.donnesMtg[num].type)) return { result: 'prob', quoi: ext2 + ' n’est pas un nom accepté pour ' + getLabelComplet(scratch.params.donnesMtg[num].type) + ' !' }
      scratch.params.donnesMtg[num].noms.splice(0, 0, ext2)
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: cose, nom: 'Renommer' }
  }

  Blockly.JavaScript.mtgEffacer = function (block) {
    const cose = function () {
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'mtgEffacer', noms: [ext1] })
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (!scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')) return { result: 'prob', quoi: 'Je ne trouve pas ' + ext1 + ' !' }
      scratch.params.afaire = []
      const num = scratch.scratchMtgRetrouve({ nom: ext1 }, 'num')
      if (num < scratch.params.MtgProg.figdep.length - scratch.params.lesMArque.length) return { result: 'prob', quoi: 'Je ne peux pas effacer la figure de départ !' }
      scratch.params.donnesMtg[num].efface = true
      // si on a une droite, faut chercher les segments et demidroite dessus et tous les effacer

      scratch.scratchMtgFinishOrdre()
      scratch._mtgSuite(1)
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: cose, nom: 'mtgEffacer' }
  }

  Blockly.JavaScript.mtgPointille = function (block) {
    const cose = function () {
      let cont = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (cont.indexOf('\\\'') !== -1) { cont = cont.replace('\\\'', '\'') }
      while (cont.indexOf('\\"') !== -1) { cont = cont.replace('\\"', '"') }
      cont = cont.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'mtgPointille', noms: [cont] })
        cont = scratch.scratchMtgReplaceLettres2(cont)
      }
      if (!scratch.scratchMtgRetrouve({ nom: cont }, 'tag')) {
        // faut faire un truc si on le retrouve pas
        // si c’est une droite ou u cercle c’est mort
        let type = '?'
        if (cont.indexOf('[') === 0 && cont.indexOf(']') === cont.length - 1) type = 'segment'
        if (cont.indexOf('(') === 0 && cont.indexOf(']') === cont.length - 1) type = 'demidroite'
        if (cont.indexOf('[') === 0 && cont.indexOf(')') === cont.length - 1) type = 'demidroite'
        if (cont.indexOf('arc ') === 0) type = 'arc'
        if (type === '?') {
          return { result: 'prob', quoi: 'Je ne trouve pas ' + cont + ' !' }
        }
        // faut que je cherche un truc qui contient
        const base = cont.replace('[', '').replace(']', '').replace('(', '').replace(')', '').replace('arc ', '')
        const base2 = scratch.scratchDecomposeEnPoints(base)
        if (base2.length !== 2) {
          return {
            result: 'prob',
            quoi: 'Je ne trouve pas " ' + cont + ' " !'
          }
        }
        const ext2 = base2[1]
        const ext1 = base2[0]
        if (scratch.scratchLigneContient([ext1, ext2], type)) {
          switch (type) {
            case 'demidroite':
              if (cont.indexOf('[') === 0) {
                scratch.scratchMtgDemiDroitePt(ext1, ext2)
              } else {
                scratch.scratchMtgDemiDroitePt(ext2, ext1)
              }
              break
            case 'segment':
              scratch.scratchMtgSegment(ext1, ext2)
              break
            case 'arc': {
              const pipush = scratch.scratchLigneContient([ext1, ext2], type, 'arc')
              scratch.scratchMtgarccentre(pipush.centre, ext1, ext2, false)
              scratch.scratchMtgarccentre(pipush.centre, ext1, ext2, true)
            }
              break
          }
        } else {
          return {
            result: 'prob',
            quoi: 'Je ne trouve pas " ' + cont + ' " !'
          }
        }
      }

      const ttyp = scratch.scratchMtgRetrouve({ nom: cont }, 'type')
      if (ttyp === 'point' ||
        ttyp === 'pointMilieu' ||
        ttyp === 'pointSur' ||
        ttyp === 'pointIntersection' ||
        ttyp === 'pointLibreAvecNom') return { result: 'prob', quoi: 'Je ne peux pas dessiner un point en pointillés  !' }
      scratch.params.afaire = []
      const num = scratch.scratchMtgRetrouve({ nom: cont }, 'num')
      if (num < scratch.params.MtgProg.figdep.length) return { result: 'prob', quoi: 'Je ne peux pas modifier la figure de départ !' }
      scratch.chgStyle(1, scratch.scratchMtgRetrouve({ nom: cont }, 'num'))

      scratch.scratchMtgFinishOrdre()
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: cose, nom: 'mtgPointille' }
  }

  Blockly.JavaScript.pointLibreAvecNom = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'pointLibreAvecNom', noms: [ext1] })
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouveGrave({ nom: ext1 }, 'tag')) return { result: 'prob', quoi: 'Il y a déjà eu un point ' + ext1 + ' !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer ce point !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(ext1) === -1) {
        if (!scratch.scratchIsNomCorecct(ext1, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour un point !' }
      }
      scratch.params.afaire = []
      scratch.scratchMtgPointLibre(ext1)
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'pointLibreAvecNom' }
  }

  Blockly.JavaScript.pointIntersection = function (block) {
    const cose = function () {
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      let toSuite
      if (scratch.params.isEditing) {
        toSuite = { type: 'pointIntersection', noms: [ext1] }
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouveGrave({ nom: ext1 }, 'tag')) return { result: 'prob', quoi: 'Il y a déjà eu un point ' + ext1 + ' !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer ce point !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(ext1) === -1) {
        if (!scratch.scratchIsNomCorecct(ext1, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour un point !' }
      }
      let cont1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'CONT1', Blockly.JavaScript.ORDER_ATOMIC))
      let cont2 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'CONT2', Blockly.JavaScript.ORDER_ATOMIC))
      while (cont1.indexOf('\\\'') !== -1) { cont1 = cont1.replace('\\\'', '\'') }
      while (cont1.indexOf('\\"') !== -1) { cont1 = cont1.replace('\\"', '"') }
      while (cont2.indexOf('\\\'') !== -1) { cont2 = cont2.replace('\\\'', '\'') }
      while (cont2.indexOf('\\"') !== -1) { cont2 = cont2.replace('\\"', '"') }
      cont1 = cont1.trim()
      cont2 = cont2.trim()
      if (scratch.params.isEditing) {
        toSuite.cont1 = cont1
        toSuite.cont2 = cont2
        scratch.params.MtgProg.suite.push(toSuite)
        cont1 = scratch.scratchMtgReplaceLettres2(cont1)
        cont2 = scratch.scratchMtgReplaceLettres2(cont2)
      }
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: cont1 }, 'tag')) {
        let type = '?'
        if (cont1.indexOf('(') === 0 && cont1.indexOf(')') === cont1.length - 1) type = 'droite'
        if (cont1.indexOf('[') === 0 && cont1.indexOf(']') === cont1.length - 1) type = 'segment'
        if (cont1.indexOf('(') === 0 && cont1.indexOf(']') === cont1.length - 1) type = 'demidroite'
        if (cont1.indexOf('[') === 0 && cont1.indexOf(')') === cont1.length - 1) type = 'demidroite'
        if (cont1.indexOf('arc ') === 0) type = 'arc'
        if (type === '?') return { result: 'prob', quoi: 'Je ne trouve pas " ' + cont1 + ' " !' }
        const base = cont1.replace('[', '').replace(']', '').replace('(', '').replace(')', '').replace('arc ', '')
        const base2 = scratch.scratchDecomposeEnPoints(base)
        if (base2.length !== 2) return { result: 'prob', quoi: 'Je ne trouve pas " ' + cont1 + ' " !' }
        const ext2 = base2[1]
        const ext1 = base2[0]
        if (scratch.scratchLigneContient([ext1, ext2], type)) {
          switch (type) {
            // TODO case de l’arc
            case 'droite': cont1 = scratch.scratchLigneContient([ext1, ext2], 'droite', 'droite')
              break
            case 'demidroite':
              if (cont1.indexOf('[') === 0) {
                scratch.params.afaire.push(() => { scratch.scratchMtgDemiDroitePt(ext1, ext2) })
              } else {
                scratch.params.afaire.push(() => { scratch.scratchMtgDemiDroitePt(ext2, ext1) })
              }
              break
            case 'segment': scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
              break
          }
        } else { return { result: 'prob', quoi: 'Je ne trouve pas " ' + cont1 + ' " !' } }
      }
      if (!scratch.scratchMtgRetrouve({ nom: cont2 }, 'tag')) {
        let type = '?'
        if (cont2.indexOf('(') === 0 && cont2.indexOf(')') === cont2.length - 1) type = 'droite'
        if (cont2.indexOf('[') === 0 && cont2.indexOf(']') === cont2.length - 1) type = 'segment'
        if (cont2.indexOf('(') === 0 && cont2.indexOf(']') === cont2.length - 1) type = 'demidroite'
        if (cont2.indexOf('[') === 0 && cont2.indexOf(')') === cont2.length - 1) type = 'demidroite'
        if (cont2.indexOf('arc ') === 0) type = 'arc'
        if (type === '?') return { result: 'prob', quoi: 'Je ne trouve pas " ' + cont2 + ' " !' }
        const base = cont2.replace('[', '').replace(']', '').replace('(', '').replace(')', '').replace('arc ', '')
        const base2 = scratch.scratchDecomposeEnPoints(base)
        if (base2.length !== 2) return { result: 'prob', quoi: 'Je ne trouve pas " ' + cont1 + ' " !' }
        const ext2 = base2[1]
        const ext1 = base2[0]
        if (scratch.scratchLigneContient([ext1, ext2], type)) {
          switch (type) {
            // TODO case de l’arc
            case 'droite': cont2 = scratch.scratchLigneContient([ext1, ext2], 'droite', 'droite')
              break
            case 'demidroite':
              if (cont2.indexOf('[') === 0) {
                scratch.params.afaire.push(() => { scratch.scratchMtgDemiDroitePt(ext1, ext2) })
              } else {
                scratch.params.afaire.push(() => { scratch.scratchMtgDemiDroitePt(ext2, ext1) })
              }
              break
            case 'segment': scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
              break
          }
        } else { return { result: 'prob', quoi: 'Je ne trouve pas " ' + cont2 + ' " !' } }
      }
      // TODO scratchYaPLusieurs( pour cont1 et cont2 )
      scratch.params.afaire.push(() => { scratch.scratchMtgPointInter(ext1, cont1, cont2) })
      scratch.scratchMtgFinishOrdre()
      return { result: 'ordrew' }
    }
    return { type: 'ordre', javascript: cose, nom: 'pointIntersection' }
  }

  Blockly.JavaScript.pointSur = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      let toSuite
      if (scratch.params.isEditing) {
        toSuite = { type: 'pointSur', noms: [ext1] }
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouveGrave({ nom: ext1 }, 'tag')) return { result: 'prob', quoi: 'Il y a déjà eu un point ' + ext1 + ' !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer ce point !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(ext1) === -1) {
        if (!scratch.scratchIsNomCorecct(ext1, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour un point !' }
      }
      let cont = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'CONT', Blockly.JavaScript.ORDER_ATOMIC))
      while (cont.indexOf('\\\'') !== -1) { cont = cont.replace('\\\'', '\'') }
      while (cont.indexOf('\\"') !== -1) { cont = cont.replace('\\"', '"') }
      cont = cont.trim()
      if (scratch.params.isEditing) {
        toSuite.cont = cont
        scratch.params.MtgProg.suite.push(toSuite)
        cont = scratch.scratchMtgReplaceLettres2(cont)
      }
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: cont }, 'tag')) {
        let type = '?'
        if (cont.indexOf('(') === 0 && cont.indexOf(')') === cont.length - 1) type = 'droite'
        if (cont.indexOf('[') === 0 && cont.indexOf(']') === cont.length - 1) type = 'segment'
        if (cont.indexOf('(') === 0 && cont.indexOf(']') === cont.length - 1) type = 'demidroite'
        if (cont.indexOf('[') === 0 && cont.indexOf(')') === cont.length - 1) type = 'demidroite'
        if (cont.indexOf('arc ') === 0) type = 'arc'
        if (type === '?') {
          return {
            result: 'prob',
            quoi: 'Je ne trouve pas " ' + cont + ' " !'
          }
        }
        const base = cont.replace('[', '').replace(']', '').replace('(', '').replace(')', '').replace('arc ', '')
        const base2 = scratch.scratchDecomposeEnPoints(base)
        if (base2.length !== 2) {
          return {
            result: 'prob',
            quoi: 'Je ne trouve pas " ' + cont + ' " !'
          }
        }
        const ext2 = base2[1]
        const ext1 = base2[0]
        if (scratch.scratchLigneContient([ext1, ext2], type)) {
          switch (type) {
            case 'droite':
              cont = scratch.scratchLigneContient([ext1, ext2], 'droite', 'droite')
              break
            case 'demidroite':
              if (cont.indexOf('[') === 0) {
                scratch.params.afaire.push(() => {
                  scratch.scratchMtgDemiDroitePt(ext1, ext2)
                })
              } else {
                scratch.params.afaire.push(() => {
                  scratch.scratchMtgDemiDroitePt(ext2, ext1)
                })
              }
              break
            case 'segment':
              scratch.params.afaire.push(() => {
                scratch.scratchMtgSegment(ext1, ext2)
              })
              break
            case 'arc': {
              const pipush = scratch.scratchLigneContient([ext1, ext2], type, 'arc')
              scratch.params.afaire.push(() => {
                scratch.scratchMtgarccentre(pipush.centre, ext1, ext2, false)
              })
              scratch.params.afaire.push(() => {
                scratch.scratchMtgarccentre(pipush.centre, ext1, ext2, true)
              })
            }
              break
          }
        } else {
          return {
            result: 'prob',
            quoi: 'Je ne trouve pas " ' + cont + ' " !'
          }
        }
      } else {
        if (scratch.scratchMtgRetrouve({ nom: cont }, 'type') === 'point') return { result: 'prob', quoi: 'Je ne peux pas placer un point sur un point !' }
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgPointSur(ext1, cont) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'pointSur' }
  }

  Blockly.JavaScript.pointMilieu = function (block) {
    const cose = function () {
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      let toSuite
      if (scratch.params.isEditing) {
        toSuite = { type: 'pointMilieu', noms: [ext1] }
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouveGrave({ nom: ext1 }, 'tag')) return { result: 'prob', quoi: 'Il y a déjà eu un point ' + ext1 + ' !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer ce point !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(ext1) === -1) {
        if (!scratch.scratchIsNomCorecct(ext1, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour un point !' }
      }
      let cont = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'CONT', Blockly.JavaScript.ORDER_ATOMIC))
      while (cont.indexOf('\\\'') !== -1) { cont = cont.replace('\\\'', '\'') }
      while (cont.indexOf('\\"') !== -1) { cont = cont.replace('\\"', '"') }
      cont = cont.trim()
      if (scratch.params.isEditing) {
        toSuite.cont = cont
        scratch.params.MtgProg.suite.push(toSuite)
        cont = scratch.scratchMtgReplaceLettres2(cont)
      }
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: '[' + cont + ']' }, 'tag')) {
        const base2 = scratch.scratchDecomposeEnPoints(cont)
        if (base2.length !== 2) return { result: 'prob', quoi: 'Il faut deux points pour nommer un segment !' }
        const ext2 = base2[1]
        const ext1 = base2[0]
        if (scratch.scratchLigneContient([ext1, ext2], '')) {
          scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
        } else { return { result: 'prob', quoi: 'Je ne trouve pas le segment "[' + cont + ']" !' } }
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgPointMilieu(ext1, '[' + cont + ']') })
      scratch.scratchMtgFinishOrdre()
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: cose, nom: 'pointMilieu' }
  }

  Blockly.JavaScript.pointAligne = function (block) {
    const cose = function () {
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      let toSuite
      if (scratch.params.isEditing) {
        toSuite = { type: 'pointAligne', noms: [ext1] }
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouveGrave({ nom: ext1 }, 'tag')) return { result: 'prob', quoi: 'Il y a déjà eu un point ' + ext1 + ' !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer ce point !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(ext1) === -1) {
        if (!scratch.scratchIsNomCorecct(ext1, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour un point !' }
      }
      let cont = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'P1', Blockly.JavaScript.ORDER_ATOMIC))
      while (cont.indexOf('\\\'') !== -1) { cont = cont.replace('\\\'', '\'') }
      while (cont.indexOf('\\"') !== -1) { cont = cont.replace('\\"', '"') }
      cont = cont.trim()
      let cont2 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'P2', Blockly.JavaScript.ORDER_ATOMIC))
      while (cont2.indexOf('\\\'') !== -1) { cont2 = cont2.replace('\\\'', '\'') }
      while (cont2.indexOf('\\"') !== -1) { cont2 = cont2.replace('\\"', '"') }
      cont2 = cont2.trim()
      if (scratch.params.isEditing) {
        toSuite.p1 = cont
        toSuite.p2 = cont2
        scratch.params.MtgProg.suite.push(toSuite)
        cont = scratch.scratchMtgReplaceLettres2(cont)
      }
      scratch.params.afaire = []
      if (cont === '') return { result: 'prob', quoi: 'Il faut nommer tous les points !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(cont) === -1) {
        if (!scratch.scratchIsNomCorecct(cont, 'point')) return { result: 'prob', quoi: 'Vérifie les noms des points !' }
      }
      if (cont2 === '') return { result: 'prob', quoi: 'Il faut nommer tous les points !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(cont2) === -1) {
        if (!scratch.scratchIsNomCorecct(cont2, 'point')) return { result: 'prob', quoi: 'Vérifie les noms des points !' }
      }
      if (!scratch.scratchMtgRetrouve({ nom: cont }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(cont) })
      }
      if (!scratch.scratchMtgRetrouve({ nom: cont2 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(cont2) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgPointAligne(ext1, cont, cont2) })
      scratch.scratchMtgFinishOrdre()
      return { result: 'ordre' }
    }
    return { type: 'ordrew', javascript: cose, nom: 'pointAligne' }
  }

  Blockly.JavaScript.pointLong = function (block) {
    const cose = function () {
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'EXT1', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      let toSuite
      if (scratch.params.isEditing) {
        toSuite = { type: 'pointLong', noms: [ext1] }
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouveGrave({ nom: ext1 }, 'tag')) return { result: 'prob', quoi: 'Il y a déjà eu un point ' + ext1 + ' !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer ce point !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(ext1) === -1) {
        if (!scratch.scratchIsNomCorecct(ext1, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour un point !' }
      }
      let cont = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'P1', Blockly.JavaScript.ORDER_ATOMIC))
      while (cont.indexOf('\\\'') !== -1) { cont = cont.replace('\\\'', '\'') }
      while (cont.indexOf('\\"') !== -1) { cont = cont.replace('\\"', '"') }
      cont = cont.trim()

      let long = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'LONG', Blockly.JavaScript.ORDER_ATOMIC))
      while (long.indexOf('\\\'') !== -1) { long = long.replace('\\\'', '\'') }
      while (long.indexOf('\\"') !== -1) { long = long.replace('\\"', '"') }
      if (scratch.params.isEditing) {
        toSuite.p1 = cont
        toSuite.long = long
        scratch.params.MtgProg.suite.push(toSuite)
        long = scratch.scratchMtgReplaceLettres2(long)
        cont = scratch.scratchMtgReplaceLettres2(cont)
      }
      if (long === '') return { result: 'prob', quoi: 'Il faut donner une longueur !' }
      const calc = scratch.calculeMG(long.replace(/,/g, '.'), 1)
      const calc2 = scratch.params.mtgApplecteur.valueOf(scratch.params.svgId, calc)
      if (calc2 === 0) return { result: 'prob', quoi: 'La longueur est nullle !' }
      if (calc2 === 'pb' || calc2 === -1) return { result: 'prob', quoi: 'Je ne peux pas calculer la longueur ' + long + ' !' }
      // faut vérifier, soit c’est un nb soit c’est une seule longueur
      if (!cUnNbOuUneLonguer(long)) {
        return { result: 'prob', quoi: 'Il faut indiquer une longueur ou un nombre !' }
      }

      scratch.params.afaire = []
      if (cont === '') return { result: 'prob', quoi: 'Il faut nommer tous les points !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(cont) === -1) {
        if (!scratch.scratchIsNomCorecct(cont, 'point')) return { result: 'prob', quoi: 'Vérifie les noms des points !' }
      }
      if (!scratch.scratchMtgRetrouve({ nom: cont }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(cont) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgPointLong(ext1, cont, calc, long) })
      scratch.scratchMtgFinishOrdre()
      return { result: 'ordrew' }
    }
    return { type: 'ordrew', javascript: cose, nom: 'pointLong' }
  }

  Blockly.JavaScript.segmentAB = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'EXT1', Blockly.JavaScript.ORDER_ATOMIC)).replace(/ /g, '')
      scratch.params.enreG.push('segmentAB avec ' + ext1)
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'segment', noms: ['[' + ext1 + ']'] })
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouve({ nom: '[' + ext1 + ']' }, 'tag')) return { result: 'prob', quoi: 'Ce segment est déjà tracé !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer ce segment !' }
      ext1 = scratch.scratchDecomposeEnPoints(ext1)
      if (ext1.length !== 2) {
        return { result: 'prob', quoi: 'Il faut deux points pour nommer un segment !' }
      }
      const ext2 = ext1[1]
      ext1 = ext1[0]
      scratch.params.enreG.push('segmentAB on a garde ' + ext1 + ' ' + ext2)
      scratch.params.afaire = []
      if (ext1 === ext2) return { result: 'prob', quoi: 'Il faut donner deux points distincts !' }
      if (!scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext1) })
      }
      if (!scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext2) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordrew', javascript: cose, nom: 'segmentAB' }
  }

  Blockly.JavaScript.segmentlongA = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'EXT1', Blockly.JavaScript.ORDER_ATOMIC)).replace(/ /g, '')
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      let toSuite
      if (scratch.params.isEditing) {
        toSuite = { type: 'segmentlongA', noms: ['[' + ext1 + ']'] }
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouve({ nom: '[' + ext1 + ']' }, 'tag')) return { result: 'prob', quoi: 'Ce segment est déjà tracé !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer ce segment !' }
      ext1 = scratch.scratchDecomposeEnPoints(ext1)
      if (ext1.length !== 2) return { result: 'prob', quoi: 'Il faut deux points pour nommer un segment !' }
      const ext2 = ext1[1]
      ext1 = ext1[0]
      if (ext1 === ext2) return { result: 'prob', quoi: 'Il faut donner deux points distincts !' }
      let long = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'LONG', Blockly.JavaScript.ORDER_ATOMIC))
      while (long.indexOf('\\\'') !== -1) { long = long.replace('\\\'', '\'') }
      while (long.indexOf('\\"') !== -1) { long = long.replace('\\"', '"') }
      if (scratch.params.isEditing) {
        toSuite.long = long
        scratch.params.MtgProg.suite.push(toSuite)
        long = scratch.scratchMtgReplaceLettres2(long)
      }
      if (long === '') return { result: 'prob', quoi: 'Il faut donner une longueur !' }
      const calc = scratch.calculeMG(long.replace(/,/g, '.'), 1)
      const calc2 = scratch.params.mtgApplecteur.valueOf(scratch.params.svgId, calc)
      if (calc2 === 0) return { result: 'prob', quoi: 'La longueur est nullle !' }
      if (calc2 === 'pb' || calc2 === -1) return { result: 'prob', quoi: 'Je ne peux pas calculer la longueur ' + long + ' !' }
      // faut vérifier, soit c’est un nb soit c’est une seule longueur
      if (!cUnNbOuUneLonguer(long)) {
        return { result: 'prob', quoi: 'Il faut indiquer une longueur ou un nombre !' }
      }

      const tagExt1 = scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')
      const tagExt2 = scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')
      if (tagExt1 && tagExt2) return { result: 'prob', quoi: 'La longueur ' + ext1 + ext2 + ' existe déjà !' }
      scratch.params.afaire = []
      let adonext1 = ext1
      let ptariv = ext2
      if (!tagExt1 && !tagExt2) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext1) })
      } else if (!tagExt1) {
        adonext1 = ext2; ptariv = ext1
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgSegmentLong(adonext1, ptariv, calc, long) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordrew', javascript: cose, nom: 'segmentlongA' }
  }
  function cUnNbOuUneLonguer (t) {
    const avire = t.trim()
    let cnb = true
    const lnb = '0123456789.,'
    for (let i = 0; i < avire.length; i++) {
      if (lnb.indexOf(avire[i]) === -1) cnb = false
    }
    if (cnb) return true
    const mm = scratch.scratchDecomposeEnPoints(avire)
    return mm.length === 2
  }

  Blockly.JavaScript.droitept = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'POINT1', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'droitept', noms: ['(' + ext1 + ')'] })
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouve({ nom: '(' + ext1 + ')' }, 'tag')) return { result: 'prob', quoi: 'Cette droite est déjà tracée !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer cette droite !' }
      ext1 = scratch.scratchDecomposeEnPoints(ext1)
      if (ext1.length !== 2) return { result: 'prob', quoi: 'Il faut deux points pour nommer une droite !' }
      const ext2 = ext1[1]
      ext1 = ext1[0]
      scratch.params.afaire = []
      if (ext1 === ext2) return { result: 'prob', quoi: 'Il faut donner deux points distincts !' }
      if (!scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext1) })
      }
      if (!scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext2) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgDroitePt(ext1, ext2) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'droitept' }
  }

  Blockly.JavaScript.droitepara = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let point = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'POINT', Blockly.JavaScript.ORDER_ATOMIC))
      let nom = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      let para = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'PARA', Blockly.JavaScript.ORDER_ATOMIC))
      while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
      while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
      while (para.indexOf('\\\'') !== -1) { para = para.replace('\\\'', '\'') }
      while (para.indexOf('\\"') !== -1) { para = para.replace('\\"', '"') }
      while (point.indexOf('\\\'') !== -1) { point = point.replace('\\\'', '\'') }
      while (point.indexOf('\\"') !== -1) { point = point.replace('\\"', '"') }
      point = point.trim()
      nom = nom.trim()
      para = para.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'droitepara', noms: [nom], point, para })
        point = scratch.scratchMtgReplaceLettres2(point)
        nom = scratch.scratchMtgReplaceLettres2(nom)
        para = scratch.scratchMtgReplaceLettres2(para)
      }
      scratch.params.afaire = []
      if (scratch.scratchMtgRetrouve({ nom }, 'tag')) return { result: 'prob', quoi: 'Cette droite est déjà tracée !' }
      if (!scratch.scratchMtgRetrouve({ nom: para }, 'tag')) {
        let type = '?'
        if (para.indexOf('(') === 0 && para.indexOf(')') === para.length - 1) type = 'droite'
        if (para.indexOf('[') === 0 && para.indexOf(']') === para.length - 1) type = 'segment'
        if (para.indexOf('(') === 0 && para.indexOf(']') === para.length - 1) type = 'demidroite'
        if (para.indexOf('[') === 0 && para.indexOf(')') === para.length - 1) type = 'demidroite'
        if (type === '?') return { result: 'prob', quoi: 'Je ne trouve pas " ' + para + ' " !' }
        const base = para.replace('[', '').replace(']', '').replace('(', '').replace(')', '')
        const base2 = scratch.scratchDecomposeEnPoints(base)
        if (base2.length !== 2) return { result: 'prob', quoi: 'Je ne trouve pas " ' + para + ' " !' }
        const ext2 = base2[1]
        const ext1 = base2[0]
        const para2 = scratch.scratchLigneContient([ext1, ext2], '', true)
        if (!para2) return { result: 'prob', quoi: 'Je ne trouve pas " ' + para + ' " !' }
        para = para2
      } else {
        const kk = scratch.scratchMtgRetrouve({ nom: para }, 'type')
        if (kk === 'point' || kk === 'cercle' || kk === 'arc') return { result: 'prob', quoi: 'Je ne peux pas tracer une parallèle à un ' + kk + ' !' }
      }
      if (!scratch.scratchMtgRetrouve({ nom: point }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(point) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgDroitePara(nom, para, point) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'droitepara' }
  }

  Blockly.JavaScript.droiteperp = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let point = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'POINT', Blockly.JavaScript.ORDER_ATOMIC))
      let nom = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      let para = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'PERP', Blockly.JavaScript.ORDER_ATOMIC))
      while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
      while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
      while (para.indexOf('\\\'') !== -1) { para = para.replace('\\\'', '\'') }
      while (para.indexOf('\\"') !== -1) { para = para.replace('\\"', '"') }
      while (point.indexOf('\\\'') !== -1) { point = point.replace('\\\'', '\'') }
      while (point.indexOf('\\"') !== -1) { point = point.replace('\\"', '"') }
      point = point.trim()
      nom = nom.trim()
      para = para.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'droiteperp', noms: [nom], point, perp: para })
        point = scratch.scratchMtgReplaceLettres2(point)
        nom = scratch.scratchMtgReplaceLettres2(nom)
        para = scratch.scratchMtgReplaceLettres2(para)
      }
      scratch.params.afaire = []
      if (scratch.scratchMtgRetrouve({ nom }, 'tag')) return { result: 'prob', quoi: 'Cette droite est déjà tracée !' }
      if (!scratch.scratchMtgRetrouve({ nom: para }, 'tag')) {
        let type = '?'
        if (para.indexOf('(') === 0 && para.indexOf(')') === para.length - 1) type = 'droite'
        if (para.indexOf('[') === 0 && para.indexOf(']') === para.length - 1) type = 'segment'
        if (para.indexOf('(') === 0 && para.indexOf(']') === para.length - 1) type = 'demidroite'
        if (para.indexOf('[') === 0 && para.indexOf(')') === para.length - 1) type = 'demidroite'
        if (type === '?') return { result: 'prob', quoi: 'Je ne trouve pas " ' + para + ' " !' }
        const base = para.replace('[', '').replace(']', '').replace('(', '').replace(')', '')

        const base2 = scratch.scratchDecomposeEnPoints(base)
        if (base2.length !== 2) return { result: 'prob', quoi: 'Je ne trouve pas " ' + para + ' " !' }
        const ext2 = base2[1]
        const ext1 = base2[0]
        const para2 = scratch.scratchLigneContient([ext1, ext2], '', true)
        if (!para2) return { result: 'prob', quoi: 'Je ne trouve pas " ' + para + ' " !' }
        para = para2
      } else {
        const kk = scratch.scratchMtgRetrouve({ nom: para }, 'type')
        if (kk === 'point' || kk === 'cercle' || kk === 'arc') return { result: 'prob', quoi: 'Je ne peux pas tracer une perpendiculaire à un ' + kk + ' !' }
      }
      if (!scratch.scratchMtgRetrouve({ nom: point }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(point) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgDroitePerp(nom, para, point) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'droiteperp' }
  }

  Blockly.JavaScript.mediatrice = function (block) {
    const cose = function () {
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      let toSuite
      if (scratch.params.isEditing) {
        toSuite = { type: 'mediatrice', noms: [ext1] }
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')) return { result: 'prob', quoi: 'Il y a déjà un élément ' + ext1 + ' !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer cette droite !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(ext1) === -1) {
        if (!scratch.scratchIsNomCorecct(ext1, 'droite')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour une droite !' }
      }
      let cont = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'CONT', Blockly.JavaScript.ORDER_ATOMIC))
      while (cont.indexOf('\\\'') !== -1) { cont = cont.replace('\\\'', '\'') }
      while (cont.indexOf('\\"') !== -1) { cont = cont.replace('\\"', '"') }
      cont = cont.trim()
      if (scratch.params.isEditing) {
        toSuite.cont = cont
        scratch.params.MtgProg.suite.push(toSuite)
        cont = scratch.scratchMtgReplaceLettres2(cont)
      }
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: '[' + cont + ']' }, 'tag')) {
        const base2 = scratch.scratchDecomposeEnPoints(cont)
        if (base2.length !== 2) return { result: 'prob', quoi: 'Il faut deux points pour nommer un segment !' }
        const ext2 = base2[1]
        const ext1 = base2[0]
        if (scratch.scratchLigneContient([ext1, ext2], '')) {
          scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
        } else { return { result: 'prob', quoi: 'Je ne trouve pas le segment "[' + cont + ']" !' } }
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgMediatrice(ext1, '[' + cont + ']') })
      scratch.scratchMtgFinishOrdre()
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: cose, nom: 'mediatrice' }
  }

  Blockly.JavaScript.demidroitept = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'ORIGINE', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'demidroitept', noms: ['[' + ext1 + ')'] })
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouve({ nom: '[' + ext1 + ')' }, 'tag')) return { result: 'prob', quoi: 'Cette demi-droite est déjà tracée !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer cette demi-droite !' }
      ext1 = scratch.scratchDecomposeEnPoints(ext1)
      if (ext1.length !== 2) return { result: 'prob', quoi: 'Il faut deux points pour nommer une droite !' }
      const ext2 = ext1[1]
      ext1 = ext1[0]
      scratch.params.afaire = []
      if (ext1 === ext2) return { result: 'prob', quoi: 'Il faut donner deux points distincts !' }
      if (!scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext1) })
      }
      if (!scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext2) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgDemiDroitePt(ext1, ext2) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'demidroitept' }
  }

  Blockly.JavaScript.cercleCentrePoint = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let centre = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'CENTRE', Blockly.JavaScript.ORDER_ATOMIC))
      let point = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'POINT', Blockly.JavaScript.ORDER_ATOMIC))
      let nom = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
      while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
      while (centre.indexOf('\\\'') !== -1) { centre = centre.replace('\\\'', '\'') }
      while (centre.indexOf('\\"') !== -1) { centre = centre.replace('\\"', '"') }
      while (point.indexOf('\\\'') !== -1) { point = point.replace('\\\'', '\'') }
      while (point.indexOf('\\"') !== -1) { point = point.replace('\\"', '"') }
      point = point.trim()
      nom = nom.trim()
      centre = centre.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'cercleCentrePoint', noms: [nom], point, centre })
        point = scratch.scratchMtgReplaceLettres2(point)
        nom = scratch.scratchMtgReplaceLettres2(nom)
        centre = scratch.scratchMtgReplaceLettres2(centre)
      }
      if (scratch.scratchMtgRetrouve({ nom }, 'tag')) return { result: 'prob', quoi: 'Ce nom est déjà pris !' }
      if (scratch.scratchMtgRetrouve({ centre, point }, 'tag')) return { result: 'prob', quoi: 'Ce cercle est déjà tracé !' }
      // verif centre
      if (centre === '') return { result: 'prob', quoi: 'Il faut nommer le centre !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(centre) === -1) {
        if (!scratch.scratchIsNomCorecct(centre, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour le centre !' }
      }
      // verif point
      if (point === '') return { result: 'prob', quoi: 'Il faut nommer le point !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(point) === -1) {
        if (!scratch.scratchIsNomCorecct(point, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour le point !' }
      }
      // verif nom
      if (nom === '') return { result: 'prob', quoi: 'Il faut nommer le cercle !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
        if (!scratch.scratchIsNomCorecct(nom, 'cercle')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour un cercle !' }
      }
      // chercje cercle centre point
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: centre }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(centre) })
      }
      if (!scratch.scratchMtgRetrouve({ nom: point }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(point) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgCercleCentrePoint(centre, point, nom) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'cercleCentrePoint' }
  }

  Blockly.JavaScript.cercleCentreRayon = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let centre = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'CENTRE', Blockly.JavaScript.ORDER_ATOMIC))
      let rayon = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'RAYON', Blockly.JavaScript.ORDER_ATOMIC))
      let nom = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
      while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
      while (centre.indexOf('\\\'') !== -1) { centre = centre.replace('\\\'', '\'') }
      while (centre.indexOf('\\"') !== -1) { centre = centre.replace('\\"', '"') }
      while (rayon.indexOf('\\\'') !== -1) { rayon = rayon.replace('\\\'', '\'') }
      while (rayon.indexOf('\\"') !== -1) { rayon = rayon.replace('\\"', '"') }
      rayon = rayon.trim()
      nom = nom.trim()
      centre = centre.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'cercleCentreRayon', noms: [nom], rayon, centre })
        rayon = scratch.scratchMtgReplaceLettres2(rayon)
        nom = scratch.scratchMtgReplaceLettres2(nom)
        centre = scratch.scratchMtgReplaceLettres2(centre)
      }
      if (scratch.scratchMtgRetrouve({ nom }, 'tag')) return { result: 'prob', quoi: 'Ce nom est déjà pris !' }
      if (scratch.scratchMtgRetrouve({ centre, rayon }, 'tag')) return { result: 'prob', quoi: 'Ce cercle est déjà tracé !' }
      // verif rayon
      if (rayon === '') return { result: 'prob', quoi: 'Il faut donner une longueur pour le rayon !' }
      const calc = scratch.calculeMG(rayon.replace(/,/g, '.'), 1)
      const calc2 = scratch.params.mtgApplecteur.valueOf(scratch.params.svgId, calc)
      if (calc2 === 0) return { result: 'prob', quoi: 'Le rayon est nul !' }
      if (calc2 === 'pb' || calc2 === -1) return { result: 'prob', quoi: 'Je ne peux pas calculer la longueur ' + rayon + ' !' }
      if (!cUnNbOuUneLonguer(rayon)) {
        return { result: 'prob', quoi: 'Il faut indiquer une longueur ou un nombre<br>pour le rayon !' }
      }// verif centre
      if (centre === '') return { result: 'prob', quoi: 'Il faut nommer le centre !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(centre) === -1) {
        if (!scratch.scratchIsNomCorecct(centre, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour le centre !' }
      }
      // verif nom
      if (nom === '') return { result: 'prob', quoi: 'Il faut nommer le cercle !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(nom) === -1) {
        if (!scratch.scratchIsNomCorecct(nom, 'cercle')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour un cercle !' }
      }
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: centre }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(centre) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgCercleCentreRayon(centre, calc, nom, rayon) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'cercleCentreRayon' }
  }

  Blockly.JavaScript.cercleDiametre = function (block) {
    const cose = function () {
      let ext1 = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (ext1.indexOf('\\\'') !== -1) { ext1 = ext1.replace('\\\'', '\'') }
      while (ext1.indexOf('\\"') !== -1) { ext1 = ext1.replace('\\"', '"') }
      ext1 = ext1.trim()
      let toSuite
      if (scratch.params.isEditing) {
        toSuite = { type: 'cercleDiametre', noms: [ext1] }
        ext1 = scratch.scratchMtgReplaceLettres2(ext1)
      }
      if (scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')) return { result: 'prob', quoi: 'Il y a déjà un élément nommé ' + ext1 + ' !' }
      if (ext1 === '') return { result: 'prob', quoi: 'Il faut nommer ce cercle !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(ext1) === -1) {
        if (!scratch.scratchIsNomCorecct(ext1, 'cercle')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour un cercle !' }
      }
      let cont = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'DIAM', Blockly.JavaScript.ORDER_ATOMIC))
      while (cont.indexOf('\\\'') !== -1) { cont = cont.replace('\\\'', '\'') }
      while (cont.indexOf('\\"') !== -1) { cont = cont.replace('\\"', '"') }
      cont = cont.trim()
      if (scratch.params.isEditing) {
        toSuite.cont = cont
        scratch.params.MtgProg.suite.push(toSuite)
        cont = scratch.scratchMtgReplaceLettres2(cont)
      }
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: '[' + cont + ']' }, 'tag')) {
        const base2 = scratch.scratchDecomposeEnPoints(cont)
        if (base2.length !== 2) return { result: 'prob', quoi: 'Il faut deux points pour nommer un segment !' }
        const ext2 = base2[1]
        const ext1 = base2[0]
        if (scratch.scratchLigneContient([ext1, ext2], '')) {
          scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
        } else { return { result: 'prob', quoi: 'Je ne trouve pas le segment "[' + cont + ']" !' } }
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgCercleDiametre(ext1, '[' + cont + ']') })
      scratch.scratchMtgFinishOrdre()
      return { result: 'ordre' }
    }
    return { type: 'ordre', javascript: cose, nom: 'cercleDiametre' }
  }

  Blockly.JavaScript.arc1PointPasse = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let point = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'POINT', Blockly.JavaScript.ORDER_ATOMIC))
      let nom = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
      while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
      while (point.indexOf('\\\'') !== -1) { point = point.replace('\\\'', '\'') }
      while (point.indexOf('\\"') !== -1) { point = point.replace('\\"', '"') }
      point = point.trim()
      nom = nom.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'arc1PointPasse', noms: [nom], point })
        point = scratch.scratchMtgReplaceLettres2(point)
        nom = scratch.scratchMtgReplaceLettres2(nom)
      }
      if (scratch.scratchMtgRetrouve({ nom, type: 'arc', point }, 'tag')) return { result: 'prob', quoi: 'Cet arc existe déjà !' }
      // verif point
      if (point === '') return { result: 'prob', quoi: 'Il faut nommer le point !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(point) === -1) {
        if (!scratch.scratchIsNomCorecct(point, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour le point !' }
      }
      // verif nom
      if (nom === '') {
        return {
          result: 'prob',
          quoi: 'Il faut nommer cet arc de cercle !'
        }
      }
      let ext1 = scratch.scratchDecomposeEnPoints(nom)
      if (ext1.length !== 2) return { result: 'prob', quoi: 'Il faut deux points pour nommer un arc de cercle !' }
      const ext2 = ext1[1]
      ext1 = ext1[0]
      if (ext1 === ext2) return { result: 'prob', quoi: 'Il faut donner deux points distincts pour extrémités !' }
      // verif sont pas alignés
      const p1 = scratch.scratchMtgRetrouve({ nom: point }, 'tag')
      const p2 = scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')
      const p3 = scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')
      if (p1 && p2 && p3) {
        const de1 = scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: p1 })
        const de2 = scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: p2 })
        const de3 = scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: p3 })
        if (scratch.scratchSontAligne(de1, de2, de3)) return { result: 'prob', quoi: 'Je ne peux pas faire un arc de cercle avec trois points alignés !' }
      }
      // chercje cercle centre point
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext1) })
      }
      if (!scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext2) })
      }
      if (!scratch.scratchMtgRetrouve({ nom: point }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(point) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgarc1PointPasse(point, ext1, ext2) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'arc1PointPasse' }
  }

  Blockly.JavaScript.arccentreplus = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let centre = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'CENTRE', Blockly.JavaScript.ORDER_ATOMIC))
      let nom = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
      while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
      while (centre.indexOf('\\\'') !== -1) { centre = centre.replace('\\\'', '\'') }
      while (centre.indexOf('\\"') !== -1) { centre = centre.replace('\\"', '"') }
      nom = nom.trim()
      centre = centre.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'arccentreplus', noms: [nom], centre })
        nom = scratch.scratchMtgReplaceLettres2(nom)
        centre = scratch.scratchMtgReplaceLettres2(centre)
      }
      if (scratch.scratchMtgRetrouve({ nom: 'arc ' + nom, type: 'arc', centre, sens: true }, 'tag')) return { result: 'prob', quoi: 'Cet arc existe déjà !' }
      // verif point
      if (centre === '') return { result: 'prob', quoi: 'Il faut nommer le centre !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(centre) === -1) {
        if (!scratch.scratchIsNomCorecct(centre, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour le centre !' }
      }
      // verif nom
      if (nom === '') return { result: 'prob', quoi: 'Il faut nommer cet arc de cercle !' }
      let ext1 = scratch.scratchDecomposeEnPoints(nom)
      if (ext1.length !== 2) return { result: 'prob', quoi: 'Il faut deux points pour nommer un arc de cercle !' }
      const ext2 = ext1[1]
      ext1 = ext1[0]
      if (ext1 === ext2) return { result: 'prob', quoi: 'Il faut donner deux points distincts pour extrémités !' }
      if (!scratch.scratchMtgRetrouve({ nom: centre }, 'tag') && scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag') && scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')) return { result: 'prob', quoi: 'Je ne trouve pas le centre !' }

      // verif sont pas alignés
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: centre }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(centre) })
      }
      const p1 = scratch.scratchMtgRetrouve({ nom: centre }, 'tag')
      let p2 = scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')
      const p3 = scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')
      if (p2 && p3 && p1) {
        if (!scratch.scracthOAegalOB(p1, p2, p3)) return { result: 'prob', quoi: 'Les longueurs ' + centre + ext1 + ' et ' + centre + ext2 + ' doivent être égales!' }
      } else if (!p2 && !p3) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext1) })
        p2 = true
      }
      if (!p2 && p3) {
        const nomaOub = scratch.params.scratchProgDonnees.nomsAlea.pop()
        scratch.params.afaire.push(() => { scratch.scratchMtgCercleCentrePoint(centre, ext2, nomaOub, true) })
        scratch.params.afaire.push(() => { scratch.scratchMtgPointSur(ext1, nomaOub) })
      } else if (!p3 && p2) {
        const nomaOub = scratch.params.scratchProgDonnees.nomsAlea.pop()
        scratch.params.afaire.push(() => { scratch.scratchMtgCercleCentrePoint(centre, ext1, nomaOub, true) })
        scratch.params.afaire.push(() => { scratch.scratchMtgPointSur(ext2, nomaOub) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgarccentre(centre, ext1, ext2, true) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'arccentreplus' }
  }

  Blockly.JavaScript.arccentremoins = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let centre = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'CENTRE', Blockly.JavaScript.ORDER_ATOMIC))
      let nom = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
      while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
      while (centre.indexOf('\\\'') !== -1) { centre = centre.replace('\\\'', '\'') }
      while (centre.indexOf('\\"') !== -1) { centre = centre.replace('\\"', '"') }
      nom = nom.trim()
      centre = centre.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'arccentremoins', noms: [nom], centre })
        nom = scratch.scratchMtgReplaceLettres2(nom)
        centre = scratch.scratchMtgReplaceLettres2(centre)
      }
      if (scratch.scratchMtgRetrouve({ nom, type: 'arc', centre, sens: false }, 'tag')) return { result: 'prob', quoi: 'Cet arc existe déjà !' }
      // verif point
      if (centre === '') return { result: 'prob', quoi: 'Il faut nommer le centre !' }
      if (scratch.params.scratchProgDonnees.nomsAleaBase.indexOf(centre) === -1) {
        if (!scratch.scratchIsNomCorecct(centre, 'point')) return { result: 'prob', quoi: 'Ce nom ne convient pas pour le centre !' }
      }
      // verif nom
      if (nom === '') return { result: 'prob', quoi: 'Il faut nommer cet arc de cercle !' }
      let ext1 = scratch.scratchDecomposeEnPoints(nom)
      if (ext1.length !== 2) return { result: 'prob', quoi: 'Il faut deux points pour nommer un arc de cercle !' }
      const ext2 = ext1[1]
      ext1 = ext1[0]
      if (ext1 === ext2) return { result: 'prob', quoi: 'Il faut donner deux points distincts pour extrémités !' }
      if (!scratch.scratchMtgRetrouve({ nom: centre }, 'tag') && scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag') && scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')) return { result: 'prob', quoi: 'Je ne trouve pas le centre !' }

      // verif sont pas alignés
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: centre }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(centre) })
      }
      const p1 = scratch.scratchMtgRetrouve({ nom: centre }, 'tag')
      let p2 = scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')
      const p3 = scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')
      if (p2 && p3 && p1) {
        if (!scratch.scracthOAegalOB(p1, p2, p3)) return { result: 'prob', quoi: 'Les longueurs ' + centre + ext1 + ' et ' + centre + ext2 + ' doivent être égales!' }
      } else if (!p2 && !p3) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext1) })
        p2 = true
      }
      if (!p2 && p3) {
        const nomaOub = scratch.params.scratchProgDonnees.nomsAlea.pop()
        scratch.params.afaire.push(() => { scratch.scratchMtgCercleCentrePoint(centre, ext2, nomaOub, true) })
        scratch.params.afaire.push(() => { scratch.scratchMtgPointSur(ext1, nomaOub) })
      } else if (!p3 && p2) {
        const nomaOub = scratch.params.scratchProgDonnees.nomsAlea.pop()
        scratch.params.afaire.push(() => { scratch.scratchMtgCercleCentrePoint(centre, ext1, nomaOub, true) })
        scratch.params.afaire.push(() => { scratch.scratchMtgPointSur(ext2, nomaOub) })
      }
      scratch.params.afaire.push(() => { scratch.scratchMtgarccentre(centre, ext1, ext2, false) })
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'arccentreplus' }
  }

  Blockly.JavaScript.angleplus = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      const mesure = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'MESURE', Blockly.JavaScript.ORDER_ATOMIC)).trim()
      let nom = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
      while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
      nom = nom.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'angleplus', noms: [nom], mesure })
        nom = scratch.scratchMtgReplaceLettres2(nom)
      }
      if (scratch.scratchMtgRetrouve({ nom: 'angle ' + nom }, 'tag')) return { result: 'prob', quoi: 'Cet angle existe déjà !' }
      if (nom === '') return { result: 'prob', quoi: 'Il faut nommer cet angle !' }
      if (mesure === '') return { result: 'prob', quoi: 'Il faut donner une mesure !' }
      if (isNaN(Number(mesure)) || Number(mesure) === 0) return { result: 'prob', quoi: 'La mesure est nulle !' }
      if (Number(mesure) > 180) return { result: 'prob', quoi: 'La mesure doit être inférieure à 180° !' }
      let ext1 = scratch.scratchDecomposeEnPoints(nom)
      if (ext1.length !== 3) return { result: 'prob', quoi: 'Il faut trois points pour nommer un angle !' }
      const ext2 = ext1[1]
      const ext3 = ext1[2]
      ext1 = ext1[0]
      if (scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag') && scratch.scratchMtgRetrouve({ nom: ext3 }, 'tag')) return { result: 'prob', quoi: 'Les points ' + ext1 + ' et ' + ext2 + ' sont déjà placés !' }
      // verif sont pas alignés
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext2) })
      }
      const p1 = scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')
      const p3 = scratch.scratchMtgRetrouve({ nom: ext3 }, 'tag')
      if (!p1 && !p3) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext1) })
        scratch.params.afaire.push(() => { scratch.scratchMtanglePlus(ext2, ext1, ext3, mesure, true) })
      } else if (!p1) {
        scratch.params.afaire.push(() => { scratch.scratchMtanglePlus(ext2, ext3, ext1, mesure, true) })
      } else {
        scratch.params.afaire.push(() => { scratch.scratchMtanglePlus(ext2, ext1, ext3, mesure, true) })
      }
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'angleplus' }
  }

  Blockly.JavaScript.anglemoins = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      const mesure = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'MESURE', Blockly.JavaScript.ORDER_ATOMIC)).trim()
      let nom = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
      while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
      nom = nom.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'angleplus', noms: [nom], mesure })
        nom = scratch.scratchMtgReplaceLettres2(nom)
      }
      if (scratch.scratchMtgRetrouve({ nom: 'angle ' + nom }, 'tag')) return { result: 'prob', quoi: 'Cet angle existe déjà !' }
      if (nom === '') return { result: 'prob', quoi: 'Il faut nommer cet angle !' }
      if (mesure === '') return { result: 'prob', quoi: 'Il faut donner une mesure !' }
      if (isNaN(Number(mesure)) || Number(mesure) === 0) return { result: 'prob', quoi: 'La mesure est nulle !' }
      if (Number(mesure) > 180) return { result: 'prob', quoi: 'La mesure doit être inférieure à 180° !' }
      let ext1 = scratch.scratchDecomposeEnPoints(nom)
      if (ext1.length !== 3) return { result: 'prob', quoi: 'Il faut trois points pour nommer un angle !' }
      const ext2 = ext1[1]
      const ext3 = ext1[2]
      ext1 = ext1[0]
      if (scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag') && scratch.scratchMtgRetrouve({ nom: ext3 }, 'tag')) return { result: 'prob', quoi: 'Les points ' + ext1 + ' et ' + ext2 + ' sont déjà placés !' }
      // verif sont pas alignés
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext2) })
      }
      const p1 = scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')
      const p3 = scratch.scratchMtgRetrouve({ nom: ext3 }, 'tag')
      if (!p1 && !p3) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext1) })
        scratch.params.afaire.push(() => { scratch.scratchMtanglePlus(ext2, ext1, ext3, mesure, false) })
      } else if (!p1) {
        scratch.params.afaire.push(() => { scratch.scratchMtanglePlus(ext2, ext3, ext1, mesure, false) })
      } else {
        scratch.params.afaire.push(() => { scratch.scratchMtanglePlus(ext2, ext1, ext3, mesure, false) })
      }
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'anglemoins' }
  }

  Blockly.JavaScript.triangleQuelconque = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let nom = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
      while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
      nom = nom.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'triangleQuelconque', noms: [nom] })
        nom = scratch.scratchMtgReplaceLettres2(nom)
      }
      if (nom === '') return { result: 'prob', quoi: 'Il faut nommer ce triangle !' }
      let ext1 = scratch.scratchDecomposeEnPoints(nom)
      if (ext1.length !== 3) return { result: 'prob', quoi: 'Il faut trois points pour nommer un triangle !' }
      const ext2 = ext1[1]
      const ext3 = ext1[2]
      if (ext1 === ext2 || ext1 === ext3 || ext3 === ext1) return { result: 'prob', quoi: 'Il faut donner trois points distincts !' }
      ext1 = ext1[0]
      scratch.params.afaire = []
      if (!scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext1) })
      }
      if (!scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext2) })
      }
      if (!scratch.scratchMtgRetrouve({ nom: ext3 }, 'tag')) {
        scratch.params.afaire.push(() => { scratch.scratchMtgPointLibre(ext3) })
      }
      const p1 = scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')
      const p2 = scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')
      const p3 = scratch.scratchMtgRetrouve({ nom: ext3 }, 'tag')
      let ok = false
      if (!p1) {
        ok = true
        scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
        scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext3) })
      }
      if (!p2) {
        ok = true
        scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext2, ext3) })
        if (p1) scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
      }
      if (!p3) {
        ok = true
        if (p2) scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext2, ext3) })
        if (p1) scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext3) })
      }
      if (p1 && p2 && p3) {
        if (!scratch.scratchLigneContient([ext1, ext2], '')) {
          ok = true
          scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
        }
        if (!scratch.scratchLigneContient([ext2, ext3], '')) {
          ok = true
          scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext2, ext3) })
        }
        if (!scratch.scratchLigneContient([ext1, ext3], '')) {
          ok = true
          scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext3) })
        }
      }
      if (!ok) return { result: 'prob', quoi: 'Ce triangle est déjà tracé !' }
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'triangleQuelconque' }
  }

  Blockly.JavaScript.triangleEquilateral = function (block) {
    const cose = function () {
      const aret = (scratch.params.isSpeed) ? 'ordre' : 'ordrew'
      let nom = scratch.vireGuillemets(Blockly.JavaScript.valueToCode(block, 'NOM', Blockly.JavaScript.ORDER_ATOMIC))
      while (nom.indexOf('\\\'') !== -1) { nom = nom.replace('\\\'', '\'') }
      while (nom.indexOf('\\"') !== -1) { nom = nom.replace('\\"', '"') }
      nom = nom.trim()
      if (scratch.params.isEditing) {
        scratch.params.MtgProg.suite.push({ type: 'triangleEquilateral', noms: [nom] })
        nom = scratch.scratchMtgReplaceLettres2(nom)
      }
      if (nom === '') return { result: 'prob', quoi: 'Il faut nommer ce triangle !' }
      let ext1 = scratch.scratchDecomposeEnPoints(nom)
      if (ext1.length !== 3) return { result: 'prob', quoi: 'Il faut trois points pour nommer un triangle !' }
      const ext2 = ext1[1]
      const ext3 = ext1[2]
      ext1 = ext1[0]
      if (ext1 === ext2 || ext1 === ext3 || ext3 === ext1) return { result: 'prob', quoi: 'Il faut donner trois points distincts !' }
      const afairep = []
      const afaireSeg = []
      const p1 = scratch.scratchMtgRetrouve({ nom: ext1 }, 'tag')
      const p2 = scratch.scratchMtgRetrouve({ nom: ext2 }, 'tag')
      const p3 = scratch.scratchMtgRetrouve({ nom: ext3 }, 'tag')
      let ok = false
      let ptAplace = ''
      if (!p1) {
        ok = true
        if (p2 && p3) {
          ptAplace = ext1
        } else {
          afairep.push(() => { scratch.scratchMtgPointLibre(ext1) })
        }
        afaireSeg.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
        afaireSeg.push(() => { scratch.scratchMtgSegment(ext1, ext3) })
      }
      if (!p2) {
        ok = true
        if (p3) {
          ptAplace = ext2
        } else {
          afairep.push(() => { scratch.scratchMtgPointLibre(ext2) })
        }
        afaireSeg.push(() => { scratch.scratchMtgSegment(ext2, ext3) })
        if (p1) afaireSeg.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
      }
      if (!p3) {
        ok = true
        ptAplace = ext3
        if (p2) afaireSeg.push(() => { scratch.scratchMtgSegment(ext2, ext3) })
        if (p1) afaireSeg.push(() => { scratch.scratchMtgSegment(ext1, ext3) })
      }
      if (p1 && p2 && p3) {
        if (!scratch.scratchLigneContient([ext1, ext2], '')) {
          ok = true
          scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext2) })
        }
        if (!scratch.scratchLigneContient([ext2, ext3], '')) {
          ok = true
          scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext2, ext3) })
        }
        if (!scratch.scratchLigneContient([ext1, ext3], '')) {
          ok = true
          scratch.params.afaire.push(() => { scratch.scratchMtgSegment(ext1, ext3) })
        }
        if (!scratch.scracthOAegalOB(p1, p2, p3) || !scratch.scracthOAegalOB(p2, p1, p3)) return { result: 'prob', quoi: 'Les longueurs ' + ext1 + ext2 + ' , ' + ext2 + ext3 + ' et ' + ext3 + ext1 + ' doivent être égales!' }
      }
      if (!ok) return { result: 'prob', quoi: 'Ce triangle est déjà tracé !' }
      if (ptAplace !== '') {
        afairep.push(() => { scratch.scratchMtgTriangleEquilateral(ext1, ext2, ext3, ptAplace) })
      }
      scratch.params.afaire = afairep.concat(afaireSeg)
      scratch.scratchMtgFinishOrdre()
      return { result: aret }
    }
    return { type: 'ordre', javascript: cose, nom: 'triangleEquilateral' }
  }
}
