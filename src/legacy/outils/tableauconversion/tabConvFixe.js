import { j3pAddElt, j3pElement } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'

/*
 * JP Vanroyen
 * Janvier 2013
 */

/** @module legacy/outils/tableauconversion */

/**
 * Construit un tableau de conversion dans conteneur
 * @param {HTMLElement|string} conteneur
 * @param {Object} options
 * @param {string[]} options.unite doit contenir l’unité (ou les unités) de ce (ou ces) tableau(x) de conversion, sous la forme d’un tableau, par ex ["m","m²"] pour avoir deux tableaux successifs (à la place de m², on peut mettre "m^2")
 * @param {boolean} options.are passer true pour ajouter les ares dans le tableau des m²
 * @param {boolean} options.litre passer true pour mettre les litres dans le tableau des m^3
 * @param {boolean} options.tonne passer true pour aller jusqu'à la tonne dans les mesures de masse
 */
function tabConvFixe (conteneur, options) {
  let ctId
  if (typeof conteneur === 'string') {
    ctId = conteneur
    conteneur = j3pElement(conteneur)
  } else {
    ctId = conteneur.id || ''
  }
  const { unite, are, litre, tonne } = options
  const arePresents = Boolean(are)
  const litrePresents = Boolean(litre)
  const tabUnitePossible = ['m', 'm^2', 'm^3', 'g', 'l']
  const tablisteUnites = [
    ['km', 'hm', 'dam', 'm', 'dm', 'cm', 'mm'],
    ['km&sup2;', 'hm&sup2;', 'dam&sup2;', 'm&sup2;', 'dm&sup2;', 'cm&sup2;', 'mm&sup2;'],
    ['km&sup3;', 'hm&sup3;', 'dam&sup3;', 'm&sup3;', 'dm&sup3;', 'cm&sup3;', 'mm&sup3;'],
    ['kg', 'hg', 'dag', 'g', 'dg', 'cg', 'mg'],
    ['', 'hL', 'daL', 'L', 'dL', 'cL', 'mL']
  ]
  const tabText = ['Tableau de conversion des unités de longueur',
    'Tableau de conversion des unités d’aire',
    'Tableau de conversion des unités de volume',
    'Tableau de conversion des unités de masse',
    'Tableau de conversion des unités de volume'
  ]
  if (tonne) {
    tablisteUnites[3].unshift('t', 'q', '')
  }
  const tabUnite = (typeof unite === 'string') ? [unite] : unite
  const divTableaux = j3pAddElt(conteneur, 'div')
  // on ne met un id que si le conteneur en a un
  if (ctId) divTableaux.id = ctId + 'Tableaux'
  for (let i = 0; i < tabUnite.length; i++) {
    if (tabUnite[i].indexOf('²') > -1) {
      tabUnite[i].replace('²', '^2')
    }
  }
  let ligne, ligneAre, ligneLitre
  for (let i = 0; i < tabUnite.length; i++) {
    const cell = j3pAddElt(divTableaux, 'div', '', { style: { textAlign: 'center' } })
    if (ctId) cell.id = ctId + 'Tableaux' + i
    j3pAffiche(cell, ctId + 'Tableaux' + i + '_Legende', '<u>' + tabText[tabUnitePossible.indexOf(tabUnite[i].toLowerCase())] + '</u>')
    const span = j3pAddElt(cell, 'span')
    if (ctId) span.id = ctId + 'Tableaux' + i + '_Tab'
    const nbColSpan = (tabUnite[i] === 'm^2') ? 2 : (tabUnite[i] === 'm^3') ? 3 : 1
    let texte = '<table width="100%" bordercolor="#000000" border="2" cellspacing="0">'
    ligne = '<tr>'
    for (let j = 0; j < tablisteUnites[tabUnitePossible.indexOf(tabUnite[i].toLowerCase())].length; j++) {
      ligne += '<td align="center" colspan=' + nbColSpan + '>' + tablisteUnites[tabUnitePossible.indexOf(tabUnite[i].toLowerCase())][j] + '</td>'
    }
    ligne += '</tr>'
    texte += ligne
    // si ce sont les unités d’aire, on peut ajouter la ligne des ares
    if ((tabUnite[i] === 'm^2') && arePresents) {
      ligneAre = ['', '', '', 'ha', '', 'a', '', 'ca', '', '', '', '', '', '']
      ligne = '<tr>'
      for (let j = 0; j < ligneAre.length; j++) {
        ligne += '<td align="center">' + ligneAre[j] + '</td>'
      }
      ligne += '</tr>'
      texte += ligne
    }
    // si ce sont les unités de volume (en m^3), on peut ajouter la ligne des litres
    if ((tabUnite[i] === 'm^3') && litrePresents) {
      ligneLitre = ['', '', '', '', '', '', '', '', '', '', '', '', 'hL', 'daL', 'L', 'dL', 'cL', 'mL', '', '', '']
      ligne = '<tr>'
      for (let j = 0; j < ligneLitre.length; j++) {
        ligne += '<td align="center">' + ligneLitre[j] + '</td>'
      }
      ligne += '</tr>'
      texte += ligne
    }
    // on ajoute enfin une ligne vide
    let nbCase = (tabUnite[i] === 'm^2') ? 14 : (tabUnite[i] === 'm^3') ? 21 : 7
    if (tabUnite[i] === 'g' && tonne) nbCase += 3
    ligne = '<tr>'
    for (let j = 0; j < nbCase; j++) {
      ligne += '<td align="center" width="' + (100 / nbCase) + '%">&nbsp;</td>'
    }
    ligne += '</tr>'
    texte += ligne
    texte += '</table>'
    span.innerHTML = texte
  }
}

export default tabConvFixe
