import { j3pDetruit, j3pEmpty, j3pGetNewId } from 'src/legacy/core/functions'
import BoutonStyleMathquill from 'src/legacy/outils/boutonsStyleMatquill'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable, addDefaultTableDetailed } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'

import './tableauConversionMobile.scss'

function TableauConversionMobile (conteneur, param) {
  this.out = false
  this.funcVirg = param.funcVirg
  this.lavirgOld = param.lavirgOld
  this.nb = param.nb
  this.conteneur = conteneur
  this.arePresents = (param.are === undefined) ? false : param.are
  this.litrePresents = (param.litre === undefined) ? false : param.litre
  this.tabUnitePossible = ['m', 'm²', 'm³', 'g', 'l']
  this.tablisteUnites = [
    ['km', 'hm', 'dam', 'm', 'dm', 'cm', 'mm'],
    ['km²', 'hm²', 'dam²', 'm²', 'dm²', 'cm²', 'mm²'],
    ['km³', 'hm³', 'dam³', 'm³', 'dm³', 'cm³', 'mm³'],
    ['kg', 'hg', 'dag', 'g', 'dg', 'cg', 'mg'],
    ['', 'hL', 'daL', 'L', 'dL', 'cL', 'mL']
  ]
  const tabText = ['Tableau de conversion des unités de longueur',
    'Tableau de conversion des unités d’aire',
    'Tableau de conversion des unités de volume',
    'Tableau de conversion des unités de masse',
    'Tableau de conversion des unités de volume'
  ]
  if (param.tonne) {
    this.tablisteUnites[3].unshift('t', 'q', '')
  }
  /**
   * Cellules du tableau principal
   * @type {TableCells}
   */
  this.tableCells = addDefaultTable(conteneur, 3, 2)
  j3pDetruit(this.tableCells[1][1])
  j3pDetruit(this.tableCells[2][1])
  this.tableCells[0][1].setAttribute('rowspan', 3)
  /**
   * Cellules de ???
   * @type {TableCells}
   */
  this.tableFcells = addDefaultTable(this.tableCells[1][0], 4, 15)
  this.unit1 = []

  this.unit = param.unite
  j3pAffiche(this.tableCells[0][0], null, '<u>' + tabText[this.tabUnitePossible.indexOf(param.unite.toLowerCase())] + '</u> ')
  this.tableCells[0][0].style.textAlign = 'center'
  this.nbColSpan = (param.unite === 'm²') ? 2 : (param.unite === 'm³') ? 3 : 1
  const objet = this
  this.lancVirg = function () { objet.afficheVirgule(this.num) }
  this.lancVirg2 = function () { objet.afficheVirgule2(this.num1, this.num2, this.num) }

  for (let j = 0; j < 7; j++) {
    const td = this.tableFcells[0][j + 4]
    this.unit1.push(addDefaultTableDetailed(td, 4, this.nbColSpan))
    const tabatrav = this.unit1[this.unit1.length - 1]
    for (let k = 0; k < this.nbColSpan; k++) {
      if (!param.clavier) j3pAffiche(tabatrav.cells[2][k], null, '&nbsp;')
      tabatrav.cells[3][k].style.padding = 0
      for (let o = 0; o < 3; o++) {
        tabatrav.cells[o][k].style.border = '1px solid black'
        tabatrav.cells[o][k].style.textAlign = 'center'
        tabatrav.cells[o][k].style.padding = 0
      }
    }
    for (let k = 1; k < tabatrav.cells[0].length; k++) {
      j3pDetruit(tabatrav.cells[0][k])
    }
    tabatrav.cells[0][0].setAttribute('colspan', this.nbColSpan)
    tabatrav.cells[0][0].style.width = '60px'
    tabatrav.table.style.width = '100%'
    tabatrav.table.style.background = '#ffffff'
    tabatrav.table.style.border = '2px solid black'
    tabatrav.table.style.borderCollapse = 'collapse'
    tabatrav.table.style.textAlign = 'center'
    j3pAffiche(tabatrav.cells[0][0], null, '$' + this.tablisteUnites[this.tabUnitePossible.indexOf(param.unite.toLowerCase())][j] + '$')
  }

  if ((param.unite === 'm²') && this.arePresents) {
    j3pAffiche(this.unit1[0].cells[1][0], null, '&nbsp;')
    j3pAffiche(this.unit1[0].cells[1][1], null, '&nbsp;')
    j3pAffiche(this.unit1[1].cells[1][0], null, 'ha')
    this.unit1[1].cells[1][0].style.color = '#fff'
    j3pAffiche(this.unit1[1].cells[1][1], null, 'ha')
    j3pAffiche(this.unit1[2].cells[1][0], null, 'a')
    this.unit1[2].cells[1][0].style.color = '#fff'
    j3pAffiche(this.unit1[2].cells[1][1], null, 'a')
    j3pAffiche(this.unit1[3].cells[1][0], null, 'ca')
    this.unit1[3].cells[1][0].style.color = '#fff'
    j3pAffiche(this.unit1[3].cells[1][1], null, 'ca')
    j3pAffiche(this.unit1[4].cells[1][0], null, '&nbsp;')
    j3pAffiche(this.unit1[4].cells[1][1], null, '&nbsp;')
    j3pAffiche(this.unit1[5].cells[1][0], null, '&nbsp;')
    j3pAffiche(this.unit1[5].cells[1][1], null, '&nbsp;')
    j3pAffiche(this.unit1[6].cells[1][0], null, '&nbsp;')
    j3pAffiche(this.unit1[6].cells[1][1], null, '&nbsp;')
  }
  if ((param.unite === 'm³') && this.litrePresents) {
    j3pAffiche(this.unit1[0].cells[1][0], null, '&nbsp;')
    j3pAffiche(this.unit1[0].cells[1][1], null, '&nbsp;')
    j3pAffiche(this.unit1[0].cells[1][2], null, '&nbsp;')
    j3pAffiche(this.unit1[1].cells[1][0], null, '&nbsp;')
    j3pAffiche(this.unit1[1].cells[1][1], null, '&nbsp;')
    j3pAffiche(this.unit1[1].cells[1][2], null, '&nbsp;')
    j3pAffiche(this.unit1[2].cells[1][0], null, '&nbsp;')
    j3pAffiche(this.unit1[2].cells[1][1], null, '&nbsp;')
    j3pAffiche(this.unit1[2].cells[1][2], null, '&nbsp;')
    j3pAffiche(this.unit1[3].cells[1][0], null, '&nbsp;')
    j3pAffiche(this.unit1[3].cells[1][1], null, '&nbsp;')
    j3pAffiche(this.unit1[3].cells[1][2], null, '&nbsp;')
    j3pAffiche(this.unit1[4].cells[1][0], null, 'hL')
    j3pAffiche(this.unit1[4].cells[1][1], null, 'daL')
    j3pAffiche(this.unit1[4].cells[1][2], null, 'L')
    j3pAffiche(this.unit1[5].cells[1][0], null, 'dL')
    j3pAffiche(this.unit1[5].cells[1][1], null, 'cL')
    j3pAffiche(this.unit1[5].cells[1][2], null, 'mL')
    j3pAffiche(this.unit1[6].cells[1][0], null, '&nbsp;')
    j3pAffiche(this.unit1[6].cells[1][1], null, '&nbsp;')
    j3pAffiche(this.unit1[6].cells[1][2], null, '&nbsp;')
  }

  if (param.clavier && !param.isco) {
    this.listZones = []
    this.listcases = [this.tableFcells[0][0], this.tableFcells[0][1], this.tableFcells[0][2], this.tableFcells[0][3]]
    for (let l = 0; l < 7; l++) {
      for (let h = 0; h < this.nbColSpan; h++) {
        const tt = addDefaultTable(this.unit1[l].cells[2][h], 1, 2)
        tt[0][0].style.padding = 0
        tt[0][1].style.padding = 0
        this.listcases.push(tt[0][0])
        this.unit1[l].cells[2][h] = tt[0][1]
      }
    }
    this.listcases.push(this.tableFcells[0][11])
    this.listcases.push(this.tableFcells[0][12])
    this.listcases.push(this.tableFcells[0][13])
    this.listcases.push(this.tableFcells[0][14])
    for (let l = 0; l < this.listcases.length; l++) {
      this.listcases[l].style.verticalAlign = 'bottom'
      this.listZones.push(new ZoneStyleMathquill1(this.listcases[l], {
        limite: 0,
        restric: '0123456789',
        forceParent: param.mepact,
        tabauto: true
      }))
    }
  }
  if (param.virgule && !param.isco) {
    this.aidetab = new BulleAide(this.tableCells[0][1], 'Clique sur une unité <BR> pour faire apparaître sa virgule', { place: 7 })
    for (let j = 0; j < 7; j++) {
      if (this.tablisteUnites[this.tabUnitePossible.indexOf(param.unite.toLowerCase())][j] !== '') {
        this.unit1[j].cells[0][0].num = j
        this.unit1[j].cells[0][0].setAttribute('class', 'tabConvUnite')
        this.unit1[j].cells[0][0].addEventListener('click', this.lancVirg, false)
      }
    }
    if ((param.unite === 'm²') && this.arePresents) {
      this.unit1[1].cells[1][1].num1 = 1
      this.unit1[1].cells[1][1].num2 = 1
      this.unit1[1].cells[1][1].num = 1
      this.unit1[1].cells[1][1].setAttribute('class', 'tabConvUnite')
      this.unit1[1].cells[1][1].addEventListener('click', this.lancVirg2, false)

      this.unit1[2].cells[1][1].num1 = 2
      this.unit1[2].cells[1][1].num2 = 1
      this.unit1[2].cells[1][1].num = 3
      this.unit1[2].cells[1][1].setAttribute('class', 'tabConvUnite')
      this.unit1[2].cells[1][1].addEventListener('click', this.lancVirg2, false)

      this.unit1[3].cells[1][1].num1 = 3
      this.unit1[3].cells[1][1].num2 = 1
      this.unit1[3].cells[1][1].num = 5
      this.unit1[3].cells[1][1].setAttribute('class', 'tabConvUnite')
      this.unit1[3].cells[1][1].addEventListener('click', this.lancVirg2, false)
    }
    if ((param.unite === 'm³') && this.litrePresents) {
      this.unit1[4].cells[1][0].num1 = 4
      this.unit1[4].cells[1][0].num2 = 0
      this.unit1[4].cells[1][0].num = 1
      this.unit1[4].cells[1][0].setAttribute('class', 'tabConvUnite')
      this.unit1[4].cells[1][0].addEventListener('click', this.lancVirg2, false)

      this.unit1[4].cells[1][1].num1 = 4
      this.unit1[4].cells[1][1].num2 = 1
      this.unit1[4].cells[1][1].num = 2
      this.unit1[4].cells[1][1].setAttribute('class', 'tabConvUnite')
      this.unit1[4].cells[1][1].addEventListener('click', this.lancVirg2, false)

      this.unit1[4].cells[1][2].num1 = 4
      this.unit1[4].cells[1][2].num2 = 2
      this.unit1[4].cells[1][2].num = 3
      this.unit1[4].cells[1][2].setAttribute('class', 'tabConvUnite')
      this.unit1[4].cells[1][2].addEventListener('click', this.lancVirg2, false)

      this.unit1[5].cells[1][0].num1 = 5
      this.unit1[5].cells[1][0].num2 = 0
      this.unit1[5].cells[1][0].num = 4
      this.unit1[5].cells[1][0].setAttribute('class', 'tabConvUnite')
      this.unit1[5].cells[1][0].addEventListener('click', this.lancVirg2, false)

      this.unit1[5].cells[1][1].num1 = 5
      this.unit1[5].cells[1][1].num2 = 1
      this.unit1[5].cells[1][1].num = 5
      this.unit1[5].cells[1][1].setAttribute('class', 'tabConvUnite')
      this.unit1[5].cells[1][1].addEventListener('click', this.lancVirg2, false)

      this.unit1[5].cells[1][2].num1 = 5
      this.unit1[5].cells[1][2].num2 = 2
      this.unit1[5].cells[1][1].num = 6
      this.unit1[5].cells[1][2].setAttribute('class', 'tabConvUnite')
      this.unit1[5].cells[1][2].addEventListener('click', this.lancVirg2, false)
    }
  }
  if (param.fleches && !param.isco) {
    this.listcases = [this.tableFcells[0][0], this.tableFcells[0][1], this.tableFcells[0][2], this.tableFcells[0][3]]
    for (let l = 0; l < 7; l++) {
      for (let h = 0; h < this.nbColSpan; h++) {
        this.listcases.push(this.unit1[l].cells[2][h])
      }
    }
    this.listcases.push(this.tableFcells[0][11])
    this.listcases.push(this.tableFcells[0][12])
    this.listcases.push(this.tableFcells[0][13])
    this.listcases.push(this.tableFcells[0][14])
    for (let l = 0; l < this.listcases.length; l++) {
      j3pAffiche(this.listcases[l], null, '$0$')
      this.listcases[l].style.padding = '0px 2px 0px 2px'
      this.listcases[l].style.width = '21px'
    }
    this.tableCells[2][0].style.textAlign = 'center'
    this.caseBout = addDefaultTable(this.tableCells[2][0], 1, 3)
    this.caseBout[0][0].style.width = '35%'
    this.caseBout[0][2].style.width = '35%'
    this.bout1 = new BoutonStyleMathquill(this.caseBout[0][1], 'bout1', 'copier le nombre', this.insere.bind(this), { taille: '90%' })
    this.listcases[0].style.verticalAlign = 'bottom'
    this.listcases[1].style.verticalAlign = 'bottom'
    this.listcases[2].style.verticalAlign = 'bottom'
    this.listcases[3].style.verticalAlign = 'bottom'
    this.tableFcells[0][11].style.verticalAlign = 'bottom'
    this.tableFcells[0][12].style.verticalAlign = 'bottom'
    this.tableFcells[0][13].style.verticalAlign = 'bottom'
    this.tableFcells[0][14].style.verticalAlign = 'bottom'
  }
  this.virgule = param.virgule
  this.clavier = param.clavier && !param.isco
  this.flech = param.fleches && !param.isco
  this.isco = param.isco
  if (param.isco) {
    this.listcases = [this.tableFcells[0][0], this.tableFcells[0][1], this.tableFcells[0][2], this.tableFcells[0][3]]
    for (let l = 0; l < 7; l++) {
      for (let h = 0; h < this.nbColSpan; h++) {
        this.listcases.push(this.unit1[l].cells[2][h])
      }
    }
    this.listcases.push(this.tableFcells[0][11])
    this.listcases.push(this.tableFcells[0][12])
    this.listcases.push(this.tableFcells[0][13])
    this.listcases.push(this.tableFcells[0][14])
    this.placeVirgDeb = this.getFuncVirg(param.uniteDep)
    this.placeVirgFin = this.getFuncVirg(param.uniteFin)
    setTimeout(this.animate.bind(this), 500)
    this.etatco = 'deb'
    for (let l = 0; l < this.listcases.length; l++) {
      this.listcases[l].style.verticalAlign = 'bottom'
    }
    for (let i = 0; i < this.listcases.length; i++) {
      j3pEmpty(this.listcases[i])
      j3pAffiche(this.listcases[i], null, '$0$')
      this.listcases[i].style.color = '#FFFFFF'
      this.listcases[i].style.background = '#FFFFFF'
      this.listcases[i].cache = true
    }
  }
}

TableauConversionMobile.prototype.animate = function animate () {
  let dep = 0
  if (this.out) return
  switch (this.etatco) {
    case 'deb':
      for (let l = 0; l < 7; l++) {
        this.unit1[l].cells[0][0].style.backgroundColor = '#ffffff'
        for (let h = 0; h < this.nbColSpan; h++) {
          this.unit1[l].cells[1][h].style.backgroundColor = '#ffffff'
          this.unit1[l].cells[2][h].style.backgroundColor = '#ffffff'
        }
      }
      j3pEmpty(this.tableCells[2][0])
      this.tableCells[2][0].style.textAlign = 'center'
      this.tableCells[2][0].style.color = '#0000FF'
      j3pAffiche(this.tableCells[2][0], null, '...<i> Je vais repèrer l’unité de départ </i> ...')
      for (let i = 0; i < this.listcases.length; i++) {
        j3pEmpty(this.listcases[i])
        j3pAffiche(this.listcases[i], null, '$0$')
        this.listcases[i].style.color = '#FFFFFF'
        this.listcases[i].cache = true
      }
      this.etatco = 'deb2'
      break
    case 'deb2':
      j3pEmpty(this.tableCells[2][0])
      j3pAffiche(this.tableCells[2][0], null, '...<i> Je vais placer ma valeur </i> ...')
      this.placeVirgDeb()
      this.etatco = 'deb3'
      break
    case 'deb3':
      j3pEmpty(this.tableCells[2][0])
      j3pAffiche(this.tableCells[2][0], null, '...<i> Je vais ajouter des zéros inutiles </i> ...')
      this.insere2()
      this.etatco = 'cont'
      break
    case 'cont':
      j3pEmpty(this.tableCells[2][0])
      j3pAffiche(this.tableCells[2][0], null, '...<i> Je vais repèrer l’unité d’arrivée </i> ...')
      for (let i = 0; i < this.listcases.length; i++) {
        if (this.listcases[i].cache) {
          this.listcases[i].style.color = '#7777ff'
        }
      }
      this.etatco = 'suite'
      break
    case 'suite':
      j3pEmpty(this.tableCells[2][0])
      j3pAffiche(this.tableCells[2][0], null, '...<i> Je vais retirer les zéros inutiles </i> ...')
      this.placeVirgFin()
      this.etatco = 'fin'
      break
    case 'fin':
      j3pEmpty(this.tableCells[2][0])
      j3pAffiche(this.tableCells[2][0], null, '...<i> terminé ! </i> ...')
      while (this.listcases[dep].textContent === '$0$0​' && dep < this.lavirg + 4) {
        this.listcases[dep].style.color = '#ffffff'
        dep++
      }
      dep = this.listcases.length - 1
      while (this.listcases[dep].textContent === '$0$0​') {
        this.listcases[dep].style.color = '#ffffff'
        dep--
      }
      this.etatco = 'deb'
      setTimeout(this.animate.bind(this), 4000)
      return
  }
  setTimeout(this.animate.bind(this), 2500)
}

TableauConversionMobile.prototype.getFuncVirg = function getFuncVirg (ob) {
  const t1 = ['k', 'h', 'da', '', 'd', 'c', 'm']
  const rangi = t1.indexOf(ob.rang)
  if (ob.unit === this.unit) {
    return function () { this.afficheVirgule(rangi) }
  } else {
    if (ob.unit === 'L') {
      const n1 = (rangi < 4) ? 4 : 5
      const n2 = (rangi + 2) % 3
      return function () { this.afficheVirgule2(n1, n2) }
    } else {
      switch (rangi) {
        case 1: return function () { this.afficheVirgule2(1, 1) }
        case 3: return function () { this.afficheVirgule2(2, 1) }
        case 5: return function () { this.afficheVirgule2(3, 1) }
      }
    }
  }
}

TableauConversionMobile.prototype.changeCool = function changeCool (cool) {
  this.conteneur.style.color = cool
}

TableauConversionMobile.prototype.afficheVirgule = function afficheVirgule (num) {
  if (this.lavirg !== undefined) {
    j3pDetruit(this.lavirgAff)
  }
  this.lavirg = num * this.nbColSpan + this.nbColSpan - 1
  for (let l = 0; l < 7; l++) {
    this.unit1[l].cells[0][0].style.backgroundColor = '#ffffff'
    for (let h = 0; h < this.nbColSpan; h++) {
      this.unit1[l].cells[1][h].style.backgroundColor = '#ffffff'
      this.unit1[l].cells[2][h].style.backgroundColor = '#ffffff'
    }
  }
  this.unit1[num].cells[0][0].style.backgroundColor = '#ffeedd'
  this.unit1[num].cells[2][this.nbColSpan - 1].style.backgroundColor = '#ffeedd'
  this.lavirgAff = j3pGetNewId()
  this.unit1[num].cells[2][this.nbColSpan - 1].style.nowrap = true
  j3pAffiche(this.unit1[num].cells[2][this.nbColSpan - 1], this.lavirgAff, '<b>,</b>', { style: { color: '#FF0000' } })
  this.unit1[num].cells[2][this.nbColSpan - 1].style.textAlign = 'right'
  this.virgMem = { t: true, num }
  if (this.funcVirg) this.funcVirg()
}

TableauConversionMobile.prototype.vireFauxVirg = function vireFauxVirg () {
  for (let j = 0; j < 7; j++) {
    const tabatrav = this.unit1[j]
    this.unit1[j].cells[0][0].style.background = '#ffffff'
    for (let k = 0; k < this.nbColSpan; k++) {
      for (let o = 1; o < 3; o++) {
        if (!tabatrav.cells[o][k].garde) tabatrav.cells[o][k].style.background = '#ffffff'
      }
    }
  }
}

TableauConversionMobile.prototype.afficheVirgule2 = function afficheVirgule2 (j1, j2, j3) {
  if (this.lavirg) {
    j3pDetruit(this.lavirgAff)
  }
  this.lavirg = j1 * this.nbColSpan + j2
  for (let l = 0; l < 7; l++) {
    this.unit1[l].cells[0][0].style.backgroundColor = '#ffffff'
    for (let h = 0; h < this.nbColSpan; h++) {
      this.unit1[l].cells[1][h].style.backgroundColor = '#ffffff'
      this.unit1[l].cells[2][h].style.backgroundColor = '#ffffff'
    }
  }
  this.unit1[j1].cells[1][j2].style.backgroundColor = '#ffeedd'
  this.unit1[j1].cells[2][j2].style.backgroundColor = '#ffeedd'
  this.lavirgAff = j3pGetNewId()
  j3pAffiche(this.unit1[j1].cells[2][j2], this.lavirgAff, '<b>,</b>', { style: { color: '#FF0000' } })
  this.unit1[j1].cells[2][j2].style.textAlign = 'right'
  this.virgMem = { t: false, num: j3, num1: j1, num2: j2 }
  if (this.funcVirg) this.funcVirg()
}

TableauConversionMobile.prototype.colorVirg = function colorVirg (good) {
  const color = good === true ? '#55aa66' : (good !== 'co') ? '#997733' : '#6666FF'
  const g = color !== '#997733'
  if (this.virgMem.t) {
    this.unit1[this.virgMem.num].cells[0][0].style.backgroundColor = color
    this.unit1[this.virgMem.num].cells[2][this.nbColSpan - 1].style.backgroundColor = color
    if (g) {
      this.unit1[this.virgMem.num].cells[2][this.nbColSpan - 1].garde = true
      if (this.lavirg) {
        j3pDetruit(this.lavirgAff)
      }
      this.lavirgAff = j3pGetNewId()
      j3pAffiche(this.unit1[this.virgMem.num].cells[2][this.nbColSpan - 1], this.lavirgAff, '<b>,</b>', { style: { color: '#FF0000' } })
      this.unit1[this.virgMem.num].cells[2][this.nbColSpan - 1].style.textAlign = 'right'
      this.lavirg = this.virgMem.num * this.nbColSpan + this.nbColSpan - 1
    }
  } else {
    this.unit1[this.virgMem.num1].cells[1][this.virgMem.num2].style.backgroundColor = color
    this.unit1[this.virgMem.num1].cells[2][this.virgMem.num2].style.backgroundColor = color
    if (g) {
      this.unit1[this.virgMem.num1].cells[2][this.virgMem.num2].garde = true
      if (this.lavirg) {
        j3pDetruit(this.lavirgAff)
      }
      this.lavirgAff = j3pGetNewId()
      j3pAffiche(this.unit1[this.virgMem.num1].cells[2][this.virgMem.num2], this.lavirgAff, '<b>,</b>', { style: { color: '#FF0000' } })
      this.unit1[this.virgMem.num1].cells[2][this.virgMem.num2].style.textAlign = 'right'
      this.lavirg = this.virgMem.num1 * this.nbColSpan + this.virgMem.num2
    }
  }
}

TableauConversionMobile.prototype.disable = function disable () {
  if (this.flech) {
    j3pEmpty(this.tableCells[2][0])
  }
  if (this.clavier) {
    for (let i = 0; i < this.listZones.length; i++) this.listZones[i].disable()
  }
  if (this.virgule) {
    this.aidetab.disable()
    j3pEmpty(this.tableCells[0][1])
    for (let j = 0; j < 7; j++) {
      this.unit1[j].cells[0][0].num = j
      this.unit1[j].cells[0][0].classList.remove('tabConvUnite')
      this.unit1[j].cells[0][0].removeEventListener('click', this.mafoncclic)
    }
    if ((this.unit === 'm²') && this.arePresents) {
      this.unit1[1].cells[1][1].classList.remove('tabConvUnite')
      this.unit1[1].cells[1][1].removeEventListener('click', this.mafoncclic2)

      this.unit1[2].cells[1][1].classList.remove('tabConvUnite')
      this.unit1[2].cells[1][1].removeEventListener('click', this.mafoncclic2)

      this.unit1[3].cells[1][1].classList.remove('tabConvUnite')
      this.unit1[3].cells[1][1].removeEventListener('click', this.mafoncclic2)
    }
    if ((this.unit === 'm³') && this.litrePresents) {
      this.unit1[4].cells[1][0].classList.remove('tabConvUnite')
      this.unit1[4].cells[1][0].removeEventListener('click', this.mafoncclic2)

      this.unit1[4].cells[1][1].classList.remove('tabConvUnite')
      this.unit1[4].cells[1][1].removeEventListener('click', this.mafoncclic2)

      this.unit1[4].cells[1][2].classList.remove('tabConvUnite')
      this.unit1[4].cells[1][2].removeEventListener('click', this.mafoncclic2)

      this.unit1[5].cells[1][0].classList.remove('tabConvUnite')
      this.unit1[5].cells[1][0].removeEventListener('click', this.mafoncclic2)

      this.unit1[5].cells[1][1].classList.remove('tabConvUnite')
      this.unit1[5].cells[1][1].removeEventListener('click', this.mafoncclic2)

      this.unit1[5].cells[1][2].classList.remove('tabConvUnite')
      this.unit1[5].cells[1][2].removeEventListener('click', this.mafoncclic2)
    }
  }
}

TableauConversionMobile.prototype.directInsere = function directInsere (bool) {
  const tt0 = addDefaultTable(this.tableFcells[0][0], 2, 1)
  const tt1 = addDefaultTable(this.tableFcells[0][1], 2, 1)
  const tt2 = addDefaultTable(this.tableFcells[0][2], 2, 1)
  const tt3 = addDefaultTable(this.tableFcells[0][3], 2, 1)
  const tt11 = addDefaultTable(this.tableFcells[0][11], 2, 1)
  const tt12 = addDefaultTable(this.tableFcells[0][12], 2, 1)
  const tt13 = addDefaultTable(this.tableFcells[0][13], 2, 1)
  const tt14 = addDefaultTable(this.tableFcells[0][14], 2, 1)
  tt0[1][0].style.padding = '0px 0px 1px 0px'
  tt1[1][0].style.padding = '0px 0px 1px 0px'
  tt2[1][0].style.padding = '0px 0px 1px 0px'
  tt3[1][0].style.padding = '0px 0px 1px 0px'
  tt11[1][0].style.padding = '0px 0px 1px 0px'
  tt12[1][0].style.padding = '0px 0px 1px 0px'
  tt13[1][0].style.padding = '0px 0px 1px 0px'
  tt14[1][0].style.padding = '0px 0px 1px 0px'
  this.listcases = [tt0[0][0], tt1[0][0], tt2[0][0], tt3[0][0]]
  this.listcasesco = [tt0[1][0], tt1[1][0], tt2[1][0], tt3[1][0]]
  for (let l = 0; l < 7; l++) {
    for (let h = 0; h < this.nbColSpan; h++) {
      this.listcases.push(this.unit1[l].cells[2][h])
      this.listcasesco.push(this.unit1[l].cells[3][h])
    }
  }
  this.listcases.push(tt11[0][0])
  this.listcases.push(tt12[0][0])
  this.listcases.push(tt13[0][0])
  this.listcases.push(tt14[0][0])
  this.listcasesco.push(tt11[1][0])
  this.listcasesco.push(tt12[1][0])
  this.listcasesco.push(tt13[1][0])
  this.listcasesco.push(tt14[1][0])
  for (let l = 0; l < this.listcases.length; l++) {
    j3pAffiche(this.listcases[l], null, '$0$')
    this.listcases[l].style.padding = '0px 2px 0px 2px'
    this.listcases[l].style.width = '21px'
  }
  this.tableCells[2][0].style.textAlign = 'center'
  this.caseBout = addDefaultTable(this.tableCells[2][0], 1, 3)
  this.caseBout[0][0].style.width = '35%'
  this.caseBout[0][2].style.width = '35%'
  this.tableFcells[0][0].style.verticalAlign = 'bottom'
  this.tableFcells[0][1].style.verticalAlign = 'bottom'
  this.tableFcells[0][2].style.verticalAlign = 'bottom'
  this.tableFcells[0][3].style.verticalAlign = 'bottom'
  this.tableFcells[0][11].style.verticalAlign = 'bottom'
  this.tableFcells[0][12].style.verticalAlign = 'bottom'
  this.tableFcells[0][13].style.verticalAlign = 'bottom'
  this.tableFcells[0][14].style.verticalAlign = 'bottom'
  if (bool) {
    this.insere4()
  } else {
    this.insere()
  }
}

TableauConversionMobile.prototype.disaClav = function disaClav () {
  if (this.clavier) {
    for (let i = 0; i < this.listZones.length; i++) this.listZones[i].disable()
  }
}

TableauConversionMobile.prototype.insere = function insere () {
  if (this.lavirg !== undefined) {
    j3pDetruit(this.lavirgAff)
  }
  const jj = String(this.nb).replace('.', '')
  const nbc = jj.length
  const pl = Math.floor((this.listcases.length - nbc) / 2)
  for (let i = 0; i < nbc; i++) {
    j3pEmpty(this.listcases[i + pl])
    j3pAffiche(this.listcases[i + pl], null, '$' + jj[i] + '$')
  }

  j3pEmpty(this.caseBout[0][1])
  j3pAffiche(this.caseBout[0][1], null, '&nbsp;Déplacer le nombre&nbsp;')
  this.bout1 = new BoutonStyleMathquill(this.caseBout[0][0], 'bout1', '<=', this.depgauche.bind(this), { taille: '90%' })
  this.bout2 = new BoutonStyleMathquill(this.caseBout[0][2], 'bout2', '=>', this.depdroite.bind(this), { taille: '90%' })
  this.caseBout[0][0].style.width = '0%'
  this.caseBout[0][2].style.width = '0%'
  this.caseBout[0][1].style.width = '100%'
  this.caseBout[0][1].style.textAlign = 'center'
  this.plnb = pl

  if (this.lavirg !== undefined) {
    this.lavirgAff = j3pGetNewId()
    j3pAffiche(this.listcases[this.lavirg + 4], this.lavirgAff, '<b>,</b>', { style: { color: '#FF0000' } })
  }
}

TableauConversionMobile.prototype.insere2 = function insere2 () {
  j3pDetruit(this.lavirgAff)
  const jj = String(this.nb).replace('.', '')
  const nbc = jj.length
  const one = String(this.nb).indexOf('.')
  const pla = (one !== -1) ? one : nbc
  for (let i = 0; i < nbc; i++) {
    j3pEmpty(this.listcases[this.lavirg - pla + i + 5])
    j3pAffiche(this.listcases[this.lavirg - pla + i + 5], null, '$' + jj[i] + '$')
    this.listcases[this.lavirg - pla + i + 5].style.color = '#0000FF'
    this.listcases[this.lavirg - pla + i + 5].cache = false
  }
  this.lavirgAff = j3pGetNewId()
  j3pAffiche(this.listcases[this.lavirg + 4], this.lavirgAff, '<b>,</b>', { style: { color: '#FF0000' } })
}

TableauConversionMobile.prototype.insere4 = function insere4 () {
  const jj = String(this.nb).replace('.', '')
  const nbc = jj.length
  const one = String(this.nb).indexOf('.')
  const pla = (one !== -1) ? one : nbc
  for (let i = 0; i < nbc; i++) {
    j3pEmpty(this.listcases[this.lavirgOld - pla + i + 5])
    j3pAffiche(this.listcases[this.lavirgOld - pla + i + 5], null, '$' + jj[i] + '$')
    this.listcases[this.lavirgOld - pla + i + 5].style.color = '#0000FF'
    this.listcases[this.lavirgOld - pla + i + 5].cache = false
  }
}

TableauConversionMobile.prototype.insere3 = function insere3 () {
  const jj = String(this.nb).replace('.', '')
  const nbc = jj.length
  const one = String(this.nb).indexOf('.')
  const pla = (one !== -1) ? one : nbc
  for (let i = 0; i < nbc; i++) {
    j3pEmpty(this.listcasesco[this.lavirg - pla + i + 5])
    j3pAffiche(this.listcasesco[this.lavirg - pla + i + 5], null, '$' + jj[i] + '$')
    this.listcasesco[this.lavirg - pla + i + 5].cache = false
  }
  this.lavirgAff2 = j3pGetNewId()
  j3pAffiche(this.listcasesco[this.lavirg + 4], this.lavirgAff2, '<b>,</b>', { style: { color: '#FF0000' } })
}

TableauConversionMobile.prototype.bnPLa = function bnPLa () {
  const jj = String(this.nb).replace('.', '')
  const nbc = jj.length
  const one = String(this.nb).indexOf('.')
  const pla = (one !== -1) ? one : nbc
  return this.lavirg - pla + 5
}

TableauConversionMobile.prototype.depgauche = function depgauche () {
  if (this.plnb === 0) return
  if (this.lavirg !== undefined) {
    j3pDetruit(this.lavirgAff)
  }
  const jj = String(this.nb).replace('.', '')
  const nbc = jj.length
  let pl = this.plnb
  for (let i = 0; i < nbc; i++) {
    j3pEmpty(this.listcases[i + pl])
    j3pAffiche(this.listcases[i + pl], null, '$0$')
  }
  this.plnb--
  pl = this.plnb
  for (let i = 0; i < nbc; i++) {
    j3pEmpty(this.listcases[i + pl])
    j3pAffiche(this.listcases[i + pl], null, '$' + jj[i] + '$')
  }
  if (this.lavirg !== undefined) {
    this.lavirgAff = j3pGetNewId()
    j3pAffiche(this.listcases[this.lavirg + 4], this.lavirgAff, '<b>,</b>', { style: { color: '#FF0000' } })
  }
}

TableauConversionMobile.prototype.depdroite = function depdroite () {
  const jj = String(this.nb).replace('.', '')
  const nbc = jj.length
  if (this.plnb + nbc === this.listcases.length) return
  if (this.lavirg !== undefined) {
    j3pDetruit(this.lavirgAff)
  }
  let pl = this.plnb
  for (let i = 0; i < nbc; i++) {
    j3pEmpty(this.listcases[i + pl])
    j3pAffiche(this.listcases[i + pl], null, '$0$')
  }
  this.plnb++
  pl = this.plnb
  for (let i = 0; i < nbc; i++) {
    j3pEmpty(this.listcases[i + pl])
    j3pAffiche(this.listcases[i + pl], null, '$' + jj[i] + '$')
  }
  if (this.lavirg !== undefined) {
    this.lavirgAff = j3pGetNewId()
    j3pAffiche(this.listcases[this.lavirg + 4], this.lavirgAff, '<b>,</b>', { style: { color: '#FF0000' } })
  }
}

export default TableauConversionMobile
