import { j3pAddElt, j3pElement } from 'src/legacy/core/functions'
import $ from 'jquery'

/**
 * Construit un tableau à double entrée dans un conteneur
 * @private
 * @param {HTMLElement|string} [madiv] conteneur du tableau
 * @param {Object} [obj] objet contenant les données du tableau
 * @param {Object} [obj.lestyle] objet de propriétés couleur et styletexte (de la forme me.styles.petit.enonce par exemple)
 * @param {Array<*>} [obj.nomsLigne] tableau contenant les textes de la première ligne
 * @param {Array<*>} [obj.nomsColonne] tableau contenant les textes de la première colonne
 * @param {Object} [obj.effectifs] obj.effectifs['ligne'+i] contiendra (sous la forme d’un array) les effectifs sur chaque ligne associée.
 * S’il est undefined ou vide, alors on construira un elt HTML dans cette case pour la compléter (éventuellement avec une zone de saisie)
 * @return {Array<*>} Tableau de tableaux avec les elts HTML (td) des cases qui sont non remplies (chaine vide sinon)
 */
function tabDoubleEntree (madiv, obj) {
  const ladiv = (typeof (madiv) === 'string') ? j3pElement(madiv) : madiv
  let largeurTexte = 0
  for (let i = 0; i < obj.nomsLigne.length; i++) {
    largeurTexte = Math.max(largeurTexte, $(ladiv).textWidth(obj.nomsLigne[i], obj.lestyle.styletexte.fontFamily, obj.lestyle.styletexte.fontSize))
  }
  largeurTexte += 20
  const largeurColonne = (obj.largeurTab - largeurTexte - 5) / obj.nomsLigne.length
  const laCouleur = (obj.lestyle.couleur) ? obj.lestyle.couleur : obj.lestyle.styletexte.color
  const table = j3pAddElt(ladiv, 'table', { width: '97%', borderColor: laCouleur, border: 2, cellSpacing: 0 })
  // Première ligne
  const ligne1 = j3pAddElt(table, 'tr')
  j3pAddElt(ligne1, 'th')
  for (let k = 0; k < obj.nomsLigne.length; k++) {
    const laLarg = (k === 0) ? largeurTexte : largeurColonne
    j3pAddElt(ligne1, 'th', obj.nomsLigne[k], { align: 'center', width: laLarg + 'px' })
  }
  const tabEltHtml = []
  for (let k = 0; k < obj.nomsColonne.length; k++) {
    tabEltHtml.push([])
    const ligne = j3pAddElt(table, 'tr')
    j3pAddElt(ligne, 'th', obj.nomsColonne[k], { align: 'center', width: largeurTexte + 'px' })
    for (let k2 = 0; k2 < obj.nomsLigne.length; k2++) {
      tabEltHtml[k].push('')
      if (obj.effectifs['ligne' + (k + 1)][k2] || (String(obj.effectifs['ligne' + (k + 1)][k2]) === '0')) {
        j3pAddElt(ligne, 'td', obj.effectifs['ligne' + (k + 1)][k2], { align: 'center', width: largeurColonne + 'px' })
      } else {
        // si effectifs['ligne..'] n’existe pas (ou vaut '') alors on crée une cellule vide
        // cet élément html sera retourné dans la section pour qu’on puisse éventuellement y mettre une zone de saisie
        tabEltHtml[k][k2] = j3pAddElt(ligne, 'td', { align: 'center', width: largeurColonne + 'px' })
      }
    }
  }
  const th = $('th')
  th.css('background-color', 'rgba(120,120,120,0.25)')
  th.css('font-weight', 'bold')
  return tabEltHtml
}

export default tabDoubleEntree
