import $ from 'jquery'
import { j3pAddElt, j3pElement, j3pFocus, j3pShowError, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
/**
 * Construit un tableau complet correspondant à une loi de probabilité
 * @private
 * @param {Object} [mesDonnees] objet contenant les données du tableau
 * @param {HTMLElement} [mesDonnees.parent] objet de propriétés couleur et styletexte (de la forme me.styles.petit.enonce par exemple)
 * @param {number} [mesDonnees.largeur] largeur du tableau (en pixels)
 * @param {string} [mesDonnees.typeFont] font utilisée pour le texte dans le div (par exemple me.styles.petit.enonce). On va récupérer les propriétés fontFamily et fontSize)
 * @param {Array<2>} [mesDonnees.legendes] légendes du tableau (par exemple : ['issues', 'probabilités']
 * @param {Array<*>} [mesDonnees.textes] tableau contenant les tests de la première ligne
 * @param {Array<*>} [mesDonnees.probas] tableau contenant des nombres pour la deuxième ligne (des probas)
 * @return {undefined} Cette fonction ne renvoie rien, elle construit juste le tableau
 */
export function tableauDonneesPrbs (mesDonnees) {
  function creerLigneTab (elt, valeurs, l1, l2, isNumber) {
    // elt est la table dans laquelle construire la ligne
    // l1 et l2 sont respectivement la largeur de la première colonne et des suivantes
    // isNumber est un booléen qui vaut true si les valeurs sont des nombres
    const ligne = j3pAddElt(elt, 'tr')
    j3pAddElt(ligne, 'td', valeurs[0], { width: l1 + 'px' })
    for (let i = 1; i < valeurs.length; i++) {
      const val = (isNumber && /\d+[,.]\d+/.test(valeurs[i])) ? j3pVirgule(valeurs[i]) : valeurs[i]
      j3pAddElt(ligne, 'td', val, { width: l2 + 'px', align: 'center' })
    }
    return ligne
  }
  const divConteneur = (typeof mesDonnees.parent === 'string') ? j3pElement(mesDonnees.parent) : mesDonnees.parent
  const leTableau = j3pAddElt(divConteneur, 'div', '', { style: { marginBottom: '1em' } })
  leTableau.setAttribute('width', mesDonnees.largeur + 'px')
  if (mesDonnees.textes.length === mesDonnees.probas.length) {
    let largeurTexte = 0
    for (let i = 0; i < mesDonnees.legendes.length; i++) {
      largeurTexte = Math.max(largeurTexte, $(leTableau).textWidth(mesDonnees.legendes[i], mesDonnees.typeFont.fontFamilly, mesDonnees.typeFont.fontSize))
    }
    largeurTexte += 20
    const largeurColonne = (mesDonnees.largeur - largeurTexte - 5) / mesDonnees.textes.length
    const table = j3pAddElt(leTableau, 'table', { width: '100%', borderColor: 'black', border: 2, cellSpacing: 0 })
    creerLigneTab(table, [mesDonnees.legendes[0]].concat(mesDonnees.textes), largeurTexte, largeurColonne, false)
    creerLigneTab(table, [mesDonnees.legendes[1]].concat(mesDonnees.probas), largeurTexte, largeurColonne, true)
  } else {
    j3pShowError('pb sur le nombre de valeurs dans les deux tableaux')
  }
}

/**
 * Construit un tableau correspondant à une loi de probabilité, avec éventuellement des zones de saisie
 * @private
 * @param {Object} [mesDonnees] objet contenant les données du tableau
 * @param {HTMLElement} [mesDonnees.parent] objet de propriétés couleur et styletexte (de la forme me.styles.petit.enonce par exemple)
 * @param {number} [mesDonnees.largeur] largeur du tableau (en pixels)
 * @param {string} [mesDonnees.typeFont] font utilisée pour le texte dans le div (par exemple me.styles.petit.enonce). On va récupérer les propriétés fontFamily et fontSize)
 * @param {Array<2>} [mesDonnees.legendes] est un tableau de 2 éléments : les légendes (x_i et proba)
 * @param {Array<*>} [mesDonnees.valeurs] tableau contenant les valeurs de X
 * @param {Array<*>} [mesDonnees.probas] tableau contenant des nombres pour la deuxième ligne (des probas)
 * @param {Array<2>} [mesDonnees.restrict] tableau de 2 éléments : les touches autorisées pour chaque ligne
 * @param {Boolean} [mesDonnees.complet] booléen qui prend la valeur true quand les réponses sont données (sinon les valeurs de X sont remplacées par des input simples et les proba par des zones de saisie mq)
 * @return {Array} Cette fonction renvoie la liste des zones de saisies du tableau
 */
export function tabLoiProba (mesDonnees) {
  const { parent, largeur, legendes, valeurs, probas, complet, typeFont, restrict } = mesDonnees
  parent.setAttribute('width', largeur + 'px')
  if (valeurs.length === probas.length) {
    let largeurTexte = 0
    for (let i = 0; i < legendes.length; i++) {
      largeurTexte = Math.max(largeurTexte, $(parent).textWidth(legendes[i], typeFont.fontFamily, typeFont.fontSize))
    }
    largeurTexte += 20
    const alignementLegendes = (/P\([U-Z]=/.test(legendes[1])) ? 'center' : 'left'
    const largeurColonne = (largeur - largeurTexte - 5) / valeurs.length
    // parent.appendChild(div)
    const table = j3pAddElt(parent, 'table', { width: '100%', border: 2, borderColor: 'black', cellSpacing: 0 })
    j3pStyle(table, { marginBottom: '1em' })
    const idValTxt = []
    const idPrbTxt = []
    for (let j = 0; j <= 1; j++) {
      const ligne = j3pAddElt(table, 'tr')
      const legende = j3pAddElt(ligne, 'td', { width: largeurTexte + 'px', align: alignementLegendes })
      j3pAffiche(legende, '', legendes[j])
      for (let i = 0; i < valeurs.length; i++) {
        const eltHtml = j3pAddElt(ligne, 'td', { width: largeurColonne + 'px', align: 'center' })
        if (j === 0) idValTxt.push(eltHtml)
        else idPrbTxt.push(eltHtml)
      }
    }
    // Maintenant, je remplis cette dernière ligne soit avec les proba, soit avec des zones inputmq (pour pouvoir mettre au format fractionnaire)
    const zoneInput = []
    for (let i = 0; i < valeurs.length; i++) {
      if (complet) {
        j3pAffiche(idValTxt[i], '', '$' + String(valeurs[i]) + '$')
        j3pAffiche(idPrbTxt[i], '', '$' + probas[i] + '$')
      } else {
        const textValx = (valeurs[i] === '?') ? '' : valeurs[i]
        const elt1 = j3pAffiche(idValTxt[i], '', '&1&', { inputmq1: { texte: textValx } })
        const textPrb = (probas[i] === '-1') ? '' : probas[i]
        const elt2 = j3pAffiche(idPrbTxt[i], '', '&1&', { inputmq1: { texte: textPrb } })
        zoneInput.push(elt1.inputmqList[0], elt2.inputmqList[0])
        mqRestriction(elt1.inputmqList[0], restrict[0])
        mqRestriction(elt2.inputmqList[0], restrict[1])
        elt1.inputmqList[0].typeReponse = ['nombre', 'exact']
        elt2.inputmqList[0].typeReponse = ['nombre', 'exact']
        elt2.inputmqList[0].solutionSimplifiee = ['reponse fausse', 'valide et on accepte']
      }
    }
    if (!complet) j3pFocus(zoneInput[0])
    // on renvoie un objet contenant le nom des zones de saisie (lorsqu’elles existent) et le tableau des effectifs cumulés croissants
    return zoneInput
  }
  j3pShowError('pb sur le nombre de valeurs dans les deux tableaux')
}
