import { j3pAddElt, j3pAjouteBouton, j3pElement, j3pEmpty, j3pFocus, j3pGetNewId, j3pPaletteMathquill, j3pStyle } from 'src/legacy/core/functions'
import { j3pCreeSegment } from 'src/legacy/core/functionsSvg'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import $ from 'jquery'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'

/**
 * Récupère le contenu des zones de saisies déjà remplies (fonction utilisée dans construireArbreCond)
 * @private
 * @param {Array} [tabDiv] tableau de toutes les zones de saisies construites dans l’arbre
 * @param {Boolean} [afficheEvt] booléen permettant de savoir si les evts sont déjà complétés ou non
 * @return {Object} propriétés tabEvts et tabPrbs qui nous donnent le tableau respectivement des zones de saisie pour les événements et pour les proba
 * propriété
 */
function recupereDonnees (tabDiv, afficheEvt) {
  // cette fonction est appelée lors de l’utilisation des boutons +/-
  const tabEvts = []
  const tabPrbs = []
  if (afficheEvt) {
    // les événements sont affichés donc on on ne récupère que les données des zones de saisies correspondant à des proba
    for (let i = 0; i < tabDiv.length; i++) {
      tabPrbs.push($(tabDiv[i]).mathquill('latex'))
    }
  } else {
    // les zones en position paire sont des événements et celles en zone impaire sont des proba
    for (let i = 0; i < tabDiv.length; i += 2) {
      tabEvts.push($(tabDiv[i]).mathquill('latex'))
      tabPrbs.push($(tabDiv[i + 1]).mathquill('latex'))
    }
  }
  return { tabEvts, tabPrbs }
}

/**
 * Construit un arbre de probabilité (avec éventuellement des zones de saisie pour les événements et les proba)
 * @private
 * @param {HTMLElement|string} [parent] div conteneur de l’arbre
 * @param {Object} options objet contenant les données de l’arbre
 * @param {Parcours} options.parcours objet contenant les données de l’arbre
 * @param {Object} [options.lestyle] couleur et style de texte
 * @param {Object} [options.lestyle.styletexte] Par exemple me.styles.petit.enonce
 * @param {Object} [options.lestyle.couleur] utiles pour la couleur des branches. Par exemple me.styles.petit.enonce.color
 * @param {Object} [options.complet] permet de savoir ce qu’on mets dans l’arbre (et donc ce qui sera à saisir)
 * @param {Boolean} [options.complet.evts] true si les événements sont affichés (sinon ce seront des zones de saisie)
 * @param {Boolean} [options.complet.proba] true si les probabilités sont affichées (sinon ce seront des zones de saisie)
 * @param {String} [options.lstRestriction] caractères utiles pour les événements lorsque options.complet.evts vaut true
 * @param {string} [options.prbRestriction] caractères utiles pour les probas (par défaut ce sera '\d,./')
 * @param {Array<*>} [options.evts1] tableau de tableaux de deux valeurs (la première est l’événement, la deuxième la proba)
 * @param {Object} [options.evts2] options.evts2['branche'+i] contiendra un tableau de tableaux de deux éléments (toujours le couple evt,proba)
 * @param {Object} [options.prerempli] contient les propriétés evts1 et evts2 correspondant à ce qui aurait déjà été complété (cela permet le rappel de cette fonction avec des zones de saisies déjà complétées)
 * @return {undefined} les propriétés fctsValid, meszonesSaisie et listeBtns seront ajoutées à l’élément parent pour récupérer les informations dans la section
 */
export function construireArbreCond (parent, { parcours, lestyle, complet, lstRestriction, prbRestriction, evts1, evts2, prerempli }) {
  function ecoute (mondiv) {
    // div est le div conteneur contenant la zone palette
    if (mondiv.className.includes('mq-editable-field')) {
      j3pEmpty(parent.laPalette)
      // on vérifie si ce sont des événements ou des proba
      if (mondiv.typeZone.includes('evt')) {
        j3pPaletteMathquill(parent.laPalette, mondiv.id, {
          liste: ['barre', 'indice']
        })
      } else {
        j3pPaletteMathquill(parent.laPalette, mondiv.id, { liste: ['fraction'] })
      }
    }
  }
  const parentDiv = (typeof parent === 'string') ? j3pElement(parent) : parent
  if (!prbRestriction) prbRestriction = '\\d,./'
  // au départ l’arbre ne contiendra que 2*une branche
  /// je tente de gérer la hauteur de mon arbre
  try {
    // dans parent, je vais créer un nouveau div dans lequel sera créé l’arbre
    // pour une nouvelle création de l’arbre, il faudra penser à vider le précédent div ce que je fais ici
    j3pEmpty(parentDiv)
    if (parent.laPalette) j3pEmpty(parent.laPalette)
  } catch (e) {
    console.warn(e) // pas normal mais pas grave
  }
  const div = j3pAddElt(parentDiv, 'div', '', { position: 'absolute' })
  let numevt, numprb, largPrb, largEvt, largPrb2, largEvt2, posVerticale, posVerticale2
  const largeurBranche = 140
  const largeurEvt = 20
  const hauteurBranche = 50 // hauteur verticale entre deux branches successives (juste l’espace pour y mettre les evts)
  const nbBrancheEvts1 = (complet.evts)
    ? evts1.length
    : (prerempli) ? prerempli.evts1.length : 1
  const donneesSaisies = { evts1: [], evts2: {} }
  // Quand on utilise les boutons +/- pour ajouter des branches, il faut récupérer le contenu des zones déjà remplies
  // donneesSaisies est un objet qui le permettra. Au début, il ne contient rien (sauf dans le cas d’un arbre où les evts seront complétés - mais cela sera fait plus loin)
  let nbBrancheEvts2 = 0
  if (!prerempli) prerempli = { evts1: [], evts2: { branche0: [['', '']] } }
  for (let i = 0; i < nbBrancheEvts1; i++) {
    donneesSaisies.evts1.push((prerempli.evts1[i]) ? [...prerempli.evts1[i]] : ['', ''])
    if (complet.evts) {
      if (complet.proba) {
        donneesSaisies.evts2['branche' + i] = [...evts2['branche' + i]]
      } else {
        donneesSaisies.evts2['branche' + i] = []
        evts2['branche' + i].forEach(tab => {
          donneesSaisies.evts2['branche' + i].push([tab[0], ''])
        })
      }
    } else {
      donneesSaisies.evts2['branche' + i] = (prerempli.evts2['branche' + i]) ? [...prerempli.evts2['branche' + i]] : [['', '']]
    }
    nbBrancheEvts2 = Math.max(nbBrancheEvts2, donneesSaisies.evts2['branche' + i].length) // on récupère ainsi le nombre max de branches partant d’un noeud (qui n’est pas la racine)
  }
  parent.mesZonesSaisie = []
  parent.listeBtns = []
  const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
  svg.setAttribute('width', (largeurBranche + largeurEvt) * 2 + 20)
  svg.setAttribute('height', nbBrancheEvts1 * nbBrancheEvts2 * hauteurBranche + 40)
  div.appendChild(svg)
  // il faut que je positionne verticalement la racine (horizontalement, c’est 0)
  const hauteurTotale = nbBrancheEvts1 * nbBrancheEvts2 * hauteurBranche
  const decalVert = hauteurTotale / 2 + 10
  numevt = numprb = 1
  const posNoeudVert = []
  let maxLargEvt = 0
  const nomZonesTxt = [] // utile pour les mettre ensuite en couleur
  let objDonnees = {} // cet objet va me permettre de récupérer les évts et les prbs déjà remplies
  let laProba, listeSignes
  for (let i = 0; i < nbBrancheEvts1; i++) {
    if (nbBrancheEvts1 % 2 === 0) {
      posVerticale = (i < nbBrancheEvts1 / 2) ? decalVert - (nbBrancheEvts1 / 2 - i - 0.5) * nbBrancheEvts2 * hauteurBranche : decalVert + (i - nbBrancheEvts1 / 2 + 0.5) * nbBrancheEvts2 * hauteurBranche
    } else {
      posVerticale = (i < (nbBrancheEvts1 - 1) / 2) ? decalVert - (nbBrancheEvts1 / 2 - i) * nbBrancheEvts2 * hauteurBranche / 2 - hauteurBranche + 10 : (Math.abs(i - (nbBrancheEvts1 - 1) / 2) < Math.pow(10, -12)) ? decalVert : decalVert + (i - (nbBrancheEvts1 - 1) / 2 + 0.5) * nbBrancheEvts2 * hauteurBranche / 2 + hauteurBranche
    }
    posNoeudVert.push(posVerticale)
    j3pCreeSegment(svg, {
      x1: 0,
      y1: decalVert,
      x2: largeurBranche,
      y2: posVerticale,
      couleur: lestyle.couleur,
      epaisseur: 1
    })
    const zoneEvt1 = j3pAddElt(div, 'div', '', { style: lestyle.styletexte })
    j3pStyle(zoneEvt1, { position: 'absolute' })
    if (complet.evts) {
      // là j'écris l’événements
      j3pAffiche(zoneEvt1, '', '$' + evts1[numevt - 1][0] + '$')
      nomZonesTxt.push(zoneEvt1)
    } else {
      // ici c’est une zone de saisie
      const elt = j3pAffiche(zoneEvt1, '', '&1&', { inputmq1: { texte: donneesSaisies.evts1[i][0] } })
      mqRestriction(elt.inputmqList[0], lstRestriction, { commandes: ['barre', 'indice'] })
      parent.mesZonesSaisie.push(elt.inputmqList[0])
      elt.inputmqList[0].typeZone = 'evt1'
    }
    // je positionne le nom de l’événement ou la zone de saisie
    largEvt = zoneEvt1.getBoundingClientRect().width
    maxLargEvt = Math.max(maxLargEvt, largEvt)
    zoneEvt1.style.top = (posVerticale - 3) + 'px'
    zoneEvt1.style.left = (largeurBranche + 12) + 'px'
    numevt++
    const zonePrb1 = j3pAddElt(div, 'div', '', { style: lestyle.styletexte })
    j3pStyle(zonePrb1, { backgroundColor: '#f6fafe', position: 'absolute' })

    if (complet.proba) {
      // j'écris les différentes probas sur les branches. Dans ce cas, il me faut un fond blanc pour qu’on voit la proba au-dessus des branches
      j3pAffiche(zonePrb1, '', '$' + evts1[numprb - 1][1] + '$')
      nomZonesTxt.push(zonePrb1)
    } else {
      // ici c’est une zone de saisie
      const elt = j3pAffiche(zonePrb1, '', '&1&', {
        inputmq1: { texte: donneesSaisies.evts1[i][1] }
      })
      mqRestriction(elt.inputmqList[0], prbRestriction, { commandes: ['fraction'] })
      elt.inputmqList[0].typeZone = 'prb1'
      parent.mesZonesSaisie.push(elt.inputmqList[0])
      if (complet.evts) {
        // j’ai écrit tous les evts, donc je connais la réponse de chaque zone
        laProba = j3pCalculValeur(evts1[numprb - 1][1])
        elt.inputmqList[0].reponse = [laProba]
        elt.inputmqList[0].typeReponse = ['nombre', 'exact']
      }
    }
    // j'écris les probabilité dans une taille plus petites
    j3pStyle(zonePrb1.childNodes[0], { fontSize: '0.9em' })
    largPrb = zonePrb1.getBoundingClientRect().width
    // je positionne la proba ou la zone de saisie
    zonePrb1.style.top = Math.floor(decalVert + 2 * (posVerticale - decalVert) / 3) + 'px'
    zonePrb1.style.left = Math.floor(2 * largeurBranche / 3 - largPrb / 2) + 'px'
    numprb++
  }
  if (!complet.evts) {
    // j’ajoute aussi le bouton +/-
    const btnPlusMoins = j3pAddElt(div, 'div', { style: lestyle.styletexte })
    j3pStyle(btnPlusMoins, { position: 'absolute' })
    parent.listeBtns.push(btnPlusMoins)
    const evts1BtnPlus = j3pGetNewId('evts1BtnPlus')
    const evts1BtnMoins = j3pGetNewId('evts1BtnMoins')
    j3pAjouteBouton(btnPlusMoins, evts1BtnPlus, 'MepBoutons2', '+', '')
    j3pAjouteBouton(btnPlusMoins, evts1BtnMoins, 'MepBoutons2', '-', '')
    // pour utiliser les boutons +/- d’une taille acceptable, je modifie les éléments css de la classe MepBoutons2 présente dans j3pboutons.css (ligne 879)
    listeSignes = [evts1BtnPlus, evts1BtnMoins]
    listeSignes.forEach(elt => j3pStyle(elt, { fontSize: '12px', padding: '1px', display: 'block', width: '12px' }))
    // il faut que je gère l’action sur ce bouton, la fonction de j3pAjouteBouton n’ayant pas de paramètre, je ne peux pas gérer
    $('#' + evts1BtnPlus).click(function () {
      // quand je clique sur le bouton - des première branches, j’ajoute
      // -un branche en-dessous
      // -une branche à droite
      if (nbBrancheEvts1 < 4) {
        objDonnees = recupereDonnees(parent.mesZonesSaisie, complet.evts)
        let posBranche2 = 0
        for (let k = 0; k < prerempli.evts1.length; k++) {
          donneesSaisies.evts1[k][0] = objDonnees.tabEvts[k]
          donneesSaisies.evts1[k][1] = objDonnees.tabPrbs[k]
          for (let k2 = 0; k2 < donneesSaisies.evts2['branche' + k].length; k2++) {
            donneesSaisies.evts2['branche' + k][k2][0] = objDonnees.tabEvts[donneesSaisies.evts1.length + posBranche2]
            donneesSaisies.evts2['branche' + k][k2][1] = objDonnees.tabPrbs[donneesSaisies.evts1.length + posBranche2]
            posBranche2++
          }
        }
        // je lui rajoute alors un élément vide
        donneesSaisies.evts1.push(['', ''])
        donneesSaisies.evts2['branche' + (donneesSaisies.evts1.length - 1)] = [['', '']]
        prerempli = Object.assign(donneesSaisies)
        // il ne me reste plus qu'à recréer l’arbre
        construireArbreCond(parentDiv, { parcours, lestyle, complet, lstRestriction, prbRestriction, evts1, evts2, prerempli })
      }
    })
    $('#' + evts1BtnMoins).click(function () {
      // quand je clique sur le bouton - des premières branches, j’enlève
      // -la dernière branche
      // -toutes les branches qui étaient attachées à celle-ci
      if (nbBrancheEvts1 > 1) {
        objDonnees = recupereDonnees(parent.mesZonesSaisie, complet.evts)
        let posBranche2 = 0
        for (let k = 0; k < donneesSaisies.evts1.length - 1; k++) {
          donneesSaisies.evts1[k][0] = objDonnees.tabEvts[k]
          donneesSaisies.evts1[k][1] = objDonnees.tabPrbs[k]
          for (let k2 = 0; k2 < donneesSaisies.evts2['branche' + k].length; k2++) {
            donneesSaisies.evts2['branche' + k][k2][0] = objDonnees.tabEvts[donneesSaisies.evts1.length + posBranche2]
            donneesSaisies.evts2['branche' + k][k2][1] = objDonnees.tabPrbs[donneesSaisies.evts1.length + posBranche2]
            posBranche2++
          }
        }
        // j’enlève alors le dernier élément'
        donneesSaisies.evts1.pop()
        delete donneesSaisies.evts2['branche' + (donneesSaisies.evts1.length)]
        // il ne me reste plus qu'à recréer l’arbre
        prerempli = Object.assign(donneesSaisies)
        construireArbreCond(parentDiv, { parcours, lestyle, complet, lstRestriction, prbRestriction, evts1, evts2, prerempli })
      }
    })
    btnPlusMoins.style.top = (decalVert - 7) + 'px'
    btnPlusMoins.style.left = '10px'
  }
  const decalageNoeud = 10 + maxLargEvt + 15
  for (let i = 0; i < nbBrancheEvts1; i++) {
    // maintenant je m’occupe des secondes branches
    // la position x du noeud est maxLargEvt+largeurBranche+4
    let maxLargEvt2 = 0
    for (let j = 0; j < donneesSaisies.evts2['branche' + i].length; j++) {
      posVerticale2 = (donneesSaisies.evts2['branche' + i].length === 1) ? posNoeudVert[i] : (donneesSaisies.evts2['branche' + i].length % 2 === 0) ? posNoeudVert[i] - (donneesSaisies.evts2['branche' + i].length / 2 - 0.5) * hauteurBranche + j * hauteurBranche : posNoeudVert[i] - Math.floor(donneesSaisies.evts2['branche' + i].length / 2 + 0.5) * hauteurBranche / 2 + j * hauteurBranche
      j3pCreeSegment(svg, {
        x1: largeurBranche + decalageNoeud,
        y1: posNoeudVert[i],
        x2: 2 * largeurBranche + maxLargEvt + 8,
        y2: posVerticale2,
        couleur: lestyle.couleur,
        epaisseur: 1
      })
      const zoneEvt2 = j3pAddElt(div, 'div', '', { style: lestyle.styletexte })
      j3pStyle(zoneEvt2, { position: 'absolute' })
      if (complet.evts) {
        // là j'écris l’événements
        j3pAffiche(zoneEvt2, '', '$' + evts2['branche' + i][j][0] + '$')
        nomZonesTxt.push(zoneEvt2)
      } else {
        const elt = j3pAffiche(zoneEvt2, '', '&1&', {
          inputmq1: { texte: donneesSaisies.evts2['branche' + i][j][0] }
        })
        mqRestriction(elt.inputmqList[0], lstRestriction, {
          commandes: ['barre', 'indice']
        })
        parent.mesZonesSaisie.push(elt.inputmqList[0])
        elt.inputmqList[0].typeZone = 'evt2'
        elt.inputmqList[0].branchePrecedente = i
      }
      // je positionne le nom de l’événement
      largEvt2 = zoneEvt2.getBoundingClientRect().width
      maxLargEvt2 = Math.max(maxLargEvt2, largEvt2)
      zoneEvt2.style.top = (posVerticale2 - 3) + 'px'
      zoneEvt2.style.left = (2 * largeurBranche + decalageNoeud + 10) + 'px'
      const zonePrb2 = j3pAddElt(div, 'div', '', { style: lestyle.styletexte })
      j3pStyle(zonePrb2, { backgroundColor: '#f6fafe', position: 'absolute' })
      if (complet.proba) {
        // j'écris les différentes probas sur les branches. Dans ce cas, il me faut un fond blanc pour qu’on voit la proba au-dessus des branches
        j3pAffiche(zonePrb2, '', '$' + evts2['branche' + i][j][1] + '$')
        nomZonesTxt.push(zonePrb2)
      } else {
        // ici c’est une zone de saisie
        const elt = j3pAffiche(zonePrb2, '', '&1&', {
          inputmq1: { texte: donneesSaisies.evts2['branche' + i][j][1] }
        })
        mqRestriction(elt.inputmqList[0], prbRestriction, { commandes: ['fraction'] })
        parent.mesZonesSaisie.push(elt.inputmqList[0])
        elt.inputmqList[0].typeZone = 'prb2'
        if (complet.evts) {
          // j’ai écrit tous les evts, donc je connais la réponse de chaque zone
          laProba = j3pCalculValeur(evts2['branche' + i][j][1])
          elt.inputmqList[0].reponse = [laProba]
          elt.inputmqList[0].typeReponse = ['nombre', 'exact']
        }
      }
      j3pStyle(zonePrb2.childNodes[0], { fontSize: '0.9em' })
      // je positionne la proba
      largPrb2 = zonePrb2.getBoundingClientRect().width
      zonePrb2.style.top = Math.floor(posNoeudVert[i] + 4 * (posVerticale2 - posNoeudVert[i]) / 5 - 2) + 'px'
      zonePrb2.style.left = Math.floor(largeurBranche + decalageNoeud + 5 * largeurBranche / 7 - largPrb2) + 'px'
    }
    if (!complet.evts) {
      // j’ajoute aussi le bouton +/-
      const btnPlusMoins = j3pAddElt(div, 'div', { style: lestyle.styletexte })
      j3pStyle(btnPlusMoins, { position: 'absolute' })
      parent.listeBtns.push(btnPlusMoins)
      const evt2BtnPlus = j3pGetNewId('evts2BtnPlus' + i)
      const evt2BtnMoins = j3pGetNewId('evt2BtnMoins' + i)
      j3pAjouteBouton(btnPlusMoins, evt2BtnPlus, 'MepBoutons2', '+', '')
      j3pAjouteBouton(btnPlusMoins, evt2BtnMoins, 'MepBoutons2', '-', '')
      // j’identifie le noeud associé à ce bouton
      // pour utiliser les boutons +/- d’une taille acceptable, je modifie les éléments css de la classe MepBoutons2 présente dans j3pboutons.css (ligne 879)
      listeSignes = [evt2BtnPlus, evt2BtnMoins]
      listeSignes.forEach(elt => {
        j3pStyle(elt, { fontSize: '12px', padding: '1px', display: 'block', width: '12px' })
        j3pElement(elt).numNoeud = i
      })
      btnPlusMoins.style.top = (posNoeudVert[i] - 7) + 'px'
      btnPlusMoins.style.left = (largeurBranche + decalageNoeud + 10) + 'px'
      // il faut que je gère l’action sur ce bouton, la fonction de j3pAjouteBouton n’ayant pas de paramètre, je ne peux pas gérer
      $('#' + evt2BtnPlus).click(function () {
        // quand je clique sur le bouton - des secondes branches, j’ajoute
        // -une branche en-dessous
        // -j’ajuste l’arbre en le recréant
        if (prerempli.evts2['branche' + j3pElement(this.id).numNoeud].length < 4) {
          objDonnees = recupereDonnees(parent.mesZonesSaisie, complet.evts)
          let posBranche2 = 0
          for (let k = 0; k < donneesSaisies.evts1.length; k++) {
            donneesSaisies.evts1[k][0] = objDonnees.tabEvts[k]
            donneesSaisies.evts1[k][1] = objDonnees.tabPrbs[k]
            for (let k2 = 0; k2 < donneesSaisies.evts2['branche' + k].length; k2++) {
              donneesSaisies.evts2['branche' + k][k2][0] = objDonnees.tabEvts[donneesSaisies.evts1.length + posBranche2]
              donneesSaisies.evts2['branche' + k][k2][1] = objDonnees.tabPrbs[donneesSaisies.evts1.length + posBranche2]
              posBranche2++
            }
          }
          // je ne touche pas à evts1
          // j’identifie le noeud auquel ajouter une branche
          donneesSaisies.evts2['branche' + (j3pElement(this.id).numNoeud)].push(['', ''])
          // il ne me reste plus qu'à recréer l’arbre
          prerempli = Object.assign(donneesSaisies)
          construireArbreCond(parentDiv, { parcours, lestyle, complet, lstRestriction, prbRestriction, evts1, evts2, prerempli })
        }
      })
      $('#' + evt2BtnMoins).click(function () {
        // quand je clique sur le bouton - des secondes branches, j’ajoute
        // -une branche en-dessous
        // -j’ajuste l’arbre en le recréant
        if (prerempli.evts2['branche' + j3pElement(this.id).numNoeud].length > 1) {
          objDonnees = recupereDonnees(parent.mesZonesSaisie, complet.evts)
          let posBranche2 = 0
          for (let k = 0; k < donneesSaisies.evts1.length; k++) {
            donneesSaisies.evts1[k][0] = objDonnees.tabEvts[k]
            donneesSaisies.evts1[k][1] = objDonnees.tabPrbs[k]
            for (let k2 = 0; k2 < donneesSaisies.evts2['branche' + k].length; k2++) {
              donneesSaisies.evts2['branche' + k][k2][0] = objDonnees.tabEvts[donneesSaisies.evts1.length + posBranche2]
              donneesSaisies.evts2['branche' + k][k2][1] = objDonnees.tabPrbs[donneesSaisies.evts1.length + posBranche2]
              posBranche2++
            }
          }
          // je ne touche pas à evts1
          // j’identifie le noeud auquel ajouter une branche
          donneesSaisies.evts2['branche' + (j3pElement(this.id).numNoeud)].pop()
          // il ne me reste plus qu'à recréer l’arbre
          prerempli = Object.assign(donneesSaisies)
          construireArbreCond(parentDiv, { parcours, lestyle, complet, lstRestriction, prbRestriction, evts1, evts2, prerempli })
        }
      })
    }
  }
  if (!complet.proba) {
    const zones = parent.mesZonesSaisie
    parent.fctsValid = complet.evts
      ? new ValidationZones({ zones, parcours })
      : new ValidationZones({ zones, validePerso: zones, parcours })
  }
  if (!complet.evts || !complet.proba) {
    parent.laPalette = j3pAddElt(div, 'div', '', { style: { position: 'absolute', top: (decalVert + 100) + 'px', left: '10px' } })
    parent.mesZonesSaisie.forEach(eltInput => {
      $(eltInput).focusin(function () { ecoute(this) })
    })
    j3pFocus(parent.mesZonesSaisie[0])
  }
  if (complet.proba) {
    // c’est que l’arbre est complet
    for (let i = 0; i < nomZonesTxt.length; i++) {
      j3pStyle(nomZonesTxt[i], { color: lestyle.couleur })
    }
  }
} // construireArbreCond

/**
 * egaliteTableauPrb permet de vérifier si deux tableaux contenant des couples de la forme Evt/Prb sont équivalents
 * @private
 * @param {Array} [tab1] tableau contenant des tableaux de couples de la forme Evt/Prb
 * @param {Array} [tab2] tableau contenant des tableaux de couples de la forme Evt/Prb
 * @return {Object} propriétés egalite qui est un booléen et positionTab qui est la position de chaque éléments de tab1[i] dans tab2
 * propriété
 */
export function egaliteTableauPrb (tab1, tab2) {
  const tabPos = []
  let egalite = (tab1.length === tab2.length)
  const tab2Copie = [...tab2]
  for (let i = 0; i < tab2Copie.length; i++) {
    tab2Copie[i].push(i) // j’ajoute la position des éléments de tab2
  }
  if (egalite) {
    for (let i = 0; i < tab1.length; i++) {
      // je regarde si tab1[i] est dans tab2
      let j = 0
      let present = false
      while (j < tab2Copie.length) {
        if ((tab1[i][0] === tab2Copie[j][0]) && (Math.abs(j3pCalculValeur(tab1[i][1]) - j3pCalculValeur(tab2Copie[j][1])) < Math.pow(10, -12))) {
          present = true
          tabPos[i] = tab2Copie[j][2]
          tab2Copie.splice(j, 1)
          j = tab2Copie.length
        } else {
          j++
        }
      }
      egalite = (egalite && present)
    }
  }
  return { egalite, positionTab: tabPos }
} // fin egaliteTableauPrb

/**
 * bonEvts permet de vérifier si les événements saisis sur les branches sont bien présents dans l’énoncé
 * @private
 * @param {Array} [tab1] tableau de couples de la forme (evt/prb) : ces couples correspondent à ce qui se trouve sur la branche d’un arbre
 * @param {Array} [listeEvts] tableau des événements présents dans l’énoncé
 * @return {Boolean} précise si les événements sont bien dans l’arbre (\overline{\overline{A}} étant considéré comme égal à A)
 */
export function bonEvts (tab1, listeEvts) {
  // tab est la liste des branches
  // listeEvts est la liste des événements de l’exo
  function enleveBarre (evt) {
    // si evt est de la forme \\overline{}, on enlève \\overline
    let monEvt = (evt.includes('overline')) ? evt.replace('\\overline', '') : evt
    // il reste encore les accolades éventuelles
    if ((monEvt.charAt(0) === '{') && (monEvt.charAt(monEvt.length - 1) === '}')) {
      monEvt = monEvt.substring(1, monEvt.length - 1)
    }
    return monEvt
  }

  let evtPresent = true
  const newList = []
  let evt
  for (let j = 0; j < tab1.length; j++) {
    evt = tab1[j][0]
    while (evt.includes('overline')) {
      evt = enleveBarre(evt)
    }
    newList.push(evt)
  }
  for (let i = 0; i < tab1.length; i++) {
    evtPresent = (evtPresent && (listeEvts.includes(newList[i])))
  }
  return evtPresent
} // bonEvts

/**
 * simplifieEvt simplifie les événement écris sous la forme du contraire du contraire
 * @private
 * @param {String} [evtTxt] un événement saisi dans un zone
 * @return {String} redonne cet événement de manière simplifiée
 * propriété
 */
export function simplifieEvt (evtTxt) {
  // pour des esprits un peu tordus, je dois aussi vérifier si l’événement n’est pas de la forme \\overline{\\overline{...}}
  let monEvt = evtTxt
  while ((monEvt.indexOf('\\overline{\\overline{') === 0) && (monEvt.lastIndexOf('}}') === monEvt.length - 2)) {
    monEvt = monEvt.substring(20, monEvt.length - 2)
  }
  return monEvt
}

/**
 * reecrireEvt réécrit l’événement pour enlever ce qui serait en trop et plutôt issu d’une faute de frappe
 * @private
 * @param {String} [evt] un événement saisi dans un zone
 * @return {String} redonne cet événement en supprimant ce qui est inutile (permettant une comparaison avec d’autres evts)
 * propriété
 */
export function reecrireEvt (evt) {
  // evt est un string correspondant à un événement
  // on corrige d’éventuelles fautes de frappes (espace en trop, \overline{} en trop)
  let newEvt = evt
  while (newEvt.includes(' ')) {
    newEvt = newEvt.replace(' ', '')
  }
  while (newEvt.includes('\\overline{}')) {
    newEvt = newEvt.replace('\\overline{}', '')
  }
  return newEvt
}
