import { j3pAjouteArea, j3pAjouteBouton, j3pDiv, j3pElement } from 'src/legacy/core/functions'

/* global Basthon pyodide shell editor graphics MathJax notie */

// ---------------------------------------------------------------------------
// initialisation composants
// ---------------------------------------------------------------------------

function initComposants (me) {
  j3pDiv(me.zones.MD, {
    id: 'div_enonce',
    contenu: '',
    // coord:[10,10],
    style: { border: '1px solid #000', background: '#f0f0f0', padding: '5px' }
    // style:me.styles.etendre('petit.enonce',{border:"1px solid #000",width:"95%",height:"70px","background":"#f0f0f0",padding:"5px"})
  })
  j3pDiv(me.zones.MD, {
    id: 'explications',
    contenu: '',
    // coord:[50,200],
    style: me.styles.petit.explications
  })
  j3pDiv(me.zones.MD, {
    id: 'correction',
    contenu: '',
    // coord:[20,100],
    style: me.styles.petit.correction
  })
  j3pDiv(me.zones.MG, { id: 'left-div', contenu: '' })
  j3pDiv('left-div', { id: 'left-div-btns', contenu: '' })
  j3pAjouteBouton('left-div-btns', 'run', '', 'run', '')
  j3pAjouteBouton('left-div-btns', 'open', '', 'open', '')
  j3pElement('open').style.display = 'none'
  j3pAjouteBouton('left-div-btns', 'download', '', 'load', '')
  j3pElement('download').style.display = 'none'
  j3pAjouteBouton('left-div-btns', 'share', '', 'share', '')
  j3pElement('share').style.display = 'none'
  j3pAjouteBouton('left-div-btns', 'hide-editor', '', 'hide-ed', '')
  j3pElement('hide-editor').style.display = 'none'
  j3pAjouteBouton('left-div-btns', 'bouton1', '', 'test', 'zzzztest()')
  j3pDiv('left-div', { id: 'editor', contenu: '' })
  j3pDiv(me.zones.MG, { id: 'right-div', contenu: '' })
  j3pDiv('right-div', { id: 'right-div-btns', contenu: '' })
  j3pAjouteBouton('right-div-btns', 'raz', '', 'raz', '')
  j3pAjouteBouton('right-div-btns', 'btn_shell', '', 'shell', '')
  j3pAjouteBouton('right-div-btns', 'btn_graph_view', '', 'graph', '')
  j3pAjouteBouton('right-div-btns', 'darklight', '', 'dark', '')
  j3pElement('darklight').style.display = 'none'
  j3pAjouteBouton('right-div-btns', 'switch', '', 'switch', '')
  j3pElement('switch').style.display = 'none'
  j3pAjouteBouton('right-div-btns', 'hide-console', '', 'hide_co', '')
  j3pAjouteBouton('right-div-btns', 'show-editor-console', '', 'show', '')
  j3pElement('show-editor-console').style.display = 'none'
  j3pDiv('right-div', { id: 'graphics', contenu: '' })
  j3pAjouteArea('right-div', { id: 'shell', contenu: '' })
  j3pDiv('right-div', {
    id: 'div_output',
    contenu: '',
    // coord: [250, 10],
    style: 'color:blue'
  })
  j3pElement('div_output').innerHTML = "<pre id='output' style='color:black'>aaa</pre>"
  j3pDiv(me.zones.MG, { id: 'footer', contenu: '' })
  j3pDiv(me.zones.MG, { id: 'share_message', contenu: '' })
  myshell()
  myeditor()
  mygraphics()
  mygui()
  j3pElement('left-div').style.width = '50%'
  j3pElement('editor').style.height = '300px'
  window.editor.resize()
  j3pElement('right-div').style.width = '48%'
  j3pElement('shell').style.width = '100%'
}

function println (texte) {
  const txt = (typeof texte !== 'undefined') ? texte : ''
  const mypre = document.getElementById('output')
  mypre.innerHTML = mypre.innerHTML + txt + '\n'
}

function effacerOutput () {
  const mypre = document.getElementById('output')
  while (mypre.lastChild) mypre.removeChild(mypre.lastChild)
}

// ---------------------------------------------------------------------------
// Exécution et vérification de code
// ---------------------------------------------------------------------------

function run (code) {
  pyodide.runPython(code)
}

function verif (algo_eleve, algo_secret, entrees, sortie) {
  let lignes
  let ligne
  let diagnostique
  // on ajoute les entrees à tester
  let debut_test
  debut_test = ''
  for (let k = 0; k < Object.keys(entrees).length; k++) {
    const cle = Object.keys(entrees)[k]
    const valeur = Object.values(entrees)[k]
    debut_test = debut_test + cle + ' = ' + valeur + '\n'
  }
  // console.log('debut_test', debut_test)
  // on ote les saisies de l’algo eleve
  lignes = algo_eleve.split('\n')
  let algo_eleve_test = debut_test
  for (let i = 0; i < (lignes.length); i++) {
    ligne = lignes[i]
    if (ligne == null) continue
    if (ligne.indexOf('input(') >= 0) continue
    algo_eleve_test = algo_eleve_test + ligne + '\n'
  }
  // console.log('algo_eleve apres', algo_eleve)
  // on ote les saisies de l’algo secret
  lignes = algo_secret.split('\n')
  let algo_secret_test = debut_test
  for (let i = 0; i < (lignes.length); i++) {
    ligne = lignes[i]
    if (ligne == null) continue
    if (ligne.indexOf('input(') >= 0) continue
    algo_secret_test = algo_secret_test + ligne + '\n'
  }
  // on exécute algo_secret_test
  run(algo_secret_test)
  const sortie_secret = pyodide.globals[sortie]
  pyodide.globals[sortie] = null
  // console.log('sortie_secret=', sortie_secret)
  // on exécute algo_eleve_test
  try {
    run(algo_eleve_test)
  } catch (err) {
    diagnostique = 'Ton programme a bogué pour'
    for (let k = 0; k < Object.keys(entrees).length; k++) {
      const cle = Object.keys(entrees)[k]
      const valeur = Object.values(entrees)[k]
      diagnostique = diagnostique + ' ' + cle + '=' + valeur
    }
    const txt_err = '' + err
    const i_err = txt_err.lastIndexOf(' line ')
    if (i_err >= 0) {
      diagnostique += '\n>>>' + txt_err.substring(i_err)
    }
    j3pElement('output').innerHTML = diagnostique
    return false
  }
  const sortie_eleve = pyodide.globals[sortie]
  // console.log('sortie_eleve=', sortie_eleve)
  // on compare les 2 exécutions
  const identique = (sortie_secret == sortie_eleve)
  if (identique) {
    j3pElement('output').innerHTML = ''
    return true
  }
  // on explique l’échec
  diagnostique = 'Résultat faux pour'
  for (let k = 0; k < Object.keys(entrees).length; k++) {
    const cle = Object.keys(entrees)[k]
    const valeur = Object.values(entrees)[k]
    diagnostique = diagnostique + ' ' + cle + '=' + valeur
  }
  diagnostique = diagnostique + '\n' + 'ta réponse : ' + sortie_eleve
  diagnostique = diagnostique + '\n' + 'la bonne réponse : ' + sortie_secret
  j3pElement('output').innerHTML = diagnostique
  return false
}

// ---------------------------------------------------------------------------
// On exporte tout ça pour notre outil j3pSkulptPython
// Rq : à ne pas confondre avec algo_python.js (dans l’outil algo)
// ---------------------------------------------------------------------------

window.j3pBasthonPython = {
  initComposants,
  effacerOutput,
  println,
  run,
  verif
}

function myshell () {
  window.shell = (function () {
    const that = document.getElementById('shell')
    that.promptPrefix = '>>> '
    that.blockPrefix = '... '
    /**
       * Initialize the shell.
       */
    that.init = async function () {
      that.value = 'Python ' + Basthon.pythonVersion + '\n'
      that.value += 'Type "help", "copyright", "credits" or "license" for more information.\n'
      that.value += that.promptPrefix
      that.history = []
      that.historyPosition = 0
      that.focus()
      that.cursorToEnd()
    }
    /**
     * Updating history with piece of code.
     */
    that.pushToHistory = function (src) {
      src.split('\n').forEach(function (line) {
        if (line.length > 0) {
          const lastHistoryLine = that.history[that.history.length - 1]
          if (line !== lastHistoryLine) { that.history.push(line) }
        }
      })
      that.historyPosition = that.history.length
    }
    /**
       * Current line hidden by history look-up.
       */
    that.lineHiddenByHistory = ''
    /**
       * Get string just after last prompt.
       */
    that.lastLine = function () {
      const src = that.value
      return src.slice(src.lastIndexOf('\n') + that.promptPrefix.length + 1)
    }
    /**
       * Display current history on last line.
       */
    that.displayCurrentHistory = function () {
      let toAdd
      if (that.historyPosition >= that.history.length) {
        toAdd = that.lineHiddenByHistory
      } else {
        toAdd = that.history[that.historyPosition]
      }
      const src = that.value
      that.value = src.slice(0, src.lastIndexOf('\n') + that.promptPrefix.length + 1) + toAdd
      that.cursorToEnd()
    }
    /**
       * Go backward in history.
       */
    that.backwardHistory = function () {
      if (that.historyPosition === that.history.length) {
        that.lineHiddenByHistory = that.lastLine()
      }
      that.historyPosition = Math.max(0, that.historyPosition - 1)
      that.displayCurrentHistory()
    }
    /**
       * Go forward in history.
       */
    that.forwardHistory = function () {
      if (that.historyPosition < that.history.length) {
        that.historyPosition++
      }
      that.displayCurrentHistory()
    }
    /**
       * New line with prompt.
       */
    that.newline = function () {
      that.value += that.promptPrefix
      that.cursorToEnd()
    }
    /**
       * Append piece of text to the shell.
       */
    that.append = function (text) {
      that.value += text
      that.cursorToEnd()
    }
    /**
       * Put the shell cursor to the end.
       */
    that.cursorToEnd = function () {
      // cursor to end
      that.cursorGoTo(that.value.length)
      that.scrollTop = that.scrollHeight
    }
    /**
       * Cursor position.
       */
    that.cursorPos = function () {
      return that.selectionStart
    }
    /**
       * Set cursor to position.
       */
    that.cursorGoTo = function (pos) {
      that.setSelectionRange(pos, pos)
    }
    /**
       * Is cursor on last line?
       */
    that.cursorOnLastLine = function () {
      return that.value.lastIndexOf('\n') < that.cursorPos()
    }
    /**
       * Is cursor after last prompt?
       */
    that.cursorAfterLastPrompt = function () {
      return that.cursorOnLastLine() && that.cursorColumn() >= that.promptPrefix.length
    }
    /**
       * Is cursor just after last prompt?
       */
    that.cursorJustAfterLastPrompt = function () {
      return that.cursorOnLastLine() && that.cursorColumn() === that.promptPrefix.length
    }
    /**
       * Is cursor right after last prompt?
       */
    that.cursorRightAfterLastPrompt = function () {
      return that.cursorOnLastLine() && that.cursorColumn() > that.promptPrefix.length
    }
    /**
       * Get the curent column of the cursor.
       */
    that.cursorColumn = function () {
      const pos = that.cursorPos()
      const before = that.value.slice(0, pos)
      return pos - before.split('\n').slice(0, -1).join('\n').length - 1
    }
    /**
       * Launch code in shell.
       */
    that.launch = function (code, interactive = true) {
      // updating history
      that.pushToHistory(code)
      // ready for computing
      Basthon.dispatchEvent('eval.request', {
        code,
        interactive
      })
    }
    /**
       * Callback for keypress event.
       */
    that.keypress = function (event) {
      if (!that.cursorAfterLastPrompt()) {
        event.preventDefault()
        return
      }
      switch (event.keyCode) {
        case 13: // return
          if (event.shiftKey) {
            that.append('\n' + that.blockPrefix)
          } else {
            let src = that.value
            // last line starting by prompt
            const start = src.lastIndexOf(that.promptPrefix)
            src = src.substr(start + that.promptPrefix.length)
            // removing newlines + ...
            src = src.split('\n... ').join('\n')
            that.append('\n')
            that.launch(src, true)
          }
          that.cursorToEnd()
          event.preventDefault()
          break
      }
    }
    /**
       * Callback for keydown event.
       */
    that.keydown = function (event) {
      switch (event.keyCode) {
        case 9: // tab
          that.append('    ')
          event.preventDefault()
          break
        case 37: // left arrow
          if (that.cursorJustAfterLastPrompt()) {
            event.preventDefault()
            event.stopPropagation()
          }
          break
        case 36: // line start
          if (that.cursorAfterLastPrompt()) {
            that.cursorGoTo(that.cursorPos() - that.cursorColumn() + that.promptPrefix.length)
            event.preventDefault()
            event.stopPropagation()
          }
          break
        case 38: // up
          if (that.cursorAfterLastPrompt()) {
            that.backwardHistory()
            event.preventDefault()
          }
          break
        case 40: // down
          if (that.cursorAfterLastPrompt()) {
            that.forwardHistory()
            event.preventDefault()
          }
          break
        case 8: // backspace
          if (!(that.cursorRightAfterLastPrompt() ||
              (that.cursorJustAfterLastPrompt() &&
                that.selectionStart < that.selectionEnd))) {
            event.preventDefault()
            event.stopPropagation()
          }
          break
        case 65: { // a (for ctrl+a)
          if (!event.ctrlKey) { break }
          const src = that.value
          const start = that.cursorPos() - that.cursorColumn() + that.promptPrefix.length
          that.setSelectionRange(start, src.length)
          event.preventDefault()
          break
        }
        case 33: // page up
          event.preventDefault()
          break
        case 34: // page down
          event.preventDefault()
          break
      }
    }
    /* pluging callbacks to events */
    that.addEventListener('keypress', that.keypress)
    that.addEventListener('keydown', that.keydown)
    return that
  })()
}

function myeditor () {
  window.editor = (function () {
    const that = window.ace.edit('editor')
    /**
       * Initialize the editor’s content.
       */
    that.loadScript = async function () {
      /* set script from:
           -> query string if submited
           -> local storage if available
           -> default otherwise
        */
      const url = new URL(window.location.href)
      const script_key = 'script'
      const from_key = 'from'
      if (url.searchParams.has(script_key)) {
        const script = url.searchParams.get(script_key)
        that.setValue(decodeURIComponent(script))
      } else if (url.searchParams.has(from_key)) {
        let fileURL = url.searchParams.get(from_key)
        fileURL = decodeURIComponent(fileURL)
        let script
        try {
          script = await Basthon.xhr({
            url: fileURL,
            method: 'GET'
          })
        } catch {
          throw Error('Le chargement du script ' + fileURL + ' a échoué.')
        }
        if (script) {
          that.setValue(script)
        }
      } else if ((typeof (localStorage) !== 'undefined') && 'py_src' in localStorage) {
        that.setValue(localStorage.py_src)
      } else {
        that.setValue('for i in range(10):\n\tprint(i)')
      }
      that.scrollToRow(0)
      that.gotoLine(0)
    }
    /**
     * Initialize the Ace editor.
     */
    that.init = async function () {
      that.session.setMode('ace/mode/python')
      that.focus()
      that.setOptions({
        enableLiveAutocompletion: true,
        highlightActiveLine: false,
        highlightSelectedWord: true,
        fontSize: '12pt'
      })
      // modif patrick
      // await that.loadScript();
    }
    /**
       * Downloading editor content as filename.
       */
    that.download = function (filename = 'script.py') {
      let code = that.getValue()
      code = code.replace(/\r\n|\r|\n/g, '\r\n') // To retain the Line breaks.
      const blob = new Blob([code], { type: 'text/plain' })
      const anchor = document.createElement('a')
      anchor.download = filename
      anchor.href = window.URL.createObjectURL(blob)
      anchor.target = '_blank'
      anchor.style.display = 'none' // just to be safe!
      document.body.appendChild(anchor)
      anchor.click()
      document.body.removeChild(anchor)
    }
    /**
       * Opening file (async) and loading it in the editor.
       */
    that.open = function (file) {
      return new Promise(function (resolve, reject) {
        const reader = new FileReader()
        reader.readAsText(file)
        reader.onload = function (event) {
          that.setValue(event.target.result)
          that.scrollToRow(0)
          that.gotoLine(0)
          resolve()
        }
        reader.onerror = reject
      })
    }
    /**
       * Returning the sharing link for the code in the editor.
       */
    that.sharingURL = function () {
      // to retain the Line breaks.
      const code = that.getValue().replace(/\r\n|\r|\n/g, '\r\n')
      const url = new URL(window.location.href)
      url.hash = ''
      url.searchParams.delete('from') // take care of collapsing params
      const script = encodeURIComponent(code).replace(/\(/g, '%28').replace(/\)/g, '%29')
      url.searchParams.set('script', script)
      return url.href
    }
    /**
       * Backup to local storage.
       */
    that.backup = async function () {
      if (typeof (localStorage) !== 'undefined') {
        localStorage.py_src = that.getValue()
      }
    }
    return that
  })()
}

function mygraphics () {
  window.graphics = (function () {
    const that = document.getElementById('graphics')
    /**
       * Cleaning graphics.
       */
    that.clean = function () {
      // textContent seems faster than innerHTML...
      that.textContent = ''
    }
    /**
       * Display an element from Basthon data.
       */
    that.display = function (data) {
      that.clean()
      switch (data.display_type) {
        case 'p5': {
          const root = data.content
          root.style.width = '100%'
          root.style.height = '100%'
          root.style.display = 'flex'
          root.style.justifyContent = 'center'
          root.style.alignItems = 'center'

          function autoFit (node) {
            node.style.width = 'auto'
            node.style.height = 'auto'
            node.style.maxWidth = '100%'
            node.style.maxHeight = '100%'
          }

          // some canvas nodes can be added later so we observe...
          const observer = new MutationObserver(
            function (mutationsList, observer) {
              for (const mutation of mutationsList) {
                for (const node of mutation.addedNodes) {
                  if (['canvas', 'video'].includes(
                    node.tagName.toLowerCase())) { autoFit(node) }
                }
              }
            })
          observer.observe(root, { childList: true })
          root.querySelectorAll('canvas,video').forEach(autoFit)
          that.appendChild(root)
          break
        }

        case 'matplotlib': {
          let root = data.content
          const canvas = root.querySelector('canvas')
          if (canvas) root = canvas
          root.style.width = ''
          root.style.height = ''
          root.style.maxWidth = '100%'
          root.style.maxHeight = '100%'
          that.appendChild(root)
          break
        }
        case 'turtle':
          // Turtle result
          window.setTimeout(function () {
            that.innerHTML = data.content.outerHTML
          }, 1)
          break
        case 'sympy':
          that.innerHTML = data.content
          if (typeof (MathJax) === 'undefined') {
            // dynamically loading MathJax
            // console.log('Loading MathJax (Sympy expression needs it).');
            (function () {
              const script = document.createElement('script')
              script.type = 'text/javascript'
              script.src = 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/MathJax.js?config=TeX-MML-AM_CHTML'
              document.getElementsByTagName('head')[0].appendChild(script)
            })()
          } else {
            // otherwise, render it
            MathJax.Hub.Queue(['Typeset', MathJax.Hub, that])
          }
          break
        case 'html':
          that.innerHTML = data.content
          break
        case 'multiple':
          /* typically dispached by display() */
          for (const mime of ['image/svg+xml',
            'image/png',
            'text/html',
            'text/plain']) {
            if (mime in data.content) {
              let content = data.content[mime]
              if (mime === 'image/png') {
                content = '<img src="data:image/png;base64,' + content + '" style="max-width: 100%; max-height: 100%;">'
              }
              that.innerHTML = content
              break
            }
          }
          break
        default:
          console.error('Not supported node type in eval.display result processing.')
      }
    }
    return that
  })()
}

function mygui () {
  window.gui = (function () {
    const that = {}
    /**
       * The error notification system.
       */
    that.notifyError = function (error) {
      that._console_error(error)
      const message = error.message || error.reason.message || error
      notie.alert({
        type: 'error',
        text: 'Erreur : ' + message,
        stay: false,
        time: 3,
        position: 'top'
      })
      // In case of error, force loader hiding.
      try {
        Basthon.Goodies.hideLoader()
      } catch {}
    }
    /**
       * Initialize the GUI.
       */
    that.init = async function () {
      /* all errors redirected to notification system */
      const onerror = that.notifyError
      window.addEventListener('error', onerror)
      window.addEventListener('unhandledrejection', onerror)
      /* console errors redirected to notification system */
      that._console_error = console.error
      console.error = function () {
        onerror({ message: String(arguments[0]) })
      }
      /* state variables */
      that.initStateFromStorage('switched', false)
      that.initStateFromStorage('darkmode', true)
      /* update appearance from state variables */
      that.updateSwitchView()
      that.updateDarkLight()
      /* connecting buttons */
      // run
      let button = document.getElementById('run')
      button.addEventListener('click', that.runScript)
      // open
      button = document.getElementById('open')
      button.addEventListener('click', that.openFile)
      // download
      button = document.getElementById('download')
      button.addEventListener('click', that.download)
      // share
      button = document.getElementById('share')
      button.addEventListener('click', that.share)
      // raz
      button = document.getElementById('raz')
      button.addEventListener('click', that.restart)
      // show shell
      button = document.getElementById('btn_shell')
      button.addEventListener('click', that.showShell)
      // show graph
      button = document.getElementById('btn_graph_view')
      button.addEventListener('click', that.showGraph)
      // dark/light mode
      button = document.getElementById('darklight')
      button.addEventListener('click', that.switchDarkLight)
      // view switch
      button = document.getElementById('switch')
      button.addEventListener('click', that.switchView)
      // single/double vew
      button = document.getElementById('hide-console')
      button.addEventListener('click', that.hideConsole)
      button = document.getElementById('hide-editor')
      button.addEventListener('click', that.hideEditor)
      button = document.getElementById('show-editor-console')
      button.addEventListener('click', that.showEditorConsole)
      // initialise Basthon
      await that.basthonInit()
    }
    /**
       * Change loader text and call init function.
       * In case of error, we continue the init process,
       * trying to do our best...
       */
    that.initCaller = async function (func, message) {
      Basthon.Goodies.setLoaderText(message)
      try {
        return await func()
      } catch (error) { that.notifyError(error) }
    }
    /**
       * Loading Basthon and connecting to events.
       */
    that.basthonInit = async function () {
      // loading Basthon (errors are fatal)
      await Basthon.Goodies.showLoader('Chargement de Basthon...', false)
      const init = that.initCaller
      // loading shell
      await init(shell.init, 'Chargement de la console...')
      // loading editor
      await init(editor.init, 'Chargement de l’éditeur...')
      // loading aux files from URL
      await init(that.loadURLAux, 'Chargement des fichiers auxiliaires...')
      // loading modules from URL
      await init(that.loadURLModules, 'Chargement des modules annexes...')
      // end
      Basthon.Goodies.hideLoader()
      /* backup before closing */
      window.onbeforeunload = function () {
        editor.backup().catch(that.notifyError)
      }
      /* Event connections */
      Basthon.addEventListener('eval.finished', function (data) {
        if (data.interactive && 'result' in data) {
          shell.append(data.result['text/plain'] + '\n')
        }
        shell.newline()
      })
      Basthon.addEventListener('eval.output', function (data) {
        shell.append(data.content)
      })
      Basthon.addEventListener('eval.error', function (data) {
        shell.newline()
      })
      Basthon.addEventListener('eval.display', function (data) {
        graphics.display(data)
      })
    }
    /**
       * Load ressources from URL (common part to files and modules).
       */
    that._loadFromURL = async function (key, put) {
      const url = new URL(window.location.href)
      const promises = []
      for (let fileURL of url.searchParams.getAll(key)) {
        fileURL = decodeURIComponent(fileURL)
        const filename = fileURL.split('/').pop()
        let promise = Basthon.xhr({
          method: 'GET',
          url: fileURL,
          responseType: 'arraybuffer'
        })
        promise = promise.then(function (data) {
          return put(filename, data)
        }).catch(function (error) {
          console.error(error)
          throw Error('Impossible de charger le fichier ' + filename + '.')
        })
        promises.push(promise)
      }
      await Promise.all(promises)
    }
    /**
       * Load auxiliary files submited via URL (aux= parameter) (async).
       */
    that.loadURLAux = function () {
      return that._loadFromURL('aux', Basthon.putFile)
    }
    /**
       * Load modules submited via URL (module= parameter) (async).
       */
    that.loadURLModules = function () {
      return that._loadFromURL('module', Basthon.putModule)
    }
    /**
       * Init a state variable from local storage (with default value).
       */
    that.initStateFromStorage = function (state, _default = false) {
      if (typeof (Storage) !== 'undefined' &&
          localStorage.getItem(state) !== null) {
        that[state] = localStorage.getItem(state) === 'true'
      } else {
        that[state] = _default
      }
    }
    /**
       * Save a state variable in local storage.
       */
    that.saveStateInStorage = function (state) {
      if (typeof (Storage) !== 'undefined') {
        localStorage.setItem(state, that[state])
      }
    }
    /**
       * Run the editor script in the shell.
       */
    that.runScript = function () {
      // backup the script just before running
      editor.backup().catch(that.notifyError)
      const src = editor.getValue()
      shell.append(src.split('\n').join('\n... ') + '\n')
      shell.launch(src, false)
      shell.focus()
      shell.cursorToEnd()
    }
    /**
       * RAZ function.
       */
    that.restart = function () {
      Basthon.restart()
      shell.init()
      graphics.clean()
    }
    /**
       * Loading file in the (emulated) local filesystem (async).
       */
    that.putFSRessource = function (file) {
      return new Promise(function (resolve, reject) {
        const reader = new FileReader()
        reader.readAsArrayBuffer(file)
        reader.onload = async function (event) {
          await Basthon.putRessource(file.name, event.target.result)
          notie.alert({
            type: 'success',
            text: file.name + ' est maintenant utilisable depuis Python',
            stay: false,
            time: 3,
            position: 'top'
          })
          resolve()
        }
        reader.onerror = reject
      })
    }
    /**
       * Open file in editor.
       */
    that.openEditor = async function (file) {
      await editor.open(file)
      notie.alert({
        type: 'success',
        text: file.name + ' est chargé dans l’éditeur',
        stay: false,
        time: 3,
        position: 'top'
      })
    }
    /**
       * Open *.py file by asking user what to do:
       * load in editor or put on (emulated) local filesystem.
       */
    that.openPythonFile = async function (file) {
      notie.confirm({
        text: 'Que faire de ' + file.name + ' ?',
        submitText: 'Charger dans l’éditeur',
        cancelText: 'Installer le module',
        position: 'top',
        submitCallback: () => { that.openEditor(file) },
        cancelCallback: () => { that.putFSRessource(file) }
      })
    }
    /**
       * Opening file: If it has .py extension, loading it in the editor
       * or put on (emulated) local filesystem (user is asked to),
       * otherwise, loading it in the local filesystem.
       */
    that.openFile = function () {
      return new Promise(function (resolve, reject) {
        const input = document.createElement('input')
        input.type = 'file'
        input.style.display = 'none'
        input.onchange = async function (event) {
          for (const file of event.target.files) {
            const ext = file.name.split('.').pop()
            if (ext === 'py') {
              await that.openPythonFile(file)
            } else {
              await that.putFSRessource(file)
            }
          }
          resolve()
        }
        input.onerror = reject

        document.body.appendChild(input)
        input.click()
        document.body.removeChild(input)
      })
    }
    /**
       * Download script in editor.
       */
    that.download = function () {
      editor.download('script.py')
    }
    /**
       * Displaying editor alone.
       */
    that.hideConsole = function () {
      let button = document.getElementById('hide-console')
      button.style.display = 'none'
      button = document.getElementById('hide-editor')
      button.style.display = ''
      let elt = document.getElementById('right-div')
      elt.style.display = 'none'
      elt = document.getElementById('left-div')
      elt.style.display = ''
      elt.style.width = '100%'
    }
    /**
       * Displaying console alone.
       */
    that.hideEditor = function () {
      let button = document.getElementById('hide-editor')
      button.style.display = 'none'
      button = document.getElementById('show-editor-console')
      button.style.display = ''
      let elt = document.getElementById('left-div')
      elt.style.display = 'none'
      elt = document.getElementById('right-div')
      elt.style.display = ''
      elt.style.width = '100%'
    }
    /**
       * Editor and console side by side.
       */
    that.showEditorConsole = function () {
      let button = document.getElementById('show-editor-console')
      button.style.display = 'none'
      button = document.getElementById('hide-console')
      button.style.display = ''
      let elt = document.getElementById('left-div')
      elt.style.display = ''
      elt.style.width = '50%'
      elt = document.getElementById('right-div')
      elt.style.display = ''
      elt.style.width = '48%'
    }
    /**
       * Update switch view.
       */
    that.updateSwitchView = function () {
      const div_left = document.getElementById('left-div')
      const div_right = document.getElementById('right-div')
      if (that.switched) {
        div_left.style.cssFloat = 'right'
        div_right.style.cssFloat = 'left'
      } else {
        div_left.style.cssFloat = 'left'
        div_right.style.cssFloat = 'right'
      }
    }
    /**
       * Switching side view.
       */
    that.switchView = function () {
      that.switched = !that.switched
      that.updateSwitchView()
      that.saveStateInStorage('switched')
    }
    /**
       * Update dark/light appearence.
       */
    that.updateDarkLight = function () {
      function removeAddClass (element, toRemove, toAdd) {
        element.classList.add(toAdd)
        element.classList.remove(toRemove)
      }

      function changeMode (from, to) {
        /* array copy to bypass HTMLCollection dynamic update */
        const elements = Array.prototype.slice.call(
          document.getElementsByClassName(from))
        for (const e of elements) removeAddClass(e, from, to)
      }

      if (that.darkmode) {
        changeMode('light', 'dark')
        editor.setTheme('ace/theme/monokai')
      } else {
        changeMode('dark', 'light')
        editor.setTheme('ace/theme/xcode')
      }
    }
    /**
       * Switch dark/light mode.
       */
    that.switchDarkLight = function () {
      that.darkmode = !that.darkmode
      that.saveStateInStorage('darkmode')
      that.updateDarkLight()
    }
    /**
       * Toggle hide/show shell/graph.
       */
    that.showShell = function () {
      shell.style.display = 'block'
      graphics.style.display = 'none'
      shell.cursorToEnd()
    }
    /**
       * Toggle hide/show shell/graph.
       */
    that.showGraph = function () {
      shell.style.display = 'none'
      graphics.style.display = 'flex'
    }
    /**
       * Share script via URL.
       */
    that.share = function () {
      const url = editor.sharingURL()
      notie.confirm({
        text: document.getElementById('share_message').innerHTML,
        submitText: 'Copier dans le presse-papier',
        cancelText: 'Tester le lien',
        position: 'top',
        submitCallback: function () { that.copyToClipboard(url) },
        cancelCallback: function () {
          const anchor = document.createElement('a')
          anchor.href = url
          anchor.target = '_blank'
          anchor.style.display = 'none' // just to be safe!
          document.body.appendChild(anchor)
          anchor.click()
          document.body.removeChild(anchor)
        }
      })
    }
    /**
       * Copy a text to clipboard.
       */
    that.copyToClipboard = function (text) {
      const textArea = document.createElement('textarea')
      // Precautions from https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
      // Place in top-left corner of screen regardless of scroll position.
      textArea.style.position = 'fixed'
      textArea.style.top = 0
      textArea.style.left = 0
      // Ensure it has a small width and height. Setting to 1px / 1em
      // doesn’t work as this gives a negative w/h on some browsers.
      textArea.style.width = '2em'
      textArea.style.height = '2em'
      // We don’t need padding, reducing the size if it does flash render.
      textArea.style.padding = 0
      // Clean up any borders.
      textArea.style.border = 'none'
      textArea.style.outline = 'none'
      textArea.style.boxShadow = 'none'
      // Avoid flash of white box if rendered for any reason.
      textArea.style.background = 'transparent'
      textArea.value = text
      document.body.appendChild(textArea)
      textArea.focus()
      textArea.select()
      /* debug * /
      try {
        const successful = document.execCommand('copy')
        const msg = successful ? 'successful' : 'unsuccessful'
        console.log('Copying text command was ' + msg)
      } catch (error) {
        console.error('Oops, unable to copy', error)
      } /**/
      document.body.removeChild(textArea)
    }
    return that
  })()
  window.gui.init()
}
