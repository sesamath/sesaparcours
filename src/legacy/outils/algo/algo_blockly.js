import $ from 'jquery'
import { j3pElement, j3pShowError } from 'src/legacy/core/functions'
import Blockly from 'src/lib/outils/blockly/original'
import './css_accessible'

/**
 * Vide #output
 * @private
 */
function effacerOutput () {
  const mypre = document.getElementById('output')
  while (mypre.lastChild) mypre.removeChild(mypre.lastChild)
}

// charge un xml pour le menu blockly, utilisé par testblockly
function loadMenus (menu_url, algo_url) {
  $.ajax({
    type: 'GET',
    url: menu_url, // "outils/algo/menus_j3p.xml" par exemple
    dataType: 'xml',
    success: function (xml) {
      // création de l’espace de travail (en particulier du menu)
      Blockly.inject('div_blockly', {
        // media: "../keys_new/media/",
        zoom: {
          controls: true,
          wheel: true,
          startScale: 1,
          maxScale: 2,
          minScale: 0.5,
          scaleSpeed: 1.1
        },
        toolbox: $(xml).find('#toolbox')[0] // telechargement du menu
      })
      // parametrages divers des fenêtres de SofusPy
      // document.getElementById("div_blockly").style.height=(window.innerHeight-100) + "px";
      document.getElementById('div_blockly').style.height = '450px'
      document.getElementById('div_blockly').style.width = '700px'
      Blockly.svgResize(Blockly.mainWorkspace)
      if (algo_url) loadSample(algo_url) // "sections/ex_blockly_casquettes7.xml" par exemple
      // loadSample("outils/algo/exemple_blockly.xml");
      // loadSample("sections/ex_blockly_casquettes7.xml");
    }
  })
}

function loadSample (monUrl) {
  // $(divsPython).hide();
  // $(divsBlockly).show();
  return $.ajax({
    type: 'GET',
    url: monUrl, // "outils/algo/exemple_blockly.xml" par exemple
    dataType: 'xml',
    success: function (xml) {
      let x = xml.firstChild
      while (x.nodeType !== 1) {
        x = x.nextSibling
      }
      /*
        for (var key in xml) {
          var value = xml;
          println(key + "=" + value);
        }
        */
      Blockly.getMainWorkspace().clear()
      return Blockly.Xml.domToWorkspace(x, Blockly.getMainWorkspace())
      // return Blockly.Xml.domToWorkspace($(xml).find("#" + name)[0], Blockly.getMainWorkspace());
    }
  })
}

function run () {
  effacerOutput()
  // Generate JavaScript code and run it.
  window.LoopTrap = 1000
  Blockly.JavaScript.INFINITE_LOOP_TRAP =
      'if (--window.LoopTrap == 0) throw "Infinite loop.";\n'
  const code = Blockly.JavaScript.workspaceToCode(Blockly.getMainWorkspace())
  try {
    // eslint-disable-next-line no-eval
    eval(code.replace(/window\.alert\(/g, 'println('))
  } catch (e) {
    console.error(e)
    println(e)
  }
}

function getCodeJS () {
  let code = Blockly.JavaScript.workspaceToCode(Blockly.getMainWorkspace())
  try {
    code = code.replace(/window\.alert\(/g, 'println(')
    return code
  } catch (e) {
    console.error(e)
    return e
  }
}

function println (texte) {
  const txt = (typeof texte !== 'undefined') ? texte : ''
  const mypre = document.getElementById('output')
  mypre.innerHTML = mypre.innerHTML + txt + '\n'
}

// ---------------------------------------------------------------------------
// Vérification de code
// ---------------------------------------------------------------------------

function verif (algo_eleve, algo_secret, entrees, type_sortie) {
  let lignes, ligne, i, k, code, cle, valeur
  // on ajoute les entrees à tester
  let debut_test = ''
  for (k = 0; k < Object.keys(entrees).length; k++) {
    cle = Object.keys(entrees)[k]
    valeur = Object.values(entrees)[k]
    debut_test = debut_test + cle + ' = ' + valeur + '\n'
  }
  // on ote les saisies de l’algo eleve
  lignes = algo_eleve.split('\n')
  let algo_eleve_test = debut_test
  for (i = 0; i < (lignes.length); i++) {
    ligne = lignes[i]
    if (ligne == null) continue
    if (ligne.indexOf('window.prompt(') >= 0) continue
    algo_eleve_test = algo_eleve_test + ligne + '\n'
  }
  // on ote les saisies de l’algo secret
  lignes = algo_secret.split('\n')
  let algo_secret_test = debut_test
  for (i = 0; i < (lignes.length); i++) {
    ligne = lignes[i]
    if (ligne == null) continue
    if (ligne.indexOf('window.prompt(') >= 0) continue
    algo_secret_test = algo_secret_test + ligne + '\n'
  }
  // on exécute algo_secret_test
  j3pElement('output').innerHTML = ''
  try {
    code = algo_secret_test.replace(/window\.alert\(/g, 'println(')
    // eslint-disable-next-line no-eval
    eval(code)
  } catch (e) {
    console.error(e)
    j3pShowError(e.message)
  }
  let sortie_secret = j3pElement('output').innerHTML
  // on exécute algo_eleve_test
  j3pElement('output').innerHTML = ''
  try {
    code = algo_eleve_test.replace(/window\.alert\(/g, 'println(')
    // eslint-disable-next-line no-eval
    eval(code)
  } catch (e) {
    console.error(e)
    j3pShowError(e.message)
  }
  let sortie_eleve = j3pElement('output').innerHTML
  // on regarde si le test élève s’exécute
  let diagnostique
  if (sortie_eleve.indexOf('Error') !== -1) {
    diagnostique = 'Ton programme a bogué pour'
    for (k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      diagnostique = diagnostique + ' ' + cle + '=' + valeur
    }
    j3pElement('output').innerHTML = diagnostique + '\n' + j3pElement('output').innerHTML
    return false
  }
  // on compare les 2 exécutions
  sortie_secret = sortie_secret.split('\n').join('')
  sortie_secret = sortie_secret.split('<br>').join('')
  sortie_eleve = sortie_eleve.split('\n').join('')
  sortie_eleve = sortie_eleve.split('<br>').join('')
  const identique = (sortie_secret == sortie_eleve)
  if (identique) {
    j3pElement('output').innerHTML = ''
    return true
  }
  // on explique l’échec
  diagnostique = 'Résultat faux pour'
  for (k = 0; k < Object.keys(entrees).length; k++) {
    cle = Object.keys(entrees)[k]
    valeur = Object.values(entrees)[k]
    diagnostique = diagnostique + ' ' + cle + '=' + valeur
  }
  diagnostique = diagnostique + '\n' + 'ta réponse : ' + sortie_eleve
  diagnostique = diagnostique + '\n' + 'la bonne réponse : ' + sortie_secret
  j3pElement('output').innerHTML = diagnostique
  return false
}

// et on exporte tout ça dans un espace de nom pour notre outil
export const algoBlockly = {
  getCodeJS,
  loadMenus,
  loadSample,
  println,
  run,
  verif
}
