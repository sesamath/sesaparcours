import { afficher, filmDessinSetElt, papGetIndex } from './functions'

const w = window

function onMouseMove (event) {
  const style = getComputedStyle(this)
  const posX = event.clientX - (this.offsetLeft + this.parentNode.offsetLeft + parseInt(style.marginLeft))
  const posY = event.clientY + this.scrollTop - (this.offsetTop + this.parentNode.offsetTop + parseInt(style.marginTop)) + this.parentNode.scrollTop
  const x = this.js.xMin + posX * this.js.Dx / this.js.larg
  const y = -(this.js.yMin + posY * this.js.Dy / this.js.haut)
  document.getElementById('ALafficheCoord').innerHTML = 'x=' + Number(x).toFixed(2) + '<br>y=' + Number(y).toFixed(2)
}

function onMouseOut () {
  document.getElementById('ALafficheCoord').innerHTML = ''
}

export default function ALrepere () {
  // calcul du pas de la grille
  function calculePas (pas) {
    pas = pas / 12.5
    const a = w.log(pas)
    const b = a - Math.floor(a)
    const dixN = Math.pow(10, a - b)
    if (b < w.log(2)) {
      return dixN
    }
    if (b < w.log(4)) {
      return 2 * dixN
    }
    if (b < w.log(8)) {
      return 5 * dixN
    }
    return 10 * dixN
  }

  this.xMin = Number(w.ALfenetre.xMin)
  this.yMin = Number(w.ALfenetre.yMin)
  this.xMax = Number(w.ALfenetre.xMax)
  this.yMax = Number(w.ALfenetre.yMax)
  this.Dx = this.xMax - this.xMin
  this.Dy = this.yMax - this.yMin
  const c = document.createElement('canvas')
  if (!c.getContext) {
    afficher('désolé, votre navigateur ne sait pas dessiner !')
    return
  }
  c.addEventListener('mousemove', onMouseMove, false)
  c.addEventListener('mouseout', onMouseOut, false)
  // référence à cet objet
  c.js = this
  const conteneur = document.getElementById('ALdroite')
  this.larg = Math.min(0.75 * conteneur.offsetWidth, 0.75 * conteneur.offsetHeight)
  this.haut = this.larg
  c.setAttribute('width', this.larg)
  c.setAttribute('height', this.haut)
  conteneur.appendChild(c)
  this.ctx = c.getContext('2d')
  this.stroke(w.ALfenetre.stroke, true)
  if (w.ALfenetre.axes) {
    this.ctx.beginPath()
    this.ctx.moveTo(this.x(0), this.y(this.yMin))
    this.ctx.lineTo(this.x(0), this.y(this.yMax))
    this.ctx.moveTo(this.x(this.xMin), this.y(0))
    this.ctx.lineTo(this.x(this.xMax), this.y(0))
    this.ctx.stroke()
    this.xPas = calculePas(this.Dx)
    this.yPas = calculePas(this.Dy)
    this.ctx.beginPath()
    this.ctx.strokeStyle = 'red'
    this.ctx.lineWidth = 2
    this.ctx.moveTo(this.x(0), this.y(0))
    this.ctx.lineTo(this.x(this.xPas), this.y(0))
    this.ctx.moveTo(this.x(0), this.y(0))
    this.ctx.lineTo(this.x(0), this.y(this.yPas))
    this.ctx.stroke()
    this.ctx.strokeStyle = 'black'
    this.ctx.lineWidth = 1
    afficher('Les segments rouges sur les axes donnent l’unité')
  }
  c.scrollIntoView(false)
}

ALrepere.prototype.stroke = function (couleur, horsAlgo) {
  const couleurs = {
    noir: 'black',
    noire: 'black',
    gris: 'gray',
    grise: 'gray',
    rouge: 'red',
    jaune: 'gold',
    bleu: 'blue',
    bleue: 'blue',
    orange: 'orange',
    violet: 'purple',
    violette: 'purple',
    vert: 'green',
    verte: 'green',
    marron: 'maroon',
    marine: 'navy',
    brun: 'brown',
    brune: 'brown'
  }
  // on accepte fr ou en, black si inconnu
  const c = Object.keys(couleurs).includes(couleur)
    ? couleurs[couleur]
    : Object.values(couleurs).includes(couleur)
      ? couleur
      : 'black'
  if (papGetIndex() <= 0 && !horsAlgo) filmDessinSetElt([1, c], -1)
  this.ctx.strokeStyle = couleurs[c]
  this.ctx.fillStyle = couleurs[c]
}

ALrepere.prototype.x = function (abscisse) {
  return this.larg * (abscisse - this.xMin) / this.Dx
}

ALrepere.prototype.y = function (ord) {
  return this.haut - this.haut * (ord - this.yMin) / this.Dy
}

ALrepere.prototype.segment = function (x1, y1, x2, y2) {
  if (papGetIndex() <= 0) {
    filmDessinSetElt([2, x1, y1, x2, y2], -1)
  }
  this.ctx.beginPath()
  this.ctx.moveTo(this.x(x1), this.y(y1))
  this.ctx.lineTo(this.x(x2), this.y(y2))
  this.ctx.stroke()
}

ALrepere.prototype.cercle = function (x, y, r) {
  if (papGetIndex() <= 0) {
    filmDessinSetElt([3, x, y, r], -1)
  }
  this.ctx.beginPath()
  r = r * this.larg / this.Dx
  this.ctx.arc(this.x(x), this.y(y), r, 0, Math.PI * 2, true)
  this.ctx.stroke()
}

ALrepere.prototype.rect = function (xm, ym, xM, yM, plein) {
  if (papGetIndex() <= 0) {
    filmDessinSetElt([4, xm, ym, xM, yM], -1)
  }
  xm = this.x(xm)
  ym = this.y(ym)
  xM = this.x(xM)
  yM = this.y(yM)
  const Xm = Math.min(xm, xM)
  const XM = Math.max(xm, xM)
  const Ym = Math.min(ym, yM)
  const YM = Math.max(ym, yM)
  const larg = XM - Xm
  const haut = YM - Ym
  if (plein) { this.ctx.fillRect(Xm, Ym, larg, haut) } else { this.ctx.strokeRect(Xm, Ym, larg, haut) }
}

ALrepere.prototype.rectPlein = function (xm, ym, xM, yM) {
  this.rect(xm, ym, xM, yM, true)
}

ALrepere.prototype.rectCreux = function (xm, ym, xM, yM) {
  this.rect(xm, ym, xM, yM, false)
}

ALrepere.prototype.point = function (x, y) {
  if (papGetIndex() <= 0) {
    filmDessinSetElt([5, x, y], -1)
  }
  this.ctx.fillRect(this.x(x), this.y(y), 3, 3)
}
