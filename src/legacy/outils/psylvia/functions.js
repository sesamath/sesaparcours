const w = window

export function afficher () {
  const idLieu = 'ALdroite'
  const lieu = document.getElementById(idLieu)
  const p = document.createElement('p')
  let texte = ''
  for (let i = 0; i < arguments.length; i++) {
    texte += alToString(arguments[i]) + ' '
  }
  const text = document.createTextNode(texte)
  p.appendChild(text)
  lieu.appendChild(p)
  p.scrollIntoView(false)
}

// film de l’exécution du programme utilisateur
let indexPap = -1

export function papReset () {
  indexPap = -1
}
export function papInc () {
  indexPap++
}
export function papGetIndex () {
  return indexPap
}

function arGetElt (ar, index) {
  if (!Number.isInteger(index)) throw Error('index invalide')
  if (index < 0) return ar[ar.length + index]
  return ar[index]
}
function arSetElt (ar, elt, index) {
  if (Number.isInteger(index)) {
    // on tolère index = length, qui revient à faire du push (index = -length remplace le 1er elt)
    if (Math.abs(index) > ar.length) throw RangeError('index hors du tableau')
    if (index < 0) ar[ar.length + index] = elt
    else ar[index] = elt
  } else {
    ar.push(elt)
  }
}
function arReset (ar) {
  while (ar.length) ar.pop()
}

const filmDessin = []
/**
 * Retourne un item de filmDessin
 * @param {number} index index, négatif pour partir de la fin
 * @return {*|Array<*>}
 */
export function filmDessinGetElt (index) {
  return arGetElt(filmDessin, index)
}
/**
 * Ajoute ou remplace un élément de filmDessin
 * @param {number[]} elt
 * @param {number} [index] si fourni ça remplace, sinon ça ajoute
 */
export function filmDessinSetElt (elt, index) {
  arSetElt(filmDessin, elt, index)
}
/**
 * Efface le film
 */
export function filmDessinEfface () {
  arReset(filmDessin)
}

const filmTortue = []
/**
 * Retourne un item de filmTortue
 * @param {number} index index, négatif pour partir de la fin
 * @return {*|Array<*>}
 */
export function filmTortueGetElt (index) {
  return arGetElt(filmTortue, index)
}
/**
 * Ajoute ou remplace un élément de filmTortue
 * @param {number[]} elt
 * @param {number} [index] si fourni ça remplace, sinon ça ajoute
 */
export function filmTortueSetElt (elt, index) {
  arSetElt(filmTortue, elt, index)
}
/**
 * Efface le film
 */
export function filmTortueEfface () {
  arReset(filmTortue)
}

let freqTortue = 25
export function getFreqTortue () {
  return freqTortue
}
export function setFreqTortue (freq) {
  if (!Number.isFinite(freq) || freq < 1) throw Error('fréquence invalide')
  freqTortue = freq
}

function alToString (objet) {
  if (objet == '') return ''
  if (typeof (objet) !== 'object' || objet.length) {
    let alGlob
    try {
      // eslint-disable-next-line no-eval
      alGlob = eval(objet)
    } catch (err) {
      console.error(err)
      alGlob = objet
      if (alGlob.charAt) {
        alGlob = alGlob.replace(/Math.sqrt/g, 'racine')
        alGlob = alGlob.replace(/&&/g, 'et')
        alGlob = alGlob.replace(/||/g, 'ou')
        alGlob = alGlob.replace(/==/g, '=')
        alGlob = alGlob.replace(/<=/g, '≤')
        alGlob = alGlob.replace(/>=/, '≥')
        alGlob = alGlob.replace(/!=/, '≠')
      }
    }
    if (!isNaN(parseFloat(alGlob))) {
      if (alGlob.length) {
        for (let i = 0; i < alGlob.length; i++) {
          if (alGlob[i] != Math.floor(alGlob[i])) {
            alGlob[i] = alGlob[i].toFixed(w.ALprecision)
          }
        }
      } else {
        if (alGlob != Math.floor(alGlob)) {
          return alGlob.toFixed(w.ALprecision)
        }
      }
    }
    return alGlob
  }
  let s = ''
  for (const x in objet) {
    s += ', ' + x + ' : ' + alToString(objet[x])
  }
  return '{ ' + s.substr(1) + ' }'
}
