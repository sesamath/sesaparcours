import { afficher, filmTortueSetElt, getFreqTortue, papGetIndex } from './functions'

const w = window

function allerVers (ctx, x, y) {
  ctx.beginPath()
  ctx.moveTo(x, y)
}

function createCanvas (larg = 100, haut = 100) {
  const c = document.createElement('canvas')
  if (!c.getContext) {
    afficher('désolé, votre navigateur ne sait pas dessiner !')
  }
  c.setAttribute('width', larg)
  c.setAttribute('height', haut)
  document.getElementById('ALdroiteTortue').appendChild(c)
  return c
}

function ligneVers (ctx, x, y) {
  ctx.lineTo(x, y)
  ctx.stroke()
}

export default function Tortue (X, Y) {
  // dimensions
  this.X = X
  this.Y = Y
  // contexte un dessin fait par la torte
  this.cnv = createCanvas(X, Y)
  this.ctx = this.cnv.getContext('2d')
  // contexte de l’icone de la tortue
  this.cnvTortue = createCanvas(X, Y)
  this.ctxTortue = this.cnvTortue.getContext('2d')
  // contexte poissons
  this.cnvPoisson = createCanvas(X, Y)
  this.ctxPoisson = this.cnvPoisson.getContext('2d')
  //  contexte Itineraire
  this.cnvItineraire = createCanvas(X, Y)
  this.ctxItineraire = this.cnvItineraire.getContext('2d')
  // image de la tortue
  this.img = new Image()
  this.img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAqCAYAAADvczj0AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QQYCgQSmBtNewAAENhJREFUaN7VWnlwFNeZ/72+pntujQ4kIUASAoOEQBE6wPEacJyQy2yMAzgGE7s2yebYrUpSm00l2U0ob+0fyW5tau9a767tYIMDSnyA7SQbO7ZxuE8hA0a3NCPNaM7umenp6fPtHwiMhISBVSqbVzVVM/3evNe/19/3/X7f95rgD6ht34KPOg6Ou3xQn3kG9p3Mwf0hAN2xFTUOAQuKPo4na8wC1QC8cydzsX8IgM9fQHZlE+ZzLNNaU7PQ8Xg8TN2iXKbnArTf+xPesQ31AMopBe+W3KZDHeKWJBSLRb6gaSIAgwDDhCL5bBfUW56Yosbt9lqj4VEvz3KUAAEA6TkDvGMbGiiQ3rPv1ib90mPeBevuuaeuZWULXxIKcUeOHbFZlnN1d58T3T633VC3xIonJkhRLwqpZKqZ4zi6f/ey3Nnz3dpIeDTmDSDy5JOgs81PCPLZXLYUAAzLJADEOfHhnVsg7e6CZluIMyw+vGMrFJ3iZFcXzOlj168HuWuRu+n+++8vHY9GXZHoGPv6m2/4DNNky0rLtO99+zvFn734M3/QH0znlBwMy3KobZOx6Lgv6A/olmWRFcsaXbZtV2oFtfYbX+X7I4lEuqsL+g0PmCLicgkh3TB8AEAtjMwJYItB2fataJv8KTYsbmB6B/pXAzg2bWPci+rqOrc+tIX5791P+xKJhLSgpib/8Y2fSPs8HprL58l3f/BXJQAgZ+WQnJWvGCZAAEDTNW5FU1NWyWaZkmDIisViVZ/f8ajg4lxYv7Ynd/TYkZhQxPhTB2Dhyp/Eq2BZli3YxF4MoOd2AZOrX7ZsAS8QLATgB7BsflW1mlPzXGXFPL23v88HYMgTxIknnwTdsgXkgY0b19QvqnW/dPBlbzQWcy9asDAxEh4tn76AIHAOIQzVdYMFgG/++dfjXS/83B0eC3unmiwxJVEq3HvPH5myLDPHT5/wtK5qVdySGD90+PC4CcgCwaZJU6YADI+Gl588eHv0RK7juAAAPwiWASib3ElKKYXjOGRysEMBeW3HGutyX68/nUmLSxqWxIdHht0ej9uQZSU00yJbH9oS3//zrgoAWH7XMuVTn/iUTilFMpXEqTOnXRcvXfRTSskkcMqyLOVYLkcYhhJCsGRxvTEencgmkvE6AKwg8LphmIJF8Mq+fcjfEeDrgK8AQXNVZVUhGou6p/cvbViatR2LhCOjHlBCTdMk9Mo8ZLZFNn16U+LXr/9PiVYsTnEhlmVo/aL6XPm8Cuv0mVOBhTUL1b6Bfv/V/oA/kPEH/FYkEgkSQrj21W3yqTOngqIomaqqanv247XbNWlmhismAMwEljCkODwyzA0MDvosyyFtrW0KvTIHudkiJ0+d8Cxf1pSbft22HdI32O8/cvRIiDoUWx/aMiVYKVmlJBwOl1NK+cbljYl0JsV96xvfSlimyYLB6TkRHssakRYsXHY4MIRCB4F/0tTAs6xhWKZ706c3Jfw+v5GWU5ysyEJT4wolkUzMShP5fJ5vX92Wv9zX65n1RjgGtu3oo+HRKRu9pL4h29nRmYtFo0jJGf74yeNMbU2tWrCSPadPzwHgixeB7vdg91xArOciRlY0IUMIBu9bt8EXTyZFwzTNSHiEzedVIRyJeCmlJODzG3/zgyf01371C2Gmp81xnHPfhg2Fk6dOzQq4o60jk0wmuVQ6NWXj0pm0KxIJuzo7OguXLr/HaQXNl0glxZrKutLaajnRffFGurw9k57Wnt+PMULgLFywyNH1Ite4bLkuSm49mUpKV8f0Dfb7CUPId//iOwmGYeh0P727c23m4qVLzIObPpMQeN6+UVQQunnTg7S3v88/0z0UdZ09efqUt6K0jAkGAxmvx5NTZEVy+3xtu3bd3J3uSEu3tQiLC1ohFJuYkOZVVBZHRoZviMYsw2aPnjjm2nDv+qzX4zHSmTQfCpUUP/uZh+TXf/NGsLe/zye6ROMLj/+Jeui377gBEJ7n7Y9u+Eh6Tedalec4yvN8cXhk2D3TPaiqyodKgno8mRD9Pp+ezqRL2ld36OfOjWo9F249Ut/S7uzYxqwReH6BJEpZOSuHSkKhopzJuK5SCQDwHG+blnltA7/25a/En//pPn9aTovTeXnr5q3J4ydPiKZtk1Qy4crl84LX4zX+8e9/zB78xavplw68dAOf19fW5R7b+Zj+/Sd+EFrS0KAkU2nJsi0tl82O7NmPS3OWPOzcAqmxqbHk3QsXuIqKckHOylBk2dWyskUWBB7vXb7sqa6u1kbDo+7rAZ/v6WF1U7/BZQzDYkpCIQyNDHsty7rWn1fzwnd3/XVhx8OP0JJgsJiRZREAykrLtNaWFnVoaFj48T/92AuA6e/vD0yygwsE0Z0PQnA4lFECF6FwUQJCAWU8juhbb03V5x9o0k1NqPniY18IHDr8juczD2ySNa1oJ1NJMTYRk8bGx6SK8nLt/vX3FY+eOD7F/xLJhLBs6bJ8NBaVps/pcbu1waFBz41mm+ePHj/qeXznY+n4RNzJ5rJCoVDgB4YG3elM2kUdSqqrqgq5XJ5SQsEwjMkyjM9m0NS4bLk3kUzeBYIqAJU1VdXlGz9y9zyfZ7i+uZGWrFoK+fx7sGZ9wg8/DDdnw+8QVHq9HoYQgng8wQwM9fuuHxcZG/M8/exPbgClaRpH6czJj6Jkb7rRr7z66g3SEwB0Q2dLQ2VmJBIRWZYxfR6/ZlOLqPkCNzg0KP7d3/4wNRwetf/r6f8sHYuOexKpuGQ7NgOgkgqQABzhAODRR8FaRZQyDAIECFGKajgQKQE4lik8u+dZs2b+/LysyKSpsVnJpNNcWk4LkiTZO7c/mjv4ykGREIL+wQHfVRnKsgx1uVzOzLycY1mWpbZtzxhDSoIBKzwWnnEz5lfPtxRFzgyPjARM25QYjiUOdcSirovf+t63r40L+gO6kstyAGwALCgWPbIVDPfINixydLQyBCIobEqggkIBQQIEjuPQKsKxZr5Q4CzTVAeH+n35vCowDEM5nncICDS9yDKEoaGSkC5JolVfV683LF5st7a0ihXl5QnDMIjjWLAtCl7gsaJphWPbtn723FkmPBYWaqpqDFESqc/no4QQdKxuZ57v2p/RjSJzufeyr2FxQ66+tt5sXtHk/PbIYW5okiXqauvlXC7L5pSsyDBM0XEc8X0qK3Iulyuv60XvpKFRBgDZvg3NhCJrs5B5C8burqllk7/8xvz1m//4Qfaf/+1fKirLK5JKPstrWjFwtX/5suXKeCzqUiaDzPVtTXtn+tjJ4zdQ2PSIPr2tbF4ln+/pDk6/LrpcdigUKmq6zmYVxbX6Q6vlVDrJDQwOegnDFL/2p1/OhUpC5F//4999+XyOn8zQrqSkFGN7unCI27Pv5jllPJFAb28vQxhS5FwuqiWKU3wrnU7zq5qa1UOH35kCOBAMFodGhmeUmyuaVmTPdp8tmW3NolZgZhMgqUxKrCyvLMiZjMgQQhlcETqLa+tzLx14WSqoGj9JhdQtSrJmFHXq0HGHw/At0ZKlG719/X2t1KGuSCQsAoDLJdgdbR0yAwbr161zZFmmhw5PLSL6vF57bGxsRhGhG/pNFd7Gj20sakWNicZi7uupy+vzGdRxyEh41AcA+UKBScqZDAjO9Q/2uynAEMALAoc6UFRLi+z9KYzb4mEdiBPCUIHnnbraunzfQL9flNzm4aNHQo7jkLcPH8KGdeuT0/8nCaJ9vTCZomcZdtba1YqmFcprv3xNiscTUnPjCgWEgGUZSJLkiIKLvvnO29dcJBwekSzLGt2zD2NzJi1XrYJ/zeqOyoySYaqrq81kKiXk83nhejAMw9irmlfl6upqC6Zl0fKyMn1e5TwrnUlzpvm+r3o8HqO9rV3pWN1mhkpKNCWXJYVCgb/a/8T3n0g2LF5sv/zKgRLbtpnYREyKTcSk8WhUSqVT/IZ167UTp05651dVqw51sGDBwuJ4dDzec+HWq5cfCLisDLpXdOoqKirpe72XCChM0zSn+KZpW+SrX/yK/dRPngrIsuzKyBlXOBKWKsrKiw9/dqt85txZz8b7P5q8b/192iuvHiw5fvKEb3B4yH3P2ruVdffemx+NhFlN07hQSVB95rmfhGzbvsHkTdNkT58942EYhhIKGKbJdrR35HsvX46ev4jcnAEeHgZqauS8aRqLZFnxTwcLAB3tHfKFSxcwPDIyRT3l8jlhLBolj+/8fObFAy+Ejh4/5rMd5xqYgaFB99nuc55gIGB888++Lr948IBHURTXjBqY4xzJJVqBYMDgXYKTV3PCpzZ+Qj1+/Fhv98Urhb45SQ8BYO9+xNOpVPhqxZBlmGvU9cAnP51Y1dxsHztxbMaoOzERc7988GV3sWjMurmTwYlOTMTcs41ZML9GLRo6J3Cc09neqS5asCj/8wMv5KbT6FyePKQ+ufGTyTfefMPDsKyh60XiOI74q9d/WWoY1k03ThIl54MmV/LKrH0uwWW3t7VrHe2d2kh4mDt99ow7p2Rzspbt/p2dLbU0wvT7/XXV1VVWZCziLystM1VV5W3b+cAUs6gVmZsJDQAoFDQrq2T5mfzXtm1GcruNj9//MebDa+4u+dCqlvzB114Z3rsPif97EW9ae2Qz3Nu34iM2waZTZ0+XDAwNiqIkWRPxCbfX5zNuZZFQaUj/oDHtrat1kFnZCvfdu958+rndTDaXk3/4Dz8SCSD9Tk4PVzajCUDt1WJBoVDgW1a2ZN1ut+H3+mwlq/D3rP1wejQSntH/vD6fUV5aZiRTSREA2la3ZR5/9PP5fF41YhMx6f0MSnGSqan1rAU1NfnysjK9UFC5iYkJR5Yzwvme8yhoGmsYhtDd4wzOKeDPfQ4ioeicPk7OZLiHNm/OC4LLvvTeJa8gCBbH8baqqvyNszhkIh6/BswwDbx16C1vZJoKkxXZRQihHo/HNE2TnV9VrSpZRchlczzDMrQ8VJYZGBo0U+l0aaFQYCilzPkLuDyngFsa0QGg9AZOtMyR06fPxOfNm+d3HJsm0ilXJpMWVzavkpcuWaK2tnxI5Xjeiifiom07JBAMFi3TZCmlpFAo8LZtE6/HKwPUsh1HAEAWzF+QDwQCplty25ZpklQmLa7tXCOv7ViT6363e3AkGjvDEJQTIMQxbBEMmTj/Lo3M6fkwJaCg7x+AXVcJG312nxNNZt/u62xd2lRCUFMSCBgXLr4buJrjtraszgDAvIp5hbWda9Rf/+Z1tnpelTaRjLuKWtEoFFS3Qynn9XiUvKoGVU3lNm96MEvhYPdzz9qgGH3zyKHw889DAYDt2xACRS0AWI4tORQDd+LDN42wX3oArCqhFkDH1LNpvL13P8avP0kMloZaq6rm++5a0mCOhMPc+Z5uv+iSbF3XWd4l2EsXL1UvXb7oMwydtW3H8fm82UJBc9m27aZAmKFIgkB1AN2kSHd1TRUT27eiHUDD5PqRvfvv7JWHm/NwFRwqg2eAEQr4CaBRwO0NInr9sN1dKADp3+7clvb1vNtTTgARBKZumCwDlJWVljJFvWALLjFWKGoZhoGay+X9DIXKWsjsfhG3Eu2F656SZ8dmiM+9gOKcAq6uBvpUDOgG+gWCNjAYIDZKZzup370POWAmXTs++ZnSkrd1pxTSVXv0+/yebD675E7Oh28KeNcuUODKUcauXTg++TuB30+LAiifLOnyxLn99ztuWUtfB/731iiPvivpOYqO4+icB7HfifD4/9JKS+EEvRijgGBQvL137529mPa/x0lnkdbHV8AAAAAASUVORK5CYII='
  // image de poisson
  const srcPoisson = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAaCAYAAAA0R0VGAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QYDCjUxAfm4RQAAAgFJREFUSMfN18uHVQEcwPHPZOQaYxYjuZJk5FJaJGoTSX9AWiQZ44oey7Rq3yrpD4iSUa1aRIw2EdUtvR8kPS+pxpippklPck2b3+TMcd/3dp0fP4dzfuf8vuf8nofuyhIUZFDyKGEee7IEthFTATaPMvqzAnc7AbagxSyAba0CNo+XWNrJg/uwEjm8afHeHTjRoACu4DzmsBprI9yTeBL6rp6TAn7hYItw92t8sVb1GY5gsJaj6TA8He2gGRlvE+ZWfM3p1PkpbK/m6GzC6DIGmoA72QbY8cT9y/A+db2C/WlHu1JGJQzVabRF/GgDbjj1rKM17HYnjYaCOmnwMJXsy3EITzvIr3wK7lgNuy/h7588r2JUwePoY5UuJP94IqfX4GMd20Xhvdil6mukZVyNDlHt+p9gGUmOmLkeNe2R0AWZxYOo4pu4g59S829Fj+De4hKu41G9JtwXx4GI/0AP4M5Ffn/H74jYDD4ljotktEf51oy+xqokXClDcJMx68H6DIF9xoYFsH7sS4V4BqdwIYZxPqprE7ZVaaTdkhs4gFfJk+WgvouxJnawddiLMw2aaDP6DRPYWcvZYWxu820HY9S0CvU1XjDXaNnsRHIBl8MHXIu0GE7sZ7PRYF/E+CpgC+797541GtU11uQPzURszz2RYr0NtsaPUF5GZawV47/zHIQaoy0+UQAAAABJRU5ErkJggg=='
  this.imgPoisson = document.createElement('img')
  this.imgPoisson.setAttribute('src', srcPoisson)
  // image des arêtes
  const srcArete = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAAbCAYAAADh0qZQAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QYDCw4wS44N3AAAA8RJREFUWMPt12uMnVMUBuDnnFaZmpaWMtpOS3Wqo1qt6A+JirqXIERci0oMCWkkbn+EJqgEqVuKEPSHhkalCSXqUpQqdatLa+hF6hY6mDE1hpbp+PMe+UxcKmmTScxOTs7e+3zfWu9a71rv3oee0TO22ShtCyO9tzPIveNjTdb9sR82owbPbq2hfTAUu20DUKfHHhyNpzG78PseWI9VWIpGnLw1hhfiA1wWsKWtpLCEgdgh63PxFC7OehamBujg7E3E5zgo4K7Negnq/8npdejEJjyHvf4F5Jh8T8f+GIQqvInzcQtGogGn4uNCYBPwWiiHXTAat6IZd8XWn0YZb+FTnITvMr+ky3PD8z0Ni3AlRuFHnIYjQ19H5t8H4ObsDyr0QH32oTVBPICzE9wiTO4K8gvsjAswF1eE+kfQF4fi8kR4XkpiQiLvi2MT3BRswGrMxEq0x2EF1AEJYFgBwyjchOPisxwWaopA+2NFDK1KZCvxLUZgMVpi+E6sy7MtqMMNeDwBTgrgOXg49fk+HsR8LMMv6frKeDLltjT1fUQS8GiSp4yf8Ax6RRKWp9Z2Ss205OUROCd0dMbQjqH1kGRkU+xMC5hGjMMxKYvR2a8LwAHx0Ymx8fFiansYDqyA7EhdvpHaK6XjqjEPQ1ISC0PnJ+gTqfkmdTkYbdn/raCR/ZLJMrbkt6rYL2EjXs38Pfycd5+PVN2NIeVsrsX4fGoLslKX9ctJ/TDsGnDtuLAg1OOz1463A2zP2FiPV9CUbNcmex1Rl4k4K+Al2zMzH9crk804IRHdlw6tjsN++CG0VORkLO5Jp5aiCNVpqv54KGXQkkzPwPWZH44bk3kBWzyhqlM+ren4Pw6ZaryUF1bHQCduD5AVAVIfwF9HsqbiooA6M03WnNo9OFSvibQI8MUJUtThanwUn03J4sb478S6Ct1taaAX0qUVQW3CZ/gyIt8amagJNRtwaehbEuMDQn1jKB+ZRpRSGRP1EKbm4YnYuR/XVLoa72JGufDyrzgqtTEvTkbE0LJ0+5ZCd9flmcp7vdMEcuz1xutZV468yfgqejk84GsCsj7lMj1BzMpRO7eol8txbyIfinewIJICp6SjB+LmOBKaP8TuAXZHSqZC5224Kr81pEmbw1BbGGlOc87HGUlIuevxOCgSc1i0aW0EedLfnN/lAiUKgVTO44GFS0i/Ls8dn1OsITelE/N8VZfLTemvLqULArYpujclTfJfLrid2/NyOqAgprOxb3e85vfBYzmba7vz/5Gq/9Ufp57RXcfvv8n4OWkNly4AAAAASUVORK5CYII='

  this.imgArete = document.createElement('img')
  this.imgArete.setAttribute('src', srcArete)
  this.reInitialise()
}

Tortue.prototype.reInitialise = function reInitialise () {
  const t = this.faitRepas(document.getElementById('ALlisteRepas'))
  this.x = t[0]
  this.y = t[1]
  this.ctxTortue.clearRect(0, 0, this.X, this.Y)
  this.ctx.clearRect(0, 0, this.X, this.Y)
  allerVers(this.ctx, this.x, this.y)
  this.angle = 0
  try {
    this.positionneImg()
  } catch (e) {
    console.error(e)
  }
  // le dessin
  if (this.dessin) {
    this.dessineItineraire()
  }
}

Tortue.prototype.dessineItineraire = function dessineItineraire () {
  this.ctxItineraire.clearRect(0, 0, this.X, this.Y)
  this.ctxItineraire.beginPath()
  this.ctxItineraire.strokeStyle = 'gold'
  this.ctxItineraire.moveTo(this.dessin[0][0], this.dessin[0][1])
  for (let i = 1; i < this.dessin.length; i++) {
    if (this.dessin[i].length === 2) { this.ctxItineraire.lineTo(this.dessin[i][0], this.dessin[i][1]) } else { this.ctxItineraire.moveTo(this.dessin[i][0], this.dessin[i][1]) }
  }
  this.ctxItineraire.stroke()
  this.ctxItineraire.strokeStyle = 'black'
  this.ctxItineraire.closePath()
}

Tortue.prototype.effaceImg = function effaceImg () {
  // Store the current transformation matrix
  this.ctxTortue.save()
  // Use the identity matrix while clearing the canvas
  this.ctxTortue.setTransform(1, 0, 0, 1, 0, 0)
  this.ctxTortue.clearRect(0, 0, this.X, this.Y)
  // Restore the transform
  this.ctxTortue.restore()
}

Tortue.prototype.positionneImg = function positionneImg () {
  const c = Math.cos(this.angle)
  const s = Math.sin(this.angle)
  this.ctxTortue.drawImage(this.img, this.x * c + this.y * s - 30, this.y * c - this.x * s - 21)
  let i
  // le repas
  if (!this.repas) { return }
  if (!this.poissons) { this.poissons = [] }
  let x, y
  this.ctxPoisson.clearRect(0, 0, 1000, 1000)
  for (i = 1; i < this.repas.length; i++) {
    x = this.repas[i][0] - 20
    y = this.repas[i][1] - 15
    if (this.poissons[i]) {
      this.ctxPoisson.drawImage(this.poissons[i], x, y)
    } else {
      this.ctxPoisson.drawImage(this.imgPoisson, x, y)
      this.poissons.push(this.imgPoisson)
    }
  }
}

Tortue.prototype.tourneRalenti = function tourneRalenti (angle) {
  const nbPas = 40
  const tourne = w.ALtortue.tourne.bind(w.ALtortue, angle / nbPas)
  for (let i = 1; i <= nbPas; i++) {
    w.setTimeout(tourne, getFreqTortue() * i)
  }
}

Tortue.prototype.avanceRalenti = function avanceRalenti (nb, sansTrace) {
  const nbPas = 40
  if (sansTrace) {
    const avance = w.ALtortue.avance.bind(w.ALtortue, nb / nbPas)
    for (let i = 1; i <= nbPas; i++) {
      w.setTimeout(avance, getFreqTortue() * i)
    }
  } else {
    const avance = w.ALtortue.avance.bind(w.ALtortue, nb / nbPas, true)
    for (let i = 1; i <= nbPas; i++) {
      w.setTimeout(avance, getFreqTortue() * i)
    }
  }
}

Tortue.prototype.tourne = function tourne (angle) {
  if (papGetIndex() <= 0) {
    filmTortueSetElt([1, angle], -1)
  }
  angle = -angle * Math.PI / 180
  this.angle += angle
  this.effaceImg()
  this.ctxTortue.rotate(angle)
  this.positionneImg()
}

Tortue.prototype.avance = function avance (nb, sansTrace, sansEnregistrer) {
  if (papGetIndex() <= 0 && !sansEnregistrer) {
    filmTortueSetElt([2, nb, sansTrace], -1)
  }
  const x = nb * Math.cos(this.angle)
  const y = nb * Math.sin(this.angle)
  this.x += x
  this.y += y
  if (!this.chaineDessin) { this.chaineDessin = '[[300,300],' } else { this.chaineDessin += ',' }
  this.chaineDessin += '[' + Math.round(this.x) + ',' + Math.round(this.y)
  if (sansTrace) { this.chaineDessin += ',0' }
  this.chaineDessin += ']'
  if (sansTrace) { allerVers(this.ctx, this.x, this.y) } else { ligneVers(this.ctx, this.x, this.y) }
  this.effaceImg()
  this.positionneImg()
}

Tortue.prototype.mange = function mange () {
  if (papGetIndex() <= 0) {
    filmTortueSetElt([3], -1)
  }
  // pour enregistrer un repas
  if (!this.chaineRepas) { this.chaineRepas = '' } else { this.chaineRepas += ',' }
  this.chaineRepas += '[' + Math.round(this.x) + ',' + Math.round(this.y) + ']'
  // initialisation d’un itinéraire suite à l’instruction "départ"
  this.chaineDessin = '[[' + Math.round(this.x) + ',' + Math.round(this.y) + ']'
  function d2 (t1, t2) {
    const x = t1[0] - t2[0]
    const y = t1[1] - t2[1]
    return x * x + y * y
  }
  let chgt = false
  for (let i = 0; i < this.repas.length; i++) {
    // mange les poissons à moins de 20 px
    if (!this.repas[i][3] && d2([this.x, this.y], this.repas[i]) < 400) {
      this.poissons[i] = this.imgArete
      this.repas[i].push(true)
      chgt = true
    }
  }
  if (chgt) {
    // (pb de synchro si une seule instruction)
    this.avance(10, true, true); this.positionneImg(); this.avance(-10, true, true)
  }
}

Tortue.prototype.faitRepas = function faitRepas (sel) {
  Tortue.prototype.repasLu = true
  Tortue.prototype.dessinLu = true
  const initTortue = [300, 300]
  const lesRepas = [initTortue]
  Tortue.prototype.repas = [initTortue]
  const lesDessins = []
  // le premier terme est la position initiale de la tortue
  // termes suivants : position des poissons
  // partie pouvant être modifiée par l’utilisateur

  // définition des repas

  lesRepas.push([[300, 300], [400, 300], [400, 200]])

  lesRepas.push([[300, 300], [400, 300], [400, 200], [300, 200]])

  lesRepas.push([[200, 300], [400, 200], [400, 100], [300, 100], [200, 100], [200, 200], [200, 300], [300, 300], [400, 300]])

  lesRepas.push([[100, 500], [100, 500], [200, 500], [300, 500], [400, 500], [500, 500], [500, 400], [500, 300], [500, 200], [500, 100], [400, 100], [300, 100], [200, 100], [100, 100], [100, 200], [100, 300], [100, 400]])

  lesRepas.push([[100, 500], [100, 500], [100, 400], [200, 400], [200, 300], [300, 300], [300, 200], [400, 200], [400, 100], [500, 100]])

  lesRepas.push([[100, 100], [100, 100], [100, 200], [100, 300], [100, 400], [100, 500], [200, 500], [200, 400], [200, 300], [200, 200], [200, 100], [300, 100], [300, 200], [300, 300], [300, 400], [300, 500], [400, 500], [400, 400], [400, 300], [400, 200], [400, 100], [500, 100], [500, 200], [500, 300], [500, 400], [500, 500]])

  // définition des figures
  lesDessins.push([[100, 300], [200, 300], [200, 200], [300, 200]])

  lesDessins.push([[300, 300], [500, 300], [400, 127], [300, 300]])

  lesDessins.push([[300, 300], [500, 300], [500, 100], [300, 100], [300, 300]])

  lesDessins.push([[300, 300], [400, 300], [450, 213], [400, 127], [300, 127], [250, 213], [300, 300]])

  lesDessins.push([[300, 300], [350, 300], [350, 240], [280, 240], [280, 320], [370, 320], [370, 220], [260, 220], [260, 340], [390, 340], [390, 200], [240, 200], [240, 360], [410, 360], [410, 180], [220, 180], [220, 380], [430, 380]])

  lesDessins.push([[300, 300], [350, 300], [313, 235], [262, 322], [387, 322], [313, 192], [225, 343], [425, 343], [313, 148], [188, 365], [463, 365], [312, 105], [150, 387], [500, 387], [312, 62], [113, 408]])

  lesDessins.push([[200, 300], [200, 400], [300, 400], [300, 300], [400, 300], [400, 200], [300, 200], [300, 100], [200, 100], [200, 200], [100, 200], [100, 300], [200, 300]])

  lesDessins.push([[100, 400], [200, 400], [200, 300], [100, 300], [100, 400], [400, 400, 0], [400, 300], [300, 300], [300, 400], [400, 400], [400, 100, 0], [300, 100], [300, 200], [400, 200], [400, 100], [100, 100, 0], [100, 200], [200, 200], [200, 100], [100, 100], [100, 400, 0]])

  lesDessins.push([[300, 400], [400, 400], [500, 300], [500, 200], [400, 100], [300, 100], [200, 200], [200, 300], [300, 400]])

  lesDessins.push([[100, 400], [300, 400], [300, 200], [100, 200], [100, 400], [110, 400, 0], [110, 390, 0], [310, 390], [310, 190], [110, 190], [110, 390], [120, 390, 0], [120, 380, 0], [320, 380], [320, 180], [120, 180], [120, 380], [130, 380, 0], [130, 370, 0], [330, 370], [330, 170], [130, 170], [130, 370], [140, 370, 0], [140, 360, 0], [340, 360], [340, 160], [140, 160], [140, 360], [150, 360, 0], [150, 350, 0], [350, 350], [350, 150], [150, 150], [150, 350], [160, 350, 0], [160, 340, 0], [360, 340], [360, 140], [160, 140], [160, 340], [170, 340, 0], [170, 330, 0], [370, 330], [370, 130], [170, 130], [170, 330], [180, 330, 0], [180, 320, 0]])

  // fin de la partie pouvant être modifiée par l’utilisateur
  let i
  if (arguments.length === 0) {
    // on met les repas dans la liste
    const s = document.getElementById('ALlisteRepas')
    for (i = 0; i < lesRepas.length; i++) {
      const opt = document.createElement('option')
      if (i === 0) {
        opt.innerHTML = 'Rien'
      } else {
        opt.innerHTML = 'repas ' + i
      }
      s.appendChild(opt)
    }
    for (i = 0; i < lesDessins.length; i++) {
      const opt = document.createElement('option')
      opt.innerHTML = 'itinéraire ' + i
      s.appendChild(opt)
    }
    return initTortue
  } else {
    // choix du repas
    i = sel.options.selectedIndex
    if (i > 0) {
      Tortue.prototype.repasLu = false
      Tortue.prototype.dessinLu = false
    } else {
      if (Tortue.prototype.repasLu) {
        return Tortue.prototype.repas[0]
      }
      if (Tortue.prototype.dessinLu) {
        return Tortue.prototype.dessin[0]
      }
    }
    Tortue.prototype.poissons = []
    if (i < lesRepas.length) {
      Tortue.prototype.repas = lesRepas[i]
      Tortue.prototype.dessin = [initTortue]
    } else {
      Tortue.prototype.dessin = lesDessins[i - lesRepas.length]
      Tortue.prototype.repas = [initTortue]
    }
    if (i <= 0) {
      return initTortue
    }
    if (i < lesRepas.length) { return lesRepas[i][0] } // position initiale de la tortue
    return lesDessins[i - lesRepas.length][0]
  }
}
