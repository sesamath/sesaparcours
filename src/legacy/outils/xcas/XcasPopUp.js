// -*- coding: utf-8-unix -*-

//   Copyright (C) 2009 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distribute  in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import './popup.css'

function XcasPopUp (left, top, width, height) {
  this.mobile = false
  this.x0 = 0
  this.y0 = 0
  this.left = left
  this.top = top
  this.div = document.createElement('div')
  this.marqueur = document.createElement('div')
  this.marqueur.setAttribute('id', 'xcmarqueur')
  this.div.setAttribute('class', 'popup')
  this.div.style.top = top + 'px'
  this.div.style.left = left + 'px'
  if (width) { this.div.style.width = width + 'px' }
  if (height) {
    this.div.style.height = height + 'px'
  }
  document.body.appendChild(this.div)
  this.titre = document.createElement('h3')
  this.div.appendChild(this.titre)
  this.contenu = document.createElement('div')
  this.contenu.js = this
  this.div.appendChild(this.contenu)
  this.img = document.createElement('img')
  this.img.setAttribute('class', 'fermer')
  this.img.setAttribute('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAAB3RJTUUH1gQFFCUoDIRw0QAAAaJJREFUOMulk0FLG0EYhp9ZV9zpKnFbgzSbo80PEA8RLbSXQv5DjulRf0x7bI/5Dx5bQSh4UrxIPJq0DdsQ4kLWZZP5euhszBpzqQMDwzvzvXzf+74Dz1zqMdCGEDgCGkDNwh3gBPjchN5Sgja0gE/1MNQlrREAEYwx3N0nnP/6nQDHTfi6QNCG1ivP+7JXrfIniojjmIkxCLDiOKz7Pi+DgMt+n0GafsxJ1FzbNx92dvTP21uSNEWgsAHWVld5vb3NabebAG+a0HPs3VG9UtGDKCJJU+oi7IvMCg9FOBQhyTIGwyF75bK2OpETNDa05i6OZ0UAByIcyANigNF4jHZdrMgzgpoSYWpnPlML5vBdKQSYymyoGoCbPxARTH5+wu+CJuYBzzvoTI3gOA4CvJVFivdWE6UU5h9DZ57gJL5PWPd9zKO2v82NI8CG5zGeTLDBKtr4rlrV3X6fJMsK9okV0HNdwiDgRxQVbbTxPL6KIirlMiXfR1nRDIBSlLQmDDa5Ho2waewtjfLu1pZ+4a6AgCCIwDjLuBgOl0f5fz/Ts9dfScq9YFxk3JAAAAAASUVORK5CYII%3D')
  this.img.js = this
  this.img.onclick = function () { this.js.supprime() }
  this.div.appendChild(this.img)
  // listeners qui seront mis au mousedown et viré au mouseup
  this.listenerMove = this.mouseMove.bind(this)
  this.listenerUp = this.mouseUp.bind(this)
  this.titre.addEventListener('mousedown', this.mouseDown.bind(this))
}

XcasPopUp.prototype.masqueFermer = function () {
  this.img.style.display = 'none'
}

XcasPopUp.prototype.supprime = function () {
  if (this.inclus) {
    if (this.inclus.style) { this.inclus.style.display = 'none' }
    this.marqueur.parentNode.replaceChild(this.inclus, this.marqueur)
  }
  document.body.removeChild(this.div)
  delete (this)
}

XcasPopUp.prototype.mouseDown = function (evt) {
  evt.preventDefault()
  this.mobile = true
  this.x0 = evt.clientX
  this.y0 = evt.clientY
  // on ajoute nos listeners
  document.addEventListener('mousemove', this.listenerMove, false)
  document.addEventListener('mouseup', this.listenerUp, false)
  return false
}

XcasPopUp.prototype.mouseMove = function mouseMove (evt) {
  if (!this.mobile) return false
  this.left += evt.clientX - this.x0
  this.top += evt.clientY - this.y0
  if (this.top < 0) { this.top = 0 }
  if (this.top > document.body.clientHeight) {
    this.top = document.body.clientHeight
  }
  const w = 50 - parseInt(this.div.style.width)
  if (this.left < w) { this.left = w }
  if (this.left > document.body.clientWidth) {
    this.left = document.body.clientWidth
  }
  this.div.style.top = this.top + 'px'
  this.div.style.left = this.left + 'px'
  this.x0 = evt.clientX
  this.y0 = evt.clientY
  return false
}

XcasPopUp.prototype.mouseUp = function mouseUp (event) {
  this.mobile = false
  // on vire les listeners
  document.removeEventListener('mousemove', this.listenerMove, false)
  document.removeEventListener('mouseup', this.listenerUp, false)
  return false
}
