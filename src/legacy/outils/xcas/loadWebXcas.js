import { j3pBaseUrl } from 'src/lib/core/constantes'
import { loadJs } from 'src/lib/utils/dom/main'

/**
 * Charge webxcas (gros ~15Mo) et retourne une fct permettant d’évaluer les expressions
 * @return {Promise<function>}
 */
export default async function loadWebXcas () {
  await loadJs(j3pBaseUrl + 'externals/webxcas/webxcas.js', { timeout: 300 })
  if (typeof window.Module !== 'object' || typeof window.Module.cwrap !== 'function') throw Error('L’outil webxcas n’a pas été chargé correctement')
  const caseval = window.Module.cwrap('_ZN4giac7casevalEPKc', 'string', ['string'])
  // ancienne fct j3pWebxcas
  return (expression) => caseval(expression)
}
