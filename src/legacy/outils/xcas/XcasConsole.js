// -*- coding: utf-8-unix -*-

//   Copyright (C) 2009 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distribute  in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import { xcasElement, xcasInsereTexte } from 'src/legacy/outils/xcas/base'
import XcasBase from 'src/legacy/outils/xcas/XcasBase'

import './xcasConsole.css'

// -----------------------------------------------------------------------------
// xcasConsole : envoyer des requetes xcas et afficher les reponses

class XcasConsole extends XcasBase {
  constructor (id, message) {
    super()
    this.initialise()
    if (id) {
      let konsole
      if (typeof id === typeof 'a') { konsole = document.getElementById(id) } else { konsole = id }
      // konsole.setAttribute("class","console");
      konsole.className = 'console'
      if (message) {
        xcasInsereTexte(message, 'p', konsole)
      }
      this.histo = document.createElement('div')
      konsole.appendChild(this.histo)
      this.input = this.createElement('input')
      this.input.onkeydown = function (event) {
        this.blur()
        this.focus()
        xcasElement(this).valide(event)
      }
      konsole.appendChild(this.input)
      konsole.setAttribute('index', this.index)
      konsole.onclick = function () { xcasElement(this).input.focus() }
      this.input.focus()
    }
  }

  valide (ev, obj) {
    if (XcasBase.isIE) {
      if (event.keyCode == XcasBase.ENTREE) { this.giac() }
    } else
      if (ev.keyCode == XcasBase.ENTREE) { this.giac() }
  }

  // envoie le contenu de la fenetre "programmation"
  prog () {
    let s = ''
    if (!this.script) { return s }
    const tab = this.script.value.split(/\n/g)
    let i
    const n = tab.length - 1
    for (i in tab) {
      if (tab[i].substr(0, 3) == '//*') { tab[i] = '' } else if (i < n) { tab[i] += '\n' }
    }
    // echange avec le serveur
    const sp = this.reqProg(tab.join(''))
    const t = sp.split(/`/g)
    if (t[1] != '' && t[1] != '\n') {
      s = 'Dans votre programme, erreur dans la ligne ' + t[1] + ' juste avant ' + t[2]
      tab[t[1] - 1] = '//* Erreur dans la ligne suivante, juste avant ' + t[2] +
        '\n' +
        tab[t[1] - 1]
    }
    this.script.value = tab.join('')
    this.changeScript = false
    this.session_id = t[0]
    return s
  }

  giac () {
    const s = this.input.value; let sprog = ''
    if (s == '') { return }
    if (this.changeScript) { sprog = this.prog() }
    if (sprog == 'undefined') { sprog = '' }
    // ------ on enregistre la requete
    this.tabHisto.push(s)
    this.input.value = ''
    this.input.focus()
    const text = document.createTextNode(s + ' ' + sprog)
    const p = document.createElement('p')
    this.histo.appendChild(p)
    const span = this.createElement('span')
    span.onclick = function () { xcasElement(this).touche(text.nodeValue) }
    span.appendChild(text)
    p.appendChild(span)
    // ------- traitement de l’instruction
    let sr = this.reqGiac(s)
    // rustine pour IE
    if (XcasBase.isIE) { sr = sr.replace(/``/g, '` `') }
    const t = sr.split(/`/g)
    // on affiche le resultat en mathml
    // let mml = this.insereMathML(p, t[2], false, false)
    // ne marche plus !
    // mml.onclick=function(){xcasElement(this).touche("("+t[1]+")");};
    this.session_id = t[0]
    // on scrolle en bas
    this.input.scrollIntoView(false)
  }

  touche (instruction) {
    this.input.value += instruction
    this.input.focus()
  }
}

export default XcasConsole
