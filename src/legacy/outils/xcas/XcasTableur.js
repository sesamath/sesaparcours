// -*- coding: utf-8-unix -*-

//   Copyright (C) 2009 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distribute  in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import { j3pShowError } from 'src/legacy/core/functions'
import { xcasElement, xcasOpacite, xcasRequete } from 'src/legacy/outils/xcas/base'
import XcasBase from 'src/legacy/outils/xcas/XcasBase'
import XcasConsole from 'src/legacy/outils/xcas/XcasConsole'
import XcasPopUp from 'src/legacy/outils/xcas/XcasPopUp'

import './XcasTableur.css'

const regLettre = /[A-E]/g
const regNombre = /[0-9]+(\.[0-9]+)?/g

window.xcasTableurActif = null
window.xcasAttendre = false

class XcasTableur extends XcasBase {
  constructor (id, nLi, nCol) {
    super()
    if (document.getElementById('xcA0')) {
      return j3pShowError('une seule feuille par page !')
    }
    this.parent = XcasBase
    this.parent()
    this.include(this.url + 'xcas/tableur.js')
    this.initialise()
    window.xcasTableurActif = this
    // exact ou approx
    this.exact = true
    this.cle = 'ma feuille'
    const bloc = document.getElementById(id)
    bloc.className = 'tableur'
    // neutralise le copier coller
    bloc.onkeydown = function (ev) { if (ev.ctrlKey) return false }
    let i, j
    this.outils = this.element('div', bloc, 'outils')
    let divEntree
    if (XcasTableur.mini) {
      divEntree = this.element('div', bloc, 'diventree')
      bloc.style.width = '415px'
    } else {
      divEntree = this.element('div', this.outils, 'diventree')
    }
    this.tabCell = this.element('span', divEntree)
    this.tabCell.appendChild(document.createTextNode('A0'))
    this.entree = this.elementIndexe('input', divEntree)
    this.insereOutil('triangleb.png', divEntree, 'Assistant', 'dispAssist', true)
    this.entree.onclick = function (event) { xcasElement(this).curseur() }
    this.entree.onkeyup = function (event) { xcasElement(this).curseur(); xcasElement(this).modeSaisie(event) }
    this.entree.onkeydown = function (event) { this.blur(); this.focus(); xcasElement(this).ctrl(event) }
    this.insereOutil('initial.gif', this.outils, 'Recalculer la feuille', 'fill_cell', true)
    this.insereOutil('flecheb.png', this.outils, 'Copier vers le bas', 'fill_bottom')
    this.insereOutil('fleched.png', this.outils, 'Copier vers la droite', 'fill_right')
    this.insereOutil('annul.gif', this.outils, 'Annuler la dernière entrée', 'annule')
    this.insereOutil('gomme.gif', this.outils, 'Vider la feuille', 'vide')
    this.insereOutil('chart.png', this.outils, 'Graphiques', 'assistGraph', true)
    this.insereOutil('ardoise.png', this.outils, 'Ardoise Xcas', 'afficheArdoise', true)
    this.insereOutil('prog.png', this.outils, 'Programmation Xcas', 'afficheProg', true)
    this.insereOutil('ouvrir.gif', this.outils, 'Ouvrir une feuille', 'ouvrir', true)
    this.insereOutil('disquette.gif', this.outils, 'Sauvegarder la feuille', 'sauve', true)
    this.insereOutil('fusion.png', this.outils, 'Fusionner les cellules sélectionnées', 'fusion')
    this.insereOutil('pref.png', this.outils, 'Préférences', 'pref', true)
    this.insereOutil('aide.gif', this.outils, 'Aide', 'aide', true)
    const conteneur = this.element('div', bloc, 'conteneur')
    this.divAssist = this.element('div', conteneur, 'assistant')
    this.faitAssist(this.divAssist)
    this.sel = this.elementIndexe('select', this.divAssist, 'commandes')
    if (XcasTableur.mini) { this.sel.style.display = 'block' }
    this.sel.onchange = function () { xcasElement(this).assiste(this) }
    this.assistant(this.sel, this.divAssist)
    this.spanAssist = this.element('span', this.divAssist)
    this.spanAssist.style.display = 'inline'
    this.divAssistGraph = this.element('div', conteneur, 'assistant')
    this.spanAssistGraph = this.element('span', this.divAssistGraph)
    this.spanAssistGraph.style.display = 'inline'
    this.faitAssist(this.divAssistGraph)
    this.progress = this.element('progress', conteneur, 'progress')
    this.progress.setAttribute('style', 'display:none')
    this.tableau = this.element('div', conteneur, 'tableau')
    const table = this.element('table', this.tableau, 'table')
    const A = 'A'
    let tr, td
    this.thead = this.element('thead', table)
    tr = this.element('tr', this.thead)
    td = this.element('th', tr, 'col1')
    this.lettres = []
    for (j = 0; j < nCol; j++) {
      td = this.element('th', tr)
      this.lettres.push(String.fromCharCode(A.charCodeAt(0) + j))
      td.appendChild(document.createTextNode(this.lettres[j]))
    }
    for (j = 0; j < this.lettres.length; j++) { this.lettres[j] = 'xc' + this.lettres[j] }
    this.tbody = this.element('tbody', table)
    this.tbody.setAttribute('id', 'corps_table')
    for (i = 0; i < nLi; i++) {
      tr = this.element('tr', this.tbody)
      td = this.element('td', tr, 'col1')
      td.appendChild(document.createTextNode(i))
      for (j = 0; j < nCol; j++) {
        td = this.elementIndexe('td', tr)
        td.setAttribute('id', this.lettres[j] + i)
        td.onclick = function () { xcasElement(this).cell(this.id) }
        td.onmousedown = function (event) { xcasElement(this).mouseDown(event, this) }
        td.onmouseover = function (event) { xcasElement(this).mouseOver(event, this) }
        td.onmouseup = function (event) { xcasElement(this).mouseUp(event, this) }
        td.ondragenter = function (event) { xcasElement(this).dragEnter(event, this) }
        // modif pour fabrique
        // td.setAttribute("onclick","xcasElement(this).cell(this.id)");
      }
    }
    this.hmax = XcasTableur.hauteurMax
    this.entreeSauve = this.entree
    this.prog = '//Taper des fonctions ici\n//et elles seront utilisables dans le tableur'
    this.cellActive = 'A0'
    this.cell('A0')
    this.debutZone = ''
    this.zone = []
    this.zoneCode = ''
    this.cellDragStart = ''
    this.cellDragEnd = ''
    this.cellZone = []
    this.saisie = false
    this.changeEntree = false
    this.n_lign = nLi
    this.n_col = nCol
    this.nb_lign = 0
    this.nb_col = 0
    this.sauvegarde = ''
    this.retourGiac = ''
    this.cell_marquees = []
    // remplissage
    if (arguments.length > 3) {
      // methode par les chaines
      if (arguments[3].charAt) {
        if (!this.cellule) {
          this.cellule = 'cxA0'
        }
        const coord = this.coordonnees(this.cellule)
        for (let i = 4; i < arguments.length; i++) {
          let lettre = 'cx' + coord[0]
          const formule = arguments[i] + ''
          const t = formule.split(/;/g)
          for (let j = 0; j < t.length; j++) {
            const cell = document.getElementById(lettre + coord[1])
            this.cell_max(lettre + coord[1])
            lettre = String.fromCharCode(lettre.charCodeAt(0) + 1)
            cell.setAttribute('formule', t[j])
            //  cell.appendChild(document.createTextNode(t[j]));
          }
          coord[1]++
        }
      } else {
        // methode par les objets
        this.remplir(arguments[3], false)
        this.timer = setInterval(window.xcasTableurActif.preremplissage, 10)
        // setTimeout("window.xcasTableurActif.fill_cell(false)",1000);
      }
    }
    return this
  }

  remplir (obj, calculer) {
    for (const i in obj) {
      if (i == this.cellActive) { this.entree.value = obj[i] }
      const cell = document.getElementById('xc' + i)
      this.cell_max(i)
      cell.setAttribute('formule', obj[i])
    }
    if (calculer) { this.fill_cell(false) }
  }

  preremplissage () {
    if (window.xcasAttendre) { return }
    clearInterval(window.xcasTableurActif.timer)
    window.xcasTableurActif.fill_cell(false)
  }

  poignee (c2) {
    // la poignee passe de c1 a c2
    const p = document.getElementById('xcdpoignee')
    let c1
    if (p) {
      c1 = p.parentNode
      c1.innerHTML = this.sauveCell
    }
    this.sauveCell = c2.innerHTML
    let esp = ''
    // var xcA1="\\"xcA1\\"";
    if (this.sauveCell == '') { esp = '&nbsp;' }
    c2.innerHTML = "<div id='xcdpoignee'>" + esp + this.sauveCell +
      "<img id='poignee' ondragstart='xcasElement(this.parentNode.parentNode).dragStart(event,this)' ondragEnd='window.xcasTableurActif.dragEnd()' src='" + this.url + "xcas/img/poignee.png'></div>"
    const xcdpoignee = document.getElementById('xcdpoignee')
    xcdpoignee.style.height = xcdpoignee.parentNode.offsetHeight + 'px'
  }

  faitAssist (div) {
    const img = document.createElement('img')
    img.src = 'xcas/img/fermer.png'
    if (div == this.divAssist) {
      img.onclick = function () {
        window.xcasTableurActif.stopAssist()
      }
    } else if (div == this.divAssistGraph) {
      img.onclick = function () {
        window.xcasTableurActif.stopAssist(true)
      }
    }
    div.appendChild(img)
  }

  stopAssist (finSaisie) {
    this.entree = this.entreeSauve
    this.divAssist.style.display = 'none'
    this.divAssistGraph.style.display = 'none'
    this.entree.focus()
    if (finSaisie) { this.saisie = false }
    this.sel.selectedIndex = 0
  }

  dispAssist () {
    if (this.divAssist.style.display == 'block') {
      // this.divAssist.style.display="none";
      this.stopAssist()
    } else {
      this.stopAssist()
      this.divAssist.style.display = 'block'
    }
  }

  assistant (sel, div) {
    let s = ''; let option
    const optGroups = [1, 8, 4, 3, 5, 6, 3]
    const groupTitles = ['Algèbre', 'Analyse', 'Arithmétique', 'Nombres complexes', 'Statistiques', 'Tirages aléatoires']
    let i, j
    for (i = 1; i < optGroups.length; i++) { optGroups[i] += optGroups[i - 1] }
    for (i = 0; i < this.commandes.length; i++) {
      for (j = 0; j < optGroups.length; j++) {
        if (optGroups[j] == i) {
          s = groupTitles[j]
          option = this.element('optgroup', sel)
          option.setAttribute('label', s)
        }
      }
      option = this.element('option', sel)
      s = this.commandes[i][0]
      option.appendChild(document.createTextNode(s))
    }
  }

  curseur () {
    // d’apres Diego Perini <dperini@nwbox.com>
    if (this.entree.createTextRange) {
      const r = document.selection.createRange().duplicate()
      r.moveEnd('character', this.entree.value.length)
      if (r.text == '') { this.entree.debut = this.entree.value.length } else { this.entree.posCurseur = this.entree.value.lastIndexOf(r.text) }
    } else {
      this.entree.posCurseur = this.entree.selectionStart
    }
  }

  insereDansEntree (chaine) {
    if (chaine == '') { return }
    if (chaine.charAt(0) == 'x') { chaine = chaine.slice(2) }
    const s = this.entree.value
    const s1 = s.substring(0, this.entree.posCurseur)
    const s2 = s.substring(this.entree.posCurseur, s.length)
    this.entree.value = s1 + chaine + s2
    this.curseur()
  }

  cell (cellule) {
    if (this.saisie) {
      this.insereDansEntree(cellule)
      return
    }
    if (this.changeEntree) {
      this.changeEntree = false
      this.fill_cell(false)
    }
    if (this.debutZone != 'copy') {
      this.debutZone = ''
      this.zone = []
    }
    let domCellule = document.getElementById('xc' + this.cellActive)
    // domCellule.style.border="1px solid #333333";
    domCellule.style.backgroundColor = 'white'
    domCellule.style.color = 'black'
    if (document.getElementById('xcdpoignee')) { domCellule.innerHTML = this.sauveCell }
    if (cellule.charAt(0) == 'x') { this.cellActive = cellule.slice(2) } else { this.cellActive = cellule }
    domCellule = document.getElementById('xc' + this.cellActive)
    if (domCellule.hasAttribute('groupe')) {
      this.cellActive = domCellule.getAttribute('groupe')
      domCellule = document.getElementById('xc' + this.cellActive)
    }
    domCellule.style.backgroundColor = '#ffff99'
    // domCellule.style.border="2px solid black ";
    this.poignee(domCellule)
    this.tabCell.firstChild.nodeValue = this.cellActive
    const formule = domCellule.getAttribute('formule')
    if (formule && formule != 'null') {
      this.entree.value = formule
      this.entree.select()
    } else { this.entree.value = '' }
    // sauvegarde pour annulation ulterieure
    this.entree.sauvegarde = this.entree.value
    this.entree.focus()
    // reinitialisation de l’assistant
    this.spanAssist.innerHTML = ''
    this.sel.selectedIndex = 0
  }

  coordonnees (ref) {
    if (ref.charAt(0) != 'x') { ref = 'xc' + ref }
    // ref est une reference de cellule, de ligne ou de colonne
    if (ref.length == 1) {
      // ligne ou colonne
      const n = parseInt(ref)
      if (isNaN(n)) { return [ref, n] } else { return ['', n] }
    }
    // cellule :
    const s = ref.slice(3)
    return [ref.slice(2, 3), parseInt(s)]
  }

  insereOutil (src, cible, titre, fonction, param = '') {
    const a = this.element('a', cible)
    a.setAttribute('title', titre)
    a.onmouseover = function () { xcasOpacite(this, false) }
    a.onmouseout = function () { xcasOpacite(this, true) }
    const img = this.elementIndexe('img', a)
    img.setAttribute('src', this.url + 'xcas/img/' + src)
    img.onclick = function () {
      xcasElement(this)[fonction](param)
    }
    // modif pour fabrique
    // img.setAttribute("onclick","xcasElement(this)."+fonction+"("+param+")");
    a.appendChild(img)
  }

  cell_max (cellule) {
    const t = this.coordonnees(cellule)
    const A = 'A'
    this.nb_col_sauv = this.nb_col
    this.nb_lign_sauv = this.nb_lign
    this.nb_lign = Math.max(this.nb_lign, t[1] + 1)
    this.nb_col = Math.max(this.nb_col, Number(t[0].charCodeAt(0)) - A.charCodeAt(0) + 1)
  }

  fusion () {
    let cell = document.getElementById('xc' + this.cellActive)
    if (this.zone.length == 0) {
      if (!cell.hasAttribute('groupe')) {
        return j3pShowError('Sélectionner d’abord des cellules à fusionner')
      }

      const coords = this.coordonnees(this.cellActive)
      if (cell.hasAttribute('rowspan')) {
        cell.removeAttribute('rowspan')
        for (let i = 0; i < this.n_lign; i++) {
          cell = document.getElementById('xc' + coords[0] + i)
          if (cell.getAttribute('groupe') == this.cellActive) {
            cell.removeAttribute('groupe')
            cell.style.display = 'table-cell'
          }
        }
        return
      }
      if (cell.hasAttribute('colspan')) {
        cell.removeAttribute('colspan')
        for (let i = 0; i < this.n_col; i++) {
          cell = document.getElementById(this.lettres[i] + coords[1])
          if (cell.getAttribute('groupe') == this.cellActive) {
            cell.removeAttribute('groupe')
            cell.style.display = 'table-cell'
          }
        }
        return
      }
    }
    if (this.zone[0] != this.zone[2] && this.zone[1] != this.zone[3]) return
    if (this.zone[1] == this.n_lign - 1 && this.zone[3] == this.n_lign - 1) {
      j3pShowError('Pas de fusion sur la dernière ligne.')
      return
    }
    const id = this.zone[0] + this.zone[1]
    cell = document.getElementById('xc' + id)
    cell.setAttribute('groupe', id)
    if (this.zone[0] == this.zone[2]) {
      cell.setAttribute('rowspan', this.zone[3] - this.zone[1] + 1)
      for (let i = this.zone[1] + 1; i <= this.zone[3]; i++) {
        cell = document.getElementById('xc' + this.zone[0] + i)
        cell.style.display = 'none'
        cell.setAttribute('groupe', id)
      }
    } else {
      const cDebut = this.indexLettre(this.zone[0])
      const cFin = this.indexLettre(this.zone[2])
      cell.setAttribute('colspan', cFin - cDebut + 1)
      for (let i = cDebut + 1; i <= cFin; i++) {
        cell = document.getElementById(this.lettres[i] + this.zone[1])
        cell.style.display = 'none'
        cell.setAttribute('groupe', id)
      }
    }
    this.zoneCode = ''
  }

  indexLettre (c) {
    for (var i = 0; i < this.lettres.length; i++) {
      if (this.lettres[i] == 'xc' + c) { break }
    }
    return i
  }

  ctrl (ev) {
    if (ev.keyCode == XcasBase.ENTREE) {
      // this.entree.select();  utile ?
      this.colorieZone(this.zone[0], this.zone[2], this.zone[1], this.zone[3], 'white')
      this.fill_cell(false)
      delete (this.entree.sauvegarde)
    }

    // deplacement avec les fleches
    const cellule = document.getElementById('xc' + this.cellActive)
    let x = 0; let y = 0
    if (cellule.hasAttribute('colspan')) { x = parseInt(cellule.getAttribute('colspan')) - 1 }
    if (cellule.hasAttribute('rowspan')) { y = parseInt(cellule.getAttribute('rowspan')) - 1 }
    const t = this.coordonnees(this.cellActive)
    const i = this.indexLettre(t[0])
    this.saisie = false
    switch (ev.keyCode) {
      case XcasBase.FLECHE_HAUTE :
        this.cell(t[0] + Math.max(t[1] - 1, 0))
        break
      case XcasBase.FLECHE_BASSE :
        this.cell(t[0] + Math.min(t[1] + 1 + y, this.n_lign - 1))
        break
      case XcasBase.TABULATION :
        ev.preventDefault()
        this.cell(this.lettres[Math.min(i + 1 + x, this.n_col - 1)] + t[1])
        break
      case XcasBase.FLECHE_GAUCHE :
        if (this.entree.selectionStart == 0) {
          this.cell(this.lettres[Math.max(i - 1, 0)] + t[1])
          this.entree.setSelectionRange(0, 0)
        }
        break
      case XcasBase.FLECHE_DROITE :
        if (this.entree.selectionEnd == this.entree.value.length) { this.cell(this.lettres[Math.min(i + 1 + x, this.n_col - 1)] + t[1]) }
        break
    }
  }

  // touche sur this.entree (veiller au focus !)
  modeSaisie (ev) {
    // touche ctrl
    if (ev.ctrlKey) {
      if (ev.keyCode == 67) {
        this.copy()
        this.saisie = false
      } else if (ev.keyCode == 86) {
        this.paste()
      }
      return
    }
    if (ev.keyCode == 27 || ev.keycode == 8) {
      this.debutZone = this.zone[0] + this.zone[1]
      if (this.zone) {
        this.colorieZone(this.zone[0], this.zone[2], this.zone[1], this.zone[3], 'white')
        this.zone = []
      }
      return
    }
    if (ev.keyCode == 13) { // entree
      this.saisie = false
      return
    }
    this.changeEntree = true
    if (this.entree.value.charAt(0) != '=') { return }
    if (ev.keyCode != 17) {
      this.saisie = true
      this.zoneCode = ''
    }
  }

  vider () {
    this.vide(true)
  }

  vide (sansConfirm) {
    if (!sansConfirm) {
      if (this.zone.length == 0) {
        // eslint-disable-next-line no-alert
        if (!confirm('Vider la feuille ?')) { return }
      } else {
        // eslint-disable-next-line no-alert
        if (!confirm('vider la zone ' + this.zone[0] + this.zone[1] + ':' + this.zone[2] + this.zone[3] + ' ?')) { return }
      }
    }
    let iDebut = 0; let iFin = this.nb_col - 1
    let jDebut = 0; let jFin = this.nb_lign - 1
    this.cell_marquees = []
    if (this.zone.length > 0) {
      iDebut = this.indexLettre(this.zone[0])
      jDebut = parseInt(this.zone[1])
      iFin = this.indexLettre(this.zone[2])
      jFin = parseInt(this.zone[3])
    }
    for (let i = iDebut; i <= iFin; i++) {
      for (let j = jDebut; j <= jFin; j++) {
        const cell = this.lettres[i] + j
        const domCellule = document.getElementById(cell)
        domCellule.setAttribute('ancienne', domCellule.getAttribute('formule'))
        domCellule.setAttribute('formule', '')
        this.cell_marquees.push(cell)
        if (domCellule.firstChild) { domCellule.removeChild(domCellule.firstChild) }
      }
    }
    if (this.zone.length == 0) {
      this.nb_col = 0
      this.nb_lign = 0
      this.entree.value = ''
      this.cell('A0')
    } else {
      this.colorieZone(this.zone[0], this.zone[2], this.zone[1], this.zone[3], 'white')
      this.zone = []
    }
  }

  annule () {
    if (this.entree.sauvegarde) {
      this.entree.value = this.entree.sauvegarde
      this.entree.focus()
      return
    }
    let i, t, domCellule
    j3pShowError(this.cell_marquees)
    for (i = 0; i < this.cell_marquees.length; i++) {
      domCellule = document.getElementById('xc' + this.cell_marquees[i])
      t = domCellule.getAttribute('ancienne')
      domCellule.setAttribute('ancienne', domCellule.getAttribute('formule'))
      domCellule.setAttribute('formule', t)
    }
    this.calculeTab()
    this.nb_lign = this.nb_lign_sauv
    this.nb_col = this.nb_col_sauv
  }

  subst_cell () {
    const domCellule = document.getElementById('xc' + this.cellActive)
    domCellule.setAttribute('ancienne', domCellule.getAttribute('formule'))
    domCellule.setAttribute('formule', this.entree.value)
  }

  fill_cell (statique) {
    if (this.changeEntree) { this.changeEntree = false }
    this.cell_marquees = [this.cellActive]
    this.cell_max(this.cellActive)
    this.subst_cell()
    const t = this.coordonnees(this.cellActive)
    this.calculeTab()
    if (statique) { return }
    if (t[1] < this.n_lign - 1) {
      t[1] += 1
      this.saisie = false
      this.cell(t[0] + t[1])
    }
  }

  remplit_cellule (cellule, formule) {
    let cell
    if (cellule.charAt(0) == 'x') { cell = document.getElementById(cellule) } else { cell = document.getElementById('xc' + cellule) }
    cell.setAttribute('formule', formule)
    this.cell_max(cellule)
  }

  colorieZone (c1, c2, li1, li2, color) {
    c1 = 'xc' + c1
    c2 = 'xc' + c2
    for (let i = 0; i < this.cellZone.length; i++) {
      if (this.cellZone[i].style) { this.cellZone[i].style.backgroundColor = 'white' }
    }
    this.cellZone = []
    let i1, i2
    for (let i = 0; i < this.lettres.length; i++) {
      if (this.lettres[i] == c1) { i1 = i }
      if (this.lettres[i] == c2) { i2 = i }
    }
    const iMin = Math.min(i1, i2)
    i2 = Math.max(i1, i2)
    for (let i = iMin; i <= i2; i++) {
      const col = this.lettres[i]
      for (let j = li1; j <= li2; j++) {
        const td = document.getElementById(col + j)
        if (!td.style.backgroundColor || td.style.backgroundColor == 'white') {
          this.cellZone.push(td)
          td.style.backgroundColor = color
        }
      }
    }
  }

  mouseDown (evt, td) {
    // if (this.cellActive!=td.id) inutile ?
    // evt.preventDefault();
    evt.stopPropagation()
    if (this.debutZone == 'copy') { return false }
    if (this.zone) {
      this.colorieZone(this.zone[0], this.zone[2], this.zone[1], this.zone[3], 'white')
      this.zone = []
    } else { this.colorieZone(this.zone[0], this.zone[2], this.zone[1], this.zone[3], 'white') }
    this.zone = this.coordonnees(td.id)
    this.zone.push('')
    this.zone.push('')
    this.debutZone = td.id
    return false
  }

  mouseOver (evt, td) {
    evt.preventDefault()
    if (this.cellDragStart != '') { return false }
    if (this.debutZone == '' || this.debutZone == 'copy') { return false }
    const ends = this.coordonnees(td.id)
    this.zone[2] = ends[0]
    this.zone[3] = ends[1]
    this.colorieZone(this.zone[0], ends[0], this.zone[1], ends[1], '#ffffe0')
    this.zoneCode = this.debutZone.substr(2) + ':' + td.id.substr(2)
    return false
  }

  mouseUp (evt, td) {
    function permute (obj, i, j) {
      const tampon = obj.zone[i]
      obj.zone[i] = obj.zone[j]
      obj.zone[j] = tampon
    }
    if (this.zone[1] > this.zone[3]) { permute(this, 1, 3) }
    if (this.indexLettre(this.zone[0]) > this.indexLettre(this.zone[2])) { permute(this, 0, 2) }
    this.entree.focus()
    if (this.debutZone == '') { return false }
    if (this.debutZone != 'copy') { this.debutZone = '' }
    if (this.saisie) { this.insereDansEntree(this.zoneCode) }
    return false
  }

  dragStart (evt, img) {
    this.cellDragStart = img.parentNode.parentNode.id
    if (document.body.style.overflow != '') { document.body.saveOverflow = document.body.style.overflow } else { document.body.saveOverflow = 'visible' }
  }

  dragEnter (evt, td) {
    const starts = this.coordonnees(this.cellDragStart)
    const ends = this.coordonnees(td.id)
    this.colorDrag(starts, ends, '#ffffac')
    this.poignee(document.getElementById('xc' + this.cellDragEnd))
    document.body.style.overflow = 'hidden'
    return false
  }

  colorDrag (starts, ends, color) {
    const x = ends[0].charCodeAt(0) - starts[0].charCodeAt(0)
    const y = ends[1] - starts[1]
    const n = ends[1] + 1
    window.status = ends[0] + n + ' ' + this.n_lign
    if (n < this.n_lign) {
      const c = document.getElementById('xc' + ends[0] + n)
      if (c.scrollIntoView) { c.scrollIntoView(false) }
    }
    if (x < y) {
      this.cellDragEnd = starts[0] + ends[1]
      this.colorieZone(starts[0], starts[0], starts[1], ends[1], color)
    } else {
      this.cellDragEnd = ends[0] + starts[1]
      this.colorieZone(starts[0], ends[0], starts[1], starts[1], color)
    }
  }

  dragEnd () {
    const starts = this.coordonnees(this.cellDragStart)
    const ends = this.coordonnees(this.cellDragEnd)
    const st = this.cellDragStart
    this.cellDragStart = ''
    this.cellDragEnd = ''
    document.body.style.overflow = document.body.saveOverflow
    this.colorDrag(starts, ends, 'white')
    if (starts[1] == ends[1]) { this.fill_right(ends[0]) } else { this.fill_bottom(ends[1]) }
    this.cell(st)
  }

  fill_bottom (nFin) {
    if (!nFin) {
      nFin = this.n_lign
    } else {
      nFin += 1
    }
    this.cell_marquees = []
    this.subst_cell()
    let cell = document.getElementById('xc' + this.cellActive)
    let formule = cell.getAttribute('formule')
    let nt = 0
    if (formule == parseInt(formule)) {
      formule = '=' + this.cellActive + '+1'
      nt = 1
    }
    const letter = this.cellActive.match(regLettre)
    const nombre = Number(this.cellActive.match(regNombre))
    let i
    for (i = nombre + 1; i < nFin; i++) {
      this.cell_marquees.push(letter + i)
      cell = document.getElementById('xc' + letter + i)
      cell.setAttribute('formule', this.translation(formule, 0, i - nombre - nt))
    }
    i = nFin - 1
    this.cell_max(letter + i)
    this.calculeTab()
    this.saisie = false
  }

  fill_right (lfin) {
    this.cell_marquees = []
    this.subst_cell()
    let cell = document.getElementById('xc' + this.cellActive)
    let formule = cell.getAttribute('formule')
    let nt = 0
    if (formule == parseInt(formule)) {
      formule = '=' + this.cellActive + '+1'
      nt = 1
    }
    cell = document.getElementById('xc' + this.cellActive)
    let letter = String(this.cellActive.match(regLettre))
    const code = Number(letter.charCodeAt(0))
    let nombre = Number(this.cellActive.match(regNombre))
    let fin
    if (lfin) { fin = lfin.charCodeAt(0) - code + 1 } else { fin = this.n_col }//= this.lettres[this.n_col-1].charCodeAt(0)-code+1;
    for (let i = 1; i < fin; i++) {
      letter = String.fromCharCode(code + i)
      this.cell_marquees.push(letter + nombre)
      cell = document.getElementById('xc' + letter + nombre)
      cell.setAttribute('formule', this.translation(formule, i - nt, 0))
    }
    if (nombre < this.n_lign - 1) { nombre++ }
    this.cell_max(this.lettres[this.n_col - 1].slice(2) + nombre)
    this.calculeTab()
    this.saisie = false
  }

  // translation de coordonnees de cellules

  translation (chaine, k, l) {
    const chiffres = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    let i = 0
    let absCol = false
    let refCol = -1
    let nouvelleChaine = ''
    let lettre, j, refLi
    while (i < chaine.length) {
      lettre = chaine.charAt(i)
      if (lettre == '$') {
        absCol = true
        nouvelleChaine += lettre
        i++
      } else {
        if (refCol == -1) {
          for (j = 0; j < this.lettres.length; j++) {
            if (lettre == this.lettres[j].slice(2)) {
              refCol = j
              break
            }
          }
        }
        if (refCol == -1) {
          nouvelleChaine += lettre
          i++
        } else {
          refLi = 0
          i++
          lettre = chaine.charAt(i)
          let absLi = false
          if (lettre == '$') {
            absLi = true
            i++
            lettre = chaine.charAt(i)
          }
          j = i
          while (lettre in chiffres) {
            refLi = 10 * refLi + parseInt(lettre)
            i++
            lettre = (i < chaine.length) ? chaine.charAt(i) : ''
          }
          if (j != i) {
            if (!absCol) { refCol = parseInt(refCol) + k }
            if (!absLi) {
              refLi += l
            } else {
              refLi = '$' + refLi
            }
            nouvelleChaine += this.lettres[refCol].slice(2) + refLi
          } else {
            nouvelleChaine += lettre
          }
          refCol = -1
          absCol = false
          absLi = false
        }
      }
    }
    return nouvelleChaine
  }

  copy () {
    if (this.zone.length != 0) {
      this.debutZone = 'copy'
      this.colorieZone(this.zone[0], this.zone[2], this.zone[1], this.zone[3], 'orange')
      return
    }
    this.ref_copy = this.cellActive
    const cellule = document.getElementById('xc' + this.cellActive)
    this.form_copy = this.entree.value
    cellule.setAttribute('formule', this.entree.value)
  }

  paste () {
    if (this.zone.length == 0) {
      this.cell_marquees = [this.cellActive]
      this.cell_max(this.cellActive)
      const s = copyPaste(this, this.ref_copy, this.cellActive, this.form_copy)
      this.entree.value = s
    } else {
      // this.debutZone="";
      const c0 = this.cellZone[0].getAttribute('id')
      const tDZ = this.coordonnees(c0)
      const tCA = this.coordonnees(this.cellActive)
      const colDZ = this.indexLettre(tDZ[0])
      const colCA = this.indexLettre(tCA[0])
      let cell, id
      for (let i = 0; i < this.cellZone.length; i++) {
        id = this.cellZone[i].getAttribute('id')
        cell = coord(this, colDZ, tDZ[1], colCA, tCA[1], id)
        copyPaste(this, id, cell)
        this.cell_max(cell)
      }
    }
    this.calculeTab()
    this.cell(this.cellActive)
    function coord (obj, colDZ, liDZ, colCA, liCA, cell) {
      const t = obj.coordonnees(cell)
      let col = obj.indexLettre(t[0])
      col = colCA + col - colDZ
      const li = liCA + t[1] - liDZ
      return obj.lettres[col] + li
    }
    function copyPaste (obj, cell1, cell2, form_copy) {
      const lettre_cell = String(cell2.match(regLettre))
      const lettre_ref = String(cell1.match(regLettre))
      const nombre_cell = Number(cell2.match(regNombre))
      const nombre_ref = Number(cell1.match(regNombre))
      let cellule
      if (cell2.charAt(0) == 'x') {
        cellule = document.getElementById(cell2)
      } else {
        cellule = document.getElementById('xc' + cell2)
      }
      if (!form_copy) {
        form_copy = document.getElementById(cell1).getAttribute('formule')
      }
      const c = obj.translation(form_copy, lettre_cell.charCodeAt(0) - lettre_ref.charCodeAt(0), nombre_cell - nombre_ref)
      cellule.setAttribute('formule', c)
      return c
    }
  }

  spreadForm (i, j) {
    const cell = document.getElementById(this.lettres[j] + i)
    const f = cell.getAttribute('formule')
    if (f != '' && f != ' ') { return f } else { return 'null' }
    // return "0";
  }

  cellSubst (cell, s, m) {
    const result = cell.getAttribute('result')
    const f = cell.getAttribute('formule')
    this.sauvegarde += f + '`'
    if (result + '' == s && result != '0') { return }
    cell.setAttribute('result', s)
    if (cell.firstChild) { cell.removeChild(cell.firstChild) }
    if (f != '' && f != ' ') {
      this.insereMathML(cell, m, false, false)
    }
  }

  spreadSubst (i, j, s, m) {
    const cell = document.getElementById(this.lettres[j] + i)
    this.cellSubst(cell, s, m)
  }

  actualiseTab () {
    const t = this.retourGiac.split(/`/g)
    XcasBase.session_id = t[0]
    let i, j, k, id
    const A = 'A'
    let s = "<table><thead id='xcthead'><tr><th class='col1'></th>"
    for (j = 0; j < this.n_col; j++) {
      s += '<th>' + String.fromCharCode(A.charCodeAt(0) + j) + '</th>'
    }
    s += "</tr></thead>\n<tbody id='xctbody'>"

    // var s="";
    this.sauvegarde = '' + this.nb_lign + '£`' + this.nb_col + '£`'
    let cell = document.getElementById('xcA0')
    for (i = 0; i < this.n_lign; i++) {
      s += "<tr>\n<td class='col1'>" + i + '</td>'
      for (j = 0; j < this.n_col; j++) {
        id = this.lettres[j] + i
        s += "<td id='" + id + "' "
        s += imite(cell)
        s += "onclick='xcasElement(this).cell(this.id)' "
        s += "onmousedown='xcasElement(this).mouseDown(event,this)' "
        s += "onmouseover='xcasElement(this).mouseOver(event,this)' "
        s += "onmouseup='xcasElement(this).mouseUp(event,this)' "
        s += "ondragenter='xcasElement(this).dragEnter(event,this)'"
        if (i < this.nb_lign && j < this.nb_col) {
          k = 2 * (i * this.nb_col + j) + 1
          if (t[k] != 'null') {
            s += "result='" + t[k] + "'><math display='inline'>" + t[k + 1] + '</math>'
          }
          this.sauvegarde += cell.getAttribute('formule') + '`'
        } else { s += '>' }
        s += '</td>'
        cell = nextCell(cell)
      }
      s += '\n</tr>'
    }
    s += '</tbody>\n</table>\n'
    // this.tbody.innerHTML=s;
    this.tableau.innerHTML = s
    this.thead = document.getElementById('xcthead')
    this.tbody = document.getElementById('xctbody')
    for (i = 0; i < this.cellZone.length; i++) { this.cellZone[i] = document.getElementById(this.cellZone[i].id) }
    if (!window.ActiveXObject) {
      setTimeout(() => window.xcasTableurActif.dimTable(), 1000)
    }
    function imite (cell) {
      let s = ''
      const attr = ['index', 'style', 'rowspan', 'colspan', 'formule', 'groupe']
      for (let i = 0; i < attr.length; i++) {
        if (cell.hasAttribute(attr[i])) { s += attr[i] + "='" + cell.getAttribute(attr[i]) + "' " }
      }
      return s
    }
    function nextCell (c) {
      // on passe a la cellule suivante
      let cNext = successeur(c)
      if (cNext == null || !cNext || !cNext.tagName) {
        // on est en fin de ligne
        cNext = c.parentNode
        cNext = successeur(cNext)
        if (cNext) {
          cNext = cNext.firstChild
          if (!cNext.tagName) { cNext = successeur(cNext) }
          cNext = successeur(cNext)
        }
      }
      return cNext
    }
    function successeur (c) {
      let cNext = c.nextSibling
      while (cNext && !cNext.tagName) { cNext = cNext.nextSibling }
      if (cNext) { return cNext }
    }
  }

  dimTable () {
    // largeur de la barre de défilement
    const l = this.tbody.offsetWidth - this.tbody.clientWidth
    if (l == 0) { return }
    if (!this.hmax.isNaN) {
      if (this.hmax < 100) { this.hmax = 100 }
      if (this.hmax > 1500) { this.hmax = 1500 }
      this.tableau.parentNode.style.maxHeight = this.hmax + 21 + 'px'
      this.tbody.style.maxHeight = this.hmax + 'px'
    }

    // this.tbody.style.maxHeight=this.hmax+"px";

    let tr = this.thead.firstChild
    while (!tr.tagName) { tr = tr.nextSibling }
    const ths = tr.childNodes
    let largCell = 0
    let cellule
    const m = this.n_lign - 1
    for (let i = 1; i <= this.n_col; i++) {
      cellule = document.getElementById(this.lettres[i - 1] + m)
      cellule.style.width = ''
    }
    for (let i = 1; i <= this.n_col; i++) {
      cellule = document.getElementById(this.lettres[i - 1] + m)
      largCell = cellule.offsetWidth
      // correction empirique
      if (largCell == parseInt(cellule.style.width) + 1) { largCell = largCell - 1 }
      cellule.style.width = largCell + 'px'
    }
    this.thead.className = 'bloc'
    this.tbody.className = 'bloc'
    const conteneur = this.tableau.parentNode
    conteneur.style.overflowY = 'hidden'
    for (let i = 1; i <= this.n_col; i++) {
      cellule = document.getElementById(this.lettres[i - 1] + m)
      largCell = cellule.offsetWidth
      // correction empirique
      if (largCell == parseInt(cellule.style.width) + 1) { largCell = largCell - 1 }
      ths[i].style.minWidth = largCell - 1 + 'px'
      ths[i].style.width = largCell + 'px'
    }
    if (ths.length <= this.n_col + 1) {
      const th = document.createElement('th')
      ths[ths.length - 1].parentNode.appendChild(th)
      th.style.minWidth = l + 'px'
      th.style.color = 'red'
    }
  }

  spreadsheet () {
    let spreadsheet = 'spreadsheet['
    let i = 0
    let j = 0
    const der_col = this.nb_col - 1; const der_lign = this.nb_lign - 1
    let formule = ''
    for (i = 0; i < this.nb_lign; i++) {
      spreadsheet += '['
      for (j = 0; j < der_col; j++) {
        formule = this.spreadForm(i, j)
        spreadsheet += '[regrouper(' + formule + '),1,0],'
      }
      formule = this.spreadForm(i, j)
      spreadsheet += '[regrouper(' + formule + '),1,0]]'
      if (i == der_lign) {
        spreadsheet += ']'
      } else {
        spreadsheet += ','
      }
    }
    return spreadsheet
  }

  calculeCache (str) {
    let spreadsheet
    if (!str) { spreadsheet = this.spreadsheet() } else { spreadsheet = str }
    if (spreadsheet.length > 10000) { this.progress.setAttribute('style', 'display:block') }
    let s
    if (this.exact) { s = this.reqTableur(spreadsheet) } else { s = this.reqTableurApprox(spreadsheet) }
    if (s.indexOf('Fatal error') >= 0) {
      j3pShowError('DUREE EXCESSIVE. CALCUL REFUSE. Passez en mode numerique')
      return
    }
    this.retourGiac = s
    this.progress.setAttribute('style', 'display:none')
  }

  calculeTab (str) {
    this.stopAssist()
    this.calculeCache(str)
    this.actualiseTab()
  }

  restaure () {
    if (this.sauvegarde == '') { return }
    const tab = this.sauvegarde.split(/`/g)
    if (parseInt(tab[0]) > this.n_lign) {
      j3pShowError('Il faut au moins ' + tab[0] + ' lignes.')
      return
    }
    if (parseInt(tab[1]) > this.n_col) {
      j3pShowError('Il faut au moins ' + tab[1] + ' colonnes.')
      return
    }
    let debut = 0
    // pour compatibilite avec l ancien format
    if (tab[0].charAt(tab[0].length - 1) == '£') {
      this.nb_lign = parseInt(tab[0])
      this.nb_col = parseInt(tab[1])
      debut = 2
    }
    for (let i = 0; i < this.nb_lign; i++) {
      for (let j = 0; j < this.nb_col; j++) {
        const cell = document.getElementById(this.lettres[j] + i)
        const k = debut + i * this.nb_col + j
        if (tab[k]) {
          cell.setAttribute('formule', tab[k])
        } else {
          cell.setAttribute('formule', '')
        }
      }
    }
    this.calculeTab()
  }

  // preferences utilisateur

  pref (bool, x, y) {
    window.xcasTableurActif = this
    const p = (x) ? new XcasPopUp(x, y, 500) : new XcasPopUp(200, 100, 500)
    p.titre.innerHTML = 'Préférences'
    const div = this.outils.parentNode
    let w = div.style.width
    if (!div.style.width) { w = '900px' }
    let approx = "checked='checked'"
    if (window.xcasTableurActif.exact) { approx = '' }
    let compl = "checked='checked'"
    let cNouveau = 0
    p.contenu.innerHTML = "<p>Largeur : <input value='" + w +
      "' onchange='window.xcasTableurActif.larg(this)'><br><br>" +
      "Hauteur max : <input value='" + this.hmax +
      "px' onchange='window.xcasTableurActif.hmax=parseInt(this.value);window.xcasTableurActif.actualiseTab();'><br>" +
      "<br>Nombre de lignes : <input value='" +
      this.n_lign +
      "' onchange='window.xcasTableurActif.redim(this,0)'></p><p>Nombre de colonnes (1-15): <input value='" +
      this.n_col +
      "' onchange='window.xcasTableurActif.redim(this,1)'></p><hr><p>" +
      "Calculs approchés <input type='checkbox' " + approx +
      "' onchange='window.xcasTableurActif.exact=!window.xcasTableurActif.exact'>" +
      " Précision <input width='3' onblur='xcasPrecision(this)' value='" +
      xcasRequete('DIGITS') + "'></p><hr>\n"
    if (xcasRequete('solve(x^2+1=0)') == '[]') {
      compl = ''
      cNouveau = 1
    }
    p.contenu.innerHTML += "<p>Résoudre les équations dans C  <input type='checkbox' " + compl +
      "' onchange='xcasChangeComplexMode(" + cNouveau + ")'></p>"
    // faut mettre ces fcts en global vu le code de goret ci-dessus
    window.xcasPrecision = function xcasPrecision (input) {
      const x = parseInt(input.value)
      if (!isNaN(x)) { xcasRequete('DIGITS:=' + x) }
    }
    window.xcasChangeComplexMode = function xcasChangeComplexMode (mode) {
      xcasRequete('complex_mode:=' + mode)
    }
  }

  larg (input) {
    const div = this.outils.parentNode
    let sauve = '900px'
    if (div.style.width) { sauve = div.style.width }
    div.style.width = input.value
    // eslint-disable-next-line no-alert
    if (!confirm('Confirmer la nouvelle largeur')) {
      div.style.width = sauve
      input.value = sauve
    }
  }

  redim (input, direction) {
    let x = parseInt(input.value)
    if (isNaN(x)) { return }
    const n_lign = this.n_lign
    const n_col = this.n_col
    let div = this.outils.parentNode
    let w = '900px'
    if (div.style.width) { w = div.style.width }
    while (div.firstChild) { div.removeChild(div.firstChild) }
    let tableur
    const s = this.sauvegarde
    if (direction == 0) {
      x = Math.max(x, this.nb_lign)
      tableur = new XcasTableur(div.id, x, n_col)
    } else {
      x = Math.min(x, 15)
      x = Math.max(x, this.nb_col)
      tableur = new XcasTableur(div.id, n_lign, x)
    }
    input.value = x
    div = tableur.outils.parentNode
    div.style.width = w
    tableur.sauvegarde = s
    tableur.restaure()
    x = parseInt(input.parentNode.js.left)
    const y = parseInt(input.parentNode.js.top)
    input.parentNode.js.supprime()
    tableur.pref(true, x, y)
  }

  sauve () {
    const leMarqueur = '@`'
    // eslint-disable-next-line no-alert
    let s = prompt('nom de la feuille ?', this.cle)
    if (!s) return
    this.cle = s

    s = leMarqueur + this.prog + leMarqueur + this.sauvegarde
    try {
      localStorage.setItem('TX' + this.cle, s) // this.sauvegarde);
    } catch (err) {
      console.error(err)
      j3pShowError('fonction non supportée par le navigateur')
    }
  }

  xcasCharge (i) {
    const leMarqueur = '@`'
    const cle = localStorage.key(i)
    const s = localStorage.getItem(cle)
    const t = s.split(leMarqueur)
    window.xcasTableurActif.prog = t[1]
    window.xcasTableurActif.sauvegarde = t[2]
    //  window.xcasTableurActif.sauvegarde=localStorage.getItem(cle);
    window.xcasTableurActif.restaure()
  }

  xcasSupprime (i) {
    const cle = localStorage.key(i)
    // eslint-disable-next-line no-alert
    if (confirm('supprimer ' + cle + ' ?')) {
      localStorage.removeItem(cle)
      this.xcasPopup?.supprime()
      window.xcasTableurActif.ouvrir()
    }
  }

  ouvrir (popup) {
    function testeLS () {
      if (localStorage) {
        return true
      } else {
        j3pShowError('Vous ne pourrez pas stocker les données ! Mettre à jour votre navigateur !')
        return false
      }
    }

    function bouton (valeur, effet) {
      const input = document.createElement('input')
      input.type = 'button'
      input.setAttribute('onclick', effet)
      input.value = valeur
      return input
    }

    function ligne (table) {
      const tr = document.createElement('tr')
      table.appendChild(tr)
      for (let i = 1; i < arguments.length; i++) {
        const td = document.createElement('td')
        tr.appendChild(td)
        td.appendChild(arguments[i])
      }
    }

    if (!testeLS()) { return }
    const n = localStorage.length
    if (n == 0) {
      j3pShowError('Aucune feuille stockée')
      return
    }
    window.xcasTableurActif = this
    this.xcasPopup = new XcasPopUp(400, 150)
    this.xcasPopup.titre.innerHTML = 'Choisir une feuille'
    const p = this.xcasPopup.contenu
    p.innerHTML = ''
    const table = document.createElement('table')
    p.appendChild(table)
    let b1, b2
    for (let i = 0; i < n; i++) {
      let cle = localStorage.key(i)
      if (cle.substring(0, 2) == 'TX') {
        cle = cle.substring(2)
        const texte = document.createTextNode(cle)
        b1 = bouton('Charger', 'xcasCharge(' + i + ');')
        b2 = bouton('supprimer', 'xcasSupprime(' + i + ');')
        ligne(table, texte, b1, b2)
      }
    }
  }

  retour (quoi, col, li) {
    if (!isNaN(col)) { col = this.lettres[col] } else { col = 'xc' + col }
    const c = document.getElementById(col + li)
    if (quoi == 'f') { return c.getAttribute('formule') } else { return c.getAttribute('result') }
  }

  formule (col, li) {
    return this.retour('f', col, li)
  }

  resultat (col, li) {
    return this.retour('r', col, li)
  }

  // ardoise

  afficheArdoise () {
    const popup = new XcasPopUp(400, 60, 350)
    popup.titre.innerHTML = 'Ardoise xcas'
    return new XcasConsole(popup.contenu)
  }

  // cadre de programmation

  afficheProg () {
    const popup = new XcasPopUp(400, 60, 350)
    popup.titre.innerHTML = 'Fenêtre de programmation'
    const t = document.createElement('textarea')
    t.value = this.prog
    popup.contenu.appendChild(t)
    t.onblur = function () { window.xcasTableurActif.prog = this.value; xcasRequete(this.value) }
  }

  // aide

  aide () {
    const p = new XcasPopUp(300, 80, 500)
    p.titre.innerHTML = 'Aide tableur'
    p.contenu.innerHTML = "<object  data='" + this.url + "xcas/tableur.html' type='text/html'></object>"
  }

  // gestion de l’assistant

  assiste (sel) {
    if (!this.saisie) {
      if (this.entree.value != '') {
        j3pShowError('Une formule doit commencer par =')
        sel.selectedIndex = 0
        sel.blur()
        return
      } else {
        this.entree.value = '='
        this.saisie = true
      }
    }
    const i = sel.selectedIndex
    if (i == 0) { return }
    const t = this.commandes[i]
    if (t.charAt) { return }
    this.spanAssist.innerHTML = ''
    let s = ' '// t[0];
    /*
    if (t[1]=="linsolve"){
      this.combi(this.spanAssist,"xcarg",t[2],true);
    } else
    if (t[1]=="matrice"){
      this.combi(this.spanAssist,"xcarg",t[2],false);
    } else
  */
    if (t[1] == 'editMatrice') {
      this.editeurMulti('matrice', '1 2 3\n3 4 5\n6 7 8')
    } else
      if (t[1] == 'editSysteme') {
        this.editeurMulti('systeme', 'x y\nx+y=3\nx-y=1')
      } else {
        for (let j = 2; j < t.length; j++) {
          if (t[j].charAt) { s += ' ' + t[j] + " <input id='xcarg" + j + "' onfocus='window.xcasTableurActif.entree=this'>" } else { s += ' ' + t[j][0] + " <input value='" + t[j][1] + "' id='xcarg" + j + "' onfocus='window.xcasTableurActif.entree=this'>" }
        }
        this.spanAssist.innerHTML += s
      }
    const input = this.bouton(this.spanAssist, 'ok')
    input.className = 'ok'
    input.onclick = function () { window.xcasTableurActif.comAssist(i) }
    if (document.getElementById('xcarg2')) { document.getElementById('xcarg2').focus() }
  }

  // Edition de matrice ou de systemes

  editeurMulti (type, contenu) {
    const titres = { matrice: 'la matrice', systeme: 'le système' }
    const popup = new XcasPopUp(400, 10, 350, 200)
    popup.titre.innerHTML = 'Entrez ' + titres[type] + ' comme ci-dessous :'
    const t = document.createElement('textarea')
    t.value = contenu
    t.style.minHeight = '170px'
    popup.contenu.appendChild(t)
    t.focus()
    // t.onblur=function(){window.xcasTableurActif.faitMatSys(type,this.value);};
    const ok = document.createElement('input')
    ok.setAttribute('type', 'button')
    ok.setAttribute('value', 'Ecrire')
    ok.setAttribute('style', 'position:absolute; bottom:10px; right:80px')
    ok.onclick = function () { window.xcasTableurActif.faitMatSys(type, this.previousSibling.value); this.parentNode.js.supprime() }
    popup.contenu.appendChild(ok)
    const annul = document.createElement('input')
    annul.setAttribute('type', 'button')
    annul.setAttribute('value', 'Annuler')
    annul.setAttribute('style', 'position:absolute; bottom:10px; right:10px')
    annul.onclick = function () { this.parentNode.js.supprime() }
    popup.contenu.appendChild(annul)
  }

  faitMatSys (type, contenu) {
    function remplaceBlancs (s1) {
      // enleve les blancs inutiles
      s1 = s1.replace(/[\s]{2,}/g, ' ')
      s1 = s1.replace(/^[\s]/, '')
      s1 = s1.replace(/[\s]$/, '')
      // remplace les blancs par des virgules
      s1 = s1.replace(' ', ',', 'g')
      return s1
    }
    const t = contenu.split('\n')// new RegExp("\n","g"));
    let i, s, s1
    if (type == 'matrice') {
      s = '['
      for (i = 0; i < t.length; i++) {
        s1 = remplaceBlancs(t[i])
        if (s1 != '') {
          if (i > 0) {
            s += ','
          }
          s += '[' + s1 + ']'
        }
      }
      s += ']'
    }
    if (type == 'systeme') {
      s = 'linsolve(['
      s1 = remplaceBlancs(t[0])
      for (i = 1; i < t.length; i++) {
        if (i > 1) { s += ',' }
        s += t[i].replace(/ /g, '')
      }
      s += '],[' + s1 + '])'
    }
    this.entree = this.entreeSauve
    this.entree.value += s
    this.stopAssist()
  }

  comAssist (index) {
    function li2matrix (li) {
      let li1 = ''; let s = ''
      // elimination des blancs
      let b = false
      for (let j = 0; j < li.length; j++) {
        if (li.charAt(j) != ' ') { b = false }
        if (!b) { li1 += li.charAt(j) }
        if (li.charAt(j) == ' ') { b = true }
      }
      if (li1 != '') { li = li1.split(' ') } else { li = [] }
      if (li.length > 0) {
        s += '[' + li[0]
        for (let j = 1; j < li.length; j++) {
          if (li[j] != '') { s += ',' + li[j] }
        }
        s += ']'
      }
      return s
    }

    const t = this.commandes[index]
    let s = ''
    let sprim = ''
    if (t[1] == 'zone2matrice') {
      s = '['
      const cDebut = this.indexLettre(this.zone[0])
      const cFin = this.indexLettre(this.zone[2])
      for (let i = this.zone[1]; i <= this.zone[3]; i++) {
        s += '['
        for (let j = cDebut; j <= cFin; j++) {
          s += this.lettres[j].substring(2) + i
          if (j < cFin) { s += ',' } else { s += ']' }
        }
        if (i < this.zone[3]) { s += ',' } else { s += ']' }
      }
    } else if (t[1] == 'linsolve') {
      s = 'linsolve(['
      sprim = ''
      for (let i = 0; i < 4; i++) {
        sprim = document.getElementById('xcarg' + i).value
        sprim = sprim.replace(/ /g, '')
        if (sprim != '') {
          if (i != 0) { s += ',' }
          s += sprim
        }
      }
      s += '],' + li2matrix(document.getElementById('xcarg4').value) + ')'
    } else if (t[1] == 'matrice') {
      let li = ''; let li1 = ''
      s = '['
      for (let i = 0; i < 4; i++) {
        li = document.getElementById('xcarg' + i).value
        li1 = li2matrix(li)
        if (li1 != '') {
          if (i > 0) { s += ',' }
          s += li1
        }
      }
      s += ']'
    } else {
      s = t[1] + '('
      for (let j = 2; j < t.length; j++) {
        if (j > 2) { s += ',' }
        s += document.getElementById('xcarg' + j).value
      }
      s += ')'
    }
    if (t[1] == 'quantile') { s += '[0]' }
    this.entree = this.entreeSauve
    this.entree.value += s
    this.stopAssist()
  }

  // graphiques

  assistGraph () {
    if (this.divAssistGraph.style.display == 'block') {
      this.stopAssist(true)
      return
    }
    this.stopAssist(true)
    this.divAssistGraph.style.display = 'block'
    if (this.exact) {
      // Il faut d’abord recalculer la feuille en mode numerique
      this.exact = false
      this.calculeCache()
      this.exact = true
    }
    this.spanAssistGraph.innerHTML = ''
    const commandes = [
      ['Graphique :', ''],
      ['Aires', 'aire'],
      ['Camembert', 'camembert'],
      ['Courbes', 'courbes'],
      ['Barres', 'barres'],
      ['Barres multiples', 'barresCote'],
      ['Histogramme', 'histogramme'],
      ['Histogramme automatique', 'histauto'],
      ['Nuage de points', 'nuage']
    ]
    const select = document.createElement('select')
    select.setAttribute('id', 'xcgselect')
    select.onchange = function () { document.getElementById('xcgspan').style.display = 'inline'; document.getElementById('xcgplage').select() }
    this.optionSelect(select, commandes)
    this.spanAssistGraph.appendChild(select)
    const span = document.createElement('span')
    span.setAttribute('id', 'xcgspan')
    this.spanAssistGraph.appendChild(span)
    const dia1 = {
      xcgplage: [' Plage de données : ', this.zoneCode]
    }
    const xcgplage = this.dialogue(span, dia1)
    xcgplage.onkeyup = function () { this.value = this.value.toUpperCase() }
    const legendes = [
      ['Titre', 'Titre du graphique'],
      ['Légende des x', 'Titre éventuel abscisses'],
      ['Légende des y', 'Titre éventuel ordonnées']
    ]
    this.combi(span, 'xcgl', legendes)

    const input = this.bouton(span, 'ok')
    input.className = 'ok'
    input.onclick = function () { window.xcasTableurActif.graphique() }

    // capture des clics sur cellule
    this.saisie = true
    this.entree = xcgplage// document.getElementById("xcgplage");
    // xcgplage.focus();
    setTimeout(() => window.xcasTableurActif.entree.focus(), 1000)
    this.include(this.url + 'jsx/jsxgraphcore.js')
    this.include(this.url + 'graphique/graphique.js')
    this.include(this.url + 'graphique/graphiquepopup.js')
  }

  bouton (conteneur, valeur) {
    const input = document.createElement('input')
    input.className = 'ok'
    input.value = valeur
    input.type = 'button'
    conteneur.appendChild(input)
    return input
  }

  combi (conteneur, prefixe, legendes, focus) {
    const select = document.createElement('select')
    select.prefixe = prefixe
    conteneur.appendChild(select)
    let option; let input; let txt = ''
    for (let i = 0; i < legendes.length; i++) {
      option = document.createElement('option')
      txt = document.createTextNode(legendes[i][0])
      option.appendChild(txt)
      select.appendChild(option)
      input = document.createElement('input')
      input.setAttribute('id', prefixe + i)
      if (i == 0) {
        input.style.display = 'inline'
        input.focus()
        select.cible = input
      } else { input.style.display = 'none' }
      input.value = legendes[i][1]
      input.className = 'graph'
      if (focus) { input.onfocus = function () { window.xcasTableurActif.entree = this } }
      conteneur.appendChild(input)
    }
    select.onchange = function () {
      if (this.cible) { this.cible.style.display = 'none' }
      const cible = document.getElementById(this.prefixe + this.selectedIndex)
      if (cible) {
        this.cible = cible
        cible.style.display = 'inline'
        cible.select()
      }
    }

    /* setTimeout("window.xcasTableurActif.selectOpt('"+prefixe+"')",1000); */

    /* var cible0= document.getElementById(prefixe+"0");
    cible0.style.display="inline";
    select.cible=cible0;
    cible0.focus();
    */
  }

  optionSelect (select, opt) {
    let option, txt
    for (let i = 0; i < opt.length; i++) {
      option = document.createElement('option')
      option.value = opt[i][1]
      txt = document.createTextNode(opt[i][0])
      option.appendChild(txt)
      select.appendChild(option)
    }
  }

  dialogue (span, dia, classe) {
    let input, txt
    for (const a in dia) {
      if (dia[a][0] != '') {
        txt = document.createTextNode(dia[a][0])
        span.appendChild(txt)
      }
      input = document.createElement('input')
      input.setAttribute('id', a)
      input.value = dia[a][1]
      if (classe) { input.className = classe }
      span.appendChild(input)
    }
    return input
  }

  /* graphiqueFenetre ???? => on vire cette méthode en attendant
  graphique () {
    let h, i, j, k, l, m
    let s
    // si ivar==1, lecture verticale
    let ivar = 1
    let plage = document.getElementById('xcgplage').value
    // remplace les ; et les blancs par des virgules
    plage = plage.replace(/;/g, ',')
    plage = plage.replace(/ /g, ',')
    const tab = plage.split(',')
    for (i = 0; i < tab.length; i++) { tab[i] = zone(this, tab[i]) }
    if (tab[0][1][0] - tab[0][0][0] > tab[0][1][1] - tab[0][0][1]) { ivar = 0 }
    const jvar = 1 - ivar

    const g = new graphiqueFenetre(10, 20, 500, 400)
    const titre = document.getElementById('xcgl0').value
    if (titre && titre != '') {
      g.dom.titre.innerHTML = titre
    }
    const t = this.retourGiac.split(/`/g)
    g.setStyle({ strokeWidth: 1, points: false, face: 'square', color1: 'red', color2: 'blue' })

    // Cherche la premiere donnee numerique
    // variante qui recherche la premiere donnee num dans n’importe quelle rangee
    // tab[i] zone i
    // tab[i][0]==[numligne,numcol] premiere cellule de la zone i
    // tab[i][1] derniere cellule de la zone i
    let br = false
    for (i = 0; i < tab.length; i++) {
      for (l = tab[i][0][ivar]; l <= tab[i][1][ivar]; l++) {
        for (m = tab[i][0][jvar]; m <= tab[i][1][jvar]; m++) {
          k = (jvar == 0)
            ? cell(this, l, m)
            : cell(this, m, l)
          if (!isNaN(parseFloat(t[k]))) {
            br = true
            break
          }
        }
        if (br) { break }
      }
      if (br) { break }
    }
    // legende des axes
    const select = document.getElementById('xcgselect')
    const gtype = select.options[select.selectedIndex].value
    if (gtype != 'camembert') {
      if (l > tab[0][0][ivar]) {
        if (ivar == 0) { g.setHorizontalTitle(t[cell(this, l - 1, tab[0][0][jvar])]) } else { g.setHorizontalTitle(t[cell(this, tab[0][0][jvar], l - 1)]) }
      } else { g.setHorizontalTitle(document.getElementById('xcgl1').value) }
      g.setVerticalTitle(document.getElementById('xcgl2').value)
    }

    // taille du tableau de donnees
    const xy = []
    for (i = tab[0][0][ivar]; i <= tab[0][1][ivar]; i++) { xy.push([]) }
    // construction du tableau de donnees
    // et de la legende generale
    let legend = {}
    m = 0
    for (h = 0; h < tab.length; h++) {
      for (j = tab[h][0][jvar]; j <= tab[h][1][jvar]; j++) {
        if (m > 0) {
          if (l > 0) {
            if (jvar == 0) { s = t[cell(this, l - 1, j)] } else { s = t[cell(this, j, l - 1)] }
            legend['title' + m] = s
          } else { legend['title' + m] = 'courbe ' + m }
          legend['color' + m] = g.jsxStyle['color' + m]
        }
        for (i = l; i <= tab[h][1][ivar]; i++) {
          if (jvar == 0) { k = cell(this, i, j) } else { k = cell(this, j, i) }
          if (t[k] && t[k] != 'null') {
            // pb : ne pousse que les donnees numeriques
            if (isNaN(parseFloat(t[k]))) { xy[m].push(t[k]) } else { xy[m].push(parseFloat(t[k])) }
          }
        }
        m += 1
      }
    }

    // construction du graphique
    // legende generale
    if (gtype != 'camembert') { g.setLegend(legend) }
    this.stopAssist(true)
    switch (gtype) {
      case 'aire':
        g.aire(0, 0, xy)
        break
      case 'camembert':
        var lgnd = {}
        g.completeListeNumerotee(lgnd, 'title', 1, xy.length, xy[0])
        g.completeListeNumerotee(lgnd, 'color', 1, xy.length, g.pieColors)
        g.setLegend(lgnd)
        g.camembert(0, 0, xy)
        legend = xy[0]
        break
      case 'courbes':
        g.courbe(0, 0, xy)
        break
      case 'barres':
        g.barres(0, 0, xy)
        break
      case 'barresCote':
        g.barresCote(0, 0, xy)
        break
      case 'histogramme':
        g.histogramme(0, 0, xy)
        break
      case 'histauto':
        // bornes inf et sup
        var min = xy[0][0]; var max = min
        for (i = 0; i < xy[0].length; i++) {
          min = Math.min(xy[0][i], min)
          max = Math.max(xy[0][i], max)
        }
        var nclass = Math.min(Math.round(xy[0].length / 5), 9)
        var borne; var bornes = []; var effectifs = []; var amplitude = (max - min) / nclass
        for (i = 0; i < nclass; i++) {
          borne = min + i * amplitude
          bornes.push(borne)
          effectifs.push(0)
        }
        bornes.push(max)
        for (i = 0; i < xy[0].length; i++) {
          j = 0
          while (xy[0][i] > bornes[j]) { j += 1 }
          j = Math.max(0, j - 1)
          effectifs[j] += 1
        }
        g.histogramme(0, 0, [bornes, effectifs])
        break
      case 'nuage':
        g.nuage(0, 0, xy)
        break
      default:
        j3pShowError('Sélectionnez le type de graphique !')
    }
    // ---- fonctions de service -----
    function cell (obj, i, j) {
      return 2 * (i * obj.nb_col + j) + 1
    }
    function numCol (lettre) {
      const A = 'A'
      return lettre.charCodeAt(0) - A.charCodeAt(0)
    }
    function zone (xt, ref) {
      const t = ref.split(':')
      let c0, c1
      if (t.length > 1) {
        // zone de type cellule1:cellule2
        c0 = xt.coordonnees(t[0])
        c1 = xt.coordonnees(t[1])
        c0[0] = numCol(c0[0])
        c1[0] = numCol(c1[0])
      } else {
        // ligne ou colonne
        c0 = xt.coordonnees(ref)
        c1 = xt.coordonnees(ref)
        if (c0[0] == '') {
          // ligne
          c0[0] = 0
          c1[0] = xt.nb_col - 1
        } else {
          // colonne
          c0[0] = numCol(c0[0])
          c0[1] = 0
          c1[0] = numCol(c1[0])
          c1[1] = xt.nb_lign - 1
        }
      }
      return [c0, c1]
    }
  } // graphique
  /* fin pb méthode graphique */
}

// statique
XcasTableur.mini = false
XcasTableur.hauteurMax = 500 // en px
XcasTableur.commandes = [
  ['Choisir ...', '', ''],

  ['Développer', 'developper', 'l’expression'],
  ['Factoriser', 'factoriser', 'l’expression'],
  ['Simplifier', 'simplifier', 'l’expression'],
  ['Forme canonique', 'forme_canonique', 'du trinôme'],
  ['Résolution équation', 'resoudre', '', ['d’inconnue', 'x']],
  ['Résolution  Système', 'editSysteme', 'Editeur de systèmes'],
  ['Editeur de  matrices', 'editMatrice', 'Editeur de matrices'],
  ['Zone \u2192 Matrice', 'zone2matrice', 'Matrice d’après la zone'],

  ['Dériver', 'deriver', 'la fonction', ['de la variable', 'x']],
  ['Intégrer', 'integration', 'la fonction', ['de la variable', 'x'], 'de', 'à'],
  ['Une primitive', 'integration', 'de la fonction', ['de la variable', 'x']],
  ['Limite de', 'limite', 'la fct', ['quand', 'x'], '→'],

  ['Quotient div euclid', 'iquo', 'de', 'par'],
  ['Reste div euclid', 'irem', 'de', 'par'],
  ['Nombre premier ?', 'est_premier', ':'],

  ['Forme algébrique', 'evalc', 'de :'],
  ['Partie réelle', 're', 'de :'],
  ['partie imaginaire', 'im', 'de :'],
  ['Module', 'abs', 'de :'],
  ['Argument', 'arg', 'de :'],

  ['Somme', 'somme', 'Série :'],
  ['Moyenne', 'moyenne', 'Série :'],
  ['Ecart-type', 'ecart_type', 'Série :'],
  ['Minimum', 'min', 'Série :'],
  ['Maximum', 'max', 'Série :'],
  ['Quantile', 'quantile', 'Série :', ['Fréquence (0.25 pour Q1) :', '0.25']],

  ['Entier aléatoire', 'alea', 'compris entre 0 et'],
  ['Réel aléatoire', 'hasard', 'compris entre', 'et'],
  ['Selon loi normale', 'randnorm', 'de moyenne', 'et d’écart-type']
]

export default XcasTableur
