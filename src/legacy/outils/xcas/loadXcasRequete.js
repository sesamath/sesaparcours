/**
 * Charge le code permettant d’interroger xcasenligne.fr, retourne la fct permettant d’évaluer les expressions
 * @return {Promise<function>}
 */
export default async function loadXcasRequete () {
  const { default: XcasBase } = await import('./XcasBase.js')
  const xcasBase = new XcasBase()
  return (expression) => xcasBase.calcule(expression)
}
