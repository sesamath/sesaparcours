// -*- coding: utf-8-unix -*-

//   Copyright (C) 2009 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distribute  in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// inclusion feuille de style
import { j3pShowError } from 'src/legacy/core/functions'

const xcasBaseUrl = 'https://www.xcasenligne.fr/fabrique/'

function xcasInclureStyle (fileName) {
  const link = document.createElement('link')
  link.type = 'text/css'
  link.href = fileName
  link.rel = 'stylesheet'
  const head = document.getElementsByTagName('head')
  if (head[0]) { head[0].appendChild(link) }
}

function XcasBase () {
  this.url = xcasBaseUrl
  this.tabHisto = []
  this.indexTabHisto = 0
}

XcasBase.prototype.initialise = function () {
  this.index = XcasBase.listeElements.length
  XcasBase.listeElements.push(this)
}

// inclusion de programmes javascript
XcasBase.prototype.include = function (fileName) {
  if (!document.getElementsByTagName) {
    return j3pShowError('Chargement des modules impossible')
  }
  const script = document.createElement('script')
  script.type = 'text/javascript'
  script.src = fileName
  const head = document.getElementsByTagName('head')
  if (head[0]) { head[0].appendChild(script) }
}

XcasBase.prototype.includeStyle = function (fileName) {
  xcasInclureStyle(fileName)
}

XcasBase.prototype.fleche = function (ev) {
  if (ev.keyCode != XcasBase.FLECHE_HAUTE && ev.keyCode != XcasBase.FLECHE_BASSE) { return }
  const input = document.getElementById('in')
  if (ev.keyCode == XcasBase.FLECHE_HAUTE && this.indexTabHisto > 0) {
    this.indexTabHisto--
    input.value = this.tabHisto[this.indexTabHisto]
  }
  if (ev.keyCode == XcasBase.FLECHE_BASSE && this.indexTabHisto < this.tabHisto.length) {
    this.indexTabHisto++
    if (this.indexTabHisto < this.tabHisto.length) {
      input.value = this.tabHisto[this.indexTabHisto]
    } else {
      input.value = ''
    }
  }
}

XcasBase.prototype.reqG = function (s, url, option, xx, yy) {
  // correction du bug sur les saisie d exposant
  for (let i = 4; i < 10; i++) {
    const reg = RegExp(String.fromCharCode(i + 8304), 'g')
    s = s.replace(reg, '^' + i)
  }
  if (!s) { return '' }
  const reg = /'/g
  s = s.replace(reg, '"')
  const req = new XMLHttpRequest()
  url = this.url + url
  try {
    req.open('post', url, false)
  } catch (err) {
    console.error(err)
    let txt = 'Erreur\n\n'
    txt += 'Acces refuse au serveur XCAS en ligne.\n\n'
    txt += 'Il faut activer l’acces aux sources de donnees sur plusieurs domaines dans les parametres de securite de votre navigateur.\n\n'
    j3pShowError(txt)
  }
  req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
  try {
    req.send('option=' + encodeURIComponent(option) + '&in=' + encodeURIComponent(s) +
         '&session_id=' + encodeURIComponent(XcasBase.session_id) +
         '&xx=' + encodeURIComponent(xx) +
         '&yy=' + encodeURIComponent(yy))
  } catch (error) {
    j3pShowError(error)
  }
  let resp = req.responseText
  for (let i = 0; i < XcasBase.htmlSymbols.length; i++) {
    const reg = RegExp(XcasBase.htmlSymbols[i], 'g')
    resp = resp.replace(reg, XcasBase.utfSymbols[i])
  }
  return resp
}

XcasBase.prototype.reqGiac = function (s) {
  return this.reqG(s, 'giac_interface.php', 'text')
}

XcasBase.prototype.reqLabomep = function (s) {
  return this.reqG(s, 'giac_interface.php', 'labomep')
}

XcasBase.prototype.reqProg = function (s) {
  return this.reqG(s, 'giac_interface.php', 'prog')
}

XcasBase.prototype.reqMath = function (s) {
  return this.reqG(s, 'giac_interface.php', 'math')
}

XcasBase.prototype.reqTableur = function reqTableur (s) {
  return this.reqG(s, 'giac_interface.php', 'spread')
}

XcasBase.prototype.reqTableurApprox = function reqTableurApprox (s) {
  return this.reqG(s, 'giac_interface.php', 'spreadf')
}

XcasBase.prototype.reqTableurTexte = function reqTableurTexte (s) {
  return this.reqG(s, 'giac_interface.php', 'cep')
}

XcasBase.prototype.reqSVG = function reqSVG (s) {
  return this.reqG(s, 'giac_interface.php', 'svg')
}

XcasBase.prototype.reqMathSVG = function reqMathSVG (s) {
  return this.reqG(s, 'giac_interface.php', 'text_math_svg')
}

XcasBase.prototype.reqSVGgrid = function reqSVGgrid (s, xx, yy) {
  return this.reqG(s, 'giac_interface.php', 'svg_grid', xx, yy)
}

// affiche le resultat en mathml

XcasBase.prototype.insereMathML = function insereMathML (id, s, inline, remplace) {
  s = '>' + s + '</math>'
  if (inline) {
    s = " display='inline'" + s
  } else {
    s = " display='block'" + s
  }
  s = '<math' + s
  if (remplace) {
    id.innerHTML = s
  } else {
    id.innerHTML += s
  }
}

XcasBase.prototype.calcule = function calcule (s) {
  let sr = this.reqGiac(s)
  // rustine pour IE
  if (XcasBase.isIE) { sr = sr.replace(/``/g, '` `') }
  const t = sr.split(/`/g)
  XcasBase.session_id = t[0]
  return t[1]
}

XcasBase.prototype.calculeMml = function calculeMml (s, conteneur, inline, remplace) {
  let sr = this.reqGiac(s)
  // rustine pour IE
  if (XcasBase.isIE) { sr = sr.replace(/``/g, '` `') }
  const t = sr.split(/`/g)
  XcasBase.session_id = t[0]
  // on affiche le resultat en mathml
  this.insereMathML(conteneur, t[2], inline, remplace)
  if (conteneur.scrollIntoView) { conteneur.scrollIntoView(false) }
  return t[1]
}

XcasBase.prototype.createElement = function createElement (type) {
  const elt = document.createElement(type)
  elt.setAttribute('index', this.index)
  return elt
}

XcasBase.prototype.element = function element (type, cible, classe) {
  const elem = document.createElement(type)
  if (classe) { elem.className = classe }
  cible.appendChild(elem)
  return elem
}

// a revoir. necessite disparue ?
XcasBase.prototype.elementIndexe = function elementIndexe (type, cible, classe) {
  const elem = this.createElement(type)
  if (classe) { elem.className = classe }
  cible.appendChild(elem)
  return elem
}

// Constantes
XcasBase.isIE = window.ActiveXObject
XcasBase.isOpera = (navigator.appName == 'Opera')
XcasBase.isFirefox = (navigator.userAgent.indexOf('Gecko') > -1 && navigator.userAgent.indexOf('KHTML') == -1)
XcasBase.isWebKit = (navigator.userAgent.indexOf('AppleWebKit/') > -1 || navigator.userAgent.indexOf('KHTML') > -1)
if (XcasBase.isIE && document.documentMode == 9) {
  XcasBase.isIE = false
  XcasBase.isWebKit = true
}

XcasBase.mathmlURL = 'http://www.w3.org/1998/Math/MathML'
XcasBase.svgURL = 'http://www.w3.org/2000/svg'

XcasBase.htmlSymbols = ['&int;', '&Integral;', '&Sigma;', '&part;', '&nbsp;',
  '&rarr;', '&VerticalBar;', '&deg;', '&infin;', '&InvisibleTimes;',
  '&pi;', '&le;', '&ge;', '"']
XcasBase.utfSymbols = ['\u222B', '\u0222B', '\u03A3', '\u2202', '\u0020',
  '\u2192', '|', '\u000B0', '\u221E', '',
  '\u03C0', '\u2264', '\u2265', "'"]

XcasBase.TABULATION = 9
XcasBase.FLECHE_HAUTE = 38
XcasBase.FLECHE_BASSE = 40
XcasBase.FLECHE_GAUCHE = 37
XcasBase.FLECHE_DROITE = 39
XcasBase.CTRL_M = 77
XcasBase.ENTREE = 13
XcasBase.session_id = ''

if (XcasBase.isWebKit) {
  xcasInclureStyle(xcasBaseUrl + 'xcas/mathml.css')
}

// liste des objets crees
XcasBase.listeElements = []

export default XcasBase
