// -*- coding: utf-8-unix -*-

//   Copyright (C) 2009 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distribute  in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import XcasBase from 'src/legacy/outils/xcas/XcasBase'

export function xcasOpacite (objet, opaque) {
  let IEopacite, opacite
  if (opaque) {
    IEopacite = '100'
    opacite = 1
  } else {
    IEopacite = '60'
    opacite = 0.6
  }
  if (XcasBase.isIE) { objet.style.filter = 'alpha(opacity=' + IEopacite + ')' } else { objet.style.opacity = opacite }
}

export function xcasElement (elt) {
  return XcasBase.listeElements[parseInt(elt.getAttribute('index'))]
}

export function xcasInsereTexte (texte, tag, cible) {
  const elt = document.createElement(tag)
  const text = document.createTextNode(texte)
  elt.appendChild(text)
  cible.appendChild(elt)
}

// fonctions de requetes

export function xcasRequete (s) {
  const x = new XcasBase()
  return x.calcule(s)
}

// pour ~viter que l’utlisateur attende la premiere reponse :
// xcasRequete("0");

/* deux fonctions que personne n’utilise…
function xcasRequeteMml (s, conteneur, inline, remplace) {
  if (typeof conteneur === typeof 'a') { conteneur = document.getElementById(conteneur) }
  const x = new XcasBase()
  return x.calculeMml(s, conteneur, inline, remplace)
}

function xcasTransformeEnPopup (id) {
  const popup = new XcasPopUp(100, 50, 300)
  const elt = document.getElementById(id)
  popup.inclus = elt.parentNode.replaceChild(popup.marqueur, elt)
  popup.contenu.appendChild(popup.inclus)
  popup.inclus.style.display = 'block'
  return popup
}
*/
