// -*- coding: utf-8-unix -*-

//   Copyright (C) 2009 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distribute  in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import { xcasRequete } from 'src/legacy/outils/xcas/base'
import XcasBase from 'src/legacy/outils/xcas/XcasBase'

// -----------------------------------------------------------------------------
// xcasProgramme : envoyer des programmes xcas

xcasProg.prototype = new XcasBase()

function xcasProg (id, code) {
  this.parent = XcasBase
  this.parent()
  this.includeStyle(this.url + 'xcas/prog.css')
  this.initialise()
  let prog
  if (id) {
    if (typeof id === typeof 'a') { prog = document.getElementById(id) } else { prog = id }
    prog.className = 'prog'
    this.input = document.createElement('textarea')
    this.input.onblur = () => xcasRequete(this.value)
    prog.appendChild(this.input)
    if (arguments.length == 2) { this.ecritCode(code) }
    this.input.focus()
  }
}

xcasProg.prototype.ecritCode = function (code) {
  this.input.value = code
  xcasRequete(code)
  this.input.focus()
}
