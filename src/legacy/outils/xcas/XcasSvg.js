// -*- coding: utf-8-unix -*-

//   Copyright (C) 2009 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distribute  in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import XcasBase from 'src/legacy/outils/xcas/XcasBase'

class XcasSvg extends XcasBase {
  constructor (id, largeur, hauteur, X, Y) {
    super()
    this.parent = XcasBase
    this.parent()
    this.largeur = largeur
    this.hauteur = hauteur
    this.include(this.url + 'xcas/svg.js')
    this.initialise()
    this.NS = 'http://www.w3.org/2000/svg'
    if (id) {
      id = document.getElementById(id)
      this.svg = document.createElementNS(this.NS, 'svg')
      this.svg.setAttribute('width', largeur + 'cm')
      this.svg.setAttribute('height', hauteur + 'cm')
      this.X = X
      this.Y = Y
      id.appendChild(this.svg)
    }
  }

  insere (str, sId, couleur, remplace, protege) {
    function changeNodeNamespace (node, namespace) {
      // from http://stackoverflow.com/questions/9831818/change-namespace-of-a-dom-node-before-during-after-import
      const dup = document.createElementNS(namespace, node.nodeName)
      if (node.hasAttributes()) {
        for (let a = node.attributes, i = a.length; i--;) {
          dup.setAttributeNS(a[i].namespaceURI, a[i].nodeName, a[i].value)
        }
      }
      if (node.hasChildNodes()) {
        const a = node.childNodes
        for (let i = 0, len = a.length; i < len; ++i) {
          if (a[i].nodeType == 1) { // ELEMENT_NODE
            dup.appendChild(changeNodeNamespace(a[i], namespace))
          } else {
            dup.appendChild(a[i].cloneNode(false))
          }
        }
      }
      return dup
    }

    if (!str) return
    let id
    if (sId && sId != '') {
      id = document.getElementById(sId)
    } else if (this.svg) {
      id = this.svg
    } else {
      return
    }

    if (remplace) {
      for (let i = id.childNodes.length - 1; i >= 0; i--) {
        const node = id.childNodes[i]
        if (protege || !node.tagName || node.getAttribute('protege') != '1') {
          id.removeChild(node)
        }
      }
    }

    const parser = new DOMParser()
    if (str.indexOf('<svg') == -1 && str.indexOf('<defs') == -1) { str = '<g>' + str + '</g>' }
    const doc = parser.parseFromString(str, 'image/svg+xml')
    let el = doc.firstChild
    el = changeNodeNamespace(el, XcasBase.svgURL)
    if (protege && el.setAttribute) { el.setAttribute('protege', '1') }
    let fils = el.firstChild
    while (fils) {
      if (fils.pathSegList) {
        const n = fils.pathSegList.numberOfItems
        for (let i = n - 1; i > 0; i--) {
          if (!this.dansCadre(fils.pathSegList.getItem(i))) { fils.pathSegList.removeItem(i) }
        }
        if (!this.dansCadre(fils.pathSegList.getItem(0))) {
          fils.pathSegList.getItem(0).y = fils.pathSegList.getItem(1).y
          fils.pathSegList.getItem(0).x = fils.pathSegList.getItem(1).x
        }
      }
      if (fils.tagName && fils.setAttribute) {
        if (sId == 'legende') { fils.setAttribute('fill', couleur) } else { fils.setAttribute('stroke', couleur) }
      }
      fils = fils.nextSibling
    }
    id.appendChild(el)
    return el
  }

  dansCadre (objet) {
    if (objet.y > this.ymax) { return false }
    if (objet.y < this.ymin) { return false }
    if (objet.x > this.xmax) { return false }
    if (objet.x < this.xmin) { return false }
    return true
  }

  svgGrid (xmin, ymin, xmax, ymax) {
    const fenetre = 'xyztrange(' + xmin + ',' + xmax + ',' + ymin + ',' + ymax + ',-5,5,' + xmin + ',' + xmax + ',' + ymin + ',' + ymax + ',-2,2,1,0,1)'
    const sr = this.reqSVGgrid(fenetre, this.X, this.Y)
    const t = sr.split(/`/g)
    let el = this.svg.firstChild
    while (el) {
      if (el.tagName != 'defs') { this.svg.removeChild(el) }
      el = el.nextSibling
    }
    const lg = (xmax - xmin) / 4
    const ht = (ymax - ymin) / 4
    this.xmin = xmin - lg
    this.ymin = ymin - ht
    this.xmax = xmax + lg
    this.ymax = ymax + ht
    this.insere(t[1])
  }

  supprime (sID) {
    const id = document.getElementById(sID)
    if (!id) { return }
    const pere = id.parentNode
    pere.removeChild(id)
  }

  exploreDom (node, clone) {
    let node1
    let i
    if (node.tagName) {
      node1 = document.createElementNS(this.NS, node.tagName)
      for (i = 0; i < node.attributes.length; i++) { node1.setAttribute(node.attributes[i].name, node.attributes[i].value) }
    } else { node1 = document.createTextNode(node.nodeValue) }
    clone.appendChild(node1)
    let fils = node.firstChild
    while (fils) {
      this.exploreDom(fils, node1)
      fils = fils.nextSibling
    }
    return clone
  }

  source (sID) {
    const id = document.getElementById(sID)
    if (!id) { return }
    const serializer = new XMLSerializer()
    const xml = serializer.serializeToString(id)
    return xml
  }

  effaceTout (sId) {
    const id = document.getElementById(sId)
    while (id.firstChild) { id.removeChild(id.firstChild) }
  }

  attribue (objet, attribut, valeur) {
    objet.setAttribute(attribut, valeur)
    if (objet.firstChild) {
      objet = objet.firstChild
      for (;;) {
        objet = objet.nextSibling
        if (objet.setAttribute) { objet.setAttribute(attribut, valeur) }
        if (!objet.nextSibling) { break }
      }
    }
  }

  changeCouleur (i, n) {
    const couleurs = ['black', 'gray', 'red', 'darkblue', 'goldenrod', 'green', 'darkorange', 'purple']
    document.getElementById('svg' + n).setAttribute('stroke', couleurs[i])
  }

  changeEpais (objet, epaisseur) {
    const isIE = window.ActiveXObject
    if (!isIE) {
      objet.setAttribute('stroke-width', epaisseur)
    }
  }

  cloneSVG () {
    return document.getElementById('cadreSVG').lastChild.cloneNode(true)
  }
}

export default XcasSvg
