// -*- coding: utf-8-unix -*-

//   Copyright (C) 2009 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distribute  in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import XcasBase from 'src/legacy/outils/xcas/XcasBase'

function Xcas () {
  this.parent = XcasBase
  this.parent()
  this.commande = ''
  this.retours = []
  this.retoursMml = []
  this.retoursSvg = []
  this.mmlEnLigne = false
  this.remplaceMml = false
  this.remplaceSvg = false
  this.couleurs = []
  this.couleurSvg = 'black'
  this.colors = []
  this.colors.noir = 'black'
  this.colors.blanc = 'white'
  this.colors.gris = 'gray'
  this.colors.rouge = 'red'
  this.colors.jaune = 'goldenrod'
  this.colors.bleu = 'darkblue'
  this.colors.orange = 'darkorange'
  this.colors.violet = 'purple'
  this.colors.vert = 'green'
  return this
}

Xcas.prototype = new XcasBase()

Xcas.prototype.argument = function (arg) {
  const elem = document.getElementById(arg)
  if (!elem) {
    return arg
  }

  /* ex ???
  if (elem.className == 'entreefm') {
    if (ex) {
      let e = new ex(elem.getAttribute('formule'))
      e = e.substInput(elem)
      return e
      // elem.setAttribute("texte",e.string());
    }
  } /* */

  switch (elem.tagName.toLowerCase()) {
    case 'input':
      return elem.value
    case 'select':
      return elem.selectedIndex
    case 'div':
      return elem.getAttribute('texte')
    case 'p':
      return elem.getAttribute('texte')
    case 'span':
      return elem.getAttribute('texte')
    case 'form' : // boutons radio
      for (let i = 0; i < elem.length; i++) {
        if (elem.elements[i].checked) { return i }
      }
      return 0
  }
}

Xcas.prototype.eval = function () {
  // synthese de la commande
  this.commande = ''
  if (arguments.length > 0) {
    this.commande = arguments[0]
    if (arguments.length > 1) {
      this.commande += '(' + this.argument(arguments[1])
      for (let i = 2; i < arguments.length; i++) { this.commande += ',' + this.argument(arguments[i]) }
      this.commande += ')'
    }
  }
  const sr = this.reqLabomep(this.commande)
  const tr = sr.split(/`SVG/g)
  let t = tr[0].split(/`/g)
  const r = t[1].split(/,/g)
  // traitement des couleurs
  this.couleurs = []
  this.retours = []
  this.retoursMml = []
  let c
  for (let i = 0; i < r.length; i++) {
    c = this.couleur(r[i])
    if (c[0]) { this.couleurs.push(c[1]) } else {
      this.retours.push(r[i])
      this.retoursMml.push(t[i + 2])
      if (i > this.couleurs.length) { this.couleurs.push('black') }
    }
  }
  // graphique eventuel
  this.retoursSvg = []
  t = tr[1].split(/`/g)
  if (t[0] && t[0] != '' && t[0] != ' ' &&
      t[0] != 'error' &&
      t[0] != 'undef') { this.retoursSvg = t }
  return sr
}

Xcas.prototype.evalSvg = function () {
  // synthese de la commande
  this.commande = ''
  if (arguments.length > 0) {
    this.commande = arguments[0]
    if (arguments.length > 1) {
      this.commande += '(' + this.argument(arguments[1])
      for (let i = 2; i < arguments.length; i++) { this.commande += ',' + this.argument(arguments[i]) }
      this.commande += ')'
    }
  }
  const sr = this.reqSVG(this.commande)
  const t = sr.split(/`/g)
  // this.retours=t[1].split(new RegExp(",","g"));
  this.retoursSvg = []
  for (let i = 1; i < t.length; i++) { this.retoursSvg.push(t[i]) }
  return sr
}

Xcas.prototype.nonEval = function () {
  this.couleurs = []
  // synthese de la commande
  this.commande = ''
  if (arguments.length > 0) {
    this.commande = arguments[0]
    if (arguments.length > 1) {
      this.commande += '(' + this.argument(arguments[1])
      for (let i = 2; i < arguments.length; i++) { this.commande += ',' + this.argument(arguments[i]) }
      this.commande += ')'
    }
  }
  const sr = this.reqMath(this.commande)
  const t = sr.split(/`/g)
  this.retours = t[1].split(/,/g)
  this.retoursMml = []
  for (let i = 2; i < t.length; i++) { this.retoursMml.push(t[i]) }
  return sr
}

Xcas.prototype.imprimeEnLigne = function (enLigne) {
  this.mmlEnLigne = enLigne
}

Xcas.prototype.remplaceQuandImprime = function (remplace) {
  this.remplaceMml = remplace
  this.remplaceSvg = remplace
}

Xcas.prototype.couleur = function (s) {
  if (s.length < 3) { return [false, s] }
  const t = s.substring(1, s.length - 1)
  if (this.colors[t]) { return [true, this.colors[t]] }
  if (t.charAt(0) == '#') { return [true, t] }
  return [false, s]
}

Xcas.prototype.imprimeMml = function (conteneur, i, enLigne, remplace) {
  if (conteneur == '') { return }
  if (this.retoursMml.length == 0) { return }
  if (typeof conteneur === typeof 'a') { conteneur = document.getElementById(conteneur) }
  if (!conteneur) { return }
  if (enLigne != null) { this.mmlEnLigne = enLigne }
  // specifique a la fabrique
  if (conteneur.getAttribute('horsligne')) { this.mmlEnLigne = false }
  if (remplace != null) { this.remplaceMml = remplace }
  if (!i) i = 0
  if (i > this.retoursMml.length) { i = this.retoursMml.length - 1 }
  if (this.retours[i] == null || this.retours[i] == 'SVG' || (i > 0 && this.retours[i - 1] == 'SVG')) { return }
  conteneur.setAttribute('texte', this.retours[i])
  this.insereMathML(conteneur, this.retoursMml[i], this.mmlEnLigne, this.remplaceMml)
  conteneur.style.color = this.couleurs[i]
  if (!this.remplaceMml) {
    const hr = document.createElement('hr')
    conteneur.appendChild(hr)
    hr.scrollIntoView(false)
  }
  if (conteneur.getAttribute('class') == 'sortie') { // on est sous la fabrique
    let h = parseInt(conteneur.style.height)
    if (isNaN(h)) { h = 0 }
    conteneur.style.height = 'auto'
    conteneur.style.lineHeight = 'auto'
    const ht = conteneur.offsetHeight
    if (ht > h + 2) {
      conteneur.style.height = ht + 'px'
      // conteneur.style.lineHeight=conteneur.style.height;
    } else {
      conteneur.style.height = h + 'px'
      // conteneur.style.lineHeight=conteneur.style.height;
    }
  }
}

Xcas.prototype.imprimeSvg = function (xcSvg, remplace, protege) {
  if (!xcSvg) { return }
  if (remplace != null) { this.remplaceSvg = remplace }
  if (protege == null) { protege = false }
  let couleur = 'black'
  if (this.couleurs.length > this.retours.length) { couleur = this.couleurs[this.couleurs.length - 1] }
  xcSvg.insere(this.retoursSvg[0], 'figures', couleur, this.remplaceSvg, protege)
  xcSvg.insere(this.retoursSvg[1], 'legende', couleur, this.remplaceSvg, protege)
}

export default Xcas
