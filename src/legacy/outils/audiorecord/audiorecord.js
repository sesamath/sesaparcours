const audioTom = {}

audioTom.leftchannel = []
audioTom.rightchannel = []
audioTom.recorder = null
audioTom.recordingLength = 0
audioTom.mediaStream = null
audioTom.sampleRate = 44100
audioTom.context = null
audioTom.blob = null

export function recupeTaille () {
  return audioTom.recordingLength
}
export function startRecordingButton () {
  // Initialize recorder
  navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia
  navigator.getUserMedia(
    {
      audio: true
    },
    function (e) {
      audioTom.leftchannel = []
      audioTom.rightchannel = []
      audioTom.recorder = null
      audioTom.recordingLength = 0
      audioTom.mediaStream = null
      audioTom.sampleRate = 44100
      audioTom.context = null
      audioTom.blob = null

      // creates the audio context
      window.AudioContext = window.AudioContext || window.webkitAudioContext
      audioTom.context = new AudioContext()

      // creates an audio node from the microphone incoming stream
      audioTom.mediaStream = audioTom.context.createMediaStreamSource(e)

      // https://developer.mozilla.org/en-US/docs/Web/API/AudioContext/createScriptProcessor
      // bufferSize: the onaudioprocess event is called when the buffer is full
      const bufferSize = 2048
      const numberOfInputChannels = 2
      const numberOfOutputChannels = 2
      if (audioTom.context.createScriptProcessor) {
        audioTom.recorder = audioTom.context.createScriptProcessor(bufferSize, numberOfInputChannels, numberOfOutputChannels)
      } else {
        audioTom.recorder = audioTom.context.createJavaScriptNode(bufferSize, numberOfInputChannels, numberOfOutputChannels)
      }

      audioTom.recorder.onaudioprocess = function (e) {
        audioTom.leftchannel.push(new Float32Array(e.inputBuffer.getChannelData(0)))
        audioTom.rightchannel.push(new Float32Array(e.inputBuffer.getChannelData(1)))
        audioTom.recordingLength += bufferSize
      }

      // we connect the recorder
      audioTom.mediaStream.connect(audioTom.recorder)
      audioTom.recorder.connect(audioTom.context.destination)
    },
    function (e) {
      console.error(e)
    })
}

export function stopRecordingButton (callback) {
  // stop recording
  audioTom.recorder.disconnect(audioTom.context.destination)
  audioTom.mediaStream.disconnect(audioTom.recorder)

  // we flat the left and right channels down
  // Float32Array[] => Float32Array
  const leftBuffer = flattenArray(audioTom.leftchannel, audioTom.recordingLength)
  const rightBuffer = flattenArray(audioTom.rightchannel, audioTom.recordingLength)
  // we interleave both channels together
  // [left[0],right[0],left[1],right[1],...]
  const interleaved = interleave(leftBuffer, rightBuffer)

  // we create our wav file
  const buffer = new ArrayBuffer(44 + interleaved.length * 2)
  const view = new DataView(buffer)

  // RIFF chunk descriptor
  writeUTFBytes(view, 0, 'RIFF')
  view.setUint32(4, 44 + interleaved.length * 2, true)
  writeUTFBytes(view, 8, 'WAVE')
  // FMT sub-chunk
  writeUTFBytes(view, 12, 'fmt ')
  view.setUint32(16, 16, true) // chunkSize
  view.setUint16(20, 1, true) // wFormatTag
  view.setUint16(22, 2, true) // wChannels: stereo (2 channels)
  view.setUint32(24, audioTom.sampleRate, true) // dwSamplesPerSec
  view.setUint32(28, audioTom.sampleRate * 4, true) // dwAvgBytesPerSec
  view.setUint16(32, 4, true) // wBlockAlign
  view.setUint16(34, 16, true) // wBitsPerSample
  // data sub-chunk
  writeUTFBytes(view, 36, 'data')
  view.setUint32(40, interleaved.length * 2, true)

  // write the PCM samples
  let index = 44
  const volume = 1
  for (let i = 0; i < interleaved.length; i++) {
    view.setInt16(index, interleaved[i] * (0x7FFF * volume), true)
    index += 2
  }
  // our final blob
  const blob = new Blob([view], { type: 'audio/wav' })
  const url = window.URL.createObjectURL(blob)

  callback(url)
}
/*
function blobToBase64 (blob, callback) {
  const reader = new FileReader()
  reader.readAsDataURL(blob)
  reader.onloadend = function () {
    const base64data = reader.result
    // eslint-disable-next-line n/no-callback-literal
    callback({ base: base64data, length: base64data.length })
  }
}
*/

function writeUTFBytes (view, offset, string) {
  for (let i = 0; i < string.length; i++) {
    view.setUint8(offset + i, string.charCodeAt(i))
  }
}

function interleave (leftChannel, rightChannel) {
  const length = leftChannel.length + rightChannel.length
  const result = new Float32Array(length)

  let inputIndex = 0

  for (let index = 0; index < length;) {
    result[index++] = leftChannel[inputIndex]
    result[index++] = rightChannel[inputIndex]
    inputIndex++
  }
  return result
}

function flattenArray (channelBuffer, recordingLength) {
  const result = new Float32Array(recordingLength)
  let offset = 0
  for (let i = 0; i < channelBuffer.length; i++) {
    const buffer = channelBuffer[i]
    result.set(buffer, offset)
    offset += buffer.length
  }
  return result
}
