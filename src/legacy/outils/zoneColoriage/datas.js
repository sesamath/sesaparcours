/**
 * @todo à décrire
 */
export const modeles = [
  {
    tourneD: false,
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 0, 1, 1, 1, 1, 0, 1]
  },
  {
    tourneD: false,
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 0, 0, 1, 1, 0, 0, 0]
  },
  {
    tourneD: false,
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 1, 0, 1, 0, 0, 0, 1]
  },
  {
    tourneD: false,
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 0, 0, 0, 0, 0, 1, 0]
  },
  {
    tourneD: false,
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 0, 1, 0, 1, 0, 0, 0]
  },
  {
    tourneD: false,
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 1, 0, 1, 1, 0, 0, 0]
  },
  {
    tourneD: false,
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 0, 1, 0, 1, 1, 0, 1]
  },
  {
    tourneD: false,
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 0, 0, 1, 0, 1, 0, 1]
  },
  {
    tourneD: false,
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 0, 1, 0, 1, 0, 1, 0]
  },
  {
    tourneD: false,
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 0, 1, 0, 1, 1, 0, 1]
  },
  {
    tourneD: true,
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(255, 255, 255)', 'rgb(255, 172, 0)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 0, 1, 1, 1, 2, 2, 2]
  },
  {
    tourneD: true,
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(255, 255, 255)', 'rgb(255, 172, 0)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 0, 1, 1, 1, 2, 1, 2]
  },
  {
    tourneD: true,
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(255, 255, 255)', 'rgb(255, 172, 0)', 'rgb(0, 0, 0)'],
    tab: [1, 2, 1, 2, 1, 0, 1, 0, 1]
  },
  {
    tourneD: true,
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(255, 255, 255)', 'rgb(255, 172, 0)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 1, 0, 1, 2, 1, 2, 2]
  },
  {
    tourneD: true,
    changeCoul: false,
    invHautBas: true,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 172, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 0, 2, 1, 2, 1, 0, 1]
  },
  {
    tourneD: true,
    changeCoul: false,
    invHautBas: true,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(255, 172, 0)', 'rgb(10, 8, 8)', 'rgb(255, 255, 255)', 'rgb(164, 123, 37)'],
    tab: [0, 0, 0, 1, 2, 1, 3, 2, 3]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(10, 8 , 8)', 'rgb(255, 172, 0)', 'rgb(255, 255, 255)', 'rgb(164, 123, 37)'],
    tab: [0, 1, 0, 2, 1, 2, 1, 3, 1]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(10, 8 , 8)', 'rgb(255, 172, 0)', 'rgb(164, 123, 37)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 0, 2, 1, 2, 1, 3, 1]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(10, 8 , 8)', 'rgb(255, 172, 0)', 'rgb(255, 255, 255)', 'rgb(164, 123, 37)'],
    tab: [0, 0, 0, 2, 1, 2, 1, 3, 1]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 3,
    couleurs: ['rgb(10, 8 , 8)', 'rgb(255, 172, 0)', 'rgb(164, 123, 37)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 0, 2, 1, 2, 1, 3, 1]
  },
  {
    changeCoul: true,
    t: 3,
    couleurs: ['rgb(252, 11, 0)', 'rgb(255, 231, 24)', 'rgb(0, 23, 255)', 'rgb(59, 154, 2)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 2, 3, 4, 3, 2, 1, 0]
  },
  {
    changeCoul: true,
    t: 3,
    couleurs: ['rgb(0, 23, 255)', 'rgb(255, 231, 24)', 'rgb(252, 11, 0)', 'rgb(59, 154, 2)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 2, 3, 4, 3, 0, 1, 2]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 4,
    couleurs: ['rgb(252, 11, 0)', 'rgb(7, 4, 4)'],
    tab: [0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 4,
    couleurs: ['rgb(252, 11, 0)', 'rgb(12, 12, 5)'],
    tab: [0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 4,
    couleurs: ['rgb(252, 11, 0)', 'rgb(14, 13, 9)'],
    tab: [0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 4,
    couleurs: ['rgb(252, 11, 0)', 'rgb(12, 12, 8)'],
    tab: [0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 4,
    couleurs: ['rgb(252, 11, 0)', 'rgb(16, 15, 10)'],
    tab: [0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1]
  },
  {
    tourneD: true,
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: false,
    t: 4,
    couleurs: ['rgb(255, 255, 255)', 'rgb(252, 11, 0)', 'rgb(0, 0, 0)'],
    tab: [1, 1, 1, 1, 0, 0, 0, 0, 2, 2, 2, 2, 0, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 4,
    couleurs: ['rgb(255, 255, 255)', 'rgb(253, 0, 0)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 2, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 4,
    couleurs: ['rgb(252, 11, 0)', 'rgb(255, 255, 255)', 'rgb(4, 5, 4)'],
    tab: [0, 1, 1, 0, 1, 2, 2, 1, 1, 2, 2, 1, 0, 1, 1, 0]
  },
  {
    t: 4,
    couleurs: ['rgb(0, 0, 0)', 'rgb(252, 11, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 1, 0, 1, 1, 1, 1, 1, 2, 2, 1, 0, 0, 0, 0]
  },
  {
    t: 4,
    couleurs: ['rgb(0, 0, 0)', 'rgb(252, 11, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 1, 0, 1, 1, 1, 1, 1, 2, 2, 1, 1, 2, 2, 1]
  },
  {
    tourneD: true,
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 4,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 231, 24)', 'rgb(252, 11, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 0, 1, 1, 0, 1, 0, 2, 2, 2, 2, 3, 3, 3, 3]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 4,
    couleurs: ['rgb(0, 23, 255)', 'rgb(255, 255, 255)', 'rgb(255, 231, 24)', 'rgb(252, 11, 0)'],
    tab: [0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 2, 0, 3, 1, 3, 1]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 4,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 231, 24)', 'rgb(255, 255, 255)', 'rgb(252, 11, 0)'],
    tab: [0, 1, 1, 0, 2, 3, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1]
  },
  {
    t: 4,
    couleurs: ['rgb(0, 0, 0)', 'rgb(252, 11, 0)', 'rgb(255, 231, 24)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 1, 0, 1, 1, 1, 1, 1, 2, 2, 1, 1, 3, 3, 1]
  },
  {
    t: 4,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 231, 24)', 'rgb(252, 11, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 0, 0, 1, 0, 0, 1, 2, 3, 3, 2, 2, 2, 2, 2]
  },
  {
    tourneD: true,
    changeCoul: false,
    invHautBas: true,
    InvGaucheDroite: false,
    t: 4,
    couleurs: ['rgb(0, 0, 0)', 'rgb(252, 11, 0)', 'rgb(255, 231, 24)', 'rgb(0, 23, 255)', 'rgb(253, 70, 253)'],
    tab: [0, 1, 2, 0, 1, 3, 4, 2, 2, 4, 3, 1, 0, 2, 1, 0]
  },
  {
    tourneD: true,
    changeCoul: false,
    invHautBas: true,
    InvGaucheDroite: false,
    t: 4,
    couleurs: ['rgb(0, 0, 0)', 'rgb(252, 11, 0)', 'rgb(255, 231, 24)', 'rgb(0, 23, 255)', 'rgb(253, 70, 253)'],
    tab: [0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 4, 4, 4, 4]
  },
  {
    tourneD: true,
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 4,
    couleurs: ['rgb(0, 0, 0)', 'rgb(252, 11, 0)', 'rgb(255, 231, 24)', 'rgb(0, 23, 255)', 'rgb(253, 70, 253)'],
    tab: [0, 1, 2, 2, 1, 0, 3, 3, 3, 3, 0, 1, 4, 4, 1, 0]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 5,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 5,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 5,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 5,
    couleurs: ['rgb(255, 255, 255)', 'rgb(19, 12, 12)'],
    tab: [0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 5,
    couleurs: ['rgb(246, 248, 246)', 'rgb(23, 16, 15)'],
    tab: [0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1]
  },
  {
    tourneD: true,
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 5,
    couleurs: ['rgb(255, 255, 255)', 'rgb(252, 11, 0)', 'rgb(0, 0, 0)'],
    tab: [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1]
  },
  {
    t: 5,
    couleurs: ['rgb(255, 255, 255)', 'rgb(252, 11, 0)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 2, 2, 2, 1, 2, 2, 0, 2, 2]
  },
  {
    t: 5,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)', 'rgb(252, 11, 0)'],
    tab: [0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 2, 0, 0, 0, 2, 2, 2, 1, 2, 2]
  },
  {
    t: 5,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)', 'rgb(252, 11, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2]
  },
  {
    InvGaucheDroite: true,
    t: 5,
    couleurs: ['rgb(255, 255, 255)', 'rgb(252, 11, 0)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 1, 0, 0, 2, 0, 1, 2, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 5,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)', 'rgb(238, 153, 71)', 'rgb(0, 23, 255)'],
    tab: [0, 0, 0, 0, 0, 1, 2, 2, 2, 1, 2, 3, 2, 3, 2, 1, 2, 2, 2, 1, 1, 1, 2, 1, 1]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 5,
    couleurs: ['rgb(255, 255, 255)', 'rgb(238, 153, 71)', 'rgb(0, 0, 0)', 'rgb(0, 23, 255)'],
    tab: [0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 2, 2, 2, 2, 2, 3, 2, 2, 2, 3, 3, 3, 3, 3, 3]
  },
  {
    t: 5,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)', 'rgb(0, 23, 255)', 'rgb(225, 123, 1)'],
    tab: [0, 1, 1, 1, 0, 1, 2, 3, 2, 1, 1, 3, 3, 3, 1, 1, 3, 0, 3, 1, 0, 2, 2, 2, 0]
  },
  {
    tourneD: true,
    changeCoul: false,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 5,
    couleurs: ['rgb(0, 23, 255)', 'rgb(0, 0, 0)', 'rgb(255, 255, 255)', 'rgb(225, 123, 1)'],
    tab: [0, 0, 1, 1, 0, 0, 1, 2, 2, 1, 0, 1, 2, 2, 1, 0, 0, 1, 1, 0, 3, 3, 3, 3, 3]
  },
  {
    InvGaucheDroite: true,
    t: 5,
    couleurs: ['rgb(255, 0, 0)', 'rgb(117, 227, 52)', 'rgb(255, 231, 24)', 'rgb(173, 85, 17)', 'rgb(150, 150, 150)'],
    tab: [0, 1, 0, 0, 2, 1, 3, 1, 0, 0, 2, 3, 2, 2, 2, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4]
  },
  {
    InvGaucheDroite: true,
    t: 5,
    couleurs: ['rgb(255, 231, 24)', 'rgb(117, 227, 52)', 'rgb(255, 0, 0)', 'rgb(173, 85, 17)', 'rgb(150, 150, 150)'],
    tab: [0, 1, 1, 1, 2, 0, 1, 1, 2, 1, 0, 2, 1, 1, 1, 0, 0, 3, 3, 4, 0, 4, 3, 3, 4]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 5,
    couleurs: ['rgb(255, 231, 24)', 'rgb(117, 227, 52)', 'rgb(255, 0, 0)', 'rgb(173, 85, 17)', 'rgb(150, 150, 150)'],
    tab: [0, 0, 0, 0, 1, 2, 2, 2, 2, 0, 3, 3, 3, 3, 2, 4, 4, 4, 4, 3, 1, 1, 1, 1, 4]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 5,
    couleurs: ['rgb(255, 231, 24)', 'rgb(117, 227, 52)', 'rgb(255, 0, 0)', 'rgb(173, 85, 17)', 'rgb(150, 150, 150)'],
    tab: [0, 1, 0, 1, 0, 1, 2, 1, 2, 1, 2, 3, 2, 3, 2, 3, 4, 3, 4, 3, 4, 0, 4, 0, 4]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(0, 23, 255)', 'rgb(59, 154, 2)'],
    tab: [0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(59, 154, 2)', 'rgb(0, 23, 255)'],
    tab: [0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0]
  },
  {
    tourneD: true,
    changeCoul: false,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 6,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 6,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 6,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)', 'rgb(243, 173, 93)'],
    tab: [0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 2, 2, 2, 2, 0, 0, 2, 1, 1, 2, 0, 0, 0, 2, 2, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(78, 224, 201)', 'rgb(255, 231, 24)', 'rgb(59, 154, 2)'],
    tab: [0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 2, 0, 2, 2, 2, 2, 2, 2]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(222, 216, 216)', 'rgb(208, 150, 21)', 'rgb(33, 12, 12)'],
    tab: [0, 0, 1, 1, 2, 2, 0, 1, 2, 1, 1, 2, 1, 1, 1, 1, 1, 2, 0, 2, 2, 2, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 6,
    couleurs: ['rgb(222, 161, 73)', 'rgb(38, 38, 56)', 'rgb(236, 226, 226)'],
    tab: [0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 2, 0, 0, 2, 1, 1, 0, 0, 0, 0, 1, 0, 0, 2, 2, 0, 0, 1, 0, 2, 2, 0, 1]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(112, 231, 224)', 'rgb(252, 11, 0)', 'rgb(59, 154, 2)'],
    tab: [0, 0, 1, 0, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 2, 2, 0, 0, 0, 0, 2, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(255, 255, 255)', 'rgb(225, 149, 33)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 1, 1, 0, 0, 2, 0, 1, 1, 2, 0, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 2, 0, 1, 1, 1, 1, 1, 1, 1]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(252, 11, 0)', 'rgb(0, 23, 255)', 'rgb(255, 231, 24)', 'rgb(253, 70, 253)'],
    tab: [0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 2, 1, 2, 2, 2, 1, 2, 2, 1, 2, 2, 2, 3, 3, 3, 1, 3, 3, 1, 3, 3, 3, 1, 3]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(252, 11, 0)', 'rgb(0, 23, 255)', 'rgb(255, 231, 24)', 'rgb(253, 70, 253)'],
    tab: [0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 2, 2, 2, 3, 3, 3, 2, 2, 2, 3, 3, 3]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(255, 231, 24)', 'rgb(252, 11, 0)', 'rgb(0, 23, 255)', 'rgb(253, 70, 253)'],
    tab: [0, 1, 1, 2, 2, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 2, 2, 2, 0, 0, 0, 3, 3, 3, 0, 0, 0, 3, 3, 3, 3, 0, 0, 3, 3, 2]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(252, 11, 0)', 'rgb(0, 23, 255)', 'rgb(255, 231, 24)', 'rgb(253, 70, 253)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 1, 1, 0, 2, 1, 1, 2, 2, 2, 2, 1, 1, 2, 3, 1, 1, 2, 2, 1, 3, 3, 3, 3, 3, 3, 3]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(252, 11, 0)', 'rgb(180, 0, 211)', 'rgb(255, 255, 255)', 'rgb(59, 154, 2)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 1, 1, 1, 2, 0, 0, 0, 1, 1, 2, 0, 0, 0, 1, 1, 2, 3, 3, 4, 4, 4, 2, 3, 3, 3, 4, 4, 2, 3, 3, 3, 4, 4, 2]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 6,
    couleurs: ['rgb(252, 11, 0)', 'rgb(180, 0, 211)', 'rgb(255, 255, 255)', 'rgb(59, 154, 2)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 0, 1, 1, 2, 3, 4, 1, 2, 0, 2, 3, 4, 1, 2, 0, 3, 3, 4, 1, 2, 0, 3, 3, 4, 1, 2, 0, 4, 3, 4, 1, 2, 0, 4]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 7,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 7,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 7,
    couleurs: ['rgb(254, 193, 7)', 'rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 2, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 7,
    couleurs: ['rgb(0, 0, 0)', 'rgb(252, 11, 0)', 'rgb(0, 23, 255)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 2, 2, 1, 2, 2, 0, 0, 1, 1, 1, 1, 1, 0, 0, 2, 2, 1, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 7,
    couleurs: ['rgb(255, 255, 255)', 'rgb(255, 231, 24)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 1, 1, 1, 1, 0, 1, 1, 2, 1, 2, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 0, 1, 1, 1, 1, 1, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 7,
    couleurs: ['rgb(255, 255, 255)', 'rgb(255, 231, 24)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 1, 1, 1, 1, 0, 1, 1, 2, 1, 2, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 2, 2, 2, 1, 1, 0, 1, 1, 1, 1, 1, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 7,
    couleurs: ['rgb(255, 255, 255)', 'rgb(255, 231, 24)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 1, 1, 1, 1, 0, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 2, 2, 2, 1, 1, 0, 1, 1, 1, 1, 1, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 7,
    couleurs: ['rgb(255, 255, 255)', 'rgb(255, 231, 24)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 1, 1, 1, 1, 0, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 2, 1, 1, 1, 2, 1, 0, 1, 1, 1, 1, 1, 0]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 7,
    couleurs: ['rgb(255, 231, 24)', 'rgb(59, 154, 2)', 'rgb(0, 23, 255)'],
    tab: [0, 0, 1, 1, 2, 2, 0, 2, 0, 0, 1, 1, 2, 2, 2, 2, 0, 0, 1, 1, 2, 1, 2, 2, 0, 0, 1, 1, 1, 1, 2, 2, 0, 0, 1, 0, 1, 1, 2, 2, 0, 0, 0, 0, 1, 1, 2, 2, 0]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 7,
    couleurs: ['rgb(255, 231, 24)', 'rgb(252, 11, 0)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 2, 2, 2, 1, 0, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 7,
    couleurs: ['rgb(0, 23, 255)', 'rgb(225, 123, 1)', 'rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 1, 1, 1, 1, 1, 0, 0, 1, 2, 1, 2, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 3, 2, 3, 3, 3, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0]
  },
  {
    InvGaucheDroite: true,
    t: 7,
    couleurs: ['rgb(0, 23, 255)', 'rgb(255, 255, 255)', 'rgb(0, 0, 0)', 'rgb(225, 123, 1)'],
    tab: [0, 0, 1, 2, 0, 0, 0, 0, 1, 1, 2, 1, 0, 0, 3, 3, 0, 2, 1, 1, 0, 3, 3, 0, 2, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 0, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0]
  },
  {
    t: 7,
    couleurs: ['rgb(225, 123, 1)', 'rgb(0, 23, 255)', 'rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 2, 3, 2, 1, 1, 1, 0, 0, 3, 0, 0, 1, 1, 3, 2, 2, 2, 3, 1, 3, 3, 1, 1, 1, 3, 3]
  },
  {
    InvGaucheDroite: true,
    t: 7,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)', 'rgb(0, 23, 255)', 'rgb(225, 123, 1)'],
    tab: [0, 1, 1, 2, 1, 1, 0, 0, 2, 1, 2, 1, 2, 0, 2, 2, 1, 2, 1, 2, 2, 2, 3, 3, 3, 3, 3, 2, 3, 1, 0, 3, 1, 0, 3, 3, 0, 0, 3, 0, 0, 3, 3, 3, 1, 1, 1, 3, 3]
  },
  {
    InvGaucheDroite: true,
    t: 7,
    couleurs: ['rgb(0, 23, 255)', 'rgb(225, 123, 1)', 'rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 2, 2, 1, 1, 1, 2, 2, 0, 0, 2, 3, 2, 0, 0, 0, 1, 1, 3, 1, 1, 0, 0, 1, 3, 2, 2, 1, 0, 0, 1, 1, 1, 1, 1, 0]
  },
  {
    tourneD: true,
    changeCoul: false,
    invHautBas: true,
    InvGaucheDroite: false,
    t: 7,
    couleurs: ['rgb(0, 0, 0)', 'rgb(0, 23, 255)', 'rgb(255, 255, 255)', 'rgb(255, 231, 24)', 'rgb(252, 11, 0)'],
    tab: [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 2, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 4, 4, 4, 1, 0, 0, 3, 4, 4, 4, 3, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 7,
    couleurs: ['rgb(121, 83, 72)', 'rgb(255, 255, 255)', 'rgb(97, 97, 97)', 'rgb(0, 0, 0)', 'rgb(250, 152, 3)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 2, 1, 1, 1, 1, 1, 2, 2, 3, 1, 1, 1, 3, 2, 2, 3, 1, 4, 1, 3, 2, 2, 1, 1, 4, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(255, 231, 24)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: true,
    invHautBas: true,
    t: 8,
    couleurs: ['rgb(0, 0, 0)', 'rgb(255, 231, 24)'],
    tab: [0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 8,
    couleurs: ['rgb(255, 231, 24)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(0, 0, 0)', 'rgb(0, 23, 255)', 'rgb(252, 11, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 2, 2, 2, 0, 2, 2, 2]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(0, 23, 255)', 'rgb(255, 231, 24)', 'rgb(59, 154, 2)'],
    tab: [0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 2, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 2, 0, 2, 0, 2, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(192, 192, 192)', 'rgb(0, 0, 0)', 'rgb(255, 255, 255)', 'rgb(252, 11, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 2, 0, 3, 0, 3, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(225, 123, 1)', 'rgb(245, 174, 104)', 'rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 2, 3, 1, 2, 3, 1, 0, 1, 3, 3, 1, 3, 3, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 0, 3, 1, 1, 1, 1, 1, 1, 3]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(192, 192, 192)', 'rgb(59, 154, 2)', 'rgb(0, 0, 0)', 'rgb(252, 11, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 2, 1, 2, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(192, 192, 192)', 'rgb(148, 91, 2)', 'rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 2, 1, 2, 1, 2, 2, 2, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(59, 154, 2)', 'rgb(192, 192, 192)', 'rgb(0, 0, 0)', 'rgb(255, 231, 24)'],
    tab: [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 2, 2, 2, 1, 1, 1, 1, 0, 0, 3, 3, 3, 3, 3, 3, 0, 0, 3, 3, 3, 3, 3, 3, 0, 0, 3, 3, 3, 3, 3, 3, 0, 0, 0, 1, 1, 1, 1, 3, 0, 0, 0, 1, 1, 1, 1, 1]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(59, 154, 2)', 'rgb(192, 192, 192)', 'rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 0, 0, 0, 1, 1, 1, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 1, 1, 1, 3, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(76, 96, 119)', 'rgb(192, 192, 192)', 'rgb(0, 0, 0)', 'rgb(255, 255, 255)'],
    tab: [0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 3, 0, 0, 3, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(0, 23, 255)', 'rgb(255, 255, 255)', 'rgb(0, 0, 0)', 'rgb(252, 11, 0)'],
    tab: [0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 2, 0, 1, 2, 0, 0, 0, 3, 0, 0, 3, 0, 0, 0, 0, 3, 0, 0, 3, 0, 0, 0, 3, 3, 3, 3, 3, 0, 3, 3, 3, 3, 2, 2, 3, 3, 3, 0, 3, 3, 3, 3, 3, 0, 3, 3, 3, 0, 3, 0, 3, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(76, 96, 119)', 'rgb(148, 91, 2)', 'rgb(245, 174, 104)', 'rgb(0, 0, 0)', 'rgb(252, 11, 0)'],
    tab: [0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 2, 3, 2, 3, 0, 0, 0, 0, 2, 2, 2, 2, 0, 0, 0, 0, 3, 3, 3, 3, 3, 0, 0, 0, 3, 2, 2, 2, 3, 0, 0, 0, 4, 4, 4, 4, 0, 0]
  },
  {
    tourneD: true,
    changeCoul: false,
    invHautBas: true,
    InvGaucheDroite: true,
    t: 8,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)', 'rgb(0, 23, 255)', 'rgb(225, 123, 1)', 'rgb(255, 231, 24)'],
    tab: [0, 0, 0, 1, 1, 1, 2, 2, 0, 0, 1, 1, 1, 2, 0, 2, 0, 1, 1, 1, 2, 0, 2, 1, 1, 3, 3, 2, 0, 2, 1, 1, 1, 1, 3, 2, 2, 1, 1, 1, 1, 3, 4, 3, 3, 1, 1, 4, 3, 4, 3, 1, 3, 1, 4, 4, 4, 3, 1, 1, 1, 4, 4, 4]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 9,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 9,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: true,
    invHautBas: true,
    InvGaucheDroite: false,
    t: 9,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 9,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)', 'rgb(252, 11, 0)'],
    tab: [0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 2, 2, 0, 0, 1, 0, 1, 2, 0, 2, 2, 0, 2, 0, 1, 1, 2, 2, 0, 0, 2, 2, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 9,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)', 'rgb(252, 11, 0)'],
    tab: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 2, 2, 1, 2, 2, 1, 0, 0, 1, 2, 2, 2, 2, 2, 1, 0, 0, 1, 2, 2, 2, 2, 2, 1, 0, 0, 0, 1, 2, 2, 2, 1, 0, 0, 0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 9,
    couleurs: ['rgb(0, 23, 255)', 'rgb(255, 231, 24)', 'rgb(0, 0, 0)', 'rgb(180, 0, 211)'],
    tab: [0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 2, 2, 2, 2, 1, 0, 0, 0, 1, 3, 1, 1, 3, 1, 0, 2, 0, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0, 1, 1, 3, 3, 1, 0, 2, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 9,
    couleurs: ['rgb(93, 165, 216)', 'rgb(44, 77, 224)', 'rgb(0, 225, 255)', 'rgb(252, 208, 168)'],
    tab: [0, 0, 1, 1, 1, 2, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 2, 1, 3, 3, 1, 0, 0, 0, 0, 0, 1, 3, 3, 3, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 2, 0, 2, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 9,
    couleurs: ['rgb(255, 255, 255)', 'rgb(33, 150, 243)', 'rgb(0, 0, 0)', 'rgb(255, 23, 58)', 'rgb(183, 28, 28)'],
    tab: [0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 2, 2, 1, 2, 2, 1, 1, 1, 2, 0, 3, 2, 3, 3, 2, 1, 1, 2, 3, 3, 3, 3, 3, 2, 1, 1, 2, 3, 3, 3, 3, 4, 2, 1, 1, 1, 2, 3, 3, 4, 2, 1, 1, 1, 1, 1, 2, 4, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 10,
    couleurs: ['rgb(255, 255, 255)', 'rgb(0, 0, 0)'],
    tab: [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0]
  },
  {
    changeCoul: true,
    invHautBas: false,
    InvGaucheDroite: false,
    t: 10,
    couleurs: ['rgb(255, 255, 255)', 'rgb(252, 11, 0)', 'rgb(0, 0, 0)'],
    tab: [0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 2, 0, 0, 0, 0, 2, 0, 1, 1, 2, 2, 2, 0, 0, 2, 2, 2, 1, 1, 0, 2, 0, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0, 2, 2, 0, 0, 0, 1, 1, 0, 0, 2, 2, 2, 2, 0, 0, 1, 1, 2, 0, 0, 2, 2, 0, 0, 2, 1, 1, 0, 2, 0, 0, 0, 0, 2, 0, 1, 1, 0, 0, 2, 2, 2, 2, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 10,
    couleurs: ['rgb(0, 23, 255)', 'rgb(148, 91, 2)', 'rgb(0, 0, 0)', 'rgb(252, 11, 0)'],
    tab: [0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 2, 0, 0, 0, 2, 2, 0, 2, 2, 3, 3, 2, 0, 0, 2, 3, 2, 3, 3, 3, 1, 3, 2, 0, 2, 3, 2, 2, 3, 3, 3, 3, 2, 0, 2, 2, 0, 0, 2, 3, 3, 3, 2, 0, 2, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0]
  },
  {
    changeCoul: false,
    invHautBas: false,
    InvGaucheDroite: true,
    t: 10,
    couleurs: ['rgb(255, 231, 24)', 'rgb(0, 23, 255)', 'rgb(59, 154, 2)', 'rgb(252, 11, 0)', 'rgb(148, 91, 2)'],
    tab: [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 2, 2, 2, 1, 1, 1, 0, 0, 1, 2, 2, 2, 3, 2, 1, 1, 1, 1, 1, 2, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 4, 2, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 4, 1, 1, 1, 1, 1, 1, 1, 2, 4, 4, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
  }
]
