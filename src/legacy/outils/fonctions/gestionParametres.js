import { j3pNombre, j3pGetRandomInt, j3pGetBornesIntervalle, j3pGetRandomFixed } from 'src/legacy/core/functions'
import { captureIntervalleFermeDecimaux, testIntervalleFermeDecimaux } from 'src/lib/utils/regexp'
import { j3pExtraireNumDen } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'

/**
 * @module legacy/outils/fonctions/gestionParametres
 */

/**
 * Génère un nombre aléatoire compris dans l’intervalle (appelle j3pGetRandomInt ou j3pGetRandomFixed suivant les bornes).
 * @param {string} intervalle Intervalle (fermé) avec des bornes entières (ça retournera un entier)
 * ou décimales (ça retournera un décimal avec au max le même nb de décimales)
 * @return {number}
 */
export function genereAlea (intervalle) {
  if (typeof intervalle !== 'string' || !intervalle.startsWith('[')) {
    console.error(Error('Appel incorrect de genereAlea'), intervalle)
    // on laisse comme c'était, ça retournait l’intervalle passé en supposant que c'était déjà un nombre
    return intervalle
  }
  // si les bornes sont entières, alors on génère un nombre entier aléatoire dans cet intervalle
  // si elles sont décimales, il faut que je génère un décimal de l’intervalle
  const [borneInf, borneSup] = j3pGetBornesIntervalle(intervalle, { alsoFloat: true })
  if (Number.isInteger(borneInf) && Number.isInteger(borneSup)) return j3pGetRandomInt(borneInf, borneSup)
  // on regarde la partie décimale la plus longue pour retourner un décimal de cette longueur
  const getDecPartLength = nb => String(nb).replace(/^.+\./, '').length
  const nbDecimales = Math.max(getDecPartLength(borneInf), getDecPartLength(borneSup))
  return j3pGetRandomFixed(borneInf, borneSup, nbDecimales)
}

/**
 * Retourne la taille de l’intervalle fermé (différence entre les bornes), 0 si ce n’était pas un intervalle
 * @private
 * @param {string} intervalle sous la forme [xxx;yyy] où xxx et yyy sont des nombres décimaux (séparateur virgule ou point)
 * @return {number}
 */
function lgIntervalle (intervalle) {
  // cette fonction renvoie la longueur de l’intervalle'
  // si ce n’est pas un intervalle, alors elle renvoie 0'
  let lg = 0
  const chunks = captureIntervalleFermeDecimaux.exec(intervalle.trim())
  if (chunks) {
    // on a bien un intervalle
    const [, borneInf, borneSup] = chunks
    lg = j3pNombre(borneInf) - j3pNombre(borneSup)
  }
  return lg
}

/**
 * Teste si la valeur correspondant à nb est dans le tableau tab (ses valeurs pouvant être au format latex)
 * @private
 * @param {number} nb
 * @param {string[]|number[]} tab
 * @return {boolean}
 */
function nbDansTab (nb, tab) {
  if (!tab) return false
  let puisPrecision = 15
  if (Math.abs(nb) > Math.pow(10, -13)) {
    const ordrePuissance = Math.floor(Math.log(Math.abs(nb)) / Math.log(10)) + 1
    puisPrecision = 15 - ordrePuissance
  }
  for (let k = 0; k < tab.length; k++) {
    if (Math.abs(nb - j3pCalculValeur(tab[k])) < Math.pow(10, -puisPrecision)) {
      return true
    }
  }
  return false
}

/**
 * Vérifie si l’intervalle passé en paramètre est bien un intervalle ou un nombre (éventuellement fractionnaire) et ce plusieurs fois (en général nbrepetitions).
 * Cela permet de valider ce qui peut être saisi par un utilisateur dans le cas où on peut imposer des valeurs.
 * Si texteIntervalle est un intervalle, la fonction va dans un second temps générer de manière aléatoire une valeur de cet intervalle.
 * @param {string} texteIntervalle éléments qui peuvent être séparés par '|', où chaque élément est un nombre, une fraction ou un intervalle
 * @param {string} intervalleDefaut
 * @param {number} nbRepet
 * @param {} tabValInterdites valeurs interdites éventuelles
 * @return {string[]} nbRepet éléments, chaque élément est un intervalle (ou l’intervalle par défaut si ce qui est précisé n’est pas compris), ou un nombre (entier, décimal ou fractionnaire). Dans ce dernier cas, on renvoit une chaine où on retrouve le code latex et la valeur décimale (arrondie) séparés par |
 */
export function constructionTabIntervalle (texteIntervalle, intervalleDefaut, nbRepet, tabValInterdites) {
  /* quelle fonction ????
   cette fonction renvoie un objet. Cet objet contient 3 tableaux de nbRepet éléments
    - leType[k] vaut 'nombre', 'fraction' ou 'intervalle' suivant la valeur de texteIntervalle
    - expression contient l’écriture de chaque valeur imposée ou gérée aléatoirement quand leType[k] vaut 'intervalle'
    - valeur contient la valeur décimale de ces nombres, cela ne peut être utile que lorsque leType[k] vaut 'fraction'
    */

  if (typeof texteIntervalle !== 'string') {
    console.error(Error('intervalle invalide, sera converti en string'), typeof texteIntervalle, texteIntervalle)
    texteIntervalle = String(texteIntervalle)
  }
  const nbFracReg = /^-?[0-9]+\/[0-9]+$/
  let k
  const DonneesIntervalle = {}
  // DonneesIntervalle contient trois propriétés qui sont des tableaux de longueur nbRepet :
  // - leType : qui correspond à ce qui a été renseigné initialement ('intervalle', 'nombre', 'fraction') - 'nombre' désigne un entier ou un décimal
  // - expression : qui est le nombre (si type=='nombre'), une valeur générée aléatoirement dans un intervalle (si type=='intervalle'), l’écriture l’atex de la fraction (si type=='fraction')
  // - valeur : qui est la valeur décimale de l’expression (utile surtout si type=='fraction')
  DonneesIntervalle.leType = []
  DonneesIntervalle.expression = []
  DonneesIntervalle.valeur = []
  let nbAlea, valNb, valFrac, tabDecomp, monnum, monden
  if (texteIntervalle.indexOf('|') > -1) {
    const tableau = texteIntervalle.split('|')
    for (k = 0; k < nbRepet; k++) {
      if (testIntervalleFermeDecimaux.test(tableau[k])) {
        // c’est bien un intervalle'
        if (lgIntervalle(tableau[k]) === 0) {
          // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
          valNb = tableau[k].substring(1, tableau[k].indexOf(';'))
          DonneesIntervalle.expression.push(valNb)
          DonneesIntervalle.leType.push('nombre')
          DonneesIntervalle.valeur.push(valNb)
        } else {
          do {
            nbAlea = genereAlea(tableau[k])
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      } else {
        // peut être est-ce tout simplement un nombre :
        if ((tableau[k] === undefined) || (tableau[k] === '')) {
          // là l’expression n’est pas renseignée
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        } else if (nbFracReg.test(tableau[k])) {
          // on a un nb fractionnaire
          // on récupère numérateur et dénominateur
          tabDecomp = tableau[k].split('/')
          monnum = j3pNombre(tabDecomp[0])
          monden = j3pNombre(tabDecomp[1])
          valFrac = monnum / monden
          if (monnum < 0) {
            monnum = -monnum
          }
          DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
          DonneesIntervalle.leType.push('fraction')
          DonneesIntervalle.valeur.push(valFrac)
        } else if (!isNaN(j3pNombre(tableau[k]))) {
          // on a un nombre
          DonneesIntervalle.expression.push(tableau[k])
          DonneesIntervalle.leType.push('nombre')
          DonneesIntervalle.valeur.push(tableau[k])
        } else {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      }
    }
  } else {
    if ((texteIntervalle === '') || (texteIntervalle === undefined)) {
      for (k = 0; k < nbRepet; k++) {
        do {
          nbAlea = genereAlea(intervalleDefaut)
        } while (nbDansTab(nbAlea, tabValInterdites))
        DonneesIntervalle.expression.push(nbAlea)
        DonneesIntervalle.leType.push('intervalle')
        DonneesIntervalle.valeur.push(nbAlea)
      }
    } else if (testIntervalleFermeDecimaux.test(texteIntervalle)) {
      if (lgIntervalle(texteIntervalle) === 0) {
        // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
        valNb = texteIntervalle.substring(1, texteIntervalle.indexOf(';'))
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push(valNb)
          DonneesIntervalle.leType.push('nombre')
          DonneesIntervalle.valeur.push(valNb)
        }
      } else {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(texteIntervalle)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      }
    } else if (nbFracReg.test(texteIntervalle)) {
      // on a un nb fractionnaire
      // on récupère numérateur et dénominateur
      tabDecomp = texteIntervalle.split('/')
      monnum = j3pNombre(tabDecomp[0])
      monden = j3pNombre(tabDecomp[1])
      valFrac = monnum / monden
      if (monnum < 0) {
        monnum = -monnum
      }
      for (k = 0; k < nbRepet; k++) {
        DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
        DonneesIntervalle.leType.push('fraction')
        DonneesIntervalle.valeur.push(valFrac)
      }
    } else if (!isNaN(j3pNombre(texteIntervalle))) {
      // on a un nombre
      for (k = 0; k < nbRepet; k++) {
        DonneesIntervalle.expression.push(texteIntervalle)
        DonneesIntervalle.leType.push('nombre')
        DonneesIntervalle.valeur.push(texteIntervalle)
      }
    } else {
      for (k = 0; k < nbRepet; k++) {
        do {
          nbAlea = genereAlea(intervalleDefaut)
        } while (nbDansTab(nbAlea, tabValInterdites))
        DonneesIntervalle.expression.push(nbAlea)
        DonneesIntervalle.leType.push('intervalle')
        DonneesIntervalle.valeur.push(nbAlea)
      }
    }
  }
  return DonneesIntervalle
}

/**
 * Cette fonction renvoie la valeur d’un calcul ou la fraction dans laquelle apparaît un calcul
 * @param {Object} [obj] objet contenant les propriétés text et obj
 * @param {string} [obj.texte] texte avec des variables écrites sous la forme £a (comme pour j3pAffiche). Par exemple text:'2*£a+£b'
 * @param {Object} [obj.obj] objet donnant les valeurs de chaque variable (comme pour j3pAffiche). Par exemple obj:{a:'2',b:'-3'}
 * @return {string} chaîne de caractère pour une fraction (number autrement) correspondant au calcul en ayant remplacé les valeurs présentes dans obj
 */
export function remplaceCalcul (obj) {
  if (obj.text === '') return ''
  let newTxt = obj.text
  let pos1
  while (newTxt.includes('£')) {
    // on remplace £x par sa valeur
    pos1 = newTxt.indexOf('£')
    const laVariable = newTxt.charAt(pos1 + 1)
    const nombre = obj.obj[laVariable]
    newTxt = newTxt.substring(0, pos1) + '(' + nombre + ')' + newTxt.substring(pos1 + 2)
  }
  if (newTxt.includes('frac')) {
    const numden = j3pExtraireNumDen(newTxt)
    return '\\frac{' + Math.round(Math.pow(10, 10) * j3pCalculValeur(numden[1])) / Math.pow(10, 10) + '}{' + Math.round(Math.pow(10, 10) * j3pCalculValeur(numden[2])) / Math.pow(10, 10) + '}'
  } else {
    return Math.round(Math.pow(10, 10) * j3pCalculValeur(newTxt)) / Math.pow(10, 10)
  }
}
