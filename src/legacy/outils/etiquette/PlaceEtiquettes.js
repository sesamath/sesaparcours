import { j3pAddContent, j3pAddElt, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pGetNewId, j3pSpan } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ecouteImg from 'src/legacy/sections/college/etikMake/ecoute.png'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { barreZone } from 'src/legacy/outils/zoneStyleMathquill/functions'

import './etiquette.css'

/* global j3p */

/**
 * À décrire
 * @param {string} iddivconteneur à décrire
 * @param {string} manouvellediv à décrire
 * @param {Etiquettes} objetiquette à décrire
 * @param {Object} [parametres]
 * @param {number} [parametres.contraintecontenu=0] à décrire
 * @param {string} [parametres.couleur] si non fourni ce sera la couleur de objetiquette
 * @param {string} [parametres.sansBord] true ou false
 * @constructor
 */
function PlaceEtiquettes (iddivconteneur, manouvellediv, objetiquette, parametres = {}) {
  /**
   * @type {Etiquettes}
   */
  this.et = objetiquette
  this.contraintecontenu = parametres.contraintecontenu || 0
  this.colonne = objetiquette.colonne
  this.couleur = parametres.couleur || objetiquette.couleur
  this.etiq = -1
  this.contenu = ''
  this.sansBord = parametres.sansBord
  if (typeof iddivconteneur === 'string') iddivconteneur = j3pElement(iddivconteneur)
  this.iddivconteneur = iddivconteneur
  this.manouvellediv = manouvellediv
  this.spanContenant = j3pDiv(iddivconteneur)
  this.spanContenant.style.display = 'inline-block'
  this.parent = objetiquette.parent
  this.mepact = objetiquette.mepact
  this.mtgAppLecteur = objetiquette.mtgAppLecteur
  this.Plazetik = j3pSpan(this.spanContenant)
  this.Plazetik.style.gridRow = '1'
  this.Plazetik.style.fontSize = '20px'
  this.Plazetik.style.fontFamily = 'Roboto, "Noto Emoji", sans-serif'
  this.Plazetik.style.gridColumn = '1'
  this.Plazetik.style.verticalAlign = 'middle'
  this.placeAide = j3pSpan(this.spanContenant)
  this.placeAide.style.gridRow = '2'
  this.placeAide.style.gridColumn = '1'
  this.Plazetik.classList.add('emplcement1')
  if (this.sansBord) {
    this.Plazetik.style.border = '0px solid black'
  }
  this.faislazet()
  this.Plazetik.style.background = this.couleur
  this.maFoncVide = this.vide.bind(this)
  this.spanContenant.addEventListener('click', this.maFoncVide, false)
  if (parametres.heightFix) {
    this.Plazetik.style.minHeight = parametres.heightFix + 'px'
    this.Plazetik.style.height = parametres.heightFix + 'px'
  }
  objetiquette.envoieCoo(0, 0, this)
}

PlaceEtiquettes.prototype.returnco = function returnco () {
  const a = this.spanContenant.getBoundingClientRect()
  const { left, top } = this.parent.getBoundingClientRect()
  this.et.envoieCoo(a.left - left + this.spanContenant.offsetWidth / 2, a.top - top + this.spanContenant.offsetHeight / 2, this)
}

PlaceEtiquettes.prototype.hideShow = function hideShow (bool) {
  this.spanContenant.style.display = (bool) ? 'none' : ''
}

PlaceEtiquettes.prototype.annulecolor = function annulecolor () {
  this.spanContenant.style.color = ''
}

PlaceEtiquettes.prototype.vide = function vide () {
  if (this.disabled) return
  this.bon = undefined
  if (this.etiq === -1) return
  this.et.etiqok[this.etiq] = true
  this.contenu = ''
  const io = Math.ceil((this.etiq + 1) / this.et.colonne) - 1
  const j = (((this.etiq + 1) - 1) % (this.et.colonne)) * 2
  if (this.et.tabEtikDou[io][j] !== null) {
    if (!this.et.unique) {
      this.et.tabEtikDou[io][j].style.visibility = undefined
    } else {
      this.et.tabEtikDou[io][j].style.visibility = 'visible'
    }
  }
  this.spanContenant.removeEventListener('click', this.maFoncVide, false)
  this.etiq = -1
  this.faislazet()
  this.spanContenant.style.color = ''
  this.spanContenant.addEventListener('click', this.maFoncVide, false)
  if (this.et.afaire) this.et.afaire(this)
}

PlaceEtiquettes.prototype.faislazet = function faislazet () {
  this.Plazetik.classList.remove('Pointer')
  j3pEmpty(this.Plazetik)
  this.hjkl = undefined
  for (let mm = 0; mm < this.et.montab.length; mm++) {
    const div2 = j3pDiv(this.Plazetik)
    div2.setAttribute('class', 'etiquetteCache')
    let buf2 = this.et.montab[mm]
    if (Array.isArray(buf2)) {
      // en fait ca peut etre un tableau de tableau si y’a des accords a prévoir (s au pluriel)
      buf2 = buf2[0]
    }
    if (typeof buf2 === 'string') {
      j3pAffiche(div2, null, buf2)
    } else {
      const el = buf2
      switch (el.type) {
        case undefined:
        case 'mathgraph': {
          const ui = addDefaultTable(div2, 1, 1)[0][0]
          const newId = j3pGetNewId('mtgetih')
          const svg = j3pCreeSVG(ui, { id: newId, width: 37 + el.content.width * 340, height: 54 + el.content.height * 253 })
          this.mtgAppLecteur.addDoc(newId, el.content.txtFigure, true)
          this.mtgAppLecteur.calculate(newId)
          this.mtgAppLecteur.display(newId)
          svg.style.position = 'relative'
        }
          break
        case 'image': {
          const limm = j3pAddElt(div2, 'img', '', { src: el.content.txtFigure, width: el.content.width, height: el.content.height })
          limm.style.width = el.content.width + 'px'
          limm.style.height = el.content.height + 'px'
          limm.style.marginTop = '2px'
          limm.style.marginBottom = '2px'
        }
          break
        case 'son': {
          const ui2 = addDefaultTable(div2, 1, 1)[0][0]
          const limm = j3pAddElt(ui2, 'img', '', { src: ecouteImg, width: '20px', height: '20px' })
          limm.style.width = '20px'
          limm.style.height = '20px'
          j3pAddContent(ui2, 'n° ' + mm)
        }
      }
    }
  }
  this.lint = j3pDiv(this.Plazetik)
  const mm = j3pAddElt(this.lint, 'span')
  j3pAddContent(mm, (this.et.codePlace) ? ('<i>..' + this.et.codePlace + '..</i>') : '...')
  mm.style.color = '#4b4b4b'
}

PlaceEtiquettes.prototype.ecris = function ecris () {
  let buf = this.contenu
  if (Array.isArray(buf)) { buf = buf[this.contraintecontenu] }
  this.faislazet()
  j3pEmpty(this.lint)

  if (typeof buf === 'string') {
    const hjkl = j3pAffiche(this.lint, null, buf)
    for (let uu = 0; uu < hjkl.mqList.length; uu++) {
      if (hjkl.mqList[uu] !== null) hjkl.mqList[uu].style.cursor = 'pointer'
    }
  } else {
    const el = buf
    switch (el.type) {
      case undefined:
      case 'mathgraph': {
        const ui = addDefaultTable(this.lint, 1, 1)[0][0]
        const newId = j3pGetNewId('mtgetih')
        j3pCreeSVG(ui, { id: newId, width: 37 + el.content.width * 340, height: 54 + el.content.height * 253 })
        this.mtgAppLecteur.addDoc(newId, el.content.txtFigure, true)
        this.mtgAppLecteur.calculate(newId)
        this.mtgAppLecteur.display(newId)
      }
        break
      case 'image': {
        const limm = j3pAddElt(this.lint, 'img', '', { src: el.content.txtFigure, width: el.content.width, height: el.content.height })
        limm.style.width = el.content.width + 'px'
        limm.style.height = el.content.height + 'px'
        limm.style.marginTop = '2px'
        limm.style.marginBottom = '2px'
      }
        break
      case 'son': {
        const ui2 = addDefaultTable(this.lint, 1, 1)[0][0]
        const limm = j3pAddElt(ui2, 'img', '', { src: ecouteImg, width: '20px', height: '20px' })
        limm.style.width = '20px'
        limm.style.height = '20px'
        j3pAddContent(ui2, 'n° ' + this.etiq)
      }
    }
  }
  this.Plazetik.classList.add('Pointer')
  this.spanContenant.addEventListener('click', this.maFoncVide, false)
  if (this.et.afaire) this.et.afaire(this)
}

PlaceEtiquettes.prototype.detruit = function detruit () {
  j3pDetruit(this.spanContenant)
  if (this.etiq !== -1) {
    const io = Math.ceil((this.etiq + 1) / this.colonne) - 1
    const j = ((this.etiq + 1) - 1) % (this.colonne)
    this.et.tabEtikDou[io][j].style.visibility = undefined
  }

  this.et.supprimemoi(this)
}

PlaceEtiquettes.prototype.corrige = function corrige (bool, bool2) {
  this.spanContenant.style.color = bool ? j3p.styles.cbien : j3p.styles.cfaux
  this.bon = bool
  if (bool2) {
    if (!this.isbarred) {
      this.isbarred = true
      return barreZone(this.spanContenant.parentNode)
    }
  }
}

PlaceEtiquettes.prototype.barre = function barre () {
  return barreZone(this.Plazetik)
}

PlaceEtiquettes.prototype.barreIfKo = function barreIfKo () {
  if (this.bon === false) return barreZone(this.spanContenant.parentNode)
}

PlaceEtiquettes.prototype.disable = function disable () {
  if (this.hjkl !== undefined) {
    for (let uu = 0; uu < this.hjkl.mqList.length; uu++) this.hjkl.mqList[uu].style.cursor = 'default'
  }
  this.disabled = true
  this.spanContenant.removeEventListener('click', this.maFoncVide, false)
  this.Plazetik.setAttribute('class', 'etiquette2 etiquette0')
}

export default PlaceEtiquettes
