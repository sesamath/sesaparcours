import { j3pIsHtmlElement } from 'src/legacy/core/functions'

/**
 * Retourne un tableau [left, top, width, height]
 * Utiliser plutôt directement [elt.getBoundingClientRect()]{@link https://developer.mozilla.org/fr/docs/Web/API/Element/getBoundingClientRect} pour récupérer les propriétés dont vous avez besoin
 * @deprecated
 * @param elt
 * @return {number[]}
 */
function findPos (elt) {
  if (!j3pIsHtmlElement(elt)) throw Error('élément invalide')
  const { left, top, width, height } = elt.getBoundingClientRect()
  return [left, top, width, height]
}

export default findPos
