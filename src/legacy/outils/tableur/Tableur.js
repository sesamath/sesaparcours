import { j3pAjouteTableau, j3pAjouteZoneTexte, j3pDiv, j3pElement } from 'src/legacy/core/functions'
import { legacyStyles } from 'src/legacy/core/StylesJ3p'

/**
JP Vanroyen
Juillet 2013
 */
/**
 * Outil tableur, utilisé dans une seule section (tableurbrevet2013)
 * @param conteneur
 * @param objet
 * @returns {Tableur}
 * @constructor
 */
function Tableur (conteneur, objet) {
  if (typeof this !== 'object') throw Error('Constructeur qui doit être appelé avec new')
  const lettres = ['', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
  let j, k
  /*
     objet = {
        nbcol : 5,
        nblig : 3,
        largeur : 70
     }
     */

  this.tab = {}
  this.tab.nblig = objet.nblig
  this.tab.nbcol = objet.nbcol
  this.tab.id = objet.id

  if (typeof (objet.largeur) === 'undefined') this.tab.largeur = '70px'
  else this.tab.largeur = objet.largeur + 'px'
  if (typeof (objet.hauteur) === 'undefined') this.tab.hauteur = '20px'
  else this.tab.hauteur = objet.hauteur + 'px'

  const letabid = []
  for (k = 0; k <= objet.nblig; k++) {
    const tb = []
    for (j = 0; j <= objet.nbcol; j++) {
      tb.push('a' + k + '_' + j)
    }
    letabid.push(tb)
  }
  j3pAjouteTableau(conteneur, { id: objet.id, nbli: objet.nblig + 1, nbcol: objet.nbcol + 1, tabid: letabid, tabcss: [] })

  const taillepolice = '20px'
  for (k = 0; k <= objet.nblig; k++) {
    for (j = 0; j <= objet.nbcol; j++) {
      j3pElement(letabid[k][j]).style.width = this.tab.largeur
      j3pElement(letabid[k][j]).style.height = this.tab.hauteur
      j3pElement('div' + letabid[k][j]).style.fontSize = taillepolice
      j3pElement('div' + letabid[k][j]).style.fontFamily = 'times new roman'
      j3pElement(letabid[k][j]).style.padding = '0px'
    }
  }

  const couleurfondintitule = '#DDD'
  const couleurintitule = '#8C8C8C'

  j3pElement(letabid[0][0]).style.background = couleurfondintitule
  for (j = 1; j <= objet.nbcol; j++) {
    j3pElement(letabid[0][j]).style.background = couleurfondintitule
    j3pElement('div' + letabid[0][j]).style.color = couleurintitule
    j3pElement('div' + letabid[0][j]).innerHTML = lettres[j]
  }

  for (k = 1; k <= objet.nblig; k++) {
    j3pElement(letabid[k][0]).style.background = couleurfondintitule
    j3pElement('div' + letabid[k][0]).style.color = couleurintitule
    j3pElement('div' + letabid[k][0]).innerHTML = k
  }

  const larg = (this.tab.nbcol + 1) * (2 + Number(this.tab.largeur.substring(0, this.tab.largeur.indexOf('px'))))
  j3pDiv(conteneur, 'divdessus', '', [0, -35],
    legacyStyles.etendre('moyen.explications',
      { width: larg + 'px', height: '35px', border: '1px solid #000', background: '#EEE', padding: '0px' })
  )
  j3pDiv('divdessus', 'divdessus1', '', [0, 0],
    legacyStyles.etendre('moyen.explications',
      {
        width: '50px',
        height: '33px',
        border: '1px solid #000',
        background: '#FFF',
        padding: '2px 0px 0px 10px',
        color: '#009',
        fontFamily: 'times new roman',
        fontStyle: 'italic',
        fontSize: '20px'
      })
  )
  j3pDiv('divdessus', 'divdessus2', '&nbsp;fx', [50, 0],
    legacyStyles.etendre('moyen.explications',
      {
        width: '20px',
        height: '33px',
        border: '1px solid #000',
        background: '#FFF',
        color: '#000',
        fontFamily: 'times new roman',
        fontStyle: 'italic',
        fontSize: '16px',
        padding: '3px 0px 0px 0px'
      })
  )
  j3pDiv('divdessus', 'divdessus3', '', [70, 0],
    legacyStyles.etendre('moyen.explications',
      {
        width: (larg - 70) + 'px',
        height: '33px',
        border: '1px solid #000',
        background: '#FFF',
        padding: '0px',
        color: '#000',
        fontFamily: 'times new roman',
        fontStyle: 'italic',
        fontSize: '16px'
      })
  )
  j3pAjouteZoneTexte('divdessus3', { id: 'zdtformule', texte: '', tailletexte: 16, width: (larg - 84) })
  j3pElement('zdtformule').style.borderWidth = '0px'

  return this
}

Tableur.prototype.select = function (lig, col) {
  const lettres = ['', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
  const id = 'a' + lig + '_' + col
  j3pElement(id).style.border = '2px solid blue'
  j3pElement('a0_' + col).style.background = '#AAA'
  j3pElement('a' + lig + '_0').style.background = '#AAA'
  j3pElement('divdessus1').innerHTML = lettres[col] + lig
}

Tableur.prototype.unselect = function (lig, col) {
  const lettres = ['', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
  const id = 'a' + lig + '_' + col
  j3pElement(id).style.border = ''
  j3pElement(id).style.borderStyle = 'inset'
  j3pElement(id).style.borderColor = 'gray'
  j3pElement(id).style.backgroundColor = 'white'
  j3pElement(id).style.borderWidth = '1px'
  j3pElement('a0_' + col).style.background = '#DDD'
  j3pElement('a' + lig + '_0').style.background = '#DDD'
  j3pElement('divdessus1').innerHTML = lettres[col] + lig
}

Tableur.prototype.ecrit = function (tab) {
  // tab = [{[1,2,ch,style],...]
  for (let k = 0; k < tab.length; k++) {
    j3pElement('diva' + tab[k][0] + '_' + tab[k][1]).innerHTML = tab[k][2]
    if (typeof (tab[k][3]) !== 'undefined') {
      for (const prop in tab[k][3]) {
        j3pElement('diva' + tab[k][0] + '_' + tab[k][1]).style[prop] = tab[k][3][prop]
      }
    }
  }
}

Tableur.prototype.ecritformule = function (ch) {
  j3pElement('zdtformule').value = ch
}

// (3,2) ==> B3
Tableur.prototype.formule = function (i, j) {
  const lettres = ['', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
  return lettres[j] + i
}

export default Tableur
