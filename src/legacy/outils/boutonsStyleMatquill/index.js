import { j3pElement, j3pGetNewId, j3pIsHtmlElement, j3pStyle } from 'src/legacy/core/functions'
import { addTable, getCells } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import './boutonsstyll.css'

/**
 * @typedef BoutonStyleMathquill~image
 * @type {Object}
 * @property {string} ch
 * @property {string} url
 */
/**
 * Description à écrire
 * @param {string|HTMLElement} conteneur
 * @param {string} nom  nom du/des bouton/s   deviendra id+nom
 *                                                      id+nom+0 .. 1 sur plusieurs boutons
 * @param {string|string[]} ch si string le contenu du bouton
 *                             si string[] le contenu des boutons
 * @param {function|function[]} onClick fonction appelée à chaque click (si function[] elles seront chacune lancée à chaque clic)
 * @param {Object} parametres
 * @param {string|boolean|boolean[]} [parametres.mathquill]
 *                                   si mathquill vaut true ou 'oui' , tous les choix sont considérés comme des formules mathquill
 *                                   si mathquill est un tableau , il doit avoir la même taille que ch
 *                                   c’est un tableau de boolean, les choix seront considérés comme des affichages mathquill
 *                                   si mathquill[x] est à true
 * @param {BoutonStyleMathquill~image[]} [parametres.image]
 *                                     les valeurs de ch correspondantes seront remplacées par les images d’url données
 * @param {string} [mes] À décrire
 * @constructor
 */
function BoutonStyleMathquill (conteneur, nom, ch, onClick, parametres, mes) {
  if (typeof this !== 'object') throw Error('Constructeur qui doit être appelé avec new')
  if (typeof conteneur === 'string') conteneur = j3pElement(conteneur)
  if (!j3pIsHtmlElement(conteneur)) throw Error('conteneur invalide')
  if (!conteneur.id) conteneur.id = j3pGetNewId('btnMq')
  this.conteneur = conteneur
  this.id = conteneur.id
  this.nom = nom
  this.modeArray = false

  if (Array.isArray(ch)) {
    this.ch = [...ch]
    this.modeArray = true
    if (Array.isArray(onClick)) {
      this.onClick = [...onClick]
      if (this.ch.length !== this.onClick.length) {
        console.error('dans BoutonStyleMathquill, "onClick" et "ch" n’ont pas le même nombre d’éléments')
      }
    } else {
      this.onClick = onClick
    }
  } else {
    this.ch = ch
    this.onClick = onClick
  }

  this.yaim = false
  this.image = []
  if (Array.isArray(parametres.image)) {
    this.yaim = true
    this.image = parametres.image.slice()
  }

  this.im = []

  if (this.modeArray) {
    if (this.yaim) {
      for (let i = 0; i < this.ch.length; i++) {
        for (let j = 0; j < this.image.length; j++) {
          if (this.ch[i] === this.image[j].ch) {
            this.ch[i] = '&nbsp;'
            this.im[i] = this.image[j].url
          }
        }
      }
    }
  } else {
    if (this.yaim) {
      this.ch = '&nbsp;&nbsp;&nbsp;&nbsp;'
      this.im = this.image[0].url
    }
  }

  if (!this.modeArray) {
    this.boutonC = addTable(this.conteneur, { nbLignes: 1, nbColonnes: 1, className: 'noMargin' })
    this.bouton = getCells(this.boutonC)
    this.bouton[0][0].classList.add('BSMathquill')
    const hjkl = j3pAffiche(this.bouton[0][0], null, this.ch)
    for (let j = 0; j < hjkl.mqList.length; j++) hjkl.mqList[j].style.cursor = 'pointer'
    this.bouton[0][0].addEventListener('click', this.onClick, false)
    if (this.yaim) {
      j3pStyle(this.bouton, { background: 'url("' + this.im + '") no-repeat 100% 30%' })
    }
  } else {
    this.bouton = []
    this.height = 0
    this.width = 0
    this.boutonC = addTable(this.conteneur, { nbLignes: 1, nbColonnes: this.ch.length * 2 + 1, className: 'noMargin' })
    this.bouton = getCells(this.boutonC)
    for (let i = 0; i < this.ch.length; i++) {
      this.bouton[0][i * 2].width = '10px'
      this.bouton[0][i * 2 + 1].classList.add('BSMathquill')
      if (this.ch[i].indexOf('frac') !== -1) this.bouton[0][i * 2 + 1].style.fontSize = '15px'
      const hfjk = j3pAffiche(this.bouton[0][i * 2 + 1], null, this.ch[i])
      for (let j = 0; j < hfjk.mqList.length; j++) hfjk.mqList[j].style.cursor = 'pointer'
      if (Array.isArray(this.onClick)) {
        this.bouton[0][i * 2 + 1].addEventListener('click', this.onClick[i], false)
      } else {
        const funci = function (event) {
          this.onClick(this.carac)
          event.stopPropagation()
        }
        this.bouton[0][i * 2 + 1].addEventListener('click', funci, false)
        this.bouton[0][i * 2 + 1].carac = mes[i]
      }
      if (this.im[i] !== undefined) {
        j3pStyle(this.bouton[0][i * 2 + 1], { background: 'url("' + this.im[i] + '") no-repeat 100% 30%' })
      }
    }
    this.bouton[0][0].width = '0px'
  }
}

export default BoutonStyleMathquill
