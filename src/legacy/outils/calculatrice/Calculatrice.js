import { j3pElement, j3pNombre, j3pShowError, j3pVirgule } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'

const tablotor = ['A', 'r', 'c', 'o', 's', 'i', 'n', 't', 'a', '√']

/**
 * Seul le constructeur Parcours utilise Calculatrice, pour en mettre une instance en global dans j3p.mepcalculatrice
 * En dec 2019 toutes les sections qui utilisent une calculatrice passent par cette variable globale
 * Il faudra que tous ceux qui utilisent cette instance globale passent par un outil ou importent ce constructeur
 * @param {Object} [options]
 * @param {number} [options.nbdecimales]
 * @constructor
 * @todo dans le constructeur, générer des HTMLElement plutôt que du code html en string, et mettre le conteneur de la calculatrice dans une propriété element, que les utilisateurs pourront insérer dans le dom où ils veulent
 * @todo virer toutes les fcts globales
 */
function Calculatrice ({ nbdecimales } = {}) {
  if (!this || typeof this !== 'object') throw Error('Constructeur qui doit être appelé avec new')
  this.nbdecimales = Number.isInteger(nbdecimales) && nbdecimales >= 0 ? nbdecimales : 5
  this.dernierresultat = 0
  this.etat = 'saisienonterminee'
  this.isShiftMode = false

  let MepHtmlCalc = '<form id="Mepcalculatrice" name="calculatriceJ3P" action="" onSubmit="j3p.mepcalculatrice.calcule();return false;">'
  MepHtmlCalc += '<table border="5px" class="tableau_general" cellpadding="0"   border-radius="10px 10px / 5px 5px" cellspacing="0" border-collapse: "none";>'
  MepHtmlCalc += '<tr>'
  MepHtmlCalc += '<td border="0px" bgcolor="#C2D6EB">'
  MepHtmlCalc += '<input id = "calctaffiche" type="text" inputmode="none" name="affiche" align="right" class="calculatriceaffiche" onClick="j3p.mepcalculatrice.onInputClick()" onKeyup="j3p.mepcalculatrice.onInputClick()" onKeypress="j3p.mepcalculatrice.onInputClick()"></td>'
  MepHtmlCalc += '</tr>'

  MepHtmlCalc += '<tr><td class="touches" border="0px">'

  MepHtmlCalc += '<table style="margin-bottom:0.5em" border="0" cellpadding="4" cellspacing="4">'
  MepHtmlCalc += '<tr>'
  MepHtmlCalc += '<td ><input class="toucheC" type="reset" value="C"  onClick="j3p.mepcalculatrice.reset()"></td>'
  MepHtmlCalc += '<td></td>'
  MepHtmlCalc += '<td></td>'
  MepHtmlCalc += '<td align = "center" ><INPUT id = "calcrad" align = "center" type= "radio" name="typeangle" value="Rad"  >Rad</td>'
  MepHtmlCalc += '<td align = "center" ><INPUT id = "calcdeg" align = "center" type= "radio" name="typeangle" value="Deg" checked >Deg</td>'
  MepHtmlCalc += '</tr>'

  MepHtmlCalc += '<tr>'
  MepHtmlCalc += '<td></td>'
  MepHtmlCalc += '<td></td>'
  MepHtmlCalc += '<td></td>'
  MepHtmlCalc += '<td></td>'
  MepHtmlCalc += '<td></td>'

  MepHtmlCalc += '</tr>'

  MepHtmlCalc += '<tr>'
  MepHtmlCalc += '<td><input class="touche" type="button"  value="del" onClick="j3p.mepcalculatrice.delCalc()"></td>'

  // A faire : gérer dans le code le calcul de puissances avec '^'
  MepHtmlCalc += '<td></td>'
  MepHtmlCalc += '<td><input class="touche" type="button"  value="(" onClick="j3p.mepcalculatrice.ajouter(\'(\')"></td>'
  MepHtmlCalc += '<td><input class="touche" type="button" value=")" onClick="j3p.mepcalculatrice.ajouter(\')\')"></td>'
  MepHtmlCalc += '<td></td>'

  MepHtmlCalc += '</tr>'

  MepHtmlCalc += '<tr >'
  MepHtmlCalc += '<td ><input class="touche" type="button"   value="^" onClick="j3p.mepcalculatrice.ajouter(\'^\')"></td>'
  MepHtmlCalc += '<td><input id="CalcCos" class="touche" type="button"   value="cos" onClick="j3p.mepcalculatrice.ajouter(\'cos(\')"></td>'
  MepHtmlCalc += '<td><input id="CalcSin" class="touche" type="button"   value="sin" onClick="j3p.mepcalculatrice.ajouter(\'sin(\')"></td>'
  // A faire : l’action onClick sur le bouton DEL
  MepHtmlCalc += '<td><input id="CalcTan" class="touche" type="button"   value="tan" onClick="j3p.mepcalculatrice.ajouter(\'tan(\')"></td>'
  MepHtmlCalc += '<td><input class="toucheOperation" type="button"   value="+" onClick="j3p.mepcalculatrice.ajouter(\'+\')"></td>'
  MepHtmlCalc += '</tr>'
  MepHtmlCalc += '<tr>'
  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="&radic;" onClick="j3p.mepcalculatrice.ajouter(\'√(\')"></td>'
  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="7" onClick="j3p.mepcalculatrice.ajouter(\'7\')"></td>'
  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="8" onClick="j3p.mepcalculatrice.ajouter(\'8\')"></td>'

  // MepHtmlCalc+='<td><input class="touche buttoncalculatrice" type="button" value="9" onClick="mepcalculatrice.ajouter(\'9\')"></td>'

  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="9" onClick="j3p.mepcalculatrice.ajouter(\'9\')"></td>'

  // lenomdelactivite.substring(0,lenomdelactivite.indexOf("["))

  MepHtmlCalc += '<td><input class="toucheOperation buttoncalculatrice" type="button" value="&minus;" onClick="j3p.mepcalculatrice.ajouter(\'-\')"></td>'
  MepHtmlCalc += '</tr>'

  MepHtmlCalc += '<tr>'
  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="&pi;" onClick="j3p.mepcalculatrice.ajouter(\'π\')"></td>'
  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="4" onClick="j3p.mepcalculatrice.ajouter(\'4\')"></td>'
  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="5" onClick="j3p.mepcalculatrice.ajouter(\'5\')"></td>'
  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="6" onClick="j3p.mepcalculatrice.ajouter(\'6\')"></td>'
  MepHtmlCalc += '<td><input class="toucheOperation buttoncalculatrice" type="button" value="×" onClick="j3p.mepcalculatrice.ajouter(\'*\')"></td>'
  MepHtmlCalc += '</tr>'
  MepHtmlCalc += '<tr>'
  MepHtmlCalc += '<td><input style="font-size:22px" class="touche buttoncalculatrice" type="button" value="&uArr;" onClick="j3p.mepcalculatrice.shift()"></td>'
  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="1" onClick="j3p.mepcalculatrice.ajouter(\'1\')"></td>'
  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="2" onClick="j3p.mepcalculatrice.ajouter(\'2\')"></td>'
  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="3" onClick="j3p.mepcalculatrice.ajouter(\'3\')"></td>'
  MepHtmlCalc += '<td><input class="toucheOperation buttoncalculatrice" type="button" value="÷" onClick="j3p.mepcalculatrice.ajouter(\'/\')"></td>'
  MepHtmlCalc += '</tr>'

  MepHtmlCalc += '<tr>'

  MepHtmlCalc += '<td></td>'
  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="0" onClick="j3p.mepcalculatrice.ajouter(\'0\')"></td>'
  MepHtmlCalc += '<td><input class="toucheVirgule buttoncalculatrice" type="button" value="." onClick="j3p.mepcalculatrice.ajouter(\'.\')"></td>'
  // A définir : l’action du onClick sur la touche ANS

  MepHtmlCalc += '<td><input class="touche buttoncalculatrice" type="button" value="ans" onClick="j3p.mepcalculatrice.ajouter(\'ans\')"></td>'
  MepHtmlCalc += '<td><input  class="toucheEgal buttoncalculatrice" type="button" value="=" onClick="j3p.mepcalculatrice.calcule()"></td>'
  MepHtmlCalc += '</tr>'

  MepHtmlCalc += '</table>'
  MepHtmlCalc += '</td>'
  MepHtmlCalc += '</tr>'
  MepHtmlCalc += '</table>'
  MepHtmlCalc += '</form>'

  /**
   * Le code html de la calculatrice
   * Mis dans le dom par j3pCreefenetres
   * @type {string}
   */
  this.html = MepHtmlCalc
}

// et on enferme le reste du code dans une iife
/**
 * Appelé par calcule
 * @param nb
 * @return {string|number}
 * @private
 */
function ecritureScient (nb) {
  // nb est un nombre qu’on va écrire en écriture scientifique dès lors qu’il sera supérieur à 10^puisPos et dont la valeur absolue sera inférieure à 10^puisNeg
  const puisPos = 13
  const puisNeg = -11
  let newNb = nb
  const posE = String(nb).indexOf('e')// on vérifie si ce nombre n’est pas déjà écrit en notation scientifique
  if (posE > -1) {
    // on se limite à 12 chiffres significatifs (au-delà, cela déborde)
    if (String(nb)[1] === '.' || String(nb)[2] === '.') {
      // nb est de la forme 2.3...e+12 ou 2.3...e-14 //attention au signe quand même
      let valeDec = (nb < 0) ? String(nb).substring(0, Math.min(11, posE + 2)) : String(nb).substring(0, Math.min(12, posE + 2))// c’est la valeur décimale comprise entre 0 et 10 (en valeur absolue)
      // Je dois l’arrondir pour éviter les zéro inutiles
      valeDec = j3pVirgule(j3pNombre(valeDec))
      newNb = valeDec + 'e' + String(nb).substring(posE + 1)
    } else {
      // pas sûr que cela se produise, mais sait-on jamais
      newNb = nb
    }
  } else {
    if (String(nb).length > puisPos) {
      const puis = Math.floor(Math.log(Math.abs(nb)) / Math.log(10)) + 1
      if (Math.abs(nb) > Math.pow(10, puisPos)) {
        newNb = (nb < 0) ? String(Number(String(nb / Math.pow(10, puis - 1)).substring(0, 12))) : String(Number(String(nb / Math.pow(10, puis - 1)).substring(0, 11)))
        newNb += 'e+' + (puis - 1)
      } else {
        newNb = Math.round(nb * Math.pow(10, puisPos - puis + 1)) / Math.pow(10, puisPos - puis + 1)
      }
    } else if (Math.abs(nb) < Math.pow(10, puisNeg)) {
      const nbAutreZero = /[1-9]/g
      const puis = String(Math.abs(nb)).split(nbAutreZero)[0].length - 1
      newNb = nb * Math.pow(10, puis)
      newNb = (nb < 0) ? String(newNb).substring(0, 12) : String(newNb).substring(0, 11)
      newNb += newNb + 'e-' + puis
    }
  }
  return newNb
}

/**
   * appelé par onKeypress pour le cas arrowLeft
   * @private
   * @return {void}
   */
function onArrowLeftPress () {
  const input = document.getElementById('calctaffiche')
  if (!input) return console.error(Error('L’élément #calctaffiche n’existe pas'))
  const ch = input.value
  let selStart = input.selectionStart - 1
  input.selectionEnd = input.selectionStart
  let caracavant = ch[selStart - 1]
  while (tablotor.includes(caracavant) && selStart !== 0) {
    selStart--
    caracavant = ch[selStart - 1]
  }
  input.selectionEnd = input.selectionStart = selStart
}

/**
   * Appelé par onKeypress pour le cas arrowRight
   * @private
   * @return {boolean}
   */
function onArrowRightPress () {
  const input = document.getElementById('calctaffiche')
  const ch = input.value
  let selStart = input.selectionStart + 1
  input.selectionEnd = input.selectionStart
  let caracavant = ch[selStart - 1]
  let ok = true
  while (tablotor.includes(caracavant) && selStart !== ch.length) {
    selStart++
    caracavant = ch[selStart - 1]
    ok = false
  }
  input.selectionEnd = input.selectionStart = selStart
  return ok
}

/**
 * Affiche text à l’écran
 * @param {string} text
 * @param [options]
 * @param {boolean} [options.withFocus]
 */
Calculatrice.prototype.affiche = function affiche (text, { withFocus = false } = {}) {
  const input = document.getElementById('calctaffiche')
  if (!input) return console.error(Error('L’élément #calctaffiche n’existe pas'))
  input.value = text
  if (withFocus) input.focus()
}
/**
 * Retourne la valeur affichée à l’écran
 * @return {string}
 */
Calculatrice.prototype.getValue = function getValue () {
  const input = document.getElementById('calctaffiche')
  if (!input) {
    console.error(Error('L’élément #calctaffiche n’existe pas'))
    return ''
  }
  return input.value
}

Calculatrice.prototype.ajouter = function ajouter (caracteres) {
  let nbInputClick = 0
  while (!this.onInputClick() && nbInputClick < 100) nbInputClick++
  if (nbInputClick === 100) return j3pShowError(Error('L’ajout de contenu dans la calculatrice a échoué'))
  const input = document.getElementById('calctaffiche')
  if (!input) return console.error(Error('L’élément #calctaffiche n’existe pas'))
  const ch = input.value
  const selStart = input.selectionStart
  input.selectionEnd = selStart
  const avant = ch.substring(0, selStart)
  let apres = ch.substring(selStart, ch.length)

  if (caracteres === 'ans') {
    input.value = avant + this.dernierresultat + apres
    const index = (avant + this.dernierresultat).length
    input.selectionStart = index
    input.selectionEnd = index
    input.focus()
    this.etat = 'saisienonterminee'
    return
  }

  if (caracteres === 'Delete') {
    const ch = input.value
    let selFin = input.selectionStart + 1
    // apparemment ce code efface plusieurs caractères d’un coup, mais c’est pas très clair…
    let caracavant = ch[selFin - 1]
    while (tablotor.includes(caracavant) && selFin !== ch.length) {
      selFin = selFin + 1
      caracavant = ch[selFin - 1]
    }
    input.selectionEnd = selFin
    apres = ch.substring(selFin, ch.length)
    // on met à jour l’input
    input.value = avant + apres
    // et le curseur
    input.selectionEnd = input.selectionStart = avant.length
    // puis on remet le focus sur #calctaffiche
    input.focus()
    return
  }

  if (caracteres === 'Backspace') {
    const ch = input.value
    let selStart = input.selectionStart - 1
    const tablotor = ['A', 'r', 'c', 'o', 's', 'i', 'n', 't', 'a', '√']
    let caracavant = ch[selStart - 1]
    while (tablotor.includes(caracavant) && selStart !== 0) {
      selStart--
      caracavant = ch[selStart - 1]
    }
    const avant = ch.substring(0, selStart)

    input.value = avant + apres
    input.selectionEnd = input.selectionStart = avant.length
    input.focus()
    return
  }

  if (this.etat === 'saisieterminee') {
    if (caracteres.length === 1 && /[+\-/*]/.test(caracteres)) {
      input.value = avant + caracteres + apres
      input.selectionEnd = input.selectionStart = (avant + caracteres).length
    } else {
      input.value = caracteres
      input.selectionEnd = input.selectionStart = caracteres.length
    }
    this.etat = 'saisienonterminee'
  } else {
    input.value = avant + caracteres + apres
    input.selectionEnd = input.selectionStart = (avant + caracteres).length
  }
  input.focus()
}

Calculatrice.prototype.calcule = function calcule () {
  const input = document.getElementById('calctaffiche')
  if (!input) return console.error(Error('L’élément #calctaffiche n’existe pas'))
  try {
    let textCalc = input.value
    const tabModif = [['√', 'Arccos', 'Arcsin', 'Arctan'], ['racine', 'acos', 'asin', 'atan']]
    for (let postabModif = 0; postabModif < tabModif[0].length; postabModif++) {
      while (textCalc.indexOf(tabModif[0][postabModif]) > -1) {
        textCalc = textCalc.replace(tabModif[0][postabModif], tabModif[1][postabModif])
      }
    }
    // On transforme les virgules (dues à un copier-coller) par des points
    textCalc = textCalc.replace(/,/g, '.')
    const unarbre = new Tarbre(textCalc, [])
    let resultat = unarbre.evalue([])
    if (Number.isNaN(resultat)) {
      input.value = 'écriture incorrecte'
      return
    }

    // on a un résultat
    const puisPrecision = (Math.abs(resultat) < Math.pow(10, -12)) ? 13 : 13 - Math.floor(Math.log(Math.abs(resultat)) / Math.log(10)) + 1; const // E(log(|resultat|)) nous donne le nombre de chiffres de la partie enyière de la valeur absolue de resultat
      nbd = Math.min(10, puisPrecision)
    resultat = (Math.round((Math.pow(10, nbd) * resultat)) / Math.pow(10, nbd))
    this.dernierresultat = resultat
    resultat = ecritureScient(resultat)
    input.value = resultat
    this.etat = 'saisieterminee'
  } catch (e) {
    console.error(e)
    input.value = 'écriture incorrecte'
  }
}

/**
   * Efface la zone de saisie de la calculatrice
   */
Calculatrice.prototype.reset = function reset () {
  const input = document.getElementById('calctaffiche')
  if (!input) return console.error(Error('L’élément #calctaffiche n’existe pas'))
  input.value = ''
  input.focus()
}

Calculatrice.prototype.delCalc = function delCalc () {
  const input = document.getElementById('calctaffiche')
  if (!input) return console.error(Error('L’élément #calctaffiche n’existe pas'))
  const listeFct = ['Arccos(', 'Arcsin(', 'Arctan(', 'cos(', 'sin(', 'tan(', '√(', 'π']
  const listeOpe = ['*', '/', '+', '-', ')', '(', '^']
  const listeChiffres = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.']
  const listeDel = listeFct.concat(listeOpe).concat(listeChiffres)
  let value = input.value
  if (value !== '') {
    for (let i = 0; i < listeDel.length; i++) {
      // je parcours toute la liste des éléments de listeDel.
      // Dès que je vois l’élément à la fin de value, alors je l’enlève
      if (value.substring(value.length - listeDel[i].length) === listeDel[i]) {
        value = value.substring(0, value.length - listeDel[i].length)
        break
      }
    }
    input.value = value
  }
  input.focus()
}

Calculatrice.prototype.shift = function shift () {
  this.isShiftMode = !this.isShiftMode
  for (const fn of ['Cos', 'Sin', 'Tan']) {
    const elt = j3pElement('Calc' + fn)
    const min = fn.toLowerCase()
    elt.value = min + (this.isShiftMode ? ' -1' : '')
    const str = (this.isShiftMode ? 'Arc' : '') + min + '('
    elt.onclick = this.ajouter.bind(this, str)
    elt.style.fontSize = this.isShiftMode ? '10px' : '16px'
  }
}

// ////////////////////////////////////////
// @todo passer ces listeners en privé
// quand on passera par des éléments du dom et du addEventListener dans le constructeur
// ////////////////////////////////////////

Calculatrice.prototype.onInputClick = function onInputClick () {
  const input = document.getElementById('calctaffiche')
  if (!input) return console.error(Error('L’élément #calctaffiche n’existe pas'))
  const ch = input.value
  let selStart = input.selectionStart
  input.selectionEnd = input.selectionStart
  const tablotor = ['A', 'r', 'c', 'o', 's', 'i', 'n', 't', 'a', '√']

  let caracavant = ch[selStart - 1]
  let ok = true

  while (tablotor.includes(caracavant) && selStart !== 0) {
    selStart--
    caracavant = ch[selStart - 1]
    ok = false
  }

  input.selectionEnd = input.selectionStart = selStart
  return ok
}

/**
 * Mis en listener click de #Mepcalculatrice et #dialogCalculatrice par j3pCreefenetres
 */
Calculatrice.prototype.fixFocusListener = function fixFocusListener () {
  const dialogCalculatrice = j3pElement('dialogCalculatrice')
  if (dialogCalculatrice) dialogCalculatrice.focus()
}

/**
   * Mis en listener keypress de #Mepcalculatrice et #dialogCalculatrice par j3pCreefenetres
   * ex fct j3pGereCalc
   * @param event
   */
Calculatrice.prototype.onKeypress = function onKeypress (event) {
  const input = document.getElementById('calctaffiche')
  if (!input) return console.error(Error('L’élément #calctaffiche n’existe pas'))
  const key = event.key
  const isValid = 'π1234567890+-./*()^'.indexOf(key) !== -1
  const needChange = '×÷,pPaAcC'.indexOf(key) !== -1
  if (document.activeElement.id === 'calctaffiche') {
    if (isValid || needChange) {
      // key est un char unique connu
      switch (key) {
        // on passe en revue les needChange
        case ',':
          this.ajouter('.')
          break
        case '×':
          this.ajouter('*')
          break
        case '÷':
          this.ajouter('/')
          break
        case 'p':
        case 'P':
          this.ajouter('π')
          break
        case 'a':
        case 'A':
          // ajouter traite ce cas particulier
          this.ajouter('ans')
          break
        case 'c':
        case 'C':
          input.value = ''
          break

          // le reste est laissé intact
        default:
          this.ajouter(key)
      }
    } else {
      // on gère ces 5 là qui on plus d’un caractère
      if (key === 'Delete' || key === 'Backspace') this.ajouter(key)
      if (key === 'Enter') this.calcule()
      if (key === 'ArrowRight') onArrowRightPress()
      if (key === 'ArrowLeft') onArrowLeftPress()
      // et on ignore tout le reste
    }
  } else {
    // #calctaffiche n’est pas document.activeElement
    // pourquoi on gère ça différement ?
    if (isValid || needChange) {
      // FIXME ça semble un bug d’ajouter aussi les caractères connus mais qui devraient être modifiés
      // cette ligne devrait plutôt être dans le default du switch qui suit, mais on laisse comme c'était
      input.value = input.value + key

      switch (key) {
        case ',':
          // faudrait pas faire ça ?
          // input.value += '.'
          break
        case '×':
          // faudrait pas faire ça ?
          // input.value += '*'
          break
        case '÷':
          // faudrait pas faire ça ?
          // input.value += '/'
          break
        case 'p':
        case 'P':
          input.value += 'π'
          break
        case 'a':
        case 'A':
          input.value += 'ans'
          break
        case 'c':
        case 'C':
          input.value = ''
          break
      }
    } else {
      if (key === 'Backspace') {
        if (input.value.length > 0) {
          input.value = input.value.substring(0, input.value.length - 1)
        }
      }
    }
    // et on donne le focus…
    input.focus()
  }
  event.stopPropagation()
  event.preventDefault()
} // onKeypress

export default Calculatrice
