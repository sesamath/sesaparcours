import { j3pAjouteArea, j3pArrondi, j3pElement, j3pRemplace } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
/* défini Blocnotes et Tarbre

Blocnotes est utilisé par
sections/college/troisieme/arithmetique/sectionalgorithmedeuclideassiste.js
sections/college/troisieme/arithmetique/sectionalgorithmedeuclidelibre.js
sections/college/troisieme/sectionalgorithmedeuclideassiste.js
sections/college/troisieme/sectionalgorithmedeuclidelibre.js
sections/lycee/complexes/section602quot.js
sections/lycee/suites/sectionSuites1.js
sections/primaire/cahiercm2/sectioncm2exN1_33.js
sections/sectioncalculenphrase.js
=> aucune en prod

Et pour les autres fcts avec des noms assez générique,
c’est plus prudent de ne pas les mettre dans l’espace de nom global

=> mettre le code utile dans des outils ad hoc le jour où ça servira
*/

// j3p.blocnotes = new Blocnotes({idconteneur:this.zones.MG,precision:6,area:{cols:30,rows:6,taillepolice:14,police:"arial",couleurpolice:"#000"}});
// j3p.blocnotes.init();

function Blocnotes (objet) {
  if (typeof this !== 'object') throw Error('Constructeur qui doit être appelé avec new')
  // objet = {idconteneur,area:{cols,rows,taillepolice,police,couleurpolice}
  this.objet = {}
  this.precision = objet.precision

  this.idconteneur = objet.idconteneur

  j3pAjouteArea(objet.idconteneur, {
    id: 'area' + objet.idconteneur,
    cols: objet.area.cols,
    rows: objet.area.rows,
    readonly: false,
    taillepolice: objet.area.taillepolice,
    police: objet.area.police,
    couleurpolice: objet.area.couleurpolice
  })

  return this
}

Blocnotes.prototype.init = function () {
  j3pElement('area' + this.idconteneur).addEventListener('keypress', this.touche, false)
}

Blocnotes.prototype.touche = function (evt) {
  switch (evt.keyCode) {
    case 13:
      this.evalue()
      break
    case 40:
      this.evalue()
      break
    case 38:
      this.evalue()
      break
  }
}
Blocnotes.prototype.evalue = function () {
  let ch = j3pElement('area' + this.idconteneur).value
  const tab = ch.split('\n')
  const tabres = []
  let res
  for (let k = 0; k < tab.length; k++) {
    if (tab[k].indexOf('=') != -1) {
      const ch1 = tab[k].substring(0, tab[k].indexOf('='))
      if (ch1.indexOf('ans') == -1) {
        const unarbre = new Tarbre(ch1, [])
        res = unarbre.evalue([])
        tabres[k] = res
      } else {
        let ch2 = ch1
        while (ch2.indexOf('ans') != -1) {
          ch2 = j3pRemplace(ch2, ch2.indexOf('ans'), ch2.indexOf('ans') + 2, tabres[k - 1])
        }
        const unarbre = new Tarbre(ch2, [])
        res = unarbre.evalue([])
        tabres[k] = res
      }

      if (ch1.charAt(tab[k].indexOf('=') - 1) == ' ') {
        tab[k] = ch1 + '= ' + j3pArrondi(res, this.precision)
      } else {
        tab[k] = ch1 + ' = ' + j3pArrondi(res, this.precision)
      }
    }
  }
  ch = tab.join('\n')
  j3pElement('area' + this.idconteneur).value = ch
}

export default Blocnotes
