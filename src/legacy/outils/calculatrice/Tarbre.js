/** @module legacy/outils/calculatrice/Tarbre */

/* défini et Tarbre

Cette version est une rev 11336 à peine nettoyée (juste le code mort viré et les eqeqeq sans risque rectifié)
Il y a davantage de nettoyage en rev11415 mais un truc ne fonctionne plus
window.ta = new Tarbre('(9)/3', [])
ta.evalue() // devrait renvoyer 3 mais renvoie 0 en rev11415

outils/j3pLoad.js charge d’office ce fichier pour l’outil calculatrice

Ça fonctionne avec sesaparcours
*/

/*
EXEMPLE d’APPEL

arbres[1] = new Tarbre("-2x+3cos(ABC)+2x-0.04+ABC/2",["ABC","x"])
resultat = arbres[1].evalue([4,3])
*/

// une iife pour isoler le reste et ne pas polluer l’espace de nom global
// avec des variables utiles ici seulement
// (et être sûr que qqun d’autre ne les affecte pas avec autre chose)
// des fonctions à usage internes (qui n’utilisent pas les variables internes de Tarbre)

/**
   * Retourne true si ensemble contient c
   * @param {*} c
   * @param {Array} ensemble
   * @return {boolean}
   */
function estdans (c, ensemble) {
  return ensemble.indexOf(c) !== -1
}
function estentierpair (x) {
  return Math.abs(x - Math.round(x)) < 0.0000001 && Math.round(x) % 2 === 0
}

function estentierimpair (x) {
  return Math.abs(x - Math.round(x)) < 0.0000001 && Math.round(x) % 2 === 1
}
function estsigne (s) {
  return s === '-' || s === '+'
}
function inserezero (s) {
  return '0' + s
}
function inserezeroplus (s) {
  return '0+' + s
}
// des constantes utilisées par Tarbre
const fonc = ['^', '/', '*', '-', '+', 'cos', 'sin', 'tan', 'racine', 'carre', 'ln', 'exp', 'abs', 'acos', 'asin', 'atan', 'ent', 'ceil', 'iden', 'oppo']
const ope = ['^', '/', '*', '-', '+', '(']
const ensemble1 = [')', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '&pi;']
const ensemble2 = ['£', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', 'P', 'x', 'y', 'z', 'u', 'v', 'π']
const ensemble3 = ['(']
// inutilisé
// var ensemble4 = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '&pi;']
// var ensemble5 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '(', ')', '&pi;']
// var ensemble6 = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9","&pi;"]
// var ensemble7 = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "&pi;"]
// var ensemble8 = ["'", '"', "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "&pi;"]
// var ensemble9 = [",",".", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

/**
   * @todo contrôler les types passés (cast éventuel) avant d’affecter les propriétés
   * @private
   * @param numero
   * @param fonc
   * @param a
   * @param b
   * @param inca
   * @param incb
   * @param bg
   * @param bd
   * @constructor
   */
function Tnoeud (numero, fonc, a, b, inca, incb, bg, bd) {
  this.a = a
  this.b = b
  this.fonc = fonc
  this.numero = numero
  this.bd = bd
  this.bg = bg
  this.inca = inca
  this.incb = incb
}

/**
 * À documenter
 * @param pexpression
 * @param pinco
 * @constructor
 */
function Tarbre (pexpression, pinco) {
  if (typeof this !== 'object') throw Error('Constructeur qui doit être appelé avec new')
  let x1 = ''
  let x2 = ''
  // Valeurs renvoyées par existe
  let pos1 = 0
  let pos2 = 0
  // Valeurs renvoyées par existe

  let nombre = 1
  const noeuds = []
  // valeur renvoyée par extraitnombre
  let numerofonction = 0
  // variable renvoyée par existefonction()
  let i1; let i2 = 0
  // valeurs renvoyées par parenthese
  let numpointeur = 0

  /**
     * Retourne true si s contient le caractère c et affecte pos1 et pos2 dans ce cas
     * @private
     * @param {string} c
     * @param {string} s
     * @return {boolean}
     */
  function existe (c, s) {
    if (typeof s === 'string' && typeof c === 'string') {
      let j
      for (let i = 0; i <= s.length - 1; i++) {
        if (s.charAt(i) === c) {
          // ce qui suit semble ne servir à rien, mais ça affecte les var globales pos1 et pos2, utilisés dans un while de construirebranche
          j = 1
          // si i = 0 la première condition ne peut pas être vraie => j=1
          while (i >= j && estdans(s.charAt(i - j), ensemble2)) j++
          // si tous les chars matchent, j vaut i+1 en sortie, donc pos1 vaudra 0
          pos1 = i - j + 1
          x1 = s.substring(pos1, i) // tous les caractères de ensemble2 contigüs qui précèdent c
          j = 1
          while (i + j < s.length && estdans(s.charAt(i + j), ensemble2)) j++
          pos2 = i + j - 1 // index du dernier caractère contigü qui matche et suit c
          x2 = s.substring(i + 1, i + j) // tous les caractères de ensemble2 contigüs qui suivent c
          return true
        }
      }
    } else {
      console.error(Error('paramètres invalides'))
    }
    return false
  }

  // affecte numerofonction
  function existefunction (s, pos) {
    //   §12§  où s[pos]=§
    if (s.charAt(pos) !== '§') {
      return false
    } else {
      let k = 1
      while (s.charAt(pos - k) !== '§') {
        k = k + 1
      }
      numerofonction = s.substring(pos - k + 1, pos) - 0
      return true
    }
  }

  // affecte numerofonction via un appel de existefunction
  function parenthese (s) {
    let max = 0
    let maxcour = 0
    let ini = 0
    for (var i = 0; i <= s.length - 1; i++) {
      if (s.charAt(i) === '(') {
        maxcour = maxcour + 1
        if (maxcour > max) {
          ini = i
        }
      }
      if (s.charAt(i) === ')') {
        if (maxcour > max) {
          max = maxcour
        }
        maxcour = maxcour - 1
      }
    }
    i1 = ini
    i = i1 + 1
    if (max != 0) {
      while (s.charAt(i) !== ')') {
        i = i + 1
      }
      i2 = i
    }
    if (existefunction(s, i1 - 1)) {
      if (numerofonction < 10) {
        i1 = i1 - 3
      } else {
        i1 = i1 - 4
      }
    }
    return max
  }

  /// ////////////////////////////////////////////////////////////////////////////////////
  /// ////////////////////////////////////////////////////////////////////////////////////
  function miseenforme (s) {
    if (estsigne(s.charAt(0))) {
      s = inserezero(s)
    } else {
      s = inserezeroplus(s)
    }
    let i, v
    // on remplace les fonctions par §numero_fonction§
    for (i = fonc.length - 6; i >= 0; i--) {
      while (s.indexOf(fonc[i + 5]) !== -1) {
        v = i + 5
        s = remplace(s, s.indexOf(fonc[v]), s.indexOf(fonc[v]) + fonc[v].length - 1, '§' + v + '§')
      }
    }
    //  on s’occupe des inconnues
    //  stockées dans arbres[i].inco = pinco (paramètre envoyé)
    //  Par exemple : ["AB","x"]
    let memoire
    for (i = 0; i <= pinco.length - 1; i++) {
      // Recherche de inco[i] et remplacement par £i£
      while (s.indexOf(pinco[i]) !== -1) {
        memoire = s.indexOf(pinco[i])
        s = remplace(s, memoire, memoire + pinco[i].length - 1, '£' + i + '£')
        // si devant une inconnue y a pas un opération on ajoute un *
        if (!estdans(s.charAt(memoire - 1), ope)) {
          s = remplace(s, memoire - 1, memoire - 1, s.charAt(memoire - 1) + '*')
        }
      }
    }
    // end for i
    i = 1
    while (i <= s.length - 1) {
      if (estdans(s.charAt(i), ensemble3)) {
        // "("
        if (existefunction(s, i - 1)) {
          // cos(
          let longueurfonction = 0
          if (numerofonction > 9) {
            longueurfonction = 2
          } else {
            longueurfonction = 1
          }
          if (estdans(s.charAt(i - longueurfonction - 3), ensemble1)) {
            // xcos(
            // trace("avant  s = "+s)
            s = remplace(s, i - longueurfonction - 3, i - longueurfonction - 3, s.charAt(-1 + i - longueurfonction) + '*')
            // trace("apres  s = "+s)
            // x*cos(
            i = i + 1
          }
          // if existefunction(s,i-1) {
        } else {
          if (estdans(s.charAt(i - 1), ensemble1)) {
            s = remplace(s, i - 1, i - 1, s.charAt(i - 1) + '*')
            i = i + 1
          }
        }
      }
      i = i + 1
    }
    // while (i <= length(s) -1) {
    // trace(s)
    return s
  }

  /// ////////////////////////////////////////////////////////////////////////////////////
  /// ////////////////////////////////////////////////////////////////////////////////////
  function estpointeur (s) {
    if (s.charAt(0) !== 'P') {
      return false
    }
    numpointeur = s.substring(1)
    return true
  }

  /// ////////////////////////////////////////////////////////////////////////////////////
  /// ////////////////////////////////////////////////////////////////////////////////////
  function constructionbranche (s) {
    let u = 0
    while (s.charAt(u) !== '(') {
      u = u + 1
    }
    u = u + 1
    let s1, v1, num1, v2, num2, incatt, incbtt, att, btt
    if ((s.charAt(u) === '0') && (s.charAt(u + 1) === '+')) {
      s1 = s.substring(u, s.length - 1)
    } else if (estsigne(s.charAt(u))) {
      s1 = '0' + s.substring(u, s.length - 1)
    } else {
      s1 = '0+' + s.substring(u, s.length - 1)
    }
    for (let i = 0; i <= 4; i++) {
      while (existe(fonc[i], s1)) {
        v1 = estpointeur(x1)
        num1 = numpointeur
        if (x1 === 'π') x1 = String(Math.PI)
        v2 = estpointeur(x2)
        if (x2 === 'π') x2 = String(Math.PI)
        num2 = numpointeur
        incatt = 0
        incbtt = 0
        att = 0
        btt = 0
        if (!v1 && !v2) {
          // si x1 n’est pas une inconnue
          if (x1.indexOf('£') === -1) {
            att = x1
          } else {
            incatt = x1.substring(1, x1.length - 1) - 0 + 1
          }
          if (x2.indexOf('£') === -1) {
            btt = x2
          } else {
            incbtt = x2.substring(1, x2.length - 1) - 0 + 1
          }
          noeuds[nombre] = new Tnoeud(nombre, i, att - 0, btt - 0, incatt - 0, incbtt - 0, 0, 0)
        } else if (!v1 && v2) {
          if (x1.indexOf('£') === -1) {
            att = x1
          } else {
            incatt = x1.substring(1, x1.length - 1) - 0 + 1
          }
          noeuds[nombre] = new Tnoeud(nombre, i, att - 0, btt - 0, incatt - 0, incbtt - 0, 0, num2)
        } else if (v1 && !v2) {
          if (x2.indexOf('£') === -1) {
            btt = x2
          } else {
            incbtt = x2.substring(1, x2.length - 1) - 0 + 1
          }
          noeuds[nombre] = new Tnoeud(nombre, i, att - 0, btt - 0, incatt - 0, incbtt - 0, num1, 0)
        } else {
          // v1 && v2
          noeuds[nombre] = new Tnoeud(nombre, i, att - 0, btt - 0, incatt - 0, incbtt - 0, num1, num2)
        }
        s1 = remplace(s1, pos1, pos2, 'P' + nombre)
        nombre++
      }
    }
    if (u > 2) {
      existefunction(s, u - 2)
      noeuds[nombre] = new Tnoeud(nombre, numerofonction, -1, 0, 0, 0, nombre - 1, 0)
      nombre++
    }
  }

  /// ////////////////////////////////////////////////////////////////////////////////////
  /// ////////////////////////////////////////////////////////////////////////////////////
  function constructionarbre (s) {
    let ch = ''
    while (parenthese(s) >= 1) {
      ch = s.substring(i1, i2 + 1)
      constructionbranche(ch)
      nombre = nombre - 1
      s = remplace(s, i1, i2, 'P' + nombre)
      nombre = nombre + 1
    }
    s = '(' + s + ')'

    constructionbranche(s)
    // res = calcul(noeuds[nombre-1])
    // return res
  }

  this.inco = pinco
  this.expression = pexpression.replace(/\*-/g, '*(-1)*')
  this.expression = miseenforme(this.expression)

  //  return
  constructionarbre(this.expression)

  // calcule noeuds et nombre
  this.nombre = nombre
  this.noeuds = []
  this.noeuds = noeuds
  this.valeursinco = []
  // définition de la propriété
  this.resultat = 0
  // définition de la propriété
}

/// ////////////////////////////////////////////////////////////////////////////////////

// remplace("abcdef",2,4,"richard") --> abrichardf
function remplace (s, pos1, pos2, ch) {
  let ret = ''
  if (pos1 > 0) {
    if (pos2 < s.length - 1) {
      ret = s.substring(0, pos1) + ch + s.substring(pos2 + 1)
    } else {
      ret = s.substring(0, pos1) + ch
    }
  } else if (pos1 == 0) {
    if (pos2 < s.length - 1) {
      ret = ch + s.substring(pos2 + 1)
    } else {
      ret = '' + ch
    }
  } else {
    ret = '' + ch
  }
  return ret
}

/**
 * Calcule l’arbre
 * @param {} valeurinco
 * @return {number}
 */
Tarbre.prototype.evalue = function (valeurinco) {
  /**
   * @param noeud
   * @return {Tnoeud}
   */
  function calcul (noeud) {
    if (noeud.bg !== 0 && noeud.bd === 0 && noeud.a === -1) {
      const elt = document.getElementById('calcdeg')
      const modif = (elt && elt.checked) ? Math.PI / 180 : 1

      switch (noeud.fonc) {
        case 5 :
          return Math.cos(modif * calcul(noeuds[noeud.bg]))
        case 6 :
          return Math.sin(modif * calcul(noeuds[noeud.bg]))
        case 7 :
          return Math.tan(modif * calcul(noeuds[noeud.bg]))
        case 8 :
          return Math.sqrt(calcul(noeuds[noeud.bg]))
        case 9 :
          return calcul(noeuds[noeud.bg]) * calcul(noeuds[noeud.bg])
        case 10 :
          return Math.log(calcul(noeuds[noeud.bg]))
        case 11 :
          return Math.exp(calcul(noeuds[noeud.bg]))
        case 12 :
          return Math.abs(calcul(noeuds[noeud.bg]))
        case 13 :
          return Math.acos(calcul(noeuds[noeud.bg])) / modif
        case 14 :
          return Math.asin(calcul(noeuds[noeud.bg])) / modif
        case 15 :
          return Math.atan(calcul(noeuds[noeud.bg])) / modif
        case 16 :
          return Math.floor(calcul(noeuds[noeud.bg]))
        case 17 :
          return Math.ceil(calcul(noeuds[noeud.bg]))
        case 18 :
          return Math.PI
      }
    }
    let aa, bb
    if ((noeud.bg !== 0) && (noeud.bd !== 0)) {
      switch (noeud.fonc) {
        case 0 :
          // return Math.exp(calcul(noeuds[toto.bd])*Math.log(calcul(noeuds[toto.bg])))
          // aa^^bb = exp(bb*ln(aa))
          aa = calcul(noeuds[noeud.bg])
          bb = calcul(noeuds[noeud.bd])
          if (aa === 0) return 0
          if (aa < 0) {
            if (estentierpair(bb)) return Math.exp(bb * Math.log(-aa))
            if (estentierimpair(bb)) return -Math.exp(bb * Math.log(-aa))
            return Math.exp(bb * Math.log(-aa))
          }
          // si on est toujours là c’est > 0
          return Math.exp(bb * Math.log(aa))

        case 1 :
          return calcul(noeuds[noeud.bg]) / calcul(noeuds[noeud.bd])
        case 2 :
          return calcul(noeuds[noeud.bg]) * calcul(noeuds[noeud.bd])
        case 3 :
          return calcul(noeuds[noeud.bg]) - calcul(noeuds[noeud.bd])
        case 4 :
          return calcul(noeuds[noeud.bg]) + calcul(noeuds[noeud.bd])
      }
    }
    if ((noeud.bg != 0) && (noeud.bd == 0)) {
      if (noeud.incb == 0) {
        switch (noeud.fonc) {
          case 0 :
            aa = calcul(noeuds[noeud.bg])
            bb = noeud.b

            if (aa == 0) { return 0 }

            if (aa < 0) {
              if (estentierpair(bb)) return Math.exp(bb * Math.log(-aa))
              if (estentierimpair(bb)) return -Math.exp(bb * Math.log(-aa))
              return Math.exp(bb * Math.log(-aa))
            }
            return Math.exp(bb * Math.log(aa))

            // return Math.exp(noeud.b*Math.log(calcul(noeuds[noeud.bg])))
          case 1 :
            return calcul(noeuds[noeud.bg]) / noeud.b
          case 2 :
            return calcul(noeuds[noeud.bg]) * noeud.b
          case 3 :
            return calcul(noeuds[noeud.bg]) - noeud.b
          case 4 :
            return calcul(noeuds[noeud.bg]) + noeud.b
        }
      } else {
        switch (noeud.fonc) {
          case 0 :
            aa = calcul(noeuds[noeud.bg])
            bb = valeurinco[noeud.incb - 1]

            if (aa == 0) { return 0 }

            if (aa < 0) {
              if (estentierpair(bb)) return Math.exp(bb * Math.log(-aa))
              if (estentierimpair(bb)) return -Math.exp(bb * Math.log(-aa))
              return Math.exp(bb * Math.log(-aa))
            }
            return Math.exp(bb * Math.log(aa))
          case 1 :
            return calcul(noeuds[noeud.bg]) / valeurinco[noeud.incb - 1]
          case 2 :
            return calcul(noeuds[noeud.bg]) * valeurinco[noeud.incb - 1]
          case 3 :
            return calcul(noeuds[noeud.bg]) - valeurinco[noeud.incb - 1]
          case 4 :
            return calcul(noeuds[noeud.bg]) + valeurinco[noeud.incb - 1]
        }
      }
    }
    if ((noeud.bg == 0) && (noeud.bd != 0)) {
      if (noeud.inca == 0) {
        switch (noeud.fonc) {
          case 0 :
            aa = noeud.a
            bb = calcul(noeuds[noeud.bd])
            if (aa == 0) { return 0 }
            if (aa < 0) {
              if (estentierpair(bb)) return Math.exp(bb * Math.log(-aa))
              if (estentierimpair(bb)) return -Math.exp(bb * Math.log(-aa))
              return Math.exp(bb * Math.log(-aa))
            }
            return Math.exp(bb * Math.log(aa))
          case 1 :
            return noeud.a / calcul(noeuds[noeud.bd])
          case 2 :
            return noeud.a * calcul(noeuds[noeud.bd])
          case 3 :
            return noeud.a - calcul(noeuds[noeud.bd])
          case 4 :
            return noeud.a + calcul(noeuds[noeud.bd])
        }
      } else {
        switch (noeud.fonc) {
          case 0 :
            aa = valeurinco[noeud.inca - 1]
            bb = calcul(noeuds[noeud.bd])
            if (aa == 0) return 0
            if (aa < 0) {
              if (estentierpair(bb)) return Math.exp(bb * Math.log(-aa))
              if (estentierimpair(bb)) return -Math.exp(bb * Math.log(-aa))
              return Math.exp(bb * Math.log(-aa))
            }
            return Math.exp(bb * Math.log(aa))
          case 1 :
            return valeurinco[noeud.inca - 1] / calcul(noeuds[noeud.bd])
          case 2 :
            return valeurinco[noeud.inca - 1] * calcul(noeuds[noeud.bd])
          case 3 :
            return valeurinco[noeud.inca - 1] - calcul(noeuds[noeud.bd])
          case 4 :
            return valeurinco[noeud.inca - 1] + calcul(noeuds[noeud.bd])
        }
      }
    }
    if ((noeud.bg == 0) && (noeud.bd == 0)) {
      if ((noeud.inca == 0) && (noeud.incb == 0)) {
        switch (noeud.fonc) {
          case 0 :
            aa = noeud.a
            bb = noeud.b
            if (aa == 0) return 0
            if (aa < 0) {
              if (estentierpair(bb)) return Math.exp(bb * Math.log(-aa))
              if (estentierimpair(bb)) return -Math.exp(bb * Math.log(-aa))
              return Math.exp(bb * Math.log(-aa))
            }
            return Math.exp(bb * Math.log(aa))
          case 1 :
            return noeud.a / noeud.b
          case 2 :
            return noeud.a * noeud.b
          case 3 :
            return noeud.a - noeud.b
          case 4 :
            return noeud.a + noeud.b
        }
      }
      if ((noeud.inca == 0) && (noeud.incb != 0)) {
        switch (noeud.fonc) {
          case 0 :
            aa = noeud.a
            bb = valeurinco[noeud.incb - 1]
            if (aa == 0) return 0
            if (aa < 0) {
              if (estentierpair(bb)) return Math.exp(bb * Math.log(-aa))
              if (estentierimpair(bb)) return -Math.exp(bb * Math.log(-aa))
              return Math.exp(bb * Math.log(-aa))
            }
            return Math.exp(bb * Math.log(aa))
          case 1 :
            return noeud.a / valeurinco[noeud.incb - 1]
          case 2 :
            return noeud.a * valeurinco[noeud.incb - 1]
          case 3 :
            return noeud.a - valeurinco[noeud.incb - 1]
          case 4 :
            return noeud.a + valeurinco[noeud.incb - 1]
        }
      }
      if ((noeud.inca != 0) && (noeud.incb == 0)) {
        switch (noeud.fonc) {
          case 0 :
            aa = valeurinco[noeud.inca - 1]
            bb = noeud.b
            if (aa === 0) return 0
            if (aa < 0) {
              if (estentierpair(bb)) return Math.exp(bb * Math.log(-aa))
              if (estentierimpair(bb)) return -Math.exp(bb * Math.log(-aa))
              return Math.exp(bb * Math.log(-aa))
            }
            return Math.exp(bb * Math.log(aa))
            // return Math.exp(noeud.b*Math.log(valeurinco[noeud.inca-1]))
          case 1 :
            return valeurinco[noeud.inca - 1] / noeud.b
          case 2 :
            return valeurinco[noeud.inca - 1] * noeud.b
          case 3 :
            return valeurinco[noeud.inca - 1] - noeud.b
          case 4 :
            return valeurinco[noeud.inca - 1] + noeud.b
        }
      }
      if ((noeud.inca != 0) && (noeud.incb != 0)) {
        switch (noeud.fonc) {
          case 0 :
            aa = valeurinco[noeud.inca - 1]
            bb = valeurinco[noeud.incb - 1]
            if (aa == 0) { return 0 }
            if (aa < 0) {
              if (estentierpair(bb)) return Math.exp(bb * Math.log(-aa))
              if (estentierimpair(bb)) return -Math.exp(bb * Math.log(-aa))
              return Math.exp(bb * Math.log(-aa))
            }
            return Math.exp(bb * Math.log(aa))
          case 1 :
            return valeurinco[noeud.inca - 1] / valeurinco[noeud.incb - 1]
          case 2 :
            return valeurinco[noeud.inca - 1] * valeurinco[noeud.incb - 1]
          case 3 :
            return valeurinco[noeud.inca - 1] - valeurinco[noeud.incb - 1]
          case 4 :
            return valeurinco[noeud.inca - 1] + valeurinco[noeud.incb - 1]
        }
      }
    }
  }

  this.valeursinco = valeurinco
  var noeuds = []
  noeuds = this.noeuds
  const nombre = this.nombre
  try {
    this.resultat = calcul(noeuds[nombre - 1])
  } catch (error) {
    console.error(error)
    return Number.NaN
  }

  if (Math.abs(this.resultat) >= 0 && Math.abs(this.resultat) < 10) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.00000000000001) {
      this.resultat = Math.round(this.resultat)
    }
  }

  if (Math.abs(this.resultat) >= 10 && Math.abs(this.resultat) < 100) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.0000000000001) {
      this.resultat = Math.round(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 100 && Math.abs(this.resultat) < 1000) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.000000000001) {
      this.resultat = Math.round(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 1000 && Math.abs(this.resultat) < 10000) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.00000000001) {
      this.resultat = Math.round(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 10000 && Math.abs(this.resultat) < 100000) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.0000000001) {
      this.resultat = Math.round(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 100000 && Math.abs(this.resultat) < 1000000) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.000000001) {
      this.resultat = Math.round(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 1000000 && Math.abs(this.resultat) < 10000000) {
    if (Math.abs(this.resultat - Math.floor(this.resultat)) < 0.00000001) {
      this.resultat = Math.floor(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 10000000 && Math.abs(this.resultat) < 100000000) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.0000001) {
      this.resultat = Math.round(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 100000000 && Math.abs(this.resultat) < 1000000000) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.000001) {
      this.resultat = Math.round(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 1000000000 && Math.abs(this.resultat) < 10000000000) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.00001) {
      this.resultat = Math.round(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 10000000000 && Math.abs(this.resultat) < 100000000000) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.0001) {
      this.resultat = Math.round(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 100000000000 && Math.abs(this.resultat) < 1000000000000) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.001) {
      this.resultat = Math.round(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 1000000000000 && Math.abs(this.resultat) < 10000000000000) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.01) {
      this.resultat = Math.round(this.resultat)
    }
  }
  if (Math.abs(this.resultat) >= 10000000000000 && Math.abs(this.resultat) < 100000000000000) {
    if (Math.abs(this.resultat - Math.round(this.resultat)) < 0.1) {
      this.resultat = Math.round(this.resultat)
    }
  }
  //  if (this.resultat==Infinity) {this.resultat=1234567.1234567}
  return this.resultat
}

export default Tarbre
