/** @module legacy/outils/geometrie/functions */

/**
 * Retourne l’angle entre deux droites
 * @param eq1
 * @param eq2
 * @return {number|*}
 */
export function j3pAngledroites (eq1, eq2) {
  if ((eq1[1] === 0) && (eq2[1] === 0)) return 0
  let res, cd, cd2
  if ((eq1[1] === 0)) {
    cd = -eq2[0] / eq2[1]
    res = Math.abs(((Math.atan(cd) - Math.PI) / Math.PI) * 180)
  } else if ((eq2[1] === 0)) {
    cd = -eq1[0] / eq1[1]
    res = Math.abs(((Math.atan(cd) - Math.PI) / Math.PI) * 180)
  } else {
    cd = -eq1[0] / eq1[1]
    cd2 = -eq2[0] / eq2[1]
    res = Math.abs(((Math.atan(cd) - Math.atan(cd2)) / Math.PI) * 180)
  }
  return (res > 90) ? 180 - res : res
} // j3pAngledroites

/**
 * Retourne la distance d’un point à une droite
 * @param x
 * @param y
 * @param eq
 * @return {number}
 */

export function j3pDistancepointdroite (x, y, eq) {
  return (Math.abs(eq[0] * x + eq[1] * y - eq[2]) / Math.sqrt(eq[0] * eq[0] + eq[1] * eq[1]))
}

/**
 * Retourne l’equation (ax + by + c = 0) de la droite passant par les deux points
 * ([xp1, yp1] et [xp2, yp2]) sous la forme d’un tableau de 3 elts [a, b, c]
 * (y = ax + b) => […, …, …]
 * @param xp1
 * @param yp1
 * @param xp2
 * @param yp2
 * @return {number[]}
 */
export function j3pEquation (xp1, yp1, xp2, yp2) {
  return [yp1 - yp2, xp2 - xp1, (yp2 - yp1) * xp1 - (xp2 - xp1) * yp1]
}
