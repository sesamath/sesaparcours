import { j3pAddElt, j3pDetruit } from 'src/legacy/core/functions'
import { addDefaultTable, addTable, getCells } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'

import './zoneclick.css'
import { barreZone } from 'src/legacy/outils/zoneStyleMathquill/functions'

function Zoneclick (iddivconteneur, montab, parametres) {
  const objet = this
  this.biencool = '#A4D8C0'
  this.fauxcool = '#E6BA7F'
  this.reponse = []
  this.reponsenb = []
  this.sel = []
  this.image = parametres.image === true
  const funcnul = () => { }
  this.afaire = parametres.afaire || funcnul

  this.recupid = function (uni) {
    return this.jegarde[uni][3]
  }
  this.cleanClass = function (ki, b) {
    ki.classList.remove('zoneClikSelect')
    ki.classList.remove('zoneClikBien')
    ki.classList.remove('zoneClikFaux')
  }

  this.recupidaide = function (uni) {
    return this.jegarde[uni][1]
  }

  this.Select = function (uni) {
    if (this.unique) {
      this.reponse = []
      this.reponsenb = []
      for (let az = 0; az < this.montab.length; az++) {
        this.cleanClass(objet.jegarde[az][3])
        this.sel[az] = false
      }
    }
    this.sel[uni] = true
    this.cleanClass(objet.jegarde[uni][3])
    objet.jegarde[uni][3].classList.add('zoneClikSelect')
    this.reponse.push(this.montab[uni])
    this.reponsenb.push(uni)
    objet.jegarde[uni][3].removeEventListener('click', this.mafonce, false)
    objet.jegarde[uni][3].addEventListener('click', this.mafonce2, false)
    if (objet.afaire) objet.afaire(this.montab[uni])
  }

  this.deSelect = function (uni) {
    this.cleanClass(objet.jegarde[uni][3])
    // objet.jegarde[uni][3].classList.add('zoneClik')
    this.reponse.splice(this.reponse.indexOf(this.montab[uni]), 1)
    this.reponsenb.splice(this.reponse.indexOf(uni), 1)
    objet.jegarde[uni][3].removeEventListener('click', this.mafonce2, false)
    objet.jegarde[uni][3].addEventListener('click', this.mafonce, false)
    this.sel[uni] = false
  }

  this.disable = function () {
    for (let i = 0; i < this.montab.length; i++) {
      this.jegarde[i][3].classList.remove('zoneClikActive')
      this.jegarde[i][3].removeEventListener('click', this.mafonce2, false)
      this.jegarde[i][3].removeEventListener('click', this.mafonce, false)
    }
  }

  this.detruit = function () {
    this.disable()
    j3pDetruit(this.spanContenant)
  }

  this.corrige = function (quoi, barre) {
    // si quoi === true
    //  ca met les select en vert

    // si quoi === false
    // ca met les select en false

    // si quoi un tab
    // ca test c’est bon , faux ou oublié

    // si barre === true
    // ca barre les fo
    // ca montre les co
    // ca disable

    const arenv = []
    if (quoi === true) {
      for (let i = 0; i < this.montab.length; i++) {
        if (this.sel[i]) {
          this.cleanClass(objet.jegarde[i][3])
          this.jegarde[i][3].classList.add('zoneClikBien')
        }
      }
    }
    if (quoi === false) {
      for (let i = 0; i < this.montab.length; i++) {
        if (this.sel[i]) {
          this.cleanClass(objet.jegarde[i][3])
          this.jegarde[i][3].classList.add('zoneClikFaux')
        }
      }
    }
    if (quoi !== false && quoi !== true) {
      if (!Array.isArray(quoi)) quoi = [quoi]
      for (let i = 0; i < this.montab.length; i++) {
        this.cleanClass(objet.jegarde[i][3])
        if (this.sel[i]) {
          if (quoi.indexOf(i) !== -1) {
            this.jegarde[i][3].classList.add('zoneClikBien')
          } else {
            this.jegarde[i][3].classList.add('zoneClikFaux')
            if (barre === true) {
              arenv.push(barreZone(this.jegarde[i][3]))
            }
          }
        } else {
          if (quoi.indexOf(i) !== -1) {
            this.jegarde[i][3].classList.add('zoneClikCo')
          }
        }
      }
    }
    if (barre === true) this.disable()
    return arenv
  }

  this.corrigelesfaux = function (quoi) {
    for (let i = 0; i < this.montab.length; i++) {
      if (this.sel[i]) {
        this.cleanClass(objet.jegarde[i][3])
        if (quoi.indexOf(i) !== -1) {
          objet.jegarde[i][3].classList.add('zoneClikBE')
        } else {
          objet.jegarde[i][3].classList.add('zoneClikFE')
        }
      }
    }
  }

  this.montab = []
  for (let i = 0; i < montab.length; i++) {
    this.montab[i] = montab[i]
  }

  for (let i = 0; i < this.montab.length; i++) {
    this.sel[i] = false
  }

  /// Decla des parametres
  //
  /// mathquill  un tableau[true,false,true,...]  true si choix mathquill
  /// si 'oui' ou true , tout est mathquill sauf premier choix si different
  /// si 'tout' tout est mathquill

  this.disabled = false

  /// si on veur declencher une fonction sur un changement
  // mettre etiq pour recuperer le num de l’étiquette
  //       contenu pour recupere le contenu
  //       place pour recup le num de la place
  this.afaire = parametres.afaire

  this.affiche = parametres.affiche ?? true

  this.maxwidthcol = parametres.maxwidthcol ?? 0
  this.tomblok = parametres.tomblok ?? false

  /// pour des trucs centré
  const uncentre = parametres.centre
  this.centre = ((uncentre === true) || (uncentre === 'true') || (uncentre === 'oui'))

  /// pour accepter ou non plusieurs réponses
  const unique = parametres.reponsesmultiples
  this.unique = ((unique === false) || (unique === 'false') || (unique === 'non'))
  const nbcol = parametres.colonne
  const nbligne = parametres.ligne
  let ya = false
  if (Number.isInteger(nbcol)) {
    this.col = nbcol
    ya = true
  } else {
    this.col = 0
  }
  if (Number.isInteger(nbligne)) {
    this.lig = nbligne
    ya = true
  } else {
    this.lig = 0
  }
  // dispo des etiquettes
  // carre par défaut
  this.dispo = 'carre'
  const dispo = parametres.dispo
  if (['Fixe', 'Fixé', 'fixe', 'fixé'].includes(dispo)) {
    if (ya) this.dispo = 'fixe'
  } else if (['ligne', 'lignes', 'line', 'lines'].includes(dispo)) {
    this.dispo = 'lignes'
  } else if (['colonnes', 'colonne', 'row', 'rows'].includes(dispo)) {
    this.dispo = 'colonnes'
  }

  this.couleurselect = parametres.couleurselect || '#A9E2F3'
  this.couleurdeselect = parametres.couleurdeselect || '#848484'

  this.width = parametres.width ?? 0
  this.height = parametres.height ?? 0

  /// /fin des parametres
  this.mafonce = function () { objet.Select(this.num) }
  this.mafonce2 = function () { objet.deSelect(this.num) }
  this.iddivconteneur = iddivconteneur
  const yu = addTable(iddivconteneur, { nbLignes: 1, nbColonnes: 1, className: 'forcewrap' })
  this.spanContenant = getCells(yu)[0][0]

  this.numpla = 0
  this.nb = this.montab.length
  this.etiqplace = []
  for (let i = 0; i < this.montab.length; i++) {
    this.etiqplace[i] = false
  }
  this.etiqlign = []
  this.etiqcol = []

  objet.finiTout = function () {
    this.listadisa = []
    objet.nb = objet.montab.length

    switch (objet.dispo) {
      case 'lignes' : {
        const tt = addDefaultTable(this.spanContenant, objet.nb, 1)
        objet.tabEt = []
        for (let k = 0; k < objet.nb; k++) {
          objet.tabEt.push(tt[k][0])
        }
      }
        break
      case 'colonnes' : {
        const tt = addDefaultTable(this.spanContenant, 1, objet.nb)
        objet.tabEt = []
        for (let k = 0; k < objet.nb; k++) {
          objet.tabEt.push(tt[0][k])
        }
      }
        break
      case 'carre' :
        objet.tabEt = []
        this.spanContenant.setAttribute('class', 'cards')
        for (let k = 0; k < objet.nb; k++) {
          const tt = j3pAddElt(this.spanContenant, 'div')
          tt.setAttribute('class', 'cards2')
          const jkl = addTable(tt, { nbLignes: 1, nbColonnes: 1, className: 'tabZOneClic' })
          const gh = getCells(jkl)
          objet.tabEt.push(gh[0][0])
        }
        break
      case 'fixe' : {
        if (objet.col !== 0) {
          objet.colonne = objet.col
          if (objet.maxwidthcol === 0) { objet.ligne = Math.ceil(objet.nb / objet.col) } else { objet.ligne = Math.ceil(objet.nb2 / objet.col) }
        } else {
          objet.ligne = objet.lig
          if (objet.maxwidthcol === 0) { objet.colonne = Math.ceil(objet.nb / objet.lig) } else { objet.colonne = Math.ceil(objet.nb2 / objet.lig) }
        }
        const tt = addDefaultTable(this.spanContenant, objet.ligne, objet.colonne)
        objet.tabEt = []
        for (let k = 0; k < objet.ligne; k++) {
          for (let u = 0; u < objet.colonne; u++) {
            objet.tabEt.push(tt[k][u])
          }
        }
      }
    }

    objet.jegarde = []
    for (let i = 0; i < montab.length; i++) {
      const jkl = addTable(objet.tabEt[i], { nbLignes: 2, nbColonnes: 3, className: 'tabZOneClic' })
      const gh = getCells(jkl)
      objet.jegarde.push(gh[0])
      objet.jegarde[i][2].style.width = '20px'
      if (this.tomblok) objet.jegarde[i][2].style.width = '10px'

      objet.jegarde[i].push(j3pAddElt(objet.jegarde[i][0], 'div'))
      objet.jegarde[i][3].addEventListener('click', objet.mafonce, false)
      objet.jegarde[i][3].classList.add('zoneClikBase')
      objet.jegarde[i][3].classList.add('zoneClikActive')
      objet.jegarde[i][3].num = i
      objet.jegarde[i][3].lecteur = gh[0][0]
      if (!this.image) {
        const tt = j3pAffiche(objet.jegarde[i][3], null, objet.montab[i])
        for (let j = 0; j < tt.mqList.length; j++) {
          this.listadisa.push(tt.mqList[j])
          tt.mqList[j].style.cursor = 'pointer'
        }
      }
    }
  }

  objet.finiTout()
}

// je combine les deux plus tard promis

export default Zoneclick
