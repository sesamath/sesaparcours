/**
 * Objet manipulé par les ≠ fonctions de ce module, qui sert de singleton pour toutes les zsm de la page courante
 * @type {ZoneStyleMathquillBase[]}
 */
let listeTabulations = []

/**
 * Cherche elt dans la liste, si on le trouve et qu’il a une propriété prevTab et que cet id est dans notre liste on le focus, sinon on focus elt, sinon le premier de la liste
 * @param {ZoneStyleMathquillBase} elt
 * @returns {undefined}
 */
export function prevTabFocus (elt) {
  if (listeTabulations.length === 0) return
  const exists = listeTabulations.includes(elt)
  if (exists) {
    if (elt.prevTab !== undefined) {
      // on a trouvé l’élément et il a une propriété prevTab (un id), on le cherche dans la liste
      const prevTab = listeTabulations.find(e => e.id === elt.prevTab)
      if (prevTab) return prevTab.focus()
      else return
    } else {
      let i = listeTabulations.indexOf(elt) + 1
      i = (i >= listeTabulations.length) ? 0 : i
      return listeTabulations[i].focus()
    }
  }
  // sinon le premier de la liste
  listeTabulations[0].focus()
}

/**
 * lance blur() sur toutes les zones (sauf celle passée en argument)
 * @param {ZoneStyleMathquillBase} elt
 */
export function blurAllTabs (elt) {
  for (const tab of listeTabulations) {
    if (tab !== elt) tab.blur()
  }
}

/**
 * Ajoute un élément à la liste
 * @param {ZoneStyleMathquillBase} elt
 */
export function addTab (elt) {
  listeTabulations.push(elt)
}
/**
 * Retire elt de la liste (s’il y était)
 * @param {HTMLElement} elt
 */
export function removeTab (elt) {
  if (!elt) return // on nous demande de retirer rien, on s’arrête pas à si peu…
  listeTabulations = listeTabulations.filter(tab => tab !== elt)
}
