import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pElement, j3pEmpty } from 'src/legacy/core/functions'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'

import './zoneStyleAffiche.css'

/**
 * Retourne l’élément html (parmi el ou ses enfants) dont le innerText vaut contenuVoulu
 * @private
 * @param {HTMLElement} elt
 * @param {string} contenuVoulu
 * @return {HTMLElement|undefined} undefined si on a pas trouvé contenuVoulu
 */
function retrouve (elt, contenuVoulu) {
  if (elt.innerText === contenuVoulu) return elt
  if (!elt.childNodes || !elt.childNodes.length) return
  for (const child of elt.childNodes) {
    const tt = retrouve(child, contenuVoulu)
    if (tt) return tt
  }
}

/**
 * à documenter
 * @param {HTMLElement|string} conteneur
 * @param {string} contenu
 * @param {Array<ListeDeroulante|ZoneStyleMathquillBase>} zonesOuListes
 * @constructor
 */
function ZoneStyleAffiche (conteneur, contenu, zonesOuListes) {
  if (typeof conteneur === 'string') conteneur = j3pElement(conteneur)
  // on ajoute la classe .zoneStyleAffiche au conteneur, pour que nos modifs css ne s’appliquent que sur nos éléments
  // (et pas tous les éléments mathquill à venir, mis par d’autres sections sans rapport avec zoneStyleAffiche)
  conteneur.classList.add('zoneStyleAffiche')
  /**
   * Le retour de j3pAffiche, à usage interne seulement
   * @private
   * @type {ResultatAffichage}
   */
  this._resultatAffichage = j3pAffiche(conteneur, '', contenu)
  /**
   * La liste de zonesOuListes ou ZoneStyleMathquill3
   * @type {Array<ListeDeroulante|ZoneStyleMathquill3>}
   */
  this.liste = []
  if (Array.isArray(zonesOuListes)) {
    for (const list of zonesOuListes) {
      // les autres types ne font rien
      if (['ld', 'zsm3'].includes(list.type)) {
        const klm = retrouve(this._resultatAffichage?.mqList[0], list.symb)
        j3pEmpty(klm)
        const zElt = list.type === 'ld'
          ? ListeDeroulante.create(klm, list.liste, list.params)
          : new ZoneStyleMathquill3(klm, list.params)
        this.liste.push(zElt)
      }
    }
  }
}

/**
 * Retourne true si tous les items ont une réponse (prop changed true pour le type ld et réponse non vide pour les zsmX)
 * @return {boolean}
 */
ZoneStyleAffiche.prototype.hasResponse = function hasResponse () {
  return this.liste.every(item => {
    const isOk = (item.type === 'ld') ? item.changed : item.reponse()
    if (isOk) return true
    item.focus()
    return false
  })
}

/**
 * Appelle la méthode disable() sur chaque item de la liste
 */
ZoneStyleAffiche.prototype.disable = function disable () {
  for (const item of this.liste) {
    if (typeof item.disable === 'function') item.disable()
    else console.error(Error('item invalide (pas de méthode disable'), item)
  }
}

/**
 * Utiliser barreIfKo
 * @alias barreIfKo
 * @deprecated
 */
ZoneStyleAffiche.prototype.barreLesFaux = function barreLesFaux () {
  return this.barreIfKo()
}

/**
 * Appelle barreIfKo() sur chaque items de la liste
 * @deprecated
 */
ZoneStyleAffiche.prototype.barreIfKo = function barreIfKo () {
  for (const item of this.liste) item.barreIfKo()
}

/**
 * Appelle la méthode corrige() sur chaque item de la liste
 */
ZoneStyleAffiche.prototype.corrige = function corrige () {
  for (const item of this.liste) item.corrige(true)
}

export default ZoneStyleAffiche
