import $ from 'jquery'

import { j3pAddElt, j3pDetruit, j3pFreezeElt, j3pGetNewId, j3pStyle } from 'src/legacy/core/functions'
import ZoneStyleMathquillBase from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquillBase'
import { addDefaultTable, addTable, getCells } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'

import { barreZone } from './functions'
import { blurAllTabs, removeTab, prevTabFocus } from './listeTabulations'

// et notre css
import './zoneStyleMathquill.scss'

// pour tester zsm2, par ex
// http://localhost:8081/?rid=sesabibli/5ef86cfe110713292761e4df ou https://j3p.sesamath.net/?rid=sesabibli/5ef86cfe110713292761e4df
// avec hasAutoKeyboard et enter
// http://localhost:8081/?rid=sesabibli/5ee4857e1d70ea257deb7f55 vs https://j3p.sesamath.net/?rid=sesabibli/5ee4857e1d70ea257deb7f55

function is2NaN (ch) {
  if (typeof ch !== 'string') return true
  const reste = ch.replace(/[0-9,]/g, '')
  return reste.length !== 0
}

class ZoneStyleMathquill2 extends ZoneStyleMathquillBase {
  /**
   * À décrire
   * @param {string|HTMLElement} conteneur
   * @param {Object} parametres
   * @param {string} [parametres.id] Éventuel id qui sera mis sur le div créé (div mis dans la propriété texta)
   * @param {string} [parametres.restric]
   * @param {string} [parametres.clavier]
   * @param {number} parametres.limite
   * @param {number} [parametres.limitenb=0]
   * @param {function} parametres.onchange
   * @constructor
   */
  constructor (conteneur, parametres) {
    // on doit toujours commencer par appeler le constructeur de la classe parente
    super(conteneur, { ...parametres, version: 2 })
    // ce qui suit est spécifique à zsm2
    this.sansBord = Boolean(parametres.sansBord)
    this.isCapsLocked = false
    this.expo = false
    this.blok = false
    this._fautMaj = false
    this.limitenb = parametres.limitenb || 0
    this.onchange = parametres.onchange
    this.lastkey = ['', '']
    this.obligeclav = parametres.clavierVisible
    if (this.obligeclav === undefined) { this.obligeclav = false }
    this._isClavierVisible = false
    this.hasAutoKeyboard = parametres.hasAutoKeyboard
    // crea de la zone
    /**
       * Le div de la zone
       * @type {HTMLElement}
       */
    this.texta = j3pAddElt(this.conteneur, 'div')
    if (parametres.id) {
      this.texta.id = parametres.id
    }
    // 0 pour simple  1 pour frac
    this.texta.type = 0
    this.texta.poscurseur = 0
    // 0 si dans num , 1 si dans den (pour pos2
    this.texta.poscurseur2 = 0
    this.texta.elemnum = []
    this.texta.elemden = []
    // j3pElement(le_nom+"inputmq1").style.backgroundColor = "white";
    this.texta.style.tabIndex = 0
    // this.texta.setAttribute("class", 'zsmMq');
    this.isblur = true
    this._clavierListener = this.clavier.bind(this)
    this._clickZoneListener = this.clickzone.bind(this)
    this._gereBlurListener = this.gereblur.bind(this)
    // on fait pareil pour les autres, pour un code homogène
    this._clickcListener = this.clickc.bind(this)
    this._onClickClavier = this.clickKlav.bind(this)
    this._metCurseurListener = this.metcurseur.bind(this)
    this._placeListener = this.place.bind(this, false)
    this._vireListener = this.vire.bind(this)
    this.constuctorFinalize(parametres.contenu || '')
  }

  barre () {
    this.avirrr = barreZone(this.conteneur)
    return this.avirrr
  }

  blur () {
    if (this.disabled) return
    if (this._isLocked) return
    this.isblur = true
    this.expo = false
    this.majaffiche('')
    const elt = this.textaCont
    if (!elt) return
    elt.className = 'zsmMq'
    if (!this.obligeclav) {
      const elt = this.textaIm
      if (elt) elt.style.display = 'none'
      this.divClavier.style.visibility = 'hidden'
      this._isClavierVisible = false
    }
    if (this.hasAutoKeyboard) {
      this.divClavierAuto.style.visibility = 'hidden'
    } else {
      // me rappelle plus pourquoi ca ?
      const elt = this.unTAbc[0][this.texta.poscurseur * 2]
      if (elt) elt.className = ''
    }
  }

  buildKeyboard () {
    const touches = this.clavierR.replace('p', 'π')
    this.buildKeyboardHelper(touches)
  }

  cacheKlavier () {
    const node = this.divClavier
    node.style.visibility = 'hidden'
    node.style.height = '0'
  }

  changetype () {
    this.expo = false
    this.texta.type = 1 - this.texta.type
    this.texta.poscurseur2 = this.texta.type
    if (this.texta.poscurseur2 === 0) {
      this.texta.poscurseur = Math.min(this.texta.poscurseur, this.texta.elemnum.length)
    } else { this.texta.poscurseur = Math.min(this.texta.poscurseur, this.texta.elemden.length) }
    this.majaffiche('')
    this.place(true)
  }

  clavier (event) {
    if (this.isblur || this.disabled) return
    if (event.key === 'Tab' || event.code === 'Tab') {
      blur()
      event.preventDefault()
      event.stopPropagation()
      prevTabFocus(this)
      return
    }
    event.preventDefault()
    event.stopPropagation()

    if ((event.key === 'Enter') || (event.code === 'Enter') || (event.code === 'NumpadEnter')) {
      this.expo = false
      if (this.enter) this.enter()
      return
    }

    let muo = event.key
    if (muo.includes('^') && muo !== '^') muo = muo.replace('^', '')
    if (muo.includes('¨') && muo !== '¨') muo = muo.replace('¨', '')
    if (muo === 'Digit0') { muo = '0' }
    if (muo === 'Dead') { muo = '^' }
    if (muo === '.') { muo = ',' }
    if (muo === 'π') muo = 'p'
    if (muo === '×') muo = '*'
    const monres = this.restric
    if ((monres.includes(muo))) {
      if (muo === '*') { muo = '\\times' }
      if (muo === 'p') { muo = 'π' }
      if (muo === '²') { muo = '^{2}' }
      if (muo === ' ') { muo = ' ' }
      this.majaffiche(muo)
      return
    }

    if (muo === 'Shift') return
    this.expo = false
    if (event.code === 'Delete') {
      if (this.texta.poscurseur2 === 0) {
        if (this.texta.elemnum.length >= this.texta.poscurseur) {
          if ((!is2NaN(this.texta.elemnum[this.texta.poscurseur])) && (this.texta.elemnum[this.texta.poscurseur].length > 1)) {
            this.texta.elemnum[this.texta.poscurseur] = this.texta.elemnum[this.texta.poscurseur].substring(1, (this.texta.elemnum[this.texta.poscurseur].length))
          } else {
            this.texta.elemnum.splice(this.texta.poscurseur, 1)
          }
          this.majaffiche('')
        }
      } else {
        if (this.texta.elemden.length >= this.texta.poscurseur) {
          if ((!is2NaN(this.texta.elemden[this.texta.poscurseur])) && (this.texta.elemden[this.texta.poscurseur].length > 1)) {
            this.texta.elemden[this.texta.poscurseur] = this.texta.elemden[this.texta.poscurseur].substring(1, (this.texta.elemden[this.texta.poscurseur].length))
          } else {
            this.texta.elemden.splice(this.texta.poscurseur, 1)
          }
          this.majaffiche('')
        }
      }
      return
    }
    if (event.code === 'ArrowRight') {
      if (this.texta.poscurseur2 === 0) {
        if (this.texta.poscurseur < this.texta.elemnum.length) {
          if ((!is2NaN(this.texta.elemnum[this.texta.poscurseur])) && (this.texta.elemnum[this.texta.poscurseur].length > 1)) {
            const u = this.texta.elemnum[this.texta.poscurseur]
            this.texta.elemnum[this.texta.poscurseur] = u.substring(0, 1)
            this.texta.elemnum.splice(this.texta.poscurseur + 1, 0, u.substring(1, u.length))
          }
          this.texta.poscurseur++
        }
        this.majaffiche('')
      } else {
        if (this.texta.poscurseur < this.texta.elemden.length) {
          if ((!is2NaN(this.texta.elemden[this.texta.poscurseur])) && (this.texta.elemden[this.texta.poscurseur].length > 1)) {
            const u = this.texta.elemden[this.texta.poscurseur]
            this.texta.elemden[this.texta.poscurseur] = u.substring(0, 1)
            this.texta.elemden.splice(this.texta.poscurseur + 1, 0, u.substring(1, u.length))
          }
          this.texta.poscurseur++
        }
        this.majaffiche('')
      }
    }
    if (event.code === 'ArrowLeft') {
      if (this.texta.poscurseur > 0) {
        if (this.texta.poscurseur2 === 0) {
          if ((!is2NaN(this.texta.elemnum[this.texta.poscurseur - 1])) && (this.texta.elemnum[this.texta.poscurseur - 1].length > 1)) {
            const u = this.texta.elemnum[this.texta.poscurseur - 1]
            this.texta.elemnum[this.texta.poscurseur - 1] = u.substring(0, u.length - 1)
            this.texta.elemnum.splice(this.texta.poscurseur, 0, u.substring(u.length - 1, u.length))
            this.texta.poscurseur++
          }
        } else {
          if ((!is2NaN(this.texta.elemden[this.texta.poscurseur - 1])) && (this.texta.elemden[this.texta.poscurseur - 1].length > 1)) {
            const u = this.texta.elemden[this.texta.poscurseur - 1]
            this.texta.elemden[this.texta.poscurseur - 1] = u.substring(0, u.length - 1)
            this.texta.elemden.splice(this.texta.poscurseur, 0, u.substring(u.length - 1, u.length))
            this.texta.poscurseur++
          }
        }

        this.texta.poscurseur--
      }
      this.majaffiche('')
    }
    if (event.code === 'ArrowUp') {
      if (this.texta.poscurseur2 === 1) {
        this.texta.poscurseur2--
        this.texta.poscurseur = Math.min(this.texta.poscurseur, this.texta.elemnum.length)
      }
      this.majaffiche('')
    }
    if (event.code === 'ArrowDown') {
      if ((this.texta.poscurseur2 === 0) && (this.texta.type === 1)) {
        this.texta.poscurseur2++
        this.texta.poscurseur = Math.min(this.texta.poscurseur, this.texta.elemden.length)
      }
      this.majaffiche('')
    }
    if (event.code === 'Backspace') {
      if (this.texta.poscurseur > 0) {
        if (this.texta.poscurseur2 === 0) {
          if ((!is2NaN(this.texta.elemnum[this.texta.poscurseur - 1])) && (this.texta.elemnum[this.texta.poscurseur - 1].length > 1)) {
            this.texta.elemnum[this.texta.poscurseur - 1] = this.texta.elemnum[this.texta.poscurseur - 1].substring(0, (this.texta.elemnum[this.texta.poscurseur - 1].length - 1))
          } else {
            this.texta.elemnum.splice((this.texta.poscurseur - 1), 1)
            this.texta.poscurseur--
          }
        } else {
          if ((!is2NaN(this.texta.elemden[this.texta.poscurseur - 1])) && (this.texta.elemden[this.texta.poscurseur - 1].length > 1)) {
            this.texta.elemden[this.texta.poscurseur - 1] = this.texta.elemden[this.texta.poscurseur - 1].substring(0, (this.texta.elemden[this.texta.poscurseur - 1].length - 1))
          } else {
            this.texta.elemden.splice((this.texta.poscurseur - 1), 1)
            this.texta.poscurseur--
          }
        }
      }
      this.majaffiche('')
    }
  }

  clickc (event) {
    if (this.disabled) { return }
    const popos = event.currentTarget.getBoundingClientRect()
    if (event.clientX > popos.x + popos.width / 2) {
      this.texta.poscurseur = event.currentTarget.num + 1
    } else {
      this.texta.poscurseur = event.currentTarget.num
    }
    this.texta.poscurseur2 = event.currentTarget.num2
    this.isblur = false
    this.majaffiche('')
  }

  clickKlav (event) {
    this._isLocked = true
    const add = event.currentTarget.mes
    if (add !== '^') {
      setTimeout(() => {
        this._isLocked = false
        this.majaffiche('')
      }, 20)
    } else {
      setTimeout(() => {
        this._isLocked = false
        this.majaffiche('£')
      }, 20)
    }
    const e = { key: add, code: add, stopPropagation: function () {}, preventDefault: function () {} }
    this.clavier(e)
  }

  clickzone () {
    if (this.disabled) return
    this.isblur = false
    this.textaCont.setAttribute('class', 'zsmMqFocused')
    this.majaffiche('')
  }

  disable () {
    if (this.disabled) { return }
    this.isblur = true
    this.blok = true
    removeTab(this)
    this.majaffiche('')
    document.removeEventListener('click', this._gereBlurListener, false)
    this.disabled = true
    this.textaCont.setAttribute('class', '')
    for (let i = 0; i < this.texta.elemnum.length; i++) {
      j3pFreezeElt(this.unTAbc[0][i * 2 + 1])
    }
    for (let i = 0; i < this.texta.elemden.length; i++) {
      j3pFreezeElt(this.unTAbc2[0][i * 2 + 1])
    }
    this.textaCont.removeEventListener('click', this.clickzone, false)

    j3pDetruit(this.textaIm)
    this.cacheKlavier()
    j3pDetruit(this.divClavier)
    if (this.divClavierAuto !== undefined) j3pDetruit(this.divClavierAuto)

    if (this.avirrr !== undefined) {
      j3pDetruit(this.avirrr)
      this.barre()
    }
  }

  focus () {
    if (this.disabled) { return }
    blurAllTabs(this)
    this.isblur = false
    this.majaffiche('')
    this.textaIm.style.display = ''
  }

  gereblur (event) {
    if (!$(event.target).closest('#' + this.textaCont.id).length) {
      this.blur()
    }
  }

  majaffiche (elem) {
    if (this.disabled) { return }
    if (elem === '£') {
      if (this.tocons === undefined) {
        this.majaffiche('')
        return
      }
      this.tocons.focus()
      this.tocons.classList.remove('zsmMqFocusedCursor')
      return
    }
    if (elem !== '') {
      this.bon = undefined
      j3pStyle(this.conteneur, { color: '' })
    }
    if (elem === '/') {
      this.changetype()
      return
    }
    if (elem === '^') {
      if (this.texta.poscurseur === 0) return
      this.expo = true
      this.tocons = this.unTAbc[0][Math.min(this.texta.poscurseur * 2, this.unTAbc[0].length - 1)]
      const jj = addDefaultTable(this.tocons, 2, 1)
      jj[0][0].style.padding = 0
      jj[1][0].style.padding = 0
      jj[0][0].classList.add('zsmMqFocusedCursor')
      jj[0][0].style.fontSize = '50%'
      jj[1][0].style.fontSize = '50%'
      jj[0][0].innerHTML = '&nbsp;'
      jj[1][0].innerHTML = '&nbsp;'
      jj[1][0].style.border = 'Opx'
      jj[1][0].style.outline = '0 solid black'
      this.tocons.classList.remove('zsmMqFocusedCursor')
      this.tocons.style.outline = '0 solid black'
      this.tocons.focus()
      return
    }
    if (elem !== '') {
      if (this.texta.poscurseur2 === 0) {
        if (this.texta.elemnum.length >= this.limite) {
          j3pStyle(this.textaCont, { background: '#ff0000' })
          setTimeout(this.remetblanc.bind(this), 100)
          this.texta.focus()
          return
        }
        if (this.expo) {
          this.texta.elemnum.splice(this.texta.poscurseur, 0, '^{' + elem + '}')
          this.expo = false
        } else {
          this.texta.elemnum.splice(this.texta.poscurseur, 0, elem)
        }
      } else {
        if (this.texta.elemden.length >= this.limite) {
          j3pStyle(this.textaCont, { background: '#ff0000' })
          setTimeout(this.remetblanc.bind(this), 100)
          this.texta.focus()
          return
        }
        if (this.expo) {
          this.texta.elemden.splice(this.texta.poscurseur, 0, '^{' + elem + '}')
          this.expo = false
        } else {
          this.texta.elemden.splice(this.texta.poscurseur, 0, elem)
        }
      }
      this.texta.poscurseur++
    }

    this.tocons = undefined

    const classClavier = (this.divClavier && this.divClavier.style.visibility !== 'hidden') ? 'zsmPictoClavierBarre' : 'zsmPictoClavier'
    if (this.textaTab !== undefined) j3pDetruit(this.textaTab)
    this.textaTab = addTable(this.texta, { nbLignes: 1, nbColonnes: 2, className: 'zoneS3BAse' })
    const perpascell = getCells(this.textaTab)
    this.textaCont = perpascell[0][0]
    this.textaCont.id = j3pGetNewId()
    this.textaIm = perpascell[0][1]
    this.textaIm.classList.add(classClavier)
    this.textaIm.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;'
    this.eLemCoNtz = j3pAddElt(this.textaCont, 'div')
    if (this.sansBord) this.textaCont.style.border = '0px solid black'
    if (this.texta.type === 1) {
      // @todo remplacer ça par un tableau de 2 lignes en mettant la classe borderTop1 sur la cellule du dessous
      // @todo mutualiser tout ça dans une fct addFraction qui ajouterait un tableau d’une colonne et deux lignes et renverrait les deux cellules (numérateur et dénominateur)
      this.tabDiv = addTable(this.eLemCoNtz, { nbLignes: 3, nbColonnes: 1, className: 'zoneS3' })
      this.tabDivc = getCells(this.tabDiv)
      this.tabDivc[1][0].classList.add('height1Black')
      this.unTAb = addTable(this.tabDivc[0][0], {
        nbLignes: 1,
        nbColonnes: this.texta.elemnum.length * 2 + 1,
        className: 'zoneS3'
      })
      this.unTAbc = getCells(this.unTAb)
      this.unTAb2 = addTable(this.tabDivc[2][0], {
        nbLignes: 1,
        nbColonnes: this.texta.elemden.length * 2 + 1,
        className: 'zoneS3'
      })
      this.unTAbc2 = getCells(this.unTAb2)
    } else {
      this.unTAb = addTable(this.eLemCoNtz, {
        nbLignes: 1,
        nbColonnes: this.texta.elemnum.length * 2 + 1,
        className: 'zoneS3'
      })
      this.unTAbc = getCells(this.unTAb)
    }
    this.tabRac = []
    this.textaCont.addEventListener('click', this._clickZoneListener, false)

    let bufel, elti, eltCi, i

    for (i = 0; i < this.texta.elemnum.length; i++) {
      bufel = this.texta.elemnum[i]
      elti = this.unTAbc[0][2 * i]
      eltCi = this.unTAbc[0][2 * i + 1]
      if (bufel.includes('widehat')) {
        j3pStyle(eltCi, { fontSize: '85%' })
        eltCi.vAlign = 'top'
      }
      if (bufel === ' ') {
        eltCi.innerHTML = '&nbsp;'
      } else {
        j3pAffiche(eltCi, null, '$' + this.texta.elemnum[i] + '$')
      }
      eltCi.num = i
      eltCi.num2 = 0
      elti.num = i
      elti.num2 = 0
      elti.tabIndex = 0
      eltCi.addEventListener('click', this._clickcListener, false)
      elti.addEventListener('focus', this._metCurseurListener, false)
      elti.addEventListener('keydown', this._clavierListener, false)
      // @todo expliquer pourquoi il faut ajouter un listener qui supprime la propagation de l’événement pour keypress et keyup
      elti.addEventListener('keypress', this._vireListener, false)
      elti.addEventListener('keyup', this._vireListener, false)
      eltCi.classList.add('noMarginPadding')
    }
    // en fait faut faire une fois de plus pour la dernièrer zone de saisie (tout à droite)
    // exactement la mm boucle que au dessus je la met à la fin
    elti = this.unTAbc[0][this.unTAbc[0].length - 1]
    elti.tabIndex = 0
    elti.addEventListener('focus', this._metCurseurListener, false)
    elti.addEventListener('keydown', this._clavierListener, false)
    elti.addEventListener('keypress', this._vireListener, false)
    elti.addEventListener('keyup', this._vireListener, false)
    elti.classList.add('noMarginPadding')
    if (i === 0) {
      if (this.texta.type === 1) {
        elti.style.backgroundColor = '#aaaaaa'
      }
      elti.innerHTML = '&nbsp;&nbsp;'
      elti.num = elti.num2 = 0
      elti.addEventListener('click', this._clickcListener, false)
    }

    if (this.texta.type === 1) {
      for (i = 0; i < this.texta.elemden.length; i++) {
        bufel = this.texta.elemden[i]
        elti = this.unTAbc2[0][2 * i]
        eltCi = this.unTAbc2[0][2 * i + 1]
        if (bufel.includes('widehat')) {
          j3pStyle(eltCi, { fontSize: '85%' })
          eltCi.vAlign = 'top'
        }
        if (bufel === ' ') {
          eltCi.innerHTML = '&nbsp;'
        } else {
          j3pAffiche(eltCi, null, '$' + this.texta.elemden[i] + '$')
        }
        eltCi.num = i
        eltCi.num2 = 1
        elti.num = i
        elti.num2 = 1
        elti.tabIndex = 0
        eltCi.addEventListener('click', this._clickcListener, false)
        elti.addEventListener('focus', this._metCurseurListener, false)
        elti.addEventListener('keydown', this._clavierListener, false)
        // @todo expliquer pourquoi il faut ajouter un listener qui supprime la propagation de l’événement pour keypress et keyup
        elti.addEventListener('keypress', this._vireListener, false)
        elti.addEventListener('keyup', this._vireListener, false)
        eltCi.classList.add('noMarginPadding')
      }
      // en fait faut faire une fois de plus pour la dernièrer zone de saisie (tout à droite)
      // exactement la mm boucle que au dessus je la met à la fin
      elti = this.unTAbc2[0][this.unTAbc2[0].length - 1]
      elti.tabIndex = 0
      elti.addEventListener('focus', this._metCurseurListener, false)
      elti.addEventListener('keydown', this._clavierListener, false)
      elti.addEventListener('keypress', this._vireListener, false)
      elti.addEventListener('keyup', this._vireListener, false)
      elti.classList.add('noMarginPadding')
      if (i === 0) {
        elti.innerHTML = '&nbsp;&nbsp;'
        elti.style.backgroundColor = '#aaaaaa'
        elti.num = 0
        elti.num2 = 1
        elti.addEventListener('click', this._clickcListener, false)
      }
    }

    this.textaIm.addEventListener('mousedown', this._placeListener, false)
    this.textaIm.style.cursor = 'pointer'

    this.textaTab.style.width = (Math.min(this.textaCont.offsetWidth, 10) + this.textaIm.offsetWidth) + 'px'

    this.textaCont.classList.add('zsmMqFocused')
    this.textaIm.classList.add('zsmMqFocused')

    if (this.hasAutoKeyboard) {
      if (this.divClavierAuto === undefined) this.buildAutoKeyboard()
      this.divClavierAuto.style.visibility = ''
      const mondiv = this.divClavierAuto
      const posbase = this.conteneur.getBoundingClientRect()
      const posMepact = this.parent.getBoundingClientRect()
      const posbase2 = this.divClavierAuto.getBoundingClientRect()
      mondiv.style.left = (posbase.x - posMepact.x) + 'px'
      if (!this.invClav) {
        mondiv.style.top = (posbase.y - posMepact.y - posbase2.height - 5) + 'px'
      } else {
        mondiv.style.top = (posbase.y - posMepact.y + posbase.height + 5) + 'px'
      }
      mondiv.style.zIndex = '105'
    }

    this.textaIm.style.display = ''

    if (!this.isblur) {
      if (this.texta.poscurseur2 === 1) {
        this.unTAbc2[0][Math.min(this.texta.poscurseur * 2, this.unTAbc2[0].length - 1)].focus()
      } else {
        this.unTAbc[0][Math.min(this.texta.poscurseur * 2, this.unTAbc[0].length - 1)].focus()
      }
    } else {
      this.textaIm.style.display = 'none'
    }
    if (this.bon === false) this.corrige(false)
    if (!this.blok && typeof this.onchange === 'function') this.onchange()
  }

  metcurseur (event) {
    event.currentTarget.className = 'zsmMqFocusedCursor'
  }

  remetblanc () {
    j3pStyle(this.textaCont, { background: '#ffffff' })
  }

  reponse () {
    let rep = ''
    for (let i = 0; i < this.texta.elemnum.length; i++) {
      if (this.texta.elemnum[i] === '{}^2') { rep += '^{2}' } else {
        if (this.texta.elemnum[i] === '{}^3') { rep += '^{3}' } else { rep += ' ' + this.texta.elemnum[i] }
      }
    }
    if (this.texta.type === 1) {
      rep = '\\frac{' + rep + '}{'
      // je sais plus pourquoi j’avais ajout ça:
      // rep = '\\frac{' + rep.replace(/ /g, '') + '}{'
      for (let i = 0; i < this.texta.elemden.length; i++) {
        if (this.texta.elemden[i] === '{}^2') { rep += '^{2}' } else {
          if (this.texta.elemden[i] === '{}^3') { rep += '^{3}' } else { rep += ' ' + this.texta.elemden[i] }
        }
      }
      rep += '}'
    }
    return rep
  }

  reponsenb () {
    let rep = ''
    for (let i = 0; i < this.texta.elemnum.length; i++) {
      rep += this.texta.elemnum[i]
    }
    rep = rep.replace(/,/g, '.').replace(')', '').replace('(', '')
    if (rep === '' && (this.texta.type === 0)) { rep = null } else { rep = parseFloat(rep) }

    let rep2 = ''
    for (let i = 0; i < this.texta.elemden.length; i++) {
      rep2 += this.texta.elemden[i]
    }
    rep2 = rep2.replace(/,/g, '.').replace(')', '').replace('(', '')

    if (this.texta.type === 0) rep2 = 1
    rep2 = parseFloat(rep2)

    return [rep, rep2]
  }

  reponse2 () {
    let rep = ''
    for (let i = 0; i < this.texta.elemnum.length; i++) {
      if (this.texta.elemnum[i] === '{}^2') { rep += ' puissance2' } else {
        if (this.texta.elemnum[i] === '{}^3') { rep += ' puissance3' } else { rep += ' ' + this.texta.elemnum[i] }
      }
    }
    rep = rep.replace(/,/g, '.')
    let rep2 = ''
    for (let i = 0; i < this.texta.elemden.length; i++) {
      if (this.texta.elemden[i] === '{}^2') { rep2 += ' puissance2' } else {
        if (this.texta.elemden[i] === '{}^3') { rep2 += ' puissance3' } else { rep2 += ' ' + this.texta.elemden[i] }
      }
    }
    rep2 = rep2.replace(/,/g, '.')
    if (rep2 === '') rep2 = 1
    return [rep, rep2]
  }

  vire (event) {
    event.preventDefault()
    event.stopPropagation()
  }
}

export default ZoneStyleMathquill2
