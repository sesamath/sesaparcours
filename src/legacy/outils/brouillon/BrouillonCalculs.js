import { j3pAddElt, j3pDetruit, j3pEmpty } from 'src/legacy/core/functions'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'

import './brouillonCalculs.scss'

/**
 */
class BrouillonCalculs {
  /**
   * À documenter
   * @param {Object} elts
   * @param {HTMLElement} elts.caseBoutons
   * @param {HTMLElement} elts.caseEgal
   * @param {HTMLElement} elts.caseCalculs
   * @param {HTMLElement} elts.caseApres
   * @param {HTMLElement} elts.caseBulle
   * @param {Object} params
   * @param {string} params.restric
   * @param {number} params.limite
   * @param {number} params.limitenb
   * @param {boolean} params.hasAutoKeyboard
   * @param {number} params.lmax
   */
  constructor (elts, params) {
    const { caseBoutons, caseEgal, caseCalculs, caseApres, caseBulle } = elts
    const { restric, limite, limitenb, hasAutoKeyboard, lmax, isBrouillon, justeg, titre } = params
    this.bull = []
    this.isBrouillon = isBrouillon !== false
    this.lisId = 0
    this.items = []
    this.justeg = justeg
    this.titre = titre
    this.caseBoutons = caseBoutons
    this.caseEgal = caseEgal
    this.caseCalculs = caseCalculs
    this.restric = restric
    this.limite = limite
    this.limitenb = limitenb
    this.hasAutoKeyboard = hasAutoKeyboard
    this.lmax = lmax
    this.nbligne = 0
    this.lignes = []
    this.ceplie = false
    this.disabled = false
    this.linetabDou = caseBoutons.parentNode
    this.tabTotal = this.linetabDou.parentNode
    this.linetabDessous = this.linetabDou.nextElementSibling
    this.caseApres = caseApres
    this.funcAvire = this.makeFirst.bind(this)
    const objet = this
    this.funcCallAdd = function () { objet.add(objet, this) }
    if (!this.justeg) j3pAffiche(caseEgal, '', '$=$')
    this.caseCalculs.style.cursor = 'pointer'
    this.yaeuFirst = false
    this.NomClass = 'caseBrouillon'
    this.caseBulle = caseBulle
    if (this.isBrouillon) {
      this.affAvire = j3pAffiche(this.caseCalculs, '', 'brouillon')
      this.affAvire.parent.style.background = '#aaaaaa'
      this.caseCalculs.addEventListener('click', this.funcAvire, false)
    } else {
      this.NomClass = 'caseFoBrouillon'
      this.funcAvire()
    }
    this.caseBoutons.parentNode.classList.add(this.NomClass)
    if (this.justeg) {
      j3pEmpty(caseEgal)
      // caseBoutons.classList.add('cache')
    }
    // if (this.caseBulle) this.caseBulle.classList.add(this.NomClass)
  }

  suppr (event) {
    const dou = this.trouvDou(event.currentTarget.id2)
    this.items[dou].zone.disable()
    j3pDetruit(this.items[dou].laligne)
    this.items.splice(dou, 1)
    this.nbligne--
    if (this.nbligne === 1) this.items[0].boutons[3].style.display = 'none'
    if (this.nbligne < this.lmax) {
      for (let i = 0; i < this.items.length; i++) {
        this.items[i].boutons[1].style.display = ''
      }
    }
  }

  plidepli () {
    this.ceplie = !this.ceplie
    this.BoutPlie[1].classList.remove('boutonMoins')
    this.BoutPlie[1].classList.remove('boutonPlus')
    if (this.ceplie) {
      this.BoutPlie[1].classList.add('boutonPlus')
      for (let i = 1; i < this.items.length; i++) {
        this.items[i].laligne.style.display = 'none'
      }
      this.items[0].boutons[1].style.display = 'none'
    } else {
      this.BoutPlie[1].classList.add('boutonMoins')
      for (let i = 1; i < this.items.length; i++) {
        this.items[i].laligne.style.display = ''
      }
      if (this.nbligne < this.lmax && !this.disabled) this.items[0].boutons[1].style.display = ''
    }
  }

  makeFirst () {
    this.yaeuFirst = true
    this.caseCalculs.style.cursor = ''
    if (this.isBrouillon) this.affAvire.parent.style.background = ''
    this.nbligne = 1
    if (this.isBrouillon) this.caseCalculs.removeEventListener('click', this.funcAvire, false)
    this.caseCalculs.innerHTML = ''
    this.items[0] = {}
    if (!this.isBrouillon) this.items[0].wraide = this.caseBulle
    this.items[0].zone = new ZoneStyleMathquill3(this.caseCalculs, {
      restric: this.restric,
      limitenb: this.limitenb,
      limite: this.limite,
      hasAutoKeyboard: this.hasAutoKeyboard,
      enter: this.funcCallAdd,
      inverse: true
    })
    this.items[0].boutons = addDefaultTable(this.caseApres, 1, 5)[0]
    this.BoutPlie = addDefaultTable(this.caseBoutons, 1, 3)[0]
    this.items[0].id = 'BrouilL0'
    if (this.justeg) {
      j3pAffiche(this.caseBoutons, null, this.titre)
    }
    if (this.lmax > 1) {
      this.items[0].laligne = this.linetabDou
      if (this.isBrouillon && !this.justeg) {
        this.BoutPlie[1].classList.add('boutonMoins')
        this.BoutPlie[1].addEventListener('click', this.plidepli.bind(this), false)
        this.BoutPlie[0].classList.add('spacer')
        this.BoutPlie[2].classList.add('spacer')
      }
      this.items[0].boutons[1].classList.add('boutonAdd')
      this.items[0].boutons[1].addEventListener('click', this.addbis.bind(this), false)
      this.items[0].boutons[1].id2 = 'BrouilL0'
      this.items[0].boutons[0].classList.add('spacer')
      this.items[0].boutons[2].classList.add('spacer')
      this.items[0].boutons[3].classList.add('boutonSuppr')
      this.items[0].boutons[3].addEventListener('click', this.suppr.bind(this), false)
      this.items[0].boutons[3].style.display = 'none'
    }
  }

  disable () {
    this.disabled = true
    if (this.yaeuFirst) {
      for (let i = 0; i < this.items.length; i++) {
        this.items[i].zone.disable()
        this.items[i].boutons[1].style.display = 'none'
        this.items[i].boutons[3].style.display = 'none'
      }
    } else {
      j3pEmpty(this.caseCalculs)
      const barre = j3pAddElt(this.caseCalculs, 's')
      j3pAddElt(barre, 'em', 'brouillon') // em plus indiqué que i
      this.caseCalculs.style.cursor = ''
      this.affAvire.parent.style.background = '#aaaaaa'
      this.caseCalculs.removeEventListener('click', this.funcAvire, false)
    }
  }

  newId () {
    this.lisId++
    return this.lisId
  }

  trouvDou (id) {
    for (let i = 0; i < this.items.length; i++) {
      if (this.items[i].id === id) return i
    }
    return 0
  }
}

BrouillonCalculs.prototype.add = function add (aff, ff) {
  const dou = this.trouvDou(ff.id2)
  if (aff.nbligne >= this.lmax) return
  const lancien = this.items[dou]
  const tr = document.createElement('tr')
  tr.classList.add(this.NomClass)
  const cells = []
  for (let i = 0; i < 4; i++) {
    cells.push(j3pAddElt(tr, 'td'))
  }
  if (!this.isBrouillon) {
    cells.push(j3pAddElt(tr, 'td', '', { className: this.NomClass }))
  }
  aff.tabTotal.insertBefore(tr, lancien.laligne.nextSibling)
  const item = {}
  if (!this.isBrouillon) {
    item.wraide = cells[4]
  }
  item.laligne = tr
  if (!this.justeg) {
    j3pAffiche(cells[1], '', '$=$')
  } else {
    j3pAffiche(cells[0], null, this.titre)
    j3pEmpty(cells[1])
  }
  const jkid = 'BrouilL' + aff.newId()
  item.caseDeb = cells[0]
  item.caseEgal = cells[1]
  item.zone = new ZoneStyleMathquill3(cells[2], {
    restric: aff.restric,
    limitenb: aff.limitenb,
    limite: aff.limite,
    hasAutoKeyboard: aff.hasAutoKeyboard,
    enter: aff.funcCallAdd,
    inverse: true
  })
  item.boutons = addDefaultTable(cells[3], 1, 5)[0]
  item.boutons[1].classList.add('boutonAdd')
  item.boutons[1].addEventListener('click', aff.addbis.bind(this), false)
  item.boutons[1].id2 = jkid
  item.boutons[3].id2 = jkid
  item.boutons[0].classList.add('spacer')
  item.boutons[2].classList.add('spacer')
  item.boutons[4].classList.add('spacer')
  item.boutons[3].classList.add('boutonSuppr')
  item.boutons[3].addEventListener('click', aff.suppr.bind(this), false)
  item.id = jkid
  aff.items.splice(dou + 1, 0, item)
  aff.nbligne++
  this.items[0].boutons[3].style.display = ''
  if (aff.nbligne >= this.lmax) {
    for (let i = 0; i < this.items.length; i++) {
      aff.items[i].boutons[1].style.display = 'none'
    }
  }
  item.zone.focus()
}

BrouillonCalculs.prototype.addbis = function add (event) {
  this.add(this, event.currentTarget)
}

BrouillonCalculs.prototype.reponse = function reponse () {
  const aret = []
  for (let i = 0; i < this.items.length; i++) {
    aret.push(this.items[i].zone.reponse())
  }
  return aret
}

BrouillonCalculs.prototype.passetoutvert = function passetoutvert () {
  for (let i = 0; i < this.items.length; i++) {
    this.items[i].zone.corrige(true)
  }
}

BrouillonCalculs.prototype.barrelesfo = function barrelesfo () {
  for (let i = 0; i < this.items.length; i++) {
    this.items[i].zone.barreIfKo()
  }
}

BrouillonCalculs.prototype.vireAide = function vireAide () {
  for (let i = this.bull.length - 1; i > -1; i--) {
    this.bull[i].disable()
    this.bull.splice(i, 1)
  }
}

BrouillonCalculs.prototype.aide = function aide (n, k) {
  this.bull.push(new BulleAide(this.items[n].wraide, k, { place: 5 }))
}

BrouillonCalculs.prototype.corrigeligne = function corrigeligne (n) {
  this.items[n].zone.corrige(false)
}

export default BrouillonCalculs
