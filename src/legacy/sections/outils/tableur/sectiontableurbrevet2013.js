import { j3pAjouteZoneTexte, j3pBarre, j3pDiv, j3pElement, j3pFocus, j3pGetRandomInt, j3pNombreBienEcrit, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import { j3pDesactive, j3pMathsAjouteDans } from 'src/lib/mathquill/functions'
import Tableur from 'src/legacy/outils/tableur/Tableur'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen
    juillet 2013

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['n_parametre1', 0, 'entier', 'Description du paramètre'],
    ['s_parametre2', '[4;13]', 'string', 'Description du paramètre'],
    ['b_parametre3', true, 'boolean', 'Description du paramètre'],
    ['t_parametre4', [1, 2, 3], 'array', 'Description du paramètre']
  ]
}

/**
 * section tableurbrevet2013
 * @this {Parcours}
 */
export default function main () {
  const me = this
  // entrée : une expression de la forme ax+b
  // sortie : [a,b]
  function corrigeetape3 (ch) {
    const unarbre = new Tarbre(ch, ['x'])
    let unresultat
    let compare = 0
    let res = true
    for (let k = 1; k <= 10; k++) {
      unresultat = unarbre.evalue([k])
      compare = me.stockage[1][0] * k + me.stockage[1][1]
      const dif = Math.abs(unresultat - compare)
      if (dif > 0.00000001) res = false
    }
    return res
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 3,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      structure: 'presentation3',

      // Paramètres de la section, avec leurs valeurs par défaut.
      a: '[-5;5]',
      b: '[-10;10]',
      df: '[-3;3]', // amplitude 6
      s_parametre2: '[4;13]',
      b_parametre3: true,
      t_parametre4: [1, 2, 3],

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par this.donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes = this.donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        phrase1: 'Ecris '
      },
      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.
      // this.limite =5;
      pe: 0
    }
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = getDonnees()

        this.surcharge()

        // Construction de la page
        this.construitStructurePage(this.donneesSection.structure)

        // par convention, this.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        this.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        this.score = 0
        this.afficheTitre('tableur1')
        if (this.donneesSection.indication) this.indication(this.zones.IG, this.donneesSection.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        j3pElement('question').innerHTML = ''
        j3pElement('explications').innerHTML = ''
        j3pElement('correction').innerHTML = ''
        this.afficheBoutonValider()
      }

      if ((me.questionCourante % me.donneesSection.nbetapes) === 1) {
        if (document.getElementById('modele_conteneur') != null) {
          j3pElement('modele_conteneur').innerHTML = ''
        }

        j3pDiv(this.zones.MG, {
          id: 'presentation',
          contenu: 'On a utilisé un tableur pour calculer les images de différentes valeurs d’une <b>fonction affine <i>f</i></b>.',
          coord: [50, 20],
          style: this.styles.toutpetit.enonce
        }
        )

        j3pDiv(this.zones.MG, {
          id: 'modele_conteneur',
          contenu: '',
          coord: [50, 100],
          style: this.styles.moyen.explications
        })

        let a, b
        do {
          a = j3pGetRandomInt(this.donneesSection.a)
        } while (a === 0)
        do {
          b = j3pGetRandomInt(this.donneesSection.b)
        } while (b === 0)
        const df = this.donneesSection.df
        const df1 = Number(df.substring(1, df.indexOf(';')))
        const df2 = Number(df.substring(df.indexOf(';') + 1, df.indexOf(']')))
        this.tableur = new Tableur('modele_conteneur', { id: 'tableur', nblig: 3, nbcol: df2 - df1 + 2 })

        me.stockage[1] = [a, b, 0, 0, df1, df2]

        this.tableur.ecrit([[1, 1, 'x', { fontStyle: 'italic' }]])
        this.tableur.ecrit([[2, 1, 'f(x)', { fontStyle: 'italic' }]])

        for (let k = 1; k <= df2 - df1 + 1; k++) {
          this.tableur.ecrit([[1, 1 + k, df1 + k - 1, { color: '#000' }], [2, 1 + k, a * (k - 1 + df1) + b, { color: '#000' }]])
        }
        j3pDiv(this.zones.MG, { id: 'question', contenu: '', coord: [50, 250], style: this.styles.petit.enonce })

        // me.stockage[1]=[a,b,question,select,df1,df2]
        const select = j3pGetRandomInt(this.donneesSection.df)
        let question
        do {
          question = j3pGetRandomInt(this.donneesSection.df)
        } while (question === select)
        me.stockage[1][2] = question
        me.stockage[1][3] = select
        this.tableur.select(2, me.stockage[1][3] - me.stockage[1][4] + 2)
        if (me.stockage[1][1] > 0) this.tableur.ecritformule('=' + me.stockage[1][0] + '*' + this.tableur.formule(2, me.stockage[1][3] - me.stockage[1][4] + 2) + '+' + me.stockage[1][1])
        else this.tableur.ecritformule('=' + me.stockage[1][0] + '*' + this.tableur.formule(2, me.stockage[1][3] - me.stockage[1][4] + 2) + me.stockage[1][1])
        j3pElement('question').innerHTML = 'Quelle est l’image de ' + question + ' par <i>f</i> ?'

        j3pAjouteZoneTexte('question', { id: 'reponse1', left: 340, top: -2, maxchars: '5', restrict: /[0-9,-]/, texte: '', width: 80 })
        j3pFocus('reponse1')

        me.stockage[0] = Math.round(question * me.stockage[1][0] + me.stockage[1][1])
      }

      if ((me.questionCourante % me.donneesSection.nbetapes) === 2) {
        // me.stockage[1]=[a,b,question,select,df1,df2]
        me.tableur.unselect(2, this.stockage[1][2] - this.stockage[1][4] + 2)
        this.tableur.select(2, me.stockage[1][3] - me.stockage[1][4] + 2)
        if (me.stockage[1][1] > 0) this.tableur.ecritformule('=' + me.stockage[1][0] + '*' + this.tableur.formule(2, me.stockage[1][3] - me.stockage[1][4] + 2) + '+' + me.stockage[1][1])
        else this.tableur.ecritformule('=' + me.stockage[1][0] + '*' + this.tableur.formule(2, me.stockage[1][3] - me.stockage[1][4] + 2) + me.stockage[1][1])
        const decal = j3pGetRandomInt(4, 8)
        const signe = j3pGetRandomInt(1, 2)
        if (signe == 1) {
          me.stockage[1][2] = me.stockage[1][5] + decal
        } else { me.stockage[1][2] = me.stockage[1][4] - decal }

        j3pMathsAjouteDans('question', { id: 'exp1', content: 'Calculer $f (£a)$', parametres: { a: me.stockage[1][2] } })
        j3pAjouteZoneTexte('question', { id: 'reponse1', left: 240, top: -2, maxchars: '5', restrict: /[0-9,]/, texte: '', width: 80 })
        j3pFocus('reponse1')
        me.stockage[0] = Math.round(me.stockage[1][2] * me.stockage[1][0] + me.stockage[1][1])
      }

      if ((me.questionCourante % me.donneesSection.nbetapes) === 0) {
        j3pMathsAjouteDans('question', { id: 'exp1', content: 'Donner l’expression de $f(x)=$', parametres: { a: me.stockage[1][2] } })
        j3pAjouteZoneTexte('question', { id: 'reponse1', tailletexte: 26, police: 'times new roman', fontStyle: 'italic', left: 300, top: -5, maxchars: '5', restrict: /[0-9x,+-]/, texte: '', width: 80 })
        j3pFocus('reponse1')
      }

      // solution stockée dans une variable globale

      //= A1+$A$2

      /*
             C’est dans le DIV "explications" que les explications sont écrites.
             En général, ces explications figurent à côté de l’énoncé.
             En revanche, en général, on écrit "C’est bon", "C’est faux" , etc, dans la partie Droite de la scène (cad dans this.zones.MD)
             et dans le DIV correction
            */

      j3pDiv(this.zones.MG, { id: 'explications', contenu: '', coord: [50, 360], style: this.styles.petit.correction })

      /// //////////////////////////////////////
      /* FIN DU CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////////

      j3pDiv(this.zones.MG, { id: 'correction', contenu: '', coord: [50, 300], style: this.styles.petit.explications })

      // Obligatoire
      this.finEnonce()

      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const repEleve = j3pValeurde('reponse1')
      if (!repEleve && !this.isElapsed) {
        this.reponseManquante('correction')
        j3pFocus('reponse1')
        return this.finCorrection()
      }

      // Une réponse a été saisie
      let bonnereponse = false
      switch (me.questionCourante % me.donneesSection.nbetapes) {
        case 1:
          bonnereponse = (repEleve == j3pNombreBienEcrit(this.stockage[0]) || repEleve == String(this.stockage[0]))
          break
        case 2:
          bonnereponse = (repEleve == j3pNombreBienEcrit(this.stockage[0]) || repEleve == String(this.stockage[0]))
          break
        case 0 :
          bonnereponse = corrigeetape3(j3pElement('reponse1').value)
      }

      if (bonnereponse) {
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        j3pElement('correction').innerHTML = cBien
        j3pDesactive('reponse1')
        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      j3pElement('correction').style.color = this.styles.cfaux

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        j3pElement('correction').innerHTML = tempsDepasse
        this.typederreurs[10]++
        j3pElement('correction').innerHTML += '<br>La solution était ' + this.stockage[0]
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      j3pElement('correction').innerHTML = cFaux

      // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
      if (this.essaiCourant < this.donneesSection.nbchances) {
        j3pElement('correction').innerHTML += '<br>' + essaieEncore
        this.typederreurs[1]++
        // il reste des essais, on reste dans l’état correction
        return this.finCorrection()
      }

      // Erreur au dernier essai
      const isPositif = this.stockage[1][1] >= 0 // c’est quoi ?
      let ch
      switch (me.questionCourante % me.donneesSection.nbetapes) {
        case 1:
          j3pElement('correction').innerHTML += '<br>La solution était ' + this.stockage[0]
          this.tableur.unselect(2, this.stockage[1][3] - this.stockage[1][4] + 2)
          this.tableur.select(2, this.stockage[1][2] - this.stockage[1][4] + 2)
          this.tableur.ecritformule('')
          j3pDesactive('reponse1')
          j3pBarre('reponse1')
          break
        case 2:
          j3pElement('correction').innerHTML += '<br>La solution était ' + this.stockage[0]
          j3pDesactive('reponse1')
          j3pBarre('reponse1')
          if (isPositif) { j3pElement('explications').innerHTML = 'Comme l’indique la formule ' + j3pElement('zdtformule').value + ', il faut d’abord multiplier par ' + this.stockage[1][0] + ' et ensuite ajouter ' + this.stockage[1][1] } else { j3pElement('explications').innerHTML = 'Comme l’indique la formule ' + j3pElement('zdtformule').value + ', il faut d’abord multiplier par ' + this.stockage[1][0] + ' et ensuite soustraire ' + (-this.stockage[1][1]) }
          j3pElement('explications').innerHTML += '<br>Ce qui donne : '
          if (this.stockage[1][2] < 0) {
            ch = isPositif
              ? '$£a*(£c)+£b=£d$'
              : '$£a*(£c)£b=£d$'
          } else {
            ch = isPositif
              ? '$£a*£c+£b=£d$'
              : '$£a*£c£b=£d$'
          }
          j3pMathsAjouteDans('explications', {
            id: 'exp2',
            content: ch,
            parametres: {
              a: this.stockage[1][0],
              b: this.stockage[1][1],
              c: this.stockage[1][2],
              d: this.stockage[0]
            }
          })
          break
        case 0:
          j3pDesactive('reponse1')
          j3pBarre('reponse1')
          if (isPositif) { j3pElement('explications').innerHTML = 'Comme l’indique la formule ' + j3pElement('zdtformule').value + ', il faut d’abord multiplier par ' + this.stockage[1][0] + ' et ensuite ajouter ' + this.stockage[1][1] } else { j3pElement('explications').innerHTML = 'Comme l’indique la formule ' + j3pElement('zdtformule').value + ', il faut d’abord multiplier par ' + this.stockage[1][0] + ' et ensuite soustraire ' + (-this.stockage[1][1]) }
          j3pElement('explications').innerHTML += '<br>La fonction est donc définie par : '

          ch = isPositif
            ? '$f(x)=£a x + £b$'
            : '$f(x)=£a x £b$'

          j3pMathsAjouteDans('explications', {
            id: 'exp2',
            content: ch,
            parametres: {
              a: this.stockage[1][0],
              b: this.stockage[1][1],
              c: this.stockage[1][2],
              d: this.stockage[0]
            }
          })
          break
      }
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break
    } // case "correction"

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
