import mainMep, { params as mepParams } from './sectionexo_mep'

/*
 * Cette section squelette charge des swf qui sont sur la bibliothèque
  http://localhost:8081/?graphe=[1,"ancienexo_mep",[{pe:"<=0",nn:"2",conclusion:"noeud 2"},{pe:">=1",nn:"1",conclusion:"noeud 1"},{exo:"2842"}]];[2,"ancienexo_mep",[{pe:">=0",nn:"fin",conclusion:"fin"},{exo:"2843"}]];
  A faire : implémenter le paramétrage possible pour afficher le bouton Suite (mais quand même avoir l’envoi final)
 */
/**
 * Cette section squelette charge des swf via la bibliothèque (qui lance le chargement sur mep-col)
 * @todo implémenter le paramétrage possible pour afficher le bouton Suite (mais quand même avoir l’envoi final)
 */

/* ex avec :
 http://localhost:8081/?graphe=[1,"exo_mep_bibli",[{pe:"<=0",nn:"2",conclusion:"noeud 2"},{pe:">=1",nn:"1",conclusion:"noeud 1"},{exo:"2842"}]];[2,"exo_mep_bibli",[{pe:">=0",nn:"fin",conclusion:"fin"},{exo:"2843"}]];
 */

const defaultId = 250

// pour les params ce sont les mêmes, à qq modifs près
export const params = {
  /**
   * @type {LegacySectionParamsParametres}
   */
  parametres: mepParams.parametres.map(param => {
    if (param[0] === 'exo') {
      // on modifie l'exo par défaut
      param[1] = defaultId
    } else if (param[0] === 'listequestions') {
      // et la personalisation du nb de question qui est différente pour le modèle 1
      // (on ne peut que préciser le nb de questions)
      param[0] = 'nbquestions'
      param[3] = 'indiquer 4 pour ne poser que les 4 premières questions (toutes si rien n’est précisé)'
    }
    return param
  })
}

/**
 * section ancienexo_mep
 * @this {Parcours}
 */
export default mainMep
