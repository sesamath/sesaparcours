import { j3pAfficheBibli, j3pGetRessourceBibli, j3pGetUrlParams, j3pShowError } from 'src/legacy/core/functions'
import { addElement } from 'src/lib/utils/dom/main'

/*
     * Cette section squelette charge des swf qui sont sur la bibliothèque
      http://localhost:8081/?graphe=[1,"aide_mep",[{pe:"<=0",nn:"2",conclusion:"noeud 2"},{pe:">=1",nn:"1",conclusion:"noeud 1"},{aide:"2842"}]];[2,"aide_mep",[{pe:">=0",nn:"fin",conclusion:"fin"},{aide:"2843"}]];
      A faire : implémenter le paramétrage possible pour afficher le bouton Suite (mais quand même avoir l’envoi final
     */
const defaultId = 500

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', '', 'string', 'Donner un éventuel indice'],
    ['aide', defaultId, 'entier', 'Identifiant de l’aide mathenpoche (son id dans sa base de données)']
  ]
}

/**
 * section aide_mep
 * @this {Parcours}
 */
export default function main () {
  /**
   * Référence au parcours courant, pour l’utiliser dans les callback
   * @type {Parcours}
   */
  const me = this

  if (this.etat === 'enonce') {
    // c'est une ressource sans score
    this.setPassive()
    if (!this.donneesSection.aide || this.donneesSection.aide === defaultId) {
      j3pShowError('Cette ressource est prévue pour être paramétrée')
      this.donneesSection.aide = defaultId
    }
    this.construitStructurePage('presentation2')
    const { indication, aide } = this.donneesSection
    if (indication) this.indication(this.zonesElts.IG, indication)
    const divExo = addElement(this.zonesElts.MG, 'div')

    // c’est impossible de passer des flashvars en param à l’iframe de la bibli
    // on charge donc le display.js de la bibliothèque et on utilise sa logique
    // pour afficher les exos mep dans notre dom
    // on charge la ressource (json) puis on l’affiche
    const idBibli = 'am/' + aide
    let sesathequeBaseId = j3pGetUrlParams('sesathequeBaseId') || this.sesathequeBaseId || 'sesabibli'
    // si le contexte donne une bibli commun, on va sur la bibli de prod, à priori la seule à avoir des ressources de type am
    // (si c'est sesabidev ou bibliloca on laisse)
    if (sesathequeBaseId.includes('com')) {
      console.warn(`Le contexte indique d’utiliser la bibli ${sesathequeBaseId} mais seule la bibli de prod contient les aides mathenpoche, on utilisera sesabibli`)
      sesathequeBaseId = 'sesabibli'
    }
    j3pGetRessourceBibli(idBibli, sesathequeBaseId, function (error, ressource) {
      if (error) return j3pShowError(error)
      me.afficheTitre(ressource.titre)
      // on ajoute la ressource de la bibli, avec ces params (qui seront passés à son display)
      const params = {
        sesathequeBaseId,
        container: divExo,
        flashvars: {
          idMep: aide
        }
      }
      j3pAfficheBibli(ressource, params, function (error) {
        if (error) return j3pShowError(error)
        me.finEnonce()
      })
    })
  }
  // section passive, pas de case correction ni navigation
}
