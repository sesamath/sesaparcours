import { j3pAfficheBibli, j3pGetRessourceBibli, j3pGetUrlParams, j3pShowError } from 'src/legacy/core/functions'
import { addElement } from 'src/lib/utils/dom/main'

/**
 * Cette section squelette charge des swf via la bibliothèque (qui lance le chargement sur mep-col)
 * @todo implémenter le paramétrage possible pour afficher le bouton Suite (mais quand même avoir l’envoi final)
 */

/* ex avec :
 http://localhost:8081/?graphe=[1,"exo_mep",[{pe:"<=0",nn:"2",conclusion:"noeud 2"},{pe:">=1",nn:"1",conclusion:"noeud 1"},{exo:"2842"}]];[2,"exo_mep",[{pe:">=0",nn:"fin",conclusion:"fin"},{exo:"2843"}]];
 ou
 http://localhost:8081/#action=play&version=1&graph=%7B%22nodes%22%3A%7B%221%22%3A%7B%22id%22%3A%221%22%2C%22section%22%3A%22exo_mep%22%2C%22label%22%3A%22N%C5%93ud%201%22%2C%22title%22%3A%22%22%2C%22maxRuns%22%3A3%2C%22connectors%22%3A%5B%7B%22id%22%3A%222cc8f56d-5c47-446a-ab8d-35c53d24f598%22%2C%22feedback%22%3A%22%22%2C%22typeCondition%22%3A%22none%22%2C%22label%22%3A%22%22%2C%22source%22%3A%221%22%2C%22target%22%3A%222%22%7D%5D%2C%22params%22%3A%7B%22exo%22%3A%222542%22%2C%22listequestions%22%3A%221%2C2%22%7D%2C%22position%22%3A%7B%22x%22%3A0%2C%22y%22%3A0%7D%7D%2C%222%22%3A%7B%22id%22%3A%222%22%2C%22section%22%3A%22%22%2C%22label%22%3A%22N%C5%93ud%202%22%2C%22position%22%3A%7B%22x%22%3A0%2C%22y%22%3A0%7D%7D%7D%2C%22startingId%22%3A%221%22%7D
 */

const defaultId = 4205

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', '', 'string', 'Un éventuel indice affiché à la demande en bas à gauche'],
    ['limite', 0, 'entier', 'Temps maximum pour l’exercice, en secondes'],
    ['exo', defaultId, 'entier', 'identifiant de l’exo mep dans la bdd'],
    ['listequestions', 'tout', 'string', '"tout" ou la liste des questions voulues, par exemple 2,3,7. Attention, tous les exercices n’acceptent pas de démarrer par une autre question que la première, il faut le vérifier…']
  ]
}

/**
 * section exo_mep
 * Attention, cette fct est commune avec la section ancienexo_mep
 * (la distinction se fait sur ds.nbquestions ou ds.listequestions)
 * @this {Parcours}
 */
export default function main () {
  // "use strict"; // pas possible car les fct J3P* que l’on appelle ne le supportent pas
  /** un référence à l’objet this (objet j3p, instance de Parcours) pour l’utiliser dans les callback */
  const me = this
  const ds = this.donneesSection
  const stor = this.storage

  /**
   * Fonction qui sera appelée par display à chaque envoi de données par
   * fonctions_reseau.swf (à la fin de chaque question)
   * @param {Resultat} resultat Les données formatées en Resultat, l’envoi d’origine est dans resultat.original
   *        (cf https://bibliotheque.sesamath.net/vendors/sesamath/Resultat.js)
   */
  function recupereScoreJ3p (resultat) {
    me.score = (resultat.original && resultat.original.score) ?? resultat.score
    try {
      if (stor.isNbQuestionsUnknow) {
        // faut les remettre visible
        me.zonesElts.question.style.visibility = 'visible'
        me.zonesElts.score.style.visibility = 'visible'
        stor.isNbQuestionsUnknow = false
      }
      if (resultat.original && resultat.original.nbq < ds.nbitems) {
        // avec 5 questions et pas 10 on corrige ici (impossible de le savoir avant le premier retour du flash)
        me.surcharge({ nbrepetitions: resultat.original.nbq })
      }
      // normalement c’est le rappel de chaque répétition par le modèle qui incrémente, mais ici il nous rappelle jamais en mode énoncé, faut le gérer nous-même
      // On se fie à la réponse pour savoir à quelle question on en est
      // on enlève tous les b de fin dans la réponse (qui normalement fait toujours la longueur de nbq)
      const repCourante = resultat?.reponse?.replace(/b*$/, '') ?? ''
      me.repetitionCourante = repCourante.endsWith('j')
        ? repCourante.length // erreur au 1er essai
        : Math.min(repCourante.length + 1, ds.nbitems) // c'était le dernier essai on indique la question suivante
      me.questionCourante = me.repetitionCourante
      // et on peut rafraîchir le score
      me.finEnonce()
      // faut pas le bouton ok
      me.cacheBoutonValider()
    } catch (error) {
      console.error('pas reussi à récupérer nbq', error)
    }
    if (resultat.fin === 'o' || resultat.fin === true) {
      // on déclenche ici la fin de l’exo, sauf si on l'a déjà fait
      // (si l'élève clique sur suite dans le flash ça nous envoie le résultat une 2e fois)
      if (stor.isFinished) {
        // pourquoi il disparaît ?
        me.afficheBoutonSectionSuivante()
        return
      }
      me.etat = 'navigation'
      // faut vérifier ça sinon sectionTerminee() ne renverra pas true et le résultat ne sera pas enregistré
      if (me.questionCourante < ds.nbitems) {
        console.error(Error(`l’exo mep dit que c’est fini alors qu’on a questionCourante = ${me.questionCourante} et nbitems = ${ds.nbitems}`))
        me.questionCourante = ds.nbitems
      }
      me.finNavigation()
      stor.isFinished = true
    }
  }

  /**
   * Renvoie les flashvars à passer à display
   * @returns {object}
   */
  function getFlashVars () {
    // On doit passer en flashvars plusieurs éléments :
    // - la fonction de CallBack qui va récupérer le score mais sous plusieurs conditions
    //     (présentes dans fonctions_reseau) :
    // - ch (anciennement cryptage) à "n" pour récuperer le score
    // - affichage_nb_questions à false pour ne pas metre le nb de questions
    //   (faux si pas toutes les questions) en bas à droite
    const flashvars = {
      // les flashvars idMep, modeleMep, abreviationLangue, idSwf, isBoutonSuite, idAide, isBoutonAide, mep_nb_wnk
      // sont imposées par la ressource, pas la peine de les envoyer elles seront écrasées
      // cf https://bibliotheque.sesamath.net/plugins/em/em.js
      idUtilisateur: 'j3p',
      suite_formateur: ds.suite,
      affichage_nb_questions: 'n',
      // nomFonctionCallback : "me.Sectionexo_mep.recupereScoreJ3p",
      // obligatoire pour savoir à la fin quand on a fini l’exo, a priori inutile car géré par fonctions.swf
      retour_fin_exercice: 'o',
      // pour ne pas avoir l’affichage de la boite de dialogue finale, voir si pertinent
      affichage_boite_finale: 'n',
      // visiter:"1",
      // pour avoir les logs de fonctions_reseau dans la console :
      mode_debug: 'o'
    }
    // le seul truc qui change entre les deux sections
    if (ds.nbquestions) {
      // section ancienexo_mep
      flashvars.nombre_questions_limitees = parseInt(ds.nbquestions, 10)
    } else if (ds.listequestions) {
      // section exo_mep
      flashvars.liste_de_questions = ds.listequestions
    }
    return flashvars
  }

  /**
   * Appelle la bibli pour récupérer la ressource, initialise titre
   * en fonction du résultat, dimensionne le div puis charge les swf pour l’affichage.
   * @param {HTMLElement} divExo Le div où mettre l'exo
   */
  function prepareExo (divExo) {
    // c’est impossible de passer des flashvars en param à l’iframe de la bibli
    // on charge donc le display.js de la bibliothèque et on utilise sa logique
    // pour afficher les exos mep dans notre dom
    // on charge la ressource (json) puis on l’affiche
    const idBibli = 'em/' + ds.exo
    let sesathequeBaseId = j3pGetUrlParams('sesathequeBaseId') || me.sesathequeBaseId || 'sesabibli'
    // si le contexte donne une bibli commun, on va sur la bibli de prod, à priori la seule à avoir des ressources de type am
    // (si c'est sesabidev ou bibliloca on laisse)
    if (sesathequeBaseId.includes('com')) {
      console.warn(`Le contexte indique d’utiliser la bibli ${sesathequeBaseId} mais seule la bibli de prod contient les aides mathenpoche, on utilisera sesabibli`)
      sesathequeBaseId = 'sesabibli'
    }

    j3pGetRessourceBibli(idBibli, sesathequeBaseId, function (error, ressource) {
      if (error) {
        j3pShowError(error)
      } else if (ressource && ressource.parametres) {
        // on vire le titre précédent (qui était provisoire, et de toute façon sinon ça déborde)
        me.afficheTitre(ressource.titre || 'Exercice interactif en flash', { replace: true })

        // pour avoir la même couleur de fond que les exos mep
        me.zonesElts.MG.style.backgroundColor = '#F4F4F3'

        // on ajoute la ressource de la bibli, avec
        const params = {
          sesathequeBaseId,
          container: divExo,
          resultatCallback: recupereScoreJ3p,
          flashvars: getFlashVars()// les flashvars, si necessaire
        }
        j3pAfficheBibli(ressource, params, function (error) {
          if (error) return j3pShowError(error)
          me.finEnonce() // passe en etat correction
          // il l’active d’office, faut remasquer ensuite, c’est recupereScoreJ3p qui passera directement en état navigation si l’exo est fini
          me.cacheBoutonValider()
          if (stor.isNbQuestionsUnknow) {
            me.zonesElts.question.style.visibility = 'hidden'
            me.zonesElts.score.style.visibility = 'hidden'
          }
        })
      } else {
        j3pShowError(new Error('Pas de ressource ou ressource incomplète'))
      }
    })
  }

  switch (me.etat) {
    case 'enonce': {
      // if (this.debutDeLaSection) inutile, toujours le cas puisqu’on est jamais rappelé en état enoncé
      if (ds.listequestions && ds.listequestions !== 'tout') {
        ds.listequestions = ds.listequestions.replace(/;/g, ',')
        const tab = ds.listequestions.split(',')
        this.surcharge({ nbrepetitions: tab.length })
        // pas besoin de _rafraichirEtat(), c’est fait dans finEnonce() quand tout est initialisé correctement par le modèle
      } else if (ds.nbquestions) {
        this.surcharge({ nbrepetitions: ds.nbquestions })
      } else {
        // on ne sait pas combien cet exo comporte de questions… faut attendre qu'il nous le dise au premier appel de recupereScoreJ3p
        stor.isNbQuestionsUnknow = true
        this.surcharge({ nbrepetitions: 10 })
      }
      // important, sinon ça va appeler correction avec la touche entrée
      // (et correction ne s'attend à être appelé qu'en temps limité)
      this.validOnEnter = false
      this.construitStructurePage('presentation2')
      // on construit le html
      const divExo = addElement(this.zonesElts.MG, 'div')
      // On met un titre provisoire
      this.afficheTitre('Chargement de la ressource ', { replace: true })
      // et on lance le chargement
      prepareExo(divExo)
      break // case "enonce":
    }

    case 'correction':
      // on ne peut arriver ici que lorsque le temps limite est atteint, on passe directement en navigation
      return this.finCorrection('navigation', true)

    case 'navigation':
      // on est appelé par recupereScoreJ3p si l’exo flash est fini,
      // ou le case correction si le temps limite est écoulé
      this.finNavigation(true)
      break // case "navigation":

    default:
      throw new Error('section appelée avec l’état ' + this.etat)
  }
}
