import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { cleanLatexForMq, unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getIndexFermant } from 'src/lib/utils/string'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Janvier 2020. Revu en décembre 2023.

    Squelette demandant de calculer une expressionv ectorielle de l’écrire sous la forme la plus simple possible
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.

    Le fichier annexe doit contenir un membre nomsPoints constenant les noms des points autorisés pour le calcul
    et un membre nomsVect contenant les noms des vecteurs autorisés par le calcul.

    La figure mtg32 doit contenir des calculs complexes ayant le même nom que les points ou vecteurs autorisés
    mais un vecteur nommé i dans l’exercice devra être associé à un calcul complexe nommé i' dans la figure.
    La figure doit contenir un calcul complexe nommé rep et un calcul nommé reponse qui doit valeur 1 si rep contient
    la (ou une des) formule attendue, 2 si le calcul correspond à la réponse mais n’est pas fini et 0 sinon.
    Elle doit aussi contenit un calcul complexe nommé vect0 ayant pour valeur 0 et défini avant le calcul complexe rep.
    Ce calcul représente le vecteur nul.

    Si la figure contient un calcul nommé nbVar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbVar
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['titre', '', 'string', 'Titre de l’activité'],
    ['validationAuto', true, 'boolean', 'Si false (par défaut) :' +
    '<br>L’élève valide ses calculs intermédiaires par la touche Entrée et, quand il estime avoir répondu à la question, clique sur le bouton OK pour valider sa réponse' +
    '<br>Le nombre de calculs intermédiaires permis est nbEssais et le nombre de validations est nbchances' +
    '<br>Si true :' +
    '<br>L’élève valide ses calculs en appuyant sur la touche Entrée ou en cliquant sur le bouton OK.' +
    '<br>Dès qu’une des réponses intermédiaires est acceptée comme réponse finale, la réponse est acceptée.' +
    '<br>L’élève peut faire nbEssais propositions et le paramètre nbchances n’est ici pas utilisé.'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['fig', '', 'editor', 'Chaîne Base 64 contenant la figure de l’exercice', initEditor],
    ['width', 800, 'entier', 'Largeur de la figure initiale'],
    ['height', 600, 'entier', 'Hauteur de la figure initiale'],
    ['entete', '\\vec{u}', 'string', 'Ce qui doit être affiché devant le signe = pour l’expression à calculer (en LaTeX)'],
    ['bigSize', false, 'boolean', 'true pour un éditeur de grande taille'],
    ['nbLatex', 1, 'entier', 'Nombre d’affichages LaTeX de la figure utilisés dans la consigne (1 au moins et jusque 4)'],
    ['charset', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul, chaîne vide pour autoriser tous les caractères'],
    ['enonceLigne1', '', 'string', 'Première ligne de la consigne initiale'],
    ['enonceLigne2', '', 'string', 'Ligne 2 de la consigne (peut être une chaîne vide)'],
    ['enonceLigne3', '', 'string', 'Ligne 3 de la consigne (peut être une chaîne vide)'],
    ['enonceLigne4', '', 'string', 'Ligne 4 de la consigne si validationAuto est false (peut être une chaîne vide)'],
    ['enonceFin', 'Appuyer sur la touche Entrée pour valider toutes les étapes, puis sur le bouton OK après la réponse finale pour valider.', 'string', 'Fin de la consigne'],
    ['enonceFinPourValidationAuto', 'Appuyer sur la touche Entrée ou cliquer sur le bouton OK pour valider les étapes du calcul.', 'string', 'Fin de la consigne en cas de validation automatique'],
    ['btnPuis', true, 'boolean', 'Bouton puissance'],
    ['btnFrac', true, 'boolean', 'Bouton fraction'],
    ['btnPi', false, 'boolean', 'Bouton pi'],
    ['btnCos', false, 'boolean', 'Bouton cosinus'],
    ['btnSin', false, 'boolean', 'Bouton sinus'],
    ['btnTan', false, 'boolean', 'Bouton tangente'],
    ['btnRac', true, 'boolean', 'Bouton Racine carrée'],
    ['btnExp', false, 'boolean', 'Bouton exponentielle'],
    ['btnLn', false, 'boolean', 'Bouton logarithme népérien'],
    ['param', 'abcd', 'string', 'Chaîne formé des paramètres que l’exercice utilise (de a à r, voir ci-dessous'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r']
  ]
}

const inputId = 'expressioninputmq1'

// cf src/legacy/sections/outils/mtg32/sectionex_Calc_Multi_Edit_Multi_Etapes.js pour ajouter un validator
/**
 * Ajoute l’éditeur mathgraph dans container, la promesse sera résolue avec la figure
 * @param {HTMLElement} container
 * @param {string} initalFig
 * @return {Promise<string>}
 */
async function initEditor (container, fig) {
  const { default: editor } = await import('./mtgEditor.js')
  return editor(container, { fig })
}

/**
 * section ex_Calc_Vect
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup (ev, parcours) {
    if (sq.marked) {
      demarqueEditeurPourErreur()
    }
    if (!sq.validationAuto && ev.keyCode === 13) {
      let valide
      let rep = getMathliveValue(inputId)
      if (rep !== '') {
        rep = traiteMathlive(rep)
        const chcalcul = rep.res
        if (rep.vectNul) {
          valide = true
          sq.resultatSyntaxe = {
            syntaxOK: true,
            syntaxVecOK: true,
            isVec: true
          }
        } else {
          const tabNames = []
          for (let k = 0; k < sq.nomsVect.length; k++) {
            let nom = sq.nomsVect[k]
            if (nom === 'i') nom = "i'"
            tabNames.push(nom)
          }
          for (let k = 0; k < sq.nomsPoints.length; k++) {
            let nom = sq.nomsPoints[k]
            if (nom === 'i') nom = "i'"
            tabNames.push(nom)
          }
          sq.resultatSyntaxe = sq.mtgAppLecteur.calcVectOK('mtg32svg', chcalcul, tabNames)
          valide = rep.valid && sq.resultatSyntaxe.syntaxOK
        }
        if (valide) {
          validation(parcours)
        } else {
          marqueEditeurPourErreur()
          j3pElement('correction').style.color = parcours.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          focusIfExists(inputId)
        }
      } else {
        focusIfExists(inputId)
      }
    }
  }

  function contientSomOuDifPoints (ch) {
    let i, j
    for (i = 0; i < sq.nomsPoints.length; i++) {
      for (j = 0; j < sq.nomsPoints.length; j++) {
        if (ch.indexOf(sq.nomsPoints[i] + '-' + sq.nomsPoints[j]) !== -1) return true
      }
    }
    for (i = 0; i < sq.nomsPoints.length; i++) {
      for (j = i; j < sq.nomsPoints.length; j++) {
        if (ch.indexOf(sq.nomsPoints[i] + '+' + sq.nomsPoints[j]) !== -1) return true
      }
    }
    return false
  }
  function contientVectSousVect (ch) {
    let i, j
    for (i = 0; i < sq.nomsVect.length; i++) {
      let st1 = sq.nomsVect[i]
      if (st1 === 'i') st1 = "i'"
      for (j = 0; j < sq.nomsVect.length; j++) {
        let st2 = sq.nomsVect[j]
        if (st2 === 'i') st2 = "i'"
        if (ch.indexOf('\\####{' + st1 + st2) !== -1) return true
      }
      // Correction version Sésaparcours : il aut aussi refuser un vecteur sous une flèche avec un point
      for (j = 0; j < sq.nomsPoints.length; j++) {
        if ((ch.indexOf('\\####{' + st1 + sq.nomsPoints[j]) !== -1) ||
            (ch.indexOf('\\####{' + sq.nomsPoints[i] + st1) !== -1)) return true
      }
    }
    return false
  }
  // Fonction qui renvoie false si la chaîne ch contient des noms de vecteurs qui n’ont pas été inclus dans des parenthèses
  // Utilisée dans traiteMathlive
  function contientVectSansFleche (ch) {
    for (let i = 0; i < sq.nomsVect.length; i++) {
      const st = sq.nomsVect[i]
      let k = ch.indexOf(st)
      if (k !== -1) {
        if (k === 0) return true
        else {
          if (!ch.substring(0, k).trim().endsWith('\\####{')) return true
          k = ch.indexOf(st, k + 1)
        }
      }
    }
    return false
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathQuill (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMq(latex.chaineLatex)
  }

  function extraitTab (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return []
    let ch = cleanLatexForMq(latex.chaineLatex, { keepText: true })
    // ici on sépare let \text différemment de
    if (ch.indexOf('\\text{') === 0) {
      const idaf = getIndexFermant(ch, 5)
      if (idaf === -1) return []
      ch = ch.substring(6, idaf)
    }
    return ch.split('//')
  }

  function traiteMathlive (ch) {
    if (ch === '\\overrightarrow{0}') return { res: '0', valid: true, vectNul: true, contientVecsNuls: false }
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    // On regarde si la réponse contient des vecteurs nuls du type vec(AA).
    // Si oui une réponse ne sera pas accepté comme simplifiée
    let contientVecsNuls = false
    let st = sq.nomsPoints
    // ON remplace tous les \overrightarrow par des \#### de façon qu’il n’y ait pas dedans de lettre
    // succeptible d'être un nom de vecteur
    ch = ch.replace(/\\overrightarrow/g, '\\####')
    if (ch.indexOf('\\####{0}') !== -1) contientVecsNuls = true
    else {
      for (let i = 0; i < st.length; i++) {
        const nom = st[i]
        if (ch.indexOf('\\####{' + nom + nom + '}') !== -1) {
          contientVecsNuls = true
          break
        }
      }
    }
    ch = ch.replace(/\\####\{0}/g, '(v000)') // à cause de l’appel à addImplicitMult, on met v000 qui sera remplacé à la fin par vect0
    // Attention : Les parenthèses entourant le v0 sont indispensables
    const bcontientVectSansFleche = contientVectSansFleche(ch)
    st = sq.nomsVect
    for (let i = 0; i < st.length; i++) {
      if (st[i] !== 'i') ch = ch.replace(new RegExp('\\\\####{(' + st[i] + ')}', 'g'), '$1')
    }
    const bsomOuDifPt = contientSomOuDifPoints(ch)
    const bvectDeVect = contientVectSousVect(ch)
    //  = ch.replace(/\\vecteur{i}/g, "i'") // i n’est pas un nom correct dans mtg32
    ch = ch.replace(/\\####\{i}/g, "i'") // i n’est pas un nom correct dans mtg32
    ch = ch.replace(/\\####\{([a-zA-Z]['"]?)([a-zA-Z]['"]?)}/g, '($2-$1)')

    const correct = (ch.indexOf('\\####{') === -1) && !bsomOuDifPt && !bvectDeVect && !bcontientVectSansFleche
    ch = unLatexify(ch)
    // S’il reste des \vecteur{ c’est que l’expression est incorrecte
    if (correct) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
    ch = ch.replace(/v000/g, 'vect0')
    return { res: ch, valid: correct, vectNul: false, contientVecsNuls }
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  function afficheInfo () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    let nbc = sq.nbchances - sq.numEssai + 1
    if (!sq.validationAuto && nbc > nbe) {
      nbc = nbe
    }
    if (sq.validationAuto) {
      if (nbe === 1) {
        afficheMathliveDans('info', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
      } else {
        afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
      }
    } else {
      if ((nbe === 1) && (nbc === 1)) {
        afficheMathliveDans('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
      } else {
        afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', { style: { color: '#7F007F' } })
      }
    }
  }

  // Le deuxième paramètre est à false seulement quand elle est appelée depuis l’étape de correction
  // Sinon la fonction est appelée par l’éditeur quand l’utilisateur appuie sur la touche entrée avec un seul paramètre
  function validation (parcours, continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = getMathliveValue(inputId)
    if (sq.resultatSyntaxe.syntaxVecOK && sq.resultatSyntaxe.isVec) {
      const resMathQuill = traiteMathlive(sq.rep)
      const chcalcul = resMathQuill.res
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
      j3pElement('correction').innerHTML = ''
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      if (sq.indicationfaute) {
        const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute', true)
        if (faute === 1) {
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
        } else {
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
      }
      sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse', true)
      if ((sq.reponse === 1) && resMathQuill.contientVecsNuls) sq.reponse = 2 // Pour ne pas accepter une réponse avec des vec(AA)
    } else { sq.reponse = 3 } // Considéré comme faux si erreur de syntaxe sur les vecteurs
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    if (sq.resultatSyntaxe.syntaxVecOK && sq.resultatSyntaxe.isVec && sq.reponse >= 1) {
      sq.paramLatex.style = { color: sq.reponse === 1 ? parcours.styles.cbien : parcours.styles.colorCorrection }
      afficheMathliveDans('formules', idrep, chdeb + '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + '$ $= ' +
        sq.rep + '$', sq.paramLatex)
    } else {
      sq.paramLatex.style = { color: parcours.styles.cfaux }
      if (sq.resultatSyntaxe.syntaxVecOK && sq.resultatSyntaxe.isVec) {
        afficheMathliveDans('formules', idrep, chdeb + '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + '$ $\\ne ' +
          sq.rep + '$', sq.paramLatex)
      } else { // On compte une  écriture incorrecte sur les vecteurs comme une faute
        afficheMathliveDans('formules', idrep, chdeb + '$' + '\\text{Invalide : }' + (sq.entete === '' ? sq.aCalculer : sq.entete) + ' = ' +
          sq.rep + '$', sq.paramLatex)
      }
    }
    // On vide le contenu de l’éditeur MathQuill
    j3pElement(inputId).value = ''

    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
      }
    } else {
      afficheInfo()
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    j3pElement(inputId).value = sq.rep
    focusIfExists(inputId)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
    const ch = extraitDeLatexPourMathQuill('solution')
    if (ch !== '') {
      $('#divSolution').css('display', 'block').html('')
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    // on charge mathgraph
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, par, car, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      sq.nbexp = 0
      sq.reponse = -1
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar', true)
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (let i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i, true)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (let j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (let i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      // On récupère les noms des points et vecteurs autorisés dans les calculs
      sq.nomsPoints = extraitTab('point')
      sq.nomsVect = extraitTab('vect')
      const nbParam = parseInt(sq.nbLatex)
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (let i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) sq.aCalculer = code
        param[t[i]] = code
      }
      sq.paramLatex = param
      sq.enonce = sq.enonce.replace(/\\vecteur/g, '\\overrightarrow')
      afficheMathliveDans('enonce', 'texte', sq.enonce, param)
      if (sq.entete !== '') {
        afficheMathliveDans('debut', 'acalc', '$' + sq.entete + '$ = ', param)
      } else {
        afficheMathliveDans('debut', 'acalc', '$' + sq.aCalculer + sq.symbexact + '$ ', param)
      }

      sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
      // Les 3 lignes suivantes déplacées ici pour que sq.mtgAppLecteur soit prêt quand onkeyup est appelé
      j3pElement(inputId).onkeyup = function (ev) {
        onkeyup(ev, parcours)
      }
      afficheInfo()
      focusIfExists(inputId)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    if (sq.bigSize) $('#formules').css('font-size', '26px')
    j3pDiv('conteneur', 'info', '')
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'

    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    if (sq.bigSize) $('#editeur').css('font-size', '30px')
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pAddElt('editeur', 'span', '', { id: 'debut' })
    afficheMathliveDans('editeur', 'expression', '&1&', {
      charset: sq.charset,
      listeBoutons: sq.listeBoutons
    })
    sq.listeBoutons = ['vecteur']
    // Les boutonsMathQuill
    const tabBoutons1 = ['fraction', 'puissance', 'racine', 'pi', 'exp', 'ln', 'sin', 'cos', 'tan']
    ;['btnFrac', 'btnPuis', 'btnRac', 'btnPi', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan'].forEach((p, i) => {
      if (sq[p]) sq.listeBoutons.push(tabBoutons1[i])
    })
    if (sq.charset !== '') {
      mathliveRestrict(inputId, sq.charset)
    }
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block')
    $('#boutonsmathquill').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    j3pPaletteMathquill('boutonsmathquill', inputId, { liste: sq.listeBoutons })
    j3pDiv('conteneur', 'divSolution', '')
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: 780,
      height: 900
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        this.validOnEnter = false // ex donneesSection.touche_entree
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1

        sq.validationAuto = parcours.donneesSection.validationAuto
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        if (!sq.validationAuto && (sq.nbchances > sq.nbEssais)) sq.nbchances = sq.nbEssais

        // Si TRUE alors la touche ENTREE appelle la correction
        // Ne fonctionne QUE SI la scène dispose d’une zone de saisie
        this.validOnEnter = sq.validationAuto
        for (let i = 1; i < 5; i++) sq['enonceLigne' + i] = parcours.donneesSection['enonceLigne' + i]
        sq.fig = parcours.donneesSection.fig
        sq.enonceFin = parcours.donneesSection.enonceFin
        sq.enonceFinPourValidationAuto = parcours.donneesSection.enonceFinPourValidationAuto
        sq.width = parcours.donneesSection.width
        sq.height = parcours.donneesSection.height
        sq.nbLatex = parcours.donneesSection.nbLatex // Le nombre de paramètres LaTeX dans le texte
        sq.titre = parcours.donneesSection.titre
        sq.entete = parcours.donneesSection.entete
        sq.bigSize = parcours.donneesSection.bigSize
        sq.charset = parcours.donneesSection.charset
        const tabBtn = ['Puis', 'Frac', 'Rac', 'Pi', 'Cos', 'Sin', 'Tan', 'Exp', 'Ln']
        tabBtn.forEach((p) => {
          sq['btn' + p] = parcours.donneesSection['btn' + p]
        })
        if (sq.fig === '') {
          sq.fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAUcAAAC0gAAAQEAAAAAAAAAAQAAACv#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAPQ0NhbGN1bENvbXBsZXhlAP####8ABXZlY3QwAAEwAAAAAQAAAAAAAAAA#####wAAAAEAB0NDYWxjdWwA#####wAFbmJ2YXIAATUAAAABQBQAAAAAAAAAAAADAP####8ABm5iY2FzMQABMgAAAAFAAAAAAAAAAAAAAAMA#####wAGbmJjYXMyAAE0AAAAAUAQAAAAAAAAAAAAAwD#####AAZuYmNhczMAATIAAAABQAAAAAAAAAAAAAADAP####8ABm5iY2FzNAABNAAAAAFAEAAAAAAAAAAAAAMA#####wAGbmJjYXM1AAEyAAAAAUAAAAAAAAAAAAAAAwD#####AAJyMQATaW50KHJhbmQoMCkqbmJjYXMxKf####8AAAACAAlDRm9uY3Rpb24C#####wAAAAEACkNPcGVyYXRpb24CAAAABBEAAAABAAAAAAAAAAA#h+BxuWPrAP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAADAAAAAwD#####AAJyMgATaW50KHJhbmQoMCkqbmJjYXMyKQAAAAQCAAAABQIAAAAEEQAAAAEAAAAAAAAAAD#oQKkuomikAAAABgAAAAQAAAADAP####8AAnIzABNpbnQocmFuZCgwKSpuYmNhczMpAAAABAIAAAAFAgAAAAQRAAAAAQAAAAAAAAAAP5Z7Ris#tAAAAAAGAAAABQAAAAMA#####wACcjQAE2ludChyYW5kKDApKm5iY2FzNCkAAAAEAgAAAAUCAAAABBEAAAABAAAAAAAAAAA#wtz84iIC8AAAAAYAAAAGAAAAAwD#####AAJyNQATaW50KHJhbmQoMCkqbmJjYXM1KQAAAAQCAAAABQIAAAAEEQAAAAEAAAAAAAAAAD#ATDMAv15QAAAABgAAAAcAAAADAP####8AAWEADigtMSlecjEqKHIyKzEpAAAABQL#####AAAAAQAKQ1B1aXNzYW5jZf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABP#AAAAAAAAAAAAAGAAAACAAAAAUAAAAABgAAAAkAAAABP#AAAAAAAAAAAAADAP####8AAmInAA4oLTEpXnIzKihyNCsxKQAAAAUCAAAABwAAAAgAAAABP#AAAAAAAAAAAAAGAAAACgAAAAUAAAAABgAAAAsAAAABP#AAAAAAAAAAAAADAP####8AAWIAIHNpKGInPS1hLGInK2ludChyYW5kKDApKjMrMSksYicp#####wAAAAEADUNGb25jdGlvbjNWYXIAAAAABQgAAAAGAAAADgAAAAgAAAAGAAAADQAAAAUAAAAABgAAAA4AAAAEAgAAAAUAAAAABQIAAAAEEQAAAAEAAAAAAAAAAD#enDZl+3IwAAAAAUAIAAAAAAAAAAAAAT#wAAAAAAAAAAAABgAAAA4AAAADAP####8AAWYABHI1KzEAAAAFAAAAAAYAAAAMAAAAAT#wAAAAAAAAAAAAAwD#####AARmb3JtAAFmAAAABgAAABAAAAADAP####8ABWZvcm0xAAZmb3JtPTEAAAAFCAAAAAYAAAARAAAAAT#wAAAAAAAA#####wAAAAIABkNMYXRleAD#####AQAA#wEAAP####8QQHb4AAAAAABARC4UeuFHrwAAAAAAAAAAAAAAAAABAAAAAAAAAAAAWVx2ZWN7dX09XFZhbHthLDIsMX1cb3ZlcnJpZ2h0YXJyb3cge1xtYXRocm17QUN9fVxWYWx7YiwyLCsxfVxvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtCQ319AAAACgD#####AQAA#wEAAP####8QQIScAAAAAABAQq4UeuFHrgAAAAAAAAAAAAAAAAABAAAAAAAAAAAASFxJZntmb3JtMX17XG92ZXJyaWdodGFycm93IHtcbWF0aHJte0JDfX19e1xvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtBQ319fQAAAAIA#####wABQQAWMS4xMzM4NjU0MysxLjIxNzc3NTQqaQAAAAUAAAAAAT#yJFATvqO#AAAABQIAAAABP#N8Ag7N+R3#####AAAAAQALQ0NvbnN0YW50ZWkAAAACAP####8AAUIAFjEuMzM3ODY1NDMrMS40MTk5NzU0KmkAAAAFAAAAAAE#9WfllMTInAAAAAUCAAAAAT#2uDggAf4HAAAACwAAAAIA#####wABQwAVMS41Mzc4MTEzKzEuNTE5OTkxMippAAAABQAAAAABP#ia4AWOtToAAAAFAgAAAAE#+FHiSuNTIQAAAAsAAAADAP####8AA2FwYgADYStiAAAABQAAAAAGAAAADQAAAAYAAAAPAAAAAgD#####AARzb2wxABFhKihCLUEpK2FwYiooQy1CKQAAAAUAAAAABQL#####AAAAAQAXQ1Jlc3VsdGF0VmFsZXVyQ29tcGxleGUAAAANAAAABQEAAAAMAAAAFgAAAAwAAAAVAAAABQIAAAAMAAAAGAAAAAUBAAAADAAAABcAAAAMAAAAFgAAAAIA#####wAEc29sMgARYXBiKihDLUEpLWIqKEItQSkAAAAFAQAAAAUCAAAADAAAABgAAAAFAQAAAAwAAAAXAAAADAAAABUAAAAFAgAAAAwAAAAPAAAABQEAAAAMAAAAFgAAAAwAAAAVAAAAAgD#####AANyZXAAATAAAAABAAAAAAAAAAD#####AAAAAwAQQ1Rlc3RFcXVpdmFsZW5jZQD#####AAR0ZXExAAAAGQAAABsBAQAAAAE#8AAAAAAAAAEAAAANAP####8ABHRlcTIAAAAaAAAAGwEBAAAAAT#wAAAAAAAAAf####8AAAABAA1DRm9uY0NvbXBsZXhlAP####8ABHplcm8AEmFicyh6KTwwLjAwMDAwMDAwMQAAAAUEAAAABED#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAAAT4RLgvoJtaVAAF6AAAAAgD#####AAV0ZXN0MQAOemVybyhyZXAtc29sMSn#####AAAAAQAWQ0FwcGVsRm9uY3Rpb25Db21wbGV4ZQAAAB4AAAAFAQAAAAwAAAAbAAAADAAAABkAAAACAP####8ABXRlc3QyAA56ZXJvKHJlcC1zb2wyKQAAABAAAAAeAAAABQEAAAAMAAAAGwAAAAwAAAAa#####wAAAAEADUNQYXJ0aWVSZWVsbGUA#####wAGdGVzdCcxAAAAHwAAABEA#####wAGdGVzdCcyAAAAIAAAAAMA#####wAFZXhhY3QAG3NpKGZvcm0xLHRlc3QnMT0xLHRlc3QnMj0xKQAAAAkAAAAABgAAABIAAAAFCAAAAAYAAAAhAAAAAT#wAAAAAAAAAAAABQgAAAAGAAAAIgAAAAE#8AAAAAAAAAAAAAMA#####wAGcmVzb2x1ABNzaShmb3JtMSx0ZXExLHRlcTIpAAAACQAAAAAGAAAAEgAAAAYAAAAcAAAABgAAAB0AAAADAP####8AB3JlcG9uc2UAGnNpKHJlc29sdSwxLHNpKGV4YWN0LDIsMCkpAAAACQAAAAAGAAAAJAAAAAE#8AAAAAAAAAAAAAkAAAAABgAAACMAAAABQAAAAAAAAAAAAAABAAAAAAAAAAAAAAADAP####8ABmFlZ2FsMQADYT0xAAAABQgAAAAGAAAADQAAAAE#8AAAAAAAAAAAAAMA#####wAGYmVnYWwxAANiPTEAAAAFCAAAAAYAAAAPAAAAAT#wAAAAAAAAAAAAAwD#####AANvcGIAAi1iAAAACAAAAAYAAAAPAAAACgD#####AQAA#wEACHNvbHV0aW9u#####xRAKQAAAAAAAEAuuFHrhR64AQHv7#sAAAAAAAAAAAAAAAEAAAAAAAAAAARSXGJlZ2lue2FycmF5fXtsfQpcdGV4dHtFbiB1dGlsaXNhbnQgbGEgZm9ybXVsZSBkZSBDaGFzbGVzIDp9ClxJZntmb3JtMX0KewpcXFx0ZXh0eyRcdmVje3V9PVxJZnthZWdhbDF9e317XFZhbHthLDAsMX0ofVxvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtBQn19K1xvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtCQ319XElme2FlZ2FsMX17fXspfVxWYWx7YiwyLCsxfVxvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtCQ319JH0KXElme2FlZ2FsMX17fXtcXFx0ZXh0eyRcdmVje3V9PVxWYWx7YSwyLDF9XG92ZXJyaWdodGFycm93IHtcbWF0aHJte0FCfX1cVmFse2EsMiwrMX1cb3ZlcnJpZ2h0YXJyb3cge1xtYXRocm17QkN9fVxWYWx7YiwyLCsxfVxvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtCQ319JH19ClxcXHRleHR7ZG9uYyAkXHZlY3t1fT1cVmFse2EsMiwxfVxvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtBQn19XFZhbHthcGIsMiwrMX1cb3ZlcnJpZ2h0YXJyb3cge1xtYXRocm17QkN9fSR9Cn0KewpcXFx0ZXh0eyRcdmVje3V9PVxWYWx7YSwyLDF9XG92ZXJyaWdodGFycm93IHtcbWF0aHJte0FDfX1cSWZ7YmVnYWwxfXsrfXtcVmFse2IsMiwrMX0ofVxvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtCQX19K1xvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtBQ319XElme2JlZ2FsMX17fXspfSR9ClxcXHRleHR7JFx2ZWN7dX09XFZhbHthLDIsMX1cb3ZlcnJpZ2h0YXJyb3cge1xtYXRocm17QUN9fVxJZntiZWdhbDF9e317XFZhbHtiLDIsKzF9KH0tXG92ZXJyaWdodGFycm93IHtcbWF0aHJte0FCfX0rXG92ZXJyaWdodGFycm93IHtcbWF0aHJte0FDfX1cSWZ7YmVnYWwxfXt9eyl9JH0KXFxcdGV4dHskXHZlY3t1fT1cVmFse2EsMiwxfVxvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtBQ319XFZhbHtvcGIsMiwrMX1cb3ZlcnJpZ2h0YXJyb3cge1xtYXRocm17QUJ9fVxWYWx7YiwyLCsxfVxvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtBQ319JH0KXFxcdGV4dHtkb25jICRcdmVje3V9PVxWYWx7b3BiLDIsMX1cb3ZlcnJpZ2h0YXJyb3cge1xtYXRocm17QUJ9fVxWYWx7YXBiLDIsKzF9XG92ZXJyaWdodGFycm93IHtcbWF0aHJte0FDfX0kfQp9IApcXCAKXGVuZHthcnJheX0AAAAKAP####8BAAAAAQAFcG9pbnT#####EEB3cAAAAAAAQGBVwo9cKPYAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAA5cdGV4dHtBLy9CLy9Dff###############w=='
          sq.enonceLigne1 = 'Il faut écrire $£a$ sous la forme la plus simple possible en fonction des vecteurs $\\vecteur{\\mathrm{AB}}$ et $£b$ (sans factoriser).'
          sq.enonceLigne2 = ''
          sq.enonceLigne3 = ''
          sq.enonceLigne4 = ''
          sq.nbLatex = 2
          sq.width = 0
          sq.height = 0
          sq.charset = '+-\\dABC()'
          sq.titre = 'Ressource à personnaliser'
          j3pShowError('Cette ressource doit être personnalisée en éditant la ressource dans l’éditeur de graphes.')
        }
        sq.enonce = ''
        for (let i = 1; i < 5; i++) {
          if (sq['enonceLigne' + i] !== '') {
            if (i !== 1) sq.enonce += '<br>'
            sq.enonce += sq['enonceLigne' + i]
          }
        }
        sq.enonce += '<br>' + (sq.validationAuto ? parcours.donneesSection.enonceFinPourValidationAuto : parcours.donneesSection.enonceFin)
        initMtg()
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pElement('enonce').innerHTML = ''
        j3pElement('debut').innerHTML = ''
        j3pElement('formules').innerHTML = ''
        j3pElement('info').innerHTML = ''
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nbLatex)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) sq.aCalculer = code
          param[t[i]] = code
        }
        sq.enonce = sq.enonce.replace(/\\vecteur/g, '\\overrightarrow')
        afficheMathliveDans('enonce', 'texte', sq.enonce, param)
        if (sq.entete !== '') {
          afficheMathliveDans('debut', 'acalc', '$' + sq.entete + '$ = ', param)
        } else {
          afficheMathliveDans('debut', 'acalc', '$' + sq.aCalculer + sq.symbexact + '$ ', param)
        }
        afficheInfo()
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {}
        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        j3pElement(inputId).value = ''
        focusIfExists(inputId)
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      const ch = getMathliveValue(inputId)
      if (ch !== '') {
        const rep = traiteMathlive(ch)
        const repcalcul = rep.res
        const tabNames = []
        let valide
        if (rep.vectNul) {
          sq.resultatSyntaxe = {
            syntaxOK: true,
            syntaxVecOK: true,
            isVec: true
          }
          valide = true
        } else {
          for (let k = 0; k < sq.nomsVect.length; k++) {
            let nom = sq.nomsVect[k]
            if (nom === 'i') nom = "i'"
            tabNames.push(nom)
          }
          for (let k = 0; k < sq.nomsPoints.length; k++) {
            let nom = sq.nomsPoints[k]
            if (nom === 'i') nom = "i'"
            tabNames.push(nom)
          }
          sq.resultatSyntaxe = sq.mtgAppLecteur.calcVectOK('mtg32svg', repcalcul, tabNames)
          valide = rep.valid && sq.resultatSyntaxe.syntaxOK
        }
        if (valide) {
          validation(this, false)
        } else {
          bilanreponse = 'incorrect'
        }
      } else {
        if (sq.nbexp === 0) bilanreponse = 'incorrect'
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (res1 === 1) {
          bilanreponse = 'exact'
        } else {
          if (res1 === 2) {
            bilanreponse = 'exactpasfini'
          } else {
            if (res1 === 3) {
              bilanreponse = 'erreursyntaxe'
            } else { bilanreponse = 'erreur' }
          }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          marqueEditeurPourErreur()
          focusIfExists(inputId)
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanreponse === 'exact') {
            this._stopTimer()
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) {
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
            }
            afficheSolution(true)
            $('#editeur').css('display', 'none')
            $('#boutonsmathquill').css('display', 'none')
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            sq.numEssai++
            if (bilanreponse === 'exactpasfini') {
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              if (bilanreponse === 'erreursyntaxe') j3pElement('correction').innerHTML = 'Calcul vectoriel invalide'
              else j3pElement('correction').innerHTML = cFaux
            }
            j3pElement('info').innerHTML = ''
            afficheInfo()
            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider

            if ((sq.nbexp < sq.nbEssais) && (sq.validationAuto
              ? true
              : (sq.numEssai <= sq.nbchances))) {
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              focusIfExists(inputId)
              // indication éventuelle ici
              // parcours.Sectionex_Calc_Vect.nbExp += 1;
            } else {
              // Erreur au nème essai
              this._stopTimer()
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) {
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              }
              afficheSolution(false)
              $('#editeur').css('display', 'none')
              $('#boutonsmathquill').css('display', 'none')

              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    } // case 'correction'

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
