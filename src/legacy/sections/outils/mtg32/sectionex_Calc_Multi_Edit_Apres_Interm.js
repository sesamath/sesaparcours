import $ from 'jquery'
import { j3pAddContent, j3pAjouteBouton, j3pDetruit, j3pElement, j3pEmpty, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { unLatexify } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'
import { addElement, focusIfExists } from 'src/lib/utils/dom/main'

const { essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    2019
    squelettemtg32_Calc_Param_Multi_Edit_Apres_Interm
    Squelette demandant de calculer une expression ou plusieurs expressions
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
    La figure doit contenir un affichage LaTeX de tag 'enonce' de une ou plusieurs lignes de \text{} contenant la consigne.
    La figure doit  contenir un affichage LaTeX de tag 'solution' de une ou plusieurs lignes de \text{} contenant
    une conclusion à afficher au-dessus de la figure (éventuelle).
    La figure doit contenir un affichage Latex de tag consigneSuite chargé de donner la consigne à afficher une fois que les calculs intermédiaires
    sont finis. Par exepmple avec u n code LaTeX commecemui-ci : \text{On demande maintenant de calculer la longueur AC}
    La figure doit contenir un affichage Latex de tag formulaire2 servant à donner ce qui doit être calculé dans la question finale
    ayant pour code par exemple \text{AC= edit1} dans le cas d’un seul éditeur.

    Il contient aussi une variable formulaire qui est une chaîne de caractères contenant éventuellement du LaTeX et contenant
    une référence aux éditeurs MathQuill utilisés. Par exemple, si on veut deux éditeurs MathQuill, la chaine devra contenir deux sous-chaînes
    "edit1" et "edit2" qui correspondront aux éditeurs MathQuill.
    Quand on vérifiera la validité de la réponse de l’élève, on mettra par exemple, la réponse
    contenue dans l’éditeur 1 dans le calcul rep1 et, après recalcul de la figure mtg32, le calcul resolu1 contiendra 1
    si la réponse contenue dans le premier éditeur est acceptée comme finale,  0 sinon.
    Le calcul exact1 contiendra 1 si la réponse du premier éditeur est bonne mais pas forcément acceptée comme finale.
    Idem pour un deuxième éditeur avec resolu2 et exact2 etc ...
    Si la paramètre simplifier est à false, un calcul exact mais pas considéré sinon comme final sera accepté comme final.
    Si la figure contenue  dans ex contient un paramètre nbVar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Le paramètre nbLatex contient le nombre d’affichages LaTeX qui doivent être récupérés de la figure pour affichage dans l’énoncé.
    La figure doit contenir : un calcul nommé interm qui contient par exemple comme formule 0.
    Lorsqu’on teste les égalités intermédiaires successives une par une, on envoie chaque formule d’égalité dans interm.
    La figure doit contenir une fontion réelle d’une variable par exemple nommée zero et contenant comme formule abs(x)<0.000000001
    La figure doit alors contenir un calcul nommé vrai avec comme formulezero(gauche(interm)-droit(interm))
    Ainsi, après recalcul de la figure, vrai renverra 1 si, à epsioln près, l’égalité entrée dans le calcul interm était vraie et 0 sinon.
    La figure doit contenir : un LaTeX servant à l’évaluation des calculs intermédiaires et contenant comme code LaTeX \For{interm} doit être présent.
    Il doit être le dernier affichage LaTeX de la figure à moins qu’il ait pour tag 'interm'
*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
MathfieldElement.soundsDirectory = '/static/mathlive/sounds'

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['titre', '', 'string', 'Titre de l’activité'],
    ['solution', true, 'boolean', 'false pour que la solution ne soit pas affichée en fin d’exercice'],
    ['calculatrice', false, 'boolean', 'true pour avoir une calculatrice disponible'],
    ['nbEssais1', 6, 'entier', 'Nombre maximum d’égalités intermédiaires autorisées avant de conclure'],
    ['nbEssais2', 2, 'entier', 'Nombre maximum d’essais  autorisés pour le calcul final'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée pour le deuxième calcul, false sinon'],
    ['fig', '', 'editor', 'Chaîne Base 64 contenant la figure de l’exercice', initEditor],
    ['width', 800, 'entier', 'Largeur de la figure initiale'],
    ['height', 520, 'entier', 'Hauteur de la figure initiale'],
    ['param', 'abcd', 'string', 'Chaîne formé des paramètres que l’exercice utilise (de a à r, voir ci-dessous'],
    ['charset1', '', 'string', 'Chaîne formée des caractères permis dans l’éditeur des calculs intermédiaires, chaîne vide pour autoriser tous les caractères'],
    ['charset2', '', 'string', 'Chaîne formée des caractères permis dans l’éditeur des calculs finaux, chaîne vide pour autoriser tous les caractères'],
    ['consigneCalculsIntermediaires', 'On peut entrer ci-dessous des égalités intermédiaires avant de donner la réponse finale (séparer les égalités par des ; s’il y en a plusieurs).<br>Il faut cliquer sur le bouton <b>Passer à la réponse finale </b> pour donner la réponse finale à la question.', 'string', 'Consigne de fin pour les calculs intermédiaires'],
    ['enonceSimplifier', '', 'string', 'Consigne supplémentaire éventuelle à afficher si la réponse doit être donnée simplifiée'],
    ['enonceFin', 'Appuyer sur la touche Entrée ou cliquer sur le bouton OK pour valider les étapes du calcul.', 'string', 'Fin de la consigne'],
    ['formulaire1', 'Egalités intermédiaires : edit1', 'string', 'Editeur pour les calculs intermédiaires. Doit contenir la chaîne edit1'],
    ['btnPuis', true, 'boolean', 'Bouton puissance'],
    ['btnFrac', true, 'boolean', 'Bouton fraction'],
    ['btnPi', false, 'boolean', 'Bouton pi'],
    ['btnCos', false, 'boolean', 'Bouton cosinus'],
    ['btnSin', false, 'boolean', 'Bouton sinus'],
    ['btnTan', false, 'boolean', 'Bouton tangente'],
    ['btnRac', true, 'boolean', 'Bouton Racine carrée'],
    ['btnExp', false, 'boolean', 'Bouton exponentielle'],
    ['btnLn', false, 'boolean', 'Bouton logarithme népérien'],
    ['btnLog', false, 'boolean', 'Bouton logarithme décimal'],
    ['btnAbs', false, 'boolean', 'Bouton valeurabsolue'],
    ['btnConj', false, 'boolean', 'Bouton conjugué'],
    ['figSol', '', 'editor', 'Code Base64 d’une figure supplémentaire pour la correction, vide pour aucune figure supplémentaire', initEditor],
    ['widthSol', 600, 'entier', 'Largeur de la figure supplémentaire de solution'],
    ['heightSol', 500, 'entier', 'Hauteur de la figure supplémentaire de solution'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r']
  ]

}

const textes = {
  ilReste: 'Il reste ',
  unEssai: 'un essai.',
  essais: 'essais.',
  uneEgalite: 'une égalité intermédiaire autorisée.',
  egalites: 'égalités intermédiaires autorisées.',
  bonPasFini: 'Le calcul est bon mais pas écrit sous la forme demandée.',
  bienPasSimplifie: 'C’est bien mais on pouvait simplifier la réponse',
  egalitesNecess: 'Il faut écrire une ou des égalités <br>séparées par des ;',
  solutionIci: '\nLa solution se trouve ci-contre.',
  solDesact: '\nL’affichage de la solution a été désactivé par le professeur.',
  tropDeInterm: 'Trop d’égalités intermédiaires',
  repIncorrecte: 'Réponse incorrecte',
  cbien: 'C’est bien !',
  cfaux: 'C’est faux !',
  ilManque: 'Il manque : ',
  unAffLat: 'un affichage LaTeX de tag ',
  avert1: 'L’affichage LaTeX de tag ',
  avert2: ' doit contenir au moins un code LaTeX \\text{}.',
  aide1: 'C’est le contenu du ou des \\text{} qui fourni le contenu à afficher.',
  avert3: ' doit contenir au moins un éditeur repéré par edit, edittext ou list suivi du numéro de l’éditeur' +
      '\n ou un ou plusieurs \\editable{}',
  unCalcNom: 'un calcul (ou une fonction) nommé ',
  et: 'et',
  ou: 'ou'
}

const aide = 'La figure doit contenir des affichages Latex de tag enonce, enonceSuite, formulaire2 et éventuellement un affichage LaTeX de tag solution'

const inputId = 'expressioninputmq1'

// cf src/legacy/sections/outils/mtg32/sectionex_Calc_Multi_Edit_Multi_Etapes.js pour ajouter un validator
/**
 * Ajoute l’éditeur mathgraph dans container, la promesse sera résolue avec la figure
 * @param {HTMLElement} container
 * @param {string} fig
 * @return {Promise<string>}
 */
async function initEditor (container, fig) {
  const { default: editor } = await import('./mtgEditor.js')
  return editor(container, { fig, validator, aide })
}

/**
 * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
 * en une chaîne affichable par MathQuill
 * @param {string} tagLatex le tag de l’affichage LaTeX de la figure
 * @param {CListeObjets} list la liste qui contient cet affichage LaTeX
 * @return {string}
 */
function extraitDeLatexPourMathlive (list, tagLatex) {
  const latex = list.getByTag(tagLatex, true)
  if (latex === null || latex.className !== 'CLatex') return ''
  return cleanLatexForMl(latex.chaineLatex)
}

function getTabErrors (list) {
  const errors = []
  const tabch = ['enonce', 'formulaire2', 'enonceSuite']
  let ch = ''
  for (const nom of tabch) {
    const latex = list.getByTag(nom, true)
    if (latex === null || latex.className !== 'CLatex') {
      ch += '\n - ' + textes.unAffLat + nom
    }
  }
  const latexInterm = list.getByTag('interm', true)
  if (latexInterm === null || latexInterm.className !== 'CLatex') {
    ch += '\n - ' + textes.unAffLat + 'interm'
  }
  if (ch !== '') {
    errors.push(textes.ilManque + ch)
    return errors
  }
  ch = ''
  for (const nom of tabch) {
    const chaineLatex = list.getByTag(nom, true).chaineLatex
    if (!chaineLatex.includes('\\text{')) {
      if (ch !== '') ch += '\n'
      ch += textes.avert1 + nom + textes.avert2
    }
  }
  if (ch !== '') {
    errors.push(ch + '\n' + textes.aide1)
  }
  const latex = extraitDeLatexPourMathlive(list, 'formulaire2')
  if (!/edit[123456789]+/g.test(latex)) {
    errors.push(textes.avert1 + 'formulaire2' + textes.avert3)
  }
  // On regarde si les calculs devant être associés aux éditeurs sont bien présents.
  let j = 1
  let st = ''
  while (new RegExp('edit' + j + '[\\D]*', 'g').test(latex)) {
    const nomCalcul1 = 'resolu' + String(j)
    if (!list.containsCalc(nomCalcul1, true)) {
      st += '\n - ' + textes.unCalcNom + nomCalcul1
    }
    j++
  }
  if (st !== '') {
    errors.push(textes.ilManque + st)
  }
  return errors
}

/**
 * Valide la figure (avant de la sauvegarder dans les paramètres)
 * S’il y a des errors dans ce que l’on retourne, ce sera affichés et bloquera la sauvegarde (l’éditeur reste ouvert),
 * s’il y a des warning ils sont affichés sans bloquer la sauvegarde
 * @param {MtgApp} mtgApp
 * @return {Promise<{warnings: string[], errors: string[]}>}
 */
async function validator (mtgApp) {
  const errors = getTabErrors(mtgApp.getList())
  return { errors, warnings: [] }
}

/**
 * Les anciennes versions permettaient de mettre l’énoncé (enonceLigneN),
 * la suite de l’énoncé (consigneSuite) et formulaire2 / nbLatex / nbLatexSol / explicationSol
 * Ils ont maintenant fournis par la figure via des affichages LaTeX
 * Pour garder la compatibilité avec les anciennes versions on initie ici sq.enonce, sq.enonceSuite et sq.formulaire2
 * Attention, avec l'éditeur de graphe v1 il n'y a pas de vérification de la figure, et comme
 * les params enonceLigneN on disparu des paramètres de la section, éditer une ressource va les
 * virer donc si la figure n'est pas remplacée par une autre ayant des affichages LaTeX ça va planter !
 */
export function upgradeParametres (params) {
  // Attention, l'éditeur v1 nous appelle (pour convertir des params) mais
  // il ne nous fourni pas de this (y'a pas d'instance de Parcours)
  if (!this?.storage) return
  const sq = this.storage
  sq.enonce = ''
  if (params.enonceLigne1) {
    for (let i = 1; i < 5; i++) {
      if (params['enonceLigne' + i]) {
        if (i !== 1) sq.enonce += '<br>'
        sq.enonce += params['enonceLigne' + i]
      }
    }
  }
  sq.formulaire2 = params.formulaire2 ? params.formulaire2 : ''
  sq.enonceSuite = ''
  if (params.consigneSuite1) {
    for (let i = 1; i < 5; i++) {
      if (sq['consigneSuite' + i] !== '') {
        if (i !== 1) sq.enonceSuite += '<br>'
        sq.enonceSuite += params['consigneSuite' + i]
      }
    }
  }
  sq.nbLatex = params.nbLatex ? params.nbLatex : 0
  sq.explicationSol = params.explicationSol ? params.explicationSol : ''
  sq.nbLatexSol = params.nbLatexSol ? params.nbLatexSol : 0
}

/**
 * section ex_Calc_Multi_Edit_Apres_Interm
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  // les éléments qu'on va créer et réutiliser ensuite
  if (!sq.elts) sq.elts = {}
  const elts = sq.elts

  function onkeyup () {
    const indice = this.indice
    if (sq.marked[indice - 1]) {
      demarqueEditeurPourErreur(indice)
      j3pEmpty(elts.correction)
    }
  }

  /**
   * Fonction affichant un message d’avertissement au démarrage de la figure s’il manque
   * des éléments pour que la figure soit un exercice multi calculs multi étapes
   */
  function alertIfErrors () {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    if ((sq.enonce === '')) {
      const errors = getTabErrors(list)
      if (errors.length > 0) {
        console.error(`Problème${errors.length > 1 ? 's' : ''} dans la figure :`, errors.join('\n'))
      }
    }
  }

  /**
   * Fonction renvoyant le nombre d’éditeurs contenus dans la chaîne ch et dont le type est contenu
   * dans la chaîne type
   * @param {string} type chaine de caractères qui peut être la chaîne 'edit' pour un éditeur MathQuill,
   * 'list' pour une liste déroulante et 'string' pour une chaîne de caractères.
   * Si, par exemple, il y a trois éditeurs MathQuill la chaîne doit contenir edit1, edit2 et edit3.
   * @param ch
   */
  function nbEdit (type, ch) {
    let res = 0
    let i = 1
    while (ch.indexOf(type + i) !== -1) {
      res++
      i++
    }
    return res
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty(elts.info)
    const nbe = sq['nbEssais' + sq.etape] - sq.numEssai + 1
    const chinterm1 = sq.etape === 2 ? textes.unEssai : textes.uneEgalite
    const chinterm2 = sq.etape === 2 ? textes.essais : textes.egalites
    const ch = (nbe === 1)
      ? textes.ilReste + chinterm1
      : textes.ilReste + nbe + ' ' + chinterm2
    afficheMathliveDans(elts.info, '', ch, { style: { color: '#7F007F' } })
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function marqueEditeurPourErreur (indice) {
    sq.marked[indice - 1] = true
    $('#expressioninputmq' + indice).css('background-color', '#FF9999')
  }
  function demarqueEditeurPourErreur (indice) {
    if (!sq.marked) return
    sq.marked[indice - 1] = false // Par précaution si appel via bouton recopier réponse trop tôt
    $('#expressioninputmq' + indice).css('background-color', '#FFFFFF')
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  function validationEditeurInterm () {
    let res = true
    const rep = getMathliveValue(inputId)
    sq.rep = rep
    sq.signeEgalManquant = false
    const tab = rep.split(';')
    if (tab.length + sq.nbCalcInterm > sq.nbEssais1) {
      sq.tropDeCalculsInterm = true
      res = false
    } else {
      for (let j = 0; j < tab.length; j++) {
        const ch = tab[j]
        let nbegal = 0
        for (let k = 0; k < ch.length; k++) {
          if (ch.charAt(k) === '=') nbegal++
        }
        if (nbegal !== 1) {
          res = false
          if (nbegal === 0) sq.signeEgalManquant = true
          break
        } else {
          const chcalcul = traiteMathlive(tab[j])
          const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'interm', chcalcul, false)
          if (!valide) {
            res = false
          }
        }
      }
      sq.tropDeCalculsInterm = false
    }
    if (!res) {
      marqueEditeurPourErreur(1)
      focusIfExists(inputId)
    } else { sq.nbCalcInterm += tab.length }

    return res
  }

  function validationEditeurs () {
    sq.signeEgalManquant = false
    let res = true
    for (let ind = 1; ind <= sq['nbCalc' + sq.etape]; ind++) {
      const rep = getMathliveValue('expressioninputmq' + ind)
      if (rep === '') {
        res = res && false
        marqueEditeurPourErreur(ind)
      } else {
        const chcalcul = traiteMathlive(rep)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep' + ind, chcalcul, true)
        if (!valide) {
          res = res && false
          marqueEditeurPourErreur(ind)
          focusIfExists(inputId)
        }
      }
    }
    return res
  }

  function montreEditeurs (bVisible) {
    if (bVisible) elts.info.style.display = 'block'
    elts.editeur.style.display = bVisible ? 'block' : 'none'
    if (sq.boutonsMathQuill) elts.btnsMq.style.display = bVisible ? 'block' : 'none'
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    if (bVisible) {
      j3pElement(inputId).value = ''
      focusIfExists(inputId)
    }
  }

  function videEditeurs () {
    for (let i = 1; i <= sq['nbCalc' + sq.etape]; i++) {
      j3pElement('expressioninputmq' + i).value = ''
    }
    // On donne le focus au premier éditeur MathQuill
    focusIfExists(inputId)
  }

  function creeEditeurs () {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    if (sq.etape === 2) {
      const ch = extraitDeLatexPourMathlive(list, 'formulaire2')
      if (!ch) {
        // celui-là est obligatoire dans la figure, sinon y'a pas d'input
        throw Error('Figure passée en paramètres invalide :\n' + textes.avert1 + '"formulaire2"' + textes.avert3)
      }
      sq.formulaire2 = ch
      sq.nbCalc2 = nbEdit('edit', sq.formulaire2)
    }
    let formulaire = sq['formulaire' + sq.etape]
    let i, k
    const t = 'abcdefghijklmnopq'
    for (k = 1; k <= sq['nbCalc' + sq.etape]; k++) formulaire = formulaire.replace('edit' + k, '&' + k + '&')
    const obj = {}
    for (k = 1; k <= sq['nbCalc' + sq.etape]; k++) obj['inputmq' + k] = {}
    const nbParam = parseInt(sq.nbLatex)
    for (i = 0; i < nbParam; i++) {
      obj[t.charAt(i)] = sq.parametres[t.charAt(i)]
    }
    obj.transposeMat = true
    obj.charset = sq['charset' + sq.etape]
    obj.listeBoutons = sq.listeBoutons
    // c'est ça qui va générer les expressioninputmqN
    afficheMathliveDans(elts.editeur, 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
    // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
    for (k = 1; k <= sq['nbCalc' + sq.etape]; k++) {
      j3pElement('expressioninputmq' + k).indice = k
      if (sq['charset' + sq.etape] !== '') {
        mathliveRestrict('expressioninputmq' + k, sq['charset' + sq.etape])
      }
      $('#expressioninputmq' + k).focusin(function () {
        ecoute.call(this)
      })
      j3pElement('expressioninputmq' + k).onkeyup = function () {
        onkeyup.call(this)
      }
    }
  }

  function afficheReponse (bilan, depasse) {
    let ch, color, i
    const num = sq.numEssai
    if (sq.etape !== 1) {
      if (bilan === 'exact') {
        ch = 'Réponse exacte : '
        color = parcours.styles.cbien
      } else {
        if (bilan === 'faux') {
          ch = 'Réponse fausse : '
          color = parcours.styles.cfaux
        } else { // Exact mais pas fini
          ch = 'Exact pas fini : '
          if (depasse) color = parcours.styles.cfaux
          else color = parcours.styles.colorCorrection
        }
      }
      if (num > 2) ch = '<br>' + ch
      const div = sq.etape === 1 ? elts.formules : elts.formulesSuite
      afficheMathliveDans(div, '', ch, { style: { color } })
    }

    if (sq.etape === 1) {
      ch = ''
      for (i = 0; i < sq.rep.length; i++) {
        ch = ch + '$\\textcolor{' + (sq.repExact[i] ? '#0000FF' : parcours.styles.cfaux) + '}{' +
        sq.rep[i] + '}$'
        if (i !== sq.rep.length - 1) ch += '; '
      }
      afficheMathliveDans(elts.formules, '',
        elts.formules.innerHTML === '' ? ch : '<br>' + ch, sq.parametres)
    } else {
      ch = sq['formulaire' + sq.etape]
      for (i = 1; i <= sq['nbCalc' + sq.etape]; i++) {
        const st = '\\textcolor{' + (sq.repResolu[i - 1] ? parcours.styles.cbien : (sq.repExact[i - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
        ch = ch.replace('edit' + i, '$' + st + sq.rep[i - 1] + '}$')
      }
      afficheMathliveDans(elts.formulesSuite, '', ch, sq.parametres)
    }
    resetKeyboardPosition()
  }

  function ecoute () {
    if (sq.boutonsMathQuill) {
      j3pEmpty(elts.btnsMq)
      const indice = this.indice
      j3pPaletteMathquill(elts.btnsMq, 'expressioninputmq' + indice, { liste: sq.listeBoutons, style: { display: 'inline-block' } })
    }
  }

  function validation () {
    let rep, j, chcalcul, exact, ch, resolu
    sq.rep = []
    sq.repExact = []
    sq.repResolu = []
    if (sq.etape === 1) {
      rep = getMathliveValue(inputId)
      sq.repInterm = rep
      const tab = rep.split(';')
      sq.correct = true
      for (j = 0; j < tab.length; j++) {
        chcalcul = traiteMathlive(tab[j])
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'interm', chcalcul)
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        exact = sq.mtgAppLecteur.valueOf('mtg32svg', 'vrai', true) === 1
        // On récupère l’affichage LaTeX de la formule intermédiaire qui doit être le dernier de la figure
        // ou alors doit avoir pour tag 'interm'
        const list = sq.mtgAppLecteur.getList('mtg32svg')
        const latex = list.getByTag('interm', true)
        if (latex !== null && latex.className === 'CLatex') {
          ch = latex.chaineLatex
        } else {
          ch = sq.mtgAppLecteur.getLatexCode('mtg32svg', -1)
        }
        if (!exact) ch = ch.replace(/=/g, '\\ne')
        sq.rep.push(ch)
        sq.repExact.push(exact)
      }
      sq.numEssai += tab.length
      if (sq.numEssai < sq.nbEssais1) elts.btnRecopier.style.display = 'block'
    } else {
      if (sq.numEssai >= 1) {
        elts.btnRecopier.style.display = 'block'
      }

      for (let ind = 1; ind <= sq['nbCalc' + sq.etape]; ind++) {
        rep = getMathliveValue('expressioninputmq' + ind)
        sq.rep.push(rep)
        chcalcul = traiteMathlive(rep)
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep' + ind, chcalcul)
      }
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      exact = true
      resolu = true
      let auMoinsUnExact = false
      for (let ind = 1; ind <= sq.nbCalc2; ind++) {
        const res = sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu' + ind, true)
        const ex = sq.mtgAppLecteur.valueOf('mtg32svg', 'exact' + ind, true)
        resolu = resolu && (res === 1)
        exact = exact && ((res === 1) || (ex === 1))
        sq.repResolu.push(res === 1)
        sq.repExact.push((res === 1) || (ex === 1))
        auMoinsUnExact = auMoinsUnExact || (ex === 1)
      }
      sq.resolu = resolu
      sq.exact = exact && auMoinsUnExact
    }
  }

  function recopierReponse () {
    for (let ind = sq['nbCalc' + sq.etape]; ind > 0; ind--) {
      demarqueEditeurPourErreur(ind)
      j3pElement('expressioninputmq' + ind).value = sq.etape === 1 ? sq.repInterm : sq.rep[ind - 1]
      if (ind === 1) focusIfExists(inputId)
    }
  }

  function etapeSuivante () {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    sq.etape = 2
    j3pEmpty(elts.editeur)
    j3pEmpty(elts.consigneCalculsIntermediaires)
    j3pEmpty(elts.info)
    elts.btnEtape2.style.display = 'none'
    elts.btnRecopier.style.display = 'none'
    // sq.parametres.e = sq['nbEssais' + sq.etape]
    const ch = extraitDeLatexPourMathlive(list, 'enonceSuite')
    if (!ch) {
      console.error('Aucun affichage Latex "enonceSuite" dans la figure passée en paramètres')
    }
    sq.enonceSuite = ch
    afficheMathliveDans(elts.enonceSuite, 'texte', sq.enonceSuite, sq.parametres)
    sq.numEssai = 1
    afficheNombreEssaisRestants()
    creeEditeurs()
    montreEditeurs(true)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    if (sq.solution) {
      sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
      const list = sq.mtgAppLecteur.getList('mtg32svg')
      const ch = extraitDeLatexPourMathlive(list, 'solution')
      if (ch) {
        j3pEmpty(elts.divSolution)
        elts.divSolution.style.display = 'block'
        const styles = parcours.styles
        afficheMathliveDans(elts.divSolution, 'solution', ch, { style: { color: bexact ? styles.cbien : styles.colorCorrection } })
      } else {
        console.error('Aucun affichage Latex "solution" dans la figure passée en paramètres => aucune solution ne sera affichée')
      }
    }
  }

  function afficheFiguresolution () {
    if (sq.solution && sq.figSol !== '') {
      let j, car, k, code
      const mtg32App = sq.mtgAppLecteur
      j3pEmpty(elts.divExplications)
      elts.divExplications.style.display = 'block'
      elts.mtg32svgsol.style.display = 'block'
      mtg32App.removeDoc('mtg32svgsol')
      mtg32App.addDoc('mtg32svgsol', sq.figSol)
      // On remplace dans la figure de solution les paramètres par les valeurs correspondant dans la figure de texte
      const ch = 'abcdefghjklmnpqr'
      for (j = 0; j < ch.length; j++) {
        car = ch.charAt(j)
        if (sq.param.indexOf(car) !== -1) {
          const par = mtg32App.valueOf('mtg32svg', car)
          mtg32App.giveFormula2('mtg32svgsol', car, par)
        }
      }
      mtg32App.calculate('mtg32svgsol', false)
      const param = {}
      for (k = 0; k < sq.nbLatexSol; k++) {
        code = mtg32App.getLatexCode('mtg32svgsol', k)
        param[ch.charAt(k)] = code
      }
      const list = sq.mtgAppLecteur.getList('mtg32svgsol')
      const st = extraitDeLatexPourMathlive(list, 'explicationSol')
      if (st !== '') sq.explicationSol = st
      afficheMathliveDans(elts.divExplications, 'solution', sq.explicationSol, param)
      mtg32App.display('mtg32svgsol')
    }
  }

  function cacheFigureSolution () {
    if (sq.figSol !== '') {
      j3pEmpty(elts.divExplications)
      elts.divExplications.style.display = 'none'
      elts.mtg32svgsol.style.display = 'none'
    }
  }

  function initMtg () {
    setMathliveCss('MepMG')
    sq.nbEssais1 = parseInt(parcours.donneesSection.nbEssais1)
    sq.nbEssais2 = parseInt(parcours.donneesSection.nbEssais2)
    sq.simplifier = parcours.donneesSection.simplifier
    sq.nbexp = 0
    sq.reponse = -1
    if (sq.simplifier === undefined) sq.simplifier = true
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    // on charge mathgraph
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, par, car, i, j, k, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      alertIfErrors()
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar', true)
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i, true)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else { sq.aleat = false }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }

      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.nbLatex)
      const t = 'abcdefghijklmnopq'
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[t.charAt(i)] = code
      }
      sq.parametres = param // Mémorisation pour l’étape 2
      const list = sq.mtgAppLecteur.getList('mtg32svg')
      if (list.getByTag('enonce', true)) {
        sq.enonce = extraitDeLatexPourMathlive(list, 'enonce')
      }
      if (parcours.donneesSection.simplifier && parcours.donneesSection.enonceSimplifier !== '') sq.enonce += '<br>' + parcours.donneesSection.enonceSimplifier
      sq.enonce += '<br>' + parcours.donneesSection.enonceFin
      afficheMathliveDans(elts.enonce, 'texte', sq.enonce, param)
      afficheMathliveDans(elts.consigneCalculsIntermediaires, 'texteCalculsInterm', sq.consigneCalculsIntermediaires, param)
      creeEditeurs()
      montreEditeurs(true)
      if (sq.boutonsMathQuill) {
        j3pPaletteMathquill(elts.btnsMq, inputId, { liste: sq.listeBoutons, style: { display: 'inline-block' } })
      } else {
        j3pDetruit(elts.btnsMq)
      }
      elts.btnEtape2.style.display = 'block'
      afficheNombreEssaisRestants()
      focusIfExists(inputId)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)
    elts.conteneur = addElement(parcours.zonesElts.MG, 'div', { style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
    elts.enonce = addElement(elts.conteneur, 'div')
    elts.consigneCalculsIntermediaires = addElement(elts.conteneur, 'div')
    // j3pAddContent(enonce, 'Chargement en cours…', { replace: true }) // on vide et remplace
    // Contient le formules entrées par l’élève
    elts.formules = addElement(elts.conteneur, 'div', { style: parcours.styles.petit.enonce })
    if (sq.bigSize) {
      elts.formules.style.fontSize = '26px'
    }
    elts.enonceSuite = addElement(elts.conteneur, 'div')
    // Contient les formules entrées par l’élève
    elts.formulesSuite = addElement(elts.conteneur, 'div', { style: parcours.styles.petit.enonce })
    elts.info = addElement(elts.conteneur, 'div')
    const ctButtons = addElement(elts.conteneur, 'div')
    // j3pAjouteBouton (container, id, className, value, onClick)
    elts.btnRecopier = j3pAjouteBouton(ctButtons, recopierReponse, { className: 'MepBoutonsRepere', value: 'Recopier réponse précédente', style: { display: 'none' } })
    // Le div qui contiendra la chaine avec le ou les éditeurs MathLive
    // (on laisse un peu de place au-dessus)
    elts.editeur = addElement(elts.conteneur, 'div', { style: { paddingTop: '10px' } })
    if (sq.bigSize) elts.editeur.style.fontSize = '30px'
    elts.btnsMq = addElement(elts.conteneur, 'div', { style: { display: 'inline-block', paddingTop: '10px' } })
    const ctBtnEtape2 = addElement(elts.conteneur, 'div')
    // On n’affiche le bouton de passage à l’étape 2 que quand la figure est prête donc dans initMtg
    elts.btnEtape2 = j3pAjouteBouton(ctBtnEtape2, etapeSuivante, { className: 'MepBoutonsRepere', value: 'Passer à la réponse finale', style: { display: 'none', marginTop: '5px' } })
    // On crée la liste des boutons disponibles
    sq.listeBoutons = []
    // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
    const tabBoutons = ['puissance', 'fraction', 'pi', 'racine', 'exp', 'ln', 'log', 'sin', 'cos', 'tan', 'abs', 'conj']
    ;['btnPuis', 'btnFrac', 'btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnLog', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj'].forEach((p, i) => {
      if (sq[p]) sq.listeBoutons.push(tabBoutons[i])
    })
    sq.boutonsMathQuill = sq.listeBoutons.length !== 0
    // Un div pour contenir la solution affichée par MathQuill si la figure contient un LaTeX de tag 'solution'
    elts.divSolution = addElement(elts.conteneur, 'div', { style: { display: 'none' } })
    const divmtg32 = addElement(elts.conteneur, 'div')
    j3pCreeSVG(divmtg32, {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    if (sq.figSol !== '') {
      elts.divExplications = addElement(elts.conteneur, 'div', { style: { display: 'none' } })
      const divFigSol = addElement(elts.conteneur, 'div')
      elts.mtg32svgsol = j3pCreeSVG(divFigSol, {
        // lui a besoin d'un id car ce sera un doc mtg
        id: 'mtg32svgsol',
        width: sq.widthSol,
        height: sq.heightSol,
        style: { display: 'none' }
      })
    }
    elts.correction = addElement(parcours.zonesElts.MD, 'div', {
      style: {
        ...parcours.styles.petit.correction,
        marginTop: '1.5em',
        marginLeft: '0.5em'
      }
    })
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        // compteur.
        sq.numEssai = 1
        sq.etape = 1
        sq.nbCalcInterm = 0
        if (parcours.donneesSection.calculatrice) {
          const divCalc = addElement(this.zonesElts.MD, 'div')
          this.fenetresjq = [{
            name: 'Calculatrice',
            title: 'Une calculatrice',
            left: 600,
            top: 10
          }]
          j3pCreeFenetres(this)
          j3pAjouteBouton(divCalc, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
        }
        // console.log("initsection");
        // Attention, le this devient window...
        sq.consigneCalculsIntermediaires = parcours.donneesSection.consigneCalculsIntermediaires
        sq.fig = parcours.donneesSection.fig
        sq.width = parcours.donneesSection.width
        sq.height = parcours.donneesSection.height
        sq.solution = parcours.donneesSection.solution

        sq.param = parcours.donneesSection.param // Chaine contenant les paramètres autorisés
        const tabBtn = ['Puis', 'Frac', 'Pi', 'Cos', 'Sin', 'Tan', 'Rac', 'Exp', 'Ln', 'Log', 'Abs']
        tabBtn.forEach((p) => {
          sq['btn' + p] = parcours.donneesSection['btn' + p]
        })
        sq.titre = parcours.donneesSection.titre
        sq.bigSize = parcours.donneesSection.bigSize
        sq.nbCalc1 = 1
        sq.formulaire1 = parcours.donneesSection.formulaire1 // Contient la chaine avec les éditeurs.
        sq.param = parcours.donneesSection.param // Chaine contenant les paramètres autorisés
        sq.charset1 = parcours.donneesSection.charset1
        if (sq.charset1.indexOf('=') === -1) sq.charset1 += '='
        if (sq.charset1.indexOf(';') === -1) sq.charset1 += ';'
        sq.charset2 = parcours.donneesSection.charset2
        sq.figSol = parcours.donneesSection.figSol
        sq.widthSol = parcours.donneesSection.widthSol
        sq.heightSol = parcours.donneesSection.heightSol
        if (sq.fig === '') {
          sq.nbLatex = 0
          sq.btnRac = true
          sq.charset1 = '0123456789;,.+-*²/=AHBC()'
          sq.charset2 = '0123456789,.+-*²/()'
          sq.fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAUeAAACygAAAQEAAAAAAAAAAQAAAA3#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAx3vnbItDmQDHe+dsi0Ob#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUBB3vnbItDmAAAAAv####8AAAABAAhDU2VnbWVudAD#####AAAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8AAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAAD#####wAAAAIABkNMYXRleAD#####AAAAAAEABmVub25jZf####8QQEqAAAAAAABAU#XCj1wo9gAAAAAAAAAAAAAAAAABAAAAAAAAAAAADVx0ZXh0e0Vub25jZX0AAAAJAP####8AAAAAAQALZW5vbmNlU3VpdGX#####EEBJgAAAAAAAQF31wo9cKPYAAAAAAAAAAAAAAAAAAQAAAAAAAAAAABJcdGV4dHtFbm9uY2VTdWl0ZX0AAAAJAP####8AAAAAAQALZm9ybXVsYWlyZTL#####EEBLgAAAAAAAQGTa4UeuFHsAAAAAAAAAAAAAAAAAAQAAAAAAAAAAABFcdGV4dHtBQyA9IGVkaXQxfQAAAAkA#####wAAAAABAAZpbnRlcm3#####EEBOAAAAAAAAQGw64UeuFHsAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAZpbnRlcm3#####AAAAAQAHQ0NhbGN1bAD#####AAdyZXNvbHUxAAExAAAAAT#wAAAAAAAAAAAAB###########'
          sq.titre = 'Ressource à personnaliser'
          j3pShowError('Cette ressource doit être personnalisée en éditant la ressource dans l’éditeur de graphes.')
        }
        sq.marked = []
        for (let k = 0; k < sq.nbCalc; k++) {
          sq.marked[k] = false
        }
        initMtg()
        // return;
      } else {
        sq.numEssai = 1
        cacheFigureSolution()
        sq.etape = 1
        sq.nbCalcInterm = 0
        videEditeurs()
        elts.btnEtape2.style.display = 'block'
        montreEditeurs(true)
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs aléatoires soient réinitialisés
        j3pEmpty(elts.correction)
        j3pEmpty(elts.divSolution)
        j3pEmpty(elts.enonce)
        j3pEmpty(elts.consigneCalculsIntermediaires)
        j3pEmpty(elts.formules)
        j3pEmpty(elts.info)
        j3pEmpty(elts.enonceSuite)
        j3pEmpty(elts.formulesSuite)

        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nbLatex)
        const t = 'abcdefghijklmnopq'
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          param[t.charAt(i)] = code
        }
        sq.parametres = param
        const list = sq.mtgAppLecteur.getList('mtg32svg')
        if (list.getByTag('enonce', true)) {
          sq.enonce = extraitDeLatexPourMathlive(list, 'enonce')
        }
        if (parcours.donneesSection.simplifier && parcours.donneesSection.enonceSimplifier !== '') sq.enonce += '<br>' + parcours.donneesSection.enonceSimplifier
        afficheMathliveDans(elts.enonce, 'texte', sq.enonce, param)
        afficheMathliveDans(elts.consigneCalculsIntermediaires, 'texteCalculsInterm', sq.consigneCalculsIntermediaires, param)
        afficheNombreEssaisRestants()
        elts.info.style.display = 'block'

        j3pEmpty(elts.editeur)
        creeEditeurs()
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          this.zonesElts.MG.scrollIntoView()
        } catch (e) {
          console.warn(e) // pas normal mais pas grave
        }
        focusIfExists(inputId)
        this.finEnonce()
      }

      break // case "enonce":
    } // case 'enonce'
    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (sq.etape === 1 ? validationEditeurInterm() : validationEditeurs()) {
        validation()
        if (sq.etape === 1) {
          bilanReponse = 'exact'
        } else {
          if (sq.simplifier) {
            if (sq.resolu) { bilanReponse = 'exact' } else {
              if (sq.exact) {
                bilanReponse = 'exactPasFini'
                simplificationPossible = true
              } else { bilanReponse = 'faux' }
            }
          } else {
            if (sq.resolu || sq.exact) {
              bilanReponse = 'exact'
              if (!sq.resolu) simplificationPossible = true
            } else { bilanReponse = 'faux' }
          }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        montreEditeurs(false)
        elts.correction.style.color = this.styles.cfaux
        elts.btnRecopier.style.display = 'none'
        elts.info.style.display = 'none'

        afficheSolution(false)
        afficheFiguresolution()
        j3pAddContent(elts.correction, tempsDepasse, { replace: true })
        if (sq.solution) j3pAddContent(elts.correction, textes.solutionIci)
        else j3pAddContent(elts.correction, textes.solDesact)
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          elts.correction.style.color = this.styles.cfaux
          j3pAddContent(elts.correction,
            sq.tropDeCalculsInterm
              ? textes.tropDeInterm
              : (sq.signeEgalManquant ? textes.egalitesNecess : textes.repIncorrecte),
            { replace: true }
          )
          this.afficheBoutonValider()
        } else {
          // Bonne réponse
          if (bilanReponse === 'exact') {
            if (sq.etape !== 1) {
              this.score += 1
              elts.correction.style.color = this.styles.cbien
              const mess = simplificationPossible ? textes.bienPasSimplifie : textes.cbien
              j3pAddContent(elts.correction, mess, { replace: true })
              montreEditeurs(false)
              sq.numEssai++ // Pour un affichage correct dans afficheReponse
            }
            afficheReponse('exact', false)
            if (sq.etape === 2) {
              elts.btnRecopier.style.display = 'none'
              elts.info.style.display = 'none'
              afficheSolution(true)
              afficheFiguresolution()
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              if (sq.numEssai > sq.nbEssais1) {
                etapeSuivante()
              } else {
                j3pEmpty(elts.info)
                afficheNombreEssaisRestants()
                videEditeurs()
              }
            }
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              elts.correction.style.color =
                (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) ? '#0000FF' : this.styles.cfaux
              j3pAddContent(elts.correction, textes.bonPasFini, { replace: true })
            } else {
              elts.correction.style.color = this.styles.cfaux
              j3pAddContent(elts.correction, textes.cfaux, { replace: true })
            }
            j3pEmpty(elts.info)
            if (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) {
              afficheNombreEssaisRestants()
              videEditeurs()
              afficheReponse(bilanReponse, false)
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              elts.correction.innerHTML += '<br>' + '<br>' + essaieEncore
            } else {
              // Erreur au nème essai
              if (sq.etape === 2) {
                montreEditeurs(false)
                elts.btnRecopier.style.display = 'none'
                elts.info.style.display = 'none'
                afficheSolution(false)
                afficheReponse(bilanReponse, true)
                afficheFiguresolution()
                if (sq.solution) elts.correction.innerHTML += '<br><br>La solution se trouve ci-contre.'
                else elts.correction.innerHTML += '<br><br>L’affichage de la solution a été désactivé par le professeur.'
                this.etat = 'navigation'
                this.sectionCourante()
              } else {
                elts.correction.innerHTML += '<br><br>On passe à la question suivante.'
                etapeSuivante()
              }
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break
    } // case 'correction'

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
