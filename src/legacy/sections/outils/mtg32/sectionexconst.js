import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pDiv, j3pElement, j3pEmpty, j3pShowError } from 'src/legacy/core/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { MathfieldElement } from 'mathlive'
import { cleanLatexForMl } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans } from 'src/lib/outils/mathlive/display'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2017

    sectioexconstt
    Sert à mettre en ligne un exercice de construction à l’aide de MathGraph32
    La figure utilisée doit comprendre une macro d’apparition d’objets d’intitulé #Solution# dont les objets qu’elle
    fait apparaître sont les objets à construire par l’élève.
    Le commentaire de la macro peut préciser quels sont les objets de type numériques que l’élève a le droit
    d’utiliser. Par exemple {a} et {f} pour utiliser un calcul nommé a et une fonction nommée f.
    Les outils permis pour la construction sont contenus dans la figure elle-même via le menu Options-
    Figure en cours - Personnalisation Menu

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais autorisés à l’élève'],

    ['width', 700, 'entier', 'Largeur totale de la fenêtre de l’application en pixels (700 par défaut)'],
    ['height', 550, 'entier', 'Hauteur totale de la fenêtre de l’application en pixels (500 par défaut)'],
    ['titre', '', 'string', 'Titre de l’activité de construction'],
    ['fig', '', 'editor', 'Chaîne Base 64 contenant la figure de l’exercice', initEditor],
    ['stylePointCroix', false, 'boolean', 'true pour qu’au démarrage le syle de point en croix soit actif<br>Sinon ce sera le rond.'],
    ['correction', false, 'boolean', 'true si on veut que la solution soit donnée à la fin en cas d’échec'],
    ['modeDys', false, 'boolean', 'true si on veut que MathGraph32 fonctionne en mode "dys"'],
    ['enonceligne1', '', 'string', 'Première ligne de la consigne'],
    ['enonceligne2', '', 'string', 'ligne 2 de la consigne (peut être une chaîne vide)'],
    ['enonceligne3', '', 'string', 'ligne 3 de la consigne (peut être une chaîne vide)'],
    ['enonceligne4', '', 'string', 'ligne 4 de la consigne (peut être une chaîne vide)'],
    ['enonceligne5', '', 'string', 'ligne 5 de la consigne (peut être une chaîne vide)'],
    ['enonceligne6', '', 'string', 'ligne 6 de la consigne (peut être une chaîne vide)'],
    ['enonceligne7', '', 'string', 'ligne 7 de la consigne (peut être une chaîne vide)'],
    ['enonceligne8', '', 'string', 'ligne 8 de la consigne (peut être une chaîne vide)'],
    ['figcor1', '', 'editor', 'Chaîne Base 64 contenant la correction principale de l’exercice', initEditor],
    ['widthcor1', 700, 'entier', 'Largeur en pixels de la première figure de correction'],
    ['heightcor1', 500, 'entier', 'Hauteur en pixels de la première figure de correction'],
    ['figcor2', '', 'editor', 'Chaîne Base 64 contenant une éventuelle seconde correction de l’exercice (peut-être une chaîne vide si une seule correction)', initEditor],
    ['widthcor2', 700, 'entier', 'Largeur en pixels de la deuxième figure de correction (peut-être une chaîne vide si une seule correction)'],
    ['heightcor2', 500, 'entier', 'Hauteur en pixels de la deuxième figure de correction (peut-être une chaîne vide si une seule correction)'],
    ['cor1ligne1', '', 'string', 'Première ligne d’explications de la première figure de correction'],
    ['cor1ligne2', '', 'string', 'ligne 2 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor1ligne3', '', 'string', 'ligne 3 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor1ligne4', '', 'string', 'ligne 4 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor1ligne5', '', 'string', 'ligne 5 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor1ligne6', '', 'string', 'ligne 6 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor1ligne7', '', 'string', 'ligne 7 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor1ligne8', '', 'string', 'ligne 8 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor2ligne1', '', 'string', 'Première ligne d’explications de la deuxième figure de correction (peut-être une chaîne vide si une seule correction)'],
    ['cor2ligne2', '', 'string', 'ligne 2 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor2ligne3', '', 'string', 'ligne 3 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor2ligne4', '', 'string', 'ligne 4 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor2ligne5', '', 'string', 'ligne 5 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor2ligne6', '', 'string', 'ligne 6 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor2ligne7', '', 'string', 'ligne 7 d’explications de la première figure de correction (peut être une chaîne vide)'],
    ['cor2ligne8', '', 'string', 'ligne 8 d’explications de la première figure de correction (peut être une chaîne vide)']
  ]
}

// cf src/legacy/sections/outils/mtg32/sectionex_Calc_Multi_Edit_Multi_Etapes.js pour ajouter un validator
/**
 * Ajoute l’éditeur mathgraph dans container, la promesse sera résolue avec la figure
 * @param {HTMLElement} container
 * @param {string} fig
 * @return {Promise<string>}
 */
async function initEditor (container, fig) {
  const { default: editor } = await import('./mtgEditor.js')
  return editor(container, { fig })
}

/**
 * section exconst
 * @this {Parcours}
 */
/**
 * section exconst
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  const textes = {
    ilReste: 'Il reste ',
    ilResteUne: 'Il reste une validation possible',
    aucunObj: 'Aucun objet n’a été construit.',
    ilFautNom: 'Il faut nommer ',
    regardeCor: 'Regarde la correction sous la figure.',
    solution: 'Solution ',
    ilManque: 'Il manque un ou des éléments de type : '
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param {string} tagLatex le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgApp.getList()
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('infoNbEssais')
    const nbe = sq.nbchances - sq.numEssai + 1
    afficheMathliveDans('infoNbEssais', 'infoessais', nbe === 1 ? textes.ilResteUne : textes.ilReste + nbe + ' validations possibles.', { style: { color: '#7F007F' } })
  }

  sq.afficheSolution = function (color) {
    $('#infoNbEssais').css('display', 'none')
    if (sq.correction) {
      // Si la figure contient un affichage LaTeX de tag solution on affiche les explications qu’il contient
      // avant les figures solutions
      const ch = extraitDeLatexPourMathlive('solution')
      if (ch !== '') {
        $('#divSolution').css('display', 'block').html('')
        afficheMathliveDans('divSolution', 'solution', ch, { style: { color } })
      }

      for (let i = 1; i <= sq.nbsol; i++) {
        j3pDiv('conteneur', 'sol' + i, '')
        j3pElement('sol' + i).style.color = color
        afficheMathliveDans('sol' + i, 'titreexp' + i, textes.solution + ((sq.nbsol > 1) ? ('n°' + i) : '') + ':\n', { style: { color: 'brown' } })
        afficheMathliveDans('sol' + i, 'solution' + i, sq['sol' + i] + '\n')
        if (sq['figcor' + i] !== '') {
          const width = sq['widthcor' + i]
          const height = sq['heightcor' + i]
          j3pDiv('conteneur', 'divmtg32sol' + i, '')
          j3pCreeSVG('divmtg32sol' + i, {
            id: 'mtg32svgsol' + i,
            width,
            height
          })
          sq.player.addDoc('mtg32svgsol' + i,
            sq['figcor' + i], true)
          sq.player.calculate('mtg32svgsol' + i, true)
          sq.player.display('mtg32svgsol' + i)
        }
      }
    }
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        this.construitStructurePage('presentation1bis')
        this.validOnEnter = false // ex donneesSection.touche_entree
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.nbchances = parseInt(this.donneesSection.nbchances)
        sq.titre = this.donneesSection.titre
        sq.width = this.donneesSection.width || 700
        sq.height = this.donneesSection.height || 550
        // Pour déboguage en local, les caractères # doivent être remplacés par des caractères !
        sq.fig = this.donneesSection.fig
        sq.stylePointCroix = this.donneesSection.stylePointCroix
        sq.modeDys = this.donneesSection.modeDys
        // if (sq.fig.indexOf('!') !== -1) sq.fig = sq.fig.replace(new RegExp('!', 'g'), '#')
        if (sq.fig.indexOf('!') !== -1) sq.fig = sq.fig.replace(/!/g, '#')
        sq.nbsol = 1 // Contiendra le nombre de figures pour la correction.
        if (sq.fig === '') {
          sq.fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABA+TMzNAANmcmH###8BAP8AAAAAIAAAgPUAAOEHAACA#gAA4SIAAID8AACA+gAAgHwAAH0MAACDPAAAgVkAAIAgAACADQAAgBIAAIAWAACAEAAAgCUAAH0XAACAKwAAgL0AAIi4AACBEAAAgREAAID#AACALAAAgCoAAIA0AACAJwAAgCgAAH0SAAB9FAAAgIkAAICKAAAAAAHjAAABbgAAAAAAAAAAAAAAAAAAABT#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQEAAAELAAFVAMAYAAAAAAAAwDQAAAAAAAAFAAFAMQAAAAAAAEAnZmZmZmZm#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAQAAACQAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAQAAAQsAAVYAwBgAAAAAAADANAAAAAAAAAUAAUA9AAAAAAAAAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQEAAAALAAABAQEAAAABAAAAA#####8AAAABAAdDTWlsaWV1AP####8BAQAAACQAAAAAAAAAAAAAAD#jMzMzMzMzBQAAAAABAAAAA#####8AAAACAAxDQ29tbWVudGFpcmUA#####wEBAAAAwAgAAAAAAABAGAAAAAAAAAAAAAUNAAAAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAALAAFPAL#wAAAAAAAAQBZmZmZmZmYFAABAY2AAAAAAAEBhoAAAAAAA#####wAAAAIACUNDZXJjbGVPUgD#####AAAAAAABAAAACAAAAAFACAAAAAAAAAD#####AAAAAQAPQ1BvaW50TGllQ2VyY2xlAP####8BAAAAACQAAAAAAAAAAAAAAD#jMzMzMzMzBQABQASWIVqWJ+0AAAAJAAAABwD#####AAAAAADALAAAAAAAAMAsAAAAAAAAAAAACg0AAAAAAAAAAAAAAAFDAAAACgD#####AQAAAAAkAAAAAAAAAAAAAAA#4zMzMzMzMwUAAUACWacQVm3aAAAACQAAAAoA#####wEAAAAAJAAAAAAAAAAAAAAAP+MzMzMzMzMFAAE#9oVZfIT3KAAAAAkAAAAKAP####8BAAAAACQAAAAAAAAAAAAAAD#jMzMzMzMzBQABP8B5IIAMofkAAAAJ#####wAAAAEAC0NNZWRpYXRyaWNlAP####8BAH8AACQAAAEAAQAAAAwAAAANAAAACwD#####AQB#AAAkAAABAAEAAAAOAAAADQAAAAYA#####wEAAAAAJAAAAAAAAAAAAAAAP+MzMzMzMzMFAAAAAAwAAAANAAAABgD#####AQAAAAAkAAAAAAAAAAAAAAA#4zMzMzMzMwUAAAAADgAAAA3#####AAAAAQAQQ01hY3JvQXBwYXJpdGlvbgD#####AQAAAAH#####DUBj4AAAAAAAQCgAAAAAAAABAAAAAAAAAAAAAAojU29sdXRpb24jAAAAAAABAAAACAAAAAAH##########8='
          sq.correction = true
          sq.figcor1 = 'TWF0aEdyYXBoSmF2YTEuMAAAABA#gAAAAANmcmH###8BAP8BAAAAAAAAAAABUwAAASYAAAAAAAAAAAAAAAAAAAAd#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEACkNQb2ludEJhc2UA#####wEBAAABCwABVQDAGAAAAAAAAMA0AAAAAAAABQABQDEAAAAAAABAJ2ZmZmZmZv####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQEAAAALAAABAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQEAAAELAAFWAMAYAAAAAAAAwDQAAAAAAAAFAAFAPQAAAAAAAAAAAAL#####AAAAAQAIQ1NlZ21lbnQA#####wEBAAAACwAAAQEBAAAAAQAAAAP#####AAAAAQAHQ01pbGlldQD#####AQEAAAALAAAAAAAAAAAAAAA#4zMzMzMzMwUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAQAAAMAIAAAAAAAAQBgAAAAAAAAAAAAFDQAAAAAAAAAAAAAAATH#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAABAAAAAwAAAAIA#####wAAAAAACwABTwC#8AAAAAAAAEAWZmZmZmZmBQAAQGNgAAAAAABAYaAAAAAAAP####8AAAACAAlDQ2VyY2xlT1IA#####wAAAAAAAQAAAAgAAAABQAgAAAAAAAAA#####wAAAAEAD0NQb2ludExpZUNlcmNsZQD#####AQAAAAALAAAAAAAAAAAAAAA#4zMzMzMzMwUAAUAEliFaliftAAAACQAAAAcA#####wAAAAAAwCwAAAAAAADALAAAAAAAAAAAAAoNAAAAAAAAAAAAAAABQwAAAAoA#####wAAAAAACwABRQDALAAAAAAAAMAxZmZmZmZmBQABQAJZpxBWbdoAAAAJAAAACgD#####AAAAAAALAAFGAMAAAAAAAAAAwDVmZmZmZmYFAAE#9oVZfIT3KAAAAAkAAAAKAP####8AAAAAAAsAAUcAQBgAAAAAAADAHZmZmZmZmgUAAT#AeSCADKH5AAAACf####8AAAABAAtDTWVkaWF0cmljZQD#####AAB#AAALAAABAAEAAAAMAAAADQAAAAsA#####wAAfwAACwAAAQABAAAADgAAAA0AAAAGAP####8AAAAAAAsAAAAAAAAAAAAAAD#jMzMzMzMzBQAAAAAMAAAADQAAAAYA#####wAAAAAACwAAAAAAAAAAAAAAP+MzMzMzMzMFAAAAAA4AAAANAAAABQD#####AAAAAAALAAABAQEAAAAMAAAAEQAAAAUA#####wAAAAAACwAAAQEBAAAAEQAAAA3#####AAAAAQAOQ01hcnF1ZVNlZ21lbnQA#####wD#AP8AAgAAAAATAAAADAD#####AP8A#wACAAAAABQAAAAFAP####8AAAAAAAsAAAEBAQAAAA0AAAASAAAABQD#####AAAAAAALAAABAQEAAAASAAAADgAAAAwA#####wD#AP8AAgEAAAAXAAAADAD#####AP8A#wACAQAAABj#####AAAAAgAXQ01hcnF1ZUFuZ2xlR2VvbWV0cmlxdWUA#####wAAAAAAAQAAAABAJIv3KlWRBwAAAAgAAAARAAAADAAAAA0A#####wAAAAAAAQAAAABAJHcDQ3ApqgAAAAgAAAASAAAADgAAAAf##########w=='
          sq.widthcor1 = 339
          sq.heightcor1 = 286
          sq.enonce = 'Avec les outils disponibles il faut créer le  point O centre du cercle C.<br>Cliquer sur OK quand la construction est finie.'
          sq.sol1 = 'On crée trois points E, F et G liés au cercle. Tout point de la médiatrice d’un segment<br>est équidistant de ses extrémités.<br>' +
            'Le centre du cercle est donc le point d’intersection des mediatrices des segment [EF] et [GH].'
          sq.titre = 'Ressource à personnaliser'
          j3pShowError('Cette ressource doit être personnalisée en éditant la ressource dans l’éditeur de graphes.')
        } else {
          sq.correction = this.donneesSection.correction
          sq.figcor1 = this.donneesSection.figcor1
          sq.widthcor1 = parseInt(this.donneesSection.widthcor1)
          sq.heightcor1 = parseInt(this.donneesSection.heightcor1)
          sq.enonce = ''
          for (let i = 1; i < 9; i++) {
            if (this.donneesSection['enonceligne' + i] !== '') {
              if (i !== 1) sq.enonce += '<br>'
              sq.enonce += this.donneesSection['enonceligne' + i]
            }
          }
          sq.sol1 = ''
          for (let i = 1; i < 9; i++) {
            if (this.donneesSection['cor1ligne' + i] !== '') {
              if (i !== 1) sq.sol1 += '<br>'
              sq.sol1 += this.donneesSection['cor1ligne' + i]
            }
          }
          sq.figcor2 = this.donneesSection.figcor2
          if (sq.figcor2 !== '') {
            sq.nbsol++
            sq.widthcor2 = parseInt(this.donneesSection.widthcor1)
            sq.heightcor2 = parseInt(this.donneesSection.heightcor1)
            sq.sol2 = ''
            for (let i = 1; i < 9; i++) {
              if (this.donneesSection['cor2ligne' + i] !== '') {
                if (i !== 1) sq.sol2 += '<br>'
                sq.sol2 += this.donneesSection['cor2ligne' + i]
              }
            }
          }
        }
        this.score = 0
        this.afficheTitre(sq.titre)

        j3pDiv(this.zones.MG, {
          id: 'conteneur',
          contenu: '',
          style: this.styles.etendre('toutpetit.enonce', { padding: '6px' })
        })
        j3pDiv('conteneur', 'enonce', '')
        j3pDiv('conteneur', 'infoNbEssais', '')
        j3pDiv('conteneur', 'divmtg32', '')
        // Un div pour contenir la solution affichée par MathQuill si la figure contient un LaTeX de tag 'solution'
        j3pDiv('conteneur', 'divSolution', '')
        $('#divSolution').css('display', 'none')

        const svgOptions = {
          idSvg: 'mtg32svg',
          width: sq.width,
          height: sq.height
        }
        const mtgOptions = {
          fig: sq.fig,
          dys: sq.modeDys,
          bplayer: true,
          open: false,
          newFig: false,
          save: false,
          options: false,
          stylePointCroix: sq.stylePointCroix,
          isEditable: true
        }
        const style = parcours.styles.petit.correction
        style.marginTop = '1.5em'
        style.marginLeft = '0.5em'
        j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })

        getMtgApp('divmtg32', svgOptions, mtgOptions).then(mtgApp => {
          sq.mtgApp = mtgApp
          // mtg32App.removeAllDoc();
          // Ci-dessous, emploer local:true pour déboguer en local et false pour version en ligne
          sq.player = mtgApp.player
          if (sq.mtgApp.getList().getByTag('enonce', true)) {
            afficheMathliveDans('enonce', 'texte', extraitDeLatexPourMathlive('enonce') + '\n')
          } else {
            afficheMathliveDans('enonce', 'texte', sq.enonce + '\n')
          }
          afficheNombreEssaisRestants()
          parcours.finEnonce()
        }).catch(j3pShowError)
      } else {
        j3pEmpty('correction')
        this.afficheBoutonValider()
        sq.numEssai = 1
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let ch, k
      this._stopTimer()
      j3pEmpty('correction')
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        // mtg32App.setActive("mtg32svg", false);
        sq.mtgApp.activateForCor()
        sq.afficheSolution(this.styles.toutpetit.correction.color)
        j3pAddContent('correction', tempsDepasse)
        if (sq.correction) j3pAddContent('correction', '\n' + textes.regardeCor)
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (!sq.mtgApp.objectConstructed()) {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pAddContent('correction', textes.aucunObj)
          return
        }
        const missingTypes = sq.mtgApp.getMissingTypes()
        if (missingTypes.length !== 0) {
          ch = textes.ilManque
          j3pElement('correction').style.color = this.styles.cfaux
          for (k = 0; k < missingTypes.length; k++) {
            ch = ch + missingTypes[k]
            if (k !== missingTypes.length - 1) ch = ch + ', '; else ch = ch + '.'
          }
          j3pAddContent('correction', ch)
          return
        }

        const missingNames = sq.mtgApp.getMissingNames()
        if (missingNames.length !== 0) {
          ch = textes.ilFautNom
          j3pElement('correction').style.color = this.styles.cfaux
          for (k = 0; k < missingNames.length; k++) {
            ch = ch + missingNames[k]
            if (k !== missingNames.length - 1) ch = ch + ', '; else ch = ch + '.'
          }
          j3pAddContent('correction', ch, { replace: true })
          return
        }
        // sq.mtgApp.setActive(false);
        let bilanreponse = ''
        const res1 = sq.mtgApp.validateAnswer()
        if (res1) {
          bilanreponse = 'exact'
        } else {
          bilanreponse = 'erreur'
        }
        // Bonne réponse
        if (bilanreponse === 'exact') {
          // mtg32App.setActive("mtg32svg", false);
          sq.mtgApp.activateForCor()
          this.score++
          j3pElement('correction').style.color = this.styles.cbien
          j3pAddContent('correction', cBien, { replace: true })
          sq.afficheSolution(this.styles.cbien)
          // mtg32App.executeMacro("mtg32svg","solution");
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          j3pElement('correction').style.color = this.styles.cfaux
          j3pAddContent('correction', cFaux, { replace: true })
          sq.numEssai++

          // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
          if (sq.numEssai <= sq.nbchances) {
            // sq.mtgApp.setActive(true);
            j3pAddContent('correction', '<br>' + essaieEncore)
            afficheNombreEssaisRestants()
            // j3pAddContent('infoessais', 'Il te reste ' + String(sq.numEssai - 1) + ' validation(s) possible(s).', { replace: true })
            // indication éventuelle ici
          } else { // Erreur au nème essai
            sq.mtgApp.activateForCor()
            j3pElement('infoNbEssais').removeChild(j3pElement('infoessais'))
            sq.afficheSolution(this.styles.toutpetit.correction.color)
            if (sq.correction) j3pAddContent('correction', '\n' + textes.regardeCor)
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    } // case 'correction'

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
