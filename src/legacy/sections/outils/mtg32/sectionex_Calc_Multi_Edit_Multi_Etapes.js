import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pBarre, j3pDetruit, j3pElement, j3pEmpty, j3pPaletteMathquill, j3pRestriction, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { unLatexify } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getFirstIndexParExcluded, getIndexFermant } from 'src/lib/utils/string'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { cleanLatexForMl, mathliveRestrict, replaceOldMQCodes, transposeMatrices } from 'src/lib/outils/mathlive/utils'
import { hideCurrentVirtualKeyboard, resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import textesGeneriques from 'src/lib/core/textes'

import { setFocusById, setFocusToFirstInput } from 'src/legacy/sections/squelettes/helpersMtg'

const { essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    2021. REvu en janvier 2024 pour passage à Mathlive

    section ex_Multi_Edit_Multi_Etapes
    Squelette permettant de poser des questions successives sous forme de formulaires.
    Le paramètre fig contient le code Base 64 de la figure associée à l’exercice.
    Le nombre d’étapes est renvoyé par la figure mtg32 associée dans le calcul nbEtapes (8 maxi).
    Chaque formulaire (qui est affiché en mode texte) peut contenir des listes de choix déroulantes,
    des éditeurs en mode texte (répérés par les caractères edit1, edit2, ...), des éditeurs en mode
    maths repérés par des \editable{} (sans numérotation). Ces derniers permettent qu’une partie d’une
    formule LaTeX soit un champ d’édition.
    Le formulaire à l’étape i est renvoyé par un affichage LaTeX (formé éventuellement d’un tableau de plusieurs lignes,
    chaque ligne étant formée d’un \text{}) dont la tag doit être formulaire suivi du n° de l’étape.
    A chaque étape, le formulaire est précédé d’un affichage de consigne qui doit reprendre les résultats des quesions précédentes.
    Cette consigne est données par un affichage LaTeX qui est en général un tableau formé de plusieurs lignes,
    chaque ligne étant formée d’un \text{} dont le contenu fourni la ligne correspondante.
    Cet affichage LaTeX de la consigne à l’étape i doit avoir pour tag consigne suivi du n° i.
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
    Ce formulaire peut contenir, en mode texte, des éditeurs mathquill, repérés dans le formulaire par edit suivi
    du numéro de l’éditeur (les n°s commençant à 1) et, en mode maths, des \editable{} ce qui permet
    de mettre des éditeurs directement à l’intérieur de formules, comme par exemple un éditeur sous une racine carrée.
    Il faut aussi contenir des contenir des choix à liste déroulantes repérés par list suivi de n° de la liste.
    Les \editable{} eux n’ont pas à être numérotés.
    Imaginons par exemple que le LaTeX de tag formulaire2 contienne ceci (nous sommes à l’étape 2) :
    \text{La fonction $f$ est list1 sur $\R$, \Val{a} a edit1 antécédents par $f$ et $f(\Val{b})=\editable{}\sqrt{2}+\editable{}$}
    Ce formulaire a un choix de liste. La figure mtg32 doit donc contenir un affichage LaTeX de tag list21, formé d’une tableau
    de deux lignes dont les deux lignes sont par exemple \text{croissante} et \text{décroissante}. Elle doit aussi contenir
    un calcul nommé reslist21 dont la valeur est l’indice de la bonne réponse (1 ou 2 ici).
    Le premier chiffre du nom du calcul (reslist321 par exemple) est le n° de l’étape et le second le n° de la liste
    dans le formulaire.
    Si la figure contient un calcul nommé listaleat21 qui vaut 1, les éléments de la liste seront présentés
    mélangés au hasard
    Si plusieurs items de la liste peuvent être acceptés, la figure doit contenir un calcul nommé par exemple repList21
    qui doit être initalisé à une valeur autre que -1.
    Si ce calcul existe, la figure doit contenir un autre calcul nommé exactList21 qui doit valoir 1 si la réponse
    de l’élève est une des réponses acceptées et 0 sinon.
    Il contient un éditeur mathquill en mode texte repéré par edit1.
    Il doit donc contenir un calcul (ou fonction) nommé rep21 et deux calculs nommés resolu21 et exact21.
    resolu21 doit valoir 1 si la réponse entrée par l’élève dans ce champ d’édition est une réponse acceptée commme
    réponse finale et 0 sinon. exact21 doit lui renvoyer 1 si la réponse contenue dans le champ est exacte et 0 sinon.
    Le formulaire contient deux \editable{}. La figure doit donc contenir deux calculs (ou fonctions) nommés repEditable21
    et repEditable22 destinés à contenir les réponses contenues dans ces champs.
    La figure doit donc aussi contenir des calculs nommés resoluEditable21, exactEditable21, resoluEditable22, exactEditable22.
    Par exemple, resoluEditable21 doit renvoyer 1 si la réponse dans le premier editable est acceptée comme réponse finale
    et 0 sinon, et exactEditable21 lui doit renvoyer 1 si la réponse dans le premier editable est exacte et 0 sinon.
    Si le paramètre bigSize du fichier annexe est présent et à true, l’éditeur de formulaire et les réponses de l’élève utiliseront
    une taille plus grande que la taille standard.
    Il et possible de faire afficher des lignes lors de la correction. Ces lignes doivent alors être les lignes d’une affichage LaTEX
    de tag solution de la figure.
    Si, par exemple à l’étape 2, on a deux champs d’éditon MathQuill dont le deuxième doit contenir une égalité, la figure MathGraph32,
    la figure doit contenir un calcul nommé estEgalie22 qui revoie la valeur 1.
    Si on ne souhaite pas que la figure mtg32 soit affichée, il faut mettre les paramètres width et height du fichier annexe
    à 0 Sinon ces variables doivent contenir la largeur et la hauteur de la figure en pixels.
    On peut aussi faire afficher à la correction une figure dont le code base 64 est contenu dans le paramètre figSol,
    les paramètres widthSol et heightSol désignant la largeur et la hauteur de cette figure.
    Avant d’afficher cette figure de correction, on donne aux calculs ou fonctions dont le nom figure dans la paramètre de nom
    param les formules qu’ils avaient dans la figure principale.
    Avant la figure de correction, on peut afficher des epxlications contenues dans le paramètre explications.
    Ces explications peuvent utiliser des affichages LaTeX de la figure de correction, leu nombre étant dans le paramètre
    nbLatexSol. On fait référence à eux via des $£a$, $£b$, etc ...
    Si la figure est affichée, elle peut contenir une macro d’intitulé solution qui sera exécutée lors de la correction.
    Si la figure contenue  dans fig un paramètre nbVar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Si on veut accepter une réponse +∞ ou -∞ dans un champ d’édition, le bouton correspondant doit être activé
    et la figure mtg32 doit contenir un calcul de nom infinity qui ne soit pas égal à -1
    Lorsqu’une étape est une étape avec des éditeurs internes et qu’on ne souhaite pas que les mauvaises réponses
    soient remplacés par les bonnes réponses (en rouge) lors du passage à l’étape suivante, il suffit
    de créer dans la figure un calc nommé correctionInterne suivi du n° d’épate et de valeur 0.
    Par défaut les réponses fausses sont remplacées par les bonnes réponses en rouge.
*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
MathfieldElement.soundsDirectory = '/static/mathlive/sounds'

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['titre', '', 'string', 'Titre de l’activité'],
    ['taillePoliceFormulaire', 24, 'entier', 'Taille en pixels de la police pour les éditeurs, 24 par défaut'],
    ['taillePoliceReponses', 22, 'entier', 'Taille en pixels de la police pour les affiches des réponses, 22 par défaut'],
    ['taillePoliceSolution', 22, 'entier', 'Taille en pixels de la police pour l’affichage de la solution, 22 par défaut'],
    ['solution', true, 'boolean', 'false pour que la solution ne soit pas affichée en fin d’exercice'],
    ['calculatrice', false, 'boolean', 'true pour avoir une calculatrice disponible'],
    ['fig', '', 'editor', 'Chaîne Base 64 contenant la figure principale de l’exercice', initEditor],
    ['estExerciceConstruction', false, 'boolean', 'true seulement si la figure est en mode construction'],
    ['erreurSiBorneFermeeSurInfini', false, 'boolean', 'Mettre à true si on veut qu’un intervalle ferme en +∞ ou -∞ compte la réponse comme fausse'],
    ['width', 800, 'entier', 'Largeur de la figure initiale'],
    ['height', 600, 'entier', 'Hauteur de la figure initiale'],
    ['param', 'abcd', 'string', 'Chaîne formé des paramètres que l’exercice utilise (de a à r, voir ci-dessous'],
    ['nbEssais1', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 1'],
    ['nbEssais2', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 2 (si présente)'],
    ['nbEssais3', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 3 (si présente)'],
    ['nbEssais4', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 4 (si présente)'],
    ['nbEssais5', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 5 (si présente)'],
    ['nbEssais6', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 6 (si présente)'],
    ['nbEssais7', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 7 (si présente)'],
    ['nbEssais8', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 8 (si présente)'],
    ['nbEssais9', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 9 (si présente)'],
    ['nbEssais10', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 10 (si présente)'],
    ['nbEssais11', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 11 (si présente)'],
    ['nbEssais12', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 12 (si présente)'],
    ['listesLatex', false, 'boolean', 'true si les items des listes sont des formules LaTeX'],
    ['coefEtape1', 0.5, 'number', 'Nombre strictement compris entre 0 et 1 à rajouter au score<br>pour une réussite à l’étape 1, si nul mmême répartition pour toutes les étapes'],
    ['coefEtapeFin', -1, 'number', 'Nombre strictement compris entre 0 et 1 à rajouter au score<br>pour une réussite à la dernière étape, pas pris en compte si égal à -1 ou si le nombre d’étapes est 2'],
    ['charset1', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 1 chaîne vide pour autoriser tous les caractères'],
    ['charset2', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 2 chaîne vide pour autoriser tous les caractères'],
    ['charset3', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 3 chaîne vide pour autoriser tous les caractères'],
    ['charset4', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 4 chaîne vide pour autoriser tous les caractères'],
    ['charset5', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 5 chaîne vide pour autoriser tous les caractères'],
    ['charset6', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 6 chaîne vide pour autoriser tous les caractères'],
    ['charset7', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 7 chaîne vide pour autoriser tous les caractères'],
    ['charset8', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 8 chaîne vide pour autoriser tous les caractères'],
    ['charset9', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 9 chaîne vide pour autoriser tous les caractères'],
    ['charset10', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 10 chaîne vide pour autoriser tous les caractères'],
    ['charset11', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 11 chaîne vide pour autoriser tous les caractères'],
    ['charset12', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul à l’étape 12 chaîne vide pour autoriser tous les caractères'],
    ['textesSensiblesCasse', true, 'boolean', 'Si false, la réponse de l’élève est convertie en majuscules et comparée à la réponse attendue'],
    ['charsetText1', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 1 chaîne vide pour autoriser tous les caractères'],
    ['charsetText2', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 2 chaîne vide pour autoriser tous les caractères'],
    ['charsetText3', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 3 chaîne vide pour autoriser tous les caractères'],
    ['charsetText4', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 4 chaîne vide pour autoriser tous les caractères'],
    ['charsetText5', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 5 chaîne vide pour autoriser tous les caractères'],
    ['charsetText6', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 6 chaîne vide pour autoriser tous les caractères'],
    ['charsetText7', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 7 chaîne vide pour autoriser tous les caractères'],
    ['charsetText8', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 8 chaîne vide pour autoriser tous les caractères'],
    ['charsetText9', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 9 chaîne vide pour autoriser tous les caractères'],
    ['charsetText10', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 10 chaîne vide pour autoriser tous les caractères'],
    ['charsetText11', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 11 chaîne vide pour autoriser tous les caractères'],
    ['charsetText12', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de chaînes à l’étape 12 chaîne vide pour autoriser tous les caractères'],
    ['btnPuis', true, 'boolean', 'Bouton puissance'],
    ['btnFrac', true, 'boolean', 'Bouton fraction'],
    ['btnRac', true, 'boolean', 'Bouton Racine carrée'],
    ['btnPi', false, 'boolean', 'Bouton pi'],
    ['btnSupegal', false, 'boolean', 'Bouton supérieur ou égal'],
    ['btnInfegal', false, 'boolean', 'Bouton inférieur ou égal'],
    ['btnCos', false, 'boolean', 'Bouton cosinus'],
    ['btnSin', false, 'boolean', 'Bouton sinus'],
    ['btnTan', false, 'boolean', 'Bouton tangente'],
    ['btnExp', false, 'boolean', 'Bouton exponentielle'],
    ['btnLn', false, 'boolean', 'Bouton logarithme népérien'],
    ['btnLog', false, 'boolean', 'Bouton logarithme décimal'],
    ['btnAbs', false, 'boolean', 'Bouton valeurabsolue'],
    ['btnConj', false, 'boolean', 'Bouton conjugué'],
    ['btnInteg', false, 'boolean', 'Bouton intégrale'],
    ['btnPrim', false, 'boolean', 'Bouton primitive'],
    ['btnInf', false, 'boolean', 'Bouton infini'],
    ['btnIndice', false, 'boolean', 'Bouton indice'],
    ['btnR', false, 'boolean', 'Bouton ensemble des réels'],
    ['btnUnion', false, 'boolean', 'Bouton symbole de réunion'],
    ['btnAcc', false, 'boolean', 'Bouton accolade'],
    ['variables', 'xt', 'string', 'Noms des variables autorisées pour un calcul d’intégrale ou de primitive'],
    ['figSol', '', 'editor', 'Code Base64 d’une figure supplémentaire pour la correction, vide pour aucune figure supplémentaire', initEditor],
    ['widthSol', 600, 'entier', 'Largeur de la figure supplémentaire de solution'],
    ['heightSol', 500, 'entier', 'Hauteur de la figure supplémentaire de solution'],
    ['nbLatexSol', 1, 'entier', 'Nombre d’affichages LaTeX de la figure supplémentaire à récupérer pour affichage dans la ligne d’explications'],
    ['explicationSol', '', 'string', 'Ligne d’explications précédent la figure supplémentaire de solution (peut être vide)'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['fig2', '', 'editor', 'Chaîne Base 64 contenant la figure éventuelle de l’exercice à partir de l’étape 2', initEditor],
    ['fig3', '', 'editor', 'Chaîne Base 64 contenant la figure éventuelle de l’exercice à partir de l’étape 3', initEditor],
    ['fig4', '', 'editor', 'Chaîne Base 64 contenant la figure éventuelle de l’exercice à partir de l’étape 4', initEditor],
    ['fig5', '', 'editor', 'Chaîne Base 64 contenant la figure éventuelle de l’exercice à partir de l’étape 5', initEditor],
    ['fig6', '', 'editor', 'Chaîne Base 64 contenant la figure éventuelle de l’exercice à partir de l’étape 6', initEditor],
    ['fig7', '', 'editor', 'Chaîne Base 64 contenant la figure éventuelle de l’exercice à partir de l’étape 7', initEditor],
    ['fig8', '', 'editor', 'Chaîne Base 64 contenant la figure éventuelle de l’exercice à partir de l’étape 8', initEditor],
    ['fig9', '', 'editor', 'Chaîne Base 64 contenant la figure éventuelle de l’exercice à partir de l’étape 9', initEditor],
    ['fig10', '', 'editor', 'Chaîne Base 64 contenant la figure éventuelle de l’exercice à partir de l’étape 10', initEditor],
    ['fig11', '', 'editor', 'Chaîne Base 64 contenant la figure éventuelle de l’exercice à partir de l’étape 11', initEditor],
    ['fig12', '', 'editor', 'Chaîne Base 64 contenant la figure éventuelle de l’exercice à partir de l’étape 12', initEditor]
  ]
}

const textes = {
  ilReste: 'Il reste ',
  essais: ' essais.',
  unEssai: ' un essai.',
  solutionIci: '\n\nLa solution se trouve ci-contre.',
  solDesact: '\nL’affichage de la solution a été désactivé par le professeur.',
  repIncorrecte: 'Réponse incorrecte',
  egaliteNecess: '\nIl faut écrire une égalité (et une seule)',
  inegaliteNecess: '\nIl faut écrire une inégalité',
  bonPasFini: 'Le calcul est bon mais pas écrit sous la forme demandée.',
  passQuestSuiv: '\n\nOn passe à la question suivante.',
  alertNbEt: 'La figure doit contenir un calcul nbEtapes de valeur entière au moins égale à 1',
  repExacte: 'Réponse exacte : ',
  repFausse: 'Reponse fausse : ',
  repExactPasFini: 'Exact pas fini : ',
  errNbEtapes: 'La figure doit contenir un calcul nommé nbEtapes contenant le nombre d’étapes de l’exercice',
  ilManque: 'Il manque : ',
  unAffLat: 'un affichage LaTeX de tag ',
  avert1: 'L’affichage LaTeX de tag ',
  avert2: ' doit contenir au moins un code LaTeX \\text{}.',
  aide1: 'C’est le contenu du ou des \\text{} qui fourni le contenu à afficher.',
  avert3: ' doit contenir au moins un éditeur repéré par edit, edittext ou list suivi du numéro de l’éditeur' +
    '\n ou un ou plusieurs \\editable{}',
  unCalcNom: 'un calcul (ou une fonction) nommé ',
  pourEtape: 'Pour l’étape ',
  ilManqueMin: ' il manque :',
  deuxCalcNom: 'deux calculs nommés',
  et: 'et',
  ou: 'ou',
  uneSeuleConst: 'La figure ne peut contenir qu’une seule étape de construction',
  nommer: 'Il faut nommer ',
  cbien: 'C’est bien !',
  cfaux: 'C’est faux !',
  infPasNombre: '<br><br>Le symbole ∞ ne représente pas un nombre.<br>On ne ferme pas un intervalle sur ∞ !'
}

/**
 * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
 * en une chaîne affichable par MathQuill
 * @param {CListeObjets} list la liste d’objets dans laquelle on recherche le LaTeX
 * @param {string} tagLatex le tag de l’affichage LaTeX de la figure
 * @return {string}
 */
function extraitDeLatexPourMathlive (list, tagLatex) {
  const latex = list.getByTag(tagLatex, true)
  if (latex === null || latex.className !== 'CLatex') return ''
  return cleanLatexForMl(latex.chaineLatex)
}

/**
 * Fonction renvoyant le nombre d’éditeurs internes contenus dans la figure de list à l’étape etape
 * c’est à dire si list contient des affichages LaTeX de tag formulaireint suivi du numéro du formulaire
 * @param {CListeObjets} list
 * @param {number} etape
 * @returns {number}
 */
function nbEditeursInternes (list, etape) {
  let res = 0
  let i = 1
  let el
  while ((el = list.getByTag('formulaireInt' + etape + String(i), true)) !== null) {
    if (el.className === 'CLatex' && el.pointLie !== null) {
      res++
      i++
    } else return res
  }
  return res
}

/**
 * Fonction renvoyant un tableau formé des messages d’erreur signalant que la figure
 * n’a pas certains éléments essentiels pour son fonctionnement
 * @param {CListeObjets} list
 */
function getTabErrors (list) {
  function estEtapeInterneOuConst (indiceEtape) {
    return (list.valueOf('etapeInterne' + indiceEtape, true) === 1) ||
      (list.valueOf('etapeConstruction' + indiceEtape, true) === 1)
  }
  const errors = []
  const nbEtapes = list.valueOf('nbEtapes', true)
  if (!Number.isInteger(nbEtapes) || nbEtapes < 1) {
    errors.push(textes.errNbEtapes)
  }
  const tabch = ['enonce', 'formulaire']
  let ch = ''
  for (let i = 1; i <= nbEtapes; i++) {
    for (let j = 0; j <= 1; j++) {
      const nom = tabch[j]
      const isEtapeInterne = estEtapeInterneOuConst(i)
      const nbEditInt = nbEditeursInternes(list, i)
      if ((j === 0 && nbEditInt === 0) || (j === 1 && !isEtapeInterne && (nbEditInt === 0))) {
        const latex = list.getByTag(nom + i, true)
        if (latex === null || latex.className !== 'CLatex') {
          ch += '\n - ' + textes.unAffLat + nom + i
        }
      }
    }
  }
  if (ch !== '') {
    errors.push(textes.ilManque + ch)
    return errors
  }
  ch = ''
  for (let i = 1; i <= nbEtapes; i++) {
    for (let j = 0; j <= 1; j++) {
      const nom = tabch[j]
      const isEtapeInterne = estEtapeInterneOuConst(i)
      const nbEditInt = nbEditeursInternes(list, i)
      if ((j === 0 && nbEditInt === 0) || (j === 1 && !isEtapeInterne && (nbEditInt === 0))) {
        const chaineLatex = list.getByTag(nom + i, true).chaineLatex
        if (chaineLatex.indexOf('\\text{') === -1) {
          if (i === 1) ch += '\n '
          ch += textes.avert1 + nom + i + textes.avert2
        }
      }
    }
  }
  if (ch !== '') {
    errors.push(ch + '\n' + textes.aide1)
  }
  // Pour chaque étape, on regarde si le formulaire contient bien des éditeurs
  // qui peuvent être plusieurs quand ils sont internes (plaqués sur la figure)
  for (let indEtape = 1; indEtape <= nbEtapes; indEtape++) {
    const nbEditInt = nbEditeursInternes(list, indEtape)
    const contientEditeursInternes = nbEditInt > 0
    const suffixe = contientEditeursInternes ? 'Int' + indEtape : String(indEtape)
    const estEtapeIneq = list.valueOf('etapeResIneq' + indEtape, true) === 1
    const tabTagFormulaire = []
    if (contientEditeursInternes) {
      for (let i = 1; i <= nbEditInt; i++) {
        tabTagFormulaire.push('formulaireInt' + indEtape + String(i))
      }
    } else tabTagFormulaire.push('formulaire' + indEtape)
    tabTagFormulaire.forEach((value, index) => {
      const suffixeNumerote = contientEditeursInternes ? suffixe + String(index + 1) : suffixe
      let latex = extraitDeLatexPourMathlive(list, value)
      if (!/edit[123456789]+|edittext[123456789]+|list[123456789]+/g.test(latex) && latex.indexOf('\\editable{}') === -1) {
        errors.push(textes.avert1 + value + textes.avert3)
      }

      // On regarde si les calculs devant être associés aux éditeurs sont bien présents.
      latex = extraitDeLatexPourMathlive(list, value)
      let j = 1
      let st = ''
      if (estEtapeIneq) {
        const nomCalcul1 = 'resolu' + indEtape
        if (!list.containsCalc(nomCalcul1, true)) {
          st += '\n - ' + textes.unCalcNom + nomCalcul1
        }
      } else {
        while (new RegExp('edit' + j + '[\\D]*', 'g').test(latex)) {
          const nomCalcul1 = 'resolu' + suffixeNumerote + String(j)
          if (!list.containsCalc(nomCalcul1, true)) {
            st += '\n - ' + textes.unCalcNom + nomCalcul1
          }
          /* Pas indispensable d’avoir un calcul exact si un calcul resolu est présent
          const nomCalcul2 = 'exact' + suffixeNumerote + String(j)
          if (!list.containsCalc(nomCalcul2, true)) {
            st += '\n - ' + textes.unCalcNom + nomCalcul2
          }
           */
          j++
        }
        j = 1
        while (new RegExp('list' + j + '[\\D]*', 'g').test(latex)) {
          const nom = 'list' + suffixeNumerote + j
          const latex = list.getByTag(nom, true)
          if (latex === null || latex.className !== 'CLatex') {
            st += '\n - ' + textes.unAffLat + nom
          }
          const nomCalc1 = list.containsCalc('resList' + suffixeNumerote + j, true)
          const nomCalc2 = list.containsCalc('repList' + suffixeNumerote + j, true)
          const nomCalc3 = list.containsCalc('exactList' + suffixeNumerote + j, true)
          if ((!nomCalc1) && (!nomCalc2 || !nomCalc3)) {
            st += '\n - ' + textes.unCalcNom + 'resList' + suffixeNumerote + j + ' ' + textes.ou + ' ' +
                textes.deuxCalcNom + ' ' + 'repList' + suffixeNumerote + j + ' ' + textes.et + ' ' + 'exactList' + suffixeNumerote + j
          }
          j++
        }
        j = 0
        let indeb = -1
        while ((indeb = latex.indexOf('\\editable{}', indeb + 1)) !== -1) {
          j++
          const tab = ['repEditable' + suffixeNumerote + j, 'resoluEditable' + suffixeNumerote + j]
          for (const nom of tab) {
            const nomCalc = list.containsCalc(nom, true)
            if (!nomCalc) {
              st += '\n - ' + textes.unCalcNom + nom
            }
          }
        }
      }
      if (st !== '') {
        errors.push(textes.ilManque + st)
      }
    })
  }
  return errors
}

/**
 * Fonction affichant un message d’avertissement au démarrage de la figure s’il manque
 * des éléments pour que la figure soit un exercice multi calculs multi étapes
 * @param {CListeObjets} list
 */
function alertIfErrors (list) {
  const errors = getTabErrors(list)
  if (errors.length > 0) {
    console.warn(errors.join('\n'))
  }
}

/**
 * Valide la figure (avant de la sauvegarder dans les paramètres)
 * S’il y a des errors dans ce que l’on retourne, ce sera affichés et bloquera la sauvegarde (l’éditeur reste ouvert),
 * s’il y a des warning ils sont affichés sans bloquer la sauvegarde
 * @param {MtgApp} mtgApp
 * @return {Promise<{warnings: string[], errors: string[]}>}
 */
async function validator (mtgApp) {
  const errors = getTabErrors(mtgApp.getList())
  return { errors, warnings: [] }
}

const aide = `<ul>La figure doit contenir&nbsp;:
  <li>Un calcul nommé "nbEtapes" contenant un entier au moins égal à 1</li>
  <li>Pour chaque étape N (de 1 à nbEtapes), un affichage Latex nommé enonceN et un autre nommé formulaireN</li>
</ul>`

/**
 * Ajoute l’éditeur mathgraph dans container, la promesse sera résolue avec la figure
 * @param {HTMLElement} container
 * @param {string} fig
 * @return {Promise<string>}
 */
async function initEditor (container, fig) {
  const { default: editor } = await import('./mtgEditor.js')
  return editor(container, { fig, validator, aide })
}

/**
 * section ex_Calc_Multi_Edit_Multi_Etapes
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup () {
    // Attention : indice est en fait l’id de l’éditeur
    const id = this.id
    if (sq.marked[id]) {
      demarqueEditeurPourErreur(id)
      j3pEmpty('correction')
    }
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = parcours.donneesSection['nbEssais' + sq.etape] - sq.numEssai + 1
    if (nbe === 1) afficheMathliveDans('info', 'texteinfo', textes.ilReste + textes.unEssai, { style: { color: '#7F007F' } })
    else afficheMathliveDans('info', 'texteinfo', textes.ilReste + nbe + textes.essais, { style: { color: '#7F007F' } })
  }

  function etablitNombreEtapesEtCoeff () {
    // Dans cette section le nombre d’étapes est dynamique et renvoyé par la figure mtg32
    alertIfErrors(getMtgList())
    sq.nbEtapes = valueOf('nbEtapes', true)
    // On peut spécifier que toutes les listes sont en LaTeX via le paramètre listesLatex ou seulement
    // celles de l’étape n°i si la figure contient un paramètre listesLatexEtape suivi du numéro d’étaoe
    sq.listesEnLatex = []
    for (let i = 1; i <= sq.nbEtapes; i++) {
      sq.listesEnLatex.push(sq.listesLatex || valueOf('listesLatexEtape' + i, true) === 1)
    }
    if (sq.nbEtapes === 1) sq.coefEtape1 = 1
    else {
      sq.coefEtape1 = parcours.donneesSection.coefEtape1
      if (sq.coefEtape1 <= 0 || sq.coefEtape1 >= 1) sq.coefEtape1 = 0.5
    }
    sq.coefEtapeFin = parcours.donneesSection.coefEtapeFin
    if (!sq.coefEtapeFin || (sq.coefEtapeFin < 0) || (sq.coefEtape1 + sq.coefEtapeFin > 1)) sq.coefEtapeFin = -1
    // Dans les premières versions, sq.coefEtapeFin n’existait pas
    if (sq.coefEtapeFin === -1) {
      sq.coefEtapesSuivantes = (1 - sq.coefEtape1) / (sq.nbEtapes - 1)
      sq.coefEtapeFin = sq.coefEtapesSuivantes
    } else {
      if (sq.nbEtapes === 2) sq.coefEtapeFin = 1 - sq.coefEtape1
      else {
        sq.coefEtapesSuivantes = (1 - sq.coefEtape1 - sq.coefEtapeFin) / (sq.nbEtapes - 2)
      }
    }
  }

  // Attention : Les }{ on été remplacés par des }/{
  function traiteIntegrales (st) {
    const faux = { valide: false, st }
    let ind
    // Traitement différent pour mathlive
    while ((ind = st.indexOf('\\int_')) !== -1) {
      const ind1 = getFirstIndexParExcluded(st, '^', ind + 5)
      if (ind1 === -1) return -1
      const a = st.substring(ind + 5, ind1)
      // Si le ^ est suivi d’une parenthèse on recherche la parenthèse fermante associée
      // Sinon c’est que la borne supérieure est formée d’un seul caractère
      let ind2, ind3, indsuiv
      if (st.charAt(ind1 + 1) === '(') {
        ind2 = ind1 + 2
        ind3 = getIndexFermant(st, ind1 + 1)
        indsuiv = ind3 + 1
      } else {
        ind2 = ind1 + 1
        ind3 = ind1 + 2
        indsuiv = ind3
      }
      if (ind3 === 0 || indsuiv >= st.length) return faux
      const b = st.substring(ind2, ind3)
      // Attention : les {}{} ont pu donner des ()/().
      if (st.charAt(indsuiv) === '/') indsuiv++
      // On recherche maintenant le d d’intégration en sautant les éventuelles parenthèses
      let ind4 = getFirstIndexParExcluded(st, '\\differentialD', indsuiv)
      if (ind4 === -1) return faux
      const fonc = st.substring(indsuiv, ind4)
      ind4 += 14
      let ind5
      if (st.charAt(ind4) === '(') {
        ind5 = getIndexFermant(st, ind4)
        ind4++
      } else {
        ind5 = ind4 + 1
      }
      if (ind5 === -1) return faux
      const varfor = st.substring(ind4, ind5) // Le caractère représentant la variable d’intégration
      st = st.substring(0, ind) + 'integrale(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind5)
    }
    return { valide: true, res: st }
  }

  /**
   * Fonction remplaçant dans la chaîne st les indices (caractère _) par des appels de suites
   * ou de fonctions (pour MathGraph32)
   * @param st
   * @returns {string}
   */
  function traiteIndices (st) {
    // Le nom d’une suite peut être une lettre ou un lettre suivi du caractère '
    st = st.replace(/(\w)_(\w+)/g, '$1' + '(' + '$2' + ')')
    st = st.replace(/(\w')_(\w+)/g, '$1' + '(' + '$2' + ')')
    let ind
    while ((ind = st.indexOf('_(')) !== -1) {
      const indpf = getIndexFermant(st, ind + 1)
      st = st.substring(0, ind) + '(' + st.substring(ind + 2, indpf) + ')' + st.substring(indpf + 1)
    }
    return st
  }

  /**
   * Fonction renvoyant un tableau formé des noms de variables utilisées par le calcul contenu dans ch
   * Si le tableau renvoyé a une longueur au moins égale à 2 on considérera que la primitive est incohérente
   * @param {string} ch
   * @return {[]}
   */
  function variablesUtilisees (ch) {
    function utiliseVariable (nomvar) {
      let res
      res = ch.search(new RegExp('\\(' + nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || ch.search(new RegExp(nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || (ch.lastIndexOf(nomvar) === ch.length - 1)
      res = res || ch === nomvar
      return res
    }
    const tab = []
    for (let i = 0; i < sq.variables.length; i++) {
      const car = sq.variables.charAt(i)
      if (utiliseVariable(car)) tab.push(car)
    }
    return tab
  }

  function traitePrimitives (st) {
    const faux = { valide: false, st }
    if (sq.variables === '') return { valide: true, res: st }
    let ind, tab
    while ((ind = st.indexOf('\\left[')) !== -1) {
      // On cherche le \right] correspondant
      const ind1 = getIndexFermant(st, ind + 5)
      if (ind1 === -1) return faux
      const fonc = st.substring(ind + 6, ind1 - 6)
      tab = variablesUtilisees(fonc)
      if (tab.length > 1) return faux
      const varfor = (tab.length === 0) ? sq.variables[0] : tab[0] // On traite le cas d’une fonction constante
      // Le crochet fermant doit être suivi d’un _
      if (st.charAt(ind1 + 1) !== '_') return faux
      const ind2 = getFirstIndexParExcluded(st, '^', ind1 + 2)
      if (ind2 === -1) return faux
      const a = st.substring(ind1 + 2, ind2)
      // Soit le caractère suivant n’est pas une parenthèse et c’est un seul caractère
      // qui est l’argument soit c’est le contenu de la parenthèse
      let ind3, ind4
      if (st.charAt(ind2 + 1) === '(') {
        ind3 = ind2 + 2
        ind4 = getIndexFermant(st, ind3)
      } else {
        ind3 = ind2 + 1
        ind4 = ind2 + 2
      }
      if (ind4 === -1 || ind4 > st.length) return faux
      const b = st.substring(ind3, ind4)
      st = st.substring(0, ind) + 'primitive(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind4 + 1)
    }
    return { valide: true, res: st }
  }

  function traiteMathlive (ch) {
    // La première ligne est nécessaire pour cette section
    ch = ch.replace(/\\R/g, '_R').replace(/\\emptyset/g, '∅')
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    // Si on tape { au clavier dans les résolutions d’inéquations le code LaTeX n’est pas le même que celui
    // obtenu via le bouton MathQuill. La ligne ci-dessous corrige ça.
    ch = ch.replaceAll('lbrace', '{').replaceAll('rbrace', '}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    ch = ch.replace(/\\infty/g, '∞')
    if (ch === '+∞' || ch === '-∞') {
      let ret = 'infinity'
      if (ch === '-∞') ret = '-' + ret
      return { valide: true, res: ret, contientIntegOuPrim: false }
    }
    if (ch.indexOf('∞') !== -1) {
      return { valide: false, res: ch, contientIntegOuPrim: false }
    }
    let { res, valide } = traiteIntegrales(ch)
    if (valide) {
      const resul = traitePrimitives(res)
      res = resul.res
      valide = resul.valide
    }
    if (valide) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', res)// Traitement des multiplications implicites
    ch = traiteIndices(ch)
    const contientIntegOuPrim = (ch.indexOf('integrale') !== -1) || (ch.indexOf('primitive') !== -1)
    return { valide, res: ch, contientIntegOuPrim }
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function marqueEditeurPourErreur (id) {
    // Il faut regarder si l’erreur a été rencontrée dans un placeHolder ou dans un éditeur Mathlive classique
    let editor // Peut être aussi bien un éditeur de texte qu’un éditeur Mathlive
    if (id.includes('ph')) {
      const ind = id.indexOf('ph')
      const idmf = id.substring(0, ind) // L’id de l’éditeur MathLive contenant le placeHolder
      editor = j3pElement(idmf)
      sq.marked[idmf] = true
      editor.setPromptState(id, 'incorrect')
    } else {
      editor = j3pElement(id)
      sq.marked[id] = true
      editor.style.backgroundColor = '#FF9999'
    }
    // On donne à l’éditeur une fonction demarquePourError qui lui permettra de se démarquer quand
    // on utilise le clavier virtuel pour le modifier
    editor.demarqueErreur = () => {
      demarqueEditeurPourErreur(editor.id)
    }
  }

  function demarqueEditeurPourErreur (id) {
    // if ($.isEmptyObject(sq.marked)) return
    if (JSON.stringify(sq.marked) === '{}') return // On teste si l’objet est vide
    sq.marked[id] = false // Par précaution si appel via bouton recopier réponse trop tôt
    // On regarde si l’éditeur où a été trouvée l’erreur est un placeHolder
    const editor = j3pElement(id) // Peut être aussi bien un éditeur Mathlive qu’un éditeur de texte
    if (editor.classList.contains('PhBlock')) {
      editor.getPrompts().forEach((id) => {
        editor.setPromptState(id, 'undefined')
      })
    } else {
      editor.style.backgroundColor = 'white'
    }
  }

  function estEtapeInterne (indiceEtape) {
    return valueOf('etapeInterne' + indiceEtape, true) === 1
  }

  /**
   * Fonction revoyant true si la figure contient un calcul nommé etapeResIneq suivi de indiceEtape
   * auquel cas à cette étape il ne doit y avoir qu’un seul éditeur dans lequel on doit
   * entrer un ensemble de solutions d’inéquation
   * @param indiceEtape
   * @returns {boolean}
   */
  function estEtapeResIneq (indiceEtape) {
    return valueOf('etapeResIneq' + indiceEtape, true) === 1
  }

  /**
   * Fonction renvoyant le nombre d’éditeurs internes si l’étape actuelle est une étape de
   * validation interne contenant des affichages LaTeX de tag formulaireint suivi du numéro d’étape
   * suivi du numéro du formulaire
   * @returns {number}
   */
  function nbEditeursInternes (etape) {
    const list = getMtgList()
    let res = 0
    if (estEtapeInterne(etape)) {
      let i = 1
      let el
      while ((el = list.getByTag('formulaireInt' + etape + String(i), true)) !== null) {
        if (el.className === 'CLatex' && el.pointLie !== null) {
          res++
          i++
        } else return res
      }
    }
    return res
  }

  function estEtapeConstruction (indiceEtape) {
    return valueOf('etapeConstruction' + indiceEtape, true) === 1
  }

  function prepareListeBoutons () {
    sq.listeBoutons = []
    // Tous les boutons sont à false par défaut
    // On commence par les boutons généraux ne servany pas pour une inéquation
    const tabBoutons1 = ['puissance', 'fraction', 'pi', 'racine', 'supegal', 'infegal', 'exp', 'ln', 'log', 'sin', 'cos', 'tan', 'abs', 'conj', 'integ', 'prim', 'indice']
    ;['btnPuis', 'btnFrac', 'btnPi', 'btnRac', 'btnSupegal', 'btnInfegal', 'btnExp', 'btnLn', 'btnLog', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj', 'btnInteg', 'btnPrim', 'btnIndice'].forEach((p, i) => {
      if (sq[p]) sq.listeBoutons.push(tabBoutons1[i])
    })

    if (estEtapeResIneq(sq.etape)) {
      // Si on est à une étape où on doit entrer un ensemble de solutions d’inéquation
      // On ajoute les boutons ad hoc au clavier virtuel
      const tabNewButtons = ['R', 'union', 'bracket']
      ;['btnR', 'btnUnion', 'btnAcc'].forEach((p, i) => {
        if (sq[p]) sq.listeBoutons.push(tabNewButtons[i])
      })
      sq.listeBoutons.push('inf') // Figure toujours dans les boutons pour une inéquation
      sq.listeBoutons.push('vide') // Idem
    } else {
      // Le bouton inf peut servir dans un éditeur de formule quand on demande une limite
      if (sq.btnInf) sq.listeBoutons.push('inf')
    }
    sq.hasBoutonsMathQuill = sq.listeBoutons.length > 0
  }

  function validationEditeurs () {
    sq.signeEgalManquant = false
    sq.signeInegManquant = false
    const listeIdEditeursAvecErreur = []
    function verifieEgalEtIneg (ind, chEstEgal, chestInegal, repcalcul, id) {
      let valideEq = true
      if (sq[chEstEgal + ind]) {
        const indeq1 = repcalcul.indexOf('=')
        const indeq2 = repcalcul.indexOf('=', indeq1 + 1)
        if (indeq1 === -1 || (indeq1 !== -1 && indeq2 !== -1)) {
          valideEq = false
          sq.signeEgalManquant = true
          marqueEditeurPourErreur(id)
          listeIdEditeursAvecErreur.push(id)
        }
      }
      if (sq[chestInegal + ind] && !/[<>]/.test(repcalcul)) {
        valideEq = false
        sq.signeInegManquant = true
        marqueEditeurPourErreur(id)
        listeIdEditeursAvecErreur.push(id)
      }
      return valideEq
    }
    const nbEditInt = nbEditeursInternes(sq.etape)
    if (estEtapeConstruction(sq.etape) || (estEtapeInterne(sq.etape) && nbEditInt === 0)) return true
    // Si on est dans une étape où on doit entrer un ensemble des solutions d’inéquations
    // il doit y avoir un seul éditeur de formule
    if (estEtapeResIneq(sq.etape)) {
      sq.vide = false
      let res = true
      const rep = getMathliveValue('expressioninputmq1')
      if (rep === '') {
        res = false
      } else {
        const chcalcul = traiteMathlive(rep).res
        if (chcalcul.indexOf('∅') !== -1) {
          res = chcalcul.length === 1
          sq.vide = res
        } else {
          const valide = validationEnsembleSol(chcalcul)
          if (!valide) res = false
        }
      }
      if (!res) {
        marqueEditeurPourErreur('expressioninputmq1')
        setTimeout(function () {
          j3pElement('expressioninputmq1').focus()
        })
      }
      return res
    }

    if (nbEditInt > 0) {
      for (let i = 1; i <= nbEditInt; i++) {
        for (let ind = 1; ind <= sq['nbCalc' + i]; ind++) {
          const id = 'repEdit' + sq.etape + String(i) + 'inputmq' + ind
          const rep = getMathliveValue(id)
          const resul = traiteMathlive(rep)
          const repcalcul = resul.res
          if (rep === '') {
            marqueEditeurPourErreur(id)
            listeIdEditeursAvecErreur.push(id)
          } else {
            const valide = resul.valide && getMtgList().syntaxValidation('repInt' + String(sq.etape) + i + String(ind), repcalcul, false)
            if (!valide) {
              marqueEditeurPourErreur(id)
              listeIdEditeursAvecErreur.push(id)
            }
          }
        }
        let indEditable = 0
        let indPhBlock = 0
        const span = j3pElement('repEdit' + sq.etape + String(i))
        // span est le span qui contient le formulaire affiché par Mathlive
        span.querySelectorAll('.PhBlock').forEach((mf) => {
          indPhBlock++
          for (let k = 0; k < mf.getPrompts().length; k++) {
            indEditable++
            const idmf = 'repEdit' + sq.etape + String(i) + 'PhBlock' + indPhBlock
            const id = idmf + 'ph' + indEditable
            const rep = getMathliveValue(idmf, { placeholderId: id })
            if (rep === '') {
              marqueEditeurPourErreur(id)
              listeIdEditeursAvecErreur.push(id)
            } else {
              const resul = traiteMathlive(rep)
              const repcalcul = resul.res
              // Ligne suivante dernier paramètre à false car addImplicitmult a déjà été appelé par traiteMathQuill
              // et si une réponse est +infini ou -infini elle ne serait pas acceptée
              const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'repEditableInt' + String(sq.etape) + i + String(indEditable), repcalcul, false)
              if (!valide) {
                marqueEditeurPourErreur(id)
                listeIdEditeursAvecErreur.push(id)
              }
            }
          }
        })
        for (let ind = 1; ind <= sq['nbTextEdit' + i]; ind++) {
          const id = 'repEdit' + sq.etape + String(i) + 'input' + ind
          const rep = j3pValeurde(id)
          if (rep === '') {
            marqueEditeurPourErreur(id)
            listeIdEditeursAvecErreur.push(id)
          }
        }
      }
    } else {
      for (let ind = 1; ind <= sq.nbCalc; ind++) {
        const id = 'expressioninputmq' + ind
        const rep = getMathliveValue(id)
        if (rep === '') {
          marqueEditeurPourErreur(id)
          listeIdEditeursAvecErreur.push(id)
        } else {
          const resul = traiteMathlive(rep)
          const repcalcul = resul.res
          // On regarde si l’éditeur n° ind doit contenir une égalité ou non
          const valideEq = verifieEgalEtIneg(ind, 'estEgalite', 'estInegalite', repcalcul, id)
          if (valideEq) {
            // Ligne suivante dernier paramètre à false car addImplicitmult a déjà été appelé par traiteMathQuill
            // et si une réponse est +infini ou -infini elle ne serait pas acceptée
            const valide = resul.valide && getMtgList().syntaxValidation('rep' + String(sq.etape) + ind, repcalcul, false)
            if (!valide) {
              marqueEditeurPourErreur(id)
              listeIdEditeursAvecErreur.push(id)
            }
          }
        }
      }
      let indEditable = 0
      let indPhBlock = 0
      const span = j3pElement('expression')
      span.querySelectorAll('.PhBlock').forEach((mf) => {
        indPhBlock++
        for (let i = 0; i < mf.getPrompts().length; i++) {
          indEditable++
          const idmf = 'expressionPhBlock' + indPhBlock
          const id = idmf + 'ph' + indEditable
          const rep = getMathliveValue(idmf, { placeholderId: id })
          if (rep === '') {
            marqueEditeurPourErreur(id)
            listeIdEditeursAvecErreur.push(id)
          } else {
            const resul = traiteMathlive(rep)
            const repcalcul = resul.res
            const valideEq = verifieEgalEtIneg(indEditable, 'estEgaliteEditable', 'estInegaliteEditable', repcalcul, id)
            if (valideEq) {
              const resul = traiteMathlive(rep)
              const repcalcul = resul.res
              // Ligne suivante dernier paramètre à false car addImplicitmult a déjà été appelé par traiteMathQuill
              // et si une réponse est +infini ou -infini elle ne serait pas acceptée
              const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'repEditable' + String(sq.etape) + indEditable, repcalcul, false)
              if (!valide) {
                marqueEditeurPourErreur(id)
                listeIdEditeursAvecErreur.push(id)
              }
            }
          }
        }
      })
      for (let ind = 1; ind <= sq.nbTextEdit; ind++) {
        const id = 'expressioninput' + ind
        const rep = j3pValeurde(id)
        if (rep === '') {
          marqueEditeurPourErreur(id)
          listeIdEditeursAvecErreur.push(id)
        }
      }
    }
    if (listeIdEditeursAvecErreur.length) {
      setFocusById(listeIdEditeursAvecErreur[0])
      return false
    }
    if (nbEditInt > 0) {
      for (let i = 1; i <= nbEditInt; i++) {
        for (let ind = 1; ind <= sq['nbList' + i]; ind++) {
          const resliste = sq.listesEnLatex[sq.etape - 1]
            ? sq['listeDeroulante' + i + String(ind)].getReponseIndex()
            : j3pElement('repEdit' + sq.etape + String(i) + 'liste' + ind).selectedIndex
          if (resliste <= 0) return false
        }
      }
      return true
    } else {
      for (let ind = 1; ind <= sq.nbList; ind++) {
        const resliste = sq.listesEnLatex[sq.etape - 1] ? sq['listeDeroulante' + ind].getReponseIndex() : j3pElement('expression' + 'liste' + ind).selectedIndex
        if (resliste === 0) return false
      }
      return true
    }
  }

  /**
   * Fonction renvoyant un tableau formé de deux tableaux réordonnés suivant les valeurs du premier tableau
   * Les deux tableaux doivent avoir la même dimension
   * @param tabval
   * @param tabfor
   */
  function ordonne (tabval, tabfor) {
    let i, j
    const tabvalaux = []
    const tabforaux = []
    const tabvalres = []
    const tabforres = []
    for (i = 0; i < tabval.length; i++) {
      tabvalaux.push(tabval[i])
      tabforaux.push(tabfor[i])
    }
    while (tabvalaux.length >= 1) {
      let valmin = tabvalaux[0]
      let indmin = 0
      for (j = 1; j < tabvalaux.length; j++) {
        if (tabvalaux[j] < valmin) {
          indmin = j
          valmin = tabvalaux[j]
        }
      }
      const val = tabvalaux[indmin]
      const form = tabforaux[indmin]
      tabvalres.push(val)
      tabforres.push(form)
      tabvalaux.splice(indmin, 1)
      tabforaux.splice(indmin, 1)
    }
    return [tabvalres, tabforres]
  }

  function validationIntervalle (ch, indeb) {
    let indcr
    let valGauche
    let valDroite
    const listePourCalc = sq.listePourCalc
    const car = ch.charAt(indeb)
    if (car !== '[' && car !== ']' && car !== '\\') return -1
    const crochet1 = ch.charAt(indeb)
    if (crochet1 === '\\') { // Traitement des valeurs isolées entre accolades
      indcr = getIndexFermant(ch, indeb + 1) // Le \ est forcémment suivi d’une (
      if (indcr === -1) return -1
      const st = ch.substring(indeb + 2, indcr)
      const tab = st.split(';')
      for (let j = 0; j < tab.length; j++) {
        const ch2 = tab[j]
        if (ch2 === '') return -1
        if (!listePourCalc.verifieSyntaxe(ch2)) return -1
        sq.bornesGauches.push(ch2)
        listePourCalc.giveFormula2('x', ch2)
        listePourCalc.calculateNG()
        valGauche = listePourCalc.valueOf('x')
        sq.valBornesGauches.push(valGauche)
        sq.valBornesDroites.push(valGauche)
        sq.bornesDroites.push(ch2)
        sq.bornesGauchesFermees.push(true)
        sq.bornesGauchesInf.push(false)
        sq.bornesDroitesFermees.push(true)
        sq.bornesDroitesInf.push(false)
      }
    } else {
      const indpv = ch.indexOf(';', indeb + 1)
      const ind1 = ch.indexOf('[', indeb + 1)
      const ind2 = ch.indexOf(']', indeb + 1)
      if (indpv === -1 || (ind1 === -1 && ind2 === -1)) return -1
      if (ind1 === -1) indcr = ind2
      else if (ind2 === -1) indcr = ind1
      else indcr = Math.min(ind1, ind2)
      if (indcr < indpv) return -1
      const crochet2 = ch.charAt(indcr)
      const left = ch.substring(indeb + 1, indpv)
      const right = ch.substring(indpv + 1, indcr)
      if (left === '' || right === '') return -1
      sq.bornesGauches.push(left)
      sq.bornesDroites.push(right)
      const borneGaucheInf = left === '-∞'
      const borneDroiteInf = right === '+∞'
      sq.bornesGauchesInf.push(borneGaucheInf)
      sq.bornesDroitesInf.push(borneDroiteInf)
      if (borneGaucheInf && borneDroiteInf) sq.reponseContientR = true
      if ((!borneGaucheInf && !listePourCalc.verifieSyntaxe(left)) || (!borneDroiteInf && !listePourCalc.verifieSyntaxe(right))) return -1
      if (!borneGaucheInf) {
        listePourCalc.giveFormula2('x', left)
        listePourCalc.calculateNG()
        valGauche = listePourCalc.valueOf('x')
      }
      if (!borneDroiteInf) {
        listePourCalc.giveFormula2('x', right)
        listePourCalc.calculateNG()
        valDroite = listePourCalc.valueOf('x')
      }
      if ((!borneGaucheInf && !borneDroiteInf) && (valGauche > valDroite)) return -1
      sq.valBornesGauches.push(valGauche)
      sq.valBornesDroites.push(valDroite)
      sq.bornesGauchesFermees.push(!borneGaucheInf && (crochet1 === '['))
      sq.bornesDroitesFermees.push(!borneDroiteInf && (crochet2 === ']'))
    }
    if (indcr < ch.length - 1) {
      if (ch.charAt(indcr + 1) !== '|') return -1
      else return indcr + 2
    } else { return indcr + 1 }
  }

  /**
   * Fonction renvoyant un tableau formé des valeurs isolées distinctes trouvées dans la réponse de l'élève
   * @returns {[number]}
   */
  function valeursIsoleesDansRep () {
    const tab = [] // Tableau qui contiendra les valeurs isolées
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (!sq.bornesGauchesInf[i] && !sq.bornesDroitesInf[i] && (sq.bornesGauches[i] === sq.bornesDroites[i])) {
        const val = sq.bornesGauches[i]
        // On regarde si val n'est pas dans un intervalle de la réunion proposée par l'élève et n'est pas déjà dans tab
        if (!tab.includes(val) && !appartientAuMoinsUnInt(val, true) && !estBorneOuverte(val)) tab.push(val)
      }
    }
    return tab
  }

  /**
   * Function renvoyant false si la solution attendue comporte des solutions isolées
   * alors que la réponse n'en contient pas le même nombre avec les valeurs isolées attendues
   * Si on renvoie false, on met false dans sq.resolu et sq.exact
   * Utilisé dans la fonction validation
   * @returns {boolean}
   */
  function valeursIsoleesCorrectes () {
    const nbSolIsolees = valueOf('nbSolIsolees' + sq.etape)
    if (nbSolIsolees > 0) {
      const valeursIsRep = valeursIsoleesDansRep()
      if (valeursIsRep.length !== nbSolIsolees) {
        sq.resolu = false
        sq.exact = false
        return false
      } else {
        // On regarde si chacune des valeurs isolées de la réponse est un bien une solution isolée
        for (let i = 0; i < valeursIsRep.length; i++) {
          giveFormula2('xTest', valeursIsRep[i])
          calculate(false)
          if (valueOf('estSolIsolee' + sq.etape) !== 1) {
            sq.resolu = false
            sq.exact = false
            return false
          }
        }
        return true
      }
    }
    return true
  }

  /**
   * Fonction renvoyant true, si, à 10^-10 près, le nombre xtest est une des bornes fermées de l’intervalla
   * ou la réunion d’intervalles proposées par l’élève
   * @param xtest
   */
  function estBorne (xtest) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if ((!sq.bornesGauchesInf[i] && ((sq.bornesGauchesFermees[i] && Math.abs(xtest - sq.valBornesGauches[i]) <= eps))) ||
        (!sq.bornesDroitesInf[i] && (sq.bornesDroitesFermees[i] && Math.abs(xtest - sq.valBornesDroites[i]) <= eps))) return true
    }
    return false
  }

  /**
   * Fonction renvoyant true si xTest est une borne gauche fermée d’un intervalle autre que l’intervalle d’indice indIntervalle
   * @param xtest
   * @param indIntervalle
   * @return {boolean}
   */
  function estBorneFermeeGaucheAutreIntQue (xtest, indIntervalle) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== indIntervalle) {
        if (!sq.bornesGauchesInf[i] && sq.bornesGauchesFermees[i] && Math.abs(xtest - sq.valBornesGauches[i]) <= eps) return true
      }
    }
    return false
  }

  /**
   * Fonction renvoyant true si le nombre xtest est (à epsilon près) une borne ouverte ou droite d'un des intervalles de la réponse
   * @param {number} xtest
   * @returns {boolean}
   */
  function estBorneOuverte (xtest) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (!sq.bornesGauchesInf[i] && !sq.bornesGauchesFermees[i] && (Math.abs(xtest - sq.valBornesGauches[i]) <= eps)) return true
    }
    for (let i = 0; i < sq.bornesDroites.length; i++) {
      if (!sq.bornesDroitesInf[i] && !sq.bornesDroitesFermees[i] && (Math.abs(xtest - sq.valBornesDroites[i]) <= eps)) return true
    }
    return false
  }

  /**
   * Fonction renvoyant true si xTest est une borne gauche ouverte d’un intervalle autre que l’intervalle d’indice indIntervalle
   * @param xtest
   * @param indIntervalle
   * @return {boolean}
   */
  function estBorneOuverteGaucheAutreIntQue (xtest, indIntervalle) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== indIntervalle) {
        if (!sq.bornesGauchesInf[i] && !sq.bornesGauchesFermees[i] && Math.abs(xtest - sq.valBornesGauches[i]) <= eps) return true
      }
    }
    return false
  }

  /**
   * Fonction renvoyant true si xTest est une borne droite fermée d’un intervalle autre que l’intervalle d’indice indIntervalle
   * @param xtest
   * @param indIntervalle
   * @return {boolean}
   */
  function estBorneFermeeDroiteAutreIntQue (xtest, indIntervalle) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== indIntervalle) {
        if (!sq.bornesDroitesInf[i] && sq.bornesDroitesFermees[i] && Math.abs(xtest - sq.valBornesDroites[i]) <= eps) return true
      }
    }
    return false
  }

  /**
   * Fonction renvoyant true si xTest est une borne droite ouverte d’un intervalle autre que l’intervalle d’indice indIntervalle
   * @param xtest
   * @param indIntervalle
   * @return {boolean}
   */
  function estBorneOuverteDroiteAutreIntQue (xtest, indIntervalle) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== indIntervalle) {
        if (!sq.bornesDroitesInf[i] && !sq.bornesDroitesFermees[i] && Math.abs(xtest - sq.valBornesDroites[i]) <= eps) return true
      }
    }
    return false
  }

  function reponseIncluseDansSol () {
    let xTest
    let xTest2
    let res, estBorneIsolee
    const eps = 0.0000000001
    const eps2 = 0.00000001
    res = valueOf('toutReelSol' + sq.etape, true)
    if (res === 1) return true
    // On commence par regarder si un des intervalles de la réunion proposée contient une borne limitante
    // de l’intervalle solution.
    // Pour cela on donne à la fonction fonctionTest de la figure une formule et on regarde si cette formule
    // est vérifiée par une borne limitante de la figure (hors valeur exclue isolée) en interrogeant
    // la valeur du calcul contientBorne de la figure
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      let form = ''
      if (!sq.bornesGauchesInf[i]) form += sq.valBornesGauches[i] + '<x'
      if (!sq.bornesDroitesInf[i]) {
        if (form !== '') form += '&'
        form += 'x<' + sq.valBornesDroites[i]
      }
      if (form !== '') {
        giveFormula2('fonctionTest', form)
        calculate(false)
        if (valueOf('contientBorne' + sq.etape, true) === 1) return false
      }
    }
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      // On étudie le cas d’une valeur isolée représentée par un intervalle dont les bornes sont confondues
      if (sq.bornesGauchesFermees[i] && sq.bornesDroitesFermees[i] && (sq.valBornesGauches[i] === sq.valBornesDroites[i])) {
        xTest = sq.valBornesGauches[i]
        giveFormula2('xTest', xTest.toFixed(16))
        calculate(false)
        if ((valueOf('estBorneFermee' + sq.etape, true) !== 1) &&
          (valueOf('estSolution' + sq.etape, true) !== 1)) return false
      } else {
        if (sq.bornesGauchesInf[i]) {
          res = valueOf('moinsInfSolution' + sq.etape, true)
          if (res !== 1) return false
        } else {
          const gauche = sq.valBornesGauches[i]
          if (!sq.bornesGauchesFermees[i]) {
            if (!appartientAuMoinsUnInt(gauche)) {
              xTest = gauche
              giveFormula2('xTest', xTest.toFixed(16))
              calculate(false)
              res = valueOf('estBorneFermee' + sq.etape, true)
              if (res === 1) return false
              estBorneIsolee = (valueOf('estBorneIsolee' + sq.etape, true) === 1)
              if (!estBorneIsolee) {
                xTest2 = gauche - eps
                giveFormula2('xTest', xTest2.toFixed(16))
                calculate(false)
                res = valueOf('estSolution' + sq.etape, true)
                if (res === 1 && !estBorneFermee(xTest)) return false
              }
            }
          }
          if (estBorne(gauche)) xTest = gauche; else xTest = gauche + eps
          giveFormula2('xTest', xTest.toFixed(16))
          calculate(false)
          res = valueOf('estSolution' + sq.etape, true)
          if (res !== 1) return false
          xTest = gauche - eps2
          if (!appartientAuMoinsUnInt(xTest)) {
            giveFormula2('xTest', xTest.toFixed(16))
            calculate(false)
            res = valueOf('estSolution' + sq.etape, true)
            if (res === 1) return false
          }
        }
        if (sq.bornesDroitesInf[i]) {
          res = valueOf('plusInfSolution' + sq.etape, true)
          if (res !== 1) return false
        } else {
          const droite = sq.valBornesDroites[i]
          if (!sq.bornesDroitesFermees[i]) {
            if (!appartientAuMoinsUnInt(droite)) {
              xTest = droite
              giveFormula2('xTest', xTest.toFixed(16))
              calculate(false)
              res = valueOf('estBorneFermee' + sq.etape, true)
              if (res === 1) return false
              estBorneIsolee = (valueOf('estBorneIsolee' + sq.etape, true) === 1)
              if (!estBorneIsolee) {
                xTest2 = droite + eps
                giveFormula2('xTest', xTest2.toFixed(16))
                calculate(false)
                res = valueOf('estSolution' + sq.etape, true)
                if (res === 1 && !estBorneFermee(xTest)) return false
              }
            }
          }
          if (estBorne(droite)) xTest = droite; else xTest = droite - eps
          giveFormula2('xTest', xTest.toFixed(16))
          calculate(false)
          res = valueOf('estSolution' + sq.etape, true)
          if (res !== 1) return false
          xTest = droite + eps2
          if (!appartientAuMoinsUnInt(xTest)) {
            giveFormula2('xTest', xTest.toFixed(16))
            calculate(false)
            res = valueOf('estSolution' + sq.etape, true)
            if (res === 1) return false
          }
        }
      }
    }
    return true
  }

  /**
   * Fonction renvoyant true si au moins un des intervalles de la réunion proposée par l'élève contient la valeur x
   * @param x
   * @param bNonSingleton si true, on ne teste pas les intervalles singletons du type [a; a]
   * @returns {boolean}
   */
  function appartientAuMoinsUnInt (x, bNonSingleton = false) {
    if (sq.reponseContientR) return true
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (sq.bornesGauchesInf[i]) {
        if (sq.bornesDroitesInf[i]) { return true } else {
          if (sq.bornesDroitesFermees[i]) {
            if (x <= sq.valBornesDroites[i]) return true
          } else {
            if (x < sq.valBornesDroites[i]) return true
          }
        }
      } else {
        if (sq.bornesDroitesInf[i]) {
          if (sq.bornesGauchesFermees[i]) {
            if (x >= sq.valBornesGauches[i]) return true
          } else {
            if (x > sq.valBornesGauches[i]) return true
          }
        } else {
          if (sq.bornesGauchesFermees[i]) {
            if (sq.bornesDroitesFermees[i]) {
              if ((x >= sq.valBornesGauches[i]) && (x <= sq.valBornesDroites[i])) {
                if (!bNonSingleton) return true
                else {
                  if (sq.valBornesGauches[i] !== sq.valBornesDroites[i]) return true
                }
              }
            } else {
              if ((x >= sq.valBornesGauches[i]) && (x < sq.valBornesDroites[i])) return true
            }
          } else {
            if (sq.bornesDroitesFermees[i]) {
              if ((x > sq.valBornesGauches[i]) && (x <= sq.valBornesDroites[i])) return true
            } else {
              if ((x > sq.valBornesGauches[i]) && (x < sq.valBornesDroites[i])) return true
            }
          }
        }
      }
    }
    return false
  }

  function estBorneFermee (x) {
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (!sq.bornesGauchesInf[i] && sq.bornesGauches[i] === x) return true
      if (!sq.bornesDroitesInf[i] && sq.bornesDroites[i] === x) return true
    }
    return false
  }

  /**
   * Fonction renvoyant true si x appratient à au moins un intervalle autre que celui d’indice ind
   * @param x
   * @param indIntervalle
   * @return {boolean}
   */
  function appartientAuMoinsUnIntAutreQue (x, indIntervalle) {
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== indIntervalle) {
        if (sq.bornesGauchesInf[i]) {
          if (sq.bornesDroitesInf[i]) { return true } else {
            if (sq.bornesDroitesFermees[i]) {
              if (x <= sq.valBornesDroites[i]) return true
            } else {
              if (x < sq.valBornesDroites[i]) return true
            }
          }
        } else {
          if (sq.bornesDroitesInf[i]) {
            if (sq.bornesGauchesFermees[i]) {
              if (x >= sq.valBornesGauches[i]) return true
            } else {
              if (x > sq.valBornesGauches[i]) return true
            }
          } else {
            if (sq.bornesGauchesFermees[i]) {
              if (sq.bornesDroitesFermees[i]) {
                if ((x >= sq.valBornesGauches[i]) && (x <= sq.valBornesDroites[i])) return true
              } else {
                if ((x >= sq.valBornesGauches[i]) && (x < sq.valBornesDroites[i])) return true
              }
            } else {
              if (sq.bornesDroitesFermees[i]) {
                if ((x > sq.valBornesGauches[i]) && (x <= sq.valBornesDroites[i])) return true
              } else {
                if ((x > sq.valBornesGauches[i]) && (x < sq.valBornesDroites[i])) return true
              }
            }
          }
        }
      }
    }
    return false
  }

  function validationEnsembleSol (ch) {
    let i, j
    const listePourCalc = sq.listePourCalc
    sq.reponseContientR = false // Sera true si un des intervalles de la réunion est ]-∞;+∞[
    if (ch === '_R' || ch === ']-∞;+∞[') {
      sq.reponseEstR = true
      return true
    } else { sq.reponseEstR = false }
    if (ch.charAt(ch.length - 1) === '|') return false
    sq.borneFermeeSurInf = false
    if (ch.indexOf('[∞') !== -1 || ch.indexOf('∞]') !== -1 || ch.indexOf('[-∞') !== -1) {
      if (sq.erreurSiBorneFermeeSurInfini) {
        sq.borneFermeeSurInf = true
        return true // Pour compter comme une erreur ce type de faute de syntaxe
      }
      return false
    }
    sq.bornesGauches = []
    sq.valBornesGauches = []
    sq.bornesGauchesFermees = []
    sq.bornesDroites = []
    sq.valBornesDroites = []
    sq.bornesDroitesFermees = []
    sq.bornesGauchesInf = []
    sq.bornesDroitesInf = []
    // On regarde s la réponse a été entrée sous la forme R-{}
    if (ch.indexOf('_R-\\(') === 0) {
      const indpf = getIndexFermant(ch, 4)
      if (indpf < ch.length - 1) return false // On ne permet que R-{} sans autre opérateur
      const chaux = ch.substring(5, indpf)
      if (chaux === '') return false
      const tabcal = []
      const tabval = []
      const tab = chaux.split(';')
      for (i = 0; i < tab.length; i++) {
        const ch2 = tab[i]
        if (ch2 === '') return -1
        if (!listePourCalc.verifieSyntaxe(ch2)) return false
        listePourCalc.giveFormula2('x', ch2)
        listePourCalc.calculateNG()
        tabcal.push(ch2)
        tabval.push(listePourCalc.valueOf('x'))
      }
      // On ordonne les valeurs trouvées entre les accolades
      const res = ordonne(tabval, tabcal)
      const tabvalord = res[0]
      const tabforord = res[1]
      // On regarde s’il, y a deux valeurs ou plus répétées. Si oui on refuse
      for (i = 0; i < tabvalord.length; i++) {
        for (j = i + 1; j < tabvalord.length - 1; j++) {
          if (tabvalord[i] === tabvalord[j]) return false
        }
      }
      for (i = 0; i < tabvalord.length; i++) {
        if (i === 0) { // Intervalle de - ∞ à la plus petite valeur
          sq.bornesGauchesInf.push(true)
          sq.bornesGauches.push('0') // Valeur sans importance
          sq.bornesGauchesFermees.push(false)
          sq.valBornesGauches.push(0)
          sq.bornesDroitesInf.push(false)
          sq.bornesDroitesFermees.push(false)
          sq.bornesDroites.push(tabforord[0])
          sq.valBornesDroites.push(tabvalord[0])
        } else {
          sq.bornesGauchesInf.push(false)
          sq.bornesGauches.push(tabforord[i - 1])
          sq.bornesGauchesFermees.push(false)
          sq.valBornesGauches.push(tabvalord[i - 1])
          sq.bornesDroitesInf.push(false)
          sq.bornesDroitesFermees.push(false)
          sq.bornesDroites.push(tabforord[i])
          sq.valBornesDroites.push(tabvalord[i])
        }
        if (i === tabvalord.length - 1) {
          sq.bornesGauchesInf.push(false)
          sq.bornesGauches.push(tabforord[i])
          sq.bornesGauchesFermees.push(false)
          sq.valBornesGauches.push(tabvalord[i])
          sq.bornesDroitesInf.push(true)
          sq.bornesDroitesFermees.push(false)
          sq.bornesDroites.push('0') // Valeru sans importance
          sq.valBornesDroites.push(0)
        }
      }
    } else {
      let ind = 0
      while (ind !== -1 && ind <= ch.length - 1) {
        ind = validationIntervalle(ch, ind)
        if (ind === -1) return false
      }
    }
    // On crée maintenant une chaîne de caractères dont le texte est celui d’une fonction mtg32 testant si un x est solution
    let st = ''
    for (i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== 0) st += '|'
      if (sq.bornesGauchesInf[i]) {
        if (sq.bornesDroitesInf[i]) {
          st += '(0=0)'
        } else { // N’importe quel test vrai fait l’affaire
          st += '(' + sq.bornesDroites[i]
          st += (sq.bornesDroitesFermees[i] ? '>=' : '>') + 'x)'
        }
      } else {
        if (sq.bornesDroitesInf[i]) {
          st += '(' + sq.bornesGauches[i]
          st += (sq.bornesGauchesFermees[i] ? '<=' : '<') + 'x)'
        } else {
          if (sq.bornesGauchesFermees[i] && sq.bornesDroitesFermees[i] && sq.bornesGauches[i] === sq.bornesDroites[i]) {
            st += '(x=' + sq.bornesGauches[i] + ')'
          } else {
            st += '(' + sq.bornesGauches[i]
            st += (sq.bornesGauchesFermees[i] ? '<=' : '<') + 'x)&('
            st += sq.bornesDroites[i]
            st += (sq.bornesDroitesFermees[i] ? '>=' : '>') + 'x)'
          }
        }
      }
    }
    sq.chaineFonctionTest = st
    sq.chaineFonctionTestBornesFermees = st.replace(/<([^=])/g, '<=$1').replace(/>([^=])/g, '>=$1') // Pour tester une réponse presque exacte avec une confusion sur les bornes
    // Une autre chaîne qui sera attribuée à repPourBornes de la figure
    st = ''
    for (i = 0; i < sq.bornesGauches.length; i++) {
      if (sq.bornesGauchesFermees[i] && sq.bornesDroitesFermees[i] && sq.bornesGauches[i] === sq.bornesDroites[i]) {
        st += '(x=' + sq.bornesGauches[i] + ')'
      } else {
        if (!sq.bornesGauchesInf[i]) {
          st += '(' + sq.bornesGauches[i]
          st += (sq.bornesGauchesFermees[i] ? '<=' : '<') + 'x)&('
        } else { st += '(' }
        if (!sq.bornesDroitesInf[i]) {
          st += sq.bornesDroites[i]
          st += (sq.bornesDroitesFermees[i] ? '>=' : '>') + 'x)'
        } else { st += '0=0)' }
      }
      if (sq.bornesGauchesFermees[i]) st += '|' + 'zeroBorne(x - (' + sq.bornesGauches[i] + '))'
      if (sq.bornesDroitesFermees[i]) st += '|' + 'zeroBorne(x - (' + sq.bornesDroites[i] + '))'
      if (i !== sq.bornesGauches.length - 1) st += '|'
    }
    sq.chaineFonctionTestPourBornes = st
    // On doit savoir si la réponse contient R pour cela elle doit contenir au moins un intervalle non borné à gauche,
    // au moins un intervalle non borné à droite
    if (!sq.reponseContientR) {
      let repContientR = sq.bornesGauchesInf.length > 0 && sq.bornesDroitesInf.length > 0
      if (repContientR) {
        for (i = 0; (i < sq.bornesGauches.length) && repContientR; i++) {
          if (!sq.bornesGauchesInf[i]) {
            if (sq.bornesGauchesFermees[i]) {
              repContientR = repContientR && (estBorneOuverteDroiteAutreIntQue(sq.bornesGauches[i], i) || appartientAuMoinsUnIntAutreQue(sq.bornesGauches[i], i))
            } else {
              repContientR = repContientR && (estBorneFermeeDroiteAutreIntQue(sq.bornesGauches[i], i) || appartientAuMoinsUnIntAutreQue(sq.bornesGauches[i], i))
            }
          }
          if (!sq.bornesDroitesInf[i]) {
            if (sq.bornesDroitesFermees[i]) {
              repContientR = repContientR && (estBorneOuverteGaucheAutreIntQue(sq.bornesDroites[i], i) || appartientAuMoinsUnIntAutreQue(sq.bornesDroites[i], i))
            } else {
              repContientR = repContientR && (estBorneFermeeGaucheAutreIntQue(sq.bornesDroites[i], i) || appartientAuMoinsUnIntAutreQue(sq.bornesDroites[i], i))
            }
          }
        }
      }
      sq.reponseContientR = repContientR
    }
    return true
  }

  /**
   * Fonction renvoyant le nombre d’éditeurs contenus dans la chaîne ch et dont le type est contenu
   * dans la chaîne type
   * @param {string} type chaine de caractères qui peut être la chaîne 'edit' pour un éditeur MathQuill,
   * 'list' pour une liste déroulante et 'string' pour une chaîne de caractères.
   * Si, par exemple, il y a trois éditeurs MathQuill la chaîne doit contenir edit1, edit2 et edit3.
   * @param {string} ch
   */
  function nbEdit (type, ch) {
    let res = 0
    let i = 1
    while (ch.indexOf(type + i) !== -1) {
      res++
      i++
    }
    return res
  }

  /**
   * Fonction renvoyant le nombre de `\editable{} contenus dans la chaîne ch
   * @param ch
   * @return {string|number}
   */
  function nbEditable (ch) {
    let res = 0
    let i = ch.indexOf('\\editable{')
    if (i === -1) return 0
    while (i !== -1) {
      res++
      ch = ch.substring(i + 10)
      i = ch.indexOf('\\editable{')
    }
    return res
  }

  /**
   * Fonction recherchant un affichage LaTeX dont le tag est :
   * Si on est dans une étape qui n’est pas une étape de validation interne, list suivi du numéro de l’étape
   * suivi de indListe (numéro de la liste dans l’unique formulaire)
   * Si on est dans une étape de validation interne avec des éditeurs MathQuill, list suivi du numéro de l’étape
   * suivi de indPourEtapeInterne (numéro du formulaire sous forme de chaîne de caractère) et suivi de indListe
   * @param indListe
   * @param indPourEtapeInterne
   * @returns {{texte: string[]}|*[]}
   */
  function extraitDonneesListe (indListe, indPourEtapeInterne) {
    const list = getMtgList()
    const nameList = indPourEtapeInterne === '' ? 'list' : 'listInt'
    // On cherche dans la liste le LaTeX qui a pour tag 'list' suivi du chiffre indListe
    const latex = list.getByTag(nameList + String(sq.etape) + indPourEtapeInterne + indListe, true)
    if (latex === null || latex.className !== 'CLatex') return []
    // On met dans le tableau qu’on va renvoyer le contenu des \text{} successifs rencontrés dans le LaTeX
    const chaineLatex = cleanLatexForMl(latex.chaineLatex, { keepText: true })
    const res = ['...']
    let i = chaineLatex.indexOf('\\text{')
    while (i !== -1) {
      const idaf = getIndexFermant(chaineLatex, i + 5)
      if (idaf === -1) break
      else res.push(chaineLatex.substring(i + 6, idaf))
      i = chaineLatex.indexOf('\\text{', idaf + 1)
    }
    // Amélioration : Si la figure contient un calcul nommé listaleat suivi du numéro indListe et qui vaut 1,
    // alors on mélange les items de la liste au hasard.
    // On crée un tableau pour connaître le vrai numéro de l’item choisi dans la liste
    const tabnum = []
    const n = res.length - 1
    for (let i = 0; i < n; i++) tabnum.push(i + 1)
    let tabres
    let tabresitem
    if (valueOf('listaleat' + sq.etape + indPourEtapeInterne + String(indListe), true) === 1) { // On mélange les items au hasard
      tabresitem = ['...']
      tabres = []
      for (let i = 0; i < n; i++) {
        const tir = Math.floor(Math.random() * tabnum.length)
        const num = tabnum[tir]
        tabres.push(num)
        tabnum.splice(tir, 1)
        tabresitem.push(res[num])
      }
    } else {
      tabres = tabnum
      tabresitem = res
    }
    sq['listaleat' + indPourEtapeInterne + indListe] = tabres
    return { texte: tabresitem }
  }

  /**
   * Fonction renvoyant la liste d’objets générée par le lecteur mtg32 de cette section
   * @return {*}
   */
  function getMtgList () {
    return sq.estExerciceConstruction ? sq.mtgApp.getList() : sq.mtgAppLecteur.getList('mtg32svg')
  }

  function executeMacro (nomMacro) {
    if (sq.estExerciceConstruction) {
      sq.mtgApp.executeMacro(nomMacro)
    } else {
      sq.mtgAppLecteur.executeMacro('mtg32svg', nomMacro)
    }
  }

  /**
   * Fonction extrayant de l’affichage LaTeX de tag tagSolText les solutions acceptées
   * pour une éditeur MathQuill contenant nbTextEdit éditeurs de textes
   * Les solutions sont renvoyées dans un tableau dont les lignes sont des tableaux
   * de nbText chaînes
   * @param {string} tagSolText
   * @param {number} nbTextEdit Le nombre d’éditeurs de texte dans l’éditeur MathQuill correspondant
   * @param {boolean} bNocase si true, toutes les chaînes sont renvoyées en minuscules
   * @returns {*[]}
   */
  function extraitSolutionsEditeursTexte (tagSolText, nbTextEdit, bNocase = false) {
    function affErr () {
      console.warn('Les données pour les résultats de champ texte sont incorrectes')
    }
    const list = getMtgList()
    const latex = list.getByTag(tagSolText, true)
    if (latex === null || latex.className !== 'CLatex') return []
    // Il faut se débarrasser de ce qui n’est pas géré par MathQuill
    let ch = latex.chaineLatex.replace(/\n/g, '').replace(/\\displaystyle/g, ' ').replace(/\\,/g, ' ')
    ch = ch.replace(/\\left\[/g, '[').replace(/\\right]/g, ']')
    // On remplace les . décimaux par des virgules
    ch = ch.replace(/(\d)\.(\d)/g, '$1,$2')
    const res = []
    let i = ch.indexOf('\\text{')
    if (i === -1) {
      affErr()
      return res
    } else {
      while (i !== -1) {
        const idaf = getIndexFermant(ch, i + 5)
        if (idaf !== -1) {
          // Les résultats possibles pour les différents éditeurs sont séparés par des **
          // et s’il y a plusieurs réponses possibles pour un même éditeur elles sont séparées par des //
          const tab = []
          let st = ch.substring(i + 6, idaf)
          if (bNocase) st = st.toLowerCase()
          const soustab = st.split('**')
          if (soustab.length !== nbTextEdit) {
            affErr()
            return []
          }
          soustab.forEach(function (el) {
            tab.push(el.split('//'))
          })
          res.push(tab)
        }
        i = ch.indexOf('\\text{', idaf + 1)
      }
    }
    return res
  }

  function montreEditeurs (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.hasBoutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
  }

  function videEditeurs () {
    if (estEtapeInterne(sq.etape) || estEtapeConstruction(sq.etape)) return
    const el = j3pElement('expression')
    el.querySelectorAll('math-field').forEach(mf => {
      if (!mf.id.includes('PhBlock')) mf.value = ''
    })
    el.querySelectorAll('.PhBlock').forEach((mf) => {
      mf.getPrompts().forEach((id) => {
        mf.setPromptValue(id, '', 'latex')
      })
      // mf.stripPromptContent() // Quand on vide l’apparence du placeholder n’est plus visible
      const idFirstEdit = mf.getPrompts()[0]
      mf.setPromptValue(idFirstEdit, '')
    })
    for (let i = 1; i <= sq.nbList; i++) {
      if (sq.listesLatex) {
        sq['listeDeroulante' + i].reset()
      } else {
        j3pElement('expressionliste' + i).selectedIndex = 0
      }
    }
    for (let i = 1; i <= sq.nbTextEdit; i++) {
      $('#expressioninput' + i).val('')
    }
    // On donne le focus au premier éditeur MathQuill
    focusPremierEditeur()
  }

  /**
   * Fonction donnant le focus au premier éditeur
   * sinon on donne le focus directement
   */
  function focusPremierEditeur () {
    // Si on est dans une étape interne, les éditeurs sont à rechercher partout
    const id = nbEditeursInternes(sq.etape) > 0 ? 'divmtg32' : 'expression'
    setFocusToFirstInput(id)
  }

  function resetMqDivPosition (iddiv) {
    const div = j3pElement(iddiv)
    if (!div) {
      console.warn('Appel incorrect de resetMqDivPosition avec pour id du div : ' + iddiv)
    }
    const edit = div.edit
    const pointLie = edit.pointLie
    const alignHor = edit.alignementHorizontal
    const alignVer = edit.alignementVertical
    const svg = j3pElement('mtg32svg')
    const rect1 = svg.getBoundingClientRect()
    const rect2 = svg.parentNode.getBoundingClientRect()
    const decx = edit.decX // Décalage horizontal de l’affichage par rapport au point
    const decy = edit.decY // Décalage vertical de l’affichage par rapport au point
    const decLeft = rect1.left - rect2.left + decx
    const decTop = rect2.top - rect2.top + decy
    const rect = div.getBoundingClientRect()
    const width = rect.width
    const height = rect.height
    const decHor = (alignHor === 0) ? 0 : ((alignHor === 1) ? -width / 2 : -width)
    const decVer = (alignVer === 0) ? 0 : ((alignVer === 1) ? -height / 2 : -height)
    div.style.left = String(pointLie.x + decHor + decLeft) + 'px'
    div.style.top = String(pointLie.y + decVer + decTop) + 'px'
  }

  function creeEditeurs () {
    const nbEditInt = nbEditeursInternes(sq.etape)
    if ((estEtapeInterne(sq.etape) && (nbEditInt === 0)) || estEtapeConstruction(sq.etape)) return
    prepareListeBoutons()
    if (nbEditInt > 0) {
      const list = getMtgList()
      for (let i = 1; i <= nbEditInt; i++) {
        const finid = sq.etape + String(i)
        const idDiv = 'formulaireInt' + finid
        let formulaire = extraitDeLatexPourMathlive(getMtgList(), idDiv)
        sq[idDiv] = formulaire // Utilisé dans corrigeEtapeInterneAvecEditeurs
        sq['nbCalc' + i] = nbEdit('edit', formulaire)
        sq['nbList' + i] = nbEdit('list', formulaire)
        sq['nbEditable' + i] = nbEditable(formulaire)
        sq['nbTextEdit' + i] = nbEdit('edittext', formulaire)
        for (let k = 1; k <= sq['nbCalc' + i]; k++) {
          formulaire = formulaire.replace('edit' + k, '&' + k + '&')
        }
        for (let k = 1; k <= sq['nbList' + i]; k++) formulaire = formulaire.replace('list' + k, '#' + k + '#')
        for (let k = 1; k <= sq['nbTextEdit' + i]; k++) formulaire = formulaire.replace('edittext' + k, '@' + k + '@')
        const edit = list.getByTag(idDiv, true) // Affichage LaTeX lié à un point
        if (edit === null || edit?.className !== 'CLatex') return
        else {
          if (edit.pointLie === null) return
        }
        const color = edit.color
        const taillePolice = edit.taillePolice
        const div = document.createElement('div')
        const svg = j3pElement('mtg32svg')
        let pNode = svg.parentNode
        if (pNode.localName === 'svg') pNode = pNode.parentNode // Pour compatibilité avec la version 8.4 dans laquelle
        // le svg de la figure est encapsulé dans un svg global
        // On attache le div au div parent du svg qui contient la figure
        pNode.appendChild(div)
        const style = 'position:absolute;margin:0;'
        div.setAttribute('style', style)
        div.style.fontSize = taillePolice + 'px'
        div.id = idDiv // On donne une id à ce div
        div.color = color // On mémorise la couleur pour la réaffecter dans corrigeEtapeInterneAvecEditeurs
        // On attache le edit au div pour pouvoir ensuite repositionner ce qu’il contient
        div.edit = edit
        const obj = {}
        for (let k = 1; k <= sq['nbCalc' + i]; k++) obj['inputmq' + k] = {}
        for (let k = 1; k <= sq['nbList' + i]; k++) {
          const donnees = extraitDonneesListe(k, String(i))
          sq['tabItemsLatex' + i + String(k)] = donnees.texte // Mémorisé pour afficheReponse
          obj['liste' + k] = donnees
        }
        for (let k = 1; k <= sq['nbTextEdit' + i]; k++) {
          obj['input' + k] = {
            texte: '',
            dynamique: true
          }
        }
        obj.charset = sq['charset' + sq.etape]
        obj.listeBoutons = sq.listeBoutons
        obj.charsetText = sq['charsetText' + sq.etape]
        obj.style = {
          color
        }
        afficheMathliveDans(div, 'repEdit' + sq.etape + String(i), formulaire, obj)
        setTimeout(function () {
          resetMqDivPosition(idDiv)
        })
        div.onkeyup = function () {
          resetMqDivPosition(idDiv)
        }
        div.onfocus = function () {
          resetMqDivPosition(idDiv)
        }
        div.onblur = function () {
          resetMqDivPosition(idDiv)
        }
        edit.cache()
        addQueue(function () {
          j3pEmpty(edit.g) // On vide la repésentation graphique du LaTeX devant devenir l’éditeur
        })
        // On donne un margin left et right aux listes déroulantes et aus éditeurs
        const idEditeur = 'repEdit' + sq.etape + String(i)
        for (let k = 1; k <= sq['nbList' + i]; k++) {
          $('#' + idEditeur + String(k)).css('margin-left', '5px').css('margin-right', '5px')
        }
        if (sq.listesEnLatex[sq.etape - 1]) {
          for (let k = 1; k <= sq['nbList' + i]; k++) {
            // $('#expressionliste' + k).css('margin-left', '5px').css('margin-right', '5px')
            const select = j3pElement(idEditeur + 'liste' + k)
            const pn = select.parentNode.parentNode
            const div = document.createElement('div')
            $(div).attr('id', idEditeur + 'divliste' + k)
            $(div).css('display', 'inline')
            pn.parentNode.replaceChild(div, pn)
            const tab = sq['tabItemsLatex' + i + String(k)]
            // Rectification suite aux modifs de liste Déroulante : S’il y a un $ en début et en fin on laisse
            // S’il n’y en a pas et que sq.listesEnLaTeX[etape - 1] est à true on les rajoute
            for (let i = 1; i < tab.length; i++) {
              let ch = tab[i].trim()
              const debdol = ch.charAt(0) === '$'
              const findol = ch.charAt(ch.length - 1) === '$'
              if (!debdol || !findol) {
                ch = (debdol ? '' : '$') + ch + (findol ? '' : '$')
                tab[i] = ch
              }
            }
            // Important : appeler avec l’option displayWithMathlive à true
            sq['listeDeroulante' + i + String(k)] = ListeDeroulante.create(idEditeur + 'divliste' + k, tab, { displayWithMathlive: true })
            $(div).css('display', 'inline')
          }
        }
        for (let k = 1; k <= sq['nbCalc' + i]; k++) {
          $('#' + idEditeur + 'inputmq' + k).css('margin-left', '5px').css('margin-right', '5px')
        }
        // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
        for (let k = 1; k <= sq['nbCalc' + i]; k++) {
          const edit = j3pElement(idEditeur + 'inputmq' + k)
          edit.indice = idEditeur + 'inputmq' + k
          if (sq['charset' + sq.etape] !== '') {
            mathliveRestrict(idEditeur + 'inputmq' + k, sq['charset' + sq.etape])
          }
          edit.addEventListener('focusin', () => {
            ecoute.call(edit)
          })
          edit.addEventListener('keyup', () => {
            onkeyup.call(edit)
          })
        }
        // Les mathfield ne sont pas dans 'expression' cette fois
        j3pElement('divmtg32').querySelectorAll('.PhBlock').forEach((mf) => {
          const id = mf.id
          if (sq.charset !== '') {
            mathliveRestrict(id, sq.charset)
          }
          mf.addEventListener('focusin', () => {
            ecoute.call(mf)
          })
          mf.addEventListener('keyup', () => {
            onkeyup.call(mf)
          })
        })
        for (let k = 1; k <= sq['nbTextEdit' + i]; k++) {
          const edit = j3pElement(idEditeur + 'input' + k)
          edit.indice = idEditeur + 'input' + k
          const nbcar = valueOf('nbcar' + String(sq.etape) + k, true)
          if (nbcar !== -1) $(edit).attr('maxlength', nbcar)
          if (sq['charsetText' + sq.etape] !== '') {
            j3pRestriction(idEditeur + 'input' + k, sq['charsetText' + sq.etape])
          }
          edit.onkeyup = function () {
            onkeyup.call(this)
          }
        }
      }
      focusPremierEditeur()
      // sq.mtgAppLecteur.updateDisplay('mtg32svg') Inutile
    } else {
      try {
        let k
        j3pEmpty('editeur')
        // Dans cette section, le formulaire est fourni par la figure mtg32 à chaque étape
        // par exemple dans le LaTeX de tag formulaire1 pour l’étape 1
        sq.formulaire = replaceOldMQCodes(extraitDeLatexPourMathlive(getMtgList(), 'formulaire' + sq.etape))
        sq.nbCalc = nbEdit('edit', sq.formulaire)
        sq.nbList = nbEdit('list', sq.formulaire)
        sq.nbTextEdit = nbEdit('edittext', sq.formulaire)
        sq.nbEditable = nbEditable(sq.formulaire)
        prepareListeBoutons()
        // S’il n’y a pas d’éditeurs mathquill inutile de montrer les boutons MathQuill
        if (sq.hasBoutonsMathQuill) {
          const el = j3pElement('boutonsmathquill')
          if (sq.nbCalc === 0 && sq.nbEditable === 0) el.style.display = 'none'
          else el.style.display = 'block'
        }
        sq.marked = {}
        let formulaire = sq.formulaire
        for (k = 1; k <= sq.nbCalc; k++) {
          formulaire = formulaire.replace('edit' + k, '&' + k + '&')
          // On interroge la figure pour savoir si l’éditeur k de l’étape correspondante
          // doit contenir une egalité ou une inégalité (calcul nommé estEgalité suivi du n° d’étape suivi de l’indice de l’éditeur)
          sq['estEgalite' + k] = valueOf('estEgalite' + sq.etape + String(k), true) === 1
          sq['estInegalite' + k] = valueOf('estInegalite' + sq.etape + String(k), true) === 1
        }
        for (k = 1; k <= sq.nbEditable; k++) {
          sq['estEgaliteEditable' + k] = valueOf('estEgaliteEditable' + sq.etape + String(k), true) === 1
          sq['estInegaliteEditable' + k] = valueOf('estInegaliteEditable' + sq.etape + String(k), true) === 1
        }
        for (k = 1; k <= sq.nbList; k++) formulaire = formulaire.replace('list' + k, '#' + k + '#')
        for (k = 1; k <= sq.nbTextEdit; k++) formulaire = formulaire.replace('edittext' + k, '@' + k + '@')
        const obj = {}
        for (k = 1; k <= sq.nbCalc; k++) obj['inputmq' + k] = {}
        for (k = 1; k <= sq.nbList; k++) {
          const donnees = extraitDonneesListe(k, '')
          sq['tabItemsLatex' + k] = donnees.texte // Mémorisé pour afficheReponse
          obj['liste' + k] = donnees
        }
        for (k = 1; k <= sq.nbTextEdit; k++) {
          obj['input' + k] = {
            texte: '',
            dynamique: true
          }
        }
        obj.charset = sq['charset' + sq.etape]
        obj.listeBoutons = sq.listeBoutons
        obj.transposeMat = true // Nécessaire pour compatibilité avec exos créés avec Mathquill
        obj.charsetText = sq['charsetText' + sq.etape]
        afficheMathliveDans('editeur', 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
        // On donne un margin left et right aux listes déroulantes et aus éditeurs
        for (k = 1; k <= sq.nbList; k++) {
          $('#expressionliste' + k).css('margin-left', '5px').css('margin-right', '5px')
        }
        if (sq.listesEnLatex[sq.etape - 1]) {
          for (k = 1; k <= sq.nbList; k++) {
            // $('#expressionliste' + k).css('margin-left', '5px').css('margin-right', '5px')
            const select = j3pElement('expressionliste' + k)
            const pn = select.parentNode.parentNode
            const div = document.createElement('div')
            $(div).attr('id', 'expressiondivliste' + k)
            $(div).css('display', 'inline')
            pn.parentNode.replaceChild(div, pn)
            const tab = sq['tabItemsLatex' + k]
            // Rectification suite aux modifs de liste Déroulante : S’il y a un $ en début et en fin on laisse
            // S’il n’y en a pas et que sq.listesLaTeX est à true on les rajoute
            for (let i = 1; i < tab.length; i++) {
              let ch = tab[i].trim()
              if (ch === '') ch = ' '
              const debdol = ch.charAt(0) === '$'
              const findol = ch.charAt(ch.length - 1) === '$'
              if (!debdol || !findol) {
                ch = (debdol ? '' : '$') + ch + (findol ? '' : '$')
                tab[i] = ch
              }
            }
            // Important : appeler avec l’option displayWithMathlive à true
            sq['listeDeroulante' + k] = ListeDeroulante.create('expressiondivliste' + k, tab, { displayWithMathlive: true })
          }
        }
        for (k = 1; k <= sq.nbCalc; k++) {
          $('#expressioninputmq' + k).css('margin-left', '5px').css('margin-right', '5px')
        }
        // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
        for (k = 1; k <= sq.nbCalc; k++) {
          const edit = j3pElement('expressioninputmq' + k)
          edit.indice = 'expressioninputmq' + k
          if (sq['charset' + sq.etape] !== '') {
            // Si on est à une étape où on doit entrer un ensemble de solutions d’inéquation
            // On ajoute les boutons ad hoc au clavier virtuel
            const listeBoutons = sq.listeBoutons
            const tabNewButtons = ['inf', 'vide', 'R', 'union', 'bracket']
            if (estEtapeResIneq(sq.etape)) {
              for (const ch of tabNewButtons) {
                if (!listeBoutons.includes(ch)) listeBoutons.push(ch)
              }
            }
            mathliveRestrict('expressioninputmq' + k, sq['charset' + sq.etape])
          }
          edit.addEventListener('focusin', () => {
            ecoute.call(edit)
          })
          edit.addEventListener('keyup', () => {
            onkeyup.call(edit)
          })
        }
        // On met les restrictions et les listeners sur chaque bloc contenant des editable (palceholders)
        j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf) => {
          const id = mf.id
          if (sq['charset' + sq.etape] !== '') {
            mathliveRestrict(id, sq['charset' + sq.etape])
          }
          mf.addEventListener('focusin', () => {
            ecoute.call(mf)
          })
          mf.addEventListener('keyup', () => {
            onkeyup.call(mf)
          })
        })
        for (k = 1; k <= sq.nbTextEdit; k++) {
          const edit = j3pElement('expressioninput' + k)
          edit.indice = 'expressioninput' + k
          const nbcar = valueOf('nbcar' + String(sq.etape) + k, true)
          if (nbcar !== -1) $(edit).attr('maxlength', nbcar)
          if (sq['charsetText' + sq.etape] !== '') {
            j3pRestriction('expressioninput' + k, sq['charsetText' + sq.etape])
          }
          edit.onkeyup = function () {
            onkeyup.call(this)
          }
        }
        focusPremierEditeur()
      } catch (e) {
        console.warn('Erreur lors de la création des éditeurs : ' + e.toString())
      }
    }
  }

  function afficheReponse (bilan, depasse) {
    if (estEtapeInterne(sq.etape) || estEtapeConstruction(sq.etape)) return // Pas pour une validation interne
    let coul
    const num = sq.numEssai
    const idrep = 'exp' + sq.etape + String(num)
    // let ch = (num > 2) ? '<br>' : ''
    // Avec MathliveAffiche il ne faut pas envoyer une chaîne commençant par un <br> car le premier affichage
    // se fait inline (celui du <br> mais le suivant est dans un <p> ce qui provoque un retour à la ligne)
    // et donc on a par exemple Réponse exacte : suivi d’un retour à la ligne suivi de la réponse
    // alors qu’on veut que tout soit sur une même ligne
    if (num >= 2) afficheMathliveDans('formules', idrep + 'br', '<br>')
    let ch

    if (bilan === 'exact') {
      ch = textes.repExacte
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        ch = textes.repFausse
        coul = parcours.styles.cfaux
      } else { // Exact mais pas fini
        ch = textes.repExactPasFini
        if (depasse) coul = parcours.styles.cfaux
        else coul = parcours.styles.colorCorrection
      }
    }
    afficheMathliveDans('formules', idrep + 'deb', ch, {
      style: {
        color: coul
      }
    })
    ch = sq.formulaire
    for (let i = 1; i <= sq.nbCalc; i++) {
      const st = '\\textcolor{' + (sq.repResolu[i - 1] ? parcours.styles.cbien : (sq.repExact[i - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
      ch = ch.replace('edit' + i, '$' + st + sq.rep[i - 1] + '}$')
    }
    // Il se peut qu’il y ait une matrice ou plusieurs dont les termes soient des editable.
    // Dans ce cas, lors de la création des éditeurs, les indices des editable ont été créés
    // par ordre croissant colonne par colonne mais lors du replacement il faut donner le contenu des
    // cellules ligne par ligne
    let indmqef = 1
    const hasMatrixEditable = ch.includes('\\begin{matrix}') && ch.includes('\\editable')
    if (hasMatrixEditable) ch = transposeMatrices(ch)
    let indexEditable = ch.indexOf('\\editable{')
    while (indexEditable !== -1) {
      const st = '\\textcolor{' + (sq.repResoluEditable[indmqef - 1] ? parcours.styles.cbien : (sq.repExactEditable[indmqef - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
      ch = ch.replace('\\editable', st + sq.repEditable[indmqef - 1] + '}') // On laisse des {} vides derrière ce qui ne change rien à l’affichage
      indmqef += 1
      indexEditable = ch.indexOf('\\editable{', indexEditable + 1)
    }
    if (hasMatrixEditable) ch = transposeMatrices(ch)
    for (let i = 1; i <= sq.nbList; i++) {
      const st = '\\textcolor{' + (sq.repResoluListe[i - 1] ? parcours.styles.cbien : parcours.styles.cfaux) + '}{'
      let chrep = sq['tabItemsLatex' + i][sq['replist' + i]]
      if (sq.listesLatex) {
        // chrep = chrep.substring(1, chrep.length - 1)
        chrep = chrep.replaceAll('$', '') // Modifié pour Mathlive car il forunit des espaces avant le $ et après
        ch = ch.replace('list' + i, '$' + st + chrep + '}$')
      } else {
        ch = ch.replace('list' + i, '$' + st + '\\text{' + chrep + '}}$')
      }
    }
    for (let i = 1; i <= sq.nbTextEdit; i++) {
      // A cause d’un buf de mathlive les deux lignes suivantes ne marchent pas avec par exemple comme co,tenu 8*a
      // const st = '\\textcolor{' + (sq.repResoluText ? parcours.styles.cbien : parcours.styles.cfaux) + '}{'
      // ch = ch.replace('edittext' + i, '$' + st + '\\text{' + sq.repText[i - 1] + '}}$')
      const color = sq.repResoluText ? parcours.styles.cbien : parcours.styles.cfaux
      ch = ch.replace('edittext' + i, '<span style="color:' + color + ';">' + sq.repText[i - 1] + '</span>')
    }
    // Il faut ici passer en options cleanHtml = false car sinon le span ci-dessus dera "nettoyé"
    // afficheMathliveDans('formules', idrep, ch, sq.parametres)
    const options = { ...sq.parametres }
    options.cleanHtml = false
    afficheMathliveDans('formules', idrep, ch, options)
    if (bilan === 'faux') { // On regarde si on a prévu un message pour certaines erreurs
      const texteFaute = extraitDeLatexPourMathlive(getMtgList(), 'faute' + sq.etape)
      if (texteFaute !== '') {
        afficheMathliveDans('formules', idrep + 'faute', '<br>' + texteFaute,
          {
            style: {
              color: coul
            }
          })
      }
    }
    resetKeyboardPosition()
  }

  function ecoute () {
    prepareListeBoutons()
    if (sq.hasBoutonsMathQuill && (this.id.indexOf('inputmq') !== -1 || this.id.indexOf('PhBlock') !== -1)) {
      j3pEmpty('boutonsmathquill')
      const estEtapeIneq = getMtgList().valueOf('etapeResIneq' + sq.etape, true) === 1
      let liste = sq.listeBoutons
      // Si on est en résolution d’inéquations on rajoute ces 3 boutons qui ne peuvent pas être dans le clavier virtuel
      if (estEtapeIneq) liste = liste.concat([';', '[', ']'])
      j3pPaletteMathquill('boutonsmathquill', this.id, { liste, nomdiv: 'palette' })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
    }
  }

  /**
   * Fonction renvoyant la valeur du calcul de nom nomCalc contenu dans la figure de l’exercice
   * @param {string} nomCalc
   * @param {boolean} bNoCase Si true on recherche le nom du calcul sans tenir compte de la casse
   * @return {number}
   */
  function valueOf (nomCalc, bNoCase) {
    if (sq.estExerciceConstruction) {
      return sq.mtgApp.valueOf(nomCalc, bNoCase)
    }
    return sq.mtgAppLecteur.valueOf('mtg32svg', nomCalc, bNoCase)
  }

  /**
   * Fonction ajoutant une fonction sur la pile d’appel du lecteur ou de l’éditeur de façon à être
   * sûr qu’elle sera exécutée après les affichages en cours
   * @param f
   */
  function addQueue (f) {
    if (sq.estExerciceConstruction) {
      sq.mtgApp.addFunctionToQueue(f)
    } else {
      sq.mtgAppLecteur.addFunctionToQueue(f)
    }
  }

  /**
   * Fonction recalculant la figure de l’exercice
   * @param {boolean} brandom si ture on relance les calculs aléatoires
   */
  function calculate (brandom) {
    if (sq.estExerciceConstruction) {
      sq.mtgApp.calculate(brandom)
    } else {
      sq.mtgAppLecteur.calculate('mtg32svg', brandom)
    }
  }

  /**
   * Fonction affichant la figure de l’exercice
   */
  function display () {
    if (sq.estExerciceConstruction) {
      sq.mtgApp.display()
    } else {
      sq.mtgAppLecteur.display('mtg32svg')
    }
  }
  /**
   * Fonction donnant au calcul ou la fonction nommé nomCalc contenu dans la figure de l’exercice
   * la formule contenue dans formule
   * @param {string} nomCalc
   * @param {string} formule
   */
  function giveFormula2 (nomCalc, formule) {
    if (sq.estExerciceConstruction) {
      sq.mtgApp.giveFormula2(nomCalc, formule)
    } else {
      sq.mtgAppLecteur.giveFormula2('mtg32svg', nomCalc, formule)
    }
  }

  function validation () {
    if (estEtapeInterne(sq.etape)) {
      const nbEditInt = nbEditeursInternes(sq.etape)
      if (nbEditInt > 0) {
        let resolu = true
        const listIdEditAvecErr = []
        for (let i = 1; i <= nbEditInt; i++) {
          // On va mémoriser les bonnes réponses pour les afficher en vert lors de l’affichage de la réponse
          sq['repResolu' + i] = []
          sq['repResoluEditable' + i] = []
          sq['repResoluListe' + i] = []
          // On vérifie d’abord les éditeurs MathQuill
          // On donne d’abord aux calculs associés la formule entrée dans les éditeurs internes
          for (let ind = 1; ind <= sq['nbCalc' + i]; ind++) {
            const id = 'repEdit' + sq.etape + String(i) + 'inputmq' + ind
            const rep = getMathliveValue(id)
            const resul = traiteMathlive(rep)
            const chcalcul = resul.res
            giveFormula2('repInt' + String(sq.etape) + i + String(ind), chcalcul)
          }
          calculate(false) // On recalcule la figure
          // Puis on regarde pour chaque éditeur si la réponse est bonne
          for (let ind = 1; ind <= sq['nbCalc' + i]; ind++) {
            const id = 'repEdit' + sq.etape + String(i) + 'inputmq' + ind
            const res = valueOf('resoluInt' + String(sq.etape) + i + String(ind), true)
            if (res !== 1) listIdEditAvecErr.push(id)
            resolu = resolu && (res === 1)
            sq['repResolu' + i].push(res === 1)
            // Si on  garde les éditeurs lors de la correction, parcours.styles.cbien est trop pale
            // car l’éditeur est désactive. On met un vert très foncé
            j3pElement(id).style.color = res === 1
              ? parcours.styles.cbien
              : parcours.styles.cfaux
          }
          // On fait ensuite de même avec les place holders s’il y en a
          let indPhBlock = 0
          let indEditable = 0
          const span = j3pElement('repEdit' + sq.etape + String(i))
          // span est le span qui contient le formulaire affiché par Mathlive
          span.querySelectorAll('.PhBlock').forEach((mf) => {
            indPhBlock++
            for (let k = 0; k < mf.getPrompts().length; k++) {
              indEditable++
              const idmf = 'repEdit' + sq.etape + String(i) + 'PhBlock' + indPhBlock
              const id = idmf + 'ph' + indEditable
              const rep = getMathliveValue(idmf, { placeholderId: id })
              const chcalcul = traiteMathlive(rep).res
              giveFormula2('repEditableInt' + sq.etape + String(i) + String(indEditable), chcalcul)
            }
          })
          calculate(false) // On recalcule la figure avant d’évaluer les réponses des place holder s’il y en a
          indPhBlock = 0
          indEditable = 0
          span.querySelectorAll('.PhBlock').forEach((mf) => {
            indPhBlock++
            for (let k = 0; k < mf.getPrompts().length; k++) {
              indEditable++
              const idmf = 'repEdit' + sq.etape + String(i) + 'PhBlock' + indPhBlock
              const id = idmf + 'ph' + indEditable
              const res = valueOf('resoluEditableInt' + String(sq.etape) + i + String(indEditable), true)
              if (res !== 1) listIdEditAvecErr.push(id)
              resolu = resolu && (res === 1)
              sq['repResoluEditable' + i].push(res === 1)
              mf.setPromptState(id, res === 1 ? 'correct' : 'incorrect')
            }
          })
          for (let ind = 1; ind <= sq['nbList' + i]; ind++) {
            const id = sq.listesEnLatex[sq.etape - 1]
              ? 'repEdit' + sq.etape + String(i) + 'divliste' + ind
              : 'repEdit' + sq.etape + String(i) + 'liste' + ind
            const indresliste = sq.listesEnLatex[sq.etape - 1] ? sq['listeDeroulante' + i + String(ind)].getReponseIndex() : j3pElement(id).selectedIndex
            // Les éléments de la liste peuvent avoir été mélangés au hasard
            // On gère les choix multiples ou les choix uniques dans la liste.
            // Si les choix multiples sont acceptés, on donne au calcul repListe suivi du n° d’étape et du n° de liste
            // la valeur choisie dans la liste et on interroge le calcul exactList suivi du n° d’étape et du n° de liste
            // qui doit valeur 1 si la réponse est une des réponses acceptées et 0 sinon.
            // Si le choix est unique on interroge la calcul resliste suivi du n° d’étape et du n° de liste qui
            // contient l’indice de la bonne réponse
            const resliste = sq['listaleat' + i + String(ind)][indresliste - 1]
            let reslisteexact
            if (valueOf('repListInt' + String(sq.etape) + i + String(ind), true) !== -1) {
              giveFormula2('repListInt' + String(sq.etape) + i + String(ind), String(resliste))
              calculate(false)
              reslisteexact = valueOf('exactListInt' + String(sq.etape) + i + String(ind), true) === 1
            } else {
              reslisteexact = resliste === valueOf('resListInt' + String(sq.etape) + i + String(ind), true)
            }
            sq['repResoluListe' + i].push(reslisteexact)
            j3pElement(id).style.color = reslisteexact
              ? parcours.styles.cbien
              : (reslisteexact === 2 ? parcours.styles.colorCorrection : parcours.styles.cfaux)
            resolu = resolu && reslisteexact
          }
          const tabrep = []
          const nbTextEdit = sq['nbTextEdit' + i]
          if (nbTextEdit > 0) {
            for (let ind = 1; ind <= nbTextEdit; ind++) {
              const rep = j3pValeurde('repEdit' + sq.etape + String(i) + 'input' + ind)
              tabrep.push(rep)
            }
            // on extrait le tableau des réponses admises pour cet éditeur de textes
            const tabsol = extraitSolutionsEditeursTexte('resTextInt' + sq.etape + String(i), nbTextEdit, !sq.textesSensiblesCasse)
            let resoluText = false
            for (let k = 0; k < tabsol.length; k++) {
              const tab = tabsol[k]
              let res = true
              for (let j = 0; j < tab.length && res; j++) {
                const chcasse = sq.textesSensiblesCasse ? tabrep[j] : tabrep[j].toLowerCase()
                res = res && tab[j].includes(chcasse)
              }
              if (res) {
                resoluText = true
                break
              }
            }
            const color = resoluText ? parcours.styles.cbien : parcours.styles.cfaux
            for (let ind = 1; ind <= nbTextEdit; ind++) {
              j3pElement('repEdit' + sq.etape + String(i) + 'input' + ind).style.color = color
            }
            // ON mémorise si la réponse texte globale pour cet éditeur est bonne ou non
            sq['repResoluText' + i] = resoluText
            resolu = resolu && resoluText
          }
        }
        const valres = valueOf('resolu' + sq.etape) // Vaut -1 si le calcul n’est pas défini
        // S’il est défini il doit valoir 1 pour que l’étape soit résolue
        if (valres !== -1) resolu = resolu && valres === 1
        sq.resolu = resolu
        sq.exact = sq.resolu
        if ((sq.numEssai < sq['nbEssais' + sq.etape]) && (listIdEditAvecErr.length > 0)) setFocusById(listIdEditAvecErr[0])
        return
      } else {
        const resolu = valueOf('resolu' + sq.etape, true)
        sq.resolu = resolu === 1
        sq.exact = sq.resolu
        return
      }
    }
    if (sq.estExerciceConstruction && estEtapeConstruction(sq.etape)) {
      sq.resolu = sq.mtgApp.validateAnswer()
      sq.exact = sq.resolu
      return
    }
    // Cas d’une étape où on doit entrer un ensemble de solution d’inéquation
    if (estEtapeResIneq(sq.etape)) {
      sq.rep = [getMathliveValue('expressioninputmq1').replace(/\\mathbb\{R}/g, '\\R')]
      if (sq.numEssai >= 1) {
        j3pElement('boutonrecopier').style.display = 'block'
      }
      if (sq.borneFermeeSurInf) {
        // Il y a eu une faute avec une borne fermée sur l’infini et le paramètre erreurSiBorneFermeeSurInf est à true
        // On compte cela comme une erreur et pas seulement une erreur de syntaxe
        sq.exact = false
        sq.resolu = false
        sq.repResolu = [false]
        sq.repExact = [false]
        return
      }
      sq.presqueResolu = false
      const vide = valueOf('vide' + sq.etape, true) === 1
      if (sq.vide) {
        if (vide) {
          sq.resolu = true
        } else {
          sq.resolu = false
          sq.exact = false
        }
      } else {
        const toutReelSol = valueOf('toutReelSol' + sq.etape, true) === 1
        if (toutReelSol) {
          if (sq.reponseEstR) { sq.resolu = true } else {
            if (sq.reponseContientR) {
              sq.exact = true
              sq.resolu = false
            } else {
              sq.exact = false
              sq.resolu = false
            }
          }
        } else {
          if (sq.reponseEstR || sq.reponseContientR) {
            sq.resolu = false
            sq.exact = false
          } else {
            giveFormula2('rep' + sq.etape, sq.chaineFonctionTest)
            giveFormula2('repPourBornes' + sq.etape, sq.chaineFonctionTestPourBornes)
            giveFormula2('repBornesFermees' + sq.etape, sq.chaineFonctionTestBornesFermees)
            calculate(false)
            // On regarde d’abord si la bonne réponse a été donnée
            if (valueOf('resolu' + sq.etape, true) === 1) {
              sq.resolu = true
            } else {
              // Il faut regarder si la figure comporte comme solution des valeurs isolées et si oui
              // si la réponse attendue comprend des valeurs isolées conformes à celles attendues
              if (valeursIsoleesCorrectes()) {
                // Sinon, on regarde d’abord pour chaque borne des intervalles entrés :
                // Si l’intervalle est fermé en cette borne ou en une autre borne équivalente, si la borne est solution de l’inéquation
                // Sinon si un nombre très proche de cette borne et dans l’intervalle est solution.
                sq.presqueResolu = valueOf('presqueResolu' + sq.etape, true) === 1
                if (!reponseIncluseDansSol()) {
                  sq.exact = false
                  sq.resolu = false
                } else {
                  sq.resolu = false
                  sq.exact = valueOf('repContientSol' + sq.etape, true) === 1
                }
              }
            }
          }
        }
      }
      sq.repResolu = [sq.resolu]
      sq.repExact = [sq.exact]
      return
    }
    // Cas classique multi-éditeur sans validation interne, sans construction
    let rep, ind, chcalcul, exact, resolu, res, ex, tab, j
    sq.rep = []
    sq.repExact = []
    sq.repEditable = []
    sq.repText = []
    sq.repExactEditable = []
    sq.repResolu = []
    sq.repExactEditable = []
    sq.repResoluEditable = []
    sq.repResoluListe = []
    sq.repResoluText = true

    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
      for (ind = 1; ind <= sq.nbCalc; ind++) {
        rep = getMathliveValue('expressioninputmq' + ind)
        sq.rep.push(rep)
        chcalcul = traiteMathlive(rep).res
        giveFormula2('rep' + String(sq.etape) + ind, chcalcul)
        // Pour certains exercices de résolution d'équation complexes on a besoin d'une fonction réelle de deux variables z et I
        // dans la formule de laquelle le nombre i est remplacé par un calcul réel I
        // Pas grave si le calcul n'existe pas car ne provoquera pas d'erreur
        let ch = chcalcul.replace(/(\W)i(\W)/g, '$1I$2')
        ch = ch.replace(/^i(\W)/g, 'I$1')
        ch = ch.replace(/(\W)i$/g, '$1I')
        if (ch === 'i') ch = 'I'
        // Pour gérer les équations d'inconnue zbarre, la figure devra contenit une fonction réelle de la variable
        // z défine par conjug(z)=abs(z)²/z
        ch = ch.replace(/conj\(/g, 'conjug(')
        giveFormula2('repComp' + String(sq.etape) + ind, ch)
      }
      // Traitement des editable (placeholders)
      let indPhBlock = 0
      let indEditable = 0
      j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf) => {
        indPhBlock++
        for (let i = 0; i < mf.getPrompts().length; i++) {
          indEditable++
          const idmf = 'expressionPhBlock' + indPhBlock
          const id = idmf + 'ph' + indEditable
          const rep = getMathliveValue(idmf, { placeholderId: id })
          sq.repEditable.push(rep)
          chcalcul = traiteMathlive(rep).res
          giveFormula2('repEditable' + String(sq.etape) + indEditable, chcalcul)
        }
      })
      calculate(false)
      exact = true
      resolu = true
      let auMoinsUnExact = false
      // On regarde d’abord les champs MathQuill s’il y en a
      for (ind = 1; ind <= sq.nbCalc; ind++) {
        res = valueOf('resolu' + String(sq.etape) + ind, true)
        ex = valueOf('exact' + String(sq.etape) + ind, true)
        resolu = resolu && (res === 1)
        exact = exact && ((res === 1) || (ex === 1))
        sq.repResolu.push(res === 1)
        sq.repExact.push((res === 1) || (ex === 1))
        auMoinsUnExact = auMoinsUnExact || (ex === 1)
      }
      // On regarde ensuite les champs Editable s’il y en a
      for (ind = 1; ind <= sq.nbEditable; ind++) {
        res = valueOf('resoluEditable' + String(sq.etape) + ind, true)
        ex = valueOf('exactEditable' + String(sq.etape) + ind, true)
        resolu = resolu && (res === 1)
        exact = exact && ((res === 1) || (ex === 1))
        sq.repResoluEditable.push(res === 1)
        sq.repExactEditable.push((res === 1) || (ex === 1))
        auMoinsUnExact = auMoinsUnExact || (ex === 1)
      }
      let listesOK = true
      for (ind = 1; ind <= sq.nbList; ind++) {
        const indresliste = sq.listesEnLatex[sq.etape - 1] ? sq['listeDeroulante' + ind].getReponseIndex() : j3pElement('expression' + 'liste' + ind).selectedIndex
        // Les éléments de la liste peuvent avoir été mélangés au hasard
        // On gère les choix multiples ou les choix uniques dans la liste.
        // Si les choix multiples sont acceptés, on donne au calcul repListe suivi du n° d’étape et du n° de liste
        // la valeur choisie dans la liste et on interroge le calcul exactList suivi du n° d’étape et du n° de liste
        // qui doit valeur 1 si la réponse est une des réponses acceptées et 0 sinon.
        // Si le choix est unique on interroge la calcul resliste suivi du n° d’étape et du n° de liste qui
        // contient l’indice de la bonne réponse
        const resliste = sq['listaleat' + ind][indresliste - 1]
        let reslisteexact
        if (valueOf('repList' + String(sq.etape) + ind, true) !== -1) {
          giveFormula2('repList' + String(sq.etape) + ind, String(resliste))
          calculate(false)
          reslisteexact = valueOf('exactList' + String(sq.etape) + ind, true) === 1
        } else {
          reslisteexact = resliste === valueOf('reslist' + String(sq.etape) + ind, true)
        }
        listesOK = listesOK && reslisteexact
        // On mémorise le choix de la sélection des listes déroulantes pour pouvoir recopier
        // la réponse précédente
        sq['replist' + ind] = indresliste
        sq.repResoluListe.push(reslisteexact)
      }
      resolu = resolu && listesOK
      // On vérifie maintenant la réponse contenue dans les champs de texte
      if (sq.nbTextEdit > 0) {
        const tabsol = extraitSolutionsEditeursTexte('resText' + sq.etape, sq.nbTextEdit, !sq.textesSensiblesCasse)
        for (ind = 1; ind <= sq.nbTextEdit; ind++) {
          rep = j3pValeurde('expressioninput' + ind)
          sq.repText.push(rep)
        }
        for (let i = 0; i < tabsol.length; i++) {
          tab = tabsol[i]
          res = true
          for (j = 0; j < tab.length && res; j++) { // tab.length doit être égal à sq.nbTextEdit
            const chcasse = sq.textesSensiblesCasse ? sq.repText[j] : sq.repText[j].toLowerCase()
            res = res && tab[j].includes(chcasse)
          }
          if (res) {
            sq.repResoluText = true
            break
          } else { sq.repResoluText = false }
        }
      }
      sq.resolu = resolu && sq.repResoluText
      sq.exact = exact && auMoinsUnExact && listesOK && sq.repResoluText
    }
  }

  function recopierReponse () {
    for (let ind = sq.nbCalc; ind > 0; ind--) {
      const ided = 'expressioninputmq' + ind
      // demarqueEditeurPourErreur(ided)
      // $('#' + ided).mathquill('latex', sq.rep[ind - 1]).blur()
      const mf = j3pElement(ided)
      mf.value = sq.rep[ind - 1]
      if (ind === 0) mf.focus()
    }
    let indiceEditable = 0
    let firstEdit = null
    let idFirstEdit = ''
    let contentFirstEdit = ''
    j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf, index) => {
      // On n’utilise pas mf.getPromps() pour parcourir ses placeholders à cause du problème des matrices
      // Pour garder la compatbilité avec mathquill les indices vont croissants par colonne et pas par ligne
      const indPhBlock = index + 1
      const nbPrompts = mf.getPrompts().length
      for (let i = 0; i < nbPrompts; i++) {
        const content = sq.repEditable[indiceEditable]
        indiceEditable++
        const id = 'expressionPhBlock' + indPhBlock + 'ph' + indiceEditable
        mf.setPromptValue(id, content)
        demarqueEditeurPourErreur(mf.id)
        if (index === 0 && indiceEditable === 1) {
          firstEdit = mf
          idFirstEdit = id
          contentFirstEdit = content
        }
      }
      // On récrit dans le premier placeHolder
      firstEdit.setPromptValue(idFirstEdit, contentFirstEdit)
    })
    for (let ind = sq.nbTextEdit; ind > 0; ind--) {
      const ided = 'expressioninput' + ind
      demarqueEditeurPourErreur(ided)
      $('#' + ided).val(sq.repText[ind - 1]).blur()
    }
    for (let ind = 1; ind <= sq.nbList; ind++) {
      if (sq.listesLatex) {
        sq['listeDeroulante' + ind].select(sq['replist' + ind])
      } else {
        j3pElement('expressionliste' + ind).selectedIndex = sq['replist' + ind]
      }
    }
    focusPremierEditeur()
  }

  function etapeSuivante () {
    j3pElement('boutonrecopier').style.display = 'none'
    j3pEmpty('editeur')
    j3pEmpty('enonce')
    j3pEmpty('enonceSuite')
    j3pEmpty('info')
    // Si l’étape actuelle est une étape interne avec des éditeurs intégrés, on doit afficher la correction
    // pour les éditeurs de formules, les editable, les listes déroulantes et les éditeurs de texte
    // et la macro d’intitulé 'macroEtape' suivi du numéro de l’étape suivante doit mettre la
    // figure en état de correction
    if (estEtapeInterne(sq.etape)) {
      corrigeEtapeInterneAvecEditeurs()
    }
    sq.etape++
    prepareListeBoutons()
    const figMasquee = valueOf('figureMasqueeEtape' + sq.etape, true) === 1
    // On regarde si la figue contient un calcul nommé figureMasqueEtape suivi du n° de l’étape
    // et de valeur 1. Si oui on masque le div de la figure.
    j3pElement('divmtg32').style.display = figMasquee ? 'none' : 'block'
    // Si une figure a été prévue pour cette étape (et les suivantes éventuellement) on la charge
    // en reprenant la même valeur pour les paramètres
    if (!sq.estExerciceConstruction && sq['fig' + sq.etape]) {
      // On mémorise d’abord la valeur des paramètres dans la figure en cours
      const ch = 'abcdefghjklmnpqr'
      const tabval = []
      for (let j = 0; j < ch.length; j++) {
        const car = ch.charAt(j)
        if (sq.param.indexOf(car) !== -1) {
          const par = valueOf(car) // La valeur du calcul dans la figure principale
          // On donne cette même valeur au calcul de même nom dans la figure de correction
          tabval.push(par)
        }
      }
      // On charge la nouvelle figure
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq['fig' + sq.etape], false)
      sq.mtgAppLecteur.calculateFirstTime('mtg32svg', false)
      for (let j = 0; j < ch.length; j++) {
        const car = ch.charAt(j)
        if (sq.param.indexOf(car) !== -1) {
          giveFormula2(car, tabval[0])
          tabval.pop()
        }
      }
      calculate(false)
      display()
    }
    // On laisse la possibilité à l’utilisateur d’exécuter une macro de la figure au début de chaque étape
    executeMacro('macroEtape' + sq.etape)
    const st = extraitDeLatexPourMathlive(getMtgList(), 'enonce' + sq.etape)
    afficheMathliveDans('enonceSuite', 'texte', st)
    sq.numEssai = 1
    afficheNombreEssaisRestants()
    creeEditeurs()
  }

  function desactiveEditeurs () {
    j3pElement('divmtg32').querySelectorAll('math-field').forEach((input) => {
      input.readOnly = true // Pour les éditeurs MathField
    })
    j3pElement('divmtg32').querySelectorAll('input[type=text]').forEach((input) => {
      // Attention : il ne faut pas désactiver les éditeurs qui sont des éditeurs de formules internes à la figure mtg32
      // Ces éditeurs ont pour parent un foreignObject
      if (input.parentNode.tagName.toLowerCase() !== 'foreignobject') input.disabled = true // Pour les champs texte
    })
  }

  function corrigeEtapeInterneAvecEditeurs () {
    const nbEditInt = nbEditeursInternes(sq.etape)
    if (nbEditInt > 0) {
      const corrigeSurFig = valueOf('correctionInterne' + sq.etape, true)
      if (corrigeSurFig === 0) {
        if (nbEditeursInternes(sq.etape) > 0) desactiveEditeurs()
        // Si on a choisi de laisser les erreurs sur la figure on désactive tous les éditeurs
        hideCurrentVirtualKeyboard()
        // On barre tous les input qui contiennent une réponse fausse
        for (let i = 1; i <= nbEditInt; i++) {
          const idspan = 'repEdit' + sq.etape + String(i)
          // On barre les éditeurs de formules simples (sans editable)
          for (let j = 1; j <= sq['nbCalc' + i]; j++) {
            if (!sq['repResolu' + i][j - 1]) {
              j3pBarre(idspan + 'inputmq' + j)
            }
          }
          // On barre les liste déroulantes avec une erreur
          for (let ind = 1; ind <= sq['nbList' + i]; ind++) {
            if (!sq['repResoluListe' + i][ind - 1]) {
              const id = sq.listesLatex
                ? idspan + 'divliste' + ind
                : idspan + 'liste' + ind
              j3pBarre(id)
            }
          }
          let indPhBlock = 0
          let indEditable = 0
          const span = j3pElement(idspan)
          span.querySelectorAll('.PhBlock').forEach((mf) => {
            indPhBlock++
            const idBlock = idspan + 'PhBlock' + indPhBlock
            let noerror = true
            for (let k = 0; (k < mf.getPrompts().length) && noerror; k++) {
              noerror = sq['repResoluEditable' + i][indEditable]
              indEditable++
            }
            // Si au moins un des editable est faux, on barre le bloc entier
            if (!noerror) j3pBarre(idBlock)
          })
        }
        return
      }
      for (let i = 1; i <= nbEditInt; i++) {
        let ch = sq['formulaireInt' + sq.etape + String(i)]
        for (let ind = 1; ind <= sq['nbCalc' + String(i)]; ind++) {
          const formula = extraitDeLatexPourMathlive(getMtgList(), 'solInt' + sq.etape + String(i) + ind)
          const st = '\\textcolor{' + (sq['repResolu' + i][ind - 1] ? parcours.styles.cbien : parcours.styles.cfaux) + '}{'
          ch = ch.replace('edit' + ind, '$' + st + formula + '}$')
        }
        for (let ind = 1; ind <= sq['nbList' + i]; ind++) {
          const st = '\\textcolor{' + (sq['repResoluListe' + i][ind - 1] ? parcours.styles.cbien : parcours.styles.cfaux) + '}{'
          const resAttendu = valueOf('reslistInt' + String(sq.etape) + i + String(ind), true)
          const indiceReelAttendu = sq['listaleat' + i + String(ind)].indexOf(resAttendu) + 1
          let chrep = sq['tabItemsLatex' + i + String(ind)][indiceReelAttendu]
          if (sq.listesEnLatex[sq.etape - 1]) {
            chrep = chrep.substring(1, chrep.length - 1)
            ch = ch.replace('list' + ind, '$' + st + chrep + '}$')
          } else {
            ch = ch.replace('list' + ind, '$' + st + '\\text{' + chrep + '}}$')
          }
        }
        const st = '\\textcolor{' + (sq['repResoluText' + i] ? parcours.styles.cbien : parcours.styles.cfaux) + '}{'
        for (let ind = 1; ind <= sq['nbTextEdit' + i]; ind++) {
          const solText = extraitDeLatexPourMathlive(getMtgList(), 'solText' + sq.etape + String(i) + ind)
          ch = ch.replace('edittext' + ind, '$' + st + '\\text{' + solText + '}}$')
        }
        const idrep = 'solution' + String(sq.etape) + i
        // On affiche la solution dans le div initial qui contenait l’éditeur MathQuill
        const div = j3pElement('formulaireInt' + sq.etape + String(i))
        const color = div.color // On a mémorisé lla couleur dans le div lors de la création des éditeurs
        j3pEmpty(div)
        afficheMathliveDans(div, idrep, ch, {
          style: { color }
        })
        // On repositionne le div contenant cet affichage
        resetMqDivPosition('formulaireInt' + sq.etape + String(i))
        executeMacro('solution' + sq.etape)
      }
    }
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    hideCurrentVirtualKeyboard()
    if (sq.solution) {
      executeMacro('solution')
      if (estEtapeInterne(sq.etape)) {
        corrigeEtapeInterneAvecEditeurs()
      }
      let ch, j, car, k, code
      j3pEmpty('editeur')
      j3pEmpty('enonce')
      j3pEmpty('enonceSuite')
      j3pEmpty('info')
      $('#divSolution').css('display', 'block').html('')
      ch = extraitDeLatexPourMathlive(getMtgList(), 'solution')
      if (ch !== '') {
        const styles = parcours.styles
        afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? styles.cbien : styles.colorCorrection } })
      }
      if (sq.figSol !== '') {
        const mtg32App = sq.mtgAppLecteur
        $('#divExplications').css('display', 'block').html('')
        $('#mtg32svgsol').css('display', 'block')
        mtg32App.removeDoc('mtg32svgsol')
        // Dans le cas où ce serait un exercice avec éditeurs internes il faut détruire
        // tous les éditeurs internes qui ont été plaqués sur la figure dans le div d’id divmtg32
        // donc on le vide mais il fait recréer le svg
        j3pEmpty('divmtg32')
        j3pCreeSVG('divmtg32', {
          id: 'mtg32svg',
          width: sq.width,
          height: sq.height
        })
        mtg32App.addDoc('mtg32svgsol', sq.figSol)
        // On remplace dans la figure de solution les paramètres par les valeurs correspondant dans la figure de texte
        ch = 'abcdefghjklmnpqr'
        for (j = 0; j < ch.length; j++) {
          car = ch.charAt(j)
          if (sq.param.indexOf(car) !== -1) {
            const par = valueOf(car) // La valeur du calcul dans la figure principale
            // On donne cette même valeur au calcul de même nom dans la figure de correction
            // Attntion ici n epas appelelr giveFormula2 tout court
            mtg32App.giveFormula2('mtg32svgsol', car, par)
          }
        }
        mtg32App.calculate('mtg32svgsol', false)
        const param = {}
        for (k = 0; k < sq.nbLatexSol; k++) {
          code = mtg32App.getLatexCode('mtg32svgsol', k)
          param[ch.charAt(k)] = code
        }
        afficheMathliveDans('divExplications', 'explications', sq.explicationSol, param)
        mtg32App.display('mtg32svgsol')
      }
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
    if (sq.figSol !== '') {
      $('#divExplications').css('display', 'none').html('')
      $('#mtg32svgsol').css('display', 'none')
    }
  }

  function initMtg () {
    /** @type {MtgOptions} */
    const mtgOptions = sq.estExerciceConstruction
      ? {
          fig: sq.fig,
          bplayer: true,
          open: false,
          newFig: false,
          save: false,
          options: false,
          displayOnLoad: false,
          isEditable: true
        }
      : {
          // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
          loadCoreWithMathJax: true
        }
    const svgOptions = {
      idSvg: 'mtg32svg',
      width: sq.width,
      height: sq.height
    }
    getMtgApp('divmtg32', svgOptions, mtgOptions).then((mtgApp) => {
      if (sq.estExerciceConstruction) {
        sq.mtgApp = mtgApp
        sq.mtgAppLecteur = mtgApp.player
      } else {
        sq.mtgApp = mtgApp
        sq.mtgAppLecteur = mtgApp
      }
      // Une liste qui servira pour le calcul des valerus des bornes des intervalles.
      // Elle contiendra un calcul x auquel on affectera des valeurs pour tester la valeur de la syntaxe et de la valeur d’une borne d’intervalle.
      sq.listePourCalc = sq.mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
      sq.marked = {}
      let par, car, i, j, k, nbrep, ar, tir, nb
      if (sq.estExerciceConstruction) {
        sq.mtgApp.calculate(false)
      } else {
        sq.mtgAppLecteur.removeAllDoc()
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
        sq.mtgAppLecteur.calculateFirstTime('mtg32svg', false)
      }
      const ch = 'abcdefghjklmnpqr'
      const nbvar = valueOf('nbvar', true)
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = valueOf('nbcas' + i, true)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          giveFormula2('r' + i, sq['tab' + i][0])
        }
      } else { sq.aleat = false }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') giveFormula2(car, par)
          }
        }
      }

      calculate(true)
      etablitNombreEtapesEtCoeff()
      display()
      executeMacro('macroEtape1') // Si une telle macro existe on l’exécute
      const figMasqueeEtape1 = valueOf('figureMasqueeEtape1', true) === 1
      if (figMasqueeEtape1) j3pElement('divmtg32').style.display = 'none'
      afficheMathliveDans('enonce', 'texte', extraitDeLatexPourMathlive(getMtgList(), 'enonce1'))
      creeEditeurs()
      if (!sq.hasBoutonsMathQuill) {
        j3pDetruit('boutonsmathquill')
      }
      afficheNombreEssaisRestants()
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    const conteneur = j3pAddElt(parcours.zonesElts.MG, 'div', { id: 'conteneur', style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
    j3pAddElt(conteneur, 'div', { id: 'enonce' })
    const formules = j3pAddElt(conteneur, 'div', { id: 'formules', style: parcours.styles.petit.enonce }) // Contient le formules entrées par l’élève
    formules.style.fontSize = sq.taillePoliceReponses + 'px'
    formules.style.paddingBottom = '8px'
    j3pAddElt(conteneur, 'div', { id: 'enonceSuite' })

    j3pAddElt(conteneur, 'div', { id: 'info' })
    j3pAddElt(conteneur, 'div', { id: 'conteneurbouton' })
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente', recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    const editeur = j3pAddElt(conteneur, 'div', { id: 'editeur', style: { paddingTop: '10px' } }) // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
    editeur.style.fontSize = sq.taillePoliceFormulaire + 'px'
    j3pAddElt(conteneur, 'div', { id: 'boutonsmathquill', style: { paddingTop: '10px' } })
    const divsol = j3pAddElt(conteneur, 'div', { id: 'divSolution', style: { display: 'none' } })
    divsol.style.fontSize = sq.taillePoliceSolution + 'px'
    j3pAddElt(conteneur, 'div', {
      id: 'divmtg32',
      style: {
        width: String(sq.width) + 'px',
        height: String(sq.height) + 'px'
      }
    })
    // Si on utilise le mtgAppLecteur il faut que le SVG soit créé à ce niveau
    // sinon il n’est pas créé assez tôt
    if (!sq.estExerciceConstruction) {
      j3pCreeSVG('divmtg32', {
        id: 'mtg32svg',
        width: sq.width,
        height: sq.height
      })
    }
    if (sq.figSol !== '') {
      j3pAddElt(conteneur, 'div', { id: 'divExplications', style: { display: 'none' } })
      const divFigSol = j3pAddElt(conteneur, 'div', { id: 'divFigSol' })
      j3pCreeSVG(divFigSol, {
        id: 'mtg32svgsol',
        width: sq.widthSol,
        height: sq.heightSol
      })
      $('#mtg32svgsol').css('display', 'none')
    }
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    initMtg()
  }

  switch (this.etat) {
    case 'enonce':
      sq.scoreInit = parcours.score
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        // compteur
        sq.etape = 1
        sq.numEssai = 1
        sq.nbexp = 0
        sq.reponse = -1
        if (parcours.donneesSection.calculatrice) {
          j3pAddElt(this.zonesElts.MD, 'div', { id: 'emplacecalc' })
          this.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
          j3pCreeFenetres(this)
          j3pAjouteBouton('emplacecalc', 'BCalculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
        }

        sq.width = parcours.donneesSection.width
        sq.height = parcours.donneesSection.height
        sq.solution = parcours.donneesSection.solution

        const tabBtn = ['Puis', 'Frac', 'Pi', 'Supegal', 'Infegal', 'Cos', 'Sin', 'Tan', 'Rac', 'Exp', 'Ln', 'Log', 'Abs', 'Conj', 'Integ', 'Prim', 'Indice', 'Inf', 'R', 'Union', 'Acc']
        tabBtn.forEach((p) => {
          sq['btn' + p] = parcours.donneesSection['btn' + p]
        })
        sq.variables = parcours.donneesSection.variables
        sq.titre = parcours.donneesSection.titre
        const bigSize = parcours.donneesSection.bigSize
        if (bigSize && !parcours.donneesSection.taillePoliceFormulaire) {
          sq.taillePoliceFormulaire = 30
          sq.taillePoliceReponses = 26
        } else {
          let taillePolice = Number(parcours.donneesSection.taillePoliceFormulaire ?? 24)
          if (taillePolice < 10 || taillePolice > 50) taillePolice = 24
          sq.taillePoliceFormulaire = taillePolice
          let taillePoliceReponses = Number(parcours.donneesSection.taillePoliceReponses ?? 22)
          if (taillePoliceReponses < 10 || taillePoliceReponses > 50) taillePoliceReponses = 22
          sq.taillePoliceReponses = taillePoliceReponses
          let taillePoliceSolution = Number(parcours.donneesSection.taillePoliceSolution ?? 22)
          if (taillePoliceSolution < 10 || taillePoliceSolution > 50) taillePoliceSolution = 22
          sq.taillePoliceSolution = taillePoliceSolution
        }

        sq.param = parcours.donneesSection.param // Chaine contenant les paramètres autorisés
        sq.erreurSiBorneFermeeSurInfini = parcours.donneesSection.erreurSiBorneFermeeSurInfini
        sq.textesSensiblesCasse = parcours.donneesSection.textesSensiblesCasse
        for (let i = 1; i <= 12; i++) {
          sq['nbEssais' + i] = parseInt(parcours.donneesSection['nbEssais' + i])
          sq['charset' + i] = parcours.donneesSection['charset' + i]
          sq['charsetText' + i] = parcours.donneesSection['charsetText' + i]
        }
        sq.figSol = parcours.donneesSection.figSol
        sq.nbLatexSol = parcours.donneesSection.nbLatexSol
        sq.widthSol = parcours.donneesSection.widthSol
        sq.heightSol = parcours.donneesSection.heightSol
        sq.explicationSol = parcours.donneesSection.explicationSol
        sq.fig = parcours.donneesSection.fig
        for (let i = 0; i <= 12; i++) {
          sq['fig' + i] = parcours.donneesSection['fig' + i]
        }
        sq.estExerciceConstruction = parcours.donneesSection.estExerciceConstruction
        sq.listesLatex = parcours.donneesSection.listesLatex === undefined ? false : parcours.donneesSection.listesLatex
        if (sq.fig === '') {
          sq.fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAARZAAACpQAAAQEAAAAAAAAAAAAAAFj#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAhuYkV0YXBlcwABMgAAAAFAAAAAAAAAAAAAAAIA#####wAGbmJjYXMxAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAJyMQATaW50KHJhbmQoMCkqbmJjYXMxKf####8AAAACAAlDRm9uY3Rpb24C#####wAAAAEACkNPcGVyYXRpb24CAAAAAxEAAAABAAAAAAAAAAA#0bbuT1KvmP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAACAAAAAgD#####AAFhAARyMSsxAAAABAAAAAAFAAAAAwAAAAE#8AAAAAAAAAAAAAIA#####wAEdGExMgALc2koYTwzLDEsMCn#####AAAAAQANQ0ZvbmN0aW9uM1ZhcgAAAAAEBAAAAAUAAAAEAAAAAUAIAAAAAAAAAAAAAT#wAAAAAAAAAAAAAQAAAAAAAAAAAAAAAgD#####AAZuYmNhczIAATkAAAABQCIAAAAAAAAAAAACAP####8ABm5iY2FzMwABOQAAAAFAIgAAAAAAAAAAAAIA#####wACcjIAE2ludChyYW5kKDApKm5iY2FzMikAAAADAgAAAAQCAAAAAxEAAAABAAAAAAAAAAA#6oGLSzTlWgAAAAUAAAAGAAAAAgD#####AAJyMwATaW50KHJhbmQoMCkqbmJjYXMzKQAAAAMCAAAABAIAAAADEQAAAAEAAAAAAAAAAD#QaslpvkfMAAAABQAAAAcAAAACAP####8AAWIABHIyKzEAAAAEAAAAAAUAAAAIAAAAAT#wAAAAAAAAAAAAAgD#####AAFjAARyMysxAAAABAAAAAAFAAAACQAAAAE#8AAAAAAAAAAAAAIA#####wACYzEADXNpKGM9YixjKzEsYykAAAAGAAAAAAQIAAAABQAAAAsAAAAFAAAACgAAAAQAAAAABQAAAAsAAAABP#AAAAAAAAAAAAAFAAAACwAAAAIA#####wACbW4ACW1pbihiLGMxKf####8AAAABAA1DRm9uY3Rpb24yVmFyAQAAAAUAAAAKAAAABQAAAAwAAAACAP####8AAm14AAltYXgoYixjMSkAAAAHAAAAAAUAAAAKAAAABQAAAAwAAAACAP####8ABW5idmFyAAE1AAAAAUAUAAAAAAAAAAAAAgD#####AANhMTIABGIrYzEAAAAEAAAAAAUAAAAKAAAABQAAAAwAAAACAP####8AA2EzNAAJYWJzKGItYzEpAAAAAwAAAAAEAQAAAAUAAAAKAAAABQAAAAwAAAACAP####8ABXJlcDQxAAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAVyZXAyMQABMAAAAAEAAAAAAAAAAAAAAAIA#####wADdGExAAtzaShhPTEsMSwwKQAAAAYAAAAABAgAAAAFAAAABAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAAAAAAEAAAAAAAAAAAAAAAIA#####wADdGEyAAtzaShhPTIsMSwwKQAAAAYAAAAABAgAAAAFAAAABAAAAAFAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAEAAAAAAAAAAAAAAAIA#####wADdGEzAAtzaShhPTMsMSwwKQAAAAYAAAAABAgAAAAFAAAABAAAAAFACAAAAAAAAAAAAAE#8AAAAAAAAAAAAAEAAAAAAAAAAAAAAAIA#####wAFcmVwMTEAATAAAAABAAAAAAAAAAAAAAACAP####8ABXJlcDEyAAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAZuYmNhczQAATkAAAABQCIAAAAAAAAAAAACAP####8AAnI0ABNpbnQocmFuZCgwKSpuYmNhczQpAAAAAwIAAAAEAgAAAAMRAAAAAQAAAAAAAAAAP+fMMIrxrFoAAAAFAAAAGQAAAAIA#####wABZAAEcjQrMQAAAAQAAAAABQAAABoAAAABP#AAAAAAAAAAAAACAP####8AAmJjAANiK2MAAAAEAAAAAAUAAAAKAAAABQAAAAsAAAACAP####8ABHJlMTEADHNpKGE9MSxiYyxkKQAAAAYAAAAABAgAAAAFAAAABAAAAAE#8AAAAAAAAAAAAAUAAAAcAAAABQAAABsAAAACAP####8ABHJlMTIADHNpKGE9MixiYyxkKQAAAAYAAAAABAgAAAAFAAAABAAAAAFAAAAAAAAAAAAAAAUAAAAcAAAABQAAABsAAAACAP####8ACHJlc29sdTExABJzaShyZXAxMT1yZTExLDEsMCkAAAAGAAAAAAQIAAAABQAAABcAAAAFAAAAHQAAAAE#8AAAAAAAAAAAAAEAAAAAAAAAAAAAAAIA#####wAIcmVzb2x1MTIAEnNpKHJlcDEyPXJlMTIsMSwwKQAAAAYAAAAABAgAAAAFAAAAGAAAAAUAAAAeAAAAAT#wAAAAAAAAAAAAAQAAAAAAAAAAAAAAAgD#####AAZuYmNhczUAATMAAAABQAgAAAAAAAAAAAACAP####8AAnI1ABNpbnQocmFuZCgwKSpuYmNhczUpAAAAAwIAAAAEAgAAAAMRAAAAAQAAAAAAAAAAP+Wca9TKYCYAAAAFAAAAIQAAAAIA#####wABZQAEcjUrMQAAAAQAAAAABQAAACIAAAABP#AAAAAAAAAAAAACAP####8AA3RlMQALc2koZT0xLDEsMCkAAAAGAAAAAAQIAAAABQAAACMAAAABP#AAAAAAAAAAAAABP#AAAAAAAAAAAAABAAAAAAAAAAAAAAACAP####8AA3RlMgALc2koZT0yLDEsMCkAAAAGAAAAAAQIAAAABQAAACMAAAABQAAAAAAAAAAAAAABP#AAAAAAAAAAAAABAAAAAAAAAAD#####AAAAAgAGQ0xhdGV4AP####8BAAAAAQAHZW5vbmNlMf####8QQC4AAAAAAABAUcAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAABVFxiZWdpbnthcnJheX17bH0KXHRleHR7T24gc291aGFpdGUgY2FsY3VsZXIgbGEgc29tbWUgJCAKClxJZnt0YTF9CntcSWZ7dGUxfQp7KCtcVmFse2J9KSsoK1xWYWx7Y30pKygtXFZhbHtkfSl9CntcSWZ7dGUyfQp7KCtcVmFse2J9KSsoLVxWYWx7ZH0pKygrXFZhbHtjfSl9CnsoLVxWYWx7ZH0pKygrXFZhbHtifSkrKCtcVmFse2N9KX19fQoKe1xJZnt0ZTF9CnsoLVxWYWx7Yn0pKygtXFZhbHtjfSkrKCtcVmFse2R9KX0Ke1xJZnt0ZTJ9CnsoLVxWYWx7Yn0pKygrXFZhbHtkfSkrKC1cVmFse2N9KX0KeygrXFZhbHtkfSkrKC1cVmFse2J9KSsoLVxWYWx7Y30pfX19CgokIH0gClxlbmR7YXJyYXl9CgoAAAAIAP####8BAAAAAQALZm9ybXVsYWlyZTH#####EEB38AAAAAAAQFtAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAZlcYmVnaW57YXJyYXl9e2x9Clx0ZXh0ezxpPk9uIGRlbWFuZGUgaWNpIGRlIHJlZ3JvdXBlciBsZXMgdGVybWVzIGRlIG3Dqm1lIHNpZ25lIDogPC9pPn0KClxcIFx0ZXh0eyAKCiRcSWZ7dGExfQp7XElme3RlMX0KeygrXFZhbHtifSkrKCtcVmFse2N9KSsoLVxWYWx7ZH0pfQp7XElme3RlMn0KeygrXFZhbHtifSkrKC1cVmFse2R9KSsoK1xWYWx7Y30pfQp7KC1cVmFse2R9KSsoK1xWYWx7Yn0pKygrXFZhbHtjfSl9fX0KCntcSWZ7dGUxfQp7KC1cVmFse2J9KSsoLVxWYWx7Y30pKygrXFZhbHtkfSl9CntcSWZ7dGUyfQp7KC1cVmFse2J9KSsoK1xWYWx7ZH0pKygtXFZhbHtjfSl9CnsoK1xWYWx7ZH0pKygtXFZhbHtifSkrKC1cVmFse2N9KX19fT0oKyRlZGl0MSQpKygtJGVkaXQyJCkkCgogfSAKXFwgClxlbmR7YXJyYXl9AAAACAD#####AQAAAAEABmxpc3QyMf####8QQF0AAAAAAABAa+AAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAQVxiZWdpbnthcnJheX17bH0KXHRleHR7JCskfQpcXCBcdGV4dHskLSR9ClxcIFx0ZXh0eyB9ClxlbmR7YXJyYXl9AAAAAgD#####AARyZTIxAAlhYnMoYmMtZCkAAAADAAAAAAQBAAAABQAAABwAAAAFAAAAGwAAAAIA#####wADdGJjAAxzaShiYz5kLDEsMCkAAAAGAAAAAAQFAAAABQAAABwAAAAFAAAAGwAAAAE#8AAAAAAAAAAAAAEAAAAAAAAAAAAAAAIA#####wAIcmVzb2x1MjEAEnNpKHJlcDIxPXJlMjEsMSwwKQAAAAYAAAAABAgAAAAFAAAAEwAAAAUAAAApAAAAAT#wAAAAAAAAAAAAAQAAAAAAAAAAAAAACAD#####AQAAAAEAB2Vub25jZTL#####EEBYgAAAAAAAQHXgAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAA9tcYmVnaW57YXJyYXl9e2x9Clx0ZXh0ezxicj5PbiBzb3VoYWl0ZSBjYWxjdWxlciBsYSBzb21tZSAkIAoKXElme3RhMX0Ke1xJZnt0ZTF9CnsoK1xWYWx7Yn0pKygrXFZhbHtjfSkrKC1cVmFse2R9KX0Ke1xJZnt0ZTJ9CnsoK1xWYWx7Yn0pKygtXFZhbHtkfSkrKCtcVmFse2N9KX0KeygtXFZhbHtkfSkrKCtcVmFse2J9KSsoK1xWYWx7Y30pfX19Cgp7XElme3RlMX0KeygtXFZhbHtifSkrKC1cVmFse2N9KSsoK1xWYWx7ZH0pfQp7XElme3RlMn0KeygtXFZhbHtifSkrKCtcVmFse2R9KSsoLVxWYWx7Y30pfQp7KCtcVmFse2R9KSsoLVxWYWx7Yn0pKygtXFZhbHtjfSl9fX0KCiQgfQoKXFwgXHRleHR7PGk+RW4gcmVncm91cGFudCBsZXMgbGVzIHRlcm1lcyBkZSBtw6ptZSBzaWduZSA6IDwvaT59CgpcXCBcdGV4dHsgCgokXElme3RhMX0Ke1xJZnt0ZTF9CnsoK1xWYWx7Yn0pKygrXFZhbHtjfSkrKC1cVmFse2R9KX0Ke1xJZnt0ZTJ9CnsoK1xWYWx7Yn0pKygtXFZhbHtkfSkrKCtcVmFse2N9KX0KeygtXFZhbHtkfSkrKCtcVmFse2J9KSsoK1xWYWx7Y30pfX19Cgp7XElme3RlMX0KeygtXFZhbHtifSkrKC1cVmFse2N9KSsoK1xWYWx7ZH0pfQp7XElme3RlMn0KeygtXFZhbHtifSkrKCtcVmFse2R9KSsoLVxWYWx7Y30pfQp7KCtcVmFse2R9KSsoLVxWYWx7Yn0pKygtXFZhbHtjfSl9fX09KCtcdGV4dGNvbG9ye2dyZWVufXtcdGV4dGJme1xJZnt0YTF9e1xWYWx7YmN9fXtcVmFse2R9fX19KSsoLVx0ZXh0Y29sb3J7Z3JlZW59e1x0ZXh0YmZ7XElme3RhMX17XFZhbHtkfX17XFZhbHtiY319fX0pJAoKIH0gClxcIFx0ZXh0ezxpPkEgY2V0dGUgw6l0YXBlLCBvbiBuJ2EgZmFpdCBxdWUgZGVzIGFkZGl0aW9ucyBkZSBkaXN0YW5jZXMgw6AgMCBwdWlzcXUnb24gcmVncm91cGUgbGVzIHRlcm1lcyBkZSBtw6ptZSBzaWduZS48L2k+fQpcXCBcdGV4dHtPbiBkZW1hbmRlIG1haW5lbmFudCBkZSBmaW5pciBsZSBjYWxjdWwgOn0KXGVuZHthcnJheX0AAAACAP####8ABnRzaWduZQASc2kocmUxMT49cmUxMiwxLDApAAAABgAAAAAEBwAAAAUAAAAdAAAABQAAAB4AAAABP#AAAAAAAAAAAAABAAAAAAAAAAAAAAAIAP####8BAAAAAQALZm9ybXVsYWlyZTL#####EEA1AAAAAAAAQE8AAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAA1FcYmVnaW57YXJyYXl9e2x9Clx0ZXh0eyAKCiRcSWZ7dGExfQp7XElme3RlMX0KeygrXFZhbHtifSkrKCtcVmFse2N9KSsoLVxWYWx7ZH0pfQp7XElme3RlMn0KeygrXFZhbHtifSkrKC1cVmFse2R9KSsoK1xWYWx7Y30pfQp7KC1cVmFse2R9KSsoK1xWYWx7Yn0pKygrXFZhbHtjfSl9fX0KCntcSWZ7dGUxfQp7KC1cVmFse2J9KSsoK1xWYWx7Y30pKygrXFZhbHtkfSl9CntcSWZ7dGUyfQp7KC1cVmFse2J9KSsoK1xWYWx7ZH0pKygtXFZhbHtjfSl9CnsoK1xWYWx7ZH0pKygtXFZhbHtifSkrKC1cVmFse2N9KX19fT0oK1x0ZXh0Y29sb3J7Z3JlZW59e1x0ZXh0YmZ7XElme3RhMX17XFZhbHtiY319e1xWYWx7ZH19fX0pKygtXHRleHRjb2xvcntncmVlbn17XHRleHRiZntcSWZ7dGExfXtcVmFse2R9fXtcVmFse2JjfX19fSkkCgogfSAgClxcIFx0ZXh0eyAKCiRcSWZ7dGExfQp7XElme3RlMX0KeygrXFZhbHtifSkrKCtcVmFse2N9KSsoLVxWYWx7ZH0pfQp7XElme3RlMn0KeygrXFZhbHtifSkrKC1cVmFse2R9KSsoK1xWYWx7Y30pfQp7KC1cVmFse2R9KSsoK1xWYWx7Yn0pKygrXFZhbHtjfSl9fX0KCntcSWZ7dGUxfQp7KC1cVmFse2J9KSsoK1xWYWx7Y30pKygrXFZhbHtkfSl9CntcSWZ7dGUyfQp7KC1cVmFse2J9KSsoK1xWYWx7ZH0pKygtXFZhbHtjfSl9CnsoK1xWYWx7ZH0pKygtXFZhbHtifSkrKC1cVmFse2N9KX19fT0kIChsaXN0MSBlZGl0MSl9CgpcXCBcdGV4dHs8cCBhbGlnbj0icmlnaHQiPjxpPiBEYW5zIGxlIGNhcyBvw7kgbGUgcsOpc3VsdGF0IHNlcmFpdCAwLCBsYSBub3RhdGlvbiBhdHRlbmR1ZSBwb3VyIGNldCBleGVyY2ljZSBlc3QgKCswKS48L2k+PC9wPn0KXGVuZHthcnJheX0AAAAIAP####8BAAAAAQAIc29sdXRpb27#####EEA6AAAAAAAAQGYgAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAABLVcYmVnaW57YXJyYXl9e2x9Clx0ZXh0ezxicj5PbiBzb3VoYWl0ZSBjYWxjdWxlciBsYSBzb21tZSAkXElme3RhMX0Ke1xJZnt0ZTF9CnsoK1xWYWx7Yn0pKygrXFZhbHtjfSkrKC1cVmFse2R9KX0Ke1xJZnt0ZTJ9CnsoK1xWYWx7Yn0pKygtXFZhbHtkfSkrKCtcVmFse2N9KX0KeygtXFZhbHtkfSkrKCtcVmFse2J9KSsoK1xWYWx7Y30pfX19Cgp7XElme3RlMX0KeygtXFZhbHtifSkrKCtcVmFse2N9KSsoK1xWYWx7ZH0pfQp7XElme3RlMn0KeygtXFZhbHtifSkrKCtcVmFse2R9KSsoLVxWYWx7Y30pfQp7KCtcVmFse2R9KSsoLVxWYWx7Yn0pKygtXFZhbHtjfSl9fX0kIH0KClxcIFx0ZXh0ezxpPkVuIHJlZ3JvdXBhbnQgbGVzIHRlcm1lcyBkZSBtw6ptZSBzaWduZSA6IDwvaT59CgpcXCBcdGV4dHsgCgokXElme3RhMX0Ke1xJZnt0ZTF9CnsoK1xWYWx7Yn0pKygrXFZhbHtjfSkrKC1cVmFse2R9KX0Ke1xJZnt0ZTJ9CnsoK1xWYWx7Yn0pKygtXFZhbHtkfSkrKCtcVmFse2N9KX0KeygtXFZhbHtkfSkrKCtcVmFse2J9KSsoK1xWYWx7Y30pfX19Cgp7XElme3RlMX0KeygtXFZhbHtifSkrKCtcVmFse2N9KSsoK1xWYWx7ZH0pfQp7XElme3RlMn0KeygtXFZhbHtifSkrKCtcVmFse2R9KSsoLVxWYWx7Y30pfQp7KCtcVmFse2R9KSsoLVxWYWx7Yn0pKygtXFZhbHtjfSl9fX09KCtcdGV4dGJme1xJZnt0YTF9e1xWYWx7YmN9fXtcVmFse2R9fX0pKygtXHRleHRiZntcSWZ7dGExfXtcVmFse2R9fXtcVmFse2JjfX19KSR9IApcXCBcdGV4dHsgJFxJZnt0YTF9CntcSWZ7dGUxfQp7KCtcVmFse2J9KSsoK1xWYWx7Y30pKygtXFZhbHtkfSl9CntcSWZ7dGUyfQp7KCtcVmFse2J9KSsoLVxWYWx7ZH0pKygrXFZhbHtjfSl9CnsoLVxWYWx7ZH0pKygrXFZhbHtifSkrKCtcVmFse2N9KX19fQoKe1xJZnt0ZTF9CnsoLVxWYWx7Yn0pKygrXFZhbHtjfSkrKCtcVmFse2R9KX0Ke1xJZnt0ZTJ9CnsoLVxWYWx7Yn0pKygrXFZhbHtkfSkrKC1cVmFse2N9KX0KeygrXFZhbHtkfSkrKC1cVmFse2J9KSsoLVxWYWx7Y30pfX19PShcSWZ7dHNpZ25lfXsrfXstfVx0ZXh0YmZ7XFZhbHtyZTIxfX0pJAoKIH0KClxcIFx0ZXh0ezxiPkEgbGEgZGVybmnDqHJlIMOpdGFwZSwgYydlc3QgdW5lIHNvdXN0cmFjdGlvbiBkZSBkaXN0YW5jZXMgw6AgMCBwdWlzcXVlIGxlcyB0ZXJtZXMgc29udCBkZSBzaWduZXMgY29udHJhaXJlcy48L2I+fQpcZW5ke2FycmF5fQAAAAgA#####wEAAAABAAD#####EEBKgAAAAAAAQGDgAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAADRcYmVnaW57YXJyYXl9e2x9CmE9XFZhbHthfSAKXFwgZT1cVmFse2V9ClxlbmR7YXJyYXl9AAAAAgD#####AAlyZXNsaXN0MjEAEnNpKHJlMTE+PXJlMTIsMSwyKQAAAAYAAAAABAcAAAAFAAAAHQAAAAUAAAAeAAAAAT#wAAAAAAAAAAAAAUAAAAAAAAAA#####wAAAAEACkNQb2ludEJhc2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBmQAAAAAAAQGEszMzMzM3#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAABEAAAAQAAAAEAAAAyAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQDwAAAAAAAAAAAAz#####wAAAAEADENUcmFuc2xhdGlvbgD#####AAAAMgAAADT#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAANAAAADUAAAANAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADYAAAA1AAAADQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAA3AAAANQAAAA0A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAOAAAADUAAAAMAP####8AAAA0AAAAMgAAAA0A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAMgAAADoAAAANAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADsAAAA6AAAADQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAA8AAAAOgAAAA0A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAPQAAADoAAAANAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAD4AAAA6AAAACAD#####AQAAAAA#8AAAAAAAAEAiAAAAAAAAAAAAAAAyEAAAAAAAAQAAAAAAAAABAAAAAAAAAAAAATAAAAAIAP####8BAAAAAMAUAAAAAAAAQBwAAAAAAAAAAAAAAD8QAAAAAAABAAAAAAAAAAEAAAAAAAAAAAACLTUAAAAIAP####8BAAAAAL#wAAAAAAAAQCAAAAAAAAAAAAAAADkQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABNQAAAAoA#####wEAAAABEAAAAQAAAAEAAAAyAD#wAAAAAAAAAAAACwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABwDoAAAAAAAAAAABDAAAADAD#####AAAAMgAAADkAAAANAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAEQAAABFAAAADAD#####AAAAMgAAAD8AAAANAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAEQAAABH#####wAAAAEACENTZWdtZW50AP####8BAAAAABAAAAEAAAEBAAAAPwAAAEgAAAAOAP####8BAAD#ABAAAAEAAAEBAAAAMgAAAEQAAAAOAP####8BAAAAABAAAAEAAAEBAAAAOQAAAEb#####AAAAAQAIQ1ZlY3RldXIA#####wEAAP8AEAAAAQAAAAIAAABIAAAARAAAAAAPAP####8BAAD#ABAAAAEAAAACAAAARAAAAEgAAAAADwD#####AQAA#wAQAAABAAAAAgAAAEQAAABGAAAAAA8A#####wEAAP8AEAAAAQAAAAIAAABGAAAARAD#####AAAAAQAHQ01pbGlldQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABIAAAARAAAABAA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAARAAAAEYAAAAIAP####8BAAD#AAAAAAAAAAAAwBgAAAAAAAAAAAAAAFAQAAAAAAABAAAAAgAAAAEAAAAAAAAAAAABNQAAAAgA#####wEAAP8AAAAAAAAAAADAFAAAAAAAAAAAAAAAURAAAAAAAAEAAAACAAAAAQAAAAAAAAAAAAE1AAAADgD#####AQAAAAAQAAABAAAAAQAAAD8AAAA5#####wAAAAIADENDb21tZW50YWlyZQD#####AQAAAAEAAP####8SQBwAAAAAAABARzMzMzMzNAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAgiNVRXhlbXBsZSA6I04gTGEgZGlzdGFuY2UgZGUgLTUgZXQgZGUgNSDDoCAwIGVzdCDDqWdhbGUgw6AgNS4KI1VPdSBlbmNvcmUgOiNOIExhIHBhcnRpZSBudW3DqXJpcXVlIGRlIC0gNSBldCBkZSA1IGVzdCDDqWdhbGUgw6AgNS7#####AAAAAQAQQ01hY3JvQXBwYXJpdGlvbgD#####AAAA#wEAAP####8QQCIAAAAAAABALMzMzMzMzAIB7+#7AAAAAAAAAAAAAAABAAAAAAAAAAAAMVZvaXIgdW4gcmFwcGVsIHN1ciBsYSBub3Rpb24gZGUgZGlzdGFuY2Ugw6AgesOpcm8AAAAAABoAAABVAAAAVAAAADIAAAA7AAAAPAAAAD0AAAA+AAAAPwAAAEEAAAA0AAAANgAAADcAAAA4AAAAOQAAAEsAAABIAAAATQAAAEwAAABSAAAATwAAAE4AAABTAAAAQAAAAEIAAABKAAAASQD#####AAAAAQARQ01hY3JvRGlzcGFyaXRpb24A#####wAAAP8BAAD#####EEB2YAAAAAAAQCrMzMzMzMACAe#v+wAAAAAAAAAAAAAAAQAAAAAAAAAAABFNYXNxdWVyIGxlIHJhcHBlbAAAAAAAGgAAAFUAAABUAAAAMgAAADsAAAA8AAAAPQAAAD4AAAA#AAAAQQAAADQAAAA2AAAANwAAADgAAAA5AAAASwAAAEgAAABNAAAATAAAAFIAAABPAAAATgAAAFMAAABAAAAAQgAAAEoAAABJ################'
          sq.listesLatex = false
          sq.charset1 = '\\d'
          sq.charset2 = '\\d'
          sq.btnPuis = false
          sq.btnFrac = false
          sq.btnRac = false
          sq.listesLatex = true
          sq.titre = 'Ressource à personnaliser'
          j3pShowError('Cette ressource doit être personnalisée en éditant la ressource dans l’éditeur de graphes.')
        }
        initDom()
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        cacheSolution()
        sq.etape = 1
        sq.marked = {}
        if (sq.estExerciceConstruction) {
          // On recharge la même figure pour tout réinitialiser
          sq.mtgApp.setFigByCode(sq.fig, false) // false pour ne pas réafficher
          sq.mtgApp.calculate(true)
        } else {
          sq.mtgAppLecteur.removeDoc('mtg32svg')
          j3pEmpty('divmtg32') // Car il peut rester des éditeurs internes à la figure qui nsont plaqués sur le svg
          // mais n’en font pas partie
          // Il faut recréer le svg de la figure
          j3pCreeSVG('divmtg32', {
            id: 'mtg32svg',
            width: sq.width,
            height: sq.height
          })
          sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, false)
        }
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            giveFormula2('r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') giveFormula2(car, par)
            }
          }
        }

        calculate(true) // true pour que les calculs aléatoires soient réinitialisés
        etablitNombreEtapesEtCoeff()
        j3pEmpty('enonce')
        j3pEmpty('enonceSuite')
        j3pEmpty('formules')
        j3pEmpty('info')

        sq.nbexp = 0
        sq.reponse = -1
        afficheMathliveDans('enonce', 'texte', extraitDeLatexPourMathlive(getMtgList(), 'enonce1'))
        const figMasqueeEtape1 = valueOf('figureMasqueeEtape1', true) === 1
        if (figMasqueeEtape1) j3pElement('divmtg32').style.display = 'none'
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        montreEditeurs(true)
        creeEditeurs()
        display() // Pour tout réafficher
        executeMacro('macroEtape1') // Si une telle macro existe on l’exécute
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {}
        focusPremierEditeur()
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      if (validationEditeurs()) {
        if (sq.estExerciceConstruction && estEtapeConstruction(sq.etape)) {
          const missingTypes = sq.mtgApp.getMissingTypes()
          if (missingTypes.length !== 0) {
            let ch = 'Il manque un ou des éléments de type : '
            j3pElement('correction').style.color = this.styles.cfaux
            for (let k = 0; k < missingTypes.length; k++) {
              ch = ch + missingTypes[k]
              if (k !== missingTypes.length - 1) ch = ch + ', '; else ch = ch + '.'
            }
            j3pElement('correction').innerHTML = ch
            return
          }

          const missingNames = sq.mtgApp.getMissingNames()
          if (missingNames.length !== 0) {
            let ch = textes.nommer
            j3pElement('correction').style.color = this.styles.cfaux
            for (let k = 0; k < missingNames.length; k++) {
              ch = ch + missingNames[k]
              if (k !== missingNames.length - 1) ch = ch + ', '; else ch = ch + '.'
            }
            j3pElement('correction').innerHTML = ch
            return
          }
        }
        validation()
        if (sq.resolu) { bilanReponse = 'exact' } else {
          if (sq.exact) {
            bilanReponse = 'exactPasFini'
          } else { bilanReponse = 'faux' }
        }
      } else {
        bilanReponse = 'incorrect'
      }

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        montreEditeurs(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        afficheSolution(false)
        j3pAddContent('correction', tempsDepasse, { replace: true })
        if (sq.solution) j3pAddContent('correction', textes.solutionIci)
        else j3pAddContent('correction', textes.solDesact)
        return this.finCorrection('navigation', true)
      }

      if (bilanReponse === 'incorrect') {
        j3pElement('correction').style.color = this.styles.cfaux
        let ch = textes.repIncorrecte
        if (sq.signeEgalManquant) ch += textes.egaliteNecess
        if (sq.signeInegManquant) ch += textes.inegaliteNecess
        j3pAddContent('correction', ch, { replace: true })
        // on reste dans l’état correction
        return this.finCorrection()
      }

      // Bonne réponse
      if (bilanReponse === 'exact') {
        const scoreprec = sq.scoreInit
        const add = (sq.etape === 1 ? sq.coefEtape1 : (sq.etape === sq.nbEtapes ? sq.coefEtapeFin : sq.coefEtapesSuivantes))
        // add = Math.floor(add * 100 + 0.5) / 100 On n’arrondit plus à 0,01 près car sinon le score final peut être tronqué
        let newscore = this.score + add
        if (newscore > scoreprec + 1) newscore = scoreprec + 1
        this.score = newscore
        j3pElement('correction').style.color = this.styles.cbien
        j3pAddContent('correction', textes.cbien, { replace: true })
        sq.numEssai++ // Pour un affichage correct dans afficheReponse
        desactiveEditeurs() // Pour les étapes internes
        afficheReponse('exact', false)
        if (sq.etape === sq.nbEtapes) {
          const score = this.score
          // Si le score est quasi entier c’est sans doute du à des erreurs d’arrondi
          if (Math.abs(score - Math.round(score)) < 0.000001) {
            this.score = Math.round(score)
          }
          j3pEmpty('enonceSuite')
          montreEditeurs(false)
          j3pElement('boutonrecopier').style.display = 'none'
          j3pElement('info').style.display = 'none'
          afficheSolution(true)
          return this.finCorrection('navigation', true)
        }
        etapeSuivante()
        // on appelle finCorrection alors qu’on vient de modifier l’énoncé (sans passer par la relance d’un énoncé par le modèle)
        // pas génial, mais ça va mettre à jour l’affichage du score (on reste dans l’état correction)
        return this.finCorrection()
      }

      sq.numEssai++
      if (bilanReponse === 'exactPasFini') {
        j3pElement('correction').style.color =
                (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) ? '#0000FF' : this.styles.cfaux
        j3pAddContent('correction', textes.bonPasFini, { replace: true })
      } else {
        j3pElement('correction').style.color = this.styles.cfaux
        j3pAddContent('correction', textes.cfaux, { replace: true })
        if (sq.borneFermeeSurInf) j3pAddContent('correction', textes.infPasNombre, { replace: false })
      }
      j3pEmpty('info')
      if (sq.numEssai <= sq['nbEssais' + sq.etape]) {
        afficheNombreEssaisRestants()
        afficheReponse(bilanReponse, false)
        videEditeurs()
        // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
        j3pAddContent('correction', '\n' + '<br>' + essaieEncore)
        // il reste des essais, on reste dans l’état correction
        return this.finCorrection()
      }

      // Erreur au dernier essai
      afficheReponse(bilanReponse, true)
      if (sq.etape === sq.nbEtapes) {
        // c'était aussi la dernière étape
        montreEditeurs(false)
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        afficheSolution(false)
        if (sq.solution) j3pAddContent('correction', textes.solutionIci)
        else j3pAddContent('correction', textes.solDesact)
        return this.finCorrection('navigation', true)
      }

      // dernier essai mais pas dernière étape
      j3pAddContent('correction', textes.passQuestSuiv)
      etapeSuivante()
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
