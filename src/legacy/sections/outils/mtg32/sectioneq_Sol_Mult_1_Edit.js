import $ from 'jquery'
import { MathfieldElement, renderMathInDocument } from 'mathlive'

import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'

import textesGeneriques from 'src/lib/core/textes'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/**
 Yves Biton
 Mars 2021

 eq_Sol_Mult_1_Edit
 Squelette demandant s’il y a ou non au moins une solution à un problème (le plus souvent une équation) et ensuite demandant la valeur exacte
 des solutions. Un seul éditeur est utilisé et les solutions doivent être séparées par des ;
 Utilise une fichier annexe dont le nom est contenu dans le paramètre ex.
 Ce fichier annexe contient dans la variable txt le code Base 64 de la figure mtg32 associée et dans la variable param
 le nom des calculs à une lettre qui peuvent être paramétrés par l’utilisateur à choisir parmi les lettres abcdefghijklmnopq.
 Pour savoir quel est le nombre de solution on interroge la valeur du calcul nbSol de la figure (0 s’il n’y a pas de solution).
 Pour savoir si une réponse est exacte et bien écrite sous la forme demandée, on interroge la valeur du calcul resolu qui vaut 0
 Si le calcul ne correspond pas à une forme attendue et sinon renvoie le n° correspondant de la solution (de 1 à nbsol).
 Pour savoir si une réponse est exacte, c’est-à-dire contient une formule du type x= ou =x donnant une des solutions,
 on met sa formule dans le calcul rep et on interroge, après recalcul de la figure, la valeur
 du calcul exact qui vaut 0 si la formule proposée ne correspond pas à une des solutions et sinon renvoie le n° correspondant de la solution (de 1 à nbsol).
 Par exemple, exact contiendra 1 si dans la réponse on a une réponse de la forme x = valeur de la solution 1.
 Si, par exemple exact vaut 1 et resolu ne vaut pas 1, c’estq ue l’élève a bien écrit x = valeur de la solution 1 mais ne l’a
 pas écrit sous la forme la plus simple demandée.
 L’élève peut aussi entrer des équations intermédiaires. Si, par exemple, nbSol vaut 2, la figure mtg32 doit contenir un calcul nommé racine1
 qui vaut 1 si la première solution est racine de l’équation proposée et 0 sinon et un calcul racine2 contenant 1 si la deuxième solution
 est racine de l’équation proposée et 0 sinon. Une réponse intermédiaire (avec des équations séparées par des ;) est acceptée si toutes les
 solutions de l’équation sont racines d’au moins une des équations proposées entre les;
 Si la figure contenue dans ex contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbvar
 au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
 toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
 */

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
MathfieldElement.soundsDirectory = '/static/mathlive/sounds'

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['nbEssais', 4, 'entier', 'Nombre d’essais maximum autorisés'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['titre', '', 'string', 'Titre de l’activité'],
    ['fig', '', 'editor', 'Chaîne Base 64 contenant la figure de l’exercice', initEditor],
    ['width', 800, 'entier', 'Largeur de la figure initiale'],
    ['height', 600, 'entier', 'Hauteur de la figure initiale'],
    ['param', 'abcd', 'string', 'Chaîne formé des paramètres que l’exercice utilise (de a à r, voir ci-dessous'],
    ['nbLatex', 1, 'entier', 'Nombre d’affichages LaTeX de la figure utilisés dans la consigne (1 au moins et jusque 4)'],
    ['charset', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul, chaîne vide pour autoriser tous les caractères'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['consigneNbSol0', 'L’équation n’admet pas de solution', 'string', 'Ce qui est affiché en premier dans la liste déroulante'],
    ['consigneNbSol1', 'L’équation admet au moins une solution', 'string', 'Ce qui est affiché en second dans la liste déroulante'],
    ['juste', 'C’est juste', 'string', 'Phrase à afficher quand la réponse est bonne'],
    ['faux', 'C’est faux', 'string', 'Phrase à afficher quand la réponse est fausse'],
    ['phraseSolution0', 'Il n’y a pas de solution.', 'string', 'Phrase à afficher quand il n’y a pas de solution'],
    ['phraseSolution1', 'Il y a au moins une solution.', 'string', 'Phrase à afficher quand il n’y a pas de solution'],
    ['consigne1', 'On veut résoudre dans $\\R$ l’équation $£a$.', 'string', 'Consigne initiale à afficher avant de demander s’il y a des solutions'],
    ['consigne2', 'L’équation $£a$ admet une ou plusieurs solutions.<br> Entrer ci-dessous la ou les solutions sous la forme $x$ = ... ou ...= $x$ séparées par des ; s’il y en a plusieurs.', 'string', 'Première ligne de la consigne à l’étape 2'],
    ['consigne3', '<br>Chaque solution doit être donnée sous la forme la plus simple possible (fraction irréductible, entier ou décimal).', 'string', 'Ligne rajoutée à la consigne à l’étape 2 dans le cas où le paramètre simplifier est à true'],
    ['consigne4', '<br>On peut aussi entrer des équations séparées par des ; pour résoudre progressivement.', 'string', 'Troisième ligne de la consigne à l’étape 2'],
    ['consigne5', '<br>Appuyer sur le bouton <B>OK</B> pour valider la réponse.', 'string', 'Dernière ligne éventuelle de la consigne à l’étape 2'],
    ['nomSolutions', 'solutions', 'string', 'Ce qui est affiché dans les bilans pour solutions au pluriel'],
    ['btnPuis', true, 'boolean', 'Bouton puissance'],
    ['btnFrac', true, 'boolean', 'Bouton fraction'],
    ['btnPi', false, 'boolean', 'Bouton pi'],
    ['btnCos', false, 'boolean', 'Bouton cosinus'],
    ['btnSin', false, 'boolean', 'Bouton sinus'],
    ['btnTan', false, 'boolean', 'Bouton tangente'],
    ['btnRac', true, 'boolean', 'Bouton Racine carrée'],
    ['btnExp', false, 'boolean', 'Bouton exponentielle'],
    ['btnLn', false, 'boolean', 'Bouton logarithme népérien'],
    ['btnAbs', false, 'boolean', 'Bouton valeur absolue'],
    ['btnConj', false, 'boolean', 'Bouton conjugué'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r']
  ]
}

const inputId = 'expressioninputmq1'

// cf src/legacy/sections/outils/mtg32/sectionex_Calc_Multi_Edit_Multi_Etapes.js pour ajouter un validator
/**
 * Ajoute l’éditeur mathgraph dans container, la promesse sera résolue avec la figure
 * @param {HTMLElement} container
 * @param {string} initalFig
 * @return {Promise<string>}
 */
async function initEditor (container, fig) {
  const { default: editor } = await import('./mtgEditor.js')
  return editor(container, { fig })
}

// sectioneq_Sol_Mult_1_Edit
/**
 * section eqSolMult1Edit
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup (ev) {
    if (sq.marked) demarqueEditeurPourErreur()
    if (ev.keyCode === 13) { // Touche Enter
      if (!validationEditeur()) {
        marqueEditeurPourErreur()
        focusIfExists(inputId)
      }
    }
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  function validationEditeur () {
    sq.signeEgalOublie = false
    sq.auMoins2Eg = false
    let res = true
    const rep = getMathliveValue(inputId)
    if (rep === '') {
      res = false
      marqueEditeurPourErreur()
    } else {
      const tab = rep.split(';')
      for (let i = 0; (i < tab.length) && res; i++) {
        if (tab[i] === '') {
          res = false
        } else {
          const chcalcul = traiteMathlive(tab[i])
          const ch = chcalcul
          let nbegal = 0
          let k = ch.indexOf('=')
          while (k !== -1) {
            nbegal++
            k = ch.indexOf('=', k + 1)
          }
          if (nbegal === 0) {
            res = false
            sq.signeEgalOublie = true
          } else {
            if (nbegal > 1) {
              res = false
              sq.auMoins2Eg = true
            } else {
              const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
              if (!valide) {
                res = false
              }
            }
          }
        }
      }
    }
    if (!res) {
      marqueEditeurPourErreur()
      focusIfExists(inputId)
    }
    return res
  }

  function validation () {
    let j, chcalcul, k, ind
    function tabContient (tab, ent) {
      for (let i = 1; i <= tab.length; i++) {
        if (tab[i - 1] === ent) return true
      }
      return false
    }
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }

    sq.rep = [] // Tableau destiné à recevoir les réponses dans les champs d’édition des solutions
    sq.reponseExacte = []
    sq.reponseResolue = []
    sq.reponseCorrecte = []
    const rep = getMathliveValue(inputId)
    sq.rep = rep
    const tab = rep.split(';')
    sq.correct = true
    for (j = 0; j < tab.length; j++) {
      chcalcul = traiteMathlive(tab[j])
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      let auMoinsUneCorrecte = false
      for (k = 1; k <= sq.nbSol; k++) {
        const estrac = sq.mtgAppLecteur.valueOf('mtg32svg', 'racine' + k, true)
        auMoinsUneCorrecte = auMoinsUneCorrecte || (estrac === 1)
        sq.reponseCorrecte.push(estrac === 1 ? k : 0)
      }
      sq.correct = sq.correct && auMoinsUneCorrecte
    }
    if (sq.correct) {
      for (ind = 1; ind <= sq.nbSol; ind++) {
        sq.correct = sq.correct && tabContient(sq.reponseCorrecte, ind)
      }
    }
    if (tab.length !== sq.nbSol) {
      sq.exact = false
      sq.resolu = false
    } else {
      for (j = 0; j < tab.length; j++) {
        chcalcul = traiteMathlive(tab[j])
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        sq.reponseExacte.push(sq.mtgAppLecteur.valueOf('mtg32svg', 'exact', true))
        sq.reponseResolue.push(sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu', true))
      }
      sq.exact = true
      sq.resolu = true
      for (ind = 1; ind <= sq.nbSol; ind++) {
        sq.exact = sq.exact && tabContient(sq.reponseExacte, ind)
        sq.resolu = sq.resolu && tabContient(sq.reponseResolue, ind)
      }
    }
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param {string} tagLatex le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  function afficheReponse (bilan, depasse) {
    let ch, coul
    if ((bilan === 'exact')) {
      ch = 'Exact : '
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'correct') {
        ch = 'Vérifié par les ' + sq.nomSolutions + ' : '
        coul = depasse ? parcours.styles.cfaux : '#0000FF'
      } else {
        if (bilan === 'faux') {
          ch = 'Faux : '
          coul = parcours.styles.cfaux
        } else { // exact pas fini
          ch = 'Exact pas fini : '
          coul = depasse ? parcours.styles.cfaux : '#0000FF'
        }
      }
    }
    const num = sq.numEssai
    if (num > 2) ch = '<br>' + ch
    const idrep = 'exp' + num
    afficheMathliveDans('formules', idrep, ch + '$' + sq.rep + '$', {
      style: {
        color: coul
      }
    })
  }

  function montreEditeur (bVisible) {
    if (bVisible) $('#divInfo').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    $('#boutonsmathquill').css('display', 'block')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    j3pElement(inputId).value = ''
    focusIfExists(inputId)
  }

  function videEditeur () {
    j3pElement(inputId).value = ''
    demarqueEditeurPourErreur()
    focusIfExists(inputId)
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.value = sq.rep
    focusIfExists(mf)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function initMtg () {
    setMathliveCss()
    // on charge mathgraph
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      sq.marked = false
      sq.nbSolEntre = false // Sera mis à true quand le nombre de solutions aura été proposé par l’élève.
      // sous la forme demandée. Ne sert que si sq.simplifier est à true;
      let par, car, i, j, k, nbrep, ar, tir, nb, nbcas
      // On crée provisoirement le DIV ci-dessous pour pouvoir calculer la figure.
      // On le détruira ensuite pour le récréer à la bonne position
      j3pDiv('conteneur', {
        id: 'formules',
        contenu: '',
        style: parcours.styles.petit.enonce
      }) // Contient le formules entrées par l’élève
      j3pDiv('conteneur', 'divSolution', '')
      $('#divSolution').css('display', 'none')
      j3pDiv('conteneur', 'divmtg32', '')
      j3pCreeSVG('divmtg32', {
        id: 'mtg32svg',
        width: sq.width,
        height: sq.height
      })
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar', true)
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i, true)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else { sq.aleat = false }

      if (sq.param) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculate('mtg32svg', true)

      sq.nbSol = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbSol', true) // Le nombre de solutions de l’équation.

      const tab = ['Choisir ci-dessous s’il y a ou non des ' + sq.nomSolutions, sq.consigneNbSol0, sq.consigneNbSol1]
      const nbLatex = parseInt(sq.nbLatex)
      // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
      const param = []
      for (i = 0; i < nbLatex; i++) {
        param.push(sq.mtgAppLecteur.getLatexCode('mtg32svg', i))
      }
      afficheMathliveDans('divConsigneNbSol', 'consigneNbSol', sq.consigne1 + '\n#1#',
        {
          liste1: { texte: tab },
          a: param[0],
          b: param[1],
          c: param[2],
          d: param[3]
        })
      j3pElement('consigneNbSolliste1').reponse = (sq.nbSol === 0) ? 1 : 2
      sq.fcts_valid = new ValidationZones({ parcours, zones: ['consigneNbSolliste1'] })

      // Le div qui contiendra l’éditeur MathQuill.
      // Cet éditeur sera masqué au début.
      j3pDiv('conteneur', 'conteneurbouton', '')
      j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
        recopierReponse)
      j3pElement('boutonrecopier').style.display = 'none'
      // On crée la liste des boutons disponibles
      // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
      sq.listeBoutons = []
      // Les boutonsMathQuill
      const tabBoutons1 = ['puissance', 'fraction', 'pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs', 'conj']
      ;['btnPuis', 'btnFrac', 'btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj'].forEach((p, i) => {
        if (sq[p]) sq.listeBoutons.push(tabBoutons1[i])
      })
      sq.boutonsMathQuill = sq.listeBoutons.length !== 0
      j3pDiv('conteneur', 'editeur', '')
      afficheMathliveDans('editeur', 'expression', 'Réponse proposée : &1&', {
        charset: sq.charset,
        listeBoutons: sq.listeBoutons
      })
      if (sq.charset !== '') {
        mathliveRestrict(inputId, sq.charset)
      }
      j3pDiv('editeur', 'boutonsmathquill', '')
      // Au départ les boutons MathQuill sont masqués
      $('#boutonsmathquill').css('display', 'none').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
      if (sq.boutonsMathQuill) j3pPaletteMathquill('boutonsmathquill', inputId, { liste: sq.listeBoutons, nomdiv: 'palette' })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
      j3pElement(inputId).onkeyup = function (ev) {
        onkeyup.call(this, ev)
      }
      // Au départ les éditeurs sont masqués
      $('#editeur').css('padding-top', '10px').css('display', 'none')

      // ON récupère le div de la figure pour le reclasser après les autres div
      const div = j3pElement('conteneur').removeChild(j3pElement('divmtg32'))
      j3pElement('conteneur').appendChild(div)
      sq.mtgAppLecteur.display('mtg32svg')
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)
    j3pDiv(parcours.zones.MG, { id: 'conteneur', contenu: '', style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
    // Quatre lignes suivantes déplacées ici depuis initMtg pour éviter que j3pAffiche("", "divInfo" ne soit appelé avant la création du div divInfo
    j3pDiv('conteneur', 'divConsigneNbSol', '') // Le div qui contiendra la liste déroulante pour le nombre de solutions
    j3pDiv('conteneur', 'divSuiteEnonce', '')
    j3pDiv('conteneur', 'divInfo', '')
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        // compteur. Si   numEssai > nbEssais, on corrige
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.simplifier = parcours.donneesSection.simplifier
        // if (parcours.donneesSection.nbchances >parcours.donneesSection.nbEssais) parcours.donneesSection.nbchances = parcours.donneesSection.nbEssais;
        sq.titre = parcours.donneesSection.titre
        sq.fig = parcours.donneesSection.fig
        sq.nbLatex = Number(parcours.donneesSection.nbLatex) || 0 // Le nombre de paramètres LaTeX dans le texte
        sq.width = parcours.donneesSection.width
        sq.height = parcours.donneesSection.height
        sq.nomSolutions = parcours.donneesSection.nomSolutions
        sq.consigneNbSol0 = parcours.donneesSection.consigneNbSol0
        sq.consigneNbSol1 = parcours.donneesSection.consigneNbSol1
        sq.phraseSolution0 = parcours.donneesSection.phraseSolution0
        sq.phraseSolution1 = parcours.donneesSection.phraseSolution1
        sq.juste = parcours.donneesSection.juste
        sq.faux = parcours.donneesSection.faux
        const tabBtn = ['Puis', 'Frac', 'Pi', 'Cos', 'Sin', 'Tan', 'Rac', 'Exp', 'Ln', 'Abs', 'Conj']
        tabBtn.forEach((p) => {
          sq['btn' + p] = parcours.donneesSection['btn' + p]
        })
        for (let i = 1; i <= 5; i++) sq['consigne' + i] = parcours.donneesSection['consigne' + i]
        sq.param = parcours.donneesSection.param
        sq.charset = parcours.donneesSection.charset
        // On rajoute le ; pour les solutions multiples des fois que l’auteur de la resoource l’ait oublié
        if (sq.charset !== '' && sq.charset.indexOf(';') === 0) sq.charset += ';'
        if (sq.fig === '') {
          sq.nbLatex = 1
          sq.fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAQDAAACgQAAAQEAAAAAAAAAAQAAAFb#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAFQ0ZvbmMA#####wAEemVybwASYWJzKHgpPDAuMDAwMDAwMDAx#####wAAAAEACkNPcGVyYXRpb24E#####wAAAAIACUNGb25jdGlvbgD#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAAAT4RLgvoJtaVAAF4#####wAAAAEAB0NDYWxjdWwA#####wAFbmJ2YXIAAjEwAAAAAUAkAAAAAAAAAAAABgD#####AAZuYmNhczEAATMAAAABQAgAAAAAAAAAAAAGAP####8ABm5iY2FzMgABNAAAAAFAEAAAAAAAAAAAAAYA#####wAGbmJjYXMzAAEyAAAAAUAAAAAAAAAAAAAABgD#####AAZuYmNhczQAATQAAAABQBAAAAAAAAAAAAAGAP####8ABm5iY2FzNQABMgAAAAFAAAAAAAAAAAAAAAYA#####wAGbmJjYXM2AAE0AAAAAUAQAAAAAAAAAAAABgD#####AAZuYmNhczcAATIAAAABQAAAAAAAAAAAAAAGAP####8ABm5iY2FzOAABNAAAAAFAEAAAAAAAAAAAAAYA#####wAGbmJjYXM5AAEyAAAAAUAAAAAAAAAAAAAABgD#####AAduYmNhczEwAAEzAAAAAUAIAAAAAAAAAAAABgD#####AAJyMQATaW50KHJhbmQoMCkqbmJjYXMxKQAAAAQCAAAAAwIAAAAEEQAAAAEAAAAAAAAAAD#Udc55u+4c#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAMAAAAGAP####8AAnIyABNpbnQocmFuZCgwKSpuYmNhczIpAAAABAIAAAADAgAAAAQRAAAAAQAAAAAAAAAAP8Tr1RbReKAAAAAHAAAABAAAAAYA#####wACcjMAE2ludChyYW5kKDApKm5iY2FzMykAAAAEAgAAAAMCAAAABBEAAAABAAAAAAAAAAA#1v6zkf0wwAAAAAcAAAAFAAAABgD#####AAJyNAATaW50KHJhbmQoMCkqbmJjYXM0KQAAAAQCAAAAAwIAAAAEEQAAAAEAAAAAAAAAAD#uEnjgr6aaAAAABwAAAAYAAAAGAP####8AAnI1ABNpbnQocmFuZCgwKSpuYmNhczUpAAAABAIAAAADAgAAAAQRAAAAAQAAAAAAAAAAP6VGWocpxiAAAAAHAAAABwAAAAYA#####wACcjYAE2ludChyYW5kKDApKm5iY2FzNikAAAAEAgAAAAMCAAAABBEAAAABAAAAAAAAAAA#7kWRwgcGUgAAAAcAAAAIAAAABgD#####AAJyNwATaW50KHJhbmQoMCkqbmJjYXM3KQAAAAQCAAAAAwIAAAAEEQAAAAEAAAAAAAAAAD#uxsWDrnzMAAAABwAAAAkAAAAGAP####8AAnI4ABNpbnQocmFuZCgwKSpuYmNhczgpAAAABAIAAAADAgAAAAQRAAAAAQAAAAAAAAAAP9VCgCiIUEwAAAAHAAAACgAAAAYA#####wACcjkAE2ludChyYW5kKDApKm5iY2FzOSkAAAAEAgAAAAMCAAAABBEAAAABAAAAAAAAAAA#2PY44HJPWAAAAAcAAAALAAAABgD#####AANyMTAAFGludChyYW5kKDApKm5iY2FzMTApAAAABAIAAAADAgAAAAQRAAAAAQAAAAAAAAAAP9+mo6QkO2AAAAAHAAAADAAAAAYA#####wAEZm9ybQANc2kocjE8PTEsMSwyKf####8AAAABAA1DRm9uY3Rpb24zVmFyAAAAAAMGAAAABwAAAA0AAAABP#AAAAAAAAAAAAABP#AAAAAAAAAAAAABQAAAAAAAAAAAAAACAP####8AA2lkeAABeAAAAAUAAAAAAAF4AAAABgD#####AAFhAARyMisyAAAAAwAAAAAHAAAADgAAAAFAAAAAAAAAAAAAAAYA#####wABYgAOKC0xKV5yMyoocjQrMSkAAAADAv####8AAAABAApDUHVpc3NhbmNl#####wAAAAEADENNb2luc1VuYWlyZQAAAAE#8AAAAAAAAAAAAAcAAAAPAAAAAwAAAAAHAAAAEAAAAAE#8AAAAAAAAAAAAAYA#####wABYwAOKC0xKV5yNSoocjYrMikAAAADAgAAAAkAAAAKAAAAAT#wAAAAAAAAAAAABwAAABEAAAADAAAAAAcAAAASAAAAAUAAAAAAAAAAAAAABgD#####AAJkMAAOKC0xKV5yNyoocjgrMSkAAAADAgAAAAkAAAAKAAAAAT#wAAAAAAAAAAAABwAAABMAAAADAAAAAAcAAAAUAAAAAT#wAAAAAAAAAAAABgD#####AAFkABZzaShhKmQwLWIqYz0wLGQwKzEsZDApAAAACAAAAAADCAAAAAMBAAAAAwIAAAAHAAAAGQAAAAcAAAAcAAAAAwIAAAAHAAAAGgAAAAcAAAAbAAAAAQAAAAAAAAAAAAAAAwAAAAAHAAAAHAAAAAE#8AAAAAAAAAAAAAcAAAAcAAAABgD#####AAFrAA8oLTEpXnI5KihyMTArMikAAAADAgAAAAkAAAAKAAAAAT#wAAAAAAAAAAAABwAAABUAAAADAAAAAAcAAAAWAAAAAUAAAAAAAAAAAAAAAgD#####AAJmMQAPKGEqeC1iKSooYyp4LWQpAAAAAwIAAAADAQAAAAMCAAAABwAAABkAAAAFAAAAAAAAAAcAAAAaAAAAAwEAAAADAgAAAAcAAAAbAAAABQAAAAAAAAAHAAAAHQABeAAAAAYA#####wACYycAA2sqYQAAAAMCAAAABwAAAB4AAAAHAAAAGQAAAAYA#####wACZCcAA2sqYgAAAAMCAAAABwAAAB4AAAAHAAAAGgAAAAIA#####wACZjIAEShhKngtYikqKGMnKngtZCcpAAAAAwIAAAADAQAAAAMCAAAABwAAABkAAAAFAAAAAAAAAAcAAAAaAAAAAwEAAAADAgAAAAcAAAAgAAAABQAAAAAAAAAHAAAAIQABeAAAAAYA#####wACZzEAE3BnY2QoYWJzKGEpLGFicyhiKSn#####AAAAAQANQ0ZvbmN0aW9uMlZhcgIAAAAEAAAAAAcAAAAZAAAABAAAAAAHAAAAGgAAAAYA#####wACZzIAE3BnY2QoYWJzKGMpLGFicyhkKSkAAAALAgAAAAQAAAAABwAAABsAAAAEAAAAAAcAAAAdAAAABgD#####AAJhMQAEYS9nMQAAAAMDAAAABwAAABkAAAAHAAAAIwAAAAYA#####wACYjEABGIvZzEAAAADAwAAAAcAAAAaAAAABwAAACMAAAAGAP####8AAmMxAARjL2cyAAAAAwMAAAAHAAAAGwAAAAcAAAAkAAAABgD#####AAJkMQAEZC9nMgAAAAMDAAAABwAAAB0AAAAHAAAAJAAAAAYA#####wACeDEABWIxL2ExAAAAAwMAAAAHAAAAJgAAAAcAAAAlAAAABgD#####AAJ4MgAFZDEvYzEAAAADAwAAAAcAAAAoAAAABwAAACcAAAAGAP####8AA2cxMgAFZzEqZzIAAAADAgAAAAcAAAAjAAAABwAAACQAAAACAP####8ABGZvcjEAF2cxMiooYTEqeC1iMSkqKGMxKngtZDEpAAAAAwIAAAADAgAAAAcAAAArAAAAAwEAAAADAgAAAAcAAAAlAAAABQAAAAAAAAAHAAAAJgAAAAMBAAAAAwIAAAAHAAAAJwAAAAUAAAAAAAAABwAAACgAAXj#####AAAAAwAQQ1Rlc3RFcXVpdmFsZW5jZQD#####AAd0ZXN0ZXEwAAAALAAAAB8BAQAAAAE#8AAAAAAAAAH#####AAAAAgAGQ0xhdGV4AP####8AAAAAAQAA#####xBAgHQAAAAAAEBdcKPXCj1wAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAApXElme2Zvcm19e1xGb3JTaW1we2YxfT0wfXtcRm9yU2ltcHtmMn09MH0AAAACAP####8ABWZhY3QxAAlhMSp4LWIxPTAAAAADCAAAAAMBAAAAAwIAAAAHAAAAJQAAAAUAAAAAAAAABwAAACYAAAABAAAAAAAAAAAAAXgAAAACAP####8ABWZhY3QyAAljMSp4LWQxPTAAAAADCAAAAAMBAAAAAwIAAAAHAAAAJwAAAAUAAAAAAAAABwAAACgAAAABAAAAAAAAAAAAAXgAAAACAP####8ABmZhY3QnMgAJYycqeC1kJz0wAAAAAwgAAAADAQAAAAMCAAAABwAAACAAAAAFAAAAAAAAAAcAAAAhAAAAAQAAAAAAAAAAAAF4AAAAAgD#####AARmb3IyABEoYSp4LWIpKmsqKGEqeC1iKQAAAAMCAAAAAwIAAAADAQAAAAMCAAAABwAAABkAAAAFAAAAAAAAAAcAAAAaAAAABwAAAB4AAAADAQAAAAMCAAAABwAAABkAAAAFAAAAAAAAAAcAAAAaAAF4AAAAAgD#####AARmb3IzAA1rKihhKngtYileMj0wAAAAAwgAAAADAgAAAAcAAAAeAAAACQAAAAMBAAAAAwIAAAAHAAAAGQAAAAUAAAAAAAAABwAAABoAAAABQAAAAAAAAAAAAAABAAAAAAAAAAAAAXgAAAACAP####8ABWZhY3QzAAdhKngtYj0wAAAAAwgAAAADAQAAAAMCAAAABwAAABkAAAAFAAAAAAAAAAcAAAAaAAAAAQAAAAAAAAAAAAF4AAAAAgD#####AAVmYWN0NAAJYycqeC1kJz0wAAAAAwgAAAADAQAAAAMCAAAABwAAACAAAAAFAAAAAAAAAAcAAAAhAAAAAQAAAAAAAAAAAAF4AAAAAgD#####AARmb3I1AAVhKng9YgAAAAMIAAAAAwIAAAAHAAAAGQAAAAUAAAAAAAAABwAAABoAAXgAAAACAP####8ABGZvcjYAB2MnKng9ZCcAAAADCAAAAAMCAAAABwAAACAAAAAFAAAAAAAAAAcAAAAhAAF4AAAAAgD#####AARmb3I3AAsoYSp4LWIpXjI9MAAAAAMIAAAACQAAAAMBAAAAAwIAAAAHAAAAGQAAAAUAAAAAAAAABwAAABoAAAABQAAAAAAAAAAAAAABAAAAAAAAAAAAAXgAAAANAP####8AAAAAAQAIc29sdXRpb27#####FEAlAAAAAAAAQCOFHrhR64QBAe#v+wAAAAAAAAAAAAAAAQAAAAAAAAAABIZcYmVnaW57YXJyYXl9e2x9Clx0ZXh0eyRcdGV4dGNvbG9ye2JsdWV9e1x0ZXh0e1NvbHV0aW9uIDp9fSR9ClxJZntmb3JtfQp7ClxcIFx0ZXh0e1VuIHByb2R1aXQgZGUgZmFjdGV1cnMgZXN0IG51bCBzaSBldCBzZXVsZW1lbnQgc2l9ClxcIFx0ZXh0e3VuIGRlcyBmYWN0ZXVycyBlc3QgbnVsLn0KXElme3Rlc3RlcTB9e317XFxcdGV4dHskIFxGb3JTaW1we2YxfT0wIFxMZWZ0cmlnaHRhcnJvdyBcRm9yU2ltcHtmb3IxfT0wJH19ClxcIFx0ZXh0eyRcRm9yU2ltcHtmMX09MCBcTGVmdHJpZ2h0YXJyb3cgXEZvclNpbXB7ZmFjdDF9XHRleHR7IG91IH1cRm9yU2ltcHtmYWN0Mn0kfQpcXCBcdGV4dHskXEZvclNpbXB7ZjF9PTAgXExlZnRyaWdodGFycm93IHg9XEZvclNpbXB7eDF9XHRleHR7IG91IH14PVxGb3JTaW1we3gyfSR9Cn0KewpcXCBcdGV4dHtVbiBwcm9kdWl0IGRlIGZhY3RldXJzIGVzdCBudWwgc2kgZXQgc2V1bGVtZW50IHNpfQpcXCBcdGV4dHt1biBkZXMgZmFjdGV1cnMgZXN0IG51bC59ClxcIFx0ZXh0eyRcRm9yU2ltcHtmMn09MCBcTGVmdHJpZ2h0YXJyb3cgXEZvclNpbXB7ZmFjdDN9XHRleHR7IG91IH1cRm9yU2ltcHtmYWN0NH0kfQpcXCBcdGV4dHskXEZvclNpbXB7ZjJ9PTAgXExlZnRyaWdodGFycm93IFxGb3JTaW1we2ZvcjV9XHRleHR7IG91IH1cRm9yU2ltcHtmb3I2fSR9ClxcIFx0ZXh0eyRcRm9yU2ltcHtmMn09MCBcTGVmdHJpZ2h0YXJyb3cgeD1cRm9yU2ltcHt4MX1cdGV4dHsgb3UgfXg9XEZvclNpbXB7eDF9JH0KXFwgXHRleHR7JFx0ZXh0e0lsIG4neSBhIGRvbmMgcXUndW5lIHNldWxlIHNvbHV0aW9uIH14PVxGb3JTaW1we3gxfS4kfQpcXCBcdGV4dHtNaWV1eCA6fQpcXCBcdGV4dHskXEZvclNpbXB7ZjJ9PTAgXExlZnRyaWdodGFycm93IFxGb3JTaW1we2ZvcjJ9PTAkfQpcXCBcdGV4dHskXEZvclNpbXB7ZjJ9PTAgXExlZnRyaWdodGFycm93IFxGb3JTaW1we2ZvcjN9JH0KXFwgXHRleHR7JFxGb3JTaW1we2YyfT0wIFxMZWZ0cmlnaHRhcnJvdyBcRm9yU2ltcHtmb3I3fSR9ClxcIFx0ZXh0eyRcRm9yU2ltcHtmMn09MCBcTGVmdHJpZ2h0YXJyb3cgXEZvclNpbXB7ZmFjdDN9JH0KXFwgXHRleHR7JFxGb3JTaW1we2YyfT0wIFxMZWZ0cmlnaHRhcnJvdyB4PVxGb3JTaW1we3gxfSR9Cn0KXGVuZHthcnJheX0AAAAGAP####8ABW5iU29sABMoZm9ybT0xKSoyKyhmb3JtPTIpAAAAAwAAAAADAgAAAAMIAAAABwAAABcAAAABP#AAAAAAAAAAAAABQAAAAAAAAAAAAAADCAAAAAcAAAAXAAAAAUAAAAAAAAAAAAAAAgD#####AANyZXAAATAAAAABAAAAAAAAAAAAAXgAAAACAP####8AA2dhdQAOZ2F1Y2hlKHJlcCh4KSkAAAAEFP####8AAAABAA5DQXBwZWxGb25jdGlvbgAAADsAAAAFAAAAAAABeAAAAAIA#####wADZHJvAA1kcm9pdChyZXAoeCkpAAAABBUAAAAOAAAAOwAAAAUAAAAAAAF4AAAAAgD#####AANkaWYADWdhdSh4KS1kcm8oeCkAAAADAQAAAA4AAAA8AAAABQAAAAAAAAAOAAAAPQAAAAUAAAAAAAF4AAAAAgD#####AAJpZAADeD0wAAAAAwgAAAAFAAAAAAAAAAEAAAAAAAAAAAABeP####8AAAABAAxDVGVzdEVxTmF0T3AA#####wAKZXN0ZWdhbGl0ZQAAADsAAAA#AAAABgD#####AAJ5MQAGMS4xMjczAAAAAT#yCWu5jH4oAAAABgD#####AAJ5MgAFMS41NTcAAAABP#jpeNT987YAAAAGAP####8AAnkzAAUxLjk3NwAAAAE##6HKwIMSbwAAAAYA#####wAGemVyb3gxAA16ZXJvKGRpZih4MSkpAAAADgAAAAEAAAAOAAAAPgAAAAcAAAApAAAABgD#####AAZ6ZXJveDIADXplcm8oZGlmKHgyKSkAAAAOAAAAAQAAAA4AAAA+AAAABwAAACoAAAAGAP####8AB3JhY2luZTEAGGVzdGVnYWxpdGUmemVybyhkaWYoeDEpKQAAAAMKAAAABwAAAEAAAAAOAAAAAQAAAA4AAAA+AAAABwAAACkAAAAGAP####8AB3JhY2luZTIAJWVzdGVnYWxpdGUmc2koZm9ybT0xLHplcm8oZGlmKHgyKSksMCkAAAADCgAAAAcAAABAAAAACAAAAAADCAAAAAcAAAAXAAAAAT#wAAAAAAAAAAAADgAAAAEAAAAOAAAAPgAAAAcAAAAqAAAAAQAAAAAAAAAAAAAAAgD#####AARzb2wxAAd4PWIxL2ExAAAAAwgAAAAFAAAAAAAAAAMDAAAABwAAACYAAAAHAAAAJQABeAAAAAIA#####wAEc29sMgAHeD1kMS9jMQAAAAMIAAAABQAAAAAAAAADAwAAAAcAAAAoAAAABwAAACcAAXgAAAAMAP####8ABHRlcTEAAABIAAAAOwEAAAAAAT#wAAAAAAAAAQAAAAwA#####wAEdGVxMgAAAEkAAAA7AQAAAAABP#AAAAAAAAABAAAABgD#####AAZyZXNvbHUAHXNpKGZvcm09MSx0ZXExKjErdGVxMioyLHRlcTEpAAAACAAAAAADCAAAAAcAAAAXAAAAAT#wAAAAAAAAAAAAAwAAAAADAgAAAAcAAABKAAAAAT#wAAAAAAAAAAAAAwIAAAAHAAAASwAAAAFAAAAAAAAAAAAAAAcAAABKAAAADAD#####AApnYXVjaGVlc3R4AAAAPAAAABgBAAAAAAE#8AAAAAAAAAEAAAAMAP####8ACWRyb2l0ZXN0eAAAAD0AAAAYAQAAAAABP#AAAAAAAAAB#####wAAAAEAF0NUZXN0RGVwZW5kYW5jZVZhcmlhYmxlAP####8ADnRlc3RkZXB4Z2F1Y2hlAAAAPAAAAAAAAAAQAP####8ADXRlc3RkZXB4ZHJvaXQAAAA9AAAAAAAAAAYA#####wALeGVnYWxnYXVjaGUAJWVzdGVnYWxpdGUmZ2F1Y2hlZXN0eCYxLXRlc3RkZXB4ZHJvaXQAAAADCgAAAAMKAAAABwAAAEAAAAAHAAAATQAAAAMBAAAAAT#wAAAAAAAAAAAABwAAAFAAAAAGAP####8ACnhlZ2FsZHJvaXQAJWVzdGVnYWxpdGUmZHJvaXRlc3R4JjEtdGVzdGRlcHhnYXVjaGUAAAADCgAAAAMKAAAABwAAAEAAAAAHAAAATgAAAAMBAAAAAT#wAAAAAAAAAAAABwAAAE8AAAAGAP####8ACXh1bmVzdHNvbAA2eGVnYWxnYXVjaGUmemVybyhkcm8oMCkteDEpfHhlZ2FsZHJvaXQmemVybyhnYXUoMCkteDEpAAAAAwsAAAADCgAAAAcAAABRAAAADgAAAAEAAAADAQAAAA4AAAA9AAAAAQAAAAAAAAAAAAAABwAAACkAAAADCgAAAAcAAABSAAAADgAAAAEAAAADAQAAAA4AAAA8AAAAAQAAAAAAAAAAAAAABwAAACkAAAAGAP####8AC3hkZXV4ZXN0c29sADZ4ZWdhbGdhdWNoZSZ6ZXJvKGRybygwKS14Mil8eGVnYWxkcm9pdCZ6ZXJvKGdhdSgwKS14MikAAAADCwAAAAMKAAAABwAAAFEAAAAOAAAAAQAAAAMBAAAADgAAAD0AAAABAAAAAAAAAAAAAAAHAAAAKgAAAAMKAAAABwAAAFIAAAAOAAAAAQAAAAMBAAAADgAAADwAAAABAAAAAAAAAAAAAAAHAAAAKgAAAAYA#####wAFZXhhY3QALHNpKGZvcm09MSx4dW5lc3Rzb2wreGRldXhlc3Rzb2wqMix4dW5lc3Rzb2wpAAAACAAAAAADCAAAAAcAAAAXAAAAAT#wAAAAAAAAAAAAAwAAAAAHAAAAUwAAAAMCAAAABwAAAFQAAAABQAAAAAAAAAAAAAAHAAAAU################w=='
          sq.width = 0
          sq.height = 0
          sq.charset = '0123456789+-*/^²()=;x.,'
          sq.titre = 'Ressource à personnaliser'
          j3pShowError('Cette ressource doit être personnalisée en éditant la ressource dans l’éditeur de graphes.')
        }
        initMtg()
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        cacheSolution()
        sq.nbSolEntre = false
        // $("#editeur").css("display", "block");
        montreEditeur(false)
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés

        j3pElement('divConsigneNbSol').innerHTML = ''
        j3pElement('divSuiteEnonce').innerHTML = ''
        j3pElement('formules').innerHTML = ''
        j3pElement('divInfo').innerHTML = ''
        // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
        sq.nbSol = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbSol', true) // Le nombre de solutions de l’équation.
        const tab = ['Choisir ci-dessous s’il y a ou non des ' + sq.nomSolutions, sq.consigneNbSol0, sq.consigneNbSol1]
        const nbLatex = parseInt(sq.nbLatex)
        // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
        const param = []
        for (let i = 0; i < nbLatex; i++) {
          param.push(sq.mtgAppLecteur.getLatexCode('mtg32svg', i))
        }
        afficheMathliveDans('divConsigneNbSol', 'consigneNbSol', sq.consigne1 + '\n#1#',
          {
            liste1: { texte: tab },
            a: param[0],
            b: param[1],
            c: param[2],
            d: param[3]
          })
        j3pElement('consigneNbSolliste1').typeReponse = [true]
        j3pElement('consigneNbSolliste1').reponse = (sq.nbSol === 0) ? 1 : 2
        sq.fcts_valid = new ValidationZones({ parcours, zones: ['consigneNbSolliste1'] })
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher

        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {}
        this.finEnonce()
      }
      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (!sq.nbSolEntre) {
        sq.fcts_valid.validationGlobale()
        const rep = j3pElement('consigneNbSolliste1').selectedIndex - 1
        if (rep === -1) { bilanReponse = 'nbSolPasEntre' } else {
          if (rep === ((sq.nbSol >= 1) ? 1 : 0)) {
            if (rep === 0) bilanReponse = 'nbSolExactFini' // Cas où il n’y a pas de solution
            else bilanReponse = 'nbSolExact'
          } else {
            bilanReponse = 'nbSolFaux'
          }
        }
      } else {
        // On regarde d’abord l’éditeur a un contenu vide ou incorrect
        if (validationEditeur()) {
          validation()
          if (sq.simplifier) {
            if (sq.resolu) { bilanReponse = 'exact' } else {
              if (sq.exact) {
                bilanReponse = 'exactPasFini'
                simplificationPossible = true
              } else {
                if (sq.correct) bilanReponse = 'correct'
                else bilanReponse = 'faux'
              }
            }
          } else {
            if (sq.resolu || sq.exact) {
              bilanReponse = 'exact'
              if (!sq.resolu) simplificationPossible = true
            } else {
              if (sq.correct) bilanReponse = 'correct'
              else bilanReponse = 'faux'
            }
          }
        } else {
          bilanReponse = 'incorrect'
        }
      }

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('divInfo').style.display = 'none'
        j3pElement('boutonrecopier').style.display = 'none'
        montreEditeur(false)
        afficheSolution(false)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        return this.finCorrection('navigation', true)
      }

      // pas de limite de temps
      if (!sq.nbSolEntre) {
        if (bilanReponse !== 'nbSolPasEntre') {
          sq.nbSolEntre = true
          if (bilanReponse === 'nbSolExactFini') {
            sq.nbSolExact = true
            this._stopTimer()
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien
            this.cacheBoutonValider()
            afficheSolution(true)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            this.finCorrection('navigation', true)
          }

          // pas nbSolExactFini
          if (bilanReponse === 'nbSolExact') {
            this.score += 0.5
            sq.nbSolExact = true
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = sq.juste + '<br>' + sq.phraseSolution1
            const nbParam = parseInt(sq.nbLatex)
            const t = ['a', 'b', 'c', 'd']
            const param = {}
            for (let i = 0; i < nbParam; i++) {
              param[t[i]] = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
            }
            param.e = sq.nbSol
            const st = sq.consigne2 + (sq.simplifier ? sq.consigne3 : '') +
                (sq.nbEssais > 1 ? sq.consigne4 : '') + sq.consigne5
            $('#divSuiteEnonce').css('color', 'black')
            afficheMathliveDans('divSuiteEnonce', 'texte', st, param)
            // On fait apparaître les éditeurs pour les solutions
            montreEditeur(true)
          } else {
            sq.nbSolExact = false
            j3pElement('correction').style.color = this.styles.cfaux
            if (sq.nbSol === 0) { // Faux alors qu’il n’y a pas de solution
              j3pElement('correction').innerHTML = sq.faux + '<br>' + sq.phraseSolution0
            } else {
              j3pElement('correction').innerHTML = sq.faux + '<br>' + sq.phraseSolution1
              const nbParam = parseInt(sq.nbLatex)
              const t = ['a', 'b', 'c', 'd']
              const param = {}
              for (let i = 0; i < nbParam; i++) {
                param[t[i]] = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
              }
              param.e = sq.nbSol
              const st = sq.consigne2 + (sq.simplifier ? sq.consigne3 : '') +
                  (sq.nbEssais > 1 ? sq.consigne4 : '') + sq.consigne5
              $('#divSuiteEnonce').css('color', 'black')
              afficheMathliveDans('divSuiteEnonce', 'texte', st, param)
            }
          }
          // On fait apparaître les éditeurs pour les solutions
          if (sq.nbSol === 0) {
            afficheSolution(sq.nbSolExact)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            return this.finCorrection('navigation', true)
          }

          montreEditeur(true)
          const nbe = sq.nbEssais - sq.numEssai + 1
          if (nbe === 1) afficheMathliveDans('divInfo', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
          else afficheMathliveDans('divInfo', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
        }
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          if (sq.signeEgalOublie) { j3pElement('correction').innerHTML = 'Ne pas oublier d’entrer les réponses avec des égalités.' } else {
            if (sq.auMoins2Eg) {
              j3pElement('correction').innerHTML = 'Il faut une seule égalité par équation.'
            } else {
              j3pElement('correction').innerHTML = 'Réponse incorrecte'
            }
          }
        } else {
          // Une réponse a été saisie
          sq.numEssai++
          if (bilanReponse === 'exact') {
            this.score += 0.5
            afficheReponse('exact', false)
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('correction').style.color = this.styles.cbien
            if (simplificationPossible) j3pElement('correction').innerHTML = 'C’est bien mais on pouvait  simplifier la réponse'
            else j3pElement('correction').innerHTML = cBien
            this.cacheBoutonValider()
            j3pElement('divInfo').style.display = 'none'
            afficheSolution(true)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            montreEditeur(false)
            return this.finCorrection('navigation', true)
          }

          // pas exact
          if (bilanReponse === 'exactPasFini') {
            j3pElement('correction').style.color =
                (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
            j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
          } else {
            if (bilanReponse === 'correct') {
              j3pElement('correction').style.color =
                  (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = 'Calcul exact mais pas résolu.'
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pElement('correction').innerHTML = cFaux
            }
          }
          j3pElement('divInfo').innerHTML = ''

          if (sq.numEssai <= sq.nbEssais) {
            const nbe = sq.nbEssais - sq.numEssai + 1
            if (nbe === 1) afficheMathliveDans('divInfo', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
            else afficheMathliveDans('divInfo', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
            videEditeur()
            afficheReponse(bilanReponse, false)
            j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
          } else {
            // Erreur au nème essai
            if (sq.nbSolExact) this.score -= 0.5 // si nbSolExact on avait aujouté 1 ou 0.5 précédemment
            this.cacheBoutonValider()
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('divInfo').style.display = 'none'
            afficheSolution(false)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            montreEditeur(false)
            afficheReponse(bilanReponse, true)
            j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
            return this.finCorrection('navigation', true)
          }
        }
      }
      // On arrive là si on reste dans l’état correction, faut appeler finCorrection quand même
      this.finCorrection()
      break
    } // case 'correction'

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
