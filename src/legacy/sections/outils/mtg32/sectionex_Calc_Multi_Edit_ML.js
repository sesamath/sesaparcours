import $ from 'jquery'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pPaletteMathquill, j3pRestrict, j3pRestriction, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'

import { setFocusById, setFocusToFirstInput } from 'src/legacy/sections/squelettes/helpersMtg'
import textesGeneriques from 'src/lib/core/textes'
import { j3pAffiche, unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { cleanLatexForMl, replaceOldMQCodes, transposeMatrices } from 'src/lib/outils/mathlive/utils'
import { convertRestriction } from 'src/lib/utils/regexp'
import { getFirstIndexParExcluded, getIndexFermant } from 'src/lib/utils/string'
import { setKeyboardContainer } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'

import 'src/lib/outils/mathlive/mathliveOverride.scss'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    2021
    section ex_Calc_Param_Multi_Edit
    Permet à l’utilisateur de créer ses propres exercices de calcul multi-éditeurs.
    Yves Biton

    Le paramètre fig doit contenir le code base 64 de la figure mtg32 associée.
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
    Utilise une fichier annexe dont le nom est contenu dans le paramètre ex.
    Le texte de la consigne peut être contenu dans les paramètres consigneLigne1 à consigneLigne4 (qui peuvent
    utiliser des affichages LaTeX de la figure via $£a$, $£b$, $£c$, $£d$, le paramètre nbLaTeX
    indiquant le npmbre de LaTeX auxquels on peut se référer.
    Ces affichages LatEx doivent être les premiers affichages LaTeX de la figure.
    $£b$ fait parexemple référence au deuxième affichage LaTeX de la figure.
    Mais il est plutôt conseillé de laisser tous ces paramètres vides et de fournir la consigne via un affichage
    LaTeX ayant pour tag consigne formé d’un tableau colonne de plusieurs lignes dont chaque ligne est un \text{}.
    Le formulaire à remplir pour les calculs peut être contenu dans le paramètre formulaire mais
    il est plutôt conseillé de la mettre dans un affichage LaTeX de tag formulaire contenant un \text{} dont le contenu
    est la consigne.
    Ce formulaire peut contenir, en mode texte, des éditeurs mathquill, repérés dans le formulaire par edit suivi
    du numéro de l’éditeur (les n°s commençant à 1) et, en mode maths, des \editable{} ce qui permet
    de mettre des éditeurs directement à l’intérieur de formules, comme par exemple un éditeur sous une racine carrée.
    Il faut aussi contenir des contenir des choix à liste déroulantes repérés par list suivi de n° de la liste.
    Les \editable{} eux n’ont pas à être numérotés.
    Si, par exemple, le champ d’édition MathQuill edit2 doit contenir une égalité, la figure mtg32 doit contenir
    un calcul nommé estEgalite2 qui renvoie comme valeur 1
    Imaginons par exemple que le LaTeX de tag formulaire contienne ceci :
    \text{La fonction $f$ est list1 sur $\R$, \Val{a} a edit1 antécédents par $f$ et $f(\Val{b})=\editable{}\sqrt{2}+\editable{}$}
    Ce formulaire a un choix de liste. La figure mtg32 doit donc contenir un affichage LaTeX de tag list1, formé d’une tableau
    de deux lignes dont les deux lignes sont par exemple \text{croissante} et \text{décroissante}. Elle doit aussi contenir
    un calcul nommé reslist1 dont la valeur est l’indice de la bonne réponse (1 ou 2 ici).
    Si plusieurs items de la liste peuvent être acceptés, la figure doit contenir un calcul nommé par exemple repList1
    qui doit être initalisé à une valeur autre que -1.
    Si ce calcul existe, la figure doit contenir un autre calcul nommé exactList1 qui doit valoir 1 si la réponse
    de l’élève est une des réponses acceptées et 0 sinon.

    Il contient un éditeur mathquill en mmode texte repéré par edit1.
    Il doit donc contenir un calcul (ou fonction) nommé rep1 et deux calculs nommés resolu1 et exact1.
    resolu1 doit valoir 1 si la réponse entrée par l’élève dans ce champ d’édition est une réponse acceptée commme
    réponse finale et 0 sinon. exact1 doit lui renvoyer 1 si la réponse contenue dans le champ est exacte et 0 sinon.
    Le formulaire contient deux \editable{}. La figure doit donc contenir deux calculs (ou fonctions) nommés repEditable1
    et repEditable2 destinés à contenir les réponses contenues dans ces champs.
    La figure doit donc aussi contenir des calculs nommés resoluEditable1, exactEditable1, resoluEditable2, exactEditable2.
    Par exemple, resoluEditable1 doit renvoyer 1 si la réponse dans le premier editable est acceptée comme réponse finale
    et 0 sinon, et exactEditable1 lui doit renvoyer 1 si la réponse dans le premier editable est exacte et 0 sinon.
    Le formulaire puet aussi contenir des éditeurs de texte. Ils sont alors répérés dans le formulaire par edittext1,
    edittext2 etc ...
    Pour vérifier la réponse de l’élève dans un champ de texte, la figure doit contenir un affichage LaTeX de tag restext.
    Cet affichage peut être un tableau de plusieurs lignes, chaque ligne étant encapsulée dans un \text.
    Imaginons par exemple que le formulaire contient trois editeurs de texte répérés par les chaînes edittext1,
    edittext2 et edittex3.
    Imaginons que deux réponse sont possibles : A ou B dans le champ 1, C dans le champ 2 et E ou F dans le champ 3
    ou alors A ou C dans le champ 1, B dans le champ 2 et E, F ou G dans le champ 3.
    Alors la première ligne de l’affichage LaTex de tag restext sera : \text{A//B**C**E//F}
    et la seconde ligne sera : \text{A//C**B**E//F//G}.
    Donc chaque ligne donne une solution possible, les solutions pour les champs étant séparés par des ** et les solutions
    multiples pour un même éditeur séparées par des //
    Pour limiter le nombre de caractères d’un champ de texte, par exemple ici le n°2, la figure devra contenir un calcul
    nommé nbcar2 contenant la valeur 1 si par exemple on souhaite ne pouvoir écrire qu’un caractère dabs ce champ.
    Si le paramètre bigSize est présent et à true, l’éditeur de formulaire et les réponses de l’élève utiliseront
    une taille plus grande que la taille standard.
    Il et possible de faire afficher des lignes lors de la correction. Ces lignes doivent alors être les lignes d’une affichage LaTEX
    de tag solution de la figure.
    Si on ne souhaite pas que la figure mtg32 soit affichée, il faut mettre les variables width et height
    à 0 Sinon ces variables doivent contenir la largeur et la hauteur de la figure en pixels.
        On peut aussi faire afficher à la correction une figure dont le code base 64 est contenu dans le paramètre figSol
    du fichier annexe, les paramètres widthSol et heightSol désignant la largeur et la hauteur de cette figure.
    Avant d’afficher cette figure de correction, on donne aus calculs ou fonctions dont le nom figure dans la paramètre de nom
    param les formules qu’ils avaient dans la figure principale.
    Avant la figure de correction, on peut afficher des epxlications contenues dans le paramètre explications.
    Ces explications peuvent utiliser des affichages LaTeX de la figure de correction, leu nombre étant dans le paramètre
    nbLatexSol. On fait référence à eux via des $£a$, $£b$, etc ...
    Si la figure est affichée, elle peut contenir une macro d’intitulé solution qui sera exécutée lors de la correction.
    Si la figure contenue  dans fig contient un paramètre nbVar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Si on veut accepter une réponse +∞ ou -∞ dans un champ d’édition, le bouton correspondant doit être activé
    et la figure mtg32 doit contenir un calcul de nom infinity qui ne soit pas égal à -1
*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
MathfieldElement.soundsDirectory = '/static/mathlive/sounds'

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['titre', '', 'string', 'Titre de l’activité'],
    ['solution', true, 'boolean', 'false pour que la solution ne soit pas affichée en fin d’exercice'],
    ['calculatrice', false, 'boolean', 'true pour avoir une calculatrice disponible'],
    ['nbEssais', 6, 'entier', 'Nombre maximum d’essais  autorisés pour le calcul'],
    ['fig', '', 'editor', 'Chaîne Base 64 contenant la figure de l’exercice', initEditor],
    ['width', 800, 'entier', 'Largeur de la figure initiale'],
    ['height', 600, 'entier', 'Hauteur de la figure initiale'],
    ['param', 'abcd', 'string', 'Chaîne formé des paramètres que l’exercice utilise (de a à r, voir ci-dessous'],
    ['nbLatex', 1, 'entier', 'Nombre d’affichages LaTeX de la figure utilisés dans la consigne (1 au moins et jusque 4)'],
    ['listesLatex', false, 'boolean', 'true si les items des listes sont des formules LaTeX'],
    ['charset', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de calcul, chaîne vide pour autoriser tous les caractères'],
    ['textesSensiblesCasse', true, 'boolean', 'Si false, la réponse de l’élève est convertie en majuscules et comparée à la réponse attendue'],
    ['charsetText', '', 'string', 'Chaîne formée des caractères permis dans les éditeurs de texte, chaîne vide pour autoriser tous les caractères'],
    ['enonceLigne1', '', 'string', 'Première ligne de la consigne initiale'],
    ['enonceLigne2', '', 'string', 'Ligne 2 de la consigne (peut être une chaîne vide)'],
    ['enonceLigne3', '', 'string', 'Ligne 3 de la consigne (peut être une chaîne vide)'],
    ['enonceLigne4', '', 'string', 'Ligne 4 de la consigne (peut être une chaîne vide)'],
    ['formulaire', '', 'string', '???'], // FIXME, mettre une aide à la saisie
    ['btnPuis', true, 'boolean', 'Bouton puissance'],
    ['btnFrac', true, 'boolean', 'Bouton fraction'],
    ['btnRac', true, 'boolean', 'Bouton Racine carrée'],
    ['btnPi', false, 'boolean', 'Bouton pi'],
    ['btnCos', false, 'boolean', 'Bouton cosinus'],
    ['btnSin', false, 'boolean', 'Bouton sinus'],
    ['btnTan', false, 'boolean', 'Bouton tangente'],
    ['btnExp', false, 'boolean', 'Bouton exponentielle'],
    ['btnLn', false, 'boolean', 'Bouton logarithme népérien'],
    ['btnLog', false, 'boolean', 'Bouton logarithme décimal'],
    ['btnConj', false, 'boolean', 'Bouton conjugué'],
    ['btnInteg', false, 'boolean', 'Bouton intégrale'],
    ['btnPrim', false, 'boolean', 'Bouton primitive'],
    ['btnInf', false, 'boolean', 'Bouton infini'],
    ['btnAbs', false, 'boolean', 'Bouton valeur absolue'],
    ['variables', 'xt', 'string', 'Noms des variables autorisées pour un calcul d’intégrale ou de primitive'],
    ['figSol', '', 'editor', 'Code Base64 d’une figure supplémentaire pour la correction, vide pour aucune figure supplémentaire', initEditor],
    ['widthSol', 600, 'entier', 'Largeur de la figure supplémentaire de solution'],
    ['heightSol', 500, 'entier', 'Hauteur de la figure supplémentaire de solution'],
    ['nbLatexSol', 1, 'entier', 'Nombre d’affichages LaTeX de la figure supplémentaire à récupérer pour affichage dans la ligne d’explications'],
    ['explicationSol', '', 'string', 'Ligne d’explications précédent la figure supplémentaire de solution (peut être vide)'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r']
  ]
}

const textes = {
  ilReste: 'Il reste ',
  essais: ' essais.',
  unEssai: ' un essai.',
  solutionIci: '\nLa solution se trouve ci-contre.',
  solDesact: '\nL’affichage de la solution a été désactivé par le professeur.',
  repIncorrecte: 'Réponse incorrecte',
  egaliteNecess: '\nIl faut écrire une égalité',
  bonPasFini: 'Le calcul est bon mais pas écrit sous la forme demandée.',
  repExacte: 'Réponse exacte : ',
  repFausse: 'Reponse fausse : ',
  repExactPasFini: 'Exact pas fini : ',
  ilManque: 'Il manque : ',
  unAffLat: 'un affichage LaTeX de tag ',
  avert1: 'L’affichage LaTeX de tag ',
  avert2: ' doit contenir au moins un code LaTeX \\text{}.',
  aide1: 'C’est le contenu du ou des \\text{} qui fourni le contenu à afficher.',
  avert3: ' doit contenir au moins un éditeur repéré par edit, edittext ou list suivi du numéro de l’éditeur' +
    '\n ou un ou plusieurs \\editable{}',
  unCalcNom: 'un calcul (ou une fonction) nommé ',
  et: 'et',
  ou: 'ou'
}

function setMathliveCss (container) {
  if (typeof container === 'string') container = j3pElement(container)
  container.classList.add('mlOverrides')
  // Ligne suivante parce que sinon pour les matrices on ne voit plus grand chose en cas d’erreur de syntaxe
  // document.body.style.setProperty('--contains-highlight-background-color', 'transparent')
}

/**
 * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
 * en une chaîne affichable par MathQuill
 * @param {CListeObjets} list la liste d’objets dans laquelle on recherche le LaTeX
 * @param {string} tagLatex le tag de l’affichage LaTeX de la figure
 * @return {string}
 */
function extraitDeLatexPourMathlive (list, tagLatex) {
  /*
                function mqMatCommand (nblig, nbcol) {
                  const tab = ['one', 'two', 'three', 'four', 'five', 'six']
                  return '\\matrix' + tab[nblig - 1] + tab[nbcol - 1]
                }
                function mqSystCommand (nblig) {
                  const tab = ['two', 'three']
                  return '\\system' + tab[nblig - 2]
                }
                 */
  const latex = list.getByTag(tagLatex, true)
  if (latex === null || latex.className !== 'CLatex') return ''
  const ch = cleanLatexForMl(latex.chaineLatex)

  // Il faut se débarrasser de ce qui n’est pas géré par MathQuill et se débarrasser des \left\{ en les remplaçant
  // par des codes bidons  \\reft\& (qui ne doivent pas contenir d’accolade pour ne pas perturber
  // le fonctionnement de cleanLatexForMq) qui seront remplacés à nouveau à la fin
  // let ch = cleanLatexForMl(latex.chaineLatex.replace(/\\left\\{(\s*\\begin{array}{l})/g, '\\reft\\&$1'))
  // let ch = cleanLatexForMq(latex.chaineLatex)
  // On traite les \matrix LaTeX en les remplaçant pas les commandes correspondantes MathQuill (si elles existent)
  /*
                let indmat = -1
                while ((indmat = ch.indexOf('\\begin{matrix}')) !== -1) {
                  const finmat = ch.indexOf('\\end{matrix}', indmat)
                  if (finmat === -1) break
                  const contenu = ch.substring(indmat + 14, finmat)
                  const tab = []
                  const tablig = contenu.split('\\\\')
                  const nblig = tablig.length
                  const nbcol = tablig[0].split('&').length
                  for (const lig of tablig) {
                    const tabcol = lig.split('&')
                    if (tabcol.length !== nbcol) return ch
                    tab.push(tabcol)
                  }
                  let remp = mqMatCommand(nblig, nbcol)
                  for (const line of tab) {
                    for (const cell of line) {
                      remp += '{' + cell.trim() + '}'
                    }
                  }
                  ch = ch.substring(0, indmat) + remp + ch.substring(finmat + 12)
                }
                // On traite maintenant les systèmes d’équations qui doivent être sous la forme
                // \left\{ suivi de \begin{array}{l} suivi de lignes séparées par des \\ suivi de \\end{array} suivi de \right.
                let indsys = -1
                while ((indsys = ch.indexOf('\\reft\\&')) !== -1) {
                  const finsys = ch.indexOf('\\right.', indsys)
                  if (finsys === -1) {
                    ch = ch.substring(0, indsys) + '\\left\\{' + ch.substring(indsys + 8)
                  }
                  const inddeb = ch.indexOf('\\begin{array}{l}', indsys)
                  const indfin = ch.indexOf('\\end{array}', inddeb)
                  if (indfin === -1 || indfin > finsys) {
                    ch = ch.substring(0, indsys) + '\\left\\{' + ch.substring(indsys + 8)
                  }
                  const contenu = ch.substring(inddeb + 16, indfin)
                  const tablig = contenu.split('\\\\')
                  const nblig = tablig.length
                  if (nblig > 3) break
                  let remp = mqSystCommand(nblig)
                  for (const line of tablig) {
                    remp += '{' + line.trim() + '}'
                  }
                  ch = ch.substring(0, indsys) + remp + ch.substring(finsys + 7)
                }
                 */
  // return ch.replace(/\\reft\\&/g, '\\left\\{')
  return ch
}

/**
 * Fonction renvoyant un tableau formé des messages d’erreur signalant que la figure
 * n’a pas certains éléments essentiels pour son fonctionnement
 * @param {CListeObjets} list
 */
function getTabErrors (list) {
  const errors = []
  const tabch = ['enonce', 'formulaire']
  let ch = ''
  for (const nom of tabch) {
    const latex = list.getByTag(nom, true)
    if (latex === null || latex.className !== 'CLatex') {
      ch += '\n - ' + textes.unAffLat + nom
    }
  }
  if (ch !== '') {
    errors.push(textes.ilManque + ch)
    return errors
  }
  ch = ''
  for (const nom of tabch) {
    const chaineLatex = list.getByTag(nom, true).chaineLatex
    if (chaineLatex.indexOf('\\text{') === -1) {
      if (ch !== '') ch += '\n'
      ch += textes.avert1 + nom + textes.avert2
    }
  }
  if (ch !== '') {
    errors.push(ch + '\n' + textes.aide1)
  }
  // On regarde si le formulaire contient bien des éditeurs
  let latex = extraitDeLatexPourMathlive(list, 'formulaire')
  if (!/edit[123456789]+|edittext[123456789]+|list[123456789]+/g.test(latex) && latex.indexOf('\\editable{}') === -1) {
    errors.push(textes.avert1 + 'formulaire' + textes.avert3)
  }

  // On regarde si les calculs devant être associés aux éditeurs sont bien présents.
  latex = extraitDeLatexPourMathlive(list, 'formulaire')
  let j = 1
  let st = ''
  while (new RegExp('edit' + j + '[\\D]*', 'g').test(latex)) {
    const nomCalcul1 = 'resolu' + String(j)
    if (!list.containsCalc(nomCalcul1, true)) {
      st += '\n - ' + textes.unCalcNom + nomCalcul1
    }
    const nomCalcul2 = 'exact' + String(j)
    if (!list.containsCalc(nomCalcul2, true)) {
      st += '\n - ' + textes.unCalcNom + nomCalcul2
    }
    j++
  }
  j = 1
  while (new RegExp('list' + j + '[\\D]*', 'g').test(latex)) {
    const nom = 'list' + j
    const latex = list.getByTag(nom, true)
    if (latex === null || latex.className !== 'CLatex') {
      st += '\n - ' + textes.unAffLat + nom
    }
    const nomCalc1 = list.containsCalc('resList' + j, true)
    const nomCalc2 = list.containsCalc('repList' + j, true)
    const nomCalc3 = list.containsCalc('exactList' + j, true)
    if ((!nomCalc1) && (!nomCalc2 || !nomCalc3)) {
      st += '\n - ' + textes.unCalcNom + 'resList' + j + ' ' + textes.ou + ' ' +
        textes.deuxCalcNom + ' ' + 'repList' + j + ' ' + textes.et + ' ' + 'exactList' + j
    }
    j++
  }
  j = 0
  let indeb = -1
  while ((indeb = latex.indexOf('\\editable{}', indeb + 1)) !== -1) {
    j++
    const tab = ['repEditable', 'resoluEditable', 'exactEditable']
    for (const nom of tab) {
      const nomCalc = list.containsCalc(nom + j, true)
      if (!nomCalc) {
        st += '\n - ' + textes.unCalcNom + nom + j
      }
    }
  }
  if (st !== '') {
    errors.push(textes.ilManque + st)
  }
  return errors
}

/**
 * Valide la figure (avant de la sauvegarder dans les paramètres)
 * S’il y a des errors dans ce que l’on retourne, ce sera affichés et bloquera la sauvegarde (l’éditeur reste ouvert),
 * s’il y a des warning ils sont affichés sans bloquer la sauvegarde
 * @param {MtgApp} mtgApp
 * @return {Promise<{warnings: string[], errors: string[]}>}
 */
async function validator (mtgApp) {
  const errors = getTabErrors(mtgApp.getList())
  return { errors, warnings: [] }
}

const aide = 'La figure doit contenir un affichage Latex de tag enonce et un autre de tag formulaire'

// cf src/legacy/sections/outils/mtg32/sectionex_Calc_Multi_Edit_Multi_Etapes.js pour ajouter un validator
/**
 * Ajoute l’éditeur mathgraph dans container, la promesse sera résolue avec la figure
 * @param {HTMLElement} container
 * @param {string} fig
 * @return {Promise<string>}
 */
async function initEditor (container, fig) {
  const { default: editor } = await import('./mtgEditor.js')
  return editor(container, { fig, validator, aide })
}

/**
 * section ex_Calc_Multi_Edit
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup () {
    // Attention : indice est en fait l’id de l’éditeur
    const id = this.id
    if (sq.marked[id]) {
      demarqueEditeurPourErreur(id)
      j3pEmpty('correction')
    }
  }

  /**
   * Fonction affichant un message d’avertissement au démarrage de la figure s’il manque
   * des éléments pour que la figure soit un exercice multi calculs multi étapes
   */
  function alertIfErrors () {
    if ((sq.enonce === '') && (sq.formulaireInit === '')) {
      const list = sq.mtgAppLecteur.getList('mtg32svg')
      const errors = getTabErrors(list)
      if (errors.length > 0) {
        console.error(errors.join('\n'))
      }
    }
  }

  function mathliveRestrict (input, restriction) {
    if (typeof input === 'string') input = j3pElement(input)
    if (!input) return // j3pElement a déjà râlé en console
    if (!restriction) restriction = /./ // on autorise tout
    if (typeof restriction === 'string') restriction = convertRestriction(restriction)
    input.addEventListener('keypress', function (event) {
      j3pRestrict(input, event, restriction)
    })
    /* L’événement keystroke ne semble pas être généré quand on tape une touche physique
                                input.addEventListener('keystroke', (event) => {
                                  if (!restriction.test(event)) {
                                    const a = 1
                                  }
                                })
                                 */
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) j3pAffiche('info', 'texteinfo', textes.ilReste + textes.unEssai, { style: { color: '#7F007F' } })
    else j3pAffiche('info', 'texteinfo', textes.ilReste + nbe + textes.essais, { style: { color: '#7F007F' } })
  }

  // Attention : Les }{ on été remplacés par des }/{
  function traiteIntegrales (st) {
    const faux = { valide: false, st }
    let ind
    // Traitement différent pour mathlive
    while ((ind = st.indexOf('\\int_')) !== -1) {
      const ind1 = getFirstIndexParExcluded(st, '^', ind + 5)
      if (ind1 === -1) return -1
      const a = st.substring(ind + 5, ind1)
      // Si le ^ est suivi d’une parenthèse on recherche la parenthèse fermante associée
      // Sinon c’est que la borne supérieure est formée d’un seul caractère
      let ind2, ind3, indsuiv
      if (st.charAt(ind1 + 1) === '(') {
        ind2 = ind1 + 2
        ind3 = getIndexFermant(st, ind1 + 1)
        indsuiv = ind3 + 1
      } else {
        ind2 = ind1 + 1
        ind3 = ind1 + 2
        indsuiv = ind3
      }
      if (ind3 === 0 || indsuiv >= st.length) return faux
      const b = st.substring(ind2, ind3)
      // Attention : les {}{} ont pu donner des ()/().
      if (st.charAt(indsuiv) === '/') indsuiv++
      // On recherche maintenant le d d’intégration en sautant les éventuelles parenthèses
      let ind4 = getFirstIndexParExcluded(st, '\\differentialD', indsuiv)
      if (ind4 === -1) return faux
      const fonc = st.substring(indsuiv, ind4)
      ind4 += 14
      let ind5
      if (st.charAt(ind4) === '(') {
        ind5 = getIndexFermant(st, ind4)
        ind4++
      } else {
        ind5 = ind4 + 1
      }
      if (ind5 === -1) return faux
      const varfor = st.substring(ind4, ind5) // Le caractère représentant la variable d’intégration
      st = st.substring(0, ind) + 'integrale(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind5)
    }
    return { valide: true, res: st }
  }

  /**
   * Fonction renvoyant un tableau formé des noms de variables utilisées par le calcul contenu dans ch
   * Si le tableau renvoyé a une longueur au moins égale à 2 on considérera que la primitive est incohérente
   * @param {string} ch
   * @return {[]}
   */
  function variablesUtilisees (ch) {
    function utiliseVariable (nomvar) {
      let res
      res = ch.search(new RegExp('\\(' + nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || ch.search(new RegExp(nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || (ch.lastIndexOf(nomvar) === ch.length - 1)
      res = res || ch === nomvar
      return res
    }

    const tab = []
    for (let i = 0; i < sq.variables.length; i++) {
      const car = sq.variables.charAt(i)
      if (utiliseVariable(car)) tab.push(car)
    }
    return tab
  }

  function traitePrimitives (st) {
    const faux = { valide: false, st }
    if (sq.variables === '') return { valide: true, res: st }
    let ind, tab
    while ((ind = st.indexOf('\\left[')) !== -1) {
      // On cherche le \right] correspondant
      const ind1 = getIndexFermant(st, ind + 5)
      if (ind1 === -1) return faux
      const fonc = st.substring(ind + 6, ind1 - 6)
      tab = variablesUtilisees(fonc)
      if (tab.length > 1) return faux
      const varfor = (tab.length === 0) ? sq.variables[0] : tab[0] // On traite le cas d’une fonction constante
      // Le crochet fermant doit être suivi d’un _
      if (st.charAt(ind1 + 1) !== '_') return faux
      const ind2 = getFirstIndexParExcluded(st, '^', ind1 + 2)
      if (ind2 === -1) return faux
      const a = st.substring(ind1 + 2, ind2)
      // Soit le caractère suivant n’est pas une parenthèse et c’est un seul caractère
      // qui est l’argument soit c’est le contenu de la parenthèse
      let ind3, ind4
      if (st.charAt(ind2 + 1) === '(') {
        ind3 = ind2 + 2
        ind4 = getIndexFermant(st, ind3)
      } else {
        ind3 = ind2 + 1
        ind4 = ind2 + 2
      }
      if (ind4 === -1 || ind4 > st.length) return faux
      const b = st.substring(ind3, ind4)
      st = st.substring(0, ind) + 'primitive(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind4 + 1)
    }
    return { valide: true, res: st }
  }

  function traiteMathQuill (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    let { res, valide } = traiteIntegrales(ch)
    if (valide) {
      const resul = traitePrimitives(res)
      res = resul.res
      valide = resul.valide
    }
    if (valide) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', res)// Traitement des multiplications implicites
    const contientIntegOuPrim = ch.includes('integrale') || ch.includes('primitive')
    return { valide, res: ch, contientIntegOuPrim }
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function marqueEditeurPourErreur (id) {
    // Il faut regarder si l’erreur a été rencontrée dans un placeHolder ou dans un éditeur Mathlive classique
    if (id.includes('ph')) {
      const ind = id.indexOf('ph')
      const idmf = id.substring(0, ind) // L’id de l’éditeur MathLive contenant le placeHolder
      const mf = j3pElement(idmf)
      sq.marked[idmf] = true
      mf.setPromptState(id, 'incorrect')
    } else {
      const mf = j3pElement(id)
      sq.marked[id] = true
      mf.style.backgroundColor = '#FF9999'
      /*
                                                const txt = getMathliveValue(mf, mf.id)
                                                mf.executeCommand(['insert', txt, {
                                                  insertionMode: 'replaceAll',
                                                  style: { color: 'red' }
                                                }])
                                                 */
    }
  }

  function demarqueEditeurPourErreur (id) {
    // if ($.isEmptyObject(sq.marked)) return
    if (JSON.stringify(sq.marked) === '{}') return // On teste si l’objet est vide
    sq.marked[id] = false // Par précaution si appel via bouton recopier réponse trop tôt
    // On regarde si l’éditeur où a été trouvée l’erreur est un placeHolder
    const mf = j3pElement(id)
    if (mf.classList.contains('PhBlock')) {
      mf.getPrompts().forEach((id) => {
        mf.setPromptState(id, 'undefined')
      })
    } else {
      mf.style.backgroundColor = 'white'
      /*
                                                const txt = getMathliveValue(mf, mf.id)
                                                mf.executeCommand(['insert', txt, {
                                                  insertionMode: 'replaceAll',
                                                  style: { color: 'black' }
                                                }])
                                                 */
    }
  }

  function validationEditeurs () {
    sq.signeEgalManquant = false
    const listeIdEditeursAvecErreur = []
    for (let ind = 1; ind <= sq.nbCalc; ind++) {
      const id = 'expressioninputmq' + ind
      const mf = j3pElement(id)
      const rep = getMathliveValue(mf)
      if (rep === '') {
        marqueEditeurPourErreur(id)
        listeIdEditeursAvecErreur.push(id)
      } else {
        // On regarde si l’éditeur n° ind doit contenir une égalité ou non
        let valideEq = true
        if (sq['estEgalite' + ind]) {
          const indeq1 = rep.indexOf('=')
          let indeq2 = -1
          if (indeq1 !== 1) {
            indeq2 = rep.indexOf('=', indeq1 + 1)
          }
          if (indeq1 === -1 || (indeq2 !== -1)) {
            valideEq = false
            sq.signeEgalManquant = true
            marqueEditeurPourErreur(id)
            listeIdEditeursAvecErreur.push(id)
          }
        }
        if (valideEq) {
          const resul = traiteMathQuill(rep)
          const repcalcul = resul.res
          // Ligne suivante dernier paramètre à false car addImplicitmult a déjà été appelé par traiteMathQuill
          // et si une réponse est +infini ou -infini elle ne serait pas acceptée
          const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep' + ind, repcalcul, false)
          if (!valide) {
            marqueEditeurPourErreur(id)
            listeIdEditeursAvecErreur.push(id)
          }
        }
      }
    }
    let indiceEditable = 1
    j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf) => {
      demarqueEditeurPourErreur(mf.id)
      mf.getPrompts().forEach((id) => {
        // const rep = getMathliveValue(mf, id)
        const rep = getMathliveValue(mf, { placeholderId: id, raw: true })
        if (rep === '') {
          marqueEditeurPourErreur(id)
          listeIdEditeursAvecErreur.push(id)
        } else {
          const resul = traiteMathQuill(rep)
          const repcalcul = resul.res
          // Ligne suivante dernier paramètre à false car addImplicitmult a déjà été appelé par traiteMathQuill
          // et si une réponse est +infini ou -infini elle ne serait pas acceptée
          const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'repEditable' + indiceEditable, repcalcul, false)
          if (!valide) {
            marqueEditeurPourErreur(id)
            listeIdEditeursAvecErreur.push(id)
          }
          indiceEditable++
        }
      })
    })
    for (let ind = 1; ind <= sq.nbTextEdit; ind++) {
      const id = 'expressioninput' + ind
      const rep = j3pValeurde(id)
      if (rep === '') {
        marqueEditeurPourErreur(id)
        listeIdEditeursAvecErreur.push(id)
      }
    }
    if (listeIdEditeursAvecErreur.length) {
      // On donne le focus au premier éditeur dont le contenu est incorrect
      /*
                                                const sel = document.querySelectorAll('.mq-editable-field, input[type=text]')
                                                for (let i = 0; i < sel.length; i++) {
                                                  const el = sel[i]
                                                  const id = el.getAttribute('id')
                                                  if (listeIdEditeursAvecErreur.indexOf(id) !== -1) {
                                                    j3pFocus(el)
                                                    break
                                                  }
                                                }
                                                 */
      setFocusById(listeIdEditeursAvecErreur[0]) // Ne marche pas à revoir
      // Si pas exploitable, supprimer listeIdEditeursAvecErreur
      return false
    }
    for (let ind = 1; ind <= sq.nbList; ind++) {
      const resliste = sq.listesLatex ? sq['listeDeroulante' + ind].getReponseIndex() : j3pElement('expression' + 'liste' + ind).selectedIndex
      if (resliste === 0) return false
    }
    return true
  }

  /**
   * Fonction renvoyant le nombre d’éditeurs contenus dans la chaîne ch et dont le type est contenu
   * dans la chaîne type
   * @param {string} type chaine de caractères qui peut être la chaîne 'edit' pour un éditeur MathQuill,
   * 'list' pour une liste déroulante et 'string' pour une chaîne de caractères.
   * Si, par exemple, il y a trois éditeurs MathQuill la chaîne doit contenir edit1, edit2 et edit3.
   * @param {string} ch
   */
  function nbEdit (type, ch) {
    let res = 0
    let i = 1
    while (ch.indexOf(type + i) !== -1) {
      res++
      i++
    }
    return res
  }

  /**
   * Fonction renvoyant le nombre de `\editable{} contenus dans la chaîne ch
   * @param ch
   * @return {string|number}
   */
  function nbEditable (ch) {
    let res = 0
    let i = ch.indexOf('\\editable{')
    if (i === -1) return 0
    while (i !== -1) {
      res++
      ch = ch.substring(i + 10)
      i = ch.indexOf('\\editable{')
    }
    return res
  }

  /**
   * Retourne les ≠ \text dans la propriété texte, éventuellement mélangés (si listaleat), le premier élément est toujours '...'
   * @param {string|number} indListe
   * @returns {{texte: string[]}}
   */
  function extraitDonneesListe (indListe) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    // On cherche dans la liste le LaTeX qui a pour tag 'list' suivi du chiffre indListe
    const latex = list.getByTag('list' + indListe, true)
    if (latex === null || latex.className !== 'CLatex') return []
    // On met dans le tableau qu’on va renvoyer le contenu des \text{} successifs rencontrés dans le LaTeX
    const chaineLatex = cleanLatexForMl(latex.chaineLatex, { keepText: true })
    const res = ['...']
    let i = chaineLatex.indexOf('\\text{')
    while (i !== -1) {
      const idaf = getIndexFermant(chaineLatex, i + 5)
      if (idaf === -1) break
      res.push(chaineLatex.substring(i + 6, idaf))
      i = chaineLatex.indexOf('\\text{', idaf + 1)
    }

    // Amélioration : Si la figure contient un calcul nommé listaleat suivi du numéro indListe et qui vaut 1,
    // alors on mélange les items de la liste au hasard.
    // On crée un tableau pour connaître le vrai numéro de l’item choisi dans la liste
    const tabnum = []
    const n = res.length - 1
    for (let i = 0; i < n; i++) tabnum.push(i + 1)
    let tabres
    let tabresitem
    if (sq.mtgAppLecteur.valueOf('mtg32svg', 'listaleat' + indListe, true) === 1) { // On mélange les items au hasard
      tabresitem = ['...']
      tabres = []
      for (let i = 0; i < n; i++) {
        const tir = Math.floor(Math.random() * tabnum.length)
        const num = tabnum[tir]
        tabres.push(num)
        tabnum.splice(tir, 1)
        tabresitem.push(res[num])
      }
    } else {
      tabres = tabnum
      tabresitem = res
    }
    sq['listaleat' + indListe] = tabres
    return { texte: tabresitem }
  }

  /**
   * Fonction renvoyant la liste d’objets générée par le lecteur mtg32 de cette section
   * @return {*}
   */
  function getMtgList () {
    return sq.mtgAppLecteur.getList('mtg32svg')
  }

  function extraitSolutionsEditeursTexte () {
    function affErr () {
      console.error('Les données pour les résultats de champ texte sont incorrectes')
    }

    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag('restext', true)
    if (latex === null || latex.className !== 'CLatex') return []
    // Il faut se débarrasser de ce qui n’est pas géré par MathQuill
    let ch = latex.chaineLatex.replace(/\n/g, '').replace(/\\displaystyle/g, ' ').replace(/\\,/g, ' ')
    ch = ch.replace(/\\left\[/g, '[').replace(/\\right\]/g, ']')
    // On remplace les . décimaux par des virgules
    ch = ch.replace(/(\d)\.(\d)/g, '$1,$2')
    const res = []
    let i = ch.indexOf('\\text{')
    if (i === -1) {
      affErr()
      return res
    }

    while (i !== -1) {
      const idaf = getIndexFermant(ch, i + 5)
      if (idaf !== -1) {
        // Les résultats possibles pour les différents éditeurs sont séparés par des **
        // et s’il y a plusieurs réponses possibles pour un même éditeur elles sont séparées par des //
        const tab = []
        const st = ch.substring(i + 6, idaf)
        const soustab = st.split('**')
        if (soustab.length !== sq.nbTextEdit) {
          affErr()
          return []
        }
        soustab.forEach(function (el) {
          tab.push(el.split('//'))
        })
        res.push(tab)
      }
      i = ch.indexOf('\\text{', idaf + 1)
    }
    return res
  }

  function montreEditeurs (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.boutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
  }

  function videEditeurs () {
    /*
                                for (let i = 1; i <= sq.nbCalc; i++) {
                                  // $('#expressioninputmq' + i).mathquill('latex', ' ')
                                  j3pElement('expressioninputmq' + i).value = ''
                                }
                                for (let i = 1; i <= sq.nbEditable; i++) {
                                  j3pElement('expressioninputmqef' + i).value = ''
                                }
                                 */
    // On vide les éditeurs simples mais on ne vide dans les blocs de editable que les editable
    const el = j3pElement('expression')
    el.querySelectorAll('math-field').forEach(mf => {
      if (!mf.id.includes('PhBlock')) mf.value = ''
    })
    el.querySelectorAll('.PhBlock').forEach((mf) => {
      mf.getPrompts().forEach((id) => {
        mf.setPromptContent(id, '', 'latex')
      })
      // mf.stripPromptContent() // Quand on vide l’apparence du placeholder n’est plus visible
      const idFirstEdit = mf.getPrompts()[0]
      mf.setPromptContent(idFirstEdit, '')
    })
    for (let i = 1; i <= sq.nbList; i++) {
      if (sq.listesLatex) {
        sq['listeDeroulante' + i].reset()
      } else {
        j3pElement('expressionliste' + i).selectedIndex = 0
      }
    }
    for (let i = 1; i <= sq.nbTextEdit; i++) {
      $('#expressioninput' + i).val('')
    }
    // On donne le focus au premier éditeur MathQuill
    setFocusToFirstInput('expression')
  }

  function creeEditeurs () {
    try {
      let i, k
      const t = 'abcdefghijklmnopq'
      j3pEmpty('editeur')
      // On regarde si le formulaire est dynamique. Dans ce cas sq.formulaireInit doit être une chaine vide
      // et le formulaire doit être récupéré depuis un affichage LaTeX de tag formulaire contenant un seul affichage de texte
      // dont le texte est ce qui doit tenir compte de formulaire, par exemple \text{Le nombre 2 a pour list1 le nombre par $f$}.
      const list = sq.mtgAppLecteur.getList('mtg32svg')
      if (list.getByTag('formulaire', true)) {
        sq.formulaire = replaceOldMQCodes(extraitDeLatexPourMathlive(list, 'formulaire'))
      } else {
        sq.formulaire = replaceOldMQCodes(sq.formulaireInit)
      }
      sq.nbCalc = nbEdit('edit', sq.formulaire)
      sq.nbList = nbEdit('list', sq.formulaire)
      sq.nbTextEdit = nbEdit('edittext', sq.formulaire)
      sq.solEditeursTextes = extraitSolutionsEditeursTexte()
      sq.nbEditable = nbEditable(sq.formulaire)
      // S’il n’y a pas d’éditeurs mathquill inutile de montrer le boutons MathQuill
      if (sq.boutonsMathQuill) {
        const el = j3pElement('boutonsmathquill')
        if (sq.nbCalc === 0 && sq.nbEditable === 0) el.style.display = 'none'
        else el.style.display = 'block'
      }
      for (k = 0; k < sq.nbCalc; k++) {
        sq.marked[k] = false
      }
      let formulaire = sq.formulaire
      for (k = 1; k <= sq.nbCalc; k++) {
        formulaire = formulaire.replace('edit' + k, '&' + k + '&')
        // On interroge la figure pour savoir si l’éditeur k de l’étape correspondante
        // doit contenir une egalité (calcul nommé estEgalité suivi du n° d’étape suivi de l’indice de l’éditeur)
        sq['estEgalite' + k] = sq.mtgAppLecteur.valueOf('mtg32svg', 'estEgalite' + k, true) === 1
      }
      for (k = 1; k <= sq.nbList; k++) formulaire = formulaire.replace('list' + k, '#' + k + '#')
      for (k = 1; k <= sq.nbTextEdit; k++) formulaire = formulaire.replace('edittext' + k, '@' + k + '@')
      const obj = {}
      for (k = 1; k <= sq.nbCalc; k++) obj['inputmq' + k] = {}
      for (k = 1; k <= sq.nbList; k++) {
        const donnees = extraitDonneesListe(k)
        sq['tabItemsLatex' + k] = donnees.texte // Mémorisé pour afficheReponse
        obj['liste' + k] = donnees
      }
      for (k = 1; k <= sq.nbTextEdit; k++) obj['input' + k] = { texte: '', dynamique: true }
      const nbParam = parseInt(sq.nbLatex)
      for (i = 0; i < nbParam; i++) {
        obj[t.charAt(i)] = sq.parametres[t.charAt(i)]
      }
      obj.styletexte = {}
      obj.transposeMat = true
      obj.charset = sq.charset
      obj.charsetText = sq.charsetText
      obj.listeBoutons = sq.listeBoutons
      // On mémorise les infos sur les blocs contenant des editable (tableau formé par le nombre de editable contenus dans chaque bloc)
      afficheMathliveDans('editeur', 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
      // On donne un margin left et right aux listes déroulantes et aux éditeurs
      for (k = 1; k <= sq.nbList; k++) {
        $('#expressionliste' + k).css('margin-left', '5px').css('margin-right', '5px')
      }
      if (sq.listesLatex) {
        for (k = 1; k <= sq.nbList; k++) {
          // $('#expressionliste' + k).css('margin-left', '5px').css('margin-right', '5px')
          const select = j3pElement('expressionliste' + k)
          const pn = select.parentNode.parentNode
          const div = document.createElement('div')
          $(div).attr('id', 'expressiondivliste' + k)
          $(div).attr('display', 'inline')
          // j3pElement('expression').replaceChild(div, pn)
          pn.parentNode.replaceChild(div, pn)
          const tab = sq['tabItemsLatex' + k]
          // Rectification suite aux modifs de liste Déroulante : S’il y a un $ en début et en fin on laisse
          // S’il n’y en a pas et que sq.listesLaTeX est à true on les rajoutes
          for (i = 1; i < tab.length; i++) {
            let ch = tab[i].trim()
            const debdol = ch.charAt(0) === '$'
            const findol = ch.charAt(ch.length - 1) === '$'
            if (!debdol || !findol) {
              ch = (debdol ? '' : '$') + ch + (findol ? '' : '$')
              tab[i] = ch
            }
          }
          sq['listeDeroulante' + k] = ListeDeroulante.create('expressiondivliste' + k, tab, { displayWithMathlive: true })
          $(div).css('display', 'inline')
        }
      }

      for (k = 1; k <= sq.nbCalc; k++) {
        $('#expressioninputmq' + k).css('margin-left', '5px').css('margin-right', '5px')
      }
      // On affecte à chaque éditeur un écouteur de focus
      for (k = 1; k <= sq.nbCalc; k++) {
        const mf = j3pElement('expressioninputmq' + k)
        if (sq.charset !== '') {
          mathliveRestrict(mf.id, sq.charset)
        }
        mf.addEventListener('focusin', () => {
          ecoute.call(mf)
        })
        mf.addEventListener('keyup', () => {
          onkeyup.call(mf)
        })
      }
      j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf) => {
        const id = mf.id
        if (sq.charset !== '') {
          mathliveRestrict(id, sq.charset)
        }
        mf.addEventListener('focusin', () => {
          ecoute.call(mf)
        })
        mf.addEventListener('keyup', () => {
          onkeyup.call(mf)
        })
      })
      for (k = 1; k <= sq.nbTextEdit; k++) {
        const edit = j3pElement('expressioninput' + k)
        edit.id = 'expressioninput' + k
        const nbcar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcar' + i, true)
        if (nbcar !== -1) $(edit).attr('maxlength', nbcar)
        if (sq.charsetText !== '') {
          j3pRestriction('expressioninput' + k, sq.charsetText)
        }
        $('#expressioninput' + k).focusin(function () {
          ecoute.call(this)
        })
        edit.onkeyup = function () {
          onkeyup.call(this)
        }
      }
      setFocusToFirstInput('expression')
    } catch (error) {
      console.error('Erreur lors de la création des éditeurs', error)
    }
  }

  function afficheReponse (bilan, depasse) {
    let coul, i
    const num = sq.numEssai
    let ch = (num > 2) ? '<br>' : ''

    const idrep = 'exp' + num
    if (bilan === 'exact') {
      ch += textes.repExacte
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        ch += textes.repFausse
        coul = parcours.styles.cfaux
      } else { // Exact mais pas fini
        ch += textes.repExactPasFini
        if (depasse) coul = parcours.styles.cfaux
        else coul = parcours.styles.colorCorrection
      }
    }
    afficheMathliveDans('formules', idrep + 'deb', ch, {
      styletexte: {
        couleur: coul
      }
    })
    ch = sq.formulaire
    for (i = 1; i <= sq.nbCalc; i++) {
      const st = '\\textcolor{' + (sq.repResolu[i - 1] ? parcours.styles.cbien : (sq.repExact[i - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
      ch = ch.replace('edit' + i, '$' + st + sq.rep[i - 1] + '}$')
    }
    // Il se peut qu’il y ait une matrice ou plusieurs dont les termes soient des editable.
    // Dans ce cas, lors de la création des éditeurs, les indices des editable ont été créés
    // par ordre croissant colonne par colonne mais lors du replacement il faut donner le contenu des
    // cellules ligne par ligne
    let indmqef = 1
    const hasMatrixEditable = ch.includes('\\begin{matrix}') && ch.includes('\\editable')
    if (hasMatrixEditable) ch = transposeMatrices(ch)
    let indexEditable = ch.indexOf('\\editable{')
    while (indexEditable !== -1) {
      const st = '\\textcolor{' + (sq.repResoluEditable[indmqef - 1] ? parcours.styles.cbien : (sq.repExactEditable[indmqef - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
      ch = ch.replace('\\editable', st + sq.repEditable[indmqef - 1] + '}') // On laisse des {} vides derrière ce qui ne change rien à l’affichage
      indmqef += 1
      indexEditable = ch.indexOf('\\editable{', indexEditable + 1)
    }
    if (hasMatrixEditable) ch = transposeMatrices(ch)
    for (i = 1; i <= sq.nbList; i++) {
      const st = '\\textcolor{' + (sq.repResoluListe[i - 1] ? parcours.styles.cbien : parcours.styles.cfaux) + '}{'
      let chrep = sq['tabItemsLatex' + i][sq['replist' + i]]
      if (sq.listesLatex) {
        chrep = chrep.substring(1, chrep.length - 1)
        ch = ch.replace('list' + i, '$' + st + chrep + '}$')
      } else {
        ch = ch.replace('list' + i, '$' + st + '\\text{' + chrep + '}}$')
      }
    }
    for (i = 1; i <= sq.nbTextEdit; i++) {
      const st = '\\textcolor{' + (sq.repResoluText ? parcours.styles.cbien : parcours.styles.cfaux) + '}{'
      ch = ch.replace('edittext' + i, '$' + st + '\\text{' + sq.repText[i - 1] + '}}$')
    }
    afficheMathliveDans('formules', idrep, ch, sq.parametres)
  }

  function ecoute () {
    if (sq.boutonsMathQuill && (this.id.indexOf('inputmq') !== -1 || this.id.indexOf('PhBlock') !== -1)) {
      j3pEmpty('boutonsmathquill')
      j3pPaletteMathquill('boutonsmathquill', this.id, { liste: sq.listeBoutons, nomdiv: 'palette' })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
    }
  }

  function validation () {
    let rep, ind, chcalcul, exact, resolu, i, j, tab, res
    sq.rep = []
    sq.repExact = []
    sq.repEditable = []
    sq.repText = []
    sq.repExactEditable = []
    sq.repResolu = []
    sq.repExactEditable = []
    sq.repResoluEditable = []
    sq.repResoluListe = []
    sq.repResoluText = true

    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
      for (ind = 1; ind <= sq.nbCalc; ind++) {
        const id = 'expressioninputmq' + ind
        const mf = j3pElement(id)
        rep = getMathliveValue(mf)
        sq.rep.push(rep)
        chcalcul = traiteMathQuill(rep).res
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep' + ind, chcalcul)
      }
      let indiceEditable = 0
      j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf, index) => {
        /* Il ne faut pas lire les prompts dans l’ordre naturel où ils sont mais dans l’ordre de leurs identifiants
                                                                à cause du problème des matrices où lignes et colonnes sont inversées dans Mathlive par rapport à Mathquill
                                                                lignes d’abord pour Mathlive et colonnes d’abord pour mathlive
                                                                mf.getPrompts().forEach((id, index) => {
                                                                  rep = getMathliveValue(mf, id)
                                                                  sq.repEditable.push(rep)
                                                                  chcalcul = traiteMathQuill(rep).res
                                                                  sq.mtgAppLecteur.giveFormula2('mtg32svg', 'repEditable' + indiceEditable, chcalcul)
                                                                  indiceEditable++
                                                                })
                                                                 */
        const indPhBlock = index + 1
        const nbPprompts = mf.getPrompts().length
        for (let i = 0; i < nbPprompts; i++) {
          indiceEditable++
          rep = mf.getPromptValue('expressionPhBlock' + indPhBlock + 'ph' + indiceEditable, 'latex-unstyled')
          sq.repEditable.push(rep)
          chcalcul = traiteMathQuill(rep).res
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'repEditable' + indiceEditable, chcalcul)
        }
      })
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      exact = true
      resolu = true
      let auMoinsUnExact = false
      for (ind = 1; ind <= sq.nbCalc; ind++) {
        res = sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu' + ind, true)
        const ex = sq.mtgAppLecteur.valueOf('mtg32svg', 'exact' + ind, true)
        resolu = resolu && (res === 1)
        exact = exact && ((res === 1) || (ex === 1))
        sq.repResolu.push(res === 1)
        sq.repExact.push((res === 1) || (ex === 1))
        auMoinsUnExact = auMoinsUnExact || (ex === 1)
      }
      // On regarde ensuite les champs Editable s’il y en a
      for (ind = 1; ind <= sq.nbEditable; ind++) {
        res = sq.mtgAppLecteur.valueOf('mtg32svg', 'resoluEditable' + ind, true)
        const ex = sq.mtgAppLecteur.valueOf('mtg32svg', 'exactEditable' + ind, true)
        resolu = resolu && (res === 1)
        exact = exact && ((res === 1) || (ex === 1))
        sq.repResoluEditable.push(res === 1)
        sq.repExactEditable.push((res === 1) || (ex === 1))
        auMoinsUnExact = auMoinsUnExact || (ex === 1)
      }
      let listesOK = true
      for (ind = 1; ind <= sq.nbList; ind++) {
        const indresliste = sq.listesLatex ? sq['listeDeroulante' + ind].getReponseIndex() : j3pElement('expression' + 'liste' + ind).selectedIndex
        // Les éléments de la liste peuvent avoir été mélangés au hasard
        // On gère les choix multiples ou les choix uniques dans la liste.
        // Si les choix multiples sont acceptés, on donne au calcul repListe suivi du n° d’étape et du n° de liste
        // la valeur choisie dans la liste et on interroge le calcul exactList suivi du n° d’étape et du n° de liste
        // qui doit valeur 1 si la réponse est une des réponses acceptées et 0 sinon.
        // Si le choix est unique on interroge la calcul resliste suivi du n° d’étape et du n° de liste qui
        // contient l’indice de la bonne réponse
        const resliste = sq['listaleat' + ind][indresliste - 1]
        let reslisteexact
        if (sq.mtgAppLecteur.valueOf('mtg32svg', 'repList' + ind, true) !== -1) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'repList' + ind, String(resliste))
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          reslisteexact = sq.mtgAppLecteur.valueOf('mtg32svg', 'exactList' + ind, true) === 1
        } else {
          reslisteexact = resliste === sq.mtgAppLecteur.valueOf('mtg32svg', 'reslist' + ind, true)
        }
        listesOK = listesOK && reslisteexact
        // On mémorise le choix de la sélection des listes déroulantes pour pouvoir recopier
        // la réponse précédente
        sq['replist' + ind] = indresliste
        sq.repResoluListe.push(reslisteexact)
      }
      resolu = resolu && listesOK
      // On vérifie maintenant la réponse contenue dans les champs de texte
      for (ind = 1; ind <= sq.nbTextEdit; ind++) {
        rep = j3pValeurde('expressioninput' + ind)
        sq.repText.push(rep)
      }
      for (i = 0; i < sq.solEditeursTextes.length; i++) {
        tab = sq.solEditeursTextes[i]
        res = true
        for (j = 0; j < tab.length && res; j++) { // tab.length doit être égal à sq.nbTextEdit
          const chcasse = sq.textesSensiblesCasse ? sq.repText[j] : sq.repText[j].toUpperCase()
          res = res && (tab[j].indexOf(chcasse) !== -1)
        }
        if (res) {
          sq.repResoluText = true
          break
        } else {
          sq.repResoluText = false
        }
      }
      sq.resolu = resolu && sq.repResoluText
      sq.exact = exact && auMoinsUnExact && listesOK && sq.repResoluText
    }
  }

  function recopierReponse () {
    for (let ind = sq.nbCalc; ind > 0; ind--) {
      const ided = 'expressioninputmq' + ind
      // demarqueEditeurPourErreur(ided)
      // $('#' + ided).mathquill('latex', sq.rep[ind - 1]).blur()
      const mf = j3pElement(ided)
      mf.value = sq.rep[ind - 1]
      if (ind === 0) mf.focus()
    }
    let indiceEditable = 0
    let firstEdit = null
    let idFirstEdit = ''
    let contentFirstEdit = ''
    j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf, index) => {
      // On n’utilise pas mf.getPromps() pour parcourir ses placeholders à cause du problème des matrices
      // Pour garder la compatbilité avec mathquill les indices vont croissants par colonne et pas par ligne
      const indPhBlock = index + 1
      const nbPrompts = mf.getPrompts().length
      for (let i = 0; i < nbPrompts; i++) {
        const content = sq.repEditable[indiceEditable]
        indiceEditable++
        const id = 'expressionPhBlock' + indPhBlock + 'ph' + indiceEditable
        mf.setPromptContent(id, content)
        demarqueEditeurPourErreur(mf.id)
        if (index === 0 && indiceEditable === 1) {
          firstEdit = mf
          idFirstEdit = id
          contentFirstEdit = content
        }
      }
      /*
                                                mf.getPrompts().forEach((id, index) => {
                                                  const content = sq.repEditable[indiceEditable - 1]
                                                  mf.setPromptContent(id, content)
                                                  demarqueEditeurPourErreur(mf.id)
                                                  if (index === 0 && indiceEditable === 1) {
                                                    firstEdit = mf
                                                    idFirstEdit = id
                                                    contentFirstEdit = content
                                                  }
                                                  indiceEditable++
                                                })
                                                 */
      // On récrit dans le premier placeHolder
      firstEdit.setPromptContent(idFirstEdit, contentFirstEdit)
    })
    for (let ind = sq.nbTextEdit; ind > 0; ind--) {
      const ided = 'expressioninput' + ind
      demarqueEditeurPourErreur(ided)
      $('#' + ided).val(sq.repText[ind - 1]).blur()
    }
    for (let ind = 1; ind <= sq.nbList; ind++) {
      if (sq.listesLatex) {
        sq['listeDeroulante' + ind].select(sq['replist' + ind])
      } else {
        j3pElement('expressionliste' + ind).selectedIndex = sq['replist' + ind]
      }
    }
    setFocusToFirstInput('expression')
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    if (sq.solution) {
      document.querySelectorAll('.math-field').forEach(mf => {
        mf.virtualKeyboard.hideThis()
      })
      sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
      $('#divSolution').css('display', 'block').html('')
      let ch = extraitDeLatexPourMathlive(getMtgList(), 'solution')
      if (ch !== '') {
        const styles = parcours.styles
        const st = replaceOldMQCodes(ch)
        afficheMathliveDans('divSolution', 'solution', st, { style: { color: bexact ? styles.cbien : styles.colorCorrection } })
      }
      if (sq.figSol !== '') {
        const mtg32App = sq.mtgAppLecteur
        $('#divExplications').css('display', 'block').html('')
        $('#mtg32svgsol').css('display', 'block')
        mtg32App.removeDoc('mtg32svgsol')
        mtg32App.addDoc('mtg32svgsol', sq.figSol)
        // On remplace dans la figure de solution les paramètres par les valeurs correspondant dans la figure de texte
        ch = 'abcdefghjklmnpqr'
        for (let j = 0; j < ch.length; j++) {
          const car = ch.charAt(j)
          if (sq.param.indexOf(car) !== -1) {
            const par = mtg32App.valueOf('mtg32svg', car)
            mtg32App.giveFormula2('mtg32svgsol', car, par)
          }
        }
        mtg32App.calculate('mtg32svgsol', false)
        const param = {}
        for (let k = 0; k < sq.nbLatexSol; k++) {
          param[ch.charAt(k)] = mtg32App.getLatexCode('mtg32svgsol', k)
        }
        j3pAffiche('divExplications', 'solution', sq.explicationSol, param)
        mtg32App.display('mtg32svgsol')
      }
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
    if (sq.figSol !== '') {
      $('#divExplications').css('display', 'none').html('')
      $('#mtg32svgsol').css('display', 'none')
    }
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      sq.marked = {}
      let code, par, car, i, j, k, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      alertIfErrors()
      // Il faut rendre le clavier mathlive par défaut cahcé
      renderMathInDocument({ renderAccessibleContent: '' })
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar', true)
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i, true)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }

      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.nbLatex)
      const t = 'abcdefghijklmnopq'
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[t.charAt(i)] = code
      }
      sq.parametres = param // Mémorisation pour l’étape 2

      let chenonce
      if (sq.mtgAppLecteur.getList('mtg32svg').getByTag('enonce', true)) {
        chenonce = replaceOldMQCodes(extraitDeLatexPourMathlive(getMtgList(), 'enonce'))
      } else {
        chenonce = replaceOldMQCodes(sq.enonce)
      }
      afficheMathliveDans('enonce', 'texte', chenonce, param)
      creeEditeurs()
      if (sq.boutonsMathQuill) {
        const firstEdit = document.querySelector('math-field')
        if (firstEdit) {
          j3pPaletteMathquill('boutonsmathquill', firstEdit, {
            liste: sq.listeBoutons,
            nomdiv: 'palette'
          })
        }
        // Ligne suivante pour un bon alignement
        $('#palette').css('display', 'inline-block')
      } else {
        j3pDetruit('boutonsmathquill')
      }
      afficheNombreEssaisRestants()
      setFocusToFirstInput('expression')
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)
    // ON précise quel est le conteneur dy clavier virtuel
    setKeyboardContainer(parcours.zonesElts.MG)
    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    // var enonce = j3pElement('enonce')
    // j3pAddContent(enonce, 'Chargement en cours…', { replace: true }) // on vide et remplace
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    if (sq.bigSize) $('#formules').css('font-size', '26px')
    j3pDiv('conteneur', 'info', '')
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
    if (sq.bigSize) $('#editeur').css('font-size', '30px')
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    // On crée la liste des boutons disponibles
    // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
    sq.listeBoutons = []
    // Tous les boutons sont à false par défaut
    const tabBoutons1 = ['puissance', 'fraction', 'pi', 'racine', 'exp', 'ln', 'log', 'sin', 'cos', 'tan', 'abs', 'conj', 'integ', 'prim', 'inf']
    ;['btnPuis', 'btnFrac', 'btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnLog', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj', 'btnInteg', 'btnPrim', 'btnInf'].forEach((p, i) => {
      if (sq[p]) sq.listeBoutons.push(tabBoutons1[i])
    })
    sq.boutonsMathQuill = sq.listeBoutons.length !== 0
    j3pDiv('conteneur', 'divSolution', '')
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    if (sq.figSol !== '') {
      j3pDiv('conteneur', 'divExplications', '')
      $('#divExplications').css('display', 'none')
      j3pDiv('conteneur', 'divFigSol', '')
      j3pCreeSVG('divFigSol', {
        id: 'mtg32svgsol',
        width: sq.widthSol,
        height: sq.heightSol
      })
      $('#mtg32svgsol').css('display', 'none')
    }
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    initMtg()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        // compteur.
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbexp = 0
        sq.reponse = -1
        if (parcours.donneesSection.calculatrice) {
          j3pDiv('MepMD', 'emplacecalc', '')
          this.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
          j3pCreeFenetres(this)
          j3pAjouteBouton('emplacecalc', 'BCalculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
        }
        // Attention, le this devient window...
        for (let i = 1; i < 5; i++) sq['enonceLigne' + i] = parcours.donneesSection['enonceLigne' + i]
        sq.fig = parcours.donneesSection.fig
        sq.nbLatex = parcours.donneesSection.nbLatex // Le nombre de paramètres LaTeX dans le texte
        sq.width = parcours.donneesSection.width
        sq.height = parcours.donneesSection.height
        sq.solution = parcours.donneesSection.solution
        sq.listesLatex = parcours.donneesSection.listesLatex === undefined ? false : parcours.donneesSection.listesLatex
        const tabBtn = ['Puis', 'Frac', 'Pi', 'Cos', 'Sin', 'Tan', 'Rac', 'Exp', 'Ln', 'Log', 'Abs', 'Conj', 'Integ', 'Prim']
        for (let i = 0; i < tabBtn.length; i++) sq['btn' + tabBtn[i]] = parcours.donneesSection['btn' + tabBtn[i]]
        sq.variables = parcours.donneesSection.variables
        sq.titre = parcours.donneesSection.titre
        sq.bigSize = parcours.donneesSection.bigSize
        sq.formulaire = parcours.donneesSection.formulaire // Contient la chaine avec les éditeurs
        sq.formulaireInit = sq.formulaire // Mémorisé pour le cas d’un formulaire dynamique
        sq.param = parcours.donneesSection.param // Chaine contenant les paramètres autorisés
        sq.charset = parcours.donneesSection.charset
        sq.textesSensiblesCasse = parcours.donneesSection.textesSensiblesCasse
        sq.charsetText = parcours.donneesSection.charsetText
        sq.figSol = parcours.donneesSection.figSol
        sq.nbLatexSol = parcours.donneesSection.nbLatexSol
        sq.widthSol = parcours.donneesSection.widthSol
        sq.heightSol = parcours.donneesSection.heightSol
        sq.explicationSol = parcours.donneesSection.explicationSol
        if (sq.fig === '') {
          sq.fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAUeAAACygAAAQEAAAAAAAAAAQAAACr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAVuYnZhcgABNAAAAAFAEAAAAAAAAAAAAAIA#####wAGbmJjYXMxAAEzAAAAAUAIAAAAAAAAAAAAAgD#####AAZuYmNhczIAATIAAAABQAAAAAAAAAAAAAACAP####8ABm5iY2FzMwABMwAAAAFACAAAAAAAAAAAAAIA#####wACcjEAE2ludChyYW5kKDApKm5iY2FzMSn#####AAAAAgAJQ0ZvbmN0aW9uAv####8AAAABAApDT3BlcmF0aW9uAgAAAAMRAAAAAQAAAAAAAAAAP9IyGBcLUsj#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAAgAAAAIA#####wACcjIAE2ludChyYW5kKDApKm5iY2FzMikAAAADAgAAAAQCAAAAAxEAAAABAAAAAAAAAAA#6#d3wEBlLgAAAAUAAAADAAAAAgD#####AAJyMwATaW50KHJhbmQoMCkqbmJjYXMzKQAAAAMCAAAABAIAAAADEQAAAAEAAAAAAAAAAD+mzs5wU8ugAAAABQAAAAQAAAACAP####8AAWEAEGludChyYW5kKDApKjcpLTMAAAAEAQAAAAMCAAAABAIAAAADEQAAAAEAAAAAAAAAAD#c#zthpm94AAAAAUAcAAAAAAAAAAAAAUAIAAAAAAAAAAAAAgD#####AAFiABBpbnQocmFuZCgwKSo3KS0zAAAABAEAAAADAgAAAAQCAAAAAxEAAAABAAAAAAAAAAA#vEWcFZItYAAAAAFAHAAAAAAAAAAAAAFACAAAAAAAAAAAAAIA#####wABYwAQaW50KHJhbmQoMCkqNyktMwAAAAQBAAAAAwIAAAAEAgAAAAMRAAAAAQAAAAAAAAAAP9tjb5kV+uAAAAABQBwAAAAAAAAAAAABQAgAAAAAAAAAAAACAP####8AAWQAEGludChyYW5kKDApKjcpLTMAAAAEAQAAAAMCAAAABAIAAAADEQAAAAEAAAAAAAAAAD#uPLN3qKasAAAAAUAcAAAAAAAAAAAAAUAIAAAAAAAAAAAAAgD#####AAFlABBpbnQocmFuZCgwKSo3KS0zAAAABAEAAAADAgAAAAQCAAAAAxEAAAABAAAAAAAAAAA#5JRnWmNDugAAAAFAHAAAAAAAAAAAAAFACAAAAAAAAAAAAAIA#####wABZgAQaW50KHJhbmQoMCkqNyktMwAAAAQBAAAAAwIAAAAEAgAAAAMRAAAAAQAAAAAAAAAAP6sW03Pc6EAAAAABQBwAAAAAAAAAAAABQAgAAAAAAAAAAAACAP####8AAWcAEGludChyYW5kKDApKjcpLTMAAAAEAQAAAAMCAAAABAIAAAADEQAAAAEAAAAAAAAAAD#nWx#BERnQAAAAAUAcAAAAAAAAAAAAAUAIAAAAAAAAAAAAAgD#####AAFoABBpbnQocmFuZCgwKSo3KS0zAAAABAEAAAADAgAAAAQCAAAAAxEAAAABAAAAAAAAAAA#6dw45iRkfAAAAAFAHAAAAAAAAAAAAAFACAAAAAAAAAAAAAIA#####wABawAEcjErMQAAAAQAAAAABQAAAAUAAAABP#AAAAAAAAAAAAACAP####8AAXAADigtMSlecjIqKHIzKzEpAAAABAL#####AAAAAQAKQ1B1aXNzYW5jZf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABP#AAAAAAAAAAAAAFAAAABgAAAAQAAAAABQAAAAcAAAABP#AAAAAAAAAAAAACAP####8AAXgAB2sqYStwKmUAAAAEAAAAAAQCAAAABQAAABAAAAAFAAAACAAAAAQCAAAABQAAABEAAAAFAAAADAAAAAIA#####wABeQAHaypiK3AqZgAAAAQAAAAABAIAAAAFAAAAEAAAAAUAAAAJAAAABAIAAAAFAAAAEQAAAAUAAAANAAAAAgD#####AAF6AAdrKmMrcCpnAAAABAAAAAAEAgAAAAUAAAAQAAAABQAAAAoAAAAEAgAAAAUAAAARAAAABQAAAA4AAAACAP####8AAXQAB2sqZCtwKmgAAAAEAAAAAAQCAAAABQAAABAAAAAFAAAACwAAAAQCAAAABQAAABEAAAAFAAAADwAAAAIA#####wAEdmFseAABeAAAAAUAAAASAAAAAgD#####AAR2YWx5AAF5AAAABQAAABMAAAACAP####8ABHZhbHoAAXoAAAAFAAAAFAAAAAIA#####wAEdmFsdAABdAAAAAUAAAAV#####wAAAAIABkNMYXRleAD#####AAAAAAEABmVub25jZf####8QQEAAAAAAAABAONcKPXCj2AAAAAAAAAAAAAAAAAABAAAAAAAAAAAAaFxiZWdpbnthcnJheX17bH0KXHRleHR7T24gZGVtYW5kZSBkZSBkb25uZXIgbGUgcsOpc3VsdGF0IGR1IGNhbGN1bCBtYXRyaWNpZWwgc3VpdmFudCA6fSAKXFwgClxlbmR7YXJyYXl9AAAACAD#####AAAAAAEACmZvcm11bGFpcmX#####EEBBAAAAAAAAQFO1wo9cKPYAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAP9cdGV4dHskClxWYWx7aywwLDF9ClxiZWdpbnttYXRyaXh9ClxWYWx7YX0gJiBcVmFse2N9ClxcIFxWYWx7Yn0gJiBcVmFse2R9ClxlbmR7bWF0cml4fQpcVmFse3AsMCwrMX0KXGJlZ2lue21hdHJpeH0KXFZhbHtlfSAmIFxWYWx7Z30KXFwgXFZhbHtmfSAmIFxWYWx7aH0KXGVuZHttYXRyaXh9Cj0KXGJlZ2lue21hdHJpeH0KXGVkaXRhYmxle30gJiBcZWRpdGFibGV7fQpcXCBcZWRpdGFibGV7fSAmIFxlZGl0YWJsZXt9ClxlbmR7bWF0cml4fQoKJH0AAAACAP####8ADHJlcEVkaXRhYmxlMQABMAAAAAEAAAAAAAAAAAAAAAIA#####wAMcmVwRWRpdGFibGUyAAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAxyZXBFZGl0YWJsZTMAATAAAAABAAAAAAAAAAAAAAACAP####8ADHJlcEVkaXRhYmxlNAABMAAAAAEAAAAAAAAAAP####8AAAAEABBDVGVzdEVxdWl2YWxlbmNlAP####8AD3Jlc29sdUVkaXRhYmxlMQAAABYAAAAcAQAAAAABP#AAAAAAAAABAQEAAAAJAP####8AD3Jlc29sdUVkaXRhYmxlMgAAABcAAAAdAQAAAAABP#AAAAAAAAABAQEAAAAJAP####8AD3Jlc29sdUVkaXRhYmxlMwAAABgAAAAeAQAAAAABP#AAAAAAAAABAQEAAAAJAP####8AD3Jlc29sdUVkaXRhYmxlNAAAABkAAAAfAQAAAAABP#AAAAAAAAABAQH#####AAAAAQAFQ0ZvbmMA#####wAEemVybwASYWJzKHgpPDAuMDAwMDAwMDAxAAAABAQAAAADAP####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAABPhEuC+gm1pUAAXgAAAACAP####8ADmV4YWN0RWRpdGFibGUxABd6ZXJvKHZhbHgtcmVwRWRpdGFibGUxKf####8AAAABAA5DQXBwZWxGb25jdGlvbgAAACQAAAAEAQAAAAUAAAAWAAAABQAAABwAAAACAP####8ADmV4YWN0RWRpeWFibGUyABd6ZXJvKHZhbHktcmVwRWRpdGFibGUyKQAAAAwAAAAkAAAABAEAAAAFAAAAFwAAAAUAAAAdAAAAAgD#####AA5leGFjdEVkaXRhYmxlMwAXemVybyh2YWx6LXJlcEVkaXRhYmxlMykAAAAMAAAAJAAAAAQBAAAABQAAABgAAAAFAAAAHgAAAAIA#####wAOZXhhY3RFZGl0YWJsZTQAF3plcm8odmFseC1yZXBFZGl0YWJsZTQpAAAADAAAACQAAAAEAQAAAAUAAAAWAAAABQAAAB8AAAAIAP####8AAAAAAQAIc29sdXRpb27#####EEBAgAAAAAAAQGK64UeuFHsAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAeZcYmVnaW57YXJyYXl9e2x9Clx0ZXh0e1BvdXIgY2FsY3VsZXIgY2V0dGUgY29tYmluYWlzb24gbGluw6lhaXJlIGRlIG1hdHJpY2VzIG9uIGVmZmVjdHVlIGxlcyBtw6ptZXMgY29tYmluYWlzb25zIGxpbsOpYWlyZXMgdGVybWUgw6AgdGVybWUgOn0KXFwgXHRleHR7JApcVmFse2ssMCwxfQpcYmVnaW57bWF0cml4fQpcVmFse2F9ICYgXFZhbHtjfQpcXCBcVmFse2J9ICYgXFZhbHtkfQpcZW5ke21hdHJpeH0KXFZhbHtwLDAsKzF9ClxiZWdpbnttYXRyaXh9ClxWYWx7ZX0gJiBcVmFse2d9ClxcIFxWYWx7Zn0gJiBcVmFse2h9ClxlbmR7bWF0cml4fQo9ClxiZWdpbnttYXRyaXh9ClxGb3JTaW1we3h9ICYgXEZvclNpbXB7en0KXFwgXEZvclNpbXB7eX0gJiBcRm9yU2ltcHt0fQpcZW5ke21hdHJpeH0KPQpcYmVnaW57bWF0cml4fQpcVmFse3h9ICYgXFZhbHt6fQpcXCBcVmFse3l9ICYgXFZhbHt0fQpcZW5ke21hdHJpeH0KJAp9IApcXCAKXGVuZHthcnJheX3###############8='
          sq.charset = '\\d+-*/()^²xy='
          sq.charsetText = 'A-Z=$0-9():'
          sq.textesSensiblesCasse = false
          sq.listesLatex = false
          sq.width = 0
          sq.height = 0
          sq.btnFrac = true
          sq.btnPuis = false
          sq.titre = 'Ressource à personnaliser'
          j3pShowError('Cette ressource doit être personnalisée en éditant la ressource dans l’éditeur de graphes.')
        }
        sq.enonce = ''
        for (let i = 1; i < 5; i++) {
          if (sq['enonceLigne' + i] !== '') {
            if (i !== 1) sq.enonce += '<br>'
            sq.enonce += sq['enonceLigne' + i]
          }
        }
        initDom()
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        cacheSolution()
        sq.marked = {}
        montreEditeurs(true)
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs aléatoires soient réinitialisés
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')

        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nbLatex)
        const t = 'abcdefghijklmnopq'
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          param[t.charAt(i)] = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        }
        sq.parametres = param
        if (sq.mtgAppLecteur.getList('mtg32svg').getByTag('enonce', true)) {
          afficheMathliveDans('enonce', 'texte', extraitDeLatexPourMathlive(getMtgList(), 'enonce'))
        } else {
          afficheMathliveDans('enonce', 'texte', sq.enonce, param)
        }
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'

        creeEditeurs()
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
        }
        setFocusToFirstInput('expression')
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      if (validationEditeurs()) {
        validation()
        if (sq.resolu) {
          bilanReponse = 'exact'
        } else {
          if (sq.exact) {
            bilanReponse = 'exactPasFini'
          } else {
            bilanReponse = 'faux'
          }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        montreEditeurs(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        afficheSolution(false)
        j3pAddContent('correction', tempsDepasse, { replace: true })
        if (sq.solution) j3pAddContent('correction', textes.solutionIci)
        else j3pAddContent('correction', textes.solDesact)
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          let ch = textes.repIncorrecte
          if (sq.signeEgalManquant) ch += textes.egaliteNecess
          j3pAddContent('correction', ch, { replace: true })
        } else {
          // Bonne réponse
          if (bilanReponse === 'exact') {
            this.score += 1
            j3pElement('correction').style.color = this.styles.cbien
            j3pAddContent('correction', cBien, { replace: true })
            montreEditeurs(false)
            sq.numEssai++ // Pour un affichage correct dans afficheReponse
            afficheReponse('exact', false)
            this._stopTimer()
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            afficheSolution(true)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
              j3pAddContent('correction', textes.bonPasFini, { replace: true })
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pAddContent('correction', cFaux, { replace: true })
            }
            j3pEmpty('info')
            if (sq.numEssai <= sq.nbEssais) {
              afficheNombreEssaisRestants()
              afficheReponse(bilanReponse, false)
              videEditeurs()
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              j3pAddContent('correction', '\n' + '<br>' + essaieEncore)
            } else {
              // Erreur au nème essai
              this._stopTimer()
              montreEditeurs(false)
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              afficheReponse(bilanReponse, true)
              afficheSolution(false)
              if (sq.solution) j3pAddContent('correction', textes.solutionIci)
              else j3pAddContent('correction', textes.solDesact)
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    } // case 'correction'
    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
