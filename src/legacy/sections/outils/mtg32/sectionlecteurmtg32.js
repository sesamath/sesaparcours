import { j3pDiv, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
/*
    Yves Biton
    Juillet 2017

    sectionlecteurmtg32
    Sert à afficher une figure MathGraph32 précédée ou non de lusqu'à 4 lignes et suivie ou non de jsuqu'à 4 lignes
    Les lignes peuvent contenir du code LaTeX.
    Utilise l’outil Math pour pouvoir mettre du LaTeX dans les lignes précédent ou suivant la figure
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['titre', 'Figure MathGraph32', 'string', 'Titre de la figure'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['width', 600, 'entier', 'Largeur en pixels de la figure (600 par défaut)'],
    ['height', 500, 'entier', 'Largeur en pixels de la figure (500 par défaut)'],
    ['fig', '', 'editor', 'Chaîne Base 64 contenant la figure à afficher', initEditor],
    ['avantligne1', '', 'string', 'Ligne 1 précédant la figure (peut être une chaîne vide)'],
    ['avantligne2', '', 'string', 'Ligne 2 précédant la figure (peut être une chaîne vide)'],
    ['avantligne3', '', 'string', 'Ligne 3 précédant la figure (peut être une chaîne vide)'],
    ['avantligne4', '', 'string', 'Ligne 4 précédant la figure (peut être une chaîne vide)'],
    ['apresligne1', '', 'string', 'Ligne 1 suivant la figure (peut être une chaîne vide)'],
    ['apresligne2', '', 'string', 'Ligne 2 suivant la figure (peut être une chaîne vide)'],
    ['apresligne3', '', 'string', 'Ligne 3 suivant la figure (peut être une chaîne vide)'],
    ['apresligne4', '', 'string', 'Ligne 4 suivant la figure (peut être une chaîne vide)']
  ]
}

// cf src/legacy/sections/outils/mtg32/sectionex_Calc_Multi_Edit_Multi_Etapes.js pour ajouter un validator
/**
 * Ajoute l’éditeur mathgraph dans container, la promesse sera résolue avec la figure
 * @param {HTMLElement} container
 * @param {string} initalFig
 * @return {Promise<string>}
 */
async function initEditor (container, fig) {
  const { default: editor } = await import('./mtgEditor.js')
  return editor(container, { fig })
}

/**
 * section lecteurmtg32
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage

  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      stor.mtgAppLecteur = mtgAppLecteur
      mtgAppLecteur.removeAllDoc()
      mtgAppLecteur.addDoc('mtg32svg', stor.fig, true)
      // Ci-dessous, emploer local:true pour déboguer en local et false pour version en ligne
      j3pAffiche('avant', 'texteavant', stor.avant + '\n')
      j3pAffiche('apres', 'texteapres', stor.apres)
      mtgAppLecteur.calculateAndDisplayAll(true)
      termineEnonce()
    }).catch(j3pShowError)
  }

  // code commun au if et else du case enonce, mais qui doit être exécuté après chargement async dans le if
  function termineEnonce () {
    // on l’appelle pour le principe, mais ici ça sert pas à grand chose vu qu’on change le bouton juste après
    me.finEnonce()
  }

  if (this.etat === 'enonce') {
    // code exécuté au lancement de la section, on teste pas debutDeLaSection car y’a jamais d’étapes ni de répétition
    this.setPassive()
    const ds = this.donneesSection
    stor.width = ds.width || 700
    stor.height = ds.height || 500
    // Pour déboguage en local, les caractères # doivent être remplacés par des caractères !
    stor.fig = ds.fig
    // if (stor.fig.indexOf('!') !== -1) stor.fig = stor.fig.replace(new RegExp('!', 'g'), '#')
    if (stor.fig.indexOf('!') !== -1) stor.fig = stor.fig.replace(/!/g, '#')
    stor.nbsol = 1 // Contiendra le nombre de figures pour la correction.
    if (stor.fig === '') {
      stor.fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABA+TMzNAANmcmH###8BAP8BAAAAAAAAAAACLwAAAZkAAAAAAAAAAAAAAAAAAAAI#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEACkNQb2ludEJhc2UA#####wAAAAAADwABVQDAMYAAAAAAAEAcAAAAAAAABQAAQEefPnz58+hAR58+fPnz6P####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAAAAWAAABAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAAPAAFWAEAVAAAAAAAAQBwAAAAAAAAFAAFAQuDBgwYMGAAAAAL#####AAAAAQAIQ1NlZ21lbnQA#####wAAAAAAAAAAAQABAAAAAQAAAAP#####AAAAAQAHQ01pbGlldQD#####AQAAAAAWAAABBQAAAAABAAAAA#####8AAAACAAxDQ29tbWVudGFpcmUA#####wAAAAAAAAAAAAAAAABAGAAAAAAAAAAAAAUPAAAAAAABAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAB###########'
      stor.avant = ''
      stor.apres = ''
      ds.titre = 'Ressource à personnaliser'
      j3pShowError('Cette ressource doit être personnalisée en éditant la ressource dans l’éditeur de graphes.')
    } else {
      stor.avant = ''
      for (let i = 1; i < 5; i++) {
        if (ds['avantligne' + i] !== '') {
          if (i !== 0) stor.avant += '<br>'
          stor.avant += ds['avantligne' + i]
        }
      }
      stor.apres = ''
      for (let i = 1; i < 5; i++) {
        if (ds['apresligne' + i] !== '') {
          if (i !== 0) stor.apres += '<br>'
          stor.apres += ds['apresligne' + i]
        }
      }
    }
    this.construitStructurePage('presentation3')
    if (ds.titre) this.afficheTitre(ds.titre)

    j3pDiv(this.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: this.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'avant', '')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pDiv('conteneur', 'apres', '')
    j3pCreeSVG('divmtg32', { id: 'mtg32svg', width: stor.width, height: stor.height })
    initMtg()
  }
  // section passive, pas de case correction ni navigation
}
