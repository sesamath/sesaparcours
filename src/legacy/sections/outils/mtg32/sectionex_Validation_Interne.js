import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pDiv, j3pElement, j3pEmpty, j3pShowError } from 'src/legacy/core/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { MathfieldElement } from 'mathlive'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { cleanLatexForMl } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans } from 'src/lib/outils/mathlive/display'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/**
 Yves Biton
 Janvier 2021. Revu en janvier 2024 pour passage à Mathlive

 ex_Validation_Interne
 Squelette demandant d’agir sur une figure pour répondre à une question.
 On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
 ex est la référence à un fichier annexe qui comprend entre autres une figure mtg32 chargée de proposée l’exercice et de vérifier la validité des solutions proposées
 Pour savoir la réponse est exacte, on interroge la valeur reponse qui vaut 1 si l’exercice a été résolu et 0 sinon.
 Si la figure contenue dans ex contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbvar
 au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
 toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
 */
// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['nbEssais', 4, 'entier', 'Nombre d’essais maximum autorisés'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['titre', '', 'string', 'Titre de l’activité'],
    ['fig', '', 'editor', 'Chaîne Base 64 contenant la figure de l’exercice', initEditor],
    ['width', 800, 'entier', 'Largeur de la figure initiale'],
    ['height', 600, 'entier', 'Hauteur de la figure initiale'],
    ['param', 'abcd', 'string', 'Chaîne formé des paramètres que l’exercice utilise (de a à r, voir ci-dessous'],
    ['nbLatex', 1, 'entier', 'Nombre d’affichages LaTeX de la figure utilisés dans la consigne (1 au moins et jusque 4)'],
    ['enonceLigne1', '', 'string', 'Première ligne de la consigne initiale'],
    ['enonceLigne2', '', 'string', 'Ligne 2 de la consigne (peut être une chaîne vide)'],
    ['enonceLigne3', '', 'string', 'Ligne 3 de la consigne (peut être une chaîne vide)'],
    ['enonceLigne4', '', 'string', 'Ligne 4 de la consigne (peut être une chaîne vide)'],
    ['enonceFin', 'Cliquer sur le bouton OK pour valider la réponse.', 'string', 'Fin de la consigne'],
    ['figSol', '', 'editor', 'Code Base64 d’une figure supplémentaire pour la correction, vide pour aucune figure supplémentaire', initEditor],
    ['widthSol', 600, 'entier', 'Largeur de la figure supplémentaire de solution'],
    ['heightSol', 500, 'entier', 'Hauteur de la figure supplémentaire de solution'],
    ['nbLatexSol', 1, 'entier', 'Nombre d’affichages LaTeX de la figure supplémentaire à récupérer pour affichage dans la ligne d’explications'],
    ['explicationSol', '', 'string', 'Ligne d’explications précédent la figure supplémentaire de solution (peut être vide)'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['x', 'random', 'string', 'Valeur de x'],
    ['y', 'random', 'string', 'Valeur de y']
  ]
}

// cf src/legacy/sections/outils/mtg32/sectionex_Calc_Multi_Edit_Multi_Etapes.js pour ajouter un validator
/**
 * Ajoute l’éditeur mathgraph dans container, la promesse sera résolue avec la figure
 * @param {HTMLElement} container
 * @param {string} fig
 * @return {Promise<string>}
 */
async function initEditor (container, fig) {
  const { default: editor } = await import('./mtgEditor.js')
  return editor(container, { fig })
}

const textes = {
  ilReste: 'Il reste ',
  essais: ' essais.',
  unEssai: ' un essai.',
  solutionIci: '\n\nLa solution se trouve ci-contre.'
}

/**
 * section ex_Validation_Interne
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function validation () {
    sq.resolu = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse', true) === 1
  }

  function initMtg () {
    setMathliveCss()
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let par, car, i, j, k, nbrep, ar, tir, nb, nbcas, code
      // Un div pour contenir la solution affichée par MathQuill si la figure contient un LaTeX de tag 'solution'
      // et évnuellement les exolications de correction passées en paramètres et la figure solution s’il y en a une
      j3pDiv('conteneur', 'divSolution', '')
      $('#divSolution').css('display', 'none')
      // On crée provisoirement le DIV ci-dessous pour pouvoir calculer la figure.
      // On le détruira ensuite pour le récréer à la bonne position
      j3pDiv('conteneur', 'divmtg32', '')
      j3pCreeSVG('divmtg32', {
        id: 'mtg32svg',
        width: sq.width,
        height: sq.height
      })
      sq.mtgAppLecteur.removeAllDoc()
      // on ajoute la figure que le fichier annexe contient
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, false)
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      const ch = 'abcdefghjklmnpqrxy'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar', true)
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i, true)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      sq.mtgAppLecteur.display('mtg32svg')

      const nbLatex = parseInt(sq.nbLatex)
      // La consigne peut contenir jusqu'à 8 paramètres a, b, c, d, e,f, g, h récupérés comme affichages LaTeX de la figure.
      const t = 'abcdefgh'
      const param = {}
      for (k = 0; k < nbLatex; k++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', k)
        param[t.charAt(k)] = code
      }
      if (sq.mtgAppLecteur.getList('mtg32svg').getByTag('enonce', true)) {
        let enonce = extraitDeLatexPourMathlive('enonce')
        if (sq.enonceFin !== '') enonce += '<br>' + sq.enonceFin + '<br>'
        afficheMathliveDans('divEnonce', 'texte', enonce)
      } else {
        afficheMathliveDans('divEnonce', 'texte', sq.enonce + '\n', param)
      }
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('divInfo')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) afficheMathliveDans('divInfo', 'texteinfo', textes.ilReste + textes.unEssai, { style: { color: '#7F007F' } })
    else afficheMathliveDans('divInfo', 'texteinfo', textes.ilReste + nbe + textes.essais, { style: { color: '#7F007F' } })
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block')
    // Si la figure contient un affichage LaTeX de tag solution on affiche les explications qu’il contient
    // avant l’éventuelle figure solution
    const styles = parcours.styles
    const coul = bexact ? styles.cbien : styles.colorCorrection
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      $('#divSolution').css('display', 'block').html('')
      afficheMathliveDans('divSolution', 'textesolution', ch, { style: { color: coul } })
    }
    if (sq.figSol !== '') {
      let j, car, par
      const width = sq.widthSol
      const height = sq.heightSol
      j3pDiv('divSolution', 'divExplications', '')
      j3pDiv('divSolution', 'divFigSolution', '')
      j3pCreeSVG('divFigSolution', { id: 'mtg32svgsol', width, height })
      sq.mtgAppLecteur.addDoc('mtg32svgsol', sq.figSol, false)
      // On remplace dans la figure de solution les paramètres par les valeurs correspondant dans la figure de texte
      const ch = 'abcdefghjklmnpqrxy'
      for (j = 0; j < ch.length; j++) {
        car = ch.charAt(j)
        if (sq.param.indexOf(car) !== -1) {
          par = sq.mtgAppLecteur.valueOf('mtg32svg', car)
          sq.mtgAppLecteur.giveFormula2('mtg32svgsol', car, par)
        }
      }
      sq.mtgAppLecteur.calculate('mtg32svgsol', true)
      // On récupère les affichages LaTeX à incorporer dans la ligne de correction s’il y en a
      const t = 'abcdefgh'
      const param = { style: { color: coul } }
      for (let k = 0; k < sq.nbLatexSol; k++) {
        const code = sq.mtgAppLecteur.getLatexCode('mtg32svgsol', k)
        param[t.charAt(k)] = code
      }
      if (sq.explicationSol) {
        afficheMathliveDans('divExplications', 'solution', sq.explicationSol, param)
      }
      sq.mtgAppLecteur.display('mtg32svgsol')
    }
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    // Trois lignes suivantes déplacées ici depuis initMtg pour éviter que j3pAffiche("", "divInfo" ne soit appelé avant la création du div divInfo
    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'divEnonce', '')
    j3pDiv('conteneur', 'divInfo', '')
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    afficheNombreEssaisRestants()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        this.construitStructurePage('presentation1bis')

        // compteur. Si   numEssai > nbEssais, on corrige
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        for (let i = 1; i < 5; i++) sq['enonceLigne' + i] = parcours.donneesSection['enonceLigne' + i]
        sq.fig = parcours.donneesSection.fig
        sq.nbLatex = parcours.donneesSection.nbLatex // Le nombre de paramètres LaTeX dans le texte
        if (sq.fig === '') {
          sq.enonceLigne1 = 'Il faut ci-dessous déplacer le point B de façon qu’on ait $£a$ puis cliquer sur le bouton OK.'
          sq.nbLatex = 1
          sq.width = 750
          sq.height = 520
          sq.param = 'ab'
          sq.fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAP5AAAChwAAAQEAAAAAAAAAAQAAAGf#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAZuYmNhczEAATIAAAABQAAAAAAAAAAAAAACAP####8AAnIxABNpbnQocmFuZCgwKSpuYmNhczEp#####wAAAAIACUNGb25jdGlvbgL#####AAAAAQAKQ09wZXJhdGlvbgIAAAADEQAAAAEAAAAAAAAAAD#iRu8iuPMg#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAEAAAACAP####8ABm5iY2FzMgABNQAAAAFAFAAAAAAAAAAAAAIA#####wACcjIAE2ludChyYW5kKDApKm5iY2FzMikAAAADAgAAAAQCAAAAAxEAAAABAAAAAAAAAAA#7OYDyem##AAAAAUAAAADAAAAAgD#####AAZuYmNhczMAATIAAAABQAAAAAAAAAAAAAACAP####8AAnIzABNpbnQocmFuZCgwKSpuYmNhczMpAAAAAwIAAAAEAgAAAAMRAAAAAQAAAAAAAAAAP+jSiqzkwiAAAAAFAAAABQAAAAIA#####wAGbmJjYXM0AAE0AAAAAUAQAAAAAAAAAAAAAgD#####AAJyNAATaW50KHJhbmQoMCkqbmJjYXM0KQAAAAMCAAAABAIAAAADEQAAAAEAAAAAAAAAAD#VPxU6rAeQAAAABQAAAAcAAAACAP####8ABm5iY2FzNQABMgAAAAFAAAAAAAAAAAAAAAIA#####wACcjUAE2ludChyYW5kKDApKm5iY2FzNSkAAAADAgAAAAQCAAAAAxEAAAABAAAAAAAAAAA#6QxV1oK5hgAAAAUAAAAJAAAAAgD#####AAZuYmNhczYAATUAAAABQBQAAAAAAAAAAAACAP####8AAnI2ABNpbnQocmFuZCgwKSpuYmNhczYpAAAAAwIAAAAEAgAAAAMRAAAAAQAAAAAAAAAAP+DEkAaujQQAAAAFAAAACwAAAAIA#####wAGbmJjYXM3AAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAJyNwATaW50KHJhbmQoMCkqbmJjYXM3KQAAAAMCAAAABAIAAAADEQAAAAEAAAAAAAAAAD#uzUVuCwi8AAAABQAAAA0AAAACAP####8ABm5iY2FzOAABNAAAAAFAEAAAAAAAAAAAAAIA#####wACcjgAE2ludChyYW5kKDApKm5iY2FzOCkAAAADAgAAAAQCAAAAAxEAAAABAAAAAAAAAAA#7LjC5WIhHAAAAAUAAAAP#####wAAAAEACkNQb2ludEJhc2UA#####wAAAAAADgABTwDAKAAAAAAAAAAAAAAAAAAAAAAFAAFAd6AAAAAAAEBvCj1wo9cK#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAAABAAAAEQE#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wAAAAABDgABSQDAGAAAAAAAAAAAAAAAAAAAAAAFAAFAPQAAAAAAAAAAABL#####AAAAAQAJQ0Ryb2l0ZUFCAP####8AAAAAABAAAAEAAAABAAAAEQAAABP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AAAAAAAQAAABAAAAAQAAABEAAAAU#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAAAAAEAAAARAAAAE#####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAVAAAAFv####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAEAAAUAAQAAABcAAAANAP####8AAAAAAQ4AAUoAwCgAAAAAAADAEAAAAAAAAAAABQACAAAAF#####8AAAACAAdDUmVwZXJlAP####8A5ubmAAAAAQAAABEAAAATAAAAGQEBAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAP####8AAAABAApDVW5pdGV4UmVwAP####8ABHVuaXQAAAAa#####wAAAAEAC0NIb21vdGhldGllAP####8AAAARAAAABAMAAAABP#AAAAAAAAAAAAAFAAAAG#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEAAAEAAAAAEwAAABz#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAARAAAAHf####8AAAABAAhDVmVjdGV1cgD#####AP8AAAAQAAABAAAAAgAAABEAAAATAAAAABMA#####wAAAP8AEAAAAQAAAAIAAAARAAAAGQAAAAACAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAAAgD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABRHcmFkdWF0aW9uQXhlc1JlcGVyZQAAABsAAAAIAAAAAwAAABoAAAAhAAAAIv####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAACMABWFic29yAAAAGv####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAACMABW9yZG9yAAAAGgAAAA8AAAAAIwAGdW5pdGV4AAAAGv####8AAAABAApDVW5pdGV5UmVwAAAAACMABnVuaXRleQAAABr#####AAAAAQAQQ1BvaW50RGFuc1JlcGVyZQAAAAAjAAAAAAAQAAABAAAFAAAAABoAAAAFAAAAJAAAAAUAAAAlAAAAGAAAAAAjAAAAAAAQAAABAAAFAAAAABoAAAAEAAAAAAUAAAAkAAAABQAAACYAAAAFAAAAJQAAABgAAAAAIwAAAAAAEAAAAQAABQAAAAAaAAAABQAAACQAAAAEAAAAAAUAAAAlAAAABQAAACcAAAAQAAAAACMAAAAoAAAABQAAACEAAAARAAAAACMAAAAAABAAAAEAAAUAAAAAKQAAACsAAAAQAAAAACMAAAAoAAAABQAAACIAAAARAAAAACMAAAAAABAAAAEAAAUAAAAAKgAAAC3#####AAAAAQAIQ1NlZ21lbnQAAAAAIwEAAAAAEAAAAQAAAAEAAAApAAAALAAAABkAAAAAIwEAAAAAEAAAAQAAAAEAAAAqAAAALgAAAAgAAAAAIwEAAAAACwABVwDAFAAAAAAAAMA0AAAAAAAAAAAFAAE#3FZ4mrzfDgAAAC######AAAAAgAIQ01lc3VyZVgAAAAAIwAGeENvb3JkAAAAGgAAADEAAAACAAAAACMABWFic3cxAAZ4Q29vcmQAAAAFAAAAMv####8AAAACABJDTGlldU9iamV0UGFyUHRMaWUBAAAAIwBmZmYAAAAAADEAAAAFAAAAIQAAADEAAAACAAAAMQAAADEAAAACAAAAACMABWFic3cyAA0yKmFic29yLWFic3cxAAAABAEAAAAEAgAAAAFAAAAAAAAAAAAAAAUAAAAkAAAABQAAADMAAAAYAAAAACMBAAAAABAAAAEAAAUAAAAAGgAAAAUAAAA1AAAABQAAACUAAAAbAQAAACMAZmZmAAAAAAA2AAAABQAAACEAAAAxAAAABQAAADEAAAAyAAAAMwAAADUAAAA2AAAACAAAAAAjAQAAAAALAAFSAEAgAAAAAAAAwCAAAAAAAAAAAAUAAT#RG06BtOgfAAAAMP####8AAAACAAhDTWVzdXJlWQAAAAAjAAZ5Q29vcmQAAAAaAAAAOAAAAAIAAAAAIwAFb3JkcjEABnlDb29yZAAAAAUAAAA5AAAAGwEAAAAjAGZmZgAAAAAAOAAAAAUAAAAiAAAAOAAAAAIAAAA4AAAAOAAAAAIAAAAAIwAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAAEAQAAAAQCAAAAAUAAAAAAAAAAAAAABQAAACUAAAAFAAAAOgAAABgAAAAAIwEAAAAAEAAAAQAABQAAAAAaAAAABQAAACQAAAAFAAAAPAAAABsBAAAAIwBmZmYAAAAAAD0AAAAFAAAAIgAAADgAAAAFAAAAOAAAADkAAAA6AAAAPAAAAD3#####AAAAAgAMQ0NvbW1lbnRhaXJlAAAAACMBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAAADELAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MSkAAAAbAQAAACMAZmZmAAAAAAA#AAAABQAAACEAAAAxAAAABAAAADEAAAAyAAAAMwAAAD8AAAAdAAAAACMBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAAADYLAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MikAAAAbAQAAACMAZmZmAAAAAABBAAAABQAAACEAAAAxAAAABgAAADEAAAAyAAAAMwAAADUAAAA2AAAAQQAAAB0AAAAAIwFmZmYAwCAAAAAAAAA#8AAAAAAAAAAAAAAAOAsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKG9yZHIxKQAAABsBAAAAIwBmZmYAAAAAAEMAAAAFAAAAIgAAADgAAAAEAAAAOAAAADkAAAA6AAAAQwAAAB0AAAAAIwFmZmYAwBwAAAAAAAAAAAAAAAAAAAAAAAAAPQsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKG9yZHIyKQAAABsBAAAAIwBmZmYAAAAAAEUAAAAFAAAAIgAAADgAAAAGAAAAOAAAADkAAAA6AAAAPAAAAD0AAABFAAAAAgD#####AAVuYnZhcgABOAAAAAFAIAAAAAAAAAAAAAIA#####wABYQAKKC0xKV5yMSpyMgAAAAQC#####wAAAAEACkNQdWlzc2FuY2X#####AAAAAQAMQ01vaW5zVW5haXJlAAAAAT#wAAAAAAAAAAAABQAAAAIAAAAFAAAABAAAAAIA#####wABYgAhc2koYT0wLCgtMSlecjMqKHI0KzEpLCgtMSlecjMqcjQp#####wAAAAEADUNGb25jdGlvbjNWYXIAAAAABAgAAAAFAAAASAAAAAEAAAAAAAAAAAAAAAQCAAAAHgAAAB8AAAABP#AAAAAAAAAAAAAFAAAABgAAAAQAAAAABQAAAAgAAAABP#AAAAAAAAAAAAAEAgAAAB4AAAAfAAAAAT#wAAAAAAAAAAAABQAAAAYAAAAFAAAACP####8AAAACAAZDTGF0ZXgA#####wEAAAABAAD#####EEB#AAAAAAAAQCSj1wo9cKgAAAAAAAAAAAAAAAAAAQAAAAAAAAAAADVcb3ZlcnJpZ2h0YXJyb3cge1xtYXRocm17QUJ9fVxiaW5vbXtcVmFse2F9fXtcVmFse2J9fQAAACEA#####wD#AAAAwCQAAAAAAABAAAAAAAAAAAAAAAAAExAAAAAAAAIAAAAAAAAAAQAAAAAAAAAAAAdcdmVje2l9AAAAIQD#####AAAA#wDAIAAAAAAAAEAUAAAAAAAAAAAAAAAZEAAAAAAAAgAAAAAAAAABAAAAAAAAAAAAB1x2ZWN7an0AAAACAP####8AAWMADigtMSlecjUqKHI2KzEpAAAABAIAAAAeAAAAHwAAAAE#8AAAAAAAAAAAAAUAAAAKAAAABAAAAAAFAAAADAAAAAE#8AAAAAAAAAAAAAIA#####wABZAAOKC0xKV5yNyoocjgrMSkAAAAEAgAAAB4AAAAfAAAAAT#wAAAAAAAAAAAABQAAAA4AAAAEAAAAAAUAAAAQAAAAAT#wAAAAAAAAAAAAGAD#####AH8AAAASAAFBAAAAAAAAAAAAQAgAAAAAAAAAAAEAAAAAGgAAAAUAAABNAAAABQAAAE7#####AAAAAQANQ1BvaW50QmFzZUVudAD#####AH8AAAASAAFCAAAAAAAAAAAAQAgAAAAAAAAAAAEAAQAAABpAJAAAAAAAAMAcAAAAAAAAAQEAAAAaAP####8AAXgAAAAaAAAAUAAAABwA#####wABeQAAABoAAABQ#####wAAAAEABUNGb25jAP####8ABHplcm8AEWFicyh4KTwwLjAwMDAwMDAxAAAABAQAAAADAP####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAABPkV5juIwjDoAAXgAAAACAP####8AB3JlcG9uc2UAF3plcm8oeC1jLWEpJnplcm8oeS1kLWIpAAAABAr#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAABTAAAABAEAAAAEAQAAAAUAAABRAAAABQAAAE0AAAAFAAAASAAAACUAAABTAAAABAEAAAAEAQAAAAUAAABSAAAABQAAAE4AAAAFAAAASQAAAAIA#####wAEZmF1eAAJMS1yZXBvbnNlAAAABAEAAAABP#AAAAAAAAAAAAAFAAAAVAAAABgA#####wH#AAAAEgAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAGgAAAAQDAAAABQAAAFEAAAAFAAAAVQAAAAUAAABSAAAAAgD#####AARhbnVsAANhPTAAAAAECAAAAAUAAABIAAAAAQAAAAAAAAAAAAAAAgD#####AARibnVsAANiPTAAAAAECAAAAAUAAABJAAAAAQAAAAAAAAAA#####wAAAAEAFENUcmFuc2xhdGlvblBhckNvb3JkAP####8AAAAaAAAABQAAAEgAAAABAAAAAAAAAAAAAAARAP####8BfwAAABIAAUMAAAAAAAAAAABACAAAAAAAAAAAAQAAAABPAAAAWQAAABMA#####wH#AAAAEAAAAQAAAAIAAABPAAAAWgD#####AAAAAQAHQ01pbGlldQD#####Af8AAAASAAAAAAAAAAAAAABACAAAAAAAAAAAAQAAAABPAAAAWgAAACEA#####wH#AAAAwAAAAAAAAADACAAAAAAAAAAAAAAAXBAAAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAB9cSWZ7YW51bH17fXtcVmFse2EsMCwxfVx2ZWN7aX19AAAAJgD#####AAAAGgAAAAEAAAAAAAAAAAAAAAUAAABJAAAAEQD#####AX8AAAASAAAAAAAAAAAAAABACAAAAAAAAAAAAQAAAABaAAAAXgAAACEA#####wF#AAAAQAAAAAAAAABACAAAAAAAAAAAAAAAXxIAAAAAAAAAAAAAAAAAAQAAAAAAAAAAABpcSWZ7cmVwb25zZX17fXtcbWF0aHJte0J9fQAAACcA#####wEAAP8AEgAAAAAAAAAAAAAAQAgAAAAAAAAAAAEAAAAAWgAAAF8AAAAhAP####8BAAD#AEAqAAAAAAAAP#AAAAAAAAAAAAAAAGEQAAH###8AAAAAAAAAAQAAAAEAAAAAAAAAAAAfXElme2JudWx9e317XFZhbHtiLDAsMX1cdmVje2p9fQAAABMA#####wEAAP8AEAAAAQAAAAIAAABaAAAAXwAAAAATAP####8BAH8AABAAAAEAAAACAAAATwAAAF8A#####wAAAAEAEENNYWNyb0FwcGFyaXRpb24A#####wEAAP8BAAD#####EEA2AAAAAAAAQD1R64UeuFICAf###wAAAAAAAAAAAAAAAQAAAAAAAAAAAAhzb2x1dGlvbgAAAAAACAAAAFYAAABbAAAAXQAAAGMAAABiAAAAXwAAAGQAAABgAP####8AAAABABFDTWFjcm9EaXNwYXJpdGlvbgD#####AQAA#wEAAP####8QQDQAAAAAAABAUtR64UeuFAIB####AAAAAAAAAAAAAAABAAAAAAAAAAAAEG1hc3F1ZXIgU29sdXRpb24AAAAAAAgAAABWAAAAWwAAAF0AAABjAAAAYgAAAF8AAABkAAAAYAAAAB7##########w=='
          sq.enonceFin = ''
          sq.nbLatexSol = 1
          sq.widthSol = 0
          sq.heightSol = 0
          sq.figSol = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAP5AAAChwAAAQEAAAAAAAAAAQAAAAT#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFiAAEyAAAAAUAAAAAAAAAA#####wAAAAIABkNMYXRleAD#####AQAA#wEAAP####8QQDoAAAAAAABAN1HrhR64UgAAAAAAAAAAAAAAAAABAAAAAAAAAAAAvVxvdmVycmlnaHRhcnJvdyB7XG1hdGhybXtBQn19XGJpbm9te1xWYWx7YX19e1xWYWx7Yn19XHRleHR7IGRhbnMgbGUgcmVww6hyZSB9KFxtYXRocm17MH07XHZlY3tpfSxcdmVje2p9KVx0ZXh0eyBzaWduaWZpZSB9XG92ZXJyaWdodGFycm93IHtcbWF0aHJte0FCfX09XFZhbHthLDAsMX1cdmVje2l9XFZhbHtiLDAsKzF9XHZlY3tqff###############w=='
          sq.explicationSol = '$£a$'
          sq.titre = 'Ressource à personnaliser'
          j3pShowError('Cette ressource doit être personnalisée en éditant la ressource dans l’éditeur de graphes.')
        } else {
          sq.param = parcours.donneesSection.param // Chaine contenant les paramètres autorisés
          sq.width = parcours.donneesSection.width || 700 // La figure doit toujpurs être visible
          sq.height = parcours.donneesSection.height || 520 // La figure doit toujpurs être visible
          sq.enonceFin = parcours.donneesSection.enonceFin
          sq.figSol = parcours.donneesSection.figSol
          sq.widthSol = parcours.donneesSection.widthSol
          sq.heightSol = parcours.donneesSection.heightSol
          sq.explicationSol = parcours.donneesSection.explicationSol
          sq.nbLatexSol = parcours.donneesSection.nbLatexSol
        }
        sq.enonce = ''
        for (let i = 1; i < 5; i++) {
          if (sq['enonceLigne' + i] !== '') {
            if (i !== 1) sq.enonce += '<br>'
            sq.enonce += sq['enonceLigne' + i]
          }
        }
        if (parcours.donneesSection.simplifier && parcours.donneesSection.enonceSimplifier !== '') sq.enonce += '<br>' + parcours.donneesSection.enonceSimplifier
        sq.titre = parcours.donneesSection.titre
        sq.charset = parcours.donneesSection.charset
        sq.enonce += '<br>' + parcours.donneesSection.enonceFin
        initMtg()
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        // On détruit la figure de correction précédente s’il y en avait une
        if (sq.figSol !== '') {
          sq.mtgAppLecteur.removeDoc('mtg32svgsol')
        }
        j3pEmpty('divSolution')
        $('#divSolution').css('display', 'none')
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, false)
        sq.mtgAppLecteur.calculate('mtg32svg', true)
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqrxy'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }
        sq.mtgAppLecteur.calculate('mtg32svg', true)
        j3pEmpty('divEnonce')
        j3pElement('divInfo').style.display = 'block'
        j3pEmpty('divInfo')
        // La consigne peut contenir jusqu'à 8 paramètres a, b, c, d, e,f, g, h récupérés comme affichages LaTeX de la figure.
        const nbLatex = parseInt(sq.nbLatex)
        const t = 'abcdefgh'
        const param = {}
        for (let k = 0; k < nbLatex; k++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', k)
          param[t.charAt(k)] = code
        }
        sq.mtgAppLecteur.display('mtg32svg')
        if (sq.mtgAppLecteur.getList('mtg32svg').getByTag('enonce', true)) {
          let enonce = extraitDeLatexPourMathlive('enonce')
          if (sq.enonceFin !== '') enonce += '<br>' + sq.enonceFin + '<br>'
          afficheMathliveDans('divEnonce', 'texte', enonce)
        } else {
          afficheMathliveDans('divEnonce', 'texte', sq.enonce + '\n', param)
        }
        afficheNombreEssaisRestants()
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.error(e)
        }
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction':
      // A cause de la limite de temps :
      validation()
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('divInfo').style.display = 'none'
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pAddContent('correction', tempsDepasse + textes.solutionIci, { replace: true })
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        sq.numEssai++
        // Bonne réponse
        if (sq.resolu) {
          this.score++
          this._stopTimer()
          sq.mtgAppLecteur.setActive('mtg32svg', false)
          j3pElement('correction').style.color = this.styles.cbien
          j3pAddContent('correction', cBien + textes.solutionIci, { replace: true })
          j3pElement('divInfo').style.display = 'none'
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
          afficheSolution(true)
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pAddContent('correction', cFaux, { replace: true })
          j3pEmpty('divInfo')
          if (sq.numEssai <= sq.nbEssais) {
            afficheNombreEssaisRestants()
            j3pAddContent('correction', '<br>' + '<br>' + essaieEncore, { replace: false })
          } else {
            // Erreur au nème essai
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            j3pElement('divInfo').style.display = 'none'
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(false)
            j3pAddContent('correction', textes.solutionIci, { replace: false })
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
