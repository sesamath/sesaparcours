import $ from 'jquery'
import { MathfieldElement, renderMathInDocument } from 'mathlive'

import '../../../../lib/outils/mathlive/mathliveOverride.scss'
import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { getFirstIndexParExcluded, getIndexFermant } from 'src/lib/utils/string'
import { resetKeyboardPosition, setKeyboardContainer } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet_2018

    Section destinée à faire faire un exercice de calcul basé sur une figure MathGraph32
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,l,m,n,p,q,r
*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}
export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['titre', 'Calcul sur les puissances. Exemple', 'string', 'Titre de l’activité'],
    ['taillePoliceFormulaire', 24, 'entier', 'Taille en pixels de la police pour les éditeurs, 24 par défaut'],
    ['taillePoliceReponses', 22, 'entier', 'Taille en pixels de la police pour les affiches des réponses, 22 par défaut'],
    ['taillePoliceSolution', 22, 'entier', 'Taille en pixels de la police pour l’affichage de la solution, 22 par défaut'],
    ['validationAuto', false, 'boolean', 'Si false (par défaut) :' +
    '<br>L’élève valide ses calculs intermédiaires par la touche Entrée et, quand il estime avoir répondu à la question, clique sur le bouton OK pour valider sa réponse' +
    '<br>Le nombre de calculs intermédiaires permis est nbEssais et le nombre de validations est nbchances' +
    '<br>Si true :' +
    '<br>L’élève valide ses calculs en appuyant sur la touche Entrée ou en cliquant sur le bouton OK.' +
    '<br>Dès qu’une des réponses intermédiaires est acceptée comme réponse finale, la réponse est acceptée.' +
    '<br>L’élève peut faire nbEssais propositions et le paramètre nbchances n’est ici pas utilisé.'],
    ['solution', true, 'boolean', 'false pour que la solution ne soit pas affichée en fin d’exercice'],
    ['calculatrice', false, 'boolean', 'true pour avoir une calculatrice disponible'],
    ['nbchances', 2, 'entier', 'Nombre d’essais autorisés à l’élève en validant par OK (seulement si validationAuto est false)'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['fig', '', 'editor', 'Chaîne Base 64 contenant la figure de l’exercice', initEditor],
    ['width', 700, 'entier', 'Largeur de la figure initiale'],
    ['height', 520, 'entier', 'Hauteur de la figure initiale'],
    ['param', 'abcd', 'string', 'Chaîne formé des paramètres que l’exercice utilise (de a à r, voir ci-dessous'],
    ['bigSize', true, 'boolean', 'true pour une éditeur de grande taille'],
    ['nomCalcul', 'A', 'string', 'Nom de l’expression à calculer'],
    ['nblatex', 1, 'entier', 'Nombre d’affichages LaTeX de la figure utilisés dans la consigne (1 au moins et jusque 4)'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon (une réponse exacte suffit)'],
    ['acceptMult1', false, 'boolean', 'true si on accepte les multiplications par 1 inutiles dans la réponse de l`élève'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['charset', '', 'string', 'Chaîne formée des caractères permis dans l’éditeur, chaîne vide pour autoriser tous les caractères'],
    ['charExclusRepFinale', '', 'string', 'chaine formée de caractères tels que, si la réponse en contient au moins un,' +
    '<br>elle ne pourra pas être considérée comme finale, même si elle est exacte.' +
    '<br>Par exemple, pour un calcul de fraction, entrer la chaîne "*" pour qu’un calcul 2*1/3' +
    '<br>ne soit pas accepté comme calcul final au lieu de 2/3.' +
    '<br>Chaîne vide par défaut.'],
    ['enonceligne1', 'Il faut calculer $£a$ sous la forme la plus simple possible.', 'string', 'Première ligne de la consigne (peut être vide si l’énoncé est dans un affichage LaTeX de tag "solution"'],
    ['enonceligne2', '', 'string', 'ligne 2 de la consigne (peut être une chaîne vide)'],
    ['enonceligne3', '', 'string', 'ligne 3 de la consigne (peut être une chaîne vide)'],
    ['enonceligne4', '', 'string', 'ligne 4 de la consigne (peut être une chaîne vide)'],
    ['enonceligne5', '', 'string', 'ligne 5 de la consigne (peut être une chaîne vide)'],
    ['enonceligne6', '', 'string', 'ligne 6 de la consigne (peut être une chaîne vide)'],
    ['enonceligne7', '', 'string', 'ligne 7 de la consigne (peut être une chaîne vide)'],
    ['enonceligne8', '', 'string', 'ligne 8 de la consigne (peut être une chaîne vide)'],
    ['enoncesimplifier', 'Le calcul final doit être écrit sous la forme la plus simple possible', 'string', 'Consigne supplémentaire éventuelle à afficher si la réponse doit être donnée simplifiée'],
    ['enoncefin', 'Appuyer sur la touche Entrée pour valider toutes les étapes, puis sur le bouton OK après la réponse finale pour valider.', 'string', 'Fin de la consigne'],
    ['enoncefinPourValidationAuto', 'Appuyer sur la touche Entrée ou cliquer sur le bouton OK pour valider les étapes du calcul.', 'string', 'Fin de la consigne en cas de validation automatique'],
    ['btnPuis', true, 'boolean', 'Bouton puissance'],
    ['btnFrac', true, 'boolean', 'Bouton fraction'],
    ['btnPi', false, 'boolean', 'Bouton pi'],
    ['btnCos', false, 'boolean', 'Bouton cosinus'],
    ['btnSin', false, 'boolean', 'Bouton sinus'],
    ['btnTan', false, 'boolean', 'Bouton tangente'],
    ['btnRac', false, 'boolean', 'Bouton Racine carrée'],
    ['btnExp', false, 'boolean', 'Bouton exponentielle'],
    ['btnLn', false, 'boolean', 'Bouton logarithme népérien'],
    ['btnLog', false, 'boolean', 'Bouton logarithme décimal'],
    ['btnAbs', false, 'boolean', 'Bouton valeurabsolue'],
    ['btnConj', false, 'boolean', 'Bouton conjugué'],
    ['btnInteg', false, 'boolean', 'Bouton intégrale'],
    ['btnPrim', false, 'boolean', 'Bouton primitive'],
    ['btnDiff', false, 'boolean', 'Bouton différentielle'],
    ['variables', 'xt', 'string', 'Noms des variables autorisées pour un calcul d’intégrale ou de primitive'],
    ['figSol', '', 'editor', 'Code Base64 d’une figure supplémentaire pour la correction, vide pour aucune figure supplémentaire', initEditor],
    ['widthSol', 600, 'entier', 'Largeur de la figure supplémentaire de solution'],
    ['heightSol', 500, 'entier', 'Hauteur de la figure supplémentaire de solution'],
    ['nblatexSol', 1, 'entier', 'Nombre d’affichages LaTeX de la figure supplémentaire à récupérer pour affichage dans la ligne d’explications'],
    ['explicationSol', '', 'string', 'Ligne d’explications précédent la figure supplémentaire de solution (peut être vide)'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r']
  ]
}

const textes = {
  fauteSynt: 'Faute de syntaxe',
  Incorrect: 'Réponse incorrecte',
  cBienMais: 'C’est bien mais on pouvait\nsimplifier la réponse',
  bonPasFini: 'Le calcul est bon mais pas écrit\n sous la forme demandée.',
  solutionIci: '\nLa solution se trouve ci-contre.',
  solDesact: '\nL’affichage de la solution a été désactivé par le professeur.',
  ilReste: 'Il reste ',
  essais: ' essais',
  unEssai: ' un essai',
  uneValid: ' une validation',
  essaisEt: ' essai(s) et ',
  validations: ' validation(s).',
  avert1: 'La figure doit contenir un calcul ou une fonction nommée rep.',
  avert2: 'La figure doit contenir un calcul ou une fonction nommée reponse' +
    '\n de valeur 1 si le contenu de rep est accepté comme répons finale' +
    '\n de valeur 2 si le contenu de rep est accepté comme réponse intermédiaire' +
    '\n et de valeur 0 sinon',
  enonceFinUnEssai: 'Appuyer sur la touche Entrée ou cliquer sur OK pour valider la réponse.'
}

const aide = `<ul>La figure doit contenir&nbsp;:
  <li>Un calcul ou une fonction nommé rep destiné à contenir la formule proposée par l’élève</li>
  <li>Un calcul nommé reponse de valeur :</li>
  1 si le contenu de rep est accepté comme résultat final
  <br>2 si le contenu de rep est considéré comme exact
  <br>et 0 sinon.
</ul>`

/**
 * function renvoyant les erreurs dans un tableau
 * @param {CListeObjets} list la liste sur laquelle on travaille
 * @return {*[]}
 */
function getTabErrors (list) {
  const errors = []
  if (!list.containsCalc('rep', true)) {
    errors.push(textes.avert1)
  }
  if (!list.containsCalc('reponse', true)) {
    errors.push(textes.avert2)
  }
  return errors
}

/**
 * Valide la figure (avant de la sauvegarder dans les paramètres)
 * S’il y a des errors dans ce que l’on retourne, ce sera affichés et bloquera la sauvegarde (l’éditeur reste ouvert),
 * s’il y a des warning ils sont affichés sans bloquer la sauvegarde
 * @param {MtgApp} mtgApp
 * @return {Promise<{warnings: string[], errors: string[]}>}
 */
async function validator (mtgApp) {
  const errors = getTabErrors(mtgApp.getList())
  return { errors, warnings: [] }
}

/**
 * Ajoute l’éditeur mathgraph dans container, la promesse sera résolue avec la figure
 * @param {HTMLElement} container
 * @param {string} fig
 * @return {Promise<string>}
 */
async function initEditor (container, fig) {
  const { default: editor } = await import('./mtgEditor.js')
  return editor(container, { fig, validator, aide })
}

/**
 * section excalcmathquill
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup (ev) {
    if (sq.marked) {
      demarqueEditeurPourErreur()
    }
    if (!sq.validationAuto && ev.keyCode === 13) {
      // const rep = j3pValeurde('expressioninputmq1')
      const rep = getMathliveValue('expressioninputmq1')
      if (!rep) return // Rajouté 4/2020 car sinon quand on passe à l’exercice suivant via la touche Entrée le champ est sur fond rouge
      const resul = traiteMathlive(rep)
      const chcalcul = resul.res
      const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
      if (valide) {
        validation(parcours)
      } else {
        marqueEditeurPourErreur()
        j3pElement('correction').style.color = parcours.styles.cfaux
        j3pAddContent('correction', textes.fauteSynt, { replace: true })
        // @todo expliquer pourquoi il faut ce setTimeout
        setTimeout(setEditorFocused, 0)
      }
    }
  }

  function setEditorFocused () {
    const mf = j3pElement('expressioninputmq1')
    mf.focus()
  }

  function setEditorFocusedAndEmpty () {
    const mf = j3pElement('expressioninputmq1')
    mf.value = '\\;' // Pour faciliter le focus sur l’éditeur
    mf.focus()
  }

  // Attention : Les }{ on été remplacés par des }/{
  function traiteIntegrales (st) {
    const faux = { valide: false, st }
    let ind
    // Traitement différent pour mathlive
    while ((ind = st.indexOf('\\int_')) !== -1) {
      const ind1 = getFirstIndexParExcluded(st, '^', ind + 5)
      if (ind1 === -1) return -1
      const a = st.substring(ind + 5, ind1)
      // Si le ^ est suivi d’une parenthèse on recherche la parenthèse fermante associée
      // Sinon c’est que la borne supérieure est formée d’un seul caractère
      let ind2, ind3, indsuiv
      if (st.charAt(ind1 + 1) === '(') {
        ind2 = ind1 + 2
        ind3 = getIndexFermant(st, ind1 + 1)
        indsuiv = ind3 + 1
      } else {
        ind2 = ind1 + 1
        ind3 = ind1 + 2
        indsuiv = ind3
      }
      if (ind3 === 0 || indsuiv >= st.length) return faux
      const b = st.substring(ind2, ind3)
      // Attention : les {}{} ont pu donner des ()/().
      if (st.charAt(indsuiv) === '/') indsuiv++
      // On recherche maintenant le d d’intégration en sautant les éventuelles parenthèses
      let ind4 = getFirstIndexParExcluded(st, '\\differentialD', indsuiv)
      if (ind4 === -1) return faux
      const fonc = st.substring(indsuiv, ind4)
      ind4 += 14
      let ind5
      if (st.charAt(ind4) === '(') {
        ind5 = getIndexFermant(st, ind4)
        ind4++
      } else {
        ind5 = ind4 + 1
      }
      if (ind5 === -1) return faux
      const varfor = st.substring(ind4, ind5) // Le caractère représentant la variable d’intégration
      st = st.substring(0, ind) + 'integrale(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind5)
    }
    return { valide: true, res: st }
  }

  /**
   * Fonction renvoyant un tableau formé des noms de variables utilisées par le calcul contenu dans ch
   * Si le tableau renvoyé a une longueur au moins égale à 2 on considérera que la primitive est incohérente
   * @param {string} ch
   * @return {[]}
   */
  function variablesUtilisees (ch) {
    function utiliseVariable (nomvar) {
      let res
      res = ch.search(new RegExp('\\(' + nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || ch.search(new RegExp(nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || (ch.lastIndexOf(nomvar) === ch.length - 1)
      res = res || ch === nomvar
      return res
    }

    const tab = []
    for (let i = 0; i < sq.variables.length; i++) {
      const car = sq.variables.charAt(i)
      if (utiliseVariable(car)) tab.push(car)
    }
    return tab
  }

  function traitePrimitives (st) {
    const faux = { valide: false, st }
    if (sq.variables === '') return { valide: true, res: st }
    let ind, tab
    while ((ind = st.indexOf('\\left[')) !== -1) {
      // On cherche le \right] correspondant
      const ind1 = getIndexFermant(st, ind + 5)
      if (ind1 === -1) return faux
      const fonc = st.substring(ind + 6, ind1 - 6)
      tab = variablesUtilisees(fonc)
      if (tab.length > 1) return faux
      const varfor = (tab.length === 0) ? sq.variables[0] : tab[0] // On traite le cas d’une fonction constante
      // Le crochet fermant doit être suivi d’un _
      if (st.charAt(ind1 + 1) !== '_') return faux
      const ind2 = getFirstIndexParExcluded(st, '^', ind1 + 2)
      if (ind2 === -1) return faux
      const a = st.substring(ind1 + 2, ind2)
      // Soit le caractère suivant n’est pas une parenthèse et c’est un seul caractère
      // qui est l’argument soit c’est le contenu de la parenthèse
      let ind3, ind4
      if (st.charAt(ind2 + 1) === '(') {
        ind3 = ind2 + 2
        ind4 = getIndexFermant(st, ind3)
      } else {
        ind3 = ind2 + 1
        ind4 = ind2 + 2
      }
      if (ind4 === -1 || ind4 > st.length) return faux
      const b = st.substring(ind3, ind4)
      st = st.substring(0, ind) + 'primitive(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind4 + 1)
    }
    return { valide: true, res: st }
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    let { res, valide } = traiteIntegrales(ch)
    if (valide) {
      const resul = traitePrimitives(res)
      res = resul.res
      valide = resul.valide
    }
    if (valide) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', res)// Traitement des multiplications implicites
    const contientIntegOuPrim = ch.includes('integrale') || ch.includes('primitive')
    return { valide, res: ch, contientIntegOuPrim }
  }

  /**
   * Fonction éliminant de la chaîne ch (représentant un calcul) les 1* et *1 inutiles
   * @param ch la chaîne à traiter
   * @returns {string} la chaîne sans les multiplications par 1 acceptées
   */
  function elimineMult1 (ch) {
    if (ch.search(/([^\d.)])1\*1\*/g) !== -1 || ch.search(/\*1\*1([^\d.(])/g) !== -1) return ch // On n'accepte pas les doubles multiplications par 1
    // On n'accepte pas les 1* suivis de deux lettres (nom de fonction)
    if (ch.search(/1\*[a-zA-Z]{2}/g) !== -1) return ch // Pas de 1* suivi de deux lettres
    if (ch.search(/\)\*1[^d.]/g) !== -1) return ch // Pas de )*1 suivi d'un autre caractère qu'un chiffre ou point décimal
    if (ch.search(/[^d.]1\*\(/g) !== -1) return ch // Pas de 1*( précédé d'autre chose qu'un chiffre ou un point décimal
    ch = ch.replace(/([^\d.)])1\*([^d])/g, '$1$2')
    ch = ch.replace(/\*1([^\d.(])/g, '$1')
    if (ch.startsWith('1*')) ch = ch.substring(2)
    if (ch.endsWith('*1')) ch = ch.substring(0, ch.length - 2)
    return ch
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex, sq.charset)
  }

  /**
   * Fonction affichant le nombre d’essais restants nbe et le nombre de chances restantes nbc
   */
  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    const nbc = Math.min(sq.nbchances - parcours.essaiCourant, nbe)
    if (sq.validationAuto) {
      if (nbe === 1) {
        afficheMathliveDans('info', 'texteinfo', textes.ilReste + textes.unEssai + '.', { style: { color: '#7F007F' } })
      } else {
        afficheMathliveDans('info', 'texteinfo', textes.ilReste + nbe + textes.essais, { style: { color: '#7F007F' } })
      }
    } else {
      if ((nbe === 1) && (nbc === 1)) {
        afficheMathliveDans('info', 'texteinfo', textes.ilReste + textes.uneValid + '.', { style: { color: '#7F007F' } })
      } else {
        afficheMathliveDans('info', 'texteinfo', textes.ilReste + nbe + textes.essaisEt + nbc + textes.validations + '.', { style: { color: '#7F007F' } })
      }
    }
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    const mathfield = j3pElement('expressioninputmq1')
    mathfield.virtualKeyboard.hide()
    if (sq.solution) {
      sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
      const styles = parcours.styles
      const div = j3pElement('divSolution')
      div.style.display = 'block'
      const ch = extraitDeLatexPourMathlive('solution')
      afficheMathliveDans(div, 'textesol', ch, { style: { color: bexact ? styles.cbien : styles.colorCorrection } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement('expressioninputmq1')
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement('expressioninputmq1')
    mf.style.backgroundColor = 'white'
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  // Cette fonction renvoie true si parcours.sectionCourante() a dû être appelé et false sinon
  function validation (parcours, continuer) {
    const mtg32App = sq.mtgAppLecteur
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    // sq.rep = j3pValeurde('expressioninputmq1')
    sq.rep = getMathliveValue('expressioninputmq1')
    sq.repMathQuill = traiteMathlive(sq.rep)
    // Ci dessous on ajoute un \displaystyle pour que la réponse soit affichée comme dans l’éditeur
    sq.rep = '\\displaystyle ' + sq.rep
    let chcalcul = sq.repMathQuill.res
    // Si on accepte les multiplications par 1 inutiles on les retire de la chaîne de calcul
    if (sq.acceptMult1) chcalcul = elimineMult1(chcalcul)
    mtg32App.giveFormula2('mtg32svg', 'rep', chcalcul)
    j3pEmpty('correction')
    mtg32App.calculate('mtg32svg', false)
    const faute = mtg32App.valueOf('mtg32svg', 'faute', true)
    if (sq.indicationfaute) {
      if (faute === 1) {
        mtg32App.executeMacro('mtg32svg', 'voirFaute') // Pour compatibilité anciennes versions
      } else {
        mtg32App.executeMacro('mtg32svg', 'masquerFaute') // Pour compatibilité anciennes versions
      }
    }
    sq.reponse = mtg32App.valueOf('mtg32svg', 'reponse', true)
    if ((sq.charExclusRepFinale !== '') && (sq.reponse === 1)) {
      // Si on a spécifié des carcatères interdits pour la réponse finale, au lieu
      // d'être considérée comme finale elle est considérée comme exacte
      // Principal intérêt : dans un exercice de calcul dont le résultat doit être une frcation
      // si le paramètre charExclusRepFinale contient a chaîne '*', la réponse 2*1/"
      // ne sera pas acceptée comme réponse finale alors que 2/3 le sera
      const reg = new RegExp('[' + sq.charExclusRepFinale + ']')
      if (reg.test(chcalcul)) sq.reponse = 2
    }
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    if (sq.reponse >= 1) {
      sq.parametres.style = { color: sq.reponse === 1 ? parcours.styles.cbien : parcours.styles.colorCorrection }
      afficheMathliveDans('formules', idrep, chdeb + '$' + sq.enTete + ' ' + sq.symbexact + ' ' + sq.rep + '$', sq.parametres)
    } else {
      const texteFaute = extraitDeLatexPourMathlive('faute')
      const chFaute = texteFaute !== '' ? '<br>' + texteFaute : ''
      sq.parametres.style = { color: parcours.styles.cfaux }
      afficheMathliveDans('formules', idrep, chdeb + '$' + sq.enTete + ' ' + sq.symbnonexact +
        ' ' + sq.rep + '$' + chFaute, sq.parametres)
    }
    // On vide le contenu de l’éditeur MathQuill
    // $('#expressioninputmq1').mathquill('latex', ' ')
    // const mf = document.getElementById('expressioninputmq1')
    const mf = j3pElement('expressioninputmq1')
    mf.value = ''
    // j3pFocus('expressioninputmq1')
    setTimeout(function () {
      setEditorFocusedAndEmpty()
      resetKeyboardPosition()
    }, 0)
    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        return true
      }
    } else {
      afficheNombreEssaisRestants()
    }
    return false
  } // validation

  function recopierReponse () {
    const mf = j3pElement('expressioninputmq1')
    mf.value = sq.rep
    mf.focus()
  }

  function afficheFiguresolution () {
    let ch, car, code
    const mtg32App = sq.mtgAppLecteur
    if (sq.solution && sq.figSol !== '') {
      $('#divExplications').css('display', 'block').html('')
      $('#mtg32svgsol').css('display', 'block')
      mtg32App.removeDoc('mtg32svgsol')
      mtg32App.addDoc('mtg32svgsol', sq.figSol)
      // On remplace dans la figure de solution les paramètres par les valeurs correspondant dans la figure de texte
      ch = 'abcdefghjklmnpqr'
      for (let j = 0; j < ch.length; j++) {
        car = ch.charAt(j)
        if (sq.param.indexOf(car) !== -1) {
          const par = mtg32App.valueOf('mtg32svg', car)
          mtg32App.giveFormula2('mtg32svgsol', car, par)
        }
      }
      mtg32App.calculate('mtg32svgsol', false)
      const param = {}
      // gestion de l’ancien param nbLatexSol
      for (let k = 0; k < sq.nbLatexSol; k++) {
        code = mtg32App.getLatexCode('mtg32svgsol', k)
        param[ch.charAt(k)] = code
      }
      afficheMathliveDans('divExplications', 'solution', sq.explicationSol, param)
      mtg32App.display('mtg32svgsol')
    }
  }

  function cacheFigureSolution () {
    if (sq.figSol !== '') {
      $('#divExplications').css('display', 'none').html('')
      $('#mtg32svgsol').css('display', 'none')
    }
  }

  function initMtg () {
    setMathliveCss('MepMG')
    setKeyboardContainer(parcours.zonesElts.MG)
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      mtgAppLecteur.removeAllDoc()
      mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
      mtgAppLecteur.calculate('mtg32svg', false)
      const ch = 'abcdefghjklmnpqr'
      if (sq.param !== undefined) {
        for (let i = 0; i < ch.length; i++) {
          const car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            const par = parcours.donneesSection[car]
            if (par !== 'random') mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      mtgAppLecteur.calculate('mtg32svg', true)
      const nbvar = mtgAppLecteur.valueOf('mtg32svg', 'nbvar', true)
      if (nbvar !== -1) {
        const nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (let i = 1; i <= nbvar; i++) {
          const nbcas = mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i, true)
          const nb = Math.max(nbrep, nbcas)
          const ar = []
          for (let j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            const tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }
      mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = sq.nblatex
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (let i = 0; i < nbParam; i++) {
        const code = mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) sq.aCalculer = code
        param[t[i]] = code
      }
      if (sq.mtgAppLecteur.getList('mtg32svg').getByTag('enonce', true)) {
        let ch = extraitDeLatexPourMathlive('enonce')
        if (sq.simplifier && sq.enoncesimplifier !== '') ch += '<br>' + sq.enoncesimplifier
        if (sq.enoncefin !== '') {
          if (sq.nbEssais === 1) ch += '<br>' + textes.enonceFinUnEssai
          else ch += '<br>' + sq.enoncefin
        }
        afficheMathliveDans('enonce', 'texte', ch)
      } else {
        afficheMathliveDans('enonce', 'texte', sq.enonce, param)
      }
      sq.parametres = param
      sq.enTete = extraitDeLatexPourMathlive('enTete')
      if (sq.enTete === '') {
        sq.enTete = (sq.nomCalcul === '') ? sq.aCalculer : sq.nomCalcul
        sq.enTete = sq.enTete.replace(/\$\$/g, ' ') // Nécessaire compatibilité avec certains exos
      }
      renderMathInDocument({ renderAccessibleContent: '' })
      sq.parametres.charset = sq.charset
      sq.parametres.listeBoutons = sq.listeBoutons
      afficheMathliveDans('editeur', 'expression', '$' + sq.enTete + '$ = &1&', sq.parametres)
      mathliveRestrict('expressioninputmq1', sq.charset)

      if (sq.listeBoutons.length !== 0) j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: sq.listeBoutons })
      sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
      $('#expressioninputmq1').keyup(onkeyup)
      setTimeout(setEditorFocusedAndEmpty, 0)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        sq.nbexp = 0
        sq.reponse = -1
        // Construction de la page
        this.construitStructurePage('presentation1bis')
        if (parcours.donneesSection.calculatrice) {
          j3pDiv('MepMD', 'emplacecalc', '')
          this.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
          j3pCreeFenetres(this)
          j3pAjouteBouton('emplacecalc', 'BCalculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
        }
        sq.solution = parcours.donneesSection.solution
        sq.validationAuto = parcours.donneesSection.validationAuto
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        if (!sq.validationAuto && (sq.nbchances > sq.nbEssais)) sq.nbchances = sq.nbEssais
        sq.fig = parcours.donneesSection.fig
        sq.width = parcours.donneesSection.width
        sq.height = parcours.donneesSection.height
        // if (sq.fig.indexOf('!') !== -1) sq.fig = sq.fig.replace(new RegExp('!', 'g'), '#') // Pour les tests en local pas de # dans la ligne de commande
        if (sq.fig.indexOf('!') !== -1) sq.fig = sq.fig.replace(/!/g, '#') // Pour les tests en local pas de # dans la ligne de commande
        sq.symbexact = '='
        sq.symbnonexact = '\\ne'

        sq.titre = parcours.donneesSection.titre
        sq.nomCalcul = parcours.donneesSection.nomCalcul
        // sq.bigSize = parcours.donneesSection.bigSize
        const bigSize = parcours.donneesSection.bigSize
        if (bigSize && !parcours.donneesSection.taillePoliceFormulaire) {
          sq.taillePoliceFormulaire = 30
          sq.taillePoliceReponses = 26
          sq.taillePoliceSolution = 26
        } else {
          let taillePoliceFormulaire = Number(parcours.donneesSection.taillePoliceFormulaire ?? 24)
          if (taillePoliceFormulaire < 10 || taillePoliceFormulaire > 50) taillePoliceFormulaire = 24
          sq.taillePoliceFormulaire = taillePoliceFormulaire
          let taillePoliceReponses = Number(parcours.donneesSection.taillePoliceReponses ?? 22)
          if (taillePoliceReponses < 10 || taillePoliceReponses > 50) taillePoliceReponses = 22
          sq.taillePoliceReponses = taillePoliceReponses
          let taillePoliceSolution = Number(parcours.donneesSection.taillePoliceSolution ?? 22)
          if (taillePoliceSolution < 10 || taillePoliceSolution > 50) taillePoliceSolution = 22
          sq.taillePoliceSolution = taillePoliceSolution
        }
        sq.nblatex = parcours.donneesSection.nblatex // Le nombre de paramètres LaTeX dans le texte
        if (!Number.isInteger(sq.nblatex) || sq.nblatex < 1 || sq.nblatex > 4) {
          console.warn(`Paramètre nblatex invalide, doit être un entier entre 1 et 4, ${sq.nblatex} fourni`)
          sq.nblatex = 1
        }
        sq.simplifier = parcours.donneesSection.simplifier
        sq.acceptMult1 = Boolean(parcours.donneesSection.acceptMult1)
        sq.enoncesimplifier = parcours.donneesSection.enoncesimplifier
        sq.enoncefin = sq.validationAuto ? parcours.donneesSection.enoncefinPourValidationAuto : parcours.donneesSection.enoncefin

        sq.param = parcours.donneesSection.param // Chaine contenant les paramètres autorisés
        const tabBtn = ['Puis', 'Frac', 'Pi', 'Cos', 'Sin', 'Tan', 'Rac', 'Exp', 'Ln', 'Log', 'Abs', 'Conj', 'Integ', 'Prim']
        tabBtn.forEach((p) => {
          sq['btn' + p] = parcours.donneesSection['btn' + p]
        })
        sq.charExclusRepFinale = parcours.donneesSection.charExclusRepFinale
        sq.variables = parcours.donneesSection.variables

        sq.enonce = ''
        for (let i = 1; i < 9; i++) {
          if (parcours.donneesSection['enonceligne' + i] !== '') {
            if (i !== 1) sq.enonce += '<br>'
            sq.enonce += parcours.donneesSection['enonceligne' + i]
          }
        }
        if (sq.simplifier && sq.enoncesimplifier !== '') sq.enonce += '<br>' + sq.enoncesimplifier
        sq.figSol = parcours.donneesSection.figSol
        if (sq.fig === '') {
          sq.fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAUeAAACygAAAQEAAAAAAAAAAQAAABf#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAVuYnZhcgABMQAAAAE#8AAAAAAAAAAAAAIA#####wAGbmJjYXMxAAE3AAAAAUAcAAAAAAAAAAAAAgD#####AAJyMQATaW50KHJhbmQoMCkqbmJjYXMxKf####8AAAACAAlDRm9uY3Rpb24C#####wAAAAEACkNPcGVyYXRpb24CAAAAAxEAAAABAAAAAAAAAAA#6+0Qnpof+P####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAACAAAAAgD#####AAFuAARyMSsyAAAABAAAAAAFAAAAAwAAAAFAAAAAAAAAAP####8AAAABAAVDRm9uYwD#####AAJlcQAHYV5uK2FebgAAAAQA#####wAAAAEACkNQdWlzc2FuY2X#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAABQAAAAQAAAAHAAAACAAAAAAAAAAFAAAABAABYf####8AAAACAAZDTGF0ZXgA#####wAAAAABAAZlbm9uY2X#####EEB9cAAAAAAAQE7rhR64UewAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAE5cdGV4dHtJbCBmYXV0IMOpY3JpcmUgJEE9XEZvclNpbXB7ZXF9JCBzb3VzIGxhIGZvcm1lIGxhIHBsdXMgc2ltcGxlIHBvc3NpYmxlLn0AAAAJAP####8AAAAAAQAGZW5UZXRl#####xBAfdAAAAAAAEA7mZmZmZmaAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAOQT1cRm9yU2ltcHtlcX0AAAAGAP####8AA3JlcAABMAAAAAEAAAAAAAAAAAABYQAAAAYA#####wADc29sAAUyKmFebgAAAAQCAAAAAUAAAAAAAAAAAAAABwAAAAgAAAAAAAAABQAAAAQAAWH#####AAAAAwAQQ1Rlc3RFcXVpdmFsZW5jZQD#####AAZyZXNvbHUAAAAJAAAACAEAAAAAAT#wAAAAAAAAAQAAAAYA#####wAEemVybwASYWJzKHgpPDAuMDAwMDAwMDAxAAAABAQAAAADAAAAAAgAAAAAAAAAAT4RLgvoJtaVAAF4AAAAAgD#####AAJuMgADMipuAAAABAIAAAABQAAAAAAAAAAAAAAFAAAABAAAAAYA#####wAGZmF1dGUxAARhXm4yAAAABwAAAAgAAAAAAAAABQAAAAwAAWEAAAAKAP####8ABWZhdXRlAAAADQAAAAgBAAAAAAE#8AAAAAAAAAEAAAAJAP####8A#wAAAQAFZmF1dGX#####FEAaAAAAAAAAQG20euFHrhQAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAHBcSWZ7ZmF1dGV9CnsKXHRleHR7QXR0ZW50aW9uIDogTmUgcGFzIGNvbmZvbmRyZSAkYV5cVmFse259ICsgIGFeXFZhbHtufSQgYXZlYyAkYV5cVmFse259IFx0aW1lcyBhXlxWYWx7bn0kfQp9Cnt9AAAABgD#####AAJtMQADYV5uAAAABwAAAAgAAAAAAAAABQAAAAQAAWEAAAAJAP####8AAAD#AQAIc29sdXRpb27#####FEAjAAAAAAAAQELR64UeuFIBAe#v+wAAAAAAAAAAAAAAAQAAAAAAAAAAAL1cYmVnaW57YXJyYXl9e2x9CntcdGV4dHtPbiB1dGlsaXNlIGxhIHLDqGdsZSBkZSBjYWxjdWwgOiAgJHgreD0yeCQgYXZlYyAkeD1cRm9yU2ltcHttMX0kfX0KXFwgXHRleHR7JEEgPSBcRm9yU2ltcHtzb2x9JH0KXFwgXHRleHR7JEE9XEZvclNpbXB7c29sfSQgw6l0YWl0IGxhIHLDqXBvbnNlIGF0dGVuZHVlLn0KXGVuZHthcnJheX0AAAACAP####8AAmExAAkxK3JhbmQoMCkAAAAEAAAAAAE#8AAAAAAAAAAAAAMRAAAAAQAAAAAAAAAAP+XRKDbaxioAAAACAP####8AAmEyAAsxKzIqcmFuZCgwKQAAAAQAAAAAAT#wAAAAAAAAAAAABAIAAAABQAAAAAAAAAAAAAADEQAAAAEAAAAAAAAAAD#CTw2wMXFgAAAAAgD#####AAJhMwALMSszKnJhbmQoMCkAAAAEAAAAAAE#8AAAAAAAAAAAAAQCAAAAAUAIAAAAAAAAAAAAAxEAAAABAAAAAAAAAAA#xz2dVUMIwAAAAAIA#####wAFZXhhY3QAQXplcm8ocmVwKGExKS1zb2woYTEpKSZ6ZXJvKHJlcChhMiktc29sKGEyKSkmemVybyhyZXAoYTMpLXNvbChhMykpAAAABAoAAAAECv####8AAAABAA5DQXBwZWxGb25jdGlvbgAAAAsAAAAEAQAAAAsAAAAIAAAABQAAABIAAAALAAAACQAAAAUAAAASAAAACwAAAAsAAAAEAQAAAAsAAAAIAAAABQAAABMAAAALAAAACQAAAAUAAAATAAAACwAAAAsAAAAEAQAAAAsAAAAIAAAABQAAABQAAAALAAAACQAAAAUAAAAUAAAAAgD#####AAdyZXBvbnNlABpzaShyZXNvbHUsMSxzaShleGFjdCwyLDApKf####8AAAABAA1DRm9uY3Rpb24zVmFyAAAAAAUAAAAKAAAAAT#wAAAAAAAAAAAADAAAAAAFAAAAFQAAAAFAAAAAAAAAAAAAAAEAAAAAAAAAAP###############w=='
          sq.charset = 'a()\\d.+-/*²^'
          sq.width = 0
          sq.height = 0
          sq.validationAuto = true
          sq.titre = 'Ressource à personnaliser'
          j3pShowError('Cette ressource doit être personnalisée en éditant la ressource dans l’éditeur de graphes.')
        } else {
          sq.charset = parcours.donneesSection.charset
        }
        if (sq.enoncefin) {
          sq.enonce += '<br>' + (sq.nbEssais === 1 ? textes.enonceFinUnEssai : sq.enoncefin)
        }
        // paramètre qui peut encore exister dans de vieux graphes
        sq.nbLatexSol = parcours.donneesSection.nbLatexSol
        if (!Number.isInteger(sq.nbLatexSol)) sq.nbLatexSol = 0
        sq.widthSol = parcours.donneesSection.widthSol
        sq.heightSol = parcours.donneesSection.heightSol
        sq.explicationSol = parcours.donneesSection.explicationSol
        // Si TRUE alors la touche ENTREE appelle la correction
        // Ne fonctionne QUE SI la scène dispose d’une zone de saisie
        this.validOnEnter = sq.validationAuto
        if (sq.titre) parcours.afficheTitre(sq.titre)

        // j3pDiv(parcours.zones.MG,"conteneur","",[5,5],parcours.styles.toutpetit.enonce);
        j3pDiv(parcours.zones.MG, {
          id: 'conteneur',
          contenu: '',
          style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
        })
        j3pDiv('conteneur', 'enonce', '')
        const formules = j3pAddElt('conteneur', 'div', {
          id: 'formules',
          style: parcours.styles.petit.enonce
        }) // Contient les formules entrées par l’élève
        formules.style.fontSize = sq.taillePoliceReponses + 'px'
        if (sq.bigSize) $('#formules').css('font-size', '26px')
        j3pElement('formules').style.paddingBottom = '8px'
        j3pDiv('conteneur', 'info', '')
        afficheNombreEssaisRestants()

        j3pDiv('conteneur', 'conteneurbouton', '')
        j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente', recopierReponse)
        j3pElement('boutonrecopier').style.display = 'none'
        const editeur = j3pAddElt('conteneur', 'div', {
          id: 'editeur',
          style: { paddingTop: '10px' }
        }) // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill

        // if (sq.bigSize) $('#editeur').css('font-size', '30px')
        editeur.style.fontSize = sq.taillePoliceFormulaire + 'px'
        $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
        j3pDiv('conteneur', 'boutonsmathquill', '')
        $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
        // On crée la liste des boutons disponibles
        // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
        sq.listeBoutons = []
        // Tous les boutons sont à false par défaut
        const tabBoutons1 = ['puissance', 'fraction', 'pi', 'racine', 'exp', 'ln', 'log', 'sin', 'cos', 'tan', 'abs', 'conj', 'integ', 'prim', 'differential']
        ;['btnPuis', 'btnFrac', 'btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnLog', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj', 'btnInteg', 'btnPrim', 'btnDiff'].forEach((p, i) => {
          if (sq[p]) sq.listeBoutons.push(tabBoutons1[i])
        })
        sq.boutonsMathQuill = sq.listeBoutons.length !== 0
        const divsol = j3pAddElt('conteneur', 'div', { id: 'divSolution', style: { display: 'none' } })
        divsol.style.fontSize = sq.taillePoliceSolution + 'px'
        $(divsol).css('display', 'none')
        j3pDiv('conteneur', 'divmtg32', '')
        j3pCreeSVG('divmtg32', {
          id: 'mtg32svg',
          width: sq.width,
          height: sq.height
        })
        if (sq.figSol !== '') {
          j3pDiv('conteneur', 'divExplications', '')
          $('#divExplications').css('display', 'none')
          j3pDiv('conteneur', 'divFigSol', '')
          j3pCreeSVG('divFigSol', {
            id: 'mtg32svgsol',
            width: sq.widthSol,
            height: sq.heightSol
          })
          $('#mtg32svgsol').css('display', 'none')
        }
        const style = parcours.styles.petit.correction
        style.marginTop = '1.5em'
        style.marginLeft = '0.5em'
        j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
        initMtg()
      } else {
        j3pEmpty('correction')
        cacheSolution()
        cacheFigureSolution()
        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')
        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        setTimeout(setEditorFocusedAndEmpty, 0)
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }
        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réitialisés

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', false) // true pour que les calculs alèatoires soient réitialisés
        j3pEmpty('enonce')
        j3pEmpty('editeur')
        j3pEmpty('formules')
        j3pEmpty('info')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = sq.nblatex
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) sq.aCalculer = code
          param[t[i]] = code
        }
        if (sq.mtgAppLecteur.getList('mtg32svg').getByTag('enonce', true)) {
          let ch = extraitDeLatexPourMathlive('enonce')
          if (sq.simplifier && sq.enoncesimplifier !== '') ch += '<br>' + sq.enoncesimplifier
          if (sq.enoncefin !== '') {
            if (sq.nbEssais === 1) ch += '<br>' + textes.enonceFinUnEssai
            else ch += '<br>' + sq.enoncefin
          }
          afficheMathliveDans('enonce', 'texte', ch)
        } else {
          afficheMathliveDans('enonce', 'texte', sq.enonce, param)
        }
        sq.parametres = param
        sq.enTete = extraitDeLatexPourMathlive('enTete')
        if (sq.enTete === '') {
          sq.enTete = (sq.nomCalcul === '') ? sq.aCalculer : sq.nomCalcul
          sq.enTete = sq.enTete.replace(/\$\$/g, ' ') // Nécessaire compatibilité avec certains exos
        }
        sq.parametres.charset = sq.charset
        sq.parametres.listeBoutons = sq.listeBoutons
        afficheMathliveDans('editeur', 'expression', '$' + sq.enTete + '$ = &1&', sq.parametres)
        mathliveRestrict('expressioninputmq1', sq.charset)
        if (sq.listeBoutons.length !== 0) j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: sq.listeBoutons })
        $('#expressioninputmq1').keyup(onkeyup)
        setTimeout(setEditorFocusedAndEmpty, 0)
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        this.finEnonce()
      }
      break
    } // case enonce

    case 'correction': {
      let bilanreponse = ''
      let simplifieri = false
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      // const ch = j3pValeurde('expressioninputmq1')
      const ch = getMathliveValue('expressioninputmq1')
      if (ch !== '') {
        // var repcalcul = traiteMathQuill(ch)
        // var valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, true)
        const resul = traiteMathlive(ch)
        const repcalcul = resul.res
        const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, true)
        if (valide) {
          if (validation(this, false)) {
            // la fct validation peut rappeller sectionCourante() sans appeler finCorrection
            // dans ce cas elle retourne true, et on arrête là.
            // Il faudrait appeler finCorrection() mais ça va afficher le bouton ok,
            // pas sûr que ce soit souhaitable dans tous les cas, on garde le comportement précédent…
            return
          }
        } else {
          bilanreponse = 'incorrect'
        }
      } else {
        if ((!this.isElapsed) && sq.nbexp === 0) return
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (sq.simplifier) { // Si on demande le calcul simplifié
          if (res1 === 1) {
            bilanreponse = 'exact'
            simplifieri = true
          } else {
            if (res1 === 2) {
              bilanreponse = 'exactpasfini'
            } else {
              bilanreponse = 'erreur'
            }
          }
        } else {
          if ((res1 === 1) || (res1 === 2)) {
            if ((res1 === 2) && sq.repMathQuill.contientIntegOuPrim) {
              bilanreponse = 'exactpasfini'
            } else {
              bilanreponse = 'exact'
            }
            if (res1 === 1) {
              simplifieri = true
            }
          } else {
            bilanreponse = 'erreur'
          }
        }
      }

      if (this.isElapsed) {
        // A cause de la limite de temps :
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        afficheSolution(false)
        afficheFiguresolution()
        j3pAddContent('correction', tempsDepasse, { replace: true })
        if (sq.solution) j3pAddContent('correction', textes.solutionIci)
        else j3pAddContent('correction', textes.solDesact)

        return this.finCorrection('navigation', true)
      }

      if (bilanreponse === 'incorrect') {
        j3pElement('correction').style.color = this.styles.cfaux
        j3pAddContent('correction', textes.Incorrect, { replace: true })
        marqueEditeurPourErreur()
        setTimeout(setEditorFocused, 0)
        return this.finCorrection()
      }

      // Une réponse a été saisie
      if (bilanreponse === 'exact') {
        // Bonne réponse
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        if (simplifieri) {
          j3pAddContent('correction', cBien, { replace: true })
        } else {
          j3pAddContent('correction', textes.cBienMais, { replace: true })
        }
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        afficheSolution(true)
        afficheFiguresolution()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        return this.finCorrection('navigation', true)
      }

      j3pElement('correction').style.color = this.styles.cfaux
      if (bilanreponse === 'exactpasfini') {
        j3pAddContent('correction', textes.bonPasFini, { replace: true })
      } else {
        j3pAddContent('correction', cFaux, { replace: true })
      }
      afficheNombreEssaisRestants()
      // On donne le focus à l’éditeur MathQuill
      setTimeout(setEditorFocusedAndEmpty, 0)

      if ((sq.nbexp < sq.nbEssais) && (sq.validationAuto || (this.essaiCourant < sq.nbchances))) {
        j3pAddContent('correction', '\n' + '<br>' + essaieEncore)
        // il reste des essais, on reste en correction
        return this.finCorrection()
      }

      // Erreur au dernier essai
      j3pElement('boutonrecopier').style.display = 'none'
      j3pElement('info').style.display = 'none'
      if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      afficheSolution(false)
      afficheFiguresolution()
      $('#editeur').css('display', 'none')
      $('#boutonsmathquill').css('display', 'none')
      if (sq.solution) j3pAddContent('correction', textes.solutionIci)
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
