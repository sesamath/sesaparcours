import { j3pAddElt, j3pEmpty } from 'src/legacy/core/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'

/**
 * @callback mtgValidatorCallback
 * @param {MtgApp} L’éditeur mathgraph qui contient la figure à valider
 * @returns {ValidationResult}
 */

/**
 * @typedef ValidationResult
 * @type Object
 * @property {string[]} errors Si non vide l’enregistrement sera bloqué
 * @property {string[]} warnings Si non vide, les warnings seront affichés et l’enregistrement fait après validation par l’utilisateur
 */

/**
 * Ajoute un éditeur mathgraph dans container, avec fig si fourni
 * @param {HTMLElement} container
 * @param {Object} [options]
 * @param {string} [options.fig] La figure à éditer (string vide par défaut)
 * @param {function} [options.validator] Un validateur à appeler avant la sauvegarde, qui devra retourner un objet {}
 * @param aide
 * @return {Promise<unknown>}
 */
export default function mtgEditor (container, { fig = '', validator, aide } = {}) {
  return new Promise((resolve, reject) => {
    // on ajoute un div pour centrer les boutons (cf https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox)
    const div = j3pAddElt(container, 'div', '', { style: { margin: '1rem', display: 'flex', justifyContent: 'space-evenly' } })
    const divWarnings = j3pAddElt(container, 'div')
    const divErrors = j3pAddElt(container, 'div')
    const cancelBtn = j3pAddElt(div, 'button', 'Annuler', { style: { flex: '0 1 auto' } })
    cancelBtn.addEventListener('click', () => resolve(fig))
    const divEditor = j3pAddElt(container, 'div')
    const mtgAppOptions = {
      save: false,
      fig
    }
    getMtgApp(divEditor, {}, mtgAppOptions).then(mtgApp => {
      if (aide) {
        // on fait en deux temps pour que j3pAddElt ne râle pas si y’a du html dedans, ici on s’en fout de détruire des nodeList y’en a pas…
        const p = j3pAddElt(container, 'p', '', { className: 'info' })
        p.innerHTML = aide
        // faut le remettre juste avant les boutons
        container.insertBefore(p, div)
      }
      const saveBtn = j3pAddElt(div, 'button', 'Sauvegarder', { style: { flex: '0 1 auto' } })
      saveBtn.addEventListener('click', async () => {
        if (validator) {
          j3pEmpty(divWarnings)
          j3pEmpty(divErrors)
          const { errors, warnings } = await validator(mtgApp)
          if (warnings?.length) {
            for (const warning of warnings) j3pAddElt(divWarnings, 'p', warning, { className: 'warning' })
          }
          if (errors?.length) {
            for (const error of errors) j3pAddElt(divErrors, 'p', error, { className: 'error' })
          } else {
            if (warnings?.length) {
              // si y’a des warnings faut un bouton Vu, pour lui laisser le temps de le(s) lire
              const message = (warnings.length > 1 ? 'J’ai lu ces avertissements' : 'J’ai lu cet avertissement') + ', enregistrer en l’état'
              const btnVu = j3pAddElt(divWarnings, 'button', message, { style: { margin: '1rem auto', display: 'block' } })
              btnVu.style.width = 'fit-content' // j3pAddElt en veut pas (il veut du numérique)
              btnVu.addEventListener('click', () => resolve(mtgApp.getBase64Code()))
            } else {
              // pas de warnings
              resolve(mtgApp.getBase64Code())
            }
          }
        } else {
          // pas de validator
          resolve(mtgApp.getBase64Code())
        }
      })
      // et on attend que l’utilisateur clique sur un des deux boutons
    }).catch(reject)
  })
}
