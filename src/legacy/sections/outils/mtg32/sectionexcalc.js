import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Novembre 2017

    http://localhost:8081/?graphe=[1,"squelettemtg32_Calc_Param",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    sectionexcalc
    Section destinée à faire faire un exercice de calcul basé sur une figure MathGraph32
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,l,m,n,p,q,r

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['titre', '', 'string', 'Titre de l’activité'],
    ['fig', '', 'editor', 'Chaîne Base 64 contenant la figure de l’exercice', initEditor],
    ['param', 'abcd', 'string', 'Chaîne formé des paramètres que l’exercice utilise (de a à r, voir ci-dessous'],
    ['nomCalcul', 'A', 'string', 'Nom de l’expression à calculer'],
    ['nbEssais', 3, 'entier', 'Nombre d’essais maximum autorisé'],
    ['nbchances', 2, 'entier', 'Nombre d’essais autorisés à l’élève'],
    ['nblatex', 1, 'entier', 'Nombre d’affichages LaTeX de la figure utilisés dans la consigne (1 au moins et jusque 4)'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['charset', '', 'string', 'Chaîne formée des caractères permis dans l’éditeur, chaîne vide pour autoriser tous les caractères'],
    ['enonceligne1', '', 'string', 'Première ligne de la consigne'],
    ['enonceligne2', '', 'string', 'ligne 2 de la consigne (peut être une chaîne vide)'],
    ['enonceligne3', '', 'string', 'ligne 3 de la consigne (peut être une chaîne vide)'],
    ['enonceligne4', '', 'string', 'ligne 4 de la consigne (peut être une chaîne vide)'],
    ['enonceligne5', '', 'string', 'ligne 5 de la consigne (peut être une chaîne vide)'],
    ['enonceligne6', '', 'string', 'ligne 6 de la consigne (peut être une chaîne vide)'],
    ['enonceligne7', '', 'string', 'ligne 7 de la consigne (peut être une chaîne vide)'],
    ['enonceligne8', '', 'string', 'ligne 8 de la consigne (peut être une chaîne vide)'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r']
  ]
}

// cf src/legacy/sections/outils/mtg32/sectionex_Calc_Multi_Edit_Multi_Etapes.js pour ajouter un validator
/**
 * Ajoute l’éditeur mathgraph dans container, la promesse sera résolue avec la figure
 * @param {HTMLElement} container
 * @param {string} fig
 * @return {Promise<string>}
 */
async function initEditor (container, fig) {
  const { default: editor } = await import('./mtgEditor.js')
  return editor(container, { fig })
}

/**
 * section excalc
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function validation (parcours, continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = sq.mtgAppLecteur.getFieldValue('mtg32svg', 'rep')
    j3pElement('correction').innerHTML = ''
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }
    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    if (sq.reponse >= 1) {
      j3pAffiche('formules', idrep, '\n' + '$' + sq.nomCalcul + ' ' + sq.symbexact +
        ' ' + sq.mtgAppLecteur.getLatexFormula('mtg32svg', 'rep') + '$', {
        style: {
          color: '#0000FF'
        }
      })
    } else {
      j3pAffiche('formules', idrep, '\n' + '$' + sq.nomCalcul + ' ' + sq.symbnonexact +
        ' ' + sq.mtgAppLecteur.getLatexFormula('mtg32svg', 'rep') + '$', {
        style: {
          color: '#FF0000'
        }
      })
    }
    sq.mtgAppLecteur.setEditorValue('mtg32svg', 'rep', '')
    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
      }
    } else {
      j3pElement('info').innerHTML = ''
      const nbe = sq.nbEssais - sq.nbexp
      const nbc = sq.nbchances - sq.numEssai + 1
      j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', {
        style: {
          color: '#7F007F'
        }
      })
    }
  }
  sq.recopierReponse = function () {
    sq.mtgAppLecteur.setEditorValue('mtg32svg', 'rep', sq.rep)
  }

  function initMtg () {
    let code, par, car, i, j, k, nbrep, ar, tir, nbcas, nb
    sq.mtgAppLecteur.removeAllDoc()
    sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
    sq.mtgAppLecteur.calculate('mtg32svg', false)

    sq.nbexp = 0
    sq.reponse = -1
    sq.mtgAppLecteur.setEditorCallBackOK('mtg32svg', 'rep', function () {
      validation(parcours)
    })
    sq.mtgAppLecteur.setEditorCharset('mtg32svg', 'rep', sq.charset)
    sq.mtgAppLecteur.setEditorsSize('mtg32svg', '17px')
    const ch = 'abcdefghjklmnpqr'
    const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
    if (nbvar !== -1) {
      nbrep = parcours.donneesSection.nbrepetitions
      sq.aleat = true
      sq.nbParamAleat = nbvar
      for (i = 1; i <= nbvar; i++) {
        nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
        nb = Math.max(nbrep, nbcas)
        ar = []
        for (j = 0; j < nb; j++) ar.push(j % nbcas)
        sq['tab' + i] = []
        for (k = 0; k < nbrep; k++) {
          tir = Math.floor(Math.random() * ar.length)
          sq['tab' + i].push(ar[tir])
          ar.splice(tir, 1)
        }
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
      }
    } else {
      sq.aleat = false
    }

    if (sq.param !== undefined) {
      for (i = 0; i < ch.length; i++) {
        car = ch.charAt(i)
        if (sq.param.indexOf(car) !== -1) {
          par = parcours.donneesSection[car]
          if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
        }
      }
    }

    sq.mtgAppLecteur.calculateAndDisplayAll(true)
    const nbParam = parseInt(sq.nblatex)
    const t = ['a', 'b', 'c', 'd']
    const param = {}
    for (i = 0; i < nbParam; i++) {
      code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
      param[t[i]] = code
    }
    param.e = String(sq.nbEssais)
    j3pAffiche('enonce', 'texte', sq.enonce, param)
    sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée

    sq.mtgAppLecteur.activateField('mtg32svg', 'rep')
    parcours.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        this.validOnEnter = false // ex donneesSection.touche_entree
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        if (sq.nbchances > sq.nbEssais) sq.nbchances = sq.nbEssais
        sq.fig = parcours.donneesSection.fig
        if (sq.fig.indexOf('!') !== -1) sq.fig = sq.fig.replace(/!/g, '#') // Pour les tests en local pas de # dans la ligne de commande
        sq.symbexact = '='
        sq.symbnonexact = '\\ne'
        if (sq.fig === '') {
          sq.titre = 'Calcul sur les puissances. Exemple'
          sq.fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH2+v4BAP8BAAAAAAAAAAAClAAAAZQAAAAAAAAAAAAAAAEAAAAt#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEAB0NDYWxjdWwA#####wAFbmJ2YXIAATIAAAABQAAAAAAAAAAAAAACAP####8ABm5iY2FzMQABNwAAAAFAHAAAAAAAAAAAAAIA#####wACcjEAE2ludChyYW5kKDApKm5iY2FzMSn#####AAAAAgAJQ0ZvbmN0aW9uAv####8AAAABAApDT3BlcmF0aW9uAgAAAAMRAAAAAQAAAAAAAAAAP+#g9BNJpsf#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAAgAAAAIA#####wAGbmJjYXMyAAE3AAAAAUAcAAAAAAAAAAAAAgD#####AAJyMgATaW50KHJhbmQoMCkqbmJjYXMyKQAAAAMCAAAABAIAAAADEQAAAAEAAAAAAAAAAD#nKnw7RWhuAAAABQAAAAT#####AAAAAQAJQ0ZvbmNOVmFyAP####8AA3JlcAAEYV4xNv####8AAAABAApDUHVpc3NhbmNl#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAFAMAAAAAAAAAAAAAIAAWEAAWIAAAACAP####8AAngxAAUxLjAzNwAAAAE#8JeNT987ZAAAAAIA#####wACeDIABjEuMDUzOQAAAAE#8NzGPxQSBgAAAAIA#####wACeDMACDEuMDEyNjQ3AAAAAT#wM81XNke7#####wAAAAEABUNGb25jAP####8ABHplcm8AEmFicyh4KTwwLjAwMDAwMDAwMQAAAAQEAAAAAwAAAAAIAAAAAAAAAAE+ES4L6CbWlQABeAAAAAIA#####wABcAAEcjErMQAAAAQAAAAABQAAAAMAAAABP#AAAAAAAAAAAAAGAP####8ABmZhdXRlMQADYV5wAAAABwAAAAgAAAAAAAAABQAAAAsAAAACAAFhAAFiAAAAAgD#####AAl0ZXN0ZmF1dDEAVnplcm8ocmVwKHgxLDApLWZhdXRlMSh4MSwwKSkqemVybyhyZXAoeDIsMCktZmF1dGUxKHgyLDApKSp6ZXJvKHJlcCh4MywwKS1mYXV0ZTEoeDMsMCkpAAAABAIAAAAEAv####8AAAABAA5DQXBwZWxGb25jdGlvbgAAAAoAAAAEAf####8AAAABABJDQXBwZWxGb25jdGlvbk5WYXIAAAACAAAABgAAAAUAAAAHAAAAAQAAAAAAAAAAAAAACwAAAAIAAAAMAAAABQAAAAcAAAABAAAAAAAAAAAAAAAKAAAACgAAAAQBAAAACwAAAAIAAAAGAAAABQAAAAgAAAABAAAAAAAAAAAAAAALAAAAAgAAAAwAAAAFAAAACAAAAAEAAAAAAAAAAAAAAAoAAAAKAAAABAEAAAALAAAAAgAAAAYAAAAFAAAACQAAAAEAAAAAAAAAAAAAAAsAAAACAAAADAAAAAUAAAAJAAAAAQAAAAAAAAAAAAAAAgD#####AAFuAARyMisxAAAABAAAAAAFAAAABQAAAAE#8AAAAAAAAAAAAAYA#####wAGZmF1dGUyAANhXm4AAAAHAAAACAAAAAAAAAAFAAAADgAAAAIAAWEAAWIAAAACAP####8ACXRlc3RmYXV0MgBWemVybyhyZXAoeDEsMCktZmF1dGUyKHgxLDApKSp6ZXJvKHJlcCh4MiwwKS1mYXV0ZTIoeDIsMCkpKnplcm8ocmVwKHgzLDApLWZhdXRlMih4MywwKSkAAAAEAgAAAAQCAAAACgAAAAoAAAAEAQAAAAsAAAACAAAABgAAAAUAAAAHAAAAAQAAAAAAAAAAAAAACwAAAAIAAAAPAAAABQAAAAcAAAABAAAAAAAAAAAAAAAKAAAACgAAAAQBAAAACwAAAAIAAAAGAAAABQAAAAgAAAABAAAAAAAAAAAAAAALAAAAAgAAAA8AAAAFAAAACAAAAAEAAAAAAAAAAAAAAAoAAAAKAAAABAEAAAALAAAAAgAAAAYAAAAFAAAACQAAAAEAAAAAAAAAAAAAAAsAAAACAAAADwAAAAUAAAAJAAAAAQAAAAAAAAAAAAAAAgD#####AAJucAADbipwAAAABAIAAAAFAAAADgAAAAUAAAALAAAABgD#####AAZmYXV0ZTMABGFebnAAAAAHAAAACAAAAAAAAAAFAAAAEQAAAAIAAWEAAWIAAAACAP####8ACXRlc3RmYXV0MwBWemVybyhyZXAoeDEsMCktZmF1dGUzKHgxLDApKSp6ZXJvKHJlcCh4MiwwKS1mYXV0ZTMoeDIsMCkpKnplcm8ocmVwKHgzLDApLWZhdXRlMyh4MywwKSkAAAAEAgAAAAQCAAAACgAAAAoAAAAEAQAAAAsAAAACAAAABgAAAAUAAAAHAAAAAQAAAAAAAAAAAAAACwAAAAIAAAASAAAABQAAAAcAAAABAAAAAAAAAAAAAAAKAAAACgAAAAQBAAAACwAAAAIAAAAGAAAABQAAAAgAAAABAAAAAAAAAAAAAAALAAAAAgAAABIAAAAFAAAACAAAAAEAAAAAAAAAAAAAAAoAAAAKAAAABAEAAAALAAAAAgAAAAYAAAAFAAAACQAAAAEAAAAAAAAAAAAAAAsAAAACAAAAEgAAAAUAAAAJAAAAAQAAAAAAAAAAAAAACQD#####AANmb3IADChhXm4pXnA9YV5ucAAAAAQIAAAABwAAAAcAAAAIAAAAAAAAAAUAAAAOAAAABQAAAAsAAAAHAAAACAAAAAAAAAAFAAAAEQABYQAAAAIA#####wAGbmVnYWxwAANuPXAAAAAECAAAAAUAAAAOAAAABQAAAAsAAAACAP####8ACmNvbmZ1c2lvbjEANXNpKG49MSxzaSh0ZXN0ZmF1dDEsMSwwKSxzaShwPTEsc2kodGVzdGZhdXQyLDEsMCksMCkp#####wAAAAEADUNGb25jdGlvbjNWYXIAAAAABAgAAAAFAAAADgAAAAE#8AAAAAAAAAAAAAwAAAAABQAAAA0AAAABP#AAAAAAAAAAAAABAAAAAAAAAAAAAAAMAAAAAAQIAAAABQAAAAsAAAABP#AAAAAAAAAAAAAMAAAAAAUAAAAQAAAAAT#wAAAAAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAgD#####AApjb25mdXNpb24yAB8xLWNvbmZ1c2lvbjEmdGVzdGZhdXQzJm4qcDw+bitwAAAABAoAAAAECgAAAAQBAAAAAT#wAAAAAAAAAAAABQAAABYAAAAFAAAAEwAAAAQJAAAABAIAAAAFAAAADgAAAAUAAAALAAAABAAAAAAFAAAADgAAAAUAAAALAAAABgD#####AAJmMQAHYV4obitwKQAAAAcAAAAIAAAAAAAAAAQAAAAABQAAAA4AAAAFAAAACwAAAAIAAWEAAWIAAAACAP####8ABm5lZ2FsMQADbj0xAAAABAgAAAAFAAAADgAAAAE#8AAAAAAAAAAAAAIA#####wAGcGVnYWwxAANwPTEAAAAECAAAAAUAAAALAAAAAT#wAAAAAAAAAAAABgD#####AAJlcQAHYV5uKmFecAAAAAQCAAAABwAAAAgAAAAAAAAABQAAAA4AAAAHAAAACAAAAAAAAAAFAAAACwAAAAIAAWEAAWIAAAAGAP####8AAm0xAANhXm4AAAAHAAAACAAAAAAAAAAFAAAADgAAAAIAAWEAAWIAAAAGAP####8AAm0yAANhXnAAAAAHAAAACAAAAAAAAAAFAAAACwAAAAIAAWEAAWL#####AAAAAgAGQ0xhdGV4AP####8BAAAAAf####8RQH#wAAAAAABANgAAAAAAAAAAAAAAAAAAAAAAIkE9XEZvclNpbXB7bTF9IFx0aW1lcyBcRm9yU2ltcHttMn3#####AAAABAAPQ0VkaXRldXJGb3JtdWxlAP####8AAAAAAf####8TQEeAAAAAAABAMQAAAAAAAAAAAAAAAgAAAAAAAAAGAAQkQT0kAAAADwEBAAE9AAEAAAACAP####8AAXMAA24rcAAAAAQAAAAABQAAAA4AAAAFAAAACwAAAAYA#####wADc29sAANhXnMAAAAHAAAACAAAAAAAAAAFAAAAIAAAAAIAAWEAAWIAAAACAP####8ABWV4YWN0AE16ZXJvKHJlcCh4MSwwKS1zb2woeDEsMCkpKnplcm8ocmVwKHgyLDApLXNvbCh4MiwwKSkqemVybyhyZXAoeDMsMCktc29sKHgzLDApKQAAAAQCAAAABAIAAAAKAAAACgAAAAQBAAAACwAAAAIAAAAGAAAABQAAAAcAAAABAAAAAAAAAAAAAAALAAAAAgAAACEAAAAFAAAABwAAAAEAAAAAAAAAAAAAAAoAAAAKAAAABAEAAAALAAAAAgAAAAYAAAAFAAAACAAAAAEAAAAAAAAAAAAAAAsAAAACAAAAIQAAAAUAAAAIAAAAAQAAAAAAAAAAAAAACgAAAAoAAAAEAQAAAAsAAAACAAAABgAAAAUAAAAJAAAAAQAAAAAAAAAAAAAACwAAAAIAAAAhAAAABQAAAAkAAAABAAAAAAAAAAD#####AAAAAwAQQ1Rlc3RFcXVpdmFsZW5jZQD#####AAZyZXNvbHUAAAAhAAAABgEAAAAAAT#wAAAAAAAAAAAAAA0A#####wEAAP8B#####xNAIAAAAAAAAEAgAAAAAAAAAQHMzP8AAAAAAAAAAAHnXGJlZ2lue2FycmF5fXtsfQp7XHRleHR7T24gdXRpbGlzZSBsYSByw6hnbGUgZGUgY2FsY3VsIDogfWFebiBcdGltZXMgYV5wID0gYSBee24rcH0gfQpcXCB7QSA9IFxJZntuZWdhbDF9e2FeMX17XEZvclNpbXB7bTF9fSBcdGltZXMgXElme3BlZ2FsMX17YV4xfXtcRm9yU2ltcHttMn0gPSBcRm9yU2ltcHtmMX0gPSBcRm9yU2ltcHtzb2x9fX0KXFwge0EgPSBcRm9yU2ltcHtzb2x9IFx0ZXh0eyDDqXRhaXQgbGEgcsOpcG9uc2UgYXR0ZW5kdWUufX0KXElme2NvbmZ1c2lvbjF9e1xce1x0ZXh0Y29sb3J7cmVkfXtcdGV4dHtBdHRlbnRpb24gOiBOZSBwYXMgY29uZm9uZHJlIH1hID0gYV4xIFx0ZXh0eyBhdmVjIH1hXjA9MSBcdGV4dHsgKHNpIH1hIFxuZSAwKX19fXt9ClxJZntjb25mdXNpb24yfXtcXHtcdGV4dGNvbG9ye3JlZH17XHRleHR7QXR0ZW50aW9uIDogTmUgcGFzIGNvbmZvbmRyZSBhdmVjIH0gXEZvclNpbXB7Zm9yfX19fXt9CgpcZW5ke2FycmF5ff####8AAAABABBDTWFjcm9BcHBhcml0aW9uAP####8BAAD#Af####8NQHlAAAAAAABAWUAAAAAAAAIAAAAAAAAAAAAADHZvaXJTb2x1dGlvbgAAAAAAAQAAACQAAAAAAgD#####AAdyZXBvbnNlABpzaShyZXNvbHUsMSxzaShleGFjdCwyLDApKQAAAAwAAAAABQAAACMAAAABP#AAAAAAAAAAAAAMAAAAAAUAAAAiAAAAAUAAAAAAAAAAAAAAAQAAAAAAAAAA#####wAAAAEAEUNNYWNyb0Rpc3Bhcml0aW9uAP####8BAAAAAf####8NQCwAAAAAAABAZiAAAAAAAAIAAAAAAAAAAAAADG1hc3F1ZXJDaGFtcAAAAAAAAQAAAB######AAAAAQARQ01hY3JvU3VpdGVNYWNyb3MA#####wEAAAAB#####w1AKgAAAAAAAEBp4AAAAAAAAgAAAAAAAAAAAAAIc29sdXRpb24AAAAAAAIAAAAnAAAAJQAAAA0A#####wH#AAAB#####xNAHAAAAAAAAEBNAAAAAAAAAAAAAAAAAAAAAAELXHRleHRjb2xvcntyZWR9e1x0ZXh0e0F0dGVudGlvbiA6IH19ClxJZntjb25mdXNpb24xfXtcdGV4dGNvbG9ye3JlZH17XHRleHR7TmUgcGFzIGNvbmZvbmRyZSB9YSA9IGFeMSBcdGV4dHsgYXZlYyB9YV4wPTEgXHRleHR7IChzaSB9YSBcbmUgMCl9fXt9ClxJZntjb25mdXNpb24yfXtcdGV4dGNvbG9ye3JlZH17XHRleHR7TmUgcGFzIGNvbmZvbmRyZSB9YV57XFZhbHtufX0gXHRpbWVzIGFee1xWYWx7cH19XHRleHR7IGF2ZWMgfShhXlxWYWx7bn0pXlxWYWx7cH19fXt9AAAAAgD#####AAVmYXV0ZQAVY29uZnVzaW9uMXxjb25mdXNpb24yAAAABAsAAAAFAAAAFgAAAAUAAAAXAAAAEAD#####AQAAAAH#####DUAwAAAAAAAAQGggAAAAAAACAAAAAAAAAAAAAAl2b2lyRmF1dGUAAAAAAAEAAAApAAAAABEA#####wEAAAAB#####w1AWwAAAAAAAEBoIAAAAAAAAgAAAAAAAAAAAAAMbWFzcXVlckZhdXRlAAAAAAABAAAAKf###############w=='
          sq.nomCalcul = 'A'
          sq.nblatex = 1
          sq.param = 'abcd'
          sq.enonce = 'Tu dois calculer $£a$ sous la forme la plus simple possible et tu as droit à $£e$ étapes maximum.'
          sq.charset = 'a[]0123456789.+-/*²^'
          // après personnalisation sq.fig ne sera plus vide et on passera plus là
          sq.titre = 'Ressource à personnaliser'
          j3pShowError('Cette ressource doit être personnalisée en éditant la ressource dans l’éditeur de graphes.')
        } else {
          sq.titre = parcours.donneesSection.titre
          sq.nomCalcul = parcours.donneesSection.nomCalcul
          sq.nblatex = parcours.donneesSection.nblatex // Le nombre de paramètres LaTeX dans le texte
          sq.param = parcours.donneesSection.param // Chaine contenant les paramètres autorisés
          sq.enonce = ''
          for (let i = 1; i < 9; i++) {
            if (parcours.donneesSection['enonceligne' + i] !== '') {
              if (i !== 0) sq.enonce += '<br>'
              sq.enonce += parcours.donneesSection['enonceligne' + i]
            }
          }
          sq.charset = parcours.donneesSection.charset
        }
        sq.enonce += '<br>Pour élever à une puissance, utiliser la touche ^ ou ² pour un carré.' +
          '<br>Appuyer sur la touche Entrée pour valider toutes les étapes, puis sur le bouton OK après la réponse finale pour valider.'

        if (sq.titre) parcours.afficheTitre(sq.titre)

        j3pDiv(parcours.zones.MG, {
          id: 'conteneur',
          contenu: '',
          style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
        })
        j3pDiv('conteneur', 'enonce', '')
        j3pDiv('conteneur', {
          id: 'formules',
          contenu: '',
          style: parcours.styles.petit.enonce
        }) // Contient le formules entrées par l’élève
        j3pDiv('conteneur', 'info', '')
        const nbe = sq.nbEssais
        const nbc = sq.nbchances
        j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', {
          style: {
            color: '#7F007F'
          }
        })
        j3pDiv('conteneur', 'conteneurbouton', '')
        j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
          'j3p.Sectionexcalc.recopierReponse()')
        j3pElement('boutonrecopier').style.display = 'none'
        /*
        j3pDiv("conteneur","divmtg32","",[0,0],j3p.styles.etendre("toutpetit.enonce",
        {
          width:"700px",
          height:"520px"
        }));
        */
        j3pDiv('conteneur', 'divmtg32', '')
        j3pCreeSVG('divmtg32', {
          id: 'mtg32svg',
          width: 700,
          height: 520
        })

        const style = parcours.styles.petit.correction
        style.marginTop = '1.5em'
        style.marginLeft = '0.5em'
        j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })

        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              sq.mtgAppLecteur = mtgAppLecteur
              initMtg()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        sq.mtgAppLecteur.setEditorsEmpty('mtg32svg')
        // sq.mtgAppLecteur.executeMacro("mtg32svg","initialiser");
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
        const parc = this
        sq.mtgAppLecteur.setEditorCallBackOK('mtg32svg', 'rep', function () {
          validation(parc)
        })
        sq.mtgAppLecteur.setEditorsSize('mtg32svg', '17px')
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient rꪮitialisés
        j3pElement('enonce').innerHTML = ''
        j3pElement('formules').innerHTML = ''
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nblatex)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          param[t[i]] = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        }
        param.e = String(sq.nbEssais)
        j3pAffiche('enonce', 'texte', sq.enonce, param)
        const nbe = sq.nbEssais - sq.nbexp
        const nbc = sq.nbchances - sq.numEssai + 1
        j3pElement('texteinfo').innerHTML = 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).'
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        sq.mtgAppLecteur.activateField('mtg32svg', 'rep') // sq.mtgAppLecteur.setActive("mtg32svg",true);
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      let simplifieri = false
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      const ch = sq.mtgAppLecteur.getFieldValue('mtg32svg', 'rep')
      if (ch.length !== 0) {
        if (sq.mtgAppLecteur.fieldValidation('mtg32svg', 'rep')) {
          validation(this, false)
        } else {
          bilanreponse = 'incorrect'
        }
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (res1 === 1) {
          bilanreponse = 'exact'
          simplifieri = true
        } else {
          if (res1 === 2) {
            bilanreponse = 'exactpasfini'
          } else {
            bilanreponse = 'erreur'
          }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve en bas de la fenêtre ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse illisible ou incorrecte'
          this.afficheBoutonValider()
          sq.mtgAppLecteur.activateField('mtg32svg', 'rep')
        } else { // Une réponse a été saisie
          // Bonne réponse
          if (bilanreponse === 'exact') {
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (simplifieri) {
              j3pElement('correction').innerHTML = cBien
            } else {
              j3pElement('correction').innerHTML = 'C’est bien mais on pouvait  simplifier la réponse'
            }
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) {
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
            }
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            sq.numEssai++
            if (bilanreponse === 'exactpasfini') {
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              j3pElement('correction').innerHTML = cFaux
            }
            j3pElement('info').innerHTML = ''
            const nbe = sq.nbEssais - sq.nbexp
            const nbc = sq.nbchances - sq.numEssai + 1
            j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', {
              style: {
                color: '#7F007F'
              }
            })

            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
            if ((sq.nbexp < sq.nbEssais) && (sq.numEssai <= sq.nbchances)) {
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              // indication éventuelle ici
              // j3p.Sectionexcalc.nbExp += 1;
              sq.mtgAppLecteur.setActive('mtg32svg', true)
              sq.mtgAppLecteur.activateField('mtg32svg', 'rep')
            } else { // Erreur au nème essai
              this._stopTimer()

              sq.mtgAppLecteur.setActive('mtg32svg', false)
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')

              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
