import { j3pAddElt, j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
/**
 * Section destinée à passer aléatoirement à un nœud suivant (le nb de choix est paramétrable)
 * @fileOverview
 * @author Daniel <daniel.caillibaud@sesamath.net>
 * @since 2023-01
 */

const defaultDatas = {
  nbAlea: 2,
  description: '',
  titre: 'Tirage au sort',
  nbrepetitions: 1 // param obligatoire
}
export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbAlea', defaultDatas.nbAlea, 'entier', 'Nombre de choix différents (il faudra créer un branchement pour chacun, 2 min, 9 max)'],
    ['description', defaultDatas.description, 'string', 'Phrase qui sera écrite comme énoncé, en attendant un clic sur OK (si vide ça passera d’office au nœud suivant)'],
    ['titre', defaultDatas.titre, 'string', 'Le titre affiché']
  ],
  // La liste des pe, une sera tirée au hasard
  pe: [
    { pe_1: 'Choix1' },
    { pe_2: 'Choix2' },
    { pe_3: 'Choix3' },
    { pe_4: 'Choix4' },
    { pe_5: 'Choix5' },
    { pe_6: 'Choix6' },
    { pe_7: 'Choix7' },
    { pe_8: 'Choix8' },
    { pe_9: 'Choix9' }
  ]
}

/**
 * section alea
 * @this {Parcours}
 */
export default function main () {
  if (this.etat === 'enonce') {
    // on indique qu’on ne gère pas de score, mais qu'on veut être rappelé en navigation
    this.setPassive()
    const ds = this.donneesSection
    // on est appelé une seule fois, forcément au début de la section, inutile de tester
    if (!Number.isInteger(ds.nbAlea) || ds.nbAlea < 2 || ds.nbAlea > 9) {
      j3pShowError('nbAlea invalide (2 min, 9 max), 2 sera imposé')
      ds.nbAlea = defaultDatas.nbAlea
    }
    this.construitStructurePage('presentation1')
    this.afficheTitre(ds.titre)
    // on détermine aléatoirement la pe
    const pe = 'pe_' + j3pGetRandomInt(1, this.donneesSection.nbAlea)
    this.parcours.pe = this.donneesSection[pe]
    if (ds.description) {
      const opts = { style: { margin: '1rem' } }
      j3pAddElt(this.zonesElts.MG, 'p', ds.description, opts)
      j3pAddElt(this.zonesElts.MG, 'p', 'Clique sur le bouton OK pour passer à la suite', opts)
    }
    this.finEnonce()
    if (!ds.description) {
      // y’a pas de description => on passe directement au nœud suivant
      this.finNavigation()
    }
  }
}
