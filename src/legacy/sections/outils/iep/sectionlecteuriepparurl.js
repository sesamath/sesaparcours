import { j3pDiv, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
/*
    Yves Biton
    Juillet 2017

    sectionlecteuriepparurl
    Sert à afficher une figure MathGraph32 prcédée ou non de lusqu'à 4 lignes et suivie ou non de jsuqu'à 4 lignes
    Les lignes peuvent contenir du code LaTeX.

*/

export const params = {
  outils: ['iep', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['titre', '', 'string', 'Titre de la figure'],
    ['url', '', 'string', 'url de l’animation IEP'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],

    ['width', 700, 'entier', 'Largeur en pixels de la figure (600 par défaut)'],
    ['height', 500, 'entier', 'Largeur en pixels de la figure (500 par défaut)'],
    ['avantligne1', '', 'string', 'Ligne 1 précédant la figure (peut être une chaîne vide)'],
    ['avantligne2', '', 'string', 'Ligne 2 précédant la figure (peut être une chaîne vide)'],
    ['avantligne3', '', 'string', 'Ligne 3 précédant la figure (peut être une chaîne vide)'],
    ['avantligne4', '', 'string', 'Ligne 4 précédant la figure (peut être une chaîne vide)'],
    ['apresligne1', '', 'string', 'Ligne 1 suivant la figure (peut être une chaîne vide)'],
    ['apresligne2', '', 'string', 'Ligne 2 suivant la figure (peut être une chaîne vide)'],
    ['apresligne3', '', 'string', 'Ligne 3 suivant la figure (peut être une chaîne vide)'],
    ['apresligne4', '', 'string', 'Ligne 4 suivant la figure (peut être une chaîne vide)']
  ]
}

/**
 * section lecteuriepparurl
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage

  function depart () {
    if (typeof iepLoad !== 'function') return j3pShowError(Error('Le lecteur instrumenpoche n’est pas chargé correctement'))
    /* global iepLoad */
    iepLoad('diviep', stor.url)
    j3pAffiche('avant', 'texteavant', stor.avant + '\n')
    j3pAffiche('apres', 'texteapres', stor.apres)
  }

  function getDonnees () {
    const ds = {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 1,
      nbetapes: 1,
      width: '800',
      height: '600',
      url: 'https://ressources.sesamath.net/coll_docs/cah/valide/manuel_accomp_2013_6G2exercice2_IePv2.xml',
      titre: 'Animation IEP',
      exp: '',
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      pe: 0
    }
    for (let i = 1; i <= 4; i++) ds['avantligne' + i] = ''
    for (let i = 1; i <= 4; i++) ds['apresligne' + i] = ''
    return ds
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = getDonnees()

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree
        this.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        stor.titre = me.donneesSection.titre
        stor.width = me.donneesSection.width || 800
        stor.height = me.donneesSection.height || 600
        stor.url = me.donneesSection.url
        stor.nbsol = 1 // Contiendra le nombre de figures pour la correction.
        stor.avant = ''
        for (let i = 1; i < 5; i++) {
          if (me.donneesSection['avantligne' + i] !== '') {
            if (i !== 0) stor.avant += '<br>'
            stor.avant += me.donneesSection['avantligne' + i]
          }
        }
        stor.apres = ''
        for (let i = 1; i < 5; i++) {
          if (me.donneesSection['apresligne' + i] !== '') {
            if (i !== 0) stor.apres += '<br>'
            stor.apres += me.donneesSection['apresligne' + i]
          }
        }
        me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.85 })
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(stor.titre)
        j3pDiv(me.zones.MG, {
          id: 'conteneur',
          contenu: '',
          style: me.styles.etendre('toutpetit.enonce', { padding: '2px' })
        })
        j3pDiv('conteneur', 'avant', '')
        j3pDiv('conteneur', 'diviep', '')
        j3pDiv('conteneur', 'apres', '')
        depart()
      }
      this.finEnonce()
      this.etat = 'navigation'
      this.sectionCourante()
      break // case "enonce":

    case 'navigation':
      this.parcours.pe = 'Vu'
      this.score = 1
      // Obligatoire
      if (this.isElapsed) { // limite de temps
        this.sectionSuivante()
      }

      break // case "navigation":
  }
}
