import { j3pDiv, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
/*
    Yves Biton
    Juillet 2017

    sectionlecteuriep
    Sert à afficher une figure MathGraph32 prcédée ou non de lusqu'à 4 lignes et suivie ou non de jsuqu'à 4 lignes
    Les lignes peuvent contenir du code LaTeX.

*/

export const params = {
  outils: ['iep', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['titre', 'Animation Instrumenpoche', 'string', 'Titre de la figure'],
    ['script', '', 'string', 'Code XML de l’animation IEP'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],

    ['width', 800, 'entier', 'Largeur en pixels de la figure (600 par défaut)'],
    ['height', 600, 'entier', 'Largeur en pixels de la figure (500 par défaut)'],
    ['avantligne1', '', 'string', 'Ligne 1 précédant la figure (peut être une chaîne vide)'],
    ['avantligne2', '', 'string', 'Ligne 2 précédant la figure (peut être une chaîne vide)'],
    ['avantligne3', '', 'string', 'Ligne 3 précédant la figure (peut être une chaîne vide)'],
    ['avantligne4', '', 'string', 'Ligne 4 précédant la figure (peut être une chaîne vide)'],
    ['apresligne1', '', 'string', 'Ligne 1 suivant la figure (peut être une chaîne vide)'],
    ['apresligne2', '', 'string', 'Ligne 2 suivant la figure (peut être une chaîne vide)'],
    ['apresligne3', '', 'string', 'Ligne 3 suivant la figure (peut être une chaîne vide)'],
    ['apresligne4', '', 'string', 'Ligne 4 suivant la figure (peut être une chaîne vide)']
  ]
}

/**
 * section lecteuriep
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const sq = this.storage

  function depart () {
    if (typeof iepLoad !== 'function') return j3pShowError(Error('Le lecteur instrumenpoche n’est pas chargé correctement'))
    /* global iepLoad */
    iepLoad('diviep', sq.script, {}, function (error) {
      if (error) return j3pShowError(error)
      j3pAffiche('avant', 'texteavant', sq.avant + '\n')
      j3pAffiche('apres', 'texteapres', sq.apres)
      me.finEnonce()
      me.etat = 'navigation'
      me.afficheBoutonSectionSuivante()
    })
  }

  if (this.etat === 'enonce') {
    // on est appelé qu’une seule fois (ni étapes ni répétition)
    me.validOnEnter = false // ex donneesSection.touche_entree
    sq.titre = this.donneesSection.titre
    sq.width = this.donneesSection.width || 800
    sq.height = this.donneesSection.height || 600
    sq.script = this.donneesSection.script
    sq.nbsol = 1 // Contiendra le nombre de figures pour la correction.
    if (sq.script === '') {
      sq.script = '<?xml version="1.0" encoding="UTF-8" ?>\n' +
            '<INSTRUMENPOCHE version="2" auteur="SebD" licence="CC-BY-SA" >\n' +
            '<commentaire texte="A. dessin du CADRE LIMITE : 800x600 pixels" />\n' +
            '<action abscisse="800" ordonnee="50" vitesse="10000" mouvement="translation" objet="crayon" />\n' +
            '<action abscisse="800" ordonnee="600" epaisseur="2" couleur="blanc" pointille="tiret" id="c1" mouvement="tracer" objet="crayon" vitesse="1000" />\n' +
            '<action abscisse="0" ordonnee="600" epaisseur="2" couleur="blanc" pointille="tiret" id="c2" mouvement="tracer" objet="crayon" vitesse="1000" />\n' +
            '<action pris="false" abscisse="10" ordonnee="50" id="50" mouvement="creer" objet="texte" />\n' +
            '<action couleur="forestgreen" taille="18" police="Tahoma" texte=" Construction d&apos;un cercle de diamètre [TS]" id="50" mouvement="ecrire" objet="texte" />\n' +
            '<action type="point" ordonnee="340" abscisse="233" couleur="noir" id="T" mouvement="creer" objet="point" />\n' +
            '<action ordonnee="-17" abscisse="-28" couleur="noir" nom="T" id="T" mouvement="nommer" objet="point" />\n' +
            '<action type="point" ordonnee="262" abscisse="403" couleur="noir" id="S" mouvement="creer" objet="point" />\n' +
            '<action couleur="noir" nom="S" id="S" mouvement="nommer" objet="point" />\n' +
            '<action ordonnee="-26" abscisse="-5" couleur="noir" nom="S" id="S" mouvement="nommer" objet="point" />\n' +
            '<action abscisse="233" ordonnee="340" cible="T" vitesse="1000" mouvement="translation" objet="crayon" />\n' +
            '<action id="4" epaisseur="0" couleur="noir" cible="S" vitesse="1000" mouvement="tracer" objet="crayon" />\n' +
            '<action pris="false" ordonnee="146" abscisse="480" id="2" mouvement="creer" objet="texte" />\n' +
            '<action tempo="10" couleur="bleu" texte="On place le milieu O du segment [TS]." id="2" mouvement="ecrire" objet="texte" />\n' +
            '<action cible="T" mouvement="translation" objet="regle" />\n' +
            '<action cible="S" mouvement="rotation" objet="regle" />\n' +
            '<action tempo="10" mouvement="montrer" objet="regle" />\n' +
            '<action sens="-5" angle="-24.65" mouvement="rotation" objet="regle" />\n' +
            '<action mouvement="montrer" objet="crayon" />\n' +
            '<action tempo="10" abscisse="317" ordonnee="301" mouvement="translation" objet="crayon" />\n' +
            '<action tempo="10" sens="-5" angle="-110.12" mouvement="rotation" objet="crayon" />\n' +
            '<action pris="false" ordonnee="480" abscisse="74" id="3" mouvement="creer" objet="texte" />\n' +
            '<action couleur="rouge" texte="Ici, TS = 6,2 cm, donc le milieu se trouve à 3,1 cm de T." id="3" mouvement="ecrire" objet="texte" />\n' +
            '<action type="point" tempo="10" ordonnee="301" abscisse="318" couleur="0x8844" id="5" mouvement="creer" objet="point" />\n' +
            '<action ordonnee="-26" abscisse="-5" couleur="noir" nom="O" id="5" mouvement="nommer" objet="point" />\n' +
            '<action tempo="10" mouvement="masquer" objet="regle" />\n' +
            '<action tempo="10" type="longueur" ordonnee="285" abscisse="352" forme="\\\\" epaisseur="2" couleur="0x8844" id="9" mouvement="creer" objet="longueur" />\n' +
            '<action type="longueur" tempo="10" ordonnee="321" abscisse="272" forme="\\\\" epaisseur="2" couleur="0x8844" id="10" mouvement="creer" objet="longueur" />\n' +
            '<action couleur="blanc" texte="Ici, TS = 6,2 cm, donc le milieu se trouve à 3,1 cm de T." id="3" mouvement="ecrire" objet="texte" />\n' +
            '<action tempo="10" mouvement="masquer" objet="crayon" />\n' +
            '<action tempo="10" pris="false" ordonnee="182" abscisse="480" id="6" mouvement="creer" objet="texte" />\n' +
            '<action couleur="0x88CCCC" tempo="10" texte="On place le milieu O du segment [TS]." id="2" mouvement="ecrire" objet="texte" />\n' +
            '<action couleur="bleu" texte="On pointe le compas sur le milieu O £lt£br£gt£ du segment [TS], et on l&apos;écarte  £lt£br£gt£ jusqu&apos;au point T." id="6" mouvement="ecrire" objet="texte" />\n' +
            '<action tempo="10" mouvement="montrer" objet="compas" />\n' +
            '<action tempo="10" cible="5" mouvement="translation" objet="compas" />\n' +
            '<action cible="T" mouvement="rotation" objet="compas" />\n' +
            '<action tempo="20" cible="T" mouvement="ecarter" objet="compas" />\n' +
            '<action couleur="0x88CCCC" texte="On pointe le compas sur le milieu O £lt£br£gt£ du segment [TS], et on l&apos;écarte  £lt£br£gt£ jusqu&apos;au point T." id="6" mouvement="ecrire" objet="texte" />\n' +
            '<action ordonnee="265" abscisse="480" id="1" mouvement="creer" objet="texte" />\n' +
            '<action couleur="bleu" texte="On trace le cercle de centre O £lt£br£gt£ et passant par T (et donc aussi par S)." id="1" mouvement="ecrire" objet="texte" />\n' +
            '<action sens="5" angle="155.1" mouvement="rotation" objet="compas" />\n' +
            '<action mouvement="lever" objet="compas" />\n' +
            '<action sens="5" angle="155.1" mouvement="rotation" objet="compas" />\n' +
            '<action couleur="bleu" epaisseur="3" sens="-5" fin="155.03" debut="517.62" id="7" mouvement="tracer" objet="compas" />\n' +
            '<action mouvement="coucher" objet="compas" />\n' +
            '<action tempo="10" mouvement="masquer" objet="compas" />\n' +
            '</INSTRUMENPOCHE>  '
      sq.avant = ''
      sq.apres = ''
      sq.titre = 'Ressource à personnaliser'
      j3pShowError('Cette ressource doit être personnalisée en éditant la ressource dans l’éditeur de graphes.')
    } else {
      sq.avant = ''
      for (let i = 1; i < 5; i++) {
        if (this.donneesSection['avantligne' + i] !== '') {
          if (i !== 0) sq.avant += '<br>'
          sq.avant += this.donneesSection['avantligne' + i]
        }
      }
      sq.apres = ''
      for (let i = 1; i < 5; i++) {
        if (this.donneesSection['apresligne' + i] !== '') {
          if (i !== 0) sq.apres += '<br>'
          sq.apres += this.donneesSection['apresligne' + i]
        }
      }
    }
    this.construitStructurePage('presentation3')
    if (sq.titre) this.afficheTitre(sq.titre)
    this.setPassive()
    // Construction des boutons faite par construitStructurePage, mais ici on veut directement afficher le bouton sectionSuivante
    this.afficheBoutonSectionSuivante()

    j3pDiv(this.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: this.styles.etendre('toutpetit.enonce', { padding: '2px' })
    })
    j3pDiv('conteneur', 'avant', '')
    j3pDiv('conteneur', 'diviep', '')
    j3pDiv('conteneur', 'apres', '')
    depart()
  }
  // section passive, pas de case correction ni navigation
}
