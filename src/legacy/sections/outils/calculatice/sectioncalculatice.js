import { j3pAddElt, j3pAfficheBibli, j3pEmpty, j3pGetRessourceBibli, j3pGetUrlParams, j3pShowError } from 'src/legacy/core/functions'

/**
 * Cette section squelette charge des exos calculatice via la bibliothèque (qui lance le chargement sur mep-col)
 */

/* ex avec :
 http://localhost:8081/?graphe=[1,"calculatice",[{pe:"<=0",nn:"2",conclusion:"noeud 2"},{pe:">=1",nn:"1",conclusion:"noeud 1"},{exo:"2842"}]];[2,"calculatice",[{pe:">=0",nn:"fin",conclusion:"fin"},{exo:"2843"}]];
 */

const defaultExo = 'sesabibli/50117'

export const params = {
  outils: [],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['exo', '', 'string', 'identifiant de l’exercice Calcul@tice dans sa sésathèque (par défaut on cherchera {sesathequeCourante}/calculatice/{exo}, sauf si exo contient un slash, par ex sesabibli/xxx)'],
    ['parametrable', false, 'boolean', 'Permettre à l’élève de paramétrer la ressource calculatice (changer le délai ou d’autres paramètres)']
  ]
}

/**
 * section calculatice
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage

  /**
   * Fonction qui sera appelée par display à la fin de l’exercice
   * @param {Resultat} resultat Les données formatées en Resultat, l’envoi d’origine est dans resultat.original
   *        (cf http://bibliotheque.sesamath.net/vendors/sesamath/Resultat.js)
   */
  function recupereScoreCalculatice (resultat) {
    me.score = resultat.score
    // faut cacher le bouton recommencer
    const btn = me.conteneur.querySelector('button.recommencer')
    if (btn) btn.style.display = 'none'
    else console.error(Error('Pas trouvé le bouton recommencer à la fin de l’exercice'))

    if (resultat.fin === true) {
      me.sectionCourante('navigation')
    }
  }

  /**
     * Appelle la bibli pour récupérer la ressource, initialise titre et nb_questions
     * en fonction du résultat, dimensionne le div puis charge les swf pour l’affichage.
     * @param {HTMLElement} div Le div à redimensionner
     */
  function prepareExo (div) {
    // c’est impossible de passer des flashvars en param à l’iframe de la bibli
    // on charge donc le display.js de la bibliothèque et on utilise sa logique
    // pour afficher les exos mep dans notre dom
    // on charge la ressource (json) puis on l’affiche
    const exo = me.donneesSection.exo
    let sesathequeBaseId, idBibli
    if (exo.includes('/')) {
      const chunks = exo.split('/')
      sesathequeBaseId = chunks.shift()
      idBibli = chunks.join('/')
    } else {
      sesathequeBaseId = j3pGetUrlParams('sesathequeBaseId') || me.sesathequeBaseId || 'sesabibli'
      idBibli = 'calculatice/' + exo
    }
    j3pGetRessourceBibli(idBibli, sesathequeBaseId, function (error, ressource) {
      if (error) {
        return j3pShowError(error)
      }
      if (ressource.type !== 'ecjs') {
        return j3pShowError(Error(`La ressource ${idBibli} n’est pas une ressource calcul@tice`))
      }
      const largeur = 959
      const hauteur = 618
      redimensionne(div, largeur, hauteur)

      // on ajoute la ressource de la bibli, avec
      const params = {
        sesathequeBaseId,
        container: me.zonesElts.MG,
        parametrable: me.donneesSection.parametrable,
        resultatCallback: recupereScoreCalculatice
      }
      j3pAfficheBibli(ressource, params, function (error) {
        if (error) return j3pShowError(error)
        // on vire le titre précédent (qui était provisoire, et de toute façon sinon ça déborde)
        me.afficheTitre(ressource.titre, { replace: true })
        me.finEnonce()
        // On efface HD car le score est affiché par calculatice (et on est pas prévenu de l’avancement des questions)
        j3pEmpty(me.zonesElts.HD)
      })
    })
  }

  /**
     * Redimensionne un div
     * @param {HTMLElement} div
     * @param {number} largeur
     * @param {number} hauteur
     */
  function redimensionne (div, largeur, hauteur) {
    // la css nous ajoute 1px de bordure et 10 margin-bottom
    // la bordure compte pas, sauf pour certains navigateurs...
    div.style.width = (largeur + 2) + 'px'
    div.style.height = (hauteur + 12) + 'px'
    // faut aussi en ajouter au grand-père
    me.conteneur.style.width = (largeur + 2) + 'px'
  }

  switch (me.etat) {
    case 'enonce':
      // normalement c’est toujours le début car on a une seule répétition
      if (!this.debutDeLaSection) {
        this.videLesZones()
        j3pShowError(Error('Erreur interne, cet exercice ne devrait pas avoir de répétition'))
      }
      if (this.donneesSection.exo === '') {
        j3pShowError('Vous devez paramétrer cette ressource pour choisir l’exercice calcul@tice voulu, ceci est un exemple avec ' + defaultExo)
        this.donneesSection.exo = defaultExo
      }
      this.construitStructurePage('presentation3')
      stor.divExo = j3pAddElt(this.zonesElts.MG, 'div')
      // On met un titre provisoire
      this.afficheTitre('Chargement de la ressource calcul@tice')
      // et on lance le chargement (Attention, ça vire la zone de boutons !)
      prepareExo(this.zonesElts.MG)
      break // case "enonce":

    case 'correction':
      // pour cacher l’exo quand le temps limite est dépassé
      if (this.donneesSection.limite) {
        stor.divExo.style.display = 'none'
      }
      break // case "correction":

    case 'navigation': {
      this.finNavigation(true)
      // on cache question 1 sur 1 et score 0,xxx sur 1
      j3pEmpty(this.zonesElts.HD)
      // faut remettre le bouton, le conteneur de boutons a été viré par le chargement de calculatice
      this.ajouteBoutons()
      this.afficheBoutonSectionSuivante(true)
      break // case "navigation":
    }

    default:
      throw new Error('section appelée avec l’état ' + this.etat)
  }
}
