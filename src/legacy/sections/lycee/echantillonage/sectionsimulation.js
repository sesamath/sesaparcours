import { j3pAddContent, j3pAddElt, j3pChaine, j3pEmpty, j3pFocus, j3pGetRandomElt, j3pGetRandomInt, j3pPGCD, j3pRestriction, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Programme python à compléter, la faire avec l’outil n’était pas possible, la validation telle qu’elle peut être faite n’étant pas permise avec de l’aléatoire
 * @author Rémi DENIAUD
 * @since janvier 2022
 * @fileOverview Cette section simule un échantillon avec choix aléatoire de deux valeurs
 */

/**
 * Les paramètres de la section, anciennement j3p.SectionXxx = {}
 */
export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {

    // Nombre de répétitions de l’exercice
    nbrepetitions: 3,
    nbchances: 2,
    nbetapes: 1,
    indication: '',
    textes: {
      titre_exo: 'Simulation',
      consigne1_1: 'Une étude statistique sur un tireur de pénalties montre qu’il marque le but dans £{p1}&nbsp;% des cas.',
      consigne1_2: 'On dispose d’un dé cubique bien équilibré.',
      consigne1_3: 'Lors d’un scrutin, £{p1}&nbsp;% des votants se sont exprimé pour le candidat A.',
      consigne1_4: 'Une urne contient £b boules blanches et £r boules rouges.',
      consigne1_5: 'Un fabricant estime que £{p1}&nbsp;% des composants qu’il fabrique sont défectueux.',
      consigne1_6: 'Un jeu de loterie est organisé et £{p1}&nbsp;% des tickets mis en vente sont gagnants.',
      consigne2_1: 'Compléter le programme suivant permettant de simuler £n pénalties tirés par ce joueur et affichant pour chacun d’eux sa réussite ou son échec.',
      consigne2_2: 'Compléter le programme suivant permettant de simuler £n lancers de ce dé et affichant à chaque lancer si on obtient la face 6 ou non.',
      consigne2_3: 'Compléter le programme suivant permettant de simuler le questionnement de £n votants pour savoir s’ils ont voté pour le candidat A ou non.',
      consigne2_4: 'Compléter le programme suivant permettant de simuler le tirage avec remise et au hasard de £n boules de cette urne et de relever leur couleur.',
      consigne2_5: 'Compléter le programme suivant permettant de simuler le tirage au sort de £n composants de la production et de vérifier pour chacun s’il est ou non défectueux.',
      consigne2_6: 'Compléter le programme suivant permettant de simuler l’achat de £n tickets et de vérifier pour chacun s’il est ou non gagnant.',
      indic: 'On se limitera à une seule inégalité dans la troisième ligne.',
      choix1: '"but"|"face 6"|"a voté pour le candidat A"|"boule blanche"|"composant défectueux"|"ticket gagnant"',
      choix2: '"manqué"|"face autre que 6"|"n’a pas voté pour le candidat A"|"boule rouge"|"composant en état"|"ticket perdant"',
      comment1: 'Il y avait toutefois plus simple dans l’utilisation du range&nbsp;!',
      comment2: 'N’utiliser qu’une seule fois la fonction randint&nbsp;!',
      comment3: 'N’utiliser qu’une seule fois la fonction random&nbsp;!',
      comment4: 'N’utiliser qu’une seule des deux fonctions randint et random&nbsp;!',
      comment5: 'La fonction random n’est pas utilisée ou mal&nbsp;!',
      comment6: 'Utilise la fonction random de manière simple (sans calcul)&nbsp;!',
      comment7: 'Il y a trop d’inégalités pour accepter la réponse&nbsp;!',
      comment8: 'Je ne comprends pas ce qui est écrit&nbsp;!',
      comment9: 'On ne veut pas plus d’une inégalité&nbsp;!',
      comment10: 'Il faut utiliser random() et non random.random()&nbsp;!',
      corr1: 'random() renvoie un nombre aléatoire dans l’intervalle $[0;1[$. La probabilité qu’il soit strictement inférieur (ou inférieur ou égal) à $£{pdecVirg}$ vaut $£{pdecVirg}$.',
      corr2: 'Donc le programme ci-dessous répond au problème&nbsp;:'
    }
  }
} // getDonnees

/**
 * Retourne les données pour chaque sujet
 * @private
 * @param {Parcours} me
 * @param {Number} numSujet
 * @return {Object}
 */
function genereAlea (me, numSujet) {
  const obj = {}
  const ds = me.donneesSection
  if (numSujet === 1) {
    obj.p = j3pGetRandomInt(570, 750) / 10
  } else if (numSujet === 2) {
    obj.p = 1 / 6
  } else if (numSujet === 3) {
    obj.p = j3pGetRandomInt(150, 230) / 10
  } else if (numSujet === 4) {
    obj.b = j3pGetRandomInt(3, 7)
    do {
      obj.r = j3pGetRandomInt(3, 7)
    } while (obj.r === obj.b)
    obj.p = obj.b / (obj.b + obj.r)
  } else if (numSujet === 5) {
    obj.p = j3pGetRandomInt(27, 44) / 10
  } else if (numSujet === 6) {
    obj.p = 2 * j3pGetRandomInt(11, 17)
  }
  obj.qdec = ''
  if (numSujet === 2) {
    obj.p1 = '1/6'
    obj.pdec = '1/6'
    obj.pdecVirg = '\\frac{1}{6}'
    obj.qdec = '5/6'
  } else if (numSujet === 4) {
    const lepgcd = j3pPGCD(obj.b, obj.b + obj.r)
    obj.p1 = obj.b / lepgcd + '/' + (obj.b + obj.r) / lepgcd
    obj.pdec = obj.p1
    obj.qdec = obj.r / lepgcd + '/' + (obj.b + obj.r) / lepgcd
    obj.pdecVirg = '\\frac{' + Math.round(obj.b / lepgcd) + '}{' + Math.round((obj.b + obj.r) / lepgcd) + '}'
  } else {
    obj.p1 = j3pVirgule(obj.p)
    obj.pdec = Math.round(obj.p * 10) / 1000
    obj.pdecVirg = j3pVirgule(obj.pdec)
  }
  if (obj.qdec === '') obj.qdec = Math.round((1 - obj.pdec) * 1000) / 1000
  // Nombre de tirages aléatoires
  obj.n = j3pGetRandomInt(10, 20) * 50
  obj.i = j3pGetRandomElt(['i', 'j', 'k'])
  obj.c1 = ds.textes.choix1.split('|')[numSujet - 1]
  obj.c2 = ds.textes.choix2.split('|')[numSujet - 1]
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(me.donneesSection.textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  const stor = me.storage

  // On établit la liste des sujets qui peuvent être donnés au fur et à mesure (évitons les répétitions quand on le peut)
  stor.listeSujetsInit = [1, 2, 3, 4, 5, 6]
  stor.listeSujets = [...stor.listeSujetsInit]
  // Les mots clés python devront être en bleu. On va les définir comme des variables, mais qui seront des spans
  const listeMotsCles = ['for', 'if', 'else', 'in', 'def', 'from', 'import', 'print']
  stor.objDonnees = {}
  listeMotsCles.forEach(mot => {
    stor.objDonnees[mot] = '<span class="blue">' + mot + '</span>'
  })
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  const conteneur = j3pAddElt(stor.elts.divEnonce, 'p')

  // Choix aléatoire d’un sujet
  const pioche = Math.floor(Math.random() * stor.listeSujets.length)
  stor.numSujet = stor.listeSujets[pioche]
  stor.listeSujets.splice(pioche, 1)
  if (stor.listeSujets.length === 0) stor.listeSujets = [...stor.listeSujetsInit]
  // On crée les valeurs utiles pour l’énoncé choisi
  const lesDonnees = genereAlea(me, stor.numSujet)
  for (const prop in lesDonnees) stor.objDonnees[prop] = lesDonnees[prop]
  // on affiche l’énoncé
  j3pEmpty(conteneur)
  for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(conteneur, 'div')
  j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + stor.numSujet], stor.objDonnees)
  j3pAffiche(stor.zoneCons2, '', ds.textes['consigne2_' + stor.numSujet], stor.objDonnees)
  j3pStyle(stor.zoneCons3, { fontStyle: 'italic', fontSize: '0.9em' })
  j3pAddContent(stor.zoneCons3, ds.textes.indic)
  j3pStyle(stor.zoneCons4, { padding: '8px', border: 'solid', fontSize: '0.9em', fontFamily: 'monospace' })
  stor.espace = '&nbsp;'
  for (let i = 1; i < 3; i++) stor.espace += '&nbsp;'
  const lignes = ['£{from} random £{import} random', //, randint',
    '£{for} £{i} £{in} range(@1@):',
    stor.espace + '£{if} @1@:',
    stor.espace + stor.espace + '£{print}(£{c1})',
    stor.espace + '£{else} :',
    stor.espace + stor.espace + '£{print}(£{c2})'
  ]
  stor.lignesCorr = [...lignes]
  // et je modifes les lignes avec les réponses
  stor.lignesCorr[1] = '£{for} £i £{in} range(£n):'
  stor.lignesCorr[2] = stor.espace + '£{if} random() < £{pdec}:'
  stor.zoneInput = []
  // Le programme
  for (let i = 1; i <= lignes.length; i++) stor['ligneProg' + i] = j3pAddElt(stor.zoneCons4, 'div')
  j3pAffiche(stor.ligneProg1, '', lignes[0], stor.objDonnees)
  stor.objDonnees.input1 = { texte: '', dynamique: true }
  const elt1 = j3pAffiche(stor.ligneProg2, '', lignes[1], stor.objDonnees)
  const elt2 = j3pAffiche(stor.ligneProg3, '', lignes[2], stor.objDonnees)
  j3pAffiche(stor.ligneProg4, '', lignes[3], stor.objDonnees)
  j3pAffiche(stor.ligneProg5, '', lignes[4], stor.objDonnees)
  j3pAffiche(stor.ligneProg6, '', lignes[5], stor.objDonnees)
  // fin du programme
  stor.zoneInput.push(elt1.inputList[0], elt2.inputList[0])
  j3pRestriction(stor.zoneInput[0], '0-9,.')
  j3pRestriction(stor.zoneInput[1], 'a-z0-9,.<>=()/\\-')
  const listeRep = ['random()<£{pdec}', '£{qdec}<random()', 'random()>£{qdec}', '£{pdec}>random()'] // Je gère les inégalités larges dans la correction
  elt2.inputList[0].reponse = listeRep.map(monTxt => j3pChaine(monTxt, stor.objDonnees))
  const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
  stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesZonesSaisie })
  j3pFocus(stor.zoneInput[0])
  // et on appelle finEnonce()
  // console.log('stor.objDonnees:', stor.objDonnees, '\n', elt2.inputList[0].reponse)
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // on affiche la correction dans la zone xxx
  const zoneExpli = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { padding: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
  j3pAffiche(zoneExpli1, '', ds.textes.corr1, stor.objDonnees)
  const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
  j3pAddContent(zoneExpli2, ds.textes.corr2)
  const zoneExpli3 = j3pAddElt(zoneExpli, 'div')
  j3pStyle(zoneExpli3, { padding: '8px', border: 'solid', fontSize: '0.9em', fontFamily: 'monospace' })
  stor.lignesCorr.forEach(ligne => {
    const divLigne = j3pAddElt(zoneExpli3, 'div')
    j3pAffiche(divLigne, '', ligne, stor.objDonnees)
  })
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = { aRepondu: false, bonneReponse: false }
  reponse.aRepondu = stor.fctsValid.valideReponses()
  stor.tooMuchRandom = null
  stor.randomAbsent = false
  stor.ecritureASimplifier = false
  stor.tooMuchInegalite = false
  stor.malEcrit = false
  stor.tooMuchSigne = false
  stor.randomPointRandom = false
  if (reponse.aRepondu) {
    // gestion de la première zone qui peut être
    // - un entier (dans ce cas, c’est £n),
    // - deux entiers, (dans ce cas, la différence doit valoir ?n)
    // - ou 3 entiers (dans ce cas, la différence des deux premiers divisée par le 3ème doit valoir £n)
    const rep = stor.fctsValid.zones.reponseSaisie[0]
    const estEntier = /^\d+$/
    const estDeuxEntiers = /^\d+,\d+$/
    const estTroisEntiers = /^\d+,\d+,\d+$/
    stor.simpleRep1 = false
    stor.fctsValid.zones.bonneReponse[stor.fctsValid.zones[0]] = false
    if (estEntier.test(rep)) {
      stor.fctsValid.zones.bonneReponse[0] = Math.abs(stor.objDonnees.n - Number(rep)) < 1e-12
      stor.simpleRep1 = true
    } else if (estDeuxEntiers.test(rep)) {
      const [nb1, nb2] = rep.split(',')
      stor.fctsValid.zones.bonneReponse[0] = Math.abs(Number(nb2) - Number(nb1) - stor.objDonnees.n) < 1e-12
    } else if (estTroisEntiers.test(rep)) {
      const [nb1, nb2, nb3] = rep.split(',')
      stor.fctsValid.zones.bonneReponse[0] = Math.abs(Math.floor((Number(nb2) - Number(nb1)) / Number(nb3)) - stor.objDonnees.n) < 1e-12
    }
    // Gestion de la deuxième zone
    let rep2 = stor.fctsValid.zones.reponseSaisie[1]
    // Si l’élève s’amuse à me mettre des nombres avec des zéros non significatifs, je les vire
    const replaceDecimal = (correspondance, p1) => String(Number(p1))
    rep2 = rep2.replace(/(\d+\.\d+)/g, replaceDecimal)
    const replaceEntier = (correspondance, p1, p2, p3) => {
      if (p1 === '.') return p1 + p2 + p3
      return p1 + String(Number(p2)) + p3
    }
    rep2 = rep2.replace(/([^\d]?)(\d+)([^.]?)/g, replaceEntier)
    // Je vérifie qu’il n’a pas écrit deux fois random ou randint, auquel cas, je lui demande une expression plus simple
    const nbRandint = (rep2.includes('randint(')) ? rep2.split('randint(').length - 1 : 0
    const nbRandom = (rep2.includes('random()')) ? rep2.split('random()').length - 1 : 0
    if (nbRandint + nbRandom > 1) {
      reponse.aRepondu = false
      stor.tooMuchRandom = { nbRandint, nbRandom }
      return null
    }
    // On teste déjà avec les valeurs les plus logiques (que j’ai mises dans les réponses attendues)
    let repOK2 = false
    rep2 = rep2.replace(/([<>])=/g, '$1')
    stor.zoneInput[1].reponse.forEach(rep => {
      repOK2 = (repOK2 || rep2 === rep)
    })
    if (!repOK2) {
      // console.log('rep2:', rep2, rep2.includes('random.random()'))
      if (rep2.includes('random.random()')) {
        // random.random() est présent alors qu’il faut écrire random()
        stor.randomPointRandom = true
        reponse.aRepondu = false
        return null
      }
      if (rep2.includes('random()')) {
        const tabSepIneg = rep2.split(/[<>]/) // tableau des éléments de la réponse séparés par < ou >
        // console.log('tabSepIneg:', tabSepIneg)
        if (!tabSepIneg.includes('random()')) {
          // random() est bien présent mais il n’est pas écrit de manière simple
          stor.ecritureASimplifier = true
          reponse.aRepondu = false
          return null
        }
        const nbIneq = (rep2.includes('>') || rep2.includes('<')) ? rep2.split(/[<>]/).length - 1 : 0
        if (nbIneq > 2 || (nbIneq === 0)) {
          repOK2 = false
          if (nbIneq > 2) stor.tooMuchInegalite = true
        } else if (nbIneq === 1) {
          repOK2 = !/\drandom/.test(rep2)
          if (repOK2) {
            let ecritureRep
            const testEcriture = []
            if (rep2.includes('<')) {
              ecritureRep = rep2.replace('random()', 'x').replace('<', '-(') + ')'
              for (let i = 0; i <= 1; i++) testEcriture.push(stor.zoneInput[1].reponse[i].replace('random()', 'x').replace('<', '-'))
            } else if (rep2.includes('>')) {
              ecritureRep = rep2.replace('random()', 'x').replace('>', '-(') + ')'
              for (let i = 3; i <= 3; i++) testEcriture.push(stor.zoneInput[1].reponse[i].replace('random()', 'x').replace('>', '-'))
            }
            repOK2 = false
            testEcriture.forEach(reponse => {
              const arbreEvalue = new Tarbre(ecritureRep + '-(' + reponse + ')+1', ['x']) // Le +1 est pour garantir l’évaluation d’une expression qui a du sens (sinon, cela donnera tjs 0
              let egalite = true
              for (let i = 1; i <= 10; i++) {
                if (Math.abs(arbreEvalue.evalue([i]) - 1) > Math.pow(10, -10)) egalite = false
              }
              repOK2 = repOK2 || egalite
            })
          } else {
            stor.malEcrit = true
          }
        } else {
          stor.tooMuchSigne = true
          reponse.aRepondu = false
          return null
        }
      } else {
        repOK2 = false
        stor.randomAbsent = true
      }
    }
    stor.fctsValid.zones.bonneReponse[1] = repOK2
    stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
    stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[1])
    if (stor.fctsValid.zones.bonneReponse[0] && stor.fctsValid.zones.bonneReponse[1]) return 1
    else return 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(me.zonesElts.MD)
  const divCorrection = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    if (stor.tooMuchRandom) {
      if (stor.tooMuchRandom.nbRandint > 1) {
        me.reponseManquante(divCorrection, ds.textes.comment2)
      } else if (stor.tooMuchRandom.nbRandom > 1) {
        me.reponseManquante(divCorrection, ds.textes.comment3)
      } else {
        me.reponseManquante(divCorrection, ds.textes.comment4)
      }
    } else if (stor.ecritureASimplifier) {
      me.reponseManquante(divCorrection, ds.textes.comment6)
    } else if (stor.tooMuchSigne) {
      me.reponseManquante(divCorrection, ds.textes.comment9)
    } else if (stor.randomPointRandom) {
      me.reponseManquante(divCorrection, ds.textes.comment10)
    } else {
      me.reponseManquante(divCorrection)
    }
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(divCorrection)
    j3pAddElt(divCorrection, 'br')
    if (stor.randomAbsent) me.reponseKo(divCorrection, ds.textes.comment5, true)
    if (stor.tooMuchInegalite) me.reponseKo(divCorrection, ds.textes.comment7, true)
    if (stor.malEcrit) me.reponseKo(divCorrection, ds.textes.comment8, true)
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(divCorrection)
    j3pAddElt(divCorrection, 'br')
    if (!stor.simpleRep1) j3pAddContent(divCorrection, ds.textes.comment1, true)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(divCorrection, 'br')
        me.reponseKo(divCorrection, null, true)
      }
    } else {
      me.reponseKo(divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
