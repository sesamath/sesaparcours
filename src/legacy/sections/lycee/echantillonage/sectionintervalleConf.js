import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pNombre, j3pPGCD, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { testIntervalleDecimaux } from 'src/lib/utils/regexp'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since janvier 2022
 * @fileOverview Cette section demande un intervalle de confiance
 */

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {
    // Nombre de répétitions de l’exercice
    nbrepetitions: 3,
    nbchances: 2,
    nbetapes: 1,
    indication: '',
    textes: {
      titre_exo: 'Estimation d’une probabilité',
      consigne1_1: 'Avant une élection, un sondage réalisé sur £n personnes révèle que £a d’entre elles pensent voter pour le candidat A.',
      consigne1_2: 'Pour étudier la diversité des espèces de poissons dans une rivière, on effectue un prélèvement de £n poissons. £a d’entre eux sont des truites.',
      consigne1_3: 'On dispose d’une piscine à balles. On prélève au hasard £n balles et £a d’entre elles sont noires.',
      consigne1_4: 'Un sondage réalisé sur un échantillon de £n lycéens français montre que £a d’entre eux pratiquent une activité physique régulière.',
      consigne1_5: 'On soupçonne un joueur d’utiliser une pièce de monnaie non équilibrée. On effectue alors £n lancers de cette pièce et à £a reprises elle tombe sur PILE.',
      consigne1_6: 'Pour évaluer l’évolution d’une épidémie dans la population d’un pays, on teste un échantillon de £n personnes. £a d’entre elles sont atteintes par cette épidémie.',
      consigne2_1: 'Sur l’ensemble des électeurs, dans quel intervalle devrait se situer la probabilité que chacune vote pour le candidat A&nbsp;?',
      consigne2_2: 'Dans quel intervalle devrait se situer la proportion de truites dans cette rivière&nbsp;?',
      consigne2_3: 'Dans quel intervalle devrait se situer la probabilité de tomber sur une balle noire dans le choix au hasard d’une balle de cette piscine&nbsp;?',
      consigne2_4: 'Dans quel intervalle devrait se situer la proportion de lycéens qui pratiquent une activité physique sur l’ensemble des lycéens français&nbsp;?',
      consigne2_5: 'Dans quel intervalle devrait se situer la probabilité de tomber sur PILE quand on joue avec cette pièce&nbsp;?',
      consigne2_6: 'Dans quel intervalle devrait se situer la probabilité qu’une personne choisie au hasard dans ce pays soit atteinte par cette épidémie&nbsp;?',
      indic: 'Les bornes de l’intervalle seront arrondies à 0,001.',
      comment1: 'L’intervalle devrait être fermé&nbsp;!',
      comment2: 'La réponse donnée n’est pas un intervalle&nbsp;!',
      comment3: 'Peut-être est-ce un problème d’arrondi.',
      corr1_1: 'Sur l’échantillon de £a personnes, la fréquence de votants pour la candidat A vaut $f=£{f1}$ et la probabilité qu’un électeur vote pour ce candidat est comprise le plus souvent dans l’intervalle $£g$.',
      corr1_2: 'Sur l’échantillon de £n poissons prélevés, la fréquence de truites vaut $f=£{f1}$. Ainsi la proportion de truites dans cette rivière est comprise le plus souvent dans l’intervalle $£g$.',
      corr1_3: 'Sur l’échantillon de balles prélevées, la fréquence de noires vaut $f=£{f1}$. Ainsi la probabilité de tomber sur une balle noire dans le choix au hasard d’une balle de cette piscine est comprise le plus souvent dans l’intervalle $£g$.',
      corr1_4: 'Sur l’échantillon de lycéens interrogés, la fréquence de ceux qui pratiquent une activité sportive vaut $f=£{f1}$. Ainsi la proportion de lycéens français pratiquant une activité physique est comprise le plus souvent dans l’intervalle $£g$.',
      corr1_5: 'Sur l’échantillon de tirages effectués, on tombe sur PILE avec une fréquence $f=£{f1}$. Ainsi la probabilité de tomber sur PILE avec cette pièce de monnaie est comprise le plus souvent dans l’intervalle $£g$.',
      corr1_6: 'Sur l’échantillon de personnes choisies, la fréquence de celles qui sont atteintes par l’épidémie vaut $f=£{f1}$. Ainsi la probabilité qu’une personne choisie au hasard dans ce pays soit atteinte par l’épidémie est comprise le plus souvent dans l’intervalle $£g$.',
      corr2: 'Donc, en arrondissant les bornes, on obtient l’intervalle $£r$.'
    }
  }
} // getDonnees

/**
 * Retourne les données pour chaque sujet
 * @private
 * @param {Parcours} me
 * @param {Number} numSujet
 * @return {Object}
 */
function genereAlea (me, numSujet) {
  const obj = {}
  // n est la taille de l’échantillon
  // a est le nombre de cas favorables
  // r est l’intervalle solution
  // i1 et i2 les bornes de cet intervalle arrondies à 0,001
  do {
    if (numSujet === 1) {
      obj.n = j3pGetRandomInt(43, 75) * 10
      obj.p = j3pGetRandomInt(210, 250) / 10
    } else if (numSujet === 2) {
      obj.n = j3pGetRandomInt(65, 120)
      obj.p = j3pGetRandomInt(280, 350) / 10
    } else if (numSujet === 3) {
      obj.n = j3pGetRandomInt(110, 140)
      obj.p = j3pGetRandomInt(650, 730) / 10
    } else if (numSujet === 4) {
      obj.n = j3pGetRandomInt(200, 300)
      obj.p = j3pGetRandomInt(450, 610) / 10
    } else if (numSujet === 5) {
      obj.n = j3pGetRandomInt(14, 23) * 10
      obj.p = j3pGetRandomInt(570, 640) / 10
    } else if (numSujet === 6) {
      obj.n = j3pGetRandomInt(180, 270)
      obj.p = j3pGetRandomInt(200, 280) / 10
    }
    obj.a = Math.round(obj.p * obj.n / 100)
    obj.borneInf = obj.a / obj.n - 1 / Math.sqrt(obj.n)
    obj.i1 = Math.floor(obj.borneInf * 1000) / 1000
    obj.borneSup = obj.a / obj.n + 1 / Math.sqrt(obj.n)
    obj.i2 = Math.ceil(obj.borneSup * 1000) / 1000
    // L’arrondi n’est pas toujours le choix pertinent de la borne de l’intervalle.
    // Pour la borne inf, on doit prendre l’arrondi par val inférieure et pour la borne sup celui par val supérieures
    // Donc je m’arrange pour que ces arrondis inf ou sup correspondent à l’arrondi véritable
  } while ((obj.i1 !== Math.round(obj.borneInf * 1000) / 1000) || (obj.i2 !== Math.round(obj.borneSup * 1000) / 1000))
  // Calcul de la fréquence et gestion de l’affichage
  const lepgcd = j3pPGCD(obj.a, obj.n)
  obj.f1 = (lepgcd > 1)
    ? '\\frac{' + obj.a + '}{' + obj.n + '}=\\frac{' + obj.a / lepgcd + '}{' + obj.n / lepgcd + '}'
    : '\\frac{' + obj.a + '}{' + obj.n + '}'
  obj.f = '\\frac{' + obj.a / lepgcd + '}{' + obj.n / lepgcd + '}'
  obj.g = '[' + obj.f + '-\\frac{1}{\\sqrt{' + obj.n + '}};' + obj.f + '+\\frac{1}{\\sqrt{' + obj.n + '}}]'
  obj.r = '[' + j3pVirgule(obj.i1) + ';' + j3pVirgule(obj.i2) + ']'
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(me.donneesSection.textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  const stor = me.storage

  // On établit la liste des sujets qui peuvent être donnés au fur et à mesure (évitons les répétitions quand on le peut)
  stor.listeSujetsInit = [1, 2, 3, 4, 5, 6]
  stor.listeSujets = [...stor.listeSujetsInit]
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  const conteneur = j3pAddElt(stor.elts.divEnonce, 'p')

  // Choix aléatoire d’un sujet
  const pioche = Math.floor(Math.random() * stor.listeSujets.length)
  stor.numSujet = stor.listeSujets[pioche]
  stor.listeSujets.splice(pioche, 1)
  if (stor.listeSujets.length === 0) stor.listeSujets = [...stor.listeSujetsInit]
  // On crée les valeurs utiles pour l’énoncé choisi
  const lesDonnees = genereAlea(me, stor.numSujet)
  stor.objDonnees = {}
  Object.entries(lesDonnees).forEach(([prop, value]) => {
    stor.objDonnees[prop] = value
  })
  // on affiche l’énoncé
  for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(conteneur, 'div')
  j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + stor.numSujet], stor.objDonnees)
  j3pAffiche(stor.zoneCons2, '', ds.textes['consigne2_' + stor.numSujet], stor.objDonnees)
  j3pStyle(stor.zoneCons3, { fontStyle: 'italic', fontSize: '0.9em' })
  j3pAddContent(stor.zoneCons3, ds.textes.indic)
  const elt = j3pAffiche(stor.zoneCons4, '', '&1&', { inputmq: { texte: '' } })
  stor.zoneInput = elt.inputmqList[0]
  stor.zoneInput.typeReponse = ['texte']
  stor.zoneInput.reponse = [stor.objDonnees.r]
  mqRestriction(stor.zoneInput, '\\d,.;[]')
  stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.zoneInput.id] })
  j3pFocus(stor.zoneInput)
  stor.zoneCalc = j3pAddElt(me.zonesElts.MD, 'div', '', { style: { padding: '10px' } })
  stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
  me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]//, left: 600, top: 10 }]
  j3pCreeFenetres(me)
  j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
  j3pAfficheCroixFenetres('Calculatrice')
  // et on appelle finEnonce()
  me.logIfDebug('stor.objDonnees:', stor.objDonnees, '\n', stor.zoneInput.reponse)
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  j3pDetruit(stor.zoneCalc)
  j3pDetruitFenetres('Calculatrice')
  // on affiche la correction dans la zone xxx
  const zoneExpli = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { padding: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
  j3pAffiche(zoneExpli1, '', ds.textes['corr1_' + stor.numSujet], stor.objDonnees)
  const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
  j3pAffiche(zoneExpli2, '', ds.textes.corr2, stor.objDonnees)
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  stor.objErreur = { pbIntervalle: false, pbIntFerme: false, pbArrondi: false }
  const laRepEleve = stor.fctsValid.zones.reponseSaisie[0]
  if (reponse.aRepondu) {
    if (!reponse.bonneReponse) {
      // On cherche la nature de l’erreur
      stor.objErreur.pbIntervalle = !testIntervalleDecimaux.test(laRepEleve)
      if (!stor.objErreur.pbIntervalle) {
        // On vérifie qu’il est fermé
        stor.objErreur.pbIntFerme = (laRepEleve[0] !== '[') || (laRepEleve[laRepEleve.length - 1] !== ']')
        if (!stor.objErreur.pbIntFerme) {
          // On récupère les bornes de l’intervalle :
          let [borneInf, borneSup] = laRepEleve.substring(1, laRepEleve.length - 1).split(';')
          borneInf = j3pNombre(borneInf)
          borneSup = j3pNombre(borneSup)
          // On vérifie si les bornes sont des valeurs approchées à 10^{-2}
          stor.objErreur.pbArrondi = Math.abs(borneInf - stor.objDonnees.i1) < Math.pow(10, -2) && Math.abs(borneSup - stor.objDonnees.i2) < Math.pow(10, -2)
        }
      }
    }
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.zoneD)
  const divCorrection = j3pAddElt(stor.zoneD, 'div')
  // pas de réponse sans limite de temps

  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(divCorrection)
    j3pAddElt(divCorrection, 'br')
    if (stor.objErreur.pbIntervalle) {
      me.reponseKo(divCorrection, ds.textes.comment2, true)
    } else if (stor.objErreur.pbIntFerme) {
      me.reponseKo(divCorrection, ds.textes.comment1, true)
    } else if (stor.objErreur.pbArrondi) {
      me.reponseKo(divCorrection, ds.textes.comment3, true)
    }
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(divCorrection)
    j3pAddElt(divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(divCorrection, 'br')
        me.reponseKo(divCorrection, null, true)
      }
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
