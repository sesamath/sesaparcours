import { j3pAjouteArea, j3pAjouteBouton, j3pAjouteDiv, j3pAjouteTableau, j3pAjouteZoneTexte, j3pArrondi, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pNombre, j3pNombreBienEcrit, j3pRestriction, j3pShowError, j3pValeurde, j3pVirgule } from 'src/legacy/core/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

//
//        JP Vanroyen
//        JUIN 2013
//
//        Section particulière ne renvoyant pas de pe et ne comportant pas de question

// FIXME cette section n’a rien à voir avec les autres et ne fonctionne pas
// elle est pourtant en prod dans sesabibli/32464 et sesacommun/606ab5adfeefd6677be036eb
// ces deux ressources ont été supprimées le 26/06/2024

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['proportion', 0.5, 'number', 'La proportion connue de la population'],
    ['tailleechantillon', 100, 'entier', 'La taille d’un échantillon'],
    ['labelscaractere', ['succès', 'échec'], 'array', 'Les caractères étudiés']
  ]
}

const textes = {
  phrase1: 'Proportion <b><i>p</i></b> dans la population ',
  phrase2: 'Taille <b><i>n</i></b> d’un échantillon ',
  phrase3: 'individu choisi au hasard.',
  phrase4: 'Le résultat est : ',
  phrase5: 'individus choisis au hasard.',
  phrase6: 'Résultat du tirage : ',
  phrase7: 'Pourcentage des fréquences en dehors de l’intervalle ',
  phrase8: 'Fréquence de ',
  phrase8bis: ' par échantillon',
  phrase9: 'Expérience',
  phrase10: 'INTERVALLE DE FLUCTUATION'
}

/**
 * section echantillons
 * @this {Parcours}
 */
export default function main () {
  function uneexperience () {
    stor.proportion = j3pNombre(j3pValeurde('reponse1'))
    const x = Math.random()
    if (x <= stor.proportion) return 0
    return 1
  }

  function uneexperience2 () {
    if (Math.random() <= stor.proportion) return 1
    return 0
  }

  function clicdiv (event) {
    const id = event.target.parentNode.getAttribute('id')
    const num = id.substring(24)
    j3pElement('echantillons_areazone').innerHTML = stor.experiences[num].ch
    j3pElement('conteneuruntirage').innerHTML = textes.phrase9 + ' : ' + (Number(num) + 1)
  }

  function onKeypress (event) {
    if (event.keyCode == 13) { // eslint-disable-line eqeqeq
      let ch = j3pValeurde('reponse3')
      if (j3pNombre(ch) > 2600) {
        j3pElement('reponse3').value = '2000'
        ch = '2000'
      }
      j3pElement('bouton10').value = j3pNombre(ch)
      stor.nombreexp = j3pNombre(ch)
    }
  }

  /**
   * @private
   * @param {string} cas doit valoir plus|moins|normal
   */
  function setLargeurIntervalle (cas) {
    switch (cas) {
      case 'plus':
        stor.largeurintervalle += 0.01
        break
      case 'moins':
        stor.largeurintervalle -= 0.01
        break
      case 'normal':
      default:
        stor.largeurintervalle = 1 / Math.sqrt(stor.tailleechantillon)
        break
    }
    // on le détruit et on le recrée
    j3pDetruit(stor.ctGraphique)
    if (stor.nbexperiences < 11) {
      stor.ctGraphique = j3pDiv('conteneur', { id: 'conteneurgraphique', contenu: '', coord: [270, 200] })
      stor.ctGraphique.style.height = '350px'
    } else {
      stor.ctGraphique = j3pDiv('conteneur', { id: 'conteneurgraphique', contenu: '', coord: [70, 150] })
      stor.ctGraphique.style.height = '400px'
    }
    graphique2(stor.largeurintervalle)
  }

  function tableau (conteneur) {
    function complete (s) {
      if (s.length >= 7) return
      switch (s.length) {
        case 1: s = '&nbsp;&nbsp;&nbsp;' + s + '&nbsp;&nbsp;&nbsp;'; break
        case 2: s = '&nbsp;&nbsp;&nbsp;' + s + '&nbsp;&nbsp;&nbsp;'; break
        case 3: s = '&nbsp;&nbsp;' + s + '&nbsp;&nbsp;'; break
        case 4: s = '&nbsp;&nbsp;' + s + '&nbsp;&nbsp;'; break
        case 5: s = '&nbsp;' + s + '&nbsp;'; break
        case 6: s = '&nbsp;' + s + '&nbsp;'; break
      }
      return s
    }

    j3pAjouteTableau(conteneur, {
      id: conteneur + 'tableau',
      nbli: 3,
      nbcol: 3,
      taille: 18,
      tabid: [[conteneur + 'a11', conteneur + 'a12', conteneur + 'a13'], [conteneur + 'a21', conteneur + 'a22', conteneur + 'a23'], [conteneur + 'a31', conteneur + 'a32', conteneur + 'a33']],
      tabcss: []
    })

    j3pElement(conteneur + 'a11').innerHTML = 'Caractère'
    j3pElement(conteneur + 'a21').innerHTML = 'Effectif'
    j3pElement(conteneur + 'a31').innerHTML = 'Fréquence'
    j3pElement(conteneur + 'a12').innerHTML = complete(me.donneesSection.labelscaractere[0])
    j3pElement(conteneur + 'a13').innerHTML = complete(me.donneesSection.labelscaractere[1])

    j3pElement('conteneura22').innerHTML = stor.succes
    j3pElement('conteneura23').innerHTML = stor.echec
    j3pElement('conteneura32').innerHTML = j3pArrondi(stor.succes / (stor.succes + stor.echec), 3)
    j3pElement('conteneura33').innerHTML = j3pArrondi(stor.echec / (stor.succes + stor.echec), 3)
  } // tableau

  function graphique () {
    if (!stor.boolgraphique) return
    const ord = stor.succes / (stor.succes + stor.echec)
    stor.repere = new Repere({
      idConteneur: 'conteneur',
      idDivRepere: 'conteneurunrepere',
      aimantage: false,
      visible: true,
      trame: true,
      fixe: true,
      larg: 550,
      haut: 350,

      pasdunegraduationX: 1,
      pixelspargraduationX: 30,
      pasdunegraduationY: 0.1,
      pixelspargraduationY: 30,
      xO: 25,
      yO: 320,
      debuty: 0,
      negatifs: false,
      objets: [
        { type: 'point', nom: 'A', par1: 1, par2: ord, fixe: true, visible: true, etiquette: false, style: { couleur: '#F00', epaisseur: 2, taille: 18, taillepoint: 5 } },
        {
          type: 'point',
          nom: textes.phrase8 + me.donneesSection.labelscaractere[0] + textes.phrase8bis,
          par1: 2,
          par2: 0.9,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 0 }
        }

      ]
    })
    stor.repere.construit()
  } // graphique

  function afficherGraphique () {
    stor.boolgraphique = !stor.boolgraphique
    if (stor.boolgraphique) {
      j3pElement('bouton7').value = 'Graphique on'
    } else {
      j3pElement('bouton7').value = 'Graphique off'
    }
    if (!stor.ctGraphique) return
    if (stor.boolgraphique) {
      stor.ctGraphique.style.display = 'block'
    } else {
      stor.ctGraphique.style.display = 'none'
    }
  }

  function graphique2 () {
    if (!stor.boolgraphique) return
    const points = []
    let k
    for (k = 0; k < stor.nbexperiences; k++) {
      points[k] = { x: 0, y: 0 }
    }
    const lesobjets = []
    for (k = 0; k < stor.nbexperiences; k++) {
      points[k].x = k + 1
      points[k].y = stor.experiences[k].succes / (stor.experiences[k].succes + stor.experiences[k].echec)
      let _couleur = '#000'
      if ((points[k].y < stor.proportion - stor.largeurintervalle) || (points[k].y > stor.proportion + stor.largeurintervalle)) {
        _couleur = '#F00'
      }
      if (!stor.intervalle) {
        _couleur = '#000'
      }
      lesobjets.push({ type: 'point', nom: 'A' + k, par1: points[k].x, par2: points[k].y, fixe: true, visible: true, etiquette: false, style: { couleur: _couleur, epaisseur: 2, taille: 18, taillepoint: 4 } })
    }
    const titre = textes.phrase8 + me.donneesSection.labelscaractere[0] + textes.phrase8bis
    let gradx, pasx
    if (stor.nbexperiences > 1050) {
      gradx = 50
      pasx = 200
      lesobjets.push({ type: 'point', nom: titre, par1: 300, par2: 0.9, fixe: true, visible: true, etiquette: true, style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 0 } })
    } else if (stor.nbexperiences > 600) {
      gradx = 30
      pasx = 50
      lesobjets.push({ type: 'point', nom: titre, par1: 300, par2: 0.9, fixe: true, visible: true, etiquette: true, style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 0 } })
    } else if (stor.nbexperiences > 200) {
      gradx = 30
      pasx = 30
      lesobjets.push({ type: 'point', nom: titre, par1: 180, par2: 0.9, fixe: true, visible: true, etiquette: true, style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 0 } })
    } else if (stor.nbexperiences > 100) {
      gradx = 30
      pasx = 10
      lesobjets.push({ type: 'point', nom: titre, par1: 60, par2: 0.9, fixe: true, visible: true, etiquette: true, style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 0 } })
    } else if (stor.nbexperiences > 40) {
      gradx = 30
      pasx = 5
      lesobjets.push({ type: 'point', nom: titre, par1: 30, par2: 0.9, fixe: true, visible: true, etiquette: true, style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 0 } })
    } else if (stor.nbexperiences > 20) {
      gradx = 30
      pasx = 2
      lesobjets.push({ type: 'point', nom: titre, par1: 12, par2: 0.9, fixe: true, visible: true, etiquette: true, style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 0 } })
    } else if (stor.nbexperiences > 10) {
      gradx = 30
      pasx = 1
      lesobjets.push({ type: 'point', nom: titre, par1: 6, par2: 0.9, fixe: true, visible: true, etiquette: true, style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 0 } })
    } else {
      lesobjets.push({ type: 'point', nom: titre, par1: 2, par2: 0.9, fixe: true, visible: true, etiquette: true, style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 0 } })
      gradx = 30
      pasx = 1
    }

    let debutx, debuty, chaine
    if (stor.intervalle) {
      if (!j3pElement('echantillons_intervalle', null)) {
        if (stor.nbexperiences < 11) {
          debutx = 270
          debuty = 565
        } else {
          debutx = 170
          debuty = 565
        }
        chaine = '[' + j3pArrondi(stor.proportion - stor.largeurintervalle, 2) + ';' + j3pArrondi(stor.proportion + stor.largeurintervalle, 2) + ']'
        j3pDiv(me.zones.MG, {
          id: 'echantillons_intervalle',
          contenu: textes.phrase7 + chaine,
          coord: [debutx, debuty],
          style: me.styles.etendre('toutpetit.enonce', { border: '0px solid #000', width: '620px', height: '45px', background: '#ADC9E1', padding: '5px', textAlign: 'right' })
        }
        )
      }
      j3pElement('echantillons_intervalle').style.display = ''

      let compteur = 0
      let freq
      let y1 = stor.proportion - stor.largeurintervalle
      let y2 = stor.proportion + stor.largeurintervalle
      for (let j = 0; j < stor.nbexperiences; j++) {
        freq = stor.experiences[j].succes / (stor.experiences[j].succes + stor.experiences[j].echec)
        if ((freq > y2) || (freq < y1)) compteur++
      }

      if (stor.nbexperiences < 11) {
        debutx = 270
        debuty = 565
      } else {
        debutx = 160
        debuty = 565
        j3pElement('echantillons_intervalle').style.width = '640px'
      }
      chaine = '[' + j3pArrondi(stor.proportion - stor.largeurintervalle, 2) + ';' + j3pArrondi(stor.proportion + stor.largeurintervalle, 2) + ']'
      j3pElement('echantillons_intervalle').style.left = debutx + 'px'
      j3pElement('echantillons_intervalle').style.top = debuty + 'px'
      j3pElement('echantillons_intervalle').innerHTML = textes.phrase7 + chaine + ' : <b>' + j3pArrondi(100 * (compteur / stor.nbexperiences), 2) + '%</b>'

      const aubout = (650 / gradx) * pasx
      y1 = stor.proportion - stor.largeurintervalle
      y2 = stor.proportion + stor.largeurintervalle
      lesobjets.push({ type: 'point', nom: 'S1', par1: 0, par2: y1, fixe: true, visible: false, etiquette: false, style: { couleur: '#006600', epaisseur: 2, taille: 18, taillepoint: 5 } })
      lesobjets.push({ type: 'point', nom: 'S2', par1: aubout, par2: y1, fixe: true, visible: false, etiquette: false, style: { couleur: '#006600', epaisseur: 2, taille: 18, taillepoint: 5 } })
      lesobjets.push({ type: 'segment', nom: 'seg1', par1: 'S1', par2: 'S2', style: { couleur: '#006600', epaisseur: 2 } })
      lesobjets.push({ type: 'point', nom: 'S3', par1: 0, par2: y2, fixe: true, visible: false, etiquette: false, style: { couleur: '#006600', epaisseur: 2, taille: 18, taillepoint: 5 } })
      lesobjets.push({ type: 'point', nom: 'S4', par1: aubout, par2: y2, fixe: true, visible: false, etiquette: false, style: { couleur: '#006600', epaisseur: 2, taille: 18, taillepoint: 5 } })
      lesobjets.push({ type: 'segment', nom: 'seg1', par1: 'S3', par2: 'S4', style: { couleur: '#006600', epaisseur: 2 } })
      lesobjets.push({ type: 'polygone', nom: 'pol1', par1: ['S1', 'S2', 'S4', 'S3'], style: { couleur: '#FA5', couleurRemplissage: '#FA5', opaciteRemplissage: 0.3 } })
    }

    let _larg, _haut, _yO, _pixelspargraduationY
    if (stor.nbexperiences < 11) {
      _larg = 550
      _haut = 350
      _yO = 320
      _pixelspargraduationY = 30
    } else {
      _larg = 750
      _haut = 400
      _yO = 370
      _pixelspargraduationY = 35
    }
    stor.repere = new Repere({
      idConteneur: 'conteneur',
      idDivRepere: 'conteneurunrepere',
      aimantage: false,
      visible: true,
      trame: true,
      fixe: true,
      larg: _larg,
      haut: _haut,

      pasdunegraduationX: pasx,
      pixelspargraduationX: gradx,
      pasdunegraduationY: 0.1,
      pixelspargraduationY: _pixelspargraduationY,
      xO: 25,
      yO: _yO,
      debuty: 0,
      negatifs: false,
      objets: lesobjets
    })
    stor.repere.construit()
    stor.lesobjets = lesobjets
  }

  function voirIntervalle () {
    stor.intervalle = !(stor.intervalle)
    let debutx, debuty
    j3pDetruit('echantillons_intervalle')
    if (stor.nbexperiences < 11) {
      debutx = 270
      debuty = 565
    } else {
      debutx = 170
      debuty = 565
    }
    j3pDiv(me.zones.MG, {
      id: 'echantillons_intervalle',
      contenu: textes.phrase7,
      coord: [debutx, debuty],
      style: me.styles.etendre('toutpetit.enonce', {
        border: '0px solid #000',
        width: '620px',
        height: '45px',
        background: '#ADC9E1',
        padding: '5px',
        textAlign: 'right'
      })
    })
    if (stor.intervalle) {
      j3pElement('bouton6').value = 'Cacher intervalle'
      j3pElement('conteneurboutons2').style.display = ''
    } else {
      j3pElement('bouton6').value = 'Voir intervalle'
      j3pElement('echantillons_intervalle').style.display = 'none'
      j3pElement('conteneurboutons2').style.display = 'none'
    }
    if (stor.intervalle) {
      const y1 = stor.proportion - stor.largeurintervalle
      const y2 = stor.proportion + stor.largeurintervalle
      let gradx, pasx
      if (stor.nbexperiences > 200) {
        gradx = 30
        pasx = 30
      } else if (stor.nbexperiences > 100) {
        gradx = 30
        pasx = 10
      } else if (stor.nbexperiences > 40) {
        gradx = 30
        pasx = 5
      } else if (stor.nbexperiences > 20) {
        gradx = 30
        pasx = 2
      } else if (stor.nbexperiences > 10) {
        gradx = 30
        pasx = 1
      } else {
        gradx = 30
        pasx = 1
      }
      j3pElement('echantillons_intervalle').style.display = ''

      let compteur = 0
      let freq
      for (let j = 0; j < stor.nbexperiences; j++) {
        freq = stor.experiences[j].succes / (stor.experiences[j].succes + stor.experiences[j].echec)
        if ((freq > y2) || (freq < y1)) compteur++
      }
      j3pElement('echantillons_intervalle').innerHTML = textes.phrase7 + '<b>' + j3pArrondi(100 * (compteur / stor.nbexperiences), 2) + '%</b>'
      const aubout = (650 / gradx) * pasx

      stor.lesobjets.push({ type: 'point', nom: 'S1', par1: 0, par2: y1, fixe: true, visible: false, etiquette: false, style: { couleur: '#006600', epaisseur: 2, taille: 18, taillepoint: 5 } })
      stor.lesobjets.push({ type: 'point', nom: 'S2', par1: aubout, par2: y1, fixe: true, visible: false, etiquette: false, style: { couleur: '#006600', epaisseur: 2, taille: 18, taillepoint: 5 } })
      stor.lesobjets.push({ type: 'segment', nom: 'seg1', par1: 'S1', par2: 'S2', style: { couleur: '#006600', epaisseur: 2 } })
      stor.lesobjets.push({ type: 'point', nom: 'S3', par1: 0, par2: y2, fixe: true, visible: false, etiquette: false, style: { couleur: '#006600', epaisseur: 2, taille: 18, taillepoint: 5 } })
      stor.lesobjets.push({ type: 'point', nom: 'S4', par1: aubout, par2: y2, fixe: true, visible: false, etiquette: false, style: { couleur: '#006600', epaisseur: 2, taille: 18, taillepoint: 5 } })
      stor.lesobjets.push({ type: 'segment', nom: 'seg1', par1: 'S3', par2: 'S4', style: { couleur: '#006600', epaisseur: 2 } })
      stor.lesobjets.push({ type: 'polygone', nom: 'pol1', par1: ['S1', 'S2', 'S4', 'S3'], style: { couleur: '#FA5', couleurRemplissage: '#FA5', opaciteRemplissage: 0.3 } })

      let _larg, _haut, _yO, _pixelspargraduationY
      if (stor.nbexperiences < 11) {
        _larg = 550
        _haut = 350
        _yO = 320
        _pixelspargraduationY = 30
      } else {
        _larg = 750
        _haut = 400
        _yO = 370
        _pixelspargraduationY = 35
      }
      stor.repere = new Repere({
        idConteneur: 'conteneurgraphique',
        idDivRepere: 'conteneurunrepere',
        aimantage: false,
        visible: true,
        trame: true,
        fixe: true,
        larg: _larg,
        haut: _haut,

        pasdunegraduationX: pasx,
        pixelspargraduationX: gradx,
        pasdunegraduationY: 0.1,
        pixelspargraduationY: _pixelspargraduationY,
        xO: 25,
        yO: _yO,
        debuty: 0,
        negatifs: false,
        objets: stor.lesobjets
      })
      stor.repere.construit()
    } else {
      j3pDetruit(stor.ctGraphique)
      if (stor.nbexperiences < 11) {
        stor.ctGraphique = j3pDiv('conteneur', { id: 'conteneurgraphique', contenu: '', coord: [270, 200] })
        stor.ctGraphique.style.height = '350px'
      } else {
        stor.ctGraphique = j3pDiv('conteneur', { id: 'conteneurgraphique', contenu: '', coord: [70, 150] })
        stor.ctGraphique.style.height = '400px'
      }

      graphique2(stor.largeurintervalle)
    }
  }

  function nTirages () {
    stor.etat = 'ntirages'
    const sauve = stor.boolgraphique
    reset()
    stor.boolgraphique = sauve

    j3pEmpty('conteneur')

    if (!j3pElement('echantillons_area', null)) {
      j3pDiv(me.zones.MG, { id: 'echantillons_area', contenu: '', coord: [400, 175] })
      j3pAjouteArea('echantillons_area', { id: 'echantillons_areazone', cols: 55, rows: 2, readonly: false, taillepolice: 12, police: 'Arial', couleurpolice: '#003366' })
    }
    let ch = ''

    stor.tailleechantillon = j3pNombre(j3pValeurde('reponse2'))
    const debutx = 20; const debuty = 170
    j3pDiv('conteneur', {
      id: 'conteneuruntirage',
      contenu: stor.tailleechantillon + ' ' + textes.phrase5 + '<br>' + textes.phrase6,
      coord: [debutx, debuty],
      style: me.styles.etendre('petit.enonce', { border: '0px solid #000', width: '500px', height: '50px', background: '', padding: '5px', textAlign: 'right' })
    }
    )

    let succes = 0
    stor.succes = 0
    stor.echec = 0
    for (let k = 0; k < stor.tailleechantillon; k++) {
      const uneexp = uneexperience()

      succes += (uneexp === 0) ? 1 : 0
      stor.succes += (uneexp === 0) ? 1 : 0
      stor.echec += 1 - ((uneexp === 0) ? 1 : 0)
      ch += me.donneesSection.labelscaractere[uneexp] + ';'
    }

    j3pElement('conteneuruntirage').innerHTML += '<b>' + succes + '</b> ' + me.donneesSection.labelscaractere[0]

    j3pElement('echantillons_areazone').innerHTML = ch

    stor.experiences[stor.nbexperiences].succes = stor.succes
    stor.experiences[stor.nbexperiences].echec = stor.echec
    stor.experiences[stor.nbexperiences].ch = stor.ch

    j3pDiv('conteneur', { id: 'conteneurtableaux', contenu: '', coord: [30, 350], style: { width: '200px', height: '200px' } })
    j3pDiv('conteneurtableaux', { id: 'conteneurtableau' + stor.nbexperiences, contenu: '', coord: [0, 0] })
    tableau('conteneurtableau' + stor.nbexperiences)

    j3pDiv('conteneur', { id: 'conteneurgraphique', contenu: '', coord: [290, 250] })
    if (stor.nbexperiences < 11) {
      stor.ctGraphique.style.height = '350px'
    } else {
      stor.ctGraphique.style.height = '400px'
    }
    graphique('conteneurgraphique')
  }

  function doExperiences () {
    if (stor.etat === 'ntirages') {
      const sauve = stor.boolgraphique

      reset()
      stor.boolgraphique = sauve
    }

    stor.etat = 'none'
    // init à false si on passe undefined et true sinon
    j3pElement('bouton6').style.display = ''
    if (!j3pElement('echantillons_area', null)) {
      if (stor.nbexperiences < 10) {
        j3pDiv(me.zones.MG, { id: 'echantillons_area', contenu: '', coord: [330, 157] })
        j3pAjouteArea('echantillons_area', { id: 'echantillons_areazone', cols: 60, rows: 1, readonly: false, taillepolice: 12, police: 'Arial', couleurpolice: '#003366' })
      }
    } else
      if (stor.nbexperiences >= 10) {
        j3pDetruit('echantillons_area')
      }
    stor.tailleechantillon = j3pNombre(j3pValeurde('reponse2'))
    j3pDetruit('conteneuruntirage')
    if (stor.nbexperiences < 10) {
      const debutx = 160; const debuty = 155
      j3pDiv('conteneur', {
        id: 'conteneuruntirage',
        contenu: textes.phrase9 + ' ' + (stor.nbexperiences + 1),
        coord: [debutx, debuty],
        style: me.styles.etendre('petit.enonce', { border: '0px solid #000', width: '500px', height: '50px', background: '', padding: '5px', textAlign: 'right' })
      }
      )
    }
    let ch = ''
    stor.succes = 0
    stor.echec = 0
    let uneexp
    for (let k = 0; k < stor.tailleechantillon; k++) {
      uneexp = uneexperience()
      stor.succes += (uneexp === 0) ? 1 : 0
      stor.echec += 1 - ((uneexp === 0) ? 1 : 0)
      ch += me.donneesSection.labelscaractere[uneexp] + ';'
    }

    stor.ch = ch
    if (stor.nbexperiences < 10) j3pElement('echantillons_areazone').innerHTML = ch

    stor.experiences[stor.nbexperiences].succes = stor.succes
    stor.experiences[stor.nbexperiences].echec = stor.echec
    stor.experiences[stor.nbexperiences].ch = stor.ch

    if (stor.nbexperiences < 10) {
      if (!j3pElement('conteneurtableaux', null)) {
        j3pDiv('conteneur', { id: 'conteneurtableaux', contenu: '', coord: [10, 200], style: { border: '1px black solid', width: '250px', height: '400px' } })
        j3pElement('conteneurtableaux').style.overflow = 'auto'
        j3pElement('conteneurtableaux').style.padding = '10px'
      }
    } else {
      j3pDetruit('conteneurtableaux')
    }
    if (stor.nbexperiences < 10) {
      j3pDiv('conteneurtableaux', { id: 'conteneurtableauintitule' + stor.nbexperiences, contenu: '<b>' + textes.phrase9 + ' ' + (stor.nbexperiences + 1) + '</b>' })
      j3pElement('conteneurtableauintitule' + stor.nbexperiences).addEventListener('click', clicdiv, false)
      j3pDiv('conteneurtableaux', { id: 'conteneurtableau' + stor.nbexperiences, contenu: '' })
      tableau('conteneurtableau' + stor.nbexperiences)
    }
    stor.nbexperiences++

    j3pDetruit(stor.ctGraphique)
    if (stor.nbexperiences < 11) {
      stor.ctGraphique = j3pDiv('conteneur', { id: 'conteneurgraphique', contenu: '', coord: [270, 200] })
      stor.ctGraphique.style.height = '350px'
    } else {
      stor.ctGraphique = j3pDiv('conteneur', { id: 'conteneurgraphique', contenu: '', coord: [70, 150] })
      stor.ctGraphique.style.height = '400px'
    }
    graphique2(stor.largeurintervalle)
    if (stor.intervalle) j3pElement('conteneurboutons2').style.display = 'block'
  }

  function doExperiencesPlusVite () {
    if (stor.etat === 'ntirages') {
      reset()
    }
    stor.etat = 'none'
    if (stor.nbexperiences === 0) {
      doExperiences()
    }
    if (stor.nbexperiences > 420) {
      return j3pShowError('Nombre maximal d’expériences atteint dans ce mode', { vanishAfter: 5 })
    }
    j3pElement('bouton6').style.display = ''
    j3pDetruit('echantillons_area')
    stor.tailleechantillon = j3pNombre(j3pValeurde('reponse2'))
    j3pDetruit('conteneuruntirage')

    for (let j = 0; j < 10; j++) {
      let ch = ''
      stor.succes = 0
      stor.echec = 0
      for (let k = 0; k < stor.tailleechantillon; k++) {
        const uneexp = uneexperience()
        stor.succes += (uneexp === 0) ? 1 : 0
        stor.echec += 1 - ((uneexp === 0) ? 1 : 0)
        ch += me.donneesSection.labelscaractere[uneexp] + ';'
      }
      stor.ch = ch
      stor.experiences[stor.nbexperiences + j].succes = stor.succes
      stor.experiences[stor.nbexperiences + j].echec = stor.echec
      stor.experiences[stor.nbexperiences + j].ch = stor.ch
    }
    j3pDetruit('conteneurtableaux')
    stor.nbexperiences += 10
    j3pDetruit(stor.ctGraphique)
    if (stor.nbexperiences < 11) {
      stor.ctGraphique = j3pDiv('conteneur', { id: 'conteneurgraphique', contenu: '', coord: [270, 200] })
      stor.ctGraphique.style.height = '350px'
    } else {
      stor.ctGraphique = j3pDiv('conteneur', { id: 'conteneurgraphique', contenu: '', coord: [70, 150] })
      stor.ctGraphique.style.height = '400px'
    }
    graphique2(stor.largeurintervalle)
    if (stor.intervalle) { j3pElement('conteneurboutons2').style.display = 'block' }
  }

  function _1000experiences () {
    reset()
    j3pDetruit('conteneurunrepere2')
    j3pDetruit(stor.ctGraphique)
    stor.ctGraphique = null
    stor.proportion = j3pNombre(j3pValeurde('reponse1'))
    stor.tailleechantillon = j3pNombre(j3pValeurde('reponse2'))
    stor.intervalle = true
    const nb = stor.nombreexp
    let uneexp = 0
    for (let j = 0; j < nb; j++) {
      for (let k = 0; k < stor.tailleechantillon; k++) {
        uneexp = uneexperience2()
        stor.experiences[stor.nbexperiences + j].succes += uneexp
      }
      stor.experiences[stor.nbexperiences + j].echec = stor.tailleechantillon - stor.experiences[stor.nbexperiences + j].succes
    }

    stor.nbexperiences = nb
    stor.ctGraphique = j3pDiv('conteneur', { id: 'conteneurgraphique', contenu: '', coord: [70, 150] })
    if (stor.nbexperiences < 11) {
      stor.ctGraphique.style.height = '350px'
    } else {
      stor.ctGraphique.style.height = '400px'
    }
    stor.boolgraphique = true
    graphique2(stor.largeurintervalle)
    j3pElement('bouton11').style.display = ''

    stor.intervalle = true
    j3pElement('conteneurboutons2').style.display = ''
  }

  function repartition () {
    const tab = []
    let k
    for (k = 0; k < stor.tailleechantillon + 1; k++) {
      tab[k] = 0
    }
    let succes
    for (k = 0; k < stor.nombreexp; k++) {
      succes = stor.experiences[k].succes
      tab[succes]++
    }

    let max = -1
    for (k = 0; k < stor.nombreexp; k++) {
      if (tab[k] > max) max = tab[k]
    }

    j3pDetruit('conteneurgraphique')
    stor.ctGraphique = null
    j3pDetruit('echantillons_intervalle')

    stor.proportion = j3pNombre(j3pValeurde('reponse1'))
    stor.tailleechantillon = j3pNombre(j3pValeurde('reponse2'))
    const np = Math.round(stor.proportion * stor.tailleechantillon)
    const pasx = Math.round(np / (400 / 30))
    const pasy = Math.round(max / (200 / 20))

    const lesobjets = []
    for (k = 0; k < stor.tailleechantillon + 1; k++) {
      if (tab[k] !== 0) {
        lesobjets.push({ type: 'point', nom: 'A' + k + '1', par1: k, par2: 0, fixe: true, visible: true, etiquette: false, style: { couleur: '#456', epaisseur: 2, taille: 18, taillepoint: 0 } })
        lesobjets.push({ type: 'point', nom: 'B' + k + '1', par1: k, par2: tab[k], fixe: true, visible: true, etiquette: false, style: { couleur: '#456', epaisseur: 2, taille: 18, taillepoint: 0 } })
        lesobjets.push({ type: 'segment', nom: 's' + k, par1: 'A' + k + '1', par2: 'B' + k + '1', style: { couleur: '#F00', epaisseur: 2 } })
      }
    }
    stor.repere = new Repere({
      idConteneur: 'conteneur',
      idDivRepere: 'conteneurunrepere2',
      aimantage: false,
      visible: true,
      trame: true,
      fixe: true,
      larg: 800,
      haut: 450,

      pasdunegraduationX: pasx,
      pixelspargraduationX: 30,
      pasdunegraduationY: pasy,
      pixelspargraduationY: 30,
      xO: 25,
      yO: 420,
      debuty: 0,
      negatifs: false,
      objets: lesobjets
    })
    stor.repere.construit()
    j3pElement('bouton11').style.display = 'none'
    j3pElement('conteneurboutons2').style.display = 'none'
  }

  function unTirage (sorte) {
    stor.etat = 'ntirages'
    j3pDetruit('echantillons_intervalle')
    j3pDetruit('echantillons_area')
    j3pEmpty('conteneur')
    if (!j3pElement('echantillons_area', null)) {
      j3pDiv('conteneur', { id: 'echantillons_area', contenu: '', coord: [370, 200] })
      j3pAjouteArea('echantillons_area', { id: 'echantillons_areazone', cols: 51, rows: 5, readonly: false, taillepolice: 14, police: 'Arial', couleurpolice: '#003366' })
    }

    const uneexp = uneexperience()

    const debutx = 50; const debuty = 200
    j3pDiv('conteneur', {
      id: 'conteneuruntirage',
      contenu: '1 ' + textes.phrase3 + '<br>' + textes.phrase4 + '<b>' + me.donneesSection.labelscaractere[uneexp] + '</b>',
      coord: [debutx, debuty],
      style: me.styles.etendre('petit.enonce', { border: '0px solid #000', width: '500px', height: '50px', background: '', padding: '5px', textAlign: 'right' })
    }
    )
    if (sorte === 'noncumul') {
      me.stockage[1] = ''
      stor.succes = (uneexp === 0) ? 1 : 0
      stor.echec = 1 - stor.succes
      j3pElement('echantillons_areazone').innerHTML = me.donneesSection.labelscaractere[uneexp] + ';'
    } else {
      stor.succes += (uneexp === 0) ? 1 : 0
      stor.echec += 1 - ((uneexp === 0) ? 1 : 0)
      j3pElement('echantillons_areazone').innerHTML = me.stockage[1] + me.donneesSection.labelscaractere[uneexp] + ';'
      me.stockage[1] = j3pElement('echantillons_areazone').innerHTML
    }

    j3pDiv('conteneur', { id: 'conteneurtableau', contenu: '', coord: [50, 300] })
    tableau('conteneurtableau')
    j3pElement('conteneurboutons2').style.display = 'none'
  }

  function reset () {
    j3pElement('conteneurboutons2').style.display = 'none'
    stor.largeurintervalle = 1 / Math.sqrt(stor.tailleechantillon)
    j3pDetruit('conteneurunrepere2')
    const id = 'conteneurtableau'
    if (j3pElement(id, null)) {
      j3pElement(id + 'a22').innerHTML = ''
      j3pElement(id + 'a23').innerHTML = ''
    }

    j3pDetruit('echantillons_area')
    j3pDetruit('conteneurgraphique')
    if (j3pElement('echantillons_areazone', null)) {
      j3pElement('echantillons_areazone').innerHTML = ''
    }

    if (j3pElement('conteneuruntirage', null)) {
      j3pElement('conteneuruntirage').innerHTML = ''
    }
    j3pDetruit('conteneurtableau')
    j3pDetruit('conteneurtableaux')
    j3pDetruit('echantillons_intervalle')

    j3pElement('bouton6').style.display = 'none'
    stor.boolgraphique = true

    j3pDetruit('conteneuruntirage')
    j3pDetruit('conteneurtableaux')
    j3pDetruit('conteneurtableautableau')
    me.stockage[1] = ''
    stor.succes = 0
    stor.echec = 0

    stor.nbexperiences = 0
    // [{succes:nb1,echec:nb2,ch:"pile;pile;face;..."]

    for (let k = 0; k < 2600; k++) {
      stor.experiences[k] = { succes: 0, echec: 0, ch: '' }
    }

    stor.intervalle = false
    j3pElement('bouton6').value = 'Voir intervalle'
    j3pElement('bouton11').style.display = 'none'
  }

  const me = this
  const stor = this.storage
  stor.proportion = 0
  stor.tailleechantillon = 0
  stor.succes = 0
  stor.echec = 0
  stor.boolgraphique = true
  stor.etat = 'none'// || "ntirages"

  stor.nombreexp = 1000

  stor.largeurintervalle = 0

  stor.experiences = []
  stor.nbexperiences = 0
  // [{succes:nb1,echec:nb2,ch:"pile;pile;face;..."]
  stor.lesobjets = []
  for (let k = 0; k < 2600; k++) {
    stor.experiences[k] = { succes: 0, echec: 0, ch: '' }
  }
  stor.intervalle = false

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.validOnEnter = false // ex donneesSection.touche_entree
        this.construitStructurePage('presentation3')
        this.stockage = [0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0]
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        this.score = 0
        stor.proportion = me.donneesSection.proportion
        stor.tailleechantillon = me.donneesSection.tailleechantillon
        stor.largeurintervalle = 1 / Math.sqrt(stor.tailleechantillon)
        j3pElement(this.zones.MG).style.backgroundColor = '#D1E1EF'
      } else {
        this.videLesZones()
      }

      j3pDiv(this.zones.MG, { id: 'conteneur', contenu: '', coord: [10, 10] })
      var ctBt = j3pDiv(this.zones.MG, { id: 'conteneurboutons', contenu: '', coord: [10, 620], style: { border: '0px solid #000', width: '880px', height: '50px', background: '#0069D1', padding: '5px', textAlign: 'right' } })
      ctBt.setAttribute('class', 'boiteombre1')
      ctBt.style.backgroundImage = '-moz-linear-gradient(left, #0075EB, #1F8FFF) '
      ctBt.style.backgroundImage = '-webkit-linear-gradient(left, #0075EB, #52A8FF)'
      ctBt.style.backgroundImage = '-o-radial-linear(left, #0075EB, #52A8FF)'
      ctBt.style.backgroundImage = 'radial-linear(left, #0075EB, #52A8FF)'

      var ctBt2 = j3pDiv(this.zones.MG, { id: 'conteneurboutons2', contenu: '', coord: [850, 300], style: { border: '0px solid #000', width: '40px', height: '105px', background: '#0069D1', padding: '5px', textAlign: 'right' } })
      ctBt2.setAttribute('class', 'boiteombre1')
      ctBt2.style.backgroundImage = '-moz-linear-gradient(left, #0075EB, #1F8FFF) '
      ctBt2.style.backgroundImage = '-webkit-linear-gradient(left, #0075EB, #52A8FF)'
      ctBt2.style.backgroundImage = '-o-radial-linear(left, #0075EB, #52A8FF)'
      ctBt2.style.backgroundImage = 'radial-linear(left, #0075EB, #52A8FF)'
      j3pAjouteBouton(ctBt2, 'boutonplus', 'MepBoutonsDG', '+', setLargeurIntervalle.bind(null, 'plus'))
      j3pAjouteBouton(ctBt2, 'boutonmoins', 'MepBoutonsDG', '-', setLargeurIntervalle.bind(null, 'moins'))
      j3pAjouteBouton(ctBt2, 'boutonnormal', 'MepBoutonsDG', 'R', setLargeurIntervalle.bind(null, 'normal'))
      j3pElement('boutonplus').style.padding = '3px 5px'
      j3pElement('boutonmoins').style.padding = '3px 7px'
      j3pElement('boutonnormal').style.padding = '3px 5px'
      ctBt2.style.display = 'none'

      j3pAjouteBouton('conteneurboutons', 'bouton1', 'MepBoutonsDG', 'RESET', reset)
      j3pAjouteBouton('conteneurboutons', 'bouton7', 'MepBoutonsDG', 'Graphique on', afficherGraphique)
      j3pAjouteBouton('conteneurboutons', 'bouton2', 'MepBoutonsDG', 'Un tirage', unTirage.bind(null, 'noncumul'))
      j3pAjouteBouton('conteneurboutons', 'bouton3', 'MepBoutonsDG', 'Répéter', unTirage.bind(null, 'cumul'))
      j3pAjouteBouton('conteneurboutons', 'bouton4', 'MepBoutonsDG', 'Effectuer n tirages', nTirages)
      j3pAjouteBouton('conteneurboutons', 'bouton5', 'MepBoutonsDG', 'Répéter expériences', doExperiences)
      j3pAjouteBouton('conteneurboutons', 'bouton8', 'MepBoutonsDG', 'Plus vite', doExperiencesPlusVite)
      j3pAjouteBouton('conteneurboutons', 'bouton6', 'MepBoutonsDG', 'Voir intervalle', voirIntervalle)
      j3pAjouteZoneTexte('conteneurboutons', { id: 'reponse3', maxchars: '5', texte: '1000', tailletexte: 14, width: 40 })
      j3pElement('reponse3').addEventListener('keypress', onKeypress, false)
      j3pAjouteBouton('conteneurboutons', 'bouton10', 'MepBoutonsDG', '1000', _1000experiences)
      j3pAjouteBouton('conteneurboutons', 'bouton11', 'MepBoutonsDG', 'Répartition', repartition)

      if (!j3pElement('conteneurgraphique', null)) {
        j3pElement('bouton6').style.display = 'none'
      }
      j3pElement('bouton11').style.display = 'none'
      var debutx = 240
      var debuty = 10
      var div = j3pDiv(this.zones.MG, {
        id: 'echantillons_titre',
        contenu: textes.phrase10,
        coord: [debutx, debuty],
        style: this.styles.etendre('grand.enonce', { fontSize: '28px', border: '0px solid #000', color: '#FFF', width: '450px', height: '55px', background: '#0075EB', padding: '5px', textAlign: 'right' })
      }
      )
      div.style.textAlign = 'center'
      div.style.backgroundImage = '-moz-radial-gradient(30deg, #0075EB, #1F8FFF) '
      div.style.backgroundImage = '-webkit-radial-gradient(30deg, #0075EB, #52A8FF)'
      div.style.backgroundImage = '-o-radial-gradient(30deg, #0075EB, #52A8FF)'
      div.style.backgroundImage = 'radial-gradient(30deg, #0075EB, #52A8FF)'
      div.style.textShadow = '1px 1px 1px #000'
      div.setAttribute('class', 'boiteombre1')

      j3pElement(this.zones.MG).style.backgroundImage = '-moz-linear-gradient(left, #BFD5E8, #D2E1EF) '

      debutx = 270
      debuty = 565
      j3pDiv(this.zones.MG, {
        id: 'echantillons_intervalle',
        contenu: textes.phrase7,
        coord: [debutx, debuty],
        style: this.styles.etendre('toutpetit.enonce', { border: '0px solid #000', width: '620px', height: '45px', background: '#ADC9E1', padding: '5px', textAlign: 'right' })
      }
      )
      j3pElement('echantillons_intervalle').style.display = 'none'

      debutx = 290
      debuty = 75
      j3pDiv(this.zones.MG, {
        id: 'echantillons_p',
        contenu: textes.phrase1,
        coord: [debutx, debuty],
        style: this.styles.etendre('toutpetit.enonce', { border: '1px solid #f0f0f0', width: '270px', height: '40px', background: '#ADC9E1', padding: '5px', textAlign: 'right' })
      })
      j3pElement('echantillons_p').style.textAlign = 'right'

      j3pAjouteZoneTexte(this.zones.MG, { id: 'reponse1', maxchars: '4', restrict: /[0-9,]/, texte: j3pVirgule(this.donneesSection.proportion), tailletexte: 22, width: 50, top: debuty + 2, left: 275 + debutx, textAlign: 'right' })
      var debut1x = 290; var debut1y = 115
      j3pDiv(this.zones.MG, {
        id: 'echantillons_taille',
        contenu: textes.phrase2,
        coord: [debut1x, debut1y],
        style: this.styles.etendre('toutpetit.enonce', { border: '1px solid #f0f0f0', width: '270px', height: '40px', background: '#ADC9E1', padding: '5px' })
      }
      ); j3pElement('echantillons_taille').style.textAlign = 'right'
      j3pAjouteZoneTexte(this.zones.MG, { id: 'reponse2', maxchars: '4', restrict: /[0-9,]/, texte: j3pVirgule(this.donneesSection.tailleechantillon), tailletexte: 22, width: 60, top: debut1y + 2, left: 275 + debut1x })

      j3pRestriction('reponse1', '0-9,')
      j3pFocus('reponse1')

      j3pAjouteDiv(this.zones.MG, 'explications', '', { style: 'color:#FF4000;position:absolute;top:300px;left:50px;font-size:28px' })

      this.finEnonce()
      // pourquoi cacher ce bouton ?
      this.cacheBoutonValider()
      break // case "enonce":

    case 'correction':

      // On teste si une réponse a été saisie
      var repEleve = j3pValeurde('reponse1')
      if ((repEleve === '') && (!this.isElapsed)) {
        this.reponseManquante('explications')
        j3pFocus('reponse1')
        this.afficheBoutonValider()
      } else {
        // Une réponse a été saisie
        if (repEleve === j3pNombreBienEcrit(this.stockage[0]) || repEleve === String(this.stockage[0])) {
          // Bonne réponse
          this._stopTimer()
          this.score++
          j3pElement('explications').style.color = this.styles.cbien
          j3pElement('explications').innerHTML = cBien

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          j3pElement('explications').style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            this._stopTimer()

            j3pElement('explications').innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()
            j3pElement('explications').innerHTML += '<br>La solution était ' + this.stockage[0]

            // RECOPIER LA CORRECTION ICI !

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            j3pElement('explications').innerHTML = cFaux

            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
            if (this.essaiCourant < this.donneesSection.nbchances) {
              j3pElement('explications').innerHTML += '<br>' + essaieEncore
              this.typederreurs[1]++
              // indication éventuelle ici
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()
              j3pElement('explications').innerHTML += '<br>' + regardeCorrection
              j3pElement('explications').innerHTML += '<br>La solution était ' + this.stockage[0]
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      this.finNavigation()

      break // case "navigation":
  }
}
