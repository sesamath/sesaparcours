import { j3pNombre, j3pShuffle, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pAddElt, j3pStyle, j3pGetNewId, j3pEmpty } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import tableauSignesVariations from 'src/legacy/outils/tableauSignesVariations'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2020
        Déterminer du nombre de solution d’une équation à l’aide du tableau de variation
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['continuite', true, 'boolean', 'Lorsque ce paramètre est à vrai, on parle de continuité de la fonction']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'Problème sur la détermination du nombre de solutions' },
    { pe_3: 'Problème dans la détermination du ou des intervalle(s)' },
    { pe_4: 'Insuffisant' }
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Équation à l’aide d’un tableau de variation',
  // on donne les phrases de la consigne
  consigne1_1: 'Soit $£f$ la fonction dont on donne le tableau de variations ci-dessous&nbsp;:',
  consigne1_2: 'Soit $£f$ une fonction continue sur $£i$ et dont on donne le tableau de variations ci-dessous&nbsp;:',
  consigne2: 'D’après ce tableau de variations, l’équation $£e$ admet @1@ solution(s).',
  consigne3_1: 'L’équation $£e$ admet une solution.',
  consigne3_2: 'L’équation $£e$ admet deux solutions.',
  consigne4_1: 'Préciser le plus précisément possible l’intervalle dans lequel se situe cette solution.',
  consigne4_2: 'Préciser le plus précisément possible les intervalles dans lesquels se situent ces solutions.',
  consigne5_1: 'La plus petite des deux&nbsp;:&1&',
  consigne5_2: 'La plus grande des deux&nbsp;:&1&',
  rmq: 'Il n’y a qu’une seule possibilité pour répondre.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Attention à l’écriture d’un intervalle&nbsp;!',
  comment2: 'Le séparateur dans un intervalle doit être un point-virgule&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Dans le tableau de variations, il faut regarder les ordonnées atteintes par la fonction $£f$.',
  corr2_1: 'L’équation admet ainsi une seule solution.',
  corr2_2: 'L’équation admet ainsi deux solutions.',
  corr3_1: 'En complétant le tableau de variation, on vérifie que la solution $x_1$ est dans l’intervalle $£i$ (on acceptait l’intervalle fermé).',
  corr3_2: 'En complétant le tableau de variation, on vérifie que les solutions $x_1$ et $x_2$ sont respectivement dans les intervalles $£i$ et $£j$ (on acceptait les intervalles fermés).'
}

/**
 * section equationContinuite
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    if (me.questionCourante % ds.nbetapes === 1) {
      j3pAffiche(stor.zoneExpli1, '', textes.corr1, { f: stor.nomf })
      j3pAffiche(stor.zoneExpli2, '', (stor.objDonnees.paramEq.tabSol.length === 1) ? textes.corr2_1 : textes.corr2_2)
    } else {
      j3pDetruit(stor.zoneCons7)
      if (stor.objDonnees.paramEq.tabSol.length === 1) {
        j3pAffiche(stor.zoneExpli1, '', textes.corr3_1, {
          i: stor.tabReponses[0]
        })
      } else {
        j3pAffiche(stor.zoneExpli1, '', textes.corr3_2, {
          i: stor.tabReponses[0],
          j: stor.tabReponses[1]
        })
      }
      genereTableau(stor.zoneExpli2, stor.objDonnees, { correction: true, couleur: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    }
  }
  function calculDonneesTab () {
    // ValX contiendra les abscisses et valY les ordonnées
    // tabVariations contient la monotonie ('croit' ou 'decroit' sur chaque intervalle)
    // nb est le nombre pour lequel on cherche à résoudre f(x)=nb
    const valX = []
    const valY = []
    let tabVariations
    const casFigure = stor.tabCasFigure[(me.questionCourante - 1) / ds.nbetapes]
    const paramEq = {}// c’est un objet qui contient nb (issu de l’équation f(x)=nb), et tabSol : des tableaux de la forme [a,b] où a et b sont les bornes de l’intervalle où se trouve la solution
    const nbsol = j3pGetRandomInt(1, 2)
    paramEq.tabSol = []
    paramEq.nb = j3pGetRandomInt(-4, 4)
    // Je prépare le tableau utilisé lors de la correction
    let valXcorr
    let valYcorr
    let valeursX // ce sont les valeur numériques. Pour x_1 et x_2, je prendrai la moyenne des bornes, ce qui permettra de les placer
    if (casFigure === 2) {
      // 2 intervalles
      valX.push(-j3pGetRandomInt(3, 10))
      valX.push(valX[0] + j3pGetRandomInt(3, 7))
      valX.push(valX[1] + j3pGetRandomInt(3, 7))
      if (j3pGetRandomInt(0, 1) === 1) {
        if (j3pGetRandomInt(0, 1)) {
          // l’unique solution est sur le premier intervalle
          valY.push(paramEq.nb + j3pGetRandomInt(4, 7))
          valY.push(paramEq.nb - j3pGetRandomInt(5, 8))
          paramEq.tabSol.push([valX[0], valX[1]])
          if (nbsol === 1) {
            valY.push(j3pGetRandomInt(valY[1] + 1, paramEq.nb - 1))
            valXcorr = [valX[0], 'x_1', valX[1], valX[2]]
            valeursX = [valX[0], (valX[0] + valX[1]) / 2, valX[1], valX[2]]
            valYcorr = [valY[0], paramEq.nb, valY[1], valY[2]]
          } else {
            valY.push(j3pGetRandomInt(paramEq.nb + 1, paramEq.nb + 7))
            paramEq.tabSol.push([valX[1], valX[2]])
            valXcorr = [valX[0], 'x_1', valX[1], 'x_2', valX[2]]
            valeursX = [valX[0], (valX[0] + valX[1]) / 2, valX[1], (valX[1] + valX[2]) / 2, valX[2]]
            valYcorr = [valY[0], paramEq.nb, valY[1], paramEq.nb, valY[2]]
          }
        } else {
          // l’unique solution est sur le deuxième intervalle
          if (nbsol === 1) {
            valY.push(paramEq.nb - j3pGetRandomInt(4, 7))
            valY.push(valY[0] - j3pGetRandomInt(5, 8))
            valY.push(j3pGetRandomInt(paramEq.nb + 2, paramEq.nb + 7))
            valXcorr = [valX[0], valX[1], 'x_1', valX[2]]
            valeursX = [valX[0], valX[1], (valX[1] + valX[2]) / 2, valX[2]]
            valYcorr = [valY[0], valY[1], paramEq.nb, valY[2]]
          } else {
            valY.push(paramEq.nb + j3pGetRandomInt(4, 7))
            paramEq.tabSol.push([valX[0], valX[1]])
            valY.push(paramEq.nb - j3pGetRandomInt(4, 8))
            valY.push(j3pGetRandomInt(paramEq.nb + 2, paramEq.nb + 7))
            valXcorr = [valX[0], 'x_1', valX[1], 'x_2', valX[2]]
            valeursX = [valX[0], (valX[0] + valX[1]) / 2, valX[1], (valX[1] + valX[2]) / 2, valX[2]]
            valYcorr = [valY[0], paramEq.nb, valY[1], paramEq.nb, valY[2]]
          }
          paramEq.tabSol.push([valX[1], valX[2]])
        }
        tabVariations = ['decroit', 'croit']
      } else {
        if (j3pGetRandomInt(0, 1)) {
          // l’unique solution est sur le premier intervalle
          valY.push(paramEq.nb - j3pGetRandomInt(4, 7))
          valY.push(paramEq.nb + j3pGetRandomInt(5, 8))
          paramEq.tabSol.push([valX[0], valX[1]])
          if (nbsol === 1) {
            valY.push(j3pGetRandomInt(paramEq.nb + 1, valY[1] - 1))
            valXcorr = [valX[0], 'x_1', valX[1], valX[2]]
            valeursX = [valX[0], (valX[0] + valX[1]) / 2, valX[1], valX[2]]
            valYcorr = [valY[0], paramEq.nb, valY[1], valY[2]]
          } else {
            valY.push(j3pGetRandomInt(paramEq.nb - 7, paramEq.nb - 1))
            paramEq.tabSol.push([valX[1], valX[2]])
            valXcorr = [valX[0], 'x_1', valX[1], 'x_2', valX[2]]
            valeursX = [valX[0], (valX[0] + valX[1]) / 2, valX[1], (valX[1] + valX[2]) / 2, valX[2]]
            valYcorr = [valY[0], paramEq.nb, valY[1], paramEq.nb, valY[2]]
          }
        } else {
          // l’unique solution est sur le deuxième intervalle
          if (nbsol === 1) {
            valY.push(paramEq.nb + j3pGetRandomInt(4, 7))
            valY.push(valY[0] + j3pGetRandomInt(5, 8))
            valY.push(j3pGetRandomInt(paramEq.nb - 2, paramEq.nb - 7))
            valXcorr = [valX[0], valX[1], 'x_1', valX[2]]
            valeursX = [valX[0], valX[1], (valX[1] + valX[2]) / 2, valX[2]]
            valYcorr = [valY[0], valY[1], paramEq.nb, valY[2]]
          } else {
            valY.push(paramEq.nb - j3pGetRandomInt(2, 5))
            paramEq.tabSol.push([valX[0], valX[1]])
            valY.push(paramEq.nb + j3pGetRandomInt(4, 8))
            valY.push(j3pGetRandomInt(paramEq.nb - 2, paramEq.nb - 7))
            valXcorr = [valX[0], 'x_1', valX[1], 'x_2', valX[2]]
            valeursX = [valX[0], (valX[0] + valX[1]) / 2, valX[1], (valX[1] + valX[2]) / 2, valX[2]]
            valYcorr = [valY[0], paramEq.nb, valY[1], paramEq.nb, valY[2]]
          }
          paramEq.tabSol.push([valX[1], valX[2]])
        }
        tabVariations = ['croit', 'decroit']
      }
    } else {
      // 3 intervalles
      valX.push(-j3pGetRandomInt(3, 10))
      valX.push(valX[0] + j3pGetRandomInt(3, 7))
      valX.push(valX[1] + j3pGetRandomInt(3, 7))
      valX.push(valX[2] + j3pGetRandomInt(3, 7))
      let posSol = j3pGetRandomInt(1, 3)
      if (casFigure === 1) {
        if (nbsol === 1) {
          if (posSol === 1) {
            valY.push(j3pGetRandomInt(paramEq.nb + 1, paramEq.nb + 7))
            valY.push(j3pGetRandomInt(paramEq.nb - 11, paramEq.nb - 5))
            paramEq.tabSol.push([valX[0], valX[1]])
            valY.push(j3pGetRandomInt(valY[1] + 1, paramEq.nb - 1))
            valY.push(valY[2] - j3pGetRandomInt(3, 10))
            valXcorr = [valX[0], 'x_1', valX[1], valX[2], valX[3]]
            valeursX = [valX[0], (valX[0] + valX[1]) / 2, valX[1], valX[2], valX[3]]
            valYcorr = [valY[0], paramEq.nb, valY[1], valY[2], valY[3]]
          } else if (posSol === 2) {
            valY.push(j3pGetRandomInt(paramEq.nb - 5, paramEq.nb - 1))
            valY.push(valY[0] - j3pGetRandomInt(2, 6))
            valY.push(j3pGetRandomInt(paramEq.nb + 5, paramEq.nb + 11))
            paramEq.tabSol.push([valX[1], valX[2]])
            valY.push(j3pGetRandomInt(paramEq.nb + 2, valY[2] - 2))
            valXcorr = [valX[0], valX[1], 'x_1', valX[2], valX[3]]
            valeursX = [valX[0], valX[1], (valX[1] + valX[2]) / 2, valX[2], valX[3]]
            valYcorr = [valY[0], valY[1], paramEq.nb, valY[2], valY[3]]
          } else {
            valY.push(j3pGetRandomInt(paramEq.nb + 7, paramEq.nb + 14))
            valY.push(j3pGetRandomInt(paramEq.nb + 2, valY[0] - 2))
            valY.push(j3pGetRandomInt(valY[1] + 3, valY[1] + 12))
            paramEq.tabSol.push([valX[1], valX[2]])
            valY.push(j3pGetRandomInt(paramEq.nb - 5, paramEq.nb - 2))
            paramEq.tabSol.push([valX[2], valX[3]])
            valXcorr = [valX[0], valX[1], valX[2], 'x_1', valX[3]]
            valeursX = [valX[0], valX[1], valX[2], (valX[2] + valX[3]) / 2, valX[3]]
            valYcorr = [valY[0], valY[1], valY[2], paramEq.nb, valY[3]]
          }
        } else {
          posSol = j3pGetRandomInt(1, 2)
          if (posSol === 1) {
            valY.push(j3pGetRandomInt(paramEq.nb + 1, paramEq.nb + 7))
            valY.push(j3pGetRandomInt(paramEq.nb - 7, paramEq.nb - 3))
            valY.push(j3pGetRandomInt(paramEq.nb + 6, paramEq.nb + 12))
            paramEq.tabSol.push([valX[0], valX[1]])
            paramEq.tabSol.push([valX[1], valX[2]])
            valY.push(j3pGetRandomInt(paramEq.nb + 1, valY[2] - 1))
            valXcorr = [valX[0], 'x_1', valX[1], 'x_2', valX[2], valX[3]]
            valeursX = [valX[0], (valX[0] + valX[1]) / 2, valX[1], (valX[1] + valX[2]) / 2, valX[2], valX[3]]
            valYcorr = [valY[0], paramEq.nb, valY[1], paramEq.nb, valY[2], valY[3]]
          } else {
            valY.push(j3pGetRandomInt(paramEq.nb - 5, paramEq.nb - 1))
            valY.push(valY[0] - j3pGetRandomInt(2, 6))
            valY.push(j3pGetRandomInt(paramEq.nb + 2, paramEq.nb + 7))
            valY.push(j3pGetRandomInt(paramEq.nb - 7, valY[2] - 2))
            paramEq.tabSol.push([valX[1], valX[2]])
            paramEq.tabSol.push([valX[2], valX[3]])
            valXcorr = [valX[0], valX[1], 'x_1', valX[2], 'x_2', valX[3]]
            valeursX = [valX[0], valX[1], (valX[1] + valX[2]) / 2, valX[2], (valX[2] + valX[3]) / 2, valX[3]]
            valYcorr = [valY[0], valY[1], paramEq.nb, valY[2], paramEq.nb, valY[3]]
          }
        }
        tabVariations = ['decroit', 'croit', 'decroit']
      } else {
        if (nbsol === 1) {
          if (posSol === 1) {
            valY.push(j3pGetRandomInt(paramEq.nb - 7, paramEq.nb - 1))
            valY.push(j3pGetRandomInt(paramEq.nb + 5, paramEq.nb + 11))
            paramEq.tabSol.push([valX[0], valX[1]])
            valY.push(j3pGetRandomInt(valY[1] - 1, paramEq.nb + 1))
            valY.push(valY[2] + j3pGetRandomInt(3, 10))
            valXcorr = [valX[0], 'x_1', valX[1], valX[2], valX[3]]
            valeursX = [valX[0], (valX[0] + valX[1]) / 2, valX[1], valX[2], valX[3]]
            valYcorr = [valY[0], paramEq.nb, valY[1], valY[2], valY[3]]
          } else if (posSol === 2) {
            valY.push(j3pGetRandomInt(paramEq.nb + 5, paramEq.nb + 1))
            valY.push(valY[0] + j3pGetRandomInt(2, 6))
            valY.push(j3pGetRandomInt(paramEq.nb - 5, paramEq.nb - 11))
            paramEq.tabSol.push([valX[1], valX[2]])
            valY.push(j3pGetRandomInt(paramEq.nb - 2, valY[2] + 2))
            valXcorr = [valX[0], valX[1], 'x_1', valX[2], valX[3]]
            valeursX = [valX[0], valX[1], (valX[1] + valX[2]) / 2, valX[2], valX[3]]
            valYcorr = [valY[0], valY[1], paramEq.nb, valY[2], valY[3]]
          } else {
            valY.push(j3pGetRandomInt(paramEq.nb - 7, paramEq.nb - 14))
            valY.push(j3pGetRandomInt(paramEq.nb - 2, valY[0] + 2))
            valY.push(j3pGetRandomInt(valY[1] - 3, valY[1] - 12))
            valY.push(j3pGetRandomInt(paramEq.nb + 5, paramEq.nb + 2))
            paramEq.tabSol.push([valX[2], valX[3]])
            valXcorr = [valX[0], valX[1], valX[2], 'x_1', valX[3]]
            valeursX = [valX[0], valX[1], valX[2], (valX[2] + valX[3]) / 2, valX[3]]
            valYcorr = [valY[0], valY[1], valY[2], paramEq.nb, valY[3]]
          }
        } else {
          posSol = j3pGetRandomInt(1, 2)
          if (posSol === 1) {
            valY.push(j3pGetRandomInt(paramEq.nb - 1, paramEq.nb - 7))
            valY.push(j3pGetRandomInt(paramEq.nb + 7, paramEq.nb + 3))
            valY.push(j3pGetRandomInt(paramEq.nb - 6, paramEq.nb - 12))
            paramEq.tabSol.push([valX[0], valX[1]])
            paramEq.tabSol.push([valX[1], valX[2]])
            valY.push(j3pGetRandomInt(paramEq.nb - 1, valY[2] + 1))
            valXcorr = [valX[0], 'x_1', valX[1], 'x_2', valX[2], valX[3]]
            valeursX = [valX[0], (valX[0] + valX[1]) / 2, valX[1], (valX[1] + valX[2]) / 2, valX[2], valX[3]]
            valYcorr = [valY[0], paramEq.nb, valY[1], paramEq.nb, valY[2], valY[3]]
          } else {
            valY.push(j3pGetRandomInt(paramEq.nb + 5, paramEq.nb + 1))
            valY.push(valY[0] + j3pGetRandomInt(2, 6))
            valY.push(j3pGetRandomInt(paramEq.nb - 2, paramEq.nb - 7))
            valY.push(j3pGetRandomInt(paramEq.nb + 7, paramEq.nb + 2))
            paramEq.tabSol.push([valX[1], valX[2]])
            paramEq.tabSol.push([valX[2], valX[3]])
            valXcorr = [valX[0], valX[1], 'x_1', valX[2], 'x_2', valX[3]]
            valeursX = [valX[0], valX[1], (valX[1] + valX[2]) / 2, valX[2], (valX[2] + valX[3]) / 2, valX[3]]
            valYcorr = [valY[0], valY[1], paramEq.nb, valY[2], paramEq.nb, valY[3]]
          }
        }
        tabVariations = ['croit', 'decroit', 'croit']
      }
    }
    return {
      valX,
      valY,
      tabVariations,
      paramEq,
      valXcorr,
      valYcorr,
      valeursX
    }
  }
  function genereTableau (div, objDonnees, obj) {
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ////////////////Construction de l’objet pour le tableau de variations
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    // div est la div dans laquelle on construit le tableau
    // objDonnees contient les données à mettre dans le tableau (valeurs et variations)
    // obj est un objet contenant :
    // correction qui permet de créer un deuxième tableau de variation mais en mode correction (ajout d’abscisses dans cet exo)
    // couleur pour la couleur dans laquelle sera construit ce tableau
    const tabDef = {}
    // Tout ce qui concerne la mise en forme :
    tabDef.mef = {}
    // la largeur
    stor.largMin = (objDonnees.tabVariations.length === 3 && obj.correction) ? 800 : 600
    tabDef.mef.L = Math.min(0.85 * me.zonesElts.MG.getBoundingClientRect().width, stor.largMin)
    // la hauteur de la ligne des x
    tabDef.mef.h = 50
    // la couleur
    tabDef.mef.macouleur = (obj.couleur) ? obj.couleur : me.styles.toutpetit.enonce.color
    // le tableau principal, qui contiendra les infos de chaque ligne, il y aura donc autant de lignes (en plus de celle des x)
    tabDef.lignes = []
    // la première et unique ligne :
    tabDef.lignes[0] = {}
    // ligne de signes ou de variations ?
    tabDef.lignes[0].type = 'variations'
    // sa hauteur :
    tabDef.lignes[0].hauteur = 110
    tabDef.lignes[0].valeurs_charnieres = {}
    // j’ai  besoin des valeurs approchées pour les ordonner facilement... (souvent ce sont les mêmes car valeurs entières mais si ce sont des expressions mathquill...)
    tabDef.lignes[0].valeurs_charnieres.valeurs_approchees = []
    // Variations : (on peut mettre const)
    tabDef.lignes[0].variations = []
    // on peut ajouter des valeurs de x avec leurs images.
    tabDef.lignes[0].images = {}
    tabDef.lignes[0].imagesapprochees = []
    tabDef.lignes[0].expression = stor.nomf
    // j’ai mis 0 comme valeur charnière centrale, mais c’est arbitraire ici, ça n’impacte en rien la suite
    // A adapter
    const valXChaine = []
    const valYChaine = []
    if (obj.correction) {
      for (let i = 0; i < objDonnees.valXcorr.length; i++) {
        valXChaine.push(String(objDonnees.valXcorr[i]))
        valYChaine.push(String(objDonnees.valYcorr[i]))
      }
    } else {
      for (let i = 0; i < objDonnees.valX.length; i++) {
        valXChaine.push(String(objDonnees.valX[i]))
        valYChaine.push(String(objDonnees.valY[i]))
      }
    }
    tabDef.lignes[0].valeurs_charnieres.valeurs_theoriques = [...valXChaine]
    // A adapter
    tabDef.lignes[0].valeurs_charnieres.valeurs_affichees = [...valXChaine]
    // on peut laisser...
    tabDef.lignes[0].valeurs_charnieres.valeurs_approchees = (obj.correction) ? [...objDonnees.valeursX] : [...objDonnees.valX]
    // A adapter
    tabDef.lignes[0].variations = objDonnees.tabVariations
    tabDef.lignes[0].cliquable = [false, false]
    // les intervalles décrivant les variations de la fonction
    tabDef.lignes[0].intervalles = []
    for (let i = 1; i < objDonnees.valX.length; i++) {
      tabDef.lignes[0].intervalles.push([String(objDonnees.valX[i - 1]), String(objDonnees.valX[i])])
    }
    // A adapter
    tabDef.lignes[0].images = []
    for (let i = 0; i < valXChaine.length; i++) {
      tabDef.lignes[0].images.push([valXChaine[i], valYChaine[i]])
    }

    tabDef.lignes[0].imagesapprochees = (obj.correction) ? [...objDonnees.valeursX] : [...objDonnees.valX]
    // on n’autorise pas l’ajout de lignes et d’intervalles
    tabDef.ajout_lignes_possible = false
    tabDef.ajout_intervalles_possible = false
    const divTabId = j3pGetNewId('divTableau')
    // faut le créer sinon ça plante
    j3pAddElt(div, 'div', '', { id: divTabId, style: { position: 'relative' } })
    tableauSignesVariations(div, divTabId, tabDef, false)
  }
  function ecoute (zone) {
    if (zone.className.includes('mq-editable-field')) {
      j3pEmpty(stor.zoneCons7)
      j3pPaletteMathquill(stor.zoneCons7, zone, { liste: ['[', ']'] })
    }
  }

  function enonceInitFirst () {
    // toujours 2 étapes
    me.surcharge({ nbetapes: 2 })
    // Construction de la page
    me.construitStructurePage('presentation1')
    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    stor.tabCasFigure = [] // avec 2 ou trois intervalles
    for (let k = 0; k < Math.ceil(ds.nbrepetitions / 2); k++) {
      stor.tabCasFigure = stor.tabCasFigure.concat([2, 3])
    }
    stor.tabCasFigure = j3pShuffle(stor.tabCasFigure)
    enonceMain()
    // prise en compte des erreurs pour pe
    me.erreurs = [0, 0]// me.erreurs[0] est pour la première étape, me.erreurs[1] pour la deuxième
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (me.questionCourante % ds.nbetapes === 1) {
      // Nouvelle fonction
      const tabnomF = ['f', 'g', 'h']
      stor.nomf = tabnomF[j3pGetRandomInt(0, tabnomF.length - 1)]
      stor.objDonnees = calculDonneesTab()
      me.logIfDebug('stor.objDonnees:', stor.objDonnees)
    }
    j3pAffiche(stor.zoneCons1, '', (ds.continuite) ? textes.consigne1_2 : textes.consigne1_1,
      {
        f: stor.nomf,
        i: '[' + stor.objDonnees.valX[0] + ';' + stor.objDonnees.valX[stor.objDonnees.valX.length - 1] + ']'
      })
    genereTableau(stor.zoneCons2, stor.objDonnees, { correction: false })
    if (me.questionCourante % ds.nbetapes === 1) {
      const elt = j3pAffiche(stor.zoneCons3, '', textes.consigne2,
        {
          e: stor.nomf + '(x)=' + stor.objDonnees.paramEq.nb,
          input1: {
            texte: '',
            dynamique: true,
            maxchars: 1
          }
        })
      stor.zoneInput = [elt.inputList[0]]
      stor.zoneInput[0].typeReponse = ['nombre', 'exact']
      stor.zoneInput[0].reponse = [stor.objDonnees.paramEq.tabSol.length]
      stor.zoneCons4.style.marginTop = '15px'
      j3pAffiche(stor.zoneCons4, '', textes.rmq, {
        style: {
          fontStyle: 'italic',
          fontSize: '0.9em'
        }
      })
    } else {
      j3pAffiche(stor.zoneCons3, '', (stor.objDonnees.paramEq.tabSol.length === 1) ? textes.consigne3_1 : textes.consigne3_2,
        { e: stor.nomf + '(x)=' + stor.objDonnees.paramEq.nb }
      )
      for (let i = 5; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(stor.zoneCons4, '', (stor.objDonnees.paramEq.tabSol.length === 1) ? textes.consigne4_1 : textes.consigne4_2)
      if (stor.objDonnees.paramEq.tabSol.length === 1) {
        const elt = j3pAffiche(stor.zoneCons5, '', '&1&', { inputmq1: { texte: '' } })
        stor.zoneInput = [...elt.inputmqList]
      } else {
        const elt1 = j3pAffiche(stor.zoneCons5, '', textes.consigne5_1, { inputmq1: { texte: '' } })
        const elt2 = j3pAffiche(stor.zoneCons6, '', textes.consigne5_2, { inputmq1: { texte: '' } })
        stor.zoneInput = [elt1.inputmqList[0], elt2.inputmqList[0]]
      }
      for (let i = 0; i < stor.zoneInput.length; i++) {
        stor.zoneInput[i].addEventListener('focusin', ecoute.bind(null, stor.zoneInput[i]))
      }
      stor.zoneCons7.style.marginTop = '10px'
      mqRestriction(stor.zoneInput[0], '[]\\d;-,.')
      stor.zoneInput[0].typeReponse = ['texte']
      stor.zoneInput[0].reponse = [']' + stor.objDonnees.paramEq.tabSol[0][0] + ';' + stor.objDonnees.paramEq.tabSol[0][1] + '[',
        '[' + stor.objDonnees.paramEq.tabSol[0][0] + ';' + stor.objDonnees.paramEq.tabSol[0][1] + ']',
        ']' + stor.objDonnees.paramEq.tabSol[0][0] + ';' + stor.objDonnees.paramEq.tabSol[0][1] + ']',
        '[' + stor.objDonnees.paramEq.tabSol[0][0] + ';' + stor.objDonnees.paramEq.tabSol[0][1] + '['
      ]
      // Le tableau ci-dessous sert à récupérer les réponses, la zone de saisie pouant être détruite à la désactivation
      stor.tabReponses = [stor.zoneInput[0].reponse[0]]
      if (stor.objDonnees.paramEq.tabSol.length === 2) {
        mqRestriction(stor.zoneInput[1], '[]\\d;-,.')
        stor.zoneInput[1].typeReponse = ['texte']
        stor.zoneInput[1].reponse = [']' + stor.objDonnees.paramEq.tabSol[1][0] + ';' + stor.objDonnees.paramEq.tabSol[1][1] + '[',
          '[' + stor.objDonnees.paramEq.tabSol[1][0] + ';' + stor.objDonnees.paramEq.tabSol[1][1] + ']',
          ']' + stor.objDonnees.paramEq.tabSol[1][0] + ';' + stor.objDonnees.paramEq.tabSol[1][1] + ']',
          '[' + stor.objDonnees.paramEq.tabSol[1][0] + ';' + stor.objDonnees.paramEq.tabSol[1][1] + '['
        ]
        stor.tabReponses.push(stor.zoneInput[1].reponse[0])
      }
    }
    const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
    j3pFocus(stor.zoneInput[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })
    // pas besoin de filer les zones à tabDef car y’a pas d’inputs dans le tableau
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      if (me.questionCourante % ds.nbetapes === 1) me.essaiCourant = ds.nbchances
      const reponse = stor.fctsValid.validationGlobale()
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        this.reponseManquante(stor.zoneCorr)
        return me.finCorrection()
      }

      // Une réponse a été saisie
      if (reponse.bonneReponse) {
        // Bonne réponse
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        afficheCorrection(true)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      if (me.questionCourante % ds.nbetapes === 0) {
        let nonIntervalle = false
        let mauvaisseparateur = false
        for (let j = stor.fctsValid.zones.reponseSaisie.length - 1; j >= 0; j--) {
          const laReponse = stor.fctsValid.zones.reponseSaisie[j]
          if ((['[', ']'].indexOf(laReponse[0]) === -1) || (['[', ']'].indexOf(laReponse[laReponse.length - 1]) === -1)) nonIntervalle = true
          if (!nonIntervalle) {
            const tabNbs = laReponse.substring(1, laReponse.length - 1).split(';')
            if (tabNbs.length === 2) {
              if (isNaN(j3pNombre(tabNbs[0])) || isNaN(j3pNombre(tabNbs[0]))) {
                nonIntervalle = true
              }
            } else {
              const tabNbs2 = laReponse.substring(1, laReponse.length - 1).split(',')
              if (tabNbs2.length === 2) {
                if (!isNaN(j3pNombre(tabNbs2[0])) && !isNaN(j3pNombre(tabNbs2[0]))) {
                  mauvaisseparateur = true
                }
              }
            }
          }
        }
        if (nonIntervalle) {
          stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
        } else if (mauvaisseparateur) {
          stor.zoneCorr.innerHTML += '<br/>' + textes.comment2
        }
      }

      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // indication éventuelle ici
        // ici il a encore la possibilité de se corriger
        return me.finCorrection()
      }

      // Erreur au dernier essai
      if (me.questionCourante % ds.nbetapes === 1) {
        me.erreurs[0]++
      } else {
        me.erreurs[1]++
      }
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(false)
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if (me.erreurs[0] / ds.nbrepetitions > 0.6) {
          me.parcours.pe = ds.pe_2
        } else if (me.erreurs[1] / ds.nbrepetitions > 0.6) {
          me.parcours.pe = ds.pe_3
        } else if (me.score / ds.nbitems > 0.8) {
          me.parcours.pe = ds.pe_1
        } else {
          me.parcours.pe = ds.pe_4
        }
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
