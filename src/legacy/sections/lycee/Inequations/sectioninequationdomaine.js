import $ from 'jquery'
import { j3pAddElt, j3pFocus, j3pMathquillXcas, j3pMonome, j3pPaletteMathquill, j3pPGCD, j3pRandomTab, j3pGetBornesIntervalle, j3pGetRandomInt, j3pEmpty, j3pStyle, j3pShowError } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    juin 2014
    On détermine le domaine de définition d’une fonction de la forme sqrt(ax+b) en résolvant une inéquation
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[-10;10]', 'string', 'coefficient a de l’écriture ax+b'],
    ['b', '[-10;10]', 'string', 'coefficient b de l’écriture ax+b']
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorr (bonneReponse) {
    j3pEmpty(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })

    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1,
      {
        f: stor.nomFct,
        e: stor.fctAffine
      })
    const ineq2 = j3pMonome(1, 1, stor.coefa) + '\\geq' + (-stor.coefb)
    const ineq3 = (stor.coefa > 0) ? 'x\\geq' + stor.borneIntervalle : 'x\\leq' + stor.borneIntervalle
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2,
      {
        e: ineq2,
        i: ineq3
      })
    j3pAffiche(stor.zoneExpli3, '', ds.textes.corr3,
      {
        f: stor.nomFct,
        i: stor.domaine_rep
      })
  }
  function getDonnees () {
    return {

      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"
      a: '[-10;10]',
      b: '[-10;10]',
      textes: {
        titre_exo: 'Recherche d’un ensemble de définition',
        consigne1: '$£f$ est la fonction définie par $£f(x)=£e$.',
        consigne2: 'Elle a pour ensemble de définition :<br>&1&.',
        comment1: 'La réponse attendue doit être un intervalle, ce qui n’est pas le cas !',
        comment2: 'Il y a un problème avec les crochets de l’intervalle !',
        comment3: 'Tu as bien la bonne valeur en laquelle s’annule la fonction affine $x\\mapsto£g$, mais l’intervalle donné n’est pas le bon !',
        comment4: 'Tu as bien trouvé la bonne valeur où s’annule la fonction affine $x\\mapsto£g$, mais ta réponse n’est pas l’intervalle solution !',
        comment5: 'L’intervalle donné n’est pas le bon !',
        comment6: 'L’ensemble attendu n’est pas une réunion !',
        corr1: 'Comme on ne peut prendre la racine carrée que d’un nombre positif ou nul, $£f$ est définie lorsque $£e\\geq 0$.',
        corr2: 'Cette inéquation équivaut à $£e$ puis à $£i$.',
        corr3: 'L’ensemble de définition de $£f$ est donc l’intervalle $£i$.'
      },

      pe: 0
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    // Construction de la page
    me.construitStructurePage({ structure: me.donneesSection.structure, ratioGauche: 0.6 })
    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(me.donneesSection.textes.titre_exo)

    if (me.donneesSection.indication) me.indication(me.zonesElts.IG, me.donneesSection.indication)
  }
  function enonceMain () {
    if (ds.a === '[0;0]') {
      j3pShowError('le coefficient a doit nécessairement être non nul', { vanishAfter: 5 })
      ds.a = '[-10;10]'
    }
    do {
      const [intMin, intMax] = j3pGetBornesIntervalle(ds.a)
      stor.coefa = j3pGetRandomInt(intMin, intMax)
    } while (Math.abs(stor.coefa) < Math.pow(10, -12))
    const [intMin, intMax] = j3pGetBornesIntervalle(ds.b)
    stor.coefb = j3pGetRandomInt(intMin, intMax)
    // on récupère ces coefficients notamment pour la correction
    const fctAffine = j3pMonome(1, 1, stor.coefa) + j3pMonome(2, 0, stor.coefb)
    stor.fctAffine = fctAffine
    const tabNomFct = ['f', 'g', 'h']
    const nomFct = j3pRandomTab(tabNomFct, [0.33333, 0.33333, 0.33334])
    stor.nomFct = nomFct
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1,
      {
        f: nomFct,
        e: '\\sqrt{' + fctAffine + '}'
      })
    const elt = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, { inputmq1: { texte: '' } })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d.,+-[];/', {
      commandes: ['fraction', 'inf', 'R', 'union']
    })
    stor.zoneInput.typeReponse = ['texte']
    let borneIntervalle
    if (Math.abs(Math.round(-stor.coefb / stor.coefa) + stor.coefb / stor.coefa) < Math.pow(10, -12)) {
      // b/a est un entier dans ce cas
      borneIntervalle = String(-stor.coefb / stor.coefa)
    } else {
      const pgcdba = j3pPGCD(stor.coefb, stor.coefa, { returnOtherIfZero: true, negativesAllowed: true })
      if (stor.coefa * stor.coefb > 0) {
        borneIntervalle = '-\\frac{' + String(Math.abs(stor.coefb / pgcdba)) + '}{' + String(Math.abs(stor.coefa / pgcdba)) + '}'
      } else {
        borneIntervalle = '\\frac{' + String(Math.abs(stor.coefb / pgcdba)) + '}{' + String(Math.abs(stor.coefa / pgcdba)) + '}'
      }
    }
    stor.borneIntervalle = borneIntervalle
    const crochetRep = []
    const bornesRep = []
    if (stor.coefa > 0) {
      crochetRep[0] = '['
      crochetRep[1] = '['
      bornesRep[0] = -stor.coefb / stor.coefa
      bornesRep[1] = '+\\infty'
      stor.domaine_rep = crochetRep[0] + borneIntervalle + ';+\\infty' + crochetRep[1]
    } else {
      crochetRep[0] = ']'
      crochetRep[1] = ']'
      bornesRep[0] = '-\\infty'
      bornesRep[1] = -stor.coefb / stor.coefa
      stor.domaine_rep = crochetRep[0] + '-\\infty' + ';' + borneIntervalle + crochetRep[1]
    }
    stor.crochetRep = crochetRep
    stor.bornesRep = bornesRep
    me.logIfDebug('crochetRep:' + crochetRep + '   bornesRep:' + bornesRep)
    stor.laPalette = j3pAddElt(stor.zoneCons3, 'div')
    j3pStyle(stor.laPalette, { paddingTop: '15px' })
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, {
      liste: ['fraction', 'inf', 'R', 'union', '[', ']']
    })
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    j3pFocus(stor.zoneInput)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        // l’intervalle sera de la forme [-b/a;+\infty[ si coefa> et ]-\infty;-b/a] sinon'
        stor.fctsValid.zones.inputs[0].reponse = (stor.coefa > 0)
          ? ['[' + stor.borneIntervalle + ';+\\infty[']
          : [']-\\infty;' + stor.borneIntervalle + ']']
        let bonneReponse
        const fctsValid = stor.fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const aRepondu = fctsValid.valideReponses()
        const detailRep = fctsValid.valideUneZone(stor.zoneInput.id, stor.zoneInput.reponse)
        bonneReponse = detailRep.bonneReponse
        fctsValid.zones.bonneReponse[0] = bonneReponse
        me.logIfDebug('detailRep.bonneReponse:' + detailRep.bonneReponse)
        let repEleve, unionPresent, bornesOK, crochetsOK, bornesPresentes, zeroPresent
        if (!bonneReponse) {
        // la réponse n’est pas celle attendue'
        // on doit vérifier si la réponse n’est pas acceptable malgré tout
          repEleve = $(stor.zoneInput).mathquill('latex')
          me.logIfDebug('repEleve : ' + repEleve)
          stor.vraiIntervalle = true
          // on vérifie qu’on n’a pas de réunion'
          unionPresent = (repEleve.includes('\\cup'))
          if (unionPresent) {
            bonneReponse = false
          } else {
          // on vérifie qu’on a un crochet au début et à la fin'
            const char1 = repEleve.charAt(0)
            const charfin = repEleve.charAt(repEleve.length - 1)
            let crochetManquant = 0
            if ((char1 !== '[') && (char1 !== ']')) {
              crochetManquant++
            }
            if ((charfin !== '[') && (charfin !== ']')) {
              crochetManquant++
            }
            me.logIfDebug('crochetManquant :' + crochetManquant)
            if (crochetManquant > 0) {
            // il n’y a pas de crochet et/ou à la fin'
              bonneReponse = false
              stor.vraiIntervalle = false
            } else {
              let intervalleSansCrochet = repEleve.substring(1)
              intervalleSansCrochet = intervalleSansCrochet.substring(0, intervalleSansCrochet.length - 1)
              const tabBornes = intervalleSansCrochet.split(';')
              bornesOK = true
              crochetsOK = true
              bornesPresentes = false
              zeroPresent = true
              let infPresent = true
              if (tabBornes.length !== 2) {
                bornesOK = false
                stor.vraiIntervalle = false
              } else {
                if ((tabBornes[0] === '+\\infty') || (tabBornes[1] === '-\\infty')) {
                  bornesOK = false
                  stor.vraiIntervalle = false
                } else {
                  const arbreTabBornes = []
                  for (let i = 0; i <= 1; i++) {
                    if (tabBornes[i].indexOf('infty') === -1) {
                      tabBornes[i] = j3pMathquillXcas(tabBornes[i])
                      arbreTabBornes[i] = new Tarbre(tabBornes[i], ['x'])
                      tabBornes[i] = arbreTabBornes[i].evalue([0])
                    }
                  }
                  me.logIfDebug('intervalle transformé : ' + char1 + tabBornes[0] + ';' + tabBornes[1] + charfin)

                  if (String(stor.bornesRep[0]).includes('infty')) {
                    bornesOK = (bornesOK && (tabBornes[0] === '-\\infty'))
                  } else {
                    bornesOK = (bornesOK && (Math.abs(tabBornes[0] - stor.bornesRep[0]) < Math.pow(10, -12)))
                  }
                  if (String(stor.bornesRep[1]).includes('infty')) {
                    bornesOK = (bornesOK && (tabBornes[1] === '+\\infty'))
                  } else {
                    bornesOK = (bornesOK && (Math.abs(tabBornes[1] - stor.bornesRep[1]) < Math.pow(10, -12)))
                  }
                  // on vérifie aussi que les crochets sont les bons
                  crochetsOK = (crochetsOK && stor.crochetRep[0] === char1)
                  crochetsOK = (crochetsOK && stor.crochetRep[1] === charfin)
                  if (!bornesOK) {
                  // L’intervalle donné par l’élève n’est pas correct'
                  // on vérifie si l’infini est bien présent, ainsi que la valeur qui annule la fonction affine'
                    infPresent = ((String(tabBornes[0]).includes('infty')) || (String(tabBornes[1]).includes('infty')))
                    zeroPresent = ((Math.abs(tabBornes[1] + stor.coefb / stor.coefa) < Math.pow(10, -12)) || (Math.abs(tabBornes[0] + stor.coefb / stor.coefa) < Math.pow(10, -12)))
                    bornesPresentes = (infPresent && zeroPresent)
                    me.logIfDebug('infPresent:' + infPresent + '   zeroPresent:' + zeroPresent)
                  }
                }
              }
              bonneReponse = (bornesOK && crochetsOK)
            }
            if (!stor.vraiIntervalle) {
              me.typederreurs[3]++
            }
          }
        }
        fctsValid.coloreUneZone(stor.zoneInput.id)

        if ((!aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            afficheCorr(bonneReponse)
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              j3pDesactive(stor.zoneInput)
              stor.zoneInput.setAttribute('style', 'color:' + me.styles.cfaux)
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (unionPresent) {
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment6
              } else {
                if (!stor.vraiIntervalle && (repEleve !== '\\mathbb{R}')) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
                } else {
                  if (bornesOK) {
                    if (!crochetsOK) {
                      stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
                    }
                  } else {
                    const ajoutCorr = j3pAddElt(stor.zoneCorr, 'div')
                    if (bornesPresentes) {
                      j3pAffiche(ajoutCorr, '', ds.textes.comment3, { g: stor.fctAffine })
                    } else {
                      if (zeroPresent) {
                        j3pAffiche(ajoutCorr, '', ds.textes.comment4, { g: stor.fctAffine })
                      } else {
                        stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment5
                      }
                    }
                  }
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorr(bonneReponse)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
