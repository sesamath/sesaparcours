import { j3pAddElt, j3pDetruit, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pPaletteMathquill, j3pRandomTab, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2019
        Réunion et intersection d’intervalles
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuestions', 'Les deux', 'liste', 'Par défaut, on demandera l’intersection et la réunion des deux intervalles mais on peut ne choisir qu’un seul de ces cas de figure.', ['Les deux', 'Intersection', 'Reunion']],
    ['casParticulier', false, 'boolean', 'Lorsque ce paramètre vaut true, alors on aura un cas particulier (intersection vide ou union égale à l’ensemble des réels) lors de chaque étape de la dernière répétition.']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo1: 'Réunion et intersection d’intervalles',
  titre_exo2: 'Intersection d’intervalles',
  titre_exo3: 'Réunion d’intervalles',
  // on donne les phrases de la consigne
  consigne1_1: 'L’intersection des intervalles $£{i1}$ et $£{i2}$ est l’intervalle&nbsp;:',
  consigne1_2: 'La réunion (ou union) des intervalles $£{i1}$ et $£{i2}$ est l’intervalle&nbsp;:',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'La réponse doit être donnée sous la forme d’un intervalle et un seul&nbsp;!',
  comment2: 'Attention aux crochets de l’intervalle&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'On a réprésenté sur une droite graduée les deux intervalles $£{i1}$ et $£{i2}$.',
  corr2_1: 'L’intersection de ces deux intervalles est alors&nbsp;:<br/>$£{i1}\\cap £{i2}=£r$.',
  corr2_2: 'La réunion de ces deux intervalles est alors&nbsp;:<br/>$£{i1}\\cup £{i2}=£r$.'
}
/**
 * section intervallesUnionInter
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  stor.nbetapes = 2

  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV8AAADnAAAAQEAAAAAAAAAAQAAAF3#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAFAABANxnbItDlYEA3Gdsi0OVg#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAAA4AAVYAwAAAAAAAAABAEAAAAAAAAAUAAUBHGdsi0OVgAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAEAAAABAAAAA#####8AAAABAAdDTWlsaWV1AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAABAAAAA#####8AAAACAAxDQ29tbWVudGFpcmUA#####wEAAAAAAAAAAAAAAABAGAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAFBAAAAAAAAAAAAQAgAAAAAAAAJAAFAaSAAAAAAAEBSQo9cKPXCAAAAAwD#####AQAAAAEQAAABAAEAAAAIAT#wAAAAAAAAAAAABAD#####AQAAAAAQAAFCAAAAAAAAAAAAQAgAAAAAAAAJAAFAc4AAAAAAAAAAAAn#####AAAAAQAHQ0NhbGN1bAD#####AAJ4MQAELTAuMv####8AAAABAAxDTW9pbnNVbmFpcmUAAAABP8mZmZmZmZoAAAAJAP####8AEWNyb2NoZXRJbnRHYXVjaGUxAAExAAAAAT#wAAAAAAAA#####wAAAAEAEUNQb2ludFBhckFic2Npc3NlAP####8AAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAIAAAACv####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAALAAAACQD#####AAJ4MgADMC4zAAAAAT#TMzMzMzMzAAAACQD#####ABBjcm9jaGV0SW50RHJvaXQxAAExAAAAAT#wAAAAAAAAAAAACwD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACAAAAAoAAAAMAAAADgAAAAUA#####wAAAP8AEAAAAQADAAAADQAAABD#####AAAAAgAJQ0NlcmNsZU9SAP####8BAAAAAAEAAAANAAAAAT#mZmZmZmZmAAAAAAMA#####wEAAAABEAAAAQABAAAADQA#8AAAAAAAAAAAAA0A#####wEAAAAAAQAAABAAAAABP+ZmZmZmZmYAAAAAAwD#####AQAAAAEQAAABAAEAAAAQAD#wAAAAAAAA#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAABMAAAAS#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAHAAEAAAAWAAAADwD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAcAAgAAABYAAAAOAP####8AAAAVAAAAFAAAAA8A#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAHAAEAAAAZAAAADwD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAcAAgAAABkAAAAFAP####8AAAD#ABAAAAEAAwAAABgAAAAXAAAABQD#####AAAA#wAQAAABAAMAAAAbAAAAGv####8AAAABAAtDU2ltaWxpdHVkZQD#####AAAAGP####8AAAABAApDT3BlcmF0aW9uAgAAAAFAVoAAAAAAAAAAAAwAAAAMAAAAEQMAAAABQAgAAAAAAAAAAAABQBwAAAAAAAD#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAAA0AAAAeAAAAEAD#####AAAAFwAAAAoAAAARAgAAAAFAVoAAAAAAAAAAAAwAAAAMAAAAEQMAAAABQAgAAAAAAAAAAAABQBwAAAAAAAAAAAASAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAAANAAAAIAAAAAUA#####wAAAP8AEAAAAQADAAAAGAAAAB8AAAAFAP####8AAAD#ABAAAAEAAwAAACEAAAAXAAAAEAD#####AAAAGwAAAAoAAAARAgAAAAFAVoAAAAAAAAAAAAwAAAAPAAAAEQMAAAABQAgAAAAAAAAAAAABQBwAAAAAAAAAAAASAP####8AAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABwAAAAAQAAAAJAAAABAA#####wAAABoAAAARAgAAAAFAVoAAAAAAAAAAAAwAAAAPAAAAEQMAAAABQAgAAAAAAAAAAAABQBwAAAAAAAAAAAASAP####8AAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABwAAAAAQAAAAJgAAAAUA#####wAAAP8AEAAAAQADAAAAGgAAACcAAAAFAP####8AAAD#ABAAAAEAAwAAABsAAAAlAAAAAwD#####AQAAAAEQAAABAAEAAAAIAD#wAAAAAAAAAAAAAwD#####AQAAAAEQAAABAAEAAAAKAD#wAAAAAAAAAAAABAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAEAAUAIAAAAAAAAAAAAKgAAAAQA#####wEAAAAAEAABQwAAAAAAAAAAAEAIAAAAAAAABQABQBgAAAAAAAAAAAAqAAAAAwD#####AAAAAAEQAAABAAEAAAAsAT#wAAAAAAAAAAAAAwD#####AQAAAAEQAAABAAEAAAAtAT#wAAAAAAAA#####wAAAAEAEENJbnREcm9pdGVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAABAAAAACsAAAAuAAAAEwD#####AQAAAAAQAAFEAAAAAAAAAAAAQAgAAAAAAAAFAAAAACsAAAAvAAAACQD#####AAJ4MwADMC41AAAAAT#gAAAAAAAAAAAACQD#####AAJ4NAADMS4yAAAAAT#zMzMzMzMzAAAACwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAALQAAADEAAAAMAAAAMgAAAAsA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAC0AAAAxAAAADAAAADMAAAANAP####8BAAAAAAEAAAA0AAAAAT#mZmZmZmZmAAAAAA0A#####wEAAAAAAQAAADUAAAABP+ZmZmZmZmYAAAAAAwD#####AQAAAAEQAAABAAEAAAA0AD#wAAAAAAAAAAAAAwD#####AQAAAAEQAAABAAEAAAA1AD#wAAAAAAAAAAAADgD#####AAAAOAAAADYAAAAPAP####8AAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAABwABAAAAOgAAAA8A#####wAAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAHAAIAAAA6AAAADgD#####AAAAOQAAADcAAAAPAP####8AAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAABwABAAAAPQAAAA8A#####wAAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAHAAIAAAA9AAAABQD#####AAB#AAAQAAABAAMAAAA8AAAAOwAAAAUA#####wAAfwAAEAAAAQADAAAAPwAAAD4AAAAFAP####8AAH8AABAAAAEAAwAAADQAAAA1AAAACQD#####ABFjcm9jaGV0SW50R2F1Y2hlMgACLTEAAAAKAAAAAT#wAAAAAAAAAAAACQD#####ABBjcm9jaGV0SW50RHJvaXQyAAExAAAAAT#wAAAAAAAAAAAAEAD#####AAAAPAAAABECAAAAAUBWgAAAAAAAAAAADAAAAEMAAAARAwAAAAFACAAAAAAAAAAAAAFAHAAAAAAAAAAAABIA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAADQAAABFAAAAEAD#####AAAAOwAAAAoAAAARAgAAAAFAVoAAAAAAAAAAAAwAAABDAAAAEQMAAAABQAgAAAAAAAAAAAABQBwAAAAAAAAAAAASAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAA0AAAARwAAAAUA#####wAAfwAAEAAAAQADAAAAPAAAAEYAAAAFAP####8AAH8AABAAAAEAAwAAAEgAAAA7AAAAEAD#####AAAAPwAAAAoAAAARAgAAAAFAVoAAAAAAAAAAAAwAAABEAAAAEQMAAAABQAgAAAAAAAAAAAABQBwAAAAAAAAAAAASAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAA1AAAASwAAABAA#####wAAAD4AAAARAgAAAAFAVoAAAAAAAAAAAAwAAABEAAAAEQMAAAABQAgAAAAAAAAAAAABQBwAAAAAAAAAAAASAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAA1AAAATQAAAAUA#####wAAfwAAEAAAAQADAAAATgAAAD4AAAAFAP####8AAH8AABAAAAEAAwAAAD8AAABMAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAEAAAAAEwAAAC4AAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAQAAAAAVAAAALgAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAABAAAAAC4AAAA4AAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAEAAAAALgAAADkAAAAJAP####8AA25iMQABMAAAAAEAAAAAAAAAAAAAAAkA#####wADbmIyAAExAAAAAT#wAAAAAAAAAAAACQD#####AANuYjMAATIAAAABQAAAAAAAAAAAAAAJAP####8AA25iNAABMwAAAAFACAAAAAAAAAAAAAcA#####wAAAAAAwAAAAAAAAABACAAAAAAAAAAAAFESAAAAAAACAAAAAAAAAAEAAAAAAAAAAAALJCNWYWwobmIxKSQAAAAHAP####8AAAAAAEAAAAAAAAAAQBAAAAAAAAAAAABSEgAAAAAAAAAAAAAAAAABAAAAAAAAAAAACyQjVmFsKG5iMikkAAAABwD#####AAAAAADACAAAAAAAAD#wAAAAAAAAAAAAUxIAAAAAAAIAAAAAAAAAAQAAAAAAAAAAAAskI1ZhbChuYjMpJAAAAAcA#####wAAAAAAQAgAAAAAAAAAAAAAAAAAAAAAAFQSAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAALJCNWYWwobmI0KSQAAAAH##########8='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true);
  }

  function modifFig () {
    // cette fonction sert à modifier la figure de base
    // num est le numéro de la question (1 ou 3) selon qu’on cherche la valeur finale ou la valeur initiale
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'x1', String(stor.bornesFig[0]))
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'x2', String(stor.bornesFig[2]))
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'x3', String(stor.bornesFig[1]))
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'x4', String(stor.bornesFig[3]))
    // console.log("bornes:",stor.bornes,"   et borneFig:",stor.bornesFig)
    // valeur affichées
    if (stor.bornes[3] === '-\\infty') {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb2', String(stor.bornes[1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb3', String(stor.bornes[0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb4', String(stor.bornes[2]))
    } else if (stor.bornes[3] === '+\\infty') {
      if (stor.bornes[2] === '-\\infty') {
        // Je suis dans le cas particulier où sont présents -infini et +infini
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb2', String(stor.bornes[0]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb3', String(stor.bornes[1]))
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb1', String(stor.bornes[0]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb2', String(stor.bornes[2]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb3', String(stor.bornes[1]))
      }
    } else {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb1', String(stor.bornes[0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb2', String(stor.bornes[2]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb3', String(stor.bornes[1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'nb4', String(stor.bornes[3]))
    }
    const crochetGauche1 = (stor.choixCrochets[0] === ']') ? -1 : 1
    const crochetDroit1 = (stor.choixCrochets[1] === ']') ? 1 : -1
    const crochetGauche2 = (stor.choixCrochets[2] === ']') ? -1 : 1
    const crochetDroit2 = (stor.choixCrochets[3] === ']') ? 1 : -1
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'crochetIntGauche1', String(crochetGauche1))
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'crochetIntDroit1', String(crochetDroit1))
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'crochetIntGauche2', String(crochetGauche2))
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'crochetIntDroit2', String(crochetDroit2))
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pDetruit(stor.laPalette)
    j3pAffiche(stor.zoneExpli1, '', textes.corr1,
      {
        i1: stor.intervalles[0],
        i2: stor.intervalles[1]
      })
    const laCorr2 = (stor.typeQuest === 'inter') ? textes.corr2_1 : textes.corr2_2
    j3pAffiche(stor.zoneExpli3, '', laCorr2,
      {
        i1: stor.intervalles[0],
        i2: stor.intervalles[1],
        r: stor.intervalleRep
      })
    /// /////////////////////////////////////// Pour MathGraph32
    const largFig = 735// on gère ici la largeur de la figure
    const hautFig = 150
    // création du div accueillant la figure mtg32
    const divMtg32 = j3pAddElt(stor.zoneExpli2, 'div')
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
    // ce qui suit lance la création initiale de la figure
    depart()
    modifFig()
  }
  function enonceMain () {
  // création du conteneur dans lequel se trouveront toutes les conaignes
    if (((stor.nbetapes === 1) || ((me.questionCourante - 1) % stor.nbetapes === 0)) || (ds.casParticulier && (me.questionCourante >= ds.nbitems - stor.nbetapes + 1))) {
      stor.intervalles = []
      const tabx = []
      const choixCrochets = []
      stor.bornesFig = []
      let c, avecInfini, choixInf
      if (ds.casParticulier && (me.questionCourante >= ds.nbitems - stor.nbetapes + 1)) {
      // Nous sommes à la dernière répétitions et on souhaite un cas particulier
        if (stor.casParticulier) {
        // on a déjà eu un cas particulier. Donc on veut l’autre
          stor.typeQuest = (stor.typeQuest === 'inter') ? 'union' : 'inter'
        } else {
        // on choisi le cas de figure
          stor.typeQuest = (ds.typeQuestions === 'Intersection')
            ? 'inter'
            : (ds.typeQuestions === 'Reunion') ? 'union' : j3pRandomTab(['union', 'inter'], [0.5, 0.5])
          stor.casParticulier = true
        }
        if (stor.typeQuest === 'inter') {
        // Cette intersection sera vide
          avecInfini = j3pGetRandomInt(0, 1) !== 0
          if (avecInfini) {
          // j’ai besoin de 3 nombres
            tabx[0] = j3pGetRandomInt(-5, -2)
            tabx[1] = tabx[0] + j3pGetRandomInt(2, 4)
            tabx[2] = tabx[1] + j3pGetRandomInt(2, 5)
            choixInf = (j3pGetRandomInt(0, 1) === 0) ? '-\\infty' : '+\\infty'
            if (choixInf === '-\\infty') {
            // Il suffit de permuter les rôles de tabx[0] et tabx[1]
              c = tabx[0]
              tabx[0] = tabx[1]
              tabx[1] = c
              choixCrochets.push(']')
              for (let i = 1; i <= 3; i++) {
                choixCrochets[i] = (j3pGetRandomInt(0, 1) === 0) ? '[' : ']'
              }
              stor.intervalles.push(']-\\infty\\quad ;\\quad' + tabx[1] + choixCrochets[1])
              stor.intervalles.push(choixCrochets[2] + tabx[0] + '\\quad ;\\quad' + tabx[2] + choixCrochets[3])
              stor.intersection = '\\emptyset'
              stor.bornesFig[0] = -10
              stor.bornesFig[3] = 1
              stor.bornesFig[2] = 0
              stor.bornesFig[1] = stor.bornesFig[2] + (tabx[0] - tabx[1]) / (tabx[2] - tabx[1])
            } else {
            // Il suffit de permuter les rôles de tabx[1] et tabx[2]
              c = tabx[2]
              tabx[2] = tabx[1]
              tabx[1] = c
              for (let i = 0; i <= 2; i++) {
                choixCrochets[i] = (j3pGetRandomInt(0, 1) === 0) ? '[' : ']'
              }
              choixCrochets.push('[')
              stor.intervalles.push(choixCrochets[0] + tabx[0] + '\\quad ;\\quad' + tabx[2] + choixCrochets[1])
              stor.intervalles.push(choixCrochets[2] + tabx[1] + '\\quad ;\\quad +\\infty' + choixCrochets[3])
              stor.intersection = '\\emptyset'
              stor.bornesFig[0] = 0
              stor.bornesFig[1] = 1
              stor.bornesFig[2] = stor.bornesFig[0] + (tabx[2] - tabx[0]) / (tabx[1] - tabx[0])
              stor.bornesFig[3] = 10
            }
            tabx.push(choixInf)
          } else {
          // j’ai besoin de 4 nombres
            tabx[0] = j3pGetRandomInt(-5, -2)
            tabx[1] = tabx[0] + j3pGetRandomInt(1, 3)
            tabx[2] = tabx[1] + j3pGetRandomInt(1, 3)
            tabx[3] = tabx[2] + j3pGetRandomInt(2, 3)
            // on permute alors tabx[1] et tabx[2]
            c = tabx[2]
            tabx[2] = tabx[1]
            tabx[1] = c
            for (let i = 0; i <= 3; i++) {
              choixCrochets[i] = (j3pGetRandomInt(0, 1) === 0) ? '[' : ']'
            }
            stor.intervalles.push(choixCrochets[0] + tabx[0] + '\\quad ;\\quad ' + tabx[2] + choixCrochets[1])
            stor.intervalles.push(choixCrochets[2] + tabx[1] + '\\quad ;\\quad ' + tabx[3] + choixCrochets[3])
            stor.intersection = '\\emptyset'
            stor.bornesFig[0] = -0.25
            stor.bornesFig[3] = 1.25
            stor.bornesFig[1] = stor.bornesFig[0] + (tabx[1] - tabx[0]) * 1.5 / (tabx[3] - tabx[0])
            stor.bornesFig[2] = stor.bornesFig[0] + (tabx[2] - tabx[0]) * 1.5 / (tabx[3] - tabx[0])
          }
        } else {
        // Cette réunion sera l’ensemble des réels
        // j’ai besoin de 2 nombres
          tabx[0] = j3pGetRandomInt(-1, 4)
          tabx[1] = tabx[0] - j3pGetRandomInt(2, 4)
          choixCrochets.push(']')
          for (let i = 1; i <= 2; i++) {
            choixCrochets[i] = (j3pGetRandomInt(0, 1) === 0) ? '[' : ']'
          }
          choixCrochets.push('[')
          stor.intervalles.push(choixCrochets[0] + '-\\infty\\quad ;\\quad' + tabx[0] + choixCrochets[1])
          stor.intervalles.push(choixCrochets[2] + tabx[1] + '\\quad ;\\quad +\\infty' + choixCrochets[3])
          stor.union = ']-\\infty\\quad ;\\quad +\\infty['
          tabx.push('-\\infty', '+\\infty')
          stor.bornesFig[0] = -10
          stor.bornesFig[1] = 0
          stor.bornesFig[2] = 1
          stor.bornesFig[3] = 10
        }
      } else {
      // On génère 2 nouveaux intervalles
      // A la première répétitions et de manière aléatoire à partir de la 3ème, les bornes ne sont que des entiers
      // A la deuxième répétitions et de manière aléatoire à partir de la 3ème, l’une des bornes est l’infini
        avecInfini = (me.questionCourante === 1)
          ? false
          : (me.questionCourante - stor.nbetapes === 1)
              ? true
              : j3pGetRandomInt(0, 1) !== 0
        if (avecInfini) {
        // j’ai besoin de 3 nombres
          tabx[0] = j3pGetRandomInt(-5, -2)
          tabx[1] = tabx[0] + j3pGetRandomInt(2, 4)
          tabx[2] = tabx[1] + j3pGetRandomInt(2, 5)
          choixInf = (j3pGetRandomInt(0, 1) === 0) ? '-\\infty' : '+\\infty'
          if (choixInf === '-\\infty') {
            choixCrochets.push(']')
            for (let i = 1; i <= 3; i++) {
              choixCrochets[i] = (j3pGetRandomInt(0, 1) === 0) ? '[' : ']'
            }
            stor.intervalles.push(']-\\infty\\quad ;\\quad' + tabx[1] + choixCrochets[1])
            stor.intervalles.push(choixCrochets[2] + tabx[0] + '\\quad ;\\quad' + tabx[2] + choixCrochets[3])
            stor.intersection = choixCrochets[2] + tabx[0] + '\\quad ;\\quad ' + tabx[1] + choixCrochets[1]
            stor.union = choixCrochets[0] + '-\\infty\\quad ;\\quad ' + tabx[2] + choixCrochets[3]
            stor.bornesFig[0] = -10
            stor.bornesFig[3] = 1
            stor.bornesFig[1] = 0
            stor.bornesFig[2] = stor.bornesFig[1] + (tabx[1] - tabx[0]) / (tabx[2] - tabx[0])
          } else {
            for (let i = 0; i <= 2; i++) {
              choixCrochets[i] = (j3pGetRandomInt(0, 1) === 0) ? '[' : ']'
            }
            choixCrochets.push('[')
            stor.intervalles.push(choixCrochets[0] + tabx[0] + '\\quad ;\\quad' + tabx[2] + choixCrochets[1])
            stor.intervalles.push(choixCrochets[2] + tabx[1] + '\\quad ;\\quad +\\infty' + choixCrochets[3])
            stor.intersection = choixCrochets[2] + tabx[1] + '\\quad ;\\quad ' + tabx[2] + choixCrochets[1]
            stor.union = choixCrochets[0] + tabx[0] + '\\quad ;\\quad +\\infty' + choixCrochets[3]
            stor.bornesFig[0] = 0
            stor.bornesFig[2] = 1
            stor.bornesFig[3] = 10
            stor.bornesFig[1] = stor.bornesFig[0] + (tabx[1] - tabx[0]) / (tabx[2] - tabx[0])
          }
          tabx.push(choixInf)
        } else {
        // j’ai besoin de 4 nombres
          tabx[0] = j3pGetRandomInt(-5, -2)
          tabx[1] = tabx[0] + j3pGetRandomInt(1, 3)
          tabx[2] = tabx[1] + j3pGetRandomInt(1, 3)
          tabx[3] = tabx[2] + j3pGetRandomInt(2, 3)
          for (let i = 0; i <= 3; i++) {
            choixCrochets[i] = (j3pGetRandomInt(0, 1) === 0) ? '[' : ']'
          }
          stor.intervalles.push(choixCrochets[0] + tabx[0] + '\\quad ;\\quad' + tabx[2] + choixCrochets[1])
          stor.intervalles.push(choixCrochets[2] + tabx[1] + '\\quad ;\\quad' + tabx[3] + choixCrochets[3])
          stor.intersection = choixCrochets[2] + tabx[1] + '\\quad ;\\quad ' + tabx[2] + choixCrochets[1]
          stor.union = choixCrochets[0] + tabx[0] + '\\quad ;\\quad ' + tabx[3] + choixCrochets[3]
          stor.bornesFig[0] = -0.25
          stor.bornesFig[3] = 1.25
          stor.bornesFig[1] = stor.bornesFig[0] + (tabx[1] - tabx[0]) * 1.5 / (tabx[3] - tabx[0])
          stor.bornesFig[2] = stor.bornesFig[0] + (tabx[2] - tabx[0]) * 1.5 / (tabx[3] - tabx[0])
        }
        stor.typeQuest = ''
      }
      stor.bornes = [...tabx]
      stor.choixCrochets = choixCrochets
    }
    if (!stor.casParticulier) {
    // on n’aura pas de cas particulier
      if (stor.nbetapes === 2) {
        if (stor.typeQuest === '') {
          stor.typeQuest = j3pRandomTab(['inter', 'union'], [0.5, 0.5])
        } else {
          stor.typeQuest = (stor.typeQuest === 'inter') ? 'union' : 'inter'
        }
      } else {
        stor.typeQuest = (ds.typeQuestions === 'Intersection') ? 'inter' : 'union'
      }
    }

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    const laCons1 = (stor.typeQuest === 'inter') ? textes.consigne1_1 : textes.consigne1_2
    j3pAffiche(stor.zoneCons1, '', laCons1,
      {
        i1: stor.intervalles[0],
        i2: stor.intervalles[1]
      })
    const elt = j3pAffiche(stor.zoneCons2, '', '&1&', { inputmq1: { texte: '' } })
    stor.zoneInput = elt.inputmqList[0]
    stor.intervalleRep = (stor.typeQuest === 'inter') ? stor.intersection : stor.union
    // console.log('reponse:',stor.intervalleRep)
    stor.zoneInput.typeReponse = ['texte']
    mqRestriction(stor.zoneInput, '\\d+-;][', {
      commandes: ['R', 'inf', 'vide']
    })
    stor.laPalette = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.laPalette, { paddingTop: '10px' })
    // palette MQ :
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, {
      liste: ['R', '[', ']', 'inf', 'vide']
    })

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    j3pFocus(stor.zoneInput)
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        const leTitre = (ds.typeQuestions === 'Intersection')
          ? textes.titre_exo2
          : (ds.typeQuestions === 'Reunion')
              ? textes.titre_exo3
              : textes.titre_exo1
        me.afficheTitre(leTitre)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        if (ds.typeQuestions !== 'Les deux') {
          stor.nbetapes = 1
          ds.nbitems = stor.nbetapes * ds.nbrepetitions
        }
        stor.casParticulier = false// Me sert seulment pour gérer les cas particuliers (intersection vide ou réunion égale à \\R)
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        let estIntervalle, bonCrochets, bonnesBornes
        if (reponse.aRepondu) {
        // Je vérifie si on a bien un intervalle
        // Tout d’abord pour me simplifier la tâche, je remplace l’infini par le nombre 1234567891
          let reponseSaisie = fctsValid.zones.reponseSaisie[0]
          if (stor.casParticulier && (stor.typeQuest === 'inter')) {
          // La réponse attendue est ensemble vide
            reponse.bonneReponse = (reponseSaisie === '\\varnothing')
          } else {
            if (reponseSaisie === '\\mathbb{R}') reponseSaisie = ']-\\infty;+\\infty['
            reponseSaisie = reponseSaisie.replace(/\\-\\infty/g, '-1234567891')
            reponseSaisie = reponseSaisie.replace(/\+\\infty/g, '1234567891')
            reponseSaisie = reponseSaisie.replace(/\\infty/g, '1234567891')
            const IntregExp = /(\[|])-?[0-9]+;-?[0-9]+(\[|])/g // new RegExp('(\\[|\\])\\-?[0-9]{1,};\\-?[0-9]{1,}(\\[|\\])', 'g')
            const tabInt = reponseSaisie.match(IntregExp)
            me.logIfDebug('tabInt:', tabInt, 'reponseSaisie:', reponseSaisie)
            estIntervalle = false
            if (IntregExp.test(reponseSaisie)) {
              estIntervalle = (tabInt.length === 1)
              if (estIntervalle) {
                estIntervalle = (tabInt[0] === reponseSaisie)
              }
            }
            if (estIntervalle) {
              bonCrochets = ((reponseSaisie[0] === stor.intervalleRep[0]) && (reponseSaisie[reponseSaisie.length - 1] === stor.intervalleRep[stor.intervalleRep.length - 1]))
              const intSansCrochets = reponseSaisie.substring(1, reponseSaisie.length - 1).split(';')
              const repSansCrochets = stor.intervalleRep.substring(1, stor.intervalleRep.length - 1).split('\\quad ;\\quad ')
              bonnesBornes = true
              for (let i = 0; i < 2; i++) {
                repSansCrochets[i] = repSansCrochets[i].replace(/\\-\\infty/g, '-1234567891')
                repSansCrochets[i] = repSansCrochets[i].replace(/\+\\infty/g, '1234567891')
                repSansCrochets[i] = repSansCrochets[i].replace(/\\infty/g, '1234567891')
                try {
                  bonnesBornes = (bonnesBornes && (Math.abs(Number(intSansCrochets[i]) - Number(repSansCrochets[i])) < Math.pow(10, -12)))
                } catch (e) {
                  bonnesBornes = false
                }
              }
              reponse.bonneReponse = (bonCrochets && bonnesBornes)
            } else {
              reponse.bonneReponse = false
            }
          }
          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
          fctsValid.coloreUneZone(stor.zoneInput.id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!estIntervalle) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment1
              } else {
                if (bonnesBornes && !bonCrochets) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment2
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
