import { j3pAddElt, j3pShuffle, j3pFocus, j3pPaletteMathquill, j3pRandomTab, j3pGetRandomInt, j3pEmpty, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    juin 2014
    Traduction d’une inégalité sous forme d’un intervalle et réciproquement

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeIneg', ['alea', 'alea', 'alea'], 'array', 'type d’inégalité pour chaque répétition, on peut choisir "inferieur", "superieur", "encadrement" ou "alea"'],
    ['borne_inf', '[-10;10]', 'string', 'borne inférieure de l’intervalle (n’existe pas si typeIneg vaut "inferieur")'],
    ['borne_sup', '[-10;10]', 'string', 'borne supérieure de l’intervalle (n’existe pas si typeIneg vaut "superieur")']
  ]

}

/**
 * section inegalite_intervalle
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // cette fonction affiche la correction selon l’étape.
    const epaisTrait = 5
    j3pEmpty(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    const couleurCorr = zoneExpli.style.color
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    if ((me.questionCourante % ds.nbetapes) === 1) {
      if (stor.typeIneg === 'encadrement') {
        j3pAffiche(zoneExpli1, '', ds.textes.corr2)
      } else {
        j3pAffiche(zoneExpli1, '', ds.textes.corr1)
      }
    } else {
      if (stor.typeIneg === 'encadrement') {
        j3pAffiche(zoneExpli1, '', ds.textes.corr5, { x: 'x' })
      } else {
        j3pAffiche(zoneExpli1, '', ds.textes.corr4, { x: 'x' })
      }
    }
    // je dois créer un petit décalage pour que les crochets soient jolis c.-à-d. qu’on gère les angles droits de ce crochet qui ne sont pas corrects au départ à cause de la pixelisation
    const decal = (epaisTrait / 2) / 30
    const leRepere = j3pAddElt(zoneExpli, 'div')
    stor.repere = new Repere({
      idConteneur: leRepere,
      visible: false,
      trame: false,
      fixe: true,
      larg: 550,
      haut: 60,

      pasdunegraduationX: 1,
      pixelspargraduationX: 30,
      pasdunegraduationY: 1,
      pixelspargraduationY: 30,
      xO: 275,
      yO: 20,
      debuty: 0,
      negatifs: true,
      objets: [{
        // début du segment de l’axe des réels
        type: 'point',
        nom: 'O1',
        par1: -10,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      },
      {
        // fin du segment de l’axe des réels
        type: 'point',
        nom: 'O2',
        par1: 10,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      },
      { type: 'segment', nom: 'axe', par1: 'O1', par2: 'O2', style: { couleur: '#000000', epaisseur: 2 } }]
    })

    // dessin des crochets
    if (stor.typeIneg === 'inferieur') {
      stor.repere.add({
        type: 'point',
        nom: 'A',
        par1: 0,
        par2: -0.5,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'B',
        par1: 0,
        par2: 0.5,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'A2',
        par1: 0,
        par2: -0.5 - decal,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'B2',
        par1: 0,
        par2: 0.5 + decal,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 's1', par1: 'A2', par2: 'B2', style: { couleur: couleurCorr, epaisseur: epaisTrait } })
      stor.repere.add({
        type: 'point',
        nom: 'Z',
        par1: -10,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'Y',
        par1: 0,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 's2', par1: 'Y', par2: 'Z', style: { couleur: couleurCorr, epaisseur: epaisTrait } })
      if (stor.signe1 === '<') {
        stor.repere.add({
          type: 'point',
          nom: 'C',
          par1: 0.2,
          par2: -0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'D',
          par1: 0.2,
          par2: 0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
      } else {
        stor.repere.add({
          type: 'point',
          nom: 'C',
          par1: -0.2,
          par2: -0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'D',
          par1: -0.2,
          par2: 0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
      }
      stor.repere.add({
        type: 'segment',
        nom: 's3',
        par1: 'C',
        par2: 'A',
        style: { couleur: couleurCorr, epaisseur: epaisTrait }
      })
      stor.repere.add({ type: 'segment', nom: 's4', par1: 'D', par2: 'B', style: { couleur: couleurCorr, epaisseur: epaisTrait } })
      stor.repere.add({
        type: 'point',
        nom: String(stor.bornesRep[1]),
        par1: 0,
        par2: -0.5,
        fixe: true,
        visible: true,
        etiquette: true,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 15, decal: [0, 35] }
      })
    } else if (stor.typeIneg === 'superieur') {
      stor.repere.add({
        type: 'point',
        nom: 'A',
        par1: 0,
        par2: -0.5,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'B',
        par1: 0,
        par2: 0.5,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'A2',
        par1: 0,
        par2: -0.5 - decal,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'B2',
        par1: 0,
        par2: 0.5 + decal,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 's1', par1: 'A2', par2: 'B2', style: { couleur: couleurCorr, epaisseur: epaisTrait } })
      stor.repere.add({
        type: 'point',
        nom: 'Z',
        par1: 10,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'Y',
        par1: 0,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 's2', par1: 'Y', par2: 'Z', style: { couleur: couleurCorr, epaisseur: epaisTrait } })

      if (stor.signe1 === '>') {
        stor.repere.add({
          type: 'point',
          nom: 'C',
          par1: -0.2,
          par2: -0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'D',
          par1: -0.2,
          par2: 0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
      } else {
        stor.repere.add({
          type: 'point',
          nom: 'C',
          par1: 0.2,
          par2: -0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'D',
          par1: 0.2,
          par2: 0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
      }
      stor.repere.add({
        type: 'segment',
        nom: 's3',
        par1: 'C',
        par2: 'A',
        style: { couleur: couleurCorr, epaisseur: epaisTrait }
      })
      stor.repere.add({ type: 'segment', nom: 's4', par1: 'D', par2: 'B', style: { couleur: couleurCorr, epaisseur: epaisTrait } })
      stor.repere.add({
        type: 'point',
        nom: String(stor.bornesRep[0]),
        par1: 0,
        par2: -0.5,
        fixe: true,
        visible: true,
        etiquette: true,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 15, decal: [0, 35] }
      })
    } else if (stor.typeIneg === 'encadrement') {
      stor.repere.add({
        type: 'point',
        nom: 'A',
        par1: -2,
        par2: -0.5,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'B',
        par1: -2,
        par2: 0.5,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'A2',
        par1: -2,
        par2: -0.5 - decal,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'B2',
        par1: -2,
        par2: 0.5 + decal,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'E',
        par1: 2,
        par2: -0.5,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'F',
        par1: 2,
        par2: 0.5,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'E2',
        par1: 2,
        par2: -0.5 - decal,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'F2',
        par1: 2,
        par2: 0.5 + decal,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 's1', par1: 'A2', par2: 'B2', style: { couleur: couleurCorr, epaisseur: epaisTrait } })
      stor.repere.add({ type: 'segment', nom: 's1bis', par1: 'E2', par2: 'F2', style: { couleur: couleurCorr, epaisseur: epaisTrait } })
      stor.repere.add({
        type: 'point',
        nom: 'Z',
        par1: 2,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'Y',
        par1: -2,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 's2', par1: 'Y', par2: 'Z', style: { couleur: couleurCorr, epaisseur: epaisTrait } })

      if (stor.signe1 === '<') {
        stor.repere.add({
          type: 'point',
          nom: 'C',
          par1: -2.2,
          par2: -0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'D',
          par1: -2.2,
          par2: 0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
      } else {
        stor.repere.add({
          type: 'point',
          nom: 'C',
          par1: -1.8,
          par2: -0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'D',
          par1: -1.8,
          par2: 0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
      }
      if (stor.signe2 === '<') {
        stor.repere.add({
          type: 'point',
          nom: 'C1',
          par1: 2.2,
          par2: -0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'D1',
          par1: 2.2,
          par2: 0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
      } else {
        stor.repere.add({
          type: 'point',
          nom: 'C1',
          par1: 1.8,
          par2: -0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'D1',
          par1: 1.8,
          par2: 0.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
      }
      stor.repere.add({
        type: 'segment',
        nom: 's3',
        par1: 'C',
        par2: 'A',
        style: { couleur: couleurCorr, epaisseur: epaisTrait }
      })
      stor.repere.add({ type: 'segment', nom: 's4', par1: 'D', par2: 'B', style: { couleur: couleurCorr, epaisseur: epaisTrait } })
      stor.repere.add({ type: 'segment', nom: 's5', par1: 'C1', par2: 'E', style: { couleur: couleurCorr, epaisseur: epaisTrait } })
      stor.repere.add({ type: 'segment', nom: 's6', par1: 'D1', par2: 'F', style: { couleur: couleurCorr, epaisseur: epaisTrait } })
      stor.repere.add({
        type: 'point',
        nom: String(stor.bornesRep[0]),
        par1: -2,
        par2: -0.5,
        fixe: true,
        visible: true,
        etiquette: true,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 15, decal: [0, 35] }
      })
      stor.repere.add({
        type: 'point',
        nom: String(stor.bornesRep[1]),
        par1: 2,
        par2: -0.5,
        fixe: true,
        visible: true,
        etiquette: true,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 15, decal: [0, 35] }
      })
    }
    stor.repere.construit()
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    if ((me.questionCourante % ds.nbetapes) === 1) {
      j3pAffiche(zoneExpli2, '', ds.textes.corr3, { i: stor.intervalle })
    } else {
      if (stor.typeIneg === 'encadrement') {
        j3pAffiche(zoneExpli2, '', ds.textes.corr6, { i: stor.inegalite_rep.replace('<', '< ') })
      } else {
        j3pAffiche(zoneExpli2, '', ds.textes.corr7, { i: stor.inegalite_rep })
      }
    }
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 2,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      typeIneg: ['alea', 'alea', 'alea'], // autre type : "inferieur", "superieur", "encadrement"
      borne_inf: '[-10;10]',
      borne_sup: '[-10;10]',

      textes: {
        titre_exo: 'Inégalités et intervalles',
        consigne1: 'Traduis l’inégalité $£i$ par un intervalle&nbsp;:',
        consigne2: 'Traduis l’encadrement $£i$ par un intervalle&nbsp;:',
        consigne3: 'Traduis le fait que $£x$ appartienne à l’intervalle $£i$ par une inégalité ou un encadrement&nbsp;:',
        comment1: 'Il y a un problème de crochet(s) !',
        comment2: 'La réponse attendue doit être un intervalle, ce qui n’est pas le cas !',
        comment3: 'Il y a un problème de signe(s) d’inégalité !',
        comment4: 'Le séparateur dans un intervalle doit être un point-virgule&nbsp;!',
        comment5: 'Du côté de l’infini, le crochet est toujours vers l’extérieur&nbsp;!',
        comment6: 'L’infini n’étant pas un nombre il ne peut être écrit dans une inégalité&nbsp;!',
        corr1: 'On peut représenter sur l’axe des réels l’ensemble des réels $x$ vérifiant l’inégalité&nbsp;:',
        corr2: 'On peut représenter sur l’axe des réels l’ensemble des réels $x$ vérifiant l’encadrement&nbsp;:',
        corr3: 'L’intervalle recherché est donc $£i$.',
        corr4: 'On peut représenter sur l’axe des réels l’ensemble des réels $£x$ vérifiant l’inégalité&nbsp;:',
        corr5: 'On peut représenter sur l’axe des réels l’ensemble des réels $£x$ vérifiant l’encadrement&nbsp;:',
        corr6: 'Cela donne alors l’encadrement $£i$.',
        corr7: 'Cela donne alors l’inégalité $£i$.'
      },

      pe: 0
    }
  }

  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })

        // par convention, me.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)
        stor.tous_alea = true
        for (let k = 0; k < ds.nbrepetitions; k++) {
          if ((ds.typeIneg[k] === 'inferieur') || (ds.typeIneg[k] === 'superieur') || (ds.typeIneg[k] === 'encadrement') || (ds.typeIneg[k] === 'inférieur') || (ds.typeIneg[k] === 'supérieur')) {
            stor.tous_alea = false
          }
        }
        const tabInit = j3pShuffle(['inferieur', 'superieur', 'encadrement'])
        stor.tableau_type_ineg = []
        if (stor.tous_alea) {
          for (let k = 0; k < ds.nbrepetitions; k++) {
            stor.tableau_type_ineg[k] = tabInit[k % 3]
          }
        } else {
          for (let k = 0; k < ds.nbrepetitions; k++) {
            if ((ds.typeIneg[k] === 'inferieur') || (ds.typeIneg[k] === 'superieur') || (ds.typeIneg[k] === 'encadrement') || (ds.typeIneg[k] === 'inférieur') || (ds.typeIneg[k] === 'supérieur')) {
              if (ds.typeIneg[k] === 'inférieur') {
                stor.tableau_type_ineg[k] = 'inferieur'
              } else if (ds.typeIneg[k] === 'supérieur') {
                stor.tableau_type_ineg[k] = 'superieur'
              } else {
                stor.tableau_type_ineg[k] = ds.typeIneg[k]
              }
            } else {
              stor.tableau_type_ineg[k] = tabInit[k % 3]
            }
          }
        }

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        // A commenter si besoin
        me.videLesZones()
      }

      const typeIneg = stor.tableau_type_ineg[Math.floor((me.questionCourante - 1) / ds.nbetapes)]
      stor.typeIneg = typeIneg
      let a, b
      do {
        a = j3pGetRandomInt(ds.borne_inf)
        b = j3pGetRandomInt(ds.borne_sup)
      } while (a >= b)
      const tabInf = ['<', '\\leq']
      const tabSup = ['>', '\\geq']
      let signe1
      let signe2
      let consigne1
      stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
      for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

      if ((me.questionCourante % ds.nbetapes) === 1) {
        // Etape 1
        let inegaliteInit
        if (typeIneg === 'encadrement') {
          // on choisi les deux signes d’inégalité'
          signe1 = j3pRandomTab(tabInf, [0.5, 0.5])
          signe2 = j3pRandomTab(tabInf, [0.5, 0.5])
          inegaliteInit = (a) + signe1 + ' x ' + signe2 + (b)
          consigne1 = ds.textes.consigne2
          stor.signe1 = signe1
          stor.signe2 = signe2
        } else if (typeIneg === 'inferieur') {
          // on choisi le signe d’inégalité'
          signe1 = j3pRandomTab(tabInf, [0.5, 0.5])
          inegaliteInit = 'x ' + signe1 + (b)
          consigne1 = ds.textes.consigne1
          stor.signe1 = signe1
        } else if (typeIneg === 'superieur') {
          // on choisi le signe d’inégalité'
          signe1 = j3pRandomTab(tabSup, [0.5, 0.5])
          inegaliteInit = 'x ' + signe1 + (a)
          consigne1 = ds.textes.consigne1
          stor.signe1 = signe1
        }
        j3pAffiche(stor.zoneCons1, '', consigne1, { i: inegaliteInit })
        const elt = j3pAffiche(stor.zoneCons2, '', '$x\\in$&1&', { inputmq1: { texte: '' } })
        stor.zoneInput = elt.inputmqList[0]
        mqRestriction(stor.zoneInput, '\\d.,+-[];<>', {
          commandes: ['inf', 'R', 'infegal', 'supegal']
        })
        stor.zoneInput.typeReponse = ['texte']
        stor.laPalette = j3pAddElt(stor.zoneCons3, 'div')
        j3pStyle(stor.laPalette, { paddingTop: '15px' })
        j3pPaletteMathquill(stor.laPalette, stor.zoneInput, {
          liste: ['inf', 'R', 'infegal', 'supegal', '[', ']']
        })
        const crochetRep = []
        const bornesRep = []
        if (typeIneg === 'superieur') {
          // l’intervalle sera de la forme [a;+\infty[ ou ]a;+\infty['
          if (signe1 === '>') {
            stor.zoneInput.reponse = [']' + a + ';+\\infty[']
            stor.tab_pb_crochet = ['[' + a + ';+\\infty[', '[' + a + ';+\\infty]', ']' + a + ';+\\infty]']
            if (a > 0) {
              stor.zoneInput.reponse.push(']+' + a + ';+\\infty[')
              stor.tab_pb_crochet.push('[+' + a + ';+\\infty[', '[+' + a + ';+\\infty]', ']+' + a + ';+\\infty]')
            }
            crochetRep[0] = ']'
          } else {
            stor.zoneInput.reponse = ['[' + a + ';+\\infty[']
            stor.tab_pb_crochet = [']' + a + ';+\\infty[', ']' + a + ';+\\infty]', '[' + a + ';+\\infty]']
            if (a > 0) {
              stor.zoneInput.reponse.push('[+' + a + ';+\\infty[')
              stor.tab_pb_crochet.push(']+' + a + ';+\\infty[', ']+' + a + ';+\\infty]', '[+' + a + ';+\\infty]')
            }
            crochetRep[0] = '['
          }
          crochetRep[1] = '['
          bornesRep[0] = a
          bornesRep[1] = '+\\infty'
          stor.intervalle = crochetRep[0] + a + ';+\\infty' + crochetRep[1]
        } else if (typeIneg === 'inferieur') {
          // l’intervalle sera de la forme ]-\infty;b[ ou ]-\infty;b]'
          if (signe1 === '<') {
            stor.zoneInput.reponse = [']-\\infty;' + b + '[']
            stor.tab_pb_crochet = [']-\\infty;' + b + ']', '[-\\infty;' + b + ']', '[-\\infty;' + b + ']']
            if (b > 0) {
              stor.zoneInput.reponse.push(']-\\infty;+' + b + '[')
              stor.tab_pb_crochet.push(']-\\infty;+' + b + ']', '[-\\infty;+' + b + ']', '[-\\infty;+' + b + '[')
            }
            crochetRep[1] = '['
          } else {
            stor.zoneInput.reponse = [']-\\infty;' + b + ']']
            stor.tab_pb_crochet = [']-\\infty;' + b + '[', '[-\\infty;' + b + '[', '[-\\infty;' + b + ']']
            if (b > 0) {
              stor.zoneInput.reponse.push(']-\\infty;+' + b + ']')
              stor.tab_pb_crochet.push(']-\\infty;+' + b + '[', '[-\\infty;+' + b + '[', '[-\\infty;+' + b + ']')
            }
            crochetRep[1] = ']'
          }
          crochetRep[0] = ']'
          bornesRep[0] = '-\\infty'
          bornesRep[1] = b
          stor.intervalle = crochetRep[0] + '-\\infty;' + b + crochetRep[1]
        } else if (typeIneg === 'encadrement') {
          // l’intervalle sera de la forme ]a;b[ ou ]a;b] ou [a;b[ ou [a;b]'
          if (signe1 === '<') {
            if (signe2 === '<') {
              stor.zoneInput.reponse = [']' + a + ';' + b + '[']
              stor.tab_pb_crochet = [']' + a + ';' + b + ']', '[' + a + ';' + b + ']', '[' + a + ';' + b + '[']
              if (a > 0) {
                if (b > 0) {
                  stor.zoneInput.reponse.push(']+' + a + ';' + b + '[', ']+' + a + ';+' + b + '[', ']' + a + ';+' + b + '[')
                  stor.tab_pb_crochet.push(']+' + a + ';' + b + ']', ']+' + a + ';+' + b + ']', ']' + a + ';+' + b + ']', '[+' + a + ';' + b + ']', '[+' + a + ';+' + b + ']', '[' + a + ';+' + b + ']', '[+' + a + ';' + b + '[', '[+' + a + ';+' + b + '[', '[' + a + ';+' + b + '[')
                } else {
                  stor.zoneInput.reponse.push(']+' + a + ';' + b + '[')
                  stor.tab_pb_crochet.push(']+' + a + ';' + b + ']', '[+' + a + ';' + b + ']', '[+' + a + ';' + b + '[')
                }
              } else {
                stor.zoneInput.reponse.push(']' + a + ';+' + b + '[')
                stor.tab_pb_crochet.push(']' + a + ';+' + b + ']', '[' + a + ';+' + b + ']', '[' + a + ';+' + b + '[')
              }
              crochetRep[1] = '['
            } else {
              stor.zoneInput.reponse = [']' + a + ';' + b + ']']
              stor.tab_pb_crochet = [']' + a + ';' + b + '[', '[' + a + ';' + b + ']', '[' + a + ';' + b + '[']
              if (a > 0) {
                if (b > 0) {
                  stor.zoneInput.reponse.push(']+' + a + ';' + b + ']', ']+' + a + ';+' + b + ']', ']' + a + ';+' + b + ']')
                  stor.tab_pb_crochet.push(']+' + a + ';' + b + '[', ']+' + a + ';+' + b + '[', ']' + a + ';+' + b + '[', '[+' + a + ';' + b + ']', '[+' + a + ';+' + b + ']', '[' + a + ';+' + b + ']', '[+' + a + ';' + b + '[', '[+' + a + ';+' + b + '[', '[' + a + ';+' + b + '[')
                } else {
                  stor.zoneInput.reponse.push(']+' + a + ';' + b + ']')
                  stor.tab_pb_crochet.push(']+' + a + ';' + b + '[', '[+' + a + ';' + b + ']', '[+' + a + ';' + b + '[')
                }
              } else {
                stor.zoneInput.reponse.push(']' + a + ';+' + b + ']')
                stor.tab_pb_crochet.push(']' + a + ';+' + b + '[', '[' + a + ';+' + b + ']', '[' + a + ';+' + b + '[')
              }
              crochetRep[1] = ']'
            }
            crochetRep[0] = ']'
          } else {
            if (signe2 === '<') {
              stor.zoneInput.reponse = ['[' + a + ';' + b + '[']
              stor.tab_pb_crochet = [']' + a + ';' + b + '[', '[' + a + ';' + b + ']', ']' + a + ';' + b + ']']
              if (a > 0) {
                if (b > 0) {
                  stor.zoneInput.reponse.push('[+' + a + ';' + b + '[', '[+' + a + ';+' + b + '[', '[' + a + ';+' + b + '[')
                  stor.tab_pb_crochet.push(']+' + a + ';' + b + ']', ']+' + a + ';+' + b + ']', ']' + a + ';+' + b + ']', '[+' + a + ';' + b + ']', '[+' + a + ';+' + b + ']', '[' + a + ';+' + b + ']', ']+' + a + ';' + b + '[', ']+' + a + ';+' + b + '[', ']' + a + ';+' + b + '[')
                } else {
                  stor.zoneInput.reponse.push('[+' + a + ';' + b + '[')
                  stor.tab_pb_crochet.push(']+' + a + ';' + b + ']', '[+' + a + ';' + b + ']', ']+' + a + ';' + b + '[')
                }
              } else {
                stor.zoneInput.reponse.push('[' + a + ';+' + b + '[')
                stor.tab_pb_crochet.push(']' + a + ';+' + b + ']', '[' + a + ';+' + b + ']', ']' + a + ';+' + b + '[')
              }
              crochetRep[1] = '['
            } else {
              stor.zoneInput.reponse = ['[' + a + ';' + b + ']']
              stor.tab_pb_crochet = [']' + a + ';' + b + ']', '[' + a + ';' + b + '[', ']' + a + ';' + b + '[']
              if (a > 0) {
                if (b > 0) {
                  stor.zoneInput.reponse.push('[+' + a + ';' + b + ']', '[+' + a + ';+' + b + ']', '[' + a + ';+' + b + ']')
                  stor.tab_pb_crochet.push(']+' + a + ';' + b + ']', ']+' + a + ';+' + b + ']', ']' + a + ';+' + b + ']', ']+' + a + ';' + b + '[', ']+' + a + ';+' + b + '[', ']' + a + ';+' + b + '[', '[+' + a + ';' + b + '[', '[+' + a + ';+' + b + '[', '[' + a + ';+' + b + '[')
                } else {
                  stor.zoneInput.reponse.push('[+' + a + ';' + b + ']')
                  stor.tab_pb_crochet.push(']+' + a + ';' + b + ']', ']+' + a + ';' + b + '[', '[+' + a + ';' + b + '[')
                }
              } else {
                stor.zoneInput.reponse.push('[' + a + ';+' + b + ']')
                stor.tab_pb_crochet.push(']' + a + ';+' + b + ']', ']' + a + ';+' + b + '[', '[' + a + ';+' + b + '[')
              }
              crochetRep[1] = ']'
            }
            crochetRep[0] = '['
          }
          bornesRep[0] = a
          bornesRep[1] = b
          stor.intervalle = crochetRep[0] + a + ';' + b + crochetRep[1]
        }
        stor.crochetRep = crochetRep
        stor.bornesRep = bornesRep
        me.logIfDebug('crochetRep:' + crochetRep + '   bornesRep:' + bornesRep)
      }

      if ((me.questionCourante % ds.nbetapes) === 0) {
        // Etape 2
        let monIntervalle
        let tabRep = []
        let tabErreur = []
        const tabPbInfini = []
        if (typeIneg === 'encadrement') {
          // on choisit les deux signes d’inégalité
          signe1 = j3pRandomTab(tabInf, [0.5, 0.5])
          signe2 = j3pRandomTab(tabInf, [0.5, 0.5])
          if (signe1 === '<') {
            if (signe2 === '<') {
              monIntervalle = ']' + a + ';' + b + '['
              tabRep = [a + '<x<' + b, b + '>x>' + a]
              tabErreur = [a + '\\le x<' + b, b + '>x\\ge' + a, a + '<x\\le' + b, b + '\\ge x>' + a, a + '\\le x\\le' + b, b + '\\ge x\\ge' + a]
              if (a > 0) {
                // on accepte le signe +
                if (b > 0) {
                  tabRep.push('+' + a + '<x<' + b, b + '>x>+' + a, '+' + a + '<x<+' + b, '+' + b + '>x>+' + a, a + '<x<+' + b, '+' + b + '>x>' + a)
                  tabErreur.push('+' + a + '\\le x<' + b, b + '>x\\ge+' + a, '+' + a + '\\le x<+' + b, '+' + b + '>x\\ge+' + a, a + '\\le x<+' + b, '+' + b + '>x\\ge' + a, '+' + a + '<x\\le' + b, b + '\\ge x>+' + a, '+' + a + '<x\\le+' + b, '+' + b + '\\ge x>+' + a, a + '<x\\le+' + b, '+' + b + '\\ge x>' + a, '+' + a + '\\le x\\le' + b, b + '\\ge x\\ge+' + a, '+' + a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge+' + a, a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge' + a)
                } else {
                  tabRep.push('+' + a + '<x<' + b, b + '>x>+' + a)
                  tabErreur.push('+' + a + '\\le x<' + b, b + '>x\\ge+' + a, '+' + a + '<x\\le' + b, b + '\\ge x>+' + a, '+' + a + '\\le x\\le' + b, b + '\\ge x\\ge+' + a)
                }
              } else {
                if (b > 0) {
                  tabRep.push(a + '<x<+' + b, '+' + b + '>x>' + a)
                  tabErreur.push(a + '\\le x<+' + b, '+' + b + '>x\\ge' + a, a + '<x\\le+' + b, '+' + b + '\\ge x>' + a, a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge' + a)
                }
              }
            } else {
              monIntervalle = ']' + a + ';' + b + ']'
              tabRep = [a + '<x\\le' + b, b + '\\ge x>' + a]
              tabErreur = [a + '\\le x' + b, b + '>x\\ge' + a, a + '<x<' + b, b + '>x>' + a, a + '\\le x\\le' + b, b + '\\ge x\\ge' + a]
              if (a > 0) {
                // on accepte le signe +
                if (b > 0) {
                  tabRep.push('+' + a + '<x\\le' + b, b + '\\ge x>+' + a, '+' + a + '<x\\le+' + b, '+' + b + '\\ge x>+' + a, a + '<x\\le+' + b, '+' + b + '\\ge x>' + a)
                  tabErreur.push('+' + a + '\\le x<' + b, b + '>x\\ge+' + a, '+' + a + '\\le x<+' + b, '+' + b + '>x\\ge+' + a, a + '\\le x<+' + b, '+' + b + '>x\\ge' + a, '+' + a + '<x<' + b, b + '>x>+' + a, '+' + a + '<x<+' + b, '+' + b + '>x>+' + a, a + '<x<+' + b, '+' + b + '>x>' + a, '+' + a + '\\le x\\le' + b, b + '\\ge x\\ge+' + a, '+' + a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge+' + a, a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge' + a)
                } else {
                  tabRep.push('+' + a + 'x\\le' + b, b + '\\ge x>+' + a)
                  tabErreur.push('+' + a + '\\le x<' + b, b + '>x\\ge+' + a, '+' + a + '<x<' + b, b + '>x>+' + a, '+' + a + '\\le x\\le' + b, b + '\\ge x\\ge+' + a)
                }
              } else {
                if (b > 0) {
                  tabRep.push(a + '<x\\le+' + b, '+' + b + '\\ge x>' + a)
                  tabErreur.push(a + '\\le x<+' + b, '+' + b + '>x\\ge' + a, a + '<x<+' + b, '+' + b + '>x>' + a, a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge' + a)
                }
              }
            }
          } else {
            if (signe2 === '<') {
              monIntervalle = '[' + a + ';' + b + '['
              tabRep = [a + '\\le x<' + b, b + '>x\\ge' + a]
              tabErreur = [a + '<x<' + b, b + '>x>' + a, a + '<x\\le' + b, b + '\\ge x>' + a, a + '\\le x\\le' + b, b + '\\ge x\\ge' + a]
              if (a > 0) {
                // on accepte le signe +
                if (b > 0) {
                  tabRep.push('+' + a + '\\le x<' + b, b + '>x\\ge+' + a, '+' + a + '\\le x<+' + b, '+' + b + '>x\\ge+' + a, a + '\\le x<+' + b, '+' + b + '>x\\ge ' + a)
                  tabErreur.push('+' + a + '<x<' + b, b + '>x>+' + a, '+' + a + '<x<+' + b, '+' + b + '>x>+' + a, a + '<x<+' + b, '+' + b + '>x>' + a, '+' + a + '<x\\le' + b, b + '\\ge x>+' + a, '+' + a + '<x\\le+' + b, '+' + b + '\\ge x>+' + a, a + '<x\\le+' + b, '+' + b + '\\ge x>' + a, '+' + a + '\\le x\\le' + b, b + '\\ge x\\ge+' + a, '+' + a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge+' + a, a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge' + a)
                } else {
                  tabRep.push('+' + a + '\\le x<' + b, b + '>x\\ge+' + a)
                  tabErreur.push('+' + a + '<x<' + b, b + '>x>+' + a, '+' + a + '<x\\le' + b, b + '\\ge x>+' + a, '+' + a + '\\le x\\le' + b, b + '\\ge x\\ge+' + a)
                }
              } else {
                if (b > 0) {
                  tabRep.push(a + '\\le x<+' + b, '+' + b + '>x\\ge ' + a)
                  tabErreur.push(a + '<x<+' + b, '+' + b + '>x>' + a, a + '<x\\le+' + b, '+' + b + '\\ge x>' + a, a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge' + a)
                }
              }
            } else {
              monIntervalle = '[' + a + ';' + b + ']'
              tabRep = [a + '\\le x\\le' + b, b + '\\ge x\\ge' + a]
              tabErreur = [a + '\\le x<' + b, b + '>x\\ge' + a, a + '<x\\le' + b, b + '\\ge x>' + a, a + '<x<' + b, b + '>x>' + a]
              if (a > 0) {
                // on accepte le signe +
                if (b > 0) {
                  tabRep.push('+' + a + '\\le x\\le ' + b, b + '\\ge x\\ge+' + a, '+' + a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge+' + a, a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge ' + a)
                  tabErreur.push('+' + a + '\\le x<' + b, b + '>x\\ge+' + a, '+' + a + '\\le x<+' + b, '+' + b + '>x\\ge+' + a, a + '\\le x<+' + b, '+' + b + '>x\\ge' + a, '+' + a + '<x\\le' + b, b + '\\ge x>+' + a, '+' + a + '<x\\le+' + b, '+' + b + '\\ge x>+' + a, a + '<x\\le+' + b, '+' + b + '\\ge x>' + a, '+' + a + '<x<' + b, b + '>x>+' + a, '+' + a + '<x<+' + b, '+' + b + '>x>+' + a, a + '<x<+' + b, '+' + b + '>x>' + a)
                } else {
                  tabRep.push('+' + a + '\\le x\\le' + b, b + '\\ge x\\ge+' + a)
                  tabErreur.push('+' + a + '\\le x<' + b, b + '>x\\ge+' + a, '+' + a + '<x\\le' + b, b + '\\ge x>+' + a, '+' + a + '<x<' + b, b + '>x>+' + a)
                }
              } else {
                if (b > 0) {
                  tabRep.push(a + '\\le x\\le+' + b, '+' + b + '\\ge x\\ge ' + a)
                  tabErreur.push(a + '\\le x<+' + b, '+' + b + '>x\\ge' + a, a + '<x\\le+' + b, '+' + b + '\\ge x>' + a, a + '<x<+' + b, '+' + b + '>x>' + a)
                }
              }
            }
          }
          stor.signe1 = signe1
          stor.signe2 = signe2
        } else if (typeIneg === 'superieur') {
          // on choisi le signe d’inégalité'
          signe1 = j3pRandomTab(tabSup, [0.5, 0.5])
          if (signe1 === '>') {
            monIntervalle = ']' + a + ';+\\infty['
            tabRep = ['x>' + a, a + '<x']
            tabErreur = [a + '\\le x', 'x\\ge' + a]
            tabPbInfini.push('+\\infty>x>' + a, a + '<x<+\\infty')
            if (a > 0) {
              tabRep.push('x>+' + a, '+' + a + '<x')
              tabErreur.push('+' + a + '\\le x', 'x\\ge+' + a)
              tabPbInfini.push('+\\infty>x>+' + a, '+' + a + '<x<+\\infty')
            }
          } else {
            monIntervalle = '[' + a + ';+\\infty['
            tabRep = ['x\\ge' + a, a + '\\le x']
            tabErreur = [a + '<x', 'x>' + a]
            tabPbInfini.push('+\\infty>x\\ge' + a, a + '\\le x<+\\infty')
            if (a > 0) {
              tabRep.push('x\\ge+' + a, '+' + a + '\\le x')
              tabErreur.push('+' + a + '<x', 'x>+' + a)
              tabPbInfini.push('+\\infty>x\\ge+' + a, '+' + a + '\\le x<+\\infty')
            }
          }
          stor.signe1 = signe1
        } else if (typeIneg === 'inferieur') {
          // on choisi le signe d’inégalité'
          signe1 = j3pRandomTab(tabInf, [0.5, 0.5])
          if (signe1 === '<') {
            monIntervalle = ']-\\infty;' + b + '['
            tabRep = ['x<' + b, b + '>x']
            tabErreur = ['x\\le' + b, b + '\\ge x']
            tabPbInfini.push('-\\infty<x<' + b, b + '>x>-\\infty')
            if (b > 0) {
              tabRep.push('x<+' + b, '+' + b + '>x')
              tabErreur.push('x\\le+' + b, '+' + b + '\\ge x')
              tabPbInfini.push('-\\infty<x<+' + b, '+' + b + '>x>-\\infty')
            }
          } else {
            monIntervalle = ']-\\infty;' + b + ']'
            tabRep = ['x\\le' + b, b + '\\ge x']
            tabErreur = ['x<' + b, b + '>x']
            tabPbInfini.push('-\\infty<x\\le' + b, b + '\\ge x>-\\infty')
            if (b > 0) {
              tabRep.push('x\\le+' + b, '+' + b + '\\ge x')
              tabErreur.push('x<+' + b, '+' + b + '>x')
              tabPbInfini.push('-\\infty<x\\le+' + b, '+' + b + '\\ge x>-\\infty')
            }
          }
          stor.signe1 = signe1
        }
        stor.bornesRep = []
        stor.bornesRep[0] = a
        stor.bornesRep[1] = b
        stor.inegalite_rep = tabRep[0]
        stor.monIntervalle = monIntervalle
        stor.tabErreur = tabErreur
        stor.tabPbInfini = tabPbInfini
        me.logIfDebug('monIntervalle:' + monIntervalle)
        me.logIfDebug('tabRep:' + tabRep + '       tabErreur:' + tabErreur)
        j3pAffiche(stor.zoneCons1, '', ds.textes.consigne3, { x: 'x', i: monIntervalle })
        const elt = j3pAffiche(stor.zoneCons2, '', '&1&', { inputmq1: { texte: '' } })
        stor.zoneInput = elt.inputmqList[0]
        mqRestriction(stor.zoneInput, '\\d.,+-[];x<>', {
          commandes: ['inf', 'R', 'infegal', 'supegal']
        })
        stor.zoneInput.typeReponse = ['texte']
        stor.zoneInput.reponse = tabRep
        stor.laPalette = j3pAddElt(stor.zoneCons3, 'div')
        j3pStyle(stor.laPalette, { paddingTop: '15px' })
        j3pPaletteMathquill(stor.laPalette, stor.zoneInput, {
          liste: ['inf', 'R', 'infegal', 'supegal', '[', ']']
        })
      }
      stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
      j3pFocus(stor.zoneInput)
      const zonesSaisie = [stor.zoneInput.id]
      stor.fctsValid = new ValidationZones({ parcours: me, zones: zonesSaisie })

      // Obligatoire
      me.finEnonce()
      break // case "enonce":
    }

    case 'correction': {
      const fctsValid = stor.fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      const maReponse = fctsValid.validationGlobale()
      const repEleve = fctsValid.zones.reponseSaisie[0]
      me.logIfDebug('repEleve : ' + repEleve)
      if ((!maReponse.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        j3pFocus(stor.zoneInput)
        me.afficheBoutonValider()
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (maReponse.bonneReponse) {
          me._stopTimer()
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          j3pEmpty(stor.laPalette)
          afficheCorrection(true)
          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()

            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.cacheBoutonValider()
            stor.zoneCorr.innerHTML += '<br>' + ds.textes.phrase2 + me.stockage[0]
            /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
            j3pDesactive(stor.zoneInput)
            stor.zoneInput.setAttribute('style', 'color:' + me.styles.cfaux)
            afficheCorrection(false)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            // on cherche à identifier les pbs
            if ((me.questionCourante % ds.nbetapes) === 1) {
              let pbCrochet = false
              for (let i = 0; i < stor.tab_pb_crochet.length; i++) {
                if (repEleve === stor.tab_pb_crochet[i]) {
                  pbCrochet = true
                }
              }
              // on vérifie qu’on a un crochet au début et à la fin'
              const char1 = repEleve.charAt(0)
              const charfin = repEleve.charAt(repEleve.length - 1)
              let crochetManquant = 0
              if ((char1 !== '[') && (char1 !== ']')) {
                crochetManquant++
              }
              if ((charfin !== '[') && (charfin !== ']')) {
                crochetManquant++
              }
              me.logIfDebug('crochetManquant :' + crochetManquant)
              stor.vrai_intervalle = true
              if (crochetManquant > 0) {
                // il n’y a pas de crochet et/ou à la fin'
                stor.vrai_intervalle = false
              } else {
                // on vérifie qu’il y a aussi un seul point-virgule'
                if (repEleve.split(';').length < 2) {
                  stor.vrai_intervalle = false
                }
              }
              if (!stor.vrai_intervalle && (repEleve !== '\\mathbb{R}')) {
                // peut-être l’élève a-t-il mis une virgule au lieu d’un point-virgule'
                if (repEleve.split(',').length === 2) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment4
                } else {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
                }
              } else {
                if (pbCrochet) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
                }
                if ((repEleve.includes('[-\\infty')) || (repEleve.includes('+\\infty]'))) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment5
                }
              }
            } else {
              let pbSigne = false
              for (let i = 0; i < stor.tabErreur.length; i++) {
                if (repEleve === stor.tabErreur[i]) {
                  pbSigne = true
                }
              }
              let pbInfiniPresent = false
              for (let i = 0; i < stor.tabPbInfini.length; i++) {
                if (repEleve === stor.tabPbInfini[i]) {
                  pbInfiniPresent = true
                }
              }
              if (pbSigne) {
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment3
              }
              if (pbInfiniPresent) {
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment6
              }
            }
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              afficheCorrection(maReponse.bonneReponse)
              j3pDesactive(stor.zoneInput)
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = (ds.nbetapes * me.score) / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
