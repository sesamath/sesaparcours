import { j3pAddElt, j3pFocus, j3pGetRandomInt, j3pPGCD, j3pRandomTab, j3pRestriction, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Avril 2019
        Dans cette section, on teste la connaissance des symboles <, >, <= et >=
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition, sauf en cas d’imprécision où on laisse une chance de plus'],
    ['typeNombres', 'fraction, racine et x', 'liste', 'les nombres comparés peuvent être de différentes formes suivant le niveau des élèves auxquels on présente cet exercice :<br/>- "fraction, racine et x" pour le lycée;<br/>- "décimaux" pour la fin du primaire et le début du collège;<br/>- "décimaux et relatifs" pour le début du collège;<br/>- "fractions et relatifs" pour le collège.', ['fraction, racine et x', 'décimaux', 'décimaux et relatifs', 'fractions et relatifs']]
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Connaître les symboles d’inégalités',
  // on donne les phrases de la consigne
  consigne1: 'Complète la phrase suivante en traduisant le symbole d’inégalité par son équivalent en français (attention à l’orthographe)&nbsp;:',
  consigne2: '$£a£s£b$ signifie que $£a$ est @1@ à $£b$.',
  // Les différentes réponses acceptées suivant les cas de figure
  rep1: 'strictement inférieur|strictement inferieur|strictement inférieure|strictement inferieure|strictement infèrieur|strictement infèrieure|strictement plus petit|plus petit strictement|inférieur strictement|infèrieur strictement|inferieur strictement|inférieure strictement|infèrieure strictement|inferieure strictement',
  rep2: 'inférieur ou égal|inferieur ou égal|inférieure ou égal|inferieure ou égal|inférieur ou egal|inferieur ou egal|inférieure ou egal|inferieure ou egal|inférieur ou égale|inferieur ou égale|inférieure ou égale|inferieure ou égale|infèrieur ou égal|infèrieure ou égal|infèrieur ou egal|infèrieure ou egal|infèrieur ou égale|infèrieure ou égale|plus petit ou égal|plus petit ou egal|plus petit ou égale|égal ou plus petit|égal ou inférieur|égal ou infèrieur|égal ou inferieur|égal ou inférieure|égal ou infèrieure|égal ou inferieure|égale ou inférieur|égale ou infèrieur|égale ou inferieur|égale ou inférieure|égal ou infèrieure|égale ou inferieure|egal ou inférieur|egal ou infèrieur|egal ou inferieur|egal ou inférieure|egal ou infèrieure|egal ou inferieure',
  rep3: 'strictement supérieur|strictement superieur|strictement supérieure|strictement superieure|strictement supèrieur|strictement supèrieure|strictement plus grand|plus grand strictement|supérieur strictement|supèrieur strictement|superieur strictement|supérieure strictement|supèrieure strictement|superieure strictement',
  rep4: 'supérieur ou égal|superieur ou égal|supérieure ou égal|superieure ou égal|supérieur ou egal|superieur ou egal|supérieure ou egal|superieure ou egal|supérieur ou égale|superieur ou égale|supérieure ou égale|superieure ou égale|supèrieur ou égal|supèrieure ou égal|supèrieur ou egal|supèrieure ou egal|supèrieur ou égale|supèrieure ou égale|plus grand ou égal|plus grand ou egal|plus grand ou égale|égal ou plus grand|égal ou supérieur|égal ou supèrieur|égal ou superieur|égal ou supérieure|égal ou supèrieure|égal ou superieure|égale ou supérieur|égale ou supèrieur|égale ou superieur|égale ou supérieure|égal ou supèrieure|égale ou superieure|egal ou supérieur|egal ou supèrieur|egal ou superieur|egal ou supérieure|egal ou supèrieure|egal ou superieure',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Cette réponse est imprécise&nbsp;!',
  inferieur: 'inférieur|inferieur|inferieure|inferieure|plus petit',
  superieur: 'supérieur|superieur|superieure|superieure|plus grand',
  // et les phrases utiles pour les explications de la réponse
  corr1: '$£a$ est strictement inférieur à $£b$.',
  corr2: '$£a$ est inférieur ou égal à $£b$.',
  corr3: '$£a$ est strictement supérieur à $£b$.',
  corr4: '$£a$ est supérieur ou égal à $£b$.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    const laCorr1 = textes['corr' + (stor.choixSigne + 1)]
    j3pAffiche(zoneExpli, '', laCorr1, {
      a: stor.nb1,
      b: stor.nb2,
      s: stor.tabSignesInit[stor.choixSigne]
    })
  }
  function enonceInitFirst () {
    ds = me.donneesSection

    // Construction de la page
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
    me.afficheTitre(textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

    stor.tabSignesInit = ['<', '\\le', '>', '\\ge']
    stor.tabSignes = [...stor.tabSignesInit]
    stor.tabChoixSqrt = [2, 3, 5]
  }
  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1)
    if (stor.tabSignes.length === 0) {
      stor.tabSignes = [...stor.tabSignesInit]
    }
    let pioche = j3pGetRandomInt(0, (stor.tabSignes.length - 1))
    const leSigne = stor.tabSignes[pioche]
    stor.tabSignes.splice(pioche, 1)
    stor.choixSigne = stor.tabSignesInit.indexOf(leSigne)
    let nume, den, pgcdNumeDen, plusOuMoins
    switch (ds.typeNombres) {
      case 'fraction, racine et x':
        if ((ds.nbrepetitions > 2) && (me.questionCourante <= 2)) {
          // Premier cas avec une fraction
          if (me.questionCourante === 1) {
            do {
              nume = j3pGetRandomInt(1, 12)
              den = j3pRandomTab([3, 6, 7, 9], [0.25, 0.25, 0.25, 0.25])
              pgcdNumeDen = j3pPGCD(nume, den)
            } while (pgcdNumeDen !== 1)
            stor.nb1 = '\\frac{' + nume + '}{' + den + '}'
            stor.nb1 = j3pGetLatexProduit(stor.nb1, '-1')
            stor.nb2 = (stor.choixSigne <= 1) ? j3pGetLatexSomme(stor.nb1, '\\frac{1}{2}') : j3pGetLatexSomme(stor.nb1, '-\\frac{1}{2}')
          } else {
            // Deuxième cas cas de figure seront avec des constantes du style \\sqrt{2}
            pioche = j3pGetRandomInt(0, (stor.tabChoixSqrt.length - 1))
            const leNB = stor.tabChoixSqrt[pioche]
            stor.tabChoixSqrt.splice(pioche, 1)
            stor.nb1 = '\\sqrt{' + leNB + '}'
            stor.nb2 = (stor.choixSigne <= 1) ? Math.ceil(Math.sqrt(leNB)) : Math.floor(Math.sqrt(leNB))
          }
        } else {
          stor.nb1 = 'x'
          stor.nb2 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 9)
        }
        break
      case 'décimaux':
        do {
          stor.nb1 = j3pGetRandomInt(1200, 2300)
        } while (stor.nb1 % 10 === 0)
        stor.nb1 = stor.nb1 / 1000
        stor.nb2 = (stor.choixSigne <= 1) ? stor.nb1 + j3pGetRandomInt(0, 1) * 0.1 + j3pGetRandomInt(1, 6) * 0.01 : stor.nb1 - j3pGetRandomInt(1, 6) * 0.01 - j3pGetRandomInt(0, 1) * 0.1
        stor.nb2 = Math.round(1000 * stor.nb2) / 1000
        break
      case 'décimaux et relatifs':
        do {
          stor.nb1 = j3pGetRandomInt(120, 230)
        } while (stor.nb1 % 10 === 0)
        plusOuMoins = j3pGetRandomInt(0, 1) * 2 - 1
        stor.nb1 = stor.nb1 / 100 * plusOuMoins
        stor.nb2 = (stor.choixSigne <= 1) ? stor.nb1 + j3pGetRandomInt(2, 5) * 0.1 : stor.nb1 - j3pGetRandomInt(2, 5) * 0.1
        stor.nb2 = Math.round(100 * stor.nb2) / 100
        break
      default:// fractions et relatifs
        do {
          nume = j3pGetRandomInt(1, 12)
          den = j3pRandomTab([3, 6, 7, 9], [0.25, 0.25, 0.25, 0.25])
          pgcdNumeDen = j3pPGCD(nume, den)
        } while (pgcdNumeDen !== 1)
        stor.nb1 = '\\frac{' + nume + '}{' + den + '}'
        plusOuMoins = j3pGetRandomInt(0, 1) * 2 - 1
        stor.nb1 = j3pGetLatexProduit(stor.nb1, plusOuMoins)
        stor.nb2 = (stor.choixSigne <= 1) ? j3pGetLatexSomme(stor.nb1, '\\frac{1}{2}') : j3pGetLatexSomme(stor.nb1, '-\\frac{1}{2}')
        break
    }
    const elt = j3pAffiche(stor.zoneCons2, '', textes.consigne2,
      {
        a: stor.nb1,
        b: stor.nb2,
        s: stor.tabSignesInit[stor.choixSigne],
        input1: { texte: '', dynamique: true }
      })
    stor.zoneInput = elt.inputList[0]
    // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
    // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche
    j3pRestriction(stor.zoneInput, 'a-zéèùàç ')
    stor.zoneInput.typeReponse = ['texte']
    stor.zoneInput.reponse = textes['rep' + (stor.choixSigne + 1)].split('|')
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    stor.nbTentative = 0
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        const reponse = { aRepondu: false, bonneReponse: false }
        let posReponseSaisie, imprecis
        reponse.aRepondu = fctsValid.valideReponses()
        if (reponse.aRepondu) {
        // Je vire les éventuels espace au début et à la fin et les double-espaces
          let reponseSaisie = fctsValid.zones.reponseSaisie[0]
          while (reponseSaisie[0] === ' ') {
            reponseSaisie = reponseSaisie.substring(1)
          }
          while (reponseSaisie[reponseSaisie.length - 1] === ' ') {
            reponseSaisie = reponseSaisie.substring(0, reponseSaisie.length - 1)
          }
          while (reponseSaisie.indexOf('  ') > -1) {
            reponseSaisie = reponseSaisie.replace('  ', ' ')
          }
          // La première fois, s’il met juste inférieur ou supérieur, je lui dis que ce n’est pas précis. Après, je compte faux
          imprecis = false
          if (stor.choixSigne < 2) {
          // inférieur
            imprecis = (textes.inferieur.split('|').includes(reponseSaisie))
          } else {
            imprecis = (textes.superieur.split('|').includes(reponseSaisie))
          }
          // console.log("reponseSaisie:",reponseSaisie,"   imprecis:",imprecis)
          reponse.aRepondu = (!imprecis || (stor.nbTentative > 0))
          if (reponse.aRepondu) {
          // reponse = fctsValid.validationGlobale();
            posReponseSaisie = stor.zoneInput.reponse.indexOf(reponseSaisie)
            reponse.bonneReponse = (posReponseSaisie > -1)
            fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
            if (reponse.bonneReponse) fctsValid.zones.inputs[0].value = fctsValid.zones.inputs[0].reponse[0]
            fctsValid.coloreUneZone(stor.zoneInput.id)
          } else {
            stor.nbTentative++
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          // On peut ajouter un commentaire particulier.
          if (imprecis) {
            msgReponseManquante = textes.comment1
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            if (posReponseSaisie > 0) {
              afficheCorr(true)
            }
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (imprecis) {
                stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
