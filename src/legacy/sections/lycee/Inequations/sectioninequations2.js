import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pFocus, j3pFreezeElt, j3pGetRandomInt, j3pPaletteMathquill, j3pGetBornesIntervalle, j3pEmpty } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import { j3pCreeTexte } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    novembre 2013
    Résoudre une inéquation du style f(x)<g(x)

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.6, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'problème entre inégalité large et stricte' },
    { pe_3: 'problème sur le sens de l’inégalité' },
    { pe_4: 'problème avec les crochets' },
    { pe_5: 'confusion entre les symboles intersection et réunion' },
    { pe_6: 'Insuffisant' }
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  // les fonctions qui suivent sont celles dont on trace les courbes représentatives (utiles pour la correction)
  function f (num, x, alpha, beta) {
    if (num === 1) {
      return -0.0142857143 * Math.pow(x - alpha, 4) + 0.25 * Math.pow(x - alpha, 3) - 0.9285714286 * Math.pow(x - alpha, 2) - 0.75 * (x - alpha) + 2.4428571429 + beta
    } else if (num === 2) {
      return -0.0625 * Math.pow(x - alpha, 3) + 0.375 * Math.pow(x - alpha, 2) + 0.5 * (x - alpha) - 3 + beta
    } else if (num === 3) {
      return 0.1785714286 * Math.pow(x - alpha, 2) - 1.1785714286 * (x - alpha) + 0.6428571429 + beta
    } else if (num === 4) {
      return -0.3035714286 * Math.pow(x - alpha, 3) + 2.3035714286 * Math.pow(x - alpha, 2) - 2.3928571429 * (x - alpha) - 3 + beta
    } else if (num === 5) {
      return 0.25 * (x - alpha) - 1.5 + beta
    } else {
      return -0.15 * Math.pow(x - alpha, 2) + 0.85 * (x - alpha) + 0.3 + beta
    }
  }
  function decoupageCupcap (rep) {
    // cette fonction renvoit un tableau contenant les différentes parties séparées par \\cup ou par \\cap
    // ces différentes parties sont en principe des intervalles
    const cupCap = /\\cup|\\cap/g // new RegExp('\\\\cup|\\\\cap', 'g')
    return rep.split(cupCap)
  }
  function bonIntervalle (rep) {
    // rep est du texte (réponse de l’élève)
    // On teste si rep est bie un intervalle ou une réunon d’intervalle'
    // on vérifie aussi si l’élève ne met pas une virgule à la place du point-virgule quand il écrit un intervalle'
    let pbCrochet = false
    let pbPointVirgule = false
    const intervalleTab = decoupageCupcap(rep)
    for (let k = 0; k < intervalleTab.length; k++) {
      // pour chacun de ces éléments, on vérifie que c’est bien un intervalle'
      if (((intervalleTab[k].charAt(0) !== '[') && (intervalleTab[k].charAt(0) !== ']')) || ((intervalleTab[k].charAt(intervalleTab[k].length - 1) !== '[') && (intervalleTab[k].charAt(intervalleTab[k].length - 1) !== ']'))) {
        pbCrochet = true
      } else {
        if ((intervalleTab[k].indexOf(';') <= 1) || (intervalleTab[k].indexOf(';') >= intervalleTab[k].length - 2)) {
          // il n’y a pas de point-virgule bien placé, donc controlons s’il n’a pas mis une virgule à la place'
          if ((intervalleTab[k].indexOf(',') > 1) && (intervalleTab[k].indexOf(',') < intervalleTab[k].length - 2)) {
            // c’est donc qu’il met une virgule à la place d’un point-virgule'
            pbPointVirgule = true
          }
        }
      }
    }
    return { nonIntervalle: pbCrochet, separateurVirgule: pbPointVirgule }
  }

  function afficheCorrection (couleurCorr) {
    const largTrait = 4
    const alpha = stor.stockage[6]
    const beta = stor.stockage[7]
    const tabIntervalle = stor.stockage[4]// je récupère le ou les intervalle(s) solution(s)
    const tabPt = []
    // je récupère les abscisses qui apparaissent dans l’ensemble des sols + images'
    for (let i = 0; i < tabIntervalle.length; i++) {
      tabPt[i] = []
      tabPt[i][0] = tabIntervalle[i].split(';')[0]// abs de la borne inf de l’intervalle sol'
      tabPt[i][1] = tabIntervalle[i].split(';')[1]// abs de la borne sup de l’intervalle sol
      tabPt[i][2] = f(stor.stockage[8], tabPt[i][0], alpha, beta)// ord du pt de la courbe d’absisse la borne inf'
      tabPt[i][3] = f(stor.stockage[8], tabPt[i][1], alpha, beta)// ord du pt de la courbe d’absisse la borne sup'
    }
    j3pEmpty(stor.divRepere)
    for (let i = 0; i < tabIntervalle.length; i++) {
      stor.repere.add({
        type: 'point',
        nom: 'A' + i,
        par1: tabPt[i][0],
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'B' + i,
        par1: tabPt[i][0],
        par2: tabPt[i][2],
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 's' + i, par1: 'A' + i, par2: 'B' + i, style: { couleur: couleurCorr, epaisseur: 1 } })
      stor.repere.add({
        type: 'point',
        nom: 'C' + i,
        par1: tabPt[i][1],
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'D' + i,
        par1: tabPt[i][1],
        par2: tabPt[i][3],
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 'seg' + i, par1: 'C' + i, par2: 'D' + i, style: { couleur: couleurCorr, epaisseur: 1 } })
      stor.repere.add({
        type: 'courbe',
        nom: 'c_corr' + i,
        par1: stor.stockage[9][stor.stockage[10]],
        par2: Number(tabPt[i][0]),
        par3: Number(tabPt[i][1]),
        par4: 100,
        style: { couleur: couleurCorr, epaisseur: 4 }
      })
      stor.repere.add({
        type: 'segment',
        nom: 'segment' + i,
        par1: 'A' + i,
        par2: 'C' + i,
        style: { couleur: couleurCorr, epaisseur: largTrait }
      })
    }
    stor.repere.construit()
    stor.divRepere.style.marginTop = '10px'
    // je recrée les noms des courbes
    const uniterep = stor.stockage[11]
    const hauteurRepere = stor.stockage[13]
    const couleurFct1 = stor.stockage[14]
    const couleurFct2 = stor.stockage[15]
    const posNomCb1TabX = [(3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.7 + alpha) * uniterep, (3.7 + alpha) * uniterep, (3.7 + alpha) * uniterep, (4.8 + alpha) * uniterep, (4.8 + alpha) * uniterep, (-1.2 + alpha) * uniterep]
    const posNomCb1TabY = [(-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (0.5 + beta) * uniterep, (0.5 + beta) * uniterep, (0.5 + beta) * uniterep, (-1.2 + beta) * uniterep, (-1.2 + beta) * uniterep, (-0.2 + beta) * uniterep]
    j3pCreeTexte('idRepere', {
      x: -stor.stockage[16] * uniterep + 10 + posNomCb1TabX[stor.stockage[10]],
      y: hauteurRepere / 2 - posNomCb1TabY[stor.stockage[10]],
      texte: 'C',
      taille: 12,
      couleur: couleurFct1,
      italique: true,
      fonte: 'times new roman'
    })
    j3pCreeTexte('idRepere', {
      x: -stor.stockage[16] * uniterep + 10 + posNomCb1TabX[stor.stockage[10]] + 9,
      y: hauteurRepere / 2 - posNomCb1TabY[stor.stockage[10]] + 4,
      texte: stor.stockage[17],
      taille: 12,
      couleur: couleurFct1,
      italique: true,
      fonte: 'times new roman'
    })
    const posNomCb2TabX = [(3.7 + alpha) * uniterep, (4.8 + alpha) * uniterep, (3.2 + alpha) * uniterep, (1 + alpha) * uniterep, (-1.2 + alpha) * uniterep, (4.8 + alpha) * uniterep, (3.2 + alpha) * uniterep, (1 + alpha) * uniterep, (3.2 + alpha) * uniterep, (1 + alpha) * uniterep, (1 + alpha) * uniterep]
    const posNomCb2TabY = [(0.5 + beta) * uniterep, (-1.2 + beta) * uniterep, (2.3 + beta) * uniterep, (-1 + beta) * uniterep, (-0.2 + beta) * uniterep, (-1.2 + beta) * uniterep, (2.3 + beta) * uniterep, (-1 + beta) * uniterep, (2.3 + beta) * uniterep, (-1 + beta) * uniterep, (-1 + beta) * uniterep]
    j3pCreeTexte('idRepere', {
      x: -stor.stockage[16] * uniterep + 10 + posNomCb2TabX[stor.stockage[10]],
      y: hauteurRepere / 2 - posNomCb2TabY[stor.stockage[10]],
      texte: 'C',
      taille: 12,
      couleur: couleurFct2,
      italique: true,
      fonte: 'times new roman'
    })
    j3pCreeTexte('idRepere', {
      x: -stor.stockage[16] * uniterep + 10 + posNomCb2TabX[stor.stockage[10]] + 9,
      y: hauteurRepere / 2 - posNomCb2TabY[stor.stockage[10]] + 4,
      texte: stor.stockage[18],
      taille: 12,
      couleur: couleurFct2,
      italique: true,
      fonte: 'times new roman'
    })
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      seuilreussite: 0.8,
      seuilerreur: 0.6,
      // On donne la possibilité de laisser visible ou non les boutons union et inter de la palette (en plus du botuon ensemble vide)
      // par défaut les 3 boutons sont présents
      palette_complete: true,
      pe_1: 'Bien',
      pe_2: 'problème entre inégalité large et stricte',
      pe_3: 'problème sur le sens de l’inégalité',
      pe_4: 'problème avec les crochets',
      pe_5: 'confusion entre les symboles intersection et réunion',
      pe_6: 'Insuffisant',
      textes: {
        titre_exo: 'Inéquation du style f(x) &le; g(x)<br>Résolution graphique',
        consigne1: 'Dans le repère ci-contre, on a tracé les courbes représentant les fonctions $£f$ (en vert) et $£g$ (en violet) définies sur l’intervalle $£i$.',
        consigne2: 'Par lecture graphique, réponds à la question suivante.<br>L’ensemble des solutions de l’inéquation $£b$ sur l’intervalle $£c$ est :<br>&1&',
        comment3: 'Attention au sens de l’inégalité&nbsp;!',
        comment4: 'Attention aux crochets&nbsp;!',
        comment6: 'Ta réponse est incomplète&nbsp;!',
        comment7: 'Attention à l’écriture d’un intervalle&nbsp;!',
        comment8: 'Le séparateur dans un intervalle doit être un point-virgule&nbsp;!',
        comment10: 'Attention : $\\cap$ signifie "intersection"&nbsp;!',
        comment11: 'Tu n’as pas résolu la bonne inéquation&nbsp;!',
        comment12: 'L’inégalité est large&nbsp;!',
        comment13: 'L’inégalité est stricte&nbsp;!',
        comment14: 'Il y a un problème de crochet(s)&nbsp;!',
        comment15: 'Regarde l’explication&nbsp;!',
        comment_prop: '$£i$ est une mauvaise réponse&nbsp;!'
      }
    }
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.5 })
        // par convention, stor.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        stor.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        // ou dans le cas de presentation3
        // me.ajouteBoutons();
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      // on construit le repère, puis la courbe représentant la fonction
      // tout d’abord le nom de la fonction'
      stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
      stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
      stor.divRepere = j3pAddElt(stor.conteneurD, 'div')
      {
        const tabNomFct = ['f', 'g', 'h']
        const nomF = tabNomFct[j3pGetRandomInt(0, 2)]
        let nomG
        do {
          nomG = tabNomFct[j3pGetRandomInt(0, 2)]
        } while (nomF === nomG)
        // je dois choisir l’un des cas de figure
        const casFigure = j3pGetRandomInt(0, 10)
        // pour faire jouer l’aléatoire j’ai une translation de chaque courbe de référence par le vecteur alpha*vec(i)+beta*vec(j)'
        const tabAlpha = ['[-2;1]', '[-2;2]', '[-1;1]', '[-1;1]', '[-2;1]', '[0;2]', '[-2;1]', '[-1;2]', '[-2;1]', '[-2;1]', '[-1;1]']
        const tabBeta = ['[-2;2]', '[-2;2]', '[-2;0]', '[-2;2]', '[-2;2]', '[-2;0]', '[-1;0]', '[-2;3]', '[-2;1]', '[-3;2]', '[-1;4]']
        const tabAlphaChoix = j3pGetBornesIntervalle(tabAlpha[casFigure])
        const alpha = j3pGetRandomInt(tabAlphaChoix[0], tabAlphaChoix[1])
        const tabBetaChoix = j3pGetBornesIntervalle(tabBeta[casFigure])
        const beta = j3pGetRandomInt(tabBetaChoix[0], tabBetaChoix[1])
        me.logIfDebug('casFigure:' + casFigure + '  alpha : ' + alpha + '  beta:' + beta)
        // intervalle correspondant au domaine de définition
        const tabIntervalleMin = [-3, -2, -2, -3, -1, -3, -1, -3, -2, -2, -3]
        const tabIntervalleMax = [7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 7]
        const ledomaine = '[' + (tabIntervalleMin[casFigure] + alpha) + ';' + (tabIntervalleMax[casFigure] + alpha) + ']'
        // bornes du repère
        const absMin = Math.min(tabIntervalleMin[casFigure] + alpha, -1) - 2
        const absMax = tabIntervalleMax[casFigure] + alpha + 1
        const ordMin = -6
        const ordMax = 6
        const betaTxt = (beta >= 0) ? '+' + beta : beta
        // écriture des fonctons pour créer les courbes dans le repère
        let f1DeX, f2DeX, f3DeX, f4DeX, f5DeX, f6DeX
        if (alpha < 0) {
          f1DeX = '-0.0142857143*(x+' + (-alpha) + ')^4+0.25*(x+' + (-alpha) + ')^3-0.9285714286*(x+' + (-alpha) + ')^2-0.75*(x+' + (-alpha) + ')+2.4428571429' + betaTxt//
          f2DeX = '-0.0625*(x+' + (-alpha) + ')^3+0.375*(x+' + (-alpha) + ')^2+0.5*(x+' + (-alpha) + ')-3' + betaTxt// g
          f3DeX = '0.1785714286*(x+' + (-alpha) + ')^2-1.1785714286*(x+' + (-alpha) + ')+0.6428571429' + betaTxt// h
          f4DeX = '-0.3035714286*(x+' + (-alpha) + ')^3+2.3035714286*(x+' + (-alpha) + ')^2-2.3928571429*(x+' + (-alpha) + ')-3' + betaTxt// p
          f5DeX = '0.25*(x+' + (-alpha) + ')-1.5' + betaTxt// a
          f6DeX = '-0.15*(x+' + (-alpha) + ')^2+0.85*(x+' + (-alpha) + ')+0.3' + betaTxt// r
        } else {
          f1DeX = '-0.0142857143*(x-' + alpha + ')^4+0.25*(x-' + alpha + ')^3-0.9285714286*(x-' + alpha + ')^2-0.75*(x-' + alpha + ')+2.4428571429' + betaTxt//
          f2DeX = '-0.0625*(x-' + alpha + ')^3+0.375*(x-' + alpha + ')^2+0.5*(x-' + alpha + ')-3' + betaTxt// g
          f3DeX = '0.1785714286*(x-' + alpha + ')^2-1.1785714286*(x-' + alpha + ')+0.6428571429' + betaTxt// h
          f4DeX = '-0.3035714286*(x-' + alpha + ')^3+2.3035714286*(x-' + alpha + ')^2-2.3928571429*(x-' + alpha + ')-3' + betaTxt// p
          f5DeX = '0.25*(x-' + alpha + ')-1.5' + betaTxt// a
          f6DeX = '-0.15*(x-' + alpha + ')^2+0.85*(x-' + alpha + ')+0.3' + betaTxt// r
        }
        const tabNomFct1 = [f1DeX, f1DeX, f1DeX, f1DeX, f1DeX, f2DeX, f2DeX, f2DeX, f3DeX, f3DeX, f6DeX]
        const tabNomFct2 = [f2DeX, f3DeX, f4DeX, f5DeX, f6DeX, f3DeX, f4DeX, f5DeX, f4DeX, f5DeX, f5DeX]
        for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
        j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, {
          f: nomF,
          g: nomG,
          i: ledomaine
        })

        // construction du repère
        const uniterep = 30
        const largeurRepere = ((absMax - absMin) + 1) * uniterep
        const hauteurRepere = ((ordMax - ordMin) + 1) * uniterep
        const couleurFct1 = '#008000'
        const couleurFct2 = '#FF00FF'
        stor.repere = new Repere({
          idConteneur: stor.divRepere,
          aimantage: false,
          visible: true,
          larg: largeurRepere,
          haut: hauteurRepere,

          pasdunegraduationX: 1,
          pixelspargraduationX: uniterep,
          pasdunegraduationY: 1,
          pixelspargraduationY: uniterep,
          xO: -absMin * uniterep + 10,
          debuty: 0,
          negatifs: true,
          fixe: true,
          trame: true,
          objets: [
            {
              type: 'courbe',
              nom: 'c1',
              par1: tabNomFct1[casFigure],
              par2: (tabIntervalleMin[casFigure] + alpha),
              par3: (tabIntervalleMax[casFigure] + alpha),
              par4: 100,
              style: { couleur: couleurFct1, epaisseur: 2 }
            },
            {
              type: 'courbe',
              nom: 'c2',
              par1: tabNomFct2[casFigure],
              par2: (tabIntervalleMin[casFigure] + alpha),
              par3: (tabIntervalleMax[casFigure] + alpha),
              par4: 100,
              style: { couleur: couleurFct2, epaisseur: 2 }
            }
          ]
        })

        // console.log('tabNomFct1[casFigure]:', tabNomFct1[casFigure], (tabIntervalleMin[casFigure] + alpha), (tabIntervalleMax[casFigure] + alpha))
        stor.repere.construit()
        stor.divRepere.style.marginTop = '10px'
        stor.divRepere.style.textAlign = 'center'
        // je crée le nom des courbes
        const posNomCb1TabX = [(3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.7 + alpha) * uniterep, (3.7 + alpha) * uniterep, (3.7 + alpha) * uniterep, (4.8 + alpha) * uniterep, (4.8 + alpha) * uniterep, (-1.2 + alpha) * uniterep]
        const posNomCb1TabY = [(-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (0.5 + beta) * uniterep, (0.5 + beta) * uniterep, (0.5 + beta) * uniterep, (-1.2 + beta) * uniterep, (-1.2 + beta) * uniterep, (-0.2 + beta) * uniterep]
        j3pCreeTexte('idRepere', {
          x: -absMin * uniterep + 10 + posNomCb1TabX[casFigure],
          y: hauteurRepere / 2 - posNomCb1TabY[casFigure],
          texte: 'C',
          taille: 12,
          couleur: couleurFct1,
          italique: true,
          fonte: 'times new roman'
        })
        j3pCreeTexte('idRepere', {
          x: -absMin * uniterep + 10 + posNomCb1TabX[casFigure] + 9,
          y: hauteurRepere / 2 - posNomCb1TabY[casFigure] + 4,
          texte: nomF,
          taille: 12,
          couleur: couleurFct1,
          italique: true,
          fonte: 'times new roman'
        })
        const posNomCb2TabX = [(3.7 + alpha) * uniterep, (4.8 + alpha) * uniterep, (3.2 + alpha) * uniterep, (1 + alpha) * uniterep, (-1.2 + alpha) * uniterep, (4.8 + alpha) * uniterep, (3.2 + alpha) * uniterep, (1 + alpha) * uniterep, (3.2 + alpha) * uniterep, (1 + alpha) * uniterep, (1 + alpha) * uniterep]
        const posNomCb2TabY = [(0.5 + beta) * uniterep, (-1.2 + beta) * uniterep, (2.3 + beta) * uniterep, (-1 + beta) * uniterep, (-0.2 + beta) * uniterep, (-1.2 + beta) * uniterep, (2.3 + beta) * uniterep, (-1 + beta) * uniterep, (2.3 + beta) * uniterep, (-1 + beta) * uniterep, (-1 + beta) * uniterep]
        j3pCreeTexte('idRepere', {
          x: -absMin * uniterep + 10 + posNomCb2TabX[casFigure],
          y: hauteurRepere / 2 - posNomCb2TabY[casFigure],
          texte: 'C',
          taille: 12,
          couleur: couleurFct2,
          italique: true,
          fonte: 'times new roman'
        })
        j3pCreeTexte('idRepere', {
          x: -absMin * uniterep + 10 + posNomCb2TabX[casFigure] + 9,
          y: hauteurRepere / 2 - posNomCb2TabY[casFigure] + 4,
          texte: nomG,
          taille: 12,
          couleur: couleurFct2,
          italique: true,
          fonte: 'times new roman'
        })
        let laReponse = ''// intervalle ou réunion d’intervalles solution'
        let inegaliteInverse = ''// réponse à f(x)<g(x) dans le cas où l’inequation est f(x)>g(x)'
        let pbLargeStricte = ''// a résolu f(x)<=g(x) au lieu de f(x)<g(x)
        let tabIntervalles = []// les intervalles sans les crochets (sert à vérifier si ce n’est qu’un pb de crochets)'
        // la figure est entièrement construite
        const ineqLarge = ((me.questionCourante) === 1) ? j3pGetRandomInt(0, 1) : (stor.stockage[5] + 1) % 2
        const choixSigne = j3pGetRandomInt(0, 1)
        const monsigne = (choixSigne === 0) ? 'inf' : 'sup'
        let numf1
        //, numf2// numéros des deux fonctions dont on a tracé les courbes
        switch (casFigure) {
          case 0:
            numf1 = 1
            // numf2 = 2
            if (monsigne === 'inf') {
              if (ineqLarge === 0) {
                laReponse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                inegaliteInverse = ']' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-3 + alpha) + ';' + (-2 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
              } else {
                laReponse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
              }
              tabIntervalles = [(-3 + alpha) + ';' + (-2 + alpha), (2 + alpha) + ';' + (6 + alpha)]
            } else {
              if (ineqLarge === 0) {
                laReponse = ']' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
              } else {
                laReponse = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                pbLargeStricte = ']' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              }
              tabIntervalles = [(-2 + alpha) + ';' + (2 + alpha), (6 + alpha) + ';' + (7 + alpha)]
            }
            break
          case 1:
            numf1 = 1
            // numf2 = 3
            if (monsigne === 'inf') {
              if (ineqLarge === 0) {
                laReponse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                inegaliteInverse = ']' + (-1 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (-1 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
              } else {
                laReponse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                inegaliteInverse = '[' + (-1 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (-1 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
              }
              tabIntervalles = [(-2 + alpha) + ';' + (-1 + alpha), (2 + alpha) + ';' + (6 + alpha)]
            } else {
              if (ineqLarge === 0) {
                laReponse = ']' + (-1 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                pbLargeStricte = '[' + (-1 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
              } else {
                laReponse = '[' + (-1 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                pbLargeStricte = ']' + (-1 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              }
              tabIntervalles = [(-1 + alpha) + ';' + (2 + alpha), (6 + alpha) + ';' + (7 + alpha)]
            }
            break
          case 2:
            numf1 = 1
            // numf2 = 4
            if (monsigne === 'inf') {
              if (ineqLarge === 0) {
                laReponse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                inegaliteInverse = ']' + (-1 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (-1 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
              } else {
                laReponse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                inegaliteInverse = '[' + (-1 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (-1 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
              }
              tabIntervalles = [(-2 + alpha) + ';' + (-1 + alpha), (2 + alpha) + ';' + (6 + alpha)]
            } else {
              if (ineqLarge === 0) {
                laReponse = ']' + (-1 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                pbLargeStricte = '[' + (-1 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
              } else {
                laReponse = '[' + (-1 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                pbLargeStricte = ']' + (-1 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              }
              tabIntervalles = [(-1 + alpha) + ';' + (2 + alpha), (6 + alpha) + ';' + (7 + alpha)]
            }
            break
          case 3:
            numf1 = 1
            // numf2 = 5
            if (monsigne === 'inf') {
              if (ineqLarge === 0) {
                laReponse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                inegaliteInverse = ']' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-3 + alpha) + ';' + (-2 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
              } else {
                laReponse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
              }
              tabIntervalles = [(-3 + alpha) + ';' + (-2 + alpha), (2 + alpha) + ';' + (6 + alpha)]
            } else {
              if (ineqLarge === 0) {
                laReponse = ']' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
              } else {
                laReponse = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                pbLargeStricte = ']' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              }
              tabIntervalles = [(-2 + alpha) + ';' + (2 + alpha), (6 + alpha) + ';' + (7 + alpha)]
            }
            break
          case 4:
            numf1 = 1
            // numf2 = 6
            if (monsigne === 'inf') {
              if (ineqLarge === 0) {
                laReponse = ']' + (1 + alpha) + ';' + (6 + alpha) + '['
                inegaliteInverse = '[' + (-1 + alpha) + ';' + (1 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = ']' + (1 + alpha) + ';' + (6 + alpha) + '['
              } else {
                laReponse = '[' + (1 + alpha) + ';' + (6 + alpha) + ']'
                inegaliteInverse = '[' + (-1 + alpha) + ';' + (1 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = ']' + (1 + alpha) + ';' + (6 + alpha) + '['
              }
              tabIntervalles = [(1 + alpha) + ';' + (6 + alpha)]
            } else {
              if (ineqLarge === 0) {
                laReponse = '[' + (-1 + alpha) + ';' + (1 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = ']' + (1 + alpha) + ';' + (6 + alpha) + '['
                pbLargeStricte = '[' + (-1 + alpha) + ';' + (1 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
              } else {
                laReponse = '[' + (-1 + alpha) + ';' + (1 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (1 + alpha) + ';' + (6 + alpha) + ']'
                pbLargeStricte = '[' + (-1 + alpha) + ';' + (1 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              }
              tabIntervalles = [(-1 + alpha) + ';' + (1 + alpha), (6 + alpha) + ';' + (7 + alpha)]
            }
            break
          case 5:
            numf1 = 2
            // numf2 = 3
            if (monsigne === 'inf') {
              if (ineqLarge === 0) {
                laReponse = '[' + (-3 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = ']' + (2 + alpha) + ';' + (6 + alpha) + '['
                pbLargeStricte = '[' + (-3 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              } else {
                laReponse = '[' + (-3 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                pbLargeStricte = '[' + (-3 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              }
              tabIntervalles = [(-3 + alpha) + ';' + (2 + alpha), (6 + alpha) + ';' + (7 + alpha)]
            } else {
              if (ineqLarge === 0) {
                laReponse = ']' + (2 + alpha) + ';' + (6 + alpha) + '['
                inegaliteInverse = '[' + (-3 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (2 + alpha) + ';' + (6 + alpha) + ']'
              } else {
                laReponse = '[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                inegaliteInverse = '[' + (-3 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = ']' + (2 + alpha) + ';' + (6 + alpha) + '['
              }
              tabIntervalles = [(2 + alpha) + ';' + (6 + alpha)]
            }
            break
          case 6:
            numf1 = 2
            // numf2 = 4
            if (monsigne === 'inf') {
              if (ineqLarge === 0) {
                laReponse = '[' + (-1 + alpha) + ';' + (alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                inegaliteInverse = ']' + (alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-1 + alpha) + ';' + (alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
              } else {
                laReponse = '[' + (-1 + alpha) + ';' + (alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                inegaliteInverse = '[' + (alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-1 + alpha) + ';' + (alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
              }
              tabIntervalles = [(-1 + alpha) + ';' + (alpha), (2 + alpha) + ';' + (6 + alpha)]
            } else {
              if (ineqLarge === 0) {
                laReponse = ']' + (alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-1 + alpha) + ';' + (alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                pbLargeStricte = '[' + (alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
              } else {
                laReponse = '[' + (alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-1 + alpha) + ';' + (alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                pbLargeStricte = ']' + (alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              }
              tabIntervalles = [(alpha) + ';' + (2 + alpha), (6 + alpha) + ';' + (7 + alpha)]
            }
            break
          case 7:
            numf1 = 2
            // numf2 = 5
            if (monsigne === 'inf') {
              if (ineqLarge === 0) {
                laReponse = ']' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
              } else {
                laReponse = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                pbLargeStricte = ']' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              }
              tabIntervalles = [(-2 + alpha) + ';' + (2 + alpha), (6 + alpha) + ';' + (7 + alpha)]
            } else {
              if (ineqLarge === 0) {
                laReponse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                inegaliteInverse = ']' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-3 + alpha) + ';' + (-2 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
              } else {
                laReponse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
              }
              tabIntervalles = [(-3 + alpha) + ';' + (-2 + alpha), (2 + alpha) + ';' + (6 + alpha)]
            }
            break
          case 8:
            numf1 = 3
            // numf2 = 4
            if (monsigne === 'inf') {
              if (ineqLarge === 0) {
                laReponse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                inegaliteInverse = ']' + (-1 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (-1 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
              } else {
                laReponse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                inegaliteInverse = '[' + (-1 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (-1 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
              }
              tabIntervalles = [(-2 + alpha) + ';' + (-1 + alpha), (2 + alpha) + ';' + (6 + alpha)]
            } else {
              if (ineqLarge === 0) {
                laReponse = ']' + (-1 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + '[\\cup]' + (2 + alpha) + ';' + (6 + alpha) + '['
                pbLargeStricte = '[' + (-1 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
              } else {
                laReponse = '[' + (-1 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (-1 + alpha) + ']\\cup[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                pbLargeStricte = ']' + (-1 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              }
              tabIntervalles = [(-1 + alpha) + ';' + (2 + alpha), (6 + alpha) + ';' + (7 + alpha)]
            }
            break
          case 9:
            numf1 = 3
            // numf2 = 5
            if (monsigne === 'inf') {
              if (ineqLarge === 0) {
                laReponse = ']' + (2 + alpha) + ';' + (6 + alpha) + '['
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (8 + alpha) + ']'
                pbLargeStricte = ']' + (2 + alpha) + ';' + (6 + alpha) + '['
              } else {
                laReponse = '[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (8 + alpha) + ']'
                pbLargeStricte = ']' + (2 + alpha) + ';' + (6 + alpha) + '['
              }
              tabIntervalles = [(2 + alpha) + ';' + (6 + alpha)]
            } else {
              if (ineqLarge === 0) {
                laReponse = '[' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (8 + alpha) + ']'
                inegaliteInverse = ']' + (2 + alpha) + ';' + (6 + alpha) + '['
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (8 + alpha) + ']'
              } else {
                laReponse = '[' + (-2 + alpha) + ';' + (2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (8 + alpha) + ']'
                inegaliteInverse = '[' + (2 + alpha) + ';' + (6 + alpha) + ']'
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (8 + alpha) + ']'
              }
              tabIntervalles = [(-2 + alpha) + ';' + (2 + alpha), (6 + alpha) + ';' + (8 + alpha)]
            }
            break
          case 10:
            numf1 = 6
            // numf2 = 5
            if (monsigne === 'inf') {
              if (ineqLarge === 0) {
                laReponse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = ']' + (-2 + alpha) + ';' + (6 + alpha) + '['
                pbLargeStricte = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              } else {
                laReponse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                inegaliteInverse = '[' + (-2 + alpha) + ';' + (6 + alpha) + ']'
                pbLargeStricte = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
              }
              tabIntervalles = [(-3 + alpha) + ';' + (-2 + alpha), (6 + alpha) + ';' + (7 + alpha)]
            } else {
              if (ineqLarge === 0) {
                laReponse = ']' + (-2 + alpha) + ';' + (6 + alpha) + '['
                inegaliteInverse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + '[\\cup]' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = '[' + (-2 + alpha) + ';' + (6 + alpha) + ']'
              } else {
                laReponse = '[' + (-2 + alpha) + ';' + (6 + alpha) + ']'
                inegaliteInverse = '[' + (-3 + alpha) + ';' + (-2 + alpha) + ']\\cup[' + (6 + alpha) + ';' + (7 + alpha) + ']'
                pbLargeStricte = ']' + (-2 + alpha) + ';' + (6 + alpha) + '['
              }
              tabIntervalles = [(-2 + alpha) + ';' + (6 + alpha)]
            }
            break
        }
        me.logIfDebug('laReponse:' + laReponse + '  inegaliteInverse:' + inegaliteInverse + '  pbLargeStricte:' + pbLargeStricte + '   tabIntervalles:' + tabIntervalles)
        stor.stockage[1] = laReponse
        stor.stockage[2] = inegaliteInverse
        stor.stockage[3] = pbLargeStricte
        stor.stockage[4] = tabIntervalles
        stor.stockage[5] = ineqLarge
        stor.stockage[6] = alpha
        stor.stockage[7] = beta
        stor.stockage[8] = numf1
        stor.stockage[9] = tabNomFct1
        stor.stockage[10] = casFigure
        stor.stockage[11] = uniterep
        stor.stockage[12] = largeurRepere
        stor.stockage[13] = hauteurRepere
        stor.stockage[14] = couleurFct1
        stor.stockage[15] = couleurFct2
        stor.stockage[16] = absMin
        stor.stockage[17] = nomF
        stor.stockage[18] = nomG
        let monIneq
        // écriture de l’inéquation à résoudre'
        if (monsigne === 'inf') {
          if (ineqLarge === 0) {
            monIneq = nomF + '(x)< ' + nomG + '(x)'
          } else {
            monIneq = nomF + '(x)\\leq ' + nomG + '(x)'
          }
        } else {
          if (ineqLarge === 0) {
            monIneq = nomF + '(x)> ' + nomG + '(x)'
          } else {
            monIneq = nomF + '(x)\\geq ' + nomG + '(x)'
          }
        }
        const elt = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, {
          b: monIneq,
          c: ledomaine,
          inputmq1: { texte: '', maxchars: '20' }
        })
        stor.zoneInput = elt.inputmqList[0]
        stor.zonePalette = j3pAddElt(stor.zoneCons2, 'div', '', { style: { paddingTop: '10px' } })
        // création de la palette
        if (ds.palette_complete) {
          j3pPaletteMathquill(stor.zonePalette, stor.zoneInput, {
            liste: ['inter', 'union', 'vide', '[', ']']
          })
        } else {
          j3pPaletteMathquill(stor.zonePalette, stor.zoneInput, {
            liste: ['vide', '[', ']']
          })
        }
        stor.zonesRep = j3pAddElt(stor.conteneur, 'div', '', {
          style: {
            paddingTop: '50px',
            color: me.styles.cfaux
          }
        })
        mqRestriction(stor.zoneInput, '\\d.,+-[];', {
          commandes: (ds.palette_complete) ? ['inter', 'union', 'vide'] : ['vide']
        })
        stor.explications = j3pAddElt(stor.conteneurD, 'div')
        stor.correction = j3pAddElt(stor.conteneurD, 'div')
        j3pFocus(stor.zoneInput)
        /// //////////////////////////////////////
        /* FIN DU CODE PRINCIPAL DE LA SECTION */
        /// //////////////////////////////////////
        // Obligatoire
        me.finEnonce()
      }
      break // case "enonce":

    case 'correction':
      // je définis la couleur de la correction
      // me.cacheBoutonValider()
      // On teste si une réponse a été saisie
      {
        let reponseEleve1 = $(stor.zoneInput).mathquill('latex')
        const reponseEleve1Init = reponseEleve1
        if (reponseEleve1.includes(':')) {
          const partition = reponseEleve1.split(':')
          reponseEleve1 = ''
          for (let i = 0; i < partition.length; i++) {
            reponseEleve1 += partition[i]
          }
        }
        me.logIfDebug('valeur : ' + reponseEleve1 + '  bonneReponse:' + stor.stockage[1])
        // on souhaite accepte qu’un élève écrive +2 au lieu de 2'
        while (reponseEleve1.includes('[+')) {
          reponseEleve1 = reponseEleve1.replace('[+', '[')
        }
        while (reponseEleve1.includes(']+')) {
          reponseEleve1 = reponseEleve1.replace(']+', ']')
        }
        while (reponseEleve1.includes(';+')) {
          reponseEleve1 = reponseEleve1.replace(';+', ';')
        }
        me.logIfDebug('valeur après modif : ' + reponseEleve1 + '  bonneReponse:' + stor.stockage[1])
        if ((reponseEleve1 === '') && (!me.isElapsed)) {
          me.reponseManquante(stor.explications)
          $(stor.zoneInput).mathquill('write', '  ').focus()
        } else {
        // Une réponse a été saisie
          let cbon = (reponseEleve1 === stor.stockage[1])
          let k
          if (!cbon) {
          // on a aussi la possibilité d’écrire les réunion dans le désordre'
            const tabRepEleve = reponseEleve1.split('\\cup')
            const tabReponse = stor.stockage[1].split('\\cup')
            if (tabRepEleve.length === tabReponse.length) {
              let presence1 = true
              for (k = 0; k < tabRepEleve.length; k++) {
                presence1 = (presence1 && (tabReponse.includes(tabRepEleve[k])))
              }
              let presence2 = true
              for (k = 0; k < tabReponse.length; k++) {
                presence2 = (presence2 && (tabRepEleve.includes(tabReponse[k])))
              }
              if (presence1 || presence2) {
                cbon = true
              }
            }
          }
          let testIntervalle, confusionInterUnion, erreurInegalite, erreurLargeStricte, erreurCrochets
          if (!cbon) {
          // je cherche les différents types d’erreur'
          // ce qui est écrit n’est pas un intervalle ou une réunion d’intervalles ou le séparateur de l’intervalle n’est asp ; mais ,'
            testIntervalle = bonIntervalle(reponseEleve1)
            // testIntervalle a alors deux propriétés : nonIntervalle et separateurVirgule
            // confusion entre union et intersection :
            confusionInterUnion = false
            if (reponseEleve1.includes('cap')) {
            // on vérifie si la réponse est bonne en remplaçant \cap par \cup
              const newReponseEleve1 = reponseEleve1.split('cap').join('cup')
              confusionInterUnion = (newReponseEleve1 === stor.stockage[1])
              me.logIfDebug('newReponseEleve1:' + newReponseEleve1)
            }
            // pb sur le sens de l’inégalité'
            erreurInegalite = (reponseEleve1 === stor.stockage[2])
            // problème entre inégalité stricte et inégalite large
            erreurLargeStricte = (reponseEleve1 === stor.stockage[3])
            // problème de crochets autres que inégalité stricte/large
            erreurCrochets = false
            if (!erreurLargeStricte) {
            // tous les élémets de tabIntervalles doivent être présents
              erreurCrochets = true
              for (let i = 0; i < stor.stockage[4].length; i++) {
                erreurCrochets = (erreurCrochets && (reponseEleve1.indexOf(stor.stockage[4][i]) > -1))
              }
            }
            me.logIfDebug(
              'confusionInterUnion:' + confusionInterUnion,
              '\nerreurInegalite:' + erreurInegalite + '   ' + stor.stockage[2],
              '\nerreurLargeStricte:' + erreurLargeStricte + '   ' + stor.stockage[4],
              '\ntabIntervalles:' + stor.stockage[4],
              '\nerreurCrochets:' + erreurCrochets
            )
          }
          // Bonne réponse
          if (cbon) {
          // me._stopTimer();
            me.score++
            stor.explications.style.color = me.styles.cbien
            stor.explications.innerHTML = '<br><br>' + cBien
            stor.explications.innerHTML += '<br>' + ds.textes.comment15

            me.typederreurs[0]++
            stor.zonePalette.innerHTML = ''
            // on désactive la zone de saisie
            // on désactive la zone de saisie
            $(stor.zoneInput).mathquill('revert')
            $(stor.zoneInput).text(reponseEleve1).mathquill()
            j3pDesactive(stor.zoneInput)
            stor.zoneInput.style.color = me.styles.cbien
            j3pFreezeElt(stor.zoneInput)
            // on affiche l’explication'
            afficheCorrection(me.styles.cbien)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.explications.style.color = me.styles.cfaux
            stor.zoneInput.setAttribute('style', 'color:' + me.styles.cfaux)
            $(stor.zoneInput).focus()
            stor.explications.style.color = '<br><br>' + me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
            // me._stopTimer();

              stor.explications.innerHTML = '<br><br>' + tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              afficheCorrection(me.styles.toutpetit.correction.color)
              j3pDesactive(stor.zoneInput)
              stor.zoneInput.setAttribute('style', 'color:' + me.styles.cfaux)
              j3pBarre(stor.zoneInput)
              stor.zonePalette.innerHTML = ''
              j3pAffiche(stor.zoneCons3, '', '$' + stor.stockage[1] + '$', { style: { color: me.styles.toutpetit.correction.color } })

              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              const nouvelleRep = j3pAddElt(stor.zonesRep, 'div')
              j3pAffiche(nouvelleRep, '', ds.textes.comment_prop, {
                i: reponseEleve1Init
              })
              stor.explications.innerHTML = '<br>' + cFaux
              stor.explications.style.color = me.styles.cfaux
              if (testIntervalle.nonIntervalle) {
                stor.explications.innerHTML += '<br>' + ds.textes.comment7
              } else if (testIntervalle.separateurVirgule) {
                stor.explications.innerHTML += '<br>' + ds.textes.comment8
              } else if (confusionInterUnion) {
                me.typederreurs[4]++
                j3pAffiche(stor.explications, '', '\n' + ds.textes.comment10)
              } else {
                if (erreurInegalite) {
                  stor.explications.innerHTML += '<br>' + ds.textes.comment11
                  me.typederreurs[1]++
                } else if (erreurLargeStricte) {
                  me.typederreurs[2]++
                  if (stor.stockage[5] === 1) {
                    stor.explications.innerHTML += '<br>' + ds.textes.comment12
                  } else {
                    stor.explications.innerHTML += '<br>' + ds.textes.comment13
                  }
                } else {
                  if (erreurCrochets) {
                    me.typederreurs[3]++
                    stor.explications.innerHTML += '<br>' + ds.textes.comment14
                  } else {
                    me.typederreurs[5]++
                  }
                }
              }

              if (me.essaiCourant < ds.nbchances) {
                $(stor.zoneInput).focus()
                stor.explications.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au nème essai
              // me._stopTimer();
                me.cacheBoutonValider()
                afficheCorrection(me.styles.toutpetit.correction.color)
                // on désactive la zone de saisie
                j3pDesactive(stor.zoneInput)
                stor.zoneInput.setAttribute('style', 'color:' + me.styles.cfaux)
                j3pBarre(stor.zoneInput)
                stor.zonePalette.innerHTML = ''
                j3pAffiche(stor.zoneCons3, '', '$' + stor.stockage[1] + '$', { style: { color: me.styles.toutpetit.correction.color } })
                stor.explications.innerHTML += '<br>' + regardeCorrection
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          const nbErreurs = ds.nbitems - me.score
          if (me.typederreurs[1] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb sur le sens de l’inégalité est supé au égal au seul fixé
            me.parcours.pe = ds.pe_3
          } else if (me.typederreurs[2] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb sur les inégalités strictes ou larges est supé au égal au seul fixé
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[3] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de crochets est supé au égal au seul fixé
            me.parcours.pe = ds.pe_4
          } else if (me.typederreurs[4] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de confusion entre union et intersection est supé au égal au seul fixé
            me.parcours.pe = ds.pe_5
          } else {
            me.parcours.pe = ds.pe_6
          }
        }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
