import { j3pAddElt, j3pDetruit, j3pFocus, j3pGetRandomElt, j3pGetRandomInt, j3pPaletteMathquill, j3pShuffle, j3pStyle } from 'src/legacy/core/functions'
import { afficheBloc, phraseBloc } from 'src/legacy/core/functionsPhrase'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Mettre ici une description de la section
 * @author DENIAUD Rémi
 * @since octobre 2023
 * @fileOverview Traduire une phrase par une inégalité
 */

// nos constantes
const structure = 'presentation1'
// const ratioGauche = 0.75 C’est la valeur par défaut du ratio de la partie gauche dans presentation1

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['difficulte', 'v1', 'liste', 'Niveau de difficulté:\n- v1 : traduction directe;\n- v2 : formulation avec "au moins égal" ou une négation', ['v1', 'v2']]
    // les paramètres peuvent être du type 'entier', 'string', 'boolean', 'liste'
  ]
}

/**
 * Un objet pour regrouper les textes affichés (ce sera plus pratique pour internationaliser ensuite)
 * @private
 */
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titreExo: 'Indentifier une inégalité',
  // on donne les phrases de la consigne
  consigne1: 'Donne l’inégalité correspondant à la phrase suivante&nbsp;:',
  consigne2_1: '$£x$ est un nombre strictement supérieur à $£n$.',
  consigne2_2: '$£x$ est un nombre strictement inférieur à $£n$.',
  consigne2_3: '$£x$ est un nombre supérieur ou égal à $£n$.',
  consigne2_4: '$£x$ est un nombre inférieur ou égal à $£n$.',
  consigne2_5: '$£x$ est un nombre strictement positif.',
  consigne2_6: '$£x$ est un nombre strictement négatif.',
  consigne2_7: '$£x$ est un nombre positif ou nul.',
  consigne2_8: '$£x$ est un nombre négatif ou nul.',
  consigne3_1: '$£x$ n’est pas un nombre inférieur ou égal à $£n$.',
  consigne3_2: '$£x$ n’est pas un nombre supérieur ou égal à $£n$.',
  consigne3_3: '$£x$ est un nombre au moins égal à $£n$.',
  consigne3_4: '$£x$ est un nombre au plus égal à $£n$.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élève
  comment: 'Tu n’as qu’une seule tentative pour répondre.'
  // et les phrases utiles pour les explications de la réponse
}

/**
 * La fonction principale, rappelée à chaque étape
 * Son this sera l’instance courante de Parcours
 * @this {Parcours}
 */
export default function maSection () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini (en général dans enonceEnd(me)), sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance courante de Parcours (objet this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  const stor = me.storage
  me.surcharge()
  // Construction de la page
  me.construitStructurePage(structure) // L’argument devient {structure:structure, ratioGauche:ratioGauche} si ratioGauche est défini comme variable globale
  me.afficheTitre(textes.titreExo)
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  const tabInit = [[1, 2, 3, 4],
    [j3pGetRandomInt(5, 6), j3pGetRandomInt(7, 8)],
    [1, 2, 3, 4, j3pGetRandomInt(5, 6), j3pGetRandomInt(7, 8)]
  ]
  stor.elts.choice = []
  const alea1 = j3pGetRandomInt(0, tabInit[0].length - 1)
  const alea2 = j3pGetRandomInt(0, tabInit[1].length - 1)
  stor.elts.choice.push(tabInit[0][alea1], tabInit[1][alea2])
  tabInit[1].splice(alea2, 1)
  tabInit[0].splice(alea1, 1)
  let numTab
  do {
    if (tabInit[1].length === 0) {
      if (tabInit[0].length === 0) {
        // on cherche dans tabInit[2]
        numTab = 2
      } else {
        // on cherche dans tabInit[0]
        numTab = 0
      }
    } else if (tabInit[0].length === 0) {
      // on cherche dans tabInit[1]
      numTab = 1
    } else {
      const choix = j3pGetRandomInt(0, 2)
      if (choix === 0) {
        // je choisis dans tabInit[1]
        numTab = 1
      } else {
        // je choisis dans tabInit[0]
        numTab = 0
      }
    }
    const alea = j3pGetRandomInt(0, tabInit[numTab].length - 1)
    stor.elts.choice.push(tabInit[numTab][alea])
    tabInit[numTab].splice(alea, 1)
    if (tabInit[2].length === 0) tabInit[2].push(1, 2, 3, 4, j3pGetRandomInt(5, 6), j3pGetRandomInt(7, 8))
  } while (stor.elts.choice.length < me.donneesSection.nbrepetitions)
  stor.elts.choice = j3pShuffle(stor.elts.choice)
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}
/**
 * Retourne les données de l’exercice
 * @private
 * @return {Object}
 */
function genereAlea () {
  /**
   * données de l’exercice qui peuvent être les variables présentes dans les textes (consignes ou corrections)
   * ou autre données utiles à la validation
   * @private
   * @type {Object} obj
   */
  const obj = {}
  obj.n = j3pGetRandomInt(-15, 15)
  obj.x = j3pGetRandomElt(['a', 'b', 'c', 'x', 'y', 'z'])
  // Pourquoi nommer cette variable n ? C’est parce que dans l’énoncé (consigne1), apparait £n qui sera remplacé par la valeur ainsi déterminée
  return obj
}
/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  // on affiche l’énoncé
  // La style de base (ici petit.enonce) peut être complété par d’autres propriétés css. C’est fait ici avec j3pStyle
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.enonce })
  j3pStyle(stor.elts.divEnonce, { padding: '10px' })
  // On génère les données de l’exo (stockées dans un objet dont on se sert entre autre pour les phrases de l’énoncé)
  stor.objVariables = genereAlea()
  // On écrit la première phrase (dans un bloc pour assurer un retour à la ligne)
  const numChoice = stor.elts.choice[me.questionCourante - 1]
  const div1 = phraseBloc(stor.elts.divEnonce, textes.consigne1, stor.objVariables)
  j3pStyle(div1, { paddingBottom: '20px' })
  const phraseConsigne = (me.donneesSection.difficulte === 'v1')
    ? textes['consigne2_' + numChoice]
    : (numChoice > 4) ? textes['consigne2_' + numChoice] : textes['consigne3_' + numChoice]
  afficheBloc(stor.elts.divEnonce, phraseConsigne, stor.objVariables)
  if (numChoice > 4) stor.objVariables.n = 0
  stor.espace = ''
  for (let i = 1; i <= 3; i++) stor.espace += '&nbsp;'
  const eltCons2 = afficheBloc(stor.elts.divEnonce, stor.espace + '&1&', {
    inputmq1: { texte: '' }
  })
  stor.zoneInput = eltCons2.inputmqList[0] // Une seule zone à valider
  stor.zoneInput.typeReponse = ['texte']
  const signeRep = (numChoice % 4 === 1)
    ? '>'
    : (numChoice % 4 === 2)
        ? '<'
        : (numChoice % 4 === 3)
            ? '\\ge'
            : '\\le'
  const signeRepContraire = (numChoice % 4 === 1)
    ? '<'
    : (numChoice % 4 === 2)
        ? '>'
        : (numChoice % 4 === 3)
            ? '\\le '
            : '\\ge '
  stor.zoneInput.reponse = [stor.objVariables.x + signeRep + stor.objVariables.n, stor.objVariables.n + signeRepContraire + stor.objVariables.x]
  mqRestriction(stor.zoneInput, stor.objVariables.x + '\\-<>0-9', { commandes: ['infegal', 'supegal'] })
  stor.fctsValid = new ValidationZones({
    zones: [stor.zoneInput],
    parcours: me
  })
  j3pFocus(stor.zoneInput)
  // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvent dans le cadre MD)
  // Il n’est ainsi construit qu’une seule fois et vidé à chaque passage dans correction()
  stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.moyen.correction })
  j3pStyle(stor.zoneCorr, { padding: '10px' })
  stor.laPalette = j3pAddElt(stor.elts.divEnonce, 'div')
  j3pStyle(stor.laPalette, { paddingTop: '15px' })
  j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['infegal', 'supegal', '<', '>'] })
  if (me.donneesSection.nbchances === 1) {
    stor.phraseComment = phraseBloc(stor.elts.divEnonce, textes.comment)
    j3pStyle(stor.phraseComment, { paddingTop: '45px', fontStyle: 'italic' })
  }

  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me, bonneReponse = false) {
  const stor = me.storage
  // const ds = me.donneesSection
  const zoneExpli = j3pAddElt(stor.elts.divEnonce, 'div', '', { style: me.styles.petit.correction })
  // La couleur de la correction sera fonction de la qualité de la réponse de l’élève
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  j3pDetruit(stor.laPalette)
  afficheBloc(zoneExpli, stor.espace + '$' + stor.zoneInput.reponse[0] + '$')
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number|null}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  if (reponse.aRepondu) {
    if (me.donneesSection.nbchances === 1) j3pDetruit(me.storage.phraseComment)
    return Number(reponse.bonneReponse)
  }
  return null
}

function correction (me) {
  const stor = me.storage
  const ds = me.donneesSection
  const score = getScore(me) // null si pas répondu
  if ((score === 1) || (me.essaiCourant >= ds.nbchances)) me.score += score
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.zoneCorr)
    return
  }
  if (score !== 1) {
    // On écrit c’est faux
    me.reponseKo(stor.zoneCorr)
    if (score === 0.5) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, textes.comment1, true)
    }
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if ((score !== 1) && me.essaiCourant < me.donneesSection.nbchances) {
    me.reponseKo(stor.zoneCorr, '<br>' + essaieEncore, true)
    return
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.zoneCorr)
    j3pDetruit(stor.laPalette)
    // Même dans le cas d’une bonne réponse, on peut afficher la correction qui aurait dans ce cas d’éventuels compléments d’information
    // afficheCorrection(me, true)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, tempsDepasse, true)
    }
    afficheCorrection(me)
  }
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (me.sectionTerminee()) {
    // fin de section, on affiche un recap global (ou pas…)

    // on peut aussi calculer la pe retournée

    // et on affiche ce bout
    me.afficheBoutonSectionSuivante(true)
  } else {
    me.etat = 'enonce'
    // on laisse le bouton suite mis par le finCorrection() précédent
    me.finNavigation(true)
  }
  // Si on a rien à afficher de particulier après la dernière répétition, donc on passe true à finNavigation
  // pour que le modèle affiche directement le bouton "section suivante" (sans devoir cliquer sur 'suite' avant)
  // me.finNavigation(true)
}
