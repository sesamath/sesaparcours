import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pNombre, j3pPaletteMathquill, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    mai 2013
    Résolution graphique d’une inéquation de la forme f(x)>k
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['choix_typeSolution', ['alea', 'alea', 'alea'], 'array', 'deux cas de figure pour l’ensemble des solutions : 1 pour un intervalle, 2 pour une réunion de 2 intervalles, par défaut, on laisse "alea"'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.6, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'problème entre inégalité large et stricte' },
    { pe_3: 'problème sur le sens de l’inégalité' },
    { pe_4: 'problème avec les crochets' },
    { pe_5: 'confusion entre les symboles intersection et réunion' },
    { pe_6: 'Insuffisant' }
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function mouseDownListener () {
    stor.mouvement = !stor.mouvement
    if (stor.mouvement) {
      stor.repere.svg.addEventListener('mousemove', mouseMoveListener, false)
    } else {
      stor.repere.svg.removeEventListener('mousemove', mouseMoveListener, false)
    }
  }
  function mouseMoveListener (event) {
    const y = event.clientY - stor.repere.svg.getBoundingClientRect().top
    stor.droiteX1Y1.setAttribute('y1', y)
    stor.droiteX1Y1.setAttribute('y2', y)
    stor.droiteX2Y2.setAttribute('y1', y)
    stor.droiteX2Y2.setAttribute('y2', y)
  }
  function decoupageCupcap (rep) {
    // cette fonction renvoit un tableau contenant les différentes parties séparées par \\cup ou par \\cap
    // ces différentes parties sont en principe des intervalles
    const cupCap = /\\cup|\\cap/g // new RegExp('\\\\cup|\\\\cap', 'g')
    return rep.split(cupCap)
  }
  function bonIntervalle (rep) {
    // rep est du texte (réponse de l’élève)
    // On teste si rep est bie un intervalle ou une réunon d’intervalle'
    // on vérifie aussi si l’élève ne met pas une virgule à la place du point-virgule quand il écrit un intervalle'
    let pbCrochet = false
    let pbPointVirgule = false
    const intervalleTab = decoupageCupcap(rep)
    for (let k = 0; k < intervalleTab.length; k++) {
      // pour chacun de ces éléments, on vérifie que c’est bien un intervalle'
      if (((intervalleTab[k].charAt(0) !== '[') && (intervalleTab[k].charAt(0) !== ']')) || ((intervalleTab[k].charAt(intervalleTab[k].length - 1) !== '[') && (intervalleTab[k].charAt(intervalleTab[k].length - 1) !== ']'))) {
        pbCrochet = true
      } else {
        if ((intervalleTab[k].indexOf(';') <= 1) || (intervalleTab[k].indexOf(';') >= intervalleTab[k].length - 2)) {
          // il n’y a pas de point-virgule bien placé, donc controlons s’il n’a pas mis une virgule à la place'
          if ((intervalleTab[k].indexOf(',') > 1) && (intervalleTab[k].indexOf(',') < intervalleTab[k].length - 2)) {
            // c’est donc qu’il met une virgule à la place d’un point-virgule'
            pbPointVirgule = true
          }
        }
      }
    }
    return { non_intervalle: pbCrochet, separateur_virgule: pbPointVirgule }
  }

  function abscissesAncrage (abs1, abs2) {
    // cette fonction rend un tableau de 2 nombres compris entre abs1 et abs2 (le premier étant plus petit que le second
    // l’écart minimal entre deux nombres parmi ces 4 est (abs2-abs1)/8'
    // Cela permet de positionner les points d’ancrage sans qu’ils ne soient trop près (c’est pour l’abscisse)
    const ecart = abs2 - abs1
    const tabRetour = []
    tabRetour[0] = ecart / 8 + abs1 + ecart / 4 * Math.random()
    tabRetour[1] = 5 * ecart / 8 + abs1 + ecart / 4 * Math.random()
    return tabRetour
  }

  function minMaxBezier (P1, P2, P3, P4) {
    // P1, P2, P3, P4 représente des ordonnées de points qui permettent de créer une courbe de Bézier
    // (P1 et P4 pour les ordonnées des pts de départ et d’arrivée), (P2 et P3 pour les ordonnées des pts d’ancrage'
    // cette fonction va me rendre les ordonnées max et min des pts de la courbe de Bezier
    let lemin = P1
    let lemax = P1
    let minExtremite = true
    let maxExtremite = true
    // ces deux dernières variables me permettent de savoir si l’extremum est atteint aux extremités du segment
    for (let t = 0; t <= 1; t += 0.01) {
      const ordInterm = Math.pow(1 - t, 3) * P1 + 3 * t * Math.pow(1 - t, 2) * P2 + 3 * Math.pow(t, 2) * (1 - t) * P3 + Math.pow(t, 3) * P4
      if (ordInterm < lemin) {
        lemin = ordInterm
        minExtremite = !(t > 0.1 && t < 0.9)
      }
      if (ordInterm > lemax) {
        lemax = ordInterm
        maxExtremite = !(t > 0.1 && t < 0.9)
      }
    }
    return [lemin, lemax, minExtremite, maxExtremite]
  }

  function ordonneTab (montableau) {
    const newTab = []
    const copieTab = []// on copie dedans le tableau initial
    for (let i = 0; i < montableau.length; i++) {
      copieTab[i] = montableau[i]
    }
    for (let i = 0; i < montableau.length; i++) {
      newTab[i] = copieTab[0]
      let posMin = 0
      if (copieTab.length > 1) {
        for (let j = 1; j < copieTab.length; j++) {
          if (newTab[i] > copieTab[j]) {
            newTab[i] = copieTab[j]
            posMin = j
          }
        }
        copieTab.splice(posMin, 1)
      }
    }
    return newTab
  }

  function afficheCorr (type, couleurCorr) {
    let solMin, solMax, solMed
    j3pDetruit(stor.commentDroite)
    // type est nbSolEq=stor.stockage[0]
    const largTrait = 4
    const c = stor.stockage[10]
    const ordPt = stor.stockage[11]
    const absMin = stor.stockage[12]
    const absMax = stor.stockage[13]
    let abs1
    if (stor.stockage[0] === 1) {
      abs1 = stor.stockage[14]
      stor.repere.add({
        type: 'point',
        nom: 'A',
        par1: abs1,
        par2: c,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'B',
        par1: abs1,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 's1', par1: 'A', par2: 'B', style: { couleur: couleurCorr, epaisseur: 1 } })
      stor.repere.add({
        type: 'point',
        nom: 'X',
        par1: absMin,
        par2: c,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'Y',
        par1: absMax,
        par2: c,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'droite', nom: 'd1', par1: 'X', par2: 'Y', style: { couleur: couleurCorr, epaisseur: 2 } })
      if (stor.stockage[6]) {
        if (stor.stockage[7] === 'inf') {
          stor.repere.add({
            type: 'point',
            nom: 'C',
            par1: absMax,
            par2: ordPt[3],
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({
            type: 'point',
            nom: 'D',
            par1: absMax,
            par2: 0,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
        } else {
          stor.repere.add({
            type: 'point',
            nom: 'C',
            par1: absMin,
            par2: ordPt[0],
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({
            type: 'point',
            nom: 'D',
            par1: absMin,
            par2: 0,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
        }
      } else {
        if (stor.stockage[7] === 'inf') {
          stor.repere.add({
            type: 'point',
            nom: 'C',
            par1: absMin,
            par2: ordPt[0],
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({
            type: 'point',
            nom: 'D',
            par1: absMin,
            par2: 0,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
        } else {
          stor.repere.add({
            type: 'point',
            nom: 'C',
            par1: absMax,
            par2: ordPt[3],
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({
            type: 'point',
            nom: 'D',
            par1: absMax,
            par2: 0,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
        }
      }
      stor.repere.add({
        type: 'segment',
        nom: 's2',
        par1: 'C',
        par2: 'D',
        style: { couleur: couleurCorr, epaisseur: 1 }
      })
      stor.repere.add({ type: 'segment', nom: 's3', par1: 'B', par2: 'D', style: { couleur: couleurCorr, epaisseur: largTrait } })
    } else if (stor.stockage[0] === 2) {
      if (stor.stockage[8]) {
        abs1 = stor.stockage[14]
        stor.repere.add({
          type: 'point',
          nom: 'A',
          par1: abs1,
          par2: c,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'B',
          par1: abs1,
          par2: 0,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({ type: 'segment', nom: 's1', par1: 'A', par2: 'B', style: { couleur: couleurCorr, epaisseur: 1 } })
        stor.repere.add({
          type: 'point',
          nom: 'X',
          par1: absMin,
          par2: c,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'Y',
          par1: absMax,
          par2: c,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({ type: 'droite', nom: 'd1', par1: 'X', par2: 'Y', style: { couleur: couleurCorr, epaisseur: 2 } })
        // l’une des borne de l’intervalle de def est sol de l’equation'
        if (stor.stockage[9]) {
          // c’est la borne inf'
          if (stor.stockage[6]) {
            if (stor.stockage[7] === 'inf') {
              stor.repere.add({
                type: 'point',
                nom: 'C',
                par1: absMax,
                par2: ordPt[3],
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
              stor.repere.add({
                type: 'point',
                nom: 'D',
                par1: absMax,
                par2: 0,
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
            } else {
              stor.repere.add({
                type: 'point',
                nom: 'C',
                par1: absMin,
                par2: ordPt[0],
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
              stor.repere.add({
                type: 'point',
                nom: 'D',
                par1: absMin,
                par2: 0,
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
            }
          } else {
            if (stor.stockage[7] === 'sup') {
              stor.repere.add({
                type: 'point',
                nom: 'C',
                par1: absMax,
                par2: ordPt[3],
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
              stor.repere.add({
                type: 'point',
                nom: 'D',
                par1: absMax,
                par2: 0,
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
            } else {
              stor.repere.add({
                type: 'point',
                nom: 'C',
                par1: absMin,
                par2: ordPt[0],
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
              stor.repere.add({
                type: 'point',
                nom: 'D',
                par1: absMin,
                par2: 0,
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
            }
          }
        } else {
          // c’est la borne sup'
          if (stor.stockage[6]) {
            if (stor.stockage[7] === 'sup') {
              stor.repere.add({
                type: 'point',
                nom: 'C',
                par1: absMin,
                par2: ordPt[0],
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
              stor.repere.add({
                type: 'point',
                nom: 'D',
                par1: absMin,
                par2: 0,
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
            } else {
              stor.repere.add({
                type: 'point',
                nom: 'C',
                par1: absMax,
                par2: ordPt[3],
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
              stor.repere.add({
                type: 'point',
                nom: 'D',
                par1: absMax,
                par2: 0,
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
            }
          } else {
            if (stor.stockage[7] === 'inf') {
              stor.repere.add({
                type: 'point',
                nom: 'C',
                par1: absMin,
                par2: ordPt[0],
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
              stor.repere.add({
                type: 'point',
                nom: 'D',
                par1: absMin,
                par2: 0,
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
            } else {
              stor.repere.add({
                type: 'point',
                nom: 'C',
                par1: absMax,
                par2: ordPt[3],
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
              stor.repere.add({
                type: 'point',
                nom: 'D',
                par1: absMax,
                par2: 0,
                fixe: true,
                visible: false,
                etiquette: false,
                style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
              })
            }
          }
        }
        stor.repere.add({
          type: 'segment',
          nom: 's2',
          par1: 'C',
          par2: 'D',
          style: { couleur: couleurCorr, epaisseur: 1 }
        })
        stor.repere.add({ type: 'segment', nom: 's3', par1: 'B', par2: 'D', style: { couleur: couleurCorr, epaisseur: largTrait } })
      } else {
        solMin = stor.stockage[15]
        solMax = stor.stockage[16]
        stor.repere.add({
          type: 'point',
          nom: 'A',
          par1: solMin,
          par2: c,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'B',
          par1: solMin,
          par2: 0,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({ type: 'segment', nom: 's1', par1: 'A', par2: 'B', style: { couleur: couleurCorr, epaisseur: 1 } })
        stor.repere.add({
          type: 'point',
          nom: 'C',
          par1: solMax,
          par2: c,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'D',
          par1: solMax,
          par2: 0,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({ type: 'segment', nom: 's2', par1: 'C', par2: 'D', style: { couleur: couleurCorr, epaisseur: 1 } })
        stor.repere.add({
          type: 'point',
          nom: 'X',
          par1: -2,
          par2: c,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'Y',
          par1: absMax,
          par2: c,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({ type: 'droite', nom: 'd1', par1: 'X', par2: 'Y', style: { couleur: couleurCorr, epaisseur: 2 } })
        if (stor.stockage[6]) {
          if (stor.stockage[7] === 'inf') {
            stor.repere.add({
              type: 'segment',
              nom: 's3',
              par1: 'B',
              par2: 'D',
              style: { couleur: couleurCorr, epaisseur: largTrait }
            })
          } else {
            stor.repere.add({
              type: 'point',
              nom: 'E',
              par1: absMin,
              par2: ordPt[0],
              fixe: true,
              visible: false,
              etiquette: false,
              style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
            })
            stor.repere.add({
              type: 'point',
              nom: 'F',
              par1: absMin,
              par2: 0,
              fixe: true,
              visible: false,
              etiquette: false,
              style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
            })
            stor.repere.add({ type: 'segment', nom: 's3', par1: 'E', par2: 'F', style: { couleur: couleurCorr, epaisseur: 1 } })
            stor.repere.add({
              type: 'point',
              nom: 'G',
              par1: absMax,
              par2: ordPt[4],
              fixe: true,
              visible: false,
              etiquette: false,
              style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
            })
            stor.repere.add({
              type: 'point',
              nom: 'H',
              par1: absMax,
              par2: 0,
              fixe: true,
              visible: false,
              etiquette: false,
              style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
            })
            stor.repere.add({ type: 'segment', nom: 's4', par1: 'G', par2: 'H', style: { couleur: couleurCorr, epaisseur: 1 } })
            stor.repere.add({ type: 'segment', nom: 's5', par1: 'B', par2: 'F', style: { couleur: couleurCorr, epaisseur: largTrait } })
            stor.repere.add({ type: 'segment', nom: 's5', par1: 'D', par2: 'H', style: { couleur: couleurCorr, epaisseur: largTrait } })
          }
        } else {
          if (stor.stockage[7] === 'sup') {
            stor.repere.add({
              type: 'segment',
              nom: 's3',
              par1: 'B',
              par2: 'D',
              style: { couleur: couleurCorr, epaisseur: largTrait }
            })
          } else {
            stor.repere.add({
              type: 'point',
              nom: 'E',
              par1: absMin,
              par2: ordPt[0],
              fixe: true,
              visible: false,
              etiquette: false,
              style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
            })
            stor.repere.add({
              type: 'point',
              nom: 'F',
              par1: absMin,
              par2: 0,
              fixe: true,
              visible: false,
              etiquette: false,
              style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
            })
            stor.repere.add({ type: 'segment', nom: 's3', par1: 'E', par2: 'F', style: { couleur: couleurCorr, epaisseur: 1 } })
            stor.repere.add({
              type: 'point',
              nom: 'G',
              par1: absMax,
              par2: ordPt[4],
              fixe: true,
              visible: false,
              etiquette: false,
              style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
            })
            stor.repere.add({
              type: 'point',
              nom: 'H',
              par1: absMax,
              par2: 0,
              fixe: true,
              visible: false,
              etiquette: false,
              style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
            })
            stor.repere.add({ type: 'segment', nom: 's4', par1: 'G', par2: 'H', style: { couleur: couleurCorr, epaisseur: 1 } })
            stor.repere.add({ type: 'segment', nom: 's5', par1: 'B', par2: 'F', style: { couleur: couleurCorr, epaisseur: largTrait } })
            stor.repere.add({ type: 'segment', nom: 's5', par1: 'D', par2: 'H', style: { couleur: couleurCorr, epaisseur: largTrait } })
          }
        }
      }
    } else if (stor.stockage[0] === 3) {
      solMin = stor.stockage[15]
      solMed = stor.stockage[16]
      solMax = stor.stockage[17]
      stor.repere.add({
        type: 'point',
        nom: 'A',
        par1: solMin,
        par2: c,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'B',
        par1: solMin,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 's1', par1: 'A', par2: 'B', style: { couleur: couleurCorr, epaisseur: 1 } })
      stor.repere.add({
        type: 'point',
        nom: 'C',
        par1: solMed,
        par2: c,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'D',
        par1: solMed,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 's2', par1: 'C', par2: 'D', style: { couleur: couleurCorr, epaisseur: 1 } })
      stor.repere.add({
        type: 'point',
        nom: 'E',
        par1: solMax,
        par2: c,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'F',
        par1: solMax,
        par2: 0,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'segment', nom: 's3', par1: 'E', par2: 'F', style: { couleur: couleurCorr, epaisseur: 1 } })
      stor.repere.add({
        type: 'point',
        nom: 'X',
        par1: absMin,
        par2: c,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({
        type: 'point',
        nom: 'Y',
        par1: absMax,
        par2: c,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
      })
      stor.repere.add({ type: 'droite', nom: 'd1', par1: 'X', par2: 'Y', style: { couleur: couleurCorr, epaisseur: 2 } })
      if (stor.stockage[6]) {
        if (stor.stockage[7] === 'inf') {
          stor.repere.add({
            type: 'point',
            nom: 'G',
            par1: absMax,
            par2: ordPt[5],
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({
            type: 'point',
            nom: 'H',
            par1: absMax,
            par2: 0,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({ type: 'segment', nom: 's4', par1: 'G', par2: 'H', style: { couleur: couleurCorr, epaisseur: 1 } })
          stor.repere.add({ type: 'segment', nom: 's5', par1: 'B', par2: 'D', style: { couleur: couleurCorr, epaisseur: largTrait } })
          stor.repere.add({ type: 'segment', nom: 's6', par1: 'F', par2: 'H', style: { couleur: couleurCorr, epaisseur: largTrait } })
        } else {
          stor.repere.add({
            type: 'point',
            nom: 'G',
            par1: absMin,
            par2: ordPt[0],
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({
            type: 'point',
            nom: 'H',
            par1: absMin,
            par2: 0,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({ type: 'segment', nom: 's4', par1: 'G', par2: 'H', style: { couleur: couleurCorr, epaisseur: 1 } })
          stor.repere.add({ type: 'segment', nom: 's5', par1: 'B', par2: 'H', style: { couleur: couleurCorr, epaisseur: largTrait } })
          stor.repere.add({ type: 'segment', nom: 's6', par1: 'F', par2: 'D', style: { couleur: couleurCorr, epaisseur: largTrait } })
        }
      } else {
        if (stor.stockage[7] === 'sup') {
          stor.repere.add({
            type: 'point',
            nom: 'G',
            par1: absMax,
            par2: ordPt[5],
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({
            type: 'point',
            nom: 'H',
            par1: absMax,
            par2: 0,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({ type: 'segment', nom: 's4', par1: 'G', par2: 'H', style: { couleur: couleurCorr, epaisseur: 1 } })
          stor.repere.add({ type: 'segment', nom: 's5', par1: 'B', par2: 'D', style: { couleur: couleurCorr, epaisseur: largTrait } })
          stor.repere.add({ type: 'segment', nom: 's6', par1: 'F', par2: 'H', style: { couleur: couleurCorr, epaisseur: largTrait } })
        } else {
          stor.repere.add({
            type: 'point',
            nom: 'G',
            par1: absMin,
            par2: ordPt[0],
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({
            type: 'point',
            nom: 'H',
            par1: absMin,
            par2: 0,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: couleurCorr, epaisseur: 2, taille: 18 }
          })
          stor.repere.add({ type: 'segment', nom: 's4', par1: 'G', par2: 'H', style: { couleur: couleurCorr, epaisseur: 1 } })
          stor.repere.add({ type: 'segment', nom: 's5', par1: 'B', par2: 'H', style: { couleur: couleurCorr, epaisseur: largTrait } })
          stor.repere.add({ type: 'segment', nom: 's6', par1: 'F', par2: 'D', style: { couleur: couleurCorr, epaisseur: largTrait } })
        }
      }
    }
    stor.repere.construit()
    // à priori on en a plus besoin en correction mais mieux vaut ne pas conserver des refs à des trucs qui n’existent plus
    stor.droiteX1Y1 = j3pElement('droiteX1Y1')
    stor.droiteX2Y2 = j3pElement('droiteX2Y2')
    stor.droiteX2Y2.setAttribute('stroke-opacity', 0)
    stor.droiteX1Y1.setAttribute('stroke-opacity', 0)
  }

  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 3,
      nbetapes: 1,
      // une étape avec une inégalité stricte, l’autre avec une inégalité large'
      indication: '',
      nbchances: 2,
      seuilreussite: 0.8,
      seuilerreur: 0.6,
      // choix du type de solution (1 pour un seul intervalle, 2 pour éventuellement une réunion)
      choix_typeSolution: ['alea', 'alea', 'alea'],
      // On donne la possibilité de laisser visible ou non les boutons union et inter de la palette (en plus du botuon ensemble vide)
      // par défaut les 3 boutons sont présents
      palette_complete: true,
      pe_1: 'Bien',
      pe_2: 'problème entre inégalité large et stricte',
      pe_3: 'problème sur le sens de l’inégalité',
      pe_4: 'problème avec les crochets',
      pe_5: 'confusion entre les symboles intersection et réunion',
      pe_6: 'Insuffisant',

      structure: 'presentation1',
      textes: {
        titre_exo: 'Inéquation du style f(x) &le; c<br>Résolution graphique',
        consigne1: 'Dans le repère ci-contre on a tracé la courbe représentant la fonction $£f$ définie sur l’intervalle $£i$.',
        consigne2: 'Par lecture graphique, réponds à la question suivante.<br>L’ensemble des solutions de l’inéquation $£b$ sur l’intervalle $£c$ est :<br>&1&',
        comment3: 'Attention au sens de l’inégalité&nbsp;!',
        comment4: 'Attention aux crochets&nbsp;!',
        comment6: 'Ta réponse est incomplète&nbsp;!',
        comment7: 'Attention à l’écriture d’un intervalle&nbsp;!',
        comment8: 'Le séparateur dans un intervalle doit être un point-virgule&nbsp;!',
        comment10: 'Attention : $\\cap$ signifie "intersection"&nbsp;!',
        comment11: 'Tu n’as pas résolu la bonne inéquation&nbsp;!',
        comment12: 'L’inégalité est large&nbsp;!',
        comment13: 'L’inégalité est stricte&nbsp;!',
        comment14: 'Il y a un problème de crochet(s)&nbsp;!',
        comment15: 'Regarde l’explication sur le graphique&nbsp;!',
        comment_droite: 'Pour t’aider, tu peux déplacer la droite en rouge en cliquant dessus&nbsp;!',
        comment_prop: '$£i$ est une mauvaise réponse&nbsp;!'
      }
    }
  }
  function enonceMain () {
    // on construit le repère, puis la courbe représentant la fonction
  // tout d’abord le nom de la fonction'
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })

    const tabNomFct = ['f', 'g', 'h']
    const nomFct = tabNomFct[j3pGetRandomInt(0, 2)]
    // ensuite je choisis le nombre c sachant qu’on résout f(x)>c'
    const c = j3pGetRandomInt(-3, 3)
    // intervalle correspondant au domaine de définition
    const ledomaine = '[-6;6]'
    const absMin = -6
    const absMax = 6
    const ordMin = -6
    const ordMax = 6
    let abs1, abs2, nbSolEq, nbParties, monsigne, audessus, nbChoix, solBorne, choixBorne, nbEssai, lePb
    let solMin, solMax, solMed

    stor.zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, {
      f: nomFct,
      i: ledomaine
    })
    // je prépare différents cas de figure sur le nombre de solution  de l’équation f(x)=c
    let typeSolution
    if ((ds.choix_typeSolution[me.questionCourante - 1] === 1) || (ds.choix_typeSolution[me.questionCourante - 1] === '1') || (ds.choix_typeSolution[me.questionCourante - 1] === 2) || (ds.choix_typeSolution[me.questionCourante - 1] === '2')) {
      typeSolution = j3pNombre(ds.choix_typeSolution[me.questionCourante - 1])
    } else {
      typeSolution = j3pGetRandomInt(1, 2)
    }
    if (typeSolution === 1) {
      // pour la première question, l’ensemble des sol ne sera qu’un intervalle'
      nbSolEq = 1 + j3pGetRandomInt(0, 1)
      if (nbSolEq === 2) {
        solBorne = true
      }
    } else {
      // pour la deuxième question, il peut y avoir une réunion d’intervalles'
      nbSolEq = 2 + j3pGetRandomInt(0, 1)
      if (nbSolEq === 2) {
        solBorne = false
      }
    }// nbParties+1 est le nombre de courbes de Bezier
    if (nbSolEq === 1) {
      nbParties = 2
    } else if (nbSolEq === 2) {
      // on peut prévoir que l’une des sol de l’eq est une borne du domaine'
      // c’est la variable booléenne solBorne'
      if (solBorne) {
        choixBorne = Boolean(j3pGetRandomInt(0, 1))
        // si (choixBorne), la solution est la borne inf du domaine de def
        nbParties = 2
      } else {
        nbParties = 3
      }
    } else if (nbSolEq === 3) {
      nbParties = 4
    }
    const absPt = []// ce tableau prend les abscisses des points
    const ordPt = []// ce tableau prend les ordonnées des points
    const absAncrage = []// ce tableau prend les abscisses des points d’ancrage
    const ordAncrage = []// ce tableau prend les ordonnées des points d’ancrage
    // il y a tjrs deux points d’ancrage entre deux points de la courbe'
    const choixSigne = j3pGetRandomInt(0, 1)
    let plusmoins, pb1, extremum1, extremum2, extremum, extremum3, lecoef, absOrdonnee, absPtsAncrage, nbEssai1,
      nbEssai2, pbSolEq3
    if (choixSigne === 0) {
      monsigne = 'inf'
    } else {
      monsigne = 'sup'
    }
    let comptErreur
    let abs3
    do {
      comptErreur = 0
      if ((nbSolEq === 1) || ((nbSolEq === 2) && solBorne)) {
        // s’il n’y a qu’une solution à l’équation'
        // on choisit si on part d’une ordonnée surpérieure à c ou inférieure'
        audessus = Boolean(j3pGetRandomInt(0, 1))
        do {
          nbChoix = 0
          do {
            abs1 = j3pGetRandomInt(-3, 3)
          } while (Math.abs(abs1 - c) < Math.pow(10, -12))
          absPt[0] = absMin
          absPt[1] = Math.min(abs1, c)
          absPt[2] = Math.max(abs1, c)
          absPt[3] = absMax
          if (abs1 < c) {
            ordPt[1] = c
          } else {
            ordPt[2] = c
          }
          if (audessus) {
            do {
              nbChoix += 1
              if ((nbSolEq === 2) && solBorne && choixBorne) {
                ordPt[0] = c
              } else {
                do {
                  nbEssai = 0
                  do {
                    nbEssai += 1
                    ordPt[0] = c + 1 + j3pGetRandomInt(1, 7)
                    if (abs1 < c) {
                      ordPt[2] = c - j3pGetRandomInt(1, 5)
                    } else {
                      ordPt[1] = c + j3pGetRandomInt(1, 5)
                    }
                  } while (((ordPt[0] >= ordMax) || (ordPt[1] >= ordMax) || (ordPt[2] <= ordMin)) && (nbEssai <= 50))
                } while (nbEssai >= 50)
              }
              plusmoins = 1
              do {
                nbEssai = 0
                do {
                  nbEssai += 1
                  if ((nbSolEq === 2) && solBorne && !choixBorne) {
                    ordPt[3] = c
                  } else {
                    ordPt[3] = c - 1 - j3pGetRandomInt(1, 7)
                    if (abs1 < c) {
                      ordPt[2] = c - j3pGetRandomInt(1, 5)
                    } else {
                      ordPt[1] = c + j3pGetRandomInt(1, 5)
                    }
                  }
                } while (((ordPt[3] <= ordMin) || (ordPt[3] >= ordMax) || (ordPt[1] >= ordMax) || (ordPt[1] <= ordMin) || (ordPt[2] <= ordMin) || (ordPt[2] >= ordMax)) && (nbEssai <= 50))
              } while (nbEssai >= 50)
              if (nbSolEq === 1) {
                lePb = ((Math.abs(ordPt[3] - ordPt[0]) < 2.5) && (nbChoix <= 10))
              } else {
                lePb = ((Math.abs(ordPt[3] - ordPt[0]) < 1.5) && (nbChoix <= 10))
              }
            } while (lePb)
          } else {
            do {
              nbChoix += 1
              if ((nbSolEq === 2) && solBorne && choixBorne) {
                ordPt[0] = c
              } else {
                do {
                  nbEssai = 0
                  do {
                    nbEssai += 1
                    ordPt[0] = c - 1 - j3pGetRandomInt(0, 7)
                    if (abs1 < c) {
                      ordPt[2] = c + j3pGetRandomInt(1, 5)
                    } else {
                      ordPt[1] = c - j3pGetRandomInt(1, 5)
                    }
                  } while (((ordPt[0] <= ordMin) || (ordPt[1] <= ordMin) || (ordPt[2] >= ordMax)) && (nbEssai <= 50))
                } while (nbEssai >= 50)
              }
              plusmoins = -1
              if ((nbSolEq === 2) && solBorne && !choixBorne) {
                ordPt[3] = c
              } else {
                do {
                  nbEssai2 = 0
                  do {
                    nbEssai2 += 1
                    ordPt[3] = c + 1 + j3pGetRandomInt(0, 7)
                    if (abs1 < c) {
                      ordPt[2] = c + j3pGetRandomInt(1, 5)
                    } else {
                      ordPt[1] = c - j3pGetRandomInt(1, 5)
                    }
                  } while (((ordPt[3] <= ordMin) || (ordPt[3] >= ordMax) || (ordPt[2] >= ordMax) || (ordPt[2] <= ordMin) || (ordPt[1] <= ordMin) || (ordPt[1] >= ordMax)) && (nbEssai2 <= 50))
                } while (nbEssai2 >= 50)
              }
              if (nbSolEq === 1) {
                lePb = ((Math.abs(ordPt[3] - ordPt[0]) < 2.5) && (nbChoix <= 10))
              } else {
                lePb = ((Math.abs(ordPt[3] - ordPt[0]) < 1.5) && (nbChoix <= 10))
              }
            } while (lePb)
          }
        } while (nbChoix >= 10)
        absPtsAncrage = []
        // ancrage pour la première partie
        absPtsAncrage[0] = abscissesAncrage(absPt[0], absPt[1])
        absAncrage[0] = absPtsAncrage[0][0]
        absAncrage[1] = absPtsAncrage[0][1]
        // ancrage pour la deuxième partie
        absPtsAncrage[1] = abscissesAncrage(absPt[1], absPt[2])
        absAncrage[2] = absPtsAncrage[1][0]
        absAncrage[3] = absPtsAncrage[1][1]
        // ancrage pour la troisième partie
        absPtsAncrage[2] = abscissesAncrage(absPt[2], absPt[3])
        absAncrage[4] = absPtsAncrage[2][0]
        absAncrage[5] = absPtsAncrage[2][1]
        // pour les ordonnées des pts d’ancrage, il faut faire un peu plus attention
        do {
          if ((nbSolEq === 2) && solBorne) {
            if (choixBorne) {
              if (ordPt[0] === ordPt[1]) {
                if (audessus) {
                  ordAncrage[0] = ordPt[0] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                } else {
                  ordAncrage[0] = ordPt[0] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                }
              } else {
                if (ordPt[0] < ordPt[1]) {
                  ordAncrage[0] = ordPt[0] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1]
                } else {
                  ordAncrage[0] = ordPt[0] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1]
                }
              }
            } else {
              do {
                ordAncrage[0] = ordPt[0] - plusmoins * (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                ordAncrage[1] = ordPt[1] + plusmoins * (0.7 + 0.2 * j3pGetRandomInt(2, 10))
              } while ((ordAncrage[1] - ordPt[1]) * (ordAncrage[0] - ordPt[0]) > 0)
            }
          } else {
            if (audessus) {
              ordAncrage[0] = Math.max(ordPt[0] + plusmoins * (0.3 + 0.2 * j3pGetRandomInt(2, 10)), ordPt[3] + plusmoins * (1 + 0.2 * j3pGetRandomInt(2, 10)))
            } else {
              ordAncrage[0] = ordPt[0] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
            }
            ordAncrage[1] = ordPt[1] + plusmoins * (1 + 0.2 * j3pGetRandomInt(2, 10))
          }
          extremum = minMaxBezier(ordPt[0], ordAncrage[0], ordAncrage[1], ordPt[1])
          if (ordPt[0] > ordPt[1]) {
            if ((nbSolEq === 2) && solBorne && choixBorne) {
              pb1 = (((extremum[0] <= ordPt[1] + 0.1) && !extremum[2]) || ((extremum[1] >= ordPt[0] - 0.1) && !extremum[3]))
            } else {
              pb1 = (((extremum[0] <= ordPt[1] + 0.1) && !extremum[2]) || (extremum[1] > ordMax) || (extremum[0] > ordMax))
            }
          } else {
            if ((nbSolEq === 2) && solBorne && choixBorne) {
              pb1 = false
            } else {
              pb1 = (((extremum[1] >= ordPt[1] - 0.1) && !extremum[3]) || (extremum[0] < ordMin) || (extremum[1] < ordMin))
            }
          }
          comptErreur += 1
        } while (pb1 && (comptErreur <= 50))
        do {
          lecoef = (ordAncrage[1] - ordPt[1]) / (absAncrage[1] - absPt[1])
          ordAncrage[2] = ordPt[1] + (absAncrage[2] - absPt[1]) * lecoef
          if ((nbSolEq === 2) && solBorne && choixBorne && (ordPt[1] !== ordPt[0])) {
            if (ordPt[0] > ordPt[1]) {
              do {
                ordAncrage[3] = ordPt[1] + (0.5 + 0.2 * j3pGetRandomInt(0, 4))
              } while (ordAncrage[3] > ordPt[2])
            } else {
              do {
                ordAncrage[3] = ordPt[1] - (0.5 + 0.2 * j3pGetRandomInt(0, 4))
              } while (ordAncrage[3] < ordPt[2])
            }
            pb1 = false
          } else if ((nbSolEq === 2) && solBorne && !choixBorne && (ordPt[2] !== ordPt[3])) {
            if (ordPt[2] > ordPt[3]) {
              ordAncrage[3] = ordPt[2] + (j3pGetRandomInt(0, 1) * 2 - 1) * (0.3 + 0.2 * j3pGetRandomInt(0, 4))
            } else {
              ordAncrage[3] = ordPt[2] + (j3pGetRandomInt(0, 1) * 2 - 1) * (0.3 + 0.2 * j3pGetRandomInt(0, 4))
            }
            extremum = minMaxBezier(ordPt[1], ordAncrage[2], ordAncrage[3], ordPt[2])
            pb1 = ((extremum[1] >= ordMax) || (extremum[0] >= ordMax) || (extremum[1] <= ordMin) || (extremum[0] <= ordMin))
          } else {
            let pbAncrage3
            do {
              ordAncrage[3] = ordPt[1] - plusmoins * (0.5 + 0.2 * j3pGetRandomInt(0, 4))
              pbAncrage3 = ((ordAncrage[3] - ordPt[1]) * (ordAncrage[3] - ordPt[2]) > 0)
            } while (pbAncrage3)
            extremum = minMaxBezier(ordPt[1], ordAncrage[2], ordAncrage[3], ordPt[2])
            if (ordPt[2] > ordPt[1]) {
              pb1 = (((extremum[0] <= ordPt[1] + 0.1) && !extremum[2]) || ((extremum[1] >= ordPt[2] - 0.1) && !extremum[3]) || (extremum[0] < ordMin))
            } else {
              pb1 = (((extremum[1] >= ordPt[1] - 0.1) && !extremum[3]) || ((extremum[0] <= ordPt[2] + 0.1) && !extremum[2]) || (extremum[0] < ordMin))
            }
          }
          if (pb1) {
            absAncrage[2] = absPt[1] + 3 * (absAncrage[2] - absPt[1]) / 4
          }
          comptErreur += 1
        } while (pb1 && (comptErreur <= 50))
        do {
          lecoef = (ordAncrage[3] - ordPt[2]) / (absAncrage[3] - absPt[2])
          ordAncrage[4] = ordPt[2] + (absAncrage[4] - absPt[2]) * lecoef
          if ((nbSolEq === 2) && solBorne && !choixBorne && (ordPt[2] !== ordPt[3])) {
            if (ordPt[2] > ordPt[3]) {
              ordAncrage[5] = ordPt[3] + (1 + 0.2 * j3pGetRandomInt(2, 10))
            } else {
              ordAncrage[5] = ordPt[3] - (1 + 0.2 * j3pGetRandomInt(2, 10))
            }
          } else {
            ordAncrage[5] = ordPt[3] - plusmoins * (1 + 0.2 * j3pGetRandomInt(2, 10))
          }
          extremum = minMaxBezier(ordPt[2], ordAncrage[4], ordAncrage[5], ordPt[3])
          if (ordPt[3] > ordPt[2]) {
            if ((nbSolEq === 2) && solBorne) {
              pb1 = (((extremum[1] >= ordPt[3] - 0.1) && !extremum[3]) || (extremum[0] < ordMin))
            } else {
              pb1 = (((extremum[0] <= ordPt[2] + 0.1) && !extremum[2]) || (extremum[1] > ordMax))
            }
          } else {
            if ((nbSolEq === 2) && solBorne && (ordPt[3] < ordPt[2])) {
              pb1 = (((extremum[0] <= ordPt[3] + 0.1) && !extremum[2]) || (extremum[1] > ordMax))
            } else {
              pb1 = (((extremum[1] >= ordPt[2] - 0.1) && !extremum[3]) || (extremum[0] < ordMin))
            }
          }
          if (pb1) {
            absAncrage[4] = absPt[2] + 3 * (absAncrage[4] - absPt[2]) / 4
          }
          comptErreur += 1
        } while (pb1 && (comptErreur <= 50))
        me.logIfDebug('audessus :' + audessus + ' et extremum :' + extremum + '  pb1:' + pb1)
      } else {
        // il y a 2 ou 3 solutions à l’équation (aucune n’est une borne de l’intervalle)
        // on choisit si on part d’une ordonnée supérieure à c ou inférieure'
        audessus = Boolean(j3pGetRandomInt(0, 1))
        {
          let numc = 0
          nbChoix = 0
          do {
            abs1 = j3pGetRandomInt(-3, 3)
            abs2 = j3pGetRandomInt(-3, 3)
          } while ((Math.abs(abs1 - c) < Math.pow(10, -12)) || (Math.abs(abs2 - c) < Math.pow(10, -12)) || (Math.abs(abs1 - abs2) < Math.pow(10, -12)))
          do {
            abs3 = j3pGetRandomInt(-4, 4)
          } while ((Math.abs(abs3 - c) < Math.pow(10, -12)) || (Math.abs(abs2 - abs3) < Math.pow(10, -12)) || (Math.abs(abs1 - abs3) < Math.pow(10, -12)))
          absPt[0] = absMin
          if (nbSolEq === 2) {
            absOrdonnee = ordonneTab([abs1, abs2, c])
            absPt[4] = absMax
          } else if (nbSolEq === 3) {
            absOrdonnee = ordonneTab([abs1, abs2, abs3, c])
            absPt[4] = absOrdonnee[3]
            absPt[5] = absMax
          }
          absPt[1] = absOrdonnee[0]
          absPt[2] = absOrdonnee[1]
          absPt[3] = absOrdonnee[2]
          for (let i = 1; i <= absOrdonnee.length; i++) {
            if (absPt[i] !== c) {
              ordPt[i] = c
            }
            // je cherche le numéro où se trouve c
            if (absPt[i] === c) {
              numc = i
            }
          }
          absPtsAncrage = []
          // ancrage pour la première partie
          absPtsAncrage[0] = abscissesAncrage(absPt[0], absPt[1])
          absAncrage[0] = absPtsAncrage[0][0]
          absAncrage[1] = absPtsAncrage[0][1]
          // ancrage pour la deuxième partie
          absPtsAncrage[1] = abscissesAncrage(absPt[1], absPt[2])
          absAncrage[2] = absPtsAncrage[1][0]
          absAncrage[3] = absPtsAncrage[1][1]
          // ancrage pour la troisième partie
          absPtsAncrage[2] = abscissesAncrage(absPt[2], absPt[3])
          absAncrage[4] = absPtsAncrage[2][0]
          absAncrage[5] = absPtsAncrage[2][1]
          // ancrage pour la quatrième partie
          absPtsAncrage[3] = abscissesAncrage(absPt[3], absPt[4])
          absAncrage[6] = absPtsAncrage[3][0]
          absAncrage[7] = absPtsAncrage[3][1]
          if (nbSolEq === 3) {
            // ancrage pour la cinquième partie
            absPtsAncrage[4] = abscissesAncrage(absPt[4], absPt[5])
            absAncrage[8] = absPtsAncrage[4][0]
            absAncrage[9] = absPtsAncrage[4][1]
          }
          // pour les ordonnées des pts d’ancrage, il faudra faire un peu plus attention
          // je détermine les ordonnées des autres points
          if (audessus) {
            if (numc === 1) {
              do {
                do {
                  ordPt[0] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                  ordPt[1] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                } while (ordPt[1] <= ordPt[0])
                if (nbSolEq === 3) {
                  ordPt[5] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                } else {
                  ordPt[4] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                }
                // ordonnées des points d’ancrage
                nbEssai1 = 0
                nbEssai2 = 0
                do {
                  nbEssai1 += 1
                  ordAncrage[0] = ordPt[0] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1] + (2 * j3pGetRandomInt(0, 1) - 1) * 0.1 * j3pGetRandomInt(0, 7)
                  lecoef = (ordAncrage[1] - ordPt[1]) / (absAncrage[1] - absPt[1])
                  ordAncrage[2] = ordPt[1] + (absAncrage[2] - absPt[1]) * lecoef
                  ordAncrage[3] = ordPt[2] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  extremum1 = minMaxBezier(ordPt[0], ordAncrage[0], ordAncrage[1], ordPt[1])
                  extremum2 = minMaxBezier(ordPt[1], ordAncrage[2], ordAncrage[3], ordPt[2])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax))
                } while (pb1 && (nbEssai1 <= 50))
                do {
                  nbEssai2 += 1
                  lecoef = (ordAncrage[3] - ordPt[2]) / (absAncrage[3] - absPt[2])
                  ordAncrage[4] = ordPt[2] + (absAncrage[4] - absPt[2]) * lecoef
                  ordAncrage[5] = ordPt[2] - (0.5 + 0.2 * j3pGetRandomInt(0, 15))
                  lecoef = (ordAncrage[5] - ordPt[3]) / (absAncrage[5] - absPt[3])
                  ordAncrage[6] = ordPt[3] + (absAncrage[6] - absPt[3]) * lecoef
                  ordAncrage[7] = ordPt[4] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  pbSolEq3 = false
                  if (nbSolEq === 3) {
                    lecoef = (ordAncrage[7] - ordPt[4]) / (absAncrage[7] - absPt[4])
                    ordAncrage[8] = ordPt[4] + (absAncrage[8] - absPt[4]) * lecoef
                    ordAncrage[9] = ordPt[4] - (0.5 + 0.2 * j3pGetRandomInt(0, 15))
                    extremum3 = minMaxBezier(ordPt[4], ordAncrage[8], ordAncrage[9], ordPt[5])
                    pbSolEq3 = ((extremum3[0] < ordMin) || (extremum3[1] > ordMax))
                  }
                  extremum1 = minMaxBezier(ordPt[2], ordAncrage[4], ordAncrage[5], ordPt[3])
                  extremum2 = minMaxBezier(ordPt[3], ordAncrage[6], ordAncrage[7], ordPt[4])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax) || pbSolEq3)
                } while (pb1 && (nbEssai2 <= 50))
              } while ((nbEssai1 >= 50) || (nbEssai2 >= 50))
            } else if (numc === 2) {
              do {
                ordPt[0] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                ordPt[2] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                if (nbSolEq === 3) {
                  ordPt[5] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                } else {
                  ordPt[4] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                }
                // ordonnées des points d’ancrage
                nbEssai1 = 0
                nbEssai2 = 0
                do {
                  nbEssai1 += 1
                  ordAncrage[0] = ordPt[0] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1] + (0.5 + 0.2 * j3pGetRandomInt(2, 15))
                  lecoef = (ordAncrage[1] - ordPt[1]) / (absAncrage[1] - absPt[1])
                  ordAncrage[2] = ordPt[1] + (absAncrage[2] - absPt[1]) * lecoef
                  ordAncrage[3] = ordPt[2] + (2 * j3pGetRandomInt(0, 1) - 1) * 0.1 * j3pGetRandomInt(0, 7)
                  extremum1 = minMaxBezier(ordPt[0], ordAncrage[0], ordAncrage[1], ordPt[1])
                  extremum2 = minMaxBezier(ordPt[1], ordAncrage[2], ordAncrage[3], ordPt[2])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax))
                } while (pb1 && (nbEssai1 <= 50))
                do {
                  nbEssai2 += 1
                  lecoef = (ordAncrage[3] - ordPt[2]) / (absAncrage[3] - absPt[2])
                  ordAncrage[4] = ordPt[2] + (absAncrage[4] - absPt[2]) * lecoef
                  ordAncrage[5] = ordPt[3] - (0.5 + 0.2 * j3pGetRandomInt(0, 5))
                  lecoef = (ordAncrage[5] - ordPt[3]) / (absAncrage[5] - absPt[3])
                  ordAncrage[6] = ordPt[3] + (absAncrage[6] - absPt[3]) * lecoef
                  ordAncrage[7] = ordPt[4] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  pbSolEq3 = false
                  if (nbSolEq === 3) {
                    lecoef = (ordAncrage[7] - ordPt[4]) / (absAncrage[7] - absPt[4])
                    ordAncrage[8] = ordPt[4] + (absAncrage[8] - absPt[4]) * lecoef
                    ordAncrage[9] = ordPt[4] - (0.5 + 0.2 * j3pGetRandomInt(0, 15))
                    extremum3 = minMaxBezier(ordPt[4], ordAncrage[8], ordAncrage[9], ordPt[5])
                    pbSolEq3 = ((extremum3[0] < ordMin) || (extremum3[1] > ordMax))
                  }
                  extremum1 = minMaxBezier(ordPt[2], ordAncrage[4], ordAncrage[5], ordPt[3])
                  extremum2 = minMaxBezier(ordPt[3], ordAncrage[6], ordAncrage[7], ordPt[4])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax) || pbSolEq3)
                } while (pb1 && (nbEssai2 <= 50))
              } while ((nbEssai1 >= 50) || (nbEssai2 >= 50))
            } else if (numc === 3) {
              do {
                ordPt[0] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                if (nbSolEq === 3) {
                  ordPt[3] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                  ordPt[5] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                } else {
                  do {
                    ordPt[3] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                    ordPt[4] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                  } while (ordPt[3] <= ordPt[4])
                }
                // ordonnées des points d’ancrage
                nbEssai1 = 0
                nbEssai2 = 0
                do {
                  nbEssai1 += 1
                  ordAncrage[0] = ordPt[0] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1] + (0.5 + 0.2 * j3pGetRandomInt(2, 15))
                  lecoef = (ordAncrage[1] - ordPt[1]) / (absAncrage[1] - absPt[1])
                  ordAncrage[2] = ordPt[1] + (absAncrage[2] - absPt[1]) * lecoef
                  ordAncrage[3] = ordPt[2] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  extremum1 = minMaxBezier(ordPt[0], ordAncrage[0], ordAncrage[1], ordPt[1])
                  extremum2 = minMaxBezier(ordPt[1], ordAncrage[2], ordAncrage[3], ordPt[2])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax))
                } while (pb1 && (nbEssai1 <= 50))
                do {
                  nbEssai2 += 1
                  lecoef = (ordAncrage[3] - ordPt[2]) / (absAncrage[3] - absPt[2])
                  ordAncrage[4] = ordPt[2] + (absAncrage[4] - absPt[2]) * lecoef
                  ordAncrage[5] = ordPt[3] + (2 * j3pGetRandomInt(0, 1) - 1) * 0.1 * j3pGetRandomInt(0, 7)
                  lecoef = (ordAncrage[5] - ordPt[3]) / (absAncrage[5] - absPt[3])
                  ordAncrage[6] = ordPt[3] + (absAncrage[6] - absPt[3]) * lecoef
                  ordAncrage[7] = ordPt[4] + (0.3 + 0.2 * j3pGetRandomInt(0, 5))
                  pbSolEq3 = false
                  if (nbSolEq === 3) {
                    lecoef = (ordAncrage[7] - ordPt[4]) / (absAncrage[7] - absPt[4])
                    ordAncrage[8] = ordPt[4] + (absAncrage[8] - absPt[4]) * lecoef
                    ordAncrage[9] = ordPt[4] - (0.5 + 0.2 * j3pGetRandomInt(0, 15))
                    extremum3 = minMaxBezier(ordPt[4], ordAncrage[8], ordAncrage[9], ordPt[5])
                    pbSolEq3 = ((extremum3[0] < ordMin) || (extremum3[1] > ordMax))
                  }
                  extremum1 = minMaxBezier(ordPt[2], ordAncrage[4], ordAncrage[5], ordPt[3])
                  extremum2 = minMaxBezier(ordPt[3], ordAncrage[6], ordAncrage[7], ordPt[4])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax) || pbSolEq3)
                } while (pb1 && (nbEssai2 <= 50))
              } while ((nbEssai1 >= 50) || (nbEssai2 >= 50))
            } else if (numc === 4) { // seulement dans le cas où nbSolEq===3
              do {
                ordPt[0] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                do {
                  ordPt[4] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                  ordPt[5] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                } while (ordPt[4] >= ordPt[5])
                // ordonnées des points d’ancrage
                nbEssai1 = 0
                nbEssai2 = 0
                do {
                  nbEssai1 += 1
                  ordAncrage[0] = ordPt[0] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1] + (0.5 + 0.2 * j3pGetRandomInt(2, 15))
                  lecoef = (ordAncrage[1] - ordPt[1]) / (absAncrage[1] - absPt[1])
                  ordAncrage[2] = ordPt[1] + (absAncrage[2] - absPt[1]) * lecoef
                  ordAncrage[3] = ordPt[2] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  extremum1 = minMaxBezier(ordPt[0], ordAncrage[0], ordAncrage[1], ordPt[1])
                  extremum2 = minMaxBezier(ordPt[1], ordAncrage[2], ordAncrage[3], ordPt[2])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax))
                } while (pb1 && (nbEssai1 <= 50))
                do {
                  nbEssai2 += 1
                  lecoef = (ordAncrage[3] - ordPt[2]) / (absAncrage[3] - absPt[2])
                  ordAncrage[4] = ordPt[2] + (absAncrage[4] - absPt[2]) * lecoef
                  ordAncrage[5] = ordPt[3] + (0.3 + 0.2 * j3pGetRandomInt(0, 5))
                  lecoef = (ordAncrage[5] - ordPt[3]) / (absAncrage[5] - absPt[3])
                  ordAncrage[6] = ordPt[3] + (absAncrage[6] - absPt[3]) * lecoef
                  ordAncrage[7] = ordPt[4] - (0.3 + 0.2 * j3pGetRandomInt(0, 5))
                  pbSolEq3 = false
                  if (nbSolEq === 3) {
                    lecoef = (ordAncrage[7] - ordPt[4]) / (absAncrage[7] - absPt[4])
                    ordAncrage[8] = ordPt[4] + (absAncrage[8] - absPt[4]) * lecoef
                    ordAncrage[9] = ordPt[5] - (0.1 * j3pGetRandomInt(0, 9))
                    extremum3 = minMaxBezier(ordPt[4], ordAncrage[8], ordAncrage[9], ordPt[5])
                    pbSolEq3 = ((extremum3[0] < ordMin) || (extremum3[1] >= c - 0.2))
                  }
                  extremum1 = minMaxBezier(ordPt[2], ordAncrage[4], ordAncrage[5], ordPt[3])
                  extremum2 = minMaxBezier(ordPt[3], ordAncrage[6], ordAncrage[7], ordPt[4])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax) || pbSolEq3)
                } while (pb1 && (nbEssai2 <= 50))
              } while ((nbEssai1 >= 50) || (nbEssai2 >= 50))
            }
          } else {
            if (numc === 1) {
              do {
                do {
                  ordPt[0] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                  ordPt[1] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                } while (ordPt[1] >= ordPt[0])
                if (nbSolEq === 3) {
                  ordPt[5] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                } else {
                  ordPt[4] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                }
                // ordonnées des points d’ancrage
                nbEssai1 = 0
                nbEssai2 = 0
                do {
                  nbEssai1 += 1
                  ordAncrage[0] = ordPt[0] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1] + (2 * j3pGetRandomInt(0, 1) - 1) * 0.1 * j3pGetRandomInt(0, 7)
                  lecoef = (ordAncrage[1] - ordPt[1]) / (absAncrage[1] - absPt[1])
                  ordAncrage[2] = ordPt[1] + (absAncrage[2] - absPt[1]) * lecoef
                  ordAncrage[3] = ordPt[2] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  extremum1 = minMaxBezier(ordPt[0], ordAncrage[0], ordAncrage[1], ordPt[1])
                  extremum2 = minMaxBezier(ordPt[1], ordAncrage[2], ordAncrage[3], ordPt[2])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax))
                } while (pb1 && (nbEssai1 <= 50))
                do {
                  nbEssai2 += 1
                  lecoef = (ordAncrage[3] - ordPt[2]) / (absAncrage[3] - absPt[2])
                  ordAncrage[4] = ordPt[2] + (absAncrage[4] - absPt[2]) * lecoef
                  ordAncrage[5] = ordPt[2] + (0.5 + 0.2 * j3pGetRandomInt(0, 15))
                  lecoef = (ordAncrage[5] - ordPt[3]) / (absAncrage[5] - absPt[3])
                  ordAncrage[6] = ordPt[3] + (absAncrage[6] - absPt[3]) * lecoef
                  ordAncrage[7] = ordPt[4] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  pbSolEq3 = false
                  if (nbSolEq === 3) {
                    lecoef = (ordAncrage[7] - ordPt[4]) / (absAncrage[7] - absPt[4])
                    ordAncrage[8] = ordPt[4] + (absAncrage[8] - absPt[4]) * lecoef
                    ordAncrage[9] = ordPt[4] + (0.5 + 0.2 * j3pGetRandomInt(0, 15))
                    extremum3 = minMaxBezier(ordPt[4], ordAncrage[8], ordAncrage[9], ordPt[5])
                    pbSolEq3 = ((extremum3[0] < ordMin) || (extremum3[1] > ordMax))
                  }
                  extremum1 = minMaxBezier(ordPt[2], ordAncrage[4], ordAncrage[5], ordPt[3])
                  extremum2 = minMaxBezier(ordPt[3], ordAncrage[6], ordAncrage[7], ordPt[4])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax) || pbSolEq3)
                } while (pb1 && (nbEssai2 <= 50))
              } while ((nbEssai1 >= 50) || (nbEssai2 >= 50))
            } else if (numc === 2) {
              do {
                ordPt[0] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                ordPt[2] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                if (nbSolEq === 3) {
                  ordPt[5] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                } else {
                  ordPt[4] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                }
                // ordonnées des points d’ancrage
                nbEssai1 = 0
                nbEssai2 = 0
                do {
                  nbEssai1 += 1
                  ordAncrage[0] = ordPt[0] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1] - (0.5 + 0.2 * j3pGetRandomInt(2, 15))
                  lecoef = (ordAncrage[1] - ordPt[1]) / (absAncrage[1] - absPt[1])
                  ordAncrage[2] = ordPt[1] + (absAncrage[2] - absPt[1]) * lecoef
                  ordAncrage[3] = ordPt[2] + (2 * j3pGetRandomInt(0, 1) - 1) * 0.1 * j3pGetRandomInt(0, 7)
                  extremum1 = minMaxBezier(ordPt[0], ordAncrage[0], ordAncrage[1], ordPt[1])
                  extremum2 = minMaxBezier(ordPt[1], ordAncrage[2], ordAncrage[3], ordPt[2])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax))
                } while (pb1 && (nbEssai1 <= 50))
                do {
                  nbEssai2 += 1
                  lecoef = (ordAncrage[3] - ordPt[2]) / (absAncrage[3] - absPt[2])
                  ordAncrage[4] = ordPt[2] + (absAncrage[4] - absPt[2]) * lecoef
                  ordAncrage[5] = ordPt[3] + (0.5 + 0.2 * j3pGetRandomInt(0, 15))
                  lecoef = (ordAncrage[5] - ordPt[3]) / (absAncrage[5] - absPt[3])
                  ordAncrage[6] = ordPt[3] + (absAncrage[6] - absPt[3]) * lecoef
                  ordAncrage[7] = ordPt[4] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  pbSolEq3 = false
                  if (nbSolEq === 3) {
                    lecoef = (ordAncrage[7] - ordPt[4]) / (absAncrage[7] - absPt[4])
                    ordAncrage[8] = ordPt[4] + (absAncrage[8] - absPt[4]) * lecoef
                    ordAncrage[9] = ordPt[4] + (0.5 + 0.2 * j3pGetRandomInt(0, 15))
                    extremum3 = minMaxBezier(ordPt[4], ordAncrage[8], ordAncrage[9], ordPt[5])
                    pbSolEq3 = ((extremum3[0] < ordMin) || (extremum3[1] > ordMax))
                  }
                  extremum1 = minMaxBezier(ordPt[2], ordAncrage[4], ordAncrage[5], ordPt[3])
                  extremum2 = minMaxBezier(ordPt[3], ordAncrage[6], ordAncrage[7], ordPt[4])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax) || pbSolEq3)
                } while (pb1 && (nbEssai2 <= 50))
              } while ((nbEssai1 >= 50) || (nbEssai2 >= 50))
            } else if (numc === 3) {
              do {
                ordPt[0] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                if (nbSolEq === 3) {
                  ordPt[3] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                  ordPt[5] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                } else {
                  do {
                    ordPt[3] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                    ordPt[4] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                  } while (ordPt[3] >= ordPt[4])
                }
                // ordonnées des points d’ancrage
                nbEssai1 = 0
                nbEssai2 = 0
                do {
                  nbEssai1 += 1
                  ordAncrage[0] = ordPt[0] - (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1] - (0.5 + 0.2 * j3pGetRandomInt(2, 15))
                  lecoef = (ordAncrage[1] - ordPt[1]) / (absAncrage[1] - absPt[1])
                  ordAncrage[2] = ordPt[1] + (absAncrage[2] - absPt[1]) * lecoef
                  ordAncrage[3] = ordPt[2] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  extremum1 = minMaxBezier(ordPt[0], ordAncrage[0], ordAncrage[1], ordPt[1])
                  extremum2 = minMaxBezier(ordPt[1], ordAncrage[2], ordAncrage[3], ordPt[2])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax))
                } while (pb1 && (nbEssai1 <= 50))
                do {
                  nbEssai2 += 1
                  lecoef = (ordAncrage[3] - ordPt[2]) / (absAncrage[3] - absPt[2])
                  ordAncrage[4] = ordPt[2] + (absAncrage[4] - absPt[2]) * lecoef
                  ordAncrage[5] = ordPt[3] + (2 * j3pGetRandomInt(0, 1) - 1) * 0.1 * j3pGetRandomInt(0, 7)
                  lecoef = (ordAncrage[5] - ordPt[3]) / (absAncrage[5] - absPt[3])
                  ordAncrage[6] = ordPt[3] + (absAncrage[6] - absPt[3]) * lecoef
                  ordAncrage[7] = ordPt[4] - (0.3 + 0.2 * j3pGetRandomInt(0, 5))
                  pbSolEq3 = false
                  if (nbSolEq === 3) {
                    lecoef = (ordAncrage[7] - ordPt[4]) / (absAncrage[7] - absPt[4])
                    ordAncrage[8] = ordPt[4] + (absAncrage[8] - absPt[4]) * lecoef
                    ordAncrage[9] = ordPt[4] + (0.5 + 0.2 * j3pGetRandomInt(0, 15))
                    extremum3 = minMaxBezier(ordPt[4], ordAncrage[8], ordAncrage[9], ordPt[5])
                    pbSolEq3 = ((extremum3[0] < ordMin) || (extremum3[1] > ordMax))
                  }
                  extremum1 = minMaxBezier(ordPt[2], ordAncrage[4], ordAncrage[5], ordPt[3])
                  extremum2 = minMaxBezier(ordPt[3], ordAncrage[6], ordAncrage[7], ordPt[4])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax) || pbSolEq3)
                } while (pb1 && (nbEssai2 <= 50))
              } while ((nbEssai1 >= 50) || (nbEssai2 >= 50))
            } else if (numc === 4) { // seulement dans le cas où nbSolEq===3
              do {
                ordPt[0] = c - j3pGetRandomInt(1, (c - ordMin - 1))
                do {
                  ordPt[4] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                  ordPt[5] = c + j3pGetRandomInt(1, (ordMax - c - 1))
                } while (ordPt[4] <= ordPt[5])
                // ordonnées des points d’ancrage
                nbEssai1 = 0
                nbEssai2 = 0
                do {
                  nbEssai1 += 1
                  ordAncrage[0] = ordPt[0] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  ordAncrage[1] = ordPt[1] - (0.5 + 0.2 * j3pGetRandomInt(2, 15))
                  lecoef = (ordAncrage[1] - ordPt[1]) / (absAncrage[1] - absPt[1])
                  ordAncrage[2] = ordPt[1] + (absAncrage[2] - absPt[1]) * lecoef
                  ordAncrage[3] = ordPt[2] + (0.3 + 0.2 * j3pGetRandomInt(2, 10))
                  extremum1 = minMaxBezier(ordPt[0], ordAncrage[0], ordAncrage[1], ordPt[1])
                  extremum2 = minMaxBezier(ordPt[1], ordAncrage[2], ordAncrage[3], ordPt[2])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax))
                } while (pb1 && (nbEssai1 <= 50))
                do {
                  nbEssai2 += 1
                  lecoef = (ordAncrage[3] - ordPt[2]) / (absAncrage[3] - absPt[2])
                  ordAncrage[4] = ordPt[2] + (absAncrage[4] - absPt[2]) * lecoef
                  ordAncrage[5] = ordPt[3] - (0.3 + 0.2 * j3pGetRandomInt(0, 5))
                  lecoef = (ordAncrage[5] - ordPt[3]) / (absAncrage[5] - absPt[3])
                  ordAncrage[6] = ordPt[3] + (absAncrage[6] - absPt[3]) * lecoef
                  ordAncrage[7] = ordPt[4] + (0.3 + 0.2 * j3pGetRandomInt(0, 5))
                  pbSolEq3 = false
                  if (nbSolEq === 3) {
                    lecoef = (ordAncrage[7] - ordPt[4]) / (absAncrage[7] - absPt[4])
                    ordAncrage[8] = ordPt[4] + (absAncrage[8] - absPt[4]) * lecoef
                    ordAncrage[9] = ordPt[5] + (0.2 * j3pGetRandomInt(0, 9))
                    extremum3 = minMaxBezier(ordPt[4], ordAncrage[8], ordAncrage[9], ordPt[5])
                    pbSolEq3 = ((extremum3[0] < c + 0.2) || (extremum3[1] > ordMax))
                  }
                  extremum1 = minMaxBezier(ordPt[2], ordAncrage[4], ordAncrage[5], ordPt[3])
                  extremum2 = minMaxBezier(ordPt[3], ordAncrage[6], ordAncrage[7], ordPt[4])
                  pb1 = ((extremum1[0] < ordMin) || (extremum1[1] > ordMax) || (extremum2[0] < ordMin) || (extremum2[1] > ordMax) || pbSolEq3)
                } while (pb1 && (nbEssai2 <= 50))
              } while ((nbEssai1 >= 50) || (nbEssai2 >= 50))
            }
          }
        }
      }
    } while (comptErreur >= 50)
    // construction de la courbe (courbe de Bézier)
    // P(t) = (1-t)^3 P1+3t(1-t)^2 P2+3t^2 (1-t) P3+t^3 P4
    // où P1=(absPt[i],ordPt[i]), P2=(absAncrage[2*i],ordAncrage[2*i]), P3=(absAncrage[2*i+1],ordAncrage[2*i+1]) et P4=(absPt[i+1],ordPt[i+1])
    // on crée un tableau
    const nbvaleurs = 100// on a nbvaleurs pts entre deux points précis
    const tabval = []
    for (let i = 0; i <= nbParties; i++) {
      for (let k = 1; k <= nbvaleurs + 1; k++) {
        tabval[i * nbvaleurs + k - 1] = []
        const t = (k - 1) / nbvaleurs
        tabval[i * nbvaleurs + k - 1][0] = Math.pow(1 - t, 3) * absPt[i] + 3 * t * Math.pow(1 - t, 2) * absAncrage[2 * i] + 3 * Math.pow(t, 2) * (1 - t) * absAncrage[2 * i + 1] + Math.pow(t, 3) * absPt[i + 1]
        tabval[i * nbvaleurs + k - 1][1] = Math.pow(1 - t, 3) * ordPt[i] + 3 * t * Math.pow(1 - t, 2) * ordAncrage[2 * i] + 3 * Math.pow(t, 2) * (1 - t) * ordAncrage[2 * i + 1] + Math.pow(t, 3) * ordPt[i + 1]
      }
    }
    me.logIfDebug('ordPt:' + ordPt + '  au-dessus : ' + audessus + '  extremum:' + extremum)
    // construction du repère
    const uniterep = 30
    const largeurRepere = ((absMax - absMin) + 1) * uniterep
    const hauteurRepere = ((ordMax - ordMin) + 1) * uniterep
    stor.divRepere = j3pAddElt(stor.conteneurD, 'div')
    j3pStyle(stor.divRepere, { textAlign: 'center' })
    const couleurCb = me.styles.moyen.enonce.color
    stor.repere = new Repere({
      idConteneur: stor.divRepere,
      aimantage: false,
      visible: true,
      larg: largeurRepere,
      haut: hauteurRepere,

      pasdunegraduationX: 1,
      pixelspargraduationX: uniterep,
      pasdunegraduationY: 1,
      pixelspargraduationY: uniterep,
      debuty: 0,
      negatifs: true,
      fixe: true,
      trame: true,
      objets: [
        { type: 'courbe_tabvaleur', nom: 'c1', tab_val: tabval, style: { couleur: 'black', epaisseur: 1 } },
        {
          type: 'point',
          nom: 'X1',
          par1: absMin,
          par2: -5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCb, epaisseur: 2, taille: 18 }
        },
        {
          type: 'point',
          nom: 'Y1',
          par1: absMax,
          par2: -5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCb, epaisseur: 2, taille: 18 }
        },
        {
          type: 'point',
          nom: 'X2',
          par1: absMin,
          par2: -5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCb, epaisseur: 2, taille: 18 }
        },
        {
          type: 'point',
          nom: 'Y2',
          par1: absMax,
          par2: -5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: couleurCb, epaisseur: 2, taille: 18 }
        },
        { type: 'droite', nom: 'd_mov', par1: 'X1', par2: 'Y1', style: { couleur: 'rgb(255,0,0)', epaisseur: 2 } },
        { type: 'droite', nom: 'd_mov2', par1: 'X2', par2: 'Y2', style: { couleur: 'rgb(255,0,0)', epaisseur: 8 } }
      ]
    })
    stor.repere.construit()
    // on le stocke pour éviter de rappeler j3pElement à chaque fois que la souris bouge d’un pixel
    stor.droiteX1Y1 = j3pElement('droiteX1Y1')
    stor.droiteX2Y2 = j3pElement('droiteX2Y2')
    stor.mouvement = false
    stor.droiteX2Y2.addEventListener('mousedown', mouseDownListener, false)
    stor.droiteX2Y2.setAttribute('stroke-opacity', 0)
    // la figure est entièrement construite
    let ineqLarge, monIneq, laReponse, inegaliteInverse, pbLargeStricte, tabIntervalles
    if ((me.questionCourante) === 1) {
      // ceci ne se fait que pour la première étape
      ineqLarge = j3pGetRandomInt(0, 1)
    } else {
      ineqLarge = (stor.stockage[5] + 1) % 2
    }
    if (ineqLarge === 0) {
      // l’inégalité sera stricte'
      if (monsigne === 'inf') {
        monIneq = nomFct + '(x)< ' + c
      } else {
        monIneq = nomFct + '(x)> ' + c
      }
    } else {
      // l’inégalité sera large'
      if (monsigne === 'inf') {
        monIneq = nomFct + '(x)\\leq ' + c
      } else {
        monIneq = nomFct + '(x)\\geq ' + c
      }
    }
    me.logIfDebug('nbSolEq:' + nbSolEq + '  audessus:' + audessus + '  monsigne:' + monsigne + '  ineqLarge:' + ineqLarge + '  monIneq:' + monIneq)
    // je préfère gérer les solutions ici pour mieux m’y retrouver'
    if (nbSolEq === 1) {
      if (audessus) {
        if (monsigne === 'inf') {
          if (ineqLarge === 1) {
            laReponse = '[' + abs1 + ';' + absMax + ']'
            inegaliteInverse = '[' + absMin + ';' + abs1 + ']'
            pbLargeStricte = ']' + abs1 + ';' + absMax + ']'
          } else {
            laReponse = ']' + abs1 + ';' + absMax + ']'
            inegaliteInverse = '[' + absMin + ';' + abs1 + '['
            pbLargeStricte = '[' + abs1 + ';' + absMax + ']'
          }
          tabIntervalles = [abs1 + ';' + absMax]
        } else {
          if (ineqLarge === 1) {
            laReponse = '[' + absMin + ';' + abs1 + ']'
            inegaliteInverse = '[' + abs1 + ';' + absMax + ']'
            pbLargeStricte = '[' + absMin + ';' + abs1 + '['
          } else {
            laReponse = '[' + absMin + ';' + abs1 + '['
            inegaliteInverse = ']' + abs1 + ';' + absMax + ']'
            pbLargeStricte = '[' + absMin + ';' + abs1 + ']'
          }
          tabIntervalles = [absMin + ';' + abs1]
        }
      } else {
        if (monsigne === 'inf') {
          if (ineqLarge === 1) {
            laReponse = '[' + absMin + ';' + abs1 + ']'
            inegaliteInverse = '[' + abs1 + ';' + absMax + ']'
            pbLargeStricte = '[' + absMin + ';' + abs1 + '['
          } else {
            laReponse = '[' + absMin + ';' + abs1 + '['
            inegaliteInverse = ']' + abs1 + ';' + absMax + ']'
            pbLargeStricte = '[' + absMin + ';' + abs1 + ']'
          }
          tabIntervalles = [absMin + ';' + abs1]
        } else {
          if (ineqLarge === 1) {
            laReponse = '[' + abs1 + ';' + absMax + ']'
            inegaliteInverse = '[' + absMin + ';' + abs1 + ']'
            pbLargeStricte = ']' + abs1 + ';' + absMax + ']'
          } else {
            laReponse = ']' + abs1 + ';' + absMax + ']'
            inegaliteInverse = '[' + absMin + ';' + abs1 + '['
            pbLargeStricte = '[' + abs1 + ';' + absMax + ']'
          }
          tabIntervalles = [abs1 + ';' + absMax]
        }
      }
    } else if (nbSolEq === 2) {
      if (solBorne) {
        // l’une des borne de l’intervalle de def est sol de l’equation'
        if (choixBorne) {
          // c’est la borne inf'
          if (audessus) {
            if (monsigne === 'inf') {
              if (ineqLarge === 1) {
                // ce cas de figure n’est pas jouable car il y aurait un singleton'
                // donc je vais remettre une inégalité stricte
                ineqLarge = 0
                monIneq = nomFct + '(x)< ' + c
              }
              laReponse = ']' + abs1 + ';' + absMax + ']'
              inegaliteInverse = '[' + absMin + ';' + abs1 + '['
              pbLargeStricte = '[' + abs1 + ';' + absMax + ']'
              tabIntervalles = [abs1 + ';' + absMax]
            } else {
              if (ineqLarge === 1) {
                laReponse = '[' + absMin + ';' + abs1 + ']'
                inegaliteInverse = '[' + abs1 + ';' + absMax + ']'
                pbLargeStricte = ']' + absMin + ';' + abs1 + '['
              } else {
                laReponse = ']' + absMin + ';' + abs1 + '['
                inegaliteInverse = ']' + abs1 + ';' + absMax + ']'
                pbLargeStricte = '[' + absMin + ';' + abs1 + ']'
              }
              tabIntervalles = [absMin + ';' + abs1]
            }
          } else {
            if (monsigne === 'sup') {
              if (ineqLarge === 1) {
                // ce cas de figure n’est pas jouable car il y aurait un singleton'
                // donc je vais remettre une inégalité stricte
                ineqLarge = 0
                monIneq = nomFct + '(x)> ' + c
              }
              laReponse = ']' + abs1 + ';' + absMax + ']'
              inegaliteInverse = '[' + absMin + ';' + abs1 + '['
              pbLargeStricte = '[' + abs1 + ';' + absMax + ']'
              tabIntervalles = [abs1 + ';' + absMax]
            } else {
              if (ineqLarge === 1) {
                laReponse = '[' + absMin + ';' + abs1 + ']'
                inegaliteInverse = '[' + abs1 + ';' + absMax + ']'
                pbLargeStricte = ']' + absMin + ';' + abs1 + '['
              } else {
                laReponse = ']' + absMin + ';' + abs1 + '['
                inegaliteInverse = ']' + abs1 + ';' + absMax + ']'
                pbLargeStricte = '[' + absMin + ';' + abs1 + ']'
              }
              tabIntervalles = [absMin + ';' + abs1]
            }
          }
        } else {
          // c’est la borne sup'
          if (audessus) {
            if (monsigne === 'sup') {
              if (ineqLarge === 1) {
                // ce cas de figure n’est pas jouable car il y aurait un singleton'
                // donc je vais remettre une inégalité stricte
                ineqLarge = 0
                monIneq = nomFct + '(x)> ' + c
              }
              laReponse = '[' + absMin + ';' + abs1 + '['
              inegaliteInverse = ']' + abs1 + ';' + absMax + '['
              pbLargeStricte = '[' + absMin + ';' + abs1 + ']'
              tabIntervalles = [absMin + ';' + abs1]
            } else {
              if (ineqLarge === 1) {
                laReponse = '[' + abs1 + ';' + absMax + ']'
                inegaliteInverse = '[' + absMin + ';' + abs1 + ']'
                pbLargeStricte = ']' + abs1 + ';' + absMax + '['
              } else {
                laReponse = ']' + abs1 + ';' + absMax + '['
                inegaliteInverse = '[' + absMin + ';' + abs1 + '['
                pbLargeStricte = '[' + abs1 + ';' + absMax + ']'
              }
              tabIntervalles = [abs1 + ';' + absMax]
            }
          } else {
            if (monsigne === 'inf') {
              if (ineqLarge === 1) {
                // ce cas de figure n’est pas jouable car il y aurait un singleton'
                // donc je vais remettre une inégalité stricte
                ineqLarge = 0
                monIneq = nomFct + '(x)< ' + c
              }
              laReponse = '[' + absMin + ';' + abs1 + '['
              inegaliteInverse = ']' + abs1 + ';' + absMax + '['
              pbLargeStricte = '[' + absMin + ';' + abs1 + ']'
              tabIntervalles = [absMin + ';' + abs1]
            } else {
              if (ineqLarge === 1) {
                laReponse = '[' + abs1 + ';' + absMax + ']'
                inegaliteInverse = '[' + absMin + ';' + abs1 + ']'
                pbLargeStricte = ']' + abs1 + ';' + absMax + '['
              } else {
                laReponse = ']' + abs1 + ';' + absMax + '['
                inegaliteInverse = ']' + absMin + ';' + abs1 + '['
                pbLargeStricte = '[' + abs1 + ';' + absMax + ']'
              }
              tabIntervalles = [abs1 + ';' + absMax]
            }
          }
        }
      } else {
        solMin = Math.min(abs1, abs2)
        solMax = Math.max(abs1, abs2)
        if (audessus) {
          if (monsigne === 'inf') {
            if (ineqLarge === 1) {
              laReponse = '[' + solMin + ';' + solMax + ']'
              inegaliteInverse = '[' + absMin + ';' + solMin + ']\\cup[' + solMax + ';' + absMax + ']'
              pbLargeStricte = ']' + solMin + ';' + solMax + '['
            } else {
              laReponse = ']' + solMin + ';' + solMax + '['
              inegaliteInverse = '[' + absMin + ';' + solMin + '[\\cup]' + solMax + ';' + absMax + ']'
              pbLargeStricte = '[' + solMin + ';' + solMax + ']'
            }
            tabIntervalles = [solMin + ';' + solMax]
          } else {
            if (ineqLarge === 1) {
              laReponse = '[' + absMin + ';' + solMin + ']\\cup[' + solMax + ';' + absMax + ']'
              inegaliteInverse = '[' + solMin + ';' + solMax + ']'
              pbLargeStricte = '[' + absMin + ';' + solMin + '[\\cup]' + solMax + ';' + absMax + ']'
            } else {
              laReponse = '[' + absMin + ';' + solMin + '[\\cup]' + solMax + ';' + absMax + ']'
              inegaliteInverse = ']' + solMin + ';' + solMax + '['
              pbLargeStricte = '[' + absMin + ';' + solMin + ']\\cup[' + solMax + ';' + absMax + ']'
            }
            tabIntervalles = [absMin + ';' + solMin, solMax + ';' + absMax]
          }
        } else {
          if (monsigne === 'sup') {
            if (ineqLarge === 1) {
              laReponse = '[' + solMin + ';' + solMax + ']'
              inegaliteInverse = '[' + absMin + ';' + solMin + ']\\cup[' + solMax + ';' + absMax + ']'
              pbLargeStricte = ']' + solMin + ';' + solMax + '['
            } else {
              laReponse = ']' + solMin + ';' + solMax + '['
              inegaliteInverse = '[' + absMin + ';' + solMin + '[\\cup]' + solMax + ';' + absMax + ']'
              pbLargeStricte = '[' + solMin + ';' + solMax + ']'
            }
            tabIntervalles = [solMin + ';' + solMax]
          } else {
            if (ineqLarge === 1) {
              laReponse = '[' + absMin + ';' + solMin + ']\\cup[' + solMax + ';' + absMax + ']'
              inegaliteInverse = '[' + solMin + ';' + solMax + ']'
              pbLargeStricte = '[' + absMin + ';' + solMin + '[\\cup]' + solMax + ';' + absMax + ']'
            } else {
              laReponse = '[' + absMin + ';' + solMin + '[\\cup]' + solMax + ';' + absMax + ']'
              inegaliteInverse = ']' + solMin + ';' + solMax + '['
              pbLargeStricte = '[' + absMin + ';' + solMin + ']\\cup[' + solMax + ';' + absMax + ']'
            }
            tabIntervalles = [absMin + ';' + solMin, solMax + ';' + absMax]
          }
        }
      }
    } else if (nbSolEq === 3) {
      // j’ordonne les différentes solutions de l’équation
      const tabInit = [abs1, abs2, abs3]
      const tabOrd = ordonneTab(tabInit)
      solMin = tabOrd[0]
      solMed = tabOrd[1]
      solMax = tabOrd[2]
      if (audessus) {
        if (monsigne === 'inf') {
          if (ineqLarge === 1) {
            laReponse = '[' + solMin + ';' + solMed + ']\\cup[' + solMax + ';' + absMax + ']'
            inegaliteInverse = '[' + absMin + ';' + solMin + ']\\cup[' + solMed + ';' + solMax + ']'
            pbLargeStricte = ']' + solMin + ';' + solMed + '[\\cup]' + solMax + ';' + absMax + ']'
          } else {
            laReponse = ']' + solMin + ';' + solMed + '[\\cup]' + solMax + ';' + absMax + ']'
            inegaliteInverse = '[' + absMin + ';' + solMin + '[\\cup]' + solMed + ';' + solMax + '['
            pbLargeStricte = '[' + solMin + ';' + solMed + ']\\cup[' + solMax + ';' + absMax + ']'
          }
          tabIntervalles = [solMin + ';' + solMed, 'cup', solMax + ';' + absMax]
        } else {
          if (ineqLarge === 1) {
            laReponse = '[' + absMin + ';' + solMin + ']\\cup[' + solMed + ';' + solMax + ']'
            inegaliteInverse = '[' + solMin + ';' + solMed + ']\\cup[' + solMax + ';' + absMax + ']'
            pbLargeStricte = '[' + absMin + ';' + solMin + '[\\cup]' + solMed + ';' + solMax + '['
          } else {
            laReponse = '[' + absMin + ';' + solMin + '[\\cup]' + solMed + ';' + solMax + '['
            inegaliteInverse = ']' + solMin + ';' + solMed + '[\\cup]' + solMax + ';' + absMax + '['
            pbLargeStricte = '[' + absMin + ';' + solMin + ']\\cup[' + solMed + ';' + solMax + ']'
          }
          tabIntervalles = [absMin + ';' + solMin, 'cup', solMed + ';' + solMax]
        }
      } else {
        if (monsigne === 'sup') {
          if (ineqLarge === 1) {
            laReponse = '[' + solMin + ';' + solMed + ']\\cup[' + solMax + ';' + absMax + ']'
            inegaliteInverse = '[' + absMin + ';' + solMin + ']\\cup[' + solMed + ';' + solMax + ']'
            pbLargeStricte = ']' + solMin + ';' + solMed + '[\\cup]' + solMax + ';' + absMax + ']'
          } else {
            laReponse = ']' + solMin + ';' + solMed + '[\\cup]' + solMax + ';' + absMax + ']'
            inegaliteInverse = '[' + absMin + ';' + solMin + '[\\cup]' + solMed + ';' + solMax + '['
            pbLargeStricte = '[' + solMin + ';' + solMed + ']\\cup[' + solMax + ';' + absMax + ']'
          }
          tabIntervalles = [solMin + ';' + solMed, 'cup', solMax + ';' + absMax]
        } else {
          if (ineqLarge === 1) {
            laReponse = '[' + absMin + ';' + solMin + ']\\cup[' + solMed + ';' + solMax + ']'
            inegaliteInverse = '[' + solMin + ';' + solMed + ']\\cup[' + solMax + ';' + absMax + ']'
            pbLargeStricte = '[' + absMin + ';' + solMin + '[\\cup]' + solMed + ';' + solMax + '['
          } else {
            laReponse = '[' + absMin + ';' + solMin + '[\\cup]' + solMed + ';' + solMax + '['
            inegaliteInverse = ']' + solMin + ';' + solMed + '[\\cup]' + solMax + ';' + absMax + '['
            pbLargeStricte = '[' + absMin + ';' + solMin + ']\\cup[' + solMed + ';' + solMax + ']'
          }
          tabIntervalles = [absMin + ';' + solMin, 'cup', solMed + ';' + solMax]
        }
      }
    }
    stor.stockage[0] = nbSolEq
    stor.stockage[1] = laReponse
    stor.stockage[2] = inegaliteInverse
    stor.stockage[3] = pbLargeStricte
    stor.stockage[4] = tabIntervalles
    stor.stockage[5] = ineqLarge
    stor.stockage[6] = audessus
    stor.stockage[7] = monsigne
    stor.stockage[10] = c
    stor.stockage[11] = ordPt
    stor.stockage[12] = absMin
    stor.stockage[13] = absMax
    if (nbSolEq === 1) {
      stor.stockage[14] = abs1
    } else if (nbSolEq === 2) {
      stor.stockage[8] = solBorne
      if (solBorne) {
        stor.stockage[9] = choixBorne
        stor.stockage[14] = abs1
      } else {
        stor.stockage[15] = solMin
        stor.stockage[16] = solMax
      }
    } else {
      stor.stockage[15] = solMin
      stor.stockage[16] = solMed
      stor.stockage[17] = solMax
    }
    me.logIfDebug('laReponse:' + laReponse + '   monIneq:' + monIneq)

    stor.zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    const elt = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, {
      b: monIneq,
      c: '[' + absMin + ' ; ' + absMax + ']',
      inputmq1: { texte: '', maxchars: '20' }
    })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d.,+-[];', {
      commandes: (ds.palette_complete) ? ['inter', 'union', 'vide'] : ['vide']
    })

    stor.zoneConsigne3 = j3pAddElt(stor.conteneur, 'div', '', { marginTop: '1em' })
    stor.zonePalette = j3pAddElt(stor.conteneur, 'div', '', { marginTop: '1em' })
    if (ds.palette_complete) {
      j3pPaletteMathquill(stor.zonePalette, stor.zoneInput, {
        liste: ['inter', 'union', 'vide', '[', ']']
      })
    } else {
      j3pPaletteMathquill(stor.zonePalette, stor.zoneInput, {
        liste: ['vide', '[', ']']
      })
    }
    stor.zonesRep = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '50px',
        color: me.styles.cfaux
      }
    })
    stor.commentDroite = j3pAddElt(stor.conteneurD, 'div')
    j3pStyle(stor.commentDroite, { paddingTop: '15px' })
    j3pAffiche(stor.commentDroite, '', ds.textes.comment_droite)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })
    j3pFocus(mesZonesSaisie[0])
    stor.explications = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '15px' } })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // on souhaite résoudre une inéquation du style f(x)<c
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()

        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.5 })
        stor.stockage = [0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()
      break // case "enonce":

    case 'correction':
      stor.fctsValid.validationGlobale()
      stor.mouvement = false
      // je définis la couleur de la correction
      // puis celle des commentaires (bons et mauvais)
      {
        const couleurCommentBon = me.styles.cbien
        const couleurCommentFaux = me.styles.cfaux
        let reponseEleve1 = $(stor.zoneInput).mathquill('latex')
        const reponseEleve1Init = reponseEleve1
        // me.cacheBoutonValider()
        stor.explications.innerHTML = ''
        if (reponseEleve1.includes(':')) {
          const partition = reponseEleve1.split('\\:')
          reponseEleve1 = ''
          for (let i = 0; i < partition.length; i++) {
            reponseEleve1 += partition[i]
          }
        }
        me.logIfDebug('valeur : ' + reponseEleve1 + '  bonneReponse:' + stor.stockage[1])
        // on souhaite accepte qu’un élève écrive +2 au lieu de 2'
        while (reponseEleve1.includes('[+')) {
          reponseEleve1 = reponseEleve1.replace('[+', '[')
        }
        while (reponseEleve1.indexOf(']+') > -1) {
          reponseEleve1 = reponseEleve1.replace(']+', ']')
        }
        while (reponseEleve1.indexOf(';+') > -1) {
          reponseEleve1 = reponseEleve1.replace(';+', ';')
        }
        me.logIfDebug('valeur après modif : ' + reponseEleve1 + '  bonneReponse:' + stor.stockage[1])
        // on teste si une réponse a été saisie
        if ((reponseEleve1 === '') && (!me.isElapsed)) {
          me.reponseManquante(stor.explications)
          // on réactive la zone de saisie
          $(stor.zoneInput).mathquill('write', '  ').focus()
        } else { // une réponse a été saisie
          let cbon = (reponseEleve1 === stor.stockage[1])
          if (!cbon) {
          // on a aussi la possibilité d’écrire les réunion dans le désordre'
            const tabRepEleve = reponseEleve1.split('\\cup')
            const tabReponse = stor.stockage[1].split('\\cup')
            if (tabRepEleve.length === tabReponse.length) {
              let presence1 = true
              for (let k = 0; k < tabRepEleve.length; k++) {
                presence1 = (presence1 && (tabReponse.includes(tabRepEleve[k])))
              }
              let presence2 = true
              for (let k = 0; k < tabReponse.length; k++) {
                presence2 = (presence2 && (tabRepEleve.includes(tabReponse[k])))
              }
              if (presence1 || presence2) cbon = true
            }
          }
          let testIntervalle, confusionInterUnion, erreurInegalite, erreurLargeStricte, erreurCrochets
          if (!cbon) {
          // je cherche les différents types d’erreur'
          // ce qui est écrit n’est pas un intervalle ou une réunion d’intervalles ou le séparateur de l’intervalle n’est asp ; mais ,'
            testIntervalle = bonIntervalle(reponseEleve1)
            // testIntervalle a alors deux propriétés : non_intervalle et separateur_virgule
            // confusion entre union et intersection :
            confusionInterUnion = false
            if (reponseEleve1.includes('cap')) {
            // on vérifie si la réponse est bonne en remplaçant \cap par \cup
              const newReponseEleve1 = reponseEleve1.split('cap').join('cup')
              confusionInterUnion = (newReponseEleve1 === stor.stockage[1])
              me.logIfDebug('newReponseEleve1:' + newReponseEleve1)
            }
            // pb sur le sens de l’inégalité'
            erreurInegalite = (reponseEleve1 === stor.stockage[2])
            // problème entre inégalité stricte et inégalite large
            erreurLargeStricte = (reponseEleve1 === stor.stockage[3])
            // problème de crochets autres que inégalité stricte/large
            erreurCrochets = false
            if (!erreurLargeStricte) {
            // tous les élémets de tabIntervalles doivent être présents
              erreurCrochets = true
              for (let i = 0; i < stor.stockage[4].length; i++) {
                erreurCrochets = (erreurCrochets && (reponseEleve1.indexOf(stor.stockage[4][i]) > -1))
              }
            }
            me.logIfDebug(
              'confusionInterUnion:' + confusionInterUnion,
              '\nerreurInegalite:' + erreurInegalite + '   ' + stor.stockage[2],
              '\nerreurLargeStricte:' + erreurLargeStricte + '   ' + stor.stockage[4],
              '\ntabIntervalles:' + stor.stockage[4],
              '\nerreurCrochets:' + erreurCrochets
            )
            stor.fctsValid.zones.bonneReponse[0] = false
          }
          stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
          const cr = stor.explications
          if (cbon) {
            me.score++
            cr.style.color = me.styles.cbien
            cr.innerHTML = cBien
            cr.innerHTML += '<br>' + ds.textes.comment15
            me.typederreurs[0]++
            j3pEmpty(stor.zonePalette)
            // on désactive la zone de saisie
            j3pDesactive(stor.zoneInput)
            // on affiche l’explication'
            afficheCorr(stor.stockage[0], couleurCommentBon)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            cr.style.color = couleurCommentFaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              cr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                            *   RECOPIER LA CORRECTION ICI !
                            */
              j3pDetruit(stor.commentDroite)
              j3pEmpty(stor.zonePalette)
              j3pDesactive(stor.zoneInput)
              j3pAffiche(stor.zoneConsigne3, '', '$' + stor.stockage[1] + '$', { style: { color: me.styles.toutpetit.correction.color } })
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
            // On affiche l’erreur de l’élève :
              const nouvelleRep = j3pAddElt(stor.zonesRep, 'div')
              j3pAffiche(nouvelleRep, '', ds.textes.comment_prop, { i: reponseEleve1Init })

              // on cherche si le type d’erreur est identifiable'
              cr.innerHTML = cFaux
              if (testIntervalle.non_intervalle) {
                cr.innerHTML += '<br>' + ds.textes.comment7
              } else if (testIntervalle.separateur_virgule) {
                cr.innerHTML += '<br>' + ds.textes.comment8
              } else if (confusionInterUnion) {
                me.typederreurs[4]++
                j3pAffiche(stor.explications, '', '\n' + ds.textes.comment10)
              } else {
                if (erreurInegalite) {
                  cr.innerHTML += '<br>' + ds.textes.comment11
                  me.typederreurs[1]++
                } else if (erreurLargeStricte) {
                  me.typederreurs[2]++
                  if (stor.stockage[5] === 1) {
                    cr.innerHTML += '<br>' + ds.textes.comment12
                  } else {
                    cr.innerHTML += '<br>' + ds.textes.comment13
                  }
                } else {
                  if (erreurCrochets) {
                    me.typederreurs[3]++
                    cr.innerHTML += '<br>' + ds.textes.comment14
                  } else {
                    me.typederreurs[5]++
                  }
                }
              }
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
              // on redonne le focus à la zone
                cr.innerHTML += '<br>' + essaieEncore
                // indication éventuelle ici
                me.etat = 'correction'
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                afficheCorr(stor.stockage[0], '#008800')
                // on désactive la zone de saisie
                j3pEmpty(stor.zonePalette)
                j3pAffiche(stor.zoneConsigne3, '', '$' + stor.stockage[1] + '$', { style: { color: me.styles.toutpetit.correction.color } })
                cr.innerHTML += '<br>' + regardeCorrection
                // on corrige ici
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          const nbErreurs = ds.nbitems - me.score
          if (me.typederreurs[1] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb sur le sens de l’inégalité est supé au égal au seul fixé
            me.parcours.pe = ds.pe_3
          } else if (me.typederreurs[2] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb sur les inégalités strictes ou larges est supé au égal au seul fixé
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[3] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de crochets est supé au égal au seul fixé
            me.parcours.pe = ds.pe_4
          } else if (me.typederreurs[4] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de confusion entre union et intersection est supé au égal au seul fixé
            me.parcours.pe = ds.pe_5
          } else {
            me.parcours.pe = ds.pe_6
          }
        }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }

      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
