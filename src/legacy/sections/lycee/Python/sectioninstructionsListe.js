import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pChaine, j3pDetruit, j3pElement, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pRandomTab, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { init, desactiverEditeur, getTextAce, runPython, runPythonCode, setTextAce } from 'src/legacy/outils/algo/algo_python'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juin 2019
        section sur les listes : instruction conditionnelle
 */

/* global ace */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['CasFigure', [1, 2, 3, 4], 'array', '4 exemples existent. On peut limiter ce choix à certains d’entre eux suivant les fonctionnalités de python utilisées&nbsp;:<br/>-1 : longueur de liste&nbsp;;<br/>-2  : ajout d’une valeur en fin de liste&nbsp;;<br/>-3 : compter le nombre d’occurences d’une valeur&nbsp;;<br/>-4 : ordonner une liste (croissant ou décroissant).'],
    ['btnAide', false, 'boolean', 'On peut avoir un bouton qui donne accès à l’ensemble des commandes utiles pour cet exercice.']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Instruction sur une liste',
  // on donne les phrases de la consigne
  consigne1: 'Dans la fenêtre ci-contre, le programme initial permet de construire une liste nommée £L de manière aléatoire.',
  consigne2_1: 'Compléter ce programme pour qu’il donne à la variable £{lg} la longueur de la liste puis qui affiche "liste trop courte" si celle-ci contient £n valeurs ou moins ou "liste assez longue" sinon.',
  consigne2_2: 'Compléter ce programme pour qu’il affiche le message "£n est dans la liste" si £n est bien dans £L ou ajoute £n en fin de liste sinon.',
  consigne2_3: 'Compléter ce programme pour qu’il donne à la variable £{nb} le nombre de fois où la valeur £n apparait dans la liste £L si £n est bien dans £L ou affiche le message "£n n’est pas dans la liste" sinon ',
  consigne2_4: 'Compléter ce programme pour qu’il range les valeurs de la liste £L dans l’ordre croissant si le premier terme est strictement plus petit que le deuxième et dans l’ordre décroissant sinon.',
  message1: 'liste trop courte',
  message3: 'liste assez longue',
  message2: '£n n’est pas dans la liste',
  message2Bis: '£n est dans la liste',
  utiliserConsole: 'Ne pas hésiter à utiliser la console pour tester le programme.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'L’algorithme doit être complété&nbsp;!',
  comment2: 'Le début du programme n’aurait pas dû être modifié&nbsp;!<br/>Reprendre la version initiale&nbsp;!',
  comment3: 'A la fin de chaque ligne contenant un mot clé, on devrait avoir deux points (par exemple ligne £l)&nbsp;!',
  comment4: "La commande <span class='code2'>£L.sort(reverse=True)</span> peut être remplacée par les deux lignes suivantes&nbsp;:<br/><span class='code2'>£L.sort()</span><br/><span class='code2'>£L.reverse()</span>",
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Voici dans la fenêtre ci-contre un programme répondant à la question.',
  correctionAlgo: 'Programme', // possible",
  pythonErreur: 'Ton programme a bogué pour',
  // pythonDiag1 : "Erreur ligne £l : le programme donne £r alors qu’on devrait avoir £{rep}.",
  pythonDiag1: 'Erreur avec cette liste générée au hasard&nbsp;:\n',
  pythonDiag2: 'Avec ton code, on obtient&nbsp;: ',
  pythonDiag3: 'alors qu’on devrait obtenir&nbsp;: ',
  sortie: 'Sortie',
  executer: 'Exécuter',
  reinitialiser: 'Réinitialiser',
  editeur: 'Editeur :',
  console: 'Console :',
  rappel: 'Rappels',
  rappel1: '<u>Voici la liste des commandes à connaitre sur les listes</u>',
  rappel2: "Dans ce qui suit la liste sera nommée <span class='code1'>maliste</span>",
  l1: "<li><span class='code2'>maliste=[10,20,30]</span>:affectation de valeurs (10,20,30) à la liste</li>",
  l2: "<li><span class='code2'>maliste.append(40)</span>:ajoute l’élément 40 à la fin de la liste</li>",
  l3: "<li><span class='code2'>maliste.pop(1)</span>:supprime l’élément d’indice 1 de la liste</li>",
  l4: "<li><span class='code2'>maliste.remove(30)</span>:supprime le premier élément de valeur 30 de la liste</li>",
  l5: "<li><span class='code2'>maliste.sort()</span>:range les valeurs de la liste dans l’ordre croissant</li>",
  l6: "<li><span class='code2'>maliste.reverse()</span>:inverse l’ordre des éléments de la liste</li>",
  l7: "<li><span class='code2'>len(maliste)</span>:donne le nombre d’éléments de la liste</li>",
  l8: "<li><span class='code2'>maliste.count(20)</span>:compte le nombre de fois où l’élément 20 apparait dans la liste</li>"
}

/**
 * section instructionsListe
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let laConsole
  // ---------------------------------------------------------------------------
  // Vérification de code
  // ---------------------------------------------------------------------------
  function verifDeuxPoints (algo) {
    // algo est l’algo au format python
    // on vérifie qu'à la fin de chaque ligne comportant un mot clé on ait les deux-points
    const motsCles = ['def', 'for', 'if', 'while', 'elif', 'else']
    const lignes = algo.split('\n')
    let deuxPointsPresents = true
    let numLigne = -1
    // console.log("algo:",algo)
    for (let i = lignes.length - 1; i >= 0; i--) {
      let txt = lignes[i].replace(/\t/g, '')
      for (let k = 0; k < motsCles.length; k++) {
        if (txt.indexOf(motsCles[k]) === 0) {
          // je vérifie qu'à la fin j’ai bien deux points (attention aux espaces)
          txt = txt.replace(/\s*/g, '')
          if (txt.charAt(txt.length - 1) !== ':') {
            deuxPointsPresents = false
            numLigne = i
          }
        }
      }
    }
    return { presents: deuxPointsPresents, numLigne: numLigne + 1 }
  }

  function execute () {
    runPython(stor.editor, laConsole)
  }

  function reinitialiser () {
    setTextAce(stor.editor, stor.algoInit)
  }

  function egaliteList (liste1, liste2) {
    // teste si deux listes
    let egalite = (typeof (liste1) === typeof (liste2))// on vérifie indirectement que ce sont deux objets du même type
    let k
    if (egalite) {
      if (typeof (liste1) === 'string') {
        egalite = (liste1 === liste2)
      } else if (!isNaN(Number(liste1))) {
        egalite = (Math.abs(liste1 - liste2) < Math.pow(10, -12))
      } else {
        egalite = (liste1.length === liste2.length)
        if (egalite) {
          for (k = 0; k < liste1.length; k++) {
            egalite = egaliteList(liste1[k], liste2[k])
          }
        }
      }
    }
    return egalite
  }

  function verifierCodePython (algoEleve, algoSecret, entrees, typeSortie) {
    me.logIfDebug('algoEleve=', algoEleve, '\nalgoSecret=', algoSecret, '\nentrees=', entrees)
    let lignes, ligne
    // on ajoute les entrees à tester
    let debutTest = ''
    let k, cle, valeur, i
    for (k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      debutTest = debutTest + cle + ' = ' + valeur + '\n'
    }
    me.logIfDebug('debutTest', debutTest, '\nalgoEleve avant', algoEleve)
    lignes = algoEleve.split('\n')
    let algoEleveTest = debutTest
    for (i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.includes('input(')) continue
      algoEleveTest = algoEleveTest + ligne + '\n'
    }
    me.logIfDebug('algoEleve apres', algoEleveTest, '\nalgo secret avant', algoSecret)
    lignes = algoSecret.split('\n')
    let algoSecretTest = debutTest
    for (i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.indexOf('input(') >= 0) continue
      algoSecretTest = algoSecretTest + ligne + '\n'
    }
    me.logIfDebug('algo secret apres', algoSecretTest)
    // on exécute algoSecretTest
    runPythonCode(algoSecretTest)
    let sortieSecret = j3pElement('output').innerHTML
    me.logIfDebug('sortieSecret=(', sortieSecret + ')')
    // on exécute algoEleveTest
    runPythonCode(algoEleveTest)
    let sortieEleve = j3pElement('output').innerHTML
    me.logIfDebug('sortieEleve=(', sortieEleve + ')')
    // on regarde si le test élève s’exécute
    let diagnostique
    if (sortieEleve.indexOf('Error') !== -1) {
      diagnostique = textes.pythonErreur
      for (k = 0; k < Object.keys(entrees).length; k++) {
        cle = Object.keys(entrees)[k]
        valeur = Object.values(entrees)[k]
        diagnostique = diagnostique + ' ' + cle + '=' + valeur
      }
      j3pElement('output').innerHTML = diagnostique + '\n' + j3pElement('output').innerHTML
      return false
    }
    // on compare les 2 exécutions
    sortieSecret = sortieSecret.replace(/\r\n/g, '')
    sortieEleve = sortieEleve.replace(/\r\n/g, '')
    let identique = true
    let posSortieSecret
    switch (typeSortie) {
      case 'number':
        identique = (Math.abs(Number(sortieSecret) - Number(sortieEleve)) < Math.pow(10, -12))
        break
      case 'string':
        // l’égalité de la sortie pose pb.
        // Je teste donc si à la fin j’ai bien la sortie attendue
        posSortieSecret = sortieEleve.toLowerCase().lastIndexOf(sortieSecret)
        identique = (posSortieSecret > -1)
        if (identique) {
          // on vérifie que c’est la dernière chose qui soit affichée
          identique = (sortieEleve.substring(posSortieSecret + sortieSecret.length).length === 0)
        }
        break
      case 'list':
        egaliteList(sortieEleve, sortieSecret)
        break
    }

    if (identique) {
      j3pElement('output').innerHTML = ''
      return true
    }
    // on explique l’échec
    diagnostique = textes.pythonDiag1
    for (k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      diagnostique = diagnostique + ' ' + cle + '=' + valeur
    }
    diagnostique = diagnostique + '\n' + textes.pythonDiag2 + sortieEleve
    diagnostique = diagnostique + '\n' + textes.pythonDiag3 + sortieSecret
    j3pElement('output').innerHTML = diagnostique
    return false
  }

  function creationEditeurPython () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    j3pAffiche(stor.zoneCons1, '', textes.editeur)
    stor.idEditor = j3pGetNewId('idEditor')
    stor.divEditor = j3pAddElt(stor.zoneCons2, 'div', '', { id: stor.idEditor })
    j3pStyle(stor.divEditor, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    stor.editor = ace.edit(stor.idEditor)
    stor.divEditor.style.height = '200px'
    stor.divEditor.style.width = '95%'
    stor.editor.resize()
    stor.editor.getSession().setUseSoftTabs(false)
    // stor.editor.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    stor.editor.getSession().setMode('ace/mode/python')
    stor.editor.setFontSize('12pt')
    stor.editor.focus()
    // et maintenant, place à la console
    j3pAffiche(stor.zoneCons3, '', textes.console)
    stor.idConsole = j3pGetNewId('idConsole')
    stor.divConsole = j3pAddElt(stor.zoneCons4, 'div', '', { id: stor.idConsole })
    j3pStyle(stor.divConsole, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    laConsole = ace.edit(stor.idConsole)
    stor.divConsole.style.height = '22px'
    stor.divConsole.style.width = '95%'
    stor.divConsole.style.marginTop = '2px'
    stor.divConsole.style.marginBottom = '2px'
    // laConsole.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    laConsole.getSession().setMode('ace/mode/python')
    laConsole.setFontSize('12pt')
    try {
      // J’ai eu un signalement où cet objet HTML n’existait pas ce qui faisait planter la section
      // Je ne vois pas comment ce peut être possible
      const tabAce = document.getElementsByClassName('ace_gutter')
      const ligne1Console = tabAce[1].childNodes[0].childNodes[0]
      ligne1Console.innerHTML = '>>>'
      ligne1Console.style.paddingLeft = '10px'
    } catch (e) {
      // Je vire la console car il y a un pb pour son affichage :
      j3pDetruit(stor.zoneCons3)
      j3pDetruit(stor.divConsole)
      console.warn('soucis avec l’élément HTML censé contenir les chevrons')
    }
    stor.divBtns = j3pAddElt(stor.zoneCons5, 'div')
    stor.divBtns.style.textAlign = 'right'
    stor.idExecuter = j3pGetNewId('executer')
    stor.idReinitialiser = j3pGetNewId('reinitialiser')
    j3pAjouteBouton(stor.divBtns, stor.idExecuter, 'MepBoutons', textes.executer, '')
    j3pAjouteBouton(stor.divBtns, stor.idReinitialiser, 'MepBoutons', textes.reinitialiser, '')
    j3pElement(stor.idExecuter).addEventListener('click', execute)
    // Et enfin la sortie de l’algo
    j3pAffiche(stor.zoneCons6, '', textes.utiliserConsole, { L: stor.nomListe })
    j3pStyle(stor.zoneCons6, { fontSize: '0.9em', fontStyle: 'italic' })
    const underline = j3pAddElt(stor.zoneCons7, 'u')
    j3pAddTxt(underline, textes.sortie)
    stor.output = 'output'
    stor.pre = j3pAddElt(stor.zoneCons7, 'pre', { id: stor.output })
    j3pStyle(stor.pre, { color: 'black' })
  }

  function afficheCorrection (bonneReponse) {
    const zoneExpli = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1)
    if (stor.quest === 4) {
      const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
      j3pAffiche(zoneExpli2, '', textes.comment4, { L: stor.nomListe })
    }
    try {
      // Détruit la fenêtre de rappels quand celle-ci existe
      j3pDetruitFenetres(stor.nameRappels)
    } catch (e) {
      console.warn(e) // pas normal mais pas grave
    }
    if (ds.btnAide) j3pDetruit(stor.rappels)
    stor.nameProg = j3pGetNewId('Programme')
    stor.fenetreCorr = j3pGetNewId('fenetreCorr')
    me.fenetresjq[0] = {
      name: stor.nameProg,
      title: textes.correctionAlgo,
      top: 110,
      left: 5,
      width: 450,
      id: stor.fenetreCorr
    }
    j3pCreeFenetres(me)
    const algoCorr = j3pAddElt(stor.fenetreCorr, 'div', '', { style: me.styles.etendre('petit.explications', { fontSize: '1.2em' }) })
    algoCorr.className = 'code1'
    activerClasseCode()
    const tabRep = stor.algoReponse.split('\n')
    const espaceTab = '&nbsp;&nbsp;&nbsp;&nbsp;'
    for (let i = 0; i < tabRep.length - 1; i++) {
      try {
        tabRep[i] = tabRep[i].replace(/\t/g, espaceTab)
        j3pAffiche(algoCorr, '', tabRep[i])
        j3pAddContent(algoCorr, '<br>')
      } catch (e) {
        console.warn(e) // pas normal mais pas grave
      }
    }
    j3pToggleFenetres(stor.nameProg)
    j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
    j3pDetruit(stor.idReinitialiser)
  }

  function nettoyageCorrection () {
    j3pDetruitFenetres(stor.nameProg)
  }

  function activerClasseCode () {
    // Cette fonction permet d’appliquer le style ci-dessous aux classes code1 et code2
    const code1 = $('.code1')
    const code2 = $('.code2')
    code1.css('font-family', 'Lucida Console')
    code1.css('font-size', '1em')
    code1.css('background-color', '#FFFFFF')
    code1.css('color', '#000000')
    code2.css('font-family', 'Lucida Console')
    code2.css('font-size', '0.8em')
    code2.css('background-color', '#FFFFFF')
    code2.css('color', '#000000')
  }

  function enonceInit () {
    me.validOnEnter = false // ex donneesSection.touche_entree
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    stor.tabQuest = ds.CasFigure
    stor.tabQuestInit = [...stor.tabQuest]

    init()
      .then(enonceMain)
      .catch(j3pShowError)
  } // enonceInit

  function enonceMain () {
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.quest = quest

    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    for (let i = 1; i <= 4; i++) stor['zoneConsD' + i] = j3pAddElt(stor.conteneurD, 'div')
    const nomListe = ['L', 'laliste', 'li']
    stor.nomListe = j3pRandomTab(nomListe, [0.334, 0.333, 0.333])

    // nb est le nombre maximum de valeurs de la liste (entre 30 et 40),
    // compt est le compteur (i, j ou k)
    const nb = (quest === 2)
      ? j3pGetRandomInt(15, 20)
      : (j3pGetRandomBool())
          ? j3pGetRandomInt(12, 17)
          : j3pGetRandomInt(30, 40)
    const compt = j3pRandomTab(['i', 'j', 'k'], [0.333, 0.333, 0.334])
    const min = j3pGetRandomInt(1, 2)
    const max = (quest === 2)
      ? (nb + 1) + j3pGetRandomInt(1, 2) * 2
      : 10 + j3pGetRandomInt(0, 2) * 2
    let seuil
    if (quest === 1) {
      seuil = 0.4 + j3pGetRandomInt(0, 2) * 0.1
      seuil = Math.round(seuil * 10) / 10// j'évite les pb éventuels sur les float
    }
    stor.algoInit = 'import random\n'
    stor.algoInit += stor.nomListe + '=[]\n'
    stor.algoInit += 'for ' + compt + ' in range(' + nb + '):\n'
    // Ici ça va dépendre du contexte
    if (quest === 1) {
      // Pour cet exemple, on a un premier test pour insérer les éléments (aléatoires de la liste)
      stor.algoInit += '\tif(random.random()<' + seuil + '):\n'
      stor.algoInit += '\t\t' + stor.nomListe + '.append(random.randint(' + min + ',' + max + '))\n'
    } else {
      stor.algoInit += '\t' + stor.nomListe + '.append(random.randint(' + min + ',' + max + '))\n'
    }
    const objVar = {}
    objVar.L = stor.nomListe
    if (quest === 1) {
      objVar.n = Math.round(nb / 2)
      objVar.n = (seuil === 0.4)
        ? objVar.n - j3pGetRandomInt(0, 2)
        : (seuil === 0.6)
            ? objVar.n + j3pGetRandomInt(0, 2)
            : objVar.n + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(0, 2)
      objVar.lg = j3pRandomTab(['lg', 'long'], [0.5, 0.5])
    } else if (quest <= 3) {
      objVar.n = j3pGetRandomInt(min + 1, max + 1)
      objVar.nb = j3pRandomTab(['n', 'nb'], [0.5, 0.5])
    }

    j3pAffiche(stor.zoneConsD1, '', textes.consigne1, objVar)
    j3pAffiche(stor.zoneConsD2, '', textes['consigne2_' + quest], objVar)
    const listeAlgoCorrInit = []
    const listeAlgoCorrFin = []
    listeAlgoCorrInit.push('import random\n')
    listeAlgoCorrInit.push(stor.nomListe + '=[]\n')
    listeAlgoCorrInit.push('for ' + compt + ' in range(' + nb + '):\n')
    if (quest === 1) {
      listeAlgoCorrInit.push('\tif(random.random()<' + seuil + '):\n')
      listeAlgoCorrInit.push('\t\t' + stor.nomListe + '.append(random.randint(' + min + ',' + max + '))\n')
    } else {
      listeAlgoCorrInit.push('\t' + stor.nomListe + '.append(random.randint(' + min + ',' + max + '))\n')
    }
    let dernieresLignes = ''
    if (quest === 1) {
      listeAlgoCorrFin.push(objVar.lg + '=len(' + stor.nomListe + ')\n')
      listeAlgoCorrFin.push('if ' + objVar.lg + '<=' + objVar.n + ':\n')
      listeAlgoCorrFin.push('\tprint("' + textes.message1 + '")\n')
      listeAlgoCorrFin.push('else :\n')
      listeAlgoCorrFin.push('\tprint("' + textes.message3 + '")\n')
      // dernieresLignes += "print "+objVar.lg
    } else if (quest === 2) {
      listeAlgoCorrFin.push('if ' + objVar.n + ' in ' + stor.nomListe + ':\n')
      listeAlgoCorrFin.push('\tprint("' + j3pChaine(textes.message2Bis, { n: objVar.n }) + '")\n')
      listeAlgoCorrFin.push('else :\n')
      listeAlgoCorrFin.push('\t' + stor.nomListe + '.append(' + objVar.n + ')\n')
      dernieresLignes += 'if ' + objVar.n + ' not in ' + stor.nomListe + ':\n'
      dernieresLignes += '\tprint "' + stor.nomListe + '="+' + stor.nomListe
    } else if (quest === 3) {
      listeAlgoCorrFin.push('if ' + objVar.n + ' in ' + stor.nomListe + ':\n')
      listeAlgoCorrFin.push('\t' + objVar.nb + '=' + stor.nomListe + '.count(' + objVar.n + ')\n')
      listeAlgoCorrFin.push('else :\n')
      listeAlgoCorrFin.push('\tprint("' + j3pChaine(textes.message2, { n: objVar.n }) + '")\n')
      dernieresLignes += 'if ' + objVar.n + ' in ' + stor.nomListe + ':\n'
      dernieresLignes += '\tprint "' + objVar.nb + '=",' + objVar.nb
    } else if (quest === 4) {
      listeAlgoCorrFin.push('if ' + stor.nomListe + '[0] < ' + stor.nomListe + '[1]:\n')
      listeAlgoCorrFin.push('\t' + stor.nomListe + '.sort()\n')
      listeAlgoCorrFin.push('else :\n')
      listeAlgoCorrFin.push('\t' + stor.nomListe + '.sort(reverse=True)\n')
      dernieresLignes += 'print ' + stor.nomListe
    }
    stor.algoReponse = ''
    for (let i = 0; i < listeAlgoCorrInit.length; i++) {
      stor.algoReponse += listeAlgoCorrInit[i]
    }
    stor.algoPartie1 = stor.algoReponse
    for (let i = 0; i < listeAlgoCorrFin.length; i++) {
      stor.algoReponse += listeAlgoCorrFin[i]
    }
    // stor.algoReponse+=dernieresLignes;
    me.logIfDebug('algoReponse:', stor.algoReponse)
    creationEditeurPython()
    setTextAce(stor.editor, stor.algoInit)
    const consoleInit = ''
    setTextAce(laConsole, consoleInit)
    stor.objVar = objVar
    j3pElement(stor.idReinitialiser).addEventListener('click', reinitialiser)

    stor.algoPartie1Comp = stor.algoPartie1 + 'print ' + stor.nomListe
    const listesTest = []
    for (let i = 1; i <= 15; i++) {
      runPythonCode(stor.algoPartie1Comp)
      listesTest.push(j3pElement('output').innerHTML)
    }
    j3pElement('output').innerHTML = ''
    stor.listesTest = listesTest
    stor.algoCorr = ''
    for (let i = 0; i < listeAlgoCorrFin.length; i++) {
      stor.algoCorr += listeAlgoCorrFin[i]
    }
    stor.algoCorr += dernieresLignes

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    me.fenetresjq = []
    // Ajout du bouton Rappels
    if (ds.btnAide) {
      stor.rappels = j3pAddElt(stor.conteneurD, 'div')
      const largeur = Number(j3pElement('Mepact').style.width.split('px')[0]) * 3 / 5
      const hauteur = me.zonesElts.HG.getBoundingClientRect().height + 10
      stor.nameRappels = j3pGetNewId('Rappels')
      stor.fenRappels = j3pGetNewId('fenetreRappels')
      me.fenetresjq.push({
        name: stor.nameRappels,
        title: textes.rappel,
        left: largeur / 3,
        top: hauteur,
        width: largeur,
        id: stor.fenRappels
      })
      j3pCreeFenetres(me)
      j3pAjouteBouton(stor.rappels, textes.rappel, 'MepBoutons', textes.rappel, j3pToggleFenetres.bind(null, stor.nameRappels))
      const mesRappels = j3pAddElt(stor.fenRappels, 'div', '', { style: me.styles.toutpetit.enonce })
      for (let i = 1; i <= 8; i++) {
        j3pAffiche(mesRappels, '', textes['l' + i])
        j3pAddContent(mesRappels, '<br>')
      }
      activerClasseCode()
      j3pAfficheCroixFenetres(stor.nameRappels)
    }

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvent dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        try {
          j3pDetruit(stor.fenetreCorr)
        } catch (e) {
          console.warn(e) // pas normal mais pas grave
        }
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const reponse = { aRepondu: false, bonneReponse: true }
      let debutModifie = false
      stor.algoEleveInit = getTextAce(stor.editor)
      const aucunChgt = (stor.algoInit === stor.algoEleveInit)
      reponse.aRepondu = !aucunChgt
      if (reponse.aRepondu) {
        // Je vérifie tout de même que l’élève n’a pas modifié les premieres lignes du programme
        debutModifie = (stor.algoEleveInit.indexOf(stor.algoInit) !== 0)
        reponse.aRepondu = !debutModifie
      }
      let k, objDeuxPts
      if (reponse.aRepondu) {
        objDeuxPts = verifDeuxPoints(stor.algoEleveInit)
        me.logIfDebug('objDeuxPts:', objDeuxPts)
        let algoTest
        if (objDeuxPts.presents) {
          algoTest = stor.algoEleveInit
          reponse.bonneReponse = true
          for (let i = 0; i < stor.listesTest.length; i++) {
            algoTest = stor.algoEleveInit.replace(stor.algoInit, 'import random\n' + stor.nomListe + '=' + stor.listesTest[i])
            let algoCorr = stor.algoCorr
            algoCorr = 'import random\n' + stor.nomListe + '=' + stor.listesTest[i] + algoCorr
            const tabLigne = algoTest.split('\n')
            for (k = tabLigne.length - 1; k >= 0; k--) {
              if (tabLigne[k] === '') {
                tabLigne.pop()
              } else if (tabLigne[k].indexOf('print') === 0) {
                tabLigne.pop()
              } else {
                break
              }
            }
            algoTest = tabLigne.join('\n')
            const objListe = {}
            if (stor.quest === 2) {
              algoTest += '\nif ' + stor.objVar.n + ' not in ' + stor.nomListe + ':\n'
              algoTest += '\tprint "' + stor.nomListe + '="+' + stor.nomListe
            } else if (stor.quest === 3) {
              if ((algoTest.indexOf(stor.objVar.nb + '=') !== -1) || (algoTest.indexOf(stor.objVar.nb + ' =') !== -1)) {
                algoTest += '\nif ' + stor.objVar.n + ' in ' + stor.nomListe + ':\n'
                algoTest += '\tprint "' + stor.objVar.nb + '=",' + stor.objVar.nb
              }
            } else if (stor.quest === 4) {
              algoTest += '\nprint ' + stor.nomListe
            }
            objListe[stor.nomListe] = stor.listesTest[i]
            reponse.bonneReponse = (reponse.bonneReponse && verifierCodePython(algoTest, algoCorr, objListe, 'string'))
          }
        } else {
          reponse.bonneReponse = false
        }
      }

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.zoneCorr.style.color = me.styles.cfaux
        stor.zoneCorr.innerHTML = (aucunChgt)
          ? textes.comment1
          : textes.comment2
        return me.finCorrection()
      }

      // Une réponse a été saisie
      if (reponse.bonneReponse) {
        // Bonne réponse
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        // afficheCorrection(true);
        desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
        j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
        j3pDetruit(stor.idReinitialiser)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      if (!objDeuxPts.presents) {
        stor.zoneCorr.innerHTML += '<br>' + j3pChaine(textes.comment3, { l: objDeuxPts.numLigne })
      }
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // indication éventuelle ici
        // ici il a encore la possibilité de se corriger
        return me.finCorrection()
      }

      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(false)
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        me.buttonsElts.sectionSuivante?.addEventListener('click', nettoyageCorrection)
        j3pElement(stor.idExecuter).removeEventListener('click', execute)
      } else {
        me.buttonsElts.suite?.addEventListener('click', nettoyageCorrection)
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
