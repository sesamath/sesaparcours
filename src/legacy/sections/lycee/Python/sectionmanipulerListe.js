import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pChaine, j3pDetruit, j3pElement, j3pGetNewId, j3pGetRandomInt, j3pRandomTab, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { init, desactiverEditeur, getTextAce, runPython, runPythonCode, setTextAce } from 'src/legacy/outils/algo/algo_python'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitToutesLesFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mai 2019
        section sur les listes : création et manipulation sur des commandes de base
 */

/* global ace */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']//,
    // ["aideConsole",true,"boolean","Lorsque ce paramètre vaut true, la console est préremplie avec une utilisation de la fonction. Sinon c’est à l’élève d’écrire un test possible."]
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Manipuler une liste',
  // on donne les phrases de la consigne
  consigne1: 'Dans la fenêtre ci-contre, on souhaite écrire dans le langage python un programme de <b>3 lignes exactement</b> qui effectue les traitements successifs suivants&nbsp;:',
  consigne2_1: '- création d’une liste nommée £L contenant les nombres £n dans cet ordre£{pt}',
  consigne2_2: '- ajout de la valeur £e à la fin de la liste £L£{pt}',
  consigne2_3: '- suppression de la valeur d’indice £i de £L£{pt}',
  consigne2_4: '- remplacement de la valeur d’indice £j par la valeur £v£{pt}',
  consigne2_5: '- création de la variable £{lg} égale à la longueur de la liste £L£{pt}',
  consigne2_6: '- insertion de la valeur £w à l’indice £k de la liste £L£{pt}',
  consigne2_7: '- rangement des valeurs de la liste £L dans l’ordre croissant£{pt}',
  consigne3: 'Ne pas hésiter à utiliser la console pour vérifier le contenu de la liste £L.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'L’algorithme doit être complété&nbsp;!',
  comment2: '3 lignes exactement auraient dues être complétées&nbsp;!',
  comment3: 'La première ligne doit permettre de définir la liste £L&nbsp;!',
  comment4: "On ne doit pas affecter plusieurs fois une valeur à £L&nbsp;!<br/>£L=... ne doit apparaître qu'à la première ligne.",
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Voici dans la fenêtre ci-contre un programme répondant à la question.',
  corr2: 'Pour supprimer la valeur d’indice £i de £L, il est aussi possible d’écrire&nbsp;:<br/>£L.pop(£i)',
  correctionAlgo: 'Programme possible',
  pythonErreur: 'Ton programme a bogué pour',
  pythonDiag1: 'Erreur ligne £l : le programme donne £r alors qu’on devrait avoir £{rep}.',
  pythonDiag2: 'Avec ton code, on obtient&nbsp;: ',
  pythonDiag3: 'alors qu’on devrait obtenir&nbsp;: ',
  sortie: 'Sortie',
  executer: 'Exécuter',
  reinitialiser: 'Réinitialiser',
  editeur: 'Editeur :',
  console: 'Console :'
}

/**
 * section manipulerListe
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let laConsole
  // ---------------------------------------------------------------------------
  // Vérification de code
  // ---------------------------------------------------------------------------
  function execute () {
    runPython(stor.editor, laConsole)
  }

  function reinitialiser () {
    setTextAce(stor.editor, stor.algoInit)
  }

  function egaliteList (liste1, liste2) {
    // teste si deux listes sont identiques
    let egalite = (typeof (liste1) === typeof (liste2))// on vérifie indirectement que ce sont deux objets du même type
    if (egalite) {
      if (typeof (liste1) === 'string') {
        egalite = (liste1 === liste2)
      } else if (!isNaN(Number(liste1))) {
        egalite = (Math.abs(liste1 - liste2) < Math.pow(10, -12))
      } else {
        egalite = (liste1.length === liste2.length)
        if (egalite) {
          for (let k = 0; k < liste1.length; k++) {
            egalite = egaliteList(liste1[k], liste2[k])
            if (!egalite) return { egalite: false, ligne: k }
          }
        }
      }
    }
    return { egalite }
  }

  function verifierCodePython (algoEleve, algoSecret, entrees, typeSortie) {
    me.logIfDebug('algoEleve=', algoEleve, '\nalgoSecret=', algoSecret, '\nentrees=', entrees)
    let lignes, ligne
    // on ajoute les entrees à tester
    let debutTest = ''
    let k, cle, valeur, i
    for (k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      debutTest = debutTest + cle + ' = ' + valeur + '\n'
    }
    me.logIfDebug('debutTest', debutTest, '\nalgoEleve avant', algoEleve)
    lignes = algoEleve.split('\n')
    let algoEleveTest = debutTest
    for (i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.includes('input(')) continue
      algoEleveTest = algoEleveTest + ligne + '\n'
    }
    me.logIfDebug('algoEleve apres', algoEleveTest, '\nalgo secret avant', algoSecret)
    lignes = algoSecret.split('\n')
    let algoSecretTest = debutTest
    for (i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.indexOf('input(') >= 0) continue
      algoSecretTest = algoSecretTest + ligne + '\n'
    }
    me.logIfDebug('algo secret apres', algoSecretTest)
    // on exécute algoSecretTest
    runPythonCode(algoSecretTest)
    let sortieSecret = j3pElement('output').innerHTML
    me.logIfDebug('sortieSecret=(', sortieSecret + ')')
    // on exécute algoEleveTest
    runPythonCode(algoEleveTest)
    let sortieEleve = j3pElement('output').innerHTML
    me.logIfDebug('sortieEleve=(', sortieEleve + ')')
    // on regarde si le test élève s’exécute
    let diagnostique
    if (sortieEleve.includes('Error')) {
      diagnostique = textes.pythonErreur
      for (k = 0; k < Object.keys(entrees).length; k++) {
        cle = Object.keys(entrees)[k]
        valeur = Object.values(entrees)[k]
        diagnostique = diagnostique + ' ' + cle + '=' + valeur
      }
      j3pElement('output').innerHTML = diagnostique + '\n' + j3pElement('output').innerHTML
      return false
    }
    // on compare les 2 exécutions
    sortieSecret = sortieSecret.split('\n').join('')
    sortieSecret = sortieSecret.split('<br>').join('')
    sortieSecret.replace(/\r\n/g, '')
    sortieEleve = sortieEleve.split('\n').join('')
    sortieEleve = sortieEleve.split('<br>').join('')
    sortieEleve.replace(/\r\n/g, '')
    let identique = true
    switch (typeSortie) {
      case 'number':
        identique = (Math.abs(Number(sortieSecret) - Number(sortieEleve)) < Math.pow(10, -12))
        break
      case 'string':
        identique = (sortieSecret === sortieEleve)
        break
      case 'list':
        identique = egaliteList(sortieEleve, sortieSecret)
        break
    }

    if (identique) {
      j3pElement('output').innerHTML = ''
      return true
    }
    // on explique l’échec
    diagnostique = textes.pythonDiag2 + sortieEleve
    diagnostique = diagnostique + '\n' + textes.pythonDiag3 + sortieSecret
    j3pElement('output').innerHTML = diagnostique
    return false
  }

  function creationEditeurPython () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    j3pAffiche(stor.zoneCons1, '', textes.editeur)
    stor.idEditor = j3pGetNewId('idEditor')
    stor.divEditor = j3pAddElt(stor.zoneCons2, 'div', '', { id: stor.idEditor })
    j3pStyle(stor.divEditor, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    stor.editor = ace.edit(stor.idEditor)
    stor.divEditor.style.height = '200px'
    stor.divEditor.style.width = '95%'
    stor.editor.resize()
    stor.editor.getSession().setUseSoftTabs(false)
    // stor.editor.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    stor.editor.getSession().setMode('ace/mode/python')
    stor.editor.setFontSize('12pt')
    stor.editor.focus()
    // et maintenant, place à la console
    j3pAffiche(stor.zoneCons3, '', textes.console)
    stor.idConsole = j3pGetNewId('idConsole')
    stor.divConsole = j3pAddElt(stor.zoneCons4, 'div', '', { id: stor.idConsole })
    j3pStyle(stor.divConsole, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    laConsole = ace.edit(stor.idConsole)
    stor.divConsole.style.height = '40px'
    stor.divConsole.style.width = '95%'
    stor.divConsole.style.marginTop = '2px'
    stor.divConsole.style.marginBottom = '2px'
    // laConsole.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    laConsole.getSession().setMode('ace/mode/python')
    laConsole.setFontSize('12pt')
    try {
      // J’ai eu un signalement où cet objet HTML n’existait pas ce qui faisait planter la section
      // Je ne vois pas comment ce peut être possible
      const tabAce = document.getElementsByClassName('ace_gutter')
      const ligne1Console = tabAce[1].childNodes[0].childNodes[0]
      ligne1Console.innerHTML = '>>>'
      ligne1Console.style.paddingLeft = '10px'
    } catch (e) {
      // Je vire la console car il y a un pb pour son affichage :
      j3pDetruit(stor.zoneCons3)
      j3pDetruit(stor.divConsole)
      console.warn('soucis avec l’élément HTML censé contenir les chevrons')
    }
    stor.divBtns = j3pAddElt(stor.zoneCons5, 'div')
    stor.divBtns.style.textAlign = 'right'
    stor.idExecuter = j3pGetNewId('executer')
    stor.idReinitialiser = j3pGetNewId('reinitialiser')
    j3pAjouteBouton(stor.divBtns, stor.idExecuter, 'MepBoutons', textes.executer, '')
    j3pAjouteBouton(stor.divBtns, stor.idReinitialiser, 'MepBoutons', textes.reinitialiser, '')
    j3pElement(stor.idExecuter).addEventListener('click', execute)
    // Et enfin la sortie de l’algo
    j3pAffiche(stor.zoneCons6, '', textes.consigne3, { L: stor.nomListe })
    j3pStyle(stor.zoneCons6, { fontSize: '0.9em', fontStyle: 'italic' })
    const underline = j3pAddElt(stor.zoneCons7, 'u')
    j3pAddTxt(underline, textes.sortie)
    stor.output = 'output'
    stor.pre = j3pAddElt(stor.zoneCons7, 'pre', { id: stor.output })
    j3pStyle(stor.pre, { color: 'black' })
  }

  function afficheCorrection (bonneReponse) {
    const zoneExpli = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1)
    stor.nameProg = j3pGetNewId('Programme')
    stor.fenetreCorr = j3pGetNewId('fenetreCorr')
    me.fenetresjq = [{
      name: stor.nameProg,
      title: textes.correctionAlgo,
      top: 110,
      left: 5,
      width: 450,
      id: stor.fenetreCorr
    }]
    j3pCreeFenetres(me)
    const algoCorr = j3pAddElt(stor.fenetreCorr, 'div', '', { style: me.styles.etendre('petit.explications', { fontSize: '1.2em' }) })
    algoCorr.className = 'code1'
    const tabRep = stor.algoCorr.split('\n')
    const espaceTab = '&nbsp;&nbsp;&nbsp;&nbsp;'
    for (let i = 0; i < tabRep.length - 1; i++) {
      try {
        tabRep[i] = tabRep[i].replace(/\t/g, espaceTab)
        j3pAffiche(algoCorr, '', tabRep[i])
        j3pAddContent(algoCorr, '<br>')
      } catch (e) {
        console.warn(e) // pas normal mais pas grave
      }
    }
    j3pToggleFenetres(stor.nameProg)
    j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
    j3pDetruit(stor.idReinitialiser)
    if (tabRep.join('!').includes('del(')) {
      // J’ajoute le fait que pop fonctionne aussi pour supprimer un élément de la liste
      const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
      j3pAffiche(zoneExpli2, '', textes.corr2, stor.objModif)
    }
    const code1 = $('.code1')
    code1.css('font-family', 'Lucida Console')
    code1.css('font-size', '1em')
    code1.css('background-color', '#FFFFFF')
    code1.css('color', '#000000')
  }

  function enonceInit () {
    me.validOnEnter = false // ex donneesSection.touche_entree
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    init()
      .then(enonceMain)
      .catch(j3pShowError)
  }

  function enonceMain () {
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    for (let i = 1; i <= 4; i++) stor['zoneConsD' + i] = j3pAddElt(stor.conteneurD, 'div')
    j3pAffiche(stor.zoneConsD1, '', textes.consigne1)
    const nomListe = ['L', 'List', 'Li']
    stor.nomListe = j3pRandomTab(nomListe, [0.334, 0.333, 0.333])
    const listeDonnees = []
    let newDonnees = j3pGetRandomInt(1, 30)
    const nbDonnees = j3pGetRandomInt(4, 5)
    const listeCons = ['consigne2_2', 'consigne2_3', 'consigne2_4', 'consigne2_5', 'consigne2_6', 'consigne2_7']
    const tabRep = []
    tabRep[0] = '£L=[' + newDonnees
    listeDonnees.push(newDonnees)
    let texteListeDonnees = String(newDonnees)
    let valOrdonnees = true
    let pbValOrdonnees = false
    for (let i = 1; i < nbDonnees; i++) {
      do {
        newDonnees = j3pGetRandomInt(1, 30)
        valOrdonnees = (valOrdonnees && (newDonnees > listeDonnees[listeDonnees.length - 1]))
        if (i === nbDonnees - 1) {
          // Si les valeurs sont ordonnées jusqu'à présent, je fais en sorte que ça ne soit plus le cas avec la dernière valeur
          pbValOrdonnees = valOrdonnees
        }
      } while ((listeDonnees.indexOf(newDonnees) > 0) || pbValOrdonnees)
      listeDonnees.push(newDonnees)
      texteListeDonnees += '&nbsp;;&nbsp;' + String(newDonnees)
      tabRep[0] += ',' + newDonnees
    }
    tabRep[0] += ']'
    j3pAffiche(stor.zoneConsD2, '', textes.consigne2_1, {
      L: stor.nomListe,
      n: texteListeDonnees,
      pt: '&nbsp;;'
    })
    const tabCons = []
    let num = j3pGetRandomInt(0, (listeCons.length - 1))
    tabCons.push(listeCons[num])
    listeCons.splice(num, 1)
    num = j3pGetRandomInt(0, (listeCons.length - 1))
    tabCons.push(listeCons[num])
    const objModif = { L: stor.nomListe, listeInit: listeDonnees }
    const ListeInterm = [...listeDonnees]
    for (let i = 1; i <= 2; i++) {
      switch (tabCons[i - 1]) {
        case 'consigne2_2':
          // consigne2_2 : "- ajout de l’élément £e à la fin de la liste £L£{pt}",
          do {
            objModif.e = j3pGetRandomInt(1, 30)
          } while (ListeInterm.indexOf(objModif.e) > -1)
          tabRep.push('£L.append(£e)')
          break
        case 'consigne2_3':
          // consigne2_3 : "- suppression de l’élément d’indice £i de £L£{pt}",
          objModif.i = j3pGetRandomInt(1, (ListeInterm.length - 2))
          tabRep.push('del(£L[£i])')
          break
        case 'consigne2_4':
          // consigne2_4 : "- remplacement de l’élément d’indice £j par la valeur £v£{pt}",
          objModif.j = j3pGetRandomInt(1, (ListeInterm.length - 2))
          do {
            objModif.v = j3pGetRandomInt(1, 30)
          } while (ListeInterm.indexOf(objModif.v) > -1)
          tabRep.push('£L[£j]=£v')
          break
        case 'consigne2_5':
          // consigne2_5 : "- création de la variable £{lg} égale à la longueur de la liste £L£{pt}",
          objModif.lg = j3pRandomTab(['lg', 'long', 'longueur'], [0.333, 0.334, 0.333])
          tabRep.push('£{lg}=len(£L)')
          break
        case 'consigne2_6':
          // consigne2_6 : "- insertion de la valeur £w à l’indice £k de la liste £L£{pt}",
          objModif.k = j3pGetRandomInt(1, (ListeInterm.length - 2))
          do {
            objModif.w = j3pGetRandomInt(1, 30)
          } while (ListeInterm.indexOf(objModif.w) > -1)
          tabRep.push('£L.insert(£k,£w)')
          break
        case 'consigne2_7':
          // consigne2_7 : "- rangement des valeurs de la liste £L dans l’ordre croissant"
          tabRep.push('£L.sort()')
          break
      }
    }
    // console.log("tabRep:",tabRep)
    for (let i = 0; i < tabRep.length; i++) tabRep[i] = j3pChaine(tabRep[i], objModif)
    objModif.pt = '&nbsp;;'
    j3pAffiche(stor.zoneConsD3, '', textes[tabCons[0]], objModif)
    objModif.pt = '.'
    j3pAffiche(stor.zoneConsD4, '', textes[tabCons[1]], objModif)
    stor.algoReponse = 'maListeReponse=[]\n'
    stor.algoCorr = ''
    const lignesAlgo = []
    stor.lignesAjoutees = []
    for (let i = 0; i < tabRep.length; i++) {
      lignesAlgo.push(j3pChaine(tabRep[i], objModif))
      stor.algoReponse += lignesAlgo[i] + '\n'
      stor.algoCorr += lignesAlgo[i] + '\n'
      if (tabRep[i].indexOf('£{lg}') === 0) {
        // c’est que c’est la ligne où on demande la longueur
        stor.lignesAjoutees.push('maListeReponse.append(' + objModif.lg + ')')
      } else {
        stor.lignesAjoutees.push('maListeReponse.append(' + objModif.L + ')')
      }
      stor.algoReponse += stor.lignesAjoutees[i] + '\n'
    }
    stor.algoReponse += 'print(maListeReponse)'
    me.logIfDebug('algoReponse:', stor.algoReponse)
    creationEditeurPython()
    stor.algoInit = ''
    setTextAce(stor.editor, stor.algoInit)
    const consoleInit = ''
    setTextAce(laConsole, consoleInit)
    stor.objModif = objModif
    j3pElement(stor.idReinitialiser).addEventListener('click', reinitialiser)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvent dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    j3pStyle(stor.zoneCorr, { paddingTop: '20px' })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const reponse = { aRepondu: false, bonneReponse: true }
      let doubleAffectation = false
      stor.algoEleveInit = getTextAce(stor.editor)
      reponse.aRepondu = (stor.algoInit !== stor.algoEleveInit)
      let nbLignesOK, bonneDefinition
      if (reponse.aRepondu) {
        // console.log("stor.algoEleveInit:",stor.algoEleveInit)
        let tabLignesInit = stor.algoEleveInit.split('\n').join('@')
        tabLignesInit = tabLignesInit.replace(/@@/g, '@')
        const tabLignes = tabLignesInit.split('@')
        while (tabLignes[tabLignes.length - 1] === '') {
          tabLignes.pop()
        }
        while (tabLignes[0] === '') {
          tabLignes.shift()
        }
        // A cet instant, j’ai enlevé toutes les lignes inutiles
        // JE vérifie tout de même que l’élève n’a pas feinté en affectant à plusieurs reprises la valeur de la liste plutôt que d’utliser une méthode
        let nbAffect = 0
        for (let i = 0; i < tabLignes.length; i++) {
          if (tabLignes[i].includes(stor.objModif.L + '=')) {
            nbAffect++
          }
        }
        if (nbAffect > 1) {
          doubleAffectation = true
          reponse.aRepondu = false
        }
        if (reponse.aRepondu) {
          nbLignesOK = (tabLignes.length === 3)
          if (nbLignesOK) {
            // Je vérifie si sur la première ligne on a bien la définition de la liste :
            tabLignes[0] = tabLignes[0].replace(/\s/g, '')
            bonneDefinition = (tabLignes[0].indexOf(stor.nomListe + '=') === 0)
            if (bonneDefinition) {
              // A la fin de chaque ligne, je récupère le contenu de la liste
              // Attention quand on demande une autre variable comme la longueur
              let newRepEleve = 'maListeReponse=[]\n'
              for (let i = 0; i < tabLignes.length; i++) {
                newRepEleve += tabLignes[i] + '\n' + stor.lignesAjoutees[i] + '\n'
              }
              newRepEleve += 'print(maListeReponse)'
              reponse.bonneReponse = verifierCodePython(newRepEleve, stor.algoReponse, {}, 'string')
            } else {
              reponse.bonneReponse = false
            }
          } else {
            reponse.bonneReponse = false
          }
        }
      }

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.zoneCorr.style.color = me.styles.cfaux
        j3pAddContent(stor.zoneCorr, (doubleAffectation)
          ? j3pChaine(textes.comment4, stor.objModif)
          : textes.comment1, { replace: true })
        return me.finCorrection()
      }

      // Une réponse a été saisie
      if (reponse.bonneReponse) {
        // Bonne réponse
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        j3pAddContent(stor.zoneCorr, cBien, { replace: true })
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        // afficheCorrection(true);
        desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
        j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
        j3pDetruit(stor.idReinitialiser)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        j3pAddContent(stor.zoneCorr, tempsDepasse, { replace: true })
        me.typederreurs[10]++
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      j3pAddContent(stor.zoneCorr, cFaux, { replace: true })
      if (nbLignesOK) {
        if (!bonneDefinition) {
          j3pAddContent(stor.zoneCorr, '<br>' + j3pChaine(textes.comment3, stor.objModif))
        }
      } else {
        // c’est que les trois lignes ne sont pas présentes
        j3pAddContent(stor.zoneCorr, '<br>' + textes.comment2)
      }
      if (me.essaiCourant < ds.nbchances) {
        j3pAddContent(stor.zoneCorr, '<br>' + essaieEncore)
        this.typederreurs[1]++
        // indication éventuelle ici
        // ici il a encore la possibilité de se corriger
        return me.finCorrection()
      }

      // Erreur au dernier essai
      j3pAddContent(stor.zoneCorr, '<br>' + regardeCorrection)
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(false)
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        me.buttonsElts.sectionSuivante?.addEventListener('click', j3pDetruitToutesLesFenetres)
        j3pElement(stor.idExecuter).removeEventListener('click', execute)
      } else {
        // faudrait pas ajouter un listener à chaque fois, mais on sait pas trop si le modèle a détruit/recréé le bouton ou pas…
        me.buttonsElts.suite?.addEventListener('click', j3pDetruitToutesLesFenetres)
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
