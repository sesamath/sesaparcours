import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pChaine, j3pDetruit, j3pElement, j3pGetNewId, j3pGetRandomInt, j3pRandomTab, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { init, desactiverEditeur, getTextAce, runPython, runPythonCode, setTextAce, verifDeuxPoints } from 'src/legacy/outils/algo/algo_python'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juin 2019
        section sur les listes : créeation d’une liste avec la boucle Pour
 */
/* global ace */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['aideFonction', false, 'boolean', 'Lorsque ce paramètre vaut true, la définition de la fonction et le mot clé "return" sont données.']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Liste à l’aide d’une boucle Pour',
  // on donne les phrases de la consigne
  consigne1_1: 'Créer ci-contre la fonction <b>sans argument</b> nommée <b>puis£b</b> qui affiche les puissances de £b allant de 1 $(£b^0)$ à £n $(£b^{£p})$.',
  consigneBis1_1: 'Compléter ci-contre la fonction nommée <b>puis£b</b> qui affiche les puissances de £b allant de 1 $(£b^0)$ à £n $(£b^{£p})$.',
  consigne1_2: 'Créer ci-contre la fonction <b>sans argument</b> nommée <b>mult</b> qui affiche les multiples de £k allant de 0 $(£k\\times 0)$ à £n $(£k\\times £p)$.',
  consigneBis1_2: 'Compléter ci-contre la fonction nommée <b>mult</b> qui affiche les multiples de £k allant de 0 $(£k\\times 0)$ à £n $(£k\\times £p)$.',
  consigne1_3: 'Créer ci-contre la fonction <b>sans argument</b> nommée <b>£{nomf}</b> qui affiche les £{typef} des entiers positifs allant de £{m1} $(£{n1}^{£k})$ à £{m2} $(£{n2}^{£k})$.',
  consigneBis1_3: 'Compléter ci-contre la fonction nommée <b>£{nomf}</b> qui affiche les £{typef} des entiers positifs allant de £{m1} $(£{n1}^{£k})$ à £{m2} $(£{n2}^{£k})$.',
  consigne1_4: '$£f$ étant la fonction définie sur $\\R$ par $£f(x)=£{a}x^2-£{b}$, créer ci-contre la fonction <b>sans argument</b> nommée <b>£{nomf}</b> qui affiche les images par la fonction $£f$ de tous les entiers compris entre £{n1} et £{n2} (inclus).',
  consigneBis1_4: '$£f$ étant la fonction définie sur $\\R$ par $£f(x)=£{a}x^2-£{b}$, compléter ci-contre la fonction nommée <b>£{nomf}</b> qui affiche les images par la fonction $£f$ de tous les entiers compris entre £{n1} et £{n2} (inclus).',
  consigne2: 'Seules les valeurs demandées doivent être affichées, aucune autre.',
  utiliserConsole: 'Ne pas hésiter à utiliser la console pour tester le programme.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'L’algorithme doit être complété&nbsp;!',
  comment2: 'Le début du programme n’aurait pas dû être modifié&nbsp;!<br/>Reprendre la version initiale&nbsp;!',
  comment3: 'A la fin de chaque ligne contenant un mot clé, on devrait avoir deux points (par exemple ligne £l)&nbsp;!',
  comment4: 'Le programme doit commencer par la création d’une fonction (à l’aide du mot clé "def")&nbsp;!',
  comment5: 'Tu ne crées pas la bonne fonction&nbsp;!',
  comment6: 'On attend une fonction sans argument&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Voici dans la fenêtre ci-contre un programme répondant à la question.',
  correctionAlgo: 'Programme', // possible",
  pythonErreur: 'Ton programme a bogué pour',
  // pythonDiag1 : "Erreur ligne £l : le programme donne £r alors qu’on devrait avoir £{rep}.",
  pythonDiag1: 'Erreur lors du test de £{nomf}()&nbsp;:\n',
  pythonDiag2: 'Avec ton code, on obtient&nbsp;: ',
  pythonDiag3: 'alors qu’on devrait obtenir&nbsp;: ',
  sortie: 'Sortie',
  executer: 'Exécuter',
  reinitialiser: 'Réinitialiser',
  editeur: 'Editeur :',
  console: 'Console :'
}

/**
 * section bouclePourListe
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage
  let laConsole
  function execute () {
    runPython(stor.editor, laConsole, false)
  }

  function reinitialiser () {
    setTextAce(stor.editor, stor.algoInit)
  }

  function verifierCodePython (algoEleve, algoSecret, entrees, typeSortie) {
    me.logIfDebug('algoEleve=', algoEleve, '\nalgoSecret=', algoSecret, '\nentrees=', entrees)
    let lignes, ligne
    // on ajoute les entrees à tester
    let debutTest = ''
    let cle, valeur
    for (let k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      debutTest = debutTest + cle + ' = ' + valeur + '\n'
    }
    me.logIfDebug('debutTest', debutTest, '\nalgoEleve avant', algoEleve)
    lignes = algoEleve.split('\n')
    let algoEleveTest = debutTest
    for (let i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne == null) continue
      if (ligne.includes('input(')) continue
      algoEleveTest = algoEleveTest + ligne + '\n'
    }
    me.logIfDebug('algoEleve apres', algoEleveTest, '\nalgo secret avant', algoSecret)
    lignes = algoSecret.split('\n')
    let algoSecretTest = debutTest
    for (let i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.indexOf('input(') >= 0) continue
      algoSecretTest = algoSecretTest + ligne + '\n'
    }
    me.logIfDebug('algo secret apres', algoSecretTest)
    // on exécute algoSecretTest
    runPythonCode(algoSecretTest)
    let sortieSecret = j3pElement('output').innerHTML
    me.logIfDebug('sortieSecret=(', sortieSecret + ')')
    // on exécute algoEleveTest
    runPythonCode(algoEleveTest)
    let sortieEleve = j3pElement('output').innerHTML
    me.logIfDebug('sortieEleve=(', sortieEleve + ')')
    // on regarde si le test élève s’exécute
    let diagnostique
    if (sortieEleve.indexOf('Error') !== -1) {
      diagnostique = textes.pythonErreur
      for (let k = 0; k < Object.keys(entrees).length; k++) {
        cle = Object.keys(entrees)[k]
        valeur = Object.values(entrees)[k]
        diagnostique = diagnostique + ' ' + cle + '=' + valeur
      }
      j3pElement('output').innerHTML = diagnostique + '\n' + j3pElement('output').innerHTML
      return false
    }
    // on compare les 2 exécutions
    sortieSecret = sortieSecret.replace(/\r\n/g, '')
    sortieEleve = sortieEleve.replace(/\r\n/g, '')
    let identique = true
    switch (typeSortie) {
      case 'number':
        identique = (Math.abs(Number(sortieSecret) - Number(sortieEleve)) < Math.pow(10, -12))
        break
      case 'string':
        // l’égalité de la sortie pose pb.
        // Je teste donc si à la fin j’ai bien la sortie attendue
        { const posSortieSecret = sortieEleve.toLowerCase().lastIndexOf(sortieSecret)
          identique = (posSortieSecret > -1)
          if (identique) {
          // on vérifie que c’est la dernière chose qui soit affichée
            identique = (sortieEleve.substring(posSortieSecret + sortieSecret.length).length === 0)
          }
        }
        break
    }

    if (identique) {
      j3pElement('output').innerHTML = ''
      return true
    }
    // on explique l’échec
    diagnostique = j3pChaine(textes.pythonDiag1, stor.obj)
    for (let k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      diagnostique = diagnostique + ' ' + cle + '=' + valeur
    }
    diagnostique = diagnostique + '\n' + textes.pythonDiag2 + '\n' + sortieEleve
    diagnostique = diagnostique + '\n' + textes.pythonDiag3 + '\n' + sortieSecret
    j3pElement('output').innerHTML = diagnostique
    return false
  }

  function creationEditeurPython (obj) {
    // obj est un objet contenant 2 propriétés:
    // hautEditeur (par défaut 200)
    // largPour (par défaut 95), c’est en pourcentage
    // hautConsole (par défaut 22)
    const h = (isNaN(Number(obj.hautEditeur))) ? 200 : obj.hautEditeur
    const l = (isNaN(Number(obj.largPour))) ? 95 : obj.largPour
    const h2 = (isNaN(Number(obj.hautConsole))) ? 22 : obj.hautConsole
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    // je m’occupe d’abord de la construction de l’éditeur
    j3pAffiche(stor.zoneCons1, '', textes.editeur)
    stor.idEditor = j3pGetNewId('idEditor')
    stor.divEditor = j3pAddElt(stor.zoneCons2, 'div', '', { id: stor.idEditor })
    j3pStyle(stor.divEditor, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    stor.editor = ace.edit(stor.idEditor)
    stor.divEditor.style.height = String(h) + 'px'
    stor.divEditor.style.width = String(l) + '%'
    stor.editor.resize()
    stor.editor.getSession().setUseSoftTabs(false)
    // stor.editor.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    stor.editor.getSession().setMode('ace/mode/python')
    stor.editor.setFontSize('12pt')
    stor.editor.focus()
    // et maintenant, place à la console
    j3pAffiche(stor.zoneCons3, '', textes.console)
    stor.idConsole = j3pGetNewId('idConsole')
    stor.divConsole = j3pAddElt(stor.zoneCons4, 'div', '', { id: stor.idConsole })
    j3pStyle(stor.divConsole, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    laConsole = ace.edit(stor.idConsole)
    stor.divConsole.style.height = String(h2) + 'px'
    stor.divConsole.style.width = String(l) + '%'
    stor.divConsole.style.marginTop = '2px'
    stor.divConsole.style.marginBottom = '2px'
    // laConsole.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    laConsole.getSession().setMode('ace/mode/python')
    laConsole.setFontSize('12pt')

    /* console.log(stor.divConsole.childNodes)
    console.log(stor.divConsole.childNodes[1].childNodes)
    console.log(stor.divConsole.childNodes[1].childNodes[0].childNodes) */
    const consolePython = stor.divConsole
    const elt = consolePython.childNodes[1]?.childNodes[0]?.childNodes[0]
    if (elt) {
      elt.innerHTML = '>>>'
      elt.style.paddingLeft = '10px'
    } else {
      j3pDetruit(stor.divConsole)
      j3pDetruit(stor.zoneCons3)
      console.error(Error('pas trouvé les enfants de la console'), stor.divConsole)
    }
    stor.divBtns = j3pAddElt(stor.zoneCons5, 'div')
    stor.divBtns.style.textAlign = 'right'
    stor.idExecuter = j3pGetNewId('executer')
    stor.idReinitialiser = j3pGetNewId('reinitialiser')
    j3pAjouteBouton(stor.divBtns, stor.idExecuter, 'MepBoutons', textes.executer, '')
    j3pAjouteBouton(stor.divBtns, stor.idReinitialiser, 'MepBoutons', textes.reinitialiser, '')
    j3pElement(stor.idExecuter).addEventListener('click', execute)
    // Et enfin la sortie de l’algo
    j3pAffiche(stor.zoneCons6, '', textes.utiliserConsole, { L: stor.nomListe })
    j3pStyle(stor.zoneCons6, { fontSize: '0.9em', fontStyle: 'italic' })
    const underline = j3pAddElt(stor.zoneCons7, 'u')
    j3pAddTxt(underline, textes.sortie)
    stor.output = 'output'
    stor.pre = j3pAddElt(stor.zoneCons7, 'pre', { id: stor.output })
    j3pStyle(stor.pre, { color: 'black' })
  }

  function afficheCorr (bonneRep) {
    const zoneExpli = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneRep) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1)
    stor.nameProg = j3pGetNewId('Programme')
    stor.fenetreCorr = j3pGetNewId('fenetreCorr')
    me.fenetresjq[0] = {
      name: stor.nameProg,
      title: textes.correctionAlgo,
      top: 110,
      left: 5,
      width: 450,
      id: stor.fenetreCorr
    }
    j3pCreeFenetres(me)
    const algoCorr = j3pAddElt(stor.fenetreCorr, 'div', '', { style: me.styles.etendre('petit.explications', { fontSize: '1.2em' }) })
    algoCorr.className = 'code1'
    activerClasseCode()
    const tabRep = stor.algoReponse.split('\n')
    const espaceTab = '&nbsp;&nbsp;&nbsp;&nbsp;'
    for (let i = 0; i < tabRep.length - 1; i++) {
      try {
        tabRep[i] = tabRep[i].replace(/\t/g, espaceTab)
        j3pAffiche(algoCorr, '', tabRep[i])
        j3pAddContent(algoCorr, '<br>')
      } catch (e) {
        console.warn(e) // pas normal mais pas grave
      }
    }
    j3pToggleFenetres(stor.nameProg)
    j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
    j3pDetruit(stor.idReinitialiser)
  }

  function nettoyageCorrection () {
    j3pDetruitFenetres(stor.nameProg)
  }

  function activerClasseCode () {
    // Cette fonction permet d’appliquer le style ci-dessous aux classes code1 et code2
    const code1 = $('.code1')
    const code2 = $('.code2')
    code1.css('font-family', 'Lucida Console')
    code1.css('font-size', '1.2em')
    code1.css('background-color', '#FFFFFF')
    code1.css('color', '#000000')
    code2.css('font-family', 'Lucida Console')
    code2.css('font-size', '0.8em')
    code2.css('background-color', '#FFFFFF')
    code2.css('color', '#000000')
  }

  function enonceInit () {
    me.validOnEnter = false // ex donneesSection.touche_entree
    // Construction de la page
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    stor.tabQuest = [1, 2, 3, 4]
    stor.tabQuestInit = [...stor.tabQuest]
    init()
      .then(enonceMain)
      .catch(j3pShowError)
  }

  function enonceMain () {
    if (stor.tabQuest.length === 0) {
      stor.tabQuest = [...stor.tabQuestInit]
    }
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.quest = quest
    const obj = {}
    switch (quest) {
      case 1:
        obj.b = j3pGetRandomInt(2, 3)
        obj.p = j3pGetRandomInt(10, 13)
        obj.n = Math.pow(obj.b, obj.p)
        obj.liste = 'listPuis'
        obj.nomf = 'puis' + obj.b
        break
      case 2:
        obj.k = j3pGetRandomInt(3, 6)
        obj.p = j3pGetRandomInt(10, 13)
        obj.n = obj.k * obj.p
        obj.liste = 'listMult'
        obj.nomf = 'mult'
        break
      case 3:
        obj.nomf = j3pRandomTab(['carres', 'cubes'], [0.5, 0.5])
        obj.typef = (obj.nomf === 'carres') ? 'carrés' : 'cubes'
        obj.k = (obj.nomf === 'carres') ? 2 : 3
        obj.n1 = j3pGetRandomInt(2, 5)
        obj.n2 = obj.n1 + 10 + j3pGetRandomInt(2, 5)
        obj.m1 = Math.pow(obj.n1, obj.k)
        obj.m2 = Math.pow(obj.n2, obj.k)
        obj.liste = (obj.nomf === 'carres') ? 'listCarres' : 'listCubes'
        break
      case 4:
        obj.f = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
        obj.nomf = 'images' + obj.f
        obj.a = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 3)
        obj.b = j3pGetRandomInt(2, 5)
        obj.n1 = j3pGetRandomInt(2, 5)
        obj.n2 = obj.n1 + 10 + j3pGetRandomInt(2, 5)
        obj.m1 = obj.a * Math.pow(obj.n1, 2) - obj.b
        obj.m2 = obj.a * Math.pow(obj.n2, 2) - obj.b
        break
    }

    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    for (let i = 1; i <= 2; i++) stor['zoneConsD' + i] = j3pAddElt(stor.conteneurD, 'div')
    const ligneAlgo1 = 'def ' + obj.nomf + '():\n'
    if (ds.aideFonction) {
      stor.algoInit = ligneAlgo1
      stor.algoInit += '\n'
    } else {
      stor.algoInit = '\n'
    }

    const consigne = (ds.aideFonction) ? textes['consigneBis1_' + quest] : textes['consigne1_' + quest]
    j3pAffiche(stor.zoneConsD1, '', consigne, obj)
    j3pAffiche(stor.zoneConsD2, '', textes.consigne2)

    const listeAlgoCorr = [ligneAlgo1]
    if (quest <= 2) {
      listeAlgoCorr.push('\tfor i in range(' + (obj.p + 1) + '):\n')
    } else {
      listeAlgoCorr.push('\tfor i in range(' + (obj.n1) + ',' + (obj.n2 + 1) + '):\n')
    }
    switch (quest) {
      case 1:
        listeAlgoCorr.push('\t\tprint(' + obj.b + '**i)\n')
        break
      case 2:
        listeAlgoCorr.push('\t\tprint(' + obj.k + '*i)\n')
        break
      case 3:
        listeAlgoCorr.push('\t\tprint(i**' + obj.k + ')\n')
        break
      case 4:
        listeAlgoCorr.push('\t\tprint(' + obj.a + '*i**2-' + obj.b + ')\n')
        break
    }
    stor.dernieresLignes = obj.nomf + '()'
    stor.algoReponse = ''
    stor.algoCorr = ''
    for (let i = 0; i < listeAlgoCorr.length; i++) {
      stor.algoReponse += listeAlgoCorr[i]
      stor.algoCorr += listeAlgoCorr[i]
    }
    stor.algoCorr += stor.dernieresLignes
    me.logIfDebug('algoReponse:', stor.algoReponse)
    creationEditeurPython({ hautEditeur: 200, hautConsole: 20 })
    setTextAce(stor.editor, stor.algoInit)
    setTextAce(laConsole, '')
    stor.obj = obj
    j3pElement(stor.idReinitialiser).addEventListener('click', reinitialiser)

    j3pElement('output').innerHTML = ''
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvent dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCorr.style.paddingTop = '15px'

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        try {
          j3pDetruit(stor.fenetreCorr)
        } catch (e) {
          console.warn(e) // pas normal mais pas grave
        }
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const reponse = { aRepondu: false, bonneReponse: true }
        stor.algoEleveInit = getTextAce(stor.editor)
        const tabLigne = stor.algoEleveInit.split('\n')
        const k = 0
        while (k < tabLigne.length) {
          if (tabLigne[k].replace(/\s/g, '') === '') {
            // Cette ligne est vide, on la vire
            tabLigne.shift()
          } else {
            break
          }
        }
        const aucunChgt = (tabLigne.length === 0)
        reponse.aRepondu = !aucunChgt
        let definitionFct = true
        let bonneFonction = true
        let avecArgument = false
        let algoTest
        let objDeuxPts
        if (reponse.aRepondu) {
        // On vérifie que le programme est bien la création d’une fonction
          algoTest = stor.algoEleveInit
          if (tabLigne[0].indexOf('def') !== 0) {
          // C’est que l’élève n’a pas créé de fonction
            definitionFct = false
            reponse.bonneReponse = false
          } else {
          // Maintenant je regarde si c’est bien la bonne fonction
            if (stor.quest === 4) {
            // L’élève peut très bien se contruire une fonction f, donc je vérifie si une ligne contient def et si c’est bien def nomf
              bonneFonction = false
              for (let i = 0; i < tabLigne.length; i++) {
                if (tabLigne[i].replace(/\s/g, '').indexOf('def' + stor.obj.nomf) === 0) {
                  bonneFonction = true
                  break
                }
              }
            } else {
              bonneFonction = (tabLigne[0].replace(/\s/g, '').indexOf('def' + stor.obj.nomf) === 0)
            }
            reponse.bonneReponse = bonneFonction
          }
          if (definitionFct && bonneFonction) {
            objDeuxPts = verifDeuxPoints(stor.algoEleveInit)
            me.logIfDebug('objDeuxPts:', objDeuxPts)
            if (objDeuxPts.presents) {
            // Je vérifie que la fonction est bien sans argument
              avecArgument = (tabLigne.join('').replace(/\s/g, '').indexOf('def' + stor.obj.nomf + '()') === -1)
              reponse.bonneReponse = !avecArgument
              reponse.aRepondu = !avecArgument
              if (reponse.bonneReponse) {
                algoTest += '\n' + stor.dernieresLignes
                reponse.bonneReponse = verifierCodePython(algoTest, stor.algoCorr, {}, 'string')
              }
            } else {
              reponse.bonneReponse = false
            }
          }
        }

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          const msgRepManquante = (avecArgument)
            ? textes.comment6
            : (aucunChgt)
                ? textes.comment1
                : textes.comment2
          me.reponseManquante(stor.zoneCorr, msgRepManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            // afficheCorr(true);
            desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
            j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
            j3pDetruit(stor.idReinitialiser)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!definitionFct) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment4
              } else if (!bonneFonction) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment5
              } else if (!objDeuxPts.presents) {
                stor.zoneCorr.innerHTML += '<br>' + j3pChaine(textes.comment3, { l: objDeuxPts.numLigne })
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        me.buttonsElts.sectionSuivante?.addEventListener('click', nettoyageCorrection)
        j3pElement(stor.idExecuter).removeEventListener('click', execute)
      } else {
        me.buttonsElts.suite?.addEventListener('click', nettoyageCorrection)
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
