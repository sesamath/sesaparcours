import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pArrondi, j3pChaine, j3pDetruit, j3pElement, j3pGetNewId, j3pGetRandomInt, j3pShowError, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { init, desactiverEditeur, getTextAce, runPython, runPythonCode, setTextAce } from 'src/legacy/outils/algo/algo_python'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitToutesLesFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mai 2019
        section sur les listes : création et manipulation sur des commandes de base
 */

/* global ace */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['aideFonction', false, 'boolean', 'Lorsque ce paramètre vaut true, la première ligne du programme (définition de la fonction) est donnée.']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Utiliser une fonction',
  // on donne les phrases de la consigne
  consigne1_1: 'Un parc d’attraction propose deux tarifs distincts&nbsp;:',
  consigne2_1: 'l’entrée coûte £e&nbsp;€ pour les enfants et £f&nbsp;€ pour les adultes.',
  consigne3_1: 'Ecrire au format python une fonction nommée tarif admettant deux paramètres enfants et parents (à écrire dans cet ordre pour la validation) et renvoyant le tarif payé par un groupe pour entrer dans ce parc.',
  consigne1_2: 'Une portion d’autoroute propose deux tarifs suivant la catégorie du véhicule&nbsp;:',
  consigne2_2: 'le péage coûte £e&nbsp;€ pour un véhicule de classe 1 et £f&nbsp;€ pour un véhicule de classe 2.',
  consigne3_2: 'Ecrire au format python une fonction nommée montant admettant deux paramètres classe1 et classe2 (à écrire dans cet ordre pour la validation) et renvoyant le montant payé par un ensemble de véhicules passés par cette barrière de péage.',
  consigne1_3: 'Une compagnie de service postale spécialisée dans les colis propose deux tarifs distincts&nbsp;:',
  consigne2_3: '£e&nbsp;€ pour un colis de moins de 400&nbsp;g et £f&nbsp;€ pour un colis de plus de 400&nbsp;g.',
  consigne3_3: 'Ecrire au format python une fonction nommée tarif admettant deux paramètres moins400 et plus400 (à écrire dans cet ordre pour la validation) et renvoyant le tarif payé par un envoi groupé de plusieurs colis.',
  consigne1_4: 'Un opérateur téléphonique propose deux tarifs à ses clients&nbsp;:',
  consigne2_4: '£e&nbsp;€ par mois pour les clients qui n’ont qu’un mobile et £f&nbsp;€ pour ceux qui ont également un abonnement à leur box.',
  consigne3_4: 'Ecrire au format python une fonction nommée montant admettant deux paramètres mobile et box (à écrire dans cet ordre pour la validation), correspondant au nombre d’abonnements mobiles et d’abonnements box d’un ensemble de clients, et renvoyant le montant total payé par cet ensemble de clients.',
  consigne1_5: 'Un peintre propose deux tarifs à ses clients suivant la surface à peindre&nbsp;:',
  consigne2_5: 'peindre un plafond revient à £e&nbsp;€ le m² et un mur à £f&nbsp;€ le m².',
  consigne3_5: 'Ecrire au format python une fonction nommée devis admettant deux paramètres plafond et mur (à écrire dans cet ordre pour la validation) et renvoyant le montant d’un devis pour un client suivant les surfaces à peindre.',
  consigne1_6: 'Une librairie propose un destockage pour travaux et met en vente ses livres suivant deux tarifs&nbsp;:',
  consigne2_6: '£e&nbsp;€ pour les bandes dessinées et £f&nbsp;€ pour les autres livres.',
  consigne3_6: 'Ecrire au format python une fonction nommée tarif admettant deux paramètres BD et livres (à écrire dans cet ordre pour la validation) et renvoyant le tarif payé pour un achat de plusieurs livres et bandes dessinées.',
  consigne1_7: 'Un camping propose deux tarifs distincts&nbsp;:',
  consigne2_7: '£e&nbsp;€ par semaine pour la location d’un mobile-home et £f&nbsp;€ pour un emplacement pour des tentes.',
  consigne3_7: 'Ecrire au format python une fonction nommée montant admettant deux paramètres mobileHome et tente (à écrire dans cet ordre pour la validation) et renvoyant le montant total de la semaine suivant les locations de plusieurs clients.',
  consigne5: 'Ne pas hésiter à utiliser la console pour tester la fonction.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'L’algorithme doit être complété&nbsp;!',
  comment2: 'A la fin de chaque ligne contenant un mot clé, on devrait avoir deux points (par exemple ligne £l)&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Voici dans la fenêtre ci-contre un programme répondant à la question.',
  correctionAlgo: 'Programme possible',
  pythonErreur: 'Ton programme a bogué pour',
  pythonDiag1: 'Si on effectue le test de la fonction avec ces valeurs&nbsp;:\n',
  pythonDiag2: 'Avec ton code, on obtient&nbsp;: ',
  pythonDiag3: 'alors qu’on devrait obtenir&nbsp;: ',
  utiliserConsole: 'Ne pas hésiter à utiliser la console pour tester le programme.',
  sortie: 'Sortie',
  executer: 'Exécuter',
  reinitialiser: 'Réinitialiser',
  editeur: 'Editeur :',
  console: 'Console :'
}

/**
 * section utiliserFct
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  let laConsole
  // ---------------------------------------------------------------------------
  // Vérification de code
  // ---------------------------------------------------------------------------
  function verifDeuxPoints (algo) {
    // algo est l’algo au format python
    // on vérifie qu'à la fin de chaque ligne comportant un mot clé on ait les deux-points
    const motsCles = ['def', 'for', 'if', 'while', 'elif', 'else']
    const lignes = algo.split('\n')
    let deuxPointsPresents = true
    let numLigne = -1
    // console.log("algo:",algo)
    let i, k
    for (i = lignes.length - 1; i >= 0; i--) {
      let txt = lignes[i].replace(/\t/g, '')
      for (k = 0; k < motsCles.length; k++) {
        if (txt.indexOf(motsCles[k]) === 0) {
          // je vérifie qu'à la fin j’ai bien deux points (attention aux espaces)
          txt = txt.replace(/\s*/g, '')
          if (txt.charAt(txt.length - 1) !== ':') {
            deuxPointsPresents = false
            numLigne = i
          }
        }
      }
    }
    return { presents: deuxPointsPresents, numLigne: numLigne + 1 }
  }

  function execute () {
    runPython(stor.editor, laConsole)
  }

  function reinitialiser () {
    setTextAce(stor.editor, stor.algoInit)
  }

  function egaliteList (liste1, liste2) {
    // teste si deux listes
    let egalite = (typeof (liste1) === typeof (liste2))// on vérifie indirectement que ce sont deux objets du même type
    if (egalite) {
      if (typeof (liste1) === 'string') {
        egalite = (liste1 === liste2)
      } else if (!isNaN(Number(liste1))) {
        egalite = (Math.abs(liste1 - liste2) < Math.pow(10, -12))
      } else {
        egalite = (liste1.length === liste2.length)
        if (egalite) {
          for (let k = 0; k < liste1.length; k++) {
            egalite = egaliteList(liste1[k], liste2[k])
          }
        }
      }
    }
    return egalite
  }

  function verifierCodePython (algoEleve, algoSecret, entrees, typeSortie) {
    me.logIfDebug('algoEleve=', algoEleve, '\nalgoSecret=', algoSecret, '\nentrees=', entrees)
    let lignes, ligne
    // on ajoute les entrees à tester
    let debutTest = ''
    let k, cle, valeur, i
    for (k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      debutTest = debutTest + cle + ' = ' + valeur + '\n'
    }
    me.logIfDebug('debutTest', debutTest, 'algoEleve avant', algoEleve)
    lignes = algoEleve.split('\n')
    let algoEleveTest = debutTest
    for (i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.includes('input(')) continue
      algoEleveTest = algoEleveTest + ligne + '\n'
    }
    me.logIfDebug('algoEleve apres', algoEleveTest, 'algo secret avant', algoSecret)
    lignes = algoSecret.split('\n')
    let algoSecretTest = debutTest
    for (i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.indexOf('input(') >= 0) continue
      algoSecretTest = algoSecretTest + ligne + '\n'
    }
    me.logIfDebug('algo secret apres', algoSecretTest)
    // on exécute algo_secret_test
    runPythonCode(algoSecretTest)
    let sortieSecret = j3pElement('output').innerHTML
    me.logIfDebug('sortieSecret=(', sortieSecret + ')')
    // on exécute algoEleveTest
    runPythonCode(algoEleveTest)
    let sortieEleve = j3pElement('output').innerHTML
    me.logIfDebug('sortieEleve=(', sortieEleve + ')')
    // on regarde si le test élève s’exécute
    let diagnostique
    if (sortieEleve.includes('Error')) {
      diagnostique = textes.pythonErreur
      for (k = 0; k < Object.keys(entrees).length; k++) {
        cle = Object.keys(entrees)[k]
        valeur = Object.values(entrees)[k]
        diagnostique = diagnostique + ' ' + cle + '=' + valeur
      }
      j3pElement('output').innerHTML = diagnostique + '\n' + j3pElement('output').innerHTML
      return false
    }
    // on compare les 2 exécutions
    sortieSecret = sortieSecret.split('\n').join('')
    sortieSecret = sortieSecret.split('<br>').join('')
    sortieSecret.replace(/\r\n/g, '')
    sortieEleve = sortieEleve.split('\n').join('')
    sortieEleve = sortieEleve.split('<br>').join('')
    sortieEleve.replace(/\r\n/g, '')
    let identique = true
    switch (typeSortie) {
      case 'number':
        identique = (Math.abs(Number(sortieSecret) - Number(sortieEleve)) < Math.pow(10, -12))
        break
      case 'string':
        identique = (sortieSecret === sortieEleve)
        break
      case 'list':
        identique = egaliteList(sortieEleve, sortieSecret)
        break
    }

    if (identique) {
      j3pElement('output').innerHTML = ''
      return true
    }
    // on explique l’échec
    diagnostique = textes.pythonDiag1
    for (k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      diagnostique = diagnostique + ' ' + cle + '=' + valeur
    }
    diagnostique = diagnostique + '\n' + textes.pythonDiag2 + sortieEleve
    diagnostique = diagnostique + '\n' + textes.pythonDiag3 + sortieSecret
    j3pElement('output').innerHTML = diagnostique
    return false
  }

  function creationEditeurPython () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    // je m’occupe d’abord de la construction de l’éditeur
    j3pAffiche(stor.zoneCons1, '', textes.editeur)
    stor.idEditor = j3pGetNewId('idEditor')
    stor.divEditor = j3pAddElt(stor.zoneCons2, 'div', '', { id: stor.idEditor })
    j3pStyle(stor.divEditor, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    stor.editor = ace.edit(stor.idEditor)
    stor.divEditor.style.height = '100px'
    stor.divEditor.style.width = '95%'
    stor.editor.resize()
    stor.editor.getSession().setUseSoftTabs(false)
    // stor.editor.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    stor.editor.getSession().setMode('ace/mode/python')
    stor.editor.setFontSize('12pt')
    stor.editor.focus()
    // Sk.editor = stor.editor
    // et maintenant, place à la console
    j3pAffiche(stor.zoneCons3, '', textes.console)
    stor.idConsole = j3pGetNewId('idConsole')
    stor.divConsole = j3pAddElt(stor.zoneCons4, 'div', '', { id: stor.idConsole })
    j3pStyle(stor.divConsole, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    laConsole = ace.edit(stor.idConsole)
    stor.divConsole.style.height = '40px'
    stor.divConsole.style.width = '95%'
    stor.divConsole.style.marginTop = '2px'
    stor.divConsole.style.marginBottom = '2px'
    // laConsole.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    laConsole.getSession().setMode('ace/mode/python')
    laConsole.setFontSize('12pt')
    let avecConsole = true
    try {
      // J’ai eu un signalement où cet objet HTML n’existait pas ce qui faisait planter la section
      // Je ne vois pas comment ce peut être possible
      stor.divConsole.childNodes[1].childNodes[0].childNodes[0].innerHTML = '>>>'
      stor.divConsole.childNodes[1].childNodes[0].childNodes[0].style.paddingLeft = '10px'
    } catch (e) {
      avecConsole = false
      // Je vire la console car il y a un pb pour son affichage :
      j3pDetruit(stor.zoneCons3)
      j3pDetruit(stor.divConsole)
      console.warn('soucis avec l’élément HTML censé contenir les chevrons')
    }
    stor.divBtns = j3pAddElt(stor.zoneCons5, 'div')
    stor.divBtns.style.textAlign = 'right'
    stor.idExecuter = j3pGetNewId('executer')
    stor.idReinitialiser = j3pGetNewId('reinitialiser')
    j3pAjouteBouton(stor.divBtns, stor.idExecuter, 'MepBoutons', textes.executer, '')
    j3pAjouteBouton(stor.divBtns, stor.idReinitialiser, 'MepBoutons', textes.reinitialiser, '')
    j3pElement(stor.idExecuter).addEventListener('click', execute)
    // Et enfin la sortie de l’algo
    if (avecConsole) {
      j3pAffiche(stor.zoneCons6, '', textes.utiliserConsole, { L: stor.nomListe })
      j3pStyle(stor.zoneCons6, { fontSize: '0.9em', fontStyle: 'italic' })
    }
    const underline = j3pAddElt(stor.zoneCons7, 'u')
    j3pAddTxt(underline, textes.sortie)
    stor.output = 'output'
    stor.pre = j3pAddElt(stor.zoneCons7, 'pre', { id: stor.output })
    j3pStyle(stor.pre, { color: 'black' })
  }

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1)
    stor.nameProg = j3pGetNewId('Programme')
    stor.fenetreCorr = j3pGetNewId('fenetreCorr')
    me.fenetresjq = [{
      name: stor.nameProg,
      title: textes.correctionAlgo,
      top: 110,
      left: 5,
      width: 450,
      id: stor.fenetreCorr
    }]
    j3pCreeFenetres(me)
    const algoCorr = j3pAddElt(stor.fenetreCorr, 'div', '', { style: me.styles.etendre('petit.explications', { fontSize: '1.2em' }) })
    const tabRep = stor.algoReponse.split('\n')
    const espaceTab = '&nbsp;&nbsp;&nbsp;&nbsp;'
    for (let i = 0; i < tabRep.length - 1; i++) {
      try {
        tabRep[i] = tabRep[i].replace(/\t/g, espaceTab)
        j3pAffiche(algoCorr, '', tabRep[i])
        j3pAddContent(algoCorr, '<br>')
      } catch (e) {
        console.warn(e) // pas normal mais pas grave
      }
    }
    j3pToggleFenetres(stor.nameProg)
    j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
    j3pDetruit(stor.idReinitialiser)
  }

  function enonceInit () {
    me.validOnEnter = false // ex donneesSection.touche_entree
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    stor.tabQuest = [1, 2, 3, 4, 5]
    stor.tabQuestInit = [...stor.tabQuest]
    init()
      .then(enonceMain)
      .catch(j3pShowError)
  }

  function enonceMain () {
    if (me.questionCourante === 1) {
      if (stor.tabQuest.length === 0) {
        stor.tabQuest = [...stor.tabQuestInit]
      }
    }
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.quest = quest
    let tarif1, tarif2,
      var1, var2, nomFct
    switch (quest) {
      case 1:
        tarif1 = j3pGetRandomInt(21, 31) / 2
        tarif2 = tarif1 + j3pGetRandomInt(9, 15) / 2
        var1 = 'enfants'
        var2 = 'parents'
        nomFct = 'tarif'
        break
      case 2:
        tarif1 = j3pGetRandomInt(80, 130) / 10
        tarif2 = tarif1 + j3pGetRandomInt(5, 15) / 2
        var1 = 'classe1'
        var2 = 'classe2'
        nomFct = 'montant'
        break
      case 3:
        tarif1 = j3pGetRandomInt(20, 30) / 10
        tarif2 = tarif1 + j3pGetRandomInt(15, 25) / 10
        var1 = 'moins400'
        var2 = 'plus400'
        nomFct = 'tarif'
        break
      case 4:
        tarif1 = j3pGetRandomInt(28, 35) / 2
        tarif2 = tarif1 + j3pGetRandomInt(8, 12) / 2
        var1 = 'mobile'
        var2 = 'box'
        nomFct = 'montant'
        break
      case 5:
        tarif1 = j3pGetRandomInt(120, 140) / 10
        tarif2 = tarif1 - j3pGetRandomInt(3, 5) / 2
        var1 = 'plafond'
        var2 = 'mur'
        nomFct = 'devis'
        break
      case 6:
        tarif1 = j3pGetRandomInt(14, 20) / 2
        tarif2 = tarif1 + j3pGetRandomInt(5, 9) / 2
        var1 = 'BD'
        var2 = 'livres'
        nomFct = 'tarif'
        break
      case 7:
        tarif1 = j3pGetRandomInt(21, 30) * 10
        tarif2 = tarif1 - 100 + j3pGetRandomInt(10, 30)
        var1 = 'mobileHome'
        var2 = 'tente'
        nomFct = 'montant'
        break
    }
    tarif1 = j3pArrondi(tarif1, 5)
    tarif2 = j3pArrondi(tarif2, 5)
    let tarifTxt1 = j3pVirgule(tarif1)
    let tarifTxt2 = j3pVirgule(tarif2)
    if (Math.abs(tarif1 - Math.round(tarif1)) > Math.pow(10, -12)) {
      // On affiche le tarif avec deux chiffres après la virgule (là il n’y en a qu’un
      tarifTxt1 += '0'
    }
    if (Math.abs(tarif2 - Math.round(tarif2)) > Math.pow(10, -12)) {
      // On affiche le tarif avec deux chiffres après la virgule (là il n’y en a qu’un
      tarifTxt2 += '0'
    }

    // console.log('tarif1:', tarif1, '   tarif2:', tarif2)
    // console.log('tarifTxt1:', tarifTxt1, '   tarifTxt2:', tarifTxt2)
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    for (let i = 1; i <= 3; i++) stor['zoneConsD' + i] = j3pAddElt(stor.conteneurD, 'div')
    j3pAffiche(stor.zoneConsD1, '', textes['consigne1_' + quest])
    j3pAffiche(stor.zoneConsD2, '', textes['consigne2_' + quest],
      {
        e: tarifTxt1,
        f: tarifTxt2
      })
    j3pAffiche(stor.zoneConsD3, '', textes['consigne3_' + quest])

    const tabRep = []
    tabRep[0] = 'def ' + nomFct + '(' + var1 + ',' + var2 + '):'
    tabRep.push('\tprix=' + tarif1 + '*' + var1 + '+' + tarif2 + '*' + var2)
    tabRep.push('\treturn prix')

    stor.algoReponse = ''
    for (let i = 0; i < tabRep.length; i++) {
      stor.algoReponse += tabRep[i] + '\n'
    }
    stor.testConsole = 'print(' + nomFct + '(' + var1 + ',' + var2 + '))'
    me.logIfDebug('algoReponse:', stor.algoReponse)
    creationEditeurPython()
    stor.algoInit = (ds.aideFonction) ? tabRep[0] + '\n\t' : ''
    setTextAce(stor.editor, stor.algoInit)
    const consoleInit = ''
    setTextAce(laConsole, consoleInit)
    j3pElement(stor.idReinitialiser).addEventListener('click', reinitialiser)
    stor.nomVars = [var1, var2]
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvent dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    j3pStyle(stor.zoneCorr, { paddingTop: '10px' })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const reponse = { aRepondu: false, bonneReponse: true }
        stor.algoEleveInit = getTextAce(stor.editor)
        reponse.aRepondu = (stor.algoInit !== stor.algoEleveInit)
        let objDeuxPts
        if (reponse.aRepondu) {
          objDeuxPts = verifDeuxPoints(stor.algoEleveInit)
          me.logIfDebug('objDeuxPts:', objDeuxPts)
          if (objDeuxPts.presents) {
            const algoEleve = (stor.algoEleveInit[stor.algoEleveInit.length - 1] === '\n')
              ? stor.algoEleveInit + stor.testConsole
              : stor.algoEleveInit + '\n' + stor.testConsole
            const algoReponse = stor.algoReponse + stor.testConsole
            const objTests = []
            for (let i = 0; i <= 6; i++) objTests.push({})
            objTests[0][stor.nomVars[0]] = 0
            objTests[0][stor.nomVars[1]] = 5
            objTests[1][stor.nomVars[0]] = 4
            objTests[1][stor.nomVars[1]] = 0
            objTests[2][stor.nomVars[0]] = 2
            objTests[2][stor.nomVars[1]] = 3
            objTests[3][stor.nomVars[0]] = 4
            objTests[3][stor.nomVars[1]] = 6
            objTests[4][stor.nomVars[0]] = 5
            objTests[4][stor.nomVars[1]] = 3
            objTests[5][stor.nomVars[0]] = 7
            objTests[5][stor.nomVars[1]] = 4
            objTests[6][stor.nomVars[0]] = 11
            objTests[6][stor.nomVars[1]] = 13
            let numTests = 0
            while (reponse.bonneReponse && numTests < objTests.length) {
              const typeRep = 'number'
              reponse.bonneReponse = verifierCodePython(algoEleve, algoReponse, objTests[numTests], typeRep)
              numTests++
            }
          }
        }

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          stor.zoneCorr.style.color = me.styles.cfaux
          // On peut ajouter un commentaire particulier.
          // j3pDiv(le_nom+"correction",le_nom+"rmq1",textes.comment1)
          stor.zoneCorr.innerHTML = textes.comment1
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            // afficheCorrection(true);
            desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
            j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
            j3pDetruit(stor.idReinitialiser)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              // j3pDiv(le_nom+"correction",le_nom+"rmq1",tempsDepasse)
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // j3pDiv(le_nom+"correction",le_nom+"rmq1",cFaux)
              if (!objDeuxPts.presents) {
                stor.zoneCorr.innerHTML += '<br>' + j3pChaine(textes.comment2, { l: objDeuxPts.numLigne })
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        me.buttonsElts.sectionSuivante?.addEventListener('click', j3pDetruitToutesLesFenetres)
        j3pElement(stor.idExecuter).removeEventListener('click', execute)
      } else {
        me.buttonsElts.suite?.addEventListener('click', j3pDetruitToutesLesFenetres)
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
