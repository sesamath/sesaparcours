import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pChaine, j3pDetruit, j3pElement, j3pGetNewId, j3pGetRandomInt, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { init, desactiverEditeur, getTextAce, runPython, runPythonCode, setTextAce, verifDeuxPoints } from 'src/legacy/outils/algo/algo_python'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitToutesLesFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juillet 2019
        section permettant de simuler une expérience avec deux issues
 */

/* global ace */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Simuler une expérience à deux issues',
  // on donne les phrases de la consigne
  consigne1: "En python, la fonction <span class='code'>random()</span> du module random renvoie un nombre aléatoire compris entre 0 et 1.",
  consigne2_1: '£p&nbsp;% des habitants d’une région ont un accès à Internet.',
  consigne2_2: '£p&nbsp;% des véhicules entretenus par un garage sont des véhicules diesel.',
  consigne2_3: 'Sur les £{e1} élèves d’une classe de première, £{e2} ont eu la moyenne au cours du dernier devoir de mathématiques.',
  consigne2_4: 'Au cours de la dernière heure, £p&nbsp;% des véhicules ayant franchi une barrière de péage étaient des berlines.',
  consigne2_5: 'Lors de la dernière assemblée générale d’une association, £{e1} membres étaient présents et parmi eux £{e2} font partie du bureau.',
  consigne2_6: 'Parmi les £{e1} jeux de console qu’Antoine possède, £{e2} sont des jeux de sport.',
  consigne3_1: 'Dans l’algorithme ci-contre écrit en python, compléter la ligne 2 à l’aide d’une seule inégalité pour simuler le choix au hasard d’un habitant de cette région selon qu’il ait accès à Internet ou non.',
  consigne3_2: 'Dans l’algorithme ci-contre écrit en python, compléter la ligne 2 à l’aide d’une seule inégalité pour simuler le choix au hasard d’un véhicule entretenu par ce garage selon que ce soit un diesel ou non.',
  consigne3_3: 'Dans l’algorithme ci-contre écrit en python, compléter la ligne 2 à l’aide d’une seule inégalité pour simuler le choix au hasard d’un élève de cette classe suivant qu’il ait eu la moyenne ou non au dernier devoir de mathématiques.',
  consigne3_4: 'Dans l’algorithme ci-contre écrit en python, compléter la ligne 2 à l’aide d’une seule inégalité pour simuler le choix au hasard d’un véhicule ayant franchi la barrière de péage suivant que ce soit une berline ou non.',
  consigne3_5: 'Dans l’algorithme ci-contre écrit en python, compléter la ligne 2 à l’aide d’une seule inégalité pour simuler le choix au hasard d’un membre de l’association suivant qu’il fasse partie du bureau ou non.',
  consigne3_6: 'Dans l’algorithme ci-contre écrit en python, compléter la ligne 2 à l’aide d’une seule inégalité pour simuler le choix au hasard d’un jeu d’Antoine suivant que ce soit un jeu de sport ou non.',
  consigne4: 'Seule la ligne 2 devra être modifiée, aucune autre...',
  // consigne5 : "Ne pas hésiter à utiliser la console pour tester la fonction.",

  // ATTENTION, pas de ’ dans ces strings qui seront interprétées par pyhthon
  print1: "cet habitant a un accès Internet|cet habitant n'a pas d'accès Internet",
  print2: "ce véhicule est un diesel|ce véhicule n'est pas un diesel",
  print3: "cet élève a eu la moyenne|cet élève n'a pas eu la moyenne",
  print4: "ce véhicule est une berline|ce véhicule n'est pas une berline",
  print5: 'ce membre fait partie du bureau|ce membre ne fait pas partie du bureau',
  print6: "ce jeu est un jeu de sport|ce jeu n'est pas un jeu de sport",
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'La ligne de l’algorithme doit être complétée&nbsp;!',
  comment2: 'A la fin de chaque ligne contenant un mot clé, on devrait avoir deux points (par exemple ligne £l)&nbsp;!',
  comment3: "Il faut utiliser la fonction <span class='code'>random()</span>&nbsp;!",
  comment4: 'On aurait pu mettre une inégalité large&nbsp;!',
  comment5: 'Il ne faut pas modifier autre chose que la ligne 2&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Voici dans la fenêtre ci-contre un programme répondant à la question.',
  correctionAlgo: 'Programme possible',
  pythonErreur: 'Ton programme a bogué pour',
  pythonDiag1: 'Si on effectue le test de la fonction avec ces valeurs&nbsp;:\n',
  pythonDiag2: 'Avec ton code, on obtient&nbsp;: ',
  pythonDiag3: 'alors qu’on devrait obtenir&nbsp;: ',
  sortie: 'Sortie',
  executer: 'Exécuter',
  reinitialiser: 'Réinitialiser',
  editeur: 'Editeur :',
  console: 'Console :'
}

/**
 * section simulDeuxIssues
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let laConsole

  function execute () {
    runPython(stor.editor, laConsole)
  }

  function reinitialiser () {
    setTextAce(stor.editor, stor.algoInit)
  }

  function verifierCodePython (algoEleve, algoSecret, entrees, typeSortie) {
    me.logIfDebug('algoEleve=', algoEleve, '\nalgoSecret=', algoSecret, '\nentrees=', entrees)
    let lignes, ligne, i, k
    // on ajoute les entrees à tester
    let debutTest = ''
    let cle; let valeur
    for (k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      debutTest = debutTest + cle + ' = ' + valeur + '\n'
    }
    me.logIfDebug('debutTest', debutTest, 'algoEleve avant', algoEleve)
    lignes = algoEleve.split('\n')
    let algoEleveTest = debutTest
    for (i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.indexOf('input(') >= 0) continue
      algoEleveTest = algoEleveTest + ligne + '\n'
    }
    me.logIfDebug('algoEleve apres', algoEleveTest, '\nalgo secret avant', algoSecret)
    lignes = algoSecret.split('\n')
    let algoSecretTest = debutTest
    for (i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.indexOf('input(') >= 0) continue
      algoSecretTest = algoSecretTest + ligne + '\n'
    }
    me.logIfDebug('algo secret apres', algoSecretTest)
    // on exécute algoSecretTest
    runPythonCode(algoSecretTest)
    let sortieSecret = j3pElement('output').innerHTML
    me.logIfDebug('sortieSecret=(', sortieSecret + ')')
    // on exécute algoEleveTest
    runPythonCode(algoEleveTest)
    let sortieEleve = j3pElement('output').innerHTML
    me.logIfDebug('sortieEleve=(', sortieEleve + ')')
    // on regarde si le test élève s’exécute
    let diagnostique
    if (sortieEleve.includes('Error')) {
      diagnostique = textes.pythonErreur
      for (k = 0; k < Object.keys(entrees).length; k++) {
        cle = Object.keys(entrees)[k]
        valeur = Object.values(entrees)[k]
        diagnostique = diagnostique + ' ' + cle + '=' + valeur
      }
      j3pElement('output').innerHTML = diagnostique + '\n' + j3pElement('output').innerHTML
      return false
    }
    // on compare les 2 exécutions
    sortieSecret = sortieSecret.split('\n').join('')
    sortieSecret = sortieSecret.split('<br>').join('')
    sortieSecret.replace(/\r\n/g, '')
    sortieEleve = sortieEleve.split('\n').join('')
    sortieEleve = sortieEleve.split('<br>').join('')
    sortieEleve.replace(/\r\n/g, '')
    let identique = true
    switch (typeSortie) {
      case 'number':
        identique = (Math.abs(Number(sortieSecret) - Number(sortieEleve)) < Math.pow(10, -12))
        break
      case 'string':
        identique = (sortieSecret === sortieEleve)
        break
    }

    if (identique) {
      j3pElement('output').innerHTML = ''
      return true
    }
    // on explique l’échec
    diagnostique = textes.pythonDiag1
    for (k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      diagnostique = diagnostique + ' ' + cle + '=' + valeur
    }
    diagnostique = diagnostique + '\n' + textes.pythonDiag2 + sortieEleve
    diagnostique = diagnostique + '\n' + textes.pythonDiag3 + sortieSecret
    j3pElement('output').innerHTML = diagnostique + '\n' + j3pElement('output').innerHTML
    return false
  }

  function activerClasseCode () {
    // Cette fonction permet d’appliquer le style ci-dessous aux classes code
    const lecode = $('.code')
    lecode.css('font-family', 'Lucida Console')
    lecode.css('font-size', '0.8em')
    lecode.css('background-color', '#FFFFFF')
    lecode.css('color', '#000000')
  }

  function creationEditeurPython (obj) {
    // obj est un objet contenant 2 propriétés:
    // hautEditeur (par défaut 200)
    // largPour (par défaut 95), c’est en pourcentage
    // hautConsole (par défaut 22)
    // boolConsole vaut true par défaut. Lorsqu’il vaut false, alors on ne voit pas la console
    const h = (isNaN(Number(obj.hautEditeur))) ? 200 : obj.hautEditeur
    const l = (isNaN(Number(obj.largPour))) ? 95 : obj.largPour
    const h2 = (isNaN(Number(obj.hautConsole))) ? 22 : obj.hautConsole
    const boolConsole = (obj.boolConsole === undefined) ? true : obj.boolConsole
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    // je m’occupe d’abord de la construction de l’éditeur
    j3pAffiche(stor.zoneCons1, '', textes.editeur)
    stor.idEditor = j3pGetNewId('idEditor')
    stor.divEditor = j3pAddElt(stor.zoneCons2, 'div', '', { id: stor.idEditor })
    j3pStyle(stor.divEditor, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    stor.editor = ace.edit(stor.idEditor)
    stor.divEditor.style.height = String(h) + 'px'
    stor.divEditor.style.width = String(l) + '%'
    stor.editor.resize()
    stor.editor.getSession().setUseSoftTabs(false)
    // stor.editor.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    stor.editor.getSession().setMode('ace/mode/python')
    stor.editor.setFontSize('12pt')
    stor.editor.focus()
    // et maintenant, place à la console
    j3pAffiche(stor.zoneCons3, '', textes.console)
    stor.idConsole = j3pGetNewId('idConsole')
    stor.divConsole = j3pAddElt(stor.zoneCons4, 'div', '', { id: stor.idConsole })
    j3pStyle(stor.divConsole, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    laConsole = ace.edit(stor.idConsole)
    stor.divConsole.style.height = String(h2) + 'px'
    stor.divConsole.style.width = String(l) + '%'
    stor.divConsole.style.marginTop = '2px'
    stor.divConsole.style.marginBottom = '2px'
    laConsole.getSession().setMode('ace/mode/python')
    laConsole.setFontSize('12pt')
    try {
      // J’ai eu un signalement où cet objet HTML n’existait pas ce qui faisait planter la section
      // Je ne vois pas comment ce peut être possible
      const tabAce = document.getElementsByClassName('ace_gutter')
      const ligne1Console = tabAce[1].childNodes[0].childNodes[0]
      ligne1Console.innerHTML = '>>>'
      ligne1Console.style.paddingLeft = '10px'
    } catch (e) {
      // Je vire la console car il y a un pb pour son affichage :
      j3pDetruit(stor.zoneCons3)
      j3pDetruit(stor.divConsole)
      console.warn('soucis avec l’élément HTML censé contenir les chevrons')
    }
    if (!boolConsole) {
      stor.zoneCons3.style.visibility = 'hidden'
      stor.zoneCons4.style.visibility = 'hidden'
    }
    stor.divBtns = j3pAddElt(stor.zoneCons5, 'div')
    stor.divBtns.style.textAlign = 'right'
    stor.idExecuter = j3pGetNewId('executer')
    stor.idReinitialiser = j3pGetNewId('reinitialiser')
    j3pAjouteBouton(stor.divBtns, stor.idExecuter, 'MepBoutons', textes.executer, '')
    j3pAjouteBouton(stor.divBtns, stor.idReinitialiser, 'MepBoutons', textes.reinitialiser, '')
    j3pElement(stor.idExecuter).addEventListener('click', execute)
    // Et enfin la sortie de l’algo
    j3pStyle(stor.zoneCons6, { fontSize: '0.9em', fontStyle: 'italic' })
    const underline = j3pAddElt(stor.zoneCons7, 'u')
    j3pAddTxt(underline, textes.sortie)
    stor.output = 'output'
    stor.pre = j3pAddElt(stor.zoneCons7, 'pre', { id: stor.output })
    j3pStyle(stor.pre, { color: 'black' })
  }

  function afficheCorrection (bonneReponse) {
    const zoneExpli = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1)
    stor.nameProg = j3pGetNewId('Programme')
    stor.fenetreCorr = j3pGetNewId('fenetreCorr')
    me.fenetresjq = [{
      name: stor.nameProg,
      title: textes.correctionAlgo,
      top: 110,
      left: 5,
      width: 450,
      id: stor.fenetreCorr
    }]
    j3pCreeFenetres(me)
    const algoCorr = j3pAddElt(stor.fenetreCorr, 'div', '', { style: me.styles.etendre('petit.explications', { fontSize: '1.2em' }) })
    const tabRep = stor.algoReponse.split('\n')
    const espaceTab = '&nbsp;&nbsp;&nbsp;&nbsp;'
    for (let i = 0; i < tabRep.length - 1; i++) {
      try {
        tabRep[i] = tabRep[i].replace(/\t/g, espaceTab)
        j3pAffiche(algoCorr, '', tabRep[i])
        j3pAddContent(algoCorr, '<br>')
      } catch (e) {
        console.warn(e) // pas normal mais pas grave
      }
    }
    j3pToggleFenetres(stor.nameProg)
    j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
    j3pDetruit(stor.idReinitialiser)
  }

  function enonceInit () {
    me.validOnEnter = false // ex donneesSection.touche_entree
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    stor.tabQuest = [1, 2, 3, 4, 5, 6]
    stor.tabQuestInit = [...stor.tabQuest]
    init()
      .then(enonceMain)
      .catch(j3pShowError)
  }

  function enonceMain () {
    if (stor.tabQuest.length === 0) {
      stor.tabQuest = [...stor.tabQuestInit]
    }
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.borneIneq = 0
    stor.quest = quest
    const objVar = {}
    switch (quest) {
      case 1:
        objVar.p = j3pGetRandomInt(72, 82)
        objVar.ligne2 = 'if random()<=' + (objVar.p / 100) + ':\n'
        objVar.ligneBis2 = 'if random()>=' + (1 - objVar.p / 100) + ':\n'
        stor.borneIneq = objVar.p / 100
        break
      case 2:
        do {
          objVar.p = j3pGetRandomInt(43, 57)
        } while (Math.abs(objVar.p - 50) < Math.pow(10, -12))
        objVar.ligne2 = 'if random()<=' + (objVar.p / 100) + ':\n'
        objVar.ligneBis2 = 'if random()>=' + (1 - objVar.p / 100) + ':\n'
        stor.borneIneq = objVar.p / 100
        break
      case 3:
        objVar.e1 = j3pGetRandomInt(31, 35)
        objVar.e2 = Math.round(objVar.e1 / 2) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(1, 5)
        objVar.ligne2 = 'if random()<=' + objVar.e2 + '/' + objVar.e1 + ':\n'
        objVar.ligneBis2 = 'if random()>=1-' + objVar.e2 + '/' + objVar.e1 + ':\n'
        stor.borneIneq = objVar.e2 / objVar.e1
        break
      case 4:
        do {
          objVar.p = j3pGetRandomInt(35, 57)
        } while (Math.abs(objVar.p - 50) < Math.pow(10, -12))
        objVar.ligne2 = 'if random()<=' + (objVar.p / 100) + ':\n'
        objVar.ligneBis2 = 'if random()>=' + (1 - objVar.p / 100) + ':\n'
        stor.borneIneq = objVar.p / 100
        break
      case 5:
        objVar.e1 = j3pGetRandomInt(25, 50)
        objVar.e2 = Math.max(Math.round(objVar.e1 / 4) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(1, 2), 3)
        objVar.ligne2 = 'if random()<=' + objVar.e2 + '/' + objVar.e1 + ':\n'
        objVar.ligneBis2 = 'if random()>=1-' + objVar.e2 + '/' + objVar.e1 + ':\n'
        stor.borneIneq = objVar.e2 / objVar.e1
        break
      case 6:
        objVar.e1 = j3pGetRandomInt(20, 35)
        objVar.e2 = Math.round(objVar.e1 / 2) + j3pGetRandomInt(1, 7)
        objVar.ligne2 = 'if random()<=' + objVar.e2 + '/' + objVar.e1 + ':\n'
        objVar.ligneBis2 = 'if random()>=1-' + objVar.e2 + '/' + objVar.e1 + ':\n'
        stor.borneIneq = objVar.e2 / objVar.e1
        break
    }

    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    for (let i = 1; i <= 3; i++) stor['zoneConsD' + i] = j3pAddElt(stor.conteneurD, 'div')
    j3pAffiche(stor.zoneConsD1, '', textes.consigne1)
    activerClasseCode()
    j3pAffiche(stor.zoneConsD2, '', '\n' + textes['consigne2_' + quest], objVar)
    j3pAffiche(stor.zoneConsD3, '', textes['consigne3_' + quest])

    const tabAlgoInit = []
    tabAlgoInit.push('from random import *\n')
    tabAlgoInit.push('if \n')
    const tabPrint = textes['print' + quest].split('|')
    tabAlgoInit.push('\tprint("' + tabPrint[0] + '")\n')
    tabAlgoInit.push('else :\n')
    tabAlgoInit.push('\tprint("' + tabPrint[1] + '")\n')

    stor.algoInit = tabAlgoInit.join('')
    const tabAlgoRep = [...tabAlgoInit]
    tabAlgoRep[1] = objVar.ligne2

    stor.tabAlgoInit = tabAlgoInit
    stor.algoReponse = tabAlgoRep.join('')
    // Mais on a aussi une bonne réponse avec une inégalité de la forme >=
    const tabAlgoRep2 = tabAlgoRep.slice()
    tabAlgoRep2[1] = objVar.ligneBis2
    stor.algoReponse2 = tabAlgoRep2.join('')
    me.logIfDebug('algoReponse:', stor.algoReponse, ' et ', stor.algoReponse2)
    creationEditeurPython({ hautEditeur: 130, hautConsole: 20, boolConsole: false })
    setTextAce(stor.editor, stor.algoInit)
    // consoleInit="";
    // setTextAce(laConsole,consoleInit);
    j3pElement(stor.idReinitialiser).addEventListener('click', reinitialiser)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvent dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    j3pStyle(stor.zoneCorr, { paddingTop: '10px' })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const reponse = { aRepondu: false, bonneReponse: false }
        let randomPresent = true
        let lignesInchangees = true
        stor.algoEleveInit = getTextAce(stor.editor)
        reponse.aRepondu = (stor.algoInit !== stor.algoEleveInit)
        let objDeuxPts, inegaliteLarge
        if (reponse.aRepondu) {
          objDeuxPts = verifDeuxPoints(stor.algoEleveInit)
          me.logIfDebug('objDeuxPts:', objDeuxPts)
          stor.algoEleveInit = stor.algoEleveInit.replace('random ()', 'random()')
          if (objDeuxPts.presents) {
          // on vérifie que seule la deuxième ligne a été modifiée
            const tabCode = stor.algoEleveInit.split('\n')
            let posIf = -1
            for (let i = 0; i < tabCode.length; i++) {
              if (tabCode[i].indexOf('if') === 0) {
                posIf = i
                break
              }
            }
            if (posIf === -1) {
              lignesInchangees = false
            }
            if (lignesInchangees) {
              let k = 0
              while (k < tabCode.length) {
                if (tabCode[k].replace(/\t/g, '').replace(/\n/g, '') === '') tabCode.splice(k, 1)
                else k++
              }
              let i2 = 0 // au cas où il y aurait un décalage sur le code élève (s’ils s’amusent à mettre une variable)
              for (let i = 0; i < tabCode.length; i++) {
                if (i === 1) {
                // traitement différent car c’est la ligne à changer. On vérifie que random() est présent
                // Mais on peut très bien mettre la valeur choisie aléatoirement dans une variable
                  const ligneSansEspace = tabCode[i].replace(/\s/g, '')
                  const posEgal = ligneSansEspace.indexOf('=random()')
                  if (posEgal > -1) {
                  // On a déclaré une variable dont je vais récupérer le nom et remplacer dans la ligne suivante la variable par la valeur
                    const monNomVar = ligneSansEspace.substring(0, posEgal)
                    const valeurRandom = ligneSansEspace.substring(posEgal + 1)
                    i++ // je passe à la ligne suivante
                    tabCode[i] = tabCode[i].replace(monNomVar, '(' + valeurRandom + ')')
                  }
                  randomPresent = (tabCode[i].indexOf('random()') > -1)
                  inegaliteLarge = ((tabCode[i].indexOf('<=') > -1) || (tabCode[i].indexOf('>=') > -1))
                } else {
                  if (stor.tabAlgoInit[i2]) {
                    if (stor.tabAlgoInit[i2].replace(/\t/g, '').replace(/\n/g, '').replace(/\s/g, '') !== tabCode[i].replace(/\t/g, '').replace(/\s/g, '')) {
                      lignesInchangees = false
                    }
                  } else {
                    lignesInchangees = false
                  }
                }
                i2++
              }
            }
            if (lignesInchangees) {
              reponse.bonneReponse = (randomPresent)
              if (reponse.bonneReponse) {
                const tabValTestees = []
                for (let i = 1; i <= 6; i++) {
                  tabValTestees.push(stor.borneIneq - Math.pow(10, -i - 2))
                  tabValTestees.push(stor.borneIneq + Math.pow(10, -i - 2))
                }
                let numTests = 0
                const typeRep = 'string'
                let algoTeste = stor.algoReponse.slice()// On départ je fais le test sur l’algo qui m’a paru le plus naturel
                while (reponse.bonneReponse && numTests < tabValTestees.length) {
                  const algoEleve = stor.algoEleveInit.replace('random()', tabValTestees[numTests])
                  const algoReponse = algoTeste.replace('random()', tabValTestees[numTests])
                  reponse.bonneReponse = verifierCodePython(algoEleve, algoReponse, {}, typeRep)
                  if (numTests === 0 && !reponse.bonneReponse) {
                  // Peut-être que l’élève a choisi une inégalité de la forme >= et non <= donc il faut que je compare avec stor.algoReponse2
                    algoTeste = stor.algoReponse2.slice()
                    const algoReponse2 = algoTeste.replace('random()', tabValTestees[numTests])
                    reponse.bonneReponse = verifierCodePython(algoEleve, algoReponse2, {}, typeRep)
                  }
                  numTests++
                }
              }
            } else {
              reponse.aRepondu = false
            }
          }
        }

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          stor.zoneCorr.style.color = me.styles.cfaux
          // On peut ajouter un commentaire particulier.
          if (!lignesInchangees) {
            stor.zoneCorr.innerHTML = textes.comment5
          } else {
            stor.zoneCorr.innerHTML = textes.comment1
          }
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            if (!inegaliteLarge) {
              stor.zoneCorr.innerHTML += '<br>' + textes.comment4
            }
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            // afficheCorrection(true);
            desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
            j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
            j3pDetruit(stor.idReinitialiser)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!objDeuxPts.presents) {
                stor.zoneCorr.innerHTML += '<br>' + j3pChaine(textes.comment2, { l: objDeuxPts.numLigne })
              } else if (!randomPresent) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment3
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        me.buttonsElts.sectionSuivante?.addEventListener('click', j3pDetruitToutesLesFenetres)
        j3pElement(stor.idExecuter).removeEventListener('click', execute)
      } else {
        me.buttonsElts.suite?.addEventListener('click', j3pDetruitToutesLesFenetres)
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
