import $ from 'jquery'

import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pArrondi, j3pChaine, j3pGetRandomElts, j3pDetruit, j3pElement, j3pGetNewId, j3pGetRandomInt, j3pNombre, j3pShowError, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { init, desactiverEditeur, getTextAce, runPython, runPythonCode, setTextAce, verifDeuxPoints } from 'src/legacy/outils/algo/algo_python'
import { testIntervalleFermeDecimaux } from 'src/lib/utils/regexp'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexOppose, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juin 2019
        section sur les listes : créeation d’une liste avec la boucle Pour
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[2;7]|[0.2;0.9]', 'string', 'Intervalle où se trouve la valeur a dans l’expression u_{n+1}=au_n+b (bien sûr a non nul différent de 1). En imposant "[-1;1]", on aura un décimal de cet intervalle (bornes non acceptées). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[0.2;0.9]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis décimale de l’intervalle [0.2;0.9]. À la place d’un intervalle, on peut aussi mettre un nombre entier ou décimal.'],
    ['b', '[-6;6]', 'string', 'Intervalle où se trouve la valeur b dans l’expression u_{n+1}=au_n+b (b non nul). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. À la place d’un intervalle, on peut aussi mettre un nombre entier ou décimal.'],
    ['termeInitial', ['0|[-5;5]', '0|[-5;5]'], 'array', 'Tableau de taille nbrepetitions où chaque terme contient deux éléments séparés par | : le premier est l’indice du premier terme et le second sa valeur (imposée ou aléatoire dans un intervalle).'],
    ['formatFonction', false, 'boolean', 'Lorsque ce paramètre vaut true, l’algorithme est demandé sous la forme d’une fonction'],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la suite étudiée est celle du nœud précédent.']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Algorithme de seuil sur les suites',
  // on donne les phrases de la consigne
  consigne1: 'Soit $(£u_n)$ la suite définie par $£u_{£n}=£i$ et, pour tout entier $n\\geq £n$, par $£u_{n+1}=£j$.',
  consigne2_1: 'On a conjecturé que la suite $(£u_n)$ converge vers $£l$.',
  consigne2_2: 'On a conjecturé que la suite $(£u_n)$ diverge vers $£l$.',
  consigne3: "Compléter l’algorithme ci-contre pour qu'à la fin n soit le premier rang à partir duquel on ait $£{cond}$.",
  consigneBis3: 'Compléter la fonction ci-contre pour qu’elle renvoie le premier rang à partir duquel on ait $£{cond}$.',
  consigne4: 'Ne pas modifier le nom des variables déjà présentes (£u et n).',
  utiliserConsole: 'Ne pas hésiter à utiliser la console pour tester le programme.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'L’algorithme doit être complété&nbsp;!',
  comment2: 'Le début du programme n’aurait pas dû être modifié&nbsp;!<br/>Reprendre la version initiale&nbsp;!',
  comment3: 'À la fin de chaque ligne contenant un mot clé, on devrait avoir deux points (par exemple ligne £l)&nbsp;!',
  comment4: 'La seule boucle permettant de répondre à la question n’est pas présente&nbsp;!',
  comment5: 'La condition présente dans la boucle while n’est pas correcte&nbsp;!',
  comment6: 'Le programme doit commencer par la création d’une fonction (à l’aide du mot clé "def")&nbsp;!',
  comment7: 'Nous sommes en présence d’une boucle infinie&nbsp;!',
  comment8: 'Pour indenter les lignes, il faut ici utiliser des tabulations et non des espaces',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Voici dans la fenêtre ci-contre un programme répondant à la question.',
  correctionAlgo: 'Programme', // possible",
  pythonErreur: 'Ton programme a bogué pour',
  // pythonDiag1 : "Erreur ligne £l : le programme donne £r alors qu’on devrait avoir £{rep}.",
  pythonDiag1: 'Erreur lors du test de £{nomf}()&nbsp;:\n',
  pythonDiagBis1: 'Erreur lors du test&nbsp;:\n',
  pythonDiag2: 'Avec ton code, on obtient&nbsp;: ',
  pythonDiag3: 'alors qu’on devrait obtenir&nbsp;: ',
  sortie: 'Sortie',
  executer: 'Exécuter',
  reinitialiser: 'Réinitialiser',
  editeur: 'Editeur :',
  console: 'Console :',
  bclInfinie: 'Boucle infinie'
}

/* global  ace */

/**
 * section algoSeuilSuite
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let laConsole
  const ds = me.donneesSection

  function execute () {
    // Il faut que je gère l’éventuel bug avec une boucle infinie
    stor.boucleInf = false
    let newEditor = stor.editor.getValue()
    // console.log('newEditor:', newEditor)
    if (newEditor.includes('while')) {
      // Il faut que je fasse attention à ne pas être dans une boucle infinie
      // console.log(newEditor)
      const tabLigne = newEditor.split('\n')
      for (let k = 0; k < tabLigne.length; k++) {
        if (tabLigne[k].includes('while')) {
          const tab = tabLigne[k].split('while')[0]// Je récupère le nombre de tabulations de cette boucle while (cela dépend s’il y a une fonction ou non)
          let remplacementWhile = tab + 'nbtest=0\n'
          remplacementWhile += tabLigne[k] + '\n'
          remplacementWhile += tab + '\tnbtest=nbtest+1\n'
          remplacementWhile += tab + '\tif nbtest>1000:\n'
          if (ds.formatFonction) {
            remplacementWhile += tab + '\t\treturn "' + textes.bclInfinie + '"\n'
          } else {
            remplacementWhile += tab + '\t\tbreak\n'
          }
          newEditor = newEditor.replace(tabLigne[k], remplacementWhile)
          newEditor += '\n' + tab + 'if nbtest>1000:\n'
          newEditor += tab + '\tprint("' + textes.bclInfinie + '")\n'
          runPythonCode(newEditor)
        }
      }
      if (laConsole) {
        if (ds.formatFonction && laConsole.getValue().includes('seuil()')) {
          newEditor += 'print(seuil())'
        }
      } else {
        if (ds.formatFonction) {
          newEditor += 'print(seuil())'
        }
      }
      // console.log("newEditor:",newEditor)
      runPythonCode(newEditor)
      stor.boucleInf = (document.getElementById('output').innerHTML.includes(textes.bclInfinie))
    }
    if (!stor.boucleInf) {
      if (ds.formatFonction) {
        runPython(stor.editor, laConsole)
      } else {
        runPython(stor.editor, false, false)
      }
    }
  }

  function reinitialiser () {
    setTextAce(stor.editor, stor.algoInit)
  }

  function verifierCodePython (algoEleve, algoSecret, entrees, typeSortie) {
    let cle, valeur
    me.logIfDebug('algoEleve=', algoEleve, '\nalgoSecret=', algoSecret, '\nentrees=', entrees)
    let lignes, ligne
    // on ajoute les entrees à tester
    let debutTest = ''
    for (let k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      debutTest = debutTest + cle + ' = ' + valeur + '\n'
    }
    me.logIfDebug('debutTest', debutTest, '\nalgoEleve avant', algoEleve)
    lignes = algoEleve.split('\n')
    let algoEleveTest = debutTest
    for (let i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.indexOf('input(') >= 0) continue
      algoEleveTest = algoEleveTest + ligne + '\n'
    }
    me.logIfDebug('algoEleve apres', algoEleveTest, '\nalgo secret avant', algoSecret)
    lignes = algoSecret.split('\n')
    let algoSecretTest = debutTest
    for (let i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.indexOf('input(') >= 0) continue
      algoSecretTest = algoSecretTest + ligne + '\n'
    }
    me.logIfDebug('algo secret apres', algoSecretTest)
    // on exécute algoSecretTest
    runPythonCode(algoSecretTest)
    let sortieSecret = j3pElement('output').innerHTML
    me.logIfDebug('sortieSecret=(', sortieSecret + ')')
    // on exécute algoEleveTest
    runPythonCode(algoEleveTest)
    let sortieEleve = j3pElement('output').innerHTML
    me.logIfDebug('sortieEleve=(', sortieEleve + ')')
    // on regarde si le test élève s’exécute
    let diagnostique
    if (sortieEleve.indexOf('Error') !== -1) {
      diagnostique = textes.pythonErreur
      for (let k = 0; k < Object.keys(entrees).length; k++) {
        cle = Object.keys(entrees)[k]
        valeur = Object.values(entrees)[k]
        diagnostique = diagnostique + ' ' + cle + '=' + valeur
      }
      j3pElement('output').innerHTML = diagnostique + '\n' + j3pElement('output').innerHTML
      return false
    }
    // on compare les 2 exécutions
    sortieSecret = sortieSecret.replace(/\r\n/g, '')
    sortieEleve = sortieEleve.replace(/\r\n/g, '')
    let identique = true
    switch (typeSortie) {
      case 'number':
        identique = (Math.abs(Number(sortieSecret) - Number(sortieEleve)) < Math.pow(10, -12))
        break
      case 'string':
        // l’égalité de la sortie pose pb.
        // Je teste donc si à la fin j’ai bien la sortie attendue
        {
          const posSortieSecret = sortieEleve.toLowerCase().lastIndexOf(sortieSecret)
          identique = (posSortieSecret > -1)
          if (identique) {
          // on vérifie que c’est la dernière chose qui soit affichée
            identique = (sortieEleve.substring(posSortieSecret + sortieSecret.length).length === 0)
          }
        }
        break
    }

    if (identique) {
      j3pElement('output').innerHTML = ''
      return true
    }
    // on explique l’échec
    if (ds.formatFonction) {
      diagnostique = j3pChaine(textes.pythonDiag1, { nomf: 'seuil' })
    } else {
      diagnostique = textes.pythonDiagBis1
    }
    for (let k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      diagnostique = diagnostique + ' ' + cle + '=' + valeur
    }
    diagnostique = diagnostique + '\n' + textes.pythonDiag2 + '\n' + sortieEleve
    diagnostique = diagnostique + '\n' + textes.pythonDiag3 + '\n' + sortieSecret
    j3pElement('output').innerHTML = diagnostique
    return false
  }

  function creationEditeurPython (obj) {
    // obj est un objet contenant 2 propriétés:
    // hautEditeur (par défaut 200)
    // largPour (par défaut 95), c’est en pourcentage
    // hautConsole (par défaut 22)
    const h = (isNaN(Number(obj.hautEditeur))) ? 200 : obj.hautEditeur
    const l = (isNaN(Number(obj.largPour))) ? 95 : obj.largPour
    const h2 = (isNaN(Number(obj.hautConsole))) ? 22 : obj.hautConsole
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    // je m’occupe d’abord de la construction de l’éditeur
    j3pAffiche(stor.zoneCons1, '', textes.editeur)
    stor.idEditor = j3pGetNewId('idEditor')
    stor.divEditor = j3pAddElt(stor.zoneCons2, 'div', '', { id: stor.idEditor })
    j3pStyle(stor.divEditor, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    stor.editor = ace.edit(stor.idEditor)
    stor.divEditor.style.height = String(h) + 'px'
    stor.divEditor.style.width = String(l) + '%'
    stor.editor.resize()
    stor.editor.getSession().setUseSoftTabs(false)
    // stor.editor.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    stor.editor.getSession().setMode('ace/mode/python')
    stor.editor.setFontSize('12pt')
    stor.editor.focus()
    // Sk.editor = stor.editor
    // et maintenant, place à la console
    let avecConsole = ds.formatFonction
    if (ds.formatFonction) {
      stor.idConsole = j3pGetNewId('idConsole')
      stor.divConsole = j3pAddElt(stor.zoneCons4, 'div', '', { id: stor.idConsole })
      j3pStyle(stor.divConsole, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
      laConsole = ace.edit(stor.idConsole)
      stor.divConsole.style.height = String(h2) + 'px'
      stor.divConsole.style.width = String(l) + '%'
      stor.divConsole.style.marginTop = '2px'
      stor.divConsole.style.marginBottom = '2px'
      // laConsole.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
      laConsole.getSession().setMode('ace/mode/python')
      laConsole.setFontSize('12pt')

      /* console.log(stor.divConsole.childNodes)
      console.log(stor.divConsole.childNodes[1].childNodes)
      console.log(stor.divConsole.childNodes[1].childNodes[0].childNodes) */
      try {
        // J’ai eu un signalement où cet objet HTML n’existait pas ce qui faisait planter la section
        // Je ne vois pas comment ce peut être possible
        // stor.divConsole.childNodes[1].childNodes[0].childNodes[0].innerHTML = '>>>'
        // stor.divConsole.childNodes[1].childNodes[0].childNodes[0].style.paddingLeft = '10px'
        const tabAce = document.getElementsByClassName('ace_gutter')
        const ligne1Console = tabAce[1].childNodes[0].childNodes[0]
        ligne1Console.innerHTML = '>>>'
        ligne1Console.style.paddingLeft = '10px'
      } catch (e) {
        avecConsole = false
        // Je vire la console car il y a un pb pour son affichage :
        j3pDetruit(stor.zoneCons3)
        j3pDetruit(stor.divConsole)
        console.warn('soucis avec l’élément HTML censé contenir les chevrons')
      }
    }
    stor.divBtns = j3pAddElt(stor.zoneCons5, 'div')
    stor.divBtns.style.textAlign = 'right'
    stor.idExecuter = j3pGetNewId('executer')
    stor.idReinitialiser = j3pGetNewId('reinitialiser')
    j3pAjouteBouton(stor.divBtns, stor.idExecuter, 'MepBoutons', textes.executer, '')
    j3pAjouteBouton(stor.divBtns, stor.idReinitialiser, 'MepBoutons', textes.reinitialiser, '')
    j3pElement(stor.idExecuter).addEventListener('click', execute)
    // Et enfin la sortie de l’algo
    if (ds.formatFonction && avecConsole) {
      j3pAffiche(stor.zoneCons3, '', textes.console)
      j3pAffiche(stor.zoneCons6, '', textes.utiliserConsole, { L: stor.nomListe })
      j3pStyle(stor.zoneCons6, { fontSize: '0.9em', fontStyle: 'italic' })
    }
    const underline = j3pAddElt(stor.zoneCons7, 'u')
    j3pAddTxt(underline, textes.sortie)
    stor.output = 'output'
    stor.pre = j3pAddElt(stor.zoneCons7, 'pre', { id: stor.output })
    j3pStyle(stor.pre, { color: 'black' })
  }

  function afficheCorr (bonneReponse) {
    const zoneExpli = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1)
    stor.nameProg = j3pGetNewId('Programme')
    stor.fenetreCorr = j3pGetNewId('fenetreCorr')
    me.fenetresjq[0] = {
      name: stor.nameProg,
      title: textes.correctionAlgo,
      top: 110,
      left: 5,
      width: 450,
      id: stor.fenetreCorr
    }
    j3pCreeFenetres(me)
    const algoCorr = j3pAddElt(stor.fenetreCorr, 'div', '', { style: me.styles.etendre('petit.explications', { fontSize: '1.2em' }) })
    algoCorr.className = 'code1'
    activerClasseCode()
    const tabRep = stor.algoReponse.split('\n')
    const espaceTab = '&nbsp;&nbsp;&nbsp;&nbsp;'
    for (let i = 0; i < tabRep.length; i++) {
      try {
        // console.log('tabRep[i]:', tabRep[i])
        tabRep[i] = tabRep[i].replace(/\t/g, espaceTab)
        j3pAffiche(algoCorr, '', tabRep[i])
        j3pAddContent(algoCorr, '<br>')
      } catch (e) {
        console.warn(e) // pas normal mais pas grave
      }
    }
    j3pToggleFenetres(stor.nameProg)
    j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
    j3pDetruit(stor.idReinitialiser)
  }

  function nettoyageCorrection () {
    j3pDetruitFenetres(stor.nameProg)
  }

  function activerClasseCode () {
    // Cette fonction permet d’appliquer le style ci-dessous aux classes code1 et code2
    const queryCode1 = $('.code1')
    const queryCode2 = $('.code2')
    queryCode1.css('font-family', 'Lucida Console')
    queryCode1.css('font-size', '1.2em')
    queryCode1.css('background-color', '#FFFFFF')
    queryCode1.css('color', '#000000')
    queryCode2.css('font-family', 'Lucida Console')
    queryCode2.css('font-size', '0.8em')
    queryCode2.css('background-color', '#FFFFFF')
    queryCode2.css('color', '#000000')
  }

  function genereAlea (int) {
    // int est un intervalle
    // mais on peut trouver des cas de figure où par faignantise, on aura utilisé cette fonction sans vérifier que int est un intervalle
    // Ce serait alors un nombre
    if (String(int).charAt(0) === '[') {
      // si les bornes sont entières, alors on génère un nombre entier aléatoire dans cet intervalle
      // si elles sont décimales, il faut que je génère un décimal de l’intervalle
      const PosPointVirgule = int.indexOf(';')
      let val1 = j3pNombre(int.substring(1, PosPointVirgule))
      let val2 = j3pNombre(int.substring(PosPointVirgule + 1, int.length - 1))
      let posPoint = -1
      if ((!String(val1).includes('.'))) {
        if ((String(val2).includes('.'))) {
          posPoint = String(val2).length - String(val2).lastIndexOf('.') - 1
        }
      } else {
        if ((String(val2).includes('.'))) {
          posPoint = Math.max(String(val1).length - String(val1).lastIndexOf('.') - 1, String(val2).length - String(val2).lastIndexOf('.') - 1)
        } else {
          posPoint = String(val1).length - String(val1).lastIndexOf('.') - 1
        }
      }
      if (val1 > val2) {
        // les bornes ne sont pas dans le bon ordre
        const val3 = val1
        val1 = val2
        val2 = val3
      }
      if (posPoint === -1) {
        // je n’ai que des nombres entiers
        return j3pGetRandomInt(val1, val2)
      } else {
        // j’ai des décimaux
        const borneInf = val1 * Math.pow(10, posPoint)
        const borneSup = val2 * Math.pow(10, posPoint)
        const nbAlea = j3pGetRandomInt(String(borneInf), String(borneSup)) * Math.pow(10, -posPoint)
        return j3pArrondi(nbAlea, 10)
      }
    } else {
      return int
    }
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut, nbRepet, tabValInterdites) {
    // cette fonction permet de vérifier si ce qui est renseigné est bien un intervalle ou un nombre (éventuellement fractionnaire) et ce plusieurs fois (en général nbrepetitions)
    // cela permet de valider ce qui peut être saisi par un utilisateur dans le cas où on peut imposer des valeurs
    // texteIntervalle est une chaîne où les éléments peuvent être séparés par '|' et où chaque élément est un nombre, une fraction ou un intervalle
    // si c’est un intervalle, la fonction va dans un second temps générer de manière aléatoire une valeur de cet intervalle
    // il peut y avoir des valeurs interdites précisées dans tabValInterdites, même si elles peuvent être dans l’intervalle
    // la fonction renvoie un tableau de nbRepet éléments
    // chaque élément est un intervalle voire l’intervalle par défaut si ce qui est précisé n’est aps compris
    // ce peut aussi être un nombre entier, décimal ou fractionnaire
    // dans ce dernier cas, on renvoit une chaine où on retrouve le code latex et la valeur décimale (arrondie) séparés par |

    /* cette fonction renvoie un objet. Cet objet contient 3 tableaux de nbRepet éléments
        - leType[k] vaut 'nombre', 'fraction' ou 'intervalle' suivant la valeur de texteIntervalle
        - expression contient l’écriture de chaque valeur imposée ou gérée aléatoirement quand leType[k] vaut 'intervalle'
        - valeur contient la valeur décimale de ces nombres, cela ne peut être utile que lorsque leType[k] vaut 'fraction'
        */
    function lgIntervalle (intervalle) {
      // cette fonction renvoie la longueur de l’intervalle'
      // si ce n’est pas un intervalle, alors elle renvoie 0'
      let lg = 0
      // var intervalleReg = new RegExp("\\[\\-?[0-9]{1,};\\-?[0-9]{1,}\\]","i");
      // const intervalleReg = testIntervalleFermeDecimaux // new RegExp('\\[\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,};\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,}\\]', 'i')
      if (intervalleReg.test(intervalle)) {
        // on a bien un intervalle
        const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
        const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
        lg = borneSup - borneInf
      }
      return lg
    }

    function nbDansTab (nb, tab) {
      // test si la valeur correspondant à nb est dans le tableau tab (ses valeurs pouvant être au format latex)
      if (tab === undefined) {
        return false
      } else {
        let puisPrecision = 15
        if (Math.abs(nb) > Math.pow(10, -13)) {
          const ordrePuissance = String(Math.abs(nb)).indexOf('.')
          puisPrecision = (ordrePuissance === -1) ? 15 - String(Math.abs(nb)).length : 15 - ordrePuissance
        }
        for (let k = 0; k < tab.length; k++) {
          if (Math.abs(nb - j3pCalculValeur(tab[k])) < Math.pow(10, -puisPrecision)) {
            return true
          }
        }
        return false
      }
    }

    const intervalleReg = testIntervalleFermeDecimaux // new RegExp('\\[\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,};\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,}\\]', 'i')
    // var nbFracReg = new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    const nbFracReg = /-?[0-9]+\/[0-9]+/g
    let k, nbAlea, valNb
    let tabDecomp, monnum, monden, valFrac
    const DonneesIntervalle = {}
    // DonneesIntervalle contient trois propriétés qui sont des tableaux de longueur nbRepet :
    // - leType : qui correspond à ce qui a été renseigné initialement ('intervalle', 'nombre', 'fraction') - 'nombre' désigne un entier ou un décimal
    // - expression : qui est le nombre (si type==='nombre'), une valeur générée aléatoirement dans un intervalle (si type==='intervalle'), l’écriture l’atex de la fraction (si type==='fraction')
    // - valeur : qui est la valeur décimale de l’expression (utile surtout si type==='fraction')
    DonneesIntervalle.leType = []
    DonneesIntervalle.expression = []
    DonneesIntervalle.valeur = []
    if (String(texteIntervalle).indexOf('|') > -1) {
      const tableau = texteIntervalle.split('|')
      for (k = 0; k < nbRepet; k++) {
        if (intervalleReg.test(tableau[k])) {
          // c’est bien un intervalle'
          if (lgIntervalle(tableau[k]) === 0) {
            // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
            valNb = tableau[k].substring(1, tableau[k].indexOf(';'))
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          } else {
            do {
              nbAlea = genereAlea(tableau[k])
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        } else {
          // peut être est-ce tout simplement un nombre :
          if ((tableau[k] === undefined) || (tableau[k] === '')) {
            // là l’expression n’est pas renseignée
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          } else if (nbFracReg.test(tableau[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tableau[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
            DonneesIntervalle.leType.push('fraction')
            DonneesIntervalle.valeur.push(valFrac)
          } else if (!isNaN(j3pNombre(tableau[k]))) {
            // on a un nombre
            DonneesIntervalle.expression.push(tableau[k])
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(tableau[k])
          } else {
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      }
    } else {
      if ((texteIntervalle === '') || (texteIntervalle === undefined)) {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      } else if (intervalleReg.test(String(texteIntervalle))) {
        if (lgIntervalle(String(texteIntervalle)) === 0) {
          // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
          valNb = String(texteIntervalle).substring(1, String(texteIntervalle).indexOf(';'))
          for (k = 0; k < nbRepet; k++) {
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          }
        } else {
          for (k = 0; k < nbRepet; k++) {
            do {
              nbAlea = genereAlea(texteIntervalle)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
          DonneesIntervalle.leType.push('fraction')
          DonneesIntervalle.valeur.push(valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push(String(texteIntervalle))
          DonneesIntervalle.leType.push('nombre')
          DonneesIntervalle.valeur.push(String(texteIntervalle))
        }
      } else {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      }
    }
    return DonneesIntervalle
  }

  function ConvertDecimal (nb, nbreDecimales) {
    // nb peut être un entier, un décimal ou un nb fractionnaire
    // si c’est un nb fractionnaire et que la forme décimale contient moins de nbreDecimales, alors on renvoie le nb sous forme décimale
    // sinon on renvoit nb sous son format initial
    const nbDecimal = j3pCalculValeur(nb)
    // pour éviter les pbs d’arrondi, je cherche par quelle puissance de 10 multiplier mon nombre (puis el divisier)
    let puisPrecision = 15
    if (Math.abs(nbDecimal) > Math.pow(10, -13)) {
      const ordrePuissance = String(Math.abs(nbDecimal)).indexOf('.')
      puisPrecision = (ordrePuissance === -1) ? 15 - String(Math.abs(nbDecimal)).length : 15 - ordrePuissance
    }
    let newNb = nb
    if (Math.abs(j3pArrondi(nbDecimal, puisPrecision) - nbDecimal) < Math.pow(10, -12)) {
      // on a une valeur décimale
      // Si elle a plus de deux chiffres après la virgule, on donne une valeur décimale
      const nbChiffreApresVirgule = String(nbDecimal).length - 1 - String(nbDecimal).lastIndexOf('.')
      if (nbChiffreApresVirgule <= nbreDecimales) {
        newNb = nbDecimal
      }
    }
    return newNb
  }

  function enonceInit () {
    me.validOnEnter = false // ex donneesSection.touche_entree
    // Le paramètre définit la largeur relative de la première colonne
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(textes.titre_exo)
    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
    if (ds.donneesPrecedentes) {
      // on récupère les données du noeud précédent stockées dans me.donneesPersistantes.suites
      if (!me.donneesPersistantes.suites) {
        ds.donneesPrecedentes = false
        j3pShowError(Error('Les données précédentes n’ont pu être récupérées. De nouvelles sont générées !\nAttention car la nature de la suite n’est peut-être pas celle qui était attendue.'))
      }
    }
    stor.defSuite = {}
    if (ds.donneesPrecedentes) {
      ds.nbrepetitions = 1
      ds.nbitems = ds.nbetapes * ds.nbrepetitions
      // on récupère tout ce que ça contient pour le mettre dans stor
      Object.entries(me.donneesPersistantes.suites).forEach(([prop, value]) => {
        stor.defSuite[prop] = value
      })
      stor.defSuite.coefA = [stor.defSuite.a]
      stor.defSuite.coefB = [stor.defSuite.b]
      stor.defSuite.termeInit[0][0] = Number(stor.defSuite.termeInit[0][0])
    } else {
      let onRecommence
      let termeU0
      let termeC
      let termeV0
      let termInitAlea
      // on génère ces valeurs
      const coefADonnees = constructionTabIntervalle(ds.a, '[2;7]', ds.nbrepetitions, [0, 1, -1, 0.3])
      const coefBDonnees = constructionTabIntervalle(ds.b, '[-6;6]', ds.nbrepetitions, [0])
      stor.defSuite.coefA = coefADonnees.expression
      stor.defSuite.coefB = coefBDonnees.expression

      // il faut que je gère le terme initial car j’ai un tableau dont les données sont de la forme 1|[...;...] ce second point pouvant être un entier ou un nb fractionnaire
      stor.defSuite.termeInit = []
      const TermeInitParDefaut = [0, '[1;5]']
      if (!isNaN(j3pNombre(String(ds.termeInitial)))) {
        // au lieu d’un tableau, n’est indiqué qu’un seul nombre. Celui-ci devient le rang initial de chaque répétition
        const leRangInit = j3pNombre(ds.termeInitial)
        for (let k = 0; k < ds.nbrepetitions; k++) {
          onRecommence = false
          do {
            termeU0 = genereAlea(TermeInitParDefaut[1])
            // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
            termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
            termeV0 = j3pGetLatexSomme(termeU0, termeC)
            onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
          } while (onRecommence)
          stor.defSuite.termeInit.push([leRangInit, termeU0])
        }
      }
      for (let k = 0; k < ds.nbrepetitions; k++) {
        if ((ds.termeInitial[k] === '') || (ds.termeInitial[k] === undefined)) {
          // on a rien renseigné donc par défaut, on met
          onRecommence = false
          do {
            termeU0 = genereAlea(TermeInitParDefaut[1])
            if (stor.defSuite.typeSuite === 'arithmeticogeometrique') {
              // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
              termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
              termeV0 = j3pGetLatexSomme(termeU0, termeC)
              onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
            }
          } while (onRecommence)
          stor.defSuite.termeInit.push([TermeInitParDefaut[0], termeU0])
        } else {
          // il faut la barre verticale pour séparer le rang et la valeur du terme initial
          const posBarre = String(ds.termeInitial[k]).indexOf('|')
          if (posBarre === -1) {
            // var EntierReg = new RegExp('[0-9]{1,}', 'i')
            const EntierReg = /[0-9]+/g
            // je n’ai pas la barre verticale
            // peut-être ai-je seulement l’indice du premier terme
            if (EntierReg.test(ds.termeInitial[k])) {
              // c’est un entier correspondant à l’indice du terme initial
              onRecommence = false
              do {
                termeU0 = genereAlea(TermeInitParDefaut[1])
                // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                termeV0 = j3pGetLatexSomme(termeU0, termeC)
                onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
              } while (onRecommence)
              stor.defSuite.termeInit.push([ds.termeInitial[k], termeU0])
            } else {
              // donc je réinitialise avec la valeur par défaut
              onRecommence = false
              do {
                termeU0 = genereAlea(TermeInitParDefaut[1])
                // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                termeV0 = j3pGetLatexSomme(termeU0, termeC)
                onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
              } while (onRecommence)
              stor.defSuite.termeInit.push([TermeInitParDefaut[0], termeU0])
            }
          } else {
            // la valeur (éventuellement aléatoire) du terme initial
            onRecommence = false
            do {
              termInitAlea = constructionTabIntervalle(ds.termeInitial[k].substring(posBarre + 1), TermeInitParDefaut[1], 1).expression[0]
              // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
              termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
              termeV0 = j3pGetLatexSomme(termInitAlea, termeC)
              onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
              // console.log('onRecommence:',onRecommence,'  termeV0:',termeV0)
            } while (onRecommence)
            // au cas où je vérifie que la première valeur est bien entière
            let rangInit = ds.termeInitial[k].substring(0, posBarre)
            rangInit = (String(j3pNombre(rangInit)).indexOf('.') === -1) ? rangInit : TermeInitParDefaut[0]
            stor.defSuite.termeInit.push([rangInit, termInitAlea])
          }
        }
      }
    }
    init()
      .then(enonceMain)
      .catch(j3pShowError)
  } // enonceInit

  function enonceMain () {
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    for (let i = 1; i <= 3; i++) stor['zoneConsD' + i] = j3pAddElt(stor.conteneurD, 'div')
    if (!ds.donneesPrecedentes) {
      // si je n’ai pas de section précédente, je génère le nom de la suite (u_n) de manière aléatoire
      stor.defSuite.nomU = j3pGetRandomElts(['u', 'v'], 1)[0]
    }
    // je mets mes valeurs dans une variable plus simple :
    stor.defSuite.a = stor.defSuite.coefA[me.questionCourante - 1]
    stor.defSuite.b = stor.defSuite.coefB[me.questionCourante - 1]
    // je prends la suite intermédiaire v_n=u_n-b/(1-a). Je note c=-b/(1-a)
    stor.defSuite.cFrac = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.b), j3pGetLatexSomme('1', j3pGetLatexOppose((j3pGetLatexQuotient(stor.defSuite.a * 1000, 1000)))))
    // si c est une valeur décimale simple, alors je l’affiche sous forme décimale
    stor.defSuite.c = ConvertDecimal(stor.defSuite.cFrac, 2)
    stor.defSuite.v0 = j3pGetLatexSomme(stor.defSuite.termeInit[me.questionCourante - 1][1], stor.defSuite.cFrac)
    stor.defSuite.typeLimite = (Math.abs(stor.defSuite.a) < 1) ? 'nombre' : 'texte'
    stor.defSuite.laLimite = (stor.defSuite.typeLimite === 'nombre')
      ? j3pGetLatexOppose(stor.defSuite.c)
      : (j3pCalculValeur(stor.defSuite.v0) < 0) ? '-\\infty' : '+\\infty'
    // me.donneesPersistantes.suites sert à envoyer des informations dans le bilan qui pourront être récupérées par une autre section sur les suites
    // Ceci a pour but de poursuivre l’étude de la suite définie dans cette section
    me.donneesPersistantes.suites = {}
    for (const prop in stor.defSuite) me.donneesPersistantes.suites[prop] = stor.defSuite[prop]
    me.logIfDebug('me.donneesPersistantes.suites:', me.donneesPersistantes.suites)
    const relRecurrence = j3pGetLatexMonome(1, 1, stor.defSuite.a, stor.defSuite.nomU + '_n') + j3pGetLatexMonome(2, 0, stor.defSuite.b, '')

    j3pAffiche(stor.zoneConsD1, '', textes.consigne1,
      {
        u: stor.defSuite.nomU,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        i: stor.defSuite.termeInit[me.questionCourante - 1][1],
        j: relRecurrence
      })
    if (ds.donneesPrecedentes) {
      const consigne2 = (String(stor.defSuite.laLimite).includes('infty')) ? textes.consigne2_2 : textes.consigne2_1
      j3pAffiche(stor.zoneConsD2, '', consigne2,
        {
          u: stor.defSuite.nomU,
          l: stor.defSuite.laLimite
        })
    }
    stor.defSuite.c = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.b), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.a)))
    stor.defSuite.c = ConvertDecimal(stor.defSuite.c, 2)
    // Les calculs suivants vont servir pour la correction :
    stor.defSuite.listeUn = [stor.defSuite.termeInit[me.questionCourante - 1][1]]
    let u = stor.defSuite.termeInit[me.questionCourante - 1][1]
    const nbDec = 4
    const borne = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b)// C’est que la suite sera croissante
      ? (stor.defSuite.laLimite === '+\\infty') ? 1000 : j3pArrondi(j3pCalculValeur(stor.defSuite.laLimite) - Math.pow(10, -nbDec), nbDec)
      : (stor.defSuite.laLimite === '-\\infty') ? -1000 : j3pArrondi(j3pCalculValeur(stor.defSuite.laLimite) + Math.pow(10, -nbDec), nbDec)
    let condition = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b) ? (u <= borne) : (u >= borne)
    stor.defSuite.conditionWhile = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b) ? stor.defSuite.nomU + '>' + borne : stor.defSuite.nomU + '<' + borne
    stor.defSuite.listeBornes = [borne]
    // On utilisera listeBornes pour tester d’autres valeurs que borne, ce qui devrait permettre de valider l’algo (enfin à peu près)
    if (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b) {
      // Ma suite est croissante
      if (stor.defSuite.laLimite === '+\\infty') {
        stor.defSuite.listeBornes.push(1000, 10000, 50000, 1000000)
      } else {
        stor.defSuite.listeBornes.push(j3pArrondi(j3pCalculValeur(stor.defSuite.laLimite) - Math.pow(10, -nbDec - 1), nbDec + 1), j3pArrondi(j3pCalculValeur(stor.defSuite.laLimite) - Math.pow(10, -nbDec + 1), nbDec - 1), j3pArrondi(j3pCalculValeur(stor.defSuite.laLimite) - Math.pow(10, -nbDec - 2), nbDec + 2), j3pArrondi(j3pCalculValeur(stor.defSuite.laLimite) - Math.pow(10, -nbDec - 3), nbDec + 3))
      }
    } else {
      // Ma suite est décroissante
      if (stor.defSuite.laLimite === '-\\infty') {
        stor.defSuite.listeBornes.push(-1000, -10000, -50000, -1000000)
      } else {
        stor.defSuite.listeBornes.push(j3pArrondi(j3pCalculValeur(stor.defSuite.laLimite) + Math.pow(10, -nbDec - 1), nbDec + 1), j3pArrondi(j3pCalculValeur(stor.defSuite.laLimite) + Math.pow(10, -nbDec + 1), nbDec - 1), j3pArrondi(j3pCalculValeur(stor.defSuite.laLimite) + Math.pow(10, -nbDec - 2), nbDec + 2), j3pArrondi(j3pCalculValeur(stor.defSuite.laLimite) + Math.pow(10, -nbDec - 3), nbDec + 3))
      }
    }
    while (condition) {
      u = j3pArrondi(stor.defSuite.a * u + stor.defSuite.b, 8)
      stor.defSuite.listeUn.push(u)
      condition = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b) ? (u <= borne) : (u >= borne)
    }
    // Suivant la monotonie de la suite
    stor.defSuite.condSeuil = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b)
      ? stor.defSuite.nomU + '_n>' + j3pVirgule(borne)
      : stor.defSuite.nomU + '_n<' + j3pVirgule(borne)
      // Suivant la monotonie de la suite
    stor.condAlgo = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b)
      ? stor.defSuite.nomU + '<=' + borne
      : stor.defSuite.nomU + '>=' + borne
    const consigne3 = (ds.formatFonction) ? textes.consigneBis3 : textes.consigne3
    j3pAffiche(stor.zoneConsD3, '', consigne3, { cond: stor.defSuite.condSeuil })

    if (ds.formatFonction) {
      stor.algoInit = 'def seuil():\n'
      stor.algoInit += '\t' + stor.defSuite.nomU + '=' + stor.defSuite.termeInit[me.questionCourante - 1][1] + '\n'
      stor.algoInit += '\tn=' + stor.defSuite.termeInit[me.questionCourante - 1][0] + '\n'
    } else {
      stor.algoInit = stor.defSuite.nomU + '=' + stor.defSuite.termeInit[me.questionCourante - 1][1] + '\n'
      stor.algoInit += 'n=' + stor.defSuite.termeInit[me.questionCourante - 1][0] + '\n'
    }
    const tab = (ds.formatFonction) ? '\t' : ''
    const afficheB = (stor.defSuite.b < 0) ? stor.defSuite.b : '+' + stor.defSuite.b
    stor.algoCorr = stor.algoInit
    stor.algoCorr += tab + 'while ' + stor.condAlgo + ':\n'
    stor.algoCorr += tab + '\tn=n+1\n'
    stor.algoCorr += tab + '\t' + stor.defSuite.nomU + '=' + stor.defSuite.a + '*' + stor.defSuite.nomU + afficheB + '\n'
    if (ds.formatFonction) {
      stor.algoCorr += '\treturn n'
    }
    creationEditeurPython({ hautEditeur: 200, hautConsole: 50 })
    setTextAce(stor.editor, stor.algoInit)
    if (ds.formatFonction) setTextAce(laConsole, '')
    j3pElement(stor.idReinitialiser).addEventListener('click', reinitialiser)
    stor.algoReponse = stor.algoCorr // algoCorr sert à vérifier si l’algorithme proposé par l’élève est équivalent à celui-ci (il contient un print à la fin pour vérifier l’affichage)
    if (ds.formatFonction) {
      stor.algoCorr += '\nprint(seuil())'
      stor.dernieresLignes = 'print(seuil())'
    } else {
      stor.algoCorr += '\nprint(n)'
      stor.dernieresLignes = 'print(n)'
    }
    j3pElement('output').innerHTML = ''

    me.logIfDebug(stor.algoReponse)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvent dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        try {
          j3pDetruit(stor.fenetreCorr)
        } catch (e) {
          console.warn(e) // pas normal mais pas grave
        }
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const reponse = { aRepondu: false, bonneReponse: true }
      stor.algoEleveInit = getTextAce(stor.editor)
      const aucunChgt = (stor.algoInit === stor.algoEleveInit)
      reponse.aRepondu = !aucunChgt
      let definitionFct = true
      let algoTest
      let whilePresent = true
      let conditionPresente = true
      let hasSpacesInsteadOfTabs = false
      let boucleInfinie
      let objDeuxPts
      if (reponse.aRepondu) {
        // On vérifie que le programme est bien la création d’une fonction
        algoTest = stor.algoEleveInit
        const tabLigne = algoTest.split('\n')
        let k = 0
        while (k < tabLigne.length) {
          if (/^\s*$/g.test(tabLigne[k])) {
            // Cette ligne est vide, on la vire
            tabLigne.splice(k, 1)
          } else if (tabLigne[k].includes('print')) {
            // On a une ligne d’affichage, donc on la vire
            tabLigne.splice(k, 1)
          } else {
            if (/^ +/.test(tabLigne[k])) {
              hasSpacesInsteadOfTabs = true
            }
            k = k + 1
          }
        }
        if (tabLigne[0]) {
          if (ds.formatFonction && !tabLigne[0].startsWith('def')) {
            // C’est que l’élève n’a pas créé de fonction, alors qu’il le devait
            definitionFct = false
            reponse.bonneReponse = false
          }
          boucleInfinie = false
          if (definitionFct) {
            algoTest = tabLigne.join('\n')
            objDeuxPts = verifDeuxPoints(algoTest)
            me.logIfDebug('objDeuxPts:', objDeuxPts)
            if (objDeuxPts.presents) {
              whilePresent = (algoTest.includes('while'))
              reponse.bonneReponse = whilePresent
              if (reponse.bonneReponse) {
                // Je vérifie que je n’ai pas une boucle infinie
                execute()
                boucleInfinie = stor.boucleInf
                if (boucleInfinie) {
                  reponse.bonneReponse = false
                } else {
                  conditionPresente = algoTest.replace(/\s/g, '').indexOf(stor.condAlgo) > -1
                  reponse.bonneReponse = conditionPresente
                  if (conditionPresente) {
                    algoTest += '\n' + stor.dernieresLignes
                    for (let i = 1; i < stor.defSuite.listeBornes.length; i++) {
                      reponse.bonneReponse = (reponse.bonneReponse && verifierCodePython(algoTest.replace(stor.defSuite.listeBornes[0], stor.defSuite.listeBornes[i]), stor.algoCorr.replace(stor.defSuite.listeBornes[0], stor.defSuite.listeBornes[i]), {}, 'number'))
                    }
                  }
                }
              }
            } else {
              reponse.bonneReponse = false
            }
          }
        } else {
          reponse.aRepondu = false
        }
      }

      if ((!reponse.aRepondu) && !me.isElapsed) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.zoneCorr.style.color = me.styles.cfaux
        stor.zoneCorr.innerHTML = (aucunChgt)
          ? textes.comment1
          : textes.comment2
        return this.finCorrection()
      }

      // Une réponse a été saisie
      // Bonne réponse
      if (reponse.bonneReponse) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        // afficheCorr(true);
        desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
        j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
        j3pDetruit(stor.idReinitialiser)
        me.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorr(false)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      if (!definitionFct) {
        stor.zoneCorr.innerHTML += '<br>' + textes.comment6
      } else if (!objDeuxPts.presents) {
        stor.zoneCorr.innerHTML += '<br>' + j3pChaine(textes.comment3, { l: objDeuxPts.numLigne })
      } else if (!whilePresent) {
        stor.zoneCorr.innerHTML += '<br>' + textes.comment4
      } else if (boucleInfinie) {
        stor.zoneCorr.innerHTML += '<br>' + textes.comment7
      } else if (!conditionPresente) {
        stor.zoneCorr.innerHTML += '<br>' + textes.comment5
      }
      if (hasSpacesInsteadOfTabs) {
        stor.zoneCorr.innerHTML += '<br>' + textes.comment8
      }
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // indication éventuelle ici
        // ici il a encore la possibilité de se corriger
        return this.finCorrection()
      }

      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorr(false)
      me.typederreurs[2]++
      return this.finCorrection('navigation', true)
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        me.buttonsElts.sectionSuivante?.addEventListener('click', nettoyageCorrection)
        j3pElement(stor.idExecuter).removeEventListener('click', execute)
      } else {
        me.buttonsElts.suite?.addEventListener('click', nettoyageCorrection)
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
