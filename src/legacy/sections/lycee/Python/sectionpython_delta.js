import { j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pDetruit, j3pElement, j3pGetNewId, j3pGetRandomInt, j3pRandomdec, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { init, desactiverEditeur, getTextAce, runPython, runPythonCode, setTextAce } from 'src/legacy/outils/algo/algo_python'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/* Section de programmation sur le discriminant */

/* global ace */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['difficulte', 0, 'entier', 'Plus ce paramètre est élevé, moins le travail est mâché (0 : la fonction est bien complétée, 1: compléter totalement)'],
    ['aide_console', 1, 'entier', '0 : rien d’écrit en console, 1 l’appel à la fonction est présent']
  ]
}

const textes = {
  titre: 'Programmer le discriminant ',
  consigne: 'Complète la fonction discriminant en utilisant tes connaissances sur les polynômes du second degré, exécute le code de la console au moins une fois (celui-ci n’est pas évalué), puis valide-le (OK).<br>Cette fonction doit renvoyer le discriminant de l’expression $ax^2+bx+c$',
  consigne_sans_console: 'Complète la fonction discriminant en utilisant tes connaissances sur les polynômes du second degré, tu peux t’aider de la console pour tester ta fonction (ceci n’est pas évalué), puis valide ton code (OK).<br>Cette fonction doit renvoyer le discriminant de l’expression $ax^2+bx+c$',
  correction: 'Il ne doit pas y avoir de message d’erreur en sortie !',
  fin: ' Pas d’autre chance pour cette question...',
  synthese1: 'A retenir : delta vaut b*b-4*a*c',
  synthese2: 'A retenir : le nombre de solutions dépend du signe de delta',
  utiliserConsole: 'Ne pas hésiter à utiliser la console pour tester le programme.',
  sortie: 'Sortie',
  executer: 'Exécuter',
  editeur: 'Editeur :',
  console: 'Console :',
  bug: 'Ton programme a bogué pour'
}

/**
 * section python_delta
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let laConsole
  // ---------------------------------------------------------------------------
  // Vérification de code
  // ---------------------------------------------------------------------------
  function execute () {
    runPython(stor.editor, laConsole)
  }

  function verifierCodePython (algoEleve, algoSecret, entrees) {
    // console.log('algoEleve=', algoEleve)
    // console.log('algoSecret=', algoSecret)
    // console.log('entrees=', entrees)
    let lignes
    let ligne
    // on ajoute les entrees à tester
    let debutTest = ''
    let i, k, cle, valeur
    for (k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      debutTest = debutTest + cle + ' = ' + valeur + '\n'
    }
    // console.log('debutTest', debutTest)
    // on ote les saisies de l’algo eleve
    // console.log('algoEleve avant', algoEleve)
    lignes = algoEleve.split('\n')
    let algoEleveTest = debutTest
    for (i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.indexOf('input(') >= 0) continue
      algoEleveTest = algoEleveTest + ligne + '\n'
    }
    // console.log('algoEleve apres', algoEleveTest)
    // on ote les saisies de l’algo secret
    // console.log('algo secret avant', algoSecret)
    lignes = algoSecret.split('\n')
    let algoSecretTest = debutTest
    for (i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.indexOf('input(') >= 0) continue
      algoSecretTest = algoSecretTest + ligne + '\n'
    }
    // console.log('algo secret apres', algoSecretTest)
    // on exécute algoSecretTest
    runPythonCode(algoSecretTest)
    let sortieSecret = j3pElement('output').innerHTML
    // console.log('sortieSecret=', sortieSecret)
    // on exécute algoEleveTest
    runPythonCode(algoEleveTest)
    let sortieEleve = j3pElement('output').innerHTML
    // console.log('sortieEleve=', sortieEleve)
    // on regarde si le test élève s’exécute
    let diagnostique
    if (sortieEleve.indexOf('Error') !== -1) {
      diagnostique = textes.bug
      for (k = 0; k < Object.keys(entrees).length; k++) {
        cle = Object.keys(entrees)[k]
        valeur = Object.values(entrees)[k]
        diagnostique = diagnostique + ' ' + cle + '=' + valeur
      }
      j3pElement('output').innerHTML = diagnostique + '\n' + j3pElement('output').innerHTML
      return false
    }
    // on compare les 2 exécutions
    sortieSecret = sortieSecret.replace(/\n/g, '').replace(/<br *\/?>/ig, '')
    sortieEleve = sortieEleve.replace(/\n/g, '').replace(/<br *\/?>/ig, '')
    const identique = (Number(sortieSecret) === Number(sortieEleve))
    if (identique) {
      j3pElement('output').innerHTML = ''
      return true
    }
    // on explique l’échec
    diagnostique = 'Résultat faux pour'
    for (k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      diagnostique = diagnostique + ' ' + cle + '=' + valeur
    }
    diagnostique = diagnostique + '\n' + 'ta réponse : ' + sortieEleve
    diagnostique = diagnostique + '\n' + 'la bonne réponse : ' + sortieSecret
    j3pElement('output').innerHTML = diagnostique
    return false
  }

  function creationEditeurPython () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    let consigne
    if (ds.aide_console === 0) {
      consigne = textes.consigne_sans_console
    } else {
      consigne = textes.consigne
    }
    j3pAffiche(stor.zoneCons1, '', consigne)
    stor.idEditor = j3pGetNewId('idEditor')
    stor.divEditor = j3pAddElt(stor.zoneCons2, 'div', '', { id: stor.idEditor })
    j3pStyle(stor.divEditor, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    stor.editor = ace.edit(stor.idEditor)
    stor.divEditor.style.height = '140px'
    stor.divEditor.style.width = '95%'
    stor.editor.resize()
    stor.editor.getSession().setUseSoftTabs(false)
    stor.editor.getSession().setMode('ace/mode/python')
    stor.editor.setFontSize('12pt')
    stor.editor.focus()

    j3pAffiche(stor.zoneCons3, '', textes.console)
    stor.idConsole = j3pGetNewId('idConsole')
    stor.divConsole = j3pAddElt(stor.zoneCons4, 'div', '', { id: stor.idConsole })
    j3pStyle(stor.divConsole, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    laConsole = ace.edit(stor.idConsole)
    stor.divConsole.style.height = '20px'
    stor.divConsole.style.width = '95%'
    stor.divConsole.style.marginTop = '10px'
    stor.divConsole.style.marginBottom = '10px'
    laConsole.getSession().setMode('ace/mode/python')
    laConsole.setFontSize('12pt')
    try {
      // J’ai eu un signalement où cet objet HTML n’existait pas ce qui faisait planter la section
      // Je ne vois pas comment ce peut être possible
      const tabAce = document.getElementsByClassName('ace_gutter')
      const ligne1Console = tabAce[1].childNodes[0].childNodes[0]
      ligne1Console.innerHTML = '>>>'
      ligne1Console.style.paddingLeft = '10px'
    } catch (e) {
      // Je vire la console car il y a un pb pour son affichage :
      j3pDetruit(stor.zoneCons3)
      j3pDetruit(stor.divConsole)
      console.warn('soucis avec l’élément HTML censé contenir les chevrons')
    }
    stor.divBtns = j3pAddElt(stor.zoneCons5, 'div')
    stor.divBtns.style.textAlign = 'right'
    stor.idExecuter = j3pGetNewId('executer')
    j3pAjouteBouton(stor.divBtns, stor.idExecuter, 'MepBoutons', textes.executer, '')
    j3pElement(stor.idExecuter).addEventListener('click', execute)

    j3pAffiche(stor.zoneCons6, '', textes.utiliserConsole, { L: stor.nomListe })
    j3pStyle(stor.zoneCons6, { fontSize: '0.9em', fontStyle: 'italic' })
    const underline = j3pAddElt(stor.zoneCons7, 'u')
    j3pAddTxt(underline, textes.sortie)
    stor.output = 'output'
    stor.pre = j3pAddElt(stor.zoneCons7, 'pre', { id: stor.output })
    j3pStyle(stor.pre, { color: 'black' })
  }

  function enonceInit () {
    me.validOnEnter = false // ex donneesSection.touche_entree
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    me.afficheTitre(textes.titre)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    init()
      .then(enonceMain)
      .catch(j3pShowError)
  }

  function enonceMain () {
    creationEditeurPython()
    let algoInit
    if (ds.difficulte === 0) {
      algoInit =
      'def discriminant(a,b,c):\n' +
      '\tdelta = \n' +
      '\treturn delta\n\n'
    }
    if (ds.difficulte === 1) {
      algoInit =
      'def discriminant(a,b,c):\n' +
      '\t\n' +
      '\treturn \n'
    }

    setTextAce(stor.editor, algoInit)
    let consoleInit = ''
    if (ds.aide_console === 1) {
      consoleInit = 'discriminant(1,2,3)'
    }
    setTextAce(laConsole, consoleInit)
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent

    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    j3pStyle(stor.zoneCorr, { paddingTop: '15px' })
    stor.zoneExpli = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(stor.zoneExpli, { paddingTop: '10px' })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // on estime que l’élève a forcément saisi une réponse
      // on rajoute le test nécessaire de la fonction
      const algoEleve = getTextAce(stor.editor) + 'print(discriminant(a,b,c))'
      let cbon
      if (me.questionCourante === 1) {
        const algoReponse = 'def discriminant(a,b,c):\n' +
          '\tdelta = b*b-4*a*c\n' +
          '\treturn delta\n' +
          'print discriminant(a,b,c)'

        cbon = verifierCodePython(algoEleve, algoReponse, { a: 1, b: 1, c: 2 }) // delta = b-4*a*c ne provoquera pas d’erreur
        if (cbon) cbon = verifierCodePython(algoEleve, algoReponse, { a: 2, b: 2, c: 1 }) // delta = b*b+4*a*c ne provoquera pas d’erreur
        if (cbon) {
          cbon = verifierCodePython(algoEleve, algoReponse, {
            a: j3pRandomdec('[2;3],1'),
            b: -2,
            c: j3pGetRandomInt(1, 10)
          })
        }
      }

      // Bonne réponse
      if (cbon) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        me.typederreurs[0]++
        // Pour empêcher l’élève de modifier son code Python
        desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
        return me.finCorrection('navigation', true)
      }
      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux

      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        return me.finCorrection() // on reste en correction
      }
      // Erreur au nème essai
      stor.zoneCorr.innerHTML += textes.fin
      if (me.questionCourante === 1) {
        j3pAddTxt(stor.zoneExpli, textes.synthese1)
      } else if (me.questionCourante === 2) {
        j3pAddTxt(stor.zoneExpli, textes.synthese2)
      }
      desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)

      me.typederreurs[2]++
      me.finCorrection('navigation', true)

      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      me.finNavigation()
      break
  }
}
