import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pFocus, j3pStyle } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { fctsEtudeCalculValeur, fctsEtudeEcrireMonome, fctsEtudeEcritureDomaine, fctsEtudeEcritureLatexDerivee, fctsEtudeEcritureLatexFonction, fctsEtudeEcriturePourCalcul, fctsEtudeEgaliteTableaux, fctsEtudeExpressionAvecFacteurs, fctsEtudeOrdonneTabSansDoublon, fctsEtudeProduitNbs, fctsEtudeSigneFct, fctsEtudeSommeNbs, fctsEtudeTableauSignesFct, fctsEtudeTabSignesProduit, fctsEtudeTransformationDomaine } from 'src/legacy/outils/fonctions/etude'
import { j3pAffiche } from 'src/lib/mathquill/functions'

import { aideModeles, initModele, initSection } from './common'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Rémi Deniaud
    Mars 2015
    Section intégrable dans un graphe avec données du graphe (on utilise la variable imposer_fct pour donner une fonction particulière suivant un modèle) ou donneesPrecedentes
    On demande le signe de la dérivée (sous la forme d’un tableau de signes)
    En cas d’ajout de modèles, il faudra juste modifier les fonctions fctsEtudeGenereAleatoire et genereTextesCorrection puis ajouter les textes de correction qui vont bien
    Non implémenté pour l’instant : le retour des réponses de l’élève pour la section bilan (qui n’a jamais été finalisée)
    C’est en case correction qu’il faudra implémenter me.DonneesPersistantes.etudeFonctions.repEleve
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['imposer_fct', '', 'string', 'On peut renseigner une fonction correspondant au modèle ou laisser vide pour générer aléatoirement une fonction suivant un modèle'],
    ['modele', [1], 'array', aideModeles],
    ['imposer_domaine', [], 'array', 'On donne le domaine de définition sous la forme d’un tableau où chaque intervalle est représenté par 4 éléments : borne inf, borne sup et les deux crochets. Par exemple ["-infini","0","]","]"] pour l’intervalle ]-infini;0].'],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la fonction étudiée est celle du nœud précédent.']
  ],

  // NOUVEAUTE DP (penser à la virgule ci-dessus), pour l’instant inutilisé dans le modèle, mais plus tard dans l’éditeur de graphes pourquoi pas...
  donnees_parcours_description: {
    fonction: 'l’expression de la fonction en latex générée aléatoirement suivant 4 modèles, si le param imposer_fct n’est pas renseigné.',
    derivee: 'l’expression de la dérivée en latex.',
    modele: 'Nombre entier, le modèle 1 correspond à (ax+b)exp(x) , modèle 2 à ..., on utilise ensuite les pptés .a, .b, etc pour récupérer les variables.'
  },
  // pour fonctionnement dans editgraphes
  donnees_parcours_nom_variable: 'imposer_fct'
}
const textes = {
  titre: 'Signe de la dérivée',
  signe1: 'Signe du produit',
  signe2: 'Signe du quotient',
  signe3: 'Signe de ',
  phrase1: 'On considère la fonction définie et dérivable sur $£a$ par : $£f(x)=£b$.',
  phrase1_bis: 'On considère la fonction définie sur $£a$, dérivable sur $£c$ par : $£f(x)=£b$.',
  phrase2: "Pour tout $x\\in £c$, $£f\\quad '(x)=£g$ <br>($£f\\quad'(x)$ est écrit sous forme factorisée).",
  phrase2bis: "Pour tout $x\\in £c$, $£f\\quad '(x)=£g$.",
  phrase3_1: "On a déjà montré que sur $£a$, $£b$ donc $£f\\quad '(x)$ est du signe de $£d$.",
  phrase3_2: "On a déjà montré que sur $£a$, $£b$ et $£c$ donc $£f\\quad '(x)$ est du signe de $£d$.",
  phrase3_3: 'On a déjà montré que sur $£a$, $£b$ et $£c$.',
  phrase3_4: 'On a déjà montré que sur $£a$, $£b$.',
  phrase4: 'On détermine alors son signe à l’aide d’un tableau.',
  phrase4_2: "On donne alors le signe de $£f\\quad '(x)$ à l’aide d’un tableau.",
  phrase5: 'Pour ajouter une valeur sur la première ligne : ',
  phrase6: 'Pour retirer une valeur sur la première ligne : ',
  remarque: 'Attention car ajouter ou enlever une colonne réinitialise totalement le tableau.',
  comment1: 'Il faut compléter entièrement le tableau',
  comment2: 'Il y a une ou plusieurs erreurs dans la première ligne (tout est alors en rouge sans pour autant être faux, à vérifier) !',
  comment3: 'Le signe d’au moins un facteur est faux (d’autres éléments sont en rouge, même si une partie peut être exacte) !',
  comment4: 'Il y a au moins un problème dans le signe du produit (certains élements de la ligne pouvant être exacts, meme s’ils sont en rouge) !',
  comment4bis: 'Il y a au moins un problème dans le signe du quotient (certains élements de la ligne pouvant être exacts, meme s’ils sont en rouge) !',
  comment5: 'Le nombre de valeurs de la première ligne est inexact !',
  comment6: 'La première ligne du tableau est correcte. Par contre, le signe de la dérivée ne l’est pas (ou l’un des zéros a été oublié) !',
  comment6_2: 'La première ligne du tableau est correcte. Par contre, le signe de la dérivée ne l’est pas !',
  corr1: 'Voici le tableau attendu&nbsp;:',
  corr2: ' La dérivée est une fonction polynôme de degré 2.<br>Son discriminant vaut $\\Delta=£d>0$ et les racines dans $\\R$ sont $£x$ et $£y$.',
  corr2bis: ' La dérivée est une fonction polynôme de degré 2.<br>Son discriminant vaut $\\Delta=0$ et la racine dans $\\R$ est $£x$.',
  corr2ter: ' La dérivée est une fonction polynôme de degré 2.<br>Son discriminant vaut $\\Delta=£d<0$ et n’admet donc pas de racine dans $\\R$.',
  corr3: 'On obtient alors le tableau&nbsp;:',
  corr3bis: 'On obtient alors le tableau (en faisant attention à l’ensemble de définition)&nbsp;:',
  // pour le modèle 4:
  corr4: 'Le numérateur est une fonction polynôme de degré 2.<br>Son discriminant vaut $\\Delta=£d>0$ et les racines dans $\\R$ sont $£x$ et $£y$.',
  corr4bis: 'Le numérateur est une fonction polynôme de degré 2.<br>Son discriminant vaut $\\Delta=0$ et la racine dans $\\R$ est $£x$.',
  corr4ter: 'Le numérateur est une fonction polynôme de degré 2.<br>Son discriminant vaut $\\Delta=£d<0$ et n’admet donc pas de racine dans $\\R$.',
  corr44: 'Le numérateur est une fonction polynôme de degré 2.<br>En résolvant $£e$, on obtient les racines du trinôme dans $\\R$ qui sont $£x$ et $£y$.',
  corr45: 'Le numérateur est une fonction polynôme de degré 2.<br>Ce trinôme se s’annule jamais il n’a ainsi pas de racine dans $\\R$.',
  corr46: 'Le numérateur est une fonction polynôme de degré 2.<br>On peut factoriser ce polynôme par $x$. Ses racines dans $\\R$ sont alors $£x$ et $£y$.',
  corr5: 'Pour trouver le signe du numérateur, on résout l’inéquation $£e$ qui donne $£f$.'
}
/**
 * section EtudeFonction_signederivee
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  // finalise l’initialisation de l’exo
  function enonceInit () {
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    initSection(me, textes.titre)
    enonceMain()
  }

  // exécuté à la fin du case enonce (dans tous les cas)
  function enonceMain () {
    stor.nom_f = 'f'
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    initModele(me)
    stor.nbFacteursPositifsReleves = 0
    // stor.defFonction.tabFacteursPositifsTrouves existera à condition qu’on soit dans un parcours avec reprise de données et que facteurderivee fait partie de ce parcours
    if (stor.defFonction.tabFacteursPositifsTrouves) stor.nbFacteursPositifsReleves = stor.defFonction.tabFacteursPositifsTrouves.length
    else stor.defFonction.tabFacteursPositifsTrouves = []
    const domaineDef = fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)
    const domaineDeriv = fctsEtudeEcritureDomaine(stor.defFonction.domaineDeriv)
    const laFonction = fctsEtudeEcritureLatexFonction(stor.defFonction.fonction, stor.defFonction.modele)
    const memeDomaine = (typeof domaineDef === 'string' || typeof domaineDeriv === 'string')
      ? domaineDef === domaineDeriv
      : fctsEtudeEgaliteTableaux(domaineDef, domaineDeriv)
    if (memeDomaine) {
      j3pAffiche(stor.zoneCons1, '', textes.phrase1,
        { f: stor.nom_f, a: domaineDef, b: laFonction })
    } else {
      j3pAffiche(stor.zoneCons1, '', textes.phrase1_bis,
        {
          f: stor.nom_f,
          a: domaineDef,
          b: laFonction,
          c: domaineDeriv
        })
    }
    stor.objet_derivee = fctsEtudeEcritureLatexDerivee(laFonction, stor.defFonction.modele, stor.defFonction.domaineDeriv, ds.debug)
    // objet_derivee.expres_derivee est l’expression de la dérivée au format latex
    if ([2, 4, 8].includes(stor.defFonction.modele)) {
      j3pAffiche(stor.zoneCons2, '', textes.phrase2bis,
        {
          f: stor.nom_f,
          g: stor.objet_derivee.expres_derivee,
          c: domaineDeriv
        })
    } else {
      j3pAffiche(stor.zoneCons2, '', textes.phrase2,
        {
          f: stor.nom_f,
          g: stor.objet_derivee.expres_derivee,
          c: domaineDeriv
        })
    }
    stor.nb_facteurs_restants = Math.round(stor.objet_derivee.tabFacteurs[0].length + stor.objet_derivee.tabFacteurs[1].length - stor.nbFacteursPositifsReleves)
    let ineq1
    if (stor.nbFacteursPositifsReleves !== 0) {
      // nous sommes passé par la section précédente (celle qui cherche les facteurs positifs)
      // et l’élève a trouvé au moins un facteur positif. Tous sont présents dans me.donneesPersistantes.etudeFonctions.facteursRestantsDerivee
      const deriveeRestante = fctsEtudeExpressionAvecFacteurs(me.donneesPersistantes.etudeFonctions.facteursRestantsDerivee)
      if (Math.abs(stor.nbFacteursPositifsReleves - 1) < Math.pow(10, -12)) {
        // on a relevé un seul facteur positif
        ineq1 = stor.defFonction.tabFacteursPositifsTrouves[0] + '>0'
        if (Math.abs(stor.nb_facteurs_restants) < Math.pow(10, -10)) {
          // il n’y a plus de facteurs : le seul facteur est positif et il a déjà été donné
          j3pAffiche(stor.zoneCons3, '', textes.phrase3_4,
            {
              f: stor.nom_f,
              a: fctsEtudeEcritureDomaine(stor.defFonction.domaineDeriv),
              b: ineq1,
              d: deriveeRestante
            })
        } else {
          j3pAffiche(stor.zoneCons3, '', textes.phrase3_1,
            {
              f: stor.nom_f,
              a: fctsEtudeEcritureDomaine(stor.defFonction.domaineDeriv),
              b: ineq1,
              d: deriveeRestante
            })
        }
      } else {
        ineq1 = stor.defFonction.tabFacteursPositifsTrouves[0] + '>0'
        if (stor.defFonction.tabFacteursPositifsTrouves.length >= 3) {
          for (let i = 2; i < stor.defFonction.tabFacteursPositifsTrouves.length - 1; i++) {
            ineq1 += '\\quad ;\\quad ' + stor.defFonction.tabFacteursPositifsTrouves[1] + '>0'
          }
        }
        const ineq2 = stor.defFonction.tabFacteursPositifsTrouves[stor.defFonction.tabFacteursPositifsTrouves.length - 1] + '>0'
        if (Math.abs(stor.nb_facteurs_restants) < Math.pow(10, -10)) {
          // il n’y a plus de facteurs : tous les facteurs positifs ont déjà été donnés
          j3pAffiche(stor.zoneCons3, '', textes.phrase3_3,
            {
              f: stor.nom_f,
              a: fctsEtudeEcritureDomaine(stor.defFonction.domaineDeriv),
              b: ineq1,
              c: ineq2,
              d: deriveeRestante
            })
        } else {
          j3pAffiche(stor.zoneCons3, '', textes.phrase3_2,
            {
              f: stor.nom_f,
              a: fctsEtudeEcritureDomaine(stor.defFonction.domaineDeriv),
              b: ineq1,
              c: ineq2,
              d: deriveeRestante
            })
        }
      }
    }
    if (Math.abs(stor.nb_facteurs_restants) < Math.pow(10, -10)) {
      j3pAffiche(stor.zoneCons4, '', textes.phrase4_2,
        { f: stor.nom_f })
    } else {
      j3pAffiche(stor.zoneCons4, '', textes.phrase4)
    }
    /// ////////////////////////////////////////////////////////////////////
    // tout le travail sur la construction du tableau de signes est là
    /// ////////////////////////////////////////////////////////////////////

    me.logIfDebug('stor.objet_derivee.tabFacteurs:' + stor.objet_derivee.tabFacteurs[0] + '  ; ' + stor.objet_derivee.tabFacteurs[1] + '  ; nb facteurs positifs déjà trouvés ' + stor.nbFacteursPositifsReleves)
    // on a deux type de tableaux
    // avec leur taille
    stor.largeur_tab = 95 * me.zonesElts.HG.getBoundingClientRect().width / 100
    stor.taille_ligne = 45
    // le domaine de dérivabilité de la fonction
    const borneInf = stor.defFonction.domaineDeriv[0]
    const borneSup = stor.defFonction.domaineDeriv[Math.floor((stor.defFonction.domaineDeriv.length - 1) / 4) * 4 + 1]
    // création du tableau
    stor.tableau1 = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { position: 'relative', top: 0, left: 0 }) })

    if (stor.nb_facteurs_restants <= 1) {
      // l’un d’eux n’a qu’une ligne en plus de celle des abscisses
      // je gère la taille du tableau
      stor.hauteur_tab = stor.taille_ligne * (2)
      stor.dimension_tab = [stor.largeur_tab, stor.hauteur_tab]
      // nombre de valeur sur la ligne des x en plus des bornes (comme ici on demande de remplir le tableau, alors il n’y en a pas)
      stor.dimension_tab.push(0)
      stor.dimension_tab.push([borneInf, borneSup])
      // maintenant je mets tous les zéros des facteurs présents dans un même tableau
      stor.les_zeros = []
      for (let i = 0; i <= 1; i++) {
        for (let k2 = 0; k2 < stor.objet_derivee.zeros_facteurs[i].length; k2++) {
          stor.les_zeros.push(stor.objet_derivee.zeros_facteurs[i][k2])
        }
      }
      stor.lesSignes = fctsEtudeSigneFct(stor.objet_derivee.expres_derivee, stor.defFonction.domaineDeriv, stor.les_zeros, ds.debug)
      // tab_signes contient alors le signe de la fonction dérivée sur chacun des intervalles.
      me.logIfDebug('stor.les_zeros:' + stor.les_zeros + '  stor.lesSignes:' + stor.lesSignes)
      let paletteAvecExp = false
      if (stor.defFonction.fonction.includes('exp') || stor.defFonction.fonction.includes('ln') || stor.defFonction.fonction.includes('e^') || stor.defFonction.fonction.includes('mathrm{e}')) {
        paletteAvecExp = true
      }
      stor.lesZones = fctsEtudeTableauSignesFct(stor.tableau1, stor.dimension_tab, [false, false], stor.les_zeros, stor.lesSignes, {
        style: me.styles.toutpetit.enonce,
        texte: textes.signe3,
        nom_f: stor.nom_f
      }, paletteAvecExp)
    } else {
      // l’autre est le tableau de signe d’un produit
      // tout d’abord le tableau contenant les facteurs
      stor.expres_tab = []
      for (let i = 0; i < stor.objet_derivee.tabFacteurs[0].length; i++) {
        if (stor.defFonction.tabFacteursPositifsTrouves.indexOf(stor.objet_derivee.tabFacteurs[0][i]) === -1) {
          // le facteur n’a pas été relevé par l’élève
          stor.expres_tab.push(stor.objet_derivee.tabFacteurs[0][i])
        }
      }
      for (let i = 0; i < stor.objet_derivee.tabFacteurs[1].length; i++) {
        if (stor.defFonction.tabFacteursPositifsTrouves.indexOf(stor.objet_derivee.tabFacteurs[1][i]) === -1) {
          // le facteur n’a pas été relevé par l’élève
          stor.expres_tab.push(stor.objet_derivee.tabFacteurs[1][i])
        }
      }
      // puis je gère la taille du tableau
      stor.hauteur_tab = stor.taille_ligne * (stor.expres_tab.length + 2)
      stor.dimension_tab = [stor.largeur_tab, stor.hauteur_tab]
      // nombre de valeur sur la ligne des x en plus des bornes :
      stor.dimension_tab.push(0)
      stor.dimension_tab.push([borneInf, borneSup])
      // gestion de la nature de la fonction à évaluer (produit ou quotient)
      stor.type_fct = ['produit']
      if (stor.objet_derivee.tabFacteurs[1].length > 0) {
        // c’est que c’est un quotient et non un produit
        stor.type_fct = ['quotient']
        stor.type_fct.push([])
        // il faut alors que je gère numérateurs et dénominateurs
        for (let i = 0; i < stor.objet_derivee.tabFacteurs[0].length; i++) {
          stor.type_fct[1].push('num')
        }
        for (let i = 0; i < stor.objet_derivee.tabFacteurs[1].length; i++) {
          stor.type_fct[1].push('den')
        }
      }
      // maintenant je mets tous les zéros des facteurs présents dans un même tableau
      stor.les_zeros = []
      for (let i = 0; i < stor.expres_tab.length; i++) {
        if (stor.objet_derivee.zeros_facteurs[i].length > 0) {
          stor.les_zeros.push(stor.objet_derivee.zeros_facteurs[i])
        }
      }
      stor.lesSignes = stor.objet_derivee.signes_facteurs
      if (stor.type_fct[0] === 'produit') {
        stor.lesZones = fctsEtudeTabSignesProduit(stor.tableau1, stor.expres_tab, stor.dimension_tab, stor.type_fct, [false, false], stor.les_zeros, stor.lesSignes, {
          style: me.styles.toutpetit.enonce,
          texte: textes.signe1,
          texte2: textes.signe3
        })
      } else {
        stor.lesZones = fctsEtudeTabSignesProduit(stor.tableau1, stor.expres_tab, stor.dimension_tab, stor.type_fct, [false, false], stor.les_zeros, stor.lesSignes, {
          style: me.styles.toutpetit.enonce,
          texte: textes.signe2,
          texte2: textes.signe3
        })
      }
    }
    stor.nb_valx = 0
    constructionDiv5()
    j3pStyle(stor.zoneCons6, { fontSize: '0.9em', fontStyle: 'italic', paddingBottom: '5px' })
    j3pAffiche(stor.zoneCons6, '', textes.remarque)
    me.logIfDebug('stor.les_zeros:' + stor.les_zeros + '  stor.lesSignes:' + stor.lesSignes.map(tab => '[' + String(tab) + ']') + '  length:' + stor.lesSignes.length)

    setTimeout(declarationZoneAvalider, 0)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  } // enonceMain

  function actionBtnPlus () {
    // c’est fonction sera appelée pour recréer le tableau de signe avec une valeur de plus dans la première ligne
    stor.nb_valx++
    stor.dimension_tab[2] = stor.nb_valx
    if (stor.nb_valx === 5) j3pDetruit(stor.lediv5)
    if (stor.nb_valx === 1) {
      // c’est que le div 6 n’était pas encore construit, donc on le fait.
      constructionDiv6()
    }
    // on reconstruit ensuite le tableau de signes
    actionBtnFin()
  }

  function actionBtnMoins () {
    // c’est fonction sera appelée pour recréer le tableau de signe avec une valeur de moins dans la première ligne
    stor.nb_valx--
    stor.dimension_tab[2] = stor.nb_valx
    if (stor.nb_valx === 0) j3pDetruit(stor.lediv6)
    if (stor.nb_valx === 4) {
      // c’est que le div 6 n’était pas encore construit, donc on le fait.
      constructionDiv5()
    }
    stor.btnsPresents = true
    // on reconstruit ensuite le tableau de signes
    actionBtnFin()
  }

  function actionBtnFin () {
    j3pEmpty(stor.tableau1)
    const paletteAvecExp = stor.defFonction.fonction.includes('exp') ||
        stor.defFonction.fonction.includes('ln') ||
        stor.defFonction.fonction.includes('e^')
    if (stor.nb_facteurs_restants <= 1) {
      stor.lesZones = fctsEtudeTableauSignesFct(stor.tableau1, stor.dimension_tab, [false, false], stor.les_zeros, stor.lesSignes, {
        style: me.styles.toutpetit.enonce,
        texte: textes.signe3,
        nom_f: stor.nom_f
      }, paletteAvecExp)
    } else {
      const txtSigne = (stor.type_fct[0] === 'produit') ? textes.signe1 : textes.signe2
      stor.lesZones = fctsEtudeTabSignesProduit(stor.tableau1, stor.expres_tab, stor.dimension_tab, stor.type_fct, [false, false], stor.les_zeros, stor.lesSignes, {
        style: me.styles.toutpetit.enonce,
        texte: txtSigne,
        texte2: textes.signe3
      })
    }
    setTimeout(declarationZoneAvalider, 0)
  }

  function constructionDiv5 () {
    stor.lediv5 = j3pAddElt(stor.zoneCons5, 'div')
    j3pAffiche(stor.lediv5, '', textes.phrase5)
    j3pAjouteBouton(stor.lediv5, actionBtnPlus, { value: '+', className: 'MepBoutons2' })
  }

  function constructionDiv6 () {
    stor.lediv6 = j3pAddElt(stor.zoneCons5, 'div')
    j3pAffiche(stor.lediv6, '', textes.phrase6)
    j3pAjouteBouton(stor.lediv6, actionBtnMoins, { value: '-', className: 'MepBoutons2' })
  }

  function declarationZoneAvalider () {
    // on récupère la liste avec le nom de toutes les zones de saisie ainsi que les listes déroulantes
    // nom du div accueillant le tableau (et donc toutes les zones : stor.tableau1
    // stor.nb_valx+2 est le nombre de zones de saisie.
    const zonesSaisie = []
    const validPerso = []
    // var tableauZerosOrdonne = fctsEtudeOrdonneTabSansDoublon(stor.les_zeros,stor.defFonction.domaineDeriv);
    stor.lesZones.input.ligneX.forEach(eltInput => zonesSaisie.push(eltInput.id))
    if (stor.nb_facteurs_restants <= 1) {
      // je suis avec un tableau à une deux lignes (pas de produit)
      stor.lesZones.input.signeFacts.forEach(eltInput => {
        zonesSaisie.push(eltInput.id)
        eltInput.typeReponse = ['texte']
      })
      stor.lesZones.liste.forEach(lstInput => {
        zonesSaisie.push(lstInput.id)
        lstInput.typeReponse = [false] // pour accepter qu’on ne modifie pas la valeur par défaut
      })
    } else {
      // on récupère tous les signes attendus :
      // je teste le signe de chaque facteur, puis le signe du produit/quotient
      stor.lesZones.input.signeFacts.forEach(eltInput => zonesSaisie.push(eltInput.id))
      stor.lesZones.liste.forEach(lstInput => {
        zonesSaisie.push(lstInput.id)
        lstInput.typeReponse = [false] // pour accepter qu’on ne modifie pas la valeur par défaut
      })
    }
    j3pFocus(stor.lesZones.input.ligneX[0])
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: zonesSaisie,
      validePerso: validPerso
    })
  }

  function tableauSigneRep (valZeroTab, lesSignes, domaine) {
    // valZeroTab est le tableau qui contient les endroits où les fonctions s’annulent
    // ces endroit sont des number ou des string (si j’ai 2 valeurs, alors elles sont séparées par |)
    // ici toutes ses valeurs sont différentes'
    // lesSignes est un tableau de tableaux de style ["+","-"] ou ["-","+"]" voire ["+"] ou ["+","-","+"]
    // il y a autant de signes que de changements de signes du facteur + 1
    const tabZeroOrdonne = fctsEtudeOrdonneTabSansDoublon(valZeroTab, domaine)
    const nbSigne = (tabZeroOrdonne.length + 1) * (lesSignes.length)// -1);
    // le tableau suivant va servir pour le signe du produit ou du quotient
    const nbSignesMoinsTab = []
    // le tableau suivant accueille tous les signes des zones de saisie
    const signeSaisie = []
    for (let i = 1; i <= tabZeroOrdonne.length + 1; i++) {
      nbSignesMoinsTab[i] = 0
    }
    for (let i = 1; i <= nbSigne; i++) {
      // je cherche l’endroit où s’annule mon expression (si elle s’annule)
      const numLigne = Math.ceil(i / (tabZeroOrdonne.length + 1))
      const numColonne = (i - 1) % (tabZeroOrdonne.length + 1) + 1
      if (numColonne === tabZeroOrdonne.length + 1) {
        // dans ce cas, nous sommes sur la dernière colonne du tableau
        if (lesSignes[numLigne - 1][lesSignes[numLigne - 1].length - 1] === '-') {
          nbSignesMoinsTab[numColonne] += 1
          signeSaisie[i] = '-'
        } else {
          signeSaisie[i] = '+'
        }
      } else {
        if (lesSignes[numLigne - 1].length === 1) {
          // la fonction ne s’annule pas'
          if (lesSignes[numLigne - 1][0] === '-') {
            nbSignesMoinsTab[numColonne] += 1
            signeSaisie[i] = '-'
          } else {
            signeSaisie[i] = '+'
          }
        } else if (!String(valZeroTab[numLigne - 1]).includes('|')) {
          // la fonction de cette ligne ne s’annule pas plus d’une fois'
          if (!!valZeroTab[numLigne - 1] && fctsEtudeCalculValeur(valZeroTab[numLigne - 1]) >= fctsEtudeCalculValeur(tabZeroOrdonne[numColonne - 1])) {
            if (lesSignes[numLigne - 1][0] === '-') {
              nbSignesMoinsTab[numColonne] += 1
              signeSaisie[i] = '-'
            } else {
              signeSaisie[i] = '+'
            }
          } else {
            if (lesSignes[numLigne - 1][1] === '-') {
              nbSignesMoinsTab[numColonne] += 1
              signeSaisie[i] = '-'
            } else {
              signeSaisie[i] = '+'
            }
          }
        } else {
          let tableauValZeroNumLigne = String(valZeroTab[numLigne - 1]).split('|')
          tableauValZeroNumLigne = fctsEtudeOrdonneTabSansDoublon(tableauValZeroNumLigne, domaine)
          me.logIfDebug('tableauValZeroNumLigne:' + tableauValZeroNumLigne)
          let k = 0
          while (fctsEtudeCalculValeur(tabZeroOrdonne[numColonne - 1]) > fctsEtudeCalculValeur(tableauValZeroNumLigne[k])) {
            k++
          }
          if (lesSignes[numLigne - 1][k] === '-') {
            nbSignesMoinsTab[numColonne] += 1
            signeSaisie[i] = '-'
          } else {
            signeSaisie[i] = '+'
          }
        }
      }
    }
    me.logIfDebug('nbSignesMoinsTab:' + nbSignesMoinsTab)
    // signes de la dernière ligne (celle du produit ou du quotient)
    for (let i = 1; i <= tabZeroOrdonne.length + 1; i++) {
      if (nbSignesMoinsTab[i] % 2 === 0) {
        signeSaisie[nbSigne + i] = '+'
      } else {
        signeSaisie[nbSigne + i] = '-'
      }
    }
    return signeSaisie
  } // fin tableauSigneRep

  function afficheCorr (bonneReponse) {
    j3pEmpty(stor.lesZones.palette)
    j3pEmpty(stor.zoneCons5)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    if ((stor.defFonction.modele === 2) || (stor.defFonction.modele === 4)) {
      // on cherche la valeur de delta
      const tabVariables = stor.objet_derivee.variables// on retrouve les coefficients du pol de degré 3
      // cela va nous servir à retrouver le signe du discriminant de la dérivée
      let delta, deltaTxt
      if (stor.defFonction.modele === 2) {
        delta = Math.pow(fctsEtudeCalculValeur(tabVariables[2]) * 2, 2) - 4 * fctsEtudeCalculValeur(tabVariables[3]) * 3 * fctsEtudeCalculValeur(tabVariables[1])
        deltaTxt = fctsEtudeSommeNbs(fctsEtudeProduitNbs(fctsEtudeProduitNbs('4', tabVariables[2]), tabVariables[2]), fctsEtudeProduitNbs('-1', fctsEtudeProduitNbs(4, fctsEtudeProduitNbs(fctsEtudeProduitNbs(tabVariables[3], 3), tabVariables[1]))))
      } else {
        delta = tabVariables[8]
        deltaTxt = tabVariables[7]
      }
      let textCorr2, tabRacines
      if (Math.abs(delta) < Math.pow(10, -12)) { // delta = 0
        if (stor.defFonction.modele === 2) {
          textCorr2 = textes.corr2bis
        } else if (stor.defFonction.modele === 4) {
          textCorr2 = textes.corr4bis
        }
        if (ds.imposer_domaine.length !== 0) {
          textCorr2 += '<br>' + textes.corr3bis
        } else {
          textCorr2 += '<br>' + textes.corr3
        }
        tabRacines = stor.objet_derivee.zeros_facteurs_init[0][0]
        j3pAffiche(stor.zoneExpli1, '', textCorr2, { x: tabRacines[0] })
      } else if (delta > 0) {
        let lequation = ''
        if (stor.defFonction.modele === 2) {
          textCorr2 = textes.corr2
        } else if (stor.defFonction.modele === 4) {
          if (Math.abs(fctsEtudeCalculValeur(tabVariables[5])) < Math.pow(10, -12)) {
            // dans la dérivée, le monome en x est nul
            textCorr2 = textes.corr44
          } else if (Math.abs(fctsEtudeCalculValeur(tabVariables[6])) < Math.pow(10, -12)) {
            // dans la dérivée, le monome en x est nul
            textCorr2 = textes.corr46
          } else {
            textCorr2 = textes.corr4
          }
          lequation = fctsEtudeEcrireMonome(1, 2, tabVariables[4]) + fctsEtudeEcrireMonome(2, 1, tabVariables[5]) + fctsEtudeEcrireMonome(3, 0, tabVariables[6]) + '=0'
        }
        if (ds.imposer_domaine.length !== 0) {
          textCorr2 += '<br>' + textes.corr3bis
        } else {
          textCorr2 += '<br>' + textes.corr3
        }
        tabRacines = stor.objet_derivee.zeros_facteurs_init[0][0].split('|')
        j3pAffiche(stor.zoneExpli1, '', textCorr2, { d: deltaTxt, x: tabRacines[0], y: tabRacines[1], e: lequation })
      } else {
        if (stor.defFonction.modele === 2) {
          textCorr2 = textes.corr2ter
        } else if (stor.defFonction.modele === 4) {
          textCorr2 = textes.corr4ter
        }
        textCorr2 += '<br>' + textes.corr3
        j3pAffiche(stor.zoneExpli1, '', textCorr2, { d: deltaTxt })
      }
    } else if (stor.defFonction.modele === 6) {
      if (stor.nb_facteurs_restants >= 1) {
        // on ne donne une explication que si le numérateur n’est pas strictement positif (ce qui pourrait arriver avec un domaine particulier)
        const coefBprime = j3pCalculValeur(stor.objet_derivee.variables[4])
        me.logIfDebug('objet_derivee.variables[4]:' + stor.objet_derivee.variables[4] + '   coefBprime:' + coefBprime)
        const ineq1 = stor.objet_derivee.tabFacteurs[0][0] + '>0'
        const signeIneq = (coefBprime > 0) ? '>' : '<'
        const ineq2 = 'x' + signeIneq + String(stor.les_zeros[0])
        j3pAffiche(stor.zoneExpli1, '', textes.corr5 + '\n' + textes.corr3,
          { e: ineq1, f: ineq2 })
      }
    } else {
      j3pAffiche(stor.zoneExpli1, '', textes.corr1)
    }
    stor.tableauCorr = j3pAddElt(stor.zoneExpli2, 'div', '', { style: me.styles.etendre('toutpetit.correction', { position: 'relative', top: 0, left: 0 }) })
    stor.dimension_tab.push(stor.defFonction.domaineDeriv)
    if (stor.nb_facteurs_restants <= 1) {
      if (bonneReponse) {
        fctsEtudeTableauSignesFct(stor.tableauCorr, stor.dimension_tab, [true, true], stor.les_zeros, stor.lesSignes, {
          couleur: me.styles.cbien,
          texte: textes.signe3,
          nom_f: stor.nom_f
        })
      } else {
        fctsEtudeTableauSignesFct(stor.tableauCorr, stor.dimension_tab, [true, true], stor.les_zeros, stor.lesSignes, {
          couleur: me.styles.petit.correction.color,
          texte: textes.signe3,
          nom_f: stor.nom_f
        })
      }
    } else {
      if (bonneReponse) {
        if (stor.type_fct[0] === 'produit') {
          fctsEtudeTabSignesProduit(stor.tableauCorr, stor.expres_tab, stor.dimension_tab, stor.type_fct, [true, true], stor.les_zeros, stor.lesSignes, {
            couleur: me.styles.cbien,
            texte: textes.signe1,
            texte2: textes.signe3
          })
        } else {
          fctsEtudeTabSignesProduit(stor.tableauCorr, stor.expres_tab, stor.dimension_tab, stor.type_fct, [true, true], stor.les_zeros, stor.lesSignes, {
            couleur: me.styles.cbien,
            texte: textes.signe2,
            texte2: textes.signe3
          })
        }
      } else {
        if (stor.type_fct[0] === 'produit') {
          fctsEtudeTabSignesProduit(stor.tableauCorr, stor.expres_tab, stor.dimension_tab, stor.type_fct, [true, true], stor.les_zeros, stor.lesSignes, {
            couleur: me.styles.petit.correction.color,
            texte: textes.signe1,
            texte2: textes.signe3
          })
        } else {
          fctsEtudeTabSignesProduit(stor.tableauCorr, stor.expres_tab, stor.dimension_tab, stor.type_fct, [true, true], stor.les_zeros, stor.lesSignes, {
            couleur: me.styles.petit.correction.color,
            texte: textes.signe2,
            texte2: textes.signe3
          })
        }
      }
    }
  }

  // console.log("debut du code : ",me.etat)

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.logIfDebug('ds.imposer_fct : ', ds.imposer_fct)
        // Construction de la page
        me.construitStructurePage('presentation1')
        enonceInit()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }

      break // case "enonce":

    case 'correction': {
      const reponse = {}
      reponse.aRepondu = true
      reponse.bonneReponse = true
      const fctsValid = stor.fctsValid
      reponse.aRepondu = fctsValid.valideReponses()
      // on vérifie qu’elles ont bien été complétées par reponse.aRepondu
      // on récupère tous les signes attendus :
      const newDomaine = fctsEtudeTransformationDomaine(stor.defFonction.domaineDeriv)
      // je dois mettre une sorte de rustine quand j’ai le tableau de signe d’un produit ou d’un quotient
      // car dans ce cas, stor.les_zeros est un array d’arrays
      // Or il devra être utilisé avec fctsEtudeOrdonneTabSansDoublon qui veut un tableau de number ou string
      // les string pouvant être sous la forme "nb1|nb2"
      const valZeroPourOrdonne = (stor.nb_facteurs_restants <= 1)
        ? [...stor.les_zeros]
        : stor.les_zeros.map(elt => elt.join('|'))
      let signesRep = tableauSigneRep(valZeroPourOrdonne, stor.lesSignes, newDomaine)
      signesRep.push(true)
      let tableauZerosOrdonne
      let bonnePremiereLigne = false
      let signeFacteurInexact = false
      if (reponse.aRepondu) {
        // il faut que j’adapte le domaine de dérivabilité dans le cas d’une valeur interdite
        // en effet, cette valeur n’est pas dans le domaine, mais elle fera partie des valeurs à écrire dans la première ligne donc je la veux dans tableauZerosOrdonne
        tableauZerosOrdonne = fctsEtudeOrdonneTabSansDoublon(valZeroPourOrdonne, newDomaine)
        if (tableauZerosOrdonne.length !== stor.nb_valx) {
          // c’est qu’il n’y a pas le bon nombre de valeurs dans la première ligne
          me.logIfDebug('le nombre de zéros donnés n’est pas le bon')
          // on considère que tout est faux :
          let nbZoneTraitee = 0
          for (let i = 0; i <= stor.nb_valx + 1; i++) {
            fctsValid.zones.bonneReponse[i] = false
            fctsValid.coloreUneZone(stor.lesZones.input.ligneX[i].id)
            nbZoneTraitee++
          }
          if (stor.nb_facteurs_restants <= 1) {
            for (let i = 1; i <= stor.nb_valx + 1; i++) {
              fctsValid.zones.bonneReponse[nbZoneTraitee + i] = false
              nbZoneTraitee++
              fctsValid.coloreUneZone(stor.lesZones.input.signeFacts[i - 1].id)
            }
            if (stor.nb_valx > 0) {
              for (let i = 1; i <= stor.nb_valx; i++) {
                fctsValid.zones.bonneReponse[nbZoneTraitee + i] = false
                nbZoneTraitee++
                fctsValid.coloreUneZone(stor.lesZones.liste[i - 1].id)
              }
            }
          } else {
            for (let i = 1; i <= (stor.nb_valx + 1) * (stor.expres_tab.length + 1); i++) {
              fctsValid.zones.bonneReponse[nbZoneTraitee + i] = false
              nbZoneTraitee++
              fctsValid.coloreUneZone(stor.lesZones.input.signeFacts[i - 1].id)
            }
            if (stor.nb_valx > 0) {
              for (let i = 1; i <= (stor.nb_valx) * (stor.expres_tab.length + 1); i++) {
                fctsValid.zones.bonneReponse[nbZoneTraitee + i] = false
                nbZoneTraitee++
                fctsValid.coloreUneZone(stor.lesZones.liste[i - 1].id)
              }
            }
          }
          reponse.bonneReponse = false
        } else {
          // c’est qu’il y a le bon nombre de valeurs dans la première ligne
          // on ne peut alors plus modifier la taille du tableau (c’est-à-dire qu’il faut détruire les boutons + et -
          if (stor.btnsPresents) {
            j3pEmpty(stor.zoneCons5)
            j3pEmpty(stor.zoneCons6)
            stor.btnsPresents = false
          }
          // je donne toutes les réponses attendues
          for (let i = 0; i <= stor.nb_valx + 1; i++) {
            const eltInput = stor.lesZones.input.ligneX[i]
            if (!Object.isFrozen(eltInput)) {
              if (i === 0) {
                // attention car c’est la borne inf du domaine

                if (stor.defFonction.domaineDeriv[0] === '-infini') {
                  eltInput.typeReponse = ['texte']
                  eltInput.reponse = ['-\\infty']
                } else {
                  eltInput.reponse = fctsEtudeCalculValeur(stor.defFonction.domaineDeriv[0])
                  eltInput.typeReponse = ['nombre', 'exact']
                }
              } else if (i === stor.nb_valx + 1) {
                // cette fois-ci c’est la borne sup du domaine
                const posBorneSup = Math.floor((stor.defFonction.domaineDeriv.length - 1) / 4) * 4 + 1
                if (stor.defFonction.domaineDeriv[posBorneSup] === '+infini') {
                  eltInput.typeReponse = ['texte']
                  eltInput.reponse = ['+\\infty']
                } else {
                  eltInput.reponse = fctsEtudeCalculValeur(stor.defFonction.domaineDeriv[posBorneSup])
                  eltInput.typeReponse = ['nombre', 'exact']
                }
              } else {
                eltInput.reponse = fctsEtudeCalculValeur(tableauZerosOrdonne[i - 1])
                eltInput.typeReponse = ['nombre', 'exact']
              }
            }
          }
          if (stor.nb_facteurs_restants <= 1) {
            signesRep = fctsEtudeSigneFct(stor.objet_derivee.expres_derivee, stor.defFonction.domaineDeriv, stor.les_zeros, ds.debug)
            // je suis avec un tableau à deux lignes (pas de produit)
            for (let i = 1; i <= stor.nb_valx + 1; i++) {
              const eltSigne = stor.lesZones.input.signeFacts[i - 1]
              if (!Object.isFrozen(eltSigne)) {
                eltSigne.reponse = [signesRep[i - 1]]
                eltSigne.typeReponse = ['texte']
              }
            }
            // par défaut, les listes déroulantes de la dernière ligne vont contenir zéro
            for (let i = 1; i <= stor.nb_valx; i++) {
              const eltListe = stor.lesZones.liste[i - 1]
              if (!Object.isFrozen(eltListe)) {
                eltListe.reponse = 2
                if (stor.objet_derivee.zeros_facteurs[1].join().includes(tableauZerosOrdonne[i - 1])) {
                  // c’est le dénominateur qui s’annule, donc c’est une valeur interdite pour la dernière ligne
                  eltListe.reponse = 1
                }
              }
            }
          } else {
            // je teste le signe de chaque facteur, puis le signe du produit/quotient
            for (let i = 1; i <= (stor.nb_valx + 1) * (stor.expres_tab.length + 1); i++) {
              const eltSigne = stor.lesZones.input.signeFacts[i - 1]
              if (!Object.isFrozen(eltSigne)) {
                eltSigne.reponse = [signesRep[i]]
                eltSigne.typeReponse = ['texte']
              }
            }
            // par défaut, les listes déroulantes vont contenir la barre verticale
            for (let i = 1; i <= stor.nb_valx * (stor.expres_tab.length + 1); i++) {
              const eltListe = stor.lesZones.liste[i - 1]
              if (!Object.isFrozen(eltListe)) eltListe.reponse = 0
            }
            // celles qui suivent sont les listes des lignes de chaque facteur
            for (let i = 1; i <= stor.nb_valx * stor.expres_tab.length; i++) {
              const numLigne = Math.floor((i - 1) / stor.nb_valx)
              const numColonne = (i - 1) % stor.nb_valx
              const arbreFacteur = new Tarbre(fctsEtudeEcriturePourCalcul(stor.expres_tab[numLigne]), ['x'])
              const xValeur = fctsEtudeCalculValeur(tableauZerosOrdonne[numColonne])
              const yValeur = arbreFacteur.evalue([xValeur])
              // là il faut que je détermine les listes dans lesquelles on trouve 0
              if (Math.abs(yValeur) < Math.pow(10, -10)) {
                // c’est que le facteur s’annule en cette valeur
                const eltListe = stor.lesZones.liste[i - 1]
                if (!Object.isFrozen(eltListe)) eltListe.reponse = 1
              }
            }
            // les listes de la dernière ligne
            for (let i = stor.nb_valx * stor.expres_tab.length + 1; i <= stor.nb_valx * (stor.expres_tab.length + 1); i++) {
              const eltListe = stor.lesZones.liste[i - 1]
              if (!Object.isFrozen(eltListe)) {
                // Le join paraît inutile, mais ne l’est pas : il sert dans le cadre du test d’une fraction
                if (stor.objet_derivee.zeros_facteurs[1].join().includes(tableauZerosOrdonne[i - stor.nb_valx * stor.expres_tab.length - 1])) {
                  eltListe.reponse = 1 // valeur interdite
                } else {
                  eltListe.reponse = 2 // zéro du produit/quotient
                }
              }
            }
          }
          // Je teste d’abord si la première ligne est correcte
          // si ce n’est pas le cas, je mets tout le tableau en rouge
          bonnePremiereLigne = true
          const premiereLigne = []
          for (let i = 0; i <= stor.nb_valx + 1; i++) {
            if (fctsValid.zones.isActive[fctsValid.zones.inputs.indexOf(stor.lesZones.input.ligneX[i])]) {
              premiereLigne[i] = fctsValid.valideUneZone(stor.lesZones.input.ligneX[i].id, stor.lesZones.input.ligneX[i].reponse).bonneReponse
              bonnePremiereLigne = (bonnePremiereLigne && premiereLigne[i])
              fctsValid.zones.bonneReponse[i] = premiereLigne[i]
            } else {
              premiereLigne[i] = true
            }
          }
          if (bonnePremiereLigne) {
            if (stor.nb_facteurs_restants <= 1) {
              // il n’y a plus qu’une seule ligne à vérifier
              const deuxiemeLigne = []
              let bonneDeuxiemeLigne = true
              for (let i = 1; i <= stor.nb_valx + 1; i++) {
                deuxiemeLigne[i] = fctsValid.valideUneZone(stor.lesZones.input.signeFacts[i - 1].id, stor.lesZones.input.signeFacts[i - 1].reponse).bonneReponse
                bonneDeuxiemeLigne = (bonneDeuxiemeLigne && deuxiemeLigne[i])
                fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(stor.lesZones.input.signeFacts[i - 1])] = deuxiemeLigne[i]
              }
              // il y a aussi les listes déroulantes
              const deuxiemeLigneListe = []
              for (let i = 1; i <= stor.nb_valx; i++) {
                deuxiemeLigneListe[i] = fctsValid.valideUneZone(stor.lesZones.liste[i - 1].id, stor.lesZones.liste[i - 1].reponse).bonneReponse
                bonneDeuxiemeLigne = (bonneDeuxiemeLigne && deuxiemeLigneListe[i])
                fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(stor.lesZones.liste[i - 1])] = deuxiemeLigneListe[i]
              }
              bonneDeuxiemeLigne = (bonneDeuxiemeLigne && deuxiemeLigneListe)
              reponse.bonneReponse = (bonneDeuxiemeLigne)
              // je mets toutes les zones et listes de la deuxième liste en rouge si c’est faux et en bleu/vert sinon
              for (let i = 1; i <= stor.nb_valx + 1; i++) {
                fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(stor.lesZones.input.signeFacts[i - 1])] = bonneDeuxiemeLigne
              }
              for (let i = 1; i <= stor.nb_valx; i++) {
                fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(stor.lesZones.liste[i - 1])] = bonneDeuxiemeLigne
              }
              if (stor.nb_valx === 0) {
                // il n’y a qu’un signe donc je ne lui donne pas la possibilité de se corriger
                me.essaiCourant = ds.nbchances
              }
              fctsValid.coloreLesZones()
            } else {
              // On contrôle chaque ligne en commençant par le signe de chaque facteur
              reponse.bonneReponse = true
              const bonneLigne = []
              const lastJ = stor.expres_tab.length + 1
              const elementLigneListe = []
              for (let j = 1; j <= lastJ; j++) {
                bonneLigne[j] = true
                const elementLigneOK = []
                for (let i = 1; i <= stor.nb_valx + 1; i++) {
                  const eltInput = stor.lesZones.input.signeFacts[i - 1 + (j - 1) * (stor.nb_valx + 1)]
                  elementLigneOK[i] = fctsValid.valideUneZone(eltInput.id, eltInput.reponse).bonneReponse
                  bonneLigne[j] = (bonneLigne[j] && elementLigneOK[i])
                  fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(eltInput)] = elementLigneOK[i]
                }
                // il faut aussi que je gère les liste déroulantes
                let numListeInit, numListeFin
                if (j === lastJ) {
                  // je suis sur la dernière ligne (je récupère les index des listes dans le tableau)
                  numListeInit = stor.nb_valx * stor.expres_tab.length
                  numListeFin = stor.nb_valx * lastJ - 1
                } else {
                  // Là les numéros des listes vont de (j-1)*nb_valx à j*nb_valx
                  numListeInit = (j - 1) * (stor.nb_valx)
                  numListeFin = numListeInit + stor.nb_valx - 1
                }
                for (let k = numListeInit; k <= numListeFin; k++) {
                  elementLigneListe[k] = fctsValid.valideUneZone(stor.lesZones.liste[k].id, stor.lesZones.liste[k].reponse).bonneReponse
                  bonneLigne[j] = (bonneLigne[j] && elementLigneListe[k])
                  fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(stor.lesZones.liste[k])] = elementLigneListe[k]
                }
                reponse.bonneReponse = (reponse.bonneReponse && bonneLigne[j])
                // finalement on met en rouge toute la ligne qui serait fausse
                if (!bonneLigne[j]) {
                  if (j <= stor.expres_tab.length) {
                    // c’est l’un des facteurs qui est faux
                    signeFacteurInexact = true
                  }
                  for (let i = 1; i <= stor.nb_valx + 1; i++) {
                    const eltInput = stor.lesZones.input.signeFacts[i - 1 + (j - 1) * (stor.nb_valx + 1)]
                    fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(eltInput)] = false
                  }
                  for (let k = numListeInit; k <= numListeFin; k++) {
                    fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(stor.lesZones.liste[k])] = false
                  }
                }
              }
              if (signeFacteurInexact) {
                // dans ce cas là, je mets la dernière ligne en rouge
                for (let i = 1; i <= stor.nb_valx + 1; i++) {
                  const eltInput = stor.lesZones.input.signeFacts[i - 1 + (lastJ - 1) * (stor.nb_valx + 1)]
                  fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(eltInput)] = false
                }
                for (let k = stor.nb_valx * stor.expres_tab.length; k < stor.lesZones.liste.length; k++) {
                  fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(stor.lesZones.liste[k])] = false
                }
              }
              fctsValid.coloreLesZones()
            }
          } else {
            // je mets tout le tableau en rouge
            if (stor.nb_facteurs_restants <= 1) {
              for (let i = 1; i <= stor.nb_valx + 1; i++) {
                me.logIfDebug('monsigne' + i + ':' + $(stor.lesZones.input.signeFacts[i - 1]).mathquill('latex'))
                fctsValid.zones.bonneReponse[stor.nb_valx + 1 + i] = false
                // j’ai mis toutes les zones de saisie accueillant un signe à bonneReponse = false
              }
              for (let i = 1; i <= stor.nb_valx; i++) {
                fctsValid.zones.bonneReponse[2 * stor.nb_valx + 2 + i] = false
                // je mets aussi toutes les listes à bonneReponse = false
              }
            } else {
              for (let i = 1; i <= (stor.nb_valx + 1) * (stor.expres_tab.length + 1); i++) {
                me.logIfDebug('monsigne' + i + ':' + $(stor.lesZones.input.signeFacts[i - 1]).mathquill('latex'))
                fctsValid.zones.bonneReponse[stor.nb_valx + 1 + i] = false
                // j’ai mis toutes les zones de saisie accueillant un signe à bonneReponse = false
              }
              for (let i = 1; i <= stor.nb_valx * (stor.expres_tab.length + 1); i++) {
                fctsValid.zones.bonneReponse[stor.nb_valx + 1 + (stor.nb_valx + 1) * (stor.expres_tab.length + 1) + i] = false
                // je mets aussi toutes les listes à bonneReponse = false
              }
            }
            reponse.bonneReponse = false
            fctsValid.coloreLesZones()
          }
        }
      }
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr, textes.comment1)
      } else {
        // Une réponse a été saisie (ou bien on est en timeout)
        // Bonne réponse
        if (reponse.bonneReponse) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          j3pEmpty(stor.zoneCons5)
          j3pEmpty(stor.zoneCons6)
          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.cacheBoutonValider()
            afficheCorr(false)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            // indication de mauvaise réponse
            // Pas d’inquiétude pour tableauZerosOrdonne : on ne passe ici que si reponse.aRepondu vaut true
            // donc ce tableau a bien été créé
            if (tableauZerosOrdonne.length !== stor.nb_valx) {
              // c’est qu’il n’y a pas le bon nombre de valeurs dans la première ligne
              stor.zoneCorr.innerHTML += '<br>' + textes.comment5
            } else if (!bonnePremiereLigne) {
              stor.zoneCorr.innerHTML += '<br>' + textes.comment2
            } else {
              if (stor.nb_facteurs_restants === 1) {
                if (stor.nb_valx === 0) {
                  // il n’y a qu’un signe donc je ne lui donne pas la possibilité de se corriger
                  // me.essaiCourant = ds.nbchances c’est déjà fait ligne 969
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment6_2
                } else {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment6
                }
              } else {
                // je regarde d’abord si ce n’est pas une ligne qui est incorrecte
                if (signeFacteurInexact) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment3
                } else {
                  // c’est juste le signe du produit/quotient
                  if (stor.objet_derivee.zeros_facteurs[1].length === 0) {
                    // produit
                    stor.zoneCorr.innerHTML += '<br>' + textes.comment4
                  } else {
                    stor.zoneCorr.innerHTML += '<br>' + textes.comment4bis
                  }
                }
              }
            }
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
            } else {
              // Erreur au nème essai
              afficheCorr(false)
              me._stopTimer()
              me.cacheBoutonValider()
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      me.finNavigation()
      break
  }
}
