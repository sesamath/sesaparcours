import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pMathquillXcas, j3pPaletteMathquill, j3pRestriction, j3pShowError } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { fctsEtudeEcritureDomaine, fctsEtudeEcritureLatexDerivee, fctsEtudeEcritureLatexFonction, fctsEtudeEcriturePourCalcul, fctsEtudeEgaliteTableaux, fctsEtudeExpressionAvecFacteurs, fctsEtudeNombreIntervalle, fctsEtudeTransformeExp } from 'src/legacy/outils/fonctions/etude'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

import { aideModeles, initModele, initSection } from './common'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

const epsilon = 1e-12

/*
    Rémi Deniaud
    Mars 2015
    Section intégrable dans un graphe avec données du graphe (on utilise la variable imposer_fct pour donner une fonction particulière suivant un modèle) ou donneesPrecedentes
    On demande les facteurs positifs de la dérivée afin de se faciliter la tâche dans la détermination de son signe
    En cas d’ajout de modèles, il faudra juste modifier les fonctions fctsEtudeGenereAleatoire et genereTextesCorrection puis ajouter les textes de correction qui vont bien
    Non implémenté pour l’instant : le retour des réponses de l’élève pour la section bilan (qui n’a jamais été finalisée)
    C’est en case correction qu’il faudra implémenter me.DonneesPersistantes.etudeFonctions.repEleve
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['imposer_fct', '', 'string', 'On peut renseigner une fonction correspondant au modèle ou laisser vide pour générer aléatoirement une fonction suivant un modèle'],
    ['modele', [1], 'array', aideModeles],
    ['imposer_domaine', [], 'array', 'On donne le domaine de définition sous la forme d’un tableau où chaque intervalle est représenté par 4 éléments : borne inf, borne sup et les deux crochets. Par exemple ["-infini","0","]","]"] pour l’intervalle ]-infini;0].'],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la fonction étudiée est celle du nœud précédent.']
  ],

  // NOUVEAUTE DP (penser à la virgule ci-dessus), pour l’instant inutilisé dans le modèle, mais plus tard dans l’éditeur de graphes pourquoi pas...
  donnees_parcours_description: {
    fonction: 'l’expression de la fonction en latex générée aléatoirement suivant 4 modèles, si le param imposer_fct n’est pas renseigné.',
    derivee: 'l’expression de la dérivée en latex.',
    modele: 'Nombre entier, le modèle 1 correspond à (ax+b)exp(x) , modèle 2 à ..., on utilise ensuite les pptés .a, .b, etc pour récupérer les variables.'
  },
  // pour fonctionnement dans editgraphes
  donnees_parcours_nom_variable: 'imposer_fct'
}

const textes = {
  titre: 'Dérivée et facteurs positifs',
  phrase1: 'On considère la fonction définie et dérivable sur $£a$ par : $f(x)=£b$.',
  phrase1_bis: 'On considère la fonction définie sur $£a$, dérivable sur $£c$ par : $f(x)=£b$.',
  phrase2: "Pour tout $x\\in £c$, $f\\quad '(x)=£g$",
  phrase2_ajout: "<br>($f\\quad'(x)$ est écrit sous forme factorisée).",
  phrase3: 'Précise le nombre de facteurs strictement positifs de la dérivée (entre 0 et 4) : @1@.',
  phrase4: 'Cite alors ces £n facteurs puis valide pour terminer l’étude du signe de la dérivée.',
  phrase4_bis: 'Cite alors ce facteur puis valide pour terminer l’étude du signe de la dérivée',
  phrase4_ter: 'Dans ce cas, valide pour terminer l’étude du signe de la dérivée',
  phrase4_4: 'Réfléchis bien il n’y en a pas autant !',
  comment1: 'C’est noté !',
  comment2: 'J’insiste : ce nombre ne peut pas être supérieur à 4 !',
  corr_1_1: 'On cherche alors le signe de $£d$ à l’aide d’un tableau de signes.',
  corr_1_2: "Sur $£a$, $£b$ donc $f\\quad '(x)$ est du signe de $£d$.",
  corr_1_2_bis: 'Sur $£a$, $£b$.',
  corr_1_3: "Sur $£a$, $£b$ et $£c$ donc $f\\quad '(x)$ est du signe de $£d$.",
  corr_1_3_bis: 'Sur $£a$, $£b$ et $£c$.',
  corr_1_4: 'On détermine alors son signe à l’aide d’un tableau.',
  corr_1_4_bis: 'On donne alors le signe de la dérivée dans un tableau.'
}
/**
 * section EtudeFonction_facteursderivee
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  function depart () {
    // je charge la figure mtg32 qui permet de vérifier si deux fonctions sont égales (en fait, on vérifie l’égalité sur 10 images)
    // txt html de la figure
    const txtFig = 'TWF0aEdyYXBoSmF2YTEuMAAAAA0+TMzNAANmcmH###8BAP8BAAAAAAAAAAAAAAAAAAAAAiYAAAGQAAAAAAAAAAAAAAAAAAAACv####8AAAABABFvYmpldHMuQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABABFvYmpldHMuQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEADG9iamV0cy5DRm9uYwD#####AAFmAAwoeCsxKSpleHAoeCn#####AAAAAQARb2JqZXRzLkNPcGVyYXRpb24CAAAAAwD#####AAAAAgAYb2JqZXRzLkNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAE#8AAAAAAAAP####8AAAACABBvYmpldHMuQ0ZvbmN0aW9uBwAAAAQAAAAAAAF4AAAAAgD#####AAFnAAd4Lyh4LTEpAAAAAwMAAAAEAAAAAAAAAAMBAAAABAAAAAAAAAABP#AAAAAAAAAAAXj#####AAAAAQAOb2JqZXRzLkNDYWxjdWwA#####wABYQACLTH#####AAAAAQATb2JqZXRzLkNNb2luc1VuYWlyZQAAAAE#8AAAAAAAAAAAAAYA#####wAEZmRlYQAEZihhKf####8AAAABABVvYmpldHMuQ0FwcGVsRm9uY3Rpb24AAAAB#####wAAAAEAFm9iamV0cy5DUmVzdWx0YXRWYWxldXIAAAADAAAABgD#####AARnZGVhAARnKGEpAAAACAAAAAIAAAAJAAAAAwAAAAYA#####wAJcHJlY2lzaW9uADgxMy1tYXgoaW50KGxuKGFicyhmZGVhKihmZGVhPD4wKSsyKihmZGVhPTApKSkvbG4oMTApKSwwKQAAAAMBAAAAAUAqAAAAAAAA#####wAAAAEAFG9iamV0cy5DRm9uY3Rpb24yVmFyAAAAAAUCAAAAAwMAAAAFBgAAAAUAAAAAAwAAAAADAgAAAAkAAAAEAAAAAwkAAAAJAAAABAAAAAEAAAAAAAAAAAAAAAMCAAAAAUAAAAAAAAAAAAAAAwgAAAAJAAAABAAAAAEAAAAAAAAAAAAAAAUGAAAAAUAkAAAAAAAAAAAAAQAAAAAAAAAAAAAABgD#####AAdlZ2FsaXRlAB5hYnMoZihhKS1nKGEpKTwxMF4oLXByZWNpc2lvbikAAAADBAAAAAUAAAAAAwEAAAAIAAAAAQAAAAkAAAADAAAACAAAAAIAAAAJAAAAA#####8AAAABABFvYmpldHMuQ1B1aXNzYW5jZQAAAAFAJAAAAAAAAAAAAAcAAAAJAAAABv####8AAAACAA1vYmpldHMuQ0xhdGV4AP####8AAAAAAAIAAAH#####AUBNAAAAAAAAQE2AAAAAAAAAAAAAAAAAAAAAAB5cdGV4dHtlZ2FsaXRlID0gfVxWYWx7ZWdhbGl0ZX3#####AAAAAQAVb2JqZXRzLkNUZXN0RXhpc3RlbmNlAP####8AAk9LAAAABf###############w=='
    stor.liste = stor.mtgAppLecteur.createList(txtFig)
  }

  function suiteEnonce () {
    // ce qui suit lance la création initiale de la figure
    depart()
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    initModele(me)

    const domaineDef = fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)
    const domaineDeriv = fctsEtudeEcritureDomaine(stor.defFonction.domaineDeriv)
    const laFonction = fctsEtudeEcritureLatexFonction(stor.defFonction.fonction, stor.defFonction.modele)
    if (fctsEtudeEgaliteTableaux(stor.defFonction.domaineDef, stor.defFonction.domaineDeriv)) {
      j3pAffiche(stor.zoneCons1, '', textes.phrase1,
        { a: domaineDef, b: laFonction })
    } else {
      j3pAffiche(stor.zoneCons1, '', textes.phrase1,
        { a: domaineDef, b: laFonction, c: domaineDeriv })
    }

    stor.objet_derivee = fctsEtudeEcritureLatexDerivee(laFonction, stor.defFonction.modele, stor.defFonction.domaineDeriv, ds.debug)
    // objet_derivee.expres_derivee est l’expression de la dérivée au format latex
    let maPhrase2 = textes.phrase2
    if ((stor.defFonction.modele === 2) || (stor.defFonction.modele === 4)) {
      // là on n’a pas de forme factorisée
      maPhrase2 += '.'
    } else {
      maPhrase2 += textes.phrase2_ajout
    }
    j3pAffiche(stor.zoneCons2, '', maPhrase2,
      { g: stor.objet_derivee.expres_derivee, c: domaineDeriv })
    const elt = j3pAffiche(stor.zoneCons3, '', textes.phrase3,
      {
        input1: { texte: '', dynamique: true, width: '12px', maxchars: 1 }
      })
    stor.zoneInput = [elt.inputList[0]]
    j3pRestriction(stor.zoneInput[0], '0-9')
    j3pFocus(stor.zoneInput[0])
    stor.zoneInput[0].addEventListener('input', creationZoneFacteur)

    // valeurs pour les calculs d’images
    const mesNombres = fctsEtudeNombreIntervalle(stor.defFonction.domaineDeriv)
    stor.tab_nombres = mesNombres.tests_images
    // je vais faire la liste ici des facteurs strictement positifs de la dérivée
    // cela signifie déjà que la fonction ne s’annule pas :
    // stor.objet_derivee.zeros_facteurs[i] est un tableau vide signifie que stor.objet_derivee.tabFacteurs[i] ne s’annule pas dans le domaine
    const listeFacteursPositifs = []
    const listeFacteursPositifsLatex = []
    const nbFacteurs = stor.objet_derivee.tabFacteurs[0].length + stor.objet_derivee.tabFacteurs[1].length
    let leFacteurLatex
    for (let i = 0; i < nbFacteurs; i++) {
      if (i < stor.objet_derivee.tabFacteurs[0].length) {
        // c’est un facteur du numérateur
        leFacteurLatex = stor.objet_derivee.tabFacteurs[0][i]
      } else {
        // c’est un facteur du dénominateur
        leFacteurLatex = stor.objet_derivee.tabFacteurs[1][i - stor.objet_derivee.tabFacteurs[0].length]
      }
      let leFacteur = j3pMathquillXcas(leFacteurLatex)
      while (leFacteur.indexOf('sqrt') > -1) {
        leFacteur.replace('sqrt', 'racine')
      }
      // avec l’exponentielle c’est un peu la galère : il faut remplacer e^... par exp(...)
      leFacteur = fctsEtudeTransformeExp(leFacteur)
      const arbreFacteur = new Tarbre(leFacteur, ['x'])
      const facteurTjsNonNul = ((stor.objet_derivee.zeros_facteurs[i].length === 0) || (i >= stor.objet_derivee.tabFacteurs[0].length))
      // il y a 2 cas de figure dans lequel le facteur ne s’annule pas :
      // -zeros_facteurs[i] est vide
      // -zeros_facteurs[i] n’est pas vide mais le facteur est au dénominateur (le ou les zéros sont dans ce cas des valeurs interdites)
      if (facteurTjsNonNul) {
        // la fonction ne s’annule pas sur le domaine
        if (stor.defFonction.domaineDeriv.length === 4) {
          // un seul intervalle dans le domaine de définition
          const valeurAEvaluer = mesNombres.nombre
          if (arbreFacteur.evalue([valeurAEvaluer]) > 0) {
            listeFacteursPositifs.push(leFacteur)
            listeFacteursPositifsLatex.push(leFacteurLatex)
          }
        } else {
          // deux intervalles dans le domaine de définition (on ne devrait pas avoir d’exemple avec plus...)
          const valeurAEvaluer1 = mesNombres.nombre
          const mesNombres2 = fctsEtudeNombreIntervalle([stor.defFonction.domaineDeriv[4], stor.defFonction.domaineDeriv[5]])
          const valeurAEvaluer2 = mesNombres2.nombre
          if (stor.tab_nombres.length < 10) {
            stor.tab_nombres = mesNombres2.tests_images
          }
          if ((arbreFacteur.evalue([valeurAEvaluer1]) > 0) && (arbreFacteur.evalue([valeurAEvaluer2]) > 0)) {
            listeFacteursPositifs.push(leFacteur)
            listeFacteursPositifsLatex.push(leFacteurLatex)
          }
        }
      }
    }

    stor.listeFacteursPositifs = listeFacteursPositifs
    stor.listeFacteursPositifsLatex = listeFacteursPositifsLatex

    me.logIfDebug('listeFacteursPositifsLatex:' + listeFacteursPositifsLatex)
    // je vais chercher ici l’espression de la fonction privée de tous ses facteurs positifs
    const facteursNonPositifsMaximal = []
    facteursNonPositifsMaximal[0] = []// facteurs du numérateur
    facteursNonPositifsMaximal[1] = []// facteurs du dénominateur
    for (let i = 0; i < nbFacteurs; i++) {
      if (i < stor.objet_derivee.tabFacteurs[0].length) {
        // c’est un facteur du numérateur
        leFacteurLatex = stor.objet_derivee.tabFacteurs[0][i]
        if (listeFacteursPositifsLatex.indexOf(leFacteurLatex) === -1) {
          // ce n’est pas un facteur positif, donc je l’ajoute dans facteursNonPositifsMaximal[0]
          facteursNonPositifsMaximal[0].push(leFacteurLatex)
        }
      } else {
        // c’est un facteur du dénominateur
        leFacteurLatex = stor.objet_derivee.tabFacteurs[1][i - stor.objet_derivee.tabFacteurs[0].length]
        if (listeFacteursPositifsLatex.indexOf(leFacteurLatex) === -1) {
          // ce n’est pas un facteur positif, donc je l’ajoute dans facteursNonPositifsMaximal[1]
          facteursNonPositifsMaximal[1].push(leFacteurLatex)
        }
      }
    }
    const deriveeRestanteMaximale = fctsEtudeExpressionAvecFacteurs(facteursNonPositifsMaximal)
    me.logIfDebug('facteursNonPositifsMaximal:' + facteursNonPositifsMaximal + '      deriveeRestanteMaximale:' + deriveeRestanteMaximale)
    stor.facteursNonPositifsMaximal = facteursNonPositifsMaximal
    stor.deriveeRestanteMaximale = deriveeRestanteMaximale
    stor.divFin = j3pAddElt(stor.conteneur, 'div')// Je créé cette div pour que la palette soit systématiquement placée dessous
    stor.zoneCons4 = j3pAddElt(stor.divFin, 'div')
    stor.laPalette = j3pAddElt(stor.conteneur, 'div')
    const mesZonesSaisie = [stor.zoneInput[0].id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    // Obligatoire
    me.finEnonce()
  }
  function ecoute (zone) {
    if (zone.className.includes('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, zone, {
        liste: ([2, 3, 4, 8].includes(stor.defFonction.modele))
          ? ['racine', 'fraction', 'puissance']
          : ['racine', 'fraction', 'puissance', 'exp', 'ln'],
        style: { paddingTop: '10px' }
      })
    }
  }

  function creationZoneFacteur () {
    let nb = stor.zoneInput[0].value
    let mesZonesSaisie
    j3pDetruit(stor.zoneCons4)
    stor.zoneCons4 = j3pAddElt(stor.divFin, 'div')
    stor.zoneInput = [stor.zoneInput[0]]
    j3pEmpty(stor.laPalette)
    if (nb === '') {
      j3pFocus(stor.zoneInput[0])
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: [stor.zoneInput[0].id],
        validePerso: [stor.zoneInput[0].id]
      })
    } else {
      if (nb === '0') {
        j3pAffiche(stor.zoneCons4, '', textes.phrase4_ter)
        mesZonesSaisie = [stor.zoneInput[0].id]
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: mesZonesSaisie,
          validePerso: mesZonesSaisie
        })
      } else if (nb === '1') {
        const elt = j3pAffiche(stor.zoneCons4, '', textes.phrase4_bis + '\n&1&',
          {
            inputmq1: { texte: '' }
          })
        stor.zoneInput.push(elt.inputmqList[0])
        mesZonesSaisie = [stor.zoneInput[0].id, stor.zoneInput[1].id]
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: mesZonesSaisie,
          validePerso: mesZonesSaisie
        })
      } else if ((nb.length > 1) || (Number(nb) > 4)) {
        nb = '0'
        j3pAffiche(stor.zoneCons4, '', textes.phrase4_4)
        mesZonesSaisie = [stor.zoneInput[0].id]
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: mesZonesSaisie,
          validePerso: mesZonesSaisie
        })
      } else {
        let textInput = '&1&'
        const objAffiche = { n: nb, inputmq1: { texte: '' } }
        for (let i = 2; i <= Number(nb); i++) {
          textInput += '<br>&' + i + '&'
          objAffiche['inputmq' + i] = { texte: '' }
        }
        const elt = j3pAffiche(stor.zoneCons4, '', textes.phrase4 + '\n' + textInput, objAffiche)
        elt.inputmqList.forEach(zone => stor.zoneInput.push(zone))
        j3pFocus(stor.zoneInput[1])
        mesZonesSaisie = stor.zoneInput.map(zone => zone.id)
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: mesZonesSaisie,
          validePerso: mesZonesSaisie
        })
      }
      if (nb !== '0') {
        // position de la palette
        j3pPaletteMathquill(stor.laPalette, stor.zoneInput[1], {
          liste: ['racine', 'fraction', 'puissance', 'exp', 'ln'],
          style: { paddingTop: '10px' }
        })
        for (let i = 1; i <= Number(nb); i++) {
          if (!stor.zoneInput[i].virtualKeyboard) {
            mqRestriction(stor.zoneInput[i], 'x\\d,.+-^()*', {
              commandes: ([2, 3, 4, 8].includes(stor.defFonction.modele))
                ? ['racine', 'fraction', 'puissance']
                : ['racine', 'fraction', 'puissance', 'exp', 'ln']
            })
          }
          $(stor.zoneInput[i]).focusin(function () {
            ecoute(this)
          })
        }
        j3pFocus(stor.zoneInput[1])
      }
    }
  }

  function afficheCorr (bonneReponse) {
    j3pEmpty(stor.laPalette)

    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    // recherche de l’expression dont on doit trouver le signe (sans les facteurs positifs déjà relevés par l’élève)
    const deriveeRestante = fctsEtudeExpressionAvecFacteurs(stor.defFonction.facteursRestantsDerivee)
    // on place dans donneesPersistantes le tableau des facteurs restants pour que cela puisse être repris dans le parcours du graphe
    me.donneesPersistantes.etudeFonctions.facteursRestantsDerivee = stor.defFonction.facteursRestantsDerivee
    let ineq1
    if (stor.nbFacteursPositifsReleves === 0) {
      // on n’a relevé aucun facteur strictement positif
      j3pAffiche(stor.zoneExpli1, '', textes.corr_1_1, { d: deriveeRestante })
    } else {
      if (stor.nbFacteursPositifsReleves === 1) {
        // on a relevé un seul facteur positif (c’est l’élève qui n’en a indiqué qu’un ?)
        ineq1 = stor.tabFacteursPositifsLatexTrouves[0] + '>0'
        // if (stor.tabFacteursPositifsLatexRestants.length>0){
        j3pAffiche(stor.zoneExpli1, '', textes.corr_1_2,
          {
            a: fctsEtudeEcritureDomaine(stor.defFonction.domaineDeriv),
            b: ineq1,
            d: deriveeRestante
          })
      } else {
        // plusieurs facteurs positifs
        // FIXME, stor.nbFacteursPositifsReleves > 1 mais stor.tabFacteursPositifsLatexTrouves peut se retrouver vide
        ineq1 = stor.tabFacteursPositifsLatexTrouves[0] + '>0'
        // si y’en a plus de deux on les ajoute (sauf la dernière) à ineq1
        if (stor.tabFacteursPositifsLatexTrouves.length >= 3) {
          for (let i = 1; i < stor.tabFacteursPositifsLatexTrouves.length - 1; i++) {
            ineq1 += '\\quad ;\\quad ' + stor.tabFacteursPositifsLatexTrouves[i] + '>0'
          }
        }
        // la dernière dans ineq2
        const ineq2 = stor.tabFacteursPositifsLatexTrouves[stor.tabFacteursPositifsLatexTrouves.length - 1] + '>0'
        me.logIfDebug('corr avec', ineq1, ineq2, deriveeRestante, 'avec', stor.tabFacteursPositifsLatexTrouves)
        if (stor.tabFacteursPositifsLatexRestants.length > 0) {
          j3pAffiche(stor.zoneExpli1, '', textes.corr_1_3,
            {
              a: fctsEtudeEcritureDomaine(stor.defFonction.domaineDeriv),
              b: ineq1,
              c: ineq2,
              d: deriveeRestante
            })
        } else {
          j3pAffiche(stor.zoneExpli1, '', textes.corr_1_3_bis,
            {
              a: fctsEtudeEcritureDomaine(stor.defFonction.domaineDeriv),
              b: ineq1,
              c: ineq2,
              d: deriveeRestante
            })
        }
      }
      if (stor.tabFacteursPositifsLatexRestants.length > 0) {
        j3pAffiche(stor.zoneExpli2, '', textes.corr_1_4)
      } else {
        j3pAffiche(stor.zoneExpli2, '', textes.corr_1_4_bis)
      }
    }
    me.donneesPersistantes.etudeFonctions.tabFacteursPositifsTrouves = stor.tabFacteursPositifsLatexTrouves
  } // afficheCorr

  // console.log("debut du code : ",me.etat)

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection(me, textes.titre)

        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              suiteEnonce()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suiteEnonce()
      }
      break // case "enonce":

    case 'correction': {
      const fctsValid = stor.fctsValid
      let reponse = { aRepondu: false, bonneReponse: false }
      reponse = fctsValid.validationGlobale()
      me.logIfDebug('reponse:', reponse)
      let tabFacteursPositifsLatexRestants, tabFacteursPositifsLatexTrouves
      if (reponse.aRepondu === false && !me.isElapsed) {
        me.reponseManquante(stor.zoneCorr)
      } else {
        // Une réponse a été saisie
        stor.defFonction.facteursRestantsDerivee = []
        const repInput1 = fctsValid.zones.reponseSaisie[0]
        const nbFacteursPositifsReleves = Number(repInput1)
        if (!Number.isInteger(nbFacteursPositifsReleves) || nbFacteursPositifsReleves < 0) {
          j3pShowError('Il faut saisir un nombre entier')
          return this.finCorrection() // on reste dans l’état correction (idem réponse manquante)
        }
        stor.tabFacteursPositifsLatexTrouves = []
        if (nbFacteursPositifsReleves > 4) {
          reponse.bonneReponse = false
          stor.defFonction.facteursRestantsDerivee[0] = stor.objet_derivee.tabFacteurs[0]
          stor.defFonction.facteursRestantsDerivee[1] = stor.objet_derivee.tabFacteurs[1]
          fctsValid.zones.bonneReponse[0] = false
          fctsValid.coloreUneZone(stor.zoneInput[0].id)
          stor.nbFacteursPositifsReleves = 0
        } else if (nbFacteursPositifsReleves === 0) {
          // il n’a pas indiqué de facteur strictement positif. Donc pour l’étude de signe, il reprend tous les facteurs
          reponse.bonnne_reponse = true
          stor.defFonction.facteursRestantsDerivee[0] = stor.objet_derivee.tabFacteurs[0]
          stor.defFonction.facteursRestantsDerivee[1] = stor.objet_derivee.tabFacteurs[1]
          fctsValid.zones.bonneReponse[0] = true
          fctsValid.coloreUneZone(stor.zoneInput[0].id)
          stor.nbFacteursPositifsReleves = 0
        } else {
          // réponse entre 1 et 4
          let nbessai = (stor.defFonction.modele === 4) ? 2 : 1
          stor.nbFacteursPositifsReleves = nbFacteursPositifsReleves
          while (nbessai > 0) {
            const tabFacteursPositifsRestants = []
            tabFacteursPositifsLatexRestants = []
            if (nbessai === 2) {
              // C’est pour le modèle 4 où il n’y a qu’un seul facteurs positif de la forme (ax^2+b)^2 (a et b positifs)
              // Sauf qu’on peut accepter 2 fois ax^2+b
              const fact = stor.listeFacteursPositifs[0]
              for (let i = 0; i <= 1; i++) {
                tabFacteursPositifsRestants.push(fact.substring(0, fact.length - 2))
              }
              for (let i = 0; i < stor.listeFacteursPositifs.length; i++) {
                tabFacteursPositifsLatexRestants.push(stor.listeFacteursPositifsLatex[i])
              }
              // @todo réécrire les affectations ci-dessus pour garantir que tabFacteursPositifsRestants et tabFacteursPositifsLatexRestants aient la même taille
            } else {
              // on clone pour pouvoir faire évoluer tabFacteursPositifsRestants au cours de la validation
              for (let i = 0; i < stor.listeFacteursPositifs.length; i++) {
                tabFacteursPositifsRestants.push(stor.listeFacteursPositifs[i])
                tabFacteursPositifsLatexRestants.push(stor.listeFacteursPositifsLatex[i])
              }
            }
            tabFacteursPositifsLatexTrouves = []
            // tabFacteursPositifsRestants contient au début tous les facteurs positifs et à la fin, seulement ceux qui n’ont pas été cités

            for (let i = 0; i < nbFacteursPositifsReleves; i++) {
              let estNul = true
              fctsValid.zones.bonneReponse[i + 1] = false
              const facteurRep = stor.liste.traiteSignesMult(fctsEtudeEcriturePourCalcul(j3pMathquillXcas($(stor.zoneInput[i + 1]).mathquill('latex'))))
              if (tabFacteursPositifsRestants.length === 0) {
                // il n’y a plus de facteurs positifs à trouver donc le facteur proposé est nécessairement faux
                estNul = false
              } else {
                for (let j = 0; j < tabFacteursPositifsRestants.length; j++) {
                  estNul = true
                  // je transforme chaque facteur pour qu’il soit compris dans mtg32
                  const facteurSol = stor.liste.traiteSignesMult(fctsEtudeEcriturePourCalcul(j3pMathquillXcas(tabFacteursPositifsRestants[j])))
                  stor.liste.giveFormula2('f', facteurSol)
                  stor.liste.giveFormula2('g', facteurRep)
                  stor.liste.calculateNG() // les fonctions f et g du fichier mtg32 prennent comme expressions le facteur attendu et le facteur saisi
                  me.logIfDebug('facteurRep de l’élève :' + facteurRep + '   facteurSol:' + facteurSol)

                  for (let k = 0; k < stor.tab_nombres.length; k++) {
                    stor.liste.giveFormula2('a', String(stor.tab_nombres[k]))
                    stor.liste.calculateNG()// dans le fichier mtg32, on donne à a les différentes valeurs de tab_nombres
                    if (Number(stor.liste.valueOf('OK')) === 0) { // ce "OK" est pour vérifier si l’objet g(a) existe bien dans mtg32
                      console.debug('je ne comprends pas la réponse saisie')// Ici il n’existe pas, c’est que la fonction saisie n’est pas comprise'
                      estNul = false
                    } else {
                      if (Number(stor.liste.valueOf('egalite')) === 0) { // c’est-à-dire que f(a)!=g(a)
                        estNul = false // donc on considère que les fonctions ne sont pas égales
                      }
                    }
                  }
                  if (estNul) {
                    if (stor.defFonction.modele === 4 && nbessai === 2) {
                      if (tabFacteursPositifsLatexTrouves.length === 0) tabFacteursPositifsLatexTrouves.push(tabFacteursPositifsLatexRestants[j])
                    } else {
                      tabFacteursPositifsLatexTrouves.push(tabFacteursPositifsLatexRestants[j])
                    }
                    tabFacteursPositifsRestants.splice(j, 1)
                    tabFacteursPositifsLatexRestants.splice(j, 1)
                    j = tabFacteursPositifsRestants.length + 1
                    fctsValid.zones.bonneReponse[i + 1] = true
                  }
                }
              }

              me.logIfDebug('estNul:' + estNul)

              if (!estNul) {
                // Test ci-dessous rajouté par Yves mais à contrôler par Rémi ...
                if (stor.nbFacteursPositifsReleves > 0) stor.nbFacteursPositifsReleves--
              }
            }
            if (nbessai === 2 && stor.nbFacteursPositifsReleves >= 1) {
              nbessai = 1
              stor.nbFacteursPositifsReleves = 1
            }
            nbessai--
          }
          stor.defFonction.facteursRestantsDerivee[0] = []
          stor.defFonction.facteursRestantsDerivee[1] = []
          let estNul2
          for (let j = 0; j < 2; j++) {
            for (let i = 0; i < stor.objet_derivee.tabFacteurs[j].length; i++) {
              for (let i2 = 0; i2 < tabFacteursPositifsLatexTrouves.length; i2++) {
                const difference2 = fctsEtudeEcriturePourCalcul(j3pMathquillXcas(stor.objet_derivee.tabFacteurs[j][i])) + '-(' + fctsEtudeEcriturePourCalcul(tabFacteursPositifsLatexTrouves[i2]) + ')'
                const arbreDif2 = new Tarbre(difference2, ['x'])
                estNul2 = true
                for (let k = 0; k < stor.tab_nombres.length; k++) {
                  if (Math.abs(arbreDif2.evalue([stor.tab_nombres[k]])) > epsilon) {
                    estNul2 = false // c’est donc que tab_fact_positif[j] n’est pas le facteur saisi
                  }
                }
                if (estNul2) {
                  // on a trouvé l’un des facteurs restants
                  i2 = tabFacteursPositifsLatexTrouves.length + 1
                }
              }
              if (!estNul2) {
                // c’est qu’on n’a pas trouvé le facteur
                stor.defFonction.facteursRestantsDerivee[j].push(stor.objet_derivee.tabFacteurs[j][i])
              }
            }
          }
          me.logIfDebug('facteurs trouvés tabFacteursPositifs:' + tabFacteursPositifsLatexTrouves + '  facteurs restants tabFacteursPositifsRestants :' + tabFacteursPositifsLatexRestants)
          me.logIfDebug('facteurs restants du numérateur:' + stor.defFonction.facteursRestantsDerivee[0] + '  facteurs restants du dénominateur :' + stor.defFonction.facteursRestantsDerivee[1])
          // on clone les elts de tabFacteursPositifsLatexTrouves dans stor.tabFacteursPositifsLatexTrouves (pourquoi pas une affectation simple ?)
          stor.tabFacteursPositifsLatexTrouves = [...tabFacteursPositifsLatexTrouves]
          // idem pour tabFacteursPositifsLatexRestants
          stor.tabFacteursPositifsLatexRestants = [...tabFacteursPositifsLatexRestants]
          reponse.bonneReponse = true
          for (let i = 0; i < nbFacteursPositifsReleves; i++) {
            reponse.bonneReponse = (reponse.bonneReponse && fctsValid.zones.bonneReponse[i + 1])
          }
          fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
          fctsValid.coloreUneZone(stor.zoneInput[0].id)
          for (let i = 0; i < nbFacteursPositifsReleves; i++) {
            fctsValid.coloreUneZone(stor.zoneInput[i + 1].id)
          }
        } // fin du else réponse entre 1 et 4

        if (reponse.bonneReponse && !me.isElapsed) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          if (nbFacteursPositifsReleves < stor.listeFacteursPositifs.length) {
            stor.zoneCorr.innerHTML = textes.comment1
          } else {
            stor.zoneCorr.innerHTML = cBien
          }
          afficheCorr(true)
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            afficheCorr(false)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            if (nbFacteursPositifsReleves > 4) {
              stor.zoneCorr.innerHTML += '<br/>' + textes.comment2
            }
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
            } else {
              // Erreur au dernier essai
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      me.finNavigation()
      break
  }
}
