import { j3pAddElt, j3pEmpty, j3pFocus, j3pPaletteMathquill, j3pPolynome, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { fctsEtudeCalculValeur, fctsEtudeDetermineLimiteSurcharge, fctsEtudeDetermineTabLimites, fctsEtudeDivisionNbs, fctsEtudeEcrireMonome, fctsEtudeEcritLimite, fctsEtudeEcritureDomaine, fctsEtudeEcritureLatexDerivee, fctsEtudeEcritureLatexFonction, fctsEtudeProduitNbs, fctsEtudeSigne, fctsEtudeSigneFois, fctsEtudeSommeNbs } from 'src/legacy/outils/fonctions/etude'
import textesGeneriques from 'src/lib/core/textes'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'

import { aideModeles, initModele, initSection } from './common'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Alexis Lecomte (et Rémi Deniaud)
    Mars 2015
    Section intégrable dans un graphe avec données du graphe (on utilise la variable imposer_fct pour donner une fonction particulière suivant un modèle) ou donneesPrecedentes
    On étudie les limites d’une fonction aux bornes de son ensemble de déf suivant un modèle (ou une fonction imposée)
    En cas d’ajout de modèles, il faudra juste modifier les fonctions fctsEtudeGenereAleatoire et genereTextesCorrection puis ajouter les textes de correction qui vont bien
    Non implémenté pour l’instant : le retour des réponses de l’élève pour la section bilan (qui n’a jamais été finalisée)
    C’est en case correction qu’il faudra implémenter me.DonneesPersistantes.etudeFonctions.repEleve
*/
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['imposer_fct', '', 'string', 'On peut renseigner une fonction correspondant au modèle ou laisser vide pour générer aléatoirement une fonction suivant un modèle'],
    ['modele', [1], 'array', aideModeles],
    ['imposer_domaine', [], 'array', 'On donne le domaine de définition sous la forme d’un tableau où chaque intervalle est représenté par 4 éléments : borne inf, borne sup et les deux crochets. Par exemple ["-infini","0","]","]"] pour l’intervalle ]-infini;0].'],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la suite étudiée est celle du nœud précédent.']
  ],
  donnees_parcours_description: 'Cette section peut réutiliser les paramètres modele, imposer_fct et imposer_domaine d’une autre section aux même paramètres.',
  donnees_parcours_nom_variable: 'imposer_fct',

  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'facteurs_non_strictement_positifs_dans_la_derivee' },
    { pe_2: 'derivee_aux_facteurs_strictement_positifs' }
  ]

}
const textes = {
  titre: 'Limites d’une fonction aux bornes de son ensemble de définition',
  phrase1: 'On considère la fonction définie sur $£a$ par : $f(x)=£b$',
  phrase2: 'Détermine la limite suivante&nbsp;: (attention tu n’as le droit qu’à un seul essai)<br/>$\\limite{x}{£a}{f(x)}=$&1&',
  phrase2_bis: 'Détermine la limite suivante&nbsp;:<br/>$\\limite{x}{£a}{f(x)}=$&1&',
  corr_1_1: 'Comme $\\limite{x}{-\\infty}{£a}=£b$ et $\\limite{x}{-\\infty}{\\mathrm{e}^x}=0$, on est face à une forme indéterminée du type "$0\\times\\infty$".<br>', // correction du modèle 1
  corr_1_2: 'Mais en développant $f(x)$, on trouve $f(x)=£a$ et comme $\\limite{x}{-\\infty}{x\\mathrm{e}^x}=0$ et $\\limite{x}{-\\infty}{\\mathrm{e}^x}=0$ (limites du cours), on a $\\limite{x}{-\\infty}{f(x)}=0$.',
  corr_1_3: 'Comme $\\limite{x}{+\\infty}{£a}=£b$ et $\\limite{x}{+\\infty}{\\mathrm{e}^x}=+\\infty$, on a $\\limite{x}{+\\infty}{f(x)}=£c$.',
  corr_2_1: 'En $\\infty$, la limite d’un polynôme est égale à la limite de son terme de plus haut degré. En effet, en factorisant par $£a$, on obtient $f(x)=£a£b$, la limite de l’expression entre parenthèses valant 1, la limite de $f(x)$ est la même que celle de $£a$<br>', // correction du modèle2
  corr_2_2: 'On a donc $\\limite{x}{-\\infty}{f(x)}=\\limite{x}{-\\infty}{£a}=£b$.',
  corr_2_3: 'On a donc $\\limite{x}{+\\infty}{f(x)}=\\limite{x}{+\\infty}{£a}=£b$.',
  corr_3_1_a: 'On a ici une forme indéterminée du type « $\\frac{\\infty}{\\infty}$ », il suffit de factoriser le numérateur et le dénominateur par $x$ puis de simplifier, on obtient $f(x)=£a$ dont la limite est $£b$.',
  corr_3_1_b: 'Le dénominateur tend vers $£a$ donc son inverse tend vers 0, en multipliant par une constante, la limite obtenue est 0.',
  corr_3_2: 'Le numérateur a pour limite $£a$  et le dénominateur 0, le quotient aura donc pour limite $+\\infty$ ou $-\\infty$. Pour le savoir il faut déterminer le signe du dénominateur.',
  corr_3_3: ' Or, comme $x$ tend vers $£a$, on a $x<£b$, la fonction $x\\mapsto £c$ étant £d sur $\\R$, on a $£c£e0$ lorsque $x<£b$, ainsi le dénominateur tend vers $£f$.<br>',
  corr_3_4: ' Or, comme $x$ tend vers $£a$, on a $x>£b$, la fonction $x\\mapsto £c$ étant £d sur $\\R$, on a $£c£e0$ lorsque $x>£b$, ainsi le dénominateur tend vers $£f$.<br>',
  corr_3_5: 'On en conclut en appliquant la règle des signes du quotient que $\\limite{x}{£a}{f(x)}=£b$',
  corr_4_1: 'Le numérateur et le dénominateur ayant une limite infinie, le quotient est une forme indéterminée. En factorisant chacun des deux par son terme prépondérant et en simplifiant par $x$ on obtient :<br>',
  corr_4_2: '$f(x)=\\frac{£a}{£b}=\\frac{£c£d}{£e£f}=\\frac{£g£d}{£h£f}$<br>',
  corr_4_3: 'Le numérateur a une limite finie : £a, le dénominateur a une limite infinie le quotient tend donc vers 0 : $\\limite{x}{£b}{f(x)}=0$',
  corr_5_1: 'Comme $\\limite{x}{£c}{£a}=£b$ et $\\limite{x}{£c}{\\mathrm{e}^{£d}}=£e$ par composition des limites (puisque $\\limite{x}{£c}{£d}=£g$ et $\\limite{X}{£g}{\\mathrm{e}^X}=£e$), par produit on a $\\limite{x}{£c}{f(x)}=£f$',
  corr_Bis5_1: '$\\limite{x}{£c}{\\mathrm{e}^{£d}}=£e$ par composition des limites (puisque $\\limite{x}{£c}{£d}=£g$ et $\\limite{X}{£g}{\\mathrm{e}^X}=£e$), on a $\\limite{x}{£c}{f(x)}=£f$',
  corr_Ter5_1: '$\\limite{x}{£c}{\\mathrm{e}^{£d}}=£e$ par composition des limites (puisque $\\limite{x}{£c}{£d}=£g$ et $\\limite{X}{£g}{\\mathrm{e}^X}=£e$), ainsi par produit on a $\\limite{x}{£c}{f(x)}=£f$',
  corr_5_2: 'Comme $\\limite{x}{£c}{£a}=£b$ et $\\limite{x}{£c}{\\mathrm{e}^{£d}}=£e$ par composition des limites (puisque $\\limite{x}{£c}{£d}=-\\infty$ et $\\limite{X}{-\\infty}{\\mathrm{e}^X}=0$) on est face à une forme indéterminée du type "$0\\times\\infty$".',
  corr_5_3: ' Pour lever l’indétermination il faut pouvoir utiliser <span style="color:red">$\\limite{X}{-\\infty}{X\\mathrm{e}^X}=0$</span> avec $X=£a$, pour cela on transforme ainsi $f(x)$ (pour tout $x$ non nul) : $f(x)=\\frac{£b}{£a}\\times£g\\times \\mathrm{e}^{£a}\\times \\mathrm{e}^{£c}$. On a ainsi $\\limite{x}{£d}{£a\\mathrm{e}^{£a}}=0$, et comme $\\limite{x}{£d}{\\frac{£b}{£a}}=£e$ on obtient $\\limite{x}{£d}{f(x)}=£f$',
  corr_5_4: ' Pour lever l’indétermination il faut pouvoir utiliser <span style="color:red">$\\limite{x}{-\\infty}{x\\mathrm{e}^x}=0$</span>, pour cela on transforme ainsi $f(x)$ (pour tout $x$ non nul) : $f(x)=\\frac{£a}{x}\\times x\\times \\mathrm{e}^{x}\\times \\mathrm{e}^{£b}$. Comme $\\limite{x}{£c}{\\frac{£a}{x}}=£d$ on obtient $\\limite{x}{£c}{f(x)}=£e$',
  corr_5_5: ' Pour lever l’indétermination il faut pouvoir utiliser <span style="color:red">$\\limite{x}{-\\infty}{x\\mathrm{e}^x}=0$</span>, pour cela on transforme ainsi $f(x)$ (pour tout $x$ réel) : $f(x)=£a\\mathrm{e}^{x}\\times \\mathrm{e}^{£b}$. Comme $\\limite{x}{£c}{x\\mathrm{e}^{x}}=0$, on obtient $\\limite{x}{£c}{f(x)}=£d$',
  corr_6_1: 'On peut écrire $f(x)$ ainsi pour tout $x$ strictement positif : $f(x)=(£a)\\times £b$. Or comme $x>0$, $\\limite{x}{0}{£b}=+\\infty$, et comme $\\limite{x}{0}\\ln(x)=-\\infty$, ainsi $\\limite{x}{0}{£c}=£d$ et donc par produit $\\limite{x}{0}{(£a)\\times £b}=£d$',
  corr_6_2: 'Comme d’après le cours $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$, on a directement $\\limite{x}{+\\infty}{f(x)}=0$',
  corr_6_3: 'Pour tout $x$ strictement positif, on peut écrire $f(x)=£a$, et comme $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$ et $\\limite{x}{+\\infty}{£b}=0$ on a $\\limite{x}{+\\infty}{f(x)}=0$',
  corr_6_4: 'On peut écrire pour tout $x$ strictement positif : $f(x)=£a$, comme d’après le cours $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$ et que $\\limite{x}{+\\infty}{£b}=0$, on a directement $\\limite{x}{+\\infty}{f(x)}=0$',
  corr_6_5: 'Pour tout $x$ stricteme. nt positif, on peut écrire $f(x)=£a$, ou encore $f(x)=£b$ et comme $\\limite{x}{+\\infty}{£d}=0$, $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$ et $\\limite{x}{+\\infty}{£c}=0$ on a $\\limite{x}{+\\infty}{f(x)}=0$',
  corr_7_1: '$\\limite{x}{£a}{£b}=0$. Or sur $£c$, $£b>0$ donc $\\limite{x}{£a}{£b}=0^+$, ainsi, comme $\\limite{X}{0^+}{\\ln X}=-\\infty$, on a $\\limite{x}{£a}{\\ln(£b)}=-\\infty$ et donc on a $\\limite{x}{£a}{£d\\ln(£b)}=£e$.<br>De plus, $\\limite{x}{£a}{£f}=£g$, donc en sommant on a $\\limite{x}{£a}{f(x)}=£e$',
  corr_7_1_bis: '$\\limite{x}{£a}{£b}=0$. Or sur $£c$, $£b>0$ donc $\\limite{x}{£a}{£b}=0^+$, ainsi, comme $\\limite{X}{0^+}{\\ln X}=-\\infty$, on a $\\limite{x}{£a}{\\ln(£b)}=-\\infty$.<br>De plus, $\\limite{x}{£a}{£f}=£g$, donc en sommant on a $\\limite{x}{£a}{f(x)}=£e$',
  corr_7_1_ter: '$\\limite{x}{£a}{\\ln(£b)}=-\\infty$ et donc on a $\\limite{x}{£a}{£d\\ln(£b)}=£e$.<br>De plus, $\\limite{x}{£a}{£f}=£g$, donc en sommant on a $\\limite{x}{£a}{f(x)}=£e$',
  corr_7_1_qua: '$\\limite{x}{£a}{\\ln(£b)}=-\\infty$.<br>De plus, $\\limite{x}{£a}{£f}=£g$, donc en sommant on a $\\limite{x}{£a}{f(x)}=£e$',
  corr_7_2: '$\\limite{x}{£a}{£b}=+\\infty$ donc $\\limite{x}{£a}{\\ln(£b)}=+\\infty$.<br>',
  corr_7_2_bis: '$\\limite{x}{£a}{\\ln(£b)}=+\\infty$.<br>',
  corr_7_3: 'On a donc $\\limite{x}{£a}{£c\\ln(£e)}=£d$. De plus, $\\limite{x}{£a}{£b}=£d$ donc en sommant on a $\\limite{x}{£a}{f(x)}=£d$',
  corr_7_3_bis: 'De plus, $\\limite{x}{£a}{£b}=£d$ donc en sommant on a $\\limite{x}{£a}{f(x)}=£d$',
  corr_7_4: 'Comme $\\limite{x}{£a}{£b}=£c$ et $\\limite{x}{£a}{£d\\ln(£e)}=£f$ on est face à une forme indéterminée du type "$\\infty-\\infty$", on va donc factoriser :<br>',
  corr_7_5: '$\\forall x \\in £a$ non nul, $f(x)=£b$ qui a pour limite $£c$ quand $x$ tend vers $£d$.<br>',
  corr_7_6: 'En effet, $\\frac{\\ln(£a)}{£b}=\\frac{£a}{£b}\\times \\frac{\\ln (£a)}{£a}$. Or $\\limite{x}{£c}{\\frac{£a}{£b}}=£d$ et comme $\\limite{X}{+\\infty}{\\frac{\\ln X}{X}}=0$, on a $\\limite{x}{£c}{\\frac{\\ln(£a)}{£a}}=0$.<br>Finalement $\\limite{x}{£c}{£e}=1$ donc $\\limite{x}{£c}{£f}=£g$. Conclusion : $\\limite{x}{£c}{f(x)}=£g$',
  corr_7_6_bis: 'En effet, $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$.<br>Finalement $\\limite{x}{£c}{£e}=£a$ donc $\\limite{x}{£c}{£f}=£g$.<br/>Conclusion : $\\limite{x}{£c}{f(x)}=£g$',
  croissante: 'croissante',
  decroissante: 'décroissante'
}

/**
 * section EtudeFonction_limite
 * @this {Parcours}
 */
export default function main () {
  // fonction fctsEtudeDetermineTabLimites externalisée dans fcts_communes car utilisée dans les variations
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  // pour le modele 3, de forme ()/() me redonne le den
  function extraitDen (fct) {
    const index = fct.indexOf('/')
    const den = fct.substring(index + 1)
    me.logIfDebug('den=', den, ' et den.substring(1,den.length-1)=', den.substring(1, den.length - 1))
    return den.substring(1, den.length - 1)
  }

  function equivalent (i) {
    const valeur = stor.tab_limites[i]
    for (let j = 0; j < stor.defFonction.limites.length; j++) {
      if (valeur === fctsEtudeEcritLimite(stor.defFonction.limites[j][0])) {
        return j
      }
    }
  }

  function init () {
    me.surcharge({ nbetapes: 3 })
    me.logIfDebug('ds.imposer_fct : ', ds.imposer_fct)
    // Construction de la page
    me.construitStructurePage('presentation1')
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    initSection(me, textes.titre)
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '15px' }) })
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    if (ds.nbetapes === 1 || (me.questionCourante % ds.nbetapes) === 1) {
      initModele(me)
    }
    const domaineImpose = fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)
    me.logIfDebug(stor.defFonction.fonction, stor.defFonction.modele, stor.defFonction.domaineDeriv)
    const laFonction = fctsEtudeEcritureLatexFonction(stor.defFonction.fonction, stor.defFonction.modele)
    // necessaire pour la presence des facteurs negatifs de la derivee
    stor.objet_derivee = fctsEtudeEcritureLatexDerivee(laFonction, stor.defFonction.modele, stor.defFonction.domaineDeriv, ds.debug)
    me.logIfDebug('stor.objet_derivee=', stor.objet_derivee)

    // détermination de la présence de facteurs négatifs dans la derivee pour déterminer la PE (permettra de squizzer une section dans un parcours)
    switch (stor.defFonction.modele) {
      case 6:// (ax+bln(x))/x^c
        stor.presence_facteurs_negatifs = true
        break

      case 5:// (ax+b)exp(cx+d)
        stor.presence_facteurs_negatifs = (stor.objet_derivee.signes_facteurs[0] !== '+')
        break

      case 4:
        // (ax+b)/(cx^2+d) avec c>0 et d>0 de dérivée (-acx^2-2bcx+ad)/(cx^2+d)^2 mais le trinome a deux racines
        stor.presence_facteurs_negatifs = true
        break

      case 3:
        stor.presence_facteurs_negatifs = (stor.objet_derivee.signes_facteurs[0] !== '+')
        break

      case 2:
        stor.presence_facteurs_negatifs = (stor.objet_derivee.signes_facteurs.length !== 1 || stor.objet_derivee.signes_facteurs[0] !== '+') // si delta négatif et a positif
        break

      default:// premier modèle j’ai forcément un facteur négatif...
        stor.presence_facteurs_negatifs = true
    }
    me.parcours.pe = (stor.presence_facteurs_negatifs) ? ds.pe_1 : ds.pe_2
    // console.log('questionCourante et nbetapes:', me.questionCourante, ds.nbetapes)
    if ((me.questionCourante % ds.nbetapes) === 1) {
      // on récupère le nb de limites pour déterminer le nbetapes, mais on le fait que en étape 1 de chaque éventuelle répétition...
      stor.tab_limites = fctsEtudeDetermineTabLimites(stor.defFonction, ds.debug)
      ds.nbetapes = stor.tab_limites.length
      ds.nbitems = ds.nbetapes * ds.nbrepetitions// on recalcule le nbitems
    }
    // console.log('après, questionCourante et nbetapes:', me.questionCourante, ds.nbetapes)
    // pour avoir la bonne limite en consigne :
    let index
    switch (me.questionCourante % ds.nbetapes) {
      case 0:// dernière étape
        index = stor.tab_limites.length - 1
        break

      default :
        index = me.questionCourante % ds.nbetapes - 1
    }
    j3pAffiche(stor.zoneCons1, '', textes.phrase1,
      {
        a: domaineImpose,
        b: laFonction
      })
    const enonce = (ds.nbchances === 1) ? textes.phrase2 : textes.phrase2_bis
    const elt = j3pAffiche(stor.zoneCons2, '', enonce,
      {
        a: stor.tab_limites[index],
        inputmq1: { texte: '' }
      })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.+-/', {
      commandes: ['racine', 'fraction', 'puissance', 'inf']
    })

    // détermination de la bonne réponse :
    me.logIfDebug('stor.defFonction.limites=', stor.defFonction.limites, ' et index=', index, ' et stor.tab_limites[index]=', stor.tab_limites[index])
    try {
      stor.bonneReponse = fctsEtudeDetermineLimiteSurcharge(stor.tab_limites[index], stor.defFonction.limites)
    } catch (error) {
      j3pShowError(error)
      // faut pas laisser la section continuer avec n’importe quoi, on arrête là
      return
    }
    // Pour ValidationZones
    if (stor.bonneReponse.includes('infty')) {
      stor.zoneInput.typeReponse = ['texte']
      stor.zoneInput.reponse = [stor.bonneReponse]
    } else { // on peut estimer que c’est une valeur exacte qu’on attend
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      stor.zoneInput.reponse = [fctsEtudeCalculValeur(stor.bonneReponse)]
    }
    me.logIfDebug('stor.bonneReponse=', stor.bonneReponse)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })

    j3pStyle(stor.zoneCons3, { padding: '15px' })
    stor.laPalette = j3pAddElt(stor.zoneCons3, 'div')
    // palette MQ :
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, {
      liste: ['racine', 'fraction', 'puissance', 'inf']
    })

    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.toutpetit.explications })
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    genereTextesCorrection()// se base sur le modèle pour que ce soit simple d’en rajouter
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '15px' }) })

    // Obligatoire
    me.finEnonce()
  }

  function genereTextesCorrection () {
    stor.tab_textes_cor = []
    stor.variables_cor = []
    let a, b, c, d, e, i, tab
    switch (stor.defFonction.modele) {
      case 7:
        a = Number(stor.objet_derivee.variables[0])
        b = Number(stor.objet_derivee.variables[1])
        c = Number(stor.objet_derivee.variables[2])
        d = Number(stor.objet_derivee.variables[3])
        e = Number(stor.objet_derivee.variables[4])
        {
          const dxpluse = fctsEtudeEcrireMonome(1, 1, d) + fctsEtudeEcrireMonome(2, 0, e)
          const axplusb = fctsEtudeEcrireMonome(1, 1, a) + fctsEtudeEcrireMonome(2, 0, b)
          const ax = fctsEtudeEcrireMonome(1, 1, a)
          const dsura = fctsEtudeDivisionNbs(d, a)
          const domaineImpose = fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)
          let cFois = c
          if (c === -1) cFois = '-'
          if (c === 1) cFois = ''
          for (i = 0; i < stor.tab_limites.length; i++) {
            const laBorne = stor.tab_limites[i]
            const ligne1 = (d === 1 && e === 0) ? textes.corr_7_2_bis : textes.corr_7_2
            if (laBorne === '+\\infty' || laBorne === '-\\infty') { // x tend vers +-inf
              if ((c > 0 && ((laBorne === '-\\infty' && a > 0) || (laBorne === '+\\infty' && a < 0))) || (c < 0 && ((laBorne === '+\\infty' && a > 0) || (laBorne === '-\\infty' && a < 0)))) { // FI inf-inf
                let limDuLn = '-\\infty'
                let limDeAxplusb = '+\\infty'
                if (c > 0) {
                  limDuLn = '+\\infty'
                  limDeAxplusb = '-\\infty'
                }
                let laPar, leProduit
                if (d === 1 && e === 0) {
                  // on ne factorise que par x
                  laPar = '\\left(' + a
                  if (b !== 0) laPar += (b > 0) ? '+\\frac{' + b + '}{x}' : '-\\frac{' + String(Math.abs(b)) + '}{x}'
                  if (c > 0) {
                    laPar += (c === 1) ? '+' : '+' + c
                  } else {
                    laPar += c
                  }
                  laPar += '\\frac{\\ln(' + dxpluse + ')}{x}\\right)'
                  leProduit = 'x' + laPar
                } else {
                  laPar = '\\left(1'
                  if (b !== 0) {
                    laPar += (b > 0) ? '+\\frac{' + b + '}{' + ax + '}' : '-\\frac{' + String(Math.abs(b)) + '}{' + ax + '}'
                  }
                  if (c > 0) {
                    laPar += (c === 1) ? '+' : '+' + c
                  } else {
                    laPar += c
                  }
                  laPar += '\\frac{\\ln(' + dxpluse + ')}{' + ax + '}'
                  laPar += '\\right)'
                  leProduit = ax + laPar
                }
                stor.tab_textes_cor.push([ligne1, textes.corr_7_4, textes.corr_7_5, (d === 1 && e === 0) ? textes.corr_7_6_bis : textes.corr_7_6])
                stor.variables_cor.push([
                  [laBorne, dxpluse],
                  [laBorne, axplusb, limDeAxplusb, cFois, dxpluse, limDuLn],
                  [domaineImpose, leProduit, stor.bonneReponse, laBorne],
                  (d === 1 && e === 0)
                    ? [a, ax, laBorne, dsura, laPar, leProduit, stor.bonneReponse]
                    : [dxpluse, ax, laBorne, dsura, laPar, leProduit, stor.bonneReponse]
                ])
              } else { // les deux termes auront pour limite +inf ou -inf
                stor.tab_textes_cor.push([ligne1, (c === 1) ? textes.corr_7_3 : textes.corr_7_3])
                stor.variables_cor.push([[laBorne, dxpluse], [laBorne, axplusb, cFois, stor.bonneReponse, dxpluse]])
              }
            } else {
              const image = fctsEtudeSommeNbs(fctsEtudeProduitNbs(a, laBorne.substring(0, laBorne.length - 2)), b)
              if (c === 1) {
                if (d === 1 && e === 0) stor.tab_textes_cor.push([textes.corr_7_1_qua])
                else stor.tab_textes_cor.push([textes.corr_7_1_bis])
                stor.variables_cor.push([[laBorne, dxpluse, domaineImpose, c, stor.bonneReponse, axplusb, image]])
              } else {
                if (d === 1 && e === 0) stor.tab_textes_cor.push([textes.corr_7_1_ter])
                else stor.tab_textes_cor.push([textes.corr_7_1])
                stor.variables_cor.push([[laBorne, dxpluse, domaineImpose, c, stor.bonneReponse, axplusb, image]])
              }
            }
          }
        }
        break

      case 6:
        a = Number(stor.objet_derivee.variables[0])
        b = Number(stor.objet_derivee.variables[1])
        c = Number(stor.objet_derivee.variables[2])
        {
          let leNum = fctsEtudeEcrireMonome(1, 0, a) + fctsEtudeEcrireMonome(2, 1, b, 'ln x')
          if (Math.abs(a) < Math.pow(10, -12)) {
            leNum = fctsEtudeEcrireMonome(1, 1, b, 'ln x')
          }
          leNum = leNum.replace('ln x', '\\ln x')
          let laFrac = '\\frac{1}{x^' + c + '}'
          if (c === 1) {
            laFrac = '\\frac{1}{x}'
          }
          let leLn = fctsEtudeEcrireMonome(1, 1, b, 'ln x')
          leLn = leLn.replace('ln x', '\\ln x')

          for (i = 0; i < stor.tab_limites.length; i++) {
            if (stor.tab_limites[i] === '0^+') {
              me.logIfDebug('LIMITE EN 0')
              stor.tab_textes_cor.push([textes.corr_6_1])
              stor.variables_cor.push([[leNum, laFrac, leLn, stor.bonneReponse]])
            }
            if (stor.tab_limites[i] === '+\\infty') {
              me.logIfDebug('LIMITE EN +inf')
              if (c === 1) {
                if (a === 0) {
                  stor.tab_textes_cor.push([textes.corr_6_2])
                  stor.variables_cor.push([[]])
                } else {
                  stor.tab_textes_cor.push([textes.corr_6_3])
                  stor.variables_cor.push([['\\frac{' + a + '}{x}' + fctsEtudeSigneFois(b) + '\\frac{\\ln x}{x}', '\\frac{' + a + '}{x}']])
                }
              } else {
                if (a === 0) {
                  stor.tab_textes_cor.push([textes.corr_6_4])
                  stor.variables_cor.push([[]])
                } else {
                  let laFrac2 = '\\frac{1}{x^' + (c - 1) + '}'
                  if (c === 2) {
                    laFrac2 = '\\frac{1}{x}'
                  }
                  stor.tab_textes_cor.push([textes.corr_6_5])
                  stor.variables_cor.push([['\\frac{' + a + fctsEtudeSigneFois(b) + '\\ln x}{x}\\times' + laFrac2 + '}', '\\left(\\frac{' + a + '}{x}' + fctsEtudeSigneFois(b) + '\\frac{\\ln x}{x}\\right)\\times' + laFrac2, '\\frac{' + a + '}{x}', laFrac2]])
                }
              }
            // stor.tab_textes_cor.push([textes.corr_6_2]);
            // stor.variables_cor.push([[leNum,laFrac,leLn,stor.bonneReponse]]);
            }
          }
        }
        break

      case 5:// reprise du case 1, mais ça aurait été plus lisible de repartir de objet_derivee.variables
        {
          let affine1 = stor.defFonction.fonction.split('exp')[0] // heureusement qu’on connait le modèle...
          if (affine1.includes('(')) affine1 = affine1.substring(1, affine1.length - 1)
          let constante1, coefdir1, lineaire1
          if (!affine1.includes('x')) {
            lineaire1 = '0'
            constante1 = affine1
            coefdir1 = 0
          } else {
            tab = affine1.split('x')
            lineaire1 = tab[0] + 'x'
            constante1 = tab[1]
            coefdir1 = tab[0]
          }
          let affine2 = stor.defFonction.fonction.split('exp')[1]
          affine2 = affine2.substring(1, affine2.lastIndexOf(')'))
          tab = affine2.split('x')
          const lineaire2 = tab[0] + 'x'
          let constante2 = (tab[1])
          let coefdir2 = tab[0]
          if (coefdir2 === '-') coefdir2 = '-1'
          if (coefdir2 === '') coefdir2 = '1'
          if (coefdir1 === '-') coefdir1 = '-1'
          if (coefdir1 === '') coefdir1 = '1'
          if (constante1 === '') constante1 = '0'
          if (constante2 === '') constante2 = '0'
          if (stor.objet_derivee.variables[0] === 0) {
            lineaire1 = '0'
            constante1 = stor.objet_derivee.variables[1]
            if (stor.objet_derivee.variables[1] > 0) {
              constante1 = '+' + constante1
            }
            affine1 = stor.objet_derivee.variables[1]
            coefdir1 = '0'
          }
          me.logIfDebug('lineaire1=', lineaire1, ' affine1=', affine1, ' constante1=', constante1, 'coefdir1=', coefdir1)
          me.logIfDebug('lineaire2=', lineaire2, ' affine2=', affine2, ' constante2=', constante2, 'coefdir2=', coefdir2)
          let lineaire2Par = lineaire2
          if (lineaire2.charAt(0) === '-') {
            lineaire2Par = '(' + lineaire2 + ')'
          }
          // obligé de feinter, ça dépend si le domaine de definition est surchargé, et donc ça dépend du nb de limites
          let lim1
          for (i = 0; i < stor.tab_limites.length; i++) {
            if (stor.tab_limites[i] === '-\\infty') {
              if (Number(coefdir1) === 0) {
                lim1 = stor.objet_derivee.variables[1]
              } else if (Number(coefdir1) < 0) {
                lim1 = '+\\infty'
              } else {
                lim1 = '-\\infty'
              }
              if (String(coefdir1) === '0') {
                if (Number(coefdir2) < 0) { // l’expo aura pour limite +inf
                  stor.variables_cor.push([[affine1, lim1, '-\\infty', affine2, '+\\infty', stor.bonneReponse, '+\\infty', '+\\infty']])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
                } else {
                  stor.variables_cor.push([[affine1, lim1, '-\\infty', affine2, '0', stor.bonneReponse, '-\\infty', '0']])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
                }
                if (Number(stor.objet_derivee.variables[1]) === 1) {
                  stor.tab_textes_cor.push([textes.corr_Bis5_1])
                } else {
                  stor.tab_textes_cor.push([textes.corr_Ter5_1])
                }
              } else {
                if (Number(coefdir2) < 0) { // l’expo aura pour limite +inf
                  stor.tab_textes_cor.push([textes.corr_5_1])
                  stor.variables_cor.push([[affine1, lim1, '-\\infty', affine2, '+\\infty', stor.bonneReponse, '+\\infty', '+\\infty']])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
                } else {
                // un cas particulier si on a coedir2==1, pas la peine de faire de chgt de variable
                  if (String(coefdir2) === '1') {
                  // et si b=0, pas de fraction à faire apparaitre
                    if (String(constante1) === '0') {
                      stor.tab_textes_cor.push([textes.corr_5_2, textes.corr_5_5])
                      stor.variables_cor.push([[affine1, lim1, '-\\infty', affine2, '0'], [lineaire1, stor.objet_derivee.variables[3], stor.tab_limites[i], stor.bonneReponse]])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
                    } else {
                      stor.tab_textes_cor.push([textes.corr_5_2, textes.corr_5_4])
                      stor.variables_cor.push([[affine1, lim1, '-\\infty', affine2, '0'], [affine1, stor.objet_derivee.variables[3], stor.tab_limites[i], fctsEtudeDivisionNbs(coefdir1, coefdir2), stor.bonneReponse]])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
                    }
                  } else {
                    stor.tab_textes_cor.push([textes.corr_5_2, textes.corr_5_3])
                    stor.variables_cor.push([[affine1, lim1, '-\\infty', affine2, '0'], [lineaire2, affine1, stor.objet_derivee.variables[3], stor.tab_limites[i], fctsEtudeDivisionNbs(coefdir1, coefdir2), stor.bonneReponse, lineaire2Par]])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
                  }
                }
              }
            }
            if (stor.tab_limites[i] === '+\\infty') {
              if (Number(coefdir1) === 0) {
                lim1 = stor.objet_derivee.variables[1]
              } else if (Number(coefdir1) < 0) {
                lim1 = '-\\infty'
              } else {
                lim1 = '+\\infty'
              }
              if (String(coefdir1) === '0') {
                if (Number(coefdir2) < 0) { // l’expo aura pour limite +inf
                  stor.variables_cor.push([[affine1, lim1, '+\\infty', affine2, '0', stor.bonneReponse, '-\\infty', '0']])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
                } else {
                  stor.variables_cor.push([[affine1, lim1, '+\\infty', affine2, '+\\infty', stor.bonneReponse, '+\\infty', '+\\infty']])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
                }
                if (Number(stor.objet_derivee.variables[1]) === 1) {
                  stor.tab_textes_cor.push([textes.corr_Bis5_1])
                } else {
                  stor.tab_textes_cor.push([textes.corr_Ter5_1])
                }
              } else {
                if (Number(coefdir2) > 0) { // l’expo aura pour limite +inf
                  stor.tab_textes_cor.push([textes.corr_5_1])
                  stor.variables_cor.push([[affine1, lim1, '+\\infty', affine2, '+\\infty', stor.bonneReponse, '+\\infty', '+\\infty']])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
                } else {
                  stor.tab_textes_cor.push([textes.corr_5_2, textes.corr_5_3])
                  stor.variables_cor.push([[affine1, lim1, '+\\infty', affine2, '0'], [lineaire2, affine1, stor.objet_derivee.variables[3], stor.tab_limites[i], fctsEtudeDivisionNbs(coefdir1, coefdir2), stor.bonneReponse, lineaire2Par]])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
                }

                stor.tab_textes_cor.push([textes.corr_1_3])
                stor.variables_cor.push([[affine1, stor.defFonction.limites_affine1[1], stor.defFonction.limites[1][1]]])// les variables de la correction de la limite en +inf
              }
            }
          }
        }
        break

      case 4:
        for (i = 0; i < stor.tab_limites.length; i++) {
          stor.tab_textes_cor.push([textes.corr_4_1, textes.corr_4_2, textes.corr_4_3])
          a = j3pPolynome([Number(stor.objet_derivee.variables[0]), Number(stor.objet_derivee.variables[1])])
          // var b=fctsEtudeSigneFois(stor.objet_derivee.variables[2])+"x^2"+signe(stor.objet_derivee.variables[3]);
          b = j3pPolynome([Number(stor.objet_derivee.variables[2]), 0, Number(stor.objet_derivee.variables[3])])
          c = j3pPolynome([Number(stor.objet_derivee.variables[0]), 0])
          d = '\\left(1+\\frac{' + stor.objet_derivee.variables[1] + '}{' + c + '}\\right)'
          e = j3pPolynome([Number(stor.objet_derivee.variables[2]), 0, 0])
          const f = '\\left(1+\\frac{' + stor.objet_derivee.variables[3] + '}{' + e + '}\\right)'
          let g = stor.objet_derivee.variables[0]
          const h = j3pPolynome([Number(stor.objet_derivee.variables[2]), 0])
          // cas particuliers
          if (Number(stor.objet_derivee.variables[1]) === 0) {
            d = ''
          } else {
            if (Number(stor.objet_derivee.variables[0]) === 1) {
              g = ''
            }
          }
          stor.variables_cor.push([[''], [a, b, c, d, e, f, g, h], [Number(stor.objet_derivee.variables[0]), stor.tab_limites[i]]])
        }

        break

      case 3:
        a = stor.objet_derivee.variables[0]
        b = stor.objet_derivee.variables[1]
        c = stor.objet_derivee.variables[2]
        d = stor.objet_derivee.variables[3]
        {
          let num, den
          for (i = 0; i < stor.tab_limites.length; i++) {
            if (stor.tab_limites[i].includes('\\infty')) { // même correction si + ou - inf, mais ça dépend du num
              if (String(stor.objet_derivee.variables[0]) === '0') { // cste au num
                stor.tab_textes_cor.push([textes.corr_3_1_b])
                // on recupere le + ou -, en esperant que le i soit le bon...
                const signe2 = stor.defFonction.limites_den[i].charAt(0)
                stor.variables_cor.push([[signe2 + '\\infty']])
              } else {
                if (d !== '0') {
                  if (d.includes('-')) {
                    den = c + '-\\frac{' + d.substring(1) + '}{x}'
                  } else {
                    den = c + '+\\frac{' + d + '}{x}'
                  }
                } else {
                  den = c
                }
                if (b !== '0') {
                  if (b.includes('-')) {
                    num = a + '-\\frac{' + b.substring(1) + '}{x}'
                  } else {
                    num = a + '+\\frac{' + b + '}{x}'
                  }
                } else {
                  num = a
                }
                stor.tab_textes_cor.push([textes.corr_3_1_a])
                stor.variables_cor.push([['\\frac{' + num + '}{' + den + '}', stor.bonneReponse]])
              }
            } else {
            // c’est une valeur interdite
              let symbole = ''
              let croissance = ''
              const cXPlusd = extraitDen(stor.defFonction.fonction)
              if (c.includes('-')) {
                croissance = textes.decroissante
              } else {
                croissance = textes.croissante
              }
              me.logIfDebug('stor.tab_limites[i]=', stor.tab_limites[i])
              let cons
              if (stor.tab_limites[i].includes('^-')) { // lim à gauche
                // console.log('AVEC i=', i, ' limite à gauche')
                cons = textes.corr_3_3
                symbole = (c.includes('-')) ? '>' : '<'
              // a : stor.tab_limites[i]
              // b : stor.tab_limites[i] en virant les deux derniers caracteres
              // c : cx+d
              // d : croissante/décroissante
              // e: < ou >
              // f : le - ou + du 0-/0+
              } else {
                // console.log('AVEC i=', i, ' limite à droite')
                cons = textes.corr_3_4
                symbole = (c.includes('-')) ? '<' : '>'
              }

              stor.tab_textes_cor.push([textes.corr_3_2, cons, textes.corr_3_5])
              stor.variables_cor.push([[fctsEtudeEcritLimite(stor.defFonction.limites_num[equivalent(i)])], [stor.tab_limites[i], stor.tab_limites[i].substring(0, stor.tab_limites[i].length - 2), cXPlusd, croissance, symbole, fctsEtudeEcritLimite(stor.defFonction.limites_den[equivalent(i)])], [stor.tab_limites[i], stor.bonneReponse]])
            }
          }
        }
        break

      case 2:
      case 8:

        for (i = 0; i < stor.tab_limites.length; i++) {
          let variable
          if (stor.defFonction.modele === 8) {
            variable = '\\left(1' + fctsEtudeSigne(fctsEtudeDivisionNbs(stor.objet_derivee.variables[1], stor.objet_derivee.variables[2])) + '\\times\\frac{1}{x}'
            variable = variable + fctsEtudeSigne(fctsEtudeDivisionNbs(stor.objet_derivee.variables[0], stor.objet_derivee.variables[2])) + '\\times\\frac{1}{x^2}'
            variable = variable + '\\right)'
            stor.variables_cor.push([[fctsEtudeEcrireMonome(1, 2, stor.objet_derivee.variables[2]), variable], [fctsEtudeEcrireMonome(1, 2, stor.objet_derivee.variables[2]), stor.bonneReponse]])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
          } else {
            variable = '\\left(1' + fctsEtudeSigne(fctsEtudeDivisionNbs(stor.objet_derivee.variables[2], stor.objet_derivee.variables[3])) + '\\times\\frac{1}{x}'
            variable = variable + fctsEtudeSigne(fctsEtudeDivisionNbs(stor.objet_derivee.variables[1], stor.objet_derivee.variables[3])) + '\\times\\frac{1}{x^2}'
            variable = variable + fctsEtudeSigne(fctsEtudeDivisionNbs(stor.objet_derivee.variables[0], stor.objet_derivee.variables[3])) + '\\times\\frac{1}{x^3}'
            variable = variable + '\\right)'
            stor.variables_cor.push([[stor.objet_derivee.variables[3] + 'x^3', variable], [stor.objet_derivee.variables[3] + 'x^3', stor.bonneReponse]])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
          }
          if (stor.tab_limites[i] === '-\\infty') {
            stor.tab_textes_cor.push([textes.corr_2_1, textes.corr_2_2])
          }
          if (stor.tab_limites[i] === '+\\infty') {
            stor.tab_textes_cor.push([textes.corr_2_1, textes.corr_2_3])
          }
        }
        break

      default : // modèle 1
        {
          let affine = stor.defFonction.fonction.split('exp')[0] // heureusement qu’on connait le modèle...
          affine = affine.substring(1, affine.length - 1)
          tab = affine.split('x')
          const lineaire = tab[0] + 'x'
          const constante = (tab[1])
          // obligé de feinter, ça dépend si le domaine de definition est surchargé, et donc ça dépend du nb de limites
          for (i = 0; i < stor.tab_limites.length; i++) {
            if (stor.tab_limites[i] === '-\\infty') {
              stor.tab_textes_cor.push([textes.corr_1_1, textes.corr_1_2])
              stor.variables_cor.push([[affine, stor.defFonction.limites_affine[0]], [lineaire + '\\mathrm{e}^x' + constante + '\\mathrm{e}^x']])// les variables de la correction de la lim en -inf, comme 2 textes, 2 tableaux de variables
            }
            if (stor.tab_limites[i] === '+\\infty') {
              stor.tab_textes_cor.push([textes.corr_1_3])
              stor.variables_cor.push([[affine, stor.defFonction.limites_affine[1], stor.defFonction.limites[1][1]]])// les variables de la correction de la limite en +inf
            }
          }
        }
        //               console.log("stor.tab_textes_cor=",stor.tab_textes_cor)
        //               console.log("stor.defFonction.limites_affine=",stor.defFonction.limites_affine)
        //               console.log("stor.variables_cor=",stor.variables_cor)
        break
    }
  }

  // normalement pas besoin de modifier en cas d’ajout de modèle de fonction (seuls les textes et la fonction genereTextesCorrection sont à modifier)
  function afficheCorrectionPerso (bonneReponse) {
    // le style n’est pas le même suivant que la réponse soit bonne ou fausse'
    stor.zoneExpli.style.color = (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
    let indexEtapeCourante
    switch (me.questionCourante % ds.nbetapes) {
      case 0:// dernière étape
        indexEtapeCourante = stor.tab_limites.length - 1
        break

      default :
        indexEtapeCourante = me.questionCourante % ds.nbetapes - 1
    }
    const tabvariables = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    // construction de l’affichage en fonction de l’étape, de stor.tab_textes_cor et de stor.variables_cor
    for (let i = 0; i < stor.tab_textes_cor[indexEtapeCourante].length; i++) {
      const txt = stor.tab_textes_cor[indexEtapeCourante][i]
      const obj = {}
      // on récupère les variables, par texte...
      for (let j = 0; j < stor.variables_cor[indexEtapeCourante][i].length; j++) {
        obj[tabvariables[j]] = stor.variables_cor[indexEtapeCourante][i][j]
      }
      obj.styletexte = {}
      j3pAffiche(stor.zoneExpli, '', txt, obj)
    }
  }

  // console.log("debut du code : ",this.etat)

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        init()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }

      break // case "enonce":

    case 'correction': {
      const fctsValid = stor.fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie

      const reponse = fctsValid.validationGlobale()

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        this.reponseManquante(stor.zoneCorr)
        return me.finCorrection()
      }

      // Une réponse a été saisie
      // Bonne réponse
      if (reponse.bonneReponse && (!me.isElapsed)) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        afficheCorrectionPerso(true)
        j3pEmpty(stor.laPalette)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux
      if (me.isElapsed) {
        // A cause de la limite de temps :
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        afficheCorrectionPerso(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      stor.zoneCorr.innerHTML = cFaux
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // il reste des essais, on reste en correction
        return me.finCorrection()
      }

      // Erreur au dernier essai
      j3pEmpty(stor.laPalette)
      afficheCorrectionPerso(false)
      // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break
    } // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      me.finNavigation(true)
      break
  }
}
