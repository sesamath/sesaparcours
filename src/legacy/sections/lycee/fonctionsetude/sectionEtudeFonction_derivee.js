import { j3pAddElt, j3pEmpty, j3pFocus, j3pMathquillXcas, j3pPaletteMathquill, j3pPolynome, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { fctsEtudeAfficheCorrection, fctsEtudeEcrireMonome, fctsEtudeEcritureDomaine, fctsEtudeEcritureLatexDerivee, fctsEtudeEcritureLatexFonction, fctsEtudeEcriturePourCalcul, fctsEtudeNombreIntervalle, fctsEtudeSigne } from 'src/legacy/outils/fonctions/etude'
import textesGeneriques from 'src/lib/core/textes'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph/index'

import { aideModeles, initModele, initSection } from './common'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Alexis Lecomte (et Rémi Deniaud)
    Mars 2015
    Section intégrable dans un graphe avec données du graphe (on utilise la variable imposer_fct pour donner une fonction particulière suivant un modèle) ou donneesPrecedentes
    On dérive la fonction
    En cas d’ajout de modèles, il faudra juste modifier les fonctions fctsEtudeGenereAleatoire et genereTextesCorrection puis ajouter les textes de correction qui vont bien
    Non implémenté pour l’instant : le retour des réponses de l’élève pour la section bilan (qui n’a jamais été finalisée)
    C’est en case correction qu’il faudra implémenter me.DonneesPersistantes.etudeFonctions.repEleve
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['imposer_fct', '', 'string', 'On peut renseigner une fonction correspondant au modèle ou laisser vide pour générer aléatoirement une fonction suivant un modèle'],
    ['modele', [1], 'array', aideModeles],
    ['imposer_domaine', [], 'array', 'On donne le domaine de définition sous la forme d’un tableau où chaque intervalle est représenté par 4 éléments : borne inf, borne sup et les deux crochets. Par exemple ["-infini","0","]","]"] pour l’intervalle ]-infini;0].'],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la fonction étudiée est celle du nœud précédent.']
  ],
  // pour fonctionnement dans editgraphes
  donnees_parcours_description: 'Cette section peut réutiliser les paramètres modele, imposer_fct et imposer_domaine d’une autre section aux même paramètres.',
  donnees_parcours_nom_variable: 'imposer_fct',

  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'facteurs_non_strictement_positifs_dans_la_derivee' },
    { pe_2: 'derivee_aux_facteurs_strictement_positifs' }
  ]

}

const textes = {
  titre: 'Dérivée d’une fonction sur son ensemble de définition',
  phrase1: 'On considère la fonction définie et dérivable sur $£a$ par : $f(x)=£b$',
  phrase1_bis: 'On considère la fonction définie sur $£a$, dérivable sur $£c$ par : $f(x)=£b$',
  phrase2: 'Détermine l’expression de sa dérivée : $f\\quad\'(x)=$&1&',
  corr_1_1: '<br>$f$ est dérivable sur $£b$ comme produit de fonctions dérivables sur $£b$ et on a $f=u\\times v$ avec $u(x)=£a$ et $v(x)=\\mathrm{e}^x$ donc $f\\quad\'=u’v+uv\'$<br>', // correction du modèle 1
  corr_1_2: 'Comme $u\'(x)=£a$ et $v\'(x)=\\mathrm{e}^x$, on a $f\\quad\'(x)=£a\\times\\mathrm{e}^x+(£b)\\mathrm{e}^x$ que l’on factorise en $f\\quad\'(x)=(£a+(£b))\\mathrm{e}^x$<br>',
  corr_1_3: 'Ainsi, $\\forall x \\in £b$, $f\\quad\'(x)=£a$',
  corr_2_1: '<br>Comme $f$ est un polynôme, $f$ est dérivable sur $£b$ et on a $f\\quad\'(x)=£a$<br>', // correction des modèles 2 et 8
  corr_2_2: 'Ainsi, après simplification, $\\forall x \\in £b$, $f\\quad\'(x)=£a$',
  // marche pour modèles 3 et 4:
  corr_3_1: '<br>$f$ est dérivable sur son ensemble de définition, comme quotient de deux fonctions dérivables sur cet ensemble avec un dénominateur qui ne s’annule pas.<br>On a $f=\\frac{u}{v}$ avec $u(x)=£a$ et $v(x)=£b$ on a donc $f\\quad\'=\\frac{u’v-uv\'}{v^2}$.<br>',
  corr_3_2: 'Or, $u\'(x)=£a$ et $v\'(x)=£b$ donc après simplification on obtient $f\\quad\'(x)=£c$',
  corr_5_1: '<br>$f$ est dérivable sur $£c$ comme produit de fonctions dérivables sur $£c$ et on a $f=u\\times v$ avec $u(x)=£a$ et $v(x)=\\mathrm{e}^{£b}$ donc $f\\quad\'=u’v+uv\'$<br>', // correction du modèle 5
  corr_5_2: 'Comme $u\'(x)=£a$ et $v\'(x)=£c\\mathrm{e}^{£b}$, on a $f\\quad\'(x)=£a\\times\\mathrm{e}^{£b}+(£d)\\times£c\\mathrm{e}^{£b}$ que l’on factorise en $f\\quad\'(x)=(£a+(£d)\\times£c)\\mathrm{e}^{£b}$<br>',
  // J’adapte le modèle 5 au cas de figure b*exp(cx+d)
  corr_Bis5_1: '<br>$f$ est dérivable $£b$, la fonction exponentielle l’étant sur $£b$, on a $f(x)=£a \\mathrm{e}^{ax+b}$ donc $f\\quad\'(x)=£a a \\mathrm{e}^{ax+b}$<br>', // £a peut être "" ou k\\times
  corr_Bis5_2: 'On obtient $f\\quad\'(x)=£b$<br/>',
  corr_6_2: 'Or, $u\'(x)=£a$ et $v\'(x)=£b$ donc $f\\quad\'(x)=£c=£d$ on obtient donc $f\\quad\'(x)=£e$',
  corr_6_3: 'Or, $u\'(x)=£a$ et $v\'(x)=£b$ donc $f\\quad\'(x)=£c=£d$ on obtient donc après simplification par $£e$ : $f\\quad\'(x)=£f$',
  corr_7_0: '$f$ est de la forme $v£a\\times\\ln$ avec $v(x)=£b$.<br>Donc $f$ est dérivable sur $£d$ et comme $\\ln$ a pour dérivée la fonction inverse on a $f\\quad\'(x)=v\'(x)£a\\times\\frac{1}{x}$',
  corr_7_1: '$f$ est de la forme $v£a\\times\\ln(u)$ avec $v(x)=£b$ et $u(x)=£c$ et $u>0$ sur $£d$.<br>Donc $f$ est dérivable sur $£d$ et comme $\\ln(u)$ a pour dérivée $\\frac{u\'}{u}$ on a $f\\quad\'(x)=v\'(x)£a\\times\\frac{u\'(x)}{u(x)}$',
  corr_7_2: '<br>Or, $v\'(x)=£a$ et $u\'(x)=£b$ donc $\\forall x \\in £c$, on a $f\\quad\'(x)=£a£d\\times\\frac{£b}{£e}$ soit après réduction au même dénominateur $f\\quad\'(x)=£f$',
  corr_7_3: '<br>Or, $v\'(x)=£a$ donc $\\forall x \\in £c$, on a $f\\quad\'(x)=£a£d\\times\\frac{£b}{£e}$ soit après réduction au même dénominateur $f\\quad\'(x)=£f$'
}
/**
 * section EtudeFonction_derivee
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function ajoutePar (nb) {
    return (Number(nb) < 0) ? '(' + nb + ')' : nb
  }

  /**
   * Retourne [numerateur, denominateur] pour le modele 3
   * @private
   * @returns {string[]} le tableau [num, den]
   */
  function extraitNumDen () {
    const fct = stor.defFonction.fonction
    const tab = []
    const index = fct.indexOf('/')
    const den = fct.substring(index + 1)
    const num = fct.substring(1, index - 1)
    me.logIfDebug('den=', den, ' et den.substring(1,den.length-1)=', den.substring(1, den.length - 1))
    tab.push(num, den.substring(1, den.length - 1))
    me.logIfDebug('tab=', tab)
    return tab
  }

  function depart () {
    // figure mathgraph (encodée en base64)
    const fig = 'TWF0aEdyYXBoSmF2YTEuMAAAAA0+TMzNAANmcmH###8BAP8BAAAAAAAAAAAAAAAAAAAAAiYAAAGQAAAAAAAAAAAAAAAAAAAACv####8AAAABABFvYmpldHMuQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABABFvYmpldHMuQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEADG9iamV0cy5DRm9uYwD#####AAFmAAwoeCsxKSpleHAoeCn#####AAAAAQARb2JqZXRzLkNPcGVyYXRpb24CAAAAAwD#####AAAAAgAYb2JqZXRzLkNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAE#8AAAAAAAAP####8AAAACABBvYmpldHMuQ0ZvbmN0aW9uBwAAAAQAAAAAAAF4AAAAAgD#####AAFnAAd4Lyh4LTEpAAAAAwMAAAAEAAAAAAAAAAMBAAAABAAAAAAAAAABP#AAAAAAAAAAAXj#####AAAAAQAOb2JqZXRzLkNDYWxjdWwA#####wABYQACLTH#####AAAAAQATb2JqZXRzLkNNb2luc1VuYWlyZQAAAAE#8AAAAAAAAAAAAAYA#####wAEZmRlYQAEZihhKf####8AAAABABVvYmpldHMuQ0FwcGVsRm9uY3Rpb24AAAAB#####wAAAAEAFm9iamV0cy5DUmVzdWx0YXRWYWxldXIAAAADAAAABgD#####AARnZGVhAARnKGEpAAAACAAAAAIAAAAJAAAAAwAAAAYA#####wAJcHJlY2lzaW9uADgxMy1tYXgoaW50KGxuKGFicyhmZGVhKihmZGVhPD4wKSsyKihmZGVhPTApKSkvbG4oMTApKSwwKQAAAAMBAAAAAUAqAAAAAAAA#####wAAAAEAFG9iamV0cy5DRm9uY3Rpb24yVmFyAAAAAAUCAAAAAwMAAAAFBgAAAAUAAAAAAwAAAAADAgAAAAkAAAAEAAAAAwkAAAAJAAAABAAAAAEAAAAAAAAAAAAAAAMCAAAAAUAAAAAAAAAAAAAAAwgAAAAJAAAABAAAAAEAAAAAAAAAAAAAAAUGAAAAAUAkAAAAAAAAAAAAAQAAAAAAAAAAAAAABgD#####AAdlZ2FsaXRlAB5hYnMoZihhKS1nKGEpKTwxMF4oLXByZWNpc2lvbikAAAADBAAAAAUAAAAAAwEAAAAIAAAAAQAAAAkAAAADAAAACAAAAAIAAAAJAAAAA#####8AAAABABFvYmpldHMuQ1B1aXNzYW5jZQAAAAFAJAAAAAAAAAAAAAcAAAAJAAAABv####8AAAACAA1vYmpldHMuQ0xhdGV4AP####8AAAAAAAIAAAH#####AUBNAAAAAAAAQE2AAAAAAAAAAAAAAAAAAAAAAB5cdGV4dHtlZ2FsaXRlID0gfVxWYWx7ZWdhbGl0ZX3#####AAAAAQAVb2JqZXRzLkNUZXN0RXhpc3RlbmNlAP####8AAk9LAAAABf###############w=='
    stor.liste = stor.mtgAppLecteur.createList(fig)
  }

  function initEnonce () {
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    initSection(me, textes.titre)
    enonce()
  }

  function enonce () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    /// /////////////////////////////////////// Pour MathGraph32

    // ce qui suit lance la création initiale de la figure
    depart()
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    // On regarde si les données sur la fonction doivent être récupérées du noeud précédent
    initModele(me)
    // on peut vérifier que tout a bien été récupéré (en mode debug)
    me.logIfDebug('me.donneesPersistantes.etudeFonctions', me.donneesPersistantes.etudeFonctions)
    const domaineDef = fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)
    const domaineDeriv = fctsEtudeEcritureDomaine(stor.defFonction.domaineDeriv)
    const laFonction = fctsEtudeEcritureLatexFonction(stor.defFonction.fonction, stor.defFonction.modele)
    stor.objet_derivee = fctsEtudeEcritureLatexDerivee(laFonction, stor.defFonction.modele, stor.defFonction.domaineDeriv, ds.debug)
    me.logIfDebug('j3p.SectionEtudeFonction_signederivee.objet_derivee=', stor.objet_derivee)
    // détermination de la présence de facteurs négatifs dans la derivee pour déterminer la PE (permettra de squizzer une section dans un parcours)
    switch (stor.defFonction.modele) {
      case 4:
        stor.presence_facteurs_negatifs = true
        break

      case 3:
        // on teste que le signe de num dans f'=num/()^2
        stor.presence_facteurs_negatifs = !(stor.objet_derivee.signes_facteurs[0].includes('+') || stor.objet_derivee.signes_facteurs[0] === '+')
        break

      case 2:
        stor.presence_facteurs_negatifs = (stor.objet_derivee.signes_facteurs.length !== 1 || stor.objet_derivee.signes_facteurs[0] !== '+') // si delta négatif et a positif
        break

      default:// j’ai forcément un facteur qui n’est pas strictement positif...
        stor.presence_facteurs_negatifs = true
    }
    me.parcours.pe = (stor.presence_facteurs_negatifs) ? ds.pe_1 : ds.pe_2
    if (domaineDef === domaineDeriv) {
      j3pAffiche(stor.zoneCons1, '', textes.phrase1,
        {
          a: domaineDef,
          b: laFonction
        })
    } else {
      j3pAffiche(stor.zoneCons1, '', textes.phrase1_bis,
        {
          a: domaineDef,
          b: laFonction,
          c: domaineDeriv
        })
    }
    stor.elt = j3pAffiche(stor.zoneCons2, '', textes.phrase2,
      {
        inputmq1: { texte: '' }
      })
    mqRestriction(stor.elt.inputmqList[0], 'x\\d,.+-^()*/', {
      commandes: ([2, 3, 4, 8].includes(stor.defFonction.modele))
        ? ['racine', 'fraction', 'puissance', 'inf']
        : ['exp', 'ln', 'racine', 'fraction', 'puissance', 'inf']
    })
    j3pFocus(stor.elt.inputmqList[0])
    stor.bonneReponse = stor.objet_derivee.expres_derivee
    // console.log('la bonne réponse =', stor.bonneReponse)

    stor.laPalette = j3pAddElt(stor.zoneCons3, 'div', '', { style: { paddingTop: '15px' } })
    j3pPaletteMathquill(stor.laPalette, stor.elt.inputmqList[0], {
      liste: ([2, 3, 4, 8].includes(stor.defFonction.modele))
        ? ['racine', 'fraction', 'puissance', 'inf']
        : ['exp', 'ln', 'racine', 'fraction', 'puissance', 'inf']
    })

    stor.zoneExpli = j3pAddElt(stor.zoneCons4, 'div', '', { style: me.styles.toutpetit.explications })

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    genereTextesCorrection()// se base sur le modèle pour que ce soit simple d’en rajouter
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.elt.inputmqList[0].id] })

    // Obligatoire
    me.finEnonce()
  }

  // Reste à tester en cas de bonne réponse si c’est factorisé pour le signaler à l’élève
  function validation (repeleve) {
    const newRepeleve = stor.liste.traiteSignesMult(fctsEtudeEcriturePourCalcul(j3pMathquillXcas(repeleve)))
    const deriveeMtg32 = stor.liste.traiteSignesMult(fctsEtudeEcriturePourCalcul(j3pMathquillXcas(stor.objet_derivee.expres_derivee)))
    stor.liste.giveFormula2('f', deriveeMtg32)
    stor.liste.giveFormula2('g', newRepeleve)
    stor.liste.calculateNG() // les fonctions f et g du fichier mtg32 prennent comme expressions la dérivée et la réponse de l’élève
    me.logIfDebug('dérivée :' + deriveeMtg32 + '   reponse_eleve:' + newRepeleve)
    const mesNombres = fctsEtudeNombreIntervalle(stor.defFonction.domaineDeriv).tests_images
    // mesNombres est un tableau contenant 10 nombres de l’ensemble de dérivabilité de la fonction
    // je vais calculer les images de ces 10 nombres par la réponse de l’élève et la dérivée attendue.
    // si ces images sont égales 2 à 2, je considère que les fonctions sont égales.

    let egaliteFcts = true

    for (let k = 0; k < mesNombres.length; k++) {
      stor.liste.giveFormula2('a', String(mesNombres[k]))
      stor.liste.calculateNG()// dans le fichier mtg32, on donne à a les différentes valeurs de mesNombres
      me.logIfDebug('a=' + stor.liste.valueOf('a') + '  egalité:' + stor.liste.valueOf('egalite'), '\nf(a)=' + stor.liste.valueOf('fdea') + '     g(a)=' + stor.liste.valueOf('gdea') + '   precision:' + stor.liste.valueOf('precision'))
      if (Number(stor.liste.valueOf('OK')) === 0) { // ce "OK" est pour vérifier si l’objet g(a) existe bien dans mtg32
        console.debug('je ne comprends pas la réponse saisie')// Ici il n’existe pas, c’est que la fonction saisie n’est pas comprise'
        egaliteFcts = false
      } else {
        if (Number(stor.liste.valueOf('egalite')) === 0) { // c’est-à-dire que f(a)!=g(a)
          egaliteFcts = false// donc on considère que les fonctions ne sont pas égales
        }
      }
    }
    return egaliteFcts
  }

  function genereTextesCorrection () {
    let a, b, c, d, e, tab
    switch (stor.defFonction.modele) {
      case 7:
        a = Number(stor.objet_derivee.variables[0])
        b = Number(stor.objet_derivee.variables[1])
        c = Number(stor.objet_derivee.variables[2])
        d = Number(stor.objet_derivee.variables[3])
        e = Number(stor.objet_derivee.variables[4])
        {
          const dxpluse = fctsEtudeEcrireMonome(1, 1, d) + fctsEtudeEcrireMonome(2, 0, e)
          const axplusb = fctsEtudeEcrireMonome(1, 1, a) + fctsEtudeEcrireMonome(2, 0, b)
          const domaineDef = fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)
          if (d === 1 && e === 0) {
            stor.tab_textes_cor = [textes.corr_7_0, textes.corr_7_3]
          } else {
            stor.tab_textes_cor = [textes.corr_7_1, textes.corr_7_2]
          }

          stor.variables_cor = [
            [fctsEtudeSigne(c), axplusb, dxpluse, domaineDef],
            [a, d, domaineDef, fctsEtudeSigne(c), dxpluse, stor.bonneReponse]
          ]
        }
        break

      case 6:
        tab = extraitNumDen()
        me.logIfDebug('tab=', tab)
        tab[0] = tab[0].replace(/ln(?:\sx|\(x\))/g, '\\ln x')
        a = Number(stor.objet_derivee.variables[0])
        b = Number(stor.objet_derivee.variables[1])
        c = Number(stor.objet_derivee.variables[2])
        {
          const uprime = '\\frac{' + b + '}{x}'
          let vprime, vcarre, vcarreMoinsUn, v

          if (c === 1) {
            vprime = '1'
            vcarre = 'x^2'
            v = 'x'
            vcarreMoinsUn = ''
          } else {
            v = 'x^' + c
            if (c === 2) {
              vprime = c + 'x'
              vcarreMoinsUn = 'x'
            } else {
              vprime = c + 'x^' + (c - 1)
              vcarreMoinsUn = 'x^' + (c - 1)
            }
            vcarre = '(x^' + c + ')^2'
          }
          let details1 = '\\frac{\\frac{' + b + '}{x}\\times ' + v + '-'
          if (a === 0 && b > 0) {
            details1 += tab[0]
          } else {
            details1 += '(' + tab[0] + ')'
          }
          details1 += '\\times ' + vprime + '}{' + vcarre + '}'
          let details2 = '\\frac{' + b
          details2 += vcarreMoinsUn
          if (a !== 0) {
            details2 += fctsEtudeSigne(-a * c) + vcarreMoinsUn
          }
          details2 += fctsEtudeSigne(-b * c) + vcarreMoinsUn + '\\ln x}{x^' + (2 * c) + '}'
          if (c === 1) {
            stor.tab_textes_cor = [textes.corr_3_1, textes.corr_6_2]
            stor.variables_cor = [
              [tab[0], v],
              [uprime, vprime, details1, details2, stor.bonneReponse]
            ]
          } else {
            stor.tab_textes_cor = [textes.corr_3_1, textes.corr_6_3]
            stor.variables_cor = [
              [tab[0], v],
              [uprime, vprime, details1, details2, vcarreMoinsUn, stor.bonneReponse]
            ]
          }
        }
        break
      case 5 :
        if (Number(stor.objet_derivee.variables[0]) === 0) {
          // De la forme b*exp(cx+d)
          stor.tab_textes_cor = [textes.corr_Bis5_1, textes.corr_Bis5_2, textes.corr_1_3]
          const maVara = (Number(stor.objet_derivee.variables[1]) === 1)
            ? ''
            : (Number(stor.objet_derivee.variables[1]) === -1) ? '-' : stor.objet_derivee.variables[1] + '\\times '
          /* corr_Bis5_1 : "<br>$f$ est dérivable sur $\\R$, la fonction exponentielle l’étant sur $\\R$ et on a $f=£a \\mathrm{e}^v$ donc $f\\quad'=£a v'\\mathrm{e}^v<br/>$",//£a peut être "" ou k\\times
   corr_Bis5_2 : "Comme $v'(x)=£b$, on a $f\\quad'(x)=£c<br/>",
   */
          stor.variables_cor = [
            [maVara, fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)],
            [stor.objet_derivee.variables[2], maVara + ajoutePar(stor.objet_derivee.variables[2]) + '\\mathrm{e}^{' + j3pPolynome([Number(stor.objet_derivee.variables[2]), Number(stor.objet_derivee.variables[3])]) + '}'], // les variables de la correction de l’étape 1, comme 2 textes, 2 tableaux de variables
            [stor.objet_derivee.expres_derivee, fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)]
          ]
        } else {
          stor.tab_textes_cor = [textes.corr_5_1, textes.corr_5_2, textes.corr_1_3]
          stor.variables_cor = [
            [j3pPolynome([Number(stor.objet_derivee.variables[0]), Number(stor.objet_derivee.variables[1])]), j3pPolynome([Number(stor.objet_derivee.variables[2]), Number(stor.objet_derivee.variables[3])]), fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)],
            [stor.objet_derivee.variables[0], j3pPolynome([Number(stor.objet_derivee.variables[2]), Number(stor.objet_derivee.variables[3])]), ajoutePar(stor.objet_derivee.variables[2]), j3pPolynome([Number(stor.objet_derivee.variables[0]), Number(stor.objet_derivee.variables[1])])], // les variables de la correction de l’étape 1, comme 2 textes, 2 tableaux de variables
            [stor.objet_derivee.expres_derivee, fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)]
          ]
        }
        break

      case 4:
        tab = extraitNumDen()
        stor.tab_textes_cor = [textes.corr_3_1, textes.corr_3_2]
        stor.variables_cor = [
          [tab[0], tab[1]],
          [stor.objet_derivee.variables[0], j3pPolynome([2 * Number(stor.objet_derivee.variables[2]), 0]), stor.bonneReponse]
        ]

        break
      case 3:
        tab = extraitNumDen()
        stor.tab_textes_cor = [textes.corr_3_1, textes.corr_3_2]
        stor.variables_cor = [
          [tab[0], tab[1]],
          [stor.objet_derivee.variables[0], stor.objet_derivee.variables[2], stor.bonneReponse]
        ]

        break
      case 2:
      case 8:
        if (stor.defFonction.modele === 8 && Math.abs(Math.abs(stor.objet_derivee.variables[2]) - 1) < Math.pow(10, -12)) {
          stor.tab_textes_cor = [textes.corr_2_1]
          stor.variables_cor = [[stor.objet_derivee.expres_derivee, fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)]]
        } else {
          stor.tab_textes_cor = [textes.corr_2_1, textes.corr_2_2]
          stor.variables_cor = (stor.defFonction.modele === 2)
            ? [
                [stor.objet_derivee.variables[3] + '\\times 3x^2' + fctsEtudeSigne(stor.objet_derivee.variables[2]) + '\\times 2x' + fctsEtudeSigne(stor.objet_derivee.variables[1]), fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)],
                [stor.objet_derivee.expres_derivee, fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)]
              ]
            : [
                [stor.objet_derivee.variables[2] + '\\times 2x' + fctsEtudeSigne(stor.objet_derivee.variables[1]), fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)],
                [stor.objet_derivee.expres_derivee, fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)]
              ]
        }
        break

      default : // modèle 1
        stor.tab_textes_cor = [textes.corr_1_1, textes.corr_1_2, textes.corr_1_3]
        stor.variables_cor = [
          [stor.objet_derivee.variables[0] + 'x' + fctsEtudeSigne(stor.objet_derivee.variables[1]), fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)],
          [stor.objet_derivee.variables[0], stor.objet_derivee.variables[0] + 'x' + fctsEtudeSigne(stor.objet_derivee.variables[1])], // les variables de la correction de l’étape 1, comme 2 textes, 2 tableaux de variables
          [stor.objet_derivee.expres_derivee, fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)]
        ]
        break
    }
  }

  // console.log("debut du code : ",me.etat)

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.logIfDebug('ds.imposer_fct : ', ds.imposer_fct)
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              initEnonce()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonce()
      }

      break // case "enonce":

    case 'correction': {
      const fctsValid = stor.fctsValid
      // comme pour l’utilisation de Validation_zones, je déclare l’objet reponse qui contient deux propriétés : aRepondu et bonneReponse
      const reponse = {}
      reponse.aRepondu = true
      reponse.bonneReponse = true
      reponse.aRepondu = fctsValid.valideReponses()
      let bonneReponse
      if (reponse.aRepondu) {
        const rep = j3pValeurde(stor.elt.inputmqList[0])
        me.logIfDebug('rep=', rep)
        const repXcas = j3pMathquillXcas(rep)
        me.logIfDebug('repXcas=', repXcas)
        bonneReponse = validation(repXcas)
        me.logIfDebug('bonneReponse=', bonneReponse)
        fctsValid.zones.bonneReponse[0] = bonneReponse
        fctsValid.coloreLesZones()
      }
      if (!reponse.aRepondu && !me.isElapsed) {
        me.reponseManquante(stor.zoneCorr)
        me.afficheBoutonValider()
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (bonneReponse && !me.isElapsed) {
          me._stopTimer()
          j3pDesactive(stor.elt.inputmqList[0])
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          fctsEtudeAfficheCorrection(stor.zoneExpli, me.styles.cbien, stor.tab_textes_cor, stor.variables_cor)
          j3pEmpty(stor.laPalette)
          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.cacheBoutonValider()
            fctsEtudeAfficheCorrection(stor.zoneExpli, me.styles.toutpetit.correction.color, stor.tab_textes_cor, stor.variables_cor)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
            } else {
              // Erreur au nème essai
              j3pEmpty(stor.laPalette)
              fctsEtudeAfficheCorrection(stor.zoneExpli, me.styles.toutpetit.correction.color, stor.tab_textes_cor, stor.variables_cor)
              j3pDesactive(stor.elt.inputmqList[0])
              me._stopTimer()
              me.cacheBoutonValider()
              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
    }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section, exceptionnelement déjà fait lors du tirage de l’aléatoire...
        // me.parcours.pe = me.score / ds.nbitems;
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
