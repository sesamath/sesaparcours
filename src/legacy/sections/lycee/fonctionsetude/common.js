/**
 * Code commun aux sections sur les études de fonction
 */

import { j3pGetRandomElt, j3pShowError } from 'src/legacy/core/functions'
import { fctsEtudeGenereAleatoire, fctsEtudeGenereDonnees, fctsEtudeReconstructionDomaine } from 'src/legacy/outils/fonctions/etude'

/**
 * Liste des modèles disponibles pour les sections 'sectionEtudeFonction_xxx.js' à modifier ici lors de l’implémentation d’un nouveau modèle
 * @type {number[]}
 */
export const tabModeles = [1, 2, 3, 4, 5, 6, 7, 8]

/**
 * String d’aide au choix des modèles possibles à mettre dans les params de la section
 * @type {string}
 */
export const aideModeles = `Tableau des modèles possibles :
- modèle 1 : (ax+b)exp(x)
- modèle 2 : ax^3+bx^2+cx+d
- modèle 3 : (ax+b)/(cx+d)
- modèle 4 : (ax+b)/(cx^2+d)
- modèle 5 : (ax+b)exp(cx+d)
- modèle 6: (a+bln(x))/x^c
- modèle 7 : ax+b+cln(dx+e), avec possibilité d’imposer juste d et e en écrivant ax+b+cln(3x-2) par exemple dans imposer_fct
- modèle 8 : ax^2+bx+c
Le choix se fera aléatoirement parmi les modèles précisés dans ce tableau`

/**
 * Code d’init commun
 * @param parcours
 * @param titre
 */
export function initSection (parcours, titre) {
  const me = parcours
  const ds = me.donneesSection
  const stor = me.storage

  me.construitStructurePage('presentation1')
  me.afficheTitre(titre)
  // NOUVEAUTE DP : en cas de fonction passée en param, il ne faut pas avoir plus d’une répétition, sinon on retrouve les mêmes données...
  if (ds.imposer_fct !== '' || ds.donneesPrecedentes) {
    me.surcharge({ nbrepetitions: 1 }) // ça va recalculer nbitems
  }
  if (ds.imposer_fct.includes('parcours')) {
    // ds.donneesPrecedentes sert à récupérer la fonction du nœud précédent
    ds.donneesPrecedentes = true
    ds.imposer_fct = ''
  }
  stor.defFonction = {}
  if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
}

/**
 * Récupère d’éventuelles données de sections précédentes et initialise le modèle de fonction
 * @param parcours
 */
export function initModele (parcours) {
  const me = parcours
  const ds = me.donneesSection
  const stor = me.storage

  let objetDonnees
  // On regarde si les données sur la fonction doivent être récupérées du noeud précédent
  if (ds.donneesPrecedentes) {
    // on récupère les données du noeud précédent stockées dans me.donneesPersistantes.etudeFonctions

    if (!me.donneesPersistantes.etudeFonctions) {
      ds.donneesPrecedentes = false
      j3pShowError(Error('Les données précédentes n’ont pu être récupérées. De nouvelles sont générées !\nAttention car la fonction n’est peut-être pas de la forme de celle qui était attendue.'))
      // Je vais relancer une nouvelle fonction, mais il faut que je récupère le modèle
      const leNoeud = me.graphe.find(noeud => noeud.length > 1)
      ds.modele = (leNoeud[2][1].modele) ? leNoeud[2][1].modele : [1] // si je ne trouve pas le modèle, je mets 1 par défaut
      ds.imposer_fct = ''
    }
  }
  if (ds.modele[0] === 0) ds.modele = tabModeles // je laisse ce cas de figure qui a été une idée passagère au cas où un graphe construit avec ce cas de figure existerait encore
  ds.modele = ds.modele.filter(m => tabModeles.includes(m)) // On enlève les cas de figure qui ne correspondent pas à un modèle
  if (ds.modele.length === 0) {
    console.error(Error('Aucune valeur du tableau modele ne correspond à un cas de figure de l’exercice. On redonne la valeur par défaut : [1]'))
    ds.modele = [1]
  }
  // on prend un modèle au hasard dans la liste demandée
  stor.defFonction.modele = j3pGetRandomElt(ds.modele) // lors du param on connait la fonction donc on spécifie le modèle ce qui permettra à la section de connaitre les limites

  if (ds.donneesPrecedentes) {
    // on récupère tout ce que ça contient pour le mettre dans stor
    for (const [prop, value] of Object.entries(me.donneesPersistantes.etudeFonctions)) {
      stor.defFonction[prop] = value
    }
  } else if (ds.imposer_fct !== '') { // c’est une fonction imposée par le prof
    me.logIfDebug('On impose une fonction particulière, éventuellement générée précédemmment : ', ds.imposer_fct)
    me.logIfDebug('rien dans le parcours donc on impose la fonction fournie')
    // je fais semblant de générer la fonction de manière aléatoire pour récupérer les domaines de def et de dérivabilité
    objetDonnees = fctsEtudeGenereDonnees(stor.defFonction.modele, ds.imposer_fct)
  } else {
    me.logIfDebug('pas de paramètre imposer_fct donc c aléatoire')
    objetDonnees = fctsEtudeGenereAleatoire(stor.defFonction.modele, ds.debug)
  }
  if (!ds.donneesPrecedentes) {
    for (const prop in objetDonnees) stor.defFonction[prop] = objetDonnees[prop]// variables plus parlantes pour la section
    if (ds.imposer_domaine.length !== 0) {
      // c’est que le domaine est imposé
      stor.defFonction.domaineDef = fctsEtudeReconstructionDomaine(ds.imposer_domaine, objetDonnees.val_interdites, ds.debug)
      // le domaine de dérivabilité sera le même (ce serait se prendre la tête pour rien si ce n’était pas le cas)
      stor.defFonction.domaineDeriv = fctsEtudeReconstructionDomaine(ds.imposer_domaine, objetDonnees.val_interdites, ds.debug)
    }
    // on stocke les données qui viennent d'être générées dans me.donneesPersistantes.etudeFonctions
    me.donneesPersistantes.etudeFonctions = {}
    for (const [prop, value] of Object.entries(stor.defFonction)) {
      me.donneesPersistantes.etudeFonctions[prop] = value
    }
  }
  me.logIfDebug('stor:', stor)
}
