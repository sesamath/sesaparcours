import { j3pAddContent, j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pPaletteMathquill, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since juin 2022
 * @fileOverview Cette section permet de donner la solution d’une équa diff y'=ay+b avec condition initiale
 * On peut ensuite reprendre ses données pour effectuer l’étude des variations de la fonction
 */
export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

const textes = {
  titre_exo: 'Solution d’une équation différentielle',
  consigne1: 'On considère l’équation $£e$.',
  consigne2: 'Déterminer la solution $f$ qui vaut $£y$ en $£x$.',
  comment1: 'La réponse est égale à la fonction attendue, mais il faut la simplifier.',
  comment2: 'Tu as une nouvelle tentative pour corriger.',
  corr1: 'L’équation équivaut à $£{e2}$.',
  corr2: 'Les fonctions solutions de cette équation différentielle sont définies sur $\\R$ sous la forme $f(x)=£{fk1}=£{fk}$ avec $k$ réel.',
  corr3: 'De plus $f(£x)=£y$, ce qui donne $£{eq1}$.',
  corr4: 'On obtient alors $k=£k$ et donc $f(x)=£f$.'
}

/**
 * section EtudeEquaDiff
 * @this {Parcours}
 */
export default function main () {
  const me = this
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(me)
        // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
        this.storage.elts = {}
        // et un autre pour nos listeners
        this.storage.listeners = {}
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour chaque sujet
 * @private
 * @return {Object}
 */
function genereAlea () {
  const obj = {}
  // L’équation est de la forme y'=ay+b avec b multiple de a (dans l’étude de la fonction, il faudra que b/a soit un entier
  // Dans l’énoncé initial, l’équation sera écrite sous la forme y'-ay=b
  do {
    obj.a = j3pGetRandomInt(-4, 4)
  } while ([0, 1].includes(obj.a))
  obj.b = obj.a * (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 4)
  obj.e = 'y\'' + j3pMonome(2, 1, -obj.a, 'y') + '=' + obj.b
  obj.e2 = 'y\'=' + j3pMonome(1, 1, +obj.a, 'y') + j3pMonome(2, 0, obj.b)
  obj.x = 0
  do {
    obj.y = j3pGetRandomInt(-5, 5)
    obj.k = obj.y + Math.round(obj.b / obj.a)
  } while ([1, 0].includes(obj.k))
  obj.fk1 = 'k\\mathrm{e}^{' + j3pMonome(1, 1, obj.a, 'x') + '}-\\frac{' + obj.b + '}{' + obj.a + '}'
  obj.fk = 'k\\mathrm{e}^{' + j3pMonome(1, 1, obj.a, 'x') + '}' + j3pMonome(2, 0, -Math.round(obj.b / obj.a))
  obj.f = j3pMonome(1, 1, obj.k, '\\mathrm{e}^{' + j3pMonome(1, 1, obj.a, 'x') + '}') + j3pMonome(2, 0, -Math.round(obj.b / obj.a))
  obj.eq1 = 'k' + j3pMonome(2, 0, -Math.round(obj.b / obj.a)) + '=' + obj.y
  obj.fonction = j3pMonome(1, 1, obj.k, 'exp(' + j3pMonome(1, 1, obj.a, 'x') + ')') + j3pMonome(2, 0, -obj.b / obj.a)
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  me.validOnEnter = true // ex donneesSection.touche_entree
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone puis énoncé
  // chargement mtg
  getMtgCore({ withMathJax: true })
    .then(
      // success
      (mtgAppLecteur) => {
        me.storage.mtgAppLecteur = mtgAppLecteur
        enonceEnd(me)
      },
      // failure
      (error) => {
        j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
      })
    // plantage dans le code de success
    .catch(j3pShowError)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.objDonnees = genereAlea()
  // on affiche l’énoncé
  for (const i of [1, 2]) {
    stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor['zoneCons' + i], '', textes['consigne' + i], stor.objDonnees)
  }
  stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div')
  const elt = j3pAffiche(stor.zoneCons3, '', '$f(x)=$&1&', stor.objDonnees)
  stor.zoneInput = elt.inputmqList[0]
  mqRestriction(stor.zoneInput, '\\d,.x+-', { commandes: ['fraction', 'exp'] })
  stor.laPalette = j3pAddElt(stor.conteneur, 'div')
  j3pStyle(stor.laPalette, { paddingTop: '15px' })
  j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['fraction', 'exp'] })
  stor.fctsValid = new ValidationZones({
    parcours: me,
    zones: [stor.zoneInput.id]
  })
  // Je récupère les données pour pouvoir les envoyer vers l’étude de fonction :
  me.donneesPersistantes.etudeFonctions = {
    domaineDef: ['-infini', '+infini', ']', '['],
    domaineDeriv: ['-infini', '+infini', ']', '['],
    limites: (stor.objDonnees.a > 0)
      ? [['-infini', String(-Math.round(stor.objDonnees.b / stor.objDonnees.a))], ['+infini', (stor.objDonnees.k > 0) ? '+\\infty' : '-\\infty']]
      : [['-infini', (stor.objDonnees.k > 0) ? '+\\infty' : '-\\infty'], ['+infini', String(-Math.round(stor.objDonnees.b / stor.objDonnees.a))]],
    limites_affine1: [stor.objDonnees.k, String(stor.objDonnees.k)],
    limites_affine2: [(stor.objDonnees.a > 0) ? '-\\infty' : '+\\infty',
      (stor.objDonnees.a > 0) ? '+\\infty' : '-\\infty'
    ],
    fonction: stor.objDonnees.fonction,
    modele: 5,
    val_interdites: []
  }
  // console.log('stor.objDonnees.f:', stor.objDonnees.f, stor.objDonnees.fonction)
  const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAPIAAACugAAAQEAAAAAAAAAAQAAAAf#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAItMf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABP#AAAAAAAAAAAAACAP####8AAWIAATIAAAABQAAAAAAAAAAAAAACAP####8AAWsAATMAAAABQAgAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wABZgAMaypleHAoYSp4KSti#####wAAAAEACkNPcGVyYXRpb24AAAAABQL#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAA#####8AAAACAAlDRm9uY3Rpb24HAAAABQIAAAAGAAAAAf####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAAGAAAAAgABeAAAAAQA#####wADcmVwAAszKmV4cCgteCkrNAAAAAUAAAAABQIAAAABQAgAAAAAAAAAAAAHBwAAAAMAAAAIAAAAAAAAAAFAEAAAAAAAAAABeP####8AAAADABBDVGVzdEVxdWl2YWxlbmNlAP####8AB2VnYWxpdGUAAAAEAAAABQEAAAAAAT#wAAAAAAAAAf###############w=='
  stor.mtgList = stor.mtgAppLecteur.createList(txtFigure)
  // Dans mtg32, la fonction est de la forme k*exp(ax)+b
  stor.mtgList.giveFormula2('a', stor.objDonnees.a)
  stor.mtgList.giveFormula2('b', -Math.round(stor.objDonnees.b / stor.objDonnees.a))
  stor.mtgList.giveFormula2('k', stor.objDonnees.k)
  stor.mtgList.calculateNG()
  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '20px' }) })
  j3pFocus(stor.zoneInput)
  stor.premiereChance = true // cela va me servir pour donner une tentative de plus si la réponse n’est pas simplifiée (mais une seule fois)
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  j3pDetruit(stor.laPalette)
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  for (const i of [1, 2, 3, 4]) {
    stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor['zoneExpli' + i], '', textes['corr' + i], stor.objDonnees)
  }
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = { aRepondu: false, bonneReponse: false }
  reponse.aRepondu = stor.fctsValid.valideReponses()
  if (reponse.aRepondu) {
    let repEleve = j3pMathquillXcas(stor.fctsValid.zones.reponseSaisie[0])
    repEleve = repEleve.replace(/e\^x/g, 'exp(x)')
    repEleve = repEleve.replace(/e\^\(/g, 'exp(')
    repEleve = repEleve.replace(/([0-9])([xe])/g, '$1*$2')
    // repEleve = repEleve.replace(/exp/g, 'e^')
    stor.mtgList.giveFormula2('rep', repEleve)
    stor.mtgList.calculateNG()
    reponse.bonneReponse = (stor.mtgList.valueOf('egalite') === 1)
    if (!reponse.bonneReponse) {
      // On vérifie si c’est juste parce que ce n’est aps simplifié //.replace(/exp/, 'e^') .replace(/exp/, 'e^')
      let fctTest = repEleve + '-(' + stor.objDonnees.fonction.replace(/e\^/, 'exp') + ')'
      fctTest = j3pMathquillXcas(fctTest.replace(/([0-9])([ex])/g, '$1*$2'))
      // console.log('repEleve:', repEleve, 'fctTest:', fctTest)
      const arbreChaine = new Tarbre(fctTest, 'x')
      stor.egalite = true
      for (let i = -4; i <= 4; i++) {
        stor.egalite = stor.egalite && (Math.abs(arbreChaine.evalue([i])) < Math.pow(10, -12))
      }
      if (stor.egalite && stor.premiereChance) me.essaiCourant -= 1
    }
    stor.fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
    stor.fctsValid.coloreUneZone(stor.zoneInput.id)
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    if (stor.egalite) {
      j3pAddContent(stor.divCorrection, '<br>')
      me.reponseKo(stor.divCorrection, textes.comment1, true)
    }
    if (stor.egalite && stor.premiereChance) {
      j3pAddContent(stor.divCorrection, '<br>')
      me.reponseKo(stor.divCorrection, textes.comment2, true)
      stor.premiereChance = false
    } else {
      me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    }
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
