import { j3pAddElt, j3pGetNewId, j3pValeurde } from 'src/legacy/core/functions'
import { fctsEtudeAfficheCorrection, fctsEtudeCalculValeur, fctsEtudeDetermineImage, fctsEtudeDetermineTabLimites, fctsEtudeDivisionNbs, fctsEtudeEcritLimite, fctsEtudeEcritureDomaine, fctsEtudeEcritureLatexDerivee, fctsEtudeEcritureLatexFonction, fctsEtudeEcriturePourCalcul, fctsEtudeEstDansDomaine, fctsEtudeExtraireNumDen, fctsEtudeProduitNbs, fctsEtudeSigneFct, fctsEtudeSommeNbs } from 'src/legacy/outils/fonctions/etude'
import tableauSignesVariations from 'src/legacy/outils/tableauSignesVariations'
import textesGeneriques from 'src/lib/core/textes'
import { j3pAffiche } from 'src/lib/mathquill/functions'

import { aideModeles, initModele, initSection } from './common'

/**
 * Jean-Claude Lhote le 27/02/2024
 * cette fonction fait le choix peu pertinent de placer le signe - au numérateur.
 * on retrouve cette écriture dans les limites, dans la ligne des x des tableaux de variations...
 * Le problème, c’est que si une fraction est donnée sous une forme correcte à savoir -\frac{numPositif}{denPositif}
 * alors, elle ne correspondra pas à ça.
 * Bug trouvé lorsque valeur_interdites comporte de telles 'bonnes fractions' et que domaineDef contient deux fois ces fractions (voir modèle 3), alors, la fraction transformée avec le - au numérateur se retrouve deux fois dans la ligne des x
 * @param txt
 * @return {*|string}
 */
export function ecritBienFraction (txt) {
  if (typeof txt !== 'string') return txt
  if (txt.includes('\\frac{')) { // il y a une fraction
    if (txt.charAt(0) === '-') {
      const [, num, den] = fctsEtudeExtraireNumDen(txt.substring(1)).map(el => Number(el))
      if (num * den > 0) return `-\\frac{${Math.abs(num)}}{${Math.abs(den)}}`
      else return `\\frac{${Math.abs(num)}}{${Math.abs(den)}}`
    } else {
      const [, num, den] = fctsEtudeExtraireNumDen(txt).map(el => Number(el))
      if (num * den > 0) return `\\frac{${Math.abs(num)}}{${Math.abs(den)}}`
      else return `-\\frac{${Math.abs(num)}}{${Math.abs(den)}}`
    }
  }
  return txt// dans tous les autres cas
}

export function ecritBienBorne (txt) {
  if (typeof txt !== 'string' || !txt.includes('frac')) return txt
  let fracPure
  let signe
  if (txt.includes('^-') || txt.includes('^+')) {
    fracPure = txt.substring(0, txt.length - 2)
    signe = txt.substring(txt.length - 2, txt.length)
  } else {
    fracPure = txt
    signe = ''
  }
  return ecritBienFraction(fracPure) + signe
}

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Alexis Lecomte (et Rémi Deniaud)
    Mars 2015
    Section intégrable dans un graphe avec données du graphe (on utilise la variable imposer_fct pour donner une fonction particulière suivant un modèle) ou donneesPrecedentes
    On édemande les variations d’une fonction (dans un tableau)
    En cas d’ajout de modèles, il faudra juste modifier les fonctions fctsEtudeGenereAleatoire et genereTextesCorrection puis ajouter les textes de correction qui vont bien
    Non implémenté pour l’instant : le retour des réponses de l’élève pour la section bilan (qui n’a jamais été finalisée)
    C’est en case correction qu’il faudra implémenter me.DonneesPersistantes.etudeFonctions.repEleve
*/
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['limites_a_trouver', true, 'boolean', 'pour savoir si l’on exige les limites dans le tableau ou non (si ce n’est pas le cas, le nombre de chances est réduit à 1)'],
    ['imposer_fct', '', 'string', 'On peut renseigner une fonction correspondant au modèle ou laisser vide pour générer aléatoirement une fonction suivant un modèle'],
    ['modele', [1], 'array', aideModeles],
    ['imposer_domaine', [], 'array', 'On donne le domaine de définition sous la forme d’un tableau où chaque intervalle est représenté par 4 éléments&nbsp;: borne inf, borne sup et les deux crochets. Par exemple ["-infini","0","]","]"] pour l’intervalle ]-∞;0].'],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la fonction étudiée est celle du nœud précédent.']
  ],

  // NOUVEAUTE DP (penser à la virgule ci-dessus), pour l’instant inutilisé dans le modèle, mais plus tard dans l’éditeur de graphes pourquoi pas...
  donnees_parcours_description: {
    fonction: 'l’expression de la fonction en latex générée aléatoirement suivant 4 modèles, si le param imposer_fct n’est pas renseigné.',
    derivee: 'l’expression de la dérivée en latex.',
    modele: 'Nombre entier, le modèle 1 correspond à (ax+b)exp(x) , modèle 2 à ..., on utilise ensuite les pptés .a, .b, etc pour récupérer les variables.'
  },
  // pour fonctionnement dans editgraphes
  donnees_parcours_nom_variable: 'imposer_fct'
}

/**
 * section EtudeFonction_variations
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  // en entrée un domaine du genre [ "-200", "\frac{-5}{3}", "[", "[", "\frac{-5}{3}", "200", "]", "]" ] et en sortie ["-200","200"]
  function extraitDomaine (domaine) {
    const tab = []
    tab.push(domaine[0])
    tab.push(domaine[domaine.length - 3])
    // console.log('tabl=', tab)
    return tab
  }

  /**
   * Pour comparer deux fractions sans s’occuper de savoir où est le signe -
   * @param {string} f1
   * @param {string} f2
   * return {boolean} true si f1=f2
   */
  function compareFractions (f1, f2) {
    if (!f1.includes('frac') || !f2.includes('frac')) return false
    let signe1 = 1
    const signe2 = 1
    if (f1.charAt(0) === '-') {
      signe1 = -1
      f1 = f1.substring(1)
    }
    if (f2.charAt(0) === '-') {
      signe1 = -1
      f2 = f2.substring(1)
    }
    const [num1, den1] = fctsEtudeExtraireNumDen(f1)
    const [num2, den2] = fctsEtudeExtraireNumDen(f2)
    const signe11 = num1 * den1 > 0 ? 1 : -1
    const signe22 = num2 * den2 > 0 ? 1 : -1
    const val1 = Math.abs(num1 / den1)
    const val2 = Math.abs(num2 / den2)
    return (signe1 * signe11 === signe2 * signe22 && val1 === val2)
  }

  function estLimiteDroiteGauche (nb) {
    // on recherche si le nb est une limite à gauche (premier booléen du tableau) et une limite à droite (second)
    // plusieurs façons de faire, on peut parcourrir domaineDef, j’ai choisi la facilité, savoir si c'était le premier element ou le dernier de ligne_des_x, a voir si compatible avec d’autres modeles (mais normalement ok)
    if (stor.ligne_des_x[0] == nb) { // eslint-disable-line eqeqeq
      return [false, true]
    }
    if (stor.ligne_des_x[stor.ligne_des_x.length - 1] == nb) { // eslint-disable-line eqeqeq
      return [true, false]
    }
    return [true, true]// dans tous les autres cas
  }

  function determineLigneDesX () {
    stor.zero_derivee = []
    let a, b, c, tab,
      zeroDerivee
    switch (stor.defFonction.modele) {
      case 7:
        zeroDerivee = stor.objet_derivee.zeros_facteurs[0][0]
        me.logIfDebug('zeroDerivee=', zeroDerivee)
        me.logIfDebug('zeroDerivee=', fctsEtudeEcriturePourCalcul(zeroDerivee))
        stor.zero_derivee_dans_domaine = fctsEtudeEstDansDomaine(fctsEtudeEcriturePourCalcul(zeroDerivee), stor.defFonction.domaineDef)
        stor.zero_derivee.push(zeroDerivee)
        if (stor.zero_derivee_dans_domaine) {
          stor.ligne_des_x = [fctsEtudeEcritLimite(stor.defFonction.domaineDef[0]), zeroDerivee, fctsEtudeEcritLimite(stor.defFonction.domaineDef[1])]
        } else {
          stor.ligne_des_x = [fctsEtudeEcritLimite(stor.defFonction.domaineDef[0]), fctsEtudeEcritLimite(stor.defFonction.domaineDef[1])]
        }
        me.logIfDebug('stor.ligne_des_x=', stor.ligne_des_x)
        break

      case 6:
        zeroDerivee = stor.objet_derivee.zeros_facteurs[0][0]
        me.logIfDebug('zeroDerivee=', zeroDerivee)
        me.logIfDebug('zeroDerivee=', fctsEtudeEcriturePourCalcul(zeroDerivee))
        stor.zero_derivee_dans_domaine = fctsEtudeEstDansDomaine(fctsEtudeEcriturePourCalcul(zeroDerivee), stor.defFonction.domaineDef)
        stor.zero_derivee.push(zeroDerivee)
        if (stor.zero_derivee_dans_domaine) {
          stor.ligne_des_x = [fctsEtudeEcritLimite(stor.defFonction.domaineDef[0]), zeroDerivee, fctsEtudeEcritLimite(stor.defFonction.domaineDef[1])]
        } else {
          stor.ligne_des_x = [fctsEtudeEcritLimite(stor.defFonction.domaineDef[0]), fctsEtudeEcritLimite(stor.defFonction.domaineDef[1])]
        }
        me.logIfDebug('stor.ligne_des_x=', stor.ligne_des_x)

        break
      case 5 :
        a = stor.objet_derivee.variables[0]
        b = stor.objet_derivee.variables[1]
        c = stor.objet_derivee.variables[2]
        if (Number(a) === 0) {
          stor.ligne_des_x = [fctsEtudeEcritLimite(stor.defFonction.domaineDef[0]), fctsEtudeEcritLimite(stor.defFonction.domaineDef[1])]
        } else {
          zeroDerivee = fctsEtudeProduitNbs(fctsEtudeProduitNbs(-1, fctsEtudeSommeNbs(a, fctsEtudeProduitNbs(b, c))), fctsEtudeDivisionNbs(1, fctsEtudeProduitNbs(c, a)))// -(a+bc)/ca
          me.logIfDebug('zeroDerivee=', zeroDerivee)
          me.logIfDebug('zeroDerivee=', fctsEtudeEcriturePourCalcul(zeroDerivee))
          // pour les autres modèles, ça peut être plus compliqué, si valeur interdite, il faut tester la présence du zero dans plusieurs intervalles.
          // A voir si c’est à coder en fonction du modèle ou si je peux tout faire d’un coup
          stor.zero_derivee_dans_domaine = fctsEtudeEstDansDomaine(fctsEtudeEcriturePourCalcul(zeroDerivee), stor.defFonction.domaineDef)
          stor.zero_derivee.push(zeroDerivee)
          //                        var fct_pour_calc = fctsEtudeEcriturePourCalcul(stor.objet_derivee.expres_derivee);
          //                        console.log("LA fct_pour_calc=",fct_pour_calc)
          //                        var arbre_fct = new Tarbre(fct_pour_calc,["x"]);
          //                        var b = arbre_fct.evalue([0]);
          if (stor.zero_derivee_dans_domaine) {
            stor.ligne_des_x = [fctsEtudeEcritLimite(stor.defFonction.domaineDef[0]), zeroDerivee, fctsEtudeEcritLimite(stor.defFonction.domaineDef[1])]
          } else {
            stor.ligne_des_x = [fctsEtudeEcritLimite(stor.defFonction.domaineDef[0]), fctsEtudeEcritLimite(stor.defFonction.domaineDef[1])]
          }
        }
        me.logIfDebug('stor.ligne_des_x=', stor.ligne_des_x)

        break
      case 4:// reprise du case 2
        tab = stor.objet_derivee.zeros_facteurs[0]
        me.logIfDebug('tab=', tab)
        stor.ligne_des_x = [fctsEtudeEcritLimite(stor.defFonction.domaineDef[0])]
        if (tab.length !== 0) {
          zeroDerivee = tab[0].split('|')
          me.logIfDebug('zeroDerivee=', zeroDerivee)
          stor.zero_derivee0_dans_domaine = fctsEtudeEstDansDomaine(fctsEtudeEcriturePourCalcul(zeroDerivee[0]), stor.defFonction.domaineDef)
          stor.zero_derivee.push(zeroDerivee[0])
          stor.zero_derivee1_dans_domaine = fctsEtudeEstDansDomaine(fctsEtudeEcriturePourCalcul(zeroDerivee[1]), stor.defFonction.domaineDef)
          stor.zero_derivee.push(zeroDerivee[1])
          me.logIfDebug('stor.zero_derivee0_dans_domaine=', stor.zero_derivee0_dans_domaine)
          me.logIfDebug('stor.zero_derivee1_dans_domaine=', stor.zero_derivee1_dans_domaine)
          if (stor.zero_derivee0_dans_domaine) {
            stor.ligne_des_x.push(zeroDerivee[0])
          }
          if (stor.zero_derivee1_dans_domaine) {
            stor.ligne_des_x.push(zeroDerivee[1])
          }
        }
        // pas de racine sinon
        stor.ligne_des_x.push(fctsEtudeEcritLimite(stor.defFonction.domaineDef[1]))
        break

      case 3 :
        stor.ligne_des_x = []
        //                        var a=stor.objet_derivee.variables[3];
        //                        var b=stor.objet_derivee.variables[2];
        //                        var c=stor.objet_derivee.variables[1];
        //                        var d=stor.objet_derivee.variables[0];
        // Il y a un problème ! voir https://forge.apps.education.fr/sesamath/sesaparcours/-/issues/247
        // Je fais en sorte que la même valeur ne soit pas ajoutée deux fois dans la ligne des x à cause d’un - placé au mauvais endroit
        for (let index = 0; index < stor.defFonction.domaineDef.length; index = index + 4) {
          const valeur = ecritBienFraction(fctsEtudeEcritLimite(stor.defFonction.domaineDef[index]))
          if (!stor.ligne_des_x.includes(valeur)) {
            if (!valeur.includes('frac') || !stor.ligne_des_x.some(el => compareFractions(el, valeur))) {
              stor.ligne_des_x.push(ecritBienFraction(valeur)) // Je reste convaincu que cette façon d’écrire des fraction n’est pas correcte.
            }
          }
          const valeur2 = ecritBienFraction(fctsEtudeEcritLimite(stor.defFonction.domaineDef[index + 1]))
          if (!stor.ligne_des_x.includes(valeur2)) {
            if (!valeur2.includes('frac') || !stor.ligne_des_x.some(el => compareFractions(el, valeur2))) {
              stor.ligne_des_x.push(ecritBienFraction(valeur2))
            }
          }
        }
        tab = stor.objet_derivee.zeros_facteurs
        me.logIfDebug('tab=', tab)
        zeroDerivee = ecritBienFraction(tab[1][0])
        me.logIfDebug('val interdite=', zeroDerivee)// ce n’est pas une racine de la dérivée mais de son dénominateur donc une valeur interdite...'
        if (fctsEtudeEstDansDomaine(fctsEtudeEcriturePourCalcul(zeroDerivee), extraitDomaine(stor.defFonction.domaineDef))) {
          stor.zero_derivee.push(zeroDerivee)
        }
        // A corriger
        // stor.ligne_des_x.push(fctsEtudeEcritLimite(stor.defFonction.domaineDef[1]));
        me.logIfDebug('ligne des x=', stor.ligne_des_x)
        break

      case 2 :
      case 8:
        tab = stor.objet_derivee.zeros_facteurs[0]
        me.logIfDebug('tab=', tab)
        stor.ligne_des_x = [fctsEtudeEcritLimite(stor.defFonction.domaineDef[0])]
        if (tab.length !== 0) {
          zeroDerivee = tab[0].split('|')
          me.logIfDebug('zeroDerivee=', zeroDerivee)
          stor.zero_derivee0_dans_domaine = fctsEtudeEstDansDomaine(fctsEtudeEcriturePourCalcul(zeroDerivee[0]), stor.defFonction.domaineDef)
          stor.zero_derivee.push(zeroDerivee[0])
          if (stor.defFonction.modele === 2) {
            stor.zero_derivee1_dans_domaine = fctsEtudeEstDansDomaine(fctsEtudeEcriturePourCalcul(zeroDerivee[1]), stor.defFonction.domaineDef)
            stor.zero_derivee.push(zeroDerivee[1])
          }
          me.logIfDebug('stor.zero_derivee0_dans_domaine=', stor.zero_derivee0_dans_domaine)
          me.logIfDebug('stor.zero_derivee1_dans_domaine=', stor.zero_derivee1_dans_domaine)
          if (stor.zero_derivee0_dans_domaine) stor.ligne_des_x.push(zeroDerivee[0])
          if (stor.zero_derivee1_dans_domaine) stor.ligne_des_x.push(zeroDerivee[1])
        }
        // pas de racine sinon
        stor.ligne_des_x.push(fctsEtudeEcritLimite(stor.defFonction.domaineDef[1]))
        break

      default :
        // on récupère d’abord le zéro de la dérivée'
        // fctsEtudeSommeNbs et fctsEtudeProduitNbs
        a = stor.objet_derivee.variables[0]
        b = stor.objet_derivee.variables[1]
        me.logIfDebug('a et b=', a, '  ', b)
        // -(a+b)/a au format latex
        zeroDerivee = fctsEtudeProduitNbs(fctsEtudeProduitNbs(-1, fctsEtudeSommeNbs(a, b)), fctsEtudeDivisionNbs(1, a))
        me.logIfDebug('zeroDerivee=', zeroDerivee)
        me.logIfDebug('zeroDerivee=', fctsEtudeEcriturePourCalcul(zeroDerivee))
        // pour les autres modèles, ça peut être plus compliqué, si valeur interdite, il faut tester la présence du zero dans plusieurs intervalles.
        // A voir si c’est à coder en fonction du modèle ou si je peux tout faire d’un coup
        stor.zero_derivee_dans_domaine = fctsEtudeEstDansDomaine(fctsEtudeEcriturePourCalcul(zeroDerivee), stor.defFonction.domaineDef)
        stor.zero_derivee.push(zeroDerivee)
        //                        var fct_pour_calc = fctsEtudeEcriturePourCalcul(stor.objet_derivee.expres_derivee);
        //                        console.log("LA fct_pour_calc=",fct_pour_calc)
        //                        var arbre_fct = new Tarbre(fct_pour_calc,["x"]);
        //                        var b = arbre_fct.evalue([0]);
        if (stor.zero_derivee_dans_domaine) {
          stor.ligne_des_x = [fctsEtudeEcritLimite(stor.defFonction.domaineDef[0]), zeroDerivee, fctsEtudeEcritLimite(stor.defFonction.domaineDef[1])]
        } else {
          stor.ligne_des_x = [fctsEtudeEcritLimite(stor.defFonction.domaineDef[0]), fctsEtudeEcritLimite(stor.defFonction.domaineDef[1])]
        }
        me.logIfDebug('stor.ligne_des_x=', stor.ligne_des_x)
        break
    }
  }

  function estUneLimite (valeur) {
    for (let i = 0; i < stor.defFonction.limites.length; i++) {
      const limite = fctsEtudeEcritLimite(stor.defFonction.limites[i][0])
      me.logIfDebug('ICI', limite)
      if (limite === valeur || limite === valeur + '^-' || limite === valeur + '^+') {
        return true
      }
    }
    return false
  }

  function calculImages () {
    stor.images_variations = []
    stor.images_racines_derivee = []
    me.logIfDebug('determine les images et zeros derivee=', stor.zero_derivee)
    for (let i = 0; i < stor.ligne_des_x.length; i++) {
      let leZero = stor.ligne_des_x[i]
      me.logIfDebug('mon zero=', leZero)
      if (!estUneLimite(leZero)) { // les bornes de l’intervalles n’ont pas à avoir leur image dans images_variations c géré plus tard (le tableau final étant table_images)
        if (String(leZero).includes('\\mathrm{e}')) {
          if (String(leZero).split('\\mathrm{e}')[1][0] !== '^') {
            // c’est qu’on a juste \\mathrm{e}, il faut donc que j'écrive \\mathrm{e}^{1} pour que ça passe
            leZero = leZero.replace('\\mathrm{e}', '\\mathrm{e}^{1}')
          }
        }
        const image = fctsEtudeDetermineImage(stor.objet_derivee, stor.defFonction.modele, leZero)
        me.logIfDebug('image=', image)
        stor.images_variations.push(image)
        if (fctsEtudeEstDansDomaine(fctsEtudeEcriturePourCalcul(leZero), stor.defFonction.domaineDef)) {
          // j’ai un zéro dans le domaine
          stor.images_racines_derivee.push(image)
        }
      }
    }
  }

  function enleveEspace (str) {
    return str.replace(/ /g, '')
  }

  function validationZone (a, b) {
    me.logIfDebug('validationZone avec a=', a, 'de valeur', fctsEtudeCalculValeur(a), 'et b=', b, ' de valeur', fctsEtudeCalculValeur(b))
    if (a.includes('infty') || b.includes('infty')) return (a === enleveEspace(b))
    return (Math.abs(fctsEtudeCalculValeur(a) - fctsEtudeCalculValeur(b)) < Math.pow(10, -12))
  }

  function validation () {
    let i, aReponduSaisies, aReponduVariations, imagesCorrectes, variationsCorrectes
    const objet = {}
    objet.bonneReponse = true
    objet.tab_imagesCorrectes = []
    variationsCorrectes = true
    imagesCorrectes = true
    aReponduVariations = false
    for (i = 0; i < stor.signes_derivee.length; i++) {
      if ((stor.signes_derivee[i] === '+' && (stor.objRep.lignes[1].variations[i] === 'decroit' || stor.objRep.lignes[1].variations[i] === 'const')) || (stor.signes_derivee[i] === '-' && (stor.objRep.lignes[1].variations[i] === 'croit' || stor.objRep.lignes[1].variations[i] === 'const'))) {
        variationsCorrectes = false
        objet.bonneReponse = false
      }
      if (stor.objRep.lignes[1].variations[i] !== 'const') {
        aReponduVariations = true
      }
    }
    aReponduSaisies = true
    for (i = 0; i < stor.objRep.lignes[1].zonesdesaisie.length; i++) {
      const nomZone = stor.objRep.lignes[1].zonesdesaisie[i]
      if (j3pValeurde(nomZone) === '') {
        aReponduSaisies = false
      }
    }
    objet.aRepondu = aReponduSaisies && aReponduVariations

    objet.aReponduSaisies = aReponduSaisies
    objet.aReponduVariations = aReponduVariations
    // validation des variations :
    if (objet.aReponduSaisies && objet.aReponduVariations) {
      objet.variationsCorrectes = variationsCorrectes
      // validation des zones de saisies : on trouve dans stor.table_images la liste des images à mettre (avec ou sans les limites) et dans stor.objRep.lignes[1].zonesdesaisie les id des zones de saisie, forcément même nb et normalement indépendant du modèle
      for (i = 0; i < stor.table_images.length; i++) {
        if (!validationZone(stor.table_images[i], j3pValeurde(stor.objRep.lignes[1].zonesdesaisie[i]))) {
          imagesCorrectes = false
          objet.bonneReponse = false
          objet.tab_imagesCorrectes[i] = false// pour mettre en rouge lors de la correction
        } else {
          objet.tab_imagesCorrectes[i] = true
        }
      }
    }
    objet.imagesCorrectes = imagesCorrectes
    return objet
  }

  function affichePrecisions (rep) {
    let txt = '<br>'
    if (!rep.imagesCorrectes) {
      if (!rep.variationsCorrectes) {
        // tout faux...
        txt += textes.precisions
      } else {
        // variations ok
        txt += textes.precisions_variations_ok_saisies_fausses + '<br>'
      }
    }
    if (!rep.variationsCorrectes && rep.variationsCorrectes) {
      txt += textes.precisions_saisies_ok_variations_fausses
    }

    j3pAffiche(stor.zoneCorr, '', txt)
  }

  // tableau de correction
  function afficheCorrectionTableau () {
    me.logIfDebug(
      'OBJET REPONSE', stor.objRep,
      '\nstor.objRep.lignes[1].imagesreponses[0]' + stor.objRep.lignes[1].imagesreponses[0],
      '\nstor.objRep.lignes[1].imagesreponses[1]' + stor.objRep.lignes[1].imagesreponses[1],
      '\nstor.objRep.lignes[1].imagesreponses[2]' + stor.objRep.lignes[1].imagesreponses[2],
      '\npremier sens' + stor.objRep.lignes[1].variations[0],
      '\nsecond sens' + stor.objRep.lignes[1].variations[1]
    )
    // var objet2=tableau_signes_variations2("modele_conteneur","tableau1",stor.objRep,correction);
    stor.tabBilan = {}// on duplique le tableau pour créer celui de la correction, on va se contenter de corriger la ligne des variations.
    stor.tabBilan = JSON.parse(JSON.stringify(stor.objRep))// une astuce pour créer réellement une nouvel objet et non une instance de stor.objRep qui serait modifié sinon...
    stor.tabBilan.mef.macouleur = me.styles.toutpetit.correction.color// On va plutôt choisir la couleur de la correction pour harmoniser (Rémi) 'rgb(255,0,0)'// un rouge qui pète
    for (let i = 0; i < stor.table_images_bis.length; i++) {
      me.logIfDebug('stor.table_images[', i, ']=', stor.table_images[i])
      stor.tabBilan.lignes[1].imagesreponses[i] = stor.table_images_bis[i]
    }
    //       stor.tabBilan.lignes[1].imagesreponses[0]=stor.defFonction.limites[0][1];
    //       stor.tabBilan.lignes[1].imagesreponses[1]=stor.images_variations[0];
    //       stor.tabBilan.lignes[1].imagesreponses[2]=stor.defFonction.limites[1][1];
    stor.tabBilan.lignes[1].variations = []
    for (let i = 0; i < stor.signes_derivee.length; i++) {
      if (stor.signes_derivee[i] === '+') {
        stor.tabBilan.lignes[1].variations.push('croit')
      } else {
        stor.tabBilan.lignes[1].variations.push('decroit')
      }
    }

    me.logIfDebug('le second tableau=', stor.tabBilan)

    const correction = true
    const tabVarCorr = j3pAddElt(stor.divExplications, 'div')
    tableauSignesVariations(tabVarCorr, 'tableau2', stor.tabBilan, correction)
    // j3pToggleFenetres("Aide");
  }

  function genereTextesCorrection () {
    let txt, next, i, leZero
    switch (stor.defFonction.modele) {
      case 2:// A MODIFIER, pour tests
        stor.tab_textes_cor = [textes.corr_1_1, textes.corr_1_2, textes.corr_1_3]
        stor.variables_cor = [[], [], []]// pas de variables dans les 3 premiers textes
        txt = '$f(£a)=£b$<br>'
        next = 0
        for (i = 0; i < stor.ligne_des_x.length; i++) {
          leZero = stor.ligne_des_x[i]
          me.logIfDebug('mon zero=', leZero)
          if (!estUneLimite(leZero)) {
            stor.tab_textes_cor.push(txt)
            stor.variables_cor.push([leZero, stor.images_variations[next]])
            next++
          }
        }
        me.logIfDebug('stor.tab_textes_cor=', stor.tab_textes_cor)

        break

      default : // modèle 1 normalement pour tous les modèles dans cette section...
        stor.variables_cor = [[], [], []]// pas de variables dans les 3 premiers textes
        txt = '$f(£a)=£b$<br>'
        next = 0
        {
          const tabAjoutTxtCorr = []
          const tabAjoutVarCorr = []

          for (i = 0; i < stor.ligne_des_x.length; i++) {
            leZero = stor.ligne_des_x[i]
            me.logIfDebug('mon zero=', leZero)
            if (!estUneLimite(leZero)) {
              tabAjoutTxtCorr.push(txt)
              tabAjoutVarCorr.push([leZero, stor.images_variations[next]])
              next++
            }
          }
          if (next > 0) {
            stor.tab_textes_cor = [textes.corr_1_1, textes.corr_1_2, textes.corr_1_3]
            stor.variables_cor = [[], [], []]// pas de variables dans les 3 premiers textes
          } else {
            stor.tab_textes_cor = [textes.corr_1_1, textes.corr_1_2]
            stor.variables_cor = [[], []]// pas de variables dans les 2 premiers textes
          }
          stor.tab_textes_cor = stor.tab_textes_cor.concat(tabAjoutTxtCorr)
          stor.variables_cor = stor.variables_cor.concat(tabAjoutVarCorr)
          me.logIfDebug('stor.tab_textes_cor=', stor.tab_textes_cor)
        }
        break
    }
  }

  const textes = {
    titre: 'Variations d’une fonction',
    phrase1: 'On considère la fonction définie et dérivable sur $£a$ par : $f(x)=£b$.',
    phrase1_bis: 'On considère la fonction définie sur $£a$, dérivable sur $£c$ par : $f(x)=£b$.',
    phrase1_a: 'Ayant déterminé le signe de $f\\quad\'(x)$, déduis-en les variations de $f$ sur son ensemble de définition : (clique sur les flèches du tableau pour modifier leur direction). ',
    phrase1_bis_a: 'On considère la fonction définie sur $£a$, dérivable sur $£c$ par : $f(x)=£b$.',
    phrase2: 'On donne le tableau de signes de $f\\quad\'$, déduis-en les variations de $f$ (clique sur les flèches du tableau pour modifier leur direction).',
    phrase2_a: 'On rappelle que $f(x)=£a$ et $f\\quad\'(x)=£b$.',
    phrase3: 'On donne également les limites suivantes : ',
    phrase3_a: 'On rappelle également les limites suivantes : ',
    phrase4: 'On donne également la limite suivante : ',
    phrase4_a: 'On rappelle également la limite suivante : ',
    corr_1_1: 'Lorsque la dérivée d’une fonction est positive sur un intervalle, la fonction est croissante.<br>', // correction du modèle 1
    corr_1_2: 'Lorsque la dérivée d’une fonction est négative sur un intervalle, la fonction est décroissante.<br>',
    corr_1_3: ' On a de plus :<br>',
    variations_manquantes: 'Il faut donner les variations de la fonction!',
    saisies_manquantes: 'Il faut compléter les zones de saisie!',
    precisions: 'Les variations sont incorrectes ainsi que les zones de saisies',
    precisions_variations_ok_saisies_fausses: 'Les variations sont exactes mais pas les zones de saisies.',
    precisions_saisies_ok_variations_fausses: 'Les zones de saisies sont exactes mais pas les variations.',
    correction_tableau: 'Correction du tableau'
  }

  // console.log("debut du code : ",me.etat)

  // un raccourci vers notre fct principale,
  // car on lui ajoute plein de propriétés, on commence par deux fcts
  function suite () {
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    initSection(me, textes.titre)
    suite2()
  }

  function suite2 () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    initModele(me)

    const domaineDef = fctsEtudeEcritureDomaine(stor.defFonction.domaineDef)
    const domaineDeriv = fctsEtudeEcritureDomaine(stor.defFonction.domaineDeriv)
    const laFonction = fctsEtudeEcritureLatexFonction(stor.defFonction.fonction, stor.defFonction.modele)
    stor.objet_derivee = fctsEtudeEcritureLatexDerivee(laFonction, stor.defFonction.modele, stor.defFonction.domaineDeriv, ds.debug)
    me.logIfDebug('me.SectionEtudeFonction_signederivee.objet_derivee=', stor.objet_derivee)

    determineLigneDesX()
    calculImages()
    let zeroTemp = stor.zero_derivee
    me.logIfDebug('ZERO_TEMP=', zeroTemp)
    if (zeroTemp[0] === undefined) {
      zeroTemp = []
    }
    stor.signes_derivee = fctsEtudeSigneFct(stor.objet_derivee.expres_derivee, stor.defFonction.domaineDeriv, zeroTemp, ds.debug)
    // console.log("signes derivee", stor.signes_derivee)

    // on récupère le nb de limites (fonction qui renseigne tab_limites en fonction du domaine) pour savoir quelles limites écrire en consigne
    stor.tab_limites = fctsEtudeDetermineTabLimites(stor.defFonction)

    const dansEtudeDeFonction = false
    if (dansEtudeDeFonction) {
      j3pAffiche(stor.zoneCons1, '', textes.phrase1_a)
      j3pAffiche(stor.zoneCons1, '', textes.phrase2_a, { a: laFonction, b: stor.objet_derivee.expres_derivee })
    } else {
      if (domaineDef === domaineDeriv) {
        j3pAffiche(stor.zoneCons1, '', textes.phrase1,
          {
            a: domaineDef,
            b: laFonction
          })
      } else {
        j3pAffiche(stor.zoneCons1, '', textes.phrase1_bis,
          {
            a: domaineDef,
            b: laFonction,
            c: domaineDeriv
          })
      }
      j3pAffiche(stor.zoneCons2, '', textes.phrase2)
    }
    let enonce = ''
    let valeur
    if (ds.limites_a_trouver) {
      if (dansEtudeDeFonction) {
        if (stor.tab_limites.length === 1) {
          enonce = textes.phrase4_a
        } else {
          enonce = textes.phrase3_a
        }
      } else {
        if (stor.tab_limites.length === 1) {
          enonce = textes.phrase4
        } else {
          enonce = textes.phrase3
        }
      }
      // cas particulier :
      if (stor.tab_limites.length === 0) {
        enonce = ''
      }
      j3pAffiche(stor.zoneCons2, '', '\n' + enonce)

      me.logIfDebug('stor.defFonction.limites=', stor.defFonction.limites)
      me.logIfDebug('stor.tab_limites=', stor.tab_limites)
      for (let j = 0; j < stor.defFonction.limites.length; j++) {
        const lim = ecritBienBorne(fctsEtudeEcritLimite(stor.defFonction.limites[j][0]))
        valeur = stor.defFonction.limites[j][1]
        if (stor.tab_limites.map(el => ecritBienBorne(el)).includes(lim)) {
          j3pAffiche(stor.zoneCons2, '', '\n$\\limite{x}{£a}{f(x)}=£b$',
            {
              a: lim,
              b: valeur
            })
        }
      }
    }
    // cf sectiontabvariations
    // QQ commentaires (en partie pour Rémi... et pour mémoire):
    // dans la fonction de j3pOutils, on fait la différence entre une valeur charnière (changement - ou debut/fin - de signe/variations) et image, ce dernier pouvant être une valeur charnière ou non
    // le ou non permettant d’ajouter des calculs de n’importe quelle valeur, et à terme, de faire varier un point à la fois sur la courbe et sur le tableau
    stor.objRep = {}
    const objetGlobal = stor.objRep
    // Attention cet Id et ceux qui en dépendent sont utilisés pour faire du querySelector dans le test browser etudeFonctionVariation.js ! Ne plus modifier ceci.
    stor.leNomDuDiv = j3pGetNewId('tableau1')
    const divParent = stor.zoneCons4
    // Tout ce qui concerne la mise en forme :
    // var couleur1="rgb(40,120,120)";
    const couleur1 = me.styles.grand.enonce.color
    objetGlobal.mef = {}
    // la largeur
    objetGlobal.mef.L = 620
    // la hauteur de la ligne des x
    objetGlobal.mef.h = 50
    // la couleur
    objetGlobal.mef.macouleur = couleur1
    objetGlobal.lignes = []
    // la première ligne, un objet donc
    objetGlobal.lignes[0] = {}
    // ligne de signes ou de variations ?
    objetGlobal.lignes[0].type = 'signes'
    // quelle hauteur ?
    objetGlobal.lignes[0].hauteur = 50
    // ce qu’on mettra dans la palette de boutons Mathquill :
    objetGlobal.liste_boutons = []
    objetGlobal.liste_boutons = ['racine', 'fraction', 'inf', 'exp', 'puissance', 'ln']
    objetGlobal.lignes[0].valeurs_charnieres = {}
    objetGlobal.lignes[0].valeurs_charnieres.valeurs_theoriques = []
    // ce qui s’affichera en haut, peut être une expression mathquill
    objetGlobal.lignes[0].valeurs_charnieres.valeurs_affichees = []
    // j’ai donc besoin des valeurs approchées pour les ordonner facilement...
    objetGlobal.lignes[0].valeurs_charnieres.valeurs_approchees = []
    // les signes
    objetGlobal.lignes[0].signes = []
    objetGlobal.lignes[0].imagesapprochees = []
    // on peut ajouter des valeurs de x avec leurs images.
    objetGlobal.lignes[0].images = []
    // la seconde ligne :
    objetGlobal.lignes[1] = {}
    // ligne de signes ou de variations ?
    objetGlobal.lignes[1].type = 'variations'
    // sa hauteur :
    objetGlobal.lignes[1].hauteur = 80
    // Variations de ...
    objetGlobal.lignes[1].valeurs_charnieres = {}
    objetGlobal.lignes[1].valeurs_charnieres.valeurs_theoriques = []
    // ce qui s’affichera en haut, peut être une expression mathquill
    objetGlobal.lignes[1].valeurs_charnieres.valeurs_affichees = []
    // j’ai donc besoin des valeurs approchées pour les ordonner facilement... (pour l’exemple ce sont les mêmes car valeurs entières)
    objetGlobal.lignes[1].valeurs_charnieres.valeurs_approchees = []
    // Variations : (on peut mettre const)
    objetGlobal.lignes[1].variations = []
    // on peut ajouter des valeurs de x avec leurs images.
    objetGlobal.lignes[1].images = []
    objetGlobal.lignes[1].imagesapprochees = []
    objetGlobal.lignes[1].intervalles = []
    objetGlobal.lignes[0].signes = []
    objetGlobal.lignes[0].expression = 'f\\quad\''
    // la seconde ligne du tableau :
    objetGlobal.lignes[1].expression = 'f '
    // récupération des infos :
    const tableTemp = []// me sert pour définir les intervalles;
    stor.table_images = []// me sert en correction pour stocker les bonnes réponses et comparer avec la rep élève
    stor.table_images_bis = []// me sert en correction dans le cas où il n’y a pas de limite à compléter : pour avoir les bons index on ne complète pas les index des limites (on se sert tjs de table_images pour la validation par contre)
    let indexImages = 0
    let valeurPourOrdonner = 0
    me.logIfDebug('stor.images_variations=', stor.images_variations)
    if (!ds.limites_a_trouver && (stor.images_variations.length === 0)) {
      ds.nbchances = 1
    }
    for (let k = 0; k < stor.ligne_des_x.length; k++) {
      valeur = stor.ligne_des_x[k]
      me.logIfDebug('!!!!!valeur!!!!=', valeur)
      objetGlobal.lignes[0].valeurs_charnieres.valeurs_theoriques.push(valeur)
      objetGlobal.lignes[0].valeurs_charnieres.valeurs_affichees.push(valeur)
      // gestion particulière pour les intervalles :
      // objetGlobal.lignes[1].intervalles sera fait dans la boucle for suivante (en même temps que les +/- des signes
      if (valeur === '-\\infty' || valeur === '+\\infty') { // là il faudra voir si c’est compatible avec tous les modèles, pour les homographiques on aura surement des soucis avec les doubles limites (mais c’est prévu dans la fonction les limites à gauche et à droite)
        objetGlobal.lignes[0].valeurs_charnieres.valeurs_approchees.push(valeur)
        tableTemp.push(valeur)// mis au chaud
        if (ds.limites_a_trouver === true) {
          objetGlobal.lignes[1].images.push([valeur, '?'])
          // je recherche le bon index dans stor.defFonction.limites
          let indexLimites = 0
          for (let indexJ = 0; indexJ < stor.defFonction.limites.length; indexJ++) {
            if (fctsEtudeEcritLimite(stor.defFonction.limites[indexJ][0]) === valeur) {
              indexLimites = indexJ
            }
          }
          stor.table_images.push(stor.defFonction.limites[indexLimites][1])
          stor.table_images_bis.push(stor.defFonction.limites[indexLimites][1])
        } else {
          objetGlobal.lignes[1].images.push([valeur, ''])
          stor.table_images_bis.push('')
        }
      } else {
        me.logIfDebug('VALEUR:', valeur, ' et indexImages=', indexImages)
        objetGlobal.lignes[0].valeurs_charnieres.valeurs_approchees.push(valeurPourOrdonner)// au pif pour ordonner
        // il faut faire un test pour savoir si valeur interdite ou non
        if (stor.defFonction.val_interdites.map(el => ecritBienFraction(el)).includes(ecritBienFraction(valeur))) {
          stor.table_images_bis.push([])// cas particulier ici pour table_images_bis utilisé en correction, on met les limites à gauche et à droite dans un element du tableau
          me.logIfDebug(valeur, ' est interdite !! et indexImages=', indexImages)
          objetGlobal.lignes[0].images.push([valeur, '||'])
          objetGlobal.lignes[0].imagesapprochees.push(valeurPourOrdonner)
          // on demande l’image par f, à affiner si juste une limite à droite ou a gauche...
          const tab = estLimiteDroiteGauche(valeur)// donne un tableau de deux booléens
          const estLimiteGauche = tab[0]
          const estLimiteDroite = tab[1]
          let indexLimiteGauche = 0
          if (estLimiteGauche && ds.limites_a_trouver) {
            for (let indexJ = 0; indexJ < stor.defFonction.limites.length; indexJ++) {
              if (fctsEtudeEcritLimite(ecritBienBorne(stor.defFonction.limites[indexJ][0])) === ecritBienFraction(valeur) + '^-') {
                indexLimiteGauche = indexJ
                me.logIfDebug('On a une limite à gauche de ', valeur)
                stor.table_images.push(ecritBienFraction(stor.defFonction.limites[indexLimiteGauche][1]))
                // indexImages++;
              }
            }
          }
          let indexLimiteDroite = 0
          if (estLimiteDroite && ds.limites_a_trouver) {
            for (let indexJ = 0; indexJ < stor.defFonction.limites.length; indexJ++) {
              if (fctsEtudeEcritLimite(ecritBienBorne(stor.defFonction.limites[indexJ][0])) === ecritBienFraction(valeur) + '^+') {
                indexLimiteDroite = indexJ
                me.logIfDebug('On a une limite à droite de ', valeur)
                stor.table_images.push(ecritBienFraction(stor.defFonction.limites[indexLimiteDroite][1]))
              }
            }
          }
          const indexTable = stor.table_images_bis.length - 1
          let chaine = ''
          if (estLimiteGauche && ds.limites_a_trouver) {
            chaine = chaine + '?'
            stor.table_images_bis[indexTable].push(stor.defFonction.limites[indexLimiteGauche][1])
          }
          chaine = chaine + '||'
          if (estLimiteDroite && ds.limites_a_trouver) {
            chaine = chaine + '?'
            stor.table_images_bis[indexTable].push(stor.defFonction.limites[indexLimiteDroite][1])
          }
          objetGlobal.lignes[1].images.push([valeur, chaine])
          // stor.table_images_bis.push(stor.defFonction.limites[indexImages][1]);
          // indexImages++;
        } else {
          // on n’a pas forcément une valeur qui annule la dérivée, si c une borne de l’intervalle, non par exemple
          if (stor.zero_derivee.includes(valeur)) {
            objetGlobal.lignes[0].images.push([valeur, '0'])
            objetGlobal.lignes[0].imagesapprochees.push(valeurPourOrdonner)
          }
          objetGlobal.lignes[1].images.push([valeur, '?'])// on demande l’image par f
          me.logIfDebug('stor.images_variations[indexImages]=', stor.images_variations[indexImages])
          stor.table_images.push(stor.images_variations[indexImages])
          stor.table_images_bis.push(stor.images_variations[indexImages])
          indexImages++
        }

        tableTemp.push(valeurPourOrdonner)// mis au chaud
        valeurPourOrdonner++
      }
    }
    me.logIfDebug('=======stor.table_images==========', stor.table_images)
    me.logIfDebug('=======stor.table_images_bis==========', stor.table_images_bis)
    // pour mémoire et eventuel débugage :
    //            objetGlobal.lignes[0].valeurs_charnieres.valeurs_theoriques=["-inf","\\frac{-11}{2}","+inf"];
    //            objetGlobal.lignes[0].valeurs_charnieres.valeurs_affichees=["?","\\frac{-11}{2}","?"];
    //            objetGlobal.lignes[0].valeurs_charnieres.valeurs_approchees=["-inf",0,"+inf"];
    //            objetGlobal.lignes[0].images=[[stor.ligne_des_x[1],"0"]];
    //            objetGlobal.lignes[0].imagesapprochees=[0];
    //            objetGlobal.lignes[1].intervalles=[["-inf",0],[0,"+inf"]];
    //            objetGlobal.lignes[1].images=[["-inf","?"],[stor.ligne_des_x[1],"?"],["+inf","?"]];

    objetGlobal.lignes[1].imagesapprochees = objetGlobal.lignes[0].valeurs_charnieres.valeurs_approchees
    // mêmes valeurs charnières...
    objetGlobal.lignes[1].valeurs_charnieres.valeurs_theoriques = objetGlobal.lignes[0].valeurs_charnieres.valeurs_theoriques
    objetGlobal.lignes[1].valeurs_charnieres.valeurs_affichees = objetGlobal.lignes[0].valeurs_charnieres.valeurs_affichees
    objetGlobal.lignes[1].valeurs_charnieres.valeurs_approchees = objetGlobal.lignes[0].valeurs_charnieres.valeurs_approchees

    objetGlobal.lignes[1].variations = []
    objetGlobal.lignes[1].cliquable = []
    for (let i = 0; i < stor.signes_derivee.length; i++) {
      if (stor.signes_derivee[i] === '+') {
        objetGlobal.lignes[0].signes.push('+')
      } else {
        objetGlobal.lignes[0].signes.push('-')
      }
      objetGlobal.lignes[1].variations.push('const')
      objetGlobal.lignes[1].cliquable.push(true)
      objetGlobal.lignes[1].intervalles.push([tableTemp[i], tableTemp[i + 1]])
    }
    objetGlobal.ajout_lignes_possible = false
    objetGlobal.ajout_intervalles_possible = false

    tableauSignesVariations(divParent, stor.leNomDuDiv, objetGlobal, false)

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', {
      style: me.styles.etendre('toutpetit.correction', {
        paddingLeft: '15px',
        paddingTop: '100px'
      })
    })
    stor.divExplications = j3pAddElt(stor.conteneur, 'div')
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.logIfDebug('ds.imposer_fct : ', ds.imposer_fct)
        me.construitStructurePage('presentation1')
        suite()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite2()
      }

      break // case "enonce":

    case 'correction': {
      const reponse = validation()
      genereTextesCorrection()// se base sur le modèle pour que ce soit simple d’en rajouter
      let correction
      me.logIfDebug('reponse=', reponse)
      if ((reponse.aRepondu === false) && (!me.isElapsed)) {
        stor.zoneCorr.style.color = me.styles.cfaux
        if (!reponse.aReponduSaisies) {
          stor.zoneCorr.innerHTML = textes.saisies_manquantes
        }
        if (!reponse.aReponduVariations) {
          stor.zoneCorr.innerHTML = textes.variations_manquantes
        }
      } else {
        // Une réponse a été saisie
        if (reponse.bonneReponse && (!me.isElapsed)) {
          // Bonne réponse
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          // on gèle le tableau de l’élève
          correction = true
          // je mets quand même le tableau en vert
          stor.objRep.mef.macouleur = me.styles.cbien
          tableauSignesVariations(stor.zoneCons4, stor.leNomDuDiv, stor.objRep, correction)
          // pour la section bilan, on récupère dans tous les cas le tableau dans stor.tabBilan
          stor.tabBilan = JSON.parse(JSON.stringify(stor.objRep))
          fctsEtudeAfficheCorrection(stor.divExplications, me.styles.cbien, stor.tab_textes_cor, stor.variables_cor)
          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.objRep.mef.macouleur = me.styles.cfaux
          stor.zoneCorr.style.color = me.styles.cfaux
          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            // on gèle le tableau de l’élève
            correction = true
            tableauSignesVariations(stor.zoneCons4, stor.leNomDuDiv, stor.objRep, correction)
            fctsEtudeAfficheCorrection(stor.divExplications, me.styles.toutpetit.correction.color, stor.tab_textes_cor, stor.variables_cor)
            afficheCorrectionTableau()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              affichePrecisions(reponse)
              me.typederreurs[1]++
              // indication éventuelle ici
            } else {
              // Erreur au nème essai
              // on gèle le tableau de l’élève
              correction = true
              tableauSignesVariations(stor.zoneCons4, stor.leNomDuDiv, stor.objRep, correction)
              fctsEtudeAfficheCorrection(stor.divExplications, me.styles.toutpetit.correction.color, stor.tab_textes_cor, stor.variables_cor)
              afficheCorrectionTableau()
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
        // pour la section bilan :
        stor.tableau_bilan = stor.tabBilan
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      me.finNavigation()
      break
  }
}
