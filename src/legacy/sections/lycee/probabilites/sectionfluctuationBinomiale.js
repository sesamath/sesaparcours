import { j3pAddContent, j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pRandomTab, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { testIntervalleDecimaux, testIntervalleEntiers, testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        février 2020
        détermination de l’intervalle de fluctuation dans le cadre d’une loi binomiale
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCons4)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3, 4].forEach(i => {
      stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
      j3pAffiche(stor['zoneExpli' + i], '', ds.textes['corr' + i], stor.objCons)
    })
  }

  function combinaison (n, p) {
    // Cette fonction calcule C_n^p sans utiliser les factorielles pour gagner un peu de temps
    if (n === p || p === 0) return 1
    let res = n
    let i = n - 1
    while (i > Math.max(p, n - p)) {
      res *= i
      i -= 1
    }
    i = 2
    while (i <= Math.min(p, n - p)) {
      res = Math.round(res / i)
      i += 1
    }
    // console.log("combinaison(",n,",",p,")=",res)
    return res
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Loi binomiale : Intervalle de fluctuation',
        // on donne les phrases de la consigne
        consigne1: 'Soit $£x$ une variable aléatoire suivant la loi binomiale $B(£n;£p)$.',
        consigne2: 'Un intervalle de fluctuation centré associé à $£x$ au seuil de £{pour}% est&nbsp;:',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Ce qui est écrit n’est pas un intervalle ou n’a pas de cohérence avec ce qui est demandé&nbsp;!',
        comment2: 'Les bornes de l’intervalle doivent être des nombres entiers&nbsp;!',
        comment3: 'Le séparateur des bornes d’un intervalle est le point-virgule&nbsp;!',
        comment4: 'L’intervalle attendu doit être fermé&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'Un tel intervalle est de la forme $[a;b]$ où $a$ et $b$ sont les plus petits entiers tels que $P(£x\\leq a)>£{p1}$ et $P(£x\\leq b)\\geq £{p2}$. ',
        corr2: 'Or d’après la calculatrice $P(£x\\leq calcule[£a-1])\\approx £{prb1}$ et $P(£x\\leq £a)\\approx £{prb2}$, d’où $a=£a$.',
        corr3: 'De plus $P(£x\\leq calcule[£b-1])\\approx £{prb3}$ et $P(£x\\leq £b)\\approx £{prb4}$, d’où $b=£b$.',
        corr4: 'Donc l’intervalle cherché est $[£a;£b]$.'
      },
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    // Construction de la page

    me.construitStructurePage({ structure: ds.structure })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    // code de création des fenêtres

    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    stor.objCons = {}
    stor.objCons.n = j3pGetRandomInt(0, 20) + 20
    stor.objCons.p = (20 + j3pGetRandomInt(0, 12) * 5) / 100
    stor.objCons.x = j3pRandomTab(['X', 'Y', 'Z'], [0.33, 0.33, 0.34])
    stor.objCons.pour = (j3pGetRandomInt(0, 2) === 0) ? 1 : 5
    stor.objCons.tabProba = []// tableau de toutes les proba de la forme P(X<=k) avec k dans [[0;n]]
    stor.objCons.tabProba.push(Math.pow(1 - stor.objCons.p, stor.objCons.n))
    for (let i = 1; i <= stor.objCons.n; i++) {
      stor.objCons.tabProba.push(stor.objCons.tabProba[i - 1] + combinaison(stor.objCons.n, i) * Math.pow(stor.objCons.p, i) * Math.pow(1 - stor.objCons.p, stor.objCons.n - i))
    }
    stor.objCons.a = 0
    stor.objCons.b = 0
    let sommePrb = stor.objCons.tabProba[0]
    let pos = 0

    while (sommePrb <= stor.objCons.pour / 200) {
      pos += 1
      sommePrb = stor.objCons.tabProba[pos]
    }
    stor.objCons.p1 = Math.round(10000 * stor.objCons.pour / 200) / 10000
    stor.objCons.p2 = Math.round(10000 * (1 - stor.objCons.pour / 200)) / 10000
    stor.objCons.a = pos
    stor.objCons.prb1 = Math.round(stor.objCons.tabProba[stor.objCons.a - 1] * Math.pow(10, 4)) / Math.pow(10, 4)
    stor.objCons.prb2 = Math.round(stor.objCons.tabProba[stor.objCons.a] * Math.pow(10, 4)) / Math.pow(10, 4)
    while (sommePrb < 1 - stor.objCons.pour / 200) {
      pos += 1
      sommePrb = stor.objCons.tabProba[pos]
    }
    stor.objCons.b = pos
    stor.objCons.prb3 = Math.round(stor.objCons.tabProba[stor.objCons.b - 1] * Math.pow(10, 4)) / Math.pow(10, 4)
    stor.objCons.prb4 = Math.round(stor.objCons.tabProba[stor.objCons.b] * Math.pow(10, 4)) / Math.pow(10, 4)
    me.logIfDebug(
      'a', stor.objCons.a,
      'prb1', stor.objCons.prb1,
      'prb2', stor.objCons.prb2,
      '\nb', stor.objCons.b,
      'prb3', stor.objCons.prb3,
      'prb4', stor.objCons.prb4
    )
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 2; i++) {
      stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(stor['zoneCons' + i], '', ds.textes['consigne' + i], stor.objCons)
    }
    stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div')
    const elt = j3pAffiche(stor.zoneCons3, '', '&1&', { inputmq1: { texte: '' } })
    stor.zoneInput = elt.inputmqList[0]
    stor.zoneCons4 = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.zoneCons4, { paddingTop: '10px' })
    j3pPaletteMathquill(stor.zoneCons4, stor.zoneInput, {
      liste: ['[', ']', ';']
    })
    stor.zoneInput.typeReponse = ['texte']
    mqRestriction(stor.zoneInput, '\\d[],;')
    j3pFocus(stor.zoneInput)
    stor.zoneInput.reponse = ['[' + stor.objCons.a + ';' + stor.objCons.b + ']']
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
              // Réponse fausse
              const eltCorrection = stor.zoneCorr
              j3pEmpty(eltCorrection)
              const reponse = stor.fctsValid.zones.reponseSaisie[0]
              j3pAddContent(eltCorrection, cFaux)
              // On vérifie que l’élève a bien écrit un intervalle
              const estIntervalle = testIntervalleDecimaux.test(reponse)
              const bornesEntieres = testIntervalleEntiers.test(reponse)
              if (estIntervalle) {
                if (bornesEntieres) {
                  // on regarde si l’intervalle est fermé
                  if (!testIntervalleFermeEntiers.test(reponse)) j3pAddContent(eltCorrection, '\n' + ds.textes.comment4)
                } else {
                  j3pAddContent(eltCorrection, '\n' + ds.textes.comment2)
                }
              } else {
                // idem testIntervalleDecimaux avec séparateur virgule
                const regSeparateurVirgule = /^[[\]](?:-?[1-9][0-9]*|0)(?:[.,][0-9]+)?,(?:-?[1-9][0-9]*|0)(?:[.,][0-9]+)?[[\]]$/
                const pbvirg = regSeparateurVirgule.test(reponse)
                if (pbvirg) j3pAddContent(eltCorrection, '\n' + ds.textes.comment3)
                else j3pAddContent(eltCorrection, '\n' + ds.textes.comment1)
              }

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
