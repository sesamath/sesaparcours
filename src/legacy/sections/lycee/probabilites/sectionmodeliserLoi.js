import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import { j3pGetLatexQuotient } from 'src/legacy/core/functionsLatex'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2019
        Déterminer une loi de probabilité
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['irreductible', true, 'boolean', 'On peut imposer ou non d’avoir des fractions irréductibles']
  ]
}

export default function main () {
  const me = this
  const stor = this.storage
  let ds = me.donneesSection

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pDetruit(stor.zoneCons5)
    if (!bonneReponse) {
      const tabExpli = j3pAddElt(zoneExpli, 'div')
      tabExpli.setAttribute('width', stor.largeurTab + 'px')
      const table = j3pAddElt(tabExpli, 'table', { width: '100%', borderColor: stor.zoneCons4.style.color, border: 2, cellSpacing: 0 })
      const ligne = j3pAddElt(table, 'tr')
      j3pAddElt(ligne, 'td', ds.textes.tableau2, { width: stor.maLoi.largeurCol1 + 'px' })
      const tabId = []
      for (let i = 0; i < stor.maLoi.tabProba.length; i++) {
        tabId.push(j3pAddElt(ligne, 'td', { width: stor.maLoi.largeurAutreCol + 'px', align: 'center' }))
      }
      for (let i = 0; i < stor.maLoi.tabProba.length; i++) {
        j3pAffiche(tabId[i], '', '$' + stor.maLoi.tabProba[i] + '$')
      }
      $(stor.zoneExpli1).css('margin-bottom', '1em')
    }
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pAffiche(stor.zoneExpli1, '', ds.textes['corr1_' + stor.quest], { n: stor.maLoi.effTotal })
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2)
  }

  function ecoute (num) {
    const elt = stor.maLoi.zonesSaisie[stor.tabZones[num].index]
    if (elt && elt.classList.contains('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, elt, { liste: ['fraction'] })
    }
  }

  /**
   * @private
   * @param {Object} mesDonnees
   * @param {string} mesDonnees.parent nom du div dans lequel est créé le tableau
   * @param {string} mesDonnees.divId nom du div créé (celui du tableau)
   * @param {number} mesDonnees.largeur largeur du tableau (utile pour configurer la largeur de chaque colonne)
   * @param {string[]} mesDonnees.legendes tableau de 2 éléments : les légendes (issues et proba)
   * @param {string[]} mesDonnees.textes tableau contenant le nom des issues
   * @param {number[]} mesDonnees.effectifs Utile pour calculer les proba (cela se fera ici)
   * @param {boolean} mesDonnees.complet passer true quand les réponses sont données (sinon les proba sont remplacées par des zones de saisie mq)
   * @return {{zonesSaisie: *[], tabValProba: *[], tabProba: *[], effTotal, largeurAutreCol: number, largeurCol1: number}}
   */
  function tableauDonneesEffectifs (mesDonnees) {
    function creerLigneTab (elt, valeurs, l1, l2) {
      // elt est la table dans laquelle ajouter la ligne
      // l1 et l2 sont respectivement la largeur de la première colonne et des suivantes
      const ligne = j3pAddElt(elt, 'tr')
      j3pAddElt(ligne, 'td', valeurs[0], { width: l1 + 'px' })
      for (let i = 1; i < valeurs.length; i++) {
        j3pAddElt(ligne, 'td', valeurs[i], { align: 'center', width: l2 + 'px' })
      }
    }
    const { parent, largeur, legendes, textes, effectifs, complet } = mesDonnees
    parent.setAttribute('width', largeur + 'px')
    if (textes.length === effectifs.length) {
      let largeurTexte = 0
      for (let i = 0; i < legendes.length; i++) {
        largeurTexte = Math.max(largeurTexte, $(parent).textWidth(legendes[i], me.styles.petit.enonce.fontFamily, me.styles.petit.enonce.fontSize))
      }
      largeurTexte += 20
      const largeurColonne = (largeur - largeurTexte - 5) / textes.length
      const table = j3pAddElt(parent, 'table', { width: '100%', borderColor: 'black', border: 2, cellSpacing: 0 })
      j3pStyle(table, { marginBottom: '0.5em' })
      creerLigneTab(table, [legendes[0]].concat(textes), largeurTexte, largeurColonne)
      // JE détermine l’effectif total
      let effTotal = effectifs[0]
      for (let i = 1; i < effectifs.length; i++) effTotal += effectifs[i]
      const tabProba = []
      // tableau qui va récupérer les proba
      const tabValProba = []// LE même tableau mais avec les valeurs exactes de ces proba
      for (let i = 0; i < effectifs.length; i++) {
        tabProba.push(j3pGetLatexQuotient(effectifs[i], effTotal))
        tabValProba.push(effectifs[i] / effTotal)
      }
      me.logIfDebug('tabProba:', tabProba)
      const ligne = j3pAddElt(table, 'tr')
      j3pAddElt(ligne, 'td', legendes[1])
      const idPrbTxt = []
      for (let i = 0; i < textes.length; i++) {
        idPrbTxt.push(j3pAddElt(ligne, 'td', { align: 'center', width: largeurColonne + 'px' }))
      }
      // Maintenant, je remplis cette dernière ligne soit avec les proba, soit avec des zones inputmq (pour pouvoir mettre au format fractionnaire)
      stor.tabZones = []
      for (let i = 0; i < textes.length; i++) {
        if (complet) {
          j3pAffiche(idPrbTxt[i], '', '$' + tabProba[i] + '$')
        } else {
          const elt = j3pAffiche(idPrbTxt[i], '', '&1&')
          mqRestriction(elt.inputmqList[0], '\\d.,/', {
            commandes: ['fraction'],
            boundingContainer: me.zonesElts.MG
          })
          stor.tabZones.push(elt.inputmqList[0])
          elt.inputmqList[0].typeReponse = ['nombre', 'exact']
          elt.inputmqList[0].reponse = [tabValProba[i]]
          elt.inputmqList[0].solutionSimplifiee = ['reponse fausse', 'valide et on accepte']
          elt.inputmqList[0].index = i
          // il faut impérativement un bind ici, sinon au moment ou cette callback passée à focusin sera exécutée
          // on sera forcément sorti de la boucle et i vaudra toujours textes.length - 1
        }
      }

      stor.laPalette = j3pAddElt(parent, 'div')
      // palette MQ :
      j3pPaletteMathquill(stor.laPalette, stor.tabZones[0], { liste: ['fraction'] })

      j3pFocus(stor.tabZones[0])
      // on renvoie un objet contenant le nom des zones de saisie (lorsqu’elles existent) et le tableau des effectifs cumulés croissants
      return {
        zonesSaisie: stor.tabZones,
        tabProba,
        tabValProba,
        effTotal,
        largeurCol1: largeurTexte,
        largeurAutreCol: largeurColonne
      }
    }
    j3pShowError('pb sur le nombre de valeurs dans les deux tableaux', { vanishAfter: 5 })
  } // fin tableauDonneesEffectifs

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      irreductible: true,
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Modéliser une loi de probabilité',
        // on donne les phrases de la consigne
        consigne1_1: 'Un sac opaque contient £{n1} jetons noirs, £{n2} jetons rouges, £{n3} jetons verts et un seul jeton bleu.',
        consigne2_1: 'On choisit au hasard un jeton du sac.',
        consigne1_2: 'Dans le réfrigérateur, Antoine a le choix entre £{n1} yaourts à la fraise, £{n2} yaourts à l’abricot, un yaourt à la poire et £{n4} yaourts à la mure.',
        consigne2_2: 'Il ouvre le réfrigérateur et prend au hasard un yaourt au fruits.',
        consigne1_3: 'Quentin se rend à la médiathèque pour emprunter un livre. Parmi ceux qu’il n’a pas encore lu et qui l’intéressent, il y a £{n1} romans, une autobiographie, £{n3} bandes dessinées et £{n4} nouvelles.',
        consigne2_3: 'Il choisit l’une de ces œuvres au hasard.',
        consigne1_4: 'Rémi veut planter un nouvel arbuste dans son jardin. Un pépiniériste lui propose £{n1} thuyas, £{n2} photinias, un weigelia, £{n4} forsythias et £{n5} lauriers.',
        consigne2_4: 'Il en choisit un au hasard.',
        consigne1_5: 'Parmi les photos destinées à devenir le fond d’écran d’un ordinateur, une représente un paysage de campagne, £{n2} un paysage de montagne, £{n3} une ville et £{n4} un bord de mer.',
        consigne2_5: 'Comme fond d’écran, l’ordinateur choisit au hasard l’une de ces photos.',
        consigne3: 'Compléter ci-dessous la loi de probabilité associée à la situation :',
        tableau1: 'issue',
        tableau2: ' probabilité',
        textTableau1: 'noir|rouge|vert|bleu',
        textTableau2: 'fraise|abricot|poire|mure',
        textTableau3: 'roman|autobiographie|B.D.|nouvelle',
        textTableau4: 'thuya|photinia|weigelia|forsythia|laurier',
        textTableau5: 'campagne|montagne|ville|bord de mer',
        typeReponse: 'Les probabilités devront être écrites de manière simplifiée, sous la forme décimale ou d’une fraction.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Les résultats auraient pu être donnés sous la forme irréductible&nbsp;!',
        comment2: 'Les résultats doivent être donnés sous forme irréductible&nbsp;!',
        comment3: 'Toutes les réponses sont-elles correctes et simplifiées&nbsp;?',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'Le sac contient £n jetons.',
        corr1_2: 'Le réfrigérateur contient £n yaourts aux fruits.',
        corr1_3: 'Quentin est intéressé par £n livres de la médiathèque.',
        corr1_4: 'Le pépiniériste a proposé £n arbustes au total.',
        corr1_5: 'L’ordinateur dispose de £n photos en tout.',
        corr2: 'Le tableau ci-dessus contient les probabilités de chaque issue, c’est la loi de probabilité.'
      },

      pe: 0
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()

    // Construction de la page
    stor.pourc = 70
    me.construitStructurePage({ structure: me.donneesSection.structure, ratioGauche: stor.pourc / 100 })

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    // code de création des fenêtres
    // Que ce soit pour un cas concret ou un aure, je n’ai que 4 cas de figure
    stor.tabQuest = [1, 2, 3, 4, 5]
    stor.tabQuestInit = [...stor.tabQuest]
  }
  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (stor.tabQuest.length === 0) {
      stor.tabQuest = [...stor.tabQuestInit]
    }
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    stor.quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.valeursEffectifs = []
    const objDonnees = {}
    let choixInit = []
    let numUn
    let nb
    switch (stor.quest) {
      case 1:
        choixInit = [2, 3, 3, 4, 4, 5, 6]
        numUn = 4
        nb = 4
        break
      case 2:
        choixInit = [2, 2, 3, 3, 4, 5]
        numUn = 3
        nb = 4
        break
      case 3:
        choixInit = [3, 4, 5, 5, 6, 4]
        numUn = 2
        nb = 4
        break
      case 4:
        choixInit = [4, 4, 5, 5, 6, 6, 7]
        numUn = 3
        nb = 5
        break
      case 5:
        choixInit = [5, 5, 6, 7, 8, 9]
        numUn = 1
        nb = 4
        break
    }
    for (let i = 1; i <= nb; i++) {
      if (i !== numUn) {
        const alea = j3pGetRandomInt(0, (choixInit.length - 1))
        stor.valeursEffectifs.push(choixInit[alea])
        choixInit.splice(alea, 1)
      } else {
        stor.valeursEffectifs.push(1)
      }
    }
    for (let i = 1; i <= stor.valeursEffectifs.length; i++) {
      objDonnees['n' + i] = stor.valeursEffectifs[i - 1]
    }
    j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + stor.quest], objDonnees)
    j3pAffiche(stor.zoneCons2, '', ds.textes['consigne2_' + stor.quest])
    j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3)
    if (ds.irreductible) {
      j3pAffiche(stor.zoneCons5, '', ds.textes.typeReponse)
      j3pStyle(stor.zoneCons5, { fontSize: '0.9em', fontStyle: 'italic', paddingTop: '40px' })
    }
    me.logIfDebug('effectifs:', stor.valeursEffectifs)
    stor.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
    const mesDonnees = {
      parent: stor.zoneCons4,
      legendes: [ds.textes.tableau1, ds.textes.tableau2],
      textes: ds.textes['textTableau' + stor.quest].split('|'),
      effectifs: stor.valeursEffectifs,
      complet: false,
      largeur: stor.largeurTab
    }
    stor.maLoi = tableauDonneesEffectifs(mesDonnees)
    setTimeout(() => {
      /// //////////////////////////////////////
      /* FIN DU CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////////
      for (let i = 0; i < mesDonnees.textes.length; i++) $(stor.tabZones[i]).focusin(ecoute.bind(null, i))
      // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
      stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: stor.maLoi.zonesSaisie.map(elt => elt.id)
      })
      stor.firstTest = true
      j3pFocus(stor.maLoi.zonesSaisie[0])
      // Obligatoire
      me.finEnonce()
    }, 0)
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = { aRepondu: false, bonneReponse: true }
        reponse.aRepondu = fctsValid.valideReponses()
        let fracIrre = true
        if (reponse.aRepondu) {
          for (let i = 0; i < fctsValid.zones.inputs.length; i++) {
            if (!fctsValid.zones.bonneReponse[i]) {
              fctsValid.zones.bonneReponse[i] = fctsValid.valideUneZone(fctsValid.zones.inputs[i], fctsValid.zones.inputs[i].reponse).bonneReponse
              if (!fctsValid.zones.reponseSimplifiee[i][1]) {
                fracIrre = false
                if (ds.irreductible) fctsValid.zones.bonneReponse[i] = false
              }
              reponse.bonneReponse = reponse.bonneReponse && fctsValid.zones.bonneReponse[i]
            }
          }
          if (ds.irreductible && !fracIrre && stor.firstTest) reponse.aRepondu = false
          if (reponse.aRepondu) fctsValid.coloreLesZones()
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgRepManquante
          if (stor.firstTest && !fracIrre) msgRepManquante = ds.textes.comment2
          stor.firstTest = false
          me.reponseManquante(stor.zoneCorr, msgRepManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            if (!fracIrre) {
              stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
            }
            afficheCorr(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!fracIrre) stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment3
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
