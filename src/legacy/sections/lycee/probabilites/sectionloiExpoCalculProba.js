import { j3pArrondi, j3pNombre, j3pShuffle, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pRandomTab, j3pAddElt, j3pStyle, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { constructionTabIntervalle } from 'src/legacy/outils/fonctions/gestionParametres'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexOppose, j3pGetLatexProduit } from 'src/legacy/core/functionsLatex'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juillet 2017
        Une loi exponentielle étant définie, on demande la valeur d’une probabilité
        Cette section peut être dans la continuité de loiExpoTrouverLambda
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbetapes', 2, 'entier', 'Pour chaque répétition, c’est-à-dire chaque loi exponentielle, on propose 2 calculs de probabilités. On peut le modifier et n’en demander qu’une seule.'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['lambda', '[0.55;2.50]', 'string', 'Intervalle où se trouve le paramètre lambda de la loi exponentielle (avec ce type d’intervalle, il peut être décimal). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]" pour qu’il soit égal à 2 dans un premier temps, entier positif pour la deuxième répétition. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire. ATTENTION à ne pas prendre une valeur trop grand (non prévu si lambda est supérieur à 2.5).'],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la suite étudiée est celle du nœud précédent.']
  ],
  // pour fonctionnement dans editgraphes
  donnees_parcours_description: 'Cette section peut réutiliser le paramètre lambda d’une autre section.',
  donnees_parcours_nom_variable: 'lambda'
}

/**
 * section loiExpoCalculProba
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3, 4].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pDetruit(stor.zoneCons4)
    let ecritureProba
    switch (stor.tabCasFigure[(me.questionCourante - 1) % ds.nbetapes]) {
      case 'gauche':
        j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1_1, { v: stor.param.nomVar })
        ecritureProba = 'P(' + stor.param.nomVar + '&lt;' + stor.borneGauche + ')=1-\\mathrm{e}^{-' + stor.param.lambdaDecimal + '\\times ' + stor.borneGauche + '}=1-\\mathrm{e}^{' + j3pGetLatexOppose(j3pGetLatexProduit(stor.param.lambdaDecimal, stor.borneGauche)) + '}'
        break
      case 'droite':
        j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1_2, { v: stor.param.nomVar })
        ecritureProba = 'P(' + stor.param.nomVar + '>' + stor.borneDroite + ')=\\mathrm{e}^{-' + stor.param.lambdaDecimal + '\\times ' + stor.borneDroite + '}=\\mathrm{e}^{' + j3pGetLatexOppose(j3pGetLatexProduit(stor.param.lambdaDecimal, stor.borneDroite)) + '}'
        break
      default :
        j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1_3, { v: stor.param.nomVar })
        ecritureProba = 'P(' + stor.bornesInt[0] + '&lt;' + stor.param.nomVar + '&lt;' + stor.bornesInt[1] + ')=\\mathrm{e}^{-' + stor.param.lambdaDecimal + '\\times ' + stor.bornesInt[0] + '}-\\mathrm{e}^{-' + stor.param.lambdaDecimal + '\\times ' + stor.bornesInt[1] + '}=\\mathrm{e}^{' + j3pGetLatexOppose(j3pGetLatexProduit(stor.param.lambdaDecimal, stor.bornesInt[0])) + '}-\\mathrm{e}^{' + j3pGetLatexOppose(j3pGetLatexProduit(stor.param.lambdaDecimal, stor.bornesInt[1])) + '}'
        break
    }
    // dans explication1, on a l’explication correspondant à la première question
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2, {
      p: ecritureProba,
      r: Math.round(Math.pow(10, 3) * stor.zoneInput.reponse[0]) / Math.pow(10, 3)
    })
    if (stor.tabCasFigure[(me.questionCourante - 1) % ds.nbetapes] === 'gauche') {
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr4, { v: stor.param.nomVar })
    }
    j3pAffiche(stor.zoneExpli4, '', ds.textes.corr3)

    function actualisationFigure () {
      const valLambda = j3pCalculValeur(stor.param.lambdaDecimal)
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'lambda', String(valLambda))
      const uniteord = 5.3 / (1.1 * valLambda)
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'uniteord', String(uniteord))
      const gradord = (valLambda < 0.1) ? 0.01 : (valLambda < 0.5) ? 0.1 : (valLambda < 1) ? 0.2 : (valLambda < 2) ? 0.5 : 1
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'pasgrad2', String(gradord))// pas des graduations sur l’axe des ordonnées
      const uniteabs = (valLambda >= 0.4)
        ? Math.min(-210 * valLambda / (20 * Math.log(0.005)), 10.5 / (stor.bornesPossibles[2] + 1))
        : 10.5 / (4 * (stor.bornesPossibles[2] + 1))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'uniteabs', String(uniteabs))
      const gradabs = (10.5 / uniteabs < 3) ? 0.5 : (10.5 / uniteabs < 8) ? 1 : 2
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'pasgrad', String(gradabs))// pas des graduations sur l’axe des abscisses
      // il faut que je bricole un peu pour les valeurs faibles de lambda
      const plusGrandGrad = 30// (valLambda<=0.4)?Math.ceil(-Math.log(gradord/(20*valLambda))/valLambda)+1:20
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'gradmax', String(plusGrandGrad))
      switch (stor.tabCasFigure[(me.questionCourante - 1) % ds.nbetapes]) {
        case 'gauche':
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', '0')
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(stor.borneGauche))
          break
        case 'droite':
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(stor.borneDroite))
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(plusGrandGrad))
          break
        default :
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(stor.bornesInt[0]))
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(stor.bornesInt[1]))
          break
      }
      stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
      stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
    }

    depart(actualisationFigure)
  }

  function depart (callback) {
    // txt html de la figure
    // cela ne se fait dans cette section qu'à la fin où on affiche la figure où apparait la zone dont on cherche l’aire
    stor.divMtg32 = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.divMtg32, { paddingTop: '10px', textAlign: 'center' })
    // et enfin la zone svg (très importante car tout est dedans)
    const largFig = 583// on gère ici la largeur de la figure
    const hautFig = 333
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(stor.divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+szMzAANmcmH2+v4BAP8BAAAAAAAAAAACRwAAAU0AAAAAAAAAAAAAAAAAAACJ#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEAB0NDYWxjdWwA#####wAIcGFzZ3JhZDEAATEAAAABP#AAAAAAAAAAAAACAP####8ACHBhc2dyYWQyAAMwLjIAAAABP8mZmZmZmZr#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAQAAFPAMA2AAAAAAAAwAAAAAAAAAAFAAFARQAAAAAAAEBxkAAAAAAA#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAAA8AAAEAAQAAAAMBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAARAAAUkAwBAAAAAAAABAEAAAAAAAAAUAAUBJAAAAAAAAAAAABP####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAwAAAAX#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAPAAABAAEAAAADAAAABv####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAMAAAAF#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAcAAAAI#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAADwAAAMAoAAAAAAAAwBAAAAAAAAAFAAEAAAAJAAAACgD#####AQAAAAEQAAFKAMAoAAAAAAAAwBAAAAAAAAAFAAIAAAAJ#####wAAAAIAB0NSZXBlcmUA#####wCAgIABAQAAAAMAAAAFAAAACwAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAP####8AAAABAAlDTG9uZ3VldXIA#####wAAAAMAAAAFAAAAAgD#####AAduYmdyYWR4AAIyMAAAAAFANAAAAAAAAAAAAAIA#####wAHbmJncmFkeQACMjAAAAABQDQAAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAXR3JhZHVhdGlvbkF4ZXNSZXBlcmVOZXcAAAAbAAAACAAAAAMAAAAMAAAADgAAAA######AAAAAQATQ0Fic2Npc3NlT3JpZ2luZVJlcAAAAAAQAAVhYnNvcgAAAAz#####AAAAAQATQ09yZG9ubmVlT3JpZ2luZVJlcAAAAAAQAAVvcmRvcgAAAAz#####AAAAAQAKQ1VuaXRleFJlcAAAAAAQAAZ1bml0ZXgAAAAM#####wAAAAEACkNVbml0ZXlSZXAAAAAAEAAGdW5pdGV5AAAADP####8AAAABABBDUG9pbnREYW5zUmVwZXJlAAAAABAAAAAAAA8AAAEFAAAAAAz#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAEQAAABMAAAASAAAAEgAAAAAQAAAAAAAPAAABBQAAAAAM#####wAAAAEACkNPcGVyYXRpb24AAAAAEwAAABEAAAATAAAAEwAAABMAAAASAAAAEgAAAAAQAAAAAAAPAAABBQAAAAAMAAAAEwAAABEAAAAUAAAAABMAAAASAAAAEwAAABT#####AAAAAQALQ0hvbW90aGV0aWUAAAAAEAAAABUAAAATAAAADv####8AAAABAAtDUG9pbnRJbWFnZQAAAAAQAAAAAAAPAAABBQAAAAAWAAAAGAAAABUAAAAAEAAAABUAAAATAAAADwAAABYAAAAAEAAAAAAADwAAAQUAAAAAFwAAABr#####AAAAAQAIQ1NlZ21lbnQAAAAAEAEAAAAADQAAAQABAAAAFgAAABkAAAAXAAAAABABAAAAAA0AAAEAAQAAABcAAAAbAAAABQAAAAAQAQAAAAALAAFXAMAUAAAAAAAAwDQAAAAAAAAFAAE#3FZ4mrzfDgAAABz#####AAAAAgAIQ01lc3VyZVgAAAAAEAAHeENvb3JkMQAAAAwAAAAeAAAAAgAAAAAQAAVhYnN3MQAHeENvb3JkMQAAABMAAAAf#####wAAAAIAEkNMaWV1T2JqZXRQYXJQdExpZQEAAAAQAWZmZgAAAB4AAAATAAAADgAAAB4AAAACAAAAHgAAAB4AAAACAAAAABAABWFic3cyAA0yKmFic29yLWFic3cxAAAAFAEAAAAUAgAAAAFAAAAAAAAAAAAAABMAAAARAAAAEwAAACAAAAASAAAAABABAAAAAA8AAAEFAAAAAAwAAAATAAAAIgAAABMAAAASAAAAGQEAAAAQAWZmZgAAACMAAAATAAAADgAAAB4AAAAFAAAAHgAAAB8AAAAgAAAAIgAAACMAAAAFAAAAABABAAAAAAsAAVIAQCAAAAAAAADAIAAAAAAAAAUAAT#RG06BtOgfAAAAHf####8AAAACAAhDTWVzdXJlWQAAAAAQAAd5Q29vcmQxAAAADAAAACUAAAACAAAAABAABW9yZHIxAAd5Q29vcmQxAAAAEwAAACYAAAAZAQAAABABZmZmAAAAJQAAABMAAAAPAAAAJQAAAAIAAAAlAAAAJQAAAAIAAAAAEAAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAAUAQAAABQCAAAAAUAAAAAAAAAAAAAAEwAAABIAAAATAAAAJwAAABIAAAAAEAEAAAAADwAAAQUAAAAADAAAABMAAAARAAAAEwAAACkAAAAZAQAAABABZmZmAAAAKgAAABMAAAAPAAAAJQAAAAUAAAAlAAAAJgAAACcAAAApAAAAKv####8AAAACAAxDQ29tbWVudGFpcmUAAAAAEAFmZmYAAAAAAAAAAABAGAAAAAAAAAAAAB4LAAH###8AAAABAAAAAAALI1ZhbChhYnN3MSkAAAAZAQAAABABZmZmAAAALAAAABMAAAAOAAAAHgAAAAQAAAAeAAAAHwAAACAAAAAsAAAAGwAAAAAQAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAIwsAAf###wAAAAEAAAAAAAsjVmFsKGFic3cyKQAAABkBAAAAEAFmZmYAAAAuAAAAEwAAAA4AAAAeAAAABgAAAB4AAAAfAAAAIAAAACIAAAAjAAAALgAAABsAAAAAEAFmZmYAwCAAAAAAAAA#8AAAAAAAAAAAACULAAH###8AAAACAAAAAQALI1ZhbChvcmRyMSkAAAAZAQAAABABZmZmAAAAMAAAABMAAAAPAAAAJQAAAAQAAAAlAAAAJgAAACcAAAAwAAAAGwAAAAAQAWZmZgDAHAAAAAAAAAAAAAAAAAAAAAAAKgsAAf###wAAAAIAAAABAAsjVmFsKG9yZHIyKQAAABkBAAAAEAFmZmYAAAAyAAAAEwAAAA8AAAAlAAAABgAAACUAAAAmAAAAJwAAACkAAAAqAAAAMgAAAAIA#####wAIdW5pdGVhYnMABDMuNzYAAAABQA4UeuFHrhQAAAACAP####8ACHVuaXRlb3JkAAQyLjQxAAAAAUADR64UeuFIAAAAEgD#####AQAAAAAQAAJJMQDAAAAAAAAAAEAQAAAAAAAABQAAAAAMAAAAEwAAADQAAAABAAAAAAAAAAAAAAASAP####8AAAAAARAAAkoxAMAyAAAAAAAAwCYAAAAAAAAFAAAAAAwAAAABAAAAAAAAAAAAAAATAAAANQAAAAYA#####wAAAAAAEAAAAQABAAAAAwAAADYAAAAGAP####8AAAAAABAAAAEAAQAAAAMAAAA3AAAACwD#####AAAAAAEBAAAAAwAAADYAAAA3AQEBAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAAAAAAAgD#####AAdncmFkbWluAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAdncmFkbWF4AAIyMAAAAAFANAAAAAAAAAAAAAIA#####wAHcGFzZ3JhZAABMQAAAAE#8AAAAAAAAAAAAA0A#####wAXR3JhZHVhdGlvbkF4ZUhvcml6b250YWwAAAAKAAAAAgAAAAUAAAA7AAAAPAAAAD0AAAADAAAANgAAAAIAAAAAPgAIZ3JhZG1pbjEAHHBhc2dyYWQqaW50KGdyYWRtaW4vcGFzZ3JhZCkAAAAUAgAAABMAAAA9#####wAAAAIACUNGb25jdGlvbgIAAAAUAwAAABMAAAA7AAAAEwAAAD0AAAACAAAAAD4ACGdyYWRtYXgxABxwYXNncmFkKmludChncmFkbWF4L3Bhc2dyYWQpAAAAFAIAAAATAAAAPQAAABwCAAAAFAMAAAATAAAAPAAAABMAAAA9AAAAAgAAAAA+AAZuYmdyYWQAHShncmFkbWF4MS1ncmFkbWluMSkvcGFzZ3JhZCsxAAAAFAAAAAAUAwAAABQBAAAAEwAAAEAAAAATAAAAPwAAABMAAAA9AAAAAT#wAAAAAAAA#####wAAAAEAEUNQb2ludFBhckFic2Npc3NlAAAAAD4AAAAAAA8AAAEFAAAAAAMAAAA2AAAAEwAAAD8AAAAdAAAAAD4AAAAAAA8AAAEFAAAAAAMAAAA2AAAAEwAAAEAAAAAXAAAAAD4AAAD#AA0AAAEAAQAAAEIAAABDAAAABQAAAAA+AAAAAAAOAAFXAMAUAAAAAAAAwDcAAAAAAAAFAAE#0KCgoKCgoQAAAET#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAAAAAD4ABGFiczEAAAADAAAANgAAAEUAAAACAAAAAD4ABGFic3cABGFiczEAAAATAAAARv####8AAAACAAZDTGF0ZXgAAAAAPgAAAAAAAAAAAAAAAABAGAAAAAAAAAAAAEUNAAH2+v4AAAABAAAAAAAKXFZhbHthYnN3fQAAABkBAAAAPgBmZmYAAABFAAAAEwAAAEEAAABFAAAAAgAAAEUAAABFAAAAGQEAAAA+AAAAAAAAAEgAAAATAAAAQQAAAEUAAAAEAAAARQAAAEYAAABHAAAASAAAAAIA#####wAIZ3JhZG1pbjEACHBhc2dyYWQxAAAAEwAAAAEAAAACAP####8ACGdyYWRtYXgxAAIyMAAAAAFANAAAAAAAAAAAAAIA#####wAGbGFtYmRhAAEyAAAAAUAAAAAAAAAA#####wAAAAEABUNGb25jAP####8AAWYAFWxhbWJkYSpleHAoLWxhbWJkYSp4KQAAABQCAAAAEwAAAE0AAAAcB#####8AAAABAAxDTW9pbnNVbmFpcmUAAAAUAgAAABMAAABN#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAABeAAAAAUA#####wEAAAAAEAACeDEBBQABQCr7siutnHYAAAAGAAAAGAD#####AAd4Q29vcmQxAAAADAAAAE8AAAACAP####8AAngxAAd4Q29vcmQxAAAAEwAAAFAAAAACAP####8AAnkxAAVmKHgxKf####8AAAABAA5DQXBwZWxGb25jdGlvbgAAAE4AAAATAAAAUQAAABIA#####wEAAAAADwAAAQUAAAAADAAAABMAAABRAAAAEwAAAFL#####AAAAAgANQ0xpZXVEZVBvaW50cwD#####AQAAAAABAAAAUwAAAfQAAQAAAE8AAAAFAAAATwAAAFAAAABRAAAAUgAAAFMAAAASAP####8BAAAAAA8AAAEFAAAAAAwAAAABAAAAAAAAAAAAAAABAAAAAAAAAAD#####AAAAAQAMQ1RyYW5zbGF0aW9uAP####8AAAADAAAABQAAABYA#####wEAAAAADwAAAQUAAAAAVQAAAFb#####AAAAAQANQ0RlbWlEcm9pdGVPQQD#####AAAAAAANAAABAAEAAABVAAAAVwAAAAUA#####wEAAAAAEAACeDIBBQABQBaJHwLstAUAAABYAAAAGAD#####AAd4Q29vcmQyAAAADAAAAFkAAAACAP####8AAngyAAd4Q29vcmQyAAAAEwAAAFoAAAACAP####8AAnkyAAVmKHgyKQAAACMAAABOAAAAEwAAAFsAAAASAP####8BAAAAAA8AAAEFAAAAAAwAAAATAAAAWwAAABMAAABcAAAAAgD#####AAhncmFkbWluMgAIcGFzZ3JhZDIAAAATAAAAAgAAAAIA#####wAIZ3JhZG1heDIAAjIwAAAAAUA0AAAAAAAAAAAADQD#####ABVHcmFkdWF0aW9uQXhlVmVydGljYWwAAAAKAAAAAgAAAAUAAABeAAAAXwAAAAIAAAADAAAANwAAAAIAAAAAYAAIZ3JhZG1pbjAAH3Bhc2dyYWQyKmludChncmFkbWluMi9wYXNncmFkMikAAAAUAgAAABMAAAACAAAAHAIAAAAUAwAAABMAAABeAAAAEwAAAAIAAAACAAAAAGAACGdyYWRtYXgwAB9wYXNncmFkMippbnQoZ3JhZG1heDIvcGFzZ3JhZDIpAAAAFAIAAAATAAAAAgAAABwCAAAAFAMAAAATAAAAXwAAABMAAAACAAAAAgAAAABgAAZuYmdyYWQAHihncmFkbWF4MC1ncmFkbWluMCkvcGFzZ3JhZDIrMQAAABQAAAAAFAMAAAAUAQAAABMAAABiAAAAEwAAAGEAAAATAAAAAgAAAAE#8AAAAAAAAAAAAB0AAAAAYAAAAAAADwAAAQUAAAAAAwAAADcAAAATAAAAYQAAAB0AAAAAYAAAAAAADwAAAQUAAAAAAwAAADcAAAATAAAAYgAAABcAAAAAYAAAAP8ADQAAAQABAAAAZAAAAGUAAAAFAAAAAGAAAAAAAA4AAVcAwBQAAAAAAADANwAAAAAAAAUAAT#QoKCgoKChAAAAZgAAAB4AAAAAYAAEYWJzMQAAAAMAAAA3AAAAZwAAAAIAAAAAYAAEYWJzdwAEYWJzMQAAABMAAABoAAAAHwAAAABgAAAAAADAIgAAAAAAAAAAAAAAAAAAAAAAZw0AAfb6#gAAAAIAAAABAApcVmFse2Fic3d9AAAAGQEAAABgAGZmZgAAAGcAAAATAAAAYwAAAGcAAAACAAAAZwAAAGcAAAAZAQAAAGAAAAAAAAAAagAAABMAAABjAAAAZwAAAAQAAABnAAAAaAAAAGkAAABqAAAAEgD#####AQAAAAAPAAABBQAAAAA6AAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAJQD#####AAAAAwAAADYAAAAWAP####8BAAAAAA8AAAEFAAAAAG0AAABuAAAAJgD#####AAAAAAANAAABAAEAAABtAAAAbwAAAAUA#####wEAAAAAEAACeDMBBQABQA7V5A93Z#wAAABwAAAAGAD#####AAd4Q29vcmQzAAAAOgAAAHEAAAACAP####8AAngzAAd4Q29vcmQzAAAAEwAAAHIAAAACAP####8AAnkzAAVmKHgzKQAAACMAAABOAAAAEwAAAHMAAAASAP####8BAAAAAA8AAAEFAAAAADoAAAATAAAAcwAAABMAAAB0AAAAJAD#####AAAAAAABAAAAdQAAAfQAAQAAAHEAAAAFAAAAcQAAAHIAAABzAAAAdAAAAHUAAAACAP####8AAWEAAzAuNQAAAAE#4AAAAAAAAAAAAAIA#####wABYgADMS4yAAAAAT#zMzMzMzMzAAAAEgD#####AAB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAYAAAAAOgAAABMAAAB3AAAAAQAAAAAAAAAAAAAAEgD#####AAB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAYAAAAAOgAAABMAAAB4AAAAAQAAAAAAAAAAAAAAFwD#####AQAAAAANAAABAAEAAAB5AAAAegAAAAUA#####wEAAAAAEAACeDQBBQABP+wQAcMIje8AAAB7AAAAGAD#####AAd4Q29vcmQ0AAAAOgAAAHwAAAACAP####8AAng0AAd4Q29vcmQ0AAAAEwAAAH0AAAACAP####8AAnk0AAVmKHg0KQAAACMAAABOAAAAEwAAAH4AAAASAP####8BAAAAABAAAAEFAAAAADoAAAATAAAAfgAAABMAAAB#AAAAJAD#####AAAAAAABAAAAgAAAAfQAAQAAAHwAAAAFAAAAfAAAAH0AAAB+AAAAfwAAAID#####AAAAAQASQ1N1cmZhY2VMaWV1RHJvaXRlAP####8AAH8AAAAABQAAAIEAAAA4AAAAEgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAOgAAABMAAAB3AAAAIwAAAE4AAAATAAAAdwAAABIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAADoAAAATAAAAeAAAACMAAABOAAAAEwAAAHgAAAAXAP####8AAH8AAA0AAAEAAQAAAIMAAAB5AAAAFwD#####AAB#AAANAAABAAEAAACEAAAAev####8AAAABAA9DVmFsZXVyQWZmaWNoZWUA#####wAAfwAAAAAAAAAAAABANAAAAAAAAAAAAHkSAAAAAAABAAAAAAAAAAACAAAAdwAAACgA#####wAAfwAAv#AAAAAAAABAMwAAAAAAAAAAAHoSAAAAAAABAAAAAAAAAAACAAAAeAAAAA3##########w=='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true)
    callback() // cette fonction ne peut être appelée qu’une fois la figure chargée. Elle permet de l’adapter au données de l’exo
  }

  function suite2 () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (stor.param.trouverLambdaAvant) {
      j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1,
        {
          v: stor.param.nomVar,
          p: stor.param.probaDonnee,
          l: '-\\frac{\\ln(' + stor.param.valProba + ')}{' + stor.param.borne + '}'
        })
      if (me.questionCourante % 2 === 1) {
        stor.param.lambdaDecimal = j3pArrondi(-Math.log(stor.param.valProba) / stor.param.borne, 3)
      }
      j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, { l: stor.param.lambdaDecimal })
    } else {
      if ((me.questionCourante - 1) % ds.nbetapes === 0) {
        if (!stor.param.nomVar) stor.param.nomVar = j3pRandomTab(['X', 'Y', 'Z', 'T'], [0.25, 0.25, 0.25, 0.25])
        stor.param.lambdaDecimal = Number(stor.param.lambda.expression[(me.questionCourante - 1) / ds.nbetapes])
        stor.param.trouverLambdaAvant = false
      }
      const consigne1 = (ds.donneesPrecedentes) ? ds.textes.consigne3_2 : ds.textes.consigne3
      j3pAffiche(stor.zoneCons1, '', consigne1, {
        v: stor.param.nomVar,
        p: stor.param.probaDonnee,
        l: stor.param.lambdaDecimal
      })
    }
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      // On choisit les cas de figure à proposer et on mélange l’ordre des questions
      const tabCasFigure = ['gauche', 'droite', 'autre']
      // 'gauche' correspond à une proba P(X<b), 'droite' à P(X>b) et 'autre' à P(a<X<b)
      stor.tabCasFigure = j3pShuffle(tabCasFigure)
      stor.bornesPossibles = []
      let pb
      do {
        // on fait attention que la proba ne soit pas trop petite ou proche de 1
        do {
          stor.bornesPossibles[0] = Math.round(10 * 0.2 * j3pGetRandomInt(3, 25)) / 10
          stor.bornesPossibles[1] = Math.round(10 * 0.2 * j3pGetRandomInt(3, 25)) / 10
          stor.bornesPossibles[2] = Math.round(10 * 0.2 * j3pGetRandomInt(3, 25)) / 10
        } while ((Math.abs(stor.bornesPossibles[0] - stor.bornesPossibles[1]) < 0.5) || (Math.abs(stor.bornesPossibles[0] - stor.bornesPossibles[2]) < 0.5) || (Math.abs(stor.bornesPossibles[1] - stor.bornesPossibles[2]) < 0.5))
        pb = (stor.param.trouverLambdaAvant) ? ((Math.abs(stor.bornesPossibles[0] - stor.param.borne) < 0.3) || (Math.abs(stor.bornesPossibles[1] - stor.param.borne) < 0.3) || (Math.abs(stor.bornesPossibles[2] - stor.param.borne) < 0.3)) : false
        const valLambda = j3pCalculValeur(stor.param.lambdaDecimal)
        // j’ai vérifié si la borne demandé n’est pas celle qui a été donnée dans la section précédente,
        const reponseProba1 = 1 - Math.exp(-valLambda * stor.bornesPossibles[0])
        const reponseProba2 = 1 - Math.exp(-valLambda * stor.bornesPossibles[1])
        const reponseProba3 = 1 - Math.exp(-valLambda * stor.bornesPossibles[2])
        pb = (pb || (Math.abs(reponseProba1) < 0.01) || (Math.abs(reponseProba1 - 1) < 0.01) || (Math.abs(reponseProba2) < 0.01) || (Math.abs(reponseProba2 - 1) < 0.01) || (Math.abs(reponseProba3) < 0.01) || (Math.abs(reponseProba3 - 1) < 0.01))
      } while (pb)
      // j’ordonne les valeurs de stor.bornesPossibles
      stor.bornesPossibles.sort()
      me.logIfDebug(stor.bornesPossibles)
      const nbCasFigure = [0, 1, 2]
      let nb
      if (stor.tabCasFigure[0] === 'gauche') {
        nb = j3pGetRandomInt(0, 2)
        stor.borneGauche = stor.bornesPossibles[nbCasFigure[nb]]
        nbCasFigure.splice(nb, 1)
        if (stor.tabCasFigure[1] === 'droite') {
          nb = j3pGetRandomInt(0, 1)
          stor.borneDroite = stor.bornesPossibles[nbCasFigure[nb]]
        } else {
          stor.bornesInt = [stor.bornesPossibles[nbCasFigure[0]], stor.bornesPossibles[nbCasFigure[1]]]
        }
      } else if (stor.tabCasFigure[0] === 'droite') {
        nb = j3pGetRandomInt(0, 2)
        stor.borneDroite = stor.bornesPossibles[nbCasFigure[nb]]
        nbCasFigure.splice(nb, 1)
        if (stor.tabCasFigure[1] === 'gauche') {
          nb = j3pGetRandomInt(0, 1)
          stor.borneGauche = stor.bornesPossibles[nbCasFigure[nb]]
        } else {
          stor.bornesInt = [stor.bornesPossibles[nbCasFigure[0]], stor.bornesPossibles[nbCasFigure[1]]]
        }
      } else {
        nb = j3pGetRandomInt(0, 2)
        stor.bornesInt = []
        stor.bornesInt.push(stor.bornesPossibles[nbCasFigure[nb]])
        nbCasFigure.splice(nb, 1)
        nb = j3pGetRandomInt(0, 1)
        stor.bornesInt.push(stor.bornesPossibles[nbCasFigure[nb]])
        stor.bornesInt.sort()
        // console.log('stor.bornesInt:',stor.bornesInt)
        nbCasFigure.splice(nb, 1)
        if (stor.tabCasFigure[1] === 'gauche') {
          stor.borneGauche = stor.bornesPossibles[nbCasFigure[0]]
        } else {
          stor.borneDroite = stor.bornesPossibles[nbCasFigure[0]]
        }
      }
    }
    const casFigure = stor.tabCasFigure[(me.questionCourante - 1) % ds.nbetapes]
    let laReponse, elt
    switch (casFigure) {
      case 'gauche' :
        elt = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne4_1,
          {
            v: stor.param.nomVar,
            b: stor.borneGauche,
            inputmq1: { texte: '' }
          })
        laReponse = 1 - Math.exp(-j3pCalculValeur(stor.param.lambdaDecimal) * stor.borneGauche)
        break
      case 'droite':
        elt = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne4_2,
          {
            v: stor.param.nomVar,
            b: stor.borneDroite,
            inputmq1: { texte: '' }
          })
        laReponse = Math.exp(-j3pCalculValeur(stor.param.lambdaDecimal) * stor.borneDroite)
        break
      default:
        elt = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne4_3,
          {
            v: stor.param.nomVar,
            b: stor.bornesInt[0],
            c: stor.bornesInt[1],
            inputmq1: { texte: '' }
          })
        laReponse = Math.exp(-j3pCalculValeur(stor.param.lambdaDecimal) * stor.bornesInt[0]) - Math.exp(-j3pCalculValeur(stor.param.lambdaDecimal) * stor.bornesInt[1])
        break
    }
    stor.zoneInput = elt.inputmqList[0]
    me.logIfDebug('laReponse :', laReponse)
    mqRestriction(stor.zoneInput, '\\d.,-')
    stor.zoneInput.typeReponse = ['nombre', 'arrondi', 0.001]
    stor.zoneInput.reponse = [laReponse]
    j3pAffiche(stor.zoneCons4, '', ds.textes.consigne5)
    j3pStyle(stor.zoneCons4, { fontStyle: 'italic', fontSize: '0.9em' })

    // on Exporte les variables présentes dans cette section pour qu’elles soient utilisées dans une autre section
    me.donneesPersistantes.loiExpo = {}
    Object.entries(stor.param).forEach(([prop, value]) => {
      me.donneesPersistantes.loiExpo[prop] = value
    })
    j3pFocus(stor.zoneInput)
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })

    // Obligatoire
    me.finEnonce()
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 2,
      nbetapes: 2,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      lambda: '[0.55;2.50]',
      donneesPrecedentes: false,

      /*
      Les textes présents dans la section
      Sont donc accessibles dans le code par donneesSection.textes.consigne1
      Possibilité d’écrire dans le code :
      var lestextes: donneesSection.textes;
      puis accès à l’aide de lestextes.consigne1
      */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Loi exponentielle : calcul de probabilité',
        // on donne les phrases de la consigne
        // les premières consignes ne vont être affichées que si on reprend les données de la section loiExpoTrouverLambda
        consigne1: '$£v$ étant une variable aléatoire suivant une loi exponentielle de paramètre $\\lambda>0$ et vérifiant $£p$, on a montré dans la question précédente que $\\lambda=£l$.',
        consigne2: 'Pour cette question, on prend $£l$ pour valeur de $\\lambda$.',
        // consigne3 est la consigne lorsqu’on n’utilise pas auparavant la section loiExpoTrouverLambda
        consigne3: 'Soit $£v$ une variable aléatoire suivant une loi exponentielle de paramètre $£l$.',
        consigne3_2: '$£v$ est toujours la variable aléatoire suivant la loi exponentielle de paramètre $£l$.',
        consigne4_1: 'La probabilité que $£v$ soit inférieure à $£b$ vaut environ &1&.',
        consigne4_2: 'La probabilité que $£v$ soit supérieure à $£b$ vaut environ &1&.',
        consigne4_3: 'La probabilité que $£v$ soit comprise entre $£b$ et $£c$ vaut environ &1&.',
        consigne5: 'Donner la valeur arrondie à $10^{-3}$ de la probabilité.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Peut-être est-ce un problème d’arrondi&nbsp;?',
        comment2: 'On rappelle qu’une probabilité est un nombre réel compris entre 0 et 1 !',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'D’après le cours, pour tout réel $t\\geq 0$, $P(£v&lt;t)=P(£v\\leq t)=1-\\mathrm{e}^{-\\lambda t}$ où $\\lambda$ est le paramètre de la loi exponentielle.',
        corr1_2: 'D’après le cours, pour tout réel $t\\geq 0$, $P(£v>t)=P(£v\\geq t)=\\mathrm{e}^{-\\lambda t}$ où $\\lambda$ est le paramètre de la loi exponentielle.',
        corr1_3: 'D’après le cours, pour tous réels positifs $a$ et $b$ tels que $a&lt;b$, $P(a&lt;£v&lt;b)=P(a\\leq £v\\leq b)=\\mathrm{e}^{-\\lambda a}-\\mathrm{e}^{-\\lambda b}$ où $\\lambda$ est le paramètre de la loi exponentielle.',
        corr2: 'Ainsi $£p\\approx £r$.',
        corr4: 'Remarque : la formule de calcul peut être vue comme une conséquence d’un résultat plus général :<br/>$P(£v&lt;t)=P(0\\leq £v&lt;t)=\\mathrm{e}^0-\\mathrm{e}^{-\\lambda t}$.',
        corr3: 'On a représenté ci-dessous la probabilité cherchée sous la forme d’une aire.'
      },
      pe: 0
    }
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // code de création des fenêtres
        if (ds.donneesPrecedentes) ds.nbrepetitions = 1
        if ((ds.nbetapes > 2) || (ds.nbetapes < 1)) ds.nbetapes = 2
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        stor.param = {}
        // on vérifie que l’on retrouve ce que l’on attend si donneesPrecedentes est à true
        if (ds.donneesPrecedentes) {
          if (!me.donneesPersistantes.loiExpo) {
            ds.donneesPrecedentes = false
            j3pShowError(Error('Les données précédentes n’ont pu être récupérées. De nouvelles sont générées !'))
          }
        }
        if (ds.donneesPrecedentes) {
          Object.entries(me.donneesPersistantes.loiExpo).forEach(([prop, value]) => {
            stor.param[prop] = value
          })
        } else {
          stor.param.lambda = constructionTabIntervalle(ds.lambda, '[0.55;2.50]', ds.nbrepetitions, [1])
          me.logIfDebug('stor.param.lambda:', stor.param.lambda)
        }
        // chargement mtg
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              suite2()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite2()
      }

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // je vérifie si l’élève n’a pas donné une valeur approchée plutôt qu’un arrondi
              if ((j3pNombre(fctsValid.zones.reponseSaisie[0]) < 0) || (j3pNombre(fctsValid.zones.reponseSaisie[0]) > 1)) {
              // c’est que l’élève donne un résultats incohérent (proba qui n’est pas entre 0 et 1)
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
              } else if (Math.abs(stor.zoneInput.reponse[0] - j3pNombre(fctsValid.zones.reponseSaisie[0])) <= 0.001) {
              // c’est que l’expression entrée est une valeur approchée à 10^{-3} et non un arrondi
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
