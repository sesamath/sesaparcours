import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pNombre, j3pRandomTab, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { tabLoiProba } from 'src/legacy/outils/tableauxProba/tabLoiProba'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juillet 2019
        Calcul de l’espérance et de la variance d’une variable aléatoire
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['texteLegende', false, 'boolean', 'Par défaut, on écrit x_i et P(X=x_i) dans la légende de la loi, mais on peut remplacer par "Valeurs de £x" et "Probabilités" lorsque ce paramètre vaudra true.'],
    ['VarianceEcartType', 'ecart type', 'liste', 'Lorsque ce paramètre vaut "ecart type" (resp. "variance"), alors on demande dans une première étape le calcul de l’espérance et dans une seconde celui de l’écart-type (ou la variance). Si on écrit "aucun", alors on ne demande que l’espérance', ['aucun', 'ecart type', 'variance']],
    ['formuleEcartType', 'formule', 'liste', 'La correction de l’écart-type peut être proposée sous deux forme : celle de la définition E(X-E(X))² ou à l’aide de la formule de Köning-Huygens E(X²)-(E(X))².', ['definition', 'formule']]
  ]

}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo1: 'Espérance et écart-type d’une variable aléatoire',
  titre_exo2: 'Espérance d’une variable aléatoire',
  titre_exo3: 'Espérance et variance d’une variable aléatoire',
  // on donne les phrases de la consigne
  consigne1: '$£x$ est une variable aléatoire dont la loi de probabilité est donnée ci-dessous&nbsp;:',
  consigne2: 'L’espérance de $£x$ vaut&nbsp;:',
  consigne3: 'On rappelle que $E(£x)=£e$. L’écart-type de $£x$ vaut alors&nbsp;:',
  consigne4: 'On rappelle que $E(£x)=£e$. La variance de $£x$ vaut&nbsp;:',
  info1: 'Arrondir à 0,01.',
  info2: 'Donner la valeur exacte.',
  legendes: 'Valeurs de $£x$|probabilités',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'N’y aurait-il pas un problème d’arrondi&nbsp;?',
  comment2: 'On rappelle par ailleurs que pour arrondir le taux à £a&nbsp;%, il faut arrondir la valeur décimale à £b.',
  // et les phrases utiles pour les explications de la réponse
  corr1: '$E(£x)$ est la moyenne des valeurs possibles de $£x$.',
  corr2: 'Donc l’espérance vaut £e.',
  corr3: 'Pour calculer l’écart-type de $£x$, on calcule tout d’abord sa variance.',
  corr3_1: 'On utilise la formule de la variance&nbsp;:',
  corr4: 'La variance de $£x$ vaut alors £v et son écart-type vaut $\\sigma(£x)=\\sqrt{V(£x)}\\approx £s$.',
  corr5: 'La variance de $£x$ vaut alors £v.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(stor.zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3, 4].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(stor.zoneExpli, 'div') })
    let formule, calcul
    if (stor.typeQuest === 'esperance') {
      j3pAffiche(stor.zoneExpli1, '', textes.corr1, { x: stor.va })
      formule = '$E(' + stor.va + ')=\\bigsum_{i=1}^{n}x_i\\times P(' + stor.va + '=x_i)=x_1\\times P(' + stor.va + '=x_1)+\\ldots +x_n\\times P(' + stor.va + '=x_n)$'
      calcul = '$E(' + stor.va + ')=' + stor.mesDonnees.valeurs[0] + '\\times ' + stor.mesDonnees.probas[0]
      for (let i = 1; i < stor.mesDonnees.valeurs.length; i++) {
        calcul += '+' + AjoutePar(stor.mesDonnees.valeurs[i]) + '\\times ' + stor.mesDonnees.probas[i]
      }
      calcul += '$'
      j3pAffiche(stor.zoneExpli2, '', formule)
      j3pAffiche(stor.zoneExpli3, '', calcul)
      j3pAffiche(stor.zoneExpli4, '', textes.corr2, { e: j3pVirgule(stor.espe) })
    } else {
      const laconsigne3 = (stor.typeQuest === 'variance') ? textes.corr3_1 : textes.corr3
      j3pAffiche(stor.zoneExpli1, '', laconsigne3, { x: stor.va })
      if (ds.formuleEcartType === 'formule') {
        formule = '$V(' + stor.va + ')=\\bigsum_{i=1}^{n}x_i^2\\times P(' + stor.va + '=x_i)-(E(' + stor.va + '))^2$<br/>$V(' + stor.va + ')=x_1^2\\times P(' + stor.va + '=x_1)+\\ldots +x_n^2\\times P(' + stor.va + '=x_n)-(E(' + stor.va + '))^2$'
        calcul = '$V(' + stor.va + ')=' + AjoutePar(stor.mesDonnees.valeurs[0]) + '^2\\times ' + stor.mesDonnees.probas[0]
        for (let i = 1; i < stor.mesDonnees.valeurs.length; i++) {
          calcul += '+' + AjoutePar(stor.mesDonnees.valeurs[i]) + '^2\\times ' + stor.mesDonnees.probas[i]
        }
        calcul += '-' + AjoutePar(stor.espe) + '^2$'
      } else {
        formule = '$V(' + stor.va + ')=\\bigsum_{i=1}^{n}(x_i-E(' + stor.va + '))^2\\times P(' + stor.va + '=x_i)$<br/>$V(' + stor.va + ')=(x_1-E(' + stor.va + '))^2\\times P(' + stor.va + '=x_1)+\\ldots +(x_n-E(' + stor.va + '))^2\\times P(' + stor.va + '=x_n)$'
        calcul = '$V(' + stor.va + ')=(' + stor.mesDonnees.valeurs[0] + '-' + AjoutePar(stor.espe) + ')^2\\times ' + stor.mesDonnees.probas[0]
        for (let i = 1; i < stor.mesDonnees.valeurs.length; i++) {
          calcul += '+(' + stor.mesDonnees.valeurs[i] + '-' + AjoutePar(stor.espe) + ')^2\\times ' + stor.mesDonnees.probas[i]
        }
        calcul += '$'
      }
      j3pAffiche(stor.zoneExpli2, '', formule)
      j3pAffiche(stor.zoneExpli3, '', calcul)
      const laconsigne4 = (stor.typeQuest === 'variance') ? textes.corr5 : textes.corr4
      j3pAffiche(stor.zoneExpli4, '', laconsigne4, {
        x: stor.va,
        v: j3pVirgule(stor.variance),
        s: Math.round(100 * Math.sqrt(stor.variance)) / 100
      })
    }
  }

  function AjoutePar (nb) {
    if (j3pNombre(String(nb)) >= 0) {
      return nb
    } else {
      return '(' + nb + ')'
    }
  }

  function enonceMain () {
    stor.typeQuest = (ds.VarianceEcartType === 'ecart type') ? 'ecartType' : 'variance'
    // création du conteneur dans lequel se trouveront toutes les consignes

    if ((stor.deuxQuestions && ((me.questionCourante - 1) % ds.nbetapes === 0)) || !stor.deuxQuestions) {
      stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
      // On choisit le nom de notre v.a.
      stor.va = j3pRandomTab(['X', 'Y', 'Z'], [0.34, 0.33, 0.33])
      stor.typeQuest = 'esperance'
      // Soit on a qu’une seule étape, soit on en a deux et nous en sommes à l’espérance
      // Du coup on déclare la nouvelle variable aléatoire
      const nbValeurs = j3pGetRandomInt(3, 4)
      stor.mesDonnees = { parent: stor.zoneCons2, va: stor.va }
      stor.mesDonnees.largeur = me.zonesElts.MG.getBoundingClientRect().width
      stor.mesDonnees.legendes = (ds.texteLegende) ? textes.legendes.split('|') : ['$x_i$', '$P(' + stor.va + '=x_i)$']
      stor.mesDonnees.complet = true
      stor.mesDonnees.valeurs = []
      const plusMoinsUn = 2 * j3pGetRandomInt(0, 1) - 1
      stor.mesDonnees.valeurs.push(plusMoinsUn * j3pGetRandomInt(1, 6))
      stor.mesDonnees.valeurs.push(stor.mesDonnees.valeurs[0] + j3pGetRandomInt(1, 8))
      stor.mesDonnees.valeurs.push(stor.mesDonnees.valeurs[1] + j3pGetRandomInt(1, 8))
      stor.mesDonnees.typeFont = me.styles.petit.enonce
      if (nbValeurs === 4) stor.mesDonnees.valeurs.push(stor.mesDonnees.valeurs[2] + j3pGetRandomInt(1, 8))
      let again
      do {
        stor.mesDonnees.probas = []
        let reste = 1
        let // C’est pour les proba : il faut qu’il me reste quelque chose à la fin
          maVal = 0
        again = false
        while (reste > 0.02 && maVal < nbValeurs - 1) {
          const newPrb = j3pGetRandomInt(2, 100 * reste) / 100
          stor.mesDonnees.probas.push(newPrb)
          reste = Math.round(100 * (reste - newPrb)) / 100
          maVal++
        }
        if (maVal === nbValeurs - 1 && reste > 0.02) {
          // je n’ai plus que la dernière proba à déterminer
          stor.mesDonnees.probas.push(reste)
        } else {
          // C’est qu’il ne reste quasiment plus rien pour la dernière proba donc on recommence
          again = true
        }
      } while (again)
      j3pAffiche(stor.zoneCons1, '', textes.consigne1, { x: stor.va })
      // console.log("stor.mesDonnees:",stor.mesDonnees)
      tabLoiProba(stor.mesDonnees)
    }
    const lesCons = []
    let valApprochee = false
    const objCons = { x: stor.va }
    if (stor.typeQuest === 'esperance') {
      // Question sur l’espérance
      lesCons.push(textes.consigne2, '$E(' + stor.va + ')=$&1&')
    } else if (stor.typeQuest === 'variance') {
      // Question sur la variance
      lesCons.push(textes.consigne4, '$V(' + stor.va + ')=$&1&')
      objCons.e = stor.espe
    } else {
      // Question sur l’écart-type
      lesCons.push(textes.consigne3, '$\\sigma(' + stor.va + ')\\approx $&1&')
      valApprochee = true
      objCons.e = stor.espe
    }
    j3pAffiche(stor.zoneCons3, '', lesCons[0], objCons)
    const elt = j3pAffiche(stor.zoneCons4, '', lesCons[1], {
      input1: { texte: '', dynamique: true }
    })
    stor.zoneInput = elt.inputmqList[0]
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    if (stor.typeQuest === 'esperance') {
      stor.espe = 0
      for (let i = 0; i < stor.mesDonnees.valeurs.length; i++) {
        stor.espe += stor.mesDonnees.valeurs[i] * stor.mesDonnees.probas[i]
      }
      stor.espe = Math.round(Math.pow(10, 5) * stor.espe) / Math.pow(10, 5)
      stor.zoneInput.reponse = [stor.espe]
    } else {
      stor.espe2 = 0
      for (let i = 0; i < stor.mesDonnees.valeurs.length; i++) {
        stor.espe2 += Math.pow(stor.mesDonnees.valeurs[i], 2) * stor.mesDonnees.probas[i]
      }
      stor.espe2 = Math.round(Math.pow(10, 5) * stor.espe2) / Math.pow(10, 5)
      stor.variance = Math.round(Math.pow(10, 5) * (stor.espe2 - Math.pow(stor.espe, 2))) / Math.pow(10, 5)
      if (stor.typeQuest === 'ecartType') {
        stor.larrondi = 0.01
        stor.zoneInput.typeReponse = ['nombre', 'arrondi', stor.larrondi]
        stor.zoneInput.reponse = [Math.sqrt(stor.variance)]
      } else {
        stor.zoneInput.reponse = [stor.variance]
      }
    }
    if (valApprochee || stor.typeQuest === 'variance') {
      stor.zoneCons5 = j3pAddElt(stor.conteneur, 'div')
      const infoArrondi = (stor.typeQuest === 'variance') ? textes.info2 : textes.info1
      j3pAffiche(stor.zoneCons5, '', infoArrondi, {
        styletexte: {
          fontStyle: 'italic',
          fontSize: '0.9em'
        }
      })
    }
    mqRestriction(stor.zoneInput, '\\d.,-')
    j3pFocus(stor.zoneInput)

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    // Paramétrage de la validation
    // zonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const zonesSaisie = [stor.zoneInput.id]
    stor.validationZones = new ValidationZones({ parcours: me, zones: zonesSaisie })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection

        me.surcharge({ nbetapes: 2 })

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70

        me.construitStructurePage({ structure: 'presentation1', ratioGauche: stor.pourc / 100 })
        const titreExo = (ds.VarianceEcartType === 'ecart type')
          ? textes.titre_exo1
          : (ds.VarianceEcartType === 'variance') ? textes.titre_exo3 : textes.titre_exo2
        me.afficheTitre(titreExo)
        stor.deuxQuestions = (['ecart type', 'variance'].indexOf(ds.VarianceEcartType) > -1)
        ds.nbetapes = (stor.deuxQuestions) ? 2 : 1
        ds.nbitems = ds.nbetapes * ds.nbrepetitions

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        if ((stor.deuxQuestions && ((me.questionCourante - 1) % ds.nbetapes === 0)) || !stor.deuxQuestions) {
          me.videLesZones()
        } else {
          // Je n’efface que ce qui ne va pas reservir
          j3pEmpty(me.zonesElts.MD)
          ;[3, 4, 5].forEach(i => j3pEmpty(stor['zoneCons' + i]))
          j3pDetruit(stor.zoneExpli)
        }
        me.afficheBoutonValider()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()
      break // case "enonce":
    }
    case 'correction': {
      // On teste si une réponse a été saisie
      const validationZones = stor.validationZones// ici je crée juste une variable pour raccourcir le nom de l’oject validationZones
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie

      const reponse = validationZones.validationGlobale()
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        me.reponseManquante(stor.zoneCorr)
        return me.finCorrection()
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (reponse.bonneReponse) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          afficheCorrection(true)
          me.typederreurs[0]++
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          let pbArrondi
          if (stor.typeQuest !== 'esperance') {
            const reponseSaisie = j3pNombre(validationZones.zones.reponseSaisie[0])
            pbArrondi = (Math.abs(stor.zoneInput.reponse[0] - reponseSaisie) < stor.larrondi * 10)
          }
          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(false)
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            if (stor.typeQuest !== 'esperance') {
              if (pbArrondi) {
                stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
              }
            }
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              return me.finCorrection()
            } else {
              // Erreur au nème essai
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              afficheCorrection(false)
              me.typederreurs[2]++
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
