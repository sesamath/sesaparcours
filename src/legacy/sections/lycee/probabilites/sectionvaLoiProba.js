import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pFocus, j3pGetRandomInt, j3pEmpty, j3pPGCD, j3pStyle, j3pValeurde, j3pGetNewId } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { tabLoiProba } from 'src/legacy/outils/tableauxProba/tabLoiProba'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juillet 2019
        Dans cette section, on demande de construire la loi de probabilité d’une variable aléatoire
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['texteLegende', false, 'boolean', 'Par défaut, on écrit x_i et P(X=x_i) dans la légende de la loi, mais on peut remplacer par "Valeurs de X" et "Probabilités" lorsque ce paramètre vaudra true.']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function afficheCorr (bonneRep) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pEmpty(stor.zoneCons5)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3, 4].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    const objVal = {}
    for (let i = 1; i <= stor.coupleRep.length; i++) {
      objVal['v' + i] = stor.coupleRep[i - 1].valeur
    }
    if ((stor.quest === 2) && (stor.param.n2 !== 4)) {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1_2, objVal)
    } else {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1_1, objVal)
    }
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2)
    stor.DonneesRep.parent = stor.zoneExpli3
    stor.DonneesRep.typeFont = me.styles.toutpetit.correction
    tabLoiProba(stor.DonneesRep)
    j3pAffiche(stor.zoneExpli4, '', ds.textes.corr3)
  }

  function constructionBtnAjoute () {
    const divBtn = j3pAddElt(stor.divAjoute, 'div')
    j3pAffiche(divBtn, '', ds.textes.phrase1)
    j3pAjouteBouton(divBtn, j3pGetNewId('bouton1'), 'MepBoutons2', '+', actionBtnPlus)
  }

  function constructionBtnEnleve () {
    const divBtn = j3pAddElt(stor.divEnleve, 'div')
    j3pAffiche(divBtn, '', ds.textes.phrase2)
    j3pAjouteBouton(divBtn, j3pGetNewId('bouton2'), 'MepBoutons2', '-', actionBtnMoins)
  }

  function actionBtnPlus () {
    // c’est fonction sera appelée pour recréer le tableau de signe avec une valeur de plus dans la première ligne
    for (let k = 0; k < stor.zoneInput.length / 2; k++) {
      // valeur de x déjà saisie (et donc à recopier par la suite)
      stor.valeursSaisies[k] = j3pValeurde(stor.zoneInput[k * 2])
      stor.mesDonnees.valeurs[k] = (stor.valeursSaisies[k] === '') ? '?' : stor.valeursSaisies[k]
      // valeur de proba déjà saisie (et donc à recopier)
      stor.probasSaisies[k] = $(stor.zoneInput[k * 2 + 1]).mathquill('latex')
      stor.mesDonnees.probas[k] = (stor.probasSaisies[k] === '') ? '-1' : stor.probasSaisies[k]
    }
    stor.mesDonnees.valeurs.push('?')
    stor.mesDonnees.probas.push('-1')
    if (stor.mesDonnees.valeurs.length === 6) j3pEmpty(stor.divAjoute)
    if (stor.mesDonnees.valeurs.length === 3) {
      // c’est que le btnEnleve n’était pas encore construit, donc on le fait.
      constructionBtnEnleve()
    }
    // on reconstruit ensuite le tableau
    j3pEmpty(stor.mesDonnees.parent)
    stor.zoneInput = tabLoiProba(stor.mesDonnees)
    declarationZoneAValider()
  }

  function actionBtnMoins () {
    // c’est fonction sera appelée pour recréer le tableau de signe avec une valeur de moins dans la première ligne
    for (let k = 0; k < stor.zoneInput.length / 2; k++) {
      // valeur de x déjà saisie (et donc à recopier par la suite)
      stor.valeursSaisies[k] = j3pValeurde(stor.zoneInput[k * 2])
      stor.mesDonnees.valeurs[k] = (stor.valeursSaisies[k] === '') ? '?' : stor.valeursSaisies[k]
      // valeur de proba déjà saisie (et donc à recopier)
      stor.probasSaisies[k] = $(stor.zoneInput[k * 2 + 1]).mathquill('latex')
      stor.mesDonnees.probas[k] = (stor.probasSaisies[k] === '') ? '-1' : stor.probasSaisies[k]
    }
    stor.mesDonnees.valeurs.pop()
    stor.mesDonnees.probas.pop()
    if (stor.mesDonnees.valeurs.length === 2) j3pEmpty(stor.divEnleve)
    if (stor.mesDonnees.valeurs.length === 5) {
      // c’est que le btnAjoute n’était pas encore construit, donc on le fait.
      constructionBtnAjoute()
    }
    // on reconstruit ensuite le tableau
    j3pEmpty(stor.mesDonnees.parent)
    stor.zoneInput = tabLoiProba(stor.mesDonnees)
    declarationZoneAValider()
  }

  function declarationZoneAValider () {
    // on récupère la liste avec le nom de toutes les zones de saisie
    // stor.mesDonnees.valeurs.length*2 est le nombre de zones de saisie.
    const mesZonesSaisie = []
    for (let i = 0; i < stor.zoneInput.length / 2; i++) {
      mesZonesSaisie.push(stor.zoneInput[i * 2].id)
      mesZonesSaisie.push(stor.zoneInput[i * 2 + 1].id)
    }
    j3pFocus(stor.zoneInput[0])
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesZonesSaisie })
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 2,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’élève pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      texteLegende: false,
      /*
      Les textes présents dans la section
      Sont donc accessibles dans le code par ds.textes.consigne1
      Possibilité d’écrire dans le code :
      var lestextes: ds.textes;
      puis accès à l’aide de lestextes.consigne1
      */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Loi de probabilité',
        // on donne les phrases de la consigne
        consigne1_1: 'Au cours d’une loterie, £t tickets ont été vendus. L’un d’eux permet de gagner la somme de £{s1}&nbsp;€, £{n1} tickets permettent de gagner £{s2}&nbsp;€ et £{n2} de rembourser la mise de départ. Les autres sont perdants.',
        consigne2_1: 'Un ticket coûte £m&nbsp;€.',
        consigne3_1: 'Un joueur achète un ticket. On note $X$ son gain algébrique (somme gagnée à laquelle on déduit le prix du ticket).',
        consigne1_2: 'Une urne contient £t boules indiscernables au toucher&nbsp;: £{n1} boules rouges numérotées de 1 à £{n1} et £{n2} boules vertes numérotées £l et £{nb}.',
        consigne2_2: 'Un joueur mise £m&nbsp;€ et tire une boule au hasard. Si elle est rouge, il gagne £{e1}&nbsp;€ et si elle est verte, il gagne en euros la valeur indiquée sur la boule.',
        consigne3_2: 'On note $X$ la variable aléatoire qui, à chaque boule choisie, associe son gain en tenant compte de la mise.',
        consigne1_3: 'A l’arrivée d’une course à pied, chaque participant peut prendre part à un tirage au sort.',
        consigne2_3: 'Un coureur a dépensé £m&nbsp;€ pour l’inscription. Lorsqu’il effectue le tirage parmi les £t tickets en jeu, l’un d’eux permet de gagner une montre connectée d’une valeur de £{s1}&nbsp;€, £{n1} tickets permettent de gagner un tee-shirt d’une valeur de £{s2}&nbsp;€ et £{n2} permettent de gagner une gourde d’une valeur de £{s3}&nbsp;€. Les autres tickets sont perdants.',
        consigne3_3: 'On note $X$ la variable aléatoire égale au gain réel du coureur (valeur du lot obtenu auquel on déduit les frais d’inscription).',
        consigne4: 'Compléter ci-dessous la loi de probabilité de la variable aléatoire $X$.',
        legendes: 'Valeurs de $X$|probabilités',
        phrase1: 'Pour ajouter une valeur de $X$ : ',
        phrase2: 'Pour retirer une valeur de $X$ : ',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Le nombre de valeurs proposé n’est pas correct&nbsp;!',
        comment2: 'Que doit vérifier la somme de toutes les probabilités&nbsp;?',
        comment3: 'Les valeurs de la variable aléatoire sont exactes, mais il faut revoir les probabilités&nbsp;!',
        comment4: 'Toutes les valeurs présentes dans la loi de probabilité doivent être différentes&nbsp;!',
        comment5: 'Pour les valeurs de X, as-tu pensé à déduire la mise&nbsp;?',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: '$X$ peut prendre les valeurs £{v1}, £{v2}, £{v3} et £{v4}.',
        corr1_2: '$X$ peut prendre les valeurs £{v1}, £{v2} et £{v3}.',
        corr2: 'On obtient ainsi la loi de $X$&nbsp;:',
        corr3: 'On remarque que la somme de toutes les probabilités vaut 1.'
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;

      pe: 0
    }
  }

  function enonceMain () {
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    stor.quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.param = {}
    stor.coupleRep = []
    let tabRep
    switch (stor.quest) {
      case 1:
        stor.param.t = 100 * j3pGetRandomInt(5, 8)// nombre de tickets
        stor.param.s1 = 500 * j3pGetRandomInt(1, 2)// gain le plus élevé
        stor.param.s2 = 50 * j3pGetRandomInt(1, 2)// deuxième gain
        stor.param.n1 = j3pGetRandomInt(5, 8)// Nombre de tickets qui permettent de gagner le deuxième gain
        stor.param.n2 = 4 * j3pGetRandomInt(3, 5)// Nombre de tickets qui permettent de rembourser
        stor.param.m = j3pGetRandomInt(3, 4)// mise de départ
        tabRep = [[1, stor.param.t], [stor.param.n1, stor.param.t], [stor.param.n2, stor.param.t], [stor.param.t - stor.param.n1 - stor.param.n2 - 1, stor.param.t]]
        stor.coupleRep.push({ valeur: (stor.param.s1 - stor.param.m), proba: 1 / stor.param.t })
        stor.coupleRep.push({ valeur: (stor.param.s2 - stor.param.m), proba: stor.param.n1 / stor.param.t })
        stor.coupleRep.push({ valeur: 0, proba: stor.param.n2 / stor.param.t })
        stor.coupleRep.push({
          valeur: -stor.param.m,
          proba: (stor.param.t - stor.param.n1 - stor.param.n2 - 1) / stor.param.t
        })
        break
      case 2:
        stor.param.t = j3pGetRandomInt(6, 7)// nombre de boules
        stor.param.n1 = Math.floor(stor.param.t / 2) + j3pGetRandomInt(0, 1)// Nombre de boules rouges
        stor.param.n2 = stor.param.t - stor.param.n1// Nombre de boules vertes
        stor.param.m = j3pGetRandomInt(2, 4)// mise de départ
        stor.param.e1 = stor.param.m + 1// gain s’il tire une rouge
        // valeurs sur les boules vertes
        switch (stor.param.n2) {
          case 2:
            stor.param.l = 0
            stor.param.nb = j3pGetRandomInt(stor.param.m + 2, stor.param.m + 3)
            tabRep = [[stor.param.n1, stor.param.t], [1, stor.param.t], [1, stor.param.t]]
            stor.coupleRep.push({ valeur: 1, proba: stor.param.n1 / stor.param.t })
            stor.coupleRep.push({ valeur: -stor.param.m, proba: 1 / stor.param.t })
            stor.coupleRep.push({ valeur: stor.param.nb - stor.param.m, proba: 1 / stor.param.t })
            break
          case 3:
            stor.param.v1 = 0
            stor.param.v2 = stor.param.m + 1
            stor.param.l = '0 ; ' + stor.param.v2
            stor.param.nb = j3pGetRandomInt(stor.param.m + 2, stor.param.m + 4)
            tabRep = [[stor.param.n1 + 1, stor.param.t], [1, stor.param.t], [1, stor.param.t]]
            stor.coupleRep.push({ valeur: 1, proba: (stor.param.n1 + 1) / stor.param.t })
            stor.coupleRep.push({ valeur: -stor.param.m, proba: 1 / stor.param.t })
            stor.coupleRep.push({ valeur: stor.param.nb - stor.param.m, proba: 1 / stor.param.t })
            break
          case 4:
            stor.param.v1 = 0
            stor.param.v2 = stor.param.m
            stor.param.v3 = stor.param.m + 1
            stor.param.l = '0 ; ' + stor.param.v2 + ' ; ' + stor.param.v3
            stor.param.nb = j3pGetRandomInt(stor.param.m + 2, stor.param.m + 3)
            tabRep = [[stor.param.n1 + 1, stor.param.t], [1, stor.param.t], [1, stor.param.t], [1, stor.param.t]]
            stor.coupleRep.push({ valeur: 1, proba: (stor.param.n1 + 1) / stor.param.t })
            stor.coupleRep.push({ valeur: 0, proba: 1 / stor.param.t })
            stor.coupleRep.push({ valeur: -stor.param.m, proba: 1 / stor.param.t })
            stor.coupleRep.push({ valeur: stor.param.nb - stor.param.m, proba: 1 / stor.param.t })
            break
        }
        break
      case 3:
        stor.param.t = 50 * j3pGetRandomInt(10, 16)// nombre de tickets
        stor.param.s1 = 50 * j3pGetRandomInt(7, 9)// gain le plus élevé
        stor.param.s2 = 5 * j3pGetRandomInt(7, 9)// deuxième gain
        stor.param.s3 = j3pGetRandomInt(6, 9)// deuxième gain
        stor.param.n1 = j3pGetRandomInt(4, 7)// Nombre de tickets qui permettent de gagner un tee-shirt
        stor.param.n2 = 2 * j3pGetRandomInt(5, 6)// Nombre de tickets qui permettent de gagner une gourde
        stor.param.m = 5 * j3pGetRandomInt(3, 5)// mise de départ
        tabRep = [[1, stor.param.t], [stor.param.n1, stor.param.t], [stor.param.n2, stor.param.t], [stor.param.t - stor.param.n1 - stor.param.n2 - 1, stor.param.t]]
        stor.coupleRep.push({ valeur: (stor.param.s1 - stor.param.m), proba: 1 / stor.param.t })
        stor.coupleRep.push({ valeur: (stor.param.s2 - stor.param.m), proba: stor.param.n1 / stor.param.t })
        stor.coupleRep.push({ valeur: (stor.param.s3 - stor.param.m), proba: stor.param.n2 / stor.param.t })
        stor.coupleRep.push({
          valeur: -stor.param.m,
          proba: (stor.param.t - stor.param.n1 - stor.param.n2 - 1) / stor.param.t
        })
        break
    }

    stor.mesDonnees = { parent: stor.zoneCons6 }
    stor.mesDonnees.largeur = me.zonesElts.MG.getBoundingClientRect().width * stor.pourc / 100
    stor.mesDonnees.legendes = (ds.texteLegende) ? ds.textes.legendes.split('|') : ['$x_i$', '$P(X=x_i)$']
    stor.mesDonnees.valeurs = ['?', '?']
    stor.mesDonnees.probas = ['-1', '-1']
    stor.mesDonnees.complet = false
    stor.mesDonnees.typeFont = me.styles.petit.enonce
    stor.mesDonnees.restrict = ['\\d,.-', '\\d,./']
    for (let i = 1; i <= 3; i++) {
      j3pAffiche(stor['zoneCons' + i], '', ds.textes['consigne' + i + '_' + stor.quest], stor.param)
    }
    j3pAffiche(stor.zoneCons4, '', ds.textes.consigne4)

    me.logIfDebug('mesDonnees:', stor.mesDonnees, '\ncoupleRep:', stor.coupleRep)
    stor.zoneInput = tabLoiProba(stor.mesDonnees)
    stor.divAjoute = j3pAddElt(stor.zoneCons5, 'div')
    stor.divEnleve = j3pAddElt(stor.zoneCons5, 'div')
    constructionBtnAjoute()
    declarationZoneAValider()
    stor.valeursSaisies = []
    stor.probasSaisies = []

    stor.DonneesRep = { }
    stor.DonneesRep.largeur = stor.mesDonnees.largeur
    stor.DonneesRep.legendes = stor.mesDonnees.legendes.slice()
    stor.DonneesRep.valeurs = []
    stor.DonneesRep.probas = []
    for (let i = 0; i < stor.coupleRep.length; i++) {
      stor.DonneesRep.valeurs.push(stor.coupleRep[i].valeur)
      // JE simplife les fractions de la forme \\frac{tabRep[i][0]}{tabRep[i][1]}
      const pgcdRep = j3pPGCD(tabRep[i][0], tabRep[i][1])
      if (pgcdRep === 1) {
        stor.DonneesRep.probas.push('\\frac{' + tabRep[i][0] + '}{' + tabRep[i][1] + '}')
      } else {
        stor.DonneesRep.probas.push('\\frac{' + tabRep[i][0] + '}{' + tabRep[i][1] + '}=\\frac{' + tabRep[i][0] / pgcdRep + '}{' + tabRep[i][1] / pgcdRep + '}')
      }
    }
    stor.DonneesRep.complet = true
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70

        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc / 100 })
        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        stor.tabQuest = [1, 2, 3]
        stor.tabQuestInit = [...stor.tabQuest]
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        // console.log("mes zones :",stor.fctsValid.zones.inputs)
        const reponse = stor.fctsValid.validationGlobale()
        let valeursDifferentes = true
        let oubliMise = false
        let sommeProba
        let toutesValeursBonnes
        let bonNombreValeurs
        if (reponse.aRepondu) {
        // toutes les zones ont été complétées
          const tabCouple = []
          const tabCoupleOK = []
          const testRep = stor.coupleRep.slice()
          sommeProba = 0
          const bonneValeur = []
          toutesValeursBonnes = true
          for (let i = 0; i < stor.fctsValid.zones.inputs.length / 2; i++) {
          // on cherche les couples valeurs/probas
            tabCouple[i] = {}
            tabCouple[i].valeur = stor.fctsValid.zones.reponseSaisie[2 * i]
            tabCouple[i].proba = j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[2 * i + 1])
            tabCoupleOK.push(false)
            sommeProba += tabCouple[i].proba
            bonneValeur[i] = false
          }
          const tabValeursDifferentes = []
          for (let i = 0; i < tabCouple.length; i++) {
            const maVal = j3pCalculValeur(tabCouple[i].valeur)
            if (tabValeursDifferentes.indexOf(maVal) === -1) {
              tabValeursDifferentes.push(maVal)
            } else {
              valeursDifferentes = false
            }
          }
          // console.log("tabCouple:",tabCouple)
          reponse.aRepondu = valeursDifferentes
          if (reponse.aRepondu) {
            bonNombreValeurs = (testRep.length === tabCouple.length)
            reponse.bonneReponse = (bonNombreValeurs && valeursDifferentes)
            if (reponse.bonneReponse) {
              j3pEmpty(stor.zoneCons5)
              // Je vérifie si l’élève n’a pas oublié les mises
              const testRep2 = stor.coupleRep.slice()
              const oubliTabMise = []
              for (let i = 0; i < tabCouple.length; i++) {
                let k = 0
                while (k < testRep2.length && !tabCoupleOK[i]) {
                  if (Math.abs(j3pCalculValeur(tabCouple[i].valeur) - testRep2[k].valeur - stor.param.m) < Math.pow(10, -12)) {
                    oubliTabMise[i] = true
                    testRep2.splice(k, 1)
                  }
                  k++
                }
              }
              oubliMise = true
              for (let i = 0; i < tabCouple.length; i++) {
                oubliMise = oubliMise && oubliTabMise[i]
              }
              for (let i = 0; i < tabCouple.length; i++) {
                let k = 0
                while (k < testRep.length && !tabCoupleOK[i]) {
                  if (Math.abs(j3pCalculValeur(tabCouple[i].valeur) - testRep[k].valeur) < Math.pow(10, -12)) {
                    let indexBonneValeur = -1
                    for (let j = 0; j < stor.coupleRep.length; j++) {
                      if (Math.abs(stor.coupleRep[j].valeur - j3pCalculValeur(tabCouple[i].valeur)) < Math.pow(10, -12)) {
                        indexBonneValeur = j
                      }
                    }
                    bonneValeur[indexBonneValeur] = true
                    // Dans le cas où la valeur proposée par l’élève est l’une des valeurs attendues, je teste si les proba sont les mêmes
                    tabCoupleOK[i] = (Math.abs(tabCouple[i].proba - j3pCalculValeur(testRep[k].proba)) < Math.pow(10, -12))
                    if (tabCoupleOK[i]) {
                      testRep.splice(k, 1)
                    }
                  }
                  k++
                }
              }
              // Je vérifie alors si tous les couples sont OK et mets les zones à false si ce n’est pas le cas
              for (let i = 0; i < tabCouple.length; i++) {
                toutesValeursBonnes = (toutesValeursBonnes && bonneValeur[i])
                reponse.bonneReponse = (reponse.bonneReponse && tabCoupleOK[i])
                if (!tabCoupleOK[i]) {
                  stor.fctsValid.zones.bonneReponse[2 * i] = false
                  stor.fctsValid.zones.bonneReponse[2 * i + 1] = false
                }
              }
            } else {
              for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
                stor.fctsValid.zones.bonneReponse[i] = false
              }
            }
            for (let i = stor.fctsValid.zones.inputs.length - 1; i >= 0; i--) {
            // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
              stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
            }
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (!valeursDifferentes) {
            msgReponseManquante = ds.textes.comment4
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!bonNombreValeurs) {
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
              } else {
                if (toutesValeursBonnes) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment3
                } else if (oubliMise) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment5
                }
                if (Math.abs(sommeProba - 1) > Math.pow(10, -12)) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
        // Obligatoire
        me.finCorrection()
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
