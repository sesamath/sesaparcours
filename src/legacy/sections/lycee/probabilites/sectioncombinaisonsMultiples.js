import { j3pAddElt, j3pAjouteBouton, j3pFocus, j3pGetRandomInt, j3pNombreBienEcrit, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        février 2020
        Calcul du nombre de choix dans un ensemble mais avec des conditions
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Dénombrements plus complets',
  // on donne les phrases de la consigne
  consigne1_1: 'On dispose de £j jetons numérotés de 1 à £j et on en extrait simultanément £e pour former un paquet.',
  consigne2_1: 'Combien de paquets contenant au moins £{p1} numéros pairs peut-on former&nbsp;?',
  consigne3_1: 'Combien de paquets contenant exactement £e numéros de même parité peut-on former&nbsp;?',
  consigne1_2: 'Une entreprise souhaite envoyer un courrier publicitaire à £c employés choisis au hasard d’un cabinet médical. Dans ce cabinet de £p employés, £m sont des médecins.',
  consigne2_2: 'Combien y a-t-il de façons de choisir les £c employés pour qu’au moins £{n1} d’entre eux soient des médecins&nbsp;?',
  consigne3_2: 'Combien y a-t-il de façons de choisir les £c employés pour qu’exactement £n d’entre eux soient des médecins&nbsp;?',
  // Le suivant est inspiré d’un sujet de bac
  consigne1_3: 'Pour réaliser des étiquettes de publipostage, une entreprise utilise une banque de données de £a adresses dont £e sont erronées, les autres étant exactes. On prélève au hasard £p étiquettes parmi les £a.',
  consigne2_3: 'Combien y a-t-il de tirages possibles pour qu’au moins £{q1} d’entre elles soient avec des adresses exactes&nbsp;?',
  consigne3_3: 'Combien y a-t-il de tirages possibles pour qu’exactement £q d’entre elles soient avec des adresses erronées&nbsp;?',
  consigne1_4: 'Un sac contient £b boules blanches, £n boules noires et £q boules rouges. On tire simultanément £t boules de l’urne.',
  consigne2_4: 'Combien y a-t-il de façons de tirer au moins £{n2} boules noires&nbsp;?',
  consigne3_4: 'Combien y a-t-il de façons de tirer £c boules de la même couleur&nbsp;?',
  consigne1_5: 'Parmi £p pièces électroniques, £{p1} proviennent d’un premier fournisseur F1, £{p2} d’un deuxième fournisseur F2 et les autres d’un troisième fournisseur F3.',
  consigne2_5: 'Combien y a-t-il de façons de choisir au hasard £c pièces de ce lot pour qu’on en ait au moins £{c2} de F1&nbsp;?',
  consigne3_5: 'Combien y a-t-il de façons de choisir au hasard £c pièces de ce lot pour qu’elles viennent toutes du même fournisseur&nbsp;?',
  consigne3: 'Réponse : &1&.',
  // remarque: "On peut se contenter d’écrire le calcul sans donner le résultat numérique.",
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: '',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Les tirages sont simultanés. Parmi les £e jetons extraits, il en faut au moins £{p1} avec un numéro pair. Cela revient alors à choisir £{p1} numéros parmi les £{p2} numéros pairs puis calcule[£e-£{p1}] parmi les impairs, ou bien calcule[£{p1}+1] numéros parmi les pairs et calcule[£e-£{p1}-1] parmi les impairs, ou bien £e numéros parmi les pairs.', //
  corr2_1: 'En tout, le nombre de paquets possibles vaut&nbsp;:<br/>$£{calcul}=£r$.',
  corr3_1: 'Les tirages sont simultanés. On peut choisir £e numéros pairs, le nombre de possibilités est $\\binom{£{p2}}{£e}$ mais on peut également choisir £e numéros impairs et dans ce cas, il y a $\\binom{calcule[£j-£{p2}]}{£e}$ possibilités.',
  corr4_1: 'Le nombre de paquets possibles est alors égal à $\\binom{£{p2}}{£e}+ \\binom{calcule[£j-£{p2}]}{£e}=£r$.',
  corr1_2: 'Les choix sont simultanés. Parmi les £c employés choisis au hasard, il faut au moins £{n1} médecins. Cela revient alors à choisir £{n1} médecins parmi les £m puis calcule[£c-£{n1}] employés parmi les calcule[£p-£m] autres employés, ou bien calcule[£{n1}+1] médecins parmi les £m puis calcule[£c-£{n1}-1] autre employé, ou bien calcule[£{n1}+2] médecins parmi les £m.',
  corr2_2: 'En tout, le nombre de choix possibles est donc &nbsp;:<br/>$£{calcul}=£r$.',
  corr3_2: 'Les choix sont simultanés. On doit choisir £n médecins parmi les £m, le nombre de possibilités est $\\binom{£m}{£n}$. Pour chacun de ces choix, on doit également choisir $calcule[£c-£n]$ employés non médecins parmi les $calcule[£p-£m]$&nbsp;; il y a $\\binom{calcule[£p-£m]}{calcule[£c-£n]}$ possibilités.',
  corr4_2: 'Le nombre de choix possibles est alors égal à $\\binom{£m}{£n}\\times \\binom{calcule[£p-£m]}{calcule[£c-£n]}=£r$.',
  corr1_3: 'Les choix sont simultanés. Parmi les £p étiquettes choisies au hasard, il faut au moins £{q1} étiquettes avec des adresses exactes. Cela revient alors à choisir £{q1} étiquettes avec des adresses exactes parmi les calcule[£a-£e] puis calcule[£p-£{q1}] étiquettes parmi les £e autres, ou bien calcule[£{q1}+1] étiquettes avec des adresses exactes et une parmi les autres, ou bien calcule[£{q1}+2] étiquettes avec des adresses exactes.',
  corr2_3: 'En tout, le nombre de choix possibles est donc&nbsp;:<br/>$£{calcul}=£r$.',
  corr3_3: 'Les choix sont simultanés. On doit choisir £q étiquettes avec des adresses erronées parmi les £e, le nombre de possibilités est $\\binom{£e}{£q}$. Pour chacun de ces choix, on doit également choisir $calcule[£p-£q]$ étiquettes avec des adresses exactes parmi les $calcule[£a-£e]$&nbsp;; il y a $\\binom{calcule[£a-£e]}{calcule[£p-£q]}$ possibilités.',
  corr4_3: 'Le nombre de choix possibles est alors égal à $\\binom{£e}{£q}\\times \\binom{calcule[£a-£e]}{calcule[£p-£q]}=£r$.',
  corr1_4: 'Les tirages sont simultanés. Parmi les £t boules extraites, il en faut au moins £{n2} qui soient noires. Cela revient alors à choisir £{n2} boules parmi les £n boules noires puis calcule[£t-£{n2}] boules parmi les calcule[£b+£q] autres, ou bien calcule[£{n2}+1] boules parmi les noires et 1 parmi les autres, ou bien calcule[£{n2}+2] boules parmi les noires.',
  corr2_4: 'En tout, le nombre de tirages possibles vaut&nbsp;:<br/>$£{calcul}=£r$.',
  corr3_4: 'Les tirages sont simultanés. On peut choisir £c boules blanches, le nombre de possibilités est $\\binom{£b}{£c}$ mais on peut également choisir £c boules noires et dans ce cas, il y a $\\binom{£n}{£c}$ possibilités ou encore £c boules rouges et dans ce cas, il y a $\\binom{£q}{£c}$ possibilités.',
  corr4_4: 'Le nombre de tirages possibles est alors égal à $\\binom{£b}{£c}+\\binom{£n}{£c}+\\binom{£q}{£c}=£r$.',
  // corr1_5: 'Les choix sont simultanés. Parmi les £p pièces extraites, il en faut au moins £{c2} pièces de F1. Cela revient alors à choisir £{c2} pièces parmi les £{p1} pièces de F1 puis calcule[£c-£{c2}] pièces parmi les calcule[£p-£{c2}] pièces restants.',
  // corr2_5: 'En tout, il y a donc $\\binom{£{p1}}{£{c2}}\\times \\binom{calcule[£p-£{c2}]}{calcule[£c-£{c2}]}=£r$ choix possibles.',

  corr1_5: 'Les choix sont simultanés. Parmi les £p pièces extraites, il en faut au moins £{c2} pièces de F1. Cela revient alors à choisir £{c2} pièces parmi les £{p1} pièces de F1 puis calcule[£c-£{c2}] pièces parmi les calcule[£p-£{p1}] pièces restants, ou bien calcule[£{c2}+1] pièces parmi les pièces de F1 puis calcule[£c-£{c2}-1] parmi les autres, ou bien calcule[£{c2}+2] pièces parmi les pièces de F1.',
  corr2_5: 'En tout, le nombre de choix possibles vaut&nbsp;:<br/>$£{calcul}=£r$.',

  corr3_5: 'Les choix sont simultanés. On peut choisir £c pièces de F1, le nombre de possibilités est $\\binom{£{p1}}{£c}$ mais on peut également choisir £c pièces de F2 et dans ce cas, il y a $\\binom{£{p2}}{£c}$ possibilités ou encore £c pièces de F3 et dans ce cas, il y a $\\binom{calcule[£p-£{p1}-£{p2}]}{£c}$ possibilités.',
  corr4_5: 'Le nombre de tirages possibles est alors égal à $\\binom{£{p1}}{£c}+\\binom{£{p2}}{£c}+\\binom{calcule[£p-£{p1}-£{p2}]}{£c}=£r$.'
}
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pAffiche(stor.zoneExpli1, '', textes['corr' + (stor.choixQuest * 2 + 1) + '_' + stor.quest], stor.objCons)
    j3pAffiche(stor.zoneExpli2, '', textes['corr' + (stor.choixQuest * 2 + 2) + '_' + stor.quest], stor.objCons)
  }

  function combinaison (n, p) {
    // Cette fonction calcule C_n^p sans utiliser les factorielles pour gagner un peu de temps
    if (n === p || p === 0) return 1
    let res = n
    let i = n - 1
    while (i > Math.max(p, n - p)) {
      res *= i
      i -= 1
    }
    i = 2
    while (i <= Math.min(p, n - p)) {
      res = Math.round(res / i)
      i += 1
    }
    // console.log("combinaison(",n,",",p,")=",res)
    return res
  }

  function enonceInitFirst () {
    ds = me.donneesSection
    me.surcharge({ nbetapes: 2 })
    // Le paramètre définit la largeur relative de la première colonne
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
    stor.tabQuest = [1, 2, 3, 4, 5]
    stor.tabQuestInit = [...stor.tabQuest]
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    if (me.questionCourante % ds.nbetapes === 1) {
      if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
      const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
      stor.quest = stor.tabQuest[pioche]
      stor.tabQuest.splice(pioche, 1)
      stor.objCons = {}
      switch (stor.quest) {
        case 1:
          stor.objCons.j = j3pGetRandomInt(15, 20)
          stor.objCons.e = j3pGetRandomInt(5, 6)
          stor.objCons.p2 = Math.floor(stor.objCons.j / 2)
          stor.objCons.p = j3pGetRandomInt(2, Math.min(stor.objCons.p2 - 1, stor.objCons.e - 2))
          stor.objCons.p1 = stor.objCons.e - 2
          stor.objCons.calcul = '\\binom{' + stor.objCons.p2 + '}{' + stor.objCons.p1 + '}\\times \\binom{' + (stor.objCons.j - stor.objCons.p2) + '}{' + (stor.objCons.e - stor.objCons.p1) + '}'
          stor.objCons.calcul += '+\\binom{' + stor.objCons.p2 + '}{' + (stor.objCons.p1 + 1) + '}\\times ' + (stor.objCons.j - stor.objCons.p2)
          stor.objCons.calcul += '+\\binom{' + stor.objCons.p2 + '}{' + stor.objCons.e + '}'
          stor.laReponse1 = combinaison(stor.objCons.p2, stor.objCons.p1) * combinaison(stor.objCons.j - stor.objCons.p2, stor.objCons.e - stor.objCons.p1)
          stor.laReponse1 += combinaison(stor.objCons.p2, stor.objCons.p1 + 1) * (stor.objCons.j - stor.objCons.p2)
          stor.laReponse1 += combinaison(stor.objCons.p2, stor.objCons.e)
          stor.laReponse2 = combinaison(stor.objCons.p2, stor.objCons.e) + combinaison(stor.objCons.j - stor.objCons.p2, stor.objCons.e)
          break
        case 2:
          stor.objCons.p = j3pGetRandomInt(25, 32)
          do {
            stor.objCons.c = j3pGetRandomInt(7, 11)
            stor.objCons.m = j3pGetRandomInt(7, 11)
          } while (stor.objCons.c >= stor.objCons.m - 2)
          stor.objCons.n = j3pGetRandomInt(2, Math.min(stor.objCons.c - 1, stor.objCons.m - 2))
          stor.objCons.n1 = stor.objCons.c - 2
          stor.objCons.calcul = '\\binom{' + stor.objCons.m + '}{' + stor.objCons.n1 + '}\\times \\binom{' + (stor.objCons.p - stor.objCons.m) + '}{' + (stor.objCons.c - stor.objCons.n1) + '}'
          stor.objCons.calcul += '+\\binom{' + stor.objCons.m + '}{' + (stor.objCons.n1 + 1) + '}\\times ' + (stor.objCons.p - stor.objCons.m)
          stor.objCons.calcul += '+\\binom{' + stor.objCons.m + '}{' + stor.objCons.c + '}'
          stor.laReponse1 = combinaison(stor.objCons.m, stor.objCons.n1) * combinaison(stor.objCons.p - stor.objCons.m, stor.objCons.c - stor.objCons.n1)
          stor.laReponse1 += combinaison(stor.objCons.m, stor.objCons.n1 + 1) * (stor.objCons.p - stor.objCons.m)
          stor.laReponse1 += combinaison(stor.objCons.m, stor.objCons.c)
          stor.laReponse2 = combinaison(stor.objCons.m, stor.objCons.n) * combinaison(stor.objCons.p - stor.objCons.m, stor.objCons.c - stor.objCons.n)
          break
        case 3:
          stor.objCons.a = j3pGetRandomInt(90, 110)
          stor.objCons.e = j3pGetRandomInt(15, 20)
          stor.objCons.p = j3pGetRandomInt(4, 5)
          stor.objCons.q = j3pGetRandomInt(2, stor.objCons.p - 2)
          stor.objCons.q1 = stor.objCons.p - 2
          stor.objCons.calcul = '\\binom{' + (stor.objCons.a - stor.objCons.e) + '}{' + stor.objCons.q1 + '}\\times \\binom{' + stor.objCons.e + '}{' + (stor.objCons.p - stor.objCons.q1) + '}'
          stor.objCons.calcul += '+\\binom{' + (stor.objCons.a - stor.objCons.e) + '}{' + (stor.objCons.q1 + 1) + '}\\times ' + stor.objCons.e
          stor.objCons.calcul += '+\\binom{' + (stor.objCons.a - stor.objCons.e) + '}{' + stor.objCons.p + '}'
          stor.laReponse1 = combinaison((stor.objCons.a - stor.objCons.e), stor.objCons.q1) * combinaison(stor.objCons.e, stor.objCons.p - stor.objCons.q1)
          stor.laReponse1 += combinaison((stor.objCons.a - stor.objCons.e), stor.objCons.q1 + 1) * stor.objCons.e
          stor.laReponse1 += combinaison((stor.objCons.a - stor.objCons.e), stor.objCons.p)
          stor.laReponse2 = combinaison(stor.objCons.e, stor.objCons.q) * combinaison(stor.objCons.a - stor.objCons.e, stor.objCons.p - stor.objCons.q)
          break
        case 4:
          do {
            stor.objCons.b = j3pGetRandomInt(5, 8)
            do {
              stor.objCons.n = j3pGetRandomInt(5, 8)
            } while (stor.objCons.n === stor.objCons.b)
            do {
              stor.objCons.q = j3pGetRandomInt(5, 8)
            } while ((stor.objCons.q === stor.objCons.b) || (stor.objCons.q === stor.objCons.n))
            stor.objCons.t1 = j3pGetRandomInt(4, 5)
          } while (stor.objCons.t1 >= stor.objCons.n - 1)
          stor.objCons.n2 = stor.objCons.t1 - 2
          stor.objCons.c = j3pGetRandomInt(2, 4)
          stor.objCons.calcul = '\\binom{' + stor.objCons.n + '}{' + stor.objCons.n2 + '}\\times \\binom{' + (stor.objCons.b + stor.objCons.q) + '}{' + (stor.objCons.t1 - stor.objCons.n2) + '}'
          stor.objCons.calcul += '+\\binom{' + stor.objCons.n + '}{' + (stor.objCons.n2 + 1) + '}\\times ' + (stor.objCons.b + stor.objCons.q)
          stor.objCons.calcul += '+\\binom{' + stor.objCons.n + '}{' + stor.objCons.t1 + '}'
          stor.laReponse1 = combinaison(stor.objCons.n, stor.objCons.n2) * combinaison((stor.objCons.b + stor.objCons.q), stor.objCons.t1 - stor.objCons.n2)
          stor.laReponse1 += combinaison(stor.objCons.n, stor.objCons.n2 + 1) * (stor.objCons.b + stor.objCons.q)
          stor.laReponse1 += combinaison(stor.objCons.n, stor.objCons.t1)
          stor.laReponse2 = combinaison(stor.objCons.b, stor.objCons.c) + combinaison(stor.objCons.n, stor.objCons.c) + combinaison(stor.objCons.q, stor.objCons.c)
          break
        case 5:
          stor.objCons.p = j3pGetRandomInt(25, 35)
          stor.objCons.p1 = j3pGetRandomInt(10, 12)
          stor.objCons.p2 = j3pGetRandomInt(7, 9)
          stor.objCons.c = j3pGetRandomInt(4, 6)
          stor.objCons.c2 = stor.objCons.c - 2
          stor.objCons.calcul = '\\binom{' + stor.objCons.p1 + '}{' + stor.objCons.c2 + '}\\times \\binom{' + (stor.objCons.p - stor.objCons.p1) + '}{' + (stor.objCons.c - stor.objCons.c2) + '}'
          stor.objCons.calcul += '+\\binom{' + stor.objCons.p1 + '}{' + (stor.objCons.c2 + 1) + '}\\times ' + (stor.objCons.p - stor.objCons.p1)
          stor.objCons.calcul += '+\\binom{' + stor.objCons.p1 + '}{' + stor.objCons.c + '}'
          stor.laReponse1 = combinaison(stor.objCons.p1, stor.objCons.c2) * combinaison((stor.objCons.p - stor.objCons.p1), stor.objCons.c - stor.objCons.c2)
          stor.laReponse1 += combinaison(stor.objCons.p1, stor.objCons.c2 + 1) * (stor.objCons.p - stor.objCons.p1)
          stor.laReponse1 += combinaison(stor.objCons.p1, stor.objCons.c)
          stor.laReponse2 = combinaison(stor.objCons.p1, stor.objCons.c) + combinaison(stor.objCons.p2, stor.objCons.c) + combinaison(stor.objCons.p - stor.objCons.p1 - stor.objCons.p2, stor.objCons.c)
          break
      }
      // stor.objCons.rep est un tableau contenant les réponses aux deux questions
      stor.objCons.rep = [stor.laReponse1, stor.laReponse2]
      stor.choixQuest = 1
    } else {
      stor.choixQuest = (stor.choixQuest + 1) % 2
    }
    stor.objCons.r = (stor.objCons.rep[stor.choixQuest] >= 1000) ? j3pNombreBienEcrit(stor.objCons.rep[stor.choixQuest]).replace(/\s/g, '\\text{ }') : stor.objCons.rep[stor.choixQuest]
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    if (stor.quest === 4) {
      stor.objCons.t = (stor.choixQuest === 0) ? stor.objCons.t1 : stor.objCons.c
    }

    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes['consigne1_' + stor.quest], stor.objCons)
    j3pAffiche(stor.zoneCons2, '', textes['consigne' + (stor.choixQuest + 2) + '_' + stor.quest], stor.objCons)
    const elt = j3pAffiche(stor.zoneCons3, '', textes.consigne3, {
      inputmq1: { texte: '' }
    })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.')
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [stor.objCons.rep[stor.choixQuest]]
    me.logIfDebug('laReponse:', stor.zoneInput.reponse)
    // Par défaut, le focus ira à la dernière zone de saisie créée. Donc ici on le remet à la première
    j3pFocus(stor.zoneInput)

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')

    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    // Paramétrage de la validation

    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: [stor.zoneInput.id]
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :

        if ((!reponse.aRepondu) && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
          if (reponse.bonneReponse) {
          // Bonne réponse
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
