import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pShuffle, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since janvier 2022
 * @fileOverview Cette section demande d’écrire un événements en fonction de deux autres événements en utilisant l’intersection, l’union ou le contraire
 */

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['simple', true, 'boolean', 'Lorsque ce paramètre vaut Vrai, on se contente d’événements assez simples. Sinon, on mélange les contraires avec l’union ou l’intersection.']
  ]
}

/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {

    // Nombre de répétitions de l’exercice
    simple: true,
    nbchances: 1,
    nbetapes: 1,
    indication: '',
    textes: {
      titre_exo: 'Langage des événements',
      consigne1: 'On dispose d’une urne contenant des boules de différentes couleurs et portant chacune un numéro.',
      consigne2: 'On tire au hasard une boule de cette urne et on définit les événements suivants&nbsp;:',
      evt1: '$£A$&nbsp;: "la boule tirée est rouge";',
      evt2: '$£B$&nbsp;: "la boule tirée est noire";',
      evt3: '$£C$&nbsp;: "la boule tirée porte un numéro pair".',
      consigne3: 'Exprimer en fonction de ces trois événements l’événement suivant&nbsp;:',
      quest1: '"la boule tirée n’est pas noire"',
      quest2: '"la boule tirée porte un numéro impair"',
      quest3: '"la boule tirée porte un numéro pair ou est de couleur noire"',
      quest4: '"la boule tirée porte un numéro pair et est de couleur rouge"',
      quest5: '"la boule tirée est rouge ou noire"',
      quest6: '"la boule tirée n’est pas rouge ou porte un numéro pair"',
      quest7: '"la boule tirée n’est ni rouge ni noire"',
      quest8: '"la boule tirée est rouge et porte un numéro impair"',
      quest9: '"la boule tirée n’est pas noire ou porte un numéro impair"',
      quest10: '"la boule tirée est noire ou porte un numéro impair"',
      indic: 'Tu n’as le droit qu’à une seule tentative pour répondre.',
      comment: '$£r$ est une mauvaise réponse&nbsp;!',
      corr: 'Voici la réponse attendue&nbsp;: $£r$.',
      corr2: 'Voici deux réponses possibles : $£r$ ou $£s$.',
      corr3: '$£r$ est une réponse possible.',
      corr4: '$£r$ est une bonne réponse.'
    }
  }
} // getDonnees

// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  me.donneesSection.nbrepetitions = (me.donneesSection.simple) ? 3 : 4
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(me.donneesSection.textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  const stor = me.storage
  const ds = me.donneesSection

  // On établit la liste des sujets qui peuvent être donnés au fur et à mesure
  // Dans cette section, on impose le nombre de répétitions à 3
  if (ds.simple) stor.listeSujetsInit = [j3pGetRandomInt(1, 2), 4, 3 + 2 * j3pGetRandomInt(0, 1)]
  else stor.listeSujetsInit = [6 + 4 * j3pGetRandomInt(0, 1), 7, 8, 9]
  stor.listeSujets = j3pShuffle(stor.listeSujetsInit)
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  const conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  // Choix du nom des événements
  stor.objDonnees = { A: 'R', B: 'N', C: 'P' }
  // on affiche l’énoncé
  for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(conteneur, 'p')
  j3pAddContent(stor.zoneCons1, ds.textes.consigne1)
  j3pAddContent(stor.zoneCons2, ds.textes.consigne2)
  const listesEvts = j3pAddElt(conteneur, 'ul')
  j3pStyle(listesEvts, { margin: '5px 0px' })
  for (let i = 1; i <= 3; i++) {
    const li = j3pAddElt(listesEvts, 'li')
    j3pAffiche(li, '', ds.textes['evt' + i], stor.objDonnees)
  }
  for (let i = 3; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(conteneur, 'p')
  j3pAddContent(stor.zoneCons3, ds.textes.consigne3)
  stor.choixQuest = stor.listeSujets[me.questionCourante - 1]
  j3pAddContent(stor.zoneCons4, ds.textes['quest' + stor.choixQuest])
  if (ds.nbchances === 1) j3pAddElt(conteneur, 'p', ds.textes.indic, { style: { fontStyle: 'italic', fontSize: '0.9em' } })
  stor.zoneRep = j3pAddElt(conteneur, 'div', '', { style: { color: me.styles.cfaux } }) // Il servira à afficher une mauvaise réponse de l’élève
  stor.zoneCons5 = j3pAddElt(conteneur, 'p')
  const elt = j3pAffiche(stor.zoneCons5, '', '&1&', { inputmq1: { texte: '' } })
  stor.zoneInput = elt.inputmqList[0]
  // je crée la chaine de caractère contenant les événements de l’énoncé pour les autoriser dans mqRestriction
  let evts = ''
  Object.entries(stor.objDonnees).forEach(([prop, value]) => {
    evts += value + value.toLowerCase()
  })
  mqRestriction(stor.zoneInput, evts, { commandes: ['barre', 'union', 'inter'] })
  stor.zoneInput.addEventListener('input', function () {
    const valeur = $(this).mathquill('latex')
    // Je récupère tout ce qui est overline, cap et cup
    let valSansOverline = valeur.replace(/cup/g, '§§§')
    valSansOverline = valSansOverline.replace(/cap/g, ':::')
    valSansOverline = valSansOverline.replace(/overline/g, '!!!')
    valSansOverline = valSansOverline.toUpperCase()
    valSansOverline = valSansOverline.replace(/!!!/g, 'overline')
    valSansOverline = valSansOverline.replace(/:::/g, 'cap')
    valSansOverline = valSansOverline.replace(/§§§/g, 'cup')
    $(this).mathquill('latex', valSansOverline)
  })
  stor.laPalette = j3pAddElt(conteneur, 'div', '', { style: { paddingTop: '10px' } })
  j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['barre', 'union', 'inter'] })
  stor.zoneInput.typeReponse = ['texte']
  stor.zoneInput.reponse = []
  if (stor.choixQuest === 1) {
    stor.zoneInput.reponse.push('\\overline{' + stor.objDonnees.B + '}')
  } else if (stor.choixQuest === 2) {
    stor.zoneInput.reponse.push('\\overline{' + stor.objDonnees.C + '}')
  } else if (stor.choixQuest === 3) {
    stor.zoneInput.reponse.push(stor.objDonnees.C + '\\cup ' + stor.objDonnees.B, stor.objDonnees.B + '\\cup ' + stor.objDonnees.C)
  } else if (stor.choixQuest === 4) {
    stor.zoneInput.reponse.push(stor.objDonnees.C + '\\cap ' + stor.objDonnees.A, stor.objDonnees.A + '\\cap ' + stor.objDonnees.C)
  } else if (stor.choixQuest === 5) {
    stor.zoneInput.reponse.push(stor.objDonnees.A + '\\cup ' + stor.objDonnees.B, stor.objDonnees.B + '\\cup ' + stor.objDonnees.A)
  } else if (stor.choixQuest === 6) {
    stor.zoneInput.reponse.push('\\overline{' + stor.objDonnees.A + '}' + '\\cup ' + stor.objDonnees.C, stor.objDonnees.C + '\\cup\\overline{' + stor.objDonnees.A + '}', '\\overline{' + stor.objDonnees.A + '\\cap\\overline{' + stor.objDonnees.C + '}}', '\\overline{\\overline{' + stor.objDonnees.C + '}\\cap ' + stor.objDonnees.A + '}')
  } else if (stor.choixQuest === 7) {
    stor.zoneInput.reponse.push('\\overline{' + stor.objDonnees.A + '}' + '\\cap\\overline{' + stor.objDonnees.B + '}', '\\overline{' + stor.objDonnees.B + '}' + '\\cap\\overline{' + stor.objDonnees.A + '}', '\\overline{' + stor.objDonnees.A + '\\cup ' + stor.objDonnees.B + '}', '\\overline{' + stor.objDonnees.B + '\\cup ' + stor.objDonnees.A + '}')
  } else if (stor.choixQuest === 8) {
    stor.zoneInput.reponse.push(stor.objDonnees.A + '\\cap\\overline{' + stor.objDonnees.C + '}', '\\overline{' + stor.objDonnees.C + '}' + '\\cap ' + stor.objDonnees.A, '\\overline{\\overline{' + stor.objDonnees.A + '}\\cup ' + stor.objDonnees.C + '}', '\\overline{' + stor.objDonnees.C + '\\cup\\overline{' + stor.objDonnees.A + '}}')
  } else if (stor.choixQuest === 9) {
    stor.zoneInput.reponse.push('\\overline{' + stor.objDonnees.B + '}' + '\\cup\\overline{' + stor.objDonnees.C + '}', '\\overline{' + stor.objDonnees.C + '}' + '\\cup\\overline{' + stor.objDonnees.B + '}', '\\overline{' + stor.objDonnees.B + '\\cap ' + stor.objDonnees.C + '}', '\\overline{' + stor.objDonnees.C + '\\cap ' + stor.objDonnees.B + '}')
  } else if (stor.choixQuest === 10) {
    stor.zoneInput.reponse.push(stor.objDonnees.B + '\\cup\\overline{' + stor.objDonnees.C + '}', '\\overline{' + stor.objDonnees.C + '}' + '\\cup ' + stor.objDonnees.B, '\\overline{\\overline{' + stor.objDonnees.B + '}\\cap ' + stor.objDonnees.C + '}', '\\overline{' + stor.objDonnees.C + '\\cap\\overline{' + stor.objDonnees.B + '}}')
  }
  j3pFocus(stor.zoneInput)
  stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.zoneInput.id] })
  me.finEnonce()
}
function simplifieEvt (evtTxt) {
  // pour des esprits un peu tordus, je dois aussi vérifier si l’événement n’est pas de la forme \\overline{\\overline{...}}
  let monEvt = evtTxt
  while ((monEvt.indexOf('\\overline{\\overline{') === 0) && (monEvt.lastIndexOf('}}') === monEvt.length - 2)) {
    monEvt = monEvt.substring(20, monEvt.length - 2)
  }
  return monEvt
}
function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  j3pDetruit(stor.laPalette)
  // on affiche la correction dans la zone xxx
  j3pEmpty(stor.zoneCons5)
  if (stor.fctsValid.zones.bonneReponse[0]) {
    j3pStyle(stor.zoneCons5, { color: me.styles.cbien })
    const laConsigne = ([stor.zoneInput.reponse[0], stor.zoneInput.reponse[1]].includes(stor.laRepEleve)) ? ds.textes.corr4 : ds.textes.corr3
    j3pAffiche(stor.zoneCons5, '', laConsigne, { r: stor.laRepEleve })
  } else {
    j3pStyle(stor.zoneCons5, { color: me.styles.cfaux })
    j3pAffiche(stor.zoneCons5, '', ds.textes.comment, { r: stor.laRepEleve })
    const zoneExpli = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, {
      padding: '10px',
      color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color
    })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'p')
    if (ds.simple) j3pAffiche(zoneExpli1, '', ds.textes.corr, { r: stor.zoneInput.reponse[0] })
    else j3pAffiche(zoneExpli1, '', ds.textes.corr2, { r: stor.zoneInput.reponse[0], s: stor.zoneInput.reponse[2] })
  }
  $('.mq-overline').each(function () {
    $(this).css('border-top', '1px solid ' + $(this).css('color'))
    // je parcours l’ensemble des overline et je leur donne la couleur dans laquelle se trouve le texte
  })
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  // Si l’élève s’est amusé à me mettre du contraire de contraire, je lui simplifie son expression
  $(stor.zoneInput).mathquill('latex', simplifieEvt($(stor.zoneInput).mathquill('latex')))
  const reponse = stor.fctsValid.validationGlobale()
  stor.laRepEleve = stor.fctsValid.zones.reponseSaisie[0]
  if (reponse.aRepondu) return (reponse.bonneReponse) ? 1 : 0
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(me.zonesElts.MD)
  const divCorrection = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
  // pas de réponse sans limite de temps

  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(divCorrection)
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    const repEleve = j3pAddElt(stor.zoneRep, 'p')
    j3pAffiche(repEleve, '', ds.textes.comment, { r: stor.laRepEleve })
    me.reponseKo(divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(divCorrection)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(divCorrection, 'br')
        me.reponseKo(divCorrection, null, true)
      }
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
