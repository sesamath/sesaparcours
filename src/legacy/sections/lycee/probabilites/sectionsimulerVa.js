import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pShuffle, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomBool, j3pGetRandomInt, j3pStyle, j3pValeurde, j3pGetNewId } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { tabLoiProba } from 'src/legacy/outils/tableauxProba/tabLoiProba'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juillet 2019
        Dans cette section, on demande de construire la loi de probabilité d’une variable aléatoire définie par un prog en python
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['texteLegende', false, 'boolean', 'Par défaut, on écrit x_i et P(X=x_i) dans la légende de la loi, mais on peut remplacer par "Valeurs de X" et "Probabilités" lorsque ce paramètre vaudra true.']
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const stor = this.storage
  const me = this
  let ds = me.donneesSection

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    try {
      j3pDetruit(stor.zoneCons3)
    } catch (e) {
      console.warn(e) // pas normal mais pas grave
    }
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3, 4].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    const objVal = {}
    for (let i = 1; i <= stor.coupleRep.length; i++) {
      objVal['v' + i] = stor.coupleRep[i - 1].valeur
    }
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, objVal)
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2)
    stor.DonneesRep.parent = stor.zoneExpli3
    stor.DonneesRep.typeFont = me.styles.toutpetit.correction
    tabLoiProba(stor.DonneesRep)
    j3pAffiche(stor.zoneExpli4, '', ds.textes.corr3)
  }

  function constructionBtnAjoute () {
    const ajoute = j3pAddElt(stor.divAjoute, 'div')
    j3pAffiche(ajoute, '', ds.textes.phrase1)
    const btn1 = j3pGetNewId('bouton1')
    j3pAjouteBouton(ajoute, btn1, 'MepBoutons2', '+', actionBtnPlus)
  }

  function constructionBtnEnleve () {
    const enleve = j3pAddElt(stor.divEnleve, 'div')
    j3pAffiche(enleve, '', ds.textes.phrase2)
    const btn2 = j3pGetNewId('bouton2')
    j3pAjouteBouton(enleve, btn2, 'MepBoutons2', '-', actionBtnMoins)
  }

  function actionBtnPlus () {
    // c’est fonction sera appelée pour recréer le tableau de signe avec une valeur de plus dans la première ligne
    for (let k = 0; k < stor.mesDonnees.valeurs.length; k++) {
      // valeur de x déjà saisie (et donc à recopier par la suite)
      stor.valeursSaisies[k] = j3pValeurde(stor.zoneInput[k * 2])
      stor.mesDonnees.valeurs[k] = (stor.valeursSaisies[k] === '') ? '?' : stor.valeursSaisies[k]
      // valeur de proba déjà saisie (et donc à recopier)
      stor.probasSaisies[k] = $(stor.zoneInput[k * 2 + 1]).mathquill('latex')
      stor.mesDonnees.probas[k] = (stor.probasSaisies[k] === '') ? '-1' : stor.probasSaisies[k]
    }
    stor.mesDonnees.valeurs.push('?')
    stor.mesDonnees.probas.push('-1')
    if (stor.mesDonnees.valeurs.length === 6) j3pEmpty(stor.divAjoute)
    if (stor.mesDonnees.valeurs.length === 3) {
      // c’est que le btnEnleve n’était pas encore construit, donc on le fait.
      constructionBtnEnleve()
    }
    // on reconstruit ensuite le tableau
    j3pEmpty(stor.mesDonnees.parent)
    stor.zoneInput = tabLoiProba(stor.mesDonnees)
    declarationZoneAValider()
  }

  function actionBtnMoins () {
    // c’est fonction sera appelée pour recréer le tableau de signe avec une valeur de moins dans la première ligne
    for (let k = 0; k < stor.mesDonnees.valeurs.length; k++) {
      // valeur de x déjà saisie (et donc à recopier par la suite)
      stor.valeursSaisies[k] = j3pValeurde(stor.zoneInput[k * 2])
      stor.mesDonnees.valeurs[k] = (stor.valeursSaisies[k] === '') ? '?' : stor.valeursSaisies[k]
      // valeur de proba déjà saisie (et donc à recopier)
      stor.probasSaisies[k] = $(stor.zoneInput[k * 2 + 1]).mathquill('latex')
      stor.mesDonnees.probas[k] = (stor.probasSaisies[k] === '') ? '-1' : stor.probasSaisies[k]
    }
    stor.mesDonnees.valeurs.pop()
    stor.mesDonnees.probas.pop()
    if (stor.mesDonnees.valeurs.length === 2) j3pEmpty(stor.divEnleve)
    if (stor.mesDonnees.valeurs.length === 5) {
      // c’est que le btnAjoute n’était pas encore construit, donc on le fait.
      constructionBtnAjoute()
    }
    // on reconstruit ensuite le tableau
    j3pEmpty(stor.mesDonnees.parent)
    stor.zoneInput = tabLoiProba(stor.mesDonnees)
    declarationZoneAValider()
  }

  function declarationZoneAValider () {
    // on récupère la liste avec le nom de toutes les zones de saisie
    // nom du div accueillant le tableau
    const mesZonesSaisie = []
    for (let i = 0; i < stor.zoneInput.length / 2; i++) {
      mesZonesSaisie.push(stor.zoneInput[i * 2].id)
      mesZonesSaisie.push(stor.zoneInput[i * 2 + 1].id)
    }
    j3pFocus(stor.zoneInput[0])
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesZonesSaisie })
  }

  function getDonnees () {
    return {
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 2,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’élève pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      texteLegende: false,
      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par ds.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: ds.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Simuler une variable aléatoire',
        // on donne les phrases de la consigne
        consigne1: 'Le programme ci-contre permet d’obtenir au hasard une valeur prise par une variable aléatoire $X$.',
        consigne2: 'Compléter ci-dessous la loi de probabilité de cette variable aléatoire.',
        legendes: 'Valeurs de $X$|probabilités',
        phrase1: 'Pour ajouter une valeur de $X$ : ',
        phrase2: 'Pour retirer une valeur de $X$ : ',

        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Le nombre de valeurs proposé n’est pas correct&nbsp;!',
        comment2: 'Que doit vérifier la somme de toutes les probabilités&nbsp;?',
        comment3: 'Les valeurs de la variable alétoire sont exactes, mais il faut revoir les probabilités&nbsp;!',
        comment4: 'Toutes les valeurs présentes dans la loi de probabilité doivent être différentes&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: '$X$ peut prendre les valeurs £{v1}, £{v2} et £{v3}.',
        corr2: 'On obtient ainsi la loi de $X$&nbsp;:',
        corr3: 'On remarque que la somme de toutes les probabilités vaut 1.'
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;

      pe: 0
    }
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.algo = j3pAddElt(stor.conteneurD, 'div')
    stor.divCorr = j3pAddElt(stor.conteneurD, 'div', '', { style: { paddingTop: '10px' } })
    const ChoixAlgo = j3pGetRandomInt(0, 2)
    const lignesAlgo = []
    let espaceTab = ''
    for (let i = 1; i <= 5; i++) espaceTab += '&nbsp;'
    lignesAlgo.push("<span class='bleu'>from </span>random <span class='bleu'>import </span>random")
    const varAlea = (j3pGetRandomBool()) ? 'alea' : 'nbAlea'
    lignesAlgo.push(varAlea + '=random()')
    stor.param = {}
    stor.param.borne1 = j3pGetRandomInt(15, 30) / 100
    stor.param.borne2 = Math.round(100 * (stor.param.borne1 + j3pGetRandomInt(15, 30) / 100)) / 100
    stor.param.ValX = []
    stor.param.ValX[0] = j3pGetRandomInt(1, 7)
    stor.param.ValX[1] = stor.param.ValX[0] + j3pGetRandomInt(3, 8)
    stor.param.ValX[2] = stor.param.ValX[1] + j3pGetRandomInt(5, 10)
    stor.param.ValX = j3pShuffle(stor.param.ValX)
    switch (ChoixAlgo) {
      case 0:
        lignesAlgo.push("<span class='bleu'>if </span>" + varAlea + '<' + stor.param.borne1 + ':')
        lignesAlgo.push(espaceTab + 'X=' + stor.param.ValX[0])
        lignesAlgo.push("<span class='bleu'>elif </span>" + varAlea + '<' + stor.param.borne2 + ':')
        lignesAlgo.push(espaceTab + 'X=' + stor.param.ValX[1])
        lignesAlgo.push("<span class='bleu'>else </span>:")
        lignesAlgo.push(espaceTab + 'X=' + stor.param.ValX[2])
        break
      case 1:
        lignesAlgo.push("<span class='bleu'>if </span>" + varAlea + '<' + stor.param.borne1 + ':')
        lignesAlgo.push(espaceTab + 'X=' + stor.param.ValX[0])
        lignesAlgo.push("<span class='bleu'>elif </span>" + varAlea + '>' + stor.param.borne2 + ':')
        lignesAlgo.push(espaceTab + 'X=' + stor.param.ValX[2])
        lignesAlgo.push("<span class='bleu'>else </span>:")
        lignesAlgo.push(espaceTab + 'X=' + stor.param.ValX[1])
        break
      case 2:
        lignesAlgo.push("<span class='bleu'>if </span>" + varAlea + '>' + stor.param.borne2 + ':')
        lignesAlgo.push(espaceTab + 'X=' + stor.param.ValX[2])
        lignesAlgo.push("<span class='bleu'>elif </span>" + varAlea + '>' + stor.param.borne1 + ':')
        lignesAlgo.push(espaceTab + 'X=' + stor.param.ValX[1])
        lignesAlgo.push("<span class='bleu'>else </span>:")
        lignesAlgo.push(espaceTab + 'X=' + stor.param.ValX[0])
        break
    }
    stor.coupleRep = []
    stor.coupleRep.push({ valeur: stor.param.ValX[0], proba: stor.param.borne1 })
    stor.coupleRep.push({
      valeur: stor.param.ValX[1],
      proba: Math.round(100 * (stor.param.borne2 - stor.param.borne1)) / 100
    })
    stor.coupleRep.push({ valeur: stor.param.ValX[2], proba: Math.round(100 * (1 - stor.param.borne2)) / 100 })

    stor.mesDonnees = { parent: stor.zoneCons4 }
    stor.mesDonnees.largeur = me.zonesElts.MG.getBoundingClientRect().width
    stor.mesDonnees.legendes = (ds.texteLegende) ? ds.textes.legendes.split('|') : ['$x_i$', '$P(X=x_i)$']
    stor.mesDonnees.valeurs = ['?', '?']
    stor.mesDonnees.probas = ['-1', '-1']
    stor.mesDonnees.complet = false
    stor.mesDonnees.typeFont = me.styles.petit.enonce
    stor.mesDonnees.restrict = ['\\d,.-', '\\d,.']
    for (let i = 1; i <= 2; i++) j3pAffiche(stor['zoneCons' + i], '', ds.textes['consigne' + i])

    me.logIfDebug('mesDonnees:', stor.mesDonnees, '\ncoupleRep:', stor.coupleRep)
    stor.zoneInput = tabLoiProba(stor.mesDonnees)
    stor.divAjoute = j3pAddElt(stor.zoneCons3, 'div')
    stor.divEnleve = j3pAddElt(stor.zoneCons3, 'div')
    constructionBtnAjoute()
    declarationZoneAValider()
    stor.valeursSaisies = []
    stor.probasSaisies = []

    stor.DonneesRep = { }
    stor.DonneesRep.largeur = stor.mesDonnees.largeur
    stor.DonneesRep.legendes = stor.mesDonnees.legendes.slice()
    stor.DonneesRep.valeurs = []
    stor.DonneesRep.probas = []
    for (let i = 0; i < stor.coupleRep.length; i++) {
      stor.DonneesRep.valeurs.push(stor.coupleRep[i].valeur)
      stor.DonneesRep.probas.push(stor.coupleRep[i].proba)
    }
    stor.DonneesRep.complet = true
    for (let i = 0; i < lignesAlgo.length; i++) {
      const ligne = j3pAddElt(stor.algo, 'div')
      j3pAffiche(ligne, '', lignesAlgo[i])
    }
    j3pStyle(stor.algo, { backgroundColor: 'white' })
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 60

        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc / 100 })
        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        let valeursDifferentes = true
        let sommeProba
        let toutesValeursBonnes
        let bonNombreValeurs
        if (reponse.aRepondu) {
        // toutes les zones ont été complétées
          const tabCouple = []
          const tabCoupleOK = []
          const testRep = stor.coupleRep.slice()
          sommeProba = 0
          const bonneValeur = []
          toutesValeursBonnes = true
          for (let i = 0; i < stor.fctsValid.zones.inputs.length / 2; i++) {
          // on cherche les couples valeurs/probas
            tabCouple[i] = {}
            tabCouple[i].valeur = stor.fctsValid.zones.reponseSaisie[2 * i]
            tabCouple[i].proba = j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[2 * i + 1])
            // console.log('tabCouple[i]:', tabCouple[i])
            tabCoupleOK.push(false)
            sommeProba += tabCouple[i].proba
            bonneValeur[i] = false
          }
          const tabValeursDifferentes = []
          for (let i = 0; i < tabCouple.length; i++) {
            const maVal = j3pCalculValeur(tabCouple[i].valeur)
            if (!tabValeursDifferentes.includes(maVal)) tabValeursDifferentes.push(maVal)
            else valeursDifferentes = false
          }
          reponse.aRepondu = valeursDifferentes
          if (reponse.aRepondu) {
            bonNombreValeurs = (testRep.length === tabCouple.length)
            reponse.bonneReponse = (bonNombreValeurs && valeursDifferentes)
            if (reponse.bonneReponse) {
              j3pEmpty(stor.zoneCons3)
              for (let i = 0; i < tabCouple.length; i++) {
                let k = 0
                while (k < testRep.length && !tabCoupleOK[i]) {
                  if (Math.abs(j3pCalculValeur(tabCouple[i].valeur) - testRep[k].valeur) < Math.pow(10, -12)) {
                    const indexBonneValeur = stor.coupleRep.indexOf(j3pCalculValeur(tabCouple[i].valeur))
                    bonneValeur[indexBonneValeur] = true
                    // Dans le cas où la valeur proposée par l’élève est l’une des valeurs attendues, je teste si les proba sont les mêmes
                    tabCoupleOK[i] = (Math.abs(tabCouple[i].proba - j3pCalculValeur(testRep[k].proba)) < Math.pow(10, -12))
                    if (tabCoupleOK[i]) {
                      testRep.splice(k, 1)
                    }
                  }
                  k++
                }
              }
              // Je vérifie alors si tous les couples sont OK et mets les zones à false si ce n’est pas le cas
              for (let i = 0; i < tabCouple.length; i++) {
                toutesValeursBonnes = (toutesValeursBonnes && bonneValeur[i])
                reponse.bonneReponse = (reponse.bonneReponse && tabCoupleOK[i])
                if (!tabCoupleOK[i]) {
                  stor.fctsValid.zones.bonneReponse[2 * i] = false
                  stor.fctsValid.zones.bonneReponse[2 * i + 1] = false
                }
              }
            } else {
              for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
                stor.fctsValid.zones.bonneReponse[i] = false
              }
            }
            for (let i = stor.fctsValid.zones.inputs.length - 1; i >= 0; i--) {
            // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
              stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
            }
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (!valeursDifferentes) {
            msgReponseManquante = ds.textes.comment4
          }
          me.reponseManquante(stor.divCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.divCorr.style.color = me.styles.cbien
            stor.divCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.divCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.divCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.divCorr.innerHTML = cFaux
              if (!bonNombreValeurs) {
                stor.divCorr.innerHTML += '<br>' + ds.textes.comment1
              } else {
                if (toutesValeursBonnes) {
                  stor.divCorr.innerHTML += '<br>' + ds.textes.comment2
                }
                if (Math.abs(sommeProba - 1) > Math.pow(10, -12)) {
                  stor.divCorr.innerHTML += '<br>' + ds.textes.comment2
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.divCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.divCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
        // Obligatoire
        me.finCorrection()
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
