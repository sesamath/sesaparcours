import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomElt, j3pGetRandomInt, j3pPaletteMathquill, j3pPGCD, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pExtraireNumDen, j3pGetLatexQuotient } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since juin 2022
 * @fileOverview Cette section demande un calcul de probabilité dans le cadre d’une succession d’épreuves indépendantes (sans parler de schéma de Bernoulli)
 */

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

const textes = {
  titre_exo: 'Succession d’épreuves indépendantes',
  consigne1: 'Une urne contient £n boules de 3 couleurs différentes&nbsp;: £r rouges, £b bleues et £v vertes.',
  consigne2: 'On tire successivement et avec remise £m boules de cette urne.',
  consigne3: 'La probabilité d’obtenir £m boules £c vaut &1&.',
  consigne4: 'Si on note R la boule rouge, B la boule bleue et V la boule verte, la probabilité d’obtenir l’issue £i dans cet ordre vaut &1&.',
  couleurs: 'rouges|bleues|vertes',
  info: 'Le résultat sera donné sous forme décimale ou d’une fraction irréductible.',
  comment1: 'La réponse doit être réduite&nbsp;!',
  comment2: 'De plus la réponse doit être réduite&nbsp;!',
  corr1: 'Les tirages successifs sont indépendants car le résultat de chaque tirage n’a pas d’incidence sur le suivant.',
  corr2: 'Ainsi la probabilité vaut&nbsp;:'
}
/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour chaque sujet
 * @private
 * @return {Object}
 */
function genereAlea () {
  const obj = {}
  do {
    do {
      obj.r = j3pGetRandomInt(3, 5)
      obj.b = j3pGetRandomInt(7, 8) - obj.r
    } while (obj.r === obj.b)
    obj.n = j3pGetRandomInt(10, 13)
    obj.v = obj.n - obj.r - obj.b
  } while (obj.v === obj.b || obj.v === obj.r)
  obj.m = j3pGetRandomInt(3, 4)
  // probabilités d’avoir m boules de la couleur
  obj.prbs = [Math.pow(obj.r / obj.n, obj.m), Math.pow(obj.b / obj.n, obj.m), Math.pow(obj.v / obj.n, obj.m)]
  obj.calcPrbs = [
    '\\frac{' + obj.r + '}{' + obj.n + '}\\times \\frac{' + obj.r + '}{' + obj.n + '}\\times\\frac{' + obj.r + '}{' + obj.n + '}' + ((obj.m === 4) ? '\\times\\frac{' + obj.r + '}{' + obj.n + '}' : ''),
    '\\frac{' + obj.b + '}{' + obj.n + '}\\times \\frac{' + obj.b + '}{' + obj.n + '}\\times\\frac{' + obj.b + '}{' + obj.n + '}' + ((obj.m === 4) ? '\\times\\frac{' + obj.b + '}{' + obj.n + '}' : ''),
    '\\frac{' + obj.v + '}{' + obj.n + '}\\times \\frac{' + obj.v + '}{' + obj.n + '}\\times\\frac{' + obj.v + '}{' + obj.n + '}' + ((obj.m === 4) ? '\\times\\frac{' + obj.v + '}{' + obj.n + '}' : '')
  ]
  obj.reps = [
    '\\frac{' + Math.pow(obj.r, obj.m) + '}{' + Math.pow(obj.n, obj.m) + '}' + ((j3pPGCD(obj.r, obj.n) === 1) ? '' : '=' + j3pGetLatexQuotient(Math.pow(obj.r, obj.m), Math.pow(obj.n, obj.m))),
    '\\frac{' + Math.pow(obj.b, obj.m) + '}{' + Math.pow(obj.n, obj.m) + '}' + ((j3pPGCD(obj.b, obj.n) === 1) ? '' : '=' + j3pGetLatexQuotient(Math.pow(obj.b, obj.m), Math.pow(obj.n, obj.m))),
    '\\frac{' + Math.pow(obj.v, obj.m) + '}{' + Math.pow(obj.n, obj.m) + '}' + ((j3pPGCD(obj.v, obj.n) === 1) ? '' : '=' + j3pGetLatexQuotient(Math.pow(obj.v, obj.m), Math.pow(obj.n, obj.m)))
  ]
  const tab = ['r', 'b', 'v']
  let nbColor, tabCouleurs
  do {
    nbColor = tab.map(elt => 0)
    tabCouleurs = []
    for (let i = 1; i <= obj.m; i++) {
      const laCouleur = j3pGetRandomElt(tab)
      nbColor[tab.indexOf(laCouleur)] += 1
      tabCouleurs.push(laCouleur)
    }
  } while (nbColor.includes(3) || nbColor.includes(4))

  obj.calcQ2 = '\\frac{' + obj[tabCouleurs[0]] + '}{' + obj.n + '}'
  tabCouleurs.slice(1).forEach(c => { obj.calcQ2 += '\\times \\frac{' + obj[c] + '}{' + obj.n + '}' })
  let numQ2 = 1
  let denQ2 = 1
  tabCouleurs.forEach(c => {
    numQ2 *= obj[c]
    denQ2 *= obj.n
  })
  const lePGCD = j3pPGCD(numQ2, denQ2)
  obj.repQ2 = '\\frac{' + numQ2 + '}{' + denQ2 + '}' + ((lePGCD === 1) ? '' : '=\\frac{' + numQ2 / lePGCD + '}{' + denQ2 / lePGCD + '}')
  obj.prbQ2 = numQ2 / denQ2
  obj.i = '(' + tabCouleurs.map(c => c.toUpperCase()).join(' ; ') + ')'
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  // Construction de la page
  me.surcharge({ nbetapes: 2 })
  me.construitStructurePage()
  me.afficheTitre(textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  // On génére de nouvelles données
  if (me.questionCourante % ds.nbetapes === 1) stor.objDonnees = genereAlea()
  // on affiche l’énoncé
  ;[1, 2].forEach(i => {
    stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor['zoneCons' + i], '', textes['consigne' + i], stor.objDonnees)
  })
  stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div')
  if (me.questionCourante % ds.nbetapes === 1) {
    // Cas où on demande des boules de la même couleur
    const tabCouleurs = textes.couleurs.split('|')
    stor.choix = j3pGetRandomInt(0, tabCouleurs.length - 1)
    const elt = j3pAffiche(stor.zoneCons3, '', textes.consigne3, {
      m: stor.objDonnees.m,
      c: tabCouleurs[stor.choix]
    })
    stor.zoneInput = elt.inputmqList[0]
    stor.zoneInput.reponse = [stor.objDonnees.prbs[stor.choix]]
  } else {
    // On demande des boules de k couleurs différentes dans un certain ordre
    const elt = j3pAffiche(stor.zoneCons3, '', textes.consigne4, stor.objDonnees)
    stor.zoneInput = elt.inputmqList[0]
    stor.zoneInput.reponse = [stor.objDonnees.prbQ2]
  }
  stor.zoneInput.typeReponse = ['nombre', 'exact']
  stor.zoneInput.solutionSimplifiee = ['reponse fausse', 'reponse fausse']
  mqRestriction(stor.zoneInput, '\\d,./', { commandes: ['fraction'] })
  stor.fctsValid = new ValidationZones({
    parcours: me,
    zones: [stor.zoneInput.id]
  })
  stor.laPalette = j3pAddElt(stor.conteneur, 'div')
  j3pStyle(stor.laPalette, { paddingTop: '10px' })
  j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['fraction'] })
  stor.zoneInfo = j3pAddElt(stor.conteneur, 'div')
  j3pStyle(stor.zoneInfo, { fontSize: '0.9em', fontStyle: 'italic', paddingTop: '50px' })
  j3pAffiche(stor.zoneInfo, '', textes.info)
  stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
  me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
  j3pCreeFenetres(me)
  j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
  j3pAfficheCroixFenetres('Calculatrice')

  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
  j3pFocus(stor.zoneInput)

  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  j3pDetruit(stor.zoneCalc)
  j3pDetruitFenetres('Calculatrice')
  j3pDetruit(stor.laPalette)
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
  ;[1, 2].forEach(i => { j3pAffiche(stor['zoneExpli' + i], '', textes['corr' + i]) })
  if (me.questionCourante % ds.nbetapes === 1) {
    j3pAffiche(stor.zoneExpli3, '', '$' + stor.objDonnees.calcPrbs[stor.choix] + '=' + stor.objDonnees.reps[stor.choix] + '$')
  } else {
    j3pAffiche(stor.zoneExpli3, '', '$' + stor.objDonnees.calcQ2 + '=' + stor.objDonnees.repQ2 + '$')
  }
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  if (reponse.aRepondu) {
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
    // On va vérifier si la solution n’est pas une fraction non réduite
    const analyseRep = j3pExtraireNumDen(stor.fctsValid.zones.reponseSaisie[0])
    if (analyseRep[0]) {
      // la réponse est une fraction
      const lePGCD = j3pPGCD(analyseRep[1], analyseRep[2])
      if (lePGCD !== 1) {
        // Le commentaire sera un peu différent selon que la réponse est bonne ou pas
        let comment = (Math.abs(j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[0]) - stor.zoneInput.reponse[0]) < Math.pow(10, -10))
          ? textes.comment1
          : textes.comment2
        comment = '<br/>' + comment
        me.reponseKo(stor.divCorrection, comment, true)
      }
    }
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
