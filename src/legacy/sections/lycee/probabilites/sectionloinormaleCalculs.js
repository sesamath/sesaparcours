import { j3pAddElt, j3pNombre, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pShowError, j3pGetNewId, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { remplaceCalcul } from 'src/legacy/outils/fonctions/gestionParametres'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    avril 2017
    Cette section permet de tester le calcul de probabilités dans le cas d’une loi normale
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['tabCasFigure', [''], 'array', 'Pour chaque répétition, on peut imposer, sous forme d’un tableau, un des exercices particuliers présents dans le fichier texte annexe :<br/>-1 : Amérique du Nord juin 2016 S<br/>-2 : Métropole septembre 2016 S<br/>-3 : Pondichéry mai 2015 S<br/>-4 : Centres étrangers 2015 S<br/>-5 : Pondichéry 2017 S<br/>-6 : Liban 2014 S<br/>-7 : Amérique du Nord 2014 S-8 : Antilles 2014<br/>-9 : Asie 2014 S<br/>-10 : Amérique du Sud 2014 S<br/>-11 : Amérique du Sud 2014 S (version 2)']
  ]
}

/**
 * section loinormaleCalculs
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    function actualisationFigure () {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'mu', '0')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'sigma', '1')
      const valA = (stor.objFig.a - stor.objFig.mu) / stor.objFig.sigma
      const valB = (stor.objFig.b - stor.objFig.mu) / stor.objFig.sigma
      if (valA <= -5) {
        // c’est qu’on a une aire à gauche
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'absPrb', String(valB - 0.3))
      } else if (valB >= 5) {
        // c’est qu’on a une aire à droite
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'absPrb', String(valA + 0.3))
      } else {
        // c’est qu’on a une aire centrée
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'absPrb', String((valA + 3 * valB) / 4))
      }
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(valA))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(valB))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a1', String(stor.objFig.a))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a2', String(stor.objFig.b))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'muExo', String(stor.objFig.mu))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'p', String(stor.obj.r))
      stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
      stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
    }

    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pAffiche(stor.zoneExpli1, '', stor.calculatrice['enonce' + stor.numeroExo][stor.numQuest], stor.obj)
    stor.obj.r = Math.round(Math.pow(10, stor.arrondi['enonce' + stor.numeroExo]) * stor.zoneInput.reponse[0]) / Math.pow(10, stor.arrondi['enonce' + stor.numeroExo])
    j3pAffiche(stor.zoneExpli2, '', stor.reponse['enonce' + stor.numeroExo][stor.numQuest], stor.obj)
    j3pAffiche(stor.zoneExpli3, '', ds.textes.corr1)
    depart(true, function () {
      // je modifie ici les données de la figure pour l’adapter à l’exo
      actualisationFigure()
    })
  }

  function depart (figureAffichee, callback) {
    // txt html de la figure
    let txtFigure
    if (figureAffichee) {
      // cela ne se fait dans cette section qu'à la fin où on affiche la figure où apparait la zone dont on cherche l’aire
      const divMtg32 = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '5px' }) })
      // et enfin la zone svg (très importante car tout est dedans)
      const largFig = 370// on gère ici la largeur de la figure
      const hautFig = 225
      stor.mtg32svg = j3pGetNewId('mtg32svg')
      j3pCreeSVG(divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
      stor.mtgAppLecteur.removeAllDoc()
      txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH2+v4BAP8BAAAAAAAAAAABcgAAAOUAAAAAAAAAAAAAAAAAAABd#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEAD0NWYXJpYWJsZUJvcm5lZQD#####AAVzaWdtYT#wAAAAAAAAP+AAAAAAAABACAAAAAAAAD+5mZmZmZmaAAADMC41AAEzAAMwLjEAAAACAP####8AAm11AAAAAAAAAADAAAAAAAAAAEAAAAAAAAAAP8mZmZmZmZoAAAItMgABMgADMC4y#####wAAAAEAB0NDYWxjdWwA#####wAFbXVFeG8AAjUwAAAAAUBJAAAAAAAAAAAAAwD#####AAJhNgAELTAuNf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABP+AAAAAAAAAAAAADAP####8AAmE1AAItMQAAAAQAAAABP#AAAAAAAAD#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAFPAMAuAAAAAAAAQBAAAAAAAAAFAAFAZWAAAAAAAEBjoAAAAAAA#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAAA8AAAEAAQAAAAYBP+qqqqqqqqv#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAARAAAUkAwBAAAAAAAABAEAAAAAAAAAUAAUBKZmZmZmZrAAAAB#####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAAEAAAAQABAAAABgAAAAj#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAPAAABAAEAAAAGAAAABwAAAAcA#####wEAAAABEAABSgDAJgAAAAAAAAAAAAAAAAAABQABwHEszMzMzM0AAAAKAAAACAD#####AQAAAAAQAAABAAEAAAAGAAAAC#####8AAAACAAdDUmVwZXJlAP####8AgICAAQEAAAAGAAAACAAAAAsAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABP#AAAAAAAAAAAAABP#AAAAAAAAD#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAAGAAAACAAAAAMA#####wAHbmJncmFkeAACMjAAAAABQDQAAAAAAAAAAAADAP####8AB25iZ3JhZHkAAjIwAAAAAUA0AAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AF0dyYWR1YXRpb25BeGVzUmVwZXJlTmV3AAAAGwAAAAgAAAADAAAADQAAAA8AAAAQ#####wAAAAEAE0NBYnNjaXNzZU9yaWdpbmVSZXAAAAAAEQAFYWJzb3IAAAAN#####wAAAAEAE0NPcmRvbm5lZU9yaWdpbmVSZXAAAAAAEQAFb3Jkb3IAAAAN#####wAAAAEACkNVbml0ZXhSZXAAAAAAEQAGdW5pdGV4AAAADf####8AAAABAApDVW5pdGV5UmVwAAAAABEABnVuaXRleQAAAA3#####AAAAAQAQQ1BvaW50RGFuc1JlcGVyZQAAAAARAAAAAAAPAAABBQAAAAAN#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAABIAAAASAAAAEwAAABEAAAAAEQAAAAAADwAAAQUAAAAADf####8AAAABAApDT3BlcmF0aW9uAAAAABIAAAASAAAAEgAAABQAAAASAAAAEwAAABEAAAAAEQAAAAAADwAAAQUAAAAADQAAABIAAAASAAAAEwAAAAASAAAAEwAAABIAAAAV#####wAAAAEAC0NIb21vdGhldGllAAAAABEAAAAWAAAAEgAAAA######AAAAAQALQ1BvaW50SW1hZ2UAAAAAEQAAAAAADwAAAQUAAAAAFwAAABkAAAAUAAAAABEAAAAWAAAAEgAAABAAAAAVAAAAABEAAAAAAA8AAAEFAAAAABgAAAAb#####wAAAAEACENTZWdtZW50AAAAABEBAAAAAA0AAAEAAQAAABcAAAAaAAAAFgAAAAARAQAAAAANAAABAAEAAAAYAAAAHAAAAAcAAAAAEQEAAAAACwABVwDAFAAAAAAAAMA0AAAAAAAABQABP9xWeJq83w4AAAAd#####wAAAAIACENNZXN1cmVYAAAAABEAB3hDb29yZDEAAAANAAAAHwAAAAMAAAAAEQAFYWJzdzEAB3hDb29yZDEAAAASAAAAIP####8AAAACABJDTGlldU9iamV0UGFyUHRMaWUBAAAAEQFmZmYAAAAfAAAAEgAAAA8AAAAfAAAAAgAAAB8AAAAfAAAAAwAAAAARAAVhYnN3MgANMiphYnNvci1hYnN3MQAAABMBAAAAEwIAAAABQAAAAAAAAAAAAAASAAAAEgAAABIAAAAhAAAAEQAAAAARAQAAAAAPAAABBQAAAAANAAAAEgAAACMAAAASAAAAEwAAABgBAAAAEQFmZmYAAAAkAAAAEgAAAA8AAAAfAAAABQAAAB8AAAAgAAAAIQAAACMAAAAkAAAABwAAAAARAQAAAAALAAFSAEAgAAAAAAAAwCAAAAAAAAAFAAE#0RtOgbToHwAAAB7#####AAAAAgAIQ01lc3VyZVkAAAAAEQAHeUNvb3JkMQAAAA0AAAAmAAAAAwAAAAARAAVvcmRyMQAHeUNvb3JkMQAAABIAAAAnAAAAGAEAAAARAWZmZgAAACYAAAASAAAAEAAAACYAAAACAAAAJgAAACYAAAADAAAAABEABW9yZHIyAA0yKm9yZG9yLW9yZHIxAAAAEwEAAAATAgAAAAFAAAAAAAAAAAAAABIAAAATAAAAEgAAACgAAAARAAAAABEBAAAAAA8AAAEFAAAAAA0AAAASAAAAEgAAABIAAAAqAAAAGAEAAAARAWZmZgAAACsAAAASAAAAEAAAACYAAAAFAAAAJgAAACcAAAAoAAAAKgAAACv#####AAAAAgAMQ0NvbW1lbnRhaXJlAAAAABEBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAfCwAB####AAAAAQAAAAAACyNWYWwoYWJzdzEpAAAAGAEAAAARAWZmZgAAAC0AAAASAAAADwAAAB8AAAAEAAAAHwAAACAAAAAhAAAALQAAABoAAAAAEQFmZmYAAAAAAAAAAABAGAAAAAAAAAAAACQLAAH###8AAAABAAAAAAALI1ZhbChhYnN3MikAAAAYAQAAABEBZmZmAAAALwAAABIAAAAPAAAAHwAAAAYAAAAfAAAAIAAAACEAAAAjAAAAJAAAAC8AAAAaAAAAABEBZmZmAMAgAAAAAAAAP#AAAAAAAAAAAAAmCwAB####AAAAAgAAAAEACyNWYWwob3JkcjEpAAAAGAEAAAARAWZmZgAAADEAAAASAAAAEAAAACYAAAAEAAAAJgAAACcAAAAoAAAAMQAAABoAAAAAEQFmZmYAwBwAAAAAAAAAAAAAAAAAAAAAACsLAAH###8AAAACAAAAAQALI1ZhbChvcmRyMikAAAAYAQAAABEBZmZmAAAAMwAAABIAAAAQAAAAJgAAAAYAAAAmAAAAJwAAACgAAAAqAAAAKwAAADP#####AAAAAQAFQ0ZvbmMA#####wABZgAvMS8oc2lnbWEqc3FydCgyKnBpKSkqZXhwKC0wLjUqKCh4LW11KS9zaWdtYSleMikAAAATAgAAABMDAAAAAT#wAAAAAAAAAAAAEwIAAAASAAAAAf####8AAAACAAlDRm9uY3Rpb24BAAAAEwIAAAABQAAAAAAAAAAAAAASAAAAAAAAABwHAAAABAAAABMCAAAAAT#gAAAAAAAA#####wAAAAEACkNQdWlzc2FuY2UAAAATAwAAABMB#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAABIAAAACAAAAEgAAAAEAAAABQAAAAAAAAAAAAXgAAAAHAP####8BAAAAAA8AAngxAQUAAb#zhYQjXZxPAAAACQAAABcA#####wAHeENvb3JkMQAAAA0AAAA2AAAAAwD#####AAJ4MQAHeENvb3JkMQAAABIAAAA3AAAAAwD#####AAJ5MQAFZih4MSn#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAAA1AAAAEgAAADgAAAARAP####8BAAAAAA8AAAEFAAAAAA0AAAASAAAAOAAAABIAAAA5#####wAAAAIADUNMaWV1RGVQb2ludHMA#####wAAAAAAAQAAADoAAAfQAAEAAAA2AAAABQAAADYAAAA3AAAAOAAAADkAAAA6AAAAAwD#####AAFhAAQtMC4yAAAABAAAAAE#yZmZmZmZmgAAAAMA#####wABYgADMS41AAAAAT#4AAAAAAAAAAAAEQD#####AQAAAAAPAAAAAAAAAAAAAABACAAAAAAAAAUAAAAADQAAABIAAAA8AAAAAQAAAAAAAAAAAAAAEQD#####AQAAAAAPAAAAAAAAAAAAAABACAAAAAAAAAUAAAAADQAAABIAAAA8AAAAHwAAADUAAAASAAAAPAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAA0AAAASAAAAPQAAAAEAAAAAAAAAAAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAA0AAAASAAAAPQAAAB8AAAA1AAAAEgAAAD0AAAAWAP####8BAAAAAA0AAAEAAQAAAD4AAABAAAAABwD#####AQAAAAAPAAJ4MgEFAAE#0S8aM29t5AAAAEIAAAAXAP####8AB3hDb29yZDIAAAANAAAAQwAAAAMA#####wACeDIAB3hDb29yZDIAAAASAAAARAAAAAMA#####wACeTIABWYoeDIpAAAAHwAAADUAAAASAAAARQAAABEA#####wEAAAAADwAAAQUAAAAADQAAABIAAABFAAAAEgAAAEYAAAAgAP####8AAAAAAAEAAABHAAAB9AABAAAAQwAAAAUAAABDAAAARAAAAEUAAABGAAAAR#####8AAAABABJDU3VyZmFjZUxpZXVEcm9pdGUA#####wAAAP8AAAAFAAAASAAAAAn#####AAAAAgAMQ0Ryb2l0ZVBhckVxAP####8BAAAAAA8AAAEAAQAAAA0AAAATCAAAAB4AAAAAAAAAEgAAAAIAAAATAQAAAB4AAAAAAAAAEgAAAAIAAAADAP####8AAmExAAEwAAAAAQAAAAAAAAAAAAAAAwD#####AAJhMgABMQAAAAE#8AAAAAAAAAAAABYA#####wAAAP8ADQAAAQABAAAAPwAAAD4AAAAWAP####8AAAD#AA0AAAEAAQAAAEEAAABA#####wAAAAEACkNJbnRlZ3JhbGUA#####wABSQAAADUAAAASAAAAPAAAABIAAAA9AAAAAUBZAAAAAAAAAAAAEQD#####AQAAAAAPAAAAAAAAAAAAAABACAAAAAAAAAUAAAAADQAAAAEAAAAAAAAAAAAAAAQAAAABP8MzMzMzMzMAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAANAAAAAQAAAAAAAAAAAAAAAT#czMzMzMzNAAAAFgD#####AAAAAAANAAABAAEAAABRAAAAUAAAABoA#####wAAAAAAQAAAAAAAAADAAAAAAAAAAAAAAFASAAAAAAABAAAAAAAOzrw9I1ZhbChtdUV4bykAAAADAP####8ABmFic1ByYgADMC41AAAAAT#gAAAAAAAAAAAAAwD#####AAZvcmRQcmIAC2YoYWJzUHJiKS8zAAAAEwMAAAAfAAAANQAAABIAAABUAAAAAUAIAAAAAAAAAAAAEQD#####AQAAAAAPAAAAAAAAAAAAAABACAAAAAAAAAUAAAAADQAAABIAAABUAAAAEgAAAFUAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAANAAAAEgAAAFQAAAABP+AAAAAAAAAAAAADAP####8AAXAAAzAuMgAAAAE#yZmZmZmZmv####8AAAABAAhDVmVjdGV1cgD#####AAAAAAANAAABAAEAAABXAAAAVgMAAAAaAP####8AAAD#AQAAAFcSAAAAAAABAAAAAgAOQWlyZT0jVmFsKHAsNSkAAAAaAP####8AAAD#AL#wAAAAAAAAP#AAAAAAAAAAAAA+EgAAAAAAAQAAAAAACiNWYWwoYTEsNSkAAAAaAP####8AAAD#AQAAAEASAAAAAAABAAAAAAAKI1ZhbChhMiw1KQAAAA7##########w=='
      stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
      // stor.mtgAppLecteur.calculateAndDisplayAll(true)
      callback() // cette fonction ne peut être appelée qu’une fois la figure chargée. Elle permet de l’adapter au données de l’exo
    } else {
      txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAAA8+TMzNAANmcmH###8BAP8BAAAAAAAAAAADSAAAAhwAAAAAAAAAAAAAAAAAAAAM#####wAAAAEAEW9iamV0cy5DQ2FsY0NvbnN0AP####8AAnBpABYzLjE0MTU5MjY1MzU4OTc5MzIzODQ2#####wAAAAEAEW9iamV0cy5DQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAOb2JqZXRzLkNDYWxjdWwA#####wACbXUAATAAAAABAAAAAAAAAAAAAAACAP####8ABXNpZ21hAAExAAAAAT#wAAAAAAAA#####wAAAAEADG9iamV0cy5DRm9uYwD#####AAFmAC8xLyhzaWdtYSpzcXJ0KDIqcGkpKSpleHAoLSh4LW11KV4yLygyKnNpZ21hXjIpKf####8AAAABABFvYmpldHMuQ09wZXJhdGlvbgIAAAAEAwAAAAE#8AAAAAAAAAAAAAQC#####wAAAAEAFm9iamV0cy5DUmVzdWx0YXRWYWxldXIAAAAC#####wAAAAIAEG9iamV0cy5DRm9uY3Rpb24BAAAABAIAAAABQAAAAAAAAAAAAAAFAAAAAAAAAAYH#####wAAAAEAE29iamV0cy5DTW9pbnNVbmFpcmUAAAAEA#####8AAAABABFvYmpldHMuQ1B1aXNzYW5jZQAAAAQB#####wAAAAIAGG9iamV0cy5DVmFyaWFibGVGb3JtZWxsZQAAAAAAAAAFAAAAAQAAAAFAAAAAAAAAAAAAAAQCAAAAAUAAAAAAAAAAAAAACAAAAAUAAAACAAAAAUAAAAAAAAAAAAF4AAAAAgD#####AAFhAAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAFiAAIxMAAAAAFAJAAAAAAAAP####8AAAABABFvYmpldHMuQ0ludGVncmFsZQD#####AAVwcm9iYQAAAAMAAAAFAAAABAAAAAUAAAAFAAAAAUA0AAAAAAAA#####wAAAAIADW9iamV0cy5DTGF0ZXgA#####wAAAAAADwAAAf####8QQEAAAAAAAABANAAAAAAAAAAAAAAAAAAAAAAAblxmcmFjezF9e1xzaWdtYVxzcXJ0ezJccGl9fVxpbnRfe1xWYWx7YX19XntcVmFse2J9fVx0ZXh0e2V9XnstXGZyYWN7KHgtXG11KV4yfXtcc2lnbWFeMn19XHRleHR7ZH14PVxWYWx7cHJvYmF9AAAACwD#####AAAAAAAPAAAB#####xBAQQAAAAAAAEBZwAAAAAAAAAAAAAAAAAAAAAAMXG11PVxWYWx7bXV9AAAACwD#####AAAAAAAPAAAB#####xBAQIAAAAAAAEBgQAAAAAAAAAAAAAAAAAAAAAASXHNpZ21hPVxWYWx7c2lnbWF9AAAACwD#####AAAAAAAPAAAB#####xBAQQAAAAAAAEBmwAAAAAAAAAAAAAAAAAAAAAAJYT1cVmFse2F9AAAACwD#####AAAAAAAPAAAB#####xBAQIAAAAAAAEBpgAAAAAAAAAAAAAAAAAAAAAAJYj1cVmFse2J9################'
      stor.liste = stor.mtgAppLecteur.createList(txtFigure)
    }
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      tabCasFigure: [''],

      textes: {
        titre_exo: 'Loi normale et calculs de probabilités',
        consigne1: 'Arrondir le résultat à $10^{-£a}$.',
        comment1: 'Peut-être est-ce un problème d’arrondi ?',
        corr1: '<br/>Cette probabilité est représentée par l’aire sur la figure ci-contre.'
      },
      pe: 0
    }
  }

  // console.log("debut du code : ",me.etat)
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    // Construction de la page
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
    /*
   Par convention,`
  `   me.typederreurs[0] = nombre de bonnes réponses
      me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
      me.typederreurs[2] = nombre de mauvaises réponses
      me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
      LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
   */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    import('../../../sectionsAnnexes/Consignesexos/LoiNormaleCalculs.js').then(function parseAnnexe ({ default: datasSection }) {
      let i, j
      stor.nb_sujets = datasSection.sujets.length
      stor.consignes = {}
      stor.nomVariables = {}
      stor.variables = {}
      stor.question = {}
      stor.calculatrice = {}
      stor.reponse = {}
      stor.arrondi = {}
      stor.valeurs = {}
      for (j = 0; j < stor.nb_sujets; j++) {
        stor.consignes['enonce' + j] = datasSection.sujets[j].consignes
        stor.nomVariables['enonce' + j] = []
        stor.variables['enonce' + j] = []
        for (i = 0; i < datasSection.sujets[j].variables.length; i++) {
          stor.nomVariables['enonce' + j][i] = datasSection.sujets[j].variables[i][0]
          const [intMin, intMax] = j3pGetBornesIntervalle(datasSection.sujets[j].variables[i][1])
          if (datasSection.sujets[j].variables[i].length === 2) {
            stor.variables['enonce' + j][i] = j3pGetRandomInt(intMin, intMax)
          } else {
            stor.variables['enonce' + j][i] = j3pGetRandomInt(intMin, intMax) / Number(datasSection.sujets[j].variables[i][2])
          }
        }
        stor.question['enonce' + j] = datasSection.sujets[j].question
        stor.calculatrice['enonce' + j] = datasSection.sujets[j].calculatrice
        stor.reponse['enonce' + j] = datasSection.sujets[j].reponse
        stor.arrondi['enonce' + j] = datasSection.sujets[j].arrondi
        stor.valeurs['enonce' + j] = datasSection.sujets[j].valeurs
      }
      // chargement mtg
      getMtgCore({ withMathJax: true })
        .then(
          // success
          (mtgAppLecteur) => {
            stor.mtgAppLecteur = mtgAppLecteur
            suite()
          },
          // failure
          (error) => {
            j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
          })
    }).catch(error => {
      // plantage dans le code de success ou avant le chargement de mtg
      j3pShowError(error, { message: 'Impossible de charger les données de cette exercice' })
    })
  }

  function suite () {
    /// /////////////////////////////////////// Pour MathGraph32
    // ce qui suit lance la création initiale de la figure
    depart(false)
    /// //////////////////////////////////////Fin de l’initialisation de la figure MathGraph32'
    // Dans cette section l’utilisateur peut imposer les consignes
    let choixExoImpose = ((ds.tabCasFigure[me.questionCourante - 1] !== undefined) && (ds.tabCasFigure[me.questionCourante - 1] !== ''))
    if (choixExoImpose) {
      // je regarde tout de même si le numéro existe bien
      if ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].indexOf(Number(ds.tabCasFigure[me.questionCourante - 1])) === -1) {
        choixExoImpose = false
      }
    }
    if (choixExoImpose) {
      // c’est que j’ai imposé l’exo à traiter
      stor.numeroExo = Number(ds.tabCasFigure[me.questionCourante - 1]) - 1
    } else {
      // je choisis au hasard un des exos à traiter (parmi ceux qui ne sont pas déjà choisis)
      let nbTentatives = 0 // c’est au cas où on aurait déjà proposé tous les cas de figure (vu le nombre d’exos possible qu’il risque d’y avoir, c’est peu probable)
      do {
        nbTentatives++
        stor.numeroExo = j3pGetRandomInt(0, (stor.nb_sujets - 1))
      } while ((ds.tabCasFigure.indexOf(stor.numeroExo + 1) > -1) && (nbTentatives < 30))
      ds.tabCasFigure[me.questionCourante - 1] = stor.numeroExo + 1
    }

    stor.obj = {} // il servira pour la consigne
    stor.obj2 = {} // il servira pour la question
    for (let i = 0; i < stor.nomVariables['enonce' + stor.numeroExo].length; i++) {
      stor.obj[stor.nomVariables['enonce' + stor.numeroExo][i]] = stor.variables['enonce' + stor.numeroExo][i]
      stor.obj2[stor.nomVariables['enonce' + stor.numeroExo][i]] = stor.variables['enonce' + stor.numeroExo][i]
    }
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= stor.consignes['enonce' + stor.numeroExo].length; i++) {
      const zoneCons = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(zoneCons, '', stor.consignes['enonce' + stor.numeroExo][i - 1], stor.obj)
    }
    const zoneCons = j3pAddElt(stor.conteneur, 'div')
    stor.numQuest = j3pGetRandomInt(0, stor.question['enonce' + stor.numeroExo].length - 1)
    // pour la question, il faudra ajouter la propriété inputmq1 à obj2
    stor.obj2.inputmq1 = { texte: '' }
    const elt = j3pAffiche(zoneCons, '', stor.question['enonce' + stor.numeroExo][stor.numQuest], stor.obj2)
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.-')
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons2, '', ds.textes.consigne1, { a: stor.arrondi['enonce' + stor.numeroExo] })

    // on donne à mtg32 les données de l’énoncé pour calculer la proba
    const mu = remplaceCalcul({
      text: stor.valeurs['enonce' + stor.numeroExo].mu,
      obj: stor.obj
    })
    const sigma = remplaceCalcul({
      text: stor.valeurs['enonce' + stor.numeroExo].sigma,
      obj: stor.obj
    })
    const a = remplaceCalcul({
      text: stor.valeurs['enonce' + stor.numeroExo].borneInf[stor.numQuest],
      obj: stor.obj
    })
    const b = remplaceCalcul({
      text: stor.valeurs['enonce' + stor.numeroExo].borneSup[stor.numQuest],
      obj: stor.obj
    })
    stor.liste.giveFormula2('mu', String(mu))
    stor.liste.giveFormula2('sigma', String(sigma))
    stor.liste.giveFormula2('a', String(a))
    stor.liste.giveFormula2('b', String(b))

    stor.objFig = {}
    stor.objFig.mu = mu
    stor.objFig.sigma = sigma
    stor.objFig.a = a
    stor.objFig.b = b
    // j’actualise alors la figure avec ces nouvelles données :
    stor.liste.calculateNG()

    me.logIfDebug('mu:', stor.liste.valueOf('mu'), '   sigma:', stor.liste.valueOf('sigma'), '   borneInf:', stor.liste.valueOf('a'), '   borneSup:', stor.liste.valueOf('b'), '     réponse:', stor.liste.valueOf('proba'))
    // et je récupère la réponse qui est la variation proba de mtg32
    stor.zoneInput.typeReponse = ['nombre', 'arrondi', Math.pow(10, -stor.arrondi['enonce' + stor.numeroExo])]
    stor.zoneInput.reponse = [stor.liste.valueOf('proba')]
    me.logIfDebug('reponse attendue : ', stor.zoneInput.reponse)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div')
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '5px' }) })
    j3pFocus(stor.zoneInput)
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        // comme pour l’utilisation de Validation_zones, je déclare l’objet reponse qui contient deux propriétés : aRepondu et bonneReponse
        let reponse = { reponse: false, bonneReponse: false }
        reponse = fctsValid.validationGlobale()

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie
          if (reponse.bonneReponse) {
          // Bonne réponse
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(true)// affichage de la correction
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
             *   RECOPIER LA CORRECTION ICI !
             */
              afficheCorrection(false)// affichage de la correction
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // on peut affiner le commentaire si on veut
              if (Math.abs(j3pNombre(fctsValid.zones.reponseSaisie[0]) - stor.zoneInput.reponse[0]) < Math.pow(10, -stor.arrondi['enonce' + stor.numeroExo] + 1)) {
              // c’est que l’élève a fait le bon calcul mais qu’il a donné une valeur approchée et non un arrondi
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorrection(false)// affichage de la correction
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
