import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomBool, j3pGetRandomInt, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since juin 2022
 * @fileOverview Cette section demande l’espérance et la variance d’une somme de variables aléatoires indépendantes
 */

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {

    // Nombre de répétitions de l’exercice
    nbrepetitions: 3,
    nbchances: 2,
    indication: '',
    textes: {
      titre_exo: 'Somme de variables aléatoire indépendantes',
      consigne1: '$X_1$, £{xi} et $X_{£k}$ sont $£k$ variables aléatoires indépendantes suivant une même loi binomiale de paramètres $n=£n$ et $p=£p$.',
      consigne2: 'On pose $S=£s$.',
      consigne3: 'Déterminer l’espérance et la variance de cette variables aléatoire $S$.',
      consigne4: '$E(S)=$&1& et $V(S)=$&2&.',
      comment1: '',
      corr1: 'Chaque variable aléatoire $X_i$ a pour espérance $\\mu=np=£n\\times £p=£{mu}$ et pour variance $V=np(1-p)=£V$.',
      corr2: 'Ainsi $E(S)=£k\\times \\mu=£{espe}$ et $V(S)=£k\\times V=£{var}$.'
    }
  }
} // getDonnees

/**
 * Retourne les données pour chaque sujet
 * @private
 * @return {Object}
 */
function genereAlea () {
  const obj = {}
  obj.k = j3pGetRandomInt(3, 5)
  obj.xi = '$X_2$'
  obj.s = 'X_1+X_2'
  for (let i = 3; i < obj.k; i++) {
    obj.xi += ', $X_' + i + '$'
    obj.s += '+X_' + i
  }
  obj.s += '+X_' + obj.k
  obj.n = (j3pGetRandomBool()) ? 2 * j3pGetRandomInt(5, 9) : 5 * j3pGetRandomInt(1, 6)
  obj.p = j3pGetRandomInt(1, 9) / 10
  obj.mu = Math.round(Math.pow(10, 5) * obj.n * obj.p) / Math.pow(10, 5)
  obj.V = Math.round(Math.pow(10, 5) * obj.mu * (1 - obj.p)) / Math.pow(10, 5)
  obj.espe = Math.round(Math.pow(10, 5) * obj.k * obj.n * obj.p) / Math.pow(10, 5)
  obj.var = Math.round(Math.pow(10, 6) * obj.k * obj.n * obj.p * (1 - obj.p)) / Math.pow(10, 6)
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(me.donneesSection.textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.objDonnees = genereAlea()
  // on affiche l’énoncé
  ;[1, 2, 3].forEach(i => {
    stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor['zoneCons' + i], '', ds.textes['consigne' + i], stor.objDonnees)
  })
  stor.zoneCons4 = j3pAddElt(stor.conteneur, 'div')
  const objCons4 = Object.assign({}, stor.objDonnees)
  objCons4.inputmq1 = { texte: '' }
  objCons4.inputmq2 = { texte: '' }
  const elt = j3pAffiche(stor.zoneCons4, '', ds.textes.consigne4, objCons4)
  stor.zoneInput = [elt.inputmqList[0], elt.inputmqList[1]]
  stor.zoneInput.forEach(elt => {
    elt.typeReponse = ['nombre', 'exact']
    mqRestriction(elt, '\\d,.')
  })
  stor.zoneInput[0].reponse = [stor.objDonnees.espe]
  stor.zoneInput[1].reponse = [stor.objDonnees.var]
  stor.fctsValid = new ValidationZones({
    parcours: me,
    zones: stor.zoneInput.map(elt => elt.id)
  })
  stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
  me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
  j3pCreeFenetres(me)
  j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
  j3pAfficheCroixFenetres('Calculatrice')

  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
  j3pFocus(stor.zoneInput[0])

  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  j3pDetruit(stor.zoneCalc)
  j3pDetruitFenetres('Calculatrice')
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  ;[1, 2].forEach(i => {
    stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor['zoneExpli' + i], '', ds.textes['corr' + i], stor.objDonnees)
  })
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  if (reponse.aRepondu) {
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
