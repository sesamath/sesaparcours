import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pShuffle, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since juin 2022
 * @fileOverview Cette section utilise la propriété de concentration d’une variable aléatoire autour de son espérance
 */

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuestions', 'les deux', 'liste', 'Questions posées à chaque répétition', ['inter-union', 'p-uplet et produit', 'les deux']]
  ]
}
const textes = {
  titre_exo: 'Ensembles et parties',
  consigne1: 'On considère les ensembles $E=£e$ et $F=£f$.',
  consigne2: 'Donner les éléments appartenant à chacun des ensembles suivants&nbsp;:',
  consigne3_1: 'Avec l’ensemble $E$, on peut construire &1& triplets (ou 3-uplets) différents.',
  consigne3_2: 'Avec l’ensemble $E$, on peut construire &1& quadruplets (ou 4-uplets) différents.',
  consigne4: 'L’ensemble $E\\times F$ contient &1& éléments.',
  comment1: 'Les éléments seront séparés par un point-virgule.',
  comment2: 'Tu n’as qu’une seule tentative pour répondre.',
  corr1_1: '$E\\cup F$ est l’union des ensembles $E$ et $F$ c’est-à-dire l’ensemble des éléments qui appartient à $E$ ou à $F$.',
  corr2_1: 'Ainsi $E\\cup F=£{cup}$.',
  corr1_2: '$E\\cap F$ est l’intersection des ensembles $E$ et $F$ c’est-à-dire l’ensemble des éléments qui appartient à la fois à $E$ et à $F$.',
  corr2_2: 'Ainsi $E\\cap F=£{cap}$.',
  corr3_1: 'Un triplet est une collection ordonnée d’objets, de la forme $£t$, ... On peut alors en construire $£{triplet}$.',
  corr3_2: 'Un quadruplet est une collection ordonnée d’objets, de la forme $£q$, ... On peut alors en construire $£{quadruplet}$.',
  corr4: '$E\\times F$ est l’ensemble des couples $(x;y)$ avec $x\\in E$ et $y\\in F$. Il contient $£p$ éléments.'
}
/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour chaque sujet
 * @private
 * @return {Object}
 */
function genereAlea () {
  const obj = {}
  const ensTotal = 'abcdefghijklmnop'.split('')
  do {
    let effE, effF
    do {
      effE = j3pGetRandomInt(4, 7)
      effF = j3pGetRandomInt(12, 14) - effE
    } while (effE >= effF)
    const choixEensTotal = j3pShuffle(ensTotal)
    obj.ensE = choixEensTotal.slice(0, effE)
    obj.ensE.sort()
    const choixFensTotal = j3pShuffle(ensTotal)
    obj.ensF = choixFensTotal.slice(0, effF)
    obj.ensF.sort()
    obj.inter = []
    obj.ensE.forEach(elt => {
      if (obj.ensF.includes(elt)) obj.inter.push(elt)
    })
  } while ((obj.inter.length <= 2) && (obj.inter.length > Math.min(obj.ensE.length, obj.ensF.length) - 2))
  obj.union = [...obj.ensE]
  obj.ensF.forEach(elt => {
    if (!obj.ensE.includes(elt)) obj.union.push(elt)
  })
  obj.e = '\\opencurlybrace ' + obj.ensE.join(';') + '\\closecurlybrace'
  obj.f = '\\opencurlybrace ' + obj.ensF.join(';') + '\\closecurlybrace'
  obj.cup = '\\opencurlybrace ' + obj.union.join(';') + '\\closecurlybrace'
  obj.cap = '\\opencurlybrace ' + obj.inter.join(';') + '\\closecurlybrace'
  obj.reptriplet = Math.pow(obj.ensE.length, 3)
  obj.triplet = obj.ensE.length + '\\times ' + obj.ensE.length + '\\times ' + obj.ensE.length + '=' + obj.reptriplet
  obj.t = '$(' + obj.ensE[0] + ', ' + obj.ensE[0] + ', ' + obj.ensE[0] + ')$, $(' + obj.ensE[0] + ', ' + obj.ensE[0] + ', ' + obj.ensE[1] + ')$, $(' + obj.ensE[0] + ', ' + obj.ensE[0] + ', ' + obj.ensE[2] + ')$'
  obj.repquadruplet = Math.pow(obj.ensE.length, 4)
  obj.quadruplet = obj.ensE.length + '\\times ' + obj.ensE.length + '\\times ' + obj.ensE.length + '\\times ' + obj.ensE.length + '=' + obj.repquadruplet
  obj.q = '$(' + obj.ensE[0] + ', ' + obj.ensE[0] + ', ' + obj.ensE[0] + ', ' + obj.ensE[0] + ')$, $(' + obj.ensE[0] + ', ' + obj.ensE[0] + ', ' + obj.ensE[0] + ', ' + obj.ensE[1] + ')$, $(' + obj.ensE[0] + ', ' + obj.ensE[0] + ', ' + obj.ensE[0] + ', ' + obj.ensE[2] + ')$'
  obj.repproduit = obj.ensE.length * obj.ensF.length
  obj.p = obj.ensE.length + '\\times ' + obj.ensF.length + '=' + obj.repproduit
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  const ds = me.donneesSection
  me.validOnEnter = true
  // ex donneesSection.touche_entree
  ds.nbetapes = (ds.typeQuestions === 'les deux') ? 2 : 1
  ds.nbitems = ds.nbetapes * ds.nbrepetitions
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  stor.listeQuest = []
  for (let i = 1; i <= ds.nbrepetitions; i++) {
    if (ds.nbetapes === 2) stor.listeQuest.push('inter-union', 'p-uplet et produit')
    else stor.listeQuest.push(ds.typeQuestions)
  }
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  if ((ds.nbetapes === 1) || (me.questionCourante % ds.nbetapes === 1)) stor.objDonnees = genereAlea()
  // on affiche l’énoncé
  ;[1, 2, 3, 4, 5, 6].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })
  j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objDonnees)
  stor.zoneInput = []
  const zonesValidePerso = []
  if (stor.listeQuest[me.questionCourante - 1] === 'inter-union') {
    j3pAffiche(stor.zoneCons2, '', textes.consigne2, stor.objDonnees)
    const elt1 = j3pAffiche(stor.zoneCons3, '', '$E\\cup F=\\opencurlybrace $&1& $\\closecurlybrace$', stor.objDonnees)
    const elt2 = j3pAffiche(stor.zoneCons4, '', '$E\\cap F=\\opencurlybrace $&1& $\\closecurlybrace$', stor.objDonnees)
    stor.zoneInput.push(elt1.inputmqList[0], elt2.inputmqList[0])
    stor.zoneInput.forEach(input => {
      mqRestriction(input, stor.objDonnees.union.join('') + ';')
      input.typeReponse = ['texte']
    })
    zonesValidePerso.push(elt1.inputmqList[0].id, elt2.inputmqList[0].id)
    ;[3, 4].forEach(i => j3pStyle(stor['zoneCons' + i], { paddingBottom: '10px' }))
    ;[5, 6].forEach(i => j3pStyle(stor['zoneCons' + i], { fontStyle: 'italic', fontSize: '0.9em' }))
    j3pAffiche(stor.zoneCons5, '', textes.comment1)
    if (ds.nbchances === 1) j3pAffiche(stor.zoneCons6, '', textes.comment2)
  } else {
    stor.choixQuest = j3pGetRandomInt(1, 2)
    const elt1 = j3pAffiche(stor.zoneCons2, '', textes['consigne3_' + stor.choixQuest], stor.objDonnees)
    const elt2 = j3pAffiche(stor.zoneCons3, '', textes.consigne4, stor.objDonnees)
    stor.zoneInput.push(elt1.inputmqList[0], elt2.inputmqList[0])
    stor.zoneInput.forEach(input => {
      mqRestriction(input, '\\d')
      input.typeReponse = ['nombre', 'exact']
    })
    stor.zoneInput[0].reponse = (stor.choixQuest === 1) ? [stor.objDonnees.reptriplet] : [stor.objDonnees.repquadruplet]
    stor.zoneInput[1].reponse = [stor.objDonnees.repproduit]
    j3pStyle(stor.zoneCons4, { fontStyle: 'italic', fontSize: '0.9em', paddingTop: '10px' })
    if (ds.nbchances === 1) j3pAffiche(stor.zoneCons4, '', textes.comment2)
  }
  stor.fctsValid = new ValidationZones({
    parcours: me,
    zones: stor.zoneInput.map(elt => elt.id),
    validePerso: zonesValidePerso
  })
  if (stor.listeQuest[me.questionCourante - 1] !== 'inter-union') {
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
  }

  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
  j3pFocus(stor.zoneInput[0])

  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  if (stor.listeQuest[me.questionCourante - 1] !== 'inter-union') {
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
  }
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  ;[1, 2, 3, 4].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
  if (stor.listeQuest[me.questionCourante - 1] === 'inter-union') {
    j3pAffiche(stor.zoneExpli1, '', textes.corr1_1)
    j3pAffiche(stor.zoneExpli2, '', textes.corr2_1, stor.objDonnees)
    j3pAffiche(stor.zoneExpli3, '', textes.corr1_2)
    j3pAffiche(stor.zoneExpli4, '', textes.corr2_2, stor.objDonnees)
  } else {
    j3pAffiche(stor.zoneExpli1, '', textes['corr3_' + stor.choixQuest], stor.objDonnees)
    j3pAffiche(stor.zoneExpli2, '', textes.corr4, stor.objDonnees)
  }
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  let reponse = { aRepondu: false, bonneReponse: false }
  if (stor.listeQuest[me.questionCourante - 1] === 'inter-union') {
    reponse.aRepondu = stor.fctsValid.valideReponses()
    if (reponse.aRepondu) {
      // Je vérifie maintenant si les ensembles sont les bons
      const repUnion = stor.fctsValid.zones.reponseSaisie[0].split(';')
      const repInter = stor.fctsValid.zones.reponseSaisie[1].split(';')
      // validation de la réponse sur l’union
      stor.fctsValid.zones.bonneReponse[0] = (stor.objDonnees.union.length === repUnion.length)
      // Je vérifie que tous les éléments de l’union sont dans la réponse
      if (stor.fctsValid.zones.bonneReponse[0]) stor.fctsValid.zones.bonneReponse[0] = stor.objDonnees.union.every(elt => repUnion.includes(elt))
      stor.fctsValid.zones.bonneReponse[1] = (stor.objDonnees.inter.length === repInter.length)
      // Je vérifie que tous les éléments de l’inter sont dans la réponse
      if (stor.fctsValid.zones.bonneReponse[1]) stor.fctsValid.zones.bonneReponse[1] = stor.objDonnees.inter.every(elt => repInter.includes(elt))
      reponse.bonneReponse = stor.fctsValid.zones.bonneReponse.every(elt => elt)
      stor.fctsValid.zones.inputs.forEach(idZone => stor.fctsValid.coloreUneZone(idZone))
    }
  } else {
    reponse = stor.fctsValid.validationGlobale()
  }
  if (reponse.aRepondu) {
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
