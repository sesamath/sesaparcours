import { j3pArrondi, j3pNombre, j3pFocus, j3pRandomTab, j3pAddElt, j3pStyle, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { constructionTabIntervalle } from 'src/legacy/outils/fonctions/gestionParametres'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient } from 'src/legacy/core/functionsLatex'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juillet 2017
        Une loi exponentielle étant définie, on demande la valeur de l’espérance de la loi
        Cette section peut être dans la continuité de loiExpoTrouverLambda et loiExpoEsperance
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['lambda', '[0.55;2.50]', 'string', 'Intervalle où se trouve le paramètre lambda de la loi exponentielle (avec ce type d’intervalle, il peut être décimal). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]" pour qu’il soit égal à 2 dans un premier temps, entier positif pour la deuxième répétition. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.'],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la suite étudiée est celle du nœud précédent.']
  ],
  // pour fonctionnement dans editgraphes
  donnees_parcours_description: 'Cette section peut réutiliser le paramètre lambda d’une autre section.',
  donnees_parcours_nom_variable: 'lambda'
}

/**
 * section loiExpoEsperance
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, { v: stor.param.nomVar })

    if (Math.abs(stor.zoneInput.reponse[0] - Math.round(stor.zoneInput.reponse[0] * 1000) / 1000) < Math.pow(10, -12)) {
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2_1, {
        v: stor.param.nomVar,
        r: Math.round(Math.pow(10, 3) * stor.zoneInput.reponse[0]) / Math.pow(10, 3),
        l: stor.param.lambdaDecimal
      })
    } else {
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2_2, {
        v: stor.param.nomVar,
        r: Math.round(Math.pow(10, 3) * stor.zoneInput.reponse[0]) / Math.pow(10, 3),
        l: stor.param.lambdaDecimal
      })
    }
  }

  function suite2 () {
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })//, coord:[0,0]
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (stor.param.trouverLambdaAvant) {
      j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1,
        {
          v: stor.param.nomVar,
          p: stor.param.probaDonnee,
          l: '-\\frac{\\ln(' + stor.param.valProba + ')}{' + stor.param.borne + '}'
        })
      stor.param.lambdaDecimal = j3pArrondi(-Math.log(stor.param.valProba) / stor.param.borne, 3)
      j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2,
        {
          l: stor.param.lambdaDecimal
        })
    } else {
      if (!stor.param.nomVar) stor.param.nomVar = j3pRandomTab(['X', 'Y', 'Z', 'T'], [0.25, 0.25, 0.25, 0.25])
      stor.param.lambdaDecimal = Number(stor.param.lambda.expression[(me.questionCourante - 1) / ds.nbetapes])
      stor.param.trouverLambdaAvant = false
      // consigne3 : "Soit $£v$ la variable aléatoire suivant une loi exponentielle de paramètre $£l$.",
      const consigne1 = (ds.donneesPrecedentes) ? ds.textes.consigne3_2 : ds.textes.consigne3
      j3pAffiche(stor.zoneCons1, '', consigne1, {
        v: stor.param.nomVar,
        p: stor.param.probaDonnee,
        l: stor.param.lambdaDecimal
      })
    }
    const laReponse = j3pCalculValeur(j3pGetLatexQuotient('1', stor.param.lambdaDecimal))
    const laConsigne = (Math.abs(laReponse - Math.round(laReponse * 1000) / 1000) < Math.pow(10, -12)) ? ds.textes.consigne4_1 : ds.textes.consigne4_2
    const elt = j3pAffiche(stor.zoneCons3, '', laConsigne,
      {
        v: stor.param.nomVar,
        inputmq1: { texte: '' }
      })
    me.logIfDebug('laReponse :', laReponse)
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d.,-')
    if (Math.abs(laReponse - Math.round(laReponse * 1000) / 1000) < Math.pow(10, -12)) {
      stor.zoneInput.typeReponse = ['nombre', 'exact']
    } else {
      stor.zoneInput.typeReponse = ['nombre', 'arrondi', 0.001]
    }
    stor.zoneInput.reponse = [laReponse]
    j3pFocus(stor.zoneInput)
    // on Exporte les variables présentes dans cette section pour qu’elles soient utilisées dans une autre section
    me.donneesPersistantes.loiExpo = {}
    Object.entries(stor.param).forEach(([prop, value]) => {
      me.donneesPersistantes.loiExpo[prop] = value
    })
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })

    // Obligatoire
    me.finEnonce()
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      lambda: '[0.55;2.50]',
      donneesPrecedentes: false,

      /*
      Les textes présents dans la section
      Sont donc accessibles dans le code par donneesSection.textes.consigne1
      Possibilité d’écrire dans le code :
      var lestextes: donneesSection.textes;
      puis accès à l’aide de lestextes.consigne1
      */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Espérance d’une loi exponentielle',
        // on donne les phrases de la consigne
        // les premières consignes ne vont être affichées que si on reprend les données de la section loiExpoTrouverLambda
        consigne1: '$£v$ étant une variable aléatoire suivant une loi exponentielle de paramètre $\\lambda>0$ et vérifiant $£p$, on a montré dans la question précédente que $\\lambda=£l$.',
        consigne2: 'Pour cette question, on prend $£l$ pour valeur de $\\lambda$.',
        // consigne3 est la consigne lorsqu’on n’utilise pas auparavant la section loiExpoTrouverLambda
        consigne3: 'Soit $£v$ une variable aléatoire suivant une loi exponentielle de paramètre $£l$.',
        consigne3_2: '$£v$ est toujours la variable aléatoire suivant la loi exponentielle de paramètre $£l$.',
        consigne4_1: 'L’espérance de la variable aléatoire $£v$ vaut &1&.',
        consigne4_2: 'La valeur arrondie à $10^{-3}$ de l’espérance de la variable aléatoire $£v$ vaut &1&.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Peut-être est-ce un problème d’arrondi&nbsp;?',
        comment2: 'L’espérance d’une variable aléatoire suivant une loi exponentielle ne peut être négative !',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'D’après le cours, $£v$ suivant une loi exponentielle de paramètre $\\lambda$, l’espérance de $£v$ vaut $\\frac{1}{\\lambda}$.',
        corr2_1: 'Ainsi $E(£v)=\\frac{1}{£l}= £r$.',
        corr2_2: 'Ainsi $E(£v)=\\frac{1}{£l}\\approx £r$.'
      },
      pe: 0
    }
  }
  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        this.surcharge()
        // Construction de la page
        this.construitStructurePage({ structure: ds.structure })
        /*
         Par convention,`
        `   this.typederreurs[0] = nombre de bonnes réponses
            this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            this.typederreurs[2] = nombre de mauvaises réponses
            this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        this.afficheTitre(ds.textes.titre_exo)
        if (ds.indication) this.indication(this.zones.IG, ds.indication)
        // code de création des fenêtres
        if (ds.donneesPrecedentes) ds.nbrepetitions = 1
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        stor.param = {}
        if (ds.donneesPrecedentes) {
          // on récupère les données du noeud précédent
          if (!me.donneesPersistantes.loiExpo) {
            ds.donneesPrecedentes = false
            j3pShowError(Error('Les données précédentes n’ont pu être récupérées. De nouvelles sont générées !'))
          }
        }
        if (ds.donneesPrecedentes) {
          Object.entries(me.donneesPersistantes.loiExpo).forEach(([prop, value]) => {
            stor.param[prop] = value
          })
        } else {
          stor.param.lambda = constructionTabIntervalle(ds.lambda, '[0.55;2.50]', ds.nbrepetitions, [1])
          me.logIfDebug('stor.param.lambda:', stor.param.lambda)
        }
        // chargement mtg
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              suite2()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
        suite2()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!this.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          this.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            this._stopTimer()
            this.score++
            stor.zoneCorr.style.color = this.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            this.typederreurs[0]++
            this.cacheBoutonValider()
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = this.styles.cfaux

            // A cause de la limite de temps :
            if (this.isElapsed) { // limite de temps
              this._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              this.typederreurs[10]++
              this.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // je vérifie si l’élève n’a pas donné une valeur approchée plutôt qu’un arrondi
              if (j3pNombre(fctsValid.zones.reponseSaisie[0]) < 0) {
              // il m’a donné une valeur négative
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
              } else if (Math.abs(stor.zoneInput.reponse[0] - j3pNombre(fctsValid.zones.reponseSaisie[0])) <= 0.001) {
              // c’est que l’expression entrée est une valeur approchée à 10^{-3} et non un arrondi
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                this.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                this._stopTimer()
                this.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                this.typederreurs[2]++
                this.etat = 'navigation'
                this.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
