import { j3pAddElt, j3pAjouteBouton, j3pNombre, j3pDetruit, j3pFocus, j3pGetRandomElt, j3pGetRandomInt, j3pVirgule, j3pGetNewId, j3pStyle, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        août 2020
        On demande de calculer une proba en lien avec une v.a. suivant une loi géométrique
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['egaliteSeulement', false, 'boolean', 'On peut n’imposer que des probabilités de la forme P(X=k). Par défaut, on aura aussi P(X>k) ou P(X<=k).'],
    ['memoire', false, 'boolean', 'Lorsque ce paramètre vait true, alors on demande de calculer une probabilité conditionnelle.']
  ]
}

/**
 * section probaLoiGeo
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAALgAAACVAAAAQEAAAAAAAAAAQAAAFz#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAFAABALdYEGJN0vUAt1gQYk3S9#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAAA4AAVYAwAAAAAAAAABAEAAAAAAAAAUAAUA#FP3ztkWiAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAEAAAABAAAAA#####8AAAABAAdDTWlsaWV1AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAABAAAAA#####8AAAACAAxDQ29tbWVudGFpcmUA#####wEAAAAAAAAAAAAAAABAGAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUA0AAAAAAAAQEzcKPXCj1gAAAADAP####8BAAAAARAAAAEAAQAAAAgBP#AAAAAAAAAAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQFhAAAAAAABAPrhR64UesP####8AAAABAA9DU3ltZXRyaWVBeGlhbGUA#####wAAAAn#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAALAAAABQD#####AAAAAAAQAAABAAEAAAAIAAAACgAAAAUA#####wAAAAAAEAAAAQABAAAADAAAAAgAAAADAP####8BAAAAARAAAAEAAQAAAAwBP#AAAAAAAAAAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQD0AAAAAAAAAAAAPAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAADAAAABD#####AAAAAQAIQ1ZlY3RldXIA#####wEAAAAAEAAAAQABAAAACAAAABAA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAABIAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAgAAAATAAAABQD#####AAAAAAAQAAABAAEAAAAUAAAAFQAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAATAAAABQD#####AAAAAAAQAAABAAEAAAAVAAAAFwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAABUAAAATAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAFwAAABMAAAAFAP####8AAAAAABAAAAEAAQAAABkAAAAaAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAFAAAABMAAAAFAP####8AAAAAABAAAAEAAQAAABwAAAAZAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAGQAAABMAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAaAAAAEwAAAAUA#####wAAAAAAEAAAAQABAAAAHgAAAB8AAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAcAAAAEwAAAAUA#####wAAAAAAEAAAAQABAAAAIQAAAB4AAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAARAAAAEwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAACMAAAATAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAJAAAABMAAAADAP####8BAAAAARAAAAEAAQAAAAoBP#AAAAAAAAD#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAARAAAAD#####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAmAAAAJwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAACgAAAATAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAKQAAABMAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAqAAAAEwAAAA0A#####wEAAAAAEAAAAQABAAAAEAAAAA8AAAAOAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAmAAAALAAAAA0A#####wEAAAAAEAAAAQABAAAAFwAAAAkAAAAOAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAmAAAALgAAAAUA#####wAAAAAAEAAAAQABAAAALQAAAC8AAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAtAAAAEwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAC8AAAATAAAABQD#####AAAAAAAQAAABAAEAAAAxAAAAMgAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAADEAAAATAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAMgAAABMAAAAFAP####8AAAAAABAAAAEAAQAAADQAAAA1AAAABwD#####AAAAAAEAAAAoEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUEAAAAHAP####8AAAAAAQAAACkQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAAcA#####wAAAAABAAAAKhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFBAAAABwD#####AAAAAAEAAAArEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAy4uLgAAAAcA#####wAAAAABAAAAJRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAMuLi4AAAAHAP####8AAAAAAEAYAAAAAAAAAAAAAAAAAAAAAAAvEAAAAAAAAAAAAAEAAAABAAAAAAAAAAAAA1g9MQAAAAcA#####wAAAAAAQBAAAAAAAAAAAAAAAAAAAAAAADIQAAAAAAAAAAAAAQAAAAEAAAAAAAAAAAADWD0yAAAABwD#####AAAAAABAEAAAAAAAAAAAAAAAAAAAAAAANRAAAAAAAAAAAAABAAAAAQAAAAAAAAAAAANYPTMAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQFnAAAAAAABAUq4UeuFHrAAAAAMA#####wEAAAABEAAAAQABAAAAPwE#8AAAAAAAAP####8AAAACAAlDQ2VyY2xlT1IA#####wEAAAAAAQAAAD8AAAABP+AAAAAAAAAA#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAEAAAABB#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAABCAAAAEQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAQAAAEIAAAAFAP####8AAAAAABAAAAEAAQAAAD8AAABEAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAARAAAABMAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAA#AAAAEwAAAAUA#####wAAAAAAEAAAAQABAAAARwAAAEYAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAABHAAAAEwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAEYAAAATAAAABQD#####AAAAAAAQAAABAAEAAABJAAAASgAAAAcA#####wAAAAABAAAAERAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFBAAAABwD#####AAAAAAEAAAAjEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUEAAAAHAP####8AAAAAAQAAACQQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAgAAAAKAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACAAAAAwAAAAGAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAQAAAAFwAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAABQAAAAQAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAGQAAABoAAAAGAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAZAAAAHP####8AAAABAAdDQ2FsY3VsAP####8AAXAAAzAuMQAAAAE#uZmZmZmZmgAAAAcA#####wAAAAABAAAATxAAAAAAAAIAAAACAAAAAQAAAAAAAAAAAAcjVmFsKHApAAAABwD#####AAAAAAEAAABREAAAAAAAAgAAAAIAAAABAAAAAAAAAAAAByNWYWwocCkAAAAHAP####8AAAAAAQAAAFMQAAAAAAACAAAAAgAAAAEAAAAAAAAAAAAHI1ZhbChwKQAAAAcA#####wAAAAAAQBQAAAAAAAAAAAAAAAAAAAAAAFAQAAAAAAACAAAAAAAAAAEAAAAAAAAAAAAJMS0jVmFsKHApAAAABwD#####AAAAAABAHAAAAAAAAEAIAAAAAAAAAAAAUhAAAAAAAAIAAAAAAAAAAQAAAAAAAAAAAAkxLSNWYWwocCkAAAAHAP####8AAAAAAEAcAAAAAAAAP#AAAAAAAAAAAABUEAAAAAAAAgAAAAAAAAABAAAAAAAAAAAACTEtI1ZhbChwKQAAAAf##########w=='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
  }
  function modifFig () {
    // 55-56-57-76-77-78 pour l’événément, 60 pour X=1, 61 pour X=2 et 62 pour X=3
    for (let i = 1; i <= 3; i++) {
      stor.mtgAppLecteur.setText(stor.mtg32svg, 59 + i, stor.objCons.X + '=' + i)
    }
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'p', String(stor.objCons.p))
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
    let corr2
    if (!ds.memoire) {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, stor.objCons)
      const largFig = 466
      const hautFig = 194
      // création du div accueillant la figure mtg32
      stor.zoneMtg32 = j3pAddElt(stor.zoneExpli2, 'div')
      // et enfin la zone svg (très importante car tout est dedans)
      stor.mtg32svg = j3pGetNewId('mtg32svg')
      j3pCreeSVG(stor.zoneMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
      depart()
      modifFig()
      if (stor.typeQuestion === 'egalite') {
        j3pAffiche(stor.zoneExpli3, '', ds.textes.corr2, stor.objCons)
      } else {
        corr2 = (stor.objCons.s === '>') ? ds.textes.corr3_1 : ds.textes.corr3_2
        j3pAffiche(stor.zoneExpli3, '', corr2, stor.objCons)
      }
    } else {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr4, stor.objCons)
      corr2 = (stor.objCons.s === '>') ? ds.textes.corr5_1 : ds.textes.corr5_2
      j3pAffiche(stor.zoneExpli2, '', corr2, stor.objCons)
    }
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.
      memoire: false,
      egaliteSeulement: false,
      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Probabilité avec une loi géométrique',
        // on donne les phrases de la consigne
        consigne1: 'Soit $£X$ une variable aléatoire suivant une loi géométrique de paramètre £{p1}.',
        consigne2: '$P(£X=£k)\\approx$ &1&.',
        consigne3: '$P(£X£s£k)\\approx$ &1&.',
        consigne4: '$P_{£X>£{k1}}(£X£s£{k2})\\approx$ &1&.',
        indic: 'On donnera l’arrondi à $10^{-£a}$.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Peut-être est-ce un problème d’arrondi&nbsp;?',
        comment2: 'On rappelle qu’une probabilité est un nombre réel compris entre 0 et 1 !',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'La situation peut être représentée par un arbre comme celui-ci avec les 3 premières répétitions (A désigne le succès dans le cas de la loi géométrique suivie par $£X$, sa probabilité vaut ainsi £{p1}). ',
        corr2: '$P(£X=£k)=(1-£p)^{calcule[£k-1]}\\times £p\\approx £r$.',
        corr3_1: '$P(£X>£k)=(1-£p)^{£k}\\approx £r$.',
        corr3_2: '$P(£X\\leq £k)=1-P(£X>£k)=1-(1-£p)^{£k}\\approx £r$.',
        corr4: 'Une loi géométrique est une loi sans mémoire c’est-à-dire que pour tous entiers $s$ et $t$, $P_{£X>t}(£X>t+s)=P(£X>s)$.',
        corr5_1: 'Ainsi $P_{£X>£{k1}}(£X>£{k2})=P_{£X>£{k1}}(£X>£{k1}+calcule[£{k2}-£{k1}])=P(£X>£k)=(1-£p)^{£k}\\approx £r$.',
        corr5_2: 'Ainsi $P_{£X>£{k1}}(£X\\leq £{k2})=1-P_{£X>£{k1}}(£X>£{k2})=1-P_{£X>£{k1}}(£X>£{k1}+£k)$<br/>$P_{£X>£{k1}}(£X\\leq £{k2})=1-P(£X>£k)=1-(1-£p)^{£k}\\approx £r$.'
      },
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    // Construction de la page

    // Le paramètre définit la largeur relative de la première colonne
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    if (!ds.memoire) {
      if (ds.egaliteSeulement) {
        stor.typeQuest = []
        for (let k = 0; k < ds.nbrepetitions; k++) stor.typeQuest.push('egalite')
      } else {
        stor.typeQuest = ['egalite']
        let typeQuest = ['inegalite']
        const typeQuestInit = ['egalite', 'inegalite']
        for (let k = 1; k < ds.nbrepetitions; k++) {
          if (typeQuest.length === 0) typeQuest = typeQuestInit.slice(0)
          const alea = j3pGetRandomInt(0, typeQuest.length - 1)
          stor.typeQuest.push(typeQuest[alea])
          typeQuest.splice(alea, 1)
        }
      }
      // console.log('stor.typeQuest :', stor.typeQuest)
    }
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    stor.objCons = {}
    stor.objCons.X = j3pGetRandomElt(['X', 'Y', 'Z'])
    stor.objCons.p = j3pGetRandomInt(5, 15) * 2 / 100
    const uneDecimale = (Math.abs(Math.round(stor.objCons.p * 10) - stor.objCons.p * 10) < Math.pow(10, -12))
    stor.objCons.p1 = j3pVirgule(stor.objCons.p)
    stor.objCons.a = 3 // pour l’arrondi)
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, stor.objCons)
    stor.objCons.s = j3pGetRandomElt(['>', '\\leq'])
    let laReponse
    if (!ds.memoire) {
      stor.typeQuestion = stor.typeQuest[me.questionCourante - 1]
      stor.objCons.k = j3pGetRandomInt((uneDecimale) ? 5 : 4, 7)
      const cons2 = (stor.typeQuestion === 'egalite') ? ds.textes.consigne2 : ds.textes.consigne3
      const elt = j3pAffiche(stor.zoneCons2, '', cons2,
        {
          X: stor.objCons.X,
          k: stor.objCons.k,
          s: stor.objCons.s,
          inputmq1: { texte: '', dynamique: true }
        })
      stor.zoneInput = elt.inputmqList[0]
      laReponse = (stor.typeQuestion === 'egalite')
        ? Math.pow(1 - stor.objCons.p, stor.objCons.k - 1) * stor.objCons.p
        : (stor.objCons.s === '>') ? Math.pow(1 - stor.objCons.p, stor.objCons.k) : 1 - Math.pow(1 - stor.objCons.p, stor.objCons.k)
    } else {
      stor.objCons.k1 = j3pGetRandomInt(2, 4)
      stor.objCons.k2 = stor.objCons.k1 + j3pGetRandomInt(uneDecimale ? 5 : 4, 7)
      stor.objCons.k = stor.objCons.k2 - stor.objCons.k1
      const elt = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne4,
        {
          X: stor.objCons.X,
          k1: stor.objCons.k1,
          k2: stor.objCons.k2,
          s: stor.objCons.s,
          inputmq1: { texte: '', dynamique: true }
        })
      stor.zoneInput = elt.inputmqList[0]
      laReponse = (stor.objCons.s === '>') ? Math.pow(1 - stor.objCons.p, stor.objCons.k) : 1 - Math.pow(1 - stor.objCons.p, stor.objCons.k)
    }
    mqRestriction(stor.zoneInput, '\\d,.-')
    j3pAffiche(stor.zoneCons3, '', ds.textes.indic, { style: { fontSize: '0.9em', fontStyle: 'italic' }, a: stor.objCons.a })
    // console.log('laReponse:', laReponse)
    stor.objCons.r = Math.round(laReponse * Math.pow(10, stor.objCons.a)) / Math.pow(10, stor.objCons.a)
    stor.zoneInput.typeReponse = ['nombre', 'arrondi', Math.pow(10, -stor.objCons.a)]
    stor.zoneInput.reponse = [laReponse]
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    const zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCorr = j3pAddElt(zoneMD, 'div', '')
    stor.zoneCalc = j3pAddElt(zoneMD, 'div', '')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    j3pFocus(stor.zoneInput)
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if ((j3pNombre(stor.fctsValid.zones.reponseSaisie[0]) < 0) || (j3pNombre(stor.fctsValid.zones.reponseSaisie[0]) > 1)) {
              // c’est que l’élève doraph32JSnne un résultats incohérent (proba qui n’est pas entre 0 et 1)
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
              } else if (Math.abs(stor.zoneInput.reponse[0] - j3pNombre(stor.fctsValid.zones.reponseSaisie[0])) <= Math.pow(10, -(stor.objCons.a - 1))) {
              // c’est que l’expression entrée est une valeur approchée à 10^{-a} et non un arrondi
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
