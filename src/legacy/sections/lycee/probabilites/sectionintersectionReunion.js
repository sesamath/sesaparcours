import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pRandomTab, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import tabDoubleEntree from 'src/legacy/outils/tableauxProba/tabDoubleEntree'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Avril 2019
        A l’aide d’un tableau à double entrée, on demande de calculer des probabilités d’intersection, de réunion d’intervalles
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuestions', [1, 2, 3, 4], 'array', 'Tableau qui contient les questions choisies (dans l’ordre croissant):<br>- 1 pour la probabilité d’un événement et de son événement contraire;<br>- 2 pour la probabilité d’une intersection;<br>-3 pour la probabilité d’une réunion;<br>-4 pour la probabilité d’une intersection avec un événement contraire.']
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pDetruit(stor.laPalette)
    const numQuest = ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes]// C’est le numéro de l’étape
    const tabCorr1 = ds.textes['corr1_' + numQuest + '_' + stor.quest].split('|')
    const laCorr1 = tabCorr1[stor.choixEvts[0]]
    let fracSimple1, affichageRep1, affichageRep2
    const laRep = stor.zoneInput[0].reponse[0]
    if (numQuest === 1) {
      fracSimple1 = j3pGetLatexQuotient(stor.effectifRep, stor.objTab.effTotal)
      const fracSimple2 = j3pGetLatexSomme('1', j3pGetLatexProduit('-1', j3pGetLatexQuotient(stor.effectifRep, stor.objTab.effTotal)))
      affichageRep1 = '\\frac{' + stor.effectifRep + '}{' + stor.objTab.effTotal + '}'
      affichageRep2 = '\\frac{' + (stor.objTab.effTotal - stor.effectifRep) + '}{' + stor.objTab.effTotal + '}'
      if (Math.abs(Math.round(10000 * laRep) / 10000 - laRep) < Math.pow(10, -12)) {
        const probaRep1 = Math.round(10000 * laRep) / 10000
        affichageRep1 += '=' + j3pVirgule(probaRep1)
        affichageRep2 = j3pVirgule(Math.round(10000 * (1 - probaRep1)) / 10000)
      } else {
        if (fracSimple1 !== affichageRep1) {
          affichageRep1 += '=' + fracSimple1
          affichageRep2 = fracSimple2
        }
      }
    } else {
      fracSimple1 = j3pGetLatexQuotient(stor.effectifRep, stor.objTab.effTotal)
      affichageRep1 = '\\frac{' + stor.effectifRep + '}{' + stor.objTab.effTotal + '}'
      if (Math.abs(Math.round(10000 * laRep) / 10000 - laRep) < Math.pow(10, -12)) {
        affichageRep1 += '=' + j3pVirgule(Math.round(10000 * laRep) / 10000)
      } else {
        if (fracSimple1 !== affichageRep1) {
          affichageRep1 += '=' + fracSimple1
        }
      }
    }
    if (numQuest === 1) {
      j3pAffiche(stor.zoneExpli1, '', laCorr1, {
        n: stor.objTab.effTotal,
        m: stor.effectifRep
      })
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2_1, {
        a: stor.nomEvtTxt,
        r1: affichageRep1,
        r2: affichageRep2
      })
    } else if (numQuest === 2) {
      j3pAffiche(stor.zoneExpli1, '', laCorr1, {
        n: stor.objTab.effTotal,
        m: stor.effectifRep
      })

      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2_2, {
        a: stor.nomEvtTxt,
        r: affichageRep1
      })
    } else if (numQuest === 3) {
      j3pAffiche(stor.zoneExpli1, '', laCorr1, {
        n: stor.objTab.effTotal,
        m: stor.effectifRep,
        a: stor.nomEvtTxt,
        l: stor.effectifLigne,
        k: stor.effectifColonne
      })
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2_2, {
        a: stor.nomEvtTxt,
        r: affichageRep1
      })
    } else {
      let laSomme = stor.tabSomme[0]
      if (stor.tabSomme.length > 1) {
        for (let i = 1; i < stor.tabSomme.length; i++) {
          laSomme += '+' + stor.tabSomme[i]
        }
        laSomme += '=' + stor.effectifRep
      }
      j3pAffiche(stor.zoneExpli1, '', laCorr1, {
        n: stor.objTab.effTotal,
        s: laSomme
      })
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2_2, {
        a: stor.nomEvtTxt,
        r: affichageRep1
      })
    }
    // je parcours l’ensemble des overline et je leur donne la couleur dans laquelle se trouve le texte
    $('.mq-overline').each(function () {
      $(this).css('border-top', '1px solid ' + $(this).css('color'))
      // je parcours l’ensemble des overline et je leur donne la couleur dans laquelle se trouve le texte
    })
  }

  function ecoute (num) {
    if (stor.zoneInput[num].className.includes('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput[num], { liste: ['fraction'] })
    }
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 4,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      typeQuestions: [1, 2, 3, 4],

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Probabilité d’intersection ou de réunion',
        // on donne les phrases de la consigne
        // Inspiré de Polynésie sept 2011 STG GRH
        consigne1_1: 'Dans un club sportif chaque membre ne pratique qu’un sport. Leur répartition est donnée dans le tableau suivant&nbsp;:',
        ligne1: 'VTT|Gymnastique|Badminton|Tir à l’arc',
        colonne1: 'Hommes|Femmes',
        consigne2_1: 'On choisit au hasard un membre du club sportif et on considère les évènements&nbsp;:',
        nomEvtsLigne1: 'V|G|B|T',
        nomEvtsColonne1: 'H|F',
        total: 'Total',
        evtsLigne1: 'la personne choisie fait du VTT&nbsp;;|la personne choisie fait de la gymnastique&nbsp;;|la personne choisie fait du badminton&nbsp;;|la personne choisie fait du tir à l’arc&nbsp;;',
        evtsColonne1: 'la personne choisie est un homme.',
        // Inspiré de Antilles sept 2011 STG GRH
        consigne1_2: 'On a interrogé un groupe d’étudiants titulaires d’un baccalauréat et qui ont poursuivi leurs études. Ces résultats sont consignés dans le tableau ci-dessous&nbsp;:',
        ligne2: 'BTS/IUT|Université|Autre formation',
        colonne2: 'Filles|Garçons',
        consigne2_2: 'On choisit au hasard un de ces étudiants et on considère les évènements&nbsp;:',
        nomEvtsLigne2: 'B|U|A',
        nomEvtsColonne2: 'F|G',
        evtsLigne2: 'l’étudiant choisi est en BTS ou IUT&nbsp;;|l’étudiant choisi est à l’université&nbsp;;|l’étudiant choisi est dans une autre formation&nbsp;;',
        evtsColonne2: 'l’étudiant choisi est une fille.',
        // Inspiré de Nouvelle Calédonie nov 2006 ACC
        consigne1_3: 'Le tableau ci-dessous donne le statut d’employés d’une entreprise obtenu suite à une enquête menée&nbsp;:',
        ligne3: 'Moins de 30 ans|Entre 30 et 40 ans|Plus de 40 ans',
        colonne3: 'Cadres|Non cadres',
        consigne2_3: 'On choisit au hasard un de ces employés et on considère les évènements&nbsp;:',
        nomEvtsLigne3: 'M|T|Q',
        nomEvtsColonne3: 'C|N',
        evtsLigne3: 'l’employé choisi a moins de 30 ans&nbsp;;|l’employé choisi a entre 30 et 40 ans&nbsp;;|l’employé choisi a plus de 40 ans&nbsp;;',
        evtsColonne3: 'l’employé choisi est un cadre.',
        // Inspiré de Polynésie juin 2005 ACC
        consigne1_4: 'Un disquaire propose dans un de ses rayons un choix de CD de catégories Rap, Soul ou Métal. Certains sont en langue française, les autres en langue anglaise comme indiqué dans le tableau ci-dessous&nbsp;:',
        ligne4: 'Rap|Soul|Métal',
        colonne4: 'Langue française|Langue anglaise',
        consigne2_4: 'On choisit au hasard un CD de ce rayon et on considère les évènements :',
        nomEvtsLigne4: 'R|S|M',
        nomEvtsColonne4: 'F|A',
        evtsLigne4: 'le CD choisi appartient à la catégorie Rap&nbsp;;|le CD choisi appartient à la catégorie Soul&nbsp;;|le CD choisi appartient à la catégorie Métal&nbsp;;',
        evtsColonne4: 'le CD choisi est en langue française.',
        // Inspiré de Polynésie septembre 2005 ACC
        consigne1_5: 'Afin de mieux connaître sa clientèle, une station de sports d’hiver a effectué une enquête dont les résultats sont donnés dans le tableau ci-dessous&nbsp;:',
        ligne5: 'Possède son matériel|Loue sur place|Loue ailleurs',
        colonne5: 'Vient 1 semaine par an|Vient tous les week-ends',
        consigne2_5: 'On choisit un client de la station et on considère les évènements :',
        nomEvtsLigne5: 'M|L|A',
        nomEvtsColonne5: 'S|W',
        evtsLigne5: 'le client choisi possède son matériel&nbsp;;|le client choisi loue son matériel sur place&nbsp;;|le client choisi loue son matériel ailleurs&nbsp;;',
        evtsColonne5: 'le client choisi vient une semaine par an.',
        consigne3: 'Que valent les probabilités suivantes&nbsp;?',
        consigne3Bis: 'Que vaut la probabilité suivante&nbsp;?',
        consigne4: '$P(\\text{£a})=$&1& et $P(\\overline{\\text{£a}})=$&2&.',
        info1: 'Les probabilités devront être écrites de manière simplifiée, sous la forme décimale ou d’une fraction irréductible.',
        info2: 'Tu n’as qu’une tentative pour répondre.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Les résultats doivent être exacts (non approchés) et donnés sous forme irréductible&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1_1_1: 'Parmi les £n membres du club, £m font du VTT.|Parmi les £n membres du club, £m font de la gymnastique.|Parmi les £n membres du club, £m font du badminton.|Parmi les £n membres du club, £m font du tir à l’arc.',
        corr2_1: 'Donc $P(\\text{£a})=£{r1}$ et $P(\\overline{\\text{£a}})=1-P(\\text{£a})=£{r2}$.',
        corr1_2_1: 'Parmi les £n membres du club, £m sont des hommes et font du VTT.|Parmi les £n membres du club, £m sont des hommes et font de la gymnastique.|Parmi les £n membres du club, £m sont des hommes et font du badminton.|Parmi les £n membres du club, £m sont des hommes et font du tir à l’arc.',
        corr2_2: 'Donc $P(£a)=£r$.',
        corr1_3_1: 'Parmi les £n membres du club, £m sont dans $£a$ : on prend les £l hommes ainsi que les membres qui font du VTT et qui n’ont pas encore été comptés (ils sont £k).|' +
          'Parmi les £n membres du club, £m sont dans $£a$ : on prend les £l hommes ainsi que les membres qui font de la gymnastique et qui n’ont pas encore été comptés (ils sont £k).|' +
          'Parmi les £n membres du club, £m sont dans $£a$ : on prend les £l hommes ainsi que les membres qui font du badminton et qui n’ont pas encore été comptés (ils sont £k).|' +
          'Parmi les £n membres du club, £m sont dans $£a$ : on prend les £l hommes ainsi que les membres qui font du tir à l’arc et qui n’ont pas encore été comptés (ils sont £k).',
        corr1_4_1: 'Parmi les £n membres du club, on cherche les hommes qui ne font pas de VTT. Ils sont au nombre de $£s$.|' +
          'Parmi les £n membres du club, on cherche les hommes qui ne font pas de gymnastique. Ils sont au nombre de $£s$.|' +
          'Parmi les £n membres du club, on cherche les hommes qui ne font pas de badminton. Ils sont au nombre de $£s$.|' +
          'Parmi les £n membres du club, on cherche les hommes qui ne font pas de tir à l’arc. Ils sont au nombre de $£s$.',
        corr1_1_2: 'Parmi les £n étudiants, £m sont un BTS ou un IUT.|Parmi les £n étudiants, £m sont à l’université.|Parmi les £n étudiants, £m suivent une autre formation.',
        corr1_2_2: 'Parmi les £n étudiants, £m sont des filles et sont en BTS ou IUT.|Parmi les £n étudiants, £m sont des filles et sont à l’université.|Parmi les £n étudiants, £m sont des filles et suivent une autre formation.',
        corr1_3_2: 'Parmi les £n étudiants, £m sont dans $£a$ : on prend les £l filles ainsi que ceux qui sont en BTS ou IUT et qui n’ont pas encore été comptés (ils sont £k).|' +
          'Parmi les £n étudiants, £m sont dans $£a$ : on prend les £l filles ainsi que ceux qui sont à l’université et qui n’ont pas encore été comptés (ils sont £k).|' +
          'Parmi les £n étudiants, £m sont dans $£a$ : on prend les £l filles ainsi que ceux qui suivent une autre formation et qui n’ont pas encore été comptés (ils sont £k).',
        corr1_4_2: 'Parmi les £n étudiants, on cherche les filles qui ne sont pas en BTS ou IUT. Ils sont au nombre de $£s$.|' +
          'Parmi les £n étudiants, on cherche les filles qui ne sont pas à l’université. Ils sont au nombre de $£s$.|' +
          'Parmi les £n étudiants, on cherche les filles qui ne sont pas dans une autre formation. Ils sont au nombre de $£s$.',
        corr1_1_3: 'Parmi les £n employés, £m ont moins de 30 ans.|Parmi les £n employés, £m ont entre 30 et 40 ans.|Parmi les £n employés, £m ont plus de 40 ans.',
        corr1_2_3: 'Parmi les £n employés, £m sont des cadres et ont moins de 30 ans.|Parmi les £n étudiants, £m sont des cadres et ont entre 30 et 40 ans.|Parmi les £n étudiants, £m sont des cadres et ont plus de 40 ans.',
        corr1_3_3: 'Parmi les £n employés, £m sont dans $£a$ : on prend les £l cadres ainsi que ceux qui ont moins de 30 ans et qui n’ont pas encore été comptés (ils sont £k).|' +
          'Parmi les £n employés, £m sont dans $£a$ : on prend les £l cadres ainsi que ceux qui ont entre 30 et 40 ans et qui n’ont pas encore été comptés (ils sont £k).|' +
          'Parmi les £n employés, £m sont dans $£a$ : on prend les £l cadres ainsi que ceux qui ont plus de 40 ans et qui n’ont pas encore été comptés (ils sont £k).',
        corr1_4_3: 'Parmi les £n employés, on cherche les cadres qui n’ont pas moins de 30 ans. Ils sont au nombre de $£s$.|' +
          'Parmi les £n employés, on cherche les cadres qui n’ont pas entre 30 et 40 ans. Ils sont au nombre de $£s$.|' +
          'Parmi les £n employés, on cherche les cadres qui n’ont pas plus de 40 ans. Ils sont au nombre de $£s$.',
        corr1_1_4: 'Parmi les £n CD du rayon, £m appartiennent à la catégorie Rap.|Parmi les £n CD du rayon, £m appartiennent à la catégorie Soul.|Parmi les £n CD du rayon, £m appartiennent à la catégorie Métal.',
        corr1_2_4: 'Parmi les £n CD du rayon, £m sont en langue française et appartiennent à la catégorie Rap.|Parmi les £n CD du rayon, £m sont en langue française et appartiennent à la catégorie Soul.|Parmi les £n CD du rayon, £m sont en langue française et appartiennent à la catégorie Métal.',
        corr1_3_4: 'Parmi les £n CD du rayon, £m sont dans $£a$ : on prend les £l en langue française ainsi que ceux qui appartiennent à la catégorie Rap et qui n’ont pas encore été comptés (ils sont £k).|' +
          'Parmi les £n CD du rayon, £m sont dans $£a$ : on prend les £l en langue française ainsi que ceux qui appartiennent à la catégorie Soul et qui n’ont pas encore été comptés (ils sont £k).|' +
          'Parmi les £n CD du rayon, £m sont dans $£a$ : on prend les £l en langue française ainsi que ceux qui appartiennent à la catégorie Métal et qui n’ont pas encore été comptés (ils sont £k).',
        corr1_4_4: 'Parmi les £n CD du rayon, on cherche ceux qui sont en langue française et qui n’appartiennent pas à la catégorie Rap. Ils sont au nombre de $£s$.|' +
          'Parmi les £n CD du rayon, on cherche ceux qui sont en langue française et qui n’appartiennent pas à la catégorie Soul. Ils sont au nombre de $£s$.|' +
          'Parmi les £n CD du rayon, on cherche ceux qui sont en langue française et qui n’appartiennent pas à la catégorie Métal. Ils sont au nombre de $£s$.',
        corr1_1_5: 'Parmi les £n clients, £m possèdent leur matériel.|Parmi les £n clients, £m louent leur matériel sur place.|Parmi les £n clients, £m louent leur matériel ailleurs.',
        corr1_2_5: 'Parmi les £n clients, £m viennent une semaine par an et possèdent leur matériel.|Parmi les £n clients, £m viennent une semaine par an et louent leur matériel sur place.|Parmi les £n clients, £m viennent une semaine par an et louent leur matériel ailleurs.',
        corr1_3_5: 'Parmi les £n clients, £m sont dans $£a$ : on prend les £l qui viennent une semaine par an ainsi que ceux qui possèdent leur matériel et qui n’ont pas encore été comptés (ils sont £k).|' +
          'Parmi les £n clients, £m sont dans $£a$ : on prend les £l qui viennent une semaine par an ainsi que ceux qui louent leur matériel sur place et qui n’ont pas encore été comptés (ils sont £k).|' +
          'Parmi les £n clients, £m sont dans $£a$ : on prend les £l qui viennent une semaine par an ainsi que ceux qui louent leur matériel ailleurs et qui n’ont pas encore été comptés (ils sont £k).',
        corr1_4_5: 'Parmi les £n clients, on cherche ceux qui viennent une semaine par an et qui ne possèdent pas leur matériel. Ils sont au nombre de $£s$.|' +
          'Parmi les £n clients, on cherche ceux qui viennent une semaine par an et qui ne louent pas leur matériel sur place. Ils sont au nombre de $£s$.|' +
          'Parmi les £n clients, on cherche ceux qui viennent une semaine par an et qui ne louent pas leur matériel ailleurs. Ils sont au nombre de $£s$.'
      },

      pe: 0
    }
  }
  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.laPalette = j3pAddElt(stor.conteneur, 'div')
    stor.lesInfos = j3pAddElt(stor.conteneur, 'div')
    if ((me.questionCourante % ds.nbetapes === 1) || (ds.nbetapes === 1)) {
      if (stor.tabQuest.length === 0) {
        stor.tabQuest = [...stor.tabQuestInit]
      }
      const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
      const quest = stor.tabQuest[pioche]
      stor.tabQuest.splice(pioche, 1)
      stor.quest = quest
      const objTab = {}
      objTab.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
      objTab.lestyle = { styletexte: me.styles.toutpetit.enonce, couleur: me.styles.toutpetit.enonce.color }
      objTab.nomsLigne = ds.textes['ligne' + quest].split('|')
      objTab.nomsLigne.push(ds.textes.total)
      objTab.nomsColonne = ds.textes['colonne' + quest].split('|')
      objTab.nomsColonne.push(ds.textes.total)

      const tabEvts1 = ds.textes['nomEvtsLigne' + quest].split('|')
      const tabEvts2 = ds.textes['nomEvtsColonne' + quest].split('|')
      const tabEvtsTxt1 = ds.textes['evtsLigne' + quest].split('|')
      const tabEvtsTxt2 = ds.textes['evtsColonne' + quest].split('|')
      // Il nous faut maintenant gérer l’aléatoire
      let effTotal
      const effTabCol = []
      const // tableau contenant l’effectif total de chaque événement présent par colonne (donc de longueur tabEvts1.length)
        effTableLigne = []
      let borneInf, borneSup, effLigne1
      switch (quest) {
        case 1:
          effTotal = j3pRandomTab([2, 4, 5], [0.333, 0.333, 0.334]) * 100
          effTabCol.push(Math.floor(effTotal / 4) + j3pGetRandomInt(11, 30))
          effTabCol.push(Math.floor(effTotal / 4) - j3pGetRandomInt(11, 30))
          effTabCol.push(Math.floor(effTotal / 4) + j3pGetRandomInt(0, 20))
          effTabCol.push(effTotal - effTabCol[0] - effTabCol[1] - effTabCol[2])
          for (let i = 0; i < effTabCol.length; i++) {
            borneInf = Math.floor(effTabCol[i] / 3)
            borneSup = Math.ceil(2 * effTabCol[i] / 3)
            effLigne1 = j3pGetRandomInt(borneInf, borneSup)
            effTableLigne[i] = [effLigne1, effTabCol[i] - effLigne1]
          }
          break
        case 2:
          effTotal = j3pRandomTab([4, 5, 8], [0.333, 0.333, 0.334]) * 100
          effTabCol.push(Math.floor(effTotal / 3) + j3pGetRandomInt(11, 30))
          effTabCol.push(Math.floor(effTotal / 3) - j3pGetRandomInt(11, 30))
          effTabCol.push(effTotal - effTabCol[0] - effTabCol[1])
          for (let i = 0; i < effTabCol.length; i++) {
            borneInf = Math.floor(effTabCol[i] / 3)
            borneSup = Math.ceil(2 * effTabCol[i] / 3)
            effLigne1 = j3pGetRandomInt(borneInf, borneSup)
            effTableLigne[i] = [effLigne1, effTabCol[i] - effLigne1]
          }
          break
        case 3:
          effTotal = j3pRandomTab([5, 20, 10], [0.333, 0.333, 0.334]) * 100
          effTabCol.push(Math.floor(effTotal / 3) + j3pGetRandomInt(20, 40))
          effTabCol.push(Math.floor(effTotal / 3) - j3pGetRandomInt(5, 30))
          effTabCol.push(effTotal - effTabCol[0] - effTabCol[1])
          for (let i = 0; i < effTabCol.length; i++) {
            borneInf = Math.floor(effTabCol[i] / 3)
            borneSup = Math.ceil(2 * effTabCol[i] / 3)
            effLigne1 = j3pGetRandomInt(borneInf, borneSup)
            effTableLigne[i] = [effLigne1, effTabCol[i] - effLigne1]
          }
          break
        case 4:
          effTotal = j3pRandomTab([4, 2.5, 5], [0.333, 0.333, 0.334]) * 100
          effTabCol.push(Math.floor(effTotal / 3) + j3pGetRandomInt(5, 15))
          effTabCol.push(Math.floor(effTotal / 3) - j3pGetRandomInt(5, 15))
          effTabCol.push(effTotal - effTabCol[0] - effTabCol[1])
          for (let i = 0; i < effTabCol.length; i++) {
            borneInf = Math.floor(effTabCol[i] / 3)
            borneSup = Math.ceil(2 * effTabCol[i] / 3)
            effLigne1 = j3pGetRandomInt(borneInf, borneSup)
            effTableLigne[i] = [effLigne1, effTabCol[i] - effLigne1]
          }
          break
        case 5:
          effTotal = j3pRandomTab([10, 20, 5], [0.333, 0.333, 0.334]) * 100
          effTabCol.push(Math.floor(effTotal / 4) + j3pGetRandomInt(0, 10))
          effTabCol.push(Math.floor(effTotal / 2) + j3pGetRandomInt(-5, 5))
          effTabCol.push(effTotal - effTabCol[0] - effTabCol[1])
          borneInf = Math.floor(effTabCol[0] / 5)
          borneSup = Math.ceil(2 * effTabCol[0] / 5)
          effLigne1 = j3pGetRandomInt(borneInf, borneSup)
          effTableLigne[0] = [effLigne1, effTabCol[0] - effLigne1]
          borneInf = Math.floor(3 * effTabCol[0] / 5)
          borneSup = Math.ceil(4 * effTabCol[0] / 5)
          effLigne1 = j3pGetRandomInt(borneInf, borneSup)
          effTableLigne[1] = [effLigne1, effTabCol[1] - effLigne1]
          borneInf = Math.floor(3 * effTabCol[0] / 5)
          borneSup = Math.ceil(4 * effTabCol[0] / 5)
          effLigne1 = j3pGetRandomInt(borneInf, borneSup)
          effTableLigne[2] = [effLigne1, effTabCol[2] - effLigne1]
          break
      }
      const sommeLigne = []
      objTab.effectifs = {}
      for (let j = 0; j < effTableLigne[0].length; j++) {
        sommeLigne.push(0)
        objTab.effectifs['ligne' + (j + 1)] = []
      }
      for (let i = 0; i < effTabCol.length; i++) {
        for (let j = 0; j < effTableLigne[i].length; j++) {
          objTab.effectifs['ligne' + (j + 1)].push(effTableLigne[i][j])
          sommeLigne[j] += effTableLigne[i][j]
        }
      }
      for (let j = 0; j < effTableLigne[0].length; j++) {
        objTab.effectifs['ligne' + (j + 1)].push(sommeLigne[j])
      }
      // obj.effectifs['ligne'+i] contiendra les effectifs sur chaque ligne associée
      objTab.effectifs['ligne' + (effTableLigne[0].length + 1)] = [...effTabCol]
      objTab.effectifs['ligne' + (effTableLigne[0].length + 1)].push(effTotal)
      objTab.effTotal = effTotal
      objTab.effTableLigne = effTableLigne
      objTab.effTableColonne = effTabCol
      stor.objTab = objTab
      stor.tabEvts1 = tabEvts1
      stor.tabEvtsTxt1 = tabEvtsTxt1
      stor.tabEvts2 = tabEvts2
      stor.tabEvtsTxt2 = tabEvtsTxt2
      me.logIfDebug('objTab:', stor.objTab)
    }

    j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + stor.quest])
    tabDoubleEntree(stor.zoneCons2, stor.objTab)
    j3pAffiche(stor.zoneCons3, '', ds.textes['consigne2_' + stor.quest])
    const tabDivEvts = []
    for (let i = 0; i < stor.tabEvtsTxt1.length + stor.tabEvtsTxt2.length; i++) {
      tabDivEvts.push(j3pAddElt(stor.zoneCons4, 'div'))
    }
    for (let i = 0; i < stor.tabEvtsTxt1.length; i++) {
      j3pAffiche(tabDivEvts[i], '', stor.tabEvts1[i] + ' : ' + stor.tabEvtsTxt1[i])
    }
    for (let i = 0; i < stor.tabEvtsTxt2.length; i++) {
      j3pAffiche(tabDivEvts[i + stor.tabEvtsTxt1.length], '', stor.tabEvts2[i] + ' : ' + stor.tabEvtsTxt2[i])
    }
    // Pour chaque étape, je prédétermine les probabilités qui seront à calculer. Cela va bien entendu dépendre du sujet
    // Etape 1 : un evt et son contraire
    // Etape 2 : une intersection d’intervalles
    // Etape 3 : une réunion d’intervalles
    // Etape 4 : une intersection de la forme (A inter (B barre))
    let choixEvt1
    let choixEvt2 = 0
    if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 1) {
      choixEvt1 = j3pGetRandomInt(0, (stor.tabEvts1.length - 1))
      stor.nomEvt = stor.tabEvts1[choixEvt1]
      stor.nomEvtTxt = stor.nomEvt
      stor.effectifRep = stor.objTab.effectifs['ligne' + (stor.tabEvts2.length + 1)][choixEvt1]
    } else {
      choixEvt1 = j3pGetRandomInt(0, (stor.tabEvts1.length - 1))
      if (stor.quest === 1) {
        choixEvt2 = 0// Pour les hommes
      }
      stor.nomEvt = [stor.tabEvts1[choixEvt1], stor.tabEvts2[choixEvt2]]
      if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 2) {
        stor.nomEvtTxt = '\\text{' + stor.tabEvts1[choixEvt1] + '}\\cap \\text{' + stor.tabEvts2[choixEvt2] + '}'
        stor.effectifRep = stor.objTab.effTableLigne[choixEvt1][choixEvt2]
      } else if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 3) {
        stor.nomEvtTxt = '\\text{' + stor.tabEvts1[choixEvt1] + '}\\cup \\text{' + stor.tabEvts2[choixEvt2] + '}'
        stor.effectifLigne = stor.objTab.effectifs['ligne' + (choixEvt2 + 1)][stor.objTab.effectifs['ligne' + (choixEvt2 + 1)].length - 1]// effTableLigne[stor.objTab.effTableLigne.length-1][choixEvt2];
        // J’ai pris tous les effectifs de la ligne. Il faut que j’ajoute ceux de la colonne que je n’ai pas encore pris
        stor.effectifColonne = stor.objTab.effTableColonne[choixEvt1] - stor.objTab.effTableLigne[choixEvt1][choixEvt2]
        stor.effectifRep = stor.effectifLigne + stor.effectifColonne
      } else {
        stor.nomEvtTxt = '\\text{' + stor.tabEvts2[choixEvt2] + '}\\cap \\overline{\\text{' + stor.tabEvts1[choixEvt1] + '}}'
        stor.tabSomme = []
        stor.effectifRep = 0
        for (let i = 0; i < stor.objTab.effTableLigne.length; i++) {
          if (i !== choixEvt1) {
            stor.tabSomme.push(stor.objTab.effTableLigne[i][choixEvt2])
            stor.effectifRep += stor.objTab.effTableLigne[i][choixEvt2]
          }
        }
      }
    }
    stor.choixEvts = [choixEvt1, choixEvt2]
    const laCons5 = (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 1) ? ds.textes.consigne3 : ds.textes.consigne3Bis
    j3pAffiche(stor.zoneCons5, '', laCons5)

    if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 1) {
      const elt = j3pAffiche(stor.zoneCons6, '', ds.textes.consigne4,
        {
          a: stor.nomEvtTxt,
          inputmq1: { texte: '' },
          inputmq2: { texte: '' }
        })
      stor.zoneInput = [...elt.inputmqList]
    } else {
      const elt = j3pAffiche(stor.zoneCons6, '', '$P(£a)=$&1&.',
        {
          a: stor.nomEvtTxt,
          inputmq1: { texte: '' }
        })
      stor.zoneInput = [...elt.inputmqList]
    }
    const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
    // palette MQ :
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], {
      liste: ['fraction']
    })
    j3pStyle(stor.lesInfos, { paddingTop: '40px' })
    stor.zoneInput[0].reponse = [stor.effectifRep / stor.objTab.effTotal]
    if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 1) {
      for (let i = 1; i <= 2; i++) {
        mqRestriction(stor.zoneInput[i - 1], '\\d,.-/', { commandes: ['fraction'] })
        stor.zoneInput[i - 1].typeReponse = ['nombre', 'exact']
        stor.zoneInput[i - 1].solutionSimplifiee = ['reponse fausse', 'non valide']
        stor.zoneInput[i - 1].index = i - 1
      }
      stor.zoneInput[1].reponse = [1 - stor.effectifRep / stor.objTab.effTotal]
      me.logIfDebug('lesRéponses : ', stor.zoneInput[0].reponse, '   ', stor.zoneInput[1].reponse, '    avec effectif :', stor.effectifRep)
      for (let i = 1; i <= 2; i++) {
        $(stor.zoneInput[i - 1]).focusin(function () {
          ecoute(this.index)
        })
      }
    } else {
      mqRestriction(stor.zoneInput[0], '\\d,.-/', { commandes: ['fraction'] })
      stor.zoneInput[0].typeReponse = ['nombre', 'exact']
      stor.zoneInput[0].solutionSimplifiee = ['reponse fausse', 'non valide']
      me.logIfDebug('lesRéponses : ', stor.zoneInput[0].reponse, '     avec effectif :', stor.effectifRep)
    }
    ;[1, 2].forEach(i => {
      stor['zoneInfo' + i] = j3pAddElt(stor.lesInfos, 'div')
      j3pStyle(stor['zoneInfo' + i], { fontSize: '0.9em', fontStyle: 'italic' })
    })
    j3pAffiche(stor.zoneInfo1, '', ds.textes.info1)
    if (ds.nbchances === 1) j3pAffiche(stor.zoneInfo2, '', ds.textes.info2)
    j3pFocus(stor.zoneInput[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70
        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc / 100 })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.tabQuest = [1, 2, 3, 4, 5]
        stor.tabQuestInit = [...stor.tabQuest]
        // pour le nombre d’étapes, c’est en fait gérer par un tableau ds.typeQuestions
        // je vérifie que le tableau est bien construit (4 valeurs au max et dans l’ordre croissant)
        let index = 0
        const tabValPossibles = [1, 2, 3, 4]
        const tabValPresentes = []
        while (index < ds.typeQuestions.length) {
          if (tabValPossibles.indexOf(ds.typeQuestions[index]) === -1) {
            // la valeur en index n’est pas correcte donc on la vire
            ds.typeQuestions.splice(index, 1)
          } else {
            // Je vérifie également qu’elle n’a pas déjà été posée
            if (tabValPresentes.indexOf(ds.typeQuestions[index]) > -1) {
              // la valeur en index existe déjà dans le tableau, donc on la vire
              ds.typeQuestions.splice(index, 1)
            } else {
              tabValPresentes.push(ds.typeQuestions[index])
              index++
            }
          }
        }
        if (ds.typeQuestions.length === 0) {
          ds.typeQuestions = [1, 2, 3, 4, 5]
        }
        ds.nbetapes = ds.typeQuestions.length
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        // ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        const fracIrre = fctsValid.zones.reponseSimplifiee[0][1]
        if (!reponse.aRepondu && (stor.zoneInput[0].solutionSimplifiee[1] === 'non valide')) {
        // Si c’est un pb de fraction irréductible, la première fois, je donne un message d’avertissement et ensuite, je compte ça comme faux
          if (!fracIrre) {
            stor.zoneInput[0].solutionSimplifiee[1] = 'reponse fausse'
            if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 1) {
              stor.zoneInput[1].solutionSimplifiee[1] = 'reponse fausse'
            }
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (!fracIrre) {
          // c’est que la fraction de la quatrième zone de saisie est réductible
            msgReponseManquante = ds.textes.comment1
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!fracIrre) {
              // c’est que la fraction de la zone de saisie est réductible
                stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
        // Obligatoire
        me.finCorrection()
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
