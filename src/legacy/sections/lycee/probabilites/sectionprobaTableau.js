import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pPGCD, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import tabDoubleEntree from 'src/legacy/outils/tableauxProba/tabDoubleEntree'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Avril 2019
        A l’aide d’un tableau à double entrée, on demande de calculer des probabilités d’intersection, de réunion d’intervalles
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['reponsesSimplifiees', false, 'boolean', 'Par défaut, on n’impose pas que les réponses soient simplifiées, mais cela peut être modifié.']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection () {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    // Je mets les corrections entre les questions
    ;[1, 2].forEach(i => {
      stor['zoneExpli' + i] = j3pAddElt(stor['zoneCons' + (5 + 2 * (i - 1))], 'div')
      stor['zoneExpli' + i].style.color = (stor.fctsValid.zones.bonneReponse[i - 1]) ? me.styles.cbien : me.styles.toutpetit.correction.color
      ;[1, 2].forEach(k => {
        stor['zonePhrase' + ((i - 1) * 2 + k)] = j3pAddElt(stor['zoneExpli' + i], 'div')
      })
    })
    j3pDetruit(stor.lesInfos)
    j3pDetruit(stor.laPalette)
    if (stor.quest === 1) {
      // Dans le cas où on aurait un effectif égal à 1, alors on doit mettre filles ou garçons au singulier
      if (stor.objRep.effectif1 === 1) {
        const g = ds.textes.garcon.split('|')
        const f = ds.textes.fille.split('|')
        stor.objRep.correction1 = stor.objRep.correction1.replace(g[0], g[1])
        stor.objRep.correction1 = stor.objRep.correction1.replace(f[0], f[1])
      }
    }
    j3pAffiche(stor.zonePhrase1, '', stor.objRep.correction1, {
      e: stor.objRep.effTotal,
      f: stor.objRep.effectif1
    })
    j3pAffiche(stor.zonePhrase2, '', ds.textes.corr2, { r: stor.objRep.detailRep1 })
    j3pAffiche(stor.zonePhrase3, '', stor.objRep.correction2, {
      e: stor.objRep.effTotal,
      f: stor.objRep.effectif2,
      c: stor.leCalcul
    })
    j3pAffiche(stor.zonePhrase4, '', ds.textes.corr2, { r: stor.objRep.detailRep2 })
  }

  function ecoute (zone) {
    if (zone.className.includes('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, zone, { liste: ['fraction'] })
    }
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut
      reponsesSimplifiees: false,
      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Probabilité dans un tableau',
        // on donne les phrases de la consigne
        // Inspiré du manuel Magnard 2019 Première
        consigne1_1: 'Les élèves d’une classe de première sont répartis suivant leur âge et leur sexe comme l’indique le tableau ci-dessous&nbsp;:',
        ligne1: '15 ans|16 ans|17 ans',
        colonne1: 'Filles|Garçons',
        consigne2_1: 'On choisit au hasard un élève de la classe.',
        total: 'Total',
        quest1_1: 'La probabilité que ce soit une fille de 15 ans vaut &1&.|La probabilité que ce soit une fille de 16 ans vaut &1&.|La probabilité que ce soit une fille de 17 ans vaut &1&.|La probabilité que ce soit un garçon de 15 ans vaut &1&.|La probabilité que ce soit un garçon de 16 ans vaut &1&.|La probabilité que ce soit un garçon de 17 ans vaut &1&.',
        quest2_1: 'La probabilité que l’élève choisi ait 16 ans ou moins vaut &1&.|La probabilité que l’élève choisi ait 16 ans ou plus vaut &1&.',
        // Inspiré de Nouvelle Calédonie novembre 2002 ACC
        consigne1_2: 'Une étudiante fabrique chaque semaine un petit stock de bijoux fantaisie qu’elle vend en fin de semaine afin de s’assurer quelques revenus. Sa production hebdomadaire de bijoux se répartit comme suit&nbsp;:',
        ligne2: 'Colliers|Bracelets|Boucles d’oreilles',
        colonne2: 'Argentés|Dorés',
        consigne2_2: 'On choisit au hasard un bijou de sa production.',
        quest1_2: 'La probabilité que ce soit un collier argenté vaut &1&.|La probabilité que ce soit un bracelet argenté vaut &1&.|La probabilité que ce soit une paire de boucles d’oreilles argenté vaut &1&.|La probabilité que ce soit un collier doré vaut &1&.|La probabilité que ce soit un bracelet doré vaut &1&.|La probabilité que ce soit une paire de boucles d’oreilles dorée vaut &1&.',
        quest2_2: 'La probabilité que ce soit un collier ou un bracelet &1&.|La probabilité que ce soit un collier ou une paire de boucles d’oreilles &1&.|La probabilité que ce soit un bracelet ou une paire de boucles d’oreilles vaut &1&.',
        // Inspiré de Nouvelle Calédonie décembre 2000 ACC
        consigne1_3: 'Une entreprise, souhaitant mettre en place un service de transport en commun, a effectué une enquête sur le mode de transport habituel de ses salariés pour se rendre au travail. Le tableau ci-dessous donne les avis concernant le projet de l’entreprise suivant le mode de transport habituel&nbsp;:',
        ligne3: 'Voiture|Bus|Vélo|Pieds',
        colonne3: 'Favorable|Non favorable',
        consigne2_3: 'On choisit au hasard un salarié de cette entreprise.',
        quest1_3: 'La probabilité qu’il soit favorable au projet et vienne en voiture au travail vaut &1&.|La probabilité qu’il soit favorable au projet et vienne en bus au travail vaut &1&.|La probabilité qu’il soit favorable au projet et vienne à vélo au travail vaut &1&.|La probabilité qu’il soit favorable au projet et vienne à pied au travail vaut &1&.|La probabilité qu’il ne soit pas favorable au projet et vienne en voiture au travail vaut &1&.|La probabilité qu’il ne soit pas favorable au projet et vienne en bus au travail vaut &1&.|La probabilité qu’il ne soit pas favorable au projet et vienne à vélo au travail vaut &1&.|La probabilité qu’il ne soit pas favorable au projet et vienne à pied au travail vaut &1&.',
        quest2_3: 'La probabilité qu’il se rende au travail à pied ou à vélo vaut &1&.|La probabilité qu’il se rende au travail en voiture ou en bus vaut &1&.|La probabilité qu’il se rende au travail en bus ou à vélo vaut &1&.',
        // Inspiré de Métropole septembre 2001 ACC
        consigne1_4: 'Les guichets d’une gare relèvent les différents types de billets vendus (individuel, famille et groupe) suivant leur type de destination. Des informations ont été reportées dans le tableau ci-dessous&nbsp;:',
        ligne4: 'Individuel|Famille|Groupe',
        colonne4: 'France|Etranger',
        consigne2_4: 'On choisit au hasard un billet vendu dans cette gare.',
        quest1_4: 'La probabilité que ce soit un billet individuel pour voyager en France vaut &1&.|La probabilité que ce soit un billet famille pour voyager en France vaut &1&.|La probabilité que ce soit un billet groupe pour voyager en France vaut &1&.|La probabilité que ce soit un billet individuel pour voyager à l’étranger vaut &1&.|La probabilité que ce soit un billet famille pour voyager à l’étranger vaut &1&.|La probabilité que ce soit un billet groupe pour voyager à l’étranger vaut &1&.',
        quest2_4: 'La probabilité que ce soit un billet individuel ou famille vaut &1&.|La probabilité que ce soit un billet individuel ou groupe vaut &1&.|La probabilité que ce soit un billet famille ou groupe vaut &1&.',
        // Inspiré de Pondichery juin 2000 ACC
        consigne1_5: "Des personnes réparties suivant leur catégorie d'âge ont été interrogées pour savoir si elles triaient le verre et le papier. Les résultats de cette enquête ont été reportés dans le tableau ci-dessous&nbsp;:",
        ligne5: 'Moins de 35 ans|Entre 35 et 50 ans|Plus de 50 ans',
        colonne5: '"Oui"|"Non"',
        consigne2_5: 'On choisit au hasard une personne parmi celles qui ont été interrogées.',
        quest1_5: 'La probabilité que cette personne ait moins de 35 ans et ait répondu "Oui" vaut &1&.|La probabilité que cette personne ait entre 35 et 50 ans et ait répondu "Oui" vaut &1&.|La probabilité que cette personne ait plus de 50 ans et ait répondu "Oui" vaut &1&.|La probabilité que cette personne ait moins de 35 ans et ait répondu "Non" vaut &1&.|La probabilité que cette personne ait entre 35 et 50 ans et ait répondu "Non" vaut &1&.|La probabilité que cette personne ait plus de 50 ans et ait répondu "Non" vaut &1&.',
        quest2_5: 'La probabilité que cette personne ait moins de 50 ans vaut &1&.|La probabilité que cette personne ait plus de 35 ans vaut &1&.|La probabilité que cette personne ait répondu "Oui" vaut &1&.',
        // Inspiré de Nouvelle Calédonie novembre 2003 ACC
        consigne1_6: 'On a interrogé des licenciés des trois sports les plus pratiqués en France&nbsp;: football, tennis et judo (et disciplines associées). Les résultats ont été reportés dans le tableau ci-dessous&nbsp;:',
        ligne6: 'Football|Tennis|Judo',
        colonne6: 'Femme|Homme',
        consigne2_6: 'On choisit au hasard un licencié parmi ceux qui ont été interrogées.',
        quest1_6: 'La probabilité que ce soit une femme qui pratique le football vaut &1&.|La probabilité que ce soit une femme qui pratique le tennis vaut &1&.|La probabilité que ce soit une femme qui pratique le judo vaut &1&.|La probabilité que ce soit un homme qui pratique le football vaut &1&.|La probabilité que ce soit un homme qui pratique le tennis vaut &1&.|La probabilité que ce soit un homme qui pratique le judo vaut &1&.',
        quest2_6: 'La probabilité que ce licencié pratique un sport individuel vaut &1&.',
        garcon: 'garçons|garçon',
        fille: 'filless|fille',
        info1: 'Les probabilités devront être écrites de manière simplifiée, sous la forme décimale exacte ou d’une fraction irréductible.',
        info2: 'Tu n’as qu’une tentative pour répondre.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Les résultats doivent être exacts (non approchés) et donnés sous forme irréductible&nbsp;!',
        // et les phrases utiles pour les explications de la réponse

        corr1_1: 'Sur les £e élèves de cette classe, il y a £f filles de 15 ans.|Sur les £e élèves de cette classe, il y a £f filles de 16 ans.|Sur les £e élèves de cette classe, il y a £f filles de 17 ans.|Sur les £e élèves de cette classe, il y a £f garçons de 15 ans.|Sur les £e élèves de cette classe, il y a £f garçons de 17 ans.|Sur les £e élèves de cette classe, il y a £f garçons de 17 ans.',
        corr2: 'Donc la probabilité cherchée vaut $£r$.',
        corr3_1: 'Sur les £e élèves de cette classe, £f ont 16 ans ou moins<br/>($£c$).|Sur les £e élèves de cette classe, £f ont 16 ans ou plus<br/>($£c$).',
        corr1_2: 'Sur les £e bijoux de sa production, £f sont des colliers argentés.|Sur les £e bijoux de sa production, £f sont des bracelets argentés.|Sur les £e bijoux de sa production, £f sont des paires de boucles d’oreille argentés.|Sur les £e bijoux de sa production, £f sont des colliers dorés.|Sur les £e bijoux de sa production, £f sont des bracelets dorés.|Sur les £e bijoux de sa production, £f sont des paires de boucles d’oreille dorés.',
        corr3_2: 'Sur les £e bijoux de sa production, £f sont soit des colliers, soit des bracelets<br/>($£c$).|Sur les £e bijoux de sa production, £f sont soit des colliers, soit des paires de boucles d’oreille<br/>($£c$).|Sur les £e bijoux de sa production, £f sont soit des bracelets, soit des paires de boucles d’oreille<br/>($£c$).',
        corr1_3: 'Sur les £e salariés de l’entreprise, £f sont favorables au projet et viennent en voiture au travail.|Sur les £e salariés de l’entreprise, £f sont favorables au projet et viennent en bus au travail.|Sur les £e salariés de l’entreprise, £f sont favorables au projet et viennent à vélo au travail.|Sur les £e salariés de l’entreprise, £f sont favorables au projet et viennent à pied au travail.|Sur les £e salariés de l’entreprise, £f ne sont pas favorables au projet et viennent en voiture au travail.|Sur les £e salariés de l’entreprise, £f ne sont pas favorables au projet et viennent en bus au travail.|Sur les £e salariés de l’entreprise, £f ne sont pas favorables au projet et viennent à vélo au travail.|Sur les £e salariés de l’entreprise, £f ne sont pas favorables au projet et viennent à pied au travail.',
        corr3_3: 'Sur les £e salariés de l’entreprise, £f se rendent au travail à pied ou à vélo<br/>($£c$).|Sur les £e salariés de l’entreprise, £f se rendent au travail en voiture ou en bus<br/>($£c$).|Sur les £e salariés de l’entreprise, £f se rendent au travail en bus ou à vélo<br/>($£c$).',
        corr1_4: 'Sur les £e billets vendus aux guichets, £f sont des billets individuels pour voyager en France.|Sur les £e billets vendus aux guichets, £f sont des billets famille pour voyager en France.|Sur les £e billets vendus aux guichets, £f sont des billets groupe pour voyager en France.|Sur les £e billets vendus aux guichets, £f sont des billets individuels pour voyager à l’étranger.|Sur les £e billets vendus aux guichets, £f sont des billets famille pour voyager à l’étranger.|Sur les £e billets vendus aux guichets, £f sont des billets groupe pour voyager à l’étranger.',
        corr3_4: 'Sur les £e billets vendus aux guichets, £f sont des billets individuels ou famille<br/>($£c$).|Sur les £e billets vendus aux guichets, £f sont des billets individuels ou groupe<br/>($£c$).|Sur les £e billets vendus aux guichets, £f sont des billets famille ou groupe<br/>($£c$).',
        corr1_5: 'Sur les £e personnes interrogées, £f ont moins de 35 ans et ont répondu "Oui".|Sur les £e personnes interrogées, £f ont entre 35 et 50 ans et ont répondu "Oui".|Sur les £e personnes interrogées, £f ont plus de 50 ans et ont répondu "Oui".|Sur les £e personnes interrogées, £f ont moins de 35 ans et ont répondu "Non".|Sur les £e personnes interrogées, £f ont entre 35 et 50 ans et ont répondu "Non".|Sur les £e personnes interrogées, £f ont plus de 50 ans et ont répondu "Non".',
        corr3_5: 'Sur les £e personnes interrogées, £f ont moins de 50 ans<br/>($£c$).|Sur les £e personnes interrogées, £f ont plus de 35 ans<br/>($£c$).|Sur les £e personnes interrogées, £f ont répondu "Oui".',
        corr1_6: 'Sur les £e licenciés de ces sports, £f sont des femmes qui pratiquent le football.|Sur les £e licenciés de ces sports, £f sont des femmes qui pratiquent le tennis.|Sur les £e licenciés de ces sports, £f sont des femmes qui pratiquent le judo.|Sur les £e licenciés de ces sports, £f sont des hommes qui pratiquent le football.|Sur les £e licenciés de ces sports, £f sont des hommes qui pratiquent le tennis.|Sur les £e licenciés de ces sports, £f sont des hommes qui pratiquent le judo.',
        corr3_6: 'Sur les £e licenciés de ces sports, £f pratiquent un sport individuel<br/>($£c$).'
      },

      pe: 0
    }
  }
  function enonceMain () {
    if (stor.tabQuest.length === 0) {
      stor.tabQuest = [...stor.tabQuestInit]
    }
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.quest = quest
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 7; i++) {
      stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    }
    stor.laPalette = j3pAddElt(stor.conteneur, 'div')
    const objTab = {}
    objTab.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
    objTab.lestyle = { styletexte: me.styles.toutpetit.enonce, couleur: me.styles.toutpetit.enonce.color }
    objTab.nomsLigne = ds.textes['ligne' + quest].split('|')
    objTab.nomsLigne.push(ds.textes.total)
    objTab.nomsColonne = ds.textes['colonne' + quest].split('|')
    // Il nous faut maintenant gérer l’aléatoire
    let effTotal
    const effTabCol = []
    const // tableau contenant l’effectif total de chaque événement présent par colonne (donc de longueur tabEvts1.length)
      effTableLigne = []
    const tabCal = []
    let eff1, eff2, borneInf, borneSup, effLigne1, tabNum2
    switch (quest) {
      case 1:
        effTotal = j3pGetRandomInt(31, 35)
        do {
          eff1 = 4 + j3pGetRandomInt(1, 2)
          eff2 = Math.floor(2 * effTotal / 3) + j3pGetRandomInt(0, 3)
        } while (effTotal - eff1 - eff2 < 3)
        effTabCol.push(eff1, eff2, effTotal - eff1 - eff2)
        for (let i = 0; i < effTabCol.length; i++) {
          borneInf = Math.floor(2 * effTabCol[i] / 3)
          borneSup = Math.ceil(effTabCol[i] / 3)
          effLigne1 = j3pGetRandomInt(borneInf, borneSup)
          effTableLigne[i] = [effLigne1, effTabCol[i] - effLigne1]
        }
        tabNum2 = [eff1 + eff2, effTotal - eff1]// tableau des numérateurs possibles suivants les questions posées
        tabCal.push(effTableLigne[0][0] + '+' + effTableLigne[0][1] + '+' + effTableLigne[1][0] + '+' + effTableLigne[1][1])
        tabCal.push(effTableLigne[1][0] + '+' + effTableLigne[1][1] + '+' + effTableLigne[2][0] + '+' + effTableLigne[2][1])
        break
      case 2:
        effTotal = j3pGetRandomInt(80, 100)
        do {
          eff1 = Math.floor(effTotal / 3) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(0, 3)
          eff2 = Math.floor(effTotal / 3) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(0, 3)
        } while ((eff1 - eff2 < 1) || (effTotal - eff1 - eff2 < 10))
        effTabCol.push(eff1, eff2, effTotal - eff1 - eff2)
        for (let i = 0; i < effTabCol.length; i++) {
          borneInf = Math.floor(effTabCol[i] / 3)
          borneSup = Math.ceil(2 * effTabCol[i] / 3)
          effLigne1 = j3pGetRandomInt(borneInf, borneSup)
          effTableLigne[i] = [effLigne1, effTabCol[i] - effLigne1]
        }
        tabNum2 = [eff1 + eff2, effTotal - eff2, effTotal - eff1]// tableau des numérateurs possibles suivants les questions posées
        tabCal.push(effTableLigne[0][0] + '+' + effTableLigne[0][1] + '+' + effTableLigne[1][0] + '+' + effTableLigne[1][1])
        tabCal.push(effTableLigne[0][0] + '+' + effTableLigne[0][1] + '+' + effTableLigne[2][0] + '+' + effTableLigne[2][1])
        tabCal.push(effTableLigne[1][0] + '+' + effTableLigne[1][1] + '+' + effTableLigne[2][0] + '+' + effTableLigne[2][1])
        break
      case 3:
        effTotal = 10 * j3pGetRandomInt(33, 40)
        {
          let eff3, eff4
          do {
            eff1 = Math.floor(2 * effTotal / 5) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(0, 3)
            eff2 = Math.floor(effTotal / 4) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(0, 3)
            eff3 = Math.floor(effTotal / 5) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(0, 3)
            eff4 = effTotal - eff1 - eff2 - eff3
          } while ((eff3 < 5) || (eff4 < 10))
          effTabCol.push(eff1, eff2, eff3, eff4)
          for (let i = 0; i < effTabCol.length; i++) {
            if (i === 0) {
              borneInf = Math.floor(2 * effTabCol[i] / 5)
              borneSup = Math.ceil(3 * effTabCol[i] / 5)
            } else {
              borneInf = Math.floor(effTabCol[i] / 2)
              borneSup = Math.ceil(3 * effTabCol[i] / 4)
            }
            effLigne1 = j3pGetRandomInt(borneInf, borneSup)
            effTableLigne[i] = [effLigne1, effTabCol[i] - effLigne1]
          }
          tabNum2 = [eff3 + eff4, eff1 + eff2, eff2 + eff3]// tableau des numérateurs possibles suivants les questions posées
          tabCal.push(effTableLigne[2][0] + '+' + effTableLigne[2][1] + '+' + effTableLigne[3][0] + '+' + effTableLigne[3][1])
          tabCal.push(effTableLigne[0][0] + '+' + effTableLigne[0][1] + '+' + effTableLigne[1][0] + '+' + effTableLigne[1][1])
          tabCal.push(effTableLigne[1][0] + '+' + effTableLigne[1][1] + '+' + effTableLigne[2][0] + '+' + effTableLigne[2][1])
        }
        break
      case 4:
        effTotal = 50 * j3pGetRandomInt(6, 9)
        do {
          eff1 = Math.floor(2 * effTotal / 5) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(0, 3)
          eff2 = Math.floor(effTotal / 4) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(0, 3)
        } while (effTotal - eff1 - eff2 < effTotal / 6)
        effTabCol.push(eff1, eff2, effTotal - eff1 - eff2)
        for (let i = 0; i < effTabCol.length; i++) {
          borneInf = Math.floor(effTabCol[i] / 2)
          borneSup = Math.ceil(3 * effTabCol[i] / 4)
          effLigne1 = j3pGetRandomInt(borneInf, borneSup)
          effTableLigne[i] = [effLigne1, effTabCol[i] - effLigne1]
        }
        tabNum2 = [eff1 + eff2, effTotal - eff2, effTotal - eff1]// tableau des numérateurs possibles suivants les questions posées
        tabCal.push(effTableLigne[0][0] + '+' + effTableLigne[0][1] + '+' + effTableLigne[1][0] + '+' + effTableLigne[1][1])
        tabCal.push(effTableLigne[0][0] + '+' + effTableLigne[0][1] + '+' + effTableLigne[2][0] + '+' + effTableLigne[2][1])
        tabCal.push(effTableLigne[1][0] + '+' + effTableLigne[1][1] + '+' + effTableLigne[2][0] + '+' + effTableLigne[2][1])
        break
      case 5:
        effTotal = 20 * j3pGetRandomInt(15, 25)
        do {
          eff1 = Math.floor(effTotal / 3) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(0, 3)
          eff2 = Math.floor(2 * effTotal / 5) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(0, 3)
        } while (effTotal - eff1 - eff2 < effTotal / 6)
        effTabCol.push(eff1, eff2, effTotal - eff1 - eff2)
        for (let i = 0; i < effTabCol.length; i++) {
          borneInf = Math.floor(3 * effTabCol[i] / 5)
          borneSup = Math.ceil(4 * effTabCol[i] / 5)
          effLigne1 = j3pGetRandomInt(borneInf, borneSup)
          effTableLigne[i] = [effLigne1, effTabCol[i] - effLigne1]
        }
        tabNum2 = [eff1 + eff2, effTotal - eff1, effTableLigne[0][0] + effTableLigne[1][0] + effTableLigne[2][0]]// tableau des numérateurs possibles suivants les questions posées
        tabCal.push(effTableLigne[0][0] + '+' + effTableLigne[0][1] + '+' + effTableLigne[1][0] + '+' + effTableLigne[1][1])
        tabCal.push(effTableLigne[1][0] + '+' + effTableLigne[1][1] + '+' + effTableLigne[2][0] + '+' + effTableLigne[2][1])
        tabCal.push(effTableLigne[0][0] + '+' + effTableLigne[1][0] + '+' + effTableLigne[2][0])
        break
      case 6:
        effTotal = 20 * j3pGetRandomInt(25, 31)
        do {
          eff1 = Math.floor(2 * effTotal / 5) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(0, 3)
          eff2 = Math.floor(effTotal / 4) + (2 * Math.floor(Math.random() * 2) - 1) * j3pGetRandomInt(0, 3)
        } while (effTotal - eff1 - eff2 < effTotal / 6)
        effTabCol.push(eff1, eff2, effTotal - eff1 - eff2)
        for (let i = 0; i < effTabCol.length; i++) {
          if (i === 0) {
            borneInf = Math.floor(1 * effTabCol[i] / 7)
            borneSup = Math.ceil(1 * effTabCol[i] / 5)
          } else {
            borneInf = Math.floor(effTabCol[i] / 4)
            borneSup = Math.ceil(2 * effTabCol[i] / 5)
          }
          effLigne1 = j3pGetRandomInt(borneInf, borneSup)
          effTableLigne[i] = [effLigne1, effTabCol[i] - effLigne1]
        }
        tabNum2 = [effTotal - eff1]// tableau des numérateurs possibles suivants les questions posées
        tabCal.push(effTableLigne[1][0] + '+' + effTableLigne[1][1] + '+' + effTableLigne[2][0] + '+' + effTableLigne[2][1])
        break
    }
    const sommeLigne = []
    objTab.effectifs = {}
    for (let j = 0; j < effTableLigne[0].length; j++) {
      sommeLigne.push(0)
      objTab.effectifs['ligne' + (j + 1)] = []
    }
    for (let i = 0; i < effTabCol.length; i++) {
      for (let j = 0; j < effTableLigne[i].length; j++) {
        objTab.effectifs['ligne' + (j + 1)].push(effTableLigne[i][j])
        sommeLigne[j] += effTableLigne[i][j]
      }
    }
    for (let j = 0; j < effTableLigne[0].length; j++) {
      objTab.effectifs['ligne' + (j + 1)].push(sommeLigne[j])
    }
    // obj.effectifs['ligne'+i] contiendra les effectifs sur chaque ligne associée
    objTab.effectifs['ligne' + (effTableLigne[0].length + 1)] = effTabCol.slice()
    objTab.effectifs['ligne' + (effTableLigne[0].length + 1)].push(effTotal)
    objTab.effTotal = effTotal
    objTab.effTableLigne = effTableLigne
    objTab.effTableColonne = effTabCol
    me.logIfDebug('objTab:', objTab)
    j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + stor.quest])
    tabDoubleEntree(stor.zoneCons2, objTab)
    j3pAffiche(stor.zoneCons3, '', ds.textes['consigne2_' + stor.quest])
    const tabQuest1 = ds.textes['quest1_' + quest].split('|')
    const numQuest1 = j3pGetRandomInt(0, (tabQuest1.length - 1))
    const colQuest1 = numQuest1 % (tabQuest1.length / 2)
    const ligneQuest1 = Math.floor(numQuest1 / (tabQuest1.length / 2))

    // console.log("numQuest1:",numQuest1,"  colQuest1:",colQuest1,"   ligneQuest1:",ligneQuest1)
    const elt = j3pAffiche(stor.zoneCons4, '', tabQuest1[numQuest1], { inputmq1: { texte: '' } })
    stor.zoneInput = [elt.inputmqList[0]]
    const tabQuest2 = ds.textes['quest2_' + quest].split('|')
    const numQuest2 = j3pGetRandomInt(0, (tabQuest2.length - 1))
    const tabCorr1 = ds.textes['corr1_' + quest].split('|')
    const tabCorr3 = ds.textes['corr3_' + quest].split('|')
    const elt2 = j3pAffiche(stor.zoneCons6, '', tabQuest2[numQuest2], { inputmq1: { texte: '' } })
    stor.zoneInput.push(elt2.inputmqList[0])
    for (let i = 0; i <= 1; i++) {
      mqRestriction(stor.zoneInput[i], '\\d,.-/', { commandes: ['fraction'] })
      stor.zoneInput[i].typeReponse = ['nombre', 'exact']
      if (ds.reponsesSimplifiees) {
        stor.zoneInput[i].solutionSimplifiee = ['reponse fausse', 'non valide']
      } else {
        stor.zoneInput[i].solutionSimplifiee = ['reponse fausse', 'valide et on accepte']
      }
    }
    stor.zoneInput[0].reponse = [effTableLigne[colQuest1][ligneQuest1] / effTotal]
    stor.zoneInput[1].reponse = [tabNum2[numQuest2] / effTotal]
    stor.objTab = objTab

    const objRep = { effTotal, effectif1: effTableLigne[colQuest1][ligneQuest1], effectif2: tabNum2[numQuest2] }
    objRep.detailRep1 = '\\frac{' + effTableLigne[colQuest1][ligneQuest1] + '}{' + effTotal + '}'
    const pgcd1 = j3pPGCD(effTableLigne[colQuest1][ligneQuest1], effTotal)
    if (pgcd1 > 1) {
      objRep.detailRep1 += '=\\frac{' + effTableLigne[colQuest1][ligneQuest1] / pgcd1 + '}{' + effTotal / pgcd1 + '}'
    }
    objRep.detailRep2 = '\\frac{' + tabNum2[numQuest2] + '}{' + effTotal + '}'
    const pgcd2 = j3pPGCD(tabNum2[numQuest2], effTotal)
    if (pgcd2 > 1) {
      objRep.detailRep2 += '=\\frac{' + tabNum2[numQuest2] / pgcd2 + '}{' + effTotal / pgcd2 + '}'
    }
    objRep.correction1 = tabCorr1[numQuest1]
    objRep.correction2 = tabCorr3[numQuest2]

    stor.objRep = objRep
    stor.leCalcul = tabCal[numQuest2] + '=' + tabNum2[numQuest2]
    me.logIfDebug('objRep:', objRep)
    // palette MQ :
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], { liste: ['fraction'] })
    j3pStyle(stor.laPalette, { paddingBottom: '45px', fontSize: '0.9em', fontStyle: 'italic' })
    stor.zoneInput.forEach(input => $(input).focusin(function () { ecoute(this) }))
    stor.lesInfos = j3pAddElt(stor.conteneur, 'div')
    if (ds.reponsesSimplifiees) {
      const info1 = j3pAddElt(stor.lesInfos, 'div')
      j3pAffiche(info1, '', ds.textes.info1)
    }
    if (ds.nbchances === 1) {
      const info2 = j3pAddElt(stor.lesInfos, 'div')
      j3pAffiche(info2, '', ds.textes.info2)
    }
    j3pFocus(stor.zoneInput[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70

        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc / 100 })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.tabQuest = [1, 2, 3, 4, 5, 6]
        stor.tabQuestInit = [...stor.tabQuest]
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        const fracIrre = (fctsValid.zones.reponseSimplifiee[0][1] && fctsValid.zones.reponseSimplifiee[1][1])
        if (!reponse.aRepondu) {
        // Si c’est un pb de fraction irréductible, la première fois, je donne un message d’avertissement et ensuite, je compte ça comme faux
          if (!fracIrre && ds.reponsesSimplifiees && ((stor.zoneInput[0].solutionSimplifiee[1] === 'non valide') || (stor.zoneInput[1].solutionSimplifiee[1] === 'non valide'))) {
            stor.zoneInput[0].solutionSimplifiee[1] = 'reponse fausse'
            stor.zoneInput[1].solutionSimplifiee[1] = 'reponse fausse'
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (!fracIrre) {
          // c’est que l’une des fractions est réductible
            msgReponseManquante = ds.textes.comment1
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection()
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection()
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!fracIrre) {
              // c’est que la fraction de la zone de saisie est réductible
                stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection()
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
