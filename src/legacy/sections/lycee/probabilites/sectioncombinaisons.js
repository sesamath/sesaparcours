import { j3pAddElt, j3pFocus, j3pGetRandomInt, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        janvier 2020
        Calcul de combinaisons
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['nbreetapes', '2', 'liste', 'Pour chaque contexte, 1 ou 2 étapes sont possibles.\nLa première est juste un calcul d’une combinaison, le second est un produit de deux combinaisons', ['1', '2']]
  ]
}
/**
 * Pour la compatibilité ascendante des graphes, nbetapes est du stype string ici alors que ce doit toujours être un entier
 * @param params
 */
export function upgradeParametres (params) {
  // j’ai fait une erreur d ns la version initiale : dans mes paramètres, j’ai mis nbetapes comme une liste
  // Or nbetapes doit être un nombre. Cette fonction permet de renommer ce paramètre. J’ai ensuite géré sa prise en compte dans la section
  if ('nbetapes' in params) params.nbreetapes = params.nbetapes
  delete params.nbetapes
}
const structure = 'presentation1' //  || "presentation2" || "presentation3"  || "presentation1bis"
/*
Les textes présents dans la section
Sont donc accessibles dans le code par textes.consigne1
*/
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Utiliser les combinaisons',
  // on donne les phrases de la consigne
  consigne1_1: 'On dispose de £n boules noires et £r boules rouges indiscernables au toucher et numérotées de 1 à calcule[£n+£r].',
  consigne2_1: 'On mélange l’ensemble de ces boules dans une urne et on en choisit simultanément £c.',
  consigne3_1: 'Le nombre de façons d’effectuer ce choix est de&nbsp;: &1&.',
  consigne4_1: 'On choisit cette fois-ci simultanément £{n1} boules parmi les noires et £{r1} boules parmi les rouges.',
  consigne1_2: 'Pour préparer des étudiants à un concours en mathématiques, un enseignant doit choisir des sujets parmi calcule[£a+£g] leçons.',
  consigne2_2: 'Il décide d’en choisir £c au hasard et simultanément.',
  consigne3_2: 'Le nombre de façons d’effectuer ce choix est de&nbsp;: &1&.',
  consigne4_2: 'Il choisit cette fois-ci simultanément £{l1} leçons parmi les £a leçons d’algèbre et £{l2} leçons parmi les £g leçons de géométrie.',
  consigne1_3: '',
  consigne2_3: 'On choisit £n cartes au hasard et simultanément d’un jeu de 32 cartes.',
  consigne3_3: 'Le nombre de façons d’effectuer ce choix est de&nbsp;: &1&.',
  consigne4_3: 'Cette fois-ci, on choisit simultanément, dans ce paquet de 32 cartes, £{n1} cartes parmi les 12 figures et £{n2} cartes parmi les autres.',
  consigne1_4: 'Une classe contient £g garçons et £f filles.',
  consigne2_4: 'Un professeur décide de choisir £e élèves de cette classe au hasard et simultanément.',
  consigne3_4: 'Le nombre de façons d’effectuer ce choix est de&nbsp;: &1&.',
  consigne4_4: 'Il choisit cette fois-ci simultanément £{e1} garçons et £{e2} filles.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: '',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Il s’agit d’une combinaison car on choisit £c boules parmi calcule[£n+£r].',
  corr1_2: 'Il s’agit d’une combinaison car on choisit £c sujets parmi calcule[£a+£g].',
  corr1_3: 'Il s’agit d’une combinaison car on choisit £n cartes parmi 32.',
  corr1_4: 'Il s’agit d’une combinaison car on choisit £e élèves parmi calcule[£g+£f].',
  corr2_1: 'Le nombre de choix possibles est de $\\binom{calcule[£n+£r]}{£c}=\\frac{calcule[£n+£r]!}{£c!\\times calcule[£n+£r-£c]!}=£s$.',
  corr2_2: 'Le nombre de choix possibles est de $\\binom{calcule[£a+£g]}{£c}=\\frac{calcule[£a+£g]!}{£c!\\times calcule[£a+£g-£c]!}=£s$.',
  corr2_3: 'Le nombre de choix possibles est de $\\binom{32}{£n}=\\frac{32!}{£n!\\times calcule[32-£n]!}=£s$.',
  corr2_4: 'Le nombre de choix possibles est de $\\binom{calcule[£g+£f]}{£e}=\\frac{calcule[£g+£f]!}{£e!\\times calcule[£g+£f-£e]!}=£s$.',
  corr3: 'Il s’agit du produit de deux combinaisons.',
  corr4_1: 'Le nombre de choix des £{n1} boules noires est de $\\binom{£n}{£{n1}}=\\frac{£n!}{£{n1}!\\times calcule[£n-£{n1}]!}=£{s1}$ et le nombre de choix des £{r1} boules rouges est de $\\binom{£r}{£{r1}}=\\frac{£r!}{£{r1}!\\times calcule[£r-£{r1}]!}=£{s2}$.',
  corr4_2: 'Le nombre de choix des £{l1} sujets d’algèbre est de $\\binom{£a}{£{l1}}=\\frac{£a!}{£{l1}!\\times calcule[£a-£{l1}]!}=£{s1}$ et le nombre de choix des £{l2} sujets de géométrie est de $\\binom{£g}{£{l2}}=\\frac{£g!}{£{l2}!\\times calcule[£g-£{l2}]!}=£{s2}$.',
  corr4_3: 'Le nombre de choix des £{n1} figures est de $\\binom{12}{£{n1}}=\\frac{12!}{£{n1}!\\times calcule[12-£{n1}]!}=£{s1}$ et le nombre de choix des £{n2} autres cartes est de $\\binom{20}{£{n2}}=\\frac{20!}{£{n2}!\\times calcule[20-£{n2}]!}=£{s2}$.',
  corr4_4: 'Le nombre de choix des £{e1} garçons est de $\\binom{£g}{£{e1}}=\\frac{£g!}{£{e1}!\\times calcule[£g-£{e1}]!}=£{s1}$ et le nombre de choix des £{e2} filles est de $\\binom{£f}{£{e2}}=\\frac{£f!}{£{e2}!\\times calcule[£f-£{e2}]!}=£{s2}$.',
  corr5: 'Donc le nombre de façons d’effectuer ces choix est de $£{s1} \\times £{s2}=calcule[£{s1}*£{s2}]$.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    if (ds.nbetapes === 2 && me.questionCourante % 2 === 0) {
      j3pAffiche(stor.zoneExpli1, '', textes.corr3, stor.objCons)
      j3pAffiche(stor.zoneExpli2, '', textes['corr4_' + stor.quest], stor.objCons)
      j3pAffiche(stor.zoneExpli3, '', textes.corr5, stor.objCons)
    } else {
      j3pAffiche(stor.zoneExpli1, '', textes['corr1_' + stor.quest], stor.objCons)
      j3pAffiche(stor.zoneExpli2, '', textes['corr2_' + stor.quest], stor.objCons)
    }
  }

  function combinaison (n, p) {
    // Cette fonction calcule C_n^p sans utiliser les factorielles pour gagner un peu de temps
    if (n === p || p === 0) return 1
    let res = n
    let i = n - 1
    while (i > Math.max(p, n - p)) {
      res *= i
      i -= 1
    }
    i = 2
    while (i <= Math.min(p, n - p)) {
      res = Math.round(res / i)
      i += 1
    }
    // console.log("combinaison(",n,",",p,")=",res)
    return res
  }

  function enonceInitFirst () {
    ds = me.donneesSection
    ds.nbetapes = Number(ds.nbreetapes)

    // Le paramètre définit la largeur relative de la première colonne
    me.construitStructurePage({ structure, ratioGauche: 0.7 })

    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
    stor.tabQuest = [1, 2, 3]
    stor.tabQuestInit = [...stor.tabQuest]
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    if (ds.nbetapes === 1 || me.questionCourante % 2 === 1) {
      if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
      const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
      stor.quest = stor.tabQuest[pioche]
      stor.tabQuest.splice(pioche, 1)
      // Choix des données (se fait systématiquement s nbetapes=1 et à l’étape 1 seulement sinon
      stor.objCons = {}
      stor.objCons.styletexte = {}
      switch (stor.quest) {
        case 1:
          stor.objCons.n = j3pGetRandomInt(7, 12)
          do {
            stor.objCons.r = j3pGetRandomInt(7, 12)
          } while (Math.abs(stor.objCons.n - stor.objCons.r) < Math.pow(10, -12))
          stor.objCons.c = Math.round((stor.objCons.n + stor.objCons.r) / 2) - j3pGetRandomInt(2, 4)
          stor.objCons.n1 = j3pGetRandomInt(2, Math.round(stor.objCons.n / 2) + 2)
          stor.objCons.r1 = j3pGetRandomInt(2, Math.round(stor.objCons.r / 2) + 2)
          stor.objCons.s = combinaison(stor.objCons.n + stor.objCons.r, stor.objCons.c)
          stor.objCons.s1 = combinaison(stor.objCons.n, stor.objCons.n1)
          stor.objCons.s2 = combinaison(stor.objCons.r, stor.objCons.r1)
          break
        case 2:
          stor.objCons.a = j3pGetRandomInt(20, 25)
          do {
            stor.objCons.g = j3pGetRandomInt(20, 25)
          } while (Math.abs(stor.objCons.a - stor.objCons.g) < Math.pow(10, -12))
          stor.objCons.c = j3pGetRandomInt(4, 6)
          stor.objCons.l1 = j3pGetRandomInt(2, 4)
          do {
            stor.objCons.l2 = j3pGetRandomInt(2, 4)
          } while (stor.objCons.l1 === stor.objCons.l2 || stor.objCons.l1 + stor.objCons.l2 >= 7)
          stor.objCons.s = combinaison(stor.objCons.a + stor.objCons.g, stor.objCons.c)
          stor.objCons.s1 = combinaison(stor.objCons.a, stor.objCons.l1)
          stor.objCons.s2 = combinaison(stor.objCons.g, stor.objCons.l2)
          break
        case 3:
          stor.objCons.n = j3pGetRandomInt(4, 6)
          stor.objCons.n1 = j3pGetRandomInt(2, 4)
          do {
            stor.objCons.n2 = j3pGetRandomInt(2, 4)
          } while (stor.objCons.n1 === stor.objCons.n2 || stor.objCons.l1 + stor.objCons.l2 >= 7)
          stor.objCons.s = combinaison(32, stor.objCons.n)
          stor.objCons.s1 = combinaison(12, stor.objCons.n1)
          stor.objCons.s2 = combinaison(20, stor.objCons.n2)
          break
        case 4:
          do {
            stor.objCons.g = j3pGetRandomInt(12, 23)
            stor.objCons.f = j3pGetRandomInt(31, 35) - stor.objCons.g
          } while (Math.abs(stor.objCons.g - stor.objCons.f) < Math.pow(10, -12))
          stor.objCons.e = j3pGetRandomInt(3, 5)
          stor.objCons.e1 = j3pGetRandomInt(2, 4)
          do {
            stor.objCons.e2 = j3pGetRandomInt(2, 4)
          } while (stor.objCons.e1 === stor.objCons.e2 || stor.objCons.e1 + stor.objCons.e2 >= 7)
          stor.objCons.s = combinaison(stor.objCons.g + stor.objCons.f, stor.objCons.e)
          stor.objCons.s1 = combinaison(stor.objCons.g, stor.objCons.e1)
          stor.objCons.s2 = combinaison(stor.objCons.f, stor.objCons.e2)
          break
      }
    }
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes['consigne1_' + stor.quest], stor.objCons)
    if (ds.nbetapes === 2 && me.questionCourante % 2 === 0) {
      j3pAffiche(stor.zoneCons2, '', textes['consigne4_' + stor.quest], stor.objCons)
    } else {
      j3pAffiche(stor.zoneCons2, '', textes['consigne2_' + stor.quest], stor.objCons)
    }
    const elt = j3pAffiche(stor.zoneCons3, '', textes['consigne3_' + stor.quest], {
      input1: { texte: '', dynamique: true, width: '12px', maxchars: 10 }
    })
    stor.zoneInput = elt.inputmqList[0]
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = (ds.nbetapes === 2 && me.questionCourante % 2 === 0)
      ? [stor.objCons.s1 * stor.objCons.s2]
      : [stor.objCons.s]
    mqRestriction(stor.zoneInput, '\\d-.,')
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
