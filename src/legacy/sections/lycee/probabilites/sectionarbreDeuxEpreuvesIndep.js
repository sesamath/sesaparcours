import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pPGCD, j3pStyle, j3pValeurde, j3pVirgule } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pMasqueFenetre } from 'src/legacy/core/functionsJqDialog'
import { construireArbreCond, reecrireEvt, simplifieEvt, egaliteTableauPrb, bonEvts } from 'src/legacy/outils/arbreProba/arbreProba'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juillet 2019
        Dans cette section, on demande de construire un arbre dans le cas d’une succession de deux épreuves indépendantes
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['arbrePartiel', false, 'boolean', 'Par défaut, il faut construire entièrement l’arbre, mais on peut ne demander que les probabilités.']
  ]

}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Succession de deux épreuves indépendantes',
  // on donne les phrases de la consigne
  consigne1_1: 'Dans la classe de Terminale1, £{p1}&nbsp;% des élèves sont majeurs au moment de passer les épreuves du baccalauréat alors qu’ils sont £{p2}&nbsp;% dans la Terminale2.',
  consigne2_1: 'On choisit successivement un élève de Terminale 1 puis un autre de Terminale2.',
  consigne1_2: 'Un couple se rend dans un magasin d’ameublement pour choisir un canapé.<br/>Avec les modèles proposés, il y a £{n1} chances sur £{n2} qu’il soit en cuir.',
  consigne2_2: 'Il décide ensuite d’acheter une table, ce second choix n’étant pas influencé par le premier. Il y a £{n3} chances sur £{n4} qu’elle soit en bois massif.',
  consigne1_3: 'On dispose dans une urne £{n1} boules jaunes et £{n2} boules rouges indiscernables au toucher.',
  consigne2_3: 'On tire alors successivement et avec remise deux boules de cette urne et on note la couleur.',
  consigne1_4: 'Un internaute dispose de deux boîtes totalement indépendantes pour ses courriels. Chaque soir, il vérifie chacune d’elles pour savoir s’il a reçu un nouveau courriel en commençant toujours par la même.',
  consigne2_4: 'Il est estime que dans £{p1}&nbsp;% des cas, sa première boîte contient un nouveau courriel, alors que cela ne se produit que dans £{p2}&nbsp;% des cas pour la seconde.',
  consigne3: 'On note les événements&nbsp;:',
  evts1: '$M_1$ : "l’élève de Terminale1 est majeur"&nbsp;;|$M_2$ : "l’élève de Terminale2 est majeur".',
  nomEvts1: 'M_1|M_2',
  evts2: '$C$ : "le canapé choisi est en cuir"&nbsp;;|$B$ : "la table est en bois massif".',
  nomEvts2: 'C|B',
  evts3: '$J_1$ : "la première boule tirée est jaune"&nbsp;;|$J_2$ : "la deuxième boule tirée est jaune".',
  nomEvts3: 'J_1|J_2',
  evts4: '$C_1$ : "la première boîte contient au moins un courriel"&nbsp;;|$C_2$ : "la seconde boîte contient au moins un courriel".',
  nomEvts4: 'C_1|C_2',
  consigne4: 'Construire l’arbre de probabilité traduisant la situation.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Le nombre de branches n’est pas correct !',
  comment2: 'Une erreur est détectée sur au moins une branche issue de ce(s) noeud(s)&nbsp;!',
  comment3: 'Une erreur est détectée sur au moins une branche issue de la racine&nbsp;!',
  comment4: ' Au moins un des événements proposés n’est pas défini dans l’énoncé&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'On obtient alors l’arbre ci-contre :',
  corr2: 'Arbre attendu :',
  corr3: 'On peut ainsi représenter la situation par l’arbre ci-contre :',
  corr4: 'D’après l’énoncé, on a les probabilités&nbsp;:',
  corr5_1: 'De plus, le choix d’un élève de Terminale1 n’ayant pas d’incidence sur celui de Terminale2, les événements $M_1$ et $M_2$ sont indépendants.',
  corr5_2: 'De plus, le choix du canapé n’ayant pas d’incidence sur celui de la table, les événements $C$ et $B$ sont indépendants.',
  corr5_3: 'De plus, les tirages étant avec remise, le choix de la première boule n’a pas d’incidence sur celui de la seconde. Les événements $J_1$ et $J_2$ sont ainsi indépendants.',
  corr5_4: 'De plus, la réception de courriels dans la première boîte n’ayant pas d’incidence sur ce qui se passe pour la deuxième, les événements $C_1$ et $C_2$ sont ainsi indépendants.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    try {
      j3pEmpty(stor.zoneArbre.laPalette)
    } catch (e) { // VideUneZoen renvoie autrement une erreur si la palette n’est asp présente
    }
    stor.zoneArbre.listeBtns.forEach(btn => j3pDetruit(btn))
    j3pDetruit(stor.idIndication)
    const objVar = {}
    let num = 97
    for (let k = 0; k < stor.prbEnonce.length; k++) {
      objVar[String.fromCharCode(num)] = stor.prbEnonce[k]
      num++
    }
    let corrProba = textes.corr4 + '<br/>'
    corrProba += '$P(' + stor.evtsCorr[0] + ')=' + stor.prbEnonce[0] + '$'
    for (let i = 1; i < stor.evtsCorr.length; i++) {
      corrProba += ' ; $P(' + stor.evtsCorr[i] + ')=' + stor.prbEnonce[i] + '$'
    }
    j3pAffiche(stor.zoneExpli1, '', corrProba)
    j3pAffiche(stor.zoneExpli2, '', textes['corr5_' + stor.quest])
    if (!bonneReponse) {
      // Présentation des probab de l’énoncé
      j3pAffiche(stor.zoneExpli3, '', textes.corr1)
      // je construis une autre fenêtre pour y mettre la correction
      const expliArbre = j3pAddElt(stor.idDetailsFenetre, 'div')
      j3pToggleFenetres(stor.idCorrDetails)
      stor.objArbreRep.lestyle.couleur = me.styles.toutpetit.correction.color
      stor.objArbreRep.parcours = me
      construireArbreCond(expliArbre, stor.objArbreRep)
    }
    const tabOverline = document.querySelectorAll('.mq-overline')
    tabOverline.forEach(elt => {
      const laCouleur = (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      j3pStyle(elt, { borderTop: '5px solid ' + laCouleur })
    })
  }

  function evaluationCorrection () {
    if (stor.zoneArbre.fctsValid) {
      const fctsValid = stor.zoneArbre.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie

      const reponse = fctsValid.validationGlobale()
      let evtsPresents = true
      let monEvt, testPremieresBranches, PbNbBranches
      stor.mesZonesSaisie = [...stor.zoneArbre.mesZonesSaisie]
      if (reponse.aRepondu) {
        if (!ds.arbrePartiel) {
          // c’est le cas de l’arbre à remplir complètement
          // La validation doit être gérée de manière personnelle
          const tabPremieresBranches = []// ce tableau contiendra des tableaux de deux éléments correspondant aux couples Evt/Prb des premières branches
          const tabSecondesBranches = []// ce tableau aura autant d’éléments que tabPremieresBranches.length, chaque élément étant un tableau des couples Evt/Prb des secondes branches
          for (let i = 0; i < stor.mesZonesSaisie.length; i += 2) {
            // je corrige d’éventuelles fautes de frappe (espace en trop, \overline{} en trop)
            const laReponse = j3pValeurde(stor.mesZonesSaisie[i])
            const letexteReponse = reecrireEvt(laReponse)
            me.logIfDebug('letexteReponse:', letexteReponse, '   ', stor.mesZonesSaisie[i])
            if (laReponse !== letexteReponse) {
              $(stor.mesZonesSaisie[i]).mathquill('revert')
              $(stor.mesZonesSaisie[i]).mathquill('editable')
              $(stor.mesZonesSaisie[i]).mathquill('write', letexteReponse)
            }
            if (stor.mesZonesSaisie[i].typeZone === 'evt1') {
              // c’est une zone pour un évt des premières branches
              // donc dans le tableau sur les premières branche, j’ajoute le tableau [evt, prb]
              monEvt = simplifieEvt(j3pValeurde(stor.mesZonesSaisie[i]))
              tabPremieresBranches.push([monEvt, j3pCalculValeur(j3pValeurde(stor.mesZonesSaisie[i + 1]))])
              tabSecondesBranches.push([])
            } else {
              // c’est une zone sur les deuxièmes branches
              // j’identifie la première branche associée
              const numPremBranche = stor.mesZonesSaisie[i].branchePrecedente
              const monEvt2 = simplifieEvt(j3pValeurde(stor.mesZonesSaisie[i]))
              tabSecondesBranches[numPremBranche].push([monEvt2, j3pCalculValeur(j3pValeurde(stor.mesZonesSaisie[i + 1]))])
            }
          }
          const bonNbPremBranches = (tabPremieresBranches.length === stor.evts1Rep.length)
          stor.nbPremieresBranches = tabPremieresBranches.length
          PbNbBranches = false
          if (bonNbPremBranches) {
            // je vérifie que le nombre de deuxièmes branches est correct
            let bonNbSecBranches = true
            for (let i = 0; i < tabPremieresBranches.length; i++) {
              bonNbSecBranches = (bonNbSecBranches && (tabSecondesBranches[i].length === stor.evts2Rep[i].length))
            }
            if (!bonNbSecBranches) PbNbBranches = true
          } else {
            PbNbBranches = true
          }

          me.logIfDebug('tabPremieresBranches:', tabPremieresBranches, '\ntabSecondesBranches:', tabSecondesBranches)

          if (PbNbBranches) {
            reponse.bonneReponse = false
            for (let i = 0; i < stor.mesZonesSaisie.length; i++) {
              fctsValid.zones.bonneReponse[i] = false
            }
          } else {
            // on vérifie les couple evt/prb et les secondes branches associées
            testPremieresBranches = egaliteTableauPrb(tabPremieresBranches, stor.evts1Rep)
            reponse.bonneReponse = testPremieresBranches.egalite
            evtsPresents = bonEvts(tabPremieresBranches, stor.listeEvt)// ce booléen vérifie si les événements donnés pas l’élèves sont (ou non) dans la liste des événements de l’exo
            if (reponse.bonneReponse) {
              // je regarde si les autres branches sont bonnes et bien associées aux premières
              const bonnesSecondesBranches = []
              for (let i = 0; i < tabPremieresBranches.length; i++) {
                me.logIfDebug(
                  'testPremieresBranches.positionTab[i]:', testPremieresBranches.positionTab[i],
                  '\ntestPremieresBranches:', testPremieresBranches, testPremieresBranches.positionTab[i],
                  '\nstor.objArbreRep.evts2["branche"+testPremieresBranches.positionTab[i]]:', stor.objArbreRep.evts2['branche' + testPremieresBranches.positionTab[i]],
                  '\ntabSecondesBranches[i]:', tabSecondesBranches[i]
                )
                bonnesSecondesBranches[i] = egaliteTableauPrb(tabSecondesBranches[i], stor.objArbreRep.evts2['branche' + testPremieresBranches.positionTab[i]]).egalite
                reponse.bonneReponse = (reponse.bonneReponse && bonnesSecondesBranches[i])
                evtsPresents = (evtsPresents && bonEvts(tabSecondesBranches[i], stor.listeEvt))
              }
              if (!reponse.bonneReponse) {
                let posZoneSecondesBranches = tabPremieresBranches.length * 2
                for (let i = 0; i < tabPremieresBranches.length; i++) {
                  if (!bonnesSecondesBranches[i]) {
                    for (let k = 0; k < tabSecondesBranches[i].length; k++) {
                      fctsValid.zones.bonneReponse[posZoneSecondesBranches] = false
                      fctsValid.zones.bonneReponse[posZoneSecondesBranches + 1] = false
                      posZoneSecondesBranches += 2
                    }
                  } else {
                    posZoneSecondesBranches += 2 * tabSecondesBranches[i].length
                  }
                }
              }
            } else {
              for (let i = 0; i < stor.mesZonesSaisie.length; i++) {
                fctsValid.zones.bonneReponse[i] = false
              }
            }
          }
          for (let i = 0; i < stor.mesZonesSaisie.length; i++) {
            fctsValid.coloreUneZone(stor.mesZonesSaisie[i].id)
          }
        } else {
          // il faut juste que je gère des variables inexistantes à l’heures actuelles et dont je me sers dans mes tests ensuite
          testPremieresBranches = {}
          testPremieresBranches.egalite = true
          for (let i = 0; i < stor.mesZonesSaisie.length; i++) {
            if (stor.mesZonesSaisie[i].typeZone === 'prb1') {
              testPremieresBranches.egalite = (testPremieresBranches.egalite && fctsValid.zones.bonneReponse[i])
            }
          }
          evtsPresents = true
        }
      }
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // on donne le focus sur la première zone mathquill vide
        stor.mesZonesSaisie.some(function (zoneSaisie) {
          if (j3pValeurde(zoneSaisie) === '') {
            j3pFocus(zoneSaisie)
            return true // ça arrête la boucle some
          }
          return false
        })
        me.reponseManquante(stor.zoneCorr)
        return me.finCorrection()
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (reponse.bonneReponse) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          afficheCorrection(true)
          me.typederreurs[0]++
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(false)
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            if (PbNbBranches) {
              stor.zoneCorr.innerHTML += '<br>' + textes.comment1
            } else if (!testPremieresBranches.egalite) {
              stor.zoneCorr.innerHTML += '<br>' + textes.comment3
            } else {
              stor.zoneCorr.innerHTML += '<br>' + textes.comment2
            }
            if (!evtsPresents) {
              stor.zoneCorr.innerHTML += ' ' + textes.comment4
            }
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              return me.finCorrection()
            } else {
              // Erreur au nème essai
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              afficheCorrection(false)
              me.typederreurs[2]++
            }
          }
        }
      }
    } else {
      console.error('!!!stor.fctsValid encore undefined')
      setTimeout(evaluationCorrection, 50)
    }
    // Obligatoire
    me.finCorrection('navigation', true)
  }

  function enonceMain () {
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    if (stor.tabQuest.length === 0) {
      stor.tabQuest = [...stor.tabQuestInit]
    }
    const largFenetre = 430
    stor.idDetailsFenetre = j3pGetNewId('detailsFenetre')
    stor.idCorrDetails = j3pGetNewId('CorDetails')
    me.fenetresjq = [
      {
        name: stor.idCorrDetails,
        title: textes.corr2,
        width: largFenetre,
        height: 400,
        left: me.zonesElts.MG.getBoundingClientRect().width - largFenetre - 20,
        top: me.zonesElts.HD.getBoundingClientRect().height + 10,
        id: stor.idDetailsFenetre
      }
    ]
    j3pCreeFenetres(me)
    stor.quest = quest
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    ;[1, 2, 3, 4, 5].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })
    const objPrb = {}
    switch (quest) {
      case 1:
        do {
          objPrb.pourc1 = j3pGetRandomInt(400, 550) / 10
          objPrb.pourc2 = j3pGetRandomInt(400, 550) / 10
        } while (Math.abs(objPrb.pourc1 - objPrb.pourc2) < 7)
        objPrb.p1 = j3pVirgule(objPrb.pourc1)
        objPrb.p2 = j3pVirgule(objPrb.pourc2)
        stor.evtsProba = [['M_1', Math.round(Math.pow(10, 3) * (objPrb.pourc1 / 100)) / Math.pow(10, 3), [['M_2', Math.round(Math.pow(10, 3) * (objPrb.pourc2 / 100)) / Math.pow(10, 3)], ['\\overline{M_2}', Math.round(Math.pow(10, 3) * (1 - objPrb.pourc2 / 100)) / Math.pow(10, 3)]]],
          ['\\overline{M_1}', Math.round(Math.pow(10, 3) * (1 - objPrb.pourc1 / 100)) / Math.pow(10, 3), [['M_2', Math.round(Math.pow(10, 3) * (objPrb.pourc2 / 100)) / Math.pow(10, 3)], ['\\overline{M_2}', Math.round(Math.pow(10, 3) * (1 - objPrb.pourc2 / 100)) / Math.pow(10, 3)]]]]
        stor.prbEnonce = [Math.round(Math.pow(10, 3) * (objPrb.pourc1 / 100)) / Math.pow(10, 3), Math.round(Math.pow(10, 3) * (objPrb.pourc2 / 100)) / Math.pow(10, 3), Math.round(Math.pow(10, 3) * (objPrb.pourc2 / 100)) / Math.pow(10, 3)]
        stor.evtsCorr = ['M_1', 'M_2']
        stor.evtsClavier = 'M'
        break
      case 2:
        do {
          const facteur1 = j3pGetRandomInt(1, 2)
          objPrb.n2 = facteur1 * 10
          objPrb.n1 = j3pGetRandomInt(3, 6) * facteur1 + j3pGetRandomInt(0, 1)
          {
            const pgcd1 = j3pPGCD(objPrb.n1, objPrb.n2)
            objPrb.n2 = objPrb.n2 / pgcd1
            objPrb.n1 = objPrb.n1 / pgcd1
          }
          const facteur2 = j3pGetRandomInt(1, 2)
          objPrb.n4 = facteur2 * 10
          objPrb.n3 = j3pGetRandomInt(3, 6) * facteur2 + j3pGetRandomInt(0, 1)
          const pgcd2 = j3pPGCD(objPrb.n3, objPrb.n4)
          objPrb.n4 = objPrb.n4 / pgcd2
          objPrb.n3 = objPrb.n3 / pgcd2
        } while (Math.abs(objPrb.n1 / objPrb.n2 - objPrb.n3 / objPrb.n4) < 0.1)
        stor.evtsProba = [['C', Math.round(Math.pow(10, 3) * (objPrb.n1 / objPrb.n2)) / Math.pow(10, 3), [['B', Math.round(Math.pow(10, 3) * (objPrb.n3 / objPrb.n4)) / Math.pow(10, 3)], ['\\overline{B}', Math.round(Math.pow(10, 3) * (1 - objPrb.n3 / objPrb.n4)) / Math.pow(10, 3)]]],
          ['\\overline{C}', Math.round(Math.pow(10, 3) * (1 - objPrb.n1 / objPrb.n2)) / Math.pow(10, 3), [['B', Math.round(Math.pow(10, 3) * (objPrb.n3 / objPrb.n4)) / Math.pow(10, 3)], ['\\overline{B}', Math.round(Math.pow(10, 3) * (1 - objPrb.n3 / objPrb.n4)) / Math.pow(10, 3)]]]]
        stor.prbEnonce = [Math.round(Math.pow(10, 3) * (objPrb.n1 / objPrb.n2)) / Math.pow(10, 3), Math.round(Math.pow(10, 3) * (objPrb.n3 / objPrb.n4)) / Math.pow(10, 3), Math.round(Math.pow(10, 3) * (objPrb.n3 / objPrb.n4)) / Math.pow(10, 3)]
        stor.evtsCorr = ['C', 'B']
        stor.evtsClavier = 'BC'
        break
      case 3:
        {
          let pgcd1
          do {
            objPrb.n1 = j3pGetRandomInt(3, 8)
            objPrb.n2 = j3pGetRandomInt(3, 8)
            pgcd1 = j3pPGCD(objPrb.n1, objPrb.n1 + objPrb.n2)
          } while ((Math.abs(objPrb.n1 - objPrb.n2) < 0.1) || (pgcd1 > 1))
        }
        stor.evtsProba = [['J_1', '\\frac{' + objPrb.n1 + '}{' + (objPrb.n1 + objPrb.n2) + '}', [['J_2', '\\frac{' + objPrb.n1 + '}{' + (objPrb.n1 + objPrb.n2) + '}'], ['\\overline{J_2}', '\\frac{' + objPrb.n2 + '}{' + (objPrb.n1 + objPrb.n2) + '}']]],
          ['\\overline{J_1}', '\\frac{' + objPrb.n2 + '}{' + (objPrb.n1 + objPrb.n2) + '}', [['J_2', '\\frac{' + objPrb.n1 + '}{' + (objPrb.n1 + objPrb.n2) + '}'], ['\\overline{J_2}', '\\frac{' + objPrb.n2 + '}{' + (objPrb.n1 + objPrb.n2) + '}']]]]
        stor.prbEnonce = ['\\frac{' + objPrb.n1 + '}{' + (objPrb.n1 + objPrb.n2) + '}', '\\frac{' + objPrb.n2 + '}{' + (objPrb.n1 + objPrb.n2) + '}', '\\frac{' + objPrb.n2 + '}{' + (objPrb.n1 + objPrb.n2) + '}']
        stor.evtsCorr = ['J_1', 'J_2']
        stor.evtsClavier = 'J'
        break
      case 4:
        do {
          objPrb.p1 = j3pGetRandomInt(24, 43)
          objPrb.p2 = j3pGetRandomInt(15, 40)
        } while ((objPrb.p1 - objPrb.p2) < 5)
        stor.evtsProba = [['C_1', Math.round(Math.pow(10, 3) * (objPrb.p1 / 100)) / Math.pow(10, 3), [['C_2', Math.round(Math.pow(10, 3) * (objPrb.p2 / 100)) / Math.pow(10, 3)], ['\\overline{C_2}', Math.round(Math.pow(10, 3) * (1 - objPrb.p2 / 100)) / Math.pow(10, 3)]]],
          ['\\overline{C_1}', Math.round(Math.pow(10, 3) * (1 - objPrb.p1 / 100)) / Math.pow(10, 3), [['C_2', Math.round(Math.pow(10, 3) * (objPrb.p2 / 100)) / Math.pow(10, 3)], ['\\overline{C_2}', Math.round(Math.pow(10, 3) * (1 - objPrb.p2 / 100)) / Math.pow(10, 3)]]]]
        stor.prbEnonce = [Math.round(Math.pow(10, 3) * (objPrb.p1 / 100)) / Math.pow(10, 3), Math.round(Math.pow(10, 3) * (objPrb.p2 / 100)) / Math.pow(10, 3), Math.round(Math.pow(10, 3) * (objPrb.p2 / 100)) / Math.pow(10, 3)]
        stor.evtsCorr = ['C_1', 'C_2']
        stor.evtsClavier = 'C'
        break
    }
    j3pAffiche(stor.zoneCons1, '', textes['consigne1_' + quest], objPrb)
    j3pAffiche(stor.zoneCons2, '', textes['consigne2_' + quest], objPrb)
    j3pAffiche(stor.zoneCons3, '', textes.consigne3)
    const tabEvts = textes['evts' + quest].split('|')
    for (let k = 0; k < tabEvts.length; k++) {
      const zoneEvt = j3pAddElt(stor.zoneCons4, 'div')
      j3pAffiche(zoneEvt, '', tabEvts[k])
    }
    j3pAffiche(stor.zoneCons5, '', textes.consigne4)

    // div supplémentaire dans la zone droite pour positionner l’arbre et les commentaires sur les réponses de l’élève
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })

    // je dois créer l’objet qui contiendra toutes les caractéristiques de mon arbre
    stor.objArbreRep = {}
    stor.objArbreRep.lestyle = {
      styletexte: me.styles.toutpetit.enonce,
      couleur: me.styles.toutpetit.enonce.color
    }
    // pour savoir si on écrit les évts et les proba (zones de saisie si false)
    stor.objArbreRep.complet = { evts: false, proba: false }
    stor.objArbreRep.evts1 = []
    stor.objArbreRep.evts2 = {}
    stor.objArbreRep.complet = { evts: true, proba: true }
    for (let i = 0; i < stor.evtsProba.length; i++) {
      stor.objArbreRep.evts1.push([stor.evtsProba[i][0], stor.evtsProba[i][1]])
      stor.objArbreRep.evts2['branche' + i] = []
      for (let j = 0; j < stor.evtsProba[i][2].length; j++) {
        stor.objArbreRep.evts2['branche' + i].push([stor.evtsProba[i][2][j][0], stor.evtsProba[i][2][j][1]])
      }
    }
    stor.objArbre = {}
    // taille du texte evt (pour les proba, il faudrait qu’elles soient plus petites sans doute) + couleur du texte et des branches
    stor.objArbre.lestyle = {
      styletexte: me.styles.toutpetit.enonce,
      couleur: me.styles.toutpetit.enonce.color
    }
    // pour savoir si on écrit les évts et les proba (zones de saisie si false)
    stor.objArbre.complet = { evts: ds.arbrePartiel, proba: false }
    if (ds.arbrePartiel) {
      // les événements sont affichées (donc pas moyen d’ajouter un noeud)
      stor.objArbre.evts1 = []
      stor.objArbre.evts2 = {}
      for (let i = 0; i < stor.evtsProba.length; i++) {
        stor.objArbre.evts1.push([stor.evtsProba[i][0], ''])
        stor.objArbre.evts2['branche' + i] = []
        for (let j = 0; j < stor.evtsProba[i][2].length; j++) {
          stor.objArbre.evts2['branche' + i].push([stor.evtsProba[i][2][j][0], ''])
        }
      }
    } else {
      stor.objArbre.evts1 = [['', '']]
      stor.objArbre.evts2 = {}
      stor.objArbre.evts2['branche' + 0] = [['', '']] // Je nomme branche0 la branche précédente et de cette branche, je fais partir une seule branche où l’évt et la proba sont pour l’instant vide
    }
    stor.objArbre.lstRestriction = stor.evtsClavier + '\\d_'
    // je dois aussi récupérer l’arbre réponse
    stor.evts1Rep = []
    stor.evts2Rep = []
    for (let i = 0; i < stor.evtsProba.length; i++) {
      stor.evts1Rep.push([stor.evtsProba[i][0], stor.evtsProba[i][1]])
      stor.evts2Rep[i] = []
      for (let j = 0; j < stor.evtsProba[i][2].length; j++) {
        stor.evts2Rep[i].push([stor.evtsProba[i][2][j][0], stor.evtsProba[i][2][j][1]])
      }
    }
    stor.listeEvt = textes['nomEvts' + quest].split('|')

    me.logIfDebug('stor.evts1Rep:', stor.evts1Rep, '\nstor.evts2Rep:', stor.evts2Rep)
    stor.zoneArbre = j3pAddElt(stor.conteneurD, 'div')
    stor.objArbre.parcours = me
    construireArbreCond(stor.zoneArbre, stor.objArbre)

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection

        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.65 })

        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.tabQuest = [1, 2, 3, 4]
        stor.tabQuestInit = [...stor.tabQuest]
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      evaluationCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        try {
          j3pElement('Mepsectioncontinuer').addEventListener('click', function cacheFenetre () {
            j3pElement(stor.idDetailsFenetre).innerHTML = ''
            j3pMasqueFenetre(stor.idCorrDetails)
          })
        } catch (error) {
          console.error(error)
        }
      } else {
        me.etat = 'enonce'
        try {
          j3pElement('Mepboutoncontinuer').addEventListener('click', function cacheFenetre () {
            j3pElement(stor.idDetailsFenetre).innerHTML = ''
            j3pMasqueFenetre(stor.idCorrDetails)
          })
        } catch (error) {
          console.error(error)
        }
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
