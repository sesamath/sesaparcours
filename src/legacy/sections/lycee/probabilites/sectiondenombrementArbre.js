import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pNombreBienEcrit, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        février 2020
        Calcul du nombre de possibilités dans le cas de choix successifs (notion d’arbre de décision)
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Chemins d’un arbre',
  // on donne les phrases de la consigne
  consigne1_1: 'Une urne dispose de £b boules numérotées de 1 à £b.',
  consigne2_1: 'Combien y a-t-il de façons d’extraire ces £b boules successivement sans remise&nbsp;?',
  consigne1_2: 'Pour son entrainement de tennis, Gabriel choisit au hasard dans sa commode un short, un tee-shirt et une paire de chaussettes.',
  consigne2_2: "Il dispose de £s shorts, £t tee-shirts et £p paires de chaussettes. Combien a-t-il de façons d'être habillé pour son entrainement&nbsp;?",
  consigne1_3: 'Pour prendre de l’avance dans son travail parce qu’il va avoir un devoir en mathématiques, français et physique-chimie, Jules décide de réviser un chapitre au hasard de chacune de ses disciplines.',
  consigne2_3: 'Il a £m chapitres de mathématiques, £f de français et £p de physique-chimie. Combien a-t-il de façon de choisir un chapitre de chacune de ces 3 disciplines&nbsp;?',
  consigne1_4: 'Un club sportif ayant des sections rugby, tennis, canoé et athlétisme organise une porte ouverte pour laquelle il a besoin d’un représentant de chacune des sections.',
  consigne2_4: 'Parmi les licenciés volontaires pour présenter leur discipline, il y en a £{ru} pour le rugby, £t pour le tennis, £c pour canoé et £a pour l’athlétisme. Combien a-t-il de façon de choisir au hasard les 4 représentants parmi les volontaires&nbsp;?',
  consigne3: 'Réponse : &1&.',
  // remarque: "On peut se contenter d’écrire le calcul sans donner le résultat numérique.",
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: '',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'On peut construire un arbre qui aura £b branches au début, puis calcule[£b-1], ainsi de suite...',
  corr2_1: 'Le nombre de tirages possibles vaut alors $£b!=£c=£r$.',
  corr1_2: 'On peut construire un arbre qui aura £s branches au début (choix du short), puis £t (choix du tee-shirt) et enfin £p (pour la paire de chaussettes).',
  corr2_2: 'Le nombre de choix possibles vaut alors $£s\\times £t\\times £p=£r$.',
  corr1_3: 'On peut construire un arbre qui aura £m branches au début (choix du chapitre en mathématiques), puis £f (choix pour le français) et enfin £p (pour la physique-chimie).',
  corr2_3: 'Le nombre de choix possibles vaut alors $£m\\times £f\\times £p=£r$.',
  corr1_4: 'On peut construire un arbre qui aura £{ru} branches au début (choix pour le rugby), puis £t (choix pour le tennis), ensuite £c (pour le canoé) et enfin £a (pour l’athlétisme).',
  corr2_4: 'Le nombre de choix possibles vaut alors $£{ru}\\times £t\\times £c\\times £a=£r$.'
}
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    j3pAffiche(stor.zoneExpli1, '', textes['corr1_' + stor.quest], stor.objCons)
    j3pAffiche(stor.zoneExpli2, '', textes['corr2_' + stor.quest], stor.objCons)
  }

  function arrangement (n, p) {
    // Cette fonction calcule A_n^p sans utiliser les factorielles pour gagner un peu de temps
    let res = n
    let i = n - 1
    while (i > n - p) {
      res *= i
      i -= 1
    }
    // console.log('arrangement(', n, ',', p, ')=', res)
    return res
  }

  function enonceInitFirst () {
    ds = me.donneesSection
    // Construction de la page

    // Le paramètre définit la largeur relative de la première colonne

    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
    stor.tabQuest = [1, 2, 3, 4]
    stor.tabQuestInit = [...stor.tabQuest]
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    stor.quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.objCons = {}
    switch (stor.quest) {
      case 1:
        stor.objCons.b = j3pGetRandomInt(4, 6)
        stor.objCons.c = stor.objCons.b
        for (let i = 1; i < stor.objCons.b; i++) {
          stor.objCons.c += '\\times ' + (stor.objCons.b - i)
        }
        stor.laReponse = arrangement(stor.objCons.b, stor.objCons.b)
        break
      case 2:
        stor.objCons.s = j3pGetRandomInt(2, 3)
        stor.objCons.t = j3pGetRandomInt(4, 6)
        stor.objCons.p = j3pGetRandomInt(5, 7)
        stor.laReponse = (stor.objCons.s * stor.objCons.t * stor.objCons.p)
        break
      case 3:
        stor.objCons.m = j3pGetRandomInt(6, 8)
        stor.objCons.f = j3pGetRandomInt(4, 6)
        stor.objCons.p = j3pGetRandomInt(3, 4)
        stor.laReponse = (stor.objCons.m * stor.objCons.f * stor.objCons.p)
        break
      case 4:
        stor.objCons.ru = j3pGetRandomInt(13, 17)
        stor.objCons.t = j3pGetRandomInt(8, 12)
        stor.objCons.c = j3pGetRandomInt(5, 9)
        stor.objCons.a = j3pGetRandomInt(7, 10)
        stor.laReponse = (stor.objCons.ru * stor.objCons.t * stor.objCons.c * stor.objCons.a)
        break
    }

    stor.objCons.r = (stor.laReponse >= 1000) ? j3pNombreBienEcrit(stor.laReponse).replace(/\s/g, '\\text{ }') : stor.laReponse
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes['consigne1_' + stor.quest], stor.objCons)
    j3pAffiche(stor.zoneCons2, '', textes['consigne2_' + stor.quest], stor.objCons)
    const elt = j3pAffiche(stor.zoneCons3, '', textes.consigne3, {
      inputmq1: { texte: '' }
    })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.')
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [stor.laReponse]
    // Par défaut, le focus ira à la dernière zone de saisie créée. Donc ici on le remet à la première
    j3pFocus(stor.zoneInput)

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')

    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    // Paramétrage de la validation

    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: [stor.zoneInput.id]
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :

        if ((!reponse.aRepondu) && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
          if (reponse.bonneReponse) {
          // Bonne réponse
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
