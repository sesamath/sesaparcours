import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pNombreBienEcrit, j3pPaletteMathquill, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        janvier 2020
        Calcul du nombre de possibilité dans le cas de répétitions de sélections
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Dénombrements simples',
  // on donne les phrases de la consigne
  consigne1_1: 'Une association de £e membres souhaite constituer son bureau qui devra contenir un président, un vice-président, un trésorier et un secrétaire.',
  consigne2_1: 'Combien y a-t-il de façons de constituer ce bureau&nbsp;?',
  consigne1_2: 'Une association organise une tombola avec pour seuls lots un bon voyage, un ordinateur portable et une tablette. Trois tirages seulement sont donc effectués.<br/>Les organisateurs vendent £t tickets.',
  consigne2_2: 'Combien y a-t-il de tirages possibles lors de cette tombola&nbsp;?',
  consigne1_3: 'Une urne contient £b boules numérotées de 1 à £b.<br/>On prélève successivement £t boules de cette urne.',
  consigne2_3: 'Combien y a-t-il de tirages possibles&nbsp;?',

  consigneBis1_1: 'À chaque début de cours, un professeur interroge au hasard un des £e élèves d’une classe (un élève peut être choisi plusieurs jours d’affilée).',
  consigneBis2_1: 'Pendant £c cours successifs, combien y a-t-il de façons d’effectuer ce tirage aléatoire&nbsp;?',
  consigneBis1_2: 'Yanis dispose d’une application sur son smartphone pour modifier le fond d’écran. Le choix des fonds disponibles se fait sur un ensemble de £f images et d’une manière complètement aléatoire.',
  consigneBis2_2: 'Combien y a-t-il de façons d’effectuer ce choix aléatoire au cours de £c changements successifs&nbsp;?',
  consigneBis1_3: 'On place £n boules numérotées de 1 à £n dans une urne. Un tirage d’une boule consiste à l’extraire de l’urne, à noter son numéro et à la remettre dans l’urne.',
  consigneBis2_3: 'Combien y a-t-il de façons d’effectuer le tirage de £b boules dans ces conditions&nbsp;?',
  consigne3: 'Réponse : &1&.',
  remarque: 'On peut se contenter d’écrire le calcul sans donner le résultat numérique.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: '',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'On choisit successivement ces 4 membres parmi les £e membres de l’association. Cela revient à choisir un quadruplet d’éléments distincts dans un ensemble en contenant £e.',
  corr2_1: 'Le nombre de façons d’effectuer ces choix vaut alors&nbsp;:<br/>$\\frac{£e!}{(£e-4)!}=£e\\times calcule[£e-1]\\times calcule[£e-2]\\times calcule[£e-3]=£r$',
  corr1_2: 'On choisit successivement ces 3 tickets parmi les £t tickets vendus. Cela revient à choisir un triplet d’éléments distincts dans un ensemble en contenant £t.',
  corr2_2: 'Le nombre de façons d’effectuer ce tirage vaut alors&nbsp;:<br/>$\\frac{£t!}{(£t-3)!}=£t\\times calcule[£t-1]\\times calcule[£t-2]=£r$.',
  corr1_3: 'On choisit successivement ces £t boules parmi les £b boules de l’urne. Cela revient à choisir une liste de £t éléments distincts dans un ensemble qui en contient £b.',
  corr2_3: 'Il y a donc $£c=£r$ façons possibles d’effectuer ce tirage.',

  corrBis1_1: 'Pour chaque cours, il y a £e façons de choisir un élève. La question posée revient alors à choisir une liste de £c éléments, éventuellement non distincts deux à deux, dans un ensemble à £e éléments.',
  corrBis2_1: 'Le nombre de façons d’effectuer ces choix vaut alors&nbsp;:<br/>$£e^{£c}=£r$',
  corrBis1_2: 'Pour chaque changement, il y a £f façons de choisir l’image. La question posée revient alors à choisir une liste de £c éléments, éventuellement non distincts deux à deux, dans un ensemble à £f éléments.',
  corrBis2_2: 'Le nombre de façons d’effectuer ces choix vaut alors&nbsp;:<br/>$£f^{£c}=£r$',
  corrBis1_3: 'Pour chaque tirage d’une boule, il y a £n façons de faire le choix. La question posée revient alors à choisir une liste de £b éléments, éventuellement non distincts deux à deux, dans un ensemble à £n éléments.',
  corrBis2_3: 'Le nombre de façons d’effectuer ce tirage de £b boules vaut alors&nbsp;:<br/>$£n^{£b}=£r$'
}
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    j3pDetruit(stor.zoneCons5)
    const corr1 = (stor.typeTirage[me.questionCourante - 1] === 'sans')
      ? textes['corr1_' + stor.quest]
      : textes['corrBis1_' + stor.quest]
    const corr2 = (stor.typeTirage[me.questionCourante - 1] === 'sans')
      ? textes['corr2_' + stor.quest]
      : textes['corrBis2_' + stor.quest]
    j3pAffiche(stor.zoneExpli1, '', corr1, stor.objCons)
    j3pAffiche(stor.zoneExpli2, '', corr2, stor.objCons)
  }

  function enonceInitFirst () {
    ds = me.donneesSection

    // Le paramètre définit la largeur relative de la première colonne
    me.construitStructurePage({ structure: 'presentation1' })

    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
    stor.tabQuest = [1, 2, 3]
    stor.tabQuestInit = [...stor.tabQuest]
    stor.tabQuest2 = [1, 2, 3]
    stor.tabQuestInit2 = [...stor.tabQuest2]
    // On choisit les cas de figures parmi 2 types : tirages successifs avec ou sans remise
    const typeTirage = ['avec', 'sans']
    let typeTirage2 = [...typeTirage]
    stor.typeTirage = []
    for (let j = 0; j < ds.nbrepetitions; j++) {
      if (typeTirage2.length === 0) typeTirage2 = [...typeTirage]
      const alea = j3pGetRandomInt(0, typeTirage2.length - 1)
      stor.typeTirage.push(typeTirage2[alea])
      typeTirage2.splice(alea, 1)
    }
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    if (stor.typeTirage[me.questionCourante - 1] === 'sans') {
      if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
      const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
      stor.quest = stor.tabQuest[pioche]
      stor.tabQuest.splice(pioche, 1)
      stor.objCons = {}
      stor.objCons.styletexte = {}
      switch (stor.quest) {
        case 1:
          stor.objCons.e = j3pGetRandomInt(29, 35)
          stor.laReponse = (stor.objCons.e * (stor.objCons.e - 1) * (stor.objCons.e - 2) * (stor.objCons.e - 3))
          break
        case 2:
          stor.objCons.t = j3pGetRandomInt(200, 250)
          stor.laReponse = (stor.objCons.t * (stor.objCons.t - 1) * (stor.objCons.t - 2))
          break
        case 3:
          stor.objCons.b = j3pGetRandomInt(9, 13)
          stor.objCons.t = j3pGetRandomInt(5, 7)
          stor.objCons.c = '\\frac{' + stor.objCons.b + '!}{(' + stor.objCons.b + '-' + stor.objCons.t + ')!}=' + stor.objCons.b
          stor.laReponse = stor.objCons.b
          for (let i = 1; i < stor.objCons.t; i++) {
            stor.objCons.c += '\\times ' + (stor.objCons.b - i)
            stor.laReponse = stor.laReponse * (stor.objCons.b - i)
          }
          break
      }
    } else {
      if (stor.tabQuest.length === 0) stor.tabQuest2 = [...stor.tabQuestInit2]
      const pioche = j3pGetRandomInt(0, (stor.tabQuest2.length - 1))
      stor.quest = stor.tabQuest2[pioche]
      stor.tabQuest2.splice(pioche, 1)
      stor.objCons = {}
      stor.objCons.styletexte = {}
      switch (stor.quest) {
        case 1:
          stor.objCons.e = j3pGetRandomInt(29, 35)
          stor.objCons.c = j3pGetRandomInt(3, 4)
          stor.laReponse = Math.pow(stor.objCons.e, stor.objCons.c)
          break
        case 2:
          stor.objCons.f = j3pGetRandomInt(15, 25)
          stor.objCons.c = j3pGetRandomInt(4, 6)
          stor.laReponse = Math.pow(stor.objCons.f, stor.objCons.c)
          break
        case 3:
          stor.objCons.n = j3pGetRandomInt(11, 15)
          stor.objCons.b = j3pGetRandomInt(7, 9)
          stor.laReponse = Math.pow(stor.objCons.n, stor.objCons.b)
          break
      }
    }
    stor.objCons.r = (stor.laReponse >= 1000) ? j3pNombreBienEcrit(stor.laReponse).replace(/\s/g, '\\text{ }') : stor.laReponse
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    const consigne1 = (stor.typeTirage[me.questionCourante - 1] === 'sans')
      ? textes['consigne1_' + stor.quest]
      : textes['consigneBis1_' + stor.quest]
    j3pAffiche(stor.zoneCons1, '', consigne1, stor.objCons)
    const consigne2 = (stor.typeTirage[me.questionCourante - 1] === 'sans')
      ? textes['consigne2_' + stor.quest]
      : textes['consigneBis2_' + stor.quest]
    j3pAffiche(stor.zoneCons2, '', consigne2, stor.objCons)
    const elt = j3pAffiche(stor.zoneCons3, '', textes.consigne3, {
      inputmq1: { texte: '' }
    })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,./^*', {
      commandes: ['puissance']
    })
    j3pAffiche(stor.zoneCons4, '', textes.remarque, {
      style: { fontStyle: 'italic', fontSize: '0.9em' }
    })
    j3pPaletteMathquill(stor.zoneCons5, stor.zoneInput)
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [stor.laReponse]
    // Par défaut, le focus ira à la dernière zone de saisie créée. Donc ici on le remet à la première
    j3pFocus(stor.zoneInput)

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')

    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')

    // Paramétrage de la validation

    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: [stor.zoneInput.id]
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :

        if ((!reponse.aRepondu) && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
