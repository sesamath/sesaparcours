import { j3pAddElt, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pPGCD, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient } from 'src/legacy/core/functionsLatex'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2019
        Calcul d’une probabilité dans un cas simple (cas favorable/cas possible)
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['PetitePolice', false, 'boolean', '<u>true</u>: La police utilisée est plus petite.'],
    ['College', false, 'boolean', '<u>true</u>: Les questions sont plus simples.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Calculer des probabilités',
  // on donne les phrases de la consigne
  consigne1_1: 'Une classe de seconde est composée de £{n1} garçons et £{n2} filles.',
  consigne2_1: 'Le professeur de mathématiques interroge un élève de la classe au hasard.',
  consigne3_1: 'La probabilité qu’il interroge un garçon vaut&nbsp;: &1&.|La probabilité qu’il interroge une fille vaut&nbsp;: &1&.',
  consigne1_2: 'Une urne opaque contient £{n1} boules blanches, £{n2} boules rouges et £{n3} boules noires.',
  consigne2_2: 'Erwan tire une boule au hasard de cette urne et note sa couleur.',
  consigne3_2: 'La probabilité qu’il tire une boule blanche vaut&nbsp;: &1&.|La probabilité qu’il tire une boule rouge vaut&nbsp;: &1&.|La probabilité qu’il tire une boule noire vaut&nbsp;: &1&.',
  consigne1_3: 'Une agence de voyage propose £{n1} séjours en Europe, £{n2} en Asie et £{n3} en Afrique.',
  consigne2_3: 'Un couple de voyageurs choisit au hasard sa destination parmi l’un des séjours proposés par l’agence.',
  consigne3_3: 'La probabilité que ce couple voyage en Europe vaut&nbsp;: &1&.|La probabilité que ce couple voyage en Asie vaut&nbsp;: &1&.|La probabilité que ce couple voyage en Afrique vaut&nbsp;: &1&.',
  consigne1_4: 'Un restaurant propose sur sa carte £{n1} viandes, £{n2} poissons en guise de plat.',
  consigne2_4: 'Léa choisit au hasard un plat présent sur la carte.',
  consigne3_4: 'La probabilité qu’elle choisisse de la viande vaut&nbsp;: &1&.|La probabilité qu’elle choisisse du poisson vaut&nbsp;: &1&.',
  consigne1_5: 'Le réfrigérateur contient des yaourts : £{n1} sont à la fraise, £{n2} à l’abricot, £{n3} à la pêche et £{n4} à la cerise.',
  consigne2_5: 'Killian choisit au hasard un de ces yaourts.',
  consigne3_5: 'La probabilité qu’il choisisse un yaourt à la fraise vaut&nbsp;: &1&.|La probabilité qu’il choisisse un yaourt à l’abricot vaut&nbsp;: &1&.|La probabilité qu’il choisisse un yaourt à la pêche vaut&nbsp;: &1&.|La probabilité qu’il choisisse un yaourt à la cerise vaut&nbsp;: &1&.',
  consigne1_6: 'Parmi les titulaires du baccalauréat d’une classe de Terminale, £{n1} ont eu la mention Très Bien, £{n2} la mention Bien, £{n3} la mention Assez Bien et £{n4} n’ont pas eu de mention.',
  consigne2_6: 'Un enseignant choisit au hasard un bachelier de cette classe.',
  consigne3_6: 'La probabilité qu’il choisisse un élève ayant eu la mention Très Bien vaut&nbsp;: &1&.|La probabilité qu’il choisisse un élève ayant eu la mention Bien vaut&nbsp;: &1&.|La probabilité qu’il choisisse un élève ayant eu la mention Assez Bien vaut&nbsp;: &1&.|La probabilité qu’il choisisse un élève ayant eu son bac sans mention vaut&nbsp;: &1&.',
  consigne1_7: 'Assia pioche une carte au hasard dans un jeu de 32 cartes',
  consigne2_7: '',
  consigne3_7: 'La probabilité qu’elle pioche le 7 de coeur vaut&nbsp;: &1&.|La probabilité qu’elle pioche un as vaut: &1&.|La probabilité qu’elle pioche un pique vaut&nbsp;: &1&.',
  consigne1_8: 'Joss tire à pile ou face avec une pièce équilibrée.',
  consigne2_8: '',
  consigne3_8: 'La probabilité qu’il obtienne pile vaut&nbsp;: &1&.|La probabilité qu’il obtienne face vaut: &1&.',
  consigne1_9: 'Yanis jette un dé équilibré à 6 faces.',
  consigne2_9: '',
  consigne3_9: 'La probabilité qu’il obtienne 2 vaut&nbsp;: &1&.|La probabilité qu’il obtienne un nombre pair vaut: &1&.|La probabilité qu’il obtienne un nombre supérieur ou égal à 5 vaut: &1&.|La probabilité qu’il obtienne un multiple de 3 vaut: &1&.',

  info1: 'Les probabilités devront être écrites de manière simplifiée, sous la forme décimale ou d’une fraction irréductible.',
  info2: 'Tu n’as qu’une tentative pour répondre.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Les résultats doivent être exacts (non approchés) et donnés sous forme irréductible&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1_1_1: 'Dans la classe de £{effTotal} élèves, le professeur a £{n1} chances d’interroger un garçon.',
  corr2_1_1: 'Donc la probabilité qu’il interroge un garçon vaut $£{fracRep}$.',
  corr1_1_2: 'Dans la classe de £{effTotal} élèves, le professeur a £{n2} chances d’interroger une fille.',
  corr2_1_2: 'Donc la probabilité qu’il interroge une fille vaut $£{fracRep}$.',
  corr1_2_1: 'L’urne contient £{effTotal} boules et Erwan a £{n1} chances de tirer une boule blanche.',
  corr2_2_1: 'Donc la probabilité qu’il tire une boule blanche vaut $£{fracRep}$.',
  corr1_2_2: 'L’urne contient £{effTotal} boules et Erwan a £{n2} chances de tirer une boule rouge.',
  corr2_2_2: 'Donc la probabilité qu’il tire une boule rouge vaut $£{fracRep}$.',
  corr1_2_3: 'L’urne contient £{effTotal} boules et Erwan a £{n3} chances de tirer une boule noire.',
  corr2_2_3: 'Donc la probabilité qu’il tire une boule noire vaut $£{fracRep}$.',
  corr1_3_1: 'L’agence propose £{effTotal} séjours et le couple a £{n1} chances de choisir d’aller en Europe.',
  corr2_3_1: 'Donc la probabilité qu’il voyage en Europe vaut $£{fracRep}$.',
  corr1_3_2: 'L’agence propose £{effTotal} séjours et le couple a £{n2} chances de choisir d’aller en Asie.',
  corr2_3_2: 'Donc la probabilité qu’il voyage en Asie vaut $£{fracRep}$.',
  corr1_3_3: 'L’agence propose £{effTotal} séjours et le couple a £{n3} chances de choisir d’aller en Afrique.',
  corr2_3_3: 'Donc la probabilité qu’il voyage en Afrique vaut $£{fracRep}$.',
  corr1_4_1: 'Le restaurant propose £{effTotal} plats sur sa carte, Léa a £{n1} chances de choisir une viande.',
  corr2_4_1: 'Donc la probabilité qu’elle choisisse de la viande vaut $£{fracRep}$.',
  corr1_4_2: 'Le restaurant propose £{effTotal} plats sur sa carte, Léa a £{n2} chances de choisir un poisson.',
  corr2_4_2: 'Donc la probabilité qu’elle choisisse du poisson vaut $£{fracRep}$.',
  corr1_5_1: 'Le réfrigérateur contient £{effTotal} yaourts et Killian a £{n1} chances d’en choisir un à la fraise.',
  corr2_5_1: 'Donc la probabilité qu’il choisisse un yaourt à la fraise vaut $£{fracRep}$.',
  corr1_5_2: 'Le réfrigérateur contient £{effTotal} yaourts et Killian a £{n2} chances d’en choisir un à l’abricot.',
  corr2_5_2: 'Donc la probabilité qu’il choisisse un yaourt à l’abricot vaut $£{fracRep}$.',
  corr1_5_3: 'Le réfrigérateur contient £{effTotal} yaourts et Killian a £{n3} chances d’en choisir un à la pêche.',
  corr2_5_3: 'Donc la probabilité qu’il choisisse un yaourt à la pêche vaut $£{fracRep}$.',
  corr1_5_4: 'Le réfrigérateur contient £{effTotal} yaourts et Killian a £{n4} chances d’en choisir un à la cerise.',
  corr2_5_4: 'Donc la probabilité qu’il choisisse un yaourt à la cerise vaut $£{fracRep}$.',
  corr1_6_1: 'Dans cette classe, £{effTotal} élèves ont eu leur bac et l’enseignant a £{n1} chances d’en choisir un qui a eu la mention Très Bien.',
  corr2_6_1: 'Donc la probabilité qu’il choisisse un bachelier ayant eu la mention Très Bien vaut $£{fracRep}$.',
  corr1_6_2: 'Dans cette classe, £{effTotal} élèves ont eu leur bac et l’enseignant a £{n2} chances d’en choisir un qui a eu la mention Bien.',
  corr2_6_2: 'Donc la probabilité qu’il choisisse un bachelier ayant eu la mention Bien vaut $£{fracRep}$.',
  corr1_6_3: 'Dans cette classe, £{effTotal} élèves ont eu leur bac et l’enseignant a £{n3} chances d’en choisir un qui a eu la mention Assez Bien.',
  corr2_6_3: 'Donc la probabilité qu’il choisisse un bachelier ayant eu la mention Assez Bien vaut $£{fracRep}$.',
  corr1_6_4: 'Dans cette classe, £{effTotal} élèves ont eu leur bac et l’enseignant a £{n4} chances d’en choisir un qui n’a pas eu de mention.',
  corr2_6_4: 'Donc la probabilité qu’il choisisse un bachelier n’ayant pas eu de mention vaut $£{fracRep}$.',

  corr1_7_1: 'Sur $32$ cartes, il y a $1$ seule 7 de coeur.',
  corr2_7_1: 'Donc la probabilité qu’elle pioche un 7 de coeur vaut $\\frac{1}{32}$.',
  corr1_7_2: 'Sur $32$ cartes, il y a $4$ as.',
  corr2_7_2: 'Donc la probabilité qu’elle pioche un as vaut $\\frac{4}{32} = \\frac{4 \\times 1}{4 \\times 8} = \\frac{1}{8}$.',
  corr1_7_3: 'Sur $32$ cartes, il y a $8$ piques (7 , 8 , 9 , 10 , Valet , Dame , Roi et as).',
  corr2_7_3: 'Donc la probabilité qu’elle pioche un pique vaut $\\frac{8}{32} = \\frac{8 \\times 1}{8 \\times 4} = \\frac{1}{4}$.',

  corr1_8_1: 'Sur $2$ côtés, il y a $1$ côté pile.',
  corr2_8_1: 'Donc la probabilité qu’il obtienne pile vaut $\\frac{1}{2}$.',
  corr1_8_2: 'Sur $2$ côtés, il y a $1$ côté face.',
  corr2_8_2: 'Donc la probabilité qu’il obtienne face vaut $\\frac{1}{2}$.',

  corr1_9_1: 'Sur $6$ résultats possibles, il y a $1$ fois le nombre 2.',
  corr2_9_1: 'Donc la probabilité qu’il obtienne 2 vaut $\\frac{1}{6}$.',
  corr1_9_2: 'Sur $6$ résultats possibles, $3$ sont pairs (2 ; 4 et 6) , ',
  corr2_9_2: 'Donc la probabilité qu’il obtienne un nombre pair vaut $\\frac{3}{6} = \\frac{3 \\times 1}{3 \\times 2} =\\frac{1}{2}$.',
  corr1_9_3: 'Sur $6$ résultats possibles, $2$ sont supérieurs ou égal à 5 (5 et 6).',
  corr2_9_3: 'Donc la probabilité qu’il obtienne un nombre supérieur ou égal à 5 vaut $\\frac{2}{6} = \\frac{2 \\times 1}{2 \\times 3} = \\frac{1}{3}$.',
  corr1_9_4: 'Sur $6$ résultats possibles,  $2$ sont des multiples de 3 (3 et 6)',
  corr2_9_4: 'Donc la probabilité qu’il obtienne un multiple de 3 vaut $\\frac{2}{6} = \\frac{2 \\times 1}{2 \\times 3} = \\frac{1}{3}$.'
}

export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pDetruit(stor.zoneCons4)
    j3pDetruit(stor.zoneCons5)
    j3pAffiche(stor.zoneExpli1, '', textes['corr1_' + stor.quest + '_' + String(stor.choixQuest + 1)], stor.objPrb)
    j3pAffiche(stor.zoneExpli2, '', textes['corr2_' + stor.quest + '_' + String(stor.choixQuest + 1)], stor.objPrb)
  }

  function enonceMain () {
    let bufpolice = 'petit'
    if (ds.PetitePolice) bufpolice = 'toutpetit'
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre(bufpolice + '.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (stor.tabQuest.length === 0) {
      stor.tabQuest = [...stor.tabQuestInit]
    }
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.quest = quest
    // ensuite je dois créer l’objet qui contiendra toutes les caractéristiques de mon arbre
    const objPrb = {}
    stor.consignesQuest = textes['consigne3_' + quest].split('|')
    stor.choixQuest = j3pGetRandomInt(0, (stor.consignesQuest.length - 1))
    switch (quest) {
      case 1:
        objPrb.n1 = j3pGetRandomInt(11, 20)
        do {
          objPrb.effTotal = j3pGetRandomInt(31, 35)
          objPrb.n2 = objPrb.effTotal - objPrb.n1
        } while (Math.abs(objPrb.n1 - objPrb.n2) < Math.pow(10, -12))
        break
      case 2:
        objPrb.n1 = j3pGetRandomInt(4, 7)
        do {
          objPrb.n2 = j3pGetRandomInt(3, 8)
          objPrb.n3 = j3pGetRandomInt(3, 7)
        } while ((Math.abs(objPrb.n1 - objPrb.n2) < Math.pow(10, -12)) || (Math.abs(objPrb.n1 - objPrb.n3) < Math.pow(10, -12)) || (Math.abs(objPrb.n3 - objPrb.n2) < Math.pow(10, -12)))
        objPrb.effTotal = objPrb.n1 + objPrb.n2 + objPrb.n3
        break
      case 3:
        objPrb.n1 = j3pGetRandomInt(4, 7)
        do {
          objPrb.n2 = j3pGetRandomInt(3, 8)
          objPrb.n3 = j3pGetRandomInt(3, 7)
        } while ((Math.abs(objPrb.n1 - objPrb.n2) < Math.pow(10, -12)) || (Math.abs(objPrb.n1 - objPrb.n3) < Math.pow(10, -12)) || (Math.abs(objPrb.n3 - objPrb.n2) < Math.pow(10, -12)))
        objPrb.effTotal = objPrb.n1 + objPrb.n2 + objPrb.n3
        break
      case 4:
        objPrb.n1 = j3pGetRandomInt(4, 8)
        do {
          objPrb.n2 = j3pGetRandomInt(3, 7)
        } while (Math.abs(objPrb.n1 - objPrb.n2) < Math.pow(10, -12))
        objPrb.effTotal = objPrb.n1 + objPrb.n2
        break
      case 5:
        {
          const choixYaourts = [3, 4, 5, 6, 7, 8, 9]
          for (let i = 1; i <= 4; i++) {
            const choixAlea = j3pGetRandomInt(0, (choixYaourts.length - 1))
            objPrb['n' + i] = choixYaourts[choixAlea]
            choixYaourts.splice(choixAlea, 1)
          }
        }
        objPrb.effTotal = objPrb.n1 + objPrb.n2 + objPrb.n3 + objPrb.n4
        break
      case 6:
        objPrb.n1 = j3pGetRandomInt(2, 6)
        do {
          objPrb.n2 = j3pGetRandomInt(3, 6)
        } while (Math.abs(objPrb.n1 - objPrb.n2) < Math.pow(10, -12))
        objPrb.n3 = j3pGetRandomInt(7, 9)
        objPrb.effTotal = j3pGetRandomInt(29, 32)
        objPrb.n4 = objPrb.effTotal - objPrb.n1 - objPrb.n2 - objPrb.n3
        break
      case 7:
        objPrb.n1 = 1
        objPrb.n2 = 4
        objPrb.n3 = 8
        objPrb.effTotal = 32
        break
      case 8:
        objPrb.n1 = 1
        objPrb.n2 = 1
        objPrb.effTotal = 2
        break
      case 9:
        objPrb.n1 = 1
        objPrb.n2 = 3
        objPrb.n3 = 2
        objPrb.n4 = 2
        objPrb.effTotal = 6
        break
    }
    const nume = Number(objPrb['n' + String(stor.choixQuest + 1)])
    if (j3pPGCD(nume, objPrb.effTotal) === 1) {
      objPrb.fracRep = '\\frac{' + nume + '}{' + objPrb.effTotal + '}'
    } else {
      objPrb.fracRep = '\\frac{' + nume + '}{' + objPrb.effTotal + '}=' + j3pGetLatexQuotient(nume, objPrb.effTotal)
    }
    stor.objPrb = objPrb
    // Choix de la question

    j3pAffiche(stor.zoneCons1, '', textes['consigne1_' + quest], objPrb)
    j3pAffiche(stor.zoneCons2, '', textes['consigne2_' + quest], objPrb)
    objPrb.inputmq1 = { texte: '' }
    const { inputmqList } = j3pAffiche(stor.zoneCons3, '', stor.consignesQuest[stor.choixQuest], objPrb)
    if (inputmqList?.[0] == null) throw Error('Aucune zone de saisie créée dans l’énoncé')
    stor.zoneInput = inputmqList[0]
    stor.laPalette = j3pAddElt(stor.zoneCons4, 'div')
    j3pStyle(stor.zoneCons4, { paddingBottom: '44px' })
    // palette MQ :
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['fraction'] })
    mqRestriction(stor.zoneInput, '\\d,.-/', { commandes: ['fraction'] })
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [nume / objPrb.effTotal]
    stor.zoneInput.solutionSimplifiee = ['reponse fausse', 'non valide']
    for (const i of [1, 2]) {
      stor['info' + i] = j3pAddElt(stor.zoneCons5, 'div')
    }
    j3pAffiche(stor.info1, '', textes.info1)
    j3pStyle(stor.zoneCons5, { fontStyle: 'italic', fontSize: '0.9em' })
    if ((ds.nbchances === 1) && (!ds.College)) j3pAffiche(stor.info2, '', textes.info2)
    j3pFocus(stor.zoneInput)
    me.logIfDebug('reponse : ', stor.zoneInput.reponse, '  et fractionnaire:', objPrb.fracRep)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    const mesZonesSaisie = [stor.zoneInput.id]

    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage('presentation1')
        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.tabQuest = [1, 2, 3, 4, 5, 6]
        if (ds.College) stor.tabQuest = [1, 2, 3, 4, 5, 7, 8, 9]
        stor.tabQuestInit = [...stor.tabQuest]
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’object fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie

      const reponse = fctsValid.validationGlobale()
      const fracIrre = fctsValid.zones.reponseSimplifiee[0][1]
      if (!reponse.aRepondu && (stor.zoneInput.solutionSimplifiee[1] === 'non valide')) {
        // Si c’est un pb de fraction irréductible, la première fois, je donne un message d’avertissement et ensuite, je compte ça comme faux
        if (!fracIrre) {
          stor.zoneInput.solutionSimplifiee[1] = 'reponse fausse'
        }
      }
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        let msgReponseManquante
        if (!fracIrre) {
          // c’est que la fraction de la quatrième zone de saisie est réductible
          msgReponseManquante = textes.comment1
        }
        me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        return me.finCorrection()
      }

      // Une réponse a été saisie
      // Bonne réponse
      if (reponse.bonneReponse) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        afficheCorrection(true)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      if (!fracIrre) {
        // c’est que la fraction de la zone de saisie est réductible
        stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
      }
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // indication éventuelle ici
        // ici il a encore la possibilité de se corriger
        return me.finCorrection('navigation', true)
      }

      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(false)
      me.typederreurs[2]++
      return me.finCorrection('navigation', true)
    } // case "correction"

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
