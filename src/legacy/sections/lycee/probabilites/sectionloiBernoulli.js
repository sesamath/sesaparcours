import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pNombre, j3pPaletteMathquill, j3pPGCD, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import $ from 'jquery'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since mars 2022
 * @fileOverview Cette section demande le paramètre d’une loi de Bernoulli dans un contexte puis le calcul de l’espérance et de l’écart-type de la va
 */

/**
 * Les paramètres de la section, anciennement j3p.SectionXxx = {}
 */
export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {

    // Nombre de répétitions de l’exercice
    nbrepetitions: 3,
    nbchances: 2,
    nbetapes: 2,
    indication: '',
    textes: {
      titre_exo: 'Loi de Bernoulli',
      consigne1_1: 'Une urne contient £n boules rouges et £m boules blanches. On tire au hasard une boule de cette urne et on note la couleur.',
      consigne1_2: 'Dans une classe de £n élèves, £m d’entre eux ont eu une note supérieure à la moyenne au cours du dernier devoir de mathématiques.',
      consigne1_3: 'On lance un dé cubique bien équilibré dont les faces sont numérotées de 1 à 6.',
      consigne1_4: 'Lors d’une séance d’entraînement à la course à pied d’un club d’athlétisme, on compte £n pratiquants de la course sur route et £m du trail.',
      consigne1_5: 'Dans un lycée, £n&nbsp;% des élèves de Terminale ont conservé la spécialité Mathématiques.',
      consigne1_6: 'Lors d’un jeu de fléchettes, un joueur amateur manque sa cible dans un cas sur £n.',
      consigne2_1: 'On considère que tirer une boule rouge est un succès lors de cette expérience aléatoire.',
      consigne2_2: 'Le professeur de mathématiques sélectionne un élève au hasard dans cette classe et on considère que le fait que ce dernier ait eu une note au-dessus de la moyenne est un succès de cette expérience aléatoire.',
      consigne2_3: 'On considère qu’obtenir lors de ce lancer un nombre inférieur ou égal à £n est un succès.',
      consigne2_4: 'On choisit l’un de ces coureurs au hasard et on considère qu’un succès est de choisir une personne qui pratique le trail.',
      consigne2_5: 'On choisit un élève de Terminale au hasard dans ce lycée et on considère qu’un succès serait qu’il ait conservé la spécialité Mathématiques en Terminale.',
      consigne2_6: 'On choisit un de ses lancers au hasard et on considère que le succès est que la cible ait été atteinte.',
      consigne3: 'Le paramètre de la loi associée à cette expérience vaut &1&.',
      consigne3bis: 'Le paramètre de la loi de Bernoulli associée à cette expérience vaut $£p$.',
      consigne4: 'Son espérance vaut &1& et son écart-type environ &2&.',
      info: 'On donnera la valeur exacte de l’espérance et un arrondi au millième de l’écart-type.',
      comment1: 'N’y aurait-il pas un problème d’arrondi dans la deuxième réponse.',
      corr1_1: 'Le paramètre de cette loi de Bernoulli associée à l’expérience est la probabilité du succès, c’est-à-dire ici de tirer une boule rouge.',
      corr1_2: 'Le paramètre de cette loi de Bernoulli associée à l’expérience est la probabilité du succès, c’est-à-dire ici de choisir un élève de la classe qui a eu la moyenne.',
      corr1_3: 'Le paramètre de cette loi de Bernoulli associée à l’expérience est la probabilité du succès, c’est-à-dire ici de tomber sur une face dont le numéro est inférieur ou égale à £n.',
      corr1_4: 'Le paramètre de cette loi de Bernoulli associée à l’expérience est la probabilité du succès, c’est-à-dire ici de tomber sur un coureur qui pratique le trail.',
      corr1_5: 'Le paramètre de cette loi de Bernoulli associée à l’expérience est la probabilité du succès, c’est-à-dire ici de choisir un élève de Terminale qui a conservé l’enseignement de Mathématiques en Terminale.',
      corr1_6: 'Le paramètre de cette loi de Bernoulli associée à l’expérience est la probabilité du succès, c’est-à-dire ici que le lancer ait atteint la cible.',
      corr2: 'Donc ce paramètre vaut $£p$.',
      corr3: 'D’après le cours, l’espérance d’une loi de Bernoulli de paramètre $p$ vaut $p$ et sa variance $p(1-p)$, l’écart-type étant la racine carrée de la variance.',
      corr4: 'Donc l’espérance de cette loi vaut $£p$ et son écart-type $\\sqrt{£p\\left(1-£p\\right)}\\simeq £e$.'
    }
  }
} // getDonnees

/**
 * Retourne les données pour chaque sujet
 * @private
 * @param {Parcours} me
 * @param {Number} numSujet
 * @return {Object}
 */
function genereAlea (me, numSujet) {
  const obj = {}
  if (numSujet === 1) {
    do {
      obj.n = j3pGetRandomInt(4, 7)
      obj.m = j3pGetRandomInt(3, 8)
    } while (obj.m === obj.n)
    const lepgcd = j3pPGCD(obj.n, obj.m + obj.n)
    obj.p = '\\frac{' + obj.n / lepgcd + '}{' + (obj.m + obj.n) / lepgcd + '}'
    obj.pval = obj.n / (obj.n + obj.m)
  } else if (numSujet === 2) {
    obj.n = j3pGetRandomInt(28, 35)
    obj.m = obj.n - j3pGetRandomInt(5, 9)
    const lepgcd = j3pPGCD(obj.n, obj.m)
    obj.p = '\\frac{' + obj.m / lepgcd + '}{' + obj.n / lepgcd + '}'
    obj.pval = obj.m / obj.n
  } else if (numSujet === 3) {
    obj.n = 2 + 2 * j3pGetRandomInt(0, 1)
    const lepgcd = j3pPGCD(obj.n, 6)
    obj.p = '\\frac{' + obj.n / lepgcd + '}{' + 6 / lepgcd + '}'
    obj.pval = obj.n / 6
  } else if (numSujet === 4) {
    do {
      obj.n = j3pGetRandomInt(7, 13)
      obj.m = j3pGetRandomInt(7, 13)
    } while (obj.m === obj.n)
    const lepgcd = j3pPGCD(obj.m, obj.m + obj.n)
    obj.p = '\\frac{' + obj.m / lepgcd + '}{' + (obj.m + obj.n) / lepgcd + '}'
    obj.pval = obj.m / (obj.n + obj.m)
  } else if (numSujet === 5) {
    obj.n1 = j3pGetRandomInt(448, 549) / 10
    obj.n = j3pVirgule(obj.n1)
    obj.p = j3pVirgule(obj.n1 / 100)
    obj.pval = obj.n1 / 100
  } else if (numSujet === 6) {
    obj.n = j3pGetRandomInt(4, 7)
    obj.p = '\\frac{' + (obj.n - 1) + '}{' + obj.n + '}'
    obj.pval = (obj.n - 1) / obj.n
  }
  obj.eval = Math.sqrt(obj.pval * (1 - obj.pval))
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(me.donneesSection.textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  const stor = me.storage

  // On établit la liste des sujets qui peuvent être donnés au fur et à mesure (évitons les répétitions quand on le peut)
  stor.listeSujetsInit = [1, 2, 3, 4, 5, 6]
  stor.listeSujets = [...stor.listeSujetsInit]
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  function ecoute (laZone) {
    if (laZone.className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.laPalette)
      if (laZone.avecFraction) j3pPaletteMathquill(stor.laPalette, laZone, { liste: ['fraction'] })
    }
  }
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
  me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]
  j3pCreeFenetres(me)
  j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
  j3pAfficheCroixFenetres('Calculatrice')
  if (me.questionCourante % 2 === 1) {
  // Choix aléatoire d’un nouveau sujet
    const pioche = Math.floor(Math.random() * stor.listeSujets.length)
    stor.numSujet = stor.listeSujets[pioche]
    stor.listeSujets.splice(pioche, 1)
    if (stor.listeSujets.length === 0) stor.listeSujets = [...stor.listeSujetsInit]
    // On crée les valeurs utiles pour l’énoncé choisi
    stor.objDonnees = genereAlea(me, stor.numSujet)
  }
  // on affiche l’énoncé
  for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
  stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
  j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + stor.numSujet], stor.objDonnees)
  j3pAffiche(stor.zoneCons2, '', ds.textes['consigne2_' + stor.numSujet], stor.objDonnees)
  if (me.questionCourante % 2 === 1) {
    const elt = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3, { inputmq1: { texte: '' } })
    stor.zoneInput = [elt.inputmqList[0]]
    elt.inputmqList[0].typeReponse = ['nombre', 'exact']
    elt.inputmqList[0].reponse = [stor.objDonnees.pval]
    mqRestriction(elt.inputmqList[0], '\\d,./-', { commandes: ['fraction'] })
    j3pPaletteMathquill(stor.laPalette, elt.inputmqList[0], { liste: ['fraction'] })
  } else {
    j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3bis, stor.objDonnees)
    const elt = j3pAffiche(stor.zoneCons4, '', ds.textes.consigne4, {
      inputmq1: { texte: '' },
      inputmq2: { texte: '' }
    })
    stor.zoneInput = elt.inputmqList
    elt.inputmqList[0].typeReponse = ['nombre', 'exact']
    elt.inputmqList[0].reponse = [stor.objDonnees.pval]
    stor.nbDecimales = 3
    elt.inputmqList[1].typeReponse = ['nombre', 'arrondi', Math.pow(10, -stor.nbDecimales)]
    elt.inputmqList[1].reponse = [stor.objDonnees.eval]
    stor.objDonnees.e = Math.round(stor.objDonnees.eval * Math.pow(10, stor.nbDecimales)) / Math.pow(10, stor.nbDecimales)
    j3pStyle(stor.zoneCons5, { fontStyle: 'italic', fontSize: '0.9em' })
    j3pAddContent(stor.zoneCons5, ds.textes.info)
    elt.inputmqList[0].avecFraction = true
    elt.inputmqList[1].avecFraction = false
    mqRestriction(elt.inputmqList[0], '\\d,./-', { commandes: ['fraction'] })
    mqRestriction(elt.inputmqList[1], '\\d,.-')
    stor.zoneInput.forEach(eltInput => $(eltInput).focusin(function () { ecoute(this) }))
    ecoute(stor.zoneInput[0])
  }
  const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
  stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
  j3pFocus(stor.zoneInput[0])
  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  j3pEmpty(stor.laPalette)
  j3pEmpty(stor.zoneCalc)
  j3pDetruitFenetres('Calculatrice')
  // on affiche la correction dans la zone xxx
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
  const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
  if (me.questionCourante % 2 === 1) {
    j3pAffiche(zoneExpli1, '', ds.textes['corr1_' + stor.numSujet], stor.objDonnees)
    j3pAffiche(zoneExpli2, '', ds.textes.corr2, stor.objDonnees)
  } else {
    j3pAffiche(zoneExpli1, '', ds.textes.corr3, stor.objDonnees)
    j3pAffiche(zoneExpli2, '', ds.textes.corr4, stor.objDonnees)
  }
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  stor.pbArrondi = false
  if (reponse.aRepondu) {
    // on vérifie si dans la deuxième question ce n’est aps un pb d’arrondi
    if (me.questionCourante % 2 === 0 && !stor.fctsValid.zones.bonneReponse[1]) {
      stor.pbArrondi = (Math.abs(stor.zoneInput[1].reponse[0] - j3pNombre(stor.fctsValid.zones.reponseSaisie[1])) < Math.pow(10, -(stor.nbDecimales - 1)))
    }
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
    if (stor.pbArrondi) {
      j3pAddElt(stor.divCorrection, 'br')
      me.reponseKo(stor.divCorrection, ds.textes.comment1, true)
    }
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
