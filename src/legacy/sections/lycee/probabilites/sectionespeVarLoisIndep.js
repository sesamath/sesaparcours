import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomFloat, j3pGetRandomInt, j3pMonome, j3pRemplacePoint, j3pRestriction, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { tabLoiProba } from 'src/legacy/outils/tableauxProba/tabLoiProba'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2020
        Calcul de l’espérance et de la variance de la somme de va indep
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Espérance et variance pour des v.a. indépendantes',
  // on donne les phrases de la consigne
  consigne1: 'Soit $£X$ la variable aléatoire dont on donne la loi ci-dessous.',
  consigne2: '$£Y$ est une deuxième variable aléatoire définie sur un même univers, indépendante de $£X$, dont l’espérance vaut $E(£Y)=£e$ et la variance $V(£Y)=£v$.',
  consigne3: 'L’espérance de la variable aléatoire $£Z$ vaut alors&nbsp;:',
  consigne4: 'On rappelle que $E(£X)=£e$.',
  consigne5: 'La variance de la variable aléatoire $£Z$ vaut alors&nbsp;:',
  consigne6: 'On a bien $E(£Z)=£f$.',
  rmq1: 'Arrondir à 0,01.',
  rmq2: 'Donner la valeur exacte.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'La formule proposée est correcte, mais je veux la valeur numérique.',
  comment2: 'On attend la valeur numérique de cette espérance.',
  comment3: 'On attend la valeur numérique de cette variance.',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'La linéarité de l’espérance nous permet de dire que $£e$, que les variables aléatoires $£X$ et $£Y$ soient indépendantes ou non.',
  corr2: 'On calcule alors l’espérance de $£X$&nbsp;',
  corr3: 'Donc $E(£Z)=£c=£r$.',
  corr4: 'Les variables aléatoires $£X$ et $£Y$ étant indépendantes, $£e$.',
  corr5_1: 'De plus $£e$.',
  corr5_2: 'De plus $£e$ et $£f$.',
  corr6: 'On calcule alors la variance de $£X$&nbsp;:',
  corr7: 'Donc $V(£Z)=£c£s£r$'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3, 4, 5].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    if (me.questionCourante % ds.nbetapes === 1) {
      j3pAffiche(stor.zoneExpli1, '', textes.corr1, {
        e: 'E(' + stor.vaZ + ')=' + stor.calcEspeZ,
        X: stor.vaX,
        Y: stor.vaY
      })
      j3pAffiche(stor.zoneExpli2, '', textes.corr2, {
        X: stor.vaX
      })
      j3pAffiche(stor.zoneExpli3, '', '$E(' + stor.vaX + ')=' + stor.lesDonnees.calculEspeX + '=' + stor.lesDonnees.espe + '$')
      j3pAffiche(stor.zoneExpli4, '', textes.corr3, {
        Z: stor.vaZ,
        c: stor.calcEspeZ,
        r: Math.round(100 * stor.zoneInput.reponse[0]) / 100
      })
    } else {
      j3pAffiche(stor.zoneExpli1, '', textes.corr4, {
        e: 'V(' + stor.vaZ + ')=' + stor.calcVarZ,
        X: stor.vaX,
        Y: stor.vaY
      })
      let laCorr2
      const objCorr2 = {}
      if (stor.calcVarX2 !== '') {
        if (stor.calcVarY2 !== '') {
          objCorr2.e = stor.calcVarX2
          objCorr2.f = stor.calcVarY2
          laCorr2 = textes.corr5_2
        } else {
          objCorr2.e = stor.calcVarX2
          laCorr2 = textes.corr5_1
        }
      } else {
        objCorr2.e = stor.calcVarY2
        laCorr2 = textes.corr5_1
      }
      if ((stor.calcVarX2 !== '') || (stor.calcVarY2 !== '')) {
        j3pAffiche(stor.zoneExpli2, '', laCorr2, objCorr2)
      }
      j3pAffiche(stor.zoneExpli3, '', textes.corr6, {
        X: stor.vaX
      })
      const varX = (Math.abs(Math.round(1000 * stor.lesDonnees.var) / 1000 - stor.lesDonnees.var) < Math.pow(10, -10)) ? '=' + stor.lesDonnees.var : '\\approx' + Math.round(100 * stor.lesDonnees.var) / 100
      j3pAffiche(stor.zoneExpli4, '', '$V(' + stor.vaX + ')=' + stor.lesDonnees.calculVarX + varX + '$')
      j3pAffiche(stor.zoneExpli5, '', textes.corr7, {
        Z: stor.vaZ,
        c: stor.calcVarZ,
        s: (varX[0] === '=') ? '=' : '\\approx',
        r: (varX[0] === '=') ? Math.round(1000 * stor.zoneInput.reponse[0]) / 1000 : Math.round(100 * stor.zoneInput.reponse[0]) / 100
      })
    }
  }

  function genereDonnees () {
    // Déterminer les valeurs de X et les probas de les obtenir
    const obj = {}
    obj.valX = []
    obj.valPrb = []
    const nbCas = j3pGetRandomInt(2, 3)
    for (let i = 0; i < nbCas; i++) {
      obj.valX.push((i === 0) ? j3pGetRandomInt(-10, 10) : obj.valX[i - 1] + j3pGetRandomInt(10, 30))
    }
    obj.valPrb.push(Math.round(100 * j3pGetRandomFloat(0.05, 0.95 * 2 / nbCas)) / 100)
    if (nbCas === 2) {
      obj.valPrb.push(Math.round(100 * (1 - obj.valPrb[0])) / 100)
    } else {
      obj.valPrb.push(Math.floor(100 * j3pGetRandomFloat(0.05, 0.95 - obj.valPrb[0])) / 100)
      obj.valPrb.push(Math.round(100 * (1 - obj.valPrb[0] - obj.valPrb[1])) / 100)
    }
    obj.espe = 0
    obj.var = 0
    obj.calculEspeX = ''
    obj.calculVarX = ''
    for (let i = 0; i < nbCas; i++) {
      obj.espe += obj.valX[i] * obj.valPrb[i]
      obj.calculEspeX += ((i === 0) ? '' : '+') + ((i > 0 && obj.valX[i] < 0) ? '(' + obj.valX[i] + ')' : obj.valX[i]) + '\\times ' + obj.valPrb[i]
      obj.calculVarX += ((i === 0) ? '' : '+') + ((obj.valX[i] < 0) ? '(' + obj.valX[i] + ')' : obj.valX[i]) + '^2\\times ' + obj.valPrb[i]
      obj.var += Math.pow(obj.valX[i], 2) * obj.valPrb[i]
    }
    obj.espe = Math.round(obj.espe * 100) / 100
    obj.calculVarX += '-' + ((obj.espe < 0) ? '(' + obj.espe + ')' : obj.espe) + '^2'
    obj.var = Math.round((obj.var - Math.pow(obj.espe, 2)) * 10000) / 10000
    return obj
  }

  function enonceInitFirst () {
    ds = me.donneesSection

    me.surcharge({ nbetapes: 2 })
    // Construction de la page
    // Le paramètre définit la largeur relative de la première colonne
    stor.pourcentage = 0.7
    // j’ai choisi de mettre ce pourcentage en paramètre pour centrer la figure par la suite

    me.construitStructurePage({ structure: 'presentation1', ratioGauche: stor.pourcentage })
    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    if (me.questionCourante % ds.nbetapes === 1) {
      const tabNomLoi = ['X', 'Y', 'Z']
      let pioche = j3pGetRandomInt(0, tabNomLoi.length - 1)
      stor.vaX = tabNomLoi[pioche]
      tabNomLoi.splice(pioche, 1)
      pioche = j3pGetRandomInt(0, tabNomLoi.length - 1)
      stor.vaY = tabNomLoi[pioche]
      stor.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
      stor.lesDonnees = genereDonnees()
      me.logIfDebug('lesDonnees:', stor.lesDonnees)
      const k1 = j3pGetRandomInt(1, 3)
      let k2
      do {
        k2 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 3)
      } while (k1 === k2)
      stor.espeY = j3pGetRandomInt(-50, 150) / 10
      stor.varY = Math.round(100 * j3pGetRandomFloat(2, 15)) / 100
      stor.vaZ = j3pMonome(1, 1, k1, stor.vaX) + j3pMonome(2, 1, k2, stor.vaY)
      stor.calcEspeZ = j3pMonome(1, 1, k1, 'E(' + stor.vaX + ')') + j3pMonome(2, 1, k2, 'E(' + stor.vaY + ')')
      stor.espeZ = k1 * stor.lesDonnees.espe + k2 * stor.espeY
      stor.varianceZ = Math.pow(k1, 2) * stor.lesDonnees.var + Math.pow(k2, 2) * stor.varY
      me.logIfDebug('espeZ:', stor.espeZ, 'varianceZ:', stor.varianceZ)
      stor.calcVarZ = 'V(' + j3pMonome(1, 1, k1, stor.vaX) + ')' + '+V(' + j3pMonome(1, 1, k2, stor.vaY) + ')'
      stor.calcVarX2 = ''
      stor.calcVarY2 = ''
      if (k1 !== 1) stor.calcVarX2 = 'V(' + j3pMonome(1, 1, k1, stor.vaX) + ')=' + j3pMonome(1, 1, Math.pow(k1, 2), 'V(' + stor.vaX + ')')
      if (k2 !== 1) stor.calcVarY2 = 'V(' + j3pMonome(1, 1, k2, stor.vaY) + ')=' + j3pMonome(1, 1, Math.pow(k2, 2), 'V(' + stor.vaY + ')')
      stor.formuleEspeZ = j3pMonome(1, 1, k1, 'E(' + stor.vaX + ')') + j3pMonome(2, 1, k2, 'E(' + stor.vaY + ')')
      stor.formuleVarZ = j3pMonome(1, 1, Math.pow(k1, 2), 'V(' + stor.vaX + ')') + j3pMonome(2, 1, Math.pow(k2, 2), 'V(' + stor.vaY + ')')
      stor.formuleEspeZ2 = j3pMonome(1, 1, k2, 'E(' + stor.vaY + ')') + j3pMonome(2, 1, k1, 'E(' + stor.vaX + ')')
      stor.formuleVarZ2 = j3pMonome(1, 1, Math.pow(k2, 2), 'V(' + stor.vaY + ')') + j3pMonome(2, 1, Math.pow(k1, 2), 'V(' + stor.vaX + ')')
    }
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 8; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, {
      X: stor.vaX,
      Y: stor.vaY
    })
    stor.objDonnees = {
      parent: stor.zoneCons2,
      va: stor.vaX,
      largeur: stor.largeurTab,
      legendes: ['$' + stor.vaX.toLowerCase() + '_i$', '$P(' + stor.vaX + '=' + stor.vaX.toLowerCase() + '_i)$'],
      valeurs: stor.lesDonnees.valX,
      probas: stor.lesDonnees.valPrb,
      typeFont: me.styles.petit.enonce,
      complet: true
    }
    tabLoiProba(stor.objDonnees)
    j3pAffiche(stor.zoneCons3, '', textes.consigne2, {
      X: stor.vaX,
      Y: stor.vaY,
      e: stor.espeY,
      v: stor.varY
    })
    if (me.questionCourante % ds.nbetapes === 1) {
      j3pAffiche(stor.zoneCons4, '', textes.consigne3, {
        Z: stor.vaZ
      })
      const elt = j3pAffiche(stor.zoneCons7, '', '$E(' + stor.vaZ + ')=$@1@', {
        input1: { texte: '', dynamique: true }
      })
      stor.zoneInput = elt.inputList[0]
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      stor.zoneInput.reponse = [stor.espeZ]
    } else {
      j3pAffiche(stor.zoneCons4, '', textes.consigne4, {
        X: stor.vaX,
        e: j3pVirgule(stor.lesDonnees.espe)
      })
      j3pAffiche(stor.zoneCons5, '', textes.consigne5, {
        Z: stor.vaZ
      })
      const elt = j3pAffiche(stor.zoneCons7, '', '$V(' + stor.vaZ + ')=$@1@', {
        input1: { texte: '', dynamique: true }
      })
      stor.zoneInput = elt.inputList[0]
      let remarque
      if (Math.abs(Math.round(1000 * stor.varianceZ) / 1000 - stor.varianceZ) < Math.pow(10, -12)) {
        stor.zoneInput.typeReponse = ['nombre', 'exact']
        remarque = textes.rmq2
      } else {
        stor.zoneInput.typeReponse = ['nombre', 'arrondi', 0.01]
        remarque = textes.rmq1
      }
      stor.zoneInput.reponse = [stor.varianceZ]
      j3pAffiche(stor.zoneCons8, '', remarque, {
        style: {
          fontStyle: 'italic',
          fontSize: '0.95em'
        }
      })
    }
    j3pRestriction(stor.zoneInput, '0-9()\\-+a-zA-Z\\.,')
    stor.zoneInput.addEventListener('input', function () {
      j3pRemplacePoint.call(this)
      while (/[a-z]/.test(this.value)) {
        const minuscule = this.value.match(/[a-z]/)[0]
        const index = this.value.indexOf(minuscule) + 1
        this.value = this.value.replace(minuscule, minuscule.toUpperCase())
        this.selectionStart = index
        this.selectionEnd = index
      }
    })
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pCreeFenetres(me)
    j3pAfficheCroixFenetres('Calculatrice')
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        let reponse = { aRepondu: false, bonneReponse: false, avecFormule: false }
        reponse.aRepondu = stor.fctsValid.valideReponses()
        if (reponse.aRepondu) {
        // on vérifie s’il n’a pas écrit la formule du cours
          const formuleAttendue = (me.questionCourante % ds.nbetapes === 1)
            ? stor.formuleEspeZ.replace('E(' + stor.vaX + ')', 'x').replace('E(' + stor.vaY + ')', 'y')
            : stor.formuleVarZ.replace('V(' + stor.vaX + ')', 'x').replace('V(' + stor.vaY + ')', 'y')
          let avecFormule = /[A-Z]/.test(stor.fctsValid.zones.reponseSaisie[0])
          if (avecFormule) {
          // on va vérifier si la formule écrite est bonne et si c’est le plus simple
            const espeReg = RegExp('E\\((\\-?[0-9]*)\\*?(' + stor.vaX + '|' + stor.vaY + ')\\)', 'g')
            let repEleve = stor.fctsValid.zones.reponseSaisie[0].replace(espeReg, '$1E($2)')
            // pour la variance c’est plus tendu
            const varReg = RegExp('V\\((\\-?[0-9]*)\\*?(' + stor.vaX + '|' + stor.vaY + ')\\)', 'g')
            repEleve = repEleve.replace(varReg, function (x) {
              const posparOuvr = x.indexOf('(')
              const posparFerm = x.indexOf(')')
              const nb = x.substring(posparOuvr + 1, posparFerm - 1)
              if (nb === '' || nb === '-') {
                return x.substring(0, posparOuvr + 1) + x.substring(posparFerm - 1)
              } else {
                return String(Math.pow(Number(nb), 2)) + x.substring(0, posparOuvr + 1) + x.substring(posparFerm - 1)
              }
            })
            let formuleSaisie
            if (me.questionCourante % ds.nbetapes === 1) {
              formuleSaisie = repEleve.replace(RegExp('E\\(' + stor.vaX + '\\)', 'g'), 'x')
              formuleSaisie = formuleSaisie.replace(RegExp('E\\(' + stor.vaY + '\\)', 'g'), 'y')
            } else {
              formuleSaisie = repEleve.replace(RegExp('V\\(' + stor.vaX + '\\)', 'g'), 'x')
              formuleSaisie = formuleSaisie.replace(RegExp('V\\(' + stor.vaY + '\\)', 'g'), 'y')
            }
            // console.log('formuleAttendue:', formuleAttendue, 'formuleSaisie:', formuleSaisie)
            const unarbre1 = new Tarbre(formuleAttendue, ['x', 'y'])
            const unarbre2 = new Tarbre(formuleSaisie, ['x', 'y'])
            let egalite = true
            for (let j = 0; j <= 7; j++) {
              for (let k = 0; k <= 7; k++) {
                egalite = egalite && (Math.abs(unarbre1.evalue([j, k]) - unarbre2.evalue([j, k])) < Math.pow(10, -8))
              }
            }
            // console.log('formuleSaisie:', formuleSaisie, repEleve)
            if (egalite) {
            // on vérifie si la formule correspond à la simplification qu’on pourrait avoir
              let reponseSaisie = stor.fctsValid.zones.reponseSaisie[0].replace(/(-\+)|(\+-)/g, '-')
              reponseSaisie = reponseSaisie.replace(/(\+\+)|(--)/g, '+')
              // console.log('reponseSaisie:', reponseSaisie)
              if (me.questionCourante % ds.nbetapes === 1) {
                avecFormule = ((reponseSaisie === stor.formuleEspeZ) || (reponseSaisie === stor.formuleEspeZ2))
              } else {
                avecFormule = ((reponseSaisie === stor.formuleVarZ) || (reponseSaisie === stor.formuleVarZ2))
              }
              stor.zoneCorr.style.color = me.styles.cfaux
              stor.zoneCorr.innerHTML = (avecFormule)
                ? textes.comment1
                : (me.questionCourante % ds.nbetapes === 1) ? textes.comment2 : textes.comment3
              reponse.aRepondu = false
            } else {
              stor.fctsValid.validationGlobale()
            }
          }
          if (avecFormule) {
            j3pEmpty(stor.zoneCons6)
            j3pAffiche(stor.zoneCons6, '', textes.consigne6, {
              f: (me.questionCourante % ds.nbetapes === 1) ? stor.formuleEspeZ : stor.formuleVarZ,
              Z: stor.vaZ
            })
          } else {
            reponse = stor.fctsValid.validationGlobale()
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
}
