import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetRandomElt, j3pGetRandomInt } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        août 2020
        Utilisation de l’inégalité de Bienaymé-Tchebychev
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Inégalité de Bienaymé-Tchebychev',
  // on donne les phrases de la consigne
  consigne1: 'Soit $£X$ une variable aléatoire d’espérance £e et de variance £V.',
  consigne2_1: 'Donner un majorant de $£p$ obtenu à l’aide de l’inégalité de Bienaymé-Tchebychev.',
  consigne2_2: 'Donner un minorant de $£p$ obtenu à l’aide de l’inégalité de Bienaymé-Tchebychev.',
  indic: 'On donnera une valeur approchée à $10^{-£a}$.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves

  // et les phrases utiles pour les explications de la réponse
  corr1: 'Rappel du cours : ',
  corr2: '$X$ étant une variable aléatoire d’espérance $\\mu$ et de variance $V$, pour tout réel $\\delta>0$,',
  corr3: '$P(|X-\\mu|\\geq \\delta) \\leq \\frac{V}{\\delta^2}$.',
  corr4_1: 'Dans cet exemple, $\\mu=£e$, $V=£V$ et $\\delta=£d$.',
  corr4_2: 'Dans cet exemple, on cherche $£p$ qui vaut $1-P(|X-\\mu|\\geq \\delta)$ avec $\\mu=£e$, $V=£V$ et $\\delta=£d$.',
  corr4_3: 'Dans cet exemple, on cherche $£p$ qui vaut $P(|X-\\mu|<\\delta)=1-P(|X-\\mu|\\geq \\delta)$ avec $\\mu=£e$, $V=£V$ et $\\delta=£d$.',
  corr5_1: 'Ainsi $£p\\leq \\frac{£V}{£d^2}$, ce qui donne $£p\\leq £r$ en arrondissant le membre de droite à $10^{-£a}$.',
  corr5_2: 'Ainsi $£p\\geq 1-\\frac{£V}{£d^2}$, ce qui donne $£p\\geq £r$ en arrondissant le membre de droite à $10^{-£a}$.',
  corr5_3: 'Ainsi $£p\\leq 1-\\frac{£V}{£d^2}$, ce qui donne $£p\\geq £r$ en arrondissant le membre de droite à $10^{-£a}$.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    // explications contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    for (let i = 1; i <= 3; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div', '')
    const leCours = []// j3pAddElt(stor.zoneexplication1, 'div', '', { style: { border: 'thick double #32a1ce;' } })
    for (let i = 1; i <= 3; i++) leCours.push(j3pAddElt(stor.zoneexplication1, 'div', ''))
    j3pAffiche(leCours[0], '', textes.corr1)
    leCours[0].style.textDecoration = 'underline'
    j3pAffiche(leCours[1], '', textes.corr2)
    j3pAffiche(leCours[2], '', textes.corr3)
    stor.zoneexplication1.style.border = 'thick double #32a1ce'
    stor.zoneexplication1.style.borderRadius = '7px'
    stor.zoneexplication2.style.paddingTop = '10px'
    const corr4 = (stor.typeQuestion === 'superieur')
      ? textes.corr4_1
      : (stor.typeQuestion === 'inferieur1') ? textes.corr4_2 : textes.corr4_3
    const corr5 = (stor.typeQuestion === 'superieur')
      ? textes.corr5_1
      : (stor.typeQuestion === 'inferieur1') ? textes.corr5_2 : textes.corr5_3
    j3pAffiche(stor.zoneexplication2, '', corr4, stor.objCons)
    j3pAffiche(stor.zoneexplication3, '', corr5, stor.objCons)
  }
  function genereDonnees (type) {
    // type vaut inferieur1, inferieur2, superieur
    // Pour inferieur1, on demande P(|X-\\mu|>\\delta) comme pour le cours
    // Pour inférieur2, on demande P(X\in]\\mu-\\delta;\\mu+\\delta[)
    // Pour superieur, on demande P(|X-\\mu|\\geq k)
    const obj = {}
    obj.X = j3pGetRandomElt(['X', 'Y', 'Z'])
    obj.e = j3pGetRandomInt(5, 15) * 10
    obj.V = Math.floor(obj.e / 10) + j3pGetRandomInt(1, Math.floor(obj.e / 10))
    do {
      obj.d = j3pGetRandomInt(Math.ceil(obj.V * 2 / 3), Math.floor(2.5 * obj.V))
    } while (Math.abs(obj.V - obj.d) <= 3)
    obj.rep = (type === 'superieur') ? obj.V / Math.pow(obj.d, 2) : 1 - obj.V / Math.pow(obj.d, 2)
    obj.a = 3
    obj.r = Math.round(obj.rep * Math.pow(10, obj.a)) / Math.pow(10, obj.a)
    obj.p = (type === 'superieur')
      ? 'P(|X-' + obj.e + '|\\geq ' + obj.d + ')'
      : (type === 'inferieur1')
          ? 'P(|X-' + obj.e + '|\\leq ' + obj.d + ')'
          : 'P(X\\in]' + (obj.e - obj.d) + ';' + (obj.e + obj.d) + '[)'
    obj.s = (type === 'superieur') ? '\\leq' : '\\geq'
    return obj
  }

  function enonceInitFirst () {
    ds = me.donneesSection
    // Construction de la page

    // Le paramètre définit la largeur relative de la première colonne

    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    let casFigure = ['inferieur1', 'inferieur2']
    const casFigureInit = ['inferieur1', 'inferieur2', 'superieur']
    stor.typeQuest = ['superieur']
    for (let k = 1; k < ds.nbrepetitions; k++) {
      if (casFigure.length === 0) casFigure = casFigureInit.slice(0)
      const alea = j3pGetRandomInt(0, casFigure.length - 1)
      stor.typeQuest.push(casFigure[alea])
      casFigure.splice(alea, 1)
    }
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    stor.typeQuestion = stor.typeQuest[me.questionCourante - 1]
    stor.objCons = genereDonnees(stor.typeQuestion)
    const cons2 = (stor.typeQuestion === 'superieur') ? textes.consigne2_1 : textes.consigne2_2
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objCons)
    j3pAffiche(stor.zoneCons2, '', cons2, stor.objCons)
    const elt = j3pAffiche(stor.zoneCons3, '', '$£p£s$&1&.',
      {
        p: stor.objCons.p,
        s: stor.objCons.s,
        inputmq1: { texte: '' }
      })
    stor.zoneInput = elt.inputmqList[0]
    j3pAffiche(stor.zoneCons4, '', textes.indic, { style: { fontSize: '0.9em', fontStyle: 'italic' }, a: stor.objCons.a })

    me.logIfDebug('stor.objCons:', stor.objCons)
    stor.zoneInput.typeReponse = ['nombre', 'approche', Math.pow(10, -stor.objCons.a)]
    stor.zoneInput.reponse = [stor.objCons.rep]
    mqRestriction(stor.zoneInput, '\\d,.')
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    const zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCorr = j3pAddElt(zoneMD, 'div')
    stor.zoneCalc = j3pAddElt(zoneMD, 'div')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    j3pFocus(stor.zoneInput)
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
