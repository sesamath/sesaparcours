import { j3pAddElt, j3pFocus, j3pGetRandomInt, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { tableauDonneesPrbs } from 'src/legacy/outils/tableauxProba/tabLoiProba'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Avril 2018
        Calcul de la probabilité d’un événement regroupement d’évts élémentaires
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pAffiche(stor.zoneExpli1, '', ds.textes['corr1_' + stor.objetRep.quest + '_' + stor.objetRep.choixQuest])
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2, {
      c: stor.objetRep.leCalcul,
      r: j3pVirgule(stor.objetRep.rep)
    })
    // dans explication2, ce qui correspond à la deuxième question
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Calculer un probabilité à partir d’une loi',
        // on donne les phrases de la consigne
        consigne1_1: 'Un cuisinier a étudié les plats commandés par ses clients.',
        consigne2_1: 'On a représenté ci-dessous la loi de probabilité correspondant aux plats choisis :',
        consigne3_1: 'Un client se présente au restaurant.',
        consigne4_1_1: 'La probabilité qu’il choisisse de la viande vaut &1&.',
        consigne4_1_2: 'La probabilité qu’il choisisse du poisson vaut &1&.',
        textTableau1: 'poulet|truite|cabillaud|bœuf|agneau',
        legendes1: 'plat|probabilité',
        consigne1_2: 'Une agence de voyage a effectué une étude des destinations choisies par ses clients.',
        consigne2_2: 'On a représenté ci-dessous la loi de probabilité correspondant aux pays choisis :',
        consigne3_2: 'Un client se présente à l’agence.',
        consigne4_2_1: 'La probabilité qu’il choisisse un pays Européen vaut &1&.',
        consigne4_2_2: 'La probabilité qu’il choisisse un pays du continent américain vaut &1&.',
        textTableau2: 'Italie|Croatie|Mexique|Japon|Suède|Brésil',
        legendes2: 'pays|probabilité',
        consigne1_3: 'Un groupe de lycéens a enquêté pour connaître le sport préféré des élèves de leur établissement.',
        consigne2_3: 'On a représenté ci-dessous la loi de probabilité correspondant aux sports choisis :',
        consigne3_3: 'Un élève se présente et on lui demande quel est son sport préféré.',
        consigne4_3_1: 'La probabilité qu’il choisisse un sport de ballon ou de balle vaut &1&.',
        consigne4_3_2: 'La probabilité qu’il choisisse un sport de raquette vaut &1&.',
        textTableau3: 'football|tennis|rugby|badminton|course à pied',
        legendes3: 'sport|probabilité',
        consigne1_4: 'Le proviseur d’un lycée a enquêté auprès des élèves de seconde désirant poursuivre en première générale afin de connaître la discipline qui influencera le choix de leur premier enseignement de spécialité.',
        consigne2_4: 'On a représenté ci-dessous la loi de probabilité correspondant aux disciplines relevées :',
        consigne3_4: 'Un élève de seconde se présente et on lui demande quelle discipline influencera le choix de son premier enseignement de spécialité.',
        consigne4_4_1: 'La probabilité qu’il réponde par un enseignement de mathématiques, physique-chime ou S.V.T. vaut &1&.',
        consigne4_4_2: 'La probabilité qu’il réponde par un enseignement qui ne serait ni mathématiques, ni physique-chime ni S.V.T. vaut &1&.',
        textTableau4: 'mathématiques|physique-chimie|S.E.S|histoire|S.V.T.|Anglais',
        legendes4: 'discipline|probabilité',
        consigne1_5: 'On a enquêté sur les cadeaux de Noël reçus par un groupe d’adolescents.',
        consigne2_5: 'On a représenté ci-dessous la loi de probabilité correspondant aux cadeaux relevées :',
        consigne3_5: 'On demande à un adolescent ce qu’il a reçu à Noël.',
        consigne4_5_1: 'La probabilité qu’il réponde par un objet issu des nouvelles technologies vaut &1&.',
        consigne4_5_2: 'La probabilité qu’il ait reçu un cadeau qui ne soit pas un objet issu des nouvelles technologies vaut &1&.',
        textTableau5: 'montre connectée|jeu de société|vêtement|ordinateur|tablette',
        legendes5: 'discipline|probabilité',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: '',
        // et les phrases utiles pour les explications de la réponse
        corr1_1_1: 'Les viandes sont le poulet, le bœuf et l’agneau.',
        corr1_1_2: 'Les poissons sont la truite et le cabillaud.',
        corr1_2_1: 'Les pays européens sont l’Italie, la Croatie et la Suède.',
        corr1_2_2: 'Les pays américains sont le Mexique et le Brésil.',
        corr1_3_1: 'Les sports de ballon ou de balle sont le football, le tennis et le rugby.',
        corr1_3_2: 'Les sports de raquette sont le tennis et le badminton.',
        corr1_4_1: 'Les disciplines mesurées sont les mathématiques, la physique-chime et les S.V.T.',
        corr1_4_2: 'Les disciplines mesurées sont les S.E.S., l’histoire et l’anglais.',
        corr1_5_1: 'Les objets issus des nouvelles technologies sont la montre connectée, l’ordinateur et la tablette.',
        corr1_5_2: 'Les objets qui ne sont pas issus des nouvelles technologies sont le jeu de société et le vêtement.',
        corr2: 'Donc la probabilité cherchée vaut $£c=£r$'
      },
      pe: 0
    }
  }
  function enonceMain () {
  // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.probabilites = []
    const prb = []
    let choixQuest = 1
    let // Pour un même sujet, on peut avoir plusieurs choix de questions
      laReponse
    let leCalcul
    switch (quest) {
      case 1:
        do {
          prb[0] = j3pGetRandomInt(20, 30)
          prb[1] = j3pGetRandomInt(20, 30)
          prb[2] = j3pGetRandomInt(20, 30)
          prb[3] = j3pGetRandomInt(15, 25)
        } while (prb[0] + prb[1] + prb[2] + prb[3] > 93)
        prb[4] = 100 - prb[0] - prb[1] - prb[2] - prb[3]
        for (let i = 0; i < prb.length; i++) {
          stor.probabilites.push(prb[i] / 100)
        }
        choixQuest = j3pGetRandomInt(1, 2)
        laReponse = (choixQuest === 1) ? (prb[0] + prb[3] + prb[4]) / 100 : (prb[1] + prb[2]) / 100
        leCalcul = (choixQuest === 1)
          ? j3pVirgule(stor.probabilites[0]) + '+' + j3pVirgule(stor.probabilites[3]) + '+' + j3pVirgule(stor.probabilites[4])
          : j3pVirgule(stor.probabilites[1]) + '+' + j3pVirgule(stor.probabilites[2])
        break
      case 2:
        // textTableau2 : "Italie|Croatie|Mexique|Japon|Suède|Brésil",
        do {
          prb[0] = j3pGetRandomInt(25, 30)
          prb[1] = j3pGetRandomInt(13, 19)
          prb[2] = j3pGetRandomInt(7, 13)
          prb[3] = j3pGetRandomInt(8, 15)
          prb[4] = j3pGetRandomInt(18, 25)
        } while (prb[0] + prb[1] + prb[2] + prb[3] + prb[4] > 95)
        prb[5] = 100 - prb[0] - prb[1] - prb[2] - prb[3] - prb[4]
        for (let i = 0; i < prb.length; i++) {
          stor.probabilites.push(prb[i] / 100)
        }
        choixQuest = j3pGetRandomInt(1, 2)
        laReponse = (choixQuest === 1) ? (prb[0] + prb[1] + prb[4]) / 100 : (prb[2] + prb[5]) / 100
        leCalcul = (choixQuest === 1)
          ? j3pVirgule(stor.probabilites[0]) + '+' + j3pVirgule(stor.probabilites[1]) + '+' + j3pVirgule(stor.probabilites[4])
          : j3pVirgule(stor.probabilites[2]) + '+' + j3pVirgule(stor.probabilites[5])
        break
      case 3:
        // "football|tennis|rugby|badminton|course à pied",
        do {
          prb[0] = j3pGetRandomInt(35, 45)
          prb[1] = j3pGetRandomInt(12, 18)
          prb[2] = j3pGetRandomInt(15, 20)
          prb[3] = j3pGetRandomInt(7, 12)
        } while (prb[0] + prb[1] + prb[2] + prb[3] > 90)
        prb[4] = 100 - prb[0] - prb[1] - prb[2] - prb[3]
        for (let i = 0; i < prb.length; i++) {
          stor.probabilites.push(prb[i] / 100)
        }
        choixQuest = j3pGetRandomInt(1, 2)
        laReponse = (choixQuest === 1) ? (prb[0] + prb[1] + prb[2]) / 100 : (prb[1] + prb[3]) / 100
        leCalcul = (choixQuest === 1)
          ? j3pVirgule(stor.probabilites[0]) + '+' + j3pVirgule(stor.probabilites[1]) + '+' + j3pVirgule(stor.probabilites[2])
          : j3pVirgule(stor.probabilites[1]) + '+' + j3pVirgule(stor.probabilites[3])
        break
      case 4:
        // textTableau4 : "mathématiques|physique-chimie|S.E.S|histoire|S.V.T.|Anglais",
        do {
          prb[0] = j3pGetRandomInt(25, 30)
          prb[1] = j3pGetRandomInt(13, 19)
          prb[2] = j3pGetRandomInt(7, 13)
          prb[3] = j3pGetRandomInt(8, 15)
          prb[4] = j3pGetRandomInt(18, 25)
        } while (prb[0] + prb[1] + prb[2] + prb[3] + prb[4] > 93)
        prb[5] = 100 - prb[0] - prb[1] - prb[2] - prb[3] - prb[4]
        for (let i = 0; i < prb.length; i++) {
          stor.probabilites.push(prb[i] / 100)
        }
        choixQuest = j3pGetRandomInt(1, 2)
        laReponse = (choixQuest === 1) ? (prb[0] + prb[1] + prb[4]) / 100 : (prb[2] + prb[3] + prb[5]) / 100
        leCalcul = (choixQuest === 1)
          ? j3pVirgule(stor.probabilites[0]) + '+' + j3pVirgule(stor.probabilites[1]) + '+' + j3pVirgule(stor.probabilites[4])
          : j3pVirgule(stor.probabilites[2]) + '+' + j3pVirgule(stor.probabilites[3]) + '+' + j3pVirgule(stor.probabilites[5])
        break
      case 5 :
        // "montre connectée|jeu de société|vêtement|ordinateur|tablette",
        do {
          prb[0] = j3pGetRandomInt(15, 25)
          prb[1] = j3pGetRandomInt(20, 30)
          prb[2] = j3pGetRandomInt(15, 20)
          prb[3] = j3pGetRandomInt(15, 20)
        } while (prb[0] + prb[1] + prb[2] + prb[3] > 87)
        prb[4] = 100 - prb[0] - prb[1] - prb[2] - prb[3]
        for (let i = 0; i < prb.length; i++) {
          stor.probabilites.push(prb[i] / 100)
        }
        choixQuest = j3pGetRandomInt(1, 2)
        laReponse = (choixQuest === 1) ? (prb[0] + prb[3] + prb[4]) / 100 : (prb[1] + prb[2]) / 100
        leCalcul = (choixQuest === 1)
          ? j3pVirgule(stor.probabilites[0]) + '+' + j3pVirgule(stor.probabilites[3]) + '+' + j3pVirgule(stor.probabilites[4])
          : j3pVirgule(stor.probabilites[1]) + '+' + j3pVirgule(stor.probabilites[2])
        break
    }

    j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + quest])
    j3pAffiche(stor.zoneCons2, '', ds.textes['consigne2_' + quest])

    stor.largeurTab = me.zonesElts.MG.getBoundingClientRect().width * stor.pourc / 100
    const mesDonnees = {
      parent: stor.zoneCons3,
      legendes: ds.textes['legendes' + quest].split('|'),
      textes: ds.textes['textTableau' + quest].split('|'),
      probas: stor.probabilites,
      complet: false,
      largeur: stor.largeurTab,
      typeFont: me.styles.petit.enonce
    }
    stor.maLoi = tableauDonneesPrbs(mesDonnees)
    j3pAffiche(stor.zoneCons4, '', ds.textes['consigne3_' + quest])
    const elt = j3pAffiche(stor.zoneCons5, '', ds.textes['consigne4_' + quest + '_' + choixQuest])
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d.,')
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [laReponse]
    stor.objetRep = { leCalcul, choixQuest, rep: laReponse, quest }
    j3pFocus(stor.zoneInput)
    // console.log("laReponse:",laReponse)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70

        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc / 100 })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zones.IG, ds.indication)
        stor.tabQuest = [1, 2, 3, 4, 5]
        stor.tabQuestInit = [...stor.tabQuest]
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
