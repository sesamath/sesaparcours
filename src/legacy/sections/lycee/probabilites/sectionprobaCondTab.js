import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pNombre, j3pPaletteMathquill, j3pPGCD, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import tabDoubleEntree from 'src/legacy/outils/tableauxProba/tabDoubleEntree'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juillet 2019
        Dans cette section, on demande des probabiltés conditionnelles à l’aide d’un tableau à double entrées
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['fracIrre', true, 'boolean', 'Par défaut, on demande des fractions irréductibles, mais cela peut être modifié.']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.laPalette)
    if (ds.fracIrre) j3pDetruit(stor.lesInfos)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pAffiche(stor.zoneExpli1, '', ds.textes['corr1_' + stor.quest], stor.objCorr)
    j3pAffiche(stor.zoneExpli2, '', ds.textes['corr2_' + stor.quest], stor.objCorr)
    $('.mq-overline').each(function () {
      $(this).css('border-top', '1px solid ' + $(this).css('color'))
      // je parcours l’ensemble des overline et je leur donne la couleur dans laquelle se trouve le texte
    })
  }

  function ecoute (zone) {
    if (zone.className.includes('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, zone, { liste: ['fraction'] })
    }
  }
  function getDonnees () {
    return {
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      fracIrre: true,
      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par ds.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: ds.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Tableau à double entrée et probabilité conditionnelle',
        // on donne les phrases de la consigne
        consigne1_1: 'On s’intéresse au groupe sanguin et au rhésus d’un groupe de £n personnes.',
        consigne1_2: 'Dans un lycée, les £n élèves peuvent être répartis suivant leur classe et suivant qu’ils mangent ou non à la cantine le midi.',
        consigne1_3: 'Dans un lycée, on demande aux élèves et enseignants s’ils préfèrent avoir cours le matin ou l’après-midi.',
        consigne1_4: 'Dans un club d’échec, on peut répartir les £n adhérents suivant leur axe et leur sexe.',
        consigne1_5: 'Une étudiante a fabriqué £n bijoux qu’elle classe en différentes catégories pour les vendre.',
        consigne2: 'Ces informations ont été reportées dans le tableau ci-dessous &nbsp;:',
        consigne3: 'Calculer les deux probabilités ci-dessous&nbsp;:',
        colonne1: 'Rhésus+|Rhésus-',
        ligne1: 'Groupe O|Groupe A|Groupe B|Groupe AB',
        colonne2: 'Seconde|Première|Terminale',
        ligne2: 'Cantine|Extérieur',
        colonne3: 'Elève|Professeur',
        ligne3: 'Matin|Après-midi',
        colonne4: 'Femme|Homme',
        ligne4: 'Moins de 18 ans|Entre 18 et 35 ans|Plus de 35 ans',
        colonne5: 'Argenté|Doré',
        ligne5: 'Colliers|Bracelets|Paires de boucles d’oreille',
        consigne3_1: 'On choisit au hasard un membre de ce groupe de personnes et on note les événements&nbsp;:',
        consigne3_2: 'On choisit au hasard un élève de ce lycée et on note les événements&nbsp;:',
        consigne3_3: 'On choisit au hasard une personne de ce lycée (parmi élèves et professeurs) et on note les événements&nbsp;:',
        consigne3_4: 'On choisit au hasard un membre de ce club et on note les événements&nbsp;:',
        consigne3_5: 'Elle choisit au hasard un bijou de sa fabrication et on note les événements&nbsp;:',
        evts1: '$R$ : la personne est de rhésus positif&nbsp;;|' +
          '$O$ : la personne est du groupe O&nbsp;;|' +
          '$A$ : la personne est du groupe A&nbsp;;|' +
          '$B$ : la personne est du groupe B&nbsp;;|' +
          '$C$ : la personne est du groupe AB.',
        evts2: '$C$ : la personne mange à la cantine&nbsp;;|' +
          '$S$ : l’élève est en seconde&nbsp;;|' +
          '$P$ : l’élève est en première&nbsp;;|' +
          '$T$ : l’élève est en terminale.',
        evts3: '$M$ : la personne préfère le matin&nbsp;;|' +
          '$E$ : la personne est un élève.',
        evts4: '$F$ : le membre choisi est une femme&nbsp;;|' +
          '$M$ : le membre choisi a moins de 18 ans&nbsp;;|' +
          '$E$ : le membre choisi a entre 18 et 35 ans&nbsp;;|' +
          '$T$ : le membre choisi a plus de 35 ans.',
        evts5: '$A$ : le bijou choisi est argenté&nbsp;;|' +
          '$C$ : le bijou choisi est un collier&nbsp;;|' +
          '$B$ : le bijou choisi est un bracelet&nbsp;;|' +
          '$O$ : le bijou choisi est une paire de boucles d’oreille.',
        rhesus: 'positif|négatif',
        groupe: 'O|A|B|AB',
        niveau: 'seconde|première|terminale',
        mange: 'la cantine|l’extérieur',
        personne: 'élèves|professeurs',
        journee: 'le matin|l’après-midi',
        age: 'moins de 18 ans|entre 18 et 35 ans|plus de 35 ans',
        sexe: 'femmes|hommes',
        bijou: 'colliers|bracelets|paires de boucles d’oreille',
        materiau: 'argentés|dorés',
        materiauf: 'argentées|dorées',
        nomEvtsLigne1: 'O|A|B|C',
        nomEvtsColonne1: 'R|\\overline{R}',
        nomEvtsLigne2: 'C|\\overline{C}',
        nomEvtsColonne2: 'S|P|T',
        nomEvtsLigne3: 'M|\\overline{M}',
        nomEvtsColonne3: 'E|\\overline{E}',
        nomEvtsLigne4: 'M|E|T',
        nomEvtsColonne4: 'F|\\overline{F}',
        nomEvtsLigne5: 'C|B|O',
        nomEvtsColonne5: 'A|\\overline{A}',
        total: 'Total',
        info1: 'Les probabilités devront être écrites de manière simplifiée, sous la forme décimale ou d’une fraction irréductible.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Les résultats doivent être exacts (non approchés) et donnés sous forme irréductible&nbsp;!',
        comment2: 'Une probabilité est un nombre compris entre 0 et 1&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'Parmi les £{n1} personnes du groupe £{p1}, £{n2} sont de rhésus £{g1}, donc la probabilité cherchée vaut $£{prb1}=£{r1}$.',
        corr2_1: 'Parmi les £{n3} personnes de rhésus £{g2}, £{n4} sont du groupe £{p2}, donc la probabilité cherchée vaut $£{prb2}=£{r2}$.',
        corr1_2: 'Parmi les £{n1} élèves qui mangent à £{p1}, £{n2} sont en £{g1}, donc la probabilité cherchée vaut $£{prb1}=£{r1}$.',
        corr2_2: 'Parmi les £{n3} élèves de £{g2}, £{n4} mangent à £{p2}, donc la probabilité cherchée vaut $£{prb2}=£{r2}$.',
        corr1_3: 'Parmi les £{n1} personnes qui préfèrent £{p1}, £{n2} sont des £{g1}, donc la probabilité cherchée vaut $£{prb1}=£{r1}$.',
        corr2_3: 'Parmi les £{n3} £{g2}, £{n4} préfèrent £{p2}, donc la probabilité cherchée vaut $£{prb2}=£{r2}$.',
        corr1_4: 'Parmi les £{n1} membres qui ont £{p1}, £{n2} sont des £{g1}, donc la probabilité cherchée vaut $£{prb1}=£{r1}$.',
        corr2_4: 'Parmi les £{n3} £{g2} membres du club, £{n4} ont £{p2}, donc la probabilité cherchée vaut $£{prb2}=£{r2}$.',
        corr1_5: 'Parmi les £{n1} £{p1}, £{n2} sont £{g1}, donc la probabilité cherchée vaut $£{prb1}=£{r1}$.',
        corr2_5: 'Parmi les £{n3} bijoux £{g2}, £{n4} sont des £{p2}, donc la probabilité cherchée vaut $£{prb2}=£{r2}$.'
      },
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()

    // Construction de la page
    stor.pourc = 70
    me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc / 100 })

    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    stor.tabQuest = [1, 2, 3, 4, 5]
    stor.tabQuestInit = [...stor.tabQuest]
  }

  function enonceMain () {
    if (stor.tabQuest.length === 0) {
      stor.tabQuest = [...stor.tabQuestInit]
    }
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.quest = quest

    const objTab = {}
    objTab.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
    objTab.lestyle = { styletexte: me.styles.toutpetit.enonce, couleur: me.styles.toutpetit.enonce.color }
    objTab.nomsLigne = ds.textes['ligne' + quest].split('|')
    objTab.nomsLigne.push(ds.textes.total)
    objTab.nomsColonne = ds.textes['colonne' + quest].split('|')
    objTab.nomsColonne.push(ds.textes.total)
    const tabEvts1 = ds.textes['nomEvtsLigne' + quest].split('|')
    const tabEvts2 = ds.textes['nomEvtsColonne' + quest].split('|')
    // Il nous faut maintenant gérer l’aléatoire
    let effTotal
    const effTabCol = []
    const // tableau contenant l’effectif total de chaque événement présent par colonne (donc de longueur tabEvts1.length)
      effTableLigne = []
    let borneInf, borneSup, effLigne2, estPositif
    do {
      switch (quest) {
        case 1:
          effTotal = j3pGetRandomInt(3, 6) * 100
          {
            const nbAB = effTotal / 100 * j3pGetRandomInt(2, 3) + j3pGetRandomInt(1, 5)
            const nbB = effTotal / 100 * j3pGetRandomInt(7, 9) + j3pGetRandomInt(1, 5)
            const nbA = effTotal / 100 * j3pGetRandomInt(37, 50) + j3pGetRandomInt(1, 10)
            effTabCol.push(Math.round(effTotal - nbA - nbB - nbAB), nbA, nbB, nbAB)
            for (let i = 0; i < effTabCol.length; i++) {
              borneInf = Math.max(Math.floor(effTabCol[i] / 7) - 5, 2)
              borneSup = Math.min(Math.ceil(effTabCol[i] / 7), effTabCol[i] - 2)
              effLigne2 = j3pGetRandomInt(borneInf, borneSup)
              effTableLigne[i] = [effTabCol[i] - effLigne2, effLigne2]
            }
          }
          break
        case 2:
          effTotal = j3pGetRandomInt(1100, 1600)
          {
            const nbcantine = Math.round(effTotal * j3pGetRandomInt(70, 80) / 100) + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(7, 25)
            effTabCol.push(nbcantine, Math.round(effTotal - nbcantine))
            for (let i = 0; i < effTabCol.length; i++) {
              borneInf = Math.max(Math.floor(effTabCol[i] / 3) - 10, 15)
              borneSup = Math.min(Math.ceil(effTabCol[i] / 3) + 10, effTabCol[i] - 15)
              const effLigne1 = j3pGetRandomInt(borneInf, borneSup)
              const borneInf2 = Math.max(Math.floor(effTabCol[i] / 3) - 10, 15)
              const borneSup2 = Math.min(Math.ceil(effTabCol[i] / 3) + 10, effTabCol[i] - 15)
              effLigne2 = j3pGetRandomInt(borneInf2, borneSup2)
              effTableLigne[i] = [effLigne1, effLigne2, effTabCol[i] - effLigne2 - effLigne1]
            }
          }
          break
        case 3:
          effTotal = j3pGetRandomInt(1300, 1700)
          {
            const nbmatin = Math.round(effTotal * j3pGetRandomInt(58, 63) / 100) + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(7, 25)
            effTabCol.push(nbmatin, Math.round(effTotal - nbmatin))
            for (let i = 0; i < effTabCol.length; i++) {
              borneInf = Math.max(Math.floor(effTabCol[i] / 7) - 10, 10)
              borneSup = Math.ceil(effTabCol[i] / 7) + 15
              effLigne2 = j3pGetRandomInt(borneInf, borneSup)
              effTableLigne[i] = [effTabCol[i] - effLigne2, effLigne2]
            }
          }
          break
        case 4:
          effTotal = j3pGetRandomInt(150, 220)
          {
            const nbmoins18 = Math.round(effTotal / 5) + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 8)
            const nbplus35 = Math.round(effTotal * 2 / 5) + j3pGetRandomInt(5, 15)
            effTabCol.push(nbmoins18, Math.round(effTotal - nbmoins18 - nbplus35), nbplus35)
            for (let i = 0; i < effTabCol.length; i++) {
              borneInf = Math.max(Math.floor(effTabCol[i] * 2 / 5) - 5, 20)
              borneSup = Math.ceil(effTabCol[i] * 2 / 5) + 15
              effLigne2 = j3pGetRandomInt(borneInf, borneSup)
              effTableLigne[i] = [effLigne2, effTabCol[i] - effLigne2]
            }
          }
          break
        case 5:
          effTotal = j3pGetRandomInt(100, 160)
          {
            const nbcolliers = Math.round(effTotal / 3) + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 8)
            const nbbracelets = Math.round(effTotal / 3) + j3pGetRandomInt(5, 15)
            effTabCol.push(nbcolliers, nbbracelets, Math.round(effTotal - nbcolliers - nbbracelets))
            for (let i = 0; i < effTabCol.length; i++) {
              borneInf = Math.max(Math.floor(effTabCol[i] * 3 / 5) - 5, 5)
              borneSup = Math.min(Math.ceil(effTabCol[i] * 4 / 5) + 15, effTabCol[i] - 15)
              effLigne2 = j3pGetRandomInt(borneInf, borneSup)
              effTableLigne[i] = [effLigne2, effTabCol[i] - effLigne2]
            }
          }
          break
      }
      const sommeLigne = []
      objTab.effectifs = {}
      for (let j = 0; j < effTableLigne[0].length; j++) {
        sommeLigne.push(0)
        objTab.effectifs['ligne' + (j + 1)] = []
      }
      for (let i = 0; i < effTabCol.length; i++) {
        for (let j = 0; j < effTableLigne[i].length; j++) {
          objTab.effectifs['ligne' + (j + 1)].push(effTableLigne[i][j])
          sommeLigne[j] += effTableLigne[i][j]
        }
      }
      for (let j = 0; j < effTableLigne[0].length; j++) {
        objTab.effectifs['ligne' + (j + 1)].push(sommeLigne[j])
      }
      // obj.effectifs['ligne'+i] contiendra les effectifs sur chaque ligne associée
      objTab.effectifs['ligne' + (effTableLigne[0].length + 1)] = [...effTabCol]
      objTab.effectifs['ligne' + (effTableLigne[0].length + 1)].push(effTotal)
      objTab.effTotal = effTotal
      objTab.effTableLigne = effTableLigne
      objTab.effTableColonne = effTabCol
      stor.tabEvts1 = tabEvts1
      // sec.tabEvtsTxt1 = tabEvtsTxt1;
      stor.tabEvts2 = tabEvts2
      // sec.tabEvtsTxt2 = tabEvtsTxt2;
      // Choix des probabilités
      estPositif = true
      for (const ligne in objTab.effectifs) {
        objTab.effectifs[ligne].forEach(nb => {
          estPositif = estPositif && (nb > 0)
        })
      }
      // Tous les termes du tableau doivent être positifs. Ce devrait être le cas, mais j’ai eu un bug lors d’un déploiement en préprod que je n’ai jamais réussi à obtenir par ailleurs
      // Donc j’effectue cette vérification qui ne va pas coûter cher.
    } while (!estPositif)
    const tabEvtLigne = ds.textes['nomEvtsLigne' + quest].split('|')
    const tabEvtColonne = ds.textes['nomEvtsColonne' + quest].split('|')
    const pioche1Prb1 = Math.floor(Math.random() * tabEvtLigne.length)
    const pioche2Prb1 = Math.floor(Math.random() * tabEvtColonne.length)
    const pioche1Prb2 = Math.floor(Math.random() * tabEvtColonne.length)
    const pioche2Prb2 = Math.floor(Math.random() * tabEvtLigne.length)
    objTab.prbEnonce = [[tabEvtLigne[pioche1Prb1], tabEvtColonne[pioche2Prb1]], [tabEvtColonne[pioche1Prb2], tabEvtLigne[pioche2Prb2]]]
    stor.objTab = objTab
    me.logIfDebug('objTab:', stor.objTab, '\npioche1Prb1:', pioche1Prb1, 'pioche2Prb1:', pioche2Prb1, 'pioche1Prb2:', pioche1Prb2, 'pioche2Prb2:', pioche2Prb2, '\nobjTab.prbEnonce:', objTab.prbEnonce)
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 8; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + quest], { n: effTotal })
    j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2)
    tabDoubleEntree(stor.zoneCons3, stor.objTab)
    j3pAffiche(stor.zoneCons4, '', ds.textes['consigne3_' + quest])
    const tabEvts = ds.textes['evts' + quest].split('|')
    for (let i = 0; i < tabEvts.length; i++) {
      const zoneEvt = j3pAddElt(stor.zoneCons5, 'div')
      j3pAffiche(zoneEvt, '', tabEvts[i])
    }
    j3pAffiche(stor.zoneCons6, '', ds.textes.consigne3)
    const elt1 = j3pAffiche(stor.zoneCons7, '', '$P_{' + objTab.prbEnonce[0][0] + '}(' + objTab.prbEnonce[0][1] + ')=$&1&',
      {
        inputmq1: { texte: '' }
      })
    const elt2 = j3pAffiche(stor.zoneCons8, '', '$P_{' + objTab.prbEnonce[1][0] + '}(' + objTab.prbEnonce[1][1] + ')=$&1&',
      {
        inputmq1: { texte: '' }
      })
    stor.zoneInput = [elt1.inputmqList[0], elt2.inputmqList[0]]
    stor.zoneInput.forEach(zone => {
      mqRestriction(zone, '\\d.,/-', { commandes: ['fraction'] })
      zone.typeReponse = ['nombre', 'exact']
      zone.solutionSimplifiee = (ds.fracIrre)
        ? ['reponse fausse', 'non valide']
        : ['valide et on accepte', 'valide et on accepte']
      $(zone).focusin(function () { ecoute(this) })
    })
    // je définis ici toutes les variables présentes dans les textes qui vont me servir dans la correction
    const objCorr = {}
    objCorr.n1 = objTab.effectifs['ligne' + (tabEvtColonne.length + 1)][pioche1Prb1]
    objCorr.n2 = objTab.effectifs['ligne' + (pioche2Prb1 + 1)][pioche1Prb1]
    objCorr.n3 = objTab.effectifs['ligne' + (pioche1Prb2 + 1)][tabEvtLigne.length]
    objCorr.n4 = objTab.effectifs['ligne' + (pioche1Prb2 + 1)][pioche2Prb2]
    objCorr.prb1 = 'P_{' + objTab.prbEnonce[0][0] + '}(' + objTab.prbEnonce[0][1] + ')'
    objCorr.prb2 = 'P_{' + objTab.prbEnonce[1][0] + '}(' + objTab.prbEnonce[1][1] + ')'
    const pgcdrep1 = j3pPGCD(objCorr.n2, objCorr.n1)
    objCorr.r1 = (pgcdrep1 === 1) ? '\\frac{' + objCorr.n2 + '}{' + objCorr.n1 + '}' : '\\frac{' + objCorr.n2 + '}{' + objCorr.n1 + '}=\\frac{' + (objCorr.n2 / pgcdrep1) + '}{' + (objCorr.n1 / pgcdrep1) + '}'
    const pgcdrep2 = j3pPGCD(objCorr.n4, objCorr.n3)
    objCorr.r2 = (pgcdrep2 === 1) ? '\\frac{' + objCorr.n4 + '}{' + objCorr.n3 + '}' : '\\frac{' + objCorr.n4 + '}{' + objCorr.n3 + '}=\\frac{' + (objCorr.n4 / pgcdrep2) + '}{' + (objCorr.n3 / pgcdrep2) + '}'
    switch (quest) {
      case 1:
        {
          const groupe = ds.textes.groupe.split('|')
          objCorr.p1 = groupe[pioche1Prb1]
          objCorr.p2 = groupe[pioche2Prb2]
          const rhesus = ds.textes.rhesus.split('|')
          objCorr.g1 = rhesus[pioche2Prb1]
          objCorr.g2 = rhesus[pioche1Prb2]
        }
        break
      case 2:
        {
          const niveau = ds.textes.niveau.split('|')
          const mange = ds.textes.mange.split('|')
          objCorr.p1 = mange[pioche1Prb1]
          objCorr.p2 = mange[pioche2Prb2]
          objCorr.g1 = niveau[pioche2Prb1]
          objCorr.g2 = niveau[pioche1Prb2]
        }
        break
      case 3:
        {
          const personne = ds.textes.personne.split('|')
          const journee = ds.textes.journee.split('|')
          objCorr.p1 = journee[pioche1Prb1]
          objCorr.p2 = journee[pioche2Prb2]
          objCorr.g1 = personne[pioche2Prb1]
          objCorr.g2 = personne[pioche1Prb2]
        }
        break
      case 4:
        {
          const age = ds.textes.age.split('|')
          const sexe = ds.textes.sexe.split('|')
          objCorr.p1 = age[pioche1Prb1]
          objCorr.p2 = age[pioche2Prb2]
          objCorr.g1 = sexe[pioche2Prb1]
          objCorr.g2 = sexe[pioche1Prb2]
        }
        break
      case 5:
        {
          const bijou = ds.textes.bijou.split('|')
          const materiau = (pioche1Prb1 === 2) ? ds.textes.materiauf.split('|') : ds.textes.materiau.split('|')
          objCorr.p1 = bijou[pioche1Prb1]
          objCorr.p2 = bijou[pioche2Prb2]
          objCorr.g1 = materiau[pioche2Prb1]
          objCorr.g2 = materiau[pioche1Prb2]
        }
        break
    }
    stor.zoneInput[0].reponse = [objCorr.n2 / objCorr.n1]
    stor.zoneInput[1].reponse = [objCorr.n4 / objCorr.n3]
    stor.objCorr = objCorr

    me.logIfDebug('stor.objCorr:', stor.objCorr, '\nles réponses:', objCorr.n2 / objCorr.n1, 'et', objCorr.n4 / objCorr.n3)
    stor.laPalette = j3pAddElt(stor.conteneur, 'div')
    // palette MQ :
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], {
      liste: ['fraction']
    })
    j3pStyle(stor.laPalette, { paddingBottom: '40px' })

    if (ds.fracIrre) {
      stor.lesInfos = j3pAddElt(stor.conteneur, 'div')
      j3pStyle(stor.lesInfos, { fontSize: '0.9em', fontStyle: 'italic' })
      j3pAffiche(stor.lesInfos, '', ds.textes.info1)
    }
    j3pFocus(stor.zoneInput[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        const fracIrre = (fctsValid.zones.reponseSimplifiee[0][1] && fctsValid.zones.reponseSimplifiee[1][1])
        if (!reponse.aRepondu && (stor.zoneInput[0].solutionSimplifiee[1] === 'non valide')) {
        // Si c’est un pb de fraction irréductible, la première fois, je donne un message d’avertissement et ensuite, je compte ça comme faux
          if (!fracIrre) {
            for (let j = 1; j >= 0; j--) stor.zoneInput[j].solutionSimplifiee[1] = 'reponse fausse'
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (!fracIrre) {
          // c’est que la fraction de la quatrième zone de saisie est réductible
            msgReponseManquante = ds.textes.comment1
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              const nbRepondu1 = j3pNombre(stor.fctsValid.zones.reponseSaisie[0])
              const nbRepondu2 = j3pNombre(stor.fctsValid.zones.reponseSaisie[1])

              if (!isNaN(nbRepondu1) && !isNaN(nbRepondu2)) {
              // on a bien des nombre
              // S’il n’est pas entre 0 et 1, on écrit un message
                if (nbRepondu1 < 0 || nbRepondu1 > 1 || nbRepondu2 < 0 || nbRepondu2 > 1) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
                }
              }

              if (!fracIrre) {
              // c’est que la fraction de la zone de saisie est réductible
                stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
