import $ from 'jquery'
import { j3pAddElt, j3pAjouteFoisDevantX, j3pArrondi, j3pDetruit, j3pElement, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pRandomTab, j3pShowError, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pCreeBoutonsFenetre, j3pDetruitToutesLesFenetres, j3pAfficheCroixFenetres } from 'src/legacy/core/functionsJqDialog'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Juillet 2017
        Une loi exponentielle est définie. A l’aide d’une probabilité, on cherche la valeur exacte du paramètre \\lambda de cette loi
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['afficheDemo', true, 'boolean', 'Par défaut on redonne le calcul d’une probabilité dans le cas de cette loi. Cela peut être évité en mettant false.']
  ]
}

/**
 * section loiExpoTrouverLambda
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3, 4].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pDetruit(stor.zoneCons4)
    let ecriturePrb
    if (['<', '\\leq'].indexOf(stor.param.signeIneg) > -1) {
      const signeContraire = (stor.param.signeIneg === '<') ? '\\geq' : '>'
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1_2,
        {
          v: stor.param.nomVar,
          s: stor.param.signeIneg,
          t: signeContraire
        })
      ecriturePrb = '1- \\mathrm{e}^{-' + stor.param.borne + '\\lambda}=' + j3pArrondi(1 - stor.param.valProba, 7) + '\\iff \\mathrm{e}^{-' + stor.param.borne + '\\lambda}=' + stor.param.valProba
    } else {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1_1,
        {
          v: stor.param.nomVar,
          s: stor.param.signeIneg
        })
      ecriturePrb = '\\mathrm{e}^{-' + stor.param.borne + '\\lambda}=' + stor.param.valProba
    }
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2, {
      p: ecriturePrb
    })
    const egaliteInterm = '-' + stor.param.borne + '\\lambda=\\ln(' + stor.param.valProba + ')'
    j3pAffiche(stor.zoneExpli3, '', ds.textes.corr3, {
      e: egaliteInterm,
      p: stor.param.valProba,
      b: stor.param.borne
    })
    if (ds.afficheDemo) afficheZoneAide()
  }

  function afficheZoneAide () {
    stor.idFenetreBtn = j3pGetNewId('fenetreBoutons')
    stor.idFenetreAide = j3pGetNewId('fenetreAide')
    stor.nameBtn = j3pGetNewId('Boutons')
    stor.nameAide = j3pGetNewId('Aide')
    const untableau = [
      {
        name: stor.nameBtn,
        title: ds.textes.titreDemo,
        left: me.zonesElts.MG.getBoundingClientRect().width,
        top: me.zonesElts.HD.getBoundingClientRect().height + stor.zoneCorr.getBoundingClientRect().height + 5,
        width: me.zonesElts.MD.getBoundingClientRect().width,
        height: 100,
        id: stor.idFenetreBtn
      }
    ]
    untableau.push({
      name: stor.nameAide,
      title: ds.textes.titreBouton,
      top: me.zonesElts.HD.getBoundingClientRect().height + stor.conteneur.getBoundingClientRect().height + 5,
      left: 5,
      width: me.zonesElts.MG.getBoundingClientRect().width,
      height: 270,
      id: stor.idFenetreAide
    })
    me.fenetresjq = untableau
    // Création mais sans affichage.
    j3pCreeFenetres(me)
    j3pCreeBoutonsFenetre(stor.idFenetreBtn, [[stor.nameAide, ds.textes.titreBouton]])
    j3pToggleFenetres(stor.nameBtn)
    j3pAfficheCroixFenetres(stor.nameAide)
    for (let i = 1; i <= 5; i++) {
      const divAide = j3pAddElt(stor.idFenetreAide, 'div')
      j3pAffiche(divAide, '', ds.textes['aide' + i])
    }
  }

  function depart () {
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABA+TMzNAANmcmH###8BAP8BAAAAAAAAAAADSAAAAiAAAAAAAAAAAAAAAAAAAAAP#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEAB0NDYWxjdWwA#####wADcHJiAAQwLjI1AAAAAT#QAAAAAAAAAAAAAgD#####AAFiAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAZsYW1iZGEACi1sbihwcmIpL2L#####AAAAAQAMQ01vaW5zVW5haXJl#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAIACUNGb25jdGlvbgb#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAAQAAAAYAAAAC#####wAAAAIABkNMYXRleAD#####AAAAAAH#####EkBuoAAAAAAAQFDAAAAAAAAAAAAAAAAAAAAAABZQKFg+XFZhbHtifSk9XFZhbHtwcmJ9AAAABwD#####AAAAAAH#####EkBugAAAAAAAQF+AAAAAAAAAAAAAAAAAAAAAACZcbGFtYmRhPS1cZnJhY3tcbG4gXFZhbHtwcmJ9fXtcVmFse2J9fQAAAAIA#####wALcHJvcG9zaXRpb24ACy1sbigwLjI1KS8yAAAAAwAAAAQDAAAABQYAAAABP9AAAAAAAAAAAAABQAAAAAAAAAAAAAACAP####8ABnZhbExvZwAHbG4ocHJiKQAAAAUGAAAABgAAAAEAAAACAP####8AB2xhbWJkYTIADGxuKHByYikvKC1iKQAAAAQDAAAABQYAAAAGAAAAAQAAAAMAAAAGAAAAAgAAAAcA#####wAAAAAB#####xJAfRAAAAAAAEBiAAAAAAAAAAAAAAAAAAAAAAAoXGxhbWJkYT0tXGZyYWN7XFZhbHt2YWxMb2csMTJ9fXtcVmFse2J9ff####8AAAADABBDVGVzdEVxdWl2YWxlbmNlAP####8AB2VnYWxpdGUAAAADAAAABgEBAAAAAT#wAAAAAAAAAf####8AAAACAAxDQ29tbWVudGFpcmUA#####wAAAAAB#####xJAYGAAAAAAAEBsgAAAAAAAAAAAAAAAAAAAAAAZw6lnYWxpdMOpID0gI1ZhbChlZ2FsaXRlKQAAAAIA#####wAMcHJvcG9zaXRpb24yABgtKC0xLjM4NjI5NDM2MTExOTg5MDYpLzIAAAADAAAABAMAAAADAAAAAT#2LkL++jnvAAAAAUAAAAAAAAAAAAAACAD#####AAhlZ2FsaXRlMgAAAAgAAAAGAQEAAAABP#AAAAAAAAABAAAACQD#####AAAAAAH#####EkB70AAAAAAAQG+AAAAAAAAAAAAAAAAAAAAAABvDqWdhbGl0w6kyID0gI1ZhbChlZ2FsaXRlMin###############8='
    stor.liste = stor.mtgAppLecteur.createList(txtFigure)
  }

  function getDonnees () {
    return {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,
      afficheDemo: true,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
    Les textes présents dans la section
    Sont donc accessibles dans le code par donneesSection.textes.consigne1
    Possibilité d’écrire dans le code :
    var lestextes: donneesSection.textes;
    puis accès à l’aide de lestextes.consigne1
    */
      textes: {
      // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Retrouver le paramètre',
        titreDemo: 'Complément',
        titreBouton: 'Démonstration de la propriété du cours',
        // on donne les phrases de la consigne
        consigne1: 'Soit $£v$ une variable aléatoire suivant une loi exponentielle de paramètre $\\lambda$, où $\\lambda$ désigne un réel strictement positif.',
        consigne2: 'Sachant que $£p$, déterminer la valeur exacte de $\\lambda$.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'On demande la valeur exacte et non une valeur approchée !',
        comment2: 'La valeur donnée est correcte, mais il faut la simplifier. Tu as une chance de plus pour le faire !',
        comment3: 'La valeur donnée est correcte, mais il faut la simplifier.',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'D’après la propriété du cours, pour tout réel $t\\geq 0$,<br/>$P(£v£s t)=\\mathrm{e}^{-\\lambda t}$.',
        corr1_2: 'D’après la propriété du cours, pour tout réel $t$, $P(£v£t t)=\\mathrm{e}^{-\\lambda t}$,<br/>ce qui implique $P(£v£s t)=1-\\mathrm{e}^{-\\lambda t}$.',
        corr2: 'On obtient alors, avec les données de l’énoncé $£p$.',
        corr3: 'Cette égalité équivaut à $£e$, c’est-à-dire $\\lambda=-\\frac{\\ln(£p)}{£b}$.',
        aide1: 'En effet, la variable aléatoire $X$ suivant une loi exponentielle de paramètre $\\lambda$, pour tout réel $t\\geq 0$,',
        aide2: '$P(X&lt;t)=P(X\\leq t)=\\int_0^t\\lambda \\mathrm{e}^{-\\lambda x}\\mathrm{d}x$.',
        aide3: 'Sur $[0;+\\infty[$, une primitive de la fonction $x\\mapsto \\lambda \\mathrm{e}^{-\\lambda x}$ est la fonction $x\\mapsto -\\mathrm{e}^{-\\lambda x}$.',
        aide4: 'Ainsi $P(X&lt;t)=[-\\mathrm{e}^{-\\lambda x}]_0^t=-\\mathrm{e}^{-\\lambda t}-(-\\mathrm{e}^{-\\lambda \\times 0})=1-\\mathrm{e}^{-\\lambda t}$.',
        aide5: 'Donc $P(X>t)=P(X\\geq t)=1-P(X&lt;t)=\\mathrm{e}^{-\\lambda t}$.'
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      /*
    Phrase d’état renvoyée par la section
    Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
    Dans le cas où la section est qualitative, lister les différents pe renvoyées :
        pe_1: "toto"
        pe_2: "tata"
        etc
    Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
    Par exemple,
    parcours.pe: donneesSection.pe_2
    */
      pe: 0
    }
  }
  function enonceMain () {
    depart()
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    const nomVar = j3pRandomTab(['X', 'Y', 'Z', 'T'], [0.25, 0.25, 0.25, 0.25])
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, { v: nomVar })
    const signeIneg = j3pRandomTab(['<', '\\leq', '>', '\\geq'], [0.25, 0.25, 0.25, 0.25])
    let borne, lambda, valProba
    do {
      do {
        borne = j3pGetRandomInt(5, 40) / 10
      } while (Math.abs(borne - 1) < Math.pow(10, -12))
      do {
        lambda = j3pGetRandomInt(55, 150) / 100
      } while (Math.abs(lambda - 1) < 0.1)
      valProba = (['>', '\\geq'].indexOf(signeIneg) > -1) ? Math.round(100 * Math.exp(-lambda * borne)) / 100 : Math.round(100 * (1 - Math.exp(-lambda * borne))) / 100
    } while (Math.abs(valProba - 0.5) < 1e-12 || (valProba < 0.05) || (valProba > 0.95))
    let probaDonnee
    if (['>', '\\geq'].includes(signeIneg)) {
      probaDonnee = 'P(' + nomVar + signeIneg + ' ' + borne + ')=' + j3pVirgule(valProba)
    } else {
      probaDonnee = 'P(' + nomVar + signeIneg + ' ' + borne + ')=' + j3pVirgule(j3pArrondi(1 - valProba, 6))
    }
    j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, { p: probaDonnee })
    const elt = j3pAffiche(stor.zoneCons3, '', '$\\lambda=$&1&.', { inputmq1: { texte: '' } })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d.,-+^/*()', { commandes: ['fraction', 'ln', 'exp'] })// on n’autorise que les lettres majuscules
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [-Math.log(valProba) / borne]
    stor.param = {
      nomVar,
      signeIneg,
      borne,
      valProba,
      probaDonnee,
      trouverLambdaAvant: true
    }
    j3pStyle(stor.zoneCons4, { paddingTop: '10px' })
    j3pPaletteMathquill(stor.zoneCons4, stor.zoneInput, { liste: ['fraction', 'ln', 'exp', '(', ')'] })

    // Actualisation des valeurs dans mtg32
    stor.liste.giveFormula2('prb', String(valProba))
    stor.liste.giveFormula2('b', String(borne))
    stor.liste.calculateNG()
    // on Exporte les variables présentes dans cette section pour qu’elles soient utilisées dans une autre section
    me.donneesPersistantes.loiExpo = {}
    Object.entries(stor.param).forEach(([prop, value]) => {
      me.donneesPersistantes.loiExpo[prop] = value
    })
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    j3pFocus(stor.zoneInput)
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.premiereReponseNonSimplifiee = true
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        // Construction de la page
        me.construitStructurePage({ structure: ds.structure })
        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // chargement mtg
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        // il faut virer la partie démonstration
        j3pDetruitToutesLesFenetres()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = { aRepondu: false, bonneReponse: false }
        reponse.aRepondu = fctsValid.valideReponses()
        let repeleve
        if (reponse.aRepondu) {
          repeleve = j3pMathquillXcas($(stor.zoneInput).mathquill('latex'))
          // je peux avoir un soci si j’oublis les parenthèses dans ln(a)
          repeleve = j3pAjouteFoisDevantX(repeleve, 'x')
          let posLn = repeleve.indexOf('ln') + 3
          const RegNb = /[\d,.]/g // new RegExp('[0-9\\.\\,]{1}', 'g')
          let nbLn = ''
          while (RegNb.test(repeleve.charAt(posLn))) {
            nbLn += repeleve.charAt(posLn)
            posLn++
          }
          repeleve = repeleve.replace('\\ln', 'ln')
          me.logIfDebug('repeleve:', repeleve)
          repeleve = repeleve.replace('ln ' + nbLn, 'ln(' + nbLn + ')')
          me.logIfDebug('repeleve après modif:', repeleve)
          stor.liste.giveFormula2('proposition', String(repeleve))
          stor.liste.calculateNG()
          reponse.bonneReponse = (stor.liste.valueOf('egalite') === 1)
          fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          fctsValid.coloreUneZone(stor.zoneInput.id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // si l’élève donne bien la bonne réponse mais qu’elle n’est pas simplifiée,
              // on lui redonne un essai si c’est la première fois que cela se produit
              const laReponse = j3pCalculValeur(repeleve)
              const ordrePuissance = Math.floor(Math.log(Math.abs(laReponse)) / Math.log(10)) + 1
              const puisPrecision = 15 - ordrePuissance
              const reponseAttendue = -Math.log(stor.param.valProba) / stor.param.borne
              const reponseCorrecteNonSimplifiee = (Math.abs(reponseAttendue - laReponse) < Math.pow(10, -puisPrecision))
              if (reponseCorrecteNonSimplifiee) {
                if (stor.premiereReponseNonSimplifiee) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
                  stor.premiereReponseNonSimplifiee = false
                  me.essaiCourant--
                } else {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment3
                }
              } else {
                // on vérifie s’il n’a pas donné une valeur approchée
                const reponseApprochee = (Math.abs(reponseAttendue - laReponse) < Math.pow(10, -5))
                if (reponseApprochee) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
                }
              }

              if (me.essaiCourant < ds.nbchances) {
                if (!reponseCorrecteNonSimplifiee) {
                  stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                }
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        j3pElement('Mepsectioncontinuer').addEventListener('click', function () {
          j3pDetruitToutesLesFenetres()
        })
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
