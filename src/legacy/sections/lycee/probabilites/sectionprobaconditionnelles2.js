import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { remplaceCalcul } from 'src/legacy/outils/fonctions/gestionParametres'
import { construireArbreCond } from 'src/legacy/outils/arbreProba/arbreProba'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    mai 2016
    Dans cette section, on propose à l’élève de résoudre un exercice de probabilité conditionnelles moins classique
    Les données fournies ne permettent pas de construire l’arbre complet.
    On peut demander une première proba d’une intersection
    puis dans un seoncd temps (ou directement) la proba condtionnelle permettant de compléter entièrement l’arbre
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuestions', [1, 2, 3], 'array', 'Tableau qui contient les questions choisies (dans l’ordre croissant):<br>- 1 pour la probabilité de l’intersection simple à obtenir;<br>- 2 pour la probabilité de l’intersection obtenue à l’aide de la formule des probabilités totales;<br>-3 pour une probabilité conditionnelle à l’aide du précédent résultat.'],
    ['tabCasFigure', [''], 'array', 'Pour chaque répétition, on peut imposer, sous forme d’un tableau,  un des exercices particuliers présents dans le fichier texte annexe :<br>-1 : Centres étrangers juin 2015 S<br>-2 : Métropole juin 2016 ES<br>-3 : Pondichery avril 2016 ES<br>- 4 : Amérique du Nord juin 2015 ES'],
    ['ecrireProbaInter', false, 'boolean', 'Ceci vaut true si on écrit clairement la proba calculée sous la forme P(A inter B), false si c’est sous la forme d’une phrase'],
    ['ecrireProbaCond', false, 'boolean', 'Ceci vaut true si on écrit la probabilité conditionnelle à calculer sous la forme P_B(A), false sinon.'],
    ['avecSachant', true, 'boolean', 'Paramètre pour la probabilité conditionnelle. S’il vaut true, alors le mot "sachant" est écrit, sinon la question est tournée sans ce mot.'],
    ['seuilreussite', 0.8, 'reel', 'Pourcentage à partir duquel on considère que l’exercice est correctement traité.'],
    ['seuilErreur', 0.4, 'reel', 'Pourcentage à partir duquel on retient l’erreur pour la phrase d’état.']
    // paramètre pour la proba conditionnelle : avec ou sans le mot sachant
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'problème dans le calcul de la probabilité d’une intersection' },
    { pe_3: 'problème dans l’utilisation de la formule des probabilités totales' },
    { pe_4: 'problème dans le calcul d’une probabilité conditionnelle' },
    { pe_5: 'Insuffisant' }
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection (bonneReponse) {
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    function afficheArbre () {
      // construction de l’arbre
      // je dois créer l’objet qui contiendra toutes les caractéristiques de mon arbre
      stor.objArbreRep = {}
      // taille du texte evt (pour les proba, il faudrait qu’elles soient plus petites sans doute) + couleur du texte et des branches
      stor.objArbreRep.lestyle = { styletexte: me.styles.toutpetit.enonce, couleur: zoneExpli.style.color }
      // pour savoir si on écrit les évts et les proba (zones de saisie si false)
      stor.objArbreRep.evts1 = []
      stor.objArbreRep.evts2 = {}
      stor.objArbreRep.complet = { evts: true, proba: true }
      let laProba
      for (let i = 0; i < stor.evtsProba['enonce' + stor.numeroExo][stor.numInter['enonce' + stor.numeroExo]].length; i++) {
        laProba = remplaceCalcul({ text: stor.evtsProba['enonce' + stor.numeroExo][stor.numInter['enonce' + stor.numeroExo]][i][1], obj: stor.obj })
        stor.objArbreRep.evts1.push([stor.evtsProba['enonce' + stor.numeroExo][stor.numInter['enonce' + stor.numeroExo]][i][0], laProba])
        stor.objArbreRep.evts2['branche' + i] = []
        for (let j = 0; j < stor.evtsProba['enonce' + stor.numeroExo][stor.numInter['enonce' + stor.numeroExo]][i][2].length; j++) {
          laProba = remplaceCalcul({ text: stor.evtsProba['enonce' + stor.numeroExo][stor.numInter['enonce' + stor.numeroExo]][i][2][j][1], obj: stor.obj })
          stor.objArbreRep.evts2['branche' + i].push([stor.evtsProba['enonce' + stor.numeroExo][stor.numInter['enonce' + stor.numeroExo]][i][2][j][0], laProba])
        }
      }
      stor.objArbreRep.parcours = me
      construireArbreCond(stor.zoneArbre, stor.objArbreRep)
    }
    function correctionPrbTotale () {
      const zoneExpli3 = j3pAddElt(zoneExpli, 'div')
      const zoneExpli4 = j3pAddElt(zoneExpli, 'div')
      if (stor.questionIntersection) {
        j3pAffiche(zoneExpli3, '', stor.correctionInterBis['enonce' + stor.numeroExo][0], stor.obj)
        const consigneFin = (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 2) ? ds.textes.corr3 + stor.correctionInterBis['enonce' + stor.numeroExo][1] : ds.textes.corr4 + stor.correctionInterBis['enonce' + stor.numeroExo][1]
        j3pAffiche(zoneExpli4, '', consigneFin, stor.obj)
      } else {
        j3pAffiche(zoneExpli3, '', stor.correctionInter['enonce' + stor.numeroExo][0], stor.obj)
        const consigneFin = (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 2) ? ds.textes.corr3 + stor.correctionInter['enonce' + stor.numeroExo][1] : ds.textes.corr4 + stor.correctionInter['enonce' + stor.numeroExo][1]
        j3pAffiche(zoneExpli4, '', consigneFin, stor.obj)
      }
    }
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    const objVar = {}
    let num = 97
    for (let k2 = 0; k2 < stor.prbEnonce['enonce' + stor.numeroExo][stor.numInter['enonce' + stor.numeroExo]].length; k2++) {
      objVar[String.fromCharCode(num)] = remplaceCalcul({ text: stor.prbEnonce['enonce' + stor.numeroExo][stor.numInter['enonce' + stor.numeroExo]][k2], obj: stor.obj })
      num++
    }
    if (stor.questionIntersection || stor.questionIntersection2) {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1)
    } else {
      j3pAffiche(stor.zoneExpli1, '', stor.correctionProba['enonce' + stor.numeroExo][stor.numInter['enonce' + stor.numeroExo]], objVar)
    }
    afficheArbre()
    let phraseRep
    switch (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes]) {
      case 1:
        j3pAffiche(stor.zoneExpli2, '', ds.textes.corr3)
        j3pAffiche(stor.zoneExpli3, '', stor.correctionInterFacile['enonce' + stor.numeroExo], stor.obj)
        stor.questionIntersection = true
        break
      case 2:
        correctionPrbTotale()
        stor.questionIntersection2 = true
        break
      default :
        {
          if (!stor.questionIntersection2) {
          // il faut que j’ajoute l’explication sur la frmule des proba totales ici
            correctionPrbTotale()
          }
          phraseRep = (stor.questionIntersection2) ? stor.correctionPrbSachant['enonce' + stor.numeroExo] : ds.textes.corr3 + stor.correctionPrbSachant['enonce' + stor.numeroExo]
          stor.obj.r = remplaceCalcul({ text: stor.reponsePrbSachant['enonce' + stor.numeroExo], obj: stor.obj })
          const zoneExpliNew = j3pAddElt(zoneExpli, 'div')
          j3pAffiche(zoneExpliNew, '', phraseRep, stor.obj)
        }
        break
    }
    // Cette seconde version est pour sesaparcours (la classe css n’est aps la même)
    $('.mq-overline').each(function () {
      $(this).css('border-top', '1px solid ' + $(this).css('color'))
      // je parcours l’ensemble des overline et je leur donne la couleur dans laquelle se trouve le texte
    })
  }
  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 2,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      pe_1: 'Bien',
      pe_2: 'problème dans le calcul de la probabilité d’une intersection',
      pe_3: 'problème dans l’utilisation de la formule des probabilités totales',
      pe_4: 'problème dans le calcul d’une probabilité conditionnelle',
      pe_5: 'Insuffisant',
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      tabCasFigure: [],
      // probaInter: true
      typeQuestions: [1, 2, 3],
      afficheEvt: false,
      ecrireProbaInter: false,
      ecrireProbaCond: false,
      avecSachant: true,
      seuilErreur: 0.4,
      seuilreussite: 0.8,
      /*
          Les textes sont présents dans un fichier externe : sectionsAnnexes/Consignesexos/probaCond.js
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Probabilités conditionnelles (2)',
        // on donne les phrases de la consigne
        consigne1: 'On a montré que $£p=£v$.',
        consigne2: 'On a montré que $£p=£v$ et que $£q=£w$.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: '',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'Voici ci-contre l’arbre déjà obtenu :',
        corr2: 'Enfin : ',
        corr3: 'La probabilité cherchée vaut alors : ',
        corr4: 'Ainsi : '
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      pe: 0
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()

    // Construction de la page
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })

    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    import('../../../sectionsAnnexes/Consignesexos/probaCond2.js').then(function parseAnnexe ({ default: datasSection }) {
      let j; let i; let numInter
      const tabExercices = []
      stor.nb_sujets = datasSection.sujets.length
      stor.consignesInit = {}
      stor.consignesDonnees = {}
      stor.consignesFin = {}
      stor.nomVariables = {}
      stor.variables = {}
      stor.numInter = {}
      // probas données dans l’énoncé
      stor.prbEnonce = {}
      stor.correctionProba = {}

      // puis les objets qui seront les questions
      stor.evtsProba = {}
      // l’intersection calculable directement
      stor.questionInterFacile1 = {}
      stor.questionInterFacile = {}
      stor.questionInterFacileFin = {}
      stor.reponseInterFacile = {}
      stor.correctionInterFacile = {}

      // l’intersection pour laquelle on utilise la formule des proba totales
      stor.questionInter1 = {}
      stor.questionInter = {}
      stor.questionInterFin = {}
      stor.reponseInter = {}
      stor.correctionInter = {}
      stor.correctionInterBis = {}

      stor.questionPrbSachant = {}
      stor.questionPrbSachantFin = {}
      stor.questionPrbSachant1 = {}
      stor.reponsePrbSachant = {}
      stor.correctionPrbSachant = {}
      stor.arrondiCond = {}

      for (j = 0; j < stor.nb_sujets; j++) {
        tabExercices.push(j)
        stor.consignesInit['enonce' + j] = datasSection.sujets[j].consignesInit
        stor.consignesDonnees['enonce' + j] = datasSection.sujets[j].consignesDonnees
        stor.consignesFin['enonce' + j] = datasSection.sujets[j].consignesFin
        stor.nomVariables['enonce' + j] = []
        stor.variables['enonce' + j] = []
        for (i = 0; i < datasSection.sujets[j].variables.length; i++) {
          stor.nomVariables['enonce' + j][i] = datasSection.sujets[j].variables[i][0]
          const [intMin, intMax] = j3pGetBornesIntervalle(datasSection.sujets[j].variables[i][1])
          if (datasSection.sujets[j].variables[i].length === 2) {
            stor.variables['enonce' + j][i] = j3pGetRandomInt(intMin, intMax)
          } else {
            stor.variables['enonce' + j][i] = j3pGetRandomInt(intMin, intMax) / Number(datasSection.sujets[j].variables[i][2])
          }
        }
        // je récupère les événements et proba qui vont me permettre de construire l’arbre de proba
        stor.evtsProba['enonce' + j] = datasSection.sujets[j].evtsProba
        stor.prbEnonce['enonce' + j] = datasSection.sujets[j].prbEnonce
        stor.correctionProba['enonce' + j] = datasSection.sujets[j].correctionProba

        numInter = stor.numInter['enonce' + j] = j3pGetRandomInt(0, (datasSection.sujets[j].questionInter.length - 1))

        // on s’intéresse ici seulement à la proba d’une intersection
        // dans le cas où on n’écrit pas explicitement la proba
        stor.questionInterFacile1['enonce' + j] = datasSection.sujets[j].questionInterFacile1[numInter]
        // on pose une question, puis on écrit la proba cherchée
        stor.questionInterFacile['enonce' + j] = datasSection.sujets[j].questionInterFacile[numInter]
        stor.questionInterFacileFin['enonce' + j] = datasSection.sujets[j].questionInterFacileFin[numInter]
        // la réponse avec présence encore des variables (géré dans cette section au niveau de la mise en place de la réponse)
        stor.reponseInterFacile['enonce' + j] = datasSection.sujets[j].probaInterFacile[numInter]
        // correction pour la fonction afficheCorrection
        stor.correctionInterFacile['enonce' + j] = datasSection.sujets[j].correctionInterFacile[numInter]

        // on s’intéresse ici seulement à la proba d’une intersection
        // dans le cas où on n’écrit pas explicitement la proba
        stor.questionInter1['enonce' + j] = datasSection.sujets[j].questionInter1[numInter]
        // on pose une question, puis on écrit la proba cherchée
        stor.questionInter['enonce' + j] = datasSection.sujets[j].questionInter[numInter]
        stor.questionInterFin['enonce' + j] = datasSection.sujets[j].questionInterFin[numInter]
        // la réponse avec présence encore des variables (géré dans cette section au niveau de la mise en place de la réponse)
        stor.reponseInter['enonce' + j] = datasSection.sujets[j].probaInter[numInter]
        // correction pour la fonction afficheCorrection
        stor.correctionInter['enonce' + j] = datasSection.sujets[j].correctionInter[numInter]
        stor.correctionInterBis['enonce' + j] = datasSection.sujets[j].correctionInterBis[numInter]

        // pour la probabilité conditionnelle
        if (ds.avecSachant) {
          stor.questionPrbSachant['enonce' + j] = datasSection.sujets[j].questionPrbSachant[numInter]
          stor.questionPrbSachantFin['enonce' + j] = datasSection.sujets[j].questionPrbSachantFin[numInter]
          stor.questionPrbSachant1['enonce' + j] = datasSection.sujets[j].questionPrbSachant1[numInter]
        } else {
          stor.questionPrbSachant['enonce' + j] = datasSection.sujets[j].questionPrbCond[numInter]
          stor.questionPrbSachant1['enonce' + j] = datasSection.sujets[j].questionPrbCond1[numInter]
        }
        stor.questionPrbSachantFin['enonce' + j] = datasSection.sujets[j].questionPrbSachantFin[numInter]

        stor.reponsePrbSachant['enonce' + j] = datasSection.sujets[j].probaCond[numInter]
        stor.correctionPrbSachant['enonce' + j] = datasSection.sujets[j].correctionCond[numInter]
      }

      // on peut demander la proba de l’intersection puis la proba conditionnelle ou seulement la seconde
      // pour le nombre d’étapes, c’est en fait gérer par un tableau ds.typeQuestions
      // je vérifie que le tableau est bien construit (3 valeurs au max et dans l’ordre croissant)
      let index = 0
      while (index < ds.typeQuestions.length - 1) {
        if (ds.typeQuestions[index] < ds.typeQuestions[index + 1]) {
          index++
        } else {
          // la valeur en index+1 n’est pas correcte donc on la vire
          ds.typeQuestions.splice(index + 1, 1)
        }
      }
      if (ds.typeQuestions.length === 0) {
        ds.typeQuestions = [1, 2, 3]
      }
      ds.nbetapes = ds.typeQuestions.length
      ds.nbitems = ds.nbetapes * ds.nbrepetitions
      // enfin je gère le cas où on demanderait un cas de figure qui n’existe pas :
      index = 0
      while (index < ds.tabCasFigure.length) {
        ds.tabCasFigure[index] = Number(ds.tabCasFigure[index])
        if ((ds.tabCasFigure[index] > 0) && (ds.tabCasFigure[index] <= tabExercices.length)) {
          index++
        } else {
          // le numéro de question choisi n’existe pas
          ds.tabCasFigure.splice(index, 1)
        }
      }
      stor.numexo = 0 // c’est pour savoir à quelle répétition nous en sommes
      suite()
    }).catch(error => {
      console.error(error)
      j3pShowError('Impossible de charger les données de cette exercice')
    })
  }
  function suite () {
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      // c’est qu’on passe à un nouvel énoncé (éventuellement le premier)
      let choixExoImpose = (ds.tabCasFigure[stor.numexo] !== undefined)
      if (choixExoImpose) {
        // je regarde tout de même si le numéro existe bien
        if (ds.tabCasFigure[stor.numexo] > stor.nb_sujets) {
          choixExoImpose = false
        }
      }
      if (choixExoImpose) {
        // c’est que j’ai imposé l’exo à traiter
        stor.numeroExo = ds.tabCasFigure[stor.numexo] - 1
      } else {
        // je choisis au hasard un des exos à traiter (parmi ceux qui ne sont pas déjà choisis)
        let nbTentatives = 0 // c’est au cas où on aurait déjà proposé tous les cas de figure (vu le nombre d’exos possible qu’il risque d’y avoir, c’est peu probable)
        do {
          nbTentatives++
          stor.numeroExo = j3pGetRandomInt(0, (stor.nb_sujets - 1))
        } while ((ds.tabCasFigure.indexOf(stor.numeroExo + 1) > -1) && (nbTentatives < 30))
        ds.tabCasFigure[(me.questionCourante - 1) / ds.nbetapes] = stor.numeroExo + 1
      }
      me.logIfDebug('ds.tabCasFigure:', ds.tabCasFigure, '   me.Sectionprobaconditionnelles.numeroExo:', stor.numeroExo)

      stor.numexo++
      stor.phrasesEnonce = stor.consignesInit['enonce' + stor.numeroExo]
      // en fonction de la variable numInter, on donne les consignes adéquates
      stor.phrasesEnonce = stor.phrasesEnonce.concat(stor.consignesDonnees['enonce' + stor.numeroExo][stor.numInter['enonce' + stor.numeroExo]])
      stor.phrasesEnonce = stor.phrasesEnonce.concat(stor.consignesFin['enonce' + stor.numeroExo])
      stor.obj = {}
      for (let i = 0; i < stor.nomVariables['enonce' + stor.numeroExo].length; i++) {
        stor.obj[stor.nomVariables['enonce' + stor.numeroExo][i]] = stor.variables['enonce' + stor.numeroExo][i]
      }
      // je réinitialise mes variables pour la répétition suivante :
      stor.questionIntersection = false
      stor.questionIntersection2 = false
      if (me.isDebug) {
        for (let i = 0; i < stor.nomVariables.length; i++) {
          console.debug(`énoncé exo ${stor.numeroExo}[${i}]`, stor.nomVariables['enonce' + stor.numeroExo][i], ' = ', stor.variables['enonce' + stor.numeroExo][i])
        }
        console.debug('stor.prbEnonce:', stor.prbEnonce['enonce' + stor.numeroExo])
        console.debug('stor.correctionProba:', stor.correctionProba['enonce' + stor.numeroExo])
        console.debug('stor.evtsProba:', stor.evtsProba['enonce' + stor.numeroExo])
      }
    }
    // je choisis
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= stor.phrasesEnonce.length; i++) {
      const zoneCons = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(zoneCons, '', stor.phrasesEnonce[i - 1], stor.obj)
    }

    if (stor.questionIntersection) {
      stor.obj.p = stor.questionInterFacileFin['enonce' + stor.numeroExo]
      stor.obj.v = 'calcule[' + stor.reponseInterFacile['enonce' + stor.numeroExo] + ']'
      const zoneCons = j3pAddElt(stor.conteneur, 'div')
      if (stor.questionIntersection2) {
        stor.obj.q = stor.questionInterFin['enonce' + stor.numeroExo]
        stor.obj.w = 'calcule[' + stor.reponseInter['enonce' + stor.numeroExo] + ']'
        j3pAffiche(zoneCons, '', ds.textes.consigne2, stor.obj)
      } else {
        j3pAffiche(zoneCons, '', ds.textes.consigne1, stor.obj)
      }
    } else {
      if (stor.questionIntersection2) {
        const zoneCons = j3pAddElt(stor.conteneur, 'div')
        stor.obj.p = stor.questionInterFin['enonce' + stor.numeroExo]
        stor.obj.v = 'calcule[' + stor.reponseInter['enonce' + stor.numeroExo] + ']'
        j3pAffiche(zoneCons, '', ds.textes.consigne1, stor.obj)
      }
    }
    ;[1, 2].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })
    let elt
    switch (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes]) {
      case 1:
        if (ds.ecrireProbaInter) {
          j3pAffiche(stor.zoneCons1, '', stor.questionInterFacile['enonce' + stor.numeroExo])
          elt = j3pAffiche(stor.zoneCons2, '', '$' + stor.questionInterFacileFin['enonce' + stor.numeroExo] + '=$&1&',
            { inputmq1: { texte: '' } })
        } else {
          elt = j3pAffiche(stor.zoneCons1, '', stor.questionInterFacile1['enonce' + stor.numeroExo],
            { inputmq1: { texte: '' } })
        }
        stor.zoneInput = elt.inputmqList[0]
        stor.zoneInput.typeReponse = ['nombre', 'exact']
        stor.zoneInput.reponse = [j3pCalculValeur(remplaceCalcul({ text: stor.reponseInterFacile['enonce' + stor.numeroExo], obj: stor.obj }))]
        break
      case 2: // on demande la proba d’une intersection obtenue à l’aide de la formule des proba totales
        if (ds.ecrireProbaInter) {
          j3pAffiche(stor.zoneCons1, '', stor.questionInter['enonce' + stor.numeroExo], {})
          elt = j3pAffiche(stor.zoneCons2, '', '$' + stor.questionInterFin['enonce' + stor.numeroExo] + '=$&1&',
            { inputmq1: { texte: '' } })
        } else {
          elt = j3pAffiche(stor.zoneCons1, '', stor.questionInter1['enonce' + stor.numeroExo],
            { inputmq1: { texte: '' } })
        }
        stor.zoneInput = elt.inputmqList[0]
        stor.zoneInput.typeReponse = ['nombre', 'exact']
        stor.zoneInput.reponse = [j3pCalculValeur(remplaceCalcul({ text: stor.reponseInter['enonce' + stor.numeroExo], obj: stor.obj }))]
        break
      default :
        // on demande une proba conditionnelle utilisant le précédent résultat
        if (ds.ecrireProbaCond) {
          j3pAffiche(stor.zoneCons1, '', stor.questionPrbSachant['enonce' + stor.numeroExo], {})
          elt = j3pAffiche(stor.zoneCons2, '', stor.questionPrbSachantFin['enonce' + stor.numeroExo] + '$=$&1&',
            { inputmq1: { texte: '' } })
        } else {
          elt = j3pAffiche(stor.zoneCons1, '', stor.questionPrbSachant1['enonce' + stor.numeroExo],
            { inputmq1: { texte: '' } })
        }
        stor.zoneInput = elt.inputmqList[0]
        stor.zoneInput.typeReponse = ['nombre', 'exact']
        stor.zoneInput.reponse = [j3pCalculValeur(remplaceCalcul({ text: stor.reponsePrbSachant['enonce' + stor.numeroExo], obj: stor.obj }))]

        break
    }
    mqRestriction(stor.zoneInput, '\\d,.-')
    me.logIfDebug('proba cond :', stor.zoneInput.reponse)

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    j3pFocus(stor.zoneInput)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]//, left: 600, top: 10 }]
    j3pCreeFenetres(me)
    stor.zoneCalc = j3pAddElt(stor.zoneD, 'div')
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    stor.zoneArbre = j3pAddElt(stor.zoneD, 'div')
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div')
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite()
      }
      break // case "enonce":

    case 'correction':
      {
      // On teste si une réponse a été saisie
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si la zone a bien été complétée ou non
        // reponse.bonneReponse qui me dit si la zones a une réponse correcte ou non.
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // indication éventuelle ici
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++

              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                // je récupère le nombre de faute de chaque sorte (intersection [4], proba cond [5])
                me.typederreurs[3 + (me.questionCourante - 1) % ds.nbetapes + 1]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          if (me.typederreurs[4] / ds.nbrepetitions >= ds.seuilErreur) {
            // pb sur la proba de l’intersection
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[5] / ds.nbrepetitions >= ds.seuilErreur) {
            // pb sur la formule des proba totales
            me.parcours.pe = ds.pe_3
          } else if (me.typederreurs[6] / ds.nbrepetitions >= ds.seuilErreur) {
            // pb sur la proba conditionnelle
            me.parcours.pe = ds.pe_4
          } else {
            // autre souci mais non identifié
            me.parcours.pe = ds.pe_5
          }
        }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
