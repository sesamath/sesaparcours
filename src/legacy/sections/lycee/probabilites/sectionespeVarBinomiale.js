import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pEmpty, j3pFocus, j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomInt, j3pNombre, j3pPaletteMathquill, j3pPGCD, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import $ from 'jquery'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexProduit } from 'src/legacy/core/functionsLatex'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since mars 2022
 * @fileOverview Cette section demande la valeur de l’espérance et l’écart-type d’une va suivant une loi binomiale
 */

/**
 * Les paramètres de la section, anciennement j3p.SectionXxx = {}
 */
export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}
const textes = {
  titre_exo: 'Espérance et variance d’une loi binomiale',
  consigne1: 'On considère une variable aléatoire £X qui suit la loi binomiale de paramètres $n=£n$ et $p=£p$.',
  consigne2: 'L’espérance de cette variable aléatoire vaut $E(£X)=$&1&.',
  consigne3: 'Son écart-type vaut $\\sigma(£X)\\simeq$&1&.',
  info: 'On donnera la valeur exacte de l’espérance et un arrondi au millième de l’écart-type.',
  comment1: 'N’y aurait-il pas un problème d’arrondi dans la deuxième réponse.',
  corr1: 'D’après le cours, l’espérance d’une loi binomiale de paramètres $n$ et $p$ vaut $np$ et sa variance $np(1-p)$, l’écart-type étant la racine carrée de la variance.',
  corr2: 'Donc l’espérance de cette loi vaut $E(£X)=£e$ et son écart-type $\\sigma(£X)=\\sqrt{£n\\times £p\\left(1-£p\\right)}\\simeq £s$.'
}
/**
 * section espeVarBinomiale
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour chaque sujet
 * @private
 * @param {Parcours} me
 * @param {Number} numSujet
 * @return {Object}
 */
function genereAlea () {
  const obj = {}
  obj.n = 5 * j3pGetRandomInt(4, 10)
  const choixp = j3pGetRandomBool() // Pour savoir si on le présente au format décimal ou sous forme fractionnaire
  if (choixp) {
    obj.pval = j3pGetRandomInt(20, 80) / 100
    obj.p = j3pVirgule(obj.pval)
    obj.e = obj.n + '\\times ' + j3pVirgule(obj.p) + '=' + j3pVirgule(obj.n * obj.pval)
  } else {
    let pb = false
    let num, den
    do {
      num = j3pGetRandomInt(2, 5)
      den = num + j3pGetRandomInt(1, 4)
      pb = j3pPGCD(num, den) !== 1
      if (!pb) {
        // je ne veux pas que sigma puisse être une valeur décimale exacte
        const sigma = Math.sqrt(obj.n * num * (den - num) / (den * den))
        pb = Math.abs(sigma * 10000 - Math.round(sigma * 10000)) < Math.pow(10, -8)
      }
    } while (pb)
    obj.p = '\\frac{' + num + '}{' + den + '}'
    obj.pval = num / den
    const esperance = j3pGetLatexProduit(obj.n, obj.p)
    obj.e = obj.n + '\\times ' + obj.p + '=' + esperance
  }
  obj.espe = obj.n * obj.pval
  obj.sigma = Math.sqrt(obj.n * obj.pval * (1 - obj.pval))
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  // On surcharge avec les parametres passés par le graphe
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  function ecoute (laZone) {
    if (laZone.className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.laPalette)
      if (laZone.avecFraction) j3pPaletteMathquill(stor.laPalette, laZone, { liste: ['fraction'] })
    }
  }
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
  me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]
  j3pCreeFenetres(me)
  j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
  j3pAfficheCroixFenetres('Calculatrice')
  stor.objDonnees = genereAlea()
  // on affiche l’énoncé
  for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
  stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
  // Choix aléatoire du nombre de la va
  const tabVa = ['X', 'Y', 'Z']
  stor.objDonnees.X = j3pGetRandomElt(tabVa)
  j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objDonnees)
  const elt1 = j3pAffiche(stor.zoneCons2, '', textes.consigne2, {
    X: stor.objDonnees.X,
    inputmq1: { texte: '' }
  })
  const elt2 = j3pAffiche(stor.zoneCons3, '', textes.consigne3, {
    X: stor.objDonnees.X,
    inputmq1: { texte: '' }
  })
  stor.zoneInput = [elt1.inputmqList[0], elt2.inputmqList[0]]
  stor.zoneInput[0].typeReponse = ['nombre', 'exact']
  stor.nbDecimales = 3
  stor.zoneInput[1].typeReponse = ['nombre', 'arrondi', Math.pow(10, -stor.nbDecimales)]
  stor.zoneInput[0].reponse = [stor.objDonnees.espe]
  stor.zoneInput[1].reponse = [stor.objDonnees.sigma]
  stor.objDonnees.s = Math.round(stor.objDonnees.sigma * Math.pow(10, stor.nbDecimales)) / Math.pow(10, stor.nbDecimales)
  j3pStyle(stor.zoneCons4, { fontStyle: 'italic', fontSize: '0.9em' })
  j3pAddContent(stor.zoneCons4, textes.info)
  stor.zoneInput[0].avecFraction = true
  stor.zoneInput[1].avecFraction = false
  mqRestriction(stor.zoneInput[0], '\\d,./-', { commandes: ['fraction'] })
  mqRestriction(stor.zoneInput[1], '\\d,.-')
  stor.zoneInput.forEach(eltInput => $(eltInput).focusin(function () { ecoute(this) }))
  ecoute(stor.zoneInput[0])
  const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
  stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
  j3pFocus(stor.zoneInput[0])
  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  j3pEmpty(stor.laPalette)
  j3pEmpty(stor.zoneCalc)
  j3pDetruitFenetres('Calculatrice')
  // on affiche la correction dans la zone xxx
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
  const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
  j3pAffiche(zoneExpli1, '', textes.corr1, stor.objDonnees)
  j3pAffiche(zoneExpli2, '', textes.corr2, stor.objDonnees)
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  stor.pbArrondi = false
  if (reponse.aRepondu) {
    // on vérifie si dans la deuxième question ce n’est aps un pb d’arrondi
    if (!stor.fctsValid.zones.bonneReponse[1]) {
      stor.pbArrondi = (Math.abs(stor.zoneInput[1].reponse[0] - j3pNombre(stor.fctsValid.zones.reponseSaisie[1])) < Math.pow(10, -(stor.nbDecimales - 1)))
    }
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
    if (stor.pbArrondi) {
      j3pAddElt(stor.divCorrection, 'br')
      me.reponseKo(stor.divCorrection, textes.comment1, true)
    }
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
