import { j3pAddElt, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import tabDoubleEntree from 'src/legacy/outils/tableauxProba/tabDoubleEntree'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// Voir STMG Polynésie septembre 2014

/*
        DENIAUd Rémi
        janvier 2020
        Dans cette section l’élève doit compléter un tableau à double entrée
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition']
  ]

}
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection () {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse)
    j3pDetruit(stor.zoneCons4)
    for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
      if (!stor.fctsValid.zones.bonneReponse[i]) {
        const zoneExpli = j3pAddElt(stor.divConteneur[i], 'div')
        zoneExpli.style.color = me.styles.toutpetit.correction.color
        j3pAffiche(zoneExpli, '', '$' + stor.zoneInput[i].reponse[0] + '$')
      }
    }
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Tableau à double entrée',
        // on donne les phrases de la consigne
        // Bac STMG Polynésie septembre 2014
        consigne1_1: 'Une classe de terminale contient £n élèves. Parmi les £g garçons de la classe, £q ne sont pas des redoublants. Il y a par ailleurs £r filles redoublantes.',
        consigne1_2: 'On achète une boîte de £n dragées bleues ou roses pouvant contenir une amande ou une noisette. £b dragées sont bleues et parmi elles, £a contiennent une amande. On trouve par ailleurs dans la boîte £r dragées roses contenant une noisette.',
        // Inspirée de bac ACC Nouvelle Calédonie 2002
        consigne1_3: 'Une étudiante confectionne des colliers, boucles d’oreilles ou bracelets fantaisie, dorés ou argentés. Sur les £m modèles confectionnés au cours d’un mois, £b sont des boucles d’oreilles dont £o sont dorées et £c sont des colliers dont £a sont argentés. Elle a par ailleurs fabriqué £e bracelets dorés.',
        // Polynésie septembre 2004 ACC
        consigne1_4: 'Un horloger bijoutier possède £m montres dans son magasin. Les montres sont de deux types : des montres de type A (à affichage analogique) et des montres de type N (à affichage numérique).<br/>Certaines de ces montres ont un bracelet métal et les autres un bracelet plastique. On compte £a montres de type A, £p montres avec un bracelet plastique dont £n sont de type N.',
        // Polynésie juin 2005 ACC
        consigne1_5: 'Un disquaire propose dans un de ses rayons un choix entre £d disques de catégories Rap, Soul ou Métal. Certains sont en langue française, les autres en langue anglaise.<br/>Sur les £f disques en langue française, £r sont dans la catégorie Rap et £m dans la catégorie Métal.<br/>£s disques sont de la Soul en langue anglaise et dans la catégorie Métal, il y a £p disques de plus en langue française qu’en langue anglaise.',
        consigne2: 'Compléter le tableau à double entrée ci-dessous',
        ligne1: 'Garçons|Filles',
        colonne1: 'Redoublants|Non&nbsp;redoublants',
        ligne2: 'Bleues|Roses',
        colonne2: 'Noisette|Amande',
        ligne3: 'Dorés|Argentés',
        colonne3: 'Colliers|Boucles&nbsp;d’oreilles|Bracelets',
        ligne4: 'Métal|Plastique',
        colonne4: 'Type&nbsp;A|Type&nbsp;N',
        ligne5: 'Métal|Soul|Rap',
        colonne5: 'Langue française|Langue anglaise',
        total: 'Total',
        chances: "Attention, tu n’as le droit qu'à une tentative pour répondre&nbsp;!",
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: '',
        // et les phrases utiles pour les explications de la réponse
        corr1: ''
      },
      pe: 0
    }
  }

  function enonceMain () {
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    stor.quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.objCons = {}
    stor.objCons.styletexte = {}
    switch (stor.quest) {
      case 1:
        stor.objCons.n = j3pGetRandomInt(31, 35)
        stor.objCons.r = j3pGetRandomInt(2, 4)
        stor.objCons.g = j3pGetRandomInt(12, 26)
        stor.objCons.q = stor.objCons.g - j3pGetRandomInt(1, stor.objCons.r - 1)
        stor.effectifsCorr = [
          [stor.objCons.g - stor.objCons.q, stor.objCons.r, stor.objCons.g - stor.objCons.q + stor.objCons.r],
          [stor.objCons.q, stor.objCons.n - stor.objCons.g - stor.objCons.r, stor.objCons.n - stor.objCons.g + stor.objCons.q - stor.objCons.r],
          [stor.objCons.g, stor.objCons.n - stor.objCons.g, stor.objCons.n]]
        break
      case 2:
        stor.objCons.n = 10 * j3pGetRandomInt(21, 28)
        stor.objCons.b = 5 * j3pGetRandomInt(21, 28)
        stor.objCons.a = Math.round(stor.objCons.b / 2) + j3pGetRandomInt(15, 28)
        stor.objCons.r = stor.objCons.n - stor.objCons.b - j3pGetRandomInt(15, 28)
        stor.effectifsCorr = [
          [stor.objCons.b - stor.objCons.a, stor.objCons.r, stor.objCons.b - stor.objCons.a + stor.objCons.r],
          [stor.objCons.a, stor.objCons.n - stor.objCons.b - stor.objCons.r, stor.objCons.n - stor.objCons.b + stor.objCons.a - stor.objCons.r],
          [stor.objCons.b, stor.objCons.n - stor.objCons.b, stor.objCons.n]]
        break
      case 3:
        stor.objCons.m = j3pGetRandomInt(120, 150)
        stor.objCons.b = j3pGetRandomInt(31, 42)
        stor.objCons.c = j3pGetRandomInt(43, 55)
        stor.objCons.a = Math.round(stor.objCons.c / 2) + j3pGetRandomInt(3, 6)
        stor.objCons.o = Math.round(stor.objCons.b / 2) + j3pGetRandomInt(6, 10)
        stor.objCons.e = stor.objCons.m - stor.objCons.c - stor.objCons.b - j3pGetRandomInt(15, 28)
        stor.effectifsCorr = [
          [stor.objCons.c - stor.objCons.a, stor.objCons.a, stor.objCons.c],
          [stor.objCons.o, stor.objCons.b - stor.objCons.o, stor.objCons.b],
          [stor.objCons.e, stor.objCons.m - stor.objCons.c - stor.objCons.b - stor.objCons.e, stor.objCons.m - stor.objCons.c - stor.objCons.b],
          [stor.objCons.c - stor.objCons.a + stor.objCons.o + stor.objCons.e, stor.objCons.m - stor.objCons.c + stor.objCons.a - stor.objCons.o - stor.objCons.e, stor.objCons.m]]
        break
      case 4:
        stor.objCons.m = j3pGetRandomInt(110, 135)
        stor.objCons.a = Math.round(stor.objCons.m / 2) + (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(5, 10)
        stor.objCons.p = j3pGetRandomInt(30, 45)
        do {
          stor.objCons.n = Math.round(stor.objCons.p / 2) - j3pGetRandomInt(3, 7)
        } while (stor.objCons.n >= stor.objCons.m - stor.objCons.a - 2)
        stor.effectifsCorr = [
          [stor.objCons.a - stor.objCons.p + stor.objCons.n, stor.objCons.p - stor.objCons.n, stor.objCons.a],
          [stor.objCons.m - stor.objCons.a - stor.objCons.n, stor.objCons.n, stor.objCons.m - stor.objCons.a],
          [stor.objCons.m - stor.objCons.p, stor.objCons.p, stor.objCons.m]]
        break
      case 5:
        // Un disquaire propose dans un de ses rayons un choix entre £d disques de catégories Rap, Soul ou Métal. Certains sont en langue française, les autres en langue anglaise.<br/>Sur les £f disques en langues française, £r sont dans la catégorie Rap et £m dans la catégorie Métal.<br/>£s disques sont de la Soul en langue anglaise et dans la catégorie Métal, il y a £p disques en plus en langue française qu’en langue anglaise.
        stor.objCons.d = 10 * j3pGetRandomInt(130, 140)
        stor.objCons.f = 10 * j3pGetRandomInt(50, 64)
        stor.objCons.r = 10 * j3pGetRandomInt(20, 24)
        stor.objCons.m = 10 * j3pGetRandomInt(12, 17)
        stor.objCons.s = 10 * j3pGetRandomInt(40, 45)
        stor.objCons.p = 5 * j3pGetRandomInt(4, 7)
        stor.effectifsCorr = [
          [stor.objCons.m, stor.objCons.f - stor.objCons.m - stor.objCons.r, stor.objCons.r, stor.objCons.f],
          [stor.objCons.m - stor.objCons.p, stor.objCons.s, stor.objCons.d - stor.objCons.f - stor.objCons.m + stor.objCons.p - stor.objCons.s, stor.objCons.d - stor.objCons.f],
          [2 * stor.objCons.m - stor.objCons.p, stor.objCons.f - stor.objCons.m - stor.objCons.r + stor.objCons.s, stor.objCons.d - stor.objCons.m + stor.objCons.p - stor.objCons.f + stor.objCons.r - stor.objCons.s, stor.objCons.d]]
        break
    }
    stor.effectifs = []
    let j
    for (let i = 0; i < stor.effectifsCorr.length; i++) {
      stor.effectifs.push([])
      for (j = 0; j < stor.effectifsCorr[i].length; j++) {
        const ajout = (i === stor.effectifsCorr.length - 1 && j === stor.effectifsCorr[i].length - 1) ? stor.effectifsCorr[i][j] : ''
        stor.effectifs[i].push(ajout)
      }
    }
    j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + stor.quest], stor.objCons)
    j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2)
    const objTab = {}
    objTab.largeurTab = me.zonesElts.MG.getBoundingClientRect().width * stor.pourc / 100
    objTab.lestyle = { styletexte: me.styles.toutpetit.enonce, couleur: me.styles.toutpetit.enonce.color }
    objTab.nomsLigne = ds.textes['ligne' + stor.quest].split('|')
    objTab.nomsLigne.push(ds.textes.total)
    objTab.nomsColonne = ds.textes['colonne' + stor.quest].split('|')
    objTab.nomsColonne.push(ds.textes.total)
    objTab.effectifs = {}
    objTab.effectifsCorr = [...stor.effectifsCorr]
    for (j = 0; j < stor.effectifsCorr.length; j++) {
      objTab.effectifs['ligne' + (j + 1)] = [...stor.effectifs[j]]
    }

    // stor.objZones = construireTableau(stor.zoneCons3, objTab)
    const tabZones = tabDoubleEntree(stor.zoneCons3, objTab)
    stor.zoneInput = []
    stor.divConteneur = []
    tabZones.forEach((ligne, k) => {
      ligne.forEach((zone, k2) => {
        if (zone !== '') {
          // La cellule possède un id, c’est qu’elle doit recevoir une zone de saisie
          const elt = j3pAffiche(zone, '', '&1&')
          mqRestriction(elt.inputmqList[0], '\\d-')
          stor.divConteneur.push(zone)
          stor.zoneInput.push(elt.inputmqList[0])
          elt.inputmqList[0].typeReponse = ['nombre', 'exact']
          elt.inputmqList[0].reponse = [objTab.effectifsCorr[k][k2]]
        }
      })
    })
    if (ds.nbchances === 1) {
      j3pAffiche(stor.zoneCons4, '', ds.textes.chances)
      j3pStyle(stor.zoneCons4, { fontSize: '0.9em', fontStyle: 'italic' })
    }
    j3pFocus(stor.zoneInput[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    stor.fctsValid = new ValidationZones({ parcours: me, zones: stor.zoneInput.map(elt => elt.id) })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70

        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc / 100 })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        stor.tabQuest = [1, 2, 3, 4, 5]
        stor.tabQuestInit = [...stor.tabQuest]
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection()
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection()
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
