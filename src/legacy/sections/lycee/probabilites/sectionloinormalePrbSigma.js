import { j3pArrondi, j3pNombre, j3pFocus, j3pShuffle, j3pGetBornesIntervalle, j3pGetRandomInt, j3pRandomTab, j3pVirgule, j3pAddElt, j3pStyle, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseManquante, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    juin 2017
    Dans cette section, on fournit une probabilité qui permet d’identifier facilement sigma qui était inconnu
    On demande alors la probabilité d’un autre événement
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

/**
 * section loinormalePrbSigma
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    const probaCours = (stor.typeIntervalle === 1) ? 'P(\\mu-\\sigma\\leq X\\leq \\mu+\\sigma)\\approx 0,683' : (stor.typeIntervalle === 2) ? 'P(\\mu-2\\sigma\\leq X\\leq \\mu+2\\sigma)\\approx 0,954' : 'P(\\mu-3\\sigma\\leq X\\leq \\mu+3\\sigma)\\approx 0,997'
    const egalite = (stor.typeIntervalle === 1) ? '\\mu+\\sigma=' + j3pArrondi(stor.mu + stor.sigma, 4) : (stor.typeIntervalle === 2) ? '\\mu+2\\sigma=' + j3pArrondi(stor.mu + 2 * stor.sigma, 4) : '\\mu+3\\sigma=' + j3pArrondi(stor.mu + 3 * stor.sigma, 4)
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1,
      {
        p: stor.probaDonnee,
        q: probaCours,
        e: egalite,
        s: stor.sigma,
        i: stor.intervalle,
        m: stor.mu
      })
    if ((stor.signeIneg === '\\leq') || (stor.signeIneg === '<')) {
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2_1,
        {
          m: stor.mu,
          s: stor.sigma,
          k: stor.k
        })
    } else {
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2_2,
        {
          m: stor.mu,
          s: stor.sigma,
          k: stor.k
        })
    }
    j3pAffiche(stor.zoneExpli3, '', ds.textes.corr3,
      {
        k: stor.k,
        s: stor.signeIneg,
        r: j3pArrondi(stor.zoneInput.reponse[0], 3)
      })

    // Je modifie la figure pour que la correction apparaisse
    modifFig({ correction: true })
    // on rend maintenant inactive la figure mtg32 (on ne peut plus déplacer les points)
    stor.mtgAppLecteur.setActive(stor.mtg32svg, false)
  }

  function depart (callback) {
    stor.mtgAppLecteur.removeAllDoc()
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH2+v4BAP8BAAAAAAAAAAACJAAAAOkAAAAAAAAAAAAAAAAAAABs#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEAD0NWYXJpYWJsZUJvcm5lZQD#####AAVzaWdtYT#wAAAAAAAAP+AAAAAAAABACAAAAAAAAD+5mZmZmZmaAAADMC41AAEzAAMwLjEAAAACAP####8AAm11AAAAAAAAAADAAAAAAAAAAEAAAAAAAAAAP8mZmZmZmZoAAAItMgABMgADMC4y#####wAAAAEAB0NDYWxjdWwA#####wACYTYABC0wLjX#####AAAAAQAMQ01vaW5zVW5haXJlAAAAAT#gAAAAAAAAAAAAAwD#####AAJhNQACLTEAAAAEAAAAAT#wAAAAAAAA#####wAAAAEACkNQb2ludEJhc2UA#####wEAAAAAEAABTwDALgAAAAAAAEAQAAAAAAAABQABQHCQAAAAAABAZcAAAAAAAP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AAAAAAAPAAABAAEAAAAFAT#qqqqqqqqr#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAEQAAFJAMAQAAAAAAAAQBAAAAAAAAAFAAFAV4AAAAAAAAAAAAb#####AAAAAQAJQ0Ryb2l0ZUFCAP####8AAAAAABAAAAEAAQAAAAUAAAAH#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAADwAAAQABAAAABQAAAAYAAAAHAP####8BAAAAARAAAUoAwCYAAAAAAAAAAAAAAAAAAAUAAcB1QAAAAAAAAAAACQAAAAgA#####wEAAAAAEAAAAQABAAAABQAAAAr#####AAAAAgAHQ1JlcGVyZQD#####AICAgAEBAAAABQAAAAcAAAAKAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACUNMb25ndWV1cgD#####AAAABQAAAAcAAAADAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAAAwD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABdHcmFkdWF0aW9uQXhlc1JlcGVyZU5ldwAAABsAAAAIAAAAAwAAAAwAAAAOAAAAD#####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAABAABWFic29yAAAADP####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAABAABW9yZG9yAAAADP####8AAAABAApDVW5pdGV4UmVwAAAAABAABnVuaXRleAAAAAz#####AAAAAQAKQ1VuaXRleVJlcAAAAAAQAAZ1bml0ZXkAAAAM#####wAAAAEAEENQb2ludERhbnNSZXBlcmUAAAAAEAAAAAAADgAAAQUAAAAADP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAARAAAAEgAAABIAAAARAAAAABAAAAAAAA4AAAEFAAAAAAz#####AAAAAQAKQ09wZXJhdGlvbgAAAAASAAAAEQAAABIAAAATAAAAEgAAABIAAAARAAAAABAAAAAAAA4AAAEFAAAAAAwAAAASAAAAEQAAABMAAAAAEgAAABIAAAASAAAAFP####8AAAABAAtDSG9tb3RoZXRpZQAAAAAQAAAAFQAAABIAAAAO#####wAAAAEAC0NQb2ludEltYWdlAAAAABAAAAAAAA4AAAEFAAAAABYAAAAYAAAAFAAAAAAQAAAAFQAAABIAAAAPAAAAFQAAAAAQAAAAAAAOAAABBQAAAAAXAAAAGv####8AAAABAAhDU2VnbWVudAAAAAAQAQAAAAANAAABAAEAAAAWAAAAGQAAABYAAAAAEAEAAAAADQAAAQABAAAAFwAAABsAAAAHAAAAABABAAAAAAsAAVcAwBQAAAAAAADANAAAAAAAAAUAAT#cVniavN8OAAAAHP####8AAAACAAhDTWVzdXJlWAAAAAAQAAd4Q29vcmQxAAAADAAAAB4AAAADAAAAABAABWFic3cxAAd4Q29vcmQxAAAAEgAAAB######AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAABABZmZmAAAAHgAAABIAAAAOAAAAHgAAAAIAAAAeAAAAHgAAAAMAAAAAEAAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAATAQAAABMCAAAAAUAAAAAAAAAAAAAAEgAAABEAAAASAAAAIAAAABEAAAAAEAEAAAAACwAAAQUAAAAADAAAABIAAAAiAAAAEgAAABIAAAAYAQAAABABZmZmAAAAIwAAABIAAAAOAAAAHgAAAAUAAAAeAAAAHwAAACAAAAAiAAAAIwAAAAcAAAAAEAEAAAAACwABUgBAIAAAAAAAAMAgAAAAAAAABQABP9EbToG06B8AAAAd#####wAAAAIACENNZXN1cmVZAAAAABAAB3lDb29yZDEAAAAMAAAAJQAAAAMAAAAAEAAFb3JkcjEAB3lDb29yZDEAAAASAAAAJgAAABgBAAAAEAFmZmYAAAAlAAAAEgAAAA8AAAAlAAAAAgAAACUAAAAlAAAAAwAAAAAQAAVvcmRyMgANMipvcmRvci1vcmRyMQAAABMBAAAAEwIAAAABQAAAAAAAAAAAAAASAAAAEgAAABIAAAAnAAAAEQAAAAAQAQAAAAALAAABBQAAAAAMAAAAEgAAABEAAAASAAAAKQAAABgBAAAAEAFmZmYAAAAqAAAAEgAAAA8AAAAlAAAABQAAACUAAAAmAAAAJwAAACkAAAAq#####wAAAAIADENDb21tZW50YWlyZQAAAAAQAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAHgsAAf###wAAAAEAAAAAAAsjVmFsKGFic3cxKQAAABgBAAAAEAFmZmYAAAAsAAAAEgAAAA4AAAAeAAAABAAAAB4AAAAfAAAAIAAAACwAAAAaAAAAABABZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAjCwAB####AAAAAQAAAAAACyNWYWwoYWJzdzIpAAAAGAEAAAAQAWZmZgAAAC4AAAASAAAADgAAAB4AAAAGAAAAHgAAAB8AAAAgAAAAIgAAACMAAAAuAAAAGgAAAAAQAWZmZgDAIAAAAAAAAD#wAAAAAAAAAAAAJQsAAf###wAAAAIAAAABAAsjVmFsKG9yZHIxKQAAABgBAAAAEAFmZmYAAAAwAAAAEgAAAA8AAAAlAAAABAAAACUAAAAmAAAAJwAAADAAAAAaAAAAABABZmZmAMAcAAAAAAAAAAAAAAAAAAAAAAAqCwAB####AAAAAgAAAAEACyNWYWwob3JkcjIpAAAAGAEAAAAQAWZmZgAAADIAAAASAAAADwAAACUAAAAGAAAAJQAAACYAAAAnAAAAKQAAACoAAAAy#####wAAAAEABUNGb25jAP####8AAWYALzEvKHNpZ21hKnNxcnQoMipwaSkpKmV4cCgtMC41KigoeC1tdSkvc2lnbWEpXjIpAAAAEwIAAAATAwAAAAE#8AAAAAAAAAAAABMCAAAAEgAAAAH#####AAAAAgAJQ0ZvbmN0aW9uAQAAABMCAAAAAUAAAAAAAAAAAAAAEgAAAAAAAAAcBwAAAAQAAAATAgAAAAE#4AAAAAAAAP####8AAAABAApDUHVpc3NhbmNlAAAAEwMAAAATAf####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAASAAAAAgAAABIAAAABAAAAAUAAAAAAAAAAAAF4AAAABwD#####AQAAAAAPAAJ4MQEFAAG#84WEI12cTwAAAAgAAAAXAP####8AB3hDb29yZDEAAAAMAAAANQAAAAMA#####wACeDEAB3hDb29yZDEAAAASAAAANgAAAAMA#####wACeTEABWYoeDEp#####wAAAAEADkNBcHBlbEZvbmN0aW9uAAAANAAAABIAAAA3AAAAEQD#####AQAAAAAPAAABBQAAAAAMAAAAEgAAADcAAAASAAAAOP####8AAAACAA1DTGlldURlUG9pbnRzAP####8AAAAAAAEAAAA5AAAB9AABAAAANQAAAAUAAAA1AAAANgAAADcAAAA4AAAAOQAAAAMA#####wABYQAELTAuMgAAAAQAAAABP8mZmZmZmZoAAAADAP####8AAWIAAzEuNQAAAAE#+AAAAAAAAAAAABEA#####wAAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAOwAAAAEAAAAAAAAAAAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAOwAAAB8AAAA0AAAAEgAAADsAAAARAP####8AAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEgAAADwAAAABAAAAAAAAAAAAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEgAAADwAAAAfAAAANAAAABIAAAA8AAAAFgD#####AAAAAAANAAABAAEAAAA9AAAAPwAAAAcA#####wEAAAAADwACeDIBBQABP9EvGjNvbeQAAABBAAAAFwD#####AAd4Q29vcmQyAAAADAAAAEIAAAADAP####8AAngyAAd4Q29vcmQyAAAAEgAAAEMAAAADAP####8AAnkyAAVmKHgyKQAAAB8AAAA0AAAAEgAAAEQAAAARAP####8BAAAAAA8AAAEFAAAAAAwAAAASAAAARAAAABIAAABFAAAAIAD#####AAAAAAABAAAARgAAAfQAAQAAAEIAAAAFAAAAQgAAAEMAAABEAAAARQAAAEb#####AAAAAQASQ1N1cmZhY2VMaWV1RHJvaXRlAP####8AAAD#AAAABQAAAEcAAAAI#####wAAAAIADENEcm9pdGVQYXJFcQD#####AQAAAAAPAAABAAEAAAAMAAAAEwgAAAAeAAAAAAAAABIAAAACAAAAEwEAAAAeAAAAAAAAABIAAAACAAAAAwD#####AAJhMQABMAAAAAEAAAAAAAAAAAAAAAMA#####wACYTIAATEAAAABP#AAAAAAAAAAAAAWAP####8AAAD#AA0AAAEAAQAAAD4AAAA9AAAAFgD#####AAAA#wANAAABAAEAAABAAAAAP#####8AAAABAApDSW50ZWdyYWxlAP####8AAUkAAAA0AAAAEgAAADsAAAASAAAAPAAAAAFAWQAAAAAAAAAAAAMA#####wACYTMAAi0xAAAABAAAAAE#8AAAAAAAAAAAAAMA#####wACYTQABC0wLjUAAAAEAAAAAT#gAAAAAAAAAAAAEQD#####AQB#AAAPAAAAAAAAAAAAAABACAAAAAAAAAUAAAAADAAAABIAAABPAAAAHwAAADQAAAASAAAATwAAABEA#####wAAfwAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAATwAAAAEAAAAAAAAAAAAAABEA#####wAAfwAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAUAAAAAEAAAAAAAAAAAAAABEA#####wEAfwAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAUAAAAB8AAAA0AAAAEgAAAFAAAAAWAP####8AAH8AAA0AAAEAAQAAAFEAAABSAAAAFgD#####AAB#AAANAAABAAEAAABTAAAAVAAAABYA#####wEAAAAADQAAAQABAAAAUgAAAFMAAAAHAP####8BAAAAABAAAngzAQUAAT#ht6mhLV2bAAAAVwAAABcA#####wAHeENvb3JkMwAAAAwAAABYAAAAAwD#####AAJ4MwAHeENvb3JkMwAAABIAAABZAAAAAwD#####AAJ5MwAFZih4MykAAAAfAAAANAAAABIAAABaAAAAEQD#####AQAAAAAPAAABBQAAAAAMAAAAEgAAAFoAAAASAAAAWwAAACAA#####wEAfwAAAQAAAFwAAAH0AAEAAABYAAAABQAAAFgAAABZAAAAWgAAAFsAAABcAAAAIQD#####AAB#AAAAAAUAAABdAAAACAAAAAMA#####wAGYWJzUHJiAAMwLjUAAAABP+AAAAAAAAAAAAADAP####8ABm9yZFByYgALZihhYnNQcmIpLzMAAAATAwAAAB8AAAA0AAAAEgAAAF8AAAABQAgAAAAAAAAAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEgAAAF8AAAASAAAAYAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAXwAAAAE#4AAAAAAAAP####8AAAABAAhDVmVjdGV1cgD#####AAAAAAANAAABAAEAAABiAAAAYQP#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAP####8AAAD#AQAAAGISAAAAAAABAAAAAgAJQWlyZSDiiYggAAADAAAATgAAAAMA#####wAIYm9ybmVJbmYAAy0xMAAAAAQAAAABQCQAAAAAAAAAAAADAP####8ACGJvcm5lU3VwAAItMQAAAAQAAAABP#AAAAAAAAAAAAAjAP####8AAkkyAAAANAAAABIAAABlAAAAEgAAAGYAAAABQGkAAAAAAAAAAAAlAP####8AAAD#AAAAAAAAAAAAQBgAAAAAAAAAAAA9EgAAAAAAAQAAAAAAAAAAAwAAAEoAAAAlAP####8AAAD#AAAAAAAAAAAAQBgAAAAAAAAAAAA#EgAAAAAAAQAAAAAAAAAAAwAAAEsAAAAlAP####8AAH8AAMAAAAAAAAAAQDMAAAAAAAAAAABTEgAAAAAAAQAAAAAAAAAAAwAAAAMAAAAlAP####8AAH8AAAAAAAAAAAAAQDMAAAAAAAAAAABSEgAAAAAAAQAAAAAAAAAAAwAAAAQAAAAN##########8='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true)
    callback()
  }

  function modifFig (st) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété correction (booléen)
    if (st.correction) {
      // on affiche la correction
      // il me manque a3 et a4 : bornes de l’intervalle sur la figure pour lequel je demande la proba
      // ainsi que a5 et a6 : bornes de l’intervalle écrites sur la figure
      if ((stor.signeIneg === '\\leq') || (stor.signeIneg === '<')) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a3', '-10')
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a4', String((stor.k - stor.mu) / stor.sigma))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a6', String(stor.k))
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a3', String((stor.k - stor.mu) / stor.sigma))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a4', '10')
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a5', String(stor.k))
      }
    } else {
      // ceci sert si jamais on souhaite modifier la courbe initiale dans la question
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'mu', String(stor.mu))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'sigma', String(stor.sigma))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(-stor.typeIntervalle))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(stor.typeIntervalle))
      const borneInf = stor.mu - stor.typeIntervalle * stor.sigma
      const borneSup = stor.mu + stor.typeIntervalle * stor.sigma
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a1', String(borneInf))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a2', String(borneSup))
      const borneInf2 = ((stor.signeIneg === '\\leq') || (stor.signeIneg === '<')) ? '-4' : String((stor.k - stor.mu) / stor.sigma)
      const borneSup2 = ((stor.signeIneg === '\\geq') || (stor.signeIneg === '>')) ? '4' : String((stor.k - stor.mu) / stor.sigma)
      // les variables suivantes vont servir à calculer la proba demandée dans l’énoncé
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'borneInf', borneInf2)
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'borneSup', borneSup2)
      // je planque la zone correction
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a3', '-10')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a4', '-10')
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // Nombre de chances dont dispose l’élève pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      textes: {
        titreExo: 'Utilisation d’une probabilité particulière, puis calcul',
        consigne1: '$X$ est une variable aléatoire qui suit une loi normale d’espérance $£m$ et d’écart-type inconnu.',
        consigne2: 'La courbe ci-dessous représente la fonction densité de cette variable aléatoire $X$.<br/>L’aire colorée est égale à la probabilité $£p$',
        consigne3: 'Déterminer alors, à l’aide de la calculatrice, la probabilité $£q$.',
        consigne4: 'Arrondir le résultat à $10^{-£a}$.',
        comment1: 'Peut-être est-ce un problème d’arrondi ?',
        comment2: 'On rappelle qu’une probabilité est un nombre réel compris entre 0 et 1 !',
        corr1: 'On sait que $£p$ avec $£i$ intervalle centré en $£m$, l’espérance de $X$. Or d’après le cours, si on note $\\mu$ l’espérance et $\\sigma$ l’écart-type de $X$, $£q$ ce qui implique $£e$ et ainsi l’écart-type de la variable aléatoire $X$ vaut $\\sigma=£s$.',
        corr2_1: 'On utilise alors la calculatrice pour calculer la probabilité attendue : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($-10000$ ou toute autre valeur qui remplacera $-\\infty$ puis $£k$) et par les valeurs de l’espérance $\\mu=£m$ et de l’écart-type $\\sigma=£s$.',
        corr2_2: 'On utilise alors la calculatrice pour calculer la probabilité attendue : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($£k$ puis $10000$ ou toute autre valeur qui remplacera $+\\infty$) et par les valeurs de l’espérance $\\mu=£m$ et de l’écart-type $\\sigma=£s$.',
        corr3: 'On obtient ainsi $P(X£s £k)\\approx £r$.'
      },
      pe: 0
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()

    // Construction de la page
    me.construitStructurePage({ structure: ds.structure })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(ds.textes.titreExo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

    // gestion des cas de figure
    const tabCasFigure = []
    let casFigure = []
    for (let k = 1; k <= ds.nbrepetitions; k++) {
      if (tabCasFigure.length === 0) {
        tabCasFigure.push(1, 2)
      }
      const alea = j3pGetRandomInt(0, (tabCasFigure.length - 1))
      casFigure.push(tabCasFigure[alea])
      tabCasFigure.splice(alea, 1)
    }
    if (ds.nbrepetitions > 2) {
      if (j3pGetRandomInt(0, 1) === 1) {
        casFigure.pop()
        casFigure.push(3)
      }
    }
    const casFigureInit = [...casFigure]
    do {
      stor.casFigure = j3pShuffle(casFigure)
      casFigure = [...casFigureInit]
    } while (stor.casFigure[0] === 3)
    me.logIfDebug('stor.casFigure:', stor.casFigure)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // on gère les variables mu et sigma ici
    const choixMu = ['[2;3]', '[4;9]', '[12;20]', '[40;90]']
    const choixSigma = [['[3;6]', 0.05], ['[2;6]', 0.2], ['[3;7]', 0.5], ['[5;10]', 1]]
    const choix = j3pGetRandomInt(0, (choixMu.length - 1))
    if (me.questionCourante % 2 === 1) {
      stor.typeIntervalle = j3pGetRandomInt(1, 2) // 1 pour l’intervalle [mu-sigma;mu+sigma] et 2 pour [mu-2sigma;mu+2sigma]
    } else {
      stor.typeIntervalle = stor.typeIntervalle % 2 + 1
    }

    stor.typeIntervalle = stor.casFigure[me.questionCourante - 1]
    const [intMinMu, intMaxMu] = j3pGetBornesIntervalle(choixMu[choix])
    stor.mu = j3pGetRandomInt(intMinMu, intMaxMu)
    const [intMinSigma, intMaxSigma] = j3pGetBornesIntervalle(choixSigma[choix][0])
    stor.sigma = j3pArrondi(j3pGetRandomInt(intMinSigma, intMaxSigma) * choixSigma[choix][1], 3)
    for (let i = 1; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, { m: stor.mu })
    const p = (stor.typeIntervalle === 1) ? 'P(' + j3pArrondi(stor.mu - stor.sigma, 4) + '\\leq X\\leq ' + j3pArrondi(stor.mu + stor.sigma, 4) + ')' : (stor.typeIntervalle === 2) ? 'P(' + j3pArrondi(stor.mu - 2 * stor.sigma, 4) + '\\leq X\\leq ' + j3pArrondi(stor.mu + 2 * stor.sigma, 4) + ')' : 'P(' + j3pArrondi(stor.mu - 3 * stor.sigma, 4) + '\\leq X\\leq ' + j3pArrondi(stor.mu + 3 * stor.sigma, 4) + ')'
    j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, { p })
    stor.probaDonnee = (stor.typeIntervalle === 1) ? p + '\\approx 0,683' : (stor.typeIntervalle === 2) ? p + '\\approx 0,954' : p + '\\approx 0,997'
    stor.intervalle = (stor.typeIntervalle === 1) ? '[' + j3pArrondi(stor.mu - stor.sigma, 4) + ';' + j3pArrondi(stor.mu + stor.sigma, 4) + ']' : (stor.typeIntervalle === 2) ? '[' + j3pArrondi(stor.mu - 2 * stor.sigma, 4) + ';' + j3pArrondi(stor.mu + 2 * stor.sigma, 4) + ']' : '[' + j3pArrondi(stor.mu - 3 * stor.sigma, 4) + ';' + j3pArrondi(stor.mu + 3 * stor.sigma, 4) + ']'
    /// /////////////////////////////////////// Pour MathGraph32
    const largFig = 548// on gère ici la largeur de la figure
    const hautFig = 229
    const divMtg32 = j3pAddElt(stor.zoneCons3, 'div')
    // et enfin la zone svg (très importante car tout est dedans)
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
    // ce qui suit lance la création initiale de la figure
    depart(function () {
      /// //////////////////////////////////////Fin de l’initialisation de la figure MathGraph32'

      // Je m’occpue maintenant de l’intervalle pour lequel on souhaite calculer la probabilité
      // Cet intervalle sera de la forme X>k ou X<k
      let ecartK = 0
      do {
        if (choixSigma[choix][1] === 0.05) {
          ecartK = j3pGetRandomInt(1, 5) * 0.1
        } else {
          ecartK = j3pGetRandomInt(1, choixSigma[choix][0].substring(choixSigma[choix][0].indexOf(';') + 1, choixSigma[choix][0].length - 1)) * choixSigma[choix][1]
        }
      } while ((Math.abs(ecartK - stor.sigma) < Math.pow(10, -12)) || (Math.abs(ecartK - 2 * stor.sigma) < Math.pow(10, -12)) || (ecartK >= 2.5 * stor.sigma))
      const plusMoins = j3pGetRandomInt(0, 1) * 2 - 1
      const signeIneg = j3pRandomTab(['\\leq', '\\geq', '<', '>'], [0.25, 0.25, 0.25, 0.25])
      stor.k = stor.mu + plusMoins * ecartK
      j3pAffiche(stor.zoneCons4, '', ds.textes.consigne3, { q: 'P(X' + signeIneg + ' ' + stor.k + ')' })
      const elt = j3pAffiche(stor.zoneCons5, '', '$P(X' + signeIneg + ' ' + j3pVirgule(stor.k) + ')\\approx $&1&.',
        {
          inputmq1: { texte: '' }
        })
      stor.zoneInput = elt.inputmqList[0]
      j3pAffiche(stor.zoneCons6, '', ds.textes.consigne4, { a: 3 })
      mqRestriction(stor.zoneInput, '\\d-.,')
      stor.signeIneg = signeIneg
      // on peut ici recharger la figure si on le souhaite
      // juste à préciser que nous ne sommes pas dans la phase de correction ici
      modifFig({ correction: false })
      // à cet instant la figure à été recalculée. Je peux donc récupérer la valeur de la proba cherchée
      stor.zoneInput.typeReponse = ['nombre', 'arrondi', Math.pow(10, -3)]
      stor.zoneInput.reponse = [Number(stor.mtgAppLecteur.valueOf(stor.mtg32svg, 'I2'))]

      me.logIfDebug('reponse : ', stor.zoneInput.reponse[0], '   sigma:', stor.sigma, '   mu:', stor.mu)
      /// //////////////////////////////////////
      /* FIN DU CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////////
      // ce qui suit est le div dans lequel seront donnés les commentaires concernant la réponse de l’élève : c’est bien, c’est faux...'
      // Comme la consigne est à droite, il faut que je positionne ces commentaire en-dessous
      stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '5px' }) })
      const mesZonesSaisie = [stor.zoneInput.id]
      stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
      j3pFocus(stor.zoneInput)
    })
    // Obligatoire
    me.finEnonce()
  }
  // console.log("debut du code : ",me.etat)
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }

      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      // comme pour l’utilisation de Validation_zones, je déclare l’objet reponse qui contient deux propriétés : aRepondu et bonneReponse
      let reponse = { aRepondu: false, bonneReponse: false }
      const fctsValid = stor.fctsValid
      // comme pour l’utilisation de Validation_zones, je déclare l’objet reponse qui contient deux propriétés : aRepondu et bonneReponse
      reponse = fctsValid.validationGlobale()

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        stor.zoneCorr.style.color = me.styles.cfaux
        me.reponseManquante(stor.zoneCorr, '<br>' + reponseManquante)
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (reponse.bonneReponse) {
          me._stopTimer()
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          afficheCorrection(true)// affichage de la correction
          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()

            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.cacheBoutonValider()
            /*
             *   RECOPIER LA CORRECTION ICI !
             */
            afficheCorrection(false)// affichage de la correction
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            // on peut affiner le commentaire si on veut
            if (!isNaN(j3pNombre(fctsValid.zones.reponseSaisie[0]))) {
              if ((j3pNombre(fctsValid.zones.reponseSaisie[0]) < 0) || (j3pNombre(fctsValid.zones.reponseSaisie[0]) > 1)) {
                // c’est que l’élève donne un résultats incohérent (proba qui n’est pas entre 0 et 1)
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
              } else if (Math.abs(j3pNombre(fctsValid.zones.reponseSaisie[0]) - stor.zoneInput.reponse[0]) < Math.pow(10, -2)) {
                // c’est que l’élève a fait le bon calcul mais qu’il a donné une valeur approchée et non un arrondi
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
              }
            }
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              afficheCorrection(false)// affichage de la correction
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
