import { j3pAddElt, j3pFocus, j3pGetRandomInt, j3pRandomTab, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        février 2020
        détermination de k tel que P(X>=k)>=p
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeIneq', 'inferieur', 'liste', 'Nature de l’inégalité dans la probabilité : P(X<=k) ou P(X>=k)', ['inferieur', 'superieur']]
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })

    if (ds.typeIneq === 'inferieur') {
      ;[1, 2, 3].forEach(i => {
        stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
        j3pAffiche(stor['zoneExpli' + i], '', ds.textes['corr' + i], stor.objCons)
      })
    } else {
      ;[1, 2].forEach(i => {
        stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
        j3pAffiche(stor['zoneExpli' + i], '', ds.textes['corr' + (i + 3)], stor.objCons)
      })
      ;[1, 2, 3].forEach(i => {
        stor['zoneExpli' + (i + 2)] = j3pAddElt(zoneExpli, 'div')
        j3pAffiche(stor['zoneExpli' + (i + 2)], '', ds.textes['corr' + i], stor.objCons)
      })
    }
  }

  function combinaison (n, p) {
    // Cette fonction calcule C_n^p sans utiliser les factorielles pour gagner un peu de temps
    if (n === p || p === 0) return 1
    let res = n
    let i = n - 1
    while (i > Math.max(p, n - p)) {
      res *= i
      i -= 1
    }
    i = 2
    while (i <= Math.min(p, n - p)) {
      res = Math.round(res / i)
      i += 1
    }
    // console.log("combinaison(",n,",",p,")=",res)
    return res
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.
      typeIneq: 'inferieur',
      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'inverse de la loi binomiale',
        // on donne les phrases de la consigne
        consigne1: 'Soit $£x$ une variable aléatoire suivant la loi binomiale $B(£n;£p)$.',
        consigne2_1: 'Soit $k$ le plus petit entier tel que $P(£x \\leq k)\\geq £{prb}$',
        consigne2_2: 'Soit $k$ le plus grand entier tel que $P(£x \\geq k)\\geq £{prb}$',
        consigne3: 'Alors $k=$&1&',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: '',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'A l’aide de la calculatrice, on peut déterminer toutes les probabilités de la forme $P(£x\\leq k)$ avec $k$ allant de 0 à £n.',
        corr2: 'Or $P(£x\\leq calcule[£k-1])\\approx £{p1}$ et $P(£x\\leq £k)\\approx £{p2}$.',
        corr3: 'Donc $k=£k$.',
        corr4: '$P(£x\\geq k)=1-P(£x < k)=1-P(£x \\leq k-1)$. Ainsi $P(£x \\geq k)\\geq £{prb}$ équivaut à $P(£x\\leq k-1)\\leq calcule[1-£{prb}]$.',
        corr5: 'On cherche alors le plus grand entier $k$ tel que $P(£x\\leq k-1)\\leq calcule[1-£{prb}]$.'
      },
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    // Construction de la page

    me.construitStructurePage({ structure: ds.structure })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    // code de création des fenêtres

    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    stor.objCons = {}
    stor.objCons.n = j3pGetRandomInt(0, 15) + 15
    stor.objCons.p = (40 + j3pGetRandomInt(0, 50)) / 100
    stor.objCons.prb = (10 + j3pGetRandomInt(0, 80)) / 100
    stor.objCons.x = j3pRandomTab(['X', 'Y', 'Z'], [0.33, 0.33, 0.34])
    stor.objCons.tabProba = []// tableau de toutes les proba de la forme P(X<=k) avec k dans [[0;n]]
    stor.objCons.tabProba.push(Math.pow(1 - stor.objCons.p, stor.objCons.n))
    for (let i = 1; i <= stor.objCons.n; i++) {
      stor.objCons.tabProba.push(stor.objCons.tabProba[i - 1] + combinaison(stor.objCons.n, i) * Math.pow(stor.objCons.p, i) * Math.pow(1 - stor.objCons.p, stor.objCons.n - i))
    }
    me.logIfDebug('tabProba:', stor.objCons.tabProba)
    stor.objCons.k = 0
    let pos = 0
    const borneSup = (ds.typeIneq === 'inferieur') ? stor.objCons.prb : 1 - stor.objCons.prb

    while (stor.objCons.tabProba[pos] <= borneSup) {
      pos += 1
    }
    stor.objCons.p1 = Math.round(10000 * stor.objCons.tabProba[pos - 1]) / 10000
    stor.objCons.p2 = Math.round(10000 * stor.objCons.tabProba[pos]) / 10000
    stor.objCons.k = pos
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, stor.objCons)
    j3pAffiche(stor.zoneCons2, '', (ds.typeIneq === 'inferieur') ? ds.textes.consigne2_1 : ds.textes.consigne2_2, stor.objCons)
    const elt = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3, {
      k: stor.objCons.k,
      inputmq1: { texte: '' }
    })
    stor.zoneInput = elt.inputmqList[0]
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    mqRestriction(stor.zoneInput, '\\d')

    stor.zoneInput.reponse = [stor.objCons.k]
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          this.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
