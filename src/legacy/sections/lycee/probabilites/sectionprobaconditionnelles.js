import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pNombre, j3pDetruit, j3pEmpty, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pShowError, j3pGetNewId, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { remplaceCalcul } from 'src/legacy/outils/fonctions/gestionParametres'
import { construireArbreCond, reecrireEvt, simplifieEvt, egaliteTableauPrb, bonEvts } from 'src/legacy/outils/arbreProba/arbreProba'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    mai 2016
    Dans cette section, on propose à l’élève de résoudre un exercice de probabilité conditionnelles
    Quatre question sont possibles : arbre de proba, la proba d’une intersection, un eutilisation de la formule des probabilités totales et un proba conditionnelles utilisant le dernier résultat
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuestions', [1, 2, 3, 4], 'array', 'Tableau qui contient les questions choisies (dans l’ordre croissant):<br>- 1 pour demander l’arbre;<br>- 2 pour la probabilité d’une intersection;<br>-3 pour une probabilité (avec formule des probabilités totales);<br>-4 pour une probabilité conditionnelle à l’aide du précédent résultat.'],
    ['tabCasFigure', [''], 'array', 'Pour chaque répétition, on peut imposer, sous forme d’un tableau,  un des exercices particuliers présents dans le fichier texte annexe :<br>-1 : Métropole juin 2015 ES<br>-2 : Pondichéry avril 2016 STMG<br>-3 : Amérique du Nord juin 2015 S<br>-4 : Polynésie septembre 2015 S<br>-5 : Pondichéry avril 2015 ES<br>-6 : Liban juin 2015 ES<br>-7 : Liban mai 2014 S<br>-8 : Pondichéry avril 2015 STMG<br>-9 : Métropole juin 2016 S<br>-10 : Polynésie juin 2015 STMG<br>-11 : Ex 14 p.339 Manuel Magnard<br>- 12 : Ex 21 p.340 Manuel Magnard<br>- 13 : Ex 22 p.340 Manuel Magnard'],
    ['afficheEvt', false, 'boolean', 'Dans la construction de l’arbre, on peut donner les événements et ne demander que les probabilités si ce paramètre vaut true.'],
    ['ecrireProbaInter', false, 'boolean', 'Ceci vaut true si on écrit clairement la proba calculée sous la forme P(A inter B), false si c’est sous la forme d’une phrase'],
    ['ecrireProbaTotale', false, 'boolean', 'Ceci vaut true si on écrit la probabilité calculée avec la formule des probabilités totales sous la forme P(B), false sinon.'],
    ['ecrireProbaCond', false, 'boolean', 'Ceci vaut true si on écrit la probabilité conditionnelle à calculer sous la forme P_B(A), false sinon.'],
    ['avecSachant', true, 'boolean', 'Paramètre pour la probabilité conditionnelle. S’il vaut true, alors le mot "sachant" est écrit, sinon la question est tournée sans ce mot.'],
    ['seuilreussite', 0.8, 'reel', 'Pourcentage à partir duquel on considère que l’exercice est correctement traité.'],
    ['seuilErreur', 0.4, 'reel', 'Pourcentage à partir duquel on retient l’erreur pour la phrase d’état.']
    // paramètre pour la proba conditionnelle : avec ou sans le mot sachant
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'problème dans la construction d’un arbre de probabilité' },
    { pe_3: 'problème dans le calcul de la probabilité d’une intersection' },
    { pe_4: 'problème dans l’utilisation de la formule des probabilités totales' },
    { pe_5: 'problème dans le calcul d’une probabilité conditionnelle' },
    { pe_6: 'Insuffisant' }
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    function afficheArbre () {
      // dans le cas d’une mauvaise réponse, je donne l’arbre
      const zoneExpliArbre1 = j3pAddElt(zoneExpliArbre, 'div')
      j3pAffiche(zoneExpliArbre1, '', ds.textes.corr3)
      stor.objArbreRep.lestyle.couleur = me.styles.toutpetit.correction.color
      construireArbreCond(stor.zoneArbre, stor.objArbreRep)
    }
    if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] !== 1) {
      j3pDetruit(stor.zoneCalc)
      j3pDetruitFenetres('Calculatrice')
    }
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color, paddingTop: '15px' } })
    const zoneExpliArbre = j3pAddElt(zoneExpli, 'div')
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    switch (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes]) {
      case 1:
        // arbre de proba
        try {
          j3pEmpty(stor.zoneArbre.laPalette)
          if ((ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 1) && !ds.afficheEvt) {
            stor.zoneArbre.listeBtns.forEach(btn => j3pDetruit(btn))
          }
          j3pDetruit(stor.idIndication)
        } catch (e) {
          console.warn(e) // pas normal mais pas grave
        }
        {
          const objVar = {}
          let num = 97
          for (let k = 0; k < stor.prbEnonce['enonce' + stor.numeroExo].length; k++) {
            objVar[String.fromCharCode(num)] = remplaceCalcul({
              text: stor.prbEnonce['enonce' + stor.numeroExo][k],
              obj: stor.obj
            })
            num++
          }
          j3pAffiche(zoneExpli1, '', stor.correctionProba['enonce' + stor.numeroExo], objVar)
          if (!bonneReponse) {
            const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
            j3pAffiche(zoneExpli2, '', ds.textes.corr1, {})
            // je construis une autre fenêtre pour y mettre la correction
            const expliArbre = j3pAddElt(stor.detailsFenetre, 'div')
            j3pToggleFenetres(stor.idCorrDetails)
            stor.objArbreRep.lestyle.couleur = me.styles.toutpetit.correction.color
            construireArbreCond(expliArbre, stor.objArbreRep)
          }
        }
        break
      case 2:
        if (!stor.questionArbre && !bonneReponse) afficheArbre()
        j3pAffiche(zoneExpli1, '', stor.correctionInter['enonce' + stor.numeroExo], stor.obj)
        break
      case 3:
        if (!stor.questionArbre && !bonneReponse) afficheArbre()
        j3pAffiche(zoneExpli1, '', ds.textes.corr4)
        {
          const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
          j3pAffiche(zoneExpli2, '', stor.correctionPrbTotale['enonce' + stor.numeroExo], stor.obj)
        }
        break
      default :
        if (!stor.questionArbre && !bonneReponse) {
          afficheArbre()
        }
        // gestion de l’arrondi éventuel de la réponse
        // Dans la probabilité conditionnelle, j’ai un paramètre £s pour le signe : = ou \\aprox, à tester avant
        {
          const reponseArrondie = Math.round(j3pCalculValeur(remplaceCalcul({
            text: stor.reponsePrbSachant['enonce' + stor.numeroExo],
            obj: stor.obj
          })) * Math.pow(10, Number(stor.arrondiCond['enonce' + stor.numeroExo]))) / Math.pow(10, Number(stor.arrondiCond['enonce' + stor.numeroExo]))
          stor.obj.r = reponseArrondie
          const lareponseExacte = j3pCalculValeur(remplaceCalcul({
            text: stor.reponsePrbSachant['enonce' + stor.numeroExo],
            obj: stor.obj
          }))
          stor.obj.s = (Math.abs(lareponseExacte - reponseArrondie) < Math.pow(10, -10)) ? '=' : '\\approx'
          j3pAffiche(zoneExpli1, '', stor.correctionPrbSachant['enonce' + stor.numeroExo], stor.obj)
        }
        break
    }
    $('.mq-overline').each(function () {
      $(this).css('border-top', '1px solid ' + $(this).css('color'))
      // je parcours l’ensemble des overline et je leur donne la couleur dans laquelle se trouve le texte
    })
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 2,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      pe_1: 'Bien',
      pe_2: 'problème dans la construction d’un arbre de probabilité',
      pe_3: 'problème dans le calcul de la probabilité d’une intersection',
      pe_4: 'problème dans l’utilisation de la formule des probabilités totales',
      pe_5: 'problème dans le calcul d’une probabilité conditionnelle',
      pe_6: 'Insuffisant',
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      tabCasFigure: [],
      typeQuestions: [1, 2, 3, 4],
      afficheEvt: false,
      ecrireProbaInter: false,
      ecrireProbaTotale: false,
      ecrireProbaCond: false,
      avecSachant: true,
      seuilErreur: 0.4,
      seuilreussite: 0.8,
      /*
          Les textes sont présents dans un fichier externe : sectionsAnnexes/Consignesexos/probaCond.js
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Probabilités conditionnelles',
        // on donne les phrases de la consigne
        consigne1_1: 'Construire l’arbre de probabilité traduisant la situation.',
        consigne1_2: 'Compléter l’arbre de probabilité traduisant la situation.',
        consigne2: 'On a représenté ci-contre l’arbre de probabilité représentant la situation.',
        consigne3: 'On a montré que $£p=£v$.',
        consigne4: 'On a montré que $£p=£v$ et $£q=£w$.',
        consigne5: 'Les boutons + et - permettent d’ajouter ou supprimer une branche au niveau du noeud (4 branches au maximum).',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Le nombre de branches n’est pas correct !',
        comment2: 'Une erreur est détectée sur au moins une branche issue de ce(s) noeud(s) !',
        comment3: 'Une erreur est détectée sur au moins une branche issue de la racine !',
        comment4: ' Au moins un des événements proposés n’est pas défini dans l’énoncé !',
        comment5: 'N’y aurait-il pas un problème d’arrondi ?',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'On obtient alors l’arbre ci-contre :',
        corr2: 'Arbre attendu :',
        corr3: 'On peut représenter la situation par l’arbre ci-contre :',
        corr4: 'Par une lecture de l’arbre (ce qui revient à utiliser la formule des probabilités totales), on obtient :'
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      pe: 0
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()

    // Construction de la page
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.65 })

    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication) me.indication(me.zones.IG, ds.indication)

    import('../../../sectionsAnnexes/Consignesexos/probaCond.js').then(function parseAnnexe ({ default: datasSection }) {
      let numQuest, numInter
      const tabExercices = []
      let numCond
      stor.nb_sujets = datasSection.sujets.length
      stor.consignes = {}
      stor.nomVariables = {}
      stor.variables = {}
      // l’objet pour le contenu de l’arbre
      stor.listeEvt = {}
      stor.evtsProba = {}
      stor.prbEnonce = {}
      stor.correctionProba = {}

      // puis les objets qui seront les questions
      stor.questionInter1 = {}
      stor.questionInter = {}
      stor.questionInterFin = {}
      stor.reponseInter = {}
      stor.correctionInter = {}

      stor.questionPrbTotale1 = {}
      stor.questionPrbTotale = {}
      stor.questionPrbTotaleFin = {}
      stor.reponsePrbTotale = {}
      stor.correctionPrbTotale = {}

      stor.questionPrbSachant = {}
      stor.questionPrbSachantFin = {}
      stor.questionPrbSachant1 = {}
      stor.reponsePrbSachant = {}
      stor.correctionPrbSachant = {}
      stor.arrondiCond = {}
      for (let j = 0; j < stor.nb_sujets; j++) {
        tabExercices.push(j)
        stor.consignes['enonce' + j] = datasSection.sujets[j].consignes
        stor.nomVariables['enonce' + j] = []
        stor.variables['enonce' + j] = []
        for (let i = 0; i < datasSection.sujets[j].variables.length; i++) {
          stor.nomVariables['enonce' + j][i] = datasSection.sujets[j].variables[i][0]
          const [intMin, intMax] = j3pGetBornesIntervalle(datasSection.sujets[j].variables[i][1])
          if (datasSection.sujets[j].variables[i].length === 2) {
            stor.variables['enonce' + j][i] = j3pGetRandomInt(intMin, intMax)
          } else {
            stor.variables['enonce' + j][i] = j3pGetRandomInt(intMin, intMax) / Number(datasSection.sujets[j].variables[i][2])
          }
        }
        // je récupère les événements et proba qui vont me permettre de construire l’arbre de proba
        stor.listeEvt['enonce' + j] = datasSection.sujets[j].listeEvt
        stor.evtsProba['enonce' + j] = datasSection.sujets[j].evtsProba
        stor.prbEnonce['enonce' + j] = datasSection.sujets[j].prbEnonce
        stor.correctionProba['enonce' + j] = datasSection.sujets[j].correctionProba

        numQuest = j3pGetRandomInt(0, (datasSection.sujets[j].questionPrbTotale.length - 1))
        numInter = j3pGetRandomInt(0, (datasSection.sujets[j].questionInter[numQuest].length - 1))
        numCond = j3pGetRandomInt(0, (datasSection.sujets[j].questionPrbSachant[numQuest].length - 1))

        // on s’intéresse ici seulement à la proba d’une intersection
        // dans le cas où on n’écrit pas explicitement la proba
        stor.questionInter1['enonce' + j] = datasSection.sujets[j].questionInter1[numQuest][numInter]
        // on pose une question, puis on écrit la proba cherchée
        stor.questionInter['enonce' + j] = datasSection.sujets[j].questionInter[numQuest][numInter]
        stor.questionInterFin['enonce' + j] = datasSection.sujets[j].questionInterFin[numQuest][numInter]
        // la réponse avec présence encore des variables (géré dans cette section au niveau de la mise en place de la réponse)
        stor.reponseInter['enonce' + j] = datasSection.sujets[j].probaInter[numQuest][numInter]
        // correction pour la fonction afficheCorrection
        stor.correctionInter['enonce' + j] = datasSection.sujets[j].correctionInter[numQuest][numInter]

        // ce qui suit est pour la formule des proba totales
        // dans le cas où on n’écrit pas explicitement la proba
        stor.questionPrbTotale1['enonce' + j] = datasSection.sujets[j].questionPrbTotale1[numQuest]
        // on écrit clairement la proba cherchée
        stor.questionPrbTotale['enonce' + j] = datasSection.sujets[j].questionPrbTotale[numQuest]
        stor.questionPrbTotaleFin['enonce' + j] = datasSection.sujets[j].questionPrbTotaleFin[numQuest]
        // la réponse avec présence encore des variables (géré dans cette section au niveau de la mise en place de la réponse)
        stor.reponsePrbTotale['enonce' + j] = datasSection.sujets[j].probaTotales[numQuest]
        // correction pour la fonction afficheCorrection
        stor.correctionPrbTotale['enonce' + j] = datasSection.sujets[j].correctionTotale[numQuest]

        // pour la probabilité conditionnelle
        if (ds.avecSachant) {
          stor.questionPrbSachant['enonce' + j] = datasSection.sujets[j].questionPrbSachant[numQuest][numCond]
          stor.questionPrbSachantFin['enonce' + j] = datasSection.sujets[j].questionPrbSachantFin[numQuest][numCond]
          stor.questionPrbSachant1['enonce' + j] = datasSection.sujets[j].questionPrbSachant1[numQuest][numCond]
        } else {
          stor.questionPrbSachant['enonce' + j] = datasSection.sujets[j].questionPrbCond[numQuest][numCond]
          stor.questionPrbSachant1['enonce' + j] = datasSection.sujets[j].questionPrbCond1[numQuest][numCond]
        }
        stor.questionPrbSachantFin['enonce' + j] = datasSection.sujets[j].questionPrbSachantFin[numQuest][numCond]

        stor.reponsePrbSachant['enonce' + j] = datasSection.sujets[j].probaCond[numQuest][numCond]
        stor.correctionPrbSachant['enonce' + j] = datasSection.sujets[j].correctionCond[numQuest][numCond]
        stor.arrondiCond['enonce' + j] = datasSection.sujets[j].arrondiCond
      }

      // ds.nbrepetitions = Math.min(ds.nbrepetitions, stor.nb_sujets)
      // pour le nombre d’étapes, c’est en fait gérer par un tableau ds.typeQuestions
      // je vérifie que le tableau est bien construit (4 valeurs au max et dans l’ordre croissant)
      let index = 0
      while (index < ds.typeQuestions.length - 1) {
        if (ds.typeQuestions[index] < ds.typeQuestions[index + 1]) {
          index++
        } else {
          // la valeur en index+1 n’est pas correcte donc on la vire
          ds.typeQuestions.splice(index + 1, 1)
        }
      }
      if (ds.typeQuestions.length === 0) {
        ds.typeQuestions = [1, 2, 3, 4]
      }
      ds.nbetapes = ds.typeQuestions.length
      ds.nbitems = ds.nbetapes * ds.nbrepetitions
      // enfin je gère le cas où on demanderait un cas de figure qui n’existe pas :
      index = 0
      while (index < ds.tabCasFigure.length) {
        ds.tabCasFigure[index] = Number(ds.tabCasFigure[index])
        if ((ds.tabCasFigure[index] > 0) && (ds.tabCasFigure[index] <= tabExercices.length)) {
          index++
        } else {
          // le numéro de question choisi n’existe pas
          ds.tabCasFigure.splice(index, 1)
        }
      }
      stor.numexo = 0 // c’est pour savoir à quelle répétition nous en sommes

      // Les variables qui suivent me servent pour savoir quelles questions intermédiares sont posées
      // on affichera alors les résultats pour les questions suivantes
      suite()
    }).catch(error => {
      console.error(error)
      j3pShowError('Impossible de charger les données de cette exercice')
    })
  }
  function suite () {
    const { top, left, width, height } = me.zonesElts.MG.getBoundingClientRect()
    const w = Math.min(440, width - 20)
    const h = Math.min(440, height - 20)
    // init des fenêtres (on peut ajouter calculatrice plus loin)
    stor.detailsFenetre = j3pGetNewId('detailsFenetre')
    stor.idCorrDetails = j3pGetNewId('CorDetails')
    me.fenetresjq = [
      {
        name: stor.idCorrDetails,
        title: ds.textes.corr2,
        width: w,
        height: h,
        // on cale en bas à droite
        left: left + width - w - 20,
        top: top + height - h - 20,
        id: stor.detailsFenetre
      }
    ]
    j3pCreeFenetres(me)

    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      // c’est qu’on passe à un nouvel énoncé (éventuellement le premier)
      let choixExoImpose = (ds.tabCasFigure[stor.numexo] !== undefined)
      if (choixExoImpose) {
        // je regarde tout de même si le numéro existe bien
        if (Number(ds.tabCasFigure[stor.numexo]) > stor.nb_sujets) {
          choixExoImpose = false
        }
      }
      if (choixExoImpose) {
        // c’est que j’ai imposé l’exo à traiter
        stor.numeroExo = Number(ds.tabCasFigure[stor.numexo]) - 1
      } else {
        // je choisis au hasard un des exos à traiter (parmi ceux qui ne sont pas déjà choisis)
        let nbTentatives = 0 // c’est au cas où on aurait déjà proposé tous les cas de figure (vu le nombre d’exos possible qu’il risque d’y avoir, c’est peu probable)
        do {
          nbTentatives++
          stor.numeroExo = j3pGetRandomInt(0, (stor.nb_sujets - 1))
        } while ((ds.tabCasFigure.indexOf(stor.numeroExo + 1) > -1) && (nbTentatives < 30))
        ds.tabCasFigure[(me.questionCourante - 1) / ds.nbetapes] = stor.numeroExo + 1
      }
      me.logIfDebug('ds.tabCasFigure:', ds.tabCasFigure, '   stor.numeroExo:', stor.numeroExo)
      stor.numexo++
      // stor.phrasesEnonce = sec.tabExercices[stor.numeroExo]
      stor.phrasesEnonce = stor.consignes['enonce' + stor.numeroExo]
      stor.obj = {}
      for (let i = 0; i < stor.nomVariables['enonce' + stor.numeroExo].length; i++) {
        stor.obj[stor.nomVariables['enonce' + stor.numeroExo][i]] = stor.variables['enonce' + stor.numeroExo][i]
      }
      // je réinitialise mes variables pour la répétition suivante :
      stor.questionArbre = false
      stor.questionIntersection = false
      stor.questionTotale = false
      // je récupère la liste des caractères à mettre dans le clavier virtuel
      let avecNum = false
      stor.lstRestriction = stor.listeEvt['enonce' + stor.numeroExo].map(evt => evt[0]).join('')
      stor.listeEvt['enonce' + stor.numeroExo].forEach((evt) => {
        if (evt.length > 1) avecNum = true
      })
      if (avecNum) stor.lstRestriction += '\\d'
    }
    // je choisis
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= stor.phrasesEnonce.length; i++) {
      stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(stor['zoneCons' + i], '', stor.phrasesEnonce[i - 1], stor.obj)
    }

    // div supplémentaire dans la zone droite pour positionner l’arbre et les commentaires sur les réponses de l’élève
    stor.zoneDroite = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    stor.zoneArbre = j3pAddElt(stor.zoneDroite, 'div')
    // on vérifie si des questions intermédiares ont été traitées
    if (stor.questionTotale) {
      // si on a utilisé la formule des probabilités totales, c’est qu’on a eu besoin de l’arbre
      stor.questionArbre = true
    }
    if (stor.questionIntersection) {
      // si la question sur l’intersection a été posée, c’est qu’on a eu besoin de l’arbre, donc je l’affiche
      stor.questionArbre = true
    }

    // je dois créer l’objet qui contiendra toutes les caractéristiques de mon arbre
    // cela me servira pour la réponse dans le cadre de l’arbre à construire ou pour afficher l’arbre dans les questions suivantes
    stor.objArbreRep = {
      parcours: me
    }
    // taille du texte evt (pour les proba, il faudrait qu’elles soient plus petites sans doute) + couleur du texte et des branches
    stor.objArbreRep.lestyle = {
      styletexte: me.styles.toutpetit.enonce,
      couleur: me.styles.toutpetit.enonce.color
    }
    // pour savoir si on écrit les évts et les proba (zones de saisie si false)
    stor.objArbreRep.complet = { evts: ds.afficheEvt, proba: false }
    stor.objArbreRep.evts1 = []
    stor.objArbreRep.evts2 = {}
    stor.objArbreRep.complet = { evts: true, proba: true }
    stor.objArbreRep.lstRestriction = stor.lstRestriction
    let laProba
    for (let i = 0; i < stor.evtsProba['enonce' + stor.numeroExo].length; i++) {
      laProba = remplaceCalcul({
        text: stor.evtsProba['enonce' + stor.numeroExo][i][1],
        obj: stor.obj
      })
      stor.objArbreRep.evts1.push([stor.evtsProba['enonce' + stor.numeroExo][i][0], laProba])
      stor.objArbreRep.evts2['branche' + i] = []
      for (let j = 0; j < stor.evtsProba['enonce' + stor.numeroExo][i][2].length; j++) {
        laProba = remplaceCalcul({
          text: stor.evtsProba['enonce' + stor.numeroExo][i][2][j][1],
          obj: stor.obj
        })
        stor.objArbreRep.evts2['branche' + i].push([stor.evtsProba['enonce' + stor.numeroExo][i][2][j][0], laProba])
      }
    }
    if (stor.questionArbre) {
      // je redonne l’arbre (à droite)
      const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(zoneCons1, '', ds.textes.consigne2)
      construireArbreCond(stor.zoneArbre, stor.objArbreRep)
    }
    if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] !== 1) {
      me.fenetresjq.push({ name: 'Calculatrice', title: 'Calculatrice' })
      j3pCreeFenetres(me) // ça ne recrée pas CorDetails qui existe déjà
      stor.zoneCalc = j3pAddElt(stor.zoneDroite, 'div')
      j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
      j3pAfficheCroixFenetres('Calculatrice')
    }

    if (stor.questionIntersection) {
      const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
      stor.obj.p = stor.questionInterFin['enonce' + stor.numeroExo]
      stor.obj.v = 'calcule[' + stor.reponseInter['enonce' + stor.numeroExo] + ']'
      if (stor.questionTotale) {
        stor.obj.q = stor.questionPrbTotaleFin['enonce' + stor.numeroExo]
        stor.obj.w = 'calcule[' + stor.reponsePrbTotale['enonce' + stor.numeroExo] + ']'
        j3pAffiche(zoneCons2, '', ds.textes.consigne4, stor.obj)
      } else {
        j3pAffiche(zoneCons2, '', ds.textes.consigne3, stor.obj)
      }
    } else {
      if (stor.questionTotale) {
        const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
        stor.obj.p = stor.questionPrbTotaleFin['enonce' + stor.numeroExo]
        stor.obj.v = 'calcule[' + stor.reponsePrbTotale['enonce' + stor.numeroExo] + ']'
        j3pAffiche(zoneCons2, '', ds.textes.consigne3, stor.obj)
      }
    }
    const zoneCons3 = j3pAddElt(stor.conteneur, 'div')
    let elt
    switch (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes]) {
      case 1:
        // on demande de construire l’arbre
        if (ds.afficheEvt) {
          j3pAffiche(zoneCons3, '', ds.textes.consigne1_2)
        } else {
          j3pAffiche(zoneCons3, '', ds.textes.consigne1_1)
          stor.idIndication = j3pAddElt(stor.conteneur, 'div')
          j3pStyle(stor.idIndication, { fontStyle: 'italic', paddingTop: '15px' })
          j3pAffiche(stor.idIndication, '', ds.textes.consigne5)
        }
        stor.questionArbre = true
        // objet qui contiendra toutes les caractéristiques de mon arbre
        stor.objArbre = {}
        // taille du texte evt (pour les proba, il faudrait qu’elles soient plus petites sans doute) + couleur du texte et des branches
        stor.objArbre.lestyle = {
          styletexte: me.styles.toutpetit.enonce,
          couleur: me.styles.toutpetit.enonce.color
        }
        // pour savoir si on écrit les évts et les proba (zones de saisie si false)
        stor.objArbre.complet = { evts: ds.afficheEvt, proba: false }
        // au départ ne contiendra que 2*une branche
        // stor.objArbre.evts1 = [['',''],['','']]   //Pour cette branche, l’événement et la proba ne sont pas complétés donc on les initialise à 'vide'
        if (ds.afficheEvt) {
          // les événements sont affichées (donc pas moyen d’ajouter un noeud)
          stor.objArbre.evts1 = []
          stor.objArbre.evts2 = {}
          for (let i = 0; i < stor.evtsProba['enonce' + stor.numeroExo].length; i++) {
            laProba = remplaceCalcul({
              text: stor.evtsProba['enonce' + stor.numeroExo][i][1],
              obj: stor.obj
            })
            stor.objArbre.evts1.push([stor.evtsProba['enonce' + stor.numeroExo][i][0], laProba])
            stor.objArbre.evts2['branche' + i] = []
            for (let j = 0; j < stor.evtsProba['enonce' + stor.numeroExo][i][2].length; j++) {
              laProba = remplaceCalcul({
                text: stor.evtsProba['enonce' + stor.numeroExo][i][2][j][1],
                obj: stor.obj
              })
              stor.objArbre.evts2['branche' + i].push([stor.evtsProba['enonce' + stor.numeroExo][i][2][j][0], laProba])
            }
          }
        } else {
          stor.objArbre.evts1 = [['', '']]
          stor.objArbre.evts2 = {}
          stor.objArbre.evts2['branche' + 0] = [['', '']] // Je nomme branche0 la branche précédente et de cette branche, je fais partir une seule branche où l’évt et la proba sont pour l’instant vide
        }
        stor.objArbre.lstRestriction = stor.lstRestriction
        // je dois aussi récupérer l’arbre réponse
        stor.evts1Rep = []
        stor.evts2Rep = []
        for (let i = 0; i < stor.evtsProba['enonce' + stor.numeroExo].length; i++) {
          let PrbRep = remplaceCalcul({
            text: stor.evtsProba['enonce' + stor.numeroExo][i][1],
            obj: stor.obj
          })
          stor.evts1Rep.push([stor.evtsProba['enonce' + stor.numeroExo][i][0], PrbRep])
          stor.evts2Rep[i] = []
          for (let j = 0; j < stor.evtsProba['enonce' + stor.numeroExo][i][2].length; j++) {
            PrbRep = remplaceCalcul({
              text: stor.evtsProba['enonce' + stor.numeroExo][i][2][j][1],
              obj: stor.obj
            })
            stor.evts2Rep[i].push([stor.evtsProba['enonce' + stor.numeroExo][i][2][j][0], PrbRep])
          }
        }
        me.logIfDebug('stor.evts1Rep:', stor.evts1Rep, '\nstor.evts2Rep:', stor.evts2Rep)
        stor.objArbre.parcours = me
        construireArbreCond(stor.zoneArbre, stor.objArbre)
        break
      case 2:
        // on demande la proba d’une intersection
        if (ds.ecrireProbaInter) {
          j3pAffiche(zoneCons3, '', stor.questionInter['enonce' + stor.numeroExo])
          const zoneCons4 = j3pAddElt(stor.conteneur, 'div')
          elt = j3pAffiche(zoneCons4, '', '$' + stor.questionInterFin['enonce' + stor.numeroExo] + '=$&1&',
            { inputmq1: { texte: '' } })
        } else {
          elt = j3pAffiche(zoneCons3, '', stor.questionInter1['enonce' + stor.numeroExo],
            { inputmq1: { texte: '' } })
        }
        stor.zoneInput = elt.inputmqList[0]
        mqRestriction(stor.zoneInput, '\\d,.-')
        // J’ajoute ce commentaire suite à une remontée de bug en-dessous
        // console.log('numeroExo:', stor.numeroExo, stor.questionInter1['enonce' + stor.numeroExo].substring(0, 30) + '...')
        stor.zoneInput.typeReponse = ['nombre', 'exact']
        stor.zoneInput.reponse = [j3pCalculValeur(remplaceCalcul({
          text: stor.reponseInter['enonce' + stor.numeroExo],
          obj: stor.obj
        }))]
        stor.questionIntersection = true
        break
      case 3 :
        // on demande une proba avec formule des proba totales
        if (ds.ecrireProbaTotale) {
          j3pAffiche(zoneCons3, '', stor.questionPrbTotale['enonce' + stor.numeroExo])
          const zoneCons4 = j3pAddElt(stor.conteneur, 'div')
          elt = j3pAffiche(zoneCons4, '', '$' + stor.questionPrbTotaleFin['enonce' + stor.numeroExo] + '=$&1&',
            { inputmq1: { texte: '' } })
        } else {
          elt = j3pAffiche(zoneCons3, '', stor.questionPrbTotale1['enonce' + stor.numeroExo],
            { inputmq1: { texte: '' } })
        }
        stor.zoneInput = elt.inputmqList[0]
        mqRestriction(stor.zoneInput, '\\d,.-')
        stor.zoneInput.typeReponse = ['nombre', 'exact']
        stor.zoneInput.reponse = [j3pCalculValeur(remplaceCalcul({
          text: stor.reponsePrbTotale['enonce' + stor.numeroExo],
          obj: stor.obj
        }))]
        stor.questionTotale = true
        break
      default:
        // on demande une proba conditionnelle
        if (ds.ecrireProbaCond) {
          j3pAffiche(zoneCons3, '', stor.questionPrbSachant['enonce' + stor.numeroExo], {})
          const zoneCons4 = j3pAddElt(stor.conteneur, 'div')
          elt = j3pAffiche(zoneCons4, '', stor.questionPrbSachantFin['enonce' + stor.numeroExo] + ' = &1&',
            { inputmq1: { texte: '' } })
        } else {
          elt = j3pAffiche(zoneCons3, '', stor.questionPrbSachant1['enonce' + stor.numeroExo],
            { inputmq1: { texte: '' } })
        }
        stor.zoneInput = elt.inputmqList[0]
        mqRestriction(stor.zoneInput, '\\d,.-')
        stor.zoneInput.typeReponse = ['nombre', 'arrondi', Math.pow(10, -1 * Number(stor.arrondiCond['enonce' + stor.numeroExo]))]
        stor.zoneInput.reponse = [j3pCalculValeur(remplaceCalcul({
          text: stor.reponsePrbSachant['enonce' + stor.numeroExo],
          obj: stor.obj
        }))]

        break
    }
    if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] > 1) me.logIfDebug('proba cond :', stor.zoneInput.reponse)

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 1) {
      stor.zoneCorr = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
    } else {
      stor.zoneCorr = j3pAddElt(stor.zoneDroite, 'div', '', { style: { paddingTop: '10px' } })
    }
    if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] !== 1) {
      // Paramétrage de la validation
      // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
      j3pFocus(stor.zoneInput)
      stor.mesZonesSaisie = [stor.zoneInput]
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: stor.mesZonesSaisie
      })
    }
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite()
      }

      // OBLIGATOIRE

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 1) ? stor.zoneArbre.fctsValid : stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 1) {
          // Je récupère la lise des zones de saisie
          stor.mesZonesSaisie = [...stor.zoneArbre.mesZonesSaisie]
        }
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu
        // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_perso ont des réponses correctes ou non.
        // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
        // Si c’est la dernière tentative de répondre, on barre également les zones fausses
        let monEvt, testPremieresBranches, evtsPresents, PbNbBranches
        if (reponse.aRepondu) {
        // il faut aussi que je teste les deux zones dont la validation est perso
        // ce n’est valable que dans le cas où on demande l’arbre complet
          if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 1) {
            if (!ds.afficheEvt) {
            // c’est le cas de l’arbre à remplir complètement
            // La validation doit être gérée de manière personnelle
              const tabPremieresBranches = []// ce tableau contiendra des tableaux de deux éléments correspondant aux couples Evt/Prb des premières branches
              const tabSecondesBranches = []// ce tableau aura autant d’éléments que tabPremieresBranches.length, chaque élément étant un tableau des couples Evt/Prb des secondes branches
              for (let i = 0; i < stor.mesZonesSaisie.length; i += 2) {
              // je corrige d’éventuelles fautes de frappe (espace en trop, \overline{} en trop)
                const laReponse = $(stor.mesZonesSaisie[i]).mathquill('latex')
                const letexteReponse = reecrireEvt(laReponse)
                me.logIfDebug('letexteReponse:', letexteReponse, '   ', stor.mesZonesSaisie[i])
                if (stor.mesZonesSaisie[i].typeZone === 'evt1') {
                // c’est une zone pour un évt des premières branches
                // donc dans le tableau sur les premières branche, j’ajoute le tableau [evt, prb]
                  monEvt = simplifieEvt($(stor.mesZonesSaisie[i]).mathquill('latex'))
                  tabPremieresBranches.push([monEvt, j3pCalculValeur($(stor.mesZonesSaisie[i + 1]).mathquill('latex'))])
                  tabSecondesBranches.push([])
                } else {
                // c’est une zone sur les deuxièmes branches
                // j’identifie la première branche associée
                  const numPremBranche = stor.mesZonesSaisie[i].branchePrecedente // listeBtns
                  const monEvt2 = simplifieEvt($(stor.mesZonesSaisie[i]).mathquill('latex'))
                  tabSecondesBranches[numPremBranche].push([monEvt2, j3pCalculValeur($(stor.mesZonesSaisie[i + 1]).mathquill('latex'))])
                }
              }
              const bonNbPremBranches = (tabPremieresBranches.length === stor.evts1Rep.length)
              stor.nbPremieresBranches = tabPremieresBranches.length
              PbNbBranches = false
              if (bonNbPremBranches) {
              // je vérifie que le nombre de deuxièmes branches est correct
                let bonNbSecBranches = true
                for (let i = 0; i < tabPremieresBranches.length; i++) {
                  bonNbSecBranches = (bonNbSecBranches && (tabSecondesBranches[i].length === stor.evts2Rep[i].length))
                }
                if (!bonNbSecBranches) PbNbBranches = true
              } else {
                PbNbBranches = true
              }

              me.logIfDebug('tabPremieresBranches:', tabPremieresBranches, '\ntabSecondesBranches:', tabSecondesBranches)

              if (PbNbBranches) {
                reponse.bonneReponse = false
                for (let i = 0; i < stor.mesZonesSaisie.length; i++) fctsValid.zones.bonneReponse[i] = false
              } else {
              // on vérifie les couple evt/prb et les secondes branches associées
                testPremieresBranches = egaliteTableauPrb(tabPremieresBranches, stor.evts1Rep)
                reponse.bonneReponse = testPremieresBranches.egalite
                evtsPresents = bonEvts(tabPremieresBranches, stor.listeEvt['enonce' + stor.numeroExo])// ce booléen vérifie si les événements donnés pas l’élèves sont (ou non) dans la liste des événements de l’exo
                if (reponse.bonneReponse) {
                // je regarde si les autres branches sont bonnes et bien associées aux premières
                  const bonnesSecondesBranches = []
                  for (let i = 0; i < tabPremieresBranches.length; i++) {
                    me.logIfDebug(
                      'testPremieresBranches.positionTab[i]:', testPremieresBranches.positionTab[i],
                      '\ntestPremieresBranches:', testPremieresBranches, testPremieresBranches.positionTab[i],
                      '\nstor.objArbreRep.evts2["branche"+testPremieresBranches.positionTab[i]]:', stor.objArbreRep.evts2['branche' + testPremieresBranches.positionTab[i]],
                      '\ntabSecondesBranches[i]:', tabSecondesBranches[i]
                    )
                    bonnesSecondesBranches[i] = egaliteTableauPrb(tabSecondesBranches[i], stor.objArbreRep.evts2['branche' + testPremieresBranches.positionTab[i]]).egalite
                    reponse.bonneReponse = (reponse.bonneReponse && bonnesSecondesBranches[i])
                    evtsPresents = (evtsPresents && bonEvts(tabSecondesBranches[i], stor.listeEvt['enonce' + stor.numeroExo]))
                  }
                  if (!reponse.bonneReponse) {
                    let posZoneSecondesBranches = tabPremieresBranches.length * 2
                    for (let i = 0; i < tabPremieresBranches.length; i++) {
                      if (!bonnesSecondesBranches[i]) {
                        for (let k = 0; k < tabSecondesBranches[i].length; k++) {
                          fctsValid.zones.bonneReponse[posZoneSecondesBranches] = false
                          fctsValid.zones.bonneReponse[posZoneSecondesBranches + 1] = false
                          posZoneSecondesBranches += 2
                        }
                      } else {
                        posZoneSecondesBranches += 2 * tabSecondesBranches[i].length
                      }
                    }
                  }
                } else {
                  for (let i = 0; i < stor.mesZonesSaisie.length; i++) {
                    fctsValid.zones.bonneReponse[i] = false
                  }
                }
              }
              for (const zone of stor.mesZonesSaisie) fctsValid.coloreUneZone(zone)
            } else {
            // il faut juste que je gère des variables inexistantes à l’heures actuelles et dont je me sers dans mes tests ensuite
              testPremieresBranches = {}
              testPremieresBranches.egalite = true
              for (let i = 0; i < stor.mesZonesSaisie.length; i++) {
                if (stor.mesZonesSaisie[i].typeZone === 'prb1') {
                  testPremieresBranches.egalite = (testPremieresBranches.egalite && fctsValid.zones.bonneReponse[i])
                }
              }
              evtsPresents = true
            }
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          // focus sur la 1re zone mathquill vide
          stor.mesZonesSaisie.some(function (zoneSaisie) {
            if ($(zoneSaisie).mathquill('latex') === '') {
              j3pFocus(zoneSaisie)
              return true // ça arrête la boucle some
            }
            return false
          })
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            j3pAddContent(stor.zoneCorr, cBien, { replace: true })
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              j3pAddContent(stor.zoneCorr, tempsDepasse, { replace: true })
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              j3pAddContent(stor.zoneCorr, cFaux, { replace: true })
              // indication éventuelle ici
              if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 1) {
              // Dans le cas de l’arbre
                if (PbNbBranches) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
                } else if (!testPremieresBranches.egalite) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment3
                } else {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
                }
                if (!evtsPresents) {
                  stor.zoneCorr.innerHTML += ' ' + ds.textes.comment4
                }
              } else if (ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes] === 4) {
              // c’est le cas avec la proba conditionnelles où la résultat attenu doit être arrondi
              // je vérifie la possibilité que l’erreur soit un problème d’arrondi
                const larrondi = Number(stor.arrondiCond['enonce' + stor.numeroExo])
                const laRep = fctsValid.zones.inputs[0].reponse[0]
                const laRepEleve = j3pNombre(fctsValid.zones.reponseSaisie[0])
                me.logIfDebug('larrondi:', larrondi, '   laRep:', laRep, '  laRepEleve:', laRepEleve)
                if (Math.abs(laRep - laRepEleve) < Math.pow(10, -larrondi + 1)) {
                  stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment5
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                j3pAddContent(stor.zoneCorr, '<br>' + essaieEncore)
                me.typederreurs[1]++

              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                j3pAddContent(stor.zoneCorr, '<br>' + regardeCorrection)
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                // je récupère le nombre de faute de chaque sorte (arbre [4], intersection [5], proba totale [6], proba cond [7])
                me.typederreurs[3 + ds.typeQuestions[(me.questionCourante - 1) % ds.nbetapes]]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          if (me.typederreurs[4] / ds.nbrepetitions >= ds.seuilErreur) {
            // pb sur l’arbre
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[5] / ds.nbrepetitions >= ds.seuilErreur) {
            // pb sur la proba de l’intersection
            me.parcours.pe = ds.pe_3
          } else if (me.typederreurs[6] / ds.nbrepetitions >= ds.seuilErreur) {
            // pb sur la formule des proba totales
            me.parcours.pe = ds.pe_4
          } else if (me.typederreurs[7] / ds.nbrepetitions >= ds.seuilErreur) {
            // pb sur la proba conditionnelle
            me.parcours.pe = ds.pe_5
          } else {
            // autre souci mais non identifié
            me.parcours.pe = ds.pe_6
          }
        }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
