import { j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pPaletteMathquill, j3pPGCD } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pMasqueFenetre, j3pDetruitToutesLesFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { construireArbreCond } from 'src/legacy/outils/arbreProba/arbreProba'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Deniaud Rémi
        avril 2021
        Dans cette section, on demande de compléter un arbre pondéré (sans parler de probabilités conditionnelles, nous sommes au niveau 2nde)
        Dans un second temps, on demande un calcul de proba
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    // explications contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    for (let i = 1; i <= 3; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div', '')
    if (me.questionCourante % ds.nbetapes === 1) {
      for (let i = 0; i < stor.obj.evts1.length; i++) {
        stor.obj['p' + (i + 1)] = stor.obj.prb_choix1[i]
      }
      j3pAffiche(stor.zoneexplication1, '', ds.textes['corr1_' + stor.quest], stor.obj)
      j3pAffiche(stor.zoneexplication2, '', ds.textes['corr2_' + stor.quest], stor.obj)
      j3pEmpty(stor.arbre.laPalette)
      if (!bonneReponse) {
        // je construis une autre fenêtre pour y mettre la correction
        const explicationArbre = j3pAddElt(stor.idDetailFenetre, 'div', '', { style: me.styles.toutpetit.explications })
        j3pToggleFenetres(stor.idCorrDetails)
        stor.objArbreRep.parcours = me
        construireArbreCond(explicationArbre, stor.objArbreRep)
        j3pAffiche(stor.zoneexplication3, '', ds.textes.corr3)
      }
    } else {
      j3pDetruit(stor.zoneCons8)
      const numTxtCorr1 = (stor.numetape === 2) ? 5 : 8
      const numTxtCorr2 = (stor.numetape === 2) ? 6 : 9
      j3pAffiche(stor.zoneexplication1, '', ds.textes['corr' + numTxtCorr1 + '_' + stor.quest][stor.numcons2])
      j3pAffiche(stor.zoneexplication2, '', ds.textes['corr' + numTxtCorr2])
      j3pAffiche(stor.zoneexplication3, '', ds.textes.corr7, {
        c: stor.objArbreRep['calcQ' + stor.numetape][stor.numcons2],
        r: stor.objArbreRep['repFracQ' + stor.numetape][stor.numcons2]
      })
    }
  }
  function fracSimplifiee (a, b) {
    // Cette fonction renvoie au format latex la fraction simplifiée de la forme \\frac{a}{b} (a et b étant positifs)
    return '\\frac{' + a / j3pPGCD(a, b) + '}{' + b / j3pPGCD(a, b) + '}'
  }
  function genereDonnees (num) {
    // Cette fonction renvoie les données (sous la forme d’un objet) dont on a besoin pour chaque cas de figure
    const obj = {}
    const evts1 = ds.textes['nom_evts' + num].split(',')
    if (num === 1) {
      obj.b = j3pGetRandomInt(3, 5)
      obj.v = j3pGetRandomInt(2, 4)
      obj.n = obj.b + obj.v + 1 // Je considère qu’il y a un rouge
      obj.prb_choix1 = [fracSimplifiee(1, obj.n), fracSimplifiee(obj.b, obj.n), fracSimplifiee(obj.v, obj.n)]
      obj.prbval_choix1 = [1 / obj.n, obj.b / obj.n, obj.v / obj.n]
      obj.prb_choix2 = []
      obj.prb_choix2.push([fracSimplifiee(obj.b, obj.n - 1), fracSimplifiee(obj.v, obj.n - 1)])
      obj.prb_choix2.push([fracSimplifiee(1, obj.n - 1), fracSimplifiee(obj.b - 1, obj.n - 1), fracSimplifiee(obj.v, obj.n - 1)])
      obj.prb_choix2.push([fracSimplifiee(1, obj.n - 1), fracSimplifiee(obj.b, obj.n - 1), fracSimplifiee(obj.v - 1, obj.n - 1)])
      obj.prbval_choix2 = [
        [obj.b / (obj.n - 1), obj.v / (obj.n - 1)],
        [1 / (obj.n - 1), (obj.b - 1) / (obj.n - 1), obj.v / (obj.n - 1)],
        [1 / (obj.n - 1), obj.b / (obj.n - 1), (obj.v - 1) / (obj.n - 1)]
      ]
      obj.evts2 = {}
      obj.evts2.branche0 = [
        [evts1[1], obj.prb_choix2[0][0]],
        [evts1[2], obj.prb_choix2[0][1]]
      ]
      ;[1, 2].forEach(i => {
        obj.evts2['branche' + i] = [
          [evts1[0], obj.prb_choix2[i][0]],
          [evts1[1], obj.prb_choix2[i][1]],
          [evts1[2], obj.prb_choix2[i][2]]
        ]
      })
      // Pour la question 2, on a deux types de réponses possibles
      obj.calcQ2 = [
        obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1],
        obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][2]
      ]
      obj.repQ2 = [
        obj.prbval_choix1[1] * obj.prbval_choix2[1][1],
        obj.prbval_choix1[2] * obj.prbval_choix2[2][2]
      ]
      obj.repFracQ2 = [
        j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1]),
        j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][2])
      ]
      obj.calcQ3 = [
        obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][2],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][1] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][0],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][0] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][0] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][2] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][1],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][1] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][2] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][0] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][1] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][2]
      ]
      obj.repQ3 = [
        obj.prbval_choix1[1] * obj.prbval_choix2[1][1] + obj.prbval_choix1[2] * obj.prbval_choix2[2][2],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][1] + obj.prbval_choix1[2] * obj.prbval_choix2[2][0],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][0] + obj.prbval_choix1[1] * obj.prbval_choix2[1][0],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][0] + obj.prbval_choix1[1] * obj.prbval_choix2[1][0] + obj.prbval_choix1[1] * obj.prbval_choix2[1][1] + obj.prbval_choix1[1] * obj.prbval_choix2[1][2] + obj.prbval_choix1[2] * obj.prbval_choix2[2][1],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][1] + obj.prbval_choix1[1] * obj.prbval_choix2[1][2] + obj.prbval_choix1[2] * obj.prbval_choix2[2][0] + obj.prbval_choix1[2] * obj.prbval_choix2[2][1] + obj.prbval_choix1[2] * obj.prbval_choix2[2][2]
      ]
      obj.repFracQ3 = [
        j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1]), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][2])),
        j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][1]), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][0])),
        j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][0]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0])),
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][0]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0])), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1])), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][2])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][1])),
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][1]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][2])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][0])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][1])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][2]))
      ]
    } else if (num === 2) {
      do {
        obj.r = j3pGetRandomInt(2, 4)
        obj.b1 = j3pGetRandomInt(2, 4)
        obj.r1 = j3pGetRandomInt(2, 4)
      } while (obj.r === obj.b1 && obj.b1 === obj.r1)
      obj.n = obj.r + obj.b1 + obj.r1
      do {
        obj.b2 = j3pGetRandomInt(2, 5)
        obj.r2 = j3pGetRandomInt(2, 4)
      } while (obj.b2 === obj.r2)
      obj.j = obj.b2 + obj.r2
      obj.prb_choix1 = [fracSimplifiee(obj.r, obj.n), fracSimplifiee(obj.b1, obj.n), fracSimplifiee(obj.r1, obj.n)]
      obj.p4 = fracSimplifiee(obj.b2, obj.j)
      obj.p5 = fracSimplifiee(obj.r2, obj.j)
      obj.prbval_choix1 = [obj.r / obj.n, obj.b1 / obj.n, obj.r1 / obj.n]
      obj.prb_choix2 = []
      obj.prb_choix2.push([fracSimplifiee(obj.b2, obj.j), fracSimplifiee(obj.r2, obj.j)])
      obj.prb_choix2.push(obj.prb_choix2[0])
      obj.prb_choix2.push(obj.prb_choix2[0])
      obj.prbval_choix2 = [
        [obj.b2 / obj.j, obj.r2 / obj.j],
        [obj.b2 / obj.j, obj.r2 / obj.j],
        [obj.b2 / obj.j, obj.r2 / obj.j]
      ]
      obj.evts2 = {}
      ;[0, 1, 2].forEach(i => {
        obj.evts2['branche' + i] = [
          [evts1[1], obj.prb_choix2[i][0]],
          [evts1[2], obj.prb_choix2[i][1]]
        ]
      })
      // Pour la question 2, on a deux types de réponses possibles
      obj.calcQ2 = [
        obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0],
        obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][1]
      ]
      obj.repQ2 = [
        obj.prbval_choix1[1] * obj.prbval_choix2[1][0],
        obj.prbval_choix1[2] * obj.prbval_choix2[2][1]
      ]
      obj.repFracQ2 = [
        j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0]),
        j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][1])
      ]
      obj.calcQ3 = [
        obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][1],
        obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][0],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][0] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][0],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][1] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][0] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][1]
      ]
      obj.repQ3 = [
        obj.prbval_choix1[1] * obj.prbval_choix2[1][0] + obj.prbval_choix1[2] * obj.prbval_choix2[2][1],
        obj.prbval_choix1[1] * obj.prbval_choix2[1][1] + obj.prbval_choix1[2] * obj.prbval_choix2[2][0],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][0] + obj.prbval_choix1[1] * obj.prbval_choix2[1][0] + obj.prbval_choix1[1] * obj.prbval_choix2[1][1] + obj.prbval_choix1[2] * obj.prbval_choix2[2][0],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][1] + obj.prbval_choix1[1] * obj.prbval_choix2[1][1] + obj.prbval_choix1[2] * obj.prbval_choix2[2][0] + obj.prbval_choix1[2] * obj.prbval_choix2[2][1]
      ]
      obj.repFracQ3 = [
        j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0]), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][1])),
        j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1]), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][0])),
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][0]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0])), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][0])),
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][1]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][0])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][1]))
      ]
    } else if (num === 3) {
      obj.s1 = 's'
      obj.s2 = 's'
      obj.s3 = 's'
      do {
        obj.n = j3pGetRandomInt(2, 5)
        obj.r = j3pGetRandomInt(1, 4)
        obj.b = j3pGetRandomInt(1, 5)
      } while (obj.n === obj.r && obj.r === obj.b)
      if (obj.r === 1) obj.s1 = ''
      if (obj.b === 1) obj.s3 = ''
      obj.t = obj.n + obj.r + obj.b
      obj.prb_choix1 = [fracSimplifiee(obj.r, obj.t), fracSimplifiee(obj.n, obj.t), fracSimplifiee(obj.b, obj.t)]
      obj.prbval_choix1 = [obj.r / obj.t, obj.n / obj.t, obj.b / obj.t]
      obj.prb_choix2 = []
      obj.prb_choix2.push(obj.prb_choix1)
      obj.prb_choix2.push(obj.prb_choix2[0])
      obj.prb_choix2.push(obj.prb_choix2[0])
      obj.prbval_choix2 = [
        obj.prbval_choix1,
        obj.prbval_choix1,
        obj.prbval_choix1
      ]
      obj.evts2 = {}
      ;[0, 1, 2].forEach(i => {
        obj.evts2['branche' + i] = [
          [evts1[0], obj.prb_choix2[i][0]],
          [evts1[1], obj.prb_choix2[i][1]],
          [evts1[2], obj.prb_choix2[i][2]]
        ]
      })
      // Pour la question 2, on a trois types de réponses possibles
      obj.calcQ2 = [
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][0],
        obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1],
        obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][2]
      ]
      obj.repQ2 = [
        obj.prbval_choix1[0] * obj.prbval_choix2[0][0],
        obj.prbval_choix1[1] * obj.prbval_choix2[1][1],
        obj.prbval_choix1[2] * obj.prbval_choix2[2][2]
      ]
      obj.repFracQ2 = [
        j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][0]),
        j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1]),
        j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][2])
      ]
      obj.calcQ3 = [
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][0] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][2],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][1] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][2] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][0],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][1] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][2] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][1],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][0] + ' + ' + obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][1] + ' + ' + obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][2] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][0]
      ]
      obj.repQ3 = [
        obj.prbval_choix1[0] * obj.prbval_choix2[0][0] + obj.prbval_choix1[1] * obj.prbval_choix2[1][1] + obj.prbval_choix1[2] * obj.prbval_choix2[2][2],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][1] + obj.prbval_choix1[1] * obj.prbval_choix2[1][0],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][2] + obj.prbval_choix1[2] * obj.prbval_choix2[2][0],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][1] + obj.prbval_choix1[1] * obj.prbval_choix2[1][0] + obj.prbval_choix1[1] * obj.prbval_choix2[1][1] + obj.prbval_choix1[1] * obj.prbval_choix2[1][2] + obj.prbval_choix1[2] * obj.prbval_choix2[2][1],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][0] + obj.prbval_choix1[0] * obj.prbval_choix2[0][1] + obj.prbval_choix1[0] * obj.prbval_choix2[0][2] + obj.prbval_choix1[1] * obj.prbval_choix2[1][0] + obj.prbval_choix1[2] * obj.prbval_choix2[2][0]
      ]
      obj.repFracQ3 = [
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][0]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][2])),
        j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][1]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0])),
        j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][2]), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][0])),
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][1]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0])), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1])), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][2])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][1])),
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][0]), j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][1])), j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][2])), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][0]))
      ]
    } else if (num === 4) {
      do {
        obj.a = j3pGetRandomInt(2, 5)
        obj.p = j3pGetRandomInt(3, 7)
        obj.s = j3pGetRandomInt(2, 4)
      } while (obj.a === obj.p && obj.p === obj.s)
      obj.t = obj.a + obj.p + obj.s
      obj.prb_choix1 = [fracSimplifiee(obj.a, obj.t), fracSimplifiee(obj.p, obj.t), fracSimplifiee(obj.s, obj.t)]
      obj.prbval_choix1 = [obj.a / obj.t, obj.p / obj.t, obj.s / obj.t]
      obj.prb_choix2 = []
      obj.prb_choix2.push([fracSimplifiee(obj.a - 1, obj.t - 1), fracSimplifiee(obj.p, obj.t - 1), fracSimplifiee(obj.s, obj.t - 1)])
      obj.prb_choix2.push([fracSimplifiee(obj.a, obj.t - 1), fracSimplifiee(obj.p - 1, obj.t - 1), fracSimplifiee(obj.s, obj.t - 1)])
      obj.prb_choix2.push([fracSimplifiee(obj.a, obj.t - 1), fracSimplifiee(obj.p, obj.t - 1), fracSimplifiee(obj.s - 1, obj.t - 1)])
      obj.prbval_choix2 = [
        [(obj.a - 1) / (obj.t - 1), obj.p / (obj.t - 1), obj.s / (obj.t - 1)],
        [obj.a / (obj.t - 1), (obj.p - 1) / (obj.t - 1), obj.s / (obj.t - 1)],
        [obj.a / (obj.t - 1), obj.p / (obj.t - 1), (obj.s - 1) / (obj.t - 1)]
      ]
      obj.evts2 = {}
      ;[0, 1, 2].forEach(i => {
        obj.evts2['branche' + i] = [
          [evts1[0], obj.prb_choix2[i][0]],
          [evts1[1], obj.prb_choix2[i][1]],
          [evts1[2], obj.prb_choix2[i][2]]
        ]
      })
      // Pour la question 2, on a trois types de réponses possibles
      obj.calcQ2 = [
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][0],
        obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1],
        obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][2]
      ]
      obj.repQ2 = [
        obj.prbval_choix1[0] * obj.prbval_choix2[0][0],
        obj.prbval_choix1[1] * obj.prbval_choix2[1][1],
        obj.prbval_choix1[2] * obj.prbval_choix2[2][2]
      ]
      obj.repFracQ2 = [
        j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][0]),
        j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1]),
        j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][2])
      ]
      obj.calcQ3 = [
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][0] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][2],
        obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][2] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][1],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][1] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][1] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][2] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][1],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][2] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][2] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][0] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][1] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][2]
      ]
      obj.repQ3 = [
        obj.prbval_choix1[0] * obj.prbval_choix2[0][0] + obj.prbval_choix1[1] * obj.prbval_choix2[1][1] + obj.prbval_choix1[2] * obj.prbval_choix2[2][2],
        obj.prbval_choix1[1] * obj.prbval_choix2[1][2] + obj.prbval_choix1[2] * obj.prbval_choix2[2][1],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][1] + obj.prbval_choix1[1] * obj.prbval_choix2[1][0],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][1] + obj.prbval_choix1[1] * obj.prbval_choix2[1][0] + obj.prbval_choix1[1] * obj.prbval_choix2[1][1] + obj.prbval_choix1[1] * obj.prbval_choix2[1][2] + obj.prbval_choix1[2] * obj.prbval_choix2[2][1],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][2] + obj.prbval_choix1[1] * obj.prbval_choix2[1][2] + obj.prbval_choix1[2] * obj.prbval_choix2[2][0] + obj.prbval_choix1[2] * obj.prbval_choix2[2][1] + obj.prbval_choix1[2] * obj.prbval_choix2[2][2]
      ]
      obj.repFracQ3 = [
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][0]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][2])),
        j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][2]), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][1])),
        j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][1]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0])),
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][1]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0])), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1])), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][2])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][1])),
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][2]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][2])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][0])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][1])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][2]))
      ]
    } else if (num === 5) {
      obj.n = j3pGetRandomInt(12, 16)
      do {
        obj.l = j3pGetRandomInt(2, 5)
        obj.j = j3pGetRandomInt(3, 7)
      } while (obj.l === obj.j || obj.n - obj.l - obj.j < 2)
      obj.t = obj.n - obj.j - obj.l
      obj.prb_choix1 = [fracSimplifiee(obj.l, obj.n), fracSimplifiee(obj.j, obj.n), fracSimplifiee(obj.t, obj.n)]
      obj.prbval_choix1 = [obj.l / obj.n, obj.j / obj.n, obj.t / obj.n]
      obj.prb_choix2 = []
      obj.prb_choix2.push([fracSimplifiee(obj.l - 1, obj.n - 1), fracSimplifiee(obj.j, obj.n - 1), fracSimplifiee(obj.t, obj.n - 1)])
      obj.prb_choix2.push([fracSimplifiee(obj.l, obj.n - 1), fracSimplifiee(obj.j - 1, obj.n - 1), fracSimplifiee(obj.t, obj.n - 1)])
      obj.prb_choix2.push([fracSimplifiee(obj.l, obj.n - 1), fracSimplifiee(obj.j, obj.n - 1), fracSimplifiee(obj.t - 1, obj.n - 1)])
      obj.prbval_choix2 = [
        [(obj.l - 1) / (obj.n - 1), obj.j / (obj.n - 1), obj.t / (obj.n - 1)],
        [obj.l / (obj.n - 1), (obj.j - 1) / (obj.n - 1), obj.t / (obj.n - 1)],
        [obj.l / (obj.n - 1), obj.j / (obj.n - 1), (obj.t - 1) / (obj.n - 1)]
      ]
      obj.evts2 = {}
      ;[0, 1, 2].forEach(i => {
        obj.evts2['branche' + i] = [
          [evts1[0], obj.prb_choix2[i][0]],
          [evts1[1], obj.prb_choix2[i][1]],
          [evts1[2], obj.prb_choix2[i][2]]
        ]
      })
      // Pour la question 2, on a trois types de réponses possibles
      obj.calcQ2 = [
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][0],
        obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1],
        obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][2]
      ]
      obj.repQ2 = [
        obj.prbval_choix1[0] * obj.prbval_choix2[0][0],
        obj.prbval_choix1[1] * obj.prbval_choix2[1][1],
        obj.prbval_choix1[2] * obj.prbval_choix2[2][2]
      ]
      obj.repFracQ2 = [
        j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][0]),
        j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1]),
        j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][2])
      ]
      obj.calcQ3 = [
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][0] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][1] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][2],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][1] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0],
        obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][2] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][1],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][0] + ' + ' + obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][1] + ' + ' + obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][2] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][0] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][0],
        obj.prb_choix1[0] + '\\times ' + obj.prb_choix2[0][2] + ' + ' + obj.prb_choix1[1] + '\\times ' + obj.prb_choix2[1][2] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][0] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][1] + ' + ' + obj.prb_choix1[2] + '\\times ' + obj.prb_choix2[2][2]
      ]
      obj.repQ3 = [
        obj.prbval_choix1[0] * obj.prbval_choix2[0][0] + obj.prbval_choix1[1] * obj.prbval_choix2[1][1] + obj.prbval_choix1[2] * obj.prbval_choix2[2][2],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][1] + obj.prbval_choix1[1] * obj.prbval_choix2[1][0],
        obj.prbval_choix1[1] * obj.prbval_choix2[1][2] + obj.prbval_choix1[2] * obj.prbval_choix2[2][1],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][0] + obj.prbval_choix1[0] * obj.prbval_choix2[0][1] + obj.prbval_choix1[0] * obj.prbval_choix2[0][2] + obj.prbval_choix1[1] * obj.prbval_choix2[1][0] + obj.prbval_choix1[2] * obj.prbval_choix2[2][0],
        obj.prbval_choix1[0] * obj.prbval_choix2[0][2] + obj.prbval_choix1[1] * obj.prbval_choix2[1][2] + obj.prbval_choix1[2] * obj.prbval_choix2[2][0] + obj.prbval_choix1[2] * obj.prbval_choix2[2][1] + obj.prbval_choix1[2] * obj.prbval_choix2[2][2]
      ]
      obj.repFracQ3 = [
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][0]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][1])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][2])),
        j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][1]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0])),
        j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][2]), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][1])),
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][0]), j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][1])), j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][2])), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][0])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][0])),
        j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(obj.prb_choix1[0], obj.prb_choix2[0][2]), j3pGetLatexProduit(obj.prb_choix1[1], obj.prb_choix2[1][2])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][0])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][1])), j3pGetLatexProduit(obj.prb_choix1[2], obj.prb_choix2[2][2]))
      ]
    }
    obj.evts1 = []
    evts1.forEach((evt, index) => {
      obj.evts1.push([evt, obj.prb_choix1[index]])
    })
    return obj
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 3,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Arbre de probabilités',
        // on donne les phrases de la consigne
        consigne1_11: 'Sonia dispose de £n crayons de couleur dans sa trousse : 1 rouge, £b bleus et les autres verts.',
        consigne1_12: 'Elle choisit au hasard deux crayons successivement et s’intéresse à leurs couleurs.',
        consigne1_13: 'Cette situation peut être représentée par un arbre de probabilités où R, B et V désignent respectivement les couleurs rouge, bleu et vert du crayon choisi.',
        consigne1_14: 'Compléter l’arbre ci-contre par les probabilités d’obtenir chaque couleur au cours de chacun des tirages.',
        consigne1_21: 'Dans son armoire, Louise a rangé ses £n tee-shirts (£r rouges, £{b1} blancs et £{r1} noirs) et ses £j jupes (£{b2} blanches et £{r2} noires).',
        consigne1_22: 'Ce matin, elle choisit au hasard un tee-shirt et une jupe.',
        consigne1_23: 'Cette situation peut être représentée par un arbre de probabilités où R, B et N désignent respectivement les couleurs rouge, blanc et noir de chaque vêtement.',
        consigne1_24: 'Compléter l’arbre ci-contre par les probabilités d’obtenir chaque couleur au cours de son choix de vêtements.',
        consigne1_31: 'Une urne opaque contient des boules  indiscernables au toucher : £r rouge£{s1}, £n noire£{s2} et £b blanche£{s3}.',
        consigne1_32: 'On tire successivement et avec remise deux boules de cette urne.',
        consigne1_33: 'Cette situation peut être représentée par un arbre de probabilités où R, N et B désignent respectivement les couleurs rouge, noir et blanc des boules.',
        consigne1_34: 'Compléter l’arbre ci-contre par les probabilités d’obtenir chaque couleur au cours de son tirage de deux boules.',
        consigne1_41: 'Chez Alban, la bibliothèque familiale contient £a romans d’aventures, £p policiers et £s livres de science fiction.',
        consigne1_42: 'Il prévoit pour ses vacances de lire deux livres issus de cette bibliothèque et les choisit au hasard.',
        consigne1_43: 'Cette situation peut être représentée par un arbre de probabilités où A, P et S désignent respectivement les romans d’aventure, les policiers et les livres de science fiction.',
        consigne1_44: 'Compléter l’arbre ci-contre par les probabilités d’obtenir les différents types de livres.',
        consigne1_51: '£n enfants sont invités à l’anniversaire de Gabin, £l d’entre eux ont choisi d’offrir un livre, £j un jeu, les autres ayant opté pour un objet lié aux nouvelles technologies.',
        consigne1_52: 'Gabin choisit au hasard les deux premiers cadeaux qu’il va ouvrir.',
        consigne1_53: 'Cette situation peut être représentée par un arbre de probabilités où L, J et T désignent le fait d’avoir un livre, un jeu ou un objet lié aux nouvelles technologies.',
        consigne1_54: 'Compléter l’arbre ci-contre par les probabilités de découvrir les différents types de cadeaux.',
        nom_evts1: 'R,B,V',
        nom_evts2: 'R,B,N',
        nom_evts3: 'R, N, B',
        nom_evts4: 'A, P, S',
        nom_evts5: 'L, J, T',
        formatPrb: 'Écrire les valeurs exactes des probabilités au format décimal ou de fractions irréductibles&nbsp;!',
        consigne2: 'On rappelle l’arbre des probabilités obtenu.',
        consigne3_1: [
          'La probabilité qu’elle choisisse deux crayons bleus vaut&nbsp;:',
          'La probabilité qu’elle choisisse deux crayons verts vaut&nbsp;:'
        ],
        consigne3_2: [
          'La probabilité qu’elle choisisse un tee-shirt et une jupe blancs vaut&nbsp;:',
          'La probabilité qu’elle choisisse un tee-shirt et une jupe noirs vaut&nbsp;:'
        ],
        consigne3_3: [
          'La probabilité de tirer deux boules rouges vaut&nbsp;:',
          'La probabilité de tirer deux boules noires vaut&nbsp;:',
          'La probabilité de tirer deux boules blanches vaut&nbsp;:'
        ],
        consigne3_4: [
          'La probabilité qu’il parte en vacances avec 2 romans d’aventure vaut&nbsp;:',
          'La probabilité qu’il parte en vacances avec 2 policiers vaut&nbsp;:',
          'La probabilité qu’il parte en vacances avec 2 livres de science fiction vaut&nbsp;:'
        ],
        consigne3_5: [
          'La probabilité qu’il découvre deux livres vaut&nbsp;:',
          'La probabilité qu’il découvre deux jeux vaut&nbsp;:',
          'La probabilité qu’il découvre deux objets liés aux nouvelles technologies vaut&nbsp;:'
        ],
        consigne4_1: [
          'La probabilité qu’elle choisisse deux crayons de la même couleur vaut&nbsp;:',
          'La probabilité qu’elle se retrouve à la fin avec un vert et un rouge (peu importe l’ordre) vaut&nbsp;:',
          'La probabilité qu’elle se retrouve à la fin avec un bleu et un rouge (peu importe l’ordre) vaut&nbsp;:',
          'La probabilité qu’elle ait au moins un bleu vaut&nbsp;:',
          'La probabilité qu’elle ait au moins un vert vaut&nbsp;:'
        ],
        consigne4_2: [
          'La probabilité qu’elle choisisse un tee-shirt et une jupe de la même couleur vaut&nbsp;:',
          'La probabilité qu’elle se retrouve à la fin avec un vêtement blanc et l’autre noir (peu importe lesquels) vaut&nbsp;:',
          'La probabilité qu’elle ait au moins un de ces deux vêtements blanc vaut&nbsp;:',
          'La probabilité qu’elle ait au moins un de ces deux vêtements noir vaut&nbsp;:'
        ],
        consigne4_3: [
          'La probabilité de tirer deux boules de la même couleur vaut&nbsp;:',
          'La probabilité de tirer une noire et une rouge (peu importe l’ordre) vaut&nbsp;:',
          'La probabilité de tirer une rouge et une blanche (peu importe l’ordre) vaut&nbsp;:',
          'La probabilité de tirer au moins une boule noire vaut&nbsp;:',
          'La probabilité de tirer au moins une boule rouge vaut&nbsp;:'
        ],
        consigne4_4: [
          'La probabilité qu’il choisisse deux livres du même genre vaut&nbsp;:',
          'La probabilité qu’il parte en vacances avec un policier et un livre de science fiction vaut&nbsp;:',
          'La probabilité qu’il parte en vacances avec un policier et un roman d’aventure vaut&nbsp;:',
          'La probabilité qu’il choisisse au moins un policier vaut&nbsp;:',
          'La probabilité qu’il choisisse au moins un livre de science fiction vaut&nbsp;:'
        ],
        consigne4_5: [
          'La probabilité qu’il découvre deux cadeaux du même style vaut&nbsp;:',
          'La probabilité qu’il ait un jeu et un livre (peu importe l’ordre) vaut&nbsp;:',
          'La probabilité qu’il ait un jeu et un objet lié aux nouvelles technologies (peu importe l’ordre) vaut&nbsp;:',
          'La probabilité qu’il ait au moins un livre vaut&nbsp;:',
          'La probabilité qu’il ait au moins un objet lié aux nouvelles technologies vaut&nbsp;:'
        ],
        formatRep1: 'Le résultat donné devra être exact sous la forme d’une fraction irréductible ou d’un décimal, mais il est possible de tester ses calculs (encore 2 tests possibles).',
        formatRep2: 'Le résultat donné devra être exact sous la forme d’une fraction irréductible ou d’un décimal, mais il est possible de tester ses calculs (encore un test possible).',
        formatRep3: 'Le résultat donné devra être exact sous la forme d’une fraction irréductible ou d’un décimal (plus aucun test n’est possible).',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Il faut tout de même penser à simplifier toutes les fractions&nbsp;!',
        comment2: 'Le calcul proposé est correct, mais cette réponse doit être simplifiée&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'Elle choisit un premier crayon rouge, bleu ou vert avec les probabilités respectives $£{p1}$, $£{p2}$ et $£{p3}$.',
        corr2_1: 'Pour le second choix, elle n’a plus que calcule[£n-1] crayons, c’est pourquoi les probabilités sur les secondes branches sont différentes.',
        corr1_2: 'Elle choisit en premier un tee-shirt rouge, blanc ou noir avec les probabilités respectives $£{p1}$, $£{p2}$ et $£{p3}$.',
        corr2_2: 'Pour la jupe, elle n’a que deux couleurs : blanc avec une probabilité $£{p4}$ et noir avec une probabilité $£{p5}$.',
        corr1_3: 'On tire en premier une boule rouge, noire ou blanche avec les probabilités respectives $£{p1}$, $£{p2}$ et $£{p3}$.',
        corr2_3: 'Comme on met la boule tirée dans l’urne, les probabilités de tirer une boule rouge, noire ou blanche au deuxième tirage restent les mêmes.',
        corr1_4: 'Il choisit un premier livre parmi les romans d’aventure, les policiers et les livres de science fiction avec les probabilités respectives $£{p1}$, $£{p2}$ et $£{p3}$.',
        corr2_4: 'Pour le second choix, il a un livre de moins dans la bibliothèque, c’est pourquoi les probabilités sur les secondes branches sont différentes.',
        corr1_5: 'Le premier cadeau ouvert sera un livre, un jeu ou un objet lié aux nouvelles technologies avec les probabilités respectives $£{p1}$, $£{p2}$ et $£{p3}$.',
        corr2_5: 'Pour le second cadeau, il y en aura un de moins, l’un d’eux ayant été découvert, c’est pourquoi les probabilités sur les secondes branches sont différentes.',
        corr3: 'Voici l’arbre attendu :',
        corr4: 'Arbre solution',
        corr5_1: [
          'Sur l’arbre, il existe un chemin permettant d’obtenir deux crayons bleus.',
          'Sur l’arbre, il existe un chemin permettant d’obtenir deux crayons verts.'
        ],
        corr5_2: [
          'Sur l’arbre, il existe un chemin permettant d’obtenir un tee-shirts et une jupe blancs.',
          'Sur l’arbre, il existe un chemin permettant d’obtenir un tee-shirts et une jupe noirs.'
        ],
        corr5_3: [
          'Sur l’arbre, il existe un chemin permettant d’obtenir deux boules rouges.',
          'Sur l’arbre, il existe un chemin permettant d’obtenir deux boules noires.',
          'Sur l’arbre, il existe un chemin permettant d’obtenir deux boules blanches.'
        ],
        corr5_4: [
          'Sur l’arbre, il existe un chemin permettant d’obtenir deux romans d’aventure.',
          'Sur l’arbre, il existe un chemin permettant d’obtenir deux policiers.',
          'Sur l’arbre, il existe un chemin permettant d’obtenir deux livres de science fiction.'
        ],
        corr5_5: [
          'Sur l’arbre, il existe un chemin permettant d’obtenir deux livres.',
          'Sur l’arbre, il existe un chemin permettant d’obtenir deux jeux.',
          'Sur l’arbre, il existe un chemin permettant d’obtenir deux objets liés aux nouvelles technologies.'
        ],
        corr6: 'On obtient la probabilité de suivre ce chemin en multipliant les probabilités des branches successives.',
        corr7: 'Donc la probabilité vaut $£c=£r$',
        corr8_1: [
          'Sur l’arbre, il existe deux chemins permettant d’obtenir deux crayons de la même couleur (bleus ou verts).',
          'Sur l’arbre, il existe deux chemins permettant d’obtenir un crayon vert et un rouge.',
          'Sur l’arbre, il existe deux chemins permettant d’obtenir un crayon bleu et un rouge.',
          'Sur l’arbre, il existe cinq chemins permettant d’obtenir au moins un crayon bleu (c’est-à-dire un ou deux bleus).',
          'Sur l’arbre, il existe cinq chemins permettant d’obtenir au moins un crayon vert (c’est-à-dire un ou deux verts).'
        ],
        corr8_2: [
          'Sur l’arbre, il existe deux chemins permettant d’obtenir un tee-shirt ou une jupe de la même couleur (blancs ou noirs).',
          'Sur l’arbre, il existe deux chemins permettant d’obtenir un vêtement blanc et l’autre noir.',
          'Sur l’arbre, il existe quatre chemins permettant d’obtenir au moins un vêtement blanc (c’est-à-dire un ou deux blancs).',
          'Sur l’arbre, il existe quatre chemins permettant d’obtenir au moins un vêtement noir (c’est-à-dire un ou deux noirs).'
        ],
        corr8_3: [
          'Sur l’arbre, il existe trois chemins permettant d’obtenir deux boules de la même couleur.',
          'Sur l’arbre, il existe deux chemins permettant d’obtenir une boule noire et une rouge (en ne tenant pas compte de l’ordre).',
          'Sur l’arbre, il existe deux chemins permettant d’obtenir une boule rouge et une blanche (en ne tenant pas compte de l’ordre).',
          'Sur l’arbre, il existe cinq chemins permettant d’obtenir au moins une boule noire (c’est-à-dire une ou deux boules noires).',
          'Sur l’arbre, il existe cinq chemins permettant d’obtenir au moins une boule rouge (c’est-à-dire une ou deux boules rouges).'
        ],
        corr8_4: [
          'Sur l’arbre, il existe trois chemins permettant d’obtenir deux livres du même genre.',
          'Sur l’arbre, il existe deux chemins permettant d’obtenir un policier et un livre de science fiction (en ne tenant pas compte de l’ordre).',
          'Sur l’arbre, il existe deux chemins permettant d’obtenir un policier et un roman d’aventure (en ne tenant pas compte de l’ordre).',
          'Sur l’arbre, il existe cinq chemins permettant d’obtenir au moins un policier (c’est-à-dire un ou deux boules policiers).',
          'Sur l’arbre, il existe cinq chemins permettant d’obtenir au moins un livre de science fiction (c’est-à-dire une ou deux livres de science fiction).'
        ],
        corr8_5: [
          'Sur l’arbre, il existe trois chemins permettant d’obtenir deux cadeaux du même style.',
          'Sur l’arbre, il existe deux chemins permettant d’obtenir un jeu et un livre (en ne tenant pas compte de l’ordre).',
          'Sur l’arbre, il existe deux chemins permettant d’obtenir un jeu et un objet lié aux nouvelles technologies (en ne tenant pas compte de l’ordre).',
          'Sur l’arbre, il existe cinq chemins permettant d’obtenir au moins un livre (c’est-à-dire une ou deux livres).',
          'Sur l’arbre, il existe cinq chemins permettant d’obtenir au moins un objet lié aux nouvelles technologies (c’est-à-dire une ou deux tels objets).'
        ],
        corr9: 'Pour chaque chemin, on multiplie les probabilités des branches successives et on ajoute toutes ces probabilités.'
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.
      // limite : 10;
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    // Construction de la page

    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    stor.tabQuest = [1, 2, 3, 4, 5]
    stor.tabQuestInit = [...stor.tabQuest]

    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    const largFenetre = 450
    stor.idDetailFenetre = j3pGetNewId('detailsFenetre')
    stor.idCorrDetails = j3pGetNewId('CorDetails')
    me.fenetresjq = [
      {
        name: stor.idCorrDetails,
        title: ds.textes.corr4,
        width: largFenetre,
        height: 600,
        left: me.zonesElts.MG.getBoundingClientRect().width - largFenetre - 20,
        top: me.zonesElts.HD.getBoundingClientRect().height + 10,
        id: stor.idDetailFenetre
      }
    ]
    j3pCreeFenetres(me)
    let i
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    stor.arbre = j3pAddElt(stor.conteneurD, 'div')
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    if (me.questionCourante % ds.nbetapes === 1) {
      if (stor.tabQuest.length === 0) {
        stor.tabQuest = [...stor.tabQuestInit]
      }
      const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
      stor.quest = stor.tabQuest[pioche]
      stor.tabQuest.splice(pioche, 1)
      stor.obj = genereDonnees(stor.quest)
    } else {
      j3pMasqueFenetre(stor.idCorrDetails)
    }
    for (i = 1; i <= 4; i++) {
      stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
      j3pAffiche(stor['zoneCons' + i], '', ds.textes['consigne1_' + stor.quest + i], stor.obj)
    }
    let mesZonesSaisie
    if (me.questionCourante % ds.nbetapes === 1) {
      stor.obj.complet = {}
      stor.obj.complet = { evts: true, proba: false }
      stor.obj.lestyle = {
        styletexte: me.styles.toutpetit.enonce,
        couleur: me.styles.toutpetit.enonce.color
      }
      stor.obj.parcours = me
      construireArbreCond(stor.arbre, stor.obj)
      stor.objArbreRep = {}
      Object.assign(stor.objArbreRep, stor.obj)
      // taille du texte evt (pour les proba, il faudrait qu’elles soient plus petites sans doute) + couleur du texte et des branches
      stor.objArbreRep.complet.proba = true
      stor.objArbreRep.lestyle.color = me.styles.toutpetit.correction.color
      stor.zoneCons5 = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '15px', fontStyle: 'italic' } })
      j3pAffiche(stor.zoneCons5, '', ds.textes.formatPrb)
    } else {
      stor.numetape = (me.questionCourante - 1) % ds.nbetapes + 1
      // J’affiche l’arbre
      stor.objArbreRep.lestyle.color = me.styles.toutpetit.enonce.color
      stor.objArbreRep.parcours = me
      construireArbreCond(stor.arbre, stor.objArbreRep)
      for (i = 5; i <= 8; i++) {
        stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
      }
      j3pAffiche(stor.zoneCons5, '', ds.textes.consigne2)
      // Choix d’une consigne
      const numcons = j3pGetRandomInt(0, ds.textes['consigne' + (stor.numetape + 1) + '_' + stor.quest].length - 1)
      j3pAffiche(stor.zoneCons6, '', ds.textes['consigne' + (stor.numetape + 1) + '_' + stor.quest][numcons])
      me.zoneRep = j3pAddElt(stor.zoneCons7, 'div', '')
      me.zoneInput = j3pAddElt(stor.zoneCons7, 'div', '')
      const elt = j3pAffiche(me.zoneInput, '', '&1&', {
        inputmq1: { texte: '' }
      })
      stor.zoneInput = elt.inputmqList[0]
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      stor.zoneInput.reponse = [stor.objArbreRep['repQ' + stor.numetape][numcons]]
      stor.numcons2 = numcons
      mqRestriction(stor.zoneInput, '\\d,./*+-', { commandes: ['fraction'] })
      mesZonesSaisie = [stor.zoneInput.id]
      stor.zoneCons8.style.paddingTop = '15px'
      stor.zoneCons8.style.height = '40px'
      stor.laPalette = j3pAddElt(stor.zoneCons8, 'div')
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['fraction'] })
      stor.zoneCons9 = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '15px', fontStyle: 'italic' } })
      j3pAffiche(stor.zoneCons9, '', ds.textes.formatRep1)
      stor.nbRmqSimp = 0 // cette variable regardera combien de fois on lui aura fait la remarque de simplifier sa réponse
      stor.repSimpl = stor.objArbreRep['repFracQ' + stor.numetape][numcons]
      j3pFocus(stor.zoneInput)
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: mesZonesSaisie,
        validePerso: (me.questionCourante % ds.nbetapes === 1) ? [] : mesZonesSaisie
      })
    }

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    // Paramétrage de la validation
    // mes_zones_saisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        j3pDetruitToutesLesFenetres()
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const fctsValid = (me.questionCourante % ds.nbetapes === 1) ? stor.arbre.fctsValid : stor.fctsValid
        const reponse = fctsValid.validationGlobale()
        let estFractionSimplifiee = true
        if ((me.questionCourante % ds.nbetapes === 1) && reponse.bonneReponse) {
        // On vérifie si chaque réponse est bien irréductible.
        // On renvoie juste un message si ce n’est pas le cas, mais on accepte la réponse
          for (let i = 0; i < fctsValid.zones.reponseSaisie.length; i++) {
            if (estFractionSimplifiee) {
              estFractionSimplifiee = fctsValid.zones.reponseSimplifiee[i][0] && fctsValid.zones.reponseSimplifiee[i][1]
            }
          }
        }
        let repNonSimplifiee = false
        if (me.questionCourante % ds.nbetapes !== 1) {
          if (reponse.aRepondu) {
          // On teste si la réponse est bonne
            reponse.bonneReponse = Math.abs(j3pCalculValeur(fctsValid.zones.reponseSaisie[0]) - stor.zoneInput.reponse[0]) < 1e-12
            if (reponse.bonneReponse) {
              if ((/[+\-*]/g.test(fctsValid.zones.reponseSaisie[0].replace(/\\times/g, '*'))) || ((fctsValid.zones.reponseSaisie[0].includes('frac')) && (!fctsValid.zones.reponseSimplifiee[0][0] || !fctsValid.zones.reponseSimplifiee[0][1]))) {
                stor.nbRmqSimp++
                repNonSimplifiee = true
                if (stor.nbRmqSimp <= 2) {
                  reponse.aRepondu = false
                } else {
                  reponse.bonneReponse = false
                }
              }
            }
            if (!reponse.bonneReponse) {
              fctsValid.zones.bonneReponse[0] = false
            }
          }
        }
        let zoneRepEleve
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // On peut ajouter un commentaire particulier.
          let msgReponseManquante
          if (repNonSimplifiee) {
            msgReponseManquante = ds.textes.comment2
            zoneRepEleve = j3pAddElt(me.zoneRep, 'div', '', { style: { color: me.styles.moyen.enonce.color } })
            j3pAffiche(zoneRepEleve, '', '$' + fctsValid.zones.reponseSaisie[0] + '$')
            stor.zoneCons9.innerHTML = (stor.nbRmqSimp === 1) ? ds.textes.formatRep2 : ds.textes.formatRep3
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
          fctsValid.coloreUneZone(fctsValid.zones.inputs[0])
          // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            if (!estFractionSimplifiee) {
              stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
            }
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (repNonSimplifiee) {
                stor.zoneCorr.innerHTML = ds.textes.comment2
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                if (me.questionCourante % ds.nbetapes !== 1) {
                  zoneRepEleve = j3pAddElt(me.zoneRep, 'div', '', { style: { color: me.styles.cfaux } })
                  j3pAffiche(zoneRepEleve, '', '$' + fctsValid.zones.reponseSaisie[0] + '$')
                }
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
