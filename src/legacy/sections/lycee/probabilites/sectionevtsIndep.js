import { j3pAddElt, j3pAjouteBouton, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pDetruit, j3pElement, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pPGCD, j3pShuffle, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import tabDoubleEntree from 'src/legacy/outils/tableauxProba/tabDoubleEntree'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since juin 2022
 * @fileOverview Cette section permet de vérifier l’indépendance ou non de deux évènements à l’aide d’un tableau à double entrée
 */

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes']
  ]
}

/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {
    // Nombre de répétitions de l’exercice
    nbrepetitions: 3,
    nbchances: 1,
    indication: '',
    textes: {
      titre_exo: 'Indépendance d’évènements',
      // on donne les phrases de la consigne
      // Inspiré de Polynésie sept 2011 STG GRH
      consigne1_1: 'Dans un club sportif chaque membre ne pratique qu’un sport. Leur répartition est donnée dans le tableau suivant&nbsp;:',
      ligne1: 'VTT|Gymnastique|Badminton|Tir à l’arc',
      colonne1: 'Hommes|Femmes',
      consigne2_1: 'On choisit au hasard un membre du club sportif et on considère les évènements&nbsp;:',
      nomEvtsLigne1: 'V|G|B|T',
      nomEvtsColonne1: 'H|F',
      total: 'Total',
      evtsLigne1: 'la personne choisie fait du VTT&nbsp;;|la personne choisie fait de la gymnastique&nbsp;;|la personne choisie fait du badminton&nbsp;;|la personne choisie fait du tir à l’arc&nbsp;;',
      evtsColonne1: 'la personne choisie est un homme.|la personne choisie est une femme.',
      // Inspiré de Antilles sept 2011 STG GRH
      consigne1_2: 'On a interrogé un groupe d’étudiants titulaires d’un baccalauréat et qui ont poursuivi leurs études. Ces résultats sont consignés dans le tableau ci-dessous&nbsp;:',
      ligne2: 'BTS/IUT|Université|Autre formation',
      colonne2: 'Filles|Garçons',
      consigne2_2: 'On choisit au hasard un de ces étudiants et on considère les évènements&nbsp;:',
      nomEvtsLigne2: 'B|U|A',
      nomEvtsColonne2: 'F|G',
      evtsLigne2: 'l’étudiant choisi est en BTS ou IUT&nbsp;;|l’étudiant choisi est à l’université&nbsp;;|l’étudiant choisi est dans une autre formation&nbsp;;',
      evtsColonne2: 'l’étudiant choisi est une fille.|l’étudiant choisi est un garçon.',
      // Inspiré de Nouvelle Calédonie nov 2006 ACC
      consigne1_3: 'Le tableau ci-dessous donne le statut d’employés d’une entreprise obtenu suite à une enquête menée&nbsp;:',
      ligne3: 'Moins de 30 ans|Entre 30 et 40 ans|Plus de 40 ans',
      colonne3: 'Cadres|Non cadres',
      consigne2_3: 'On choisit au hasard un de ces employés et on considère les évènements&nbsp;:',
      nomEvtsLigne3: 'M|T|Q',
      nomEvtsColonne3: 'C|N',
      evtsLigne3: 'l’employé choisi a moins de 30 ans&nbsp;;|l’employé choisi a entre 30 et 40 ans&nbsp;;|l’employé choisi a plus de 40 ans&nbsp;;',
      evtsColonne3: 'l’employé choisi est un cadre.|l’employé choisi n’est pas un cadre.',
      // Inspiré de Polynésie juin 2005 ACC
      consigne1_4: 'Un disquaire propose dans un de ses rayons un choix de CD de catégories Rap, Soul ou Métal. Certains sont en langue française, les autres en langue anglaise comme indiqué dans le tableau ci-dessous&nbsp;:',
      ligne4: 'Rap|Soul|Métal',
      colonne4: 'Langue française|Langue anglaise',
      consigne2_4: 'On choisit au hasard un CD de ce rayon et on considère les évènements :',
      nomEvtsLigne4: 'R|S|M',
      nomEvtsColonne4: 'F|A',
      evtsLigne4: 'le CD choisi appartient à la catégorie Rap&nbsp;;|le CD choisi appartient à la catégorie Soul&nbsp;;|le CD choisi appartient à la catégorie Métal&nbsp;;',
      evtsColonne4: 'le CD choisi est en langue française.|le CD choisi est en langue anglaise.',
      // Inspiré de Polynésie septembre 2005 ACC
      consigne1_5: 'Afin de mieux connaître sa clientèle, une station de sports d’hiver a effectué une enquête dont les résultats sont donnés dans le tableau ci-dessous&nbsp;:',
      ligne5: 'Possède son matériel|Loue sur place|Loue ailleurs',
      colonne5: 'Vient 1 semaine par an|Vient tous les week-ends',
      consigne2_5: 'On choisit un client de la station et on considère les évènements :',
      nomEvtsLigne5: 'M|L|A',
      nomEvtsColonne5: 'S|W',
      evtsLigne5: 'le client choisi possède son matériel&nbsp;;|le client choisi loue son matériel sur place&nbsp;;|le client choisi loue son matériel ailleurs&nbsp;;',
      evtsColonne5: 'le client choisi vient une semaine par an.|le client choisi vient tous les week-ends.',
      consigne3: 'Les évènements £A et £B sont-ils indépendants&nbsp;?',
      cocher1: 'oui',
      cocher2: 'non',
      info2: 'Tu n’as qu’une tentative pour répondre.',
      corr1: '$P(£A)=£a$ et $P_{£B}(£A)=£b$.',
      corr2: 'Comme $P(£A)=P_{£B}(£A)$, on conclut que les évènements $£A$ et $£B$ sont indépendants.',
      corr3: 'Comme $P(£A)\\neq P_{£B}(£A)$, on conclut que les évènements $£A$ et $£B$ ne sont pas indépendants.'
    }
  }
} // getDonnees

/**
 * Retourne les données pour chaque sujet
 * @private
 * @param {Number} quest : numéro de la question (la première ayant une colonne de plus que les autres)
 * @param {Array} tabEvts : tableau de tableau contenant les événements de l’énoncé
 * @param {Boolean} isIndep
 * @return {Object}
 */
function genereAlea (quest, tabEvts, isIndep) {
  // tableau contenant l’effectif total de chaque évènement présent par colonne (donc de longueur tabEvts1.length)
  const effTableLigne = []
  let borneInf, borneSup, effLigne1
  // Dans cet exercice, il faut faire attention à ce qu’on prend pour effectif total
  // En effet, je devrai prend des effectifs d’evts A et B de sorte que card(A)*card(B) = effTotal*car(A inter B)
  // Donc il faut choisir correctement
  let totalLigneChoix, totalColonneChoix, choixCol, cardInter
  let effTotal
  const effTabCol = (quest === 1) ? [0, 0, 0, 0] : [0, 0, 0]
  do {
    const nb2 = j3pGetRandomInt(3, 5)
    const nb3 = (nb2 === 5) ? 3 : (nb2 === 4) ? j3pGetRandomInt(3, 4) : 4
    const choixDernierFacteurs = j3pShuffle([5, 7, 11]).slice(1)
    const produitTotal = Math.pow(2, nb2) * Math.pow(3, nb3) * choixDernierFacteurs[0] * choixDernierFacteurs[1]
    const choixFacteurs = choixDernierFacteurs[j3pGetRandomInt(0, 1)]
    const nb2Inter = j3pGetRandomInt(0, 2)
    const nb3Inter = 2 - nb2Inter
    cardInter = Math.pow(2, nb2Inter) * Math.pow(3, nb3Inter) * choixFacteurs
    effTotal = produitTotal / cardInter
    choixCol = j3pGetRandomInt(0, effTabCol.length - 2)
    totalColonneChoix = Math.pow(2, j3pGetRandomInt(1, 3))
    totalColonneChoix *= Math.pow(3, j3pGetRandomInt(1, 3))
    totalColonneChoix *= choixDernierFacteurs[j3pGetRandomInt(0, 1)]
    totalLigneChoix = produitTotal / totalColonneChoix
  } while ((totalLigneChoix / cardInter < 4) || (totalColonneChoix / cardInter < 3))
  if (totalColonneChoix > totalLigneChoix) {
    const stock = totalColonneChoix
    totalColonneChoix = totalLigneChoix
    totalLigneChoix = stock
  }
  const choixLigne = j3pGetRandomInt(0, 1)
  // C’est là que je distingue le cas où les evts seront ou non independants
  effTabCol[choixCol] = (isIndep) ? totalColonneChoix : totalColonneChoix + (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(3, 8)
  for (let i = 0; i < effTabCol.length; i++) effTableLigne[i] = [0, 0]
  if (quest === 1) {
    // 4 colonnes et 2 lignes
    // Je dois remplir la ligne où j’ai déjà cardInter et totalLigneChoix
    const leResteLigneChoix = totalLigneChoix - cardInter
    for (let i = 0; i <= 2; i++) {
      if (i !== choixCol) {
        borneInf = Math.floor(leResteLigneChoix / 5)
        borneSup = Math.ceil(2 * leResteLigneChoix / 5)
        effLigne1 = j3pGetRandomInt(borneInf, borneSup)
        effTableLigne[i][choixLigne] = effLigne1
      } else {
        effTableLigne[i][choixLigne] = cardInter
      }
    }
    effTableLigne[3][choixLigne] = totalLigneChoix - effTableLigne[0][choixLigne] - effTableLigne[1][choixLigne] - effTableLigne[2][choixLigne]
    // Là je complète la dernière ligne (total de chaque colonne
    const leReste = effTotal - effTabCol[choixCol]
    for (let i = 0; i <= 2; i++) {
      if (i !== choixCol) effTabCol[i] = Math.floor(leReste / 3) + j3pGetRandomInt(7, 14)
    }
    effTabCol[3] = effTotal - effTabCol[0] - effTabCol[1] - effTabCol[2]
  } else {
    // 3 colonnes et 2 lignes
    // Je dois remplir la ligne où j’ai déjà cardInter et totalLigneChoix
    const leResteLigneChoix = totalLigneChoix - cardInter
    for (let i = 0; i <= 1; i++) {
      if (i !== choixCol) {
        borneInf = Math.floor(leResteLigneChoix / 4)
        borneSup = Math.ceil(2 * leResteLigneChoix / 4)
        effLigne1 = j3pGetRandomInt(borneInf, borneSup)
        effTableLigne[i][choixLigne] = effLigne1
      } else {
        effTableLigne[i][choixLigne] = cardInter
      }
    }
    effTableLigne[2][choixLigne] = totalLigneChoix - effTableLigne[0][choixLigne] - effTableLigne[1][choixLigne]
    // Là je complète la dernière ligne (total de chaque colonne
    const leReste = effTotal - effTabCol[choixCol]
    for (let i = 0; i <= 1; i++) {
      if (i !== choixCol) effTabCol[i] = Math.floor(leReste / 3) + j3pGetRandomInt(7, 14)
    }
    effTabCol[2] = effTotal - effTabCol[0] - effTabCol[1]
  }
  // Il ne me reste plus qu'à compléter la ligne où je n’ai pas choisi l’événement
  for (let i = 0; i < effTabCol.length; i++) {
    effTableLigne[i][1 - choixLigne] = effTabCol[i] - effTableLigne[i][choixLigne]
  }
  const A = tabEvts[0][choixCol]
  const B = tabEvts[1][choixLigne]
  let prbBsachantA = '\\frac{' + cardInter + '}{' + totalLigneChoix + '}'
  const lePGCDprbCond = j3pPGCD(cardInter, totalLigneChoix)
  if (lePGCDprbCond !== 1) prbBsachantA += '=\\frac{' + cardInter / lePGCDprbCond + '}{' + totalLigneChoix / lePGCDprbCond + '}'
  let prbB = '\\frac{' + effTabCol[choixCol] + '}{' + effTotal + '}'
  const lePGCDprbB = j3pPGCD(effTabCol[choixCol], effTotal)
  if (lePGCDprbB !== 1) prbB += '=\\frac{' + effTabCol[choixCol] / lePGCDprbB + '}{' + effTotal / lePGCDprbB + '}'
  return { effTableLigne, effTabCol, effTotal, evtsChoisis: [A, B], probasRep: [prbB, prbBsachantA] }
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(me.donneesSection.textes.titre_exo)
  const stor = me.storage
  stor.tabQuest = [1, 2, 3, 4, 5]
  stor.tabQuestInit = [...stor.tabQuest]
  // J’impose les cas d’événements indépendants ou non
  stor.indep = []
  for (let i = 0; i < Math.floor(me.donneesSection.nbrepetitions / 2); i++) stor.indep.push(true, false)
  if (me.donneesSection.nbrepetitions % 2 !== 0) stor.indep.push(j3pGetRandomBool())
  stor.indep = j3pShuffle(stor.indep)
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  const pioche = j3pGetRandomInt(0, stor.tabQuest.length - 1)
  stor.numQuest = stor.tabQuest[pioche]
  stor.tabQuest.splice(pioche, 1)
  if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  // on affiche l’énoncé
  ;[1, 2, 3, 4, 5, 6, 7].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })

  stor.objTab = {}
  stor.objTab.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
  stor.objTab.lestyle = { styletexte: me.styles.toutpetit.enonce, couleur: me.styles.toutpetit.enonce.color }
  stor.objTab.nomsLigne = ds.textes['ligne' + stor.numQuest].split('|')
  stor.objTab.nomsLigne.push(ds.textes.total)
  stor.objTab.nomsColonne = ds.textes['colonne' + stor.numQuest].split('|')
  stor.objTab.nomsColonne.push(ds.textes.total)
  stor.tabEvts1 = ds.textes['nomEvtsLigne' + stor.numQuest].split('|')
  stor.tabEvts2 = ds.textes['nomEvtsColonne' + stor.numQuest].split('|')
  stor.tabEvtsTxt1 = ds.textes['evtsLigne' + stor.numQuest].split('|')
  stor.tabEvtsTxt2 = ds.textes['evtsColonne' + stor.numQuest].split('|')
  const isIndep = stor.indep[me.questionCourante - 1]
  const { effTableLigne, effTabCol, effTotal, evtsChoisis, probasRep } = genereAlea(stor.numQuest, [stor.tabEvts1, stor.tabEvts2], isIndep)
  stor.isIndep = isIndep
  stor.objDonnees = {
    A: evtsChoisis[0],
    B: evtsChoisis[1],
    a: probasRep[0],
    b: probasRep[1]
  }
  const sommeLigne = []
  stor.objTab.effectifs = {}
  for (let j = 0; j < effTableLigne[0].length; j++) {
    sommeLigne.push(0)
    stor.objTab.effectifs['ligne' + (j + 1)] = []
  }
  for (let i = 0; i < effTabCol.length; i++) {
    for (let j = 0; j < effTableLigne[i].length; j++) {
      stor.objTab.effectifs['ligne' + (j + 1)].push(effTableLigne[i][j])
      sommeLigne[j] += effTableLigne[i][j]
    }
  }
  for (let j = 0; j < effTableLigne[0].length; j++) {
    stor.objTab.effectifs['ligne' + (j + 1)].push(sommeLigne[j])
  }
  // obj.effectifs['ligne'+i] contiendra les effectifs sur chaque ligne associée
  stor.objTab.effectifs['ligne' + (effTableLigne[0].length + 1)] = [...effTabCol]
  stor.objTab.effectifs['ligne' + (effTableLigne[0].length + 1)].push(effTotal)
  j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + stor.numQuest])
  tabDoubleEntree(stor.zoneCons2, stor.objTab)
  j3pAffiche(stor.zoneCons3, '', ds.textes['consigne2_' + stor.numQuest])
  const tabDivEvts = []
  for (let i = 0; i < stor.tabEvtsTxt1.length + 1; i++) {
    tabDivEvts.push(j3pAddElt(stor.zoneCons4, 'div'))
  }
  for (let i = 0; i < stor.tabEvtsTxt1.length; i++) {
    j3pAffiche(tabDivEvts[i], '', stor.tabEvts1[i] + ' : ' + stor.tabEvtsTxt1[i])
  }
  const posEvt = stor.tabEvts2.indexOf(evtsChoisis[1])
  j3pAffiche(tabDivEvts[stor.tabEvtsTxt1.length], '', stor.tabEvts2[posEvt] + ' : ' + stor.tabEvtsTxt2[posEvt])
  j3pAffiche(stor.zoneCons5, '', ds.textes.consigne3, {
    A: evtsChoisis[0],
    B: evtsChoisis[1]
  })
  const espacetxt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
  j3pAffiche(stor.zoneCons6, '', espacetxt)
  stor.nameRadio = j3pGetNewId('choix')
  stor.idsRadio = ['1', '2'].map(i => j3pGetNewId('radio' + i))
  j3pBoutonRadio(stor.zoneCons6, stor.idsRadio[0], stor.nameRadio, 0, ds.textes.cocher1)
  j3pAffiche(stor.zoneCons6, '', espacetxt + espacetxt)
  j3pBoutonRadio(stor.zoneCons6, stor.idsRadio[1], stor.nameRadio, 1, ds.textes.cocher2)
  j3pStyle(stor.zoneCons7, { fontSize: '0.9em', fontStyle: 'italic', paddingTop: '10px' })
  j3pAffiche(stor.zoneCons7, '', ds.textes.info2)
  stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
  me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
  j3pCreeFenetres(me)
  j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
  j3pAfficheCroixFenetres('Calculatrice')

  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })

  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  j3pDetruit(stor.zoneCalc)
  j3pDetruitFenetres('Calculatrice')
  j3pDetruit(stor.zoneCons7)
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
  j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, stor.objDonnees)
  j3pAffiche(stor.zoneExpli2, '', (stor.isIndep) ? ds.textes.corr2 : ds.textes.corr3, stor.objDonnees)
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const choixRadio = j3pBoutonRadioChecked(stor.nameRadio)
  const bonChoixRadio = ((choixRadio[0] === 0) && stor.isIndep) || ((choixRadio[0] === 1) && !stor.isIndep)
  const reponse = { aRepondu: (choixRadio[0] > -1), bonneReponse: bonChoixRadio }
  if (reponse.aRepondu) {
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  const choixRadio = j3pBoutonRadioChecked(stor.nameRadio)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  stor.idsRadio.forEach(elt => j3pDesactive(elt))
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    j3pElement('label' + stor.idsRadio[choixRadio[0]]).style.color = me.styles.cbien
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    j3pBarre('label' + stor.idsRadio[choixRadio[0]])
    j3pElement('label' + stor.idsRadio[choixRadio[0]]).style.color = me.styles.cfaux
    j3pElement('label' + stor.idsRadio[(choixRadio[0] + 1) % 2]).style.color = me.styles.toutpetit.correction.color
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
