import { j3pAddElt, j3pDetruit, j3pElement, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pMasqueFenetre } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexQuotient, j3pGetLatexOppose, j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { construireArbreCond } from 'src/legacy/outils/arbreProba/arbreProba'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mai 2020
        Connaissant certaines probabilités (ou probabilités conditionnelles), on demande de compléter un arbre de proba
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Arbre avec probabilités conditionnelles',
  // on donne les phrases de la consigne
  consigne1: 'L’arbre ci-contre est incomplet. Le compléter à l’aide des probabilités ci-dessous&nbsp;:',
  consigne2: 'Compléter l’arbre de probabilité traduisant la situation.',
  rmq: 'Attention : toutes les probabilités à placer ne sont pas écrites et parmi celles qui sont données, certaines sont inutiles.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: '',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Arbre attendu :'
}
/**
 * section arbreProbaCond
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection () {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.divArbre.laPalette)
    const arbreFenetre = j3pAddElt(stor.fenetre, 'div', '', { style: me.styles.toutpetit.explications })
    j3pToggleFenetres(stor.corrDetails)
    stor.objArbreRep.lestyle.couleur = me.styles.toutpetit.correction.color
    stor.objArbreRep.parcours = me
    construireArbreCond(arbreFenetre, stor.objArbreRep)
    const tabOverline = document.querySelectorAll('.mq-overline')
    tabOverline.forEach(elt => {
      const laCouleur = elt.style.color
      j3pStyle(elt, { borderTop: '1px solid ' + laCouleur })
    })
  }

  function enonceInitFirst () {
    ds = me.donneesSection
    // Construction de la page
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })

    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zones.IG, ds.indication)

    const largFenetre = 520
    stor.fenetre = j3pGetNewId('detailsFenetre')
    stor.corrDetails = j3pGetNewId('corDetails')
    me.fenetresjq = [
      {
        name: stor.corrDetails,
        title: textes.corr1,
        width: largFenetre,
        height: 400,
        left: me.zonesElts.MG.getBoundingClientRect().width - largFenetre - 20,
        top: me.zonesElts.HD.getBoundingClientRect().height + 10,
        id: stor.fenetre
      }
    ]
    j3pCreeFenetres(me)
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    j3pEmpty(stor.fenetre)
    j3pMasqueFenetre(stor.corrDetails) // On cache la fenêtre de la correction
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1)
    stor.choixEvt = []
    stor.choixEvt.push(String.fromCharCode(65 + j3pGetRandomInt(0, 15)))
    let newEvt
    do {
      newEvt = String.fromCharCode(65 + j3pGetRandomInt(0, 15))
    } while (newEvt === stor.choixEvt[0])
    stor.choixEvt.push(newEvt)
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    stor.divArbre = j3pAddElt(stor.zoneD, 'div')
    stor.objArbre = {}
    stor.objArbre.lestyle = {
      styletexte: me.styles.toutpetit.enonce,
      couleur: me.styles.toutpetit.enonce.color
    }
    stor.objArbre.complet = { evts: true, proba: false }
    // les événements sont affichées (donc pas moyen d’ajouter un noeud)
    const choixArbre = j3pGetRandomBool() // s’il vaut true alors on écrit d’abord A puis B dans l’arbre
    stor.objArbre.evts1 = []
    stor.objArbre.evts2 = {}
    stor.objArbre.evts1.push([stor.choixEvt[0], ''])
    stor.objArbre.evts1.push(['\\overline{' + stor.choixEvt[0] + '}', ''])
    for (let i = 0; i < 2; i++) {
      stor.objArbre.evts2['branche' + i] = []
      stor.objArbre.evts2['branche' + i].push([stor.choixEvt[1], ''])
      stor.objArbre.evts2['branche' + i].push(['\\overline{' + stor.choixEvt[1] + '}', ''])
    }
    if (!choixArbre) {
      stor.objArbre.evts1[0][0] = stor.choixEvt[1]
      stor.objArbre.evts1[1][0] = '\\overline{' + stor.choixEvt[1] + '}'

      for (let i = 0; i < 2; i++) {
        stor.objArbre.evts2['branche' + i][0][0] = stor.choixEvt[0]
        stor.objArbre.evts2['branche' + i][1][0] = '\\overline{' + stor.choixEvt[0] + '}'
      }
    }
    // Je détermine les probabilités
    const ChoixPrb = [[1, 2], [1, 3], [2, 3], [1, 4], [3, 4], [2, 5], [3, 5], [1, 6], [5, 6]]
    const prbA = ChoixPrb[j3pGetRandomInt(0, ChoixPrb.length - 1)]
    let prbB1, prbB2
    do {
      prbB1 = ChoixPrb[j3pGetRandomInt(0, ChoixPrb.length - 1)]
    } while (Math.abs(prbA[1] - prbB1[1]) < Math.pow(10, -12))
    do {
      prbB2 = ChoixPrb[j3pGetRandomInt(0, ChoixPrb.length - 1)]
    } while ((Math.abs(prbB2[1] - prbB1[1]) < Math.pow(10, -12)) || (Math.abs(prbB2[1] - prbA[1]) < Math.pow(10, -12)))
    // Je calcule les autres probabilites
    stor.objArbreRep = {}
    stor.objArbreRep.lestyle = {
      styletexte: me.styles.toutpetit.enonce,
      couleur: me.styles.toutpetit.enonce.color
    }
    stor.objArbreRep.complet = { evts: true, proba: true }
    stor.objArbreRep.evts1 = []
    stor.objArbreRep.evts2 = {}
    const ecriturePrbA = '\\frac{' + prbA[0] + '}{' + prbA[1] + '}'
    const ecriturePrbABarre = j3pGetLatexSomme(1, j3pGetLatexOppose(ecriturePrbA))
    const ecriturePBSachantA = '\\frac{' + prbB1[0] + '}{' + prbB1[1] + '}'
    const ecriturePBSachantABarre = '\\frac{' + prbB2[0] + '}{' + prbB2[1] + '}'
    const ecriturePrbB = j3pGetLatexSomme(j3pGetLatexProduit(ecriturePrbA, ecriturePBSachantA), j3pGetLatexProduit(ecriturePrbABarre, ecriturePBSachantABarre))
    const ecriturePASachantB = j3pGetLatexQuotient(j3pGetLatexProduit(ecriturePrbA, ecriturePBSachantA), ecriturePrbB)
    const ecriturePrbBBarre = j3pGetLatexSomme(1, j3pGetLatexOppose(ecriturePrbB))
    const ecriturePASachantBBarre = j3pGetLatexQuotient(j3pGetLatexSomme(ecriturePrbA, j3pGetLatexOppose(j3pGetLatexProduit(ecriturePrbA, ecriturePBSachantA))), ecriturePrbBBarre)

    me.logIfDebug(
      'ecriturePrbA:', ecriturePrbA,
      '\necriturePrbABarre:', ecriturePrbABarre,
      '\necriturePBSachantA:', ecriturePBSachantA,
      '\necriturePBSachantABarre:', ecriturePBSachantABarre,
      '\necriturePrbB:', ecriturePrbB,
      '\necriturePASachantB:', ecriturePASachantB,
      '\necriturePrbBBarre:', ecriturePrbBBarre,
      '\necriturePASachantBBarre:', ecriturePASachantBBarre
    )
    for (let i = 0; i < 2; i++) stor.objArbreRep.evts2['branche' + i] = []
    let prbBBarre
    if (choixArbre) {
      stor.objArbreRep.evts1.push([stor.choixEvt[0], ecriturePrbA])
      stor.objArbreRep.evts1.push(['\\overline{' + stor.choixEvt[0] + '}', ecriturePrbABarre])
      stor.objArbreRep.evts2.branche0 = []
      stor.objArbreRep.evts2.branche0.push([stor.choixEvt[1], ecriturePBSachantA])
      prbBBarre = j3pGetLatexSomme(1, j3pGetLatexOppose(stor.objArbreRep.evts2.branche0[0][1]))
      stor.objArbreRep.evts2.branche0.push(['\\overline{' + stor.choixEvt[1] + '}', prbBBarre])
      stor.objArbreRep.evts2.branche1 = []
      stor.objArbreRep.evts2.branche1.push([stor.choixEvt[1], '\\frac{' + prbB2[0] + '}{' + prbB2[1] + '}'])
      prbBBarre = j3pGetLatexSomme(1, j3pGetLatexOppose(stor.objArbreRep.evts2.branche1[0][1]))
      stor.objArbreRep.evts2.branche1.push(['\\overline{' + stor.choixEvt[1] + '}', prbBBarre])
    } else {
      stor.objArbreRep.evts1.push([stor.choixEvt[1], ecriturePrbB])
      stor.objArbreRep.evts1.push(['\\overline{' + stor.choixEvt[1] + '}', ecriturePrbBBarre])
      stor.objArbreRep.evts2.branche0 = []
      stor.objArbreRep.evts2.branche0.push([stor.choixEvt[0], ecriturePASachantB])
      const prbABarre = j3pGetLatexSomme(1, j3pGetLatexOppose(stor.objArbreRep.evts2.branche0[0][1]))
      stor.objArbreRep.evts2.branche0.push(['\\overline{' + stor.choixEvt[0] + '}', prbABarre])
      stor.objArbreRep.evts2.branche1 = []
      stor.objArbreRep.evts2.branche1.push([stor.choixEvt[0], ecriturePASachantBBarre])
      prbBBarre = j3pGetLatexSomme(1, j3pGetLatexOppose(stor.objArbreRep.evts2.branche1[0][1]))
      stor.objArbreRep.evts2.branche1.push(['\\overline{' + stor.choixEvt[0] + '}', prbBBarre])
    }

    // Dans les deux cas, on donne P(A), P(B), P_A(B) et P_B(A) dans l’énoncé
    let choixAffichagePrb = j3pGetRandomBool()
    let consigne2 = (choixAffichagePrb)
      ? '$P(' + stor.choixEvt[0] + ')=' + ecriturePrbA + '$ ; ' + '$P(' + stor.choixEvt[1] + ')=' + ecriturePrbB + '$&nbsp;; '
      : '$P(' + stor.choixEvt[1] + ')=' + ecriturePrbB + '$ ; ' + '$P(' + stor.choixEvt[0] + ')=' + ecriturePrbA + '$&nbsp;; '
    choixAffichagePrb = j3pGetRandomBool()
    consigne2 += (choixAffichagePrb)
      ? '$P_' + stor.choixEvt[0] + '(' + stor.choixEvt[1] + ')=' + ecriturePBSachantA + '$&nbsp;; ' + '$P_' + stor.choixEvt[1] + '(' + stor.choixEvt[0] + ')=' + ecriturePASachantB + '$&nbsp;; '
      : '$P_' + stor.choixEvt[1] + '(' + stor.choixEvt[0] + ')=' + ecriturePASachantB + '$&nbsp;; ' + '$P_' + stor.choixEvt[0] + '(' + stor.choixEvt[1] + ')=' + ecriturePBSachantA + '$&nbsp;; '
    consigne2 += (choixAffichagePrb)
      ? '$P_{\\overline{' + stor.choixEvt[0] + '}}(' + stor.choixEvt[1] + ')=' + ecriturePBSachantABarre + '$&nbsp;; ' + '$P_{\\overline{' + stor.choixEvt[1] + '}}(' + stor.choixEvt[0] + ')=' + ecriturePASachantBBarre + '$.'
      : '$P_{\\overline{' + stor.choixEvt[1] + '}}(' + stor.choixEvt[0] + ')=' + ecriturePASachantBBarre + '$&nbsp;; ' + '$P_{\\overline{' + stor.choixEvt[0] + '}}(' + stor.choixEvt[1] + ')=' + ecriturePBSachantABarre + '$.'
    j3pAffiche(stor.zoneCons2, '', consigne2)
    j3pAffiche(stor.zoneCons3, '', textes.consigne2)
    j3pStyle(stor.zoneCons4, { fontStyle: 'italic', fontSize: '0.9em' })
    j3pAffiche(stor.zoneCons4, '', textes.rmq)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvent dans le cadre MD)
    stor.divCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    // je redonne l’arbre (à droite)
    stor.objArbre.evts1 = [...stor.objArbreRep.evts1]
    stor.objArbre.evts2 = Object.assign({}, stor.objArbreRep.evts2)
    stor.objArbre.parcours = me
    construireArbreCond(stor.divArbre, stor.objArbre)
    stor.divCorr = j3pAddElt(stor.zoneD, 'div')
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.divArbre.fctsValid.validationGlobale()

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          this.reponseManquante(stor.divCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            j3pDetruit(stor.divArbre.laPalette)
            stor.divCorr.style.color = me.styles.cbien
            stor.divCorr.innerHTML = cBien
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.divCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.divCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection()
            } else {
            // Réponse fausse :
              stor.divCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.divCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.divCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection()
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        j3pElement('Mepsectioncontinuer').addEventListener('click', j3pMasqueFenetre.bind(null, stor.corrDetails))
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
