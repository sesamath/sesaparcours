import { j3pAddElt, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        janvier 2020
        Compléter un diagramme de Venn par les effectifs
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Compléter un diagramme',
  // on donne les phrases de la consigne
  consigne1_1: 'Parmi les £n employés d’une entreprise, £c sont des cadres. De plus £m employés ont moins de 40&nbsp;ans et on compte £d cadres parmi eux.',
  consigne1_2: 'Dans une concession vendant des véhicules d’occasion, on dénombre £v voitures à vendre. £f d’entre elles sont de marque française. Les voitures de plus de 5&nbsp;ans sont au nombre de £c et £d d’entre elles sont d’une marque étrangère.',
  consigne1_3: '£a amis sont réunis au restaurant où ils ont un plat commun et peuvent choisir ou non une entrée et/ou un dessert. £e d’entre eux décident de ne pas prendre d’entrée alors qu’ils sont £d à prendre uniquement un dessert. £p amis prennent à la fois une entrée et un dessert.',
  consigne1_4: '£e élèves d’un groupe suivant les spécialités mathématiques et N.S.I. comparent leur dernière note dans chacune de ces deux disciplines. £m élèves du groupe ont eu la moyenne en mathématiques et seulement £n l’ont dans les deux disciplines. Enfin £f élèves ont la moyenne dans au moins une de ces deux disciplines.',
  consigne1_5: 'Une association sportive regroupe £a adhérents et propose différentes activités. £r pratiquent le rugby et £t le tennis. £n adhérents ne pratiquent aucune de ces deux activités.',
  consigne2: 'Donner les valeurs des variables ci-dessous correspondant aux effectifs de chaque zone du diagramme construit ci-contre&nbsp;:',
  legende1: 'Employés|Cadres|Moins\nde 40 ans',
  legende2: 'Voitures|Marque\nfrançaise|Plus de\n5 ans',
  legende3: 'Amis|Prennent\nune entrée|Prennent\nun dessert',
  legende4: 'Groupe d’élèves|Ont la moyenne\nen mathématiques|Ont la moyenne\nen N.S.I.',
  legende5: 'Adhérents|Pratiquent\nle rugby|Pratiquent\nle tennis',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: '',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: '£d employés sont cadres et ont moins de 40&nbsp;ans ainsi b=£d.',
  corr2_1: 'Comme £m employés ont moins de 40&nbsp;ans, on en déduit que $\\text{c}=£m-£d=calcule[£m-£d]$.',
  corr3_1: 'Le nombre de cadres qui ont plus de 40&nbsp;ans vaut $\\text{a}=£c-£d=calcule[£c-£d]$.',
  corr4_1: 'Enfin, d est le nombre d’employés qui ne sont pas cadres et n’ont pas 40&nbsp;ans.',
  corr5_1: 'Il y en a&nbsp;: $\\text{d}=£n-calcule[£c-£d]-£d-calcule[£m-£d]=calcule[£n-£c-£m+£d]$.',
  corr1_2: '£d voitures ont plus de 5&nbsp;ans et sont d’une marque étrangère, ainsi c=£d.',
  corr2_2: 'Comme £c voitures ont plus de 5&nbsp;ans, on en déduit que $\\text{b}=£c-£d=calcule[£c-£d]$.',
  corr3_2: 'Le nombre de voitures de marque française qui ont moins de 5&nbsp;ans vaut $\\text{a}=£f-calcule[£c-£d]=calcule[£f-£c+£d]$.',
  corr4_2: 'Enfin, d est le nombre de voitures de cette concession qui sont d’une marque étrangère et qui ont moins de 5&nbsp;ans.',
  corr5_2: 'Il y en a&nbsp;: $\\text{d}=£v-calcule[£f-£c+£d]-calcule[£c-£d]-£d=calcule[£v-£f-£d]$.',
  corr1_3: '£p amis du groupe ont pris une entrée et un dessert, ainsi b=£p.',
  corr2_3: '£e personnes n’ont pas pris d’entrée et £d d’entre eux ont pris un dessert. On en déduit que c=£d et que $\\text{d}=£e-£d=calcule[£e-£d]$.',
  corr3_3: 'Enfin, a est le nombre d’amis qui ont pris une entrée mais pas de dessert.',
  corr4_3: '$£a-£e=calcule[£a-£e]$ est le nombre d’amis qui ont pris une entrée.',
  corr5_3: 'Donc $\\text{a}=calcule[£a-£e]-£p=calcule[£a-£e-£p]$.',
  corr1_4: '£n élèves ont la moyenne dans les deux disciplines, ainsi b=£n.',
  corr2_4: 'Comme £m élèves ont la moyenne en mathématiques, on en déduit que $\\text{a}=£m-£n=calcule[£m-£n]$.',
  corr3_4: '£f élèves ont la moyenne dans une moins une de ces deux disciplines, ce qui représente $\\text{a}+\\text{b}+\\text{c}$, alors $\\text{c}=£f-calcule[£m-£n]-£n=calcule[£f-£m]$.',
  corr4_4: 'Enfin, d est le nombre d’élèves qui n’ont la moyenne dans aucune de ces deux disciplines.',
  corr5_4: 'Il y en a&nbsp;: $\\text{d}=£e-£f=calcule[£e-£f]$.',
  corr1_5: '£n adhérents ne pratiquent aucune de ces deux activités, ainsi d=£n.',
  corr2_5: 'Ainsi $£a-£n=calcule[£a-£n]$ adhérents pratiquent le rugby ou le tennis.',
  corr3_5: 'Comme £r pratiquent le rugby et £t le tennis, on en déduit que $\\text{b}=£r+£t-calcule[£a-£n]=calcule[£r+£t-£a+£n]$.',
  corr4_5: 'Enfin, le nombre d’adhérents qui ne pratiquent que le rugby vaut $\\text{a}=£r-calcule[£r+£t-£a+£n]=calcule[-£t+£a-£n]$.',
  corr5_5: 'De même $\\text{c}=£t-calcule[£r+£t-£a+£n]=calcule[-£r+£a-£n]$.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    // txt html de la figure
    const txtFig = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAQuAAADrQAAAQEAAAAAAAAAAQAAAEL#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAPAAFVAMAkAAAAAAAAQBAAAAAAAAAFAABAN4hysCDEnEA3iHKwIMSc#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABEAAAEAAQAAAAEBP+qqqqqqqqv#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAAA8AAVYAwAAAAAAAAABAEAAAAAAAAAUAAUBHiHKwIMScAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAEAAAABAAAAA#####8AAAABAAdDTWlsaWV1AP####8BAAAAABEAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAABAAAAA#####8AAAACAAxDQ29tbWVudGFpcmUA#####wEAAAAAAAAAAAAAAABAGAAAAAAAAAAAAAUNAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAARAAABBQABQGh1HrhR64VAaEwNp0DadAAAAAMA#####wEAAAABEQAAAQABAAAACAE#6qqqqqqqqwAAAAIA#####wEAAAAAEQAAAQUAAUBsanQNp0DaQGE2uFHrhR7#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAEAAAAIAAAACgAAAAIA#####wEAAAAAEQAAAQUAAUByUo9cKPXCQF54G06BtOgAAAAJAP####8BAAAAAAEAAAAIAAAADP####8AAAABAA9DUG9pbnRMaWVDZXJjbGUA#####wEAAP8AEQAAAAAAAAAAAAAAQAgAAAAAAAAFAAE##tEB4rSAlwAAAAv#####AAAAAQANQ0RlbWlEcm9pdGVPQQD#####AQAA#wANAAABAAEAAAAIAAAADv####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAPAAAADf####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAD#ABEAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAEAAAAA0A#####wAAAP8AEQAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAAAQ#####wAAAAEAEENEcm9pdGVQYXJhbGxlbGUA#####wEAAP8AEQAAAQABAAAADgAAAAn#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAA#wARAAABAAEAAAARAAAACf####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8AAAD#ABEAAAAAAAAAAAAAAEAIAAAAAAAABwAAAAATAAAAFP####8AAAACABJDTGlldU9iamV0UGFyUHRMaWUA#####wAAAP8AAAAVAAAAAUCPQAAAAAAAAAAADgAAAAcAAAAOAAAADwAAABAAAAARAAAAEwAAABQAAAAVAAAAAgD#####AQAA#wARAAABBwABQHRNOgbToG1AZKa4UeuFHgAAAAIA#####wEAAP8AEQAAAQcAAUB44o9cKPXCQGO2uFHrhR4AAAAJAP####8BAAD#AAEAAAAXAAAAGAAAAAIA#####wEAAP8AEQAAAQcAAUB8X+SxfksYQF4NcKPXCjwAAAAJAP####8BAAD#AAEAAAAXAAAAGv####8AAAABAAlDRHJvaXRlQUIA#####wEAAP8AEQAAAQABAAAAFwAAABgAAAAKAP####8B#wAAABEAAAAAAAAAAAAAAEAIAAAAAAAABQABP#ONamzhM1MAAAAZAAAACwD#####AQAAAAANAAABAAEAAAAXAAAAHQAAAAwA#####wAAAB4AAAAbAAAADQD#####AQAAAAARAAAAAAAAAAAAAABACAAAAAAAAAcAAQAAAB8AAAANAP####8AAAAAABEAAAAAAAAAAAAAAEAIAAAAAAAABwACAAAAHwAAAA4A#####wEAAAAAEQAAAQABAAAAHQAAABwAAAAPAP####8BAAAAABEAAAEAAQAAACAAAAAcAAAAEAD#####AP8AAAARAAAAAAAAAAAAAABACAAAAAAAAAcAAAAAIgAAACMAAAARAP####8A#wAAAAAAJAAAAAFAj0AAAAAAAAAAAB0AAAAHAAAAHQAAAB4AAAAfAAAAIAAAACIAAAAjAAAAJAAAAAIA#####wEAAAAAEQAAAQUAAUBxl+SxfksYQGcmuFHrhR4AAAACAP####8BAAAAABEAAAEFAAFAeVqPXCj1wkBnrA2nQNp0AAAACQD#####AQAAAAABAAAAJgAAACcAAAACAP####8BAAAAABEAAAEFAAFAgCPyWL8ljEBpcWL8li#IAAAACQD#####AQAAAAABAAAAJgAAACkAAAASAP####8BAAAAABEAAAEAAQAAACYAAAAnAAAACgD#####AQB#AAARAAAAAAAAAAAAAABACAAAAAAAAAUAAUAR5zCZPawKAAAAKAAAAAsA#####wEAfwAADQAAAQABAAAAJgAAACwAAAAMAP####8AAAAtAAAAKgAAAA0A#####wEAfwAAEQAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAAuAAAADQD#####AAB#AAARAAAAAAAAAAAAAABACAAAAAAAAAUAAgAAAC4AAAAOAP####8BAH8AABEAAAEAAQAAACwAAAArAAAADwD#####AQB#AAARAAABAAEAAAAvAAAAKwAAABAA#####wAAfwAAEQAAAAAAAAAAAAAAQAgAAAAAAAAHAAAAADEAAAAyAAAAEQD#####AAB#AAAAADMAAAABQI9AAAAAAAAAAAAsAAAABwAAACwAAAAtAAAALgAAAC8AAAAxAAAAMgAAADMAAAACAP####8AAAD#ABEAAAEHAAFAWIo9cKPXCEBUQsX5LF+QAAAABQD#####AAAA#wAQAAABAAIAAAAVAAAANQAAAAIA#####wAAfwAAEQAAAQcAAUBihR64UeuFQHK2BtOgbToAAAAFAP####8AAH8AABAAAAEAAgAAADMAAAA3AAAAAgD#####AP8AAAARAAABBwABQHq1OgbToG1AUpgbToG06AAAAAUA#####wD#AAAAEAAAAQACAAAAJAAAADkAAAAHAP####8AAH8AAMAmAAAAAAAAwAgAAAAAAAAAAAA3EQAAAAAAAgAAAAEAAAABAAAAAAAAAAAACVRvdXJpc3RlcwAAAAcA#####wAAAP8AwCAAAAAAAADAKAAAAAAAAAAAADURAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAJRnJhbsOnYWlzAAAABwD#####AP8AAABAGAAAAAAAAMAcAAAAAAAAAAAAOREAAAAAAAAAAAABAAAAAQAAAAAAAAAAAArDiXRyYW5nZXJzAAAABwD#####AAAAAAH#####FUBgQAAAAAAAQGcKPXCj1woAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAFhAAAABwD#####AAAAAAH#####FUBvQAAAAAAAQGUqPXCj1woAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAFiAAAABwD#####AAAAAAH#####FUB2kAAAAAAAQGBqPXCj1woAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAFjAAAABwD#####AAAAAAH#####FUB0sAAAAAAAQHBVHrhR64UAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAFkAAAAB###########'
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFig, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
  }
  function modifFig (st) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété correction (booléen)
    if (st.correction) {
      // on affiche la correction
      for (let i = 0; i < 4; i++) {
        stor.mtgAppLecteur.setText(stor.mtg32svg, 62 + i, String.fromCharCode(97 + i) + '=' + stor.objCons.tabRep[i], false)
      }
    } else {
      // ceci sert si jamais on souhaite modifier la courbe initiale dans la question
      const lesTextes = textes['legende' + stor.quest].split('|')
      for (let i = 0; i < 3; i++) {
        stor.mtgAppLecteur.setText(stor.mtg32svg, 59 + i, lesTextes[i], false)
      }
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3, 4, 5].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    // le style n’est pas le même suivant que la réponse soit bonne ou fausse'
    for (let i = 1; i <= 5; i++) j3pAffiche(stor['zoneExpli' + i], '', textes['corr' + i + '_' + stor.quest], stor.objCons)
    modifFig({ correction: true })
  }

  /**
   * Appelé au 2e passage (et suivant) dans le case:enonce, passe ensuite à enonceMain
   */
  function enonceInitFirst () {
    ds = me.donneesSection

    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
    stor.tabQuest = [1, 2, 3, 4, 5]
    stor.tabQuestInit = [...stor.tabQuest]

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }
  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    const largFig = 560 // on gère ici la largeur de la figure
    const hautFig = 347
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    // création du div accueillant la figure mtg32
    stor.divMtg32 = j3pAddElt(me.zonesElts.MD, 'div')
    // et enfin la zone svg (très importante car tout est dedans)
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(stor.divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
    depart()
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    let pioche
    do {
      pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
      stor.quest = stor.tabQuest[pioche]
      // LA question 5 étant un peu plus difficile, on ne la propose pas en 1ère question
    } while (stor.quest === 5 && me.questionCourante === 1)
    stor.tabQuest.splice(pioche, 1)

    modifFig({ correction: false })

    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 6; i++) stor['zoneExpli' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.objCons = {}
    stor.objCons.styletexte = {}
    stor.objCons.tabRep = []
    let pb
    switch (stor.quest) {
      case 1:
        stor.objCons.n = 10 * j3pGetRandomInt(120, 180)
        stor.objCons.c = Math.round(stor.objCons.n / 4) + j3pGetRandomInt(14, 28)
        stor.objCons.m = Math.round(3 * stor.objCons.n / 5) + j3pGetRandomInt(0, 18)
        do {
          stor.objCons.d = Math.round(2 * stor.objCons.c / 5) + j3pGetRandomInt(0, 8)
        } while (stor.objCons.d >= stor.objCons.c - 10)
        stor.objCons.tabRep.push(stor.objCons.c - stor.objCons.d)
        stor.objCons.tabRep.push(stor.objCons.d)
        stor.objCons.tabRep.push(stor.objCons.m - stor.objCons.d)
        stor.objCons.tabRep.push(stor.objCons.n - stor.objCons.m - stor.objCons.c + stor.objCons.d)
        break
      case 2:
        stor.objCons.v = j3pGetRandomInt(70, 85)
        stor.objCons.f = Math.round(stor.objCons.v / 2) + j3pGetRandomInt(3, 7)
        stor.objCons.c = Math.round(3 * stor.objCons.v / 5) + j3pGetRandomInt(0, 8)
        stor.objCons.d = Math.round(2 * stor.objCons.c / 5) + j3pGetRandomInt(0, 3)
        stor.objCons.tabRep.push(stor.objCons.f - stor.objCons.c + stor.objCons.d)
        stor.objCons.tabRep.push(stor.objCons.c - stor.objCons.d)
        stor.objCons.tabRep.push(stor.objCons.d)
        stor.objCons.tabRep.push(stor.objCons.v - stor.objCons.f - stor.objCons.d)
        break
      case 3:
        stor.objCons.a = j3pGetRandomInt(35, 42)
        stor.objCons.e = j3pGetRandomInt(18, 23)
        stor.objCons.d = j3pGetRandomInt(7, 12)
        do {
          stor.objCons.p = j3pGetRandomInt(3, 8)
          pb = (Math.abs(stor.objCons.d - stor.objCons.p) < Math.pow(10, -12))
          pb = (pb || stor.objCons.p > stor.objCons.a - stor.objCons.e - 4)
          pb = (pb || Math.abs(stor.objCons.e - stor.objCons.d - stor.objCons.d) < Math.pow(10, -12))
          pb = (pb || Math.abs(stor.objCons.e - stor.objCons.d - stor.objCons.p) < Math.pow(10, -12))
          pb = (pb || Math.abs(stor.objCons.a - stor.objCons.e - 2 * stor.objCons.p) < Math.pow(10, -12))
          pb = (pb || Math.abs(stor.objCons.a - stor.objCons.e - stor.objCons.p - stor.objCons.d) < Math.pow(10, -12))
          pb = (pb || Math.abs(stor.objCons.a - 2 * stor.objCons.e - stor.objCons.p + stor.objCons.d) < Math.pow(10, -12))
        } while (pb)
        stor.objCons.tabRep.push(stor.objCons.a - stor.objCons.e - stor.objCons.p)
        stor.objCons.tabRep.push(stor.objCons.p)
        stor.objCons.tabRep.push(stor.objCons.d)
        stor.objCons.tabRep.push(stor.objCons.e - stor.objCons.d)
        break
      case 4:
        stor.objCons.e = j3pGetRandomInt(29, 34)
        stor.objCons.m = j3pGetRandomInt(13, 19)
        stor.objCons.n = j3pGetRandomInt(4, 7)
        do {
          stor.objCons.f = stor.objCons.m + j3pGetRandomInt(3, 8)
          pb = (Math.abs(stor.objCons.f - stor.objCons.n) < Math.pow(10, -12))
          pb = (pb || stor.objCons.p > stor.objCons.a - stor.objCons.e - 4)
          pb = (pb || Math.abs(stor.objCons.m - stor.objCons.n - stor.objCons.n) < Math.pow(10, -12))
          pb = (pb || Math.abs(stor.objCons.f - 2 * stor.objCons.m + stor.objCons.n) < Math.pow(10, -12))
          pb = (pb || Math.abs(stor.objCons.e + stor.objCons.m - 2 * stor.objCons.f) < Math.pow(10, -12))
          pb = (pb || Math.abs(stor.objCons.e - stor.objCons.f - stor.objCons.m + stor.objCons.n) < Math.pow(10, -12))
          pb = (pb || Math.abs(stor.objCons.e - stor.objCons.f - stor.objCons.n) < Math.pow(10, -12))
        } while (pb)
        stor.objCons.tabRep.push(stor.objCons.m - stor.objCons.n)
        stor.objCons.tabRep.push(stor.objCons.n)
        stor.objCons.tabRep.push(stor.objCons.f - stor.objCons.m)
        stor.objCons.tabRep.push(stor.objCons.e - stor.objCons.f)
        break
      case 5:
        stor.objCons.a = 5 * j3pGetRandomInt(24, 30)
        stor.objCons.r = j3pGetRandomInt(31, 37)
        stor.objCons.t = j3pGetRandomInt(24, 30)
        stor.objCons.d = j3pGetRandomInt(5, 11)
        stor.objCons.n = stor.objCons.a - (stor.objCons.r + stor.objCons.t - stor.objCons.d)
        stor.objCons.tabRep.push(stor.objCons.r - stor.objCons.d)
        stor.objCons.tabRep.push(stor.objCons.d)
        stor.objCons.tabRep.push(stor.objCons.t - stor.objCons.d)
        stor.objCons.tabRep.push(stor.objCons.n)
        break
    }
    j3pAffiche(stor.zoneExpli1, '', textes['consigne1_' + stor.quest], stor.objCons)
    j3pAffiche(stor.zoneExpli2, '', textes.consigne2)
    const elts = []
    elts.push(j3pAffiche(stor.zoneExpli3, '', 'a=&1&', { styletexte: '', inputmq1: { texte: '', dynamique: true, width: '12px', maxchars: 6 } }))
    elts.push(j3pAffiche(stor.zoneExpli4, '', 'b=&1&', { styletexte: '', inputmq1: { texte: '', dynamique: true, width: '12px', maxchars: 6 } }))
    elts.push(j3pAffiche(stor.zoneExpli5, '', 'c=&1&', { styletexte: '', inputmq1: { texte: '', dynamique: true, width: '12px', maxchars: 6 } }))
    elts.push(j3pAffiche(stor.zoneExpli6, '', 'd=&1&', { styletexte: '', inputmq1: { texte: '', dynamique: true, width: '12px', maxchars: 6 } }))
    stor.zoneInput = elts.map(elt => elt.inputmqList[0])
    for (let i = 1; i <= 4; i++) {
      stor.zoneInput[i - 1].typeReponse = ['nombre', 'exact']
      stor.zoneInput[i - 1].reponse = [stor.objCons.tabRep[i - 1]]
      mqRestriction(stor.zoneInput[i - 1], '\\d-')
    }
    j3pFocus(stor.zoneInput[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // Paramétrage de la validation
    // zonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const zonesSaisie = stor.zoneInput.map(elt => elt.id)
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({ parcours: me, zones: zonesSaisie })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si les zones ont bien été complétées ou non
        // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
        // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
        // Si c’est la dernière tentative de répondre, on barre également les zones fausses
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          this.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",this.etat)
}
