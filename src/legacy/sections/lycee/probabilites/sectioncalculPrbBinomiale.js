import { j3pNombre, j3pFocus, j3pGetRandomInt, j3pRandomTab, j3pVirgule, j3pAddElt, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        février 2020
        calcul de probabilités de la forme P(X<k), P(X<=k) ou P(k1<X<=k2)
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Loi binomiale : probabilités',
  // on donne les phrases de la consigne
  consigne1: 'Soit $£x$ une variable aléatoire suivant la loi binomiale $B(£n;£p)$.',
  consigne2_1: '$P(£x<£k)\\approx$&1&',
  consigne2_2: '$P(£x\\geq£k)\\approx$&1&',
  consigne2_3: '$P(£{k1} < £x \\leq £{k2})\\approx$&1&',
  consigne3: 'Arrondir à $10^{-3}$.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Une probabilité est un nombre compris entre 0 et 1&nbsp;!',
  comment2: 'Peut-être un problème d’arrondi&nbsp;?',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'La calculatrice permet de calculer $P(£x\\leq k)$ où $k$ est un entier compris entre 0 et £n : dans la partie distributions, choisir "loi Binomiale Cumulative" (Bcd, cdf ou FRep suivant les modèles). On complète ensuite par les paramètres de la loi binomiale (£n et £{prb}) et par la valeur $k$.',
  corr2_1: 'Ici, $P(£x<£k)=P(£x\\leq calcule[£k-1])\\approx £r$.',
  corr2_2: 'Ici, $P(£x\\geq £k)=1-P(£x< £k)=1-P(£x\\leq calcule[£k-1])\\approx £r$.',
  corr2_3: 'Ici, $P(£{k1} < £x\\leq £{k2})=P(£x \\leq £{k2})-P(£x\\leq £{k1})$.',
  corr2_31: '$P(£{k1} < £x\\leq £{k2})\\approx £{r2}-£{r1}$',
  corr2_32: '$P(£{k1} < £x\\leq £{k2})\\approx £r$',
  corr3Suite: 'Attention à ne pas trop arrondir les résultats intermédiaires pour avoir le bon arrondi final.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pAffiche(stor.zoneExpli1, '', textes.corr1, stor.objCons)
    j3pAffiche(stor.zoneExpli2, '', textes['corr2_' + stor.tabQuest[me.questionCourante - 1]], stor.objCons)
    if (stor.tabQuest[me.questionCourante - 1] === 3) {
      ;[3, 4, 5].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
      j3pAffiche(stor.zoneExpli3, '', textes.corr2_31, stor.objCons)
      j3pAffiche(stor.zoneExpli4, '', textes.corr2_32, stor.objCons)
      j3pAffiche(stor.zoneExpli5, '', textes.corr3Suite, stor.objCons)
    }
  }

  function combinaison (n, p) {
    // Cette fonction calcule C_n^p sans utiliser les factorielles pour gagner un peu de temps
    if (n === p || p === 0) return 1
    let res = n
    let i = n - 1
    while (i > Math.max(p, n - p)) {
      res *= i
      i -= 1
    }
    i = 2
    while (i <= Math.min(p, n - p)) {
      res = Math.round(res / i)
      i += 1
    }
    // console.log("combinaison(",n,",",p,")=",res)
    return res
  }

  function enonceInitFirst () {
    ds = me.donneesSection

    // Le paramètre définit la largeur relative de la première colonne
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    // création du choix des questions : 1 pour P(X<k), 2 pour P(X>=k) et 3 pour P(k1<X<=k2)
    const choix1 = j3pGetRandomInt(1, 2)
    stor.tabQuest = [choix1]
    const tabChoix2 = [3 - choix1, 3]
    if (ds.nbrepetitions === 2) {
      stor.tabQuest.push(3)
    } else {
      for (let k = 1; k <= 2; k++) {
        const leChoix = j3pGetRandomInt(0, tabChoix2.length - 1)
        stor.tabQuest.push(tabChoix2[leChoix])
        tabChoix2.splice(leChoix, 1)
      }
    }
    if (ds.nbrepetitions > 3) {
      let tabChoix3 = [1, 2, 3]
      const tabChoix3Init = [1, 2, 3]
      for (let k = 4; k <= ds.nbrepetitions; k++) {
        if (tabChoix3.length === 0) tabChoix3 = [...tabChoix3Init]
        const leChoix = j3pGetRandomInt(0, tabChoix3.length - 1)
        stor.tabQuest.push(tabChoix3[leChoix])
        tabChoix3.splice(leChoix, 1)
      }
    }
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    stor.objCons = {}
    let nbTests
    do {
      nbTests = 0 // cela me sert à éviter de tomber sur un cas particulier où on ne trouverait pas de proba convenable
      stor.objCons.n = j3pGetRandomInt(0, 10) + 12
      stor.objCons.p = (stor.tabQuest[me.questionCourante - 1] === 3) ? (30 + j3pGetRandomInt(0, 40)) / 100 : (20 + j3pGetRandomInt(0, 60)) / 100
      stor.objCons.prb = j3pVirgule(stor.objCons.p)
      stor.objCons.x = j3pRandomTab(['X', 'Y', 'Z'], [0.33, 0.33, 0.34])
      stor.objCons.a = 3 // Pour l’arrondi
      stor.objCons.tabProba = []// tableau de toutes les proba de la forme P(X<=k) avec k dans [[0;n]]
      stor.objCons.tabProba.push(Math.pow(1 - stor.objCons.p, stor.objCons.n))
      for (let i = 1; i <= stor.objCons.n; i++) {
        stor.objCons.tabProba.push(stor.objCons.tabProba[i - 1] + combinaison(stor.objCons.n, i) * Math.pow(stor.objCons.p, i) * Math.pow(1 - stor.objCons.p, stor.objCons.n - i))
      }
      do {
        nbTests++
        stor.objCons.k = j3pGetRandomInt(3, stor.objCons.n - 3)
      } while ((stor.tabQuest[me.questionCourante - 1] <= 2) && ((stor.objCons.tabProba[stor.objCons.k - 1] < 0.002) || (stor.objCons.tabProba[stor.objCons.k - 1] > 0.998)) && (nbTests < 50))
      nbTests = 0
      do {
        nbTests++
        stor.objCons.k1 = j3pGetRandomInt(3, Math.floor(stor.objCons.n / 2))
        stor.objCons.k2 = j3pGetRandomInt(Math.ceil(stor.objCons.n / 2), stor.objCons.n - 3)
      } while ((stor.tabQuest[me.questionCourante - 1] === 3) && ((stor.objCons.k2 - stor.objCons.k1 < 3) || (stor.objCons.tabProba[stor.objCons.k1 - 1] < 0.002) || (stor.objCons.tabProba[stor.objCons.k2] > 0.998)) && (nbTests < 50))
    } while (nbTests === 50)
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objCons)
    let elt
    if (stor.tabQuest[me.questionCourante - 1] === 3) {
      elt = j3pAffiche(stor.zoneCons2, '', textes['consigne2_' + stor.tabQuest[me.questionCourante - 1]], {
        k1: stor.objCons.k1,
        k2: stor.objCons.k2,
        x: stor.objCons.x,
        inputmq1: { texte: '', dynamique: true, width: '12px' }
      })
    } else {
      elt = j3pAffiche(stor.zoneCons2, '', textes['consigne2_' + stor.tabQuest[me.questionCourante - 1]], {
        k: stor.objCons.k,
        x: stor.objCons.x,
        inputmq1: { texte: '', dynamique: true, width: '12px' }
      })
    }
    stor.zoneInput = elt.inputmqList[0]
    stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.zoneCons3, { fontStyle: 'italic', fontSize: '0.9em' })
    j3pAffiche(stor.zoneCons3, '', textes.consigne3)
    stor.zoneInput.typeReponse = ['nombre', 'arrondi', Math.pow(10, -stor.objCons.a)]
    mqRestriction(stor.zoneInput, '\\d,.\\-')

    stor.objCons.rep = (stor.tabQuest[me.questionCourante - 1] === 1)
      ? stor.objCons.tabProba[stor.objCons.k - 1]
      : (stor.tabQuest[me.questionCourante - 1] === 2)
          ? 1 - stor.objCons.tabProba[stor.objCons.k - 1]
          : stor.objCons.tabProba[stor.objCons.k2] - stor.objCons.tabProba[stor.objCons.k1]
    stor.objCons.r = Math.round(stor.objCons.rep * Math.pow(10, 3)) / Math.pow(10, 3)
    if (stor.tabQuest[me.questionCourante - 1] === 3) {
      stor.objCons.r1 = Math.round(stor.objCons.tabProba[stor.objCons.k1] * Math.pow(10, 5)) / Math.pow(10, 5)
      stor.objCons.r2 = Math.round(stor.objCons.tabProba[stor.objCons.k2] * Math.pow(10, 5)) / Math.pow(10, 5)
    }
    me.logIfDebug('la réponse:', stor.objCons.rep)
    stor.zoneInput.reponse = [stor.objCons.rep]
    j3pFocus(stor.zoneInput)
    me.logIfDebug('stor.objCons.tabProba:', stor.objCons.tabProba)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          this.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // on vérifie si on n’a pas un pb sur la proba saisie (inférieure à 0 ou supérieure à 1)
              const valeurSaisie = j3pNombre(stor.fctsValid.zones.reponseSaisie[0])
              const probaOK = (valeurSaisie >= 0 && valeurSaisie <= 1)
              // On regarde également un éventuel problème d’arrondi
              const pbArrondi = (Math.abs(j3pNombre(stor.fctsValid.zones.reponseSaisie[0]) - stor.objCons.rep) < 3 * Math.pow(10, -stor.objCons.a))
              if (!probaOK) {
                stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
              } else if (pbArrondi) {
                stor.zoneCorr.innerHTML += '<br/>' + textes.comment2
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
