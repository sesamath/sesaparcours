import { j3pAddElt, j3pNombre, j3pFocus, j3pGetRandomInt, j3pVirgule, j3pStyle, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { genereAlea, constructionTabIntervalle } from 'src/legacy/outils/fonctions/gestionParametres'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    février 2017
    Dans cette section, on a une loi normale dont on donne la courbe représentant la densité
    L’espérance est donnée, mais pas l’écart-type
    On représente une proba sous la forme d’une aire et on en demande une autre à l’aide des caractéristiques de symétrie
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeLoi', 'quelconque', 'liste', 'On peut imposer la loi normale : <br/>- "centreeReduite" pour avoir N(0;1);<br/>-"centree" pour avoir N(0;sigma^2);<br/>-"quelconque" (par défaut) pour N(mu;sigma^2).', ['quelconque', 'centreeReduite', 'centree']],
    ['mu', '[0;12]|0', 'string', 'L’espérance de la loi normale est par défaut un entier de l’intervalle [0;12]. On peut imposer un nombre de cet intervalle avec 2 chiffres après la virgule en écrivant "[0;12]|2" (le séparateur | étant important), on peut aussi imposer une valeur particulière en écrivant "2.3" par exemple.'],
    ['sigma', '[1;4]|1', 'string', 'L’écart-type de la loi normale est par défaut un décimal de l’intervalle [1;4] contenant un chiffre après la virgule. On peut imposer un nombre de cet intervalle avec 2 chiffres après la virgule en écrivant "[1;4]|2" (le séparateur | étant important), on peut aussi imposer une valeur particulière en écrivant "2.3" par exemple.'],
    ['probaPhrase', true, 'boolean', 'Ce paramètre vaut \'true\' (par défaut) si on souhaite que la probabilité donnée le soit sous la forme d’une phrase. Sinon il sera écrit P(...)=...']
  ]
}

/**
 * section loinormaleAire
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3, 4].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })

    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1,
      {
        e: stor.ecart1,
        f: stor.ecart2,
        v: stor.valeur,
        m: stor.mu
      })
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2, { p: stor.p1 })
    j3pAffiche(stor.zoneExpli3, '', ds.textes.corr3, { p: stor.p2, q: stor.p3 })
    j3pAffiche(stor.zoneExpli4, '', ds.textes.corr4)
    // Je modifie la figure pour que la correction apparaisse
    modifFig({ correction: true }, function () {})
    // on rend maintenant inactive la figure mtg32 (on ne peut plus déplacer les points)
    stor.mtgAppLecteur.setActive(stor.mtg32svg, false)
  }

  function depart (callback) {
    stor.mtgAppLecteur.removeAllDoc()
    // txt html de la figure
    // txtFigure = "TWF0aEdyYXBoSmF2YTEuMAAAAA8+TMzNAANmcmH###8BAP8BAAAAAAAAAAACJAAAANoAAAAAAAAAAAAAAAAAAABj#####wAAAAEAEW9iamV0cy5DQ2FsY0NvbnN0AP####8AAnBpABYzLjE0MTU5MjY1MzU4OTc5MzIzODQ2#####wAAAAEAEW9iamV0cy5DQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAWb2JqZXRzLkNWYXJpYWJsZUJvcm5lZQD#####AAVzaWdtYT#wAAAAAAAAP+AAAAAAAABACAAAAAAAAD+5mZmZmZmaAAADMC41AAEzAAMwLjEAAAACAP####8AAm11AAAAAAAAAADAAAAAAAAAAEAAAAAAAAAAP8mZmZmZmZoAAAItMgABMgADMC4y#####wAAAAEADm9iamV0cy5DQ2FsY3VsAP####8AAmE2AAQtMC41#####wAAAAEAE29iamV0cy5DTW9pbnNVbmFpcmUAAAABP+AAAAAAAAAAAAADAP####8AAmE1AAItMQAAAAQAAAABP#AAAAAAAAD#####AAAAAQARb2JqZXRzLkNQb2ludEJhc2UA#####wEAAAAAEAABTwDALgAAAAAAAEAQAAAAAAAABQABQHEAAAAAAABAZIAAAAAAAP####8AAAABABtvYmpldHMuQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAADwAAAQABAAAABQE#6qqqqqqqq#####8AAAABABZvYmpldHMuQ1BvaW50TGllRHJvaXRlAP####8BAAAAARAAAUkAwBAAAAAAAABAEAAAAAAAAAUAAUBXgAAAAAAAAAAABv####8AAAABABBvYmpldHMuQ0Ryb2l0ZUFCAP####8AAAAAABAAAAEAAQAAAAUAAAAH#####wAAAAEAHW9iamV0cy5DRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAAAAAA8AAAEAAQAAAAUAAAAGAAAABwD#####AQAAAAEQAAFKAMAmAAAAAAAAAAAAAAAAAAAFAAHAdUAAAAAAAAAAAAkAAAAIAP####8BAAAAABAAAAEAAQAAAAUAAAAK#####wAAAAIADm9iamV0cy5DUmVwZXJlAP####8AgICAAAAAAAEBAQAAAAUAAAAHAAAACgAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAP####8AAAABABBvYmpldHMuQ0xvbmd1ZXVyAP####8AAAAFAAAABwAAAAMA#####wAHbmJncmFkeAACMjAAAAABQDQAAAAAAAAAAAADAP####8AB25iZ3JhZHkAAjIwAAAAAUA0AAAAAAAA#####wAAAAEAG29iamV0cy5DSW1wbGVtZW50YXRpb25Qcm90bwD#####ABdHcmFkdWF0aW9uQXhlc1JlcGVyZU5ldwAAABsAAAAIAAAAAwAAAAwAAAAOAAAAD#####8AAAABABpvYmpldHMuQ0Fic2Npc3NlT3JpZ2luZVJlcAAAAAAQAAVhYnNvcgAAAAz#####AAAAAQAab2JqZXRzLkNPcmRvbm5lZU9yaWdpbmVSZXAAAAAAEAAFb3Jkb3IAAAAM#####wAAAAEAEW9iamV0cy5DVW5pdGV4UmVwAAAAABAABnVuaXRleAAAAAz#####AAAAAQARb2JqZXRzLkNVbml0ZXlSZXAAAAAAEAAGdW5pdGV5AAAADP####8AAAABABdvYmpldHMuQ1BvaW50RGFuc1JlcGVyZQAAAAAQAAAAAAAOAAABBQAAAAAM#####wAAAAEAFm9iamV0cy5DUmVzdWx0YXRWYWxldXIAAAARAAAAEgAAABIAAAARAAAAABAAAAAAAA4AAAEFAAAAAAz#####AAAAAQARb2JqZXRzLkNPcGVyYXRpb24AAAAAEgAAABEAAAASAAAAEwAAABIAAAASAAAAEQAAAAAQAAAAAAAOAAABBQAAAAAMAAAAEgAAABEAAAATAAAAABIAAAASAAAAEgAAABT#####AAAAAQASb2JqZXRzLkNIb21vdGhldGllAAAAABAAAAAVAAAAEgAAAA7#####AAAAAQASb2JqZXRzLkNQb2ludEltYWdlAAAAABAAAAAAAA4AAAEFAAAAABYAAAAYAAAAFAAAAAAQAAAAFQAAABIAAAAPAAAAFQAAAAAQAAAAAAAOAAABBQAAAAAXAAAAGv####8AAAABAA9vYmpldHMuQ1NlZ21lbnQAAAAAEAEAAAAAAAAAAQABAAAAFgAAABkAAAAWAAAAABABAAAAAAAAAAEAAQAAABcAAAAbAAAABwAAAAAQAQAAAAALAAFXAMAUAAAAAAAAwDQAAAAAAAAFAAE#3FZ4mrzfDgAAABz#####AAAAAgAPb2JqZXRzLkNNZXN1cmVYAAAAABAAB3hDb29yZDEAAAAMAAAAHgAAAAMAAAAAEAAFYWJzdzEAB3hDb29yZDEAAAASAAAAH#####8AAAACABlvYmpldHMuQ0xpZXVPYmpldFBhclB0TGllAQAAABABZmZmAAIAAAEAAAAeAAAAEgAAAA4AAAAeAAAAAgAAAB4AAAAeAAAAAwAAAAAQAAVhYnN3MgANMiphYnNvci1hYnN3MQAAABMBAAAAEwIAAAABQAAAAAAAAAAAAAASAAAAEQAAABIAAAAgAAAAEQAAAAAQAQAAAAALAAABBQAAAAAMAAAAEgAAACIAAAASAAAAEgAAABgBAAAAEAFmZmYAAgAAAQAAACMAAAASAAAADgAAAB4AAAAFAAAAHgAAAB8AAAAgAAAAIgAAACMAAAAHAAAAABABAAAAAAsAAVIAQCAAAAAAAADAIAAAAAAAAAUAAT#RG06BtOgfAAAAHf####8AAAACAA9vYmpldHMuQ01lc3VyZVkAAAAAEAAHeUNvb3JkMQAAAAwAAAAlAAAAAwAAAAAQAAVvcmRyMQAHeUNvb3JkMQAAABIAAAAmAAAAGAEAAAAQAWZmZgACAAABAAAAJQAAABIAAAAPAAAAJQAAAAIAAAAlAAAAJQAAAAMAAAAAEAAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAATAQAAABMCAAAAAUAAAAAAAAAAAAAAEgAAABIAAAASAAAAJwAAABEAAAAAEAEAAAAACwAAAQUAAAAADAAAABIAAAARAAAAEgAAACkAAAAYAQAAABABZmZmAAIAAAEAAAAqAAAAEgAAAA8AAAAlAAAABQAAACUAAAAmAAAAJwAAACkAAAAq#####wAAAAIAE29iamV0cy5DQ29tbWVudGFpcmUAAAAAEAFmZmYAAgAAAAAAAAAAAAAAQBgAAAAAAAAAAAAeCwAB####AAAAAQAAAAAACyNWYWwoYWJzdzEpAAAAGAEAAAAQAWZmZgACAAABAAAALAAAABIAAAAOAAAAHgAAAAQAAAAeAAAAHwAAACAAAAAsAAAAGgAAAAAQAWZmZgACAAAAAAAAAAAAAABAGAAAAAAAAAAAACMLAAH###8AAAABAAAAAAALI1ZhbChhYnN3MikAAAAYAQAAABABZmZmAAIAAAEAAAAuAAAAEgAAAA4AAAAeAAAABgAAAB4AAAAfAAAAIAAAACIAAAAjAAAALgAAABoAAAAAEAFmZmYAAgAAAMAgAAAAAAAAP#AAAAAAAAAAAAAlCwAB####AAAAAgAAAAEACyNWYWwob3JkcjEpAAAAGAEAAAAQAWZmZgACAAABAAAAMAAAABIAAAAPAAAAJQAAAAQAAAAlAAAAJgAAACcAAAAwAAAAGgAAAAAQAWZmZgACAAAAwBwAAAAAAAAAAAAAAAAAAAAAACoLAAH###8AAAACAAAAAQALI1ZhbChvcmRyMikAAAAYAQAAABABZmZmAAIAAAEAAAAyAAAAEgAAAA8AAAAlAAAABgAAACUAAAAmAAAAJwAAACkAAAAqAAAAMv####8AAAABAAxvYmpldHMuQ0ZvbmMA#####wABZgAvMS8oc2lnbWEqc3FydCgyKnBpKSkqZXhwKC0wLjUqKCh4LW11KS9zaWdtYSleMikAAAATAgAAABMDAAAAAT#wAAAAAAAAAAAAEwIAAAASAAAAAf####8AAAACABBvYmpldHMuQ0ZvbmN0aW9uAQAAABMCAAAAAUAAAAAAAAAAAAAAEgAAAAAAAAAcBwAAAAQAAAATAgAAAAE#4AAAAAAAAP####8AAAABABFvYmpldHMuQ1B1aXNzYW5jZQAAABMDAAAAEwH#####AAAAAgAYb2JqZXRzLkNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAABIAAAACAAAAEgAAAAEAAAABQAAAAAAAAAAAAXgAAAAHAP####8BAAAAAA8AAngxAQUAAb#zhYQjXZxPAAAACAAAABcA#####wAHeENvb3JkMQAAAAwAAAA1AAAAAwD#####AAJ4MQAHeENvb3JkMQAAABIAAAA2AAAAAwD#####AAJ5MQAFZih4MSn#####AAAAAQAVb2JqZXRzLkNBcHBlbEZvbmN0aW9uAAAANAAAABIAAAA3AAAAEQD#####AQAAAAAPAAABBQAAAAAMAAAAEgAAADcAAAASAAAAOP####8AAAACABRvYmpldHMuQ0xpZXVEZVBvaW50cwD#####AAAAAAAAAAABAAEAAAA5AAAB9AABAAAANQAAAAUAAAA1AAAANgAAADcAAAA4AAAAOQAAAAMA#####wABYQAELTAuMgAAAAQAAAABP8mZmZmZmZoAAAADAP####8AAWIAAzEuNQAAAAE#+AAAAAAAAAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAOwAAAAEAAAAAAAAAAAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAOwAAAB8AAAA0AAAAEgAAADsAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEgAAADwAAAABAAAAAAAAAAAAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEgAAADwAAAAfAAAANAAAABIAAAA8AAAAFgD#####AQAAAAAAAAABAAEAAAA9AAAAPwAAAAcA#####wEAAAAADwACeDIBBQABP9EvGjNvbeQAAABBAAAAFwD#####AAd4Q29vcmQyAAAADAAAAEIAAAADAP####8AAngyAAd4Q29vcmQyAAAAEgAAAEMAAAADAP####8AAnkyAAVmKHgyKQAAAB8AAAA0AAAAEgAAAEQAAAARAP####8BAAAAAA8AAAEFAAAAAAwAAAASAAAARAAAABIAAABFAAAAIAD#####AAAAAAAAAAABAAEAAABGAAAB9AABAAAAQgAAAAUAAABCAAAAQwAAAEQAAABFAAAARv####8AAAABABlvYmpldHMuQ1N1cmZhY2VMaWV1RHJvaXRlAP####8AAAD#AAAAAAEAAAAFAAAARwAAAAj#####AAAAAgATb2JqZXRzLkNEcm9pdGVQYXJFcQD#####AQAAAAAPAAABAAEAAAAMAAAAEwgAAAAeAAAAAAAAABIAAAACAAAAEwEAAAAeAAAAAAAAABIAAAACAAAAAwD#####AAJhMQABMAAAAAEAAAAAAAAAAAAAAAMA#####wACYTIAATEAAAABP#AAAAAAAAAAAAAWAP####8AAAD#AAAAAAEAAQAAAD4AAAA9AAAAFgD#####AAAA#wAAAAABAAEAAABAAAAAP#####8AAAABABFvYmpldHMuQ0ludGVncmFsZQD#####AAFJAAAANAAAABIAAAA7AAAAEgAAADwAAAABQFkAAAAAAAD#####AAAAAgANb2JqZXRzLkNMYXRleAD#####AAAA#wACAAAAAAAAAAAAAABAGAAAAAAAAAAAAD0QAAAAAAABAAAAAAAIXFZhbHthMX0AAAAkAP####8AAAD#AAIAAAAAAAAAAAAAAEAYAAAAAAAAAAAAPxAAAAAAAAEAAAAAAAhcVmFse2EyfQAAAAMA#####wACYTMAAi0xAAAABAAAAAE#8AAAAAAAAAAAAAMA#####wACYTQABC0wLjUAAAAEAAAAAT#gAAAAAAAAAAAAEQD#####AQB#AAAPAAAAAAAAAAAAAABACAAAAAAAAAUAAAAADAAAABIAAABRAAAAHwAAADQAAAASAAAAUQAAABEA#####wAAfwAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAUQAAAAEAAAAAAAAAAAAAABEA#####wAAfwAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAUgAAAAEAAAAAAAAAAAAAABEA#####wEAfwAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAUgAAAB8AAAA0AAAAEgAAAFIAAAAWAP####8AAH8AAAAAAAEAAQAAAFMAAABUAAAAFgD#####AAB#AAAAAAABAAEAAABVAAAAVgAAABYA#####wEAAAAAAAAAAQABAAAAVAAAAFUAAAAHAP####8BAAAAABAAAngzAQUAAT#ht6mhLV2bAAAAWQAAABcA#####wAHeENvb3JkMwAAAAwAAABaAAAAAwD#####AAJ4MwAHeENvb3JkMwAAABIAAABbAAAAAwD#####AAJ5MwAFZih4MykAAAAfAAAANAAAABIAAABcAAAAEQD#####AQAAAAAQAAABBQAAAAAMAAAAEgAAAFwAAAASAAAAXQAAACAA#####wEAfwAAAAAAAQABAAAAXgAAAfQAAQAAAFoAAAAFAAAAWgAAAFsAAABcAAAAXQAAAF4AAAAhAP####8AAH8AAAAAAAEAAAAFAAAAXwAAAAgAAAAkAP####8AAH8AAAIAAADAFAAAAAAAAEA5AAAAAAAAAAAAVBAAAAAAAAEAAAAAAAhcVmFse2E1fQAAACQA#####wAAfwAAAgAAAMAAAAAAAAAAQDkAAAAAAAAAAABVEAAAAAAAAQAAAAAACFxWYWx7YTZ9AAAADf##########"
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH2+v4BAP8BAAAAAAAAAAACJAAAAM8AAAAAAAAAAAAAAAAAAABj#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEAD0NWYXJpYWJsZUJvcm5lZQD#####AAVzaWdtYT#wAAAAAAAAP+AAAAAAAABACAAAAAAAAD+5mZmZmZmaAAADMC41AAEzAAMwLjEAAAACAP####8AAm11AAAAAAAAAADAAAAAAAAAAEAAAAAAAAAAP8mZmZmZmZoAAAItMgABMgADMC4y#####wAAAAEAB0NDYWxjdWwA#####wACYTYABC0wLjX#####AAAAAQAMQ01vaW5zVW5haXJlAAAAAT#gAAAAAAAAAAAAAwD#####AAJhNQACLTEAAAAEAAAAAT#wAAAAAAAA#####wAAAAEACkNQb2ludEJhc2UA#####wEAAAAAEAABTwDALgAAAAAAAEAQAAAAAAAABQABQHEQAAAAAABAYUAAAAAAAP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AAAAAAAPAAABAAEAAAAFAT#qqqqqqqqr#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAEQAAFJAMAQAAAAAAAAQBAAAAAAAAAFAAFAV4AAAAAAAAAAAAb#####AAAAAQAJQ0Ryb2l0ZUFCAP####8AAAAAABAAAAEAAQAAAAUAAAAH#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAADwAAAQABAAAABQAAAAYAAAAHAP####8BAAAAARAAAUoAwCYAAAAAAAAAAAAAAAAAAAUAAcB1QAAAAAAAAAAACQAAAAgA#####wEAAAAAEAAAAQABAAAABQAAAAr#####AAAAAgAHQ1JlcGVyZQD#####AICAgAEBAAAABQAAAAcAAAAKAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACUNMb25ndWV1cgD#####AAAABQAAAAcAAAADAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAAAwD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABdHcmFkdWF0aW9uQXhlc1JlcGVyZU5ldwAAABsAAAAIAAAAAwAAAAwAAAAOAAAAD#####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAABAABWFic29yAAAADP####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAABAABW9yZG9yAAAADP####8AAAABAApDVW5pdGV4UmVwAAAAABAABnVuaXRleAAAAAz#####AAAAAQAKQ1VuaXRleVJlcAAAAAAQAAZ1bml0ZXkAAAAM#####wAAAAEAEENQb2ludERhbnNSZXBlcmUAAAAAEAAAAAAADgAAAQUAAAAADP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAARAAAAEgAAABIAAAARAAAAABAAAAAAAA4AAAEFAAAAAAz#####AAAAAQAKQ09wZXJhdGlvbgAAAAASAAAAEQAAABIAAAATAAAAEgAAABIAAAARAAAAABAAAAAAAA4AAAEFAAAAAAwAAAASAAAAEQAAABMAAAAAEgAAABIAAAASAAAAFP####8AAAABAAtDSG9tb3RoZXRpZQAAAAAQAAAAFQAAABIAAAAO#####wAAAAEAC0NQb2ludEltYWdlAAAAABAAAAAAAA4AAAEFAAAAABYAAAAYAAAAFAAAAAAQAAAAFQAAABIAAAAPAAAAFQAAAAAQAAAAAAAOAAABBQAAAAAXAAAAGv####8AAAABAAhDU2VnbWVudAAAAAAQAQAAAAANAAABAAEAAAAWAAAAGQAAABYAAAAAEAEAAAAADQAAAQABAAAAFwAAABsAAAAHAAAAABABAAAAAAsAAVcAwBQAAAAAAADANAAAAAAAAAUAAT#cVniavN8OAAAAHP####8AAAACAAhDTWVzdXJlWAAAAAAQAAd4Q29vcmQxAAAADAAAAB4AAAADAAAAABAABWFic3cxAAd4Q29vcmQxAAAAEgAAAB######AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAABABZmZmAAAAHgAAABIAAAAOAAAAHgAAAAIAAAAeAAAAHgAAAAMAAAAAEAAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAATAQAAABMCAAAAAUAAAAAAAAAAAAAAEgAAABEAAAASAAAAIAAAABEAAAAAEAEAAAAACwAAAQUAAAAADAAAABIAAAAiAAAAEgAAABIAAAAYAQAAABABZmZmAAAAIwAAABIAAAAOAAAAHgAAAAUAAAAeAAAAHwAAACAAAAAiAAAAIwAAAAcAAAAAEAEAAAAACwABUgBAIAAAAAAAAMAgAAAAAAAABQABP9EbToG06B8AAAAd#####wAAAAIACENNZXN1cmVZAAAAABAAB3lDb29yZDEAAAAMAAAAJQAAAAMAAAAAEAAFb3JkcjEAB3lDb29yZDEAAAASAAAAJgAAABgBAAAAEAFmZmYAAAAlAAAAEgAAAA8AAAAlAAAAAgAAACUAAAAlAAAAAwAAAAAQAAVvcmRyMgANMipvcmRvci1vcmRyMQAAABMBAAAAEwIAAAABQAAAAAAAAAAAAAASAAAAEgAAABIAAAAnAAAAEQAAAAAQAQAAAAALAAABBQAAAAAMAAAAEgAAABEAAAASAAAAKQAAABgBAAAAEAFmZmYAAAAqAAAAEgAAAA8AAAAlAAAABQAAACUAAAAmAAAAJwAAACkAAAAq#####wAAAAIADENDb21tZW50YWlyZQAAAAAQAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAHgsAAf###wAAAAEAAAAAAAsjVmFsKGFic3cxKQAAABgBAAAAEAFmZmYAAAAsAAAAEgAAAA4AAAAeAAAABAAAAB4AAAAfAAAAIAAAACwAAAAaAAAAABABZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAjCwAB####AAAAAQAAAAAACyNWYWwoYWJzdzIpAAAAGAEAAAAQAWZmZgAAAC4AAAASAAAADgAAAB4AAAAGAAAAHgAAAB8AAAAgAAAAIgAAACMAAAAuAAAAGgAAAAAQAWZmZgDAIAAAAAAAAD#wAAAAAAAAAAAAJQsAAf###wAAAAIAAAABAAsjVmFsKG9yZHIxKQAAABgBAAAAEAFmZmYAAAAwAAAAEgAAAA8AAAAlAAAABAAAACUAAAAmAAAAJwAAADAAAAAaAAAAABABZmZmAMAcAAAAAAAAAAAAAAAAAAAAAAAqCwAB####AAAAAgAAAAEACyNWYWwob3JkcjIpAAAAGAEAAAAQAWZmZgAAADIAAAASAAAADwAAACUAAAAGAAAAJQAAACYAAAAnAAAAKQAAACoAAAAy#####wAAAAEABUNGb25jAP####8AAWYALzEvKHNpZ21hKnNxcnQoMipwaSkpKmV4cCgtMC41KigoeC1tdSkvc2lnbWEpXjIpAAAAEwIAAAATAwAAAAE#8AAAAAAAAAAAABMCAAAAEgAAAAH#####AAAAAgAJQ0ZvbmN0aW9uAQAAABMCAAAAAUAAAAAAAAAAAAAAEgAAAAAAAAAcBwAAAAQAAAATAgAAAAE#4AAAAAAAAP####8AAAABAApDUHVpc3NhbmNlAAAAEwMAAAATAf####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAASAAAAAgAAABIAAAABAAAAAUAAAAAAAAAAAAF4AAAABwD#####AQAAAAAPAAJ4MQEFAAG#84WEI12cTwAAAAgAAAAXAP####8AB3hDb29yZDEAAAAMAAAANQAAAAMA#####wACeDEAB3hDb29yZDEAAAASAAAANgAAAAMA#####wACeTEABWYoeDEp#####wAAAAEADkNBcHBlbEZvbmN0aW9uAAAANAAAABIAAAA3AAAAEQD#####AQAAAAAPAAABBQAAAAAMAAAAEgAAADcAAAASAAAAOP####8AAAACAA1DTGlldURlUG9pbnRzAP####8AAAAAAAEAAAA5AAAB9AABAAAANQAAAAUAAAA1AAAANgAAADcAAAA4AAAAOQAAAAMA#####wABYQAELTAuMgAAAAQAAAABP8mZmZmZmZoAAAADAP####8AAWIAAzEuNQAAAAE#+AAAAAAAAAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAOwAAAAEAAAAAAAAAAAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAOwAAAB8AAAA0AAAAEgAAADsAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEgAAADwAAAABAAAAAAAAAAAAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEgAAADwAAAAfAAAANAAAABIAAAA8AAAAFgD#####AQAAAAANAAABAAEAAAA9AAAAPwAAAAcA#####wEAAAAADwACeDIBBQABP9EvGjNvbeQAAABBAAAAFwD#####AAd4Q29vcmQyAAAADAAAAEIAAAADAP####8AAngyAAd4Q29vcmQyAAAAEgAAAEMAAAADAP####8AAnkyAAVmKHgyKQAAAB8AAAA0AAAAEgAAAEQAAAARAP####8BAAAAAA8AAAEFAAAAAAwAAAASAAAARAAAABIAAABFAAAAIAD#####AAAAAAABAAAARgAAAfQAAQAAAEIAAAAFAAAAQgAAAEMAAABEAAAARQAAAEb#####AAAAAQASQ1N1cmZhY2VMaWV1RHJvaXRlAP####8AAAD#AAAABQAAAEcAAAAI#####wAAAAIADENEcm9pdGVQYXJFcQD#####AQAAAAAPAAABAAEAAAAMAAAAEwgAAAAeAAAAAAAAABIAAAACAAAAEwEAAAAeAAAAAAAAABIAAAACAAAAAwD#####AAJhMQABMAAAAAEAAAAAAAAAAAAAAAMA#####wACYTIAATEAAAABP#AAAAAAAAAAAAAWAP####8AAAD#AA0AAAEAAQAAAD4AAAA9AAAAFgD#####AAAA#wANAAABAAEAAABAAAAAP#####8AAAABAApDSW50ZWdyYWxlAP####8AAUkAAAA0AAAAEgAAADsAAAASAAAAPAAAAAFAWQAAAAAAAAAAAAMA#####wACYTMAAi0xAAAABAAAAAE#8AAAAAAAAAAAAAMA#####wACYTQABC0wLjUAAAAEAAAAAT#gAAAAAAAAAAAAEQD#####AQB#AAAPAAAAAAAAAAAAAABACAAAAAAAAAUAAAAADAAAABIAAABPAAAAHwAAADQAAAASAAAATwAAABEA#####wEAfwAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAATwAAAAEAAAAAAAAAAAAAABEA#####wEAfwAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAUAAAAAEAAAAAAAAAAAAAABEA#####wEAfwAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAUAAAAB8AAAA0AAAAEgAAAFAAAAAWAP####8AAH8AAA0AAAEAAQAAAFEAAABSAAAAFgD#####AAB#AAANAAABAAEAAABTAAAAVAAAABYA#####wEAAAAADQAAAQABAAAAUgAAAFMAAAAHAP####8BAAAAABAAAngzAQUAAT#ht6mhLV2bAAAAVwAAABcA#####wAHeENvb3JkMwAAAAwAAABYAAAAAwD#####AAJ4MwAHeENvb3JkMwAAABIAAABZAAAAAwD#####AAJ5MwAFZih4MykAAAAfAAAANAAAABIAAABaAAAAEQD#####AQAAAAAQAAABBQAAAAAMAAAAEgAAAFoAAAASAAAAWwAAACAA#####wEAfwAAAQAAAFwAAAH0AAEAAABYAAAABQAAAFgAAABZAAAAWgAAAFsAAABcAAAAIQD#####AAB#AAAAAAUAAABdAAAACP####8AAAABAA9DVmFsZXVyQWZmaWNoZWUA#####wAAAP8Av#AAAAAAAABAAAAAAAAAAAAAAD0SAAAAAAABAAAAAAAAAAAEAAAASgAAACQA#####wAAAP8AP#AAAAAAAAA#8AAAAAAAAAAAAD8SAAAAAAABAAAAAAAAAAAEAAAASwAAACQA#####wAAfwAAAAAAAAAAAABAFAAAAAAAAAAAAFISAAAAAAABAAAAAAAAAAACAAAABAAAACQA#####wAAfwAAwBgAAAAAAABAGAAAAAAAAAAAAFMSAAAAAAABAAAAAAAAAAACAAAAAwAAAA3##########w=='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true)
    callback()
  }

  function modifFig (st, finConsigne) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété correction (booléen)
    if (st.correction) {
      // on affiche la correction
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a3', String(stor.borneInfRep))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a4', String(stor.borneSupRep))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a5', String(stor.afficheBorneInfCherchee))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a6', String(stor.afficheBorneSupCherchee))
    } else {
      // ceci sert si jamais on souhaite modifier la courbe initiale dans la question
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(stor.borneInf))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(stor.borneSup))
      // gestion de l’affichage des valeurs
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a1', String(stor.afficheBorneInf))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a2', String(stor.afficheBorneSup))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a3', '-10')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a4', '-10')
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
    finConsigne()
  }

  function suite () {
    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    let i, borneInf, borneSup, OK
    // je dois gérer les paramètres mu et sigma, espérer que ce qui a été éventuellement saisi est correct
    if ((ds.typeLoi.toLowerCase() === 'centreereduite') || (ds.typeLoi.toLowerCase() === 'centréeréduite') || (ds.typeLoi.toLowerCase() === 'centree') || (ds.typeLoi.toLowerCase() === 'centrée')) {
      stor.tabMu = {}
      stor.tabMu.expression = []
      stor.tabMu.valeur = []
      for (i = 0; i < ds.nbrepetitions; i++) {
        stor.tabMu.expression.push('0')
        stor.tabMu.valeur.push(0)
      }
    } else {
      if (ds.mu.indexOf('|') > -1) {
        const tabmuInit = ds.mu.split('|')
        stor.tabMu = constructionTabIntervalle(tabmuInit[0], '[-6;6]', ds.nbrepetitions, [0])
        if (!isNaN(Number(tabmuInit[1]))) {
          for (i = 0; i < ds.nbrepetitions; i++) {
            if (stor.tabMu.leType[i] === 'intervalle') {
              borneInf = Number(tabmuInit[0].substring(1).split(';')[0]) * Math.pow(10, Number(tabmuInit[1]))
              borneSup = Number(tabmuInit[0].substring(1, tabmuInit[0].length - 1).split(';')[1]) * Math.pow(10, Number(tabmuInit[1]))
              do {
                stor.tabMu.expression[i] = genereAlea('[' + borneInf + ';' + borneSup + ']') / Math.pow(10, Number(tabmuInit[1]))
                stor.tabMu.valeur[i] = j3pCalculValeur(stor.tabMu.expression[i])
                OK = (i === 0) ? true : (Math.abs(stor.tabMu.valeur[i] - stor.tabMu.valeur[i - 1]) > Math.pow(10, -12))
              } while (!OK)
            }
          }
        }
      } else {
        stor.tabMu = constructionTabIntervalle(ds.mu, '[-6;6]', ds.nbrepetitions, [0])
      }
    }
    if ((ds.typeLoi.toLowerCase() === 'centreereduite') || (ds.typeLoi.toLowerCase() === 'centréeréduite')) {
      stor.tabSigma = {}
      stor.tabSigma.expression = []
      stor.tabSigma.valeur = []
      for (i = 0; i < ds.nbrepetitions; i++) {
        stor.tabSigma.expression.push('1')
        stor.tabSigma.valeur.push(1)
      }
    } else {
      if (ds.sigma.indexOf('|') > -1) {
        const tabsigmaInit = ds.sigma.split('|')
        stor.tabSigma = constructionTabIntervalle(tabsigmaInit[0], '[1.1;4.0]', ds.nbrepetitions, [1])
        if (!isNaN(Number(tabsigmaInit[1]))) {
          for (i = 0; i < ds.nbrepetitions; i++) {
            if (stor.tabSigma.leType[i] === 'intervalle') {
              borneInf = Number(tabsigmaInit[0].substring(1).split(';')[0]) * Math.pow(10, Number(tabsigmaInit[1]))
              borneSup = Number(tabsigmaInit[0].substring(1, tabsigmaInit[0].length - 1).split(';')[1]) * Math.pow(10, Number(tabsigmaInit[1]))
              do {
                stor.tabSigma.expression[i] = genereAlea('[' + borneInf + ';' + borneSup + ']') / Math.pow(10, Number(tabsigmaInit[1]))
                stor.tabSigma.valeur[i] = j3pCalculValeur(stor.tabSigma.expression[i])
                OK = (i === 0) ? true : (Math.abs(stor.tabSigma.valeur[i] - stor.tabSigma.valeur[i - 1]) > Math.pow(10, -12))
              } while (!OK)
            }
          }
        }
      } else {
        stor.tabSigma = constructionTabIntervalle(ds.sigma, '[1.0;4.0]', ds.nbrepetitions, [1])
      }
    }
    // je peux faire varier les questions à poser
    const tabQuestInit = ['gauche', 'droite', 'centreGauche', 'centreDroit']
    stor.tabQuest = []
    for (i = 0; i < ds.nbrepetitions; i++) {
      if (tabQuestInit.length === 0) {
        tabQuestInit.push('gauche', 'droite', 'centreGauche', 'centreDroit')
        i--
      } else {
        const pioche = j3pGetRandomInt(0, (tabQuestInit.length - 1))
        stor.tabQuest.push(tabQuestInit[pioche])
        tabQuestInit.splice(pioche, 1)
      }
    }
    me.logIfDebug('tabQuest:', stor.tabQuest)
    suite2()
  }

  function suite2 () {
    /// /////////////////////////////////////// Pour MathGraph32
    const largFig = 548// on gère ici la largeur de la figure
    const hautFig = 218
    // pos_x est la position pour que la figure soit centrée (j’utilise ici stor.pourcentage déclaré plus haut)'
    // création du div accueillant la figure mtg32
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div')
    stor.divMtg32 = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '5px' }) })
    // et enfin la zone svg (très importante car tout est dedans)
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(stor.divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
    // ce qui suit lance la création initiale de la figure
    // depart()

    me.logIfDebug('stor.tabMu.expression:', stor.tabMu.expression, '   stor.tabSigma.expression:', stor.tabSigma.expression)

    /// //////////////////////////////////////Fin de l’initialisation de la figure MathGraph32'
    // dans ce type de section, je mets plutôt les consignes à gauche
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })//, coord:[0,0]
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.mu = stor.tabMu.expression[me.questionCourante - 1]
    stor.sigma = stor.tabSigma.expression[me.questionCourante - 1]
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, { m: stor.mu, s: stor.sigma })
    // je vais choisir la valeur de a en fonction de sigma de sorte qu’il soit dans ]0.5sigma;2sigma[ et différent de sigma
    let nbChiffreApresVirgule = (String(stor.mu).includes('.')) ? String(stor.mu).length - String(stor.mu).indexOf('.') : 0
    const nbChiffreApresVirguleSigma = (String(stor.sigma).includes('.')) ? String(stor.sigma).length - String(stor.sigma).indexOf('.') - 1 : 0
    nbChiffreApresVirgule = Math.max(Math.max(nbChiffreApresVirgule, nbChiffreApresVirguleSigma), 1)
    let pb
    do {
      stor.a = j3pGetRandomInt((0.5 * stor.sigma) * Math.pow(10, nbChiffreApresVirgule), (2 * stor.sigma) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
      // il faut aussi que j’empêche l’aire de valoir 0.25 (cela engendrerait une mauvaise idée chez les élèves)
      // on sait que P(-b<=X<=b)=0.5 lorsque b\\simeq 0,67448975 lorsque X suit la loi normale centrée réduite
      pb = (Math.abs(stor.a / stor.sigma - 0.67448975) < 0.02)
    } while ((Math.abs(stor.a, stor.sigma) < 3 * Math.pow(10, -nbChiffreApresVirgule)) || pb)
    let consigne = ds.textes.consigne2_1
    if (stor.tabQuest[me.questionCourante - 1] === 'gauche') {
      // on demande un intervalle de la forme ]-\infty;mu-a]
      // on donne P(mu<=X<=mu+a)
      // borneInf et borneSup vont servir à hachurer l’aire dans la figure mtg32
      stor.borneInf = 0
      stor.borneSup = stor.a / stor.sigma
      stor.borneInfRep = -10
      stor.borneSupRep = -stor.a / stor.sigma
      // intervalle sur lequel on a l’aire hachurée
      stor.afficheBorneInf = stor.mu
      stor.afficheBorneSup = Math.round((stor.mu + stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
      // intervalle sur lequel on cherche l’aire
      stor.afficheBorneSupCherchee = Math.round((stor.mu - stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
      stor.afficheBorneInfCherchee = ''
      // là j’actualise la figure et je récupère la valeur de l’aire (arrondie à 10^{-3})
      depart(function () {
        modifFig({ correction: false },
          function () {
            stor.valAire = Math.round(Number(stor.mtgAppLecteur.valueOf(stor.mtg32svg, 'I')) * Math.pow(10, 3)) / Math.pow(10, 3)
            stor.doubleAireSolution = Math.round(Math.pow(10, 7) * (1 - 2 * stor.valAire)) / Math.pow(10, 7)
            if (!ds.probaPhrase) {
              stor.ecrireProba = 'P(' + j3pVirgule(stor.afficheBorneInf) + '\\leq X\\leq ' + j3pVirgule(stor.afficheBorneSup) + ')=' + j3pVirgule(stor.valAire)
            }
            stor.ecrireProbaCherchee = 'P(X\\leq ' + j3pVirgule(stor.afficheBorneSupCherchee) + ')'
            // j'écris ici ce dont j’aurai besoin dans les réponses
            const borneInf2 = Math.round((stor.mu - stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
            stor.p1 = 'P(' + j3pVirgule(borneInf2) + '\\leq X\\leq ' + j3pVirgule(stor.mu) + ')=' + j3pVirgule(stor.valAire)
            stor.p2 = 'P(X\\leq ' + j3pVirgule(borneInf2) + ')+P(X\\geq ' + stor.afficheBorneSup + ')=1-2\\times ' + j3pVirgule(stor.valAire) + '=' + j3pVirgule(stor.doubleAireSolution)
            stor.p3 = 'P(X\\leq ' + j3pVirgule(borneInf2) + ')=\\frac{' + j3pVirgule(stor.doubleAireSolution) + '}{2}=' + j3pVirgule(Math.round(Math.pow(10, 10) * (1 - 2 * stor.valAire) / 2) / Math.pow(10, 10))
            stor.valeur = j3pVirgule(stor.afficheBorneSup)
            const monEcart1 = j3pVirgule(Math.round(Math.pow(10, 5) * (stor.afficheBorneSup - stor.mu)) / Math.pow(10, 5))
            if (Math.abs(stor.mu) < Math.pow(10, -12)) {
              stor.ecart1 = monEcart1
            } else {
              stor.ecart1 = (stor.mu < 0) ? j3pVirgule(stor.afficheBorneSup) + '-(' + stor.mu + ')' : j3pVirgule(stor.afficheBorneSup) + '-' + stor.mu
              stor.ecart1 += '=' + monEcart1
            }
            stor.ecart2 = j3pVirgule(stor.mu) + '-' + monEcart1
            stor.ecart2 += '=' + j3pVirgule(stor.afficheBorneSupCherchee)
            finAffichage()
          })
      }
      )
    } else if (stor.tabQuest[me.questionCourante - 1] === 'droite') {
      // on demande un intervalle de la forme ]mu+a;+\infty[
      // on donne P(mu-a<=X<=mu)
      // borneInf et borneSup vont servir à hachurer l’aire dans la figure mtg32
      stor.borneSup = 0
      stor.borneInf = -stor.a / stor.sigma
      stor.borneInfRep = stor.a / stor.sigma
      stor.borneSupRep = 10
      // intervalle sur lequel on a l’aire hachurée
      stor.afficheBorneSup = stor.mu
      stor.afficheBorneInf = Math.round((stor.mu - stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
      // intervalle sur lequel on cherche l’aire
      stor.afficheBorneInfCherchee = Math.round((stor.mu + stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
      stor.afficheBorneSupCherchee = ''
      // là j’actualise la figure et je récupère la valeur de l’aire (arrondie à 10^{-3})
      depart(function () {
        modifFig({ correction: false },
          function () {
            stor.valAire = Math.round(Number(stor.mtgAppLecteur.valueOf(stor.mtg32svg, 'I')) * Math.pow(10, 3)) / Math.pow(10, 3)
            stor.doubleAireSolution = Math.round(Math.pow(10, 7) * (1 - 2 * stor.valAire)) / Math.pow(10, 7)
            if (!ds.probaPhrase) {
              stor.ecrireProba = 'P(' + j3pVirgule(stor.afficheBorneInf) + '\\leq X\\leq ' + j3pVirgule(stor.afficheBorneInf) + ')=' + j3pVirgule(stor.valAire)
            }
            stor.ecrireProbaCherchee = 'P(X\\geq ' + j3pVirgule(stor.afficheBorneInfCherchee) + ')'
            // j'écris ici ce dont j’aurai besoin dans les réponses
            const borneSup2 = Math.round((stor.mu + stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
            stor.p1 = 'P(' + j3pVirgule(stor.mu) + '\\leq X\\leq ' + j3pVirgule(borneSup2) + ')=' + j3pVirgule(stor.valAire)
            stor.p2 = 'P(X\\leq' + j3pVirgule(stor.afficheBorneInf) + ')+P(X\\geq ' + j3pVirgule(borneSup2) + ')=1-2\\times ' + j3pVirgule(stor.valAire) + '=' + j3pVirgule(stor.doubleAireSolution)
            stor.p3 = 'P(X\\geq ' + j3pVirgule(borneSup2) + ')=\\frac{' + j3pVirgule(stor.doubleAireSolution) + '}{2}=' + j3pVirgule(Math.round(Math.pow(10, 10) * (1 - 2 * stor.valAire) / 2) / Math.pow(10, 10))
            stor.valeur = j3pVirgule(stor.afficheBorneInf)
            const monEcart1 = j3pVirgule(Math.round(Math.pow(10, 5) * (stor.mu - stor.afficheBorneInf)) / Math.pow(10, 5))
            if (Math.abs(stor.mu) < Math.pow(10, -12)) {
              stor.ecart1 = monEcart1
            } else {
              stor.ecart1 = (stor.afficheBorneInf < 0) ? j3pVirgule(stor.mu) + '-(' + stor.afficheBorneInf + ')' : j3pVirgule(stor.mu) + '-' + stor.afficheBorneInf
              stor.ecart1 += '=' + monEcart1
            }
            stor.ecart2 = j3pVirgule(stor.mu) + '+' + monEcart1
            stor.ecart2 += '=' + j3pVirgule(stor.afficheBorneInfCherchee)
            finAffichage()
          })
      }
      )
    } else if (stor.tabQuest[me.questionCourante - 1] === 'centreGauche') {
      // on demande un intervalle de la forme ]mu-a;mu]
      // on donne P(X>=mu+a)
      // borneInf et borneSup vont servir à hachurer l’aire dans la figure mtg32
      stor.borneSup = 10
      stor.borneInf = stor.a / stor.sigma
      stor.borneInfRep = -stor.a / stor.sigma
      stor.borneSupRep = 0
      // intervalle sur lequel on a l’aire hachurée
      stor.afficheBorneSup = '10'
      stor.afficheBorneInf = Math.round((stor.mu + stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
      // intervalle sur lequel on cherche l’aire
      stor.afficheBorneInfCherchee = Math.round((stor.mu - stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
      stor.afficheBorneSupCherchee = stor.mu
      // là j’actualise la figure et je récupère la valeur de l’aire (arrondie à 10^{-3})
      depart(function () {
        modifFig({ correction: false },
          function () {
            stor.valAire = Math.round(Number(stor.mtgAppLecteur.valueOf(stor.mtg32svg, 'I')) * Math.pow(10, 3)) / Math.pow(10, 3)
            stor.doubleAireSolution = Math.round(Math.pow(10, 7) * (1 - 2 * stor.valAire)) / Math.pow(10, 7)
            if (!ds.probaPhrase) {
              stor.ecrireProba = 'P(X\\geq ' + j3pVirgule(stor.afficheBorneInf) + ')=' + j3pVirgule(stor.valAire)
            }
            stor.ecrireProbaCherchee = 'P(' + j3pVirgule(stor.afficheBorneInfCherchee) + '\\leq X\\leq ' + j3pVirgule(stor.afficheBorneSupCherchee) + ')'
            consigne = ds.textes.consigne2_2
            // j'écris ici ce dont j’aurai besoin dans les réponses
            const borneInf2 = Math.round((stor.mu - stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
            stor.p1 = 'P(X\\leq ' + j3pVirgule(borneInf2) + ')=P(X\\geq ' + j3pVirgule(stor.afficheBorneInf) + ')=' + j3pVirgule(stor.valAire)
            stor.p2 = 'P(' + j3pVirgule(borneInf2) + '\\leq X\\leq ' + j3pVirgule(stor.afficheBorneInf) + ')=1-2\\times ' + j3pVirgule(stor.valAire) + '=' + j3pVirgule(stor.doubleAireSolution)
            stor.p3 = 'P(' + j3pVirgule(borneInf2) + '\\leq X\\leq ' + j3pVirgule(stor.mu) + ')=\\frac{' + j3pVirgule(stor.doubleAireSolution) + '}{2}=' + j3pVirgule(Math.round(Math.pow(10, 10) * (1 - 2 * stor.valAire) / 2) / Math.pow(10, 10))
            stor.valeur = j3pVirgule(stor.afficheBorneInf)
            const monEcart1 = j3pVirgule(Math.round(Math.pow(10, 5) * (stor.afficheBorneInf - stor.mu)) / Math.pow(10, 5))
            if (Math.abs(stor.mu) < Math.pow(10, -12)) {
              stor.ecart1 = monEcart1
            } else {
              stor.ecart1 = (stor.mu < 0) ? j3pVirgule(stor.afficheBorneInf) + '-(' + stor.mu + ')' : j3pVirgule(stor.afficheBorneInf) + '-' + stor.mu
              stor.ecart1 += '=' + monEcart1
            }
            stor.ecart2 = j3pVirgule(stor.mu) + '-' + monEcart1
            stor.ecart2 += '=' + j3pVirgule(stor.afficheBorneInfCherchee)
            finAffichage()
          })
      }
      )
    } else {
      // on demande un intervalle de la forme ]mu;mu+a]
      // on donne P(X<=mu-a)
      // borneInf et borneSup vont servir à hachurer l’aire dans la figure mtg32
      stor.borneInf = -10
      stor.borneSup = -stor.a / stor.sigma
      stor.borneInfRep = 0
      stor.borneSupRep = stor.a / stor.sigma
      // intervalle sur lequel on a l’aire hachurée
      stor.afficheBorneSup = Math.round((stor.mu - stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
      stor.afficheBorneInf = '-10'
      // intervalle sur lequel on cherche l’aire
      stor.afficheBorneInfCherchee = stor.mu
      stor.afficheBorneSupCherchee = Math.round((stor.mu + stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
      // là j’actualise la figure et je récupère la valeur de l’aire (arrondie à 10^{-3})
      depart(function () {
        modifFig({ correction: false },
          function () {
            stor.valAire = Math.round(Number(stor.mtgAppLecteur.valueOf(stor.mtg32svg, 'I')) * Math.pow(10, 3)) / Math.pow(10, 3)
            stor.doubleAireSolution = Math.round(Math.pow(10, 7) * (1 - 2 * stor.valAire)) / Math.pow(10, 7)
            if (!ds.probaPhrase) {
              stor.ecrireProba = 'P(X\\leq ' + j3pVirgule(stor.afficheBorneSup) + ')=' + j3pVirgule(stor.valAire)
            }
            stor.ecrireProbaCherchee = 'P(' + j3pVirgule(stor.afficheBorneInfCherchee) + '\\leq X\\leq ' + j3pVirgule(stor.afficheBorneSupCherchee) + ')'
            consigne = ds.textes.consigne2_3
            // j'écris ici ce dont j’aurai besoin dans les réponses
            const borneSup2 = Math.round((stor.mu + stor.a) * Math.pow(10, nbChiffreApresVirgule)) / Math.pow(10, nbChiffreApresVirgule)
            stor.p1 = 'P(X\\geq ' + j3pVirgule(borneSup2) + ')=P(X\\leq ' + j3pVirgule(stor.afficheBorneSup) + ')=' + j3pVirgule(stor.valAire)
            stor.p2 = 'P(' + j3pVirgule(stor.afficheBorneSup) + '\\leq X\\leq' + j3pVirgule(borneSup2) + ')=1-2\\times ' + j3pVirgule(stor.valAire) + '=' + j3pVirgule(stor.doubleAireSolution)
            stor.p3 = 'P(' + j3pVirgule(stor.mu) + '\\leq X\\leq ' + j3pVirgule(borneSup2) + ')=\\frac{' + j3pVirgule(stor.doubleAireSolution) + '}{2}=' + j3pVirgule(Math.round(Math.pow(10, 10) * (1 - 2 * stor.valAire) / 2) / Math.pow(10, 10))
            stor.valeur = j3pVirgule(stor.afficheBorneSup)
            const monEcart1 = j3pVirgule(Math.round(Math.pow(10, 5) * (stor.mu - stor.afficheBorneSup)) / Math.pow(10, 5))
            if (Math.abs(stor.mu) < Math.pow(10, -12)) {
              stor.ecart1 = monEcart1
            } else {
              stor.ecart1 = (stor.afficheBorneSup < 0) ? j3pVirgule(stor.mu) + '-(' + stor.afficheBorneSup + ')' : j3pVirgule(stor.mu) + '-' + stor.afficheBorneSup
              stor.ecart1 += '=' + monEcart1
            }
            stor.ecart2 = j3pVirgule(stor.mu) + '+' + monEcart1
            stor.ecart2 += '=' + j3pVirgule(stor.afficheBorneSupCherchee)
            finAffichage()
          })
      }
      )
    }
    me.logIfDebug('valAire', stor.valAire, 'probaPhrase', ds.probaPhrase, 'afficheBorneInfCherchee', stor.afficheBorneInfCherchee, 'afficheBorneSupCherchee:', stor.afficheBorneSupCherchee)

    function finAffichage () {
      const probaAffichee = (ds.probaAffichee) ? stor.ecrireProba : j3pVirgule(stor.valAire)
      j3pAffiche(stor.zoneCons2, '', consigne,
        {
          a: stor.afficheBorneInf,
          b: stor.afficheBorneSup,
          p: probaAffichee
        })
      let elt
      if (ds.probaPhrase) {
        let consignePhrase
        if (stor.tabQuest[me.questionCourante - 1] === 'gauche') {
          consignePhrase = ds.textes.consigne4_1
        } else if (stor.tabQuest[me.questionCourante - 1] === 'droite') {
          consignePhrase = ds.textes.consigne4_3
        } else {
          consignePhrase = ds.textes.consigne4_2
        }
        elt = j3pAffiche(stor.zoneCons3, '', consignePhrase,
          {
            a: stor.afficheBorneInfCherchee,
            b: stor.afficheBorneSupCherchee,
            inputmq1: { texte: '' }
          })
      } else {
        elt = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3,
          {
            p: stor.ecrireProbaCherchee,
            inputmq1: { texte: '' }
          })
      }
      stor.zoneInput = elt.inputmqList[0]
      mqRestriction(stor.zoneInput, '\\d.,')
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      stor.zoneInput.reponse = [(1 - 2 * stor.valAire) / 2]
      j3pFocus(stor.zoneInput)
    }

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    // ce qui suit est le div dans lequel seront donnés les commentaires concernant la réponse de l’élève : c’est bien, c’est faux...'
    // Comme la consigne est à droite, il faut que je positionne ces commentaire en-dessous
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '5px' }) })
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    stor.mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: stor.mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      mu: '[0;12]|0',
      sigma: '[1.0;4.0]|1',
      probaPhrase: true,
      typeLoi: 'quelconque',

      textes: {
        titre_exo: 'Calcul de probabilité et aire sous la courbe',
        // consigne1 : "Soit $X$ une variable aléatoire suivant une loi normale de paramètres $\\mu=£m$ et $\\sigma=£s$.",
        consigne1: 'Soit $X$ une variable aléatoire suivant une loi normale d’espérance $\\mu=£m$.',
        consigne2_1: 'Ci-contre, on a représenté la courbe de densité de $X$. La probabilité que $X$ soit comprise entre $£a$ et $£b$ vaut $£p$ : c’est l’aire de la zone colorée ci-contre.',
        consigne2_2: 'Ci-contre, on a représenté la courbe de densité de $X$. La probabilité que $X$ soit supérieure à $£a$ vaut $£p$ : c’est l’aire de la zone colorée ci-contre.',
        consigne2_3: 'Ci-contre, on a représenté la courbe de densité de $X$. La probabilité que $X$ soit inférieure à $£b$ vaut $£p$ : c’est l’aire de la zone colorée ci-contre.',
        consigne3: 'On en déduit que $£p=$&1&.',
        consigne4_1: 'On en déduit que la probabilité que $X$ soit inférieure à $£b$ vaut : &1&.',
        consigne4_2: 'On en déduit que la probabilité que $X$ soit compris entre $£a$ et $£b$ vaut : &1&.',
        consigne4_3: 'On en déduit que la probabilité que $X$ soit supérieure à $£a$ vaut : &1&.',
        comment1: 'Une probabilité est un nombre compris entre 0 et 1!',
        corr1: 'L’écart entre $\\mu=£m$ et $£v$ vaut $£e$. De plus $£f$.',
        corr2: 'La droite d’équation $x=\\mu$ étant un axe de symétrie de la courbe, on déduit $£p$.',
        corr3: 'Ainsi $£p$ et $£q$.',
        corr4: 'On pouvait également utiliser $P(X\\leq \\mu)=P(X\\geq \\mu)=\\frac{1}{2}$.'
      },
      pe: 0
    }
  }

  // console.log("debut du code : ",me.etat)

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        // Construction de la page
        // Le paramètre définit la largeur relative de la première colonne
        stor.pourcentage = 0.5
        // j’ai choisi de mettre ce pourcentage en paramètre pour centrer la figure par la suite
        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourcentage })
        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo)
        // chargement mtg
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              suite()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite2()
      }

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // comme pour l’utilisation de Validation_zones, je déclare l’objet reponse qui contient deux propriétés : aRepondu et bonneReponse
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(true)// affichage de la correction
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
             *   RECOPIER LA CORRECTION ICI !
             */
              afficheCorrection(false)// affichage de la correction
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // on peut affiner le commentaire si on veut
              const nbRepondu = j3pNombre(stor.fctsValid.zones.reponseSaisie[0])

              if (!isNaN(nbRepondu)) {
              // on a bien un nombre
              // S’il n’est pas entre 0 et 1, on écrit un message
                if (nbRepondu < 0 || nbRepondu > 1) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorrection(false)// affichage de la correction
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
