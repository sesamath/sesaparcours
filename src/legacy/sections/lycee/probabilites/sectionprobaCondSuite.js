import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pElement, j3pEmpty, j3pExtraireCoefsFctAffine, j3pFocus, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pMasqueFenetre } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexQuotient } from 'src/legacy/core/functionsLatex'
import { construireArbreCond, reecrireEvt, simplifieEvt, egaliteTableauPrb } from 'src/legacy/outils/arbreProba/arbreProba'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    mai 2016
    Dans cette section, on propose à l’élève de résoudre un exercice de probabilité conditionnelles
    Quatre question sont possibles : arbre de proba, la proba d’une intersection, un eutilisation de la formule des probabilités totales et un proba conditionnelles utilisant le dernier résultat
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuestions', [1, 2], 'array', 'Tableau qui contient les questions choisies (dans l’ordre croissant):<br>- 1 pour demander l’arbre à l’instant n;<br>- 2 pour donner la relation de récurrence de la suite.'],
    ['tabCasFigure', [], 'array', 'Pour chaque répétition, on peut imposer, sous forme d’un tableau, un des exercices particuliers présents dans le fichier texte annexe :<br/>-1 : sujet inventé<br/>-2 Pondichéry avril 2013 S<br/>-3 Polynésie juin 2011 S<br/>-4 Antilles-Guyane septembre 2011 S'],
    ['afficheEvt', false, 'boolean', 'Dans la construction de l’arbre, on peut donner les événements et ne demander que les probabilités si ce paramètre vaut true.'],
    ['seuilreussite', 0.8, 'reel', 'Pourcentage à partir duquel on considère que l’exercice est correctement traité.'],
    ['seuilErreur', 0.4, 'reel', 'Pourcentage à partir duquel on retient l’erreur pour la phrase d’état.']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'problème dans la construction de l’arbre à l’instant n' },
    { pe_3: 'problème dans l’expression de la formule de récurrence de la suite' },
    { pe_4: 'Insuffisant' }
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  /**
   * bonEvts permet de vérifier si les événements saisis sur les branches sont bien présents dans l’énoncé
   * Cette fonction est une variate de celle présente dans arbresPrbs.js car on accepte l’oubli des indices
   * @private
   * @param {Array} [tab1] tableau de couples de la forme (evt/prb) : ces couples correspondent à ce qui se trouve sur la branche d’un arbre
   * @param {Array} [listeEvts] tableau des événements présents dans l’énoncé
   * @return {Boolean} précise si les événements sont bien dans l’arbre (\overline{\overline{A}} étant considéré comme égal à A)
   */
  function bonEvts (tab1, listeEvts) {
    // console.log('listeEvts:',listeEvts)
    // tab est la liste des branches
    // listeEvts est la liste des événements de l’exo
    function enleveBarre (evt) {
      // si evt est de la forme \\overline{}, on enlève \\overline
      let monEvt = (evt.includes('overline')) ? evt.replace('\\overline', '') : evt
      // console.log('monEvt:',monEvt)
      // il reste encore les accolades éventuelles
      if ((monEvt.charAt(0) === '{') && (monEvt.charAt(monEvt.length - 1) === '}')) {
        monEvt = monEvt.substring(1, monEvt.length - 1)
      }
      return monEvt
    }
    const listeSansIndice = []
    let evtsansIndice
    for (let j = 0; j < listeEvts.length; j++) {
      if (listeEvts[j].indexOf('_')) {
        evtsansIndice = listeEvts[j].replace(/_{([^{}]+)}/g, '$1')
        evtsansIndice = evtsansIndice.replace(/_([^{}])/g, '$1')
        listeSansIndice.push(evtsansIndice)
      }
    }
    const listeEvtsAcceptes = listeEvts.concat(listeSansIndice) // Dans le cas où on a un événement avec un indice, on accepte l’oubli de l’indice
    let evtPresent = true
    const newList = []
    let evt
    for (let j = 0; j < tab1.length; j++) {
      evt = tab1[j][0]
      while (evt.includes('overline')) {
        evt = enleveBarre(evt)
      }
      newList.push(evt)
    }
    for (let i = 0; i < tab1.length; i++) {
      evtPresent = (evtPresent && (listeEvtsAcceptes.indexOf(newList[i]) > -1))
    }
    return evtPresent
  }
  /**
   * Cette fonction renvoie la valeur d’un calcul ou la fraction dans laquelle apparaît un calcul. C’est une version modifiée de celle qu’on trouve dans fonctions/gestionParametres.js
   * @param {Object} [obj] objet contenant les propriétés text et obj
   * @param {string} [obj.texte] texte avec des variables écrites sous la forme £a (comme pour j3pAffiche). Par exemple text:'2*£a+£b'
   * @param {Object} [obj.obj] objet donnant les valeurs de chaque variable (comme pour j3pAffiche). Par exemple obj:{a:'2',b:'-3'}
   * @return {string} chaîne de caractère pour une fraction (number autrement) correspondant au calcul en ayant remplacé les valeurs présentes dans obj
   */
  function remplaceCalcul (obj) {
    // obj contient trois propriétés (la dernière étant optionnelle):
    // -obj.text qui est le texte correspondant à la formule de calcul
    // -obj.obj qui est la liste des valeurs à remplacer et leurs valeurs respectives
    // Par exemple : RemplaceCalcul({text:'2*£a+£b', obj:{a:'2',b:'-3'}})
    // le résultat renvoyé est la valeur du calcul (format numérique)
    if (obj.calcul === undefined) {
      obj.calcul = true
    }
    let newTxt = obj.text
    let pos1
    while (newTxt.includes('£')) {
      // on remplace £x par sa valeur
      pos1 = newTxt.indexOf('£')
      const laVariable = newTxt.charAt(pos1 + 1)
      const nombre = obj.obj[laVariable]
      newTxt = newTxt.substring(0, pos1) + '(' + nombre + ')' + newTxt.substring(pos1 + 2)
    }
    if (newTxt.includes('frac')) {
      const numden = /frac{([^}]+)}{([^}]+)}$/.exec(newTxt)
      return '\\frac{' + Math.round(Math.pow(10, 10) * j3pCalculValeur(numden[1])) / Math.pow(10, 10) + '}{' + Math.round(Math.pow(10, 10) * j3pCalculValeur(numden[2])) / Math.pow(10, 10) + '}'
    } else {
      return Math.round(Math.pow(10, 10) * j3pCalculValeur(newTxt)) / Math.pow(10, 10)
    }
  }

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    function afficheArbre () {
      // dans le cas d’une mauvaise réponse, je donne l’arbre
      j3pAffiche(zoneExpli1, '', ds.textes.corr3)
      zoneExpli1 = j3pAddElt(zoneExpli, 'div')
      stor.objArbreRep.lestyle.couleur = me.styles.toutpetit.correction.color
      const zoneArbre = j3pAddElt(stor.zoneDroite, 'div')
      construireArbreCond(zoneArbre, stor.objArbreRep)
    }
    if (ds.typeQuestions[me.etapeCourante - 1] === 2) j3pDetruit(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    let zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    switch (ds.typeQuestions[me.etapeCourante - 1]) {
      case 1:
        // arbre de proba
        try {
          if ((ds.typeQuestions[me.etapeCourante - 1] === 1) && !ds.afficheEvt) {
            j3pEmpty(stor.zoneArbre.laPalette)
            stor.zoneArbre.listeBtns.forEach(btn => j3pDetruit(btn))
          } else {
            j3pEmpty(stor.laPalette)
          }
          j3pDetruit(stor.indication)
        } catch (e) {
          console.warn(e) // pas normal mais pas grave
        }
        {
          const objVar = {}
          let num = 97
          for (let i = 0; i < stor.prbEnonce['enonce' + stor.numeroExo].length; i++) {
            objVar[String.fromCharCode(num)] = (String(stor.prbEnonce['enonce' + stor.numeroExo][i]).indexOf('n') > -1)
              ? stor.prbEnonce['enonce' + stor.numeroExo][i]
              : remplaceCalcul({
                text: stor.prbEnonce['enonce' + stor.numeroExo][i],
                obj: stor.obj
              })
            num++
          }
          j3pAffiche(zoneExpli1, '', stor.correctionProba['enonce' + stor.numeroExo], objVar)
        }
        if (!bonneReponse) {
          const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
          j3pAffiche(zoneExpli2, '', ds.textes.corr1)
          // je construis une autre fenêtre pour y mettre la correction
          const zoneExpliArbre = j3pAddElt(stor.detailsFenetre, 'div', '', { style: me.styles.toutpetit.explications })
          j3pToggleFenetres(stor.idCorrDetails)
          stor.objArbreRep.lestyle.couleur = me.styles.toutpetit.correction.color
          construireArbreCond(zoneExpliArbre, stor.objArbreRep)
        }

        break
      default :
        if (!stor.questionArbre && !bonneReponse) afficheArbre()
        // gestion de l’arrondi éventuel de la réponse
        j3pAffiche(zoneExpli1, '', stor.correctionPrbTotale['enonce' + stor.numeroExo], stor.obj)
        break
    }
    // Pour sesaparcours, c’est une autre classe pour ces barres :
    $('.mq-overline').each(function () {
      $(this).css('border-top', '1px solid ' + $(this).css('color'))
      // je parcours l’ensemble des overline et je leur donne la couleur dans laquelle se trouve le texte
    })
  }

  function fractionComplete (texte) {
    // cette fonction vérifie si texte est l’équivalent d’un écriture sous la forme d’une fraction
    const testReg = /\([^()]+\)\/\(\d+\)/ // new RegExp('\\({1}.+\\){1}\\/\\({1}[0-9]+\\){1}', 'i')
    let posFrac
    let posInit
    if (testReg.test(texte)) {
      if (texte.match(testReg)[0] === texte) {
        posFrac = texte.lastIndexOf('/')
        let numparferm = 1
        posInit = posFrac - 2// en position posFrac-2 on a nécessairement ')'
        while (posInit > -1) {
          if (texte[posInit] === ')') {
            numparferm++
          } else if (texte[posInit] === '(') {
            numparferm--
          }
          posInit--
          if (posInit === -1) {
            return true
          }
          if (numparferm === 0) {
            posInit = -1
          }
        }
        return false
      } else {
        return false
      }
    } else {
      return false
    }
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 2,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      pe_1: 'Bien',
      pe_2: 'problème dans la construction de l’arbre à l’instant n',
      pe_3: 'problème dans l’expression de la formule de récurrence de la suite',
      pe_4: 'Insuffisant',
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      tabCasFigure: [],
      typeQuestions: [1, 2],
      afficheEvt: false,
      seuilErreur: 0.4,
      seuilreussite: 0.8,
      /*
          Les textes sont présents dans un fichier externe : sectionsAnnexes/Consignesexos/probaCond.js
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Probabilités conditionnelles et suite',
        // on donne les phrases de la consigne
        consigne1_1: 'Construire l’arbre de probabilité traduisant la situation entre les instants $n$ et $n+1$.',
        consigne1_2: 'Compléter l’arbre de probabilité traduisant la situation entre les instants $n$ et $n+1$.',
        consigne2: 'On a représenté ci-contre l’arbre de probabilité représentant la situation.',
        consigne5: 'Les boutons + et - permettent d’ajouter ou supprimer une branche au niveau du noeud (4 branches au maximum).',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Le nombre de branches n’est pas correct !',
        comment2: 'Une erreur est détectée sur au moins une branche issue de ce(s) noeud(s) !',
        comment3: 'Une erreur est détectée sur au moins une branche issue de la racine !',
        comment4: ' Au moins un des événements proposés n’est pas défini dans l’énoncé !',
        comment5: 'La réponse est correcte mais elle doit être donnée sous forme simplifiée !',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'On obtient alors l’arbre ci-contre :',
        corr2: 'Arbre attendu :',
        corr3: 'On peut représenter la situation par l’arbre ci-contre :'
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      pe: 0
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    // Construction de la page

    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.65 })

    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    const largFenetre = 400
    stor.detailsFenetre = j3pGetNewId('detailsFenetre')
    stor.idCorrDetails = j3pGetNewId('CorDetails')
    me.fenetresjq = [
      {
        name: stor.idCorrDetails,
        title: ds.textes.corr2,
        width: largFenetre,
        height: 400,
        left: me.zonesElts.MG.getBoundingClientRect().width - largFenetre - 20,
        top: me.zonesElts.HD.getBoundingClientRect().height + 10,
        id: stor.detailsFenetre
      }
    ]
    j3pCreeFenetres(me)
    import('../../../sectionsAnnexes/Consignesexos/probaSuite.js').then(function parseAnnexe ({ default: datasSection }) {
      let j
      let i
      const tabExercices = []
      stor.nb_sujets = datasSection.sujets.length
      stor.consignes = {}
      stor.nomVariables = {}
      stor.variables = {}
      // l’objet pour le contenu de l’arbre
      stor.listeEvt = {}
      stor.evtsProba = {}
      stor.prbEnonce = {}
      stor.correctionProba = {}

      // puis les objets qui seront les questions
      stor.questionPrbTotale = {}
      stor.reponsePrbTotale = {}
      stor.correctionPrbTotale = {}

      for (j = 0; j < stor.nb_sujets; j++) {
        tabExercices.push(j)
        stor.consignes['enonce' + j] = datasSection.sujets[j].consignes
        stor.nomVariables['enonce' + j] = []
        // je récupère les événements et proba qui vont me permettre de construire l’arbre de proba
        stor.listeEvt['enonce' + j] = datasSection.sujets[j].listeEvt
        stor.evtsProba['enonce' + j] = datasSection.sujets[j].evtsProba
        stor.prbEnonce['enonce' + j] = datasSection.sujets[j].prbEnonce
        stor.correctionProba['enonce' + j] = datasSection.sujets[j].correctionProba

        // ce qui suit est pour la formule des proba totales
        // on écrit clairement la proba cherchée
        stor.questionPrbTotale['enonce' + j] = datasSection.sujets[j].questionPrbTotale
        // la réponse avec présence encore des variables (géré dans cette section au niveau de la mise en place de la réponse)
        stor.reponsePrbTotale['enonce' + j] = datasSection.sujets[j].reponsePrbTotale
        let onRecommence = false
        let nbTentatives = 0
        do {
          stor.variables['enonce' + j] = []
          for (i = 0; i < datasSection.sujets[j].variables.length; i++) {
            stor.nomVariables['enonce' + j][i] = datasSection.sujets[j].variables[i][0]
            const [intMin, intMax] = j3pGetBornesIntervalle(datasSection.sujets[j].variables[i][1])
            if (datasSection.sujets[j].variables[i].length === 2) {
              stor.variables['enonce' + j][i] = j3pGetRandomInt(intMin, intMax)
            } else {
              stor.variables['enonce' + j][i] = j3pGetRandomInt(intMin, intMax) / Number(datasSection.sujets[j].variables[i][2])
            }
            // console.log('stor.nomVariables[enonce'+j+']['+i+']='+stor.nomVariables['enonce'+j][i])
            // console.log('stor.variables[enonce'+j+']['+i+']='+stor.variables['enonce'+j][i])
          }
          const objEnonce = {}
          // Il faut tout de même que je me méfis des données car il ne faudrait pas que la suite devienne constante
          // cela causerait un souci avec SuiteTermeGeneral
          for (i = 0; i < stor.nomVariables['enonce' + j].length; i++) {
            objEnonce[stor.nomVariables['enonce' + j][i]] = stor.variables['enonce' + j][i]
          }
          const a = remplaceCalcul({
            text: stor.reponsePrbTotale['enonce' + j][1],
            obj: objEnonce
          })
          const b = remplaceCalcul({
            text: stor.reponsePrbTotale['enonce' + j][2],
            obj: objEnonce
          })
          const u0 = remplaceCalcul({
            text: stor.reponsePrbTotale['enonce' + j][5],
            obj: objEnonce
          })
          const c = -b / (1 - a)
          onRecommence = (Math.abs(j3pCalculValeur(u0) + j3pCalculValeur(c)) < Math.pow(10, -12))
          nbTentatives++
        } while (onRecommence && nbTentatives < 30)
        // Cet exo ne permet pas d’imposer des données, donc impossible qu’on tombe sur une boucle infinie
        // Mais je mets quand même ce garde fou
        if (nbTentatives === 30) console.error(Error('Il y a un pb car cela génère une boucle infinie'))
        // correction pour la fonction afficheCorrection
        stor.correctionPrbTotale['enonce' + j] = datasSection.sujets[j].correctionPrbTotale
      }

      // ds.nbrepetitions = Math.min(ds.nbrepetitions, stor.nb_sujets)
      // pour le nombre d’étapes, c’est en fait géré par un tableau ds.typeQuestions
      // je vérifie que le tableau est bien construit (2 valeurs au max et dans l’ordre croissant)
      let index = 0
      while (index < ds.typeQuestions.length - 1) {
        if ((ds.typeQuestions[index] < ds.typeQuestions[index + 1]) && (ds.typeQuestions[index] < 3)) {
          index++
        } else {
          // la valeur en index+1 n’est pas correcte donc on la vire
          ds.typeQuestions.splice(index + 1, 1)
        }
      }
      if (ds.typeQuestions.length === 0) {
        ds.typeQuestions = [1, 2]
      }
      ds.nbetapes = ds.typeQuestions.length
      ds.nbitems = ds.nbetapes * ds.nbrepetitions
      // enfin je gère le cas où on demanderait un cas de figure qui n’existe pas :
      index = 0
      while (index < ds.tabCasFigure.length) {
        ds.tabCasFigure[index] = Number(ds.tabCasFigure[index])
        if ((ds.tabCasFigure[index] > 0) && (ds.tabCasFigure[index] <= tabExercices.length)) {
          index++
        } else {
          // le numéro de question choisi n’existe pas
          ds.tabCasFigure.splice(index, 1)
        }
      }
      stor.numexo = 0 // c’est pour savoir à quelle répétition nous en sommes
      suite()
    }).catch(error => {
      console.error(error)
      j3pShowError('Impossible de charger les données de cette exercice')
    })
  }
  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    suite()
  }
  function suite () {
    try {
      j3pEmpty(stor.detailsFenetre)
      j3pMasqueFenetre(stor.idCorrDetails)
    } catch (e) {
      console.warn(e) // pas normal mais pas grave
    }
    let i, j, laProba
    if (me.etapeCourante === 1) {
      // c’est qu’on passe à un nouvel énoncé (éventuellement le premier)
      let choixExoImpose = (ds.tabCasFigure[stor.numexo] !== undefined)
      if (choixExoImpose) {
        // je regarde tout de même si le numéro existe bien
        if (Number(ds.tabCasFigure[stor.numexo]) > stor.nb_sujets) {
          choixExoImpose = false
        }
      }
      if (choixExoImpose) {
        // c’est que j’ai imposé l’exo à traiter
        stor.numeroExo = Number(ds.tabCasFigure[stor.numexo]) - 1
      } else {
        // je choisis au hasard un des exos à traiter (parmi ceux qui ne sont pas déjà choisis)
        let nbTentatives = 0 // c’est au cas où on aurait déjà proposé tous les cas de figure (vu le nombre d’exos possible qu’il risque d’y avoir, c’est peu probable)
        do {
          nbTentatives++
          stor.numeroExo = j3pGetRandomInt(0, (stor.nb_sujets - 1))
        } while ((ds.tabCasFigure.indexOf(stor.numeroExo + 1) > -1) && (nbTentatives < 30))
        ds.tabCasFigure[(me.questionCourante - 1) / ds.nbetapes] = stor.numeroExo + 1
      }
      me.logIfDebug('ds.tabCasFigure:', ds.tabCasFigure, '   stor.numeroExo:', stor.numeroExo)
      stor.numexo++
      stor.phrasesEnonce = stor.consignes['enonce' + stor.numeroExo]
      stor.obj = {}
      for (i = 0; i < stor.nomVariables['enonce' + stor.numeroExo].length; i++) {
        stor.obj[stor.nomVariables['enonce' + stor.numeroExo][i]] = stor.variables['enonce' + stor.numeroExo][i]
      }
      // je réinitialise ma variable pour la répétition suivante :
      stor.questionArbre = false
      // je récupère la liste des caractères à mettre dans le clavier virtuel
      stor.lstRestriction = stor.listeEvt['enonce' + stor.numeroExo].map(evt => evt[0]).join('')
      stor.lstRestriction += '\\dn+-_'
    }
    // je choisis
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (i = 1; i <= stor.phrasesEnonce.length; i++) {
      stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(stor['zoneCons' + i], '', stor.phrasesEnonce[i - 1], stor.obj)
    }

    // div supplémentaire dans la zone droite pour positionner l’arbre et les commentaires sur les réponses de l’élève
    stor.zoneDroite = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })

    // je dois créer l’objet qui contiendra toutes les caractéristiques de mon arbre
    // cela me servira pour la réponse dans le cadre de l’arbre à construire ou pour afficher l’arbre dans les questions suivantes
    stor.objArbreRep = {
      parcours: me
    }
    // taille du texte evt (pour les proba, il faudrait qu’elles soient plus petites sans doute) + couleur du texte et des branches
    stor.objArbreRep.lestyle = {
      styletexte: me.styles.toutpetit.enonce,
      couleur: me.styles.toutpetit.enonce.color
    }
    // pour savoir si on écrit les évts et les proba (zones de saisie si false)
    stor.objArbreRep.complet = { evts: ds.afficheEvt, proba: false }
    stor.objArbreRep.evts1 = []
    stor.objArbreRep.evts2 = {}
    stor.objArbreRep.complet = { evts: true, proba: true }
    for (i = 0; i < stor.evtsProba['enonce' + stor.numeroExo].length; i++) {
      // dans ce cas de figure la proba peut être un nombre mais aussi une expression contenant le terme d’une suite
      laProba = stor.evtsProba['enonce' + stor.numeroExo][i][1]
      if (!stor.evtsProba['enonce' + stor.numeroExo][i][1].includes('_n')) {
        // c’est que c’est une donnée numérique
        laProba = remplaceCalcul({
          text: stor.evtsProba['enonce' + stor.numeroExo][i][1],
          obj: stor.obj
        })
      }
      stor.objArbreRep.evts1.push([stor.evtsProba['enonce' + stor.numeroExo][i][0], laProba])
      stor.objArbreRep.evts2['branche' + i] = []
      for (j = 0; j < stor.evtsProba['enonce' + stor.numeroExo][i][2].length; j++) {
        laProba = remplaceCalcul({
          text: stor.evtsProba['enonce' + stor.numeroExo][i][2][j][1],
          obj: stor.obj
        })
        stor.objArbreRep.evts2['branche' + i].push([stor.evtsProba['enonce' + stor.numeroExo][i][2][j][0], laProba])
      }
    }
    if (ds.typeQuestions[me.etapeCourante - 1] !== 1) {
      stor.zoneArbre = j3pAddElt(stor.zoneDroite, 'div')
      if (stor.questionArbre) {
        // je redonne l’arbre (à droite)
        const zoneCons = j3pAddElt(stor.conteneur, 'div')
        j3pAffiche(zoneCons, '', ds.textes.consigne2)
        construireArbreCond(stor.zoneArbre, stor.objArbreRep)
      }
    }
    const zoneConsSuivante = j3pAddElt(stor.conteneur, 'div')
    switch (ds.typeQuestions[me.etapeCourante - 1]) {
      case 1:
        // on demande de construire l’arbre
        if (ds.afficheEvt) {
          j3pAffiche(zoneConsSuivante, '', ds.textes.consigne1_2)
        } else {
          j3pAffiche(zoneConsSuivante, '', ds.textes.consigne1_1)
          stor.indication = j3pAddElt(stor.conteneur, 'div')
          j3pAffiche(stor.indication, '', ds.textes.consigne5)
          j3pStyle(stor.indication, { fontStyle: 'italic', paddingTop: '10px' })
        }
        stor.questionArbre = true
        // on crée le div qui va contenir l’arbre : cela se fait dans la zone MD (dans le div stor.zoneDroite qui est dans zones.MD)
        stor.zoneArbre = j3pAddElt(stor.zoneDroite, 'div')
        // ensuite je dois créer l’objet qui contiendra toutes les caractéristiques de mon arbre
        stor.objArbre = {}
        // taille du texte evt (pour les proba, il faudrait qu’elles soient plus petites sans doute) + couleur du texte et des branches
        stor.objArbre.lestyle = {
          styletexte: me.styles.toutpetit.enonce,
          couleur: me.styles.toutpetit.enonce.color
        }
        // pour savoir si on écrit les évts et les proba (zones de saisie si false)
        stor.objArbre.complet = { evts: ds.afficheEvt, proba: false }
        // au départ ne contiendra que 2*une branche
        // stor.objArbre.evts1 = [['',''],['','']]   //Pour cette branche, l’événement et la proba ne sont pas complétés donc on les initialise à 'vide'
        if (ds.afficheEvt) {
          // les événements sont affichées (donc pas moyen d’ajouter un noeud)
          stor.objArbre.evts1 = []
          stor.objArbre.evts2 = {}
          for (i = 0; i < stor.evtsProba['enonce' + stor.numeroExo].length; i++) {
            stor.objArbre.evts1.push([stor.evtsProba['enonce' + stor.numeroExo][i][0], ''])
            stor.objArbre.evts2['branche' + i] = []
            for (j = 0; j < stor.evtsProba['enonce' + stor.numeroExo][i][2].length; j++) {
              stor.objArbre.evts2['branche' + i].push([stor.evtsProba['enonce' + stor.numeroExo][i][2][j][0], ''])
            }
          }
        } else {
          stor.objArbre.evts1 = [['', '']]
          stor.objArbre.evts2 = {}
          stor.objArbre.evts2['branche' + 0] = [['', '']] // Je nomme branche0 la branche précédente et de cette branche, je fais partir une seule branche où l’évt et la proba sont pour l’instant vide
        }
        // je dois aussi récupérer l’arbre réponse
        stor.evts1Rep = []
        stor.evts2Rep = []
        for (i = 0; i < stor.evtsProba['enonce' + stor.numeroExo].length; i++) {
          let PrbRep = stor.evtsProba['enonce' + stor.numeroExo][i][1]
          if (stor.evtsProba['enonce' + stor.numeroExo][i][1].indexOf('_n') === -1) {
            // c’est que c’est une donnée numérique
            PrbRep = remplaceCalcul({
              text: stor.evtsProba['enonce' + stor.numeroExo][i][1],
              obj: stor.obj
            })
          }
          stor.evts1Rep.push([stor.evtsProba['enonce' + stor.numeroExo][i][0], PrbRep])
          stor.evts2Rep[i] = []
          for (j = 0; j < stor.evtsProba['enonce' + stor.numeroExo][i][2].length; j++) {
            PrbRep = remplaceCalcul({
              text: stor.evtsProba['enonce' + stor.numeroExo][i][2][j][1],
              obj: stor.obj
            })
            stor.evts2Rep[i].push([stor.evtsProba['enonce' + stor.numeroExo][i][2][j][0], PrbRep])
          }
        }
        me.logIfDebug('stor.evts1Rep:', stor.evts1Rep, 'stor.evts2Rep:', stor.evts2Rep)
        stor.objArbre.lstRestriction = stor.lstRestriction
        stor.objArbre.prbRestriction = '\\d+,.-*n_/' + stor.reponsePrbTotale['enonce' + stor.numeroExo][3][0]
        stor.objArbre.parcours = me
        construireArbreCond(stor.zoneArbre, stor.objArbre)
        break
      default:
        // on demande la relation de récurrence de la suite
        j3pAffiche(zoneConsSuivante, '', stor.questionPrbTotale['enonce' + stor.numeroExo][0])
        {
          const zoneConsAgain = j3pAddElt(stor.conteneur, 'div')
          j3pAffiche(zoneConsAgain, '', stor.questionPrbTotale['enonce' + stor.numeroExo][1])
          j3pStyle(zoneConsAgain, { fontStyle: 'italic' })
          const zoneConsRep = j3pAddElt(stor.conteneur, 'div')
          const elt = j3pAffiche(zoneConsRep, '', '$' + stor.questionPrbTotale['enonce' + stor.numeroExo][2] + '=$&1&',
            { inputmq1: { texte: '' } })
          stor.zoneInput = elt.inputmqList[0]
        }
        stor.zoneInput.typeReponse = ['texte']
        stor.zoneInput.reponse = [stor.reponsePrbTotale['enonce' + stor.numeroExo][0]]
        // La variable suivante est utilisée si l’élève donne la bonne expression de la suite, mais non simplifiée
        // on ne lui compte pas faux lors de la première tentative, mais on lui redonne une chance de simplifier son expression
        stor.alerteSimplification = false

        // on exporte les variables présentes dans cette section pour qu’elles soient utilisées dans SuiteTermeGeneral
        stor.defSuite = {}
        stor.defSuite.nomU = stor.reponsePrbTotale['enonce' + stor.numeroExo][3].split('_')[0]
        stor.defSuite.a = remplaceCalcul({
          text: stor.reponsePrbTotale['enonce' + stor.numeroExo][1],
          obj: stor.obj
        })
        stor.defSuite.b = remplaceCalcul({
          text: stor.reponsePrbTotale['enonce' + stor.numeroExo][2],
          obj: stor.obj
        })
        stor.defSuite.termeInitial = String(stor.reponsePrbTotale['enonce' + stor.numeroExo][4]) + '|' + remplaceCalcul({
          text: stor.reponsePrbTotale['enonce' + stor.numeroExo][5],
          obj: stor.obj
        })
        stor.defSuite.typeSuite = 'arithmeticoGeometrique'
        // me.donneesPersistantes.suites sert à envoyer des informations dans le bilan qui pourront être récupérées par une autre section sur les suites
        // Ceci a pour but de poursuivre l’étude de la suite définie dans cette section
        me.donneesPersistantes.suites = {}
        for (const prop in stor.defSuite) {
          me.donneesPersistantes.suites[prop] = stor.defSuite[prop]
          stor[prop] = stor.defSuite[prop]
        }
        stor.laPalette = j3pAddElt(stor.conteneur, 'div')
        j3pPaletteMathquill(stor.laPalette, stor.zoneInput, {
          liste: ['fraction', 'indice']
        })
        {
          const laRestrict = '\\d+,.-*/n_' + stor.reponsePrbTotale['enonce' + stor.numeroExo][3][0]
          mqRestriction(stor.zoneInput, laRestrict, {
            commandes: ['fraction', 'indice']
          })
        }
        break
    }
    if (ds.typeQuestions[me.etapeCourante - 1] > 1) me.logIfDebug('proba cond :', stor.zoneInput.reponse)

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvent dans le cadre MD)
    if (ds.typeQuestions[me.etapeCourante - 1] === 1) stor.zoneCorr = j3pAddElt(stor.conteneur, 'div')
    else stor.zoneCorr = j3pAddElt(stor.zoneDroite, 'div')
    if (ds.typeQuestions[me.etapeCourante - 1] === 1) {
      j3pFocus(stor.zoneArbre.mesZonesSaisie[0])
      stor.mesZonesSaisie = [...stor.zoneArbre.mesZonesSaisie]
    } else {
      j3pFocus(stor.zoneInput)
      stor.mesZonesSaisie = [stor.zoneInput.id]
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: stor.mesZonesSaisie,
        validePerso: stor.mesZonesSaisie
      })
    }

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = ((ds.typeQuestions[me.etapeCourante - 1] === 1) && !ds.afficheEvt) ? stor.zoneArbre.fctsValid : stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si les 4 zones ont bien été complétées ou non
        // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
        // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
        // Si c’est la dernière tentative de répondre, on barre également les zones fausses
        let monEvt, PbNbBranches, testPremieresBranches, evtsPresents, imagesEgales
        if (reponse.aRepondu) {
        // il faut aussi que je teste les deux zones dont la validation est perso
        // ce n’est valable que dans le cas où on demande l’arbre complet
          if ((ds.typeQuestions[me.etapeCourante - 1] === 1) && !ds.afficheEvt) {
          // c’est le cas de l’arbre à remplir complètement
          // La validation doit être gérée de manière personnelle
            // Je récupère la liste des zones de saisie
            stor.mesZonesSaisie = [...stor.zoneArbre.mesZonesSaisie]
            const tabPremieresBranches = []// ce tableau contiendra des tableaux de deux éléments correspondant aux couples Evt/Prb des premières branches
            const tabSecondesBranches = []// ce tableau aura autant d’éléments que tabPremieresBranches.length, chaque élément étant un tableau des couples Evt/Prb des secondes branches
            for (let i = 0; i < stor.mesZonesSaisie.length; i += 2) {
            // je corrige d’éventuelles fautes de frappe (espace en trop, \overline{} en trop)
              const laReponse = $(stor.mesZonesSaisie[i]).mathquill('latex')
              const letexteReponse = reecrireEvt(laReponse)
              me.logIfDebug('letexteReponse:', letexteReponse, '   ', stor.mesZonesSaisie[i])
              if (laReponse !== letexteReponse) {
                $(stor.mesZonesSaisie[i]).mathquill('revert')
                $(stor.mesZonesSaisie[i]).mathquill('editable')
                $(stor.mesZonesSaisie[i]).mathquill('write', letexteReponse)
              }
              if (stor.mesZonesSaisie[i].typeZone === 'evt1') {
              // c’est une zone pour un évt des premières branches
              // donc dans le tableau sur les premières branche, j’ajoute le tableau [evt, prb]
                monEvt = simplifieEvt($(stor.mesZonesSaisie[i]).mathquill('latex'))
                tabPremieresBranches.push([monEvt, $(stor.mesZonesSaisie[i + 1]).mathquill('latex')])
                tabSecondesBranches.push([])
              } else {
              // c’est une zone sur les deuxièmes branches
              // j’identifie la première branche associée
                const numPremBranche = stor.mesZonesSaisie[i].branchePrecedente
                const monEvt2 = simplifieEvt($(stor.mesZonesSaisie[i]).mathquill('latex'))
                tabSecondesBranches[numPremBranche].push([monEvt2, j3pCalculValeur($(stor.mesZonesSaisie[i + 1]).mathquill('latex'))])
              }
            }
            const bonNbPremBranches = (tabPremieresBranches.length === stor.evts1Rep.length)
            stor.nbPremieresBranches = tabPremieresBranches.length
            PbNbBranches = false
            // console.log('tabPremieresBranches:',tabPremieresBranches,'  longueur:',tabPremieresBranches.length)
            if (bonNbPremBranches) {
            // je vérifie que le nombre de deuxièmes branches est correct
              let bonNbSecBranches = true
              for (let i = 0; i < tabPremieresBranches.length; i++) {
                bonNbSecBranches = (bonNbSecBranches && (tabSecondesBranches[i].length === stor.evts2Rep[i].length))
              }
              if (!bonNbSecBranches) {
                PbNbBranches = true
              }
            } else {
              PbNbBranches = true
            }

            me.logIfDebug('tabPremieresBranches:', tabPremieresBranches, '\ntabSecondesBranches:', tabSecondesBranches)

            if (PbNbBranches) {
              reponse.bonneReponse = false
              for (let i = 0; i < stor.mesZonesSaisie.length; i++) {
                fctsValid.zones.bonneReponse[i] = false
              }
            } else {
            // on vérifie les couple evt/prb et les secondes branches associées
              testPremieresBranches = egaliteTableauPrb(stor.evts1Rep, tabPremieresBranches)
              reponse.bonneReponse = testPremieresBranches.egalite
              evtsPresents = bonEvts(tabPremieresBranches, stor.listeEvt['enonce' + stor.numeroExo])// ce booléen vérifie si les événements donnés pas l’élèves sont (ou non) dans la liste des événements de l’exo
              if (reponse.bonneReponse) {
              // je regarde si les autres branches sont bonnes et bien associées aux premières
                const bonnesSecondesBranches = []
                for (let i = 0; i < tabPremieresBranches.length; i++) {
                  me.logIfDebug(
                    'testPremieresBranches.positionTab[i]:', testPremieresBranches.positionTab[i],
                    '\ntestPremieresBranches:', testPremieresBranches, testPremieresBranches.positionTab[i],
                    '\nstor.objArbreRep.evts2["branche"+testPremieresBranches.positionTab[i]]:', stor.objArbreRep.evts2['branche' + testPremieresBranches.positionTab[i]],
                    '\ntabSecondesBranches[i]:', tabSecondesBranches[i]
                  )
                  bonnesSecondesBranches[i] = egaliteTableauPrb(stor.objArbreRep.evts2['branche' + testPremieresBranches.positionTab[i]], tabSecondesBranches[i]).egalite
                  reponse.bonneReponse = (reponse.bonneReponse && bonnesSecondesBranches[i])
                  evtsPresents = (evtsPresents && bonEvts(tabSecondesBranches[i], stor.listeEvt['enonce' + stor.numeroExo]))
                }
                if (!reponse.bonneReponse) {
                  let posZoneSecondesBranches = tabPremieresBranches.length * 2
                  for (let i = 0; i < tabPremieresBranches.length; i++) {
                    if (!bonnesSecondesBranches[i]) {
                      for (let k = 0; k < tabSecondesBranches[i].length; k++) {
                        fctsValid.zones.bonneReponse[posZoneSecondesBranches] = false
                        fctsValid.zones.bonneReponse[posZoneSecondesBranches + 1] = false
                        posZoneSecondesBranches += 2
                      }
                    } else {
                      posZoneSecondesBranches += 2 * tabSecondesBranches[i].length
                    }
                  }
                }
              } else {
                for (let i = 0; i < stor.mesZonesSaisie.length; i++) {
                  fctsValid.zones.bonneReponse[i] = false
                }
              }
            }
            for (let i = 0; i < stor.mesZonesSaisie.length; i++) {
              fctsValid.coloreUneZone(stor.mesZonesSaisie[i])
            }
          } else if (ds.typeQuestions[me.etapeCourante - 1] === 2) {
          // question où on demande p_{n+1} en fonction de p_n
            const coefa = stor.a
            const coefb = stor.b
            const maFonctionAffine = coefa + '*x+(' + coefb + ')'
            let reponseEleve = $(stor.zoneInput).mathquill('latex')
            while (reponseEleve.indexOf(stor.reponsePrbTotale['enonce' + stor.numeroExo][3]) > -1) {
              reponseEleve = reponseEleve.replace(stor.reponsePrbTotale['enonce' + stor.numeroExo][3], 'x')
            }
            reponseEleve = j3pMathquillXcas(reponseEleve)

            // console.log('reponseEleve:',reponseEleve,'  maFonctionAffine:',maFonctionAffine)
            // il faut que je vérifie si c’est bien une fonction affine simplifiée
            let tabCoefRep
            if (fractionComplete(reponseEleve)) {
            // mon expression est de la forme (ax+b)/c
              const tabDecoupageFrac = reponseEleve.split('/')
              const numRep = tabDecoupageFrac[0].substring(1, tabDecoupageFrac[0].length - 1)
              // recherche des coefs a et b de ce numérateur ax+b
              tabCoefRep = j3pExtraireCoefsFctAffine(numRep)
              const denRep = (tabDecoupageFrac[1].charAt(0) === '(') ? tabDecoupageFrac[1].substring(1, tabDecoupageFrac[1].length - 1) : tabDecoupageFrac[1]
              tabCoefRep[0] = j3pGetLatexQuotient(tabCoefRep[0], denRep)
              tabCoefRep[1] = j3pGetLatexQuotient(tabCoefRep[1], denRep)
            } else {
            // Mon nouveau pb est d’avoir quelque chose de la forme (ax)/c+d
              const numexRepExp = /\([\d,.x]\)\/\(\d+\)/ // new RegExp('\\({1}[0-9\\.\\,x]{1,}\\){1}/{1}\\({1}[0-9]{1,}\\){1}', 'i')
              if (numexRepExp.test(reponseEleve)) {
                const tab = reponseEleve.match(numexRepExp)
                // console.log('tab:',tab)
                for (let i = 0; i < tab.length; i++) {
                  if (tab[i].includes('x')) {
                    let nume = tab[i].substring(1, tab[i].indexOf('/') - 1)
                    const deno = tab[i].substring(tab[i].indexOf('/') + 2, tab[i].length - 1)
                    nume = nume.replace('x', '')
                    // console.log('nume:',nume,'  deno:',deno)
                    reponseEleve = reponseEleve.replace(tab[i], '(' + nume + ')/(' + deno + ')*x')
                  }
                }
              }
              // console.log('reponseEleve:',reponseEleve)
              tabCoefRep = j3pExtraireCoefsFctAffine(reponseEleve)
            }
            let repFonctionAffine = tabCoefRep[0] + '*x+(' + tabCoefRep[1] + ')'
            repFonctionAffine = j3pMathquillXcas(repFonctionAffine)
            // console.log('repFonctionAffine:',repFonctionAffine)
            imagesEgales = true // là je vérifie juste sir les deux expressions donnent bien les mêmes images
            const arbreLaReponse = new Tarbre(maFonctionAffine, ['x'])
            const arbreRepEleve = new Tarbre(repFonctionAffine, ['x'])
            const arbreRepInit = new Tarbre(reponseEleve, ['x'])
            let bonneFctAffine = (repFonctionAffine.split('x').length === 2) // là je vérifie si la réponse de l’élève est correcte et simplifiée
            if (bonneFctAffine) {
            // autre chose pour avoir la bonneFctAffine : je ne dois pas avoir plus d’un signe + (hormis éventuellement 1 premier signe + que je ne prends pas en compte)
              bonneFctAffine = (repFonctionAffine.substring(1).split('+').length <= 2)
            }
            if (bonneFctAffine) {
              for (let i = 2; i <= 10; i++) {
                bonneFctAffine = (bonneFctAffine && (Math.abs(arbreLaReponse.evalue([i]) - arbreRepEleve.evalue([i])) < Math.pow(10, -10)))
              }
            // console.log('bonneFctAffine:',bonneFctAffine)
            }
            if (!bonneFctAffine) {
              for (let i = 2; i <= 10; i++) {
                imagesEgales = (imagesEgales && (Math.abs(arbreLaReponse.evalue([i]) - arbreRepInit.evalue([i])) < Math.pow(10, -10)))
              }
              if (imagesEgales) {
                if (stor.alerteSimplification) {
                  stor.alerteSimplification = true
                  me.essaiCourant--
                }
              }
            }
            reponse.bonneReponse = bonneFctAffine
            fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
            fctsValid.coloreUneZone(stor.mesZonesSaisie[0])
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          // focus sur la 1re zone vide
          stor.mesZonesSaisie.some(function (zoneSaisie) {
            const laSaisie = (typeof zoneSaisie === 'string') ? j3pElement(zoneSaisie) : zoneSaisie
            if ($(laSaisie).mathquill('latex') === '') {
              j3pFocus(zoneSaisie)
              return true // ça arrête la boucle some
            }
            return false
          })
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // indication éventuelle ici
              if (ds.typeQuestions[me.etapeCourante - 1] === 1) {
              // Dans le cas de l’arbre
                if (PbNbBranches) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
                } else if (!testPremieresBranches.egalite) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment3
                } else {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
                }
                if (!evtsPresents) {
                  stor.zoneCorr.innerHTML += ' ' + ds.textes.comment4
                }
              } else {
                if (imagesEgales) {
                  stor.zoneCorr.innerHTML = ds.textes.comment5
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++

              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                // je récupère le nombre de faute de chaque sorte (arbre [4], intersection [5], proba totale [6], proba cond [7])
                me.typederreurs[3 + ds.typeQuestions[me.etapeCourante - 1]]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          if (me.typederreurs[4] / ds.nbrepetitions >= ds.seuilErreur) {
            // pb sur l’arbre
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[5] / ds.nbrepetitions >= ds.seuilErreur) {
            // pb sur la proba de l’intersection
            me.parcours.pe = ds.pe_3
          } else {
            // autre souci mais non identifié
            me.parcours.pe = ds.pe_4
          }
        }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
