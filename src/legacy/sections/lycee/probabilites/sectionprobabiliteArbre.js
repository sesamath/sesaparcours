import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pChaine, j3pDetruit, j3pElement, j3pGetRandomBool, j3pGetRandomInt, j3pPaletteMathquill, j3pRandomTab, j3pFocus, j3pGetNewId, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeSegment } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Avril 2019
        Construction d’un arbre pour un calcul de probabilité
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['avecArbreTableau', true, 'boolean', 'Avant de demander un calcul de probabilité, on peut (et c’est le cas par défaut) demander quel arbre ou quel tableau représente le contexte.'],
    ['arbreTableau', 'arbre', 'liste', 'On peut choisir comme représenter la situation : à l’aide d’un arbre (par défaut) ou d’un tableau.', ['arbre', 'tableau']]
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    if ((me.questionCourante % ds.nbetapes === 0) || !ds.avecArbreTableau) {
      j3pDetruit(stor.laPalette)
      j3pDetruit(stor.zoneCons6)
    }
    let laCorr1, laCorr4
    if ((me.questionCourante % ds.nbetapes === 1) && ds.avecArbreTableau) {
      laCorr1 = (ds.arbreTableau === 'arbre') ? ds.textes.corrArbre1 : ds.textes.corrTableau1
      j3pAffiche(stor.zoneExpli1, '', laCorr1)
      const laCorr2 = (ds.arbreTableau === 'arbre') ? ds.textes['corrArbre2_' + stor.quest] : ds.textes['corrTableau2_' + stor.quest]
      j3pAffiche(stor.zoneExpli2, '', laCorr2)
    } else {
      if (!ds.avecArbreTableau) {
        laCorr4 = (ds.arbreTableau === 'arbre') ? ds.textes.corrArbre4 : ds.textes.corrTableau4
        j3pAffiche(stor.zoneExpli1, '', laCorr4)
        stor.zoneArbre = j3pAddElt(stor.zoneFig, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { position: 'relative', left: '5px' }) })
        if (ds.arbreTableau === 'arbre') {
          construireArbre(stor.zoneArbre, stor.objArbre)
        } else {
          construireTableau(stor.zoneArbre, stor.objArbre)
        }
      }
      laCorr1 = (ds.arbreTableau === 'arbre') ? ds.textes.corrArbre3 : ds.textes.corrTableau3
      const nbIssues = Math.pow(stor.objArbre.evts1.length, 2)
      j3pAffiche(stor.zoneExpli2, '', laCorr1, { n: nbIssues })
      const fracRep = ('\\frac{' + stor.objRep.issues + '}{' + nbIssues + '}' === stor.objRep.laReponse)
        ? stor.objRep.laReponse
        : '\\frac{' + stor.objRep.issues + '}{' + nbIssues + '}=' + stor.objRep.laReponse
      laCorr4 = (stor.objRep.issues === 1) ? ds.textes.corr42 : ds.textes.corr41
      j3pAffiche(stor.zoneExpli3, '', laCorr4, {
        p: stor.objRep.issues,
        f: fracRep
      })
    }
  }

  function construireArbre (madiv, obj) {
    // cette fonction construit un arbre pondéré avec les evts
    // madiv est le div conteneur de mon arbre
    // obj est l’objet qui contiendra les propriétés pour la construction
    // taille du texte evt (pour les proba, il faudrait qu’elles soient plus petites sans doute) + couleur du texte et des branches
    // obj.lestyle.styletexte et obj.lestyle.couleur
    // obj.evts1 = [['','']]   //Pour cette branche, l’événement et la proba ne sont pas complétés donc on les initialise à 'vide'
    // obj.evts2['branche'+0] = [[''],['']]    //Je nomme branche0 la branche précédente et de cette branche, je fais partir deux branches
    // je tente de gérer la hauteur de mon arbre
    const div = j3pAddElt(madiv, 'div')
    let i, j, hautEvt, largEvt, largEvt2
    let posVerticale, posVerticale2
    const largeurBranche = 120
    const largeurEvt = 20
    const hauteurBranche = 30 // hauteur verticale entre deux branches successives (juste l’espace pour y mettre les evts)
    const nbBrancheEvts1 = obj.evts1.length
    let nbBrancheEvts2 = 0
    for (i = 0; i < nbBrancheEvts1; i++) {
      nbBrancheEvts2 = Math.max(nbBrancheEvts2, obj.evts2['branche' + i].length) // on récupère ainsi le nombre max de branches partant d’un noeud (qui n’est pas la racine)
    }
    stor.mesZonesSaisie = []
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    svg.setAttribute('width', (largeurBranche + largeurEvt) * 2 + 20)
    svg.setAttribute('height', nbBrancheEvts1 * nbBrancheEvts2 * hauteurBranche + 10)
    div.appendChild(svg)
    // il faut que je positionne verticalement la racine (horizontalement, c’est 0)
    const hauteurTotale = nbBrancheEvts1 * nbBrancheEvts2 * hauteurBranche
    const decalVert = hauteurTotale / 2 + 10
    let largeurTotale = 0
    const posNoeudVert = []
    let maxLargEvt = 0
    let numevt = 1
    const nomZonesTxt = [] // utile pour les mettre ensuite en couleur
    const decalHor = 5// Je décale un peut les evts de l’extrémité du segment
    for (i = 0; i < nbBrancheEvts1; i++) {
      if (nbBrancheEvts1 % 2 === 0) {
        posVerticale = (i < nbBrancheEvts1 / 2) ? decalVert - (nbBrancheEvts1 / 2 - i - 0.5) * nbBrancheEvts2 * hauteurBranche : decalVert + (i - nbBrancheEvts1 / 2 + 0.5) * nbBrancheEvts2 * hauteurBranche
      } else {
        posVerticale = (i < (nbBrancheEvts1 - 1) / 2) ? decalVert - (nbBrancheEvts1 / 2 - i) * nbBrancheEvts2 * hauteurBranche / 2 - hauteurBranche + 10 : (Math.abs(i - (nbBrancheEvts1 - 1) / 2) < Math.pow(10, -12)) ? decalVert : decalVert + (i - (nbBrancheEvts1 - 1) / 2 + 0.5) * nbBrancheEvts2 * hauteurBranche / 2 + hauteurBranche
      }
      posNoeudVert.push(posVerticale)
      j3pCreeSegment(svg, {
        x1: 0,
        y1: decalVert,
        x2: largeurBranche,
        y2: posVerticale,
        couleur: obj.lestyle.couleur,
        epaisseur: 1
      })
      const divEvts1 = j3pAddElt(div, 'div', '', { style: obj.lestyle.styletexte })
      j3pStyle(divEvts1, { position: 'absolute' })
      j3pAffiche(divEvts1, '', '$' + obj.evts1[numevt - 1][0] + '$')
      nomZonesTxt.push(divEvts1)
      largEvt = divEvts1.getBoundingClientRect().width
      maxLargEvt = Math.max(maxLargEvt, largEvt)
      hautEvt = divEvts1.getBoundingClientRect().height
      divEvts1.style.top = (posVerticale - hautEvt / 2) + 'px'
      divEvts1.style.left = (largeurBranche + decalHor) + 'px'
      numevt += 1
      // Les branches ne portent pas de probabilités dans ce type d’arbre
    }
    const decalageNoeud = 10 + maxLargEvt + 7
    for (i = 0; i < nbBrancheEvts1; i++) {
      // maintenant je m’occupe des secondes branches
      // la position x du noeud est maxLargEvt+largeurBranche+4
      let maxLargEvt2 = 0
      for (j = 0; j < obj.evts2['branche' + i].length; j++) {
        posVerticale2 = (obj.evts2['branche' + i].length === 1) ? posNoeudVert[i] : (obj.evts2['branche' + i].length % 2 === 0) ? posNoeudVert[i] - (obj.evts2['branche' + i].length / 2 - 0.5) * hauteurBranche + j * hauteurBranche : posNoeudVert[i] - Math.floor(obj.evts2['branche' + i].length / 2 + 0.5) * hauteurBranche / 2 + j * hauteurBranche
        j3pCreeSegment(svg, {
          x1: largeurBranche + decalageNoeud,
          y1: posNoeudVert[i],
          x2: 2 * largeurBranche + largEvt + 4,
          y2: posVerticale2,
          couleur: obj.lestyle.couleur,
          epaisseur: 1
        })
        const divEvts2 = j3pAddElt(div, 'div', '', { style: obj.lestyle.styletexte })
        j3pStyle(divEvts2, { position: 'absolute' })
        j3pAffiche(divEvts2, '', '$' + obj.evts2['branche' + i][j][0] + '$')
        nomZonesTxt.push(divEvts2)
        largEvt2 = divEvts2.getBoundingClientRect().width
        maxLargEvt2 = Math.max(maxLargEvt2, largEvt2)
        divEvts2.style.top = (posVerticale2 - hautEvt / 2) + 'px'
        divEvts2.style.left = (2 * largeurBranche + decalageNoeud) + 'px'
        if (largeurTotale === 0) {
          largeurTotale = (2 * largeurBranche + decalageNoeud) + largEvt2
        }
      }
    }
    nomZonesTxt.forEach(elt => $(elt).css('color', obj.lestyle.couleur))
  }

  function construireTableau (conteneur, obj) {
    // cette fonction construit un tableau à double entrée
    // conteneur est le div conteneur de mon tableau
    // obj est l’objet qui contiendra les propriétés pour la construction
    // taille du texte evt couleur du texte et des bordures
    // obj.lestyle.styletexte et obj.lestyle.couleur
    // obj.evts1 = [['','']]   //Pour cette branche, l’événement et la proba ne sont pas complétés donc on les initialise à 'vide'
    // obj.evts2['branche'+0] = [[''],['']]    //Je nomme branche0 la branche précédente et de cette branche, je fais partir deux branches
    const madiv = (typeof (conteneur) === 'string') ? j3pElement(conteneur) : conteneur
    const table = j3pAddElt(madiv, 'table', { width: '97%', borderColor: obj.lestyle.couleur, border: 2, cellSpacing: 0 })
    const ligne1 = j3pAddElt(table, 'tr')
    j3pAddElt(ligne1, 'th')
    for (let k = 0; k < obj.evts1.length; k++) {
      const legLigne0 = j3pAddElt(ligne1, 'th', { align: 'center' })
      j3pAffiche(legLigne0, '', '$' + obj.evts1[k][0] + '$')
    }
    for (let k = 0; k < obj.evts2.branche0.length; k++) {
      const ligne = j3pAddElt(table, 'tr')
      const legende = j3pAddElt(ligne, 'th', { align: 'center' })
      j3pAffiche(legende, '', '$' + obj.evts2.branche0[k][0] + '$')
      for (let k2 = 0; k2 < obj.evts1.length; k2++) {
        const evt = j3pAddElt(ligne, 'td', { align: 'center' })
        j3pAffiche(evt, '', '$' + obj.evts1[k2][0] + obj.evts2.branche0[k][0] + '$')
      }
    }
    const th = $('th')
    th.css('background-color', 'rgba(120,120,120,0.25)')
    th.css('font-weight', 'bold')
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 2,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      avecArbreTableau: true,
      arbreTableau: 'arbre',

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo1: 'Utiliser un arbre de dénombrement',
        titre_exo2: 'Utiliser un tableau pour dénombrer',
        // on donne les phrases de la consigne
        consigne1_1: 'On lance deux fois de suite une pièce bien équilibrée et on note à chaque fois la face apparente.',
        consigne1_2: 'On dispose d’un dé cubique bien équilibré où les faces opposées sont de la même couleur : bleu, vert et rouge. On lance deux fois ce dé et on note la couleur de la face supérieure.',
        consigne1_3: 'On demande à un ordinateur de choisir de manière aléatoire un nombre entier compris entre 1 et 2. On lui demande d’effectuer ce choix à deux reprises et on note la valeur obtenue.',
        consigne1_4: 'Dans une urne, sont disposées trois boules : une rouge, une blanche et une noire. On tire successivement deux boules de cette urne en remettant la première tirée.',
        consigne1_5: 'On tire successivement deux cartes d’un jeu de 32 cartes en remettant la première tirée. On s’intéresse alors aux couleurs pique, trèfle, cœur et carreau de chacune de ces cartes.',
        consigne2_1: 'Le résultat de l’expérience aléatoire est la suite des faces obtenues dans l’ordre, par exemple PF (pour pile puis face).',
        consigne2_2: 'Le résultat de l’expérience aléatoire est la suite des couleurs obtenues dans l’ordre, par exemple VB (pour vert puis bleu).',
        consigne2_3: 'Le résultat de l’expérience aléatoire est la suite des chiffres obtenues dans l’ordre, par exemple 12 (pour un puis deux).',
        consigne2_4: 'Le résultat de l’expérience aléatoire est la suite des couleurs obtenues dans l’ordre, par exemple RN (pour rouge puis noir).',
        consigne2_5: 'Le résultat de l’expérience aléatoire est la suite des couleurs obtenues dans l’ordre, par exemple CaT (pour carreau puis trèfle).',
        consigne3_1: '$P$ et $F$ désignent respectivement "Pile" et "Face" et $L_1$ et $L_2$ désignent chacun des deux lancers.',
        consigne3_2: '$B$, $V$ et $R$ désignent respectivement les couleurs bleu, vert ou rouge et $D_1$ et $D_2$ désignent chacun des deux dés.',
        consigne3_3: '$1$ et $2$ désignent les chiffres possibles et $C_1$ et $C_2$ désignent les deux choix de l’ordinateur.',
        consigne3_4: '$R$, $B$ et $N$ désignent respectivement les couleurs rouge, blanc ou noir et $T_1$ et $T_2$ désignent chacun des deux tirages.',
        consigne3_5: '$P$, $T$, $Co$ et $Ca$ désignent respectivement les couleurs pique, trèfle, cœur ou carreau et $T_1$ et $T_2$ désignent chacun des deux tirages.',
        consigne4_1: 'Sélectionne l’arbre ci-dessous correspondant à la situation.',
        consigne4_2: 'Sélectionne le tableau ci-dessous correspondant à la situation.',
        consigne5_1: 'Voici ci-contre l’arbre représentant la situation :',
        consigne5_2: 'Voici ci-contre le tableau représentant la situation :',
        consigne6_1_1: 'Quelle est la probabilité d’obtenir deux fois "£f" au cours de cette expérience&nbsp;?',
        consigne6_1_2: 'Quelle est la probabilité d’obtenir deux résultats différents au cours de ces deux lancers&nbsp;?',
        consigne6_1_3: 'Quelle est la probabilité d’obtenir au moins une fois "£f" au cours de ces deux lancers&nbsp;?',
        consigne6_2_1: 'Quelle est la probabilité d’obtenir deux fois la couleur £c au cours de cette expérience&nbsp;?',
        consigne6_2_2: 'Quelle est la probabilité d’obtenir deux couleurs différentes au cours de ces deux lancers&nbsp;?',
        consigne6_2_3: 'Quelle est la probabilité d’obtenir au moins une fois la couleur £c au cours de ces deux lancers&nbsp;?',
        consigne6_3_1: 'Quelle est la probabilité d’obtenir le nombre £n au cours de cette expérience&nbsp;?',
        consigne6_3_2: 'Quelle est la probabilité d’obtenir deux chiffres différents au cours de ces deux choix aléatoires&nbsp;?',
        consigne6_3_3: 'Quelle est la probabilité d’obtenir au moins une fois le chiffre £n au cours de ces deux choix aléatoires&nbsp;?',
        consigne6_4_1: 'Quelle est la probabilité d’obtenir deux fois la couleur £c au cours de cette expérience&nbsp;?',
        consigne6_4_2: 'Quelle est la probabilité d’obtenir deux couleurs différentes au cours de ces deux tirages&nbsp;?',
        consigne6_4_3: 'Quelle est la probabilité d’obtenir au moins une fois la couleur £c au cours de ces deux tirages&nbsp;?',
        consigne6_5_1: 'Quelle est la probabilité d’obtenir deux fois la couleur £c au cours de cette expérience&nbsp;?',
        consigne6_5_2: 'Quelle est la probabilité d’obtenir deux couleurs différentes au cours de ces deux tirages&nbsp;?',
        consigne6_5_3: 'Quelle est la probabilité d’obtenir au moins une fois la couleur £c au cours de ces deux tirages&nbsp;?',
        consigne7: 'Réponse : &1&.',
        consigne8: 'Les probabilités devront être écrites de manière simplifiée, sous la forme décimale ou d’une fraction irréductible.',
        consigne9: 'Tu n’as qu’une tentative pour répondre.',
        arbre: 'arbre £n',
        tableau: 'tableau £n',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Les résultats doivent être exacts (non approchés) et donnés sous forme irréductible&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corrArbre1: 'Chaque nœud de l’arbre correspond à une épreuve aléatoire.',
        corrTableau1: 'Sur la première ligne et la première colonne, on trouve les différentes éventualités au cours de chaque étape de cette expérience aléatoire.',
        corrArbre2_1: 'Le premier nœud correspond alors au résultat du premier lancer (pile ou face) et le second à celui du deuxième lancer (toujours pile ou face).',
        corrArbre2_2: 'Le premier nœud correspond alors à la couleur lors du premier lancer (bleu, vert ou rouge) et le second à celle du deuxième lancer (toujours bleu, vert ou rouge).',
        corrArbre2_3: 'Le premier nœud correspond alors au résultat du premier choix aléatoire (1 ou 2) et le second à celui du deuxième choix (toujours 1 ou 2).',
        corrArbre2_4: 'Le premier nœud correspond alors au résultat du premier tirage (rouge, blanc ou noir) et le second à celui du deuxième tirage (toujours rouge, blanc ou noir).',
        corrArbre2_5: 'Le premier nœud correspond alors au résultat du premier tirage (pique, trèfle, cœur ou carreau) et le second à celui du deuxième tirage (toujours pique, trèfle, cœur ou carreau).',
        corrTableau2_1: 'La première ligne correspond alors au résultat du premier lancer (pile ou face) et la première colonne à celui du deuxième lancer (toujours pile ou face).',
        corrTableau2_2: 'La première ligne correspond alors à la couleur lors du premier lancer (bleu, vert ou rouge) et la première colonne à celle du deuxième lancer (toujours bleu, vert ou rouge).',
        corrTableau2_3: 'La première ligne correspond alors au résultat du premier choix aléatoire (1 ou 2) et la première colonne à celui du deuxième choix (toujours 1 ou 2).',
        corrTableau2_4: 'La première ligne correspond alors à la couleur lors du premier tirage (rouge, blanc ou noir) et la première colonne à celle du deuxième tirage (toujours rouge, blanc ou noir).',
        corrTableau2_5: 'La première ligne correspond alors à la couleur lors du premier tirage (pique, trèfle, cœur ou carreau) et la première colonne à celle du deuxième tirage (toujours pique, trèfle, cœur ou carreau).',
        corrArbre3: 'D’après l’arbre, l’expérience possède £n issues.',
        corrTableau3: 'D’après le tableau, l’expérience possède £n issues.',
        corr41: '£p issues réalisent l’événement de l’énoncé. Donc la probabilité cherchée vaut $£f$.',
        corr42: 'Une issue réalise l’événement de l’énoncé. Donc la probabilité cherchée vaut $£f$.',
        corrArbre4: 'Voici l’arbre représentant la situation ci-contre :',
        corrTableau4: 'Voici le tableau représentant la situation ci-contre :'
      },
      pe: 0
    }
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (ds.nbetapes === 1 || me.questionCourante % ds.nbetapes === 1) {
      if (stor.tabQuest.length === 0) {
        stor.tabQuest = [...stor.tabQuestInit]
      }
      const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
      const quest = stor.tabQuest[pioche]
      stor.tabQuest.splice(pioche, 1)
      // ensuite je dois créer l’objet qui contiendra toutes les caractéristiques de mon arbre
      stor.objArbre = {}
      stor.objArbre2 = {}
      // taille du texte evt (pour les proba, il faudrait qu’elles soient plus petites sans doute) + couleur du texte et des branches
      stor.objArbre.lestyle = { styletexte: me.styles.toutpetit.enonce, couleur: me.styles.toutpetit.enonce.color }
      stor.objArbre.complet = { evts: ds.afficheEvt, proba: false }
      stor.objArbre2.lestyle = {
        styletexte: me.styles.toutpetit.enonce,
        couleur: me.styles.toutpetit.enonce.color
      }
      stor.objArbre2.complet = { evts: ds.afficheEvt, proba: false }
      switch (quest) {
        case 1:
          stor.objArbre.evts1 = [['P', ''], ['F', '']]
          stor.objArbre.evts2 = {}
          for (let i = 0; i < stor.objArbre.evts1.length; i++) {
            stor.objArbre.evts2['branche' + i] = [['P', ''], ['F', '']]
          }
          stor.objArbre2.evts1 = [['L_1', ''], ['L_2', '']]
          stor.objArbre2.evts2 = {}
          for (let i = 0; i < stor.objArbre2.evts1.length; i++) {
            stor.objArbre2.evts2['branche' + i] = [['P', ''], ['F', '']]
          }
          break
        case 2:
          stor.objArbre.evts1 = [['B', ''], ['V', ''], ['R', '']]
          stor.objArbre.evts2 = {}
          for (let i = 0; i < stor.objArbre.evts1.length; i++) {
            stor.objArbre.evts2['branche' + i] = [['B', ''], ['V', ''], ['R', '']]
          }
          stor.objArbre2.evts1 = [['D_1', ''], ['D_2', '']]
          stor.objArbre2.evts2 = {}
          for (let i = 0; i < stor.objArbre2.evts1.length; i++) {
            stor.objArbre2.evts2['branche' + i] = [['B', ''], ['V', ''], ['R', '']]
          }
          break
        case 3:
          stor.objArbre.evts1 = [['1', ''], ['2', '']]
          stor.objArbre.evts2 = {}
          for (let i = 0; i < stor.objArbre.evts1.length; i++) {
            stor.objArbre.evts2['branche' + i] = [['1', ''], ['2', '']]
          }
          stor.objArbre2.evts1 = [['C_1', ''], ['C_2', '']]
          stor.objArbre2.evts2 = {}
          for (let i = 0; i < stor.objArbre2.evts1.length; i++) {
            stor.objArbre2.evts2['branche' + i] = [['1', ''], ['2', '']]
          }
          break
        case 4:
          stor.objArbre.evts1 = [['R', ''], ['B', ''], ['N', '']]
          stor.objArbre.evts2 = {}
          for (let i = 0; i < stor.objArbre.evts1.length; i++) {
            stor.objArbre.evts2['branche' + i] = [['R', ''], ['B', ''], ['N', '']]
          }
          stor.objArbre2.evts1 = [['T_1', ''], ['T_2', '']]
          stor.objArbre2.evts2 = {}
          for (let i = 0; i < stor.objArbre2.evts1.length; i++) {
            stor.objArbre2.evts2['branche' + i] = [['R', ''], ['B', ''], ['N', '']]
          }
          break
        case 5:
          stor.objArbre.evts1 = [['P', ''], ['T', ''], ['Co', ''], ['Ca', '']]
          stor.objArbre.evts2 = {}
          for (let i = 0; i < stor.objArbre.evts1.length; i++) {
            stor.objArbre.evts2['branche' + i] = [['P', ''], ['T', ''], ['Co', ''], ['Ca', '']]
          }
          stor.objArbre2.evts1 = [['T_1', ''], ['T_2', '']]
          stor.objArbre2.evts2 = {}
          for (let i = 0; i < stor.objArbre2.evts1.length; i++) {
            stor.objArbre2.evts2['branche' + i] = [['P', ''], ['T', ''], ['Co', ''], ['Ca', '']]
          }
          break
      }
      stor.quest = quest
    }
    j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + stor.quest])
    j3pAffiche(stor.zoneCons2, '', ds.textes['consigne2_' + stor.quest])
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneFig = j3pAddElt(stor.conteneurD, 'div')
    if ((me.questionCourante % ds.nbetapes === 1) && ds.avecArbreTableau) {
      j3pAffiche(stor.zoneCons3, '', ds.textes['consigne3_' + stor.quest])
      const laCons4 = (ds.arbreTableau === 'arbre') ? ds.textes.consigne4_1 : ds.textes.consigne4_2
      j3pAffiche(stor.zoneCons4, '', laCons4)
      stor.zonesFigs = j3pAddElt(stor.zoneCons5, 'div')
      const largMG = me.zonesElts.MG.getBoundingClientRect().width
      stor.zonesFigs.setAttribute('width', largMG + 'px')
      // const texte = ''
      let choixPos
      stor.idsRadio = []
      ;[1, 2].forEach(i => {
        // Création d’id pour les boutons radio et les arbres
        stor.idsRadio.push(j3pGetNewId('idRadio' + i))
      })
      const table = j3pAddElt(stor.zonesFigs, 'div', { width: '100%', borderColor: 'black', border: 0 })
      const conteneurRadio = []
      stor.conteneurArbre = []
      const ligne1 = j3pAddElt(table, 'tr')
      for (let i = 1; i <= 2; i++) {
        conteneurRadio.push(j3pAddElt(ligne1, 'td', { width: (largMG / 2 - 3) + 'px', align: 'center' }))
      }
      const ligne2 = j3pAddElt(table, 'tr')
      for (let i = 1; i <= 2; i++) {
        stor.conteneurArbre.push(j3pAddElt(ligne2, 'td', { width: (largMG / 2 - 3) + 'px' }))
      }
      stor.idChoix = j3pGetNewId('choix')
      if (ds.arbreTableau === 'arbre') {
        ;[0, 1].forEach(i => {
          stor['zoneArbre' + i] = j3pAddElt(stor.conteneurArbre[i], 'div', '', { style: me.styles.etendre('toutpetit.enonce', { position: 'relative', left: '5px' }) })
        })
        choixPos = j3pGetRandomInt(0, 1)
        if (choixPos === 0) {
          construireArbre(stor.zoneArbre0, stor.objArbre)
          construireArbre(stor.zoneArbre1, stor.objArbre2)
          stor.numArbre = 1
        } else {
          construireArbre(stor.zoneArbre0, stor.objArbre2)
          construireArbre(stor.zoneArbre1, stor.objArbre)
          stor.numArbre = 2
        }
        j3pBoutonRadio(conteneurRadio[0], stor.idsRadio[0], stor.idChoix, 0, j3pChaine(ds.textes.arbre, { n: 1 }))
        j3pBoutonRadio(conteneurRadio[1], stor.idsRadio[1], stor.idChoix, 1, j3pChaine(ds.textes.arbre, { n: 2 }))
      } else {
        choixPos = j3pGetRandomInt(0, 1)
        if (choixPos === 0) {
          construireTableau(stor.conteneurArbre[0], stor.objArbre)
          construireTableau(stor.conteneurArbre[1], stor.objArbre2)
          stor.numArbre = 1
        } else {
          construireTableau(stor.conteneurArbre[0], stor.objArbre2)
          construireTableau(stor.conteneurArbre[1], stor.objArbre)
          stor.numArbre = 2
        }
        j3pBoutonRadio(conteneurRadio[0], stor.idsRadio[0], stor.idChoix, 0, j3pChaine(ds.textes.tableau, { n: 1 }))
        j3pBoutonRadio(conteneurRadio[1], stor.idsRadio[1], stor.idChoix, 1, j3pChaine(ds.textes.tableau, { n: 2 }))
      }
    } else {
      if (ds.avecArbreTableau) {
        const laCons3 = (ds.arbreTableau === 'arbre') ? ds.textes.consigne5_1 : ds.textes.consigne5_2
        j3pAffiche(stor.zoneCons3, '', laCons3)
      }
      const objQuest4 = {}
      let laReponse, laReponseTxt, nbIssues, choixAlea
      switch (stor.quest) {
        case 1:
          objQuest4.f = (j3pGetRandomBool()) ? 'Pile' : 'Face'
          laReponse = (stor.numQuestProba === 1) ? 1 / 4 : (stor.numQuestProba === 2) ? 1 / 2 : 3 / 4
          laReponseTxt = (stor.numQuestProba === 1) ? '\\frac{1}{4}' : (stor.numQuestProba === 2) ? '\\frac{1}{2}' : '\\frac{3}{4}'
          nbIssues = (stor.numQuestProba === 1) ? 1 : (stor.numQuestProba === 2) ? 2 : 3
          break
        case 2:
          choixAlea = j3pGetRandomInt(0, 2)
          objQuest4.c = (choixAlea === 0) ? 'bleu' : (choixAlea === 1) ? 'vert' : 'rouge'
          laReponse = (stor.numQuestProba === 1) ? 1 / 9 : (stor.numQuestProba === 2) ? 2 / 3 : 5 / 9
          laReponseTxt = (stor.numQuestProba === 1) ? '\\frac{1}{9}' : (stor.numQuestProba === 2) ? '\\frac{2}{3}' : '\\frac{5}{9}'
          nbIssues = (stor.numQuestProba === 1) ? 1 : (stor.numQuestProba === 2) ? 6 : 5
          break
        case 3:
          if (stor.numQuestProba === 1) {
            objQuest4.n = j3pRandomTab([11, 12, 21, 22], [0.25, 0.25, 0.25, 0.25])
          } else {
            objQuest4.n = j3pGetRandomInt(1, 2)
          }
          laReponse = (stor.numQuestProba === 1) ? 1 / 4 : (stor.numQuestProba === 2) ? 1 / 2 : 3 / 4
          laReponseTxt = (stor.numQuestProba === 1) ? '\\frac{1}{4}' : (stor.numQuestProba === 2) ? '\\frac{1}{2}' : '\\frac{3}{4}'
          nbIssues = (stor.numQuestProba === 1) ? 1 : (stor.numQuestProba === 2) ? 2 : 3
          break
        case 4:
          choixAlea = j3pGetRandomInt(0, 2)
          objQuest4.c = (choixAlea === 0) ? 'rouge' : (choixAlea === 1) ? 'blanc' : 'noir'
          laReponse = (stor.numQuestProba === 1) ? 1 / 9 : (stor.numQuestProba === 2) ? 2 / 3 : 5 / 9
          laReponseTxt = (stor.numQuestProba === 1) ? '\\frac{1}{9}' : (stor.numQuestProba === 2) ? '\\frac{2}{3}' : '\\frac{5}{9}'
          nbIssues = (stor.numQuestProba === 1) ? 1 : (stor.numQuestProba === 2) ? 6 : 5
          break
        case 5:
          choixAlea = j3pGetRandomInt(0, 3)
          objQuest4.c = (choixAlea === 0) ? 'pique' : (choixAlea === 1) ? 'trèfle' : (choixAlea === 2) ? 'cœur' : 'carreau'
          laReponse = (stor.numQuestProba === 1) ? 1 / 16 : (stor.numQuestProba === 2) ? 3 / 4 : 7 / 16
          laReponseTxt = (stor.numQuestProba === 1) ? '\\frac{1}{16}' : (stor.numQuestProba === 2) ? '\\frac{3}{4}' : '\\frac{7}{16}'
          nbIssues = (stor.numQuestProba === 1) ? 1 : (stor.numQuestProba === 2) ? 12 : 7
          break
      }
      stor.objRep = { laReponse: laReponseTxt, issues: nbIssues }
      j3pAffiche(stor.zoneCons4, '', ds.textes['consigne6_' + stor.quest + '_' + stor.numQuestProba], objQuest4)
      const elt = j3pAffiche(stor.zoneCons5, '', ds.textes.consigne7, { inputmq1: { texte: '' } })
      stor.zoneInput = elt.inputmqList[0]
      stor.laPalette = j3pAddElt(stor.zoneCons7, 'div')
      // palette MQ :
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput, {
        liste: ['fraction']
      })
      mqRestriction(stor.zoneInput, '\\d,.-/', {
        commandes: ['fraction']
      })
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      stor.zoneInput.reponse = [laReponse]
      stor.zoneInput.solutionSimplifiee = ['reponse fausse', 'non valide']
      stor.numQuestProba = stor.numQuestProba % 3 + 1
      j3pStyle(stor.zoneCons6, { fontStyle: 'italic', fontSize: '0.9em' })
      ;[1, 2].forEach(i => j3pAddElt(stor.zoneCons6, 'div', ds.textes['consigne' + (7 + i)]))
      const mesZonesSaisie = [stor.zoneInput.id]
      j3pFocus(stor.zoneInput)
      stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })

      if (ds.avecArbreTableau) {
        stor.zoneArbre = j3pAddElt(stor.zoneFig, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { position: 'relative', left: '5px' }) })
        if (ds.arbreTableau === 'arbre') {
          construireArbre(stor.zoneArbre, stor.objArbre)
        } else {
          construireTableau(stor.zoneArbre, stor.objArbre)
        }
      }
    }

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70

        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc / 100 })
        if (!ds.avecArbreTableau) {
          ds.nbetapes = 1
          ds.nbitems = ds.nbetapes * ds.nbrepetitions
        }
        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0
        const leTitre = (ds.arbreTableau === 'arbre') ? ds.textes.titre_exo1 : ds.textes.titre_exo2
        me.afficheTitre(leTitre)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.tabQuest = [1, 2, 3, 4, 5]
        stor.tabQuestInit = [...stor.tabQuest]
        stor.numQuestProba = (ds.nbrepetitions <= 2) ? j3pGetRandomInt(1, 2) : 1
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        let numRadioSelect
        let // ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
          fracIrre = true// utile pour le calcul de proba
        let reponse
        if ((me.questionCourante % 2 === 1) && ds.avecArbreTableau) {
          reponse = { aRepondu: false, bonneReponse: false }
          reponse.aRepondu = (j3pBoutonRadioChecked(stor.idChoix)[0] > -1)
          if (reponse.aRepondu) {
            numRadioSelect = j3pBoutonRadioChecked(stor.idChoix)[0] + 1
            reponse.bonneReponse = (numRadioSelect === stor.numArbre)
          }
        } else {
          reponse = fctsValid.validationGlobale()
          fracIrre = fctsValid.zones.reponseSimplifiee[0][1]
          if (!reponse.aRepondu && (stor.zoneInput.solutionSimplifiee[1] === 'non valide')) {
          // Si c’est un pb de fraction irréductible, la première fois, je donne un message d’avertissement et ensuite, je compte ça comme faux
            if (!fracIrre) stor.zoneInput.solutionSimplifiee[1] = 'reponse fausse'
          }
        }

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // On peut ajouter un commentaire particulier.
          let msgReponseManquante
          if (!fracIrre) {
          // c’est que la fraction de la quatrième zone de saisie est réductible
            msgReponseManquante = ds.textes.comment1
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            if ((me.questionCourante % 2 === 1) && ds.avecArbreTableau) {
              for (let i = 0; i <= 1; i++) j3pDesactive(stor.idsRadio[i])
            }
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!fracIrre) {
              // c’est que la fraction de la zone de saisie est réductible
                stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                if ((me.questionCourante % 2 === 1) && ds.avecArbreTableau) {
                  for (let i = 0; i <= 1; i++) j3pDesactive(stor.idsRadio[i])
                  j3pBarre('label' + stor.idsRadio[numRadioSelect - 1])
                  j3pBarre(stor.conteneurArbre[numRadioSelect - 1])
                }
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
