import { j3pAddElt, j3pArrondi, j3pNombre, j3pFocus, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pImporteAnnexe, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { remplaceCalcul } from 'src/legacy/outils/fonctions/gestionParametres'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    avril 2017
    Cette section permet de retrouver la valeur du paramètre sigma dans le cas d’une loi normale dont on connait l’espérance et une probabilité
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['nbetapes', 1, 'entier', 'Par défaut, on demande directement la valeur de l’écart-type. On peut demander une étape intermédiaire et mettant la valeur 2 à nbetapes.'],
    ['tabCasFigure', [''], 'array', 'Pour chaque répétition, on peut imposer, sous forme d’un tableau, un des exercices particuliers présents dans le fichier texte annexe :<br/>-1 : Amérique du Nord juin 2016 S<br/>-2 : Métropole septembre 2016 S<br/>-3 : Amérique du Nord juin 2015 S<br/>-4 : Amérique du Nord juin 2014 S<br/>-5 : Asie juin 2014 S<br/>-6 : Antilles septembre 2014 S<br/>-7 : Amérique du Sud novembre 2014 S'],
    ['intervalleGauche', false, 'boolean', 'Lorsque ce paramètre vaut true, on repasse par un intervalle à gauche même si la probabilité initiale n’est pas nécessairement ce type d’intervalle (utile pour certains modèles de calculatrices).']
  ]
}

/**
 * section loinormaleSigma
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })

    function actualisationFigure () {
      let valA, valB
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'mu', '0')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'sigma', '1')
      if (ds.intervalleGauche) {
        valA = remplaceCalcul({
          text: stor.bornesGraph['enonce' + stor.numeroExo][1][0],
          obj: stor.obj
        })
        valB = remplaceCalcul({
          text: stor.bornesGraph['enonce' + stor.numeroExo][1][1],
          obj: stor.obj
        })
      } else {
        valA = remplaceCalcul({
          text: stor.bornesGraph['enonce' + stor.numeroExo][0][0],
          obj: stor.obj
        })
        valB = remplaceCalcul({
          text: stor.bornesGraph['enonce' + stor.numeroExo][0][1],
          obj: stor.obj
        })
      }
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(valA))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(valB))
      if (!ds.intervalleGauche && String(stor.bornesGraph['enonce' + stor.numeroExo][0][1]) === '5') {
        // c’est qu’on a un intervalle à droite
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a1', String(stor.obj.u))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a2', String('10'))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'absPrb', String(valA + 0.3))
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a1', String(-stor.obj.u))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a2', String(stor.obj.u))
        if (ds.intervalleGauche || String(stor.bornesGraph['enonce' + stor.numeroExo][0][0]) === '-5') {
          if (stor.obj.p > 0.5) {
            stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'absPrb', String(valB / 2))
          } else {
            stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'absPrb', String(valB - 0.3))
          }
        } else {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'absPrb', String(valB / 2))
        }
      }
      if (ds.intervalleGauche) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'p', String(stor.obj.q))
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'p', String(stor.obj.p))
      }
      stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
      stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
    }

    let numccl
    if (ds.nbetapes === 2) {
      if (me.questionCourante % 2 === 1) {
        if (ds.intervalleGauche) {
          for (let i = 1; i <= stor.explicationsCompTexas['enonce' + stor.numeroExo].length; i++) {
            const zones = j3pAddElt(zoneExpli, 'div')
            j3pAffiche(zones, '', stor.explicationsCompTexas['enonce' + stor.numeroExo][i - 1], stor.obj)
          }
        } else {
          for (let i = 1; i <= stor.explicationsComp['enonce' + stor.numeroExo].length; i++) {
            const zones = j3pAddElt(zoneExpli, 'div')
            j3pAffiche(zones, '', stor.explicationsComp['enonce' + stor.numeroExo][i - 1], stor.obj)
          }
        }
        depart(true, function () {
          // je modifie ici les données de la figure pour l’adapter à l’exo
          actualisationFigure()
        })
      } else {
        if (ds.intervalleGauche) {
          for (let i = 1; i <= stor.explicationsTexas2['enonce' + stor.numeroExo].length; i++) {
            const zones = j3pAddElt(zoneExpli, 'div')
            j3pAffiche(zones, '', stor.explicationsTexas2['enonce' + stor.numeroExo][i - 1], stor.obj)
          }
        } else {
          for (let i = 1; i <= stor.explications2['enonce' + stor.numeroExo].length; i++) {
            const zones = j3pAddElt(zoneExpli, 'div')
            j3pAffiche(zones, '', stor.explications2['enonce' + stor.numeroExo][i - 1], stor.obj)
          }
        }
        numccl = ((ds.nbetapes === 1) || (me.questionCourante % 2 === 2)) ? 0 : 1
        const lastExpli = j3pAddElt(zoneExpli, 'div')
        if (ds.intervalleGauche) {
          j3pAffiche(lastExpli, '', stor.conclusionTexas['enonce' + stor.numeroExo][numccl], stor.obj)
        } else {
          j3pAffiche(lastExpli, '', stor.conclusion['enonce' + stor.numeroExo][numccl], stor.obj)
        }
      }
    } else {
      if (ds.intervalleGauche) {
        for (let i = 1; i <= stor.explicationsCompTexas2['enonce' + stor.numeroExo].length; i++) {
          const zones = j3pAddElt(zoneExpli, 'div')
          j3pAffiche(zones, '', stor.explicationsCompTexas2['enonce' + stor.numeroExo][i - 1], stor.obj)
        }
        for (let i = 1; i <= stor.explicationsTexas['enonce' + stor.numeroExo].length; i++) {
          const zones = j3pAddElt(zoneExpli, 'div')
          j3pAffiche(zones, '', stor.explicationsTexas['enonce' + stor.numeroExo][i - 1], stor.obj)
        }
      } else {
        for (let i = 1; i <= stor.explicationsComp2['enonce' + stor.numeroExo].length; i++) {
          const zones = j3pAddElt(zoneExpli, 'div')
          j3pAffiche(zones, '', stor.explicationsComp2['enonce' + stor.numeroExo][i - 1], stor.obj)
        }
        for (let i = 1; i <= stor.explications['enonce' + stor.numeroExo].length; i++) {
          const zones = j3pAddElt(zoneExpli, 'div')
          j3pAffiche(zones, '', stor.explications['enonce' + stor.numeroExo][i - 1], stor.obj)
        }
      }
      numccl = ((ds.nbetapes === 1) || (me.questionCourante % 2 === 2)) ? 0 : 1
      const lastExpli = j3pAddElt(zoneExpli, 'div')
      if (ds.intervalleGauche) {
        j3pAffiche(lastExpli, '', stor.conclusionTexas['enonce' + stor.numeroExo][numccl], stor.obj)
      } else {
        j3pAffiche(lastExpli, '', stor.conclusion['enonce' + stor.numeroExo][numccl], stor.obj)
      }
      depart(true, function () {
        // je modifie ici les données de la figure pour l’adapter à l’exo
        actualisationFigure()
      })
    }
  }

  function depart (figureAffichee, callback) {
    // txt html de la figure
    let txtFigure
    if (figureAffichee) {
      // cela ne se fait dans cette section qu'à la fin où on affiche la figure où apparait la zone dont on cherche l’aire
      const divMtg32 = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '5px' }) })
      // et enfin la zone svg (très importante car tout est dedans)
      const largFig = 370// on gère ici la largeur de la figure
      const hautFig = 198
      stor.mtg32svg = j3pGetNewId('mtg32svg')
      j3pCreeSVG(divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
      stor.mtgAppLecteur.removeAllDoc()
      // txt html de la figure
      txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH2+v4BAP8BAAAAAAAAAAABcgAAAMoAAAAAAAAAAAAAAAAAAABc#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEAD0NWYXJpYWJsZUJvcm5lZQD#####AAVzaWdtYT#wAAAAAAAAP+AAAAAAAABACAAAAAAAAD+5mZmZmZmaAAADMC41AAEzAAMwLjEAAAACAP####8AAm11AAAAAAAAAADAAAAAAAAAAEAAAAAAAAAAP8mZmZmZmZoAAAItMgABMgADMC4y#####wAAAAEAB0NDYWxjdWwA#####wACYTYABC0wLjX#####AAAAAQAMQ01vaW5zVW5haXJlAAAAAT#gAAAAAAAAAAAAAwD#####AAJhNQACLTEAAAAEAAAAAT#wAAAAAAAA#####wAAAAEACkNQb2ludEJhc2UA#####wEAAAAAEAABTwDALgAAAAAAAEAQAAAAAAAABQABQGVgAAAAAABAY6AAAAAAAP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAAAAPAAABAAEAAAAFAT#qqqqqqqqr#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAEQAAFJAMAQAAAAAAAAQBAAAAAAAAAFAAFASmZmZmZmawAAAAb#####AAAAAQAJQ0Ryb2l0ZUFCAP####8AAAAAABAAAAEAAQAAAAUAAAAH#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAADwAAAQABAAAABQAAAAYAAAAHAP####8BAAAAARAAAUoAwCYAAAAAAAAAAAAAAAAAAAUAAcBxLMzMzMzNAAAACQAAAAgA#####wEAAAAAEAAAAQABAAAABQAAAAr#####AAAAAgAHQ1JlcGVyZQD#####AICAgAEBAAAABQAAAAcAAAAKAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACUNMb25ndWV1cgD#####AAAABQAAAAcAAAADAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAAAwD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABdHcmFkdWF0aW9uQXhlc1JlcGVyZU5ldwAAABsAAAAIAAAAAwAAAAwAAAAOAAAAD#####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAABAABWFic29yAAAADP####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAABAABW9yZG9yAAAADP####8AAAABAApDVW5pdGV4UmVwAAAAABAABnVuaXRleAAAAAz#####AAAAAQAKQ1VuaXRleVJlcAAAAAAQAAZ1bml0ZXkAAAAM#####wAAAAEAEENQb2ludERhbnNSZXBlcmUAAAAAEAAAAAAADwAAAQUAAAAADP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAARAAAAEgAAABIAAAARAAAAABAAAAAAAA8AAAEFAAAAAAz#####AAAAAQAKQ09wZXJhdGlvbgAAAAASAAAAEQAAABIAAAATAAAAEgAAABIAAAARAAAAABAAAAAAAA8AAAEFAAAAAAwAAAASAAAAEQAAABMAAAAAEgAAABIAAAASAAAAFP####8AAAABAAtDSG9tb3RoZXRpZQAAAAAQAAAAFQAAABIAAAAO#####wAAAAEAC0NQb2ludEltYWdlAAAAABAAAAAAAA8AAAEFAAAAABYAAAAYAAAAFAAAAAAQAAAAFQAAABIAAAAPAAAAFQAAAAAQAAAAAAAPAAABBQAAAAAXAAAAGv####8AAAABAAhDU2VnbWVudAAAAAAQAQAAAAANAAABAAEAAAAWAAAAGQAAABYAAAAAEAEAAAAADQAAAQABAAAAFwAAABsAAAAHAAAAABABAAAAAAsAAVcAwBQAAAAAAADANAAAAAAAAAUAAT#cVniavN8OAAAAHP####8AAAACAAhDTWVzdXJlWAAAAAAQAAd4Q29vcmQxAAAADAAAAB4AAAADAAAAABAABWFic3cxAAd4Q29vcmQxAAAAEgAAAB######AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAABABZmZmAAAAHgAAABIAAAAOAAAAHgAAAAIAAAAeAAAAHgAAAAMAAAAAEAAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAATAQAAABMCAAAAAUAAAAAAAAAAAAAAEgAAABEAAAASAAAAIAAAABEAAAAAEAEAAAAADwAAAQUAAAAADAAAABIAAAAiAAAAEgAAABIAAAAYAQAAABABZmZmAAAAIwAAABIAAAAOAAAAHgAAAAUAAAAeAAAAHwAAACAAAAAiAAAAIwAAAAcAAAAAEAEAAAAACwABUgBAIAAAAAAAAMAgAAAAAAAABQABP9EbToG06B8AAAAd#####wAAAAIACENNZXN1cmVZAAAAABAAB3lDb29yZDEAAAAMAAAAJQAAAAMAAAAAEAAFb3JkcjEAB3lDb29yZDEAAAASAAAAJgAAABgBAAAAEAFmZmYAAAAlAAAAEgAAAA8AAAAlAAAAAgAAACUAAAAlAAAAAwAAAAAQAAVvcmRyMgANMipvcmRvci1vcmRyMQAAABMBAAAAEwIAAAABQAAAAAAAAAAAAAASAAAAEgAAABIAAAAnAAAAEQAAAAAQAQAAAAAPAAABBQAAAAAMAAAAEgAAABEAAAASAAAAKQAAABgBAAAAEAFmZmYAAAAqAAAAEgAAAA8AAAAlAAAABQAAACUAAAAmAAAAJwAAACkAAAAq#####wAAAAIADENDb21tZW50YWlyZQAAAAAQAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAHgsAAf###wAAAAEAAAAAAAsjVmFsKGFic3cxKQAAABgBAAAAEAFmZmYAAAAsAAAAEgAAAA4AAAAeAAAABAAAAB4AAAAfAAAAIAAAACwAAAAaAAAAABABZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAjCwAB####AAAAAQAAAAAACyNWYWwoYWJzdzIpAAAAGAEAAAAQAWZmZgAAAC4AAAASAAAADgAAAB4AAAAGAAAAHgAAAB8AAAAgAAAAIgAAACMAAAAuAAAAGgAAAAAQAWZmZgDAIAAAAAAAAD#wAAAAAAAAAAAAJQsAAf###wAAAAIAAAABAAsjVmFsKG9yZHIxKQAAABgBAAAAEAFmZmYAAAAwAAAAEgAAAA8AAAAlAAAABAAAACUAAAAmAAAAJwAAADAAAAAaAAAAABABZmZmAMAcAAAAAAAAAAAAAAAAAAAAAAAqCwAB####AAAAAgAAAAEACyNWYWwob3JkcjIpAAAAGAEAAAAQAWZmZgAAADIAAAASAAAADwAAACUAAAAGAAAAJQAAACYAAAAnAAAAKQAAACoAAAAy#####wAAAAEABUNGb25jAP####8AAWYALzEvKHNpZ21hKnNxcnQoMipwaSkpKmV4cCgtMC41KigoeC1tdSkvc2lnbWEpXjIpAAAAEwIAAAATAwAAAAE#8AAAAAAAAAAAABMCAAAAEgAAAAH#####AAAAAgAJQ0ZvbmN0aW9uAQAAABMCAAAAAUAAAAAAAAAAAAAAEgAAAAAAAAAcBwAAAAQAAAATAgAAAAE#4AAAAAAAAP####8AAAABAApDUHVpc3NhbmNlAAAAEwMAAAATAf####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAASAAAAAgAAABIAAAABAAAAAUAAAAAAAAAAAAF4AAAABwD#####AQAAAAAPAAJ4MQEFAAG#84WEI12cTwAAAAgAAAAXAP####8AB3hDb29yZDEAAAAMAAAANQAAAAMA#####wACeDEAB3hDb29yZDEAAAASAAAANgAAAAMA#####wACeTEABWYoeDEp#####wAAAAEADkNBcHBlbEZvbmN0aW9uAAAANAAAABIAAAA3AAAAEQD#####AQAAAAAPAAABBQAAAAAMAAAAEgAAADcAAAASAAAAOP####8AAAACAA1DTGlldURlUG9pbnRzAP####8AAAAAAAEAAAA5AAAH0AABAAAANQAAAAUAAAA1AAAANgAAADcAAAA4AAAAOQAAAAMA#####wABYQAELTAuMgAAAAQAAAABP8mZmZmZmZoAAAADAP####8AAWIAAzEuNQAAAAE#+AAAAAAAAAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAOwAAAAEAAAAAAAAAAAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAOwAAAB8AAAA0AAAAEgAAADsAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEgAAADwAAAABAAAAAAAAAAAAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEgAAADwAAAAfAAAANAAAABIAAAA8AAAAFgD#####AQAAAAANAAABAAEAAAA9AAAAPwAAAAcA#####wEAAAAADwACeDIBBQABP9EvGjNvbeQAAABBAAAAFwD#####AAd4Q29vcmQyAAAADAAAAEIAAAADAP####8AAngyAAd4Q29vcmQyAAAAEgAAAEMAAAADAP####8AAnkyAAVmKHgyKQAAAB8AAAA0AAAAEgAAAEQAAAARAP####8BAAAAAA8AAAEFAAAAAAwAAAASAAAARAAAABIAAABFAAAAIAD#####AAAAAAABAAAARgAAAfQAAQAAAEIAAAAFAAAAQgAAAEMAAABEAAAARQAAAEb#####AAAAAQASQ1N1cmZhY2VMaWV1RHJvaXRlAP####8AAAD#AAAABQAAAEcAAAAI#####wAAAAIADENEcm9pdGVQYXJFcQD#####AQAAAAAPAAABAAEAAAAMAAAAEwgAAAAeAAAAAAAAABIAAAACAAAAEwEAAAAeAAAAAAAAABIAAAACAAAAAwD#####AAJhMQABMAAAAAEAAAAAAAAAAAAAAAMA#####wACYTIAATEAAAABP#AAAAAAAAAAAAAWAP####8AAAD#AA0AAAEAAQAAAD4AAAA9AAAAFgD#####AAAA#wANAAABAAEAAABAAAAAP#####8AAAABAApDSW50ZWdyYWxlAP####8AAUkAAAA0AAAAEgAAADsAAAASAAAAPAAAAAFAWQAAAAAAAAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAAQAAAAAAAAAAAAAAAT#czMzMzMzNAAAAFgD#####AAAAAAANAAABAAEAAABQAAAATwAAABoA#####wAAAAAAv#AAAAAAAABACAAAAAAAAAAAAE8SAAAAAAABAAAAAAABMAAAAAMA#####wAGYWJzUHJiAAMwLjUAAAABP+AAAAAAAAAAAAADAP####8ABm9yZFByYgALZihhYnNQcmIpLzMAAAATAwAAAB8AAAA0AAAAEgAAAFMAAAABQAgAAAAAAAAAAAARAP####8BAAAAAA8AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEgAAAFMAAAASAAAAVAAAABEA#####wEAAAAADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAASAAAAUwAAAAE#4AAAAAAAAAAAAAMA#####wABcAADMC4yAAAAAT#JmZmZmZma#####wAAAAEACENWZWN0ZXVyAP####8AAAAAAA0AAAEAAQAAAFYAAABVAwAAABoA#####wAAAP8BAAAAVhIAAAAAAAEAAAACAA5BaXJlPSNWYWwocCw1KQAAABoA#####wAAAP8Av#AAAAAAAAA#8AAAAAAAAAAAAD0SAAAAAAABAAAAAAAKI1ZhbChhMSw1KQAAABoA#####wAAAP8BAAAAPxIAAAAAAAEAAAAAAAojVmFsKGEyLDUpAAAADf##########'
      stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
      // stor.mtgAppLecteur.calculateAndDisplayAll(true)
      callback() // cette fonction ne peut être appelée qu’une fois la figure chargée. Elle permet de l’adapter au données de l’exo
    } else {
      // ici c’est la figure qui ne sert qu'à faire des calculs, elle n’est utile qu’au début de la question
      // je me sers du même fichier que pour loinormaleCalculs
      txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAAA8+TMzNAANmcmH###8BAP8BAAAAAAAAAAADSAAAAhwAAAAAAAAAAAAAAAAAAAAM#####wAAAAEAEW9iamV0cy5DQ2FsY0NvbnN0AP####8AAnBpABYzLjE0MTU5MjY1MzU4OTc5MzIzODQ2#####wAAAAEAEW9iamV0cy5DQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAOb2JqZXRzLkNDYWxjdWwA#####wACbXUAATAAAAABAAAAAAAAAAAAAAACAP####8ABXNpZ21hAAExAAAAAT#wAAAAAAAA#####wAAAAEADG9iamV0cy5DRm9uYwD#####AAFmAC8xLyhzaWdtYSpzcXJ0KDIqcGkpKSpleHAoLSh4LW11KV4yLygyKnNpZ21hXjIpKf####8AAAABABFvYmpldHMuQ09wZXJhdGlvbgIAAAAEAwAAAAE#8AAAAAAAAAAAAAQC#####wAAAAEAFm9iamV0cy5DUmVzdWx0YXRWYWxldXIAAAAC#####wAAAAIAEG9iamV0cy5DRm9uY3Rpb24BAAAABAIAAAABQAAAAAAAAAAAAAAFAAAAAAAAAAYH#####wAAAAEAE29iamV0cy5DTW9pbnNVbmFpcmUAAAAEA#####8AAAABABFvYmpldHMuQ1B1aXNzYW5jZQAAAAQB#####wAAAAIAGG9iamV0cy5DVmFyaWFibGVGb3JtZWxsZQAAAAAAAAAFAAAAAQAAAAFAAAAAAAAAAAAAAAQCAAAAAUAAAAAAAAAAAAAACAAAAAUAAAACAAAAAUAAAAAAAAAAAAF4AAAAAgD#####AAFhAAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAFiAAIxMAAAAAFAJAAAAAAAAP####8AAAABABFvYmpldHMuQ0ludGVncmFsZQD#####AAVwcm9iYQAAAAMAAAAFAAAABAAAAAUAAAAFAAAAAUA0AAAAAAAA#####wAAAAIADW9iamV0cy5DTGF0ZXgA#####wAAAAAADwAAAf####8QQEAAAAAAAABANAAAAAAAAAAAAAAAAAAAAAAAblxmcmFjezF9e1xzaWdtYVxzcXJ0ezJccGl9fVxpbnRfe1xWYWx7YX19XntcVmFse2J9fVx0ZXh0e2V9XnstXGZyYWN7KHgtXG11KV4yfXtcc2lnbWFeMn19XHRleHR7ZH14PVxWYWx7cHJvYmF9AAAACwD#####AAAAAAAPAAAB#####xBAQQAAAAAAAEBZwAAAAAAAAAAAAAAAAAAAAAAMXG11PVxWYWx7bXV9AAAACwD#####AAAAAAAPAAAB#####xBAQIAAAAAAAEBgQAAAAAAAAAAAAAAAAAAAAAASXHNpZ21hPVxWYWx7c2lnbWF9AAAACwD#####AAAAAAAPAAAB#####xBAQQAAAAAAAEBmwAAAAAAAAAAAAAAAAAAAAAAJYT1cVmFse2F9AAAACwD#####AAAAAAAPAAAB#####xBAQIAAAAAAAEBpgAAAAAAAAAAAAAAAAAAAAAAJYj1cVmFse2J9################'
      stor.liste = stor.mtgAppLecteur.createList(txtFigure)
    }
  }
  function getDonnees () {
    return {

      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      tabCasFigure: [''],
      intervalleGauche: false,

      textes: {
        titre_exo: 'Déterminer l’écart-type d’une loi normale',
        consigne1: 'Donner une valeur approchée du résultat à $10^{-£a}$ près.',
        consigne2: 'Donner une valeur approchée du résultat à l’unité près.',
        comment1: 'Peut-être est-ce un problème de valeur approchée ?',
        comment2: 'Peut-être est-ce un problème de valeur approchée, notamment dans le calcul intermédiaire ?',
        corr1: ''
      },
      pe: 0
    }
  }

  // console.log("debut du code : ",me.etat)
  function suite () {
    /// /////////////////////////////////////// Pour MathGraph32
    // ce qui suit lance la création initiale de la figure
    depart(false)
    /// //////////////////////////////////////Fin de l’initialisation de la figure MathGraph32'
    if ((ds.nbetapes === 1) || ((ds.nbetapes === 2) && (me.questionCourante % 2 === 1))) {
      // Dans cette section l’utilisateur peut imposer les consignes
      let choixExoImpose = ((ds.tabCasFigure[me.questionCourante - 1] !== undefined) && (ds.tabCasFigure[me.questionCourante - 1] !== ''))
      if (choixExoImpose) {
        // je regarde tout de même si le numéro existe bien
        if (Number(ds.tabCasFigure[me.questionCourante - 1]) > stor.nb_sujets) {
          choixExoImpose = false
        }
      }
      if (choixExoImpose) {
        // c’est que j’ai imposé l’exo à traiter
        stor.numeroExo = Number(ds.tabCasFigure[me.questionCourante - 1]) - 1
      } else {
        // je choisis au hasard un des exos à traiter (parmi ceux qui ne sont pas déjà choisis)
        let nbTentatives = 0 // c’est au cas où on aurait déjà proposé tous les cas de figure (vu le nombre d’exos possible qu’il risque d’y avoir, c’est peu probable)
        do {
          nbTentatives++
          stor.numeroExo = j3pGetRandomInt(0, (stor.nb_sujets - 1))
        } while ((ds.tabCasFigure.indexOf(stor.numeroExo + 1) > -1) && (nbTentatives < 30))
        ds.tabCasFigure[me.questionCourante - 1] = stor.numeroExo + 1
      }
      // stor.numeroExo = stor.nb_sujets-1
      // Pour l’aléatoire, les valeurs de l’énoncé doivent générées ici
      stor.variables['enonce' + stor.numeroExo] = []
      for (let i = 0; i < stor.nomVariables['enonce' + stor.numeroExo].length; i++) {
        const [intMin, intMax] = j3pGetBornesIntervalle(stor.mesvariables['enonce' + stor.numeroExo][i][1])
        if (stor.mesvariables['enonce' + stor.numeroExo][i].length === 2) {
          stor.variables['enonce' + stor.numeroExo][i] = j3pGetRandomInt(intMin, intMax)
        } else {
          stor.variables['enonce' + stor.numeroExo][i] = j3pGetRandomInt(intMin, intMax) / Number(stor.mesvariables['enonce' + stor.numeroExo][i][2])
        }
      }
    }
    stor.obj = {} // il servira pour la consigne
    stor.obj2 = {} // il servira pour la question
    for (let i = 0; i < stor.nomVariables['enonce' + stor.numeroExo].length; i++) {
      stor.obj[stor.nomVariables['enonce' + stor.numeroExo][i]] = stor.variables['enonce' + stor.numeroExo][i]
      stor.obj2[stor.nomVariables['enonce' + stor.numeroExo][i]] = stor.variables['enonce' + stor.numeroExo][i]
    }
    // on donne à mtg32 les données de l’énoncé pour calculer la proba
    const mu = remplaceCalcul({
      text: stor.valeurs['enonce' + stor.numeroExo].mu,
      obj: stor.obj
    })
    const sigma = remplaceCalcul({
      text: stor.valeurs['enonce' + stor.numeroExo].sigma,
      obj: stor.obj
    })
    const a = remplaceCalcul({
      text: stor.valeurs['enonce' + stor.numeroExo].borneInf,
      obj: stor.obj
    })
    const b = remplaceCalcul({
      text: stor.valeurs['enonce' + stor.numeroExo].borneSup,
      obj: stor.obj
    })
    const u = remplaceCalcul({
      text: stor.valeurs['enonce' + stor.numeroExo].u,
      obj: stor.obj
    })
    stor.liste.giveFormula2('mu', String(mu))
    stor.liste.giveFormula2('sigma', String(sigma))
    stor.liste.giveFormula2('a', String(a))
    stor.liste.giveFormula2('b', String(b))
    // j’actualise alors la figure avec ces nouvelles données :
    stor.liste.calculateNG()
    stor.obj.p = j3pArrondi(stor.liste.valueOf('proba'), stor.arrondiProba['enonce' + stor.numeroExo])
    stor.obj.u = j3pArrondi(u, stor.arrondiU['enonce' + stor.numeroExo])
    try {
      stor.obj.q = remplaceCalcul({
        text: stor.valeurs['enonce' + stor.numeroExo].q,
        obj: stor.obj
      })
    } catch (e) {}
    me.logIfDebug('la proba (aire cad p) : ', stor.obj.p, '   et u:', stor.obj.u)
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= stor.consignes['enonce' + stor.numeroExo].length; i++) {
      const zoneCons = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(zoneCons, '', stor.consignes['enonce' + stor.numeroExo][i - 1], stor.obj)
    }
    let elt
    if (ds.nbetapes === 2) {
      if (me.questionCourante % 2 === 1) {
        if (ds.intervalleGauche) {
          for (let i = 1; i <= stor.questComplementaireTexas['enonce' + stor.numeroExo].length; i++) {
            const zoneCons = j3pAddElt(stor.conteneur, 'div')
            j3pAffiche(zoneCons, '', stor.questComplementaireTexas['enonce' + stor.numeroExo][i - 1], stor.obj)
          }
        } else {
          for (let i = 1; i <= stor.questComplementaire['enonce' + stor.numeroExo].length; i++) {
            const zoneCons = j3pAddElt(stor.conteneur, 'div')
            j3pAffiche(zoneCons, '', stor.questComplementaire['enonce' + stor.numeroExo][i - 1], stor.obj)
          }
        }
        const zoneCons = j3pAddElt(stor.conteneur, 'div')
        elt = j3pAffiche(zoneCons, '', '$u\\approx$&1&.', { inputmq1: { texte: '' } })
      } else {
        if (ds.intervalleGauche) {
          for (let i = 1; i <= stor.infoComplementaireTexas['enonce' + stor.numeroExo].length; i++) {
            const zoneCons = j3pAddElt(stor.conteneur, 'div')
            j3pAffiche(zoneCons, '', stor.infoComplementaireTexas['enonce' + stor.numeroExo][i - 1], stor.obj)
          }
        } else {
          for (let i = 1; i <= stor.infoComplementaire['enonce' + stor.numeroExo].length; i++) {
            const zoneCons = j3pAddElt(stor.conteneur, 'div')
            j3pAffiche(zoneCons, '', stor.infoComplementaire['enonce' + stor.numeroExo][i - 1], stor.obj)
          }
        }
        const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
        j3pAffiche(zoneCons1, '', stor.questionComp['enonce' + stor.numeroExo], stor.obj)
        const zoneCons = j3pAddElt(stor.conteneur, 'div')
        elt = j3pAffiche(zoneCons, '', '$\\sigma\\approx$&1&.', { inputmq1: { texte: '' } })
      }
    } else {
      const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(zoneCons1, '', stor.question['enonce' + stor.numeroExo], stor.obj)
      const zoneCons = j3pAddElt(stor.conteneur, 'div')
      elt = j3pAffiche(zoneCons, '', '$\\sigma\\approx$&1&.', { inputmq1: { texte: '' } })
    }
    stor.zoneInput = elt.inputmqList[0]
    me.logIfDebug('mu:', stor.liste.valueOf('mu'), '   sigma:', stor.liste.valueOf('sigma'), '   borneInf:', stor.liste.valueOf('a'), '   borneSup:', stor.liste.valueOf('b'), '     proba:', stor.liste.valueOf('proba'))
    // et je récupère la réponse qui est la variation proba de mtg32
    if ((ds.nbetapes === 2) && (me.questionCourante % 2 === 1)) {
      // ici c’est pour la valeur de u
      stor.zoneInput.typeReponse = ['nombre', 'approche', Math.pow(10, -stor.arrondiU['enonce' + stor.numeroExo])]
    } else {
      stor.zoneInput.typeReponse = ['nombre', 'approche', Math.pow(10, -stor.arrondi['enonce' + stor.numeroExo])]
    }
    const divAjout = j3pAddElt(stor.conteneur, 'div')
    if ((ds.nbetapes === 2) && (me.questionCourante % 2 === 1)) {
      stor.zoneInput.reponse = [remplaceCalcul({
        text: stor.valeurs['enonce' + stor.numeroExo].u,
        obj: stor.obj
      })]
      if (Number(stor.arrondiU['enonce' + stor.numeroExo]) === 0) {
        j3pAffiche(divAjout, '', ds.textes.consigne2)
      } else {
        j3pAffiche(divAjout, '', ds.textes.consigne1, { a: stor.arrondiU['enonce' + stor.numeroExo] })
      }
    } else {
      stor.zoneInput.reponse = [stor.liste.valueOf('sigma')]
      if (Number(stor.arrondi['enonce' + stor.numeroExo]) === 0) {
        j3pAffiche(divAjout, '', ds.textes.consigne2)
      } else {
        j3pAffiche(divAjout, '', ds.textes.consigne1, { a: stor.arrondi['enonce' + stor.numeroExo] })
      }
    }

    me.logIfDebug('reponse attendue : ', stor.zoneInput.reponse)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '5px' }) })
    mqRestriction(stor.zoneInput, '\\d,.-')
    j3pFocus(stor.zoneInput)

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        // Construction de la page
        me.construitStructurePage({ structure: ds.structure })
        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo)
        ds.nbetapes = (Number(ds.nbetapes) === 2) ? 2 : 1
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        j3pImporteAnnexe('Consignesexos/LoiNormaleSigma.js').then(function parseAnnexe (datasSection) {
          let j, i
          stor.nb_sujets = datasSection.sujets.length
          stor.consignes = {}
          stor.nomVariables = {}
          stor.variables = {}// ce sont les valeurs des variables qui seront aléatoires d’une étape à l’autre (si jamais ou souhaite mettre 2 fois le^même exo mais avec des valeurs différentes)
          stor.mesvariables = {}
          stor.questComplementaire = {}
          stor.questComplementaireTexas = {}
          stor.infoComplementaire = {}
          stor.infoComplementaireTexas = {}
          stor.question = {}
          stor.questionComp = {}
          stor.valeurs = {}
          stor.bornesGraph = {}
          stor.explicationsComp = {}
          stor.explicationsComp2 = {}
          stor.explicationsCompTexas = {}
          stor.explicationsCompTexas2 = {}
          stor.explications = {}
          stor.explicationsTexas = {}
          stor.explications2 = {}
          stor.explicationsTexas2 = {}
          stor.conclusion = {}
          stor.conclusionTexas = {}
          stor.arrondi = {}
          stor.arrondiU = {}
          stor.arrondiProba = {}
          for (j = 0; j < stor.nb_sujets; j++) {
            stor.consignes['enonce' + j] = datasSection.sujets[j].consignes
            stor.nomVariables['enonce' + j] = []
            stor.mesvariables['enonce' + j] = []
            for (i = 0; i < datasSection.sujets[j].variables.length; i++) {
              stor.nomVariables['enonce' + j][i] = datasSection.sujets[j].variables[i][0]
              stor.mesvariables['enonce' + j][i] = datasSection.sujets[j].variables[i]// (datasSection.sujets[j].variables[i].length==2)?j3pRandom(datasSection.sujets[j].variables[i][1]):j3pRandom(datasSection.sujets[j].variables[i][1])/Number(datasSection.sujets[j].variables[i][2])
            }
            stor.questComplementaire['enonce' + j] = datasSection.sujets[j].questComplementaire
            stor.infoComplementaire['enonce' + j] = datasSection.sujets[j].infoComplementaire
            stor.questComplementaireTexas['enonce' + j] = datasSection.sujets[j].questComplementaireTexas
            stor.infoComplementaireTexas['enonce' + j] = datasSection.sujets[j].infoComplementaireTexas
            stor.questionComp['enonce' + j] = datasSection.sujets[j].questionComp
            stor.question['enonce' + j] = datasSection.sujets[j].question
            stor.explicationsComp['enonce' + j] = datasSection.sujets[j].explicationsComp
            stor.explicationsCompTexas['enonce' + j] = datasSection.sujets[j].explicationsCompTexas
            stor.explications['enonce' + j] = datasSection.sujets[j].explications
            stor.explicationsTexas['enonce' + j] = datasSection.sujets[j].explicationsTexas
            stor.explications2['enonce' + j] = datasSection.sujets[j].explications2
            stor.explicationsTexas2['enonce' + j] = datasSection.sujets[j].explicationsTexas2
            stor.explicationsComp2['enonce' + j] = datasSection.sujets[j].explicationsComp2
            stor.explicationsCompTexas2['enonce' + j] = datasSection.sujets[j].explicationsCompTexas2
            stor.conclusion['enonce' + j] = datasSection.sujets[j].conclusion
            stor.conclusionTexas['enonce' + j] = datasSection.sujets[j].conclusionTexas
            stor.arrondi['enonce' + j] = datasSection.sujets[j].arrondi
            stor.arrondiU['enonce' + j] = datasSection.sujets[j].arrondiU
            stor.arrondiProba['enonce' + j] = datasSection.sujets[j].arrondiProba
            stor.valeurs['enonce' + j] = datasSection.sujets[j].valeurs
            stor.bornesGraph['enonce' + j] = datasSection.sujets[j].bornesGraph
          }
          // chargement mtg
          getMtgCore({ withMathJax: true })
            .then(
              // success
              (mtgAppLecteur) => {
                stor.mtgAppLecteur = mtgAppLecteur
                suite()
              },
              // failure
              (error) => {
                j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
              })
            // plantage dans le code de success
            .catch(j3pShowError)
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        // comme pour l’utilisation de Validation_zones, je déclare l’objet reponse qui contient deux propriétés : aRepondu et bonneReponse
        let reponse = { aRepondu: false, bonneReponse: false }
        reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(true)// affichage de la correction
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
             *   RECOPIER LA CORRECTION ICI !
             */
              afficheCorrection(false)// affichage de la correction
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // on peut affiner le commentaire si on veut
              if ((ds.nbetapes === 2) && (me.questionCourante % 2 === 1)) {
                if (!isNaN(j3pNombre(fctsValid.zones.reponseSaisie[0]))) {
                  if (Math.abs(j3pNombre(fctsValid.zones.reponseSaisie[0]) - stor.zoneInput.reponse[0]) < Math.pow(10, -stor.arrondiU['enonce' + stor.numeroExo] + 1)) {
                  // c’est que l’élève a fait le bon calcul mais qu’il a fait une erreur dans la valeur approchée
                    stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
                  }
                }
              } else {
                if (!isNaN(j3pNombre(fctsValid.zones.reponseSaisie[0]))) {
                  if (Math.abs(j3pNombre(fctsValid.zones.reponseSaisie[0]) - stor.zoneInput.reponse[0]) < Math.pow(10, -stor.arrondi['enonce' + stor.numeroExo] + 1)) {
                  // c’est que l’élève a fait le bon calcul mais qu’il a donné une mauvaise valeur approchée
                    stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
                  }
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorrection(false)// affichage de la correction
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
