import { j3pAddElt, j3pFocus, j3pGetRandomInt, j3pNombre, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { tableauDonneesPrbs } from 'src/legacy/outils/tableauxProba/tabLoiProba'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juillet 2019
        Compléter une loi de proba dans un cas de non équiprobabilité

 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const explications = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(explications, 'div')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, {
      s: stor.objetRep.leCalcul[0],
      r: j3pVirgule(stor.zoneInput[0].reponse[0])
    })
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2, {
      s: stor.objetRep.leCalcul[1],
      r: j3pVirgule(stor.zoneInput[1].reponse[0])
    })
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Probabilités dans un cas de non équiprobabilité',
        // on donne les phrases de la consigne
        consigne1_1: 'Un restaurant propose 4 menus à des tarifs différents.',
        consigne2_1: 'Pour chacun de ces tarifs, le gérant a relevé dans le tableau incomplet ci-dessous la probabilité qu’un client choisisse le menu en question.',
        legende1: 'Tarif du menu (en €)',
        consigne3_1: 'La probabilité qu’un client choisi au hasard prenne le menu à £e&nbsp;€ vaut &1&.',
        consigne4_11: 'La probabilité qu’il choisisse à menu coûtant au moins £f&nbsp;€ vaut &1&.',
        consigne4_12: 'La probabilité qu’il choisisse à menu coûtant £f&nbsp;€ ou moins vaut &1&.',
        consigne1_2: 'Les organisateurs d’un trail proposent 5 courses différentes.',
        consigne2_2: 'Pour chacune des distances proposées, ils ont relevé dans le tableau incomplet ci-dessous la probabilité qu’un coureur choisisse la course en question.',
        legende2: 'distance (en km)',
        consigne3_2: 'La probabilité qu’un coureur choisi au hasard se soit inscrit à la course de £e&nbsp;km vaut &1&.',
        consigne4_21: 'La probabilité qu’il choisisse une course d’au moins £f&nbsp;km vaut &1&.',
        consigne4_22: 'La probabilité qu’il choisisse une course de £f&nbsp;km ou moins vaut &1&.',
        consigne1_3: 'Un magasin propose des ordinateurs classés suivant 5 gammes de prix.',
        consigne2_3: 'Le gérant de ce magasin a relevé dans le tableau incomplet ci-dessous la probabilité qu’un acheteur choisisse tel ou tel ordinateur selon son prix.',
        legende3: 'prix (en €)',
        consigne3_3: 'La probabilité qu’un acheteur choisi au hasard reparte avec un ordinateur à £e&nbsp;€ vaut &1&.',
        consigne4_31: 'La probabilité qu’il choisisse un ordinateur coûtant au moins £f&nbsp;€ vaut &1&.',
        consigne4_32: 'La probabilité qu’il choisisse un ordinateur coûtant £f&nbsp;€ ou moins vaut &1&.',
        consigne1_4: 'Une pizzeria propose 4 tailles différentes (diamètres) pour ses pizzas.',
        consigne2_4: 'Le gérant a relevé dans le tableau incomplet ci-dessous la probabilité qu’un client choisisse telle ou telle taille.',
        legende4: 'diamètre (en cm)',
        consigne3_4: 'La probabilité qu’un acheteur choisi au hasard reparte avec une pizza de diamètre £e&nbsp;cm vaut &1&.',
        consigne4_41: 'La probabilité que le diamètre de la pizza choisie soit d’au moins £f&nbsp;cm vaut &1&.',
        consigne4_42: 'La probabilité que le diamètre de la pizza choisie soit de £f&nbsp;cm ou moins vaut &1&.',
        consigne1_5: 'On dispose d’un dé déséquilibré à six faces (numérotées de 1 à 6).',
        consigne2_5: 'Après de très nombreux lancers, on estime dans le tableau incomplet ci-dessous les probabilités de chacune des faces.',
        legende5: 'face',
        consigne3_5: 'En lançant ce dé, la probabilité de tomber sur la face numéro £e vaut &1&.',
        consigne4_51: 'La probabilité que le numéro de la face soit d’au moins £f vaut &1&.',
        consigne4_52: 'La probabilité que le numéro de la face soit £f ou moins vaut &1&.',
        legendePrb: 'probabilité',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Une probabilité est un nombre compris entre 0 et 1&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'Le tableau ci-dessus étant une loi de probabilité, la somme de toutes les probabilités vaut 1. La probabilité manquante vaut ainsi&nbsp;:<br/>$£s=£r$.',
        corr2: 'La réponse à la deuxième question vaut par ailleurs&nbsp;:<br/>$£s=£r$'
      },
      pe: 0
    }
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.largeurTab = me.zonesElts.MG.getBoundingClientRect().width * stor.pourc / 100
    const prb = []
    const choixQuest = j3pGetRandomInt(1, 2)
    const // Pour un même sujet, on peut avoir plusieurs choix de questions
      laReponse = []
    const leCalcul = []
    let numCaseVide
    let numCaseSep// numCaseSep est la case à partir de laquelle on regarde au-dessus ou en-dessous
    stor.probabilites = []
    stor.textesDonnees = []
    switch (quest) {
      case 1:
        do {
          prb[0] = j3pGetRandomInt(35, 45)
          prb[1] = j3pGetRandomInt(20, 25)
          prb[2] = j3pGetRandomInt(20, 25)
        } while (prb[0] + prb[1] + prb[2] > 93)
        prb[3] = 100 - prb[0] - prb[1] - prb[2]
        stor.textesDonnees = ['15,50', '24,50', '29,50', '37,50']
        break
      case 2:
        do {
          prb[0] = j3pGetRandomInt(30, 40)
          prb[1] = j3pGetRandomInt(15, 20)
          prb[2] = j3pGetRandomInt(15, 20)
          prb[3] = j3pGetRandomInt(10, 20)
        } while (prb[0] + prb[1] + prb[2] + prb[3] > 93)
        prb[4] = 100 - prb[0] - prb[1] - prb[2] - prb[3]
        stor.textesDonnees = [10 + j3pGetRandomInt(0, 2), 17 + j3pGetRandomInt(0, 2), 25 + j3pGetRandomInt(0, 4), 40 + j3pGetRandomInt(0, 4), 60 + j3pGetRandomInt(0, 4)]
        break
      case 3:
        do {
          prb[0] = j3pGetRandomInt(25, 30)
          prb[1] = j3pGetRandomInt(25, 30)
          prb[2] = j3pGetRandomInt(15, 20)
          prb[3] = j3pGetRandomInt(15, 20)
        } while (prb[0] + prb[1] + prb[2] + prb[3] > 93)
        prb[4] = 100 - prb[0] - prb[1] - prb[2] - prb[3]
        stor.textesDonnees = [349 + j3pGetRandomInt(0, 1) * 50, 449 + j3pGetRandomInt(0, 1) * 50, 549 + j3pGetRandomInt(0, 1) * 50, 699 + j3pGetRandomInt(0, 1) * 50, 849 + j3pGetRandomInt(0, 1) * 50]
        break
      case 4:
        do {
          prb[0] = j3pGetRandomInt(15, 20)
          prb[1] = j3pGetRandomInt(30, 40)
          prb[2] = j3pGetRandomInt(30, 40)
        } while (prb[0] + prb[1] + prb[2] > 93)
        prb[3] = 100 - prb[0] - prb[1] - prb[2]
        stor.textesDonnees = [20 + j3pGetRandomInt(0, 2), 26 + j3pGetRandomInt(0, 2), 32 + j3pGetRandomInt(0, 2), 38 + j3pGetRandomInt(0, 1) * 2]
        break
      case 5:
        do {
          prb[0] = j3pGetRandomInt(25, 30)
          prb[1] = j3pGetRandomInt(10, 15)
          prb[2] = j3pGetRandomInt(15, 20)
          prb[3] = j3pGetRandomInt(10, 15)
          prb[4] = j3pGetRandomInt(10, 15)
        } while (prb[0] + prb[1] + prb[2] + prb[3] + prb[4] > 93)
        prb[5] = 100 - prb[0] - prb[1] - prb[2] - prb[3] - prb[4]
        stor.textesDonnees = [1, 2, 3, 4, 5, 6]
        break
    }
    let numRestants
    if (prb.length === 4) {
      numCaseVide = j3pGetRandomInt(0, 3)
      laReponse[0] = prb[numCaseVide] / 100
      numRestants = [0, 1, 2, 3]
      numRestants.splice(numRestants.indexOf(numCaseVide), 1)
      leCalcul[0] = '1-(' + j3pVirgule(prb[numRestants[0]] / 100) + '+' + j3pVirgule(prb[numRestants[1]] / 100) + '+' + j3pVirgule(prb[numRestants[2]] / 100) + ')'
      numCaseSep = j3pGetRandomInt(1, 2)
      laReponse[1] = (choixQuest === 1) // menu coutant au moins...
        ? (numCaseSep === 1) ? (prb[1] + prb[2] + prb[3]) / 100 : (prb[2] + prb[3]) / 100
        : (numCaseSep === 1) ? (prb[0] + prb[1]) / 100 : (prb[0] + prb[1] + prb[2]) / 100
      leCalcul[1] = (choixQuest === 1)
        ? (numCaseSep === 1)
            ? j3pVirgule(prb[1] / 100) + '+' + j3pVirgule(prb[2] / 100) + '+' + j3pVirgule(prb[3] / 100)
            : j3pVirgule(prb[2] / 100) + '+' + j3pVirgule(prb[3] / 100)
        : (numCaseSep === 1)
            ? j3pVirgule(prb[0] / 100) + '+' + j3pVirgule(prb[1] / 100)
            : j3pVirgule(prb[0] / 100) + '+' + j3pVirgule(prb[1] / 100) + '+' + j3pVirgule(prb[2] / 100)
    } else if (prb.length === 5) {
      numCaseVide = j3pGetRandomInt(0, 4)
      laReponse[0] = prb[numCaseVide] / 100
      numRestants = [0, 1, 2, 3, 4]
      numRestants.splice(numRestants.indexOf(numCaseVide), 1)
      leCalcul[0] = '1-(' + j3pVirgule(prb[numRestants[0]] / 100) + '+' + j3pVirgule(prb[numRestants[1]] / 100) + '+' + j3pVirgule(prb[numRestants[2]] / 100) + '+' + j3pVirgule(prb[numRestants[3]] / 100) + ')'
      numCaseSep = j3pGetRandomInt(1, 3)
      laReponse[1] = (choixQuest === 1) // menu coutant au moins...
        ? (numCaseSep === 1) ? (prb[1] + prb[2] + prb[3] + prb[4]) / 100 : (numCaseSep === 2) ? (prb[2] + prb[3] + prb[4]) / 100 : (prb[3] + prb[4]) / 100
        : (numCaseSep === 1) ? (prb[0] + prb[1]) / 100 : (numCaseSep === 2) ? (prb[0] + prb[1] + prb[2]) / 100 : (prb[0] + prb[1] + prb[2] + prb[3]) / 100
      leCalcul[1] = (choixQuest === 1)
        ? (numCaseSep === 1)
            ? j3pVirgule(prb[1] / 100) + '+' + j3pVirgule(prb[2] / 100) + '+' + j3pVirgule(prb[3] / 100) + '+' + j3pVirgule(prb[4] / 100)
            : (numCaseSep === 2)
                ? j3pVirgule(prb[2] / 100) + '+' + j3pVirgule(prb[3] / 100) + '+' + j3pVirgule(prb[4] / 100)
                : j3pVirgule(prb[3] / 100) + '+' + j3pVirgule(prb[4] / 100)
        : (numCaseSep === 1)
            ? j3pVirgule(prb[0] / 100) + '+' + j3pVirgule(prb[1] / 100)
            : (numCaseSep === 2)
                ? j3pVirgule(prb[0] / 100) + '+' + j3pVirgule(prb[1] / 100) + '+' + j3pVirgule(prb[2] / 100)
                : j3pVirgule(prb[0] / 100) + '+' + j3pVirgule(prb[1] / 100) + '+' + j3pVirgule(prb[2] / 100) + '+' + j3pVirgule(prb[3] / 100)
    } else if (prb.length === 6) {
      numCaseVide = j3pGetRandomInt(0, 5)
      laReponse[0] = prb[numCaseVide] / 100
      numRestants = [0, 1, 2, 3, 4, 5]
      numRestants.splice(numRestants.indexOf(numCaseVide), 1)
      leCalcul[0] = '1-(' + j3pVirgule(prb[numRestants[0]] / 100) + '+' + j3pVirgule(prb[numRestants[1]] / 100) + '+' + j3pVirgule(prb[numRestants[2]] / 100) + '+' + j3pVirgule(prb[numRestants[3]] / 100) + '+' + j3pVirgule(prb[numRestants[4]] / 100) + ')'
      numCaseSep = j3pGetRandomInt(1, 3)
      laReponse[1] = (choixQuest === 1) // menu coutant au moins...
        ? (numCaseSep === 1) ? (prb[1] + prb[2] + prb[3] + prb[4] + prb[5]) / 100 : (numCaseSep === 2) ? (prb[2] + prb[3] + prb[4] + prb[5]) / 100 : (prb[3] + prb[4] + prb[5]) / 100
        : (numCaseSep === 1) ? (prb[0] + prb[1]) / 100 : (numCaseSep === 2) ? (prb[0] + prb[1] + prb[2]) / 100 : (prb[0] + prb[1] + prb[2] + prb[3]) / 100
      leCalcul[1] = (choixQuest === 1)
        ? (numCaseSep === 1)
            ? j3pVirgule(prb[1] / 100) + '+' + j3pVirgule(prb[2] / 100) + '+' + j3pVirgule(prb[3] / 100) + '+' + j3pVirgule(prb[4] / 100) + '+' + j3pVirgule(prb[5] / 100)
            : (numCaseSep === 2)
                ? j3pVirgule(prb[2] / 100) + '+' + j3pVirgule(prb[3] / 100) + '+' + j3pVirgule(prb[4] / 100) + '+' + j3pVirgule(prb[5] / 100)
                : j3pVirgule(prb[3] / 100) + '+' + j3pVirgule(prb[4] / 100) + '+' + j3pVirgule(prb[5] / 100)
        : (numCaseSep === 1)
            ? j3pVirgule(prb[0] / 100) + '+' + j3pVirgule(prb[1] / 100)
            : (numCaseSep === 2)
                ? j3pVirgule(prb[0] / 100) + '+' + j3pVirgule(prb[1] / 100) + '+' + j3pVirgule(prb[2] / 100)
                : j3pVirgule(prb[0] / 100) + '+' + j3pVirgule(prb[1] / 100) + '+' + j3pVirgule(prb[2] / 100) + '+' + j3pVirgule(prb[3] / 100)
    }
    me.logIfDebug('laReponse:', laReponse)
    for (let i = 0; i < prb.length; i++) {
      if (i === numCaseVide) stor.probabilites.push('?')
      else stor.probabilites.push(prb[i] / 100)
    }
    j3pAffiche(stor.zoneCons1, '', ds.textes['consigne1_' + quest])
    j3pAffiche(stor.zoneCons1, '', ds.textes['consigne2_' + quest])
    const mesDonnees = {
      parent: stor.zoneCons3,
      legendes: [ds.textes['legende' + quest], ds.textes.legendePrb],
      textes: stor.textesDonnees,
      probas: stor.probabilites,
      largeur: stor.largeurTab,
      typeFont: me.styles.petit.enonce
    }
    stor.maLoi = tableauDonneesPrbs(mesDonnees)
    const elt1 = j3pAffiche(stor.zoneCons4, '', ds.textes['consigne3_' + quest], {
      e: stor.textesDonnees[numCaseVide],
      inputmq1: { texte: '', dynamique: true }
    })
    const elt2 = j3pAffiche(stor.zoneCons5, '', ds.textes['consigne4_' + quest + choixQuest], {
      f: stor.textesDonnees[numCaseSep],
      inputmq1: { texte: '', dynamique: true }
    })
    stor.zoneInput = [elt1.inputmqList[0], elt2.inputmqList[0]]
    stor.zoneInput.forEach((zoneInput, index) => {
      zoneInput.reponse = [laReponse[index]]
      mqRestriction(zoneInput, '\\d,.')
      zoneInput.typeReponse = ['nombre', 'exact']
    })
    stor.objetRep = { leCalcul, choixQuest, rep: laReponse, quest }
    j3pFocus(stor.zoneInput[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = stor.zoneInput.map(eltInput => eltInput.id)
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70

        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc / 100 })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.tabQuest = [1, 2, 3, 4, 5]
        stor.tabQuestInit = [...stor.tabQuest]
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // On vérifie s’il a bien donné des valeurs entre 0 et 1
              let entre01 = true
              for (let i = 0; i <= 1; i++) {
                const repSaisie = j3pNombre(fctsValid.zones.reponseSaisie[i])
                if (repSaisie < 0 || repSaisie > 1) entre01 = false
              }
              if (!entre01) stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
