import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pChaine, j3pShuffle, j3pDetruit, j3pElement, j3pFocus, j3pGetRandomBool, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pVirgule, j3pGetNewId, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Deniaud Rémi
        avril 2021
        Application de la colinéarité avec des droites parallèles ou des points alignés
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuest', 'les deux', 'liste', 'On peut choisir le type de questions parmi des points alignés, des droites parallèles ou les deux', ['points alignés', 'droites parallèles', 'les deux']]
  ],
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'problème dans le calcul de coordonnées de vecteurs' },
    { pe_3: 'problème pour obtenir des vecteurs servant à justifier la parallélisme ou l’alignement' },
    { pe_4: 'problème pour le calcul du déterminant' },
    { pe_5: 'problème dans la détermination du parallélisme ou de l’alignement' },
    { pe_6: 'Insuffisant' }
  ]
  // dans le cas d’une section QUANTITATIVE
  /*  */
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Application de la colinéarité de vecteurs',
  // on donne les phrases de la consigne
  consigne1_1: 'Dans le plan muni d’un repère, on définit les points $£A(£{xA};£{yA})$, $£B(£{xB};£{yB})$ et $£C(£{xC};£{yC})$.',
  consigne1_2: 'Dans le plan muni d’un repère, on définit les points $£A(£{xA};£{yA})$, $£B(£{xB};£{yB})$, $£C(£{xC};£{yC})$ et $£D(£{xD};£{yD})$.',
  consigne2_1: 'On souhaite vérifier si ces points sont alignés en utilisant des vecteurs colinéaires.',
  consigne2_2: 'On souhaite vérifier si les droites $(£A£B)$ et $(£C£D)$ sont parallèles en utilisant des vecteurs colinéaires.',
  consigne3: 'Choisir deux vecteurs permettant de répondre au problème et calculer leurs coordonnées.',
  consigne4: '$\\vecteur{\\editable{}}\\matrixtwoone{\\editable{}}{\\editable{}}$ et $\\vecteur{\\editable{}}\\matrixtwoone{\\editable{}}{\\editable{}}$.',
  consigne4Rep: 'Tu as répondu : $\\vecteur{£{zone1}}\\matrixtwoone{£{zone2}}{£{zone3}}$ et $\\vecteur{£{zone4}}\\matrixtwoone{£{zone5}}{£{zone6}}$.',
  consigne5: 'On a obtenu dans la première question&nbsp;',
  consigne6: 'Le déterminant de ces deux vecteurs vaut&nbsp;:',
  consigne7: 'Que peut-on en conclure ?',
  consigne8_1: 'Les points $£A$, $£B$ et $£C$ sont alignés.',
  consigne8_2: 'Les points $£A$, $£B$ et $£C$ ne sont pas alignés.',
  consigne9_1: 'Les droites $(£A£B)$ et $(£C£D)$ sont parallèles.',
  consigne9_2: 'Les droites $(£A£B)$ et $(£C£D)$ ne sont pas parallèles.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1_2: 'Il reste £t tentatives.',
  comment1_1: 'Il reste une tentative.',
  comment2: 'Les vecteurs choisis ne permettent pas de conclure&nbsp;!',
  comment2Bis: 'Tout est en rouge, mais cela ne signifie pas que tout est faux&nbsp;!',
  comment3: 'Les vecteurs permettent de réponse au problème, mais il y a un souci dans le calcul des coordonnées&nbsp;!',
  comment4: 'Il faut sélectionner l’une des deux propositions&nbsp;!',
  comment5: 'On attend un nombre et pas un calcul&nbsp;!',
  comment6: 'Je ne comprends pas ce qui est écrit&nbsp;!',
  comment7: 'Le déterminant est faux, mais la deuxième partie de la réponse est cohérente&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'On choisit deux vecteurs construits avec les trois points, par exemple $\\vecteur{£u}$ et $\\vecteur{£v}$.',
  corr1_2: 'On choisit par exemple les vecteurs $\\vecteur{£u}$ et $\\vecteur{£v}$.',
  corr1_3: 'Avec les vecteurs $\\vecteur{£u}$ et $\\vecteur{£v}$, on obtient&nbsp;:',
  corr2: '$£{rep1}$ <br/>et $£{rep2}$.',
  corr3_1: 'Comme le déterminant est nul, on en déduit que les vecteurs $\\vecteur{£u}$ et $\\vecteur{£v}$ sont colinéaires.',
  corr3_2: 'Comme le déterminant est non nul, on en déduit que les vecteurs $\\vecteur{£u}$ et $\\vecteur{£v}$ ne sont pas colinéaires.',
  corr4_1: 'On conclut que les points $£A$, $£B$ et $£C$ sont alignés.',
  corr4_2: 'On conclut que les points $£A$, $£B$ et $£C$ ne sont pas alignés.',
  corr5_1: 'On conclut que les droites $(£A£B)$ et $(£C£D)$ sont parallèles.',
  corr5_2: 'On conclut que les droites $(£A£B)$ et $(£C£D)$ ne sont pas parallèles.'
}
const nbetapes = 2

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let i
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    // explications contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    for (i = 1; i <= 3; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div')
    if (me.questionCourante % nbetapes === 1) {
      const corrtxt1 = (stor.bonVecteurs)
        ? textes.corr1_3
        : (stor.quest) ? textes.corr1_1 : textes.corr1_2
      j3pAffiche(stor.zoneexplication1, '', corrtxt1, stor.objDonnees)
      j3pAffiche(stor.zoneexplication2, '', textes.corr2, stor.objDonnees)
      j3pDetruit(stor.zoneCons6)
    } else {
      j3pDetruit(stor.zoneCons7)
      j3pAffiche(stor.zoneexplication1, '', '$\\det\\left(\\vecteur{£u};\\vecteur{£v}\\right)=\\determinanttwo{£{absu}}{£{absv}}{£{ordu}}{£{ordv}}=£c=£r$.', stor.objDonnees)
      j3pAffiche(stor.zoneexplication2, '', (stor.objDonnees.colineaires) ? textes.corr3_1 : textes.corr3_2, stor.objDonnees)
      if (stor.quest) {
        j3pAffiche(stor.zoneexplication3, '', (stor.objDonnees.colineaires) ? textes.corr4_1 : textes.corr4_2, stor.objDonnees)
      } else {
        j3pAffiche(stor.zoneexplication3, '', (stor.objDonnees.colineaires) ? textes.corr5_1 : textes.corr5_2, stor.objDonnees)
      }
    }
  }
  function AjoutePar (nb) {
    if (nb < 0) return '(' + j3pVirgule(nb) + ')'
    return j3pVirgule(nb)
  }
  function genereDonnees (isPointsAlignes, proba) {
    const nomPts = []
    const obj = {}
    const tabPts = ['A', 'B', 'C', 'D']
    for (let i = 0; i < 8; i++) nomPts.push(String.fromCharCode(65 + i))
    let alea
    for (let i = 0; i < ((isPointsAlignes) ? 3 : 4); i++) {
      alea = j3pGetRandomInt(0, nomPts.length - 1)
      obj[tabPts[i]] = nomPts[alea]
      nomPts.splice(alea, 1)
    }
    const choixk = [[2, 3], [3, 4], [2, 5], [3, 5], [4, 5], [2, 7], [3, 7], [4, 7], [5, 7], [3, 8], [5, 8], [2, 9], [4, 9], [5, 9], [7, 9]]
    let num, den
    do {
      alea = j3pGetRandomInt(0, choixk.length - 1)
      const choixSigne = 2 * j3pGetRandomInt(0, 1) - 1
      const ordre = j3pGetRandomBool()
      num = choixSigne * ((ordre) ? choixk[alea][0] : choixk[alea][1])
      den = (ordre) ? choixk[alea][1] : choixk[alea][0]
      obj.xu = den * j3pGetRandomInt(1, Math.floor(130 / (den * num)))
      obj.yu = den * j3pGetRandomInt(1, Math.floor(130 / (den * num)))
    } while (Math.abs(obj.xu - obj.yu) < 1e-12 || Math.abs(obj.xu) < 1e-12 || Math.abs(obj.yu) < Math.pow(10, -12))
    obj.xv = num * obj.xu / den
    obj.yv = num * obj.yu / den
    obj.colineaires = Math.random() < proba
    obj.xA = j3pGetRandomInt(-8, 8)
    obj.yA = j3pGetRandomInt(-8, 8)
    obj.xB = obj.xA + obj.xu
    obj.yB = obj.yA + obj.yu

    if (isPointsAlignes) {
      obj.xC = obj.xA + obj.xv
      obj.yC = obj.yA + obj.yv
      if (!obj.colineaires) {
        do {
          obj.xC2 = obj.xC + j3pGetRandomInt(-1, 1)
          obj.yC2 = obj.yC + j3pGetRandomInt(-1, 1)
        } while (Math.abs((obj.xC2 - obj.xA) * obj.yu - (obj.yC2 - obj.yA) * obj.xu) < Math.pow(10, -12))
        obj.xC = obj.xC2
        obj.yC = obj.yC2
      }
    } else {
      do {
        obj.xC = j3pGetRandomInt(-8, 8)
      } while (Math.abs(obj.xA - obj.xC) < Math.pow(10, -12))
      do {
        obj.yC = j3pGetRandomInt(-8, 8)
      } while (Math.abs(obj.yA - obj.yC) < Math.pow(10, -12))
      obj.xD = obj.xC + obj.xv
      obj.yD = obj.yC + obj.yv
      if (!obj.colineaires) {
        do {
          obj.xD2 = obj.xD + j3pGetRandomInt(-1, 1)
          obj.yD2 = obj.yD + j3pGetRandomInt(-1, 1)
        } while (Math.abs((obj.xD2 - obj.xC) * obj.yu - (obj.yD2 - obj.yC) * obj.xu) < Math.pow(10, -12))
        obj.xD = obj.xD2
        obj.yD = obj.yD2
      }
    }
    // Les vecteurs que je vais prendre par défaut
    obj.u = obj.A + obj.B
    obj.absu = obj.xB - obj.xA
    obj.ordu = obj.yB - obj.yA
    obj.rep1 = '\\vecteur{' + obj.u + '}\\matrixtwoone{x_' + obj.B + '-x_' + obj.A + '}{y_' + obj.B + '-y_' + obj.A + '};\\vecteur{' + obj.u + '}\\matrixtwoone{' + obj.xB + '-' + AjoutePar(obj.xA) + '}{' + obj.yB + '-' + AjoutePar(obj.yA) + '}; \\vecteur{' + obj.u + '}\\matrixtwoone{' + (obj.xB - obj.xA) + '}{' + (obj.yB - obj.yA) + '}'
    if (isPointsAlignes) {
      obj.v = obj.A + obj.C
      obj.absv = obj.xC - obj.xA
      obj.ordv = obj.yC - obj.yA
      obj.rep2 = '\\vecteur{' + obj.v + '}\\matrixtwoone{x_' + obj.C + '-x_' + obj.A + '}{y_' + obj.C + '-y_' + obj.A + '};\\vecteur{ ' + obj.v + '}\\matrixtwoone{' + obj.xC + '-' + AjoutePar(obj.xA) + '}{' + obj.yC + '-' + AjoutePar(obj.yA) + '}; \\vecteur{' + obj.v + '}\\matrixtwoone{' + (obj.xC - obj.xA) + '}{' + (obj.yC - obj.yA) + '}'
    } else {
      obj.v = obj.C + obj.D
      obj.absv = obj.xD - obj.xC
      obj.ordv = obj.yD - obj.yC
      obj.rep2 = '\\vecteur{' + obj.v + '}\\matrixtwoone{x_' + obj.D + '-x_' + obj.C + '}{y_' + obj.D + '-y_' + obj.C + '};\\vecteur{ ' + obj.v + '}\\matrixtwoone{' + obj.xD + '-' + AjoutePar(obj.xC) + '}{' + obj.yD + '-' + AjoutePar(obj.yC) + '}; \\vecteur{' + obj.v + '}\\matrixtwoone{' + (obj.xD - obj.xC) + '}{' + (obj.yD - obj.yC) + '}'
    }
    obj.c = obj.absu + '\\times ' + AjoutePar(obj.ordv) + '-' + AjoutePar(obj.absv) + '\\times ' + AjoutePar(obj.ordu)
    obj.r = obj.absu * obj.ordv - obj.absv * obj.ordu

    me.logIfDebug('obj:', obj)
    return obj
  }

  function enonceInitFirst () {
    // Le paramètre définit la largeur relative de la première colonne
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    stor.typeQuest = []
    if (ds.typeQuest === 'les deux') {
      // alternance des cas de figure
      const casFigure = ['points alignés', 'droites parallèles']
      for (let k = 1; k <= ds.nbrepetitions / 2; k++) stor.typeQuest.push(casFigure[0], casFigure[1])
      if (ds.nbrepetitions % 2 === 1) stor.typeQuest.push(casFigure[j3pGetRandomInt(0, 1)])
      stor.typeQuest = j3pShuffle(stor.typeQuest)
    } else {
      for (let k = 0; k < ds.nbrepetitions; k++) stor.typeQuest.push(ds.typeQuest)
    }
    me.logIfDebug('stor.typeQuest:', stor.typeQuest)
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    // me.ajouteBoutons();
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    stor.numEssai = 1
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    if (me.questionCourante % nbetapes === 1) {
      stor.quest = (stor.typeQuest[Math.floor(me.questionCourante / nbetapes)] === 'points alignés')
      // stor.quest vaut true pour des points alignés
      stor.objDonnees = genereDonnees(stor.quest, 0.4) // 0.4 est la proba d’avoir des vecteurs colinéaires
    }
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    const nbDiv = (me.questionCourante % nbetapes === 1) ? 6 : 9
    for (let i = 1; i <= nbDiv; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    j3pAffiche(stor.zoneCons1, '', (stor.quest) ? textes.consigne1_1 : textes.consigne1_2, stor.objDonnees)
    j3pAffiche(stor.zoneCons2, '', (stor.quest) ? textes.consigne2_1 : textes.consigne2_2, stor.objDonnees)
    stor.zoneInput = []
    if (me.questionCourante % nbetapes === 1) {
      j3pAffiche(stor.zoneCons3, '', textes.consigne3)
      // stor.zoneCons4 servira à mettre les réponses de l’élèves
      const elt = j3pAffiche(stor.zoneCons5, '', textes.consigne4)
      stor.zoneInput = [...elt.inputmqefList]
      for (let i = 1; i <= 6; i++) {
        if (i % 3 === 1) {
          mqRestriction(stor.zoneInput[i - 1], 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')
          stor.zoneInput[i - 1].addEventListener('input', function () {
            if (j3pValeurde(this).length > 2) $(this).mathquill('latex', j3pValeurde(this).substring(0, 2))
          })
          stor.zoneInput[i - 1].addEventListener('change', function () {
            if (j3pValeurde(this).length > 2) $(this).mathquill('latex', j3pValeurde(this).substring(0, 2))
          })
        } else {
          mqRestriction(stor.zoneInput[i - 1], '\\d,.-+')
        }
      }
      const consIndic = (ds.nbchances === 1) ? textes.comment1_1 : j3pChaine(textes.comment1_2, { t: ds.nbchances })
      stor.zoneCons6.style.fontSize = '0.9em'
      stor.zoneCons6.style.fontStyle = 'italic'
      stor.zoneCons6.paddingTop = '8px'
      j3pAffiche(stor.zoneCons6, '', consIndic)
      if (stor.quest) {
      // points alignés
        stor.couplesVecteurs = [
          [stor.objDonnees.A + stor.objDonnees.B, stor.objDonnees.A + stor.objDonnees.C],
          [stor.objDonnees.A + stor.objDonnees.B, stor.objDonnees.C + stor.objDonnees.A],
          [stor.objDonnees.B + stor.objDonnees.A, stor.objDonnees.A + stor.objDonnees.C],
          [stor.objDonnees.B + stor.objDonnees.A, stor.objDonnees.C + stor.objDonnees.A],
          [stor.objDonnees.A + stor.objDonnees.B, stor.objDonnees.B + stor.objDonnees.C],
          [stor.objDonnees.A + stor.objDonnees.B, stor.objDonnees.C + stor.objDonnees.B],
          [stor.objDonnees.B + stor.objDonnees.A, stor.objDonnees.B + stor.objDonnees.C],
          [stor.objDonnees.B + stor.objDonnees.A, stor.objDonnees.C + stor.objDonnees.B],
          [stor.objDonnees.A + stor.objDonnees.C, stor.objDonnees.B + stor.objDonnees.C],
          [stor.objDonnees.A + stor.objDonnees.C, stor.objDonnees.C + stor.objDonnees.B],
          [stor.objDonnees.C + stor.objDonnees.A, stor.objDonnees.B + stor.objDonnees.C],
          [stor.objDonnees.C + stor.objDonnees.A, stor.objDonnees.C + stor.objDonnees.B]
        ]
        stor.calculsCoord = [
          ['\\matrixtwoone{x_' + stor.objDonnees.B + '-x_' + stor.objDonnees.A + '}{y_' + stor.objDonnees.B + '-y_' + stor.objDonnees.A + '}', '\\matrixtwoone{x_' + stor.objDonnees.C + '-x_' + stor.objDonnees.A + '}{y_' + stor.objDonnees.C + '-y_' + stor.objDonnees.A + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.B + '-x_' + stor.objDonnees.A + '}{y_' + stor.objDonnees.B + '-y_' + stor.objDonnees.A + '}', '\\matrixtwoone{x_' + stor.objDonnees.A + '-x_' + stor.objDonnees.C + '}{y_' + stor.objDonnees.A + '-y_' + stor.objDonnees.C + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.A + '-x_' + stor.objDonnees.B + '}{y_' + stor.objDonnees.A + '-y_' + stor.objDonnees.B + '}', '\\matrixtwoone{x_' + stor.objDonnees.C + '-x_' + stor.objDonnees.A + '}{y_' + stor.objDonnees.C + '-y_' + stor.objDonnees.A + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.A + '-x_' + stor.objDonnees.B + '}{y_' + stor.objDonnees.A + '-y_' + stor.objDonnees.B + '}', '\\matrixtwoone{x_' + stor.objDonnees.A + '-x_' + stor.objDonnees.C + '}{y_' + stor.objDonnees.A + '-y_' + stor.objDonnees.C + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.B + '-x_' + stor.objDonnees.A + '}{y_' + stor.objDonnees.B + '-y_' + stor.objDonnees.A + '}', '\\matrixtwoone{x_' + stor.objDonnees.C + '-x_' + stor.objDonnees.B + '}{y_' + stor.objDonnees.C + '-y_' + stor.objDonnees.B + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.B + '-x_' + stor.objDonnees.A + '}{y_' + stor.objDonnees.B + '-y_' + stor.objDonnees.A + '}', '\\matrixtwoone{x_' + stor.objDonnees.B + '-x_' + stor.objDonnees.C + '}{y_' + stor.objDonnees.B + '-y_' + stor.objDonnees.C + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.A + '-x_' + stor.objDonnees.B + '}{y_' + stor.objDonnees.A + '-y_' + stor.objDonnees.B + '}', '\\matrixtwoone{x_' + stor.objDonnees.C + '-x_' + stor.objDonnees.B + '}{y_' + stor.objDonnees.C + '-y_' + stor.objDonnees.B + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.A + '-x_' + stor.objDonnees.B + '}{y_' + stor.objDonnees.A + '-y_' + stor.objDonnees.B + '}', '\\matrixtwoone{x_' + stor.objDonnees.B + '-x_' + stor.objDonnees.C + '}{y_' + stor.objDonnees.B + '-y_' + stor.objDonnees.C + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.C + '-x_' + stor.objDonnees.A + '}{y_' + stor.objDonnees.C + '-y_' + stor.objDonnees.A + '}', '\\matrixtwoone{x_' + stor.objDonnees.C + '-x_' + stor.objDonnees.B + '}{y_' + stor.objDonnees.C + '-y_' + stor.objDonnees.B + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.C + '-x_' + stor.objDonnees.A + '}{y_' + stor.objDonnees.C + '-y_' + stor.objDonnees.A + '}', '\\matrixtwoone{x_' + stor.objDonnees.B + '-x_' + stor.objDonnees.C + '}{y_' + stor.objDonnees.B + '-y_' + stor.objDonnees.C + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.A + '-x_' + stor.objDonnees.C + '}{y_' + stor.objDonnees.A + '-y_' + stor.objDonnees.C + '}', '\\matrixtwoone{x_' + stor.objDonnees.C + '-x_' + stor.objDonnees.B + '}{y_' + stor.objDonnees.C + '-y_' + stor.objDonnees.B + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.A + '-x_' + stor.objDonnees.C + '}{y_' + stor.objDonnees.A + '-y_' + stor.objDonnees.C + '}', '\\matrixtwoone{x_' + stor.objDonnees.B + '-x_' + stor.objDonnees.C + '}{y_' + stor.objDonnees.B + '-y_' + stor.objDonnees.C + '}']
        ]
        stor.calculsCoordVal = [
          ['\\matrixtwoone{' + stor.objDonnees.xB + '-' + AjoutePar(stor.objDonnees.xA) + '}{' + stor.objDonnees.yB + '-' + AjoutePar(stor.objDonnees.yA) + '}', '\\matrixtwoone{' + stor.objDonnees.xC + '-' + AjoutePar(stor.objDonnees.xA) + '}{' + stor.objDonnees.yC + '-' + AjoutePar(stor.objDonnees.yA) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xB + '-' + AjoutePar(stor.objDonnees.xA) + '}{' + stor.objDonnees.yB + '-' + AjoutePar(stor.objDonnees.yA) + '}', '\\matrixtwoone{' + stor.objDonnees.xA + '-' + AjoutePar(stor.objDonnees.xC) + '}{' + stor.objDonnees.yA + '-' + AjoutePar(stor.objDonnees.yC) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xA + '-' + AjoutePar(stor.objDonnees.xB) + '}{' + stor.objDonnees.yA + '-' + AjoutePar(stor.objDonnees.yB) + '}', '\\matrixtwoone{' + stor.objDonnees.xC + '-' + AjoutePar(stor.objDonnees.xA) + '}{' + stor.objDonnees.yC + '-' + AjoutePar(stor.objDonnees.yA) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xA + '-' + AjoutePar(stor.objDonnees.xB) + '}{' + stor.objDonnees.yA + '-' + AjoutePar(stor.objDonnees.yB) + '}', '\\matrixtwoone{' + stor.objDonnees.xA + '-' + AjoutePar(stor.objDonnees.xC) + '}{' + stor.objDonnees.yA + '-' + AjoutePar(stor.objDonnees.yC) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xB + '-' + AjoutePar(stor.objDonnees.xA) + '}{' + stor.objDonnees.yB + '-' + AjoutePar(stor.objDonnees.yA) + '}', '\\matrixtwoone{' + stor.objDonnees.xC + '-' + AjoutePar(stor.objDonnees.xB) + '}{' + stor.objDonnees.yC + '-' + AjoutePar(stor.objDonnees.yB) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xB + '-' + AjoutePar(stor.objDonnees.xA) + '}{' + stor.objDonnees.yB + '-' + AjoutePar(stor.objDonnees.yA) + '}', '\\matrixtwoone{' + stor.objDonnees.xB + '-' + AjoutePar(stor.objDonnees.xC) + '}{' + stor.objDonnees.yB + '-' + AjoutePar(stor.objDonnees.yC) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xA + '-' + AjoutePar(stor.objDonnees.xB) + '}{' + stor.objDonnees.yA + '-' + AjoutePar(stor.objDonnees.yB) + '}', '\\matrixtwoone{' + stor.objDonnees.xC + '-' + AjoutePar(stor.objDonnees.xB) + '}{' + stor.objDonnees.yC + '-' + AjoutePar(stor.objDonnees.yB) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xA + '-' + AjoutePar(stor.objDonnees.xB) + '}{' + stor.objDonnees.yA + '-' + AjoutePar(stor.objDonnees.yB) + '}', '\\matrixtwoone{' + stor.objDonnees.xB + '-' + AjoutePar(stor.objDonnees.xC) + '}{' + stor.objDonnees.yB + '-' + AjoutePar(stor.objDonnees.yC) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xC + '-' + AjoutePar(stor.objDonnees.xA) + '}{' + stor.objDonnees.yC + '-' + AjoutePar(stor.objDonnees.yA) + '}', '\\matrixtwoone{' + stor.objDonnees.xC + '-' + AjoutePar(stor.objDonnees.xB) + '}{' + stor.objDonnees.yC + '-' + AjoutePar(stor.objDonnees.yB) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xC + '-' + AjoutePar(stor.objDonnees.xA) + '}{' + stor.objDonnees.yC + '-' + AjoutePar(stor.objDonnees.yA) + '}', '\\matrixtwoone{' + stor.objDonnees.xB + '-' + AjoutePar(stor.objDonnees.xC) + '}{' + stor.objDonnees.yB + '-' + AjoutePar(stor.objDonnees.yC) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xA + '-' + AjoutePar(stor.objDonnees.xC) + '}{' + stor.objDonnees.yA + '-' + AjoutePar(stor.objDonnees.yC) + '}', '\\matrixtwoone{' + stor.objDonnees.xC + '-' + AjoutePar(stor.objDonnees.xB) + '}{' + stor.objDonnees.yC + '-' + AjoutePar(stor.objDonnees.yB) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xA + '-' + AjoutePar(stor.objDonnees.xC) + '}{' + stor.objDonnees.yA + '-' + AjoutePar(stor.objDonnees.yC) + '}', '\\matrixtwoone{' + stor.objDonnees.xB + '-' + AjoutePar(stor.objDonnees.xC) + '}{' + stor.objDonnees.yB + '-' + AjoutePar(stor.objDonnees.yC) + '}']
        ]
        stor.valCoord = [
          [[stor.objDonnees.xB - stor.objDonnees.xA, stor.objDonnees.yB - stor.objDonnees.yA], [stor.objDonnees.xC - stor.objDonnees.xA, stor.objDonnees.yC - stor.objDonnees.yA]],
          [[stor.objDonnees.xB - stor.objDonnees.xA, stor.objDonnees.yB - stor.objDonnees.yA], [stor.objDonnees.xA - stor.objDonnees.xC, stor.objDonnees.yA - stor.objDonnees.yC]],
          [[stor.objDonnees.xA - stor.objDonnees.xB, stor.objDonnees.yA - stor.objDonnees.yB], [stor.objDonnees.xC - stor.objDonnees.xA, stor.objDonnees.yC - stor.objDonnees.yA]],
          [[stor.objDonnees.xA - stor.objDonnees.xB, stor.objDonnees.yA - stor.objDonnees.yB], [stor.objDonnees.xA - stor.objDonnees.xC, stor.objDonnees.yA - stor.objDonnees.yC]],
          [[stor.objDonnees.xB - stor.objDonnees.xA, stor.objDonnees.yB - stor.objDonnees.yA], [stor.objDonnees.xC - stor.objDonnees.xB, stor.objDonnees.yC - stor.objDonnees.yB]],
          [[stor.objDonnees.xB - stor.objDonnees.xA, stor.objDonnees.yB - stor.objDonnees.yA], [stor.objDonnees.xB - stor.objDonnees.xC, stor.objDonnees.yB - stor.objDonnees.yC]],
          [[stor.objDonnees.xA - stor.objDonnees.xB, stor.objDonnees.yA - stor.objDonnees.yB], [stor.objDonnees.xC - stor.objDonnees.xB, stor.objDonnees.yC - stor.objDonnees.yB]],
          [[stor.objDonnees.xA - stor.objDonnees.xB, stor.objDonnees.yA - stor.objDonnees.yB], [stor.objDonnees.xB - stor.objDonnees.xC, stor.objDonnees.yB - stor.objDonnees.yC]],
          [[stor.objDonnees.xC - stor.objDonnees.xA, stor.objDonnees.yC - stor.objDonnees.yA], [stor.objDonnees.xC - stor.objDonnees.xB, stor.objDonnees.yC - stor.objDonnees.yB]],
          [[stor.objDonnees.xC - stor.objDonnees.xA, stor.objDonnees.yC - stor.objDonnees.yA], [stor.objDonnees.xB - stor.objDonnees.xC, stor.objDonnees.yB - stor.objDonnees.yC]],
          [[stor.objDonnees.xA - stor.objDonnees.xC, stor.objDonnees.yA - stor.objDonnees.yC], [stor.objDonnees.xC - stor.objDonnees.xB, stor.objDonnees.yC - stor.objDonnees.yB]],
          [[stor.objDonnees.xA - stor.objDonnees.xC, stor.objDonnees.yA - stor.objDonnees.yC], [stor.objDonnees.xB - stor.objDonnees.xC, stor.objDonnees.yB - stor.objDonnees.yC]]
        ]
      } else {
        stor.couplesVecteurs = [
          [stor.objDonnees.A + stor.objDonnees.B, stor.objDonnees.C + stor.objDonnees.D],
          [stor.objDonnees.A + stor.objDonnees.B, stor.objDonnees.D + stor.objDonnees.C],
          [stor.objDonnees.B + stor.objDonnees.A, stor.objDonnees.C + stor.objDonnees.D],
          [stor.objDonnees.B + stor.objDonnees.A, stor.objDonnees.D + stor.objDonnees.C]
        ]
        stor.calculsCoord = [
          ['\\matrixtwoone{x_' + stor.objDonnees.B + '-x_' + stor.objDonnees.A + '}{y_' + stor.objDonnees.B + '-y_' + stor.objDonnees.A + '}', '\\matrixtwoone{x_' + stor.objDonnees.D + '-x_' + stor.objDonnees.C + '}{y_' + stor.objDonnees.D + '-y_' + stor.objDonnees.C + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.B + '-x_' + stor.objDonnees.A + '}{y_' + stor.objDonnees.B + '-y_' + stor.objDonnees.A + '}', '\\matrixtwoone{x_' + stor.objDonnees.C + '-x_' + stor.objDonnees.D + '}{y_' + stor.objDonnees.C + '-y_' + stor.objDonnees.D + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.A + '-x_' + stor.objDonnees.B + '}{y_' + stor.objDonnees.A + '-y_' + stor.objDonnees.B + '}', '\\matrixtwoone{x_' + stor.objDonnees.D + '-x_' + stor.objDonnees.C + '}{y_' + stor.objDonnees.D + '-y_' + stor.objDonnees.C + '}'],
          ['\\matrixtwoone{x_' + stor.objDonnees.A + '-x_' + stor.objDonnees.B + '}{y_' + stor.objDonnees.A + '-y_' + stor.objDonnees.B + '}', '\\matrixtwoone{x_' + stor.objDonnees.C + '-x_' + stor.objDonnees.D + '}{y_' + stor.objDonnees.C + '-y_' + stor.objDonnees.D + '}']
        ]
        stor.calculsCoordVal = [
          ['\\matrixtwoone{' + stor.objDonnees.xB + '-' + AjoutePar(stor.objDonnees.xA) + '}{' + stor.objDonnees.yB + '-' + AjoutePar(stor.objDonnees.yA) + '}', '\\matrixtwoone{' + stor.objDonnees.xD + '-' + AjoutePar(stor.objDonnees.xC) + '}{' + stor.objDonnees.yD + '-' + AjoutePar(stor.objDonnees.yC) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xB + '-' + AjoutePar(stor.objDonnees.xA) + '}{' + stor.objDonnees.yB + '-' + AjoutePar(stor.objDonnees.yA) + '}', '\\matrixtwoone{' + stor.objDonnees.xC + '-' + AjoutePar(stor.objDonnees.xD) + '}{' + stor.objDonnees.yC + '-' + AjoutePar(stor.objDonnees.yD) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xA + '-' + AjoutePar(stor.objDonnees.xB) + '}{' + stor.objDonnees.yA + '-' + AjoutePar(stor.objDonnees.yB) + '}', '\\matrixtwoone{' + stor.objDonnees.xD + '-' + AjoutePar(stor.objDonnees.xC) + '}{' + stor.objDonnees.yD + '-' + AjoutePar(stor.objDonnees.yC) + '}'],
          ['\\matrixtwoone{' + stor.objDonnees.xA + '-' + AjoutePar(stor.objDonnees.xB) + '}{' + stor.objDonnees.yA + '-' + AjoutePar(stor.objDonnees.yB) + '}', '\\matrixtwoone{' + stor.objDonnees.xC + '-' + AjoutePar(stor.objDonnees.xD) + '}{' + stor.objDonnees.yC + '-' + AjoutePar(stor.objDonnees.yD) + '}']
        ]
        stor.valCoord = [
          [[stor.objDonnees.xB - stor.objDonnees.xA, stor.objDonnees.yB - stor.objDonnees.yA], [stor.objDonnees.xD - stor.objDonnees.xC, stor.objDonnees.yD - stor.objDonnees.yC]],
          [[stor.objDonnees.xB - stor.objDonnees.xA, stor.objDonnees.yB - stor.objDonnees.yA], [stor.objDonnees.xC - stor.objDonnees.xD, stor.objDonnees.yC - stor.objDonnees.yD]],
          [[stor.objDonnees.xA - stor.objDonnees.xB, stor.objDonnees.yA - stor.objDonnees.yB], [stor.objDonnees.xD - stor.objDonnees.xC, stor.objDonnees.yD - stor.objDonnees.yC]],
          [[stor.objDonnees.xA - stor.objDonnees.xB, stor.objDonnees.yA - stor.objDonnees.yB], [stor.objDonnees.xC - stor.objDonnees.xD, stor.objDonnees.yC - stor.objDonnees.yD]]
        ]
      }
      for (let i = 1; i <= 6; i += 3) {
        stor.zoneInput[i - 1].typeReponse = ['texte']
        stor.zoneInput[i].typeReponse = ['nombre', 'exact']
        stor.zoneInput[i + 1].typeReponse = ['nombre', 'exact']
      }
    } else {
      j3pAffiche(stor.zoneCons3, '', textes.consigne5)
      j3pAffiche(stor.zoneCons4, '', '$\\vecteur{£u}\\matrixtwoone{£{absu}}{£{ordu}}$ ; $\\vecteur{£v}\\matrixtwoone{£{absv}}{£{ordv}}$', stor.objDonnees)
      j3pAffiche(stor.zoneCons5, '', textes.consigne6)
      const elt = j3pAffiche(stor.zoneCons6, '', '$\\det\\left(\\vecteur{' + stor.objDonnees.u + '};\\vecteur{' + stor.objDonnees.v + '}\\right)=$&1&',
        {
          inputmq1: {}
        })
      stor.zoneInput.push(elt.inputmqList[0])
      mqRestriction(stor.zoneInput[0], '0\\d/-,.*()+', { commandes: ['fraction'] })
      stor.zoneCons7.style.paddingTop = '5px'
      stor.zoneCons7.style.paddingBottom = '45px'
      j3pPaletteMathquill(stor.zoneCons7, stor.zoneInput[0], { liste: ['fraction'] })
      j3pAffiche(stor.zoneCons8, '', textes.consigne7)
      stor.nameRadio = j3pGetNewId('choix')
      stor.idsRadio = []
      stor.idsLabelRadio = []
      for (let i = 1; i <= 2; i++) {
        const divRadio = j3pAddElt(stor.zoneCons9, 'div')
        stor.idsRadio.push(j3pGetNewId('radio'))
        stor.idsLabelRadio.push(j3pGetNewId('labelRadio'))
        j3pBoutonRadio(divRadio, stor.idsRadio[i - 1], stor.nameRadio, 0, '')
        if (stor.quest) j3pAffiche('label' + stor.idsRadio[i - 1], stor.idsLabelRadio[i - 1], textes['consigne8_' + i], stor.objDonnees)
        else j3pAffiche('label' + stor.idsRadio[i - 1], stor.idsLabelRadio[i - 1], textes['consigne9_' + i], stor.objDonnees)
      }
    }
    me.logIfDebug('données:', stor.objDonnees)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    const zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(zoneMD, 'div', '')
    stor.zoneCorr = j3pAddElt(zoneMD, 'div', '')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validation de cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })
    j3pFocus(mesZonesSaisie[0])
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        let reponse = {}
        reponse.aRepondu = stor.fctsValid.valideReponses()
        me.logIfDebug('reponse:', reponse, stor.fctsValid.zones.reponseSaisie)
        let estCalcul = false
        let estNombre = true
        let bonDeterminant = true
        let j
        let numRadioSelect, bonRadio
        if (reponse.aRepondu) {
          if (me.questionCourante % nbetapes === 1) {
          // On cherche à savoir si la première zone est une réponse acceptable
            const indexV1possibles = []
            const indexV2possibles = []
            let index0 = -1
            let index1 = -1
            for (let i = 0; i < stor.couplesVecteurs.length; i++) {
              if (stor.fctsValid.zones.reponseSaisie[0] === stor.couplesVecteurs[i][0]) indexV1possibles.push(i)
              if (stor.fctsValid.zones.reponseSaisie[0] === stor.couplesVecteurs[i][1]) indexV2possibles.push(i)
            }
            let listeReponses = []
            let bonCalcCoord = []
            // Pour chaque index où on a trouvé le premier vecteur, on cherche si le 2ème est bien celui qui a été proposé
            // On aura alors l’index à vérifier pour les autres zones
            for (let i = 0; i < indexV1possibles.length; i++) {
              if (stor.fctsValid.zones.reponseSaisie[3] === stor.couplesVecteurs[indexV1possibles[i]][1]) {
                index0 = indexV1possibles[i]
                listeReponses = [stor.couplesVecteurs[index0][0], stor.valCoord[index0][0][0], stor.valCoord[index0][0][1], stor.couplesVecteurs[index0][1], stor.valCoord[index0][1][0], stor.valCoord[index0][1][1]]
                bonCalcCoord = [stor.calculsCoord[index0][0], stor.calculsCoord[index0][1], stor.calculsCoordVal[index0][0], stor.calculsCoordVal[index0][1]]
              }
            }
            if (index0 === -1) {
              for (let i = 0; i < indexV2possibles.length; i++) {
                if (stor.fctsValid.zones.reponseSaisie[3] === stor.couplesVecteurs[indexV2possibles[i]][0]) {
                  index1 = indexV2possibles[i]
                  listeReponses = [stor.couplesVecteurs[index1][1], stor.valCoord[index1][1][0], stor.valCoord[index1][1][1], stor.couplesVecteurs[index1][0], stor.valCoord[index1][0][0], stor.valCoord[index1][0][1]]
                  bonCalcCoord = [stor.calculsCoord[index1][1], stor.calculsCoord[index1][0], stor.calculsCoordVal[index1][1], stor.calculsCoordVal[index1][0]]
                }
              }
            }
            stor.bonVecteurs = (index0 !== -1 || index1 !== -1)
            if (stor.bonVecteurs) {
              for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
                stor.fctsValid.zones.inputs[i].reponse = [listeReponses[i]]
              }
              stor.objDonnees.u = listeReponses[0]
              stor.objDonnees.v = listeReponses[3]
              stor.objDonnees.absu = listeReponses[1]
              stor.objDonnees.ordu = listeReponses[2]
              stor.objDonnees.absv = listeReponses[4]
              stor.objDonnees.ordv = listeReponses[5]
              stor.objDonnees.rep1 = '\\vecteur{' + stor.objDonnees.u + '}' + bonCalcCoord[0] + ';\\vecteur{' + stor.objDonnees.u + '}' + bonCalcCoord[2] + ';\\vecteur{' + stor.objDonnees.u + '}\\matrixtwoone{' + stor.objDonnees.absu + '}{' + stor.objDonnees.ordu + '}'
              stor.objDonnees.rep2 = '\\vecteur{' + stor.objDonnees.v + '}' + bonCalcCoord[1] + ';\\vecteur{' + stor.objDonnees.v + '}' + bonCalcCoord[3] + ';\\vecteur{' + stor.objDonnees.v + '}\\matrixtwoone{' + stor.objDonnees.absv + '}{' + stor.objDonnees.ordv + '}'
              // Je récupère également ce dont j’ai besoin pour le calcul du déterminant
              stor.objDonnees.c = stor.objDonnees.absu + '\\times ' + AjoutePar(stor.objDonnees.ordv) + '-' + AjoutePar(stor.objDonnees.absv) + '\\times ' + AjoutePar(stor.objDonnees.ordu)
              stor.objDonnees.r = stor.objDonnees.absu * stor.objDonnees.ordv - stor.objDonnees.absv * stor.objDonnees.ordu
              reponse = stor.fctsValid.validationGlobale()
            } else {
            // les vecteurs proposés ne permettent pas de répondre à la question
            // je passe tout en rouge
              reponse.bonneReponse = false
              for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
                stor.fctsValid.zones.bonneReponse[i] = false
              }
              stor.fctsValid.coloreLesZones()
            }
          } else {
          // Je vérifie que je n’ai qu’un nombre, pas un calcul
          // Je vérifie que le calcul donne bien le déterminant
            const valDeterminant = j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[0])
            bonDeterminant = (Math.abs(valDeterminant - stor.objDonnees.r) < Math.pow(10, -12))
            me.logIfDebug('comparaison déterminant : ', valDeterminant, stor.objDonnees.r)
            if (bonDeterminant) {
              estNombre = !isNaN(Number(valDeterminant))
              const repSaisie = j3pMathquillXcas(stor.fctsValid.zones.reponseSaisie[0])
              estCalcul = ((/[-+/*()]/.test(repSaisie.substring(1))) && (stor.fctsValid.zones.reponseSaisie[0].replace(/\\frac{-\d+}{\d+}/, '') !== ''))
              reponse.aRepondu = !estCalcul && estNombre
              stor.fctsValid.zones.bonneReponse[0] = true
            } else {
              stor.fctsValid.zones.bonneReponse[0] = false
            }
            if (reponse.aRepondu) {
            // On vérifie qu’un bouton radio a été sélectionné
              numRadioSelect = j3pBoutonRadioChecked(stor.nameRadio)[0] + 1
              reponse.aRepondu = (numRadioSelect > 0)
              if (reponse.aRepondu) {
                const determinantNul = Math.abs(valDeterminant) < 1e-12
                bonRadio = (determinantNul) ? numRadioSelect === 1 : numRadioSelect === 2
                reponse.bonneReponse = bonDeterminant && bonRadio
                if (bonDeterminant && !bonRadio) {
                // On ne lui donne pas de nouvelle chance
                  stor.numEssai = ds.nbchances
                  me.essaiCourant = ds.nbchances - 1
                }
              }
            }
            if (reponse.aRepondu) stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
          }
        }
        if (!reponse.aRepondu && !me.isElapsed) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée
          let msgReponseManquante // si c’est pas initialisé ce sera le message par défaut
          if (me.questionCourante % nbetapes === 0) {
            if (!estNombre) {
              msgReponseManquante = textes.comment6
            } else if (estCalcul) {
              msgReponseManquante = textes.comment5
              j3pFocus(stor.zoneInput[0])
            } else if (numRadioSelect === 0) {
              msgReponseManquante = textes.comment4
            }
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :

            if (me.questionCourante % nbetapes === 0) {
              for (j = 1; j <= 2; j++) j3pDesactive(stor.idsRadio[j - 1])
              j3pElement(stor.idsLabelRadio[numRadioSelect - 1]).style.color = me.styles.cbien
            }
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              if (me.questionCourante % nbetapes === 0) {
                for (j = 1; j <= 2; j++) j3pDesactive(stor.idsRadio[j - 1])
                if (!bonRadio) {
                  j3pBarre(stor.idsLabelRadio[numRadioSelect - 1])
                  j3pElement(stor.idsLabelRadio[numRadioSelect - 1]).style.color = me.styles.cfaux
                }
              }
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if ((me.questionCourante % nbetapes === 0) && !bonDeterminant && bonRadio) stor.zoneCorr.innerHTML += '<br>' + textes.comment7
              stor.numEssai++
              if ((me.questionCourante % nbetapes === 1)) {
                if (stor.bonVecteurs) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment3
                } else {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment2
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment2Bis
                }
              }
              if (stor.numEssai <= ds.nbchances) {
                if (me.questionCourante % nbetapes === 1) {
                  const consIndicCorr = (ds.nbchances + 1 - stor.numEssai === 1) ? textes.comment1_1 : j3pChaine(textes.comment1_2, { t: ds.nbchances + 1 - stor.numEssai })
                  stor.zoneCons6.innerHTML = consIndicCorr
                  me.logIfDebug('consIndicCorr:', consIndicCorr)
                  // J'écris la réponse de l’élève
                  const newDiv = j3pAddElt(stor.zoneCons4, 'div', '', { style: { color: me.styles.cfaux } })
                  const objRep = {}
                  for (j = 0; j < 6; j++) objRep['zone' + (j + 1)] = stor.fctsValid.zones.reponseSaisie[j]
                  j3pAffiche(newDiv, '', textes.consigne4Rep, objRep)
                }
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                if (me.questionCourante % nbetapes === 0) {
                  for (j = 1; j <= 2; j++) j3pDesactive(stor.idsRadio[j - 1])
                  if (!bonRadio) {
                    j3pBarre(stor.idsLabelRadio[numRadioSelect - 1])
                    j3pElement(stor.idsLabelRadio[numRadioSelect - 1]).style.color = me.styles.cfaux
                  }
                  if (!bonDeterminant) me.typederreurs[6]++
                  me.typederreurs[7]++
                } else {
                  if (stor.bonVecteurs) me.typederreurs[4]++
                  me.typederreurs[5]++
                }
                afficheCorrection(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const monScore = me.score / ds.nbitems
        const freqErreurCalcCoordVect = me.typederreurs[4] / ds.nbetapes
        const freqErreurQ1 = me.typederreurs[5] / ds.nbetapes
        const freqErreurCalcDet = me.typederreurs[6] / ds.nbetapes
        const freqErreurQ2 = me.typederreurs[7] / ds.nbetapes
        if (monScore >= 0.8) {
          me.parcours.pe = ds.pe_1
        } else {
          if (monScore <= 0.25) {
            me.parcours.pe = ds.pe_6
          } else if (freqErreurCalcCoordVect >= 0.5) {
            me.parcours.pe = ds.pe_2
          } else if (freqErreurQ1 >= 0.5) {
            me.parcours.pe = ds.pe_3
          } else if (freqErreurCalcDet >= 0.5) {
            me.parcours.pe = ds.pe_4
          } else if (freqErreurQ2 >= 0.5) {
            me.parcours.pe = ds.pe_5
          } else {
            me.parcours.pe = ds.pe_6
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
