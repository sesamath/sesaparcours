import { j3pAddElt, j3pAjouteBouton, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pDetruit, j3pElement, j3pFocus, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Deniaud Rémi
        avril 2021
        On donne les coordonnées de deux vecteurs et on demande de calculer le déterminant puis de conclure sur leur colinérité
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Vecteurs colinéaires ou non ?',
  // on donne les phrases de la consigne
  consigne1: 'On définit dans le plan deux vecteurs par leurs coordonnées : $\\vec{£u}\\matrixtwoone{£{xu}}{£{yu}}$ et $\\vec{£v}\\matrixtwoone{£{xv}}{£{yv}}$.',
  consigne2: 'Le déterminant de ces deux vecteurs vaut&nbsp;:',
  consigne3: 'Que peut-on en conclure ?',
  consigne4_1: 'Les vecteurs $\\vec{£u}$ et $\\vec{£v}$ sont colinéaires.',
  consigne4_2: 'Les vecteurs $\\vec{£u}$ et $\\vec{£v}$ ne sont pas colinéaires.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Il faut sélectionner l’une des deux propositions&nbsp;!',
  comment2: 'On attend un nombre et pas un calcul&nbsp;!',
  comment3: 'Je ne comprends pas ce qui est écrit&nbsp;!',
  comment4: 'Le déterminant est faux, mais la deuxième partie de la réponse est cohérente&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Comme le déterminant est nul, on conclut que les vecteurs $\\vec{£u}$ et $\\vec{£v}$ sont colinéaires.',
  corr1_2: 'Comme le déterminant est non nul, on conclut que les vecteurs $\\vec{£u}$ et $\\vec{£v}$ ne sont pas colinéaires.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let i
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    j3pDetruit(stor.zoneCons4)
    // explications contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    for (i = 1; i <= 2; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div', '')
    j3pAffiche(stor.zoneexplication1, '', '$\\det\\left(\\vec{£u};\\vec{£v}\\right)=\\determinanttwo{£{xu}}{£{xv}}{£{yu}}{£{yv}}=£c=£r$.', stor.objDonnees)
    j3pAffiche(stor.zoneexplication2, '', (stor.objDonnees.colineaires) ? textes.corr1_1 : textes.corr1_2, stor.objDonnees)
    // dans explication2, ce qui correspond à la deuxième question
  }
  function AjoutePar (nb) {
    if (nb < 0) return '(' + String(nb) + ')'
    return String(nb)
  }
  function genereDonnees (proba) {
    // proba est la probabilité que les vecteurs soient colinéaires
    const obj = {}
    const tabNom = ['u', 'v', 'w', 'z']
    let alea = j3pGetRandomInt(0, tabNom.length - 1)
    obj.u = tabNom[alea]
    tabNom.splice(alea, 1)
    obj.v = tabNom[j3pGetRandomInt(0, tabNom.length - 1)]
    obj.colineaires = Math.random() < proba
    const choixk = [[2, 3], [3, 4], [2, 5], [3, 5], [4, 5], [2, 7], [3, 7], [4, 7], [5, 7], [3, 8], [5, 8], [2, 9], [4, 9], [5, 9], [7, 9]]
    let num, den, decalage
    do {
      alea = j3pGetRandomInt(0, choixk.length - 1)
      const choixSigne = 2 * j3pGetRandomInt(0, 1) - 1
      const ordre = j3pGetRandomBool()
      num = choixSigne * ((ordre) ? choixk[alea][0] : choixk[alea][1])
      den = (ordre) ? choixk[alea][1] : choixk[alea][0]
      obj.xu = den * j3pGetRandomInt(1, Math.floor(130 / (den * num)))
      obj.yu = den * j3pGetRandomInt(1, Math.floor(130 / (den * num)))
    } while (Math.abs(obj.xu - obj.yu) < 1e-12 || Math.abs(obj.xu) < 1e-12 || Math.abs(obj.yu) < Math.pow(10, -12))
    obj.xv = num * obj.xu / den
    obj.yv = num * obj.yu / den
    if (!obj.colineaires) {
      do {
        decalage = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
      } while (Math.abs(obj.yu - (obj.yv + decalage)) < Math.pow(10, -12))
      obj.yv += decalage
    }
    obj.c = String(obj.xu) + '\\times ' + AjoutePar(obj.yv) + '-' + AjoutePar(obj.xv) + '\\times ' + AjoutePar(obj.yu)
    obj.r = obj.xu * obj.yv - obj.xv * obj.yu
    me.logIfDebug('obj:', obj)
    return obj
  }

  function enonceInitFirst () {
    // Le paramètre définit la largeur relative de la première colonne
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    stor.numEssai = 1
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    stor.objDonnees = genereDonnees(0.4)
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objDonnees)
    j3pAffiche(stor.zoneCons2, '', textes.consigne2)
    const elt = j3pAffiche(stor.zoneCons3, '', '$\\det\\left(\\vec{£u};\\vec{£v}\\right)=$&1&',
      {
        u: stor.objDonnees.u,
        v: stor.objDonnees.v,
        inputmq1: {}
      })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d/-,.*()+', { commandes: ['fraction'] })
    stor.zoneCons4.style.paddingTop = '5px'
    stor.zoneCons4.style.paddingBottom = '40px'
    j3pPaletteMathquill(stor.zoneCons4, stor.zoneInput, { liste: ['fraction'] })
    j3pAffiche(stor.zoneCons5, '', textes.consigne3)
    stor.nameRadio = j3pGetNewId('choix')
    stor.idsRadio = []
    stor.idsLabelRadio = []
    for (let i = 1; i <= 2; i++) {
      const divRadio = j3pAddElt(stor.zoneCons6, 'div')
      stor.idsRadio.push(j3pGetNewId('radio'))
      j3pBoutonRadio(divRadio, stor.idsRadio[i - 1], stor.nameRadio, 0, '')
      stor.idsLabelRadio.push(j3pGetNewId('labelRadio'))
      j3pAffiche('label' + stor.idsRadio[i - 1], stor.idsLabelRadio[i - 1], textes['consigne4_' + i], stor.objDonnees)
    }
    // //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    const zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(zoneMD, 'div', '')
    stor.zoneCorr = j3pAddElt(zoneMD, 'div', '')
    j3pStyle(stor.zoneCorr, { paddingTop: '10px' })
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    j3pFocus(stor.zoneInput)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: [stor.zoneInput.id]
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        let estCalcul = false
        let estNombre = true
        let bonDeterminant = true
        let numRadioSelect, bonRadio, valDeterminant
        if (reponse.aRepondu) {
        // Je vérifie que je n’ai qu’un nombre, pas un calcul
        // Je vérifie que le calcul donne bien le déterminant
          valDeterminant = j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[0])
          bonDeterminant = (Math.abs(valDeterminant - stor.objDonnees.r) < Math.pow(10, -12))
          if (bonDeterminant) {
            estNombre = !isNaN(Number(valDeterminant))
            const repSaisie = j3pMathquillXcas(stor.fctsValid.zones.reponseSaisie[0])
            estCalcul = ((/[-+/*()]{1}/.test(repSaisie.substring(1))) && (stor.fctsValid.zones.reponseSaisie[0].replace(/\\frac{-[0-9]+}{[0-9]+}/, '') !== ''))
            reponse.aRepondu = !estCalcul && estNombre
          } else {
            stor.fctsValid.zones.bonneReponse[0] = false
          }
        }
        if (reponse.aRepondu) {
        // On vérifie qu’un bouton radio a été sélectionné
          numRadioSelect = j3pBoutonRadioChecked(stor.nameRadio)[0] + 1
          reponse.aRepondu = (numRadioSelect > 0)
          if (reponse.aRepondu) {
            const determinantNul = Math.abs(j3pCalculValeur(valDeterminant)) < 1e-12
            bonRadio = (determinantNul) ? numRadioSelect === 1 : numRadioSelect === 2
            reponse.bonneReponse = bonDeterminant && bonRadio
            if (bonDeterminant && !bonRadio) {
            // On ne lui donne pas de nouvelle chance
              stor.numEssai = ds.nbchances
              me.essaiCourant = ds.nbchances
            }
          }
        }
        if (reponse.aRepondu) stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (!estNombre) {
            msgReponseManquante = textes.comment3
          } else if (estCalcul) {
            msgReponseManquante = textes.comment2
            j3pFocus(stor.zoneInput)
          } else if (numRadioSelect === 0) {
            msgReponseManquante = textes.comment1
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            for (let j = 1; j <= 2; j++) j3pDesactive(stor.idsRadio[j - 1])
            j3pElement('label' + stor.idsRadio[numRadioSelect - 1]).style.color = me.styles.cbien
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              for (let j = 1; j <= 2; j++) j3pDesactive(stor.idsRadio[j - 1])
              if (!bonRadio) {
                j3pBarre(stor.idsLabelRadio[numRadioSelect - 1])
                j3pElement('label' + stor.idsRadio[numRadioSelect]).style.color = me.styles.cfaux
              }
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!bonDeterminant && bonRadio) stor.zoneCorr.innerHTML += '<br>' + textes.comment4
              stor.numEssai++
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                for (let j = 1; j <= 2; j++) j3pDesactive(stor.idsRadio[j - 1])
                if (!bonRadio) {
                  j3pBarre(stor.idsLabelRadio[numRadioSelect - 1])
                  j3pElement('label' + stor.idsRadio[numRadioSelect - 1]).style.color = me.styles.cfaux
                }
                afficheCorrection(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
}
