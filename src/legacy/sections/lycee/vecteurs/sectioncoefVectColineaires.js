import { j3pAddElt, j3pDetruit, j3pFocus, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Deniaud Rémi
        avril 2021
        On représente deux vecteurs colinéaires et on demande d’exprimer l’un d’eux en fonction de l’autre
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['coefSimples', true, 'boolean', 'On peut imposer des coefficients qui ne soient pas trop difficiles (2 premiers entiers et les autres peu difficiles, sinon, le premier sera entier, le suivant assez simple et les derniers plus compliqués).']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Vecteurs colinéaires',
  // on donne les phrases de la consigne
  consigne1: 'Les vecteurs $\\vecteur{£v}$ et $\\vecteur{£w}$ de la figure ci-contre sont colinéaires.',
  consigne2: 'Déterminer le réel $k$ vérifiant $\\vecteur{£v}=k\\vecteur{£w}$.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Tu n’as le droit qu’à un seul essai.',
  comment2: 'On attend un nombre et pas un calcul&nbsp;!',
  comment3: 'Je ne comprends pas ce qui est écrit&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Les vecteurs sont de même sens, le coefficient cherché est donc positif.',
  corr1_2: 'Les vecteurs ne sont pas de même sens, le coefficient cherché est donc négatif.',
  corr2_1: 'La longueur $£v$ est plus grande que la longueur $£w$ donc le coefficient $k$ est supérieur à 1.',
  corr2_2: 'La longueur $£v$ est plus petite que la longueur $£w$ donc le coefficient $k$ est entre 0 et 1.',
  corr2_3: 'La longueur $£v$ est plus grande que la longueur $£w$ donc le coefficient $k$ est inférieur à $-1$.',
  corr2_4: 'La longueur $£v$ est plus petite que la longueur $£w$ donc le coefficient $k$ est entre $-1$ et $0$.',
  corr3_1: 'En regardant le rapport entre les valeurs ajoutées sur la figure, on trouve $\\vecteur{£v}=£k\\vecteur{£w}$.',
  corr3_2: 'En regardant le rapport entre les longueurs, on trouve $\\vecteur{£v}=£k\\vecteur{£w}$.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    j3pDetruit(stor.zoneCons4)
    // explications contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    for (let i = 1; i <= 3; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div', '')
    j3pAffiche(stor.zoneexplication1, '', (stor.objDonnees.coefk > 0) ? textes.corr1_1 : textes.corr1_2)
    const phraseCorr2 = (stor.objDonnees.coefk > 0)
      ? (Math.abs(stor.objDonnees.coefk) > 1) ? textes.corr2_1 : textes.corr2_2
      : (Math.abs(stor.objDonnees.coefk) > 1) ? textes.corr2_3 : textes.corr2_4
    j3pAffiche(stor.zoneexplication2, '', phraseCorr2, stor.objDonnees)
    const phraseCorr3 = (stor.objDonnees.horizontal || stor.objDonnees.vertical) ? textes.corr3_2 : textes.corr3_1
    j3pAffiche(stor.zoneexplication3, '', phraseCorr3, stor.objDonnees)
    modifFig({ correction: true })
  }
  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAPIAAACugAAAQEAAAAAAAAAAQAAAHv#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAAAAUAAcArR64UeuFAQHUA9cKPXCn#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAEOAAFJAMAYAAAAAAAAAAAAAAAAAAAAAAUAAUBBc#fO2RaHAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAAEAAAAQAAAAEAAAABAAAAA#####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8AAAAAABAAAAEAAAABAAAAAQAAAAT#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQAABQABAAAABwAAAAkA#####wAAAAABDgABSgDAKAAAAAAAAMAQAAAAAAAAAAAFAAIAAAAH#####wAAAAIAB0NSZXBlcmUA#####wDm5uYAAAABAAAAAQAAAAMAAAAJAQEAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEAAAEAAAAAAwAAAAz#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAABAAAADf####8AAAABAAdDQ2FsY3VsAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAAEQD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABRHcmFkdWF0aW9uQXhlc1JlcGVyZQAAABsAAAAIAAAAAwAAAAoAAAAPAAAAEP####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAABEABWFic29yAAAACv####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAABEABW9yZG9yAAAACgAAAAsAAAAAEQAGdW5pdGV4AAAACv####8AAAABAApDVW5pdGV5UmVwAAAAABEABnVuaXRleQAAAAr#####AAAAAQAQQ1BvaW50RGFuc1JlcGVyZQAAAAARAAAAAAAQAAABAAAFAAAAAAoAAAAOAAAAEgAAAA4AAAATAAAAFgAAAAARAAAAAAAQAAABAAAFAAAAAAoAAAANAAAAAA4AAAASAAAADgAAABQAAAAOAAAAEwAAABYAAAAAEQAAAAAAEAAAAQAABQAAAAAKAAAADgAAABIAAAANAAAAAA4AAAATAAAADgAAABUAAAAMAAAAABEAAAAWAAAADgAAAA8AAAAPAAAAABEAAAAAABAAAAEAAAUAAAAAFwAAABkAAAAMAAAAABEAAAAWAAAADgAAABAAAAAPAAAAABEAAAAAABAAAAEAAAUAAAAAGAAAABv#####AAAAAQAIQ1NlZ21lbnQAAAAAEQEAAAAAEAAAAQAAAAEAAAAXAAAAGgAAABcAAAAAEQEAAAAAEAAAAQAAAAEAAAAYAAAAHAAAAAQAAAAAEQEAAAAACwABVwDAFAAAAAAAAMA0AAAAAAAAAAAFAAE#3FZ4mrzfDgAAAB3#####AAAAAgAIQ01lc3VyZVgAAAAAEQAGeENvb3JkAAAACgAAAB8AAAARAAAAABEABWFic3cxAAZ4Q29vcmQAAAAOAAAAIP####8AAAACABJDTGlldU9iamV0UGFyUHRMaWUBAAAAEQBmZmYAAAAAAB8AAAAOAAAADwAAAB8AAAACAAAAHwAAAB8AAAARAAAAABEABWFic3cyAA0yKmFic29yLWFic3cxAAAADQEAAAANAgAAAAFAAAAAAAAAAAAAAA4AAAASAAAADgAAACEAAAAWAAAAABEBAAAAABAAAAEAAAUAAAAACgAAAA4AAAAjAAAADgAAABMAAAAZAQAAABEAZmZmAAAAAAAkAAAADgAAAA8AAAAfAAAABQAAAB8AAAAgAAAAIQAAACMAAAAkAAAABAAAAAARAQAAAAALAAFSAEAgAAAAAAAAwCAAAAAAAAAAAAUAAT#RG06BtOgfAAAAHv####8AAAACAAhDTWVzdXJlWQAAAAARAAZ5Q29vcmQAAAAKAAAAJgAAABEAAAAAEQAFb3JkcjEABnlDb29yZAAAAA4AAAAnAAAAGQEAAAARAGZmZgAAAAAAJgAAAA4AAAAQAAAAJgAAAAIAAAAmAAAAJgAAABEAAAAAEQAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABMAAAAOAAAAKAAAABYAAAAAEQEAAAAAEAAAAQAABQAAAAAKAAAADgAAABIAAAAOAAAAKgAAABkBAAAAEQBmZmYAAAAAACsAAAAOAAAAEAAAACYAAAAFAAAAJgAAACcAAAAoAAAAKgAAACv#####AAAAAgAMQ0NvbW1lbnRhaXJlAAAAABEBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAAAB8LAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MSkAAAAZAQAAABEAZmZmAAAAAAAtAAAADgAAAA8AAAAfAAAABAAAAB8AAAAgAAAAIQAAAC0AAAAbAAAAABEBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAAACQLAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MikAAAAZAQAAABEAZmZmAAAAAAAvAAAADgAAAA8AAAAfAAAABgAAAB8AAAAgAAAAIQAAACMAAAAkAAAALwAAABsAAAAAEQFmZmYAwCAAAAAAAAA#8AAAAAAAAAAAAAAAJgsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKG9yZHIxKQAAABkBAAAAEQBmZmYAAAAAADEAAAAOAAAAEAAAACYAAAAEAAAAJgAAACcAAAAoAAAAMQAAABsAAAAAEQFmZmYAwBwAAAAAAAAAAAAAAAAAAAAAAAAAKwsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKG9yZHIyKQAAABkBAAAAEQBmZmYAAAAAADMAAAAOAAAAEAAAACYAAAAGAAAAJgAAACcAAAAoAAAAKgAAACsAAAAzAAAAEQD#####AAJ4QQABMQAAAAE#8AAAAAAAAAAAABEA#####wACeUEAATUAAAABQBQAAAAAAAAAAAARAP####8AAnhCAAEyAAAAAUAAAAAAAAAAAAAAEQD#####AAJ5QgABNgAAAAFAGAAAAAAAAAAAABEA#####wACeEMAATEAAAABP#AAAAAAAAAAAAARAP####8AAnlDAAEzAAAAAUAIAAAAAAAAAAAAEQD#####AAJ4RAABNQAAAAFAFAAAAAAAAAAAABEA#####wACeUQAATUAAAABQBQAAAAAAAAAAAAWAP####8AAAAAARAAAUEAAAAAAAAAAABACAAAAAAAAAAAAwAAAAAKAAAADgAAADUAAAAOAAAANgAAABYA#####wAAAAABEAABQgAAAAAAAAAAAEAIAAAAAAAAAAADAAAAAAoAAAAOAAAANwAAAA4AAAA4AAAAFgD#####AAAAAAEQAAFDAAAAAAAAAAAAQAgAAAAAAAAAAAMAAAAACgAAAA4AAAA5AAAADgAAADoAAAAWAP####8AAAAAARAAAUQAAAAAAAAAAABACAAAAAAAAAAAAwAAAAAKAAAADgAAADsAAAAOAAAAPP####8AAAABAAhDVmVjdGV1cgD#####AAAA#wAQAAABAAAAAQAAAD8AAABAAAAAABwA#####wAAAP8AEAAAAQAAAAEAAAA9AAAAPgAAAAAbAP####8AAAAAAAAAAAAAAAAAQAgAAAAAAAAAAAAAAD0QAAAAAAABAAAAAAAAAAEAAAAAAAAAAAADI0lBAAAAGwD#####AAAAAABAAAAAAAAAAEAQAAAAAAAAAAAAAAA+EAAAAAAAAQAAAAAAAAABAAAAAAAAAAAAAyNJQgAAABsA#####wAAAAAAv#AAAAAAAABAEAAAAAAAAAAAAAAAPxAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAMjSUMAAAAbAP####8AAAAAAEAAAAAAAAAAQBgAAAAAAAAAAAAAAEAQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAADI0lEAAAAEQD#####AAVhYnNBQgAKYWJzKHhCLXhBKf####8AAAACAAlDRm9uY3Rpb24AAAAADQEAAAAOAAAANwAAAA4AAAA1AAAAEQD#####AAVhYnNDRAAKYWJzKHhELXhDKQAAAB0AAAAADQEAAAAOAAAAOwAAAA4AAAA5AAAAEQD#####AAN4QTEAAi01#####wAAAAEADENNb2luc1VuYWlyZQAAAAFAFAAAAAAAAAAAABEA#####wADeEIxAAItNQAAAB4AAAABQBQAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAoAAAAOAAAASQAAAA4AAAA2AAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAKAAAADgAAAEoAAAAOAAAANgAAABwA#####wAAAAAAEAAAAQAAAQEAAABLAAAATAAAAAAcAP####8AAAAAABAAAAEAAAEBAAAATAAAAEsAAAAAEQD#####AAN4QzEAAi01AAAAHgAAAAFAFAAAAAAAAAAAABEA#####wADeEQxAAItNQAAAB4AAAABQBQAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAoAAAAOAAAATwAAAA4AAAA6AAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAKAAAADgAAAFAAAAAOAAAAOgAAABwA#####wAAAAAAEAAAAQAAAQEAAABRAAAAUgAAAAAcAP####8AAAAAABAAAAEAAAEBAAAAUgAAAFEAAAAAEQD#####AAN4STEACyh4QTEreEIxKS8yAAAADQMAAAANAAAAAA4AAABJAAAADgAAAEoAAAABQAAAAAAAAAAAAAARAP####8AA3lJMQACeUEAAAAOAAAANgAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACgAAAA4AAABVAAAADgAAAFb#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAP####8AAAAAAQAAAAAAVxAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAAAAAIAAABHAAAAEQD#####AAN4STIACyh4QzEreEQxKS8yAAAADQMAAAANAAAAAA4AAABPAAAADgAAAFAAAAABQAAAAAAAAAAAAAARAP####8AA3lJMgACeUMAAAAOAAAAOgAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACgAAAA4AAABZAAAADgAAAFoAAAAfAP####8AAAAAAQAAAAAAWxAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAAAAAIAAABIAAAAEQD#####AAN4QTIAAnhBAAAADgAAADUAAAARAP####8AA3lBMgACMTUAAAABQC4AAAAAAAAAAAARAP####8AA3hCMgACeEEAAAAOAAAANQAAABEA#####wADeUIyAAIxNQAAAAFALgAAAAAAAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACgAAAA4AAABdAAAADgAAAF4AAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAoAAAAOAAAAXwAAAA4AAABgAAAAHAD#####AQAAAAAQAAABAAABAQAAAGEAAABiAAAAABwA#####wEAAAAAEAAAAQAAAQEAAABiAAAAYQD#####AAAAAQAHQ01pbGlldQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABiAAAAYQAAABEA#####wAGQUJ2ZXJ0AAphYnMoeUIteUEpAAAAHQAAAAANAQAAAA4AAAA4AAAADgAAADYAAAAfAP####8AAAAAAMAQAAAAAAAAAAAAAAAAAAAAAAAAAGUQAAAAAAACAAAAAQAAAAEAAAAAAAAAAAAAAAACAAAAZgAAABEA#####wADeEMyAAJ4QwAAAA4AAAA5AAAAEQD#####AAN5QzIAAjE1AAAAAUAuAAAAAAAAAAAAEQD#####AAN4RDIAAnhDAAAADgAAADkAAAARAP####8AA3lEMgACMTUAAAABQC4AAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAoAAAAOAAAAaAAAAA4AAABpAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAKAAAADgAAAGoAAAAOAAAAawAAABwA#####wEAAAAAEAAAAQAAAQEAAABsAAAAbQAAAAAcAP####8BAAAAABAAAAEAAAEBAAAAbQAAAGwAAAAAIAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABtAAAAbAAAABEA#####wAGQ0R2ZXJ0AAphYnMoeUQteUMpAAAAHQAAAAANAQAAAA4AAAA8AAAADgAAADoAAAAfAP####8AAAAAAMAUAAAAAAAAP#AAAAAAAAAAAAAAAHAQAAAAAAACAAAAAQAAAAEAAAAAAAAAAAAAAAACAAAAcQAAABEA#####wADeEkzAAkoeEEreEIpLzIAAAANAwAAAA0AAAAADgAAADUAAAAOAAAANwAAAAFAAAAAAAAAAAAAABEA#####wADeUkzAAIxNQAAAAFALgAAAAAAAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACgAAAA4AAABzAAAADgAAAHQAAAAfAP####8AAAAAAL#wAAAAAAAAP#AAAAAAAAAAAAAAAHUQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAAAAAACAAAARwAAABEA#####wADeEk0AAkoeEMreEQpLzIAAAANAwAAAA0AAAAADgAAADkAAAAOAAAAOwAAAAFAAAAAAAAAAAAAABEA#####wADeUk0AAIxNQAAAAFALgAAAAAAAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACgAAAA4AAAB3AAAADgAAAHgAAAAfAP####8AAAAAAL#wAAAAAAAAQAgAAAAAAAAAAAAAAHkQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAAAAAACAAAASAAAAA7##########w=='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
  }
  function modifFig (st) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété correction (booléen)
    let i
    if (st.correction) {
      // on affiche la correction
      if (stor.objDonnees.horizontal) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yI3', stor.objDonnees.yA)
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yI4', stor.objDonnees.yC)
      } else if (stor.objDonnees.vertical) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yA2', stor.objDonnees.yA)
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yB2', stor.objDonnees.yB)
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yC2', stor.objDonnees.yC)
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yD2', stor.objDonnees.yD)
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xA1', stor.objDonnees.xA)
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xB1', stor.objDonnees.xB)
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xC1', stor.objDonnees.xC)
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xD1', stor.objDonnees.xD)
      }
    } else {
      // ceci sert si jamais on souhaite modifier la courbe initiale dans la question
      for (i = 0; i < 4; i++) {
        stor.mtgAppLecteur.setText(stor.mtg32svg, 67 + i, stor.objDonnees[String.fromCharCode(65 + i)])
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'x' + String.fromCharCode(65 + i), stor.objDonnees['x' + String.fromCharCode(65 + i)])
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'y' + String.fromCharCode(65 + i), stor.objDonnees['y' + String.fromCharCode(65 + i)])
      }
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }
  function distancePtDroite (pt1, pt2, pt3) {
    // distance de pt1 à la droite passant par les 2 autres points (pt2 et pt3 sont différents)
    const a = pt3[1] - pt2[1]
    const b = -(pt3[0] - pt2[0])
    const c = pt2[1] * (pt3[0] - pt2[0]) - pt2[0] * (pt3[1] - pt2[1])
    return Math.abs(a * pt1[0] + b * pt1[1] + c) / Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2))
  }
  function genereDonnees (typeCoef) {
    // Cette fonction crée un objet avec les noms des points (4 points pour cette figure) et leurs coordonnées sur la figure.
    // On obtient aussi le coefficient attendu (format numérique et latex)
    // simple est un booléen
    const tabPts = ['A', 'B', 'C', 'D']
    const nomPts = []
    const obj = {}
    let alea
    for (let i = 0; i < 8; i++) nomPts.push(String.fromCharCode(65 + i))
    for (let i = 0; i < tabPts.length; i++) {
      alea = j3pGetRandomInt(0, nomPts.length - 1)
      obj[tabPts[i]] = nomPts[alea]
      nomPts.splice(alea, 1)
    }
    // Je place les points A, B, C et D pour chercher le coef k tel que \vec{AB}=k\vec{CD}
    // Les points doivent rester dans le cadre (1;1) (13;9)
    let choixk = []
    if (typeCoef === 'entier') {
      for (let i = 2; i <= 7; i++) choixk.push([i, 1])
    } else if (typeCoef === 'simple') {
      choixk = [[1, 2], [1, 3], [2, 3], [3, 2], [1, 4], [3, 4], [4, 3], [1, 5], [1, 6]]
    } else {
      choixk = [[2, 5], [3, 5], [4, 5], [2, 7], [3, 7], [4, 7], [5, 7], [3, 8], [5, 8], [2, 9], [4, 9], [5, 9], [7, 9]]
    }
    alea = j3pGetRandomInt(0, choixk.length - 1)
    const choixSigne = 2 * j3pGetRandomInt(0, 1) - 1
    let num
    let den
    if (typeCoef === 'nonsimple') {
      const ordre = j3pGetRandomBool()
      obj.coefk = (ordre) ? choixk[alea][0] / choixk[alea][1] : choixk[alea][1] / choixk[alea][0]
      obj.k = (ordre) ? '\\frac{' + choixk[alea][0] + '}{' + choixk[alea][1] + '}' : '\\frac{' + choixk[alea][1] + '}{' + choixk[alea][0] + '}'
      num = (ordre) ? choixk[alea][0] : choixk[alea][1]
      den = (ordre) ? choixk[alea][1] : choixk[alea][0]
    } else {
      obj.coefk = choixk[alea][0] / choixk[alea][1]
      obj.k = (typeCoef === 'entier')
        ? String(obj.coefk)
        : '\\frac{' + choixk[alea][0] + '}{' + choixk[alea][1] + '}'
      num = choixk[alea][0]
      den = choixk[alea][1]
    }
    obj.k = ((choixSigne === 1) ? '' : '-') + obj.k
    let v1
    let v2
    let pt1 = []
    let pt2 = []
    let pt3 = []
    let pt4 = []
    let auxi
    do {
      v1 = [den * j3pGetRandomInt(1, Math.floor(12 / (den * num))), den * j3pGetRandomInt(1, Math.floor(8 / (den * num)))]
      v2 = v1.map(function (x) { return num * x / den })
      // console.log('v1:', v1, v2, num, den)
    } while ((v1[0] === 0 && v1[1] === 0) || v1[1] > 8 || v2[1] > 8)
    let horsCadre
    do {
      pt1 = [j3pGetRandomInt(1, 13 - v2[0]), j3pGetRandomInt(1, 9 - v2[1])]
      pt2 = [pt1[0] + v2[0], pt1[1] + v2[1]]
      pt3 = [j3pGetRandomInt(1, 13 - v1[0]), j3pGetRandomInt(1, 9 - v1[1])]
      pt4 = [pt3[0] + v1[0], pt3[1] + v1[1]]
      if (j3pGetRandomBool()) {
        auxi = pt1[1]
        pt1[1] = pt2[1]
        pt2[1] = auxi
        auxi = pt3[1]
        pt3[1] = pt4[1]
        pt4[1] = auxi
      }
      horsCadre = pt1[0] < 1 || pt1[0] > 13 || pt2[0] < 1 || pt2[0] > 13 || pt3[0] < 1 || pt3[0] > 13 || pt4[0] < 1 || pt4[0] > 13
      horsCadre = horsCadre || pt1[1] < 1 || pt1[1] > 9 || pt2[1] < 1 || pt2[1] > 9 || pt3[1] < 1 || pt3[1] > 9 || pt4[1] < 1 || pt4[1] > 9
    } while (distancePtDroite(pt3, pt1, pt2) < 2 || horsCadre)
    if (choixSigne === 1) {
      obj.xA = pt1[0]
      obj.yA = pt1[1]
      obj.xB = pt2[0]
      obj.yB = pt2[1]
      obj.xC = pt3[0]
      obj.yC = pt3[1]
      obj.xD = pt4[0]
      obj.yD = pt4[1]
    } else {
      const choixVec = j3pGetRandomBool()
      obj.xA = (choixVec) ? pt1[0] : pt2[0]
      obj.yA = (choixVec) ? pt1[1] : pt2[1]
      obj.xB = (choixVec) ? pt2[0] : pt1[0]
      obj.yB = (choixVec) ? pt2[1] : pt1[1]
      obj.xC = (choixVec) ? pt4[0] : pt3[0]
      obj.yC = (choixVec) ? pt4[1] : pt3[1]
      obj.xD = (choixVec) ? pt3[0] : pt4[0]
      obj.yD = (choixVec) ? pt3[1] : pt4[1]
    }
    obj.horizontal = (v1[1] === 0)
    obj.vertical = (v1[0] === 0)
    obj.coefk = obj.coefk * choixSigne
    // Les vecteurs seront les attributs v et w
    obj.v = obj.A + obj.B
    obj.w = obj.C + obj.D
    me.logIfDebug('v1:', v1, '  et v2:', v2, 'obj_donnees:', obj)
    return obj
  }

  function enonceInitFirst () {
    // Le paramètre définit la largeur relative de la première colonne
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })

    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    stor.typeFacteurs = []
    let j
    if (ds.coefSimples) {
      // Pour la première moitié, on a des entiers, ensuite des fractions
      for (j = 0; j < ds.nbrepetitions; j++) {
        if (j < Math.ceil(ds.nbrepetitions / 2)) {
          stor.typeFacteurs.push('entier')
        } else {
          stor.typeFacteurs.push('simple')
        }
      }
    } else {
      stor.typeFacteurs.push('entier')
      for (j = 1; j < ds.nbrepetitions; j++) {
        if (j < Math.ceil(ds.nbrepetitions / 2)) {
          stor.typeFacteurs.push('simple')
        } else {
          stor.typeFacteurs.push('nomsimple')
        }
      }
    }

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    let i
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div')

    const largFig = 458
    const hautFig = 327
    stor.zoneMtg = j3pAddElt(stor.conteneurD, 'div')
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneMtg.style.textAlign = 'center'
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(stor.zoneMtg, { id: stor.mtg32svg, width: largFig, height: hautFig })
    depart()
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    stor.objDonnees = genereDonnees(stor.typeFacteurs[me.questionCourante - 1])
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objDonnees)

    j3pAffiche(stor.zoneCons2, '', textes.consigne2, stor.objDonnees)
    const elt = j3pAffiche(stor.zoneCons3, '', '$\\vecteur{£v}=$&1&$\\vecteur{£w}$.',
      {
        v: stor.objDonnees.v,
        w: stor.objDonnees.w,
        inputmq1: {}
      })

    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d/-,.', { commandes: ['fraction'] })
    stor.zoneCons4.style.paddingTop = '5px'
    stor.zoneCons4.style.paddingBottom = '40px'
    j3pPaletteMathquill(stor.zoneCons4, stor.zoneInput, { liste: ['fraction'] })
    stor.zoneCons5.style.fontStyle = 'italic'
    stor.zoneCons5.style.fontSize = '0.9em'
    j3pAffiche(stor.zoneCons5, '', textes.comment1)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [stor.objDonnees.coefk]
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvent un tableau vide
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })
    modifFig({ correction: false })
    j3pFocus(stor.zoneInput)
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        let reponse = {}
        reponse.aRepondu = stor.fctsValid.valideReponses()
        // A cet instant reponse contient deux éléments :
        let estCalcul = false
        let estNombre = true
        if (reponse.aRepondu) {
        // il faut aussi que je teste les deux zones dont la validation est perso
        // Je vérifie que je n’ai qu’un nombre, pas un calcul
          estNombre = !isNaN(Number(j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[0])))
          estCalcul = (stor.fctsValid.zones.reponseSaisie[0].indexOf('-') > 0 && (stor.fctsValid.zones.reponseSaisie[0].replace(/\\frac{-[0-9]+}{[0-9]+}/, '') !== ''))
          reponse.aRepondu = !estCalcul && estNombre
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // On peut ajouter un commentaire particulier.
          let msgReponseManquante
          if (!estNombre) {
            msgReponseManquante = textes.comment3
          } else if (estCalcul) {
            msgReponseManquante = textes.comment2
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
          reponse = stor.fctsValid.validationGlobale()
          // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
