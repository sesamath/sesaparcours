import { j3pNombre, j3pFocus, j3pGetRandomInt, j3pRestriction, j3pAddElt, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2019
        Corrdonnées des points d’intersection de deux droite ou d’une droite avec un des axes
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

/**
 * section IntersectionDroitesAxes
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1)
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2, {
      x: stor.tabCoord[7][0],
      y: stor.tabCoord[7][1]
    })
    modifFig({ correction: true })
  }

  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV8AAADnAAAAQEAAAAAAAAAAQAAAFr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAFAAFAbSAAAAAAAEBtoUeuFHrh#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8AAAAAAQ4AAUkAwBgAAAAAAAAAAAAAAAAAAAUAAUA#AAAAAAAAAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAAEAAAAQABAAAAAQAAAAP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AAAAAAAQAAABAAEAAAABAAAABP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQUAAQAAAAcAAAAJAP####8AAAAAAQ4AAUoAwCgAAAAAAADAEAAAAAAAAAUAAgAAAAf#####AAAAAgAHQ1JlcGVyZQD#####AObm5gABAAAAAQAAAAMAAAAJAQEAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEBAAAAAAMAAAAM#####wAAAAEACUNMb25ndWV1cgD#####AAAAAQAAAA3#####AAAAAQAHQ0NhbGN1bAD#####AAduYmdyYWR4AAIyMAAAAAFANAAAAAAAAAAAABEA#####wAHbmJncmFkeQACMjAAAAABQDQAAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAUR3JhZHVhdGlvbkF4ZXNSZXBlcmUAAAAbAAAACAAAAAMAAAAKAAAADwAAABD#####AAAAAQATQ0Fic2Npc3NlT3JpZ2luZVJlcAAAAAARAAVhYnNvcgAAAAr#####AAAAAQATQ09yZG9ubmVlT3JpZ2luZVJlcAAAAAARAAVvcmRvcgAAAAoAAAALAAAAABEABnVuaXRleAAAAAr#####AAAAAQAKQ1VuaXRleVJlcAAAAAARAAZ1bml0ZXkAAAAK#####wAAAAEAEENQb2ludERhbnNSZXBlcmUAAAAAEQAAAAAAEAAAAQUAAAAACgAAAA4AAAASAAAADgAAABMAAAAWAAAAABEAAAAAABAAAAEFAAAAAAoAAAANAAAAAA4AAAASAAAADgAAABQAAAAOAAAAEwAAABYAAAAAEQAAAAAAEAAAAQUAAAAACgAAAA4AAAASAAAADQAAAAAOAAAAEwAAAA4AAAAVAAAADAAAAAARAAAAFgAAAA4AAAAPAAAADwAAAAARAAAAAAAQAAABBQAAAAAXAAAAGQAAAAwAAAAAEQAAABYAAAAOAAAAEAAAAA8AAAAAEQAAAAAAEAAAAQUAAAAAGAAAABv#####AAAAAQAIQ1NlZ21lbnQAAAAAEQEAAAAAEAAAAQABAAAAFwAAABoAAAAXAAAAABEBAAAAABAAAAEAAQAAABgAAAAcAAAABAAAAAARAQAAAAALAAFXAMAUAAAAAAAAwDQAAAAAAAAFAAE#3FZ4mrzfDgAAAB3#####AAAAAgAIQ01lc3VyZVgAAAAAEQAGeENvb3JkAAAACgAAAB8AAAARAAAAABEABWFic3cxAAZ4Q29vcmQAAAAOAAAAIP####8AAAACABJDTGlldU9iamV0UGFyUHRMaWUBAAAAEQBmZmYAAAAfAAAADgAAAA8AAAAfAAAAAgAAAB8AAAAfAAAAEQAAAAARAAVhYnN3MgANMiphYnNvci1hYnN3MQAAAA0BAAAADQIAAAABQAAAAAAAAAAAAAAOAAAAEgAAAA4AAAAhAAAAFgAAAAARAQAAAAAQAAABBQAAAAAKAAAADgAAACMAAAAOAAAAEwAAABkBAAAAEQBmZmYAAAAkAAAADgAAAA8AAAAfAAAABQAAAB8AAAAgAAAAIQAAACMAAAAkAAAABAAAAAARAQAAAAALAAFSAEAgAAAAAAAAwCAAAAAAAAAFAAE#0RtOgbToHwAAAB7#####AAAAAgAIQ01lc3VyZVkAAAAAEQAGeUNvb3JkAAAACgAAACYAAAARAAAAABEABW9yZHIxAAZ5Q29vcmQAAAAOAAAAJwAAABkBAAAAEQBmZmYAAAAmAAAADgAAABAAAAAmAAAAAgAAACYAAAAmAAAAEQAAAAARAAVvcmRyMgANMipvcmRvci1vcmRyMQAAAA0BAAAADQIAAAABQAAAAAAAAAAAAAAOAAAAEwAAAA4AAAAoAAAAFgAAAAARAQAAAAAQAAABBQAAAAAKAAAADgAAABIAAAAOAAAAKgAAABkBAAAAEQBmZmYAAAArAAAADgAAABAAAAAmAAAABQAAACYAAAAnAAAAKAAAACoAAAAr#####wAAAAIADENDb21tZW50YWlyZQAAAAARAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAHwsAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAsjVmFsKGFic3cxKQAAABkBAAAAEQBmZmYAAAAtAAAAAUA0AAAAAAAAAAAAHwAAAAQAAAAfAAAAIAAAACEAAAAtAAAAGwAAAAARAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAJAsAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAsjVmFsKGFic3cyKQAAABkBAAAAEQBmZmYAAAAvAAAAAUA0AAAAAAAAAAAAHwAAAAYAAAAfAAAAIAAAACEAAAAjAAAAJAAAAC8AAAAbAAAAABEBZmZmAMAgAAAAAAAAP#AAAAAAAAAAAAAmCwAAAAAAAgAAAAEAAAABAAAAAAAAAAAACyNWYWwob3JkcjEpAAAAGQEAAAARAGZmZgAAADEAAAABQDQAAAAAAAAAAAAmAAAABAAAACYAAAAnAAAAKAAAADEAAAAbAAAAABEBZmZmAMAcAAAAAAAAAAAAAAAAAAAAAAArCwAAAAAAAgAAAAEAAAABAAAAAAAAAAAACyNWYWwob3JkcjIpAAAAGQEAAAARAGZmZgAAADMAAAABQDQAAAAAAAAAAAAmAAAABgAAACYAAAAnAAAAKAAAACoAAAArAAAAMwAAABEA#####wACeDEAAi0y#####wAAAAEADENNb2luc1VuYWlyZQAAAAFAAAAAAAAAAAAAABEA#####wACeTIAAi0zAAAAHAAAAAFACAAAAAAAAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAADAAAAAAoAAAAOAAAANQAAAAEAAAAAAAAAAAAAABEA#####wACeDIAATIAAAABQAAAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAwAAAAAKAAAADgAAADgAAAAOAAAANgAAAAUA#####wAAAAAAEAAAAQABAAAANwAAADkAAAARAP####8AAnhBAAExAAAAAT#wAAAAAAAAAAAAEQD#####AAJ5QQAVKHhBKnkyLXgxKnkyKS8oeDIteDEpAAAADQMAAAANAQAAAA0CAAAADgAAADsAAAAOAAAANgAAAA0CAAAADgAAADUAAAAOAAAANgAAAA0BAAAADgAAADgAAAAOAAAANQAAABYA#####wAAAAAAEAABQQDAGAAAAAAAAEAIAAAAAAAAAwAAAAAKAAAADgAAADsAAAAOAAAAPAAAABEA#####wACeEIAAi03AAAAHAAAAAFAHAAAAAAAAAAAABEA#####wACeUIAEnkyKih4Qi14MSkvKHgyLXgxKQAAAA0DAAAADQIAAAAOAAAANgAAAA0BAAAADgAAAD4AAAAOAAAANQAAAA0BAAAADgAAADgAAAAOAAAANQAAABYA#####wAAAAAAEAABQgDAFAAAAAAAAEAQAAAAAAAAAwAAAAAKAAAADgAAAD4AAAAOAAAAPwAAABEA#####wACeTMAATIAAAABQAAAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAwAAAAAKAAAAAQAAAAAAAAAAAAAADgAAAEEAAAAFAP####8AAAAAABAAAAEAAQAAAEIAAAA5AAAAEQD#####AAJ4QwACLTMAAAAcAAAAAUAIAAAAAAAAAAAAEQD#####AAR0ZXN0AAV5Mj15MwAAAA0IAAAADgAAADYAAAAOAAAAQQAAABEA#####wACeUMAHXNpKHkyPXkzLHkyLCh5Mi15MykqeEMveDIreTMp#####wAAAAEADUNGb25jdGlvbjNWYXIAAAAADQgAAAAOAAAANgAAAA4AAABBAAAADgAAADYAAAANAAAAAA0DAAAADQIAAAANAQAAAA4AAAA2AAAADgAAAEEAAAAOAAAARAAAAA4AAAA4AAAADgAAAEEAAAAWAP####8AAAAAABAAAUMAwBQAAAAAAABACAAAAAAAAAMAAAAACgAAAA4AAABEAAAADgAAAEYAAAARAP####8AAnhEAAEzAAAAAUAIAAAAAAAAAAAAEQD#####AAJ5RAAdc2koeTI9eTMseTIsKHkyLXkzKSp4RC94Mit5MykAAAAdAAAAAA0IAAAADgAAADYAAAAOAAAAQQAAAA4AAAA2AAAADQAAAAANAwAAAA0CAAAADQEAAAAOAAAANgAAAA4AAABBAAAADgAAAEgAAAAOAAAAOAAAAA4AAABBAAAAFgD#####AAAAAAAQAAFEAMAUAAAAAAAAQAgAAAAAAAADAAAAAAoAAAAOAAAASAAAAA4AAABJAAAAEQD#####AAJ4RQACLTQAAAAcAAAAAUAQAAAAAAAAAAAAEQD#####AAJ5RQABNAAAAAFAEAAAAAAAAAAAABYA#####wAAfwABEAABRQDAFAAAAAAAAMA4AAAAAAAAAwAAAAAKAAAADgAAAEsAAAAOAAAATAAAABEA#####wAFdGVzdEUABXlFPj0wAAAADQcAAAAOAAAATAAAAAEAAAAAAAAAAAAAABYA#####wAAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAADAAAAAAoAAAAOAAAASwAAAAEAAAAAAAAAAP####8AAAACAAZDTGF0ZXgA#####wAAfwAAP#AAAAAAAADACAAAAAAAAAAAAE0QAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAWXElme3Rlc3RFfXtcdGV4dHtFfX17fQAAAB4A#####wAAfwAAP#AAAAAAAABAGAAAAAAAAAAAAE0QAAAAAAABAAAAAAAAAAEAAAAAAAAAAAAWXElme3Rlc3RFfXt9e1x0ZXh0e0V9fQAAABYA#####wAAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAADAAAAAAoAAAABAAAAAAAAAAAAAAAOAAAATAAAABcA#####wAAfwAAEAAAAQECAAAATQAAAFIAAAAXAP####8AAH8AABAAAAEBAgAAAE0AAABPAAAAEQD#####AAZ0ZXN0RTIABXhFPj0wAAAADQcAAAAOAAAASwAAAAEAAAAAAAAAAAAAAB4A#####wAAfwABAAAAUhAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAABdcSWZ7dGVzdEUyfXtcVmFse3lFfX17fQAAAB4A#####wAAfwABAAAAUhAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAABdcSWZ7dGVzdEUyfXt9e1xWYWx7eUV9fQAAAB4A#####wAAfwAAAAAAAAAAAADAAAAAAAAAAAAAAE8QAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAWXElme3Rlc3RFfXt9e1xWYWx7eEV9fQAAAB4A#####wAAfwABAAAATxAAAAAAAAIAAAAAAAAAAQAAAAAAAAAAABZcSWZ7dGVzdEV9e1xWYWx7eEV9fXt9AAAADv##########'
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
  }

  function modifFig (st) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété correction (booléen)
    if (st.correction) {
      // on affiche la correction
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xE', String(stor.tabCoord[7][0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yE', String(stor.tabCoord[7][1]))
    } else {
      // je cache le point utile dans la correction
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xE', '-100')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yE', '-100')
      // je modifie alors la position des points à l’origine de la construction des deux droites
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'x1', String(stor.tabCoord[0][0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'x2', String(stor.tabCoord[1][0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'y2', String(stor.tabCoord[1][1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'y3', String(stor.tabCoord[2][1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xA', String(stor.tabCoord[3][0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yA', String(stor.tabCoord[3][1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xB', String(stor.tabCoord[4][0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yB', String(stor.tabCoord[4][1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xC', String(stor.tabCoord[5][0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yC', String(stor.tabCoord[5][1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xD', String(stor.tabCoord[6][0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yD', String(stor.tabCoord[6][1]))
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 2,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
      Les textes présents dans la section
      Sont donc accessibles dans le code par donneesSection.textes.consigne1
      Possibilité d’écrire dans le code :
      var lestextes: donneesSection.textes;
      puis accès à l’aide de lestextes.consigne1
      */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Coordonnées de points d’intersection',
        // on donne les phrases de la consigne
        consigne1: 'Les droites (AB) et (CD) sont représentées ci-contre&nbsp;:',
        consigne2_1: 'Le point d’intersection de £d avec l’axe des abscisses a pour coordonnées&nbsp;: @1@.',
        consigne2_2: 'Le point d’intersection de £d avec l’axe des ordonnées a pour coordonnées&nbsp;: @1@.',
        consigne2_3: 'Le point d’intersection des droites (AB) et (CD) a pour coordonnées&nbsp;: @1@.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: "Je ne comprends pas ce qui est écrit&nbsp;!<br>Les coordonnées d’une point s'écrivent sous la forme (x;y) avec x et y nombres réels&nbsp;!",
        // et les phrases utiles pour les explications de la réponse
        corr1: 'Le point E représenté ci-contre est le point d’intersection recherché.',
        corr2: 'Ses coordonnées sont $(£x\\quad;\\quad £y)$.'
      },

      pe: 0
    }
  }
  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    stor.divMtg32 = j3pAddElt(stor.conteneurD, 'div')
    let tabCoord, x1, x2, y2, y3, pb
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      // Nouvelles droites
      do {
        let tabcoefDir, tabordOrig, donneesAcceptees, absPtGauche, absPtDroit
        do {
          tabCoord = []
          x1 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
          tabCoord[0] = [x1, 0]
          do {
            x2 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
          } while (Math.abs(x2 - x1) < Math.pow(10, -12))
          do {
            y2 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
          } while (Math.abs(Math.abs(x2) - Math.abs(y2)) < Math.pow(10, -12))
          tabCoord.push([x2, y2])
          do {
            y3 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
          } while (Math.abs(y3 - y2) < Math.pow(10, -12))
          tabCoord.push([0, y3])
          tabcoefDir = [y2 / (x2 - x1), (y3 - y2) / (0 - x2)]
          tabordOrig = [-tabcoefDir[0] * x1, y3]
          donneesAcceptees = true
          absPtGauche = []
          absPtDroit = []
          const ordPtGauche = []
          const ordPtDroit = []
          // Je cherche les coordonnées des points aux extremités du cadre pour avoir un cas de figure où ils ne sont pas trop proches l’un de l’autre
          // Je me limite aux coordonnées (-7;-7) et (7;7)
          donneesAcceptees = ((tabcoefDir[0] * tabcoefDir[1] < 0) || ((tabcoefDir[0] * tabcoefDir[1] > 0) && (Math.abs(tabcoefDir[0] - tabcoefDir[1]) > 1.5)))
          // console.log("tabcoefDir:", tabcoefDir)
          for (let i = 0; i < tabcoefDir.length; i++) {
            absPtGauche[i] = -7
            absPtDroit[i] = 7
            ordPtGauche[i] = tabcoefDir[i] * absPtGauche[i] + tabordOrig[i]
            if (ordPtGauche[i] > 7 || ordPtGauche[i] < -7) {
              // LE point le plus à gauche n’a pas pour abscisse -7, je vais donc chercher son abscisses
              absPtGauche[i] = (tabcoefDir[i] > 0) ? (-7 - tabordOrig[i]) / tabcoefDir[i] : (7 - tabordOrig[i]) / tabcoefDir[i]
            }
            ordPtDroit[i] = tabcoefDir[i] * absPtDroit[i] + tabordOrig[i]
            if (ordPtDroit[i] > 7 || ordPtDroit[i] < -7) {
              // LE point le plus à gauche n’a pas pour abscisse -7, je vais donc chercher son abscisses
              absPtDroit[i] = (tabcoefDir[i] > 0) ? (7 - tabordOrig[i]) / tabcoefDir[i] : (-7 - tabordOrig[i]) / tabcoefDir[i]
            }
            if (absPtDroit[i] - absPtGauche[i] < 9) {
              donneesAcceptees = false
            }
          }
          // console.log("absPtDroit:", absPtDroit, "  absPtGauche:", absPtGauche)
        } while (!donneesAcceptees)
        // Il me reste à placer les points A, B, C et D assez éloignés des points (x1;0), (x2;y2) et (0;y3)
        let essai = 0
        pb = false
        let xA, xB, xC, xD
        do {
          xA = absPtGauche[0] + ((0.5 + Math.random()) / 8) * (absPtDroit[0] - absPtGauche[0])
          essai++
        } while (((Math.abs(xA - x1) < 1) || (Math.abs(xA - x2) < 1)) && (essai < 50))
        if (essai === 50) {
          pb = true
        } else {
          do {
            xB = absPtGauche[0] + ((6.5 + Math.random()) / 8) * (absPtDroit[0] - absPtGauche[0])
            essai++
          } while (((Math.abs(xB - x1) < 1) || (Math.abs(xB - x2) < 1)) && (essai < 60))
          tabCoord.push([xA, xA * tabcoefDir[0] + tabordOrig[0]], [xB, xB * tabcoefDir[0] + tabordOrig[0]])
          if (essai === 60) {
            pb = true
          } else {
            do {
              xC = absPtGauche[1] + ((1 + Math.random()) / 5) * (absPtDroit[1] - absPtGauche[1])
              essai++
            } while (((Math.abs(xC) < 1) || (Math.abs(xC - x2) < 1)) && (essai < 70))
            if (essai === 70) {
              pb = true
            } else {
              do {
                xD = absPtGauche[1] + ((3 + Math.random()) / 5) * (absPtDroit[1] - absPtGauche[1])
                essai++
              } while (((Math.abs(xD) < 1) || (Math.abs(xD - x2) < 1)) && (essai < 80))
              if (essai === 80) {
                pb = true
              }
            }
          }
        }
        tabCoord.push([xC, xC * tabcoefDir[1] + tabordOrig[1]], [xD, xD * tabcoefDir[1] + tabordOrig[1]])
        stor.tabCoord = tabCoord
        // console.log("pb:",pb,"  essai:",essai)
      } while (pb)
    }
    const largFig = 470
    const // on gère ici la largeur de la figure
      hautFig = 470
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(stor.divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
    depart()
    modifFig(({ correction: false }))
    let laCons2, laReponse
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1)
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      // intersection avec l’un des axes
      const pioche = j3pGetRandomInt(0, (stor.tabAxes.length - 1))
      const leChoix = stor.tabAxes[pioche]
      stor.tabAxes.splice(pioche, 1)

      me.logIfDebug('tabAxes:', stor.tabAxes)

      laCons2 = (leChoix === 'abscisses') ? ds.textes.consigne2_1 : ds.textes.consigne2_2
      laReponse = (leChoix === 'abscisses') ? '(' + x1 + ';0)' : '(0;' + y3 + ')'
      stor.tabCoord[7] = (leChoix === 'abscisses') ? [x1, 0] : [0, y3]// Coordonnées du point E
      stor.laDroite = (leChoix === 'abscisses') ? '(AB)' : '(CD)'
    } else {
      laCons2 = ds.textes.consigne2_3
      laReponse = '(' + stor.tabCoord[1][0] + ';' + stor.tabCoord[1][1] + ')'
      stor.tabCoord[7] = [stor.tabCoord[1][0], stor.tabCoord[1][1]]// Coordonnées du point E
      stor.laDroite = ''
    }
    const elt = j3pAffiche(stor.zoneCons2, '', laCons2,
      {
        d: stor.laDroite,
        input1: { texte: '', dynamique: true, maxchars: 8 }
      })
    stor.zoneInput = elt.inputList[0]
    j3pRestriction(stor.zoneInput, '(),.;0-9\\-+')
    stor.zoneInput.typeReponse = ['texte']
    stor.zoneInput.reponse = [laReponse]
    j3pFocus(stor.zoneInput)

    me.logIfDebug('tabCoord:', stor.tabCoord)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: [stor.zoneInput.id]
    })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.5 })

        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        const tabAxes = ['abscisses', 'ordonnées']
        stor.tabAxes = []
        for (let i = 1; i < ds.nbrepetitions / 2; i++) {
          stor.tabAxes = stor.tabAxes.concat(tabAxes)
        }
        if (ds.nbrepetitions % 2 === 1) {
          stor.tabAxes.push(tabAxes[j3pGetRandomInt(0, 1)])
        }

        me.logIfDebug('tabAxes:', stor.tabAxes)
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie

      const reponse = fctsValid.validationGlobale()
      let coordBienEcrites = true
      // A cet instant reponse contient deux éléments :
      if (reponse.aRepondu) {
        const repEleve = fctsValid.zones.reponseSaisie[0]
        // On vérifie que c’est bien l’écriture des coordonnées d’un point
        // Je vire les signes + inutiles

        // const coordRegExp = new RegExp('\\((\\-|\\+)?[0-9]{1,},?[0-9]?;(\\-|\\+)?[0-9]{1,},?[0-9]?\\)', 'g')
        // capture un couple de coordonnées d’un points (coordonnées au format décimal)
        const coordRegExp = /^\((?:-?[1-9][0-9]*|0)(?:[.,][0-9]+)?;(?:-?[1-9][0-9]*|0)(?:[.,][0-9]+)?\)$/
        if (coordRegExp.test(repEleve)) {
          const nbs = repEleve.substring(1, repEleve.length - 1).split(';')
          reponse.bonneReponse = (Math.abs(stor.tabCoord[7][0] - j3pNombre(nbs[0])) < Math.pow(10, -12))
          reponse.bonneReponse = (reponse.bonneReponse && (Math.abs(stor.tabCoord[7][1] - j3pNombre(nbs[1])) < Math.pow(10, -12)))
        } else {
          reponse.bonneReponse = false
          coordBienEcrites = false
        }
        fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
        // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
        fctsValid.coloreUneZone(stor.zoneInput.id)
      }
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        me.reponseManquante(stor.zoneCorr)
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (reponse.bonneReponse) {
          me._stopTimer()
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          afficheCorrection(true)
          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()

            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.cacheBoutonValider()
            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(false)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
            // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
            if (!coordBienEcrites) {
              // c’est que l’expression entrée dans la quatrième zone de saisie est simplifiable
              stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
            }
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              afficheCorrection(false)
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
