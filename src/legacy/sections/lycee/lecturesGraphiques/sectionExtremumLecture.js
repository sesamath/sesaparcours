import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pNombre, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pRemplacePoint, j3pRestriction, j3pStyle, j3pVirgule, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import tableauSignesVariations from 'src/legacy/outils/tableauSignesVariations'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2019
        Lecture graphique ou à l’aide d’un tableau de variation d’un extremum d’une fonction
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeLecture', 'courbe', 'liste', 'On peut proposer de lire un extremum à l’aide de la courbe représentative d’une fonction ou de son tableau de variation', ['courbe', 'tableau']],
    ['situationConcrete', false, 'boolean', 'Dans le cas du tableau de variations, on peut proposer un contexte concret (la valeur du maximum sera alors à calculer).'],
    ['intervalleReduit', false, 'boolean', 'Dans un cas autre qu’une situation concrete, si ce paramètre vaut true, alors l’extremum demandé dans la dernière répétition sera un extremum local et non global.']
  ]
}

/**
 * section ExtremumLecture
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    // le style n’est pas le même suivant que la réponse soit bonne ou fausse'
    if (ds.situationConcrete) {
      j3pDetruit(stor.zoneCalc)
      j3pDetruitFenetres('Calculatrice')
    }
    if (ds.typeLecture === 'courbe') j3pDetruit(stor.zoneCons5)
    for (let i = 1; i <= 3; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    if ((ds.typeLecture === 'tableau') && ds.situationConcrete) {
      j3pAffiche(stor.zoneExpli1, '', ds.textes['corr4_' + stor.quest], {
        f: stor.nomf,
        r1: j3pVirgule(stor.zoneInput[0].reponse[0]),
        r2: j3pVirgule(stor.zoneInput[1].reponse[0]),
        y: j3pVirgule(stor.objDonnees.coordMinMax[1])
      })
    } else {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, {
        f: stor.nomf,
        x: j3pVirgule(stor.objDonnees.coordMinMax[0]),
        y: j3pVirgule(stor.objDonnees.coordMinMax[1])
      })
      const laCorr2 = (stor.objDonnees.minMax === 1) ? ds.textes.corr2_2 : ds.textes.corr2_1
      const laCorr3 = (stor.objDonnees.minMax === 1) ? ds.textes.corr3_2 : ds.textes.corr3_1
      j3pAffiche(stor.zoneExpli2, '', laCorr2, {
        f: stor.nomf,
        x: j3pVirgule(stor.objDonnees.bornesInt2[0]),
        y: j3pVirgule(stor.objDonnees.bornesInt2[1]),
        m: j3pVirgule(stor.objDonnees.coordMinMax[1])
      })
      j3pAffiche(stor.zoneExpli3, '', laCorr3, {
        f: stor.nomf,
        i: stor.monIntervalle2,
        m: j3pVirgule(stor.objDonnees.coordMinMax[1]),
        n: j3pVirgule(stor.objDonnees.coordMinMax[0])
      })
    }
    if (ds.typeLecture === 'courbe') {
      modifFig({ correction: true })
    }
  }

  function calculDonneesTab (typeQuest, numQuest) {
    function f (x) {
      if (numQuest === 1) {
        return -Math.pow(x, 3) / 3 + 10 * Math.pow(x, 2) - 36 * x - 72
      } else if (numQuest === 2) {
        return -100 * Math.pow(x, 2) + 3000 * x - 12500
      } else if (numQuest === 3) {
        return x * (longueur / 2 - x)
      } else if (numQuest === 4) {
        return Math.pow(x, 2) / 16 + Math.pow((longueur - x), 2) / 16
      }
    }

    // Pour un tableau de variations, les données peuvent être issues d’une situation concrete ou non
    let minMax = j3pGetRandomInt(1, 2)
    const valX = []
    const valY = []
    const borneInt = []
    const borneInt2 = []
    const coordMinMax = []
    const tabVariations = []
    let longueur
    if (typeQuest === 'concret') {
      let expression, posExtremum
      switch (numQuest) {
        case 1:
          minMax = 2// On a un maximum
          expression = '-\\frac{x^3}{3}+10x^2-36x-72'
          valX.push(0, 2, 18, 20)
          valY.push(-72, j3pArrondi(f(2), 2), j3pArrondi(f(18), 2), j3pArrondi(f(20), 2))
          borneInt.push(valX[0], valX[3])
          borneInt2.push(valX[0], valX[3])
          coordMinMax.push(valX[2], valY[2])
          posExtremum = 2
          tabVariations.push('decroit', 'croit', 'decroit')
          break
        case 2:
          minMax = 2// On a un maximum
          expression = '-100x^2+3000x-12500'
          valX.push(0, 15, 35)
          valY.push(-12500, j3pArrondi(f(15), 2), j3pArrondi(f(35), 2))
          borneInt.push(valX[0], valX[2])
          borneInt2.push(valX[0], valX[2])
          coordMinMax.push(valX[1], valY[1])
          posExtremum = 1
          tabVariations.push('croit', 'decroit')
          break
        case 3:
          minMax = 2// On a un maximum
          longueur = 2 * j3pGetRandomInt(0, 10) + 10
          expression = 'x(' + j3pArrondi(longueur / 2, 2) + '-x)'
          /* f = function (x) {
            return x * (longueur / 2 - x)
          } */
          valX.push(0, longueur / 4, longueur / 2)
          valY.push(0, j3pArrondi(f(longueur / 4), 2), 0)
          borneInt.push(valX[0], valX[2])
          borneInt2.push(valX[0], valX[2])
          coordMinMax.push(valX[1], valY[1])
          posExtremum = 1
          tabVariations.push('croit', 'decroit')
          break
        case 4:
          minMax = 1// On a un minimum
          longueur = 8 * j3pGetRandomInt(3, 9)
          expression = '\\frac{x^2}{16}+\\frac{(' + j3pArrondi(longueur, 2) + '-x)^2}{16}'
          valX.push(0, longueur / 2, longueur)
          valY.push(f(0), j3pArrondi(f(longueur / 2), 2), j3pArrondi(f(longueur), 2))
          borneInt.push(valX[0], valX[2])
          borneInt2.push(valX[0], valX[2])
          coordMinMax.push(valX[1], valY[1])
          posExtremum = 1
          tabVariations.push('decroit', 'croit')
          break
      }
      return {
        bornesInt: borneInt,
        bornesInt2: borneInt2,
        valX,
        valY,
        fct: f,
        express: expression,
        minMax,
        coordMinMax,
        posExtremum,
        tabVariations,
        lg: longueur
      }
    } else {
      switch (numQuest) {
        case 1:
          valX.push(Math.floor(Math.random() * 31) * 0.5 - 10)
          valX.push(valX[0] + Math.floor(Math.random() * 15) * 0.5 + 3)
          valX.push(valX[1] + Math.floor(Math.random() * 15) * 0.5 + 3)
          if (minMax === 1) {
            // on aura un minimum
            valY.push(-3 + Math.floor(Math.random() * 37) * 0.5)
            valY.push(0)
            valY.push(-3 + Math.floor(Math.random() * 37) * 0.5)
            valY[1] = Math.min(valY[0], valY[2]) - (Math.floor(Math.random() * 8) * 0.5 + 2)
            tabVariations.push('decroit', 'croit')
          } else {
            // on aura un maximum
            valY.push(-15 + Math.floor(Math.random() * 37) * 0.5)
            valY.push(0)
            valY.push(-15 + Math.floor(Math.random() * 37) * 0.5)
            valY[1] = Math.max(valY[0], valY[2]) + (Math.floor(Math.random() * 8) * 0.5 + 2)
            tabVariations.push('croit', 'decroit')
          }
          // bornes de l’intervalle domaine
          borneInt.push(valX[0], valX[2])
          coordMinMax.push(valX[1], valY[1])
          // borne de l’intervalle sur lequel on cherche l’extremum
          borneInt2.push(valX[0], valX[2])
          break
        case 2:
          valX.push(Math.floor(Math.random() * 31) * 0.5 - 20)
          valX.push(valX[0] + Math.floor(Math.random() * 15) * 0.5 + 3)
          valX.push(valX[1] + Math.floor(Math.random() * 11) * 0.5 + 2)
          valX.push(valX[2] + Math.floor(Math.random() * 11) * 0.5 + 2)
          {
            const choixExtremum = j3pGetRandomInt(1, 2)
            if (choixExtremum === 1) {
            // on aura d’abord un maximum, puis un minimum
              valY.push(-5 + Math.floor(Math.random() * 25) * 0.5)
              valY[3] = -5 + Math.floor(Math.random() * 25) * 0.5
              valY[1] = Math.max(valY[0], valY[3]) + (Math.floor(Math.random() * 12) * 0.5 + 2)
              valY[2] = Math.min(valY[0], valY[3]) - (Math.floor(Math.random() * 12) * 0.5 + 2)
              tabVariations.push('croit', 'decroit', 'croit')
              if (minMax === 2) {
                coordMinMax.push(valX[1], valY[1])
              } else {
                coordMinMax.push(valX[2], valY[2])
              }
            } else { // on aura d’abord un minimum, puis un maximum
              valY.push(-5 + Math.floor(Math.random() * 25) * 0.5)
              valY[3] = -5 + Math.floor(Math.random() * 25) * 0.5
              valY[1] = Math.min(valY[0], valY[3]) - (Math.floor(Math.random() * 12) * 0.5 + 2)
              valY[2] = Math.max(valY[0], valY[3]) + (Math.floor(Math.random() * 12) * 0.5 + 2)
              tabVariations.push('decroit', 'croit', 'decroit')
              if (minMax === 1) {
                coordMinMax.push(valX[1], valY[1])
              } else {
                coordMinMax.push(valX[2], valY[2])
              }
            }
            borneInt.push(valX[0], valX[3])
            // borne de l’intervalle sur lequel on cherche l’extremum
            borneInt2.push(valX[0], valX[3])
          }
          break
        case 3:
          // La fonction sera décroissante, puis croissante, puis décroissante et enfin croissante
          valX.push(Math.floor(Math.random() * 31) * 0.5 - 15)
          valX.push(valX[0] + Math.floor(Math.random() * 15) * 0.5 + 3)
          valX.push(valX[1] + Math.floor(Math.random() * 11) * 0.5 + 2)
          valX.push(valX[2] + Math.floor(Math.random() * 11) * 0.5 + 2)
          valX.push(valX[3] + Math.floor(Math.random() * 11) * 0.5 + 2)
          valY.push(-5 + Math.floor(Math.random() * 25) * 0.5)
          valY[4] = -5 + Math.floor(Math.random() * 25) * 0.5
          valY[1] = Math.min(valY[0], valY[4]) - (Math.floor(Math.random() * 15) * 0.5 + 2)
          valY[2] = Math.max(valY[0], valY[4]) + (Math.floor(Math.random() * 15) * 0.5 + 2)
          do {
            valY[3] = Math.min(valY[0], valY[4]) - (Math.floor(Math.random() * 15) * 0.5 + 2)
          } while (valY[3] === valY[1])
          if (valY[1] < valY[3]) {
            coordMinMax.push(valX[1], valY[1])
          } else {
            coordMinMax.push(valX[3], valY[3])
          }
          minMax = 1
          borneInt.push(valX[0], valX[4])
          // borne de l’intervalle sur lequel on cherche l’extremum
          borneInt2.push(valX[0], valX[4])
          tabVariations.push('decroit', 'croit', 'decroit', 'croit')
          break
        case 4:
          valX.push(Math.floor(Math.random() * 31) * 0.5 - 15)
          valX.push(valX[0] + Math.floor(Math.random() * 15) * 0.5 + 3)
          valX.push(valX[1] + Math.floor(Math.random() * 11) * 0.5 + 2)
          valX.push(valX[2] + Math.floor(Math.random() * 11) * 0.5 + 2)
          valX.push(valX[3] + Math.floor(Math.random() * 11) * 0.5 + 2)
          valY.push(-5 + Math.floor(Math.random() * 25) * 0.5)
          valY[4] = -5 + Math.floor(Math.random() * 25) * 0.5
          valY[1] = Math.max(valY[0], valY[4]) + (Math.floor(Math.random() * 15) * 0.5 + 2)
          valY[2] = Math.min(valY[0], valY[4]) - (Math.floor(Math.random() * 15) * 0.5 + 2)
          do {
            valY[3] = Math.max(valY[0], valY[4]) + (Math.floor(Math.random() * 15) * 0.5 + 2)
          } while (valY[3] === valY[1])
          if (valY[1] > valY[3]) {
            coordMinMax.push(valX[1], valY[1])
          } else {
            coordMinMax.push(valX[3], valY[3])
          }
          minMax = 2
          borneInt.push(valX[0], valX[4])
          // borne de l’intervalle sur lequel on cherche l’extremum
          borneInt2.push(valX[0], valX[4])
          tabVariations.push('croit', 'decroit', 'croit', 'decroit')
          break
        default:
          valX.push(Math.floor(Math.random() * 31) * 0.5 - 15)
          valX.push(valX[0] + Math.floor(Math.random() * 15) * 0.5 + 3)
          valX.push(valX[1] + Math.floor(Math.random() * 11) * 0.5 + 2)
          valX.push(valX[2] + Math.floor(Math.random() * 11) * 0.5 + 2)
          valX.push(valX[3] + Math.floor(Math.random() * 11) * 0.5 + 2)

          if (minMax === 1) { // on a d’abord un minimum local
            valY.push(-5 + Math.floor(Math.random() * 25) * 0.5)
            valY[4] = -5 + Math.floor(Math.random() * 25) * 0.5
            valY[1] = Math.min(valY[0], valY[4]) - (Math.floor(Math.random() * 15) * 0.5 + 2)
            valY[2] = Math.max(valY[0], valY[4]) + (Math.floor(Math.random() * 15) * 0.5 + 2)
            do {
              valY[3] = Math.min(valY[0], valY[4]) - (Math.floor(Math.random() * 15) * 0.5 + 2)
            } while (valY[3] === valY[1])
            borneInt.push(valX[0], valX[4])
            if (valY[1] < valY[3]) {
              borneInt2.push(valX[2] + Math.floor(Math.random() * (valX[3] - valX[2])) * 0.5, valX[4])
              coordMinMax.push(valX[3], valY[3])
            } else {
              borneInt2.push(valX[0], valX[2] - Math.floor(Math.random() * (valX[2] - valX[1])) * 0.5)
              coordMinMax.push(valX[1], valY[1])
            }
            tabVariations.push('decroit', 'croit', 'decroit', 'croit')
          } else {
            valY.push(-5 + Math.floor(Math.random() * 25) * 0.5)
            valY[4] = -5 + Math.floor(Math.random() * 25) * 0.5
            valY[1] = Math.max(valY[0], valY[4]) + (Math.floor(Math.random() * 15) * 0.5 + 2)
            valY[2] = Math.min(valY[0], valY[4]) - (Math.floor(Math.random() * 15) * 0.5 + 2)
            do {
              valY[3] = Math.max(valY[0], valY[4]) + (Math.floor(Math.random() * 15) * 0.5 + 2)
            } while (valY[3] === valY[1])
            borneInt.push(valX[0], valX[4])
            if (valY[1] > valY[3]) {
              borneInt2.push(valX[2] + Math.floor(Math.random() * (valX[3] - valX[2])) * 0.5, valX[4])
              coordMinMax.push(valX[3], valY[3])
            } else {
              borneInt2.push(valX[0], valX[2] - Math.floor(Math.random() * (valX[2] - valX[1])) * 0.5)
              coordMinMax.push(valX[1], valY[1])
            }
            tabVariations.push('croit', 'decroit', 'croit', 'decroit')
          }
          break
      }
      return {
        valX,
        valY,
        bornesInt: borneInt,
        bornesInt2: borneInt2,
        coordMinMax,
        minMax,
        tabVariations
      }
    }
  }

  function calculDonneesGraph (numQuest) {
    let i
    const coefx = 1
    const coefy = 1
    let xMin = -5
    let xMax = 5
    let yMin = -5
    let yMax = 5
    const decalage = 0.15
    let minMax = j3pGetRandomInt(0, 1)
    const abs = []
    const ord = []
    const coefDir = []
    let borneInt2
    let numFct = 1 // numFct servira pour savoir quelle courbe afficher dans mtg32
    let coordMinMax
    let negPos
    switch (numQuest) { // on a 2 variations seulement
      case 1:
        negPos = j3pGetRandomInt(0, 1)
        abs[0] = xMin + 0.5 * j3pGetRandomInt(0, 2)
        abs[2] = xMax - 0.5 * j3pGetRandomInt(0, 2)
        abs[1] = Math.round((abs[0] + abs[2]) / 2) + (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(0, 3)
        if (negPos === 1) { // le minimum sera atteint pour une abscisse négative
          ord[0] = j3pGetRandomInt(4, 8) * 0.5
          ord[2] = j3pGetRandomInt(4, 8) * 0.5
          do {
            ord[1] = -j3pGetRandomInt(1, 4)
          } while (Math.abs(abs[1] - ord[1]) < Math.pow(10, -10))
          coefDir[0] = -0.1 * j3pGetRandomInt(4, 10)
          coefDir[1] = 0
          coefDir[2] = 0.1 * j3pGetRandomInt(4, 10)
        } else { // le minimum sera atteint pour une abscisse positive
          ord[0] = j3pGetRandomInt(7, 10) * 0.5
          ord[2] = j3pGetRandomInt(7, 10) * 0.5
          do {
            ord[1] = j3pGetRandomInt(0, 2)
          } while (Math.abs(abs[1] - ord[1]) < Math.pow(10, -10))
          coefDir[0] = -0.1 * j3pGetRandomInt(1, 5)
          coefDir[1] = 0
          coefDir[2] = 0.1 * j3pGetRandomInt(1, 5)
        }
        if (minMax === 0) {
          // on aura un maximum
          for (i = 0; i < ord.length; i++) {
            ord[i] = -ord[i]
            coefDir[i] = -coefDir[i]
          }
        }
        coordMinMax = [abs[1], ord[1]]
        ord[3] = ord[4] = ord[2]
        abs[3] = abs[4] = abs[2]
        borneInt2 = [abs[0], abs[4]]
        break
      case 2:
        negPos = j3pGetRandomInt(0, 1)
        abs[0] = xMin
        abs[3] = xMax
        do {
          abs[1] = xMin + 2 + j3pGetRandomInt(0, 3)
          abs[2] = xMax - 2 - j3pGetRandomInt(0, 3)
        } while ((abs[2] - abs[1] < 2) || (abs[2] - abs[1] > 5))
        // on aura d’abord un maximum, puis un minimum
        ord[0] = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(0, 2) / 2
        ord[3] = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(0, 2) / 2
        do {
          ord[1] = Math.ceil(Math.max(Math.abs(ord[0]), Math.abs(ord[3]))) + 2 + j3pGetRandomInt(0, 2)
        } while (Math.abs(abs[1] - ord[1]) < Math.pow(10, -10))
        do {
          ord[2] = -Math.ceil(Math.max(Math.abs(ord[0]), Math.abs(ord[3]))) - 2 - j3pGetRandomInt(0, 2)
        } while (Math.abs(abs[2] - ord[2]) < Math.pow(10, -10))
        coordMinMax = (minMax === 0) ? [abs[1], ord[1]] : [abs[2], ord[2]]
        coefDir[0] = 0.1 * j3pGetRandomInt(0, 5)
        coefDir[1] = 0
        coefDir[2] = 0
        coefDir[3] = -0.1 * j3pGetRandomInt(0, 5)
        if (negPos === 1) { // on aura d’abord un minimum, puis un maximum
          for (i = 0; i <= 3; i++) {
            ord[i] = -ord[i]
            coefDir[i] = -coefDir[i]
          }
          coordMinMax = (minMax === 1) ? [abs[1], ord[1]] : [abs[2], ord[2]]
        }
        abs[4] = abs[3]
        ord[4] = ord[3]
        borneInt2 = [abs[0], abs[4]]
        numFct = 2
        break
      default :
        negPos = j3pGetRandomInt(0, 1)
        abs[0] = xMin
        abs[4] = xMax
        {
          let pb
          do {
            abs[1] = xMin + 1.5 + j3pGetRandomInt(0, 5) / 2
            abs[2] = abs[1] + 1.5 + j3pGetRandomInt(0, 4) / 2
            abs[3] = xMax - 1.5 - j3pGetRandomInt(0, 4) / 2
            pb = (abs[3] - abs[2] < 2)
          } while (pb)
        }
        // on aura d’abord un maximum, puis un minimum, puis un autre maximum
        ord[0] = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(0, 2) / 2
        ord[4] = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(0, 2) / 2
        do {
          ord[1] = Math.ceil(Math.max(Math.abs(ord[0]), Math.abs(ord[4]))) + 2 + j3pGetRandomInt(0, 4) / 2
        } while (Math.abs(abs[1] - ord[1]) < Math.pow(10, -10))
        do {
          ord[3] = Math.ceil(Math.max(Math.abs(ord[0]), Math.abs(ord[4]))) + 2 + j3pGetRandomInt(0, 4) / 2
        } while ((Math.abs(abs[3] - ord[3]) < Math.pow(10, -10)) || (Math.abs(ord[1] - ord[3]) < 1))
        do {
          ord[2] = -Math.ceil(Math.max(Math.abs(ord[0]), Math.abs(ord[4]))) - 2 - j3pGetRandomInt(0, 4) / 2
        } while (Math.abs(abs[2] - ord[2]) < Math.pow(10, -10))
        minMax = 0
        coordMinMax = (numQuest === 5)
          ? (ord[1] > ord[3]) ? [abs[3], ord[3]] : [abs[1], ord[1]]
          : (ord[1] > ord[3]) ? [abs[1], ord[1]] : [abs[3], ord[3]]// Pour numQuest ==5, le minimum n’est pas celui sur tout le domaine
        coefDir[0] = 0.1 * j3pGetRandomInt(0, 5)
        coefDir[1] = 0
        coefDir[2] = 0
        coefDir[3] = 0
        coefDir[4] = -0.1 * j3pGetRandomInt(0, 5)
        if (negPos === 1) { // on aura d’abord un minimum, puis un maximum, puis un minimum
          for (i = 0; i <= 3; i++) {
            ord[i] = -ord[i]
            coefDir[i] = -coefDir[i]
          }
          coordMinMax = (numQuest === 5)
            ? (ord[1] < ord[3]) ? [abs[3], ord[3]] : [abs[1], ord[1]]
            : (ord[1] < ord[3]) ? [abs[1], ord[1]] : [abs[3], ord[3]]
          minMax = 1
        }
        if (numQuest === 5) {
          let newBorne
          if (negPos === 1) { // minimium
            if (ord[1] < ord[3]) {
              newBorne = Math.ceil(abs[1] + abs[2]) / 2
              borneInt2 = [newBorne, abs[4]]
            } else {
              newBorne = Math.floor(abs[2] + abs[3]) / 2
              borneInt2 = [abs[0], newBorne]
            }
          } else { // maximum
            if (ord[1] > ord[3]) {
              newBorne = Math.ceil(abs[1] + abs[2]) / 2
              borneInt2 = [newBorne, abs[4]]
            } else {
              newBorne = Math.floor(abs[2] + abs[3]) / 2
              borneInt2 = [abs[0], newBorne]
            }
          }
        } else {
          borneInt2 = [abs[0], abs[4]]
        }
        numFct = 3
        break
    }
    for (i = 0; i < abs.length; i++) {
      abs[i] = abs[i] * coefx
      ord[i] = ord[i] * coefy
    }
    xMin = xMin * coefx
    xMax = xMax * coefx
    yMin = yMin * coefy
    yMax = yMax * coefy
    return {
      abs,
      ord,
      coefDir,
      bornesInt: [abs[0], abs[4]],
      bornesInt2: borneInt2,
      numFct,
      bornesRepx: [xMin - decalage, xMax + decalage],
      bornesRepy: [yMin - decalage, yMax + decalage],
      coordMinMax,
      minMax
    }
  }

  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV8AAADnAAAAQEAAAAAAAAAAQAAASn#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAJPMQDAKAAAAAAAAAAAAAAAAAAABQABQAQAAAAAAABAeNXCj1wo9v####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAAAAQAAABAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAEOAAJJMQDAGAAAAAAAAAAAAAAAAAAABQABQECAAAAAAAAAAAAC#####wAAAAEACUNEcm9pdGVBQgD#####AQAAAAAQAAABAAEAAAABAAAAA#####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAAAAABAAAAEAAQAAAAEAAAAE#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAABAAAAAQAAAAP#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAABQAAAAb#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAEOAAJKMQDAKAAAAAAAAMAQAAAAAAAABQACAAAAB#####8AAAACAAdDUmVwZXJlAP####8A5ubmAAEAAAABAAAAAwAAAAgAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABP#AAAAAAAAAAAAABP#AAAAAAAAD#####AAAAAQAHQ0NhbGN1bAD#####AAJ4TwABNgAAAAFAGAAAAAAAAAAAAAsA#####wACeU8AATYAAAABQBgAAAAAAAD#####AAAAAQAQQ1BvaW50RGFuc1JlcGVyZQD#####AQAAAAAOAAFPAMAoAAAAAAAAQAAAAAAAAAAFAAAAAAn#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAACgAAAA0AAAALAAAACwD#####AAZ1bml0ZXgAATEAAAABP#AAAAAAAAAAAAALAP####8ABnVuaXRleQABMQAAAAE#8AAAAAAAAAAAAAwA#####wEAAAAADgABSgDAJAAAAAAAAMAgAAAAAAAABQAAAAAJAAAADQAAAAr#####AAAAAQAKQ09wZXJhdGlvbgAAAAANAAAACwAAAA0AAAAOAAAADAD#####AQAAAAEOAAFJAMAIAAAAAAAAQAgAAAAAAAAFAAAAAAkAAAAOAAAAAA0AAAAKAAAADQAAAA0AAAANAAAACwAAAAoA#####wDm5uYAAQAAAAwAAAAQAAAADwEBAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAAAAAAsA#####wAIZ3JhZG1heHkAATYAAAABQBgAAAAAAAAAAAAMAP####8BAAAAABAAAksyAEAUAAAAAAAAwCgAAAAAAAAFAAAAABEAAAABAAAAAAAAAAAAAAANAAAAEv####8AAAABAAhDU2VnbWVudAD#####AAAAAAAQAAABAAEAAAAPAAAAEwAAAAQA#####wEAAAAAEAACdzMAQBgAAAAAAADAKgAAAAAAAAUAAT#gSnkEp5BLAAAAFP####8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAADP####8AAAABAAtDUG9pbnRJbWFnZQD#####AWZmZgAQAAJ3NAAAAAAAAAAAAEAIAAAAAAAABQAAAAAVAAAAFv####8AAAACAAhDTWVzdXJlWQD#####AAN5dzQAAAARAAAAFwAAABIA#####wADeXczAAAAEQAAABUAAAALAP####8ABmdyYWR5MQABMgAAAAFAAAAAAAAAAAAAAAsA#####wAHZ3JhZHZ1ZQAabW9kKGludCh5dzMrMC41KSxncmFkeTEpPTAAAAAOCP####8AAAABAA1DRm9uY3Rpb24yVmFyBv####8AAAACAAlDRm9uY3Rpb24CAAAADgAAAAANAAAAGQAAAAE#4AAAAAAAAAAAAA0AAAAaAAAAAQAAAAAAAAAAAAAACQD#####AQAAAAAQAAABBQABAAAAB#####8AAAABAApDVW5pdGV4UmVwAP####8ABHVuaXQAAAAJ#####wAAAAEAC0NIb21vdGhldGllAP####8AAAABAAAADgMAAAABP#AAAAAAAAAAAAANAAAAHQAAABEA#####wEAAAAAEAACVyIBAQAAAAADAAAAHv####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAAfAAAACwD#####AAduYmdyYWR4AAIyMAAAAAFANAAAAAAAAAAAAAsA#####wAHbmJncmFkeQACMjAAAAABQDQAAAAAAAAAAAAFAP####8AAAAAAA4AAAEAAQAAAAwAAAAPAAAABQD#####AAAAAAAOAAABAAEAAAAMAAAAEAAAAAsA#####wAIZGl2R3JhZHgAAjEwAAAAAUAkAAAAAAAA#####wAAAAIADENDb21tZW50YWlyZQD#####AAAAAAH#####EECHUAAAAAAAQIbAUeuFHrgAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAA9jYWRyZSA0MDAgKiA0MDAAAAALAP####8ACGdyYWRtYXh4AAIxMAAAAAFAJAAAAAAAAAAAAAwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAABEAAAANAAAAJwAAAAEAAAAAAAAAAAAAAA8A#####wEAAAAAEAAAAQABAAAAEAAAACgAAAAEAP####8BAAAAABAAAXcAQAgAAAAAAADAOgAAAAAAAAUAAT#qVszBEvI3AAAAKf####8AAAACAAhDTWVzdXJlWAD#####AAJ4dwAAABEAAAAq#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQD#####AYSEhAAAAAAAAAAAAEAIAAAAAAAAAAAAKg4AAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAr#####wAAAAIAEkNMaWV1T2JqZXRQYXJQdExpZQD#####AISEhAAAACwAAAANAAAAJwAAACoAAAADAAAAKgAAACsAAAAsAAAAEQD#####AYSEhAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAKgAAABYAAAALAP####8AA3h3JwADLXh3#####wAAAAEADENNb2luc1VuYWlyZQAAAA0AAAArAAAAGgD#####AYSEhAC#8AAAAAAAAEAQAAAAAAAAAAAALg4AAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAvAAAAGwD#####AISEhAAAADAAAAANAAAAJwAAACoAAAAFAAAAKgAAACsAAAAuAAAALwAAADAAAAADAP####8BhISEARAAAAEAAQAAACoAP#AAAAAAAAAAAAAEAP####8BhISEABAAAAAAAAAAAAAAAEAIAAAAAAAABQABwBk7ZFocrAAAAAAyAAAAEAD#####AAAAKgAAABEA#####wGEhIQAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAADMAAAA0AAAADwD#####AYSEhAAQAAABAAEAAAAzAAAANQAAABsA#####wCEhIQAAAA2AAAADQAAACcAAAAqAAAABgAAACoAAAAyAAAAMwAAADQAAAA1AAAANgAAABEA#####wGEhIQAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAADMAAAAWAAAAEQD#####AYSEhAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAANQAAABYAAAAPAP####8BhISEABAAAAEAAQAAADgAAAA5AAAAGwD#####AISEhAAAADoAAAANAAAAJwAAACoAAAAIAAAAKgAAADIAAAAzAAAANAAAADUAAAA4AAAAOQAAADoAAAAPAP####8BAAAAABAAAAEAAQAAAAwAAAAoAAAABAD#####AQAAAAAQAAJ3JwDAEAAAAAAAAMA5AAAAAAAABQABP9iv+oDhiUEAAAA8AAAAGQD#####AAN4dyIAAAARAAAAPQAAAAMA#####wGEhIQBEAAAAQABAAAAPQA#8AAAAAAAAAAAAAQA#####wGEhIQAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFADYk3S8aoAAAAAD8AAAAQAP####8AAAA9AAAAEQD#####AYSEhAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAQAAAAEEAAAAPAP####8BhISEABAAAAEAAQAAAEAAAABCAAAAEQD#####AYSEhAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAQAAAABYAAAARAP####8BhISEABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAABCAAAAFgAAAA8A#####wGEhIQAEAAAAQABAAAARAAAAEUAAAAbAP####8AhISEAAAAQwAAAA4AAAAADgIAAAANAAAAJQAAAA0AAAAnAAAAAT#wAAAAAAAAAAAAPQAAAAYAAAA9AAAAPwAAAEAAAABBAAAAQgAAAEMAAAAbAP####8AhISEAAAARgAAAA4AAAAADgIAAAANAAAAJQAAAA0AAAAnAAAAAT#wAAAAAAAAAAAAPQAAAAgAAAA9AAAAPwAAAEAAAABBAAAAQgAAAEQAAABFAAAARgAAAAsA#####wAIZGl2R3JhZHkAATUAAAABQBQAAAAAAAAAAAAYAP####8BZmZmAMAiAAAAAAAAAAAAAAAAAAAAAAAVEAAB####AAAAAgAAAAEAAAABAAAAAAAAAAAACyNWYWwoeXczLDApAAAAGAD#####AWZmZgDAIAAAAAAAAL#wAAAAAAAAAAAAFxAAAAAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKHl3NCwwKQAAAAMA#####wFmZmYBEAACZDQBAAE#7MzMzMzMzQAAABUBP#AAAAAAAAAAAAAEAP####8BZmZmABAAAk0yAAAAAAAAAAAAQAgAAAAAAAAFAAHAGNkWhysCAAAAAEwAAAAQAP####8AAAAVAAAAEQD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAATQAAAE4AAAAPAP####8BZmZmABAAAAEAAQAAAE0AAABPAAAAGwD#####AHd3dwAAAFAAAAANAAAAEgAAABUAAAAGAAAAFQAAAEwAAABNAAAATgAAAE8AAABQAAAAEQD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAATQAAABYAAAARAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAABPAAAAFgAAAA8A#####wFmZmYAEAAAAQABAAAAUwAAAFIAAAAbAP####8Ad3d3AAAAVAAAAA0AAAASAAAAFQAAAAgAAAAVAAAATAAAAE0AAABOAAAATwAAAFIAAABTAAAAVAAAAA8A#####wBmZmYAEAAAAQABAAAADAAAABMAAAAEAP####8BZmZmABAAAnc1AEAYAAAAAAAAwCgAAAAAAAAFAAE#2kUv69iQTAAAAFYAAAADAP####8BZmZmARAAAAEAAQAAAFcBP#AAAAAAAAAAAAAEAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAABQABwBDZFocrAgAAAABYAAAAEAD#####AAAAVwAAABEA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAFkAAABaAAAADwD#####AWZmZgAQAAABAAEAAABZAAAAWwAAABEA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAFkAAAAWAAAAEQD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAWwAAABYAAAAPAP####8BZmZmABAAAAEAAQAAAF0AAABeAAAAGwD#####AHd3dwAAAFwAAAAOAAAAAA4CAAAADQAAAEkAAAANAAAAEgAAAAE#8AAAAAAAAAAAAFcAAAAGAAAAVwAAAFgAAABZAAAAWgAAAFsAAABcAAAAGwD#####AHd3dwAAAF8AAAAOAAAAAA4CAAAADQAAAEkAAAANAAAAEgAAAAE#8AAAAAAAAAAAAFcAAAAIAAAAVwAAAFgAAABZAAAAWgAAAFsAAABdAAAAXgAAAF8AAAALAP####8ABHhtaW4AAi01AAAAHAAAAAFAFAAAAAAAAAAAAAsA#####wAEeG1heAABNgAAAAFAGAAAAAAAAAAAAAwA#####wEAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAFAAAAABEAAAANAAAAYgAAAAEAAAAAAAAAAAAAAAwA#####wEAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAFAAAAABEAAAANAAAAYwAAAAEAAAAAAAAAAAAAAA8A#####wEAAAAAEAAAAQABAAAAZAAAAGUAAAAEAP####8BAAAAABAAAngxAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAAAAAAAAAAAAGYAAAAZAP####8AAngxAAAAEQAAAGcAAAALAP####8ABXhpbml0AAQtNC44AAAAHAAAAAFAEzMzMzMzMwAAAAsA#####wAFaW1hZ2UABTMuNTQ4AAAAAUAMYk3S8an8AAAADAD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAAAAEQAAAAEAAAAAAAAAAAAAAA0AAABq#####wAAAAIABkNMYXRleAD#####AXd3dwDAJgAAAAAAAAAAAAAAAAAAAAAAFQ4AAAAAAAIAAAABAAAAAQAAAAAAAAAAABxcSWZ7Z3JhZHZ1ZX17XFZhbHt5dzMsMH19eyB9AAAAGwD#####AHd3dwAAAGwAAAANAAAAEgAAABUAAAAEAAAAFQAAABkAAAAbAAAAbAAAAAsA#####wAKZ3JhZHZ1ZU5lZwAfbW9kKGludChhYnMoeXczKSswLjUpLGdyYWR5MSk9MAAAAA4IAAAAEwYAAAAUAgAAAA4AAAAAFAAAAAANAAAAGQAAAAE#4AAAAAAAAAAAAA0AAAAaAAAAAQAAAAAAAAAAAAAAHQD#####AXd3dwDAKAAAAAAAAAAAAAAAAAAAAAAAFw4AAAAAAAIAAAABAAAAAQAAAAAAAAAAAB9cSWZ7Z3JhZHZ1ZU5lZ317XFZhbHt5dzQsMH19eyB9AAAAGwD#####AHd3dwAAAG8AAAANAAAAEgAAABUAAAAGAAAAFQAAABcAAAAYAAAAGQAAAG4AAABvAAAACwD#####AAJyMQADMi4zAAAAAUACZmZmZmZmAAAACwD#####AAJyMgACLTMAAAAcAAAAAUAIAAAAAAAAAAAACwD#####AAJyMwAELTYuNQAAABwAAAABQBoAAAAAAAAAAAALAP####8AA210MQABMQAAAAE#8AAAAAAAAAAAAAsA#####wADbXQyAAEwAAAAAQAAAAAAAAAAAAAACwD#####AANtdDMAAi0yAAAAHAAAAAFAAAAAAAAAAAAAAAsA#####wADbXQ0AAExAAAAAT#wAAAAAAAAAAAACwD#####AANtdDUAATAAAAABAAAAAAAAAAAAAAALAP####8AA210NgABMAAAAAEAAAAAAAAAAAAAAAsA#####wADbXQ3AAQtMC41AAAAHAAAAAE#4AAAAAAAAAAAAAsA#####wADbXQ4AAExAAAAAT#wAAAAAAAAAAAACwD#####AANtdDkAATAAAAABAAAAAAAAAAAAAAALAP####8ABG10MTAAATAAAAABAAAAAAAAAAAAAAALAP####8ABG10MTEAATAAAAABAAAAAAAAAAAAAAALAP####8ABG10MTIAAi0yAAAAHAAAAAFAAAAAAAAAAAAAAAsA#####wAEYWJzMQACLTUAAAAcAAAAAUAUAAAAAAAAAAAACwD#####AARhYnMyAAItMQAAABwAAAABP#AAAAAAAAAAAAALAP####8ABGFiczMAATEAAAABP#AAAAAAAAAAAAALAP####8ABGFiczQAATMAAAABQAgAAAAAAAAAAAALAP####8ABGFiczUAATYAAAABQBgAAAAAAAAAAAALAP####8ABG9yZDEAAi00AAAAHAAAAAFAEAAAAAAAAAAAAAsA#####wAEb3JkMgABMQAAAAE#8AAAAAAAAAAAAAsA#####wAEb3JkMwACLTIAAAAcAAAAAUAAAAAAAAAAAAAACwD#####AARvcmQ0AAE0AAAAAUAQAAAAAAAAAAAACwD#####AARvcmQ1AAQtMC41AAAAHAAAAAE#4AAAAAAAAAAAAAwA#####wAAAAABEAABQQAAAAAAAAAAAEAIAAAAAAAABQAAAAARAAAADQAAAIAAAAANAAAAhQAAAAwA#####wEAAAAAEAABQgAAAAAAAAAAAEAIAAAAAAAABQAAAAARAAAADQAAAIEAAAANAAAAhgAAAAwA#####wEAAAAAEAABQwAAAAAAAAAAAEAIAAAAAAAABQAAAAARAAAADQAAAIIAAAANAAAAhwAAAAwA#####wEAAAAAEAABRAAAAAAAAAAAAEAIAAAAAAAABQAAAAARAAAADQAAAIMAAAANAAAAiAAAAAwA#####wAAAAABEAABRQAAAAAAAAAAAEAIAAAAAAAABQAAAAARAAAADQAAAIQAAAANAAAAiQAAAAsA#####wAIYWZmaWNoZTEAATEAAAABP#AAAAAAAAAAAAALAP####8ACGFmZmljaGUyAAExAAAAAT#wAAAAAAAAAAAACwD#####AAhhZmZpY2hlMwABMQAAAAE#8AAAAAAAAAAAAAsA#####wAEbXQxMwABMAAAAAEAAAAAAAAAAAAAAAsA#####wAEbXQxNAABMAAAAAEAAAAAAAAAAAAAAAsA#####wAEbXQxNQABMAAAAAEAAAAAAAAAAAAAAAsA#####wACYzEAATEAAAABP#AAAAAAAAAAAAALAP####8AAmMyAAEwAAAAAQAAAAAAAAAAAAAACwD#####AAJjMwAELTAuNgAAABwAAAABP+MzMzMzMzMAAAALAP####8ABG10MTYAAmMxAAAADQAAAJUAAAALAP####8ABG10MTcAAmMyAAAADQAAAJYAAAALAP####8ABG10MTgAAmMzAAAADQAAAJf#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wASQ291cmJlIGRlIGZvbmN0aW9uAAAAFAAAAAIAAAAHAAAAEQAAAJgAAACZAAAAmgAAAIoAAACLAAAAjAAAABkAAAAAmwAHeENvb3JkMQAAABEAAACKAAAAGQAAAACbAAd4Q29vcmQyAAAAEQAAAIsAAAAZAAAAAJsAB3hDb29yZDMAAAARAAAAjAAAABIAAAAAmwAHeUNvb3JkMQAAABEAAACKAAAAEgAAAACbAAd5Q29vcmQyAAAAEQAAAIsAAAASAAAAAJsAB3lDb29yZDMAAAARAAAAjAAAAAsAAAAAmwACYTEARyhtdDE2K210MTcpLyh4Q29vcmQyLXhDb29yZDEpXjIrMiooeUNvb3JkMS15Q29vcmQyKS8oeENvb3JkMi14Q29vcmQxKV4zAAAADgAAAAAOAwAAAA4AAAAADQAAAJgAAAANAAAAmf####8AAAABAApDUHVpc3NhbmNlAAAADgEAAAANAAAAnQAAAA0AAACcAAAAAUAAAAAAAAAAAAAADgMAAAAOAgAAAAFAAAAAAAAAAAAAAA4BAAAADQAAAJ8AAAANAAAAoAAAAB8AAAAOAQAAAA0AAACdAAAADQAAAJwAAAABQAgAAAAAAAAAAAALAAAAAJsAAmIxAEcoMiptdDE2K210MTcpLyh4Q29vcmQxLXhDb29yZDIpKzMqKHlDb29yZDIteUNvb3JkMSkvKHhDb29yZDIteENvb3JkMSleMgAAAA4AAAAADgMAAAAOAAAAAA4CAAAAAUAAAAAAAAAAAAAADQAAAJgAAAANAAAAmQAAAA4BAAAADQAAAJwAAAANAAAAnQAAAA4DAAAADgIAAAABQAgAAAAAAAAAAAAOAQAAAA0AAACgAAAADQAAAJ8AAAAfAAAADgEAAAANAAAAnQAAAA0AAACcAAAAAUAAAAAAAAAA#####wAAAAEABUNGb25jAAAAAJsAAmYxADphMSoodC14Q29vcmQxKV4zK2IxKih0LXhDb29yZDEpXjIrbXQxNioodC14Q29vcmQxKSt5Q29vcmQxAAAADgAAAAAOAAAAAA4AAAAADgIAAAANAAAAogAAAB8AAAAOAf####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAANAAAAnAAAAAFACAAAAAAAAAAAAA4CAAAADQAAAKMAAAAfAAAADgEAAAAhAAAAAAAAAA0AAACcAAAAAUAAAAAAAAAAAAAADgIAAAANAAAAmAAAAA4BAAAAIQAAAAAAAAANAAAAnAAAAA0AAACfAAF0AAAACwAAAACbAANhMTEARyhtdDE3K210MTgpLyh4Q29vcmQzLXhDb29yZDIpXjIrMiooeUNvb3JkMi15Q29vcmQzKS8oeENvb3JkMy14Q29vcmQyKV4zAAAADgAAAAAOAwAAAA4AAAAADQAAAJkAAAANAAAAmgAAAB8AAAAOAQAAAA0AAACeAAAADQAAAJ0AAAABQAAAAAAAAAAAAAAOAwAAAA4CAAAAAUAAAAAAAAAAAAAADgEAAAANAAAAoAAAAA0AAAChAAAAHwAAAA4BAAAADQAAAJ4AAAANAAAAnQAAAAFACAAAAAAAAAAAAAsAAAAAmwADYjExAEcoMiptdDE3K210MTgpLyh4Q29vcmQyLXhDb29yZDMpKzMqKHlDb29yZDMteUNvb3JkMikvKHhDb29yZDMteENvb3JkMileMgAAAA4AAAAADgMAAAAOAAAAAA4CAAAAAUAAAAAAAAAAAAAADQAAAJkAAAANAAAAmgAAAA4BAAAADQAAAJ0AAAANAAAAngAAAA4DAAAADgIAAAABQAgAAAAAAAAAAAAOAQAAAA0AAAChAAAADQAAAKAAAAAfAAAADgEAAAANAAAAngAAAA0AAACdAAAAAUAAAAAAAAAAAAAAIAAAAACbAAJmMgA8YTExKih0LXhDb29yZDIpXjMrYjExKih0LXhDb29yZDIpXjIrbXQxNyoodC14Q29vcmQyKSt5Q29vcmQyAAAADgAAAAAOAAAAAA4AAAAADgIAAAANAAAApQAAAB8AAAAOAQAAACEAAAAAAAAADQAAAJ0AAAABQAgAAAAAAAAAAAAOAgAAAA0AAACmAAAAHwAAAA4BAAAAIQAAAAAAAAANAAAAnQAAAAFAAAAAAAAAAAAAAA4CAAAADQAAAJkAAAAOAQAAACEAAAAAAAAADQAAAJ0AAAANAAAAoAABdAAAACABAAAAmwABZgAkKHQ8eENvb3JkMikqZjEodCkrKHQ+PXhDb29yZDIpKmYyKHQpAAAADgAAAAAOAgAAAA4EAAAAIQAAAAAAAAANAAAAnf####8AAAABAA5DQXBwZWxGb25jdGlvbgAAAKQAAAAhAAAAAAAAAA4CAAAADgcAAAAhAAAAAAAAAA0AAACdAAAAIgAAAKcAAAAhAAAAAAABdAAAAAwAAAAAmwAAAAAAEAAAAQUAAAAAEQAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAwAAAAAmwAAAAAAEAAAAQUAAAAAEQAAAAE#8AAAAAAAAAAAAAEAAAAAAAAAAAAAAAUAAAAAmwAAAAAADAAAAQABAAAAqQAAAKoAAAAEAAAAAJsAAAAAAAwAAXgBBQABP#vwo9cKPXMAAACrAAAAGQAAAACbAAd4Q29vcmQ0AAAAEQAAAKwAAAALAAAAAJsAAXgAB3hDb29yZDQAAAANAAAArQAAAAsAAAAAmwABeQAEZih4KQAAACIAAACoAAAADQAAAK4AAAAMAAAAAJsAAAAAABAAAAEFAAAAABEAAAANAAAArgAAAA0AAACv#####wAAAAIADUNMaWV1RGVQb2ludHMBAAAAmwEAAAAAAQAAALAAAAH0AAEAAACsAAAABQAAAKwAAACtAAAArgAAAK8AAACwAAAACwD#####AAJjNAABMAAAAAEAAAAAAAAAAAAAAAsA#####wACYzUABC0wLjUAAAAcAAAAAT#gAAAAAAAAAAAACwD#####AARtdDE5AAJjMQAAAA0AAACVAAAACwD#####AARtdDIwAAJjMgAAAA0AAACWAAAACwD#####AARtdDIxAAJjMwAAAA0AAACXAAAACwD#####AARtdDIyAAJjNAAAAA0AAACyAAAAHgD#####ABJDb3VyYmUgZGUgZm9uY3Rpb24AAAAZAAAAAgAAAAkAAAARAAAAtAAAALUAAAC2AAAAtwAAAIoAAACLAAAAjAAAAI0AAAAZAAAAALgAB3hDb29yZDEAAAARAAAAigAAABkAAAAAuAAHeENvb3JkMgAAABEAAACLAAAAGQAAAAC4AAd4Q29vcmQzAAAAEQAAAIwAAAAZAAAAALgAB3hDb29yZDQAAAARAAAAjQAAABIAAAAAuAAHeUNvb3JkMQAAABEAAACKAAAAEgAAAAC4AAd5Q29vcmQyAAAAEQAAAIsAAAASAAAAALgAB3lDb29yZDMAAAARAAAAjAAAABIAAAAAuAAHeUNvb3JkNAAAABEAAACNAAAACwAAAAC4AAJhMQBHKG10MTkrbXQyMCkvKHhDb29yZDIteENvb3JkMSleMisyKih5Q29vcmQxLXlDb29yZDIpLyh4Q29vcmQyLXhDb29yZDEpXjMAAAAOAAAAAA4DAAAADgAAAAANAAAAtAAAAA0AAAC1AAAAHwAAAA4BAAAADQAAALoAAAANAAAAuQAAAAFAAAAAAAAAAAAAAA4DAAAADgIAAAABQAAAAAAAAAAAAAAOAQAAAA0AAAC9AAAADQAAAL4AAAAfAAAADgEAAAANAAAAugAAAA0AAAC5AAAAAUAIAAAAAAAAAAAACwAAAAC4AAJiMQBHKDIqbXQxOSttdDIwKS8oeENvb3JkMS14Q29vcmQyKSszKih5Q29vcmQyLXlDb29yZDEpLyh4Q29vcmQyLXhDb29yZDEpXjIAAAAOAAAAAA4DAAAADgAAAAAOAgAAAAFAAAAAAAAAAAAAAA0AAAC0AAAADQAAALUAAAAOAQAAAA0AAAC5AAAADQAAALoAAAAOAwAAAA4CAAAAAUAIAAAAAAAAAAAADgEAAAANAAAAvgAAAA0AAAC9AAAAHwAAAA4BAAAADQAAALoAAAANAAAAuQAAAAFAAAAAAAAAAAAAACAAAAAAuAACZjEAOmExKih0LXhDb29yZDEpXjMrYjEqKHQteENvb3JkMSleMittdDE5Kih0LXhDb29yZDEpK3lDb29yZDEAAAAOAAAAAA4AAAAADgAAAAAOAgAAAA0AAADBAAAAHwAAAA4BAAAAIQAAAAAAAAANAAAAuQAAAAFACAAAAAAAAAAAAA4CAAAADQAAAMIAAAAfAAAADgEAAAAhAAAAAAAAAA0AAAC5AAAAAUAAAAAAAAAAAAAADgIAAAANAAAAtAAAAA4BAAAAIQAAAAAAAAANAAAAuQAAAA0AAAC9AAF0AAAACwAAAAC4AANhMTEARyhtdDIwK210MjEpLyh4Q29vcmQzLXhDb29yZDIpXjIrMiooeUNvb3JkMi15Q29vcmQzKS8oeENvb3JkMy14Q29vcmQyKV4zAAAADgAAAAAOAwAAAA4AAAAADQAAALUAAAANAAAAtgAAAB8AAAAOAQAAAA0AAAC7AAAADQAAALoAAAABQAAAAAAAAAAAAAAOAwAAAA4CAAAAAUAAAAAAAAAAAAAADgEAAAANAAAAvgAAAA0AAAC#AAAAHwAAAA4BAAAADQAAALsAAAANAAAAugAAAAFACAAAAAAAAAAAAAsAAAAAuAADYjExAEcoMiptdDIwK210MjEpLyh4Q29vcmQyLXhDb29yZDMpKzMqKHlDb29yZDMteUNvb3JkMikvKHhDb29yZDMteENvb3JkMileMgAAAA4AAAAADgMAAAAOAAAAAA4CAAAAAUAAAAAAAAAAAAAADQAAALUAAAANAAAAtgAAAA4BAAAADQAAALoAAAANAAAAuwAAAA4DAAAADgIAAAABQAgAAAAAAAAAAAAOAQAAAA0AAAC#AAAADQAAAL4AAAAfAAAADgEAAAANAAAAuwAAAA0AAAC6AAAAAUAAAAAAAAAAAAAAIAAAAAC4AAJmMgA8YTExKih0LXhDb29yZDIpXjMrYjExKih0LXhDb29yZDIpXjIrbXQyMCoodC14Q29vcmQyKSt5Q29vcmQyAAAADgAAAAAOAAAAAA4AAAAADgIAAAANAAAAxAAAAB8AAAAOAQAAACEAAAAAAAAADQAAALoAAAABQAgAAAAAAAAAAAAOAgAAAA0AAADFAAAAHwAAAA4BAAAAIQAAAAAAAAANAAAAugAAAAFAAAAAAAAAAAAAAA4CAAAADQAAALUAAAAOAQAAACEAAAAAAAAADQAAALoAAAANAAAAvgABdAAAAAsAAAAAuAADYTEyAEcobXQyMSttdDIyKS8oeENvb3JkNC14Q29vcmQzKV4yKzIqKHlDb29yZDMteUNvb3JkNCkvKHhDb29yZDQteENvb3JkMyleMwAAAA4AAAAADgMAAAAOAAAAAA0AAAC2AAAADQAAALcAAAAfAAAADgEAAAANAAAAvAAAAA0AAAC7AAAAAUAAAAAAAAAAAAAADgMAAAAOAgAAAAFAAAAAAAAAAAAAAA4BAAAADQAAAL8AAAANAAAAwAAAAB8AAAAOAQAAAA0AAAC8AAAADQAAALsAAAABQAgAAAAAAAAAAAALAAAAALgAA2IxMgBHKDIqbXQyMSttdDIyKS8oeENvb3JkMy14Q29vcmQ0KSszKih5Q29vcmQ0LXlDb29yZDMpLyh4Q29vcmQ0LXhDb29yZDMpXjIAAAAOAAAAAA4DAAAADgAAAAAOAgAAAAFAAAAAAAAAAAAAAA0AAAC2AAAADQAAALcAAAAOAQAAAA0AAAC7AAAADQAAALwAAAAOAwAAAA4CAAAAAUAIAAAAAAAAAAAADgEAAAANAAAAwAAAAA0AAAC#AAAAHwAAAA4BAAAADQAAALwAAAANAAAAuwAAAAFAAAAAAAAAAAAAACAAAAAAuAACZjMAPGExMioodC14Q29vcmQzKV4zK2IxMioodC14Q29vcmQzKV4yK210MjEqKHQteENvb3JkMykreUNvb3JkMwAAAA4AAAAADgAAAAAOAAAAAA4CAAAADQAAAMcAAAAfAAAADgEAAAAhAAAAAAAAAA0AAAC7AAAAAUAIAAAAAAAAAAAADgIAAAANAAAAyAAAAB8AAAAOAQAAACEAAAAAAAAADQAAALsAAAABQAAAAAAAAAAAAAAOAgAAAA0AAAC2AAAADgEAAAAhAAAAAAAAAA0AAAC7AAAADQAAAL8AAXQAAAAgAQAAALgAAmYxAEModDx4Q29vcmQyKSpmMSh0KSsodD49eENvb3JkMikqKHQ8eENvb3JkMykqZjIodCkrKHQ+PXhDb29yZDMpKmYzKHQpAAAADgAAAAAOAAAAAA4CAAAADgQAAAAhAAAAAAAAAA0AAAC6AAAAIgAAAMMAAAAhAAAAAAAAAA4CAAAADgIAAAAOBwAAACEAAAAAAAAADQAAALoAAAAOBAAAACEAAAAAAAAADQAAALsAAAAiAAAAxgAAACEAAAAAAAAADgIAAAAOBwAAACEAAAAAAAAADQAAALsAAAAiAAAAyQAAACEAAAAAAAF0AAAADAAAAAC4AAAAAAAQAAABBQAAAAARAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAADAAAAAC4AAAAAAAQAAABBQAAAAARAAAAAT#wAAAAAAAAAAAAAQAAAAAAAAAAAAAABQAAAAC4AAAAAAAMAAABAAEAAADLAAAAzAAAAAQAAAAAuAAAAAAADAABeAEFAAE#+#Cj1wo9cwAAAM0AAAAZAAAAALgAB3hDb29yZDUAAAARAAAAzgAAAAsAAAAAuAABeAAHeENvb3JkNQAAAA0AAADPAAAACwAAAAC4AAF5AAVmMSh4KQAAACIAAADKAAAADQAAANAAAAAMAAAAALgAAAAAABAAAAEFAAAAABEAAAANAAAA0AAAAA0AAADRAAAAIwEAAAC4AQAAAAABAAAA0gAAAfQAAQAAAM4AAAAFAAAAzgAAAM8AAADQAAAA0QAAANIAAAALAP####8ABG10MjMAAmMxAAAADQAAAJUAAAALAP####8ABG10MjQAAmMyAAAADQAAAJYAAAALAP####8ABG10MjUAAmMzAAAADQAAAJcAAAALAP####8ABG10MjYAAmM0AAAADQAAALIAAAALAP####8ABG10MjcAAmM1AAAADQAAALMAAAAeAP####8AEkNvdXJiZSBkZSBmb25jdGlvbgAAAB4AAAACAAAACwAAABEAAADUAAAA1QAAANYAAADXAAAA2AAAAIoAAACLAAAAjAAAAI0AAACOAAAAGQAAAADZAAd4Q29vcmQxAAAAEQAAAIoAAAAZAAAAANkAB3hDb29yZDIAAAARAAAAiwAAABkAAAAA2QAHeENvb3JkMwAAABEAAACMAAAAGQAAAADZAAd4Q29vcmQ0AAAAEQAAAI0AAAAZAAAAANkAB3hDb29yZDUAAAARAAAAjgAAABIAAAAA2QAHeUNvb3JkMQAAABEAAACKAAAAEgAAAADZAAd5Q29vcmQyAAAAEQAAAIsAAAASAAAAANkAB3lDb29yZDMAAAARAAAAjAAAABIAAAAA2QAHeUNvb3JkNAAAABEAAACNAAAAEgAAAADZAAd5Q29vcmQ1AAAAEQAAAI4AAAALAAAAANkAAmExAEcobXQyMyttdDI0KS8oeENvb3JkMi14Q29vcmQxKV4yKzIqKHlDb29yZDEteUNvb3JkMikvKHhDb29yZDIteENvb3JkMSleMwAAAA4AAAAADgMAAAAOAAAAAA0AAADUAAAADQAAANUAAAAfAAAADgEAAAANAAAA2wAAAA0AAADaAAAAAUAAAAAAAAAAAAAADgMAAAAOAgAAAAFAAAAAAAAAAAAAAA4BAAAADQAAAN8AAAANAAAA4AAAAB8AAAAOAQAAAA0AAADbAAAADQAAANoAAAABQAgAAAAAAAAAAAALAAAAANkAAmIxAEcoMiptdDIzK210MjQpLyh4Q29vcmQxLXhDb29yZDIpKzMqKHlDb29yZDIteUNvb3JkMSkvKHhDb29yZDIteENvb3JkMSleMgAAAA4AAAAADgMAAAAOAAAAAA4CAAAAAUAAAAAAAAAAAAAADQAAANQAAAANAAAA1QAAAA4BAAAADQAAANoAAAANAAAA2wAAAA4DAAAADgIAAAABQAgAAAAAAAAAAAAOAQAAAA0AAADgAAAADQAAAN8AAAAfAAAADgEAAAANAAAA2wAAAA0AAADaAAAAAUAAAAAAAAAAAAAAIAAAAADZAAJmMgA6YTEqKHQteENvb3JkMSleMytiMSoodC14Q29vcmQxKV4yK210MjMqKHQteENvb3JkMSkreUNvb3JkMQAAAA4AAAAADgAAAAAOAAAAAA4CAAAADQAAAOQAAAAfAAAADgEAAAAhAAAAAAAAAA0AAADaAAAAAUAIAAAAAAAAAAAADgIAAAANAAAA5QAAAB8AAAAOAQAAACEAAAAAAAAADQAAANoAAAABQAAAAAAAAAAAAAAOAgAAAA0AAADUAAAADgEAAAAhAAAAAAAAAA0AAADaAAAADQAAAN8AAXQAAAALAAAAANkAA2ExMQBHKG10MjQrbXQyNSkvKHhDb29yZDMteENvb3JkMileMisyKih5Q29vcmQyLXlDb29yZDMpLyh4Q29vcmQzLXhDb29yZDIpXjMAAAAOAAAAAA4DAAAADgAAAAANAAAA1QAAAA0AAADWAAAAHwAAAA4BAAAADQAAANwAAAANAAAA2wAAAAFAAAAAAAAAAAAAAA4DAAAADgIAAAABQAAAAAAAAAAAAAAOAQAAAA0AAADgAAAADQAAAOEAAAAfAAAADgEAAAANAAAA3AAAAA0AAADbAAAAAUAIAAAAAAAAAAAACwAAAADZAANiMTEARygyKm10MjQrbXQyNSkvKHhDb29yZDIteENvb3JkMykrMyooeUNvb3JkMy15Q29vcmQyKS8oeENvb3JkMy14Q29vcmQyKV4yAAAADgAAAAAOAwAAAA4AAAAADgIAAAABQAAAAAAAAAAAAAANAAAA1QAAAA0AAADWAAAADgEAAAANAAAA2wAAAA0AAADcAAAADgMAAAAOAgAAAAFACAAAAAAAAAAAAA4BAAAADQAAAOEAAAANAAAA4AAAAB8AAAAOAQAAAA0AAADcAAAADQAAANsAAAABQAAAAAAAAAAAAAAgAAAAANkAAmYyADxhMTEqKHQteENvb3JkMileMytiMTEqKHQteENvb3JkMileMittdDI0Kih0LXhDb29yZDIpK3lDb29yZDIAAAAOAAAAAA4AAAAADgAAAAAOAgAAAA0AAADnAAAAHwAAAA4BAAAAIQAAAAAAAAANAAAA2wAAAAFACAAAAAAAAAAAAA4CAAAADQAAAOgAAAAfAAAADgEAAAAhAAAAAAAAAA0AAADbAAAAAUAAAAAAAAAAAAAADgIAAAANAAAA1QAAAA4BAAAAIQAAAAAAAAANAAAA2wAAAA0AAADgAAF0AAAACwAAAADZAANhMTIARyhtdDI1K210MjYpLyh4Q29vcmQ0LXhDb29yZDMpXjIrMiooeUNvb3JkMy15Q29vcmQ0KS8oeENvb3JkNC14Q29vcmQzKV4zAAAADgAAAAAOAwAAAA4AAAAADQAAANYAAAANAAAA1wAAAB8AAAAOAQAAAA0AAADdAAAADQAAANwAAAABQAAAAAAAAAAAAAAOAwAAAA4CAAAAAUAAAAAAAAAAAAAADgEAAAANAAAA4QAAAA0AAADiAAAAHwAAAA4BAAAADQAAAN0AAAANAAAA3AAAAAFACAAAAAAAAAAAAAsAAAAA2QADYjEyAEcoMiptdDI1K210MjYpLyh4Q29vcmQzLXhDb29yZDQpKzMqKHlDb29yZDQteUNvb3JkMykvKHhDb29yZDQteENvb3JkMyleMgAAAA4AAAAADgMAAAAOAAAAAA4CAAAAAUAAAAAAAAAAAAAADQAAANYAAAANAAAA1wAAAA4BAAAADQAAANwAAAANAAAA3QAAAA4DAAAADgIAAAABQAgAAAAAAAAAAAAOAQAAAA0AAADiAAAADQAAAOEAAAAfAAAADgEAAAANAAAA3QAAAA0AAADcAAAAAUAAAAAAAAAAAAAAIAAAAADZAAJmMwA8YTEyKih0LXhDb29yZDMpXjMrYjEyKih0LXhDb29yZDMpXjIrbXQyNSoodC14Q29vcmQzKSt5Q29vcmQzAAAADgAAAAAOAAAAAA4AAAAADgIAAAANAAAA6gAAAB8AAAAOAQAAACEAAAAAAAAADQAAANwAAAABQAgAAAAAAAAAAAAOAgAAAA0AAADrAAAAHwAAAA4BAAAAIQAAAAAAAAANAAAA3AAAAAFAAAAAAAAAAAAAAA4CAAAADQAAANYAAAAOAQAAACEAAAAAAAAADQAAANwAAAANAAAA4QABdAAAAAsAAAAA2QAEYTEyMQBHKG10MjYrbXQyNykvKHhDb29yZDUteENvb3JkNCleMisyKih5Q29vcmQ0LXlDb29yZDUpLyh4Q29vcmQ1LXhDb29yZDQpXjMAAAAOAAAAAA4DAAAADgAAAAANAAAA1wAAAA0AAADYAAAAHwAAAA4BAAAADQAAAN4AAAANAAAA3QAAAAFAAAAAAAAAAAAAAA4DAAAADgIAAAABQAAAAAAAAAAAAAAOAQAAAA0AAADiAAAADQAAAOMAAAAfAAAADgEAAAANAAAA3gAAAA0AAADdAAAAAUAIAAAAAAAAAAAACwAAAADZAARiMTIxAEcoMiptdDI2K210MjcpLyh4Q29vcmQ0LXhDb29yZDUpKzMqKHlDb29yZDUteUNvb3JkNCkvKHhDb29yZDUteENvb3JkNCleMgAAAA4AAAAADgMAAAAOAAAAAA4CAAAAAUAAAAAAAAAAAAAADQAAANcAAAANAAAA2AAAAA4BAAAADQAAAN0AAAANAAAA3gAAAA4DAAAADgIAAAABQAgAAAAAAAAAAAAOAQAAAA0AAADjAAAADQAAAOIAAAAfAAAADgEAAAANAAAA3gAAAA0AAADdAAAAAUAAAAAAAAAAAAAAIAAAAADZAAJmNAA+YTEyMSoodC14Q29vcmQ0KV4zK2IxMjEqKHQteENvb3JkNCleMittdDI2Kih0LXhDb29yZDQpK3lDb29yZDQAAAAOAAAAAA4AAAAADgAAAAAOAgAAAA0AAADtAAAAHwAAAA4BAAAAIQAAAAAAAAANAAAA3QAAAAFACAAAAAAAAAAAAA4CAAAADQAAAO4AAAAfAAAADgEAAAAhAAAAAAAAAA0AAADdAAAAAUAAAAAAAAAAAAAADgIAAAANAAAA1wAAAA4BAAAAIQAAAAAAAAANAAAA3QAAAA0AAADiAAF0AAAAIAEAAADZAAJmMgBiKHQ8eENvb3JkMikqZjIodCkrKHQ+PXhDb29yZDIpKih0PHhDb29yZDMpKmYyKHQpKyh0Pj14Q29vcmQzKSoodDx4Q29vcmQ0KSpmMyh0KSsodD49eENvb3JkNCkqZjQodCkAAAAOAAAAAA4AAAAADgAAAAAOAgAAAA4EAAAAIQAAAAAAAAANAAAA2wAAACIAAADmAAAAIQAAAAAAAAAOAgAAAA4CAAAADgcAAAAhAAAAAAAAAA0AAADbAAAADgQAAAAhAAAAAAAAAA0AAADcAAAAIgAAAOkAAAAhAAAAAAAAAA4CAAAADgIAAAAOBwAAACEAAAAAAAAADQAAANwAAAAOBAAAACEAAAAAAAAADQAAAN0AAAAiAAAA7AAAACEAAAAAAAAADgIAAAAOBwAAACEAAAAAAAAADQAAAN0AAAAiAAAA7wAAACEAAAAAAAF0AAAADAAAAADZAAAAAAAQAAABBQAAAAARAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAADAAAAADZAAAAAAAQAAABBQAAAAARAAAAAT#wAAAAAAAAAAAAAQAAAAAAAAAAAAAABQAAAADZAAAAAAAMAAABAAEAAADxAAAA8gAAAAQAAAAA2QAAAAAADAABeAEFAAE#+#Cj1wo9cwAAAPMAAAAZAAAAANkAB3hDb29yZDYAAAARAAAA9AAAAAsAAAAA2QABeAAHeENvb3JkNgAAAA0AAAD1AAAACwAAAADZAAF5AAVmMih4KQAAACIAAADwAAAADQAAAPYAAAAMAAAAANkAAAAAABAAAAEFAAAAABEAAAANAAAA9gAAAA0AAAD3AAAAIwEAAADZAQAAAAABAAAA+AAAAfQAAQAAAPQAAAAFAAAA9AAAAPUAAAD2AAAA9wAAAPgAAAAgAP####8AAmcxAA8xL2FmZmljaGUxKmYoeCkAAAAOAgAAAA4DAAAAAT#wAAAAAAAAAAAADQAAAI8AAAAiAAAAqAAAACEAAAAAAAF4AAAAIAD#####AAJnMgAQMS9hZmZpY2hlMipmMSh4KQAAAA4CAAAADgMAAAABP#AAAAAAAAAAAAANAAAAkAAAACIAAADKAAAAIQAAAAAAAXgAAAAgAP####8AAmczABAxL2FmZmljaGUzKmYyKHgpAAAADgIAAAAOAwAAAAE#8AAAAAAAAAAAAA0AAACRAAAAIgAAAPAAAAAhAAAAAAABeAAAAAwA#####wEAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAFAAAAABEAAAANAAAAgAAAAAEAAAAAAAAAAAAAAAwA#####wEAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAFAAAAABEAAAANAAAAggAAAAEAAAAAAAAAAAAAAA8A#####wEAAAAAEAAAAQABAAAA#QAAAP4AAAAEAP####8BAAAAABAAAngyAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAAAAAAAAAAAAP8AAAAZAP####8AAngyAAAAEQAAAQAAAAALAP####8AAnkxAAZnMSh4MikAAAAiAAAA+gAAAA0AAAEBAAAADAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAEQAAAA0AAAEBAAAADQAAAQIAAAAjAP####8AAAAAAAEAAAEDAAAB9AABAAABAAAAAAQAAAEAAAABAQAAAQIAAAEDAAAADAD#####AQAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAUAAAAAEQAAAA0AAACDAAAAAQAAAAAAAAAAAAAADwD#####AQAAAAAQAAABAAEAAAD9AAABBQAAAAQA#####wEAAAAAEAACeDMAAAAAAAAAAABACAAAAAAAAAUAAQAAAAAAAAAAAAABBgAAABkA#####wACeDMAAAARAAABBwAAAAsA#####wACeTIABmcyKHgzKQAAACIAAAD7AAAADQAAAQgAAAAMAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAARAAAADQAAAQgAAAANAAABCQAAACMA#####wAAAAAAAQAAAQoAAAH0AAEAAAEHAAAABAAAAQcAAAEIAAABCQAAAQoAAAAMAP####8BAAAAABAAAABACAAAAAAAAAAAAAAAAAAABQAAAAARAAAADQAAAIQAAAABAAAAAAAAAAAAAAAPAP####8BAAAAABAAAAEAAQAAAP0AAAEMAAAABAD#####AQAAAAAQAAJ4NAAAAAAAAAAAAEAIAAAAAAAABQABAAAAAAAAAAAAAAENAAAAGQD#####AAJ4NAAAABEAAAEOAAAACwD#####AAJ5MwAGZzMoeDQpAAAAIgAAAPwAAAANAAABDwAAAAwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAABEAAAANAAABDwAAAA0AAAEQAAAAIwD#####AAAAAAABAAABEQAAAfQAAQAAAQ4AAAAEAAABDgAAAQ8AAAEQAAABEQAAAAsA#####wAEeEV4dAABMgAAAAFAAAAAAAAAAAAAAAsA#####wAEeUV4dAABMwAAAAFACAAAAAAAAAAAAAwA#####wAAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAABEAAAANAAABEwAAAA0AAAEUAAAADAD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAAAAEQAAAA0AAAETAAAAAQAAAAAAAAAAAAAADAD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAAAAEQAAAAEAAAAAAAAAAAAAAA0AAAEUAAAADwD#####AAB#AAAQAAABAAIAAAEVAAABFwAAAA8A#####wAAfwAAEAAAAQACAAABFQAAARYAAAALAP####8ABHJlcHgAAi0yAAAAHAAAAAFAAAAAAAAAAAAAAAsA#####wAEcmVweQACLTQAAAAcAAAAAUAQAAAAAAAAAAAACwD#####AAV0ZXN0MQAHeEV4dD49MAAAAA4HAAAADQAAARMAAAABAAAAAAAAAAAAAAALAP####8ABXRlc3QyAAZ4RXh0PDAAAAAOBAAAAA0AAAETAAAAAQAAAAAAAAAAAAAACwD#####AAV0ZXN0MwAHeUV4dD49MAAAAA4HAAAADQAAARQAAAABAAAAAAAAAAAAAAALAP####8ABXRlc3Q0AAZ5RXh0PDAAAAAOBAAAAA0AAAEUAAAAAQAAAAAAAAAAAAAAHQD#####AAB#AADANgAAAAAAAAAAAAAAAAAAAAABFxAAAAAAAAIAAAABAAAAAQAAAAAAAAAAABhcSWZ7dGVzdDF9e1xWYWx7cmVweX19e30AAAAdAP####8AAH8AAMAAAAAAAAAAQDYAAAAAAAAAAAEWEAAAAAAAAQAAAAAAAAABAAAAAAAAAAAAGFxJZnt0ZXN0M317XFZhbHtyZXB4fX17fQAAAB0A#####wAAfwAAQD0AAAAAAAAAAAAAAAAAAAAAARcQAAAAAAACAAAAAQAAAAEAAAAAAAAAAAAYXElme3Rlc3QyfXtcVmFse3JlcHl9fXt9AAAAHQD#####AAB#AAC#8AAAAAAAAMAIAAAAAAAAAAABFhAAAAAAAAEAAAACAAAAAQAAAAAAAAAAABhcSWZ7dGVzdDR9e1xWYWx7cmVweH19e30AAAALAP####8ABXhJbnQxAAIxMQAAAAFAJgAAAAAAAAAAAAsA#####wAFeEludDIAAjE1AAAAAUAuAAAAAAAAAAAADAD#####AAB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAcAAAAAEQAAAA0AAAEkAAAAAQAAAAAAAAAAAAAADAD#####AAB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAcAAAAAEQAAAA0AAAElAAAAAQAAAAAAAAAAAAAADwD#####AAB#AAAQAAABAAMAAAEmAAABJwAAACD##########w=='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
  }

  function modifFig (st) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété correction (booléen)
    let i
    if (st.correction) {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xExt', String(stor.objDonnees.coordMinMax[0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yExt', String(stor.objDonnees.coordMinMax[1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xInt1', String(stor.objDonnees.bornesInt2[0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xInt2', String(stor.objDonnees.bornesInt2[1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'repx', String(stor.objDonnees.coordMinMax[0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'repy', String(stor.objDonnees.coordMinMax[1]))
    } else {
      // modifications à faire au début
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'grady1', '1')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'divGradx', '2')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'divGrady', '2')
      for (i = 1; i <= 5; i++) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs' + i, String(stor.objDonnees.abs[i - 1]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'ord' + i, String(stor.objDonnees.ord[i - 1]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'c' + i, String(stor.objDonnees.coefDir[i - 1]))
      }
      const afficheCourbe = [0, 0, 0]
      afficheCourbe[stor.objDonnees.numFct - 1] = 1
      for (i = 1; i <= 3; i++) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche' + i, String(afficheCourbe[i - 1]))
      }
      // Bornes du domaine sur lequel on trace la courbe
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xmin', String(stor.objDonnees.bornesRepx[0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xmax', String(stor.objDonnees.bornesRepx[1]))
      // Je positionne l’origine du repère
      const xO = Math.abs(stor.objDonnees.bornesRepx[0]) * 12 / (stor.objDonnees.bornesRepx[1] - stor.objDonnees.bornesRepx[0])
      const yO = Math.abs(stor.objDonnees.bornesRepy[0]) * 12 / (stor.objDonnees.bornesRepy[1] - stor.objDonnees.bornesRepy[0])
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xO', String(xO))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yO', String(yO))
      const unitex = 12 / (stor.objDonnees.bornesRepx[1] - stor.objDonnees.bornesRepx[0])
      const unitey = 12 / (stor.objDonnees.bornesRepy[1] - stor.objDonnees.bornesRepy[0] + 1)
      // console.log((stor.objFct.bornesAbs[1]-stor.objFct.bornesAbs[0]),"   unitex:",unitex)
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'unitex', String(unitex))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'unitey', String(unitey))
      // on replace les points pour la réponse très loin
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xExt', String(3 * stor.objDonnees.bornesRepx[1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yExt', String(3 * stor.objDonnees.bornesRepy[1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xInt1', String(3 * stor.objDonnees.bornesRepx[1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xInt2', String(3 * stor.objDonnees.bornesRepx[1]))
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 4,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      typeLecture: 'courbe',
      situationConcrete: false,
      intervalleReduit: false,
      /*
      Les textes présents dans la section
      Sont donc accessibles dans le code par donneesSection.textes.consigne1
      Possibilité d’écrire dans le code :
      var lestextes: donneesSection.textes;
      puis accès à l’aide de lestextes.consigne1
      */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Extremum d’une fonction',
        // on donne les phrases de la consigne
        consigne1_1: 'On a tracé la courbe représentative d’une fonction $£f$ définie sur $£i$.',
        consigne1_2: 'On a construit le tableau de variations d’une fonction $£f$ définie sur $£i$.',
        consigne2_1: 'Le minimum de la fonction $£f$ sur l’intervalle $£j$ vaut @1@.',
        consigne2_2: 'Le maximum de la fonction $£f$ sur l’intervalle $£j$ vaut @1@.',
        consigne3: 'Il est atteint pour la valeur @1@.',
        consigne4_1: 'Une entreprise fabrique et commercialise un produit.',
        consigne5_1: 'On note $x$ la quantité de produit fabriquée, exprimée en kg, et limitée à 20&nbsp;kg.',
        consigne6_1: 'Le bénéfice fait par l’entreprise est donné par la fonction $£f$ définie par $£f(x)=£e$.',
        consigne7: 'On a construit ci-dessous le tableau de variations (incomplet) de la fonction $£f$.',
        consigne8_1: 'Le bénéfice est maximal pour une production de : @1@ kg.',
        consigne9_1: 'Calcule alors la valeur exacte de ce maximum : @1@',
        consigne4_2: 'Une petite entreprise fabrique un maximum de 35 ordinateurs par mois.',
        consigne5_2: 'On note $x$ le nombre d’ordinateurs produits.',
        consigne6_2: 'Le bénéfice (en euros) réalisé par l’entreprise est donné par la fonction $£f$ définie par : $£f(x)=£e$.',
        consigne8_2: 'Le bénéfice est maximal pour une production de @1@ ordinateurs.',
        consigne9_2: 'Calcule alors la valeur exacte de ce maximum : @1@',
        consigne4_3: 'Avec un fil de $£l$ cm de longueur, on construit des rectangles de différents formats.',
        consigne5_3: 'On note $x$ la longueur d’un côté d’un de ces rectangles.',
        consigne6_3: 'L’aire de ce rectangle est donné par la fonction $£f$ définie par $£f(x)=£e$.',
        consigne8_3: 'L’aire est maximale lorsque $x=$@1@.',
        consigne9_3: 'Calcule alors la valeur exacte de ce maximum : @1@',
        consigne4_4: 'On coupe une ficelle de longueur $£l$ cm en deux morceaux avec lesquels on forme deux carrés.',
        consigne5_4: 'On note $x$ la longueur d’un des deux bouts de ficelle.',
        consigne6_4: 'La somme des aires des deux carrés est donnée par la fonction $£f$ définie par : $£f(x)=£e$.',
        consigne8_4: 'La somme des aires des deux carrés est minimale lorsque $x=$@1@.',
        consigne9_4: 'Calcule alors la valeur exacte de ce minimum : @1@',
        consigne10: 'Les valeurs attendues correspondent aux graduations indiquées sur les axes.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Attention à l’intervalle sur lequel est recherché le maximum&nbsp;!',
        comment2: 'Attention à l’intervalle sur lequel est recherché le minimum&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: '$£f(£x)=£y$.',
        corr2_1: 'Pour tout réel $x$ compris entre $£x$ et $£y$, $£f(x)$ est inférieur ou égal à $£m$.',
        corr2_2: 'Pour tout réel $x$ compris entre $£x$ et $£y$, $£f(x)$ est supérieur ou égal à $£m$.',
        corr3_1: 'Donc $£m$ est le maximum de $£f$ sur $£i$ et il est atteint pour $x=£n$.',
        corr3_2: 'Donc $£m$ est le minimum de $£f$ sur $£i$ et il est atteint pour $x=£n$.',
        corr4_1: 'Le bénéfice est maximal pour une production de £{r1}&nbsp;kg et, dans ce cas, il vaut $£f(£{r1})=£{r2}$&nbsp;€.',
        corr4_2: 'Le bénéfice est maximal pour une production de £{r1}&nbsp;ordinateur et, dans ce cas, il vaut $£f(£{r1})=£{r2}$&nbsp;€.',
        corr4_3: 'L’aire est maximal lorsque $x=£{r1}$ et, dans ce cas, elle vaut $£f(£{r1})=£{r2}$&nbsp;cm².',
        corr4_4: 'La somme des aires des deux carrés est minimale lorsque $x=£{r1}$ et, dans ce cas, elle vaut $£f(£{r1})=£{r2}$&nbsp;cm².'
      },
      pe: 0
    }
  }
  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // Choix de la question
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    let quest
    if (ds.intervalleReduit && (me.questionCourante === ds.nbrepetitions)) {
      quest = 5
    } else {
      const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
      quest = stor.tabQuest[pioche]
      stor.tabQuest.splice(pioche, 1)
    }
    const tabNomF = ['f', 'g', 'h']
    stor.nomf = tabNomF[j3pGetRandomInt(0, 2)]
    let monIntervalle, monIntervalle2, tabDef
    const valXChaine = []
    const valYChaine = []
    if (ds.typeLecture === 'tableau') {
      // on peut avoir un tableau quelconque ou un tableau issu d’une situation concrete
      if (ds.situationConcrete) {
        stor.objDonnees = calculDonneesTab('concret', quest)
      } else {
        stor.objDonnees = calculDonneesTab('abstrait', quest)
      }
      me.logIfDebug('stor.objDonnees:', stor.objDonnees)
      monIntervalle = '[' + j3pVirgule(stor.objDonnees.bornesInt[0]) + '\\quad ;\\quad ' + j3pVirgule(stor.objDonnees.bornesInt[1]) + ']'
      monIntervalle2 = '[' + j3pVirgule(stor.objDonnees.bornesInt2[0]) + '\\quad ;\\quad ' + j3pVirgule(stor.objDonnees.bornesInt2[1]) + ']'
      // l’objet global comportera toutes les informations générales du tableau
      tabDef = {}
      // Tout ce qui concerne la mise en forme :
      tabDef.mef = {}
      // la largeur
      const largMin = (stor.objDonnees.tabVariations.length >= 4) ? 800 : 600
      tabDef.mef.L = Math.max(85 * me.zonesElts.HG.getBoundingClientRect().width / 100, largMin)
      // la hauteur de la ligne des x
      tabDef.mef.h = 50
      // la couleur
      tabDef.mef.macouleur = me.styles.toutpetit.enonce.color
      // le tableau principal, qui contiendra les infos de chaque ligne, il y aura donc autant de lignes (en plus de celle des x)
      tabDef.lignes = []
      // la première et unique ligne :
      tabDef.lignes[0] = {}
      // ligne de signes ou de variations ?
      tabDef.lignes[0].type = 'variations'
      // sa hauteur :
      tabDef.lignes[0].hauteur = 110
      tabDef.lignes[0].valeurs_charnieres = {}
      // j’ai  besoin des valeurs approchées pour les ordonner facilement... (souvent ce sont les mêmes car valeurs entières mais si ce sont des expressions mathquill...)
      tabDef.lignes[0].valeurs_charnieres.valeurs_approchees = []
      // Variations : (on peut mettre const)
      tabDef.lignes[0].variations = []
      // on peut ajouter des valeurs de x avec leurs images.
      tabDef.lignes[0].images = {}
      tabDef.lignes[0].imagesapprochees = []
      tabDef.lignes[0].expression = stor.nomf
      // j’ai mis 0 comme valeur charnière centrale, mais c’est arbitraire ici, ça n’impacte en rien la suite
      // A adapter
    } else {
      stor.objDonnees = calculDonneesGraph(quest)
      me.logIfDebug('Données:', stor.objDonnees)
      monIntervalle = '[' + j3pVirgule(stor.objDonnees.bornesInt[0]) + '\\quad;\\quad' + j3pVirgule(stor.objDonnees.bornesInt[1]) + ']'
      monIntervalle2 = '[' + j3pVirgule(stor.objDonnees.bornesInt2[0]) + '\\quad;\\quad' + j3pVirgule(stor.objDonnees.bornesInt2[1]) + ']'
    }
    stor.monIntervalle2 = monIntervalle2
    stor.zoneInput = []
    if (ds.situationConcrete && (ds.typeLecture === 'tableau')) {
      const objCons = {
        f: stor.nomf,
        e: stor.objDonnees.express,
        l: stor.objDonnees.lg
      }
      j3pAffiche(stor.zoneCons1, '', ds.textes['consigne4_' + quest], objCons)
      j3pAffiche(stor.zoneCons2, '', ds.textes['consigne5_' + quest], objCons)
      j3pAffiche(stor.zoneCons3, '', ds.textes['consigne6_' + quest], objCons)
      j3pAffiche(stor.zoneCons4, '', ds.textes.consigne7, objCons)
      for (let i = 6; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
      objCons.input1 = { texte: '', dynamique: true }
      const elt1 = j3pAffiche(stor.zoneCons6, '', ds.textes['consigne8_' + quest], objCons)
      const elt2 = j3pAffiche(stor.zoneCons7, '', ds.textes['consigne9_' + quest], objCons)
      stor.zoneInput.push(elt1.inputList[0], elt2.inputList[0])
      stor.zoneInput.forEach((eltInput, index) => {
        j3pRestriction(eltInput, '0-9,.\\-')
        eltInput.typeReponse = ['nombre', 'exact']
        eltInput.reponse = [stor.objDonnees.coordMinMax[index]]
        eltInput.addEventListener('input', j3pRemplacePoint)
      })
    } else {
      const laCons1 = (ds.typeLecture === 'tableau') ? ds.textes.consigne1_2 : ds.textes.consigne1_1
      j3pAffiche(stor.zoneCons1, '', laCons1,
        {
          f: stor.nomf,
          i: monIntervalle
        })
      const laCons2 = (stor.objDonnees.minMax === 1) ? ds.textes.consigne2_1 : ds.textes.consigne2_2
      const elt1 = j3pAffiche(stor.zoneCons3, '', laCons2,
        {
          f: stor.nomf,
          j: monIntervalle2,
          input1: { text: '', dynamique: true }
        })
      const elt2 = j3pAffiche(stor.zoneCons4, '', ds.textes.consigne3,
        { input1: { text: '', dynamique: true } })
      stor.zoneInput.push(elt1.inputList[0], elt2.inputList[0])
      stor.zoneInput.forEach((eltInput, index) => {
        j3pRestriction(eltInput, '0-9,.\\-')
        eltInput.typeReponse = ['nombre', 'exact']
        eltInput.reponse = [stor.objDonnees.coordMinMax[1 - index]]
        eltInput.addEventListener('input', j3pRemplacePoint)
      })
    }
    j3pFocus(stor.zoneInput[0])
    me.logIfDebug('rep1:', stor.zoneInput[0].reponse, '     rep2:', stor.zoneInput[1].reponse)
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
    if (ds.typeLecture === 'tableau') {
      for (let i = 0; i < stor.objDonnees.valX.length; i++) {
        valXChaine.push(String(stor.objDonnees.valX[i]))
        if (ds.situationConcrete) {
          if (i === stor.objDonnees.posExtremum) {
            valYChaine.push('')
          } else {
            valYChaine.push(String(stor.objDonnees.valY[i]))
          }
        } else {
          valYChaine.push(String(stor.objDonnees.valY[i]))
        }
      }
      // A adapter
      tabDef.lignes[0].valeurs_charnieres.valeurs_theoriques = [...valXChaine]
      // A adapter
      tabDef.lignes[0].valeurs_charnieres.valeurs_affichees = [...valXChaine]
      // on peut laisser...
      tabDef.lignes[0].valeurs_charnieres.valeurs_approchees = [...stor.objDonnees.valX]
      // A adapter
      tabDef.lignes[0].variations = stor.objDonnees.tabVariations
      tabDef.lignes[0].cliquable = [false, false]
      // les intervalles décrivant les variations de la fonction
      tabDef.lignes[0].intervalles = []
      for (let i = 1; i < valXChaine.length; i++) {
        tabDef.lignes[0].intervalles.push([valXChaine[i - 1], valXChaine[i]])
      }
      // A adapter
      tabDef.lignes[0].images = []
      for (let i = 0; i < valXChaine.length; i++) {
        tabDef.lignes[0].images.push([valXChaine[i], valYChaine[i]])
      }
      tabDef.lignes[0].imagesapprochees = [...stor.objDonnees.valX]
      // on n’autorise pas l’ajout de lignes et d’intervalles
      tabDef.ajout_lignes_possible = false
      tabDef.ajout_intervalles_possible = false
      const correction = false
      if (ds.situationConcrete) {
        tableauSignesVariations(stor.zoneCons5, j3pGetNewId('divTableau'), tabDef, correction)
      } else {
        tableauSignesVariations(stor.zoneCons2, j3pGetNewId('divTableau'), tabDef, correction)
      }
    } else {
      if (ds.typeLecture === 'courbe') {
        /// /////////////////////////////////////// Pour MathGraph32
        const largFig = 400// on gère ici la largeur de la figure
        const hautFig = 400
        // création du div accueillant la figure mtg32
        stor.divMtg32 = j3pAddElt(stor.zoneCons2, 'div')
        // et enfin la zone svg (très importante car tout est dedans)
        stor.mtg32svg = j3pGetNewId('mtg32svg')
        j3pCreeSVG(stor.divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
        // ce qui suit lance la création initiale de la figure
        depart()
        modifFig({ correction: false })
        j3pAffiche(stor.zoneCons5, '', ds.textes.consigne10)
        j3pStyle(stor.zoneCons5, { fontSize: '0.9em', fontStyle: 'italic' })
      }
    }
    stor.quest = quest

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    if (ds.situationConcrete) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
      j3pAfficheCroixFenetres('Calculatrice')
    }

    // Paramétrage de la validation
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // pas besoin de passer fctsValid.zones.inputs dans tabDef.validationZonesInputs car il n’y a pas d’inputs dans le tableau
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        // Construction de la page
        me.construitStructurePage({ structure: ds.structure })

        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // Que ce soit pour un cas concret ou un aure, je n’ai que 4 cas de figure
        stor.tabQuest = [1, 2, 3, 4]
        stor.tabQuestInit = [...stor.tabQuest]
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (stor.quest === 5) {
              // On peut se demander s’il n’a pas cherché sur le mauvais intervalle
              //   console.log(j3pNombre(fctsValid.zones.reponseSaisie[1]), '   bornes :', stor.objDonnees.bornesInt2)
                if ((j3pNombre(fctsValid.zones.reponseSaisie[1]) < stor.objDonnees.bornesInt2[0]) || (j3pNombre(fctsValid.zones.reponseSaisie[1]) > stor.objDonnees.bornesInt2[1])) {
                  if (stor.objDonnees.minMax === 1) {
                    stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment2
                  } else {
                    stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
                  }
                }
              }
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
// console.log("fin du code : ",me.etat)
}
