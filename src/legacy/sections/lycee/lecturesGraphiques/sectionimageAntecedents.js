import { j3pAddElt, j3pArrondi, j3pNombre, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pRemplacePoint, j3pRestriction, j3pStyle, j3pVirgule, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mars 2019
        Exercice reprenant un exo flash permettant de lire graphiquement une image ou des antécédents
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuest', 'image et antecedents', 'liste', 'décrit les différentes questions qui seront posées, on peut avoir "image", "antecedents", "equation" ou une combinaison de ces questions.', ['image', 'antecedents', 'image et antecedents', 'equation', 'image et equation']],
    ['avecIntervalle', true, 'boolean', 'Par défaut, l’ensemble de définition est donné sous la forme d’un intervalle, mais on peut modifier en donnant la valeur false à ce paramètre'],
    ['seuilReussite', 0.8, 'reel', 'seuil au-dessus duquel on considère que l’élève a réussi la notion'],
    ['seuilErreur', 0.4, 'reel', 'seuil au-dessus duquel on considère que la notion n’est pas acquise']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'Problème sur les images' },
    { pe_3: 'Problème sur les antécédents' },
    { pe_4: 'Problème sur les équations' },
    { pe_5: 'Insuffisant' }
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo1: 'Lecture graphique d’image',
  titre_exo2: 'Lecture graphique d’antécédents',
  titre_exo3: 'Lecture graphique de solutions d’équations',
  titre_exo4: 'Lecture graphique d’image et d’antécédents',
  titre_exo5: 'Recherche d’image et résolution d’équations à l’aide d’un graphique',
  // on donne les phrases de la consigne
  consigne1_1: 'On définit sur l’intervalle $£i$ une fonction $£f$ dont on donne une représentation graphique.',
  consigne1_2: 'Entre £a et £b, on définit une fonction $£f$ par la représentation graphique ci-contre.',
  consigne2_1: 'L’image de £x par $£f$ vaut environ @1@.',
  consigne2_2: 'Par la fonction $£f$, le nombre £y #1#.',
  consigne2_3: 'L’équation $£f(x)=£y$ #1#.',
  choix1: 'n’a pas d’antécédent',
  choix2: 'admet exactement 1 antécédent',
  choix3: 'admet exactement 2 antécédents',
  choix4: 'admet exactement 3 antécédents',
  choix1_1: 'n’admet aucune solution',
  choix2_1: 'admet exactement 1 solution',
  choix3_1: 'admet exactement 2 solutions',
  choix4_1: 'admet exactement 3 solutions',
  consigne3_1: 'Cet antécédent est @1@.',
  consigne3_2: 'Ces antécédents sont @1@',
  consigne3_3: 'Cette solution est @1@.',
  consigne3_4: 'Ces solutions sont @1@',
  et: 'et',
  consigne4_1: 'On donnera une valeur approchée à 0,1 près.',
  consigne4_2: 'On donnera des valeurs approchées à 0,1 près.',
  consigne5: 'Pour faciliter la lecture, on dispose d’un point bleu mobile sur la courbe et d’un point rouge permettant de déplacer la droite horizontale.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1_1: 'Le nombre d’antécédents proposé n’est pas le bon !',
  comment1_2: 'Le nombre de solutions proposé n’est pas le bon !',
  comment2_1: 'Le nombre d’antécédents est pourtant correct !',
  comment2_2: 'Le nombre de solutions est pourtant correct !',
  comment3: 'La réponse donnée doit être un nombre et non un calcul !',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'L’image de $£x$ par $£f$ est notée $£f(£x)$.',
  corr1_2: 'Graphiquement, on identifie le point de la courbe d’abscisse $£x$ comme représenté dans le repère ci-contre.',
  corr1_3: 'Ainsi $£f(£x)\\approx £r$.',
  corr2_1: 'Les antécédents de $£y$ par $£f$ sont les abscisses des points de la courbe d’ordonnée $£y$.',
  corr2_2: 'Graphiquement, on identifie $£y$ sur l’axe des ordonnées et on détermine le nombre de points de la courbe admettant $£y$ pour ordonnée (à l’aide de la droite en vert).',
  corr2_31: 'Ainsi $£y$ n’admet pas d’antécédent par $£f$.',
  corr2_32: 'Ainsi l’antécédent de $£y$ par $£f$ est $£r$.',
  corr2_33: 'Ainsi les antécédents de $£y$ par $£f$ sont $£r$.',
  corr3_1: 'Les solutions de l’équation $£f(x)=£y$ sont les abscisses des points de la courbe d’ordonnée $£y$.',
  corr3_31: 'Ainsi l’équation $£f(x)=£y$ n’admet pas de solution.',
  corr3_32: 'Ainsi l’équation $£f(x)=£y$ admet une unique solution : $£r$.',
  corr3_33: 'Ainsi l’équation $£f(x)=£y$ admet £n solutions : $£r$.'
}

/**
 * section imageAntecedents
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    stor.explications = j3pAddElt(stor.conteneur, 'div')
    // le style n’est pas le même suivant que la réponse soit bonne ou fausse'
    stor.explications.style.color = (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
    // le div stor.explications contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    for (let i = 1; i <= 3; i++) stor['expli' + i] = j3pAddElt(stor.explications, 'div')
    const objImage = {}
    objImage.f = stor.nomf
    if (stor.typeQuest[me.questionCourante - 1] === 'image') {
      j3pEmpty(stor.zoneCons3)
      j3pEmpty(stor.zoneCons4)
      objImage.x = j3pVirgule(stor.xval)
      objImage.r = j3pVirgule(Math.round(stor.zoneInput[0].reponse[0] * 10) / 10)
      j3pAffiche(stor.expli1, '', textes.corr1_1, objImage)
      j3pAffiche(stor.expli2, '', textes.corr1_2, objImage)
      if (!bonneReponse) j3pAffiche(stor.expli3, '', textes.corr1_3, objImage)
      modifFig({ correction: true, type: 'image', bonneRep: bonneReponse })
    } else {
      j3pEmpty(stor.zoneCons6)
      j3pEmpty(stor.zoneInfo)
      objImage.y = j3pVirgule(stor.objFct.yval)
      const laCorr1 = (stor.typeQuest[me.questionCourante - 1] === 'antecedents') ? textes.corr2_1 : textes.corr3_1
      j3pAffiche(stor.expli1, '', laCorr1, objImage)
      j3pAffiche(stor.expli2, '', textes.corr2_2, objImage)
      if (!bonneReponse) {
        const laCorr3 = (stor.typeQuest[me.questionCourante - 1] === 'antecedents')
          ? (stor.objFct.anteYval.length === 0)
              ? textes.corr2_31
              : (stor.objFct.anteYval.length === 1) ? textes.corr2_32 : textes.corr2_33
          : (stor.objFct.anteYval.length === 0)
              ? textes.corr3_31
              : (stor.objFct.anteYval.length === 1) ? textes.corr3_32 : textes.corr3_33
        // On établit la liste des solutions de l’équation
        objImage.r = ''
        if (stor.objFct.anteYval.length === 1) {
          objImage.r = stor.objFct.anteYval[0]
        } else if (stor.objFct.anteYval.length > 1) {
          objImage.r = stor.objFct.anteYval[0]
          for (let i = 2; i <= stor.objFct.anteYval.length; i++) {
            objImage.r += '\\quad;\\quad' + stor.objFct.anteYval[i - 1]
          }
        }
        objImage.n = stor.objFct.anteYval.length
        j3pAffiche(stor.expli3, '', laCorr3, objImage)
      }
      modifFig({ correction: true, type: 'antecedent', bonneRep: bonneReponse })
    }
  }

  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV8AAADnAAAAQEAAAAAAAAAAQAAAK3#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAJPMQDAKAAAAAAAAAAAAAAAAAAABQABQAQAAAAAAABAeNXCj1wo9v####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAAAAQAAABAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAEOAAJJMQDAGAAAAAAAAAAAAAAAAAAABQABQECAAAAAAAAAAAAC#####wAAAAEACUNEcm9pdGVBQgD#####AQAAAAAQAAABAAEAAAABAAAAA#####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAAAAABAAAAEAAQAAAAEAAAAE#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAABAAAAAQAAAAP#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAABQAAAAb#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAEOAAJKMQDAKAAAAAAAAMAQAAAAAAAABQACAAAAB#####8AAAACAAdDUmVwZXJlAP####8A5ubmAAEAAAABAAAAAwAAAAgAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABP#AAAAAAAAAAAAABP#AAAAAAAAD#####AAAAAQAHQ0NhbGN1bAD#####AAJ4TwABNgAAAAFAGAAAAAAAAAAAAAsA#####wACeU8AATYAAAABQBgAAAAAAAD#####AAAAAQAQQ1BvaW50RGFuc1JlcGVyZQD#####AQAAAAAOAAFPAMAoAAAAAAAAQAAAAAAAAAAFAAAAAAn#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAACgAAAA0AAAALAAAACwD#####AAZ1bml0ZXgAATEAAAABP#AAAAAAAAAAAAALAP####8ABnVuaXRleQABMQAAAAE#8AAAAAAAAAAAAAwA#####wEAAAAADgABSgDAJAAAAAAAAMAgAAAAAAAABQAAAAAJAAAADQAAAAr#####AAAAAQAKQ09wZXJhdGlvbgAAAAANAAAACwAAAA0AAAAOAAAADAD#####AQAAAAEOAAFJAMAIAAAAAAAAQAgAAAAAAAAFAAAAAAkAAAAOAAAAAA0AAAAKAAAADQAAAA0AAAANAAAACwAAAAoA#####wDm5uYAAQAAAAwAAAAQAAAADwEBAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAAAAAAsA#####wAIZ3JhZG1heHkAATYAAAABQBgAAAAAAAAAAAAMAP####8BAAAAABAAAksyAEAUAAAAAAAAwCgAAAAAAAAFAAAAABEAAAABAAAAAAAAAAAAAAANAAAAEv####8AAAABAAhDU2VnbWVudAD#####AAAAAAAQAAABAAEAAAAPAAAAEwAAAAQA#####wEAAAAAEAACdzMAQBgAAAAAAADAKgAAAAAAAAUAAT#gSnkEp5BLAAAAFP####8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAADP####8AAAABAAtDUG9pbnRJbWFnZQD#####AWZmZgAQAAJ3NAAAAAAAAAAAAEAIAAAAAAAABQAAAAAVAAAAFv####8AAAACAAhDTWVzdXJlWQD#####AAN5dzQAAAARAAAAFwAAABIA#####wADeXczAAAAEQAAABUAAAALAP####8ABmdyYWR5MQABMgAAAAFAAAAAAAAAAAAAAAsA#####wAHZ3JhZHZ1ZQAabW9kKGludCh5dzMrMC41KSxncmFkeTEpPTAAAAAOCP####8AAAABAA1DRm9uY3Rpb24yVmFyBv####8AAAACAAlDRm9uY3Rpb24CAAAADgAAAAANAAAAGQAAAAE#4AAAAAAAAAAAAA0AAAAaAAAAAQAAAAAAAAAAAAAACQD#####AQAAAAAQAAABBQABAAAAB#####8AAAABAApDVW5pdGV4UmVwAP####8ABHVuaXQAAAAJ#####wAAAAEAC0NIb21vdGhldGllAP####8AAAABAAAADgMAAAABP#AAAAAAAAAAAAANAAAAHQAAABEA#####wEAAAAAEAACVyIBAQAAAAADAAAAHv####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAAfAAAACwD#####AAduYmdyYWR4AAIyMAAAAAFANAAAAAAAAAAAAAsA#####wAHbmJncmFkeQACMjAAAAABQDQAAAAAAAAAAAAFAP####8AAAAAAA4AAAEAAQAAAAwAAAAPAAAABQD#####AAAAAAAOAAABAAEAAAAMAAAAEAAAAAsA#####wAIZGl2R3JhZHgAAjEwAAAAAUAkAAAAAAAA#####wAAAAIADENDb21tZW50YWlyZQD#####AAAAAAH#####EECHUAAAAAAAQIbAUeuFHrgAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAA9jYWRyZSA0MDAgKiA0MDAAAAALAP####8ACGdyYWRtYXh4AAIxMAAAAAFAJAAAAAAAAAAAAAwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAABEAAAANAAAAJwAAAAEAAAAAAAAAAAAAAA8A#####wEAAAAAEAAAAQABAAAAEAAAACgAAAAEAP####8BAAAAABAAAXcAQAgAAAAAAADAOgAAAAAAAAUAAT#qVszBEvI3AAAAKf####8AAAACAAhDTWVzdXJlWAD#####AAJ4dwAAABEAAAAq#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQD#####AYSEhAAAAAAAAAAAAEAIAAAAAAAAAAAAKhAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAr#####wAAAAIAEkNMaWV1T2JqZXRQYXJQdExpZQD#####AISEhAAAACwAAAANAAAAJwAAACoAAAADAAAAKgAAACsAAAAsAAAAEQD#####AYSEhAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAKgAAABYAAAALAP####8AA3h3JwADLXh3#####wAAAAEADENNb2luc1VuYWlyZQAAAA0AAAArAAAAGgD#####AYSEhAC#8AAAAAAAAEAQAAAAAAAAAAAALhAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAvAAAAGwD#####AISEhAAAADAAAAANAAAAJwAAACoAAAAFAAAAKgAAACsAAAAuAAAALwAAADAAAAADAP####8BhISEARAAAAEAAQAAACoAP#AAAAAAAAAAAAAEAP####8BhISEABAAAAAAAAAAAAAAAEAIAAAAAAAABQABwBk7ZFocrAAAAAAyAAAAEAD#####AAAAKgAAABEA#####wGEhIQAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAADMAAAA0AAAADwD#####AYSEhAAQAAABAAEAAAAzAAAANQAAABsA#####wCEhIQAAAA2AAAADQAAACcAAAAqAAAABgAAACoAAAAyAAAAMwAAADQAAAA1AAAANgAAABEA#####wGEhIQAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAADMAAAAWAAAAEQD#####AYSEhAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAANQAAABYAAAAPAP####8BhISEABAAAAEAAQAAADgAAAA5AAAAGwD#####AISEhAAAADoAAAANAAAAJwAAACoAAAAIAAAAKgAAADIAAAAzAAAANAAAADUAAAA4AAAAOQAAADoAAAAPAP####8BAAAAABAAAAEAAQAAAAwAAAAoAAAABAD#####AQAAAAAQAAJ3JwDAEAAAAAAAAMA5AAAAAAAABQABP9iv+oDhiUEAAAA8AAAAGQD#####AAN4dyIAAAARAAAAPQAAAAMA#####wGEhIQBEAAAAQABAAAAPQA#8AAAAAAAAAAAAAQA#####wGEhIQAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFADYk3S8aoAAAAAD8AAAAQAP####8AAAA9AAAAEQD#####AYSEhAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAQAAAAEEAAAAPAP####8BhISEABAAAAEAAQAAAEAAAABCAAAAEQD#####AYSEhAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAQAAAABYAAAARAP####8BhISEABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAABCAAAAFgAAAA8A#####wGEhIQAEAAAAQABAAAARAAAAEUAAAAbAP####8AhISEAAAAQwAAAA4AAAAADgIAAAANAAAAJQAAAA0AAAAnAAAAAT#wAAAAAAAAAAAAPQAAAAYAAAA9AAAAPwAAAEAAAABBAAAAQgAAAEMAAAAbAP####8AhISEAAAARgAAAA4AAAAADgIAAAANAAAAJQAAAA0AAAAnAAAAAT#wAAAAAAAAAAAAPQAAAAgAAAA9AAAAPwAAAEAAAABBAAAAQgAAAEQAAABFAAAARgAAAAsA#####wAIZGl2R3JhZHkAATUAAAABQBQAAAAAAAAAAAAYAP####8BZmZmAMAiAAAAAAAAAAAAAAAAAAAAAAAVEAAB####AAAAAgAAAAEAAAABAAAAAAAAAAAACyNWYWwoeXczLDApAAAAGAD#####AWZmZgDAIAAAAAAAAL#wAAAAAAAAAAAAFxAAAAAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKHl3NCwwKQAAAAMA#####wFmZmYBEAACZDQBAAE#7MzMzMzMzQAAABUBP#AAAAAAAAAAAAAEAP####8BZmZmABAAAk0yAAAAAAAAAAAAQAgAAAAAAAAFAAHAGNkWhysCAAAAAEwAAAAQAP####8AAAAVAAAAEQD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAATQAAAE4AAAAPAP####8BZmZmABAAAAEAAQAAAE0AAABPAAAAGwD#####AHd3dwAAAFAAAAANAAAAEgAAABUAAAAGAAAAFQAAAEwAAABNAAAATgAAAE8AAABQAAAAEQD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAATQAAABYAAAARAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAABPAAAAFgAAAA8A#####wFmZmYAEAAAAQABAAAAUwAAAFIAAAAbAP####8Ad3d3AAAAVAAAAA0AAAASAAAAFQAAAAgAAAAVAAAATAAAAE0AAABOAAAATwAAAFIAAABTAAAAVAAAAA8A#####wBmZmYAEAAAAQABAAAADAAAABMAAAAEAP####8BZmZmABAAAnc1AEAYAAAAAAAAwCgAAAAAAAAFAAE#2kUv69iQTAAAAFYAAAADAP####8BZmZmARAAAAEAAQAAAFcBP#AAAAAAAAAAAAAEAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAABQABwBDZFocrAgAAAABYAAAAEAD#####AAAAVwAAABEA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAFkAAABaAAAADwD#####AWZmZgAQAAABAAEAAABZAAAAWwAAABEA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAFkAAAAWAAAAEQD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAWwAAABYAAAAPAP####8BZmZmABAAAAEAAQAAAF0AAABeAAAAGwD#####AHd3dwAAAFwAAAAOAAAAAA4CAAAADQAAAEkAAAANAAAAEgAAAAE#8AAAAAAAAAAAAFcAAAAGAAAAVwAAAFgAAABZAAAAWgAAAFsAAABcAAAAGwD#####AHd3dwAAAF8AAAAOAAAAAA4CAAAADQAAAEkAAAANAAAAEgAAAAE#8AAAAAAAAAAAAFcAAAAIAAAAVwAAAFgAAABZAAAAWgAAAFsAAABdAAAAXgAAAF8AAAALAP####8AAmEwAAExAAAAAT#wAAAAAAAAAAAACwD#####AAJhMQACLTEAAAAcAAAAAT#wAAAAAAAAAAAACwD#####AAJhMgAELTEvMgAAABwAAAAOAwAAAAE#8AAAAAAAAAAAAAFAAAAAAAAAAAAAAAsA#####wACYTMAATAAAAABAAAAAAAAAAAAAAALAP####8AAmE0AAEwAAAAAQAAAAAAAAAA#####wAAAAEABUNGb25jAP####8AAWYAHGEwK2ExKngrYTIqeF4yK2EzKnheMythNCp4XjQAAAAOAAAAAA4AAAAADgAAAAAOAAAAAA0AAABiAAAADgIAAAANAAAAY#####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAAOAgAAAA0AAABk#####wAAAAEACkNQdWlzc2FuY2UAAAAeAAAAAAAAAAFAAAAAAAAAAAAAAA4CAAAADQAAAGUAAAAfAAAAHgAAAAAAAAABQAgAAAAAAAAAAAAOAgAAAA0AAABmAAAAHwAAAB4AAAAAAAAAAUAQAAAAAAAAAAF4AAAACwD#####AAR4bWluAAItNQAAABwAAAABQBQAAAAAAAAAAAALAP####8ABHhtYXgAATYAAAABQBgAAAAAAAAAAAAMAP####8BAAAAABAAAABACAAAAAAAAAAAAAAAAAAABQAAAAARAAAADQAAAGgAAAABAAAAAAAAAAAAAAAMAP####8BAAAAABAAAABACAAAAAAAAAAAAAAAAAAABQAAAAARAAAADQAAAGkAAAABAAAAAAAAAAAAAAAPAP####8BAAAAABAAAAEAAQAAAGoAAABrAAAABAD#####AQAAAAAQAAJ4MQAAAAAAAAAAAEAIAAAAAAAABQABAAAAAAAAAAAAAABsAAAAGQD#####AAJ4MQAAABEAAABtAAAACwD#####AAJ5MQAFZih4MSn#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAABnAAAADQAAAG4AAAAMAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAARAAAADQAAAG4AAAANAAAAb#####8AAAACAA1DTGlldURlUG9pbnRzAP####8AAAD#AAEAAABwAAAB9AABAAAAbQAAAAQAAABtAAAAbgAAAG8AAABw#####wAAAAEAFUNQb2ludExpZUxpZXVQYXJQdExpZQD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAkAAT#dPhN+t6nPAAAAcT#dPhN+t6nP#####wAAAAEADUNQb2ludFByb2pldGUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAAHIAAAAkAAAAIwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAkAAAAAcgAAACMAAAAPAP####8AAAD#ABAAAAEBAQAAAHIAAABzAAAADwD#####AAAA#wAQAAABAQEAAAByAAAAdAAAAAsA#####wAFeGluaXQABC00LjgAAAAcAAAAAUATMzMzMzMzAAAACwD#####AAVpbWFnZQAFMy41NDgAAAABQAxiTdLxqfwAAAAMAP####8AAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAAARAAAADQAAAHcAAAANAAAAeAAAAAwA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAABEAAAANAAAAdwAAAAEAAAAAAAAAAAAAAAwA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAABEAAAABAAAAAAAAAAAAAAANAAAAeAAAAA8A#####wAAfwAAEAAAAQICAAAAewAAAHkAAAAPAP####8AAH8AABAAAAECAgAAAHkAAAB6#####wAAAAIABkNMYXRleAD#####AXd3dwDAJgAAAAAAAAAAAAAAAAAAAAAAFRAAAAAAAAIAAAABAAAAAQAAAAAAAAAAABxcSWZ7Z3JhZHZ1ZX17XFZhbHt5dzMsMH19eyB9AAAAGwD#####AHd3dwAAAH4AAAANAAAAEgAAABUAAAAEAAAAFQAAABkAAAAbAAAAfgAAAAsA#####wAKZ3JhZHZ1ZU5lZwAfbW9kKGludChhYnMoeXczKSswLjUpLGdyYWR5MSk9MAAAAA4IAAAAEwYAAAAUAgAAAA4AAAAAFAAAAAANAAAAGQAAAAE#4AAAAAAAAAAAAA0AAAAaAAAAAQAAAAAAAAAAAAAAJAD#####AXd3dwDAKAAAAAAAAAAAAAAAAAAAAAAAFxAAAAAAAAIAAAABAAAAAQAAAAAAAAAAAB9cSWZ7Z3JhZHZ1ZU5lZ317XFZhbHt5dzQsMH19eyB9AAAAGwD#####AHd3dwAAAIEAAAANAAAAEgAAABUAAAAGAAAAFQAAABcAAAAYAAAAGQAAAIAAAACBAAAACwD#####AA5hYnNEcm9pdGVSb3VnZQADNS41AAAAAUAWAAAAAAAAAAAACwD#####AA9vcmREcm9pdGVSb3VnZTEABC01LjgAAAAcAAAAAUAXMzMzMzMzAAAACwD#####AA9vcmREcm9pdGVSb3VnZTIAAzUuNQAAAAFAFgAAAAAAAAAAAAwA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAABEAAAANAAAAgwAAAA0AAACEAAAADAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAEQAAAA0AAACDAAAADQAAAIUAAAAPAP####8B#wAAABAAAAEAAQAAAIcAAACGAAAABAD#####AP8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAT#mWsG7wsNiAAAAiAAAAAMA#####wD#AAABEAAAAQABAAAAiQE#8AAAAAAAAAAAAAsA#####wAMb3JkRHJvaXRlUmVwAAM0LjUAAAABQBIAAAAAAAAAAAAMAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAAARAAAADQAAAIMAAAANAAAAiwAAAAMA#####wAAfwABEAAAAQABAAAAjAE#8AAAAAAAAAAAAAsA#####wAFYW50ZTEAATIAAAABQAAAAAAAAAAAAAALAP####8ABWFudGUyAAEzAAAAAUAIAAAAAAAAAAAACwD#####AAVhbnRlMwABNAAAAAFAEAAAAAAAAAAAAAwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAABEAAAANAAAAjgAAAA0AAACLAAAADAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAEQAAAA0AAACPAAAADQAAAIsAAAAMAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAARAAAADQAAAJAAAAANAAAAiwAAACMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAJEAAAAkAAAAIwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAkgAAACQAAAAjAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAACTAAAAJAAAAA8A#####wAAfwAAEAAAAQICAAAAkQAAAJQAAAAPAP####8AAH8AABAAAAECAgAAAJIAAACVAAAADwD#####AAB#AAAQAAABAgIAAACTAAAAlgAAAAsA#####wAJdGVzdEltYWdlAAdpbWFnZT4wAAAADgUAAAANAAAAeAAAAAEAAAAAAAAAAAAAACQA#####wAAfwAAP#AAAAAAAADAIAAAAAAAAAAAAHoQAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAdXElme3Rlc3RJbWFnZX17fXtcVmFse3hpbml0fX0AAAALAP####8ABXRlc3RYAAd4aW5pdD4wAAAADgUAAAANAAAAdwAAAAEAAAAAAAAAAAAAAAsA#####wAGaW1hZ2UxAAM0LjUAAAABQBIAAAAAAAAAAAAkAP####8AAH8AAMA#AAAAAAAAwAAAAAAAAAAAAAB7EAAAAAAAAgAAAAEAAAABAAAAAAAAAAAAGlxJZnt0ZXN0WH17XFZhbHtpbWFnZTF9fXt9AAAAJAD#####AAB#AADAEAAAAAAAAEA0AAAAAAAAAAAAehAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAB1cSWZ7dGVzdEltYWdlfXtcVmFse3hpbml0fX17fQAAACQA#####wAAfwAAQCQAAAAAAAA#8AAAAAAAAAAAAHsQAAAAAAAAAAAAAQAAAAEAAAAAAAAAAAAaXElme3Rlc3RYfXt9e1xWYWx7aW1hZ2UxfX0AAAAMAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAARAAAAAQAAAAAAAAAAAAAADQAAAIsAAAAkAP####8AAH8AAQAAAKEQAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAUXFZhbHtvcmREcm9pdGVSZXAsMX0AAAALAP####8ACHRlc3RBbnRlAA5vcmREcm9pdGVSZXA+MAAAAA4FAAAADQAAAIsAAAABAAAAAAAAAAAAAAALAP####8AAnIxAAMyLjMAAAABQAJmZmZmZmYAAAALAP####8AAnIyAAItMwAAABwAAAABQAgAAAAAAAAAAAALAP####8AAnIzAAQtNi41AAAAHAAAAAFAGgAAAAAAAAAAACQA#####wAAfwAAAAAAAAAAAADAEAAAAAAAAAAAAJQQAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAZXElme3Rlc3RBbnRlfXt9e1xWYWx7cjF9fQAAACQA#####wAAfwAAP#AAAAAAAADAEAAAAAAAAAAAAJUQAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAZXElme3Rlc3RBbnRlfXt9e1xWYWx7cjJ9fQAAACQA#####wAAfwAAAAAAAAAAAADAEAAAAAAAAAAAAJYQAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAZXElme3Rlc3RBbnRlfXt9e1xWYWx7cjN9fQAAACQA#####wAAfwAAAAAAAAAAAABANAAAAAAAAAAAAJQQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAAZXElme3Rlc3RBbnRlfXtcVmFse3IxfX17fQAAACQA#####wAAfwAAv#AAAAAAAABANAAAAAAAAAAAAJUQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAAZXElme3Rlc3RBbnRlfXtcVmFse3IyfX17fQAAACQA#####wAAfwAAAAAAAAAAAABANAAAAAAAAAAAAJYQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAAZXElme3Rlc3RBbnRlfXtcVmFse3IzfX17fQAAACD##########w=='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true);
  }

  function modifFig (st) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété correction (booléen)
    let i
    if (st.correction) {
      // on affiche la correction
      if (st.type === 'image') {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xinit', String(stor.xval))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'image', String(stor.zoneInput[0].reponse[0]))
        if (st.bonneRep) {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'image1', String(j3pNombre(stor.fctsValid.zones.reponseSaisie[0])))
        } else {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'image1', String(j3pArrondi(stor.zoneInput[0].reponse[0], 1)))
        }
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'ordDroiteRep', String(stor.objFct.yval))
        // J’ordonne les valeurs de anteYval pour qu’elle soient dans l’ordre croissant
        let anteOrdonne = []
        if (stor.objFct.anteYval.length > 0) {
          anteOrdonne = stor.objFct.anteYval.sort(function (x, y) {
            return x - y
          })
          for (i = 0; i < anteOrdonne.length; i++) {
            stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'ante' + (i + 1), String(anteOrdonne[i]))
          }
        }
        if (st.bonneRep) {
          const repOrdonne = stor.fctsValid.zones.reponseSaisie.splice(1).sort(function (x, y) {
            return j3pNombre(String(x)) - j3pNombre(String(y))
          })
          for (i = 1; i <= repOrdonne.length; i++) {
            stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'r' + i, String(j3pNombre(repOrdonne[i - 1])))
          }
        } else {
          for (i = 1; i <= anteOrdonne.length; i++) {
            stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'r' + i, String(j3pArrondi(anteOrdonne[i - 1], 1)))
          }
        }
      }
    } else {
      // ceci sert si jamais on souhaite modifier la courbe initiale dans la question
      // positionnement de l’origine du repère puis des unités
      // Coefficients du polynôme dont on trace la courbe représentative
      for (i = 0; i < 5; i++) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a' + i, String(stor.objFct.coefPoly[i]))
      }
      // Bornes du domaine sur lequel on trace la courbe
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xmin', String(stor.objFct.bornes[0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xmax', String(stor.objFct.bornes[1]))
      // Je positionne l’origine du repère
      const xO = Math.abs(stor.objFct.bornesAbs[0]) * 12 / (stor.objFct.bornesAbs[1] - stor.objFct.bornesAbs[0])
      const yO = Math.abs(stor.objFct.bornesOrd[0]) * 12 / (stor.objFct.bornesOrd[1] - stor.objFct.bornesOrd[0])
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xO', String(xO))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yO', String(yO))
      const unitex = 12 / (stor.objFct.bornesAbs[1] - stor.objFct.bornesAbs[0])
      const unitey = 12 / (stor.objFct.bornesOrd[1] - stor.objFct.bornesOrd[0])
      // console.log((stor.objFct.bornesAbs[1]-stor.objFct.bornesAbs[0]),"   unitex:",unitex)
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'unitex', String(unitex))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'unitey', String(unitey))
      // Je gère alors les graduations
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'grady1', String(stor.objFct.pasGrady))
      // console.log("stor.objFct.pasGrady:",stor.objFct.pasGrady)
      // Positionnement de la droite Rouge
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'absDroiteRouge', String(stor.objFct.bornesAbs[1] - unitex / 10))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'ordDroiteRouge1', String(stor.objFct.bornesOrd[0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'ordDroiteRouge2', String(stor.objFct.bornesOrd[1]))

      const gradmaxy = Math.ceil(Math.max(-stor.objFct.bornesOrd[0], stor.objFct.bornesOrd[1])) + 3
      const gradmaxx = Math.ceil(Math.max(-stor.objFct.bornesAbs[0], stor.objFct.bornesAbs[1])) + 3
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'gradmaxx', String(gradmaxx))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'gradmaxy', String(gradmaxy))
      const divUnitex = 10
      const divUnitey = (stor.quest[(me.questionCourante - 1) / ds.nbetapes] >= 8)
        ? (stor.objFct.bornesOrd[1] - stor.objFct.bornesOrd[0] > 20) ? 1 : 2
        : (stor.quest[(me.questionCourante - 1) / ds.nbetapes] >= 6) ? 5 : 2
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'divGradx', String(divUnitex))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'divGrady', String(divUnitey))
      // Je mets le point qui me servira à la correction de l’image très loin
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xinit', '100')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'image', '100')
      // Puis ce qui me servira pour les antécédents
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'ordDroiteRep', '-30')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'ante1', '-30')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'ante2', '-30')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'ante3', '-30')
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  // Construit un polynôme de degré n tel que y admette n antécédents: x[i]
  function construitPolyAntecedents (n, x, y) {
    let coefPoly = []
    if (n === 2) {
      coefPoly = [x[0] * x[1] + y, -x[0] - x[1], 1]
    }
    if (n === 3) {
      coefPoly = [-x[0] * x[1] * x[2] + y, x[0] * x[1] + x[0] * x[2] + x[1] * x[2], -x[0] - x[1] - x[2], 1]
    }
    if (n === 4) {
      coefPoly = [x[0] * x[1] * x[2] * x[3] + y, -(x[0] * x[1] * x[2] + x[0] * x[2] * x[3] + x[0] * x[1] * x[3] + x[1] * x[2] * x[3]), x[0] * x[1] + x[0] * x[2] + x[0] * x[3] + x[1] * x[2] + x[1] * x[3] + x[2] * x[3], -(x[0] + x[1] + x[2] + x[3]), 1]
    }
    // Le polynôme sera de la forme coefPoly[0]+coefPoly[1] x+coefPoly[2] x²+...
    return coefPoly
  }

  // Construit un polynôme de degré 3 tel que y admette 1 antécédent x
  // P(t)= t^3 + (b-x)t^2 + (1-bx)t -x + y   avec  -2 < b < 2
  function construitPoly3Unantecedent (b, x, y) {
    return [-x + y, 1 - b * x, b - x, 1]
  }

  function maxMin (fonctionF, xmin, xmax, pas) {
    let maxF = -100
    let minF = 100
    let maxFAtteint = [xmin]
    let minFAtteint = [xmin]
    for (let vari = Math.ceil(xmin * 100) / 100; vari <= xmax; vari += pas) {
      vari = Math.round(vari / pas) * pas
      if (Math.abs(fonctionF(vari) - maxF) <= Math.pow(10, -5)) {
        if (vari - maxFAtteint[maxFAtteint.length - 1] - pas / 10 < pas) {
          maxFAtteint[maxFAtteint.length - 1] = vari
        } else {
          maxFAtteint.push(vari)
        }
      } else if (fonctionF(vari) - maxF > Math.pow(10, -5)) {
        maxF = fonctionF(vari)
        maxFAtteint = [vari]
      }
      if (Math.abs(fonctionF(vari) - minF) <= Math.pow(10, -5)) {
        if (vari - minFAtteint[minFAtteint.length - 1] - pas / 10 < pas) {
          minFAtteint[minFAtteint.length - 1] = vari
        } else {
          minFAtteint.push(vari)
        }
      } else if (fonctionF(vari) - minF < Math.pow(10, -5)) {
        minF = fonctionF(vari)
        minFAtteint = [vari]
      }
    }
    return { maxi: maxF, mini: minF, absMax: maxFAtteint, absMin: minFAtteint }
  }

  function defFonction (num) {
    // num est l’ensemble des cas de figure possibles
    let a
    let b
    const plusmoins = j3pGetRandomInt(0, 1) * 2 - 1
    let polyinter = []
    let xinit
    let xfin // bornes du domaine de definition
    let decalage = 0.15
    const antecedentsYval = []
    let a1, a2
    let mina, maxa, yval
    if (num === 1) {
      //  Fonction a x + b
      a = 0
      while ((a <= 0.4) && (a >= -0.4)) {
        a = j3pGetRandomInt(-20, 20) / 8
      }
      a = plusmoins * (0.4 + j3pGetRandomInt(0, 99) / 100)
      b = j3pGetRandomInt(0, 7) - 3
      polyinter = [b, a]
      // Je cherche alors les dimension du repère à afficher
      xinit = Math.min(-3.7, a - 1.2)
      xfin = Math.max(3.8, a + 1.2)
      decalage = 0.2
    } else if (num <= 3) {
      // fonction a x² + b x + c --> deux d’antécédent  a>0
      // pour num === 2
      // fonction a x² + b x + c --> 2 antécédents a<0
      // pour num === 3
      // Construit un polynôme de degré n tel que y a n antécédents: x[i]
      a1 = 0
      a2 = 0
      while (Math.abs(a1 - a2) < 1) {
        a1 = j3pGetRandomInt(0, 6) - 3
        a2 = j3pGetRandomInt(0, 6) - 3
      }
      do {
        yval = j3pGetRandomInt(-6, 6) / 2
      } while (Math.abs(yval) < 0.3)
      // par exemple a1=1 et a2=-3
      polyinter = construitPolyAntecedents(2, [a1, a2], yval)
      for (let i = 0; i < polyinter.length; i++) {
        polyinter[i] = (num === 2) ? j3pArrondi(polyinter[i], 3) : -j3pArrondi(polyinter[i], 3)
      }
      if (num === 3) yval = -yval
      mina = Math.min(a1, a2)
      maxa = Math.max(a1, a2)
      xinit = Math.min(mina - 1 - j3pGetRandomInt(0, 2) * 0.5, -1.3)
      xfin = Math.max(maxa + 1 + j3pGetRandomInt(0, 2) * 0.5, 1.3)
      antecedentsYval.push(a1, a2)
    } else if (num <= 5) {
      // Construit un polynôme de degré 2 tel que y a 0 antécédent  a>0
      // pour num === 4
      // Construit un polynôme de degré 2 tel que y a 0 antécédent  a<0
      // pour num === 5
      a1 = 0
      a2 = 0
      while (Math.abs(a1 - a2) < 1) {
        a1 = j3pGetRandomInt(0, 6) - 3
        a2 = j3pGetRandomInt(0, 6) - 3
      }
      // par exemple a1=1 et a2=-3
      polyinter = construitPolyAntecedents(2, [a1, a2], 0)
      for (let i = 0; i < polyinter.length; i++) {
        polyinter[i] = (num === 4) ? j3pArrondi(polyinter[i], 3) : -j3pArrondi(polyinter[i], 3)
      }
      mina = Math.min(a1, a2)
      maxa = Math.max(a1, a2)
      xinit = Math.min(mina - 1 - j3pGetRandomInt(0, 2) * 0.5, -1.3)
      xfin = Math.max(maxa + 1 + j3pGetRandomInt(0, 2) * 0.5, 1.3)
      // antecedentsYval reste vide
    } else if (num <= 7) {
      // fonction a x^3 + b x² + c x + d --> trois à l’endroit
      // pour num === 6
      // fonction a x^3 + b x² + c x + d --> trois à l’envers
      // pour num === 7
      // Construit un polynôme de degré  3 tel que y a n antécédents: x[i]
      a1 = 0
      a2 = 0
      let a3 = 0
      while ((Math.abs(a1 - a2) < 1) || (Math.abs(a1 - a3) < 1) || (Math.abs(a2 - a3) < 1)) {
        a1 = j3pGetRandomInt(-26, 26) / 10
        a2 = j3pGetRandomInt(-26, 26) / 10
        a3 = j3pGetRandomInt(-26, 26) / 10
      }
      do {
        yval = j3pGetRandomInt(-6, 6) / 2
      } while (Math.abs(yval) < 1)
      polyinter = construitPolyAntecedents(3, [a1, a2, a3], yval)
      for (let i = 0; i < polyinter.length; i++) {
        polyinter[i] = (num === 6) ? j3pArrondi(polyinter[i], 3) : -j3pArrondi(polyinter[i], 3)
      }
      if (num === 7) yval = -yval
      mina = Math.min(a1, Math.min(a2, a3))
      maxa = Math.max(a1, Math.max(a2, a3))
      xinit = Math.min(mina - 0.5, -1.3)
      xfin = Math.max(maxa + 0.5, 1.3)
      antecedentsYval.push(a1, a2, a3)
    } else if (num <= 9) {
      // fonction a x^3 + b x² + c x + d --> trois à l’endroit
      // pour num === 8
      // fonction a x^3 + b x² + c x + d --> trois à l’envers
      // pour num === 9
      // Construit un polynôme de degré  3 tel que y a 1 antécédent x
      a1 = j3pGetRandomInt(-30, 30) / 10
      do {
        yval = j3pGetRandomInt(-5, 5)
      } while (Math.abs(yval) < 1)
      polyinter = construitPoly3Unantecedent(1, a1, yval)
      for (let i = 0; i < polyinter.length; i++) {
        polyinter[i] = (num === 8) ? j3pArrondi(polyinter[i], 3) : -j3pArrondi(polyinter[i], 3)
      }
      if (num === 9) yval = -yval
      mina = a1
      maxa = a1
      xinit = Math.min(mina - 0.5 - j3pGetRandomInt(0, 1) * 0.5, -1.8)
      xfin = Math.max(maxa + 0.5 + j3pGetRandomInt(0, 1) * 0.5, 1.8)
      antecedentsYval.push(a1)
    }
    const minRepx = xinit - decalage
    const maxRepx = xfin + decalage
    // je m’intéresse maintenant aux ordonnées pour voir quel intervelle doit être visible
    const Fct = genereFonction(num, polyinter) // Fct.fct est la fonction, Fct.ecriture est son expression qu’on utilisera dans mtg32
    let minrepY
    let maxrepY
    const objMaxMin = maxMin(Fct.fct, xinit, xfin, 0.01)// contient les propriétés maxi, mini, absMax et absMin
    if (num === 1) {
      minrepY = Math.min(objMaxMin.mini - 2, -5.05)
      maxrepY = Math.max(objMaxMin.maxi + 2, 5.05)
      do {
        yval = Math.round(10 * (Math.random() * (maxrepY - minrepY - 1) + minrepY + 0.5)) / 10
      } while ((Math.abs(yval - objMaxMin.mini) <= 0.2) || (Math.abs(yval - objMaxMin.maxi) <= 0.2))
      stor.xval = (yval - polyinter[0]) / polyinter[1]
      if ((stor.xval >= xinit) && (stor.xval <= xfin)) {
        antecedentsYval.push(stor.xval)
      }
    } else if (num <= 3) {
      minrepY = Math.min(objMaxMin.mini - 2, -5.2)
      maxrepY = Math.max(objMaxMin.maxi + 3.5, 5.2)
    } else if (num === 4) {
      yval = Math.floor(2 * (objMaxMin.mini - 2 - j3pGetRandomInt(0, 3))) / 2
      minrepY = Math.min(yval - 2, -5.2)
      maxrepY = Math.max(objMaxMin.maxi + 3.5, 5.2)
    } else if (num === 5) {
      yval = Math.ceil(2 * (objMaxMin.maxi + 2 + j3pGetRandomInt(0, 3))) / 2
      minrepY = Math.min(objMaxMin.mini - 2, -5.2)
      maxrepY = Math.max(yval + 3.5, 5.2)
    } else if (num <= 7) {
      minrepY = Math.min(objMaxMin.mini - 2.5, -5.2)
      maxrepY = Math.max(objMaxMin.maxi + 2.5, 5.2)
    } else {
      minrepY = Math.min(objMaxMin.mini - 3.5, -5.2)
      maxrepY = Math.max(objMaxMin.maxi + 3.5, 5.2)
    }
    // console.log("maxrepY:",maxrepY,"  minrepY:",minrepY)
    const pasGrady = (maxrepY - minrepY > 25)
      ? 5
      : (maxrepY - minrepY > 18)
          ? 4
          : (maxrepY - minrepY > 12) ? 2 : 1
    return {
      coefPoly: polyinter,
      bornes: [xinit, xfin],
      bornesOrd: [minrepY, maxrepY],
      bornesAbs: [minRepx, maxRepx],
      Fct,
      yval,
      anteYval: antecedentsYval,
      pasGrady
    }
  }

  function genereFonction (quest, tabCoef) {
    // quest sert à identifier les différents cas de figure
    // tabCoef est le tableau des coefficients de chaque monome dans le polynôme
    let f, ecritureFct
    if (quest === 1) {
      f = function (x) {
        return tabCoef[0] + tabCoef[1] * x
      }
      ecritureFct = tabCoef[0] + '+(' + tabCoef[1] + '*x)'
    } else if (quest <= 5) {
      f = function (x) {
        return tabCoef[0] + tabCoef[1] * x + tabCoef[2] * Math.pow(x, 2)
      }
      ecritureFct = tabCoef[0] + '+(' + tabCoef[1] + '*x)' + '+(' + tabCoef[2] + '*x^2)'
    } else {
      f = function (x) {
        return tabCoef[0] + tabCoef[1] * x + tabCoef[2] * Math.pow(x, 2) + tabCoef[3] * Math.pow(x, 3)
      }
      ecritureFct = tabCoef[0] + '+(' + tabCoef[1] + '*x)' + '+(' + tabCoef[2] + '*x^2)' + '+(' + tabCoef[3] + '*x^3)'
    }
    return { fct: f, ecriture: ecritureFct }
  }

  function enonceMain () {
    /// /////////////////////////////////////// Pour MathGraph32
    const largFig = 400// on gère ici la largeur de la figure
    const hautFig = 400
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // création du div accueillant la figure mtg32
    stor.zoneMtg32 = j3pAddElt(me.zonesElts.MD, 'div')
    // et enfin la zone svg (très importante car tout est dedans)
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(stor.zoneMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
    // ce qui suit lance la création initiale de la figure
    depart()
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })

    if ((ds.nbetapes === 1) || ((me.questionCourante % ds.nbetapes) === 1)) {
    // on génère une nouvelle fonction et donc une nouvelle courbe
      const quest = stor.quest[(me.questionCourante - 1) / ds.nbetapes]
      stor.objFct = defFonction(quest)
      me.logIfDebug('stor.objFct:', stor.objFct)
      // J’ai alors la fonction objFct.Fct.fct et son expression qui sera renvoyée à mtg32 :objFct.Fct.ecriture
      // objFct.coefPoly est le tableau des coefficient du polynôme
      // J’ai aussi les bornes (domaine de def objFct.bornes et celles du repère visible : objFct.bornesAbs et objFct.bornesOrd)
      // J’ai enfin objFct.yval qui est la valeur dont on cherchera les antécédents et objFct.anteYval le tableau des antécédents de ces valeurs
      for (let i = stor.objFct.coefPoly.length; i <= 4; i++) {
        stor.objFct.coefPoly.push(0)
      }
      stor.intervalle = '[' + j3pVirgule(stor.objFct.bornes[0]) + '\\quad;\\quad' + j3pVirgule(stor.objFct.bornes[1]) + ']'
      const tabNomf = ['f', 'g', 'h']
      stor.nomf = tabNomf[j3pGetRandomInt(0, 2)]
    }
    modifFig({ correction: false })
    for (let i = 1; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    const laCons1 = (ds.avecIntervalle) ? textes.consigne1_1 : textes.consigne1_2
    const objCons1 = { f: stor.nomf }
    if (ds.avecIntervalle) {
      objCons1.i = stor.intervalle
    } else {
      objCons1.a = j3pVirgule(stor.objFct.bornes[0])
      objCons1.b = j3pVirgule(stor.objFct.bornes[1])
    }
    j3pAffiche(stor.zoneCons1, '', laCons1, objCons1)
    const laCons2 = (stor.typeQuest[me.questionCourante - 1] === 'image')
      ? textes.consigne2_1
      : (stor.typeQuest[me.questionCourante - 1] === 'antecedents')
          ? textes.consigne2_2
          : textes.consigne2_3
    let mesZonesSaisie = []
    if (stor.typeQuest[me.questionCourante - 1] === 'image') {
      // Je cherche une valeur de x dont je vais demander l’image :
      let xval, valAcceptee
      do {
        xval = Math.round(10 * (stor.objFct.bornes[0] + 0.5 + Math.random() * (stor.objFct.bornes[1] - stor.objFct.bornes[0] - 1))) / 10
        valAcceptee = true
        for (let i = 0; i < stor.objFct.anteYval.length; i++) {
          valAcceptee = valAcceptee && (Math.abs(stor.objFct.anteYval[i] - xval) >= 0.5)
        }
        valAcceptee = (valAcceptee && (Math.abs(xval) >= 0.3))
      } while (!valAcceptee)
      stor.xval = xval
      const elt = j3pAffiche(stor.zoneCons2, '', laCons2, {
        x: j3pVirgule(xval),
        f: stor.nomf,
        input1: { texte: '', dynamique: true }
      })
      stor.zoneInput = [elt.inputList[0]]
      stor.zoneInput[0].addEventListener('input', j3pRemplacePoint)
      j3pRestriction(stor.zoneInput[0], '0-9\\.,\\-')
      const approximation = (stor.quest[(me.questionCourante - 1) / ds.nbetapes] <= 7) ? 0.25 : 0.4
      stor.zoneInput[0].typeReponse = ['nombre', 'approche', approximation]
      stor.zoneInput[0].reponse = [stor.objFct.Fct.fct(xval)]
      j3pAffiche(stor.zoneCons3, '', textes.consigne4_1)
      j3pAffiche(stor.zoneCons4, '', textes.consigne5)
      const zoneItalique = [stor.zoneCons3, stor.zoneCons4]
      zoneItalique.forEach((elt) => { j3pStyle(elt, { fontStyle: 'italic', fontSize: '0.9em', paddingTop: '10px' }) })
      mesZonesSaisie.push(stor.zoneInput[0].id)
      stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
      me.logIfDebug('réponse:', stor.zoneInput[0].reponse)
      j3pFocus(stor.zoneInput[0])
    } else {
      if (stor.typeQuest[me.questionCourante - 1] === 'antecedents') {
        stor.objCons2 = {
          y: j3pVirgule(stor.objFct.yval),
          f: stor.nomf,
          liste1: { texte: ['', textes.choix1, textes.choix2, textes.choix3, textes.choix4] }
        }
      } else {
        stor.objCons2 = {
          y: j3pVirgule(stor.objFct.yval),
          f: stor.nomf,
          liste1: { texte: ['', textes.choix1_1, textes.choix2_1, textes.choix3_1, textes.choix4_1] }
        }
      }
      const elt1 = j3pAffiche(stor.zoneCons2, '', laCons2, stor.objCons2)
      stor.listeInput = elt1.selectList[0]
      stor.objCons3 = {}
      mesZonesSaisie.push(stor.listeInput.id)
      me.logIfDebug('antécédents:', stor.objFct.anteYval)
      stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
      stor.listeInput.addEventListener('change', function () {
        const monIndex = this.selectedIndex
        j3pEmpty(stor.zoneCons3)
        if (stor.zoneInfo) j3pEmpty(stor.zoneInfo)
        else stor.zoneInfo = j3pAddElt(stor.zoneCons4, 'div')
        stor.zoneInput = []
        if (monIndex >= 1) {
          let laCons3 = ''
          stor.listeInput.style.color = me.styles.toutpetit.enonce.color
          if (monIndex === 1) {
            mesZonesSaisie = [stor.listeInput.id]
          } else if (monIndex === 2) { // une unique solution
            laCons3 = (stor.typeQuest[me.questionCourante - 1] === 'antecedents') ? textes.consigne3_1 : textes.consigne3_3
            stor.objCons3.input1 = { texte: '', dynamique: true }
            const elt = j3pAffiche(stor.zoneCons3, '', laCons3, stor.objCons3)
            stor.zoneInput = [elt.inputList[0]]
            j3pRestriction(stor.zoneInput[0], '0-9\\.,\\-')
            stor.zoneInput[0].addEventListener('input', j3pRemplacePoint)
            mesZonesSaisie = [stor.listeInput.id, stor.zoneInput[0].id]
            j3pAffiche(stor.zoneInfo, '', textes.consigne4_1)
          } else {
            // Ajout du texte à textes.consigne3_2
            let AjoutTxt = ''
            stor.objCons3.input1 = { texte: '', dynamique: true }
            for (let j = 2; j < monIndex - 1; j++) {
              AjoutTxt += '; @' + j + '@'
              stor.objCons3['input' + j] = { texte: '', dynamique: true }
            }
            // et pour le dernier
            AjoutTxt += ' ' + textes.et + ' @' + (monIndex - 1) + '@'
            stor.objCons3['input' + (monIndex - 1)] = { texte: '', dynamique: true }
            laCons3 = (stor.typeQuest[me.questionCourante - 1] === 'antecedents') ? textes.consigne3_2 : textes.consigne3_4
            laCons3 += AjoutTxt
            const elt = j3pAffiche(stor.zoneCons3, '', laCons3, stor.objCons3)
            stor.zoneInput = [...elt.inputList]
            mesZonesSaisie = [stor.listeInput.id].concat(stor.zoneInput.map(elt => elt.id))
            stor.zoneInput.forEach(eltInput => {
              j3pRestriction(eltInput, '0-9\\.,\\-')
              eltInput.addEventListener('input', j3pRemplacePoint)
            })
            j3pAffiche(stor.zoneInfo, '', textes.consigne4_2)
          }
        }
        if (monIndex > 1) j3pFocus(stor.zoneInput[0])
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: mesZonesSaisie,
          validePerso: mesZonesSaisie
        })
        stor.mesZonesSaisie = mesZonesSaisie
      })
      const zoneItalique = [stor.zoneCons4, stor.zoneCons6]
      zoneItalique.forEach((elt) => { j3pStyle(elt, { fontStyle: 'italic', fontSize: '0.9em', paddingTop: '10px' }) })
      j3pStyle(stor.zoneCons6, { paddingTop: '15px' })
      j3pAffiche(stor.zoneCons6, '', textes.consigne5)
    }
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div')

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.6 })

        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        const leTitre = (ds.typeQuest === 'image')
          ? textes.titre_exo1
          : (ds.typeQuest === 'antecedents')
              ? textes.titre_exo2
              : (ds.typeQuest === 'equation')
                  ? textes.titre_exo3
                  : (ds.typeQuest === 'image et antecedents')
                      ? textes.titre_exo4
                      : textes.titre_exo5
        me.afficheTitre(leTitre)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // code de création des fenêtres

        if ((ds.typeQuest === 'image et antecedents') || (ds.typeQuest === 'image et equation')) {
          ds.nbetapes = 2
        }
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        stor.typeQuest = []
        for (let i = 1; i <= ds.nbrepetitions; i++) {
          if (ds.typeQuest === 'image et antecedents') {
            stor.typeQuest.push('image', 'antecedents')
          } else if (ds.typeQuest === 'image et equation') {
            stor.typeQuest.push('image', 'equation')
          } else {
            stor.typeQuest.push(ds.typeQuest)
          }
        }
        stor.quest = []
        const questInit = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        for (let i = 0; i < ds.nbrepetitions; i++) {
          const pioche = j3pGetRandomInt(0, (questInit.length - 1))
          stor.quest.push(questInit[pioche])
          questInit.splice(pioche, 1)
          if (questInit.length === 0) questInit.push(1, 2, 3, 4, 5, 6, 7, 8, 9)
        }
        me.logIfDebug('stor.quest:', stor.quest)
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      let estCalcul = false
      let reponse
      if (stor.typeQuest[me.questionCourante - 1] === 'image') {
        reponse = fctsValid.validationGlobale()
        if (reponse.aRepondu && !reponse.bonneReponse) {
          // je vérifie qu’il me donne un nombre et non un calcul
          estCalcul = (isNaN(j3pNombre(fctsValid.zones.reponseSaisie[0])) && !isNaN(j3pCalculValeur(fctsValid.zones.reponseSaisie[0])))
        }
      } else {
        reponse = { aRepondu: false, bonneReponse: false }
        reponse.aRepondu = fctsValid.valideReponses()
      }
      // A cet instant reponse contient deux éléments :
      // reponse.aRepondu qui me dit si les 4 zones ont bien été complétées ou non
      // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses
      let bonneRep1
      if (reponse.aRepondu && (stor.typeQuest[me.questionCourante - 1] !== 'image')) {
        bonneRep1 = (stor.listeInput.selectedIndex === stor.objFct.anteYval.length + 1)
        let bonneRep2 = true
        const inputCorrect = []
        if (stor.mesZonesSaisie.length > 1) {
          for (let i = 1; i < stor.mesZonesSaisie.length; i++) {
            inputCorrect.push(false)
          }
        }
        if (bonneRep1 && (stor.listeInput.selectedIndex > 1)) {
          // Il a le bon nombre d’antécédents (et celui-ci n’est aps nul)
          // Je teste alors les solutions proposées s’il y en a
          // On va chercher si toutes les réponses attendues sont présentes.
          const repAttendues = [...stor.objFct.anteYval]
          for (let j = 1; j < stor.mesZonesSaisie.length; j++) {
            for (let i = 0; i < repAttendues.length; i++) {
              if (Math.abs(repAttendues[i] - j3pNombre(fctsValid.zones.reponseSaisie[j])) <= 0.25) {
                inputCorrect[j - 1] = true
                repAttendues.splice(i, 1)
              }
            }
            bonneRep2 = (bonneRep2 && inputCorrect[j - 1])
          }
          for (let j = 1; j < stor.mesZonesSaisie.length; j++) {
            estCalcul = estCalcul || (isNaN(j3pNombre(fctsValid.zones.reponseSaisie[j])) && !isNaN(j3pCalculValeur(fctsValid.zones.reponseSaisie[j])))
          }
        }
        reponse.bonneReponse = (bonneRep1 && bonneRep2)
        fctsValid.zones.bonneReponse[0] = bonneRep1
        fctsValid.coloreUneZone(stor.listeInput.id)
        if (stor.mesZonesSaisie.length > 1) {
          for (let i = 1; i < stor.mesZonesSaisie.length; i++) {
            fctsValid.zones.bonneReponse[i] = inputCorrect[i - 1]
            fctsValid.coloreUneZone(stor.zoneInput[i - 1].id)
          }
        }
      }

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        return me.finCorrection()
      }

      // Une réponse a été saisie
      if (reponse.bonneReponse) {
        // Bonne réponse
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        afficheCorrection(true)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        me.cacheBoutonValider()
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      if (stor.typeQuest[me.questionCourante - 1] === 'antecedents') {
        if (!bonneRep1) {
          stor.zoneCorr.innerHTML += '<br/>' + textes.comment1_1
        } else {
          stor.zoneCorr.innerHTML += '<br/>' + textes.comment2_1
        }
      } else if (stor.typeQuest[me.questionCourante - 1] === 'equation') {
        if (!bonneRep1) {
          stor.zoneCorr.innerHTML += '<br/>' + textes.comment1_2
        } else {
          stor.zoneCorr.innerHTML += '<br/>' + textes.comment2_2
        }
      }
      if (estCalcul) {
        stor.zoneCorr.innerHTML += '<br/>' + textes.comment3
      }
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // indication éventuelle ici
        // ici il a encore la possibilité de se corriger
        return me.finCorrection()
      }

      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(false)
      if (stor.typeQuest[me.questionCourante - 1] === 'image') {
        me.typederreurs[4]++
      } else if (stor.typeQuest[me.questionCourante - 1] === 'antecedents') {
        me.typederreurs[5]++
      } else {
        me.typederreurs[6]++
      }
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if (me.score / ds.nbitems >= ds.seuilReussite) {
          me.parcours.pe = ds.pe_1
        } else {
          if (me.typederreurs[4] / ds.nbrepetitions > ds.seuilErreur) { // problèmes avec les images
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[5] / ds.nbrepetitions > ds.seuilErreur) { // problèmes avec les antécédents
            me.parcours.pe = ds.pe_3
          } else if (me.typederreurs[6] / ds.nbrepetitions > ds.seuilErreur) { // problèmes avec les équations
            me.parcours.pe = ds.pe_4
          } else { // Il n’a pas bien réussi
            me.parcours.pe = ds.pe_5
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
