import { j3pAddElt, j3pNombre, j3pDetruit, j3pElement, j3pFocus, j3pGetRandomInt, j3pRemplacePoint, j3pRestriction, j3pStyle, j3pVirgule, j3pGetBornesIntervalle, j3pGetNewId } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import { j3pCreeTexte } from 'src/legacy/core/functionsSvg'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD
        avril 2019
        Résolution graphique d’équations de la forme f(x)=g(x)
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['avecIntervalle', true, 'boolean', 'Par défaut, l’ensemble de définition est donné sous la forme d’un intervalle, mais on peut modifier en donnant la valeur false à ce paramètre']
  ]

}
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  // les fonctions qui suivent sont celles dont on trace les courbes représentatives (utiles pour la correction)
  function f1 (x, alpha, beta) {
    return -0.0142857143 * Math.pow(x - alpha, 4) + 0.25 * Math.pow(x - alpha, 3) - 0.9285714286 * Math.pow(x - alpha, 2) - 0.75 * (x - alpha) + 2.4428571429 + beta
  }

  function f2 (x, alpha, beta) {
    return -0.0625 * Math.pow(x - alpha, 3) + 0.375 * Math.pow(x - alpha, 2) + 0.5 * (x - alpha) - 3 + beta
  }

  function f3 (x, alpha, beta) {
    return 0.1785714286 * Math.pow(x - alpha, 2) - 1.1785714286 * (x - alpha) + 0.6428571429 + beta
  }

  function f4 (x, alpha, beta) {
    return -0.3035714286 * Math.pow(x - alpha, 3) + 2.3035714286 * Math.pow(x - alpha, 2) - 2.3928571429 * (x - alpha) - 3 + beta
  }

  function f5 (x, alpha, beta) {
    return 0.25 * (x - alpha) - 1.5 + beta
  }

  function f6 (x, alpha, beta) {
    return -0.15 * Math.pow(x - alpha, 2) + 0.85 * (x - alpha) + 0.3 + beta
  }

  function f7 (x, alpha, beta) {
    return 2 * (x - alpha) - 5 + beta
  }

  function f8 (x, alpha, beta) {
    return 0.75 * (x - alpha) - 4.5 + beta
  }

  function f (num, x, alpha, beta) {
    if (num === 1) {
      return f1(x, alpha, beta)
    } else if (num === 2) {
      return f2(x, alpha, beta)
    } else if (num === 3) {
      return f3(x, alpha, beta)
    } else if (num === 4) {
      return f4(x, alpha, beta)
    } else if (num === 5) {
      return f5(x, alpha, beta)
    } else if (num === 6) {
      return f6(x, alpha, beta)
    } else if (num === 7) {
      return f7(x, alpha, beta)
    } else if (num === 8) {
      return f8(x, alpha, beta)
    }
  }

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let i
    stor.explications = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '15px' } })
    // le style n’est pas le même suivant que la réponse soit bonne ou fausse'
    stor.explications.style.color = (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
    j3pDetruit(stor.zoneCons4)
    for (i = 1; i <= 3; i++) stor['zoneCorr' + i] = j3pAddElt(stor.explications, 'div')
    j3pAffiche(stor.zoneCorr1, '', ds.textes.corr1, {
      f: stor.nomFcts[0],
      g: stor.nomFcts[1]
    })
    const laCorr2 = (stor.lesReponses.length === 0)
      ? ds.textes.corr2_1
      : (stor.lesReponses.length === 1) ? ds.textes.corr2_2 : ds.textes.corr2_3
    j3pAffiche(stor.zoneCorr2, '', laCorr2, { n: stor.lesReponses.length })
    if (stor.lesReponses.length === 0) {
      j3pAffiche(stor.zoneCorr3, '', ds.textes.corr3_1)
    } else if (stor.lesReponses.length === 1) {
      j3pAffiche(stor.zoneCorr3, '', ds.textes.corr3_2, { s: stor.lesReponses[0] })
    } else {
      let listeSol = j3pVirgule(stor.lesReponses[0])
      if (stor.lesReponses.length === 2) {
        listeSol += '\\text{ ' + ds.textes.et + ' }' + j3pVirgule(stor.lesReponses[1])
      } else {
        for (i = 1; i < stor.lesReponses.length - 1; i++) {
          listeSol += '\\quad ; \\quad ' + j3pVirgule(stor.lesReponses[i])
        }
        listeSol += '\\text{ ' + ds.textes.et + ' }' + j3pVirgule(stor.lesReponses[stor.lesReponses.length - 1])
      }
      j3pAffiche(stor.zoneCorr3, '', ds.textes.corr3_3, { s: listeSol })
    }
    creationRepere(stor.objRep, true, stor.explications.style.color)
  }

  function creationRepere (obj, correction, colorCorr) {
    // je ne m’embête pas car cette fonction est un copier-coller de code. Du coup je renomme mes variables sans l’objet
    const largRepere = obj.largRepere
    const hautRepere = obj.hautRepere
    const uniterep = obj.uniterep
    const colorFct1 = obj.colorFct1
    const colorFct2 = obj.colorFct2
    const absMin = obj.absMin
    const tabIntervalleMin = obj.tabIntervalleMin
    const tabIntervalleMax = obj.tabIntervalleMax
    const casFigure = obj.casFigure
    const alpha = obj.alpha
    const beta = obj.beta
    const tabNomsFcts1 = obj.tabNom[0]
    const tabNomsFcts2 = obj.tabNom[1]
    let i
    if (!correction) {
      stor.repere = new Repere({
        idConteneur: stor.maFigure,
        idDivRepere: stor.idRepere,
        aimantage: false,
        visible: true,
        larg: largRepere,
        haut: hautRepere,

        pasdunegraduationX: 1,
        pixelspargraduationX: uniterep,
        pasdunegraduationY: 1,
        pixelspargraduationY: uniterep,
        xO: -absMin * uniterep + 10,
        debuty: 0,
        negatifs: true,
        fixe: true,
        trame: true,
        objets: [
          {
            type: 'courbe',
            nom: j3pGetNewId('cb1'),
            par1: tabNomsFcts1[casFigure],
            par2: (tabIntervalleMin[casFigure] + alpha),
            par3: (tabIntervalleMax[casFigure] + alpha),
            par4: 100,
            style: { couleur: colorFct1, epaisseur: 2 }
          },
          {
            type: 'courbe',
            nom: j3pGetNewId('cb2'),
            par1: tabNomsFcts2[casFigure],
            par2: (tabIntervalleMin[casFigure] + alpha),
            par3: (tabIntervalleMax[casFigure] + alpha),
            par4: 100,
            style: { couleur: colorFct2, epaisseur: 2 }
          }
        ]
      })
      stor.repere.construit()
    } else {
      const ordonnees = []
      for (i = 0; i < stor.lesReponses.length; i++) {
        ordonnees[i] = f(stor.nomFcts[2], stor.lesReponses[i], alpha, beta)
      }
      for (i = 0; i < stor.lesReponses.length; i++) {
        stor.repere.add({
          type: 'point',
          nom: 'A' + i,
          par1: stor.lesReponses[i],
          par2: 0,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: colorCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'point',
          nom: 'B' + i,
          par1: stor.lesReponses[i],
          par2: ordonnees[i],
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: colorCorr, epaisseur: 2, taille: 18 }
        })
        stor.repere.add({
          type: 'segment',
          nom: 's' + i,
          par1: 'A' + i,
          par2: 'B' + i,
          style: { couleur: colorCorr, epaisseur: 3 }
        })
      }
      stor.repere.construit()
    }
    j3pElement(stor.idRepere).style.marginTop = '10px'
    // je crée le nom des courbes
    const nomCb1TabX = [(3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.6 + alpha) * uniterep, (3.7 + alpha) * uniterep, (3.7 + alpha) * uniterep, (3.7 + alpha) * uniterep, (4.8 + alpha) * uniterep, (4.8 + alpha) * uniterep, (-1.2 + alpha) * uniterep, (3.6 + alpha) * uniterep, (1.5 + alpha) * uniterep]
    const nomCb1TabY = [(-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (-2.8 + beta) * uniterep, (0.5 + beta) * uniterep, (0.5 + beta) * uniterep, (0.5 + beta) * uniterep, (-1.2 + beta) * uniterep, (-1.2 + beta) * uniterep, (-0.2 + beta) * uniterep, (-1.8 + beta) * uniterep, (1.8 + beta) * uniterep]
    j3pCreeTexte('idRepere', {
      x: -absMin * uniterep + 10 + nomCb1TabX[casFigure],
      y: hautRepere / 2 - nomCb1TabY[casFigure],
      texte: 'C',
      taille: 12,
      couleur: colorFct1,
      italique: true,
      fonte: 'times new roman'
    })
    j3pCreeTexte('idRepere', {
      x: -absMin * uniterep + 10 + nomCb1TabX[casFigure] + 9,
      y: hautRepere / 2 - nomCb1TabY[casFigure] + 4,
      texte: stor.nomFcts[0],
      taille: 12,
      couleur: colorFct1,
      italique: true,
      fonte: 'times new roman'
    })
    const nomCb2TabX = [(3.7 + alpha) * uniterep, (4.8 + alpha) * uniterep, (3.2 + alpha) * uniterep, (1 + alpha) * uniterep, (-1.2 + alpha) * uniterep, (4.8 + alpha) * uniterep, (3.2 + alpha) * uniterep, (1 + alpha) * uniterep, (3.2 + alpha) * uniterep, (1 + alpha) * uniterep, (1 + alpha) * uniterep, (1.5 + alpha) * uniterep, (1.5 + alpha) * uniterep]
    const nomCb2TabY = [(0.5 + beta) * uniterep, (-1.2 + beta) * uniterep, (2.3 + beta) * uniterep, (-1 + beta) * uniterep, (-0.2 + beta) * uniterep, (-1.2 + beta) * uniterep, (2.3 + beta) * uniterep, (-1 + beta) * uniterep, (2.3 + beta) * uniterep, (-1 + beta) * uniterep, (-1 + beta) * uniterep, (-2.5 + beta) * uniterep, (-3.8 + beta) * uniterep]
    j3pCreeTexte('idRepere', {
      x: -absMin * uniterep + 10 + nomCb2TabX[casFigure],
      y: hautRepere / 2 - nomCb2TabY[casFigure],
      texte: 'C',
      taille: 12,
      couleur: colorFct2,
      italique: true,
      fonte: 'times new roman'
    })
    j3pCreeTexte('idRepere', {
      x: -absMin * uniterep + 10 + nomCb2TabX[casFigure] + 9,
      y: hautRepere / 2 - nomCb2TabY[casFigure] + 4,
      texte: stor.nomFcts[1],
      taille: 12,
      couleur: colorFct2,
      italique: true,
      fonte: 'times new roman'
    })
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 4,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      avecIntervalle: true,
      /*
      Les textes présents dans la section
      Sont donc accessibles dans le code par donneesSection.textes.consigne1
      Possibilité d’écrire dans le code :
      var lestextes: donneesSection.textes;
      puis accès à l’aide de lestextes.consigne1
      */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Equation de la forme f(x) = g(x)<br>Résolution graphique',
        // on donne les phrases de la consigne
        consigne1: 'Dans le repère ci-contre, on a tracé les courbes représentant les fonctions $£f$ (en vert) et $£g$ (en violet) définies sur l’intervalle $£i$.',
        consigne1_2: 'Dans le repère ci-contre, entre les abscisses $£a$ et $£b$, on a tracé les courbes représentant les fonctions $£f$ (en vert) et $£g$ (en violet).',
        consigne2: 'Par lecture graphique, l’équation $£f(x)=£g(x)$ #1#.',
        consigne3_1: 'Cette solution est @1@.',
        consigne3_2: 'Ces solutions sont @1@',
        choix1: 'n’admet aucune solution',
        choix2: 'admet exactement 1 solution',
        choix3: 'admet exactement 2 solutions',
        choix4: 'admet exactement 3 solutions',
        choix5: 'admet exactement 4 solutions',
        consigne4: 'Les valeurs attendues correspondent aux graduations indiquées sur les axes.',
        et: 'et',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Le nombre de solutions proposé n’est pas le bon&nbsp;!',
        comment2: 'Le nombre de solutions est pourtant correct&nbsp;!',
        comment3: 'La réponse donnée doit être un nombre et non un calcul&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'Les solutions de l’équation $£f(x)=£g(x)$ sont les abscisses des points d’intersection des deux courbes.',
        corr2_1: 'Ici, les courbes n’ont aucun point d’intersection.',
        corr2_2: 'Ici, les courbes ont un unique point d’intersection.',
        corr2_3: 'Ici, les courbes ont £n points d’intersection.',
        corr3_1: 'Donc l’équation n’admet pas de solution.',
        corr3_2: 'Donc l’unique solution de l’équation est $£s$',
        corr3_3: 'Donc les solutions de l’équation sont $£s$.'

      },

      pe: 0
    }
  }
  function enonceMain () {
    const tabNomsFcts = ['f', 'g', 'h']
    const nomF = tabNomsFcts[j3pGetRandomInt(0, 2)]
    let nomG
    do {
      nomG = tabNomsFcts[j3pGetRandomInt(0, 2)]
    } while (nomF === nomG)
    // je dois choisir l’un des cas de figure
    const casFigure = j3pGetRandomInt(0, 12)
    // pour faire jouer l’aléatoire j’ai une translation de chaque courbe de référence par le vecteur alpha*vec(i)+beta*vec(j)'
    const tabAlpha = ['[-2;1]', '[-2;2]', '[-1;1]', '[-1;1]', '[-2;1]', '[0;2]', '[-2;1]', '[-1;2]', '[-2;1]', '[-2;1]', '[-1;1]', '[-2;0]', '[-2;0]']
    const [alphaMin, alphaMax] = j3pGetBornesIntervalle(tabAlpha[casFigure])
    const alpha = j3pGetRandomInt(alphaMin, alphaMax)
    const tabBeta = ['[-2;2]', '[-2;2]', '[-2;0]', '[-2;2]', '[-2;2]', '[-2;0]', '[-1;0]', '[-2;3]', '[-2;1]', '[-3;2]', '[-1;4]', '[-3;1]', '[-1;2]']
    const [betaMin, betaMax] = j3pGetBornesIntervalle(tabBeta[casFigure])
    const beta = j3pGetRandomInt(betaMin, betaMax)
    me.logIfDebug('casFigure:' + casFigure + '  alpha : ' + alpha + '  beta:' + beta)
    // intervalle correspondant au domaine de définition
    const tabIntervalleMin = [-3, -2, -2, -3, -1, -3, -1, -3, -2, -2, -3, -2, -2]
    const tabIntervalleMax = [7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 7, 7, 8]
    const ledomaine = '[' + (tabIntervalleMin[casFigure] + alpha) + '\\quad ;\\quad ' + (tabIntervalleMax[casFigure] + alpha) + ']'
    // bornes du repère
    const absMin = Math.min(tabIntervalleMin[casFigure] + alpha, -1) - 2
    const absMax = tabIntervalleMax[casFigure] + alpha + 1
    const ordMin = -6
    const ordMax = 6
    const betaTxt = (beta >= 0) ? '+' + beta : beta
    // écriture des fonctons pour créer les courbes dans le repère
    let fDeX1, fDeX2, fDeX3, fDeX4, fDeX5, fDeX6, fDeX7, fDeX8
    stor.idRepere = j3pGetNewId('unrepere')
    if (alpha < 0) {
      fDeX1 = '-0.0142857143*(x+' + (-alpha) + ')^4+0.25*(x+' + (-alpha) + ')^3-0.9285714286*(x+' + (-alpha) + ')^2-0.75*(x+' + (-alpha) + ')+2.4428571429' + betaTxt//
      fDeX2 = '-0.0625*(x+' + (-alpha) + ')^3+0.375*(x+' + (-alpha) + ')^2+0.5*(x+' + (-alpha) + ')-3' + betaTxt// g
      fDeX3 = '0.1785714286*(x+' + (-alpha) + ')^2-1.1785714286*(x+' + (-alpha) + ')+0.6428571429' + betaTxt// h
      fDeX4 = '-0.3035714286*(x+' + (-alpha) + ')^3+2.3035714286*(x+' + (-alpha) + ')^2-2.3928571429*(x+' + (-alpha) + ')-3' + betaTxt// p
      fDeX5 = '0.25*(x+' + (-alpha) + ')-1.5' + betaTxt// a
      fDeX6 = '-0.15*(x+' + (-alpha) + ')^2+0.85*(x+' + (-alpha) + ')+0.3' + betaTxt// r
      fDeX7 = '2*(x+' + (-alpha) + ')-5' + betaTxt
      fDeX8 = '0.75*(x+' + (-alpha) + ')-4.5' + betaTxt
    } else {
      fDeX1 = '-0.0142857143*(x-' + alpha + ')^4+0.25*(x-' + alpha + ')^3-0.9285714286*(x-' + alpha + ')^2-0.75*(x-' + alpha + ')+2.4428571429' + betaTxt//
      fDeX2 = '-0.0625*(x-' + alpha + ')^3+0.375*(x-' + alpha + ')^2+0.5*(x-' + alpha + ')-3' + betaTxt// g
      fDeX3 = '0.1785714286*(x-' + alpha + ')^2-1.1785714286*(x-' + alpha + ')+0.6428571429' + betaTxt// h
      fDeX4 = '-0.3035714286*(x-' + alpha + ')^3+2.3035714286*(x-' + alpha + ')^2-2.3928571429*(x-' + alpha + ')-3' + betaTxt// p
      fDeX5 = '0.25*(x-' + alpha + ')-1.5' + betaTxt// a
      fDeX6 = '-0.15*(x-' + alpha + ')^2+0.85*(x-' + alpha + ')+0.3' + betaTxt// r
      fDeX7 = '2*(x-' + (-alpha) + ')-5' + betaTxt
      fDeX8 = '0.75*(x-' + (alpha) + ')-4.5' + betaTxt
    }
    const tabNomsFcts1 = [fDeX1, fDeX1, fDeX1, fDeX1, fDeX1, fDeX2, fDeX2, fDeX2, fDeX3, fDeX3, fDeX6, fDeX3, fDeX6]
    const tabNomsFcts2 = [fDeX2, fDeX3, fDeX4, fDeX5, fDeX6, fDeX3, fDeX4, fDeX5, fDeX4, fDeX5, fDeX5, fDeX7, fDeX8]

    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (ds.avecIntervalle) {
      j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, {
        f: nomF,
        g: nomG,
        i: ledomaine
      })
    } else {
      j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1_2, {
        f: nomF,
        g: nomG,
        a: (tabIntervalleMin[casFigure] + alpha),
        b: (tabIntervalleMax[casFigure] + alpha)
      })
    }
    const elt = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, {
      f: nomF,
      g: nomG,
      liste1: { texte: ['', ds.textes.choix1, ds.textes.choix2, ds.textes.choix3, ds.textes.choix4, ds.textes.choix5] }
    })
    stor.listeInput = elt.selectList[0]
    let mesZonesSaisie = []
    stor.fctsValides = new ValidationZones({
      parcours: me,
      zones: [stor.listeInput.id]
    })
    stor.zoneInput = []
    stor.listeInput.addEventListener('change', function () {
      const monIndex = this.selectedIndex
      try {
        stor.zoneCons3.innerText = ''
      } catch (e) {
        console.warn(e) // pas normal mais pas grave
      }
      if (monIndex >= 1) {
        stor.listeInput.style.color = me.styles.toutpetit.enonce.color
        stor.objCons3 = {}
        if (monIndex === 1) {
          mesZonesSaisie = [stor.listeInput.id]
        } else if (monIndex === 2) { // une unique solution
          stor.objCons3.input1 = { texte: '', dynamique: true }
          const elt = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3_1, stor.objCons3)
          j3pRestriction(elt.inputList[0], '0-9\\.,\\-')
          stor.zoneInput = [elt.inputList[0]]
          stor.zoneInput[0].addEventListener('input', j3pRemplacePoint)
          mesZonesSaisie = [stor.listeInput.id, stor.zoneInput[0].id]
        } else {
          // Ajout du texte à ds.textes.consigne3_2
          let AjoutTxt = ''
          stor.objCons3.input1 = { texte: '', dynamique: true }
          for (let j = 2; j < monIndex - 1; j++) {
            AjoutTxt += '; @' + j + '@'
            stor.objCons3['input' + j] = { texte: '', dynamique: true }
          }
          // et pour le dernier
          AjoutTxt += ' ' + ds.textes.et + ' @' + (monIndex - 1) + '@'
          stor.objCons3['input' + (monIndex - 1)] = { texte: '', dynamique: true }
          const elts = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3_2 + AjoutTxt, stor.objCons3)
          stor.zoneInput = [...elts.inputList]
          mesZonesSaisie = [stor.listeInput.id].concat(stor.zoneInput.map(elt => elt.id))
          for (let i = 1; i < monIndex; i++) {
            j3pRestriction(stor.zoneInput[i - 1], '0-9\\.,\\-')
            stor.zoneInput[i - 1].addEventListener('input', j3pRemplacePoint)
          }
        }
        if (monIndex > 1) j3pFocus(stor.zoneInput[0])
      }
      stor.fctsValides = new ValidationZones({
        parcours: me,
        zones: mesZonesSaisie,
        validePerso: mesZonesSaisie
      })
      stor.mesZonesSaisie = mesZonesSaisie
    })
    // Je récupère les réponses attendues suivant les cas de figure
    const lesReponses = []
    let numf1// Numéros des fonctions dont on a tracé les courbes
    switch (casFigure) {
      case 0:
        numf1 = 1
        lesReponses.push(-2 + alpha, 2 + alpha, 6 + alpha)
        break
      case 1:
        numf1 = 1
        lesReponses.push(-1 + alpha, 2 + alpha, 6 + alpha)
        break
      case 2:
        numf1 = 1
        lesReponses.push(-1 + alpha, 2 + alpha, 6 + alpha)
        break
      case 3:
        numf1 = 1
        lesReponses.push(-2 + alpha, 2 + alpha, 6 + alpha)
        break
      case 4:
        numf1 = 1
        lesReponses.push(1 + alpha, 6 + alpha)
        break
      case 5:
        numf1 = 2
        lesReponses.push(2 + alpha, 6 + alpha)
        break
      case 6:
        numf1 = 2
        lesReponses.push(alpha, 2 + alpha, 6 + alpha)
        break
      case 7:
        numf1 = 2
        lesReponses.push(-2 + alpha, 2 + alpha, 6 + alpha)
        break
      case 8:
        numf1 = 3
        lesReponses.push(-1 + alpha, 2 + alpha, 6 + alpha)
        break
      case 9:
        numf1 = 3
        lesReponses.push(2 + alpha, 6 + alpha)
        break
      case 10:
        numf1 = 6
        lesReponses.push(-2 + alpha, 6 + alpha)
        break
      case 11:
        numf1 = 3
        lesReponses.push(2 + alpha)
        break
      case 12:
        numf1 = 6
        lesReponses.push(6 + alpha)
        break
    }
    stor.lesReponses = lesReponses
    /// /////////////////////////////////////////////Pour les courbes
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // création du div accueillant la figure mtg32
    stor.maFigure = j3pAddElt(stor.conteneurD, 'div')
    stor.maCorrection = j3pAddElt(stor.conteneurD, 'div')
    // construction du repère
    const uniterep = 30
    const objRep = {
      largRepere: ((absMax - absMin) + 1) * uniterep,
      hautRepere: ((ordMax - ordMin) + 1) * uniterep,
      uniterep: 30,
      colorFct1: '#008000',
      colorFct2: '#FF00FF',
      absMin,
      tabIntervalleMin,
      tabIntervalleMax,
      casFigure,
      alpha,
      beta,
      tabNom: [tabNomsFcts1, tabNomsFcts2]
    }
    stor.nomFcts = [nomF, nomG, numf1]// J’ajoute numf1 pour le récupérer pour la correction
    creationRepere(objRep, false)
    stor.objRep = objRep
    // Obligatoire
    j3pAffiche(stor.zoneCons4, '', ds.textes.consigne4)
    j3pStyle(stor.zoneCons4, { fontStyle: 'italic', fontSize: '0.85em' })
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })

        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        let estCalcul = false
        const reponse = {
          aRepondu: stor.fctsValides.valideReponses(),
          bonneReponse: false
        }
        let j, bonneRep1
        if (reponse.aRepondu) {
          bonneRep1 = (stor.listeInput.selectedIndex === stor.lesReponses.length + 1)
          let bonneRep2 = true
          const inputCorrect = []
          if (stor.mesZonesSaisie.length > 1) {
            for (let i = 1; i < stor.mesZonesSaisie.length; i++) {
              inputCorrect.push(false)
            }
          }
          if (bonneRep1 && (stor.listeInput.selectedIndex > 1)) {
          // Il a le bon nombre d’antécédents (et celui-ci n’est pas nul)
          // Je teste alors les solutions proposées s’il y en a
          // On va chercher si toutes les réponses attendues sont présentes.
            const repAttendues = [...stor.lesReponses]
            for (j = 1; j < stor.mesZonesSaisie.length; j++) {
              for (let i = 0; i < repAttendues.length; i++) {
                if (Math.abs(repAttendues[i] - j3pNombre(stor.fctsValides.zones.reponseSaisie[j])) <= 0.25) {
                  inputCorrect[j - 1] = true
                  repAttendues.splice(i, 1)
                }
              }
              bonneRep2 = (bonneRep2 && inputCorrect[j - 1])
            }
            for (j = 1; j < stor.mesZonesSaisie.length; j++) {
              estCalcul = estCalcul || (isNaN(j3pNombre(stor.fctsValides.zones.reponseSaisie[j])) && !isNaN(j3pCalculValeur(stor.fctsValides.zones.reponseSaisie[j])))
            }
          }
          reponse.bonneReponse = (bonneRep1 && bonneRep2)
          stor.fctsValides.zones.bonneReponse[0] = bonneRep1
          stor.fctsValides.coloreUneZone(stor.listeInput.id)
          if (stor.mesZonesSaisie.length > 1) {
            for (let i = 1; i < stor.mesZonesSaisie.length; i++) {
              stor.fctsValides.zones.bonneReponse[i] = inputCorrect[i - 1]
              stor.fctsValides.coloreUneZone(stor.zoneInput[i - 1].id)
            }
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // On peut ajouter un commentaire particulier.
          me.reponseManquante(stor.maCorrection, false)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.maCorrection.style.color = me.styles.cbien
            stor.maCorrection.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.maCorrection.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.maCorrection.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.maCorrection.innerHTML = cFaux
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
              if (!bonneRep1) {
                stor.maCorrection.innerHTML += '<br/>' + ds.textes.comment1
              } else {
                stor.maCorrection.innerHTML += '<br/>' + ds.textes.comment2
              }
              if (estCalcul) {
                stor.maCorrection.innerHTML += '<br/>' + ds.textes.comment3
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.maCorrection.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.maCorrection.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
