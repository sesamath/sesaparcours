import { j3pAddElt, j3pAddContent, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pChaine, j3pElement, j3pGetRandomInt, j3pVirgule, j3pGetNewId } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import tableauSignesVariations from 'src/legacy/outils/tableauSignesVariations'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2019
        A l’aide d’un tableau de variation, on demande de comparer deux images
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Comparaison sur un tableau de variations',
  // on donne les phrases de la consigne
  consigne1: 'On donne ci-dessous le tableau de variations d’une fonction $£f$ définie sur l’intervalle $£i$.',
  consigne2: 'Coche la bonne proposition :',
  consigne3: 'On ne peut pas comparer $£{i1}$ et $£{i2}$.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Fais une figure en respectant l’ordre des sommets et tu trouveras facilement les diagonales !',
  comment2: 'La fraction doit être irréductible !',
  comment3: 'Simplifie l’écriture de ta réponse !',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'On place les nombres $£{n1}$ et $£{n2}$ dans le tableau de variations comme représenté ci-dessous.',
  corr2_1: 'Comme $£f$ est strictement croissante sur $[£{n1}\\quad ; \\quad £{n2}]$, on conclut que $£f(£{n1}) < £f(£{n2})$.',
  corr2_2: 'Comme $£f$ est strictement décroissante sur $[£{n1}\\quad ; \\quad £{n2}]$, on conclut que $£f(£{n1}) > £f(£{n2})$.',
  corr2_3: 'Attention car sur l’intervalle $[£{n1}\\quad ; \\quad £{n2}]$ la fonction $£f$ n’est pas monotone (ni croissante, ni décroissante), il faut donc regarder les images possibles de $£{n1}$ et $£{n2}$ par $£f$.',
  corr2_4: 'Par exemple, on peut avoir $£f(£{n1})=£{i1}$ et $£f(£{n2})=£{i2}$ et dans ce cas $£f(£{n1})£{s1}£f(£{n2})$,',
  corr2_5: 'ou bien $£f(£{n1})=£{j1}$ et $£f(£{n2})=£{j2}$ et dans ce cas $£f(£{n1})£{s2}£f(£{n2})$.',
  corr2_6: 'On conclut alors qu’on ne peut pas comparer $£f(£{n1})$ et $£f(£{n2})$.'
  // corr2_3: 'Comme $£f$ n’est pas monotone sur $[£{n1}\\quad ; \\quad £{n2}]$ (ni croissante, ni décroissante sur cet intervalle), on conclut qu’on ne peut pas comparer $£f(£{n1})$ et $£f(£{n2})$.'
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  function calculDonneesTab (numQuest) {
    // numQuest vaut entre 1 et 3. Il permet de savoir quel cas de figure on prend (le deuxième étant un cas où on peut peut pas comparer les images
    const maxMin = j3pGetRandomInt(0, 1) * 2 - 1
    const valX = []
    const valY = []
    const tabVariations = []
    const tabVariations2 = []
    let choixAbs = []
    let choixOrd = []
    const ecartOrd = 4 + j3pGetRandomInt(0, 6)
    const ecartMin = 10
    let x1
    let x2
    let numRep// x1 et x2 sont les abscisses dont on va (tenter) de comparer les images. numRep est le numéro de la réponse : 1 pour <, 2 pour > et 3 sinon
    const imagesFictives = [] // elles vont servir à récuperer des images qui vont prouver qu’on ne peut pas comparer les images
    let im1, im2, im3, im4
    switch (numQuest) {
      case 1:
        valX.push(-20 + j3pGetRandomInt(0, 7))
        valX.push(valX[0] + ecartMin + j3pGetRandomInt(0, 6))
        valX.push(valX[1] + ecartMin + j3pGetRandomInt(0, 6))
        // On a un minimum en valY[1]
        valY[1] = 5 - j3pGetRandomInt(0, 14)
        valY[0] = valY[1] + ecartOrd + j3pGetRandomInt(0, 6)
        valY[2] = valY[1] + ecartOrd + j3pGetRandomInt(0, 6)
        choixAbs = [valX[0]]
        {
          const choixInt = j3pGetRandomInt(0, 1)
          for (let i = 0; i < valY.length; i++) {
            valY[i] = valY[i] * maxMin
          }
          if (choixInt === 0) {
            do {
              x1 = valX[0] + 3 + j3pGetRandomInt(0, 4)
              x2 = valX[1] - 2 - j3pGetRandomInt(0, 3)
            } while (x1 >= x2)
            choixAbs.push(x1, x2, valX[1], valX[2])
            choixOrd = [valY[0], '£f(' + x1 + ')', '£f(' + x2 + ')', valY[1], valY[2]]
          } else {
            do {
              x1 = valX[1] + 3 + j3pGetRandomInt(0, 4)
              x2 = valX[2] - 2 - j3pGetRandomInt(0, 3)
            } while (x1 >= x2)
            choixAbs.push(valX[1], x1, x2, valX[2])
            choixOrd = [valY[0], valY[1], '£f(' + x1 + ')', '£f(' + x2 + ')', valY[2]]
          }
          if (maxMin === 1) {
            tabVariations.push('decroit', 'croit')
            numRep = (choixInt === 0) ? 2 : 1
          } else {
            tabVariations.push('croit', 'decroit')
            numRep = (choixInt === 0) ? 1 : 2
          }
        }
        break
      case 2:
        valX.push(-20 + j3pGetRandomInt(0, 7))
        valX.push(valX[0] + ecartMin + j3pGetRandomInt(0, 6))
        valX.push(valX[1] + ecartMin + j3pGetRandomInt(0, 6))
        // valY[1] correspond au maximum
        valY[1] = -5 + j3pGetRandomInt(0, 14)
        valY[0] = valY[1] - ecartOrd - j3pGetRandomInt(0, 6)
        valY[2] = valY[1] - ecartOrd - j3pGetRandomInt(0, 6)
        // je prends 2 images entre valY[1] et min(valY[0], valY[2])
        do {
          im1 = j3pGetRandomInt(valY[0] + 1, valY[1] - 1)
          im2 = j3pGetRandomInt(valY[2] + 1, valY[1] - 1)
        } while (im1 >= im2)
        do {
          im3 = j3pGetRandomInt(valY[0] + 1, valY[1] - 1)
          im4 = j3pGetRandomInt(valY[2] + 1, valY[1] - 1)
        } while (im3 <= im4)
        for (let i = 0; i < valY.length; i++) valY[i] = valY[i] * maxMin
        if (maxMin === -1) {
          tabVariations.push('decroit', 'croit')
        } else {
          tabVariations.push('croit', 'decroit')
        }
        imagesFictives.push(im1 * maxMin, im2 * maxMin, im3 * maxMin, im4 * maxMin)
        numRep = 3
        x1 = valX[0] + 3 + j3pGetRandomInt(0, 4)
        x2 = valX[1] + 3 + j3pGetRandomInt(0, 4)
        choixAbs.push(valX[0], x1, valX[1], x2, valX[2])
        choixOrd = [valY[0], '£f(' + x1 + ')', valY[1], '£f(' + x2 + ')', valY[2]]
        break
      case 3:
        valX.push(-20 + j3pGetRandomInt(0, 7))
        valX.push(valX[0] + ecartMin + j3pGetRandomInt(0, 6))
        valX.push(valX[1] + ecartMin + j3pGetRandomInt(0, 6))
        valX.push(valX[2] + ecartMin + j3pGetRandomInt(0, 6))
        // ord2 correspond au minimum local
        valY[1] = 5 - j3pGetRandomInt(0, 14)
        valY[0] = valY[1] + ecartOrd + j3pGetRandomInt(0, 6)
        valY[2] = valY[1] + ecartOrd + j3pGetRandomInt(0, 6)
        do {
          valY[3] = valY[1] + 1 + j3pGetRandomInt(0, 4)
        } while (valY[3] >= valY[2])
        for (let i = 0; i < valY.length; i++) {
          valY[i] = valY[i] * maxMin
        }
        if (maxMin === 1) {
          tabVariations.push('decroit', 'croit', 'decroit')
        } else {
          tabVariations.push('croit', 'decroit', 'croit')
        }
        switch (j3pGetRandomInt(0, 2)) {
          case 0:
            do {
              x1 = valX[0] + 3 + j3pGetRandomInt(0, 4)
              x2 = valX[1] - 2 - j3pGetRandomInt(0, 3)
            } while (x1 >= x2)
            choixAbs.push(valX[0], x1, x2, valX[1], valX[2], valX[3])
            choixOrd = [valY[0], '£f(' + x1 + ')', '£f(' + x2 + ')', valY[1], valY[2], valY[3]]
            numRep = (maxMin === 1) ? 2 : 1
            break
          case 1:
            do {
              x1 = valX[1] + 3 + j3pGetRandomInt(0, 4)
              x2 = valX[2] - 2 - j3pGetRandomInt(0, 3)
            } while (x1 >= x2)
            choixAbs.push(valX[0], valX[1], x1, x2, valX[2], valX[3])
            choixOrd = [valY[0], valY[1], '£f(' + x1 + ')', '£f(' + x2 + ')', valY[2], valY[3]]
            numRep = (maxMin === 1) ? 1 : 2
            break
          default :
            do {
              x1 = valX[2] + 3 + j3pGetRandomInt(0, 4)
              x2 = valX[3] - 2 - j3pGetRandomInt(0, 3)
            } while (x1 >= x2)
            choixAbs.push(valX[0], valX[1], valX[2], x1, x2, valX[3])
            choixOrd = [valY[0], valY[1], valY[2], '£f(' + x1 + ')', '£f(' + x2 + ')', valY[3]]
            numRep = (maxMin === 1) ? 2 : 1
            break
        }
        break
    }
    return {
      valX,
      valY,
      tabVariations,
      tabVariations2,
      choixAbs,
      choixOrd,
      absComparaison: [x1, x2],
      numRep,
      imagesFictives
    }
  }

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    j3pElement('label' + stor.idsRadio[stor.objDonnees.numRep - 1]).style.color = zoneExpli.style.color
    for (let i = 1; i <= 3; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', textes.corr1, {
      n1: stor.objDonnees.absComparaison[0],
      n2: stor.objDonnees.absComparaison[1]
    })

    const valXChaine2 = []
    const valYChaine2 = []
    for (let i = 0; i < stor.objDonnees.choixAbs.length; i++) {
      valXChaine2.push(String(stor.objDonnees.choixAbs[i]))
      valYChaine2.push(j3pChaine(String(stor.objDonnees.choixOrd[i]), { f: stor.nomf }))
    }
    const objetGlobal2 = {}
    objetGlobal2.mef = {}
    objetGlobal2.mef.L = stor.largMin + 50
    objetGlobal2.mef.h = 50
    objetGlobal2.mef.macouleur = zoneExpli.style.color
    objetGlobal2.lignes = []
    objetGlobal2.lignes[0] = {}
    objetGlobal2.lignes[0].type = 'variations'
    objetGlobal2.lignes[0].hauteur = 110
    objetGlobal2.lignes[0].valeurs_charnieres = {}
    objetGlobal2.lignes[0].valeurs_charnieres.valeurs_approchees = []
    objetGlobal2.lignes[0].variations = []
    objetGlobal2.lignes[0].images = {}
    objetGlobal2.lignes[0].imagesapprochees = []
    objetGlobal2.lignes[0].expression = stor.nomf
    objetGlobal2.lignes[0].valeurs_charnieres.valeurs_theoriques = [...valXChaine2]
    objetGlobal2.lignes[0].valeurs_charnieres.valeurs_affichees = [...valXChaine2]
    objetGlobal2.lignes[0].valeurs_charnieres.valeurs_approchees = [...stor.objDonnees.choixAbs]
    objetGlobal2.lignes[0].variations = stor.objDonnees.tabVariations
    objetGlobal2.lignes[0].cliquable = []
    objetGlobal2.lignes[0].intervalles = []
    for (let i = 1; i < stor.valXChaine.length; i++) {
      objetGlobal2.lignes[0].intervalles.push([stor.valXChaine[i - 1], stor.valXChaine[i]])
    }
    objetGlobal2.lignes[0].images = []
    for (let i = 0; i < valXChaine2.length; i++) {
      objetGlobal2.lignes[0].images.push([valXChaine2[i], valYChaine2[i]])
    }

    objetGlobal2.lignes[0].imagesapprochees = [...stor.objDonnees.choixAbs]
    // on n’autorise pas l’ajout de lignes et d’intervalles
    objetGlobal2.ajout_lignes_possible = false
    objetGlobal2.ajout_intervalles_possible = false

    tableauSignesVariations(stor.zoneExpli2, j3pGetNewId('divTableau'), objetGlobal2, true)
    const laCorr3 = (stor.objDonnees.numRep === 1)
      ? textes.corr2_1
      : (stor.objDonnees.numRep === 2) ? textes.corr2_2 : textes.corr2_3
    j3pAffiche(stor.zoneExpli3, '', laCorr3, {
      f: stor.nomf,
      n1: stor.objDonnees.absComparaison[0],
      n2: stor.objDonnees.absComparaison[1]
    })
    if (stor.objDonnees.numRep === 3) {
      // J’ai d’autres commentaires à ajouter
      for (let i = 4; i <= 6; i++) {
        stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
        j3pAffiche(stor['zoneExpli' + i], '', textes['corr2_' + i], {
          f: stor.nomf,
          n1: stor.objDonnees.absComparaison[0],
          n2: stor.objDonnees.absComparaison[1],
          i1: stor.objDonnees.imagesFictives[0],
          i2: stor.objDonnees.imagesFictives[1],
          j1: stor.objDonnees.imagesFictives[2],
          j2: stor.objDonnees.imagesFictives[3],
          s1: (stor.objDonnees.imagesFictives[0] < stor.objDonnees.imagesFictives[1]) ? '<' : '>',
          s2: (stor.objDonnees.imagesFictives[2] < stor.objDonnees.imagesFictives[3]) ? '<' : '>'
        })
      }
    }
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.objDonnees = calculDonneesTab(quest)
    const valXChaine = []
    const valYChaine = []
    for (let i = 0; i < stor.objDonnees.valX.length; i++) {
      valXChaine.push(String(stor.objDonnees.valX[i]))
      valYChaine.push(String(stor.objDonnees.valY[i]))
    }
    const leDomaine = '[' + j3pVirgule(stor.objDonnees.valX[0]) + '\\quad ;\\quad ' + j3pVirgule(stor.objDonnees.valX[stor.objDonnees.valX.length - 1]) + ']'
    const tabNomF = ['f', 'g', 'h']
    stor.nomf = tabNomF[j3pGetRandomInt(0, 2)]
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, { f: stor.nomf, i: leDomaine })
    const objetGlobal = {}
    // Tout ce qui concerne la mise en forme :
    objetGlobal.mef = {}
    // la largeur
    stor.largMin = (stor.objDonnees.tabVariations.length >= 4) ? 800 : 600
    objetGlobal.mef.L = Math.min(85 * me.zonesElts.HG.getBoundingClientRect().width / 100, stor.largMin)
    // la hauteur de la ligne des x
    objetGlobal.mef.h = 50
    // la couleur
    objetGlobal.mef.macouleur = me.styles.toutpetit.enonce.color
    // le tableau principal, qui contiendra les infos de chaque ligne, il y aura donc autant de lignes (en plus de celle des x)
    objetGlobal.lignes = []
    // la première et unique ligne :
    objetGlobal.lignes[0] = {}
    // ligne de signes ou de variations ?
    objetGlobal.lignes[0].type = 'variations'
    // sa hauteur :
    objetGlobal.lignes[0].hauteur = 110
    objetGlobal.lignes[0].valeurs_charnieres = {}
    // j’ai  besoin des valeurs approchées pour les ordonner facilement... (souvent ce sont les mêmes car valeurs entières mais si ce sont des expressions mathquill...)
    objetGlobal.lignes[0].valeurs_charnieres.valeurs_approchees = []
    // Variations : (on peut mettre const)
    objetGlobal.lignes[0].variations = []
    // on peut ajouter des valeurs de x avec leurs images.
    objetGlobal.lignes[0].images = {}
    objetGlobal.lignes[0].imagesapprochees = []
    objetGlobal.lignes[0].expression = stor.nomf
    // j’ai mis 0 comme valeur charnière centrale, mais c’est arbitraire ici, ça n’impacte en rien la suite
    // A adapter
    objetGlobal.lignes[0].valeurs_charnieres.valeurs_theoriques = [...valXChaine]
    // A adapter
    objetGlobal.lignes[0].valeurs_charnieres.valeurs_affichees = [...valXChaine]
    // on peut laisser...
    objetGlobal.lignes[0].valeurs_charnieres.valeurs_approchees = [...stor.objDonnees.valX]
    // A adapter
    objetGlobal.lignes[0].variations = stor.objDonnees.tabVariations
    objetGlobal.lignes[0].cliquable = [false, false]
    // les intervalles décrivant les variations de la fonction
    objetGlobal.lignes[0].intervalles = []
    for (let i = 1; i < valXChaine.length; i++) {
      objetGlobal.lignes[0].intervalles.push([valXChaine[i - 1], valXChaine[i]])
    }
    // A adapter
    objetGlobal.lignes[0].images = []
    for (let i = 0; i < valXChaine.length; i++) {
      objetGlobal.lignes[0].images.push([valXChaine[i], valYChaine[i]])
    }

    objetGlobal.lignes[0].imagesapprochees = [...stor.objDonnees.valX]
    // on n’autorise pas l’ajout de lignes et d’intervalles
    objetGlobal.ajout_lignes_possible = false
    objetGlobal.ajout_intervalles_possible = false
    const correction = false
    me.logIfDebug('variations:', objetGlobal.lignes[0].variations, '\nstor.objDonnees.valX:', stor.objDonnees.valX)
    tableauSignesVariations(stor.zoneCons2, j3pGetNewId('divTableau'), objetGlobal, correction)

    j3pAffiche(stor.zoneCons3, '', textes.consigne2)
    const ineq1 = '$' + stor.nomf + '(' + stor.objDonnees.absComparaison[0] + ')< ' + stor.nomf + '(' + stor.objDonnees.absComparaison[1] + ')$'
    const ineq2 = '$' + stor.nomf + '(' + stor.objDonnees.absComparaison[0] + ')> ' + stor.nomf + '(' + stor.objDonnees.absComparaison[1] + ')$'
    const image1 = stor.nomf + '(' + stor.objDonnees.absComparaison[0] + ')'
    const image2 = stor.nomf + '(' + stor.objDonnees.absComparaison[1] + ')'
    const texteRadio3 = j3pChaine(textes.consigne3, { i1: image1, i2: image2 })
    stor.nameRadio = j3pGetNewId('choix')
    stor.idsRadio = [0, 1, 2].map(i => j3pGetNewId('radio' + i))
    stor.idsLabelRadio = [0, 1, 2].map(i => j3pGetNewId('labelRadio' + i))
    j3pBoutonRadio(stor.zoneCons4, stor.idsRadio[0], stor.nameRadio, 0, '')
    j3pAffiche('label' + stor.idsRadio[0], stor.idsLabelRadio[0], ineq1)
    j3pAffiche(stor.zoneCons4, '', '\n')
    j3pBoutonRadio(stor.zoneCons4, stor.idsRadio[1], stor.nameRadio, 1, '')
    j3pAffiche('label' + stor.idsRadio[1], stor.idsLabelRadio[1], ineq2)
    j3pAffiche(stor.zoneCons4, '', '\n')
    j3pBoutonRadio(stor.zoneCons4, stor.idsRadio[2], stor.nameRadio, 2, '')
    j3pAffiche('label' + stor.idsRadio[2], stor.idsLabelRadio[2], texteRadio3)
    j3pAffiche(stor.zoneCons4, '', '\n')
    stor.valXChaine = [...valXChaine]
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage('presentation1')

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // Que ce soit pour un cas concret ou un aure, je n’ai que 4 cas de figure
        stor.tabQuest = [1, 2, 3]
        stor.tabQuestInit = [...stor.tabQuest]
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      // création du conteneur dans lequel se trouveront toutes les conaignes
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = {
          aRepondu: false,
          bonneReponse: false
        }
        let numRadioSelect
        reponse.aRepondu = (j3pBoutonRadioChecked(stor.nameRadio)[0] > -1)
        if (reponse.aRepondu) {
          numRadioSelect = Number(j3pBoutonRadioChecked(stor.nameRadio)[0]) + 1
          reponse.bonneReponse = (numRadioSelect === stor.objDonnees.numRep)
        // console.log("numRep:", stor.objDonnees.numRep, "   numRadioSelect:", numRadioSelect)
        }

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            j3pAddContent(stor.zoneCorr, cBien, { replace: true })
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            for (let i = 1; i <= 3; i++) j3pDesactive(stor.idsRadio[i - 1])
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              j3pAddContent(stor.zoneCorr, tempsDepasse, { replace: true })
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              for (let i = 1; i <= 3; i++) j3pDesactive(stor.idsRadio[i - 1])
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              j3pAddContent(stor.zoneCorr, cFaux, { replace: true })
              if (me.essaiCourant < ds.nbchances) {
                j3pAddContent(stor.zoneCorr, '<br>' + essaieEncore)
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                j3pAddContent(stor.zoneCorr, '<br>' + regardeCorrection)
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                for (let i = 1; i <= 3; i++) j3pDesactive(stor.idsRadio[i - 1])
                j3pBarre(stor.idsLabelRadio[numRadioSelect - 1])
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
