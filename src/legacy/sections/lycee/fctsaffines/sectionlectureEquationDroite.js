import { j3pAddElt, j3pAjouteFoisDevantX, j3pChaine, j3pShuffle, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pPaletteMathquill, j3pPGCD, j3pGetNewId, j3pRandomTab, j3pShowError } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Deniaud Rémi
        mai 2021
        Lecture graphique d’une équation réduite de droite
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['paralleleAxe', false, 'boolean', 'Si ce paramètre vaut false, alors on donnera une droite parallèle à l’un des axes parmi tous les exemples'],
    ['coefFractionnaire', false, 'boolean', 'Si ce paramètre vaut false, alors tous les coefficients directeurs seront entiers. Sinon, ils seront tous fractionnaires.'],
    ['tauxAccroissement', false, 'boolean', 'Lorsque ce paramètre vaut true, alors on demande juste un taux d’accroissement de la fonction affine (la droite n’est dans ce cas pas parallèle à un des axes)']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    j3pDetruit(stor.zoneCons4)
    j3pDetruit(stor.zoneCons5)
    // explications contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    const numCorr = (ds.tauxAccroissement) ? '4' : (stor.tabQuest[me.questionCourante - 1] + 1)
    for (let i = 0; i <= ds.textes['corr' + numCorr].length; i++) {
      stor['zoneexplication' + i] = j3pAddElt(explications, 'div')
      j3pAffiche(stor['zoneexplication' + i], '', ds.textes['corr' + numCorr][i], stor.objDonnees)
    }
    modifFig({ correction: true })
  }
  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAPIAAACugAAAQEAAAAAAAAAAQAAAFD#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAAAAUAAUBrgAAAAAAAQGvD1wo9cKT#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAEOAAFJAMAYAAAAAAAAAAAAAAAAAAAAAAUAAEBBc#fO2RaHAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAAEAAAAQAAAAEAAAABAAAAA#####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8AAAAAABAAAAEAAAABAAAAAQAAAAT#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQAABQABAAAABwAAAAkA#####wAAAAABDgABSgDAKAAAAAAAAMAQAAAAAAAAAAAFAAIAAAAH#####wAAAAIAB0NSZXBlcmUA#####wDm5uYAAAABAAAAAQAAAAMAAAAJAQEAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEAAAEAAAAAAwAAAAz#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAABAAAADf####8AAAABAAdDQ2FsY3VsAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAAEQD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABRHcmFkdWF0aW9uQXhlc1JlcGVyZQAAABsAAAAIAAAAAwAAAAoAAAAPAAAAEP####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAABEABWFic29yAAAACv####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAABEABW9yZG9yAAAACgAAAAsAAAAAEQAGdW5pdGV4AAAACv####8AAAABAApDVW5pdGV5UmVwAAAAABEABnVuaXRleQAAAAr#####AAAAAQAQQ1BvaW50RGFuc1JlcGVyZQAAAAARAAAAAAAQAAABAAAFAAAAAAoAAAAOAAAAEgAAAA4AAAATAAAAFgAAAAARAAAAAAAQAAABAAAFAAAAAAoAAAANAAAAAA4AAAASAAAADgAAABQAAAAOAAAAEwAAABYAAAAAEQAAAAAAEAAAAQAABQAAAAAKAAAADgAAABIAAAANAAAAAA4AAAATAAAADgAAABUAAAAMAAAAABEAAAAWAAAADgAAAA8AAAAPAAAAABEAAAAAABAAAAEAAAUAAAAAFwAAABkAAAAMAAAAABEAAAAWAAAADgAAABAAAAAPAAAAABEAAAAAABAAAAEAAAUAAAAAGAAAABv#####AAAAAQAIQ1NlZ21lbnQAAAAAEQEAAAAAEAAAAQAAAAEAAAAXAAAAGgAAABcAAAAAEQEAAAAAEAAAAQAAAAEAAAAYAAAAHAAAAAQAAAAAEQEAAAAACwABVwDAFAAAAAAAAMA0AAAAAAAAAAAFAAE#3FZ4mrzfDgAAAB3#####AAAAAgAIQ01lc3VyZVgAAAAAEQAGeENvb3JkAAAACgAAAB8AAAARAAAAABEABWFic3cxAAZ4Q29vcmQAAAAOAAAAIP####8AAAACABJDTGlldU9iamV0UGFyUHRMaWUBAAAAEQBmZmYAAAAAAB8AAAAOAAAADwAAAB8AAAACAAAAHwAAAB8AAAARAAAAABEABWFic3cyAA0yKmFic29yLWFic3cxAAAADQEAAAANAgAAAAFAAAAAAAAAAAAAAA4AAAASAAAADgAAACEAAAAWAAAAABEBAAAAABAAAAEAAAUAAAAACgAAAA4AAAAjAAAADgAAABMAAAAZAQAAABEAZmZmAAAAAAAkAAAADgAAAA8AAAAfAAAABQAAAB8AAAAgAAAAIQAAACMAAAAkAAAABAAAAAARAQAAAAALAAFSAEAgAAAAAAAAwCAAAAAAAAAAAAUAAT#RG06BtOgfAAAAHv####8AAAACAAhDTWVzdXJlWQAAAAARAAZ5Q29vcmQAAAAKAAAAJgAAABEAAAAAEQAFb3JkcjEABnlDb29yZAAAAA4AAAAnAAAAGQEAAAARAGZmZgAAAAAAJgAAAA4AAAAQAAAAJgAAAAIAAAAmAAAAJgAAABEAAAAAEQAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABMAAAAOAAAAKAAAABYAAAAAEQEAAAAAEAAAAQAABQAAAAAKAAAADgAAABIAAAAOAAAAKgAAABkBAAAAEQBmZmYAAAAAACsAAAAOAAAAEAAAACYAAAAFAAAAJgAAACcAAAAoAAAAKgAAACv#####AAAAAgAMQ0NvbW1lbnRhaXJlAAAAABEBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAAAB8LAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MSkAAAAZAQAAABEAZmZmAAAAAAAtAAAADgAAAA8AAAAfAAAABAAAAB8AAAAgAAAAIQAAAC0AAAAbAAAAABEBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAAACQLAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MikAAAAZAQAAABEAZmZmAAAAAAAvAAAADgAAAA8AAAAfAAAABgAAAB8AAAAgAAAAIQAAACMAAAAkAAAALwAAABsAAAAAEQFmZmYAwCAAAAAAAAA#8AAAAAAAAAAAAAAAJgsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKG9yZHIxKQAAABkBAAAAEQBmZmYAAAAAADEAAAAOAAAAEAAAACYAAAAEAAAAJgAAACcAAAAoAAAAMQAAABsAAAAAEQFmZmYAwBwAAAAAAAAAAAAAAAAAAAAAAAAAKwsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKG9yZHIyKQAAABkBAAAAEQBmZmYAAAAAADMAAAAOAAAAEAAAACYAAAAGAAAAJgAAACcAAAAoAAAAKgAAACsAAAAzAAAAEQD#####AAVtTnVtZQABMwAAAAFACAAAAAAAAAAAABEA#####wAFbURlbm8AATEAAAABP#AAAAAAAAAAAAARAP####8AAXAAAi00#####wAAAAEADENNb2luc1VuYWlyZQAAAAFAEAAAAAAAAP####8AAAACAAxDRHJvaXRlUGFyRXEA#####wAAAP8AEAAAAQAAAAIAAAAKAAAADQj#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAABAAAADQAAAAANAgAAAA0DAAAADgAAADUAAAAOAAAANgAAAB4AAAAAAAAADgAAADcAAAANAQAAAB4AAAABAAAADQAAAAANAgAAAA0DAAAADgAAADUAAAAOAAAANgAAAB4AAAAAAAAADgAAADcAAAARAP####8AAWsAATIAAAABQAAAAAAAAAAAAAAdAP####8AAAD#ABAAAAEAAAACAAAACgAAAA0IAAAAHgAAAAAAAAAOAAAAOQAAAA0BAAAAHgAAAAAAAAAOAAAAOQAAABEA#####wACeEEAATIAAAABQAAAAAAAAAAAAAARAP####8AAnlBAAMtMjAAAAAcAAAAAUA0AAAAAAAAAAAAFgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAKAAAADgAAADsAAAAOAAAAPAAAABEA#####wACeEIAATMAAAABQAgAAAAAAAAAAAARAP####8AAnlCAAMtMjAAAAAcAAAAAUA0AAAAAAAAAAAAFgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAKAAAADgAAAD4AAAAOAAAAP#####8AAAABAAhDVmVjdGV1cgD#####AP8AAAAQAAABAAAAAgAAAD0AAABAAP####8AAAABAAdDTWlsaWV1AP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAD0AAABA#####wAAAAIABkNMYXRleAD#####AP8AAAEAAAAAAEIQAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAIXERlbHRhX3gAAAARAP####8AAnhDAAEzAAAAAUAIAAAAAAAAAAAAEQD#####AAJ5QwADLTIwAAAAHAAAAAFANAAAAAAAAAAAABYA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACgAAAA4AAABEAAAADgAAAEUAAAAfAP####8A#wAAABAAAAEAAAACAAAAQAAAAEYAAAAAIAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABAAAAARgAAACEA#####wD#AAAAQBAAAAAAAAAAAAAAAAAAAAAAAAAASBAAAAAAAAAAAAABAAAAAQAAAAAAAAAAAAhcRGVsdGFfeQAAABEA#####wACcDIAAy0yMAAAABwAAAABQDQAAAAAAAAAAAAWAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAoAAAABAAAAAAAAAAAAAAAOAAAASgAAABsA#####wD#AAAAwD0AAAAAAADAGAAAAAAAAAAAAAAASxAAAAAAAAIAAAABAAAAAQAAAAAAAAAAAA8jSXAjTiA9ICNWYWwocCn#####AAAAAQAFQ0ZvbmMA#####wABZgAPbU51bWUvbURlbm8qeCtwAAAADQAAAAANAgAAAA0DAAAADgAAADUAAAAOAAAANgAAAB4AAAAAAAAADgAAADcAAXgAAAAiAP####8AA3JlcAAFMyp4KzEAAAANAAAAAA0CAAAAAUAIAAAAAAAAAAAAHgAAAAAAAAABP#AAAAAAAAAAAXj#####AAAAAwAQQ1Rlc3RFcXVpdmFsZW5jZQD#####AAdlZ2FsaXRlAAAATQAAAE4BAAAAAAE#8AAAAAAAAAEAAAAO##########8='
    stor.mtgList = stor.mtgAppLecteur.createList(txtFigure)
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
  }
  function modifFig (st) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété correction (booléen)
    if (st.correction) {
      // on affiche la correction
      if (stor.tabQuest[me.questionCourante - 1] === 2) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xA', String(stor.objDonnees.ptA[0]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yA', String(stor.objDonnees.ptA[1]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xB', String(stor.objDonnees.ptB[0]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yB', String(stor.objDonnees.ptB[1]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'xC', String(stor.objDonnees.ptC[0]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'yC', String(stor.objDonnees.ptC[1]))
        if (!ds.tauxAccroissement) stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'p2', String(stor.objDonnees.p))
      }
    } else {
      if (stor.tabQuest[me.questionCourante - 1] === 0) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'k', String(stor.objDonnees.k))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'p', '-20')
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'mNume', '0')
        stor.mtgList.giveFormula2('k', String(stor.objDonnees.k))
      } else if (stor.tabQuest[me.questionCourante - 1] === 1) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'k', '-20')
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'p', String(stor.objDonnees.k))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'mNume', '0')
        stor.mtgList.giveFormula2('p', String(stor.objDonnees.k))
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'k', '-20')
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'p', String(stor.objDonnees.p))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'mNume', String(stor.objDonnees.nume))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'mDeno', String(stor.objDonnees.deno))
        stor.mtgList.giveFormula2('p', String(stor.objDonnees.p))
        stor.mtgList.giveFormula2('mNume', String(stor.objDonnees.nume))
        stor.mtgList.giveFormula2('mDeno', String(stor.objDonnees.deno))
      }
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }
  function genereDonnees (tabNum, frac) {
    /* tabNum est un tableau contenant les numéros des cas de figure :
    0 pour une droite verticale
    1 pour une droite horizontale
    2 pour une droite non parallèle à l’un des axes
    frac est un booléen : s’il vaut true, le coefficient directeur sera fractionnaire
     */
    const tabObj = []
    let listeK = [1, 2, 3, 4]
    let choixCoefdir = [1, 2, 3, 4]
    const choixCoefdirInit = [...choixCoefdir]
    let pioche
    let choixFrac = [
      [[1, 2], [3, 2], [5, 2]],
      [[1, 3], [2, 3], [4, 3], [5, 3]],
      [[1, 4], [3, 4], [5, 4], [7, 4]],
      [[1, 5], [2, 5], [3, 5], [4, 5], [6, 5]]
    ]
    const choixFracInit = choixFrac.concat([])
    for (let i = 0; i < tabNum.length; i++) {
      tabObj.push({})
      const signe = (j3pGetRandomInt(0, 1) * 2 - 1)
      if (tabNum[i] <= 1) {
        pioche = j3pGetRandomInt(0, listeK.length - 1)
        tabObj[i].k = signe * listeK[pioche]
        listeK.slice(pioche, 1)
        if (listeK.length === 0) listeK = [1, 2, 3, 4]
        tabObj[i].d = ((tabNum[i] === 0) ? 'x' : 'y') + '=' + tabObj[i].k
        tabObj[i].eqCart = ((tabNum[i] === 0) ? 'x' : 'y') + '-(' + tabObj[i].k + ')'
      } else {
        if (frac) {
          pioche = j3pGetRandomInt(0, choixFrac.length - 1)
          const choix = choixFrac[pioche][j3pGetRandomInt(0, choixFrac[pioche].length - 1)]
          tabObj[i].deno = choix[1]
          tabObj[i].nume = choix[0]
          choixFrac.slice(pioche, 1)
          if (choixFrac.length === 0) choixFrac = [...choixFracInit]
        } else {
          tabObj[i].deno = 1
          pioche = j3pGetRandomInt(0, choixCoefdir.length - 1)
          tabObj[i].nume = choixCoefdir[pioche]
          choixCoefdir.slice(pioche, 0)
          if (choixCoefdir.length === 0) choixCoefdir = [...choixCoefdirInit]
        }
        tabObj[i].nume *= signe
        let pointApresOK, pointAvantOK
        do {
          tabObj[i].p = j3pGetRandomInt(-4, 4)
          pointApresOK = Math.abs(tabObj[i].p + tabObj[i].nume) <= 6
          pointAvantOK = Math.abs(tabObj[i].p - tabObj[i].nume) <= 6
        } while (!pointApresOK && !pointAvantOK)
        // gestion des points utiles sur mtg32
        if (ds.tauxAccroissement) {
          const absPossibles = []
          for (let j = -5; j <= 5; j++) {
            const ord = tabObj[i].nume * j / tabObj[i].deno + tabObj[i].p
            if (Math.abs(ord) < 7 && (Math.abs(Math.round(ord) - ord) < Math.pow(10, -12))) absPossibles.push(j)
          }
          // Maintenant je choisis 2 abscisses parmi celles possibles
          const tabAbs = []
          for (let j = 1; j <= 2; j++) {
            const alea = j3pGetRandomInt(0, absPossibles.length - 1)
            tabAbs.push(absPossibles[alea])
            absPossibles.splice(alea, 1)
          }
          const tabAbs2 = (tabAbs[0] < tabAbs[1]) ? [...tabAbs] : [tabAbs[1], tabAbs[0]]
          tabObj[i].ptA = [tabAbs2[0], Math.round(tabAbs2[0] * tabObj[i].nume / tabObj[i].deno + tabObj[i].p)]
          tabObj[i].ptB = [tabAbs2[1], Math.round(tabAbs2[0] * tabObj[i].nume / tabObj[i].deno + tabObj[i].p)]
          tabObj[i].ptC = [tabAbs2[1], Math.round(tabAbs2[1] * tabObj[i].nume / tabObj[i].deno + tabObj[i].p)]
        } else {
          if (pointApresOK) {
            tabObj[i].ptA = [0, tabObj[i].p]
            tabObj[i].ptB = [tabObj[i].deno, tabObj[i].p]
            tabObj[i].ptC = [tabObj[i].deno, tabObj[i].p + tabObj[i].nume]
          } else if (pointAvantOK) {
            tabObj[i].ptA = [-tabObj[i].deno, tabObj[i].p - tabObj[i].nume]
            tabObj[i].ptB = [0, tabObj[i].p - tabObj[i].nume]
            tabObj[i].ptC = [0, tabObj[i].p]
          }
        }
        tabObj[i].m = (frac) ? '\\frac{' + tabObj[i].nume + '}{' + tabObj[i].deno + '}' : String(tabObj[i].nume)
        // et le calcul :
        if (ds.tauxAccroissement) {
          const leNume = tabObj[i].ptC[1] - tabObj[i].ptA[1]
          const leDeno = tabObj[i].ptC[0] - tabObj[i].ptA[0]
          tabObj[i].c = '\\frac{' + leNume + '}{' + leDeno + '}'
          if (Math.abs(leDeno - 1) < Math.pow(10, -12)) {
            tabObj[i].c += '=' + String(leNume)
          } else if (j3pPGCD(Math.abs(leNume), Math.abs(leDeno)) !== 1) {
            tabObj[i].c += '=' + ((frac) ? '\\frac{' + tabObj[i].nume + '}{' + tabObj[i].deno + '}' : String(tabObj[i].nume))
          }
        } else {
          tabObj[i].c = (frac) ? tabObj[i].m : '\\frac{' + tabObj[i].nume + '}{1}=' + String(tabObj[i].nume)
        }
        tabObj[i].d = 'y=' + j3pGetLatexMonome(1, 1, tabObj[i].m, 'x') + j3pMonome(2, 0, tabObj[i].p)
        tabObj[i].eqCart = 'y-(' + (tabObj[i].nume / tabObj[i].deno) + '*x+(' + tabObj[i].p + '))'
      }
    }
    return tabObj
  }
  function fractionSimpli (texte) {
    // renvoie un boooléen disant si texte contient une fraction non simplifiée
    // Ce texte est obtenu après un appel à j3pMathquill_Xacs qui présente les fractions sous la (2)/(3)
    const frac = /\(-?([0-9])+\)\/\(-?([0-9]+)\)/g
    // Je récupère toutes les fractions présentes dans le texte
    if (frac.test(texte)) {
      const tabFrac = texte.match(frac)
      for (let i = 0; i < tabFrac.length; i++) {
        const mafraction = tabFrac[i].replace(frac, '$1/$2').split('/')
        if (Number(mafraction[1]) !== 0) {
          // Je calcule le pgcd du numérateur et du dénominateur
          const monPgcd = j3pPGCD(Number(mafraction[0]), Number(mafraction[1]))
          if (Math.abs(Number(mafraction[1])) === 1 || monPgcd !== 1) return false
        }
      }
    }
    return true
  }
  function testEquivalence (rep, sol) {
    // rep et sol sont deux fonctions de x et y (obtenues ici à partir d’équations de droites). Cette fonction vérifie si elles sont équivalentes
    const arbreRep = new Tarbre(rep, ['x', 'y'])
    const arbreSol = new Tarbre(sol, ['x', 'y'])
    for (let i = 0; i <= 6; i++) {
      for (let j = 0; j <= 6; j++) {
        if (Math.abs(arbreRep.evalue([i, j]) - arbreSol.evalue([i, j])) > Math.pow(10, -10)) return false
      }
    }
    return true
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 5,
      nbetapes: 1,
      tauxAccroissement: false,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.
      paralleleAxe: false,
      coefFractionnaire: false,
      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titreExo: 'Équation réduite de droite par lecture graphique',
        titreExo2: 'Taux d’accroissement d’une fonction affine',
        // on donne les phrases de la consigne
        consigne1: 'Donner l’équation réduite de la droite tracée dans le repère ci-contre.',
        consigne2: 'On a tracé ci-contre la courbe représentative d’une fonction affine $£f$.<br/>Donner le taux d’accroissement de $£f$ entre £a et £b.',
        indic2: 'Il reste £t tentatives.',
        indic1: 'Il reste une tentative.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'La réponse est bien une équation de la droite mais elle n’est pas écrite sous forme réduite&nbsp;!',
        comment2: 'Les fractions doivent être écrites sous forme réduite&nbsp;!',
        comment3: 'Réponse fausse&nbsp;: $£r$',
        comment4: 'Réponse non réduite&nbsp;: $£r$',
        comment5: 'On attend une équation de droite, donc une égalité&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: [
          'Cette droite est parallèle à l’axe des ordonnées. Son équation réduite est donc de la forme $x=k$.',
          'Or tous les points ont pour abscisse $£k$.',
          'Donc l’équation réduite de la droite est $x=£k$.'
        ],
        corr2: [
          'Cette droite est parallèle à l’axe des abscisses. Son équation réduite est donc de la forme $y=k$.',
          'Or tous les points ont pour ordonnée $£k$.',
          'Donc l’équation réduite de la droite est $y=£k$.'
        ],
        corr3: [
          'Cette droite n’est parallèle à aucun des deux axes du repère. Elle admet donc une équation réduite de la forme $y=mx+p$.',
          '$p$ est l’ordonnée à l’origine de la droite et vaut $£p$ comme indiqué sur le graphique.',
          '$m$ est le coefficient directeur égal à $\\frac{\\Delta_{y}}{\\Delta_{x}}=£c$.',
          'L’équation réduite de la droite est donc $£d$.'
        ],
        corr4: [
          'Le taux d’accroissement de la fonction $£f$ entre £a et £b est le coefficient directeur de la droite.',
          'Il est égal à $\\frac{\\Delta_{y}}{\\Delta_{x}}=£c$.',
          'Remarque : pour une fonction affine, ce taux d’accroissement est constant. Il ne dépend pas des abscisses choisies pour le déterminer.'
        ]
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.
      // limite : 10;
      /*
      Phrase d’état renvoyée par la section
      Si pe:0 alors la section est quantitative et renvoie un score entre 0 et 1.
      Dans le cas où la section est qualitative, lister les différents pe renvoyées :
          pe_1 : "toto"
          pe_2 : "tata"
          etc
      Dans la partie navigation, il faudra affecter la variable me.parcours.pe à l’une des précédentes.
      Par exemple,
      me.parcours.pe = ds.pe_2
      */
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    // Construction de la page
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.5 })
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    const titreExo = (ds.tauxAccroissement) ? ds.textes.titreExo2 : ds.textes.titreExo
    me.afficheTitre(titreExo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    if (ds.tauxAccroissement) ds.paralleleAxe = false
    // gestion des cas de figure
    const tabQuest = []
    if (ds.paralleleAxe) tabQuest.push(j3pGetRandomInt(0, 1))
    for (let i = tabQuest.length; i < ds.nbrepetitions; i++) tabQuest.push(2)
    stor.tabQuest = j3pShuffle(tabQuest)
    stor.tabObjDonnees = genereDonnees(stor.tabQuest, ds.coefFractionnaire)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    stor.numEssai = 1
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div')

    const largFig = 440
    const hautFig = 440
    stor.zoneMtg = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneMtg.style.textAlign = 'center'
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(stor.zoneMtg, { id: stor.mtg32svg, width: largFig, height: hautFig })
    depart()

    stor.objDonnees = {}
    for (const prop in stor.tabObjDonnees[me.questionCourante - 1]) {
      stor.objDonnees[prop] = stor.tabObjDonnees[me.questionCourante - 1][prop]
    }
    me.logIfDebug('stor.objDonnees:', stor.objDonnees)
    modifFig({ correction: false })
    // Dans le conteneur gauche, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    if (ds.tauxAccroissement) {
      stor.nomf = j3pRandomTab(['f', 'g', 'h'], [0.334, 0.333, 0.333])
      stor.objDonnees.f = stor.nomf
      stor.objDonnees.a = stor.objDonnees.ptA[0]
      stor.objDonnees.b = stor.objDonnees.ptC[0]
    }
    if (ds.tauxAccroissement) j3pAffiche(stor.zoneCons1, '', ds.textes.consigne2, stor.objDonnees)
    else j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1)
    const elt = j3pAffiche(stor.zoneCons3, '', '&1&')
    stor.zoneInput = elt.inputmqList[0]
    const toucheRestrict = (ds.tauxAccroissement) ? '\\d,./-' : 'xy\\d,.+/=-'
    mqRestriction(stor.zoneInput, toucheRestrict, { commandes: ['fraction'] })
    if (ds.tauxAccroissement) {
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      stor.zoneInput.reponse = [stor.objDonnees.nume / stor.objDonnees.deno]
    }
    // palette MQ :
    stor.zoneCons4.style.paddingTop = '5px'
    stor.zoneCons4.style.paddingBottom = '40px'
    stor.laPalette = j3pAddElt(stor.zoneCons4, 'div')
    j3pPaletteMathquill(stor.zoneCons4, stor.zoneInput, { liste: ['fraction'] })
    const consIndic = (ds.nbchances === 1) ? ds.textes.indic1 : j3pChaine(ds.textes.indic2, { t: ds.nbchances })
    stor.zoneCons5.style.fontSize = '0.9em'
    stor.zoneCons5.style.fontStyle = 'italic'
    stor.zoneCons5.paddingTop = '8px'
    j3pAffiche(stor.zoneCons5, '', consIndic)
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: (ds.tauxAccroissement) ? [] : mesZonesSaisie
    })
    stor.chancePourReduire = true
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        me.logIfDebug('réponse', reponse)
        let fractionsSimplifiees = true
        let isEquation = true
        let equationsEquiv = false
        if (reponse.aRepondu && !ds.tauxAccroissement) {
          const repEleve = j3pMathquillXcas(stor.fctsValid.zones.reponseSaisie[0])
          fractionsSimplifiees = fractionSimpli(repEleve)
          reponse.aRepondu = fractionsSimplifiees
          if (reponse.aRepondu) {
          // Je vérifie qu’il s’agit bien d’une équation (présence d’un seul signe =)
            const decoupageRep = repEleve.split('=')
            isEquation = (decoupageRep.length === 2)
            reponse.aRepondu = isEquation
            if (isEquation) {
              if (stor.tabQuest[me.questionCourante - 1] === 2) {
                const posy = decoupageRep.indexOf('y')
                if (posy === -1) {
                // l’équation n’est dans tout les cas pas une équation réduite
                  reponse.bonneReponse = false
                } else {
                // si j’ai une droite représentant une fonction affine, je mets la réponse de l’élève dans rep(x) pour mtg32
                // ensuite, je vérifie la valeur de egalite qui vaut 1 s’il y a bien égalité
                  stor.mtgList.giveFormula2('rep', j3pAjouteFoisDevantX(decoupageRep[1 - posy], 'x'))
                  stor.mtgList.calculateNG()
                  me.logIfDebug('rep:', stor.mtgList.getFormula('rep'), stor.mtgList.getFormula('f'), stor.mtgList.getFormula('mNume'), stor.mtgList.getFormula('mDeno'), stor.mtgList.getFormula('p'), j3pAjouteFoisDevantX(decoupageRep[1 - posy], 'x'))
                  reponse.bonneReponse = stor.mtgList.valueOf('egalite') === 1
                }
              } else {
                const xy = ['x', 'y'][stor.tabQuest[me.questionCourante - 1]]
                reponse.bonneReponse = ((decoupageRep[0] === xy && Number(decoupageRep[1]) === stor.objDonnees.k) || (decoupageRep[1] === xy && Number(decoupageRep[0]) === stor.objDonnees.k))
              }
              if (!reponse.bonneReponse) equationsEquiv = testEquivalence(decoupageRep[0] + '-(' + decoupageRep[1] + ')', stor.objDonnees.eqCart)
              if (!equationsEquiv) equationsEquiv = testEquivalence(decoupageRep[1] + '-(' + decoupageRep[0] + ')', stor.objDonnees.eqCart)
              if (equationsEquiv && stor.chancePourReduire) {
                stor.numEssai--
                stor.chancePourReduire = false
                me.essaiCourant--
              }
              // Cette deuxième tentative est utile si on écrit mx+p=y et non y=mx+p
              // on appelle alors la fonction qui va mettre en couleur la réponse bonne ou fausse
              stor.fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
              stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
            }
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (!isEquation) {
            msgReponseManquante = ds.textes.comment5
          } else if (!fractionsSimplifiees) {
            msgReponseManquante = ds.textes.comment2
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.sectionCourante('navigation')
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              stor.numEssai++
              if (equationsEquiv) stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCons5.innerHTML = (ds.nbchances + 1 - stor.numEssai === 1) ? ds.textes.indic1 : j3pChaine(ds.textes.indic2, { t: ds.nbchances + 1 - stor.numEssai })
                const newDiv = j3pAddElt(stor.zoneCons2, 'div', '', { style: { color: me.styles.cfaux } })
                const laConsigne = (equationsEquiv) ? ds.textes.comment4 : ds.textes.comment3
                j3pAffiche(newDiv, '', laConsigne, { r: stor.fctsValid.zones.reponseSaisie[0] })
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.sectionCourante('navigation')
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
