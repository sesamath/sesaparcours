import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pRandomTab, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mai 2019
        Une équation de droite étant donnée, on complète un tableau de valeurs : une ordonnée et une abscisse
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['fctAffine', false, 'boolean', 'Lorsque ce paramètre vaut true, alors on parle d’une fonction affine, sinon, on présente sous la forme d’une équation de droite.'],
    ['coefFrac', false, 'boolean', 'On peut forcer le coefficient directeur de la droite à être sous forme fractionnaire, cela rend les calculs plus compliqués.'],
    ['seuilReussite', 0.7, 'reel', 'seuil au-dessus duquel on considère que l’élève a réussi la notion'],
    ['seuilErreur', 0.6, 'reel', 'seuil au-dessus duquel on considère que la notion n’est pas acquise']
  ],
  // dans le cas d’une section QUANTITATIVE
  // "pe":[
  //    {"pe":0}
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'Problème dans la résolution d’équations de degré 1 (cas où coefFrac vaut false)' },
    { pe_3: 'Problème dans la résolution d’équations de degré 1 avec fractions (cas où coefFrac vaut true)' },
    { pe_4: 'Insuffisant' }
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pDetruit(stor.laPalette)
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, stor.obj)
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2, stor.obj)
    j3pAffiche(stor.zoneExpli3, '', ds.textes.corr3, stor.obj)
    if (!bonneReponse) {
      j3pAffiche(stor.zoneExpli4, '', ds.textes.corr4, stor.obj)
      stor.DonneesTab.parent = stor.zoneExpli5
      stor.DonneesTab.corr = true
      creationtableauValeurs(stor.DonneesTab)
    }
  }
  function ecoute (eltInput) {
    if (eltInput.className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, eltInput, { liste: ['fraction'] })
    }
  }
  function avecParenthese (nb) {
    // écrit bien le nombre dans un calcul (cad avec parenthèses si ce nombre est négatif)
    if (j3pCalculValeur(nb) < 0) {
      return '(' + nb + ')'
    } else {
      return nb
    }
  }
  function creationtableauValeurs (mesDonnees) {
    // mesDonnees est un objet
    // mesDonnees.parent est le nom du div dans lequel est créé le tableau
    // mesDonnees.legendes est un tableau de 2 éléments : les légendes (x et y ou f(x))
    // mesDonnees.tabVal contient les abscisses et ordonnées des deux points
    // mesDonnees.corr est un booléen. S’il vaut false, alors y n’apparaitra pas dans la première colonne et x n’apparaitre pas dans la deuxième
    const divTab = j3pAddElt(mesDonnees.parent, 'div')
    const largeurTab = 0.8 * me.zonesElts.MG.getBoundingClientRect().width
    const largeurTexte = 0
    const largeurColonne = (largeurTab - largeurTexte - 5) / 3
    const table = j3pAddElt(divTab, 'table', { width: '80%', borderColor: 'black', border: 2, cellSpacing: 0 })
    const ligne1 = j3pAddElt(table, 'tr')
    const couleurEnonce = $(stor.conteneur).css('color')
    const divLigne1 = []
    for (let i = 1; i <= 3; i++) {
      divLigne1.push(j3pAddElt(ligne1, 'td', { width: largeurColonne + 'px', align: 'center' }))
      if (i !== 3) {
        j3pStyle(divLigne1[i - 1], { color: couleurEnonce })
      }
    }
    const ligne2 = j3pAddElt(table, 'tr')
    const divLigne2 = []
    for (let i = 1; i <= 3; i++) {
      divLigne2.push(j3pAddElt(ligne2, 'td', { width: largeurColonne + 'px', align: 'center' }))
      if (i !== 2) {
        j3pStyle(divLigne2[i - 1], { color: couleurEnonce })
      }
    }
    // Maintenant, je remplis chaque case de la table
    j3pAffiche(divLigne1[0], '', '$' + mesDonnees.legendes[0] + '$')
    j3pAffiche(divLigne1[1], '', '$' + mesDonnees.tabVal[0][0] + '$')
    j3pAffiche(divLigne2[0], '', '$' + mesDonnees.legendes[1] + '$')
    j3pAffiche(divLigne2[2], '', '$' + mesDonnees.tabVal[1][1] + '$')
    stor.zoneInput = []
    let tabZones
    if (mesDonnees.corr) {
      j3pAffiche(divLigne1[2], '', '$' + mesDonnees.tabVal[1][0] + '$')
      j3pAffiche(divLigne2[1], '', '$' + mesDonnees.tabVal[0][1] + '$')
    } else {
      const elt1 = j3pAffiche(divLigne1[2], '', '&1&', { inputmq1: { texte: '' } })
      stor.zoneInput.push(elt1.inputmqList[0])
      const elt2 = j3pAffiche(divLigne2[1], '', '&1&', { inputmq1: { texte: '' } })
      stor.zoneInput.push(elt2.inputmqList[0])
      mqRestriction(stor.zoneInput[0], '\\d.,/-')
      mqRestriction(stor.zoneInput[1], '\\d.,/-')
      stor.zoneInput[0].typeReponse = ['nombre', 'exact']
      stor.zoneInput[1].typeReponse = ['nombre', 'exact']
      stor.zoneInput[1].reponse = [j3pCalculValeur(mesDonnees.tabVal[0][1])]
      stor.zoneInput[0].reponse = [j3pCalculValeur(mesDonnees.tabVal[1][0])]
      stor.zoneInput[0].solutionSimplifiee = ['non valide', 'valide et on accepte']
      stor.zoneInput[1].solutionSimplifiee = ['non valide', 'valide et on accepte']
      tabZones = stor.zoneInput.map(eltInput => eltInput.id)
      stor.laPalette = j3pAddElt(mesDonnees.parent, 'div', '', { style: { paddingTop: '10px' } })
      // palette MQ :
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], { liste: ['fraction'] })
      for (let i = 0; i < tabZones.length; i++) {
        $(stor.zoneInput[i]).focusin(function () {
          ecoute(this)
        })
      }
      me.logIfDebug('rep1:', stor.zoneInput[0].reponse[0], '   rep2:', stor.zoneInput[1].reponse[0])
      j3pFocus(stor.zoneInput[1])
    }
    // on renvoie un objet contenant le nom des zones de saisie (lorsqu’elles existent) et le tableau des effectifs cumulés croissants
    return { zonesSaisie: tabZones }
  }
  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 4,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      fctAffine: false,
      coefFrac: false,
      seuilReussite: 0.7,
      seuilErreur: 0.6,

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo1: 'Equation de droite et tableau de valeurs',
        titre_exo2: 'Fonction affine et tableau de valeurs',
        // on donne les phrases de la consigne
        consigne1_1: 'Compléter le tableau de valeurs suivant sachant que $£e$.',
        consigne1_2: 'Compléter le tableau de valeurs suivant de la fonction affine $£f$ définie par $£f(x)=£e$.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'On attend une réponse sous forme simplifiée (un nombre), pas un calcul&nbsp;!',
        comment2: 'Les fractions auraient pu être simplifiées&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'Pour la première zone, on a $x=£x$ et on calcule&nbsp;:<br/>$£c$.',
        corr2: 'Pour la deuxième zone, on a $£y$ et on résout alors l’équation $£{eq}$.',
        corr3: 'Ceci nous donne $£{eq2}$ puis $x=£r$.',
        corr4: 'On obtient alors le tableau&nbsp;:'
      },
      pe_1: 'Bien',
      pe_2: 'Problème dans la résolution d’équations de degré 1',
      pe_3: 'Probalème dans la résolution d’équation de degré 1 avec fractions',
      pe_4: 'Insuffisant',
      pe: 0
    }
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // On détermine dans un premier temps les données permettant d’avoir une équation de droite
    const obj = {}
    if (ds.fctAffine) {
      obj.f = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
    }
    let x1, y1, x2, y2, coefa, coefb
    if (ds.coefFrac) {
      // Coef fractionnaire
      const choixFracTab = ['\\frac{1}{3}', '\\frac{2}{3}', '-\\frac{1}{3}', '-\\frac{2}{3}', '\\frac{1}{4}', '-\\frac{1}{4}', '\\frac{3}{4}', '-\\frac{3}{4}', '\\frac{1}{6}', '\\frac{5}{6}', '\\frac{4}{3}', '\\frac{5}{3}', '-\\frac{4}{3}', '-\\frac{5}{3}']
      const probaTab = []
      for (let i = 0; i < choixFracTab.length - 1; i++) {
        probaTab.push(Math.round(1000 / choixFracTab.length) / 1000)
      }
      probaTab.push(Math.round(1000 * (1 - (choixFracTab.length - 1) * Math.round(1000 / choixFracTab.length) / 1000)) / 1000)

      coefa = j3pRandomTab(choixFracTab, probaTab)
      x1 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 6)
      do {
        y1 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 6)
      } while (Math.abs(Math.abs(x1) - Math.abs(y1)) < Math.pow(10, -12))
      coefb = j3pGetLatexSomme(y1, j3pGetLatexProduit('-1', j3pGetLatexProduit(x1, coefa)))
      do {
        x2 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 6)
      } while (Math.abs(Math.abs(x1) - Math.abs(x2)) < Math.pow(10, -12))
      y2 = j3pGetLatexSomme(j3pGetLatexProduit(x2, coefa), coefb)
    } else {
      do {
        coefa = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
      } while (Math.abs(coefa - 1) < Math.pow(10, -12))
      do {
        coefb = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
      } while (Math.abs(coefa - coefb) < Math.pow(10, -12))
      x1 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 6)
      do {
        x2 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 6)
      } while (Math.abs(Math.abs(x1) - Math.abs(x2)) < Math.pow(10, -12))
      y1 = Math.round(x1 * coefa + coefb)
      y2 = Math.round(x2 * coefa + coefb)
    }
    obj.TabVal = [[x2, y2], [x1, y1]]
    obj.x = obj.TabVal[0][0]
    const xCalc = (obj.x < 0) ? '(' + obj.x + ')' : obj.x
    if (coefa === -1) {
      obj.c = '=-' + xCalc + j3pGetLatexMonome(2, 0, coefb, 'x') + '=' + obj.TabVal[0][1]
    } else {
      obj.c = '=' + coefa + '\\times ' + xCalc + j3pGetLatexMonome(2, 0, coefb, 'x') + '=' + obj.TabVal[0][1]
    }
    if (ds.fctAffine) {
      obj.y = obj.f + '(x)=' + obj.TabVal[1][1]
      obj.e = j3pGetLatexMonome(1, 1, coefa, 'x') + j3pGetLatexMonome(2, 0, coefb, 'x')
      obj.c = obj.f + '(' + obj.x + ')' + obj.c
      obj.eq = obj.TabVal[1][1] + '=' + obj.e
    } else {
      obj.y = 'y=' + obj.TabVal[1][1]
      obj.e = 'y=' + j3pGetLatexMonome(1, 1, coefa, 'x') + j3pGetLatexMonome(2, 0, coefb, 'x')
      obj.c = 'y' + obj.c
      obj.eq = obj.TabVal[1][1] + '=' + j3pGetLatexMonome(1, 1, coefa, 'x') + j3pGetLatexMonome(2, 0, coefb, 'x')
    }
    const membreGauche = j3pGetLatexSomme(obj.TabVal[1][1], j3pGetLatexProduit('-1', coefb))
    obj.eq2 = membreGauche + '=' + j3pGetLatexMonome(1, 1, coefa, 'x')
    if (ds.coefFrac) {
      obj.r = '\\frac{' + membreGauche + '}{' + coefa + '}=' + membreGauche + '\\times ' + avecParenthese(j3pGetLatexQuotient(1, coefa)) + '=' + obj.TabVal[1][0]
    } else {
      obj.r = '\\frac{' + membreGauche + '}{' + coefa + '}=' + obj.TabVal[1][0]
    }
    // console.log("tabVal:",obj.TabVal,"   coefa:",coefa,"   coefb:",coefb, "    eq2:",obj.eq2)
    const laCons1 = (ds.fctAffine) ? ds.textes.consigne1_2 : ds.textes.consigne1_1
    j3pAffiche(stor.zoneCons1, '', laCons1, obj)
    // Et maintenant construction du tableau de valeurs
    const DonneesTab = {}
    DonneesTab.parent = stor.zoneCons2
    DonneesTab.legendes = ['x']
    if (ds.fctAffine)DonneesTab.legendes.push(obj.f + '(x)')
    else DonneesTab.legendes.push('y')
    DonneesTab.tabVal = obj.TabVal
    DonneesTab.corr = false
    const eltTableau = creationtableauValeurs(DonneesTab)
    stor.obj = obj
    stor.DonneesTab = DonneesTab
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = eltTableau.zonesSaisie
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70

        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc / 100 })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0
        const leTitre = (ds.fctAffine) ? ds.textes.titre_exo2 : ds.textes.titre_exo1
        me.afficheTitre(leTitre)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // code de création des fenêtres
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // On peut ajouter un commentaire particulier.
          let msgReponseManquante
          // Ici dans la 4ème zone, si on donne une fraction non réduite, on donnera à reponse.aRepondu la valeur false
          // On sait que la fraction est non réduite car dans le tableau fctsValid.zones.reponseSimplifiee[3] (correspondant au caractère simplifié ou non de la 4ème zone),
          // l’élément [1] vaut false
          if (!fctsValid.zones.reponseSimplifiee[0][0] || !fctsValid.zones.reponseSimplifiee[1][0]) {
          // c’est que la fraction de la quatrième zone de saisie est réductible
            msgReponseManquante = ds.textes.comment1
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            if (!fctsValid.zones.reponseSimplifiee[0][1] || !fctsValid.zones.reponseSimplifiee[1][1]) {
            // il existe une fraction non réductible
              stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment2
            }
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
              // JE regarde si ce n’est aps la résolution d’équation qui pose problème (en gros, il ne sait pas répondre à la question 2)
                if (fctsValid.zones.bonneReponse[0] && !fctsValid.zones.bonneReponse[1]) {
                // Il sait calculer l’image mais pas résoudre l’équation
                  if (ds.coefFrac) {
                    me.typederreurs[5]++
                  } else {
                    me.typederreurs[4]++
                  }
                } else if (!fctsValid.zones.bonneReponse[0] && !fctsValid.zones.bonneReponse[1]) {
                // Il ne sait faire aucune des deux questions
                  me.typederreurs[3]++
                }
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        // me.parcours.pe = me.score / ds.nbitems;
        if (me.score / ds.nbitems >= ds.seuilReussite) {
          me.parcours.pe = ds.pe_1
        } else {
          if (me.typederreurs[3] / ds.nbrepetitions > ds.seuilErreur) { // problème sur le calcul d’image et d’antécédent
            me.parcours.pe = ds.pe_4
          } else {
            if (ds.coefFrac) {
              if ((me.typederreurs[3] + me.typederreurs[5]) / ds.nbrepetitions > ds.seuilErreur) { // problème sur le d’antécédent mais pas d’image
                me.parcours.pe = ds.pe_3
              }
            } else {
              if ((me.typederreurs[3] + me.typederreurs[4]) / ds.nbrepetitions > ds.seuilErreur) { // problème sur le d’antécédent mais pas d’image
                me.parcours.pe = ds.pe_2
              }
            }
          }
        }
        if (me.parcours.pe === '') { me.parcours.pe = ds.pe_4 }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
