import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pEmpty, j3pShuffle, j3pPGCD, j3pDetruit, j3pElement, j3pFocus, j3pGetRandomInt, j3pMonome, j3pPaletteMathquill, j3pRandomTab, j3pStyle, j3pGetNewId } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    octobre 2014
    Dans cette section, on cherche à savoir si une fonction donnée est affine ou pas
*/

const defaultTabEx = [1, 2, 3, 4, 5, 6]
const defaultNbQuestions = 6

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['tab_ex', defaultTabEx, 'array', 'Ce tableau contient la liste des exemples choisis\n1 pour ax/b+c\n2 pour racine(a)x\n3 pour (ax+b)/c\n4 pour (a+bx)/x\n5 pour racine(ax)+b\n6 pour (ax+b)^2-(c-ax)^2.'],
    ['nb_questions', defaultNbQuestions, 'entier', 'Nombre de questions (choisies aléatoirement dans le tableau tab_ex).']
  ]
}

const textes = {
  titre_exo: 'Fonction affine ou non affine ?',
  consigne1: 'La fonction $£f$ définie par $£f(x)=£g$ est-elle affine ?',
  consigne2: "$£f(x)$ s'écrit donc sous la forme $ax+b$ où $a$ et $b$ sont des réels constants.",
  consigne3: 'Dans ce cas, $a$ vaut &1& et $b$ vaut &2&.',
  cocher1: 'oui',
  cocher2: 'non',
  comment1: 'Il faut sélectionner l’une des deux propositions !',
  comment2: 'Complète les deux zones !',
  comment3: 'Elle est bien affine mais les coefficients donnés ne sont pas les bons !',
  corr1: "La fonction s'écrit sous la forme $£f(x)=£g$, elle est donc bien affine c’est-à-dire de la forme $ax+b$ où $a=£a$ et $b=£b$.",
  corr2: 'En développant $£f(x)$, on obtient $£e=£d$.<br/>$£f(x)$ est donc bien sous la forme $ax+b$ où $a=£a$ et $b=£b$, $£f$ est donc affine.',
  corr3: "Cette fonction ne s'écrit pas sous la forme $ax+b$ avec $a$ et $b$ réels constants.",
  corr4: 'Cette fonction est même linéaire.',
  corr5: 'En effet $£f(x)=£g$.',
  corr6: 'De plus cette fonction n’est pas définie sur $\\R$.<br>Donc elle n’est pas affine.',
  corr7: '$£f(x)$ est égal à $£g$ et non à $£h$ qui aurait été l’expression d’une fonction affine.'
}

/**
 * section affineounon
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function generationFonction (num) {
    let fdex// expression de la fonction au format latex
    // c’est cette valeur qui sera renvoyée par la fonction'
    let affine = true// valeur renvoyée pour savoir si la fonction crée est affine ou pas
    let a, b, c, expressA, expressB
    let monCoefa, monCoefb, formeAxb
    if (num === 1) {
      // fonction(a/b)x+c
      do {
        a = j3pGetRandomInt(-5, 5)
        b = j3pGetRandomInt(2, 6)
      } while ((Math.abs(a) < 1e-12 || (j3pPGCD(Math.abs(a), b, { returnOtherIfZero: true }) !== 1)))
      do {
        c = j3pGetRandomInt(-5, 5)
      } while (Math.abs(c) < Math.pow(10, -12))
      fdex = '\\frac{' + j3pMonome(1, 1, a) + '}{' + b + '}' + j3pMonome(2, 0, c)
      monCoefa = a / b
      monCoefb = c
      expressA = '\\frac{' + a + '}{' + b + '}'
      expressB = c
      formeAxb = '\\frac{' + a + '}{' + b + '}x' + j3pMonome(2, 0, c)
    } else if ((num === 2) || (num === 5)) {
      let estSimplifiable
      do {
        a = j3pGetRandomInt(2, 15)
        // on vérifie si la racone carrée de a est simplifiable (ce qu’on ne veut pas)'
        let k = 1
        estSimplifiable = false
        while ((Math.pow(k, 2) < a) && !estSimplifiable) {
          if (Math.abs(Math.sqrt(a / Math.pow(k, 2)) - Math.round(Math.sqrt(a / Math.pow(k, 2))) < Math.pow(10, -12))) {
            estSimplifiable = true
          }
          k++
        }
      } while (estSimplifiable)
      if (num === 2) {
        // fonction sqrt{a}x
        fdex = '\\sqrt{' + a + '}x'
        monCoefa = Math.sqrt(a)
        monCoefb = 0
        expressA = '\\sqrt{' + a + '}'
        expressB = 0
        formeAxb = fdex
      } else if (num === 5) {
        // fonction sqrt{ax}+b
        do {
          b = j3pGetRandomInt(-5, 5)
        } while (Math.abs(b) < Math.pow(10, -12))
        fdex = '\\sqrt{' + j3pMonome(1, 1, a) + '}' + j3pMonome(2, 0, b)
        formeAxb = '\\sqrt{' + a + '}x' + j3pMonome(2, 0, b)
        affine = false
      }
    } else if (num === 3) {
      // fonction(ax+b)/c
      let pgcdAC, pgcdBC
      do {
        do {
          a = j3pGetRandomInt(-5, 5)
        } while (Math.abs(a) < Math.pow(10, -12))
        do {
          b = j3pGetRandomInt(-5, 5)
        } while (Math.abs(b) < Math.pow(10, -12))
        c = j3pGetRandomInt(2, 5)
        pgcdAC = j3pPGCD(Math.abs(a), Math.abs(c), { returnOtherIfZero: true })
        pgcdBC = j3pPGCD(Math.abs(b), Math.abs(c), { returnOtherIfZero: true })
      } while ((Math.abs(pgcdAC - 1) >= Math.pow(10, -12)) || (Math.abs(pgcdBC - 1) >= Math.pow(10, -12)))
      fdex = '\\frac{' + j3pMonome(1, 1, a) + j3pMonome(2, 0, b) + '}{' + c + '}'
      monCoefa = a / c
      monCoefb = b / c
      expressA = '\\frac{' + a + '}{' + c + '}'
      expressB = '\\frac{' + b + '}{' + c + '}'
      if (b > 0) {
        formeAxb = '\\frac{' + a + '}{' + c + '}x+\\frac{' + b + '}{' + c + '}'
      } else {
        formeAxb = '\\frac{' + a + '}{' + c + '}x-\\frac{' + (-b) + '}{' + c + '}'
      }
    } else if (num === 4) {
      // fonction(b+ax)/x
      do {
        do {
          a = j3pGetRandomInt(-5, 5)
          b = j3pGetRandomInt(-5, 5)
        } while ((Math.abs(a) < Math.pow(10, -10)) || (Math.abs(b) < Math.pow(10, -10)))
      } while (Math.abs(j3pPGCD(Math.abs(a), Math.abs(b), { returnOtherIfZero: true }) - 1) >= Math.pow(10, -12))
      fdex = '\\frac{' + j3pMonome(1, 0, b) + j3pMonome(2, 1, a) + '}{x}'
      affine = false
      formeAxb = '\\frac{' + b + '}{x}' + j3pMonome(2, 0, a)// ce n’est pas une fonction affine mais je me sers de cette variable pour éviter d’en créer une autre'
    } else if (num === 6) {
      // fonction (ax+b)^2-(c+ax)^2
      let coefX, coefCst
      do {
        do {
          a = j3pGetRandomInt(-5, 5)
        } while (Math.abs(a) < 1.1)
        do {
          b = j3pGetRandomInt(-5, 5)
        } while (Math.abs(b) < Math.pow(10, -12))
        do {
          c = j3pGetRandomInt(-5, 5)
        } while (Math.abs(c) < Math.pow(10, -12))
        coefX = 2 * a * (b - c)
        coefCst = Math.pow(b, 2) - Math.pow(c, 2)
      } while ((Math.abs(coefX) < Math.pow(10, -12)) || (Math.abs(coefCst) < Math.pow(10, -12)))
      fdex = '(' + j3pMonome(1, 1, a) + j3pMonome(2, 0, b) + ')^2-(' + j3pMonome(1, 0, c) + j3pMonome(2, 1, a) + ')^2'
      monCoefa = coefX
      monCoefb = coefCst
      expressA = coefX
      expressB = coefCst
      formeAxb = j3pMonome(1, 1, coefX) + j3pMonome(2, 0, coefCst)
      stor.dvpt_quest6 = j3pMonome(1, 2, Math.pow(a, 2)) + j3pMonome(2, 1, 2 * a * b) + j3pMonome(3, 0, Math.pow(b, 2)) + '-(' + j3pMonome(1, 0, Math.pow(c, 2)) + j3pMonome(2, 1, 2 * a * c) + j3pMonome(3, 2, Math.pow(a, 2)) + ')'
    }
    me.logIfDebug('fdex : ' + fdex + '    formeAxb:' + formeAxb)
    me.logIfDebug('coef_a:' + monCoefa + '    coef_b:' + monCoefb)
    return {
      express_fct: fdex,
      expressAffine: formeAxb,
      fct_affine: affine,
      coef_a: monCoefa,
      coef_b: monCoefb,
      a_latex: expressA,
      b_latex: expressB
    }
  }

  function ecoute (laZone) {
    if (laZone.className.includes('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, laZone, {
        liste: ['racine', 'fraction']
      })
    }
  }

  function afficheCorrection (bonneReponse) {
    if (stor.nb_div === 3) j3pEmpty(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let i = 1; i <= 3; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    if (stor.numQuest <= 3) {
      j3pAffiche(stor.zoneExpli1, '', textes.corr1,
        {
          f: stor.nomF,
          g: stor.fonctionAffine.expressAffine,
          a: stor.fonctionAffine.a_latex,
          b: stor.fonctionAffine.b_latex
        })
      if (stor.numQuest === 2) {
        j3pAffiche(stor.zoneExpli2, '', textes.corr4)
      }
    } else if (stor.numQuest <= 5) {
      j3pAffiche(stor.zoneExpli1, '', textes.corr3)
      if (stor.numQuest === 4) {
        j3pAffiche(stor.zoneExpli2, '', textes.corr5 + ' ' + textes.corr6,
          { f: stor.nomF, g: stor.fonctionAffine.expressAffine })
      } else {
        j3pAffiche(stor.zoneExpli2, '', textes.corr7,
          {
            f: stor.nomF,
            g: stor.fonctionAffine.express_fct,
            h: stor.fonctionAffine.expressAffine
          })
        j3pAffiche(stor.zoneExpli3, '', textes.corr6)
      }
    } else if (stor.numQuest === 6) {
      j3pAffiche(stor.zoneExpli1, '', textes.corr2,
        {
          f: stor.nomF,
          e: stor.dvpt_quest6,
          d: stor.fonctionAffine.expressAffine,
          a: stor.fonctionAffine.a_latex,
          b: stor.fonctionAffine.b_latex
        })
    }
  }

  function enonceMain () {
    stor.numQuest = stor.tabex[me.questionCourante - 1]
    const nomF = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
    const fonctionAffine = generationFonction(stor.numQuest)
    const espacetxt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '5px' }) })
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons1, '', textes.consigne1, { f: nomF, g: fonctionAffine.express_fct })
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div', espacetxt)
    stor.idsRadio = [j3pGetNewId('radio1'), j3pGetNewId('radio2')]
    stor.nameRadio = j3pGetNewId('choix')
    j3pBoutonRadio(zoneCons2, stor.idsRadio[0], stor.nameRadio, 0, textes.cocher1)
    j3pAffiche(zoneCons2, '', espacetxt + espacetxt)
    j3pBoutonRadio(zoneCons2, stor.idsRadio[1], stor.nameRadio, 1, textes.cocher2)
    stor.nb_div = 2
    stor.zoneInput = []
    j3pElement(stor.idsRadio[0]).onclick = function () {
      if (stor.nb_div === 2) {
        stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div')
        const elt = j3pAffiche(stor.zoneCons3, '', textes.consigne2 + '\n' + textes.consigne3,
          {
            f: nomF,
            inputmq1: { texte: '' },
            inputmq2: { texte: '' }
          })
        stor.zoneInput = [...elt.inputmqList]
        stor.zoneCons4 = j3pAddElt(stor.conteneur, 'div')
        j3pStyle(stor.zoneCons4, { paddingTop: '15px' })
        stor.laPalette = j3pAddElt(stor.zoneCons4, 'div')
        // palette MQ :
        $(stor.zoneInput[0]).focusin(function () {
          ecoute(this)
        })
        $(stor.zoneInput[1]).focusin(function () {
          ecoute(this)
        })
        ecoute(stor.zoneInput[0])
        mqRestriction(stor.zoneInput[0], '\\d,./-', {
          commandes: ['racine', 'fraction']
        })
        mqRestriction(stor.zoneInput[1], '\\d,./-', {
          commandes: ['racine', 'fraction']
        })
        j3pFocus(stor.zoneInput[0])
        stor.nb_div = 3
        const mesZonesSaisie = stor.zoneInput.map(eltInput => eltInput.id)
        stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
      }
    }
    j3pElement(stor.idsRadio[1]).onclick = function () {
      if (stor.nb_div === 3) {
        j3pDetruit(stor.zoneCons3)
        j3pDetruit(stor.zoneCons4)
        stor.nb_div = 2
      }
    }
    stor.nomF = nomF
    stor.fonctionAffine = fonctionAffine
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // contrôle des paramètres
        if (!Array.isArray(ds.tab_ex)) {
          console.error(Error('Erreur de paramétrage, le paramètre tab_ex n’est pas un tableau'))
          ds.tab_ex = defaultTabEx
        }
        const tabEx = ds.tab_ex.filter(numTypeExo => Number.isInteger(numTypeExo) && numTypeExo > 0 && numTypeExo < 7)
        if (tabEx.length < ds.tab_ex) {
          console.error(Error(`Paramétrage invalide, tab_ex contient des types d’exercices invalides (ça doit être des entiers entre 1 et 6), [${ds.tab_ex.join(',')}] => [${tabEx.join(',')}]`))
          ds.tab_ex = tabEx.length ? tabEx : defaultTabEx
        }

        // on rempli tabex à partir d’un mélange des ≠ types possibles
        // (on utilise pas j3pGetRandomElt pour ne pas avoir plusieurs fois le même avec d’autres ignorés,
        // dans le cas où on aurait autant d’éléments dans tab_ex que nb_questions on veut un de chaque dans le désordre)
        stor.tabex = []
        let pioche = []
        while (stor.tabex.length < ds.nb_questions) {
          // quand tous sont utilisés on recommence
          if (!pioche.length) pioche = j3pShuffle(ds.tab_ex)
          stor.tabex.push(pioche.pop())
        }
        if (!Number.isInteger(ds.nb_questions) || ds.nb_questions < 1 || ds.nb_questions > 20) {
          console.error(`Paramètre nb_questions invadide (entier entre 1 et 20) : ${ds.nb_questions} => `)
        }

        // dans les params, c’est nb_questions et pas nbrepetitions, faut donc l’imposer ici
        me.surcharge({ nbrepetitions: ds.nb_questions })
        // Construction de la page
        me.construitStructurePage('presentation1')

        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      enonceMain()
      me.finEnonce()
      break // case "enonce":

    case 'correction':{
      // On teste si une réponse a été saisie
      let reponse = {
        aRepondu: false,
        bonneReponse: false
      }
      reponse.aRepondu = (j3pBoutonRadioChecked(stor.nameRadio)[0] > -1)
      const numRadioSelect = j3pBoutonRadioChecked(stor.nameRadio)[0] + 1
      const boutonCoche = reponse.aRepondu
      /** Pour savoir s’il y a des inputs et donc si on peut donner une chance de plus (sur du oui/non plusieurs chances n’ont pas de sens) */
      let hasInputs = false

      if (reponse.aRepondu) {
        // un bouton radio a été sélectionné
        if (j3pBoutonRadioChecked(stor.nameRadio)[0] === 0) {
          const fctsValid = stor.fctsValid
          // si l’élève clique sur "oui" et que c’est la bonne réponse, il faut valider les zones de saisie'
          stor.zoneInput[0].typeReponse = ['nombre', 'exact']
          stor.zoneInput[1].typeReponse = ['nombre', 'exact']
          reponse = { aRepondu: false, bonneReponse: false }
          reponse.aRepondu = fctsValid.valideReponses()
          if (reponse.aRepondu) {
            hasInputs = true
            if (stor.fonctionAffine.fct_affine) {
              stor.zoneInput[0].reponse = [stor.fonctionAffine.coef_a]
              stor.zoneInput[1].reponse = [stor.fonctionAffine.coef_b]
              reponse = fctsValid.validationGlobale()
            } else {
              fctsValid.zones.bonneReponse[0] = false
              fctsValid.zones.bonneReponse[1] = false
              fctsValid.coloreUneZone(stor.zoneInput[0].id)
              fctsValid.coloreUneZone(stor.zoneInput[1].id)
              reponse.bonneReponse = false
            }
          }
        } else {
          // il a répondu non => pas de zone de saisie => rien de spécial à faire sinon noter si c'était vrai ou pas
          reponse.bonneReponse = (!stor.fonctionAffine.fct_affine)
        }
      }

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        stor.zoneCorr.style.color = me.styles.cfaux
        if (!boutonCoche) {
          stor.zoneCorr.innerHTML = textes.comment1
        } else {
          stor.zoneCorr.innerHTML = textes.comment2
        }
        return this.finCorrection()
      }

      // Une réponse a été saisie
      // Bonne réponse
      if (reponse.bonneReponse) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        j3pDesactive(stor.idsRadio[0])
        j3pDesactive(stor.idsRadio[1])
        afficheCorrection(true)
        me.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux
      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        j3pDesactive(stor.idsRadio[0])
        j3pDesactive(stor.idsRadio[1])
        afficheCorrection(false)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      if ((j3pBoutonRadioChecked(stor.nameRadio)[0] === 0) && stor.fonctionAffine.affine) {
        stor.zoneCorr.innerHTML += textes.comment3
      }
      if (hasInputs && me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        return this.finCorrection() // on reste en correction
      }

      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      j3pDesactive(stor.idsRadio[0])
      j3pDesactive(stor.idsRadio[1])
      if ((Number(numRadioSelect) === 1) && !stor.fonctionAffine.fct_affine) {
        // il  sélectionné le premier bouton radio à tord
        j3pBarre('label' + stor.idsRadio[0])
      }
      if ((Number(numRadioSelect) === 2) && stor.fonctionAffine.fct_affine) {
        // il  sélectionné le deuxième bouton radio à tord
        j3pBarre('label' + stor.idsRadio[1])
      }
      afficheCorrection(false)
      me.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      me.finNavigation()
      break // case "navigation":
  }
}
