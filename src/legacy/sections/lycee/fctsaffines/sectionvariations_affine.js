import { j3pAddElt, j3pFocus, j3pGetRandomInt, j3pMonome, j3pNombre, j3pPGCD, j3pRandomTab, j3pRestriction, j3pShowError, j3pVirgule } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    novembre 2014
    Cette fonction permet de déteminer le sens de variation d’une fonction affine

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['a', ['m', 'm', 'm/n', 'm/n'], 'array', 'tableau regroupant les cas de figure proposés pour le coefficient a dans l’écriture ax+b, "m" signifie entier et "m/n" une fraction.<br>On peut aussi écrire racine(m) ou m*racine(n).<br>m et n peuvent être des nombres fixés.<br>Le nombre de questions sera alors égal à la longueur de ce tableau (et du suivant).'],
    ['b', ['p', 'p', 'p', 'p/q'], 'array', 'tableau regroupant les cas de figure proposés pour le coefficient a dans l’écriture ax+b, "p" signifie entier et "p/q" une fraction.<br>On peut aussi écrire racine(p) ou p*racine(q). Sa longueur est la même que pour le précédent tableau.<br>p et q peuvent être des nombres fixés.<br>'],
    ['ecriture_fct', 'alea', 'string', 'On peut écrire "ax+b", "b+ax" ou laisser le choix de l’écriture aléatoire : "alea". On peut aussi écrire "ax+b|b+ax|..." pour imposer d’une question à l’autre différentes formes pour l’expression. On peut aussi utiliser "ax/d+b" ou "b+ax/d" pour obtenir par exemple -2x/7+2.']
  ]
}

const textes = {
  titre_exo: 'Variations d’une fonction affine',
  consigne1: 'Soit $£f$ la fonction affine définie sur $\\R$ par $£f(x)=£g$.',
  consigne2: 'La fonction $£f$ est strictement @1@ sur $\\R$.',
  les_reponses1: 'croissante|croissant|croissantes|croisante|croisant|croisants',
  les_reponses2: 'décroissante|décroissant|décroissantes|décroisante|décroisant|décroisants|decroissante|decroissant|decroissantes|decroisante|decroisant|decroisants',
  corr1: 'Le sens de variation d’une fonction affine sous la forme $ax+b$ ou $b+ax$ est donné par le signe de $a$.',
  corr2: 'Ici $a=£a>0$, donc la fonction affine est <b>strictement croissante</b> sur $\\R$.',
  corr3: 'Ici $a=£a<0$, donc la fonction affine est <b>strictement décroissante</b> sur $\\R$.',
  comment1: 'Attention tout de même à l’orthographe (erreur corrigée par le logiciel) !'
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (isBonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    // var pos_y = j3pElement(le_nom+"consignes").offsetHeight+5;
    // la correction sera écrite en-dessous de la figure mtg32
    const color = (isBonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color } })
    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', textes.corr1)
    if (stor.propCoef.valCoefs[0] > 0) {
      j3pAffiche(stor.zoneExpli2, '', textes.corr2, { a: stor.propCoef.coefs[0] })
    } else {
      j3pAffiche(stor.zoneExpli2, '', textes.corr3, { a: stor.propCoef.coefs[0] })
    }
  }

  function generationCoef (typeCoef) {
    // typeCoef et typeCoef peut être de la forme "entier", "fraction", "racine(a)" ou "a*racine(b)"
    let aVal// valeur exacte ou approchée à 10^-15 près du coef
    let aLatex// valeur de aVal au format latex
    let ecriturePos = []// l’écriture du coef peut être différente suuivant sa position dans l’écriture dans l’écriture de f(x)'
    // en 0 c’est quand c’est le premier terme (devant x ou non), en 1, c’est le second
    if (typeCoef === 'entier') {
      do {
        aVal = j3pGetRandomInt(-7, 7)
      } while ((Math.abs(aVal) < Math.pow(10, -12)) || (Math.abs(Math.abs(aVal) - 1) < Math.pow(10, -12)))
      aLatex = j3pVirgule(aVal)
      ecriturePos = [aLatex, aLatex, aLatex, aLatex]// cela sera géré ensuite par j3pMonome
    } else if (typeCoef === 'fraction') {
      let num, den, pgcd
      do {
        do {
          num = j3pGetRandomInt(-8, 8)
        } while (Math.abs(num) < Math.pow(10, -12))
        den = j3pGetRandomInt(2, 9)
        pgcd = j3pPGCD(Math.abs(num), Math.abs(den), { returnOtherIfZero: true })
      } while (Math.abs(pgcd - 1) > Math.pow(10, -12))
      aVal = num / den
      aLatex = '\\frac{' + num + '}{' + den + '}'
      // il faut ausi que je gère les différentes écritures de la fraction suivant le coef et la position dans l’écriture ax+b ou b+ax'
      ecriturePos[0] = aLatex
      if (num < 0) {
        ecriturePos[1] = '-\\frac{' + (-num) + '}{' + den + '}'
      } else {
        ecriturePos[1] = '+\\frac{' + (num) + '}{' + den + '}'
      }
    } else if ((typeCoef === 'racine(a)') || (typeCoef === '-racine(a)')) {
      const radical = j3pRandomTab([2, 3, 5, 6, 7, 10, 11, 13], [0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125])
      aVal = Math.sqrt(radical)
      aLatex = '\\sqrt{' + radical + '}'
      // il faut ausi que je gère les différentes écritures de la racine suivant le coef et la position dans l’écriture ax+b ou b+ax'
      ecriturePos[0] = aLatex
      ecriturePos[1] = '+' + aLatex
      if (typeCoef === '-racine(a)') {
        aVal = -aVal
        aLatex = '-' + aLatex
        ecriturePos[0] = aLatex
        ecriturePos[1] = aLatex
      }
    } else if (typeCoef === 'a*racine(b)') {
      let coefA
      const radical2 = j3pRandomTab([2, 3, 5, 6, 7, 10, 11, 13], [0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125])
      do {
        coefA = j3pGetRandomInt(-7, 7)
      } while ((Math.abs(coefA) < Math.pow(10, -12)) || (Math.abs(coefA - 1) < Math.pow(10, -12)))
      aVal = coefA * Math.sqrt(radical2)
      if (Math.abs(coefA + 1) < Math.pow(10, -12)) {
        aLatex = '-\\sqrt{' + radical2 + '}'
      } else {
        aLatex = j3pVirgule(coefA) + '\\sqrt{' + radical2 + '}'
      }
      ecriturePos[0] = aLatex
      if (coefA > 0) {
        ecriturePos[1] = '+' + aLatex
      } else {
        if (Math.abs(coefA + 1) < Math.pow(10, -12)) {
          ecriturePos[1] = aLatex
        } else {
          ecriturePos[1] = '-' + j3pVirgule(-coefA) + '\\sqrt{' + radical2 + '}'
        }
      }
    }
    return { valeur: aVal, val_txt: aLatex, ecritureCoefs: ecriturePos }
  }
  function ecritureFct (proprietesCoef) {
    // var proprietesCoef = {coefs:mesCoefs,valCoefs:mesCoefsVal,type:typeCoef,ecriture:ecriture}
    let fdex, tabAcc, monNum, monDen, signeInit
    if (proprietesCoef.type[1] === 'entier') {
      if (proprietesCoef.type[0] === 'entier') {
        if (proprietesCoef.ecriture === 'ax+b') {
          fdex = j3pMonome(1, 1, j3pNombre(proprietesCoef.coefs[0])) + j3pMonome(2, 0, j3pNombre(proprietesCoef.coefs[1]))
        } else {
          fdex = j3pMonome(1, 0, j3pNombre(proprietesCoef.coefs[1])) + j3pMonome(2, 1, j3pNombre(proprietesCoef.coefs[0]))
        }
      } else {
        if ((proprietesCoef.type[0] === 'fraction') && ((proprietesCoef.ecriture === 'ax/d+b') || (proprietesCoef.ecriture === 'b+ax/d'))) {
          // proprietesCoef.ecritureCoefPos[0][0] est de la forme (-)\\frac{...}{...}
          tabAcc = proprietesCoef.ecritureCoefPos[0][0].split('}{')
          monNum = tabAcc[0].substring(tabAcc[0].indexOf('{') + 1)
          monDen = tabAcc[1].substring(0, tabAcc[1].length - 1)
          signeInit = ''
          if (proprietesCoef.ecritureCoefPos[0][0].charAt(0) === '-') signeInit = '-'
          if (proprietesCoef.ecriture === 'ax/d+b') {
            fdex = signeInit + '\\frac{' + j3pMonome(1, 1, j3pNombre(monNum)) + '}{' + monDen + '}' + j3pMonome(2, 0, j3pNombre(proprietesCoef.coefs[1]))
          } else if (proprietesCoef.ecriture === 'b+ax/d') {
            if (signeInit === '') {
              signeInit = '+'
            }
            fdex = j3pMonome(1, 0, j3pNombre(proprietesCoef.coefs[1])) + signeInit + '\\frac{' + j3pMonome(1, 1, j3pNombre(monNum)) + '}{' + monDen + '}'
          }
        } else {
          if (proprietesCoef.ecriture === 'ax+b') {
            fdex = proprietesCoef.ecritureCoefPos[0][0] + 'x' + j3pMonome(2, 0, j3pNombre(proprietesCoef.coefs[1]))
          } else {
            fdex = j3pMonome(1, 0, j3pNombre(proprietesCoef.coefs[1])) + proprietesCoef.ecritureCoefPos[0][1] + 'x'
          }
        }
      }
    } else {
      if (proprietesCoef.type[0] === 'entier') {
        if (proprietesCoef.ecriture === 'ax+b') {
          fdex = j3pMonome(1, 1, j3pNombre(proprietesCoef.coefs[0])) + proprietesCoef.ecritureCoefPos[1][1]
        } else {
          fdex = proprietesCoef.ecritureCoefPos[1][0] + j3pMonome(2, 1, j3pNombre(proprietesCoef.coefs[0]))
        }
      } else {
        if ((proprietesCoef.type[0] === 'fraction') && ((proprietesCoef.ecriture === 'ax/d+b') || (proprietesCoef.ecriture === 'b+ax/d'))) {
          // proprietesCoef.ecritureCoefPos[0][0] est de la forme (-)\\frac{...}{...}
          tabAcc = proprietesCoef.ecritureCoefPos[0][0].split('}{')
          monNum = tabAcc[0].substring(tabAcc[0].indexOf('{') + 1)
          monDen = tabAcc[1].substring(0, tabAcc[1].length - 1)
          signeInit = ''
          if (proprietesCoef.ecritureCoefPos[0][0].charAt(0) === '-') {
            signeInit = '-'
          }
          if (proprietesCoef.ecriture === 'ax/d+b') {
            fdex = signeInit + '\\frac{' + j3pMonome(1, 1, j3pNombre(monNum)) + '}{' + monDen + '}' + proprietesCoef.ecritureCoefPos[1][1]
          } else if (proprietesCoef.ecriture === 'b+ax/d') {
            if (signeInit === '') {
              signeInit = '+'
            }
            fdex = proprietesCoef.ecritureCoefPos[1][0] + signeInit + '\\frac{' + j3pMonome(1, 1, j3pNombre(monNum)) + '}{' + monDen + '}'
          }
        } else {
          if (proprietesCoef.ecriture === 'ax+b') {
            fdex = proprietesCoef.ecritureCoefPos[0][0] + 'x' + proprietesCoef.ecritureCoefPos[1][1]
          } else {
            fdex = proprietesCoef.ecritureCoefPos[1][0] + proprietesCoef.ecritureCoefPos[0][1] + 'x'
          }
        }
      }
    }

    return fdex
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 4,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      // Paramètres de la section, avec leurs valeurs par défaut.
      a: ['m', 'm', 'm/n', 'm/n'],
      b: ['p', 'p', 'p', 'p/q'],
      ecriture_fct: 'alea'
    }
  }

  function enonceMain () {
    stor.ecriture_fct = stor.tabEcritureFct[me.questionCourante - 1]
    // maintenant il ne reste plus qu'à écrire corrctement l’expression de la fonction affine
    let ecriture = 'ax+b'
    if ((stor.ecriture_fct === 'b+ax') || (stor.ecriture_fct === 'ax/d+b') || (stor.ecriture_fct === 'b+ax/d')) {
      ecriture = stor.ecriture_fct
    } else if (stor.ecriture_fct === 'alea') {
      ecriture = j3pRandomTab(['ax+b', 'b+ax'], [0.5, 0.5])
    }

    // je génère la fonction affine
    if ((!ds.a[me.questionCourante - 1]) || (ds.a[me.questionCourante - 1] === '')) {
      ds.a[me.questionCourante - 1] = 'a'
    }
    if ((!ds.b[me.questionCourante - 1]) || (ds.b[me.questionCourante - 1] === '')) {
      ds.b[me.questionCourante - 1] = 'a'
    }
    const choixCoef = (['ax/d+b', 'b+ax/d'].includes(stor.ecriture_fct))
      ? ['m/n', 'm']
      : [ds.a[me.questionCourante - 1], ds.b[me.questionCourante - 1]]
    // ["a",["m","m","m/n","m/n"],'array','tableau regroupant les cas de figure proposés pour le coefficient a dans l’écriture ax+b, "m" signifie entier et "m/n" une fraction.<br>On peut aussi écrire racine(m) ou m*racine(n).<br>m et n peuvent être des nombres fixés.<br>Le nombre de questions sera alors égal à la longueur de ce tableau (et du suivant).'],
    // on cherche le type de nombre attendu (entier, fraction, racine(a) ou a*racine(b)
    const entierReg = /^[a-z]/ // new RegExp('^[a-z]$', 'i')
    const fractionReg = /[a-z]\/[a-z]/ // new RegExp('[a-z]/[a-z]', 'i')
    const racine1Reg = /^racine\([a-z]\)/ // new RegExp('^racine\\([a-z]\\)', 'i')
    const racine2Reg = /(-|[a-z])\*?racine\([a-z]\)/ // new RegExp('(\\-|[a-z])\\*?racine\\([a-z]\\)', 'i')
    // et les suivants dans le cas où on impose la valeur
    const fractionImpReg = /-?[0-9]+\/[0-9]+/ // new RegExp('\\-?[0-9]{1,}/[-0-9]{1,}', 'i')
    const racine1ImpReg = /^racine\([0-9.,]+\)/ // new RegExp('^racine\\([0-9\\.,]{1,}\\)', 'i')
    const racine2ImpReg = /[-0-9]+\*?racine\([0-9.,]+\)/ // new RegExp('[-0-9]{1,}\\*?racine\\([0-9\\.,]{1,}\\)', 'i')
    let nbTentatives = 0
    let pbEcriture = false
    const typeCoef = []
    const ecritureCoefPos = []
    const mesCoefs = []
    const mesCoefsVal = []
    do {
      const propCoef = []
      for (let i = 0; i <= 1; i++) {
        let choixEffectue = false
        if ((choixCoef[i].length === 3) && fractionReg.test(choixCoef[i])) {
          typeCoef[i] = 'fraction'
          // on choisit une fraction de manière aléatoire
          choixEffectue = true
        } else if (racine2Reg.test(choixCoef[i])) {
          if (choixCoef[i].charAt(0) === '-') {
            typeCoef[i] = '-racine(a)'// on choisit un nombre de la forme -racine(b)
          } else {
            typeCoef[i] = 'a*racine(b)'// on choisit un nombre de la forme a*racine(b)
          }
          choixEffectue = true
        } else if ((choixCoef[i].length === 9) && racine1Reg.test(choixCoef[i])) {
          typeCoef[i] = 'racine(a)'
          // on choisit un nombre de la forme racine(a)
          choixEffectue = true
        } else if (entierReg.test(choixCoef[i])) {
          typeCoef[i] = 'entier'
          // on choisit un entier de manière aléatoire
          choixEffectue = true
        }
        if (choixEffectue) {
          if (typeCoef[i] === 'entier') {
            if (i === 0) {
              propCoef[i] = generationCoef(typeCoef[i])
            } else {
              propCoef[i] = generationCoef(typeCoef[i])
            }
          } else {
            propCoef[i] = generationCoef(typeCoef[i])
          }

          mesCoefs[i] = propCoef[i].val_txt
          mesCoefsVal[i] = propCoef[i].valeur
          ecritureCoefPos[i] = propCoef[i].ecritureCoefs
        } else {
          ecritureCoefPos[i] = []
          // dans ce cas c’est que l’utilisateur veut entrer sa propre valeur fixée'
          if (racine1ImpReg.test(choixCoef[i])) {
            // l’utilisateur a écrit un nombre de la forme racine(b)'
            const tabRacineInit = choixCoef[i].match(racine1ImpReg)
            // c’est un nombre de la forme racine(...)'
            const nbRacine = j3pNombre(tabRacineInit[0].substring(7, tabRacineInit[0].length - 1))
            if (isNaN(nbRacine)) {
              j3pShowError('je ne comprends pas le nombre ' + choixCoef[i], { vanishAfter: 5 })
              pbEcriture = true
            } else {
              mesCoefs[i] = '\\sqrt{' + j3pVirgule(nbRacine) + '}'
              mesCoefsVal[i] = Math.sqrt(nbRacine)
            }
            typeCoef[i] = 'racine(a)'
            ecritureCoefPos[i][0] = mesCoefs[i]
            ecritureCoefPos[i][1] = '+' + mesCoefs[i]
          } else if (racine2ImpReg.test(choixCoef[i])) {
            const racine1bisImpReg = /racine\([0-9.,]+\)/ // new RegExp('racine\\([0-9\\.,]{1,}\\)', 'i')
            const tabRacine = choixCoef[i].match(racine1bisImpReg)
            mesCoefs[i] = choixCoef[i]
            for (let kReg = 0; kReg < tabRacine.length; kReg++) {
              const radical = tabRacine[kReg].substring(7, tabRacine[kReg].length - 1)
              mesCoefs[i] = mesCoefs[i].replace('*' + tabRacine[kReg], '\\sqrt{' + radical + '}')
              mesCoefs[i] = mesCoefs[i].replace(tabRacine[kReg], '\\sqrt{' + radical + '}')
            }
            ecritureCoefPos[i][0] = mesCoefs[i]
            if (mesCoefs[i].charAt(0) === '-') {
              ecritureCoefPos[i][1] = mesCoefs[i]
            } else {
              ecritureCoefPos[i][1] = '+' + mesCoefs[i]
            }
            const arbreRacine = new Tarbre(choixCoef[i], ['x'])
            mesCoefsVal[i] = arbreRacine.evalue(['0'])
            for (let ii = 1; ii <= 3; ii++) {
              const calcRacine = arbreRacine.evalue([ii])
              if (Math.abs(mesCoefsVal[i] - calcRacine) > Math.pow(10, -12)) {
                // cela signifie qu’il y a un pb avec choix_nbs'
                pbEcriture = true
                j3pShowError('je ne comprends pas le nombre ' + choixCoef[i], { vanishAfter: 5 })
              }
            }
            typeCoef[i] = 'a*racine(b)'
          } else if (fractionImpReg.test(choixCoef[i])) {
            let numFrac = choixCoef[i].split('/')[0]
            let denFrac = choixCoef[i].split('/')[1]
            let signeFracTxt, coefMoins1
            if (numFrac.charAt(0) === '-') {
              // on exclut le signe moins de la fraction
              signeFracTxt = '-'
              coefMoins1 = -1
              numFrac = numFrac.substring(1, numFrac.length)
            } else {
              signeFracTxt = ''
              coefMoins1 = 1
            }
            if (numFrac.charAt(0) === ('(')) numFrac = numFrac.substring(1, numFrac.length)
            if (numFrac.charAt(numFrac.length - 1) === (')')) numFrac = numFrac.substring(0, numFrac.length - 1)
            if (denFrac.charAt(0) === ('(')) denFrac = denFrac.substring(1, denFrac.length)
            if (denFrac.charAt(denFrac.length - 1) === (')')) denFrac = denFrac.substring(0, denFrac.length - 1)
            numFrac = j3pNombre(numFrac)
            denFrac = j3pNombre(denFrac)
            if (isNaN(numFrac) || isNaN(denFrac)) {
              j3pShowError('je ne comprends pas le nombre ' + choixCoef[i], { vanishAfter: 5 })
              pbEcriture = true
            } else {
              mesCoefs[i] = signeFracTxt + '\\frac{' + j3pVirgule(numFrac) + '}{' + j3pVirgule(denFrac + '}')
              ecritureCoefPos[i][0] = mesCoefs[i]
              if (coefMoins1 < 0) {
                ecritureCoefPos[i][1] = mesCoefs[i]
              } else {
                ecritureCoefPos[i][1] = '+' + mesCoefs[i]
              }
              mesCoefsVal[i] = coefMoins1 * numFrac / denFrac
            }
            typeCoef[i] = 'fraction'
          } else if (!isNaN(j3pNombre(choixCoef[i]))) {
            mesCoefs[i] = choixCoef[i]
            mesCoefsVal[i] = j3pNombre(choixCoef[i])
            typeCoef[i] = 'entier'
            ecritureCoefPos[i][0] = ecritureCoefPos[i][1] = mesCoefs[i]
          } else {
            // on peut encore avoir des cas de figure avec pi
            if (choixCoef[i].indexOf('pi') > -1) {
              const piTab = choixCoef[i].split('pi')
              let newChoixNbs = ''
              if (piTab.length > 1) {
                for (let j = 0; j < piTab.length - 1; j++) {
                  if (piTab[j].charAt(piTab[j].length - 1) === '*') {
                    // alors on vire ce signe de multiplication
                    newChoixNbs += piTab[j].substring(0, piTab[j].length - 1) + '\\pi'
                  } else {
                    newChoixNbs += piTab[j] + '\\pi'
                  }
                }
                newChoixNbs += piTab[piTab.length - 1]
                mesCoefs[i] = newChoixNbs
                if ((mesCoefs[i].substring(1).indexOf('-') > -1) || (mesCoefs[i].substring(1).indexOf('+') > -1)) {
                  // on met alors des parenthèses
                  ecritureCoefPos[i][0] = '(' + mesCoefs[i] + ')'
                  ecritureCoefPos[i][1] = '+(' + mesCoefs[i] + ')'
                } else {
                  if (mesCoefs[i].charAt(0) === '-') {
                    ecritureCoefPos[i][0] = mesCoefs[i]
                    ecritureCoefPos[i][1] = mesCoefs[i]
                  } else {
                    ecritureCoefPos[i][0] = mesCoefs[i]
                    ecritureCoefPos[i][1] = '+' + mesCoefs[i]
                  }
                }
              } else {
                newChoixNbs = choixCoef[i]
                mesCoefs[i] = newChoixNbs.replace('pi', '\\pi')
                ecritureCoefPos[i][0] = mesCoefs[i]
                ecritureCoefPos[i][0] = '+' + mesCoefs[i]
              }
              const piReg = /[0-9]pi/g // new RegExp('[0-9]pi', 'ig')
              while (piReg.test(choixCoef[i])) {
                const cas1 = choixCoef[i].match(piReg)[0]
                const posPi = cas1.indexOf('pi')
                const cas1Bis = cas1.substring(0, posPi) + '*' + cas1.substring(posPi)
                choixCoef[i] = choixCoef[i].replace(cas1, cas1Bis)
              }
              while (choixCoef[i].indexOf('pi') > -1) {
                choixCoef[i] = choixCoef[i].replace('pi', Math.PI)
              }
              const arbreNb = new Tarbre(choixCoef[i], ['x'])
              mesCoefsVal[i] = arbreNb.evalue([0])
              for (let k = 1; k <= 3; k++) {
                if (Math.abs(mesCoefsVal[i] - arbreNb.evalue([k])) > Math.pow(10, -12)) {
                  // ce n’est pas une constante'
                  pbEcriture = true
                }
              }
              typeCoef[i] = 'autre'
              if (pbEcriture) {
                j3pShowError('je ne comprends pas le nombre ' + choixCoef[i], { vanishAfter: 5 })
              }
            } else {
              // je ne vois pas d’autres cas'
              j3pShowError('je ne comprends pas le nombre ' + choixCoef[i], { vanishAfter: 5 })
              pbEcriture = true
            }
          }
        }
      }// fin du for
      me.logIfDebug('mesCoefs:' + mesCoefs)
      nbTentatives++
    } while ((Math.abs(Math.abs(mesCoefsVal[0]) - Math.abs(mesCoefsVal[1])) < Math.pow(10, -12)) && (nbTentatives > 50) && !pbEcriture)

    let fdex
    if (nbTentatives >= 50) {
      j3pShowError('les deux coefficients sont égaux, ce qui n’est pas pertinent', { vanishAfter: 5 })
    } else if (!pbEcriture) {
      const proprietesCoef = {
        coefs: mesCoefs,
        valCoefs: mesCoefsVal,
        type: typeCoef,
        ecriture,
        ecritureCoefPos
      }
      fdex = ecritureFct(proprietesCoef)
      stor.propCoef = proprietesCoef
    }
    me.logIfDebug('choixCoef:' + choixCoef)
    stor.nom_fct = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // j3pDiv(me.zonesElts.MG,{id:le_nom+"consignes",contenu:"",coord:[0,0],style:me.styles.etendre('petit.enonce',{padding:"5px"})});
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // j3pDiv(le_nom+"consignes",{id:le_nom+"zone_consigne1",contenu:"",style:{}});
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, { f: stor.nom_fct, g: fdex })
    // j3pDiv(le_nom+"consignes",{id:le_nom+"zone_consigne2",contenu:"",style:{}});
    const elt = j3pAffiche(stor.zoneCons2, '', textes.consigne2,
      {
        f: stor.nom_fct,
        input1: { texte: '', dynamique: true, width: '5px' }
      })
    stor.zoneInput = elt.inputList[0]
    j3pRestriction(stor.zoneInput, 'a-zéèçà\\s')
    stor.zoneInput.typeReponse = ['texte']
    if (stor.propCoef.valCoefs[0] > 0) {
      stor.zoneInput.reponse = textes.les_reponses1.split('|')
    } else {
      stor.zoneInput.reponse = textes.les_reponses2.split('|')
    }
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '5px' }) })
    j3pFocus(stor.zoneInput)

    const mesZonesSaisie = [stor.zoneInput]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage('presentation1')

        /*
                 Par convention,`
                `   this.typederreurs[0] = nombre de bonnes réponses
                    this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    this.typederreurs[2] = nombre de mauvaises réponses
                    this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        // on gère le nombre de répétitions
        // on vérifie déjà que a et b ont la même longueur.
        ds.nbrepetitions = Math.min(ds.a.length, ds.b.length)
        ds.nbitems = ds.nbetapes * ds.nbrepetitions

        // au début on gère ecriture_fct
        let tabEcritureFct = []
        if (ds.ecriture_fct.includes('|')) {
          // on impose d’une quetion à l’autre la forme de la question'
          tabEcritureFct = ds.ecriture_fct.split('|')
        }
        for (let i = 0; i < ds.nbrepetitions; i++) {
          if (!ds.ecriture_fct.includes('|')) {
            tabEcritureFct[i] = ds.ecriture_fct
          } else {
            if ((!tabEcritureFct[i]) || (tabEcritureFct[i] === '')) {
              tabEcritureFct[i] = 'alea'
            }
          }
        }
        stor.tabEcritureFct = tabEcritureFct
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid

      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      const reponse = fctsValid.validationGlobale()

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        return me.finCorrection()
      }

      // Une réponse a été saisie
      // Bonne réponse
      if (reponse.bonneReponse) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        if (String(fctsValid.zones.reponseSaisie[0]) !== String(stor.zoneInput.reponse[0])) {
          // c’est que l’élève a fait une faute d’orthographe tolérée par le logiciel
          stor.zoneCorr.innerHTML += '<br>' + textes.comment1
        }
        afficheCorrection(true)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        afficheCorrection(false)
        j3pDesactive(stor.zoneInput)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      if (me.essaiCourant >= ds.nbchances) {
        // Erreur au dernier essai
        stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
        me.typederreurs[2]++
        afficheCorrection(false)
        me.etat = 'navigation'
        return me.finCorrection('navigation', true)
      }
      // il reste des essais
      stor.zoneCorr.innerHTML += '<br>' + essaieEncore
      me.typederreurs[1]++
      // on reste dans l’état correction
      me.finCorrection()
      break // case le_nom+"correction":
    }

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
