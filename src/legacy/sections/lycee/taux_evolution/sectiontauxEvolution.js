import { j3pAddElt, j3pAjouteBouton, j3pChaine, j3pFocus, j3pNombre, j3pDetruit, j3pGetRandomInt, j3pVirgule, j3pGetBornesIntervalle, j3pShowError, j3pGetNewId } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitToutesLesFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pExtraireNumDen } from 'src/legacy/core/functionsLatex'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
   Exemple de Graphe de test
   j3p.html?graphe=[1,"tauxEvolution",[{pe:">=0",nn:"2",conclusion:"Etape 2"}]];[2,"tauxEvolution",[{pe:">=0",nn:"fin",conclusion:"fin"}]];
 */

/*
        DENIAUD Rémi
        octobre 2018
        Dans cette section, on peut calculer un taux d’évolution, calculer une valeur finale à l’aide d’un taux et/ou une valeur finale à l’aide d’un taux
        Section adapatée au clavier virtuel en 10/2021
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuestions', [1, 2, 3], 'array', 'Pour chaque répétition, on peut proposer entre 1 et 3 questions (les 3 par défaut).<br/>1 correspond au calcul d’une valeur d’arrivée;<br/>2 au calcul d’un taux d’évolution;<br/>3 au calcul d’une valeur de départ.'],
    ['seuilReussite', 0.8, 'reel', 'seuil au-dessus duquel on considère que l’élève a réussi l’exercice'],
    ['seuilErreur', 0.4, 'reel', 'seuil en-dessous duquel on considère que la notion n’est pas acquise']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'Le calcul d’un taux d’évolution n’est pas acquis' },
    { pe_3: 'Le calcul d’une valeur d’arrivée à l’aide d’un taux d’évolution n’est pas acquis' },
    { pe_4: 'Le calcul d’une valeur de départ à l’aide d’un taux d’évolution n’est pas acquis' },
    { pe_5: 'Le calcul d’un taux d’évolution et d’une valeur d’arrivée à l’aide d’un taux d’évolution ne sont pas acquis' },
    { pe_6: 'Le calcul d’un taux d’évolution et d’une valeur de départ à l’aide d’un taux d’évolution ne sont pas acquis' },
    { pe_7: 'Le calcul d’une valeur d’arrivée et d’une valeur de départ à l’aide d’un taux d’évolution ne sont pas acquis' },
    { pe_8: 'Gros soucis avec les arrondis' },
    { pe_9: 'Insuffisant' }
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo1: 'Calcul d’un taux d’évolution',
  titre_exo2: 'Calculs à l’aide d’un taux d’évolution',
  titre_exo3: 'Calcul à l’aide d’un taux d’évolution',
  titre_exo4: 'Evolution d’une grandeur',
  // il faut parfois gérer la présence de valeurs approchées et donc ajouter ces mots...
  txt1: 'environ ',
  txt2: 'd’environ',
  txt3: 'de',
  liste1: 'augmenté',
  liste2: 'diminué',
  // les consignes :
  cons1: '',
  // les différents commentaires envisagés suivant les réponses fausses de l’élève
  comment1: 'N’y aurait-il pas un problème d’arrondi&nbsp;?',
  comment2: 'On rappelle par ailleurs que pour arrondir le taux à £a&nbsp;%, il faut arrondir la valeur décimale à £b.',
  // et les phrases utiles pour les explications de la réponse
  // corr1 : "Le calcul du taux d’évolution en pourcentage se fait grâce à la formule :<br/> $\\frac{\\text{Valeur d’arrivée}-\\text{Valeur de départ}}{\\text{Valeur de départ}}\\times 100$.",
  corr1: 'Le calcul du taux d’évolution se fait grâce à la formule : $\\frac{\\text{Valeur d’arrivée}-\\text{Valeur de départ}}{\\text{Valeur de départ}}$.',
  corr2: 'La situation peut être résumée par le schéma ci-contre :',
  corr3: '£{r1}\\text{ soit £e}£r'
}
/**
 * Ajouter une description ici
 * @fileOverview
 * @author Remi DENIAUD <remi.deniaud@sesamath.net>
*/

/**
 * section tauxEvolution
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV7AAADnAAAAQEAAAAAAAAAAQAAAE7#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAFAAFAaRPXCj1wpEBbwUeuFHre#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAAQ4AAUkAwBgAAAAAAAAAAAAAAAAAAAUAAUBS2FHrhR64AAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAQAAAAP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAABAAAABP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQUAAQAAAAcAAAAJAP####8BAAAAAQ4AAUoAwCgAAAAAAADAEAAAAAAAAAUAAgAAAAf#####AAAAAgAHQ1JlcGVyZQD#####Aebm5gABAAAAAQAAAAMAAAAJAQEAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEBAAAAAAMAAAAM#####wAAAAEACUNMb25ndWV1cgD#####AAAAAQAAAA3#####AAAAAQAHQ0NhbGN1bAD#####AAduYmdyYWR4AAIyMAAAAAFANAAAAAAAAAAAABEA#####wAHbmJncmFkeQACMjAAAAABQDQAAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAUR3JhZHVhdGlvbkF4ZXNSZXBlcmUAAAAbAAAACAAAAAMAAAAKAAAADwAAABD#####AAAAAQATQ0Fic2Npc3NlT3JpZ2luZVJlcAAAAAARAAVhYnNvcgAAAAr#####AAAAAQATQ09yZG9ubmVlT3JpZ2luZVJlcAAAAAARAAVvcmRvcgAAAAoAAAALAAAAABEABnVuaXRleAAAAAr#####AAAAAQAKQ1VuaXRleVJlcAAAAAARAAZ1bml0ZXkAAAAK#####wAAAAEAEENQb2ludERhbnNSZXBlcmUAAAAAEQAAAAAAEAAAAQUAAAAACgAAAA4AAAASAAAADgAAABMAAAAWAAAAABEAAAAAABAAAAEFAAAAAAoAAAANAAAAAA4AAAASAAAADgAAABQAAAAOAAAAEwAAABYAAAAAEQAAAAAAEAAAAQUAAAAACgAAAA4AAAASAAAADQAAAAAOAAAAEwAAAA4AAAAVAAAADAAAAAARAAAAFgAAAA4AAAAPAAAADwAAAAARAAAAAAAQAAABBQAAAAAXAAAAGQAAAAwAAAAAEQAAABYAAAAOAAAAEAAAAA8AAAAAEQAAAAAAEAAAAQUAAAAAGAAAABv#####AAAAAQAIQ1NlZ21lbnQAAAAAEQEAAAAAEAAAAQABAAAAFwAAABoAAAAXAAAAABEBAAAAABAAAAEAAQAAABgAAAAcAAAABAAAAAARAQAAAAALAAFXAMAUAAAAAAAAwDQAAAAAAAAFAAE#3FZ4mrzfDgAAAB3#####AAAAAgAIQ01lc3VyZVgAAAAAEQAGeENvb3JkAAAACgAAAB8AAAARAAAAABEABWFic3cxAAZ4Q29vcmQAAAAOAAAAIP####8AAAACABJDTGlldU9iamV0UGFyUHRMaWUBAAAAEQFmZmYAAAAfAAAADgAAAA8AAAAfAAAAAgAAAB8AAAAfAAAAEQAAAAARAAVhYnN3MgANMiphYnNvci1hYnN3MQAAAA0BAAAADQIAAAABQAAAAAAAAAAAAAAOAAAAEgAAAA4AAAAhAAAAFgAAAAARAQAAAAAQAAABBQAAAAAKAAAADgAAACMAAAAOAAAAEwAAABkBAAAAEQFmZmYAAAAkAAAADgAAAA8AAAAfAAAABQAAAB8AAAAgAAAAIQAAACMAAAAkAAAABAAAAAARAQAAAAALAAFSAEAgAAAAAAAAwCAAAAAAAAAFAAE#0RtOgbToHwAAAB7#####AAAAAgAIQ01lc3VyZVkAAAAAEQAGeUNvb3JkAAAACgAAACYAAAARAAAAABEABW9yZHIxAAZ5Q29vcmQAAAAOAAAAJwAAABkBAAAAEQFmZmYAAAAmAAAADgAAABAAAAAmAAAAAgAAACYAAAAmAAAAEQAAAAARAAVvcmRyMgANMipvcmRvci1vcmRyMQAAAA0BAAAADQIAAAABQAAAAAAAAAAAAAAOAAAAEwAAAA4AAAAoAAAAFgAAAAARAQAAAAAQAAABBQAAAAAKAAAADgAAABIAAAAOAAAAKgAAABkBAAAAEQFmZmYAAAArAAAADgAAABAAAAAmAAAABQAAACYAAAAnAAAAKAAAACoAAAAr#####wAAAAIADENDb21tZW50YWlyZQAAAAARAWZmZgA#8AAAAAAAAEAcAAAAAAAAAAAAHwsAAf###wAAAAEAAAAAAAsjVmFsKGFic3cxKQAAABkBAAAAEQFmZmYAAAAtAAAADgAAAA8AAAAfAAAABAAAAB8AAAAgAAAAIQAAAC0AAAAbAAAAABEBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAkCwAB####AAAAAQAAAAAACyNWYWwoYWJzdzIpAAAAGQEAAAARAWZmZgAAAC8AAAAOAAAADwAAAB8AAAAGAAAAHwAAACAAAAAhAAAAIwAAACQAAAAvAAAAGwAAAAARAWZmZgDAIAAAAAAAAD#wAAAAAAAAAAAAJgsAAf###wAAAAIAAAABAAsjVmFsKG9yZHIxKQAAABkBAAAAEQFmZmYAAAAxAAAADgAAABAAAAAmAAAABAAAACYAAAAnAAAAKAAAADEAAAAbAAAAABEBZmZmAMAcAAAAAAAAAAAAAAAAAAAAAAArCwAB####AAAAAgAAAAEACyNWYWwob3JkcjIpAAAAGQEAAAARAWZmZgAAADMAAAAOAAAAEAAAACYAAAAGAAAAJgAAACcAAAAoAAAAKgAAACsAAAAzAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACv####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQAAAAAAAAAAAAAABAAAAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAUAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAgD#####AQAAAAAQAAABBQABQGkgAAAAAABAcqCj1wo9cAAAAAcA#####wEAAAAAAQAAADcAAAA1#####wAAAAIAHENNYXJxdWVBbmdsZU9yaWVudGVJbmRpcmVjdGUA#####wAAAAAAAQAAAAFAbgOIU0p1+gAAADUAAAA3AAAANgAAAAAbAP####8AAAAAAAAAAAAAAAAAQBQAAAAAAAAAAAA1EAAAAAAAAQAAAAAAEVZhbGV1cgpkZSBkw6lwYXJ0AAAAGwD#####AAAAAAAAAAAAAAAAAEAIAAAAAAAAAAAANhAAAAAAAAEAAAAAABFWYWxldXIKZCdhcnJpdsOpZQAAAAgA#####wAAAAUAAAA4AAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAQAAADwAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAAPAAAABEA#####wAFcG9zVkEAAi0xAAAAHAAAAAE#8AAAAAAAAAAAABEA#####wAFcG9zVkQAAi0xAAAAHAAAAAE#8AAAAAAAAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAAcAAAAAUAAAAAAAAAAAAAADgAAAEAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAUAAAAAAAAAAAAAADgAAAD8AAAARAP####8AAlZBAAQxMjAwAAAAAUCSwAAAAAAAAAAAEQD#####AAJWRAAEMTAwMAAAAAFAj0AAAAAAAAAAABsA#####wAAAAAAQAAAAAAAAADAGAAAAAAAAAAAAEEQAAAAAAABAAAAAgAII1ZhbChWRCkAAAAbAP####8AAAAAAAAAAAAAAAAAwBwAAAAAAAAAAABCEAAAAAAAAQAAAAIACCNWYWwoVkEpAAAAEQD#####AAtwb3NUYXV4UGx1cwABMQAAAAE#8AAAAAAAAAAAABEA#####wAMcG9zVGF1eE1vaW5zAAEyAAAAAUAAAAAAAAAAAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAEAAAAAAAAAAAAAAA4AAABHAAAAEQD#####AAR0YXV4AAMtMjUAAAAcAAAAAUA5AAAAAAAAAAAAEQD#####AAR0ZXN0AAZ0YXV4PjAAAAANBQAAAA4AAABKAAAAAQAAAAAAAAAAAAAAEQD#####AAp2YWxBYnNUYXV4AAlhYnModGF1eCn#####AAAAAgAJQ0ZvbmN0aW9uAAAAAA4AAABK#####wAAAAIABkNMYXRleAD#####AAAAAAEAAABJEAAAAAAAAQAAAAEAb1xJZnt0ZXN0fXtcdGltZXNcbGVmdCgxK1xmcmFje1xWYWx7dmFsQWJzVGF1eH19ezEwMH1ccmlnaHQpfXtcdGltZXNcbGVmdCgxLVxmcmFje1xWYWx7dmFsQWJzVGF1eH19ezEwMH1ccmlnaHQpfQAAAA7##########w=='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true);
  }

  function modifFig (num) {
    // cette fonction sert à modifier la figure de base
    // num est le numéro de la question (1 ou 3) selon qu’on cherche la valeur finale ou la valeur initiale
    let VD = 0
    let VA = 0
    let taux
    if (num === 1) {
      VD = RemplaceCalcul({
        text: stor.valeurs['enonce' + stor.sujetChoisi][0],
        obj: stor.obj
      })
      taux = RemplaceCalcul({
        text: stor.valeurs['enonce' + stor.sujetChoisi][1],
        obj: stor.obj
      })
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posVA', '10')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posVD', '-1')
    } else {
      VA = RemplaceCalcul({
        text: stor.valeurs['enonce' + stor.sujetChoisi][0],
        obj: stor.obj
      })
      taux = RemplaceCalcul({
        text: stor.valeurs['enonce' + stor.sujetChoisi][2],
        obj: stor.obj
      })
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posVA', '-1')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posVD', '10')
    }
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'taux', String(taux))
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'VD', String(VD))
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'VA', String(VA))
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let i
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    j3pDetruitToutesLesFenetres()
    me.zoneexplication1 = j3pAddElt(explications, 'div', '')
    switch (stor.numQuest) {
      case 1:
        j3pAffiche(me.zoneexplication1, '', textes.corr2, stor.obj)
        for (i = 1; i <= stor.correctionVA['enonce' + stor.sujetChoisi].length; i++) {
          stor['zoneexplication' + (i + 1)] = j3pAddElt(explications, 'div', '')
          j3pAffiche(stor['zoneexplication' + (i + 1)], '', stor.correctionVA['enonce' + stor.sujetChoisi][i - 1], stor.obj)
        }
        break
      case 2:
        j3pAffiche(me.zoneexplication1, '', textes.corr1, stor.obj)
        if (stor.calculVA) {
          for (i = 1; i <= stor.correctionTaux2['enonce' + stor.sujetChoisi].length; i++) {
            stor['zoneexplication' + (i + 1)] = j3pAddElt(explications, 'div', '')
            j3pAffiche(stor['zoneexplication' + (i + 1)], '', stor.correctionTaux2['enonce' + stor.sujetChoisi][i - 1], stor.obj)
          }
        } else {
          for (i = 1; i <= stor.correctionTaux1['enonce' + stor.sujetChoisi].length; i++) {
            stor['zoneexplication' + (i + 1)] = j3pAddElt(explications, 'div', '')
            j3pAffiche(stor['zoneexplication' + (i + 1)], '', stor.correctionTaux1['enonce' + stor.sujetChoisi][i - 1], stor.obj)
          }
        }
        break
      default :
        j3pAffiche(me.zoneexplication1, '', textes.corr2, stor.obj)
        for (i = 1; i <= stor.correctionVD['enonce' + stor.sujetChoisi].length; i++) {
          stor['zoneexplication' + (i + 1)] = j3pAddElt(explications, 'div', '')
          j3pAffiche(stor['zoneexplication' + (i + 1)], '', stor.correctionVD['enonce' + stor.sujetChoisi][i - 1], stor.obj)
        }
        break
    }
    if (stor.numQuest !== 2) {
      /// /////////////////////////////////////// Pour MathGraph32
      const largFig = 410// on gère ici la largeur de la figure
      const hautFig = 220
      // création du div accueillant la figure mtg32
      stor.divMtg32 = j3pAddElt(stor.zoneCorr, 'div', '', { style: me.styles.petit.enonce })
      // et enfin la zone svg (très importante car tout est dedans)
      stor.mtg32svg = j3pGetNewId('mtg32svg')
      j3pCreeSVG(stor.divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
      // ce qui suit lance la création initiale de la figure
      depart()
      modifFig(stor.numQuest)
    }
  }

  function RemplaceCalcul (obj) {
    // obj contient trois propriétés (la dernière étant optionnelle):
    // -obj.text qui est le texte correspondant à la formule de calcul
    // -obj.obj qui est la liste des valeurs à remplacer et leurs valeurs respectives
    // Par exemple : RemplaceCalcul({text:'2*£a+£b', obj:{a:'2',b:'-3'}})
    // le résultat renvoyé est la valeur du calcul (format numérique)
    if (obj.calcul === undefined) {
      obj.calcul = true
    }
    let newTxt = obj.text
    let pos1
    while (newTxt.indexOf('£') !== -1) {
      // on remplace £x par sa valeur
      pos1 = newTxt.indexOf('£')
      const laVariable = newTxt.charAt(pos1 + 1)
      const nombre = obj.obj[laVariable]
      newTxt = newTxt.substring(0, pos1) + '(' + nombre + ')' + newTxt.substring(pos1 + 2)
    }
    if (newTxt.indexOf('frac') > -1) {
      const numden = j3pExtraireNumDen(newTxt)
      return '\\frac{' + Math.round(Math.pow(10, 10) * j3pCalculValeur(numden[1])) / Math.pow(10, 10) + '}{' + Math.round(Math.pow(10, 10) * j3pCalculValeur(numden[2])) / Math.pow(10, 10) + '}'
    } else {
      return Math.round(Math.pow(10, 10) * j3pCalculValeur(newTxt)) / Math.pow(10, 10)
    }
  }

  function suite () {
    // OBLIGATOIRE
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    let i
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    let numConsigne = 0
    let larrondi, larrondiVA, resultatExact, resultatExact2
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      // on choisit un nouveau sujet
      // on choisit à chaque repetition un sujet (parmi ceux qui ne l’ont pas encore été (ou parmi tous les sujets s’ils ont tous été traités)
      // on peut forcer le choix du sujet (paramètre non renseigné car il sert juste pour effectuer les tests)
      if (ds.numSujet === undefined || isNaN(ds.numSujet)) ds.numSujet = -1
      if (stor.listeSujets.length === 0) {
        stor.listeSujets = [...stor.listeSujetsInit]
      }
      me.logIfDebug('stor.listeSujets:', stor.listeSujets)
      if (ds.numSujet > -1) {
        stor.sujetChoisi = ds.numSujet
      } else {
        const choix = j3pGetRandomInt(0, (stor.listeSujets.length - 1))
        stor.sujetChoisi = stor.listeSujets[choix]
        stor.listeSujets.splice(choix, 1)
      }
      // console.log('listeSujets:', stor.listeSujets, 'et sujetChoisi:', stor.sujetChoisi)
      // gestion des variables dans cet énoncé :
      stor.nomVariables = []
      stor.variables = []
      stor.obj = {}
      for (i = 0; i < stor.mesVariables['enonce' + stor.sujetChoisi].length; i++) {
        stor.nomVariables[i] = stor.mesVariables['enonce' + stor.sujetChoisi][i][0]
        const [leMin, leMax] = j3pGetBornesIntervalle(stor.mesVariables['enonce' + stor.sujetChoisi][i][1])
        stor.variables[i] = (stor.mesVariables['enonce' + stor.sujetChoisi][i].length === 2) ? j3pGetRandomInt(leMin, leMax) : j3pGetRandomInt(leMin, leMax) / Number(stor.mesVariables['enonce' + stor.sujetChoisi][i][2])
      }
      for (i = 0; i < stor.nomVariables.length; i++) {
        stor.obj[stor.nomVariables[i]] = stor.variables[i]
      }

      // calcul des réponse
      const calcVA = RemplaceCalcul({
        text: stor.reponses['enonce' + stor.sujetChoisi].va,
        obj: stor.obj
      })
      const calcTaux1 = 100 * RemplaceCalcul({
        text: stor.reponses['enonce' + stor.sujetChoisi].tx1,
        obj: stor.obj
      })
      // f sera le résultat de calcVA*(1+£b/100)
      larrondiVA = stor.arrondis['enonce' + stor.sujetChoisi][0]
      stor.obj.f = Math.round(calcVA / larrondiVA) * larrondiVA
      if (String(stor.obj.f).indexOf('.') > -1) {
        stor.obj.f = Math.round(stor.obj.f * Math.pow(10, 5)) / Math.pow(10, 5)
      }

      const calcTaux2 = 100 * RemplaceCalcul({
        text: stor.reponses['enonce' + stor.sujetChoisi].tx2,
        obj: stor.obj
      })
      const calcVD = RemplaceCalcul({
        text: stor.reponses['enonce' + stor.sujetChoisi].vd,
        obj: stor.obj
      })
      stor.rep = { calcVA, calcTaux1, calcTaux2, calcVD }
      stor.calculVA = false
      me.logIfDebug('calcVA:', calcVA, '    calcTaux1:', calcTaux1, '    calcTaux2:', calcTaux2, '   calcVD:', calcVD)
    }

    stor.obj.styletexte = {}
    const numQuest = stor.typeQuestions[(me.questionCourante - 1) % ds.nbetapes]

    switch (numQuest) {
      case 1:
        // calcul de la valeur d’arrivée
        larrondi = stor.arrondis['enonce' + stor.sujetChoisi][0]
        resultatExact = (Math.ceil(stor.rep.calcVA / larrondi) * larrondi - stor.rep.calcVA < Math.pow(10, -10))
        resultatExact2 = (Math.ceil(stor.rep.calcVA / (larrondi / 100)) * (larrondi / 100) - stor.rep.calcVA < Math.pow(10, -10))
        stor.obj.e = resultatExact ? '' : textes.txt1
        stor.obj.s = resultatExact2 ? '=' : '\\approx'
        stor.calculVA = true
        // la réponse à l’arrondi demandé (utile pour la correction):
        stor.obj.r = Math.round(stor.rep.calcVA / larrondi) * larrondi
        stor.obj.r1 = Math.round(100 * stor.rep.calcVA * Math.round(1 / larrondi)) * larrondi / 100
        stor.obj.r1 = Math.round(stor.obj.r1 * Math.pow(10, 6)) / Math.pow(10, 6)
        break
      case 2:
        // calcul d’un taux (différent suivant qu’on soit passé par le calcul de la valeur d’arrivée avant ou non)
        larrondiVA = stor.arrondis['enonce' + stor.sujetChoisi][0]
        if (stor.calculVA) {
          stor.obj.f = (larrondiVA > 1)
            ? Math.round(stor.rep.calcVA / larrondiVA) * larrondiVA
            : Math.round(stor.rep.calcVA * Math.round(1 / larrondiVA)) * larrondiVA
          if (String(stor.obj.f).indexOf('.') > -1) {
            stor.obj.f = Math.round(stor.obj.f * Math.pow(10, 5)) / Math.pow(10, 5)
          }
        }
        larrondi = stor.arrondis['enonce' + stor.sujetChoisi][1]
        {
          const valTaux = (stor.calculVA) ? stor.rep.calcTaux2 : stor.rep.calcTaux1
          resultatExact = (Math.ceil(valTaux / larrondi) * larrondi - valTaux < Math.pow(10, -10))
          stor.obj.e = resultatExact ? textes.txt3 : textes.txt2
          stor.obj.e1 = resultatExact ? '' : textes.txt1
          stor.obj.s = resultatExact ? '=' : '\\approx'
          // la réponse à l’arrondi demandé (utile pour la correction):
          stor.obj.r = Math.round(valTaux * Math.round(1 / larrondi)) * larrondi
          stor.obj.t = Math.round(valTaux * Math.pow(10, 3)) / Math.pow(10, 5)
        }
        break
      default :
        // calcul de la valeur de départ
        larrondi = stor.arrondis['enonce' + stor.sujetChoisi][2]
        resultatExact = (Math.ceil(stor.rep.calcVD / larrondi) * larrondi - stor.rep.calcVD < Math.pow(10, -10))
        resultatExact2 = (Math.ceil(stor.rep.calcVD / (larrondi / 100)) * (larrondi / 100) - stor.rep.calcVD < Math.pow(10, -10))
        stor.obj.e = resultatExact ? '' : textes.txt1
        stor.obj.s = resultatExact2 ? '=' : '\\approx'
        stor.obj.r = (larrondi > 1)
          ? Math.round(stor.rep.calcVD / larrondi) * larrondi
          : Math.round(stor.rep.calcVD * Math.round(1 / larrondi)) / Math.round(1 / larrondi)
        stor.obj.r1 = Math.round(100 * stor.rep.calcVD / larrondi) * larrondi / 100
        stor.obj.r1 = Math.round(stor.obj.r1 * Math.pow(10, 6)) / Math.pow(10, 6)
        break
    }
    stor.obj.r = Math.round(stor.obj.r * Math.pow(10, 6)) / Math.pow(10, 6)
    if (numQuest !== 2) {
      stor.obj.r3 = stor.obj.r
      if (!resultatExact) {
        stor.obj.r3 = textes.corr3.replace('£{r1}', stor.obj.r1)
        stor.obj.r3 = stor.obj.r3.replace('£e', stor.obj.e)
        stor.obj.r3 = stor.obj.r3.replace('£r', stor.obj.r)
      }
    }
    // attention car obj.r peut être mal arrondi à cause de la variable larrondi qui peut être un décimal
    stor.obj.r = Math.round(stor.obj.r * Math.pow(10, 6)) / Math.pow(10, 6)
    stor.obj.r1 = Math.round(stor.obj.r1 * Math.pow(10, 6)) / Math.pow(10, 6)
    for (i = 0; i < stor.consignes['enonce' + stor.sujetChoisi].length; i++) {
      stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
      j3pAffiche(stor['zoneCons' + i], '', stor.consignes['enonce' + stor.sujetChoisi][i],
        stor.obj)
      numConsigne++
    }

    // et pour la question
    let nbElementsQuest
    switch (numQuest) {
      case 1:
        {
          nbElementsQuest = stor.questionVA['enonce' + stor.sujetChoisi].length
          for (i = 0; i < nbElementsQuest - 1; i++) {
            stor['zoneCons' + numConsigne] = j3pAddElt(stor.conteneur, 'div', '')
            j3pAffiche(stor['zoneCons' + numConsigne], '', stor.questionVA['enonce' + stor.sujetChoisi][i],
              stor.obj)
            numConsigne++
          }
          // et maintenant la dernière correspondant à la zone de saisie
          stor.obj.inputmq1 = { texte: '' }
          stor['zoneCons' + numConsigne] = j3pAddElt(stor.conteneur, 'div', '')
          const elt = j3pAffiche(stor['zoneCons' + numConsigne], '', stor.questionVA['enonce' + stor.sujetChoisi][nbElementsQuest - 1],
            stor.obj)
          stor.zoneInput = elt.inputmqList[0]
          stor.zoneInput.reponse = [stor.rep.calcVA]
        }
        break
      case 2:
        nbElementsQuest = (stor.calculVA) ? stor.questionTaux2['enonce' + stor.sujetChoisi].length : stor.questionTaux1['enonce' + stor.sujetChoisi].length
        {
          let laquest
          for (i = 0; i < nbElementsQuest - 1; i++) {
            stor['zoneCons' + numConsigne] = j3pAddElt(stor.conteneur, 'div', '')
            laquest = (stor.calculVA) ? stor.questionTaux2['enonce' + stor.sujetChoisi][i] : stor.questionTaux1['enonce' + stor.sujetChoisi][i]
            j3pAffiche(stor['zoneCons' + numConsigne], '', laquest,
              stor.obj)
            numConsigne++
          }
          // et maintenant la dernière correspondant à la zone de saisie
          stor.obj.inputmq1 = { texte: '' }
          const corrListe = (stor.calculVA) ? (stor.rep.calcTaux2 > 0) ? 1 : 2 : (stor.rep.calcTaux1 > 0) ? 1 : 2
          stor.obj.liste1 = {
            texte: ['', textes.liste1, textes.liste2],
            correction: corrListe,
            taillepolice: me.styles.petit.enonce.fontSize,
            couleur: me.styles.petit.enonce.color
          }
          stor['zoneCons' + numConsigne] = j3pAddElt(stor.conteneur, 'div', '')
          laquest = (stor.calculVA) ? stor.questionTaux2['enonce' + stor.sujetChoisi][nbElementsQuest - 1] : stor.questionTaux1['enonce' + stor.sujetChoisi][nbElementsQuest - 1]
          const elt = j3pAffiche(stor['zoneCons' + numConsigne], '', laquest,
            stor.obj)
          stor.zoneInput = elt.inputmqList[0]
          stor.laListe = elt.selectList[0]
          stor.zoneInput.reponse = (stor.calculVA) ? [Math.abs(stor.rep.calcTaux2)] : [Math.abs(stor.rep.calcTaux1)]
          stor.laListe.reponse = [corrListe]
          stor.laListe.typeReponse = [true]
        }
        break
      default :
        {
          nbElementsQuest = stor.questionVD['enonce' + stor.sujetChoisi].length
          for (i = 0; i < nbElementsQuest - 1; i++) {
            stor['zoneCons' + numConsigne] = j3pAddElt(stor.conteneur, 'div', '')
            j3pAffiche(stor['zoneCons' + numConsigne], '', stor.questionVD['enonce' + stor.sujetChoisi][i],
              stor.obj)
            numConsigne++
          }
          // et maintenant la dernière correspondant à la zone de saisie
          stor.obj.inputmq1 = { texte: '' }
          stor['zoneCons' + numConsigne] = j3pAddElt(stor.conteneur, 'div', '')
          const elt = j3pAffiche(stor['zoneCons' + numConsigne], '', stor.questionVD['enonce' + stor.sujetChoisi][nbElementsQuest - 1],
            stor.obj)
          stor.zoneInput = elt.inputmqList[0]
          stor.zoneInput.reponse = [stor.rep.calcVD]
        }
        break
    }
    if (!resultatExact) {
      stor['zoneCons' + (numConsigne + 1)] = j3pAddElt(stor.conteneur, 'div', '')
      j3pAffiche(stor['zoneCons' + (numConsigne + 1)], '', stor.arrondisTxt['enonce' + stor.sujetChoisi][numQuest - 1],
        stor.obj)
    }
    mqRestriction(stor.zoneInput, '\\d.,', {
      boundingContainer: me.zonesElts.MG
    })

    me.logIfDebug('resultatExact:', resultatExact, larrondi)

    stor.zoneInput.typeReponse = (resultatExact) ? ['nombre', 'exact'] : ['nombre', 'arrondi', larrondi]
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = (numQuest === 2) ? [stor.laListe.id, stor.zoneInput.id] : [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    stor.numQuest = numQuest
    stor.numConsigne = numConsigne
    stor.larrondi = larrondi
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvent dans le cadre MD)
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.zoneD, 'div', '')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]//, left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '')
    j3pFocus(stor.zoneInput)
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.6 })
        try {
          const tabInterm = ((typeof ds.typeQuestions === 'string') || (typeof ds.typeQuestions === 'number')) ? [1, 2, 3] : ds.typeQuestions
          if (tabInterm.length > 3) {
            tabInterm.splice(3)
          }
          stor.typeQuestions = []
          // Enfin je m’assure que les éléments saisis sont bien entre 1 et 3
          for (let i = 0; i < tabInterm.length; i++) {
            if ((Number(tabInterm[i]) >= 1) && (Number(tabInterm[i]) <= 3) && (stor.typeQuestions.indexOf(Number(tabInterm[i])) === -1)) {
              stor.typeQuestions.push(Number(tabInterm[i]))
            }
          }
          stor.typeQuestions.sort()
        } catch (e) {
          stor.typeQuestions = [1, 2, 3]
        }
        ds.nbetapes = stor.typeQuestions.length
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        const titreExo = (stor.typeQuestions.length === 1)
          ? (stor.typeQuestions[0] === 2) ? textes.titre_exo1 : textes.titre_exo3
          : ((stor.typeQuestions.length === 2) && (stor.typeQuestions.indexOf(2) === -1))
              ? textes.titre_exo2
              : textes.titre_exo4
        me.afficheTitre(titreExo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        // On initialise le nombre d’erreur à chaque type de question à 0
        stor.erreurVA = stor.erreurTaux = stor.erreurVD = stor.erreurArrondi = 0
        stor.listeSujets = []
        stor.listeSujetsInit = []
        import('../../../sectionsAnnexes/Consignesexos/tauxEvolution.js').then(function parseAnnexe ({ default: datasSection }) {
          let j
          stor.nb_sujets = datasSection.sujets.length
          stor.consignes = {}
          // stor.nomVariables = {}
          stor.mesVariables = {}
          stor.questionVA = {}
          stor.questionTaux1 = {}
          stor.questionTaux2 = {}
          stor.questionVD = {}
          stor.reponses = {}
          stor.arrondisTxt = {}
          stor.arrondis = {}
          stor.correctionVA = {}
          stor.correctionTaux1 = {}
          stor.correctionTaux2 = {}
          stor.correctionVD = {}
          stor.valeurs = {}
          for (j = 0; j < stor.nb_sujets; j++) {
            stor.consignes['enonce' + j] = datasSection.sujets[j].consignes
            stor.mesVariables['enonce' + j] = datasSection.sujets[j].variables
            stor.questionVA['enonce' + j] = datasSection.sujets[j].questionVA
            stor.questionTaux1['enonce' + j] = datasSection.sujets[j].questionTaux1
            stor.questionTaux2['enonce' + j] = datasSection.sujets[j].questionTaux2
            stor.questionVD['enonce' + j] = datasSection.sujets[j].questionVD
            stor.arrondis['enonce' + j] = datasSection.sujets[j].arrondis
            stor.arrondisTxt['enonce' + j] = datasSection.sujets[j].arrondisTxt
            stor.reponses['enonce' + j] = datasSection.sujets[j].reponses
            stor.correctionVA['enonce' + j] = datasSection.sujets[j].correctionVA
            stor.correctionTaux1['enonce' + j] = datasSection.sujets[j].correctionTaux1
            stor.correctionTaux2['enonce' + j] = datasSection.sujets[j].correctionTaux2
            stor.correctionVD['enonce' + j] = datasSection.sujets[j].correctionVD
            stor.valeurs['enonce' + j] = datasSection.sujets[j].valeurs
            stor.listeSujets.push(j)
            stor.listeSujetsInit.push(j)
          }
          // chargement mtg
          getMtgCore({ withMathJax: true })
            .then(
              // success
              (mtgAppLecteur) => {
                stor.mtgAppLecteur = mtgAppLecteur
                suite()
              },
              // failure
              (error) => {
                j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
              })
            // plantage dans le code de success
            .catch(j3pShowError)
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite()
      }

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        let pbArrondi = false

        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si la zone a bien été complétée
        // reponse.bonneReponse qui teste si l’intervalle donné est le bon
        // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
        // Si c’est la dernière tentative de répondre, on barre également les zones fausses
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
          if (reponse.bonneReponse) {
          // Bonne réponse
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // on regarde s’il n’y a pas un problème d’arrondi
            if (stor.zoneInput.typeReponse[1] === 'arrondi') {
            // On vérifie si c’est une valeur approchée
              let reponseSaisie = (stor.numQuest === 2) ? stor.fctsValid.zones.reponseSaisie[1] : stor.fctsValid.zones.reponseSaisie[0]
              reponseSaisie = j3pNombre(reponseSaisie)
              pbArrondi = (Math.abs(stor.zoneInput.reponse[0] - reponseSaisie) < stor.larrondi * 10)
            }
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (pbArrondi) {
                stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
              }
              if (stor.numQuest === 2) {
              // gestion de la phrase où on a des variables
                const phraseComment = j3pChaine(textes.comment2, {
                  a: j3pVirgule(stor.larrondi),
                  b: j3pVirgule(Math.round(stor.larrondi * Math.pow(10, 3)) / Math.pow(10, 5))
                })
                stor.zoneCorr.innerHTML += '<br/>' + phraseComment
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                // On relève alors les 3 types de fautes commise
                if (stor.numQuest === 1) {
                  stor.erreurVA++
                } else if (stor.numQuest === 2) {
                  stor.erreurTaux++
                } else {
                  stor.erreurVD++
                }
                if (pbArrondi) {
                  stor.erreurArrondi++
                }
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const tauxEchecVA = stor.erreurVA / ds.nbrepetitions
        const tauxEchecTaux = stor.erreurTaux / ds.nbrepetitions
        const tauxEchecVD = stor.erreurVD / ds.nbrepetitions
        const tauxEchecArrondi = stor.erreurArrondi / ds.nbitems

        me.logIfDebug('   VA:', tauxEchecVA, '  Taux:', tauxEchecTaux, '   VD:', tauxEchecVD, '  Arrondi:', tauxEchecArrondi)
        if (tauxEchecArrondi > ds.seuilErreur) {
          me.parcours.pe = ds.pe_8
        } else {
          if (tauxEchecVA > ds.seuilErreur) {
            if (tauxEchecTaux > ds.seuilErreur) {
              me.parcours.pe = (tauxEchecVD >= (1 - ds.seuilErreur)) ? ds.pe_9 : ds.pe_5
            } else {
              me.parcours.pe = (tauxEchecVD >= (1 - ds.seuilErreur)) ? ds.pe_7 : ds.pe_3
            }
          } else {
            if (tauxEchecTaux > ds.seuilErreur) {
              me.parcours.pe = (tauxEchecVD >= (1 - ds.seuilErreur)) ? ds.pe_6 : ds.pe_2
            } else {
              if (tauxEchecVD >= (1 - ds.seuilErreur)) {
                me.parcours.pe = ds.pe_4
              } else {
                me.parcours.pe = (me.score / ds.nbitems >= ds.seuilReussite) ? ds.pe_1 : ds.pe_9
              }
            }
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)
      break // case "navigation":
  }
}
