import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pNombre, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pStyle, j3pVirgule, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
   Exemple de Graphe de test
   j3p.html?graphe=[1,"tauxReciproque",[{pe:">=0",nn:"2",conclusion:"Etape 2"}]];[2,"tauxReciproque",[{pe:">=0",nn:"fin",conclusion:"fin"}]];
 */
/*
        Rémi DENIAUD
        octobre 2018
        Déterminer dans différents cas de figure un taux d’évolution réciproque
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Taux d’évolution réciproque',
  // on donne les phrases de la consigne
  consigne1_1: 'Le prix du gasoil a augmenté de £a % au cours des six premiers mois de l’année.',
  consigne2_1: 'Pour revenir à sa valeur initiale six mois plus tard, le prix du gasoil devra #1# £{env} &1& %.',
  consigne1_2: 'La consommation de cigarette chez les moins de 18 ans a augmenté de £a % au cours des 5 dernières années.',
  consigne2_2: 'Pour revenir à sa valeur initiale, la consommation de cigarette devra #1# £{env} &1& %.',
  consigne1_3: 'Le nombre de participants à un festival a diminué de £a % au cours cette année.',
  consigne2_3: 'Pour revenir à sa valeur de l’année précédente, ce nombre devra #1# £{env} &1& % l’année suivante.',
  consigne1_4: 'Le chiffre d’affaires d’une entreprise a baissé de £a % au cours de l’année.',
  consigne2_4: 'Pour revenir à sa valeur initiale dans un an, le chiffre d’affaires devra #1# £{env} &1& %.',
  consigne1_5: 'Le nombre d’élèves d’un lycée a diminué de £a % cette année.',
  consigne2_5: 'Pour revenir à sa valeur initiale l’année suivante, le nombre d’élèves devra #1# £{env} &1& %.',
  consigne3: 'Arrondir à 0,01 %.',
  choix1: 'augmenter',
  choix2: 'diminuer',
  txt1: 'd’environ',
  txt2: 'de',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'N’y aurait-il pas un problème d’arrondi ?',
  comment2: 'On rappelle par ailleurs que pour arrondir le taux à 0,01 %, il faut arrondir la valeur décimale à 0,0001.',
  comment3: 'L’opposé du taux donné dans l’énoncé n’est certainement pas la bonne réponse !',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'La situation peut être résumée par le schéma ci-dessous :',
  corr2: 'On chercher alors $t$ tel que $(1£{t1})(1+t)=1$.',
  corr3: 'On obtient $1+t=\\frac{1}{1£{t1}}£e£{coef}$ et ainsi $t£e£{coef}-1=£q$ soit £p %.',
  corr4_1: 'Donc le prix du gasoil devra diminuer £f £r % dans les six mois à venir pour retrouver son prix initial.',
  corr4_2: 'Donc la consommation de cigarettes devra diminuer £f £r % au cours des cinq prochaines années pour retrouver sa valeur initiale.',
  corr4_3: 'Donc le nombre de participants devra augmenter £f £r % l’année suivante pour retrouver sa valeur initiale.',
  corr4_4: 'Donc le chiffre d’affaires de l’entreprise devra augmenter £f £r % dans un an pour retrouver son niveau de l’année passée.',
  corr4_5: 'Donc le nombre d’élèves du lycée devra augmenter £f £r % l’année suivante pour retrouver sa valeur initiale.',
  corr5: 'Avoir proposé une augmentation de £a % était une erreur car<br/>$(1£{t1})\\times (1£{t2})=£v\\neq 1$.'
}
/**
 * section tauxReciproque
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const laRep = stor.laReponse
    const laRepAffichee = Math.abs(Math.round(laRep * 100) / 100)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    for (let i = 1; i <= 6; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', textes.corr1)
    /// /////////////////////////////////////// Pour MathGraph32
    const largFig = 480
    const hautFig = 220
    // création du div accueillant la figure mtg32
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(stor.zoneExpli2, { id: stor.mtg32svg, width: largFig, height: hautFig })
    j3pStyle(stor.zoneExpli2, { textAlign: 'center' })
    // ce qui suit lance la création initiale de la figure
    depart()
    modifFig()
    stor.obj.t1 = (stor.taux > 0) ? '+\\frac{' + j3pVirgule(Math.abs(stor.taux)) + '}{100}' : '-\\frac{' + j3pVirgule(Math.abs(stor.taux)) + '}{100}'
    stor.obj.t2 = (stor.taux < 0) ? '+\\frac{' + j3pVirgule(Math.abs(stor.taux)) + '}{100}' : '-\\frac{' + j3pVirgule(Math.abs(stor.taux)) + '}{100}'
    stor.obj.coef = (stor.zoneInputMq.typeReponse[1] === 'exact') ? Math.round(Math.pow(10, 7) * (1 + Math.round(laRep * 1000) / 100000)) / Math.pow(10, 7) : Math.round(Math.pow(10, 7) * (1 + Math.round(laRep * 100) / 10000)) / Math.pow(10, 7)
    stor.obj.e = stor.repExacte ? '=' : '\\approx'
    stor.obj.q = (stor.zoneInputMq.typeReponse[1] === 'exact') ? Math.round(laRep * 10000) / 1000000 : Math.round(laRep * 100) / 10000
    stor.obj.p = (stor.zoneInputMq.typeReponse[1] === 'exact') ? j3pVirgule(Math.round(laRep * 10000) / 10000) : j3pVirgule(Math.round(laRep * 100) / 100)
    stor.obj.f = stor.repExacte ? textes.txt2 : textes.txt1
    stor.obj.r = j3pVirgule(Math.abs(laRepAffichee))
    stor.obj.v = j3pVirgule(j3pArrondi((1 + stor.taux / 100) * (1 - stor.taux / 100), 6))
    for (let i = 3; i <= 4; i++) j3pAffiche(stor['zoneExpli' + i], '', textes['corr' + (i - 1)], stor.obj)
    j3pAffiche(stor.zoneExpli5, '', textes['corr4_' + stor.choixSujet], stor.obj)
    if (stor.opposeTaux) {
      j3pAffiche(stor.zoneExpli6, '', textes.corr5, stor.obj)
    }
  }

  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV7AAADnAAAAQEAAAAAAAAAAQAAAGH#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAFAAFAYtPXCj1wpEBTwUeuFHri#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAAQ4AAUkAwBgAAAAAAAAAAAAAAAAAAAUAAUBEsKPXCj1wAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAQAAAAP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAABAAAABP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQUAAQAAAAcAAAAJAP####8BAAAAAQ4AAUoAwCgAAAAAAADAEAAAAAAAAAUAAgAAAAf#####AAAAAgAHQ1JlcGVyZQD#####Aebm5gABAAAAAQAAAAMAAAAJAQEAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgDALAAAAAAAAEAAAAAAAAAAAQAAAAADAAAADP####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAAN#####wAAAAEAB0NDYWxjdWwA#####wAHbmJncmFkeAACMjAAAAABQDQAAAAAAAAAAAARAP####8AB25iZ3JhZHkAAjIwAAAAAUA0AAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AFEdyYWR1YXRpb25BeGVzUmVwZXJlAAAAGwAAAAgAAAADAAAACgAAAA8AAAAQ#####wAAAAEAE0NBYnNjaXNzZU9yaWdpbmVSZXAAAAAAEQAFYWJzb3IAAAAK#####wAAAAEAE0NPcmRvbm5lZU9yaWdpbmVSZXAAAAAAEQAFb3Jkb3IAAAAKAAAACwAAAAARAAZ1bml0ZXgAAAAK#####wAAAAEACkNVbml0ZXlSZXAAAAAAEQAGdW5pdGV5AAAACv####8AAAABABBDUG9pbnREYW5zUmVwZXJlAAAAABEAAAAAABAAAAEFAAAAAAoAAAAOAAAAEgAAAA4AAAATAAAAFgAAAAARAAAAAAAQAAABBQAAAAAKAAAADQAAAAAOAAAAEgAAAA4AAAAUAAAADgAAABMAAAAWAAAAABEAAAAAABAAAAEFAAAAAAoAAAAOAAAAEgAAAA0AAAAADgAAABMAAAAOAAAAFQAAAAwAAAAAEQAAABYAAAAOAAAADwAAAA8AAAAAEQAAAAAAEAAAAQUAAAAAFwAAABkAAAAMAAAAABEAAAAWAAAADgAAABAAAAAPAAAAABEAAAAAABAAAAEFAAAAABgAAAAb#####wAAAAEACENTZWdtZW50AAAAABEBAAAAABAAAAEAAQAAABcAAAAaAAAAFwAAAAARAQAAAAAQAAABAAEAAAAYAAAAHAAAAAQAAAAAEQEAAAAACwABVwDAFAAAAAAAAMA0AAAAAAAABQABP9xWeJq83w4AAAAd#####wAAAAIACENNZXN1cmVYAAAAABEABnhDb29yZAAAAAoAAAAfAAAAEQAAAAARAAVhYnN3MQAGeENvb3JkAAAADgAAACD#####AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAABEBZmZmAAAAHwAAAA4AAAAPAAAAHwAAAAIAAAAfAAAAHwAAABEAAAAAEQAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABIAAAAOAAAAIQAAABYAAAAAEQEAAAAAEAAAAQUAAAAACgAAAA4AAAAjAAAADgAAABMAAAAZAQAAABEBZmZmAAAAJAAAAA4AAAAPAAAAHwAAAAUAAAAfAAAAIAAAACEAAAAjAAAAJAAAAAQAAAAAEQEAAAAACwABUgBAIAAAAAAAAMAgAAAAAAAABQABP9EbToG06B8AAAAe#####wAAAAIACENNZXN1cmVZAAAAABEABnlDb29yZAAAAAoAAAAmAAAAEQAAAAARAAVvcmRyMQAGeUNvb3JkAAAADgAAACcAAAAZAQAAABEBZmZmAAAAJgAAAA4AAAAQAAAAJgAAAAIAAAAmAAAAJgAAABEAAAAAEQAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABMAAAAOAAAAKAAAABYAAAAAEQEAAAAAEAAAAQUAAAAACgAAAA4AAAASAAAADgAAACoAAAAZAQAAABEBZmZmAAAAKwAAAA4AAAAQAAAAJgAAAAUAAAAmAAAAJwAAACgAAAAqAAAAK#####8AAAACAAxDQ29tbWVudGFpcmUAAAAAEQFmZmYAP#AAAAAAAABAHAAAAAAAAAAAAB8LAAH###8AAAABAAAAAAALI1ZhbChhYnN3MSkAAAAZAQAAABEBZmZmAAAALQAAAA4AAAAPAAAAHwAAAAQAAAAfAAAAIAAAACEAAAAtAAAAGwAAAAARAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAJAsAAf###wAAAAEAAAAAAAsjVmFsKGFic3cyKQAAABkBAAAAEQFmZmYAAAAvAAAADgAAAA8AAAAfAAAABgAAAB8AAAAgAAAAIQAAACMAAAAkAAAALwAAABsAAAAAEQFmZmYAwCAAAAAAAAA#8AAAAAAAAAAAACYLAAH###8AAAACAAAAAQALI1ZhbChvcmRyMSkAAAAZAQAAABEBZmZmAAAAMQAAAA4AAAAQAAAAJgAAAAQAAAAmAAAAJwAAACgAAAAxAAAAGwAAAAARAWZmZgDAHAAAAAAAAAAAAAAAAAAAAAAAKwsAAf###wAAAAIAAAABAAsjVmFsKG9yZHIyKQAAABkBAAAAEQFmZmYAAAAzAAAADgAAABAAAAAmAAAABgAAACYAAAAnAAAAKAAAACoAAAArAAAAMwAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAr#####AAAAAQAMQ01vaW5zVW5haXJlAAAAAUAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAQA#####wEAAAAAEAAAAQUAAUAMzWTXStuDAAAABQAAAAcA#####wEAAAAAAQAAADcAAAA1#####wAAAAIAHENNYXJxdWVBbmdsZU9yaWVudGVJbmRpcmVjdGUA#####wAAAAAAAQAAAAFAZUPUZDpD8wAAADUAAAA3AAAANgAAAAAbAP####8AAAAAAAAAAAAAAAAAQBQAAAAAAAAAAAA1EAAAAAAAAQAAAAAAEVZhbGV1cgpkZSBkw6lwYXJ0AAAACAD#####AAAABQAAADgAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAOwAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAAA7AAAAEQD#####AAVwb3NWQQACLTEAAAAcAAAAAT#wAAAAAAAAAAAAEQD#####AAVwb3NWRAACLTEAAAAcAAAAAT#wAAAAAAAAAAAAEQD#####AAJWQQAEMTIwMAAAAAFAksAAAAAAAAAAABEA#####wACVkQABDEwMDAAAAABQI9AAAAAAAAAAAARAP####8AC3Bvc1RhdXhQbHVzAAExAAAAAT#wAAAAAAAAAAAAEQD#####AAxwb3NUYXV4TW9pbnMAATIAAAABQAAAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAQAAAAAAAAAAAAAADgAAAEIAAAARAP####8ABHRhdXgAAy0yNQAAABwAAAABQDkAAAAAAAAAAAARAP####8ABHRlc3QABnRhdXg+MAAAAA0FAAAADgAAAEUAAAABAAAAAAAAAAAAAAARAP####8ACnZhbEFic1RhdXgACWFicyh0YXV4Kf####8AAAACAAlDRm9uY3Rpb24AAAAADgAAAEX#####AAAAAgAGQ0xhdGV4AP####8AAAAAAQAAAEQQAAAAAAABAAAAAQBvXElme3Rlc3R9e1x0aW1lc1xsZWZ0KDErXGZyYWN7XFZhbHt2YWxBYnNUYXV4fX17MTAwfVxyaWdodCl9e1x0aW1lc1xsZWZ0KDEtXGZyYWN7XFZhbHt2YWxBYnNUYXV4fX17MTAwfVxyaWdodCl9#####wAAAAIADENEcm9pdGVQYXJFcQD#####AQAAAAAQAAABAAEAAAAKAAAADQj#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAAAUASAAAAAAAAAAAADQEAAAAhAAAAAAAAAAFAEgAAAAAAAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQAQAAAAAAAAAAAABAAAAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAUAaAAAAAAAAAAAAAQAAAAAAAAAAAAAABgD#####AQAAAAAQAAABAAEAAAA3AAAABf####8AAAACABFDTWFycXVlQW5nbGVEcm9pdAD#####AQAAAAABAAAAAUAiAAAAAAAAAAAATP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAABJAAAATAAAAAcA#####wEAAAAAAQAAAE4AAABLAAAAHQD#####AAAAAAABAAAAAEBlNJVsMLmjAAAASgAAAE4AAABLAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQBIAAAAAAAAAAAABP#AAAAAAAAAAAAAbAP####8AAAAAAAAAAAAAAAAAQBgAAAAAAAAAAABLEAAAAAAAAQAAAAAAEVZhbGV1cgpkZSBkw6lwYXJ0AAAAHwD#####AAAAAAEAAABREAAAAAAAAQAAAAEAFlx0aW1lc1xsZWZ0KDErdFxyaWdodCkAAAAgAP####8BAAAAABAAAAEAAQAAAAoAAAANCAAAACEAAAAAAAAAAUACAAAAAAAAAAAADQEAAAAhAAAAAAAAAAFAAgAAAAAAAAAAAAQA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFAHlnUMvovRQAAAFQAAAAGAP####8BAAAAABAAAAEAAQAAADUAAAACAAAABAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBLgo9cKPXIAAAAVgAAAAcA#####wEAAAAAAQAAAFUAAABXAAAABgD#####AQAAAAAQAAABAAEAAABLAAAAAgAAAAgA#####wAAAFkAAABYAAAACQD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAgAAAFoAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAWv####8AAAACABpDTWFycXVlQW5nbGVPcmllbnRlRGlyZWN0ZQD#####AAAAAAABAAAAAUB5nVlnUPcMAAAAVwAAAFUAAABcAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQAIAAAAAAAAAAAAcAAAAAUAIAAAAAAAAAAAAHwD#####AAAAAADAFAAAAAAAAMAiAAAAAAAAAAAAXhAAAAAAAAEAAAABAAhcdGltZXMgMQAAABsA#####wAAAAAAQBAAAAAAAABAGAAAAAAAAAAAADYQAAAAAAABAAAAAAARVmFsZXVyCmQnYXJyaXbDqWUAAAAO##########8='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true);
  }

  function modifFig () {
    // cette fonction sert à modifier la figure de base
    // num est le numéro de la question (1 ou 3) selon qu’on cherche la valeur finale ou la valeur initiale
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'taux', String(stor.taux))
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function enonceInitFirst () {
    // Construction de la page
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

    me.afficheTitre(textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    // création de la zone mtg32

    stor.tabSujet = [1, 2, 3, 4, 5]

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    me.videLesZones()
    enonceMain()
  }
  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    let taux
    if (stor.tabSujet.length === 0) {
      stor.tabSujet = [1, 2, 3, 4]
    }
    const alea = j3pGetRandomInt(0, (stor.tabSujet.length - 1))
    const choixSujet = stor.tabSujet[alea]
    stor.tabSujet.splice(alea, 1)
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.obj = {}// objet contenant comme propriétés les variables
    stor.obj.styletexte = {}
    switch (choixSujet) {
      case 1:
        do {
          taux = j3pGetRandomInt(23, 40) / 10
        } while (String(taux).indexOf('.') === -1)
        break
      case 2:
        do {
          taux = j3pGetRandomInt(20, 30) / 10
        } while (String(taux).indexOf('.') === -1)
        break
      case 3:
        do {
          taux = j3pGetRandomInt(62, 85) / 10
        } while (String(taux).indexOf('.') === -1)
        taux = -taux
        break
      case 4:
        do {
          taux = j3pGetRandomInt(35, 51) / 10
        } while (String(taux).indexOf('.') === -1)
        taux = -taux
        break
      default:
        do {
          taux = j3pGetRandomInt(45, 67) / 10
        } while (String(taux).indexOf('.') === -1)
        taux = -taux
        break
    }
    stor.obj.a = j3pVirgule(Math.abs(taux))
    j3pAffiche(stor.zoneCons1, '', textes['consigne1_' + choixSujet], stor.obj)
    // Pour la question, j’ai un input et une liste
    const laReponse = j3pArrondi(1 / (1 + taux / 100) - 1, 7) * 100
    const repExacte = Math.abs(laReponse * 100 - Math.round(laReponse * 100)) < 1e-12
    stor.obj.env = (repExacte)
      ? textes.txt2
      : textes.txt1
    stor.obj.liste1 = { texte: ['', textes.choix1, textes.choix2] }
    stor.obj.inputmq1 = { texte: '' }
    const elt = j3pAffiche(stor.zoneCons2, '', textes['consigne2_' + choixSujet], stor.obj)
    stor.zoneInputMq = elt.inputmqList[0]
    stor.laListe = elt.selectList[0]
    const corrListe = (laReponse > 0) ? 1 : 2
    stor.laListe.reponse = [corrListe]
    stor.laListe.typeReponse = [true]
    if (repExacte) {
      stor.zoneInputMq.typeReponse = ['nombre', 'exact']
    } else {
      stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(stor.zoneCons3, '', textes.consigne3, stor.obj)
      stor.zoneInputMq.typeReponse = ['nombre', 'arrondi', 0.01]
      stor.larrondi = 0.01
    }
    stor.zoneInputMq.reponse = [Math.abs(laReponse)]
    mqRestriction(stor.zoneInputMq, '\\d,.')
    j3pFocus(stor.zoneInputMq)
    stor.taux = taux
    stor.opposeTaux = false// pour la correction
    stor.choixSujet = choixSujet
    stor.laReponse = laReponse
    me.logIfDebug('laReponse:', laReponse)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    const zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(zoneMD, 'div')
    stor.zoneCorr = j3pAddElt(zoneMD, 'div', '', { style: { paddingTop: '15px' } })
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.laListe.id, stor.zoneInputMq.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // finEnonce Obligatoire, mais la 1re fois faut le faire après chargement mtg32
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }

      break // case "enonce"

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              let opposeTaux = false
              let reponseSaisie = fctsValid.zones.reponseSaisie[1]
              let pbArrondi = false
              reponseSaisie = j3pNombre(reponseSaisie)
              if (!fctsValid.zones.bonneReponse[1]) {
                if ((stor.taux < 0) && (stor.laListe.selectedIndex === 1) && (reponseSaisie === Math.abs(stor.taux))) {
                  opposeTaux = true
                }
                if ((stor.taux > 0) && (stor.laListe.selectedIndex === 2) && (reponseSaisie === Math.abs(stor.taux))) {
                  opposeTaux = true
                }
                if (opposeTaux) {
                  stor.zoneCorr.innerHTML += '<br/>' + textes.comment3
                  stor.opposeTaux = true
                } else {
                  const laRepAttendue = stor.zoneInputMq.reponse[0]
                  if (stor.zoneInputMq.typeReponse[1] === 'arrondi') {
                    // On vérifie si c’est une valeur approchée
                    pbArrondi = (Math.abs(laRepAttendue - reponseSaisie) < stor.larrondi * 10)
                  }
                  if (pbArrondi) {
                    stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
                    stor.zoneCorr.innerHTML += '<br/>' + textes.comment2
                  }
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
