import { j3pAddElt, j3pArrondi, j3pChaine, j3pShuffle, j3pFocus, j3pGetRandomInt, j3pRemplacePoint, j3pVirgule, j3pAutoSizeInput, j3pSetProps } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import { phraseBloc } from 'src/legacy/core/functionsPhrase'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'
import './evolutionTableur.css'
const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Rémi DENIAUD
        décembre 2018
        On présente des données dans un tableur
        Il est possible de demander quelle formule saisir mais aussi la valeur de la moyenne et de l’écart-type des données
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuest', 'taux', 'liste', 'prend les valeurs "taux" (par défaut) si on ne demande que la formule du calcul d’un taux d’évolution ou "valeur" si on ne demande que la formule du calcul d’une valeur à l’aide d’un taux ou "les deux".', ['taux', 'valeur', 'les deux']],
    ['avecDollar', false, 'boolean', 'Si ce paramètre vaut false (ce qui est le cas par défaut), alors il n’y aura pas besoin du symbole dollar dans la formule.'],
    ['seuilReussite', 0.8, 'reel', 'seuil au-dessus duquel on considère que l’élève a réussi la notion'],
    ['seuilErreur', 0.4, 'reel', 'seuil au-dessus duquel on considère que la notion n’est pas acquise']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'Problème dans l’utilisation du dollar dans une formule' },
    { pe_3: 'Insuffisant' }
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Taux d’évolution et tableur',
  // on donne les phrases de la consigne
  consigne1_1: 'On a relevé dans la feuille de calcul (inactive) ci-dessous la population d’une petite commune d’année en année.',
  consigne1_2: 'On a relevé dans la feuille de calcul (inactive) ci-dessous le prix d’un kilogramme de cerises (en euros) sur les étalages d’un marché d’un mois à l’autre.',
  consigne1_3: 'On a relevé dans la feuille de calcul (inactive) ci-dessous l’affluence (nombre de spectateurs) dans un stade d’un match à l’autre.',
  consigne1_4: 'On a relevé dans la feuille de calcul (inactive) ci-dessous le nombre de courriels émis par une société de jour en jour.',
  consigne1_5: 'On a relevé dans la feuille de calcul (inactive) ci-dessous la distance parcourue (en km) par un adepte de la course à pied d’un jour d’entrainement à l’autre.',
  consigne1_6: 'On a relevé dans la feuille de calcul (inactive) ci-dessous le nombre d’élèves d’un lycée d’année en année.',
  consigne2_1: "On a relevé dans la feuille de calcul (inactive) ci-dessous la population d’une petite commune en £a et les taux d’évolution de cette population <span class='bgTab'>d’année en année</span>.",
  consignebis2_1: "On a relevé dans la feuille de calcul (inactive) ci-dessous la population d’une petite commune en £a et les taux d’évolution de cette population <span class='bgTab'>par rapport à cette année £a.</span>",
  consigne2_2: "On a relevé dans la feuille de calcul (inactive) ci-dessous le prix d’un kilogramme de cerises (en euros) sur les étalages d’un marché en avril et les taux d’évolution de ce prix <span class='bgTab'>d’un mois à l’autre</span>.",
  consignebis2_2: "On a relevé dans la feuille de calcul (inactive) ci-dessous le prix d’un kilogramme de cerises (en euros) sur les étalages d’un marché en avril et les taux d’évolution de ce prix <span class='bgTab'>par rapport à ce mois d’avril</span>.",
  consigne2_3: "On a relevé dans la feuille de calcul (inactive) ci-dessous l’affluence (nombre de spectateurs) dans un stade au cours d’un premier match et les taux d’évolution de cette affluence <span class='bgTab'>de match en match</span>.",
  consignebis2_3: "On a relevé dans la feuille de calcul (inactive) ci-dessous l’affluence (nombre de spectateurs) dans un stade au cours d’un premier match et les taux d’évolution de cette affluence <span class='bgTab'>par rapport à ce premier match</span>.",
  consigne2_4: "On a relevé dans la feuille de calcul (inactive) ci-dessous le nombre de courriels émis par une société un lundi et les taux d’évolution de ce nombre de courriels émis <span class='bgTab'>de jour en jour</span>.",
  consignebis2_4: "On a relevé dans la feuille de calcul (inactive) ci-dessous le nombre de courriels émis par une société un lundi et les taux d’évolution de ce nombre de courriels émis <span class='bgTab'>par rapport au lundi</span>.",
  consigne2_5: "On a relevé dans la feuille de calcul (inactive) ci-dessous la distance parcourue (en km) par un adepte de la course à pied un premier jour et les taux d’évolution de cette distance parcourue <span class='bgTab'>d’un jour d’entrainement à l’autre</span>.",
  consignebis2_5: "On a relevé dans la feuille de calcul (inactive) ci-dessous la distance parcourue (en km) par un adepte de la course à pied un premier jour et les taux d’évolution de cette distance <span class='bgTab'>par rapport au premier jour d’entrainement</span>.",
  consigne2_6: "On a relevé dans la feuille de calcul (inactive) ci-dessous le nombre d’élèves d’un lycée en £a et les taux d’évolution de ce nombre d’élèves <span class='bgTab'>d’année en année</span>.",
  consignebis2_6: "On a relevé dans la feuille de calcul (inactive) ci-dessous le nombre d’élèves d’un lycée en £a et les taux d’évolution de ce nombre d’élèves <span class='bgTab'>par rapport à cette année £a</span>.",
  table1_1: 'Année',
  table2_1: 'Population',
  table1_2: 'Mois',
  table2_2: 'Prix au kilo',
  table1_3: 'Match',
  table2_3: 'Affluence',
  table1_4: 'Jour',
  table2_4: 'Courriels',
  table1_5: 'Jour',
  table2_5: 'Distance',
  table1_6: 'Année',
  table2_6: 'Elèves',
  table3: 'Taux',
  consigne3_1: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution du nombre d’habitants de cette commune <span class='bgTab'>d’une année à l’autre</span>&nbsp;?",
  consignebas3_1: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution du nombre d’habitants de cette commune <span class='bgTab'>d’une année à l’autre</span>&nbsp;?",
  consignebis3_1: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution du nombre d’habitants de cette commune de chaque année <span class='bgTab'>par rapport à l’année de référence £a</span>&nbsp;?",
  consignebasbis3_1: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution du nombre d’habitants de cette commune de chaque année <span class='bgTab'>par rapport à l’année de référence £a</span>&nbsp;?",
  consigne3_2: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution du prix du kilogramme de cerises <span class='bgTab'>d’un mois à l’autre</span>&nbsp;?",
  consignebas3_2: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution du prix du kilogramme de cerises <span class='bgTab'>d’un mois à l’autre</span>&nbsp;?",
  consignebis3_2: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution du prix du kilogramme de cerises de chaque mois <span class='bgTab'>par rapport au mois d’avril</span>&nbsp;?",
  consignebasbis3_2: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution du prix du kilogramme de cerises de chaque mois <span class='bgTab'>par rapport au mois d’avril</span>&nbsp;?",
  consigne3_3: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution de l’affluence dans ce stade <span class='bgTab'>d’un match à l’autre</span>&nbsp;?",
  consignebas3_3: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution de l’affluence dans ce stade <span class='bgTab'>d’un match à l’autre</span>&nbsp;?",
  consignebis3_3: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution de l’affluence dans ce stade de chaque match <span class='bgTab'>par rapport au premier d’entre eux</span>&nbsp;?",
  consignebasbis3_3: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution de l’affluence dans ce stade de chaque match <span class='bgTab'>par rapport au premier d’entre eux</span>&nbsp;?",
  consigne3_4: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution du nombre de courriels émis par cette société <span class='bgTab'>d’un jour à l’autre</span>&nbsp;?",
  consignebas3_4: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution du nombre de courriels émis par cette société <span class='bgTab'>d’un jour à l’autre</span>&nbsp;?",
  consignebis3_4: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution du nombre de courriels émis par cette société de chaque jour <span class='bgTab'>par rapport au lundi</span>&nbsp;?",
  consignebasbis3_4: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution du nombre de courriels émis par cette société de chaque jour <span class='bgTab'>par rapport au lundi</span>&nbsp;?",
  consigne3_5: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution de la distance parcourue par cet adepte de la course à pied <span class='bgTab'>d’un jour d’entrainement à l’autre</span>&nbsp;?",
  consignebas3_5: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution de la distance parcourue par cet adepte de la course à pied <span class='bgTab'>d’un jour d’entrainement à l’autre</span>&nbsp;?",
  consignebis3_5: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution de la distance parcourue par cet adepte de la course à pied de chaque jour d’entrainement <span class='bgTab'>par rapport au premier</span>&nbsp;?",
  consignebasbis3_5: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution de la distance parcourue par cet adepte de la course à pied de chaque jour d’entrainement <span class='bgTab'>par rapport au premier</span>&nbsp;?",
  consigne3_6: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution du nombre d’élèves de ce lycée <span class='bgTab'>d’une année à l’autre</span>&nbsp;?",
  consignebas3_6: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution du nombre d’élèves de ce lycée <span class='bgTab'>d’une année à l’autre</span>&nbsp;?",
  consignebis3_6: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le taux d’évolution du nombre d’élèves de ce lycée de chaque année <span class='bgTab'>par rapport à l’année de référence £a</span>&nbsp;?",
  consignebasbis3_6: "Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le taux d’évolution du nombre d’élèves de ce lycée de chaque année <span class='bgTab'>par rapport à l’année de référence £a</span>&nbsp;?",
  consigne4_1: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le nombre d’habitants de cette commune pour chaque année&nbsp;?',
  consignebas4_1: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le nombre d’habitants de cette commune pour chaque année&nbsp;?',
  consigne4_2: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le prix du kilogramme de cerises pour chaque mois&nbsp;?',
  consignebas4_2: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le prix du kilogramme de cerises pour chaque mois&nbsp;?',
  consigne4_3: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne l’affluence dans ce stade pour chaque match&nbsp;?',
  consignebas4_3: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne l’affluence dans ce stade pour chaque match&nbsp;?',
  consigne4_4: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le nombre de courriels émis par cette société pour chaque jour&nbsp;?',
  consignebas4_4: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le nombre de courriels émis par cette société pour chaque jour&nbsp;?',
  consigne4_5: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne la distance parcourue par cet adepte de la course à pied pour chaque jour d’entrainement&nbsp;?',
  consignebas4_5: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne la distance parcourue par cet adepte de la course à pied pour chaque jour d’entrainement&nbsp;?',
  consigne4_6: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers la droite, on obtienne le nombre d’élèves de ce lycée pour chaque année&nbsp;?',
  consignebas4_6: 'Quelle formule faut-il écrire en £c pour, qu’en la recopiant vers le bas, on obtienne le nombre d’élèves de ce lycée pour chaque année&nbsp;?',
  consigne5: 'Formule : @1@',
  mois1: 'avril',
  mois2: 'mai',
  mois3: 'juin',
  mois4: 'juillet',
  mois5: 'août',
  mois6: 'septembre',
  jour1: 'lundi',
  jour2: 'mardi',
  jour3: 'mercredi',
  jour4: 'jeudi',
  jour5: 'vendredi',
  match: 'match',
  jour: 'jour',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Attention : quelle est la première chose à écrire pour une formule du tableur&nbsp;?',
  comment2: 'Le dollar n’est pas correctement utilisé&nbsp;!',
  comment3: 'Dans la formule, il doit y avoir une référence à une cellule&nbsp;!',
  comment4: 'Tu as cependant ajouté un dollar inutile !',
  commentbis4: 'Tu as cependant ajouté des dollars inutiles !',
  comment5: 'N’y aurait-il pas un problème dans l’utilisation du symbole $&nbsp;?',
  comment6: 'Une formule écrite en £c ne peut contenir la référence à cellule £c&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Le calcul à effectuer en £c est $£{calcul}$, ainsi on écrit la formule £{r1}.',
  corr2: 'En recopiant cette formule vers la droite, elle doit devenir £{r2}.',
  corrbas2: 'En recopiant cette formule vers le bas, elle doit devenir £{r2}.',
  corr3: 'Ainsi la formule à écrire en £c est £r.',
  corr4: 'On peut aussi écrire £s même si tous les dollars de cette formule ne sont pas indispensables.'
}
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  function creationTab (obj) {
    // obj.elt est l’élément HTML dans lequel construire la table
    // obj.tabDonnees est le tableau contenant toutes les données :
    // obj.tabDonnees[0] correspond aux légendes
    // les données sont dans obj.tabDonnees[2] et les taux dans obj.tabDonnees[3] ou obj.tabDonnees[4] (ce dernier étant pour les taux par rapport à la première valeur)
    // obj.largTableur correspond à la largeur totale de la table
    // obj.question est le type de question ("taux" ou "valeur" pour savoir la formule de quelle cellule sera demandée
    const largCol1 = 35
    let nomCellule// nomCellule sera la cellule dans laquelle écrire la formule
    let largCol2, largAutresColonnes
    if (obj.position === 'ligne') {
      largCol2 = 100
      largAutresColonnes = (obj.largTableur - largCol1 - largCol2) / obj.tabDonnees[2].length
    } else {
      largAutresColonnes = (obj.largTableur - largCol1) / obj.tabDonnees[0].length
    }
    const table = j3pAddElt(obj.elt, 'table')
    j3pSetProps(table, { width: '100%', borderColor: 'black', border: 2, cellSpacing: 0, lineHeight: 1, style: { fontSize: '0.8em' } })
    const ligne1 = j3pAddElt(table, 'tr')
    const case1 = j3pAddElt(ligne1, 'th', '', { style: { backgroundColor: '#BABABA' } })
    const caseTh = []
    j3pSetProps(case1, { width: largCol1 + 'px' })
    if (obj.position === 'ligne') {
      for (let j = 0; j <= obj.tabDonnees[1].length; j++) {
        caseTh.push(j3pAddElt(ligne1, 'th', String.fromCharCode(65 + j)))
      }
      for (let i = 0; i < obj.tabDonnees[0].length; i++) {
        const ligne = j3pAddElt(table, 'tr')
        caseTh.push(j3pAddElt(ligne, 'th', String(i + 1)))
        j3pAddElt(ligne, 'td', obj.tabDonnees[0][i])
        if (i === 2) {
          const caseVide = j3pAddElt(ligne, 'td')
          j3pSetProps(caseVide, { align: 'center', width: largAutresColonnes + 'px', style: { backgroundColor: '#696969' } })
        }// dans la ligne des taux, la première case est vide
        for (let j = 0; j < obj.tabDonnees[i + 1].length; j++) {
          const nb = String(obj.tabDonnees[i + 1][j]).replace(/\./g, ',')
          const laCase = j3pAddElt(ligne, 'td', nb)
          j3pSetProps(laCase, { align: 'center', width: largAutresColonnes + 'px' })
        }
      }
      nomCellule = (obj.question === 'taux') ? 'C3' : 'C2'
    } else {
      for (let j = 0; j < obj.tabDonnees[0].length; j++) {
        caseTh.push(j3pAddElt(ligne1, 'th', String.fromCharCode(65 + j)))
      }
      // ligne des légendes
      const ligne2 = j3pAddElt(table, 'tr')
      const case1ligne2 = j3pAddElt(ligne2, 'th', 1, { style: { backgroundColor: '#BABABA' } })
      j3pSetProps(case1ligne2, { align: 'center', width: largCol1 + 'px' })
      for (let j = 0; j < obj.tabDonnees[0].length; j++) {
        const td = j3pAddElt(ligne2, 'td', obj.tabDonnees[0][j])
        j3pSetProps(td, { align: 'center', width: largAutresColonnes + 'px' })
      }
      for (let i = 0; i < obj.tabDonnees[1].length; i++) {
        const ligne = j3pAddElt(table, 'tr')
        const case1ligne = j3pAddElt(ligne, 'th', (i + 2), { style: { backgroundColor: '#BABABA' } })
        j3pSetProps(case1ligne, { align: 'center', width: largCol1 + 'px' })
        const legende = j3pAddElt(ligne, 'td', obj.tabDonnees[1][i])
        j3pSetProps(legende, { align: 'center', width: largAutresColonnes + 'px' })
        const nb = String(obj.tabDonnees[2][i]).replace(/\./g, ',')
        const td = j3pAddElt(ligne, 'td', nb)
        j3pSetProps(td, { align: 'center', width: largAutresColonnes + 'px' })
        if (i === 0) {
          const caseVide = j3pAddElt(ligne, 'td')
          j3pSetProps(caseVide, { align: 'center', width: largAutresColonnes + 'px', style: { backgroundColor: '#696969' } })
        } else {
          const td = j3pAddElt(ligne, 'td', obj.tabDonnees[3][i - 1])
          j3pSetProps(td, { align: 'center', width: largAutresColonnes + 'px' })
        }
      }
      nomCellule = (obj.question === 'taux') ? 'C3' : 'B3'
    }
    caseTh.forEach(elt => j3pSetProps(elt, { style: { backgroundColor: '#BABABA' } }))
    return nomCellule
  }

  function verificationFormule (formuleRep, tab, decalage, direction) {
    // tab est le tableau des données. Par contre, il peut y avoir un décalage horizontal (lettre en [0]) et/ou vertical (chiffre en [1]) suivant où la première case dans laquelle se trouvent les données
    // direction vaut "droite" ou "bas" selon la façon dont on va recopier la formule
    let formSansDollarInutile = formuleRep.toUpperCase()
    let dollarsInutiles = false
    let nbDollarsInutiles = 0
    const cellule = /[A-Z][1-9]/g // permet d’identifier la présence de la référence d’une cellule dans la formule
    const formuleSansDollar = formSansDollarInutile.replace(/\$/g, '')
    const presenceEgal = (formSansDollarInutile[0] === '=')
    const presenceCellule = cellule.test(formuleSansDollar)
    const tabFormuleOK = []
    let bonneFormule1 = false // bonneFormule1 vérifie seulement si la formule marche pour la première cellule (c’est pour gérer la bonne utilisation du dollar)
    let bonneFormule = false// bonneFormule vérifie le reste
    // on teste si le nom de la cellule dans laquelle on écrit la formule est dans cette formule. Si c’est le cas, on renvoie directement le message d’erreur
    const presenceNomCellule = formuleSansDollar.includes(stor.nomCellule)
    let celluleAvecDollarInutile, tabCelluleAvecDollarInutile, k, maCell
    if (presenceEgal && presenceCellule && !presenceNomCellule) {
      // test des dollars inutiles
      if (direction === 'droite') {
        // les dollars devant les numéros de lignes des références des cellules sont inutiles
        celluleAvecDollarInutile = /[A-Z]\$[1-9]/g
        dollarsInutiles = celluleAvecDollarInutile.test(formSansDollarInutile)
        if (dollarsInutiles) {
          tabCelluleAvecDollarInutile = formSansDollarInutile.match(celluleAvecDollarInutile)
          nbDollarsInutiles = tabCelluleAvecDollarInutile.length
          for (k = 0; k < tabCelluleAvecDollarInutile.length; k++) {
            maCell = tabCelluleAvecDollarInutile[k][0] + tabCelluleAvecDollarInutile[k][2]
            formSansDollarInutile = formSansDollarInutile.replace(tabCelluleAvecDollarInutile[k], maCell)
          }
          // console.log("tabCelluleAvecDollarInutile:",tabCelluleAvecDollarInutile)
        }
      } else {
        // les dollars devant les numéros de colonnes des références des cellules sont inutiles
        celluleAvecDollarInutile = /\$[A-Z]\$?[1-9]/g
        dollarsInutiles = celluleAvecDollarInutile.test(formSansDollarInutile)
        if (dollarsInutiles) {
          tabCelluleAvecDollarInutile = formSansDollarInutile.match(celluleAvecDollarInutile)
          nbDollarsInutiles = tabCelluleAvecDollarInutile.length
          for (k = 0; k < tabCelluleAvecDollarInutile.length; k++) {
            maCell = tabCelluleAvecDollarInutile[k].substring(1)
            formSansDollarInutile = formSansDollarInutile.replace(tabCelluleAvecDollarInutile[k], maCell)
          }
          // console.log("tabCelluleAvecDollarInutile:",tabCelluleAvecDollarInutile)
        }
      }
      // arrivé ici, je n’ai plus de dollars inutiles dans la formule
      // console.log("formuleSansDollar:",formuleSansDollar,"   formSansDollarInutile:",formSansDollarInutile);
      let tabCellule = formuleSansDollar.match(cellule)
      let formule = formuleSansDollar.substring(1)
      let j, num, nb
      try {
        for (let i = 0; i < tabCellule.length; i++) {
          num = tabCellule[i][0].charCodeAt(0) - 65
          nb = Number(tabCellule[i][1])
          if (direction === 'bas') {
            formule = formule.replace(tabCellule[i], tab[num - decalage[0]][nb - decalage[1] - 1])
          } else {
            formule = formule.replace(tabCellule[i], tab[nb - decalage[1] - 1][num - decalage[0]])
          }
          // console.log("formule:", formule)
        }
        if (stor.lesQuest[me.questionCourante - 1] === 'taux') {
          tabFormuleOK[0] = Math.abs(j3pCalculValeur(formule) - tab[1][1]) < 1e-12
        } else {
          tabFormuleOK[0] = Math.abs(j3pCalculValeur(formule) - tab[0][1]) < 1e-12
        }
      } catch (error) {
        console.error(error)
        tabFormuleOK[0] = false
      }
      bonneFormule = bonneFormule1 = tabFormuleOK[0]
      if (bonneFormule1) { // il ne sert à rien d’aller plus loin si cela ne marche pas pour la première cellule
        // A cet instant j’ai vérifié si la formule marche pour la première case
        // Maintenant, il faut que je regarde les suivantes avec les éventuels dollars. Je teste maintenant sur formSansDollarInutile
        try {
          const formuleInit = formSansDollarInutile.substring(1)
          const cell1 = /\$[A-Z]{1}[0-9]{1,}/g
          const cell2 = /[A-Z]{1}\$[0-9]{1,}/g
          // de toute façon on n’a plus à cet instant de possibilité d’avoir un dollar devant la ligne et la colonne car on prend formSansDollarInutile
          for (j = 2; j < tab[0].length; j++) {
            formule = formuleInit
            if (cell1.test(formule)) {
              tabCellule = formule.match(cell1)
              for (let i = 0; i < tabCellule.length; i++) { // Donc on a trouvé des références de la forme $B2, valable seulement si on recopie vers la droite
                num = tabCellule[i][1].charCodeAt(0) - 65
                nb = Number(tabCellule[i].substring(2))
                formule = formule.replace(tabCellule[i], tab[nb - decalage[1] - 1][num - decalage[0]])
              }
            }
            if (cell2.test(formule)) {
              tabCellule = formule.match(cell2)
              for (let i = 0; i < tabCellule.length; i++) { // Donc on a trouvé des références de la forme B$2, valable seulement si on recopie vers le bas
                num = tabCellule[i][0].charCodeAt(0) - 65
                nb = Number(tabCellule[i].substring(2))
                formule = formule.replace(tabCellule[i], tab[num - decalage[0]][nb - decalage[1] - 1])
              }
            }
            if (cellule.test(formule)) {
              tabCellule = formule.match(cellule)
              for (let i = 0; i < tabCellule.length; i++) { // Donc on a trouvé des références de la forme B2, valable aussi bien si on recopie vers le bas que vers la droite
                num = tabCellule[i][0].charCodeAt(0) - 65
                nb = Number(tabCellule[i].substring(1))
                if (direction === 'bas') {
                  formule = formule.replace(tabCellule[i], tab[num - decalage[0]][nb - decalage[1] - 1 + j - 1])
                } else {
                  formule = formule.replace(tabCellule[i], tab[nb - decalage[1] - 1][num - decalage[0] + j - 1])
                }
                // console.log("formule:", formule, "   j:", j)
              }
            }
            if (stor.lesQuest[me.questionCourante - 1] === 'taux') {
              tabFormuleOK[j - 1] = Math.abs(j3pCalculValeur(formule) - tab[1][j]) < 1e-12
              // console.log("formule:", formule, "  ", j3pCalculValeur(formule), "   ", tab[1][j])
            } else {
              tabFormuleOK[j - 1] = Math.abs(j3pCalculValeur(formule) - tab[0][j]) < 1e-12
              // console.log("formule:", formule, "  ", j3pCalculValeur(formule), "   ", tab[0][j])
            }
            bonneFormule = bonneFormule && tabFormuleOK[j - 1]
          }
        } catch (error) {
          console.error(error)
          bonneFormule = false
        }
      }
    }
    return {
      Egal: presenceEgal,
      Cell: presenceCellule,
      bonneRep: [bonneFormule1, bonneFormule],
      dollarsInutiles: [dollarsInutiles, nbDollarsInutiles],
      nomCel: presenceNomCellule
    }
  }

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const explications = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let i = 1; i <= 3; i++) stor['zoneExpli' + i] = j3pAddElt(explications, 'div')
    const laCorr2 = (stor.positionDonnees[me.questionCourante - 1] === 'colonne') ? textes.corrbas2 : textes.corr2
    j3pAffiche(stor.zoneExpli1, '', textes.corr1, stor.obj)
    j3pAffiche(stor.zoneExpli2, '', laCorr2, stor.obj)
    j3pAffiche(stor.zoneExpli3, '', textes.corr3, stor.obj)
    if (stor.obj.s) phraseBloc(explications, textes.corr4, stor.obj)
  }

  function suite () {
    // OBLIGATOIRE
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    // Je génère des valeurs correspondant à un contexte particulier
    if (isNaN(ds.numSujet)) ds.numSujet = -1
    if (ds.numSujet > -1) {
      stor.choixSujet = ds.numSujet
    } else {
      if (stor.tabChoixSujet.length === 0) {
        stor.tabChoixSujet = [...stor.tabChoixSujetInit]
      }
      const alea = Math.floor(j3pGetRandomInt(0, (stor.tabChoixSujet.length - 1)))
      stor.choixSujet = stor.tabChoixSujet[alea]
      stor.tabChoixSujet.splice(alea, 1)
    }
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    const tabDonnees = []
    tabDonnees.push([textes['table1_' + stor.choixSujet], textes['table2_' + stor.choixSujet], textes.table3])
    // détermination des données :
    const tabTaux = []
    const tabTauxExact = []
    const tabTaux2 = []
    const tabTauxExact2 = []
    let tabValeurs
    let tabDates
    let nbJour, tabJour, nbAnnees, tabAnnees, augPop
    switch (stor.choixSujet) {
      case 1:
        nbAnnees = j3pGetRandomInt(4, 5)
        tabAnnees = [2018]
        {
          const tabPopulation = []
          for (let i = 1; i <= nbAnnees; i++) {
            tabAnnees.unshift(2018 - i)
          }
          tabPopulation.push(j3pGetRandomInt(700, 800))
          for (let i = 1; i < tabAnnees.length; i++) {
            augPop = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(15, 40)
            tabPopulation.push(tabPopulation[i - 1] + augPop)
          }
          tabValeurs = tabPopulation
          tabDates = tabAnnees
        }
        break
      case 2:
        {
          const nbMois = j3pGetRandomInt(5, 6)
          const tabMois = []
          const tabPrix = []
          for (let i = 1; i <= nbMois; i++) {
            tabMois.push(textes['mois' + i])
          }
          tabPrix.push(j3pGetRandomInt(125, 150) / 10)
          tabPrix.push(j3pArrondi(tabPrix[0] - j3pGetRandomInt(7, 12) / 10, 1))
          tabPrix.push(j3pArrondi(tabPrix[1] - j3pGetRandomInt(4, 8) / 10, 1))
          tabPrix.push(j3pArrondi(tabPrix[2] + j3pGetRandomInt(7, 12) / 10, 1))
          tabPrix.push(j3pArrondi(tabPrix[3] + j3pGetRandomInt(4, 8) / 10, 1))
          if (nbMois === 6) tabPrix.push(j3pArrondi(tabPrix[4] + j3pGetRandomInt(4, 8) / 10, 1))
          tabValeurs = tabPrix
          tabDates = tabMois
        }
        break
      case 3:
        {
          const nbMatch = j3pGetRandomInt(5, 6)
          const tabMatch = []
          const tabAffluence = []
          for (let i = 1; i <= nbMatch; i++) {
            tabMatch.push(textes.match + ' ' + i)
          }
          tabAffluence.push(j3pGetRandomInt(12500, 26000))
          tabAffluence.push(j3pGetRandomInt(12500, 26000))
          tabAffluence.push(j3pGetRandomInt(12500, 26000))
          tabAffluence.push(j3pGetRandomInt(12500, 26000))
          tabAffluence.push(j3pGetRandomInt(12500, 26000))
          if (nbMatch === 6) tabAffluence.push(j3pGetRandomInt(12500, 26000))
          tabValeurs = tabAffluence
          tabDates = tabMatch
        }
        break
      case 4:
        nbJour = 5
        tabJour = []
        {
          const tabCourriels = []
          for (let i = 1; i <= nbJour; i++) {
            tabJour.push(textes['jour' + i])
          }
          tabCourriels.push(j3pGetRandomInt(160, 260))
          tabCourriels.push(j3pGetRandomInt(100, 200))
          tabCourriels.push(j3pGetRandomInt(100, 200))
          tabCourriels.push(j3pGetRandomInt(100, 200))
          tabCourriels.push(j3pGetRandomInt(150, 250))
          tabValeurs = tabCourriels
          tabDates = tabJour
        }
        break
      case 5:
        nbJour = j3pGetRandomInt(5, 6)
        tabJour = []
        {
          const tabDistance = []
          for (let i = 1; i <= nbJour; i++) {
            tabJour.push(textes.jour + ' ' + i)
          }
          tabDistance.push(j3pGetRandomInt(100, 200) / 10)
          tabDistance.push(j3pGetRandomInt(100, 200) / 10)
          tabDistance.push(j3pGetRandomInt(100, 200) / 10)
          tabDistance.push(j3pGetRandomInt(100, 200) / 10)
          tabDistance.push(j3pGetRandomInt(100, 200) / 10)
          if (nbJour === 6) tabDistance.push(j3pGetRandomInt(100, 200) / 10)
          tabValeurs = tabDistance
          tabDates = tabJour
        }
        break
      default:
        nbAnnees = j3pGetRandomInt(4, 5)
        tabAnnees = [2018]
        {
          const tabEleves = []
          for (let i = 1; i <= nbAnnees; i++) {
            tabAnnees.unshift(2018 - i)
          }
          tabEleves.push(j3pGetRandomInt(1200, 1500))
          for (let i = 1; i < tabAnnees.length; i++) {
            augPop = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(30, 50)
            tabEleves.push(tabEleves[i - 1] + augPop)
          }
          tabValeurs = tabEleves
          tabDates = tabAnnees
        }
        break
    }
    for (let i = 1; i < tabDates.length; i++) {
      const taux = j3pArrondi(100 * (tabValeurs[i] - tabValeurs[i - 1]) / tabValeurs[i - 1], 2)
      tabTauxExact.push((tabValeurs[i] - tabValeurs[i - 1]) / tabValeurs[i - 1])
      tabTaux.push(j3pVirgule(taux) + '&nbsp;%')
    }
    for (let i = 1; i < tabDates.length; i++) {
      const taux2 = j3pArrondi(100 * (tabValeurs[i] - tabValeurs[0]) / tabValeurs[0], 2)
      tabTauxExact2.push((tabValeurs[i] - tabValeurs[0]) / tabValeurs[0])
      tabTaux2.push(j3pVirgule(taux2) + '&nbsp;%')
    }
    me.logIfDebug('tabDates:', tabDates, '  tabValeurs:', tabValeurs, '   tabTaux:', tabTaux, '   tabTaux2:', tabTaux2)
    tabDonnees.push(tabDates, tabValeurs)
    if (stor.dollar[me.questionCourante - 1]) {
      tabDonnees.push(tabTaux2)
      stor.tauxExact = tabTauxExact2
    } else {
      tabDonnees.push(tabTaux)
      stor.tauxExact = tabTauxExact
    }
    stor.tableauDonnees = [...tabDonnees[tabDonnees.length - 2]]// cela permet de récupérer les valeur du caractère et ainsi effectuer les tests
    if (stor.lesQuest[me.questionCourante - 1] === 'taux') {
      // je vais cacher les taux
      for (let i = 0; i < tabDonnees[tabDonnees.length - 1].length; i++) {
        tabDonnees[tabDonnees.length - 1][i] = ''
      }
    } else {
      // je vais cacher les valeurs à partir de la deuxième
      for (let i = 1; i < tabDonnees[tabDonnees.length - 2].length; i++) {
        tabDonnees[tabDonnees.length - 2][i] = ''
      }
    }

    stor.obj = {
      a: tabDonnees[1][0]
    }
    const laConsigne1 = (stor.lesQuest[me.questionCourante - 1] === 'taux')
      ? textes['consigne1_' + stor.choixSujet]
      : stor.dollar[me.questionCourante - 1]
        ? textes['consignebis2_' + stor.choixSujet]
        : textes['consigne2_' + stor.choixSujet]
    j3pAffiche(stor.zoneCons1, '', laConsigne1, stor.obj)
    let largTableur = me.zonesElts.MG.getBoundingClientRect().width
    if (stor.positionDonnees[me.questionCourante - 1] === 'colonne') largTableur = largTableur / 2
    stor.objTable = {
      elt: stor.zoneCons2,
      tabDonnees,
      largTableur,
      position: stor.positionDonnees[me.questionCourante - 1],
      question: stor.lesQuest[me.questionCourante - 1]
    }
    stor.nomCellule = creationTab(stor.objTable)
    stor.obj.c = stor.nomCellule
    const laConsigne2 = (stor.lesQuest[me.questionCourante - 1] === 'taux')
      ? stor.dollar[me.questionCourante - 1]
        ? (stor.positionDonnees[me.questionCourante - 1] === 'colonne') ? textes['consignebasbis3_' + stor.choixSujet] : textes['consignebis3_' + stor.choixSujet]
        : (stor.positionDonnees[me.questionCourante - 1] === 'colonne') ? textes['consignebas3_' + stor.choixSujet] : textes['consigne3_' + stor.choixSujet]
      : (stor.positionDonnees[me.questionCourante - 1] === 'colonne') ? textes['consignebas4_' + stor.choixSujet] : textes['consigne4_' + stor.choixSujet]
    j3pAffiche(stor.zoneCons3, '', laConsigne2, stor.obj)
    const elt = j3pAffiche(stor.zoneCons4, '', textes.consigne5,
      {
        input1: { dynamique: true, texte: '' }
      })
    stor.zoneInput = elt.inputList[0]
    stor.zoneInput.typeReponse = ['texte']
    const lesRep = []
    let formuleInitiale, formuleRecopiee, formuleAvecDollarSup
    if (stor.lesQuest[me.questionCourante - 1] === 'taux') {
      if (stor.dollar[me.questionCourante - 1]) {
        if (stor.positionDonnees[me.questionCourante - 1] === 'colonne') {
          lesRep.push('=(B3-B$2)/B$2', '=(B3-B$2)*100/B$2')
          formuleInitiale = '=(B3-B2)/B2'
          formuleRecopiee = '=(B4-B2)/B2'
        } else {
          lesRep.push('=(C2-$B2)/$B2', '=(C2-$B2)*100/B2')
          formuleInitiale = '=(C2-B2)/B2'
          formuleRecopiee = '=(D2-B2)/B2'
        }
        formuleAvecDollarSup = '=(C2-$B$2)/$B$2'
      } else {
        if (stor.positionDonnees[me.questionCourante - 1] === 'colonne') {
          lesRep.push('=(B3-B2)/B2', '=(B3-B2)*100/B2')
          formuleInitiale = '=(B3-B2)/B2'
          formuleRecopiee = '=(B4-B3)/B3'
        } else {
          lesRep.push('=(C2-B2)/B2', '=(C2-B2)*100/B2')
          formuleInitiale = '=(C2-B2)/B2'
          formuleRecopiee = '=(D2-C2)/C2'
        }
      }
    } else {
      if (stor.dollar[me.questionCourante - 1]) {
        if (stor.positionDonnees[me.questionCourante - 1] === 'colonne') {
          lesRep.push('=B$2*(1+C3)', '=B$2*(1+C3/100)')
          formuleInitiale = '=B2*(1+C3)'
          formuleRecopiee = '=B2*(1+C4)'
        } else {
          lesRep.push('=$B2*(1+C3)', '=$B2*(1+C3/100)')
          formuleInitiale = '=B2*(1+C3)'
          formuleRecopiee = '=B2*(1+D3)'
        }
        formuleAvecDollarSup = '=$B$2*(1+C3)'
      } else {
        if (stor.positionDonnees[me.questionCourante - 1] === 'colonne') {
          lesRep.push('=B2*(1+C3)', '=B2*(1+C3/100)')
          formuleInitiale = '=B2*(1+C3)'
          formuleRecopiee = '=B3*(1+C4)'
        } else {
          lesRep.push('=B2*(1+C3)', '=B2*(1+C3/100)')
          formuleInitiale = '=B2*(1+C3)'
          formuleRecopiee = '=C2*(1+D3)'
        }
      }
    }
    // calcul permettant de trouver la première valeur :
    stor.obj.calcul = (stor.lesQuest[me.questionCourante - 1] === 'taux')
      ? '\\frac{' + j3pVirgule(stor.objTable.tabDonnees[2][1]) + '-' + j3pVirgule(stor.objTable.tabDonnees[2][0]) + '}{' + j3pVirgule(stor.objTable.tabDonnees[2][0]) + '}'
      : '' + j3pVirgule(stor.objTable.tabDonnees[2][0]) + '\\times(1' + j3pGetLatexMonome(2, 0, j3pArrondi(stor.tauxExact[0], 4)) + ')'
    stor.obj.r = (lesRep[0].includes('$')) ? lesRep[0].replace(/\$/g, '&#36;') : lesRep[0]
    stor.obj.r1 = formuleInitiale
    stor.obj.r2 = formuleRecopiee
    stor.obj.s = formuleAvecDollarSup
    me.logIfDebug('reponse:', lesRep)
    stor.zoneInput.reponse = lesRep
    j3pFocus(stor.zoneInput)
    const mesZonesSaisie = [stor.zoneInput.id]
    const validePerso = [stor.zoneInput.id]
    stor.zoneInput.addEventListener('input', j3pRemplacePoint)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.correction = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso })
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Le paramètre définit la largeur relative de la première colonne
        stor.pourcMG = 70

        me.construitStructurePage({ structure: 'presentation1', ratioGauche: stor.pourcMG / 100 })

        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        stor.tabChoixSujetInit = [1, 2, 3, 4, 5, 6]
        stor.tabChoixSujet = [...stor.tabChoixSujetInit]
        stor.lesQuest = []
        let tabChoix
        if (ds.typeQuest === 'les deux') {
          for (let i = 1; i <= ds.nbrepetitions / 2; i++) {
            stor.lesQuest.push('taux', 'valeur')
          }
          if (ds.nbrepetitions % 2 !== 0) {
            tabChoix = ['taux', 'valeur']
            stor.lesQuest.push(tabChoix[j3pGetRandomInt(0, 1)])
          }
        } else {
          for (let i = 0; i < ds.nbrepetitions; i++) {
            stor.lesQuest.push(ds.typeQuest)
          }
        }
        stor.lesQuest = j3pShuffle(stor.lesQuest)
        // positionDonnees me permet de positionner les données en ligne ou en colonne dans la feuille de calcul
        stor.positionDonnees = []
        for (let i = 1; i <= ds.nbrepetitions / 2; i++) {
          stor.positionDonnees.push('ligne', 'colonne')
        }
        if (ds.nbrepetitions % 2 !== 0) {
          tabChoix = ['ligne', 'colonne']
          stor.positionDonnees.push(tabChoix[j3pGetRandomInt(0, 1)])
        }
        stor.positionDonnees = j3pShuffle(stor.positionDonnees)
        stor.dollar = []// savoir si un dollar sera nécessaire dans la réponse à donner
        if (ds.avecDollar) {
          // on a des dollars une fois sur deux (en moyenne car les cas de figure sont mélangés)
          for (let i = 1; i <= ds.nbrepetitions / 2; i++) {
            stor.dollar.push(true, false)
          }
          if (ds.nbrepetitions % 2 !== 0) {
            const tabChoixDollar = [true, false]
            stor.dollar.push(tabChoixDollar[j3pGetRandomInt(0, 1)])
          }
          stor.dollar = j3pShuffle(stor.dollar)
        } else {
          for (let i = 0; i < ds.nbrepetitions; i++) {
            stor.dollar.push(false)
          }
        }
        suite()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        // question sur le tableur
        const reponse = { aRepondu: false, bonneReponse: true }
        reponse.aRepondu = fctsValid.valideReponses()
        let objRep, objRep2
        if (reponse.aRepondu) {
          const tab = [stor.tableauDonnees, [''].concat(stor.tauxExact)]
          const decalage = [1, 1]
          const laDirection = (stor.positionDonnees[me.questionCourante - 1] === 'colonne') ? 'bas' : 'droite'
          objRep = verificationFormule(fctsValid.zones.reponseSaisie[0], tab, decalage, laDirection)
          objRep2 = { bonneRep: [false, false] }
          reponse.bonneReponse = objRep.bonneRep[1]
          if (!reponse.bonneReponse && !objRep.bonneRep[0] && objRep.Egal && objRep.Cell && !objRep.nomCel) {
          // c’est peut-être que l’élève n’a pas pris la valeur décimale du taux mais la valeur multipliée par 100...
          // pas besoin de tester ça s’il n’y a pas de égal, de référence à une cellule et si la formule marche pour la première cellule
            const newTauxExact = ['']
            for (let k = 0; k < stor.tauxExact.length; k++) {
              newTauxExact.push(stor.tauxExact[k] * 100)
            }
            objRep2 = verificationFormule(fctsValid.zones.reponseSaisie[0], [stor.tableauDonnees, newTauxExact], decalage, laDirection)
            reponse.bonneReponse = objRep2.bonneRep[1]
          }
          if (reponse.bonneReponse) {
            stor.zoneInput.value = fctsValid.zones.reponseSaisie[0].toUpperCase()
            j3pAutoSizeInput(stor.zoneInput)
          }
          fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
          fctsValid.coloreUneZone(fctsValid.zones.inputs[0])
        }
        // A cet instant reponse contient deux éléments :

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.correction)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.correction.style.color = me.styles.cbien
            stor.correction.innerHTML = cBien
            if (objRep.dollarsInutiles[0]) {
            // Dans la réponse on a trouvé des dollars qui ne sont pas utiles mais la réponse est quand même acceptables
              if (objRep.dollarsInutiles[1] > 1) {
                stor.correction.innerHTML += '<br/>' + textes.commentbis4
              } else {
                stor.correction.innerHTML += '<br/>' + textes.comment4
              }
            }
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.correction.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.correction.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.correction.innerHTML = cFaux
              if (!objRep.Egal) {
                stor.correction.innerHTML += '<br/>' + textes.comment1
              }
              if (!objRep.Cell) {
                stor.correction.innerHTML += '<br/>' + textes.comment3
              }
              if (objRep.nomCel) {
                stor.correction.innerHTML += '<br/>' + j3pChaine(textes.comment6, { c: stor.nomCellule })
              }
              if (objRep.Egal && objRep.Cell && objRep.bonneRep[0]) {
                stor.correction.innerHTML += '<br/>' + textes.comment5
              } else {
                if (objRep2.bonneRep[0]) {
                  stor.correction.innerHTML += '<br/>' + textes.comment5
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.correction.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.correction.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                if (objRep.Egal && objRep.Cell && objRep.bonneRep[0]) { // c’est que j’ai un problème avec le dollar
                  me.typederreurs[3]++
                } else {
                  if (objRep2.bonneRep[0]) {
                    me.typederreurs[3]++
                  }
                }
                afficheCorrection(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const tauxEchecDollar = me.typederreurs[3] / (ds.nbrepetitions)
        if (me.score / ds.nbitems >= ds.seuilReussite) {
          me.parcours.pe = ds.pe_1
        } else if (tauxEchecDollar > ds.seuilErreur) {
          me.parcours.pe = ds.pe_2
        } else {
          me.parcours.pe = ds.pe_3
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
