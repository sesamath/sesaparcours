import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pDetruit, j3pGetRandomInt, j3pNombre, j3pFocus, j3pStyle, j3pVirgule, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Rémi DENIAUD
        octobre 2018
        calcul du taux succesifs suite à plieurs évolutions (2 par défaut)
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['deuxEvolutions', true, 'boolean', 'Si ce paramètre vaut true, on aura deux évolutions successives. Il faut mettre false pour en avoir plus (3 ou 4).']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Taux d’évolution successifs',
  // on donne les phrases de la consigne
  consigne1: 'La valeur d’une action a £c de £a % en 2016 puis a £d de £b % en 2017.',
  consigne2: 'La population d’une petite ville a £c de £a % en 2016 puis a £d de £b % en 2017.',
  consigne3: 'Le nombre de visites d’un site internet a £c de £a % en 2016 puis a £d de £b % en 2017.',
  consigne4: 'Le nombre de véhicules empruntant le centre ville a £c de £a % en 2016 puis a £d de £b % en 2017.', // £c et £d correspondent à "augmenté" ou "diminu" suivant le signe des taux
  consigne10_1: 'Sur ces deux années, elle a #1# £{env} &1& %.',
  consigne10_2: 'Sur ces deux années, il a #1# £{env} &1& %.',
  consignebis1: 'Le tableau ci-dessous représente l’évolution de la valeur d’une action de 2013 à £{annee}',
  consignebis2: 'Le tableau ci-dessous représente l’évolution de la population d’une petite ville de 2013 à £{annee}',
  consignebis3: 'Le tableau ci-dessous représente l’évolution du nombre de visites d’un site Internet de 2013 à £{annee}',
  consignebis4: 'Le tableau ci-dessous représente l’évolution du nombre de véhicules empruntant le centre ville de 2013 à £{annee}',
  consignebis10_1: 'Entre 2013 et £{annee}, elle a #1# £{env} &1& %.',
  consignebis10_2: 'Entre 2013 et £{annee}, il a #1# £{env} &1& %.',
  tableau1: 'Année',
  tableau2: 'Pourcentage d’évolution',
  consigne11: 'Arrondir à 0,1 %.',
  choix1: 'augmenté',
  choix2: 'diminué',
  txt1: 'd’environ',
  txt2: 'de',
  // les différents commentaires envisagés suivant les réponses fausses de l’élève
  comment1: 'N’y aurait-il pas un problème d’arrondi ?',
  comment2: 'On rappelle par ailleurs que pour arrondir le taux à 0,1 %, il faut arrondir la valeur décimale à 0,001.',
  comment3: 'Ajouter les taux d’évolutions successifs ne peut donner le taux recherché !',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'La situation peut être résumée par le schéma ci-dessous :',
  corr1_2: 'On chercher alors $t$ tel que $1+t=(1£{t1})(1£{t2})$.',
  corr1_3: 'On obtient $1+t£e£{coef}$ et ainsi $t£e£{coef}-1=£q$ soit £p %.',
  corr1_41: 'Donc la valeur de l’action a £k £f £{p2}&nbsp;% au cours de ces deux années.',
  corr1_42: 'Donc la population de cette ville a £k £f £{p2}&nbsp;% au cours de ces deux années.',
  corr1_43: 'Donc le nombre de visites de ce site a £k £f £{p2}&nbsp;% au cours de ces deux années.',
  corr1_44: 'Donc le nombre de véhicules empruntant le centre ville a £k £f £{p2}&nbsp;% au cours de ces deux années.',
  corr2_21: 'On chercher alors $t$ tel que $1+t=(1£{t1})(1£{t2})(1£{t3})$.',
  corr2_22: 'On chercher alors $t$ tel que $1+t=(1£{t1})(1£{t2})(1£{t3})(1£{t4})$.',
  corr2_3: 'On obtient $1+t£e£{coef}$ et ainsi $t£e£{coef}-1=£q$ soit £p %.',
  corr2_41: 'Donc de 2013 à £{annee}, la valeur de l’action a £k £f £{p2}&nbsp;%.',
  corr2_42: 'Donc de 2013 à £{annee}, la population de cette ville a £k £f £{p2}&nbsp;%.',
  corr2_43: 'Donc de 2013 à £{annee}, le nombre de visites de ce site a £k £f £{p2}&nbsp;%.',
  corr2_44: 'Donc de 2013 à £{annee}, le nombre de véhicules empruntant le centre ville a £k £f £{p2}&nbsp;%.'
}
/**
 * section tauxSuccessifs
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const laRep = stor.laReponse
    const laRepAffichee = Math.abs(Math.round(laRep * 10) / 10)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', textes.corr1_1)
    // dans explication1, on a l’explication correspondant à la première question
    /// /////////////////////////////////////// Pour MathGraph32
    const largFig = (stor.evolutions.length === 2)
      ? 480
      : (stor.evolutions.length === 3) ? 490 : 630
    const hautFig = 220
    // création du div accueillant la figure mtg32
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(stor.zoneExpli2, { id: stor.mtg32svg, width: largFig, height: hautFig })
    j3pStyle(stor.zoneExpli2, { textAlign: 'center' })
    // ce qui suit lance la création initiale de la figure
    depart(stor.evolutions)
    modifFig(stor.evolutions)
    stor.obj.e = stor.repExacte ? '=' : '\\approx'
    stor.obj.r = j3pVirgule(Math.abs(laRepAffichee))
    stor.obj.k = (laRep < 0) ? textes.choix2 : textes.choix1
    stor.obj.f = stor.repExacte ? textes.txt2 : textes.txt1
    if (ds.deuxEvolutions) {
      stor.obj.t1 = (stor.taux1 > 0) ? '+\\frac{' + j3pVirgule(Math.abs(stor.taux1)) + '}{100}' : '-\\frac{' + j3pVirgule(Math.abs(stor.taux1)) + '}{100}'
      stor.obj.t2 = (stor.taux2 > 0) ? '+\\frac{' + j3pVirgule(Math.abs(stor.taux2)) + '}{100}' : '-\\frac{' + j3pVirgule(Math.abs(stor.taux2)) + '}{100}'
      stor.obj.coef = (stor.inputMqZone.typeReponse[1] === 'exact') ? Math.round(Math.pow(10, 7) * (1 + Math.round(laRep * 1000) / 100000)) / Math.pow(10, 7) : Math.round(Math.pow(10, 7) * (1 + Math.round(laRep * 10) / 1000)) / Math.pow(10, 7)
      stor.obj.q = (stor.inputMqZone.typeReponse[1] === 'exact') ? Math.round(laRep * 1000) / 100000 : Math.round(laRep * 10) / 1000
      stor.obj.p = (stor.inputMqZone.typeReponse[1] === 'exact') ? j3pVirgule(Math.round(laRep * 1000) / 1000) : j3pVirgule(Math.round(laRep * 10) / 10)
      stor.obj.p2 = (stor.inputMqZone.typeReponse[1] === 'exact') ? j3pVirgule(Math.round(Math.abs(laRep) * 1000) / 1000) : j3pVirgule(Math.round(Math.abs(laRep) * 10) / 10)
      j3pAffiche(stor.zoneExpli3, '', textes.corr1_2, stor.obj)
      j3pAffiche(stor.zoneExpli4, '', textes.corr1_3, stor.obj)
      j3pAffiche(stor.zoneExpli5, '', textes['corr1_4' + stor.choixSujet], stor.obj)
    } else {
      for (let i = 0; i < stor.evolutions.length; i++) {
        stor.obj['t' + (i + 1)] = (stor.evolutions[i] > 0) ? '+\\frac{' + j3pVirgule(Math.abs(stor.evolutions[i])) + '}{100}' : '-\\frac{' + j3pVirgule(Math.abs(stor.evolutions[i])) + '}{100}'
      }
      stor.obj.coef = (stor.inputMqZone.typeReponse[1] === 'exact') ? Math.round(Math.pow(10, 7) * (1 + Math.round(laRep * 1000) / 100000)) / Math.pow(10, 7) : Math.round(Math.pow(10, 7) * (1 + Math.round(laRep * 10) / 1000)) / Math.pow(10, 7)
      stor.obj.q = (stor.inputMqZone.typeReponse[1] === 'exact') ? Math.round(laRep * 1000) / 100000 : Math.round(laRep * 10) / 1000
      stor.obj.p = (stor.inputMqZone.typeReponse[1] === 'exact') ? j3pVirgule(Math.round(laRep * 1000) / 1000) : j3pVirgule(Math.round(laRep * 10) / 10)
      stor.obj.p2 = (stor.inputMqZone.typeReponse[1] === 'exact') ? j3pVirgule(Math.round(Math.abs(laRep) * 1000) / 1000) : j3pVirgule(Math.round(Math.abs(laRep) * 10) / 10)
      if (stor.evolutions.length === 3) {
        j3pAffiche(stor.zoneExpli3, '', textes.corr2_21, stor.obj)
      } else {
        j3pAffiche(stor.zoneExpli3, '', textes.corr2_22, stor.obj)
      }
      j3pAffiche(stor.zoneExpli4, '', textes.corr1_3, stor.obj)
      j3pAffiche(stor.zoneExpli5, '', textes['corr2_4' + stor.choixSujet], stor.obj)
    }
  }

  function tableauDonnees (mesDonnees) {
    // mesDonnees est un objet
    // mesDonnees.parent est le nom du div dans lequel est créé le tableau
    // mesDonnees.largeur est la largeur du tableau (utile pour configurer la largeur de chaque colonne)
    // mesDonnees.textes est un tableau de 2 éléments max : "Année", Evolution"
    // mesDonnees.annees : tableau des années
    // mesDonnees.evolutions : tableaux des évolutions
    function creerLigneTab (elt, valeurs, l1, l2) {
      // elt est l’élément html dans lequel construire la ligne
      // l1 et l2 sont respectivement la largeur de la première colonne et des suivantes
      const ligne = j3pAddElt(elt, 'tr')
      j3pAddElt(ligne, 'td', valeurs[0], { width: l1 + 'px' })
      for (let i = 1; i < valeurs.length; i++) {
        j3pAddElt(ligne, 'td', String(valeurs[i]).replace(/\./g, ','), { align: 'center', width: l2 + 'px' })
      }
    }
    const leTableau = j3pAddElt(mesDonnees.parent, 'div', { width: mesDonnees.largeur + 'px' })
    if (mesDonnees.valeurs.length === mesDonnees.evolutions.length) {
      let largeurTexte = 0
      for (let i = 0; i < mesDonnees.textes.length; i++) {
        // largeurTexte = Math.max(largeurTexte, mesDonnees.textes[i].textWidth(me.styles.petit.enonce.fontFamily, me.styles.petit.enonce.fontSize.substring(0, me.styles.petit.enonce.fontSize.length - 2)))
        largeurTexte = Math.max(largeurTexte, $(leTableau).textWidth(mesDonnees.textes[i], me.styles.petit.enonce.fontFamily, me.styles.petit.enonce.fontSize))
      }
      largeurTexte += 10
      const largeurColonne = (mesDonnees.largeur - largeurTexte - 5) / mesDonnees.evolutions.length
      const table = j3pAddElt(leTableau, 'table', { width: '100%', borderColor: 'black', border: 2, cellSpacing: 0 })
      creerLigneTab(table, [mesDonnees.textes[0]].concat(mesDonnees.valeurs), largeurTexte, largeurColonne)
      creerLigneTab(table, [mesDonnees.textes[1]].concat(mesDonnees.evolutions), largeurTexte, largeurColonne)
    } else {
      j3pShowError('pb sur le nombre de valeurs dans les deux tableaux')
    }
  }

  function depart (tabTaux) {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFig = tabTaux.length === 2
      ? 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV7AAADnAAAAQEAAAAAAAAAAQAAAGP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAFAAFAYtPXCj1wpEBTwUeuFHri#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAAQ4AAUkAwBgAAAAAAAAAAAAAAAAAAAUAAUBEsKPXCj1wAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAQAAAAP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAABAAAABP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQUAAQAAAAcAAAAJAP####8BAAAAAQ4AAUoAwCgAAAAAAADAEAAAAAAAAAUAAgAAAAf#####AAAAAgAHQ1JlcGVyZQD#####Aebm5gABAAAAAQAAAAMAAAAJAQEAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgDALAAAAAAAAEAAAAAAAAAAAQAAAAADAAAADP####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAAN#####wAAAAEAB0NDYWxjdWwA#####wAHbmJncmFkeAACMjAAAAABQDQAAAAAAAAAAAARAP####8AB25iZ3JhZHkAAjIwAAAAAUA0AAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AFEdyYWR1YXRpb25BeGVzUmVwZXJlAAAAGwAAAAgAAAADAAAACgAAAA8AAAAQ#####wAAAAEAE0NBYnNjaXNzZU9yaWdpbmVSZXAAAAAAEQAFYWJzb3IAAAAK#####wAAAAEAE0NPcmRvbm5lZU9yaWdpbmVSZXAAAAAAEQAFb3Jkb3IAAAAKAAAACwAAAAARAAZ1bml0ZXgAAAAK#####wAAAAEACkNVbml0ZXlSZXAAAAAAEQAGdW5pdGV5AAAACv####8AAAABABBDUG9pbnREYW5zUmVwZXJlAAAAABEAAAAAABAAAAEFAAAAAAoAAAAOAAAAEgAAAA4AAAATAAAAFgAAAAARAAAAAAAQAAABBQAAAAAKAAAADQAAAAAOAAAAEgAAAA4AAAAUAAAADgAAABMAAAAWAAAAABEAAAAAABAAAAEFAAAAAAoAAAAOAAAAEgAAAA0AAAAADgAAABMAAAAOAAAAFQAAAAwAAAAAEQAAABYAAAAOAAAADwAAAA8AAAAAEQAAAAAAEAAAAQUAAAAAFwAAABkAAAAMAAAAABEAAAAWAAAADgAAABAAAAAPAAAAABEAAAAAABAAAAEFAAAAABgAAAAb#####wAAAAEACENTZWdtZW50AAAAABEBAAAAABAAAAEAAQAAABcAAAAaAAAAFwAAAAARAQAAAAAQAAABAAEAAAAYAAAAHAAAAAQAAAAAEQEAAAAACwABVwDAFAAAAAAAAMA0AAAAAAAABQABP9xWeJq83w4AAAAd#####wAAAAIACENNZXN1cmVYAAAAABEABnhDb29yZAAAAAoAAAAfAAAAEQAAAAARAAVhYnN3MQAGeENvb3JkAAAADgAAACD#####AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAABEBZmZmAAAAHwAAAA4AAAAPAAAAHwAAAAIAAAAfAAAAHwAAABEAAAAAEQAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABIAAAAOAAAAIQAAABYAAAAAEQEAAAAAEAAAAQUAAAAACgAAAA4AAAAjAAAADgAAABMAAAAZAQAAABEBZmZmAAAAJAAAAA4AAAAPAAAAHwAAAAUAAAAfAAAAIAAAACEAAAAjAAAAJAAAAAQAAAAAEQEAAAAACwABUgBAIAAAAAAAAMAgAAAAAAAABQABP9EbToG06B8AAAAe#####wAAAAIACENNZXN1cmVZAAAAABEABnlDb29yZAAAAAoAAAAmAAAAEQAAAAARAAVvcmRyMQAGeUNvb3JkAAAADgAAACcAAAAZAQAAABEBZmZmAAAAJgAAAA4AAAAQAAAAJgAAAAIAAAAmAAAAJgAAABEAAAAAEQAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABMAAAAOAAAAKAAAABYAAAAAEQEAAAAAEAAAAQUAAAAACgAAAA4AAAASAAAADgAAACoAAAAZAQAAABEBZmZmAAAAKwAAAA4AAAAQAAAAJgAAAAUAAAAmAAAAJwAAACgAAAAqAAAAK#####8AAAACAAxDQ29tbWVudGFpcmUAAAAAEQFmZmYAP#AAAAAAAABAHAAAAAAAAAAAAB8LAAH###8AAAABAAAAAAALI1ZhbChhYnN3MSkAAAAZAQAAABEBZmZmAAAALQAAAA4AAAAPAAAAHwAAAAQAAAAfAAAAIAAAACEAAAAtAAAAGwAAAAARAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAJAsAAf###wAAAAEAAAAAAAsjVmFsKGFic3cyKQAAABkBAAAAEQFmZmYAAAAvAAAADgAAAA8AAAAfAAAABgAAAB8AAAAgAAAAIQAAACMAAAAkAAAALwAAABsAAAAAEQFmZmYAwCAAAAAAAAA#8AAAAAAAAAAAACYLAAH###8AAAACAAAAAQALI1ZhbChvcmRyMSkAAAAZAQAAABEBZmZmAAAAMQAAAA4AAAAQAAAAJgAAAAQAAAAmAAAAJwAAACgAAAAxAAAAGwAAAAARAWZmZgDAHAAAAAAAAAAAAAAAAAAAAAAAKwsAAf###wAAAAIAAAABAAsjVmFsKG9yZHIyKQAAABkBAAAAEQFmZmYAAAAzAAAADgAAABAAAAAmAAAABgAAACYAAAAnAAAAKAAAACoAAAArAAAAMwAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAr#####AAAAAQAMQ01vaW5zVW5haXJlAAAAAUAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAQA#####wEAAAAAEAAAAQUAAUAMzWTXStuDAAAABQAAAAcA#####wEAAAAAAQAAADcAAAA1#####wAAAAIAHENNYXJxdWVBbmdsZU9yaWVudGVJbmRpcmVjdGUA#####wAAAAAAAQAAAAFAZUPUZDpD8wAAADUAAAA3AAAANgAAAAAbAP####8AAAAAAAAAAAAAAAAAQBQAAAAAAAAAAAA1EAAAAAAAAQAAAAAAEVZhbGV1cgpkZSBkw6lwYXJ0AAAACAD#####AAAABQAAADgAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAOwAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAAA7AAAAEQD#####AAVwb3NWQQACLTEAAAAcAAAAAT#wAAAAAAAAAAAAEQD#####AAVwb3NWRAACLTEAAAAcAAAAAT#wAAAAAAAAAAAAEQD#####AAJWQQAEMTIwMAAAAAFAksAAAAAAAAAAABEA#####wACVkQABDEwMDAAAAABQI9AAAAAAAAAAAARAP####8AC3Bvc1RhdXhQbHVzAAExAAAAAT#wAAAAAAAAAAAAEQD#####AAxwb3NUYXV4TW9pbnMAATIAAAABQAAAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAQAAAAAAAAAAAAAADgAAAEIAAAARAP####8ABHRhdXgAAy0yNQAAABwAAAABQDkAAAAAAAAAAAARAP####8ABHRlc3QABnRhdXg+MAAAAA0FAAAADgAAAEUAAAABAAAAAAAAAAAAAAARAP####8ACnZhbEFic1RhdXgACWFicyh0YXV4Kf####8AAAACAAlDRm9uY3Rpb24AAAAADgAAAEX#####AAAAAgAGQ0xhdGV4AP####8AAAAAAQAAAEQQAAAAAAABAAAAAQBvXElme3Rlc3R9e1x0aW1lc1xsZWZ0KDErXGZyYWN7XFZhbHt2YWxBYnNUYXV4fX17MTAwfVxyaWdodCl9e1x0aW1lc1xsZWZ0KDEtXGZyYWN7XFZhbHt2YWxBYnNUYXV4fX17MTAwfVxyaWdodCl9#####wAAAAIADENEcm9pdGVQYXJFcQD#####AQAAAAAQAAABAAEAAAAKAAAADQj#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAAAUASAAAAAAAAAAAADQEAAAAhAAAAAAAAAAFAEgAAAAAAAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQAQAAAAAAAAAAAABAAAAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAUAaAAAAAAAAAAAAAQAAAAAAAAAAAAAABgD#####AQAAAAAQAAABAAEAAAA3AAAABf####8AAAACABFDTWFycXVlQW5nbGVEcm9pdAD#####AQAAAAABAAAAAUAiAAAAAAAAAAAATP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAABJAAAATAAAAAcA#####wEAAAAAAQAAAE4AAABLAAAAHQD#####AAAAAAABAAAAAEBlNJVsMLmjAAAASgAAAE4AAABLAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQBIAAAAAAAAAAAABP#AAAAAAAAAAAAAbAP####8AAAAAAAAAAAAAAAAAQBgAAAAAAAAAAABLEAAAAAAAAQAAAAAAEVZhbGV1cgpkJ2Fycml2w6llAAAAEQD#####AAV0YXV4MgACMTIAAAABQCgAAAAAAAAAAAARAP####8ABXRlc3QyAAd0YXV4Mj4wAAAADQUAAAAOAAAAUwAAAAEAAAAAAAAAAAAAABEA#####wALdmFsQWJzVGF1eDIACmFicyh0YXV4MikAAAAeAAAAAA4AAABTAAAAHwD#####AAAAAAEAAABREAAAAAAAAQAAAAEAclxJZnt0ZXN0Mn17XHRpbWVzXGxlZnQoMStcZnJhY3tcVmFse3ZhbEFic1RhdXgyfX17MTAwfVxyaWdodCl9e1x0aW1lc1xsZWZ0KDEtXGZyYWN7XFZhbHt2YWxBYnNUYXV4Mn19ezEwMH1ccmlnaHQpfQAAACAA#####wEAAAAAEAAAAQABAAAACgAAAA0IAAAAIQAAAAAAAAABQAIAAAAAAAAAAAANAQAAACEAAAAAAAAAAUACAAAAAAAAAAAABAD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUAeWdQy+i9FAAAAVwAAAAYA#####wEAAAAAEAAAAQABAAAANQAAAAIAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQEuCj1wo9cgAAABZAAAABwD#####AQAAAAABAAAAWAAAAFoAAAAGAP####8BAAAAABAAAAEAAQAAAEsAAAACAAAACAD#####AAAAXAAAAFsAAAAJAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAAXQAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAABd#####wAAAAIAGkNNYXJxdWVBbmdsZU9yaWVudGVEaXJlY3RlAP####8AAAAAAAEAAAABQHmdWWdQ9wwAAABaAAAAWAAAAF8AAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAAgAAAAAAAAAAABwAAAABQAgAAAAAAAAAAAAfAP####8AAAAAAMAUAAAAAAAAwCIAAAAAAAAAAABhEAAAAAAAAQAAAAEAC1x0aW1lcygxK3QpAAAADv##########'
      : (tabTaux.length === 3)
          ? 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV7AAADnAAAAQEAAAAAAAAAAQAAAG######AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAFAAFAWmeuFHrhSEBWQUeuFHrq#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAAQ4AAUkAwBgAAAAAAAAAAAAAAAAAAAUAAUA#YUeuFHrgAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAQAAAAP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAABAAAABP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQUAAQAAAAcAAAAJAP####8BAAAAAQ4AAUoAwCgAAAAAAADAEAAAAAAAAAUAAgAAAAf#####AAAAAgAHQ1JlcGVyZQD#####Aebm5gABAAAAAQAAAAMAAAAJAQEAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgDALAAAAAAAAEAAAAAAAAAAAQAAAAADAAAADP####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAAN#####wAAAAEAB0NDYWxjdWwA#####wAHbmJncmFkeAACMjAAAAABQDQAAAAAAAAAAAARAP####8AB25iZ3JhZHkAAjIwAAAAAUA0AAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AFEdyYWR1YXRpb25BeGVzUmVwZXJlAAAAGwAAAAgAAAADAAAACgAAAA8AAAAQ#####wAAAAEAE0NBYnNjaXNzZU9yaWdpbmVSZXAAAAAAEQAFYWJzb3IAAAAK#####wAAAAEAE0NPcmRvbm5lZU9yaWdpbmVSZXAAAAAAEQAFb3Jkb3IAAAAKAAAACwAAAAARAAZ1bml0ZXgAAAAK#####wAAAAEACkNVbml0ZXlSZXAAAAAAEQAGdW5pdGV5AAAACv####8AAAABABBDUG9pbnREYW5zUmVwZXJlAAAAABEAAAAAABAAAAEFAAAAAAoAAAAOAAAAEgAAAA4AAAATAAAAFgAAAAARAAAAAAAQAAABBQAAAAAKAAAADQAAAAAOAAAAEgAAAA4AAAAUAAAADgAAABMAAAAWAAAAABEAAAAAABAAAAEFAAAAAAoAAAAOAAAAEgAAAA0AAAAADgAAABMAAAAOAAAAFQAAAAwAAAAAEQAAABYAAAAOAAAADwAAAA8AAAAAEQAAAAAAEAAAAQUAAAAAFwAAABkAAAAMAAAAABEAAAAWAAAADgAAABAAAAAPAAAAABEAAAAAABAAAAEFAAAAABgAAAAb#####wAAAAEACENTZWdtZW50AAAAABEBAAAAABAAAAEAAQAAABcAAAAaAAAAFwAAAAARAQAAAAAQAAABAAEAAAAYAAAAHAAAAAQAAAAAEQEAAAAACwABVwDAFAAAAAAAAMA0AAAAAAAABQABP9xWeJq83w4AAAAd#####wAAAAIACENNZXN1cmVYAAAAABEABnhDb29yZAAAAAoAAAAfAAAAEQAAAAARAAVhYnN3MQAGeENvb3JkAAAADgAAACD#####AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAABEBZmZmAAAAHwAAAA4AAAAPAAAAHwAAAAIAAAAfAAAAHwAAABEAAAAAEQAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABIAAAAOAAAAIQAAABYAAAAAEQEAAAAAEAAAAQUAAAAACgAAAA4AAAAjAAAADgAAABMAAAAZAQAAABEBZmZmAAAAJAAAAA4AAAAPAAAAHwAAAAUAAAAfAAAAIAAAACEAAAAjAAAAJAAAAAQAAAAAEQEAAAAACwABUgBAIAAAAAAAAMAgAAAAAAAABQABP9EbToG06B8AAAAe#####wAAAAIACENNZXN1cmVZAAAAABEABnlDb29yZAAAAAoAAAAmAAAAEQAAAAARAAVvcmRyMQAGeUNvb3JkAAAADgAAACcAAAAZAQAAABEBZmZmAAAAJgAAAA4AAAAQAAAAJgAAAAIAAAAmAAAAJgAAABEAAAAAEQAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABMAAAAOAAAAKAAAABYAAAAAEQEAAAAAEAAAAQUAAAAACgAAAA4AAAASAAAADgAAACoAAAAZAQAAABEBZmZmAAAAKwAAAA4AAAAQAAAAJgAAAAUAAAAmAAAAJwAAACgAAAAqAAAAK#####8AAAACAAxDQ29tbWVudGFpcmUAAAAAEQFmZmYAP#AAAAAAAABAHAAAAAAAAAAAAB8LAAH###8AAAABAAAAAAALI1ZhbChhYnN3MSkAAAAZAQAAABEBZmZmAAAALQAAAA4AAAAPAAAAHwAAAAQAAAAfAAAAIAAAACEAAAAtAAAAGwAAAAARAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAJAsAAf###wAAAAEAAAAAAAsjVmFsKGFic3cyKQAAABkBAAAAEQFmZmYAAAAvAAAADgAAAA8AAAAfAAAABgAAAB8AAAAgAAAAIQAAACMAAAAkAAAALwAAABsAAAAAEQFmZmYAwCAAAAAAAAA#8AAAAAAAAAAAACYLAAH###8AAAACAAAAAQALI1ZhbChvcmRyMSkAAAAZAQAAABEBZmZmAAAAMQAAAA4AAAAQAAAAJgAAAAQAAAAmAAAAJwAAACgAAAAxAAAAGwAAAAARAWZmZgDAHAAAAAAAAAAAAAAAAAAAAAAAKwsAAf###wAAAAIAAAABAAsjVmFsKG9yZHIyKQAAABkBAAAAEQFmZmYAAAAzAAAADgAAABAAAAAmAAAABgAAACYAAAAnAAAAKAAAACoAAAArAAAAMwAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAr#####AAAAAQAMQ01vaW5zVW5haXJlAAAAAUAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAQA#####wEAAAAAEAAAAQUAAUADYWhCkd28AAAABQAAAAcA#####wEAAAAAAQAAADcAAAA1#####wAAAAIAHENNYXJxdWVBbmdsZU9yaWVudGVJbmRpcmVjdGUA#####wAAAAAAAQAAAAFAWIpOB+gIDAAAADUAAAA3AAAANgAAAAAbAP####8AAAAAAAAAAAAAAAAAQBQAAAAAAAAAAAA1EAAAAAAAAQAAAAAAEVZhbGV1cgpkZSBkw6lwYXJ0AAAACAD#####AAAABQAAADgAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAOwAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAAA7AAAAEQD#####AAVwb3NWQQACLTEAAAAcAAAAAT#wAAAAAAAAAAAAEQD#####AAVwb3NWRAACLTEAAAAcAAAAAT#wAAAAAAAAAAAAEQD#####AAJWQQAEMTIwMAAAAAFAksAAAAAAAAAAABEA#####wACVkQABDEwMDAAAAABQI9AAAAAAAAAAAARAP####8AC3Bvc1RhdXhQbHVzAAExAAAAAT#wAAAAAAAAAAAAEQD#####AAxwb3NUYXV4TW9pbnMAATIAAAABQAAAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAQAAAAAAAAAAAAAADgAAAEIAAAARAP####8ABHRhdXgAAy0yNQAAABwAAAABQDkAAAAAAAAAAAARAP####8ABHRlc3QABnRhdXg+MAAAAA0FAAAADgAAAEUAAAABAAAAAAAAAAAAAAARAP####8ACnZhbEFic1RhdXgACWFicyh0YXV4Kf####8AAAACAAlDRm9uY3Rpb24AAAAADgAAAEX#####AAAAAgAGQ0xhdGV4AP####8AAAAAAMAIAAAAAAAAwCgAAAAAAAAAAABEEAAAAAAAAQAAAAEAb1xJZnt0ZXN0fXtcdGltZXNcbGVmdCgxK1xmcmFje1xWYWx7dmFsQWJzVGF1eH19ezEwMH1ccmlnaHQpfXtcdGltZXNcbGVmdCgxLVxmcmFje1xWYWx7dmFsQWJzVGF1eH19ezEwMH1ccmlnaHQpff####8AAAACAAxDRHJvaXRlUGFyRXEA#####wEAAAAAEAAAAQABAAAACgAAAA0I#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAFAEgAAAAAAAAAAAA0BAAAAIQAAAAAAAAABQBIAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAUAEAAAAAAAAAAAAAQAAAAAAAAAAAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAGgAAAAAAAAAAAAEAAAAAAAAAAAAAAAYA#####wEAAAAAEAAAAQABAAAANwAAAAX#####AAAAAgARQ01hcnF1ZUFuZ2xlRHJvaXQA#####wEAAAAAAQAAAAFAIgAAAAAAAAAAAEz#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAASQAAAEwAAAAHAP####8BAAAAAAEAAABOAAAASwAAAB0A#####wAAAAAAAQAAAABAWMeJQp1DTAAAAEoAAABOAAAASwAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAUASAAAAAAAAAAAAAT#wAAAAAAAAAAAAEQD#####AAV0YXV4MgACMTIAAAABQCgAAAAAAAAAAAARAP####8ABXRlc3QyAAd0YXV4Mj4wAAAADQUAAAAOAAAAUgAAAAEAAAAAAAAAAAAAABEA#####wALdmFsQWJzVGF1eDIACmFicyh0YXV4MikAAAAeAAAAAA4AAABSAAAAHwD#####AAAAAAA#8AAAAAAAAMAoAAAAAAAAAAAAURAAAAAAAAEAAAABAHJcSWZ7dGVzdDJ9e1x0aW1lc1xsZWZ0KDErXGZyYWN7XFZhbHt2YWxBYnNUYXV4Mn19ezEwMH1ccmlnaHQpfXtcdGltZXNcbGVmdCgxLVxmcmFje1xWYWx7dmFsQWJzVGF1eDJ9fXsxMDB9XHJpZ2h0KX0AAAAgAP####8BAAAAABAAAAEAAQAAAAoAAAANCAAAACEAAAAAAAAAAUASAAAAAAAAAAAADQEAAAAhAAAAAAAAAAFAEgAAAAAAAAAAAAQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFAMUWBtpPS+gAAAFYAAAAGAP####8BAAAAABAAAAEAAQAAADUAAAACAAAABAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBLgo9cKPXIAAAAWAAAAAcA#####wEAAAAAAQAAAFcAAABZAAAABgD#####AQAAAAAQAAABAAEAAABLAAAAAgAAACAA#####wEAAAAAEAAAAQABAAAACgAAAA0IAAAAIQAAAAAAAAABQCYAAAAAAAAAAAANAQAAACEAAAAAAAAAAUAmAAAAAAAAAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAEgAAAAAAAAAAABwAAAABQApmZmZmZmYAAAAfAP####8AAAAAAMAUAAAAAAAAP#AAAAAAAAAAAABdEAAAAAAAAQAAAAEAC1x0aW1lcygxK3Qp#####wAAAAEAD0NTeW1ldHJpZUF4aWFsZQD#####AAAASQAAAA8A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAEQAAABf#####wAAAAEADENEcm9pdGVJbWFnZQD#####AQAAAAAQAAABAAEAAAAFAAAAXwAAACMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAEwAAABhAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAHAAAAAAAAAAAAAEAAAAAAAAAAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQCYAAAAAAAAAAAABAAAAAAAAAAAAAAAHAP####8BAAAAAAEAAABiAAAAYwAAAB0A#####wAAAAAAAQAAAAFAWKotVOL1EAAAAGMAAABiAAAAZAAAAAAGAP####8BAAAAABAAAAEAAQAAAFkAAABY#####wAAAAEAI0NBdXRyZVBvaW50SW50ZXJzZWN0aW9uRHJvaXRlQ2VyY2xlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAABaAAAAZwAAAFn#####AAAAAgAaQ01hcnF1ZUFuZ2xlT3JpZW50ZURpcmVjdGUA#####wAAAAAAAQAAAAFAg7ZtuCz87QAAAFkAAABXAAAAaAAAAAARAP####8ABXRhdXgzAAQtMS4yAAAAHAAAAAE#8zMzMzMzMwAAABEA#####wAFdGVzdDMAB3RhdXgzPjAAAAANBQAAAA4AAABqAAAAAQAAAAAAAAAAAAAAEQD#####AAt2YWxBYnNUYXV4MwAKYWJzKHRhdXgzKQAAAB4AAAAADgAAAGoAAAAfAP####8AAAAAAMAAAAAAAAAAwCYAAAAAAAAAAABgEAAAAAAAAQAAAAEAclxJZnt0ZXN0M317XHRpbWVzXGxlZnQoMStcZnJhY3tcVmFse3ZhbEFic1RhdXgzfX17MTAwfVxyaWdodCl9e1x0aW1lc1xsZWZ0KDEtXGZyYWN7XFZhbHt2YWxBYnNUYXV4M319ezEwMH1ccmlnaHQpfQAAABsA#####wAAAAAAv#AAAAAAAABAGAAAAAAAAAAAAGQQAAAAAAABAAAAAAARVmFsZXVyCmQnYXJyaXbDqWUAAAAO##########8='
          : 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV7AAADnAAAAQEAAAAAAAAAAQAAAHr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAFAAFAWmeuFHrhSEBUgUeuFHri#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAAQ4AAUkAwBgAAAAAAAAAAAAAAAAAAAUAAUA#YUeuFHrgAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAQAAAAP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAABAAAABP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQUAAQAAAAcAAAAJAP####8BAAAAAQ4AAUoAwCgAAAAAAADAEAAAAAAAAAUAAgAAAAf#####AAAAAgAHQ1JlcGVyZQD#####Aebm5gABAAAAAQAAAAMAAAAJAQEAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgDALAAAAAAAAEAAAAAAAAAAAQAAAAADAAAADP####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAAN#####wAAAAEAB0NDYWxjdWwA#####wAHbmJncmFkeAACMjAAAAABQDQAAAAAAAAAAAARAP####8AB25iZ3JhZHkAAjIwAAAAAUA0AAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AFEdyYWR1YXRpb25BeGVzUmVwZXJlAAAAGwAAAAgAAAADAAAACgAAAA8AAAAQ#####wAAAAEAE0NBYnNjaXNzZU9yaWdpbmVSZXAAAAAAEQAFYWJzb3IAAAAK#####wAAAAEAE0NPcmRvbm5lZU9yaWdpbmVSZXAAAAAAEQAFb3Jkb3IAAAAKAAAACwAAAAARAAZ1bml0ZXgAAAAK#####wAAAAEACkNVbml0ZXlSZXAAAAAAEQAGdW5pdGV5AAAACv####8AAAABABBDUG9pbnREYW5zUmVwZXJlAAAAABEAAAAAABAAAAEFAAAAAAoAAAAOAAAAEgAAAA4AAAATAAAAFgAAAAARAAAAAAAQAAABBQAAAAAKAAAADQAAAAAOAAAAEgAAAA4AAAAUAAAADgAAABMAAAAWAAAAABEAAAAAABAAAAEFAAAAAAoAAAAOAAAAEgAAAA0AAAAADgAAABMAAAAOAAAAFQAAAAwAAAAAEQAAABYAAAAOAAAADwAAAA8AAAAAEQAAAAAAEAAAAQUAAAAAFwAAABkAAAAMAAAAABEAAAAWAAAADgAAABAAAAAPAAAAABEAAAAAABAAAAEFAAAAABgAAAAb#####wAAAAEACENTZWdtZW50AAAAABEBAAAAABAAAAEAAQAAABcAAAAaAAAAFwAAAAARAQAAAAAQAAABAAEAAAAYAAAAHAAAAAQAAAAAEQEAAAAACwABVwDAFAAAAAAAAMA0AAAAAAAABQABP9xWeJq83w4AAAAd#####wAAAAIACENNZXN1cmVYAAAAABEABnhDb29yZAAAAAoAAAAfAAAAEQAAAAARAAVhYnN3MQAGeENvb3JkAAAADgAAACD#####AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAABEBZmZmAAAAHwAAAA4AAAAPAAAAHwAAAAIAAAAfAAAAHwAAABEAAAAAEQAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABIAAAAOAAAAIQAAABYAAAAAEQEAAAAAEAAAAQUAAAAACgAAAA4AAAAjAAAADgAAABMAAAAZAQAAABEBZmZmAAAAJAAAAA4AAAAPAAAAHwAAAAUAAAAfAAAAIAAAACEAAAAjAAAAJAAAAAQAAAAAEQEAAAAACwABUgBAIAAAAAAAAMAgAAAAAAAABQABP9EbToG06B8AAAAe#####wAAAAIACENNZXN1cmVZAAAAABEABnlDb29yZAAAAAoAAAAmAAAAEQAAAAARAAVvcmRyMQAGeUNvb3JkAAAADgAAACcAAAAZAQAAABEBZmZmAAAAJgAAAA4AAAAQAAAAJgAAAAIAAAAmAAAAJgAAABEAAAAAEQAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABMAAAAOAAAAKAAAABYAAAAAEQEAAAAAEAAAAQUAAAAACgAAAA4AAAASAAAADgAAACoAAAAZAQAAABEBZmZmAAAAKwAAAA4AAAAQAAAAJgAAAAUAAAAmAAAAJwAAACgAAAAqAAAAK#####8AAAACAAxDQ29tbWVudGFpcmUAAAAAEQFmZmYAP#AAAAAAAABAHAAAAAAAAAAAAB8LAAH###8AAAABAAAAAAALI1ZhbChhYnN3MSkAAAAZAQAAABEBZmZmAAAALQAAAA4AAAAPAAAAHwAAAAQAAAAfAAAAIAAAACEAAAAtAAAAGwAAAAARAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAJAsAAf###wAAAAEAAAAAAAsjVmFsKGFic3cyKQAAABkBAAAAEQFmZmYAAAAvAAAADgAAAA8AAAAfAAAABgAAAB8AAAAgAAAAIQAAACMAAAAkAAAALwAAABsAAAAAEQFmZmYAwCAAAAAAAAA#8AAAAAAAAAAAACYLAAH###8AAAACAAAAAQALI1ZhbChvcmRyMSkAAAAZAQAAABEBZmZmAAAAMQAAAA4AAAAQAAAAJgAAAAQAAAAmAAAAJwAAACgAAAAxAAAAGwAAAAARAWZmZgDAHAAAAAAAAAAAAAAAAAAAAAAAKwsAAf###wAAAAIAAAABAAsjVmFsKG9yZHIyKQAAABkBAAAAEQFmZmYAAAAzAAAADgAAABAAAAAmAAAABgAAACYAAAAnAAAAKAAAACoAAAArAAAAMwAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAr#####AAAAAQAMQ01vaW5zVW5haXJlAAAAAUAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAQA#####wEAAAAAEAAAAQUAAUADYWhCkd28AAAABQAAAAcA#####wEAAAAAAQAAADcAAAA1#####wAAAAIAHENNYXJxdWVBbmdsZU9yaWVudGVJbmRpcmVjdGUA#####wAAAAAAAQAAAAFAWIpOB+gIDAAAADUAAAA3AAAANgAAAAAbAP####8AAAAAAAAAAAAAAAAAQBQAAAAAAAAAAAA1EAAAAAAAAQAAAAAAEVZhbGV1cgpkZSBkw6lwYXJ0AAAACAD#####AAAABQAAADgAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAOwAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAAA7AAAAEQD#####AAVwb3NWQQACLTEAAAAcAAAAAT#wAAAAAAAAAAAAEQD#####AAVwb3NWRAACLTEAAAAcAAAAAT#wAAAAAAAAAAAAEQD#####AAJWQQAEMTIwMAAAAAFAksAAAAAAAAAAABEA#####wACVkQABDEwMDAAAAABQI9AAAAAAAAAAAARAP####8AC3Bvc1RhdXhQbHVzAAExAAAAAT#wAAAAAAAAAAAAEQD#####AAxwb3NUYXV4TW9pbnMAATIAAAABQAAAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAQAAAAAAAAAAAAAADgAAAEIAAAARAP####8ABHRhdXgAAy0yNQAAABwAAAABQDkAAAAAAAAAAAARAP####8ABHRlc3QABnRhdXg+MAAAAA0FAAAADgAAAEUAAAABAAAAAAAAAAAAAAARAP####8ACnZhbEFic1RhdXgACWFicyh0YXV4Kf####8AAAACAAlDRm9uY3Rpb24AAAAADgAAAEX#####AAAAAgAGQ0xhdGV4AP####8AAAAAAMAIAAAAAAAAwCgAAAAAAAAAAABEEAAAAAAAAQAAAAEAb1xJZnt0ZXN0fXtcdGltZXNcbGVmdCgxK1xmcmFje1xWYWx7dmFsQWJzVGF1eH19ezEwMH1ccmlnaHQpfXtcdGltZXNcbGVmdCgxLVxmcmFje1xWYWx7dmFsQWJzVGF1eH19ezEwMH1ccmlnaHQpff####8AAAACAAxDRHJvaXRlUGFyRXEA#####wEAAAAAEAAAAQABAAAACgAAAA0I#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAFAEgAAAAAAAAAAAA0BAAAAIQAAAAAAAAABQBIAAAAAAAAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAUAEAAAAAAAAAAAAAQAAAAAAAAAAAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAGgAAAAAAAAAAAAEAAAAAAAAAAAAAAAYA#####wEAAAAAEAAAAQABAAAANwAAAAX#####AAAAAgARQ01hcnF1ZUFuZ2xlRHJvaXQA#####wEAAAAAAQAAAAFAIgAAAAAAAAAAAEz#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAASQAAAEwAAAAHAP####8BAAAAAAEAAABOAAAASwAAAB0A#####wAAAAAAAQAAAABAWMeJQp1DTAAAAEoAAABOAAAASwAAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAUASAAAAAAAAAAAAAT#wAAAAAAAAAAAAEQD#####AAV0YXV4MgACMTIAAAABQCgAAAAAAAAAAAARAP####8ABXRlc3QyAAd0YXV4Mj4wAAAADQUAAAAOAAAAUgAAAAEAAAAAAAAAAAAAABEA#####wALdmFsQWJzVGF1eDIACmFicyh0YXV4MikAAAAeAAAAAA4AAABSAAAAHwD#####AAAAAAA#8AAAAAAAAMAoAAAAAAAAAAAAURAAAAAAAAEAAAABAHJcSWZ7dGVzdDJ9e1x0aW1lc1xsZWZ0KDErXGZyYWN7XFZhbHt2YWxBYnNUYXV4Mn19ezEwMH1ccmlnaHQpfXtcdGltZXNcbGVmdCgxLVxmcmFje1xWYWx7dmFsQWJzVGF1eDJ9fXsxMDB9XHJpZ2h0KX0AAAAgAP####8BAAAAABAAAAEAAQAAAAoAAAANCAAAACEAAAAAAAAAAUAbAAAAAAAAAAAADQEAAAAhAAAAAAAAAAFAGwAAAAAAAAAAAAQA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFAMUWBtpPS+gAAAFYAAAAGAP####8BAAAAABAAAAEAAQAAADUAAAACAAAABAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBLgo9cKPXIAAAAWAAAAAcA#####wEAAAAAAQAAAFcAAABZAAAABgD#####AQAAAAAQAAABAAEAAABLAAAAAgAAACAA#####wEAAAAAEAAAAQABAAAACgAAAA0IAAAAIQAAAAAAAAABQCYAAAAAAAAAAAANAQAAACEAAAAAAAAAAUAmAAAAAAAAAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAGwAAAAAAAAAAABwAAAABQA4AAAAAAAAAAAAfAP####8AAAAAAMAUAAAAAAAAP#AAAAAAAAAAAABdEAAAAAAAAQAAAAEAC1x0aW1lcygxK3Qp#####wAAAAEAD0NTeW1ldHJpZUF4aWFsZQD#####AAAASQAAAA8A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAEQAAABf#####wAAAAEADENEcm9pdGVJbWFnZQD#####AQAAAAAQAAABAAEAAAAFAAAAXwAAACMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAEwAAABhAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAHAAAAAAAAAAAAAEAAAAAAAAAAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQCYAAAAAAAAAAAABAAAAAAAAAAAAAAAHAP####8BAAAAAAEAAABiAAAAYwAAAB0A#####wAAAAAAAQAAAAFAWKotVOL1EAAAAGMAAABiAAAAZAAAAAAGAP####8BAAAAABAAAAEAAQAAAFkAAABY#####wAAAAEAI0NBdXRyZVBvaW50SW50ZXJzZWN0aW9uRHJvaXRlQ2VyY2xlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAABaAAAAZwAAAFn#####AAAAAgAaQ01hcnF1ZUFuZ2xlT3JpZW50ZURpcmVjdGUA#####wAAAAAAAQAAAAFAhD2y+SZtEAAAAFkAAABXAAAAaAAAAAARAP####8ABXRhdXgzAAQtMS4yAAAAHAAAAAE#8zMzMzMzMwAAABEA#####wAFdGVzdDMAB3RhdXgzPjAAAAANBQAAAA4AAABqAAAAAQAAAAAAAAAAAAAAEQD#####AAt2YWxBYnNUYXV4MwAKYWJzKHRhdXgzKQAAAB4AAAAADgAAAGoAAAAfAP####8AAAAAAMAAAAAAAAAAwCYAAAAAAAAAAABgEAAAAAAAAQAAAAEAclxJZnt0ZXN0M317XHRpbWVzXGxlZnQoMStcZnJhY3tcVmFse3ZhbEFic1RhdXgzfX17MTAwfVxyaWdodCl9e1x0aW1lc1xsZWZ0KDEtXGZyYWN7XFZhbHt2YWxBYnNUYXV4M319ezEwMH1ccmlnaHQpfQAAACAA#####wEAAAAAEAAAAQABAAAACgAAAA0IAAAAIQAAAAAAAAABQCsAAAAAAAAAAAANAQAAACEAAAAAAAAAAUArAAAAAAAAAAAAIwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAATAAAAG4AAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAUAnAAAAAAAAAAAAAQAAAAAAAAAAAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFALwAAAAAAAAAAAAEAAAAAAAAAAAAAAAcA#####wEAAAAAAQAAAG8AAABwAAAAHQD#####AAAAAAABAAAAAUBYvW8PEv9TAAAAcAAAAG8AAABxAAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQCsAAAAAAAAAAAABP#AAAAAAAAAAAAARAP####8ABXRhdXg0AAIxNAAAAAFALAAAAAAAAAAAABEA#####wAFdGVzdDQAB3RhdXg0PjAAAAANBQAAAA4AAAB1AAAAAQAAAAAAAAAAAAAAEQD#####AAt2YWxBYnNUYXV4NAAKYWJzKHRhdXg0KQAAAB4AAAAADgAAAHUAAAAfAP####8AAAAAAMAIAAAAAAAAwCQAAAAAAAAAAAB0EAAAAAAAAQAAAAEAclxJZnt0ZXN0NH17XHRpbWVzXGxlZnQoMStcZnJhY3tcVmFse3ZhbEFic1RhdXg0fX17MTAwfVxyaWdodCl9e1x0aW1lc1xsZWZ0KDEtXGZyYWN7XFZhbHt2YWxBYnNUYXV4NH19ezEwMH1ccmlnaHQpfQAAABsA#####wAAAAABAAAAcRAAAAAAAAEAAAAAABFWYWxldXIKZCdhcnJpdsOpZQAAAA7##########w=='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFig, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true);
  }

  function modifFig (tabTaux) {
    // cette fonction sert à modifier la figure de base
    // num est le numéro de la question (1 ou 3) selon qu’on cherche la valeur finale ou la valeur initiale
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'taux', String(tabTaux[0]))
    for (let i = 2; i <= tabTaux.length; i++) {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'taux' + i, String(tabTaux[i - 1]))
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.obj = {}// objet contenant comme propriétés les variables
    // Ce qui précède permet de choisir le num de sujet en étant sûr de ne pas récupérer le même énoncé (que le précédent)*;
    if (stor.tabSujet.length === 0) stor.tabSujet = [1, 2, 3, 4]
    const alea = j3pGetRandomInt(0, (stor.tabSujet.length - 1))
    const choixSujet = stor.tabSujet[alea]
    stor.tabSujet.splice(alea, 1)
    let taux2, laReponse, repExacte, corrListe, consigneQuest
    if (ds.deuxEvolutions) {
      // on détermine les taux de manière aléatoire
      const taux1 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(35, 75) / 10
      do {
        taux2 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(35, 75) / 10
      } while (Math.abs(Math.abs(taux1) - Math.abs(taux2)) < 1.5)
      stor.obj.a = j3pVirgule(Math.abs(taux1))
      stor.obj.b = j3pVirgule(Math.abs(taux2))
      stor.obj.c = (taux1 > 0) ? textes.choix1 : textes.choix2
      stor.obj.d = (taux2 > 0) ? textes.choix1 : textes.choix2
      stor.obj.styletexte = {}
      j3pAffiche(stor.zoneCons1, '', textes['consigne' + choixSujet], stor.obj)
      // Pour la question, j’ai un input et une liste
      laReponse = j3pArrondi((1 + taux1 / 100) * (1 + taux2 / 100) - 1, 7) * 100
      repExacte = (Math.abs(laReponse * 100 - Math.round(laReponse * 100)) < Math.pow(10, -12))
      stor.obj.env = (repExacte) ? textes.txt2 : textes.txt1
      stor.obj.liste1 = { texte: ['', textes.choix1, textes.choix2] }
      stor.obj.inputmq1 = { texte: '' }
      corrListe = (repExacte) ? (laReponse > 0) ? 1 : 2 : (laReponse > 0) ? 1 : 2
      consigneQuest = (choixSujet <= 2) ? textes.consigne10_1 : textes.consigne10_2
      const eltsZone2 = j3pAffiche(stor.zoneCons2, '', consigneQuest, stor.obj)
      stor.inputMqZone = eltsZone2.inputmqList[0]
      stor.selectZone = eltsZone2.selectList[0]
      stor.taux1 = taux1
      stor.taux2 = taux2
      stor.evolutions = [taux1, taux2]
    } else {
      const nbEvol = j3pGetRandomInt(3, 4)
      stor.obj.annee = 2013 + nbEvol
      j3pAffiche(stor.zoneCons1, '', textes['consignebis' + choixSujet], stor.obj)
      stor.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
      stor.annees = [2014]
      for (let i = 1; i < nbEvol; i++) stor.annees.push(2014 + i)
      stor.evolutions = []
      stor.evolutions.push((j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(30, 85) / 10)
      for (let i = 2; i <= nbEvol; i++) {
        let tauxAcceptable
        do {
          taux2 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(30, 85) / 10
          tauxAcceptable = true
          for (let j = 1; j <= i - 1; j++) {
            tauxAcceptable = (tauxAcceptable && (Math.abs(Math.abs(stor.evolutions[j - 1]) - Math.abs(taux2)) > 0.8))
          }
        } while (!tauxAcceptable)
        stor.evolutions.push(taux2)
      }
      const tabTaux = []
      for (let i = 1; i <= nbEvol; i++) {
        if (stor.evolutions[i - 1] > 0) {
          tabTaux.push('+' + stor.evolutions[i - 1] + ' %')
        } else {
          tabTaux.push(stor.evolutions[i - 1] + ' %')
        }
      }
      // console.log("stor.evolutions:",stor.evolutions)
      const objValeurs = {
        parent: stor.zoneCons2,
        textes: [textes.tableau1, textes.tableau2],
        valeurs: stor.annees,
        evolutions: tabTaux,
        largeur: stor.largeurTab
      }
      tableauDonnees(objValeurs)

      // Pour la question, j’ai un input et une liste
      laReponse = (1 + stor.evolutions[0] / 100)
      for (let i = 1; i < stor.evolutions.length; i++) laReponse = laReponse * (1 + stor.evolutions[i] / 100)
      laReponse = (laReponse - 1) * 100
      repExacte = (Math.abs(laReponse * 100 - Math.round(laReponse * 100)) < Math.pow(10, -12))
      stor.obj.env = (repExacte) ? textes.txt2 : textes.txt1
      stor.obj.liste1 = { texte: ['', textes.choix1, textes.choix2] }
      stor.obj.inputmq1 = { texte: '' }
      corrListe = (repExacte) ? (laReponse > 0) ? 1 : 2 : (laReponse > 0) ? 1 : 2
      consigneQuest = (choixSujet <= 2) ? textes.consignebis10_1 : textes.consignebis10_2
      stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div')
      const eltsZone2 = j3pAffiche(stor.zoneCons3, '', consigneQuest, stor.obj)
      stor.inputMqZone = eltsZone2.inputmqList[0]
      stor.selectZone = eltsZone2.selectList[0]
    }
    stor.selectZone.reponse = [corrListe]
    stor.selectZone.typeReponse = [true]
    if (repExacte) {
      stor.inputMqZone.typeReponse = ['nombre', 'exact']
    } else {
      const divArrondi = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(divArrondi, '', textes.consigne11, stor.obj)
      stor.inputMqZone.typeReponse = ['nombre', 'arrondi', 0.1]
    }
    stor.inputMqZone.reponse = [Math.abs(laReponse)]
    mqRestriction(stor.inputMqZone, '\\d,.')
    const mesZonesSaisie = [stor.selectZone.id, stor.inputMqZone.id]
    stor.choixSujet = choixSujet
    stor.repExacte = repExacte
    stor.laReponse = laReponse
    me.logIfDebug('laReponse:', laReponse, '   repExacte:', repExacte, '   obj.env:', stor.obj.env)
    j3pFocus(stor.inputMqZone)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    const zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(zoneMD, 'div')
    stor.zoneCorr = j3pAddElt(zoneMD, 'div')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70

        me.construitStructurePage({ structure: 'presentation1', ratioGauche: stor.pourcentage / 100 })
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        stor.tabSujet = [1, 2, 3, 4]

        // chargement mtg
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        let pbArrondi = false
        let ajoutTaux = false
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = stor.fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si les 4 zones ont bien été complétées ou non
        // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
        // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
        // Si c’est la dernière tentative de répondre, on barre également les zones fausses

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            let reponseSaisie = stor.fctsValid.zones.reponseSaisie[1]
            reponseSaisie = j3pNombre(reponseSaisie)
            const typeArrondi = stor.inputMqZone.typeReponse[1] === 'arrondi'
            const leRepAttendue = stor.inputMqZone.reponse[0]
            if (typeArrondi) {
            // On vérifie si c’est une valeur approchée
              pbArrondi = (Math.abs(leRepAttendue - reponseSaisie) < stor.larrondi * 10)
            }
            if (ds.deuxEvolutions) {
              ajoutTaux = (Math.abs(Math.abs(stor.taux1 + stor.taux2) - reponseSaisie) < Math.pow(10, -2))
            } else {
              let sommeTaux = stor.evolutions[0]
              for (let i = 1; i < stor.evolutions.length; i++) {
                sommeTaux += stor.evolutions[i]
              }
              // console.log("sommeTaux:",sommeTaux)
              ajoutTaux = (Math.abs(Math.abs(sommeTaux) - reponseSaisie) < Math.pow(10, -2))
            }
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (pbArrondi) {
                stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
                stor.zoneCorr.innerHTML += '<br/>' + textes.comment2
              } else if (ajoutTaux) {
                stor.zoneCorr.innerHTML += '<br/>' + textes.comment3
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)
      break // case "navigation":
  }
}
