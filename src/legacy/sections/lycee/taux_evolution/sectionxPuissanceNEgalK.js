import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pNombre, j3pPaletteMathquill, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import { afficheBloc, phraseBloc } from 'src/legacy/core/functionsPhrase'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { mqRestriction } from 'src/lib/mathquill/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Mettre ici une description de la section
 * @author DENIAUD Rémi
 * @since juillet 2022
 * @fileOverview Résolution d’une équation de la forme x^n=k
 */
// nos constantes
const structure = 'presentation1'
// const ratioGauche = 0.75 C’est la valeur par défaut du ratio de la partie gauche dans presentation1
// N’utiliser cette variable que si ce ratio est différent
// (utilisé dans ce cas dans me.construitStructurePage(structure, ratioGauche) de initSection()

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition']
    // les paramètres peuvent être du type 'entier', 'string', 'boolean', 'liste'
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titreExo: 'Equation x^n=k',
  // on donne les phrases de la consigne
  consigne1: 'L’unique solution positive de l’équation $x^{£n}=£k$ vaut &1& (valeur exacte et simplifiée), ce qui, une fois arrondi à 0,001 donne environ &2&.',
  consigne2: 'Tu n’as qu’une seule tentative pour répondre.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élève
  comment1: 'Peut-être est-ce un problème d’arrondi&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'D’après le cours, pour tous réels positifs $x$ et $k$ et tout entier naturel supérieur ou égal à 2, l’équation $x^n=k$ équivaut à $x=k^{\\frac{1}{n}}$.',
  corr2: 'Donc la solution de l’équation $x^{£n}=£k$ est $£k^{\\frac{1}{£n}}$ qui vaut environ £s.'
}
/**
 * La fonction principale, rappelée à chaque étape
 * Son this sera l’instance courante de Parcours
 * @this {Parcours}
 */
export default function maSection () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini (en général dans enonceEnd(me)), sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance courante de Parcours (objet this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  const stor = me.storage
  // On surcharge avec les parametres passés par le graphe
  // Construction de la page
  me.construitStructurePage(structure) // ajouter un deuxième argument ratioGauche s’il est défini en tant que variable globale au début
  me.afficheTitre(textes.titreExo)
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone

  // ou stocker des variables, par exemples celles qui serviraient pour la pe

  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}
/**
 * Retourne les données de l’exercice
 * @private
 * @return {Object}
 */
function genereAlea () {
  /**
   * données de l’exercice qui peuvent être les variables présentes dans les textes (consignes ou corrections)
   * ou autre données utiles à la validation
   * @private
   * @type {Object} obj
   */
  const obj = {}
  obj.n = j3pGetRandomInt(4, 7)
  do {
    obj.kNb = j3pGetRandomInt(124, 234) / 100
    obj.sol = Math.pow(obj.kNb, 1 / obj.n)
    // Je ne veux pas tomber sur une valeur exacte
  } while (Math.abs(obj.sol - Math.round(obj.sol * 100) / 100) < Math.pow(10, -10))
  obj.k = j3pVirgule(obj.kNb)
  obj.s = j3pVirgule(Math.round(1000 * obj.sol) / 1000)
  return obj
}
/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // on affiche l’énoncé
  // La style de base (ici petit.enonce) peut être complété par d’autres propriétés css. C’est fait ici avec j3pStyle
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.enonce })
  j3pStyle(stor.elts.divEnonce, { padding: '10px' })
  // On génère les données de l’exo (stockées dans un objet dont on se sert entre autre pour les phrases de l’énoncé)
  stor.objVariables = genereAlea()
  // On écrit la première phrase (dans un bloc pour assurer un retour à la ligne)
  const eltCons1 = afficheBloc(stor.elts.divEnonce, textes.consigne1, stor.objVariables)
  if (ds.nbchances === 1) {
    const phrase2 = phraseBloc(stor.elts.divEnonce, textes.consigne2)
    j3pStyle(phrase2, { paddingTop: '10px', fontStyle: 'italic', fontSize: '0.9em' })
  }
  stor.zonesInput = [...eltCons1.inputmqList]
  stor.zonesInput[1].typeReponse = ['nombre', 'arrondi', 0.001]
  stor.zonesInput[1].reponse = [stor.objVariables.sol]
  stor.tabPalette = ['fraction', 'puissance']
  mqRestriction(stor.zonesInput[0], '\\d^+/-,.', { commandes: stor.tabPalette })
  mqRestriction(stor.zonesInput[1], '\\d,.')
  stor.elts.laPalette = j3pAddElt(stor.elts.divEnonce, 'div')
  j3pStyle(stor.elts.laPalette, { paddingTop: '10px' })
  j3pPaletteMathquill(stor.elts.laPalette, stor.zonesInput[0], { liste: stor.tabPalette })
  stor.zonesInput.forEach((zone, index) => { zone.addEventListener('focusin', ecoute.bind(null, stor, index)) })
  stor.fctsValid = new ValidationZones({
    parcours: me,
    zones: stor.zonesInput,
    validePerso: [stor.zonesInput[0]]
  })
  j3pFocus(stor.zonesInput[0])
  // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvent dans le cadre MD)
  // Il n’est ainsi construit qu’une seule fois et vidé à chaque passage dans correction()
  stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.moyen.correction })
  j3pStyle(stor.zoneD, { padding: '10px' })
  stor.zoneCalc = j3pAddElt(stor.zoneD, 'div')
  stor.zoneCorr = j3pAddElt(stor.zoneD, 'div')
  stor.calc = j3pGetNewId('Calculatrice')
  j3pStyle(stor.zoneCorr, { paddingTop: '10px' })
  me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
  j3pCreeFenetres(me)
  j3pAjouteBouton(stor.zoneCalc, j3pToggleFenetres.bind(null, 'Calculatrice'), { id: stor.calc, className: 'MepBoutons', value: 'Calculatrice' })
  j3pAfficheCroixFenetres('Calculatrice')
  // et on appelle finEnonce()
  me.finEnonce()
}
function ecoute (stor, num) {
  if (stor.zonesInput[num].className.includes('mq-editable-field')) {
    j3pEmpty(stor.elts.laPalette)
    if (num === 0) j3pPaletteMathquill(stor.elts.laPalette, stor.zonesInput[num], { liste: stor.tabPalette })
  }
}
function afficheCorrection (me, bonneReponse = false) {
  const stor = me.storage
  j3pDetruit(stor.zoneCalc)
  j3pDetruitFenetres('Calculatrice')
  const zoneExpli = j3pAddElt(stor.elts.divEnonce, 'div', '', { style: me.styles.petit.correction })
  // La couleur de la correction sera fonction de la qualité de la réponse de l’élève
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  afficheBloc(zoneExpli, textes.corr1, stor.objVariables)
  afficheBloc(zoneExpli, textes.corr2, stor.objVariables)
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number|null}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  if (reponse.aRepondu) {
    // Il faut que je teste la première zone
    const puisRegExp = /^(\d+,?\d*)\^\{(.+)}$/g
    const fracRegExp = /^\\frac\{(\d+)}\{(\d+)}$/g
    const decimalRedExp = /^\d+,?\d*$/g
    const rep0 = stor.fctsValid.zones.reponseSaisie[0]
    const chunks = puisRegExp.exec(rep0)
    let bonneRep1
    if (chunks) {
      // J’ai trouvé une puissance (chunks[2])
      const chunks2 = fracRegExp.exec(chunks[2])
      const chunks3 = decimalRedExp.exec(chunks[2])
      if (chunks3) {
        // Puissance décimale, je vérifie si elle est égale à 1/n
        bonneRep1 = Math.abs(j3pNombre(chunks3[0]) - 1 / stor.objVariables.n) < 1e-12
      } else if (chunks2) {
        // Puissance fractionnaire, je vérifie si elle est égale à 1/n
        bonneRep1 = (chunks2[1] === '1') && (chunks2[2] === String(stor.objVariables.n))
      } else {
        bonneRep1 = false
      }
      if (bonneRep1) {
        // Je vérifie aussi que le nombre élevé à la puissance est le bon
        bonneRep1 = (Math.abs(stor.objVariables.kNb - j3pNombre(chunks[1])) < Math.pow(10, -12))
      }
    } else {
      bonneRep1 = false
    }
    stor.fctsValid.zones.bonneReponse[0] = bonneRep1
    stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
    reponse.bonneReponse = (reponse.bonneReponse && bonneRep1)
    if (reponse.bonneReponse) return 1
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
}

function correction (me) {
  const stor = me.storage
  const ds = me.donneesSection
  const score = getScore(me) // null si pas répondu
  if ((score === 1) || (me.essaiCourant >= ds.nbchances)) me.score += score
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.zoneCorr)
    return
  }
  if (score !== 1) {
    // On écrit c’est faux
    me.reponseKo(stor.zoneCorr)
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if ((score !== 1) && me.essaiCourant < me.donneesSection.nbchances) {
    me.reponseKo(stor.zoneCorr, '<br>' + essaieEncore, true)
    return
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.zoneCorr)
    // Même dans le cas d’une bonne réponse, on peut afficher la correction qui aurait dans ce cas d’éventuels compléments d’information
    afficheCorrection(me, true)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, tempsDepasse, true)
    }
    afficheCorrection(me)
  }
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) {
    me.etat = 'enonce'
  }
  // on laisse le bouton suite mis par le finCorrection() précédent
  me.finNavigation(true)

  // Si on a rien à afficher de particulier après la dernière répétition, donc on passe true à finNavigation
  // pour que le modèle affiche directement le bouton "section suivante" (sans devoir cliquer sur 'suite' avant)
  // me.finNavigation(true)
}
