import { j3pAddElt, j3pAjouteBouton, j3pChaine, j3pDetruit, j3pFocus, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pRemplacePoint, j3pShuffle, j3pShuffleMulti, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mars 2019
        Dans cette section, on donne une formule et on demande ce qu’elle devient quand on la recopie vers la droite ou vers le bas
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['avecDollar', false, 'boolean', 'Si ce paramètre vaut false (ce qui est le cas par défaut), alors il n’y aura pas besoin du symbole dollar dans la formule.'],
    ['doubleDollar', false, 'boolean', 'Si avecDollar vaut true, alors si ce paramètre vaut true, on peut proposer des cellules de la formule $B$2 dans une formule.']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Recopie de formules',
  // on donne les phrases de la consigne
  consigne1: 'Dans une feuille de calcul, on écrit dans la cellule £c la formule : <br>£f',
  consigne2_1: 'On recopie cette formule vers la droite. Quelle formule est alors écrite dans la cellule £{c2}&nbsp;?',
  consigne2_2: 'On recopie cette formule vers le bas. Quelle formule est alors écrite dans la cellule £{c2}&nbsp;?',
  consigne3: 'Formule : @1@',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Attention : quelle est la première chose à écrire pour une formule du tableur&nbsp;?',
  comment2: 'Il y a un souci dans l’utilisation de la puissance dans la formule ! Cette réponse n’est pas prise en compte.',
  aide1: 'Aide',
  aide2: 'Feuille vide',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Quand on recopie une formule vers la droite, les références des cellules évoluent de la manière suivante :',
  corr1_2: 'Quand on recopie une formule vers le bas, les références des cellules évoluent de la manière suivante :',
  corr2_1: '- les lettres désignant les colonnes passent à la suivante, sauf si elles sont précédées d’un dollar&nbsp;;',
  corr3_1: '- les nombres désignant les lignes ne bougent pas.',
  corr2_2: '- les lettres désignant les colonnes ne bougent pas&nbsp;;',
  corr3_2: '- les nombres désignant les lignes augmentent, sauf s’ils sont précédés d’un dollar.',
  corr4: 'Dans notre exemple, la formule devient alors :<br/>£r'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function creationTab (obj) {
    // obj.eltHtml es l’élément HTML dans lequel construire la table
    // obj.largTableur correspond à la largeur totale de la table
    // on ne fait qu’afficher une feuille de calcul vide avec 4 lignes et 4 colonnes
    const largCol1 = 35
    const nbLignes = 4
    const nbColonnes = 4
    const largAutresColonnes = (obj.largTableur - largCol1) / nbColonnes
    const table = j3pAddElt(obj.eltHtml, 'table', { width: '100%', borderColr: 'black', border: 2, cellSpacing: 0, style: { fontSize: '0.8em' } })
    const ligne1 = j3pAddElt(table, 'tr')
    j3pAddElt(ligne1, 'th', { width: largCol1 + 'px', style: { backgroundColor: '#BABABA' } })
    for (let j = 0; j < nbColonnes; j++) {
      j3pAddElt(ligne1, 'th', String.fromCharCode(65 + j), { width: largAutresColonnes + 'px', style: { backgroundColor: '#BABABA' } })
    }
    for (let i = 0; i < nbLignes; i++) {
      const ligne = j3pAddElt(table, 'tr')
      j3pAddElt(ligne, 'th', String(i + 1), { width: largCol1 + 'px', style: { backgroundColor: '#BABABA' } })
      for (let j = 0; j < nbColonnes; j++) {
        j3pAddElt(ligne, 'td', { width: largAutresColonnes + 'px' })
      }
    }
    // $('th').css('background-color', '#BABABA')
    // $('table').css('font-size', '0.8em')
  }
  function formuleRecopiee (formule, direction) {
    // formule est la formule initiale
    // direction vaut "ligne" ou "colonne" suivant la façon dont on effectue la recopie
    const cellule = /\$?[A-Z]\$?[1-9]/g // new RegExp('\\$?[A-Z]{1}\\S?[1-9]{1}', 'g')// permet d’identifier la présence de la référence d’une cellule dans la formule
    const tabCellule = formule.match(cellule)
    let newCellule
    let newFormule = formule
    me.logIfDebug('formule avant:', formule)
    for (let i = 0; i < tabCellule.length; i++) {
      const num = (tabCellule[i][0] === '$') ? tabCellule[i][1].charCodeAt(0) - 65 : tabCellule[i][0].charCodeAt(0) - 65
      const nb = Number(tabCellule[i][tabCellule[i].length - 1])
      if (direction === 'ligne') {
        if (tabCellule[i][0] === '$') {
          newCellule = '$' + String.fromCharCode(num + 65) + tabCellule[i].substring(2)
        } else {
          newCellule = String.fromCharCode(num + 66) + tabCellule[i].substring(1)
        }
      } else {
        if (tabCellule[i][tabCellule[i].length - 2] === '$') {
          newCellule = (tabCellule[i][0] === '$') ? '$' : ''
          newCellule += String.fromCharCode(num + 65) + '$' + String(nb)
        } else {
          newCellule = tabCellule[i].substring(0, tabCellule[i].length - 1) + String(nb + 1)
        }
      }
      newFormule = newFormule.replace(tabCellule[i], newCellule)
    }
    return newFormule
  }
  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    j3pDetruit(stor.zoneBtn)
    j3pDetruitFenetres(stor.laFeuille)
    const numCorr = (stor.positionDonnees[me.questionCourante - 1] === 'ligne') ? 1 : 2
    for (let i = 1; i <= 3; i++) {
      const zoneExpli = j3pAddElt(stor.zoneExpli, 'div')
      j3pAffiche(zoneExpli, '', textes['corr' + i + '_' + numCorr])
    }
    const zoneExpli = j3pAddElt(stor.zoneExpli, 'div')
    j3pAffiche(zoneExpli, '', textes.corr4, { r: stor.zoneInput.reponse[0].replace(/\$/g, '&dollar;') })
  }

  function enonceMain () {
    const numCol = j3pGetRandomInt(1, 3)
    const numLigne = j3pGetRandomInt(2, 3)
    stor.cellule = {
      numCol,
      numLigne,
      nomCell: String.fromCharCode(65 + numCol) + String(numLigne)
    }
    stor.obj = {}
    // on déclare les cellule qui vont m’intéresser
    stor.obj.c = stor.cellule.nomCell
    stor.obj.c2 = (stor.positionDonnees[me.questionCourante - 1] === 'ligne')
      ? String.fromCharCode(65 + numCol + 1) + String(numLigne)
      : String.fromCharCode(65 + numCol) + String(numLigne + 1)

    const objChaine = {}
    objChaine.h = String.fromCharCode(65 + numCol) + String(numLigne - 1)
    objChaine.g = String.fromCharCode(65 + numCol - 1) + String(numLigne)
    objChaine.gd = String.fromCharCode(65 + numCol - 1) + '$' + String(numLigne)
    objChaine.hg = String.fromCharCode(65 + numCol - 1) + String(numLigne - 1)
    objChaine.hgd = String.fromCharCode(65 + numCol - 1) + '$' + String(numLigne - 1)
    objChaine.hd = String.fromCharCode(65 + numCol) + '$' + String(numLigne - 1)
    // pour les suites geo ou arith
    objChaine.signe = (j3pGetRandomBool()) ? '-' : '+'
    objChaine.r = j3pGetRandomInt(2, 7)
    objChaine.q = j3pVirgule(j3pGetRandomInt(2, 8) / 10 + j3pGetRandomInt(0, 1))
    objChaine.u0 = j3pVirgule(j3pGetRandomInt(15, 30) / 10)
    // Pour les formules, j’utilise function j3pChaine(ch,objet) ligne 4313 de j3pOutils
    const laFormule = j3pChaine(stor.formules[me.questionCourante - 1], objChaine)
    stor.obj.f = laFormule.replace(/\$/g, '&dollar;')
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.obj)
    const laCons2 = (stor.positionDonnees[me.questionCourante - 1] === 'ligne')
      ? textes.consigne2_1
      : textes.consigne2_2
    j3pAffiche(stor.zoneCons2, '', laCons2, stor.obj)
    const zoneCons = j3pAffiche(stor.zoneCons3, '', textes.consigne3,
      {
        input1: { dynamique: true, texte: '' }
      })
    stor.zoneInput = zoneCons.inputList[0]
    stor.zoneInput.typeReponse = ['texte']
    stor.zoneInput.reponse = [formuleRecopiee(laFormule, stor.positionDonnees[me.questionCourante - 1])]
    me.logIfDebug('réponse :', stor.zoneInput.reponse)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneBtn = j3pAddElt(stor.zoneMD, 'div')
    stor.correction = j3pAddElt(stor.zoneMD, 'div')

    me.fenetresjq = [{ name: 'Aide', title: textes.aide2 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneBtn, j3pGetNewId('btnFeuille'), 'MepBoutons', textes.aide2, j3pToggleFenetres.bind(null, 'Aide'))

    stor.laFeuille = j3pAddElt('dialogAide', 'div')
    creationTab({ eltHtml: stor.laFeuille, largTableur: 500 })
    j3pAfficheCroixFenetres('Aide')

    j3pFocus(stor.zoneInput)
    stor.zoneInput.addEventListener('input', j3pRemplacePoint)
    // Paramétrage de la validation
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.zoneInput.id] })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        // positionDonnees me permet de positionner les données en ligne ou en colonne dans la feuille de calcul
        stor.positionDonnees = []
        for (let i = 1; i <= ds.nbrepetitions / 2; i++) {
          stor.positionDonnees.push('ligne', 'colonne')
        }
        let tabChoix
        if (ds.nbrepetitions % 2 !== 0) {
          tabChoix = ['ligne', 'colonne']
          stor.positionDonnees.push(tabChoix[j3pGetRandomInt(0, 1)])
        }
        // cas de figure avec dollar ou non
        stor.avecDollar = []
        if (ds.avecDollar) {
          for (let i = 1; i <= ds.nbrepetitions / 4; i++) {
            stor.avecDollar.push(false, true, true, false)
          }
          if (ds.nbrepetitions % 4 !== 0) {
            tabChoix = !j3pGetRandomInt(0, 1)
            for (let i = 0; i < ds.nbrepetitions % 4; i++) {
              stor.avecDollar.push(tabChoix)
              tabChoix = !tabChoix
            }
          }
        } else {
          for (let i = 1; i < ds.nbrepetitions; i++) {
            stor.avecDollar.push(false)
          }
        }
        if (ds.avecDollar) {
          const tabDerange = j3pShuffleMulti(stor.positionDonnees, stor.avecDollar)
          stor.positionDonnees = tabDerange[0]
          stor.avecDollar = tabDerange[1]
        } else {
          stor.positionDonnees = j3pShuffle(stor.positionDonnees)
        }
        // je vais définir toutes les formules possibles et imaginables ici
        stor.formules = []
        // dans stor.formules.droiteSans, on met les formules à tirer vers la droite sans dollar, même principe pour les autres...
        // dans ce qui suit, £h designera la cellule au-dessus de celle qui a la formule, £g pour celle qui est à gauche
        // on peut aussi utliser £{hg}
        // £q sera utilisé comme raison d’une suite géométrique, on peut aussi avoir £u0
        // £r est utilisé comme raison d’une suite arithmetique, £{signe} vaut + ou -
        // £{hgd} sera la cellule en haut à gauche où le dollar sera entre la lettre et le chiffre
        let droiteSans = []
        let basSans = []
        let droiteAvec = []
        let basAvec = []
        let droiteAvecDouble = []
        let basAvecDouble = []
        droiteSans.push('=£g*(1+£h)', '=£g*£q', '=£g£{signe}£r', '=£{u0}*£q^£h', '=£{u0}£{signe}£r*£h', '=£q*£g£{signe}£r', '=£r*£g£{signe}£{hg}', '=£g£{signe}£h')
        basSans.push('=£h*(1+£g)', '=£h*£q', '=£h£{signe}£r', '=£{u0}*£q^£g', '=£{u0}£{signe}£r*£g', '=£q*£h£{signe}£r', '=£r*£h£{signe}£{hg}', '=£h£{signe}£g')
        droiteAvec.push('=£g*(1+$£{hg})', '=£h/$£g', '=$£g*£q^£h', '=£g*$£{hg}', '=£{u0}*$£{hg}^£h', '=£{u0}£{signe}$£{hg}*£h')
        basAvec.push('=£h*(1+£{hgd})', '=£g/£{hgd}', '=£{hd}*£q^£g', '=£g*£{hgd}', '=£{u0}*£{hgd}^£h', '=£{u0}£{signe}£{hgd}*£g')
        // Formule où on a un double dollar
        droiteAvecDouble.push('=£g*(1+$£{hgd})', '=£h/$£{gd}', '=$£{g}*£q^£h', '=£g*$£{hgd}', '=£{u0}*$£{hgd}^£h', '=£{u0}£{signe}$£{hgd}*£h')
        basAvecDouble.push('=£h*(1+$£{hgd})', '=£g/$£{hgd}', '=$£{hd}*£q^£g', '=£g*$£{hgd}', '=£{u0}*$£{hgd}^£h', '=£{u0}£{signe}$£{hgd}*£g')
        const droiteSansInit = [...droiteSans]
        const basSansInit = [...basSans]
        const droiteAvecInit = [...droiteAvec]
        const basAvecInit = [...basAvec]
        const droiteAvecDoubleInit = [...droiteAvecDouble]
        const basAvecDoubleInit = [...basAvecDouble]
        let leDollarDouble = (ds.doubleDollar)
          ? (j3pGetRandomInt(0, 1) === 1)
          : false
        let pioche
        for (let i = 0; i < ds.nbrepetitions; i++) {
          if (stor.avecDollar[i]) {
            if (stor.positionDonnees[i] === 'ligne') {
              // on pioche dans les formules pour tirer vers la droite
              pioche = j3pGetRandomInt(0, (droiteAvec.length - 1))
              if (leDollarDouble) {
                stor.formules.push(droiteAvecDouble[pioche])
              } else {
                stor.formules.push(droiteAvec[pioche])
              }
              droiteAvec.splice(pioche)
              droiteAvecDouble.splice(pioche)
              if (droiteAvec.length === 0) {
                droiteAvec = [...droiteAvecInit]
                droiteAvecDouble = [...droiteAvecDoubleInit]
              }
            } else {
              // on pioche dans les formules pour tirer vers le bas
              pioche = j3pGetRandomInt(0, (basAvec.length - 1))
              if (leDollarDouble) {
                stor.formules.push(basAvecDouble[pioche])
              } else {
                stor.formules.push(basAvec[pioche])
              }
              basAvec.splice(pioche)
              basAvecDouble.splice(pioche)
              if (basAvec.length === 0) {
                basAvec = [...basAvecInit]
                basAvecDouble = [...basAvecDoubleInit]
              }
            }
            leDollarDouble = (ds.doubleDollar) ? !leDollarDouble : false
          } else {
            if (stor.positionDonnees[i] === 'ligne') {
              // on pioche dans les formules pour tirer vers la droite
              pioche = j3pGetRandomInt(0, (droiteSans.length - 1))
              stor.formules.push(droiteSans[pioche])
              droiteSans.splice(pioche)
              if (droiteSans.length === 0) droiteSans = [...droiteSansInit]
            } else {
              // on pioche dans les formules pour tirer vers le bas
              pioche = j3pGetRandomInt(0, (basSans.length - 1))
              stor.formules.push(basSans[pioche])
              basSans.splice(pioche)
              if (basSans.length === 0) basSans = [...basSansInit]
            }
          }
        }
        me.logIfDebug('stor.formules:', stor.formules)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      // création du conteneur dans lequel se trouveront toutes les consignes
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = { aRepondu: false, bonneReponse: false }
        let pbsAccents
        reponse.aRepondu = stor.fctsValid.valideReponses()
        if (reponse.aRepondu) {
          pbsAccents = /[ÂÊÎÔÛ]/g.test(stor.fctsValid.zones.reponseSaisie[0].toUpperCase())
          reponse.aRepondu = !pbsAccents
          if (reponse.aRepondu) {
            reponse.bonneReponse = (stor.fctsValid.zones.reponseSaisie[0].toUpperCase() === stor.fctsValid.zones.inputs[0].reponse[0])
            if (reponse.bonneReponse) stor.fctsValid.zones.inputs[0].value = stor.fctsValid.zones.inputs[0].reponse[0]
            stor.fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
            stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (pbsAccents) {
            msgReponseManquante = textes.comment2
          }
          me.reponseManquante(stor.correction, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.correction.style.color = me.styles.cbien
            stor.correction.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.correction.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.correction.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.correction.innerHTML = cFaux
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
              const presenceEgal = (stor.fctsValid.zones.reponseSaisie[0][0] === '=')
              if (!presenceEgal) {
                stor.correction.innerHTML += '<br/>' + textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.correction.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.correction.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
