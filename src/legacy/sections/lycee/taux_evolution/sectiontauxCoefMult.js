import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pShuffle, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pRemplacePoint, j3pRestriction, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUd Rémi
        Mars 2019
        Dans cette section, on associe un taux d’évolution avec le coefficient multiplicateur
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuest', 'les deux', 'liste', 'Description du type de question : si le paramètre vaut "taux", alors on donne un taux et on demande le coefficient multiplicateur associé. Pour "coef", c’est le contraire.<br/>Par défaut, on aura les deux cas de figure', ['coef', 'taux', 'les deux']]
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Taux d’évolution et coefficient multiplicateur',
  // on donne les phrases de la consigne
  consigne1_1: 'Le nombre d’élèves d’un lycée a augmenté de £t&nbsp;% en un an.',
  consigne1_2: 'Les ventes d’un magasin de vêtement ont augmenté de £t&nbsp;% en trois ans.',
  consigne1_3: 'Les ventes d’un magasin de vêtement ont augmenté de £t&nbsp;% en trois ans.',
  consigne1_4: 'Au cours du mois de mars, le prix de l’essence a augmenté de £t&nbsp;%.',
  consigne1_5: 'Le salaire moyen des employés d’une entreprise à augmenté de £t&nbsp;% en un an.',
  consigne2_1: 'Dans un département, le nombre d’accidents de la route a baissé de £t&nbsp;% en un an.',
  consigne2_2: 'Le chiffre d’affaires d’une entreprise a baissé de £t&nbsp;% en un an.',
  consigne2_3: 'Pendant les soldes, le prix d’une veste a baissé de £t&nbsp;%.',
  consigne2_4: 'En trois ans, le prix d’un modèle de smartphone a baissé de £t&nbsp;%.',
  consigne2_5: 'Le nombre de visites d’un site Internet a baissé de £t&nbsp;% en un mois.',
  consigne3_1: 'Le nombre de visites d’un musée a été multiplié par £c en un an.',
  consigne3_2: 'Le nombre de visites d’un site web a été multiplié par £c en un an.',
  consigne3_3: 'En un mois, le nombre de spams reçus dans ma boite mail a été multiplié par £c.',
  consigne3_4: 'Du premier au deuxième set, le nombre de premières balles d’un serveur au tennis a été multiplié par £c.',
  consigne3_5: 'En un an, la consommation électrique d’un appartement a été multipliée par £c.',
  consigne4_1: 'Pour les soldes, le prix initial d’un pull a été multiplié par £c.',
  consigne4_2: 'La distance parcourue par mon véhicule personnel a été multiplié par £c en un an.',
  consigne4_3: 'Dans une ville, le nombre d’accident de scooter a été multiplié par £c en un mois.',
  consigne4_4: 'Sur une route nationale, le nombre d’excès de vitesse a été multiplié par £c en un an.',
  consigne4_5: 'D’un mois à l’autre, le temps passé par Antoine devant les écrans a été multiplié par £c.',
  consigne5_1: 'Le coefficient multiplicateur (au format décimal) associé à cette évolution vaut @1@.',
  consigne5_2: 'Cette situation correspond à #1# de @1@&nbsp;%.',
  choix1: 'une augmentation',
  choix2: 'une diminution',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: '',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Une valeur augmente de $t$&nbsp;% signifie qu’elle est multipliée par $\\left(1+\\frac{t}{100}\\right)$.',
  corr1_2: 'Une valeur baisse de $t$&nbsp;% signifie qu’elle est multipliée par $\\left(1-\\frac{t}{100}\\right)$.',
  corr2_1: 'Dans cet exemple, nous avons une augmentation de £t&nbsp;% donc le coefficient multiplicateur vaut $\\left(1+\\frac{£t}{100}\\right)=£r$.',
  corr2_2: 'Dans cet exemple, nous avons une baisse de £t&nbsp;% donc le coefficient multiplicateur vaut $\\left(1-\\frac{£t}{100}\\right)=£r$.',
  corr3_1: 'Dans cet exemple, $£c=\\left(1+\\frac{£t}{100}\\right)$. Donc nous avons une augmentation de £t&nbsp;%.',
  corr3_2: 'Dans cet exemple, $£c=\\left(1-\\frac{£t}{100}\\right)$. Donc nous avons une diminution de £t&nbsp;%.'
}
/**
 * Ajouter une description ici
 * @fileOverview
 * @author Remi Deniaud
*/
/**
 * section tauxCoefMult
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let i
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    for (i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    const laCorr1 = (stor.donnees.taux > 0)
      ? textes.corr1_1
      : textes.corr1_2
    j3pAffiche(stor.zoneExpli1, '', laCorr1)
    let laCorr2
    if (Number(stor.lesConsignes[me.questionCourante - 1][stor.lesConsignes[me.questionCourante - 1].length - 3]) <= 2) {
      laCorr2 = (stor.donnees.taux > 0) ? textes.corr2_1 : textes.corr2_2
    } else {
      laCorr2 = (stor.donnees.taux > 0) ? textes.corr3_1 : textes.corr3_2
    }
    j3pAffiche(stor.zoneExpli2, '', laCorr2, {
      c: j3pVirgule(Math.abs(stor.donnees.coef)),
      t: j3pVirgule(Math.abs(stor.donnees.taux)),
      r: j3pVirgule(stor.zoneInput.reponse[0])
    })
    // dans explication2, ce qui correspond à la deuxième question
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    // @todo mieux nommer ces var intermédiaires
    const itemPrec = me.questionCourante - 1
    const index1 = stor.lesConsignes[itemPrec].length - 3
    const index2 = stor.lesConsignes[itemPrec].length - 1
    const data1 = stor.lesConsignes[itemPrec][index1]
    const data2 = stor.lesConsignes[itemPrec][index2]
    let leTaux
    if (data1 === '1') {
      switch (data2) {
        case '1':
          leTaux = j3pGetRandomInt(8, 16) / 10
          break
        case '2':
          leTaux = j3pGetRandomInt(8, 16)
          break
        case '3':
          leTaux = j3pGetRandomInt(11, 23)
          break
        case '4':
          leTaux = j3pGetRandomInt(11, 23) / 10
          break
        default:
          leTaux = j3pGetRandomInt(7, 18) / 10
          break
      }
    } else if (data1 === '2') {
      switch (data2) {
        case '1':
          leTaux = -j3pGetRandomInt(15, 25) / 10
          break
        case '2':
          leTaux = -j3pGetRandomInt(5, 12) / 10
          break
        case '3':
          leTaux = -j3pGetRandomInt(3, 7) * 5
          break
        case '4':
          do {
            leTaux = -j3pGetRandomInt(16, 25)
          } while (leTaux === -20)
          break
        default:
          leTaux = -j3pGetRandomInt(16, 25) / 10
          break
      }
    } else if (data1 === '3') {
      switch (data2) {
        case '1':
          leTaux = j3pGetRandomInt(27, 39) / 10
          break
        case '2':
          leTaux = j3pGetRandomInt(125, 225) / 10
          break
        case '3':
          leTaux = j3pGetRandomInt(35, 90) / 10
          break
        case '4':
          do {
            leTaux = j3pGetRandomInt(16, 35)
          } while (leTaux % 10 === 0)
          break
        default:
          leTaux = j3pGetRandomInt(16, 32) / 10
          break
      }
    } else if (data1 === '4') {
      switch (data2) {
        case '1':
          leTaux = -j3pGetRandomInt(4, 9) * 5
          break
        case '2':
          leTaux = -j3pGetRandomInt(7, 13)
          break
        case '3':
          leTaux = -j3pGetRandomInt(35, 90) / 10
          break
        case '4':
          leTaux = -j3pGetRandomInt(16, 35) / 10
          break
        default:
          leTaux = -j3pGetRandomInt(16, 32) / 10
          break
      }
    }
    const leCoef = j3pArrondi(1 + leTaux / 100, 5)
    if (Number(data1) <= 2) {
      j3pAffiche(zoneCons1, '', textes[stor.lesConsignes[itemPrec]],
        {
          t: j3pVirgule(Math.abs(leTaux))
        })
    } else {
      j3pAffiche(zoneCons1, '', textes[stor.lesConsignes[itemPrec]],
        {
          c: j3pVirgule(Math.abs(leCoef))
        })
    }
    stor.donnees = { taux: leTaux, coef: leCoef }
    // Dans cette section, j’ai une deuxième phrase à compléter
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    let mesZonesSaisie
    if (Number(data1) <= 2) {
      // on demande le coef mult
      const elt = j3pAffiche(zoneCons2, '', textes.consigne5_1, {
        input1: { texte: '', dynamique: true }
      })
      stor.zoneInput = elt.inputList[0]
      stor.zoneInput.reponse = [stor.donnees.coef]
      mesZonesSaisie = [stor.zoneInput.id]
    } else {
      // on demande le taux
      const elt = j3pAffiche(zoneCons2, '', textes.consigne5_2, {
        input1: { texte: '', dynamique: true },
        liste1: { texte: ['', textes.choix1, textes.choix2] }
      })
      stor.zoneInput = elt.inputList[0]
      stor.zoneInput.reponse = [Math.abs(stor.donnees.taux)]
      const corrListe = (stor.donnees.taux > 0) ? 1 : 2
      elt.selectList[0].reponse = [corrListe]
      mesZonesSaisie = [stor.zoneInput.id, elt.selectList[0].id]
    }
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    j3pFocus(stor.zoneInput)
    j3pRestriction(stor.zoneInput, '0-9\\.,')
    stor.zoneInput.addEventListener('input', j3pRemplacePoint)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    const zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(zoneMD, 'div')
    stor.zoneCorr = j3pAddElt(zoneMD, 'div')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)

    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // code de création des fenêtres
        stor.typeConsigne = []
        if (ds.typeQuest === 'les deux') {
          for (let i = 0; i < ds.nbrepetitions / 2; i++) {
            stor.typeConsigne.push('taux', 'coef')
          }
          if (ds.nbrepetitions % 2 === 1) {
            const choixTab = ['taux', 'coef']
            stor.typeConsigne.push(choixTab[j3pGetRandomInt(0, 1)])
          }
          stor.typeConsigne = j3pShuffle(stor.typeConsigne)
        } else {
          for (let i = 1; i <= ds.nbrepetitions; i++) {
            stor.typeConsigne.push(ds.typeQuest)
          }
        }
        const tabConsTauxAug = ['consigne1_1', 'consigne1_2', 'consigne1_3', 'consigne1_4', 'consigne1_5']
        const tabConsTauxBaisse = ['consigne2_1', 'consigne2_2', 'consigne2_3', 'consigne2_4', 'consigne2_5']
        const tabConsCoefAug = ['consigne3_1', 'consigne3_2', 'consigne3_3', 'consigne3_4', 'consigne3_5']
        const tabConsCoefBaisse = ['consigne4_1', 'consigne4_2', 'consigne4_3', 'consigne4_4', 'consigne4_5']
        let choixAug = -1
        let pioche
        stor.lesConsignes = []
        for (let i = 0; i < ds.nbrepetitions; i++) {
          if (stor.typeConsigne[i] === 'taux') {
            if (choixAug === -1) {
              choixAug = j3pGetRandomInt(0, 1)
            } else {
              choixAug = (choixAug + 1) % 2
            }
            if (choixAug === 0) {
              pioche = j3pGetRandomInt(0, (tabConsTauxAug.length - 1))
              stor.lesConsignes.push(tabConsTauxAug[pioche])
              tabConsTauxAug.splice(pioche, 1)
            } else {
              pioche = j3pGetRandomInt(0, (tabConsTauxBaisse.length - 1))
              stor.lesConsignes.push(tabConsTauxBaisse[pioche])
              tabConsTauxBaisse.splice(pioche, 1)
            }
          } else {
            if (choixAug === -1) {
              choixAug = j3pGetRandomInt(0, 1)
            } else {
              choixAug = (choixAug + 1) % 2
            }
            if (choixAug === 0) {
              pioche = j3pGetRandomInt(0, (tabConsCoefAug.length - 1))
              stor.lesConsignes.push(tabConsCoefAug[pioche])
              tabConsCoefAug.splice(pioche, 1)
            } else {
              pioche = j3pGetRandomInt(0, (tabConsCoefBaisse.length - 1))
              stor.lesConsignes.push(tabConsCoefBaisse[pioche])
              tabConsCoefBaisse.splice(pioche, 1)
            }
          }
        }
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      // création du conteneur dans lequel se trouveront toutes les conaignes
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
          if (reponse.bonneReponse) {
          // Bonne réponse
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
