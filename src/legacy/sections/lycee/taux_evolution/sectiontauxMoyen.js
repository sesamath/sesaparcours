import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pNombre, j3pRandomTab, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import { afficheBloc, phraseBloc } from 'src/legacy/core/functionsPhrase'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { mqRestriction } from 'src/lib/mathquill/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Mettre ici une description de la section
 * @author Rémi DENIAUD
 * @since juillet 2022
 * @fileOverview calcul d’un taux moyen
 */

// nos constantes
const structure = 'presentation1'
// const ratioGauche = 0.75 C’est la valeur par défaut du ratio de la partie gauche dans presentation1
// N’utiliser cette variable que si ce ratio est différent
// (utilisé dans ce cas dans me.construitStructurePage(structure, ratioGauche) de initSection()

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['questionInter', 'aucune', 'liste', 'Il est possible d’avoir une question intermédiaire avec calcul d’un taux d’évolution global (soit avec des évolutions successives, soit avec la valeur initiale et la valeur finale)', ['aucune', 'evolutions successives', 'valeurs']]
    // les paramètres peuvent être du type 'entier', 'string', 'boolean', 'liste'
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titreExo1: 'Taux d’évolution moyen',
  titreExo2: 'Evolutions et taux d’évolution moyen',
  // on donne les phrases de la consigne
  consigneValeurs1_1: 'En £n années, le cours d’une action est passé de £{v1}&nbsp;€ à £{v2}&nbsp;€.',
  consigneValeurs1_2: 'En £n années, la population d’une petite ville est passée de £{v1} à £{v2}&nbsp;habitants.',
  consigneValeurs1_3: 'En £n années, le nombre de visites journalier d’un site internet est passé de £{v1} à £{v2}.',
  consigneValeurs1_4: 'En £n années, le nombre de véhicules vendus par une concession en un an est passé de £{v1} à £{v2}.',
  consigneTaux1_1: 'Sur les £n dernières années, le cours d’une action a subi les évolutions successives : £{t1} et £{t2}.',
  consigneTaux1_2: 'Sur les £n dernières années, la population d’une petite ville a subi les évolutions successives : £{t1} et £{t2}.',
  consigneTaux1_3: 'Sur les £n dernières années, le nombre de visites journalier d’un site internet a subi les évolutions successives : £{t1} et £{t2}.',
  consigneTaux1_4: 'Sur les £n dernières années, le nombre de véhicules vendus par une concession en un an a subi les évolutions successives : £{t1} et £{t2}.',
  consigneQ1: 'Le taux d’évolution global sur cette période vaut environ &1&%.',
  consigne2_1: 'Le cours d’une action a subi une augmentation de £t&nbsp;% en £n ans.',
  consigne2_2: 'La population d’une petite ville a subi une augmentation de £t&nbsp;% en £n ans.',
  consigne2_3: 'Le nombre de visites journalier d’un site internet a subi une augmentation de £t&nbsp;% en £n ans.',
  consigne2_4: 'Le nombre de véhicules vendus par une concession en un an a subi une augmentation de £t&nbsp;% en £n ans.',
  consigne2Moins_1: 'Le cours d’une action a subi une diminution de £t&nbsp;% en £n ans.',
  consigne2Moins_2: 'La population d’une petite ville a subi une diminution de £t&nbsp;% en £n ans.',
  consigne2Moins_3: 'Le nombre de visites journalier de ce site a subi une diminution de £t&nbsp;% en £n ans.',
  consigne2Moins_4: 'Le nombre de véhicules vendus par une concession en un an a subi une diminution de £t&nbsp;% en £n ans.',
  consigne2Bis_1: 'Le cours de cette action a donc subi une augmentation de £t&nbsp;% en £n ans.',
  consigne2Bis_2: 'La population de cette ville a donc subi une augmentation de £t&nbsp;% en £n ans.',
  consigne2Bis_3: 'Le nombre de visites journalier de ce site a donc subi une augmentation de £t&nbsp;% en £n ans.',
  consigne2Bis_4: 'Le nombre de véhicules vendus par cette concession a donc subi une augmentation de £t&nbsp;% en £n ans.',
  consigne2MoinsBis_1: 'Le cours de cette action a donc subi une diminution de £t&nbsp;% en £n ans.',
  consigne2MoinsBis_2: 'La population de cette ville a donc subi une diminution de £t&nbsp;% en £n ans.',
  consigne2MoinsBis_3: 'Le nombre de visites journalier de ce site a donc subi une diminution de £t&nbsp;% en £n ans.',
  consigne2MoinsBis_4: 'Le nombre de véhicules vendus par cette concession a donc subi une diminution de £t&nbsp;% en £n ans.',
  consigne3: 'Le taux annuel moyen (en pourcentage) vaut environ &1&%.',
  info: 'Arrondir à £p&nbsp;%.',
  environ: 'environ ',
  // les différents commentaires envisagés suivant les réponses fausses de l’élève
  comment1: 'Peut-être y a-t-il un problème d’arrondi&nbsp;?',
  comment2: 'Attention car il s’agit d’une augmentation ici&nbsp;!',
  comment3: 'Attention car il s’agit d’une diminution ici&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'On cherche $t$ tel que $1+\\frac{t}{100}=£{CMTxt}$',
  corr2: 'Ceci donne $1+\\frac{t}{100}£e£{CMArrondi}$ et ainsi $t\\simeq £{valTaux}$.',
  corr3_1: 'Ainsi, le taux d’évolution global sur cette période vaut environ £t&nbsp;% et on conclut que le cours a augmenté d’environ £t&nbsp;% au cours des £n années.',
  corr3_2: 'Ainsi, le taux d’évolution global sur cette période vaut environ £t&nbsp;% et on conclut la population a augmenté d’environ £t&nbsp;% au cours des £n années.',
  corr3_3: 'Ainsi, le taux d’évolution global sur cette période vaut environ £t&nbsp;% et on conclut le nombre de visites journalier de ce site a augmenté d’environ £t&nbsp;% au cours des £n années.',
  corr3_4: 'Ainsi, le taux d’évolution global sur cette période vaut environ £t&nbsp;% et on conclut le nombre de véhicules vendus par cette concession a augmenté d’environ £t&nbsp;% au cours des £n années.',
  corr3Moins_1: 'Ainsi, le taux d’évolution global sur cette période vaut environ £{valTaux}&nbsp;% et on conclut le cours a diminué d’environ £t&nbsp;% au cours des £n années.',
  corr3Moins_2: 'Ainsi, le taux d’évolution global sur cette période vaut environ £{valTaux}&nbsp;% et on conclut la population a diminué d’environ £t&nbsp;% au cours des £n années.',
  corr3Moins_3: 'Ainsi, le taux d’évolution global sur cette période vaut environ £{valTaux}&nbsp;% et on conclut le nombre de visites journalier de ce site a diminué d’environ £t&nbsp;% au cours des £n années.',
  corr3Moins_4: 'Ainsi, le taux d’évolution global sur cette période vaut environ £{valTaux}&nbsp;% et on conclut le nombre de véhicules vendus par cette concession a diminué d’environ £t&nbsp;% au cours des £n années.',
  corr4: 'Le taux d’évolution vaut $£{calculTaux}$ soit environ £tSigne&nbsp;%.',
  corr5: 'Le taux moyen est le taux d’évolution à appliquer £n fois pour que cela corresponde à l’évolution globale.',
  corr6: 'On cherche alors $t$ tel que $\\left(1+\\frac{t}{100}\\right)^{£n}=£{CMCalc}$.',
  corr7: 'Ceci nous donne $1+\\frac{t}{100}=\\left(£{CMCalc}\\right)^{\\frac{1}{£n}}$.',
  corr8: 'Ainsi $1+\\frac{t}{100}\\simeq £{CMMoy}$ et $t\\simeq £{tM}$.',
  corr9: 'Le taux annuel moyen vaut ainsi £{tMAbs}&nbsp;% et on conclut que l’augmentation annuelle moyenne est d’environ £{tMAbs}&nbsp;%.',
  corr9Moins: 'Le taux annuel moyen vaut ainsi £{tM}&nbsp;% et on conclut que la diminution annuelle moyenne est d’environ £{tMAbs}&nbsp;%.'
}
/**
 * La fonction principale, rappelée à chaque étape
 * Son this sera l’instance courante de Parcours
 * @this {Parcours}
 */
export default function maSection () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini (en général dans enonceEnd(me)), sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  const ds = me.donneesSection
  const stor = me.storage
  // On surcharge avec les parametres passés par le graphe
  // Construction de la page
  me.construitStructurePage(structure) // ajouter un deuxième argument ratioGauche s’il est défini en tant que variable globale au début
  ds.nbetapes = (ds.questionInter === 'aucune') ? 1 : 2
  ds.nbitems = ds.nbetapes * ds.nbrepetitions
  me.afficheTitre((ds.nbetapes === 1) ? textes.titreExo1 : textes.titreExo2)
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone

  // ou stocker des variables, par exemples celles qui serviraient pour la pe
  me.storage.tabSujet = [1, 2, 3, 4]
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}
/**
 * Retourne les données de l’exercice
 * @private
 * @param {String} casFigure c’est le type d’exercice
 * @param {number} numSujet c’est le numéro du sujet
 * @return {Object}
 */
function genereAlea (casFigure, numSujet) {
  const obj = {}
  obj.n = j3pGetRandomInt(3, 5)
  // Dans le cas de plusieurs taux successifs, il faut que je les détermine, pour qu'à la fin, on trouve le taux t
  if (casFigure === 'aucune') {
    obj.tauxArrondi = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(obj.n * 15, obj.n * 15 + 20) / 10
    obj.taux = obj.tauxArrondi
    obj.CM = (1 + obj.tauxArrondi / 100)
  } else {
    if (casFigure === 'evolutions successives') {
      let sommeTaux = 0
      do {
        obj.lesTaux = []
        obj.CM = 1
        for (let i = 0; i < obj.n; i++) {
          const newTaux = j3pRandomTab([-1, 1], [0.33, 0.67]) * j3pGetRandomInt(25, 45) / 10
          obj.lesTaux.push(newTaux)
          obj.CM *= (1 + newTaux / 100)
          sommeTaux += newTaux
        }
        obj.taux = (obj.CM - 1) * 100
      // Dans cet exemple, je cherche seulement un taux global positif (et pas trop petit)
      } while ((obj.taux < 2.5) || (Math.abs(obj.taux - sommeTaux) < 0.2))
      obj.t1 = ((obj.lesTaux[0] > 0) ? '+' : '') + j3pVirgule(obj.lesTaux[0]) + '&nbsp;%'
      obj.CMTxt = '\\left(1' + ((obj.lesTaux[0] > 0) ? '+' : '-') + '\\frac{' + j3pVirgule(Math.abs(obj.lesTaux[0])) + '}{100}\\right)'
      obj.lesTaux.slice(1).forEach(taux => {
        obj.CMTxt += '\\times \\left(1' + ((taux > 0) ? '+' : '-') + '\\frac{' + j3pVirgule(Math.abs(taux)) + '}{100}\\right)'
      })
      obj.lesTaux.slice(1, obj.lesTaux.length - 1).forEach(taux => {
        obj.t1 += '; ' + ((taux > 0) ? '+' : '') + j3pVirgule(taux) + '&nbsp;%'
      })
      obj.t2 = ((obj.lesTaux[obj.lesTaux.length - 1] > 0) ? '+' : '') + j3pVirgule(obj.lesTaux[obj.lesTaux.length - 1]) + '&nbsp;%'
      obj.tauxArrondi = Math.round(obj.taux * 10000) / 10000
      obj.CMArrondi = (1 + obj.tauxArrondi / 100)
      obj.e = (Math.abs(obj.taux - obj.tauxArrondi) < Math.pow(10, -10)) ? '=' : '\\simeq '
    } else {
      do {
        if (numSujet === 1) {
          obj.val1 = j3pGetRandomInt(600, 800) / 10
          obj.val2 = obj.val1 + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(20, 35) / 10
        } else if (numSujet === 2) {
          obj.val1 = j3pGetRandomInt(2000, 2500)
          obj.val2 = obj.val1 + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(60, 120)
        } else if (numSujet === 3) {
          obj.val1 = j3pGetRandomInt(500, 750)
          obj.val2 = obj.val1 + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(25, 33)
        } else if (numSujet === 4) {
          obj.val1 = j3pGetRandomInt(180, 250)
          obj.val2 = obj.val1 + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(14, 24)
        }
        obj.CM = obj.val2 / obj.val1
        // Je ne veux pas de valeur exacte
      } while (Math.abs(Math.round(obj.CM * 1000) - obj.CM * 1000) < Math.pow(10, -12))
      obj.val2 = Math.round(obj.val2 * 10) / 10
      obj.v1 = j3pVirgule(obj.val1)
      obj.v2 = j3pVirgule(obj.val2)
      obj.calculTaux = '\\frac{' + obj.v2 + '-' + obj.v1 + '}{' + obj.v1 + '}\\times 100'
      obj.taux = 100 * (obj.val2 - obj.val1) / obj.val1
      obj.tauxArrondi = Math.round(obj.taux * 10000) / 10000
      obj.CMArrondi = (1 + obj.tauxArrondi / 100)
    }
  }
  // la première version de tauxArrondi permettait d’afficher le coef mult avec 4 chiffres après la virgule
  // Pour la suite, je n’en garderai qu’un
  obj.tauxArrondi = Math.round(obj.tauxArrondi * 10) / 10
  obj.CMCalc = (obj.taux > 0) ? '1+\\frac{' + j3pVirgule(obj.tauxArrondi) + '}{100}' : '1-\\frac{' + j3pVirgule(Math.abs(obj.tauxArrondi)) + '}{100}'
  obj.valTaux = Math.round(obj.taux * 10) / 10
  obj.tSigne = j3pVirgule(obj.valTaux)
  obj.t = j3pVirgule(Math.abs(obj.valTaux))
  obj.tauxMoy = (Math.pow(1 + obj.tauxArrondi / 100, 1 / obj.n) - 1) * 100
  obj.CMMoy = Math.round(10000 * (1 + obj.tauxMoy / 100)) / 10000
  obj.tM = j3pVirgule(Math.round(obj.tauxMoy * 100) / 100)
  obj.tMAbs = j3pVirgule(Math.round(Math.abs(obj.tauxMoy) * 100) / 100)
  return obj
}
/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // on affiche l’énoncé
  // La style de base (ici petit.enonce) peut être complété par d’autres propriétés css. C’est fait ici avec j3pStyle
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.enonce })
  j3pStyle(stor.elts.divEnonce, { padding: '10px' })
  // On génère les données de l’exo (stockées dans un objet dont on se sert entre autre pour les phrases de l’énoncé)
  if (ds.nbetapes === 1 || (me.questionCourante % ds.nbetapes === 1)) {
    // Ce qui suit permet de choisir le num de sujet en étant sûr de ne pas récupérer le même énoncé (que le précédent)*;
    if (stor.tabSujet.length === 0) stor.tabSujet = [1, 2, 3, 4]
    const alea = j3pGetRandomInt(0, (stor.tabSujet.length - 1))
    stor.choixSujet = stor.tabSujet[alea]
    stor.tabSujet.splice(alea, 1)
    stor.objVariables = genereAlea(ds.questionInter, stor.choixSujet)
  }
  let laConsigneRep = textes.consigne3
  if (ds.questionInter === 'aucune') {
    // ds.nbetapes vaut 1 dans ce cas
    phraseBloc(stor.elts.divEnonce, (stor.objVariables.valTaux > 0) ? textes['consigne2_' + stor.choixSujet] : textes['consigne2Moins_' + stor.choixSujet], stor.objVariables)
  } else {
    if (ds.questionInter === 'evolutions successives') {
      phraseBloc(stor.elts.divEnonce, textes['consigneTaux1_' + stor.choixSujet], stor.objVariables)
    } else {
      phraseBloc(stor.elts.divEnonce, textes['consigneValeurs1_' + stor.choixSujet], stor.objVariables)
    }
    if (me.questionCourante % ds.nbetapes === 1) laConsigneRep = textes.consigneQ1
    else phraseBloc(stor.elts.divEnonce, (stor.objVariables.valTaux > 0) ? textes['consigne2Bis_' + stor.choixSujet] : textes['consigne2MoinsBis_' + stor.choixSujet], stor.objVariables)
  }
  const eltCons2 = afficheBloc(stor.elts.divEnonce, laConsigneRep)
  stor.zoneInput = eltCons2.inputmqList[0]
  mqRestriction(stor.zoneInput, '\\d,.-+')
  stor.larrondi = (ds.nbetapes === 1 || (me.questionCourante % ds.nbetapes === 0)) ? 0.01 : 0.1
  stor.zoneInput.typeReponse = ['nombre', 'arrondi', stor.larrondi]
  stor.zoneInput.reponse = (ds.nbetapes === 1 || (me.questionCourante % ds.nbetapes === 0))
    ? [stor.objVariables.tauxMoy]
    : [stor.objVariables.taux]
  me.logIfDebug('reponse:', stor.zoneInput.reponse)
  const eltInfo = phraseBloc(stor.elts.divEnonce, textes.info, {
    p: j3pVirgule(stor.larrondi)
  })
  j3pStyle(eltInfo, { paddingTop: '10px', fontStyle: 'italic', fontSize: '0.9em' })
  stor.fctsValid = new ValidationZones({
    parcours: me,
    zones: [stor.zoneInput]
  })
  j3pFocus(stor.zoneInput)
  // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvent dans le cadre MD)
  // Il n’est ainsi construit qu’une seule fois et vidé à chaque passage dans correction()
  stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.moyen.correction })
  j3pStyle(stor.zoneD, { padding: '10px' })
  stor.zoneCalc = j3pAddElt(stor.zoneD, 'div')
  stor.zoneCorr = j3pAddElt(stor.zoneD, 'div')
  stor.calc = j3pGetNewId('Calculatrice')
  j3pStyle(stor.zoneCorr, { paddingTop: '10px' })
  me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
  j3pCreeFenetres(me)
  j3pAjouteBouton(stor.zoneCalc, j3pToggleFenetres.bind(null, 'Calculatrice'), { id: stor.calc, className: 'MepBoutons', value: 'Calculatrice' })
  j3pAfficheCroixFenetres('Calculatrice')
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me, bonneRep = false) {
  const stor = me.storage
  const ds = me.donneesSection
  j3pDetruit(stor.zoneCalc)
  j3pDetruitFenetres('Calculatrice')
  const zoneExpli = j3pAddElt(stor.elts.divEnonce, 'div', '', { style: me.styles.petit.correction })
  // La couleur de la correction sera fonction de la qualité de la réponse de l’élève
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneRep) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  let tabIndexCorr
  if ((ds.questionInter === 'aucune') || (me.questionCourante % ds.nbetapes === 0)) {
    // taux moyen
    tabIndexCorr = [5, 6, 7, 8]
  } else {
    tabIndexCorr = (ds.questionInter === 'evolutions successives') ? [1, 2] : [4]
  }
  for (const i of tabIndexCorr) afficheBloc(zoneExpli, textes['corr' + i], stor.objVariables)
  if ((ds.questionInter === 'aucune') || (me.questionCourante % ds.nbetapes === 0)) {
    phraseBloc(zoneExpli, (stor.objVariables.tauxMoy > 0) ? textes.corr9 : textes.corr9Moins, stor.objVariables)
  } else {
    if (ds.questionInter === 'evolutions successives') phraseBloc(zoneExpli, (stor.objVariables.valTaux > 0) ? textes['corr3_' + stor.choixSujet] : textes['corr3Moins_' + stor.choixSujet], stor.objVariables)
  }
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number|null}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  if (reponse.aRepondu) {
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
}

function correction (me) {
  const stor = me.storage
  const ds = me.donneesSection
  const score = getScore(me) // null si pas répondu
  if ((score === 1) || (me.essaiCourant >= ds.nbchances)) me.score += score
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.zoneCorr)
    return
  }
  if (score !== 1) {
    // On écrit c’est faux
    me.reponseKo(stor.zoneCorr)
    // Je vérifie si la réponse donnée n’est pas juste un pb d’arrondi
    if (Math.abs(j3pNombre(stor.fctsValid.zones.reponseSaisie[0]) - stor.zoneInput.reponse[0]) < stor.larrondi * 10) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, textes.comment1, true)
    }
    if (stor.zoneInput.reponse[0] * j3pNombre(stor.fctsValid.zones.reponseSaisie[0]) < 0) {
      // C’est qu’il y a un pb de signe
      const rmqSigne = (stor.zoneInput.reponse[0] > 0) ? textes.comment2 : textes.comment3
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, rmqSigne, true)
    }
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if ((score !== 1) && me.essaiCourant < me.donneesSection.nbchances) {
    me.reponseKo(stor.zoneCorr, '<br>' + essaieEncore, true)
    return
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.zoneCorr)
    // Même dans le cas d’une bonne réponse, on peut afficher la correction qui aurait dans ce cas d’éventuels compléments d’information
    afficheCorrection(me, true)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, tempsDepasse, true)
    }
    afficheCorrection(me)
  }
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (me.sectionTerminee()) {
    // fin de section, on affiche un recap global (ou pas…)

    // on peut aussi calculer la pe retournée

    // et on affiche ce bout
    me.afficheBoutonSectionSuivante(true)
  } else {
    me.etat = 'enonce'
    // on laisse le bouton suite mis par le finCorrection() précédent
    me.finNavigation(true)
  }
  // Si on a rien à afficher de particulier après la dernière répétition, donc on passe true à finNavigation
  // pour que le modèle affiche directement le bouton "section suivante" (sans devoir cliquer sur 'suite' avant)
  // me.finNavigation(true)
}
