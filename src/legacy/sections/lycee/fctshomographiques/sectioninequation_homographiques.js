import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pDiv, j3pElement, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pPaletteMathquill, j3pPGCD, j3pRandomTab } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pCreeBoutonsFenetre } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    août 2014
    On donne une inéquation de la forme f(x)<g(x) où f est une fonction homographique et g est homographique, affine ou constante.
    On demande d’e réduire cette inéquation d’écrire cette inéquation sous la forme h(x)<0
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['type_fct2', 'constante', 'liste', 'nature de la deuxième fonction', ['constante', 'affine', 'homographique']],
    ['imposer_fcts', ['', ''], 'array', 'on peut imposer chacune des deux fonctions sous la forme "(ax+b)/(cx+d)" ou "a/(cx+d)" ou "a"...'],
    ['imposer_ineq', '', 'string', 'on peut imposer l’inégalité : "<", ">" ">=" ou "<="']
  ]

}

/**
 * section inequation_homographiques
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function ppcm (x, y) {
    return Math.abs(x) * Math.abs(y) / j3pPGCD(x, y, { returnOtherIfZero: true })
  }
  function ecrireFctAffine (coefa, coefb, ordre) {
    // Cette fonction revoie une chaine de caractère donnant la fonction affine ax+b
    // ordre vaut "alea", "ax+b" ou "b+ax"
    let choix
    if (ordre === 'alea') {
      choix = j3pGetRandomInt(0, 1)
    } else if (ordre === 'ax+b') {
      choix = 0
    } else {
      choix = 1
    }
    let expressAffine
    if (Math.abs(coefb) < Math.pow(10, -12)) {
      // f(x) = ax
      expressAffine = j3pMonome(1, 1, coefa)
    } else if (Math.abs(coefa) < Math.pow(10, -12)) {
      // f(x) = b
      expressAffine = j3pMonome(1, 0, coefb)
    } else {
      if (choix === 0) {
        // f(x) = ax+b
        expressAffine = j3pMonome(1, 1, coefa) + j3pMonome(2, 0, coefb)
      } else {
        // f(x) = b+ax
        expressAffine = j3pMonome(1, 0, coefb) + j3pMonome(2, 1, coefa)
      }
    }
    return expressAffine
  }
  function choixFct (type) {
    // fonction qui permet de définir de manière aléatoire une fonction
    // type peut valoir "constante" (non nulle), "affine" ou "homographique"
    // cette fonction renvoie cinq propriétés : expression (au format latex), valzero (le zéro de la fonction - vaut "" s’il n’y en a pas), valinterdite (valeur interdite de la fonction - vaut "" s’il n’y en a pas), expressZero et expressInterdite (ces deux derniers au format latex)
    let fdex
    let coefA, coefB, coefC, coefD
    let valzero
    let valinterdite
    let expressZero
    let expressInterdite
    let expressNum, expressDen
    let lepgcd
    if (type === 'constante') {
      coefA = 0
      coefC = 0
      coefD = 1
      do {
        fdex = j3pGetRandomInt(-6, 6)
      } while (Math.abs(fdex) < Math.pow(10, -12))
      coefB = fdex
      fdex = String(fdex)
      valzero = ''
      valinterdite = ''
      expressZero = ''
      expressInterdite = ''
      expressNum = fdex
      expressDen = '1'
    } else if (type === 'affine') {
      do {
        coefA = j3pGetRandomInt(-6, 6)
        coefB = j3pGetRandomInt(-6, 6)
      } while (Math.abs(coefA) < Math.pow(10, -12))
      coefC = 0
      coefD = 1
      const choixEcriture = j3pGetRandomInt(0, 1)
      if (choixEcriture === 0) {
        // la fct sera écrite sous la forme ax+b
        fdex = ecrireFctAffine(coefA, coefB, 'ax+b')
      } else {
        // la fct sera écrite sous la forme b+ax
        fdex = ecrireFctAffine(coefA, coefB, 'b+ax')
      }
      if (Math.abs(Math.round(-coefB / coefA) - (-coefB / coefA)) < Math.pow(10, -12)) {
        // le zero est un entier
        expressZero = String(-coefB / coefA)
      } else {
        lepgcd = j3pPGCD(Math.abs(coefA), Math.abs(coefB), { returnOtherIfZero: true })
        if (coefB * coefA < 0) {
          expressZero = '\\frac{' + String(Math.abs(coefB / lepgcd)) + '}{' + String(Math.abs(coefA / lepgcd)) + '}'
        } else {
          expressZero = '-\\frac{' + String(Math.abs(coefB / lepgcd)) + '}{' + String(Math.abs(coefA / lepgcd)) + '}'
        }
      }
      valzero = -coefB / coefA
      valinterdite = ''
      expressInterdite = ''
      expressNum = j3pMonome(1, 1, coefA) + j3pMonome(2, 0, coefB)
      expressDen = '1'
    } else {
      // la fonction est homographique
      let zeroNum, zeroDen, pbQuotientNonSimplifie
      do {
        do {
          coefA = j3pGetRandomInt(-6, 6)
          coefB = j3pGetRandomInt(-6, 6)
          // coefA et coefB ne peuvent être nuls en même temps
        } while ((Math.abs(coefA) < Math.pow(10, -12)) && (Math.abs(coefB) < Math.pow(10, -12)))
        const pgcd1 = j3pPGCD(Math.abs(coefA), Math.abs(coefB), { returnOtherIfZero: true })
        zeroNum = -coefB / coefA
        do {
          coefC = j3pGetRandomInt(-6, 6)
        } while (Math.abs(coefC) < Math.pow(10, -12))
        do {
          coefD = j3pGetRandomInt(-6, 6)
        } while (Math.abs(coefD) < Math.pow(10, -12))
        // ici j’impose que coefD soit non nul'
        zeroDen = -coefD / coefC
        const pgcd2 = j3pPGCD(Math.abs(coefC), Math.abs(coefD), { returnOtherIfZero: true })
        pbQuotientNonSimplifie = (j3pPGCD(pgcd1, pgcd2, { returnOtherIfZero: true }) > 1)
      } while ((Math.abs(zeroNum - zeroDen) < Math.pow(10, -12)) || pbQuotientNonSimplifie)
      // coefA peut être nul dans ce cas
      const choixEcriturenum = j3pGetRandomInt(0, 1)
      const choixEcritureden = j3pGetRandomInt(0, 1)
      let num
      let den
      if (choixEcriturenum === 0) {
        // la fct sera écrite sous la forme ax+b
        num = ecrireFctAffine(coefA, coefB, 'ax+b')
      } else {
        // la fct sera écrite sous la forme b+ax
        num = ecrireFctAffine(coefA, coefB, 'b+ax')
      }
      if (choixEcritureden === 0) {
        // la fct sera écrite sous la forme ax+b
        den = ecrireFctAffine(coefC, coefD, 'ax+b')
      } else {
        // la fct sera écrite sous la forme b+ax
        den = ecrireFctAffine(coefC, coefD, 'b+ax')
      }
      fdex = '\\frac{' + num + '}{' + den + '}'
      if (Math.abs(Math.round(-coefB / coefA) - (-coefB / coefA)) < Math.pow(10, -12)) {
        // le zero est un entier
        expressZero = String(-coefB / coefA)
      } else {
        lepgcd = j3pPGCD(Math.abs(coefA), Math.abs(coefB), { returnOtherIfZero: true })
        if (coefB * coefA < 0) {
          expressZero = '\\frac{' + String(Math.abs(coefB / lepgcd)) + '}{' + String(Math.abs(coefA / lepgcd)) + '}'
        } else {
          expressZero = '-\\frac{' + String(Math.abs(coefB / lepgcd)) + '}{' + String(Math.abs(coefA / lepgcd)) + '}'
        }
      }
      if (Math.abs(Math.round(-coefD / coefC) - (-coefD / coefC)) < Math.pow(10, -12)) {
        // la valeur interdite est un entier
        expressInterdite = String(-coefD / coefC)
      } else {
        const lepgcd2 = j3pPGCD(Math.abs(coefC), Math.abs(coefD), { returnOtherIfZero: true })
        if (coefC * coefD < 0) {
          expressInterdite = '\\frac{' + String(Math.abs(coefD / lepgcd2)) + '}{' + String(Math.abs(coefC / lepgcd2)) + '}'
        } else {
          expressInterdite = '-\\frac{' + String(Math.abs(coefD / lepgcd2)) + '}{' + String(Math.abs(coefC / lepgcd2)) + '}'
        }
      }
      valzero = -coefB / coefA
      valinterdite = -coefD / coefC
      expressNum = num// j3pMonome(1,1,coefA)+j3pMonome(2,0,coefB);
      expressDen = den// j3pMonome(1,1,coefC)+j3pMonome(2,0,coefD);
    }

    me.logIfDebug('fdex : ' + fdex)
    return {
      expression: fdex,
      coefstab: [coefA, coefB, coefC, coefD],
      zero: valzero,
      zero_latex: expressZero,
      interdite: valinterdite,
      interdite_latex: expressInterdite,
      expressNum,
      expressDen
    }
  }
  function eltsFct (fctTxt) {
    // on donne une fonction homographique ou affine ou constante
    // on cherche la valeur des coefs
    let imposerFctTab = []
    const arbreImposerFct = []
    let coefA, coefB, coefC, coefD
    if (fctTxt.indexOf('/') > -1) {
      // c’est une fonction homographique'
      imposerFctTab = fctTxt.split('/')
      if (imposerFctTab[0].charAt(0) === '(') {
        imposerFctTab[0] = imposerFctTab[0].substring(1, imposerFctTab[0].length)
      }
      if (imposerFctTab[0].charAt(imposerFctTab[0].length - 1) === ')') {
        imposerFctTab[0] = imposerFctTab[0].substring(0, imposerFctTab[0].length - 1)
      }
      if (imposerFctTab[1].charAt(0) === '(') {
        imposerFctTab[1] = imposerFctTab[1].substring(1, imposerFctTab[1].length)
      }
      if (imposerFctTab[1].charAt(imposerFctTab[1].length - 1) === ')') {
        imposerFctTab[1] = imposerFctTab[1].substring(0, imposerFctTab[1].length - 1)
      }
      // la premiere fonction
      arbreImposerFct[0] = new Tarbre(imposerFctTab[0], ['x'])
      coefB = arbreImposerFct[0].evalue([0])
      coefA = arbreImposerFct[0].evalue([1]) - arbreImposerFct[0].evalue([0])
      arbreImposerFct[1] = new Tarbre(imposerFctTab[1], ['x'])
      coefD = arbreImposerFct[1].evalue([0])
      coefC = arbreImposerFct[1].evalue([1]) - arbreImposerFct[1].evalue([0])
      let num = imposerFctTab[0]
      if ((imposerFctTab[0][0] === '(') && (imposerFctTab[0][imposerFctTab[0].length - 1] === ')')) {
        num = imposerFctTab[0].substring(1, imposerFctTab[0].length - 1)
      }
      let den = imposerFctTab[1]
      if ((imposerFctTab[1][0] === '(') && (imposerFctTab[1][imposerFctTab[1].length - 1] === ')')) {
        den = imposerFctTab[1].substring(1, imposerFctTab[1].length - 1)
      }
      return {
        natureFct: 'homographique',
        coefstab: [coefA, coefB, coefC, coefD],
        express_latex: '\\frac{' + num + '}{' + den + '}',
        expressNum: num,
        expressDen: den
      }
    } else {
      // on n’a qu’une fonction affine (qui peut être constante)'
      const arbre2ImposerFct = new Tarbre(fctTxt, ['x'])
      coefB = arbre2ImposerFct.evalue([0])
      coefA = arbre2ImposerFct.evalue([1]) - arbre2ImposerFct.evalue([0])
      coefC = 0
      coefD = 1
      let expressNum, expressDen, natureFct
      if (Math.abs(coefA) < Math.pow(10, -12)) {
        natureFct = 'constante'
        expressNum = coefB
        expressDen = 1
      } else {
        natureFct = 'affine'
        if (Math.abs(coefB) < Math.pow(10, -12)) {
          expressNum = j3pMonome(1, 1, coefA)
        } else {
          expressNum = j3pMonome(1, 1, coefA) + j3pMonome(2, 0, coefB)
        }
      }
      return {
        natureFct,
        coefstab: [coefA, coefB, coefC, coefD],
        express_latex: fctTxt,
        expressDen,
        expressNum
      }
    }
  }

  function afficheCorrection (bonneReponse) {
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pDetruit(stor.zoneBtn)
    let ineq1, ineq2
    if (stor.natureFct2 === 'homographique') {
      ineq1 = stor.expressFct1 + '-' + stor.expressFct2 + stor.signeIneq + '0'
    } else if (stor.natureFct2 === 'affine') {
      if ((stor.coefs_fct2[1] === 0) && (stor.coefs_fct2[0] > 0)) {
        ineq1 = stor.expressFct1 + '-' + stor.expressFct2 + stor.signeIneq + '0'
      } else {
        ineq1 = stor.expressFct1 + '-(' + stor.expressFct2 + ')' + stor.signeIneq + '0'
      }
    } else { // fct2 est une fonction constante
      ineq1 = stor.expressFct1 + j3pMonome(2, 0, -stor.coefs_fct2[1]) + stor.signeIneq + '0'
    }
    let denDerniereIneq, determinantCoefDen, coefM, coefN
    if (stor.natureFct1 === 'homographique') {
      if (stor.natureFct2 === 'homographique') {
        // recherche du dénominateur commun
        determinantCoefDen = stor.coefs_fct1[2] * stor.coefs_fct2[3] - stor.coefs_fct2[2] * stor.coefs_fct1[3]
        let numPremiereFrac, denPremiereFrac, numDeuxiemeFrac, denDeuxiemeFrac
        if (Math.abs(determinantCoefDen) < Math.pow(10, -12)) {
          // on aura dans ce cas un dénominateur plus simple que celui proposé
          let coefCdenCommun = 0
          let coefDdenCommun = 0
          const ppcmC = ppcm(Math.abs(stor.coefs_fct1[2]), Math.abs(stor.coefs_fct2[2]))
          let iDeb = 1
          while (coefCdenCommun === 0) {
            coefM = iDeb * ppcmC / stor.coefs_fct2[2]
            coefN = coefM * stor.coefs_fct1[3] / stor.coefs_fct2[3]
            if ((coefM < 0) && (coefN < 0)) {
              coefM = -coefM
              coefN = -coefN
            }
            if ((Math.abs(Math.round(coefM) - coefM) < Math.pow(10, -12)) && (Math.abs(Math.round(coefN) - coefN) < Math.pow(10, -12))) {
              coefCdenCommun = stor.coefs_fct1[2] * coefM
              coefDdenCommun = stor.coefs_fct1[3] * coefM
            }
            iDeb++
          }
          let coefMtxt = String(coefM)
          let coefNtxt = String(coefN)
          if (coefM < 0) {
            coefMtxt = '(' + String(coefM) + ')'
          }
          if (coefN < 0) {
            coefNtxt = '(' + String(coefN) + ')'
          }

          if ((Math.abs(stor.coefs_fct1[1]) < Math.pow(10, -12)) || (Math.abs(stor.coefs_fct1[0]) < Math.pow(10, -12))) {
            // coefb ou coefa de la 1ère fct nul
            // donc on ne met pas de parenthèse sur le premier facteur
            if (Math.abs(coefM - 1) < Math.pow(10, -12)) {
              numPremiereFrac = stor.param_fcts[0][0]
            } else {
              numPremiereFrac = stor.param_fcts[0][0] + '\\times ' + coefMtxt
            }
          } else {
            if (Math.abs(coefM - 1) < Math.pow(10, -12)) {
              numPremiereFrac = stor.param_fcts[0][0]
            } else {
              numPremiereFrac = '(' + stor.param_fcts[0][0] + ')\\times ' + coefMtxt
            }
          }
          if ((Math.abs(stor.coefs_fct2[1]) < Math.pow(10, -12)) || (Math.abs(stor.coefs_fct2[0]) < Math.pow(10, -12))) {
            // coefb ou coefa de la 2ème fct nul
            // donc on ne met pas de parenthèse sur le premier facteur
            if (Math.abs(coefN - 1) < Math.pow(10, -12)) {
              numDeuxiemeFrac = stor.param_fcts[1][0]
            } else {
              numDeuxiemeFrac = stor.param_fcts[1][0] + '\\times ' + coefNtxt
            }
          } else {
            if (Math.abs(coefN - 1) < Math.pow(10, -12)) {
              numDeuxiemeFrac = stor.param_fcts[1][0]
            } else {
              numDeuxiemeFrac = '(' + stor.param_fcts[1][0] + ')\\times ' + coefNtxt
            }
          }
          if (Math.abs(stor.coefs_fct1[3]) < Math.pow(10, -12)) {
            // coefD nul dans la première fraction, donc pas de parenthèses
            if (Math.abs(coefM - 1) < Math.pow(10, -12)) {
              denPremiereFrac = stor.param_fcts[0][1]
            } else {
              denPremiereFrac = stor.param_fcts[0][1] + '\\times ' + coefMtxt
            }
          } else {
            if (Math.abs(coefM - 1) < Math.pow(10, -12)) {
              denPremiereFrac = stor.param_fcts[0][1]
            } else {
              denPremiereFrac = '(' + stor.param_fcts[0][1] + ')\\times ' + coefMtxt
            }
          }
          if (Math.abs(stor.coefs_fct2[3]) < Math.pow(10, -12)) {
            // coefD nul dans la première fraction, donc pas de parenthèses
            if (Math.abs(coefN - 1) < Math.pow(10, -12)) {
              denDeuxiemeFrac = stor.param_fcts[1][1]
            } else {
              denDeuxiemeFrac = stor.param_fcts[1][1] + '\\times ' + coefNtxt
            }
          } else {
            if (Math.abs(coefN - 1) < Math.pow(10, -12)) {
              denDeuxiemeFrac = stor.param_fcts[1][1]
            } else {
              denDeuxiemeFrac = '(' + stor.param_fcts[1][1] + ')\\times ' + coefNtxt
            }
          }
          denDerniereIneq = j3pMonome(1, 1, coefCdenCommun) + j3pMonome(2, 0, coefDdenCommun)
        } else {
          if ((Math.abs(stor.coefs_fct1[1]) < Math.pow(10, -12)) || (Math.abs(stor.coefs_fct1[0]) < Math.pow(10, -12))) {
            // coefb ou coefa de la 1ère fct nul
            // donc on ne met pas de parenthèse sur le premier facteur
            if ((Math.abs(stor.coefs_fct2[3]) < Math.pow(10, -12)) && (stor.coefs_fct2[2] > 0)) {
              // si coefd de la deuxième fonction est nul et coefc de la deuxième fonction est positif
              // là aussi pas de parenthèses sur le deuxième facteur
              numPremiereFrac = stor.param_fcts[0][0] + '\\times ' + stor.param_fcts[1][1]
            } else {
              numPremiereFrac = stor.param_fcts[0][0] + '(' + stor.param_fcts[1][1] + ')'
            }
          } else {
            if ((Math.abs(stor.coefs_fct2[3]) < Math.pow(10, -12)) && (stor.coefs_fct2[2] > 0)) {
              // si coefd de la deuxième fonction est nul et coefc de la deuxième fonction est positif
              // là aussi pas de parenthèses sur le deuxième facteur
              numPremiereFrac = '(' + stor.param_fcts[0][0] + ')\\times ' + stor.param_fcts[1][1]
            } else {
              numPremiereFrac = '(' + stor.param_fcts[0][0] + ')(' + stor.param_fcts[1][1] + ')'
            }
          }
          if (Math.abs(stor.coefs_fct1[3]) < Math.pow(10, -12)) {
            // coefd de la 1ère fct nul
            // donc on ne met pas de parenthèse sur le premier facteur
            if ((Math.abs(stor.coefs_fct2[3]) < Math.pow(10, -12)) && (stor.coefs_fct2[2] > 0)) {
              // si coefd de la deuxième fonction est nul et coefc de la deuxième fonction est positif
              // là aussi pas de parenthèses sur le deuxième facteur
              denPremiereFrac = stor.param_fcts[0][1] + '\\times ' + stor.param_fcts[1][1]
            } else {
              denPremiereFrac = stor.param_fcts[0][1] + '(' + stor.param_fcts[1][1] + ')'
            }
          } else {
            if ((Math.abs(stor.coefs_fct2[3]) < Math.pow(10, -12)) && (stor.coefs_fct2[2] > 0)) {
              // si coefd de la deuxième fonction est nul et coefc de la deuxième fonction est positif
              // là aussi pas de parenthèses sur le deuxième facteur
              denPremiereFrac = '(' + stor.param_fcts[0][1] + ')\\times ' + stor.param_fcts[1][1]
            } else {
              denPremiereFrac = '(' + stor.param_fcts[0][1] + ')(' + stor.param_fcts[1][1] + ')'
            }
          }
          if ((Math.abs(stor.coefs_fct2[1]) < Math.pow(10, -12)) || (Math.abs(stor.coefs_fct2[0]) < Math.pow(10, -12))) {
            // coefb ou coefa de la 2ème fct nul
            // donc on ne met pas de parenthèse sur le premier facteur
            if ((Math.abs(stor.coefs_fct1[3]) < Math.pow(10, -12)) && (stor.coefs_fct1[2] > 0)) {
              // si coefd de la première fonction est nul et coefc de la première fonction est positif
              // là aussi pas de parenthèses sur le deuxième facteur
              numDeuxiemeFrac = stor.param_fcts[1][0] + '\\times ' + stor.param_fcts[0][1]
            } else {
              numDeuxiemeFrac = stor.param_fcts[1][0] + '(' + stor.param_fcts[0][1] + ')'
            }
          } else {
            if ((Math.abs(stor.coefs_fct1[3]) < Math.pow(10, -12)) && (stor.coefs_fct1[2] > 0)) {
              // si coefd de la première fonction est nul et coefc de la première fonction est positif
              // là aussi pas de parenthèses sur le deuxième facteur
              numDeuxiemeFrac = '(' + stor.param_fcts[1][0] + ')\\times ' + stor.param_fcts[0][1]
            } else {
              numDeuxiemeFrac = '(' + stor.param_fcts[1][0] + ')(' + stor.param_fcts[0][1] + ')'
            }
          }
          if (Math.abs(stor.coefs_fct2[3]) < Math.pow(10, -12)) {
            // coefd de la 2ème fct nul
            // donc on ne met pas de parenthèse sur le premier facteur
            if ((Math.abs(stor.coefs_fct1[3]) < Math.pow(10, -12)) && (stor.coefs_fct1[2] > 0)) {
              // si coefd de la premiere fonction est nul et coefc de la première fonction est positif
              // là aussi pas de parenthèses sur le deuxième facteur
              denDeuxiemeFrac = stor.param_fcts[1][1] + '\\times ' + stor.param_fcts[0][1]
            } else {
              denDeuxiemeFrac = stor.param_fcts[1][1] + '(' + stor.param_fcts[0][1] + ')'
            }
          } else {
            if ((Math.abs(stor.coefs_fct1[3]) < Math.pow(10, -12)) && (stor.coefs_fct1[2] > 0)) {
              // si coefd de la première fonction est nul et coefc de la première fonction est positif
              // là aussi pas de parenthèses sur le deuxième facteur
              denDeuxiemeFrac = '(' + stor.param_fcts[1][1] + ')\\times ' + stor.param_fcts[0][1]
            } else {
              denDeuxiemeFrac = '(' + stor.param_fcts[1][1] + ')(' + stor.param_fcts[0][1] + ')'
            }
          }
          denDerniereIneq = denPremiereFrac
        }
        ineq2 = '\\frac{' + numPremiereFrac + '}{' + denPremiereFrac + '}-\\frac{' + numDeuxiemeFrac + '}{' + denDeuxiemeFrac + '}' + stor.signeIneq + '0'
      } else if (stor.natureFct2 === 'affine') {
        // stor.param_fcts = [[expressNum1, expressDen1], [expressNum2, expressDen2]];
        // stor.coefs_fct1 = fonction1.coefstab;
        if ((stor.coefs_fct2[1] === 0) && (stor.coefs_fct2[0] > 0)) {
          ineq2 = stor.expressFct1 + '-\\frac{' + stor.expressFct2 + '(' + stor.param_fcts[0][1] + ')}{' + stor.param_fcts[0][1] + '}' + stor.signeIneq + '0'
        } else {
          ineq2 = stor.expressFct1 + '-\\frac{(' + stor.expressFct2 + ')(' + stor.param_fcts[0][1] + ')}{' + stor.param_fcts[0][1] + '}' + stor.signeIneq + '0'
        }
        denDerniereIneq = stor.param_fcts[0][1]
      } else { // fct2 est une fonction constante
        if (stor.coefs_fct2[1] < 0) {
          if (Math.abs(stor.coefs_fct2[1] + 1) < Math.pow(10, -12)) {
            ineq2 = stor.expressFct1 + '+\\frac{' + stor.param_fcts[0][1] + '}{' + stor.param_fcts[0][1] + '}' + stor.signeIneq + '0'
          } else {
            ineq2 = stor.expressFct1 + '+\\frac{' + (-stor.coefs_fct2[1]) + '(' + stor.param_fcts[0][1] + ')}{' + stor.param_fcts[0][1] + '}' + stor.signeIneq + '0'
          }
        } else {
          if (Math.abs(stor.coefs_fct2[1] - 1) < Math.pow(10, -12)) {
            ineq2 = stor.expressFct1 + '-\\frac{' + stor.param_fcts[0][1] + '}{' + stor.param_fcts[0][1] + '}' + stor.signeIneq + '0'
          } else {
            ineq2 = stor.expressFct1 + '-\\frac{' + stor.expressFct2 + '(' + stor.param_fcts[0][1] + ')}{' + stor.param_fcts[0][1] + '}' + stor.signeIneq + '0'
          }
        }
        denDerniereIneq = stor.param_fcts[0][1]
      }
    } else if (stor.natureFct1 === 'affine') {
      // dans ce cas stor.natureFct2 vaut nécessairement "homographique"
      if (stor.coefs_fct2[1] === 0) {
        ineq2 = '\\frac{' + stor.expressFct2 + '(' + stor.param_fcts[1][1] + ')}{' + stor.param_fcts[1][1] + '}-' + stor.expressFct2 + stor.signeIneq + '0'
      } else {
        ineq2 = '\\frac{(' + stor.expressFct2 + ')(' + stor.param_fcts[1][1] + ')}{' + stor.param_fcts[1][1] + '}-' + stor.expressFct2 + stor.signeIneq + '0'
      }
      denDerniereIneq = stor.param_fcts[1][1]
    } else { // stor.natureFct1 === "constante
      ineq2 = '\\frac{(' + stor.expressFct2 + ')(' + stor.param_fcts[1][1] + ')}{' + stor.param_fcts[1][1] + '}-' + stor.expressFct2 + stor.signeIneq + '0'
      denDerniereIneq = stor.param_fcts[1][1]
    }
    me.logIfDebug('ineq1:' + ineq1 + '   ineq2:' + ineq2)
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, { i: ineq1, j: ineq2 }) // dernière inégalité
    // gestion du numérateur (version simplifiée)
    let coefCstNum, coefXnum
    // Parfois, on peut trouver un terme en x^2 au numérateur. On détermine alors le coef de ce monome
    const coefCarreNum = stor.coefs_fct1[0] * stor.coefs_fct2[2] - stor.coefs_fct2[0] * stor.coefs_fct1[2]
    // determinantCoefDen n’existe que dans le cas où la fonction de droite est homographique (idem pour coefM et coefN)
    if ((stor.natureFct2 === 'homographique') && (Math.abs(determinantCoefDen) < Math.pow(10, -12))) {
      coefCstNum = stor.coefs_fct1[1] * coefM - stor.coefs_fct2[1] * coefN
      coefXnum = stor.coefs_fct1[0] * coefM - stor.coefs_fct2[0] * coefN
    } else {
      coefCstNum = stor.coefs_fct1[1] * stor.coefs_fct2[3] - stor.coefs_fct1[3] * stor.coefs_fct2[1]
      coefXnum = stor.coefs_fct1[0] * stor.coefs_fct2[3] + stor.coefs_fct1[1] * stor.coefs_fct2[2] - stor.coefs_fct2[1] * stor.coefs_fct1[2] - stor.coefs_fct1[3] * stor.coefs_fct2[0]
    }
    me.logIfDebug('stor.coefs_fct:', stor.coefs_fct1, stor.coefs_fct2)
    let ineq3
    if (Math.abs(coefCarreNum) > Math.pow(10, -12)) {
      ineq3 = '\\frac{' + j3pMonome(1, 2, coefCarreNum) + j3pMonome(2, 1, coefXnum) + j3pMonome(3, 0, coefCstNum) + '}{' + denDerniereIneq + '}' + stor.signeIneq + '0'
    } else {
      if (Math.abs(coefXnum) > Math.pow(10, -12)) {
        ineq3 = '\\frac{' + j3pMonome(1, 1, coefXnum) + j3pMonome(2, 0, coefCstNum) + '}{' + denDerniereIneq + '}' + stor.signeIneq + '0'
      } else {
        ineq3 = '\\frac{' + coefCstNum + '}{' + denDerniereIneq + '}' + stor.signeIneq + '0'
      }
    }
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2, { i: ineq3 })
  }

  function chargePaletteBrouillon (zonetexte) {
    const listeMq = ['fraction', 'supegal', 'infegal', 'equivaut']
    const container = j3pElement(stor.idPaletteBr)
    j3pPaletteMathquill(container, zonetexte, { liste: listeMq })
  }
  function aide () {
    // cette foncton permet de donner accès à un brouillon
    stor.nameBtn = j3pGetNewId('Boutons')
    stor.fenetreBtns = j3pGetNewId('fenetreBoutons')
    stor.nameBrouillon = j3pGetNewId('Brouillon')
    stor.fenetreBrouillon = j3pGetNewId('fenetreBrouillon')
    const untableau = [
      {
        name: stor.nameBtn,
        title: ' ',
        left: me.zonesElts.MG.getBoundingClientRect().width + 15,
        top: me.zonesElts.ID.getBoundingClientRect().height,
        id: stor.fenetreBtns
      }
    ]
    const titreAide = ds.textes.aide
    untableau.push({
      name: stor.nameBrouillon,
      title: titreAide,
      top: me.zonesElts.ID.getBoundingClientRect().height + 30,
      // left: Math.round(j3pElement('j3pContainer').offsetWidth / 2),
      // Ligne suivante changée par Yves car plante dans SesaParcours
      left: Math.round((me.zonesElts.MG.getBoundingClientRect().width + me.zonesElts.MD.getBoundingClientRect().width) / 2),
      width: 500,
      height: 300,
      id: stor.fenetreBrouillon
    })
    me.fenetresjq = untableau
    // Création mais sans affichage.
    j3pCreeFenetres(me)
    j3pCreeBoutonsFenetre(stor.fenetreBtns, [[stor.nameBrouillon, titreAide]])

    stor.zoneBtn = j3pAddElt(stor.conteneurD, 'div')
    j3pAjouteBouton(stor.zoneBtn, stor.nameBrouillon, 'MepBoutons', 'Brouillon', j3pToggleFenetres.bind(null, stor.nameBrouillon))
    j3pAfficheCroixFenetres(stor.nameBrouillon)
    stor.idPaletteBr = j3pGetNewId('Palette_br')
    const elt = j3pAffiche(stor.fenetreBrouillon, '', '&1&', { inputmq1: { texte: '' } })
    j3pDiv(stor.fenetreBrouillon, { id: stor.idPaletteBr, contenu: '<br>', style: me.styles.petit.enonce })
    // la palette
    const hauteurZone = '300px'
    chargePaletteBrouillon(elt.inputmqList[0])
    mqRestriction(elt.inputmqList[0], 'abcdefghijklmnopqrstuvwxyz\\d,.-x*+()/', { commandes: ['fraction', 'supegal', 'infegal', 'equivaut'] })
    elt.parent.style.height = hauteurZone
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,
      type_fct2: 'constante',
      imposer_fcts: ['', ''],
      imposer_ineq: '',
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      textes: {
        titreExo: 'Transformation d’une inéquation',
        cons1: 'On souhaite résoudre dans $\\R$ l’inéquation $£i$.',
        cons2: 'Ecrire une inéquation équivalente à la première sous la forme $\\frac{A(x)}{B(x)}£s 0$ (numérateur et dénominateur n’étant pas forcément réduits) :',
        comment1: '',
        corr1: 'L’inéquation équivaut à $£i$ c’est-à-dire $£j$.',
        corr2: 'On obtient ainsi l’inéquation $£i$.',
        aide: 'Brouillon'
      },

      pe: 0
    }
  }
  function enonceMain () {
    let expressFct1, expressFct2
    let expressNum1, expressDen1
    let expressNum2, expressDen2
    let natureFct1, natureFct2
    // creation de la premiere fonction
    if ((ds.imposer_fcts[0] === undefined) || (ds.imposer_fcts[0] === '')) {
      const fct1 = choixFct('homographique')
      expressFct1 = fct1.expression
      expressNum1 = fct1.expressNum
      expressDen1 = fct1.expressDen
      natureFct1 = 'homographique'
      stor.coefs_fct1 = fct1.coefstab
    } else {
      const fonction1 = eltsFct(ds.imposer_fcts[0])
      expressFct1 = fonction1.express_latex
      expressNum1 = fonction1.expressNum// coefstab[0]+"x"+fonction1.coefstab[1];
      expressDen1 = fonction1.expressDen// coefstab[2]+"x"+fonction1.coefstab[3];
      natureFct1 = fonction1.natureFct
      stor.coefs_fct1 = fonction1.coefstab
    }
    me.logIfDebug('expressNum1:' + expressNum1 + '   expressDen1:' + expressDen1)
    // puis de la deuxième
    if ((ds.imposer_fcts[1] === undefined) || (ds.imposer_fcts[1] === '')) {
      const fct2 = choixFct(ds.type_fct2)
      expressFct2 = fct2.expression
      expressNum2 = fct2.expressNum
      expressDen2 = fct2.expressDen
      natureFct2 = ds.type_fct2
      stor.coefs_fct2 = fct2.coefstab
    } else {
      const fonction2 = eltsFct(ds.imposer_fcts[1])
      expressFct2 = fonction2.express_latex
      expressNum2 = fonction2.expressNum// coefstab[0]+"x"+fonction2.coefstab[1];
      expressDen2 = fonction2.expressDen// coefstab[2]+"x"+fonction2.coefstab[3];
      natureFct2 = fonction2.natureFct
      stor.coefs_fct2 = fonction2.coefstab
    }
    me.logIfDebug('expressNum2:' + expressNum2 + '   expressDen2:' + expressDen2)
    let signeIneq
    if (ds.imposer_ineq === '<') {
      signeIneq = '<'
    } else if (ds.imposer_ineq === '>') {
      signeIneq = '>'
    } else if (ds.imposer_ineq === '<=') {
      signeIneq = '\\leq'
    } else if (ds.imposer_ineq === '>=') {
      signeIneq = '\\geq'
    } else {
      signeIneq = j3pRandomTab(['<', '>', '\\leq', '\\geq'], [0.25, 0.25, 0.25, 0.25])
    }
    const ineq = expressFct1 + signeIneq + expressFct2

    // et maintenant le texte (à faire après le brouillon pour conserver le focus (sinon je n’arrive pas à le redonner...)'
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 3; i++)stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.cons1, { i: ineq })
    j3pAffiche(stor.zoneCons2, '', ds.textes.cons2, { s: signeIneq })
    const expression = '$\\frac{\\editable{}}{\\editable{}}' + signeIneq + '0$'
    const elt = j3pAffiche(stor.zoneCons3, '', expression)
    stor.zoneInput = [...elt.inputmqefList]
    j3pFocus(stor.zoneInput[0])
    mqRestriction(stor.zoneInput[0], '\\d,.-x*+()')
    mqRestriction(stor.zoneInput[1], '\\d,.-x*+()')
    stor.natureFct1 = natureFct1
    stor.natureFct2 = natureFct2
    stor.expressFct1 = expressFct1
    stor.expressFct2 = expressFct2
    stor.signeIneq = signeIneq
    stor.param_fcts = [[expressNum1, expressDen1], [expressNum2, expressDen2]]
    let denominateurAttendu, numerateurAttendu
    if (stor.natureFct1 === 'homographique') {
      if (stor.natureFct2 === 'homographique') {
      // denominateur commun (je prends le produit)
        denominateurAttendu = '(' + stor.param_fcts[0][1] + ')*(' + stor.param_fcts[1][1] + ')'
        // pour le numérateur, je ne cherche pas ici la version simplifiée
        numerateurAttendu = '(' + stor.param_fcts[0][0] + ')*(' + stor.param_fcts[1][1] + ')-(' + stor.param_fcts[1][0] + ')*(' + stor.param_fcts[0][1] + ')'
      } else {
      // pas de dénominateur pour la deuxième fonction
      // denominateur commun (c’est celui de la fonction 1)
        denominateurAttendu = stor.param_fcts[0][1]
        // pour le numérateur, je ne cherche pas ici la version simplifiée
        numerateurAttendu = '(' + stor.param_fcts[0][0] + ')-(' + stor.param_fcts[1][0] + ')*(' + stor.param_fcts[0][1] + ')'
      }
    } else {
      if (stor.natureFct2 === 'homographique') {
      // denominateur commun (je prends celui de la fonction 2)
        denominateurAttendu = stor.param_fcts[1][1]
        // pour le numérateur, je ne cherche pas ici la version simplifiée
        numerateurAttendu = '(' + stor.param_fcts[0][0] + ')*(' + stor.param_fcts[1][1] + ')-(' + stor.param_fcts[1][0] + ')'
      } else {
      // aucune des deux fonction n’a de dénominateur (cas qui ne devrait pas se produire, sauf s on impose ces fonctions - ce qui est ridicule)'
      // denominateur commun (c’est celui de la fonction 1)
        denominateurAttendu = '1'
        // pour le numérateur, je ne cherche pas ici la version simplifiée
        numerateurAttendu = '(' + stor.param_fcts[0][0] + ')-(' + stor.param_fcts[1][0] + ')'
      }
    }
    stor.denominateurAttendu = denominateurAttendu
    stor.numerateurAttendu = numerateurAttendu
    me.logIfDebug('numerateurAttendu:' + numerateurAttendu + '   denominateurAttendu:' + denominateurAttendu)
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneBtn = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCorr.style.paddingTop = '10px'
    // création de la zone de brouillon
    aide()
    const zonesSaisie = stor.zoneInput.map(elt => elt.id)
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: zonesSaisie,
      validePerso: zonesSaisie
    })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 0.6

        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titreExo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      enonceMain()
      break // case "enonce":
    }
    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie

      const reponse = fctsValid.validationGlobale()
      reponse.bonneReponse = true// il faut que je l’initiale à false car validationGlobale ne donne que aRepondu
      if (me.isElapsed) {
        reponse.bonneReponse = false
      }
      if (reponse.aRepondu) {
        // les deux zones de saisie doivent être testées de manière manuelle
        let bonneReponse2 = true
        // recuperation des réponses de l’élève
        let repEleve1 = $(stor.zoneInput[0]).mathquill('latex')
        let repEleve2 = $(stor.zoneInput[1]).mathquill('latex')
        me.logIfDebug('repEleve1:' + repEleve1 + '   repEleve2:' + repEleve2)
        // gestion du pb des parenthèses : \left et \right sont à virer

        repEleve1 = j3pMathquillXcas(repEleve1)
        repEleve2 = j3pMathquillXcas(repEleve2)
        me.logIfDebug('Après modif, repEleve1:' + repEleve1 + '   repEleve2:' + repEleve2)
        const quotientEleve = '(' + repEleve1 + ')/(' + repEleve2 + ')'
        const arbreDif = new Tarbre(quotientEleve + '-(' + stor.numerateurAttendu + ')/(' + stor.denominateurAttendu + ')', ['x'])
        for (let i = 1.05; i <= 1.17; i += 0.01) {
          bonneReponse2 = (bonneReponse2 && (Math.abs(arbreDif.evalue([i])) < Math.pow(10, -12)))
        }
        if (!bonneReponse2) {
          reponse.bonneReponse = bonneReponse2
          fctsValid.zones.bonneReponse[0] = false
          fctsValid.zones.bonneReponse[1] = false
        }
        // on appelle de nouveau la fonction qui va mettre en couleur les réponses bonnes ou fausses
        fctsValid.coloreUneZone(stor.zoneInput[0].id)
        fctsValid.coloreUneZone(stor.zoneInput[1].id)
      }
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        me.afficheBoutonValider()
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (reponse.bonneReponse) {
          me._stopTimer()
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          afficheCorrection(true)
          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()

            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.cacheBoutonValider()
            /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
            j3pDesactive(stor.zoneInput[0])
            j3pDesactive(stor.zoneInput[1])
            afficheCorrection(false)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux

            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              afficheCorrection(false)
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
