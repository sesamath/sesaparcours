import $ from 'jquery'
import { j3pBarre, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetRandomInt, j3pMonome, j3pNombre, j3pPaletteMathquill, j3pPGCD, j3pGetBornesIntervalle, j3pStyle, j3pAddElt } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    mars 2014
    On demande à l’élève de déterminer le domaine de définition d’une fonction homographique

*/
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[-4;4]', 'string', 'entier a dans (ax+b)/(cx+d), il peut être nul si b ne l’est pas'],
    ['b', '[-7;7]', 'string', 'entier b dans (ax+b)/(cx+d), il peut être nul'],
    ['c', '[-4;4]', 'string', 'entier c dans (ax+b)/(cx+d), il ne peut pas être nul'],
    ['d', '[-7;7]', 'string', 'entier d dans (ax+b)/(cx+d), il peut être nul'],
    ['zero_entier', false, 'boolean', "On peut imposer à la valeur intrdite d'êre entière, sinon c’est aléatoire"]
  ]
}
const textes = {
  titre_exo: 'Domaine de définition d’une fonction homographique',
  consigne1: '$£f$ est une fonction homographique définie par : <br>$£g$.',
  consigne2: 'Son ensemble (ou domaine) de définition est : <br>&1&.',
  phrase_erreur1: 'Il y a une erreur dans le calcul de la valeur interdite&nbsp;!',
  phrase_erreur2: 'Attention : les fractions doivent être irréductibles&nbsp;!',
  phrase_erreur3: 'Tu n’as donné qu’un seul bon intervalle de la réponse&nbsp;!',
  phrase_erreur4: 'Cependant la valeur interdite est bien présente dans ta réponse&nbsp;!',
  comment7: 'Attention à l’écriture de l’ensemble&nbsp;!',
  comment8: 'Le séparateur dans un intervalle doit être un point-virgule&nbsp;!',
  comment9: 'Il y a un souci dans l’écriture d’une fraction&nbsp;!',
  corr1: 'Pour que $£f$ soit définie, il faut que $£d$ ce qui donne<br>$£i$.',
  corr2: 'Donc $£f$ est définie sur $\\R\\setminus\\bracket{£b}=£a$.'
}

/**
 * section homographique_domaine
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  function decoupageCupcap (rep) {
    // cette fonction renvoit un tableau contenant les différentes parties séparées par \\cup ou par \\cap
    // ces différentes parties sont en principe des intervalles
    const cupcap = /\\cup|\\cap/g // new RegExp('\\\\cup|\\\\cap', 'g')
    return rep.split(cupcap)
  }
  function bonIntervalle (rep) {
    // rep est du texte (réponse de l’élève)
    // On teste si rep est bien un intervalle ou une réunon d’intervalle'
    // on vérifie aussi si l’élève ne met pas une virgule à la place du point-virgule quand il écrit un intervalle'
    let pbCrochet = false
    let pbPointVirgule = false
    let RPrive = false
    const intervalleTab = decoupageCupcap(rep)
    for (let k = 0; k < intervalleTab.length; k++) {
      // pour chacun de ces éléments, on vérifie que c’est bien un intervalle'
      if (((intervalleTab[k].charAt(0) !== '[') && (intervalleTab[k].charAt(0) !== ']')) || ((intervalleTab[k].charAt(intervalleTab[k].length - 1) !== '[') && (intervalleTab[k].charAt(intervalleTab[k].length - 1) !== ']'))) {
        pbCrochet = true
        // on teste alors s’il n’a pas écrit \\R privé de {...}'
        if (rep.indexOf('\\mathbb{R}') === 0) {
          RPrive = true
        }
      } else {
        if ((intervalleTab[k].indexOf(';') <= 1) || (intervalleTab[k].indexOf(';') >= intervalleTab[k].length - 2)) {
          // il n’y a pas de point-virgule bien placé, donc controlons s’il n’a pas mis une virgule à la place'
          if ((intervalleTab[k].indexOf(',') > 1) && (intervalleTab[k].indexOf(',') < intervalleTab[k].length - 2)) {
            // c’est donc qu’il met une virgule à la place d’un point-virgule'
            pbPointVirgule = true
          }
        }
      }
    }
    if (rep === '\\mathbb{R}') pbCrochet = false
    return { non_intervalle: pbCrochet, separateur_virgule: pbPointVirgule, RPrive }
  }

  function afficheCorrection (bonneRep) {
    j3pEmpty(stor.zoneCons3)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { color: (bonneRep) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', textes.corr1,
      {
        f: stor.stockage[10],
        d: stor.stockage[11] + '\\neq 0',
        i: 'x\\neq' + stor.stockage[13]
      })
    const domaineRep = ']-\\infty;' + stor.stockage[13] + '[\\cup]' + stor.stockage[13] + ';+\\infty['
    j3pAffiche(stor.zoneExpli2, '', textes.corr2,
      {
        f: stor.stockage[10],
        a: domaineRep,
        b: stor.stockage[13],
        style_texte: {}
      })
  }

  function enonceMain () {
    // on détermine les valeurs des paramètres a, b, c et d dans f(x)=(ax+b)/(cx+d)
    let coeffA, coeffB, coeffC, coeffD, pgcd2, pgcd3, signe
    do {
      const [minB, maxB] = j3pGetBornesIntervalle(ds.b)
      coeffB = j3pGetRandomInt(minB, maxB)
      if (Math.abs(coeffB) > Math.pow(10, -12)) {
        // a peut être nul si b ne l’est pas'
        const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
        coeffA = j3pGetRandomInt(minA, maxA)
      } else {
        // a ne peut être nul si b l’est'
        do {
          const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
          coeffA = j3pGetRandomInt(minA, maxA)
        } while (Math.abs(coeffA) < Math.pow(10, -12))
      }
      // le dénominateur
      // c ne peut être nul
      if (ds.c === '[0;0]') ds.c = '[-4;4]'
      do {
        const [minC, maxC] = j3pGetBornesIntervalle(ds.c)
        coeffC = j3pGetRandomInt(minC, maxC)
      } while (Math.abs(coeffC) < Math.pow(10, -12))
      // d peut être nul
      if (ds.zero_entier) {
        // d est un multiple de c
        coeffD = j3pGetRandomInt(-4, 4) * coeffC
      } else {
        const [minD, maxD] = j3pGetBornesIntervalle(ds.d)
        coeffD = j3pGetRandomInt(minD, maxD)
      }
      // il ne faut pas que ma fraction soit simplifiable
      const pgcd1 = j3pPGCD(coeffA, coeffB, { returnOtherIfZero: true, negativesAllowed: true })
      pgcd2 = j3pPGCD(coeffC, coeffD, { returnOtherIfZero: true, negativesAllowed: true })
      pgcd3 = j3pPGCD(pgcd1, pgcd2, { returnOtherIfZero: true, negativesAllowed: true })
      me.logIfDebug('pgcd1:' + pgcd1 + '   pgcd2:' + pgcd2 + '  pgcd3:' + pgcd3)
    } while (Math.abs(pgcd3 - 1) > Math.pow(10, -12))
    let num
    let den
    if (Math.abs(coeffB) < Math.pow(10, -12)) {
      num = j3pMonome(1, 1, coeffA)
    } else {
      if (Math.abs(coeffA) < Math.pow(10, -12)) {
        num = coeffB
      } else {
        num = j3pMonome(1, 1, coeffA) + j3pMonome(2, 0, coeffB)
      }
    }
    if (Math.abs(coeffD) < Math.pow(10, -12)) {
      den = j3pMonome(1, 1, coeffC)
    } else {
      den = j3pMonome(1, 1, coeffC) + j3pMonome(2, 0, coeffD)
    }
    // nom de la fonction
    const tabNomFct = ['f', 'g', 'h']
    const nomFct = tabNomFct[j3pGetRandomInt(0, 2)]
    const fdex = nomFct + '(x)=\\frac{' + num + '}{' + den + '}'
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '15px' }) })
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, { f: nomFct, g: fdex })
    const elt = j3pAffiche(stor.zoneCons2, '', textes.consigne2, { inputmq1: { texte: '' } })
    stor.zoneInput = elt.inputmqList[0]
    j3pStyle(stor.zoneCons3, { paddingTop: '15px' })
    j3pPaletteMathquill(stor.zoneCons3, stor.zoneInput, {
      liste: ['R', 'fraction', 'inf', 'union', '[', ']', 'bracket']
    })
    // on détermine la valeur interdite
    let valInterdite
    let valInterditeLatex
    let valInterditeLatexBis = ''
    let valInterditeLatexTer = ''
    let valInterditeLatex4 = ''
    if (Math.abs(coeffD) < Math.pow(10, -12)) {
      // la valeur interdite est alors 0
      valInterdite = String(0)
      valInterditeLatex = valInterdite
    } else {
      if (Math.abs(coeffD) % Math.abs(coeffC) === 0) {
        valInterdite = String(-coeffD / coeffC)
        valInterditeLatex = valInterdite
      } else {
        signe = -coeffC * coeffD / Math.abs(coeffC * coeffD)
        valInterdite = String(signe * Math.abs(coeffD) / pgcd2) + '/' + String(Math.abs(coeffC) / pgcd2)
        valInterditeLatex = '\\frac{' + String(signe * Math.abs(coeffD) / pgcd2) + '}{' + String(Math.abs(coeffC) / pgcd2) + '}'
        if (signe === -1) {
          valInterditeLatexBis = '-\\frac{' + String(Math.abs(coeffD) / pgcd2) + '}{' + String(Math.abs(coeffC) / pgcd2) + '}'
          valInterditeLatexTer = '\\frac{' + String(Math.abs(coeffD) / pgcd2) + '}{' + String(-Math.abs(coeffC) / pgcd2) + '}'
        } else {
          valInterditeLatexBis = '\\frac{' + String(-Math.abs(coeffD) / pgcd2) + '}{' + String(-Math.abs(coeffC) / pgcd2) + '}'
          valInterditeLatexTer = '-\\frac{' + String(-Math.abs(coeffD) / pgcd2) + '}{' + String(Math.abs(coeffC) / pgcd2) + '}'
          valInterditeLatex4 = '-\\frac{' + String(Math.abs(coeffD) / pgcd2) + '}{' + String(-Math.abs(coeffC) / pgcd2) + '}'
        }
      }
    }
    stor.valeur_interdite = -coeffD / coeffC
    stor.stockage[10] = nomFct
    stor.stockage[11] = den
    stor.stockage[12] = valInterdite
    stor.stockage[13] = valInterditeLatex

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '5px' }) })

    // liste des bonnes réponses acceptées
    const listeRep = []
    listeRep.push('\\mathbb{R}\\setminus\\left\\{' + stor.stockage[13] + '\\right\\}', '\\mathbb{R}\\setminus{' + stor.stockage[13] + '}')
    listeRep.push('\\mathbb{R}-\\left\\{' + stor.stockage[13] + '\\right\\}', '\\mathbb{R}-{' + stor.stockage[13] + '}')
    listeRep.push('\\mathbb{R}\\backslash\\left\\{' + stor.stockage[13] + '\\right\\}', '\\mathbb{R}\\backslash{' + stor.stockage[13] + '}')
    listeRep.push('R\\setminus\\left\\{' + stor.stockage[13] + '\\right\\}', 'R\\setminus{' + stor.stockage[13] + '}')
    listeRep.push('R-\\left\\{' + stor.stockage[13] + '\\right\\}', 'R-{' + stor.stockage[13] + '}')
    listeRep.push('R\\backslash\\left\\{' + stor.stockage[13] + '\\right\\}', 'R\\backslash{' + stor.stockage[13] + '}')
    listeRep.push('\\mathbb{R}\\setminus\\bracket{' + stor.stockage[13] + '}', '\\mathbb{R}\\setminus{' + stor.stockage[13] + '}')
    listeRep.push('\\mathbb{R}-\\bracket{' + stor.stockage[13] + '}', '\\mathbb{R}-{' + stor.stockage[13] + '}')
    listeRep.push('\\mathbb{R}\\backslash\\bracket{' + stor.stockage[13] + '}', '\\mathbb{R}\\backslash{' + stor.stockage[13] + '}')
    listeRep.push('R\\setminus\\bracket{' + stor.stockage[13] + '}', 'R\\setminus{' + stor.stockage[13] + '}')
    listeRep.push('R-\\bracket\\{' + stor.stockage[13] + '\\}', 'R-{' + stor.stockage[13] + '}')
    listeRep.push('R\\backslash\\bracket{' + stor.stockage[13] + '}', 'R\\backslash{' + stor.stockage[13] + '}')

    listeRep.push(']-\\infty;' + stor.stockage[13] + '[\\cup]' + stor.stockage[13] + ';+\\infty[')
    listeRep.push(']' + stor.stockage[13] + ';+\\infty[\\cup]-\\infty;' + stor.stockage[13] + '[')

    // je vérifie si l’élève n’a pas rentré qu’un seul des deux intervalles
    const listeIntervalle = []
    listeIntervalle.push(']-\\infty;' + stor.stockage[13] + '[')
    listeIntervalle.push(']' + stor.stockage[13] + ';+\\infty[')

    // je vérifie si l’élève a bien fait apparaître la valeur interdite'
    const tabValInterdite = []
    tabValInterdite.push(valInterditeLatex)

    // j’y ajoute aussi toutes les variétés d’écriture des fractions'
    if ((Math.abs(coeffD) > Math.pow(10, -12)) && (Math.abs(coeffD) % Math.abs(coeffC) !== 0)) {
      listeRep.push('\\mathbb{R}\\setminus\\left\\{' + valInterditeLatexBis + '\\right\\}', '\\mathbb{R}\\setminus{' + valInterditeLatexBis + '}')
      listeRep.push('\\mathbb{R}-\\left\\{' + valInterditeLatexBis + '\\right\\}', '\\mathbb{R}-{' + valInterditeLatexBis + '}')
      listeRep.push('\\mathbb{R}\\backslash\\left\\{' + valInterditeLatexBis + '\\right\\}', '\\mathbb{R}\\backslash{' + valInterditeLatexBis + '}')
      listeRep.push('R\\setminus\\left\\{' + valInterditeLatexBis + '\\right\\}', 'R\\setminus{' + valInterditeLatexBis + '}')
      listeRep.push('R-\\left\\{' + valInterditeLatexBis + '\\right\\}', 'R-{' + valInterditeLatexBis + '}')
      listeRep.push('R\\backslash\\left\\{' + valInterditeLatexBis + '\\right\\}', 'R\\backslash{' + valInterditeLatexBis + '}')
      listeRep.push('\\mathbb{R}\\setminus\\bracket{' + valInterditeLatexBis + '}', '\\mathbb{R}\\setminus{' + valInterditeLatexBis + '}')
      listeRep.push('\\mathbb{R}-\\bracket{' + valInterditeLatexBis + '}', '\\mathbb{R}-{' + valInterditeLatexBis + '}')
      listeRep.push('\\mathbb{R}\\backslash\\bracket{' + valInterditeLatexBis + '}', '\\mathbb{R}\\backslash{' + valInterditeLatexBis + '}')
      listeRep.push('R\\setminus\\bracket{' + valInterditeLatexBis + '}', 'R\\setminus{' + valInterditeLatexBis + '}')
      listeRep.push('R-\\bracket{' + valInterditeLatexBis + '}', 'R-{' + valInterditeLatexBis + '}')
      listeRep.push('R\\backslash\\bracket{' + valInterditeLatexBis + '}', 'R\\backslash{' + valInterditeLatexBis + '}')

      listeRep.push(']-\\infty;' + valInterditeLatexBis + '[\\cup]' + valInterditeLatexBis + ';+\\infty[')
      listeRep.push(']' + valInterditeLatexBis + ';+\\infty[\\cup]-\\infty;' + valInterditeLatexBis + '[')
      listeRep.push(']-\\infty;' + valInterditeLatex + '[\\cup]' + valInterditeLatexBis + ';+\\infty[')
      listeRep.push(']' + valInterditeLatex + ';+\\infty[\\cup]-\\infty;' + valInterditeLatexBis + '[')
      listeRep.push(']-\\infty;' + valInterditeLatexBis + '[\\cup]' + valInterditeLatex + ';+\\infty[')
      listeRep.push(']' + valInterditeLatexBis + ';+\\infty[\\cup]-\\infty;' + valInterditeLatex + '[')

      listeRep.push('\\mathbb{R}\\setminus\\left\\{' + valInterditeLatexTer + '\\right\\}', '\\mathbb{R}\\setminus{' + valInterditeLatexTer + '}')
      listeRep.push('\\mathbb{R}-\\left\\{' + valInterditeLatexTer + '\\right\\}', '\\mathbb{R}-{' + valInterditeLatexTer + '}')
      listeRep.push('\\mathbb{R}\\backslash\\left\\{' + valInterditeLatexTer + '\\right\\}', '\\mathbb{R}\\backslash{' + valInterditeLatexTer + '}')
      listeRep.push('R\\setminus\\left\\{' + valInterditeLatexTer + '\\right\\}', 'R\\setminus{' + valInterditeLatexTer + '}')
      listeRep.push('R-\\left\\{' + valInterditeLatexTer + '\\right\\}', 'R-{' + valInterditeLatexTer + '}')
      listeRep.push('R\\backslash\\left\\{' + valInterditeLatexTer + '\\right\\}', 'R\\backslash{' + valInterditeLatexTer + '}')
      listeRep.push('\\mathbb{R}\\bracket{' + valInterditeLatexTer + '}', '\\mathbb{R}\\setminus{' + valInterditeLatexTer + '}')
      listeRep.push('\\mathbb{R}-\\bracket{' + valInterditeLatexTer + '}', '\\mathbb{R}-{' + valInterditeLatexTer + '}')
      listeRep.push('\\mathbb{R}\\backslash\\bracket{' + valInterditeLatexTer + '}', '\\mathbb{R}\\backslash{' + valInterditeLatexTer + '}')
      listeRep.push('R\\setminus\\bracket{' + valInterditeLatexTer + '}', 'R\\setminus{' + valInterditeLatexTer + '}')
      listeRep.push('R-\\bracket{' + valInterditeLatexTer + '}', 'R-{' + valInterditeLatexTer + '}')
      listeRep.push('R\\backslash\\bracket{' + valInterditeLatexTer + '}', 'R\\backslash{' + valInterditeLatexTer + '}')

      listeRep.push(']-\\infty;' + valInterditeLatexTer + '[\\cup]' + valInterditeLatexTer + ';+\\infty[')
      listeRep.push(']' + valInterditeLatexTer + ';+\\infty[\\cup]-\\infty;' + valInterditeLatexTer + '[')
      listeRep.push(']-\\infty;' + valInterditeLatex + '[\\cup]' + valInterditeLatexTer + ';+\\infty[')
      listeRep.push(']' + valInterditeLatex + ';+\\infty[\\cup]-\\infty;' + valInterditeLatexTer + '[')
      listeRep.push(']-\\infty;' + valInterditeLatexTer + '[\\cup]' + valInterditeLatex + ';+\\infty[')
      listeRep.push(']' + valInterditeLatexTer + ';+\\infty[\\cup]-\\infty;' + valInterditeLatex + '[')
      listeRep.push(']-\\infty;' + valInterditeLatexBis + '[\\cup]' + valInterditeLatexTer + ';+\\infty[')
      listeRep.push(']' + valInterditeLatexBis + ';+\\infty[\\cup]-\\infty;' + valInterditeLatexTer + '[')
      listeRep.push(']-\\infty;' + valInterditeLatexTer + '[\\cup]' + valInterditeLatexBis + ';+\\infty[')
      listeRep.push(']' + valInterditeLatexTer + ';+\\infty[\\cup]-\\infty;' + valInterditeLatexBis + '[')

      listeIntervalle.push(']-\\infty;' + valInterditeLatexBis + '[')
      listeIntervalle.push(']-\\infty;' + valInterditeLatexTer + '[')
      listeIntervalle.push(']' + valInterditeLatexBis + ';+\\infty[')
      listeIntervalle.push(']' + valInterditeLatexTer + ';+\\infty[')

      tabValInterdite.push(valInterditeLatexBis)
      tabValInterdite.push(valInterditeLatexTer)
      if (signe === 1) {
        listeRep.push('\\mathbb{R}\\setminus\\left\\{' + valInterditeLatex4 + '\\right\\}', '\\mathbb{R}\\setminus{' + valInterditeLatex4 + '}')
        listeRep.push('\\mathbb{R}-\\left\\{' + valInterditeLatex4 + '\\right\\}', '\\mathbb{R}-{' + valInterditeLatex4 + '}')
        listeRep.push('\\mathbb{R}\\backslash\\left\\{' + valInterditeLatex4 + '\\right\\}', '\\mathbb{R}\\backslash{' + valInterditeLatex4 + '}')
        listeRep.push('R\\setminus\\left\\{' + valInterditeLatex4 + '\\right\\}', 'R\\setminus{' + valInterditeLatex4 + '}')
        listeRep.push('R-\\left\\{' + valInterditeLatex4 + '\\right\\}', 'R-{' + valInterditeLatex4 + '}')
        listeRep.push('R\\backslash\\left\\{' + valInterditeLatex4 + '\\right\\}', 'R\\backslash{' + valInterditeLatex4 + '}')
        listeRep.push('\\mathbb{R}\\setminus\\bracket{' + valInterditeLatex4 + '}', '\\mathbb{R}\\setminus{' + valInterditeLatex4 + '}')
        listeRep.push('\\mathbb{R}-\\bracket{' + valInterditeLatex4 + '}', '\\mathbb{R}-{' + valInterditeLatex4 + '}')
        listeRep.push('\\mathbb{R}\\backslash\\bracket{' + valInterditeLatex4 + '}', '\\mathbb{R}\\backslash{' + valInterditeLatex4 + '}')
        listeRep.push('R\\setminus\\bracket{' + valInterditeLatex4 + '}', 'R\\setminus{' + valInterditeLatex4 + '}')
        listeRep.push('R-\\bracket{' + valInterditeLatex4 + '}', 'R-{' + valInterditeLatex4 + '}')
        listeRep.push('R\\backslash\\bracket{' + valInterditeLatex4 + '}', 'R\\backslash{' + valInterditeLatex4 + '}')

        listeRep.push(']-\\infty;' + valInterditeLatex4 + '[\\cup]' + valInterditeLatex4 + ';+\\infty[')
        listeRep.push(']' + valInterditeLatex4 + ';+\\infty[\\cup]-\\infty;' + valInterditeLatex4 + '[')
        listeRep.push(']-\\infty;' + valInterditeLatex + '[\\cup]' + valInterditeLatex4 + ';+\\infty[')
        listeRep.push(']' + valInterditeLatex + ';+\\infty[\\cup]-\\infty;' + valInterditeLatex4 + '[')
        listeRep.push(']-\\infty;' + valInterditeLatex4 + '[\\cup]' + valInterditeLatex + ';+\\infty[')
        listeRep.push(']' + valInterditeLatex4 + ';+\\infty[\\cup]-\\infty;' + valInterditeLatex + '[')
        listeRep.push(']-\\infty;' + valInterditeLatexBis + '[\\cup]' + valInterditeLatex4 + ';+\\infty[')
        listeRep.push(']' + valInterditeLatexBis + ';+\\infty[\\cup]-\\infty;' + valInterditeLatex4 + '[')
        listeRep.push(']-\\infty;' + valInterditeLatex4 + '[\\cup]' + valInterditeLatexBis + ';+\\infty[')
        listeRep.push(']' + valInterditeLatex4 + ';+\\infty[\\cup]-\\infty;' + valInterditeLatexBis + '[')
        listeRep.push(']-\\infty;' + valInterditeLatexTer + '[\\cup]' + valInterditeLatex4 + ';+\\infty[')
        listeRep.push(']' + valInterditeLatexTer + ';+\\infty[\\cup]-\\infty;' + valInterditeLatex4 + '[')
        listeRep.push(']-\\infty;' + valInterditeLatex4 + '[\\cup]' + valInterditeLatexTer + ';+\\infty[')
        listeRep.push(']' + valInterditeLatex4 + ';+\\infty[\\cup]-\\infty;' + valInterditeLatexTer + '[')

        listeIntervalle.push(']' + valInterditeLatex4 + ';+\\infty[')
        listeIntervalle.push(']-\\infty;' + valInterditeLatex4 + '[')
        tabValInterdite.push(valInterditeLatex4)
      }
    }
    stor.zoneInput.reponse = listeRep
    stor.zoneInput.intervalles_rep = listeIntervalle
    stor.zoneInput.tabValInterdite = tabValInterdite
    mqRestriction(stor.zoneInput, '\\d.,;/-+[]}{\\\\', {
      commandes: ['R', 'fraction', 'inf', 'union', 'bracket']
    })
    me.logIfDebug('listeRep:' + listeRep)
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage('presentation1')
        // par convention, stor.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        stor.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      let repEleve = $(stor.zoneInput).mathquill('latex')
      let aRepondu = (repEleve !== '')
      // si l’élève écrit une fraction, elle doit être irréductible'
      let irreductible = true
      let ensMalEcrit = false
      if (aRepondu) {
        // on cherche si une fraction est
        // écrite
        const fracEntier = /frac\{-?[\d ]+}\{[\d ]+}/
        if (fracEntier.test(repEleve)) {
          for (const frac of repEleve.match(fracEntier)) {
            // on prend les deux premiers morceaux qui précèdent un } en omettant leur 1er caractère
            const reg = /^frac\{([^}]+)}\{([^}]+)}/
            if (reg.test(frac)) {
              const [, numFrac, denFrac] = frac.split(reg)
              if ([numFrac, denFrac].includes(' ')) {
                ensMalEcrit = true
              } else {
                const pgcdFrac = j3pPGCD(Number(numFrac), Number(denFrac), {
                  returnOtherIfZero: true,
                  negativesAllowed: true
                })
                if (pgcdFrac > 1) {
                  irreductible = false
                }
                me.logIfDebug('numFrac:' + numFrac + '  denFrac:' + denFrac + '  irreductible:' + irreductible)
              }
            } else {
              console.error(Error('fraction invalide : ' + frac))
            }
          }
          if (!irreductible) {
            aRepondu = false
          }
        }
      }
      if (!aRepondu && (!me.isElapsed)) {
        let msgReponseManquante
        if (!irreductible) msgReponseManquante = textes.phrase_erreur2
        j3pFocus(stor.zoneInput)
        me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        return me.finCorrection() // on reste dans l’état correction
      }

      // Une réponse a été saisie
      // Bonne réponse
      // on souhaite accepte qu’un élève écrive +2 au lieu de 2'
      // attention à ne pas virer le + devant l’infini'
      let bonneRep = false
      if (!ensMalEcrit) {
        while (repEleve.indexOf('+\\frac') > -1) {
          repEleve = repEleve.replace('+\\frac', '\\frac')
        }
        const crochetReg = /[[\]]\+[-0-9+]/ // new RegExp('(\\[|\\])\\+[-0-9+]{1}', 'gi')
        let modifTab, newValTab, k
        while (crochetReg.test(repEleve)) {
          modifTab = repEleve.match(crochetReg)
          for (k = 0; k < modifTab.length; k++) {
            newValTab = modifTab[k].charAt(0) + modifTab[k].substring(2)
            repEleve = repEleve.replace(modifTab[k], newValTab)
          }
        }
        const accReg = /[{;]\+[-0-9+]/ // new RegExp('(\\{|;)\\+[-0-9+]{1}', 'gi')// valable pour l’accolade ouvrante ou le point-virgule'
        while (accReg.test(repEleve)) {
          modifTab = repEleve.match(accReg)
          for (k = 0; k < modifTab.length; k++) {
            newValTab = modifTab[k].charAt(0) + modifTab[k].substring(2)
            repEleve = repEleve.replace(modifTab[k], newValTab)
          }
        }
        let repElevebis = repEleve
        me.logIfDebug('valeur après modif : ' + repEleve)
        // je n’avais pas pensé à mettre les décimaux si nécessaire
        if (Math.abs(Math.round(stor.valeur_interdite * Math.pow(10, 6)) - stor.valeur_interdite * Math.pow(10, 6)) < Math.pow(10, -12)) {
          // dans ce cas la valeur interdite est décimale
          const nomDecReg = /(?:-?[1-9][0-9]*|0),[0-9]+/g // new RegExp('[0-9]{1,}\\,[0-9]{1,}', 'g')
          if (nomDecReg.test(repElevebis)) {
            const repEleveDec = repElevebis.match(nomDecReg)
            for (let i = 0; i < repEleveDec.length; i++) {
              const nbDec = repEleveDec[i].length - repEleveDec[i].indexOf(',') - 1
              let newNum = j3pNombre(repEleveDec[i]) * Math.pow(10, nbDec)
              let newDen = Math.pow(10, nbDec)
              const pgcdNew = j3pPGCD(newNum, newDen, { returnOtherIfZero: true, negativesAllowed: true })
              newNum = newNum / pgcdNew
              newDen = newDen / pgcdNew
              repElevebis = repElevebis.replace(repEleveDec[i], '\\frac{' + newNum + '}{' + newDen + '}')
            }
          }
        }

        bonneRep = false
        for (let i = 0; i < stor.zoneInput.reponse.length; i++) {
          if (repElevebis === stor.zoneInput.reponse[i]) {
            bonneRep = true
          }
          me.logIfDebug(repEleve + '  ' + repElevebis + '   ' + stor.zoneInput.reponse[i] + '  bonneRep:' + bonneRep)
        }
      }
      if (bonneRep) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        afficheCorrection(true)
        me.typederreurs[0]++
        j3pDesactive(stor.zoneInput)
        stor.zoneInput.setAttribute('style', 'color:' + me.styles.cbien)
        j3pFreezeElt(stor.zoneInput)
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux
      // on met en rouge ce qui est faux
      stor.zoneInput.style.color = me.styles.cfaux
      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        j3pDesactive(stor.zoneInput)
        afficheCorrection(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      // je cherche à identifier le type de mauvaise réponse
      const testIntervalle = bonIntervalle(repEleve)
      // je cherche si le pb ne vient pas que de la valeur interdite
      const Rpresent = ((repEleve.includes('\\mathbb{R}\\backslash\\left\\{')) || (repEleve.includes('\\mathbb{R}\\setminus\\left\\{')) || (repEleve.includes('\\mathbb{R}-\\left\\{')) || (repEleve.includes('\\mathbb{R}-\\bracket{')))
      const infCupPresents = ((repEleve.indexOf(']-\\infty;') > -1) && (repEleve.indexOf('[\\cup]') > -1) && (repEleve.indexOf(';+\\infty[') > -1))
      stor.zoneCorr.innerHTML = cFaux
      // on cherche si l’un des deux intervalles n’est pas présent
      let intervallePresent = false
      for (let i = 0; i < stor.zoneInput.intervalles_rep.length; i++) {
        if (repEleve.indexOf(stor.zoneInput.intervalles_rep[i]) > -1) {
          intervallePresent = true
        }
      }
      let valInterditePresente = false
      for (let i = 0; i < stor.zoneInput.tabValInterdite.length; i++) {
        if (repEleve.indexOf(stor.zoneInput.tabValInterdite[i]) > -1) {
          valInterditePresente = true
        }
      }
      if (ensMalEcrit) {
        stor.zoneCorr.innerHTML += '<br>' + textes.comment9
      } else {
        if (!Rpresent) {
          if (testIntervalle.non_intervalle) {
            stor.zoneCorr.innerHTML += '<br>' + textes.comment7
          } else if (testIntervalle.separateur_virgule) {
            stor.zoneCorr.innerHTML += '<br>' + textes.comment8
          }
        }
        if (Rpresent || infCupPresents) {
          stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur1
        } else if (intervallePresent) {
          stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur3
        } else if (valInterditePresente) {
          stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur4
        }
      }
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // indication éventuelle ici
        j3pFocus(stor.zoneInput)
      } else {
        // Erreur au dernier essai
        stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
        j3pDesactive(stor.zoneInput)
        stor.zoneInput.setAttribute('style', 'color:' + me.styles.cfaux)
        j3pBarre(stor.zoneInput)
        afficheCorrection(false)
        me.typederreurs[2]++
        me.finCorrection('navigation', true)
      }
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      me.finNavigation()
      break // case "navigation":
  }
}
