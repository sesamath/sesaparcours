import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pPGCD, j3pRandomTab, j3pStyle } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pCreeBoutonsFenetre, j3pDetruitToutesLesFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    juillet 2014
    On donne une fonction de la forme f(x)=a'+b'/(cx+d) et on demande d’écrire f(x) sous la forme
    (ax+b)/(cx+d) pour reconnaitre une fonction homographique
*/
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    // ["a","[-10;10]","string", "coefficient a dans l’écriture (ax+b)/(cx+d)"],
    ['b', '[-10;10]', 'string', 'coefficient b dans l’écriture (ax+b)/(cx+d)'],
    ['c', '[-5;5]', 'string', 'coefficient c (non nul) dans l’écriture (ax+b)/(cx+d)'],
    ['d', '[-5;5]', 'string', 'coefficient d dans l’écriture (ax+b)/(cx+d)']
  ]

}

const textes = {
  titre_exo: 'Reconnaître une fonction homographique',
  titreAide: 'Rappel du cours',
  consigne1: 'Soit $£f$ la fonction définie sur $\\R\\setminus\\bracket{£d}$ par :<br>$£f(x)=£e$.',
  consigne2: '$£f$ est une fonction homographique : en effet, pour tout $x$ appartenant à $\\R\\setminus\\bracket{£e}$,',
  aide: 'Fonction homographique',
  aide2: "On appelle <b>fonction homographique</b> toute fonction $h$ qui peut s'écrire comme quotient de fonctions affines.<br>Soient $a$, $b$, $c$ et $d$ quatre réels tels que $ad-bc\\neq 0$ et $c\\neq 0$ : $h(x)=\\frac{ax+b}{cx+d}$.",
  comment1: 'Le numérateur doit être simplifié&nbsp;!',
  comment2: 'Cette fonction n’est pas une fonction homographique&nbsp;!'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function aide () {
    stor.nameBtn = j3pGetNewId('Boutons')
    stor.nameAide = j3pGetNewId('Aide')
    stor.fenetreAide = j3pGetNewId('fenetreAide')
    stor.fenetreBoutons = j3pGetNewId('fenetreBoutons')
    const untableau = [
      {
        name: stor.nameBtn,
        title: textes.titreAide,
        left: Math.round(stor.conteneur.getBoundingClientRect().width),
        top: me.zonesElts.HD.getBoundingClientRect().height + 150,
        id: stor.fenetreBoutons
      }
    ]
    untableau.push({
      name: stor.nameAide,
      title: textes.aide,
      top: me.zonesElts.HD.getBoundingClientRect().height + 25,
      left: Math.round(me.zonesElts.HD.getBoundingClientRect().width),
      width: 500,
      id: stor.fenetreAide
    })
    me.fenetresjq = untableau
    // Création mais sans affichage.
    j3pCreeFenetres(me)
    j3pCreeBoutonsFenetre(stor.fenetreBoutons, [['Aide', textes.aide]])
    // j3pToggleFenetres('Boutons')
    stor.idBtn = j3pAddElt(stor.conteneurD, 'div')
    j3pStyle(stor.idBtn, { paddingTop: '7px' })
    j3pAjouteBouton(stor.idBtn, stor.nameAide, 'MepBoutons', 'Aide', j3pToggleFenetres.bind(null, stor.nameAide))
    j3pAfficheCroixFenetres(stor.nameAide)

    const coursDebut = j3pAddElt(stor.fenetreAide, 'div')
    j3pAffiche(coursDebut, '', textes.aide2)
  }

  function afficheCorrection (bonneReponse) {
    // on affiche la correction
    // on cherche la position de la zone de correction en y
    j3pDetruitToutesLesFenetres()
    j3pDetruit(stor.idBtn)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    const tabCoef = stor.coef_tab
    let expression1
    let signeB1 = '+'
    if (tabCoef[1] < 0) signeB1 = '-'
    if (Math.abs(tabCoef[0] - 1) < Math.pow(10, -12)) {
      expression1 = '\\frac{' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + '}{' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + '}' + signeB1 + '\\frac{' + Math.abs(tabCoef[1]) + '}{' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + '}'
    } else if (Math.abs(tabCoef[0] + 1) < Math.pow(10, -12)) {
      expression1 = '\\frac{-(' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + ')}{' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + '}' + signeB1 + '\\frac{' + Math.abs(tabCoef[1]) + '}{' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + '}'
    } else {
      expression1 = '\\frac{' + tabCoef[0] + '(' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + ')}{' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + '}' + signeB1 + '\\frac{' + Math.abs(tabCoef[1]) + '}{' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + '}'
    }
    let expression2
    let expression3
    if (Math.abs(tabCoef[3]) < Math.pow(10, -12)) {
      expression2 = '\\frac{' + j3pMonome(1, 1, tabCoef[0] * tabCoef[2]) + j3pMonome(3, 0, tabCoef[1]) + '}{' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + '}'
      expression3 = ''
    } else {
      expression2 = '\\frac{' + j3pMonome(1, 1, tabCoef[0] * tabCoef[2]) + j3pMonome(2, 0, tabCoef[0] * tabCoef[3]) + j3pMonome(3, 0, tabCoef[1]) + '}{' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + '}'
      expression3 = '=\\frac{' + j3pMonome(1, 1, tabCoef[4]) + j3pMonome(2, 0, tabCoef[5]) + '}{' + j3pMonome(1, 1, tabCoef[2]) + j3pMonome(2, 0, tabCoef[3]) + '}'
    }
    j3pAffiche(zoneExpli, '', '$£f(x)=' + expression1 + '=' + expression2 + expression3 + '$', { f: stor.nomF })
  }

  function enonceMain () {
    const nomF = j3pRandomTab(['f', 'g', 'h'], [0.3333, 0.3333, 0.3334])
    let coefA, coefB, coefC, coefD// pour l’expression de la forme a&+b1/(cx+d)
    let valInterdite, pgcdCD
    do {
      do {
        if (ds.c === '[0;0]') {
          ds.c = '[-5;5]'
        }
        const [lemin, lemax] = j3pGetBornesIntervalle(ds.c)
        coefC = j3pGetRandomInt(lemin, lemax)
      } while (Math.abs(coefC) < Math.pow(10, -12))
      const [lemin, lemax] = j3pGetBornesIntervalle(ds.d)
      if (lemin === lemax) {
        coefD = lemin
      } else {
        do {
          coefD = j3pGetRandomInt(lemin, lemax)
        } while (Math.abs(coefD) < Math.pow(10, -10))
      }
      pgcdCD = j3pPGCD(coefC, coefD, { returnOtherIfZero: true, negativesAllowed: true })
      do {
        coefA = j3pGetRandomInt(-3, 3) * coefC
      } while (Math.abs(coefA) < Math.pow(10, -12))
      // a est un multiple de c donc on pourra écrire simplement f(x) sous la forme a1+b1/(cx+d)
      const [lemin2, lemax2] = j3pGetBornesIntervalle(ds.b)
      coefB = j3pGetRandomInt(lemin2, lemax2)
    } while (Math.abs(coefA * coefD - coefB * coefC) < Math.pow(10, -12))
    me.logIfDebug('a :' + coefA + '   b:' + coefB + '   c:' + coefC + '   d:' + coefD)
    if (Math.abs(coefD / coefC - Math.round(coefD / coefC)) < Math.pow(10, -12)) {
    // la valeur interdite est un entier
      valInterdite = -coefD / coefC
    } else {
    // la valeur interdite n’est pas un entier
      if (coefC * coefD > 0) {
        valInterdite = '-\\frac{' + Math.abs(coefD / pgcdCD) + '}{' + Math.abs(coefC / pgcdCD) + '}'
      } else {
        valInterdite = '\\frac{' + Math.abs(coefD / pgcdCD) + '}{' + Math.abs(coefC / pgcdCD) + '}'
      }
    }
    const coefA1 = coefA / coefC
    const coefB1 = coefB - coefA1 * coefD
    let fdex
    if (coefB1 > 0) {
      fdex = coefA1 + '+\\frac{' + coefB1 + '}{' + j3pMonome(1, 1, coefC) + j3pMonome(2, 0, coefD) + '}'
    } else {
      fdex = coefA1 + '-\\frac{' + (-coefB1) + '}{' + j3pMonome(1, 1, coefC) + j3pMonome(2, 0, coefD) + '}'
    }
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, { f: nomF, d: valInterdite, e: fdex })

    // "$£f$ est une fonction homographique : en effet, pour tout $x$ appartenant à $£e$, $£f(x)=\\frac{\\editable{a}}{£d$}$.",
    j3pAffiche(stor.zoneCons2, '', textes.consigne2,
      { f: nomF, e: valInterdite, d: j3pMonome(1, 1, coefC) + j3pMonome(2, 0, coefD) })
    const expression = nomF + '(x)=\\frac{\\editable{}}{' + j3pMonome(1, 1, coefC) + j3pMonome(2, 0, coefD) + '}'
    const elt = j3pAffiche(stor.zoneCons3, '', '$' + expression + '$')
    stor.coef_tab = [coefA1, coefB1, coefC, coefD, coefA, coefB]
    stor.nomF = nomF
    stor.zoneInput = elt.inputmqefList[0]
    mqRestriction(stor.zoneInput, '\\dx+-*')
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    aide()
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div', '', { style: { paddingTop: '10px' } })

    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection

        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1bis', ratioGauche: 0.6 })

        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        let repEleve = $(stor.zoneInput).mathquill('latex')
        repEleve = j3pMathquillXcas(repEleve)
        // ici dans repEleve, je retrouve le numérateur écrit en langage latex
        let aRepondu = true
        me.logIfDebug('repEleve : ' + repEleve)
        if (repEleve === '') {
          aRepondu = false
        }
        let nonHomographique = false
        let bonneReponse, simplifie
        if (aRepondu) {
          let numerateurOK = true
          // on regarde si la réponse est celle qui est attendue, soit ax+b
          const difference = j3pMathquillXcas(repEleve + '-(' + (stor.coef_tab[4]) + 'x+' + (stor.coef_tab[5]) + ')')
          let calc
          for (let i = 0; i <= 10; i++) {
            const arbreDif = new Tarbre(difference, ['x'])
            calc = arbreDif.evalue([i])
            if (Math.abs(calc) > Math.pow(10, -12)) numerateurOK = false
          }
          me.logIfDebug('difference:' + difference + '   numerateurOK:' + numerateurOK)
          // à cet instant on sait s’il y a égalité'
          // on veut aussi savoir si c’est simplifié'
          // il faut donc que "x" n’apparaîsse qu’une et une seule fois"
          simplifie = (repEleve.split('x').length <= 2)
          let nbSignePlusMoins = 0
          if (repEleve.charAt(0) === '-') {
            nbSignePlusMoins = (repEleve.split('+').length - 1) + (repEleve.split('-').length - 2)
          } else {
            nbSignePlusMoins = (repEleve.split('+').length - 1) + (repEleve.split('-').length - 1)
          }
          if (nbSignePlusMoins > 1) simplifie = false
          for (let i = 2; i <= 9; i++) {
            if (repEleve.includes('x^' + i)) nonHomographique = true
          }
          for (let i = 1; i <= 9; i++) {
            for (let j = 0; j <= 9; j++) {
              if (repEleve.includes('x^(' + i + j)) nonHomographique = true
            }
          }
          me.logIfDebug('simplifie:' + simplifie + '  nonHomographique:' + nonHomographique)
          bonneReponse = (numerateurOK && simplifie && !nonHomographique)
          fctsValid.zones.bonneReponse[0] = bonneReponse
          fctsValid.coloreUneZone(stor.zoneInput.id)
        }
        if ((!aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(true)
            j3pDesactive(stor.zoneInput)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            j3pFocus(stor.zoneInput)
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              j3pDesactive(stor.zoneInput)
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (nonHomographique) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment2
              } else if (!simplifie) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
