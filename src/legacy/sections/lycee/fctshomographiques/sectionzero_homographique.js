import $ from 'jquery'
import { j3pAddElt, j3pFreezeElt, j3pNombre, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pPaletteMathquill, j3pPGCD, j3pRandomTab, j3pStyle, j3pGetBornesIntervalle } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    août 2014
    On demande de préciser la valeur pour laquelle la fonction homographique n’est aps définie, ainsi que celle où elle s’annule
    On peut étendre cette section à un quotient de polynômes
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['imposer_fcts', ['|', '|'], 'array', 'Pour chaque répétition, on peut imposer les fonctions du numérateur et du dénominateur (et même y mettre autre chose qu’une fonction affine, voire des coefficients avec des fractions ou des racines carrées - ces fonctions seront séparées par |, par exemple "2x-3|x^2")'],
    ['fcts_affines', ['true|true', 'true|true'], 'array', 'tableau où chaque élément contient deux booléens séparés par | pour préciser si le numérateur et le dénominateur sont bien affines.'],
    ['zeros_fcts_nonaffines', ['|', '|'], 'array', 'ce tableau contient, pour chaque répétition, les zéros des fonctions non affines sous la forme "-2/3" par exemple (si la première fonction ne s’annule pas et que la deuxième le fait en 1, écrire "|1", s’il y en a plusieurs les séparer par &, par exemple "-2/3|5&racine(5)"'],
    ['zero_seulement', false, 'boolean', 's’il vaut true, on ne demande que la valeur en laquelle s’annule la fonction (valable seulement pour une fonction homographique). Ce paramètre sera le même pour toutes les répétitions.'],
    ['interdite_seulement', false, 'boolean', 's’il vaut true, on ne demande que la valeur en laquelle la fonction n’est pas définie (valable seulement pour une fonction homographique). Ce paramètre sera le même pour toutes les répétitions.']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function transformationLatex (express) {
    // cette fonction sert à convertir l’espresson de la fonction au format latex'
    // c’est notamment pour la gestion de sfractions et racines carrées'
    let express1 = ''
    const chaineRacineFct = /(-?racine\([0-9.]+\))/g // new RegExp('([\\-]?racine\\([0-9\\.]{1,}\\))', 'ig')
    const expressRacineTab = express.split(chaineRacineFct)
    for (let i = 0; i < expressRacineTab.length; i++) {
      if (expressRacineTab[i].indexOf('racine') > -1) {
        // on a une racine carrée
        const expressRacine2 = /racine\(/g // new RegExp('racine\\(', 'ig')
        const modifExpressRacine1 = expressRacineTab[i].replace(expressRacine2, '\\sqrt{')
        const expressRacine3 = /\)/g // new RegExp('\\)', 'ig')
        express1 += modifExpressRacine1.replace(expressRacine3, '}')
      } else {
        express1 += expressRacineTab[i]
      }
    }
    me.logIfDebug('express1 après racine:' + express1)
    // on remplace .../... par \\frac{...}{...}
    const chaineFrac = /\(?([0-9,.]+\/[0-9,.]+)\)?/g // new RegExp('\\(?([0-9,\\.]{1,}/[0-9,\\.]{1,})\\)?', 'ig')
    const tabExpressFraction2 = express1.split(chaineFrac)
    me.logIfDebug('tabExpressFraction2:' + tabExpressFraction2)
    let expressLatex = ''
    for (let i = 0; i < tabExpressFraction2.length; i++) {
      if (tabExpressFraction2[i].indexOf('/') > -1) {
        // on a une fraction
        const expressFrac = tabExpressFraction2[i].split('/')
        expressLatex += '\\frac{' + expressFrac[0] + '}{' + expressFrac[1] + '}'
      } else {
        expressLatex += tabExpressFraction2[i]
      }
    }
    return expressLatex
  }

  function simplificationQuotientLatex (num, den) {
    // ici num et den ne sont pas seulement des nombres entiers
    let i
    let simplRacine
    const presenceSomme = ((num.indexOf('+') > -1) || (den.indexOf('+') > -1))
    const presenceDifference = (((num.indexOf('-') > -1) && (num.split('-')[0] !== '')) || ((den.indexOf('-') > -1) && (den.split('-')[0] !== '')))
    if (presenceSomme || presenceDifference) {
      // là je ne vois pas trop comment je peux simplifier donc je balance le quotient sans simplification'
      if ((den.charAt(0) === '(') && (den.charAt(den.length - 1) === ')')) {
        simplRacine = '\\frac{' + num + '}{' + den.substring(1, den.length - 1) + '}'
      } else {
        simplRacine = '\\frac{' + num + '}{' + den + '}'
      }
    } else {
      // on gère d’abord la présence de racines carrées'
      const chaineRac = /racine\([0-9]+\)/g // new RegExp('racine\\([0-9]{1,}\\)', 'ig')
      // chaineRac permet de récupérer ce dont on prend la racine carrée
      let signeNum = '+'
      let signeDen = '+'
      let signeQuotient = 'positif'
      let nbRacineDen, resteRacineDen
      if (num.indexOf('racine') > -1) {
        const nbRacineNum = num.match(chaineRac)
        const resteRacineNum = num.split(chaineRac)
        if (resteRacineNum[0].charAt(0) === '-') {
          signeNum = '-'
          resteRacineNum[0] = resteRacineNum[0].substring(1, resteRacineNum[0].length)
        }
        // on enlève les parenthèse si elles existent encore
        if ((resteRacineNum[0].charAt(0) === '(') && (resteRacineNum[0].charAt(resteRacineNum[0].length - 1) === ')')) {
          resteRacineNum[0] = resteRacineNum[0].substring(1, resteRacineNum[0].length - 1)
        }
        me.logIfDebug('resteRacineNum:' + resteRacineNum)
        let valRacineNum = 1
        for (i = 0; i < nbRacineNum.length; i++) {
          nbRacineNum[i] = nbRacineNum[i].substring(7, nbRacineNum[i].length - 1)
          valRacineNum = valRacineNum * j3pNombre(nbRacineNum[i])
        }
        me.logIfDebug('nbRacineNum:' + nbRacineNum + '   valRacineNum:' + valRacineNum)
        if (den.indexOf('racine') > -1) {
          // on a une racine carrée au numérateur et au dénominateur
          nbRacineDen = den.match(chaineRac)
          resteRacineDen = den.split(chaineRac)
          if (resteRacineDen[0].charAt(0) === '-') {
            signeDen = '-'
            resteRacineDen[0] = resteRacineDen[0].substring(1, resteRacineDen[0].length)
          }
          // on enlève les parenthèses éventuelles
          if ((resteRacineDen[0].charAt(0) === '(') && (resteRacineDen[0].charAt(resteRacineDen[0].length - 1) === ')')) {
            resteRacineDen[0] = resteRacineDen[0].substring(1, resteRacineDen[0].length - 1)
          }
          if (signeNum !== signeDen) {
            signeQuotient = 'negatif'
          }
          me.logIfDebug('resteRacineDen:' + resteRacineDen)
          let valRacineDen = 1
          for (i = 0; i < nbRacineDen.length; i++) {
            nbRacineDen[i] = nbRacineDen[i].substring(7, nbRacineDen[i].length - 1)
            valRacineDen = valRacineDen * j3pNombre(nbRacineDen[i])
          }
          me.logIfDebug('nbRacineDen:' + nbRacineDen + '   valRacineDen:' + valRacineDen)
          let pgcdQuotient
          if (Math.abs(Math.round(Math.sqrt(valRacineNum / valRacineDen)) - Math.sqrt(valRacineNum / valRacineDen)) < Math.pow(10, -12)) {
            // la racine carrée se simplifie et va disparaitre
            simplRacine = String(Math.sqrt(valRacineNum / valRacineDen))
          } else if (Math.abs(Math.round(valRacineNum / valRacineDen) - (valRacineNum / valRacineDen)) < Math.pow(10, -12)) {
            // on simplifie le quotient, mais la racine carrée demeure
            simplRacine = '\\sqrt{' + String(valRacineNum / valRacineDen) + '}'
          } else {
            // on vérifie si on peut simplifier le quotient
            if (((Math.abs(Math.round(valRacineNum) - valRacineNum) < Math.pow(10, -12))) && ((Math.abs(Math.round(valRacineDen) - valRacineDen) < Math.pow(10, -12)))) {
              // j’ai bien deux entiers'
              pgcdQuotient = j3pPGCD(Math.abs(valRacineNum), Math.abs(valRacineDen))
              // on vérifie si on ne peut pas simplifier sqrt(a/b)
              if (Math.abs(Math.round(Math.sqrt(valRacineNum / pgcdQuotient)) - Math.sqrt(valRacineNum / pgcdQuotient)) < Math.pow(10, -12)) {
                // sqrt(a) s'écrit comme un entier'
                if (Math.abs(Math.round(Math.sqrt(valRacineDen / pgcdQuotient)) - Math.sqrt(valRacineDen / pgcdQuotient)) < Math.pow(10, -12)) {
                  // sqrt(b) s'écrit comme un entier'
                  // simplRacine = "\\frac{"+String(Math.sqrt(valRacineNum/pgcdQuotient))+"}{"+String(Math.sqrt(valRacineDen/pgcdQuotient))+"}";
                  simplRacine = String(Math.sqrt(valRacineNum / pgcdQuotient)) + '/' + String(Math.sqrt(valRacineDen / pgcdQuotient))
                } else {
                  // sqrt(b) ne s'écrit pas comme un entier'
                  if (Math.abs(Math.sqrt(valRacineNum / pgcdQuotient) - 1) < Math.pow(10, -12)) {
                    simplRacine = '\\frac{\\sqrt{' + String(valRacineDen / pgcdQuotient) + '}}{' + String(valRacineDen / pgcdQuotient) + '}'
                  } else {
                    simplRacine = '\\frac{' + String(Math.sqrt(valRacineNum / pgcdQuotient)) + '\\sqrt{' + String(valRacineDen / pgcdQuotient) + '}}{' + String(valRacineDen / pgcdQuotient) + '}'
                  }
                }
              } else {
                // sqrt(a) ne s'écrit pas comme un entier'
                if (Math.abs(Math.round(Math.sqrt(valRacineDen / pgcdQuotient)) - Math.sqrt(valRacineDen / pgcdQuotient)) < Math.pow(10, -12)) {
                  // sqrt(b) s'écrit comme un entier'
                  simplRacine = '\\frac{\\sqrt{' + String(valRacineNum / pgcdQuotient) + '}}{' + String(Math.sqrt(valRacineDen / pgcdQuotient)) + '}'
                } else {
                  // sqrt(b) ne s'écrit pas comme un entier'
                  simplRacine = '\\sqrt{\\frac{' + String(valRacineNum / pgcdQuotient) + '}{' + String(valRacineDen / pgcdQuotient) + '}}'
                }
              }
            }
          }
          // gestion du quotient en dehors de la racine carrée
          if (resteRacineNum[0] === '') {
            resteRacineNum[0] = '1'
          }
          if (resteRacineDen[0] === '') {
            resteRacineDen[0] = '1'
          }
          if (Math.abs(Math.round(Math.sqrt(valRacineNum / valRacineDen)) - Math.sqrt(valRacineNum / valRacineDen)) < Math.pow(10, -12)) {
            if (resteRacineNum[0] === '') {
              if (resteRacineDen[0] !== '') {
                resteRacineNum[0] = simplRacine
                simplRacine = simpQuotientFractions(resteRacineNum[0], resteRacineDen[0])
              }
            } else {
              // si resteRacineNum[0] est une fraction, je dois multiplier le numérateur par simplRacine
              resteRacineNum[0] = simpQuotientFractions(resteRacineNum[0], '1/' + simplRacine)
              if (resteRacineDen[0] !== '') {
                simplRacine = simpQuotientFractions(resteRacineNum[0], resteRacineDen[0])
              } else {
                simplRacine = resteRacineNum[0]
              }
            }
          } else if ((Math.abs(Math.round(Math.sqrt(valRacineNum / pgcdQuotient)) - Math.sqrt(valRacineNum / pgcdQuotient)) < Math.pow(10, -12)) && (Math.abs(Math.round(Math.sqrt(valRacineDen / pgcdQuotient)) - Math.sqrt(valRacineDen / pgcdQuotient)) < Math.pow(10, -12))) {
            if (resteRacineNum[0] === '') {
              if (resteRacineDen[0] !== '') {
                resteRacineNum[0] = simplRacine
                simplRacine = simpQuotientFractions(resteRacineNum[0], resteRacineDen[0])
              }
            } else {
              // si resteRacineNum[0] est une fraction, je dois multiplier le numérateur par simplRacine
              const recupFrac = simplRacine.split('/')
              resteRacineNum[0] = simpQuotientFractions(resteRacineNum[0], recupFrac[1] + '/' + recupFrac[0])
              if (resteRacineNum[0] === '') {
                resteRacineNum[0] = '1'
              }
              if (resteRacineDen[0] !== '') {
                simplRacine = simpQuotientFractions(resteRacineNum[0], resteRacineDen[0])
              } else {
                simplRacine = resteRacineNum[0]
              }
              if (simplRacine === '') {
                simplRacine = '1'
              }
            }
          } else {
            const coef1 = simpQuotientFractions(resteRacineNum[0], resteRacineDen[0])
            if (String(coef1) !== '1') {
              simplRacine = coef1 + simplRacine
            }
          }
          simplRacine = transformationLatex(simplRacine)
        } else {
          if (resteRacineNum[0] === '') {
            resteRacineNum[0] = '1'
          }
          if (den.charAt(0) === '-') {
            signeDen = '-'
            den = den.substring(1, den.length)
          }
          if ((den.charAt(0) === '(') && (den.charAt(den.length - 1) === ')')) {
            den = den.substring(1, den.length - 1)
          }
          if (signeNum !== signeDen) {
            signeQuotient = 'negatif'
          }
          // il y a une racine carrée au numérateur, mais pas au dénominateur
          simplRacine = simpQuotientFractions(resteRacineNum[0], den) + '\\sqrt{' + nbRacineNum[0] + '}'
        }
        if (signeQuotient === 'negatif') {
          simplRacine = '-' + simplRacine
        }
        simplRacine = transformationLatex(simplRacine)
      } else {
        if (den.indexOf('racine') > -1) {
          // on a une racine carrée au numérateur et au dénominateur
          nbRacineDen = den.match(chaineRac)
          resteRacineDen = den.split(chaineRac)
          if (resteRacineDen[0].charAt(0) === '-') {
            signeDen = '-'
            resteRacineDen[0] = resteRacineDen[0].substring(1, resteRacineDen[0].length)
          }
          // on enlève les parenthèses éventuelles
          if ((resteRacineDen[0].charAt(0) === '(') && (resteRacineDen[0].charAt(resteRacineDen[0].length - 1) === ')')) {
            resteRacineDen[0] = resteRacineDen[0].substring(1, resteRacineDen[0].length - 1)
          }
          if (resteRacineDen[0] === '') {
            resteRacineDen[0] = '1'
          }
          if ((num.charAt(0) === '(') && (num.charAt(num.length - 1) === ')')) {
            num = num.substring(1, num.length - 1)
          }
          if (signeNum !== signeDen) {
            signeQuotient = 'negatif'
          }
          // il y a une racine carrée au numérateur, mais pas au dénominateur
          const coef1 = simpQuotientFractions(num, resteRacineDen[0])
          if (coef1 === '1') {
            simplRacine = '\\frac{1}{' + nbRacineDen[0] + '}'
          } else {
            simplRacine = coef1 + '\\frac{1}{' + nbRacineDen[0] + '}'
          }
          if (signeQuotient === 'negatif') {
            simplRacine = '-' + simplRacine
          }
        } else {
          if (num.charAt(0) === '-') {
            signeNum = '-'
            num = num.substring(1, num.length)
          }
          if (den.charAt(0) === '-') {
            signeDen = '-'
            den = den.substring(1, den.length)
          }
          if ((den.charAt(0) === '(') && (den.charAt(den.length - 1) === ')')) {
            den = den.substring(1, den.length - 1)
          }
          if ((num.charAt(0) === '(') && (num.charAt(num.length - 1) === ')')) {
            num = num.substring(1, num.length - 1)
          }
          if (num === '') {
            num = '1'
          }
          if (den === '') {
            den = '1'
          }
          if (signeNum !== signeDen) {
            signeQuotient = 'negatif'
          }
          simplRacine = simpQuotientFractions(num, den)
          if (simplRacine === '') {
            simplRacine = '1'
          }
          if (signeQuotient === 'negatif') {
            simplRacine = '-' + simplRacine
          }
        }
        simplRacine = transformationLatex(simplRacine)
      }
    }
    return simplRacine
  }

  function simpQuotientFractions (frac1, frac2) {
    // frac1 et frac2 sont de la forme a/b au format string ou bien a, toujours au format string
    let newNum, newDen, frac2Num, frac2Den
    if (frac1.indexOf('/') > -1) {
      const frac1Num = j3pNombre(frac1.split('/')[0])
      const frac1Den = j3pNombre(frac1.split('/')[1])
      if (frac2.includes('/')) {
        frac2Num = j3pNombre(frac2.split('/')[0])
        frac2Den = j3pNombre(frac2.split('/')[1])
        newNum = frac1Num * frac2Den
        newDen = frac2Num * frac1Den
      } else {
        // ici frac2 est un nombre
        newNum = frac1Num
        newDen = j3pNombre(frac2) * frac1Den
      }
    } else {
      // ici frac1 est un nombre
      if (frac2.includes('/')) {
        frac2Num = j3pNombre(frac2.split('/')[0])
        frac2Den = j3pNombre(frac2.split('/')[1])
        newNum = j3pNombre(frac1) * frac2Den
        newDen = frac2Num
      } else {
        // frac2 est aussi un nombre
        newNum = j3pNombre(frac1)
        newDen = j3pNombre(frac2)
      }
    }
    if (Math.abs(newNum / newDen - Math.round(newNum / newDen)) < Math.pow(10, -12)) {
      // le quotient est un entier
      // s’il est égal à 1'
      if (Math.abs(newNum / newDen - 1) < Math.pow(10, -12)) {
        return ''
      } else if (Math.abs(newNum / newDen + 1) < Math.pow(10, -12)) {
        return '-'
      } else {
        return String(newNum / newDen)
      }
    } else {
      const pgcdQuot = j3pPGCD(Math.abs(newNum), Math.abs(newDen))
      if (newNum * newDen < 0) {
        return '-' + String(Math.abs(newNum / pgcdQuot)) + '/' + String(Math.abs(newDen / pgcdQuot))
      } else {
        return String(Math.abs(newNum / pgcdQuot)) + '/' + String(Math.abs(newDen / pgcdQuot))
      }
    }
  }

  function constructionFct (fctImposee, numFct) {
    // cette fonction détermine l’expression d’une fonction affine (sous forme latex)'
    // et rend les coefficients a et b dans l’écriture "ax+b"'
    let coefA, coefB
    let i, arbreCalc
    let tabRacine, coefALatex, solEq, solEqLatex, lePgcd
    const chaineFraction = /\(?([0-9,.]+\/[0-9,.]+)\)?/g // new RegExp('\\(?([0-9,\\.]{1,}/[0-9,\\.]{1,})\\)?', 'ig')
    if (fctImposee !== '') {
      if (fctImposee === 'ax') {
        do {
          const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
          coefA = j3pGetRandomInt(minA, maxA)
        } while ((Math.abs(coefA) < Math.pow(10, -12)) || (Math.abs(coefA - 1) < Math.pow(10, -12)))
        fctImposee = j3pMonome(1, 1, coefA)
        stor.simplif_rep[numFct] = false
      } else if (fctImposee === 'x+b') {
        do {
          const [minB, maxB] = j3pGetBornesIntervalle(ds.a)
          coefB = j3pGetRandomInt(minB, maxB)
        } while (Math.abs(coefB) < Math.pow(10, -12))
        fctImposee = 'x' + j3pMonome(2, 0, coefB)
        stor.simplif_rep[numFct] = false
      }
      let imposerFctNew = ''
      if ((fctImposee.indexOf('racine') > -1) || (fctImposee.indexOf('/') > -1)) {
        stor.simplif_rep[numFct] = true
        // si j’ai ...racine(...), il faut que je mette un signe de multiplication avant racine'
        const chaineRacinenb = /([0-9)]racine\([0-9.]+\))/g // new RegExp('([0-9\\)]{1}racine\\([0-9\\.]{1,}\\))', 'ig')
        const tabRacineNb = fctImposee.split(chaineRacinenb)
        me.logIfDebug('tabRacineNb:' + tabRacineNb)
        for (i = 0; i < tabRacineNb.length; i++) {
          const posRacine = tabRacineNb[i].indexOf('racine(')
          if ((posRacine > 0) && (tabRacineNb[i].charAt(posRacine - 1) !== '-') && (tabRacineNb[i].charAt(posRacine - 1) !== '+')) {
            // ici on ajoute "*" avant racine(...)
            imposerFctNew += tabRacineNb[i].split('racine')[0] + '*racine' + tabRacineNb[i].split('racine')[1]
          } else {
            imposerFctNew += tabRacineNb[i]
          }
        }
        me.logIfDebug('imposerFctNew après modif :' + imposerFctNew)
        const chaineRacine = /(-?racine\([0-9.]+\))/g // new RegExp('([\\-]?racine\\([0-9\\.]{1,}\\))', 'ig')
        tabRacine = fctImposee.split(chaineRacine)

        // on cherche les coefficients a (écrit au format latex) et b
        let coefAInit
        let coefBInit
        let coefBInitOppose
        if (fctImposee.indexOf('x') === fctImposee.length - 1) {
          // dans ce cas x est le dernier caractère (fonction écrite sous la forme b+ax)
          const dernierePosPlus = fctImposee.lastIndexOf('+')
          const dernierePosMoins = fctImposee.lastIndexOf('-')
          coefAInit = fctImposee.substring(Math.max(dernierePosPlus, dernierePosMoins) + 1, fctImposee.length - 1)
          if (dernierePosMoins > dernierePosPlus) {
            // on ajoute un signe moins
            coefAInit = '-' + coefAInit
            coefBInit = fctImposee.substring(0, Math.max(dernierePosPlus, dernierePosMoins))
          }
        } else {
          // dans ce cas l’expression est sous la forme ax+b'
          const posX = fctImposee.indexOf('x')
          coefAInit = fctImposee.substring(0, posX)
          if (fctImposee.charAt(posX + 1) === '+') {
            coefBInit = fctImposee.substring(posX + 2, fctImposee.length)
          } else {
            coefBInit = fctImposee.substring(posX + 1, fctImposee.length)
          }
        }
        if (coefBInit.charAt(0) === '-') {
          coefBInitOppose = coefBInit.substring(1, coefBInit.length)
        } else {
          coefBInitOppose = '-' + coefBInit
        }
        me.logIfDebug('coefAInit:' + coefAInit + '   coefBInit:' + coefBInit)
        coefALatex = transformationLatex(coefAInit)
        const coefBOpposeLatex = transformationLatex(coefBInitOppose)
        stor.quotient_rep[numFct] = '\\frac{' + coefBOpposeLatex + '}{' + coefALatex + '}'
        // on cherche le zéro de la fonction affine écrit au format latex
        solEqLatex = simplificationQuotientLatex(coefBInitOppose, coefAInit)
        me.logIfDebug('coefALatex:' + coefALatex + '    coefBOpposeLatex:' + coefBOpposeLatex + '   solEq_corr:' + solEqLatex)
      } else {
        arbreCalc = new Tarbre(fctImposee, ['x'])
        coefA = arbreCalc.evalue([1]) - arbreCalc.evalue([0])
        coefB = arbreCalc.evalue([0])
        coefALatex = coefA
        // coefB_latex = coefB;
        if (Math.abs(coefB / coefA - Math.round(coefB / coefA)) < Math.pow(10, -12)) {
          // la racine est un entier
          solEqLatex = String(-coefB / coefA)
        } else {
          lePgcd = j3pPGCD(Math.abs(coefB), Math.abs(coefA))
          if (coefA * coefB < 0) {
            solEqLatex = '\\frac{' + String(Math.abs(coefB) / lePgcd) + '}{' + String(Math.abs(coefA) / lePgcd) + '}'
          } else {
            solEqLatex = '\\frac{' + String(-Math.abs(coefB) / lePgcd) + '}{' + String(Math.abs(coefA) / lePgcd) + '}'
          }
        }
        imposerFctNew = fctImposee
      }
      me.logIfDebug('imposerFctNew:' + imposerFctNew)
      arbreCalc = new Tarbre(imposerFctNew, ['x'])
      coefA = arbreCalc.evalue([1]) - arbreCalc.evalue([0])
      coefB = arbreCalc.evalue([0])
      solEq = -coefB / coefA
      me.logIfDebug('coefA:' + coefA + '   coefB:' + coefB + '    solEq:' + solEq + '   solEqLatex:' + solEqLatex)
    } else {
      let solutionEq
      do {
        do {
          coefA = j3pGetRandomInt(-10, 10)
        } while (Math.abs(coefA) < Math.pow(10, -13))
        coefB = j3pGetRandomInt(-10, 10)
        solutionEq = -coefB / coefA
      } while (Math.abs(solutionEq - Math.round(solutionEq)) < 0.00001)
      lePgcd = j3pPGCD(Math.abs(coefB), Math.abs(coefA))
      if (Math.abs(lePgcd + 1) < Math.pow(10, -12)) {
        stor.simplif_rep[numFct] = false
      } else {
        stor.simplif_rep[numFct] = true
        stor.quotient_rep[numFct] = '\\frac{' + String(coefA) + '}{' + String(coefB) + '}'
      }
      if (coefA * coefB < 0) {
        solEq = String(Math.abs(coefB) / lePgcd) + '/' + String(Math.abs(coefA) / lePgcd)
        solEqLatex = '\\frac{' + String(Math.abs(coefB) / lePgcd) + '}{' + String(Math.abs(coefA) / lePgcd) + '}'
      } else {
        solEq = String(-Math.abs(coefB) / lePgcd) + '/' + String(Math.abs(coefA) / lePgcd)
        solEqLatex = '\\frac{' + String(-Math.abs(coefB) / lePgcd) + '}{' + String(Math.abs(coefA) / lePgcd) + '}'
      }
    }
    me.logIfDebug('solEq : ' + solEq)
    let expressAffine, choixAlea
    if (fctImposee !== '') {
      if (fctImposee === 'ax') {
        expressAffine = j3pMonome(1, 1, coefA)
      } else if (fctImposee === 'x+b') {
        choixAlea = j3pGetRandomInt(0, 1)
        if (choixAlea === 0) {
          expressAffine = j3pMonome(1, 1, 1) + j3pMonome(2, 0, coefB)
        } else {
          expressAffine = j3pMonome(1, 0, coefB) + j3pMonome(2, 1, 1)
        }
      } else if ((fctImposee.indexOf('racine') > -1) || (fctImposee.indexOf('/') > -1)) {
        let expressAffine1 = ''
        // on remplace racine(...) par \\sqrt{...}
        for (i = 0; i < tabRacine.length; i++) {
          if (tabRacine[i].indexOf('racine') > -1) {
            // on a une racine carrée
            const chaineRacine2 = /racine\(/g // new RegExp('racine\\(', 'ig')
            const modifRacine1 = tabRacine[i].replace(chaineRacine2, '\\sqrt{')
            const chaineRacine3 = /\)/g // new RegExp('\\)', 'ig')
            expressAffine1 += modifRacine1.replace(chaineRacine3, '}')
          } else {
            expressAffine1 += tabRacine[i]
          }
        }
        me.logIfDebug('expressAffine1 après racine:' + expressAffine1)
        // on remplace .../... par \\frac{...}{...}

        const tabFraction2 = expressAffine1.split(chaineFraction)
        me.logIfDebug('tabFraction2:' + tabFraction2)
        expressAffine = ''
        for (i = 0; i < tabFraction2.length; i++) {
          if (tabFraction2[i].indexOf('/') > -1) {
            // on a une fraction
            const tabFrac = tabFraction2[i].split('/')
            expressAffine += '\\frac{' + tabFrac[0] + '}{' + tabFrac[1] + '}'
          } else {
            expressAffine += tabFraction2[i]
          }
        }
        me.logIfDebug('expressAffine après fraction:' + expressAffine)
      } else {
        expressAffine = fctImposee
      }
    } else {
      choixAlea = j3pGetRandomInt(0, 1)
      expressAffine = (choixAlea === 0) ? j3pMonome(1, 1, coefA) + j3pMonome(2, 0, coefB) : j3pMonome(1, 0, coefB) + j3pMonome(2, 1, coefA)
    }
    // si le signe "*" est présent, on le remplace par une vraie multiplication"
    me.logIfDebug('expressAffine:' + expressAffine)

    while (expressAffine.indexOf('*') > -1) {
      expressAffine = expressAffine.replace('*', '\\times ')
    }
    me.logIfDebug('modif multiplication expressAffine:' + expressAffine)

    return { expressAffine, coefALatex, solEq, solEqLatex }
  }

  function ecoute (num) {
    if (stor.mesZonesSaisie[num].className.includes('mq-editable-field')) {
      j3pDetruit(stor.laPaletteMQ)
      const divparent = (num === 0) ? stor.zoneCons4 : stor.zoneCons7
      stor.laPaletteMQ = j3pAddElt(divparent, 'div')
      j3pPaletteMathquill(stor.laPaletteMQ, stor.mesZonesSaisie[num], {
        liste: ['fraction', 'racine', 'vide']
      })
    }
  }
  function convertLatex (nb) {
    // convertion d’un nb écrit au format string au format latex'
    // c’est utile pour les fractions et les racines carrées'
    // cela va servir à écrire les solutions données par l’utilisateur'
    let i
    const fracString = /\(?([0-9]+\/[0-9]+)\)?/g // new RegExp('\\(?([0-9]{1,}/[0-9]{1,})\\)?', 'ig')
    const racineString = /(racine\([0-9]+\)|racine\([0-9]+\/[0-9]+\))/g // new RegExp('(racine\\([0-9]{1,}\\)|racine\\([0-9]{1,}/[0-9]{1,}\\))', 'g')
    let newnb = ''
    if (nb.includes('racine')) {
      // une racine est présente
      const tabRac = nb.split(racineString)
      for (i = 0; i < tabRac.length; i++) {
        if (tabRac[i].includes('racine')) {
          newnb += '\\sqrt{' + tabRac[i].substring(tabRac[i].indexOf('racine(') + 7, tabRac[i].indexOf(')')) + '}'
        } else {
          newnb += tabRac[i]
        }
      }
    } else {
      newnb = nb
    }
    let nbLatex = ''
    if (newnb.indexOf('/') > -1) {
      // une fraction est présente
      const tabFrac = newnb.split(fracString)
      for (i = 0; i < tabFrac.length; i++) {
        if (tabFrac[i].indexOf('/') > -1) {
          nbLatex += '\\frac{' + tabFrac[i].split('/')[0] + '}{' + tabFrac[i].split('/')[1] + '}'
        } else {
          nbLatex += tabFrac[i]
        }
      }
    } else {
      nbLatex = newnb
    }
    me.logIfDebug('nbLatex:' + nbLatex)
    return nbLatex
  }

  function afficheCorrection () {
    let i
    const espacetxt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
    j3pDetruit(stor.laPaletteMQ)
    if (ds.zero_seulement) j3pDetruit(stor.zoneCons5)
    else j3pDetruit(stor.zoneCons8)
    // var pos_y = stor.pos_y1+5;
    stor.explications1 = j3pAddElt(stor.zoneCons4, 'div')
    let ensvalinterdites = ''
    let enszeros = ''
    let ensvalinterdites2, vraiEnszeros2, vraiEnszeros
    if (ds.zero_seulement) {
      if (stor.egalite_zeros) {
        stor.explications1.style.color = me.styles.cbien
      } else {
        stor.explications1.style.color = me.styles.toutpetit.correction.color
        j3pAffiche(stor.zoneCons3, '', espacetxt, { style: { color: me.styles.petit.correction.color } })
        stor.zoneRep = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.toutpetit.correction })
        if (stor.tabZeroLatexNew.length === 0) {
          // reponse_zone
          j3pAffiche(stor.zoneCons3, '', '$\\emptyset$', { style: { color: me.styles.petit.correction.color } })
        } else {
          vraiEnszeros2 = stor.tabZeroLatexNew[0]
          if (stor.tabZeroLatexNew.length > 0) {
            for (i = 1; i < stor.tabZeroLatexNew.length; i++) {
              vraiEnszeros2 += ';' + stor.tabZeroLatexNew[i]
            }
          }
          j3pAffiche(stor.zoneCons3, '', '$' + vraiEnszeros2 + '$', { style: { color: me.styles.petit.correction.color } })
        }
      }
      if ((stor.tabZeroLatex.length === 1) && (stor.tabZeroLatexNew.length === 1)) {
        j3pAffiche(stor.explications1, '', ds.textes.corr4,
          {
            f: stor.nomF,
            n: stor.expressNum,
            d: stor.expressDen,
            z: stor.tabZeroLatex[0],
            s: stor.tabInterditLatex
          })
      } else {
        if (stor.tabZeroLatex.length === 0) {
          j3pAffiche(stor.explications1, '', ds.textes.corr4_5,
            {
              f: stor.nomF,
              n: stor.expressNum,
              d: stor.expressDen
            })
        } else {
          enszeros = stor.tabZeroLatex[0]
          if (stor.tabZeroLatex.length > 0) {
            for (i = 1; i < stor.tabZeroLatex.length; i++) {
              enszeros += ';' + stor.tabZeroLatex[i]
            }
          }
          if (stor.tabInterditLatex.length === 0) {
            j3pAffiche(stor.explications1, '', ds.textes.corr4_4,
              {
                f: stor.nomF,
                n: stor.expressNum,
                d: stor.expressDen,
                e: enszeros
              })
          } else {
            ensvalinterdites = stor.tabInterditLatex[0]
            if (stor.tabInterditLatex.length > 0) {
              for (i = 1; i < stor.tabInterditLatex.length; i++) {
                ensvalinterdites += ';' + stor.tabInterditLatex[i]
              }
            }
            if (stor.tabZeroLatexNew.length === 0) {
              j3pAffiche(stor.explications1, '', ds.textes.corr4ter,
                {
                  f: stor.nomF,
                  n: stor.expressNum,
                  d: stor.expressDen,
                  e: enszeros,
                  g: ensvalinterdites
                })
            } else {
              vraiEnszeros = stor.tabZeroLatexNew[0]
              if (stor.tabZeroLatexNew.length > 0) {
                for (i = 1; i < stor.tabZeroLatexNew.length; i++) {
                  vraiEnszeros += ';' + stor.tabZeroLatexNew[i]
                }
              }
              j3pAffiche(stor.explications1, '', ds.textes.corr4bis,
                {
                  f: stor.nomF,
                  n: stor.expressNum,
                  d: stor.expressDen,
                  e: enszeros,
                  g: ensvalinterdites,
                  h: vraiEnszeros
                })
            }
          }
        }
      }
    } else if (ds.interdite_seulement) {
      if (stor.egalite_interdites) {
        stor.explications1.style.color = me.styles.cbien
      } else {
        stor.explications1.style.color = me.styles.toutpetit.correction.color
        j3pAffiche(stor.zoneCons3, '', espacetxt)
        if (stor.tabInterdit.length === 0) {
          j3pAffiche(stor.zoneCons3, '', '$\\emptyset$', { style: { color: me.styles.petit.correction.color } })
        } else {
          ensvalinterdites2 = stor.tabInterditLatex[0]
          if (stor.tabInterditLatex.length > 0) {
            for (i = 1; i < stor.tabInterditLatex.length; i++) {
              ensvalinterdites2 += ';' + stor.tabInterditLatex[i]
            }
          }
          j3pAffiche(stor.zoneCons3, '', '$' + ensvalinterdites2 + '$', { style: { color: me.styles.petit.correction.color } })
        }
      }
      if (stor.tabInterdit.length === 0) {
        j3pAffiche(stor.explications1, '', ds.textes.corr5ter,
          { f: stor.nomF, d: stor.expressDen })
      } else if (stor.tabInterdit.length === 1) {
        j3pAffiche(stor.explications1, '', ds.textes.corr5,
          {
            f: stor.nomF,
            d: stor.expressDen,
            s: stor.tabInterditLatex
          })
      } else {
        ensvalinterdites = stor.tabInterditLatex[0]
        if (stor.tabInterditLatex.length > 0) {
          for (i = 1; i < stor.tabInterditLatex.length; i++) {
            ensvalinterdites += ';' + stor.tabInterditLatex[i]
          }
        }
        j3pAffiche(stor.explications1, '', ds.textes.corr5bis,
          {
            f: stor.nomF,
            d: stor.expressDen,
            e: ensvalinterdites
          })
      }
    } else {
      if (stor.egalite_interdites) {
        stor.explications1.style.color = me.styles.cbien
      } else {
        stor.explications1.style.color = me.styles.toutpetit.correction.color
        j3pAffiche(stor.zoneCons3, '', espacetxt)
        if (stor.tabInterdit.length === 0) {
          j3pAffiche(stor.zoneCons3, '', '$\\emptyset$', { style: { color: me.styles.petit.correction.color } })
        } else {
          ensvalinterdites2 = stor.tabInterditLatex[0]
          if (stor.tabInterditLatex.length > 0) {
            for (i = 1; i < stor.tabInterditLatex.length; i++) {
              ensvalinterdites2 += ';' + stor.tabInterditLatex[i]
            }
          }
          j3pAffiche(stor.zoneCons3, '', '$' + ensvalinterdites2 + '$', { style: { color: me.styles.petit.correction.color } })
        }
      }
      stor.explications2 = j3pAddElt(stor.zoneCons7, 'div')
      if (stor.egalite_zeros) {
        stor.explications2.style.color = me.styles.cbien
      } else {
        stor.explications2.style.color = me.styles.toutpetit.correction.color
        j3pAffiche(stor.zoneCons6, '', espacetxt, { style: { color: me.styles.petit.correction.color } })
        if (stor.tabZeroLatexNew.length === 0) {
          j3pAffiche(stor.zoneCons6, '', '$\\emptyset$', { style: { color: me.styles.petit.correction.color } })
        } else {
          vraiEnszeros2 = stor.tabZeroLatexNew[0]
          if (stor.tabZeroLatexNew.length > 0) {
            for (i = 1; i < stor.tabZeroLatexNew.length; i++) {
              vraiEnszeros2 += ';' + stor.tabZeroLatexNew[i]
            }
          }
          j3pAffiche(stor.zoneCons6, '', '$' + vraiEnszeros2 + '$', { style: { color: me.styles.petit.correction.color } })
        }
      }
      if (stor.tabInterdit.length === 0) {
        j3pAffiche(stor.explications1, '', ds.textes.corr1bis + '\n' + ds.textes.corr2bis,
          {
            f: stor.nomF,
            e: stor.expressDen,
            s: ensvalinterdites
          })
      } else {
        ensvalinterdites = stor.tabInterditLatex[0]
        if (stor.tabInterditLatex.length > 0) {
          for (i = 1; i < stor.tabInterditLatex.length; i++) {
            ensvalinterdites += ';' + stor.tabInterditLatex[i]
          }
        }
        j3pAffiche(stor.explications1, '', ds.textes.corr1 + '\n' + ds.textes.corr2,
          {
            f: stor.nomF,
            e: stor.expressDen,
            s: ensvalinterdites
          })
      }
      if (stor.tabZero.length === stor.tabZeroNew.length) {
        // dans ce cas, aucune valeur annulant le numérateur n’est une valeur interdite'
        if (stor.tabZero === '') {
          j3pAffiche(stor.explications2, '', ds.textes.corr3bis,
            {
              f: stor.nomF,
              e: stor.expressNum,
              s: enszeros
            })
        } else {
          enszeros = stor.tabZeroLatex[0]
          if (stor.tabZeroLatex.length > 0) {
            for (i = 1; i < stor.tabZeroLatex.length; i++) enszeros += ';' + stor.tabZeroLatex[i]
          }
          j3pAffiche(stor.explications2, '', ds.textes.corr3,
            {
              f: stor.nomF,
              e: stor.expressNum,
              s: enszeros
            })
        }
      } else {
        // il y a au moins une valeur annulant le numérateur qui est une valeur interdite
        enszeros = stor.tabZeroLatex[0]
        if (stor.tabZeroLatex.length > 0) {
          for (i = 1; i < stor.tabZeroLatex.length; i++) enszeros += ';' + stor.tabZeroLatex[i]
        }
        j3pAffiche(stor.explications2, '', ds.textes.corr6,
          {
            f: stor.nomF,
            e: stor.expressNum,
            s: enszeros
          })
        if (stor.tabZeroNew.length === 0) {
          let maCorr7 = ds.textes.corr7bis
          if (stor.tabInterditLatex.length > 1) {
            maCorr7 = ds.textes.corr7
          }
          j3pAffiche(stor.explications2, '', '\n' + maCorr7,
            {
              f: stor.nomF,
              e: stor.expressNum,
              s: enszeros
            })
        } else {
          vraiEnszeros = stor.tabZeroLatexNew[0]
          if (stor.tabZeroLatexNew.length > 0) {
            for (i = 1; i < stor.tabZeroLatexNew.length; i++) {
              vraiEnszeros += ';' + stor.tabZeroLatexNew[i]
            }
          }
          let maCorr8 = ds.textes.corr8bis
          if (stor.tabInterditLatex.length > 1) {
            maCorr8 = ds.textes.corr8
          }
          j3pAffiche(stor.explications2, '', '\n' + maCorr8,
            { s: vraiEnszeros })
        }
      }
      me.logIfDebug('ensvalinterdites:' + ensvalinterdites + '   enszeros:' + enszeros + '')
    }
  }

  function convertRacine (express) {
    // dans express, on remplace racine(...) par \sqrt{...}
    let newExpress = express
    const racineReg = /racine\([\d,.x+-]+\)/g // new RegExp('racine\\([0-9,\\.x+\\-]{1,}\\)', 'gi')
    if (racineReg.test(newExpress)) {
      const tabRacine = newExpress.match(racineReg)
      for (let i = 0; i < tabRacine.length; i++) {
        const radical = tabRacine[i].substring(7, tabRacine[i].length - 1)
        newExpress = newExpress.replace(tabRacine[i], '\\sqrt{' + radical + '}')
      }
    }
    return newExpress
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 2,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      imposer_fcts: ['|', '|'],
      fcts_affines: ['true|true', 'true|true'],
      zeros_fcts_nonaffines: ['|', '|'],
      zero_seulement: false,
      interdite_seulement: false,
      textes: {
        titre_exo: 'Valeur interdite ou qui annule une fonction',
        consigne1: 'Soit $£f$ la fonction définie par $£f(x)=£e$.',
        consigne2: 'Préciser la (ou les) valeur(s) en laquelle (ou lesquelles) s’annule la fonction $£f$&nbsp;:',
        consigne2bis: 'Préciser la (ou les) valeur(s) en laquelle (ou lesquelles) la fonction $£f$ n’est pas définie&nbsp;:',
        consigne3: 'Dans les deux cas :<br>- s’il y en a plusieurs, on séparera les valeurs par un point-virgule&nbsp;;',
        consigne3bis: '- S’il y en a plusieurs, on séparera les valeurs par un point-virgule;',
        consigne4: '- s’il n’y en a pas, on écrira l’ensemble vide (palette de boutons).',
        comment1: 'Tu n’as pas le bon nombre de valeurs en lesquelles la fonction n’est pas définie et s’annule.',
        comment2: 'Tu n’as pas le bon nombre de valeurs en lesquelles la fonction n’est pas définie.',
        comment3: 'Tu n’as pas le bon nombre de valeurs en lesquelles la fonction s’annule.',
        comment4: 'Il y a une erreur au niveau de la (ou les) valeur(s) interdite(s) ainsi que la (ou les) valeur(s) qui annule(nt) la fonction.',
        comment5: 'Il y a une erreur au niveau de la (ou les) valeur(s) qui annule(nt) la fonction.',
        comment6: 'Il y a une erreur au niveau de la (ou les) valeur(s) interdite(s).',
        corr1: 'On résout l’équation $£e=0$.<br>L’ensemble des solutions de cette équation est $\\bracket{£s}$.',
        corr1bis: 'On résout l’équation $£e=0$.<br>Cette équation n’admet pas de solution.',
        corr2: 'Donc $£f$ est définie sur $\\R\\setminus\\bracket{£s}$.',
        corr2bis: 'Donc $£f$ est définie sur $\\R$.',
        corr3: 'La fonction $£f$ s’annule lorsque $£e=0$.<br>L’ensemble des solutions de cette équation est $\\bracket{£s}$.',
        corr3bis: 'La fonction $£f$ s’annule lorsque $£e=0$.<br>Cette équation n’admet pas de solution donc $£f$ ne s’annule pas.',
        corr4: '$£f$ s’annule lorsque $£n=0$ et $£d\\neq 0$.<br>Or $£d\\neq 0$ pour $x\\neq £s$ et $£n=0$ pour $x=£z$.<br>Donc $£f$ s’annule en $£z$.',
        corr4bis: '$£f$ s’annule lorsque $£n=0$ et $£d\\neq 0$.<br>{$£e$} est l’ensemble des valeurs en lesquelles le numérateur s’annule.<br>$\\bracket{£g}$ est l’ensemble des valeurs en lesquelles le dénominateur s’annule.<br>Donc $\\bracket{£h}$ est l’ensemble des valeurs en lesquelles la fonction $£f$ s’annule.',
        corr4ter: '$£f$ s’annule lorsque $£n=0$ et $£d\\neq 0$.<br>{$£e$} est l’ensemble des valeurs en lesquelles le numérateur s’annule.<br>$\\bracket{£g}$ est l’ensemble des valeurs en lesquelles le dénominateur s’annule.<br>Donc la fonction $£f$ ne s’annule pas.',
        corr4_4: '$£f$ s’annule lorsque $£n=0$ et $£d\\neq 0$.<br>{$£e$} est l’ensemble des valeurs en lesquelles le numérateur s’annule et le dénominateur ne s’annule pas.<br>Donc $\\bracket{£e}$ est l’ensemble des valeurs en lesquelles la fonction $£f$ s’annule.',
        corr4_5: '$£f$ s’annule lorsque $£n=0$ et $£d\\neq 0$.<br>Or le numérateur ne s’annule pas, donc la fonction $£f$ ne s’annule pas.',
        corr5: '$£f$ est définie lorsque $£d\\neq 0$.<br>Or l’équation $£d=0$ a pour solution $x=£s$. Donc la valeur interdite est $£s$ et $£f$ est définie sur $\\R\\setminus\\bracket{£s}$.',
        corr5bis: '$£f$ est définie lorsque $£d\\neq 0$.<br>Or {$£e$} est l’ensemble des solutions de l’équation $£d=0$ c’est donc l’ensemble des valeurs interdites et $£f$ est définie sur $\\R\\setminus\\bracket{£e}$.',
        corr5ter: '$£f$ est définie lorsque $£d\\neq 0$.<br>Or l’équation $£d=0$ n’admet pas de solution, donc $£f$ n’admet pas de valeur interdite et elle est définie sur $\\R$.',
        corr6: 'L’ensemble des solutions de l’équation $£e=0$ est $\\bracket{£s}$.',
        corr7: 'Mais comme on exclut les valeurs interdites, la fonction ne s’annule pas.',
        corr7bis: 'Mais comme on exclut la valeur interdite, la fonction ne s’annule pas.',
        corr8: 'Mais comme on exclut les valeurs interdites, $\\bracket{£s}$ est l’ensemble des valeurs en lesquelles s’annule la fonction.',
        corr8bis: 'Mais comme on exclut la valeur interdite, $\\bracket{£s}$ est l’ensemble des valeurs en lesquelles s’annule la fonction.'
      },

      pe: 0
    }
  }
  function enonceMain () {
    stor.simplif_rep = []// ce tableau de booléens permet de savoir si les zéros des fonctions affines devront être simplifiés ou non
    stor.quotient_rep = []// ce tableau de bolléens contient le quotient sous forme non simplifiée
    // on construit les deux fonctions (affines le plus souvent)
    let expressNum
    let expressDen
    let tabZero = []
    const tabZeroLatex = []
    let tabInterdit = []
    const tabInterditLatex = []
    let pos
    // si zero_seulement vaut true, alors nécessairement interdite_seulement doit valoir false;
    if (ds.zero_seulement) {
      ds.interdite_seulement = false
    }
    me.logIfDebug('ds.imposer_fcts:' + ds.imposer_fcts)
    const pbRacineCalcul = /([\d]racine)/g // new RegExp('([0-9]racine)', 'ig')
    if (ds.fcts_affines[me.questionCourante - 1] === undefined) {
      ds.fcts_affines[me.questionCourante - 1] = 'true|true'
    }
    if (ds.imposer_fcts[me.questionCourante - 1] === undefined) {
      ds.imposer_fcts[me.questionCourante - 1] = '|'
    }
    // pour le numerateur
    if (String(ds.fcts_affines[me.questionCourante - 1]).split('|')[0] === 'true') {
      const paramFct1 = constructionFct(ds.imposer_fcts[me.questionCourante - 1].split('|')[0], 0)
      expressNum = paramFct1.expressAffine
      tabZero[0] = paramFct1.solEq
      tabZeroLatex[0] = paramFct1.solEqLatex
    } else {
      expressNum = ds.imposer_fcts[me.questionCourante - 1].split('|')[0]
      // pb si on y trouve racine(...)
      expressNum = convertRacine(expressNum)
      tabZero = ds.zeros_fcts_nonaffines[me.questionCourante - 1].split('|')[0].split('&')
      me.logIfDebug('zeros_fcts_nonaffines[0]:' + tabZero)
      for (let i = 0; i < tabZero.length; i++) {
        tabZeroLatex.push(convertLatex(tabZero[i]))
        while (tabZero[i].search(pbRacineCalcul) > -1) {
          // ...racine(...) est présent dans tabInterdit[i]
          pos = tabZero[i].search(pbRacineCalcul)
          tabZero[i] = tabZero[i].substring(0, pos + 1) + '*' + tabZero[i].substring(pos + 1, tabZero[i].length)
        }
      }
    }
    me.logIfDebug('tabZero:' + tabZero + '    tabZeroLatex : ' + tabZeroLatex)
    if (String(ds.fcts_affines[me.questionCourante - 1]).split('|')[1] === 'true') {
      let again
      do {
        const paramFct2 = constructionFct(ds.imposer_fcts[me.questionCourante - 1].split('|')[1], 1)
        expressDen = paramFct2.expressAffine
        tabInterdit[0] = paramFct2.solEq
        tabInterditLatex[0] = paramFct2.solEqLatex
        again = false
        if (String(ds.fcts_affines[me.questionCourante - 1]).split('|')[0] === 'true') {
          // C’est qu’on a un quotient de fonctions affines, donc on fait en sorte que le zéro du numérateur ne soit pas celui du dénominateur
          again = tabInterdit[0] === tabZero[0]
        }
      } while (again)
    } else {
      expressDen = ds.imposer_fcts[me.questionCourante - 1].split('|')[1]
      // pb si on y trouve racine(...)
      expressDen = convertRacine(expressDen)
      tabInterdit = ds.zeros_fcts_nonaffines[me.questionCourante - 1].split('|')[1].split('&')
      me.logIfDebug('zeros_fcts_nonaffines[1]:' + tabInterdit)
      for (let i = 0; i < tabInterdit.length; i++) {
        tabInterditLatex.push(convertLatex(tabInterdit[i]))
        while (tabInterdit[i].search(pbRacineCalcul) > -1) {
          // ...racine(...) est présent dans tabInterdit[i]
          pos = tabInterdit[i].search(pbRacineCalcul)
          tabInterdit[i] = tabInterdit[i].substring(0, pos + 1) + '*' + tabInterdit[i].substring(pos + 1, tabInterdit[i].length)
        }
      }
    }
    if ((tabInterdit.length === 1) && (tabInterdit[0] === '')) {
      // on vient ici si tabInterdit est de longueur 1 en contenant seulement ""
      // dans ce cas, on vide complètement tabInterdit
      tabInterdit = []
    }
    me.logIfDebug('tabInterdit:' + tabInterdit + '    tabInterditLatex : ' + tabInterditLatex)
    // il faut faire attention car il ne faut pas que des élèves de tabZero soit das tabInterdit
    const tabZeroNew = []
    const tabZeroLatexNew = []
    if (!((tabZero.length === 0) || ((tabZero.length === 1) && (tabZero[0] === '')))) {
      // on ne vient pas ici si tab_zeor est vide ou que tabZero est de longueur 1 en contenant seulement ""
      for (let i = 0; i < tabZero.length; i++) {
        let aAjouter = true
        for (let j = 0; j < tabInterdit.length; j++) {
          const arbreCalcNew = new Tarbre(tabZero[i] + '-(' + tabInterdit[j] + ')', ['x'])
          stor.madif = arbreCalcNew.evalue([0])
          if (Math.abs(stor.madif) < Math.pow(10, -12)) {
            aAjouter = false
          }
        }
        if (aAjouter) {
          tabZeroNew.push(tabZero[i])
          tabZeroLatexNew.push(tabZeroLatex[i])
        }
      }
    }
    stor.tabZero = tabZero
    stor.tabZeroNew = tabZeroNew
    stor.tabInterdit = tabInterdit
    stor.tabZeroLatex = tabZeroLatex
    stor.tabZeroLatexNew = tabZeroLatexNew
    stor.tabInterditLatex = tabInterditLatex
    stor.expressNum = expressNum
    stor.expressDen = expressDen
    const nomFTab = ['f', 'g', 'h']
    stor.mesZonesSaisie = []
    const nomF = j3pRandomTab(nomFTab, [0.3333, 0.3333, 0.3334])
    stor.nomF = nomF
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '12px' }) })
    for (let i = 1; i <= 9; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, { f: nomF, e: '\\frac{' + expressNum + '}{' + expressDen + '}' })
    stor.laPaletteMQ = j3pAddElt(stor.zoneCons4, 'div')
    if (ds.zero_seulement) {
      j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2,
        { f: nomF })
      const elt = j3pAffiche(stor.zoneCons1, '', '&1&',
        {
          inputmq1: { texte: '' }
        })
      stor.mesZonesSaisie.push(elt.inputmqList[0])
      j3pStyle(stor.zoneCons4, { paddingTop: '30px' })
      j3pAffiche(stor.zoneCons4, '', ds.textes.consigne3bis, { f: nomF })
      j3pAffiche(stor.zoneCons4, '', '\n' + ds.textes.consigne4, { f: nomF })
      mqRestriction(elt.inputmqList[0], '\\d,.;/-', { commandes: ['fraction', 'racine', 'vide'] })
    } else {
      // on demande d’abord là où la fonction n’est pas définie'
      j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2bis, { f: nomF })
      const elt = j3pAffiche(stor.zoneCons3, '', '&1&', { inputmq1: { texte: '' } })
      stor.mesZonesSaisie.push(elt.inputmqList[0])
      $(stor.mesZonesSaisie[0]).focusin(ecoute.bind(null, 0))
      if (!ds.interdite_seulement) {
        // on ne demande pas que les valeurs interdites
        // puis là où s’annule la fonction'
        j3pStyle(stor.zoneCons5, { paddingTop: '45px' })
        j3pAffiche(stor.zoneCons5, '', ds.textes.consigne2, { f: nomF })
        const elt = j3pAffiche(stor.zoneCons6, '', '&1&', { inputmq1: { texte: '' } })
        stor.mesZonesSaisie.push(elt.inputmqList[0])
        $(stor.mesZonesSaisie[1]).focusin(ecoute.bind(null, 1))
      }
      j3pStyle(stor.zoneCons8, { paddingTop: '45px' })
      if (ds.interdite_seulement) j3pAffiche(stor.zoneCons8, '', ds.textes.consigne3bis, { f: nomF })
      else j3pAffiche(stor.zoneCons8, '', ds.textes.consigne3, { f: nomF })
      j3pAffiche(stor.zoneCons8, '', '\n' + ds.textes.consigne4, { f: nomF })
      if (!ds.interdite_seulement) mqRestriction(stor.mesZonesSaisie[1], '\\d,.;/-', { commandes: ['fraction', 'racine', 'vide'] })
      mqRestriction(stor.mesZonesSaisie[0], '\\d,.;/-', { commandes: ['fraction', 'racine', 'vide'] })
    }
    // palette MQ :
    j3pPaletteMathquill(stor.laPaletteMQ, stor.mesZonesSaisie[0], {
      liste: ['fraction', 'racine', 'vide']
    })

    j3pFocus(stor.mesZonesSaisie[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    stor.explications = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.correction', { padding: '10px' }) })
    const mesZonesSaisie = stor.mesZonesSaisie.map((elt) => elt.id)
    if ((ds.zero_seulement) || (ds.interdite_seulement)) {
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: mesZonesSaisie,
        validePerso: mesZonesSaisie
      })
    } else {
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: mesZonesSaisie,
        validePerso: mesZonesSaisie
      })
    }

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        const presenceValeur = []
        const presenceValeurInt = []
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        let zeroEleve, repEleve2
        if (reponse.aRepondu) {
        // il faut maintenant que je teste les deux zones dont la validation est perso
          const bonneRep = [false, false]
          // pour les zéros
          let repEleve1
          let valAjoutee
          if ((ds.zero_seulement) || (ds.interdite_seulement)) {
            repEleve1 = $(stor.mesZonesSaisie[0]).mathquill('latex')
          } else {
            repEleve1 = $(stor.mesZonesSaisie[1]).mathquill('latex')
          }
          let interditeEleveInit, interditeEleve
          if (ds.interdite_seulement) {
          // on ne demande que les valeurs interdites
            interditeEleveInit = repEleve1.split(';')
            interditeEleve = []
            if (interditeEleveInit[0] !== '\\varnothing') {
              for (let i = 0; i < interditeEleveInit.length; i++) {
                if (interditeEleveInit[i] !== '') {
                  valAjoutee = j3pMathquillXcas(interditeEleveInit[i])
                  while (valAjoutee.includes('sqrt')) {
                    valAjoutee = valAjoutee.replace('sqrt', 'racine')
                  }
                  interditeEleve.push(valAjoutee)
                }
              }
            }
            me.logIfDebug('interditeEleve:' + interditeEleve)
          } else {
          // on ne demande que les zéros ou les zéros et les vals interdites
          // pour l’instant je ne teste que les zéros'
            const zeroEleveInit = repEleve1.split(';')
            me.logIfDebug('repEleve1:' + repEleve1 + '   zeroEleveInit:' + zeroEleveInit)
            zeroEleve = []
            if (zeroEleveInit[0] !== '\\varnothing') {
              for (let i = 0; i < zeroEleveInit.length; i++) {
                if (zeroEleveInit[i] !== '') {
                  valAjoutee = j3pMathquillXcas(zeroEleveInit[i])
                  while (valAjoutee.indexOf('sqrt') > -1) {
                    valAjoutee = valAjoutee.replace('sqrt', 'racine')
                  }
                  zeroEleve.push(valAjoutee)
                }
              }
            }
            me.logIfDebug('zeroEleve:' + zeroEleve)
          }
          if ((!ds.zero_seulement) && (!ds.interdite_seulement)) {
          // on fait de même pour les valeurs interdites
            repEleve2 = $(stor.mesZonesSaisie[0]).mathquill('latex')
            interditeEleveInit = repEleve2.split(';')

            me.logIfDebug('repEleve2:' + repEleve2 + '   interditeEleveInit:' + interditeEleveInit)
            interditeEleve = []
            if (interditeEleveInit[0] !== '\\varnothing') {
              for (let i = 0; i < interditeEleveInit.length; i++) {
                if (interditeEleveInit[i] !== '') {
                  valAjoutee = j3pMathquillXcas(interditeEleveInit[i])
                  while (valAjoutee.includes('sqrt')) {
                    valAjoutee = valAjoutee.replace('sqrt', 'racine')
                  }
                  interditeEleve.push(valAjoutee)
                }
              }
            }
            me.logIfDebug('interditeEleve:' + interditeEleve)
          }
          // on s’occupe de la validation des zéros de la fonction'
          stor.egalite_nbsinterdites_OK = true
          stor.egalite_nbszeros_OK = true// on l’initialise à true, pour le cas où on ne demanderait que la valeur interdite'
          let arbreCalc
          if (!ds.interdite_seulement) {
            stor.egalite_nbszeros_OK = (zeroEleve.length === stor.tabZeroNew.length)
            stor.egalite_zeros = true
            if (stor.egalite_nbszeros_OK) {
            // si l’élève a le bon nombre de zéro(s), on vérifie les valeurs
            // s’il n’y en a pas'
              if ((stor.tabZeroNew[0] === undefined) || (String(stor.tabZeroNew[0]) === '')) {
                stor.egalite_zeros = (repEleve1 === '\\varnothing')
              } else {
              // s’il y en a'
                for (let i = 0; i < zeroEleve.length; i++) {
                  presenceValeur[i] = false
                  for (let j = 0; j < stor.tabZeroNew.length; j++) {
                    arbreCalc = new Tarbre(stor.tabZeroNew[j] + '-(' + zeroEleve[i] + ')', ['x'])
                    stor.madif = arbreCalc.evalue([0])
                    if (Math.abs(stor.madif) < Math.pow(10, -12)) {
                      presenceValeur[i] = true
                    }
                  }
                  stor.egalite_zeros = (stor.egalite_zeros && presenceValeur[i])
                  me.logIfDebug('presenceValeur:' + presenceValeur + '   ' + stor.egalite_zeros + '   (la i-ème réponse de l’élève est-elle dans le tableau des réponses attendues au niveau des zéros)')
                }
              }
              stor.egalite_nbsinterdites_OK = true
            } else {
              stor.egalite_zeros = false
            }
          }
          if (!ds.zero_seulement) {
          // on s’occupe de la validation des valeurs interdites de la fonction'
            stor.egalite_nbsinterdites_OK = (interditeEleve.length === stor.tabInterdit.length)
            stor.egalite_interdites = true
            if (stor.egalite_nbsinterdites_OK) {
            // si l’élève a le bon nombre de valeur(s) interdite(s), on vérifie les valeurs
            // s’il n’y en a pas'
              if ((stor.tabInterdit[0] === undefined) || (String(stor.tabInterdit[0]) === '')) {
                stor.egalite_interdites = (repEleve2 === '\\varnothing')
              } else {
              // s’il y en a'
                for (let i = 0; i < interditeEleve.length; i++) {
                  presenceValeurInt[i] = false
                  for (let j = 0; j < stor.tabInterdit.length; j++) {
                    arbreCalc = new Tarbre(stor.tabInterdit[j] + '-(' + interditeEleve[i] + ')', ['x'])
                    stor.madif = arbreCalc.evalue([0])
                    if (Math.abs(stor.madif) < Math.pow(10, -12)) {
                      presenceValeurInt[i] = true
                    }
                  }
                  stor.egalite_interdites = (stor.egalite_interdites && presenceValeurInt[i])
                  me.logIfDebug('presenceValeurInt:' + presenceValeurInt + '   ' + stor.egalite_interdites + '   (la i-ème réponse de l’élève est-elle dans le tableau des réponses attendues au niveau des valeurs interdites)')
                }
              }
            } else {
              stor.egalite_interdites = false
            }
          }
          let nbZone
          if (ds.zero_seulement) {
            nbZone = 1
            bonneRep[0] = stor.egalite_zeros
            bonneRep[1] = true
            stor.egalite_interdites = true
          } else if (ds.interdite_seulement) {
            nbZone = 1
            bonneRep[0] = stor.egalite_interdites
            bonneRep[1] = true
            stor.egalite_zeros = true
          } else {
            nbZone = 2
            bonneRep[0] = stor.egalite_interdites
            bonneRep[1] = stor.egalite_zeros
          }
          reponse.bonneReponse = (stor.egalite_zeros && stor.egalite_interdites)
          for (let i = 0; i < nbZone; i++) {
            if (!bonneRep[i]) {
              fctsValid.zones.bonneReponse[i] = false
            }
          }
          // on appelle de nouveau la fonction qui va mettre en couleur les réponses bonnes ou fausses
          if ((!ds.zero_seulement) && (!ds.interdite_seulement)) {
            fctsValid.coloreUneZone(stor.mesZonesSaisie[1].id)
          }
          fctsValid.coloreUneZone(stor.mesZonesSaisie[0].id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.explications)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.explications.style.color = me.styles.cbien
            stor.explications.innerHTML = cBien
            afficheCorrection()
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.explications.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.explications.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              if ((ds.zero_seulement) || (ds.interdite_seulement)) {
                j3pDesactive(stor.mesZonesSaisie[0])
                j3pFreezeElt(stor.mesZonesSaisie[0])
              } else {
                stor.mesZonesSaisie.forEach((elt) => {
                  j3pDesactive(elt)
                  j3pFreezeElt(elt)
                })
              }
              afficheCorrection()
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.explications.innerHTML = cFaux
              if (!stor.egalite_nbszeros_OK) {
                if (!stor.egalite_nbsinterdites_OK) {
                  stor.explications.innerHTML += '<br>' + ds.textes.comment1
                } else {
                  stor.explications.innerHTML += '<br>' + ds.textes.comment3
                }
              } else {
                if (!stor.egalite_nbsinterdites_OK) {
                  stor.explications.innerHTML += '<br>' + ds.textes.comment2
                } else {
                // dans ce cas on a le bon nombre de zéros et valeurs interdites
                  if (!stor.egalite_interdites) {
                    if (!stor.egalite_zeros) {
                      stor.explications.innerHTML += '<br>' + ds.textes.comment4
                    } else {
                      stor.explications.innerHTML += '<br>' + ds.textes.comment6
                    }
                  } else {
                    if (!stor.egalite_zeros) {
                      stor.explications.innerHTML += '<br>' + ds.textes.comment5
                    }
                  }
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.explications.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.explications.innerHTML += '<br>' + regardeCorrection
                afficheCorrection()
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
