import { j3pAddElt, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pMonome, j3pPaletteMathquill, j3pPGCD } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mars 2020
        Cette section permet de calculer la limite de certaines suites
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Limite d’une suite',
  // on donne les phrases de la consigne
  consigne1: 'Calculer la limite suivante.',
  remarque: 'Tu n’as le droit qu’à une seule tentative.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Pour l’infini, il faut préciser le signe&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Comme $£{l1}$, le passage à l’inverse nous permet de conclure que $£{lfin}$.',
  corr2: 'Pour tout entier naturel $n$, $£e$. Or $£{l1}$, ainsi le passage à l’inverse nous permet de conclure que $£{lfin}$',
  corr3: 'Comme $£{l1}$ et $£{l2}$, par somme on obtient $£{lfin}$.',
  corr4: 'Sachant que $£{l1}$, on obtient $£{l2}$. De plus $£{l3}$ donc $£{lfin}$.',
  corr5: 'Sachant que $£e$ et que $£{l1}$, on obtient $£{l2}$. De plus $£{l3}$ donc par somme $£{lfin}$.',
  corr6: 'Comme $£{l1}$, on obtient $£{l2}$. De plus $£{l3}$ et ainsi par somme, $£{lfin}$.',
  corr7: '$£{l1}$ et $£{l2}$ donc par somme, $£{lfin}$.',
  corr8: 'Comme $£{l1}$, le passage à l’inverse nous permet de conclure que $£{lfin}$.'
}
/**
 * section limiteSuite
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCons3)
    j3pDetruit(stor.zoneCons4)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    const correction1 = textes['corr' + stor.quest]
    j3pAffiche(zoneExpli1, '', correction1, stor.objCons)
  }

  function donneesSuite (num) {
    // num est le cas de figure
    // Cette fonction renvoie un objet contenant différentes propriétés de la suite (son expression, sa limite, ...)
    let a
    let b
    let c
    let d
    let signe
    const obj = {}
    if (num === 1) {
      // c/(an+b)
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(Math.abs(a) - Math.abs(b)) < Math.pow(10, -12))
      do {
        c = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (j3pPGCD(Math.abs(a), Math.abs(c)) !== 1)
      const deno = j3pMonome(1, 1, a, 'n') + j3pMonome(2, 0, b, 'n')
      obj.un = '\\frac{' + c + '}{' + deno + '}'
      obj.lim = '0'
      obj.l1 = (a > 0) ? '\\limite{n}{+\\infty}{' + deno + '}=+\\infty' : '\\limite{n}{+\\infty}{' + deno + '}=-\\infty'
    } else if (num === 2) {
      // exp(-an)
      a = j3pGetRandomInt(1, 5)
      obj.un = '\\mathrm{e}^{' + j3pMonome(1, 1, -a, 'n') + '}'
      obj.lim = '0'
      obj.l1 = '\\limite{n}{+\\infty}{\\mathrm{e}^{' + j3pMonome(1, 1, a, 'n') + '}}=+\\infty'
      obj.e = obj.un + '=\\frac{1}{\\mathrm{e}^{' + j3pMonome(1, 1, a, 'n') + '}}'
    } else if (num === 3) {
      // an^2+bn
      signe = (2 * j3pGetRandomInt(0, 1) - 1)
      a = j3pGetRandomInt(1, 5) * signe
      do {
        b = j3pGetRandomInt(1, 5) * signe
      } while (j3pPGCD(Math.abs(a), Math.abs(b)) !== 1)
      obj.un = j3pMonome(1, 2, a, 'n') + j3pMonome(2, 1, b, 'n')
      obj.lim = (a > 0) ? '+\\infty' : '-\\infty'
      obj.l1 = '\\limite{n}{+\\infty}{' + j3pMonome(1, 2, a, 'n') + '}=' + ((a > 0) ? '+\\infty' : '-\\infty')
      obj.l2 = '\\limite{n}{+\\infty}{' + j3pMonome(1, 1, b, 'n') + '}=' + ((b > 0) ? '+\\infty' : '-\\infty')
    } else if (num === 4) {
      // cexp(n)+a/n^b
      signe = (2 * j3pGetRandomInt(0, 1) - 1)
      a = j3pGetRandomInt(1, 5)
      b = j3pGetRandomInt(1, 3)
      do {
        c = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(c - 1) < Math.pow(10, -12))
      obj.un = j3pMonome(1, 1, c, '\\mathrm{e}^n') + ((signe === '+') ? '+' : '-') + '\\frac{' + a + '}{' + j3pMonome(1, b, 1, 'n') + '}'
      obj.lim = (c > 0) ? '+\\infty' : '-\\infty'
      obj.l1 = '\\limite{n}{+\\infty}{\\mathrm{e}^n}=+\\infty'
      obj.l2 = '\\limite{n}{+\\infty}{' + j3pMonome(1, 1, c, '\\mathrm{e}^n') + '}=' + ((c > 0) ? '+\\infty' : '-\\infty')
      obj.l3 = '\\limite{n}{+\\infty}{\\frac{' + a + '}{' + j3pMonome(1, b, 1, 'n') + '}}=0'
    } else if (num === 5) {
      // cexp(-n)+an^b
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      b = j3pGetRandomInt(1, 3)
      do {
        c = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(c - 1) < Math.pow(10, -12))
      obj.un = j3pMonome(1, 1, c, '\\mathrm{e}^{-n}') + j3pMonome(2, b, a, 'n')
      obj.lim = (a > 0) ? '+\\infty' : '-\\infty'
      obj.e = j3pMonome(1, 1, c, '\\mathrm{e}^{-n}') + '=\\frac{' + c + '}{\\mathrm{e}^n}'
      obj.l1 = '\\limite{n}{+\\infty}{\\mathrm{e}^n}=+\\infty'
      obj.l2 = '\\limite{n}{+\\infty}{' + j3pMonome(1, 1, c, '\\mathrm{e}^{-n}') + '}=0'
      obj.l3 = '\\limite{n}{+\\infty}{' + j3pMonome(1, b, a, 'n') + '}=' + ((a > 0) ? '+\\infty' : '-\\infty')
    } else if (num === 6) {
      // aexp(n)+cn^b
      signe = (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        a = j3pGetRandomInt(1, 5) * signe
      } while (Math.abs(a - 1) < Math.pow(10, -12))
      b = j3pGetRandomInt(1, 3)
      do {
        c = j3pGetRandomInt(1, 5) * signe
      } while ((Math.abs(c - 1) < Math.pow(10, -12)) || (j3pPGCD(Math.abs(a), Math.abs(c)) !== 1))
      obj.un = j3pMonome(1, 1, a, '\\mathrm{e}^{n}') + j3pMonome(2, b, c, 'n')
      obj.lim = (a > 0) ? '+\\infty' : '-\\infty'
      obj.l1 = '\\limite{n}{+\\infty}{\\mathrm{e}^{n}}=+\\infty'
      obj.l2 = '\\limite{n}{+\\infty}{' + j3pMonome(1, 1, a, '\\mathrm{e}^{n}') + '}=' + ((a > 0) ? '+\\infty' : '-\\infty')
      obj.l3 = '\\limite{n}{+\\infty}{' + j3pMonome(1, b, c, 'n') + '}=' + ((a > 0) ? '+\\infty' : '-\\infty')
    } else if (num === 7) {
      // an^b+c/n^d
      do {
        a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(a - 1) < Math.pow(10, -12))
      b = j3pGetRandomInt(1, 3)
      do {
        d = j3pGetRandomInt(1, 3)
      } while (Math.abs(b - d) < Math.pow(10, -12))
      do {
        c = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(c - 1) < Math.pow(10, -12))
      obj.un = j3pMonome(1, b, a, 'n') + ((c > 0) ? '+' : '-') + '\\frac{' + Math.abs(c) + '}{' + j3pMonome(1, d, 1, 'n') + '}'
      obj.lim = (a > 0) ? '+\\infty' : '-\\infty'
      obj.l1 = '\\limite{n}{+\\infty}{' + j3pMonome(1, b, a, 'n') + '}=' + ((a > 0) ? '+\\infty' : '-\\infty')
      obj.l2 = '\\limite{n}{+\\infty}{\\frac{' + Math.abs(c) + '}{' + j3pMonome(1, d, 1, 'n') + '}}=0'
    } else if (num === 8) {
      // a/(bn^c+d)
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      c = j3pGetRandomInt(1, 3)
      do {
        d = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (j3pPGCD(Math.abs(b), Math.abs(d)) !== 1)
      obj.un = '\\frac{' + a + '}{' + j3pMonome(1, c, b, 'n') + j3pMonome(2, 0, d, 'n') + '}'
      obj.lim = '0'
      obj.l1 = '\\limite{n}{+\\infty}{' + j3pMonome(1, c, b, 'n') + j3pMonome(2, 0, d, 'n') + '}=' + ((b > 0) ? '+\\infty' : '-\\infty')
    }
    obj.lfin = '\\limite{n}{+\\infty}{' + obj.un + '}=' + obj.lim
    return obj
  }

  function enonceInitFirst () {
    // Construction de la page
    // Le paramètre définit la largeur relative de la première colonne
    stor.pourcentage = 0.7
    // j’ai choisi de mettre ce pourcentage en paramètre pour centrer la figure par la suite

    me.construitStructurePage({ structure: 'presentation1', ratioGauche: stor.pourcentage })
    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    stor.tabQuest = []
    for (let k = 1; k <= 7; k++) {
      stor.tabQuest.push(k)
    }
    stor.tabQuestInit = [...stor.tabQuest]

    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    stor.quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.objCons = {}
    Object.assign(stor.objCons, donneesSuite(stor.quest))
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1)
    const consigneLim = '$\\limite{n}{+\\infty}{' + stor.objCons.un + '}=$&1&'
    stor.elt = j3pAffiche(stor.zoneCons2, '', consigneLim, {
      inputmq1: { texte: '' }
    })
    mqRestriction(stor.elt.inputmqList[0], '\\d,+./^-', { commandes: ['inf', 'fraction', 'exp'] })
    if (ds.nbchances === 1) {
      j3pAffiche(stor.zoneCons3, '', textes.remarque, {
        style: { fontStyle: 'italic', fontSize: '0.9em' }
      })
    }
    j3pPaletteMathquill(stor.zoneCons4, stor.elt.inputmqList[0], {
      liste: ['inf', 'fraction', 'exp']
    })

    stor.elt.inputmqList[0].typeReponse = ['texte']
    stor.elt.inputmqList[0].reponse = [stor.objCons.lim]
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.elt.inputmqList[0].id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })
    j3pFocus(stor.elt.inputmqList[0])
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        let reponse = {}
        let infini = false
        reponse.aRepondu = stor.fctsValid.valideReponses()
        if (reponse.aRepondu) {
        // JE regarde si l’élève n’a écrit que infini sans signe
          infini = (stor.fctsValid.zones.reponseSaisie[0] === '\\infty')
          if (infini) {
            reponse.aRepondu = false
          } else {
            reponse = stor.fctsValid.validationGlobale()
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // On peut ajouter un commentaire particulier.
          let msgReponseManquante
          if (infini) {
            msgReponseManquante = textes.comment1
            j3pFocus(stor.elt.inputmqList[0])
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
              }
            }
          }
        }
        // Obligatoire
        me.finCorrection('navigation', true)
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
