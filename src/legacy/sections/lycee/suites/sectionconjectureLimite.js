import { j3pAddElt, j3pArrondi, j3pGetRandomElts, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pNombre, j3pPaletteMathquill, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { testIntervalleFermeDecimaux } from 'src/lib/utils/regexp'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexOppose, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        décembre 2019
        Dans cette section on donne une suite arithmético-géométrique. On demande de conjecturer sa limite (à l’aide du mode "suite" de la calculatrice)
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[2;7]|[0.2;0.9]', 'string', 'Intervalle où se trouve la valeur a dans l’expression u_{n+1}=au_n+b (bien sûr a non nul différent de 1). En imposant "[-1;1]", on aura un décimal de cet intervalle (bornes non acceptées). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[0.2;0.9]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis décimale de l’intervalle [0.2;0.9]. A la place d’un intervalle, on peut aussi mettre un nombre entier ou décimal.'],
    ['b', '[-6;6]', 'string', 'Intervalle où se trouve la valeur b dans l’expression u_{n+1}=au_n+b (b non nul). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier ou décimal.'],
    ['termeInitial', ['0|[-5;5]', '0|[-5;5]', '0|[-5;5]'], 'array', 'Tableau de taille nbrepetitions où chaque terme contient deux éléments séparés par | : le premier est l’indice du premier terme et le second sa valeur (imposée ou aléatoire dans un intervalle).']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'conjecturer la limite d’une suite',
  // on donne les phrases de la consigne
  consigne1: 'Soit $(£u_n)$ la suite définie par $£u_{£n}=£i$ et, pour tout entier $n\\geq £n$, par $£u_{n+1}=£j$.',
  consigne2: 'À l’aide de la calculatrice, conjecturer la limite de la suite $(£u_n).$',
  consigne3: '$\\limite{n}{+\\infty}{£u_n}$ semble valoir &1&.',
  consigne4: 'Attention, tu n’as qu’une tentative pour répondre !',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  commentPlusPrecis: 'Il faut être plus précis avec un signe + ou - !',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'À l’aide du mode suite de la calculatrice, on peut calculer les premiers termes&nbsp;:',
  corr2: 'On conjecture alors que $\\limite{n}{+\\infty}{£u_n}=£r$.'
}

/**
 * section conjectureLimite
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.laPalette)
    j3pDetruit(stor.zoneCon4)
    const laCouleur = (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
    stor.explications = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: laCouleur } })
    for (let k = 1; k <= 3; k++) stor['zoneExpli' + k] = j3pAddElt(stor.explications, 'div')
    j3pAffiche(stor.zoneExpli1, '', textes.corr1)
    // J’affiche quelques valeurs de u_n permettant de conjecturer la limite

    const objCorr = {}
    objCorr.styletexte = {}
    objCorr.u = stor.defSuite.nomU
    const indiceMax = Math.floor((stor.listeUn.length - 1) / 5) * 5
    objCorr.t1 = (Math.abs(j3pArrondi(stor.listeUn[indiceMax - 10], 8) - stor.listeUn[indiceMax - 10]) < Math.pow(10, -12)) ? '=' + j3pArrondi(stor.listeUn[indiceMax - 10], 8) : '\\approx ' + j3pArrondi(stor.listeUn[indiceMax - 10], 8)
    objCorr.t2 = (Math.abs(j3pArrondi(stor.listeUn[indiceMax - 5], 8) - stor.listeUn[indiceMax - 5]) < Math.pow(10, -12)) ? '=' + j3pArrondi(stor.listeUn[indiceMax - 5], 8) : '\\approx ' + j3pArrondi(stor.listeUn[indiceMax - 5], 8)
    objCorr.t3 = (Math.abs(j3pArrondi(stor.listeUn[indiceMax], 8) - stor.listeUn[indiceMax]) < Math.pow(10, -12)) ? '=' + j3pArrondi(stor.listeUn[indiceMax], 8) : '\\approx ' + j3pArrondi(stor.listeUn[indiceMax], 8)
    j3pAffiche(stor.zoneExpli2, '', '$£u_{' + (indiceMax - 10) + '}£{t1}$&nbsp;; $£u_{' + (indiceMax - 5) + '}£{t2}$&nbsp;; $£u_{' + indiceMax + '}£{t3}$.', objCorr)
    j3pAffiche(stor.zoneExpli3, '', textes.corr2,
      {
        r: stor.laLimite,
        u: stor.defSuite.nomU
      })
  }

  function genereAlea (int) {
    // int est un intervalle
    // mais on peut trouver des cas de figure où par faignantise, on aura utilisé cette fonction sans vérifier que int est un intervalle
    // Ce serait alors un nombre
    if (String(int).charAt(0) === '[') {
      // si les bornes sont entières, alors on génère un nombre entier aléatoire dans cet intervalle
      // si elles sont décimales, il faut que je génère un décimal de l’intervalle
      const PosPointVirgule = int.indexOf(';')
      let val1 = j3pNombre(int.substring(1, PosPointVirgule))
      let val2 = j3pNombre(int.substring(PosPointVirgule + 1, int.length - 1))
      let posPoint = -1
      if ((String(val1).indexOf('.') === -1)) {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = String(val2).length - String(val2).lastIndexOf('.') - 1
        }
      } else {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = Math.max(String(val1).length - String(val1).lastIndexOf('.') - 1, String(val2).length - String(val2).lastIndexOf('.') - 1)
        } else {
          posPoint = String(val1).length - String(val1).lastIndexOf('.') - 1
        }
      }
      if (val1 > val2) {
        // les bornes ne sont pas dans le bon ordre
        const val3 = val1
        val1 = val2
        val2 = val3
      }
      if (posPoint === -1) {
        // je n’ai que des nombres entiers
        return j3pGetRandomInt(val1, val2)
      } else {
        // j’ai des décimaux
        const borneInf = val1 * Math.pow(10, posPoint)
        const borneSup = val2 * Math.pow(10, posPoint)
        const nbAlea = j3pGetRandomInt(String(borneInf), String(borneSup)) * Math.pow(10, -posPoint)
        return j3pArrondi(nbAlea, 10)
      }
    } else {
      return int
    }
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut, nbRepet, tabValInterdites) {
    // cette fonction permet de vérifier si ce qui est renseigné est bien un intervalle ou un nombre (éventuellement fractionnaire) et ce plusieurs fois (en général nbrepetitions)
    // cela permet de valider ce qui peut être saisi par un utilisateur dans le cas où on peut imposer des valeurs
    // texteIntervalle est une chaîne où les éléments peuvent être séparés par '|' et où chaque élément est un nombre, une fraction ou un intervalle
    // si c’est un intervalle, la fonction va dans un second temps générer de manière aléatoire une valeur de cet intervalle
    // il peut y avoir des valeurs interdites précisées dans tabValInterdites, même si elles peuvent être dans l’intervalle
    // la fonction renvoie un tableau de nbRepet éléments
    // chaque élément est un intervalle voire l’intervalle par défaut si ce qui est précisé n’est aps compris
    // ce peut aussi être un nombre entier, décimal ou fractionnaire
    // dans ce dernier cas, on renvoit une chaine où on retrouve le code latex et la valeur décimale (arrondie) séparés par |

    /* cette fonction renvoie un objet. Cet objet contient 3 tableaux de nbRepet éléments
    - leType[k] vaut 'nombre', 'fraction' ou 'intervalle' suivant la valeur de texteIntervalle
    - expression contient l’écriture de chaque valeur imposée ou gérée aléatoirement quand leType[k] vaut 'intervalle'
    - valeur contient la valeur décimale de ces nombres, cela ne peut être utile que lorsque leType[k] vaut 'fraction'
    */
    function lgIntervalle (intervalle) {
      // cette fonction renvoie la longueur de l’intervalle'
      // si ce n’est pas un intervalle, alors elle renvoie 0'
      let lg = 0
      if (testIntervalleFermeDecimaux.test(intervalle)) {
        // on a bien un intervalle
        const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
        const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
        lg = borneSup - borneInf
      }
      return lg
    }

    function nbDansTab (nb, tab) {
      // test si la valeur correspondant à nb est dans le tableau tab (ses valeurs pouvant être au format latex)
      if (tab === undefined) {
        return false
      } else {
        let puisPrecision = 15
        if (Math.abs(nb) > Math.pow(10, -13)) {
          const ordrePuissance = String(Math.abs(nb)).indexOf('.')
          puisPrecision = (ordrePuissance === -1) ? 15 - String(Math.abs(nb)).length : 15 - ordrePuissance
        }
        for (let k = 0; k < tab.length; k++) {
          if (Math.abs(nb - j3pCalculValeur(tab[k])) < Math.pow(10, -puisPrecision)) {
            return true
          }
        }
        return false
      }
    }

    const nbFracReg = /-?[0-9]+\/[0-9]+/ // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const DonneesIntervalle = {}
    // DonneesIntervalle contient trois propriétés qui sont des tableaux de longueur nbRepet :
    // - leType : qui correspond à ce qui a été renseigné initialement ('intervalle', 'nombre', 'fraction') - 'nombre' désigne un entier ou un décimal
    // - expression : qui est le nombre (si type==='nombre'), une valeur générée aléatoirement dans un intervalle (si type==='intervalle'), l’écriture l’atex de la fraction (si type==='fraction')
    // - valeur : qui est la valeur décimale de l’expression (utile surtout si type==='fraction')
    DonneesIntervalle.leType = []
    DonneesIntervalle.expression = []
    DonneesIntervalle.valeur = []
    let nbAlea, valNb, tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).indexOf('|') > -1) {
      const tableau = texteIntervalle.split('|')
      for (k = 0; k < nbRepet; k++) {
        if (testIntervalleFermeDecimaux.test(tableau[k])) {
          // c’est bien un intervalle'
          if (lgIntervalle(tableau[k]) === 0) {
            // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
            valNb = tableau[k].substring(1, tableau[k].indexOf(';'))
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          } else {
            do {
              nbAlea = genereAlea(tableau[k])
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        } else {
          // peut être est-ce tout simplement un nombre :
          if ((tableau[k] === undefined) || (tableau[k] === '')) {
            // là l’expression n’est pas renseignée
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          } else if (nbFracReg.test(tableau[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tableau[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
            DonneesIntervalle.leType.push('fraction')
            DonneesIntervalle.valeur.push(valFrac)
          } else if (!isNaN(j3pNombre(tableau[k]))) {
            // on a un nombre
            DonneesIntervalle.expression.push(tableau[k])
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(tableau[k])
          } else {
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      }
    } else {
      if ((texteIntervalle === undefined) || (texteIntervalle === '')) {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      } else if (testIntervalleFermeDecimaux.test(String(texteIntervalle))) {
        if (lgIntervalle(String(texteIntervalle)) === 0) {
          // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
          valNb = String(texteIntervalle).substring(1, String(texteIntervalle).indexOf(';'))
          for (k = 0; k < nbRepet; k++) {
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          }
        } else {
          for (k = 0; k < nbRepet; k++) {
            do {
              nbAlea = genereAlea(texteIntervalle)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
          DonneesIntervalle.leType.push('fraction')
          DonneesIntervalle.valeur.push(valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push(String(texteIntervalle))
          DonneesIntervalle.leType.push('nombre')
          DonneesIntervalle.valeur.push(String(texteIntervalle))
        }
      } else {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      }
    }
    return DonneesIntervalle
  }

  function ConvertDecimal (nb, nbreDecimales) {
    // nb peut être un entier, un décimal ou un nb fractionnaire
    // si c’est un nb fractionnaire et que la forme décimale contient moins de nbreDecimales, alors on renvoie le nb sous forme décimale
    // sinon on renvoit nb sous son format initial
    const nbDecimal = j3pCalculValeur(nb)
    // pour éviter les pbs d’arrondi, je cherche par quelle puissance de 10 multiplier mon nombre (puis el divisier)
    let puisPrecision = 15
    if (Math.abs(nbDecimal) > Math.pow(10, -13)) {
      const ordrePuissance = String(Math.abs(nbDecimal)).indexOf('.')
      puisPrecision = (ordrePuissance === -1) ? 15 - String(Math.abs(nbDecimal)).length : 15 - ordrePuissance
    }
    let newNb = nb
    if (Math.abs(j3pArrondi(nbDecimal, puisPrecision) - nbDecimal) < Math.pow(10, -12)) {
      // on a une valeur décimale
      // Si elle a plus de deux chiffres après la virgule, on donne une valeur décimale
      const nbChiffreApresVirgule = String(nbDecimal).length - 1 - String(nbDecimal).lastIndexOf('.')
      if (nbChiffreApresVirgule <= nbreDecimales) {
        newNb = nbDecimal
      }
    }
    return newNb
  }

  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation1')
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    me.afficheTitre(textes.titre_exo)
    // on génère de manière aléatoire les variables et données de l’exercice
    stor.defSuite = {}
    stor.coefADonnees = constructionTabIntervalle(ds.a, '[2;7]', ds.nbrepetitions, [0, 1, -1, 0.3])
    stor.coefBDonnees = constructionTabIntervalle(ds.b, '[-6;6]', ds.nbrepetitions, [0])
    stor.defSuite.coefA = stor.coefADonnees.expression
    stor.defSuite.coefB = stor.coefBDonnees.expression

    // il faut que je gère le terme initial car j’ai un tableau dont les données sont de la forme 1|[...;...] ce second point pouvant être un entier ou un nb fractionnaire
    stor.defSuite.termeInit = []
    const TermeInitParDefaut = [0, '[1;5]']
    let onRecommence, k, termeU0, termeC, termeV0
    if (!isNaN(j3pNombre(String(ds.termeInitial)))) {
      // au lieu d’un tableau, n’est indiqué qu’un seul nombre. Celui-ci devient le rang initial de chaque répétition
      const leRangInit = j3pNombre(ds.termeInitial)
      for (k = 0; k < ds.nbrepetitions; k++) {
        onRecommence = false
        do {
          termeU0 = genereAlea(TermeInitParDefaut[1])
          // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
          termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
          termeV0 = j3pGetLatexSomme(termeU0, termeC)
          onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
        } while (onRecommence)
        stor.defSuite.termeInit.push([leRangInit, termeU0])
      }
    }
    for (k = 0; k < ds.nbrepetitions; k++) {
      if ((ds.termeInitial[k] === undefined) || (ds.termeInitial[k] === '')) {
        // on a rien renseigné donc par défaut, on met
        termeU0 = genereAlea(TermeInitParDefaut[1])
        stor.defSuite.termeInit.push([TermeInitParDefaut[0], termeU0])
      } else {
        // il faut la barre verticale pour séparer le rang et la valeur du terme initial
        const posBarre = String(ds.termeInitial[k]).indexOf('|')
        if (posBarre === -1) {
          const EntierReg = /[0-9]+/i
          // je n’ai pas la barre verticale
          // peut-être ai-je seulement l’indice du premier terme
          if (EntierReg.test(ds.termeInitial[k])) {
            // c’est un entier correspondant à l’indice du terme initial
            onRecommence = false
            do {
              termeU0 = genereAlea(TermeInitParDefaut[1])
              // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
              termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
              termeV0 = j3pGetLatexSomme(termeU0, termeC)
              onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
            } while (onRecommence)
            stor.defSuite.termeInit.push([ds.termeInitial[k], termeU0])
          } else {
            // donc je réinitialise avec la valeur par défaut
            onRecommence = false
            do {
              termeU0 = genereAlea(TermeInitParDefaut[1])
              // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
              termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
              termeV0 = j3pGetLatexSomme(termeU0, termeC)
              onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
            } while (onRecommence)
            stor.defSuite.termeInit.push([TermeInitParDefaut[0], termeU0])
          }
        } else {
          // la valeur (éventuellement aléatoire) du terme initial
          onRecommence = false

          let termInitAlea
          do {
            termInitAlea = constructionTabIntervalle(ds.termeInitial[k].substring(posBarre + 1), TermeInitParDefaut[1], 1).expression[0]
            // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
            termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
            termeV0 = j3pGetLatexSomme(termInitAlea, termeC)
            onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
            // console.log('onRecommence:',onRecommence,'  termeV0:',termeV0)
          } while (onRecommence)
          // au cas où je vérifie que la première valeur est bien entière
          let rangInit = ds.termeInitial[k].substring(0, posBarre)
          rangInit = (String(j3pNombre(rangInit)).indexOf('.') === -1) ? rangInit : TermeInitParDefaut[0]
          stor.defSuite.termeInit.push([rangInit, termInitAlea])
        }
      }
    }
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // si je n’ai pas de section précédente, je génère le nom de la suite (u_n) de manière aléatoire
    stor.defSuite.nomU = j3pGetRandomElts(['u', 'v'], 1)[0]
    stor.defSuite.a = stor.defSuite.coefA[me.questionCourante - 1]
    stor.defSuite.b = stor.defSuite.coefB[me.questionCourante - 1]
    const relRecurrence = j3pGetLatexMonome(1, 1, stor.defSuite.a, stor.defSuite.nomU + '_n') + j3pGetLatexMonome(2, 0, stor.defSuite.b, '')

    j3pAffiche(stor.zoneCons1, '', textes.consigne1,
      {
        u: stor.defSuite.nomU,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        i: stor.defSuite.termeInit[me.questionCourante - 1][1],
        j: relRecurrence
      })
    // je prends la suite intermédiaire v_n=u_n-b/(1-a). Je note c=-b/(1-a)
    stor.cFrac = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.b), j3pGetLatexSomme('1', j3pGetLatexOppose((j3pGetLatexQuotient(stor.defSuite.a * 1000, 1000)))))
    // si c est une valeur décimale simple, alors je l’affiche sous forme décimale
    stor.defSuite.c = ConvertDecimal(stor.cFrac, 2)
    j3pAffiche(stor.zoneCons2, '', textes.consigne2,
      { u: stor.defSuite.nomU })
    stor.defSuite.v0 = j3pGetLatexSomme(stor.defSuite.termeInit[me.questionCourante - 1][1], stor.cFrac)
    const elt3 = j3pAffiche(stor.zoneCons3, '', textes.consigne3,
      {
        u: stor.defSuite.nomU,
        inputmq1: { texte: '' }
      })
    j3pStyle(stor.zoneCons5, { paddingTop: '15px' })
    stor.laPalette = j3pAddElt(stor.zoneCons5, 'div')
    const eltInputMq = elt3.inputmqList[0]
    if (Math.abs(stor.defSuite.a) < 1) j3pPaletteMathquill(stor.laPalette, eltInputMq, { liste: ['fraction', 'inf'] })
    else j3pPaletteMathquill(stor.laPalette, elt3.inputmqList[0], { liste: ['inf'] })

    j3pAffiche(stor.zoneCons4, '', textes.consigne4)
    j3pStyle(stor.zoneCons4, { fontStyle: 'italic', fontSize: '0.85em' })

    // je mets de côté la réponse attendue
    stor.typeLimite = (Math.abs(stor.defSuite.a) < 1) ? 'nombre' : 'texte'
    stor.laLimite = (stor.typeLimite === 'nombre')
      ? j3pGetLatexOppose(stor.defSuite.c)
      : (j3pCalculValeur(stor.defSuite.v0) < 0) ? '-\\infty' : '+\\infty'
    stor.laLimiteVal = (stor.typeLimite === 'nombre')
      ? j3pCalculValeur(stor.laLimite)
      : stor.laLimite
    eltInputMq.typeReponse = [stor.typeLimite, 'exact']
    eltInputMq.reponse = [stor.laLimiteVal]
    const mesZonesSaisie = [eltInputMq.id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })

    // Les calculs suivants vont servir pour la correction :
    stor.listeUn = [stor.defSuite.termeInit[me.questionCourante - 1][1]]
    let j = 0
    let u = stor.defSuite.termeInit[me.questionCourante - 1][1]
    const borne = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b)// C’est que la suite sera croissante
      ? (stor.laLimite === '+\\infty') ? 200000 : j3pArrondi(j3pCalculValeur(stor.laLimite), 5) - Math.pow(10, -5)
      : (stor.laLimite === '-\\infty') ? -200000 : j3pArrondi(j3pCalculValeur(stor.laLimite), 5) + Math.pow(10, -5)
    let condition = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b) ? (u <= borne) : (u >= borne)
    while (condition || (j <= 10)) {
      u = j3pArrondi(stor.defSuite.a * u + stor.defSuite.b, 8)
      stor.listeUn.push(u)
      condition = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b) ? (u <= borne) : (u >= borne)
      j++
    }
    me.logIfDebug('listeUn:', stor.listeUn, 'la limite:', stor.laLimite)
    // me.donneesPersistantes.suites sert à envoyer des informations dans le bilan qui pourront être récupérées par une autre section sur les suites
    // Ceci a pour but de poursuivre l’étude de la suite définie dans cette section
    me.donneesPersistantes.suites = { ...stor.defSuite }
    me.logIfDebug('me.donneesPersistantes.suites:', me.donneesPersistantes.suites)
    mqRestriction(eltInputMq, '\\d,.+-/', {
      commandes: (Math.abs(stor.defSuite.a) < 1) ? ['fraction', 'inf'] : ['inf']
    })
    j3pFocus(eltInputMq)

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      enonceMain()
      break
    }

    case 'correction':
      // On teste si une réponse a été saisie
      {
        let infiniSansSigne = false
        let reponse = {
          aRepondu: false, bonneReponse: false
        }
        reponse.aRepondu = stor.fctsValid.valideReponses()
        const eltInputMq = stor.fctsValid.zones.inputs[0]
        if (reponse.aRepondu) {
          if (j3pValeurde(eltInputMq) === '\\infty') {
            // il ne me donne pas une réponse complète
            infiniSansSigne = true
            reponse.aRepondu = false
          } else {
            reponse = stor.fctsValid.validationGlobale()
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (infiniSansSigne) msgReponseManquante = textes.commentPlusPrecis
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          j3pFocus(eltInputMq)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation(true)

      break // case "navigation":
  }
}
