import $ from 'jquery'
import { j3pAddElt, j3pAjouteTableau, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pGetNewId, j3pRandomTab, j3pStyle, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, j3pMathsAjouteDans, mqRestriction, traiteMathQuillSansMultImplicites } from 'src/lib/mathquill/functions'
import { j3pDetruitToutesLesFenetres } from 'src/legacy/core/functionsJqDialog'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { signeFois } from 'src/lib/utils/number'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nb_etapes', 2, 'entier', 'nb d’étapes,1 ou 2, si 1 on ne demande que la relation de récurrence'],
    ['definition_suite', ['somme1', 'somme2', 'produit'], 'array', 'tableau non modifiable permettant de définir si la suite peut être définie comme somme 1 : u(n+1)=u(n)+a, somme 2 : u(n+1)=u(n)+/-n+a ou produit : u(n+1)=a*u(n)'],
    ['probas_definition', [0.5, 0.3, 0.2], 'array', 'probabilités d’apparition des définitions précédentes, les deux tableaux doivent donc avoir le même nombre d’éléments']
  ]
}
const textes = {
  titre_exo: 'Définition d’une suite',
  phrase1: 'Voici les premiers termes d’une suite définie par récurrence. Compléter le terme manquant :',
  phrase2: 'Quelle semble être la relation de récurrence, c’est-à-dire la relation permettant de calculer le terme $u_{n+1}$ en fonction du terme précédent $u_n$?',
  // phrase2 : "Quelle semble être la relation permettant de calculer un terme en fonction du précédent ?$$"
  correction1: 'Pour calculer un terme, on ajoute $£a$ au terme précédent.<br>Ainsi, ',
  correction2_1: 'Pour calculer un terme, on ajoute au terme précédent une quantité qui augmente d’un à chaque étape.<br>Ainsi, ',
  correction2_1bis: 'Pour calculer un terme, on ajoute au terme précédent une quantité qui diminue d’un à chaque étape.<br>Ainsi, ',
  correction2_2: 'Pour calculer un terme, comme on doit ajouter au précédent une quantité qui augmente d’un à chaque étape, le calcul de $u_{n+1}$ dépend du terme précédent $u_n$ mais aussi de l’indice $n$.<br>Pour $n=0$, $u_{0+1}=u_0£a$, on ajoute donc $£a$ en ajoutant l’indice $n$.<br>D’où, $u_{n+1}=u_n+n£a$',
  correction2_2bis: 'Pour calculer un terme, comme on doit ajouter au précédent une quantité qui diminue d’un à chaque étape, le calcul de $u_{n+1}$ dépend du terme précédent $u_n$ mais aussi de l’indice $n$.<br>Pour $n=0$, $u_{0+1}=u_0£a$, on ajoute donc $£a$ en enlevant l’indice $n$.<br>D’où, $u_{n+1}=u_n-n£a$',
  correction3: 'Pour calculer un terme, on multiplie le terme précédent par $£a$.<br>Ainsi, ',
  correction4: 'On demande d’exprimer $u_{n+1}$ en fonction de $u_{n}$, ce dernier doit donc être présent dans la réponse...'
}
/**
 * section Suites2
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  // fonction encore non externalisée pour desactiver une zone mq : on recréé l’affichage
  function depart () {
    // Attention cela ne marchait pas pour une suite géométrique de raison négative
    // Il fallait que le dans le test d'équivalence utilisé dans la figure on coche des deux côtés
    // Remplacement des valeurs et suppression des multiplications par 1
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABQAAmZy####AQD#AQAAAAAAAAAABSwAAALZAAABAQAAAAAAAAAAAAAACP####8AAAABAApDQ2FsY0NvbnN0AP####8AAnBpABYzLjE0MTU5MjY1MzU4OTc5MzIzODQ2#####wAAAAEACkNDb25zdGFudGVACSH7VEQtGP####8AAAABAAdDQ2FsY3VsAP####8AAWEAATEAAAABP#AAAAAAAAAAAAACAP####8AAWIAATIAAAABQAAAAAAAAAAAAAACAP####8AAWMAATMAAAABQAgAAAAAAAD#####AAAAAQAJQ0ZvbmNOVmFyAP####8AAWYACWEqeCtiKnkrY#####8AAAABAApDT3BlcmF0aW9uAAAAAAQAAAAABAL#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAAf####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAAEAgAAAAUAAAACAAAABgAAAAEAAAAFAAAAAwAAAAIAAXgAAXkAAAADAP####8AB3JlcG9uc2UACXgrMip5KzMuMgAAAAQAAAAABAAAAAAGAAAAAAAAAAQCAAAAAUAAAAAAAAAAAAAABgAAAAEAAAABQAmZmZmZmZoAAAACAAF4AAF5#####wAAAAQAEENUZXN0RXF1aXZhbGVuY2UA#####wAHZWdhbGl0ZQAAAAQAAAAFAQEAAAABP#AAAAAAAAABAQH#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8AAAAAP4AAAAEAAP####8QQDmAAAAAAABARmZmZmZmZgAAAAAAAAAAAAAAAAABAAAAAAAAAAAAFWVnYWxpdGU9I1ZhbChlZ2FsaXRlKf###############w=='
    me.mtgList = stor.mtgAppLecteur.createList(txtFigure)
  }
  function traiteMathQuill (ch) {
    ch = traiteMathQuillSansMultImplicites(ch)
    ch = me.mtgList.addImplicitMult(ch)// Traitement des multiplications implicites
    return ch
  }

  // utilisée en évaluation de l’étape 2
  function convertitPolynome (chaine) {
    // console.log('chaine=' + chaine)
    // la chaine peut contenir u_n et n, à remplacer par x et y, l’idéal pour se genre de choses étant les expressions régulières, quand on connait...
    chaine = chaine.replace(/u_n/, 'x')
    // console.log('chaine après traitement des u_n: ' + chaine)
    chaine = chaine.replace(/n/, 'y')
    // console.log('chaine après traitement des n: ' + chaine)
    return chaine
  }
  // gestion de la réponse eleve :
  function bonneReponse (rep) {
    stor.objDonnees.reponse = traiteMathQuill(rep)
    me.mtgList.giveFormula2('a', String(stor.objDonnees.a))
    me.mtgList.giveFormula2('b', String(stor.objDonnees.b))
    me.mtgList.giveFormula2('c', String(stor.objDonnees.c))
    me.mtgList.giveFormula2('reponse', stor.objDonnees.reponse)
    me.mtgList.calculateNG()// on recalcule tout dans MathGraph32
    if (me.isDebug) {
      console.debug('reponse de l’élève envoyée à mtg32:', me.mtgList.getFormula('reponse'), stor.objDonnees.reponse)
      console.debug('reponse attendue par mtg32:', me.mtgList.getForSimp('f'))
    }
    return me.mtgList.valueOf('egalite') === 1
  }
  // détection de la solution en fonction de n au lieu de
  // gestion de l’affichage de la correction
  function afficheCorrection (bonneReponse) {
    if (bonneReponse) j3pStyle(stor.zoneExpli, { color: me.styles.cbien })
    let phCorr
    if (stor.premiereQuestion) {
      switch (me.stockage[2]) {
        case 'somme1':
          // u(n+1)=u(n)+a
          phCorr = '$u_4=u_3' + signe(me.stockage[4]) + '=' + me.storage.solution[1] + '$'
          j3pAffiche(stor.zoneExpli, '', textes.correction1 + phCorr, { a: me.stockage[4] })
          break

        case 'somme2':
          // u(n+1)=u(n)+/-n+a
          if (me.stockage[5] === 1) {
            // augmente d’un
            phCorr = textes.correction2_1
          } else {
            // diminue d’un
            phCorr = textes.correction2_1bis
          }
          phCorr += '$u_4=u_3' + signe(me.stockage[4]) + '=' + me.storage.solution[1] + '$'
          j3pAffiche(stor.zoneExpli, '', phCorr, { a: me.stockage[4] })
          break

        case 'produit':
          // u(n+1)=a*u(n)
          phCorr = '$u_4=u_3\\times' + signeFois(me.stockage[4]) + '=' + me.storage.solution[1] + '$'
          j3pAffiche(stor.zoneExpli, '', textes.correction3 + phCorr, { a: me.stockage[4] })
          break
      }
    } else {
      // seconde étape
      j3pDetruit(stor.zoneCons4) // C’est pour virer la palette
      switch (me.stockage[2]) {
        case 'somme1':
          phCorr = '$u_{n+1}=u_{n}' + signe(me.stockage[4]) + '$'
          j3pAffiche(stor.zoneExpli, '', textes.correction1 + phCorr, { a: me.stockage[4] })

          break

        case 'somme2':
          if (me.stockage[5] === 1) {
            // augmente d’un
            phCorr = textes.correction2_2
          } else {
            // diminue d’un
            phCorr = textes.correction2_2bis
          }
          j3pAffiche(stor.zoneExpli, '', phCorr, { a: signe(me.stockage[6]) })

          break

        case 'produit':

          phCorr = '$u_{n+1}=u_{n}\\times' + signeFois(me.stockage[4]) + '='
          if (me.stockage[4] === -1) {
            phCorr += '-u_n$'
          } else {
            phCorr += me.stockage[4] + 'u_n$'
          }
          j3pAffiche(stor.zoneExpli, '', textes.correction3 + phCorr, { a: me.stockage[4] })

          break
      }
    }
  }

  function signe (nombre) {
    if (nombre < 0) {
      return '-' + Math.abs(nombre)
    } else {
      return '+' + Math.abs(nombre)
    }
  }
  function signeAlea (nombre) {
    if (nombre < 0) {
      return '-'
    } else {
      return '+'
    }
  }
  // la fonction qui génère les données
  function genereDonnees () {
    me.stockage[1] = []
    me.stockage[2] = j3pRandomTab(ds.definition_suite, ds.probas_definition)
    let a, b, j
    stor.objDonnees = {}
    // stor.objDonnees contiendra les variable a, b et c utiles pour la correction (dans l’écriture au_n+bn+c)
    switch (me.stockage[2]) {
      case 'somme1':
        // u(n+1)=u(n)+a
        do {
          a = j3pGetRandomInt(-9, 9)
          b = j3pGetRandomInt(-9, 9)
        } while (a === 0 || b === 0 || (a === 1 && b === 0))
        for (j = 0; j < 4; j++) {
          me.stockage[1][j] = '$' + (b + j * a) + '$'
        }
        me.storage.solution[1] = (b + 4 * a)
        me.storage.solution[2] = 'x' + signe(a)
        // ALEX 10
        me.stockage[4] = a
        stor.objDonnees.a = 1
        stor.objDonnees.b = 0
        stor.objDonnees.c = a
        break

      case 'somme2':
        // u(n+1)=u(n)+/-n+a
        {
          let alea
          do {
            a = j3pGetRandomInt(-9, 9)
            b = j3pGetRandomInt(-9, 9)
            // 1 ou -1
            alea = 2 * j3pGetRandomInt(0, 1) - 1
          } while (a === 0 || b === 0)
          me.stockage[1][0] = b
          me.stockage[5] = alea
          for (j = 1; j < 4; j++) {
            me.stockage[1][j] = (me.stockage[1][j - 1] + (j - 1) * alea + a)
          }

          me.storage.solution[1] = (me.stockage[1][3] + (3) * alea + a)
          me.stockage[4] = 3 * alea + a
          me.stockage[6] = a
          for (j = 0; j < 4; j++) {
            me.stockage[1][j] = '$' + me.stockage[1][j] + '$'
          }
          me.storage.solution[2] = 'x' + signeAlea(alea) + 'y' + signe(a)
          stor.objDonnees.a = 1
          stor.objDonnees.b = alea
          stor.objDonnees.c = a
        }
        break

      case 'produit':
        // u(n+1)=a*u(n)
        do {
          a = j3pGetRandomInt(-4, 4)
          b = j3pGetRandomInt(-9, 9)
        } while (a === 0 || b === 0 || a === 1)
        for (j = 0; j < 4; j++) {
          me.stockage[1][j] = '$' + (b * Math.pow(a, j)) + '$'
        }
        me.storage.solution[1] = (b * Math.pow(a, 4))
        me.storage.solution[2] = a + 'x'
        me.stockage[4] = a
        stor.objDonnees.a = a
        stor.objDonnees.b = 0
        stor.objDonnees.c = 0
        break
    }
    // utile en etape 2 :
    me.stockage[1][4] = '$' + me.storage.solution[1] + '$'
  }

  function genereDivs () {
    stor.zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    stor.zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    j3pAjouteTableau(stor.zoneCons2, {
      id: stor.idTableau,
      nbli: 2,
      nbcol: 5,
      tabid: stor.tabId,
      tabcss: []
    })
    for (let j = 0; j < 5; j++) j3pMathsAjouteDans(stor.tabId[0][j], { content: '$u_{' + j + '}$' })
    // console.log('me.stockage[1]:', me.stockage[1])
    for (let j = 0; j < 5; j++) j3pMathsAjouteDans(stor.tabId[1][j], { content: me.stockage[1][j] })
  }

  function enonceMain () {
    // on complète l’énoncé en fonction de l’étape et on génère les données aléatoires:
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.correction', { padding: '10px' }) })
    stor.zoneExpli = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.correction', { padding: '20px' }) })
    stor.tabId = []
    stor.idTableau = j3pGetNewId('untab')
    for (let i = 1; i <= 2; i++) stor.tabId.push([0, 1, 2, 3, 4].map(k => j3pGetNewId('a' + i + String(k))))
    stor.premiereQuestion = (me.questionCourante % ds.nbetapes) === 1 || ds.nbetapes === 1
    if (stor.premiereQuestion) {
      // Définition des balises div
      j3pDetruitToutesLesFenetres()
      stor.zoneCons1 = j3pAddElt(stor.conteneur, 'div')
      stor.zoneCons2 = j3pAddElt(stor.conteneur, 'div')
      // première étape:
      j3pAffiche(stor.zoneCons1, '', textes.phrase1)
      // appel à la fonction qui s’occuper de générer les donnnées en fonction du paramétrage
      genereDonnees()
      // on affiche les termes de la suite dans un div : (usage de j3pAjouteTableau)
      // le div Enonce étant créé et contenant déjà une consigne, on créé un div à la suite sans spécifier de coordonnées, il viendra se positionner en dessous :
      j3pAjouteTableau(stor.zoneCons2, {
        id: stor.idTableau,
        nbli: 2,
        nbcol: 5,
        tabid: stor.tabId,
        tabcss: []
      })
      for (let j = 0; j < 5; j++) j3pMathsAjouteDans(stor.tabId[0][j], { content: '$u_{' + j + '}$' })
      stor.elt = j3pAffiche(stor.tabId[1][4], '', ' &1&',
        {
          inputmq1: { texte: '', correction: me.storage.solution[1] }
        })
      mqRestriction(stor.elt.inputmqList[0], '-\\d.,/', { commandes: ['fraction'] })
      stor.elt.inputmqList[0].typeReponse = ['nombre', 'exact']
      stor.elt.inputmqList[0].reponse = [me.storage.solution[1]]
      // on complète maintenant la fin du tableau :
      // affichage des termes de la suite
      for (let j = 0; j < 4; j++) j3pMathsAjouteDans(stor.tabId[1][j], { content: me.stockage[1][j] })
      // me sert en correction
      me.stockage[3] = '$£a$'
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: [stor.elt.inputmqList[0].id]
      })
    } else {
      genereDivs()
      // ALEX 7 OK seconde étape :
      // on écrit tous les termes en noir (a24 uniquement)
      j3pEmpty(stor.tabId[1][4])
      j3pAffiche(stor.tabId[1][4], '', me.stockage[1][4])
      // puis on demande la relation de récurrence :
      j3pEmpty(stor.zoneCons1)
      j3pAffiche(stor.zoneCons1, '', textes.phrase2)
      // le div supplémentaire :
      stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div')

      stor.elt = j3pAffiche(stor.zoneCons3, '', '$u_{n+1}=$&1&',
        {
          inputmq1: { texte: '', correction: me.storage.solution[2] }
        })
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: [stor.elt.inputmqList[0].id],
        validePerso: [stor.elt.inputmqList[0].id]
      })
      mqRestriction(stor.elt.inputmqList[0], '()-+*\\d.,/n', { commandes: ['Un', 'n', 'puissance', 'fraction'] })
      // ALEX 5bis OK : me sert en correction :
      me.stockage[3] = '$u_{n+1}=£a$'
      stor.zoneCons4 = j3pAddElt(stor.conteneur, 'div')
      stor.zoneCons4.style.marginTop = '15px'
      j3pPaletteMathquill(stor.zoneCons4, stor.elt.inputmqList[0], {
        liste: ['Un', 'n', 'puissance']
      })
      // je redonne le focus a la zone qui était à la palette :
      j3pFocus(stor.elt.inputmqList[0])
      depart()
    }
    j3pFocus(stor.elt.inputmqList[0])
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        if (ds.nb_etapes === 1) {
          ds.nbetapes = 1
          ds.nbitems = ds.nbetapes * ds.nbrepetitions
        }
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.6 })
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.storage.solution = []
        me.afficheTitre(textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // chargement mtg
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':

      // On teste si une réponse a été saisie
      //  test si réponse, commun aux deux étapes
      // on teste si une réponse a été saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        let repEleve
        if (reponse.aRepondu && !stor.premiereQuestion) {
          repEleve = $(stor.elt.inputmqList[0]).mathquill('latex')
          reponse.bonneReponse = bonneReponse(convertitPolynome(repEleve))
          stor.fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
          stor.fctsValid.coloreUneZone(stor.elt.inputmqList[0].id)
        }
        if (!reponse.aRepondu && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.elt.inputmqList[0])
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // ALEX 5 OK externalisation du test de la bonne reponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(true)
            j3pDesactive(stor.elt.inputmqList[0])
            stor.elt.inputmqList[0].style.color = me.styles.cbien
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // ALEX 6 OK
              afficheCorrection(false)
              // ALEX 5bis OK :
              j3pDesactive(stor.elt.inputmqList[0])
              stor.elt.inputmqList[0].style.color = me.styles.cfaux
            } else {
            // Réponse fausse :
              j3pEmpty(stor.zoneCorr)
              if (!stor.premiereQuestion) {
                const expEleve = convertitPolynome(repEleve)
                if (!expEleve.includes('x')) {
                // pas de u(n)
                  j3pMathsAjouteDans(stor.zoneCorr, { content: textes.correction4 })
                } else {
                  stor.zoneCorr.innerHTML = cFaux
                }
              } else {
                stor.zoneCorr.innerHTML = cFaux
              }

              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                j3pFocus(stor.elt.inputmqList[0])
                return me.finCorrection()
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // ALEX 6 BIS OK :
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
}
