import { j3pShuffle, j3pFocus, j3pGetRandomInt, j3pRandomTab, j3pRestriction, j3pAddElt } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        février 2020
        Dans cette section, on utilise un théorème de convergence d’une suite
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['nbetapes', 2, 'entier', 'Par défaut cette section est en deux étapes. Mais si on donne à ce paramètre la valeur 1, toutes les questions seront posées au cours de la même étape.']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Théorème de convergence d’une suite',
  // on donne les phrases de la consigne
  consigne1_1: 'Soit $(£u_n)$ la suite définie sur $\\N$ et vérifiant pour tout entier naturel $£i$.',
  consigne1_2: 'Soit $(£u_n)$ la suite définie sur $\\N^*$ et vérifiant pour tout entier naturel non nul $£i$.',
  consigne2: 'Donner les deux mots permettant de justifier que la suite $(£u_n)$ est convergente&nbsp;:',
  consigne3: 'La suite $(£u_n)$ est @1@ et @2@.',
  consigne4: 'La suite $(£u_n)$ est £{r1} et £{r2} par $£l$.',
  consigne5: 'Que vérifie alors sa limite $\\ell$&nbsp;?',
  monotonie1: 'croissante|croissant|croisante|croisant',
  monotonie2: 'décroissante|décroissant|decroissante|decroissant|décroisante|décroisant|decroisante|decroisant',
  majoree: 'majorée|majoré|majoree|majore',
  minoree: 'minorée|minoré|minoree|minore',
  indic: 'Tu n’as qu’une tentative pour répondre&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Pour tout $n\\in £n$, $£{i1}$ donc la suite $(£u_n)$ est £{r1}.',
  corr2: 'De plus, pour tout $n\\in £n$, $£{i2}$ donc la suite $(£u_n)$ est £{r2} par $£l$.',
  corr3: 'La suite $(£u_n)$ est alors convergente.',
  corr4: 'Sa limite $\\ell$ vérifie alors $£{i3}$.'
}

/**
 * section convergenceSuite
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    if ((ds.nbetapes === 2) && (me.questionCourante % ds.nbetapes === 0)) {
      if (!bonneReponse) {
        stor.zoneexplication1 = j3pAddElt(zoneExpli, 'div')
        j3pAffiche(stor.zoneexplication1, '', textes.corr4, stor.objCons)
      }
    } else {
      for (let k = 1; k <= 3; k++) {
        stor['zoneExpli' + k] = j3pAddElt(zoneExpli, 'div')
        j3pAffiche(stor['zoneExpli' + k], '', textes['corr' + k], stor.objCons)
      }
      if (ds.nbetapes === 1) {
        stor.zoneexplication4 = j3pAddElt(zoneExpli, 'div')
        j3pAffiche(stor.zoneexplication4, '', textes.corr4, stor.objCons)
      }
    }
  }

  function enonceInitFirst () {
    // Construction de la page
    me.construitStructurePage('presentation1')

    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    let tabTypeSuite = ['croissante', 'decroissante']
    const tabTypeSuiteInit = [...tabTypeSuite]
    const choixMonotonie = []
    for (let k = 1; k <= ds.nbrepetitions; k++) {
      if (tabTypeSuite.length === 0) {
        tabTypeSuite = [...tabTypeSuiteInit]
      }
      const pioche = j3pGetRandomInt(0, tabTypeSuite.length - 1)
      choixMonotonie.push(tabTypeSuite[pioche])
      tabTypeSuite.splice(pioche, 1)
    }
    stor.choixMonotonie = j3pShuffle(choixMonotonie)
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '15px' }) })
    if (ds.nbetapes === 1 || me.questionCourante % ds.nbetapes === 1) {
      stor.objCons = {}
      // Nouvelle suite
      stor.objCons.u = j3pRandomTab(['u', 'v', 'w'], [0.34, 0.33, 0.33])
      stor.objCons.n = (j3pGetRandomInt(0, 1) === 0) ? '\\N' : '\\N^*'
      stor.objCons.l = j3pGetRandomInt(-7, 7)
      stor.monotonie = stor.choixMonotonie[Math.floor(me.questionCourante / ds.nbetapes)]
      const signeIneq = []
      for (let i = 1; i <= 2; i++) {
        const signe = (stor.monotonie === 'croissante')
          ? (j3pGetRandomInt(0, 1)) ? '<' : '\\leq '
          : (j3pGetRandomInt(0, 1)) ? '>' : '\\geq '
        signeIneq.push(signe)
      }
      stor.objCons.i = stor.objCons.u + '_n ' + signeIneq[0] + ' ' + stor.objCons.u + '_{n+1} ' + signeIneq[1] + ' ' + stor.objCons.l

      // console.log('inequation:', stor.objCons.i)

      stor.objCons.i1 = stor.objCons.u + '_n ' + signeIneq[0] + ' ' + stor.objCons.u + '_{n+1}'
      stor.objCons.i2 = stor.objCons.u + '_n ' + signeIneq[1] + ' ' + stor.objCons.l
      stor.objCons.i3 = (stor.monotonie === 'croissante')
        ? '\\ell\\leq ' + stor.objCons.l
        : '\\ell\\geq ' + stor.objCons.l
      stor.objCons.r1 = (stor.monotonie === 'croissante') ? textes.monotonie1.split('|')[0] : textes.monotonie2.split('|')[0]
      stor.objCons.r2 = (stor.monotonie === 'croissante') ? textes.majoree.split('|')[0] : textes.minoree.split('|')[0]
    }
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    const consigne1 = (stor.objCons.n === '\\N') ? textes.consigne1_1 : textes.consigne1_2
    j3pAffiche(stor.zoneCons1, '', consigne1, stor.objCons)
    if (ds.nbetapes === 2) {
      if (me.questionCourante % ds.nbetapes === 1) {
        j3pAffiche(stor.zoneCons2, '', textes.consigne2, stor.objCons)
        const elts = j3pAffiche(stor.zoneCons3, '', textes.consigne3, {
          u: stor.objCons.u,
          input1: { texte: '', dynamique: true, width: '12px' },
          input2: { texte: '', dynamique: true, width: '12px' }
        })
        j3pRestriction(elts.inputList[0], 'a-zéèàùêçîô')
        j3pRestriction(elts.inputList[1], 'a-zéèàùêçîô')
        elts.inputList[0].typeReponse = ['texte']
        elts.inputList[1].typeReponse = ['texte']
        elts.inputList[0].reponse = (stor.monotonie === 'croissante') ? textes.monotonie1.split('|') : textes.monotonie2.split('|')
        elts.inputList[1].reponse = (stor.monotonie === 'croissante') ? textes.majoree.split('|') : textes.minoree.split('|')
        j3pFocus(elts.inputList[0])
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: elts.inputList.map(elt => elt.id),
          validePerso: elts.inputList.map(elt => elt.id)
        })
        stor.lesZones = elts.inputList
      } else {
        j3pAffiche(stor.zoneCons2, '', textes.consigne4, stor.objCons)
        j3pAffiche(stor.zoneCons3, '', textes.consigne5, stor.objCons)
        const elt1 = j3pAffiche(stor.zoneCons4, '', '$\\ell$ #1# $£l$', {
          l: stor.objCons.l,
          liste1: { texte: ['', '=', '<', '>', '&le;', '&ge;'] }
        })
        elt1.selectList[0].reponse = (stor.monotonie === 'croissante') ? 4 : 5
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: [elt1.selectList[0].id]
        })
        j3pAffiche(stor.zoneCons5, '', textes.indic, {
          style: { fontStyle: 'italic', fontSize: '0.9em' }
        })
        j3pFocus(elt1.selectList[0])
        stor.lesZones = [elt1.selectList[0]]
      }
    } else {
      j3pAffiche(stor.zoneCons2, '', textes.consigne2, stor.objCons)
      const elt1 = j3pAffiche(stor.zoneCons3, '', textes.consigne3, {
        u: stor.objCons.u,
        input1: { texte: '', dynamique: true, width: '12px' },
        input2: { texte: '', dynamique: true, width: '12px' }
      })
      j3pAffiche(stor.zoneCons4, '', textes.consigne5)
      j3pRestriction(elt1.inputList[0], 'a-zéèàùêçîô')
      j3pRestriction(elt1.inputList[1], 'a-zéèàùêçîô')
      elt1.inputList[0].typeReponse = ['texte']
      elt1.inputList[1].typeReponse = ['texte']
      elt1.inputList[0].reponse = (stor.monotonie === 'croissante') ? textes.monotonie1.split('|') : textes.monotonie2.split('|')
      elt1.inputList[1].reponse = (stor.monotonie === 'croissante') ? textes.majoree.split('|') : textes.minoree.split('|')
      const elt2 = j3pAffiche(stor.zoneCons5, '', '$\\ell$ #1# $£l$', {
        l: stor.objCons.l,
        liste1: { texte: ['', '=', '<', '>', '&le;', '&ge;'] }
      })
      elt2.selectList[0].reponse = (stor.monotonie === 'croissante') ? 4 : 5
      j3pFocus(elt1.inputList[0])
      stor.lesZones = [elt1.inputList[0], elt1.inputList[1], elt2.selectList[0]]
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: stor.lesZones.map(elt => elt.id),
        validePerso: elt1.inputList.map(elt => elt.id)
      })
    }

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      if ((ds.nbetapes === 2) && (me.questionCourante % ds.nbetapes === 0)) {
        // On n’autorise qu’une seule chance dans ce cas de figure
        me.essaiCourant = ds.nbchances
      }
      {
        const reponse = stor.fctsValid.validationGlobale()
        if (reponse.aRepondu && ((ds.nbetapes === 1) || (me.questionCourante % ds.nbetapes === 1))) {
        // il faut aussi que je teste les deux zones dont la validation est perso
          let bonneReponse2 = false
          const listeCroissante = textes.monotonie1.split('|')
          const listeDecroissante = textes.monotonie2.split('|')
          const listeMajoree = textes.majoree.split('|')
          const listeMinoree = textes.minoree.split('|')

          let choix2 = true
          if (stor.monotonie === 'croissante') {
            bonneReponse2 = (bonneReponse2 || (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[0], listeCroissante).bonneReponse && stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[1], listeMajoree).bonneReponse))
            if (bonneReponse2) {
              stor.fctsValid.zones.inputs[0].value = listeCroissante[0]
              stor.fctsValid.zones.inputs[1].value = listeMajoree[0]
              choix2 = false
            }
            bonneReponse2 = (bonneReponse2 || (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[1], listeCroissante).bonneReponse && stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[0], listeMajoree).bonneReponse))
            if (bonneReponse2 && choix2) {
              stor.fctsValid.zones.inputs[1].value = listeCroissante[0]
              stor.fctsValid.zones.inputs[0].value = listeMajoree[0]
            }
          } else {
            bonneReponse2 = (bonneReponse2 || (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[0], listeDecroissante).bonneReponse && stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[1], listeMinoree).bonneReponse))
            if (bonneReponse2) {
              stor.fctsValid.zones.inputs[0].value = listeDecroissante[0]
              stor.fctsValid.zones.inputs[1].value = listeMinoree[0]
              choix2 = false
            }
            bonneReponse2 = (bonneReponse2 || (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[1], listeDecroissante).bonneReponse && stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[0], listeMinoree).bonneReponse))
            if (bonneReponse2 && choix2) {
              stor.fctsValid.zones.inputs[1].value = listeDecroissante[0]
              stor.fctsValid.zones.inputs[0].value = listeMinoree[0]
            }
          }
          reponse.bonneReponse = (reponse.bonneReponse && bonneReponse2)
          // bonneReponse2 ne prend en compte que les zones dans validePerso
          if (!bonneReponse2) {
            if (stor.monotonie === 'croissante') {
              if ((stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[0], listeCroissante).bonneReponse) || (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[0], listeMajoree).bonneReponse)) {
                stor.fctsValid.zones.bonneReponse[0] = true
                stor.fctsValid.zones.bonneReponse[1] = false
                stor.fctsValid.zones.inputs[0].value = (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[0], listeCroissante).bonneReponse) ? listeCroissante[0] : listeMajoree[0]
              } else {
                stor.fctsValid.zones.bonneReponse[0] = false
                if ((stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[1], listeCroissante).bonneReponse) || (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[1], listeMajoree).bonneReponse)) {
                  stor.fctsValid.zones.bonneReponse[1] = true
                  stor.fctsValid.zones.inputs[1].value = (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[1], listeCroissante).bonneReponse) ? listeCroissante[0] : listeMajoree[0]
                } else {
                  stor.fctsValid.zones.bonneReponse[1] = false
                }
              }
            // si les 2 zones de validePerso ne sont pas bonnes, on met false dans stor.fctsValid.zones.bonneReponse[i] où i est la position de la zone dans le tableau mes_zones_saisie
            } else {
              if ((stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[0], listeDecroissante).bonneReponse) || (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[0], listeMinoree).bonneReponse)) {
                stor.fctsValid.zones.bonneReponse[0] = true
                stor.fctsValid.zones.bonneReponse[1] = false
                stor.fctsValid.zones.inputs[0].value = (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[0], listeDecroissante).bonneReponse) ? listeDecroissante[0] : listeMinoree[0]
              } else {
                stor.fctsValid.zones.bonneReponse[0] = false
                if ((stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[1], listeDecroissante).bonneReponse) || (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[1], listeMinoree).bonneReponse)) {
                  stor.fctsValid.zones.bonneReponse[1] = true
                  stor.fctsValid.zones.inputs[1].value = (stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[1], listeDecroissante).bonneReponse) ? listeDecroissante[0] : listeMinoree[0]
                } else {
                  stor.fctsValid.zones.bonneReponse[1] = false
                }
              }
            // si les 2 zones de validePerso ne sont pas bonnes, on met false dans stor.fctsValid.zones.bonneReponse[i] où i est la position de la zone dans le tableau mes_zones_saisie
            }
          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          }
          stor.fctsValid.coloreUneZone(stor.lesZones[0].id)
          stor.fctsValid.coloreUneZone(stor.lesZones[1].id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          this.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
// console.log("fin du code : ",me.etat)
}
