import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pGetRandomElts, j3pDetruit, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMonome, j3pNombre, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { testIntervalleFermeDecimaux } from 'src/lib/utils/regexp'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexOppose, j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import { arithGeo, arithmetique, geometrique } from 'src/legacy/sections/lycee/suites/constantesSuites'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// on réexporte la fct d’upgrade avec arithGeo
export { upgradeParametresAg as upgradeParametres } from './constantesSuites'

/**
 * Dans cette section on demande la valeur du terme d’une suite arithmétique, géométrique ou arithmético-géométrique pour laquelle on donne le terme général
 * @author DENIAUD Rémi
 * @since Juin 2017
 * @module
 */

// la valeur par défaut dépend du niveau
const aTSdefault = '[-1.1;2.1]'
const aTESdefault = '[0.5;1.7]'

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeSuite', arithGeo, 'liste', 'La suite peut être arithmético-géométrique (par défaut et on passe alors par une suite intermédiaire), géométrique ou arithmétique.', [arithGeo, arithmetique, geometrique]],
    ['a', aTSdefault, 'string', `Intervalle où se trouve la valeur a dans l’expression u_{n+1}=au_n+b (bien sûr a non nul et avec ce type d’intervalle, il peut être décimal). Par défaut c’est "${aTESdefault}" pour le niveau TES. S’il vaut 1, (u_n) est arithmétique. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.`],
    ['b', '[-0.5;1]', 'string', 'Intervalle où se trouve la valeur b dans l’expression u_{n+1}=au_n+b (avec ce type d’intervalle, il peut être décimal). S’il vaut 0, (u_n) est géométique. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.'],
    ['termeInitial', ['0|[0.4;5]', '0|[-4;-1]', '1|[1;6]'], 'array', 'Tableau de taille nbrepetitions où chaque terme contient deux éléments séparés par | : le premier est l’indice du premier terme et le second sa valeur (imposée ou aléatoire dans un intervalle).'],
    ['niveau', 'TS', 'string', 'TS par défaut mais on peut mettre "TES" pour que la raison de la suite géométrique soit positive.'],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la suite étudiée est celle du nœud précédent.']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo1: 'Calcul d’un terme d’une suite arithmético-géométrique',
  titre_exo2: 'Calcul d’un terme d’une suite arithmétique',
  titre_exo3: 'Calcul d’un terme d’une suite géométrique',
  // on donne les phrases de la consigne
  consigne1: 'Soit $(£u_n)$ la suite définie par $£u_{£n}=£i$ et, pour tout entier $n\\geq £n$, par $£u_{n+1}=£j$.',
  consigne2_1: '$(£u_n)$ est une suite géométrique de raison $£r$ et de premier terme $£u_{£n}=£i$.',
  consigne2_2: '$(£u_n)$ est une suite arithmétique de raison $£r$ et de premier terme $£u_{£n}=£i$.',
  consigne2_3: 'La suite $(£v_n)$ est définie, pour tout entier $n\\geq £n$, par $£v_n=£u_n£c$.',
  consigne3: '$(£v_n)$ est géométrique de raison $£r$ et de premier terme $£v_{£n}=£i$.',
  consigne4: 'Pour tout entier $n\\geq £n$, $£u_n=£t$.',
  consigne5_1: 'Calculer le terme $£u_{£n}$ (on donnera la valeur exacte).',
  consigne5_2: 'Calculer le terme $£u_{£n}$ (on donnera la valeur arrondie à $10^{-£a}$).',
  consigne5_3: 'Calculer le terme $£u_{£n}$ (on donnera la valeur arrondie à l’unité).',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  pasExact: 'On demande la valeur exacte et non une valeur approchée !',
  pbArrondi: 'Peut-être y a-t-il un problème d’arrondi ?',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'En utilisant la formule explicite du terme général de la suite $(£u_n)$, on obtient :'
}

/**
 * section calculTerme
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let k = 1; k <= 3; k++) stor['zoneExpli' + k] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', textes.corr1,
      { u: stor.defSuite.nomU })
    if (stor.arrondi === -1) {
      j3pAffiche(stor.zoneExpli2, '', '$' + stor.defSuite.nomU + '_{' + stor.monIndice + '}=' + stor.calculDuTerme + '=' + j3pArrondi(stor.laReponse, 8) + '$')
    } else {
      j3pAffiche(stor.zoneExpli2, '', '$' + stor.defSuite.nomU + '_{' + stor.monIndice + '}=' + stor.calculDuTerme + '\\approx' + j3pArrondi(stor.laReponse, stor.arrondi) + '$')
    }
  }

  function genereAlea (int) {
    // int est un intervalle
    // mais on peut trouver des cas de figure où par faignantise, on aura utilisé cette fonction sans vérifier que int est un intervalle
    // Ce serait alors un nombre
    if (String(int).charAt(0) === '[') {
      // si les bornes sont entières, alors on génère un nombre entier aléatoire dans cet intervalle
      // si elles sont décimales, il faut que je génère un décimal de l’intervalle
      const PosPointVirgule = int.indexOf(';')
      let val1 = j3pNombre(int.substring(1, PosPointVirgule))
      let val2 = j3pNombre(int.substring(PosPointVirgule + 1, int.length - 1))
      let posPoint = -1
      if ((String(val1).indexOf('.') === -1)) {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = String(val2).length - String(val2).lastIndexOf('.') - 1
        }
      } else {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = Math.max(String(val1).length - String(val1).lastIndexOf('.') - 1, String(val2).length - String(val2).lastIndexOf('.') - 1)
        } else {
          posPoint = String(val1).length - String(val1).lastIndexOf('.') - 1
        }
      }
      if (val1 > val2) {
        // les bornes ne sont pas dans le bon ordre
        const val3 = val1
        val1 = val2
        val2 = val3
      }
      if (posPoint === -1) {
        // je n’ai que des nombres entiers
        const [minInt, maxInt] = j3pGetBornesIntervalle(int)
        return j3pGetRandomInt(minInt, maxInt)
      } else {
        // j’ai des décimaux
        const borneInf = val1 * Math.pow(10, posPoint)
        const borneSup = val2 * Math.pow(10, posPoint)
        const nbAlea = j3pGetRandomInt(String(borneInf), String(borneSup)) * Math.pow(10, -posPoint)
        return Math.round(nbAlea * Math.pow(10, 10)) / Math.pow(10, 10)
      }
    } else {
      return int
    }
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut, nbRepet, tabValInterdites) {
    // cette fonction permet de vérifier si ce qui est renseigné est bien un intervalle ou un nombre (éventuellement fractionnaire) et ce plusieurs fois (en général nbrepetitions)
    // cela permet de valider ce qui peut être saisi par un utilisateur dans le cas où on peut imposer des valeurs
    // texteIntervalle est une chaîne où les éléments peuvent être séparés par '|' et où chaque élément est un nombre, une fraction ou un intervalle
    // si c’est un intervalle, la fonction va dans un second temps générer de manière aléatoire une valeur de cet intervalle
    // il peut y avoir des valeurs interdites précisées dans tabValInterdites, même si elles peuvent être dans l’intervalle
    // la fonction renvoie un tableau de nbRepet éléments
    // chaque élément est un intervalle voire l’intervalle par défaut si ce qui est précisé n’est aps compris
    // ce peut aussi être un nombre entier, décimal ou fractionnaire
    // dans ce dernier cas, on renvoit une chaine où on retrouve le code latex et la valeur décimale (arrondie) séparés par |

    /* cette fonction renvoie un objet. Cet objet contient 3 tableaux de nbRepet éléments
        - leType[k] vaut 'nombre', 'fraction' ou 'intervalle' suivant la valeur de texteIntervalle
        - expression contient l’écriture de chaque valeur imposée ou gérée aléatoirement quand leType[k] vaut 'intervalle'
        - valeur contient la valeur décimale de ces nombres, cela ne peut être utile que lorsque leType[k] vaut 'fraction'
        */
    function lgIntervalle (intervalle) {
      // cette fonction renvoie la longueur de l’intervalle'
      // si ce n’est pas un intervalle, alors elle renvoie 0'
      let lg = 0
      // var intervalleReg = new RegExp("\\[\\-?[0-9]{1,};\\-?[0-9]{1,}\\]","i");
      if (testIntervalleFermeDecimaux.test(intervalle)) {
        // on a bien un intervalle
        const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
        const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
        lg = borneSup - borneInf
      }
      return lg
    }

    function nbDansTab (nb, tab) {
      // test si la valeur correspondant à nb est dans le tableau tab (ses valeurs pouvant être au format latex)
      if (tab === undefined) {
        return false
      } else {
        let puisPrecision = 15
        if (Math.abs(nb) > Math.pow(10, -13)) {
          const ordrePuissance = Math.floor(Math.log(Math.abs(nb)) / Math.log(10)) + 1
          puisPrecision = 15 - ordrePuissance
        }
        for (let k = 0; k < tab.length; k++) {
          if (Math.abs(nb - j3pCalculValeur(tab[k])) < Math.pow(10, -puisPrecision)) {
            return true
          }
        }
        return false
      }
    }

    const intervalleReg = testIntervalleFermeDecimaux // new RegExp('\\[\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,};\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,}\\]', 'i')
    const nbFracReg = /-?[0-9]+\/[0-9]+/ // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const DonneesIntervalle = {}
    // DonneesIntervalle contient trois propriétés qui sont des tableaux de longueur nbRepet :
    // - leType : qui correspond à ce qui a été renseigné initialement ('intervalle', 'nombre', 'fraction') - 'nombre' désigne un entier ou un décimal
    // - expression : qui est le nombre (si type==='nombre'), une valeur générée aléatoirement dans un intervalle (si type==='intervalle'), l’écriture l’atex de la fraction (si type==='fraction')
    // - valeur : qui est la valeur décimale de l’expression (utile surtout si type==='fraction')
    DonneesIntervalle.leType = []
    DonneesIntervalle.expression = []
    DonneesIntervalle.valeur = []
    let nbAlea, valNb, tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).indexOf('|') > -1) {
      const tableau = texteIntervalle.split('|')
      for (k = 0; k < nbRepet; k++) {
        if (intervalleReg.test(tableau[k])) {
          // c’est bien un intervalle'
          if (lgIntervalle(tableau[k]) === 0) {
            // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
            valNb = tableau[k].substring(1, tableau[k].indexOf(';'))
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          } else {
            do {
              nbAlea = genereAlea(tableau[k])
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        } else {
          // peut être est-ce tout simplement un nombre :
          if ((tableau[k] === undefined) || (tableau[k] === '')) {
            // là l’expression n’est pas renseignée
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          } else if (nbFracReg.test(tableau[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tableau[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
            DonneesIntervalle.leType.push('fraction')
            DonneesIntervalle.valeur.push(valFrac)
          } else if (!isNaN(j3pNombre(tableau[k]))) {
            // on a un nombre
            DonneesIntervalle.expression.push(tableau[k])
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(tableau[k])
          } else {
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      }
    } else {
      if ((texteIntervalle === '') || (texteIntervalle === undefined)) {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      } else if (intervalleReg.test(String(texteIntervalle))) {
        if (lgIntervalle(String(texteIntervalle)) === 0) {
          // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
          valNb = String(texteIntervalle).substring(1, String(texteIntervalle).indexOf(';'))
          for (k = 0; k < nbRepet; k++) {
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          }
        } else {
          for (k = 0; k < nbRepet; k++) {
            do {
              nbAlea = genereAlea(texteIntervalle)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
          DonneesIntervalle.leType.push('fraction')
          DonneesIntervalle.valeur.push(valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push(String(texteIntervalle))
          DonneesIntervalle.leType.push('nombre')
          DonneesIntervalle.valeur.push(String(texteIntervalle))
        }
      } else {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      }
    }
    return DonneesIntervalle
  }

  function ConvertDecimal (nb, nbreDecimales) {
    // nb peut être un entier, un décimal ou un nb fractionnaire
    // si c’est un nb fractionnaire et que la forme décimale contient moins de nbreDecimales, alors on renvoie le nb sous forme décimale
    // sinon on renvoit nb sous son format initial
    const nbDecimal = j3pCalculValeur(nb)
    // pour éviter les pbs d’arrondi, je cherche par quelle puissance de 10 multiplier mon nombre (puis el divisier)
    let puisPrecision = 15
    if (Math.abs(nbDecimal) > Math.pow(10, -13)) {
      const ordrePuissance = Math.floor(Math.log(Math.abs(nbDecimal)) / Math.log(10)) + 1
      puisPrecision = 15 - ordrePuissance
    }
    let newNb = nb
    if (Math.abs(Math.round(nbDecimal * Math.pow(10, puisPrecision)) / Math.pow(10, puisPrecision) - nbDecimal) < Math.pow(10, -12)) {
      // on a une valeur décimale
      // Si elle a plus de deux chiffres après la virgule, on donne une valeur décimale
      const nbChiffreApresVirgule = String(nbDecimal).length - 1 - String(nbDecimal).lastIndexOf('.')
      if (nbChiffreApresVirgule <= nbreDecimales) {
        newNb = nbDecimal
      }
    }
    return newNb
  }

  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation1')

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    stor.defSuite = {}
    if (ds.donneesPrecedentes) {
      // on récupère les données du noeud précédent stockées dans me.donneesPersistantes.suites
      if (!me.donneesPersistantes.suites) {
        ds.donneesPrecedentes = false
        console.error(Error('On a donneesPrecedentes à true mais sans donneesPersistantes.suites'), me.donneesPersistantes, ds.donneesPrecedentes)
        j3pShowError(Error('Les données précédentes n’ont pu être récupérées. De nouvelles sont générées !\nAttention car la nature de la suite n’est peut-être pas celle qui était attendue.'))
      }
    }

    if (ds.donneesPrecedentes) {
      // si on récupère des données précédentes il faut imposer une seule répétition (on veut bosser sur la suite précédente, aucune autre générée aléatoirement)
      if (ds.nbrepetitions !== 1) {
        console.error('Pb de paramétrage, imposer donneesPrecedentes avec plusieurs répétitions n’a pas de sens')
        me.surcharge({ nbrepetitions: 1 })
      }

      // on récupère tout ce que ça contient pour le mettre dans stor
      stor.defSuite = { ...me.donneesPersistantes.suites }
      stor.defSuite.typeSuite = ([arithmetique, geometrique].includes(stor.defSuite.typeSuite))
        ? stor.defSuite.typeSuite
        : arithGeo
      stor.defSuite.coefA = [stor.defSuite.a]
      stor.defSuite.coefB = [stor.defSuite.b]
      stor.defSuite.termeInit[0][1] = stor.defSuite.u0
      stor.defSuite.termeInit[0][0] = Number(stor.defSuite.termeInit[0][0])
      if (!stor.defSuite.niveau) stor.defSuite.niveau = 'TS'
    } else {
      stor.defSuite.niveau = (ds.niveau.toUpperCase() === 'TES') ? 'TES' : 'TS'
      if (stor.defSuite.niveau === 'TES') {
        if (ds.a === aTSdefault) {
          // c’est qu’on a laissé a par défaut. Donc il faut que je lui mette la valeur par défaut du niveau TES
          ds.a = aTESdefault
        }
      }
      if (ds.typeSuite === arithmetique) {
        stor.defSuite.typeSuite = arithmetique
      } else if (ds.typeSuite === geometrique) {
        stor.defSuite.typeSuite = geometrique
      } else {
        stor.defSuite.typeSuite = arithGeo
      }
    }
    const leTitreExo = (stor.defSuite.typeSuite === arithGeo)
      ? textes.titre_exo1
      : (stor.defSuite.typeSuite === arithmetique)
          ? textes.titre_exo2
          : textes.titre_exo3
    me.afficheTitre(leTitreExo)
    let onRecommence, k
    if (!ds.donneesPrecedentes) {
      // on génère de manière aléatoire les variables et données de l’exercice
      const intervalleDefaut = (stor.defSuite.niveau === 'TES') ? aTESdefault : aTSdefault
      stor.coefADonnees = constructionTabIntervalle(ds.a, intervalleDefaut, ds.nbrepetitions, [0, 1, -1])
      stor.coefBDonnees = constructionTabIntervalle(ds.b, '[-0.5;1]', ds.nbrepetitions, (stor.defSuite.typeSuite === arithmetique) ? [0, 1] : [0])
      stor.defSuite.coefA = stor.coefADonnees.expression
      stor.defSuite.coefB = stor.coefBDonnees.expression

      // il faut que je gère le terme initial car j’ai un tableau dont les données sont de la forme 1|[...;...] ce second point pouvant être un entier ou un nb fractionnaire
      stor.defSuite.termeInit = []
      const TermeInitParDefaut = [0, '[0.4;5]']
      let termeU0, termeC, termeV0
      if (!isNaN(j3pNombre(String(ds.termeInitial)))) {
        // au lieu d’un tableau, n’est indiqué qu’un seul nombre. Celui-ci devient le rang initial de chaque répétition
        const leRangInit = j3pNombre(ds.termeInitial)
        for (k = 0; k < ds.nbrepetitions; k++) {
          onRecommence = false
          do {
            termeU0 = genereAlea(TermeInitParDefaut[1])
            if (stor.defSuite.typeSuite === arithGeo) {
              // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
              termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
              termeV0 = j3pGetLatexSomme(termeU0, termeC)
              onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
            }
          } while (onRecommence)
          stor.defSuite.termeInit.push([leRangInit, termeU0])
        }
      }
      for (k = 0; k < ds.nbrepetitions; k++) {
        if ((ds.termeInitial[k] === undefined) || (ds.termeInitial[k] === '')) {
          // on a rien renseigné donc par défaut, on met
          onRecommence = false
          do {
            termeU0 = genereAlea(TermeInitParDefaut[1])
            if (stor.defSuite.typeSuite === arithGeo) {
              // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
              termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
              termeV0 = j3pGetLatexSomme(termeU0, termeC)
              onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
            }
          } while (onRecommence)
          stor.defSuite.termeInit.push([TermeInitParDefaut[0], termeU0])
        } else {
          // il faut la barre verticale pour séparer le rang et la valeur du terme initial
          const posBarre = String(ds.termeInitial[k]).indexOf('|')
          if (posBarre === -1) {
            const EntierReg = /\d+/ // new RegExp('[0-9]{1,}', 'i')
            // je n’ai pas la barre verticale
            // peut-être ai-je seulement l’indice du premier terme
            if (EntierReg.test(ds.termeInitial[k])) {
              // c’est un entier correspondant à l’indice du terme initial
              onRecommence = false
              do {
                termeU0 = genereAlea(TermeInitParDefaut[1])
                if (stor.defSuite.typeSuite === arithGeo) {
                  // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                  termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                  termeV0 = j3pGetLatexSomme(termeU0, termeC)
                  onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
                }
              } while (onRecommence)
              stor.defSuite.termeInit.push([ds.termeInitial[k], termeU0])
            } else {
              // donc je réinitialise avec la valeur par défaut
              onRecommence = false
              do {
                termeU0 = genereAlea(TermeInitParDefaut[1])
                if (stor.defSuite.typeSuite === arithGeo) {
                  // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                  termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                  termeV0 = j3pGetLatexSomme(termeU0, termeC)
                  onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
                }
              } while (onRecommence)
              stor.defSuite.termeInit.push([TermeInitParDefaut[0], termeU0])
            }
          } else {
            // la valeur (éventuellement aléatoire) du terme initial
            onRecommence = false
            let termInitAlea
            do {
              termInitAlea = constructionTabIntervalle(ds.termeInitial[k].substring(posBarre + 1), TermeInitParDefaut[1], 1).expression[0]
              if (stor.defSuite.typeSuite === arithGeo) {
                // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                termeV0 = j3pGetLatexSomme(termInitAlea, termeC)
                onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
              }
            } while (onRecommence)
            // au cas où je vérifie que la première valeur est bien entière
            let rangInit = ds.termeInitial[k].substring(0, posBarre)
            rangInit = (String(j3pNombre(rangInit)).indexOf('.') === -1) ? rangInit : TermeInitParDefaut[0]
            stor.defSuite.termeInit.push([rangInit, termInitAlea])
          }
        }
      }
    }
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (!ds.donneesPrecedentes) {
      // si je n’ai pas de section précédente, je génère le nom de la suite (u_n) de manière aléatoire
      stor.defSuite.nomU = j3pGetRandomElts(['u', 'v'], 1)[0]
      do {
        stor.defSuite.nomV = j3pGetRandomElts(['t', 'x', 'y', 'z'], 1)[0]
      } while (stor.defSuite.nomU === stor.defSuite.nomV)// ce dernier cas peut se produire si on a une section précédente
    }// je mets mes valeurs dans une variable plus simple :
    stor.defSuite.a = stor.defSuite.coefA[me.questionCourante - 1]
    stor.defSuite.b = stor.defSuite.coefB[me.questionCourante - 1]
    // on exporte les variables présentes dans cette section pour qu’elles soient utilisées dans natureSuite
    // me.donneesPersistantes.suites sert à envoyer des informations dans le bilan qui pourront être récupérées par une autre section sur les suites
    // Ceci a pour but de poursuivre l’étude de la suite définie dans cette section
    me.donneesPersistantes.suites = { ...stor.defSuite }
    const relRecurrence = (stor.defSuite.typeSuite === arithmetique)
      ? stor.defSuite.nomU + '_n' + j3pGetLatexMonome(2, 0, stor.defSuite.b, '')
      : (stor.defSuite.typeSuite === geometrique)
          ? j3pGetLatexMonome(1, 1, stor.defSuite.a, stor.defSuite.nomU + '_n')
          : j3pGetLatexMonome(1, 1, stor.defSuite.a, stor.defSuite.nomU + '_n') + j3pGetLatexMonome(2, 0, stor.defSuite.b, '')
    // J’ai eu une reprise de graphe où stor.defSuite.termeInit[me.questionCourante - 1][1] était undefined d’où un plantage
    // Là c’est une rustine mais qui ne devrait pas se produire
    if (!stor.defSuite.termeInit[me.questionCourante - 1][1]) stor.defSuite.termeInit[me.questionCourante - 1][1] = 1

    j3pAffiche(stor.zoneCons1, '', textes.consigne1,
      {
        u: stor.defSuite.nomU,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        i: stor.defSuite.termeInit[me.questionCourante - 1][1],
        j: relRecurrence
      })
    let affichageRaison
    if (stor.defSuite.typeSuite !== arithmetique) {
      affichageRaison = (String(stor.defSuite.a).indexOf('frac') > -1)
        ? '\\left(' + stor.defSuite.a + '\\right)'
        : (j3pNombre(stor.defSuite.a) < 0)
            ? '\\left(' + stor.defSuite.a + '\\right)'
            : stor.defSuite.a
    }
    let numIdSuivant = 3
    let vn
    if (stor.defSuite.typeSuite === arithmetique) {
      j3pAffiche(stor.zoneCons2, '', textes.consigne2_2,
        {
          u: stor.defSuite.nomU,
          r: stor.defSuite.b,
          n: stor.defSuite.termeInit[me.questionCourante - 1][0],
          i: stor.defSuite.termeInit[me.questionCourante - 1][1]
        })
      vn = (Math.abs(j3pCalculValeur(stor.defSuite.u0)) < Math.pow(10, -12))
        ? j3pGetLatexMonome(1, 1, stor.defSuite.b, 'n') + j3pGetLatexMonome(2, 0, j3pGetLatexOppose(j3pGetLatexProduit(stor.defSuite.termeInit[me.questionCourante - 1][0], stor.defSuite.b)))
        : j3pGetLatexMonome(1, 1, stor.defSuite.b, 'n') + j3pGetLatexMonome(2, 0, j3pGetLatexSomme(stor.defSuite.termeInit[me.questionCourante - 1][1], j3pGetLatexOppose(j3pGetLatexProduit(stor.defSuite.termeInit[me.questionCourante - 1][0], stor.defSuite.b))), '')
    } else if (stor.defSuite.typeSuite === geometrique) {
      j3pAffiche(stor.zoneCons2, '', textes.consigne2_1,
        {
          u: stor.defSuite.nomU,
          r: stor.defSuite.a,
          n: stor.defSuite.termeInit[me.questionCourante - 1][0],
          i: stor.defSuite.termeInit[me.questionCourante - 1][1]
        })
      stor.defSuite.v0 = stor.defSuite.termeInit[me.questionCourante - 1][1]
      vn = (Math.abs(j3pCalculValeur(stor.defSuite.termeInit[me.questionCourante - 1][1]) - 1) < Math.pow(10, -12))
        ? affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
        : (Math.abs(j3pCalculValeur(stor.defSuite.termeInit[me.questionCourante - 1][1]) + 1) < Math.pow(10, -12))
            ? '-' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
            : stor.defSuite.termeInit[me.questionCourante - 1][1] + '\\times' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
    } else {
      // suite arithmetico-geometrique
      // je prends la suite intermédiaire v_n=u_n-b/(1-a). Je note c=-b/(1-a)
      stor.defSuite.c = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.b), j3pGetLatexSomme('1', j3pGetLatexOppose((stor.defSuite.a))))
      // si c est une valeur décimale simple, alors je l’affiche sous forme décimale
      stor.defSuite.c = ConvertDecimal(stor.defSuite.c, 2)
      j3pAffiche(stor.zoneCons2, '', textes.consigne2_3,
        {
          u: stor.defSuite.nomU,
          v: stor.defSuite.nomV,
          r: stor.defSuite.a,
          n: stor.defSuite.termeInit[me.questionCourante - 1][0],
          c: j3pGetLatexMonome(2, 0, stor.defSuite.c, '')
        })
      stor.defSuite.v0 = j3pGetLatexSomme(stor.defSuite.termeInit[me.questionCourante - 1][1], stor.defSuite.c)
      j3pAffiche(stor.zoneCons3, '', textes.consigne3,
        {
          v: stor.defSuite.nomV,
          r: stor.defSuite.a,
          n: stor.defSuite.termeInit[me.questionCourante - 1][0],
          i: stor.defSuite.v0
        })
      vn = (Math.abs(j3pCalculValeur(stor.defSuite.v0) - 1) < Math.pow(10, -12))
        ? affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
        : (Math.abs(j3pCalculValeur(stor.defSuite.v0) + 1) < Math.pow(10, -12))
            ? '-' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
            : stor.defSuite.v0 + '\\times' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
      vn = vn + j3pGetLatexMonome(2, 0, j3pGetLatexOppose(stor.defSuite.c), '')
      numIdSuivant = 4
    }
    j3pAffiche(stor['zoneCons' + numIdSuivant], '', textes.consigne4,
      {
        u: stor.defSuite.nomU,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        t: vn
      })
    numIdSuivant++
    // choix de l’indice du terme à calculer
    let laReponse
    let laReponseInter
    let nbPassage = 0
    let souciParametre = false
    // la variable laReponseInter me sert dans le cas des suites arithmetico-géométriques
    // JE ne veux pas que laReponseInter soit nulle à 10^{-3}
    // nbPassage va me servir pour baisser monIndice si vraiment je n’arrive pas à avoir les contraintes imposées
    // souciParametre prendra la valeur true si les contraintes ne sont pas satisfaites (utilisation dans la boucle)
    do {
      souciParametre = false
      nbPassage++
      if (nbPassage > 100) {
        stor.monIndice = j3pGetRandomInt(3, 4)
      } else if (nbPassage > 80) {
        stor.monIndice = j3pGetRandomInt(5, 6)
      } else if (nbPassage > 50) {
        stor.monIndice = j3pGetRandomInt(7, 10)
      } else {
        stor.monIndice = (stor.defSuite.typeSuite === arithmetique)
          ? j3pGetRandomInt(15, 40)
          : (stor.defSuite.typeSuite === geometrique)
              ? j3pGetRandomInt(11, 25)
              : j3pGetRandomInt(15, 25)
      }
      if (stor.defSuite.typeSuite === arithmetique) {
        // là je fais en sorte que l’indice ne soit pas trop grand si la raison est proche de 0
        laReponseInter = laReponse = stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b * (stor.monIndice - stor.defSuite.termeInit[me.questionCourante - 1][0])
      } else if (stor.defSuite.typeSuite === geometrique) {
        laReponseInter = laReponse = stor.defSuite.termeInit[me.questionCourante - 1][1] * Math.pow(stor.defSuite.a, stor.monIndice - stor.defSuite.termeInit[me.questionCourante - 1][0])
      } else {
        laReponse = (stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b / (stor.defSuite.a - 1)) * Math.pow(stor.defSuite.a, stor.monIndice - stor.defSuite.termeInit[me.questionCourante - 1][0]) - stor.defSuite.b / (stor.defSuite.a - 1)
        laReponseInter = (stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b / (stor.defSuite.a - 1)) * Math.pow(stor.defSuite.a, stor.monIndice - stor.defSuite.termeInit[me.questionCourante - 1][0])
      }
      if (String(laReponseInter).includes('e')) {
        // on a un nombre de la forme a*10^b donc je n’en veux pas
        souciParametre = true
      }
      // il faut que je fasse attention que la réponse ne soit pas un nombre trop grand
      if ((Math.abs(laReponseInter) > 150000) || (Math.abs(laReponseInter) < Math.pow(10, -4))) {
        // on a un nombre trop grand en valeur absolue ou trop proche de zéro
        souciParametre = true
      }
      if (!souciParametre) {
        // il faut dans ce cas que je gère l’arrondi
        if (Math.abs(laReponseInter * Math.pow(10, 4) - Math.round(laReponseInter * Math.pow(10, 4))) < Math.pow(10, -6)) {
          stor.arrondi = -1
        } else if (Math.abs(laReponseInter) > 1000) {
          stor.arrondi = 0
        } else if (Math.abs(laReponseInter) > 10) {
          stor.arrondi = 1
        } else if (Math.abs(laReponseInter) > 1) {
          stor.arrondi = 2
        } else if (Math.abs(laReponseInter) > 0.01) {
          stor.arrondi = 3
        } else {
          stor.arrondi = 4
        }
      }
    } while (souciParametre)
    stor.laReponse = laReponse
    if (stor.defSuite.typeSuite === arithmetique) {
      stor.calculDuTerme = (Math.abs(j3pCalculValeur(stor.defSuite.u0)) < Math.pow(10, -12))
        ? stor.defSuite.b + '\\times ' + stor.monIndice
        : stor.defSuite.b + '\\times ' + stor.monIndice + j3pGetLatexMonome(2, 0, j3pGetLatexSomme(stor.defSuite.termeInit[me.questionCourante - 1][1], j3pGetLatexOppose(j3pGetLatexProduit(stor.defSuite.termeInit[me.questionCourante - 1][0], stor.defSuite.b))), '')
    } else if (stor.defSuite.typeSuite === geometrique) {
      stor.calculDuTerme = (Math.abs(j3pCalculValeur(stor.defSuite.termeInit[me.questionCourante - 1][1]) - 1) < Math.pow(10, -12))
        ? affichageRaison + '^{' + stor.monIndice + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
        : (Math.abs(j3pCalculValeur(stor.defSuite.termeInit[me.questionCourante - 1][1]) + 1) < Math.pow(10, -12))
            ? '-' + affichageRaison + '^{' + stor.monIndice + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
            : stor.defSuite.termeInit[me.questionCourante - 1][1] + '\\times' + affichageRaison + '^{' + stor.monIndice + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
    } else {
      stor.calculDuTerme = (Math.abs(j3pCalculValeur(stor.defSuite.v0) - 1) < Math.pow(10, -12))
        ? affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
        : (Math.abs(j3pCalculValeur(stor.defSuite.v0) + 1) < Math.pow(10, -12))
            ? '-' + affichageRaison + '^{' + stor.monIndice + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
            : stor.defSuite.v0 + '\\times' + affichageRaison + '^{' + stor.monIndice + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
      stor.calculDuTerme += j3pGetLatexMonome(2, 0, j3pGetLatexOppose(stor.defSuite.c), '')
    }

    if (stor.arrondi === -1) {
      j3pAffiche(stor['zoneCons' + numIdSuivant], '', textes.consigne5_1,
        { u: stor.defSuite.nomU, n: stor.monIndice })
    } else if (stor.arrondi === 0) {
      j3pAffiche(stor['zoneCons' + numIdSuivant], '', textes.consigne5_3,
        { u: stor.defSuite.nomU, n: stor.monIndice })
    } else {
      j3pAffiche(stor['zoneCons' + numIdSuivant], '', textes.consigne5_2,
        {
          u: stor.defSuite.nomU,
          n: stor.monIndice,
          a: stor.arrondi
        })
    }
    numIdSuivant++
    if (stor.arrondi === -1) {
      stor.elt = j3pAffiche(stor['zoneCons' + numIdSuivant], '', '$' + stor.defSuite.nomU + '_{' + stor.monIndice + '}=$&1&.',
        { inputmq1: { texte: '' } })
    } else {
      stor.elt = j3pAffiche(stor['zoneCons' + numIdSuivant], '', '$' + stor.defSuite.nomU + '_{' + stor.monIndice + '}\\approx$&1&.',
        { inputmq1: { texte: '' } })
    }
    stor.elt.inputmqList[0].typeReponse = (stor.arrondi === -1) ? ['nombre', 'exact'] : ['nombre', 'arrondi', Math.pow(10, -stor.arrondi)]
    stor.elt.inputmqList[0].reponse = (stor.defSuite.typeSuite === geometrique)
      ? [stor.defSuite.termeInit[me.questionCourante - 1][1] * Math.pow(stor.defSuite.a, stor.monIndice - stor.defSuite.termeInit[me.questionCourante - 1][0])]
      : (stor.defSuite.typeSuite === arithmetique)
          ? [stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b * (stor.monIndice - stor.defSuite.termeInit[me.questionCourante - 1][0])]
          : [(stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b / (stor.defSuite.a - 1)) * Math.pow(stor.defSuite.a, stor.monIndice - stor.defSuite.termeInit[me.questionCourante - 1][0]) - stor.defSuite.b / (stor.defSuite.a - 1)]
    numIdSuivant++
    mqRestriction(stor.elt.inputmqList[0], '\\d.,-')
    me.logIfDebug('réponse : ', stor.elt.inputmqList[0].reponse[0])
    const mesZonesSaisie = [stor.elt.inputmqList[0].id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    const conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(conteneurD, 'div')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(conteneurD, 'div')
    j3pStyle(stor.zoneCorr, { paddingTop: '10px' })

    j3pFocus(stor.elt.inputmqList[0])
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        let reponse = {
          aRepondu: false, bonneReponse: false
        }
        reponse = fctsValid.validationGlobale()

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (stor.arrondi > 0) {
                if (Math.abs(j3pNombre(fctsValid.zones.reponseSaisie[0]) - stor.laReponse) < Math.pow(10, -stor.arrondi + 1)) {
                // c’est que l’élève a fait le bon calcul mais qu’il a donné une valeur approchée et non un arrondi
                  stor.zoneCorr.innerHTML += '<br>' + textes.pbArrondi
                }
              } else if (stor.arrondi === -1) {
                if (Math.abs(j3pNombre(fctsValid.zones.reponseSaisie[0]) - stor.laReponse) < Math.pow(10, -2)) {
                // c’est que l’élève a fait le bon calcul mais qu’il a donné une valeur approchée et non un arrondi
                  stor.zoneCorr.innerHTML += '<br>' + textes.pasExact
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
