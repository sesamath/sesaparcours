import { j3pAddElt, j3pArrondi, j3pGetRandomElts, j3pFocus, j3pGetRandomInt, j3pNombre, j3pShowError, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { testIntervalleFermeDecimaux } from 'src/lib/utils/regexp'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexOppose, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// y’a pas de typeSuite, seulement donneesPrecedentes
export { upgradeParametreDonneesPrecedentes as upgradeParametres } from './constantesSuites'

/*
        DENIAUD Rémi
        décembre 2019
        Dans cette section, on part d’une suite arithmético-géométrique et on cherche un seuil : rang à partir duquel on dépasse telle valeur
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[2;7]|[2;7]|[0.2;0.9]', 'string', 'Intervalle où se trouve la valeur a dans l’expression u_{n+1}=au_n+b (bien sûr a non nul différent de 1). En imposant "[-1;1]", on aura un décimal de cet intervalle (bornes non acceptées). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[0.2;0.9]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis décimale de l’intervalle [0.2;0.9]. A la place d’un intervalle, on peut aussi mettre un nombre entier ou décimal.'],
    ['b', '[-6;6]', 'string', 'Intervalle où se trouve la valeur b dans l’expression u_{n+1}=au_n+b (b non nul). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier ou décimal.'],
    ['termeInitial', ['0|[-5;5]', '0|[-5;5]', '0|[-5;5]'], 'array', 'Tableau de taille nbrepetitions où chaque terme contient deux éléments séparés par | : le premier est l’indice du premier terme et le second sa valeur (imposée ou aléatoire dans un intervalle).'],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la suite étudiée est celle du nœud précédent.']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Déterminer un seuil dans le cas d’une suite',
  // on donne les phrases de la consigne
  consigne1: 'Soit $(£u_n)$ la suite définie par $£u_{£n}=£i$ et, pour tout entier $n\\geq £n$, par $£u_{n+1}=£j$.',
  consigne2_1: 'On a conjecturé que la suite $(£u_n)$ converge vers $£l$.',
  consigne2_2: 'On a conjecturé que la suite $(£u_n)$ diverge vers $£l$.',
  consigne3: 'À l’aide de la calculatrice, le premier rang $n$ à partir duquel $£{cond}$ est &1&.',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'À l’aide du mode suite de la calculatrice, on peut calculer les termes successifs de la suite $(£u_n)$.',
  corr2: 'Or $£u_{£{n1}}£{s1}£{u1}$ ; $£u_{£{n2}}£{s2}£{u2}$ et $£u_{£{n3}}£{s3}£{u3}$.',
  corr3: 'Donc à partir du rang £r, $£{cond}$.'
}

/**
 * section determinerSeuil
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let k = 1; k <= 3; k++) stor['zoneExpli' + k] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', textes.corr1,
      {
        u: stor.defSuite.nomU
      })
    const objCorr = {}
    objCorr.styletexte = {}
    objCorr.u = stor.defSuite.nomU
    for (let i = 1; i <= 3; i++) {
      objCorr['n' + i] = stor.listeUn.length - 4 + i
      objCorr['s' + i] = (j3pArrondi(stor.listeUn[stor.listeUn.length - 4 + i], 8) === stor.listeUn[stor.listeUn.length - 4 + i]) ? '\\approx ' : '='
      objCorr['u' + i] = j3pArrondi(stor.listeUn[stor.listeUn.length - 4 + i], 8)
    }
    j3pAffiche(stor.zoneExpli2, '', textes.corr2, objCorr)
    j3pAffiche(stor.zoneExpli3, '', textes.corr3, {
      cond: stor.condSeuil,
      r: stor.listeUn.length - 1
    })
  }

  function genereAlea (int) {
    // int est un intervalle
    // mais on peut trouver des cas de figure où par faignantise, on aura utilisé cette fonction sans vérifier que int est un intervalle
    // Ce serait alors un nombre
    if (String(int).charAt(0) === '[') {
      // si les bornes sont entières, alors on génère un nombre entier aléatoire dans cet intervalle
      // si elles sont décimales, il faut que je génère un décimal de l’intervalle
      const PosPointVirgule = int.indexOf(';')
      let val1 = j3pNombre(int.substring(1, PosPointVirgule))
      let val2 = j3pNombre(int.substring(PosPointVirgule + 1, int.length - 1))
      let posPoint = -1
      if ((String(val1).indexOf('.') === -1)) {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = String(val2).length - String(val2).lastIndexOf('.') - 1
        }
      } else {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = Math.max(String(val1).length - String(val1).lastIndexOf('.') - 1, String(val2).length - String(val2).lastIndexOf('.') - 1)
        } else {
          posPoint = String(val1).length - String(val1).lastIndexOf('.') - 1
        }
      }
      if (val1 > val2) {
        // les bornes ne sont pas dans le bon ordre
        const val3 = val1
        val1 = val2
        val2 = val3
      }
      if (posPoint === -1) {
        // je n’ai que des nombres entiers
        return j3pGetRandomInt(val1, val2)
      } else {
        // j’ai des décimaux
        const borneInf = val1 * Math.pow(10, posPoint)
        const borneSup = val2 * Math.pow(10, posPoint)
        const nbAlea = j3pGetRandomInt(String(borneInf), String(borneSup)) * Math.pow(10, -posPoint)
        return j3pArrondi(nbAlea, 10)
      }
    } else {
      return int
    }
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut, nbRepet, tabValInterdites) {
    // cette fonction permet de vérifier si ce qui est renseigné est bien un intervalle ou un nombre (éventuellement fractionnaire) et ce plusieurs fois (en général nbrepetitions)
    // cela permet de valider ce qui peut être saisi par un utilisateur dans le cas où on peut imposer des valeurs
    // texteIntervalle est une chaîne où les éléments peuvent être séparés par '|' et où chaque élément est un nombre, une fraction ou un intervalle
    // si c’est un intervalle, la fonction va dans un second temps générer de manière aléatoire une valeur de cet intervalle
    // il peut y avoir des valeurs interdites précisées dans tabValInterdites, même si elles peuvent être dans l’intervalle
    // la fonction renvoie un tableau de nbRepet éléments
    // chaque élément est un intervalle voire l’intervalle par défaut si ce qui est précisé n’est aps compris
    // ce peut aussi être un nombre entier, décimal ou fractionnaire
    // dans ce dernier cas, on renvoit une chaine où on retrouve le code latex et la valeur décimale (arrondie) séparés par |

    /* cette fonction renvoie un objet. Cet objet contient 3 tableaux de nbRepet éléments
    - leType[k] vaut 'nombre', 'fraction' ou 'intervalle' suivant la valeur de texteIntervalle
    - expression contient l’écriture de chaque valeur imposée ou gérée aléatoirement quand leType[k] vaut 'intervalle'
    - valeur contient la valeur décimale de ces nombres, cela ne peut être utile que lorsque leType[k] vaut 'fraction'
    */
    function lgIntervalle (intervalle) {
      // cette fonction renvoie la longueur de l’intervalle'
      // si ce n’est pas un intervalle, alors elle renvoie 0'
      let lg = 0
      if (testIntervalleFermeDecimaux.test(intervalle)) {
        // on a bien un intervalle
        const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
        const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
        lg = borneSup - borneInf
      }
      return lg
    }

    function nbDansTab (nb, tab) {
      // test si la valeur correspondant à nb est dans le tableau tab (ses valeurs pouvant être au format latex)
      if (tab === undefined) {
        return false
      } else {
        let puisPrecision = 15
        if (Math.abs(nb) > Math.pow(10, -13)) {
          const ordrePuissance = String(Math.abs(nb)).indexOf('.')
          puisPrecision = (ordrePuissance === -1) ? 15 - String(Math.abs(nb)).length : 15 - ordrePuissance
        }
        for (let k = 0; k < tab.length; k++) {
          if (Math.abs(nb - j3pCalculValeur(tab[k])) < Math.pow(10, -puisPrecision)) {
            return true
          }
        }
        return false
      }
    }

    const nbFracReg = /-?[0-9]+\/[0-9]+/ // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const DonneesIntervalle = {}
    // DonneesIntervalle contient trois propriétés qui sont des tableaux de longueur nbRepet :
    // - leType : qui correspond à ce qui a été renseigné initialement ('intervalle', 'nombre', 'fraction') - 'nombre' désigne un entier ou un décimal
    // - expression : qui est le nombre (si type==='nombre'), une valeur générée aléatoirement dans un intervalle (si type==='intervalle'), l’écriture l’atex de la fraction (si type==='fraction')
    // - valeur : qui est la valeur décimale de l’expression (utile surtout si type==='fraction')
    DonneesIntervalle.leType = []
    DonneesIntervalle.expression = []
    DonneesIntervalle.valeur = []
    let nbAlea, valNb, tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).indexOf('|') > -1) {
      const tableau = texteIntervalle.split('|')
      for (k = 0; k < nbRepet; k++) {
        if (testIntervalleFermeDecimaux.test(tableau[k])) {
          // c’est bien un intervalle'
          if (lgIntervalle(tableau[k]) === 0) {
            // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
            valNb = tableau[k].substring(1, tableau[k].indexOf(';'))
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          } else {
            do {
              nbAlea = genereAlea(tableau[k])
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        } else {
          // peut être est-ce tout simplement un nombre :
          if ((tableau[k] === undefined) || (tableau[k] === '')) {
            // là l’expression n’est pas renseignée
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          } else if (nbFracReg.test(tableau[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tableau[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
            DonneesIntervalle.leType.push('fraction')
            DonneesIntervalle.valeur.push(valFrac)
          } else if (!isNaN(j3pNombre(tableau[k]))) {
            // on a un nombre
            DonneesIntervalle.expression.push(tableau[k])
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(tableau[k])
          } else {
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      }
    } else {
      if ((texteIntervalle === undefined) || (texteIntervalle === '')) {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      } else if (testIntervalleFermeDecimaux.test(String(texteIntervalle))) {
        if (lgIntervalle(String(texteIntervalle)) === 0) {
          // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
          valNb = String(texteIntervalle).substring(1, String(texteIntervalle).indexOf(';'))
          for (k = 0; k < nbRepet; k++) {
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          }
        } else {
          for (k = 0; k < nbRepet; k++) {
            do {
              nbAlea = genereAlea(texteIntervalle)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
          DonneesIntervalle.leType.push('fraction')
          DonneesIntervalle.valeur.push(valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push(String(texteIntervalle))
          DonneesIntervalle.leType.push('nombre')
          DonneesIntervalle.valeur.push(String(texteIntervalle))
        }
      } else {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      }
    }
    return DonneesIntervalle
  }

  function ConvertDecimal (nb, nbreDecimales) {
    // nb peut être un entier, un décimal ou un nb fractionnaire
    // si c’est un nb fractionnaire et que la forme décimale contient moins de nbreDecimales, alors on renvoie le nb sous forme décimale
    // sinon on renvoit nb sous son format initial
    const nbDecimal = j3pCalculValeur(nb)
    // pour éviter les pbs d’arrondi, je cherche par quelle puissance de 10 multiplier mon nombre (puis el divisier)
    let puisPrecision = 15
    if (Math.abs(nbDecimal) > Math.pow(10, -13)) {
      const ordrePuissance = String(Math.abs(nbDecimal)).indexOf('.')
      puisPrecision = (ordrePuissance === -1) ? 15 - String(Math.abs(nbDecimal)).length : 15 - ordrePuissance
    }
    let newNb = nb
    if (Math.abs(j3pArrondi(nbDecimal, puisPrecision) - nbDecimal) < Math.pow(10, -12)) {
      // on a une valeur décimale
      // Si elle a plus de deux chiffres après la virgule, on donne une valeur décimale
      const nbChiffreApresVirgule = String(nbDecimal).length - 1 - String(nbDecimal).lastIndexOf('.')
      if (nbChiffreApresVirgule <= nbreDecimales) {
        newNb = nbDecimal
      }
    }
    return newNb
  }

  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation1')
    me.afficheTitre(textes.titre_exo)

    let k
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    stor.defSuite = {}
    if (ds.donneesPrecedentes && !me.donneesPersistantes.suites) {
      ds.donneesPrecedentes = false
      j3pShowError(Error('Les données précédentes n’ont pu être récupérées. De nouvelles sont générées !\nAttention car la nature de la suite n’est peut-être pas celle qui était attendue.'))
    }
    if (ds.donneesPrecedentes) {
      // si on récupère des données précédentes il faut imposer une seule répétition (on veut bosser sur la suite précédente, aucune autre générée aléatoirement)
      if (ds.nbrepetitions !== 1) {
        console.error('Pb de paramétrage, imposer donneesPrecedentes avec plusieurs répétitions n’a pas de sens')
        me.surcharge({ nbrepetitions: 1 })
      }
      // on récupère tout ce que ça contient pour le mettre dans stor
      stor.defSuite = { ...me.donneesPersistantes.suites }

      stor.defSuite.coefA = [stor.defSuite.a]
      stor.defSuite.coefB = [stor.defSuite.b]
      stor.defSuite.termeInit[0][0] = Number(stor.defSuite.termeInit[0][0])
    } else {
      stor.coefADonnees = constructionTabIntervalle(ds.a, '[2;7]', ds.nbrepetitions, [0, 1, -1, 0.3])
      stor.coefBDonnees = constructionTabIntervalle(ds.b, '[-6;6]', ds.nbrepetitions, [0])
      stor.defSuite.coefA = stor.coefADonnees.expression
      stor.defSuite.coefB = stor.coefBDonnees.expression

      // il faut que je gère le terme initial car j’ai un tableau dont les données sont de la forme 1|[...;...] ce second point pouvant être un entier ou un nb fractionnaire
      stor.defSuite.termeInit = []
      const TermeInitParDefaut = [0, '[1;5]']
      let onRecommence
      let termeU0
      let termeC
      let termeV0
      if (!isNaN(j3pNombre(String(ds.termeInitial)))) {
        // au lieu d’un tableau, n’est indiqué qu’un seul nombre. Celui-ci devient le rang initial de chaque répétition
        const leRangInit = j3pNombre(ds.termeInitial)
        for (k = 0; k < ds.nbrepetitions; k++) {
          onRecommence = false
          do {
            termeU0 = genereAlea(TermeInitParDefaut[1])
            // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
            termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
            termeV0 = j3pGetLatexSomme(termeU0, termeC)
            onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
          } while (onRecommence)
          stor.defSuite.termeInit.push([leRangInit, termeU0])
        }
      }
      for (k = 0; k < ds.nbrepetitions; k++) {
        if ((ds.termeInitial[k] === undefined) || (ds.termeInitial[k] === '')) {
          // on a rien renseigné donc par défaut, on met
          termeU0 = genereAlea(TermeInitParDefaut[1])
          stor.defSuite.termeInit.push([TermeInitParDefaut[0], termeU0])
        } else {
          // il faut la barre verticale pour séparer le rang et la valeur du terme initial
          const posBarre = String(ds.termeInitial[k]).indexOf('|')
          if (posBarre === -1) {
            const EntierReg = /\\d+/ // new RegExp('[0-9]{1,}', 'i')
            // je n’ai pas la barre verticale
            // peut-être ai-je seulement l’indice du premier terme
            if (EntierReg.test(ds.termeInitial[k])) {
              // c’est un entier correspondant à l’indice du terme initial
              onRecommence = false
              do {
                termeU0 = genereAlea(TermeInitParDefaut[1])
                // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                termeV0 = j3pGetLatexSomme(termeU0, termeC)
                onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
              } while (onRecommence)
              stor.defSuite.termeInit.push([ds.termeInitial[k], termeU0])
            } else {
              // donc je réinitialise avec la valeur par défaut
              onRecommence = false
              do {
                termeU0 = genereAlea(TermeInitParDefaut[1])
                // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                termeV0 = j3pGetLatexSomme(termeU0, termeC)
                onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
              } while (onRecommence)
              stor.defSuite.termeInit.push([TermeInitParDefaut[0], termeU0])
            }
          } else {
            // la valeur (éventuellement aléatoire) du terme initial
            onRecommence = false
            let termInitAlea
            do {
              termInitAlea = constructionTabIntervalle(ds.termeInitial[k].substring(posBarre + 1), TermeInitParDefaut[1], 1).expression[0]
              // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
              termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
              termeV0 = j3pGetLatexSomme(termInitAlea, termeC)
              onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
              // console.log('onRecommence:',onRecommence,'  termeV0:',termeV0)
            } while (onRecommence)
            // au cas où je vérifie que la première valeur est bien entière
            let rangInit = ds.termeInitial[k].substring(0, posBarre)
            rangInit = (String(j3pNombre(rangInit)).indexOf('.') === -1) ? rangInit : TermeInitParDefaut[0]
            stor.defSuite.termeInit.push([rangInit, termInitAlea])
          }
        }
      }
    }
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '8px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // choix du nom des suites
    if (!ds.donneesPrecedentes) {
      // si je n’ai pas de section précédente, je génère le nom de la suite (u_n) de manière aléatoire
      stor.defSuite.nomU = j3pGetRandomElts(['u', 'v'], 1)[0]
    }
    // je mets mes valeurs dans une variable plus simple :
    stor.defSuite.a = stor.defSuite.coefA[me.questionCourante - 1]
    stor.defSuite.b = stor.defSuite.coefB[me.questionCourante - 1]
    // je prends la suite intermédiaire v_n=u_n-b/(1-a). Je note c=-b/(1-a)
    stor.cFrac = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.b), j3pGetLatexSomme('1', j3pGetLatexOppose((j3pGetLatexQuotient(stor.defSuite.a * 1000, 1000)))))
    // si c est une valeur décimale simple, alors je l’affiche sous forme décimale
    stor.defSuite.c = ConvertDecimal(stor.cFrac, 2)
    stor.defSuite.v0 = j3pGetLatexSomme(stor.defSuite.termeInit[me.questionCourante - 1][1], stor.cFrac)
    stor.typeLimite = (Math.abs(stor.defSuite.a) < 1) ? 'nombre' : 'texte'
    stor.laLimite = (stor.typeLimite === 'nombre')
      ? j3pGetLatexOppose(stor.defSuite.c)
      : (j3pCalculValeur(stor.defSuite.v0) < 0) ? '-\\infty' : '+\\infty'
    // me.donneesPersistantes.suites sert à envoyer des informations dans le bilan qui pourront être récupérées par une autre section sur les suites
    // Ceci a pour but de poursuivre l’étude de la suite définie dans cette section
    me.donneesPersistantes.suites = { ...stor.defSuite }
    me.logIfDebug('me.donneesPersistantes.suites:', me.donneesPersistantes.suites)
    const relRecurrence = j3pGetLatexMonome(1, 1, stor.defSuite.a, stor.defSuite.nomU + '_n') + j3pGetLatexMonome(2, 0, stor.defSuite.b, '')

    j3pAffiche(stor.zoneCons1, '', textes.consigne1,
      {
        u: stor.defSuite.nomU,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        i: stor.defSuite.termeInit[me.questionCourante - 1][1],
        j: relRecurrence
      })
    if (ds.donneesPrecedentes) {
      const consigne2 = (String(stor.laLimite).indexOf('infty') > -1) ? textes.consigne2_2 : textes.consigne2_1
      j3pAffiche(stor.zoneCons2, '', consigne2,
        {
          u: stor.defSuite.nomU,
          l: stor.laLimite
        })
    }
    stor.defSuite.c = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.b), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.a)))
    stor.defSuite.c = ConvertDecimal(stor.defSuite.c, 2)
    // Les calculs suivants vont servir pour la correction :

    stor.listeUn = [stor.defSuite.termeInit[me.questionCourante - 1][1]]
    let u = stor.defSuite.termeInit[me.questionCourante - 1][1]
    const nbDec = (stor.defSuite.a < 0.35) ? 5 : 3
    const borne = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b)// C’est que la suite sera croissante
      ? (stor.laLimite === '+\\infty') ? 100000 : j3pArrondi(j3pCalculValeur(stor.laLimite) - Math.pow(10, -nbDec), nbDec)
      : (stor.laLimite === '-\\infty') ? -100000 : j3pArrondi(j3pCalculValeur(stor.laLimite) + Math.pow(10, -nbDec), nbDec)
    let condition = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b) ? (u <= borne) : (u >= borne)
    while (condition) {
      u = j3pArrondi(stor.defSuite.a * u + stor.defSuite.b, 8)
      stor.listeUn.push(u)
      condition = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b) ? (u <= borne) : (u >= borne)
    }
    stor.condSeuil = (stor.defSuite.termeInit[me.questionCourante - 1][1] < stor.defSuite.a * stor.defSuite.termeInit[me.questionCourante - 1][1] + stor.defSuite.b) // C’est que la suite sera croissante
      ? stor.defSuite.nomU + '_n>' + j3pVirgule(borne)
      : stor.defSuite.nomU + '_n<' + j3pVirgule(borne)
    stor.elt = j3pAffiche(stor.zoneCons3, '', textes.consigne3,
      {
        cond: stor.condSeuil,
        inputmq1: { texte: '' }
      })
    me.logIfDebug('listeUn:', stor.listeUn, 'la limite:', stor.laLimite)

    // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
    // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche
    mqRestriction(stor.elt.inputmqList[0], '\\d')// on n’autorise que les lettres minuscules
    stor.elt.inputmqList[0].typeReponse = ['nombre', 'exact']
    stor.elt.inputmqList[0].reponse = [stor.listeUn.length - 1]
    j3pFocus(stor.elt.inputmqList[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    stor.mesZonesSaisie = [stor.elt.inputmqList[0].id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({ parcours: me, zones: stor.mesZonesSaisie })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              j3pDesactive(stor.elt.inputmqList[0])
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
