import { j3pAddElt, j3pGetRandomElts, j3pDetruit, j3pFocus, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pMathquillXcas, j3pNombre, j3pPaletteMathquill, j3pShowError, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pCreeBoutonsFenetre, j3pDetruitToutesLesFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexOppose, j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import { arithGeo, arithmetique, geometrique } from 'src/legacy/sections/lycee/suites/constantesSuites'
import textesGeneriques from 'src/lib/core/textes'

// pas d’upgradeParametres générique car dans cette section typeSuite est un entier !
// => seulement donneesPrecedentes
export { upgradeParametreDonneesPrecedentes as upgradeParametres } from './constantesSuites'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        novembre 2016
        Dans cette section on propose de calculer la somme des termes d’une suite dont le terme général est de la forme
        u_n=a*q^(n-n0)+b*n+c
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeSuite', 1, 'entier', '4 cas de figure peuvent être proposés :<br/>-1: (par défaut), le cas le plus complet : u_n=a*q^(n-n0)+b*n+c<br/>-2 : u_n=a*q^(n-n0), pour une suite géométrique<br/>-3 : u_n=b*n+c, pour une suite arithmétique<br/>-4 : u_n=a*q^(n-n0)+c pour une suite arithmético-géométrique'],
    ['a', '[-2;1.7]', 'string', 'Intervalle où se trouve la valeur a dans l’expression u_{n+1}=a*q^(n-n0)+b*n+c (avec ce type d’intervalle, il peut être décimal). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.'],
    ['q', '[-2.5;2.5]', 'string', 'Intervalle où se trouve la valeur q dans l’expression u_{n+1}=a*q^(n-n0)+b*n+c (avec ce type d’intervalle, il peut être décimal) (intervalle "[0.5;2.5]" pour le niveau TES). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.'],
    ['b', '[-0.5;1]', 'string', 'Intervalle où se trouve la valeur b dans l’expression u_{n+1}=a*q^(n-n0)+b*n+c (avec ce type d’intervalle, il peut être décimal). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.'],
    ['c', '[-4;4]', 'string', 'Intervalle où se trouve la valeur c dans l’expression u_{n+1}=a*q^(n-n0)+b*n+c. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.'],
    ['rangInitial', [0, 1], 'array', 'Tableau de taille nbrepetitions où chaque terme contient le rang initial.'],
    ['afficheAide', false, 'boolean', 'Si ce paramètre vaut true, alors on donne accès aux formules du cours sur la somme de termes de suites arithmétiques et géométriques.'],
    ['niveau', 'TS', 'string', 'prend la valeur "TS" (par défaut) ou "TES" (utile pour rédiger la correction).'],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la suite étudiée est celle du nœud précédent.']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Somme des termes d’une suite',
  titre_aide: 'Aide & outils',
  titre_bouton: 'Rappels de cours',
  aide: 'Aide',
  // on donne les phrases de la consigne
  consigne1_1: 'Soit $(£u_n)$ la suite définie par $£u_{£n}=£i$ et, pour tout entier $n\\geq £n$, par $£u_{n+1}=£j$.',
  consigne1_2: 'Soit $(£u_n)$ la suite définie pour tout entier $n\\geq £n$, par $£u_n=£j$.',
  consigne2: 'Pour tout entier $n\\geq £n$, $£u_n=£t$.',
  consigne3: 'On veut calculer la somme des termes de cette suite : ',
  // consigne4 : '$S=\\sum_{k=£n}^{n} £u_k=£u_{£n}+£u_{£o}+£u_{£p}+\\ldots+£u_n$',
  consigne4: '$S=\\bigsum{k=£n}{n} £u_k=£u_{£n}+£u_{£o}+£u_{£p}+\\ldots+£u_n$',
  consigne5: '$S=$&1&',
  // textes présents dans la partie "Rappels de cours"
  aide1: 'On rappelle que :<br/>pour tout réel $q\\neq 0$, $1+q+q^2+\\ldots+q^n=\\frac{1-q^{n+1}}{1-q}$<br/>De plus $1+2+3+\\ldots+n=\\frac{n(n+1)}{2}$.',
  aide2: 'Il existe aussi une formule plus générale pour les suites géométriques de raison $q$ différente de 0 et 1 et de premier terme $u_p$:<br/>$\\bigsum{k=p}{n} u_k=u_p+u_{p+1}+\\ldots+u_n=u_p\\times \\frac{1-q^{n-p+1}}{1-q}$',
  aide3: 'Il existe aussi une formule plus générale pour les suites arithmétiques de raison $r\\neq 0$ et de premier terme $u_p$ : <br/>$\\bigsum{k=p}{n} u_k=u_p+u_{p+1}+\\ldots+u_n=(n-p+1)\\times \\frac{u_p+u_n}{2}$',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: '',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'On effectue alors un regroupement de membres de la même forme :',
  corr2: 'En simplifiant en partie, on obtient :'
}

/**
 * section sommeTermesSuite
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    if (!stor.afficheAide) afficheZoneAide()
    j3pDetruit(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let k = 1; k <= 7; k++) stor['zoneExpli' + k] = j3pAddElt(zoneExpli, 'div')
    // pour me simplifier la vie dans les écritures :
    const q = stor.defSuite.coefQ[me.questionCourante - 1]
    const a = stor.defSuite.coefA[me.questionCourante - 1]
    const b = stor.defSuite.coefB[me.questionCourante - 1]
    const c = stor.defSuite.coefC[me.questionCourante - 1]
    let decoupageSomme, dernierePuissanceQ
    if (stor.defSuite.typeSuite % 2 === 0) {
      dernierePuissanceQ = (stor.defSuite.rangInitial[me.questionCourante - 1] === 0) ? 'n' : 'n-' + stor.defSuite.rangInitial[me.questionCourante - 1]
      decoupageSomme = affichageTerme(a, b, c, stor.afficheQ, 0, 1) + affichageTerme(a, b, c, stor.afficheQ, 1, 2) + '+\\ldots' + affichageTerme(a, b, c, stor.afficheQ, dernierePuissanceQ, 3)
    } else {
      decoupageSomme = affichageTerme(a, b, c, stor.afficheQ, stor.defSuite.rangInitial[me.questionCourante - 1], 1) + affichageTerme(a, b, c, stor.afficheQ, Number(stor.defSuite.rangInitial[me.questionCourante - 1] + 1), 2) + '+\\ldots' + affichageTerme(a, b, c, stor.afficheQ, 'n', 3)
    }
    j3pAffiche(stor.zoneExpli1, '', '$S=' + decoupageSomme + '$')
    if (stor.defSuite.typeSuite !== 2) {
      j3pAffiche(stor.zoneExpli2, '', textes.corr1)
    }
    let nouvelleSomme = ''
    let nouvelleSomme2 = ''
    let calculSomme = ''
    let nouvelleSomme3 = ''
    let nouvelleSomme4 = ''
    let membreNul = true// c’est qu’on n’a encore rien écrit
    if (Math.abs(j3pCalculValeur(a)) > Math.pow(10, -12)) {
      const leSigneDevantldots1 = (j3pCalculValeur(a) < 0) ? '-' : '+'
      if (stor.defSuite.typeSuite % 2 === 0) {
        // dans le cas d’une suite géométrique ou arithmético-géométrique, on aura une somme de la forme :
        // 1+q+...+q^{n-n0}
        nouvelleSomme += affichageTerme(a, 0, 0, stor.afficheQ, 0, 1) + affichageTerme(a, 0, 0, stor.afficheQ, 1, 2) + leSigneDevantldots1 + '\\ldots' + affichageTerme(a, 0, 0, stor.afficheQ, dernierePuissanceQ, 3)
      } else {
        nouvelleSomme += '\\left(' + affichageTerme(a, 0, 0, stor.afficheQ, stor.defSuite.rangInitial[me.questionCourante - 1], 1) + affichageTerme(a, 0, 0, stor.afficheQ, stor.defSuite.rangInitial[me.questionCourante - 1] + 1, 2) + leSigneDevantldots1 + '\\ldots' + affichageTerme(a, 0, 0, stor.afficheQ, 'n', 3) + '\\right)'
      }
      membreNul = false
      let sommePuisQ
      let simplSommePuisQ
      // on retrouvera la somme de la forme q^n0+q^(n0+1)+...+q^n
      let simplSommePuisQ2
      if (stor.defSuite.typeSuite === 1) {
        if (Number(stor.defSuite.rangInitial[me.questionCourante - 1]) === 0) {
          sommePuisQ = '1' + j3pGetLatexMonome(2, 0, stor.defSuite.coefQ[me.questionCourante - 1], '') + '+\\ldots' + affichageTerme(1, 0, 0, stor.afficheQ, 'n', 3)
          simplSommePuisQ = '\\frac{1-' + stor.afficheQ + '^{n+1}}{1-' + stor.afficheQ + '}'
          simplSommePuisQ2 = '\\left(1-' + stor.afficheQ + '^{n+1}\\right)'
        } else if (Number(stor.defSuite.rangInitial[me.questionCourante - 1]) === 1) {
          sommePuisQ = j3pGetLatexMonome(1, 0, stor.defSuite.coefQ[me.questionCourante - 1], '') + affichageTerme(1, 0, 0, stor.afficheQ, '2', 2) + '+\\ldots' + affichageTerme(1, 0, 0, stor.afficheQ, 'n', 3)
          simplSommePuisQ = '\\frac{1-' + stor.afficheQ + '^{n}}{1-' + stor.afficheQ + '}'
          simplSommePuisQ2 = '\\left(1-' + stor.afficheQ + '^{n}\\right)'
        } else {
          sommePuisQ = affichageTerme(1, 0, 0, stor.afficheQ, stor.defSuite.rangInitial[me.questionCourante - 1], 1) + affichageTerme(1, 0, 0, stor.afficheQ, stor.defSuite.rangInitial[me.questionCourante - 1] + 1, 2) + '+\\ldots' + affichageTerme(1, 0, 0, stor.afficheQ, 'n', 3)
          simplSommePuisQ = '\\frac{1-' + stor.afficheQ + '^{n-' + (stor.defSuite.rangInitial[me.questionCourante - 1] - 1) + '}}{1-' + stor.afficheQ + '}'
          simplSommePuisQ2 = '\\left(1-' + stor.afficheQ + '^{n-' + (stor.defSuite.rangInitial[me.questionCourante - 1] - 1) + '}\\right)'
        }
      } else {
        // cas d’une suite géométrique ou arithmético-géométrique, on aura une somme de la forme :
        if (Number(stor.defSuite.rangInitial[me.questionCourante - 1]) === 0) {
          sommePuisQ = '1' + j3pGetLatexMonome(2, 0, stor.defSuite.coefQ[me.questionCourante - 1], '') + '+\\ldots' + affichageTerme(1, 0, 0, stor.afficheQ, 'n', 3)
          simplSommePuisQ = '\\frac{1-' + stor.afficheQ + '^{n+1}}{1-' + stor.afficheQ + '}'
          simplSommePuisQ2 = '\\left(1-' + stor.afficheQ + '^{n+1}\\right)'
        } else if (Number(stor.defSuite.rangInitial[me.questionCourante - 1]) === 1) {
          sommePuisQ = '1' + j3pGetLatexMonome(2, 0, stor.defSuite.coefQ[me.questionCourante - 1], '') + '+\\ldots' + affichageTerme(1, 0, 0, stor.afficheQ, 'n-1', 3)
          simplSommePuisQ = '\\frac{1-' + stor.afficheQ + '^{n}}{1-' + stor.afficheQ + '}'
          simplSommePuisQ2 = '\\left(1-' + stor.afficheQ + '^{n}\\right)'
        } else {
          sommePuisQ = '1' + j3pGetLatexMonome(2, 0, stor.defSuite.coefQ[me.questionCourante - 1], '') + '+\\ldots' + affichageTerme(1, 0, 0, stor.afficheQ, 'n-' + stor.defSuite.rangInitial[me.questionCourante - 1], 3)
          simplSommePuisQ = '\\frac{1-' + stor.afficheQ + '^{n-' + (stor.defSuite.rangInitial[me.questionCourante - 1] - 1) + '}}{1-' + stor.afficheQ + '}'
          simplSommePuisQ2 = '\\left(1-' + stor.afficheQ + '^{n-' + (stor.defSuite.rangInitial[me.questionCourante - 1] - 1) + '}\\right)'
        }
      }
      if (Math.abs(j3pCalculValeur(a) - 1) < Math.pow(10, -12)) {
        nouvelleSomme2 = sommePuisQ
        nouvelleSomme3 = simplSommePuisQ
      } else if (Math.abs(j3pCalculValeur(a) + 1) < Math.pow(10, -12)) {
        nouvelleSomme3 = '-' + simplSommePuisQ
        nouvelleSomme2 = '-\\left(' + sommePuisQ + '\\right)'
      } else {
        nouvelleSomme3 = a + '\\times' + simplSommePuisQ
        nouvelleSomme2 = a + '\\left(' + sommePuisQ + '\\right)'
      }
      const coefDevantPar = j3pGetLatexQuotient(a, j3pGetLatexSomme('1', j3pGetLatexOppose(q)))

      me.logIfDebug('q:', q, '  a:', a, '  j3pGetLatexSomme(1,j3pGetLatexOppose(q)):', j3pGetLatexSomme('1', j3pGetLatexOppose(q)), '   coefDevantPar:', coefDevantPar)

      if (Math.abs(j3pCalculValeur(coefDevantPar) - 1) < Math.pow(10, -12)) {
        nouvelleSomme4 = simplSommePuisQ2
      } else if (Math.abs(j3pCalculValeur(coefDevantPar) + 1) < Math.pow(10, -12)) {
        nouvelleSomme4 = '-' + simplSommePuisQ2
      } else {
        nouvelleSomme4 = coefDevantPar + simplSommePuisQ2
      }
    }
    if (Math.abs(j3pCalculValeur(b)) > Math.pow(10, -12)) {
      const signeDevantPar = (membreNul) ? '' : '+'
      const leSigneDevantldots2 = (j3pCalculValeur(b) < 0) ? '-' : '+'
      nouvelleSomme += signeDevantPar + '\\left(' + affichageTerme(0, b, 0, 0, stor.defSuite.rangInitial[me.questionCourante - 1], 1) + affichageTerme(0, b, 0, 0, stor.defSuite.rangInitial[me.questionCourante - 1] + 1, 2) + leSigneDevantldots2 + '\\ldots' + affichageTerme(0, b, 0, 0, 'n', 3) + '\\right)'
      let calculSomme2
      let tabSomme3
      if (Number(stor.defSuite.rangInitial[me.questionCourante - 1]) === 0) {
        calculSomme = '1+\\ldots+n'
        calculSomme2 = '\\frac{n(n+1)}{2}'
        tabSomme3 = ['n', '(n+1)']
      } else if (Number(stor.defSuite.rangInitial[me.questionCourante - 1]) === 1) {
        calculSomme = '1+2+\\ldots+n'
        calculSomme2 = '\\frac{n(n+1)}{2}'
        tabSomme3 = ['n', '(n+1)']
      } else {
        calculSomme = stor.defSuite.rangInitial[me.questionCourante - 1] + '+' + (stor.defSuite.rangInitial[me.questionCourante - 1] + 1) + '+\\ldots+n'
        calculSomme2 = '\\frac{(n+' + stor.defSuite.rangInitial[me.questionCourante - 1] + ')(n-' + (stor.defSuite.rangInitial[me.questionCourante - 1] - 1) + ')}{2}'
        tabSomme3 = ['(n+' + stor.defSuite.rangInitial[me.questionCourante - 1] + ')', '(n-' + (stor.defSuite.rangInitial[me.questionCourante - 1] - 1) + ')']
      }
      if (Math.abs(j3pCalculValeur(b) - 1) < Math.pow(10, -12)) {
        nouvelleSomme2 += '+\\left(' + calculSomme + '\\right)'
        nouvelleSomme3 += '+' + calculSomme2
      } else if (Math.abs(j3pCalculValeur(b) + 1) < Math.pow(10, -12)) {
        nouvelleSomme2 += '-\\left(' + calculSomme + '\\right)'
        nouvelleSomme3 += '-' + calculSomme2
      } else {
        let newb = b
        if (String(b).indexOf('frac') > -1) {
          newb = (j3pCalculValeur(b) < 0) ? '-' + j3pGetLatexProduit('-1', b) : b
        }
        if (!membreNul) {
          // il me faut peut-être ajouter un signe
          newb = (j3pCalculValeur(b) < 0) ? newb : '+' + newb
        }
        nouvelleSomme2 += newb + '\\left(' + calculSomme + '\\right)'
        nouvelleSomme3 += newb + '\\times' + calculSomme2
      }
      const dernierCoef = j3pGetLatexQuotient(b, '2')
      if (membreNul) {
        nouvelleSomme4 += j3pGetLatexMonome(1, 1, dernierCoef, tabSomme3[0]) + tabSomme3[1]
      } else {
        nouvelleSomme4 += j3pGetLatexMonome(2, 1, dernierCoef, tabSomme3[0]) + tabSomme3[1]
      }
    }
    if (Math.abs(j3pCalculValeur(c)) > Math.pow(10, -12)) {
      const leSigneDevantldots3 = (j3pCalculValeur(c) < 0) ? '-' : '+'
      let dernierMembre, toutDernier
      nouvelleSomme += '+\\left(' + j3pGetLatexMonome(1, 0, c, '') + j3pGetLatexMonome(2, 0, c, '') + leSigneDevantldots3 + '\\ldots' + j3pGetLatexMonome(2, 0, c, '') + '\\right)'
      const newc = (String(c).charAt(0) === '-') ? '\\left(' + c + '\\right)' : c
      if (Number(stor.defSuite.rangInitial[me.questionCourante - 1]) === 0) {
        dernierMembre = '+(n+1)\\times ' + newc
        toutDernier = j3pGetLatexMonome(2, 1, c, '(n+1)')
      } else if (Number(stor.defSuite.rangInitial[me.questionCourante - 1]) === 1) {
        dernierMembre = '+n\\times ' + newc
        toutDernier = j3pGetLatexMonome(2, 1, c, 'n')
      } else {
        dernierMembre = '+(n-' + (stor.defSuite.rangInitial[me.questionCourante - 1] - 1) + ')\\times ' + newc
        toutDernier = j3pGetLatexMonome(2, 1, c, '(n-' + (stor.defSuite.rangInitial[me.questionCourante - 1] - 1) + ')')
      }
      nouvelleSomme2 += dernierMembre
      nouvelleSomme3 += toutDernier
      nouvelleSomme4 += toutDernier
    }
    if (stor.defSuite.typeSuite !== 2) {
      j3pAffiche(stor.zoneExpli3, '', '$S=' + nouvelleSomme + '$')
    }
    j3pAffiche(stor.zoneExpli4, '', '$S=' + nouvelleSomme2 + '$')
    j3pAffiche(stor.zoneExpli5, '', '$S=' + nouvelleSomme3 + '$')
    j3pAffiche(stor.zoneExpli6, '', textes.corr2)
    j3pAffiche(stor.zoneExpli7, '', '$S=' + nouvelleSomme4 + '$')
  }

  function afficheZoneAide () {
    const nameBtn = j3pGetNewId('Boutons')
    const fenetreBtn = j3pGetNewId('fenetreBoutons')
    const nameAide = j3pGetNewId('Aide')
    const fenetreAide = j3pGetNewId('fenetreAide')
    const untableau = [
      {
        name: nameBtn,
        title: textes.titre_aide,
        left: me.zonesElts.MD.offsetLeft,
        top: me.zonesElts.MD.offsetTop + 100,
        height: 120,
        id: fenetreBtn
      }
    ]
    untableau.push({
      name: nameAide,
      title: textes.titre_bouton,
      top: me.zonesElts.MD.offsetTop + 60,
      left: me.zonesElts.MG.offsetLeft,
      width: 500,
      id: fenetreAide
    })
    me.fenetresjq = untableau
    // Création mais sans affichage.
    j3pCreeFenetres(me)
    j3pCreeBoutonsFenetre(fenetreBtn, [[nameAide, textes.titre_bouton]])
    j3pToggleFenetres(nameBtn)
    for (let i = 1; i <= 3; i++) {
      const elt = j3pAddElt(fenetreAide, 'div')
      j3pAffiche(elt, '', textes['aide' + i])
    }
  }

  function affichageTerme (a, b, c, q, n, pos) {
    // a,b,c,q et n sont les paramètres de la suite a*q^n+b*n+c où je remplace chaque terme par les données chiffrées
    // ici q est afficheQ de l’exo
    let newa = a
    let ligne = ''
    if (pos > 1) {
      // on doit éventuellement ajouter un signe devant
      if (Math.abs(j3pCalculValeur(a)) > Math.pow(10, -12)) {
        if (String(a).includes('frac')) {
          newa = (j3pCalculValeur(a) < 0) ? '-' + j3pGetLatexProduit('-1', a) : a
        }
        if (j3pCalculValeur(a) > 0) {
          ligne += '+'
        }
      } else if (Math.abs(j3pCalculValeur(b)) > Math.pow(10, -12)) {
        if (j3pCalculValeur(b) > 0) {
          ligne += '+'
        }
      }
    }
    ligne += (Math.abs(j3pCalculValeur(a)) < Math.pow(10, -12)) ? '' : (Number(a) === 1) ? q + '^{' + n + '}' : (Number(a) === -1) ? '-' + q + '^{' + n + '}' : newa + '\\times' + q + '^{' + n + '}'
    let newb = b
    if (String(b).indexOf('frac') > -1) {
      newb = (j3pCalculValeur(b) < 0) ? '-' + j3pGetLatexProduit('-1', b) : b
    }
    let newc = c
    if (String(c).indexOf('frac') > -1) {
      newc = (j3pCalculValeur(c) < 0) ? '-' + j3pGetLatexProduit('-1', c) : c
    }
    if (Math.abs(j3pCalculValeur(b)) > Math.pow(10, -12)) {
      const membre2 = (Number(b) === 1) ? '+' + n : (Number(b) === -1) ? '-' + n : (Math.abs(j3pCalculValeur(a)) < Math.pow(10, -12)) ? newb + '\\times ' + n : (j3pCalculValeur(b) < 0) ? newb + '\\times ' + n : '+' + newb + '\\times ' + n
      ligne += membre2
    }
    if (Math.abs(j3pCalculValeur(c)) > Math.pow(10, -12)) {
      const membre3 = (j3pCalculValeur(c) < 0) ? newc : '+' + newc
      ligne += membre3
    }
    // console.log('membre2:',membre2,'  membre3:',membre3,'   ligne:',ligne)
    return ligne
  }

  function genereAlea (int) {
    // int est un intervalle
    // mais on peut trouver des cas de figure où par faignantise, on aura utilisé cette fonction sans vérifier que int est un intervalle
    // Ce serait alors un nombre
    if (String(int).charAt(0) === '[') {
      // si les bornes sont entières, alors on génère un nombre entier aléatoire dans cet intervalle
      // si elles sont décimales, il faut que je génère un décimal de l’intervalle
      const PosPointVirgule = int.indexOf(';')
      let val1 = j3pNombre(int.substring(1, PosPointVirgule))
      let val2 = j3pNombre(int.substring(PosPointVirgule + 1, int.length - 1))
      let posPoint = -1
      if ((String(val1).indexOf('.') === -1)) {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = String(val2).length - String(val2).lastIndexOf('.') - 1
        }
      } else {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = Math.max(String(val1).length - String(val1).lastIndexOf('.') - 1, String(val2).length - String(val2).lastIndexOf('.') - 1)
        } else {
          posPoint = String(val1).length - String(val1).lastIndexOf('.') - 1
        }
      }
      if (val1 > val2) {
        // les bornes ne sont pas dans le bon ordre
        const val3 = val1
        val1 = val2
        val2 = val3
      }
      if (posPoint === -1) {
        // je n’ai que des nombres entiers
        const [minInt, maxInt] = j3pGetBornesIntervalle(int)
        return j3pGetRandomInt(minInt, maxInt)
      } else {
        // j’ai des décimaux
        const borneInf = val1 * Math.pow(10, posPoint)
        const borneSup = val2 * Math.pow(10, posPoint)
        const nbAlea = j3pGetRandomInt(String(borneInf), String(borneSup)) * Math.pow(10, -posPoint)
        return Math.round(nbAlea * Math.pow(10, 10)) / Math.pow(10, 10)
      }
    } else {
      return int
    }
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut, nbRepet, tabValInterdites) {
    // cette fonction permet de vérifier si ce qui est renseigné est bien un intervalle ou un nombre (éventuellement fractionnaire) et ce plusieurs fois (en général nbrepetitions)
    // cela permet de valider ce qui peut être saisi par un utilisateur dans le cas où on peut imposer des valeurs
    // texteIntervalle est une chaîne où les éléments peuvent être séparés par '|' et où chaque élément est un nombre, une fraction ou un intervalle
    // si c’est un intervalle, la fonction va dans un second temps générer de manière aléatoire une valeur de cet intervalle
    // il peut y avoir des valeurs interdites précisées dans tabValInterdites, même si elles peuvent être dans l’intervalle
    // la fonction renvoie un tableau de nbRepet éléments
    // chaque élément est un intervalle voire l’intervalle par défaut si ce qui est précisé n’est aps compris
    // ce peut aussi être un nombre entier, décimal ou fractionnaire
    // dans ce dernier cas, on renvoit une chaine où on retrouve le code latex et la valeur décimale (arrondie) séparés par |

    /* cette fonction renvoie un objet. Cet objet contient 3 tableaux de nbRepet éléments
        - leType[k] vaut 'nombre', 'fraction' ou 'intervalle' suivant la valeur de texteIntervalle
        - expression contient l’écriture de chaque valeur imposée ou gérée aléatoirement quand leType[k] vaut 'intervalle'
        - valeur contient la valeur décimale de ces nombres, cela ne peut être utile que lorsque leType[k] vaut 'fraction'
        */
    function lgIntervalle (intervalle) {
      // cette fonction renvoie la longueur de l’intervalle'
      // si ce n’est pas un intervalle, alors elle renvoie 0'
      let lg = 0
      // var intervalleReg = new RegExp("\\[\\-?[0-9]{1,};\\-?[0-9]{1,}\\]","i");
      const intervalleReg = /\[-?[0-9]+(.|,)?[0-9]*;-?[0-9]+(.|,)?[0-9]*]/g // new RegExp('\\[\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,};\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,}\\]', 'i')
      if (intervalleReg.test(intervalle)) {
        // on a bien un intervalle
        const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
        const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
        lg = borneSup - borneInf
      }
      return lg
    }

    function nbDansTab (nb, tab) {
      // test si la valeur correspondant à nb est dans le tableau tab (ses valeurs pouvant être au format latex)
      if (tab === undefined) {
        return false
      } else {
        let puisPrecision = 15
        if (Math.abs(nb) > Math.pow(10, -13)) {
          const ordrePuissance = Math.floor(Math.log(Math.abs(nb)) / Math.log(10)) + 1
          puisPrecision = 15 - ordrePuissance
        }
        for (let k = 0; k < tab.length; k++) {
          if (Math.abs(nb - j3pCalculValeur(tab[k])) < Math.pow(10, -puisPrecision)) {
            return true
          }
        }
        return false
      }
    }

    const intervalleReg = /\[-?[0-9]+(.|,)?[0-9]*;-?[0-9]+(.|,)?[0-9]*]/g // new RegExp('\\[\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,};\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,}\\]', 'i')
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const DonneesIntervalle = {}
    // DonneesIntervalle contient trois propriétés qui sont des tableaux de longueur nbRepet :
    // - leType : qui correspond à ce qui a été renseigné initialement ('intervalle', 'nombre', 'fraction') - 'nombre' désigne un entier ou un décimal
    // - expression : qui est le nombre (si type==='nombre'), une valeur générée aléatoirement dans un intervalle (si type==='intervalle'), l’écriture l’atex de la fraction (si type==='fraction')
    // - valeur : qui est la valeur décimale de l’expression (utile surtout si type==='fraction')
    DonneesIntervalle.leType = []
    DonneesIntervalle.expression = []
    DonneesIntervalle.valeur = []
    let nbAlea,
      valNb,
      tabDecomp,
      monnum,
      monden,
      valFrac
    if (String(texteIntervalle).indexOf('|') > -1) {
      const tableau = texteIntervalle.split('|')
      for (k = 0; k < nbRepet; k++) {
        if (intervalleReg.test(tableau[k])) {
          // c’est bien un intervalle'
          if (lgIntervalle(tableau[k]) === 0) {
            // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
            valNb = tableau[k].substring(1, tableau[k].indexOf(';'))
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          } else {
            do {
              nbAlea = genereAlea(tableau[k])
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        } else {
          // peut être est-ce tout simplement un nombre :
          if ((tableau[k] === undefined) || (tableau[k] === '')) {
            // là l’expression n’est pas renseignée
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          } else if (nbFracReg.test(tableau[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tableau[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
            DonneesIntervalle.leType.push('fraction')
            DonneesIntervalle.valeur.push(valFrac)
          } else if (!isNaN(j3pNombre(tableau[k]))) {
            // on a un nombre
            DonneesIntervalle.expression.push(tableau[k])
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(tableau[k])
          } else {
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      }
    } else {
      if ((texteIntervalle === '') || (texteIntervalle === undefined)) {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      } else if (intervalleReg.test(String(texteIntervalle))) {
        if (lgIntervalle(String(texteIntervalle)) === 0) {
          // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
          valNb = String(texteIntervalle).substring(1, String(texteIntervalle).indexOf(';'))
          for (k = 0; k < nbRepet; k++) {
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          }
        } else {
          for (k = 0; k < nbRepet; k++) {
            do {
              nbAlea = genereAlea(texteIntervalle)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
          DonneesIntervalle.leType.push('fraction')
          DonneesIntervalle.valeur.push(valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push(String(texteIntervalle))
          DonneesIntervalle.leType.push('nombre')
          DonneesIntervalle.valeur.push(String(texteIntervalle))
        }
      } else {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      }
    }
    return DonneesIntervalle
  }

  function ConvertDecimal (nb, nbreDecimales) {
    // nb peut être un entier, un décimal ou un nb fractionnaire
    // si c’est un nb fractionnaire et que la forme décimale contient moins de nbreDecimales, alors on renvoie le nb sous forme décimale
    // sinon on renvoit nb sous son format initial
    const nbDecimal = j3pCalculValeur(nb)
    // pour éviter les pbs d’arrondi, je cherche par quelle puissance de 10 multiplier mon nombre (puis el divisier)
    let puisPrecision = 15
    if (Math.abs(nbDecimal) > Math.pow(10, -13)) {
      const ordrePuissance = Math.floor(Math.log(Math.abs(nbDecimal)) / Math.log(10)) + 1
      puisPrecision = 15 - ordrePuissance
    }
    let newNb = nb
    if (Math.abs(Math.round(nbDecimal * Math.pow(10, puisPrecision)) / Math.pow(10, puisPrecision) - nbDecimal) < Math.pow(10, -12)) {
      // on a une valeur décimale
      // Si elle a plus de deux chiffres après la virgule, on donne une valeur décimale
      const nbChiffreApresVirgule = String(nbDecimal).length - 1 - String(nbDecimal).lastIndexOf('.')
      if (nbChiffreApresVirgule <= nbreDecimales) {
        newNb = nbDecimal
      }
    }
    return newNb
  }

  function testExpressions (express1, express2, variable) {
    // cette fonction vérifie si expression représentent bien la même fonction ou la même suite
    // variable est la variable présente dans les expressions
    const newExpres = [express1, express2].map(expr => expr.replace(/([a-z])\(/g, '$1*('))
    const unarbre1 = new Tarbre(newExpres[0], [variable])
    const unarbre2 = new Tarbre(newExpres[1], [variable])
    let egaliteExp = true
    for (let i = 2; i <= 9; i++) {
      egaliteExp = (egaliteExp && (Math.abs(unarbre1.evalue([i]) - unarbre2.evalue([i])) < Math.pow(10, -12)))
    }
    return egaliteExp
  }

  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation1')
    me.afficheTitre(textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    stor.afficheAide = ds.afficheAide
    stor.defSuite = {}
    if (ds.donneesPrecedentes) {
      // on récupère les données du noeud précédent stockées dans me.donneesPersistantes.suites
      if (!me.donneesPersistantes.suites) {
        ds.donneesPrecedentes = false
        j3pShowError(Error('Les données précédentes n’ont pu être récupérées. De nouvelles sont générées !\nAttention car la nature de la suite n’est peut-être pas celle qui était attendue.'))
      }
    }
    if (ds.donneesPrecedentes) {
      // si on récupère des données précédentes il faut imposer une seule répétition (on veut bosser sur la suite précédente, aucune autre générée aléatoirement)
      if (ds.nbrepetitions !== 1) {
        console.error('Pb de paramétrage, imposer donneesPrecedentes avec plusieurs répétitions n’a pas de sens')
        me.surcharge({ nbrepetitions: 1 })
      }
      // on récupère tout ce que ça contient pour le mettre dans stor
      stor.defSuite = { ...me.donneesPersistantes.suites }

      me.logIfDebug('stor.defSuite:', stor.defSuite)
      // il faut transformer le typeSuite habituel en entier pour cette section
      stor.defSuite.typeSuite = (stor.defSuite.typeSuite === arithmetique)
        ? 3
        : (stor.defSuite.typeSuite === geometrique) ? 2 : 4
      stor.defSuite.coefQ = [stor.defSuite.a]
      if (stor.defSuite.typeSuite === 4) {
        // suite arithméticogéométrique
        stor.defSuite.coefA = [ConvertDecimal(j3pGetLatexSomme(stor.defSuite.u0, stor.defSuite.c), 2)]
        stor.defSuite.coefC = [j3pGetLatexOppose(stor.defSuite.c)]
      } else if (stor.defSuite.typeSuite === 3) {
        // suite arithmétique
        stor.defSuite.coefA = [0]
        stor.defSuite.coefC = [j3pGetLatexSomme(stor.defSuite.u0, j3pGetLatexProduit(stor.defSuite.b, j3pGetLatexOppose(stor.defSuite.termeInit[0][0])))]
        // coefB n’est présent que pour une suite arithmétique
        stor.defSuite.coefB = [stor.defSuite.b]
      } else if (stor.defSuite.typeSuite === 2) {
        // suite gémétrique
        stor.defSuite.coefA = [stor.defSuite.u0]
        stor.defSuite.coefC = []// j3pGetLatexOppose(stor.defSuite.c)]
      }
      stor.defSuite.rangInitial = [Number(stor.defSuite.termeInit[0][0])]
      if (!stor.niveau) stor.niveau = 'TS'
    } else {
      stor.niveau = (ds.niveau.toUpperCase() === 'TES') ? 'TES' : 'TS'
      if (stor.niveau === 'TES') {
        if (ds.q === '[-2.5;2.5]') {
          // c’est qu’on a laissé a par défaut. Donc il faut que je lui mette la valeur par défaut du niveau TES
          ds.q = '[0.5;2.5]'
        }
      }
      stor.defSuite.typeSuite = ds.typeSuite
      const intervalleDefaut = (stor.niveau === 'TES') ? '[0.5;2.5]' : '[-2.5;2.5]'
      const tabValInterdites = {}
      const nomVar = ['a', 'b', 'c', 'q']
      const tabDefaut = ['[-2;1.7]', '[-0.5;1]', '[-4;4]', intervalleDefaut]
      tabValInterdites.a = [0]
      tabValInterdites.b = [0]
      tabValInterdites.q = [0, 1, -1]
      tabValInterdites.c = (ds.typeSuite === 4) ? [0] : []
      // on génère de manière aléatoire les variables et données de l’exercice
      for (let k = 0; k < nomVar.length; k++) {
        stor.defSuite['coef' + nomVar[k].toUpperCase() + 'Donnees'] = constructionTabIntervalle(me.donneesSection[nomVar[k]], tabDefaut[k], ds.nbrepetitions, tabValInterdites[nomVar[k]])
        stor.defSuite['coef' + nomVar[k].toUpperCase()] = stor.defSuite['coef' + nomVar[k].toUpperCase() + 'Donnees'].expression
      }
      // gestion du rang initial
      stor.defSuite.rangInitial = []
      if (typeof (ds.rangInitial) === 'number') {
        for (let k = 1; k <= ds.nbrepetitions; k++) {
          stor.defSuite.rangInitial.push(ds.rangInitial)
        }
      } else if (typeof (ds.rangInitial) === 'string') {
        if (isNaN(Number(ds.rangInitial))) {
          for (let k = 1; k <= ds.nbrepetitions; k++) {
            if (k === ds.nbrepetitions && k > 1) {
              stor.defSuite.rangInitial.push(1)
            } else {
              stor.defSuite.rangInitial.push(0)
            }
          }
        } else {
          for (let k = 1; k <= ds.nbrepetitions; k++) {
            stor.defSuite.rangInitial.push(Number(ds.rangInitial))
          }
        }
      } else {
        for (let k = 0; k < ds.rangInitial.length; k++) {
          if (ds.rangInitial[k] === undefined) {
            if (k === ds.nbrepetitions - 1 && k > 0) {
              stor.defSuite.rangInitial.push(1)
            } else {
              stor.defSuite.rangInitial.push(0)
            }
          } else {
            stor.defSuite.rangInitial.push(Number(ds.rangInitial[k]))
          }
        }
      }
    }
    if (stor.defSuite.typeSuite === 2) {
      // on a une suite géométrique, donc coefB et coefC ne contiennent que des zéros
      for (let k = 0; k < ds.nbrepetitions; k++) {
        stor.defSuite.coefB[k] = 0
        stor.defSuite.coefC[k] = 0
      }
    } else if (stor.defSuite.typeSuite === 3) {
      // on a une suite arithmetique, donc coefA et coefQ ne contiennent que des zéros
      for (let k = 0; k < ds.nbrepetitions; k++) {
        stor.defSuite.coefA[k] = 0
        stor.defSuite.coefQ[k] = 0
      }
    } else if (stor.defSuite.typeSuite === 4) {
      // on a une suite arithmético-géométrique, donc coefB ne contient que des zéros
      for (let k = 0; k < ds.nbrepetitions; k++) {
        stor.defSuite.coefB[k] = 0
      }
    }
    me.logIfDebug('coefA:', stor.defSuite.coefA,
      '\nstor.defSuite.coefB:', stor.defSuite.coefB,
      '\nstor.defSuite.coefC:', stor.defSuite.coefC,
      '\nstor.defSuite.coefQ:', stor.defSuite.coefQ
    )
  }

  function enonceMain () {
    // me.donneesPersistantes.suites sert à envoyer des informations dans le bilan qui pourront être récupérées par une autre section sur les suites
    // Ceci a pour but de poursuivre l’étude de la suite définie dans cette section
    // (si on avait déjà récupéré defSuite dans donneesPersistantes on va remettre la même chose)
    // ATTENTION, il faut convertir typeSuite en un truc que les autres comprennent
    let typeSuite
    switch (stor.defSuite.typeSuite) {
      case 2:
        typeSuite = geometrique
        break
      case 3:
        typeSuite = arithmetique
        break
      default:
        typeSuite = arithGeo
    }
    me.donneesPersistantes.suites = { ...stor.defSuite, typeSuite }
    me.logIfDebug('me.donneesPersistantes.suites:', me.donneesPersistantes.suites)
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (stor.defSuite.rangInitial[me.questionCourante - 1] === undefined) stor.defSuite.rangInitial[me.questionCourante - 1] = 0
    // les variables qui suivent sont là pour me faciliter la tâche au niveau des écritures
    const q = stor.defSuite.coefQ[me.questionCourante - 1]
    const a = stor.defSuite.coefA[me.questionCourante - 1]
    const b = stor.defSuite.coefB[me.questionCourante - 1]
    const c = stor.defSuite.coefC[me.questionCourante - 1]
    me.logIfDebug('q:', q, '  a:', a, '  b:', b, '   c:', c)
    let aFoisQpuissanceN
    if (stor.defSuite.typeSuite !== 3) {
      // ma suite n’est pas arithmétique, on verra donc l’expression de la forme a*q^n
      // il faut alors que j'écrive q sous la bonne forme (parenthèses éventuelles)
      const afficheQ = ((String(q).indexOf('frac') > -1) || (String(q).charAt(0) === '-')) ? '\\left(' + q + '\\right)' : q
      const puissanceQ = (Number(stor.defSuite.rangInitial[me.questionCourante - 1]) === 0) ? 'n' : 'n-' + Number(stor.defSuite.rangInitial[me.questionCourante - 1])
      aFoisQpuissanceN = (Number(a) === 1) ? afficheQ + '^{' + puissanceQ + '}' : (Number(a) === -1) ? '-' + afficheQ + '^{' + puissanceQ + '}' : a + '\\times' + afficheQ + '^{' + puissanceQ + '}'
      // console.log('afficheQ:',afficheQ,'   puissanceQ:',puissanceQ)
      stor.afficheQ = afficheQ
    }
    stor.defSuite.nomU = (stor.defSuite.nomU === undefined) ? j3pGetRandomElts(['u', 'v', 'w'], 1)[0] : stor.defSuite.nomU
    let termeUn,
      relRecurrence,
      termInit
    if (stor.defSuite.typeSuite === 1) {
      // suite complète a*q^n+bn+c
      termeUn = aFoisQpuissanceN + j3pGetLatexMonome(2, 1, b, 'n') + j3pGetLatexMonome(3, 0, c, '')
    } else if (stor.defSuite.typeSuite === 2) {
      // suite géométrique a*q^n
      termeUn = aFoisQpuissanceN
      relRecurrence = j3pGetLatexMonome(1, 1, q, stor.defSuite.nomU + '_n')
      termInit = a
    } else if (stor.defSuite.typeSuite === 3) {
      // suite arithmétique bn+c
      termeUn = j3pGetLatexMonome(1, 1, b, 'n') + j3pGetLatexMonome(2, 0, c, '')
      relRecurrence = stor.defSuite.nomU + '_n' + j3pGetLatexMonome(2, 0, b, '')
      termInit = j3pGetLatexSomme(j3pGetLatexProduit(b, stor.defSuite.rangInitial[me.questionCourante - 1]), c)
    } else {
      // suite arithmético-géométrique a*q^n+c
      // je cherche b1 de l’écriture u_{n+1}=a1*un+b_1 (a_1 étant égal à q)
      const b1 = ConvertDecimal(j3pGetLatexProduit(c, j3pGetLatexSomme('1', j3pGetLatexOppose(q))), 2)
      termeUn = aFoisQpuissanceN + j3pGetLatexMonome(2, 0, c, '')
      relRecurrence = j3pGetLatexMonome(1, 1, q, stor.defSuite.nomU + '_n') + j3pGetLatexMonome(2, 0, b1, '')
      termInit = ConvertDecimal(j3pGetLatexSomme(a, j3pGetLatexSomme(j3pGetLatexProduit(b, stor.defSuite.rangInitial[me.questionCourante - 1]), c)), 2)
    }
    // console.log('termInit:',termInit,'  termeUn:',termeUn)
    let numIdSuivant = 2
    me.logIfDebug('stor.defSuite.typeSuite:', stor.defSuite.typeSuite, '  stor.defSuite.rangInitial:', stor.defSuite.rangInitial, '  stor.defSuite.nomU:', stor.defSuite.nomU)
    if (stor.defSuite.typeSuite === 1) {
      j3pAffiche(stor.zoneCons1, '', textes.consigne1_2,
        {
          u: stor.defSuite.nomU,
          n: stor.defSuite.rangInitial[me.questionCourante - 1],
          j: termeUn
        })
    } else {
      j3pAffiche(stor.zoneCons1, '', textes.consigne1_1,
        {
          u: stor.defSuite.nomU,
          n: stor.defSuite.rangInitial[me.questionCourante - 1],
          j: relRecurrence,
          i: termInit
        })
      j3pAffiche(stor.zoneCons2, '', textes.consigne2,
        {
          u: stor.defSuite.nomU,
          n: stor.defSuite.rangInitial[me.questionCourante - 1],
          t: termeUn
        })
      numIdSuivant = 3
    }
    j3pAffiche(stor['zoneCons' + numIdSuivant], '', textes.consigne3)
    numIdSuivant++
    j3pAffiche(stor['zoneCons' + numIdSuivant], '', textes.consigne4,
      {
        u: stor.defSuite.nomU,
        n: stor.defSuite.rangInitial[me.questionCourante - 1],
        o: Number(stor.defSuite.rangInitial[me.questionCourante - 1]) + 1,
        p: Number(stor.defSuite.rangInitial[me.questionCourante - 1]) + 2
      })
    numIdSuivant++
    stor.elt = j3pAffiche(stor['zoneCons' + numIdSuivant], '', textes.consigne5, { inputmq1: { texte: '' } })
    mqRestriction(stor.elt.inputmqList[0], '\\d,.-+*/n^()', { commandes: ['fraction', 'puissance'] })
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.elt.inputmqList[0].id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvent un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })
    stor.laPalette = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.laPalette, { paddingTop: '15px' })
    j3pPaletteMathquill(stor.laPalette, stor.elt.inputmqList[0], {
      liste: ['fraction', 'puissance']
    })
    stor.numIdSuivant = numIdSuivant
    if (stor.defSuite.typeSuite % 2 === 1) {
      // dans le cas d’une suite arithmétique (cas 1) ou a*q^n+bn+c (cas 3)
      stor.solutionAttendue = a + '*(1-(' + q + ')^(n+1-' + stor.defSuite.rangInitial[me.questionCourante - 1] + '))/(1-(' + q + '))+(' + b + ')*(n+' + stor.defSuite.rangInitial[me.questionCourante - 1] + ')*(n+1-' + stor.defSuite.rangInitial[me.questionCourante - 1] + ')/2+(' + c + ')*(n+1-' + stor.defSuite.rangInitial[me.questionCourante - 1] + ')'
    } else {
      stor.solutionAttendue = a + '*(1-(' + q + ')^(n+1-' + stor.defSuite.rangInitial[me.questionCourante - 1] + '))/(1-(' + q + '))+(' + c + ')*(n+1-' + stor.defSuite.rangInitial[me.questionCourante - 1] + ')'
    }

    if (stor.afficheAide) {
      afficheZoneAide()
    }
    j3pFocus(stor.elt.inputmqList[0])

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        j3pDetruitToutesLesFenetres()
      }
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si la zone a bien été complétée ou non
        // reponse.bonneReponse
        if (reponse.aRepondu) {
        // il faut aussi que je teste si le résultat donné est égale à ce qu’on attend
        // je ne teste en aucun cas une écriture simplifiée du résultat
          const repEleve = j3pValeurde(stor.elt.inputmqList[0])
          me.logIfDebug('repEleve1:' + repEleve)
          const repEleveXcas1 = j3pMathquillXcas(repEleve)
          const repAttendue = j3pMathquillXcas(stor.solutionAttendue)
          // console.log('repEleveXcas1:',repEleveXcas1,'   repAttendue:',repAttendue)
          reponse.bonneReponse = testExpressions(repEleveXcas1, repAttendue, 'n')
          // bonne_reponse2 ne prend en compte que les zones dans validePerso
          if (!reponse.bonneReponse) {
            fctsValid.zones.bonneReponse[0] = false
          // si les 2 zones de validePerso ne sont pas bonnes, on met false dans fctsValid.zones.bonneReponse[i] où i est la position de la zone dans le tableau mesZonesSaisie
          }
          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          fctsValid.coloreUneZone(stor.elt.inputmqList[0].id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.buttonsElts.sectionSuivante.addEventListener('click', j3pDetruitToutesLesFenetres)
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
