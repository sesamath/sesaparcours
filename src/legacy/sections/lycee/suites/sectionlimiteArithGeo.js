import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pGetRandomElts, j3pDetruit, j3pElement, j3pFocus, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pMonome, j3pNombre, j3pPaletteMathquill, j3pShowError, j3pVirgule, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexOppose, j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import { arithGeo, arithmetique, geometrique, normalizeTypeSuiteAg } from 'src/legacy/sections/lycee/suites/constantesSuites'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// on réexporte la fct d’upgrade avec arithGeo
export { upgradeParametresAg as upgradeParametres } from './constantesSuites'

/*
        DENIAUD Rémi
        Octobre 2016
        Dans cette section on demande la limite d’une suite arithmétique, géométrique ou arithmético-géométrique pour laquelle on donne le terme général
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['typeSuite', arithGeo, 'liste', 'La suite peut être arithmético-géométrique (par défaut et on passe alors par une suite intermédiaire), géométrique ou arithmétique.', [arithGeo, geometrique, arithmetique]],
    ['a', '[-1.1;2.1]', 'string', 'Intervalle où se trouve la valeur a dans l’expression u_{n+1}=au_n+b (bien sûr a non nul et avec ce type d’intervalle, il peut être décimal). Par défaut c’est "[0.5;1.7]" pour le niveau TES. S’il vaut 1, (u_n) est arithmétique. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.'],
    ['b', '[-0.5;1]', 'string', 'Intervalle où se trouve la valeur b dans l’expression u_{n+1}=au_n+b (avec ce type d’intervalle, il peut être décimal). S’il vaut 0, (u_n) est géométique. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.'],
    ['termeInitial', ['0|[0.4;5]', '0|[-4;-1]', '1|[1;6]'], 'array', 'Tableau de taille nbrepetitions où chaque terme contient deux éléments séparés par | : le premier est l’indice du premeir terme et le second sa valeur (imposée ou aléatoire dans un intervalle).'],
    ['niveau', 'TS', 'liste', 'prend la valeur "TS" (par défaut) ou "TES" (utile pour rédiger la correction).', ['TS', 'TES']],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la suite étudiée est celle du nœud précédent.']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo1: 'Limite d’une suite arithmético-géométrique',
  titre_exo2: 'Limite d’une suite arithmétique',
  titre_exo3: 'Limite d’une suite géométrique',
  // on donne les phrases de la consigne
  consigne1: 'Soit $(£u_n)$ la suite définie par $£u_{£n}=£i$ et, pour tout entier $n\\geq £n$, par $£u_{n+1}=£j$.',
  consigne2_1: '$(£u_n)$ est une suite géométrique de raison $£r$ et de premier terme $£u_{£n}=£i$.',
  consigne2_2: '$(£u_n)$ est une suite arithmétique de raison $£r$ et de premier terme $£u_{£n}=£i$.',
  consigne2_3: 'La suite $(£v_n)$ est définie, pour tout entier $n\\geq £n$, par $£v_n=£u_n£c$.',
  consigne3: '$(£v_n)$ est géométrique de raison $£r$ et de premier terme $£v_{£n}=£i$.',
  consigne4: 'Pour tout entier $n\\geq £n$, $£u_n=£t$.',
  consigne5: 'Cette suite $(£u_n)$ admet-elle une limite ?',
  consigne7: 'Attention, tu n’as qu’une tentative pour répondre !',
  cocher1: 'OUI',
  cocher2: 'NON',
  consigne6: 'Dans ce cas, $\\limite{n}{+\\infty}{£u_n}=$&1&',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Il faut sélectionner l’une des deux propositions !',
  comment2: 'Il faut être plus précis avec un signe + ou - !',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Comme $-1<£q<1$, $\\limite{n}{+\\infty}{£r^{£n}}=0$,',
  corr1_10: 'Comme $0<£q<1$, $\\limite{n}{+\\infty}{£r^{£n}}=0$,',
  corr1_2: 'Comme $£q<-1$, $\\limite{n}{+\\infty}{£r^{£n}}$ n’existe pas. Donc $(£u_n)$ n’admet pas de limite.',
  corr1_3: 'Comme $£q>1$, $\\limite{n}{+\\infty}{£r^{£n}}=+\\infty$.',
  corr2: 'On conclut que $\\limite{n}{+\\infty}{£u_n}=£l$.',
  corr3: 'Comme $£b£s0$, $\\limite{n}{+\\infty}{£u_n}=£l$.'
}

/**
 * section limiteArithGeo
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCor (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    if (j3pBoutonRadioChecked(stor.nameRadio)[0] === 0) {
      j3pDetruit(stor.laPalette)
    }
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let k = 1; k <= 3; k++) stor['zoneExpli' + k] = j3pAddElt(zoneExpli, 'div')
    if (stor.defSuite.typeSuite === arithmetique) {
      const leSigne = (stor.laLimite.charAt(0) === '-') ? '<' : '>'
      j3pAffiche(stor.zoneExpli1, '', textes.corr3,
        {
          b: stor.defSuite.b,
          s: leSigne,
          l: stor.laLimite,
          u: stor.defSuite.nomU
        })
    } else {
      const ecrireRaison = (String(stor.defSuite.a).indexOf('frac') > -1) ? '\\left(' + stor.defSuite.a + '\\right)' : (j3pCalculValeur(stor.defSuite.a) < 0) ? '\\left(' + stor.defSuite.a + '\\right)' : stor.defSuite.a
      const puissance = 'n' + j3pGetLatexMonome(2, 0, -stor.defSuite.termeInit[me.questionCourante - 1][0], '')
      if (stor.admetLimite) {
        if (String(stor.laLimite).indexOf('infty') > -1) {
          j3pAffiche(stor.zoneExpli1, '', textes.corr1_3,
            {
              r: ecrireRaison,
              q: stor.defSuite.a,
              n: puissance
            })
        } else {
          const textCorr = ((stor.defSuite.niveau === 'TES')) ? textes.corr1_10 : textes.corr1_1
          j3pAffiche(stor.zoneExpli1, '', textCorr,
            {
              r: ecrireRaison,
              q: stor.defSuite.a,
              n: puissance
            })
        }
        j3pAffiche(stor.zoneExpli2, '', textes.corr2,
          {
            u: stor.defSuite.nomU,
            l: stor.laLimiteTexte
          })
      } else {
        j3pAffiche(stor.zoneExpli1, '', textes.corr1_2,
          {
            u: stor.defSuite.nomU,
            r: ecrireRaison,
            q: stor.defSuite.a,
            n: puissance
          })
      }
    }
  }

  function genereAlea (int) {
    // int est un intervalle
    // mais on peut trouver des cas de figure où par faignantise, on aura utilisé cette fonction sans vérifier que int est un intervalle
    // Ce serait alors un nombre
    if (String(int).charAt(0) === '[') {
      // si les bornes sont entières, alors on génère un nombre entier aléatoire dans cet intervalle
      // si elles sont décimales, il faut que je génère un décimal de l’intervalle
      const PosPointVirgule = int.indexOf(';')
      let val1 = j3pNombre(int.substring(1, PosPointVirgule))
      let val2 = j3pNombre(int.substring(PosPointVirgule + 1, int.length - 1))
      let posPoint = -1
      if ((String(val1).indexOf('.') === -1)) {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = String(val2).length - String(val2).lastIndexOf('.') - 1
        }
      } else {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = Math.max(String(val1).length - String(val1).lastIndexOf('.') - 1, String(val2).length - String(val2).lastIndexOf('.') - 1)
        } else {
          posPoint = String(val1).length - String(val1).lastIndexOf('.') - 1
        }
      }
      if (val1 > val2) {
        // les bornes ne sont pas dans le bon ordre
        const val3 = val1
        val1 = val2
        val2 = val3
      }
      if (posPoint === -1) {
        // je n’ai que des nombres entiers
        const [minInt, maxInt] = j3pGetBornesIntervalle(int)
        return j3pGetRandomInt(minInt, maxInt)
      } else {
        // j’ai des décimaux
        const borneInf = val1 * Math.pow(10, posPoint)
        const borneSup = val2 * Math.pow(10, posPoint)
        const nbAlea = j3pGetRandomInt(String(borneInf), String(borneSup)) * Math.pow(10, -posPoint)
        return Math.round(nbAlea * Math.pow(10, 10)) / Math.pow(10, 10)
      }
    } else {
      return int
    }
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut, nbRepet, tabValInterdites) {
    // cette fonction permet de vérifier si ce qui est renseigné est bien un intervalle ou un nombre (éventuellement fractionnaire) et ce plusieurs fois (en général nbrepetitions)
    // cela permet de valider ce qui peut être saisi par un utilisateur dans le cas où on peut imposer des valeurs
    // texteIntervalle est une chaîne où les éléments peuvent être séparés par '|' et où chaque élément est un nombre, une fraction ou un intervalle
    // si c’est un intervalle, la fonction va dans un second temps générer de manière aléatoire une valeur de cet intervalle
    // il peut y avoir des valeurs interdites précisées dans tabValInterdites, même si elles peuvent être dans l’intervalle
    // la fonction renvoie un tableau de nbRepet éléments
    // chaque élément est un intervalle voire l’intervalle par défaut si ce qui est précisé n’est aps compris
    // ce peut aussi être un nombre entier, décimal ou fractionnaire
    // dans ce dernier cas, on renvoit une chaine où on retrouve le code latex et la valeur décimale (arrondie) séparés par |

    /* cette fonction renvoie un objet. Cet objet contient 3 tableaux de nbRepet éléments
        - leType[k] vaut 'nombre', 'fraction' ou 'intervalle' suivant la valeur de texteIntervalle
        - expression contient l’écriture de chaque valeur imposée ou gérée aléatoirement quand leType[k] vaut 'intervalle'
        - valeur contient la valeur décimale de ces nombres, cela ne peut être utile que lorsque leType[k] vaut 'fraction'
        */
    function lgIntervalle (intervalle) {
      // cette fonction renvoie la longueur de l’intervalle'
      // si ce n’est pas un intervalle, alors elle renvoie 0'
      let lg = 0
      // var intervalleReg = new RegExp("\\[\\-?[0-9]{1,};\\-?[0-9]{1,}\\]","i");
      const intervalleReg = /\[-?[0-9]+(.|,)?[0-9]*;-?[0-9]+(.|,)?[0-9]*]/g // new RegExp('\\[\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,};\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,}\\]', 'i')
      if (intervalleReg.test(intervalle)) {
        // on a bien un intervalle
        const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
        const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
        lg = borneSup - borneInf
      }
      return lg
    }

    function nbDansTab (nb, tab) {
      // test si la valeur correspondant à nb est dans le tableau tab (ses valeurs pouvant être au format latex)
      if (tab === undefined) {
        return false
      } else {
        let puisPrecision = 15
        if (Math.abs(nb) > Math.pow(10, -13)) {
          const ordrePuissance = Math.floor(Math.log(Math.abs(nb)) / Math.log(10)) + 1
          puisPrecision = 15 - ordrePuissance
        }
        for (let k = 0; k < tab.length; k++) {
          if (Math.abs(nb - j3pCalculValeur(tab[k])) < Math.pow(10, -puisPrecision)) {
            return true
          }
        }
        return false
      }
    }

    const intervalleReg = /\[-?[0-9]+(.|,)?[0-9]*;-?[0-9]+(.|,)?[0-9]*]/g// new RegExp('\\[\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,};\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,}\\]', 'i')
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const DonneesIntervalle = {}
    // DonneesIntervalle contient trois propriétés qui sont des tableaux de longueur nbRepet :
    // - leType : qui correspond à ce qui a été renseigné initialement ('intervalle', 'nombre', 'fraction') - 'nombre' désigne un entier ou un décimal
    // - expression : qui est le nombre (si type==='nombre'), une valeur générée aléatoirement dans un intervalle (si type==='intervalle'), l’écriture l’atex de la fraction (si type==='fraction')
    // - valeur : qui est la valeur décimale de l’expression (utile surtout si type==='fraction')
    DonneesIntervalle.leType = []
    DonneesIntervalle.expression = []
    DonneesIntervalle.valeur = []
    let nbAlea, valNb, tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).indexOf('|') > -1) {
      const tableau = texteIntervalle.split('|')
      for (k = 0; k < nbRepet; k++) {
        if (intervalleReg.test(tableau[k])) {
          // c’est bien un intervalle'
          if (lgIntervalle(tableau[k]) === 0) {
            // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
            valNb = tableau[k].substring(1, tableau[k].indexOf(';'))
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          } else {
            do {
              nbAlea = genereAlea(tableau[k])
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        } else {
          // peut être est-ce tout simplement un nombre :
          if ((tableau[k] === undefined) || (tableau[k] === '')) {
            // là l’expression n’est pas renseignée
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          } else if (nbFracReg.test(tableau[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tableau[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
            DonneesIntervalle.leType.push('fraction')
            DonneesIntervalle.valeur.push(valFrac)
          } else if (!isNaN(j3pNombre(tableau[k]))) {
            // on a un nombre
            DonneesIntervalle.expression.push(tableau[k])
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(tableau[k])
          } else {
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      }
    } else {
      if ((texteIntervalle === undefined) || (texteIntervalle === '')) {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      } else if (intervalleReg.test(String(texteIntervalle))) {
        if (lgIntervalle(String(texteIntervalle)) === 0) {
          // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
          valNb = String(texteIntervalle).substring(1, String(texteIntervalle).indexOf(';'))
          for (k = 0; k < nbRepet; k++) {
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          }
        } else {
          for (k = 0; k < nbRepet; k++) {
            do {
              nbAlea = genereAlea(texteIntervalle)
            } while (nbDansTab(nbAlea, tabValInterdites))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
          DonneesIntervalle.leType.push('fraction')
          DonneesIntervalle.valeur.push(valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push(String(texteIntervalle))
          DonneesIntervalle.leType.push('nombre')
          DonneesIntervalle.valeur.push(String(texteIntervalle))
        }
      } else {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabValInterdites))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      }
    }
    return DonneesIntervalle
  }

  function ConvertDecimal (nb, nbreDecimales) {
    // nb peut être un entier, un décimal ou un nb fractionnaire
    // si c’est un nb fractionnaire et que la forme décimale contient moins de nbreDecimales, alors on renvoie le nb sous forme décimale
    // sinon on renvoit nb sous son format initial
    const nbDecimal = j3pCalculValeur(nb)
    // pour éviter les pbs d’arrondi, je cherche par quelle puissance de 10 multiplier mon nombre (puis el divisier)
    let puisPrecision = 15
    if (Math.abs(nbDecimal) > Math.pow(10, -13)) {
      const ordrePuissance = Math.floor(Math.log(Math.abs(nbDecimal)) / Math.log(10)) + 1
      puisPrecision = 15 - ordrePuissance
    }
    let newNb = nb
    if (Math.abs(Math.round(nbDecimal * Math.pow(10, puisPrecision)) / Math.pow(10, puisPrecision) - nbDecimal) < Math.pow(10, -12)) {
      // on a une valeur décimale
      // Si elle a plus de deux chiffres après la virgule, on donne une valeur décimale
      const nbChiffreApresVirgule = String(nbDecimal).length - 1 - String(nbDecimal).lastIndexOf('.')
      if (nbChiffreApresVirgule <= nbreDecimales) {
        newNb = nbDecimal
      }
    }
    return newNb
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (!ds.donneesPrecedentes) {
      // si je n’ai pas de section précédente, je génère le nom de la suite (u_n) de manière aléatoire
      stor.defSuite.nomU = j3pGetRandomElts(['u', 'v'], 1)[0]
      do {
        stor.defSuite.nomV = j3pGetRandomElts(['t', 'x', 'y', 'z'], 1)[0]
      } while (stor.defSuite.nomU === stor.defSuite.nomV)// ce dernier cas peut se produire si on a une section précédente
    }// je mets mes valeurs dans une variable plus simple :
    stor.defSuite.a = stor.defSuite.coefA[me.questionCourante - 1]
    stor.defSuite.b = stor.defSuite.coefB[me.questionCourante - 1]
    // me.donneesPersistantes.suites sert à envoyer des informations dans le bilan qui pourront être récupérées par une autre section sur les suites
    // Ceci a pour but de poursuivre l’étude de la suite définie dans cette section
    me.donneesPersistantes.suites = { ...stor.defSuite }
    me.logIfDebug('me.donneesPersistantes.suites:', me.donneesPersistantes.suites)
    const relRecurrence = (stor.defSuite.typeSuite === arithmetique)
      ? stor.defSuite.nomU + '_n' + j3pGetLatexMonome(2, 0, stor.defSuite.b, '')
      : (stor.defSuite.typeSuite === geometrique)
          ? j3pGetLatexMonome(1, 1, stor.defSuite.a, stor.defSuite.nomU + '_n')
          : j3pGetLatexMonome(1, 1, stor.defSuite.a, stor.defSuite.nomU + '_n') + j3pGetLatexMonome(2, 0, stor.defSuite.b, '')

    j3pAffiche(stor.zoneCons1, '', textes.consigne1,
      {
        u: stor.defSuite.nomU,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        i: stor.defSuite.termeInit[me.questionCourante - 1][1],
        j: relRecurrence
      })
    let affichageRaison
    if (stor.defSuite.typeSuite !== arithmetique) {
      affichageRaison = (String(stor.defSuite.a).indexOf('frac') > -1)
        ? '\\left(' + stor.defSuite.a + '\\right)'
        : (j3pNombre(stor.defSuite.a) < 0)
            ? '\\left(' + stor.defSuite.a + '\\right)'
            : stor.defSuite.a
    }
    let numIdSuivant = 3
    let vn
    if (stor.defSuite.typeSuite === arithmetique) {
      j3pAffiche(stor.zoneCons2, '', textes.consigne2_2,
        {
          u: stor.defSuite.nomU,
          r: stor.defSuite.b,
          n: stor.defSuite.termeInit[me.questionCourante - 1][0],
          i: stor.defSuite.termeInit[me.questionCourante - 1][1]
        })
      vn = (Math.abs(j3pCalculValeur(stor.defSuite.u0)) < Math.pow(10, -12))
        ? j3pGetLatexMonome(1, 1, stor.defSuite.b, 'n') + j3pGetLatexMonome(2, 0, j3pGetLatexOppose(j3pGetLatexProduit(stor.defSuite.termeInit[me.questionCourante - 1][0], stor.defSuite.b)))
        : j3pGetLatexMonome(1, 1, stor.defSuite.b, 'n') + j3pGetLatexMonome(2, 0, j3pGetLatexSomme(stor.defSuite.termeInit[me.questionCourante - 1][1], j3pGetLatexOppose(j3pGetLatexProduit(stor.defSuite.termeInit[me.questionCourante - 1][0], stor.defSuite.b))), '')
    } else if (stor.defSuite.typeSuite === geometrique) {
      j3pAffiche(stor.zoneCons2, '', textes.consigne2_1,
        {
          u: stor.defSuite.nomU,
          r: stor.defSuite.a,
          n: stor.defSuite.termeInit[me.questionCourante - 1][0],
          i: stor.defSuite.termeInit[me.questionCourante - 1][1]
        })
      stor.defSuite.v0 = stor.defSuite.termeInit[me.questionCourante - 1][1]
      vn = (Math.abs(j3pCalculValeur(stor.defSuite.termeInit[me.questionCourante - 1][1]) - 1) < Math.pow(10, -12))
        ? affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
        : (Math.abs(j3pCalculValeur(stor.defSuite.termeInit[me.questionCourante - 1][1]) + 1) < Math.pow(10, -12))
            ? '-' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
            : stor.defSuite.termeInit[me.questionCourante - 1][1] + '\\times' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
    } else { // suite arithmetico-geometrique
      // je prends la suite intermédiaire v_n=u_n-b/(1-a). Je note c=-b/(1-a)
      stor.defSuite.c = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.b), j3pGetLatexSomme('1', j3pGetLatexOppose((stor.defSuite.a))))
      // si c est une valeur décimale simple, alors je l’affiche sous forme décimale
      stor.defSuite.c = ConvertDecimal(stor.defSuite.c, 2)
      j3pAffiche(stor.zoneCons2, '', textes.consigne2_3,
        {
          u: stor.defSuite.nomU,
          v: stor.defSuite.nomV,
          r: stor.defSuite.a,
          n: stor.defSuite.termeInit[me.questionCourante - 1][0],
          c: j3pGetLatexMonome(2, 0, stor.defSuite.c, '')
        })
      stor.defSuite.v0 = j3pGetLatexSomme(stor.defSuite.termeInit[me.questionCourante - 1][1], stor.defSuite.c)
      j3pAffiche(stor.zoneCons3, '', textes.consigne3,
        {
          v: stor.defSuite.nomV,
          r: stor.defSuite.a,
          n: stor.defSuite.termeInit[me.questionCourante - 1][0],
          i: stor.defSuite.v0
        })
      vn = (Math.abs(j3pCalculValeur(stor.defSuite.v0) - 1) < Math.pow(10, -12))
        ? affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
        : (Math.abs(j3pCalculValeur(stor.defSuite.v0) + 1) < Math.pow(10, -12))
            ? '-' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
            : stor.defSuite.v0 + '\\times' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
      vn = vn + j3pGetLatexMonome(2, 0, j3pGetLatexOppose(stor.defSuite.c), '')
      numIdSuivant = 4
    }
    j3pAffiche(stor['zoneCons' + numIdSuivant], '', textes.consigne4,
      {
        u: stor.defSuite.nomU,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        t: vn
      })
    numIdSuivant++
    j3pAffiche(stor['zoneCons' + numIdSuivant], '', textes.consigne5,
      { u: stor.defSuite.nomU })
    numIdSuivant++
    if (ds.nbchances === 1) {
      j3pAffiche(stor['zoneCons' + numIdSuivant], '', textes.consigne7, { style: { fontStyle: 'italic' } })
      numIdSuivant++
    }
    const espacetxt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
    stor.idsRadio = [j3pGetNewId('radio1'), j3pGetNewId('radio2')]
    stor.idsLabelRadio = [j3pGetNewId('labelRadio'), j3pGetNewId('labelRadio')]
    stor.nameRadio = j3pGetNewId('choix')
    j3pBoutonRadio(stor['zoneCons' + numIdSuivant], stor.idsRadio[0], stor.nameRadio, 0, textes.cocher1)
    j3pAffiche(stor['zoneCons' + numIdSuivant], '', espacetxt + espacetxt)
    j3pBoutonRadio(stor['zoneCons' + numIdSuivant], stor.idsRadio[1], stor.nameRadio, 1, textes.cocher2)
    stor.zoneVisibles = false
    numIdSuivant++
    stor.num = numIdSuivant
    j3pElement(stor.idsRadio[0]).onclick = function () {
      if (!stor.zoneVisibles) {
        stor.zoneConsSup = j3pAddElt(stor.conteneur, 'div')
        stor.elt = j3pAffiche(stor.zoneConsSup, '', textes.consigne6,
          {
            u: stor.defSuite.nomU,
            inputmq1: { texte: '' }
          })
        stor.zoneConsSup2 = j3pAddElt(stor.conteneur, 'div')
        stor.laPalette = j3pAddElt(stor.zoneConsSup2, 'div')
        j3pPaletteMathquill(stor.laPalette, stor.elt.inputmqList[0], {
          liste: ['fraction', 'inf']
        })
        // palette MQ :
        mqRestriction(stor.elt.inputmqList[0], '\\d,./-+', { commandes: ['fraction', 'inf'] })
        stor.zoneVisibles = true
        j3pFocus(stor.elt.inputmqList[0])
        const mesZonesSaisie = [stor.elt.inputmqList[0].id]
        stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
      }
    }
    j3pElement(stor.idsRadio[1]).onclick = function () {
      if (stor.zoneVisibles) {
        j3pDetruit(stor.zoneConsSup)
        j3pDetruit(stor.zoneConsSup2)
        stor.zoneVisibles = false
      }
    }
    // je mets de côté la réponse attendue
    stor.admetLimite = true
    stor.typeLimite = 'nombre'
    if (stor.defSuite.typeSuite === arithmetique) {
      if (j3pCalculValeur(stor.defSuite.b) < 0) {
        stor.laLimite = '-\\infty'
        stor.laLimiteTexte = '-\\infty'
      } else {
        stor.laLimite = stor.laLimiteTexte = '+\\infty'
      }
      stor.typeLimite = 'texte'
    } else {
      if (j3pCalculValeur(stor.defSuite.a) <= -1 + Math.pow(10, -12)) {
        stor.admetLimite = false
      } else if (j3pCalculValeur(stor.defSuite.a) > 1 + Math.pow(10, -12)) {
        stor.laLimite = (j3pCalculValeur(stor.defSuite.v0) < -Math.pow(10, -12)) ? '-\\infty' : '+\\infty'
        stor.typeLimite = 'texte'
        stor.laLimiteTexte = stor.laLimite
      } else {
        // la sujet admet une limite finie
        stor.laLimite = (stor.defSuite.typeSuite === geometrique)
          ? 0
          : j3pCalculValeur(j3pGetLatexOppose(stor.defSuite.c))
        stor.laLimiteTexte = (stor.defSuite.typeSuite === geometrique)
          ? 0
          : j3pGetLatexOppose(stor.defSuite.c)
      }
    }

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage('presentation1')
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.defSuite = {}
        if (ds.donneesPrecedentes && !me.donneesPersistantes.suites) {
          ds.donneesPrecedentes = false
          j3pShowError(Error('Les données précédentes n’ont pu être récupérées. De nouvelles sont générées !\nAttention car la nature de la suite n’est peut-être pas celle qui était attendue.'))
        }
        if (ds.donneesPrecedentes) {
          // si on récupère des données précédentes il faut imposer une seule répétition (on veut bosser sur la suite précédente, aucune autre générée aléatoirement)
          if (ds.nbrepetitions !== 1) {
            console.error('Pb de paramétrage, imposer donneesPrecedentes avec plusieurs répétitions n’a pas de sens')
            me.surcharge({ nbrepetitions: 1 })
          }
          // on récupère tout ce que ça contient pour le mettre dans stor
          stor.defSuite = { ...me.donneesPersistantes.suites }

          // on blinde typeSuite, arithGeo par défaut
          if (![arithmetique, geometrique].includes(normalizeTypeSuiteAg(stor.defSuite.typeSuite, { lax: true }))) {
            stor.defSuite.typeSuite = arithGeo
          }

          stor.defSuite.coefA = [stor.defSuite.a]
          stor.defSuite.coefB = [stor.defSuite.b]
          stor.defSuite.termeInit[0][1] = stor.defSuite.u0
          stor.defSuite.termeInit[0][0] = Number(stor.defSuite.termeInit[0][0])
          if (!stor.defSuite.niveau) stor.defSuite.niveau = 'TS'
        } else {
          stor.defSuite.niveau = (ds.niveau.toUpperCase() === 'TES') ? 'TES' : 'TS'
          if (stor.defSuite.niveau === 'TES') {
            if (ds.a === '[-1.1;2.1]') {
              // c’est qu’on a laissé a par défaut. Donc il faut que je lui mette la valeur par défaut du niveau TES
              ds.a = '[0.5;1.7]'
            }
          }
          if (ds.typeSuite === arithmetique) {
            stor.defSuite.typeSuite = arithmetique
          } else if (ds.typeSuite === geometrique) {
            stor.defSuite.typeSuite = geometrique
          } else {
            stor.defSuite.typeSuite = arithGeo
          }
        }
        const leTitreExo = (stor.defSuite.typeSuite === arithGeo)
          ? textes.titre_exo1
          : (stor.defSuite.typeSuite === arithmetique)
              ? textes.titre_exo2
              : textes.titre_exo3
        me.afficheTitre(leTitreExo)
        if (!ds.donneesPrecedentes) {
          // on génère de manière aléatoire les variables et données de l’exercice
          const intervalleDefaut = (stor.defSuite.niveau === 'TES') ? '[0.5;1.7]' : '[-1.1;2.1]'
          stor.coefADonnees = constructionTabIntervalle(ds.a, intervalleDefaut, ds.nbrepetitions, [0, 1, -1])
          stor.coefBDonnees = constructionTabIntervalle(ds.b, '[-0.5;1]', ds.nbrepetitions, [0])
          stor.defSuite.coefA = stor.coefADonnees.expression
          stor.defSuite.coefB = stor.coefBDonnees.expression

          // il faut que je gère le terme initial car j’ai un tableau dont les données sont de la forme 1|[...;...] ce second point pouvant être un entier ou un nb fractionnaire
          stor.defSuite.termeInit = []
          const TermeInitParDefaut = [0, '[0.4;5]']
          let onRecommence, k, termeU0, termeC, termeV0
          if (!isNaN(j3pNombre(String(ds.termeInitial)))) {
            // au lieu d’un tableau, n’est indiqué qu’un seul nombre. Celui-ci devient le rang initial de chaque répétition
            const leRangInit = j3pNombre(ds.termeInitial)
            for (k = 0; k < ds.nbrepetitions; k++) {
              onRecommence = false
              do {
                termeU0 = genereAlea(TermeInitParDefaut[1])
                if (stor.defSuite.typeSuite === arithGeo) {
                  // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                  termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                  termeV0 = j3pGetLatexSomme(termeU0, termeC)
                  onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
                }
              } while (onRecommence)
              stor.defSuite.termeInit.push([leRangInit, termeU0])
            }
          }
          for (k = 0; k < ds.nbrepetitions; k++) {
            if ((ds.termeInitial[k] === undefined) || (ds.termeInitial[k] === '')) {
              // on a rien renseigné donc par défaut, on met
              onRecommence = false
              do {
                termeU0 = genereAlea(TermeInitParDefaut[1])
                if (stor.defSuite.typeSuite === arithGeo) {
                  // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                  termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                  termeV0 = j3pGetLatexSomme(termeU0, termeC)
                  onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
                }
              } while (onRecommence)
              stor.defSuite.termeInit.push([TermeInitParDefaut[0], termeU0])
            } else {
              // il faut la barre verticale pour séparer le rang et la valeur du terme initial
              const posBarre = String(ds.termeInitial[k]).indexOf('|')
              if (posBarre === -1) {
                const EntierReg = /[0-9]+/g // new RegExp('[0-9]{1,}', 'i')
                // je n’ai pas la barre verticale
                // peut-être ai-je seulement l’indice du premier terme
                if (EntierReg.test(ds.termeInitial[k])) {
                  // c’est un entier correspondant à l’indice du terme initial
                  onRecommence = false
                  do {
                    termeU0 = genereAlea(TermeInitParDefaut[1])
                    if (stor.defSuite.typeSuite === arithGeo) {
                      // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                      termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                      termeV0 = j3pGetLatexSomme(termeU0, termeC)
                      onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
                    }
                  } while (onRecommence)
                  stor.defSuite.termeInit.push([ds.termeInitial[k], termeU0])
                } else {
                  // donc je réinitialise avec la valeur par défaut
                  onRecommence = false
                  do {
                    termeU0 = genereAlea(TermeInitParDefaut[1])
                    if (stor.defSuite.typeSuite === arithGeo) {
                      // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                      termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                      termeV0 = j3pGetLatexSomme(termeU0, termeC)
                      onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
                    }
                  } while (onRecommence)
                  stor.defSuite.termeInit.push([TermeInitParDefaut[0], termeU0])
                }
              } else {
                // la valeur (éventuellement aléatoire) du terme initial
                onRecommence = false
                let termInitAlea
                let nbTentatives = 0
                do {
                  termInitAlea = constructionTabIntervalle(ds.termeInitial[k].substring(posBarre + 1), TermeInitParDefaut[1], 1).expression[0]
                  if (stor.defSuite.typeSuite === arithGeo) {
                    // dans ce cas, il ne faudrait pas que u0+c soit égal à 0 sinon la suite deviendrait constante
                    termeC = j3pGetLatexQuotient(j3pGetLatexOppose(stor.defSuite.coefB[k]), j3pGetLatexSomme('1', j3pGetLatexOppose(stor.defSuite.coefA[k])))
                    termeV0 = j3pGetLatexSomme(termInitAlea, termeC)
                    onRecommence = (Math.abs(j3pCalculValeur(termeV0)) < Math.pow(10, -12))
                    nbTentatives++
                    if (nbTentatives === 50) {
                      // c’est que l’utilisateur a imposé une valeur initiale qui génère une suite constante
                      // Je choisis de regénérer la valeur de u0 en lui donnant l’intervalle par défaut
                      const nbInit = j3pNombre(termInitAlea)
                      const signe1 = (nbInit > 0) ? 1 : -1
                      const borneInf = Math.round(100000 * (nbInit - signe1 * nbInit / 10) / 100000)
                      const borneSup = Math.round(100000 * (nbInit + signe1 * nbInit / 10) / 100000)
                      ds.termeInitial[k] = ds.termeInitial[k].substring(0, posBarre) + '|[' + j3pVirgule(borneInf) + ';' + j3pVirgule(borneSup) + ']'
                      console.error(Error(`Les données imposées génèrent une boucle infinie - on tombe sur une suite constante, ce qui ne devrait pas être le cas. Je remets une valeur aléatoire dans [${borneInf};${borneSup}]`))
                    }
                  }
                  // console.log('onRecommence:',onRecommence,'  termeV0:',termeV0)
                } while (onRecommence && nbTentatives < 100)
                if (nbTentatives >= 100) {
                  console.error(Error('Problème avec le terme initial de la suite'))
                }
                // au cas où je vérifie que la première valeur est bien entière
                let rangInit = ds.termeInitial[k].substring(0, posBarre)
                rangInit = (String(j3pNombre(rangInit)).indexOf('.') === -1) ? rangInit : TermeInitParDefaut[0]
                stor.defSuite.termeInit.push([rangInit, termInitAlea])
              }
            }
          }
        }
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        let reponse = {
          aRepondu: false, bonneReponse: false
        }
        reponse.aRepondu = (j3pBoutonRadioChecked(stor.nameRadio)[0] > -1)
        const numRadioSelect = j3pBoutonRadioChecked(stor.nameRadio)[0] + 1
        const boutonCoche = reponse.aRepondu
        let infiniSansSigne = false
        if (reponse.aRepondu) {
        // un bouton radio a été sélectionné
          if (j3pBoutonRadioChecked(stor.nameRadio)[0] === 0) {
            if (j3pValeurde(stor.elt.inputmqList[0]) === '\\infty') {
            // il ne me donne pas une réponse complète
              infiniSansSigne = true
              reponse.aRepondu = false
            } else {
              const fctsValid = stor.fctsValid
              // si l’élève clique sur "oui" et que c’est la bonne réponse, il faut valider la zone de saisie'
              if (stor.admetLimite) {
                stor.elt.inputmqList[0].typeReponse = [stor.typeLimite, 'exact']
                stor.elt.inputmqList[0].reponse = [stor.laLimite]
                reponse = fctsValid.validationGlobale()
              } else {
                ds.nbchances--
                fctsValid.zones.bonneReponse[0] = false
                fctsValid.coloreUneZone(stor.elt.inputmqList[0].id)
                reponse.bonneReponse = false
              }
            }
          } else {
            reponse.bonneReponse = (!stor.admetLimite)
            if (!reponse.bonneReponse) {
              ds.nbchances--
            }
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (infiniSansSigne) {
            msgReponseManquante = textes.comment2
          } else if (!boutonCoche) {
            msgReponseManquante = textes.comment1
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            j3pDesactive(stor.idsRadio[0])
            j3pDesactive(stor.idsRadio[1])
            afficheCor(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCor(false)
              j3pDesactive(stor.idsRadio[0])
              j3pDesactive(stor.idsRadio[1])
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                j3pDesactive(stor.idsRadio[0])
                j3pDesactive(stor.idsRadio[1])
                if ((numRadioSelect === 1) && !stor.admetLimite) {
                // il  sélectionné le premier bouton radio à tord
                  j3pBarre('label' + stor.idsRadio[0])
                }
                if ((numRadioSelect === 2) && stor.admetLimite) {
                // il  sélectionné le deuxième bouton radio à tord
                  j3pBarre('label' + stor.idsRadio[1])
                }
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCor(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
