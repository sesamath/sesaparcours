import $ from 'jquery'
import { j3pAddElt, j3pAjouteFoisDevantX, j3pGetRandomElts, j3pShuffle, j3pDetruit, j3pElement, j3pEmpty, j3pExtraireCoefsFctAffine, j3pFocus, j3pFractionLatex, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pNombre, j3pPaletteMathquill, j3pPGCD, j3pGetBornesIntervalle, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { testIntervalleFermeDecimaux } from 'src/lib/utils/regexp'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pExtraireNumDen, j3pGetLatexOppose, j3pGetLatexProduit, j3pGetLatexPuissance, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import { aleatoire, arithGeo, arithmetique, geometrique } from 'src/legacy/sections/lycee/suites/constantesSuites'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

const regRestric = /[\d,./\-*n^+)(]/

// on réexporte la fct d’upgrade avec arithGeo
export { upgradeParametresAg as upgradeParametres } from './constantesSuites'

/*
    Rémi DENIAUD
    octobre 2016
    Cette section teste la détermination du terme général d’une suite qu’elle soit :
        - arithmétique
        - géométrique
        - arithmético-géométrique
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeSuite', arithGeo, 'liste', 'La suite peut être arithmético-géométrique (par défaut et on passe alors par une suite intermédiaire), géométrique, arithmétique ou de manière aléatoire entre arithmétique et géométrique.', [arithGeo, geometrique, arithmetique, aleatoire]],
    ['a', '[-1.1;2.1]', 'string', 'Intervalle où se trouve la valeur a dans l’expression u_{n+1}=au_n+b (bien sûr a non nul et avec ce type d’intervalle, il peut être décimal). Par défaut c’est "[0.5;1.7]" pour le niveau TES. S’il vaut 1, (u_n) est arithmétique. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.'],
    ['b', '[-0.5;1]', 'string', 'Intervalle où se trouve la valeur b dans l’expression u_{n+1}=au_n+b (avec ce type d’intervalle, il peut être décimal). S’il vaut 0, (u_n) est géométique. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.'],
    ['termeInitial', ['0|[0.4;5]', '0|[-4;-1]', '1|[1;6]'], 'array', 'Tableau de taille nbrepetitions où chaque terme contient deux éléments séparés par | : le premier est l’indice du premeir terme et le second sa valeur (imposée ou aléatoire dans un intervalle).'],
    ['UnDirect', true, 'boolean', 'Paramètre seulement valable pour une suite aithmético-géométrique : si "true", on ne demande pas l’expression de la suite géométrique intermédiaire.'],
    ['afficherPhrase', true, 'boolean', 'Afficher dans l’énoncé une phrase de la forme "(u_n) est géométrique de raison ..."'],
    ['niveau', 'TS', 'liste', 'prend la valeur "TS" (par défaut) ou "TES" (utile pour rédiger la correction).', ['TS', 'TES']],
    ['donneesPrecedentes', false, 'boolean', 'Lorsque ce paramètre vaut Vrai, alors la suite étudiée est celle du nœud précédent.']
  ]
}

const textes = {
  titre_exo: 'Terme général d’une suite',
  consigne1: 'Soit $(£u_n)$ la suite définie par $£u_{£n}=£i$ et, pour tout entier $n\\geq £n$, par $£u_{n+1}=£j$.',
  consigne2_1: '$(£u_n)$ est une suite géométrique de raison $£r$ et de premier terme $£u_{£n}=£i$.',
  consigne2_2: '$(£u_n)$ est une suite arithmétique de raison $£r$ et de premier terme $£u_{£n}=£i$.',
  consigne2_3: 'La suite $(£v_n)$ est définie, pour tout entier $n\\geq £n$, par $£v_n=£u_n£c$.',
  consigne3: '$(£v_n)$ est géométrique de raison $£r$ et de premier terme $£v_{£n}=£i$.',
  consigne4_1: 'Ainsi, pour tout entier $n\\geq £n$, $£u_n=$&1&.',
  consigne4_2: 'Ainsi, pour tout entier $n\\geq £n$, $£v_n=$&1&\ret $£u_n=$&2&.',
  comment1: 'La réponse est correcte mais elle doit être donnée sous forme simplifiée !',
  comment2: ' Chaque réponse doit être simplifiée !',
  corr1: '$(£v_n)$ étant géométrique de raison $q=£r$ et de premier terme $£v_{£n}=£i$, on a pour tout entier $n\\geq £n$, $£v_n=£v_{£n}\\times q^{£p}=£w$.',
  corr2: 'De plus, $£v_n=£u_n£c$ ce qui donne $£u_n=£v_n£d=£w£d$.',
  corr3: '$(£v_n)$ étant arithmétrique de raison $r=£r$ et de premier terme $£v_{£n}=£i$, on a pour tout entier $n\\geq £n$, $£v_n=£v_{£n}+r£p=£w$.',
  corr4: '$(£v_n)$ étant géométrique de raison $q=£r$ et de premier terme $£v_{£n}=£i$, on a pour tout entier $n\\geq £n$, $£v_n=£v_{£n}\\times q^{£p}=£w$.'
}

/**
 * section SuiteTermeGeneral
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.laPalette)
    // la correction sera écrite en-dessous de la figure mtg32
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let k = 1; k <= 4; k++) stor['zoneExpli' + k] = j3pAddElt(zoneExpli, 'div')
    let affichageRaison
    if (stor.defSuite.typeSuite !== arithmetique) {
      affichageRaison = (String(stor.defSuite.a).includes('frac'))
        ? '\\left(' + stor.defSuite.a + '\\right)'
        : (j3pNombre(stor.defSuite.a) < 0)
            ? '\\left(' + stor.defSuite.a + '\\right)'
            : stor.defSuite.a
    }
    let vn, puisQ
    if (stor.defSuite.typeSuite === arithmetique) {
      const raisonFoisN = (stor.defSuite.termeInit[me.questionCourante - 1][0] === 0) ? j3pGetLatexMonome(2, 1, stor.defSuite.b, 'n') : j3pGetLatexMonome(2, 1, stor.defSuite.b, '(n' + j3pMonome(2, 0, j3pGetLatexOppose(stor.defSuite.termeInit[me.questionCourante - 1][0])) + ')')
      vn = (Math.abs(j3pCalculValeur(stor.defSuite.u0)) < Math.pow(10, -12))
        ? j3pGetLatexMonome(1, 1, stor.defSuite.b, 'n') + j3pGetLatexMonome(2, 0, j3pGetLatexOppose(j3pGetLatexProduit(stor.defSuite.termeInit[me.questionCourante - 1][0], stor.defSuite.b)))
        : j3pGetLatexMonome(1, 1, stor.defSuite.b, 'n') + j3pGetLatexMonome(2, 0, j3pGetLatexSomme(stor.defSuite.u0, j3pGetLatexOppose(j3pGetLatexProduit(stor.defSuite.termeInit[me.questionCourante - 1][0], stor.defSuite.b))), '')
      // facteur devant r pour une suite arithmétique
      const factR = (Number(stor.defSuite.termeInit[me.questionCourante - 1][0]) === 0) ? '\\times n' : '\\times(n-' + stor.defSuite.termeInit[me.questionCourante - 1][0] + ')'
      j3pAffiche(stor.zoneExpli1, '', textes.corr3, {
        v: stor.defSuite.nomU,
        r: stor.defSuite.b,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        i: stor.defSuite.u0,
        w: vn,
        s: raisonFoisN,
        p: factR
      })
    } else if (stor.defSuite.typeSuite === geometrique) {
      vn = (Math.abs(j3pCalculValeur(stor.defSuite.u0) - 1) < Math.pow(10, -12))
        ? affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
        : (Math.abs(j3pCalculValeur(stor.defSuite.u0) + 1) < Math.pow(10, -12))
            ? '-' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
            : stor.defSuite.u0 + '\\times' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
      // puissance dans le cas d’une suite géo
      puisQ = (Number(stor.defSuite.termeInit[me.questionCourante - 1][0]) === 0) ? 'n' : 'n-' + stor.defSuite.termeInit[me.questionCourante - 1][0]
      j3pAffiche(stor.zoneExpli1, '', textes.corr4, {
        v: stor.defSuite.nomU,
        r: stor.defSuite.a,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        i: stor.defSuite.u0,
        w: vn,
        s: affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}',
        m: 'n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])),
        p: puisQ
      })
    } else {
      vn = (Math.abs(j3pCalculValeur(stor.defSuite.v0) - 1) < Math.pow(10, -12))
        ? affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
        : (Math.abs(j3pCalculValeur(stor.defSuite.v0) + 1) < Math.pow(10, -12))
            ? '-' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
            : stor.defSuite.v0 + '\\times' + affichageRaison + '^{n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])) + '}'
      puisQ = (Number(stor.defSuite.termeInit[me.questionCourante - 1][0]) === 0) ? 'n' : 'n-' + stor.defSuite.termeInit[me.questionCourante - 1][0]
      j3pAffiche(stor.zoneExpli1, '', textes.corr1, {
        v: stor.defSuite.nomV,
        u: stor.defSuite.nomU,
        r: stor.defSuite.a,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        i: stor.defSuite.v0,
        w: vn,
        s: affichageRaison,
        m: 'n' + j3pMonome(2, 0, -j3pNombre(stor.defSuite.termeInit[me.questionCourante - 1][0])),
        p: puisQ
      })
      j3pAffiche(stor.zoneExpli2, '', textes.corr2, {
        v: stor.defSuite.nomV,
        u: stor.defSuite.nomU,
        c: j3pGetLatexMonome(2, 0, stor.defSuite.c, ''),
        d: j3pGetLatexMonome(2, 0, j3pGetLatexOppose(stor.defSuite.c), ''),
        w: vn
      })
    }
  }

  function initFigure () {
    // txt html de la figure
    const txtFig = 'TWF0aEdyYXBoSmF2YTEuMAAAAA8+TMzNAANmcmH###8BAP8BAAAAAAAAAAAD0wAAAiQAAAAAAAAAAAAAAAAAAACk#####wAAAAEAEW9iamV0cy5DQ2FsY0NvbnN0AP####8AAnBpABYzLjE0MTU5MjY1MzU4OTc5MzIzODQ2#####wAAAAEAEW9iamV0cy5DQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAQb2JqZXRzLkNGb25jTlZhcgD#####AAhhcnJvbmRpcgAaaW50KHgqMTBecHVpcyswLjUpLzEwXnB1aXP#####AAAAAQARb2JqZXRzLkNPcGVyYXRpb24D#####wAAAAIAEG9iamV0cy5DRm9uY3Rpb24CAAAAAwAAAAADAv####8AAAACABhvYmpldHMuQ1ZhcmlhYmxlRm9ybWVsbGUAAAAA#####wAAAAEAEW9iamV0cy5DUHVpc3NhbmNlAAAAAUAkAAAAAAAAAAAABQAAAAEAAAABP+AAAAAAAAAAAAAGAAAAAUAkAAAAAAAAAAAABQAAAAEAAAACAAF4AARwdWlz#####wAAAAEADm9iamV0cy5DQ2FsY3VsAP####8ABGJudW0AAi0x#####wAAAAEAE29iamV0cy5DTW9pbnNVbmFpcmUAAAABP#AAAAAAAAAAAAAHAP####8ABGJkZW4AATIAAAABQAAAAAAAAAAAAAAHAP####8AAWIACWJudW0vYmRlbgAAAAMD#####wAAAAEAFm9iamV0cy5DUmVzdWx0YXRWYWxldXIAAAACAAAACQAAAAMAAAAHAP####8ABGJ2YWwADmFycm9uZGlyKGIsMTAp#####wAAAAEAGW9iamV0cy5DQXBwZWxGb25jdGlvbk5WYXIAAAACAAAAAQAAAAkAAAAEAAAAAUAkAAAAAAAAAAAABwD#####AARhbnVtAAEzAAAAAUAIAAAAAAAAAAAABwD#####AARhZGVuAAEyAAAAAUAAAAAAAAAAAAAABwD#####AAFhAAlhbnVtL2FkZW4AAAADAwAAAAkAAAAGAAAACQAAAAcAAAAHAP####8ABGF2YWwADmFycm9uZGlyKGEsMTApAAAACgAAAAIAAAABAAAACQAAAAgAAAABQCQAAAAAAAAAAAAHAP####8ABXVkZW4wAAE1AAAAAUAUAAAAAAAAAAAABwD#####AAV1bnVtMAABMgAAAAFAAAAAAAAAAAAAAAcA#####wACbjAAATAAAAABAAAAAAAAAAAAAAAHAP####8ABWFkZW4yABJhYnModWRlbjAqYW51bV5uMCkAAAAEAAAAAAMCAAAACQAAAAoAAAAGAAAACQAAAAYAAAAJAAAADAAAAAcA#####wAFYW51bTIAMHVudW0wKmFkZW5ebjAqYWJzKHVkZW4wKmFudW1ebjApLyh1ZGVuMCphbnVtXm4wKQAAAAMDAAAAAwIAAAADAgAAAAkAAAALAAAABgAAAAkAAAAHAAAACQAAAAwAAAAEAAAAAAMCAAAACQAAAAoAAAAGAAAACQAAAAYAAAAJAAAADAAAAAMCAAAACQAAAAoAAAAGAAAACQAAAAYAAAAJAAAADAAAAAcA#####wACYTIAC2FudW0yL2FkZW4yAAAAAwMAAAAJAAAADgAAAAkAAAANAAAABwD#####AAVhdmFsMgAPYXJyb25kaXIoYTIsMTApAAAACgAAAAIAAAABAAAACQAAAA8AAAABQCQAAAAAAAAAAAAHAP####8AAnUwAAt1bnVtMC91ZGVuMAAAAAMDAAAACQAAAAsAAAAJAAAACgAAAAcA#####wACdjAAFnNpKGE8PjEsdTAtYi8oMS1hKSx1MCn#####AAAAAQAUb2JqZXRzLkNGb25jdGlvbjNWYXIAAAAAAwkAAAAJAAAACAAAAAE#8AAAAAAAAAAAAAMBAAAACQAAABEAAAADAwAAAAkAAAAEAAAAAwEAAAABP#AAAAAAAAAAAAAJAAAACAAAAAkAAAARAAAABwD#####AAV2dmFsMAAPYXJyb25kaXIodjAsMTApAAAACgAAAAIAAAABAAAACQAAABIAAAABQCQAAAAAAAAAAAAHAP####8AAWMAE3NpKGE8PjEsLWIvKDEtYSksMCkAAAALAAAAAAMJAAAACQAAAAgAAAABP#AAAAAAAAAAAAAIAAAAAwMAAAAJAAAABAAAAAMBAAAAAT#wAAAAAAAAAAAACQAAAAgAAAABAAAAAAAAAAAAAAAHAP####8ABGN2YWwADmFycm9uZGlyKGMsMTApAAAACgAAAAIAAAABAAAACQAAABQAAAABQCQAAAAAAAAAAAAHAP####8ADGNzdGVGcmFjaW5pdAAYdW51bTAvdWRlbjAtbjAqYm51bS9iZGVuAAAAAwEAAAADAwAAAAkAAAALAAAACQAAAAoAAAADAwAAAAMCAAAACQAAAAwAAAAJAAAAAgAAAAkAAAADAAAABwD#####AA1zaWduZWNzdGVJbml0AB5jc3RlRnJhY2luaXQvYWJzKGNzdGVGcmFjaW5pdCkAAAADAwAAAAkAAAAWAAAABAAAAAAJAAAAFgAAAAcA#####wAEY3N0ZQAHdTAtYipuMAAAAAMBAAAACQAAABEAAAADAgAAAAkAAAAEAAAACQAAAAz#####AAAAAgANb2JqZXRzLkNMYXRleAD#####AAAAAAAPAAAB#####xFASYAAAAAAAEBQAAAAAAAAAAAAAAAAAAAAAAAadV97bisxfT1cVmFse2F9dV9uK1xWYWx7Yn0AAAAMAP####8AAAAAAA8AAAH#####EUBNAAAAAAAAQF0AAAAAAAAAAAAAAAAAAAAAAA92X249dV9uK1xWYWx7Y30AAAAMAP####8AAAAAAA8AAAH#####EUBKgAAAAAAAQDcAAAAAAAAAAAAAAAAAAAAAAAx1XzA9XFZhbHt1MH0AAAAMAP####8AAAAAAA8AAAH#####EUB20AAAAAAAQF8AAAAAAAAAAAAAAAAAAAAAAAx2XzA9XFZhbHt2MH0AAAAMAP####8AAAAAAA8AAAH#####EUBGAAAAAAAAQGTAAAAAAAAAAAAAAAAAAAAAAOtcYmVnaW57YXJyYXl9e2x9Clx0ZXh0e0V4cHJlc3Npb24gZGUgbGEgc3VpdGUgZ8Opb23DqXRyaXF1ZSAtIH1cXApcdGV4dHtxdWUgY2Ugc29pdCB9KHVfbilcdGV4dHsgb3UgfSh2X24pXHRleHR7IGRhbnMgbGUgY2FzIG#DuSB9KHVfbilcdGV4dHsgc2VyYWl0IGFyaXRobcOpdGljby1nw6lvbcOpcmlxdWV9XFwgCnZfbj1cVmFse3YwfVx0aW1lcyBcVmFse2F9XntuLVxWYWx7bjB9fVxcIApcZW5ke2FycmF5fQoKAAAADAD#####AAAAAAAPAAAB#####xFAR4AAAAAAAEBwUAAAAAAAAAAAAAAAAAAAAACfXGJlZ2lue2FycmF5fXtsfQpcdGV4dHtEYW5zIGxlIGNhcyBvw7kgbGEgc3VpdGUgfSh1X24pXHRleHR7IGVzdCBhcml0aG3DqXRpY28tZ8Opb23DqXRyaXF1ZSA6fVxcCnVfbj1cVmFse3YwfVx0aW1lcyBcVmFse2F9XntuLVxWYWx7bjB9fS1cVmFse2N9XFwgClxlbmR7YXJyYXl9AAAADAD#####AAAAAAAPAAAB#####xFAR4AAAAAAAEB1EAAAAAAAAAAAAAAAAAAAAACHXGJlZ2lue2FycmF5fXtsfQpcdGV4dHtFeHByZXNzaW9uIGRlIH0odV9uKVx0ZXh0eyBsb3JzcXVlIGNlbGxlLWNpIGVzdCBhcml0aG3DqXRpcXVlfVxcCnVfbj1cVmFse3UwfStcVmFse2J9KG4tXFZhbHtuMH0pXFwgClxlbmR7YXJyYXl9#####wAAAAEADG9iamV0cy5DRm9uYwD#####AANyZXAADC0wLjYqMS41Xm4rMQAAAAMAAAAACAAAAAMCAAAAAT#jMzMzMzMzAAAABgAAAAE#+AAAAAAAAAAAAAUAAAAAAAAAAT#wAAAAAAAAAAFuAAAABwD#####AARudmFsAAEyAAAAAUAAAAAAAAAAAAAABwD#####AAhjc3RlRnJhYwAZYXJyb25kaXIoY3N0ZUZyYWNpbml0LDEwKQAAAAoAAAACAAAAAQAAAAkAAAAWAAAAAUAkAAAAAAAAAAAADQD#####AARmR2VvAB51bnVtMC91ZGVuMCooYW51bS9hZGVuKV4obi1uMCkAAAADAgAAAAMDAAAACQAAAAsAAAAJAAAACgAAAAYAAAADAwAAAAkAAAAGAAAACQAAAAcAAAADAQAAAAUAAAAAAAAACQAAAAwAAW4AAAANAP####8ABnJlcGdlbwAJMioyXihuLTEpAAAAAwIAAAABQAAAAAAAAAAAAAAGAAAAAUAAAAAAAAAAAAAAAwEAAAAFAAAAAAAAAAE#8AAAAAAAAAABbv####8AAAADABdvYmpldHMuQ1Rlc3RFcXVpdmFsZW5jZQD#####AAplZ2FsaXRlR2VvAAAAIwAAACQBAAAAAAE#8AAAAAAAAAH#####AAAAAQAWb2JqZXRzLkNWYWxldXJBZmZpY2hlZQD#####AAAAAAAPAAAB#####xFAdhAAAAAAAEB8QAAAAAAAAAAAAAAAAAAAAAAlRWdhbGl0w6kgZGUgbGEgc3VpdGUgZ8Opb23DqXRyaXF1ZSA6IAAAAgAAACUAAAAHAP####8ABWFudW0zACFhbnVtMi9wZ2NkKGFicyhhbnVtMiksYWJzKGFkZW4yKSkAAAADAwAAAAkAAAAO#####wAAAAEAFG9iamV0cy5DRm9uY3Rpb24yVmFyAgAAAAQAAAAACQAAAA4AAAAEAAAAAAkAAAANAAAABwD#####AAVhZGVuMwAhYWRlbjIvcGdjZChhYnMoYW51bTIpLGFicyhhZGVuMikpAAAAAwMAAAAJAAAADQAAABACAAAABAAAAAAJAAAADgAAAAQAAAAACQAAAA0AAAANAP####8AB2ZHZW9CaXMAGWFudW0zL2FkZW4zKihhbnVtL2FkZW4pXm4AAAADAgAAAAMDAAAACQAAACcAAAAJAAAAKAAAAAYAAAADAwAAAAkAAAAGAAAACQAAAAcAAAAFAAAAAAABbgAAAA4A#####wANZWdhbGl0ZUdlb0JpcwAAACkAAAAkAQAAAAABP#AAAAAAAAABAAAABwD#####AAVhbnVtNAB1KHVudW0wKmJkZW4qKGFkZW4tYW51bSktdWRlbjAqYm51bSphZGVuKSphZGVuXm4wKmFicyh1ZGVuMCpiZGVuKihhZGVuLWFudW0pKmFudW1ebjApLyh1ZGVuMCpiZGVuKihhZGVuLWFudW0pKmFudW1ebjApAAAAAwMAAAADAgAAAAMCAAAAAwEAAAADAgAAAAMCAAAACQAAAAsAAAAJAAAAAwAAAAMBAAAACQAAAAcAAAAJAAAABgAAAAMCAAAAAwIAAAAJAAAACgAAAAkAAAACAAAACQAAAAcAAAAGAAAACQAAAAcAAAAJAAAADAAAAAQAAAAAAwIAAAADAgAAAAMCAAAACQAAAAoAAAAJAAAAAwAAAAMBAAAACQAAAAcAAAAJAAAABgAAAAYAAAAJAAAABgAAAAkAAAAMAAAAAwIAAAADAgAAAAMCAAAACQAAAAoAAAAJAAAAAwAAAAMBAAAACQAAAAcAAAAJAAAABgAAAAYAAAAJAAAABgAAAAkAAAAMAAAABwD#####AAVhZGVuNAAjYWJzKHVkZW4wKmJkZW4qKGFkZW4tYW51bSkqYW51bV5uMCkAAAAEAAAAAAMCAAAAAwIAAAADAgAAAAkAAAAKAAAACQAAAAMAAAADAQAAAAkAAAAHAAAACQAAAAYAAAAGAAAACQAAAAYAAAAJAAAADAAAAAcA#####wAFYW51bTUAIWFudW00L3BnY2QoYWJzKGFudW00KSxhYnMoYWRlbjQpKQAAAAMDAAAACQAAACsAAAAQAgAAAAQAAAAACQAAACsAAAAEAAAAAAkAAAAsAAAABwD#####AAVhZGVuNQAhYWRlbjQvcGdjZChhYnMoYW51bTQpLGFicyhhZGVuNCkpAAAAAwMAAAAJAAAALAAAABACAAAABAAAAAAJAAAAKwAAAAQAAAAACQAAACwAAAANAP####8AC2ZHZW9GcmFjQmlzABlhbnVtNS9hZGVuNSooYW51bS9hZGVuKV5uAAAAAwIAAAADAwAAAAkAAAAtAAAACQAAAC4AAAAGAAAAAwMAAAAJAAAABgAAAAkAAAAHAAAABQAAAAAAAW4AAAAOAP####8AEWVnYWxpdGVHZW9GcmFjQmlzAAAALwAAACQBAAAAAAE#8AAAAAAAAAEAAAAHAP####8ABWFudW02AFsodW51bTAqYmRlbiooYWRlbi1hbnVtKS11ZGVuMCpibnVtKmFkZW4pKnVkZW4wKmJkZW4qKGFkZW4tYW51bSkvYWJzKHVkZW4wKmJkZW4qKGFkZW4tYW51bSkpAAAAAwMAAAADAgAAAAMBAAAAAwIAAAADAgAAAAkAAAALAAAACQAAAAMAAAADAQAAAAkAAAAHAAAACQAAAAYAAAADAgAAAAMCAAAACQAAAAoAAAAJAAAAAgAAAAkAAAAHAAAAAwIAAAADAgAAAAkAAAAKAAAACQAAAAMAAAADAQAAAAkAAAAHAAAACQAAAAYAAAAEAAAAAAMCAAAAAwIAAAAJAAAACgAAAAkAAAADAAAAAwEAAAAJAAAABwAAAAkAAAAGAAAABwD#####AAVhZGVuNgAbYWJzKHVkZW4wKmJkZW4qKGFkZW4tYW51bSkpAAAABAAAAAADAgAAAAMCAAAACQAAAAoAAAAJAAAAAwAAAAMBAAAACQAAAAcAAAAJAAAABgAAAAcA#####wAFYW51bTcAIWFudW02L3BnY2QoYWJzKGFudW02KSxhYnMoYWRlbjYpKQAAAAMDAAAACQAAADEAAAAQAgAAAAQAAAAACQAAADEAAAAEAAAAAAkAAAAyAAAABwD#####AAVhZGVuNwAhYWRlbjYvcGdjZChhYnMoYW51bTYpLGFicyhhZGVuNikpAAAAAwMAAAAJAAAAMgAAABACAAAABAAAAAAJAAAAMQAAAAQAAAAACQAAADIAAAANAP####8ACGZHZW9GcmFjAB5hbnVtNy9hZGVuNyooYW51bS9hZGVuKV4obi1uMCkAAAADAgAAAAMDAAAACQAAADMAAAAJAAAANAAAAAYAAAADAwAAAAkAAAAGAAAACQAAAAcAAAADAQAAAAUAAAAAAAAACQAAAAwAAW4AAAAOAP####8ADmVnYWxpdGVHZW9GcmFjAAAANQAAACQBAAAAAAE#8AAAAAAAAAEAAAAHAP####8AB2NzdGVudW0AGHVudW0wKmJkZW4tdWRlbjAqYm51bSpuMAAAAAMBAAAAAwIAAAAJAAAACwAAAAkAAAADAAAAAwIAAAADAgAAAAkAAAAKAAAACQAAAAIAAAAJAAAADAAAAAcA#####wAHY3N0ZWRlbgAKdWRlbjAqYmRlbgAAAAMCAAAACQAAAAoAAAAJAAAAAwAAAAcA#####wAIY3N0ZW51bTIAJ2NzdGVudW0vcGdjZChhYnMoY3N0ZW51bSksYWJzKGNzdGVkZW4pKQAAAAMDAAAACQAAADcAAAAQAgAAAAQAAAAACQAAADcAAAAEAAAAAAkAAAA4AAAABwD#####AAhjc3RlZGVuMgAnY3N0ZWRlbi9wZ2NkKGFicyhjc3RlbnVtKSxhYnMoY3N0ZWRlbikpAAAAAwMAAAAJAAAAOAAAABACAAAABAAAAAAJAAAANwAAAAQAAAAACQAAADgAAAANAP####8AAmYyAB1ibnVtL2JkZW4qbitjc3RlbnVtMi9jc3RlZGVuMgAAAAMAAAAAAwIAAAADAwAAAAkAAAACAAAACQAAAAMAAAAFAAAAAAAAAAMDAAAACQAAADkAAAAJAAAAOgABbgAAAA4A#####wAIZWdhbGl0ZTIAAAA7AAAAIAEAAAAAAT#wAAAAAAAAAQAAAA8A#####wAAAAAADwAAAf####8RQE8AAAAAAABAfbAAAAAAAAAAAAAAAAAAAAAADcOpZ2FsaXTDqSAyIDoAAAIAAAA8AAAABwD#####AA1pbWFnZXNFZ2FsZXMyACBhYnMoZjIobnZhbCktcmVwKG52YWwpKTwxMF4oLTEyKQAAAAMEAAAABAAAAAADAf####8AAAABABVvYmpldHMuQ0FwcGVsRm9uY3Rpb24AAAA7AAAACQAAACEAAAARAAAAIAAAAAkAAAAhAAAABgAAAAFAJAAAAAAAAAAAAAgAAAABQCgAAAAAAAD#####AAAAAgATb2JqZXRzLkNDb21tZW50YWlyZQD#####AAAAAAAPAAAB#####xFAgxAAAAAAAEBHAAAAAAAAAAAAAAAAAAAAAABPdGVzdCBkZSBsJ8OpZ2FsaXTDqSBlbnRyZSBmMigjVmFsKG52YWwpKSBldCByZXAoI1ZhbChudmFsKSkKI1ZhbChpbWFnZXNFZ2FsZXMyKQAAAAcA#####wAEY251bQAzLWJudW0qYWRlbiphYnMoYmRlbiooYWRlbi1hbnVtKSkvKGJkZW4qKGFkZW4tYW51bSkpAAAACAAAAAMDAAAAAwIAAAADAgAAAAkAAAACAAAACQAAAAcAAAAEAAAAAAMCAAAACQAAAAMAAAADAQAAAAkAAAAHAAAACQAAAAYAAAADAgAAAAkAAAADAAAAAwEAAAAJAAAABwAAAAkAAAAGAAAABwD#####AARjZGVuABVhYnMoYmRlbiooYWRlbi1hbnVtKSkAAAAEAAAAAAMCAAAACQAAAAMAAAADAQAAAAkAAAAHAAAACQAAAAYAAAAHAP####8ABWNudW0yAB5jbnVtL3BnY2QoYWJzKGNudW0pLGFicyhjZGVuKSkAAAADAwAAAAkAAABAAAAAEAIAAAAEAAAAAAkAAABAAAAABAAAAAAJAAAAQQAAAAcA#####wAFY2RlbjIAHmNkZW4vcGdjZChhYnMoY251bSksYWJzKGNkZW4pKQAAAAMDAAAACQAAAEEAAAAQAgAAAAQAAAAACQAAAEAAAAAEAAAAAAkAAABBAAAADQD#####AAFmACp1bnVtMC91ZGVuMCooYW51bS9hZGVuKV4obi1uMCktY251bTIvY2RlbjIAAAADAQAAAAMCAAAAAwMAAAAJAAAACwAAAAkAAAAKAAAABgAAAAMDAAAACQAAAAYAAAAJAAAABwAAAAMBAAAABQAAAAAAAAAJAAAADAAAAAMDAAAACQAAAEIAAAAJAAAAQwABbgAAAA4A#####wAIZWdhbGl0ZTEAAABEAAAAIAEAAAAAAT#wAAAAAAAAAQAAAA8A#####wAAAAAADwAAAf####8RQE+AAAAAAABAe2AAAAAAAAAAAAAAAAAAAAAADcOpZ2FsaXTDqTEgOiAAAAIAAABFAAAADQD#####AAVmRnJhYwAqYW51bTcvYWRlbjcqKGFudW0vYWRlbileKG4tbjApLWNudW0yL2NkZW4yAAAAAwEAAAADAgAAAAMDAAAACQAAADMAAAAJAAAANAAAAAYAAAADAwAAAAkAAAAGAAAACQAAAAcAAAADAQAAAAUAAAAAAAAACQAAAAwAAAADAwAAAAkAAABCAAAACQAAAEMAAW4AAAAOAP####8ADGVnYWxpdGVGcmFjMQAAAEcAAAAgAQAAAAABP#AAAAAAAAABAAAADQD#####AAZmRnJhYzIAFGJudW0vYmRlbipuK2NzdGVGcmFjAAAAAwAAAAADAgAAAAMDAAAACQAAAAIAAAAJAAAAAwAAAAUAAAAAAAAACQAAACIAAW4AAAAOAP####8ADGVnYWxpdGVGcmFjMgAAAEkAAAAgAQAAAAABP#AAAAAAAAABAAAADQD#####AAhmRnJhY0JpcwAlYW51bTUvYWRlbjUqKGFudW0vYWRlbilebi1jbnVtMi9jZGVuMgAAAAMBAAAAAwIAAAADAwAAAAkAAAAtAAAACQAAAC4AAAAGAAAAAwMAAAAJAAAABgAAAAkAAAAHAAAABQAAAAAAAAADAwAAAAkAAABCAAAACQAAAEMAAW4AAAAOAP####8AD2VnYWxpdGVGcmFjYmlzMQAAAEsAAAAgAQAAAAABP#AAAAAAAAABAAAADQD#####AARmQmlzACVhbnVtMy9hZGVuMyooYW51bS9hZGVuKV5uLWNudW0yL2NkZW4yAAAAAwEAAAADAgAAAAMDAAAACQAAACcAAAAJAAAAKAAAAAYAAAADAwAAAAkAAAAGAAAACQAAAAcAAAAFAAAAAAAAAAMDAAAACQAAAEIAAAAJAAAAQwABbgAAAA4A#####wALZWdhbGl0ZWJpczEAAABNAAAAIAEAAAAAAT#wAAAAAAAAAQAAAA0A#####wAMZkRlY2ltYWxHZW8xABh2dmFsMCooYW51bS9hZGVuKV4obi1uMCkAAAADAgAAAAkAAAATAAAABgAAAAMDAAAACQAAAAYAAAAJAAAABwAAAAMBAAAABQAAAAAAAAAJAAAADAABbgAAAA0A#####wAMZkRlY2ltYWxHZW8yABF2dmFsMCphdmFsXihuLW4wKQAAAAMCAAAACQAAABMAAAAGAAAACQAAAAkAAAADAQAAAAUAAAAAAAAACQAAAAwAAW4AAAANAP####8ADGZEZWNpbWFsR2VvMwAMYXZhbDIqYXZhbF5uAAAAAwIAAAAJAAAAEAAAAAYAAAAJAAAACQAAAAUAAAAAAAFuAAAADQD#####AAxmRGVjaW1hbEdlbzQAE2F2YWwyKihhbnVtL2FkZW4pXm4AAAADAgAAAAkAAAAQAAAABgAAAAMDAAAACQAAAAYAAAAJAAAABwAAAAUAAAAAAAFuAAAADQD#####AAxmRGVjaW1hbEdlbzUAF3VudW0wL3VkZW4wKmF2YWxeKG4tbjApAAAAAwIAAAADAwAAAAkAAAALAAAACQAAAAoAAAAGAAAACQAAAAkAAAADAQAAAAUAAAAAAAAACQAAAAwAAW4AAAANAP####8ADGZEZWNpbWFsR2VvNgAXYW51bTcvYWRlbjcqYXZhbF4obi1uMCkAAAADAgAAAAMDAAAACQAAADMAAAAJAAAANAAAAAYAAAAJAAAACQAAAAMBAAAABQAAAAAAAAAJAAAADAABbgAAAA0A#####wAMZkRlY2ltYWxHZW83ABJhbnVtMy9hZGVuMyphdmFsXm4AAAADAgAAAAMDAAAACQAAACcAAAAJAAAAKAAAAAYAAAAJAAAACQAAAAUAAAAAAAFuAAAADQD#####AAxmRGVjaW1hbEdlbzgAEmFudW01L2FkZW41KmF2YWxebgAAAAMCAAAAAwMAAAAJAAAALQAAAAkAAAAuAAAABgAAAAkAAAAJAAAABQAAAAAAAW4AAAAOAP####8ACmVnYURlY0dlbzEAAABPAAAAJAEAAAAAAT#wAAAAAAAAAQAAAA4A#####wAKZWdhRGVjR2VvMgAAAFAAAAAkAQAAAAABP#AAAAAAAAABAAAADgD#####AAplZ2FEZWNHZW8zAAAAUQAAACQBAAAAAAE#8AAAAAAAAAEAAAAOAP####8ACmVnYURlY0dlbzQAAABSAAAAJAEAAAAAAT#wAAAAAAAAAQAAAA4A#####wAKZWdhRGVjR2VvNQAAAFMAAAAkAQAAAAABP#AAAAAAAAABAAAADgD#####AAplZ2FEZWNHZW82AAAAVAAAACQBAAAAAAE#8AAAAAAAAAEAAAAOAP####8ACmVnYURlY0dlbzcAAABVAAAAJAEAAAAAAT#wAAAAAAAAAQAAAA4A#####wAKZWdhRGVjR2VvOAAAAFYAAAAkAQAAAAABP#AAAAAAAAABAAAADQD#####AAlmRGVjaW1hbDEAJHZ2YWwwKihhbnVtL2FkZW4pXihuLW4wKS1jbnVtMi9jZGVuMgAAAAMBAAAAAwIAAAAJAAAAEwAAAAYAAAADAwAAAAkAAAAGAAAACQAAAAcAAAADAQAAAAUAAAAAAAAACQAAAAwAAAADAwAAAAkAAABCAAAACQAAAEMAAW4AAAANAP####8ACWZEZWNpbWFsMgAddnZhbDAqYXZhbF4obi1uMCktY251bTIvY2RlbjIAAAADAQAAAAMCAAAACQAAABMAAAAGAAAACQAAAAkAAAADAQAAAAUAAAAAAAAACQAAAAwAAAADAwAAAAkAAABCAAAACQAAAEMAAW4AAAANAP####8ACWZEZWNpbWFsMwAWdnZhbDAqYXZhbF4obi1uMCktY3ZhbAAAAAMBAAAAAwIAAAAJAAAAEwAAAAYAAAAJAAAACQAAAAMBAAAABQAAAAAAAAAJAAAADAAAAAkAAAAVAAFuAAAADQD#####AAlmRGVjaW1hbDQAHXZ2YWwwKihhbnVtL2FkZW4pXihuLW4wKS1jdmFsAAAAAwEAAAADAgAAAAkAAAATAAAABgAAAAMDAAAACQAAAAYAAAAJAAAABwAAAAMBAAAABQAAAAAAAAAJAAAADAAAAAkAAAAVAAFuAAAADQD#####AAlmRGVjaW1hbDUAI3VudW0wL3VkZW4wKmF2YWxeKG4tbjApLWNudW0yL2NkZW4yAAAAAwEAAAADAgAAAAMDAAAACQAAAAsAAAAJAAAACgAAAAYAAAAJAAAACQAAAAMBAAAABQAAAAAAAAAJAAAADAAAAAMDAAAACQAAAEIAAAAJAAAAQwABbgAAAA0A#####wAJZkRlY2ltYWw2ABx1bnVtMC91ZGVuMCphdmFsXihuLW4wKS1jdmFsAAAAAwEAAAADAgAAAAMDAAAACQAAAAsAAAAJAAAACgAAAAYAAAAJAAAACQAAAAMBAAAABQAAAAAAAAAJAAAADAAAAAkAAAAVAAFuAAAADQD#####AAlmRGVjaW1hbDcAI3VudW0wL3VkZW4wKihhbnVtL2FkZW4pXihuLW4wKS1jdmFsAAAAAwEAAAADAgAAAAMDAAAACQAAAAsAAAAJAAAACgAAAAYAAAADAwAAAAkAAAAGAAAACQAAAAcAAAADAQAAAAUAAAAAAAAACQAAAAwAAAAJAAAAFQABbgAAAA0A#####wAJZkRlY2ltYWw4AB9hdmFsMiooYW51bS9hZGVuKV5uLWNudW0yL2NkZW4yAAAAAwEAAAADAgAAAAkAAAAQAAAABgAAAAMDAAAACQAAAAYAAAAJAAAABwAAAAUAAAAAAAAAAwMAAAAJAAAAQgAAAAkAAABDAAFuAAAADQD#####AAlmRGVjaW1hbDkAGGF2YWwyKmF2YWxebi1jbnVtMi9jZGVuMgAAAAMBAAAAAwIAAAAJAAAAEAAAAAYAAAAJAAAACQAAAAUAAAAAAAAAAwMAAAAJAAAAQgAAAAkAAABDAAFuAAAADQD#####AApmRGVjaW1hbDEwABFhdmFsMiphdmFsXm4tY3ZhbAAAAAMBAAAAAwIAAAAJAAAAEAAAAAYAAAAJAAAACQAAAAUAAAAAAAAACQAAABUAAW4AAAANAP####8ACmZEZWNpbWFsMTEAGGF2YWwyKihhbnVtL2FkZW4pXm4tY3ZhbAAAAAMBAAAAAwIAAAAJAAAAEAAAAAYAAAADAwAAAAkAAAAGAAAACQAAAAcAAAAFAAAAAAAAAAkAAAAVAAFuAAAADQD#####AApmRGVjaW1hbDEyACNhbnVtNy9hZGVuNyphdmFsXihuLW4wKS1jbnVtMi9jZGVuMgAAAAMBAAAAAwIAAAADAwAAAAkAAAAzAAAACQAAADQAAAAGAAAACQAAAAkAAAADAQAAAAUAAAAAAAAACQAAAAwAAAADAwAAAAkAAABCAAAACQAAAEMAAW4AAAANAP####8ACmZEZWNpbWFsMTMAHGFudW03L2FkZW43KmF2YWxeKG4tbjApLWN2YWwAAAADAQAAAAMCAAAAAwMAAAAJAAAAMwAAAAkAAAA0AAAABgAAAAkAAAAJAAAAAwEAAAAFAAAAAAAAAAkAAAAMAAAACQAAABUAAW4AAAANAP####8ACmZEZWNpbWFsMTQAI2FudW03L2FkZW43KihhbnVtL2FkZW4pXihuLW4wKS1jdmFsAAAAAwEAAAADAgAAAAMDAAAACQAAADMAAAAJAAAANAAAAAYAAAADAwAAAAkAAAAGAAAACQAAAAcAAAADAQAAAAUAAAAAAAAACQAAAAwAAAAJAAAAFQABbgAAAA0A#####wAKZkRlY2ltYWwxNQAeYW51bTUvYWRlbjUqYXZhbF5uLWNudW0yL2NkZW4yAAAAAwEAAAADAgAAAAMDAAAACQAAAC0AAAAJAAAALgAAAAYAAAAJAAAACQAAAAUAAAAAAAAAAwMAAAAJAAAAQgAAAAkAAABDAAFuAAAADQD#####AApmRGVjaW1hbDE2ABdhbnVtNS9hZGVuNSphdmFsXm4tY3ZhbAAAAAMBAAAAAwIAAAADAwAAAAkAAAAtAAAACQAAAC4AAAAGAAAACQAAAAkAAAAFAAAAAAAAAAkAAAAVAAFuAAAADQD#####AApmRGVjaW1hbDE3AB5hbnVtNS9hZGVuNSooYW51bS9hZGVuKV5uLWN2YWwAAAADAQAAAAMCAAAAAwMAAAAJAAAALQAAAAkAAAAuAAAABgAAAAMDAAAACQAAAAYAAAAJAAAABwAAAAUAAAAAAAAACQAAABUAAW4AAAANAP####8ACmZEZWNpbWFsMTgAHmFudW0zL2FkZW4zKmF2YWxebi1jbnVtMi9jZGVuMgAAAAMBAAAAAwIAAAADAwAAAAkAAAAnAAAACQAAACgAAAAGAAAACQAAAAkAAAAFAAAAAAAAAAMDAAAACQAAAEIAAAAJAAAAQwABbgAAAA0A#####wAKZkRlY2ltYWwxOQAXYW51bTMvYWRlbjMqYXZhbF5uLWN2YWwAAAADAQAAAAMCAAAAAwMAAAAJAAAAJwAAAAkAAAAoAAAABgAAAAkAAAAJAAAABQAAAAAAAAAJAAAAFQABbgAAAA0A#####wAKZkRlY2ltYWwyMAAeYW51bTMvYWRlbjMqKGFudW0vYWRlbilebi1jdmFsAAAAAwEAAAADAgAAAAMDAAAACQAAACcAAAAJAAAAKAAAAAYAAAADAwAAAAkAAAAGAAAACQAAAAcAAAAFAAAAAAAAAAkAAAAVAAFuAAAADgD#####AAtlZ2FEZWNpbWFsMQAAAF8AAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAtlZ2FEZWNpbWFsMgAAAGAAAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAtlZ2FEZWNpbWFsMwAAAGEAAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAtlZ2FEZWNpbWFsNAAAAGIAAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAtlZ2FEZWNpbWFsNQAAAGMAAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAtlZ2FEZWNpbWFsNgAAAGQAAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAtlZ2FEZWNpbWFsNwAAAGUAAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAtlZ2FEZWNpbWFsOAAAAGYAAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAtlZ2FEZWNpbWFsOQAAAGcAAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAxlZ2FEZWNpbWFsMTAAAABoAAAAIAEAAAAAAT#wAAAAAAAAAAAAAA4A#####wAMZWdhRGVjaW1hbDExAAAAaQAAACABAAAAAAE#8AAAAAAAAAAAAAAOAP####8ADGVnYURlY2ltYWwxMgAAAGoAAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAxlZ2FEZWNpbWFsMTMAAABrAAAAIAEAAAAAAT#wAAAAAAAAAAAAAA4A#####wAMZWdhRGVjaW1hbDE0AAAAbAAAACABAAAAAAE#8AAAAAAAAAAAAAAOAP####8ADGVnYURlY2ltYWwxNQAAAG0AAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAxlZ2FEZWNpbWFsMTYAAABuAAAAIAEAAAAAAT#wAAAAAAAAAAAAAA4A#####wAMZWdhRGVjaW1hbDE3AAAAbwAAACABAAAAAAE#8AAAAAAAAAAAAAAOAP####8ADGVnYURlY2ltYWwxOAAAAHAAAAAgAQAAAAABP#AAAAAAAAAAAAAADgD#####AAxlZ2FEZWNpbWFsMTkAAABxAAAAIAEAAAAAAT#wAAAAAAAAAAAAAA4A#####wAMZWdhRGVjaW1hbDIwAAAAcgAAACABAAAAAAE#8AAAAAAAAAAAAAANAP####8ADmZEZWNpbWFsQXJpdGgxABZidmFsKm4rY3N0ZW51bS9jc3RlZGVuAAAAAwAAAAADAgAAAAkAAAAFAAAABQAAAAAAAAADAwAAAAkAAAA3AAAACQAAADgAAW4AAAANAP####8ADmZEZWNpbWFsQXJpdGgyAA9idmFsKm4rY3N0ZUZyYWMAAAADAAAAAAMCAAAACQAAAAUAAAAFAAAAAAAAAAkAAAAiAAFuAAAADgD#####AAtlZ2FEZWNBcml0MQAAAIcAAAAgAQAAAAABP#AAAAAAAAABAAAADgD#####AAtlZ2FEZWNBcml0MgAAAIgAAAAgAQAAAAABP#AAAAAAAAABAAAABwD#####AAxpbWFnZXNFZ2FsZXMAQ2FicyhmKG52YWwpLXJlcChudmFsKSk8MTBeKC0xMil8YWJzKGZGcmFjKG52YWwpLXJlcChudmFsKSk8MTBeKC0xMikAAAADCwAAAAMEAAAABAAAAAADAQAAABEAAABEAAAACQAAACEAAAARAAAAIAAAAAkAAAAhAAAABgAAAAFAJAAAAAAAAAAAAAgAAAABQCgAAAAAAAAAAAADBAAAAAQAAAAAAwEAAAARAAAARwAAAAkAAAAhAAAAEQAAACAAAAAJAAAAIQAAAAYAAAABQCQAAAAAAAAAAAAIAAAAAUAoAAAAAAAAAAAAEgD#####AAAAAAAPAAAB#####xFAgyAAAAAAAEAUAAAAAAAAAAAAAAAAAAAAAABNdGVzdCBkZSBsJ8OpZ2FsaXTDqSBlbnRyZSBmKCNWYWwobnZhbCkpIGV0IHJlcCgjVmFsKG52YWwpKQojVmFsKGltYWdlc0VnYWxlcykAAAAHAP####8ADWltYWdlc0VnYWxlczMAT2FicyhmR2VvKG52YWwpLXJlcGdlbyhudmFsKSk8MTBeKC0xMil8YWJzKGZHZW9GcmFjKG52YWwpLXJlcGdlbyhudmFsKSk8MTBeKC0xMikAAAADCwAAAAMEAAAABAAAAAADAQAAABEAAAAjAAAACQAAACEAAAARAAAAJAAAAAkAAAAhAAAABgAAAAFAJAAAAAAAAAAAAAgAAAABQCgAAAAAAAAAAAADBAAAAAQAAAAAAwEAAAARAAAANQAAAAkAAAAhAAAAEQAAACQAAAAJAAAAIQAAAAYAAAABQCQAAAAAAAAAAAAIAAAAAUAoAAAAAAAAAAAAEgD#####AAAAAAAPAAAB#####xFAgxgAAAAAAEBXAAAAAAAAAAAAAAAAAAAAAABUdGVzdCBkZSBsJ8OpZ2FsaXTDqSBlbnRyZSBmR2VvKCNWYWwobnZhbCkpIGV0IHJlcGdlbygjVmFsKG52YWwpKQojVmFsKGltYWdlc0VnYWxlczMpAAAABwD#####AAhwcGNtZGVuZgARcHBjbSh1ZGVuMCxjZGVuMikAAAAQAwAAAAkAAAAKAAAACQAAAEMAAAAHAP####8ACHVudW1kZXIwABR1bnVtMCpwcGNtZGVuZi91ZGVuMAAAAAMDAAAAAwIAAAAJAAAACwAAAAkAAACPAAAACQAAAAoAAAAHAP####8ACGNudW1kZXIyABRjbnVtMipwcGNtZGVuZi9jZGVuMgAAAAMDAAAAAwIAAAAJAAAAQgAAAAkAAACPAAAACQAAAEMAAAAHAP####8ADHBwY21kZW5mRnJhYwARcHBjbShhZGVuNyxjZGVuMikAAAAQAwAAAAkAAAA0AAAACQAAAEMAAAAHAP####8ACGFudW1kZXI3ABhhbnVtNypwcGNtZGVuZkZyYWMvYWRlbjcAAAADAwAAAAMCAAAACQAAADMAAAAJAAAAkgAAAAkAAAA0AAAABwD#####AAljbnVtZGVyMjIAF2NudW0qcHBjbWRlbmZGcmFjL2NkZW4yAAAAAwMAAAADAgAAAAkAAABAAAAACQAAAJIAAAAJAAAAQwAAAAcA#####wAPcHBjbWRlbmZGcmFjQmlzABFwcGNtKGFkZW41LGNkZW4yKQAAABADAAAACQAAAC4AAAAJAAAAQwAAAAcA#####wAIYW51bWRlcjUAG2FudW01KnBwY21kZW5mRnJhY0Jpcy9hZGVuNQAAAAMDAAAAAwIAAAAJAAAALQAAAAkAAACVAAAACQAAAC4AAAAHAP####8ACWNudW1kZXIyMwAbY251bTIqcHBjbWRlbmZGcmFjQmlzL2NkZW4yAAAAAwMAAAADAgAAAAkAAABCAAAACQAAAJUAAAAJAAAAQwAAAAcA#####wALcHBjbWRlbmZCaXMAEXBwY20oYWRlbjMsY2RlbjIpAAAAEAMAAAAJAAAAKAAAAAkAAABDAAAABwD#####AAhhbnVtZGVyMwAXYW51bTMqcHBjbWRlbmZCaXMvYWRlbjMAAAADAwAAAAMCAAAACQAAACcAAAAJAAAAmAAAAAkAAAAoAAAABwD#####AAljbnVtZGVyMjQAF2NudW0yKnBwY21kZW5mQmlzL2NkZW4yAAAAAwMAAAADAgAAAAkAAABCAAAACQAAAJgAAAAJAAAAQwAAAA0A#####wAKZkRlY2ltYWwyMQAnKHVudW1kZXIwKmFeKG4tbjApLWNudW1kZXIyKSoxL3BwY21kZW5mAAAAAwMAAAADAgAAAAMBAAAAAwIAAAAJAAAAkAAAAAYAAAAJAAAACAAAAAMBAAAABQAAAAAAAAAJAAAADAAAAAkAAACRAAAAAT#wAAAAAAAAAAAACQAAAI8AAW4AAAANAP####8ACmZEZWNpbWFsMjIALChhbnVtZGVyNyphXihuLW4wKS1jbnVtZGVyMjIpKjEvcHBjbWRlbmZGcmFjAAAAAwMAAAADAgAAAAMBAAAAAwIAAAAJAAAAkwAAAAYAAAAJAAAACAAAAAMBAAAABQAAAAAAAAAJAAAADAAAAAkAAACUAAAAAT#wAAAAAAAAAAAACQAAAJIAAW4AAAANAP####8ACmZEZWNpbWFsMjMAKihhbnVtZGVyNSphXm4tY251bWRlcjIzKSoxL3BwY21kZW5mRnJhY0JpcwAAAAMDAAAAAwIAAAADAQAAAAMCAAAACQAAAJYAAAAGAAAACQAAAAgAAAAFAAAAAAAAAAkAAACXAAAAAT#wAAAAAAAAAAAACQAAAJUAAW4AAAANAP####8ACmZEZWNpbWFsMjQAJihhbnVtZGVyMyphXm4tY251bWRlcjI0KSoxL3BwY21kZW5mQmlzAAAAAwMAAAADAgAAAAMBAAAAAwIAAAAJAAAAmQAAAAYAAAAJAAAACAAAAAUAAAAAAAAACQAAAJoAAAABP#AAAAAAAAAAAAAJAAAAmAABbgAAAA4A#####wAMZWdhRGVjaW1hbDIxAAAAmwAAACABAAAAAAE#8AAAAAAAAAEAAAAOAP####8ADGVnYURlY2ltYWwyMgAAAJwAAAAgAQAAAAABP#AAAAAAAAABAAAADgD#####AAxlZ2FEZWNpbWFsMjMAAACdAAAAIAEAAAAAAT#wAAAAAAAAAQAAAA4A#####wAMZWdhRGVjaW1hbDI0AAAAngAAACABAAAAAAE#8AAAAAAAAAEAAAAPAP####8AAAAAAA0AAAH#####EUBo4AAAAAAAQHrwAAAAAAAAAAAAAAAAAAAAAA7DqWdhbGl0ZUZyYWMxPQAAAgAAAEj###############8='
    stor.liste = stor.mtgAppLecteur.createList(txtFig)
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut, nbRepet) {
    // cette fonction permet de vérifier si ce qui est renseigné est bien un intervalle ou un nombre (éventuellement fractionnaire) et ce plusieurs fois (en général nbrepetitions)
    // cela permet de valider ce qui peut être saisi par un utilisateur dans le cas où on peut imposer des valeurs
    // la fonction renvoie un tableau de nbRepet éléments
    // chaque élément est un intervalle voire l’intervalle par défaut si ce qui est précisé n’est aps compris
    // ce peut aussi être un nombre entier, décimal ou fractionnaire
    // dans ce dernier cas, on renvoit une chaine où on retrouve le code latex et la valeur décimale (arrondie) séparés par |
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp
    let monnum
    let monden
    let valFrac
    if (String(texteIntervalle).includes('|')) {
      const tableau = texteIntervalle.split('|')
      for (k = 0; k < nbRepet; k++) {
        if (testIntervalleFermeDecimaux.test(tableau[k])) {
          // c’est bien un intervalle'
          if (lgIntervalle(tableau[k]) === 0) {
            // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
            tabIntervalle.push(tableau[k].substring(1, tableau[k].indexOf(';')))
          } else {
            tabIntervalle.push(tableau[k])
          }
        } else {
          // peut être est-ce tout simplement un nombre :
          if ((tableau[k] === undefined) || (tableau[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(tableau[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tableau[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(tableau[k]))) {
            // on a un nombre (mais sous forme de string car tableau[k] est issu d’un split de string)
            tabIntervalle.push(tableau[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if ((texteIntervalle === '') || (texteIntervalle === undefined)) {
        for (k = 0; k < nbRepet; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (testIntervalleFermeDecimaux.test(String(texteIntervalle))) {
        if (lgIntervalle(String(texteIntervalle)) === 0) {
          // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
          texteIntervalle = String(texteIntervalle).substring(1, String(texteIntervalle).indexOf(';'))
        }
        for (k = 0; k < nbRepet; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < nbRepet; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < nbRepet; k++) {
          tabIntervalle.push(String(texteIntervalle))
        }
      } else {
        for (k = 0; k < nbRepet; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    // maintenant je garde en mémoire le fait que j’ai un nombre ou un intervalle
    const typeDonnees = []
    // tableau qui peut contenir les valeurs "intervalle", "nombre", "fraction" suivant le contenu de tabIntervalle
    for (let i = 0; i < nbRepet; i++) {
      if (tabIntervalle[i].charAt(0) === '[') {
        typeDonnees[i] = 'intervalle'
      } else if (tabIntervalle[i].includes('frac')) {
        typeDonnees[i] = 'fraction'
      } else {
        typeDonnees[i] = 'nombre'
      }
    }
    return { donnees: tabIntervalle, leType: typeDonnees }
  }

  function lgIntervalle (intervalle) {
    // cette fonction renvoie la longueur de l’intervalle'
    // si ce n’est pas un intervalle, alors elle renvoie 0'
    let lg = 0
    // var intervalleReg = new RegExp("\\[\\-?[0-9]{1,};\\-?[0-9]{1,}\\]","i");
    const intervalleReg = /\[-?[0-9]+(.|,)?[0-9]*;-?[0-9]+(.|,)?[0-9]*]/g // new RegExp('\\[\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,};\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,}\\]', 'i')
    if (intervalleReg.test(intervalle)) {
      // on a bien un intervalle
      const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
      const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
      lg = borneSup - borneInf
    }
    return lg
  }

  function genereAlea (int) {
    // int est un intervalle
    /// /mais on peut trouver des cas de figure où par faignantise, on aura utilisé cette fonction sans vérifier que int est un intervalle
    // Ce serait alors un nombre
    if (String(int).charAt(0) === '[') {
      // si les bornes sont entières, alors on génère un nombre entier aléatoire dans cet intervalle
      // si elles sont décimales, il faut que je génère un décimal de l’intervalle
      const PosPointVirgule = int.indexOf(';')
      let val1 = j3pNombre(int.substring(1, PosPointVirgule))
      let val2 = j3pNombre(int.substring(PosPointVirgule + 1, int.length - 1))
      let posPoint = -1
      if (!String(val1).includes('.')) {
        if (String(val2).includes('.')) {
          posPoint = String(val2).length - String(val2).lastIndexOf('.') - 1
        }
      } else {
        if (String(val2).includes('.')) {
          posPoint = Math.max(String(val1).length - String(val1).lastIndexOf('.') - 1, String(val2).length - String(val2).lastIndexOf('.') - 1)
        } else {
          posPoint = String(val1).length - String(val1).lastIndexOf('.') - 1
        }
      }
      // console.log('int:',int,'   posPoint:',posPoint)
      if (val1 > val2) {
        // les bornes ne sont pas dans le bon ordre
        const val3 = val1
        val1 = val2
        val2 = val3
      }
      if (posPoint === -1) {
        // je n’ai que des nombres entiers
        const [minInt, maxInt] = j3pGetBornesIntervalle(int)
        return j3pGetRandomInt(minInt, maxInt)
      } else {
        // j’ai des décimaux
        const borneInf = val1 * Math.pow(10, posPoint)
        const borneSup = val2 * Math.pow(10, posPoint)
        const nbAlea = j3pGetRandomInt(String(borneInf), String(borneSup)) * Math.pow(10, -posPoint)
        return Math.round(nbAlea * Math.pow(10, 10)) / Math.pow(10, 10)
      }
    } else {
      return int
    }
  }

  function convertDecimal (nb, nbreDecimales) {
    // nb peut être un entier, un décimal ou un nb fractionnaire
    // si c’est un nb fractionnaire et que la forme décimale contient moins de nbreDecimales, alors on renvoie le nb sous forme décimale
    // sinon on renvoit nb sous son format initial
    const nbDecimal = j3pCalculValeur(nb)
    // pour éviter les pbs d’arrondi, je cherche par quelle puissance de 10 multiplier mon nombre (puis el divisier)
    let puisPrecision = 15
    if (Math.abs(nbDecimal) > Math.pow(10, -13)) {
      const ordrePuissance = Math.floor(Math.log(Math.abs(nbDecimal)) / Math.log(10)) + 1
      puisPrecision = 15 - ordrePuissance
    }
    let newNb = nb
    if (Math.abs(Math.round(nbDecimal * Math.pow(10, puisPrecision)) / Math.pow(10, puisPrecision) - nbDecimal) < Math.pow(10, -12)) {
      // on a une valeur décimale
      // Si elle a plus de deux chiffres après la virgule, on donne une valeur décimale
      const nbChiffreApresVirgule = String(nbDecimal).length - 1 - String(nbDecimal).lastIndexOf('.')
      if (nbChiffreApresVirgule <= nbreDecimales) {
        newNb = nbDecimal
      }
    }
    return newNb
  }

  function ecoute (num) {
    if (stor.phraseRep.inputmqList[num].className.includes('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, stor.phraseRep.inputmqList[num], {
        liste: ['puissance', 'fraction', '(']
      })
    }
  }

  function fractionComplete (texte) {
    // cette fonction vérifie sur texte est l’équivalent d’un écriture sous la forme d’une fraction
    const testReg = /\(.+\)\/\([0-9]+\)/g // new RegExp('\\({1}.+\\){1}\\/\\({1}[0-9]+\\){1}', 'i')
    let posFrac
    let posInit
    if (testReg.test(texte)) {
      posFrac = texte.lastIndexOf('/')
      let numparferm = 1
      posInit = posFrac - 2// en position posFrac-2 on a nécessairement ')'
      while (posInit > -1) {
        if (texte[posInit] === ')') {
          numparferm++
        } else if (texte[posInit] === '(') {
          numparferm--
        }
        posInit--
        if (posInit === -1) {
          return true
        }
        if (numparferm === 0) {
          posInit = -1
        }
      }
    }
    return false
  }

  function IdentifierCoef (texte) {
    const puisRegExp = /\^\(n[+-][0-9]+\)/g // new RegExp('\\^\\(n[\\+\\-]+[0-9]+\\)', 'i') Je ne comprends pas la présence du 1er + (sans l’échapement) dans la regExp initiale
    const ajoutRegExp = /[+-][0-9]+/g // new RegExp('[\\+\\-]+[0-9]+', 'i') même remarque

    // le texte est une expression dans laquelle on cherche quelque chose de la forme a*q^{n+/-p}
    // cette fonction doit identifier l’expression a*q^{n+/-p} et identifier a, q et p
    // ici les nombres a et q ne sont pas au format latex
    // elle renvoie un objet de propriétés expresInit et expresFin
    // expressInit est a*q^{n-p} et expressFin est (a*q^{+/-2})*q^n
    let expressionInitiale = texte
    let expressionFinale = texte
    let a, coefa, q, p, a2

    function DernierCoef (monTexte) {
      // on part de la position finale de texte, le dernier caractère étant une parenthèse
      // on remonte alors jusqu'à ce qu’on trouve la parenthèse ouvrante correspondante
      // la fonction renvoie un objet avec l’expression du nombre en fin de chaine et la position du premier caractère qui n’est plus dans le nb
      const chiffreRegExp = /[0-9]+/i // new RegExp('[0-9]{1}', 'i')
      const dernierCaractere = monTexte[monTexte.length - 1]
      let posInit
      if (dernierCaractere === ')') {
        // Le dernier coef de l’expression est de la forme (...)
        posInit = monTexte.length - 2
        let nbparFermante = 1
        let coefIdentifie = false
        while (!coefIdentifie && (posInit > 0)) {
          if (monTexte[posInit] === '(') {
            if (nbparFermante === 1) {
              // je regarde tout de même si ce n’est pas un quotient de la forme (..)/(..)
              if (monTexte[posInit - 1] === '/') {
                // je suis dans ce cas avec une fraction de la forme (..)/(..)
                nbparFermante--
                posInit--
              } else {
                coefIdentifie = true
              }
            } else {
              nbparFermante--
              posInit--
            }
          } else {
            if (monTexte[posInit] === ')') {
              nbparFermante++
            }
            posInit--
          }
        }
        const leNombre = monTexte.substring(posInit, monTexte.length)// (estFraction)?monTexte.substring(posInit,monTexte.length):monTexte.substring(posInit+1,monTexte.length-1)
        return { nb: leNombre, monIndex: posInit }
      }
      // dernierCaractere !== ')', on continue
      if (chiffreRegExp.test(dernierCaractere)) {
        // le dernier coef est un nombre
        posInit = monTexte.length - 2
        let onContinue = true
        while (onContinue && (posInit >= 0)) {
          if (chiffreRegExp.test(monTexte[posInit]) || (monTexte[posInit] === '.') || (monTexte[posInit] === ',')) {
            posInit--
          } else {
            onContinue = false
          }
        }
        return { nb: monTexte.substring(posInit + 1, monTexte.length), monIndex: posInit + 1 }
      }
      // le dernier coef n’est pas un nombre
      return { nb: '', monIndex: monTexte.length - 1 }// mais ce cas de figure ne devrait pas arriver
    }

    if (puisRegExp.test(texte)) {
      const puissance = texte.match(puisRegExp)[0]
      p = puissance.match(ajoutRegExp)[0]// j’obtiens le nb p lors que la puissance est de la forme n+/-p
      if (p.charAt(0) === '+') {
        p = p.substring(1)
      }
      const decompTexte = texte.split(puisRegExp)
      const partieAGerer = decompTexte[0]
      let rechercheNb = DernierCoef(partieAGerer)
      q = rechercheNb.nb
      const coefq = ((rechercheNb.nb.charAt(0) === '(') && (rechercheNb.nb.charAt(rechercheNb.nb.length - 1) === ')')) ? q.substring(1, rechercheNb.nb.length - 1) : q

      const posInit = rechercheNb.monIndex - 1
      // maintenant il faut que je trouve le réel a
      if (partieAGerer[posInit] === '*') {
        rechercheNb = DernierCoef(partieAGerer.substring(0, posInit))
        a = rechercheNb.nb + '*'
        coefa = rechercheNb.nb
      } else if (partieAGerer[posInit] === '-') {
        a = '-'
        coefa = '-1'
      } else {
        a = ''
        coefa = '1'
      }
      let coefAvantPuissance = true// Cela va me servir pour reconstruire les éléments à remplacer
      if (((a === '-') || (a === '')) && (decompTexte[1].length > 0)) {
        // peut-être que le coef a est situé après la puissance
        const chiffreParMultRegExp = /[0-9(*]/i // new RegExp('[0-9\\(\\*]{1}', 'i')// on cherche si on a un chiffre, une parenthèse ou le signe de multiplication
        if (chiffreParMultRegExp.test(decompTexte[1][0])) {
          // On cherche l’élément qui suit cette multiplication
          let texteQuiSuit = decompTexte[1]
          if (texteQuiSuit[0] === '*') {
            // je ne prends pas en compte le signe de multiplication
            texteQuiSuit = texteQuiSuit.substring(1)
          }
          // Dans les RegExp initiale, le flag valait i, mais je pense que g est plus approprié
          const fracRegExp = /-?\(-?[0-9]+\)\/\([0-9]+\)/g // new RegExp('\\-?\\(\\-?[0-9]{1,}\\)\\/\\([0-9]{1,}\\)', 'i')// pour chercher une fraction
          const fracRegExp2 = /\(-?\(-?[0-9]+\)\/\([0-9]+\)\)/g // new RegExp('\\(\\-?\\(\\-?[0-9]{1,}\\)\\/\\([0-9]{1,}\\)\\)', 'i')// pour chercher une fraction
          const nbDecParRegExp = /\([+-]?[0-9]+\.[0-9]+\)/g // new RegExp('\\((\\-|\\+)?[0-9]{1,}\\.[0-9]{1,}\\)', 'i')// pour chercher un nombre décimal entre parenthèses
          const nbParRegExp = /\([+-]?[0-9]+\)/g // new RegExp('\\((\\-|\\+)?[0-9]{1,}\\)', 'i')// pour chercher un nombre entier entre parenthèses
          const nbDecRegExp = /[0-9]+\.[0-9]+/g // new RegExp('[0-9]{1,}\\.[0-9]{1,}', 'i')// pour chercher un nombre decimal positif
          const nbEntRegExp = /[0-9]+/g // new RegExp('[0-9]{1,}', 'i')// pour chercher un nombre positif
          const tabRegExp = [fracRegExp2, fracRegExp, nbDecParRegExp, nbParRegExp, nbDecRegExp, nbEntRegExp]
          for (let i = 0; i < tabRegExp.length; i++) {
            if (tabRegExp[i].test(texteQuiSuit)) {
              const tabDecomp = texteQuiSuit.match(tabRegExp[i])
              // console.log("tabDecomp:",tabDecomp,'   i=',i)
              if (texteQuiSuit.startsWith(tabDecomp[0])) {
                a2 = tabDecomp[0]
                if (i === 0) {
                  // il faut que je vire les parenthèses au début
                  if (tabDecomp[0][1] === '-') { // en plus j’ai un signe - au début
                    coefa = '(-' + tabDecomp[0].substring(3, tabDecomp[0].length - 1)
                  } else {
                    coefa = tabDecomp[0].substring(1, tabDecomp[0].length - 1)
                  }
                } else if (i === 2) { // il faut que je vire les parenthèses
                  coefa = tabDecomp[0].substring(1, tabDecomp[0].length - 1)
                } else {
                  coefa = tabDecomp[0]
                }
              }
              break
            }
          }
          expressionInitiale = q + '^(n' + j3pMonome(2, 0, p) + ')*' + a2
          coefAvantPuissance = false
        }
      }
      // console.log("coefa:"+coefa)
      let newCoefa = j3pFractionLatex(coefa)
      const newCoefq = j3pFractionLatex(coefq)
      // Je mets un garde-fou ici pour ne pas appliquer j3pGetLatexQuotient ou j3pGetLatexProduit à NaN
      // En effet, j3pGetLatexFrac n’aime pas du tout
      // C’est pour controuner des réponses d’élèves où les fractions ne sont pas complétée (\\frac{}{2} par exemple)
      if (isNaN(j3pCalculValeur(newCoefa)) || isNaN(j3pCalculValeur(newCoefq))) return { expressInit: Number.NaN, expressFin: Number.NaN }
      let puisCoefq = j3pGetLatexPuissance(newCoefq, String(Math.abs(p)))
      puisCoefq = (p > 0) ? puisCoefq : j3pGetLatexQuotient('1', puisCoefq)
      newCoefa = j3pMathquillXcas(j3pGetLatexProduit(newCoefa, puisCoefq))
      if (newCoefa === '1') {
        expressionFinale = q + '^n'
      } else if (newCoefa === '-1') {
        expressionFinale = '-' + q + '^n'
      } else {
        // c’est ici que newCoefa peut être négatif et sous forme fractionnaire
        if (j3pCalculValeur(newCoefa) < 0 && newCoefa.includes('/')) {
          // il est écrit sous la forme (-...)/(...) alors que je le voudrais de la forme -(...)/(...)
          newCoefa = '-(' + newCoefa.substring(2)
          if (rechercheNb.monIndex - 1 >= 0) {
            if (partieAGerer[rechercheNb.monIndex - 1] === '+') {
              // je le vire
              a = '+' + a
            }
          }
        }
        expressionFinale = newCoefa + '*' + q + '^n'
      }
      if (coefAvantPuissance) {
        expressionInitiale = a + q + '^(n' + j3pMonome(2, 0, p) + ')'
      }
      me.logIfDebug('a:', a, '  coefa:', newCoefa, '  q:', q, '  coefq:', coefq, '  p:', p, ' expressioninitiale:' + expressionInitiale, '  remplacée par ', expressionFinale)
    }
    return { expressInit: expressionInitiale, expressFin: expressionFinale }
  }

  function enleverDoublePuissance (nomZone) {
    // nomZone est unzone de saisie mathquill
    // si une faute de frappe entraine un truc du style ...^{^...}, alors il faut virer la première puissance qui est en trop
    const eltInput = (typeof nomZone === 'string') ? j3pElement(nomZone) : nomZone
    let texteSaisi = j3pValeurde(eltInput)
    let pos = texteSaisi.indexOf('^{^')
    while (pos > -1) {
      let posAccFermante = -1
      let nbAccOuvrante = 0
      let posIndex = pos + 1
      // tout le travail est là
      // je trouve l’accolade fermante
      while (posAccFermante === -1) {
        if (texteSaisi[posIndex] === '{') {
          nbAccOuvrante++
        } else if (texteSaisi[posIndex] === '}') {
          nbAccOuvrante--
          if (nbAccOuvrante === 0) {
            posAccFermante = posIndex
          }
        }
        posIndex++
      }
      const puissance = texteSaisi.substring(pos, posAccFermante + 1)
      const newPuissance = puissance.substring(2, puissance.length - 1)
      texteSaisi = texteSaisi.replace(puissance, newPuissance)
      pos = texteSaisi.indexOf('^{^')
    }
    $(eltInput).mathquill('latex', texteSaisi)
  } // fin enleverDoublePuissance

  function enleverDoubleSigne (texte) {
    // texte est la saisie d’un élève qu’on aurait peut-être modifiée (notamment avec cette histoire de puissance n-1)
    const tabSigneDouble = ['++', '+-', '-+', '--']
    const tabSigneEquivalent = ['+', '-', '-', '+']
    for (let i = 0; i < tabSigneDouble.length; i++) {
      while (texte.includes(tabSigneDouble[i])) {
        texte = texte.replace(tabSigneDouble[i], tabSigneEquivalent[i])
      }
    }
    return texte
  }

  function enonceInit () {
    // Construction de la page
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
    me.afficheTitre(textes.titre_exo)
    stor.defSuite = {}
    if (ds.donneesPrecedentes) {
      // on récupère les données du noeud précédent stockées dans me.donneesPersistantes.suites
      if (!me.donneesPersistantes.suites) {
        ds.donneesPrecedentes = false
        j3pShowError(Error('Les données précédentes n’ont pu être récupérées. De nouvelles sont générées !\nAttention car la nature de la suite n’est peut-être pas celle qui était attendue.'))
      }
    }
    if (ds.donneesPrecedentes) {
      ds.nbrepetitions = 1
      ds.nbitems = ds.nbetapes * ds.nbrepetitions
      // on récupère tout ce que ça contient pour le mettre dans stor
      Object.entries(me.donneesPersistantes.suites).forEach(([prop, value]) => {
        stor.defSuite[prop] = value
      })
      if (!stor.defSuite.termeInit && stor.defSuite.termeInitial) stor.defSuite.termeInit = [stor.defSuite.termeInitial.split('|')]
      stor.defSuite.typeSuite = ([arithmetique, geometrique].includes(stor.defSuite.typeSuite)) ? stor.defSuite.typeSuite : arithGeo
      stor.defSuite.coefB = {}
      stor.defSuite.coefA = {}
      if (stor.defSuite.typeSuite === arithmetique) {
        stor.defSuite.coefB = stor.defSuite.coefRDonnees
        stor.defSuite.coefB.leType = []
        stor.defSuite.coefB.donnees = []
        if (stor.defSuite.coefRDonnees.leType === 'fraction') {
          stor.defSuite.coefB.leType.push('fraction')
          stor.defSuite.coefB.donnees.push(stor.defSuite.coefRDonnees.expression[0] + '|' + stor.defSuite.coefRDonnees.valeur[0])
        } else {
          stor.defSuite.coefB.leType.push('nombre')
          stor.defSuite.coefB.donnees.push(stor.defSuite.coefRDonnees.expression[0])
        }
        // console.log('stor.defSuite.coefR:',stor.defSuite.coefRDonnees.leType,' coefB:',stor.defSuite.coefB.donnees)
      } else if (stor.defSuite.typeSuite === geometrique) {
        stor.defSuite.coefA.leType = []
        stor.defSuite.coefA.donnees = []
        if (stor.defSuite.coefRDonnees.leType === 'fraction') {
          stor.defSuite.coefA.leType.push('fraction')
          stor.defSuite.coefA.donnees.push(stor.defSuite.coefRDonnees.expression[0] + '|' + stor.defSuite.coefRDonnees.valeur[0])
        } else {
          stor.defSuite.coefA.leType.push('nombre')
          stor.defSuite.coefA.donnees.push(stor.defSuite.coefRDonnees.expression[0])
        }
        // console.log('stor.defSuite.coefR:',stor.defSuite.coefRDonnees.leType,' coefA:',stor.defSuite.coefA.donnees)
      }
      if (!stor.defSuite.niveau) stor.defSuite.niveau = 'TS'
    } else {
      stor.tabTypeSuite = []
      if (ds.typeSuite === arithmetique) {
        for (let k = 1; k <= ds.nbrepetitions; k++) {
          stor.tabTypeSuite.push(arithmetique)
        }
      } else if (ds.typeSuite === geometrique) {
        for (let k = 1; k <= ds.nbrepetitions; k++) {
          stor.tabTypeSuite.push(geometrique)
        }
      } else if ((ds.typeSuite.toLowerCase().includes('alea')) || (ds.typeSuite.toLowerCase().includes('aléa'))) {
        // donc là ce sera aléatoire
        const alea = j3pGetRandomInt(0, 1)
        const tabType = (alea === 0) ? [arithmetique, geometrique] : [geometrique, arithmetique]
        const tabTypeInit = []
        for (let k = 0; k < ds.nbrepetitions; k++) {
          tabTypeInit.push(tabType[k % 2])
        }
        stor.tabTypeSuite = j3pShuffle(tabTypeInit)
      } else {
        for (let k = 1; k <= ds.nbrepetitions; k++) {
          stor.tabTypeSuite.push(arithGeo)
        }
      }
    }
    stor.defSuite.niveau = (ds.niveau.toUpperCase() === 'TES') ? 'TES' : 'TS'
    if (stor.defSuite.niveau === 'TES') {
      if (ds.a === '[-1.1;2.1]') {
        // c’est qu’on a laissé a par défaut. Donc il faut que je lui mette la valeur par défaut du niveau TES
        ds.a = '[0.5;1.7]'
      }
    }
    if (!ds.donneesPrecedentes) {
      // on génère de manière aléatoire les variables et données de l’exercice
      const intervalleDefaut = (stor.defSuite.niveau === 'TES') ? '[0.5;1.7]' : '[-1.1;2.1]'
      stor.defSuite.coefA = constructionTabIntervalle(ds.a, intervalleDefaut, ds.nbrepetitions, [0, 1, -1])
      stor.defSuite.coefB = constructionTabIntervalle(ds.b, '[-0.5;1]', ds.nbrepetitions, [0])
      // il faut que je gère le terme initial car j’ai un tableau dont les données sont de la forme 1|[...;...] ce second point pouvant être un entier ou un nb fractionnaire
      stor.defSuite.termeInit = []
      const TermeInitParDefaut = [0, '[0.4;5]']
      if (!isNaN(j3pNombre(String(ds.termeInitial)))) {
        // au lieu d’un tableau, n’est indiqué qu’un seul nombre. Celui-ci devient le rang initial de chaque répétition
        const leRangInit = j3pNombre(ds.termeInitial)
        for (let k = 0; k < ds.nbrepetitions; k++) stor.defSuite.termeInit.push([leRangInit, TermeInitParDefaut[1]])
      }
      for (let k = 0; k < ds.nbrepetitions; k++) {
        if ((ds.termeInitial[k] === '') || (ds.termeInitial[k] === undefined)) {
          // on a rien renseigné donc par défaut, on met
          stor.defSuite.termeInit.push(TermeInitParDefaut)
        } else {
          // il faut la barre verticale pour séparer le rang et la valeur du terme initial
          const posBarre = String(ds.termeInitial[k]).indexOf('|')
          if (posBarre === -1) {
            const EntierReg = /[0-9]+/g // new RegExp('[0-9]{1,}', 'i')
            // je n’ai pas la barre verticale
            // peut-être ai-je seulement l’indice du premier terme
            if (EntierReg.test(ds.termeInitial[k])) {
              // c’est un entier correspondant à l’indice du terme initial
              stor.defSuite.termeInit.push([ds.termeInitial[k], TermeInitParDefaut[1]])
            } else {
              // donc je réinitialise avec la valeur par défaut
              stor.defSuite.termeInit.push(TermeInitParDefaut)
            }
          } else {
            // la valeur (éventuellement aléatoire) du terme initial
            const termInitAlea = constructionTabIntervalle(ds.termeInitial[k].substring(posBarre + 1), TermeInitParDefaut[1], 1).donnees[0]
            // au cas où je vérifie que la première valeur est bien entière
            let rangInit = ds.termeInitial[k].substring(0, posBarre)
            rangInit = Number.isInteger(j3pNombre(rangInit))
              ? rangInit
              : TermeInitParDefaut[0]
            stor.defSuite.termeInit.push([rangInit, termInitAlea])
          }
        }
      }
    }
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          initFigure()
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (!ds.donneesPrecedentes) {
      // typeSuite peut varier d’une question à l’autre. On relève ça d’un taleau
      stor.defSuite.typeSuite = stor.tabTypeSuite[me.questionCourante - 1]
    }
    let onRecommence = false
    let nbTentatives = 0
    do {
      if ((stor.defSuite.typeSuite === arithGeo) && (ds.donneesPrecedentes)) {
        // dans ce cas, nous sommes dans le cas d’une suite arithmético-géométrique où les données sont issues d’un graphe et donc de la section précédente
        stor.defSuite.u0 = stor.defSuite.termeInit[me.questionCourante - 1][1]
      } else {
        stor.defSuite.u0 = (String(stor.defSuite.termeInit[me.questionCourante - 1][1]).includes('|')) ? stor.defSuite.termeInit[me.questionCourante - 1][1].split('|')[0] : genereAlea(stor.defSuite.termeInit[me.questionCourante - 1][1])
      }
      if (Math.abs(j3pCalculValeur(stor.defSuite.u0)) > Math.pow(10, -12)) {
        let fracU0 = j3pGetLatexQuotient('1', stor.defSuite.u0)
        fracU0 = j3pGetLatexQuotient('1', fracU0)
        const decompU0 = j3pExtraireNumDen(fracU0)
        if (decompU0[0]) {
          stor.unum0 = decompU0[1]
          stor.uden0 = decompU0[2]
        } else {
          stor.unum0 = stor.defSuite.u0
          stor.uden0 = 1
        }
      } else {
        stor.unum0 = 0
        stor.uden0 = 1
      }
      let valeurb
      if (stor.defSuite.typeSuite === arithmetique) {
        // u_{n+1}=au_n+b où a=1 et b<>0
        do {
          stor.defSuite.b = (stor.defSuite.coefB.leType[me.questionCourante - 1] === 'intervalle') ? genereAlea(stor.defSuite.coefB.donnees[me.questionCourante - 1]) : stor.defSuite.coefB.donnees[me.questionCourante - 1]
          valeurb = stor.defSuite.b
          if (String(stor.defSuite.b).includes('|')) {
            // on a un nb fractionnaire
            valeurb = stor.defSuite.b.split('|')[1]
            stor.defSuite.b = stor.defSuite.b.split('|')[0]
          }
          nbTentatives++
          if (nbTentatives === 50) {
            // c’est que l’utilisateur a imposé une suite constante (b=0)
            // Je choisis de regénérer la valeur de b en lui donnant l’intervalle par défaut
            console.error(Error('Les données imposées génèrent une boucle infinie - on n’a pas la suite géométrique attendue'))
            stor.defSuite.coefB.donnees[me.questionCourante - 1] = '[-0.5;1]'
          }
        } while (Math.abs(valeurb) < Math.pow(10, -12) && nbTentatives < 100)
        if (nbTentatives >= 100) {
          console.error(Error('pas réussi à générer stor.defSuite.b'), stor.defSuite.b)
        }
        stor.defSuite.a = 1
      } else if (stor.defSuite.typeSuite === geometrique) {
        // u_{n+1}=au_n où a<>1 et a<>-1 et b=0
        do {
          if (stor.defSuite.coefA.leType[me.questionCourante - 1] === 'intervalle') {
            stor.defSuite.a = genereAlea(stor.defSuite.coefA.donnees[me.questionCourante - 1])
          } else {
            stor.defSuite.a = stor.defSuite.coefA.donnees[me.questionCourante - 1]
            if (String(stor.defSuite.a).includes('|')) {
              // on a un nb fractionnaire
              stor.defSuite.a = stor.defSuite.a.split('|')[0]
            }
          }
          nbTentatives++
          if (nbTentatives === 50) {
            // c’est que l’utilisateur a imposé une suite constante (a=1), une suite alternée (a=-1) ou une suite nulle à partir du rang 1 (a=0)
            // Je choisis de regénérer la valeur de a en lui donnant l’intervalle par défaut
            console.error(Error('Les données imposées génèrent une boucle infinie - on n’a pas la suite géométrique attendue'))
            stor.defSuite.coefA.leType[me.questionCourante - 1] = 'intervalle'
            stor.defSuite.coefA.donnees[me.questionCourante - 1] = '[0.1;2.1]'
          }
        } while ((Math.abs(Math.abs(stor.defSuite.a) - 1) < Math.pow(10, -12) || Math.abs(stor.defSuite.a) < Math.pow(10, -12)) && nbTentatives < 100)
        if (nbTentatives >= 100) {
          console.error(Error('pas réussi à générer stor.defSuite.a'), stor.defSuite.a)
        }
        stor.defSuite.b = 0
      } else {
        if ((!ds.donneesPrecedentes)) {
          // on doit générer les données pour cet exo
          // u_{n+1}=au_n+b
          // a doit être différent de 1 et -&
          if (stor.defSuite.coefA.leType[me.questionCourante - 1] === 'intervalle') {
            do {
              stor.defSuite.a = genereAlea(stor.defSuite.coefA.donnees[me.questionCourante - 1])
            } while ((Math.abs(Math.abs(stor.defSuite.a) - 1) < Math.pow(10, -12)) || (Math.abs(stor.defSuite.a) < Math.pow(10, -12)))
          } else {
            stor.defSuite.a = stor.defSuite.coefA.donnees[me.questionCourante - 1]
            if (String(stor.defSuite.a).includes('|')) {
              // on a un nb fractionnaire
              stor.defSuite.a = stor.defSuite.a.split('|')[0]
            }
          }
          // b est non nul
          if ((stor.defSuite.coefB.leType[me.questionCourante - 1] === 'intervalle')) {
            do {
              stor.defSuite.b = genereAlea(stor.defSuite.coefB.donnees[me.questionCourante - 1])
            } while (Math.abs(stor.defSuite.b) < Math.pow(10, -12))
          } else {
            stor.defSuite.b = stor.defSuite.coefB.donnees[me.questionCourante - 1]
            if (String(stor.defSuite.b).includes('|')) {
              // on a un nb fractionnaire
              stor.defSuite.b = stor.defSuite.b.split('|')[0]
            }
          }
        }
      }
      if (stor.defSuite.typeSuite === arithGeo) {
        // je prends la suite intermédiaire v_n=u_n-b/(1-a). Je note c=-b/(1-a)
        stor.defSuite.c = j3pGetLatexQuotient(j3pGetLatexProduit('-1', stor.defSuite.b), j3pGetLatexSomme('1', j3pGetLatexProduit((stor.defSuite.a), '-1')))
        // si c est une valeur décimale simple, alors je l’affiche sous forme décimale
        stor.defSuite.c = convertDecimal(stor.defSuite.c, 2)
        onRecommence = (Math.abs(j3pCalculValeur(stor.defSuite.u0) + j3pCalculValeur(stor.defSuite.c)) < Math.pow(10, -12))
        // console.log('onRecommence:',onRecommence,'  ',stor.defSuite.u0,' et ',stor.defSuite.c)
        if (nbTentatives === 50) {
          // c’est que je n’ai que des constantes qui génèrent une suite constante
          // Je choisis de regénérer la valeur de a en lui donnant l’intervalle par défaut
          console.error(Error('Les données imposées génèrent une boucle infinie - la raison de la suite devenant nulle. Je redonne à a l’intervalle par défaut'))
          // Suivant la section qui précède, coefA peut être de nature très différente

          do {
            stor.defSuite.a = genereAlea(constructionTabIntervalle('intervalle', '[0.1;2.1]', 1, [0, 1, -1]).donnees[0])
          } while ((Math.abs(Math.abs(stor.defSuite.a) - 1) < Math.pow(10, -12)) || (Math.abs(stor.defSuite.a) < Math.pow(10, -12)))
        }
      }
      nbTentatives++
    } while (onRecommence && nbTentatives < 70)
    if (nbTentatives === 70) console.error(Error('Il y a un souci avec les données imposées'))
    let fraca = j3pGetLatexQuotient('1', stor.defSuite.a)
    fraca = j3pGetLatexQuotient('1', fraca)
    const decompa = j3pExtraireNumDen(fraca)
    if (decompa[0]) {
      stor.anum = decompa[1]
      stor.aden = decompa[2]
    } else {
      stor.anum = stor.defSuite.a
      stor.aden = 1
    }
    const fracb = (Number(stor.defSuite.b) === 0) ? '0' : j3pGetLatexQuotient('1', j3pGetLatexQuotient('1', stor.defSuite.b))
    const decompb = j3pExtraireNumDen(fracb)
    if (decompb[0]) {
      stor.bnum = decompb[1]
      stor.bden = decompb[2]
    } else {
      stor.bnum = stor.defSuite.b
      stor.bden = 1
    }
    if (!ds.donneesPrecedentes) stor.defSuite.nomU = j3pGetRandomElts(['u', 'v'], 1)[0]
    if (stor.defSuite.typeSuite === arithGeo && !stor.defSuite.nomV) stor.defSuite.nomV = j3pGetRandomElts(['t', 'x', 'y', 'z'], 1)[0]
    // me.donneesPersistantes.suites sert à envoyer des informations dans le bilan qui pourront être récupérées par une autre section sur les suites
    // Ceci a pour but de poursuivre l’étude de la suite définie dans cette section
    me.donneesPersistantes.suites = {}
    for (const prop in stor.defSuite) me.donneesPersistantes.suites[prop] = stor.defSuite[prop]
    me.logIfDebug('me.donneesPersistantes.suites:', me.donneesPersistantes.suites)
    stor.laPalette = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1,
      {
        u: stor.defSuite.nomU,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        i: stor.defSuite.u0,
        j: j3pGetLatexMonome(1, 1, stor.defSuite.a, stor.defSuite.nomU + '_n') + j3pGetLatexMonome(2, 0, stor.defSuite.b, '')
      })
    if (stor.defSuite.typeSuite === arithmetique) {
      if (ds.afficherPhrase) {
        j3pAffiche(stor.zoneCons2, '', textes.consigne2_2,
          {
            u: stor.defSuite.nomU,
            r: stor.defSuite.b,
            n: stor.defSuite.termeInit[me.questionCourante - 1][0],
            i: stor.defSuite.u0
          })
      }
    } else if (stor.defSuite.typeSuite === geometrique) {
      if (ds.afficherPhrase) {
        j3pAffiche(stor.zoneCons2, '', textes.consigne2_1,
          {
            u: stor.defSuite.nomU,
            r: stor.defSuite.a,
            n: stor.defSuite.termeInit[me.questionCourante - 1][0],
            i: stor.defSuite.u0
          })
      }
    } else {
      j3pAffiche(stor.zoneCons2, '', textes.consigne2_3,
        {
          u: stor.defSuite.nomU,
          v: stor.defSuite.nomV,
          n: stor.defSuite.termeInit[me.questionCourante - 1][0],
          c: j3pGetLatexMonome(2, 0, stor.defSuite.c, '')
        })
      stor.defSuite.v0 = j3pGetLatexSomme(stor.defSuite.u0, stor.defSuite.c)
      if (ds.afficherPhrase) {
        j3pAffiche(stor.zoneCons3, '', textes.consigne3,
          {
            v: stor.defSuite.nomV,
            r: stor.defSuite.a,
            n: stor.defSuite.termeInit[me.questionCourante - 1][0],
            i: stor.defSuite.v0
          })
      }
      if (ds.UnDirect) {
        stor.phraseRep = j3pAffiche(stor.zoneCons4, '', textes.consigne4_1,
          {
            u: stor.defSuite.nomU,
            n: stor.defSuite.termeInit[me.questionCourante - 1][0],
            inputmq1: { texte: '' }
          })
        j3pPaletteMathquill(stor.laPalette, stor.phraseRep.inputmqList[0], {
          liste: ['puissance', 'fraction', '(']
        })
        mqRestriction(stor.phraseRep.inputmqList[0], regRestric, { commandes: ['puissance', 'fraction'] })
      } else {
        stor.phraseRep = j3pAffiche(stor.zoneCons4, '', textes.consigne4_2,
          {
            v: stor.defSuite.nomV,
            u: stor.defSuite.nomU,
            n: stor.defSuite.termeInit[me.questionCourante - 1][0],
            inputmq1: { texte: '' },
            inputmq2: { texte: '' }
          })
        j3pPaletteMathquill(stor.laPalette, stor.phraseRep.inputmqList[0], {
          liste: ['puissance', 'fraction', '(']
        })
        for (const i of [0, 1]) {
          stor.phraseRep.inputmqList[i].addEventListener('focusin', ecoute.bind(null, i))
          mqRestriction(stor.phraseRep.inputmqList[i], regRestric, { commandes: ['puissance', 'fraction'] })
        }
      }
      j3pFocus(stor.phraseRep.inputmqList[0])
    }
    if ((stor.defSuite.typeSuite === arithmetique) || (stor.defSuite.typeSuite === geometrique)) {
      // ce qui suit est la même consigne que la suite soit arithmétique ou géométrique
      stor.phraseRep = j3pAffiche(stor.zoneCons3, '', textes.consigne4_1,
        {
          u: stor.defSuite.nomU,
          n: stor.defSuite.termeInit[me.questionCourante - 1][0],
          inputmq1: { texte: '' }
        })
      const listeBoutons = (stor.defSuite.typeSuite === arithmetique) ? ['puissance', 'fraction'] : ['puissance', 'fraction', '(']
      j3pPaletteMathquill(stor.laPalette, stor.phraseRep.inputmqList[0], { liste: listeBoutons })
      mqRestriction(stor.phraseRep.inputmqList[0], regRestric, { commandes: ['puissance', 'fraction'] })
      j3pFocus(stor.phraseRep.inputmqList[0])
    }
    const mesZonesSaisie = []
    if (stor.defSuite.typeSuite === arithGeo) {
      mesZonesSaisie.push(stor.phraseRep.inputmqList[0].id)
      if (!ds.UnDirect) mesZonesSaisie.push(stor.phraseRep.inputmqList[1].id)
    } else {
      mesZonesSaisie.push(stor.phraseRep.inputmqList[0].id)
    }
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '5px' }) })

    // gestion des variables dans mtg32
    // stor.liste.giveFormula2("a",String(stor.defSuite.a))
    // stor.liste.giveFormula2("b",String(stor.defSuite.b))
    stor.liste.giveFormula2('anum', String(stor.anum))
    stor.liste.giveFormula2('aden', String(stor.aden))
    stor.liste.giveFormula2('bnum', String(stor.bnum))
    stor.liste.giveFormula2('bden', String(stor.bden))
    // stor.liste.giveFormula2("c",String(stor.defSuite.c))
    stor.liste.giveFormula2('n0', String(stor.defSuite.termeInit[me.questionCourante - 1][0]))
    // stor.liste.giveFormula2("u0",String(stor.defSuite.u0))
    stor.liste.giveFormula2('unum0', String(stor.unum0))
    stor.liste.giveFormula2('uden0', String(stor.uden0))
    // pour les affectations qui suivent, je pensais que cela allait être calculé par mtg32, mais ça ne marche pas
    // j’utilise dans le fichier mtg32 la fonction arrondir(x,puis) qui ne semble pas être prise en compte dans J3P
    // Du coup j’ai affecté les valeurs à la main...

    // La variable suivante est utilisée si l’élève donne la bonne expression de la suite, mais non simplifiée
    // on ne lui compte pas faux lors de la première tentative, mais on lui redonne une chance de simplifier son expression
    stor.alerteSimplification = false
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }

      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid
      // comme pour l’utilisation de Validation_zones, je déclare l’objet reponse qui contient deux propriétés : aRepondu et bonneReponse
      let reponse = { reponse: false, bonneReponse: false }
      me.logIfDebug('avant:', j3pValeurde(stor.phraseRep.inputmqList[0]))

      enleverDoublePuissance(fctsValid.zones.inputs[0])
      me.logIfDebug('après:', j3pValeurde(stor.phraseRep.inputmqList[0]))

      reponse = fctsValid.validationGlobale()
      let egaliteImages
      if (reponse.aRepondu) {
        // je teste ici la réponse de l’élève
        const repEleve = []
        const reponseEleve = []
        if ((stor.defSuite.typeSuite === arithmetique) || (stor.defSuite.typeSuite === geometrique)) {
          reponseEleve.push(j3pValeurde(stor.phraseRep.inputmqList[0]))
          repEleve.push(j3pMathquillXcas(reponseEleve[0]))
        } else {
          reponseEleve.push(j3pValeurde(stor.phraseRep.inputmqList[0]))
          repEleve.push(j3pMathquillXcas(reponseEleve[0]))
          if (!ds.UnDirect) {
            reponseEleve.push(j3pValeurde(stor.phraseRep.inputmqList[1]))
            repEleve.push(j3pMathquillXcas(reponseEleve[1]))
          }
        }
        // console.log("repeleve:"+repEleve)
        for (let k = 0; k < repEleve.length; k++) repEleve[k] = j3pAjouteFoisDevantX(repEleve[k], 'n')
        if (stor.defSuite.typeSuite === arithmetique) {
          me.logIfDebug('repEleve avant changements :', repEleve)
          const FracRegExp = /^\(.+\)\/\([0-9]+\)/i // new RegExp('^\\(.+\\)/\\([0-9]{1,}\\)$', 'i')
          if (FracRegExp.test(repEleve[0])) {
            const tabFrac = repEleve[0].match(FracRegExp)[0].split('/')
            const deno = tabFrac[1].substring(1, tabFrac[1].length - 1)
            // le numérateur doit être une fonction affine de n
            const tabCoefs = j3pExtraireCoefsFctAffine(tabFrac[0].substring(1, tabFrac[0].length - 1), 'n')
            const pgcdAffine = []
            pgcdAffine[0] = j3pPGCD(Math.abs(Number(tabCoefs[0])), Math.abs(Number(deno)))
            pgcdAffine[1] = j3pPGCD(Math.abs(Number(tabCoefs[1])), Math.abs(Number(deno)))
            const signeQuot = []
            const newNum = []
            const newDen = []
            const coefRep = []
            if (j3pPGCD(pgcdAffine[0], pgcdAffine[1]) === 1) {
              // on a un quotient qui est écrit de manière simplifiée
              // maintenant il faut que je vérifie s’il est le bon
              for (let j = 0; j <= 1; j++) {
                signeQuot[j] = (Number(tabCoefs[j]) / Number(deno) > 0) ? '+' : '-'
                newNum.push(Math.abs(Number(tabCoefs[j]) / pgcdAffine[j]))
                newDen.push(Math.abs(Number(deno) / pgcdAffine[j]))
                coefRep[j] = (Math.abs(Math.round(newNum[j] / newDen[j]) - newNum[j] / newDen[j]) < Math.pow(10, -12)) ? newNum[j] / newDen[j] : '(' + newNum[j] + ')/(' + newDen[j] + ')'
              }
              signeQuot[0] = (signeQuot[0] === '+') ? '' : '-'
              repEleve[0] = signeQuot[0] + coefRep[0] + '*n' + signeQuot[1] + coefRep[1]
              me.logIfDebug('repEleve après changements :', repEleve)
            }
          }
        } else {
          // il y a des cas de figure auxquels je ne peux pas penser mais qui doivent être acceptés
          // c’est lorsque a*q^n peut être écrit (aq)*q^{n-1} par exemple...
          for (let k = 0; k < repEleve.length; k++) {
            try {
              // Si la réponse de l’élève est vraiment du grand n’importe quoi, il peut arriver qua ça plante
              // Avec ce try catch, on évite le plantage et il est certain que l’élève aura faux avec ce qui suit
              const substitution = IdentifierCoef(repEleve[k])// c’est un objet
              me.logIfDebug('substitution.expressInit:', substitution.expressInit, '   substitution.expressFin:', substitution.expressFin)
              if (isNaN(substitution.expressInit) && isNaN(substitution.expressInit)) {
                repEleve[k] = repEleve[k].replace(substitution.expressInit, substitution.expressFin)
                repEleve[k] = enleverDoubleSigne(repEleve[k])
              }
            } catch (error) {
              console.error(error)
            }
          }
          me.logIfDebug('repEleve après:', repEleve)
        }
        // console.log('reponseEleve:',reponseEleve,'  repEleve:',repEleve)

        if (stor.defSuite.typeSuite === geometrique) {
          stor.liste.giveFormula2('repgeo', repEleve[0])
        } else if (stor.defSuite.typeSuite === arithmetique) {
          stor.liste.giveFormula2('rep', repEleve[0])
        } else {
          if (ds.UnDirect) {
            stor.liste.giveFormula2('rep', repEleve[0])
          } else {
            stor.liste.giveFormula2('repgeo', repEleve[0])
            stor.liste.giveFormula2('rep', repEleve[1])
          }
        }
        stor.liste.calculateNG()
        if (stor.defSuite.typeSuite === geometrique) {
          // une seule réponse attendue
          reponse.bonneReponse = (((stor.liste.valueOf('egaliteGeo') === 1) || (stor.liste.valueOf('egaliteGeoBis') === 1) || (stor.liste.valueOf('egaliteGeoFrac') === 1) || (stor.liste.valueOf('egaliteGeoFracBis') === 1)))
          me.logIfDebug('egalites:', stor.liste.valueOf('egaliteGeo'), stor.liste.valueOf('egaliteGeoBis'), stor.liste.valueOf('egaliteGeoFrac'), stor.liste.valueOf('egaliteGeoFracBis'))
          if (!reponse.bonneReponse) {
            // il reste encore tous les cas de figure avec des valeurs décimales à vérifier
            for (let k = 1; k <= 8; k++) {
              reponse.bonneReponse = (reponse.bonneReponse || (stor.liste.valueOf('egaDecGeo' + k) === 1))
            }
          }
          fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
        } else if (stor.defSuite.typeSuite === arithmetique) {
          // une seule réponse attendue
          reponse.bonneReponse = (((stor.liste.valueOf('egalite2') === 1) || (stor.liste.valueOf('egaliteFrac2') === 1)))
          if (!reponse.bonneReponse) {
            // il reste encore tous les cas de figure avec des valeurs décimales à vérifier
            for (let k = 1; k <= 2; k++) {
              reponse.bonneReponse = (reponse.bonneReponse || (stor.liste.valueOf('egaDecArit' + k) === 1))
            }
          }
          fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
        } else {
          if (ds.UnDirect) {
            // une seule réponse attendue
            reponse.bonneReponse = (((stor.liste.valueOf('egalite1') === 1) || (stor.liste.valueOf('egalitebis1') === 1) || (stor.liste.valueOf('egaliteFrac1') === 1) || (stor.liste.valueOf('egaliteFracbis1') === 1)))
            fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
            if (!fctsValid.zones.bonneReponse[0]) {
              // il reste encore tous les cas de figure avec des valeurs décimales à vérifier
              for (let k = 1; k <= 24; k++) {
                fctsValid.zones.bonneReponse[0] = (fctsValid.zones.bonneReponse[0] || (stor.liste.valueOf('egaDecimal' + k) === 1))
                // console.log(k+':'+(stor.liste.valueOf('egaDecimal'+k)))
              }
              reponse.bonneReponse = fctsValid.zones.bonneReponse[0]
            }
          } else {
            fctsValid.zones.bonneReponse[0] = (((stor.liste.valueOf('egaliteGeo') === 1) || (stor.liste.valueOf('egaliteGeoBis') === 1) || (stor.liste.valueOf('egaliteGeoFrac') === 1) || (stor.liste.valueOf('egaliteGeoFracBis') === 1)))
            if (!fctsValid.zones.bonneReponse[0]) {
              // il reste encore tous les cas de figure avec des valeurs décimales à vérifier
              for (let k = 1; k <= 12; k++) {
                fctsValid.zones.bonneReponse[0] = (fctsValid.zones.bonneReponse[0] || (stor.liste.valueOf('egaDecGeo' + k) === 1))
              }
            }
            fctsValid.zones.bonneReponse[1] = (((stor.liste.valueOf('egalite1') === 1) || (stor.liste.valueOf('egalitebis1') === 1) || (stor.liste.valueOf('egaliteFrac1') === 1) || (stor.liste.valueOf('egaliteFracbis1') === 1)))
            if (!fctsValid.zones.bonneReponse[1]) {
              // il reste encore tous les cas de figure avec des valeurs décimales à vérifier
              for (let k = 1; k <= 24; k++) {
                fctsValid.zones.bonneReponse[1] = (fctsValid.zones.bonneReponse[1] || (stor.liste.valueOf('egaDecimal' + k) === 1))
              }
            }
            reponse.bonneReponse = (fctsValid.zones.bonneReponse[0] && fctsValid.zones.bonneReponse[1])
          }
        }
        me.logIfDebug('n0:', stor.liste.valueOf('egaliteGeo'), '  reponse:', stor.liste.getFormula('rep'), ' reEleve[0]:', repEleve[0], '  fFrac:', stor.liste.getLatexFormula('fFrac'), '   egaDecimal3:', stor.liste.valueOf('egaDecimal3'))
        // Pour être bonne, la réponse doit être égale à ce qui est attendu et doit être simplifiée
        // Pour un premier essai, on peut ne pas comptabiliser la tentative où l’élève aurait la bonne réponse mais non simplifiée
        if (!reponse.bonneReponse) {
          egaliteImages = true
          for (let k2 = 4; k2 <= 10; k2++) {
            stor.liste.giveFormula2('nval', String(k2))
            stor.liste.calculateNG()
            if (stor.defSuite.typeSuite === geometrique) {
              egaliteImages = (egaliteImages && (stor.liste.valueOf('imagesEgales3') === 1))
            } else if (stor.defSuite.typeSuite === arithmetique) {
              egaliteImages = (egaliteImages && (stor.liste.valueOf('imagesEgales2') === 1))
            } else {
              egaliteImages = (egaliteImages && (stor.liste.valueOf('imagesEgales') === 1))
              if (!ds.UnDirect) {
                egaliteImages = (egaliteImages && (stor.liste.valueOf('imagesEgales3') === 1))
              }
            }
            // console.log('nbval:', k2, 'egalite:', egaliteImages)
          }
          if (egaliteImages) {
            if (stor.defSuite.typeSuite === arithGeo) {
              if (ds.UnDirect) {
                if (fractionComplete(repEleve[0])) {
                  // bon là je triche mais je considère que s’il a écrit une fraction globale et que celle-ci correspond à ce qu’on attend, alors la réponse sera acceptée
                  reponse.bonneReponse = fctsValid.zones.bonneReponse[0] = true
                }
              } else {
                if (fractionComplete(repEleve[1])) {
                  // bon là je triche mais je considère que s’il a écrit une fraction globale et que celle-ci correspond à ce qu’on attend, alors la réponse sera acceptée
                  fctsValid.zones.bonneReponse[1] = true
                  reponse.bonneReponse = (fctsValid.zones.bonneReponse[0] && fctsValid.zones.bonneReponse[1])
                }
              }
            }
            if (stor.alerteSimplification) {
              stor.alerteSimplification = true
              me.essaiCourant--
            }
          }
        }
        if (stor.defSuite.typeSuite === arithGeo) {
          fctsValid.coloreUneZone(stor.phraseRep.inputmqList[0].id)
          if (!ds.UnDirect) {
            fctsValid.coloreUneZone(stor.phraseRep.inputmqList[1].id)
          }
        } else {
          fctsValid.coloreUneZone(stor.phraseRep.inputmqList[0].id)
        }
      }
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        return me.finCorrection()
      }

      // Une réponse a été saisie
      if (reponse.bonneReponse) {
        // Bonne réponse
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        afficheCorr(true)// affichage de la correction
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux
      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        afficheCorr(false)// affichage de la correction
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      if (egaliteImages) {
        if ((stor.defSuite.typeSuite === arithGeo) && !ds.UnDirect) {
          stor.zoneCorr.innerHTML += textes.comment2 + '<br/>'
        } else {
          stor.zoneCorr.innerHTML = textes.comment1 + '<br/>'
        }
      }
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        return me.finCorrection()
      }
      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      afficheCorr(false)// affichage de la correction
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
