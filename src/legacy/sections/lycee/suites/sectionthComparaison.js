import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pChaine, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomElt, j3pGetRandomInt, j3pPaletteMathquill, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction, traiteMathQuillSansMultImplicites } from 'src/lib/mathquill/functions'
import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Rémi DENIAUD
        décembre 2019
        Cette section permet, en plusieurs étapes de déterminer la limite d’ue suite en utilisant un théorème de comparaison ou le théorème des gendarmes
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeTh', 'alea', 'liste', 'type de théorème appliqué pour la suite proposée', ['alea', 'comparaison', 'gendarmes']]
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Limite d’une suite à l’aide d’un théorème de comparaison',
  // on donne les phrases de la consigne
  consigne1: 'On définit sur $£d$ la suite $(£u_n)$ par&nbsp;: $£u_n=£{un}$.',
  nonnul: ' non nul',
  // consigne2 : "$n$ étant un entier naturel£n, en partant de l’encadrement le plus précis qui soit de $£f$, déterminer le plus meilleur encadrement possible de $£u_n$.",
  consigne2: '$n$ étant un entier naturel£n, en partant du plus petit encadrement possible de $£f$, déterminer le meilleur encadrement de $£u_n$.',
  consigne3: 'On a montré que, pour tout entier naturel£n $n$, $£e$.',
  consigne4: 'Cliquer ci-dessous sur la (ou les) limite(s) indispensable(s) pour conclure sur la limite de $(£u_n)$&nbsp;:',
  consigne5: 'seulement $\\limite{n}{+\\infty}{£{vn}}$',
  consigne6: 'deux limites : $\\limite{n}{+\\infty}{£{vn}}$ et $\\limite{n}{+\\infty}{£{wn}}$',
  consigne7: 'Comme $\\limite{n}{+\\infty}{£{vn}}=$&1&, en déduit que $\\limite{n}{+\\infty}{£u_n}=$&2&.',
  consigne8: 'Comme $\\limite{n}{+\\infty}{£{vn}}=$&1& et que $\\limite{n}{+\\infty}{£{wn}}=$&2&,<br/> on en déduit que $\\limite{n}{+\\infty}{£u_n}=$&3&.',
  indic: 'Les termes de l’encadrement seront donnés sous forme développée et de sorte qu’il soit simple de calculer leurs limites.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Il faut sélectionner l’une des propositions&nbsp;!',
  comment2: 'Cette limite ne permet pas de répondre à la question&nbsp;!',
  comment3: 'Cette limite ne suffit pas pour répondre à la question&nbsp;!',
  comment4: 'Ces deux limites ne sont pas indispensables pour répondre à la question&nbsp;!',
  comment5: 'Il y a un problème avec les inégalités&nbsp;!',
  comment6: 'Au moins une expression n’est pas sous la forme attendue, c’est-à-dire simplifiée ou permettant de calculer sa limite&nbsp;!&nbsp;!',
  comment7: 'De plus, un signe d’inégalité peut être faux, voire les deux.',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Sachant que pour tout entier naturel£n $n$, $-1\\leq £f\\leq 1$, on obtient $£{e2}$ ce qui équivaut à $£{e3}$',
  corr1_2: 'Sachant que pour tout entier naturel£n $n$, $-1\\leq £f\\leq 1$, on obtient $£{e3}$',
  corr2: 'Comme $\\limite{n}{+\\infty}{£{vn}}=+\\infty$ et que $£u_n\\geq £{vn}$ pour $n$ assez grand, on conclut que $\\limite{n}{+\\infty}{£u_n}=+\\infty$.',
  corr3: 'Comme $\\limite{n}{+\\infty}{£{vn}}=-\\infty$ et que $£u_n\\leq £{vn}$ pour $n$ assez grand, on conclut que $\\limite{n}{+\\infty}{£u_n}=-\\infty$.',
  corr4: 'Comme $\\limite{n}{+\\infty}{£{vn}}=£l$, que $\\limite{n}{+\\infty}{£{wn}}=£l$ et que $£{vn}\\leq £u_n\\leq £{wn}$ pour $n$ assez grand, on conclut, d’après le théorème des gendarmes, que $\\limite{n}{+\\infty}{£u_n}=£l$.'
}
/**
 * section thComparaison
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  const txtFig = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAN1AAADrQAAAQEAAAAAAAAAAQAAABT#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAFiAAItM#####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQAgAAAAAAAAAAAACAP####8AAWMAAi0yAAAAAwAAAAFAAAAAAAAAAAAAAAIA#####wABZAACLTgAAAADAAAAAUAgAAAAAAAAAAAAAgD#####AAFlAAItMQAAAAMAAAABP#AAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wABZgAVYSpuXjIrYipuK2MrZC9uK2Uvbl4y#####wAAAAEACkNPcGVyYXRpb24AAAAABQAAAAAFAAAAAAUAAAAABQL#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAAf####8AAAABAApDUHVpc3NhbmNl#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAFAAAAAAAAAAAAAAAUCAAAABgAAAAIAAAAIAAAAAAAAAAYAAAADAAAABQMAAAAGAAAABAAAAAgAAAAAAAAABQMAAAAGAAAABQAAAAcAAAAIAAAAAAAAAAFAAAAAAAAAAAABbgAAAAQA#####wADcmVwAA4oLTIqbl4yKzMpL25eMgAAAAUDAAAABQAAAAADAAAABQIAAAABQAAAAAAAAAAAAAAHAAAACAAAAAAAAAABQAAAAAAAAAAAAAABQAgAAAAAAAAAAAAHAAAACAAAAAAAAAABQAAAAAAAAAAAAW7#####AAAAAwAQQ1Rlc3RFcXVpdmFsZW5jZQD#####AAplcXVpdmFsZW50AAAABgAAAAcBAAAAAAE#8AAAAAAAAAEAAAACAP####8AAmExAAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAJiMQABMAAAAAEAAAAAAAAAAAAAAAIA#####wACYzEAAi0yAAAAAwAAAAFAAAAAAAAAAAAAAAIA#####wACZDEAATAAAAABAAAAAAAAAAAAAAACAP####8AAmUxAAEzAAAAAUAIAAAAAAAAAAAABAD#####AAFnABphMSpuXjIrYjEqbitjMStkMS9uK2UxL25eMgAAAAUAAAAABQAAAAAFAAAAAAUAAAAABQIAAAAGAAAACQAAAAcAAAAIAAAAAAAAAAFAAAAAAAAAAAAAAAUCAAAABgAAAAoAAAAIAAAAAAAAAAYAAAALAAAABQMAAAAGAAAADAAAAAgAAAAAAAAABQMAAAAGAAAADQAAAAcAAAAIAAAAAAAAAAFAAAAAAAAAAAABbgAAAAQA#####wAEcmVwMQADMypuAAAABQIAAAABQAgAAAAAAAAAAAAIAAAAAAABbgAAAAkA#####wALZXF1aXZhbGVudDEAAAAOAAAADwEAAAAAAT#wAAAAAAAAAQAAAAIA#####wACbjEAATUAAAABQBQAAAAAAAAAAAACAP####8ACGVnYWxpdGUxARJhYnMoZihuMSktcmVwKG4xKSk8MTBeKC04KSZhYnMoZihuMSsxKS1yZXAobjErMSkpPDEwXigtOCkmYWJzKGYobjErMiktcmVwKG4xKzIpKTwxMF4oLTgpJmFicyhmKG4xKzMpLXJlcChuMSszKSk8MTBeKC04KSZhYnMoZihuMSs0KS1yZXAobjErNCkpPDEwXigtOCkmYWJzKGYobjErNSktcmVwKG4xKzUpKTwxMF4oLTgpJmFicyhmKG4xKzYpLXJlcChuMSs2KSk8MTBeKC04KSZhYnMoZihuMSs3KS1yZXAobjErNykpPDEwXigtOCkmYWJzKGYobjErOCktcmVwKG4xKzgpKTwxMF4oLTgpAAAABQoAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUE#####wAAAAIACUNGb25jdGlvbgAAAAAFAf####8AAAABAA5DQXBwZWxGb25jdGlvbgAAAAYAAAAGAAAAEQAAAAsAAAAHAAAABgAAABEAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAGAAAABQAAAAAGAAAAEQAAAAE#8AAAAAAAAAAAAAsAAAAHAAAABQAAAAAGAAAAEQAAAAE#8AAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAAAYAAAAFAAAAAAYAAAARAAAAAUAAAAAAAAAAAAAACwAAAAcAAAAFAAAAAAYAAAARAAAAAUAAAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAABgAAAAUAAAAABgAAABEAAAABQAgAAAAAAAAAAAALAAAABwAAAAUAAAAABgAAABEAAAABQAgAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAGAAAABQAAAAAGAAAAEQAAAAFAEAAAAAAAAAAAAAsAAAAHAAAABQAAAAAGAAAAEQAAAAFAEAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAAAYAAAAFAAAAAAYAAAARAAAAAUAUAAAAAAAAAAAACwAAAAcAAAAFAAAAAAYAAAARAAAAAUAUAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAABgAAAAUAAAAABgAAABEAAAABQBgAAAAAAAAAAAALAAAABwAAAAUAAAAABgAAABEAAAABQBgAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAGAAAABQAAAAAGAAAAEQAAAAFAHAAAAAAAAAAAAAsAAAAHAAAABQAAAAAGAAAAEQAAAAFAHAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAAAYAAAAFAAAAAAYAAAARAAAAAUAgAAAAAAAAAAAACwAAAAcAAAAFAAAAAAYAAAARAAAAAUAgAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAACAP####8ACGVnYWxpdGUyARthYnMoZyhuMSktcmVwMShuMSkpPDEwXigtOCkmYWJzKGcobjErMSktcmVwMShuMSsxKSk8MTBeKC04KSZhYnMoZyhuMSsyKS1yZXAxKG4xKzIpKTwxMF4oLTgpJmFicyhnKG4xKzMpLXJlcDEobjErMykpPDEwXigtOCkmYWJzKGcobjErNCktcmVwMShuMSs0KSk8MTBeKC04KSZhYnMoZyhuMSs1KS1yZXAxKG4xKzUpKTwxMF4oLTgpJmFicyhnKG4xKzYpLXJlcDEobjErNikpPDEwXigtOCkmYWJzKGcobjErNyktcmVwMShuMSs3KSk8MTBeKC04KSZhYnMoZyhuMSs4KS1yZXAxKG4xKzgpKTwxMF4oLTgpAAAABQoAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUEAAAACgAAAAAFAQAAAAsAAAAOAAAABgAAABEAAAALAAAADwAAAAYAAAARAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAADgAAAAUAAAAABgAAABEAAAABP#AAAAAAAAAAAAALAAAADwAAAAUAAAAABgAAABEAAAABP#AAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAOAAAABQAAAAAGAAAAEQAAAAFAAAAAAAAAAAAAAAsAAAAPAAAABQAAAAAGAAAAEQAAAAFAAAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAAA4AAAAFAAAAAAYAAAARAAAAAUAIAAAAAAAAAAAACwAAAA8AAAAFAAAAAAYAAAARAAAAAUAIAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAADgAAAAUAAAAABgAAABEAAAABQBAAAAAAAAAAAAALAAAADwAAAAUAAAAABgAAABEAAAABQBAAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAOAAAABQAAAAAGAAAAEQAAAAFAFAAAAAAAAAAAAAsAAAAPAAAABQAAAAAGAAAAEQAAAAFAFAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAAA4AAAAFAAAAAAYAAAARAAAAAUAYAAAAAAAAAAAACwAAAA8AAAAFAAAAAAYAAAARAAAAAUAYAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAADgAAAAUAAAAABgAAABEAAAABQBwAAAAAAAAAAAALAAAADwAAAAUAAAAABgAAABEAAAABQBwAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAOAAAABQAAAAAGAAAAEQAAAAFAIAAAAAAAAAAAAAsAAAAPAAAABQAAAAAGAAAAEQAAAAFAIAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAA################'
  // stor.cListeObjets affecté affecté au chargement de mtg, c’est un CListeObjets
  function traiteMathQuill (ch) {
    ch = traiteMathQuillSansMultImplicites(ch)
    ch = stor.cListeObjets.addImplicitMult(ch)// Traitement des multiplications implicites
    return ch
  }

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let k = 1; k <= 3; k++) stor['zoneExpli' + k] = j3pAddElt(zoneExpli, 'div')
    j3pDetruit(stor.laPalette)
    if (me.questionCourante % ds.nbetapes === 1) {
      j3pDetruit(stor.indic)
      if ((stor.typeQuest[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === 'comparaison') && (stor.choixCasFigure === 1) && (Math.abs(stor.c - 1) < Math.pow(10, -10))) {
        j3pAffiche(stor.zoneExpli1, '', textes.corr1_2, stor.objCons)
      } else {
        j3pAffiche(stor.zoneExpli1, '', textes.corr1_1, stor.objCons)
      }
    } else {
      let correction2 = textes.corr2
      if (stor.typeQuest[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === 'comparaison') {
        if (stor.a < 0) {
          correction2 = textes.corr3
        }
      } else {
        stor.objCons.l = stor.a
        correction2 = textes.corr4
      }
      j3pAffiche(stor.zoneExpli1, '', correction2, stor.objCons)
    }
  }

  function ecoute (num) {
    if (num < 2) {
      if (stor.elt.inputmqList[num].className.indexOf('mq-editable-field') > -1) {
        j3pEmpty(stor.laPalette)
        j3pPaletteMathquill(stor.laPalette, stor.elt.inputmqList[num], {
          liste: ['puissance', 'fraction', 'sin', 'cos']
        })
      }
    } else {
      if (stor.elt2.inputmqList[num - 2].className.indexOf('mq-editable-field') > -1) {
        j3pEmpty(stor.laPalette)
        j3pPaletteMathquill(stor.laPalette, stor.elt2.inputmqList[num - 2], {
          liste: ['inf', 'fraction']
        })
      }
    }
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (me.questionCourante % ds.nbetapes === 1) {
      // Nouvel exemple
      stor.objCons = {}
      stor.objCons.styletexte = {}
      stor.objCons.u = j3pGetRandomElt(['u', 'v'])
      stor.objCons.f = (stor.choixFct[(me.questionCourante - 1) / ds.nbetapes] === 'sin')
        ? '\\mathrm{sin}(n)'
        : (stor.choixFct === 'cos')
            ? '\\mathrm{cos}(n)'
            : '(-1)^n'
      stor.coefmtgGauche = []// liste des coefficients du membre de gauche a, b, c, d et e de l’expression an^2+bn+c+d/n+e/n^2
      stor.coefmtgDroite = []// liste des coefficients du membre de droite  a, b, c, d et e de l’expression an^2+bn+c+d/n+e/n^2
      if (stor.typeQuest[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === 'comparaison') {
        stor.choixCasFigure = j3pGetRandomInt(1, 2)
        if (stor.choixCasFigure === 1) {
          // L’exemple choisi sera de la forme a*n^k+b*n^{k-1}+c*sin(n) où sin(n) peut être remplacé par cos(n) ou (-1)^n,
          // si k=2, a et b seront du même signe (b pouvant être nul)
          // si k=1, on s’en moque
          stor.objCons.d = '\\N'
          stor.k = j3pGetRandomInt(1, 2)
          stor.a = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
          stor.b = (stor.k === 2)
            ? (stor.a / Math.abs(stor.a)) * j3pGetRandomInt(0, 3)
            : (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 6)
          stor.c = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 4)
          stor.objCons.n = ''
          stor.objCons.un = j3pGetLatexMonome(1, stor.k, stor.a, 'n') + j3pGetLatexMonome(2, stor.k - 1, stor.b, 'n') + j3pGetLatexMonome(3, 1, stor.c, stor.objCons.f)
          if (stor.k === 2) {
            stor.membreGauche = (stor.c > 0)
              ? j3pGetLatexMonome(1, stor.k, stor.a, 'n') + j3pGetLatexMonome(2, stor.k - 1, stor.b, 'n') + j3pGetLatexMonome(3, 0, -stor.c, stor.objCons.f)
              : j3pGetLatexMonome(1, stor.k, stor.a, 'n') + j3pGetLatexMonome(2, stor.k - 1, stor.b, 'n') + j3pGetLatexMonome(3, 0, stor.c, stor.objCons.f)
            stor.membreDroite = (stor.c > 0)
              ? j3pGetLatexMonome(1, stor.k, stor.a, 'n') + j3pGetLatexMonome(2, stor.k - 1, stor.b, 'n') + j3pGetLatexMonome(3, 0, stor.c, stor.objCons.f)
              : j3pGetLatexMonome(1, stor.k, stor.a, 'n') + j3pGetLatexMonome(2, stor.k - 1, stor.b, 'n') + j3pGetLatexMonome(3, 0, -stor.c, stor.objCons.f)
          } else {
            stor.membreGauche = (stor.c > 0)
              ? j3pGetLatexMonome(1, 1, stor.a, 'n') + j3pGetLatexMonome(2, 0, stor.b - stor.c, 'n')
              : j3pGetLatexMonome(1, 1, stor.a, 'n') + j3pGetLatexMonome(2, 0, stor.b + stor.c, 'n')
            stor.membreDroite = (stor.c > 0)
              ? j3pGetLatexMonome(1, 1, stor.a, 'n') + j3pGetLatexMonome(2, 0, stor.b + stor.c, 'n')
              : j3pGetLatexMonome(1, 1, stor.a, 'n') + j3pGetLatexMonome(2, 0, stor.b - stor.c, 'n')
          }
          stor.objCons.e2 = (stor.c > 0)
            ? (-stor.c) + '\\leq ' + j3pGetLatexMonome(1, 1, stor.c, stor.objCons.f) + '\\leq ' + stor.c
            : (-stor.c) + '\\geq ' + j3pGetLatexMonome(1, 1, stor.c, stor.objCons.f) + '\\geq ' + stor.c + '\\iff' + stor.c + '\\leq ' + j3pGetLatexMonome(1, 1, stor.c, stor.objCons.f) + '\\leq ' + (-stor.c)
          stor.objCons.e3 = stor.membreGauche + '\\leq ' + stor.objCons.u + '_n' + '\\leq ' + stor.membreDroite
          stor.bonChoix = (stor.a > 0) ? 1 : 2 // membre à choisir pour réussir à conclure sur la limite (réponse à l’étape 2)
          stor.coefmtgGauche[0] = (stor.k === 2) ? stor.a : 0
          stor.coefmtgGauche[1] = (stor.k === 2) ? stor.b : stor.a
          stor.coefmtgGauche[2] = (stor.k === 2) ? -Math.abs(stor.c) : stor.b - Math.abs(stor.c)
          stor.coefmtgGauche[3] = 0
          stor.coefmtgGauche[4] = 0
          stor.coefmtgDroite[0] = (stor.k === 2) ? stor.a : 0
          stor.coefmtgDroite[1] = (stor.k === 2) ? stor.b : stor.a
          stor.coefmtgDroite[2] = (stor.k === 2) ? Math.abs(stor.c) : stor.b + Math.abs(stor.c)
          stor.coefmtgDroite[3] = 0
          stor.coefmtgDroite[4] = 0
        } else {
          // L’exemple choisi sera de la forme a*n^k+(b*sin(n))/n^k' où sin(n) peut être remplacé par cos(n) ou (-1)^n,
          // k>=1 et k'>=1 (ils peuvent être égaux ou non)
          stor.objCons.d = '\\N^*'
          stor.k = j3pGetRandomInt(1, 2)
          stor.k2 = j3pGetRandomInt(1, 2)
          stor.a = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
          stor.b = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
          stor.objCons.n = textes.nonnul
          stor.objCons.un = j3pGetLatexMonome(1, stor.k, stor.a, 'n')
          if (stor.b < 0) {
            stor.objCons.un += '-\\frac{' + j3pGetLatexMonome(1, 1, Math.abs(stor.b), stor.objCons.f) + '}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}'
          } else {
            stor.objCons.un += '+\\frac{' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}'
          }
          stor.membreGauche = j3pGetLatexMonome(1, stor.k, stor.a, 'n') + '-\\frac{' + Math.abs(stor.b) + '}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}'
          stor.membreDroite = j3pGetLatexMonome(1, stor.k, stor.a, 'n') + '+\\frac{' + Math.abs(stor.b) + '}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}'
          stor.objCons.e2 = (stor.b === 1)
            ? '-\\frac{1}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}\\leq \\frac{1}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}'
            : (stor.b > 0)
                ? (-stor.b) + '\\leq ' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '\\leq ' + stor.b + '\\iff -\\frac{' + stor.b + '}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}\\leq \\frac{' + stor.b + '}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}'
                : (-stor.b) + '\\geq ' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '\\geq ' + stor.b + '\\iff' + stor.b + '\\leq ' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '\\leq ' + (-stor.b) + '\\iff -\\frac{' + Math.abs(stor.b) + '}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}\\leq \\frac{' + stor.b + '}{' + j3pGetLatexMonome(1, stor.k2, 1, 'n') + '}'
          stor.objCons.e3 = stor.membreGauche + '\\leq ' + stor.objCons.u + '_n' + '\\leq ' + stor.membreDroite
          stor.coefmtgGauche[0] = (stor.k === 2) ? stor.a : 0
          stor.coefmtgGauche[1] = (stor.k === 2) ? 0 : stor.a
          stor.coefmtgGauche[2] = 0
          stor.coefmtgGauche[3] = (stor.k2 === 2) ? 0 : -Math.abs(stor.b)
          stor.coefmtgGauche[4] = (stor.k2 === 2) ? -Math.abs(stor.b) : 0
          stor.coefmtgDroite[0] = (stor.k === 2) ? stor.a : 0
          stor.coefmtgDroite[1] = (stor.k === 2) ? 0 : stor.a
          stor.coefmtgDroite[2] = 0
          stor.coefmtgDroite[3] = (stor.k2 === 2) ? 0 : Math.abs(stor.b)
          stor.coefmtgDroite[4] = (stor.k2 === 2) ? Math.abs(stor.b) : 0
        }
        stor.objCons.vn = (stor.a > 0) ? stor.membreGauche : stor.membreDroite
        stor.bonChoix = (stor.a > 0) ? 1 : 2 // membre à choisir pour réussir à conclure sur la limite (réponse à l’étape 2)
      } else {
        // L’exemple choisi sera de la forme a+(b*sin(n))/n^k où sin(n) peut être remplacé par cos(n) ou (-1)^n,
        // k>=1
        stor.objCons.d = '\\N^*'
        stor.k = j3pGetRandomInt(1, 2)
        stor.a = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
        stor.b = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
        stor.objCons.n = textes.nonnul
        stor.objCons.un = j3pGetLatexMonome(1, 0, stor.a, 'n')
        if (stor.b < 0) {
          stor.objCons.un += '-\\frac{' + j3pGetLatexMonome(1, 1, Math.abs(stor.b), stor.objCons.f) + '}{' + j3pGetLatexMonome(1, stor.k, 1, 'n') + '}'
        } else {
          stor.objCons.un += '+\\frac{' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '}{' + j3pGetLatexMonome(1, stor.k, 1, 'n') + '}'
        }
        stor.membreGauche = j3pGetLatexMonome(1, 0, stor.a, 'n') + '-\\frac{' + Math.abs(stor.b) + '}{' + j3pGetLatexMonome(1, stor.k, 1, 'n') + '}'
        stor.membreDroite = j3pGetLatexMonome(1, 0, stor.a, 'n') + '+\\frac{' + Math.abs(stor.b) + '}{' + j3pGetLatexMonome(1, stor.k, 1, 'n') + '}'
        stor.objCons.e2 = (stor.b > 0)
          ? (-stor.b) + '\\leq ' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '\\leq ' + stor.b + '\\iff -\\frac{' + stor.b + '}{' + j3pGetLatexMonome(1, stor.k, 1, 'n') + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '}{' + j3pGetLatexMonome(1, stor.k, 1, 'n') + '}\\leq \\frac{' + stor.b + '}{' + j3pGetLatexMonome(1, stor.k, 1, 'n') + '}'
          : (-stor.b) + '\\geq ' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '\\geq ' + stor.b + '\\iff' + stor.b + '\\leq ' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '\\leq ' + (-stor.b) + '\\iff -\\frac{' + Math.abs(stor.b) + '}{' + j3pGetLatexMonome(1, stor.k, 1, 'n') + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, stor.b, stor.objCons.f) + '}{' + j3pGetLatexMonome(1, stor.k, 1, 'n') + '}\\leq \\frac{' + stor.b + '}{' + j3pGetLatexMonome(1, stor.k, 1, 'n') + '}'
        stor.objCons.e3 = stor.membreGauche + '\\leq ' + stor.objCons.u + '_n' + '\\leq ' + stor.membreDroite
        stor.objCons.vn = stor.membreGauche
        stor.objCons.wn = stor.membreDroite
        stor.bonChoix = 3 // membre à choisir pour réussir à conclure sur la limite (réponse à l’étape 2)
        stor.coefmtgGauche[0] = 0
        stor.coefmtgGauche[1] = 0
        stor.coefmtgGauche[2] = stor.a
        stor.coefmtgGauche[3] = (stor.k === 2) ? 0 : -Math.abs(stor.b)
        stor.coefmtgGauche[4] = (stor.k === 2) ? -Math.abs(stor.b) : 0
        stor.coefmtgDroite[0] = 0
        stor.coefmtgDroite[1] = 0
        stor.coefmtgDroite[2] = stor.a
        stor.coefmtgDroite[3] = (stor.k === 2) ? 0 : Math.abs(stor.b)
        stor.coefmtgDroite[4] = (stor.k === 2) ? Math.abs(stor.b) : 0
      }
    }
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objCons)

    let mesZonesSaisie = []
    let validePerso = []
    if (me.questionCourante % ds.nbetapes === 1) {
      j3pAffiche(stor.zoneCons2, '', textes.consigne2, stor.objCons)
      const ligneRep = '&1& #1# $£u_n$ #2# &2&'
      stor.ligneRep = j3pAddElt(stor.zoneCons3, 'div')
      stor.indic = j3pAddElt(stor.zoneCons3, 'div')
      stor.elt = j3pAffiche(stor.zoneCons3, '', ligneRep, {
        u: stor.objCons.u,
        inputmq1: { texte: '' },
        liste1: { texte: ['', '<', '&le;', '>', '&ge;'] },
        liste2: { texte: ['', '<', '&le;', '>', '&ge;'] },
        inputmq2: { texte: '' }
      })
      stor.elt.inputmqList.forEach(eltInput => mqRestriction(eltInput, '\\d,+-n/^()', { commandes: ['puissance', 'fraction', 'sin', 'cos'] }))
      j3pAffiche(stor.indic, '', textes.indic)
      j3pStyle(stor.indic, { fontStyle: 'italic', fontSize: '0.85em' })
      stor.elt.inputmqList[0].typeReponse = ['texte']
      stor.elt.inputmqList[0].typeReponse = ['texte']
      mesZonesSaisie = [stor.elt.inputmqList[0].id, stor.elt.selectList[0].id, stor.elt.selectList[1].id, stor.elt.inputmqList[1].id]
      validePerso = mesZonesSaisie.slice(0)
      j3pFocus(stor.elt.inputmqList[0])
      // Paramétrage de la validation

      stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso })
      stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '15px' } })
      stor.elt.inputmqList[0].lesCommandes = ['puissance', 'fraction', 'sin', 'cos']
      stor.elt.inputmqList[1].lesCommandes = ['puissance', 'fraction', 'sin', 'cos']
      $(stor.elt.inputmqList[0]).focusin(ecoute.bind(null, 0))
      $(stor.elt.inputmqList[1]).focusin(ecoute.bind(null, 1))
      ecoute(0)
    } else {
      const tabChoix = []
      stor.objCons.e = stor.membreGauche + '\\leq £u_n\\leq ' + stor.membreDroite
      j3pAffiche(stor.zoneCons2, '', textes.consigne3, stor.objCons)
      j3pAffiche(stor.zoneCons3, '', textes.consigne4, stor.objCons)
      tabChoix.push(j3pChaine(textes.consigne5, { vn: stor.membreGauche }))
      tabChoix.push(j3pChaine(textes.consigne5, { vn: stor.membreDroite }))
      tabChoix.push(j3pChaine(textes.consigne6, {
        vn: stor.membreGauche,
        wn: stor.membreDroite
      }))
      stor.btnRadioSelect = 0
      stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '15px' } })
      stor.idsRadio = []
      stor.idsLabelRadio = []
      stor.nameRadio = j3pGetNewId('choix')
      for (let k = 1; k <= 3; k++) {
        stor['divBtn' + k] = j3pAddElt(stor.zoneCons3, 'div')
        stor.idsRadio.push(j3pGetNewId('radio' + k))
        stor.idsLabelRadio.push(j3pGetNewId('labelRadio' + k))
        j3pBoutonRadio(stor['divBtn' + k], stor.idsRadio[k - 1], stor.nameRadio, k - 1, '')
        j3pAffiche('label' + stor.idsRadio[k - 1], stor.idsLabelRadio[k - 1], tabChoix[k - 1])
        j3pElement(stor.idsRadio[k - 1]).num = String(k)
        j3pElement(stor.idsRadio[k - 1]).addEventListener('click', function () {
          const numRadio = this.num // c’est un string, voir ci-dessus String(this.id).split('radio')[1]
          if (numRadio !== String(stor.btnRadioSelect)) {
            mesZonesSaisie = []
            if (stor.btnRadioSelect > 0) {
              j3pEmpty(stor.zoneCons4)
              j3pEmpty(stor.laPalette)
            }
            if (numRadio <= 2) {
              stor.elt2 = j3pAffiche(stor.zoneCons4, '', textes.consigne7, {
                inputmq1: { texte: '' },
                inputmq2: { texte: '' },
                u: stor.objCons.u,
                vn: (numRadio === '1') ? stor.membreGauche : stor.membreDroite
              })
              for (let i = 0; i <= 1; i++) {
                stor.elt2.inputmqList[i].typeReponse = ['texte']
                mesZonesSaisie.push(stor.elt2.inputmqList[i].id)
                mqRestriction(stor.elt2.inputmqList[i], '\\d,+-/', { commandes: ['inf', 'fraction'] })
              }
              $(stor.elt2.inputmqList[0]).focusin(ecoute.bind(null, 2))
              $(stor.elt2.inputmqList[1]).focusin(ecoute.bind(null, 3))
              j3pFocus(stor.elt2.inputmqList[0])
              ecoute(2)
            } else {
              stor.elt2 = j3pAffiche(stor.zoneCons4, '', textes.consigne8, {
                inputmq1: { texte: '' },
                inputmq2: { texte: '' },
                inputmq3: { texte: '' },
                u: stor.objCons.u,
                vn: stor.membreGauche,
                wn: stor.membreDroite
              })
              for (let i = 0; i <= 2; i++) {
                stor.elt2.inputmqList[i].typeReponse = ['texte']
                mesZonesSaisie.push(stor.elt2.inputmqList[i].id)
                $(stor.elt2.inputmqList[i]).focusin(ecoute.bind(null, i + 2))
                mqRestriction(stor.elt2.inputmqList[i], '\\d,+-/', { commandes: ['inf', 'fraction'] })
              }
              j3pFocus(stor.elt2.inputmqList[0])
              ecoute(2)
            }
            stor.btnRadioSelect = Number(numRadio)
            validePerso = mesZonesSaisie.slice(0)
            stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso })
          }
        })
      }
    }

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    me.finEnonce()
  } // enonceMain

  // initialise la section et charge mathgraph (async)
  function enonceInit () {
    me.surcharge({ nbetapes: 2 })

    // Construction de la page
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
    me.afficheTitre(textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    // code de création des fenêtres

    stor.typeQuest = []
    let alea, tabChoix
    if (ds.typeTh === 'alea') {
      const tabChoixInit = ['comparaison', 'gendarmes']
      tabChoix = tabChoixInit.slice(0)
      for (let i = 1; i <= ds.nbrepetitions; i++) {
        if (tabChoix.length === 0) {
          tabChoix = tabChoixInit.slice(0)
        }
        alea = j3pGetRandomInt(0, tabChoix.length - 1)
        stor.typeQuest.push(tabChoix[alea])
        tabChoix.splice(alea, 1)
      }
    } else {
      for (let i = 1; i <= ds.nbrepetitions; i++) {
        stor.typeQuest.push(ds.typeTh)
      }
    }
    stor.choixFct = []
    let choixFct = ['sin', 'cos', '-1']
    const choixFctInit = choixFct.slice(0)
    for (let i = 1; i <= ds.nbrepetitions; i++) {
      if (choixFct.length === 0) {
        choixFct = choixFctInit.slice(0)
      }
      alea = j3pGetRandomInt(0, choixFct.length - 1)
      stor.choixFct.push(choixFct[alea])
      choixFct.splice(alea, 1)
    }
    // J’ai planifié les fonctions choisies dans le cadre de l’encadrement initial (sin(n), cos(n) ou (-1)^n
    ds.nbChancesInit = ds.nbchances

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.cListeObjets = mtgAppLecteur.createList(txtFig)
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  } // enonceInit

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      let reponse = { aRepondu: false, bonneReponse: false }
      let pbsInegalites
      let reponsesEgales
      let btnRadioNonCoche
      let bonChoixRadio
      if (me.questionCourante % ds.nbetapes === 1) {
        reponse.aRepondu = stor.fctsValid.valideReponses()
        pbsInegalites = true
        if (reponse.aRepondu) {
          // validation du cas où on aurait écrit zone1<=u_n<=zone2
          stor.cListeObjets.giveFormula2('a', stor.coefmtgGauche[0])
          stor.cListeObjets.giveFormula2('b', stor.coefmtgGauche[1])
          stor.cListeObjets.giveFormula2('c', stor.coefmtgGauche[2])
          stor.cListeObjets.giveFormula2('d', stor.coefmtgGauche[3])
          stor.cListeObjets.giveFormula2('e', stor.coefmtgGauche[4])
          stor.cListeObjets.giveFormula2('a1', stor.coefmtgDroite[0])
          stor.cListeObjets.giveFormula2('b1', stor.coefmtgDroite[1])
          stor.cListeObjets.giveFormula2('c1', stor.coefmtgDroite[2])
          stor.cListeObjets.giveFormula2('d1', stor.coefmtgDroite[3])
          stor.cListeObjets.giveFormula2('e1', stor.coefmtgDroite[4])
          const repeleveGauche = $(stor.elt.inputmqList[0]).mathquill('latex')
          const repeleveDroite = $(stor.elt.inputmqList[1]).mathquill('latex')
          const expEleveGauche = traiteMathQuill(repeleveGauche)
          const expEleveDroite = traiteMathQuill(repeleveDroite)
          const inegIndex1 = stor.elt.selectList[0].selectedIndex
          const inegIndex2 = stor.elt.selectList[1].selectedIndex
          // Premier cas de figure d’une bonne réponse :
          // membregauche<=u_n<=membreDroite
          stor.cListeObjets.giveFormula2('rep', expEleveGauche)
          stor.cListeObjets.giveFormula2('rep1', expEleveDroite)
          stor.cListeObjets.giveFormula2('n1', '7')
          stor.cListeObjets.calculateNG(false)
          const bonneZoneGauche = stor.cListeObjets.valueOf('equivalent') === 1
          const bonneZoneDroite = stor.cListeObjets.valueOf('equivalent1') === 1
          let bonneZoneGauche1
          let bonneZoneDroite1
          let premiereCas = bonneZoneGauche
          premiereCas = (premiereCas && ((String(stor.choixFct) === '-1' && inegIndex1 === 2) || (String(stor.choixFct) !== '-1' && inegIndex1 <= 2)))
          premiereCas = (premiereCas && bonneZoneDroite)
          premiereCas = (premiereCas && ((String(stor.choixFct) === '-1' && inegIndex2 === 2) || (String(stor.choixFct) !== '-1' && inegIndex2 <= 2)))
          reponse.bonneReponse = premiereCas
          // console.log("réponses:",bonneZoneGauche,inegIndex1,bonneZoneDroite,inegIndex2)
          // Si la réponse n’est pas celle attendue, je cherche tout de même à savoir si l’élève n’a pas donné des réponses égales (mais non équivalentes en écriture) à ce qui est attendu :
          // Ce sont les variables mtg32 egalite1 et egalite2 qui vont servir à la place de bonneZoneGauche et bonneZoneDroite
          let egaliteZoneGauche = stor.cListeObjets.valueOf('egalite1') === 1
          let egaliteZoneDroite = stor.cListeObjets.valueOf('egalite2') === 1
          reponsesEgales = (egaliteZoneGauche && ((String(stor.choixFct) === '-1' && inegIndex1 === 2) || (String(stor.choixFct) !== '-1' && inegIndex1 <= 2)) && egaliteZoneDroite && ((String(stor.choixFct) === '-1' && inegIndex2 === 2) || (String(stor.choixFct) !== '-1' && inegIndex2 <= 2)))
          let deuxiemeCas = false
          if (!reponse.bonneReponse && !reponsesEgales) {
            stor.cListeObjets.giveFormula2('rep1', expEleveGauche)
            stor.cListeObjets.giveFormula2('rep', expEleveDroite)
            stor.cListeObjets.calculateNG(false)
            bonneZoneDroite1 = stor.cListeObjets.valueOf('equivalent') === 1
            bonneZoneGauche1 = stor.cListeObjets.valueOf('equivalent1') === 1
            deuxiemeCas = bonneZoneDroite1
            deuxiemeCas = (deuxiemeCas && ((String(stor.choixFct) === '-1' && inegIndex1 === 4) || (String(stor.choixFct) !== '-1' && inegIndex1 >= 3)))
            deuxiemeCas = (deuxiemeCas && bonneZoneGauche1)
            deuxiemeCas = (deuxiemeCas && ((String(stor.choixFct) === '-1' && inegIndex2 === 4) || (String(stor.choixFct) !== '-1' && inegIndex2 >= 3)))
            reponse.bonneReponse = deuxiemeCas
            // console.log("réponses bis:",bonneZoneGauche1,inegIndex1,bonneZoneDroite1,inegIndex2)
            // Si la réponse n’est pas celle attendue, je cherche tout de même à savoir si l’élève n’a pas donné des réponses égales (mais non équivalentes en écriture) à ce qui est attendu :
            // Ce sont les variables mtg32 egalitebis1 et egalitebis2 qui vont servir à la place de bonneZoneGauche et bonneZoneDroite
            egaliteZoneGauche = stor.cListeObjets.valueOf('egalite1') === 1
            egaliteZoneDroite = stor.cListeObjets.valueOf('egalite2') === 1
            reponsesEgales = (egaliteZoneGauche && ((String(stor.choixFct) === '-1' && inegIndex1 === 4) || (String(stor.choixFct) !== '-1' && inegIndex1 >= 3)) && egaliteZoneDroite && ((String(stor.choixFct) === '-1' && inegIndex2 === 4) || (String(stor.choixFct) !== '-1' && inegIndex2 >= 3)))
          }
          for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
            stor.fctsValid.zones.bonneReponse[i] = true
          }
          if (reponse.bonneReponse) {
            for (let i = 0; i < 4; i++) {
              stor.fctsValid.zones.bonneReponse[i] = true
              stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
            }
          } else {
            pbsInegalites = (inegIndex1 !== inegIndex2)
            // Il faut que je gère ce qui devra être mis en rouge
            if (pbsInegalites) {
              ;[1, 2].forEach(i => { stor.fctsValid.zones.bonneReponse[i] = false })
            }
            if (inegIndex1 % 2 === 1) stor.fctsValid.zones.bonneReponse[1] = false
            if (inegIndex2 % 2 === 1) stor.fctsValid.zones.bonneReponse[2] = false
            stor.fctsValid.zones.bonneReponse[0] = (bonneZoneGauche || bonneZoneGauche1)
            if (bonneZoneGauche) stor.fctsValid.zones.bonneReponse[3] = bonneZoneDroite
            if (bonneZoneGauche && bonneZoneDroite) {
              for (const i of [1, 2]) stor.fctsValid.zones.bonneReponse[i] = premiereCas
            }
            if (bonneZoneGauche1) stor.fctsValid.zones.bonneReponse[3] = bonneZoneDroite1
            if (bonneZoneGauche1 && bonneZoneDroite1) {
              for (const i of [1, 2]) stor.fctsValid.zones.bonneReponse[i] = deuxiemeCas
            }
            if (!bonneZoneGauche && !bonneZoneGauche1) stor.fctsValid.zones.bonneReponse[3] = (bonneZoneDroite || bonneZoneDroite1)
            if (reponsesEgales) {
              // Les réponses données ne sont pas sous la bonne forme, mais elles sont correctes, donc je donne une chance de plus
              // Par contre, je le fais au max 2 fois :
              if (ds.nbchances < ds.nbChancesInit + 2) ds.nbchances++
            }
            if (me.essaiCourant < ds.nbchances - 1) {
              // On ne valide rien à cet instant car l’encadrement doit pouvoir être modifié
              // Par contre on met en couleur
              for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
                stor.fctsValid.zones.inputs[i].style.color = (stor.fctsValid.zones.bonneReponse[i]) ? me.styles.cbien : me.styles.cfaux
              }
            } else {
              for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
                stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
              }
            }
          }
        }
      } else {
        reponse.aRepondu = (stor.btnRadioSelect > 0)
        btnRadioNonCoche = (stor.btnRadioSelect === 0)
        bonChoixRadio = true
        if (reponse.aRepondu) {
          reponse.aRepondu = stor.fctsValid.valideReponses()
          // On vérifie si le bouton coché est le bon
          bonChoixRadio = (String(stor.btnRadioSelect) === String(stor.bonChoix))
          if (bonChoixRadio) {
            const mesZonesSaisie = []
            if (stor.bonChoix === 1) {
              // le membre de gauche permet de répondre, c’est que la limite est +\infty
              for (let i = 0; i <= 1; i++) {
                stor.elt2.inputmqList[i].reponse = ['+\\infty']
                mesZonesSaisie.push(stor.elt2.inputmqList[i].id)
              }
            } else if (stor.bonChoix === 2) {
              // le membre de droite permet de répondre, c’est que la limite est -\infty
              for (let i = 0; i <= 1; i++) {
                stor.elt2.inputmqList[i].reponse = ['-\\infty']
                mesZonesSaisie.push(stor.elt2.inputmqList[i].id)
              }
            } else {
              // On a besoin des deux membres, on a donc un th des gendarmes
              for (let i = 0; i <= 2; i++) {
                stor.elt2.inputmqList[i].typeReponse = ['nombre', 'exact']
                stor.elt2.inputmqList[i].reponse = [stor.a]
                mesZonesSaisie.push(stor.elt2.inputmqList[i].id)
              }
            }
            stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
            reponse = stor.fctsValid.validationGlobale()
          } else {
            reponse.bonneReponse = false
          }
        }
      }
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        let msgReponseManquante
        if ((me.questionCourante % ds.nbetapes === 0) && btnRadioNonCoche) {
          msgReponseManquante = textes.comment1
        }
        me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        return me.finCorrection()
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (reponse.bonneReponse) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          if (me.questionCourante % ds.nbetapes === 0) {
            for (let j = 1; j <= 3; j++) j3pDesactive(stor.idsRadio[j - 1])
          }
          afficheCorr(true)
          me.typederreurs[0]++
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            if (me.questionCourante % ds.nbetapes === 0) {
              for (let j = 1; j <= 3; j++) j3pDesactive(stor.idsRadio[j - 1])
              j3pStyle(stor.idsLabelRadio[stor.bonChoix - 1], { color: me.styles.toutpetit.correction.color })
            }
            afficheCorr(false)
          } else {
            // Réponse fausse :
            let comment
            if (me.etapeCourante === 1) {
              if (pbsInegalites) {
                comment = textes.comment5
              } else if (!reponsesEgales) {
                comment = textes.comment6
              }
            } else {
              if (!bonChoixRadio) {
                if (stor.bonChoix === 3) {
                  comment = textes.comment3
                } else if (stor.btnRadioSelect === 3) {
                  comment = textes.comment4
                } else {
                  comment = textes.comment2
                }
              }
            }
            if (!comment) comment = cFaux
            if (me.etapeCourante === 1 && !stor.inegalitesExactes) comment += '<br>' + textes.comment7
            stor.zoneCorr.innerHTML = comment

            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              return me.finCorrection()
            } else {
              // Erreur au nème essai
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              if (me.questionCourante % ds.nbetapes === 0) {
                ;[0, 1, 2].forEach(j => j3pDesactive(stor.idsRadio[j]))
                if (!bonChoixRadio) {
                  j3pBarre(stor.idsLabelRadio[j3pBoutonRadioChecked(stor.nameRadio)[0]])
                  stor.fctsValid.zones.inputs.forEach(input => j3pDesactive(input))
                  j3pBarre(stor.zoneCons4)
                  for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
                    j3pDesactive(stor.fctsValid.zones.inputs[i])
                  }
                }
                j3pStyle(stor.idsLabelRadio[stor.bonChoix - 1], { color: me.styles.toutpetit.correction.color })
                j3pStyle(stor.zoneCons4, { color: me.styles.cfaux })
              }
              afficheCorr(false)
              me.typederreurs[2]++
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
