import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pPGCD, j3pRandomTab, j3pStyle } from 'src/legacy/core/functions'
// importTarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
 * Section proposant une suite et demandant de compléter les premiers termes
 *
 * Développeur : Alexis Lecomte
 *
 * septembre 2013
 *
 * Scénario :
 * une étape, relation de récurrence
  Paramètre: [f(n),f(u_n),f(u_n,n)] avec probas
  Valeur exacte

  Modèles (qq fractions)
  f(n) polynome de degré 1, 2 et homographique
  f(u_n) arithmetico-geometrique
  f(u_n,n):
  a(n)*u_n+b(n)
  a et b maxi degré 1
 *
 *
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['typeSuite', ['f(n)', 'f(un)', 'f(un,n)'], 'array', 'Les types de suites possibles, avec leur probas à modifier'],
    ['probasSuite', [0.3, 0.3, 0.4], 'array', 'Probabilité d’apparition des suites précédentes, la somme devant faire 1'],
    ['N', 3, 'entier', 'nb de termes (-1 car u0 est donné) à calculer (<=5)']
  ]
}
const textes = {
  titre_exo: 'Appliquer la définition d’une suite',
  phrase1: 'Voici les premiers termes d’une suite définie par récurrence. Compléter le terme manquant :',
  phrase2: 'Quelle semble être la relation de récurrence, c’est-à-dire la relation permettant de calculer le terme $u_{n+1}$ en fonction du terme précédent $u_n$?',
  consigne1_1: 'On définit une suite $(u_{n})$ par $£a$ pour tout entier naturel $n$.<br>',
  consigne1_2: 'Déterminer les $£a$ premiers termes de la suite :<br>',
  consigne2_1: 'On définit une suite $(u_{n})$ par $u_0=£a$ et $£b$ pour tout entier naturel $n$.<br>',
  consigne2_2: 'Déterminer les $£a$ premiers termes de la suite :<br>'
}
/**
 * section Suites3
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function ecoute (eltInput, avecPuissance) {
    if (stor.laPalette) {
      // Ce test est utile pour sesaparcours pour qu’on n’ait pas de message d’erreur si on clique la zone réponse
      // id est celui de la zone de saisie
      const laPalette = ['fraction']
      if (avecPuissance) laPalette.push('puissance')
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, eltInput, { liste: laPalette })
    }
  }

  // fonction qui à une chaine latex d’une fraction retourne le num et le den
  function extraitFrac (chaineLatex) {
    function accoladeFermante (chaine, index) {
      // fonction qui donne la position de l’accolade fermante correspondant à l’accolade ouvrante positionnée en index de la chaine
      // pour l’instant 1 accolade ouvrante (à l’appel de la fonction)
      let indexaccolade = 1
      let indexvariable = index
      while (indexaccolade !== 0) {
        // je peux avoir des accolades internes (par groupe de 2 necessairement)
        indexvariable++
        if (chaine.charAt(indexvariable) === '{') indexaccolade++
        if (chaine.charAt(indexvariable) === '}') indexaccolade--
      }
      return indexvariable
    }
    const objet = {}
    // la position de la première accolade
    const pos1 = chaineLatex.indexOf('\\frac') + 5
    const pos2 = accoladeFermante(chaineLatex, pos1)
    const A = chaineLatex.substring(pos1 + 1, pos2)
    const pos3 = accoladeFermante(chaineLatex, pos2 + 1)
    const B = chaineLatex.substring(pos2 + 2, pos3)
    objet.num = A
    objet.den = B
    return objet
  }
  function lesigne (nombre) {
    // nombre = Number(nombre);
    const absNombre = Math.abs(nombre)
    let nombre1
    if (nombre < 0) {
      nombre1 = ' - ' + absNombre
    } else {
      nombre1 = ' + ' + absNombre
    }
    return nombre1
  }

  function lesigneFacteur (nombre) {
    if (nombre === 1) {
      return ' + '
    } else {
      if (nombre === -1) {
        return ' - '
      } else {
        return lesigne(nombre)
      }
    }
  }
  function signeFacteur (nombre) {
    if (nombre === 1) {
      return ''
    } else {
      if (nombre === -1) {
        return '-'
      } else {
        return nombre
      }
    }
  }
  // Fonction qui retourne a/b simplifiée au format latex, sous forme d’entier si besoin
  function simplifieFrac (a, b) {
    // console.log("Dans simplifie frac, a="+a+" et b="+b)
    let expression
    if (a % b === 0) {
      expression = a / b + ''
    } else {
      const numerateur = a / j3pPGCD(Math.abs(a), Math.abs(b))
      const denominateur = b / j3pPGCD(Math.abs(a), Math.abs(b))
      expression = '\\frac{' + numerateur + '}{' + denominateur + '}'
      if (numerateur < 0 && denominateur < 0) {
        expression = '\\frac{' + Math.abs(numerateur) + '}{' + Math.abs(denominateur) + '}'
      }
    }
    // //console.log("expression:"+expression)
    return expression
  }
  function definirSolutionV2 (a, b, num, den, alea, signe, k) {
    let leNum, leDen
    for (let i = 1; i <= (ds.N - 1); i++) {
      //  //console.log("i="+i)
      if (String(stor.solution[1][i - 1]).includes('\\frac')) {
        // la réponse précédente est une fraction, j’extraie son num et son den
        leNum = extraitFrac(stor.solution[1][i - 1]).num
        leDen = extraitFrac(stor.solution[1][i - 1]).den
      } else {
        // c’est un entier
        leNum = stor.solution[1][i - 1]
        leDen = 1
      }
      if (alea === 1) {
        // a est entier
        stor.solution[1][i] = simplifieFrac(a * leNum + b * leDen, leDen)
      } else {
        if (signe === 1) {
          stor.solution[1][i] = simplifieFrac(num * leNum + den * leDen * (den - num) * k, den * leDen)
        } else {
          stor.solution[1][i] = simplifieFrac(-num * leNum + den * leDen * (den + num) * k, den * leDen)
        }
      }
      // me.stockage[5][i]=a+
      // pour la correction détaillée :
    }
  }
  function tirageCalcul () {
    me.stockage[20] = j3pRandomTab(ds.typeSuite, ds.probasSuite)
    const phCorr = []

    /*
Modèles (qq fractions)
f(n) polynome de degré 1, 2 et homographique
f(u_n) arithmetico-geometrique
f(u_n,n):
 a(n)*u_n+b(n)
a et b maxi degré 1 */
    // Deux cas de figures : a entier différent de -1,0,1 ou a fraction comprise entre -1 et 1
    let alea, a, b, c, d, i, expression, u0, r, u1, k
    let den, num, signe
    switch (me.stockage[20]) {
      case 'f(n)':
        // console.log("f(n)");
        // u0 est à calculer
        me.stockage[40] = 0

        alea = j3pGetRandomInt(1, 3)
        // var alea=3;
        switch (alea) {
          case 1:
            // pol de degré 1
            do {
              a = j3pGetRandomInt(-7, 7)
            } while (a === -1 || a === 0 || a === 1)
            do {
              b = j3pGetRandomInt(-7, 7)
            } while (b === -1 || b === 0 || b === 1)
            // pour l’énoncé, au format latex donc
            expression = 'u_n=' + a + 'n' + lesigne(b)
            // définition des solutions :
            for (i = 0; i <= (ds.N - 1); i++) {
              stor.solution[1][i] = a * i + b
              phCorr[i] = '$u_' + i + '=' + a + '\\times' + i + lesigne(b) + '=' + String(stor.solution[1][i]) + '$'
            }
            break

          case 2:
            // pol de degré 2
            do {
              a = j3pGetRandomInt(-7, 7)
            } while (a === -1 || a === 0 || a === 1)
            do {
              b = j3pGetRandomInt(-7, 7)
            } while (b === -1 || b === 0 || b === 1)
            do {
              c = j3pGetRandomInt(-7, 7)
            } while (c === -1 || c === 0 || c === 1)
            // pour l’énoncé, au format latex donc
            expression = 'u_n=' + a + 'n^2' + lesigne(b) + 'n' + lesigne(c)
            // définition des solutions :
            for (i = 0; i <= (ds.N - 1); i++) {
              stor.solution[1][i] = a * i * i + b * i + c
              phCorr[i] = '$u_' + i + '=' + a + '\\times' + i + '^2' + lesigne(b) + '\\times ' + i + lesigne(c) + '=' + String(stor.solution[1][i]) + '$'
            }
            break

          case 3:
            // homographique
            do {
              a = j3pGetRandomInt(-9, 9)
              b = j3pGetRandomInt(-9, 9)
              c = j3pGetRandomInt(-9, 9)
              d = j3pGetRandomInt(-9, 9)
            } while (a === -1 || a === 0 || a === 1 || b === -1 || b === 0 || b === 1 || c === -1 || c === 0 || c === 1 || d === -1 || d === 0 || d === 1 || (c + d === 0) || (2 * c + d) === 0 || (3 * c + d) === 0 || (4 * c + d) === 0 || (5 * c + d) === 0 || (a === b))
            // pour l’énoncé, au format latex donc
            expression = 'u_n=' + '\\frac{' + a + 'n' + lesigne(b) + '}{' + c + 'n' + lesigne(d) + '}'
            // définition des solutions :
            for (i = 0; i <= (ds.N - 1); i++) {
              stor.solution[1][i] = '\\frac{' + (a * i + b) + '}{' + (c * i + d) + '}'
              phCorr[i] = '$u_' + i + '=\\frac{' + a + '\\times' + i + lesigne(b) + '}{' + c + '\\times' + i + lesigne(d) + '}=' + simplifieFrac(a * i + b, c * i + d) + '$'
            }
            break
        }

        // AFFICHAGE DE LA CONSIGNE :
        me.stockage[4] = expression
        u0 = stor.solution[1][0]
        me.stockage[21] = phCorr
        /* console.log('phCorr[0]=' + phCorr[0] + ' et me.stockage[21][0]=' + me.stockage[21][0])
        console.log('u0=' + u0)
        console.log('expression=' + expression) */
        j3pAffiche(stor.zoneCons1, '', textes.consigne1_1, { a: expression })
        j3pAffiche(stor.zoneCons1, '', textes.consigne1_2, { a: ds.N })

        break

      case 'f(un)':
        // code repris de la section Suites1, inutilement compliqué car elle avait plusieurs étapes...
        // console.log("f(un)")
        // u0 est donné
        me.stockage[40] = 1
        alea = j3pGetRandomInt(1, 2)
        if (alea === 1) {
          // a entier
          do {
            a = j3pGetRandomInt(-7, 7)
          } while (a === -1 || a === 0 || a === 1)
          // on définit r pour que v=u-r
          do {
            r = j3pGetRandomInt(-7, 7)
          } while (r === -1 || r === 0 || r === 1 || a === r)
          // il reste à écrire b=r*(1-a)
          b = r * (1 - a)
          // il faut éviter la suite constante...
          do {
            u0 = j3pGetRandomInt(2, 7)
            u1 = a * u0 + b
          } while (u1 === u0 || u0 - r === 1)
          // var u0=r+1;
          /* dans ces conditions la suite (v_n) ne sera pas convergente (raison non comprise entre -1 et 1 :
                    me.stockage[10]=false; */
        } else {
          // a fraction irréductible comprise entre -1 et 1
          // dans ces conditions la suite (v_n)  sera convergente (raison comprise entre -1 et 1 :
          // me.stockage[10]=true;
          den = j3pGetRandomInt(2, 7)
          do {
            num = j3pGetRandomInt(1, den)
          } while (j3pPGCD(num, den) !== 1)
          signe = j3pGetRandomInt(1, 2)
          // var signe=2;
          if (signe === 1) {
            a = '\\frac{' + num + '}{' + den + '}'
          } else {
            a = '-\\frac{' + num + '}{' + den + '}'
          }
          // on définit r comme multiple du den pour que b soit entier
          k = j3pGetRandomInt(2, 7)
          r = den * k
          if (signe === 1) {
            b = (den - num) * k
          } else {
            b = (den + num) * k
          }
          do {
            u0 = den * j3pGetRandomInt(1, 4) * (2 * j3pGetRandomInt(1, 2) - 3)
            stor.solution[1][0] = u0
            // POUR VERIFIER U1<>U2, obligé de lancer ici les solutions de la q1
            definirSolutionV2(a, b, num, den, alea, signe, k)
          } while (String(stor.solution[1][1]) === String(stor.solution[1][2]) || u0 - r === 1)
        }
        // pour l’énoncé, au format latex donc
        expression = 'u_{n+1}=' + a + 'u_n' + lesigne(b)
        stor.solution[1][0] = u0
        // console.log("u0="+u0)
        me.stockage[1] = a
        me.stockage[2] = b
        me.stockage[3] = r
        me.stockage[4] = expression

        // les solutions de la question 1 :
        definirSolutionV2(a, b, num, den, alea, signe, k)
        // pour la correction détaillée de la q1 :
        me.stockage[5] = 'u_{£a}=£bu_£c' + lesigne(b) + '=£b\\times£e' + lesigne(b) + '=£d'
        // AFFICHAGE DE LA CONSIGNE :
        u0 = stor.solution[1][0]
        me.stockage[21] = phCorr
        /* console.log('phCorr[0]=' + phCorr[0] + ' et me.stockage[21][0]=' + me.stockage[21][0])

        console.log('u0=' + u0)
        console.log('expression=' + expression) */
        j3pAffiche(stor.zoneCons1, '', textes.consigne2_1, { a: u0, b: expression })
        j3pAffiche(stor.zoneCons1, '', textes.consigne2_2, { a: ds.N })

        break

      default :
        // console.log("f(un,n)");
        /* f(u_n,n):
                a(n)*u_n+b(n)
                a et b maxi degré 1 */
        // u0 est donné
        me.stockage[40] = 1
        alea = j3pGetRandomInt(1, 3)
        // var alea=3
        switch (alea) {
          case 1:
            // a*u_n+b*n+c
            do {
              a = j3pGetRandomInt(-9, 9)
              b = j3pGetRandomInt(-9, 9)
              c = j3pGetRandomInt(-9, 9)
            } while (a === 0 || a === 1 || a === -1 || b === 0 || b === -1 || b === 1 || c === 0)
            // pour l’énoncé, au format latex donc
            expression = 'u_{n+1}=' + a + 'u_n' + lesigneFacteur(b) + 'n' + lesigne(c)
            // il faut éviter la suite constante...
            do {
              u0 = j3pGetRandomInt(2, 7)
              u1 = a * u0 + c
            } while (u1 === u0)
            stor.solution[1][0] = u0
            // définition des solutions, d’abord en nombres :
            for (i = 1; i <= (ds.N - 1); i++) {
              stor.solution[1][i] = a * stor.solution[1][i - 1] + b * (i - 1) + c
            }
            // ensuite en txt...
            for (i = 0; i <= (ds.N - 1); i++) {
              phCorr[i] = '$u_' + i + '=' + a + '\\times' + ajoutePar(String(stor.solution[1][i - 1])) + lesigne(b) + '\\times ' + (i - 1) + lesigne(c) + '=' + String(stor.solution[1][i]) + '$'
            }
            break

          case 2:
            // (an+b)u_n+c
            do {
              a = j3pGetRandomInt(-9, 9)
              b = j3pGetRandomInt(-9, 9)
              c = j3pGetRandomInt(-9, 9)
            } while (a === 0 || a === 1 || a === -1 || b === 0 || c === 0)
            // pour l’énoncé, au format latex donc
            expression = 'u_{n+1}=(' + signeFacteur(a) + 'n' + lesigne(b) + ')u_n' + lesigne(c)
            // il faut éviter la suite constante...
            do {
              u0 = j3pGetRandomInt(2, 7)
              u1 = b * u0 + c
            } while (u1 === u0)
            stor.solution[1][0] = u0
            // définition des solutions, d’abord en nombres :
            for (i = 1; i <= (ds.N - 1); i++) {
              stor.solution[1][i] = (a * (i - 1) + b) * stor.solution[1][i - 1] + c
            }
            // ensuite en txt...
            for (i = 0; i <= (ds.N - 1); i++) {
              phCorr[i] = '$u_' + i + '=(' + a + '\\times' + (i - 1) + lesigne(b) + ')\\times ' + ajoutePar(String(stor.solution[1][i - 1])) + lesigne(c) + '=' + String(stor.solution[1][i]) + '$'
            }

            break

          case 3:
            // (an+b)u_n+cn+d
            do {
              a = j3pGetRandomInt(-9, 9)
              b = j3pGetRandomInt(-9, 9)
              c = j3pGetRandomInt(-9, 9)
              d = j3pGetRandomInt(-9, 9)
            } while (a === 0 || a === 1 || a === -1 || b === 0 || c === 0 || c === -1 || c === 1 || d === 0)
            // pour l’énoncé, au format latex donc
            expression = 'u_{n+1}=(' + signeFacteur(a) + 'n' + lesigne(b) + ')u_n' + lesigneFacteur(c) + 'n' + lesigne(d)
            // il faut éviter la suite constante...
            do {
              u0 = j3pGetRandomInt(2, 7)
              u1 = b * u0 + d
            } while (u1 === u0)
            stor.solution[1][0] = u0
            // définition des solutions, d’abord en nombres :
            for (i = 1; i <= (ds.N - 1); i++) {
              stor.solution[1][i] = (a * (i - 1) + b) * stor.solution[1][i - 1] + c * (i - 1) + d
            }
            // ensuite en txt...
            for (i = 0; i <= (ds.N - 1); i++) {
              phCorr[i] = '$u_' + i + '=(' + a + '\\times' + (i - 1) + lesigne(b) + ')\\times ' + ajoutePar(String(stor.solution[1][i - 1])) + lesigne(c) + '\\times ' + (i - 1) + lesigne(d) + '=' + String(stor.solution[1][i]) + '$'
            }

            break
        }

        // AFFICHAGE DE LA CONSIGNE :
        u0 = stor.solution[1][0]
        me.stockage[4] = expression
        // pour la correction :
        me.stockage[21] = phCorr
        /* console.log('phCorr[0]=' + phCorr[0] + ' et me.stockage[21][0]=' + me.stockage[21][0])
        console.log('u0=' + u0)
        console.log('expression=' + expression) */
        j3pAffiche(stor.zoneCons1, '', textes.consigne2_1, { a: u0, b: expression })
        j3pAffiche(stor.zoneCons1, '', textes.consigne2_2, { a: ds.N })

        break
    }
  }
  function ajoutePar (nb) {
    return (nb < 0) ? '(' + nb + ')' : nb
  }
  // gestion de l’affichage de la correction
  function afficheCorrection (bonneReponse) {
    if (bonneReponse) j3pStyle(stor.zoneExpli, { color: me.styles.cbien })
    let sortie, i
    function ajouteParentheses (nb) {
      if (String(nb).includes('\\frac')) {
        // c’est une fraction
        sortie = '(' + nb + ')'
      } else {
        sortie = (nb < 0) ? '(' + nb + ')' : nb
      }
      return sortie
    }
    // console.log("affiche correction")
    j3pDetruit(stor.laPalette)
    if (me.stockage[20] === 'f(un)') {
      for (i = 1; i <= (ds.N - 1); i++) {
        j3pAffiche(stor.zoneExpli, '', '\n$' + me.stockage[5] + '$', { a: i, b: me.stockage[1], c: i - 1, d: stor.solution[1][i], e: ajouteParentheses(stor.solution[1][i - 1]) })
      }
    } else {
      for (i = me.stockage[40]; i <= (ds.N - 1); i++) {
        j3pAffiche(stor.zoneExpli, '', '\n' + me.stockage[21][i])
      }
    }
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        me.construitStructurePage({ structure: 'presentation1' })
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.storage.solution = []
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      stor.solution[1] = []
      stor.solution[2] = []
      stor.solution[3] = []
      stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
      stor.zoneCons1 = j3pAddElt(stor.conteneur, 'div')
      stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px' } })
      // palette MQ :
      // garde-fou :
      if (ds.N > 5) ds.N = 5
      tirageCalcul()
      {
      // suivant SI U0 donné ou non
        const mesZonesSaisie = []
        if (Number(me.stockage[40]) === 1) {
          j3pAffiche(stor.zoneCons1, '', '$u_0=£a$\n', { a: stor.solution[1][0] })
        } else {
          stor.elt = j3pAffiche(stor.zoneCons1, '', '$u_0=$&1&\n', {
            inputmq1: { texte: '' }
          })
          stor.elt.inputmqList[0].addEventListener('focusin', ecoute.bind(null, stor.elt.inputmqList[0], false))
          mesZonesSaisie.push(stor.elt.inputmqList[0].id)
          mqRestriction(stor.elt.inputmqList[0], '\\d,./-', {
            commandes: ['fraction']
          })
          stor.elt.inputmqList[0].typeReponse = ['nombre', 'exact']
          stor.elt.inputmqList[0].reponse = [j3pCalculValeur(stor.solution[1][0])]
        }

        for (let i = 1; i <= (ds.N - 1); i++) {
        // modifier la correction pour affichage attendu (simplifier l’expression)
          stor['elt' + i] = j3pAffiche(stor.zoneCons1, '', '\n$u_' + i + '=$&1&\n', {
            inputmq1: { texte: '' }
          })
          mesZonesSaisie.push(stor['elt' + i].inputmqList[0].id)
          mqRestriction(stor['elt' + i].inputmqList[0], '\\d,./-', {
            commandes: ['fraction'],
            boundingContainer: me.zonesElts.MG
          })
          stor['elt' + i].inputmqList[0].typeReponse = ['nombre', 'exact']
          stor['elt' + i].inputmqList[0].reponse = [j3pCalculValeur(stor.solution[1][i])]
        }
        // je donne le focus à la première zone
        if (Number(me.stockage[40]) === 1) j3pFocus(stor.elt1.inputmqList[0])
        else j3pFocus(stor.elt.inputmqList[0])
        //
        j3pPaletteMathquill(stor.laPalette, stor.elt1.inputmqList[0], { liste: ['fraction'] })
        stor.elt1.inputmqList[0].addEventListener('focusin', ecoute.bind(null, stor.elt1.inputmqList[0], false))
        let div = stor.elt2
        if (div) stor.elt2.inputmqList[0].addEventListener('focusin', ecoute.bind(null, stor.elt2.inputmqList[0], false))
        div = stor.elt3
        if (div) stor.elt3.inputmqList[0].addEventListener('focusin', ecoute.bind(null, stor.elt3.inputmqList[0], false))
        div = stor.elt4
        if (div) stor.elt4.inputmqList[0].addEventListener('focusin', ecoute.bind(null, stor.elt4.inputmqList[0], false))
        stor.zoneCalc = j3pAddElt(me.zonesElts.MD, 'div', '', { style: { padding: '10px' } })
        me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
        j3pCreeFenetres(me)
        j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
        j3pAfficheCroixFenetres('Calculatrice')
        stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.correction', { padding: '10px' }) })
        stor.zoneExpli = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.explications', { padding: '15px' }) })
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: mesZonesSaisie
        })
      }
      me.finEnonce()
      break // case "enonce":

    case 'correction':

      // On teste si une réponse a été saisie
      //  test si réponse, commun aux deux étapes
      {
        const reponse = stor.fctsValid.validationGlobale()
        if (!reponse.aRepondu && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux

              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
}
