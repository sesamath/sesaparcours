import { j3pAddElt, j3pFocus, j3pGetRandomBool, j3pGetRandomInt, j3pRandomTab, j3pShuffle, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import { afficheBloc, phraseBloc } from 'src/legacy/core/functionsPhrase'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Mettre ici une description de la section
 * @author DENIAUD Rémi
 * @since juillet 2022
 * @fileOverview Sens de variation d’une suite arithmétique ou géométrique
 */

// nos constantes
const structure = 'presentation1'
// const ratioGauche = 0.75 C’est la valeur par défaut du ratio de la partie gauche dans presentation1
// N’utiliser cette variable que si ce ratio est différent
// (utilisé dans ce cas dans me.construitStructurePage(structure, ratioGauche) de initSection()

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['typeSuite', 'alea', 'liste', 'Nature de la suite. Choix entre "arithmetique", "geometrique" ou "alea" (choix aléatoire entre ces 2 cas de figure)', ['alea', 'arithmetique', 'geometrique']]
    // les paramètres peuvent être du type 'entier', 'string', 'boolean', 'liste'
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titreExo1: 'Sens de variation d’une suite arithmétique',
  titreExo2: 'Sens de variation d’une suite géométrique',
  titreExo3: 'Sens de variation d’une suite',
  // on donne les phrases de la consigne
  consigne1_1: 'Soit $(£u_n)$ la suite arithmétique de raison $£{rTxt}$ et de premier terme $£u_{£{n0}}=£{un0Txt}$.',
  consigne1_2: 'Soit $(£u_n)$ la suite géométrique de raison $£{rTxt}$ et de premier terme $£u_{£{n0}}=£{un0Txt}$.',
  consigne2: 'Donner le sens de variation de cette suite.',
  consigne3: 'La suite $(£u_n)$ est #1#.',
  choix: 'croissante|décroissante|constante|non monotone',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'La suite $(£u_n)$ est arithmétique avec une raison strictement positive. Donc cette suite est croissante.',
  corr1_2: 'La suite $(£u_n)$ est arithmétique avec une raison strictement négative. Donc cette suite est décroissante.',
  corr2_1: 'La suite $(£u_n)$ est géométrique avec une raison strictement supérieure à 1 et un premier terme strictement positif. Donc cette suite est croissante.',
  corr2_2: 'La suite $(£u_n)$ est géométrique avec une raison strictement supérieure à 1 et un premier terme strictement négatif. Donc cette suite est décroissante.',
  corr2_3: 'La suite $(£u_n)$ est géométrique avec une raison strictement négative. Donc cette suite est non monotone.',
  corr2_4: 'La suite $(£u_n)$ est géométrique avec une raison comprise entre 0 et 1 et un premier terme strictement positif. Donc cette suite est décroissante.',
  corr2_5: 'La suite $(£u_n)$ est géométrique avec une raison comprise entre 0 et 1 et un premier terme strictement négatif. Donc cette suite est croissante.'
}
/**
 * La fonction principale, rappelée à chaque étape
 * Son this sera l’instance courante de Parcours
 * @this {Parcours}
 */
export default function maSection () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini (en général dans enonceEnd(me)), sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance courante de Parcours (objet this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  const stor = me.storage
  // Construction de la page
  me.construitStructurePage(structure) // L’argument devient {structure:structure, ratioGauche:ratioGauche} si ratioGauche est défini comme variable globale
  const titreExo = (me.donneesSection.typeSuite === 'arithmetique')
    ? textes.titreExo1
    : (me.donneesSection.typeSuite === 'geometrique')
        ? textes.titreExo2
        : textes.titreExo3
  me.afficheTitre(titreExo)
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
  if (me.donneesSection.typeSuite === 'alea') {
    let nbArith = 0
    let nbGeo = 0
    stor.lstQuest = []
    for (let i = 0; i < me.donneesSection.nbrepetitions; i++) {
      if (nbArith === me.donneesSection.nbrepetitions - 1) {
        stor.lstQuest.push('geometrique')
      } else if (nbGeo === me.donneesSection.nbrepetitions - 1) {
        stor.lstQuest.push('arithmetique')
      } else {
        const alea = (j3pGetRandomBool()) ? 'arithmetique' : 'geometrique'
        if (alea === 'arithmetique') nbArith++
        else nbGeo++
        stor.lstQuest.push(alea)
      }
      stor.lstQuest = j3pShuffle(stor.lstQuest)
    }
  } else {
    stor.lstQuest = Array(me.donneesSection.nbrepetitions).fill(me.donneesSection.typeSuite)
  }
  // console.log('stor.lstQuest:', stor.lstQuest)
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone

  // ou stocker des variables, par exemples celles qui serviraient pour la pe

  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}
/**
 * Retourne les données de l’exercice
 * @private
 * @return {Object}
 */
function genereAlea (type) {
  /**
   * données de l’exercice qui peuvent être les variables présentes dans les textes (consignes ou corrections)
   * ou autre données utiles à la validation
   * @private
   * @param {string} type nature de la suite ('arithmetique' ou 'geometrique')
   * @type {Object} obj
   */
  const obj = {}
  obj.u = j3pRandomTab(['u', 'v', 'w'], [0.334, 0.333, 0.333])
  if (type === 'arithmetique') {
    obj.r = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(3, 60) / 10
  } else {
    // suite géométrique
    const leSigne = j3pRandomTab([-1, 1], [0.666, 0.334])
    const val = (j3pGetRandomBool()) ? j3pGetRandomInt(2, 9) / 10 : j3pGetRandomInt(14, 30) / 5
    obj.r = leSigne * val
  }
  do {
    obj.un0 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(3, 10) / 2
  } while (Math.abs(obj.r - obj.un0) < Math.pow(10, -12))
  obj.n0 = j3pRandomTab([0, 1], [0.666, 0.334])
  obj.rTxt = j3pVirgule(obj.r)
  obj.un0Txt = j3pVirgule(obj.un0)
  return obj
}
/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  // on affiche l’énoncé
  // La style de base (ici petit.enonce) peut être complété par d’autres propriétés css. C’est fait ici avec j3pStyle
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.enonce })
  j3pStyle(stor.elts.divEnonce, { padding: '10px' })
  // On génère les données de l’exo (stockées dans un objet dont on se sert entre autre pour les phrases de l’énoncé)
  stor.natureSuite = stor.lstQuest[me.questionCourante - 1]
  stor.objVariables = genereAlea(stor.natureSuite)
  // On écrit la première phrase (dans un bloc pour assurer un retour à la ligne)
  const consigne1 = (stor.natureSuite === 'arithmetique') ? textes.consigne1_1 : textes.consigne1_2
  afficheBloc(stor.elts.divEnonce, consigne1, stor.objVariables)
  phraseBloc(stor.elts.divEnonce, textes.consigne2)
  const eltCons3 = afficheBloc(stor.elts.divEnonce, textes.consigne3, {
    u: stor.objVariables.u,
    liste1: { texte: [''].concat(textes.choix.split('|')) }
  })
  stor.zoneSelect = eltCons3.selectList[0]
  if (stor.natureSuite === 'arithmetique') {
    stor.zoneSelect.reponse = [(stor.objVariables.r > 0) ? 1 : 2]
  } else {
    if (stor.objVariables.r < 0) {
      stor.zoneSelect.reponse = [4]
    } else if (stor.objVariables.r > 1) {
      stor.zoneSelect.reponse = [(stor.objVariables.un0 > 0) ? 1 : 2]
    } else {
      stor.zoneSelect.reponse = [(stor.objVariables.un0 > 0) ? 2 : 1]
    }
  }
  stor.fctsValid = new ValidationZones({
    zones: [stor.zoneSelect],
    parcours: me
  })
  j3pFocus(stor.zoneSelect)
  // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvent dans le cadre MD)
  // Il n’est ainsi construit qu’une seule fois et vidé à chaque passage dans correction()
  stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.moyen.correction })
  j3pStyle(stor.zoneCorr, { padding: '10px' })
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me, bonneReponse = false) {
  const stor = me.storage
  const zoneExpli = j3pAddElt(stor.elts.divEnonce, 'div', '', { style: me.styles.petit.correction })
  // La couleur de la correction sera fonction de la qualité de la réponse de l’élève
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  const laCorrection = (stor.natureSuite === 'arithmetique')
    ? (stor.objVariables.r > 0) ? textes.corr1_1 : textes.corr1_2
    : (stor.objVariables.r < 0)
        ? textes.corr2_3
        : (stor.objVariables.r > 1)
            ? (stor.objVariables.un0 > 0) ? textes.corr2_1 : textes.corr2_2
            : (stor.objVariables.un0 > 0) ? textes.corr2_4 : textes.corr2_5
  afficheBloc(zoneExpli, laCorrection, stor.objVariables)
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number|null}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  if (reponse.aRepondu) {
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
}

function correction (me) {
  const stor = me.storage
  const ds = me.donneesSection
  const score = getScore(me) // null si pas répondu
  if ((score === 1) || (me.essaiCourant >= ds.nbchances)) me.score += score
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.zoneCorr)
    return
  }
  if (score !== 1) {
    // On écrit c’est faux
    me.reponseKo(stor.zoneCorr)
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if ((score !== 1) && me.essaiCourant < me.donneesSection.nbchances) {
    me.reponseKo(stor.zoneCorr, '<br>' + essaieEncore, true)
    return
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.zoneCorr)
    // Même dans le cas d’une bonne réponse, on peut afficher la correction qui aurait dans ce cas d’éventuels compléments d’information
    afficheCorrection(me, true)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, tempsDepasse, true)
    }
    afficheCorrection(me)
  }
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) {
    me.etat = 'enonce'
  }
  // Si on a rien à afficher de particulier après la dernière répétition, donc on passe true à finNavigation
  // pour que le modèle affiche directement le bouton "section suivante" (sans devoir cliquer sur 'suite' avant)
  me.finNavigation(true)
}
