import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pDetruit, j3pElement, j3pEmpty, j3pGetNewId, j3pGetRandomInt, j3pMonome, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since juin 2022
 * @fileOverview Cette section est juste un QCM sur la monotonie d’une suite définie à l’aide d’une intégrale
 */

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes']
  ]
}
const textes = {
  titreExo: 'Variation d’une suite définie par une intégrale',
  consigne1: 'Soit $(I_n)$ la suite définie pour tout entier naturel $n$ par $I_n=£I$.',
  consigne2: 'Cette suite est',
  croissante: 'croissante',
  decroissante: 'décroissante',
  comment1: '',
  corr1: 'Pour tout entier naturel $n$,',
  corr2: 'pour tout réel $t\\in£i$, $£{ineq1}$.',
  corr4: 'Ainsi $£{ineq2}$ et $£{ineq3}$.',
  corr3_1: 'Donc la suite $(I_n)$ est bien croissante.',
  corr3_2: 'Donc la suite $(I_n)$ est bien décroissante.'
}
/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour chaque sujet
 * @private
 * @return {Object}utilise la propriété de concentration d’une variable aléatoire autour de son espérance
 */
function genereAlea (casFigure) {
  const obj = {}
  obj.a = j3pGetRandomInt(0, 1)
  obj.b = obj.a + 1
  obj.k = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 2)
  obj.arrayIneq = [] // Je mets la succession des inégalités qui conduisent à la conclusion
  switch (casFigure) {
    case 1:
      // Intégrale de la forme I_n=\int_a^b t^nexp(kt) dt
      obj.I = '\\int_' + obj.a + '^' + obj.b + 't^n\\mathrm{e}^{' + j3pMonome(1, 1, obj.k, 't') + '}\\text{d}t'
      obj.signe = (obj.a === 0) ? '\\geq ' : '\\leq '
      obj.arrayIneq.push('t^n' + obj.signe + 't^{n+1}')
      obj.arrayIneq.push('t^n\\mathrm{e}^{kt}' + obj.signe + 't^{n+1}\\mathrm{e}^{kt}')
      obj.ineq2 = '\\int_' + obj.a + '^' + obj.b + 't^n\\mathrm{e}^{kt}\\text{d}t' + obj.signe + '\\int_' + obj.a + '^' + obj.b + 't^{n+1}\\mathrm{e}^{kt}\\text{d}t'
      break
    case 2:
      obj.I = '\\int_' + obj.a + '^' + obj.b + '(1-t^n)\\mathrm{e}^{' + j3pMonome(1, 1, obj.k, 't') + '}\\text{d}t'
      obj.signe1 = (obj.a === 0) ? '\\geq ' : '\\leq '
      obj.signe = (obj.a === 1) ? '\\geq ' : '\\leq '
      obj.arrayIneq.push('t^n' + obj.signe1 + 't^{n+1}')
      obj.arrayIneq.push('-t^n' + obj.signe + '-t^{n+1}')
      obj.arrayIneq.push('1-t^n' + obj.signe + '1-t^{n+1}')
      obj.arrayIneq.push('(1-t^n)\\mathrm{e}^{kt}' + obj.signe + '(1-t^{n+1})\\mathrm{e}^{kt}')
      obj.ineq2 = '\\int_' + obj.a + '^' + obj.b + '(1-t^n)\\mathrm{e}^{kt}\\text{d}t' + obj.signe + '\\int_' + obj.a + '^' + obj.b + '(1-t^{n+1})\\mathrm{e}^{kt}\\text{d}t'
      break
    case 3:
      obj.b = obj.a + j3pGetRandomInt(1, 3)
      obj.I = '\\int_' + obj.a + '^' + obj.b + 't\\mathrm{e}^{nt}\\text{d}t'
      obj.signe = '\\leq '
      obj.arrayIneq.push('nt' + obj.signe + '(n+1)t')
      obj.arrayIneq.push('\\mathrm{e}^{nt}' + obj.signe + '\\mathrm{e}^{(n+1)t}')
      obj.arrayIneq.push('t\\mathrm{e}^{nt}' + obj.signe + 't\\mathrm{e}^{(n+1)t}')
      obj.ineq2 = '\\int_' + obj.a + '^' + obj.b + 't\\mathrm{e}^{nt}\\text{d}t' + obj.signe + '\\int_' + obj.a + '^' + obj.b + 't\\mathrm{e}^{(n+1)t}\\text{d}t'
      break
  }
  obj.ineq1 = obj.arrayIneq[0]
  obj.ineq3 = 'I_n' + obj.signe + 'I_{n+1}'
  obj.i = '[' + obj.a + ';' + obj.b + ']'
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(textes.titreExo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
  stor.listeQuest = []
  let typeQuest = [1, 2, 3]
  const typeQuestInit = [1, 2, 3]
  for (let i = 1; i <= me.donneesSection.nbrepetitions; i++) {
    const alea = j3pGetRandomInt(0, typeQuest.length - 1)
    stor.listeQuest.push(typeQuest[alea])
    typeQuest.splice(alea, 1)
    if (typeQuest.length === 0) typeQuest = [...typeQuestInit]
  }
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.laQuest = stor.listeQuest[me.questionCourante - 1]
  stor.objDonnees = genereAlea(stor.laQuest)
  // on affiche l’énoncé
  ;[1, 2].forEach(i => {
    stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor['zoneCons' + i], '', textes['consigne' + i], stor.objDonnees)
  })
  const zoneCons3 = j3pAddElt(stor.conteneur, 'div')
  const espacetxt = '&nbsp;&nbsp;&nbsp;&nbsp;'
  // zoneCons3, je mets les boutons radio
  stor.nameRadio = j3pGetNewId('choix')
  stor.idsRadio = ['1', '2'].map(i => j3pGetNewId('radio' + i))
  j3pAffiche(zoneCons3, '', espacetxt)
  j3pBoutonRadio(zoneCons3, stor.idsRadio[0], stor.nameRadio, 0, textes.croissante)
  j3pAffiche(zoneCons3, '', espacetxt + espacetxt)
  j3pBoutonRadio(zoneCons3, stor.idsRadio[1], stor.nameRadio, 1, textes.decroissante)
  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })

  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  j3pDetruit(stor.zoneCalc)
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  ;[1, 2].forEach(i => {
    stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor['zoneExpli' + i], '', textes['corr' + i], stor.objDonnees)
  })
  stor.objDonnees.arrayIneq.slice(1).forEach(eq => {
    const zoneExplii = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExplii, '', '$\\iff ' + eq + '$')
  })
  const zoneExplin = j3pAddElt(zoneExpli, 'div')
  j3pAffiche(zoneExplin, '', textes.corr4, stor.objDonnees)
  const zoneExpliLast = j3pAddElt(zoneExpli, 'div')
  const ccl = (stor.objDonnees.signe === '\\geq ') ? textes.corr3_2 : textes.corr3_1
  j3pAffiche(zoneExpliLast, '', ccl)
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  stor.choixRadio = j3pBoutonRadioChecked(stor.nameRadio)[0]
  const reponse = { aRepondu: false, bonneReponse: false }
  reponse.aRepondu = stor.choixRadio > -1
  if (reponse.aRepondu) {
    reponse.bonneReponse = (stor.objDonnees.signe === '\\geq ') ? (stor.choixRadio === 1) : (stor.choixRadio === 0)
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }
  j3pElement('label' + stor.idsRadio[stor.choixRadio]).style.color = (score === 1)
    ? me.styles.cbien
    : me.styles.cfaux
  stor.idsRadio.forEach(elt => j3pDesactive(elt))
  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    j3pBarre('label' + stor.idsRadio[stor.choixRadio])
    j3pElement('label' + stor.idsRadio[1 - stor.choixRadio]).style.color = me.styles.moyen.correction.color
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
