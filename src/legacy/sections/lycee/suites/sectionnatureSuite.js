import { j3pAddElt, j3pEmpty, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomBool, j3pGetRandomElts, j3pGetRandomInt, j3pNombre, j3pRestriction } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { testIntervalleFermeDecimaux } from 'src/lib/utils/regexp'
import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import { aleatoire, arithmetique, geometrique, isArithVariant, isGeoVariant } from './constantesSuites'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// on réexporte la fct d’upgrade générique de nos suites
export { upgradeParametres } from './constantesSuites'

/*
        DENIAUD Rémi
        octobre 2016
        Dans cette section, on définit une suite (arithmétique ou géométrique) par sa relation de récurrence et on demande sa nature
        Section adaptée au clavier virtuel en 10/2021
 */

// les types possibles pour cette section
const typesSuites = [aleatoire, arithmetique, geometrique]

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['r', '[-7;7]', 'string', '(ou [1;10]$ pour le niveau TES) Intervalle où se trouve la raison de la suite arithmétique ou géométrique (bien sûr r non nul et différent de 1 si la suite est géométrique ; avec ce type d’intervalle, il peut être décimal). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;6]|[-6;-1]" pour qu’il soit égal à 2 dans un premier temps, entier positif ensuite puis entier négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier, décimal ou fractionnaire.'],
    ['termeInitial', ['0|[0.4;5]', '0|[-4;-1]', '1|[1;6]'], 'array', 'Tableau de taille nbrepetitions où chaque terme contient deux éléments séparés par | : le premier est l’indice du premier terme et le second sa valeur (imposée ou aléatoire dans un intervalle).'],
    ['typeSuite', aleatoire, 'liste', 'Nature de la suite. Par défaut, ce sera aléatoire d’une question à l’autre.', typesSuites],
    ['niveau', 'TS', 'liste', 'TS par défaut mais on peut mettre "TES" pour que la raison de la suite géométrique soit positive.', ['TS', 'TES']],
    ['GeoRaisonFrac', false, 'boolean', 'Dans le cas d’une suite géométrique, on peut imposer à écrire la relation de récurrence sous forme d’une fraction (u_{n+1}=a*u_n/b)']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Nature de suite',
  // on donne les phrases de la consigne
  consigne1: 'Soit $(£u_n)$ la suite définie par $£u_{£n}=£i$ et, pour tout entier $n\\geq £n$, par $£u_{n+1}=£j$.',
  consigne2: 'On s’intéresse à la nature de cette suite $(£u_n)$.',
  consigne3: 'La suite $(£u_n)$ est @1@.',
  consigne4: 'De plus sa raison vaut &1& et son premier terme $£u_{\\editable{}}=$&3&.',
  // gestion de cas de figure qui ont du sens mais qui ne répondent pas à la question sur la nature de la suite
  croissante: 'croissante|croissant|croisante',
  decroissante: 'décroissante|decroissante|decroissant|décroissant|décroisante|decroisante',
  convergente: 'convergente|convergent',
  divergente: 'divergente|divergent',
  positive: 'positive|positif',
  negative: 'négative|negative|négatif|negatif',
  majoree: 'majorée|majoree|majoré|majore',
  minoree: 'minorée|minoree|minoré|minore',
  alternee: 'alternée|alternee|alterné|alterne',
  recurrente: 'récurrente|recurrente|récurente|recurente|récurrent|recurrent',
  explicite: 'explicite',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Ce qui est écrit n’a pas de sens !',
  comment1_2: 'On demande la nature de la suite. La réponse donnée a du sens mais elle ne répond pas à cette question !',
  comment1_3: 'On demande la nature de la suite. La réponse donnée a du sens mais elle est inexacte et, dans tous les cas, ne répond pas à cette question !',
  comment2: 'La suite n’est pas arithmétique !',
  comment3: 'La suite n’est pas géométrique !',
  comment4: 'La suite n’est pas arithmético-géométrique !',
  comment5: 'Il faut aussi compléter le rang du premier terme de la suite !',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Pour tout entier $n\\geq £n$, $£u_{n+1}=£j$ où $£r$ est une constante.',
  corr2: 'Donc la suite $(£u_n)$ est géométrique de raison $£r$ et de premier terme $£u_{£n}=£i$.',
  corr3: 'Donc la suite $(£u_n)$ est arithmétique de raison $£r$ et de premier terme $£u_{£n}=£i$.'
}

/**
 * section natureSuite
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let k = 1; k <= 2; k++) stor['zoneExpli' + k] = j3pAddElt(zoneExpli, 'div')
    const affichageReleRec = ((stor.defSuite.typeSuite === arithmetique) && (j3pCalculValeur(stor.defSuite.r) < -Math.pow(10, -12))) ? stor.UnBis1 + '=' + stor.defSuite.nomU + '_n+(' + stor.defSuite.r + ')' : stor.UnBis1
    j3pAffiche(stor.zoneExpli1, '', textes.corr1,
      {
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        u: stor.defSuite.nomU,
        j: affichageReleRec,
        r: stor.defSuite.ecrireR
      })
    const casCorr2 = (stor.defSuite.typeSuite === arithmetique) ? textes.corr3 : textes.corr2
    j3pAffiche(stor.zoneExpli2, '', casCorr2,
      {
        u: stor.defSuite.nomU,
        r: stor.defSuite.ecrireR,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        i: stor.defSuite.termeInit[me.questionCourante - 1][1]
      })
  }

  function genereAlea (int) {
    // int est un intervalle
    // mais on peut trouver des cas de figure où par faignantise, on aura utilisé cette fonction sans vérifier que int est un intervalle
    // Ce serait alors un nombre
    if (String(int).charAt(0) === '[') {
      // si les bornes sont entières, alors on génère un nombre entier aléatoire dans cet intervalle
      // si elles sont décimales, il faut que je génère un décimal de l’intervalle
      const PosPointVirgule = int.indexOf(';')
      let val1 = j3pNombre(int.substring(1, PosPointVirgule))
      let val2 = j3pNombre(int.substring(PosPointVirgule + 1, int.length - 1))
      let posPoint = -1
      if ((String(val1).indexOf('.') === -1)) {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = String(val2).length - String(val2).lastIndexOf('.') - 1
        }
      } else {
        if ((String(val2).indexOf('.') > -1)) {
          posPoint = Math.max(String(val1).length - String(val1).lastIndexOf('.') - 1, String(val2).length - String(val2).lastIndexOf('.') - 1)
        } else {
          posPoint = String(val1).length - String(val1).lastIndexOf('.') - 1
        }
      }
      if (val1 > val2) {
        // les bornes ne sont pas dans le bon ordre
        const val3 = val1
        val1 = val2
        val2 = val3
      }
      if (posPoint === -1) {
        // je n’ai que des nombres entiers
        const [minInt, maxInt] = j3pGetBornesIntervalle(int)
        return j3pGetRandomInt(minInt, maxInt)
      } else {
        // j’ai des décimaux
        const borneInf = val1 * Math.pow(10, posPoint)
        const borneSup = val2 * Math.pow(10, posPoint)
        const nbAlea = j3pGetRandomInt(String(borneInf), String(borneSup)) * Math.pow(10, -posPoint)
        return Math.round(nbAlea * Math.pow(10, 10)) / Math.pow(10, 10)
      }
    } else {
      return int
    }
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut, nbRepet, tabValInterdites) {
    // cette fonction permet de vérifier si ce qui est renseigné est bien un intervalle ou un nombre (éventuellement fractionnaire) et ce plusieurs fois (en général nbrepetitions)
    // cela permet de valider ce qui peut être saisi par un utilisateur dans le cas où on peut imposer des valeurs
    // texteIntervalle est une chaîne où les éléments peuvent être séparés par '|' et où chaque élément est un nombre, une fraction ou un intervalle
    // si c’est un intervalle, la fonction va dans un second temps générer de manière aléatoire une valeur de cet intervalle
    // il peut y avoir des valeurs interdites précisées dans tabValInterdites, même si elles peuvent être dans l’intervalle
    // tabValInterdites peut contenir des valeurs numérique ou des tableaux de valeurs numériques
    // ce second cas de figure est utilisé si d’une repet à l’autre les valeurs interdites sont différentes
    // la fonction renvoie un tableau de nbRepet éléments
    // chaque élément est un intervalle voire l’intervalle par défaut si ce qui est précisé n’est aps compris
    // ce peut aussi être un nombre entier, décimal ou fractionnaire
    // dans ce dernier cas, on renvoit une chaine où on retrouve le code latex et la valeur décimale (arrondie) séparés par |
    /* cette fonction renvoie un objet. Cet objet contient 3 tableaux de nbRepet éléments
        - leType[k] vaut 'nombre', 'fraction' ou 'intervalle' suivant la valeur de texteIntervalle
        - expression contient l’écriture de chaque valeur imposée ou gérée aléatoirement quand leType[k] vaut 'intervalle'
        - valeur contient la valeur décimale de ces nombres, cela ne peut être utile que lorsque leType[k] vaut 'fraction'
        */
    function lgIntervalle (intervalle) {
      // cette fonction renvoie la longueur de l’intervalle'
      // si ce n’est pas un intervalle, alors elle renvoie 0'
      let lg = 0
      // var intervalleReg = new RegExp("\\[\\-?[0-9]{1,};\\-?[0-9]{1,}\\]","i");
      // const intervalleReg = new RegExp('\\[\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,};\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,}\\]', 'i')
      if (testIntervalleFermeDecimaux.test(intervalle)) {
        // on a bien un intervalle
        const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
        const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
        lg = borneSup - borneInf
      }
      return lg
    }

    function nbDansTab (nb, tab) {
      // test si la valeur correspondant à nb est dans le tableau tab (ses valeurs pouvant être au format latex)
      if (tab === undefined) {
        return false
      } else {
        let puisPrecision = 15
        if (Math.abs(nb) > Math.pow(10, -13)) {
          const ordrePuissance = Math.floor(Math.log(Math.abs(nb)) / Math.log(10)) + 1
          puisPrecision = 15 - ordrePuissance
        }
        for (let k = 0; k < tab.length; k++) {
          if (Math.abs(nb - j3pCalculValeur(tab[k])) < Math.pow(10, -puisPrecision)) {
            return true
          }
        }
        return false
      }
    }

    const intervalleReg = testIntervalleFermeDecimaux // new RegExp('\\[\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,};\\-?[0-9]{1,}(\\.|\\,)?[0-9]{0,}\\]', 'i')
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const DonneesIntervalle = {}
    // DonneesIntervalle contient trois propriétés qui sont des tableaux de longueur nbRepet :
    // - leType : qui correspond à ce qui a été renseigné initialement ('intervalle', 'nombre', 'fraction') - 'nombre' désigne un entier ou un décimal
    // - expression : qui est le nombre (si type==='nombre'), une valeur générée aléatoirement dans un intervalle (si type==='intervalle'), l’écriture l’atex de la fraction (si type==='fraction')
    // - valeur : qui est la valeur décimale de l’expression (utile surtout si type==='fraction')
    DonneesIntervalle.leType = []
    DonneesIntervalle.expression = []
    DonneesIntervalle.valeur = []
    // gestions de valeurs interdites
    const tabInterdites = []
    if (tabValInterdites !== undefined) {
      if (typeof (tabValInterdites[0]) === 'number') {
        // dans ce cas, les valeurs interdites seront les mêmes d’une répétition à l’autre
        for (k = 0; k < nbRepet; k++) {
          tabInterdites.push(tabValInterdites)
        }
      } else {
        for (k = 0; k < nbRepet; k++) {
          tabInterdites.push(tabValInterdites[k])
        }
      }
    }
    let valNb, nbAlea
    let tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).indexOf('|') > -1) {
      const tableau = texteIntervalle.split('|')
      for (k = 0; k < nbRepet; k++) {
        if (intervalleReg.test(tableau[k])) {
          // c’est bien un intervalle'
          if (lgIntervalle(tableau[k]) === 0) {
            // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
            valNb = tableau[k].substring(1, tableau[k].indexOf(';'))
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          } else {
            do {
              nbAlea = genereAlea(tableau[k])
            } while (nbDansTab(nbAlea, tabInterdites[k]))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        } else {
          // peut être est-ce tout simplement un nombre :
          if ((tableau[k] === undefined) || (tableau[k] === '')) {
            // là l’expression n’est pas renseignée
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabInterdites[k]))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          } else if (nbFracReg.test(tableau[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tableau[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
            DonneesIntervalle.leType.push('fraction')
            DonneesIntervalle.valeur.push(valFrac)
          } else if (!isNaN(j3pNombre(tableau[k]))) {
            // on a un nombre
            DonneesIntervalle.expression.push(tableau[k])
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(tableau[k])
          } else {
            do {
              nbAlea = genereAlea(intervalleDefaut)
            } while (nbDansTab(nbAlea, tabInterdites[k]))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      }
    } else {
      if ((texteIntervalle === '') || (texteIntervalle === undefined)) {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabInterdites[k]))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      } else if (intervalleReg.test(String(texteIntervalle))) {
        if (lgIntervalle(String(texteIntervalle)) === 0) {
          // C’est un intervalle de longueur nulle, donc finalement c’est juste une valeur
          valNb = String(texteIntervalle).substring(1, String(texteIntervalle).indexOf(';'))
          for (k = 0; k < nbRepet; k++) {
            DonneesIntervalle.expression.push(valNb)
            DonneesIntervalle.leType.push('nombre')
            DonneesIntervalle.valeur.push(valNb)
          }
        } else {
          for (k = 0; k < nbRepet; k++) {
            do {
              nbAlea = genereAlea(texteIntervalle)
            } while (nbDansTab(nbAlea, tabInterdites[k]))
            DonneesIntervalle.expression.push(nbAlea)
            DonneesIntervalle.leType.push('intervalle')
            DonneesIntervalle.valeur.push(nbAlea)
          }
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push('\\frac{' + monnum + '}{' + monden + '}')
          DonneesIntervalle.leType.push('fraction')
          DonneesIntervalle.valeur.push(valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < nbRepet; k++) {
          DonneesIntervalle.expression.push(String(texteIntervalle))
          DonneesIntervalle.leType.push('nombre')
          DonneesIntervalle.valeur.push(String(texteIntervalle))
        }
      } else {
        for (k = 0; k < nbRepet; k++) {
          do {
            nbAlea = genereAlea(intervalleDefaut)
          } while (nbDansTab(nbAlea, tabInterdites[k]))
          DonneesIntervalle.expression.push(nbAlea)
          DonneesIntervalle.leType.push('intervalle')
          DonneesIntervalle.valeur.push(nbAlea)
        }
      }
    }
    // console.log('expression:'+DonneesIntervalle.expression)
    return DonneesIntervalle
  }

  function finQuestion (type) {
    if (((type === geometrique) || (type === arithmetique)) && !stor.dernierePhraseVisible) {
      stor.dernierePhraseVisible = true
      // consigne4 : 'De plus sa raison vaut &1& et son premier terme &2&$=&3&.'
      stor.elt2 = j3pAffiche(stor.zoneCons4, '', textes.consigne4,
        {
          inputmq1: { texte: '' },
          u: stor.defSuite.nomU,
          inputmq3: { texte: '' }
        })
      stor.elt2.inputmqList[0].typeReponse = ['nombre', 'exact']
      stor.elt2.inputmqList[0].reponse = [j3pCalculValeur(stor.defSuite.r)]
      stor.elt2.inputmqefList[0].typeReponse = ['nombre', 'exact']
      stor.elt2.inputmqefList[0].reponse = [stor.defSuite.termeInit[me.questionCourante - 1][0]]
      stor.elt2.inputmqList[1].typeReponse = ['nombre', 'exact']
      stor.elt2.inputmqList[1].reponse = [j3pCalculValeur(stor.defSuite.termeInit[me.questionCourante - 1][1])]
      mqRestriction(stor.elt2.inputmqList[0], '\\d.,/-+')
      mqRestriction(stor.elt2.inputmqList[1], '\\d.,/-+')
      mqRestriction(stor.elt2.inputmqefList[0], '\\d.,/-+')
      j3pFocus(stor.elt2.inputmqList[0], true)
      stor.mesZonesSaisie = [stor.elt1.inputList[0], stor.elt2.inputmqList[0], stor.elt2.inputmqefList[0], stor.elt2.inputmqList[1]]
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: stor.mesZonesSaisie
      })
    }
  }

  function initSection () {
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
    me.afficheTitre(textes.titre_exo)
    stor.defSuite = {}

    stor.defSuite.niveau = (ds.niveau.toUpperCase() === 'TES') ? 'TES' : 'TS'
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    stor.tabtypeSuite = []
    if (ds.typeSuite === geometrique) {
      while (stor.tabtypeSuite.length < ds.nbrepetitions) stor.tabtypeSuite.push(geometrique)
    } else if (ds.typeSuite === arithmetique) {
      while (stor.tabtypeSuite.length < ds.nbrepetitions) stor.tabtypeSuite.push(arithmetique)
    } else {
      // aléatoire
      while (stor.tabtypeSuite.length < ds.nbrepetitions) stor.tabtypeSuite.push(j3pGetRandomBool() ? arithmetique : geometrique)
    }
    // ici d’une question à l’autre la valeur interdite n’est pas la même
    const tableauValInterdites = []

    for (let k = 0; k < ds.nbrepetitions; k++) {
      tableauValInterdites[k] = (stor.tabtypeSuite[k] === arithmetique) ? [0] : [0, 1, -1]
    }
    stor.defSuite.coefRDonnees = constructionTabIntervalle(ds.r, '[-7;7]', ds.nbrepetitions, tableauValInterdites)
    stor.coefR = stor.defSuite.coefRDonnees.expression

    // je fais un correctif dans le cas d’une suite géométrique si je veux que la raison soit un peu moins facile à lire :
    // u_{n+1}=a*u_n/b
    const tabQuotient = [[1, 3], [2, 3], [-1, 3], [-2, 3], [1, 5], [2, 5], [3, 5], [2, 7], [1, 6], [3, 7], [-5, 6], [-4, 7], [-4, 5], [-3, 5]]
    stor.NumDenFraction = []
    for (let k = 0; k < ds.nbrepetitions; k++) {
      if ((stor.tabtypeSuite[k] === geometrique) && ((ds.GeoRaisonFrac) || (ds.GeoRaisonFrac === 'true'))) {
        stor.GeoRaisonFrac = true
        const alea = j3pGetRandomInt(0, (tabQuotient.length - 1))
        stor.NumDenFraction.push(tabQuotient[alea])
        tabQuotient.splice(alea, 1)
      } else {
        stor.NumDenFraction.push([])
      }
    }

    // il faut que je gère le terme initial car j’ai un tableau dont les données sont de la forme 1|[...;...] ce second point pouvant être un entier ou un nb fractionnaire
    stor.defSuite.termeInit = []
    const TermeInitParDefaut = [0, '[0.4;5]']
    if (!isNaN(j3pNombre(String(ds.termeInitial)))) {
      // au lieu d’un tableau, n’est indiqué qu’un seul nombre. Celui-ci devient le rang initial de chaque répétition
      const leRangInit = (typeof ds.termeInitial === 'object' && ds.termeInitial.length === 0) ? 0 : j3pNombre(ds.termeInitial)
      for (let k = 0; k < ds.nbrepetitions; k++) {
        stor.defSuite.termeInit.push([leRangInit, genereAlea(TermeInitParDefaut[1])])
      }
    }
    for (let k = 0; k < ds.nbrepetitions; k++) {
      if ((ds.termeInitial[k] === '') || (ds.termeInitial[k] === undefined)) {
        // on a rien renseigné donc par défaut, on met
        stor.defSuite.termeInit.push([TermeInitParDefaut[0], genereAlea(TermeInitParDefaut[1])])
      } else {
        // il faut la barre verticale pour séparer le rang et la valeur du terme initial
        const posBarre = String(ds.termeInitial[k]).indexOf('|')
        if (posBarre === -1) {
          const EntierReg = /[0-9]+/ // new RegExp('[0-9]{1,}', 'i')
          // je n’ai pas la barre verticale
          // peut-être ai-je seulement l’indice du premier terme
          if (EntierReg.test(ds.termeInitial[k])) {
            // c’est un entier correspondant à l’indice du terme initial
            stor.defSuite.termeInit.push([ds.termeInitial[k], genereAlea(TermeInitParDefaut[1])])
          } else {
            // donc je réinitialise avec la valeur par défaut
            stor.defSuite.termeInit.push([TermeInitParDefaut[0], genereAlea(TermeInitParDefaut[1])])
          }
        } else {
          // la valeur (éventuellement aléatoire) du terme initial
          const termInitAlea = constructionTabIntervalle(ds.termeInitial[k].substring(posBarre + 1), TermeInitParDefaut[1], 1).expression[0]
          // au cas où je vérifie que la première valeur est bien entière
          let rangInit = ds.termeInitial[k].substring(0, posBarre)
          rangInit = (String(j3pNombre(rangInit)).indexOf('.') === -1) ? rangInit : TermeInitParDefaut[0]
          stor.defSuite.termeInit.push([rangInit, termInitAlea])
        }
      }
    }
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '8px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // choix du nom des suites
    stor.defSuite.nomU = j3pGetRandomElts(['u', 'v'], 1)[0]
    // je mets mes valeurs dans une variable plus simple :
    stor.defSuite.r = stor.coefR[me.questionCourante - 1]
    stor.defSuite.typeSuite = stor.tabtypeSuite[me.questionCourante - 1]
    stor.defSuite.ecrireR = stor.defSuite.r // pour écrire la raison notamment quand elle est fractionnaire
    // écriture du terme u_{n+1}
    if (stor.defSuite.typeSuite === arithmetique) {
      stor.defSuite.Un1 = stor.defSuite.nomU + '_n' + j3pGetLatexMonome(2, 0, stor.defSuite.r, stor.defSuite.nomU + '_n')
      stor.UnBis1 = stor.defSuite.Un1
    } else {
      if (stor.GeoRaisonFrac) {
        stor.defSuite.Un1 = '\\frac{' + j3pGetLatexMonome(1, 1, stor.NumDenFraction[me.questionCourante - 1][0], stor.defSuite.nomU + '_n') + '}{' + stor.NumDenFraction[me.questionCourante - 1][1] + '}'
        stor.defSuite.r = stor.NumDenFraction[me.questionCourante - 1][0] / stor.NumDenFraction[me.questionCourante - 1][1]
        stor.defSuite.ecrireR = '\\frac{' + stor.NumDenFraction[me.questionCourante - 1][0] + '}{' + stor.NumDenFraction[me.questionCourante - 1][1] + '}'
        stor.UnBis1 = stor.defSuite.ecrireR + '\\times ' + stor.defSuite.nomU + '_n'
      } else {
        stor.defSuite.Un1 = j3pGetLatexMonome(1, 1, stor.defSuite.r, stor.defSuite.nomU + '_n')
        stor.UnBis1 = stor.defSuite.Un1
      }
    }
    j3pAffiche(stor.zoneCons1, '', textes.consigne1,
      {
        u: stor.defSuite.nomU,
        n: stor.defSuite.termeInit[me.questionCourante - 1][0],
        i: stor.defSuite.termeInit[me.questionCourante - 1][1],
        j: stor.defSuite.Un1
      })
    j3pAffiche(stor.zoneCons2, '', textes.consigne2, { u: stor.defSuite.nomU })
    stor.elt1 = j3pAffiche(stor.zoneCons3, '', textes.consigne3,
      {
        u: stor.defSuite.nomU,
        input1: { texte: '', dynamique: true, width: '12px' }
      })
    stor.dernierePhraseVisible = false
    // un listener pour remplacer une orthographe approchante par la réponse attendue
    stor.elt1.inputList[0].addEventListener('input', function () {
      if (isGeoVariant(this.value)) {
        this.value = geometrique
        finQuestion(geometrique)
      } else if (isArithVariant(this.value)) {
        this.value = arithmetique
        finQuestion(arithmetique)
      } else {
        j3pEmpty(stor.zoneCons4)
        stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.elt1.inputList[0]] })
        stor.dernierePhraseVisible = false
      }
    })
    // on exporte les variables présentes dans cette section pour qu’elles soient utilisées dans natureSuite
    // me.donneesPersistantes.suites sert à envoyer des informations dans le bilan qui pourront être récupérées par une autre section sur les suites
    // Ceci a pour but de poursuivre l’étude de la suite définie dans cette section
    me.donneesPersistantes.suites = {}
    for (const prop in stor.defSuite) {
      me.donneesPersistantes.suites[prop] = stor.defSuite[prop]
    }

    // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
    // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche
    j3pRestriction(stor.elt1.inputList[0], 'a-zéèçà\\-êîâù')// on n’autorise que les lettres minuscules
    stor.elt1.inputList[0].typeReponse = ['texte']
    if (stor.defSuite.typeSuite === arithmetique) {
      stor.elt1.inputList[0].reponse = arithmetique
    } else {
      stor.elt1.inputList[0].reponse = geometrique
    }
    j3pFocus(stor.elt1.inputList[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mes_zones_saisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    stor.mesZonesSaisie = [stor.elt1.inputList[0]]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: stor.mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        // lancement de la section => construction de la page
        initSection()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      let reponse = { aRepondu: false, bonneReponse: false }
      reponse.aRepondu = fctsValid.valideReponses()
      let reponseAvecIndice = true // pour vérifier si l’élève a donné le rang de la suite
      if (reponse.aRepondu && stor.dernierePhraseVisible) {
        // il faut quand même que je vérifie si la zone avec v_... est complétée
        reponseAvecIndice = fctsValid.zones.aRepondu[1]
      }
      let isArith, isGeo, isNatureSuiteOk
      if (reponse.aRepondu) {
        reponse.bonneReponse = fctsValid.valideUneZone(stor.mesZonesSaisie[0], stor.mesZonesSaisie[0].reponse).bonneReponse
        if (reponse.bonneReponse) {
          reponse = stor.fctsValid.validationGlobale()
        } else {
          stor.fctsValid.zones.bonneReponse[0] = false
          stor.fctsValid.coloreUneZone(stor.mesZonesSaisie[0])
        }
        let repSansAccent = stor.elt1.inputList[0].value
        while (repSansAccent.indexOf('é') > -1) {
          repSansAccent = repSansAccent.replace('é', 'e')
        }
        isArith = (stor.elt1.inputList[0].value === arithmetique)
        isGeo = (stor.elt1.inputList[0].value === geometrique)
        isNatureSuiteOk = (isArith || isGeo)
        if (!fctsValid.zones.bonneReponse[0] && isNatureSuiteOk) {
          for (let i = 3; i >= 1; i--) {
            fctsValid.zones.bonneReponse[i] = false
            fctsValid.coloreUneZone(stor.mesZonesSaisie[i])
          }
        }
      }
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        let msgReponseManquante
        if (!reponseAvecIndice) {
          msgReponseManquante = textes.comment5
          j3pFocus(stor.mesZonesSaisie[2])
        }
        me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        return me.finCorrection()
      }

      // Une réponse a été saisie
      // Bonne réponse
      if (reponse.bonneReponse) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        afficheCorr(true)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        j3pDesactive(stor.elt1.inputList[0])
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorr(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      stor.zoneCorr.innerHTML = cFaux
      if (!isNatureSuiteOk) {
        const valeurRaison = j3pCalculValeur(stor.defSuite.r)
        const valeurTermeInit = j3pCalculValeur(stor.defSuite.termeInit[me.questionCourante - 1][1])
        // Ici on affine le commentaire en vérifiant si l’élève n’a pas donné une réponse qui aurait du sens mais qui ne décrirait pas la nature de la suite
        if (textes.croissante.split('|').indexOf(fctsValid.zones.reponseSaisie[0]) > -1) {
          // on regarde si la suite est bien croissante
          if (stor.defSuite.typeSuite === arithmetique) {
            stor.zoneCorr.innerHTML = (valeurRaison > 0) ? textes.comment1_2 : textes.comment1_3
          } else {
            stor.zoneCorr.innerHTML = (((valeurRaison > 1) && (valeurTermeInit > 0)) || ((valeurRaison > 0) && (valeurRaison < 1) && (valeurTermeInit < 0))) ? textes.comment1_2 : textes.comment1_3
          }
        } else if (textes.decroissante.split('|').indexOf(fctsValid.zones.reponseSaisie[0]) > -1) {
          // on regarde si la suite est bien décroissante
          if (stor.defSuite.typeSuite === arithmetique) {
            stor.zoneCorr.innerHTML = (valeurRaison < 0) ? textes.comment1_2 : textes.comment1_3
          } else {
            stor.zoneCorr.innerHTML = (((valeurRaison > 1) && (valeurTermeInit < 0)) || ((valeurRaison > 0) && (valeurRaison < 1) && (valeurTermeInit > 0))) ? textes.comment1_2 : textes.comment1_3
          }
        } else if (textes.convergente.split('|').indexOf(fctsValid.zones.reponseSaisie[0]) > -1) {
          // on regarde si la suite est bien convergente
          if (stor.defSuite.typeSuite === arithmetique) {
            stor.zoneCorr.innerHTML = textes.comment1_3
          } else {
            stor.zoneCorr.innerHTML = ((valeurRaison > -1) && (valeurRaison < 1)) ? textes.comment1_2 : textes.comment1_3
          }
        } else if (textes.divergente.split('|').indexOf(fctsValid.zones.reponseSaisie[0]) > -1) {
          // on regarde si la suite est bien divergente
          if (stor.defSuite.typeSuite === arithmetique) {
            stor.zoneCorr.innerHTML = textes.comment1_2
          } else {
            stor.zoneCorr.innerHTML = ((valeurRaison < 0) || (valeurRaison > 1)) ? textes.comment1_2 : textes.comment1_3
          }
        } else if (textes.positive.split('|').indexOf(fctsValid.zones.reponseSaisie[0]) > -1) {
          // on regarde si la suite est bien positive
          if (stor.defSuite.typeSuite === arithmetique) {
            stor.zoneCorr.innerHTML = ((valeurRaison > 0) && (valeurTermeInit > 0)) ? textes.comment1_2 : textes.comment1_3
          } else {
            stor.zoneCorr.innerHTML = ((valeurRaison > 0) || (valeurTermeInit > 0)) ? textes.comment1_2 : textes.comment1_3
          }
        } else if (textes.negative.split('|').indexOf(fctsValid.zones.reponseSaisie[0]) > -1) {
          // on regarde si la suite est bien negative
          if (stor.defSuite.typeSuite === arithmetique) {
            stor.zoneCorr.innerHTML = ((valeurRaison < 0) && (valeurTermeInit < 0)) ? textes.comment1_2 : textes.comment1_3
          } else {
            stor.zoneCorr.innerHTML = ((valeurRaison > 0) || (valeurTermeInit < 0)) ? textes.comment1_2 : textes.comment1_3
          }
        } else if (textes.majoree.split('|').indexOf(fctsValid.zones.reponseSaisie[0]) > -1) {
          // on regarde si la suite est bien majorée
          if (stor.defSuite.typeSuite === arithmetique) {
            stor.zoneCorr.innerHTML = (valeurRaison < 0) ? textes.comment1_2 : textes.comment1_3
          } else {
            stor.zoneCorr.innerHTML = (((valeurRaison > -1) && (valeurRaison < 1)) || (((valeurRaison > 1) && (valeurTermeInit < 0)) || ((valeurRaison > 0) && (valeurRaison < 1) && (valeurTermeInit > 0)))) ? textes.comment1_2 : textes.comment1_3
          }
        } else if (textes.minoree.split('|').indexOf(fctsValid.zones.reponseSaisie[0]) > -1) {
          // on regarde si la suite est bien minorée
          if (stor.defSuite.typeSuite === arithmetique) {
            stor.zoneCorr.innerHTML = (valeurRaison > 0) ? textes.comment1_2 : textes.comment1_3
          } else {
            stor.zoneCorr.innerHTML = (((valeurRaison > -1) && (valeurRaison < 1)) || (((valeurRaison > 1) && (valeurTermeInit > 0)) || ((valeurRaison > 0) && (valeurRaison < 1) && (valeurTermeInit < 0)))) ? textes.comment1_2 : textes.comment1_3
          }
        } else if (textes.alternee.split('|').indexOf(fctsValid.zones.reponseSaisie[0]) > -1) {
          // on regarde si la suite est bien alternée
          if (stor.defSuite.typeSuite === arithmetique) {
            stor.zoneCorr.innerHTML = textes.comment1_3
          } else {
            stor.zoneCorr.innerHTML = (valeurRaison < 0) ? textes.comment1_2 : textes.comment1_3
          }
        } else if (textes.recurrente.split('|').indexOf(fctsValid.zones.reponseSaisie[0]) > -1) {
          // on regarde si la suite est bien recurrente
          stor.zoneCorr.innerHTML = textes.comment1_2
        } else if (textes.explicite.split('|').indexOf(fctsValid.zones.reponseSaisie[0]) > -1) {
          // on regarde si la suite est bien recurrente
          stor.zoneCorr.innerHTML = textes.comment1_3
        } else {
          stor.zoneCorr.innerHTML += ' ' + textes.comment1
        }
      } else if (isArith && (stor.defSuite.typeSuite === geometrique)) {
        stor.zoneCorr.innerHTML += ' ' + textes.comment2
      } else if (isGeo && (stor.defSuite.typeSuite === arithmetique)) {
        stor.zoneCorr.innerHTML += ' ' + textes.comment3
      }

      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // il reste un essai, on reste dans l’état correction
        return me.finCorrection()
      }

      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorr(false)
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
