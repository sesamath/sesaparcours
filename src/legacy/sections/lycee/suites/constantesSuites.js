// pour utiliser des variables plutôt que des strings littérales dans le code
export const aleatoire = 'aléatoire'
export const arithGeo = 'arithmético-géométrique'
export const arithmetique = 'arithmétique'
export const geometrique = 'géométrique'
export const typesPossibles = [aleatoire, arithmetique, geometrique]
export const typesPossiblesAg = [aleatoire, arithmetique, geometrique, arithGeo]

// les variantes acceptées
export const isAleaVariant = (type) => /al[eé]atoire/.test(type)
export const isArithGeoVariant = (type) => /arith?m[eé]tico[- ]?g[eé]om[eé]trique/.test(type)
export const isArithVariant = (type) => /arith?m[eé]tique/.test(type)
export const isGeoVariant = (type) => /g[eé]om[eé]trique/.test(type)

/**
 * Retourne la valeur canonique de typeSuite (toujours minuscule avec accent)
 * @param {string} type
 * @param {Object} options
 * @param {boolean} [options.lax=false] passer true pour retourner la valeur passée plutôt que de planter quand type ne correspond à rien de connu
 * @return {string}
 * @throws {Error} si type ne ressemble à rien de connu (et lax est false)
 */
export function normalizeTypeSuite (type, { lax = false } = {}) {
  const typeLc = type.toLowerCase()
  if (isAleaVariant(typeLc)) return aleatoire
  if (isArithVariant(typeLc)) return arithmetique
  if (isGeoVariant(typeLc)) return geometrique
  if (lax) return type
  throw Error(`type de suite ${type} inconnu`)
}

export function upgradeParametreDonneesPrecedentes (parametres) {
  if (typeof parametres.donneesPrecedentes === 'string') parametres.donneesPrecedentes = Boolean(parametres.donneesPrecedentes)
}

/**
 * Met à jour les paramètres du graphe pour typeSuite, pour régler les pbs d’accents dans ce params (ça a varié au cours du temps)
 * @param parametres
 */
export function upgradeParametres (parametres) {
  upgradeParametreDonneesPrecedentes(parametres)
  if (!parametres.typeSuite) {
    // on est appelé avec les params du graphe, qui ne contiennent pas forcément tous les params
    return
  }
  parametres.typeSuite = normalizeTypeSuite(parametres.typeSuite)
  if (!typesPossibles.includes(parametres.typeSuite)) {
    console.error(`Le paramètre typeSuite ${parametres.typeSuite} est invalide (doit être ${typesPossibles.join('|')}) => ${typesPossibles[0]} imposé`)
    parametres.typeSuite = typesPossibles[0]
  }
}

// idem avec arithGeo inclus
export function normalizeTypeSuiteAg (type, options) {
  if (isArithGeoVariant(type.toLowerCase())) return arithGeo
  return normalizeTypeSuite(type, options)
}

export function upgradeParametresAg (parametres) {
  upgradeParametreDonneesPrecedentes(parametres)
  if (!parametres.typeSuite) {
    // on est appelé avec les params du graphe, qui ne contiennent pas forcément tous les params
    return
  }
  const type = normalizeTypeSuiteAg(parametres.typeSuite)
  if (type === arithGeo) {
    parametres.typeSuite = arithGeo
  } else {
    upgradeParametres(parametres)
  }
}
