import { j3pAddElt, j3pBarre, j3pEmpty, j3pMathquillXcas, j3pPaletteMathquill, j3pRestriction, j3pValeurde, j3pFocus, j3pStyle } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { fraction, sommeMonomes, creeTableauAleatoire, entierAlea, estEntier } from 'src/legacy/outils/espace/fctsEspace'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
/*
    Auteur : Laurent Fréguin
    Date : Juillet 2018
    Remarques : J’en ai chié ;-)
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    // ["limite","","entier","Temps disponible par répétition, en secondes"],
    ['nb_chances_init', 1, 'entier', 'Nombre d’essais pour l’initialisation'],
    ['nb_chances_formule_heredite', 2, 'entier', 'Nombre d’essais pour déterminer la formule au rang n+1'],
    ['nb_erreurs_heredite', 3, 'entier', 'Nombre d’erreurs acceptées pour démontrer l’hérédité'],
    ['nb_etapes_heredite', 6, 'entier', 'Nombre d’étapes disponibles pour prouver l’hérédité, mini 2 et maxi 9'],
    ['probas_modele', [0.33, 0.33, 0.34], 'array', 'probabilité que sorte le modèle 1,2 ou 3'],
    ['probas_rang_depart', [0.33, 0.33, 0.34], 'array', 'probabilité que sorte rang de départ soit 0,1 ou 2'],
    ['redaction', '', 'string', 'Choix de la rédaction : valeur=vide ou bis pour sans ou avec référence à Pn']
  ]
}

const textes = {
  titreExo: 'Raisonnement par récurrence',
  phrase_1: 'C’est bien!',
  phrase_2: 'C’est faux!<br>Essaie encore',
  phrase_3: 'C’est faux!<br>Regarde la correction',
  phrase_i1: 'Opération non autorisée.<br>Recommence!',
  phrase_i2: 'On ajoute $£n$ à chaque membre',
  phrase_i3: 'On multiplie chaque membre par $£n$',
  phrase_i4: 'Expression exacte.',
  phrase_i5: 'Expression fausse',
  phrase_e1: 'Soit $(u_n)$ la suite définie pour tout entier $£a$ par $£b$ avec $£c$. <br>On veut démontrer par récurrence que $£d$ pour tout entier $£a$.<br> ● Initialisation : Elle démarre au rang &1&.<br>',
  phrase_e1bis: 'Soit $(u_n)$ la suite définie pour tout entier $£a$ par $£b$ avec $£c$. <br>On veut démontrer par récurrence que $£d$ pour tout entier $£a$.<br>Pour tout entier $£a$, notons $P_n$ la propriété $£d$.<br>● Initialisation : Elle démarre au rang &1&.<br>',
  phrase_c1: '<u>Correction:</u><br>On demande de démontrer cette égalité pour tout entier $£a$.<br> Le plus petit de ces entiers est £e.<br>L’initialisation part donc du rang £e.<br> Au rang $£e$, on a $£f$ et $£H$<br>',
  phrase_c1bis: '<u>Correction:</u><br>On demande de démontrer cette égalité pour tout entier $£a$.<br> Le plus petit de ces entiers est £e.<br>L’initialisation part donc du rang £e.<br>$P_{£e}$ est vérifiée puisque $£f$ et $£H$<br>',
  phrase_e2: 'Supposons qu’il existe un entier $£a$ tel que $£d$.<br>Il s’agit de prouver que $£m$ &1&<br>(Ecris ^n pour exposant n ou utilise le bouton ci-dessous)',
  phrase_e2bis: 'Supposons qu’il existe un entier $£a$ tel que $P_{n}$ soit vraie, c’est à dire que $£d$.<br>Il s’agit de prouver que $P_{n+1}$ est vraie donc que $£m$ &1&<br>(Ecris ^n pour exposant n)',
  phrase_c2: '<u>Correction:</u><br>L’hérédité consiste à démontrer que s’il existe un entier $£a$ tel que $£d$, alors la formule sera valable au rang $n+1$<br>On doit donc prouver que $£g$.<br>',
  phrase_c2bis: '<u>Correction:</u><br>L’hérédité consiste à démontrer que s’il existe un entier $£a$ tel que $P_{n}$ est vraie, alors $P_{n+1}$ sera vraie aussi.<br>On doit donc prouver que $£g$.<br>',
  phrase_c3: 'Or $£h$.<br>Il s’agit donc prouver que  $£i$',

  phrase_e3: 'On rappelle que $£b$.<br>Et qu’on souhaite démontrer que $£i$.<br>Agis <strong>pas à pas </strong>sur chacun des membres l’égalité donnée par l’hypothèse de récurrence&nbsp;:<br> $£d$.<br>Tu as le droit à £j étapes maximum pour atteindre l’objectif.<br>Remarque : tu peux écrire +0 si tu souhaites une ligne de plus pour réduire tes calculs.<br>',
  phrase_o: 'Objectif :<br>$£i$',
  phrase_j1: 'Opération à effectuer sur chaque membre : #1# @1@<br>',
  phrase_j2: 'Opération à effectuer sur chaque membre : #2# @2@<br>',
  phrase_j3: 'Opération à effectuer sur chaque membre : #3# @3@<br>',
  phrase_j4: 'Opération à effectuer sur chaque membre : #4# @4@<br>',
  phrase_j5: 'Opération à effectuer sur chaque membre : #5# @5@<br>',
  phrase_j6: 'Opération à effectuer sur chaque membre : #6# @6@<br>',
  phrase_j7: 'Opération à effectuer sur chaque membre : #7# @7@<br>',
  phrase_j8: 'Opération à effectuer sur chaque membre : #8# @8@<br>',
  phrase_j9: 'Opération à effectuer sur chaque membre : #9# @9@<br>',
  phrase_h1: '$£k$=&1&<br>',
  phrase_h2: '$£k$=&2&<br>',
  phrase_h3: '$£k$=&3&<br>',
  phrase_h4: '$£k$=&4&<br>',
  phrase_h5: '$£k$=&5&<br>',
  phrase_h6: '$£k$=&6&<br>',
  phrase_h7: '$£k$=&7&<br>',
  phrase_h8: '$£k$=&8&<br>',
  phrase_h9: '$£k$=&9&<br>',
  phrase_k: '$£k$<br>',
  phrase_s: 'L’expression est correcte, mais à simplifier davantage.',
  phrase_heredite_simpl: 'L’expression est correcte, mais quel est l’objectif?',
  phrase_echoue: 'Tu n’as pas réussi&nbsp;!<br>Regarde la correction.',
  phrase_nbEtapes: 'Tu n’as pas réussi en £j étapes&nbsp;!<br/>Regarde la correction&nbsp;',
  phrase_c4: '<u>Correction:</u><br>En partant de l’égalité donnée par l’hypothèse de récurrence :<br>$£d$<br>',
  phrase_c5: 'On multiplie chaque membre par $£A$:<br>$£D$ car $£G$<br>',
  phrase_c6: 'On ajoute $£B$ à chaque membre :<br>$£E$<br>',
  phrase_c7: 'Puis on ajoute $£C$ à chaque membre :<br>$£F$<br>Soit $£i$<br>',
  phrase_c8: '<strong>Par récurrence</strong>, on conclut que $£d$ pour tout entier $£a$',
  phrase_ind1: 'Pour calculer $u_{n+1}$ lorsqu’on connait $u_n$, on commence prioritairement par une multiplication...',
  phrase_ind2: 'Pour calculer $u_{n+1}$ lorsqu’on connait $u_n$, on peut ajouter une certaine quantité dépendant de n...',
  nbchances1: 'Tu peux encore te permettre £n erreurs&nbsp;!',
  nbchances2: 'Tu peux encore te permettre 1 erreur&nbsp;!',
  aide: 'Aide & outils'
}

/**
 * section recur
 * @this {Parcours}
 */
export default function main () {
  /**
   * Raccourci vers l’instance courante de parcours (le this de la fct principale)
   * @type {Parcours}
   */
  const me = this
  /**
   * raccourci vers me.storage, pour stocker tout ce qu’on veut entre les ≠ appels de la section
   * @type {Object}
   */
  const stor = me.storage
  const ds = me.donneesSection
  function arrondi (x, n) {
    return Math.round(x * Math.pow(10, n)) / Math.pow(10, n)
  }

  function egal (a, b) {
    // Renvoie true si la différence entre a et b est inférieure ou égale à 10^-9
    return (Math.abs(a - b) < Math.pow(10, -9))
  }

  function tabEgaux (tab1, tab2) {
    // renvoie true si tab1 et tab2 sont identiques
    if (tab1.length !== tab2.length) {
      return false
    } else {
      for (let i = 0; i < tab1.length; i++) {
        if (tab1[i] !== tab2[i]) {
          return false
        }
      }
    }
    return true
  }
  function evaluer (strExpres, tabVars, tabVals) {
    // Cette fonction renvoie la valeur obtenue lorsqu’on remplace dans strExpres les variables de tabVars par leurs valeurs de tabVals

    const expr = new Tarbre(strExpres, tabVars)
    return expr.evalue(tabVals)
  }

  function estAffine (strExpres, tabVar) {
    // le tableau des variables est de taille 1 ou 2.
    // Cette fonction renvoie un objet dont les pptés sont les suivantes :
    // .affine=true si l’expression est affine par rapport aux variables du tableau tabVar'
    // false sinon
    // .coeff = un tableau de coefficient devant chaque variable
    // En pratique, on part du principe que f(x,y)=a+b*x+c*y puis on retrouve a,b,et c.
    // Puis on compare strExpres à f(x,y)
    // On va commencer par détecter s’il existe une chaine du genre s12,t2 etc, car la fonction evelue evalue mal ces chaines'

    const stock = {}
    const tabChiffres = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

    let sousChaine1 = ''
    let sousChaine2 = ''
    let chaineBizarre = 0

    for (let k = 0; k < 10; k++) {
      // sousChaine1="";
      // sousChaine2=""
      sousChaine1 = tabVar[0] + tabChiffres[k]
      sousChaine2 = tabVar[1] + tabChiffres[k]
      if (strExpres.indexOf(sousChaine1) !== -1 || strExpres.indexOf(sousChaine2) !== -1) {
        chaineBizarre = chaineBizarre + 1
        k = 10
      }
    }

    if (chaineBizarre === 0) {
      // Exctraction des a,b et c potentiels
      const arbre1 = new Tarbre(strExpres, tabVar)

      let a = arbre1.evalue([0, 0])
      let b = arbre1.evalue([1, 0]) - a
      let c = arbre1.evalue([0, 1]) - a
      c = arrondi(c, 12)
      a = arrondi(a, 12)
      b = arrondi(b, 12)
      let difference = 0
      let affine = true
      // Comparaison à Str_expression sur un double tableau
      for (let i = 2; i < 10; i++) {
        for (let j = 2; j < 10; j++) {
          difference = Math.abs(arbre1.evalue([i, j]) - (a + b * i + c * j))

          if (difference > Math.pow(10, -9)) {
            affine = false
            i = 10
            j = 10
          }
        }
      }
      if (affine) {
        stock.affine = true
        stock.coeff = [a, b, c]
        stock.coeff_frac = [fraction(a).ch, fraction(b).ch, fraction(c).ch]
      } else {
        stock.affine = false
        // stock.coeff=[];
        // stock.coeff_frac=[]
      }
    } else {
      stock.affine = false
    }

    return stock
  }

  function egaliteLitterale (strExpres1, strExpres2, tabVars, tabToTest) {
    // Compare les strExpres1 et strExpres2 pour les valeurs tabVars
    // en remplaçant chacune d’elles par tabToTest
    const expr1 = new Tarbre(strExpres1, tabVars)
    const expr2 = new Tarbre(strExpres2, tabVars)
    for (let i = 0; i < tabToTest.length; i++) {
      const e1 = expr1.evalue(tabToTest[i])
      const e2 = expr2.evalue(tabToTest[i])
      if (!egal(e1, e2)) return false
    }
    return true
  }

  function substituer (listCoeffs, intVal) {
    // Cette fonction remplace x par str_valeur dans le polynome défini par liste des coeffs.
    // Le plus haut degré est d’indice 0 dans listCoeffs
    const lstVar = []
    let x = intVal.toString()
    const n = listCoeffs.length
    let a = ''
    if (intVal < 0) {
      x = '(' + x + ')'
    }
    for (let k = 0; k < n - 2; k++) {
      a = x + '^{' + (n - k - 1).toString() + '}'
      if (Math.abs(listCoeffs[k]) !== 1) {
        a = '\\times ' + a
      }
      lstVar.push(a)
    }
    // terme de degré 1
    a = x
    if (Math.abs(listCoeffs[n - 2]) !== 1) {
      a = '\\times ' + a
    }
    lstVar.push(a)
    // terme de degré 0
    lstVar.push('')

    return sommeMonomes(listCoeffs, lstVar)
  }

  function retire (monTab, index) {
    const stock = monTab.slice(0)
    stock.splice(index, 1)
    return stock
  }

  function ecrituresAccept (tabCoeffs, tabFamilles) {
    // Cette fonction donne la liste de toutes les permutations possibles somme des coeff * famille
    // ex : ecrituresAccept([1,2,3],["a","b","c"]) renvoie ["a+2b+3c","a+3c+2b","2b+a+3c",etc...]
    let stock = []
    const n = tabFamilles.length
    if (n === 2) {
      const a = sommeMonomes(tabCoeffs, tabFamilles)
      const b = sommeMonomes(tabCoeffs.reverse(), tabFamilles.reverse())
      stock = [j3pMathquillXcas(a), j3pMathquillXcas(b)]
    } else {
      let tabCopieCoeffs = []
      let tabCopieFamilles = []
      for (let k = 0; k < tabFamilles.length; k++) {
        tabCopieCoeffs = retire(tabCoeffs, k)
        tabCopieFamilles = retire(tabFamilles, k)
        const tabPossibles = ecrituresAccept(tabCopieCoeffs, tabCopieFamilles)
        for (let m = 0; m < tabPossibles.length; m++) {
          let c = sommeMonomes([1, tabCoeffs[k]], [tabPossibles[m], tabFamilles[k]])
          c = j3pMathquillXcas(c)
          stock.push(c)
        }
      }
    }

    return stock
  }

  function arithmetico (intA, intQ, strExpo, intB, strN, intC) {
    // Ecrit correctement a*q^exposant+b*n+c, où exposant est une chaine avec q non nul
    let stock

    if (intQ < 0) {
      stock = '(' + intQ.toString() + ')^{' + strExpo + '}'
    } else {
      stock = intQ.toString() + '^{' + strExpo + '}'
    }
    if (intA === 0) {
      stock = ''
    } else {
      if (intA === -1) {
        // stock="‒"+stock
        stock = '-' + stock
      } else {
        if (intA !== 1) {
          stock = fraction(intA).mathq + '\\times ' + stock
        }
      }
    }

    stock = sommeMonomes([1, intB, intC], [stock, strN, ''])
    return stock
  }

  function numEtape (itemActuel, nbTotaleEtapes) {
    // Cette fonction
    const n = itemActuel % nbTotaleEtapes
    if (n === 0) return nbTotaleEtapes
    return n
  }

  function newFormule () {
    const tabFct = stor.fonction
    const tabCoeffs = stor.exercice.coefficients
    // Cette fonction applique la fonction f(x,n)=ax+bn+c contenue dans donc [a,b,c] aux coefficients de la famille
    let stock = [tabFct[0] * tabCoeffs[0], tabFct[0] * tabCoeffs[1], tabFct[0] * tabCoeffs[2]]
    stock[1] += tabFct[1]
    stock[2] += tabFct[2]
    let stringToEvaluate
    if (stor.exercice.modele === 3) {
      stringToEvaluate = sommeMonomes(stock, ['n^2', 'n', ''])
    } else {
      const q = stor.exercice.raison
      const p = stock[0] / q
      if (!estEntier(p)) {
        stringToEvaluate = arithmetico(stock[0], stor.exercice.raison, stor.exercice.exposant, stock[1], 'n', stock[2])
      } else {
        stringToEvaluate = arithmetico(p, stor.exercice.raison, stor.exercice.nextRang, stock[1], 'n', stock[2])
      }
    }
    stock = {}
    stock.mathq = stringToEvaluate
    stringToEvaluate = j3pMathquillXcas(stringToEvaluate)
    stock.ch = stringToEvaluate
    return stock
  }

  function opeValide (strOpe, strChaine, tabFctActuelle) {
    // str_op=+,- ou *
    // strChaine = cte ou fonction affine de n, sinon, valide=false
    // tabFctActuelle : l’état actuelle de l’hérédité après actions pas à pas
    // renvoie un objet de pptés :
    // .valide : true si l’opération est acceptée, false sinon
    // .tab:nouvel etat fonctionnel ie tableau des coeffs associée à l’action affine f(x,n) associées
    // f(x,n)=2x+n-3 se traduit par : [2,1,-3] donc [coeff_x,coeff_n,cte]
    // .phrase pour retourer l’action choisie sous forme de phrase
    const stock = {}
    // On commence par vérifier que strChaine est une fonction affine de n
    const f = estAffine(strChaine, ['n'])
    stock.valide = f.affine

    if (stock.valide) {
      const a = f.coeff
      if (strOpe === '×') {
        // Il y a présence de n donc chaine non constante, on refuse la multiplication
        if (a[1] !== 0 || a[0] === 0) {
          stock.valide = false
        } else {
          // la chaine est constante, on évalue la chaine
          const k = evaluer(strChaine, ['t'], 1)
          stock.tab = [k * tabFctActuelle[0], k * tabFctActuelle[1], k * tabFctActuelle[2]]
          stock.action = f.coeff_frac[0]
          stock.action = fraction(j3pCalculValeur(f.coeff_frac[0])).mathq
        }
      } else {
        // On a une addition ou une soustraction
        stock.tab = [tabFctActuelle[0], tabFctActuelle[1] + a[1], tabFctActuelle[2] + a[0]]
        stock.action = sommeMonomes([f.coeff[1], f.coeff[0]], ['n', ''])
      }
    }
    return stock
  }

  function affichage () {
    const objExo = stor.exercice
    const div = j3pAddElt(stor.conteneur, 'div')

    switch (objExo.numero_question) {
      case 1:
        stor.elt1 = phrase(div, 'phrase_e1', 'black', objExo)
        mqRestriction(stor.elt1.inputmqList[0], '\\d')
        j3pFocus(stor.elt1.inputmqList[0])
        break

      case 2:
        stor.elt2 = phrase(div, 'phrase_e2', 'black', objExo)
        stor.divPalette = j3pAddElt(stor.conteneur, 'div')
        j3pStyle(stor.divPalette, { paddingTop: '10px' })
        j3pPaletteMathquill(stor.divPalette, stor.elt2.inputmqList[0], { liste: ['puissance'] })
        mqRestriction(stor.elt2.inputmqList[0], '\\d,.n^/+*()-', { commandes: ['puissance'] })
        j3pFocus(stor.elt2.inputmqList[0])
        break

      case 3:
        phrase(div, 'phrase_e3', 'black', objExo)
        {
          const div2 = j3pAddElt(stor.conteneur, 'div')
          stor.elt3 = phrase(div2, 'phrase_j1', 'black', objExo)
          j3pRestriction(stor.elt3.inputList[0], '0-9n+/-')
          j3pFocus(stor.elt3.inputList[0])
        }
        stor.nbErreurs = 0
        break
    }
  }

  function correction () {
    const objExo = stor.exercice
    let zonesSaisie = []
    let objValid = {}
    let bilan = {}
    let laZone = ''
    let num
    // Définition des phrases de correction et d’indication par défaut.
    let phraseNewChance = ['phrase_2']
    let colorNewChance = me.styles.cfaux
    let phraseRegarde = ['phrase_3']
    const colorRegarde = me.styles.cfaux
    let phraseCorr = []
    function cfaux () {
      // remets les bonnes phrases et les bonnes couleurs
      // Ce sont des variables globales à la question
      colorNewChance = me.styles.cfaux
      phraseNewChance = ['phrase_2']
    }
    let phraseCbien = ['phrase_1']
    const colorCbien = me.styles.cbien
    let repSimple
    let contenu = 0
    let bonneReponse
    let bonneRep
    let contenuMq
    switch (objExo.numero_question) {
      case 1:
        stor.elt1.inputmqList[0].typeReponse = ['nombre', 'exact']
        stor.elt1.inputmqList[0].reponse = stor.exercice.e
        zonesSaisie = [stor.elt1.inputmqList[0].id]
        objValid = new ValidationZones({ parcours: me, zones: zonesSaisie })
        bilan = objValid.validationGlobale()

        if (!bilan.bonneReponse && bilan.aRepondu) {
          // reste à voir si on est dans autre_chance ou pas
          // on augmente le nombre d’essais de 1
          stor.exercice.numEssai[0] += 1
          cfaux()
        }
        phraseCorr = ['phrase_c1']
        break

      case 2:
        stor.elt2.inputmqList[0].typeReponse = ['nombre']
        // On met une réponse au hasard, puisqu’on fera une validation perso
        stor.elt2.inputmqList[0].reponse = 100000
        zonesSaisie = [stor.elt2.inputmqList[0].id]
        objValid = new ValidationZones({ parcours: me, zones: zonesSaisie, validePerso: zonesSaisie })
        bilan = objValid.validationGlobale()

        contenu = j3pValeurde(stor.elt2.inputmqList[0])
        contenuMq = contenu
        contenu = j3pMathquillXcas(contenu)
        bonneReponse = objExo.formNextRang
        repSimple = objExo.tabRepSimplifiee.includes(contenu)
        {
          const exact = egaliteLitterale(bonneReponse, contenu, ['n'], [[2], [3]])

          bilan.bonneReponse = exact && repSimple
          if (bilan.aRepondu) {
            if (!bilan.bonneReponse) {
            // On doit voir si la réponse est bien simplifiée
              if (!exact) {
                cfaux()
                objValid.zones.bonneReponse[0] = false
              } else {
              // La réponse n’a pas été simplifiée
                cfaux()
                phraseNewChance = ['phrase_s']
                phraseRegarde = ['phrase_s']
                if (objExo.numEssai[numQuest - 1] < objExo.nb_chances[numQuest - 1] - 1) {
                  j3pEmpty(stor.conteneur)
                  stor.exercice.m = 'u_{n+1}=' + contenuMq + '='
                  phrase(stor.conteneur, 'phrase_e2', me.styles.petit.enonce.color, objExo)
                  objValid.zones.bonneReponse[0] = false
                } else {
                  phraseCbien = ['phrase_s']
                  bilan.bonneReponse = true
                  objValid.zones.bonneReponse[0] = true
                }
              }
              stor.exercice.numEssai[1] += 1
            } else {
              objValid.zones.bonneReponse[0] = true
            }
            objValid.coloreUneZone(stor.elt2.inputmqList[0].id)
          }
          phraseCorr = ['phrase_c2']
          if (objExo.modele > 1) {
            phraseCorr.push('phrase_c3')
          }
        }

        break

      case 3: {
        switch (objExo.modele) {
          case 1:
            phraseCorr = ['phrase_c4', 'phrase_c5', 'phrase_c7', 'phrase_c8']
            break

          case 2:
            phraseCorr = ['phrase_c4', 'phrase_c5', 'phrase_c6', 'phrase_c7', 'phrase_c8']
            break

          case 3:
            phraseCorr = ['phrase_c4', 'phrase_c6', 'phrase_c7', 'phrase_c8']
            break
        }

        // nombres de fautes dans les calculs de l’hérédité pour décaler l’objectif
        stor.nbfh = 0
        stor.etape_heredite += 1
        const f = stor.fonction
        stor.numZone = Math.floor(stor.etape_heredite / 2) + 1
        let op
        let ch
        let test
        // je ne m’en sors pas pour savoir si je suis à choisir l’opération ou à écrire l’expression
        // donc j’ajoute cette variable qui vaut true si on est à choisir l’opération
        stor.choixOpe = true
        switch (stor.etape_heredite % 2) {
          case 1:
            // phase choix d’opération sur chaque membre
            stor.sous_chance = 0
            num = stor.numZone.toString()
            laZone = stor.elt3.inputList[0]
            // On vient de choisir une opération
            laZone.typeReponse = ['texte']
            // On met une réponse au hasard, puisqu’on fera une validation perso
            laZone.reponse = 100000
            zonesSaisie = [laZone.id]
            objValid = new ValidationZones({ parcours: me, zones: zonesSaisie, validePerso: zonesSaisie })
            bilan = objValid.validationGlobale()
            op = j3pValeurde(stor.elt3.selectList[0])
            ch = j3pValeurde(laZone)
            ch = j3pMathquillXcas(ch)
            test = opeValide(op, ch, f)

            // On oriente de toute façon vers autrechance à cette étape
            bilan.bonneReponse = false
            phraseNewChance = ['phrase_i1']

            if (bilan.aRepondu) {
              if (test.valide) {
                j3pDesactive(laZone)
                j3pDesactive(stor.elt3.selectList[0])
                colorNewChance = me.styles.cbien

                stor.exercice.n = test.action
                phraseNewChance = ['phrase_i3']
                if (op === '+') phraseNewChance = ['phrase_i2']

                // on affiche une zone de saisie pour calculer le nouvel état de la relation de récurrence

                stor.fonction = test.tab
                stor.exercice.k = sommeMonomes(test.tab, ['u_n', 'n', ''])
                const couleur = me.styles.petit.enonce.color
                if (tabEgaux(test.tab, objExo.tab_sol)) {
                  // couleur = 'blue'
                  stor.exercice.fin = true
                  stor.exercice.k = 'u_{n+1}=' + stor.exercice.k
                }
                const divNext = j3pAddElt(stor.conteneur, 'div')
                stor.eltBis3 = phrase(divNext, 'phrase_h' + num, couleur, stor.exercice)
                stor.divPalette = j3pAddElt(stor.conteneur, 'div')
                j3pStyle(stor.divPalette, { paddingTop: '10px' })
                j3pPaletteMathquill(stor.divPalette, stor.eltBis3.inputmqList[0], { liste: ['fraction', 'puissance'] })
                mqRestriction(stor.eltBis3.inputmqList[0], '\\d,.,n+-/^*', { commandes: ['fraction', 'puissance'] })
                j3pFocus(stor.eltBis3.inputmqList[0])
              } else {
                phraseNewChance = ['phrase_i1']
                stor.etape_heredite -= 1
              }
            } else {
              stor.etape_heredite -= 1
            }
            break

          case 0:
            stor.choixOpe = false
            // j3pEmpty(d.divPalette)
            // Ici, on propose la zone de saisie pour que l’élève complète le membre de gauche après opération
            num = (stor.numZone - 1).toString()
            laZone = stor.eltBis3.inputmqList[0]
            laZone.typeReponse = ['nombre']
            // On met une réponse au hasard, puisqu’on fera une validation perso
            laZone.reponse = 100000
            zonesSaisie = [laZone.id]
            objValid = new ValidationZones({ parcours: me, zones: zonesSaisie, validePerso: zonesSaisie })
            bilan = objValid.validationGlobale()

            contenu = j3pValeurde(laZone)
            contenuMq = contenu
            contenu = j3pMathquillXcas(contenu)

            // On oriente de toute façon vers autrechance à cette étape sauf s’il a épuisé toutes ses possibilités
            bilan.bonneReponse = false

            phraseNewChance = ['phrase_i4']
            colorNewChance = me.styles.cbien
            bonneRep = newFormule()
            bonneReponse = egaliteLitterale(bonneRep.ch, contenu, ['n'], [[2], [3]])
            repSimple = true
            if (bilan.aRepondu) {
              if (!stor.exercice.fin) {
                if (!bonneReponse) {
                  stor.nbErreurs++
                  if (me.essaiCourant === ds.nb_erreurs_heredite) stor.exercice.numEssai[2] = stor.exercice.nb_chances[2]
                  stor.sous_chance += 1
                  objValid.zones.bonneReponse[0] = false
                  j3pStyle(laZone, { color: me.styles.cfaux })
                  if (stor.sous_chance <= 2) {
                    stor.etape_heredite -= 1
                    phraseNewChance = ['phrase_i5']
                    colorNewChance = me.styles.cfaux
                  } else {
                    // On calcule pour l’élève et on le laisse poursuivre
                    stor.nbfh += 1
                    j3pDesactive(laZone)
                    stor.exercice.numEssai[2] += 1
                    stor.exercice.k += '=' + bonneRep.mathq

                    if (stor.exercice.numEssai[2] < stor.exercice.nb_chances[2]) {
                      const divNext = j3pAddElt(stor.conteneur, 'div')
                      phrase(divNext, 'phrase_k', me.styles.cfaux, stor.exercice)
                      num = stor.numZone.toString()
                      const divNext2 = j3pAddElt(stor.conteneur, 'div')
                      stor.elt3 = phrase(divNext2, 'phrase_j' + num, me.styles.petit.enonce.color, objExo)
                      j3pRestriction(stor.elt3.inputList[0], '0-9n+/-')
                      // j3pFocus(d.elt3.inputList[0])
                      phraseNewChance = ['phrase_i5']
                      colorNewChance = me.styles.cfaux
                    } else {
                      phraseRegarde = ['phrase_echoue']
                    }
                  }
                } else {
                  stor.exercice.numEssai[2] += 1
                  objValid.zones.bonneReponse[0] = true
                  j3pDesactive(laZone)
                  j3pStyle(laZone, { color: me.styles.cbien })
                  // il faut afficher une nouvelle opération
                  if (stor.numZone > ds.nb_etapes_heredite) {
                    phraseRegarde = ['phrase_nbEtapes']
                  } else if (stor.exercice.numEssai[2] < stor.exercice.nb_chances[2]) {
                    num = stor.numZone.toString()
                    const divNext2 = j3pAddElt(stor.conteneur, 'div')
                    stor.elt3 = phrase(divNext2, 'phrase_j' + num, me.styles.petit.enonce.color, objExo)
                    j3pRestriction(stor.elt3.inputList[0], '0-9n+/-')
                    j3pFocus(stor.elt3.inputList[0])
                  } else {
                    phraseRegarde = ['phrase_echoue']
                  }
                }
                laZone.barrer = false
                // objValid.coloreUneZone(laZone.id)
                if (!bonneReponse && (stor.sous_chance > 2 || stor.nbErreurs === ds.nb_erreurs_heredite)) j3pBarre(laZone)
              } else {
                // simplification u_n+1
                stor.sous_chance += 1
                repSimple = objExo.tabRepSimplifiee.includes(contenu)
                if (stor.sous_chance < ds.nb_erreurs_heredite) {
                  bilan.bonneReponse = bonneReponse && repSimple
                  if (!bonneReponse) {
                    stor.nbErreurs++
                    objValid.zones.bonneReponse[0] = false
                    cfaux()
                    phraseRegarde = (stor.numZone > ds.nb_etapes_heredite) ? ['phrase_nbEtapes'] : ['phrase_echoue']
                  } else {
                    // la réponse est bonne. Est-elle simplifiée?
                    if (!repSimple) {
                      stor.nbErreurs++
                      cfaux()
                      phraseNewChance = ['phrase_heredite_simpl']
                      phraseRegarde = ['phrase_heredite_simpl']
                      phraseRegarde = ['phrase_echoue']
                      objValid.zones.bonneReponse[0] = false
                    } else {
                      objValid.zones.bonneReponse[0] = true
                    }
                  }
                  stor.exercice.numEssai[1] += 1
                  j3pStyle(laZone, { color: ((objValid.zones.bonneReponse[0]) ? me.styles.cbien : me.styles.cfaux) })
                  // objValid.coloreUneZone(laZone.id)
                  if ((!bonneReponse || !repSimple) && (stor.nbErreurs === ds.nb_erreurs_heredite)) {
                    j3pDesactive(laZone)
                    j3pBarre(laZone)
                  }
                  stor.etape_heredite -= 1
                } else {
                  // on s’arrête là!
                  if (bonneReponse && repSimple) {
                    objValid.zones.bonneReponse[0] = true
                    bilan.bonneReponse = true
                  } else {
                    stor.nbErreurs++
                    if (bonneReponse && !repSimple) {
                      phraseRegarde = ['phrase_heredite_simpl']
                    } else {
                      phraseRegarde = ['phrase_echoue']
                    }
                    objValid.zones.bonneReponse[0] = false
                    stor.exercice.numEssai[2] = objExo.nb_chances[numQuest - 1] + 1
                  }
                  j3pDesactive(laZone)
                  j3pStyle(laZone, { color: ((objValid.zones.bonneReponse[0]) ? me.styles.cbien : me.styles.cfaux) })
                  // objValid.coloreUneZone(laZone.id)
                  if (!bonneReponse || !repSimple) j3pBarre(laZone)
                  me.essaiCourant = ds.nb_etapes_heredite
                }
              }
            } else {
              // Si pas de réponse
              stor.etape_heredite -= 1
            }

            break
        }
        break
      } // case 3
    }
    if (objExo.numero_question <= 2) {
      bilan.autre_chance = objExo.numEssai[numQuest - 1] < objExo.nb_chances[numQuest - 1]
    } else {
      if (stor.choixOpe || (bonneReponse && repSimple)) {
        // l’exo se poursuit si on est à choisir l’opération ou si on a eu une bonne réponse au calcul de l’expression et que le nombre max d’étapes n’est aps atteint
        bilan.autre_chance = (stor.numZone <= ds.nb_etapes_heredite)
        if (bilan.aRepondu) me.essaiCourant--
        if (!stor.choixOpe) j3pEmpty(stor.divPalette)
      } else {
        bilan.autre_chance = (ds.nb_erreurs_heredite > stor.nbErreurs)
      }
    }
    // On définit les fonctions d’indication et de correction
    bilan.indication = function () {
      lesPhrases(stor.zoneCorr, phraseNewChance, colorNewChance, objExo)
    }

    bilan.regarde = function () {
      lesPhrases(stor.zoneCorr, phraseRegarde, colorRegarde, objExo)
      // phrase("correction","sp1",phraseNewChance[0],couleurCorr,objExo)
    }

    bilan.cbien = function () {
      lesPhrases(stor.zoneCorr, phraseCbien, colorCbien, objExo)
    }

    bilan.correction = function (bonneRep) {
      lesPhrases(stor.zoneExpli, phraseCorr, (bonneRep) ? me.styles.cbien : me.styles.petit.explications.color, objExo)
      // Je vire les boutons de la palette Mathquill quand elle est présente (étape 2)
      if (objExo.numero_question >= 2) j3pEmpty(stor.divPalette)
    }

    return bilan
  }

  function phrase (leConteneur, phraseChoisie, couleur, objExo) {
    // objExo a été généré par genere_exo et contient les infos nécessaire sà l’affichage
    const copie = objExo
    // On peut éventuellement rajouter la couleur dans copie
    copie.styletexte = { couleur }
    let laPhrase = textes[phraseChoisie + objExo.redaction]
    if (laPhrase === undefined) laPhrase = textes[phraseChoisie]
    return j3pAffiche(leConteneur, '', laPhrase, copie)
  }

  function lesPhrases (leConteneur, lstPhrasesChoisies, couleur, objExo) {
    // On fait une boucle sur list phrases choisies en créant des span indexés à partir de 1
    for (let i = 0; i < lstPhrasesChoisies.length; i++) {
      phrase(leConteneur, lstPhrasesChoisies[i], couleur, objExo)
    }
  }

  function genererExo (options) {
    // options est un objet
    // options.indice=indice du premier élément
    // options.signe=signe de q : 1 implique q positif,-1 négatif
    // Soit u_n=alpha+beta*q^(n-k) alors u_(n+1)=q*u_n+alpha*(1-q) et u_k=alpha+beta
    // qu’on écrira u_(n+1)=a*u_n+b
    // On va retourner un objet exercice

    const exercice = options
    // On définit le nombre de chances pour chaque question:
    exercice.nb_chances = [stor.nb_chances_init, stor.nb_chances_formule_heredite, stor.nb_etapes_heredite]
    exercice.numEssai = [0, 0, 0]

    // choix de l’indice de départ
    const k = options.rang_depart
    const strK = k.toString()
    let exposant
    let exposantInit
    if (k === 0) {
      exercice.a = 'n\\in \\N'
      exposant = 'n'
      exercice.exposant = exposant
      exposantInit = '0'
    } else {
      exercice.a = 'n\\geq' + strK
      exposant = 'n' + '-' + strK
      exercice.exposant = 'n' + '-' + strK
      exposantInit = strK + '-' + strK
    }

    const maxAlpha = 10
    const maxBeta = 10
    const maxGamma = 10
    const maxQ = 10

    // Pour la formule explicite
    let alpha = 0
    let beta = 0
    let gamma = 0
    let q = 0
    // Pour celle de récurrence
    let a = 0
    let b = 0
    let c = 0
    let formuleExpl
    let uInit
    let justifRangInit
    let formNextRang
    let formNextRangReduit
    let demNextFormReduite
    let coefficients
    let famille
    let tabRepSimplifiee
    let gamma1
    let gamma2
    let beta1
    let beta2
    let formNextRangInter
    let sgnAlpha
    let absAlpha
    let nextRang
    let fam1
    let fam2
    switch (options.modele) {
      case 3:
        // u_n=alpha*n²+beta*n+gamma
        do {
          alpha = entierAlea(-maxAlpha, maxAlpha, [0])
          beta = entierAlea(-maxBeta, maxBeta, [0])
          gamma = entierAlea(-maxGamma, maxGamma, [0])
          a = 1
          b = 2 * alpha
          c = alpha + beta - k * b
        }
        while (a * b * c === 0)

        uInit = gamma.toString()
        beta1 = beta - 2 * alpha * k
        gamma1 = gamma - beta * k + alpha * k * k
        famille = ['n^2', 'n', '']
        coefficients = [alpha, beta1, gamma1]
        formuleExpl = 'u_n=' + sommeMonomes(coefficients, famille)

        justifRangInit = substituer(coefficients, k)
        beta2 = 2 * alpha + beta1
        gamma2 = alpha + beta1 + gamma1
        formNextRang = sommeMonomes(coefficients, ['(n+1)^2', '(n+1)', ''])
        formNextRangInter = sommeMonomes(coefficients, ['(n^2+2n+1)', '(n+1)', ''])
        formNextRangReduit = sommeMonomes([alpha, beta2, gamma2], famille)
        demNextFormReduite = formNextRang + '=' + formNextRangInter + '=' + formNextRangReduit
        tabRepSimplifiee = ecrituresAccept([alpha, beta2, gamma2], famille)
        // la correction de l’hérédité
        // var test=opeValide("+",b.toString()+"n",[1,0,0])
        exercice.E = sommeMonomes([1, b, 0], ['u_n', 'n', '']) + '=' + sommeMonomes([alpha, beta1 + b, gamma1], famille)
        exercice.F = sommeMonomes([1, b, c], ['u_n', 'n', '']) + '=' + sommeMonomes([alpha, beta1 + b, gamma1 + c], famille)
        exercice.indication = textes.phrase_ind2
        break

      default:

        // u_n=alpha*q^n+beta*n+gamma
        // modele 1: beta=0
        // modele 2 : beta!=0
        q = entierAlea(2, maxQ, [])
        exercice.raison = q
        alpha = entierAlea(2, maxAlpha, [q]) * Math.pow(-1, entierAlea(0, 1, []))
        sgnAlpha = (Math.abs(alpha) < Math.pow(10, -12)) ? 0 : alpha / Math.abs(alpha)
        absAlpha = Math.abs(alpha).toString()
        if (options.modele === 1) {
          beta = 0
        } else {
          beta = entierAlea(-maxBeta, maxBeta, [0])
        }
        do {
          gamma = entierAlea(-maxGamma, maxGamma, [0])
          c = beta + gamma * (1 - q) - b * k
        }
        while (c === 0)

        a = q
        b = beta * (1 - q)
        c = beta + gamma * (1 - q) - b * k

        uInit = (alpha + gamma).toString()
        gamma1 = gamma - beta * k
        famille = [q.toString() + '^{' + exposant + '}', 'n', '']
        formuleExpl = 'u_n=' + arithmetico(alpha, q, exposant, beta, 'n', gamma1)
        coefficients = [alpha, beta, gamma1]

        if (Math.abs(beta) !== 1) {
          justifRangInit = arithmetico(alpha, q, exposantInit, beta, '\\times ' + strK, gamma1)
        } else {
          justifRangInit = arithmetico(alpha, q, exposantInit, beta, strK, gamma1)
        }
        if (k === 1) {
          nextRang = 'n'
          // On prépare les solutions possibles par permutations
          // var fam="*"+q.toString()+"^n"
          fam1 = absAlpha + '*' + q.toString() + '^n'
          fam2 = q.toString() + '^n' + '*' + absAlpha
        } else {
          nextRang = (k === 0) ? 'n+' + (1 - k).toString() : 'n' + (1 - k).toString()
          // var fam="*"+q.toString()+"^("+nextRang+")"
          fam1 = absAlpha + '*' + q.toString() + '^(' + nextRang + ')'
          fam2 = q.toString() + '^(' + nextRang + ')*' + absAlpha
        }
        gamma2 = beta + gamma1
        formNextRang = arithmetico(alpha, q, nextRang, beta, '(n+1)', gamma1)
        // formNextRangInter=sommeMonomes([alpha,beta1,gamma1],"(n^2+2n+1)","(n+1)","")
        formNextRangReduit = arithmetico(alpha, q, nextRang, beta, 'n', gamma2)

        // tabRepSimplifiee=ecrituresAccept([alpha,beta,gamma2],[fam,"n",""])
        tabRepSimplifiee = ecrituresAccept([sgnAlpha, beta, gamma2], [fam1, 'n', ''])
        tabRepSimplifiee = tabRepSimplifiee.concat(ecrituresAccept([sgnAlpha, beta, gamma2], [fam2, 'n', '']))

        demNextFormReduite = formNextRang + '=' + formNextRangReduit

        // Cette partie servira dans l’application de f à l’étape hérédité
        exercice.nextRang = nextRang
        exercice.alpha = alpha
        exercice.beta = beta
        exercice.gamma1 = gamma1
        // Correction
        exercice.D = sommeMonomes([a, 0, 0], ['u_n', 'n', '']) + '=' + arithmetico(alpha, q, nextRang, a * beta, 'n', a * gamma1)
        exercice.E = sommeMonomes([a, b, 0], ['u_n', 'n', '']) + '=' + arithmetico(alpha, q, nextRang, a * beta + b, 'n', a * gamma1)
        exercice.F = sommeMonomes([a, b, c], ['u_n', 'n', '']) + '=' + arithmetico(alpha, q, nextRang, beta, 'n', gamma2)
        exercice.G = q.toString() + '×' + alpha.toString() + '×' + q.toString() + '^{' + exposant + '}=' + alpha.toString() + '×' + q.toString() + '×' + q.toString() + '^{' + exposant + '}=' + alpha.toString() + '×' + q.toString() + '^{' + nextRang + '}'
        exercice.indication = textes.phrase_ind1

        break
    }

    let formuleRec = sommeMonomes([a, b, c], ['u_n', 'n', ''])
    // coefficients de la fonction f(x,n) telle que u_(n+1)=f(u_n)
    exercice.tab_sol = [a, b, c]

    formuleRec = 'u_{n+1}=' + formuleRec
    exercice.b = formuleRec
    exercice.formuleRec = formuleRec
    exercice.c = 'u_{' + strK + '}=' + uInit
    exercice.d = formuleExpl
    exercice.e = k

    exercice.f = justifRangInit + '=' + uInit
    exercice.g = 'u_{n+1}=' + formNextRang
    exercice.h = demNextFormReduite
    exercice.i = 'u_{n+1}=' + formNextRangReduit
    exercice.j = exercice.nb_chances[2]
    exercice.formNextRang = j3pMathquillXcas(formNextRangReduit)
    exercice.coefficients = coefficients
    exercice.famille = famille
    exercice.m = 'u_{n+1}='
    // Création de zone de saisie pour l’hérédité
    for (let i = 1; i < 10; i++) {
      exercice['liste' + i.toString()] = { texte: ['+', '×'], taillepolice: 20, correction: 0 }
      exercice['inputmq' + i.toString()] = {}
      exercice['input' + i.toString()] = {}
    }
    exercice.tabRepSimplifiee = tabRepSimplifiee
    exercice.fin = false
    exercice.A = a
    exercice.B = sommeMonomes([b], ['n'])
    exercice.C = c
    exercice.H = 'u_{' + strK + '}=' + uInit

    return exercice
  }

  function enonceMain () {
    numQuest = numEtape(me.questionCourante, ds.nbetapes)
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '15px' }) })

    if (numQuest === 1) {
      const options = {}
      const numExo = Math.floor(me.questionCourante / ds.nbetapes)
      options.modele = stor.modele[numExo]
      options.rang_depart = stor.rang_depart[numExo]
      options.redaction = ds.redaction
      stor.exercice = genererExo(options)
      stor.fonction = [1, 0, 0]
      stor.etape_heredite = 0
    }
    if (numQuest === 3) {
      me.indication(me.zonesElts.IG, stor.exercice.indication)
    }

    stor.exercice.numero_question = numQuest
    // On positionne le div explications
    stor.zoneExpli = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '15px' }) })
    affichage()

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }
  let numQuest
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // toujours 3 étapes
        me.surcharge({ nbetapes: 3 })
        // Construction de la page
        me.construitStructurePage('presentation1')
        // On récupère les paramètres :
        stor.modele = creeTableauAleatoire([1, 2, 3], ds.probas_modele, ds.nbrepetitions)
        stor.rang_depart = creeTableauAleatoire([0, 1, 2], ds.probas_rang_depart, ds.nbrepetitions)
        stor.nb_chances_init = ds.nb_chances_init
        stor.nb_chances_formule_heredite = ds.nb_chances_formule_heredite
        stor.nb_etapes_heredite = ds.nb_etapes_heredite
        me.afficheTitre(textes.titreExo)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        j3pEmpty(me.zonesElts.IG)
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////

      enonceMain()

      break // case "enonce":

    case 'correction':
      // Il faut d’abord analyser la réponse en fonction de numQuest
      numQuest = numEtape(me.questionCourante, ds.nbetapes)
      if (stor.exercice.numero_question === 3) {
        // One doit pas barrer si on arrive à la dernière étape, pour laisser à l’élève le temps de rectifier son calcul
        ds.nbchances = stor.exercice.nb_chances[numQuest - 1]
      } else {
        ds.nbchances = stor.exercice.nb_chances[numQuest - 1]
      }
      {
        const bilan = correction()

        // On teste si une réponse a été saisie

        if (!bilan.aRepondu) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (bilan.bonneReponse) {
            me.score++
            stor.zoneCorr.innerHTML = ''
            bilan.cbien()
            bilan.correction(true)
          } else {
            j3pEmpty(stor.zoneCorr)
            if (bilan.autre_chance) {
              bilan.indication()
              if (stor.exercice.numero_question === 3) {
                if (stor.etape_heredite % 2 === 0) {
                  const nb = ds.nb_erreurs_heredite - stor.nbErreurs
                  if (nb > 0) {
                    const divNbChances = j3pAddElt(stor.zoneCorr, 'div', '', { style: { color: me.styles.cfaux } })
                    const txtNbChances = (nb > 1) ? textes.nbchances1 : textes.nbchances2
                    j3pAffiche(divNbChances, '', txtNbChances, { n: nb })
                  }
                } else {
                  // Enfin, je donne le focus à la zone de saisie (ici c’est la question où on choisit l’opération à effectuer)
                  j3pFocus(stor.elt3.inputList[0])
                }
              }
              return me.finCorrection()
            } else {
            // Erreur au nème essai
              bilan.regarde()
              bilan.correction(false)
            }
          }
        }
      }
      // Obligatoire

      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
