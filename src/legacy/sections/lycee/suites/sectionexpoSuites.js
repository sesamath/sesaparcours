import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomElt, j3pGetRandomInt, j3pMonome, j3pPaletteMathquill, j3pShuffle, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since juin 2022
 * @fileOverview Dans cette section, on vérifie si une suite définie avec la fonction exponentielle est arithmétique ou géométrique (ou ni l’un ni l’autre)
 */

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}
const textes = {
  titre_exo: 'Suite et fonction exponentielle',
  consigne1: 'Soit $(£u_n)$ la suite définie pour tout entier naturel $n$ par $£u_{n}=£{un}$.',
  consigne2: 'La suite $(£u_n)$ est&nbsp;:',
  consigne3: 'géométrique|arithmétique|ni arithmétique ni géométrique',
  consigne4: 'Sa raison vaut alors &1&.',
  comment1: '',
  corr1: 'Pour tout entier naturel $n$,',
  corr2_1: 'Donc la suite $(£u_n)$ est géométrique de raison $£r$.',
  corr2_2: 'Donc la suite $(£u_n)$ est arithmétique de raison $£r$.',
  corr2_3: 'Donc la suite $(£u_n)$ n’est ni arithmétique, ni géométrique.',
  corr3: 'Or $£p$ et $£q$.'
}
/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour chaque sujet
 * @private
 * @param {Number} cas : entier entre 1 et 3 donnant le cas de figure (1 pour géo, 2 pour arith et 3 pour ni l’un ni l’autre)
 * @return {Object}
 */
function genereAlea (cas) {
  const obj = {}
  do {
    obj.a = j3pGetRandomInt(-3, 4)
  } while ([0, 1].includes(obj.a))
  obj.b = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
  obj.u = j3pGetRandomElt(['u', 'v', 'w'])
  obj.tabCalc = []
  if (cas === 1) {
    obj.un = '\\mathrm{e}^{' + j3pMonome(1, 1, obj.a, 'n') + j3pMonome(2, 0, obj.b) + '}'
    obj.tabCalc.push(obj.u + '_{n+1}=\\mathrm{e}^{' + j3pMonome(1, 1, obj.a, '(n+1)') + j3pMonome(2, 0, obj.b) + '}=\\mathrm{e}^{' + j3pMonome(1, 1, obj.a, 'n') + j3pMonome(2, 0, obj.a) + j3pMonome(2, 0, obj.b) + '}')
    obj.tabCalc.push(obj.u + '_{n+1}=\\mathrm{e}^{' + obj.a + '}\\times \\mathrm{e}^{' + j3pMonome(1, 1, obj.a, 'n') + j3pMonome(2, 0, obj.b) + '}=\\mathrm{e}^{' + obj.a + '}\\times ' + obj.u + '_n')
  } else if (cas === 2) {
    obj.un = '\\mathrm{e}^{' + obj.a + '}n' + j3pMonome(2, 0, obj.b)
    obj.tabCalc.push(obj.u + '_{n+1}=\\mathrm{e}^{' + obj.a + '}(n+1)' + j3pMonome(2, 0, obj.b) + '=\\mathrm{e}^{' + obj.a + '}n+\\mathrm{e}^{' + obj.a + '}' + j3pMonome(2, 0, obj.b))
    obj.tabCalc.push(obj.u + '_{n+1}=\\mathrm{e}^{' + obj.a + '}+' + obj.u + '_n')
  } else {
    obj.un = '\\mathrm{e}^{' + j3pMonome(1, 1, obj.a, 'n') + '}' + j3pMonome(2, 0, obj.b)
    obj.tabCalc.push(obj.u + '_0=1' + j3pMonome(2, 0, obj.b) + '=' + (1 + obj.b) + '\\quad ; \\quad ' + obj.u + '_1=\\mathrm{e}^{' + obj.a + '}' + j3pMonome(2, 0, obj.b) + '\\quad ; \\quad ' + obj.u + '_2=\\mathrm{e}^{' + 2 * obj.a + '}' + j3pMonome(2, 0, obj.b))
    obj.tabCalc.push(obj.u + '_2-' + obj.u + '_1\\neq ' + obj.u + '_1-' + obj.u + '_0')
    obj.tabCalc.push('\\frac{' + obj.u + '_2}{' + obj.u + '_1}\\neq \\frac{' + obj.u + '_1}{' + obj.u + '_0}')
  }
  obj.r = '\\mathrm{e}^{' + obj.a + '}'
  obj.rVal = j3pCalculValeur('exp(' + obj.a + ')')
  // console.log('obj:', obj)
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  // Construction de la page
  me.construitStructurePage({ structure: 'presentation1' })
  me.afficheTitre(textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  const stor = me.storage
  stor.typeQuest = []
  if (me.donneesSection.nbrepetitions === 1) {
    stor.typeQuest.push(j3pGetRandomInt(1, 2))
  } else if (me.donneesSection.nbrepetitions === 2) {
    const choix1 = j3pGetRandomInt(1, 2)
    stor.typeQuest.push(choix1, 3 - choix1)
  } else {
    for (let i = 1; i < me.donneesSection.nbrepetitions / 2; i++) stor.typeQuest.push(1, 2)
    if (me.donneesSection.nbrepetitions % 2 === 0) stor.typeQuest.push(j3pGetRandomInt(1, 2))
    stor.typeQuest.push(3)
    stor.typeQuest = j3pShuffle(stor.typeQuest)
  }
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.leChoix = stor.typeQuest[me.questionCourante - 1]
  stor.objDonnees = genereAlea(stor.leChoix)
  // on affiche l’énoncé
  ;[1, 2, 3, 4, 5].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })
  j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objDonnees)
  j3pAffiche(stor.zoneCons2, '', textes.consigne2, stor.objDonnees)
  stor.nameRadio = j3pGetNewId('choix')
  stor.idsRadio = ['1', '2', '3'].map(i => j3pGetNewId('radio' + i))
  const choixRadio = textes.consigne3.split('|')
  ;[0, 1, 2].forEach(i => {
    stor['divRadio' + (i + 1)] = j3pAddElt(stor.zoneCons3, 'div')
    j3pBoutonRadio(stor['divRadio' + (i + 1)], stor.idsRadio[i], stor.nameRadio, i, choixRadio[i])
    j3pElement(stor.idsRadio[i]).index = i
  })
  j3pStyle(stor.zoneCons5, { paddingTop: '10px' })
  stor.btnSelect = -1
  ;[1, 2].forEach(i => {
    j3pElement(stor.idsRadio[i - 1]).addEventListener('click', function () {
      if (stor.btnSelect !== this.index) {
        ;[4, 5].forEach(i => j3pEmpty(stor['zoneCons' + i]))
        const elt = j3pAffiche(stor.zoneCons4, '', textes.consigne4)
        stor.zoneInput = elt.inputmqList[0]
        stor.zoneInput.typeReponse = ['nombre', 'exact']
        stor.zoneInput.reponse = [stor.objDonnees.rVal]
        mqRestriction(stor.zoneInput, '\\d,.e^-', { commandes: ['puissance', 'exp'] })
        j3pPaletteMathquill(stor.zoneCons5, stor.zoneInput, { liste: ['puissance', 'exp'] })
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: [stor.zoneInput.id]
        })
        stor.btnSelect = this.index
      }
      j3pFocus(stor.zoneInput)
    })
  })
  j3pElement(stor.idsRadio[2]).addEventListener('click', function () {
    ;[4, 5].forEach(i => j3pEmpty(stor['zoneCons' + i]))
  })
  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })

  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  if (stor.choixRadio !== 2) j3pDetruit(stor.zoneCons5)
  ;[0, 1, 2].forEach(i => j3pDesactive(stor.idsRadio[i]))
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  ;[1, 2, 3, 4].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
  j3pAffiche(stor.zoneExpli1, '', textes.corr1)
  if (stor.leChoix === 3) {
    j3pAffiche(stor.zoneExpli2, '', '$' + stor.objDonnees.tabCalc[0] + '$')
    j3pAffiche(stor.zoneExpli3, '', textes.corr3, {
      p: stor.objDonnees.tabCalc[1],
      q: stor.objDonnees.tabCalc[2]
    })
  } else {
    ;[2, 3].forEach(i => j3pAffiche(stor['zoneExpli' + i], '', '$' + stor.objDonnees.tabCalc[i - 2] + '$'))
  }
  j3pAffiche(stor.zoneExpli4, '', textes['corr2_' + stor.leChoix], stor.objDonnees)
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  let reponse = { aRepondu: false, bonneReponse: false }
  stor.choixRadio = j3pBoutonRadioChecked(stor.nameRadio)[0]
  reponse.aRepondu = (stor.choixRadio > -1)
  if (reponse.aRepondu) {
    stor.bonChoixRadio = (stor.choixRadio === stor.leChoix - 1)
    if (stor.leChoix === 3) {
      // ni arithmetique, ni geometrique
      reponse.bonneReponse = stor.bonChoixRadio
      if (!stor.bonChoixRadio) {
        stor.fctsValid.zones.bonneReponse[0] = false
        stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
      }
    } else {
      if (stor.bonChoixRadio) {
        reponse = stor.fctsValid.validationGlobale()
      } else {
        reponse.bonneReponse = false
        if (stor.choixRadio !== 2) {
          stor.fctsValid.zones.bonneReponse[0] = false
          stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
        }
      }
    }
  }
  if (reponse.aRepondu) {
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  j3pElement('label' + stor.idsRadio[stor.choixRadio]).style.color = stor.bonChoixRadio ? me.styles.cbien : me.styles.cfaux
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (!stor.bonChoixRadio) {
      j3pBarre('label' + stor.idsRadio[stor.choixRadio])
      j3pElement('label' + stor.idsRadio[stor.leChoix - 1]).style.color = me.styles.moyen.correction.color
    }
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
