import { j3pAddElt, j3pDetruit, j3pExtraireCoefsFctAffine, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pShowError, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pExtraireNumDen } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import { arithGeo } from 'src/legacy/sections/lycee/suites/constantesSuites'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Rémi DENIAUD
        octobre 2016
        Dans cette section, on part d’un contexte (issu de sujets de bac - voir Fichier sectionsAnnexes/Consignesexos/tauxSuites.js)
        et on demande d’exprimer le terme a_{n+1} en fonctin de a_n (suite arithmético-géométrique)
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['tabCasFigure', [], 'array', 'Pour chaque répétition, on peut imposer, sous forme d’un tableau, un des exercices particuliers présents dans le fichier texte annexe&nbsp;:\n-1&nbsp;: Pondichéry avril 2016 ES\n-2&nbsp;: Liban mai 2016 ES\n-3&nbsp;: Amérique du Nord juin 2016 ES\n-4&nbsp;: Métropole juin 2016 ES\n-5&nbsp;: Asie juin 2016 ES\n-6&nbsp;: Métropole septembre 2016 ES\n-7&nbsp;: Antilles septembre 2016 ES\n-8&nbsp;: Exercice 83 p. 36 manuel Magnard S']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Taux d’évolution et suite arithmético-géométrique',
  // les phrases de la consigne sont dans un fichier externe
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'La réponse est correcte mais elle doit être donnée sous forme simplifiée !'
  // et les phrases utiles pour les explications de la réponse
  // elles sont également dans le fichier externe
}

/**
 * section EvolutionSuite
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let i = 0; i < stor.correctionSuite['enonce' + stor.numeroExo].length; i++) {
      stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
      j3pAffiche(stor['zoneExpli' + i], '', stor.correctionSuite['enonce' + stor.numeroExo][i],
        stor.obj)
    }
  }

  function RemplaceCalcul (obj) {
    // obj contient trois propriétés (la dernière étant optionnelle):
    // -obj.text qui est le texte correspondant à la formule de calcul
    // -obj.obj qui est la liste des valeurs à remplacer et leurs valeurs respectives
    // Par exemple : RemplaceCalcul({text:'2*£a+£b', obj:{a:'2',b:'-3'}})
    // le résultat renvoyé est la valeur du calcul (format numérique)
    if (!obj.calcul) {
      obj.calcul = true
    }
    let newTxt = obj.text
    let pos1
    while (newTxt.includes('£')) {
      // on remplace £x par sa valeur
      pos1 = newTxt.indexOf('£')
      const laVariable = newTxt.charAt(pos1 + 1)
      const nombre = obj.obj[laVariable]
      newTxt = newTxt.substring(0, pos1) + '(' + nombre + ')' + newTxt.substring(pos1 + 2)
    }
    if (newTxt.includes('frac')) {
      const numden = j3pExtraireNumDen(newTxt)
      return '\\frac{' + Math.round(Math.pow(10, 10) * j3pCalculValeur(numden[1])) / Math.pow(10, 10) + '}{' + Math.round(Math.pow(10, 10) * j3pCalculValeur(numden[2])) / Math.pow(10, 10) + '}'
    } else {
      return Math.round(Math.pow(10, 10) * j3pCalculValeur(newTxt)) / Math.pow(10, 10)
    }
  }

  function fractionComplete (texte) {
    // cette fonction vérifie si texte est l’équivalent d’un écriture sous la forme d’une fraction
    const testReg = /^\(.+\)\/\([0-9]+\)$/g // new RegExp('\\({1}.+\\){1}\\/\\({1}[0-9]+\\){1}', 'i')
    let posFrac, posInit
    if (testReg.test(texte)) {
      if (texte.match(testReg)[0] === texte) {
        posFrac = texte.lastIndexOf('/')
        let numparferm = 1
        posInit = posFrac - 2// en position posFrac-2 on a nécessairement ')'
        while (posInit > -1) {
          if (texte[posInit] === ')') {
            numparferm++
          } else if (texte[posInit] === '(') {
            numparferm--
          }
          posInit--
          if (posInit === -1) {
            return true
          }
          if (numparferm === 0) {
            posInit = -1
          }
        }
        return false
      } else {
        return false
      }
    } else {
      return false
    }
  }
  function rechercheCoefAffine (fctAffine, nomVar) {
    // fctAffine représente une fonction affine (x est la variable par defaut)
    // mais si elle apparait plusieurs fois, j3pExtraireCoefsFctAffine ne fonctionnera pas, d’où cette fonction qui fera la même chose
    // celle-ci s’applique plutôt sur des réponses élèves, pas toujours lumineuses...
    // La fonction renvoie un objet : estAffine qui est un booleen, coefX et coefConst
    let laFct = fctAffine
    while (laFct.includes(nomVar)) laFct = laFct.replace(nomVar, 'x')
    const coefAffine = [0, 0]
    const recupFrac = /[+-]?\([0-9]+\.?[0-9]*\)\/\([0-9]\)x/g.exec(laFct) // Pour récupérer les expressions (a)/(b)*x
    if (recupFrac) {
      for (let i = 1; i < recupFrac.length; i++) {
        coefAffine[0] += j3pCalculValeur(recupFrac[i])
        laFct = laFct.replace(recupFrac[i], '')
      }
    }
    const recupNb = laFct.match(/[+-]?[0-9]+\.?[0-9]*x/g) // Pour récupérer les expressions k*x
    if (recupNb) {
      for (let i = 0; i < recupNb.length; i++) {
        coefAffine[0] += j3pCalculValeur(recupNb[i].substring(0, recupNb[i].length - 1))
        laFct = laFct.replace(recupNb[i], '')
      }
    }
    coefAffine[1] = (laFct === '') ? 0 : j3pCalculValeur(laFct)
    return { estAffine: !laFct.includes('x'), coefX: coefAffine[0], coefConst: coefAffine[1] }
  }
  function suite () {
    // OBLIGATOIRE

    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    let choixExoImpose = (ds.tabCasFigure[stor.numexo] !== undefined)
    if (choixExoImpose) {
      // je regarde tout de même si le numéro existe bien
      if (Number(ds.tabCasFigure[stor.numexo]) > stor.nbSujets) choixExoImpose = false
    }
    if (choixExoImpose) {
      // c’est que j’ai imposé l’exo à traiter
      stor.numeroExo = Number(ds.tabCasFigure[stor.numexo]) - 1
    } else {
      // je choisis au hasard un des exos à traiter (parmi ceux qui ne sont pas déjà choisis)
      let nbTentatives = 0 // c’est au cas où on aurait déjà proposé tous les cas de figure (vu le nombre d’exos possible qu’il risque d’y avoir, c’est peu probable)
      do {
        nbTentatives++
        stor.numeroExo = j3pGetRandomInt(0, (stor.nbSujets - 1))
      } while ((ds.tabCasFigure.indexOf(stor.numeroExo + 1) > -1) && (nbTentatives < 30))
      ds.tabCasFigure[(me.questionCourante - 1) / ds.nbetapes] = stor.numeroExo + 1
    }
    me.logIfDebug('ds.tabCasFigure:', ds.tabCasFigure, '   stor.numeroExo:', stor.numeroExo)
    stor.numexo++
    // stor.phrasesEnonce = stor.tabExercices[stor.numeroExo]
    stor.phrasesEnonce = stor.consignes['enonce' + stor.numeroExo]
    stor.obj = {}
    for (let i = 0; i < stor.nomVariables['enonce' + stor.numeroExo].length; i++) {
      stor.obj[stor.nomVariables['enonce' + stor.numeroExo][i]] = stor.variables['enonce' + stor.numeroExo][i]
    }
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '8px' }) })
    for (let i = 1; i <= stor.phrasesEnonce.length; i++) {
      stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(stor['zoneCons' + i], '', stor.phrasesEnonce[i - 1],
        stor.obj)
    }
    j3pStyle(stor['zoneCons' + (stor.phrasesEnonce.length)], { fontStyle: 'italic' })
    const numIdSuivant = stor.phrasesEnonce.length + 1
    stor['zoneCons' + numIdSuivant] = j3pAddElt(stor.conteneur, 'div')
    stor.phraseRep = j3pAffiche(stor['zoneCons' + numIdSuivant], '', '$' + stor.questionSuite['enonce' + stor.numeroExo] + '=$&1&',
      { inputmq1: { texte: '' } })
    stor.phraseRep.inputmqList[0].typeReponse = ['texte']
    stor.phraseRep.inputmqList[0].reponse = [stor.reponseSuite['enonce' + stor.numeroExo][0]]
    // La variable suivante est utilisée si l’élève donne la bonne expression de la suite, mais non simplifiée
    // on ne lui compte pas faux lors de la première tentative, mais on lui redonne une chance de simplifier son expression
    stor.alerteSimplification = false
    // on exporte les variables présentes dans cette section pour qu’elles soient utilisées dans SuiteTermeGeneral
    stor.defSuite.nomU = stor.reponseSuite['enonce' + stor.numeroExo][3].split('_')[0]
    let c
    let nbTentative = 0
    do {
      stor.defSuite.a = RemplaceCalcul({
        text: stor.reponseSuite['enonce' + stor.numeroExo][1],
        obj: stor.obj
      })
      stor.defSuite.b = RemplaceCalcul({
        text: stor.reponseSuite['enonce' + stor.numeroExo][2],
        obj: stor.obj
      })
      stor.defSuite.termeInitial = String(stor.reponseSuite['enonce' + stor.numeroExo][4]) + '|' + RemplaceCalcul({
        text: stor.reponseSuite['enonce' + stor.numeroExo][5],
        obj: stor.obj
      })
      nbTentative++
      c = -stor.defSuite.b / (1 - stor.defSuite.a)
    } while (Math.abs(c + stor.defSuite.termeInitial) < Math.pow(10, -12) && (nbTentative < 20))
    if (nbTentative === 20) j3pShowError(Error('Il y a un souci avec les données car cela va générer une boucle infinie à un moment'))
    stor.defSuite.typeSuite = arithGeo
    // me.donneesPersistantes.suites sert à envoyer des informations dans le bilan qui pourront être récupérées par une autre section sur les suites
    // Ceci a pour but de poursuivre l’étude de la suite définie dans cette section
    me.donneesPersistantes.suites = { ...stor.defSuite }
    me.logIfDebug('me.donneesPersistantes.suites:', me.donneesPersistantes.suites)
    stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px' } })
    j3pPaletteMathquill(stor.laPalette, stor.phraseRep.inputmqList[0], {
      liste: ['fraction', 'indice']
    })
    mqRestriction(stor.phraseRep.inputmqList[0], '\\d,.+-*/()n_' + stor.defSuite.nomU, {
      commandes: ['fraction', 'indice']
    })
    j3pFocus(stor.phraseRep.inputmqList[0])
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mes_zones_saisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    stor.mes_zones_saisie = [stor.phraseRep.inputmqList[0].id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: stor.mes_zones_saisie,
      validePerso: stor.mes_zones_saisie
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage('presentation1')
        me.afficheTitre(textes.titre_exo)
        stor.defSuite = {}
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        import('../../../sectionsAnnexes/Consignesexos/tauxSuites.js').then(function parseAnnexe ({ default: datasSection }) {
          let j
          let i
          const tabExercices = []
          stor.nbSujets = datasSection.sujets.length
          stor.consignes = {}
          stor.nomVariables = {}
          stor.variables = {}
          stor.questionSuite = {}
          stor.reponseSuite = {}
          stor.correctionSuite = {}
          for (j = 0; j < stor.nbSujets; j++) {
            tabExercices.push(j)
            stor.consignes['enonce' + j] = datasSection.sujets[j].consignes
            stor.nomVariables['enonce' + j] = []
            stor.variables['enonce' + j] = []
            for (i = 0; i < datasSection.sujets[j].variables.length; i++) {
              stor.nomVariables['enonce' + j][i] = datasSection.sujets[j].variables[i][0]
              const [minInt, maxInt] = j3pGetBornesIntervalle(datasSection.sujets[j].variables[i][1])
              stor.variables['enonce' + j][i] = (datasSection.sujets[j].variables[i].length === 2) ? j3pGetRandomInt(minInt, maxInt) : j3pGetRandomInt(minInt, maxInt) / Number(datasSection.sujets[j].variables[i][2])
              // console.log('stor.nomVariables[enonce'+j+']['+i+']='+stor.nomVariables['enonce'+j][i])
              // console.log('stor.variables[enonce'+j+']['+i+']='+stor.variables['enonce'+j][i])
            }
            stor.questionSuite['enonce' + j] = datasSection.sujets[j].questionSuite
            stor.reponseSuite['enonce' + j] = datasSection.sujets[j].reponseSuite
            stor.correctionSuite['enonce' + j] = datasSection.sujets[j].correctionSuite
          }
          // enfin je gère le cas où on demanderait un cas de figure qui n’existe pas :
          let index = 0
          while (index < ds.tabCasFigure.length) {
            ds.tabCasFigure[index] = Number(ds.tabCasFigure[index])
            if ((ds.tabCasFigure[index] > 0) && (ds.tabCasFigure[index] <= tabExercices.length)) {
              index++
            } else {
              // le numéro de question choisi n’existe pas
              ds.tabCasFigure.splice(index, 1)
            }
          }
          stor.numexo = 0 // c’est pour savoir à quelle répétition nous en sommes

          suite()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()

        suite()
      }

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si la zone a bien été complétée ou non
        // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
        // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
        // Si c’est la dernière tentative de répondre, on barre également les zones fausses
        let imagesEgales
        if (reponse.aRepondu) {
        // question où on demande p_{n+1} en fonction de p_n
          const coefa = stor.defSuite.a
          const coefb = stor.defSuite.b
          const maFonctionAffine = coefa + '*x+(' + coefb + ')'
          let reponseEleve = j3pValeurde(stor.phraseRep.inputmqList[0])
          while (reponseEleve.indexOf(stor.reponseSuite['enonce' + stor.numeroExo][3]) > -1) {
            reponseEleve = reponseEleve.replace(stor.reponseSuite['enonce' + stor.numeroExo][3], 'x')
          }
          if (reponseEleve.includes('n')) {
            // c’est nécessairement faux car c’est qu’il reste n dans l’écriture
            reponse.bonneReponse = false
          } else {
            reponseEleve = j3pMathquillXcas(reponseEleve)

            // console.log('reponseEleve:',reponseEleve,'  maFonctionAffine:',maFonctionAffine)
            // il faut que je vérifie si c’est bien une fonction affine simplifiée
            let tabCoefRep
            // je remplace les expressions fractionnaires de la forme (a)/(b) par a/b
            reponseEleve = reponseEleve.replace(/\(([0-9,.]+)\)\/\(([0-9]+)\)/, '$1/$2')
            try {
              if (fractionComplete(reponseEleve)) {
                // mon expression est de la forme (ax+b)/c
                const tabDecoupageFrac = reponseEleve.split('/')
                const numRep = tabDecoupageFrac[0].substring(1, tabDecoupageFrac[0].length - 1)
                // recherche des coefs a et b de ce numérateur ax+b
                tabCoefRep = [0, 0]
                if (numRep.split('x').length > 1) {
                  const coefficientsAffines = rechercheCoefAffine(numRep)
                  if (coefficientsAffines.estAffine) tabCoefRep = [coefficientsAffines.coefX, coefficientsAffines.coefConst]
                } else {
                  tabCoefRep = j3pExtraireCoefsFctAffine(numRep)
                  const denRep = (tabDecoupageFrac[1].charAt(0) === '(') ? tabDecoupageFrac[1].substring(1, tabDecoupageFrac[1].length - 1) : tabDecoupageFrac[1]
                  tabCoefRep[0] = j3pGetLatexQuotient(tabCoefRep[0], denRep)
                  tabCoefRep[1] = j3pGetLatexQuotient(tabCoefRep[1], denRep)
                }
              } else {
                // Mon nouveau pb est d’avoir quelque chose de la forme (ax)/c+d
                const numexRepExp = /\([0-9.,x]+\)\/\([0-9]+\)/i // new RegExp('\\({1}[0-9\\.\\,x]{1,}\\){1}/{1}\\({1}[0-9]{1,}\\){1}', 'i')
                if (numexRepExp.test(reponseEleve)) {
                  const tab = reponseEleve.match(numexRepExp)
                  // console.log('tab:', tab)
                  for (let i = 0; i < tab.length; i++) {
                    if (tab[i].indexOf('x') > -1) {
                      let nume = tab[i].substring(1, tab[i].indexOf('/') - 1)
                      const deno = tab[i].substring(tab[i].indexOf('/') + 2, tab[i].length - 1)
                      nume = nume.replace('x', '')
                      // console.log('nume:',nume,'  deno:',deno)
                      reponseEleve = reponseEleve.replace(tab[i], '(' + nume + ')/(' + deno + ')*x')
                    }
                  }
                }
                // console.log('reponseEleve:', reponseEleve)
                tabCoefRep = j3pExtraireCoefsFctAffine(reponseEleve)
              }
            } catch (e) {
              console.warn(e)
              // Cela se produit avec une réponse élève non cohérente
              // on est alors sûr que la réponse sera considérée comme fausse
              tabCoefRep = [0, 0]
            }
            let repFonctionAffine = tabCoefRep[0] + '*x+(' + tabCoefRep[1] + ')'
            repFonctionAffine = j3pMathquillXcas(repFonctionAffine)
            // console.log('repFonctionAffine:',repFonctionAffine)
            imagesEgales = true // là je vérifie juste sir les deux expressions donnent bien les mêmes images
            const arbreLaReponse = new Tarbre(maFonctionAffine, ['x'])
            const arbreRepEleve = new Tarbre(repFonctionAffine, ['x'])
            const arbreRepInit = new Tarbre(reponseEleve, ['x'])
            let bonneFctAffine = (repFonctionAffine.split('x').length === 2) // là je vérifie si la réponse de l’élève est correcte et simplifiée
            if (bonneFctAffine) {
              // autre chose pour avoir la bonneFctAffine : je ne dois pas avoir plus d’un signe + (hormis éventuellement 1 premier signe + que je ne prends pas en compte)
              bonneFctAffine = (repFonctionAffine.substring(1).split('+').length <= 2)
            }
            if (bonneFctAffine) {
              for (let i = 2; i <= 10; i++) {
                bonneFctAffine = (bonneFctAffine && (Math.abs(arbreLaReponse.evalue([i]) - arbreRepEleve.evalue([i])) < Math.pow(10, -10)))
              }
              // console.log('bonneFctAffine:',bonneFctAffine)
            }
            if (!bonneFctAffine) {
              for (let i = 2; i <= 10; i++) {
                imagesEgales = (imagesEgales && (Math.abs(arbreLaReponse.evalue([i]) - arbreRepInit.evalue([i])) < Math.pow(10, -10)))
              }
              if (imagesEgales) {
                if (stor.alerteSimplification) {
                  stor.alerteSimplification = true
                  // on ne lui compte pas ça comme un vrai essai
                  me.essaiCourant--
                }
              }
            }
            reponse.bonneReponse = bonneFctAffine
          }
          fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
          fctsValid.coloreUneZone(stor.mes_zones_saisie[0])
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (imagesEgales) {
                stor.zoneCorr.innerHTML = textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
