/**
 * Cette section affiche une série de 10 calculs du produit de 2 nombres complexes
 * Les coefficients sont entiers relatifs. section quantitative
 *
 * Code repris par Rémi en août 2020 pour enlever xcas
 * @author Alexis Lecomte
 * @author Rémi DENIAUD
 * @since 2012-2013
 * @fileOverview
 */
import $ from 'jquery'
import { j3pAddElt, j3pAjouteFoisDevantX, j3pBarre, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pShowError } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import { j3pAffiche, j3pDesactive, j3pMathsAjouteDans, mqRestriction } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 10, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['a', '[-9;9]', 'string', 'coefficient a de ci(a+ib)'],
    ['b', '[-9;9]', 'string', 'coefficient b de ci(a+ib)'],
    ['c', '[-9;9]', 'string', 'coefficient c de ci(a+ib)']
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 10,
      nbetapes: 1,
      textes: {
        titreExo: 'Calcul de ki(a+ib)',
        question1: 'Ecris sous forme algébrique le produit suivant&nbsp;:',
        correction1: 'Ta réponse n’est pas écrite sous forme algébrique simplifiée. Voici la bonne réponse : $£a$.',
        correction2: 'Ta réponse est exacte mais n’est pas écrite sous forme algébrique. Voici la bonne réponse : $£a$.',
        correction3: 'La bonne réponse est $£a$.'
      },
      a: '[-9;9]',
      b: '[-9;9]',
      c: '[-9;9]',
      nbchances: 1,
      structure: 'presentation1',
      indication: ''
    }
  }
  function depart () {
    getMtgCore().then(
      // success
      (mtg32App) => {
        // txt html de la figure
        const fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAALgAAACVAAAAQEAAAAAAAAAAQAAAAb#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAFiAAItMf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABP#AAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wABZgAFYSp4K2L#####AAAAAQAKQ09wZXJhdGlvbgAAAAAFAv####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAB#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAYAAAACAAF4AAAABAD#####AANyZXAABTIqeC0xAAAABQEAAAAFAgAAAAFAAAAAAAAAAAAAAAcAAAAAAAAAAT#wAAAAAAAAAAF4#####wAAAAMAEENUZXN0RXF1aXZhbGVuY2UA#####wAHZWdhbGl0ZQAAAAMAAAAEAQAAAAABP#AAAAAAAAAB################'
        stor.liste = mtg32App.createList(fig)
        enonceMain()
      },
      // failure
      (error) => {
        j3pShowError(error, { message: 'Le chargement de mathgraph a échoué, impossible de continuer' })
      }
    ).catch(error => {
      // là c’est notre code à l’intérieur de success qui a planté
      j3pShowError(error, { message: 'Erreur lors de l’initialisation' })
    })
  }

  // fonction qui teste si la réponse élève est égale à stor.solution, de manière formelle, et qui vérifie également que l’écriture est algébrique (un seul i)
  function analyseFormelle (reponseEleve) {
    const retour = {}
    let i
    let repEleveXcas = j3pMathquillXcas(reponseEleve.replace(/i/g, 'x'))
    repEleveXcas = j3pAjouteFoisDevantX(repEleveXcas, 'x')
    stor.liste.giveFormula2('rep', repEleveXcas)
    stor.liste.calculateNG()
    retour.bon = (stor.liste.valueOf('egalite') === 1)
    if (!retour.bon) {
      // La réponse équivalente à la réponse attendue, mais elle est peut-être exacte mais non simplifiée
      const arbreChaine = new Tarbre(repEleveXcas + '-(' + stor.solution.replace(/i/g, 'x') + ')', ['x'])
      let egalite = true
      for (i = 1; i <= 15; i++) {
        egalite = egalite && (Math.abs(arbreChaine.evalue([i])) < Math.pow(10, -12))
      }
      retour.bon = egalite
      const formeAlg = [
        /^-?[0-9]+[+-][0-9]*\*?x$/g,
        /^-?[0-9]*\*?x$/g,
        /^-?[0-9]+$/g,
        /^-?[0-9]*\*?x[+-][0-9]+$/g
      ]
      // s’arrête au premier truc qui vérifie, cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/some
      retour.simplifie = formeAlg.some(function (regexp) {
        return regexp.test(repEleveXcas)
      })
    } else {
      retour.simplifie = true
    }
    return retour
  }

  // le booléen est présent pour que la correction soit 'classique' dans le cas d’un pb lié au temps de réponse
  function maCorrection (reponseeleve, bool) {
    let unechaine = '<br>' + ds.textes.correction3
    const bilan = 'autre'
    if (bool && !reponseeleve.simplifie && reponseeleve.bon) {
      // ce n’est pas une forme algébrique (par exemple deux i dans la réponse) mais la réponse est exacte
      unechaine = ds.textes.correction2
    }
    if (bool && !reponseeleve.simplifie && !reponseeleve.bon) {
      // ce n’est pas une forme algébrique (par exemple deux i dans la réponse)
      unechaine = ds.textes.correction1
    }
    j3pMathsAjouteDans(stor.zoneExplications, 'correction', unechaine, { a: stor.solution })
    return bilan
  }

  function correctionDetaillee () {
    const tabLignes = [me.stockage[0], me.stockage[1], stor.solution]
    for (let i = 1; i <= 3; i++) {
      stor['zoneCorr' + i] = j3pAddElt(stor.zoneExplications, 'div', '')
      j3pAffiche(stor['zoneCorr' + i], '', '$z_1=' + tabLignes[i - 1] + '$')
    }
  }
  // Utilisée pour remettre en forme a+ib ou b+ia

  function formatAplusiB (a, b, Choix) {
    // Choix=1 donne a+ib et Choix=2 donne ib+a, avec les simplifications de signes.
    // En utilisant xcas, Normal(1-2x) écrit -2x+1, mais 1-2i si on utilise i à la place de x, car i vu comme complexe.
    const stock = (Choix === 1)
      ? simplifieEcritureDegre1(b, a, 2)
      : simplifieEcritureDegre1(b, a, 1)
    return remplaceXparI(stock)
  }

  function simplifieEcritureDegre1 (a, b, Choix) {
    // Cette fonction fait simplifier par xcas les écritures littéralles ax+b si Choix=1
    // et b+ax si Choix=2
    // du style 1x+2=x+2 ou 2x+0=2x etc
    const stock = (Choix === 1)
      ? j3pMonome(1, 1, a) + j3pMonome((a === 0) ? 1 : 2, 0, b)
      : j3pMonome(1, 0, b) + j3pMonome((b === 0) ? 1 : 2, 1, a)
    return enleveFois(stock)
  }

  function remplaceXparI (chaine) {
    // Cette fonction remplace tout les x par des i pour pouvoir faire appel aux simplifications xcas
    let stock = ''
    for (let i = 0; i < chaine.length; i++) {
      if (chaine.charAt(i) === 'x') { stock = stock + 'i' } else { stock = stock + chaine.charAt(i) }
    }
    return stock
  }

  function enleveFois (chaine) {
    return chaine.replace(/\*/g, '')
  }

  function enonceMain () {
    // On s’occupe de la question répétitive
    // On génère a et b non nuls. puis c et d avec c ou d éventuellement nul.
    let a, b, c
    do {
      const [borneInfa, borneSupa] = j3pGetBornesIntervalle(ds.a)
      const [borneInfb, borneSupb] = j3pGetBornesIntervalle(ds.b)
      a = j3pGetRandomInt(borneInfa, borneSupa)
      b = j3pGetRandomInt(borneInfb, borneSupb)
    } while (a * b === 0)

    do {
      const [borneInfc, borneSupc] = j3pGetBornesIntervalle(ds.c)
      c = j3pGetRandomInt(borneInfc, borneSupc)
    } while ((c === 0) || (c === 1) || (c === -1))

    // AplusIB=1 décide de a+ib et AplusIB=2 pour ib+a ainsi que CplusID
    // On affiche (a+ib)(c+id)  ou c(a+ib) ou id(a+ib) aux permutations de i près.
    stor.solution = formatAplusiB(-b * c, a * c, 1)
    // me.stockage[0]=formatAplusiB(a*c+b*d,a*d+b*c,1)
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '20px' }) }
    )
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    stor.zoneCons1.style.paddingBottom = '30px'
    j3pAffiche(stor.zoneCons1, '', ds.textes.question1)
    // On met en forme le produit :
    // On s’occupe de la question répétitive
    const sortequestion = j3pGetRandomInt(1, 2)

    let lasaisie = ''
    let lasaisie2
    switch (sortequestion) {
      case 1:
        lasaisie = formatAplusiB(a, b, 1)
        lasaisie = formatAplusiB(0, c, 1) + '(' + lasaisie + ')'
        // Pour la correction détaillée :
        lasaisie2 = c + 'i \\times '
        if (a >= 0) {
          lasaisie2 += a
        } else {
          lasaisie2 += '(' + a + ')'
        }
        if (c >= 0) {
          lasaisie2 += '+' + c + 'i \\times'
        } else {
          lasaisie2 += c + 'i \\times'
        }
        if (b >= 0) {
          lasaisie2 += b + 'i'
        } else {
          lasaisie2 += '(' + b + 'i)'
        }
        break
      case 2:
        lasaisie = formatAplusiB(a, b, 2)
        lasaisie = formatAplusiB(0, c, 1) + '(' + lasaisie + ')'
        // Pour la correction détaillée :
        lasaisie2 = c + 'i \\times '
        if (b >= 0) {
          lasaisie2 += 'i' + b
        } else {
          lasaisie2 += '(' + b + 'i)'
        }
        if (c >= 0) {
          lasaisie2 += '+' + c + 'i \\times'
        } else {
          lasaisie2 += c + 'i \\times'
        }
        if (a >= 0) {
          lasaisie2 += a
        } else {
          lasaisie2 += '(' + a + ')'
        }
        //   me.indication(me.zonesElts.IG,ds.indication2);
        break
    }
    j3pAffiche(stor.zoneCons2, '', '$z_1=' + lasaisie + '$')
    const elt = j3pAffiche(stor.zoneCons3, '', '$z_1=$&1&', { inputmq1: { texte: '' } })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d-+i')
    me.stockage[0] = lasaisie
    me.stockage[1] = lasaisie2
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneExplications = j3pAddElt(stor.conteneur, 'div', '<br>', { style: { color: me.styles.toutpetit.correction.color } })
    // Chargement de la figure mtg32
    stor.liste.giveFormula2('b', String(-b * c))
    stor.liste.giveFormula2('a', String(a * c))
    j3pFocus(stor.zoneInput)
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        me.surcharge()
        ds = me.donneesSection
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
        me.typederreurs = [0, 0, 0]
        me.afficheTitre(ds.textes.titreExo)
        if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
        depart()
      } else {
        me.videLesZones()
        enonceMain()
      }
      // il ne faut plus mettre de code ici car depart est async !
      break // case "enonce":

    case 'correction': {
      const repEleve = $(stor.zoneInput).mathquill('latex')
      if ((repEleve === '') && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        j3pFocus(stor.zoneInput)
        me.finCorrection()
      } else { // une réponse a été saisie
        const reponseAnalysee = analyseFormelle(repEleve)
        if (reponseAnalysee.bon && reponseAnalysee.simplifie) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          stor.zoneInput.style.color = me.styles.cbien
          j3pDesactive(stor.zoneInput)
          j3pFreezeElt(stor.zoneInput)
          correctionDetaillee('juste')
          me.typederreurs[0]++
          me.finCorrection('navigation', true)
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          // à cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            maCorrection(reponseAnalysee, false)
            me.finCorrection('navigation', true)
          } else {
            // réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            // S’il y a deux chances,on appelle à nouveau le bouton Valider
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML = cFaux
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              j3pFocus(stor.zoneInput)
              me.finCorrection() // on reste dans l’état correction
            } else {
              // Erreur au second essai
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              maCorrection(reponseAnalysee, true)
              correctionDetaillee('faux')
              stor.zoneInput.style.color = me.styles.cfaux
              j3pDesactive(stor.zoneInput)
              j3pBarre(stor.zoneInput)
              me.finCorrection('navigation', true)
            }
          }
        }
      }
      break
    }
    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
