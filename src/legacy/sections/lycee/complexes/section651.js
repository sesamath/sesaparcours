import { j3pAddElt, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'

/**
 * Cours : Définition des parties réelle et imaginaires
 * @fileOverview
 */

export const params = { outils: ['calculatrice', 'mathquill'] }

const titreExo = 'Partie réelle et partie imaginaire'

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this

  function enonceThen (content) {
    const divCours = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    for (const ligne of content.split(/(\n|<br *\/?>)/)) {
      const div = j3pAddElt(divCours, 'div')
      j3pAffiche(div, '', ligne)
    }
    me.finEnonce()
  }

  if (this.etat === 'enonce') {
    // on a toujours une seule répétition, donc on est toujours au début de la section, inutile de tester
    this.setPassive()
    me.construitStructurePage('presentation2')
    me.afficheTitre(titreExo)
    j3pImporteAnnexe('cours/c_partiescomplexe.txt')
      .then(text => enonceThen(text))
      .catch(j3pShowError)
  }
  // section passive, pas de case correction ni navigation
}
