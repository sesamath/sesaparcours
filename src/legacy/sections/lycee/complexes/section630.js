import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pAjouteDans, j3pAjouteFoisDevantX, j3pBarre, j3pBoutonRadio, j3pCompareTab, j3pMonome, j3pShuffleMulti, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pShowError, j3pImporteAnnexe, j3pGetNewId } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitToutesLesFenetres } from 'src/legacy/core/functionsJqDialog'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/**
 * Savoir reconnaître l’équation d’un cercle
 * Reprendre le principe de l’Exercice 'Reconnaître une équation de droite' du site Mathenpoche :
 * 2G4, partie 'Droites et équations'.
 * en programmant plusieurs items (d’abord un qcm avec la reconnaissance ou non
 * de l’équation d’un cercle, et si oui, on demande les coordonnées du centre et du rayon),
 * prévoir 3 items préalables sur la forme canonique (pour la détermination des coordonnées du centre et du rayon)
 * Phrases d’état :
 * « Bien » : 70% au moins de réussite
 * « pas bien » sinon
 * @fileOverview
 */

// FIXME utiliser les méthodes de repere ou des fcts j3p pour construire ça
const figSvg = `<svg width="380" height="210">
<line x1="106.3" y1="90.7" x2="111.7" y2="85.3" style="stroke:black;stroke-width:2;" />
<line x1="106.3" y1="85.3" x2="111.7" y2="90.7" style="stroke:black;stroke-width:2;" />
<text x="111.7" y="85.3" style="fill:black; font-size:16pt;">O</text><circle cx="109" cy="88" r="81" style="stroke-width:2; stroke:blue;fill:none;"></circle><text x="136" y="115" style="fill:red; font-size:16pt;">R</text><line x1="163" y1="147.4" x2="109" y2="88" style="stroke:red;stroke-width:3;" />
<line x1="160.3" y1="171.7" x2="165.7" y2="166.3" style="stroke:black;stroke-width:0;" />
<line x1="160.3" y1="166.3" x2="165.7" y2="171.7" style="stroke:black;stroke-width:0;" />
<text x="165.7" y="166.3" style="fill:black; font-size:16pt;">M(x;y)</text><line x1="163" y1="147.4" x2="163" y2="115" style="stroke:black;stroke-width:1;stroke-dasharray:3,2;" />
<line x1="163" y1="147.4" x2="190" y2="147.4" style="stroke:black;stroke-width:1;stroke-dasharray:3,2;" />
<line x1="190" y1="88" x2="109" y2="88" style="stroke:black;stroke-width:1;stroke-dasharray:3,2;" />
<line x1="109" y1="115" x2="109" y2="88" style="stroke:black;stroke-width:1;stroke-dasharray:3,2;" />
<text x="109" y="128.5" style="fill:blue; font-size:14pt; font-style:italic;">a</text><text x="198.1" y="88" style="fill:blue; font-size:14pt; font-style:italic;">b</text><line x1="1" y1="115" x2="379" y2="115" style="stroke:black;stroke-width:2;"><line x1="190" y1="1" x2="190" y2="209" style="stroke:black;stroke-width:2;"><text x="181.9" y="125.8" style="fill:black; font-size:12pt;">0</text><line x1="163" y1="120.4" x2="163" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="136" y1="120.4" x2="136" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="109" y1="120.4" x2="109" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="82" y1="120.4" x2="82" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="55" y1="120.4" x2="55" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="28" y1="120.4" x2="28" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="1" y1="120.4" x2="1" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="217" y1="120.4" x2="217" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="244" y1="120.4" x2="244" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="271" y1="120.4" x2="271" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="298" y1="120.4" x2="298" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="325" y1="120.4" x2="325" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="352" y1="120.4" x2="352" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="379" y1="120.4" x2="379" y2="109.6" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="142" x2="195.4" y2="142" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="169" x2="195.4" y2="169" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="196" x2="195.4" y2="196" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="88" x2="195.4" y2="88" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="61" x2="195.4" y2="61" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="34" x2="195.4" y2="34" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="7" x2="195.4" y2="7" style="stroke:black;stroke-width:1;" />
</svg>`

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['aide', true, 'boolean', 'Aide de cours disponible sur l’équation d’un cercle en première étape'],
    ['nbchances', 2, 'entier', 'Nombre de chances pour les questions 2 et 3 (1 seule pour la première).']
  ]
}

const textes = {
  titre_exo: 'Reconnaissance d’une équation de cercle',
  question1: "Parmi les équations suivantes une seule est susceptible d'être une équation de cercle. Laquelle&nbsp;?",
  question2a: 'On va maintenant chercher à savoir si l’équation $£a$ est l’équation d’un cercle ou non.',
  question2b: 'Fais apparaitre dans le membre de gauche une somme de deux formes canoniques&nbsp;:',
  question3: 'L’équation $£a$ est donc équivalente à $£b = 0$.<br><br>Est-ce l’équation d’un cercle ? (si oui, préciser les coordonnées de son centre et son rayon)',
  question3b1: 'Abscisse du centre du cercle&nbsp;: &1&',
  question3b2: 'Ordonnée du centre du cercle&nbsp;: &1&',
  question3b3: 'Rayon du cercle&nbsp;: &1&',
  erreur1: 'La réponse n’est pas écrite sous forme canonique.',
  erreur2: 'La réponse est égale à l’expression attendue, mais n’est pas sous la bonne forme&nbsp;!',
  corrige2Init: 'L’équation $£a$ est équivalente à $£b$.',
  corrige2: 'C’est l’équation d’un cercle dont le centre a pour coordonnées ($£c$;$£d$) et pour rayon $£e$.',
  corrige3: 'Une somme de carrés étant positive dans $\\R$, ce n’est pas une équation d’un cercle mais de l’ensemble vide.',
  correction1_1: '$£a$ est l’équation réduite d’une droite.',
  correction1_2: '$£a$ est l’équation d’une droite parallèle à l’axe des ordonnées',
  correction1_3: '$£a$ est l’équation cartésienne d’une droite.',
  correction1_4: '$£a$ n’est pas l’équation d’un cercle à cause de la présence d’un terme en $xy$',
  brouillon: 'Tu peux utiliser la zone suivante comme brouillon :<br>(tu peux ajouter des lignes en cliquant sur le bouton suivant)',
  correction_detaille: 'Explications détaillées : $£a$ est la forme développée d’un polynôme $f$ de degré 2: $ax^2+bx+c$ avec $a=1$, $b=£b$ et $c=0$ et sa forme canonique est $a(x-\\alpha)^2+\\beta$ où $(\\alpha;\\beta)$ sont les coordonnées du sommet de la parabole représentant $f$. On a $\\alpha=-\\frac{b}{2a}=£c$ et $\\beta=f(\\alpha)=£d=£e$. On obtient donc $£f$ comme forme canonique. <br>De même, la forme canonique de $£g$ est $£h$.',
  //   correction1_5:"$£a$ n’est pas l’équation d’un cercle à cause de la présence d’un terme en $xy$'
  aide: 'Aide',
  aide2: 'Une aide',
  leBrouillon: 'Brouillon',
  oui: 'oui',
  non: 'non'
}

/**
 * section 630
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function depart () {
    // txt html de la figure
    const txtFig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAPIAAACugAAAQEAAAAAAAAAAQAAAAf#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAFiAAItM#####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQAgAAAAAAAAAAAACAP####8AAVIAATMAAAABQAgAAAAAAAD#####AAAAAQAJQ0ZvbmNOVmFyAP####8AAWYAESh4LWEpXjIrKHktYileMi1S#####wAAAAEACkNPcGVyYXRpb24BAAAABQD#####AAAAAQAKQ1B1aXNzYW5jZQAAAAUB#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAABAAAAAUAAAAAAAAAAAAAABgAAAAUBAAAABwAAAAEAAAAIAAAAAgAAAAFAAAAAAAAAAAAAAAgAAAADAAAAAgABeAABeQAAAAQA#####wADcmVwABEoeC0yKV4yKyh5KzMpXjItMgAAAAUBAAAABQAAAAAGAAAABQEAAAAHAAAAAAAAAAFAAAAAAAAAAAAAAAFAAAAAAAAAAAAAAAYAAAAFAAAAAAcAAAABAAAAAUAIAAAAAAAAAAAAAUAAAAAAAAAAAAAAAUAAAAAAAAAAAAAAAgABeAABef####8AAAADABBDVGVzdEVxdWl2YWxlbmNlAP####8AB2VnYWxpdGUAAAAEAAAABQEAAAAAAT#wAAAAAAAAAf###############w=='
    stor.mtgList = stor.mtgAppLecteur.createList(txtFig)
  }

  function verifEgalite (expres1, expres2) {
    // expres1 et expres2 sont des expressions à 2 variables x et y
    // cette fonction vérifie si ces deux expressions sont égales (pas nécessairement équivalentes au sens mtg32)
    const arbreEleve = new Tarbre(expres1, ['x', 'y'])
    const arbreSol = new Tarbre(expres2, ['x', 'y'])
    let bonneRep = true
    for (let i = 1; i <= 4; i++) {
      for (let j = 1; j <= 4; j++) {
        if (Math.abs(arbreEleve.evalue([i, j]) - arbreSol.evalue([i, j])) > Math.pow(10, -12)) bonneRep = false
      }
    }
    return bonneRep
  }

  /**
   * utile en item3 pour afficher/masquer les zones de saisies suivant la case du qcm cochée
   * @private
   */
  function afficherZones () {
    stor.zoneInput = []
    for (let i = 3; i <= 5; i++) {
      const elt = j3pAffiche(stor['zoneCons' + i], '', textes['question3b' + (i - 2)],
        { inputmq1: { texte: '' } }
      )
      stor.zoneInput.push(elt.inputmqList[0])
      mqRestriction(stor.zoneInput[i - 3], '\\d+-')
    }
    const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    j3pFocus(stor.zoneInput[0])
    stor.btnChecked = 1
  }

  function cacherZones () {
    for (let i = 3; i <= 5; i++) {
      j3pEmpty(stor['zoneCons' + i])
    }
    stor.btnChecked = 2
  }

  function chargePaletteBrouillon (zonetexte) {
    me.tabBtns = ['racine', 'fraction', 'puissance']
    j3pPaletteMathquill(stor.laPalette, zonetexte, { liste: me.tabBtns })
  }

  // permet d’afficher la bonne palette mathquill du brouillon, en fonction de celle qui est active :
  function changerBrouillon (zonetexte) {
    j3pEmpty(stor.laPalette)
    chargePaletteBrouillon(stor.eltBrouillon.inputmqList[0])
  }

  // Ce qui se passe quand on clique sur le bouton '+' du brouillon
  function ajouteEtape () {
    if (me.stockage[30] < 4) {
      me.stockage[30]++
      stor.eltBrouillon = j3pAffiche(stor.expression3, '', '&1&\n\n', {
        inputmq1: {
          texte: ''
        },
        style: { fontSize: '18px' }
      })
      // ptet inutile
      const zonetemp = me.stockage[30]
      // Pour que la bonne palette s’affiche au changement de zone
      stor.eltBrouillon.inputmqList[0].onmouseup = changerBrouillon.bind(null, zonetemp)
      chargePaletteBrouillon(stor.eltBrouillon.inputmqList[0])
      mqRestriction(stor.eltBrouillon.inputmqList[0], '\\d,.abcdefghijklmnopqrstuvwxyz+-*', { boundingContainer: j3pElement(stor.fenetreBrouillon) })
    }
  }

  function signe (nombre) {
    return (nombre < 0) ? ' - ' + Math.abs(nombre) : ' + ' + Math.abs(nombre)
  }

  function signeCoefx (nombre) {
    // nombre = Number(nombre);
    const absNb = Math.abs(nombre)
    if (nombre < 0) {
      return (Math.abs(nombre + 1) < Math.pow(10, -12)) ? ' - ' : ' - ' + absNb
    } else {
      return (Math.abs(nombre - 1) < Math.pow(10, -12)) ? ' + ' : ' + ' + absNb
    }
  }

  function ajoutePar (nombre) {
    if (nombre < 0) {
      return '(' + nombre + ')'
    } else {
      return nombre
    }
  }

  // La fonction suivante renvoit une équation au format latex, de droite si quest=1, 2 ou 3, de forme  x^2+bx+y^2+cy+dxy+e=0 si quest=4,
  // équation d’un cercle si quest=5 et équation ressemblant à celle d’un cercle mais menant à (x-a)^2+(y-b)^2=R avec R négatif une fois développée si quest=6.
  function choixEquation (quest) {
    let equation, a, b, c, d, R, coefX, coefY, coefa, coefc, coefb, ordOrig
    if (quest === 1) {
      // équation du style y=ax+b où a est un entier
      let coefDir
      do {
        coefDir = -7 + Math.floor(Math.random() * 15)
      } while (Math.abs(coefDir) < 1.5)
      do {
        ordOrig = -7 + Math.floor(Math.random() * 15)
      } while (Math.abs(ordOrig) < 0.5)
      do {
        coefb = -3 + Math.floor(Math.random() * 7)
      } while ((coefb > -0.5) && (coefb < 1.5))
      // coefa = -coefDir * coefb
      // coefc = ordOrig * coefb
      equation = '$y = ' + j3pMonome(1, 1, coefDir) + j3pMonome(2, 0, ordOrig) + '$' // '$y = ' + ecrireCoefx(coefDir) + 'x' + signe(ordOrig) + '$'
    } else if (quest === 2) {
      // équation du style x=c où c est un entier
      c = -7 + Math.floor(Math.random() * 15)
      // équation du style ax+b=0
      do {
        coefa = -5 + Math.floor(Math.random() * 11)
      } while (Math.abs(coefa) < 1.5)
      // coefb = -coefa * c
      // modif alex :
      equation = '$x = ' + c + '$'
    } else if (quest === 3) {
      // l’équation sera du style ax+by=c

      const tabNum = [2, 1, 3, 4, 5, 7, -2, -1, -3, -4, -5, -7, 3, 6, -3, -6, 5, -5]
      const tabDen = [3, 3, 4, 3, 3, 4, 3, 3, 4, 3, 3, 4, 5, 5, 5, 5, 6, 6]

      const choixFrac = Math.floor(Math.random() * tabNum.length)
      const num1 = tabNum[choixFrac]
      const den1 = tabDen[choixFrac]
      tabNum.splice(choixFrac, 1)
      tabDen.splice(choixFrac, 1)
      do {
        ordOrig = -7 + Math.floor(Math.random() * 15)
      } while (Math.abs(ordOrig) < 0.5)
      coefa = -num1
      coefb = den1
      coefc = den1 * ordOrig
      equation = '$' + j3pMonome(1, 1, coefa) + j3pMonome(2, 1, coefb, 'y') + j3pMonome(3, 0, -coefc) + '= 0$'
      // fin code Rémi (flash 2G4s1ex6)
    } else if (quest === 4) {
      // l’équation sera du type x^2+bx+y^2+cy+dxy+e=0
      let e
      do {
        b = j3pGetRandomInt(-4, 4)
        c = j3pGetRandomInt(-4, 4)
        d = j3pGetRandomInt(-4, 4)
        e = j3pGetRandomInt(-4, 4)
      } while (b === 0 || c === 0 || d === 0 || e === 0)
      equation = '$x^2' + j3pMonome(2, 1, b) + j3pMonome(3, 2, 1, 'y') + j3pMonome(4, 1, c, 'y') + j3pMonome(5, 1, d, 'xy') + j3pMonome(6, 0, e) + '=0$'
    } else if (quest === 5) {
      // l’équation sera celle d’un cercle non vide : x^2-2ax+y^2-2by+d=0 qui equivaut à (x-a)^2+(y-b)^2=R2 avec d=a^2+b^2-R^2
      do {
        a = j3pGetRandomInt(-9, 9)
        b = j3pGetRandomInt(-9, 9)
        R = j3pGetRandomInt(1, 9)
      } while (a === 0 || b === 0 || a === b)
      // cercle non centré en O de préférence
      coefX = -2 * a
      coefY = -2 * b
      d = a * a + b * b - R * R
      // Dans l’équation qui suit, ne surtout pas virer d’espace à la fin = 0 car on trouve ensuite du substring(1, equation.length-3) pour juste avoir le membre de gauche
      equation = '$x^2' + j3pMonome(2, 1, coefX) + j3pMonome(3, 2, 1, 'y') + j3pMonome(4, 1, coefY, 'y') + j3pMonome(5, 0, d) + '= 0$'
      // ma réponse de la question 2 et 3 au format xcas :
      me.stockage[2] = '(x' + signe(-a) + ')^2+(y' + signe(-b) + ')^2' + signe(-R * R)
      // pour la correction détaillée de la question 3 :
      me.stockage[7] = '(x' + signe(-a) + ')^2+(y' + signe(-b) + ')^2 = ' + R + '^2'
      // Je ne sais pas pourquoi il y a des espaces, mais je les vire
      me.stockage[2] = me.stockage[2].replace(/\s/g, '')
      me.stockage[7] = me.stockage[7].replace(/\s/g, '')
      // pour la correction détaillée
      me.stockage[5] = {}
      me.stockage[5].etape2 = '(x' + signe(-a) + ')^2 - ' + (a * a) + ' + (y' + signe(-b) + ')^2 - ' + (b * b) + signe(d)
      // la réponse à la question 3 :
      me.storage.solution[3] = [true, false]
      // avec les bonnes réponses :
      me.storage.solution[4] = [a, b, R]
      // tout ce dont on a besoin en correction détaillée :
      me.stockage[5].a = 'x^2' + j3pMonome(2, 1, coefX)
      me.stockage[5].b = coefX
      me.stockage[5].c = a
      me.stockage[5].d = ajoutePar(a) + '^2' + signeCoefx(coefX) + '\\times' + ajoutePar(a)
      me.stockage[5].e = -a * a
      me.stockage[5].f = '(x' + signe(-a) + ')^2 - ' + (a * a)
      me.stockage[5].g = 'y^2' + j3pMonome(2, 1, coefY, 'y')
      me.stockage[5].h = '(y' + signe(-b) + ')^2 - ' + (b * b)
    } else if (quest === 6) {
      // l’équation sera une expression développée de (x-a)^2+(y-b)^2=R avec R négatif.
      do {
        a = j3pGetRandomInt(-9, 9)
        b = j3pGetRandomInt(-9, 9)
        R = j3pGetRandomInt(-9, -1)
        d = a * a + b * b - R
      } while (a === 0 || b === 0 || d === 0)
      coefX = -2 * a
      coefY = -2 * b
      // Dans l’équation qui suit, ne surtout pas virer d’espace à la fin = 0 car on trouve ensuite du substring(1, equation.length-3) pour juste avoir le membre de gauche
      equation = '$x^2' + j3pMonome(2, 1, coefX) + '+ y^2' + j3pMonome(4, 1, coefY, 'y') + j3pMonome(5, 0, d) + ' = 0$'
      // ma réponse de la question 2 et 3 au format xcas :
      me.stockage[2] = '(x' + signe(-a) + ')^2+(y' + signe(-b) + ')^2' + signe(-R)
      // pour la correction détaillée de la question 3 :
      me.stockage[7] = '(x' + signe(-a) + ')^2+(y' + signe(-b) + ')^2 = ' + signe(R)
      // Je ne sais pas pourquoi il y a des espaces, mais je les vire
      me.stockage[2] = me.stockage[2].replace(/\s/g, '')
      me.stockage[7] = me.stockage[7].replace(/\s/g, '')
      me.storage.solution[4] = [a, b, R]
      // pour la correction détaillée
      me.stockage[5] = {}
      me.stockage[5].etape2 = '(x' + signe(-a) + ')^2 - ' + (a * a) + '+ (y' + signe(-b) + ')^2 - ' + (b * b) + signe(d)
      // la réponse à la question 3 :
      me.storage.solution[3] = [false, true]
      // tout ce dont on a besoin en correction détaillée :
      me.stockage[5].a = 'x^2' + j3pMonome(2, 1, coefX)
      me.stockage[5].b = coefX
      me.stockage[5].c = a
      me.stockage[5].d = ajoutePar(a) + '^2' + signeCoefx(coefX) + '\\times' + ajoutePar(a)
      me.stockage[5].e = -a * a
      me.stockage[5].f = '(x' + signe(-a) + ')^2 - ' + (a * a)
      me.stockage[5].g = 'y^2' + j3pMonome(2, 1, coefY, 'y')
      me.stockage[5].h = '(y' + signe(-b) + ')^2 - ' + (b * b)
    }
    return equation
  }

  /**
   * Retourne true si l’on n’a pas deux formes canoniques ou encore des termes en x en dehors des deux parenthèses
   * @private
   * @param ch
   * @returns {boolean} true si y’a un pb de forme
   */
  function detectePbForme (ch) {
    // je vire d’abord les espaces :
    ch = ch.replace(/\s/g, '')
    // JE RECHERCHE UN PB DE FORME :
    // je detecte l’absence de deux parenthèses
    const nbParOuvrantes = []
    for (let i = 0; i <= ch.length; i++) {
      if (ch.substring(i, i + 6) === '\\left(') {
        nbParOuvrantes.push(i)
        i = i + 5
      }
    }
    const nbParFermantes = []
    for (let i = 0; i <= ch.length; i++) {
      if (ch.substring(i, i + 9) === '\\right)^2') {
        i = i + 8
        nbParFermantes.push(i)
      }
    }

    if (nbParFermantes.length !== 2 || nbParOuvrantes.length !== 2) {
      return true
    }

    // j’ai bien deux parenthèses (...)^2
    let chTemp = ''
    let chTempbis = ''

    chTemp = ch.slice(nbParOuvrantes[0], nbParFermantes[0] + 1)
    chTempbis = ch.slice(nbParOuvrantes[1], nbParFermantes[1] + 1)
    const chTemp2 = ch.split(chTemp)
    // il reste une (...)^2 dans chTemp2[1]
    const chTemp3 = chTemp2[1].split(chTempbis)
    const chTemp4 = []
    chTemp4[0] = chTemp2[0]
    chTemp4[1] = chTemp3[0]
    chTemp4[2] = chTemp3[1]
    if (chTemp2[0].length !== 0 && chTemp2[1].length !== 0) {
      // je ne veux pas d’éléments à droite et à gauche des parenthèses
      return true
    }
    // Je ne sais pas comment un utilisateur a pu obtenir chTemp4[2] undefined, mais ceci devrait contourner ce pb (je le fais sur tous les elts de chTemp)
    // De toute façon, s’il l’est, c’est que la réponse n’est pas sous la bonne forme
    chTemp4.forEach(elt => { if (!elt || elt.includes('x') || elt.includes('y')) return true })
    return false
  }

  function afficheCorrection (bonneReponse) {
    const explications = j3pAddElt(stor.divEnonce, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    let i
    j3pDetruitToutesLesFenetres()
    if ((me.questionCourante % ds.nbetapes) === 1) {
      for (let k = 1; k <= 4; k++) {
        me.phraseCorr = j3pAddElt(explications, 'div')
        if (me.stockage[4][k - 1] === 'droite_reduite') {
          j3pAffiche(me.phraseCorr, 'zonecor', textes.correction1_1, { a: me.stockage[3][k - 1] })
        }
        if (me.stockage[4][k - 1] === 'droite_verticale') {
          j3pAffiche(me.phraseCorr, 'zonecor2', textes.correction1_2, { a: me.stockage[3][k - 1] })
        }
        if (me.stockage[4][k - 1] === 'droite_cartesienne') {
          j3pAffiche(me.phraseCorr, 'zonecor3', textes.correction1_3, { a: me.stockage[3][k - 1] })
        }
        if (me.stockage[4][k - 1] === 'xy') {
          j3pAffiche(me.phraseCorr, 'zonecor4', textes.correction1_4, { a: me.stockage[3][k - 1] })
        }
      }
    }
    if ((me.questionCourante % ds.nbetapes) === 2) {
      j3pDetruit(stor.zoneCons4)
      j3pDetruit(me.zoneBtn)
      for (i = 1; i <= 4; i++) stor['zoneCorr' + i] = j3pAddElt(explications, 'div')

      j3pAffiche(stor.zoneCorr1, 'zonecor1', '$' + me.stockage[1].substring(0, me.stockage[1].length - 3) + ' = ' + me.stockage[5].etape2 + '$')
      j3pAffiche(stor.zoneCorr2, 'zonecor2', '$' + me.stockage[1].substring(0, me.stockage[1].length - 3) + ' = ' + me.stockage[2] + '$')
      me.logIfDebug('textes.correction_detaille=' + textes.correction_detaille + ' me.stockage[5].a=' + me.stockage[5].a + ' me.stockage[5].b=' + me.stockage[5].b + ' me.stockage[5].c=' + me.stockage[5].c + ' me.stockage[5].d=' + me.stockage[5].d + ' me.stockage[5].e=' + me.stockage[5].e + ' me.stockage[5].f=' + me.stockage[5].f + ' me.stockage[5].g=' + me.stockage[5].g + ' me.stockage[5].h=' + me.stockage[5].h)
      // ajout des explications complémentaires sur les formes canoniques :
      stor.zoneCorr4.style.paddingTop = '10px'
      j3pAffiche(stor.zoneCorr4, 'zonecor4', textes.correction_detaille, {
        a: me.stockage[5].a,
        b: me.stockage[5].b,
        c: me.stockage[5].c,
        d: me.stockage[5].d,
        e: me.stockage[5].e,
        f: me.stockage[5].f,
        g: me.stockage[5].g,
        h: me.stockage[5].h
      })
    }
    if ((me.questionCourante % ds.nbetapes) === 0) {
      for (i = 1; i <= 2; i++) stor['zoneCorr' + i] = j3pAddElt(explications, 'div')
      j3pAffiche(stor.zoneCorr1, 'corrige1', textes.corrige2Init, {
        a: me.stockage[2],
        b: me.stockage[7]
      })
      const phraseCorr2 = (me.stockage[4][0] === 'cercle') ? textes.corrige2 : textes.corrige3
      j3pAffiche(stor.zoneCorr2, 'corrige2', phraseCorr2, {
        c: me.storage.solution[4][0],
        d: me.storage.solution[4][1],
        e: me.storage.solution[4][2]
      })
    }
  }

  function enonceInit () {
    // on impose toujours 3 étapes
    me.surcharge({ nbetapes: 3 })
    me.construitStructurePage('presentation1')
    // Le nombre de chances ne peut être surchargé que pour les questions 2 et 3, pas pour la première
    // dans les fait, pour la question1, on ne prend pas en compte ce nbchances : jamais n’est donnée une deuxième chance
    stor.nbchancesInit = ds.nbchances
    me.stockage = [0, 0, 0, 0]
    me.storage.solution = []
    me.typederreurs = [0, 0, 0, 0, 0, 0]
    me.afficheTitre(textes.titre_exo)
    // on charge le premier fichier
    j3pImporteAnnexe('cours/c_equationcercle_1.html').then(htmlContent => {
      stor.cercle1 = htmlContent
      // puis le 2e
      return j3pImporteAnnexe('cours/c_equationcercle_2.html')
    }).then(htmlContent => {
      stor.cercle2 = htmlContent
      // et on peut continuer avec le chargement de mathgraph
      return getMtgCore({ withMathjax: true })
    }).then(
      // success
      (mtgAppLecteur) => {
        stor.mtgAppLecteur = mtgAppLecteur
        enonceThen()
      },
      // failure
      (error) => j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
    ).catch(error => {
      j3pShowError(error, { message: 'Impossible de charger les données de cette exercice' })
    })
  }

  function enonceThen () {
    let tabProp
    let i
    stor.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Première étape d’une répétition DANS LE CAS où y a plus de 2 répétitions
    if ((me.questionCourante % ds.nbetapes) === 1) {
      ds.nbchances = 1
      // stockage4 servira pour la correction détaillée
      me.stockage[4] = []
      let equation2, equation3
      // Tirage pour avoir les deux cas de figure (cercle vide et cercle non vide) en cas de répétitions >=2
      if (me.questionCourante === 1) {
        me.stockage[10] = j3pGetRandomInt(5, 6)
      } else if (me.questionCourante === 4) {
        me.stockage[10] = (me.stockage[10] - 4) % 2 + 5
      } else {
        // après c’est aléatoire
        me.stockage[10] = j3pGetRandomInt(5, 6)
      }
      me.stockage[4][0] = (me.stockage[10] === 5) ? 'cercle' : 'vide'
      const equation1 = choixEquation(me.stockage[10])
      // je stocke pour les questions suivantes, en enlevant les dollars déjà présents dans la consigne :
      me.stockage[1] = equation1.substring(1, equation1.length - 1)
      const alea = j3pGetRandomInt(1, 3)
      if (alea === 1) {
        equation2 = choixEquation(1)
        equation3 = choixEquation(2)
        me.stockage[4][1] = 'droite_reduite'
        me.stockage[4][2] = 'droite_verticale'
      }
      if (alea === 2) {
        equation2 = choixEquation(2)
        equation3 = choixEquation(3)
        me.stockage[4][1] = 'droite_verticale'
        me.stockage[4][2] = 'droite_cartesienne'
      }
      if (alea === 3) {
        equation2 = choixEquation(1)
        equation3 = choixEquation(3)
        me.stockage[4][1] = 'droite_reduite'
        me.stockage[4][2] = 'droite_cartesienne'
      }
      const equation4 = choixEquation(4)
      me.stockage[4][3] = 'xy'
      tabProp = [equation1, equation2, equation3, equation4]
      j3pAffiche(stor.divEnonce, '', textes.question1)
      // stockage3 servira pour la correction détaillée
      me.stockage[3] = []
      me.stockage[3] = [equation1.substring(1, equation1.length - 1), equation2.substring(1, equation2.length - 1), equation3.substring(1, equation3.length - 1), equation4.substring(1, equation4.length - 1)]

      // je dérange les tableaux de la même manière
      const tabs = j3pShuffleMulti(tabProp, [true, false, false, false])
      tabProp = tabs[0]
      me.storage.solution[1] = tabs[1]

      // C’est là que je construis la liste des réponses
      // je vais stocker le nom des cases à cocher
      stor.nomInput = []
      stor.idsRadio = []
      stor.nameRadio = j3pGetNewId('choix')
      for (i = 0; i < tabProp.length; i++) {
        stor['q1choix' + i] = j3pAddElt(stor.divEnonce, 'div', '', { style: { padding: '8px 0' } })
        stor.idsRadio.push('radio' + (i + 1))
        j3pBoutonRadio(stor['q1choix' + i], stor.idsRadio[i], stor.nameRadio, i + 1, '')
        j3pAffiche('label' + stor.idsRadio[i], '', tabProp[i])
      }
      if (ds.aide) {
        stor.zoneAide = j3pAddElt(stor.zoneD, 'div', '')
        stor.fenetreAide = j3pGetNewId('fenetreAide')
        me.fenetresjq = [{ name: 'Aide', title: textes.aide2, width: 450, left: 50, top: 50, id: stor.fenetreAide }]
        j3pCreeFenetres(me)
        j3pAffiche(stor.fenetreAide, '', stor.cercle1)
        j3pAjouteDans(stor.fenetreAide, figSvg)
        j3pAffiche(stor.fenetreAide, '', stor.cercle2)

        j3pAjouteBouton(stor.zoneAide, 'Aide', 'MepBoutons', textes.aide, j3pToggleFenetres.bind(null, 'Aide'))
        j3pAfficheCroixFenetres('Aide')
      }
    }

    // Deuxième étape d’une répétition
    if ((me.questionCourante % ds.nbetapes) === 2) {
      ds.nbchances = stor.nbchancesInit
      for (i = 1; i <= 4; i++) {
        stor['zoneCons' + i] = j3pAddElt(stor.divEnonce, 'div')
      }
      j3pAffiche(stor.zoneCons1, '', textes.question2a, { a: me.stockage[1] })
      j3pAffiche(stor.zoneCons2, '', textes.question2b)
      stor.eltQ2 = j3pAffiche(stor.zoneCons3, '', '$£a = $&1&', {
        inputmq1: { texte: '' },
        a: me.stockage[1].substring(0, me.stockage[1].length - 3)
      })
      me.zoneBtn = j3pAddElt(stor.zoneD, 'div', '')
      stor.fenetreBrouillon = j3pGetNewId('fenetreBrouillon')
      me.fenetresjq = [{ name: 'Brouillon', title: textes.leBrouillon, width: 450, left: 300, top: 100, id: stor.fenetreBrouillon }]
      j3pCreeFenetres(me)
      j3pAffiche(stor.fenetreBrouillon, '', textes.brouillon)
      stor.expression2 = j3pAddElt(stor.fenetreBrouillon, 'div')
      stor.btnPlus = j3pGetNewId('EtapePlus')
      me.stockage[30] = 0
      j3pAjouteBouton(stor.expression2, stor.btnPlus, '', '+', ajouteEtape)
      j3pAffiche(stor.expression2, '', '\n', {})
      stor.expression3 = j3pAddElt(stor.fenetreBrouillon, 'div')
      stor.laPalette = j3pAddElt(stor.fenetreBrouillon, 'div', '', { style: me.styles.etendre('petit.enonce', { paddingTop: '15px' }) })
      j3pAjouteBouton(me.zoneBtn, 'Brouillon', 'MepBoutons', textes.leBrouillon, j3pToggleFenetres.bind(null, 'Brouillon'))
      j3pAfficheCroixFenetres('Brouillon')

      // La palette
      stor.zoneCons4.style.paddingTop = '10px'
      stor.zoneCons4.style.paddingBottom = '20px'
      j3pPaletteMathquill(stor.zoneCons4, stor.eltQ2.inputmqList[0], {
        liste: ['racine', 'fraction', 'puissance']
      })
      // Chargement de la figure mtg32
      depart()
      stor.mtgList.giveFormula2('a', String(me.storage.solution[4][0]))
      stor.mtgList.giveFormula2('b', String(me.storage.solution[4][1]))
      if (me.stockage[4][0] === 'cercle') {
        stor.mtgList.giveFormula2('R', String(Math.pow(me.storage.solution[4][2], 2)))
      } else {
        stor.mtgList.giveFormula2('R', String(me.storage.solution[4][2]))
      }
      stor.mtgList.calculateNG()
      mqRestriction(stor.eltQ2.inputmqList[0], '\\d+-^xy*/()', {
        commandes: ['racine', 'fraction', 'puissance']
      })
      j3pFocus(stor.eltQ2.inputmqList[0])
    }

    // Troisième étape d’une répétition ou première étape si il n’y en a qu’une
    // trois étape dans cet exemple, donc il faut mettre ==0
    if ((me.questionCourante % ds.nbetapes) === 0) {
      // désigne le nb d’essais (2 essais possibles dans le cas où il y a un pb de forme et que la réponse est juste)
      me.stockage[11] = 0
      for (i = 1; i <= 5; i++) {
        stor['zoneCons' + i] = j3pAddElt(stor.divEnonce, 'div')
      }
      j3pAffiche(stor.zoneCons1, '', textes.question3, {
        a: me.stockage[1],
        b: me.stockage[2]
      })
      const espacetxt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
      stor.idsRadio = []
      ;[1, 2].forEach(i => stor.idsRadio.push(j3pGetNewId('radio' + i)))
      stor.nameRadio = j3pGetNewId('choix')
      j3pBoutonRadio(stor.zoneCons2, stor.idsRadio[0], stor.nameRadio, 0, textes.oui)
      j3pAffiche(stor.zoneCons2, '', espacetxt + espacetxt)
      j3pBoutonRadio(stor.zoneCons2, stor.idsRadio[1], stor.nameRadio, 1, textes.non)
      j3pElement(stor.idsRadio[0]).onclick = afficherZones
      j3pElement(stor.idsRadio[1]).onclick = cacherZones
      stor.btnChecked = 0
    }

    // zone accueillant les explications

    if ((me.questionCourante % ds.nbetapes) === 1 || (me.questionCourante % ds.nbetapes) === 0) {
      stor.zoneExpli = j3pAddElt(stor.divEnonce, 'div')
    }
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '')
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        me.videLesZones()
        enonceThen()
      }
      break // case "enonce":

    case 'correction': {
      let reponse = { aRepondu: false, bonneReponse: false }
      let reponseEleve
      if ((me.questionCourante % ds.nbetapes) === 1) {
        // le qcm
        reponseEleve = []
        reponse.aRepondu = false
        let numChecked = 0
        for (let k = 0; k < me.storage.solution[1].length; k++) {
          reponseEleve[k] = j3pElement(stor.idsRadio[k]).checked
          if (reponseEleve[k]) {
            reponse.aRepondu = true
            numChecked = k + 1
          }
        }
        if (reponse.aRepondu) {
          if (j3pCompareTab(me.storage.solution[1], reponseEleve)) {
            reponse.bonneReponse = true
            j3pElement('label' + stor.idsRadio[numChecked - 1]).style.color = me.styles.cbien
          } else {
            const numCorrect = me.storage.solution[1].indexOf(true)
            j3pElement('label' + stor.idsRadio[numChecked - 1]).style.color = me.styles.cfaux
            j3pElement('label' + stor.idsRadio[numCorrect]).style.color = me.styles.toutpetit.correction.color
          }
          for (let k = 1; k <= 4; k++) j3pDesactive(stor.idsRadio[k - 1])
          if (!reponse.bonneReponse) j3pBarre('label' + stor.idsRadio[numChecked - 1])
          j3pDetruit(stor.zoneAide)
        }
      }
      let egaliteFcts
      if ((me.questionCourante % ds.nbetapes) === 2) {
        // ecriture comme somme de deux FC
        // récupération de la réponse de l’élève au format MQ:
        reponseEleve = $(stor.eltQ2.inputmqList[0]).mathquill('latex')
        reponse.aRepondu = (reponseEleve !== '')
        if (reponse.aRepondu) {
          // A l’aide de la réponse, on détecte un éventuel pb
          stor.hasFormPb = detectePbForme(reponseEleve)
          // Appel à la fonction externe qui transforme la réponse au format Mathquill (latex) au format XCAS
          reponseEleve = j3pMathquillXcas(reponseEleve)
          reponseEleve = j3pAjouteFoisDevantX(reponseEleve, 'x')
          reponseEleve = j3pAjouteFoisDevantX(reponseEleve, 'y')
          stor.mtgList.giveFormula2('rep', reponseEleve)
          stor.mtgList.calculateNG()
          reponse.bonneReponse = stor.mtgList.valueOf('egalite') === 1
          if (!reponse.bonneReponse) {
            // on vérifie s’il y a égalité entre l’expression donnée et celle attendue (mais non écrite avec des formes canoniques
            // la réponse attendue est dans me.stockage[2]
            egaliteFcts = verifEgalite(reponseEleve, me.stockage[2])
          }
          stor.eltQ2.inputmqList[0].style.color = (reponse.bonneReponse) ? me.styles.cbien : me.styles.cfaux
        }
      }
      let caseBienCochee = false
      if ((me.questionCourante % ds.nbetapes) === 0) {
        reponse.aRepondu = (stor.btnChecked > 0)
        if (reponse.aRepondu) {
          if (stor.btnChecked === 1) {
            // Il a répondu 'Oui'
            if (me.stockage[4][0] === 'cercle') {
              caseBienCochee = true
              // c’est bien un cercle, on vérifie toutes les zones
              for (let k = 3; k <= 5; k++) {
                stor.zoneInput[k - 3].typeReponse = ['nombre', 'exact']
              }
              stor.zoneInput[0].reponse = [me.storage.solution[4][0]]
              stor.zoneInput[1].reponse = [me.storage.solution[4][1]]
              stor.zoneInput[2].reponse = [me.storage.solution[4][2]]
              reponse = stor.fctsValid.validationGlobale()
            } else {
              reponse.bonneReponse = false
              for (let k = 0; k < 3; k++) {
                stor.fctsValid.zones.bonneReponse[k] = false
                stor.fctsValid.coloreUneZone(stor.zoneInput[k].id)
              }
            }
          } else {
            if (me.stockage[4][0] !== 'cercle') caseBienCochee = true
            reponse.bonneReponse = caseBienCochee
          }
        }
      }

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        return me.finCorrection() // on reste en correction
      }

      // Une réponse a été saisie
      if (reponse.bonneReponse) {
        // Bonne réponse
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        afficheCorrection(true)
        if ((me.questionCourante % ds.nbetapes) === 2) j3pDesactive(stor.eltQ2.inputmqList[0])
        if ((me.questionCourante % ds.nbetapes) === 0) {
          for (let k = 1; k <= 2; k++) j3pDesactive(stor.idsRadio[k - 1])
          j3pElement('label' + stor.idsRadio[stor.btnChecked - 1]).style.color = me.styles.cbien
        }
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      if (me.isElapsed) {
        // A cause de la limite de temps :
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(false)
        if ((me.questionCourante % ds.nbetapes) === 2) {
          j3pDesactive(stor.eltQ2.inputmqList[0])
          j3pFreezeElt(stor.eltQ2.inputmqList[0])
        }
        if ((me.questionCourante % ds.nbetapes) === 0) {
          for (let k = 1; k <= 2; k++) j3pDesactive(stor.idsRadio[k])
        }
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      stor.zoneCorr.innerHTML = cFaux
      if (me.questionCourante % ds.nbetapes === 2) {
        if (stor.hasFormPb) stor.zoneCorr.innerHTML += '<br/>' + textes.erreur1
        if (egaliteFcts) stor.zoneCorr.innerHTML += '<br/>' + textes.erreur2
      }
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // ici il a encore la possibilité de se corriger, on reste en état correction
        return me.finCorrection()
      }

      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      if ((me.questionCourante % ds.nbetapes) === 0) {
        for (let k = 1; k <= 2; k++) j3pDesactive(stor.idsRadio[k - 1])
        if (caseBienCochee) {
          j3pElement('label' + stor.idsRadio[stor.btnChecked - 1]).style.color = me.styles.cbien
        } else {
          j3pElement('label' + stor.idsRadio[stor.btnChecked - 1]).style.color = me.styles.cfaux
          j3pBarre('label' + stor.idsRadio[stor.btnChecked - 1])
        }
      }
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(false)
      if ((me.questionCourante % ds.nbetapes) === 2) {
        if (stor.hasFormPb) {
          me.typederreurs[1]++
        } else {
          me.typederreurs[2]++
        }
        j3pDesactive(stor.eltQ2.inputmqList[0])
        j3pBarre(stor.eltQ2.inputmqList[0])
      } else {
        me.typederreurs[2]++
      }
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      me.finNavigation(true)
      break // case "navigation":
  }
}
