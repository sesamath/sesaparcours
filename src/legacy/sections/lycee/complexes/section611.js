import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pElement, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
 * QCM sur la forme algébrique de (a+ib)(a-ib), section possible de remediation de la section602Quot
 * Développeur : Alexis Lecomte, juillet 2012
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 10, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['a', '[-5;5]', 'string', 'coefficient a de (a-ib)(a+ib)'],
    ['b', '[-5;5]', 'string', 'coefficient b de (a-ib)(a+ib)'],
    ['niveau', 'facile', 'string', 'niveau facile (que (a+ib)(a-ib) à l’ordre près) ou difficile (en plus, de manière aléatoire, (ib+a)(ib-a) ou (a-ib)(ib+a) etc.)']
  ]
  // dans le cas d’une section QUALITATIVE

}
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 5,
      nbetapes: 1,
      // ordre aléatoire des réponses :
      chamboule: true,
      limite: '',
      a: '[-5;5]',
      b: '[-5;5]',
      textes: {
        titreExo: 'Produit d’un complexe par son conjugué',
        question: 'Sélectionne la forme algébrique du produit $£k$',
        correction: 'En effet :<br>'
      },
      // question: "Sélectionne la forme algébrique du produit $£k$ :";
      // niveau facile (que (a+ib)(a-ib) à l’ordre près) ou difficile (en plus, de manière aléatoire, (ib+a)(ib-a) ou (a-ib)(ib+a) etc.)
      niveau: 'facile',
      affichage_correction: true,
      structure: 'presentation1',
      indication: ''
    }
  }
  function derange (tab) {
    // Si mon tab a pour longueur n la fonction derange stocke dans stockage[3] le tableau [0,... , n-1] dans le désordre si la variable chamboule est à true
    // stockage[4] contient la permutation inverse.
    me.stockage[3] = []
    me.stockage[4] = []
    if (me.donneesSection.chamboule) {
      for (let i = 0; i <= tab.length - 1; i++) {
        me.stockage[3][i] = j3pGetRandomInt(0, (tab.length - 1))
        for (let j = 0; j < i; j++) {
          if (me.stockage[3][i] === me.stockage[3][j]) {
            i--
          }
        }
      }
      // détermination de la permutation inverse
      for (let i = 0; i <= tab.length - 1; i++) {
        me.stockage[4][me.stockage[3][i]] = i
      }
    } else {
      for (let i = 0; i <= tab.length - 1; i++) {
        me.stockage[3][i] = i
        me.stockage[4][i] = i
      }
    }
  }
  // Utilisée pour remettre en forme a+ib ou b+ia
  function GestionAPlusiB (a, b, Choix) {
    // Choix=1 donne a+ib et Choix=2 donne ib+a, avec les simplifications de signes.
    // console.log("a="+a+" et b="+b)
    let Stock = ''
    switch (Choix) {
      case 1:
        Stock = SimplifierEcritureDegre1(a, b, 2)
        break
      case 2:
        Stock = SimplifierEcritureDegre1(a, b, 1)
    }
    // Stock=RemplaceXparI(Stock);
    // console.log(Stock)
    return Stock
  }
  function SimplifierEcritureDegre1 (a, b, Choix) {
    // Cette fonction fait simplifier  les écritures littéralles ax+b si Choix=1
    // et b+ax si Choix=2
    // du style 1x+2=x+2 ou 2x+0=2x etc
    let Stock = ''
    switch (Choix) {
      case 2:
        // a+ib
        // Stock=xcasRequete(a+"x+"+b);
        if (a === 0) {
          if (b === 1) { Stock = 'i' }
          if (b === -1) { Stock = '-i' }
          if ((b !== 1) && (b !== -1)) { Stock = b + 'i' }
        } else {
          if (b === 0) {
            Stock = a + ''
          } else {
            // les deux non nuls
            if (b > 0) {
              if (b === 1) { Stock = a + '+i' }
              if (b !== 1) { Stock = a + '+' + b + 'i' }
            } else {
              if (b === -1) { Stock = a + '-i' }
              if (b !== -1) { Stock = a + '' + b + 'i' }
            }
          }
        }

        break
      case 1:
        // Stock=xcasRequete(b+"+"+a+"x");
        if (a === 0) {
          if (b === 1) { Stock = 'i' }
          if (b === -1) { Stock = '-i' }
          if ((b !== 1) && (b !== -1)) { Stock = b + 'i' }
        } else {
          if (b === 0) {
            Stock = a + ''
          } else {
            // les deux non nuls
            if (b > 0) {
              if ((b === 1) && (a > 0)) { Stock = 'i+' + a }
              if ((b === 1) && (a < 0)) { Stock = 'i' + a }
              if ((b !== 1) && (a > 0)) { Stock = b + 'i+' + a }
              if ((b !== 1) && (a < 0)) { Stock = b + 'i' + a }
            } else {
              if ((b === -1) && (a > 0)) { Stock = '-i+' + a }
              if ((b === -1) && (a < 0)) { Stock = '-i' + a }
              if ((b !== -1) && (a > 0)) { Stock = b + 'i+' + a }
              if ((b !== -1) && (a < 0)) { Stock = b + 'i' + a }
            }
          }
        }
        break
    }
    return Stock
  }

  function afficheCorrection (bonneReponse) {
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    if (me.donneesSection.niveau === 'difficile' && me.stockage[9] === 'oui') {
      j3pAffiche(zoneExpli1, '', '$£k$ = ' + '$£n$ = ' + '$£l$ = ' + '$£m$ = ' + me.storage.solution, { k: me.stockage[5], l: me.stockage[6], m: me.stockage[7], n: me.stockage[8] })
    } else {
      j3pAffiche(zoneExpli1, '', '$£k$ = ' + '$£l$ = ' + '$£m$ = ' + me.storage.solution, { k: me.stockage[5], l: me.stockage[6], m: me.stockage[7] })
    }
  }
  function enonceMain () {
    let a, b
    do {
      const [borneInfa, borneSupa] = j3pGetBornesIntervalle(ds.a)
      const [borneInfb, borneSupb] = j3pGetBornesIntervalle(ds.b)
      a = j3pGetRandomInt(borneInfa, borneSupa)
      b = j3pGetRandomInt(borneInfb, borneSupb)
    } while ((a === b) || (a === -b) || (b === 0) || (a === 0))
    // A MODIFIER :
    // me.stockage=[-b*c,a*c,sortequestion];

    let lasaisie = ''
    let corrige1
    let sortequestion
    if (ds.niveau === 'facile') {
      sortequestion = j3pGetRandomInt(1, 2)
    } else {
    // présentation aléatoire
      sortequestion = j3pGetRandomInt(3, 14)
    }
    if (a < 0) {
      corrige1 = '(' + a + ')^2'
    } else {
      corrige1 = a + '^2'
    }
    if (Math.abs(b) === 1) {
      corrige1 += ' - i^2'
    } else {
      corrige1 += ' - (' + Math.abs(b) + 'i)^2'
    }
    const corrige2 = a * a + ' + ' + b * b
    // pour affichage d’un terme de plus dans la correction
    me.stockage[9] = 'oui'
    switch (sortequestion) {
    // niveau facile
      case 1:
        lasaisie = '(' + GestionAPlusiB(a, b, 1) + ')(' + GestionAPlusiB(a, -b, 1) + ')'
        break
      case 2:
        lasaisie = '(' + GestionAPlusiB(a, -b, 1) + ')(' + GestionAPlusiB(a, b, 1) + ')'
        break
        // niveau difficile
      case 3:
        lasaisie = '(' + GestionAPlusiB(a, b, 2) + ')(' + GestionAPlusiB(a, -b, 2) + ')'
        break
      case 4:
        lasaisie = '(' + GestionAPlusiB(a, b, 2) + ')(' + GestionAPlusiB(a, -b, 1) + ')'
        break
      case 5:
        lasaisie = '(' + GestionAPlusiB(a, -b, 2) + ')(' + GestionAPlusiB(a, b, 2) + ')'
        break
      case 6:
        lasaisie = '(' + GestionAPlusiB(a, -b, 2) + ')(' + GestionAPlusiB(a, b, 1) + ')'
        break
      case 7:
        lasaisie = '(' + GestionAPlusiB(a, b, 2) + ')(' + GestionAPlusiB(a, -b, 2) + ')'
        break
      case 8:
        lasaisie = '(' + GestionAPlusiB(a, b, 2) + ')(' + GestionAPlusiB(a, -b, 1) + ')'
        break
      case 9:
        lasaisie = '(' + GestionAPlusiB(a, -b, 2) + ')(' + GestionAPlusiB(a, b, 2) + ')'
        break
      case 10:
        lasaisie = '(' + GestionAPlusiB(a, -b, 2) + ')(' + GestionAPlusiB(a, b, 1) + ')'
        break
      case 11:
        lasaisie = '(' + GestionAPlusiB(a, b, 1) + ')(' + GestionAPlusiB(a, -b, 2) + ')'
        break
      case 12:
        me.stockage[9] = 'non'
        lasaisie = '(' + GestionAPlusiB(a, b, 1) + ')(' + GestionAPlusiB(a, -b, 1) + ')'
        break
      case 13:
        lasaisie = '(' + GestionAPlusiB(a, -b, 1) + ')(' + GestionAPlusiB(a, b, 2) + ')'
        break
      case 14:
        me.stockage[9] = 'non'
        lasaisie = '(' + GestionAPlusiB(a, -b, 1) + ')(' + GestionAPlusiB(a, b, 1) + ')'
        break
    }
    // utiles en correction :
    me.stockage[8] = '(' + GestionAPlusiB(a, b, 1) + ')(' + GestionAPlusiB(a, -b, 1) + ')'
    me.stockage[5] = lasaisie
    me.stockage[6] = corrige1
    me.stockage[7] = corrige2
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.question, { k: lasaisie })
    const tabRep = ['$' + (a * a + b * b) + '$', '$' + (a * a - b * b) + '$', '$' + (-a * a - b * b) + '$', '$' + (-a * a + b * b) + '$', '$' + (a * a + b * b) + ' + ' + 2 * Math.abs(a) * Math.abs(b) + 'i$', '$' + (a * a + b * b) + ' - ' + 2 * Math.abs(a) * Math.abs(b) + 'i$']
    // je dérange le tableau (en fait il n’est pas modifié mais on détermine une permutation (et son inverse)
    derange(tabRep)
    // je garde la solution pour la correction
    me.storage.solution = tabRep[0]
    stor.idRadios = []
    stor.nameRadio = j3pGetNewId('choix')
    for (let k = 1; k <= tabRep.length; k++) {
      const laListe = j3pAddElt(stor.zoneCons2, 'div')
      stor.idRadios.push(j3pGetNewId('radio' + k))
      j3pBoutonRadio(laListe, stor.idRadios[k - 1], stor.nameRadio, k - 1, '')
      j3pAffiche('label' + stor.idRadios[k - 1], '', tabRep[me.stockage[3][k - 1]])
    }
    stor.tabRep = tabRep
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce': {
      if (me.debutDeLaSection) {
        // code exécuté UNE SEULE FOIS avant le premier item
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        // me.HauteurLignes(30,-1,-1,-1,-1);
        me.score = 0
        me.stockage = [0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0]
        me.afficheTitre(ds.textes.titreExo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      // On s’occupe de la question répétitive

      // je génère mes valeurs :
      enonceMain()
      break // case "enonce":
    }

    case 'correction': {
      stor.zoneCorr.innerHTML = ''
      const reponse = {
        aRepondu: false,
        bonneReponse: false
      }
      // on teste si une réponse a été saisie
      reponse.aRepondu = (j3pBoutonRadioChecked(stor.nameRadio)[0] > -1)
      const numRadioSelect = j3pBoutonRadioChecked(stor.nameRadio)[0] + 1
      reponse.bonneReponse = (j3pBoutonRadioChecked(stor.nameRadio)[0] === me.stockage[3].indexOf(0))
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
      } else { // une réponse a été saisie
        if (reponse.bonneReponse) {
          me._stopTimer()
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
          for (let j = 1; j <= stor.tabRep.length; j++) {
            j3pDesactive(stor.idRadios[j - 1])
          }
          if (ds.affichage_correction) {
            afficheCorrection(true)
          }
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          // à cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            for (let j = 0; j < stor.tabRep.length; j++) {
              j3pDesactive(stor.idRadios[j - 1])
            }
            if (ds.affichage_correction) {
              afficheCorrection(false)
            }
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            // S’il y a deux chances,on appelle à nouveau le bouton Valider
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              me.etat = 'correction'
              me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
            } else {
              // Erreur au second essai
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              me.typederreurs[2]++
              for (let j = 1; j <= stor.tabRep.length; j++) {
                j3pDesactive(stor.idRadios[j - 1])
              }
              j3pBarre('label' + stor.idRadios[numRadioSelect - 1])
              j3pElement('label' + stor.idRadios[numRadioSelect - 1]).style.color = me.styles.cfaux
              $('#label' + stor.idRadios[me.stockage[3].indexOf(0)]).css('color', me.styles.toutpetit.correction.color)
              if (ds.affichage_correction) {
                afficheCorrection(false)
              }
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      me.finCorrection()
      break
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
