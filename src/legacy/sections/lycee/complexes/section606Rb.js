import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen
    2012

   Donner la valeur exacte du module d’un complexe sous forme algbrique

 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'maximum : 10r'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles']
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function getDonnees () {
    return {

      typesection: 'lycee',

      nbrepetitions: 3,
      nbetapes: 6,
      textes: {
        titreExo: 'Calculer la valeur exacte d’un module',
        question1: "L’objectif est de déterminer la valeur exacte du <span class='rouge'>module </span>du complexe suivant et de l’écrire sous la forme $£a\\sqrt{£b}$, $£b$ étant l’entier le plus petit possible&nbsp;:",
        enonce1: 'Préciser la partie réelle:',
        enonce2: 'Préciser la partie imaginaire:',
        enonce3: 'Calculer le carré de la partie réelle:',
        enonce4: 'Calculer le carré de la partie imaginaire:',
        enonce5: 'Or on sait que $|z|=\\sqrt{Re(z)^2+Im(z)^2}$<br>',
        enonce5bis: 'Donc, (écrire le résultat sous la forme $\\sqrt{a}$)<br>',
        enonce6: '<br>Simplifions le radical:',
        correction1: '<br>La partie réelle du complexe $a+\\mathrm{i}b$ est $a$.<br>Donc ici la partie réelle vaut :\n',
        correction2: '<br>La partie imaginaire du complexe $a+\\mathrm{i}b$ est $b$.<br>Donc ici la partie imaginaire vaut :\n',
        explications3: '<br>Non le carré de la partie réelle est $£a$&nbsp;:<br>',
        explications4: '<br>Non le carré de la partie imaginaire est $£a$&nbsp;:<br>',
        explications5: '<br>Non, la somme des deux carrés vaut : $£a$.<br>',
        correction5: 'On a donc&nbsp;: \n',
        correction6: 'Non, on décompose le radicande ainsi&nbsp;: $£a = £c \\times £d =  £d^2 \\times £b$.',
        correction6bis: 'On a donc $|z_1|=\\sqrt{£d^2 \\times £b}=£d\\sqrt{£b}$.',
        ccl: 'CONCLUSION : $|z_1|=$£a.'
      },
      indication: '',
      nbchances: 2,
      // surchargenbchances=false;
      structure: 'presentation1',
      limite: '',
      surchargelimite: false

    }
  }

  function retourneexpressionmathquill$ (tab) {
    // tab=[-2,3] ==> retourne -2\\sqrt3
    // tab=[-1,] ==> retourne -1
    // tab=[-1,5] ==> -\\sqrt5
    if (tab[1] === '') {
      return '$' + tab[0] + '$'
    } else {
      return '$' + tab[0] + '\\sqrt{' + tab[1] + '}$'
    }
  }

  function testReponseEleve () {
    const obj = {}
    let aRepondu = true
    let bool = false
    let reponseEleve, reponseEleveb
    obj.zone = stor.zoneInput
    if ((me.questionCourante % ds.nbetapes) === 1) {
      obj.enonce = '$Re(z_1)$=$£a$'
      reponseEleve = $(obj.zone).mathquill('latex')
      if (reponseEleve === '') {
        aRepondu = false
      }
      reponseEleve = traduitChaineMathquill(reponseEleve)
      if ((String(me.stockage[6] + me.stockage[0]) === reponseEleve[0]) && (String(me.stockage[2]) === reponseEleve[1])) {
        bool = true
      }
    }
    if ((me.questionCourante % ds.nbetapes) === 2) {
      obj.enonce = '$Im(z_1)$=$£a$'
      reponseEleve = $(obj.zone).mathquill('latex')
      if (reponseEleve === '') {
        aRepondu = false
      }
      reponseEleve = traduitChaineMathquill(reponseEleve)
      if (((me.stockage[7] + me.stockage[1]) === reponseEleve[0]) && (String(me.stockage[3]) === reponseEleve[1])) {
        bool = true
      }
    }
    if ((me.questionCourante % ds.nbetapes) === 3) {
      obj.enonce = '$Re(z_1)^2$=$£a$'
      reponseEleve = $(obj.zone).mathquill('latex')
      if (reponseEleve === '') {
        aRepondu = false
      }
      reponseEleve = traduitChaineMathquill(reponseEleve)
      if (reponseEleve[0] === String(me.stockage[4])) {
        bool = true
      }
    }
    if ((me.questionCourante % ds.nbetapes) === 4) {
      obj.enonce = '$Im(z_1)^2$=$£a$'
      reponseEleve = $(obj.zone).mathquill('latex')
      if (reponseEleve === '') {
        aRepondu = false
      }
      reponseEleveb = traduitChaineMathquill(reponseEleve)
      if (reponseEleveb[0] === String(me.stockage[5])) {
        bool = true
      }
    }
    if ((me.questionCourante % ds.nbetapes) === 5) {
      obj.enonce = '$|z_1|$=$£a$'
      reponseEleve = $(obj.zone).mathquill('latex')
      if (reponseEleve === '') {
        aRepondu = false
      }
      reponseEleve = traduitChaineMathquill(reponseEleve)
      if (reponseEleve[1] === String(me.stockage[5] + me.stockage[4])) {
        bool = true
      }
    }
    if ((me.questionCourante % ds.nbetapes) === 0) {
      obj.enonce = '$|z_1|$=$£a$'
      reponseEleve = $(obj.zone).mathquill('latex')
      if (reponseEleve === '') {
        aRepondu = false
      }
      reponseEleve = traduitChaineMathquill(reponseEleve)
      if ((reponseEleve[0] === String(me.stockage[8])) && (reponseEleve[1] === String(me.stockage[9]))) {
        bool = true
      }
    }
    obj.reponse = reponseEleve
    obj.repondu = aRepondu
    obj.juste = bool
    return obj
  }
  function macorrection (reponseeleve, solution, stockage) {
    // reponseeleve=[a,b] pour aRacine(b)
    // solution=idem
    // stockage = stockage=[6,2,"",3,36,12,"","",4,3]
    j3pEmpty(stor.laPalette)
    const unobjet = {
      unechaine: '',
      bilan: ''
    }
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(stor.zoneExpli, 'div') })
    if ((me.questionCourante % ds.nbetapes) === 1) {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.correction1)
      j3pAffiche(stor.zoneExpli2, '', retourneexpressionmathquill$(solution[0]))
      return unobjet
    }

    if ((me.questionCourante % ds.nbetapes) === 2) {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.correction2)
      j3pAffiche(stor.zoneExpli2, '', retourneexpressionmathquill$(solution[1]))
      return unobjet
    }

    if ((me.questionCourante % ds.nbetapes) === 3) {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.explications3, { a: me.stockage[4] })
      if (stockage[2] !== '') {
        if (stockage[0] !== '') {
          j3pAffiche(stor.zoneExpli2, '', '$(£c£a\\sqrt{£b})^2=£a^2 \\times £b=£d$', { a: stockage[0], b: stockage[2], c: stockage[6], d: stockage[4] })
        } else {
          if (stockage[6] !== '') {
            j3pAffiche(stor.zoneExpli2, '', '$(-\\sqrt{£b})^2=£d$', { a: stockage[0], b: stockage[2], c: stockage[6], d: stockage[4] })
          } else {
            j3pAffiche(stor.zoneExpli2, '', '$(\\sqrt{£b})^2=£d$', { a: stockage[0], b: stockage[2], c: stockage[6], d: stockage[4] })
          }
        }
      } else {
        j3pAffiche(stor.zoneExpli2, '', '$(£c£a)^2=£d$', { a: stockage[0], b: stockage[2], c: stockage[6], d: stockage[4] })
      }
      return unobjet
    }

    if ((me.questionCourante % ds.nbetapes) === 4) {
      // stockage=[6,2,"",3,36,12,"","",4,3]
      j3pAffiche(stor.zoneExpli1, '', ds.textes.explications4, { a: me.stockage[5] })
      if (stockage[3] !== '') {
        if (stockage[1] !== '') {
          j3pAffiche(stor.zoneExpli2, '', '$(£c£a\\sqrt{£b})^2=£a^2 \\times £b=£d$', { a: stockage[1], b: stockage[3], c: stockage[7], d: stockage[5] })
        } else {
          if (stockage[7] !== '') {
            j3pAffiche(stor.zoneExpli2, '', '$(-\\sqrt{£b})^2=£d$', { a: stockage[1], b: stockage[3], c: stockage[7], d: stockage[5] })
          } else {
            j3pAffiche(stor.zoneExpli2, '', '$(\\sqrt{£b})^2=£d$', { a: stockage[1], b: stockage[3], c: stockage[7], d: stockage[5] })
          }
        }
      } else {
        j3pAffiche(stor.zoneExpli2, '', '$(£c£a)^2=£d$', { a: stockage[1], b: stockage[3], c: stockage[7], d: stockage[5] })
      }
      return unobjet
    }

    if ((me.questionCourante % ds.nbetapes) === 5) {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.explications5, { a: me.storage.solution[1] })
      j3pAffiche(stor.zoneExpli2, '', ds.textes.correction5)
      j3pAffiche(stor.zoneExpli3, '', '$|z_1|=$' + retourneexpressionmathquill$(solution), { a: me.stockage[0], b: me.stockage[1] })
      return unobjet
    }

    if ((me.questionCourante % ds.nbetapes) === 0) {
      // stockage=[6,2,"",3,36,12,"","",4,3]
      const lasomme = stockage[4] + stockage[5]
      j3pAffiche(stor.zoneExpli1, '', ds.textes.correction6, { a: lasomme, b: stockage[9], c: stockage[8] * stockage[8], d: stockage[8] })
      j3pAffiche(stor.zoneExpli2, '', ds.textes.correction6bis, { b: stockage[9], d: stockage[8] })
      return unobjet
    }
  }

  function estDansTableau (element, tableau) {
    // console.log('element=' + element + ' et tableau=' + tableau)
    let bool = false
    for (let i = 0; i <= me.storage.aleas.length - 1; i++) {
      if (tableau[i] === element) {
        bool = true
      }
    }
    return bool
  }

  function traduitChaineMathquill (ch) {
    // je retourne dans tab[0] ce qu’il y a hors racine et dans tab[1] ce qu’il y a dans la racine
    // je vire d’abord les espaces :
    let pos
    while (ch.includes(':')) {
      pos = ch.indexOf(':')
      ch = ch.substring(0, pos - 1) + ch.substring(pos + 1)
    }
    while (ch.includes('cdot')) {
      pos = ch.indexOf('cdot')
      ch = ch.substring(0, pos - 1) + ch.substring(pos + 4)
    }
    const tab = ['', '']
    // dans le cas où la solution est entière
    if (!ch.includes('sqrt')) {
      tab[0] = ch
    } else {
      // a racine(b)
      pos = ch.indexOf('sqrt')// on pourrait améliorer en testant si la solution est de la forme racine(b) a
      if (pos === 1) {
        // je recherche le } de fin de racine
        const pos1 = ch.indexOf('{')
        const pos2 = ch.indexOf('}')
        tab[1] = ch.substring(pos1 + 1, pos2)
        tab[0] = ch.substring(pos2 + 1)
      } else {
        tab[0] = ch.substring(0, pos - 1)
        tab[1] = ch.substring(pos + 5, ch.length - 1)
      }
    }

    return tab
  }

  // DEBUT DE LA SECTION
  function enonceMain () {
  // z_1 = a racine(c) + b racine(d) i
  // mon tableau de données de la forme : [a,c,b,d,me.storage.solution[0],me.storage.solution[1]] plus deux valeurs qui servent en correction (les carrés)
  // par exemple pour le premier, l’affichage sera   +/- 6 +/- 2 racine(3) i et la bonne réponse 4 racine(3)
    stor.tab = []
    stor.tab[0] = [6, '', 2, 3, 4, 3, 36, 12]
    stor.tab[1] = [2, 6, 2, 6, 4, 3, 24, 24]
    stor.tab[2] = [5, '', 5, '', 5, 2, 25, 25]
    stor.tab[3] = ['', 30, 2, 5, 5, 2, 30, 20]
    stor.tab[4] = [2, 5, 2, 3, 4, 2, 20, 12]
    stor.tab[5] = [4, '', 4, '', 4, 2, 16, 16]
    stor.tab[6] = [4, '', 2, '', 2, 5, 16, 4]
    stor.tab[7] = [3, 3, 3, 2, 3, 5, 27, 18]
    stor.tab[8] = [6, '', 3, 2, 3, 6, 36, 18]
    stor.tab[9] = ['', 30, 2, 6, 3, 6, 30, 24]
    stor.tab[10] = [1, '', '', 7, 2, 2, 1, 7]
    stor.tab[11] = [2, '', 2, '', 2, 2, 4, 4]
    stor.tab[12] = [1, '', '', 11, 2, 3, 1, 11]
    stor.tab[13] = [2, '', 2, 2, 2, 3, 4, 8]
    stor.tab[14] = ['', 2, '', 10, 2, 3, 2, 10]
    stor.tab[15] = [3, '', 3, 2, 3, 3, 9, 18]
    stor.tab[16] = [2, 2, 8, '', 6, 2, 8, 64]
    stor.tab[17] = [2, 6, 4, 3, 6, 2, 24, 48]
    const listePalette = ['racine']
    if ((me.questionCourante % ds.nbetapes) === 1) {
      me.videLesZones()
      stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '15px' }) })
      ;[1, 2].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })
      // pour éviter d’avoir le même tirage, suppose qu’il y a moins de répétitions que de possibilités...
      let nbAlea
      do {
        nbAlea = j3pGetRandomInt(0, (stor.tab.length - 1))
      } while (me.questionCourante !== 1 && estDansTableau(nbAlea, me.storage.aleas))

      stor.aleas.push(nbAlea)
      // me.tab[0]=[6,"",2,3,4,3,36,12];
      const a = stor.tab[nbAlea][0]
      const c = stor.tab[nbAlea][1]
      const b = stor.tab[nbAlea][2]
      const d = stor.tab[nbAlea][3]
      const e = stor.tab[nbAlea][6]
      const f = stor.tab[nbAlea][7]
      const g = stor.tab[nbAlea][4]
      const h = stor.tab[nbAlea][5]
      //  me.stockage=[a,b,c,d,e,f,"","",g,h];
      me.stockage[0] = a
      me.stockage[1] = b
      me.stockage[2] = c
      me.stockage[3] = d
      me.stockage[4] = e
      me.stockage[5] = f
      me.stockage[6] = ''
      me.stockage[7] = ''
      me.stockage[8] = g
      me.stockage[9] = h

      // me.stockage=[6,2,"",3,36,12,"","",4,3]
      stor.solution[0] = stor.tab[nbAlea][4]
      stor.solution[1] = stor.tab[nbAlea][5]

      let lexpression1
      // z1 = a racine(c) +/- b racine(d) i avec gestion des différents cas particuliers possibles

      lexpression1 = '$z_1='
      const lepremiersigne = j3pGetRandomInt(1, 2)
      const lesecondsigne = j3pGetRandomInt(1, 2)
      if (lepremiersigne === 1) {
      // si a racine(c) est non nul : (marche dans le cas où a="")
        if (!((a === 0) && (c === 0))) {
          lexpression1 += '£x'
        }
        // utile en correction
        me.stockage[6] = ''
      } else {
      // j’ajoute un moins dans le cas où a racine(c) est non nul
        if (!((a === 0) && (c === 0))) {
          lexpression1 += '-£x'
        }
        // utile en correction
        me.stockage[6] = '-'
      }
      if ((c !== '') && (c > 0)) {
      // ajout de racine(c) si définie
        lexpression1 += '\\sqrt{£w}'
      }
      // la suite si b*racine(d) est non nulle
      if (!((b === 0) && (d === 0))) {
        if (lesecondsigne === 1) {
        // pas de plus s’il n’y a rien avant
          if (!((a === 0) && (c === 0))) {
            lexpression1 += ' + '
          }
          me.stockage[7] = ''
        } else {
          lexpression1 += ' - '
          me.stockage[7] = '-'
        }
        lexpression1 += '£y'
        if ((d !== '') && (d !== 0)) {
          lexpression1 += '\\sqrt{£t}'
        }
        lexpression1 += 'i'
      }
      j3pAffiche(stor.zoneCons1, '', ds.textes.question1, { a: 'a', b: 'b' })

      lexpression1 += '$'
      // document.title=lexpression1+" %%% "+me.stockage;
      stor.solution = [[me.stockage[6] + me.stockage[0], me.stockage[2]], [me.stockage[7] + me.stockage[1], me.stockage[3]]]
      j3pAffiche(stor.zoneCons2, '', '&nbsp;&nbsp;&nbsp;&nbsp;' + lexpression1, {
        x: a,
        y: b,
        w: c,
        t: d
      })
      j3pStyle(stor.zoneCons2, { paddingBottom: '20px' })
      stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
      const zoneDCons1 = j3pAddElt(stor.conteneurD, 'div')
      const zoneDCons2 = j3pAddElt(stor.conteneurD, 'div')
      j3pAffiche(zoneDCons1, '', ds.textes.enonce1)
      const elt = j3pAffiche(zoneDCons2, '', '$Re(z_1)$=&1&', { inputmq1: { texte: '' } })
      stor.zoneInput = elt.inputmqList[0]
    } else {
      j3pDetruit(stor.zoneExpli)
    }

    if ((me.questionCourante % ds.nbetapes) === 2) {
      j3pDetruit(stor.zoneCorr)
      j3pDetruit(stor.laPalette)
      j3pEmpty(stor.conteneurD)
      const zoneDCons1 = j3pAddElt(stor.conteneurD, 'div')
      const zoneDCons2 = j3pAddElt(stor.conteneurD, 'div')
      const zoneDCons3 = j3pAddElt(stor.conteneurD, 'div')
      j3pAffiche(zoneDCons1, '', '$Re(z_1)=$' + retourneexpressionmathquill$(stor.solution[0]), {})

      j3pAffiche(zoneDCons2, '', ds.textes.enonce2 + '\n')
      const elt = j3pAffiche(zoneDCons3, '', '$Im(z_1)$=&1&', { inputmq1: { texte: '' } })
      stor.zoneInput = elt.inputmqList[0]
    }

    if ((me.questionCourante % ds.nbetapes) === 3) {
      j3pDetruit(stor.zoneCorr)
      j3pDetruit(stor.laPalette)
      j3pEmpty(stor.conteneurD)
      stor.zoneDCons1 = j3pAddElt(stor.conteneurD, 'div')
      stor.zoneDCons2 = j3pAddElt(stor.conteneurD, 'div')
      stor.zoneDCons3 = j3pAddElt(stor.conteneurD, 'div')
      stor.zoneDCons4 = j3pAddElt(stor.conteneurD, 'div')
      j3pAffiche(stor.zoneDCons1, '', '$Re(z_1)=$' + retourneexpressionmathquill$(stor.solution[0]), {})
      j3pAffiche(stor.zoneDCons2, '', '$Im(z_1)=$' + retourneexpressionmathquill$(stor.solution[1]), {})

      stor.solution = [me.stockage[4], '']
      j3pAffiche(stor.zoneDCons3, '', ds.textes.enonce3)
      const elt = j3pAffiche(stor.zoneDCons4, '', '$Re(z_1)^2$=&1&', { inputmq1: { texte: '' } })
      stor.zoneInput = elt.inputmqList[0]
    }

    if ((me.questionCourante % ds.nbetapes) === 4) {
      j3pDetruit(stor.zoneCorr)
      j3pDetruit(stor.laPalette)
      j3pEmpty(stor.zoneDCons3)
      j3pEmpty(stor.zoneDCons4)
      stor.zoneDCons5 = j3pAddElt(stor.conteneurD, 'div')
      j3pAffiche(stor.zoneDCons3, '', '$Re(z_1)^2=$' + retourneexpressionmathquill$(stor.solution), {})

      stor.solution = [me.stockage[5], '']
      j3pAffiche(stor.zoneDCons4, '', ds.textes.enonce4)
      const elt = j3pAffiche(stor.zoneDCons5, '', '$Im(z_1)^2$=&1&', { inputmq1: { texte: '' } })
      stor.zoneInput = elt.inputmqList[0]
    }

    if ((me.questionCourante % ds.nbetapes) === 5) {
      j3pDetruit(stor.zoneCorr)
      j3pDetruit(stor.laPalette)
      j3pEmpty(stor.zoneDCons4)
      j3pEmpty(stor.zoneDCons5)
      stor.zoneDCons6 = j3pAddElt(stor.conteneurD, 'div')
      stor.zoneDCons7 = j3pAddElt(stor.conteneurD, 'div')
      j3pAffiche(stor.zoneDCons4, '', '$Im(z_1)^2=$' + retourneexpressionmathquill$(stor.solution), {})
      j3pAffiche(stor.zoneDCons5, '', ds.textes.enonce5, { a: me.stockage[4], b: me.stockage[5] })
      j3pAffiche(stor.zoneDCons6, '', ds.textes.enonce5bis, {})

      stor.solution = ['', me.stockage[5] + me.stockage[4]]
      const elt = j3pAffiche(stor.zoneDCons7, '', '$|z_1|$=&1&', { inputmq1: { texte: '' } })
      stor.zoneInput = elt.inputmqList[0]
    }

    if ((me.questionCourante % ds.nbetapes) === 0) {
      j3pDetruit(stor.zoneCorr)
      j3pDetruit(stor.laPalette)
      j3pEmpty(stor.zoneDCons6)
      j3pEmpty(stor.zoneDCons7)
      j3pAffiche(stor.zoneDCons6, '', '$|z_1|=$' + retourneexpressionmathquill$(stor.solution), {})

      j3pAffiche(stor.zoneDCons7, '', ds.textes.enonce6)
      stor.solution = [me.stockage[8], me.stockage[9]]
      stor.zoneDCons8 = j3pAddElt(stor.conteneurD, 'div')

      const elt = j3pAffiche(stor.zoneDCons8, '', '$|z_1|$=&1&', { inputmq1: { texte: '' } })
      stor.zoneInput = elt.inputmqList[0]
      stor.bilan = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.enonce', { padding: '20px' }) })
      stor.bilan.style.color = '#00F'
    }

    mqRestriction(stor.zoneInput, '\\d,.-+i', { commandes: listePalette })
    stor.laPalette = j3pAddElt(stor.conteneurD, 'div')
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: listePalette })
    j3pFocus(stor.zoneInput)
    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div')
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('moyen.explications', { paddingTop: '10px' }) })

    // me.focus("reponse1");
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':

      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.5 })
        me.stockage = [0, 0, 0, 0]
        me.storage.aleas = []
        me.storage.solution = []
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titreExo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      }

      enonceMain()
      break // case "enonce":

    case 'correction':

      me.cacheBoutonValider()
      stor.zoneCorr.innerHTML = ''
      // on teste si une réponse a été saisie

      {
        const objet = testReponseEleve()
        const exact = objet.juste
        const aRepondu = objet.repondu
        const repEleve = objet.reponse
        if ((!aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
        } else { // une réponse a été saisie
          if (exact) {
            if (me.questionCourante === 6) {
              j3pAffiche(stor.bilan, '', ds.textes.ccl, { a: retourneexpressionmathquill$(stor.solution) })
            }
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            stor.zoneExpli.style.color = me.styles.cbien
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pEmpty(stor.laPalette)
            me.etat = 'navigation'
            me.sectionCourante()

          // j3pMathsAjouteDans("conteneur",{id:"exp1",content:'la fraction $\\frac{3}{£a}$ est irréductible',parametres:{a:7}})
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              macorrection(repEleve, stor.solution, me.stockage)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                j3pFocus(stor.zoneInput)
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
              // j3pElement("explications").innerHTML+='<br>' + regardeCorrection
                me._stopTimer()
                me.typederreurs[2]++
                macorrection(repEleve, stor.solution, me.stockage)
                stor.zoneInput.style.color = me.styles.cfaux
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
