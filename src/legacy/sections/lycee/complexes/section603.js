import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
 *Auteur : Alexis Lecomte
 *Date : 2012->juin 2013
 *Remarque : Un point M(z) du plan complexe étant donné, donnner la forme algébrique de z. Section qualitative testant la confusion des coordonnées
 *Section adaptée au clavier virtuel (et corrigée par Yves en 10-2021
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', 'Identifier les parties réelle et imaginaire de z. Ce sont les coordonnées de l’image.', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['seuilreussite', 0.8, 'reel', 'Permet de gérer l’obtention de la pe 1'],
    ['seuil_confusion', 0.2, 'reel', 'Permet de gérer l’obtention de la pe 2'],
    ['a', '[-6;6]', 'string', 'Partie réelle'],
    ['b', '[-4;4]', 'string', 'Partie imaginaire']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'confusion des deux coordonnées' },
    { pe_3: 'insuffisant' }
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 5,
      nbetapes: 1,
      textes: {
        titreExo: 'Lire l’affixe d’un point du plan complexe',
        question1_1: 'Soit M un point du plan complexe d’affixe $z$.',
        question1_2: 'A l’aide d’une lecture graphique, <br>donner la forme algébrique du complexe $z$ : $z =$&1&',
        correction1: ' Voici l’écriture algébrique de $z$ :<br>$z = £a$.',
        correction2: ' Tu confonds partie réelle et partie imaginaire.<br> Voici l’écriture algébrique de $z$ : $£a$.',
        correction3: ' Tu confonds partie réelle et partie imaginaire.<br>'
      },
      indication: 'Identifier les parties réelle et imaginaire de z. Ce sont les coordonnées de l’image.',
      // on ne pourra pas excéder cette fenêtre, même en surchargeant
      a: '[-6;6]',
      b: '[-4;4]',
      nbchances: 2,
      limite: '',
      pe_1: 'bien', // à partir de 70%
      pe_2: 'confusion des deux coordonnées', // à partir de 30% (ici)
      pe_3: 'insuffisant', // moins de 70% de  bonnes reponses
      seuilreussite: 0.8,
      seuil_confusion: 0.2,
      structure: 'presentation1'
    }
  }

  function macorrection (repEleve) {
    const unobjet = { unechaine: '', bilan: '' }
    unobjet.unechaine = ds.textes.correction1
    // unobjet.unechaine = " C’est faux, voici l’écriture algébrique de z :<br>z = "+solution[0];
    unobjet.bilan = 'autre'
    const solution = []// pour ne pas écraser le tableau solution avec ce test :
    const reponseInverse = construitSolution(stor.b, stor.a, repEleve, solution).reponse_exacte
    if (reponseInverse) {
      unobjet.bilan = 'inversion'
      unobjet.unechaine = ds.textes.correction2
    }
    return unobjet
  }
  // fonction qui à partir de a et b entiers, construit toutes les écritures possibles de a+ib
  // et les stocke dans le tableau tab (par exemple stor.solution), si la reponse élève est dans ce tableau des réponses possibles
  // l’objet retourné contient une propriété booléenne reponse_exacte, ainsi qu’une propriété tab qui est le tableau renseigné
  // (on trouve en position 0 l’écriture "attendue", utile en correction par exemple)
  // Exemple de section utilisant cette fonction : section603
  function construitSolution (a, b, reponseEleve, tab) {
    const objetRetour = {}
    let bonneReponse = false
    // la réponse sous forme "attendue" est toujours en tab[0]
    if (b === 0) {
      tab[0] = a
    } else {
      // b non nul
      if (a === 0) {
        // a nul
        if (b > 0) {
          tab[1] = 'i' + b
          tab[0] = b + 'i'
          tab[2] = '+i' + b
          tab[3] = '+' + b + 'i'
          if (b === 1) {
            tab[0] = 'i'
            tab[4] = b + 'i'
          }
        } else {
          tab[1] = '-i' + Math.abs(b)
          tab[0] = b + 'i'
          if (b === -1) {
            tab[0] = '-i'
            tab[2] = b + 'i'
          }
        }
      } else {
        // a et b non nuls
        if (a > 0) {
          if (b > 0) {
            // a et b positifs
            tab[1] = a + '+i' + b
            tab[0] = a + '+' + b + 'i'
            tab[2] = 'i' + b + '+' + a
            tab[3] = b + 'i+' + a
            tab[4] = '+' + a + '+i' + b
            tab[5] = '+' + a + '+' + b + 'i'
            tab[6] = '+i' + b + '+' + a
            tab[7] = '+' + b + 'i+' + a
            if (b === 1) {
              tab[0] = a + '+i'
              tab[8] = a + '+' + b + 'i'
              tab[9] = 'i+' + a
              tab[10] = '+' + a + '+i'
              tab[11] = '+i+' + a
            }
          } else {
            // a positif et b négatif
            tab[1] = a + '-i' + Math.abs(b)
            tab[0] = '' + a + b + 'i'
            tab[2] = '-i' + Math.abs(b) + '+' + a
            tab[3] = b + 'i+' + a
            tab[4] = '+' + a + '-i' + Math.abs(b)
            tab[5] = '+' + a + b + 'i'
            if (b === -1) {
              tab[0] = a + '-i'
              tab[6] = '' + a + b + 'i'
              tab[7] = '-i+' + a
              tab[8] = '+' + a + '-i'
            }
          }
        } else {
          // a < 0
          if (b > 0) {
            // a négatif et b positif
            tab[1] = a + '+i' + b
            tab[0] = a + '+' + b + 'i'
            tab[2] = 'i' + b + a
            tab[3] = b + 'i' + a
            tab[4] = '+i' + b + a
            tab[5] = '+' + b + 'i' + a
            if (b === 1) {
              tab[0] = a + '+i'
              tab[6] = a + '+' + b + 'i'
              tab[7] = 'i' + a
              tab[8] = '+i' + a
            }
          } else {
            // a et b négatifs
            tab[1] = a + '-i' + Math.abs(b)
            tab[0] = '' + a + b + 'i'
            tab[2] = '-i' + Math.abs(b) + a
            tab[3] = b + 'i' + a
            if (b === -1) {
              tab[0] = a + '-i'
              tab[4] = '' + a + b + 'i'
              tab[5] = '-i' + a
            }
          }
        }
      }
    }
    for (let i = 0; i < tab.length; i++) {
      if (String(reponseEleve) === String(tab[i])) {
        bonneReponse = true
      }
    }
    objetRetour.reponse_exacte = bonneReponse
    objetRetour.tableau_reponses = tab
    return objetRetour
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.75 })
    stor.solution = [0, 0, 0]
    me.typederreurs = [0, 0, 0, 0]
    me.score = 0

    me.afficheTitre(ds.textes.titreExo, true)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
  }
  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '25px' }) })
    let a, b
    do {
      const [borneInfa, borneSupa] = j3pGetBornesIntervalle(ds.a)
      const [borneInfb, borneSupb] = j3pGetBornesIntervalle(ds.b)
      a = j3pGetRandomInt(borneInfa, borneSupa)
      b = j3pGetRandomInt(borneInfb, borneSupb)
    } while (a > 6 || a < -6 || b < -4 || b > 4 || ((a === 0) && (b === 0)) || (a === b))
    stor.zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    stor.zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.question1_1)
    const elt = j3pAffiche(stor.zoneCons2, '', ds.textes.question1_2, { inputmq1: { texte: '' } })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '+i\\d-')
    stor.maFigure = j3pAddElt(stor.conteneur, 'div')
    stor.maFigure.style.padding = '7px 0'
    stor.idRepere = j3pGetNewId('unrepere')
    const unefigure = ''
    stor.repere = new Repere({
      idConteneur: stor.maFigure,
      idDivRepere: stor.idRepere,
      aimantage: false,
      visible: true,
      larg: 400,
      haut: 270,

      pasdunegraduationX: 1,
      pixelspargraduationX: 27,
      pasdunegraduationY: 1,
      pixelspargraduationY: 27,
      debuty: 0,
      negatifs: true,
      fixe: true,
      trame: true,
      objets: [
        {
          type: 'point',
          nom: 'M',
          par1: a,
          par2: b,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { taillepoint: 4, couleur: 'red', epaisseur: 3, taille: 16 }
        },
        {
          type: 'point',
          nom: 'X1',
          par1: 0,
          par2: 0,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: 'red', epaisseur: 3, taille: 16 }
        },
        {
          type: 'point',
          nom: 'X2',
          par1: 0,
          par2: b,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: 'black', epaisseur: 2, taille: 16 }
        },
        {
          type: 'point',
          nom: 'X3',
          par1: a,
          par2: 0,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: 'black', epaisseur: 2, taille: 16 }
        },
        { type: 'segment', nom: 's_1', par1: 'X1', par2: 'M', style: { couleur: 'red', epaisseur: 4 } }
      ]
    })
    stor.repere.construit()

    stor.figure = unefigure
    stor.maFigure.innerHTML += unefigure
    stor.a = a
    stor.b = b
    stor.zoneExpli = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '20px' }) })
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '10px' }) })
    j3pFocus(stor.zoneInput)
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce"

    case 'correction':

      // obligatoire
      stor.zoneCorr.innerHTML = ''
      // on teste si une réponse a été saisie
      {
        const reponseEleve = $(stor.zoneInput).mathquill('latex')
        const bonneReponse = construitSolution(stor.a, stor.b, reponseEleve, stor.solution).reponse_exacte

        if (!reponseEleve && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
        } else { // une réponse a été saisie
          if (bonneReponse) {
            me.score++
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            me._stopTimer()

            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneInput.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              j3pDesactive(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)

              me.typederreurs[10]++
              me._stopTimer()
              // je classe cette erreur comme 'autre'
              me.typederreurs[3]++
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.solution = []// pour ne pas écraser le tableau solution avec ce test :
                const reponseInverse = construitSolution(stor.b, stor.a, reponseEleve, stor.solution).reponse_exacte
                j3pFocus(stor.zoneInput)
                if (reponseInverse) {
                  stor.zoneCorr.innerHTML += ds.textes.correction3
                }
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                const ch = macorrection(reponseEleve)
                me._stopTimer()
                j3pEmpty(stor.zoneExpli)
                j3pAffiche(stor.zoneExpli, '', ch.unechaine, { a: stor.solution[0] })
                switch (ch.bilan) {
                  case 'inversion':
                    me.typederreurs[1]++
                    break
                  case 'autre':
                    me.typederreurs[2]++
                    break
                }
                // je modifie ma figure :
                document.getElementById(stor.idRepere).innerHTML = ''
                stor.repere.add({
                  type: 'segment',
                  nom: 's_2',
                  par1: 'X2',
                  par2: 'M',
                  style: { couleur: 'black', epaisseur: 1, pointilles: 4 }
                })
                stor.repere.add({
                  type: 'segment',
                  nom: 's_3',
                  par1: 'X3',
                  par2: 'M',
                  style: { couleur: 'black', epaisseur: 1, pointilles: 4 }
                })
                stor.repere.construit()
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break

    case 'navigation':

      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems
        // on détecte l’inversion des parties imaginaires et réelles
        if ((me.typederreurs[1] / ds.nbitems) >= ds.seuil_confusion) {
          me.parcours.pe = ds.pe_2
        } else {
          // on n’a pas détecté d’erreur type :
          if (pourcentagereussite >= ds.seuilreussite) {
            me.parcours.pe = ds.pe_1
          } else {
            me.parcours.pe = ds.pe_3
          }
        }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
