import { j3pAddElt, j3pDetruit, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pPaletteMathquill, j3pPGCD, j3pShowError } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

// Cette section affiche une série de 10 calculs du quotient de 2 nombres conmplexes'
// Les coefficients sont entiers relatifs.
// Erreurs types :
//  PE : pbforme si la réponse n’est pas sous forme algébrique
//  PE : pbconjugue pour (a+bi)(c+di)/(c^2+d^2) ou (a+bi)(c+di)/(c^2-d^2)
//  PE : pbicarre pour (a+bi)(c-di)/(c^2-d^2) ou encore ((ac-bd)+i(bc-ad)))/(c^2+d^2)
// Laurent F. & Alexis L
// Section revue par Yves pour adaptation clavier virtuel en 10-2021
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 10, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['a', '[-9;9]', 'string', 'coefficient a de (a+ib)/(c+id)'],
    ['b', '[-9;9]', 'string', 'coefficient b de (a+ib)/(c+id)'],
    ['c', '[-9;9]', 'string', 'coefficient c de (a+ib)/(c+id)'],
    ['d', '[-9;9]', 'string', 'coefficient d de (a+ib)/(c+id)']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'reponse correcte mais pas sous forme algebrique' },
    { pe_3: 'erreur uniquement dans le conjugue du dénominateur cad reponse (a+bi)(c+di)/(c^2+d^2) ou (a+bi)(c+di)/(c^2-d^2)' },
    { pe_4: 'erreur uniquement dans icarre cad (a+bi)(c-di)/(c^2-d^2) ou encore ((ac-bd)+i(bc-ad)))/(c^2+d^2)' },
    { pe_5: 'insuffisant sans erreur détectée' }
  ]
}

/**
 * section 602quot
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function depart () {
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAPIAAACugAAAQEAAAAAAAAAAQAAACD#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAJhMQABMgAAAAFAAAAAAAAAAAAAAAIA#####wACYjEAATEAAAABP#AAAAAAAAAAAAACAP####8AAmMxAAItM#####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQAgAAAAAAAAAAAACAP####8AAmQxAAE3AAAAAUAcAAAAAAAA#####wAAAAEABUNGb25jAP####8AAWYADWExL2IxK2MxL2QxKnj#####AAAAAQAKQ09wZXJhdGlvbgAAAAAFA#####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAABAAAABgAAAAIAAAAFAgAAAAUDAAAABgAAAAMAAAAGAAAABP####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAXgAAAAEAP####8AA3JlcAAILTMvNyp4KzIAAAAFAAAAAAMAAAAFAgAAAAUDAAAAAUAIAAAAAAAAAAAAAUAcAAAAAAAAAAAABwAAAAAAAAABQAAAAAAAAAAAAXj#####AAAAAwAQQ1Rlc3RFcXVpdmFsZW5jZQD#####AAhlZ2FsaXRlMQAAAAUAAAAGAQAAAAABP#AAAAAAAAABAAAAAgD#####AAJhMgACLTEAAAADAAAAAT#wAAAAAAAAAAAAAgD#####AAJiMgABMwAAAAFACAAAAAAAAAAAAAIA#####wACYzIAATkAAAABQCIAAAAAAAAAAAACAP####8AAmQyAAE0AAAAAUAQAAAAAAAAAAAAAgD#####AAJhMwACLTYAAAADAAAAAUAYAAAAAAAAAAAAAgD#####AAJiMwABNQAAAAFAFAAAAAAAAAAAAAIA#####wACYzMAATUAAAABQBQAAAAAAAAAAAACAP####8AAmQzAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAJhNAABMgAAAAFAAAAAAAAAAAAAAAIA#####wACYjQAAi0xAAAAAwAAAAE#8AAAAAAAAAAAAAIA#####wACYzQAATYAAAABQBgAAAAAAAAAAAACAP####8AAmQ0AAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAJhNQABMgAAAAFAAAAAAAAAAAAAAAIA#####wACYjUAATkAAAABQCIAAAAAAAAAAAACAP####8AAmM1AAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAJkNQABMQAAAAE#8AAAAAAAAAAAAAQA#####wABZwANYTIvYjIrYzIvZDIqeAAAAAUAAAAABQMAAAAGAAAACAAAAAYAAAAJAAAABQIAAAAFAwAAAAYAAAAKAAAABgAAAAsAAAAHAAAAAAABeAAAAAQA#####wABaAANYTMvYjMrYzMvZDMqeAAAAAUAAAAABQMAAAAGAAAADAAAAAYAAAANAAAABQIAAAAFAwAAAAYAAAAOAAAABgAAAA8AAAAHAAAAAAABeAAAAAQA#####wABagANYTQvYjQrYzQvZDQqeAAAAAUAAAAABQMAAAAGAAAAEAAAAAYAAAARAAAABQIAAAAFAwAAAAYAAAASAAAABgAAABMAAAAHAAAAAAABeAAAAAQA#####wABawANYTUvYjUrYzUvZDUqeAAAAAUAAAAABQMAAAAGAAAAFAAAAAYAAAAVAAAABQIAAAAFAwAAAAYAAAAWAAAABgAAABcAAAAHAAAAAAABeAAAAAgA#####wAIZWdhbGl0ZTIAAAAYAAAABgEAAAAAAT#wAAAAAAAAAQAAAAgA#####wAIZWdhbGl0ZTMAAAAZAAAABgEAAAAAAT#wAAAAAAAAAQAAAAgA#####wAIZWdhbGl0ZTQAAAAaAAAABgEAAAAAAT#wAAAAAAAAAQAAAAgA#####wAIZWdhbGl0ZTUAAAAbAAAABgEAAAAAAT#wAAAAAAAAAf###############w=='
    stor.mtgList = stor.mtgAppLecteur.createList(txtFigure)
  }

  function correctionDetaillee (a, b, c, d) {
    me.logIfDebug('me.storage:', me.storage)
    j3pDetruit(stor.zoneCons4)
    let ch1, ch2, ch3, numerateur, denominateur, numerateur2, signe, correction
    for (let i = 1; i <= 3; i++) {
      if (i === 1) {
        ch1 = stor.nume
        ch2 = stor.deno
        ch3 = stor.conj
        numerateur = (stor.a === 0 || stor.b === 0) ? ch1 : '\\left(' + ch1 + '\\right)'
        if (stor.c === 0 && stor.d < 0) {
          // le conjugué du dénominateur n’a pas de moins'
          numerateur2 = '\\times' + ch3
          denominateur = '\\left(' + ch2 + '\\right)'
        } else {
          numerateur2 = '\\left(' + ch3 + '\\right)'
          denominateur = (stor.c === 0 && stor.d > 0) ? ch2 : '\\left(' + ch2 + '\\right)'
        }
        correction = '$Q=\\frac{' + numerateur + numerateur2 + '}{' + denominateur + numerateur2 + '}$'
      } else if (i === 2) {
        // ETAPE 2 :
        ch1 = stor.dvptNum1
        ch2 = stor.dvptNum2 + ''
        signe = (ch2.charAt(0) === '-') ? '' : '+'
        numerateur = (stor.dvptNum2 === '0') ? ch1 : ch1 + signe + ch2
        denominateur = (stor.c === 0) ? d * d : ((c > 0) ? c + '^2' : '(' + c + ')^2') + '+' + ((d > 0) ? d + '^2' : '(' + d + ')^2')
        correction = '$Q=\\frac{' + numerateur + '}{' + denominateur + '}$'
      } else if (i === 3) {
        // stor.nonReduit permet de savoir si une étape de plus est nécessaire il y aura une étape de plus car on peut simplifer l’écriture'
        correction = (stor.nonReduit) ? '$Q = ' + stor.solution[2] + '$' : '$Q = ' + stor.reponse + '$'
      }
      stor['ligne' + i] = j3pAddElt(stor.zoneExpli, 'div')
      j3pAffiche(stor['ligne' + i], '', correction)
    }
    if (stor.nonReduit) {
      stor.ligne4 = j3pAddElt(stor.zoneExpli, 'div')
      j3pAffiche(stor.ligne4, '', '$Q = ' + stor.reponse + '$')
    }
  }

  // fonction qui teste si la réponse élève est égale à stor.solution[0], de manière formelle, et qui vérifie également que l’écriture est algébrique (un seul i)
  function analyseFormelle (reponseEleve) {
    const retour = {}
    let i
    me.logIfDebug('reponse=' + stor.reponse + ' et reponseEleve=' + reponseEleve)
    stor.mtgList.giveFormula2('rep', reponseEleve.replace(/i/g, 'x'))
    stor.mtgList.calculateNG()
    retour.bon = (stor.mtgList.valueOf('egalite1') === 1)
    if (!retour.bon) {
      // La réponse équivalente à la réponse attendue, mais elle est peut-être exacte mais non simplifiée
      const arbreChaine = new Tarbre(reponseEleve.replace(/i/g, 'x') + '-(' + stor.reponse.replace(/i/g, 'x') + ')', ['x'])
      let egalite = true
      for (i = 1; i <= 15; i++) {
        egalite = egalite && (Math.abs(arbreChaine.evalue([i])) < Math.pow(10, -12))
      }
      retour.bon = egalite
      const formeAlg = [
        /^-?[0-9]+[+-][0-9]*\*?x$/g,
        /^-?[0-9]*\*?x$/g,
        /^-?[0-9]+$/g,
        /^-?[0-9]*\*?x[+-][0-9]+$/g
      ]
      // s’arrête au premier truc qui vérifie, cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/some
      retour.simplifie = formeAlg.some(function (regexp) {
        return regexp.test(reponseEleve)
      })
    } else {
      retour.simplifie = true
    }
    me.logIfDebug('analyse_formelle', retour)
    return retour
  }

  // Utilisée pour remettre en forme a+ib ou b+ia
  function formatAplusiB (b, a, choix) {
    // choix=1 donne a+ib et choix=2 donne ib+a, avec les simplifications de signes.
    if (choix === 1) {
      if (a === 0) {
        return j3pMonome(1, 1, b, 'i')
      } else {
        return j3pMonome(1, 0, a, 'i') + j3pMonome(2, 1, b, 'i')
      }
    } else {
      if (b === 0) {
        return a
      } else {
        return j3pMonome(1, 1, b, 'i') + j3pMonome(2, 0, a, 'i')
      }
    }
  }

  function enleveFois (chaine) {
    // Cette fonction sert à enever les fois que rajoute xcas lors du traitement
    // Et on enlève le signe "*" de la chaine :
    return chaine.replace(/\*/g, '')
  }

  function coefSimplifie (a, b, c, d) {
    // on veut écrire l’expression a/b+c/d*x avec des fractions réduite
    const pgcd1 = (a === 0 || b === 0) ? 0 : j3pPGCD(Math.abs(a), Math.abs(b))
    const pgcd2 = (c === 0 || d === 0) ? 0 : j3pPGCD(Math.abs(c), Math.abs(d))
    return [a / pgcd1, b / pgcd1, c / pgcd2, d / pgcd2]
  }

  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 10,
      nbetapes: 1,
      seuilreussite: 0.7,
      // la réponse n’est pas sous forme algébrique :
      seuilpbforme: 0.2,
      // détectetion de (c+id)(c-id)=c^2-d^2 et de ((ac-bd)+i(bc-ad)))/(c^2+d^2)
      seuilpbicarre: 0.2,
      // détection d’une confusion entre c+id et c-id
      seuilpbconjugue: 0.2,
      // me.SeuilBienMaisaSignaler=0.2
      textes: {
        titre_exo: 'Quotient de nombres complexes',
        question1: 'Donnez l’écriture algébrique du quotient Q de nombres complexes :',
        correction1: 'Ton résultat n’est pas écrit sous forme algébrique.',
        correction2: 'Tu as fait une erreur liée au conjugué du dénominateur.',
        correction3: 'Tu as fait une erreur au dénominateur.',
        correction4: 'Étudie bien la correction.'
      },
      nbchances: 2,
      a: '[-9;9]',
      b: '[-9;9]',
      c: '[-9;9]',
      d: '[-9;9]',
      pe_1: 'bien',
      pe_2: 'pbforme',
      pe_3: 'pbconjugue',
      pe_4: 'pbicarre',
      pe_5: 'mal',
      structure: 'presentation1',
      indication: 'Multiplie numérateur et dénominateur par le conjugué du dénominateur'
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
    me.typederreurs = [0, 0, 0]
    stor.solution = []
    me.score = 0
    me.afficheTitre(ds.textes.titre_exo, false)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    depart()
    let a, b, c, d
    do {
      const [borneInfa, borneSupa] = j3pGetBornesIntervalle(ds.a)
      const [borneInfb, borneSupb] = j3pGetBornesIntervalle(ds.b)
      a = j3pGetRandomInt(borneInfa, borneSupa)
      b = j3pGetRandomInt(borneInfb, borneSupb)
    } while ((a * b === 0) || (a + b === 0))

    do {
      const [borneInfd, borneSupd] = j3pGetBornesIntervalle(ds.d)
      d = j3pGetRandomInt(borneInfd, borneSupd)
    } while (d === 0)

    do {
      const [borneInfc, borneSupc] = j3pGetBornesIntervalle(ds.c)
      c = j3pGetRandomInt(borneInfc, borneSupc)
    } while (a * d - b * c === 0 || c === d)

    stor.nume = formatAplusiB(b, a, 1)
    stor.deno = formatAplusiB(d, c, 1)
    // solution[0] sert pour le case correction storage.reponse comme affichage de la solution finale en correction détaillée
    const calcul = '(' + formatAplusiB(b, a, 1) + ')/(' + formatAplusiB(d, c, 1) + ')'
    stor.solution[0] = enleveFois(calcul)
    // A la place :
    const pgcd1 = (Math.abs(a * c + b * d) < Math.pow(10, -12)) ? 1 : j3pPGCD(Math.abs(a * c + b * d), Math.abs(c * c + d * d))
    const pgcd2 = (Math.abs(b * c - a * d) < Math.pow(10, -12)) ? 1 : j3pPGCD(Math.abs(b * c - a * d), Math.abs(c * c + d * d))
    if (pgcd1 === 1 && pgcd2 === 1) {
      // pas de simplification
      stor.nonReduit = false
    } else {
      stor.nonReduit = true
      // la solution non simplifiée
      stor.solution[2] = '\\frac{' + (a * c + b * d) + '}{' + (c * c + d * d) + '}+\\frac{' + (b * c - a * d) + '}{' + (c * c + d * d) + '}i'
    }
    // cas particulier si la fraction est entière :
    if (Math.abs(a * c + b * d) < Math.pow(10, -12)) {
      stor.reponse = ''
    } else if (((a * c + b * d) / pgcd1) / ((c * c + d * d) / pgcd1) === Math.round(((a * c + b * d) / pgcd1) / ((c * c + d * d) / pgcd1))) {
      stor.reponse = ((a * c + b * d) / pgcd1) / ((c * c + d * d) / pgcd1)
    } else {
      stor.reponse = '\\frac{' + ((a * c + b * d) / pgcd1) + '}{' + ((c * c + d * d) / pgcd1) + '}'
    }
    stor.reponse += (b * c - a * d > 0)
      ? (stor.reponse === '') ? '' : '+'
      : '-'
    if (((b * c - a * d) / pgcd2) / ((c * c + d * d) / pgcd2) === Math.round(((b * c - a * d) / pgcd2) / ((c * c + d * d) / pgcd2))) {
      stor.reponse += j3pMonome(1, 1, (Math.abs(b * c - a * d) / pgcd2) / ((c * c + d * d) / pgcd2), 'i')
    } else {
      stor.reponse += '\\frac{' + (Math.abs(b * c - a * d) / pgcd2) + '}{' + ((c * c + d * d) / pgcd2) + '}i'
    }
    stor.conj = formatAplusiB(-d, c, 1)
    stor.dvptNum1 = formatAplusiB(-a * d, a * c, 1)
    stor.dvptNum2 = formatAplusiB(b * c, b * d, 2)

    me.logIfDebug('réponse:', stor.reponse)
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '20px' }) }
    )
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    j3pAffiche(stor.zoneCons1, '', ds.textes.question1)
    j3pAffiche(stor.zoneCons2, '', '$Q=\\frac{' + stor.nume + '}{' + stor.deno + '}$')
    const elt = j3pAffiche(stor.zoneCons3, '', '$Q=$&1&', { inputmq1: { texte: '' } })
    stor.zoneCons4.style.paddingTop = '10px'
    stor.zoneCons4.style.paddingBottom = '40px'
    stor.zoneInput = elt.inputmqList[0]
    j3pPaletteMathquill(stor.zoneCons4, stor.zoneInput, { liste: ['fraction'] })
    mqRestriction(stor.zoneInput, '\\d,./*i+-', { commandes: ['fraction'] })
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px', paddingTop: '150px' }) })
    stor.a = a
    stor.b = b
    stor.c = c
    stor.d = d
    // variables utiles pour comparer avec la réponse de l’élève
    stor.coefsRep = [(a * c + b * d) / pgcd1, (c * c + d * d) / pgcd1, (b * c - a * d) / pgcd2, (c * c + d * d) / pgcd2]
    // Erreurs particulières
    const coefErreur1 = coefSimplifie(a * c - b * d, c * c + d * d, b * c + a * d, c * c + d * d)
    const coefErreur2 = coefSimplifie(a * c - b * d, c * c - d * d, b * c + a * d, c * c - d * d)
    const coefErreur3 = coefSimplifie(a * c + b * d, c * c - d * d, b * c - a * d, c * c - d * d)
    const coefErreur4 = coefSimplifie(a * c - b * d, c * c + d * d, b * c - a * d, c * c + d * d)

    me.logIfDebug('stor.coefsRep:', stor.coefsRep)
    // console.log(coefErreur1, coefErreur2, coefErreur3, coefErreur4)
    for (let i = 1; i <= 4; i++) {
      const lettre = String.fromCharCode(96 + i)
      stor.mtgList.giveFormula2(lettre + '1', stor.coefsRep[i - 1])
      stor.mtgList.giveFormula2(lettre + '2', coefErreur1[i - 1])
      stor.mtgList.giveFormula2(lettre + '3', coefErreur2[i - 1])
      stor.mtgList.giveFormula2(lettre + '4', coefErreur3[i - 1])
      stor.mtgList.giveFormula2(lettre + '5', coefErreur4[i - 1])
    }
    stor.mtgList.calculateNG()
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })
    j3pFocus(stor.zoneInput)
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break

    case 'correction':
      // pour les étapes dans la correction
      // me.cacheBoutonValider()
      stor.zoneCorr.innerHTML = ''
      // on teste si une réponse a été saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        let repEleveXcas
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
        } else { // une réponse a été saisie
          repEleveXcas = j3pMathquillXcas(stor.fctsValid.zones.reponseSaisie[0])
          const reponseAnalysee = analyseFormelle(repEleveXcas)
          reponse.bonneReponse = reponseAnalysee.bon && reponseAnalysee.simplifie
          stor.fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
          stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
          if (reponse.bonneReponse) {
            me.score++
            // correction détaillée :
            stor.zoneExpli.style.color = me.styles.cbien
            correctionDetaillee(stor.a, stor.b, stor.c, stor.d)
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            stor.zoneCorr.innerHTML += '<br>'
            stor.zoneCorr.innerHTML += ds.textes.correction4
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneExpli.style.color = me.styles.petit.correction.color
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              correctionDetaillee(stor.a, stor.b, stor.c, stor.d)
              // je classe cette erreur comme 'autre'
              me.typederreurs[4]++
            } else {
            // réponse fausse :
              if (reponseAnalysee.bon && !reponseAnalysee.simplifie) {
              // ce n’est pas une forme algébrique'
                if (me.essaiCourant === ds.nbchances) me.typederreurs[1]++
                stor.zoneCorr.innerHTML = ds.textes.correction1
              } else {
              // bonne écriture mais résultat faux, on fait des tests de reconnaissance si l’élève a saisi (a+bi)(c+di)/(c^2+d^2) ou (a+bi)(c+di)/(c^2-d^2) ou (a+bi)(c-di)/(c^2-d^2)'
                if ((stor.mtgList.valueOf('egalite2') === 1) || (stor.mtgList.valueOf('egalite3') === 1)) {
                // pb de conjugué ;
                  if (me.essaiCourant === ds.nbchances) me.typederreurs[2]++
                  stor.zoneCorr.innerHTML = ds.textes.correction2
                } else {
                  if ((stor.mtgList.valueOf('egalite4') === 1) || (stor.mtgList.valueOf('egalite5') === 1)) {
                    if (me.essaiCourant === ds.nbchances) me.typederreurs[3]++
                    stor.zoneCorr.innerHTML = ds.textes.correction3
                  } else {
                  // pas d’erreur particulière de reconnue :
                    if (me.essaiCourant === ds.nbchances) me.typederreurs[4]++
                    stor.zoneCorr.innerHTML = cFaux
                  }
                }
              }
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                j3pFocus(stor.zoneInput)
              } else {
              // Erreur au dernier essai
                me._stopTimer()
                correctionDetaillee(stor.a, stor.b, stor.c, stor.d)
                me.sectionCourante('navigation')
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems
        if (((me.typederreurs[3] / ds.nbitems) >= ds.seuilpbicarre) || ((me.typederreurs[1] / ds.nbitems) >= ds.seuilpbforme || ((me.typederreurs[2] / ds.nbitems) >= ds.seuilpbconjugue))) {
          if ((me.typederreurs[1] / ds.nbitems) >= ds.seuilpbforme) {
            // la réponse n’est pas sous forme algébrique'
            me.parcours.pe = ds.pe_2
          }
          if ((me.typederreurs[2] / ds.nbitems) >= ds.seuilpbconjugue) {
            // pb de calcul avec le conjugué
            me.parcours.pe = ds.pe_3
          }
          if ((me.typederreurs[3] / ds.nbitems) >= ds.seuilpbicarre) {
            // je détecte un problème de calcul de i^2
            me.parcours.pe = ds.pe_4
          }
        } else {
          // sinon
          if (pourcentagereussite >= ds.seuilreussite) {
            // c’est bon
            me.parcours.pe = ds.pe_1
          } else {
            // pas bon
            me.parcours.pe = ds.pe_5
          }
        }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
