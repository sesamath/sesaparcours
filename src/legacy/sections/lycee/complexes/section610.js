import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pElement, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pMonome, j3pNombre, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
 * QCM sur la forme imaginaire de ki(a+ib), section possible de remediation de la section602bis
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 10, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],

    ['a', '[-9;9]', 'string', 'coefficient a de ci(a+ib)'],
    ['b', '[-9;9]', 'string', 'coefficient b de ci(a+ib)'],
    ['c', '[-9;9]', 'string', 'coefficient c de ci(a+ib)']
  ]
  // dans le cas d’une section QUALITATIVE

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 5,
      nbetapes: 1,
      // ordre aléatoire des réponses :
      chamboule: true,

      a: '[-9;9]',
      b: '[-9;9]',
      c: '[-9;9]',
      textes: {
        titreExo: 'Partie imaginaire de ki(a+ib)',
        question: 'Sélectionne la partie imaginaire du complexe $z=£k$ ',
        correction1: 'La bonne réponse est $£a$ :',
        correction6: ' de partie imaginaire '
      },

      propositions: ['$£ki$',
        '$£k$',
        '$£k$',
        '$£ki$',
        '$£k$',
        '$£ki$'
      ],
      pe: 0, // ou faux
      structure: 'presentation1',
      indication: ''
    }
  }

  function ecritPar (chaine, nombre) {
    if (nombre < 0) {
      return '(-' + chaine + ')'
    } else {
      return chaine
    }
  }

  function ecritSigne (chaine, nombre) {
    if (nombre < 0) {
      return '-' + chaine
    } else {
      return '+' + chaine
    }
  }

  function ecritCoef (chaine, nombre) {
    if (nombre < 0) {
      return '-' + chaine
    } else {
      return chaine
    }
  }

  function ecritSansUn (nombre) {
    if (j3pNombre(nombre) === 1) {
      return ''
    } else {
      return nombre
    }
  }

  function correction (juste) {
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '10px', color: (juste) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })

    const unechaine = me.donneesSection.textes.correction1
    if (!juste) {
      j3pAffiche(stor.zoneExpli1, '', unechaine, { a: stor.solution })
    }

    const phraseCorrige = me.donneesSection.textes.correction6
    j3pAffiche(stor.zoneExpli2, '', me.stockage[7] + ',' + phraseCorrige + '$' + String(stor.solution) + '$', {
      x: Math.abs(me.stockage[8]),
      y: ecritSansUn(Math.abs(me.stockage[5])),
      z: Math.abs(me.stockage[6]),
      w: Math.abs(me.stockage[1]),
      v: Math.abs(me.stockage[0])
    })
  }

  function derange (tab) {
    // Si mon tab a pour longueur n la fonction derange stocke dans stockage[3] le tableau [0,... , n-1] dans le désordre si la variable chamboule est à true
    // stockage[4] contient la permutation inverse.
    me.stockage[3] = []
    me.stockage[4] = []
    if (me.donneesSection.chamboule) {
      for (let i = 0; i <= tab.length - 1; i++) {
        me.stockage[3][i] = j3pGetRandomInt(0, (tab.length - 1))
        for (let j = 0; j < i; j++) {
          if (me.stockage[3][i] === me.stockage[3][j]) {
            i--
          }
        }
      }
      // détermination de la permutation inverse
      for (let i = 0; i <= tab.length - 1; i++) {
        me.stockage[4][me.stockage[3][i]] = i
      }
    } else {
      for (let i = 0; i <= tab.length - 1; i++) {
        me.stockage[3][i] = i
        me.stockage[4][i] = i
      }
    }
  }

  // Utilisée pour remettre en forme a+ib ou b+ia
  function GestionAPlusiB (a, b, Choix) {
    // Choix=1 donne a+ib et Choix=2 donne ib+a, avec les simplifications de signes.
    let Stock = ''
    switch (Choix) {
      case 1:
        Stock = SimplifierEcritureDegre1(b, a, 2)
        break
      case 2:
        Stock = SimplifierEcritureDegre1(b, a, 1)
    }
    // Stock=RemplaceXparI(Stock);
    return Stock
  }

  function SimplifierEcritureDegre1 (a, b, Choix) {
    // Cette fonction fait simplifier par xcas les écritures littéralles ax+b si Choix=1
    // et b+ax si Choix=2
    // du style 1x+2=x+2 ou 2x+0=2x etc
    let Stock = ''
    switch (Choix) {
      case 2:
        // a+ib
        // Stock=xcasRequete(a+"x+"+b);
        if (a === 0) {
          if (b === 1) { Stock = 'i' }
          if (b === -1) { Stock = '-i' }
          if ((b !== 1) && (b !== -1)) { Stock = b + 'i' }
        } else {
          if (b === 0) {
            Stock = a + ''
          } else {
            // les deux non nuls
            if (b > 0) {
              if (b === 1) { Stock = a + '+i' }
              if (b !== 1) { Stock = a + '+' + b + 'i' }
            } else {
              if (b === -1) { Stock = a + '-i' }
              if (b !== -1) { Stock = a + '' + b + 'i' }
            }
          }
        }

        break
      case 1:
        // Stock=xcasRequete(b+"+"+a+"x");
        if (a === 0) {
          if (b === 1) { Stock = 'i' }
          if (b === -1) { Stock = '-i' }
          if ((b !== 1) && (b !== -1)) { Stock = b + 'i' }
        } else {
          if (b === 0) {
            Stock = a + ''
          } else {
            // les deux non nuls
            if (b > 0) {
              if ((b === 1) && (a > 0)) { Stock = 'i+' + a }
              if ((b === 1) && (a < 0)) { Stock = 'i' + a }
              if ((b !== 1) && (a > 0)) { Stock = b + 'i+' + a }
              if ((b !== 1) && (a < 0)) { Stock = b + 'i' + a }
            } else {
              if ((b === -1) && (a > 0)) { Stock = '-i+' + a }
              if ((b === -1) && (a < 0)) { Stock = '-i' + a }
              if ((b !== -1) && (a > 0)) { Stock = b + 'i+' + a }
              if ((b !== -1) && (a < 0)) { Stock = b + 'i' + a }
            }
          }
        }
        break
    }
    return Stock
  }

  function enonceMain () {
    // On s’occupe de la question répétitive
    const sortequestion = j3pGetRandomInt(1, 2)
    // je génère mes valeurs :
    let a, b, c
    do {
      a = j3pGetRandomInt(ds.a)
      b = j3pGetRandomInt(ds.b)
      const [borneInfc, borneSupc] = j3pGetBornesIntervalle(ds.c)
      c = j3pGetRandomInt(borneInfc, borneSupc)
    } while (a * b * c === 0 || a === b || a === -b)

    // vient du code de 602bis, partie stockage 3 et 4 utilisés pour la permutation
    me.stockage = [-b * c, a * c, sortequestion, '', '', b, c]

    let lasaisie = ''
    let lacorrection
    switch (sortequestion) {
      case 1:
        lasaisie = GestionAPlusiB(b, a, 1)
        lasaisie = j3pMonome(1, 1, c, 'i') + '(' + lasaisie + ')'
        lacorrection = '$z=' + ecritCoef('£zi', c) + ' \\times ' + ecritPar('£x', a) + ecritSigne('£zi', c) + ' \\times ' + ecritPar('£yi', b) + '=' + ecritCoef('£w', a * c) + 'i' + ecritSigne('£v', -b * c) + '=' + ecritCoef('£v', -b * c) + ecritSigne('£w', a * c) + 'i$'
        break
      case 2:
        lasaisie = GestionAPlusiB(b, a, 2)
        lasaisie = j3pMonome(1, 1, c, 'i') + '(' + lasaisie + ')'
        lacorrection = '$z=' + ecritCoef('£zi', c) + ' \\times ' + ecritPar('£yi', b) + ecritSigne('£zi', c) + ' \\times ' + ecritPar('£x', a) + '=' + ecritCoef('£v', -b * c) + ecritSigne('£w', a * c) + 'i$'
        break
    }
    stor.solution = a * c
    me.stockage[7] = lacorrection
    me.stockage[8] = a
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.question, { k: lasaisie })
    const tabRep = [c * a, c * a, c * b, c * b, -c * b, -c * b]
    // je dérange le tableau (en fait il n’est pas modifié mais on détermine une permutation (et son inverse)
    derange(tabRep)
    stor.idRadios = []
    stor.nameRadio = j3pGetNewId('choix')
    for (let k = 1; k <= tabRep.length; k++) {
      const laListe = j3pAddElt(stor.zoneCons2, 'div')
      stor.idRadios.push(j3pGetNewId('radio' + k))
      j3pBoutonRadio(laListe, stor.idRadios[k - 1], stor.nameRadio, k - 1, '')
      j3pAffiche('label' + stor.idRadios[k - 1], '', ds.propositions[me.stockage[3][k - 1]], { k: tabRep[me.stockage[3][k - 1]] })
    }
    stor.tabRep = tabRep
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce': {
      // code exécuté UNE SEULE FOIS avant le premier item

      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        // me.HauteurLignes(30,-1,-1,-1,-1);
        me.score = 0
        me.stockage = [0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0]
        me.afficheTitre(ds.textes.titreExo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":
    }
    case 'correction': {
      me.cacheBoutonValider()
      stor.zoneCorr.innerHTML = ''
      const reponse = {
        aRepondu: false,
        bonneReponse: false
      }
      reponse.aRepondu = (j3pBoutonRadioChecked(stor.nameRadio)[0] > -1)
      const numRadioSelect = j3pBoutonRadioChecked(stor.nameRadio)[0] + 1
      reponse.bonneReponse = (j3pBoutonRadioChecked(stor.nameRadio)[0] === me.stockage[3].indexOf(1))

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        me.afficheBoutonValider()
      } else { // une réponse a été saisie
        if (reponse.bonneReponse) {
          me._stopTimer()
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          me.typederreurs[0]++
          for (let j = 1; j <= stor.tabRep.length; j++) {
            j3pDesactive(stor.idRadios[j - 1])
          }
          correction(true)
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          // à cause de la limite de temps :
          if (me.isElapsed) {
            correction(false)

            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            // la correction
            for (let j = 0; j < stor.tabRep.length; j++) {
              j3pDesactive(stor.idRadios[j])
            }
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // réponse fausse sans limite de temps
            stor.zoneCorr.innerHTML = cFaux
            // S’il y a deux chances,on appelle à nouveau le bouton Valider
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              me.etat = 'correction'
              // j3pFocus("affiche1input1");
              me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
            } else {
              // Erreur au second essai
              me._stopTimer()
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              me.typederreurs[2]++
              // la correction

              for (let j = 1; j <= stor.tabRep.length; j++) {
                j3pDesactive(stor.idRadios[j - 1])
              }
              j3pBarre('label' + stor.idRadios[numRadioSelect - 1])
              j3pElement('label' + stor.idRadios[numRadioSelect - 1]).style.color = me.styles.cfaux
              $('#label' + stor.idRadios[me.stockage[3].indexOf(1)]).css('color', me.styles.toutpetit.correction.color)
              // Affichage de la correction :
              correction(false)

              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
