import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pChaine, j3pFocus, j3pFreezeElt, j3pGetRandomInt, j3pNombreBienEcrit, j3pVirgule } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Auteur : Laurent Fréguin, puis Alexis Lecomte pour maj et modifs
    Date : 2012 -> juin 2013
    Remarques : section qualitative (distinguant la confusion de deux parties) demandant la partie réelle ou imaginaire de k(a+ib)
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', 'Développe mentalement pour distinguer les deux parties.', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['seuilreussite', 0.7, 'reel', 'Permet de gérer l’obtention de la pe 1'],
    ['seuil_confusion', 0.2, 'reel', 'Permet de gérer l’obtention de la pe 2'],
    ['seuil_pbi', 0.2, 'reel', 'Permet de gérer l’obtention de la pe 3'],
    ['a', '[-9;9]', 'string', 'coefficient a de c(a+ib)'],
    ['b', '[-9;9]', 'string', 'coefficient b de c(a+ib)'],
    ['c', '[-9;9]', 'string', 'coefficient c de c(a+ib)']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'confusion des deux parties' },
    { pe_3: 'pbi' },
    { pe_4: 'mal' }
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 5,
      nbetapes: 1,
      seuilreussite: 0.7,
      seuil_pbi: 0.2,
      seuil_confusion: 0.2,
      indication: 'Développe mentalement pour distinguer les deux parties.',
      textes: {
        titre_exo: 'Parties réelles et imaginaires de k(a+ib)',
        question1: 'Détermine la partie réelle de £{z1}.',
        question2: 'Détermine la partie imaginaire de £{z1}.',
        correction1: 'Tu confonds partie réelle et partie imaginaire.<br> La bonne réponse est ' + '$£a$.',
        correction2: 'Attention à ne pas inclure i dans la réponse...<br> La bonne réponse est ' + '$£a$.',
        correction3: 'Peut-être une erreur de distributivité... La bonne réponse est $£a$.',
        correction4: ' , de partie réelle ',
        correction5: ' , de partie imaginaire '
      },
      nbchances: 1,
      a: '[-9;9]',
      b: '[-9;9]',
      c: '[-9;9]',
      pe_1: 'bien',
      pe_2: 'confusion des deux parties',
      pe_3: 'pbi',
      pe_4: 'mal',
      limite: '',
      structure: 'presentation1'
    }
  }

  function ecritPar (chaine, nombre) {
    if (nombre < 0) {
      return '(-' + chaine + ')'
    } else {
      return chaine
    }
  }
  function ecritSigne (chaine, nombre) {
    if (nombre < 0) {
      return '-' + chaine
    } else {
      return '+' + chaine
    }
  }
  function ecritCoef (chaine, nombre) {
    if (nombre < 0) {
      return '-' + chaine
    } else {
      return chaine
    }
  }
  function ecritSansUn (nombre) {
    if (nombre === 1) {
      return ''
    } else {
      return nombre
    }
  }
  function maCorrection (reponseEleve) {
    // var unobjet={unechaine:"",bilan:""};
    //  me.stockage = [bonne réponse,1 ou 2,reelle,imaginaire];
    let unechaine = ds.textes.correction3
    const typequestion = me.stockage[1]// 1 ou 2
    let bilan = 'autre'
    // a+ib OU ib+a

    if (
      ((typequestion === 1) && (reponseEleve === me.stockage[3])) ||
            ((typequestion === 2) && (reponseEleve === me.stockage[2]))
    ) {
      bilan = 'confusion'
      unechaine = ds.textes.correction1
    }

    if ((reponseEleve === 'i' + me.stockage[0]) ||
              (reponseEleve === me.stockage[0] + 'i') ||
              ((reponseEleve === 'i') && (me.stockage[0] === 1)) ||
              ((reponseEleve === '-i') && (me.stockage[0] === -1))
    ) {
      bilan = 'pbi'
      unechaine = ds.textes.correction2
    }
    stor.zoneExpli.style.color = me.styles.petit.correction.color
    me.zoneCorr1 = j3pAddElt(stor.zoneExpli, 'div')
    me.zoneCorr2 = j3pAddElt(stor.zoneExpli, 'div')
    j3pAffiche(me.zoneCorr1, '', unechaine, { a: me.stockage[0] })
    const phraseCorrige = (typequestion === 1) ? ds.textes.correction4 : ds.textes.correction5
    const divCorrection = j3pAddElt(me.zoneCorr2, 'div')
    j3pAffiche(divCorrection, '', '$' + me.stockage[7] + '$' + phraseCorrige + '$' + j3pVirgule(me.stockage[0]) + '$', { x: Math.abs(me.stockage[4]), y: ecritSansUn(Math.abs(me.stockage[5])), z: Math.abs(me.stockage[6]), w: Math.abs(me.stockage[2]), v: Math.abs(me.stockage[3]) })
    return bilan
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
    me.stockage = [0, 0, 0, 0]
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(ds.textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
  }
  function enonceMain () {
    const partie = j3pGetRandomInt(1, 2)
    let a, b, c
    do {
      a = j3pGetRandomInt(ds.a)
      b = j3pGetRandomInt(ds.b)
      c = j3pGetRandomInt(ds.c)
    } while ((a * b * c === 0) || (c === 1) || (c === -1) || (a === b))
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '25px' }) })

    let debutquestion = ''
    switch (partie) {
      case 1:
        debutquestion = ds.textes.question1
        me.stockage = [a * c, 1, a * c, b * c, a, b, c]
        break
      case 2:
        debutquestion = ds.textes.question2
        me.stockage = [b * c, 2, a * c, b * c, a, b, c]
        break
    }

    let lexpression1
    // Normalement $z_1=£z\\times£x+£z\\times£yi=£w+£vi$
    const lacorrection = 'z_1=' + ecritCoef('£z', c) + '\\times' + ecritPar('£x', a) + ecritSigne('£z', c) + '\\times' + ecritPar('£yi', b) + '=' + ecritCoef('£w', a * c) + ecritSigne('£v', b * c) + 'i'
    if (b > 0) {
      lexpression1 = (b === 1) ? '$z_1=£z(£x+i)$' : '$z_1=£z(£x+£yi)$'
    } else {
      lexpression1 = (b === -1) ? '$z_1=£z(£x-i)$' : '$z_1=£z(£x£yi)$'
    }
    // pour la correction détaillée :
    me.stockage[7] = lacorrection
    const divEnonce = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(divEnonce, '', debutquestion, { x: a, y: b, z: c, z1: j3pChaine(lexpression1, { x: a, y: b }) })

    const zoneSaisie = j3pAddElt(stor.conteneur, 'div')
    const elts = j3pAffiche(zoneSaisie, '', '&1&', { inputmq1: { texte: '' } })
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '30px' } })
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '10px' }) })
    stor.zoneInput = elts.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.-+i')
    j3pFocus(stor.zoneInput)
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce': {
      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }

      enonceMain()

      break // case "enonce":
    }
    case 'correction': {
      stor.zoneCorr.innerHTML = ''
      // on teste si une réponse a été saisie

      // var repEleve = j3pValeurde('affiche1input1')
      const repEleve = $(stor.zoneInput).mathquill('latex')
      if (!repEleve && !me.isElapsed) {
        me.reponseManquante(stor.zoneCorr)
        j3pFocus(stor.zoneInput)
      } else { // une réponse a été saisie
        if (j3pCalculValeur(repEleve) === j3pNombreBienEcrit(me.stockage[0]) || repEleve === String(me.stockage[0])) {
          me._stopTimer()
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          me.typederreurs[0]++
          stor.zoneInput.style.color = me.styles.cbien
          j3pDesactive(stor.zoneInput)
          j3pFreezeElt(stor.zoneInput)
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          // à cause de la limite de temps :
          stor.zoneInput.style.color = me.styles.cfaux
          if (me.isElapsed) { // limite de temps
            me._stopTimer()
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            // S’il y a deux chances,on appelle à nouveau le bouton Valider
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              //   me.typederreurs[1]++;
              // indication éventuelle ici
              me.etat = 'correction'
              j3pFocus(stor.zoneInput)
              me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
            } else {
              // Erreur au second essai
              me._stopTimer()
              stor.zoneExpli.style.display = ''
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              const ch = maCorrection(repEleve)
              switch (ch) {
                case 'confusion':
                  me.typederreurs[1]++
                  break
                case 'pbi':
                  me.typederreurs[2]++
                  break
                case 'autre':
                  me.typederreurs[3]++
                  break
              }
              j3pDesactive(stor.zoneInput)
              j3pBarre(stor.zoneInput)
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems
        if (((me.typederreurs[1] / ds.nbitems) >= ds.seuil_confusion) || ((me.typederreurs[2] / ds.nbitems) >= ds.seuil_pbi)) {
          if ((me.typederreurs[1] / ds.nbitems) >= ds.seuil_confusion) {
            // je détecte la confusion entre les deux parties
            me.parcours.pe = ds.pe_2
          }
          if ((me.typederreurs[2] / ds.nbitems) >= ds.seuil_pbi) {
            // je détecte la présence de i dans la réponse
            me.parcours.pe = ds.pe_3
          }
        } else {
          // sinon
          if (pourcentagereussite >= ds.seuilreussite) {
            // c’est bon
            me.parcours.pe = ds.pe_1
          } else {
            // pas bon
            me.parcours.pe = ds.pe_4
          }
        }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()

      break // case "navigation":
  }
}
