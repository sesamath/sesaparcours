import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pChaine, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pNombreBienEcrit } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, j3pMathsAjouteDans, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Auteur : Laurent Fréguin, puis Alexis Lecomte pour maj et modifs
    Date : 2012 -> juin 2013
    Remarques : section qualitative (distinguant la confusion de deux parties) demandant la partie réelle ou imaginaire de ki(a+ib)
    Section revue par Yves pour adaptation clavier virtuel en 10-2021
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 10, 'entier', 'aucun'],
    ['indication', 'Développe mentalement pour distinguer les deux parties.', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['seuilreussite', 0.7, 'reel', 'Permet de gérer l’obtention de la pe 1'],
    ['seuil_confusion', 0.2, 'reel', 'Permet de gérer l’obtention de la pe 4'],
    ['seuil_pbi', 0.2, 'reel', 'Permet de gérer l’obtention de la pe 3'],
    ['seuil_pbi_carre', 0.2, 'reel', 'Permet de gérer l’obtention de la pe 2'],
    ['a', '[3;12]', 'string', 'coefficient a de ci(a+ib)'],
    ['b', '[-9;9]', 'string', 'coefficient b de ci(a+ib)'],
    ['c', '[-9;9]', 'string', 'coefficient c de ci(a+ib)']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'pb i' },
    { pe_3: 'pb i carré' },
    { pe_4: 'confusion des deux parties' },
    { pe_5: 'mal' }
  ]

}

/**
 * section 602bis
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 10,
      nbetapes: 1,
      seuilreussite: 0.7,
      seuil_pbi: 0.2,
      seuil_confusion: 0.2,
      seuil_pbi_carre: 0.2,
      indication: 'Développe mentalement pour distinguer les deux parties',
      textes: {
        titre_exo: 'Parties réelles et imaginaires de ki(a+ib)',
        question1: 'Détermine la partie réelle de £z.',
        question2: 'Détermine la partie imaginaire de £z.',
        correction1: 'Tu confonds partie réelle et partie imaginaire.<br> La bonne réponse est $£a$.',
        correction2: 'Attention à ne pas inclure i dans la réponse....<br> La bonne réponse est $£a$.',
        correction3: 'Peut-être une erreur de distributivité...<br>La bonne réponse est $£a$.',
        correction4: 'Attention, i²=-1, la bonne réponse est ' + '$£a$.',
        correction5: ' de partie réelle $£r$.',
        correction6: ' de partie imaginaire $£r$.'
      },

      a: '[3;12]',
      b: '[-9;9]',
      c: '[-9;9]',
      pe_1: 'bien',
      pe_2: 'pb i',
      pe_3: 'pb i carré',
      pe_4: 'confusion des deux parties',
      pe_5: 'mal',

      structure: 'presentation1'
    }
  }
  function ecritPar (chaine, nombre) {
    if (nombre < 0) {
      return '(-' + chaine + ')'
    } else {
      return chaine
    }
  }
  function ecritSigne (chaine, nombre) {
    if (nombre < 0) {
      return '-' + chaine
    } else {
      return '+' + chaine
    }
  }
  function ecritCoef (chaine, nombre) {
    if (nombre < 0) {
      return '-' + chaine
    } else {
      return chaine
    }
  }
  function ecritSansUn (nombre) {
    if (nombre === 1) {
      return ''
    } else {
      return nombre
    }
  }
  function macorrection (reponseeleve) {
    // var unobjet={unechaine:"",bilan:""};
    let unechaine = ds.textes.correction3
    let bilan = 'autre'
    // a+ib OU ib+a
    if (((me.storage.solution[0] === me.stockage[0]) && (reponseeleve === me.stockage[1])) ||
             ((me.storage.solution[0] === me.stockage[1]) && (reponseeleve === me.stockage[0]))
    ) {
      bilan = 'confusion'
      unechaine = ds.textes.correction1
    }

    if ((reponseeleve === 'i' + me.storage.solution[0]) ||
              (reponseeleve === me.storage.solution[0] + 'i') ||
              ((reponseeleve === 'i') && (me.storage.solution[0] === 1)) ||
              ((reponseeleve === '-i') && (me.storage.solution[0] === -1))
    ) {
      bilan = 'pb i'
      unechaine = ds.textes.correction2
    }
    if ((me.stockage[3] === 1) && (reponseeleve === -me.storage.solution[0])) {
      bilan = 'pb i carré'
      unechaine = ds.textes.correction4
    }
    stor.zoneExpli.style.color = me.styles.petit.correction.color
    stor.zoneCorr1 = j3pAddElt(stor.zoneExpli, 'div')
    stor.zoneCorr2 = j3pAddElt(stor.zoneExpli, 'div')
    stor.zoneCorr2.style.paddingTop = '20px'
    j3pAffiche(stor.zoneCorr1, '', unechaine, { a: me.storage.solution[0] })

    const phraseCorrige = (me.stockage[3] === 1) ? ds.textes.correction5 : ds.textes.correction6
    j3pAffiche(stor.zoneCorr2, '', me.stockage[7] + ',\n' + phraseCorrige, { x: Math.abs(me.stockage[4]), y: ecritSansUn(Math.abs(me.stockage[5])), z: Math.abs(me.stockage[6]), w: Math.abs(me.stockage[1]), v: Math.abs(me.stockage[0]), r: String(me.storage.solution[0]) })

    return bilan
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
    me.stockage = [0, 0, 0, 0]
    me.storage.solution = []
    me.typederreurs = [0, 0, 0, 0]
    me.score = 0
    me.afficheTitre(ds.textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
  }
  switch (me.etat) {
    case 'enonce':

      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }
      {
        const sortequestion = j3pGetRandomInt(1, 2)
        const partie = j3pGetRandomInt(1, 2)
        let a, b, c
        do {
          const [borneInfa, borneSupa] = j3pGetBornesIntervalle(ds.a)
          const [borneInfb, borneSupb] = j3pGetBornesIntervalle(ds.b)
          const [borneInfc, borneSupc] = j3pGetBornesIntervalle(ds.c)
          a = j3pGetRandomInt(borneInfa, borneSupa)
          b = j3pGetRandomInt(borneInfb, borneSupb)
          c = j3pGetRandomInt(borneInfc, borneSupc)
        } while ((a * b * c === 0) || (a === -b) || (c === 1) || (c === -1) || (a === b))

        stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '25px' }) })

        me.stockage = [-b * c, a * c, sortequestion, partie, a, b, c]

        const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
        const zoneCons2 = j3pAddElt(stor.conteneur, 'div')

        let lexpression1 = ''// Normalement $z_1=£x+£yi$
        if (b > 0) {
          if (b === 1) {
            lexpression1 = '$z_1=£zi(£x+i)$'
          }
          if (b !== 1) {
            lexpression1 = '$z_1=£zi(£x+£yi)$'
          }
        } else {
          if (b === -1) {
            lexpression1 = '$z_1=£zi(£x-i)$'
          }
          if (b !== -1) {
            lexpression1 = '$z_1=£zi(£x£yi)$'
          }
        }
        let lexpression2 = ''/// /Normalement $z_1=£yi+£x$
        // les deux non nuls
        if (b > 0) {
          if ((b === 1) && (a > 0)) {
            lexpression2 = '$z_1=£zi(i+£x)$'
          }
          if ((b === 1) && (a < 0)) {
            lexpression2 = '$z_1=£zi(i£x)$'
          }
          if ((b !== 1) && (a > 0)) {
            lexpression2 = '$z_1=£zi(£yi+£x)$'
          }
          if ((b !== 1) && (a < 0)) {
            lexpression2 = '$z_1=£zi(£yi£x)$'
          }
        } else {
          if ((b === -1) && (a > 0)) {
            lexpression2 = '$z_1=£zi(-i+£x)$'
          }
          if ((b === -1) && (a < 0)) {
            lexpression2 = '$z_1=£zi(-i£x)$'
          }
          if ((b !== -1) && (a > 0)) {
            lexpression2 = '$z_1=£zi(£yi+£x)$'
          }
          if ((b !== -1) && (a < 0)) {
            lexpression2 = '$z_1=£zi(£yi£x)$'
          }
        }
        let lacorrection
        switch (partie) {
          case 1:
            j3pAffiche(zoneCons1, '', ds.textes.question1, { z: j3pChaine(lexpression1, { x: a, y: b, z: c }) })
            break
          case 2:
            j3pAffiche(zoneCons1, '', ds.textes.question2, { z: j3pChaine(lexpression1, { x: a, y: b, z: c }) })
            break
        }

        switch (sortequestion) {
          case 1:
            lacorrection = '$z_1=' + ecritCoef('£zi', c) + ' \\times ' + ecritPar('£x', a) + ecritSigne('£zi', c) + ' \\times ' + ecritPar('£yi', b) + '=' + ecritCoef('£w', a * c) + 'i' + ecritSigne('£v', -b * c) + '=' + ecritCoef('£v', -b * c) + ecritSigne('£w', a * c) + 'i$'
            if (partie === 1) {
              me.storage.solution[0] = -b * c
            } else {
              me.storage.solution[0] = a * c
            }
            break
          case 2: {
            const content = '&nbsp;&nbsp;&nbsp;&nbsp;' + lexpression2
            j3pMathsAjouteDans(zoneCons2, { content, parametres: { x: a, y: b, z: c } })
            lacorrection = '$z_1=' + ecritCoef('£zi', c) + ' \\times ' + ecritPar('£yi', b) + ecritSigne('£zi', c) + ' \\times ' + ecritPar('£x', a) + '=' + ecritCoef('£v', -b * c) + ecritSigne('£w', a * c) + 'i$'
            if (partie === 1) {
              me.storage.solution[0] = -b * c
            } else {
              me.storage.solution[0] = a * c
            }
            break
          }
        }

        me.stockage[7] = lacorrection
        const zoneCons3 = j3pAddElt(stor.conteneur, 'div')
        const elt = j3pAffiche(zoneCons3, '', '&1&', { inputmq1: { texte: '' } })
        stor.zoneInput = elt.inputmqList[0]
        mqRestriction(stor.zoneInput, '\\d-+i')
        stor.zoneExpli = j3pAddElt(stor.conteneur, 'div')
        stor.zoneExpli.style.paddingTop = '20px'
        stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '10px' }) })
        j3pFocus(stor.zoneInput)
      }
      me.finEnonce()
      break // case "enonce":

    case 'correction':
      me.cacheBoutonValider()
      stor.zoneCorr.innerHTML = ''
      // on teste si une réponse a été saisie
      {
        const repEleve = $(stor.zoneInput).mathquill('latex')
        // j3pValeurde('affiche1input1')
        let ch
        if (!repEleve && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
        } else { // une réponse a été saisie
          if (j3pCalculValeur(repEleve) === j3pNombreBienEcrit(me.storage.solution[0]) || repEleve === String(me.storage.solution[0])) {
            me.score++
            me._stopTimer()
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            j3pDesactive(stor.zoneInput)
            stor.zoneInput.style.color = me.styles.cbien
            j3pFreezeElt(stor.zoneInput)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneInput.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me._stopTimer()
              me.typederreurs[10]++
              ch = macorrection(repEleve)
              j3pDesactive(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)
              // je classe cette erreur comme 'autre'
              me.typederreurs[4]++
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                //  me.typederreurs[1]++;
                // indication éventuelle ici
                me.etat = 'correction'
                // j3pFocus("affiche1inputmq1");
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
                j3pFocus(stor.zoneInput)
              } else {
              // Erreur au second essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me._stopTimer()
                ch = macorrection(repEleve)
                switch (ch) {
                  case 'pb i':
                    me.typederreurs[1]++
                    break
                  case 'pb i carré':
                    me.typederreurs[2]++
                    break
                  case 'confusion':
                    me.typederreurs[3]++
                    break
                  case 'autre':
                    me.typederreurs[4]++
                    break
                }
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break

    case 'navigation':
      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems
        if (((me.typederreurs[3] / ds.nbitems) >= ds.seuil_confusion) || ((me.typederreurs[1] / ds.nbitems) >= ds.seuil_pbi || ((me.typederreurs[2] / ds.nbitems) >= ds.seuil_pbi_carre))) {
          if ((me.typederreurs[3] / ds.nbitems) >= ds.seuil_confusion) {
            // je détecte la confusion entre les deux parties
            me.parcours.pe = ds.pe_4
          }
          if ((me.typederreurs[1] / ds.nbitems) >= ds.seuil_pbi) {
            // je détecte la présence de i dans la réponse
            me.parcours.pe = ds.pe_2
          }
          if ((me.typederreurs[2] / ds.nbitems) >= ds.seuil_pbi_carre) {
            // je détecte un pb de calcul de i^2=-1 dans la réponse
            me.parcours.pe = ds.pe_3
          }
        } else {
          // sinon
          if (pourcentagereussite >= ds.seuilreussite) {
            // c’est bon
            me.parcours.pe = ds.pe_1
          } else {
            // pas bon
            me.parcours.pe = ds.pe_5
          }
        }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
