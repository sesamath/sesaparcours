import { j3pAddElt } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'

/*
 *  Cours : Distributivité.
 */
export const params = { outils: ['calculatrice', 'mathquill'] }

const propriete = ` 
  Pour tous nombres complexes $k$, $a$ et $b$, on a :
  <span class="rouge">$k$</span>(<span class="vert">$a$</span>+<span class="bleu">$b$</span>)$ =
  $<span class="rouge">$k$</span><span class="vert">$a$</span>$+$<span class="rouge">$k$</span><span  class="bleu">$b$</span>.
`
const exemples = [
  '$2(x+3)=2x+6$.',
  '$4(2+4i)=8+16i$.',
  '$-5i(3+2i)=-5i \\times 3-5i \\times 2i=-15i+10=10-15i$'
]

export default function main () {
  if (this.etat === 'enonce') {
    if (!this.debutDeLaSection) {
      // on passe jamais là, y’a qu’une question
      this.notif(Error('Il ne devrait y avoir qu’une seule répétition'))
    }
    this.setPassive()
    this.construitStructurePage('presentation2')
    this.afficheTitre('Distributivité')
    const divCours = j3pAddElt(this.zonesElts.MG, 'div', '', { style: this.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    j3pAffiche(divCours, '', propriete)
    const ulExemple = j3pAddElt(this.zonesElts.MG, 'ul', 'Exemple :')
    for (const exemple of exemples) {
      const li = j3pAddElt(ulExemple, 'li')
      j3pAffiche(li, '', exemple)
    }
    this.finEnonce()
  }
  // section passive, pas de case correction ni navigation
}
