import { j3pAddElt, j3pAjouteBouton, j3pBarre, j3pNombre, j3pDetruit, j3pEnvironEgal, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pNombreBienEcrit, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
 *
 * Calcul de la valeur approchée du module d’un complexe, avec calculatrice
 * Section corrigée et adaptée au clavier virtuel par Yves en 10-2021
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['a', '[-9;9]', 'string', 'Intervalle d’appartenance de a (partie réelle)'],
    ['b', '[-9;9]', 'string', 'Intervalle d’appartenance de b']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 5,
      nbetapes: 1,

      textes: {
        titreExo: 'Calculer un module',
        question1: "Calculer la valeur approchée à 0,1 près du <span class='rouge'>module </span>du complexe suivant&nbsp;:",
        question2: "Donner la partie <span class='rouge'>imaginaire</span> du complexe suivant&nbsp;:",
        correction1: ' La bonne réponse est $£a$ car $\\sqrt{£b^2+£c^2}\\simeq £a$.',
        consigne1: 'à 0,1 près, le module vaut : &1&'
      },
      indication: '',
      a: '[-9;9]',
      b: '[-9;9]',
      nbchances: 1,

      structure: 'presentation1'
    }
  }

  function gereParenthese (nb) {
    return (nb < 0)
      ? '(' + nb + ')'
      : String(nb)
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
    me.stockage = [0, 0, 0, 0]
    me.storage.solution = []
    me.typederreurs = [0, 0, 0, 0]
    me.afficheTitre(ds.textes.titreExo, true)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
  }
  // DEBUT DE LA SECTION
  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '30px' }) })
    let a, b
    do {
      const [borneInfa, borneSupa] = j3pGetBornesIntervalle(ds.a)
      const [borneInfb, borneSupb] = j3pGetBornesIntervalle(ds.b)
      a = j3pGetRandomInt(borneInfa, borneSupa)
      b = j3pGetRandomInt(borneInfb, borneSupb)
    } while ((a * b === 0))

    const sortequestion = j3pGetRandomInt(1, 2)
    me.stockage = [a, b]

    let lexpression1 = ''// Normalement $z_1=£x+£yi$
    if (a === 0) {
      if (b === 1) {
        lexpression1 = '$z_1=i$'
      }
      if (b === -1) {
        lexpression1 = '$z_1=-i$'
      }
      if ((b !== 1) && (b !== -1)) {
        lexpression1 = '$z_1=£yi$'
      }
    } else {
      if (b === 0) {
        lexpression1 = '$z_1=£x$'
      } else {
        // les deux non nuls
        if (b > 0) {
          if (b === 1) {
            lexpression1 = '$z_1=£x+i$'
          }
          if (b !== 1) {
            lexpression1 = '$z_1=£x+£yi$'
          }
        } else {
          if (b === -1) {
            lexpression1 = '$z_1=£x-i$'
          }
          if (b !== -1) {
            lexpression1 = '$z_1=£x£yi$'
          }
        }
      }
    }
    let lexpression2 = ''/// /Normalement $z_1=£yi+£x$
    if (a === 0) {
      if (b === 1) {
        lexpression2 = '$z_1=i$'
      }
      if (b === -1) {
        lexpression2 = '$z_1=-i$'
      }
      if ((b !== 1) && (b !== -1)) {
        lexpression2 = '$z_1=£yi$'
      }
    } else {
      if (b === 0) {
        lexpression2 = '$z_1=£x$'
      } else {
        // les deux non nuls
        if (b > 0) {
          if ((b === 1) && (a > 0)) {
            lexpression2 = '$z_1=i+£x$'
          }
          if ((b === 1) && (a < 0)) {
            lexpression2 = '$z_1=i£x$'
          }
          if ((b !== 1) && (a > 0)) {
            lexpression2 = '$z_1=£yi+£x$'
          }
          if ((b !== 1) && (a < 0)) {
            lexpression2 = '$z_1=£yi£x$'
          }
        } else {
          if ((b === -1) && (a > 0)) {
            lexpression2 = '$z_1=-i+£x$'
          }
          if ((b === -1) && (a < 0)) {
            lexpression2 = '$z_1=-i£x$'
          }
          if ((b !== -1) && (a > 0)) {
            lexpression2 = '$z_1=£yi+£x$'
          }
          if ((b !== -1) && (a < 0)) {
            lexpression2 = '$z_1=£yi£x$'
          }
        }
      }
    }

    // me.storage.solution[0]=a+b;
    me.storage.solution[0] = Math.sqrt(a * a + b * b)
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    const zoneCons3 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons1, '', ds.textes.question1)
    switch (sortequestion) {
      case 1:
        j3pAffiche(zoneCons2, '', lexpression1, { x: a, y: b })
        break
      case 2:
        j3pAffiche(zoneCons2, '', lexpression2, { x: a, y: b })
        break
    }

    const elt = j3pAffiche(zoneCons3, '', ds.textes.consigne1, {
      inputmq1: { texte: '', dynamique: true }
    })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,-.')
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '20px' }) })
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('moyen.explications', { paddingTop: '30px' }) })
    stor.zoneExpli.style.paddingTop = '10px'

    j3pFocus(stor.zoneInput)

    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':

      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }
      enonceMain()

      break // case "enonce":

    case 'correction':

      // me.cacheBoutonValider()
      stor.zoneCorr.innerHTML = ''
      // on teste si une réponse a été saisie
      {
        const repEleve = j3pValeurde(stor.zoneInput)

        if (!repEleve && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
        } else { // une réponse a été saisie
          if (j3pEnvironEgal(j3pNombre(repEleve), me.storage.solution[0], 0.1)) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            j3pDetruit(stor.zoneCalc)
            j3pDetruitFenetres('Calculatrice')
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            stor.zoneInput.style.color = me.styles.cfaux
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              j3pDesactive(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)
              j3pDetruit(stor.zoneCalc)
              j3pDetruitFenetres('Calculatrice')
              j3pAffiche(stor.zoneExpli, '', ds.textes.correction1, {
                a: j3pNombreBienEcrit(Math.round(10 * me.storage.solution[0]) / 10),
                b: gereParenthese(me.storage.solution[0]),
                c: gereParenthese(me.stockage[1])
              })
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                j3pFocus(stor.zoneInput)
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
                me._stopTimer()
                // Erreur au second essai
                j3pAffiche(stor.zoneExpli, '', ds.textes.correction1, {
                  a: j3pNombreBienEcrit(Math.round(10 * me.storage.solution[0]) / 10),
                  b: gereParenthese(me.stockage[0]),
                  c: gereParenthese(me.stockage[1])
                })
                me.typederreurs[2]++
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                j3pFreezeElt(stor.zoneInput)
                j3pDetruit(stor.zoneCalc)
                j3pDetruitFenetres('Calculatrice')
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()

      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
