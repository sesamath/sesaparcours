import { j3pAddElt, j3pEmpty, j3pEnvironEgal, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen
    2012-2013
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['a', '[-4;4]', 'string', 'Intervalle pour l’abscisse'],
    ['b', '[-4;4]', 'string', 'Intervalle pour l’ordonnée'],
    ['seuilreussite', 0.7, 'reel', 'Permet de gérer l’obtention de la pe 1'],
    ['seuil_confusion', 0.2, 'reel', 'Permet de gérer l’obtention de la pe 2']
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'Confusion des deux coordonnées' },
    { pe_3: 'insuffisant' }
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function getDonnees () {
    return {
      // mieux vaut assurer ici et forcer le clic

      typesection: 'lycee',
      nbrepetitions: 5,
      nbetapes: 1,
      seuilreussite: 0.7,
      seuil_confusion: 0.2,
      figure: '@options;\nrepereortho(150,150,30,1,1){ 0 , moyen , noir , num1 };grille();\n@figure;\nM = point(-1,2) {2};\n',
      textes: {
        titreExo: 'Placer l’image d’un complexe',
        question1_1: 'Soit le complexe ',
        question1_2: 'Placer l’image M de ce complexe dans le repère.',
        question1_3: '', // Une légère marge d’erreur est tolérée.<br>",
        correction1: ' Voici l’image de M :<br>',
        correction2: 'Tu confonds abscisse et ordonnee.<br> Voici l’image de M:',
        correction3: 'En effet,',
        correction4: 'la partie réelle est égale à ',
        correction5: 'et la partie imaginaire est égale à '
      },
      indication: 'Identifier la partie imaginaire du complexe : quel réel multiplie i ? Cette partie imaginaire est l’ordonnée...',
      nbchances: 1,

      a: '[-4;4]',
      b: '[-4;4]',

      pe_2: 'Confusion des deux coordonnées', // à partir de this.seuil_confusion
      // SINON
      pe_1: 'bien', // à partir de this.seuilreussite
      pe_3: 'insuffisant', // si moins de this.seuilreussite

      structure: 'presentation1'
    }
  }

  function macorrection (reponseeleve, solution) {
    const unobjet = { unechaine: '', bilan: '' }
    unobjet.unechaine = ds.textes.correction1
    unobjet.bilan = 'autre'
    if ((j3pEnvironEgal(stor.reponseEleve[0], solution[0][1], 0.2)) && (j3pEnvironEgal(stor.reponseEleve[1], solution[0][0], 0.2))) {
      unobjet.bilan = 'inversion'
      unobjet.unechaine = ds.textes.correction2
    }
    return unobjet
  }
  function enonceMain () {
    stor.enonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '15px' }) })
    stor.divRepere = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '15px' }) })
    stor.unrepere = j3pGetNewId('unrepere')
    stor.repere = new Repere({
      idConteneur: stor.divRepere,
      idDivRepere: stor.unrepere,
      aimantage: true,
      visible: true,
      trame: true,
      fixe: false,
      larg: 400,
      haut: 380,

      pasdunegraduationX: 1,
      pixelspargraduationX: 30,
      pasdunegraduationY: 1,
      pixelspargraduationY: 30,
      xO: 200,
      yO: 190,
      debuty: 0,
      negatifs: true,
      objets: [
        {
          type: 'point',
          nom: 'M',
          par1: -5.5,
          par2: -5.5,
          fixe: false,
          visible: true,
          etiquette: true,
          style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 5 }
        }
      ]
    })
    stor.repere.construit()
    let a, b
    do {
      const [minInta, maxInta] = j3pGetBornesIntervalle(ds.a)
      a = j3pGetRandomInt(minInta, maxInta)
      const [minIntb, maxIntb] = j3pGetBornesIntervalle(ds.b)
      b = j3pGetRandomInt(minIntb, maxIntb)
    } while (((a === 0) && (b === 0)) || (a === b))

    let lexpression1 = ''// Normalement $z_1=£x+£yi$
    if (a === 0) {
      if (b === 1) { lexpression1 = '$z_1=i$' }
      if (b === -1) { lexpression1 = '$z_1=-i$' }
      if ((b !== 1) && (b !== -1)) { lexpression1 = '$z_1=£yi$' }
    } else {
      if (b === 0) {
        lexpression1 = '$z_1=£x$'
      } else {
        // les deux non nuls
        if (b > 0) {
          if (b === 1) { lexpression1 = '$z_1=£x+i$' }
          if (b !== 1) { lexpression1 = '$z_1=£x+£yi$' }
        } else {
          if (b === -1) { lexpression1 = '$z_1=£x-i$' }
          if (b !== -1) { lexpression1 = '$z_1=£x£yi$' }
        }
      }
    }

    j3pAffiche(stor.enonce, '', ds.textes.question1_1 + ' : ' + lexpression1 + '\n\n' + ds.textes.question1_2,
      {
        x: a,
        y: b
      }
    )

    stor.solution[0] = [a, b]

    stor.explications = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '10px' }) })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté UNE SEULE FOIS avant le premier item

      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        stor.solution = []
        me.typederreurs = [0, 0, 0, 0]
        me.score = 0

        me.afficheTitre(ds.textes.titreExo, true)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      enonceMain()

      break // case "enonce"

    case 'correction': {
      stor.explications.innerHTML = ''
      // on teste si une réponse a été saisie
      stor.reponseEleve = stor.repere.getObjet('M')
      // const aRepondu = (j3pEnvironEgal(stor.reponseEleve.x, -5.5, 0.2) && j3pEnvironEgal(stor.reponseEleve.y, -5.5, 0.2))
      const aRepondu = false
      // on pourrait tester si le point a bougé...
      if (aRepondu && (!me.isElapsed)) {
        me.reponseManquante(stor.explications)
      } else { // une réponse a été saisie
        const bonneReponse = j3pEnvironEgal(stor.reponseEleve.x, stor.solution[0][0], 0.2) && j3pEnvironEgal(stor.reponseEleve.y, stor.solution[0][1], 0.2)
        if (bonneReponse) {
          me._stopTimer()

          me.score++
          stor.explications.style.color = me.styles.cbien
          stor.explications.innerHTML = cBien
          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.explications.style.color = me.styles.cfaux
          // à cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()
            stor.explications.innerHTML = tempsDepasse
            const ch = macorrection([stor.reponseEleve.x, stor.reponseEleve.y], stor.solution)
            stor.explications.innerHTML += '<br>' + ch.unechaine
            stor.explications.innerHTML += '<br>' + ds.textes.correction3
            stor.explications.innerHTML += '<br>' + ds.textes.correction4
            stor.explications.innerHTML += '<br>' + ds.textes.correction5
            me.typederreurs[10]++
            j3pEmpty(stor.divRepere)
            stor.repere = new Repere({
              idConteneur: stor.divRepere,
              idDivRepere: stor.unrepere,
              aimantage: false,
              visible: true,
              trame: true,
              fixe: true,
              larg: 400,
              haut: 380,

              pasdunegraduationX: 1,
              pixelspargraduationX: 30,
              pasdunegraduationY: 1,
              pixelspargraduationY: 30,
              xO: 200,
              yO: 190,
              debuty: 0,
              negatifs: true,
              objets: [
                {
                  type: 'point',
                  nom: 'M',
                  par1: stor.reponseEleve.x,
                  par2: stor.reponseEleve.y,
                  fixe: true,
                  visible: true,
                  etiquette: true,
                  style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 5 }
                },
                {
                  type: 'point',
                  nom: 'Solution',
                  par1: stor.solution[0][0],
                  par2: stor.solution[0][1],
                  fixe: true,
                  visible: true,
                  etiquette: true,
                  style: { couleur: '#F00', epaisseur: 2, taille: 18, taillepoint: 5 }
                },
                {
                  type: 'point',
                  nom: 'Solution1',
                  par1: stor.solution[0][0],
                  par2: 0,
                  fixe: true,
                  visible: true,
                  etiquette: false,
                  style: { couleur: '#F00', epaisseur: 2, taille: 18, taillepoint: 4 }
                },
                {
                  type: 'point',
                  nom: 'Solution2',
                  par1: 0,
                  par2: stor.solution[0][1],
                  fixe: true,
                  visible: true,
                  etiquette: false,
                  style: { couleur: '#F00', epaisseur: 2, taille: 18, taillepoint: 4 }
                }
              ]
            })
            stor.repere.construit()
            me.etat = 'navigation'
            me.sectionCourante()
          } else { // réponse fausse :
            stor.explications.innerHTML = cFaux
            // S’il y a deux chances,on appelle à nouveau le bouton Valider
            if (me.essaiCourant < ds.nbchances) {
              stor.explications.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              me.etat = 'correction'
              // j3pFocus("affiche1input1");
              me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
            } else {
              // Erreur au second essai
              me._stopTimer()
              const ch = macorrection([stor.reponseEleve.x, stor.reponseEleve.y], stor.solution)
              stor.explications.innerHTML += ch.unechaine

              stor.explications.innerHTML += '<br>' + ds.textes.correction3
              stor.explications.innerHTML += '<br>' + ds.textes.correction4 + stor.solution[0][0]
              stor.explications.innerHTML += '<br>' + ds.textes.correction5 + stor.solution[0][1]
              // me.typederreurs[2]++;
              // document.getElementById("Enonce").innerHTML="";
              j3pEmpty(stor.divRepere)
              stor.repere = new Repere({
                idConteneur: stor.divRepere,
                idDivRepere: stor.unrepere,
                aimantage: false,
                visible: true,
                trame: true,
                fixe: true,
                larg: 400,
                haut: 380,

                pasdunegraduationX: 1,
                pixelspargraduationX: 30,
                pasdunegraduationY: 1,
                pixelspargraduationY: 30,
                xO: 200,
                yO: 190,
                debuty: 0,
                negatifs: true,
                objets: [
                  {
                    type: 'point',
                    nom: 'M',
                    par1: stor.reponseEleve.x,
                    par2: stor.reponseEleve.y,
                    fixe: true,
                    visible: true,
                    etiquette: true,
                    style: { couleur: '#000', epaisseur: 2, taille: 18, taillepoint: 5 }
                  },
                  {
                    type: 'point',
                    nom: 'Solution',
                    par1: stor.solution[0][0],
                    par2: stor.solution[0][1],
                    fixe: true,
                    visible: true,
                    etiquette: true,
                    style: { couleur: '#F00', epaisseur: 2, taille: 18, taillepoint: 5 }
                  },
                  {
                    type: 'point',
                    nom: 'Solution1',
                    par1: stor.solution[0][0],
                    par2: 0,
                    fixe: true,
                    visible: true,
                    etiquette: false,
                    style: { couleur: '#F00', epaisseur: 2, taille: 18, taillepoint: 4 }
                  },
                  {
                    type: 'point',
                    nom: 'Solution2',
                    par1: 0,
                    par2: stor.solution[0][1],
                    fixe: true,
                    visible: true,
                    etiquette: false,
                    style: { couleur: '#F00', epaisseur: 2, taille: 18, taillepoint: 4 }
                  }
                ]
              })
              stor.repere.construit()
              switch (ch.bilan) {
                case 'confusion':
                  me.typederreurs[2]++
                  break
                case 'autre':
                  me.typederreurs[3]++
                  break
              }
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems

        if ((me.typederreurs[2] / ds.nbitems) >= ds.seuil_confusion) {
          // je détecte la confusion entre les deux parties
          me.parcours.pe = ds.pe_2
        } else {
          // sinon
          if (pourcentagereussite >= ds.seuilreussite) {
            // c’est bon
            me.parcours.pe = ds.pe_1
          } else {
            // pas bon
            me.parcours.pe = ds.pe_3
          }
        }
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
