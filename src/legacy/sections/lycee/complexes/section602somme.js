import $ from 'jquery'
import { j3pAddElt, j3pAjouteFoisDevantX, j3pBarre, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMathquillXcas, j3pShowError } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// Cette section affiche une série de 10 calculs de la somme de 2 nombres conmplexes'
// Les coefficients sont entiers relatifs.

export const params = { outils: ['calculatrice', 'mathquill'] }

/**
 * section 602somme
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function depart () {
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAALgAAACVAAAAQEAAAAAAAAAAQAAAAb#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAFiAAItMf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABP#AAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wABZgAFYSp4K2L#####AAAAAQAKQ09wZXJhdGlvbgAAAAAFAv####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAB#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAYAAAACAAF4AAAABAD#####AANyZXAABTIqeC0xAAAABQEAAAAFAgAAAAFAAAAAAAAAAAAAAAcAAAAAAAAAAT#wAAAAAAAAAAF4#####wAAAAMAEENUZXN0RXF1aXZhbGVuY2UA#####wAHZWdhbGl0ZQAAAAMAAAAEAQAAAAABP#AAAAAAAAAB################'
    stor.mtgList = stor.mtgAppLecteur.createList(txtFigure)
  }
  // fonction qui teste si la réponse élève est égale à me.storage.solution, de manière formelle, et qui vérifie également que l’écriture est algébrique (un seul i)
  function analyseFormelle (reponseEleve) {
    const retour = {}
    let i
    let repEleveXcas = j3pMathquillXcas(reponseEleve.replace(/i/g, 'x'))
    repEleveXcas = j3pAjouteFoisDevantX(repEleveXcas, 'x')
    stor.mtgList.giveFormula2('rep', repEleveXcas)
    stor.mtgList.calculateNG()
    retour.bon = (stor.mtgList.valueOf('egalite') === 1)
    if (!retour.bon) {
      // La réponse équivalente à la réponse attendue, mais elle est peut-être exacte mais non simplifiée
      const arbreChaine = new Tarbre(repEleveXcas + '-(' + me.storage.solution[0].replace(/i/g, 'x') + ')', ['x'])
      let egalite = true
      for (i = 1; i <= 15; i++) {
        egalite = egalite && (Math.abs(arbreChaine.evalue([i])) < Math.pow(10, -12))
      }
      retour.bon = egalite
      const formeAlg = [
        /^-?[0-9]+[+-][0-9]*\*?x$/g,
        /^-?[0-9]*\*?x$/g,
        /^-?[0-9]+$/g,
        /^-?[0-9]*\*?x[+-][0-9]+$/g
      ]
      // s’arrête au premier truc qui vérifie, cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/some
      retour.simplifie = formeAlg.some(function (regexp) {
        return regexp.test(repEleveXcas)
      })
    } else {
      retour.simplifie = true
    }
    return retour
  }

  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 10,
      nbetapes: 1,
      question1: 'Calculez la somme S des nombres complexes :',
      indication: 'La partie réelle (resp.imaginaire) d’une somme est la somme des parties réelles (resp.imaginaires)',
      textes: {
        titre_exo: 'Somme de nombres complexes',
        question1: 'Calculez la somme S des nombres complexes :'
      },
      nbchances: 1,
      a: '[-9;9]',
      b: '[-9;9]',
      c: '[-9;9]',
      d: '[-9;9]',
      structure: 'presentation1'
    }
  }

  function macorrection (reponseeleve, bool) {
    if (!bool) stor.zoneExpli.style.color = me.styles.petit.correction.color
    const unechaine = '$S=£a$'
    j3pAffiche(stor.zoneExpli, '', unechaine, { a: me.storage.solution[0] })
  }

  // Utilisée pour remettre en forme a+ib ou b+ia
  function GestionAPlusiB (a, b, Choix) {
    // Choix=1 donne a+ib et Choix=2 donne ib+a, avec les simplifications de signes.
    // En utilisant xcas, Normal(1-2x) écrit -2x+1, mais 1-2i si on utilise i à la place de x, car i vu comme complexe.
    let Stock = ''
    switch (Choix) {
      case 1:
        Stock = SimplifierEcritureDegre1(b, a, 2)
        break
      case 2:
        Stock = SimplifierEcritureDegre1(b, a, 1)
    }
    return Stock
  }

  function SimplifierEcritureDegre1 (a, b, Choix) {
    // Cette fonction fait simplifier par xcas les écritures littéralles ax+b si Choix=1
    // et b+ax si Choix=2
    // du style 1x+2=x+2 ou 2x+0=2x etc
    let Stock = ''
    switch (Choix) {
      case 2:
        // a+ib
        // Stock=xcasRequete(a+"x+"+b);
        if (a === 0) {
          if (b === 1) { Stock = 'i' }
          if (b === -1) { Stock = '-i' }
          if ((b !== 1) && (b !== -1)) { Stock = b + 'i' }
        } else {
          if (b === 0) {
            Stock = a + ''
          } else {
            // les deux non nuls
            if (b > 0) {
              if (b === 1) { Stock = a + '+i' }
              if (b !== 1) { Stock = a + '+' + b + 'i' }
            } else {
              if (b === -1) { Stock = a + '-i' }
              if (b !== -1) { Stock = a + '' + b + 'i' }
            }
          }
        }

        break
      case 1:
        // Stock=xcasRequete(b+"+"+a+"x");
        if (a === 0) {
          if (b === 1) { Stock = 'i' }
          if (b === -1) { Stock = '-i' }
          if ((b !== 1) && (b !== -1)) { Stock = b + 'i' }
        } else {
          if (b === 0) {
            Stock = a + ''
          } else {
            // les deux non nuls
            if (b > 0) {
              if ((b === 1) && (a > 0)) { Stock = 'i+' + a }
              if ((b === 1) && (a < 0)) { Stock = 'i' + a }
              if ((b !== 1) && (a > 0)) { Stock = b + 'i+' + a }
              if ((b !== 1) && (a < 0)) { Stock = b + 'i' + a }
            } else {
              if ((b === -1) && (a > 0)) { Stock = '-i+' + a }
              if ((b === -1) && (a < 0)) { Stock = '-i' + a }
              if ((b !== -1) && (a > 0)) { Stock = b + 'i+' + a }
              if ((b !== -1) && (a < 0)) { Stock = b + 'i' + a }
            }
          }
        }
        break
    }
    return Stock
  }

  function MiseEnFormeSomme (Chaine1, Chaine2, Choix) {
    // Cette fonction renvoie la nouvelle chaine Chaine1+Chaine2 si Choix=1
    // ou Chaine2+chaine1 si Choix=2
    let Stock
    if (Choix === 1) {
      if (Chaine2.charAt(0) === '-') {
        Stock = Chaine1 + Chaine2
      } else { Stock = Chaine1 + '+' + Chaine2 }
    } else {
      if (Chaine1.charAt(0) === '-') {
        Stock = Chaine2 + Chaine1
      } else { Stock = Chaine2 + '+' + Chaine1 }
    }
    return Stock
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
    me.typederreurs = [0, 0]
    me.storage.solution = []
    me.afficheTitre(ds.textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function enonceMain () {
    // On s’occupe de la question répétitive
    // On génère a et b non nuls. puis c et d avec c ou d éventuellement nul.
    let a, b, c, d
    do {
      const [borneInfa, borneSupa] = j3pGetBornesIntervalle(ds.a)
      const [borneInfb, borneSupb] = j3pGetBornesIntervalle(ds.b)
      a = j3pGetRandomInt(borneInfa, borneSupa)
      b = j3pGetRandomInt(borneInfb, borneSupb)
    } while (a * b === 0)
    do {
      const [borneInfc, borneSupc] = j3pGetBornesIntervalle(ds.c)
      const [borneInfd, borneSupd] = j3pGetBornesIntervalle(ds.d)
      c = j3pGetRandomInt(borneInfc, borneSupc)
      d = j3pGetRandomInt(borneInfd, borneSupd)
    } while ((c === 0) && (d === 0))
    // AplusIB=1 décide de a+ib et AplusIB=2 pour ib+a ainsi que CplusID
    // Ordre=1 décide de a+ib+c+id et Ordre=2 de c+id+a+ib
    me.storage.solution[0] = GestionAPlusiB(a + c, b + d, 1)
    const AplusIB = j3pGetRandomInt(1, 2)
    const CplusID = j3pGetRandomInt(1, 2)
    const Ordre = j3pGetRandomInt(1, 2)
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
    ;[1, 2, 3].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })
    j3pAffiche(stor.zoneCons1, '', ds.textes.question1)
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '20px' }) })
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div')
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div')
    stor.zoneExpli.style.color = me.styles.cbien
    stor.zoneExpli.style.paddingTop = '20px'
    const lasaisie = 'S=' + MiseEnFormeSomme(GestionAPlusiB(a, b, AplusIB), GestionAPlusiB(c, d, CplusID), Ordre)
    j3pAffiche(stor.zoneCons2, '', '$' + lasaisie + '$')
    const elt = j3pAffiche(stor.zoneCons3, '', '$S=$&1&', { inputmq1: { texte: '', correction: '' + me.storage.solution[0] } })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.+-i')
    depart()
    stor.mtgList.giveFormula2('a', String(a + c))
    stor.mtgList.giveFormula2('b', String(b + d))
    stor.mtgList.calculateNG()
    j3pFocus(stor.zoneInput)
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
        enonceMain()
      }
      break

    case 'correction':
      // obligatoire
      stor.zoneCorr.innerHTML = ''
      // on teste si une réponse a été saisie
      {
        const repEleve = $(stor.zoneInput).mathquill('latex')
        if (!repEleve && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
        } else { // une réponse a été saisie
          const reponseAnalysee = analyseFormelle(repEleve)
          if (reponseAnalysee.bon && reponseAnalysee.simplifie) {
          //  if (repEleve == j3pNombreBienEcrit(me.storage.solution[0]) || repEleve == String(me.storage.solution[0])) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneInput.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              j3pDesactive(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)
              macorrection(reponseAnalysee, false)
              // je classe cette erreur comme 'autre'
              me.typederreurs[3]++
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                macorrection(repEleve, false)
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      break

    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
