import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pBarre, j3pDetruit, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetRandomInt, j3pPaletteMathquill, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
 *Auteur : Alexie Lecomte
 *Date : 2012 -> juin 2013
 *Remarques :  Donner la valeur exacte du module d’un complexe sous forme algébrique, section quantitative
 *  Utilise MathQUILL
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', 'Le module de  z=a+ib est égal à la distance OM, où M est l’image de z.', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['niveau', 0, 'entier', '0, 1, 2 ou 3, permet de gérer la difficulte : 0 aleatoire, 1 facile, 2 moyen 3 difficile']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 5,
      nbetapes: 1,
      nbchances: 2,
      // pour gérer la difficulté :
      niveau: 0,
      textes: {
        titre_exo: 'Calculer la valeur exacte d’un module',
        question1: "Calculer la valeur exacte du <span class='rouge'>module </span>du complexe suivant et l’écrire sous la forme $£a\\sqrt{£b}$, b étant le plus petit possible :",
        correction1: ' La réponse est correcte mais non simplifiée :',
        correction2: ' La bonne réponse est',
        correction3: ' La réponse est correcte mais non simplifiée.'
      },
      // question1: "Calculer la valeur exacte du <span class='rouge'>module </span>du complexe suivant et l’écrire sous la forme $£a\\sqrt{£b}$, b étant le plus petit possible :";
      indication: 'Le module de  z=a+ib est égal à la distance OM, où M est l’image de z.',
      limite: '',
      structure: 'presentation1'
    }
  }

  function macorrection (reponseeleve, param) {
    // me.stockage=[a,b,c,sortequestion,partie];
    j3pDetruit('Calc')
    j3pDetruitFenetres('Calculatrice')
    const unobjet = {
      unechaine: '',
      bilan: ''
    }
    if (param === 1) {
      // message, non simplifiée
      unobjet.unechaine = ds.textes.correction1 + '<br>' + '$|z_1|=\\sqrt{(£u£x'
    }
    if (param === 2) {
      // pas bon
      unobjet.unechaine = ds.textes.correction2 + ' $£m\\sqrt{£n}$ :<br>' + '$|z_1|=\\sqrt{(£u£x'
    }
    if (param === 0) {
      // réponse exacte on met juste la correction détaillée
      unobjet.unechaine = '<br>' + '$|z_1|=\\sqrt{(£u£x'
    }
    if (me.stockage[2] !== '' && me.stockage[2] !== 0) {
      unobjet.unechaine += '\\sqrt{£w}'
    }
    unobjet.unechaine += ')^2 + (£v£y'
    if (me.stockage[3] !== '' && me.stockage[3] !== 0) {
      unobjet.unechaine += '\\sqrt{£t}'
    }
    unobjet.unechaine += ')^2}$ <br> $|z_1|= \\sqrt{£s + £r} = \\sqrt{£p} = £m\\sqrt{£n}$'
    unobjet.bilan = 'autre'
    j3pEmpty(stor.laPalette)
    return unobjet
  }

  function traduitChaineMathquill (ch) {
    // je retourne dans tab[0] ce qu’il y a hors racine et dans tab[1] ce qu’il y a dans la racine
    // je vire d’abord les espaces :
    // console.log("ch="+ch)
    let pos
    while (ch.indexOf(':') !== -1) {
      pos = ch.indexOf(':')
      ch = ch.substring(0, pos - 1) + ch.substring(pos + 1)
    }
    while (ch.indexOf('cdot') !== -1) {
      pos = ch.indexOf('cdot')
      ch = ch.substring(0, pos - 1) + ch.substring(pos + 4)
    }
    const tab = []
    // dans le cas où la solution est entière
    if (ch.indexOf('\\sqrt') === -1) {
      tab[0] = ch
    } else {
      // a racine(b)
      pos = ch.indexOf('\\sqrt')// on pourrait améliorer en testant si la solution est de la forme racine(b) a
      if (pos === 0) {
        // je recherche le } de fin de racine
        const pos1 = ch.indexOf('{')
        const pos2 = ch.indexOf('}')
        tab[1] = ch.substring(pos1 + 1, pos2)
        tab[0] = ch.substring(pos2 + 1)
      } else {
        tab[0] = ch.substring(0, pos)
        tab[1] = ch.substring(pos + 6, ch.length - 1)
      }
    }

    return tab
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })

    me.stockage = [0, 0, 0, 0]
    me.typederreurs = [0, 0, 0, 0]
    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
  }
  // DEBUT DE LA SECTION
  function enonceMain () {
    const tableauComplet = []

    tableauComplet[0] = [4, '', 4, '', 4, 2, 16, 16]
    tableauComplet[1] = [4, '', 2, '', 2, 5, 16, 4]
    tableauComplet[2] = [5, '', 5, '', 5, 2, 25, 25]
    tableauComplet[3] = [6, '', 2, '', 2, 10, 36, 4]
    tableauComplet[4] = [3, '', 3, '', 3, 2, 9, 9]
    tableauComplet[5] = [2, '', 8, '', 2, 17, 4, 64]

    tableauComplet[6] = ['', 24, '', 51, 5, 3, 24, 51]
    tableauComplet[7] = ['', 57, '', 41, 7, 2, 57, 41]
    tableauComplet[8] = ['', 22, '', 23, 3, 5, 22, 23]
    tableauComplet[9] = ['', 43, '', 29, 6, 2, 43, 29]
    tableauComplet[10] = ['', 17, '', 31, 4, 3, 17, 31]
    tableauComplet[11] = ['', 13, '', 7, 2, 5, 13, 7]

    tableauComplet[12] = [6, '', 2, 3, 4, 3, 36, 12]
    tableauComplet[13] = [2, 6, 2, 6, 4, 3, 24, 24]
    tableauComplet[14] = ['', 30, 2, 5, 5, 2, 30, 20]
    tableauComplet[15] = [2, 5, 2, 3, 4, 2, 20, 12]
    tableauComplet[16] = [3, 3, 3, 2, 3, 5, 27, 18]
    tableauComplet[17] = [6, '', 3, 2, 3, 6, 36, 18]
    tableauComplet[18] = ['', 30, 2, 6, 3, 6, 30, 24]
    let nbAlea
    switch (ds.niveau) {
      case 1 :
        nbAlea = j3pGetRandomInt(0, 5)
        break
      case 2 :
        nbAlea = j3pGetRandomInt(6, 11)
        break
      case 3 :
        nbAlea = j3pGetRandomInt(12, 18)
        break
      default :
        nbAlea = j3pGetRandomInt(0, (tableauComplet.length - 1))
        break
    }

    // var  nbAlea=9;
    const a = tableauComplet[nbAlea][0]
    const c = tableauComplet[nbAlea][1]
    const b = tableauComplet[nbAlea][2]
    const d = tableauComplet[nbAlea][3]
    const e = tableauComplet[nbAlea][6]
    const f = tableauComplet[nbAlea][7]
    me.stockage = [a, b, c, d, e, f]
    stor.solution = []
    stor.solution[0] = tableauComplet[nbAlea][4]
    stor.solution[1] = tableauComplet[nbAlea][5]
    stor.valSolution = tableauComplet[nbAlea][4] * Math.sqrt(tableauComplet[nbAlea][5])
    let lexpression1 = ''// Normalement $z_1=£x+£yi$

    // z1 = a racine(c) +/- b racine(d) i avec gestion des différents cas particuliers possibles
    lexpression1 = '$z_1='
    const lepremiersigne = j3pGetRandomInt(1, 2)
    const lesecondsigne = j3pGetRandomInt(1, 2)
    if (lepremiersigne === 1) {
    // si a racine(c) est non nul : (marche dans le cas où a="")
      if (!((a === 0) && (c === 0))) {
        lexpression1 += '£x'
      }
      // utile en correction
      me.stockage[6] = ''
    } else {
    // j’ajoute un moins dans le cas où a racine(c) est non nul
      if (!((a === 0) && (c === 0))) {
        lexpression1 += '-£x'
      }
      // utile en correction
      me.stockage[6] = '-'
    }
    if ((c !== '') && (c > 0)) {
    // ajout de racine(c) si définie
      lexpression1 += '\\sqrt{£w}'
    }
    // la suite si b*racine(d) est non nulle
    if (!((b === 0) && (d === 0))) {
      if (lesecondsigne === 1) {
      // pas de plus s’il n’y a rien avant
        if (!((a === 0) && (c === 0))) {
          lexpression1 += ' + '
        }
        me.stockage[7] = ''
      } else {
        lexpression1 += ' - '
        me.stockage[7] = '-'
      }
      lexpression1 += '£y'
      if ((d !== '') && (d !== 0)) {
        lexpression1 += '\\sqrt{£t}'
      }
      lexpression1 += 'i'
    }
    lexpression1 += '$'

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
    ;[1, 2, 3, 4].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })
    j3pAffiche(stor.zoneCons1, '', ds.textes.question1, { a: 'a', b: 'b' })
    j3pAffiche(stor.zoneCons2, '', lexpression1, { x: a, y: b, w: c, t: d })
    const elt = j3pAffiche(stor.zoneCons3, '', '$|z_1|=$&1&', { inputmq1: { texte: '' } })
    stor.zoneInput = elt.inputmqList[0]
    j3pStyle(stor.zoneCons4, { paddingTop: '10px' })
    stor.laPalette = j3pAddElt(stor.zoneCons4, 'div')

    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['racine'] })

    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '20px' }) })
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('moyen.explications', { paddingTop: '10px' }) })

    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')

    mqRestriction(stor.zoneInput, '\\d,.+-i', { commandes: ['racine'] })
    j3pFocus(stor.zoneInput)
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':

      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }
      // par exemple pour le premier, l’affichage sera   +/- 6 +/- 2 racine(3) i et la bonne réponse 4 racine(3)
      enonceMain()
      break // case "enonce":

    case 'correction':
      me.cacheBoutonValider()
      stor.zoneExpli.innerHTML = ''
      // on teste si une réponse a été saisie
      {
        const reponseEleve = $(stor.zoneInput).mathquill('latex')

        if ((reponseEleve === '') && (!me.isElapsed)) {
          j3pFocus(stor.zoneInput)
          me.reponseManquante(stor.zoneCorr)
        } else { // une réponse a été saisie
          const repEleveNettoyee = traduitChaineMathquill(reponseEleve)
          me.logIfDebug('rep eleve=', repEleveNettoyee)
          let ch
          if (Number(repEleveNettoyee[0]) === stor.solution[0] && Number(repEleveNettoyee[1]) === stor.solution[1]) {
            me.score++
            me._stopTimer()
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)

            stor.zoneCorr.innerHTML = cBien
            stor.zoneInput.style.color = me.styles.cbien
            ch = macorrection(reponseEleve, 0)
            j3pAffiche(stor.zoneExpli, '', ch.unechaine, {
              x: me.stockage[0],
              y: me.stockage[1],
              w: me.stockage[2],
              t: me.stockage[3],
              s: me.stockage[4],
              r: me.stockage[5],
              u: me.stockage[6],
              v: me.stockage[7],
              p: me.stockage[4] + me.stockage[5],
              m: stor.solution[0],
              n: stor.solution[1]
            })
            j3pEmpty(stor.laPalette)
            me.typederreurs[0]++
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me._stopTimer()
              me.typederreurs[10]++
              stor.zoneInput.style.color = me.styles.cfaux
              j3pDesactive(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)
              // je classe cette erreur comme 'autre'
              me.typederreurs[3]++
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
            // S’il y a deux chances,on appelle à nouveau le bouton Valider
              stor.zoneInput.style.color = me.styles.cfaux
              if (me.essaiCourant < ds.nbchances) {
                if (String(repEleveNettoyee[0]) === '' && Number(repEleveNettoyee[1]) === me.stockage[4] + me.stockage[5]) {
                  j3pAffiche(stor.zoneCorr, '', ds.textes.correction3, {})
                  stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                } else {
                  stor.zoneCorr.innerHTML = cFaux
                  stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                }
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                // j3pFocus("affiche1input1");
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                me._stopTimer()
                stor.zoneExpli.style.color = me.styles.cfaux

                stor.zoneCorr.innerHTML = cFaux
                if (repEleveNettoyee[0] === '' && repEleveNettoyee[1] === me.stockage[4] + me.stockage[5]) {
                  ch = macorrection(reponseEleve, 1)
                  j3pAffiche(stor.zoneExpli, '', ch.unechaine, {
                    x: me.stockage[0],
                    y: me.stockage[1],
                    w: me.stockage[2],
                    t: me.stockage[3],
                    s: me.stockage[4],
                    r: me.stockage[5],
                    u: me.stockage[6],
                    v: me.stockage[7],
                    p: me.stockage[4] + me.stockage[5],
                    m: stor.solution[0],
                    n: stor.solution[1]
                  })
                } else {
                  ch = macorrection(reponseEleve, 2)
                  me.typederreurs[1]++
                  j3pAffiche(stor.zoneExpli, '', ch.unechaine, {
                    x: me.stockage[0],
                    y: me.stockage[1],
                    w: me.stockage[2],
                    t: me.stockage[3],
                    s: me.stockage[4],
                    r: me.stockage[5],
                    u: me.stockage[6],
                    v: me.stockage[7],
                    p: me.stockage[4] + me.stockage[5],
                    m: stor.solution[0],
                    n: stor.solution[1]
                  })
                }
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                j3pFreezeElt(stor.zoneInput)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break
    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
