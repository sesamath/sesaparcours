import { j3pAddElt, j3pBarre, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pNombreBienEcrit, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Alexis Lecomte
    2012-2013

 *  Donner la partie réelle d’un complexe donné sous la forme:
 *      a+bi
 *      bi+a
 *      a,b appartenant à des intervalles parametrables
 *      section quantitative
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'aucun'],
    ['indication', 'La partie réelle de a+ib est a et sa partie imaginaire est b (attention à l’ordre)', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['a', '[-2;2]', 'string', 'Intervalle d’appartenance de a'],
    ['b', '[-9;9]', 'string', 'Intervalle d’appartenance de b']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function getDonnees () {
    return {

      typesection: 'lycee',

      nbrepetitions: 3,
      nbetapes: 1,
      textes: {
        titre_exo: 'Identifier les deux parties d’un complexe',
        question1: "Donner la partie <span class='rouge'>réelle </span>du complexe suivant:",
        question2: "Donner la partie <span class='rouge'>imaginaire</span> du complexe suivant",
        correction1: 'Tu confonds partie réelle et partie imaginaire.<br> La bonne réponse est ' + '$£a$',
        correction2: 'Attention à ne pas inclure i dans la réponse....<br> La bonne réponse est ' + '$£a$',
        correction3: 'La bonne réponse est ' + '$£a$',
        corr1: 'En effet,<br>' + '&nbsp;&nbsp;&nbsp;&nbsp;£z $= (£x)+i\\times(£y)$'
      },
      indication: 'La partie réelle de a+ib est a et sa partie imaginaire est b (attention à l’ordre)',
      nbchances: 1,
      a: '[-2;2]',
      b: '[-9;9]',
      structure: 'presentation1'
    }
  }
  function macorrection (reponseeleve) {
    // me.stockage=[a,b,c,sortequestion,partie];
    // var unobjet={unechaine:"",bilan:""};
    let unechaine = ds.textes.correction3
    // a+ib OU ib+a
    if (((stor.solution[0] === me.stockage[0]) && (reponseeleve === me.stockage[1])) ||
             ((stor.solution[0] === me.stockage[1]) && (reponseeleve === me.stockage[0]))
    ) {
      unechaine = ds.textes.correction1
    }

    if ((reponseeleve === 'i' + stor.solution[0]) ||
              (reponseeleve === stor.solution[0] + 'i') ||
              ((reponseeleve === 'i') && (stor.solution[0] === 1)) ||
              ((reponseeleve === '-i') && (stor.solution[0] === -1))
    ) {
      unechaine = ds.textes.correction2
    }
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(stor.conteneurCorr, 'div') })
    j3pAffiche(stor.zoneExpli1, '', unechaine, { a: stor.solution[0] })
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr1, { x: me.stockage[0], y: me.stockage[1], z: me.stockage[5] })
  }

  // DEBUT DE LA SECTION
  function enonceMain () {
    const sortequestion = j3pGetRandomInt(1, 2)

    const partie = j3pGetRandomInt(1, 2)
    let a, b
    do {
      const [borneInfa, borneSupa] = j3pGetBornesIntervalle(ds.a)
      const [borneInfb, borneSupb] = j3pGetBornesIntervalle(ds.b)
      a = j3pGetRandomInt(borneInfa, borneSupa)
      b = j3pGetRandomInt(borneInfb, borneSupb)
    } while ((a === 0) && (b === 0))
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
    const enonce = j3pAddElt(stor.conteneur, 'div')

    switch (partie) {
      case 1:
        j3pAffiche(enonce, '', ds.textes.question1)
        break
      case 2:
        j3pAffiche(enonce, '', ds.textes.question2)
        break
    }

    me.stockage = [a, b, 0, sortequestion, partie]

    let lexpression1 = ''// Normalement $z_1=£x+£yi$
    if (a === 0) {
      if (b === 1) {
        lexpression1 = '$z_1=i$'
      }
      if (b === -1) {
        lexpression1 = '$z_1=-i$'
      }
      if ((b !== 1) && (b !== -1)) {
        lexpression1 = '$z_1=£yi$'
      }
    } else {
      if (b === 0) {
        lexpression1 = '$z_1=£x$'
      } else {
      // les deux non nuls
        if (b > 0) {
          if (b === 1) {
            lexpression1 = '$z_1=£x+i$'
          }
          if (b !== 1) {
            lexpression1 = '$z_1=£x+£yi$'
          }
        } else {
          if (b === -1) {
            lexpression1 = '$z_1=£x-i$'
          }
          if (b !== -1) {
            lexpression1 = '$z_1=£x£yi$'
          }
        }
      }
    }
    let lexpression2 = ''/// /Normalement $z_1=£yi+£x$
    if (a === 0) {
      if (b === 1) {
        lexpression2 = '$z_1=i$'
      }
      if (b === -1) {
        lexpression2 = '$z_1=-i$'
      }
      if ((b !== 1) && (b !== -1)) {
        lexpression2 = '$z_1=£yi$'
      }
    } else {
      if (b === 0) {
        lexpression2 = '$z_1=£x$'
      } else {
      // les deux non nuls
        if (b > 0) {
          if ((b === 1) && (a > 0)) {
            lexpression2 = '$z_1=i+£x$'
          }
          if ((b === 1) && (a < 0)) {
            lexpression2 = '$z_1=i£x$'
          }
          if ((b !== 1) && (a > 0)) {
            lexpression2 = '$z_1=£yi+£x$'
          }
          if ((b !== 1) && (a < 0)) {
            lexpression2 = '$z_1=£yi£x$'
          }
        } else {
          if ((b === -1) && (a > 0)) {
            lexpression2 = '$z_1=-i+£x$'
          }
          if ((b === -1) && (a < 0)) {
            lexpression2 = '$z_1=-i£x$'
          }
          if ((b !== -1) && (a > 0)) {
            lexpression2 = '$z_1=£yi+£x$'
          }
          if ((b !== -1) && (a < 0)) {
            lexpression2 = '$z_1=£yi£x$'
          }
        }
      }
    }
    const enonce2 = j3pAddElt(stor.conteneur, 'div')
    switch (sortequestion) {
      case 1:
        j3pAffiche(enonce2, '', '&nbsp;&nbsp;&nbsp;&nbsp;' + lexpression1, { x: a, y: b })
        if (partie === 1) {
          me.storage.solution[0] = a
        } else {
          me.storage.solution[0] = b
        }
        me.stockage[5] = lexpression1
        break
      case 2:
        j3pAffiche(enonce2, '', '&nbsp;&nbsp;&nbsp;&nbsp;' + lexpression2, { x: a, y: b })
        if (partie === 1) {
          me.storage.solution[0] = a
        } else {
          me.storage.solution[0] = b
        }
        me.stockage[5] = lexpression2
        break
    }
    const enonce3 = j3pAddElt(stor.conteneur, 'div')
    const elt = j3pAffiche(enonce3, '', '&1&', { inputmq1: { texte: '' } })
    stor.zoneInput = elt.inputmqList[0]
    stor.conteneurCorr = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '20px' }) })
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '10px' }) })
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    mqRestriction(stor.zoneInput, '\\d,.+-i')
    j3pFocus(stor.zoneInput)
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':

      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        me.stockage = [0, 0, 0, 0]
        me.storage.solution = []
        me.typederreurs = [0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo, true)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      enonceMain()

      break // case "enonce":

    case 'correction':

      stor.zoneCorr.innerHTML = ''
      // on teste si une réponse a été saisie
      {
        const repEleve = j3pValeurde(stor.zoneInput)

        if (!repEleve && !me.isElapsed) {
          j3pFocus(stor.zoneInput)
          me.reponseManquante(stor.zoneCorr)
        } else { // une réponse a été saisie
          if (repEleve === j3pNombreBienEcrit(me.storage.solution[0]) || repEleve === String(me.storage.solution[0])) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            me.etat = 'navigation'
            j3pDesactive(stor.zoneInput)
            stor.zoneInput.style.color = me.styles.cbien
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              macorrection(repEleve)
              // je classe cette erreur comme 'autre'
              me.typederreurs[3]++
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                me._stopTimer()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                macorrection(repEleve)
                stor.zoneInput.style.color = me.styles.cfaux
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break

    case 'navigation':

      if (me.sectionTerminee()) {
        me.parcours.pe = me.typederreurs[0] / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()

      break // case "navigation":
  }
}
