import { j3pAddElt, j3pAjouteFoisDevantX, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMathquillXcas, j3pShowError } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// Cette section affiche une série de 10 calculs du produit de 2 nombres complexes'
// Les coefficients sont entiers relatifs.
// section quantitative
// Laurent F.
// Section reprise en juin 2021 par Rémi DENIAUD
// Section revue par Yves pour adaptation clavier virtuel en 10-2021

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 10, 'entier', 'aucun'],
    ['indication', '(a+ib)(c+id)=ac-bd+(ad+bc)i', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['a', '[-9;9]', 'intervalle', 'coefficient a de (a+ib)(c+id)'],
    ['b', '[-9;9]', 'intervalle', 'coefficient b de (a+ib)(c+id)'],
    ['c', '[-9;9]', 'intervalle', 'coefficient c de (a+ib)(c+id)'],
    ['d', '[-9;9]', 'intervalle', 'coefficient d de (a+ib)(c+id)']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = this.storage

  function depart () {
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAALgAAACVAAAAQEAAAAAAAAAAQAAAAb#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAFiAAItMf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABP#AAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wABZgAFYSp4K2L#####AAAAAQAKQ09wZXJhdGlvbgAAAAAFAv####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAB#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAYAAAACAAF4AAAABAD#####AANyZXAABTIqeC0xAAAABQEAAAAFAgAAAAFAAAAAAAAAAAAAAAcAAAAAAAAAAT#wAAAAAAAAAAF4#####wAAAAMAEENUZXN0RXF1aXZhbGVuY2UA#####wAHZWdhbGl0ZQAAAAMAAAAEAQAAAAABP#AAAAAAAAAB################'
    stor.mtgList = stor.mtgAppLecteur.createList(txtFigure)
  }
  // fonction qui teste si la réponse élève est égale à j3p.storage.solution, de manière formelle, et qui vérifie également que l’écriture est algébrique (un seul i)
  function analyseFormelle (reponseEleve) {
    const retour = {}
    let i
    let repEleveXcas = j3pMathquillXcas(reponseEleve.replace(/i/g, 'x'))
    repEleveXcas = j3pAjouteFoisDevantX(repEleveXcas, 'x')
    stor.mtgList.giveFormula2('rep', repEleveXcas)
    stor.mtgList.calculateNG()
    retour.bon = (stor.mtgList.valueOf('egalite') === 1)
    if (!retour.bon) {
      // La réponse équivalente à la réponse attendue, mais elle est peut-être exacte mais non simplifiée
      const arbreChaine = new Tarbre(repEleveXcas + '-(' + me.storage.solution[0].replace(/i/g, 'x') + ')', ['x'])
      let egalite = true
      for (i = 1; i <= 15; i++) {
        egalite = egalite && (Math.abs(arbreChaine.evalue([i])) < Math.pow(10, -12))
      }
      retour.bon = egalite
      const formeAlg = [
        /^-?[0-9]+[+-][0-9]*\*?x$/g,
        /^-?[0-9]*\*?x$/g,
        /^-?[0-9]+$/g,
        /^-?[0-9]*\*?x[+-][0-9]+$/g
      ]
      // s’arrête au premier truc qui vérifie, cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/some
      retour.simplifie = formeAlg.some(function (regexp) {
        return regexp.test(repEleveXcas)
      })
    } else {
      retour.simplifie = true
    }
    return retour
  }
  function getDonnees () {
    return {

      typesection: 'lycee',

      nbrepetitions: 10,
      nbetapes: 1,
      textes: {
        titre_exo: 'Produit de nombres complexes',
        question1: 'Calculez sous forme algébrique le produit $P$ des nombres complexes&nbsp;:',
        correction0: 'On développe l’expression&nbsp;:',
        dvpt: '$£c$ sachant que $i^2=-1$.',
        correction1: 'Ta réponse n’est pas écrite sous forme algébrique.',
        correction2: 'Ta réponse est exacte mais n’est pas écrite sous forme algébrique.',
        correction3: 'La bonne réponse est $£a$.',
        correction4: 'Voici la bonne réponse : $£a$.',
        comment1: 'Tu n’as le droit qu’à un seul essai.'
      },

      a: '[-9;9]',
      b: '[-9;9]',
      c: '[-9;9]',
      d: '[-9;9]',

      nbchances: 1,
      structure: 'presentation1',
      indication: '(a+ib)(c+id)=ac-bd+(ad+bc)i'
    }
  }
  // le booléen est présent pour que la correction soit 'classique' dans le cas d’un pb lié au temps de réponse
  function macorrection (reponseeleve, bool) {
    let lastRep = ds.textes.correction3
    const bilan = 'autre'
    // JE donne les détails de la correction
    if (bool && !reponseeleve.simplifie) {
      // ce n’est pas une forme algébrique (par exemple deux i dans la réponse) mais la réponse est exacte
      const unechaine = (reponseeleve.bon) ? ds.textes.correction2 : ds.textes.correction1
      const leBilan = j3pAddElt(stor.zoneExpli, 'div')
      j3pAffiche(leBilan, '', unechaine)
      lastRep = ds.textes.correction4
    }
    for (let i = 1; i <= 2; i++) stor['dvpt' + i] = j3pAddElt(stor.zoneExpli, 'div')
    j3pAffiche(stor.dvpt1, '', ds.textes.correction0)
    j3pAffiche(stor.dvpt2, '', ds.textes.dvpt, { c: stor.etapeDvpt })
    const leBilan2 = j3pAddElt(stor.zoneExpli, 'div')
    j3pAffiche(leBilan2, '', lastRep, { a: me.storage.solution[0] })
    return bilan
  }
  // Utilisée pour remettre en forme a+ib ou b+ia
  function GestionAPlusiB (a, b, Choix) {
    // Choix=1 donne a+ib et Choix=2 donne ib+a, avec les simplifications de signes.
    let Stock = ''
    switch (Choix) {
      case 1:
        Stock = SimplifierEcritureDegre1(b, a, 2)
        break
      case 2:
        Stock = SimplifierEcritureDegre1(b, a, 1)
    }
    // Stock=RemplaceXparI(Stock);
    return Stock
  }
  function SimplifierEcritureDegre1 (a, b, Choix) {
    // Cette fonction fait simplifier par xcas les écritures littéralles ax+b si Choix=1
    // et b+ax si Choix=2
    // du style 1x+2=x+2 ou 2x+0=2x etc
    let Stock = ''
    switch (Choix) {
      case 2:
        // a+ib
        // Stock=xcasRequete(a+"x+"+b);
        if (a === 0) {
          if (b === 1) { Stock = 'i' }
          if (b === -1) { Stock = '-i' }
          if ((b !== 1) && (b !== -1)) { Stock = b + 'i' }
        } else {
          if (b === 0) {
            Stock = a + ''
          } else {
            // les deux non nuls
            if (b > 0) {
              if (b === 1) { Stock = a + '+i' }
              if (b !== 1) { Stock = a + '+' + b + 'i' }
            } else {
              if (b === -1) { Stock = a + '-i' }
              if (b !== -1) { Stock = a + '' + b + 'i' }
            }
          }
        }

        break
      case 1:
        // Stock=xcasRequete(b+"+"+a+"x");
        if (a === 0) {
          if (b === 1) { Stock = 'i' }
          if (b === -1) { Stock = '-i' }
          if ((b !== 1) && (b !== -1)) { Stock = b + 'i' }
        } else {
          if (b === 0) {
            Stock = a + ''
          } else {
            // les deux non nuls
            if (b > 0) {
              if ((b === 1) && (a > 0)) { Stock = 'i+' + a }
              if ((b === 1) && (a < 0)) { Stock = 'i' + a }
              if ((b !== 1) && (a > 0)) { Stock = b + 'i+' + a }
              if ((b !== 1) && (a < 0)) { Stock = b + 'i' + a }
            } else {
              if ((b === -1) && (a > 0)) { Stock = '-i+' + a }
              if ((b === -1) && (a < 0)) { Stock = '-i' + a }
              if ((b !== -1) && (a > 0)) { Stock = b + 'i+' + a }
              if ((b !== -1) && (a < 0)) { Stock = b + 'i' + a }
            }
          }
        }
        break
    }
    return Stock
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
    me.stockage = [0, 0, 0, 0]
    me.typederreurs = [0, 0, 0, 0]
    me.score = 0
    me.afficheTitre(ds.textes.titre_exo, false)
    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
          j3pFocus(stor.zoneInput)
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    let a, b, c, d
    do {
      const [borneInfa, borneSupa] = j3pGetBornesIntervalle(ds.a)
      const [borneInfb, borneSupb] = j3pGetBornesIntervalle(ds.b)
      a = j3pGetRandomInt(borneInfa, borneSupa)
      b = j3pGetRandomInt(borneInfb, borneSupb)
    }
    while (a * b === 0)

    do {
      const [borneInfc, borneSupc] = j3pGetBornesIntervalle(ds.c)
      const [borneInfd, borneSupd] = j3pGetBornesIntervalle(ds.d)
      c = j3pGetRandomInt(borneInfc, borneSupc)
      d = j3pGetRandomInt(borneInfd, borneSupd)
    }
    while (((c === 0) && (d === 0)) || (d === 0 && c === 1))
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '20px' }) }
    )
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    j3pAffiche(stor.zoneCons1, 'Enonce', ds.textes.question1)
    me.stockage = [a, b, c, d]
    // AplusIB=1 décide de a+ib et AplusIB=2 pour ib+a ainsi que CplusID
    // On affiche (a+ib)(c+id)  ou c(a+ib) ou id(a+ib) aux permutations de i près.
    me.storage.solution[0] = GestionAPlusiB(a * d + b * c, a * c - b * d, 1)
    const AplusIB = j3pGetRandomInt(1, 2)
    const CplusID = j3pGetRandomInt(1, 2)
    // On met en forme le produit :
    let lasaisie, ch1, ch2
    if (AplusIB === 1) {
      ch1 = SimplifierEcritureDegre1(a, b, 2)
    } else {
      ch1 = SimplifierEcritureDegre1(a, b, 1)
    }
    if (CplusID === 1) {
      ch2 = SimplifierEcritureDegre1(c, d, 2)
    } else {
      ch2 = SimplifierEcritureDegre1(c, d, 1)
    }
    // console.log('AplusIB:', AplusIB, ch1, 'CplusID:', CplusID, ch2)
    // Ecriture du développement
    let tabMonome
    if (AplusIB === 1) {
      if (CplusID === 1) {
        tabMonome = [[0, a * c], [1, a * d], [1, b * c], [2, b * d]]
      } else {
        tabMonome = [[1, a * d], [0, a * c], [2, b * d], [1, b * c]]
      }
    } else {
      if (CplusID === 1) {
        tabMonome = [[1, b * c], [2, b * d], [0, a * c], [1, a * d]]
      } else {
        tabMonome = [[2, b * d], [1, b * c], [1, a * d], [0, a * c]]
      }
    }
    let rgSuivant = 1
    stor.etapeDvpt = 'P='
    for (let i = 0; i < tabMonome.length; i++) {
      if (tabMonome[i][1] !== 0) {
        stor.etapeDvpt += j3pGetLatexMonome(rgSuivant, tabMonome[i][0], tabMonome[i][1], 'i')
        rgSuivant += 1
      }
    }
    if (c === 0) {
      lasaisie = ch2 + '(' + ch1 + ')'
    } else {
      if (d === 0) {
        lasaisie = ch2 + '(' + ch1 + ')'
      } else {
        lasaisie = '(' + ch1 + ')(' + ch2 + ')'
      }
    }
    // lasaisie=RemplaceXparI(lasaisie)
    j3pAffiche(stor.zoneCons2, '', '$P=' + lasaisie + '$')
    const elt = j3pAffiche(stor.zoneCons3, '', '$P=$&1&', { inputmq1: { texte: '' } })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.+-i')
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '15px' }) })
    stor.zoneExpli = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '20px' }) })
    // Chargement de la figure mtg32
    depart()
    stor.mtgList.giveFormula2('a', String(a * d + b * c))
    stor.mtgList.giveFormula2('b', String(a * c - b * d))
    stor.mtgList.calculateNG()
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })
    if (ds.nbchances === 1) stor.zoneComment = j3pAddElt(stor.conteneur, 'div', ds.textes.comment1, { style: { paddingTop: '10px', fontSize: '0.9em', fontStyle: 'italic' } })
    // console.log('solution:', me.storage.solution)
    j3pFocus(stor.zoneInput)
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break

    case 'correction':
      stor.zoneCorr.innerHTML = ''
      {
        const reponse = stor.fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
        } else { // une réponse a été saisie
          const repEleveXcas = j3pMathquillXcas(stor.fctsValid.zones.reponseSaisie[0])
          const reponseAnalysee = analyseFormelle(repEleveXcas)
          reponse.bonneReponse = reponseAnalysee.bon && reponseAnalysee.simplifie
          stor.fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
          stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              macorrection(reponseAnalysee, false)
              // je classe cette erreur comme 'autre'
              me.typederreurs[3]++
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              } else {
              // Erreur au second essai
                me._stopTimer()
                me.sectionCourante('navigation')
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                macorrection(reponseAnalysee, true)
              }
            }
          }
        }
      }
      break

    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break// case "navigation":
  }
}
