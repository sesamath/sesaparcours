import { j3pAddElt, j3pBarre, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pNombreBienEcrit, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques
/*
    Alexis Lecomte
    2012-2013

*/

/*
 *
 *  Donner la partie réelle d’un complexe donné sous la forme:
 *      a+bi
 *      bi+a
 *      a,b appartenant à des intervalles parametrables
 *      section qualitative testant la confusion entre les deux parties et la présence de i dans la partie imaginaire
 */

export const params = {

  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['a', '[-2;2]', 'string', 'Intervalle d’appartenance de a'],
    ['b', '[-9;9]', 'string', 'Intervalle d’appartenance de b'],
    ['seuilreussite', 0.7, 'reel', 'seuil à partir duquel pe_1 (Bien) est renvoyée'],
    ['seuil_inclusion_i', 0.2, 'reel', 'seuil à partir duquel pe_3 (inclusion de i dans la partie imaginaire) est renvoyée'],
    ['seuil_confusion', 0.2, 'reel', 'seuil à partir duquel pe_2 (confusion des deux parties) est renvoyée']
  ],

  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'confusion des deux parties' },
    { pe_3: 'inclusion de i dans la partie imaginaire' },
    { pe_4: 'insuffisant' }
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {

      typesection: 'lycee',

      nbrepetitions: 10,
      nbetapes: 1,
      seuilreussite: 0.7,
      // seuil_pbi=0.2;
      seuil_inclusion_i: 0.2,
      seuil_confusion: 0.2,
      textes: {
        titreExo: 'Identifier les deux parties d’un complexe',
        question1: "Donner la partie <span class='rouge'>réelle </span>du complexe suivant:",
        question2: "Donner la partie <span class='rouge'>imaginaire</span> du complexe suivant",
        correction1: 'Tu confonds partie réelle et partie imaginaire.<br> La bonne réponse est ' + '$£a$',
        correction2: 'Attention à ne pas inclure i dans la réponse....<br> La bonne réponse est ' + '$£a$',
        correction3: 'La bonne réponse est ' + '$£a$',
        corr1: 'En effet,<br>' + '&nbsp;&nbsp;&nbsp;&nbsp;$ £z = ' + '(£x)+\\mathrm{i}\\times(£y)$'
      },
      //  question1: "Donner la partie <span class='rouge'>réelle </span>du complexe suivant:";
      indication: 'La partie réelle de a+ib est a et sa partie imaginaire est b (attention à l’ordre)',
      nbchances: 1,
      a: '[-2;2]',
      b: '[-9;9]',
      pe_1: 'bien',
      pe_2: 'confusion des deux parties',
      pe_3: 'inclusion de i dans la partie imaginaire',
      pe_4: 'insuffisant',
      // limite="";
      // structure="tableau2";
      structure: 'presentation1'
    }
  }
  function macorrection (reponseeleve) {
    let unechaine = ds.textes.correction3
    let bilan = 'autre'
    // a+ib OU ib+a
    if (((me.storage.solution[0] === me.stockage[0]) && (reponseeleve === me.stockage[1])) ||
             ((me.storage.solution[0] === me.stockage[1]) && (reponseeleve === me.stockage[0]))
    ) {
      bilan = 'confusion'
      unechaine = ds.textes.correction1
    }

    if ((reponseeleve === 'i' + me.storage.solution[0]) ||
              (reponseeleve === me.storage.solution[0] + 'i') ||
              ((reponseeleve === 'i') && (me.storage.solution[0] === 1)) ||
              ((reponseeleve === '-i') && (me.storage.solution[0] === -1))
    ) {
      bilan = 'pb i'
      unechaine = ds.textes.correction2
    }
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('moyen.explications', { paddingTop: '40px' }) })
    stor.zoneExpli = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli, '', unechaine, { a: me.storage.solution[0] })
    stor.zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    j3pStyle(stor.zoneExpli2, { paddingTop: '10px' })
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr1, { x: me.stockage[0], y: me.stockage[1], z: me.stockage[5].substring(1, me.stockage[5].length - 1) })
    return bilan
  }

  // DEBUT DE LA SECTION

  switch (me.etat) {
    case 'enonce':

      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        me.stockage = [0, 0, 0, 0]
        stor.solution = []
        me.typederreurs = [0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titreExo, true)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      {
        const sortequestion = j3pGetRandomInt(1, 2)

        const partie = j3pGetRandomInt(1, 2)
        let a, b
        do {
          const [borneInfa, borneSupa] = j3pGetBornesIntervalle(ds.a)
          const [borneInfb, borneSupb] = j3pGetBornesIntervalle(ds.b)
          a = j3pGetRandomInt(borneInfa, borneSupa)
          b = j3pGetRandomInt(borneInfb, borneSupb)
        } while ((a === 0) && (b === 0))
        stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
        for (let i = 1; i <= 3; i++) {
          stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '5px' } })
        }

        switch (partie) {
          case 1:
            j3pAffiche(stor.zoneCons1, '', ds.textes.question1)
            break
          case 2:
            j3pAffiche(stor.zoneCons1, '', ds.textes.question2)
            break
        }

        me.stockage = [a, b, 0, sortequestion, partie]

        let lexpression1 = ''// Normalement $z_1=£x+£yi$
        if (a === 0) {
          if (b === 1) {
            lexpression1 = '$z_1=\\mathrm{i}$'
          }
          if (b === -1) {
            lexpression1 = '$z_1=-\\mathrm{i}$'
          }
          if ((b !== 1) && (b !== -1)) {
            lexpression1 = '$z_1=£y\\mathrm{i}$'
          }
        } else {
          if (b === 0) {
            lexpression1 = '$z_1=£x$'
          } else {
          // les deux non nuls
            if (b > 0) {
              if (b === 1) {
                lexpression1 = '$z_1=£x+\\mathrm{i}$'
              }
              if (b !== 1) {
                lexpression1 = '$z_1=£x+£y\\mathrm{i}$'
              }
            } else {
              if (b === -1) {
                lexpression1 = '$z_1=£x-\\mathrm{i}$'
              }
              if (b !== -1) {
                lexpression1 = '$z_1=£x£y\\mathrm{i}$'
              }
            }
          }
        }
        let lexpression2 = ''/// /Normalement $z_1=£yi+£x$
        if (a === 0) {
          if (b === 1) {
            lexpression2 = '$z_1=\\mathrm{i}$'
          }
          if (b === -1) {
            lexpression2 = '$z_1=-\\mathrm{i}$'
          }
          if ((b !== 1) && (b !== -1)) {
            lexpression2 = '$z_1=£y\\mathrm{i}$'
          }
        } else {
          if (b === 0) {
            lexpression2 = '$z_1=£x$'
          } else {
          // les deux non nuls
            if (b > 0) {
              if ((b === 1) && (a > 0)) {
                lexpression2 = '$z_1=\\mathrm{i}+£x$'
              }
              if ((b === 1) && (a < 0)) {
                lexpression2 = '$z_1=\\mathrm{i}£x$'
              }
              if ((b !== 1) && (a > 0)) {
                lexpression2 = '$z_1=£y\\mathrm{i}+£x$'
              }
              if ((b !== 1) && (a < 0)) {
                lexpression2 = '$z_1=£y\\mathrm{i}£x$'
              }
            } else {
              if ((b === -1) && (a > 0)) {
                lexpression2 = '$z_1=-\\mathrm{i}+£x$'
              }
              if ((b === -1) && (a < 0)) {
                lexpression2 = '$z_1=-\\mathrm{i}£x$'
              }
              if ((b !== -1) && (a > 0)) {
                lexpression2 = '$z_1=£y\\mathrm{i}+£x$'
              }
              if ((b !== -1) && (a < 0)) {
                lexpression2 = '$z_1=£y\\mathrm{i}£x$'
              }
            }
          }
        }
        switch (sortequestion) {
          case 1:
            j3pAffiche(stor.zoneCons2, '', '&nbsp;&nbsp;&nbsp;&nbsp;' + lexpression1, { x: a, y: b })
            if (partie === 1) {
              stor.solution[0] = a
            } else {
              stor.solution[0] = b
            }
            me.stockage[5] = lexpression1
            break
          case 2:
            j3pAffiche(stor.zoneCons2, '', '&nbsp;&nbsp;&nbsp;&nbsp;' + lexpression2, { x: a, y: b })
            if (partie === 1) {
              stor.solution[0] = a
            } else {
              stor.solution[0] = b
            }
            me.stockage[5] = lexpression2
            break
        }

        const elt = j3pAffiche(stor.zoneCons3, 'affiche1', '&1&',
          {
            inputmq1: { texte: '' }
          }
        )
        stor.zoneInput = elt.inputmqList[0]
        mqRestriction(stor.zoneInput, '\\d,.+-i')
        stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
        j3pFocus(stor.zoneInput)
        me.finEnonce()
      }
      break // case "enonce":

    case 'correction':

      stor.zoneCorr.innerHTML = ''
      // on teste si une réponse a été saisie
      {
        const repEleve = j3pValeurde(stor.zoneInput)

        if ((repEleve === '') && (!me.isElapsed)) {
          j3pFocus(stor.zoneInput)
          me.reponseManquante(stor.zoneCorr)
        } else { // une réponse a été saisie
          if (j3pCalculValeur(repEleve) === j3pNombreBienEcrit(stor.solution[0]) || repEleve === String(stor.solution[0])) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            j3pDesactive(stor.zoneInput)
            stor.zoneInput.style.color = me.styles.cbien
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            let ch
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              ch = macorrection(repEleve)
              // je classe cette erreur comme 'autre'
              me.typederreurs[3]++
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              stor.zoneInput.style.color = me.styles.cfaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                //  me.typederreurs[1]++;
                // indication éventuelle ici
                me.etat = 'correction'
                // j3pFocus("affiche1inputmq1");
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                me._stopTimer()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                ch = macorrection(repEleve)
                switch (ch) {
                  case 'confusion':
                    me.typederreurs[1]++
                    break
                  case 'pb i':
                    me.typederreurs[2]++
                    break
                  case 'autre':
                    me.typederreurs[3]++
                    break
                }
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break

    case 'navigation':
      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems

        if (((me.typederreurs[1] / ds.nbitems) >= ds.seuil_confusion) || ((me.typederreurs[2] / ds.nbitems) >= ds.seuil_inclusion_i)) {
          if ((me.typederreurs[1] / ds.nbitems) >= ds.seuil_confusion) {
            // je détecte la confusion entre les deux parties
            me.parcours.pe = ds.pe_2
          }
          if ((me.typederreurs[2] / ds.nbitems) >= ds.seuil_inclusion_i) {
            // je détecte la présence de i dans la réponse
            me.parcours.pe = ds.pe_3
          }
        } else {
          // sinon
          if (pourcentagereussite >= ds.seuilreussite) {
            // c’est bon
            me.parcours.pe = ds.pe_1
          } else {
            // pas bon
            me.parcours.pe = ds.pe_4
          }
        }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()

      break // case "navigation":
  }
}
