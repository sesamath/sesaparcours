import { j3pAddElt, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche, j3pMathsAjouteDans } from 'src/lib/mathquill/functions'

/**
 * Cours : Définition du module d’un complexe
 */
const titreExo = 'Module d’un complexe'

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['titre', titreExo, 'string', 'Titre affiché']
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this

  function enonceThen (content) {
    const div = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    j3pMathsAjouteDans(div, { content })
    // FIXME utiliser les méthodes de Repere pour construire ça
    const divSvg = j3pAddElt(div, 'div')
    divSvg.innerHTML = `<svg width="380" height="190">
<line x1="106.3" y1="43.7" x2="111.7" y2="38.3" style="stroke:black;stroke-width:2;" />
<line x1="106.3" y1="38.3" x2="111.7" y2="43.7" style="stroke:black;stroke-width:2;" />
<text x="111.7" y="38.3" style="fill:black; font-size:16pt;">M</text>
<text x="136" y="81.5" style="fill:red; font-size:16pt;">|z|</text>
<line x1="190" y1="95" x2="109" y2="41" style="stroke:red;stroke-width:3;" />
<line x1="190" y1="41" x2="109" y2="41" style="stroke:black;stroke-width:1;stroke-dasharray:3,2;" />
<line x1="109" y1="95" x2="109" y2="41" style="stroke:black;stroke-width:1;stroke-dasharray:3,2;" />
<text x="109" y="108.5" style="fill:blue; font-size:14pt;">a</text>
<text x="198.1" y="41" style="fill:blue; font-size:14pt;">b</text>
<line x1="1" y1="95" x2="379" y2="95" style="stroke:black;stroke-width:2;" />
<line x1="190" y1="1" x2="190" y2="189" style="stroke:black;stroke-width:2;" />
<text x="181.9" y="105.8" style="fill:black; font-size:12pt;">0</text>
<line x1="163" y1="100.4" x2="163" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="136" y1="100.4" x2="136" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="109" y1="100.4" x2="109" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="82" y1="100.4" x2="82" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="55" y1="100.4" x2="55" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="28" y1="100.4" x2="28" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="1" y1="100.4" x2="1" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="217" y1="100.4" x2="217" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="244" y1="100.4" x2="244" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="271" y1="100.4" x2="271" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="298" y1="100.4" x2="298" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="325" y1="100.4" x2="325" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="352" y1="100.4" x2="352" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="379" y1="100.4" x2="379" y2="89.6" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="122" x2="195.4" y2="122" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="149" x2="195.4" y2="149" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="176" x2="195.4" y2="176" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="68" x2="195.4" y2="68" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="41" x2="195.4" y2="41" style="stroke:black;stroke-width:1;" />
<line x1="184.6" y1="14" x2="195.4" y2="14" style="stroke:black;stroke-width:1;" />
</svg>`
    j3pAffiche(div, '', 'Ci-dessus, on a $z=-3+2i$. Donc $|z|=\\sqrt{(-3)^2+2^2}=\\sqrt{13}$')
    me.finEnonce()
  } // enonceThen

  if (this.etat === 'enonce') {
    // on a toujours une seule répétition, donc on est toujours au début de la section, inutile de tester
    this.setPassive()
    me.construitStructurePage('presentation2')
    me.afficheTitre(me.donneesSection.titre)
    j3pImporteAnnexe('cours/c_modulecomplexe.txt')
      .then(enonceThen)
      .catch(j3pShowError)
  }
  // section passive, pas de case correction ni navigation
}
