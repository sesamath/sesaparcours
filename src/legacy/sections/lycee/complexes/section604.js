import { j3pAddElt, j3pBarre, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
         JP Vanroyen
         2012

 *    CALCUL MENTAL AVEC LES COMPLEXES
 *    CALCULS DU TYPE ai * bi ou ai * bi * ci les i étant présents ou non (au moins 1) a,b,c étant tous différents de -1, 0 et 1.
 *    le paramètre nbfacteurs peut être égal à 2, 3 ou alea (les deux premiers items ayant forcément deux facteurs).
 *    On analyse si la réponse est correcte mais non simplifiée
 *    Section adaptée au clavier virtuel par Yves en 10-2021
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['nbfacteurs', '[3;3]', 'string', 'Nombre de facteurs'],
    ['a', '[-4;4]', 'string', 'Intervalle d’appartenance du premier facteur'],
    ['b', '[-4;4]', 'string', 'Intervalle d’appartenance du second facteur'],
    ['c', '[-4;4]', 'string', 'Intervalle d’appartenance du troisième facteur']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function traitementReponse (reponseEleve) {
    const objet = {}
    if (String(reponseEleve) === String(stor.solution[0])) {
      objet.juste = true
      objet.simplifiee = true
    } else {
      objet.simplifiee = false
      let maChaine
      if (String(stor.solution[0]).includes('i')) {
        const lindice = stor.solution[0].indexOf('i')
        maChaine = stor.solution[0].substring(0, lindice)
      }
      if (String(stor.solution[0]).includes('i') && (reponseEleve === maChaine + '*i' || reponseEleve === 'i' + maChaine || reponseEleve === 'i*' + maChaine || reponseEleve === 'i*(' + maChaine + ')' || reponseEleve === '(' + maChaine + ')i' || reponseEleve === 'i(' + maChaine + ')' || reponseEleve === '(' + maChaine + ')*i')) {
        objet.juste = true
        objet.simplifiee = false
      } else {
        objet.juste = false
        objet.simplifiee = false
      }
    }
    return objet
  }

  function afficheCorrection () {
    // elle n’est appelée qu’en cas de mauvaise réponse
    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(stor.zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.correction2,
      {
        a: String(stor.solution[0]).replace(/i/g, '\\mathrm{i}'),
        b: me.stockage[1],
        c: me.stockage[2]
      })
    j3pDesactive(stor.zoneInput)
    j3pBarre(stor.zoneInput)
    let nbi = 0
    stor.tabi.forEach(function (elt) {
      if (elt === 'i') nbi++
    })
    const calci = (nbi === 2) ? '\\mathrm{i}^2=-1' : '\\mathrm{i}^3=-\\mathrm{i}'
    if (nbi > 1) {
      j3pAffiche(stor.zoneExpli2, '', ds.textes.correction3, { a: calci })
    }
  }

  function getDonnees () {
    return {

      typesection: 'lycee',

      textes: {
        titreExo: 'Calcul mental et complexes',
        texte1: 'Complète l’égalité suivante :<br>',
        correction1: 'C’est bien, même si la réponse n’est pas simplifiée au maximum<br>On attend plutôt cette écriture : $£a$',
        correction2: 'Voici la bonne réponse : $£a$<br>En effet : $£b = £c$',
        correction3: 'De plus $£a$.'
        // correction3:"<br>En effet : $£a£b$"
      },
      // nbfacteurs est soit 2 soit 3 soit "alea""
      nbfacteurs: '[3;3]',

      // pourcentage de réussite envoyé
      nbchances: 1,
      a: '[-4;4]',
      b: '[-4;4]',
      c: '[-4;4]',

      nbrepetitions: 5,
      nbetapes: 1,
      structure: 'presentation1',
      indication: ''
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
    me.typederreurs = [0, 0]
    me.stockage = [0, 0, 0, 0]
    stor.solution = []
    me.afficheTitre(ds.textes.titreExo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }

      // je génére mes deux ou trois nombres :
      {
        let nombre1, nombre2, nombre3
        do {
          const [borneInfa, borneSupa] = j3pGetBornesIntervalle(ds.a)
          const [borneInfb, borneSupb] = j3pGetBornesIntervalle(ds.b)
          const [borneInfc, borneSupc] = j3pGetBornesIntervalle(ds.c)
          nombre1 = j3pGetRandomInt(borneInfa, borneSupa)
          nombre2 = j3pGetRandomInt(borneInfb, borneSupb)
          nombre3 = j3pGetRandomInt(borneInfc, borneSupc)
        } while (nombre1 === 0 || nombre2 === 0 || nombre3 === 0 || nombre1 === 1 || nombre2 === 1 || nombre3 === 1 || nombre1 === -1 || nombre2 === -1 || nombre3 === -1)
        // pour savoir s’il y a 2 ou 3 facteurs :
        let nombreDeFacteurs

        switch (String(ds.nbfacteurs)) {
          case '[2;2]' :
            nombreDeFacteurs = 2
            me.stockage[1] = '(' + nombre1 + ')' + '\\times' + '(' + nombre2 + ')'
            me.stockage[2] = nombre1 * nombre2
            break
          case '[3;3]' :
            nombreDeFacteurs = 3
            me.stockage[1] = '(' + nombre1 + ')' + '\\times' + '(' + nombre2 + ')' + '\\times' + '(' + nombre3 + ')'
            me.stockage[2] = nombre1 * nombre2 * nombre3
            break

          default :
          // on choisit de toute façon 2 facteurs pour les deux premiers items, pour graduer la difficulté
            nombreDeFacteurs = (me.questionCourante <= Math.min(2, ds.nbrepetitions - 2)) ? 2 : j3pGetRandomInt(2, 3)
            if (nombreDeFacteurs === 2) {
              me.stockage[1] = '(' + nombre1 + ')' + '\\times' + '(' + nombre2 + ')'
              me.stockage[2] = nombre1 * nombre2
            } else {
              me.stockage[1] = '(' + nombre1 + ')' + '\\times' + '(' + nombre2 + ')' + '\\times' + '(' + nombre3 + ')'
              me.stockage[2] = nombre1 * nombre2 * nombre3
            }
            // console.log('nombreDeFacteurs:', nombreDeFacteurs, me.stockage[1], me.stockage[2])
            break
        }

        // i ou pas i, j’en veux au moins un dans les deux premiers
        let tabi = []
        let lasaisie = ';'
        let nombreDeI, i, j
        do {
          tabi = ['', '', '']
          nombreDeI = 0

          for (i = 0; i < nombreDeFacteurs; i++) {
            j = j3pGetRandomInt(0, 1)

            // si j’ai un i
            if (j === 1) {
              tabi[i] = 'i'
              nombreDeI++
            }
          }
        } while ((tabi[0] === '') && (tabi[1] === '') && (tabi[2] === ''))

        if (nombreDeFacteurs === 2) {
          if (nombre2 < 0) {
            lasaisie = nombre1 + tabi[0] + ' \\times (' + nombre2 + tabi[1] + ')' + ' = '
          } else {
            lasaisie = nombre1 + tabi[0] + ' \\times ' + nombre2 + tabi[1] + ' = '
          }
          // La solution :
          if (nombreDeI === 1) {
          // un i :
            stor.solution[0] = Math.round(parseInt(nombre1) * parseInt(nombre2)) + 'i'
          }
          if (nombreDeI === 2) {
          // deux i :
            stor.solution[0] = Math.round(parseInt(nombre1) * parseInt(nombre2)) * (-1)
          }
        } else {
        // j’ai trois facteurs
          if (nombre2 < 0) {
            if (nombre3 < 0) {
              lasaisie = nombre1 + tabi[0] + ' × (' + nombre2 + tabi[1] + ')' + ' × (' + nombre3 + tabi[2] + ')' + ' = '
            } else {
              lasaisie = nombre1 + tabi[0] + ' × (' + nombre2 + tabi[1] + ')' + ' × ' + nombre3 + tabi[2] + ' = '
            }
          } else {
            if (nombre3 < 0) {
              lasaisie = nombre1 + tabi[0] + ' × ' + nombre2 + tabi[1] + ' × (' + nombre3 + tabi[2] + ')' + ' = '
            } else {
              lasaisie = nombre1 + tabi[0] + ' × ' + nombre2 + tabi[1] + ' × ' + nombre3 + tabi[2] + ' = '
            }
          }
          // La solution :
          if (nombreDeI === 1) {
          // un i :
            stor.solution[0] = Math.round(parseInt(nombre1) * parseInt(nombre2) * parseInt(nombre3)) + 'i'
          }
          if (nombreDeI === 2) {
          // deux i :
            stor.solution[0] = Math.round(parseInt(nombre1) * parseInt(nombre2) * parseInt(nombre3)) * (-1)
          }
          if (nombreDeI === 3) {
          // t i :
            stor.solution[0] = Math.round(parseInt(nombre1) * parseInt(nombre2) * parseInt(nombre3)) * (-1) + 'i'
          }
        }
        me.stockage[0] = lasaisie

        stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
        ;[1, 2].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })
        j3pAffiche(stor.zoneCons1, '', ds.textes.texte1)
        const laSaisie2 = lasaisie.replace(/i(?!m)/g, '\\mathrm{i}')
        const elt = j3pAffiche(stor.zoneCons2, '', '$' + laSaisie2 + '$&1&',
          {
            inputmq1: { width: '18px', dynamique: true, maxchars: '15' }
          })
        stor.zoneInput = elt.inputmqList[0]
        mqRestriction(stor.zoneInput, '\\d,-i')

        stor.zoneExpli = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '30px' }) })
        stor.tabi = tabi
        j3pFocus(stor.zoneInput)
        stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '10px' }) })
      }
      me.finEnonce()
      break // case "enonce":

    case 'correction':

      stor.zoneCorr.innerHTML = ''
      // on teste si une réponse a été saisie
      {
        const repEleve = j3pValeurde(stor.zoneInput)

        if ((repEleve === '') && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else { // une réponse a été saisie
          const reponseEtudiee = traitementReponse(repEleve)
          if (reponseEtudiee.juste) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            if (reponseEtudiee.simplifiee) {
              stor.zoneCorr.innerHTML = cBien
            }
            me.typederreurs[0]++
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneInput.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              afficheCorrection()

              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                // j3pFocus("affiche1input1");
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
                j3pFocus(stor.zoneInput)
              } else {
                me._stopTimer()
                afficheCorrection()
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
