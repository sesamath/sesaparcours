import { j3pAddElt, j3pAjouteBouton, j3pChaine, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomInt, j3pPaletteMathquill, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mai 2020
        Sections en 3 étapes. Pour les deux premières, on demande la décomposition d’un nombre ne facteurs premiers
        Pour la troisième on demande le PGCD
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['typeNbs', 'grands', 'liste', 'On peut imposer des nombres plus ou moins grands suivant le niveau de difficulté attendu', ['grands', 'petits']],
    ['nbchancesDecomp', 4, 'entier', 'Nombre possible de validations de la décomposition'],
    ['nbchancesPGCD', 1, 'entier', 'Nombre possible de validations de la réponse sur le PGCD'],
    ['seuilReussite', 0.65, 'reel', 'Seuil à partir duquel on considère la compétence acquise']
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Problème dans la décomposition en produit de facteurs premiers' },
    { pe_2: 'Problème dans la détermination du PGCD' },
    { pe_3: 'La décomposition et la détermination du PGCD posent problème' },
    { pe_4: 'Bien' }
  ]
}
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let i
    j3pDetruit(stor.zoneCalc)
    j3pDetruit(stor.zoneCons5)
    j3pDetruitFenetres('Calculatrice')
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    if (me.questionCourante % ds.nbetapes === 0) {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, stor.objCons)
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2, stor.objCons)
    } else {
      let produit = stor.objCons.reponse
      let produitTxt = ''
      const tabFacteurs = [100, 10, 9, 9, 3, 25]
      for (i = 0; i < tabFacteurs.length; i++) {
        // On cherche les facteurs intéressants
        if (produit % tabFacteurs[i] === 0) {
          if (produitTxt === '') {
            produitTxt = String(tabFacteurs[i])
          } else {
            produitTxt += '\\times ' + tabFacteurs[i]
          }
          produit = produit / tabFacteurs[i]
        }
      }
      let produitRep = ''
      for (i = 0; i < stor.objCons.lesFacteurs.length; i++) {
        if (produit % stor.objCons.lesFacteurs[i] === 0) {
          let puis = 0
          while (produit % Math.pow(stor.objCons.lesFacteurs[i], puis + 1) === 0) {
            puis++
          }
          produit = produit / Math.pow(stor.objCons.lesFacteurs[i], puis)
          if (produitTxt !== '') produitTxt += '\\times '
          produitTxt += String(Math.pow(stor.objCons.lesFacteurs[i], puis))
        }
      }
      for (i = 0; i < stor.objCons.lesFacteurs.length; i++) {
        if (stor.objCons['lesPuis' + (me.questionCourante % ds.nbetapes)][stor.objCons.lesFacteurs[i]] > 0) {
          if (produitRep !== '') produitRep += '\\times '
          produitRep += (stor.objCons['lesPuis' + (me.questionCourante % ds.nbetapes)][stor.objCons.lesFacteurs[i]] === 1)
            ? stor.objCons.lesFacteurs[i]
            : stor.objCons.lesFacteurs[i] + '^' + stor.objCons['lesPuis' + (me.questionCourante % ds.nbetapes)][stor.objCons.lesFacteurs[i]]
        }
      }
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr3)
      j3pAffiche(stor.zoneExpli2, '', '$' + stor.objCons.n + '=' + produitTxt + '$')
      j3pAffiche(stor.zoneExpli3, '', '$' + stor.objCons.n + '=' + produitRep + '$')
    }
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 2,
      nbetapes: 3,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchancesDecomp: 4,
      nbchancesPGCD: 1,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.
      seuilReussite: 0.65,
      typeNbs: 'grands',
      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'PGCD avec décomposition en produit de facteurs premiers',
        // on donne les phrases de la consigne
        consigne1: 'Écrire le nombre £n sous la forme d’un produit de facteurs premiers (sans répétition d’un même facteur mais avec utilisation éventuelle des puissances).',
        nbEssais_1: 'Il reste £e essais.',
        nbEssais_2: 'Il reste un essai.',
        consigne2: 'Sachant que $£{d1}$ et $£{d2}$, le plus grand diviseur commun de £{n1} et £{n2} vaut &1&.',
        rmq: 'Écrire la valeur de ce nombre et non sa décomposition.',
        nbEssais_3: 'Il n’y a qu’un seul essai pour répondre.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'La réponse donnée est bien égale à £n, mais elle n’est pas écrite sous la forme demandée&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'Pour chacun des facteurs premiers présents dans la décomposition de £{n1} et £{n2}, on prend la plus grande puissance possible présente.',
        corr2: 'Ainsi $\\text{PGCD}(£{n1}\\quad ;\\quad £{n2})=£c=£r$.',
        corr3: 'On peut isoler des facteurs assez simples à identifier, notamment à l’aide des critères de divisibilité.'
      },
      pe_1: 'Problème dans la décomposition en produit de facteurs premiers',
      pe_2: 'Problème dans la détermination du PGCD',
      pe_3: 'La décomposition et la détermination du PGCD posent problème',
      pe_4: 'Bien'
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }
  function afficheNbTentatives (nb) {
    j3pEmpty(stor.zoneCons2)
    if (nb === 1) {
      j3pAffiche(stor.zoneCons2, '', ds.textes.nbEssais_2, { style: { color: 'blue' } })
    } else if (nb > 1) {
      j3pAffiche(stor.zoneCons2, '', ds.textes.nbEssais_1, { e: nb, style: { color: 'blue' } })
    }
  }
  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    let i
    if (me.questionCourante % ds.nbetapes === 0) {
      ds.nbchances = ds.nbchancesPGCD
    } else {
      ds.nbchances = ds.nbchancesDecomp
    }
    if (me.questionCourante % ds.nbetapes === 1) {
      // Nouvelle question, donc on choisit 2 nombres dont on cherche le PGCD
      stor.objCons = {}
      const tabFacteurs1 = [2, 3] // Ces facteurs pourront être pris entre 0 et 4 fois (0 et 2 pour le niveau facile)
      const tabFacteurs2 = [5, 7, 11] // Ces facteurs seront choisis entre 0 et 2 fois (si les puissances de 2 et 3 ne sont pas trop élevées) (0 et 1 fois pour le niveau facile)
      const tabFacteurs3 = [13, 17, 19, 23] // si on n’en choisit pas dans tabFacteurs2 ou un seul avec une puissance 1, alors on peut choisir l’un de ces facteurs
      stor.objCons.lesFacteurs = tabFacteurs1.concat(tabFacteurs2, tabFacteurs3)
      do {
        stor.objCons.lesPuis1 = {}
        stor.objCons.n1 = 1
        stor.objCons.n2 = 1
        stor.objCons.r = 1 // Ce sera le pgcd
        stor.objCons.lesPuis2 = {}
        stor.objCons.lesPuisPGCD = {}
        // Cet objet fonctionnera comme un dictionnaire où les clés seront les facteurs des tableaux précédents et les valeurs leur puissance dans le premier nombre (resp le deuxième)
        // traitement du premier nombre
        let sommePuis = 0
        if (ds.typeNbs === 'grands') {
          stor.objCons.lesPuisPGCD[2] = j3pGetRandomInt(0, 3)
          stor.objCons.lesPuisPGCD[3] = j3pGetRandomBool() ? 4 - stor.objCons.lesPuisPGCD[2] : 3 - stor.objCons.lesPuisPGCD[2]
        } else {
          do {
            stor.objCons.lesPuisPGCD[2] = j3pGetRandomInt(0, 2)
            stor.objCons.lesPuisPGCD[3] = j3pGetRandomInt(0, (stor.objCons.lesPuisPGCD[2] === 2) ? 1 : 2)
          } while (stor.objCons.lesPuisPGCD[2] + stor.objCons.lesPuisPGCD[3] < 2)
        }
        for (i = 0; i < tabFacteurs1.length; i++) {
          do {
            stor.objCons.lesPuis1[tabFacteurs1[i]] = j3pGetRandomInt(stor.objCons.lesPuisPGCD[tabFacteurs1[i]], (ds.typeNbs === 'grands') ? 4 : 3)
            stor.objCons.lesPuis2[tabFacteurs1[i]] = j3pGetRandomInt(stor.objCons.lesPuisPGCD[tabFacteurs1[i]], (ds.typeNbs === 'grands') ? 4 : 3)
          } while (stor.objCons.lesPuis2[tabFacteurs1[i]] > stor.objCons.lesPuisPGCD[tabFacteurs1[i]] && stor.objCons.lesPuis1[tabFacteurs1[i]] > stor.objCons.lesPuisPGCD[tabFacteurs1[i]])
          stor.objCons.n1 *= Math.pow(tabFacteurs1[i], stor.objCons.lesPuis1[tabFacteurs1[i]])
          stor.objCons.n2 *= Math.pow(tabFacteurs1[i], stor.objCons.lesPuis2[tabFacteurs1[i]])
          sommePuis += stor.objCons.lesPuisPGCD[tabFacteurs1[i]]
          stor.objCons.r *= Math.pow(tabFacteurs1[i], stor.objCons.lesPuisPGCD[tabFacteurs1[i]])
        }
        const choixFacteur2 = j3pGetRandomElt(tabFacteurs2.concat(tabFacteurs3))
        const puis = (ds.typeNbs === 'grands')
          ? (sommePuis < 4 && (tabFacteurs2.indexOf(choixFacteur2) > -1)) ? j3pGetRandomInt(1, 2) : 1
          : 1
        stor.objCons.lesPuisPGCD[choixFacteur2] = puis
        stor.objCons.r *= Math.pow(choixFacteur2, puis)
        sommePuis = 0
        for (i = 0; i < tabFacteurs2.length; i++) {
          if (tabFacteurs2[i] === choixFacteur2) {
            stor.objCons.lesPuis1[tabFacteurs2[i]] = j3pGetRandomInt(stor.objCons.lesPuisPGCD[choixFacteur2], 2)
            if (stor.objCons.lesPuis1[tabFacteurs2[i]] === stor.objCons.lesPuisPGCD[choixFacteur2]) {
              stor.objCons.lesPuis2[tabFacteurs2[i]] = j3pGetRandomInt(stor.objCons.lesPuisPGCD[choixFacteur2], 2)
            } else {
              stor.objCons.lesPuis2[tabFacteurs2[i]] = stor.objCons.lesPuisPGCD[choixFacteur2]
            }
          } else if (sommePuis === 0) {
            stor.objCons.lesPuisPGCD[tabFacteurs2[i]] = 0
            if (stor.objCons.lesPuisPGCD[choixFacteur2] === 2) {
              stor.objCons.lesPuis1[tabFacteurs2[i]] = 0
              stor.objCons.lesPuis2[tabFacteurs2[i]] = 0
            } else {
              stor.objCons.lesPuis1[tabFacteurs2[i]] = j3pGetRandomInt(0, 1)
              do {
                stor.objCons.lesPuis2[tabFacteurs2[i]] = j3pGetRandomInt(0, 1)
              } while ((stor.objCons.lesPuis1[tabFacteurs2[i]] === 1) && (stor.objCons.lesPuis2[tabFacteurs2[i]] === 1))
              sommePuis += Math.max(stor.objCons.lesPuis1[tabFacteurs2[i]], stor.objCons.lesPuis2[tabFacteurs2[i]])
            }
          } else {
            stor.objCons.lesPuisPGCD[tabFacteurs2[i]] = 0
            stor.objCons.lesPuis1[tabFacteurs2[i]] = 0
            stor.objCons.lesPuis2[tabFacteurs2[i]] = 0
          }
          stor.objCons.n1 *= Math.pow(tabFacteurs2[i], stor.objCons.lesPuis1[tabFacteurs2[i]])
          stor.objCons.n2 *= Math.pow(tabFacteurs2[i], stor.objCons.lesPuis2[tabFacteurs2[i]])
        }
        for (i = 0; i < tabFacteurs3.length; i++) {
          if (tabFacteurs3[i] === choixFacteur2) {
            stor.objCons.lesPuis1[choixFacteur2] = stor.objCons.lesPuisPGCD[choixFacteur2]
            stor.objCons.lesPuis2[choixFacteur2] = stor.objCons.lesPuisPGCD[choixFacteur2]
          } else if (sommePuis === 0) {
            stor.objCons.lesPuisPGCD[tabFacteurs3[i]] = 0
            if (stor.objCons.lesPuisPGCD[choixFacteur2] === 2) {
              stor.objCons.lesPuis1[tabFacteurs3[i]] = 0
              stor.objCons.lesPuis2[tabFacteurs3[i]] = 0
            } else {
              stor.objCons.lesPuis1[tabFacteurs3[i]] = j3pGetRandomInt(0, 1)
              do {
                stor.objCons.lesPuis2[tabFacteurs3[i]] = j3pGetRandomInt(0, 1)
              } while ((stor.objCons.lesPuis1[tabFacteurs3[i]] === 1) && (stor.objCons.lesPuis2[tabFacteurs3[i]] === 1))
              sommePuis += Math.max(stor.objCons.lesPuis1[tabFacteurs3[i]], stor.objCons.lesPuis2[tabFacteurs3[i]])
            }
          } else {
            stor.objCons.lesPuisPGCD[tabFacteurs3[i]] = 0
            stor.objCons.lesPuis1[tabFacteurs3[i]] = 0
            stor.objCons.lesPuis2[tabFacteurs3[i]] = 0
          }
          stor.objCons.n1 *= Math.pow(tabFacteurs3[i], stor.objCons.lesPuis1[tabFacteurs3[i]])
          stor.objCons.n2 *= Math.pow(tabFacteurs3[i], stor.objCons.lesPuis2[tabFacteurs3[i]])
        }
      } while (stor.objCons.r === stor.objCons.n1 || stor.objCons.r === stor.objCons.n2)
    }
    me.logIfDebug('stor.objCons:', stor.objCons)

    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    stor.objCons.n = (me.questionCourante % ds.nbetapes === 1) ? stor.objCons.n1 : stor.objCons.n2
    for (i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    let mesZonesSaisie
    if (me.questionCourante % ds.nbetapes === 0) {
      // recherche du PGCD
      for (i = 1; i <= 2; i++) {
        stor.objCons['d' + i] = ''
        for (const elt in stor.objCons['lesPuis' + i]) {
          if (stor.objCons['d' + i] !== '' && stor.objCons['lesPuis' + i][elt] > 0) stor.objCons['d' + i] += '\\times '
          if (stor.objCons['lesPuis' + i][elt] === 1) {
            stor.objCons['d' + i] += elt
          } else if (stor.objCons['lesPuis' + i][elt] > 0) {
            stor.objCons['d' + i] += elt + '^{' + stor.objCons['lesPuis' + i][elt] + '}'
          }
        }
        stor.objCons['d' + i] = stor.objCons['n' + i] + '=' + stor.objCons['d' + i]
      }
      // Pour l’affichage du PGCD en produit de facteurs premiers
      stor.objCons.c = ''
      for (const elt in stor.objCons.lesPuisPGCD) {
        if (stor.objCons.c !== '' && stor.objCons.lesPuisPGCD[elt] > 0) stor.objCons.c += '\\times '
        if (stor.objCons.lesPuisPGCD[elt] === 1) {
          stor.objCons.c += elt
        } else if (stor.objCons.lesPuisPGCD[elt] > 0) {
          stor.objCons.c += elt + '^{' + stor.objCons.lesPuisPGCD[elt] + '}'
        }
      }
      stor.objCons.reponse = stor.objCons.r
      const elt = j3pAffiche(stor.zoneCons1, '', ds.textes.consigne2, stor.objCons)
      stor.zoneInput = elt.inputmqList[0]
      j3pAffiche(stor.zoneCons2, '', ds.textes.rmq)
      if (ds.nbchances === 1) j3pAffiche(stor.zoneCons3, '', ds.textes.nbEssais_3, { style: { fontStyle: 'italic', fontSize: '0.9em' } })
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      stor.zoneInput.reponse = [stor.objCons.r]
      mesZonesSaisie = [stor.zoneInput.id]
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: mesZonesSaisie
      })
      mqRestriction(stor.zoneInput, '\\d')
      j3pFocus(stor.zoneInput)
    } else {
      stor.nbTentatives = Math.max(ds.nbchancesDecomp, 1)
      j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, stor.objCons)
      afficheNbTentatives(stor.nbTentatives)
      const elt = j3pAffiche(stor.zoneCons4, '', '$£n=$&1&', {
        n: stor.objCons.n,
        inputmq1: { texte: '' }
      })
      stor.zoneInput = elt.inputmqList[0]
      mqRestriction(stor.zoneInput, '\\d*\\^', { commandes: ['puissance'] })
      j3pStyle(stor.zoneCons5, { paddingTop: '10px' })
      j3pPaletteMathquill(stor.zoneCons5, stor.zoneInput, { liste: ['puissance'] })
      mesZonesSaisie = [stor.zoneInput.id]
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: mesZonesSaisie,
        validePerso: mesZonesSaisie
      })
      stor.objCons.reponse = stor.objCons['n' + (me.questionCourante % ds.nbetapes)]
      j3pFocus(stor.zoneInput)
    }
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.zoneD, 'div', '')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]//, left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '')

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        let formeSimplifiee = true
        let i
        const reponse = stor.fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si les 4 zones ont bien été complétées ou non
        // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau validePerso ont des réponses correctes ou non.
        if ((me.questionCourante % ds.nbetapes !== 0) && reponse.aRepondu) {
          let reponseSaisie = stor.fctsValid.zones.reponseSaisie[0]
          reponse.bonneReponse = (Math.abs(j3pCalculValeur(reponseSaisie) - stor.objCons.reponse) < Math.pow(10, -12))
          if (reponse.bonneReponse) {
          // On vérifie si c’est la forme simplifiée
            const num = me.questionCourante % ds.nbetapes
            for (i = 0; i < stor.objCons.lesFacteurs.length; i++) {
              if (stor.objCons['lesPuis' + num][stor.objCons.lesFacteurs[i]] === 1) {
                reponseSaisie = reponseSaisie.replace(stor.objCons.lesFacteurs[i], '')
              } else if (stor.objCons['lesPuis' + num][stor.objCons.lesFacteurs[i]] > 0) {
                reponseSaisie = reponseSaisie.replace(stor.objCons.lesFacteurs[i] + '^' + stor.objCons['lesPuis' + num][stor.objCons.lesFacteurs[i]], '')
              }
            }
            // Si j’ai bien tous les facteurs souhaités, alors je ne devrais plus avoir de chiffre à la fin
            reponse.bonneReponse = !(/[0-9]/g.test(reponseSaisie))
            if (!reponse.bonneReponse) {
              formeSimplifiee = false
              reponse.bonneReponse = false
              stor['zoneTentative' + stor.nbTentatives] = j3pAddElt(stor.zoneCons3, 'div', '', { style: { color: me.styles.cbien } })
              j3pAffiche(stor['zoneTentative' + stor.nbTentatives], '', '$' + stor.objCons.n + '= ' + stor.fctsValid.zones.reponseSaisie[0] + '$')
            }
          } else {
            stor['zoneTentative' + stor.nbTentatives] = j3pAddElt(stor.zoneCons3, 'div', '', { style: { color: me.styles.cfaux } })
            j3pAffiche(stor['zoneTentative' + stor.nbTentatives], '', '$' + stor.objCons.n + '\\neq ' + stor.fctsValid.zones.reponseSaisie[0] + '$')
          }
          if (!reponse.bonneReponse) {
            stor.fctsValid.zones.bonneReponse[0] = false
            stor.nbTentatives--
            afficheNbTentatives(stor.nbTentatives)
          }
          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          stor.fctsValid.coloreUneZone(stor.zoneInput.id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
              if (!formeSimplifiee) {
              // c’est que l’expression entrée dans la quatrième zone de saisie est simplifiable
                stor.zoneCorr.innerHTML += '<br>' + j3pChaine(ds.textes.comment1, { n: stor.objCons.n })
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                // On comptabilise le nombre d’erreur de chaque type
                if (me.questionCourante % ds.nbetapes === 0) {
                  me.typederreurs[4]++ // pb sur le PGCD
                } else {
                  me.typederreurs[3]++ // Pb sur la décomposition
                }
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        // On vérifie s’il a fait trop d’erreur sur la décomposition
        const erreurDecomp = (me.typederreurs[3] / (2 * ds.nbrepetitions) > (1 - ds.seuilReussite))
        const erreurPGCD = (me.typederreurs[4] / (ds.nbrepetitions) > (1 - ds.seuilReussite))
        if (erreurDecomp) {
          if (erreurPGCD) {
            me.parcours.pe = ds.pe_3
          } else {
            me.parcours.pe = ds.pe_1
          }
        } else if (erreurPGCD) {
          me.parcours.pe = ds.pe_2
        } else {
          me.parcours.pe = ds.pe_4
        }
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
