import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pChaine, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pPaletteMathquill, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pMathsAjouteDans, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgApp } from 'src/lib/outils/mathgraph'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    2020
    Section demandant d’abord de décomposer un entier en produit de facteurs premiers puis de donner tous ses diviseurs
 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['aDecomposer', '[24;29;32;37;36;40;41;48;50;56;64;75;88;98;108;150;220;225;242;308;450]', 'string',
      'Liste des nombres dont on demandera la décomposition et les diviseurs. Si vide, on choisit un nombre entre min et max au hasard'],
    ['min', 6, 'entier', 'Valeur mini du nombre dont on demande les diviseurs'],
    ['max', 100, 'entier', 'Valeur mini du nombre dont on demande les diviseurs'],
    ['nbEssais1', 3, 'entier', 'Nombre d’essais maximum autorisés pour la décomposition en produit de facteurs premeirs'],
    ['nbEssais2', 2, 'entier', 'Nombre d’essais maximum autorisés pour donner tous les diviseurs'],
    ['coefEtape1', 0.5, 'number', 'Nombre strictement compris entre 0 et 1 à rajouter au score<br>pour une réussite à l’étape 1']
  ]
}

const textes = {
  consigneInit: 'Le but est de trouver tous les diviseurs dans $\\N$ du nombre $£a$.' +
        '<br>Dans un premier temps on demande de donner la décomposition de $£a$ en produit de puissances de facteurs premiers.' +
        '<br>Elle doit être écrite sous la forme la plus simple possible (ne pas utiliser d’exposant égal à 1). Si le nombre est premier, entrer le nombre comme réponse.',
  consEditeur: 'Diviseurs de $£a$ : &1&',
  etape2_cons1: 'On demande maintenant de donner tous les diviseurs positifs de $£a$, séparés par des ;',
  recopier: 'Recopier réponse précédente',
  essai1: 'Il reste un essai.',
  essai2: 'Il reste £e essais.',
  validation: 'Il reste une validation.',
  rep1: 'Réponse exacte&nbsp;: ',
  rep2: 'Réponse fausse&nbsp;: ',
  rep3: 'Exact pas fini&nbsp;: ',
  lasol: 'La solution se trouve ci-contre.',
  questSuiv: 'On passe à la question suivante.',
  pbDecomp: 'Le nombre n’est pas décomposé comme demandé',
  pbRepet: 'Un même diviseur ne doit pas être répété plusieurs fois',
  divAbsents: 'Il manque des diviseurs.',
  rep4_1: 'Les diviseurs positifs de £a sont £b',
  rep4_2: 'Les diviseurs positifs de £a ne sont pas £b',
  corr1: '£a est un  nombre premier. Ses deux seuls diviseurs dans $\\N$ sont 1 et £a.',
  corr2: 'Les diviseurs positifs de £a sont&nbsp;: ',
  corr3: 'Rangés dans l’ordre croissant&nbsp;: ',
  corr4: 'On peut obtenir ces solutions à l’aide d’un arbre.',
  nextQuest: 'On passe à la question suivante.',
  decompFournie: 'Voici la décomposition demandée :'
}

/**
 * section diviseurs_Entier
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const stor = this.storage
  const ds = this.donneesSection

  function onkeyup (ev) {
    if (stor.marked) demarqueEditeurPourErreur()
  }

  function traiteMathQuill (ch) {
    ch = ch.replace(/\\times/g, '*') // POur traiter les signes de multiplication
    return stor.listePourCalc.addImplicitMult(ch) // Traitement des multiplications implicites
  }

  function montrePaletteBoutons (bvisible) {
    $('#boutonsmathquill').css('display', bvisible ? 'block' : 'none')
  }

  function zero (x) {
    return Math.abs(x) < 1e-9
  }

  function entier (x) {
    return zero(x - Math.floor(x))
  }

  /**
   * Renvoie le plus grand exposant de q qui divise n si n est divisible par q, sinon renvoie 0
   */
  function divPar (x, q) {
    if (entier(x / q)) {
      let exp = 1
      let k = q
      while (entier(x / (q * k))) {
        exp++
        k = k * q
      }
      return exp
    }
    return 0
  }

  function listeADecomposer () {
    let st = ds.aDecomposer
    if (st === '') {
      return []
    } else {
      const res = []
      // On retire les crochets de début et de fin
      st = st.replace(/\s*\[\s*/g, '').replace(/\s*]\s*/g, '').replace(/,/g, ';')
      const tab = st.split(';')
      for (let i = 0; i < tab.length; i++) {
        if (stor.listePourCalc.verifieSyntaxe(tab[i])) {
          stor.listePourCalc.giveFormula2('rep', tab[i])
          stor.listePourCalc.calculateNG()
          const nb = stor.listePourCalc.valueOf('rep')
          if (nb !== 1) res.push(nb)
        }
      }
      return res
    }
  }

  function prepareNombre () {
    let n
    if (stor.listeADecomposerInit.length !== 0) {
      if (stor.listeADecomposer.length === 0) stor.listeADecomposer = stor.listeADecomposerInit
      // On tire un nombre au hasard parmi les nombres à décomposer restants
      const ind = Math.floor(Math.random() * stor.listeADecomposer.length)
      n = stor.listeADecomposer[ind]
      stor.listeADecomposer.splice(ind, 1)
    } else {
      const min = ds.min
      const max = ds.max
      n = min + Math.floor(Math.random() * (max - min + 1))
    }
    let nb = n
    const factprem = []
    const exposants = []
    let exp = divPar(n, 2)
    if (exp > 0) {
      factprem.push(2)
      exposants.push(exp)
      nb = nb / Math.pow(2, exp)
    }
    exp = divPar(n, 3)
    if (exp > 0) {
      factprem.push(3)
      exposants.push(exp)
      nb = nb / Math.pow(3, exp)
    }
    let k = 5
    let pas = 4
    while (k * k <= nb) {
      exp = divPar(nb, k)
      if (exp > 0) {
        factprem.push(k)
        exposants.push(exp)
        nb = nb / Math.pow(k, exp)
      }
      pas = 6 - pas
      k = k + pas
    }
    if (nb !== 1) {
      factprem.push(nb)
      exposants.push(1)
    }
    let st = ''
    for (let i = 0; i < factprem.length; i++) {
      if (i > 0) st += '*'
      st += String(factprem[i])
      if (exposants[i] > 1) st += '^' + String(exposants[i])
    }
    stor.n = n
    stor.factprem = factprem
    stor.exposants = exposants
    stor.decomp = st
    let nbdiv = 1
    for (let i = 0; i < exposants.length; i++) {
      nbdiv *= (exposants[i] + 1)
    }
    stor.nbdiv = nbdiv
  }

  function getLatexDecomp () {
    stor.listePourCalc.giveFormula2('calc', stor.decomp)
    stor.listePourCalc.calculateNG()
    return stor.listePourCalc.getLatexCode(0)
  }

  /**
   * Fonction récursive fournissant dans un tableau la liste de tous les diviseurs d’un entier naturel
   * @param tabdiv : Un tableau contenant la liste de diviseurs premier p du nombre
   * @param tabexp : un tableau fournissant, pour chaque diviseur premier, la plus grand exposant k tel que p^k divise le nombre
   * @return {[]|number[]}
   */
  function listeDiviseurs (tabdiv, tabexp) {
    const len = tabdiv.length
    if (len === 0) return [1]
    const newtabdiv = tabdiv.slice(1, len)
    const newtabexp = tabexp.slice(1, len)
    const newlistdiv = listeDiviseurs(newtabdiv, newtabexp)
    const div = tabdiv[0]
    const exp = tabexp[0]
    const res = []
    for (let i = 0; i <= exp; i++) {
      for (let j = 0; j < newlistdiv.length; j++) {
        res.push(Math.pow(div, i) * newlistdiv[j])
      }
    }
    return res
  }

  /**
   * Fonction renvoyant, pour chacun des diviseurs du nombre stor.n à décomposer, une chaîne représentant sa décomposition
   * suivant tous les diviseurs premeirs aevc des exposants égaux à 1 lorsque le facteur premier ne divise pas stor.n
   * @param div
   * @return {string}
   */
  function decomposeDiviseur (div) {
    let st = '$' + String(div) + '='
    for (let i = 0; i < stor.factprem.length; i++) {
      const exp = divPar(div, stor.factprem[i])
      if (i !== 0) st += '\\times '
      st += String(stor.factprem[i]) + '^{' + String(exp) + '}'
    }
    st += '$'
    return st
  }

  function marqueEditeurPourErreur (indice) {
    stor.marked = true
    $('#expressioninputmq1').css('background-color', '#FF9999')
  }

  function demarqueEditeurPourErreur (indice) {
    if (!stor.marked) return
    stor.marked = false // Par précaution si appel via bouton recopier réponse trop tôt
    $('#expressioninputmq1').css('background-color', '#FFFFFF')
  }

  function validationEditeur () {
    let chcalcul, valide, res
    const rep = j3pValeurde('expressioninputmq1')
    if (stor.etape === 1) {
      chcalcul = traiteMathQuill(rep)
      valide = stor.listePourCalc.verifieSyntaxe(chcalcul)
      if (valide) return true
      else {
        marqueEditeurPourErreur()
        res = false
      }
    } else {
      // On attnd des nombres entiers séparés par des ;
      res = true
      if (rep === '') {
        res = false
        marqueEditeurPourErreur()
      } else {
        const tab = rep.split(';')
        for (let i = 0; (i < tab.length) && res; i++) {
          if (tab[i] === '') {
            res = false
          }
        }
      }
    }
    if (!res) {
      marqueEditeurPourErreur()
      j3pFocus('expressioninputmq1')
    }
    return res
  }

  function montreEditeur (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    if (bVisible) {
      $('#expressioninputmq1').mathquill('latex', ' ')
      j3pFocus('expressioninputmq1')
    }
  }

  function videEditeur () {
    $('#expressioninputmq1').mathquill('latex', ' ').focus()
  }

  function creeEditeur () {
    let st
    if (stor.etape === 1) st = '$£a$ = &1&'
    else st = textes.consEditeur
    j3pAffiche('editeur', 'expression', st, { a: stor.n }) // stor.formulaire contient ou plusieurs éditeurs MathQuill
    // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
    mqRestriction('expressioninputmq1', stor.etape === 1 ? '\\d²\\^\\*' : '\\d\\;', {
      boundingContainer: parcours.zonesElts.MG
    })
    j3pElement('expressioninputmq1').onkeyup = function (ev) {
      onkeyup.call(this)
    }
  }

  function afficheReponse (bilan, depasse) {
    let content, color
    if (bilan === 'exact') {
      content = textes.rep1
      color = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        content = textes.rep2
        color = parcours.styles.cfaux
      } else { // Exact mais pas fini
        content = textes.rep3
        if (depasse) color = parcours.styles.cfaux
        else color = '#0000FF'
      }
    }
    const num = stor.numEssai
    const idrep = 'exp' + String(stor.etape) + String(num)
    if (num > 2) content = '<br>' + content
    j3pAffiche(stor.etape === 1 ? 'formules' : 'formulesSuite', idrep + 'deb', content, { style: { color } })
    if (stor.etape === 1) {
      content = '$' + '\\textcolor{' + color + '}{' + String(stor.n) + ((bilan === 'faux') ? '\\ne' : '=') + stor.rep + '}$'
    } else {
      const chDiv = (bilan === 'faux')
        ? j3pChaine(textes.rep4_2, { a: String(stor.n), b: stor.rep })
        : j3pChaine(textes.rep4_1, { a: String(stor.n), b: stor.rep })
      content = '$' + '\\textcolor{' + color + '}{\\text{' + chDiv + '}}$'
    }
    j3pMathsAjouteDans(stor.etape === 1 ? 'formules' : 'formulesSuite', { id: idrep + String(stor.etape), content, parametres: { style: { color } } })
  }

  function afficheSolution () {
    let st
    const tabdiviseurs = listeDiviseurs(stor.factprem, stor.exposants)
    if ((stor.factprem.length === 1) && (stor.exposants[0] === 1)) { // Cas d’un nombre premier
      st = j3pChaine(textes.corr1, { a: stor.n }) // stor.n + ' est un  nombre premier. Ses deux seuls diviseurs dans $\\N$ sont 1 et ' + stor.n + '.'
    } else {
      st = j3pChaine(textes.corr2, { a: stor.n }) // 'Les diviseurs de ' + stor.n + ' sont : '
      for (let i = 0; i < tabdiviseurs.length; i++) {
        if (i !== 0) st += ';'
        st += String(tabdiviseurs[i])
      }
      let tabdiviseursOrdonne = []
      tabdiviseursOrdonne = tabdiviseursOrdonne.concat(tabdiviseurs)
      tabdiviseursOrdonne.sort(function compare (a, b) { return a - b })
      let identiques = true
      for (let i = 0; i < tabdiviseurs.length; i++) {
        if (tabdiviseurs[i] !== tabdiviseursOrdonne[i]) {
          identiques = false
          break
        }
      }
      if (!identiques) {
        st += '<br>' + textes.corr3
        for (let i = 0; i < tabdiviseursOrdonne.length; i++) {
          if (i !== 0) st += ';'
          st += String(tabdiviseursOrdonne[i])
        }
      }
      st += '<br>' + textes.corr4
      // On rajoute pour chaque diviseur sa décomposition avec éventuellement des exposants égaux à 0
      for (let i = 0; i < tabdiviseurs.length; i++) {
        st += '<br>' + decomposeDiviseur(tabdiviseurs[i])
      }
    }
    j3pAffiche('divcorrection', 'textecor', st, { style: parcours.styles.petit.enonce })
  }

  function validation () {
    if (stor.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    stor.rep = j3pValeurde('expressioninputmq1')
    if (stor.etape === 1) {
      const chcalcul = traiteMathQuill(stor.rep)
      stor.listePourCalc.giveFormula2('rep', chcalcul)
      stor.listePourCalc.giveFormula2('calc', stor.decomp)
      stor.listePourCalc.calculateNG()
      stor.resolu = stor.listePourCalc.valueOf('resolu') === 1
      stor.exact = stor.listePourCalc.valueOf('exact') === 1
    } else {
      const tab = stor.rep.split(';')
      let reponsedouble = false
      const tabrep = []
      stor.diviseursExacts = true
      for (let i = 0; i < tab.length; i++) {
        if (!entier(stor.n / tab[i])) {
          stor.resolu = false
          stor.exact = false
          stor.diviseursExacts = false
          return
        }
        if (tabrep.indexOf(tab[i]) !== -1) {
          reponsedouble = true
        } else tabrep.push(tab[i])
      }
      if (tabrep.length === stor.nbdiv) {
        stor.resolu = !reponsedouble
        stor.exact = true
      } else {
        stor.resolu = false
        stor.exact = false
      }
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    $('#expressioninputmq1').mathquill('latex', stor.rep)
    j3pFocus('expressioninputmq1')
  }

  function etapeSuivante () {
    stor.etape = 2
    j3pEmpty('editeur')
    j3pEmpty('enonce')
    j3pEmpty('info')
    const st = textes.etape2_cons1 // 'On demande maintenant de donner tous les diviseurs de $£a$, séparés par des ;'
    // j3pAffiche('enonceSuite','texte', st, { a: stor.n})
    j3pAffiche('enonceSuite', 'texte', st, { a: stor.n })
    stor.numEssai = 1
    const nbe = ds['nbEssais' + stor.etape] - stor.numEssai + 1
    if (nbe === 1) j3pAffiche('info', 'texteinfo', textes.essai1, { style: { color: '#7F007F' } })
    else j3pAffiche('info', 'texteinfo', textes.essai2, { style: { color: '#7F007F' }, e: nbe })
    creeEditeur()
    montreEditeur(true)
    montrePaletteBoutons(false)
  }

  function depart () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans depart et modif_fig
      loadCoreWithMathJax: true
    }

    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      stor.mtgAppLecteur = mtgAppLecteur
      stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAARZAAACpQAAAQEAAAAAAAAAAQAAAAf#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AANyZXAAATAAAAABAAAAAAAAAAAAAAACAP####8ABGNhbGMAATAAAAABAAAAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wAEemVybwASYWJzKHgpPDAuMDAwMDAwMDAx#####wAAAAEACkNPcGVyYXRpb24E#####wAAAAIACUNGb25jdGlvbgD#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAAAT4RLgvoJtaVAAF4AAAAAgD#####AAVleGFjdAAOemVybyhyZXAtY2FsYyn#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAAADAAAABAH#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAAQAAAAgAAAAC#####wAAAAMAEENUZXN0RXF1aXZhbGVuY2UA#####wAGcmVzb2x1AAAAAQAAAAIAAAAAAAE#8AAAAAAAAAH#####AAAAAgAGQ0xhdGV4AP####8AAAAAAQAA#####xBAOgAAAAAAAEA2ZmZmZmZmAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAOXEZvclNpbXB7Y2FsY33###############8=')
      stor.listeADecomposer = listeADecomposer()
      stor.listeADecomposerInit = stor.listeADecomposer
      prepareNombre()
      stor.marked = false
      stor.etape = 1
      j3pAffiche('enonce', 'texte', textes.consigneInit, { a: stor.n })
      creeEditeur()
      j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: ['puissance'], nomdiv: 'palette' })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
      j3pFocus('expressioninputmq1')
    }).catch(j3pShowError)
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        // compteur.
        stor.numEssai = 1
        stor.nbEssais1 = parseInt(ds.nbEssais1)
        stor.nbEssais2 = parseInt(ds.nbEssais2)
        stor.coefEtape1 = parseFloat(ds.coefEtape1)
        if (ds.max < ds.min) ds.max = ds.min

        parcours.afficheTitre('Trouver les diviseurs d’un entier')
        j3pDiv(parcours.zones.MG, { id: 'conteneur', contenu: '', style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
        j3pDiv('conteneur', 'enonce', '')
        j3pDiv('conteneur', {
          id: 'formules',
          contenu: '',
          style: parcours.styles.petit.enonce
        }) // Contient le formules entrées par l’élève
        j3pDiv('conteneur', 'divsolutionetape1', '') // Le div qui contiendra la correction
        j3pDiv('conteneur', 'enonceSuite', '')
        j3pDiv('conteneur', {
          id: 'formulesSuite',
          contenu: '',
          style: parcours.styles.petit.enonce
        }) // Contient le formules entrées par l’élève

        j3pDiv('conteneur', 'info', '')
        const nbe = ds.nbEssais1
        if (nbe === 1) j3pAffiche('info', 'texteinfo', textes.essai1, { style: { color: '#7F007F' } })
        else j3pAffiche('info', 'texteinfo', textes.essai2, { style: { color: '#7F007F' }, e: nbe })
        j3pDiv('conteneur', 'conteneurbouton', '')
        j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', textes.recopier,
          recopierReponse)
        j3pElement('boutonrecopier').style.display = 'none'
        j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
        $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
        j3pDiv('conteneur', 'boutonsmathquill', '')
        $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
        j3pDiv('conteneur', 'divcorrection', '') // Le div qui contiendra la correction
        depart()
        const style = parcours.styles.petit.correction
        style.marginTop = '1.5em'
        style.marginLeft = '0.5em'
        j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
      } else {
        j3pEmpty('correction')
        this.afficheBoutonValider()
        stor.numEssai = 1
      }
      if (this.questionCourante > 1) {
        stor.etape = 1
        stor.marked = false
        prepareNombre()
        // videEditeur()
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')
        j3pEmpty('enonceSuite')
        j3pEmpty('formulesSuite')
        j3pEmpty('divsolutionetape1')
        j3pEmpty('divcorrection')
        j3pAffiche('enonce', 'texte', textes.consigneInit, { a: stor.n })
        const nbe = ds.nbEssais1 - stor.numEssai + 1
        if (nbe === 1) j3pAffiche('info', 'texteinfo', textes.validation, { style: { color: '#7F007F' } })
        else j3pAffiche('info', 'texteinfo', textes.essai2, { style: { color: '#7F007F' }, e: nbe })
        j3pElement('info').style.display = 'block'
        j3pEmpty('editeur')
        creeEditeur()
        montreEditeur(true)
        montrePaletteBoutons(true)
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          parcours.zonesElts.MG.scrollIntoView()
        } catch (e) {}
        j3pFocus('expressioninputmq1')
      }
      this.finEnonce()

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      if (validationEditeur()) {
        validation()
        if (stor.resolu) bilanReponse = 'exact'
        else {
          if (stor.exact) {
            bilanReponse = 'exactPasFini'
          } else bilanReponse = 'faux'
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        montreEditeur(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        afficheSolution()
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>' + textes.lasol
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            stor.mtgAppLecteur.setActive('mtg32svg', false)
            this.score += stor.etape === 1 ? stor.coefEtape1 : (1 - stor.coefEtape1)
            j3pElement('correction').style.color = this.styles.cbien
            let mess = cBien
            if (stor.etape === 1) mess += '<br>' + textes.questSuiv
            j3pElement('correction').innerHTML = mess
            montreEditeur(false)
            stor.numEssai++ // Pour un affichage correct dans afficheReponse
            afficheReponse('exact', false)
            if (stor.etape === 2) {
              this._stopTimer()
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              afficheSolution()
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              etapeSuivante()
            }
          } else {
            stor.numEssai++
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                  (stor.numEssai <= ds['nbEssais' + stor.etape]) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = (stor.etape === 1)
                ? textes.pbDecomp
                : textes.pbRepet
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pElement('correction').innerHTML = cFaux + (stor.diviseursExacts ? '<br>' + textes.divAbsents : '')
            }
            j3pEmpty('info')
            if (stor.numEssai <= ds['nbEssais' + stor.etape]) {
              const nbe = ds['nbEssais' + stor.etape] - stor.numEssai + 1
              if (nbe === 1) j3pAffiche('info', 'texteinfo', textes.essai1, { style: { color: '#7F007F' } })
              else j3pAffiche('info', 'texteinfo', textes.essai2, { style: { color: '#7F007F' }, e: nbe })
              videEditeur()
              afficheReponse(bilanReponse, false)
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
            } else {
              // Erreur au nème essai
              if (stor.etape === 2) {
                this._stopTimer()
                stor.mtgAppLecteur.setActive('mtg32svg', false)
                montreEditeur(false)
                j3pElement('boutonrecopier').style.display = 'none'
                j3pElement('info').style.display = 'none'

                afficheSolution()
                afficheReponse(bilanReponse, true)
                j3pElement('correction').innerHTML += '<br><br>' + textes.lasol
                this.etat = 'navigation'
                this.sectionCourante()
              } else {
                const stsol = textes.decompFournie + '<br>$' + stor.n + '=' + getLatexDecomp() + '$'
                j3pAffiche('divsolutionetape1', 'textedecomp', stsol, { style: parcours.styles.petit.enonce })
                j3pElement('correction').innerHTML += '<br><br>' + textes.nextQuest
                etapeSuivante()
              }
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
