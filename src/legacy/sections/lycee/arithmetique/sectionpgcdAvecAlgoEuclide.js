import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomInt, j3pMathquillXcas, j3pPGCD, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mai 2020
        Dans cete section, on cherche à déterminer le PGCD de deux nombres à l’aide de l’algorithme d’Euclide
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 3, 'entier', 'Nombre d’essais par répétition']
  ]

}
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    j3pDetruit(stor.zoneCons7)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1)
    // Dans me.zoneExpli2, on met toutes les étapes de l’algo
    const lignesAlgo = []
    for (let i = 0; i < stor.objCons.etapes.length; i++) {
      lignesAlgo[i] = j3pAddElt(stor.zoneExpli2, 'div', '')
      j3pAffiche(lignesAlgo[i], '', '$' + stor.objCons.etapes[i] + '$')
    }
    // dans explication1, on a l’explication correspondant à la première question
    j3pAffiche(stor.zoneExpli3, '', ds.textes.corr2, stor.objCons)
  }
  function afficheNbTentatives (nb) {
    j3pEmpty(stor.zoneCons3)
    if (nb === 1) {
      j3pAffiche(stor.zoneCons3, '', ds.textes.nbEssais_2, { style: { color: 'blue' } })
    } else if (nb !== 0) {
      j3pAffiche(stor.zoneCons3, '', ds.textes.nbEssais_1, { e: stor.nbTentatives, style: { color: 'blue' } })
    }
  }
  function genereDonnees () {
    // Cette fonction renvoie un objet contenant deux nombres n1 et n2 et leur pgcd r
    const obj = {}
    do {
      do {
        obj.r = 1
        // On peut lui mettre un facteur 2, 3 avec une proba 1/2
        if (j3pGetRandomBool()) obj.r = j3pGetRandomInt(2, 3)
        // éventuellement un autre facteur du tableau [5, 7, 11, 13, 17, 19] là encore avec une proba 1/2
        if (j3pGetRandomBool()) obj.r *= j3pGetRandomElt([5, 7, 11, 13, 17, 19])
        do {
          obj.n1 = obj.r * j3pGetRandomInt(Math.ceil(50 / obj.r), Math.ceil(800 / obj.r))
          obj.n2 = obj.r * j3pGetRandomInt(Math.ceil(50 / obj.r), Math.ceil(800 / obj.r))
        } while (Math.abs(obj.n1 - obj.n2) < obj.r + 1)
      } while ((j3pPGCD(obj.n1, obj.n2) !== obj.r) || (obj.n1 === obj.r) || (obj.n2 === obj.r))
      // On va également construire les étapes de l’algorithme'
      obj.etapes = []
      let a = Math.max(obj.n1, obj.n2)
      let b = Math.min(obj.n1, obj.n2)
      let q
      let r = b
      while (r !== 0) {
        q = Math.floor(a / b)
        r = a % b
        obj.etapes.push(a + '=' + b + '\\times ' + q + '+' + r)
        if (r !== 0) {
          a = b
          b = r
        }
      }
    } while (obj.etapes.length === 2 || obj.etapes.length > 5)
    me.logIfDebug('obj:', obj)
    return obj
  }
  function genereZone (legalite) {
    // Cette fonction va écrire dans stor.zoneCons4 l’étape de l’algo d’Euclide qui aura été réussi par l’élève
    if (stor.premiereEtape) {
      stor.tabEtapes = []
    } else {
      stor.tabEtapes.push(j3pAddElt(stor.zoneCons4, 'div', '', { style: { color: me.styles.cbien } }))
      j3pAffiche(stor.tabEtapes[stor.tabEtapes.length - 1], '', '$' + legalite + '$')
    }

    afficheLigne()
    stor.premiereEtape = false
  }
  function afficheLigne () {
    stor.validationPGCD = false
    j3pEmpty(stor.zoneCons6)
    const elt = j3pAffiche(stor.zoneCons6, '', '&1&', { inputmq1: { texte: '' } })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d=+*-', {
      boundingContainer: me.zonesElts.MG
    })
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: [stor.zoneInput.id],
      validePerso: [stor.zoneInput.id]
    })
    if (!stor.premiereEtape) {
      j3pEmpty(stor.zoneCons5)
      j3pAffiche(stor.zoneCons5, '', ds.textes.consigne3)
      j3pEmpty(stor.zoneCons7)
      j3pAjouteBouton(stor.zoneCons7, consigneConclusion, {
        id: stor.btnCcl,
        value: ds.textes.consigne4_1,
        className: 'MepBoutons'
      })
    }
    j3pEmpty(stor.zoneCons8)
    j3pFocus(stor.zoneInput, (me.questionCourante !== 1 || !stor.premiereEtape))
  }
  function consigneConclusion () {
    // On a cliqué sur le bouton demandant de conclure par le PGCD.
    // Or si on n’est pas encore à cette étape, on doit avoir une erreur
    // C’est le cas si le dernier reste est strictement séupérieur à 1
    if (stor.algo.diviseur <= 1) {
      // on peut conclure
      stor.validationPGCD = true
      j3pEmpty(stor.zoneCons6)
      const elt = j3pAffiche(stor.zoneCons8, '', ds.textes.consigne5, {
        inputmq1: {
          texte: '',
          dynamique: true
        },
        n1: stor.objCons.n1,
        n2: stor.objCons.n2
      })
      stor.zoneInput = elt.inputmqList[0]
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      stor.zoneInput.reponse = [stor.objCons.r]
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: [stor.zoneInput.id]
      })
      mqRestriction(stor.zoneInput, '\\d', {
        boundingContainer: me.zonesElts.MG
      })
      j3pEmpty(stor.zoneCons7)
      j3pAjouteBouton(stor.zoneCons7, afficheLigne, {
        id: stor.btnCcl,
        value: ds.textes.consigne4_2,
        className: 'MepBoutons'
      })
      j3pFocus(stor.zoneInput)
    } else {
      // Là il n’aurait pas dû cliquer
      stor.zoneCorr.style.color = me.styles.cfaux
      stor.zoneCorr.style.color = me.styles.cfaux
      stor.zoneCorr.innerHTML = cFaux
      stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment3
      j3pEmpty(stor.zoneCons7)
      if (me.essaiCourant >= ds.nbchances) {
        // C’est comme si on avait cliqué sur "valider"
        j3pEmpty(stor.zoneCons6)
        me._stopTimer()
        me.cacheBoutonValider()
        stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
        // Là il ne peut plus se corriger. On lui affiche alors la solution
        afficheCorrection(false)
        me.typederreurs[2]++
        me.etat = 'navigation'
        me.sectionCourante()
      } else {
        stor.nbTentatives--
        afficheNbTentatives(stor.nbTentatives)
        j3pFocus(stor.zoneInput, true)
      }
    }
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 2,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 3,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.
      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'PGCD à l’aide de l’algorithme d’Euclide',
        // on donne les phrases de la consigne
        consigne1: 'On souhaite déterminer le PGCD de £{n1} et £{n2} à l’aide de l’algorithme d’Euclide.',
        consigne2: 'Écrire les étapes successives de cet algorithme.',
        consigne3: 'Si cette dernière égalité permet de donner le PGCD alors cliquer sur le bouton, sinon poursuivre l’algorithme ci-dessous.',
        consigne4_1: 'Je peux conclure par le PGCD',
        consigne4_2: 'Retour à l’algorithme',
        consigne5: 'Le PGCD de £{n1} et £{n2} vaut &1&.',
        nbEssais_1: '£e erreurs sont possibles.',
        nbEssais_2: 'Une erreur est possible.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Même si l’égalité est vraie, elle ne correspond pas à une étape de l’algorithme d’Euclide.',
        comment2: 'À chaque étape de l’algorithme, on doit avoir une égalité',
        comment3: 'À cette étape de l’algorithme, on ne peut pas encore conclure&nbsp;!',
        comment4: 'Tu n’as pas choisi la bonne valeur comme dividende&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'Voici les étapes successives de cet algorithme&nbsp;:',
        corr2: 'Le PGCD de £{n1} et £{n2} est le dernier reste non nul de cet algorithme, c’est donc £r.'
      },
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    // Construction de la page

    // Le paramètre définit la largeur relative de la première colonne
    // j’ai choisi de mettre ce pourcentage en paramètre pour centrer la figure par la suite

    me.construitStructurePage({ structure: ds.structure })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    stor.numEssai = 1
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    stor.objCons = genereDonnees()
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 8; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, stor.objCons)
    j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2)
    stor.btnCcl = j3pGetNewId('btnCcl')
    stor.nbTentatives = ds.nbchances
    afficheNbTentatives(stor.nbTentatives)
    // Dans stor.zoneCons4, on met toutes les étapes déjà effectuées
    stor.premiereEtape = true
    stor.algo = {}
    stor.algo.dividende = Math.max(stor.objCons.n1, stor.objCons.n2)
    stor.algo.diviseur = Math.min(stor.objCons.n1, stor.objCons.n2)
    genereZone('')
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.zoneD, 'div', '')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]//, left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '')

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        const bonneEtape = [false, false, false]
        let etapeAlgo = false
        let diviseurPresent = false
        let egalPresent, r
        if (reponse.aRepondu && !stor.validationPGCD) {
        // On doit valider une étape de l’algo d’euclide
          etapeAlgo = true
          // On vérifie qu’il y a une égalité
          me.repSaisie = stor.fctsValid.zones.reponseSaisie[0]
          const tabEgalite = me.repSaisie.split('=')
          egalPresent = (tabEgalite.length === 2)
          if (egalPresent) {
            const decomp = (tabEgalite[0] === String(stor.algo.dividende))
              ? tabEgalite[0]
              : (tabEgalite[1] === String(stor.algo.dividende)) ? tabEgalite[1] : ''
            let leCalcul
            // decom est alors l’expression b*q+r ou bien vide si le dividende n’est pas présent
            if (decomp === '') {
            // On regarde s’il n’a pas mis le diviseur à la lace du dividende
              diviseurPresent = (tabEgalite[0] === String(stor.algo.diviseur)) || (tabEgalite[1] === String(stor.algo.dividende))
              reponse.bonneReponse = false
              reponse.aRepondu = true
              stor.fctsValid.zones.bonneReponse[0] = false
            } else {
              leCalcul = (tabEgalite[0] === String(stor.algo.dividende)) ? j3pMathquillXcas(tabEgalite[1]) : j3pMathquillXcas(tabEgalite[0])
              bonneEtape[1] = Math.abs(stor.algo.dividende - j3pCalculValeur(leCalcul)) < 1e-12
              if (bonneEtape[1]) {
              // Il me reste à vérifier si leCalcul correspond bien à b*q+r
                const q = Math.floor(stor.algo.dividende / stor.algo.diviseur)
                r = stor.algo.dividende % stor.algo.diviseur
                // On accepte dans l’algo qu’il mette a=b*q+0
                bonneEtape[2] = (leCalcul === stor.algo.diviseur + '*' + q + '+' + r) || (leCalcul === r + '+' + stor.algo.diviseur + '*' + q)
                bonneEtape[2] = bonneEtape[2] || (leCalcul === q + '*' + stor.algo.diviseur + '+' + r) || (leCalcul === r + '+' + q + '*' + stor.algo.diviseur)
                if (r === 0) bonneEtape[2] = bonneEtape[2] || (leCalcul === stor.algo.diviseur + '*' + q) || (leCalcul === q + '*' + stor.algo.diviseur)
                if (q === 1) bonneEtape[2] = bonneEtape[2] || (leCalcul === stor.algo.diviseur + '+' + r) || (leCalcul === r + '+' + stor.algo.diviseur)
              // console.log('leCalcul:', leCalcul, q, r)
              }
              bonneEtape[0] = bonneEtape[1] && bonneEtape[2]
              if (bonneEtape[0]) {
                reponse.aRepondu = false
                stor.algo.dividende = stor.algo.diviseur
                stor.algo.diviseur = r
              }
              reponse.bonneReponse = bonneEtape[0]
              if (!reponse.bonneReponse) stor.fctsValid.zones.bonneReponse[0] = false
              me.logIfDebug('bonneEtape:', bonneEtape)
            }
          } else {
            reponse.bonneReponse = false
            reponse.aRepondu = true
            stor.fctsValid.zones.bonneReponse[0] = false
          }
          if (reponse.bonneReponse) {
            me.essaiCourant--
          } else {
            stor.nbTentatives--
            afficheNbTentatives(stor.nbTentatives)
          }
          stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          if (etapeAlgo) {
            if (bonneEtape[0]) {
              stor.zoneCorr.style.color = me.styles.cbien
              stor.zoneCorr.innerHTML = cBien
              genereZone(me.repSaisie)
            } else {
              stor.zoneCorr.style.color = me.styles.cfaux
              stor.numEssai++
              if (stor.numEssai > ds.nbchances) {
                reponse.aRepondu = false
                reponse.bonneReponse = false
              }
            }
          } else {
            me.reponseManquante(stor.zoneCorr)
          }
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              stor.numEssai++
              if (!stor.validationPGCD) {
                if (!egalPresent) stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment2
                if (bonneEtape[1]) stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
                if (diviseurPresent) stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment4
                stor.fctsValid.zones.bonneReponse[0] = false
              }
              const tryAgain = ((stor.validationPGCD && (me.essaiCourant < ds.nbchances)) || (!stor.validationPGCD && stor.numEssai <= ds.nbchances))
              if (tryAgain) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
