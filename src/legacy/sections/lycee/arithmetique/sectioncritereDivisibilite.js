import { j3pAddElt, j3pAjouteCaseCoche, j3pBarre, j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomInt, j3pPGCD, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mai 2020
        Dans une liste de nombres, on sélectionne ceux qui sont divisibles par 2, 3, ... (ou 6, 18, ...)
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['listeDiviseurs', [2, 3, 5, 9], 'array', 'Liste des nombres (parmi 2, 3, 4, 5, 9, 10 et 11) dont on cherche les multiples.'],
    ['avecProduit', false, 'boolean', 'Cet exercice peut être décliné avec la recherche de multiples du produit de 2 éléments premiers entre eux de listeDiviseurs.']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titreExo: 'Critères de divisibilité',
  // on donne les phrases de la consigne
  consigne1: 'Parmi les nombres suivants, sélectionner tous ceux qui sont divisibles par £n.',
  rmq: 'Attention car il n’y a qu’un essai possible pour répondre.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: '',
  // et les phrases utiles pour les explications de la réponse
  corr1_2: 'Un nombre est divisible par 2 (pair) si le chiffre des unités vaut 0 ; 2 ; 4 ; 6 ou 8.',
  corr1_3: 'Un nombre est divisible par 3 si la somme de ses chiffres est divisible par 3.',
  corr1_4: 'Un nombre est divisible par 4 si ses deux chiffres de droites forment un nombre divisible par 4.',
  corr1_5: 'Un nombre est divisible par 5 si le chiffre des unités vaut 0 ou 5',
  corr1_9: 'Un nombre est divisible par 9 si la somme de ses chiffres est divisible par 9.',
  corr1_10: 'Un nombre est divisible par 10 si le chiffre des unités vaut 10.',
  corr1_11: 'Un nombre est divisible par 11 si la différence entre la somme de ses chiffres de rangs pairs et la somme de ses chiffres de rangs impairs est divisible par 11.',
  corr2: '$£{n3}=£{n2}\\times £{n1}$ où £{n1} et £{n2} n’ont pas de diviseur commun autre que 1 et $-1$. Un nombre est alors divisible par £{n3} lorsqu’il est divisible par £{n1} et par £{n2}.',
  corr3: 'Par exemple pour £n, la somme des chiffres de rangs pairs vaut $£{s1}$, celle des chiffres de rangs impairs vaut $£{s2}$ et la différence vaut £d qui est bien un multiple de 11, donc £n est divisible par 11.',
  corr4: 'Les nombres divisibles par £n de la liste sont en bleu.'
}
/**
 * section critereDivisibilite
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    if (ds.avecProduit) {
      j3pAffiche(stor.zoneExpli1, '', textes.corr2, {
        n1: stor.Diviseur[0],
        n2: stor.Diviseur[1],
        n3: stor.Diviseur[2]
      })
    } else {
      j3pAffiche(stor.zoneExpli1, '', textes['corr1_' + stor.leDiviseur])
      if (stor.leDiviseur === 11) {
        // On cherche le premier nombre de la liste qui soit un diviseur de 11
        let nb
        for (let i = 0; i < stor.tabChoix.length; i++) {
          if (Math.abs(Math.round(stor.tabChoix[i] / 11) - stor.tabChoix[i] / 11) < Math.pow(10, -12)) {
            nb = stor.tabChoix[i]
            break
          }
        }
        // On prend les chiffres de nb de rang pair
        let sommePaire = String(nb)[String(nb).length - 1]
        let laSommePaire = Number(String(nb)[String(nb).length - 1])
        for (let i = String(nb).length - 3; i > -1; i -= 2) {
          sommePaire = String(nb)[i] + '+' + sommePaire
          laSommePaire += Number(String(nb)[i])
        }
        // On prend les chiffres de nb de rang impair
        let sommeImpaire = String(nb)[String(nb).length - 2]
        let laSommeImpaire = Number(String(nb)[String(nb).length - 2])
        for (let i = String(nb).length - 4; i > -1; i -= 2) {
          sommeImpaire = String(nb)[i] + '+' + sommeImpaire
          laSommeImpaire += Number(String(nb)[i])
        }
        j3pAffiche(stor.zoneExpli2, '', textes.corr3, {
          n: nb,
          s1: sommePaire + '=' + laSommePaire,
          s2: sommeImpaire + '=' + laSommeImpaire,
          d: Math.abs(laSommePaire - laSommeImpaire)
        })
      }
    }
    j3pAffiche(stor.zoneExpli3, '', textes.corr4, { n: stor.leDiviseur })
  }
  /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par textes.consigne1
  */

  function enonceInitFirst () {
    ds = me.donneesSection

    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

    me.afficheTitre(textes.titreExo)
    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    // On fait le tri dans la liste proposée pour qu’il n’y ait pas d’autres valeurs que 2, 3, 5, 9, 10 et 11
    stor.tabDiviseurs = []
    ds.listeDiviseurs.forEach(function (x) {
      if ([2, 3, 4, 5, 9, 10, 11].includes(Number(x))) stor.tabDiviseurs.push(Number(x))
    })
    let pioche
    stor.choixDiviseurs = []
    if (ds.avecProduit) {
      // Je génère la liste des produits autorisés
      if (stor.tabDiviseurs.length <= 1) {
        console.error('Il faut au moins deux nombres dans le tableau listeDiviseurs pour pouvoir faire un produit. JE le remets à sa valeur par défaut [2, 3, 5, 9]')
        stor.tabDiviseurs = [2, 3, 5, 9]
      }
      stor.produitDiviseurs = []
      while (stor.produitDiviseurs.length === 0) {
        for (let i = 0; i < stor.tabDiviseurs.length - 1; i++) {
          for (let j = i + 1; j < stor.tabDiviseurs.length; j++) {
            if (j3pPGCD(stor.tabDiviseurs[i], stor.tabDiviseurs[j]) === 1 && (stor.tabDiviseurs[i] * stor.tabDiviseurs[j] !== 10)) stor.produitDiviseurs.push([stor.tabDiviseurs[i], stor.tabDiviseurs[j], stor.tabDiviseurs[i] * stor.tabDiviseurs[j]])
          }
        }
        if (stor.produitDiviseurs.length === 0) {
          console.warn('Les facteurs proposés ne permettent pas d’avoir un produit intéressant (on retombe sur un critère de divisibilité classique)')
          stor.tabDiviseurs = [2, 3, 5, 9]
        }
      }
      // Pour chaque répétitions, je prépare mes choix
      let copieProduit = [...stor.produitDiviseurs]
      for (let i = 0; i < ds.nbrepetitions; i++) {
        pioche = j3pGetRandomInt(0, copieProduit.length - 1)
        // stor.choixDiviseurs.push(copieProduit[pioche]) copieProduit[pioche] va être modifié... ce qui va altérer aussi celui qui est dans stor.choixDiviseurs !
        stor.choixDiviseurs.push(copieProduit[pioche].slice()) // là on crée un nouveau tableau indépendant de copieProduit[pioche]
        copieProduit.splice(pioche, 1)
        if (copieProduit.length === 0) copieProduit = [...stor.produitDiviseurs]
      }
    } else {
      // Pour chaque répétitions, je prépare mes choix
      let copieDiviseurs = [...stor.tabDiviseurs]
      for (let i = 0; i < ds.nbrepetitions; i++) {
        pioche = j3pGetRandomInt(0, copieDiviseurs.length - 1)
        stor.choixDiviseurs.push(copieDiviseurs[pioche])
        copieDiviseurs.splice(pioche, 1)
        if (copieDiviseurs.length === 0) copieDiviseurs = [...stor.tabDiviseurs]
      }
    }
    // console.log('stor.choixDiviseurs:', stor.choixDiviseurs)
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    stor.Diviseur = stor.choixDiviseurs[me.questionCourante - 1] // c’est un nombre si avecProduit vaut false et un tableau sinon
    stor.leDiviseur = (ds.avecProduit) ? stor.Diviseur[2] : stor.Diviseur
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, { n: stor.leDiviseur })
    // Il faut que je choisisse des nombres
    const tabMult = []
    const tabNonMult = []
    const borneInf = Math.floor(120 / stor.leDiviseur)
    for (let i = 0; i < 10; i++) {
      let choix1, choix2
      do {
        choix1 = stor.leDiviseur * j3pGetRandomInt(borneInf, borneInf + 200)
      } while (tabMult.includes(choix1))
      tabMult.push(choix1)
      do {
        choix2 = stor.leDiviseur * j3pGetRandomInt(borneInf, borneInf + 200) + j3pGetRandomInt(1, stor.leDiviseur - 1)
      } while (tabMult.includes(choix2))
      tabNonMult.push(choix2)
    }
    let nbMult = 0
    let nbNonMult = 0
    let pioche
    let tabMultCopie = [...tabMult]
    let tabNonMultCopie = [...tabNonMult]
    const nbChoix = (ds.avecProduit) ? j3pGetRandomInt(4, 5) : j3pGetRandomInt(6, 7)
    const nbMin = (ds.avecProduit) ? 1 : 3
    let produitUnFacteur = false
    do {
      stor.tabChoix = []
      for (let i = 1; i <= nbChoix; i++) {
        const alea = j3pGetRandomBool()
        if (alea) {
          pioche = j3pGetRandomInt(0, tabMultCopie.length - 1)
          stor.tabChoix.push(tabMultCopie[pioche])
          tabMultCopie.splice(pioche, 1)
          nbMult++
        } else {
          if (ds.avecProduit && !produitUnFacteur) {
            // Dans le cas d’un diviseur, produit de deux nombres de listeDiviseur, il faut que je mettre un nombre dans la liste qui soit divisible par l’un des deux facteurs
            let multiple
            do {
              const choixDiv = j3pGetRandomElt([stor.Diviseur[0], stor.Diviseur[1]])
              multiple = choixDiv * j3pGetRandomInt(Math.floor(120 / choixDiv), Math.floor(120 / choixDiv) + 200)
            } while (Math.abs(multiple / stor.leDiviseur - Math.round(multiple / stor.leDiviseur)) < Math.pow(10, -12))
            stor.tabChoix.push(multiple)
            produitUnFacteur = true
          } else {
            pioche = j3pGetRandomInt(0, tabNonMultCopie.length - 1)
            stor.tabChoix.push(tabNonMultCopie[pioche])
            tabNonMultCopie.splice(pioche, 1)
          }
          nbNonMult++
        }
      }
      tabMultCopie = [...tabMult]
      tabNonMultCopie = [...tabNonMult]
    } while (nbMult < nbMin || nbNonMult < nbMin)
    stor.tabChoix.sort(function (a, b) { return a - b })
    stor.nomInput = []
    stor.bonnesCases = []
    for (let i = 1; i <= stor.tabChoix.length; i++) {
      stor['choix' + i] = j3pAddElt(stor.zoneCons2, 'div', '')
      stor.nomInput.push(j3pAjouteCaseCoche(stor['choix' + i], {
        label: String(stor.tabChoix[i - 1])
      }))
      if (tabMult.indexOf(stor.tabChoix[i - 1]) > -1) stor.bonnesCases.push(i - 1)
    }
    // console.log('stor.bonnesCases:', stor.bonnesCases)
    if (ds.nbchances === 1) {
      stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div', '')
      j3pAffiche(stor.zoneCons3, '', textes.rmq, { style: { fontSize: '0.9em', fontStyle: 'italic' } })
    }
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const reponse = { aRepondu: false, bonneReponse: true }
        const reponseEleve = []
        for (let k = 0; k < stor.tabChoix.length; k++) {
          reponseEleve.push(stor.nomInput[k].checked)
          if (reponseEleve[k]) reponse.aRepondu = true
        }
        if (reponse.aRepondu) {
          for (let k = 0; k < stor.tabChoix.length; k++) {
            reponse.bonneReponse = reponse.bonneReponse && ((stor.bonnesCases.indexOf(k) > -1) ? reponseEleve[k] : !reponseEleve[k])
          }
          if (!reponse.bonneReponse && (me.essaiCourant === ds.nbchances)) {
          // Quand je suis à la dernière possibilité et qu’il a faux, je barre les mauvaises réponses et les mets en rouge
            for (let k = 0; k < stor.tabChoix.length; k++) {
              if ((stor.bonnesCases.indexOf(k) === -1) && reponseEleve[k]) {
                stor.nomInput[k].label.style.color = me.styles.cfaux
                j3pBarre(stor.nomInput[k].label)
              }
              if (stor.bonnesCases.indexOf(k) > -1) {
              // On la met dans la couleur de la correction
                stor.nomInput[k].label.style.color = me.styles.toutpetit.correction.color
              }
            }
          }
          if (me.essaiCourant === ds.nbchances || reponse.bonneReponse) {
            for (let k = 0; k < stor.tabChoix.length; k++) j3pDesactive(stor.nomInput[k])
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            for (let k = 0; k < stor.tabChoix.length; k++) {
              if (stor.bonnesCases.indexOf(k) > -1) {
              // On la met dans la couleur de la correction
                stor.nomInput[k].label.style.color = me.styles.cbien
              }
            }
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
