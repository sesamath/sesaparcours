import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mai 2020
        Dans cette section, on demande de déterminer le reste dans la division euclidienne d’un dividende par un diviseur
        Plusieurs types de questions peuvent être posés :
        - avec la division euclidienne
        - à l’aide des critères de divisibilité pour 2, 3, 4, 5, 9 et 10
        - en partant de a=b*q+r avec q < b et r>=q et on demanderait ensuite le reste de a par q
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuest', 'critereDivisibilite', 'liste', 'On peut proposer cet exercice sous trois formes :<br/>-avec les critères de divisibilité (les diviseurs seront alors choisis);<br/>-avec l’utilisation de la division euclidienne;<br/>-avec une première égalité a=b*q+r mais qui ne donnera pas directement la réponse.', ['critereDivisibilite', 'DivEuclidienne', 'egaliteInitiale']],
    ['listeDiviseurs', [2, 3, 4, 5, 9, 10], 'array', 'Liste des diviseurs acceptés (parmi 2, 3, 4, 5, 9, 10) dans le cas où on choisit typeQuest=critereDivisibilité']
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    if (ds.typeQuest === 'egaliteInitiale') {
      j3pDetruit(stor.zoneCalc)
      j3pDetruitFenetres('Calculatrice')
    }
    if (ds.typeQuest === 'critereDivisibilite') {
      const laConsCorr = (stor.objCons.b === 2 || stor.objCons.b === 5)
        ? ds.textes.corr1_2
        : (stor.objCons.b === 3 || stor.objCons.b === 9)
            ? ds.textes.corr1_3
            : (stor.objCons.b === 4) ? ds.textes.corr1_4 : ds.textes.corr1_10
      const laConsCorr2 = (stor.objCons.b === 2 || stor.objCons.b === 5)
        ? ds.textes.corr2_2
        : (stor.objCons.b === 3 || stor.objCons.b === 9)
            ? ds.textes.corr2_3
            : (stor.objCons.b === 4) ? ds.textes.corr2_4 : ds.textes.corr2_10
      if (stor.objCons.b === 2 || stor.objCons.b === 5) {
        stor.objCons.u = stor.objCons.a % 10
        stor.objCons.e = '$' + stor.objCons.a + '=10\\times ' + Math.floor(stor.objCons.a / 10) + '+' + stor.objCons.u + '$'
      } else if (stor.objCons.b === 3 || stor.objCons.b === 9) {
        stor.objCons.s = 0
        for (let i = 0; i < String(stor.objCons.a).length; i++) {
          stor.objCons.s += Number(String(stor.objCons.a)[i])
        }
      } else if (stor.objCons.b === 4) {
        stor.objCons.u = stor.objCons.a % 100
        stor.objCons.e = '$' + stor.objCons.a + '=100\\times ' + Math.floor(stor.objCons.a / 100) + '+' + stor.objCons.u + '$'
      } else if (stor.objCons.b === 10) {
        stor.objCons.e = '$' + stor.objCons.a + '=10\\times ' + Math.floor(stor.objCons.a / 10) + '+' + stor.objCons.r + '$'
      }
      j3pAffiche(stor.zoneExpli1, '', laConsCorr, stor.objCons)
      j3pAffiche(stor.zoneExpli2, '', laConsCorr2, stor.objCons)
    } else if (ds.typeQuest === 'DivEuclidienne') {
      stor.objCons.e = stor.objCons.a + '=' + stor.objCons.b + '\\times ' + Math.floor(stor.objCons.a / stor.objCons.b) + '+' + stor.objCons.r
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr3, stor.objCons)
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr3_2, stor.objCons)
    } else {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr4, stor.objCons)
      stor.objCons.p = stor.objCons.q + '\\times ' + stor.objCons.b
      stor.objCons.e2 = stor.objCons.r2 + '=' + stor.objCons.b + '\\times ' + Math.floor(stor.objCons.r2 / stor.objCons.b) + '+' + stor.objCons.r
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr5, stor.objCons)
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr6, stor.objCons)
    }
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.
      typeQuest: 'critereDivisibilite',
      listeDiviseurs: [2, 3, 4, 5, 9],
      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Reste dans la division euclidienne',
        // on donne les phrases de la consigne
        consigne1: 'En utilisant le critère de divisibilité du nombre £b, on peut rapidement conclure que le reste dans la division euclidienne de £a par £b vaut &1&.',
        consigne2: 'En effectuant la division euclidienne de £a par £b, on peut en déduire que le reste obtenu dans cette division euclidienne vaut &1&.',
        consigne3: 'La division euclidienne de £a par £q donne $£d$.',
        consigne4: 'On peut en déduire que le reste dans la division euclidienne de £a par £b vaut &1&.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Attention, il y a un piège. Que doit vérifier le reste dans la division euclidienne d’un nombre par un autre&nbsp;?',
        // et les phrases utiles pour les explications de la réponse
        corr1_2: '10 est divisible par £b, or £e, ainsi le reste dans la division de £a par £b est égal au reste dans la division de l’unité par £b.',
        corr2_2: 'Or le reste dans la division de £u par £b vaut £r. Donc le reste dans la division euclidienne de £a par £b vaut £r.',
        corr1_3: 'La divisibilité d’un nombre par £b étant obtenue en faisant la somme des chiffres, il en va de même pour la recherche du reste dans la division euclidienne par £b&nbsp;: ce reste est égal au reste dans la division euclidienne de la somme des chiffres par £b.',
        corr2_3: 'Or la somme des chiffres vaut £s et le reste dans la division euclidienne de £s par £b vaut £r. Donc le reste dans la division euclidienne de £a par £b vaut £r.',
        corr1_4: '100 est divisible par 4, or £e, ainsi le reste dans la division de £a par 4 est égal au reste dans la division du nombre obtenu avec ses deux chiffres de droite par 4.',
        corr1_10: 'Comme £e, le reste dans la division de £a par £b est égal au chiffre des unités.',
        corr2_10: 'Donc ce reste vaut £r.',
        corr2_4: 'Or le reste dans la division de £u par 4 vaut £r. Donc le reste dans la division euclidienne de £a par 4 vaut £r.',
        corr3: 'En posant la division euclidienne de £a par £b, on obtient $£e$ avec $0\\leq £r<£b$.',
        corr3_2: 'Ainsi le reste dans la division euclidienne de £a par £b vaut £r.',
        corr4: 'L’égalité $£d$ ne nous permet pas de conclure car $£{r2}\\geq £b$.',
        corr5: 'Or $£p$ est divisible par £b et $£{e2}$ avec $0\\leq £r<£b$.',
        corr6: 'Donc £r est le reste dans la division euclidienne de £a par £b.'
      },
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    if (ds.typeQuest === 'critereDivisibilite') {
      // On fait le tri dans la liste proposée pour qu’il n’y ait pas d’autres valeurs que 2, 3, 4, 5, 9, 10
      stor.tabDiviseurs = []
      ds.listeDiviseurs.forEach(function (x) {
        if ([2, 3, 4, 5, 9, 10].indexOf(Number(x)) > -1) stor.tabDiviseurs.push(Number(x))
      })
      // Pour chaque répétitions, je prépare mes choix
      stor.choixDiviseurs = []
      let copieDiviseurs = [...stor.tabDiviseurs]
      for (let i = 0; i < ds.nbrepetitions; i++) {
        const pioche = j3pGetRandomInt(0, copieDiviseurs.length - 1)
        stor.choixDiviseurs.push(copieDiviseurs[pioche])
        copieDiviseurs.splice(pioche, 1)
        if (copieDiviseurs.length === 0) copieDiviseurs = [...stor.tabDiviseurs]
      }
    }
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    let laConsigne
    stor.objCons = {}
    if (ds.typeQuest === 'critereDivisibilite') {
      stor.objCons.b = stor.choixDiviseurs[me.questionCourante - 1]
      stor.objCons.r = j3pGetRandomInt(0, stor.objCons.b - 1)
      stor.objCons.a = stor.objCons.b * j3pGetRandomInt(1200, 2670) + stor.objCons.r
      laConsigne = ds.textes.consigne1
    } else if (ds.typeQuest === 'DivEuclidienne') {
      do {
        stor.objCons.b = j3pGetRandomInt(13, 29)
      } while (stor.objCons.b % 5 === 0)
      stor.objCons.r = j3pGetRandomInt(0, stor.objCons.b - 1)
      stor.objCons.a = stor.objCons.b * j3pGetRandomInt(75, 223) + stor.objCons.r
      laConsigne = ds.textes.consigne2
    } else {
      do {
        stor.objCons.b = j3pGetRandomInt(13, 29)
      } while (stor.objCons.b % 5 === 0)
      stor.objCons.q = j3pGetRandomInt(120, 267)
      stor.objCons.r2 = j3pGetRandomInt(2 * stor.objCons.b, stor.objCons.q - 3)
      stor.objCons.r = stor.objCons.r2 % stor.objCons.b
      stor.objCons.a = stor.objCons.b * stor.objCons.q + stor.objCons.r2
      stor.objCons.d = stor.objCons.a + '=' + stor.objCons.q + '\\times ' + stor.objCons.b + '+' + stor.objCons.r2
      laConsigne = ds.textes.consigne3
    }
    const elt1 = j3pAffiche(stor.zoneCons1, '', laConsigne, stor.objCons)
    if (ds.typeQuest === 'egaliteInitiale') {
      const elt2 = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne4, stor.objCons)
      stor.zoneInput = elt2.inputmqList[0]
    } else {
      stor.zoneInput = elt1.inputmqList[0]
    }
    mqRestriction(stor.zoneInput, '\\d+-')
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [stor.objCons.r]
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    if (ds.typeQuest !== 'critereDivisibilite') {
      stor.zoneCalc = j3pAddElt(stor.zoneD, 'div', '')
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]//, left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
      j3pAfficheCroixFenetres('Calculatrice')
    }
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '')
    j3pFocus(stor.zoneInput)
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        let pbReste = false
        if (!reponse.bonneReponse && ds.typeQuest === 'egaliteInitiale') {
        // Dans ce cas, le piège est de mettre la valeur du reste dans l’égalité proposée
          if (String(stor.fctsValid.zones.reponseSaisie[0]) === String(stor.objCons.r2)) {
            pbReste = true
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (pbReste) {
                stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
