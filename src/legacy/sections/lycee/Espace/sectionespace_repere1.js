import { j3pCompareTab, j3pShuffle, j3pFocus, j3pGetRandomInt, j3pRandomTab, j3pVirgule, j3pAddElt, j3pGetNewId } from 'src/legacy/core/functions'
import _3D, { j3pProduitMatVec } from 'src/legacy/outils/3D/3D'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
   Dév : Alexis Lecomte
   Date : Février-mars 2014
   Scénario : cube, coordonnées de points dans un repère.
   Amélioration possible : en faire une section qualitative pour tester l’inversion des coordonnées, à voir si pertinent en terme de remédiation
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['point_du_repere', false, 'boolean', 'Le point considéré est-il un point du repère (hors origine) ?'],
    ['repere_simple', false, 'boolean', 'Le repère choisi est-il classique ou non ?'],
    ['point_a_trouver', 'symetrique', 'string', 'milieu ou sommet ou symetrique : le point à chercher est-il un milieu d’une arête, un sommet du cube ou un symétrique (coordonnées -1 ou 2)'],
    ['figure_visible', true, 'boolean', 'Pour savoir si la figure est visible. Si non (paramètre à false), le parameter repers_simple sera forcé à true pour simplifier l’affichage de la figure en correction']
  ]
}

/**
 * section espace_repere1
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function constructionCube () {
    const coordSommets = [[], [1, -1, -1], [1, 1, -1], [-1, 1, -1], [-1, -1, -1], [1, -1, 1], [1, 1, 1], [-1, 1, 1], [-1, -1, 1]]

    const objet = { // on définit les éléments de la figure 3D
      dim: { larg: 400, haut: 400, zoom: stor.zoom },
      sommets: coordSommets,
      nomsommets: ['', stor.points[0], stor.points[1], stor.points[2], stor.points[3], stor.points[4], stor.points[5], stor.points[6], stor.points[7]],
      sommetsvisibles: ['', true, true, true, true, true, true, true, true],
      faces: [[4, 3, 2, 1], [1, 2, 6, 5], [2, 3, 7, 6], [3, 4, 8, 7], [4, 1, 5, 8], [5, 6, 7, 8]]
    }
    stor.solide1 = new _3D(stor.conteneurCube, objet)
    stor.solide1.rotation = { Ox: 0, Oy: 10, Oz: -30 }
    stor.solide1.arotation = stor.solide1.rotation
    stor.solide1.construit()
    if (!stor.point_du_cube) { // on doit rajouter le point
      stor.solide1.ajoute(
        [{ nom: stor.nom_points[4], type: 'point', coord: coordDansRepere([stor.solutions[0], stor.solutions[1], stor.solutions[2]], stor.points_correspondance), couleur: 'bleu' }
        ]
      )
    }
  }
  // retourne les coord de tab dans le repere designe par me.repere
  function coordDansRepere (tab, repere) {
    // exemple : coordDansRepere([1,0.5,1],[[],[],[],[]]); où repère contient les coordonnées des 4 points
    const O = repere[0]// objet coord de O
    const I = repere[1]
    const J = repere[2]
    const K = repere[3]
    const passage = [
      [I[0] - O[0], J[0] - O[0], K[0] - O[0]],
      [I[1] - O[1], J[1] - O[1], K[1] - O[1]],
      [I[2] - O[2], J[2] - O[2], K[2] - O[2]]
    ]
    let vecOM = [tab[0], tab[1], tab[2]]// les coordonnées de OM dans la nouvelles base sont celles de M
    vecOM = j3pProduitMatVec(passage, vecOM)
    return [vecOM[0] + O[0], vecOM[1] + O[1], vecOM[2] + O[2]]
  }
  // on affiche en couleur le repère du cube et d’une autre couleur le vecteur à décomposer
  function afficherDetailsCube () {
    if (!ds.figure_visible) constructionCube()
    let objTemp = []
    const tabCoords = []
    const tabObjets = []
    tabCoords[0] = ''; tabCoords[1] = [1, -1, -1]; tabCoords[2] = [1, 1, -1]; tabCoords[3] = [-1, 1, -1]; tabCoords[4] = [-1, -1, -1]; tabCoords[5] = [1, -1, 1]; tabCoords[6] = [1, 1, 1]; tabCoords[7] = [-1, 1, 1]; tabCoords[8] = [-1, -1, 1]
    // console.log("stor.nom_points="+stor.nom_points)
    for (let i = 0; i < 8; i++) {
      if (stor.nom_points.includes(stor.points[i])) {
        objTemp[i] = {}
        objTemp[i].nom = stor.points[i]
        objTemp[i].type = 'point'
        if (stor.points[i] === stor.nom_points[4]) { // point à trouver
          objTemp[i].couleur = 'rouge'
        } else {
          if (stor.points[i] === stor.nom_points[0]) { // origine
            objTemp[i].couleur = '#000'// noir marche pas
          } else {
            objTemp[i].couleur = 'bleu'
          }
        }
        objTemp[i].coord = tabCoords[i + 1]
        tabObjets.push(objTemp[i])
      }
    }
    // on rajoute maintenant la décomposition en somme de vecteurs :
    // on créé d’abord les extrémités des vecteurs, invisibles
    for (let i = 0; i < stor.decomposition.points.length; i++) {
      const extr1 = {}
      extr1.nom = stor.decomposition.points[i]
      extr1.visible = false
      extr1.type = 'point'
      extr1.coord = coordDansRepere(stor.decomposition.coordonnees[i], stor.points_correspondance)
      tabObjets.push(extr1)
    }
    // puis les vecteurs
    for (let i = 0; i < stor.decomposition.points.length - 1; i++) {
      objTemp = {}
      objTemp.nom = 'vec' + i
      objTemp.type = 'vecteur'
      objTemp.couleur = '#F00'// en rouge
      objTemp.visible = true
      objTemp.pointilles = true
      objTemp.ext1 = stor.decomposition.points[i]
      objTemp.ext2 = stor.decomposition.points[i + 1]
      tabObjets.push(objTemp)
    }
    if (stor.decomposition.points.length > 2) {
      const objTemp2 = {}
      objTemp2.nom = 'vec25'
      objTemp2.type = 'vecteur'
      objTemp2.visible = true
      objTemp2.pointilles = false
      objTemp2.ext1 = stor.decomposition.points[0]
      objTemp2.ext2 = stor.decomposition.points[stor.decomposition.points.length - 1]
      tabObjets.push(objTemp2)
    }
    stor.solide1.ajoute(
      tabObjets
    )
    for (let i = 0; i < 8; i++) {
      if (stor.nom_points.includes(stor.points[i])) stor.solide1.invisible(stor.points[i])
    }
  }

  // Test si l’élève a permuté des coordonnées
  function inversionCoordonnees () {
    const inversionCoordonnees1 = stor.fctValid.valideUneZone(stor.zoneInput[0].id, [stor.solutions[0]]) && stor.fctValid.valideUneZone(stor.zoneInput[1].id, [stor.solutions[2]]) && stor.fctValid.valideUneZone(stor.zoneInput[2].id, [stor.solutions[1]])
    const inversionCoordonnees2 = stor.fctValid.valideUneZone(stor.zoneInput[0].id, [stor.solutions[2]]) && stor.fctValid.valideUneZone(stor.zoneInput[1].id, [stor.solutions[1]]) && stor.fctValid.valideUneZone(stor.zoneInput[2].id, [stor.solutions[0]])
    const inversionCoordonnees3 = stor.fctValid.valideUneZone(stor.zoneInput[0].id, [stor.solutions[1]]) && stor.fctValid.valideUneZone(stor.zoneInput[1].id, [stor.solutions[0]]) && stor.fctValid.valideUneZone(stor.zoneInput[2].id, [stor.solutions[2]])
    const inversionCoordonnees4 = stor.fctValid.valideUneZone(stor.zoneInput[0].id, [stor.solutions[1]]) && stor.fctValid.valideUneZone(stor.zoneInput[1].id, [stor.solutions[2]]) && stor.fctValid.valideUneZone(stor.zoneInput[2].id, [stor.solutions[0]])
    const inversionCoordonnees5 = stor.fctValid.valideUneZone(stor.zoneInput[0].id, [stor.solutions[2]]) && stor.fctValid.valideUneZone(stor.zoneInput[1].id, [stor.solutions[0]]) && stor.fctValid.valideUneZone(stor.zoneInput[2].id, [stor.solutions[1]])
    return inversionCoordonnees1 || inversionCoordonnees2 || inversionCoordonnees3 || inversionCoordonnees4 || inversionCoordonnees5
  }

  function coordonnees (a) {
    // renvoie les coordonnées du point choisi dans le repère 3,0,2,7
    // ou celles du vecteur si l’entrée contient un tableau à deux dimensions'
    let b
    switch (a.length) {
      case 1:
        switch (a[0]) {
          case 0:
            b = [1, 0, 0]
            break

          case 1:
            b = [1, 1, 0]
            break

          case 2:
            b = [0, 1, 0]
            break

          case 3:
            b = [0, 0, 0]
            break

          case 4:
            b = [1, 0, 1]
            break

          case 5:
            b = [1, 1, 1]
            break

          case 6:
            b = [0, 1, 1]
            break

          case 7:
            b = [0, 0, 1]
            break
        }

        break

      case 2:
        {
          const c = coordonnees([a[0]])
          const d = coordonnees([a[1]])
          b = [d[0] - c[0], d[1] - c[1], d[2] - c[2]]
        }
        break
    }
    return b
  }
  function coord3D (nomsSommets, probaRepere) {
    // nomsSommets est un tableau de 8 éléments type string qui contient les noms des sommets
    // probaRepere est un tableau :[proba repère 1,proba repère 2,proba repère autre]
    // repere1=repère du fond, repere2=le préféré d’Alexis :) et autre
    // cette fonction renvoie un objet :
    // qui a les propriétés suivantes :
    // .repere : tableau des 4 sommets définissant le repère nommés par leur numéros
    // .nom_repere : tableau des 4 noms des sommets.
    // .centre : numéro du centre du repère
    // .nom_centre : nom du centre du repère
    // .abscisse, .ordonnee et .cote
    // et la méthode generer_point(choix), avec choix="tout_sauf_sommet" ou "tout_sauf_repere"
    // qui génère un tableau à 4 éléments :[nom du sommet dont on doit trouver les coordonnées,[abscisse,ordonnée,cote] de ce sommet]

    const choixDuRepere = j3pRandomTab(['repere1', 'repere2', 'autre'], probaRepere)

    let imagesDesSommets = []
    let nouveauRepere = []

    let z = 0
    let a, b
    switch (choixDuRepere) {
      case 'repere1':
        nouveauRepere = [3, 0, 2, 7]
        imagesDesSommets = [0, 1, 2, 3, 4, 5, 6, 7]
        z = 1
        break

      case 'repere2':
        nouveauRepere = [0, 3, 1, 4]
        imagesDesSommets = [3, 2, 1, 0, 7, 6, 5, 4]
        z = -1
        break

      case 'autre':
        a = []
        b = [3, 0, 2, 7]
        while (j3pCompareTab(b, [3, 0, 2, 7]) || j3pCompareTab(b, [0, 3, 1, 4])) {
          //  while (b.toSource() === [3, 0, 2, 7].toSource() || b.toSource() === [0, 3, 1, 4].toSource()) {
          z = j3pGetRandomInt(0, 1)
          z = Math.pow(-1, z)
          switch (z) {
            case 1:
              a = j3pShuffle([[0, 6], [2, 4], [5, 3], [7, 1]])
              break

            case -1:
              a = j3pShuffle([[6, 0], [4, 2], [3, 5], [1, 7]])
              break
          }
          imagesDesSommets = [a[0][0], a[3][1], a[1][0], a[2][1], a[1][1], a[2][0], a[0][1], a[3][0]]
          b = [imagesDesSommets[3], imagesDesSommets[0], imagesDesSommets[2], imagesDesSommets[7]]
        }
        nouveauRepere = b
        break
    }
    const nomNouveauRepere = [nomsSommets[nouveauRepere[0]], nomsSommets[nouveauRepere[1]], nomsSommets[nouveauRepere[2]], nomsSommets[nouveauRepere[3]]]
    return {
      // propriétés :
      repere: nouveauRepere,
      nom_repere: nomNouveauRepere,
      centre: nouveauRepere[0],
      nom_centre: nomNouveauRepere[0],
      abscisse: nomNouveauRepere[0] + nomNouveauRepere[1],
      ordonnee: nomNouveauRepere[0] + nomNouveauRepere[2],
      cote: nomNouveauRepere[0] + nomNouveauRepere[3],
      // méthode :
      generer_point: function (choix) {
        // choix="tout_sauf_centre" : on élimine le centre du repère dans les points dont on doit déterminer les coordonnées
        // ou "tout_sauf_repere" : on élimine dans ce cas les 4 points qui définissent le repère.
        let sommetDuCube, coordSommetChoisi, coordSommet, sommetChoisi
        switch (choix) { // "milieu_arete" ou "symetrique_sommet" ou "point_du_repere" ou "tout_sauf_repere"
          case 'milieu_arete':
            // console.log('MILIEU')
            sommetDuCube = false
            // on détermine les coordonnées :
            // milieu d’une arête mais on pourrait faire la moyenne des coordonnées de deux sommets...
            coordSommetChoisi = []
            coordSommet = []
            coordSommet.push('0.5')
            if (j3pGetRandomInt(0, 1) === 1) {
              coordSommet.push('0')
            } else {
              coordSommet.push('1')
            }
            if (j3pGetRandomInt(0, 1) === 1) {
              coordSommet.push('0')
            } else {
              coordSommet.push('1')
            }
            coordSommetChoisi = j3pShuffle(coordSommet)
            break
          case 'symetrique_sommet':
            sommetDuCube = false
            coordSommetChoisi = []
            coordSommet = []

            if (j3pGetRandomInt(0, 1) === 1) {
              coordSommet.push(2)
            } else {
              coordSommet.push(-1)
            }
            if (j3pGetRandomInt(0, 1) === 1) {
              coordSommet.push(0)
            } else {
              coordSommet.push(1)
            }
            if (j3pGetRandomInt(0, 1) === 1) {
              coordSommet.push(0)
            } else {
              coordSommet.push(1)
            }
            coordSommetChoisi = j3pShuffle(coordSommet)
            break
          case 'tout_sauf_centre':
            sommetChoisi = j3pRandomTab([0, 1, 2, 4, 5, 6, 7], [0.14, 0.14, 0.14, 0.14, 0.15, 0.15, 0.14])
            sommetDuCube = true
            break
          case 'tout_sauf_repere':
            sommetDuCube = true
            sommetChoisi = j3pRandomTab([1, 4, 5, 6], [0.25, 0.25, 0.25, 0.25])
            break
          default :// point_du_repere, sauf centre
            sommetDuCube = true
            sommetChoisi = j3pRandomTab([0, 2, 7], [0.33, 0.33, 0.34])
            break
        }
        let nomImageSommetChoisi
        if (sommetDuCube) {
          coordSommetChoisi = coordonnees([sommetChoisi])
          const imageSommetChoisi = imagesDesSommets[sommetChoisi]
          nomImageSommetChoisi = nomsSommets[imageSommetChoisi]
        } else {
          let lettre = String.fromCharCode(65 + j3pGetRandomInt(0, 25))
          while (stor.points.includes(lettre)) {
            lettre = String.fromCharCode(65 + j3pGetRandomInt(0, 25))
          }
          nomImageSommetChoisi = lettre
        }
        // on garde en mémoire si le point à trouver est un sommet du cube ou non
        stor.point_du_cube = sommetDuCube
        return [nomImageSommetChoisi, coordSommetChoisi]
      }
    }
  }

  function getDonnees () {
    return {
      typesection: 'lycee', //

      nbrepetitions: 3,
      nbetapes: 1,
      indication: 'Déplace le solide à la souris pour visualiser correctement',
      nbchances: 2,
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"
      point_a_trouver: 'symetrique', // "milieu"||"sommet"||"symetrique". le point à chercher est-il un milieu d’une arête, un sommet du cube ou un symétrique (coordonnées -1 ou 2)
      point_du_repere: false, // sauf origine
      repere_simple: true, // un des deux repères faciles ou non.
      figure_visible: true,
      textes: {
        titre_exo: 'Dans un cube...',
        phrase1: 'On considère le cube £a. ',
        phrase2: 'Dans le repère orthonormé $(£a,\\vecteur{£a£b},\\vecteur{£a£c},\\vecteur{£a£d})$, détermine les coordonnées du point £e&nbsp;:',
        phrase3: 'Dans le repère orthonormé $(£a,\\vecteur{£a£b},\\vecteur{£a£c},\\vecteur{£a£d})$, détermine les coordonnées du point £e&nbsp;: (ce point a été obtenu uniquement à l’aide de milieu ou de symétrique de sommet du cube)',
        correction1: 'Attention à la confusion entre les coordonnées !',
        explication1: 'On a $\\vecteur{£a£b}=$£c$\\vecteur{£a£d}+$£e$\\vecteur{£a£f}+$£g$\\vecteur{£a£h}$ donc les coordonnées du point £b sont $(£c;£e;£g)$.',
        indication2: 'Dessine la figure au brouillon...'
      },
      // limite: 10;
      pe: 0
    }
  }
  function enonceMain () {
    // Les sommets du cube
    const point = []
    // valeur initiale :
    const deb = 65 + j3pGetRandomInt(0, 15)
    for (let k = 0; k < 8; k++) point[k] = String.fromCharCode(deb + k)
    // pour mémoire, les noms des sommets du cube :
    stor.points = []
    for (let i = 0; i < point.length; i++) {
      stor.points[i] = point[i]
    }
    // tests code LaurentF
    let m
    if (ds.repere_simple) {
      if (j3pGetRandomInt(0, 1) === 0) {
        m = coord3D(point, [1, 0, 0])
      } else {
        m = coord3D(point, [0, 1, 0])
      }
    } else {
      m = coord3D(point, [0, 0, 1])
    }
    stor.zoom = 40
    let tab
    switch (ds.point_a_trouver) {
      case 'milieu':// le point à trouver est un milieu d’une arête
        tab = m.generer_point('milieu_arete')
        break
      case 'symetrique':// le point à trouver est un symétrique d’un sommet par rapport à un autre, on peut donc avoir -1 ou 2 dans une coordonnées
        tab = m.generer_point('symetrique_sommet')
        stor.zoom = 30
        break
      default:// le point à trouver est un sommet du cube
        if (ds.point_du_repere) {
          tab = m.generer_point('point_du_repere')
        } else {
          tab = m.generer_point('tout_sauf_repere')
        }
        break
    }
    // le nom du cube
    stor.nom_cube = point[0] + point[1] + point[2] + point[3] + point[4] + point[5] + point[6] + point[7]
    // les trois points du repère et le point demandé en fonction de la difficulté
    stor.nom_points = []
    stor.nom_points.push(m.nom_repere[0])// sommet
    stor.nom_points.push(m.nom_repere[1])// point1
    stor.nom_points.push(m.nom_repere[2])// point2
    stor.nom_points.push(m.nom_repere[3])// point3
    stor.nom_points.push(tab[0])// le nom du point à trouver
    stor.solutions = []
    stor.solutions.push(tab[1][0])
    stor.solutions.push(tab[1][1])
    stor.solutions.push(tab[1][2])
    // console.log("solutions="+tab[1][0]+","+tab[1][1]+","+tab[1][2]);
    stor.decomposition = {}// utile en correction pour le détail du chemin
    stor.decomposition.points = []
    stor.decomposition.coordonnees = []
    stor.decomposition.points.push(stor.nom_points[0])
    stor.decomposition.coordonnees.push([0, 0, 0])
    if (tab[1][0] !== 0) { // il y a une décomposition suivant la première coordonnée
      stor.decomposition.points.push('M1')// pour usage pour tracer le vecteur, le nom du point sera invisible
      stor.decomposition.coordonnees.push([tab[1][0], 0, 0])
    }
    if (tab[1][1] !== 0) { // il y a une décomposition suivant la seconde coordonnée
      stor.decomposition.points.push('M2')// pour usage pour tracer le vecteur, le nom du point sera invisible
      stor.decomposition.coordonnees.push([tab[1][0], tab[1][1], 0])
    }
    if (tab[1][2] !== 0) { // il y a une décomposition suivant la troisième coordonnée
      stor.decomposition.points.push(tab[0])// pour usage pour tracer le vecteur, le nom du point sera invisible
      stor.decomposition.coordonnees.push([tab[1][0], tab[1][1], tab[1][2]])
    }
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    for (let i = 1; i <= 3; i++)stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('toutpetit.explications', { paddingTop: '15px' }) })

    j3pAffiche(stor.zoneCons1, '', ds.textes.phrase1, { a: stor.nom_cube })
    if (stor.point_du_cube) {
      j3pAffiche(stor.zoneCons2, '', ds.textes.phrase2,
        {
          a: stor.nom_points[0],
          b: stor.nom_points[1],
          c: stor.nom_points[2],
          d: stor.nom_points[3],
          e: stor.nom_points[4]
        })
    } else {
      j3pAffiche(stor.zoneCons2, '', ds.textes.phrase3,
        {
          a: stor.nom_points[0],
          b: stor.nom_points[1],
          c: stor.nom_points[2],
          d: stor.nom_points[3],
          e: stor.nom_points[4]
        })
    }
    const elt = j3pAffiche(stor.zoneCons3, '', '£a(&1&;&2&;&3&)',
      {
        a: stor.nom_points[4],
        inputmq1: { dynamique: true, width: '20px', correction: j3pVirgule(tab[1][0]) + '' },
        inputmq2: { dynamique: true, width: '20px', correction: j3pVirgule(tab[1][1]) + '' },
        inputmq3: { dynamique: true, width: '20px', correction: j3pVirgule(tab[1][2]) + '' }
      }
    )
    stor.zoneInput = [...elt.inputmqList]
    stor.zoneInput.forEach((eltInput, index) => {
      mqRestriction(eltInput, '-\\d,./', { commandes: ['fraction'] })
      eltInput.typeReponse = ['nombre', 'exact']
      eltInput.reponse = tab[1][index]
    })
    j3pFocus(stor.zoneInput[0])
    // la visualisation du cube :
    const coordSommets = [[], [1, -1, -1], [1, 1, -1], [-1, 1, -1], [-1, -1, -1], [1, -1, 1], [1, 1, 1], [-1, 1, 1], [-1, -1, 1]]
    // correspondance des 4 sommets du repère
    stor.points_correspondance = []
    stor.points_correspondance[0] = coordSommets[point.indexOf(stor.nom_points[0]) + 1] // coordonnées de l’origine du repère dans le repère d’affichage
    stor.points_correspondance[1] = coordSommets[point.indexOf(stor.nom_points[1]) + 1] // coordonnées du repère dans le repère d’affichage
    stor.points_correspondance[2] = coordSommets[point.indexOf(stor.nom_points[2]) + 1] // coordonnées du repère dans le repère d’affichage
    stor.points_correspondance[3] = coordSommets[point.indexOf(stor.nom_points[3]) + 1] // coordonnées du repère dans le repère d’affichage
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.conteneurCube = j3pGetNewId('conteneurCube')
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { id: stor.conteneurCube, style: me.styles.etendre('moyen.enonce', { border: '1px solid #000', background: '#fff', padding: '20x' }) })
    if (ds.figure_visible) constructionCube()
    // le tableau contenant toutes les zones de saisie
    const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
    stor.fctValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.5 })
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.figure_visible === false) {
          ds.indication = ds.textes.indication2
          ds.repere_simple = true// paramètre forcé.
        }
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const reponse = stor.fctValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficherDetailsCube()
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              afficherDetailsCube()
              stor.zoneCorr.innerHTML += '<br>' + '<br>' + regardeCorrection
              j3pAffiche(stor.zoneExpli, '', ds.textes.explication1,
                {
                  a: stor.nom_points[0],
                  b: stor.nom_points[4],
                  c: j3pVirgule(stor.solutions[0]),
                  d: stor.nom_points[1],
                  e: j3pVirgule(stor.solutions[1]),
                  f: stor.nom_points[2],
                  g: j3pVirgule(stor.solutions[2]),
                  h: stor.nom_points[3]
                })
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              const permutation = inversionCoordonnees()

              if (this.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                if (permutation) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.correction1 + '<br>'
                }
                me.typederreurs[1]++
                stor.fctValid.redonneFocus()
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                if (permutation) {
                  me.typederreurs[3]++
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.correction1 + '<br>'
                } else {
                  stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                  me.typederreurs[2]++
                }
                afficherDetailsCube()
                j3pAffiche(stor.zoneExpli, '', ds.textes.explication1,
                  {
                    a: stor.nom_points[0],
                    b: stor.nom_points[4],
                    c: j3pVirgule(stor.solutions[0]),
                    d: stor.nom_points[1],
                    e: j3pVirgule(stor.solutions[1]),
                    f: stor.nom_points[2],
                    g: j3pVirgule(stor.solutions[2]),
                    h: stor.nom_points[3]
                  })
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
