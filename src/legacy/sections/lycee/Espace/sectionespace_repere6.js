import { j3pAddElt, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pAfficheSysteme } from 'src/legacy/core/functionsSysteme'
import { j3pEvalue } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
   Dév : Alexis Lecomte
   Date : mai 2014
   Scénario : représentation paramétrique d’une droite donnée, donner un ou deux point(s) ou vecteur(s) directeur(s) de la droite
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['type_question', ['point', 'vecteur'], 'array', 'Tableau de même longueur que le nombre de répétitions, permettant de déterminer si l’on demande un ou deux points de la droite ou bien un ou deux vecteurs directeurs de la droite'],
    ['nb_questions', 2, 'liste', 'Nombre de point(s) ou vecteur(s) directeur(s) demandé(s)', [1, 2]]
  ]

}

/**
 * section espace_repere6
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function validation (rep0, rep1, rep2, rep3, rep4, rep5) {
    const rep = []
    const point = []
    const point2 = []
    const t = []
    const t2 = []
    const vec = []
    const vec2 = []
    let test = true
    t[0] = t[1] = t[2] = 0
    t2[0] = t2[1] = t2[2] = 0
    rep[0] = j3pMathquillXcas(rep0)
    rep[1] = j3pMathquillXcas(rep1)
    rep[2] = j3pMathquillXcas(rep2)
    if (Number(ds.nb_questions) === 2) {
      rep[3] = j3pMathquillXcas(rep3)
      rep[4] = j3pMathquillXcas(rep4)
      rep[5] = j3pMathquillXcas(rep5)
    }
    const objReponse = {}
    objReponse.aRepondu = true
    objReponse.bon1 = false
    objReponse.bon2 = false
    // on vérifie que les réponses sont non vides :
    const startIndexRep = Number(ds.nb_questions) === 2 ? 5 : 2
    for (let i = startIndexRep; i > -1; i--) {
      if ((rep[i]) === '') {
        objReponse.aRepondu = false
        objReponse.focus_a_donner = stor.zoneInput[i]
      }
    }
    // validité dans le cas où on demande un ou deux points :
    if (ds.type_question[me.questionCourante - 1] === 'point') {
      for (let i = 0; i < 3; i++) {
        // coordonnée du point de l’élève:
        point[i] = j3pEvalue(rep[i])
        // on vérifie s’il est sur la droite :
        if (stor.coord_vecteur[i] !== 0) { // pour éviter la division par zéro
          t[i] = (point[i] - stor.coord[i]) / stor.coord_vecteur[i]
        }
        if (Number(ds.nb_questions) === 2) {
          // coordonnées d’un second point de l’élève:
          point2[i] = j3pEvalue(rep[i + 3])
          // on vérifie aussi s’il est sur la droite
          if (stor.coord_vecteur[i] !== 0) { // pour éviter la division par zéro
            t2[i] = (point2[i] - stor.coord[i]) / stor.coord_vecteur[i]
          }
        }
      }
      // si un des coeffs du vecteur directeur est nul, on ne pas comparer la valeur de t correspondante à l’équation, donc on lui donne arbitrairement une des deux autres valeurs par contre on doit tester si l’on a bien la même réponse (par ex z=2)
      for (let i = 0; i < 3; i++) {
        if (stor.coord_vecteur[i] === 0) { // Correctif, si les 2 autres valeurs étaient négatives, ça ne corrigeait rien...
          // t[i] = Math.max(t[0], t[1], t[2])
          t[i] = t[(i + 1) % 3]
          if (Number(ds.nb_questions) === 2) {
            // t2[i] = Math.max(t2[0], t2[1], t2[2])
            t2[i] = t2[(i + 1) % 3]
          }
          test = (test && (stor.coord[i] === point[i]))
        }
      }
      if (test && Math.abs(t[0] - t[1]) < 0.00001 && Math.abs(t[0] - t[2]) < 0.00001) {
        objReponse.bon1 = true
      }
      if (Number(ds.nb_questions) === 2) {
        if (test && Math.abs(t2[0] - t2[1]) < 0.00001 && Math.abs(t2[0] - t2[2]) < 0.00001) {
          objReponse.bon2 = true
        }
        objReponse.bon = objReponse.bon1 && objReponse.bon2
      } else {
        objReponse.bon = objReponse.bon1
      }
    } else { // on demandait un ou deux vecteurs directeurs
      for (let i = 0; i < 3; i++) {
        // coordonnées du premier vecteur directeur de l’élève
        vec[i] = j3pMathquillXcas(rep[i])
      }
      const diff1 = Math.abs(j3pEvalue(vec[0] + '*(' + stor.coord_vecteur[1] + ')') - j3pEvalue(vec[1] + '*(' + stor.coord_vecteur[0] + ')'))
      const diff2 = Math.abs(j3pEvalue(vec[1] + '*(' + stor.coord_vecteur[2] + ')') - j3pEvalue(vec[2] + '*(' + stor.coord_vecteur[1] + ')'))
      const diff3 = Math.abs(j3pEvalue(vec[0] + '*(' + stor.coord_vecteur[2] + ')') - j3pEvalue(vec[2] + '*(' + stor.coord_vecteur[0] + ')'))
      if (diff1 < 0.000001 && diff2 < 0.000001 && diff3 < 0.000001) {
        objReponse.bon1 = true
      }
      if (j3pEvalue(vec[0]) === 0 && j3pEvalue(vec[1]) === 0 && j3pEvalue(vec[2]) === 0) { // correctif si on met le vecteur nul
        objReponse.bon1 = false
      }
      if (Number(ds.nb_questions) === 2) {
        for (let i = 0; i < 3; i++) vec2[i] = rep[i + 3] // coordonnées du second vecteur directeur de l’élève
        const diff4 = Math.abs(j3pEvalue(vec2[0] + '*(' + stor.coord_vecteur[1] + ')') - j3pEvalue(vec2[1] + '*(' + stor.coord_vecteur[0] + ')'))
        const diff5 = Math.abs(j3pEvalue(vec2[1] + '*(' + stor.coord_vecteur[2] + ')') - j3pEvalue(vec2[2] + '*(' + stor.coord_vecteur[1] + ')'))
        const diff6 = Math.abs(j3pEvalue(vec2[0] + '*(' + stor.coord_vecteur[2] + ')') - j3pEvalue(vec2[2] + '*(' + stor.coord_vecteur[0] + ')'))
        if (diff4 < 0.000001 && diff5 < 0.000001 && diff6 < 0.000001) {
          objReponse.bon2 = true
        }
        if (j3pEvalue(vec2[0]) === 0 && j3pEvalue(vec2[1]) === 0 && j3pEvalue(vec2[2]) === 0) objReponse.bon2 = false
        // Mais je ne veux pas non plus accepter 2 fois le même vecteur
        if (Math.abs(j3pEvalue(vec2[0]) - j3pEvalue(vec[0])) < 1e-12 &&
          Math.abs(j3pEvalue(vec2[1]) - j3pEvalue(vec[1])) < 1e-12 &&
          Math.abs(j3pEvalue(vec2[2]) - j3pEvalue(vec[2])) < Math.pow(10, -12)) {
          objReponse.bon2 = false
        }
        objReponse.bon = objReponse.bon1 && objReponse.bon2
      } else {
        objReponse.bon = objReponse.bon1
      }
    }
    return objReponse
  }

  function correctionDetaillee (bonneRep) {
    const explications = j3pAddElt(stor.divExplication, 'div', '', { style: me.styles.etendre('toutpetit.explications', { paddingTop: '15px' }) })
    if (bonneRep) explications.style.color = me.styles.cbien
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(explications, 'div')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.correction1)
    j3pAfficheSysteme(stor.zoneExpli2, '', ds.textes.correction2, [['$x=x_A+\\alpha t$', '$y=y_A+\\beta t$', '$z=z_A+\\gamma t$']], { inputmq1: {}, inputmq2: {}, inputmq3: {} }, explications.style.color)

    if (ds.type_question[me.questionCourante - 1] === 'point') {
      j3pAffiche(stor.zoneExpli3, '', ds.textes.correction3, { a: stor.coord[0], b: stor.coord[1], c: stor.coord[2] })
      if (Number(ds.nb_questions) === 2) {
        j3pAffiche(stor.zoneExpli4, '', ds.textes.correction3bis, { a: stor.coord[0] + stor.coord_vecteur[0], b: stor.coord[1] + stor.coord_vecteur[1], c: stor.coord[2] + stor.coord_vecteur[2] })
      }
      j3pAffiche(stor.zoneExpli5, '', ds.textes.correction3ter)
    } else {
      j3pAffiche(stor.zoneExpli3, '', ds.textes.correction4, { a: stor.coord_vecteur[0], b: stor.coord_vecteur[1], c: stor.coord_vecteur[2] })
      if (Number(ds.nb_questions) === 2) {
        j3pAffiche(stor.zoneExpli4, '', ds.textes.correction4bis, { a: -stor.coord_vecteur[0], b: -stor.coord_vecteur[1], c: -stor.coord_vecteur[2] })
      } else {
        j3pAffiche(stor.zoneExpli5, '', ds.textes.correction4ter)
      }
    }
  }
  function getDonnees () {
    return {
      typesection: 'lycee', //

      nbrepetitions: 2,
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"
      type_question: ['point', 'point'],
      nb_questions: 2,
      textes: {
        titreExo: 'Elements caractéristiques d’une droite',
        phrase1: 'Dans un repère orthonormé de l’espace, on considère la droite $D$ dont une représentation paramétrique est :',
        phrase2: '‡1‡ avec $t\\in \\R$.<br>',
        phrase3_1: 'Donne les coordonnées d’un point de cette droite :<br>(&1&;&2&;&3&)',
        phrase3_2: 'Donne les coordonnées de deux points de cette droite :<br>(&1&;&2&;&3&) et (&4&;&5&;&6&)',
        phrase4_1: 'Donne les coordonnées d’un vecteur directeur de cette droite :<br>(&1&;&2&;&3&)',
        phrase4_2: 'Donne les coordonnées de deux vecteurs directeurs de cette droite :<br>(&1&;&2&;&3&) et (&4&;&5&;&6&)',
        indication3: 'Il y a au moins une zone de saisie incorrecte.',
        correction1: 'La représentation paramétrique d’une droite passant par le point $A(x_A;y_A;z_A)$ et de vecteur directeur $\\vec{u}(\\alpha;\\beta;\\gamma)$ est de la forme :',
        correction2: '‡1‡, où $t$ décrit $\\R$.',
        correction3: 'On lit ainsi directement dans la représentation paramétrique (£a;£b;£c) comme coordonnées d’un point de la droite (ce qui correspond à la valeur 0 pour le paramètre $t$).',
        correction3bis: 'En remplaçant $t$ par 1, on obtient un autre point de la droite, de coordonnnées (£a;£b;£c).',
        correction3ter: 'Remarque : on peut obtenir une infinité de points de la droite, en remplaçant $t$ par d’autres valeurs (à chaque valeur de $t$ correspond un point de la droite)',
        correction4: 'En utilisant les coefficients multipliant $t$ de la représentation paramétrique, on obtient comme vecteur directeur $\\vec{u}$(£a;£b;£c).',
        correction4bis: 'Pour obtenir un autre vecteur directeur, il suffit de prendre un vecteur colinéaire à $\\vec{u}$, par exemple son opposé : $-\\vec{u}$(£a;£b;£c).',
        correction4ter: 'Remarque : on peut obtenir une infinité de vecteurs directeurs de la droite, en prenant un vecteur colinéaire à $\\vec{u}$ (c’est-à-dire en prenant des coordonnées proportionnelles à celles de $\\vec{u}$).',

        correction5: 'Les coordonnées du point choisi sont cependant exactes.',
        correction6: 'Les coordonnées du vecteur directeur utilisé sont cependant exactes.'
      },
      pe: 0
    }
  }
  function enonceMain () {
  /// //////////////////////////////////
  /* LE CODE PRINCIPAL DE LA SECTION */
  /// //////////////////////////////////
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    stor.divEnonce = j3pAddElt(stor.conteneur, 'div')
    stor.divExplication = j3pAddElt(stor.conteneur, 'div')
    stor.coord = []
    // stor.coord2=[];
    stor.coord_vecteur = []

    // Déclarataion des DIV
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.divEnonce, 'div')

    // définition des variables :

    while (true) { // on évite les 3 coordonnées du vecteur nulles
      stor.coord[0] = j3pGetRandomInt(-5, 5)
      stor.coord[1] = j3pGetRandomInt(-5, 5)
      stor.coord[2] = j3pGetRandomInt(-5, 5)
      stor.coord_vecteur[0] = j3pGetRandomInt(-5, 5)
      stor.coord_vecteur[1] = j3pGetRandomInt(-5, 5)
      stor.coord_vecteur[2] = j3pGetRandomInt(-5, 5)
      if (!(stor.coord_vecteur[0] === 0 && stor.coord_vecteur[1] === 0 && stor.coord_vecteur[2] === 0)) {
        break
      }
    }
    const reponse = { x: '', y: '', z: '' }// on définit le système à afficher
    if (stor.coord[0] !== 0) reponse.x += stor.coord[0]
    if (stor.coord[1] !== 0) reponse.y += stor.coord[1]
    if (stor.coord[2] !== 0) reponse.z += stor.coord[2]
    if (stor.coord_vecteur[0] !== 0) {
      if (stor.coord_vecteur[0] > 0) {
        if (reponse.x !== '') reponse.x += '+'
        if (!(stor.coord_vecteur[0] === 1)) reponse.x += stor.coord_vecteur[0]
      } else {
        if (stor.coord_vecteur[0] === -1) reponse.x += '-'
        else reponse.x += stor.coord_vecteur[0]
      }
      reponse.x += 't'
    }
    if (stor.coord_vecteur[1] !== 0) {
      if (stor.coord_vecteur[1] > 0) {
        if (reponse.y !== '') reponse.y += '+'
        if (!(stor.coord_vecteur[1] === 1)) reponse.y += stor.coord_vecteur[1]
      } else {
        if (stor.coord_vecteur[1] === -1) reponse.y += '-'
        else reponse.y += stor.coord_vecteur[1]
      }
      reponse.y += 't'
    }
    if (stor.coord_vecteur[2] !== 0) {
      if (stor.coord_vecteur[2] > 0) {
        if (reponse.z !== '') reponse.z += '+'
        if (!(stor.coord_vecteur[2] === 1)) reponse.z += stor.coord_vecteur[2]
      } else {
        if (stor.coord_vecteur[2] === -1) reponse.z += '-'
        else reponse.z += stor.coord_vecteur[2]
      }
      reponse.z += 't'
    }
    // cas particuliers :
    if (stor.coord[0] === 0 && stor.coord_vecteur[0] === 0) reponse.x = 0
    if (stor.coord[1] === 0 && stor.coord_vecteur[1] === 0) reponse.y = 0
    if (stor.coord[2] === 0 && stor.coord_vecteur[2] === 0) reponse.z = 0
    const consigne1 = ds.textes.phrase1
    const consigne2 = ds.textes.phrase2
    j3pAffiche(stor.zoneCons1, '', consigne1)
    j3pAfficheSysteme(stor.zoneCons2, '', consigne2, [['$x=' + reponse.x + '$', '$y=' + reponse.y + '$', '$z=' + reponse.z + '$']], { a: stor.coord_vecteur[0], b: stor.coord_vecteur[1], c: stor.coord_vecteur[2] }, me.styles.toutpetit.enonce.color)

    let consigne3
    if (ds.type_question[me.questionCourante - 1] === 'point') {
      if (Number(ds.nb_questions) === 1) {
      // 1 point
        consigne3 = ds.textes.phrase3_1
      } else {
      // 2 points
        consigne3 = ds.textes.phrase3_2
      }
    } else {
      if (Number(ds.nb_questions) === 1) {
      // 1 vect dir
        consigne3 = ds.textes.phrase4_1
      } else {
      // 2 vect dir
        consigne3 = ds.textes.phrase4_2
      }
    }
    let elt
    if (Number(ds.nb_questions) === 1) {
      elt = j3pAffiche(stor.zoneCons3, '', consigne3, { inputmq1: {}, inputmq2: {}, inputmq3: {} })
    } else {
      elt = j3pAffiche(stor.zoneCons3, '', consigne3, { inputmq1: {}, inputmq2: {}, inputmq3: {}, inputmq4: {}, inputmq5: {}, inputmq6: {} })
    }
    stor.zoneInput = [...elt.inputmqList]
    stor.zoneInput.forEach(eltInput => mqRestriction(eltInput, 't/\\d-', { commandes: ['fraction'] }))
    j3pFocus(stor.zoneInput[0])
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '20px' }) })
    const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        // si ds.type_question ne contient qu’un élément alors que ds.nbrepetitions ne vaut pas 1 alors les autres questions seront de la même nature
        if (ds.type_question.length === 1 && ds.nbrepetitions !== 1) {
          for (let i = 1; i < ds.nbrepetitions; i++) ds.type_question.push(ds.type_question[0])
        }
        if (ds.nbrepetitions !== ds.type_question.length) j3pShowError(Error('La taille du tableau type_question doit être égale à nbrepetitions\nIci : nbrepetitions=' + ds.nbrepetitions + ' et longueur du tableau :' + ds.type_question.length))
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titreExo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      enonceMain()

      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      let repEleve1, repEleve2, repEleve3, rep
      if (Number(ds.nb_questions) === 1) {
        repEleve1 = j3pValeurde(stor.zoneInput[0])
        repEleve2 = j3pValeurde(stor.zoneInput[1])
        repEleve3 = j3pValeurde(stor.zoneInput[2])
        rep = validation(repEleve1, repEleve2, repEleve3)
      } else {
        repEleve1 = j3pValeurde(stor.zoneInput[0])
        repEleve2 = j3pValeurde(stor.zoneInput[1])
        repEleve3 = j3pValeurde(stor.zoneInput[2])
        const repEleve4 = j3pValeurde(stor.zoneInput[3])
        const repEleve5 = j3pValeurde(stor.zoneInput[4])
        const repEleve6 = j3pValeurde(stor.zoneInput[5])
        rep = validation(repEleve1, repEleve2, repEleve3, repEleve4, repEleve5, repEleve6)
      }

      if ((!rep.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        j3pFocus(rep.focus_a_donner)
        me.afficheBoutonValider()
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (rep.bon) {
          me._stopTimer()
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          me.typederreurs[0]++
          me.cacheBoutonValider()
          // on affiche la correction détaillée quand même
          correctionDetaillee(true)
          for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
            stor.fctsValid.zones.bonneReponse[i] = true
            stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
          }
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          for (let i = 0; i <= 2; i++) {
            stor.fctsValid.zones.bonneReponse[i] = rep.bon1
            stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
          }
          if (Number(ds.nb_questions) === 2) {
            for (let i = 3; i <= 5; i++) {
              stor.fctsValid.zones.bonneReponse[i] = rep.bon2
              stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
            }
          }
          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.cacheBoutonValider()
            correctionDetaillee(false)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux

            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + ds.textes.indication3
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              if (rep.juste === 'coord_point') {
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.correction5
              }
              if (rep.juste === 'coord_vecteur') {
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.correction6
              }
              correctionDetaillee(false)
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
