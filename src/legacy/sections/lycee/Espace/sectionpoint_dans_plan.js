import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pGetRandomElts, j3pElement, j3pGetNewId, j3pRemplace, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { creeTableauAleatoire, fonctionTab, tableauVrai, sommeMonomes, entierAlea, pgcdZarb } from 'src/legacy/outils/espace/fctsEspace'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Auteur Laurent Fréguin
    Date 29/10/2015
    Tester si un point est dans un plan d’équation cartésienne donné
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['proba_appartient', 0.5, 'reel', 'Probabilité d’appartenance du point au plan']

  ]

  /*
    OU
    // dans le cas d’une section QUALITATIVE
    "pe":[
        {"pe_1":"toto"},
        {"pe_2":"tata"},
        {"pe_3":"titi"}
    ]
    */
}

/**
 * section point_dans_plan
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function substitution (tabDesCoeffs, tabDesVariables, tabDesValeurs) {
    // cette fonction a pour but de remplacer x,y,z par les coordonnées d’un point DE FACON PROPRE
    // ex : 2x+y-z avec x=3,y=-2 et z=3 doit donner2*3+(-2)-(-3)
    // On prend l’expression obtenue après simplification via sommeMonomes
    let stock = sommeMonomes(tabDesCoeffs, tabDesVariables)
    // Puis on retire des tableaux la variable cte "" et le coeff associé
    const tabVarSansCte = []
    const tabCoeffsSansCte = []
    for (let k = 0; k < tabDesVariables.length; k++) {
      if (tabDesVariables[k] !== '') {
        tabVarSansCte.push(tabDesVariables[k])
        tabCoeffsSansCte.push(tabDesCoeffs[k])
      }
    }

    // On essaie à présent de remplacer chaque variable par la valeur numérique correspondante.

    let n

    for (let k = 0; k < tabVarSansCte.length; k++) {
      n = stock.indexOf(tabVarSansCte[k])
      if (n !== -1) {
        if (Number(tabCoeffsSansCte[k]) === 1 || Number(tabCoeffsSansCte[k]) === -1) {
          if (tabDesValeurs[k] < 0) {
            stock = j3pRemplace(stock, n, n + tabVarSansCte[k].length - 1, '(' + tabDesValeurs[k] + ')')
          } else {
            stock = j3pRemplace(stock, n, n + tabVarSansCte[k].length - 1, tabDesValeurs[k])
          }
          // stock=j3pRemplace(stock,n,n+tabDesVariables[k].length-1,tabDesValeurs[k])
        } else {
          if (tabDesValeurs[k] < 0) {
            stock = j3pRemplace(stock, n, n + tabVarSansCte[k].length - 1, '\\times(' + tabDesValeurs[k] + ')')
          } else {
            stock = j3pRemplace(stock, n, n + tabVarSansCte[k].length - 1, '\\times' + tabDesValeurs[k])
          }
        }
      }
    }
    return stock
  }

  function egal (a, b) {
    // Renvoie true si la différence entre a et b est inférieure ou égale à 10^-9
    return Math.abs(a - b) < Math.pow(10, -9)
  }

  function afficheCorrection (objSolution, exact) {
    // j3pAffiche("div_e2","span_e2_1",objet_textes_correction[tab_phrases[1]],{A:objet_reponse.point_reference.nom,styletexte:{}})
    if (exact) stor.zoneExpli.style.color = me.styles.cbien
    const D = objSolution.D.nom
    const coordD = objSolution.D.coord

    const coeffs = objSolution.coeffs
    const appartient = objSolution.appartient
    const eqD = sommeMonomes(coeffs, ['x_' + D, 'y_' + D, 'z_' + D, '']) + '='
    let remplace = substitution(coeffs, ['x', 'y', 'z', ''], coordD) + '=' + objSolution.remplace_D

    if (!appartient) {
      remplace = remplace + '\\neq 0'
    }
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(stor.zoneExpli, 'div') })
    j3pAffiche(stor.zoneExpli1, '', ds.textes.phrase_correction_generale, { a: eqD + remplace })
    if (appartient) {
      j3pAffiche(stor.zoneExpli2, '', ds.textes.phrase_correction_conclusion_appartient, { A: D })
    } else {
      j3pAffiche(stor.zoneExpli2, '', ds.textes.phrase_correction_conclusion_appartient_pas, { A: D })
    }
  }

  function getDonnees () {
    return {
      typesection: 'lycee', //

      nbrepetitions: 3,
      nbetapes: 1,
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      proba_appartient: 0.5,
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      textes: {
        titreExo: 'Le point est-il dans le plan ?',
        affichage_espace: 'Voir la figure',
        phrase_Enonce: 'Dans un repère de l’espace, on considère le point $£A(£a;£b;£c)$ et le plan $P$ d’équation $£d$.<br>Le point $£A$ appartient-il au plan $P$ ?',
        phrase_1: 'oui',
        phrase_2: 'non',
        phrase_correction_generale: '<br>On a   $£a$',
        phrase_correction_conclusion_appartient: '<br>Ainsi, le point $£A$ appartient au plan $P$.',
        phrase_correction_conclusion_appartient_pas: '<br>Ainsi, le point $£A$ n’appartient pas au plan $P$.'
      },

      pe: 0
    }
  }

  function entreBornes (x) {
    // renvoie true si abs(x)<=bornes_coordonnees
    return (Math.abs(x) <= stor.borneCoordonnees)
  }
  function enonceMain () {
    stor.borneCoordonnees = 5
    let appartient = true
    let xA, yA, zA, xB, yB, zB, xC, yC, zC, xD, yD, zD, xU, yU, zU, xV, yV, zV
    let a, b, c, d, coordEntreBornes
    stor.probas[me.questionCourante - 1] = true
    if (stor.probas[me.questionCourante - 1]) {
      do {
        xA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])

        xB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])

        xC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])

        xU = xB - xA
        yU = yB - yA
        zU = zB - zA
        xV = xC - xA
        yV = yC - yA
        zV = zC - zA

        const s = entierAlea(-5, 5, [0])
        const t = entierAlea(-5, 5, [0])

        xD = xA + s * xU + t * xV
        yD = yA + s * yU + t * yV
        zD = zA + s * zU + t * zV

        a = yU * zV - zU * yV
        b = zU * xV - xU * zV
        c = xU * yV - yU * xV
        d = -a * xA - b * yA - c * zA

        const toutesLesCoord = [xD, yD, zD, a, b, c, d]
        coordEntreBornes = tableauVrai(fonctionTab(entreBornes, toutesLesCoord))
      } while ((a === 0 && b === 0) || (a === 0 && c === 0) || (b === 0 && c === 0) || (!coordEntreBornes))
    } else {
      appartient = false
      do {
        xA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])

        xB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])

        xC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])

        xD = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yD = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zD = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])

        xU = xB - xA
        yU = yB - yA
        zU = zB - zA
        xV = xC - xA
        yV = yC - yA
        zV = zC - zA

        a = yU * zV - zU * yV
        b = zU * xV - xU * zV
        c = xU * yV - yU * xV
        d = -a * xA - b * yA - c * zA
        const toutesLesCoord = [a, b, c, d]
        coordEntreBornes = tableauVrai(fonctionTab(entreBornes, toutesLesCoord))
      }
      while ((a === 0 && b === 0) || (a === 0 && c === 0) || (b === 0 && c === 0) || egal(a * xD + b * yD + c * zD + d, 0) || (!coordEntreBornes))
    }

    // Simplification éventuelle de a, b,c et d
    const g = pgcdZarb([a, b, c, d])
    a = a / g
    b = b / g
    c = c / g
    d = d / g

    const tabDesChoix = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'L', 'M', 'N', 'R']
    const mesPoints = j3pGetRandomElts(tabDesChoix, 4)
    const eqDeP = sommeMonomes([a, b, c, d], ['x', 'y', 'z', '']) + '=0'

    const A = {}
    A.nom = mesPoints[0]
    A.coord = [xA, yA, zA]

    const B = {}
    B.nom = mesPoints[1]
    B.coord = [xB, yB, zB]

    const C = {}
    C.nom = mesPoints[2]
    C.coord = [xC, yC, zC]

    const D = {}
    D.nom = mesPoints[3]
    D.coord = [xD, yD, zD]

    const u = [xB - xA, yB - yA, zB - zA]

    const v = [xC - xA, yC - yA, zC - zA]

    stor.solution = {}
    stor.solution.A = A
    stor.solution.D = D
    stor.solution.coeffs = [a, b, c, d]

    stor.solution.a = a
    stor.solution.b = b
    stor.solution.c = c
    stor.solution.d = d

    stor.solution.u = u
    stor.solution.v = v

    stor.solution.appartient = appartient
    stor.solution.remplace_D = a * xD + b * yD + c * zD + d

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '15px' }) })
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.phrase_Enonce, { A: D.nom, a: xD, b: yD, c: zD, d: eqDeP })

    j3pStyle(stor.zoneCons2, { paddingTop: '20px' })
    // tentative bouton radio
    stor.idsRadio = []
    stor.nameRadio = j3pGetNewId('choix')
    ;[1, 2].forEach(i => {
      stor.idsRadio.push(j3pGetNewId('radio' + i))
      j3pBoutonRadio(stor.zoneCons2, stor.idsRadio[i - 1], stor.nameRadio, 0, ds.textes['phrase_' + i])
    })

    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.toutpetit.explications })

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':

      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        if (ds.nbrepetitions > 1) {
          stor.probas = creeTableauAleatoire([true, false], [ds.proba_appartient, 1 - ds.proba_appartient], ds.nbrepetitions)
        }

        // Construction de la page
        me.construitStructurePage(ds.structure)

        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titreExo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'correction':
      {
        let exact = true
        const objSol = stor.solution

        // On teste si une réponse a été saisie
        let aReponduQcm = false
        // if (j3pElement("bill1").checked||j3pElement("bill2").checked){
        if (j3pElement(stor.idsRadio[0]).checked || j3pElement(stor.idsRadio[1]).checked) {
          aReponduQcm = true
        }

        if (!aReponduQcm) {
          me.reponseManquante(stor.zoneCorr)
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie=

          if (j3pElement(stor.idsRadio[0]).checked === objSol.appartient) {
          // j3pElement("bill1").disabled=true;
          // j3pElement("bill2").disabled=true;
            j3pElement(stor.idsRadio[0]).disabled = true
            j3pElement(stor.idsRadio[1]).disabled = true
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien

            afficheCorrection(objSol, exact)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              } else {
              // Erreur au nème essai
                exact = false

                j3pElement(stor.idsRadio[0]).disabled = true
                j3pElement(stor.idsRadio[1]).disabled = true
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++
                if (objSol.appartient) {
                  j3pElement('label' + stor.idsRadio[1]).style.color = me.styles.cfaux
                  j3pBarre('label' + stor.idsRadio[1])
                } else {
                  j3pElement('label' + stor.idsRadio[0]).style.color = me.styles.cfaux
                  j3pBarre('label' + +stor.idsRadio[0])
                }

                afficheCorrection(objSol, exact)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
