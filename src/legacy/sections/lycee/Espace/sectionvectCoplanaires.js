import $ from 'jquery'
import { j3pAddElt, j3pBoutonRadio, j3pBoutonRadioChecked, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pGetNewId, j3pMonome, j3pGetRandomInt, j3pPaletteMathquill, j3pStyle, j3pBarre } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import { afficheSystemeEquations } from 'src/lib/widgets/systemeEquations'
import { j3pAfficheSysteme } from 'src/legacy/core/functionsSysteme'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques
/**
 * @author Rémi DENIAUD
 * @since juin 2022
 * @fileOverview Cette section permet de vérifier si 3 vecteurs sont coplanaires
 */

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

/**
 * section vectCoplanaires
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {

    // Nombre de répétitions de l’exercice
    nbrepetitions: 3,
    nbchances: 2,
    indication: '',
    textes: {
      titre_exo: 'Vecteurs coplanaires',
      consigne1: 'Dans un repère $(O; \\vec{i}, \\vec{j}, \\vec{k})$, on définit les vecteurs $\\vec{u}£{coordU}$, $\\vec{v}£{coordV}$ et $\\vec{w}£{coordW}$.',
      consigne2: 'Ces trois vecteurs sont-ils coplanaires&nbsp;?',
      consigne3: 'Quel type d’inégalité te permet de le justifier&nbsp;?',
      consigne4: 'Donne ensuite les valeurs de $a$ et $b$.',
      consigne5: '$a=$&1& et $b=$&2&.',
      oui: 'oui',
      non: 'non',
      comment1: '',
      corr1: 'Pour le vérifier, on peut par exemple chercher les réels $a$ et $b$ tels que £{uvw} en résolvant le système&nbsp;:',
      corr2: 'Ce système équivaut à ‡1‡ $\\iff$ ‡2‡',
      corr3: '‡1‡ $\\iff$ ‡2‡',
      corr4: 'On obtient une contradiction, ce qui permet de conclure que de telles valeurs $a$ et $b$ n’existent pas et donc que les vecteurs $\\vec{u}$, $\\vec{v}$ et $\\vec{w}$ ne sont pas coplanaires.',
      corr5: 'Ainsi ‡1‡ et on conclut que $£e$.',
      corr6: 'Donc les vecteurs $\\vec{u}$, $\\vec{v}$ et $\\vec{w}$ sont coplanaires.'
    }
  }
} // getDonnees

/**
 * Retourne les données pour chaque sujet
 * @private
 * @param {Boolean} estCoplanaire : permet de donner des coordonnées en fonction du cas de figure
 * @return {Object}
 */
function genereAlea (estCoplanaire) {
  const obj = {}
  let minIndexPlus1, minIndexMoins1
  function estColineaire (v1, v2) {
    // v1 et v2 sont des vecteurs écris sous forme de tableau de 3 valeurs
    return ((Math.abs(v1[0] * v2[1] - v1[1] * v2[0]) < Math.pow(10, -10)) && (Math.abs(v1[0] * v2[2] - v1[2] * v2[0]) < Math.pow(10, -10)))
  }
  let pbAlea
  do {
    let pbCoord
    do {
      obj.xV = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
      obj.yV = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
      obj.zV = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
      obj.xW = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
      obj.yW = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
      obj.zW = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
      const coordArray = [obj.xV, obj.xW, obj.yV, obj.yW, obj.zV, obj.zW]
      minIndexPlus1 = coordArray.indexOf(1)
      minIndexMoins1 = coordArray.indexOf(-1)
      // Pour qu’on puisse utiliser n’importe quel type d’égalité permettant de justifier que les vecteurs son coplanaires,
      // je m’arrange pour que la substitution soit facile avec n’importe quel cas de figure et glissant un coef 1 ou -1 à la fois dans v et w
      pbCoord = (![obj.xV, obj.yV, obj.zV].includes(1) && ![obj.xV, obj.yV, obj.zV].includes(-1)) || (![obj.xW, obj.yW, obj.zW].includes(1) && ![obj.xW, obj.yW, obj.zW].includes(-1))
    } while (pbCoord || ((minIndexPlus1 === -1) && (minIndexMoins1 === -1)) || (estColineaire([obj.xV, obj.yV, obj.zV], [obj.xW, obj.yW, obj.zW])))
    // obj.aTab et obj.bTab seront des array et non des number : en effet, ils pourront servir dans 3 cas différent :
    // u=av+bw, v=au+bw ou w=au+bv
    // Le plus simple, le cas 1 car dans ce cas, ce seront des entiers choisis aléatoirement
    do {
      obj.a = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
      obj.b = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
      // Je fais quand même en sorte qu’ils soient différents (en valeur absolue)
    } while (Math.abs(Math.abs(obj.a) - Math.abs(obj.b)) < Math.pow(10, -10))
    obj.xU = obj.a * obj.xV + obj.b * obj.xW
    obj.yU = obj.a * obj.yV + obj.b * obj.yW
    obj.zU = obj.a * obj.zV + obj.b * obj.zW
    // La vérification suivante ne semble pas indispensable, mais au cas où...
    const inPlus = (minIndexPlus1 === -1) ? 6 : minIndexPlus1
    const inMoins = (minIndexMoins1 === -1) ? 6 : minIndexMoins1
    const pos1 = Math.min(inPlus, inMoins)
    pbAlea = (Math.abs(obj.xU) < Math.pow(10, -10)) && ([0, 1].includes(pos1))
    pbAlea = pbAlea || ((Math.abs(obj.yU) < Math.pow(10, -10)) && ([2, 3].includes(pos1)))
    pbAlea = pbAlea || ((Math.abs(obj.zU) < Math.pow(10, -10)) && ([4, 5].includes(pos1)))
    const detUW = obj.xU * obj.yW - obj.yU * obj.xW
    const detVW = obj.xV * obj.yW - obj.yV * obj.xW
    const detUV = obj.xU * obj.yV - obj.yU * obj.xV
    pbAlea = pbAlea || (Math.abs(detUW) < Math.pow(10, -10)) || (Math.abs(detVW) < Math.pow(10, -10)) || (Math.abs(detUV) < Math.pow(10, -10))
  } while (pbAlea || estColineaire([obj.xU, obj.yU, obj.zU], [obj.xW, obj.yW, obj.zW]) || estColineaire([obj.xV, obj.yV, obj.zV], [obj.xU, obj.yU, obj.zU]))
  if (estCoplanaire) {
    obj.aTab = [obj.a]
    obj.bTab = [obj.b]
    // Pour les autres, il faut résoudre un système (2*2 va suffire)
    const detUW = obj.xU * obj.yW - obj.yU * obj.xW
    const detVW = obj.xV * obj.yW - obj.yV * obj.xW
    const detUV = obj.xU * obj.yV - obj.yU * obj.xV
    obj.aTab.push(detVW / detUW, -detVW / detUV)
    obj.bTab.push(detUV / detUW, detUW / detUV)
    // console.log('obj.a:', obj.a, 'obj.b:', obj.b, 'obj.aTab:', obj.aTab, 'obj.bTab:', obj.bTab)
    // Mais je peux avoir besoin d’affichages fractionnaires de ces coefs a et b (s’ils ne sont pas entiers)
    obj.aTxt = [String(obj.a), j3pGetLatexQuotient(detVW, detUW), j3pGetLatexQuotient(-detVW, detUV)]
    obj.bTxt = [String(obj.b), j3pGetLatexQuotient(detUV, detUW), j3pGetLatexQuotient(detUW, detUV)]
  } else {
    let pb0
    do {
      const choixVarModifiee = j3pGetRandomInt(0, 2)
      const modif = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 2)
      if (choixVarModifiee === 0) {
        obj.xU += modif
        pb0 = (Math.abs(obj.xU) < Math.pow(10, -10))
      } else if (choixVarModifiee === 1) {
        obj.yU += modif
        pb0 = (Math.abs(obj.yU) < Math.pow(10, -10))
      } else {
        obj.zU += modif
        pb0 = (Math.abs(obj.zU) < Math.pow(10, -10))
      }
    } while (pb0)
  }
  const tabCoord = [
    [obj.xU, obj.yU, obj.zU, obj.xV, obj.yV, obj.zV, obj.xW, obj.yW, obj.zW],
    [obj.xV, obj.yV, obj.zV, obj.xU, obj.yU, obj.zU, obj.xW, obj.yW, obj.zW],
    [obj.xW, obj.yW, obj.zW, obj.xU, obj.yU, obj.zU, obj.xV, obj.yV, obj.zV]
  ]
  // On retient tous les systèmes obtenus dans les différentes corrections possibles
  obj.tabSysteme = [[], [], []]
  for (let i = 0; i < 3; i++) {
    const [xU, yU, zU, xV, yV, zV, xW, yW, zW] = tabCoord[i]
    obj.tabSysteme[i] = [
      ['$' + xU + '=' + j3pMonome(1, 1, xV, 'a') + j3pMonome(2, 1, xW, 'b') + '$',
        '$' + yU + '=' + j3pMonome(1, 1, yV, 'a') + j3pMonome(2, 1, yW, 'b') + '$',
        '$' + zU + '=' + j3pMonome(1, 1, zV, 'a') + j3pMonome(2, 1, zW, 'b') + '$']
    ]
    // J’identifie l’équation que je vais modifier pour faire une substitution
    let numEq, // l’équation dans laquelle effectuer la substitution
      c1, // savoir si le premier coef qui permet de faire une substitution est 1 ou -1
      indexVar // me servira pour savoir quelle variable j’isole
    if (i === 0) {
      if (minIndexPlus1 === -1) {
        numEq = Math.floor(minIndexMoins1 / 2)
        c1 = -1
        indexVar = minIndexMoins1
      } else if (minIndexMoins1 === -1) {
        numEq = Math.floor(minIndexPlus1 / 2)
        c1 = 1
        indexVar = minIndexPlus1
      } else {
        numEq = Math.floor(Math.min(minIndexPlus1, minIndexMoins1) / 2)
        c1 = (minIndexPlus1 < minIndexMoins1) ? 1 : -1
        indexVar = Math.min(minIndexPlus1, minIndexMoins1)
      }
    } else {
      if (i === 1) {
        const coordArray = [obj.xU, obj.xW, obj.yU, obj.yW, obj.zU, obj.zW]
        minIndexPlus1 = (coordArray.includes(1)) ? coordArray.indexOf(1) : 6
        minIndexMoins1 = (coordArray.includes(-1)) ? coordArray.indexOf(-1) : 6
      } else {
        const coordArray = [obj.xU, obj.xV, obj.yU, obj.yV, obj.zU, obj.zV]
        minIndexPlus1 = (coordArray.includes(1)) ? coordArray.indexOf(1) : 6
        minIndexMoins1 = (coordArray.includes(-1)) ? coordArray.indexOf(-1) : 6
      }
      numEq = Math.floor(Math.min(minIndexPlus1, minIndexMoins1) / 2)
      c1 = (minIndexPlus1 < minIndexMoins1) ? 1 : -1
      indexVar = Math.min(minIndexPlus1, minIndexMoins1)
    }
    // Dans ce qui va suivre, (minIndexPlus1 % 2 === 0) traduit le fait que c’est le coef devant a qui vaut 1 ou -1
    obj.tabSysteme[i].push([], [], [], [], [])
    if (numEq === 0) {
      const subst = (indexVar % 2 === 0)
        ? c1 * xU + j3pMonome(2, 1, -c1 * xW, 'b')
        : c1 * xU + j3pMonome(2, 1, -c1 * xV, 'a')
      obj.tabSysteme[i][1].push((indexVar % 2 === 0) ? '$a=' + subst + '$' : '$b=' + subst + '$')
      obj.tabSysteme[i][1].push('$' + yU + '=' + j3pMonome(1, 1, yV, 'a') + j3pMonome(2, 1, yW, 'b') + '$',
        '$' + zU + '=' + j3pMonome(1, 1, zV, 'a') + j3pMonome(2, 1, zW, 'b') + '$')
      // 2ème système
      obj.tabSysteme[i][2].push(obj.tabSysteme[i][1][0])
      obj.tabSysteme[i][2].push('$' + yU + '=' + j3pMonome(1, 1, yV, (indexVar % 2 === 0) ? '(' + subst + ')' : 'a') + j3pMonome(2, 1, yW, (indexVar % 2 === 0) ? 'b' : '(' + subst + ')') + '$',
        '$' + zU + '=' + j3pMonome(1, 1, zV, (indexVar % 2 === 0) ? '(' + subst + ')' : 'a') + j3pMonome(2, 1, zW, (indexVar % 2 === 0) ? 'b' : '(' + subst + ')') + '$')
      // 3ème système
      obj.tabSysteme[i][3].push(obj.tabSysteme[i][1][0])
      obj.tabSysteme[i][3].push((indexVar % 2 === 0)
        ? '$' + yU + '=' + j3pMonome(1, 0, c1 * yV * xU) + j3pMonome(2, 1, -c1 * yV * xW, 'b') + j3pMonome(2, 1, yW, 'b') + '$'
        : '$' + yU + '=' + j3pMonome(1, 1, yV, 'a') + j3pMonome(2, 0, c1 * yW * xU) + j3pMonome(2, 1, -c1 * yW * xV, 'a') + '$'
      )
      obj.tabSysteme[i][3].push((indexVar % 2 === 0)
        ? '$' + zU + '=' + j3pMonome(1, 0, c1 * zV * xU) + j3pMonome(2, 1, -c1 * zV * xW, 'b') + j3pMonome(2, 1, zW, 'b') + '$'
        : '$' + zU + '=' + j3pMonome(1, 1, zV, 'a') + j3pMonome(2, 0, c1 * zW * xU) + j3pMonome(2, 1, -c1 * zW * xV, 'a') + '$'
      )
      // 4ème système
      obj.tabSysteme[i][4].push(obj.tabSysteme[i][1][0])
      obj.tabSysteme[i][4].push((indexVar % 2 === 0)
        ? ((Math.abs(yW - c1 * yV * xW) < Math.pow(10, -10)) ? '$' + yU + '=' + String(c1 * yV * xU) + '$' : '$b=' + j3pGetLatexQuotient(yU - c1 * yV * xU, yW - c1 * yV * xW) + '$')
        : ((Math.abs(yV - c1 * yW * xV) < Math.pow(10, -10)) ? '$' + yU + '=' + String(c1 * yW * xU) + '$' : '$a=' + j3pGetLatexQuotient(yU - c1 * yW * xU, yV - c1 * yW * xV) + '$')
      )
      obj.tabSysteme[i][4].push((indexVar % 2 === 0)
        ? ((Math.abs(zW - c1 * zV * xW) < Math.pow(10, -10)) ? '$' + zU + '=' + String(c1 * zV * xU) + '$' : '$b=' + j3pGetLatexQuotient(zU - c1 * zV * xU, zW - c1 * zV * xW) + '$')
        : ((Math.abs(zV - c1 * zW * xV) < Math.pow(10, -10)) ? '$' + zU + '=' + String(c1 * zW * xU) + '$' : '$a=' + j3pGetLatexQuotient(zU - c1 * zW * xU, zV - c1 * zW * xV) + '$')
      )
      if (estCoplanaire) {
        // last système
        obj.tabSysteme[i][5].push((indexVar % 2 === 0) ? '$a=' + obj.aTxt[i] + '$' : '$b=' + obj.bTxt[i] + '$')
        obj.tabSysteme[i][5].push((indexVar % 2 === 0) ? '$b=' + obj.bTxt[i] + '$' : '$a=' + obj.aTxt[i] + '$')
        obj.tabSysteme[i][5].push((indexVar % 2 === 0) ? '$b=' + obj.bTxt[i] + '$' : '$a=' + obj.aTxt[i] + '$')
      }
    } else if (numEq === 1) {
      const subst = (indexVar % 2 === 0)
        ? c1 * yU + j3pMonome(2, 1, -c1 * yW, 'b')
        : c1 * yU + j3pMonome(2, 1, -c1 * yV, 'a')
      obj.tabSysteme[i][1].push('$' + xU + '=' + j3pMonome(1, 1, xV, 'a') + j3pMonome(2, 1, xW, 'b') + '$')
      obj.tabSysteme[i][1].push((indexVar % 2 === 0)
        ? '$a=' + subst + '$'
        : '$b=' + subst + '$')
      obj.tabSysteme[i][1].push('$' + zU + '=' + j3pMonome(1, 1, zV, 'a') + j3pMonome(2, 1, zW, 'b') + '$')
      // 2ème système
      obj.tabSysteme[i][2].push('$' + xU + '=' + j3pMonome(1, 1, xV, (indexVar % 2 === 0) ? '(' + subst + ')' : 'a') + j3pMonome(2, 1, xW, (indexVar % 2 === 0) ? 'b' : '(' + subst + ')') + '$')
      obj.tabSysteme[i][2].push(obj.tabSysteme[i][1][1])
      obj.tabSysteme[i][2].push('$' + zU + '=' + j3pMonome(1, 1, zV, (indexVar % 2 === 0) ? '(' + subst + ')' : 'a') + j3pMonome(2, 1, zW, (indexVar % 2 === 0) ? 'b' : '(' + subst + ')') + '$')
      // 3ème système
      obj.tabSysteme[i][3].push((indexVar % 2 === 0)
        ? '$' + xU + '=' + j3pMonome(1, 0, c1 * xV * yU) + j3pMonome(2, 1, -c1 * xV * yW, 'b') + j3pMonome(2, 1, xW, 'b') + '$'
        : '$' + xU + '=' + j3pMonome(1, 1, xV, 'a') + j3pMonome(2, 0, c1 * xW * yU) + j3pMonome(2, 1, -c1 * xW * yV, 'a') + '$'
      )
      obj.tabSysteme[i][3].push(obj.tabSysteme[i][1][1])
      obj.tabSysteme[i][3].push((indexVar % 2 === 0)
        ? '$' + zU + '=' + j3pMonome(1, 0, c1 * zV * yU) + j3pMonome(2, 1, -c1 * zV * yW, 'b') + j3pMonome(2, 1, zW, 'b') + '$'
        : '$' + zU + '=' + j3pMonome(1, 1, zV, 'a') + j3pMonome(2, 0, c1 * zW * yU) + j3pMonome(2, 1, -c1 * zW * yV, 'a') + '$'
      )
      // 4ème système
      obj.tabSysteme[i][4].push((indexVar % 2 === 0)
        ? ((Math.abs(xW - c1 * xV * yW) < Math.pow(10, -10)) ? '$' + xU + '=' + String(c1 * xV * yU) + '$' : '$b=' + j3pGetLatexQuotient(xU - c1 * xV * yU, xW - c1 * xV * yW) + '$')
        : ((Math.abs(xV - c1 * xW * yV) < Math.pow(10, -10)) ? '$' + xU + '=' + String(c1 * xW * yU) + '$' : '$a=' + j3pGetLatexQuotient(xU - c1 * xW * yU, xV - c1 * xW * yV) + '$')
      )
      obj.tabSysteme[i][4].push(obj.tabSysteme[i][1][1])
      obj.tabSysteme[i][4].push((indexVar % 2 === 0)
        ? ((Math.abs(zW - c1 * zV * yW) < Math.pow(10, -10)) ? '$' + zU + '=' + String(c1 * zV * yU) + '$' : '$b=' + j3pGetLatexQuotient(zU - c1 * zV * yU, zW - c1 * zV * yW) + '$')
        : ((Math.abs(zV - c1 * zW * yV) < Math.pow(10, -10)) ? '$' + zU + '=' + String(c1 * zW * yU) + '$' : '$a=' + j3pGetLatexQuotient(zU - c1 * zW * yU, zV - c1 * zW * yV) + '$')
      )
      if (estCoplanaire) {
        // last système
        obj.tabSysteme[i][5].push((indexVar % 2 === 0) ? '$b=' + obj.bTxt[i] + '$' : '$a=' + obj.aTxt[i] + '$')
        obj.tabSysteme[i][5].push((indexVar % 2 === 0) ? '$a=' + obj.aTxt[i] + '$' : '$b=' + obj.bTxt[i] + '$')
        obj.tabSysteme[i][5].push((indexVar % 2 === 0) ? '$b=' + obj.bTxt[i] + '$' : '$a=' + obj.aTxt[i] + '$')
      }
    } else {
      const subst = (indexVar % 2 === 0)
        ? c1 * zU + j3pMonome(2, 1, -c1 * zW, 'b')
        : c1 * zU + j3pMonome(2, 1, -c1 * zV, 'a')
      obj.tabSysteme[i][1].push('$' + xU + '=' + j3pMonome(1, 1, xV, 'a') + j3pMonome(2, 1, xW, 'b') + '$',
        '$' + yU + '=' + j3pMonome(1, 1, yV, 'a') + j3pMonome(2, 1, yW, 'b') + '$')
      obj.tabSysteme[i][1].push((indexVar % 2 === 0)
        ? '$a=' + subst + '$'
        : '$b=' + subst + '$')
      // 2ème système
      obj.tabSysteme[i][2].push('$' + xU + '=' + j3pMonome(1, 1, xV, (indexVar % 2 === 0) ? '(' + subst + ')' : 'a') + j3pMonome(2, 1, xW, (indexVar % 2 === 0) ? 'b' : '(' + subst + ')') + '$',
        '$' + yU + '=' + j3pMonome(1, 1, yV, (indexVar % 2 === 0) ? '(' + subst + ')' : 'a') + j3pMonome(2, 1, yW, (indexVar % 2 === 0) ? 'b' : '(' + subst + ')') + '$')
      obj.tabSysteme[i][2].push(obj.tabSysteme[i][1][2])
      // 3ème système
      obj.tabSysteme[i][3].push((indexVar % 2 === 0)
        ? '$' + xU + '=' + j3pMonome(1, 0, c1 * xV * zU) + j3pMonome(2, 1, -c1 * xV * zW, 'b') + j3pMonome(2, 1, xW, 'b') + '$'
        : '$' + xU + '=' + j3pMonome(1, 1, xV, 'a') + j3pMonome(2, 0, c1 * xW * zU) + j3pMonome(2, 1, -c1 * xW * zV, 'a') + '$'
      )
      obj.tabSysteme[i][3].push((indexVar % 2 === 0)
        ? '$' + yU + '=' + j3pMonome(1, 0, c1 * yV * zU) + j3pMonome(2, 1, -c1 * yV * zW, 'b') + j3pMonome(2, 1, yW, 'b') + '$'
        : '$' + yU + '=' + j3pMonome(1, 1, yV, 'a') + j3pMonome(2, 0, c1 * yW * zU) + j3pMonome(2, 1, -c1 * yW * zV, 'a') + '$'
      )
      obj.tabSysteme[i][3].push(obj.tabSysteme[i][1][2])
      // 4ème système
      obj.tabSysteme[i][4].push((indexVar % 2 === 0)
        ? ((Math.abs(xW - c1 * xV * zW) < Math.pow(10, -10)) ? '$' + xU + '=' + String(c1 * xV * zU) + '$' : '$b=' + j3pGetLatexQuotient(xU - c1 * xV * zU, xW - c1 * xV * zW) + '$')
        : ((Math.abs(xV - c1 * xW * zV) < Math.pow(10, -10)) ? '$' + xU + '=' + String(c1 * xW * zU) + '$' : '$a=' + j3pGetLatexQuotient(xU - c1 * xW * zU, xV - c1 * xW * zV) + '$')
      )
      obj.tabSysteme[i][4].push((indexVar % 2 === 0)
        ? ((Math.abs(yW - c1 * yV * zW) < Math.pow(10, -10)) ? '$' + yU + '=' + String(c1 * yV * zU) + '$' : '$b=' + j3pGetLatexQuotient(yU - c1 * yV * zU, yW - c1 * yV * zW) + '$')
        : ((Math.abs(yV - c1 * yW * zV) < Math.pow(10, -10)) ? '$' + yU + '=' + String(c1 * yW * zU) + '$' : '$a=' + j3pGetLatexQuotient(yU - c1 * yW * zU, yV - c1 * yW * zV) + '$')
      )
      obj.tabSysteme[i][4].push(obj.tabSysteme[i][1][2])
      if (estCoplanaire) {
        // last système
        obj.tabSysteme[i][5].push((indexVar % 2 === 0) ? '$b=' + obj.bTxt[i] + '$' : '$a=' + obj.aTxt[i] + '$')
        obj.tabSysteme[i][5].push((indexVar % 2 === 0) ? '$b=' + obj.bTxt[i] + '$' : '$a=' + obj.aTxt[i] + '$')
        obj.tabSysteme[i][5].push((indexVar % 2 === 0) ? '$a=' + obj.aTxt[i] + '$' : '$b=' + obj.bTxt[i] + '$')
      }
    }
  }
  obj.coordU = '(' + obj.xU + ';' + obj.yU + ';' + obj.zU + ')'
  obj.coordV = '(' + obj.xV + ';' + obj.yV + ';' + obj.zV + ')'
  obj.coordW = '(' + obj.xW + ';' + obj.yW + ';' + obj.zW + ')'
  if (estCoplanaire) {
    obj.e = [
      '\\vec{u}=' + j3pGetLatexMonome(1, 1, obj.aTxt[0], '\\vec{v}') + j3pGetLatexMonome(2, 1, obj.bTxt[0], '\\vec{w}'),
      '\\vec{v}=' + j3pGetLatexMonome(1, 1, obj.aTxt[1], '\\vec{u}') + j3pGetLatexMonome(2, 1, obj.bTxt[1], '\\vec{w}'),
      '\\vec{w}=' + j3pGetLatexMonome(1, 1, obj.aTxt[2], '\\vec{u}') + j3pGetLatexMonome(2, 1, obj.bTxt[2], '\\vec{v}')
    ]
    // console.log('obj.e:', obj.e)
  }
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(me.donneesSection.textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  stor.elts.isCoplanairesArray = []
  for (let i = 1; i <= me.donneesSection.nbrepetitions; i++) stor.elts.isCoplanairesArray.push(true)
  // Je ne mets un cas de vecteurs non coplanaires que si nbrepetitions n’est pas trop petit
  if (me.donneesSection.nbrepetitions > 2) stor.elts.isCoplanairesArray[j3pGetRandomInt(0, me.donneesSection.nbrepetitions - 1)] = false
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  function ecoute (laZone) {
    if (laZone.className.includes('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, laZone, { liste: ['fraction'] })
    }
  }
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.estCoplanaire = stor.elts.isCoplanairesArray[me.questionCourante - 1]
  stor.objDonnees = genereAlea(stor.estCoplanaire)
  me.logIfDebug('stor.objDonnees:', stor.objDonnees)
  // on affiche l’énoncé
  ;[1, 2, 3, 4, 5, 6, 7, 8].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })
  ;[1, 2].forEach(i => { j3pAffiche(stor['zoneCons' + i], '', ds.textes['consigne' + i], stor.objDonnees) })
  // zoneCons3, je mets les boutons radio
  stor.nameRadio = j3pGetNewId('choix')
  stor.idsRadio = ['1', '2'].map(i => j3pGetNewId('radio' + i))
  const espacetxt = '&nbsp;&nbsp;&nbsp;&nbsp;'
  j3pAffiche(stor.zoneCons3, '', espacetxt)
  j3pBoutonRadio(stor.zoneCons3, stor.idsRadio[0], stor.nameRadio, 0, ds.textes.oui)
  j3pAffiche(stor.zoneCons3, '', espacetxt + espacetxt)
  j3pBoutonRadio(stor.zoneCons3, stor.idsRadio[1], stor.nameRadio, 1, ds.textes.non)
  stor.radio1Select = -1
  stor.radio2Select = -1
  stor.tabEgalites = ['$\\vec{u}=a\\vec{v}+b\\vec{w}$', '$\\vec{v}=a\\vec{u}+b\\vec{w}$', '$\\vec{w}=a\\vec{u}+b\\vec{v}$']
  stor.idsEgalites = [1, 2, 3].map(i => j3pGetNewId('idEgalite' + i))
  stor.nameEgalite = j3pGetNewId('choixEgalite')
  stor.zoneInput = []
  j3pElement(stor.idsRadio[0]).addEventListener('click', function () {
    if (stor.radio1Select !== 0) {
      // On avait déjà sélectionné oui, donc on ne touche à rien
      j3pAffiche(stor.zoneCons4, '', ds.textes.consigne3)
      ;[0, 1, 2].forEach(i => {
        j3pAffiche(stor.zoneCons5, '', espacetxt)
        j3pBoutonRadio(stor.zoneCons5, stor.idsEgalites[i], stor.nameEgalite, i, '')
        j3pAffiche('label' + stor.idsEgalites[i], '', stor.tabEgalites[i])
        // })
        j3pElement(stor.idsEgalites[i]).index = i
        j3pElement(stor.idsEgalites[i]).addEventListener('click', function () {
          if (stor.radio2Select !== j3pElement(stor.idsEgalites[i]).index) {
            ;[6, 7, 8].forEach(i => j3pEmpty(stor['zoneCons' + i]))
            j3pAffiche(stor.zoneCons6, '', ds.textes.consigne4)
            const elt = j3pAffiche(stor.zoneCons7, '', ds.textes.consigne5, {
              inputmq1: { texte: '' },
              inputmq2: { texte: '' }
            })
            stor.zoneInput = [...elt.inputmqList]
            ;[0, 1, 2].forEach(i => {
              j3pElement(stor.idsEgalites[i]).onclick = j3pFocus.bind(null, stor.zoneInput[0])
            })
            stor.zoneInput.forEach(eltInput => {
              eltInput.typeReponse = ['nombre', 'exact']
              mqRestriction(eltInput, '\\d,./-', { commandes: ['fraction'] })
              // $(eltInput).focusin(ecoute.bind(null, this)) // Je ne sais pas pourquoi cela ne fonctionne pas
              // eltInput.addEventListener('focus', ecoute.bind(null, this)) // ça non plus
              $(eltInput).focusin(function () {
                ecoute(this)
              })
            })
            stor.laPalette = j3pAddElt(stor.zoneCons8, 'div')
            j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], { liste: ['fraction'] })
            j3pFocus(stor.zoneInput[0])
            stor.fctsValid = new ValidationZones({
              parcours: me,
              zones: stor.zoneInput.map(elt => elt.id)
            })
          }
          stor.radio2Select = j3pBoutonRadioChecked(stor.nameEgalite)[0]
        })
      })
      stor.radio1Select = 0
    }
  })
  j3pElement(stor.idsRadio[1]).addEventListener('click', function () {
    ;[4, 5, 6, 7, 8].forEach(i => j3pEmpty(stor['zoneCons' + i]))
    stor.radio1Select = 1
    stor.radio2Select = -1
  })
  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  if (stor.radio1Select === 0) j3pDetruit(stor.laPalette)
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  ;[1, 2, 3, 4, 5, 6].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
  const numRep = (stor.radio1Select === 1) ? 0 : stor.radio2Select
  j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, { uvw: stor.tabEgalites[numRep] })
  afficheSystemeEquations(stor.zoneExpli2, [
    { contenu: stor.objDonnees.tabSysteme[numRep][0][0] },
    { contenu: stor.objDonnees.tabSysteme[numRep][0][1] },
    { contenu: stor.objDonnees.tabSysteme[numRep][0][2] }
  ])
  j3pAfficheSysteme(stor.zoneExpli3, '', ds.textes.corr2, [stor.objDonnees.tabSysteme[numRep][1], stor.objDonnees.tabSysteme[numRep][2]], {}, zoneExpli.style.color)
  j3pAfficheSysteme(stor.zoneExpli4, '', ds.textes.corr3, [stor.objDonnees.tabSysteme[numRep][3], stor.objDonnees.tabSysteme[numRep][4]], {}, zoneExpli.style.color)
  if (stor.estCoplanaire) {
    j3pAfficheSysteme(stor.zoneExpli5, '', ds.textes.corr5, [stor.objDonnees.tabSysteme[numRep][5]], { e: stor.objDonnees.e[numRep] }, zoneExpli.style.color)
    j3pAffiche(stor.zoneExpli6, '', ds.textes.corr6, stor.objDonnees)
  } else {
    j3pAffiche(stor.zoneExpli5, '', ds.textes.corr4, stor.objDonnees)
  }
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  // const reponse = stor.fctsValid.validationGlobale()
  let reponse = { aRepondu: false, bonneReponse: false }
  const choixRadio = j3pBoutonRadioChecked(stor.nameRadio)[0]
  reponse.aRepondu = (choixRadio > -1)
  let choixEgalite
  if (reponse.aRepondu) {
    if (choixRadio === 0) {
      choixEgalite = j3pBoutonRadioChecked(stor.nameEgalite)[0]
      if (choixEgalite === -1) {
        reponse.aRepondu = false
        return null
      } else {
        reponse.aRepondu = stor.fctsValid.valideReponses()
        if (!reponse.aRepondu) return null
      }
    }
  }
  if (reponse.aRepondu) {
    if (choixRadio === 0) {
      // il a repondu oui, donc si mes vecteurs sont bien coplanaires, il faut que je vérifie les coefs choisis
      if (stor.estCoplanaire) {
        stor.bonChoix1 = true
        // Je regarde le choix de l’élève
        /* const choixEgalite = j3pBoutonRadioChecked(stor.nameEgalite)[0]
        if (choixEgalite === -1) return null */
        stor.zoneInput[0].reponse = [stor.objDonnees.aTab[choixEgalite]]
        stor.zoneInput[1].reponse = [stor.objDonnees.bTab[choixEgalite]]
        // console.log('Les réponses attendues:', stor.zoneInput[0].reponse, stor.zoneInput[1].reponse)
        reponse = stor.fctsValid.validationGlobale()
        if (reponse.aRepondu) {
          ;[0, 1].forEach(i => j3pDesactive(stor.idsRadio[i]))
          j3pElement('label' + stor.idsRadio[0]).style.color = me.styles.cbien
        } else {
          return null
        }
      } else {
        stor.bonChoix1 = false
        reponse.bonneReponse = false
        ;[0, 1].forEach(i => {
          stor.fctsValid.zones.bonneReponse[i] = false
          stor.fctsValid.coloreUneZone(stor.zoneInput[i].id)
        })
      }
    } else {
      stor.bonChoix1 = !stor.estCoplanaire
      reponse.bonneReponse = !stor.estCoplanaire
    }
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  const choixRadio = j3pBoutonRadioChecked(stor.nameRadio)[0]
  if (choixRadio === 0) [0, 1, 2].forEach(i => j3pDesactive(stor.idsEgalites[i]))
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
    ;[0, 1].forEach(i => j3pDesactive(stor.idsRadio[i]))
    j3pElement('label' + stor.idsRadio[choixRadio]).style.color = stor.bonChoix1 ? me.styles.cbien : me.styles.cfaux
    if (!stor.bonChoix1) {
      j3pBarre('label' + stor.idsRadio[choixRadio])
      j3pElement('label' + stor.idsRadio[1 - choixRadio]).style.color = me.styles.moyen.correction.color
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
