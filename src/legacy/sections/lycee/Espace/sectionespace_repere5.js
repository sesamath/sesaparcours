import { j3pAddElt, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pAfficheSysteme } from 'src/legacy/core/functionsSysteme'
import { afficheSystemeEquations } from 'src/lib/widgets/systemeEquations'
import { j3pEvalueExpression } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*

/*
   Dév : Alexis Lecomte
   Date : Février/mars 2014 pour l’accolade puis mai pour la section
   Scénario : représentation paramétrique d’une droite connaissant deux points ou un point+un vecteur directeur

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['type_droite', ['deux_points', 'deux_points'], 'array', 'Tableau de même longueur que le nombre de répétitions, permettant de déterminer si la droite est définie par un point et un vecteur directeur (point_vecteur), passant par l’origine et avec un vecteur directeur (origine) ou par deux points (deux_points)']
  ]

}

/**
 * section espace_repere5
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function validation (rep0, rep1, rep2) {
    const rep = []
    const calc = []
    const point = []
    const point2 = []
    const t = []
    const t2 = []
    const coord = []
    let onContinue = true
    let test = true
    t[0] = t[1] = t[2] = 0
    t2[0] = t2[1] = t2[2] = 0
    rep[0] = rep0
    rep[1] = rep1
    rep[2] = rep2
    const objReponse = {}
    objReponse.aRepondu = true
    objReponse.bon = false
    // on vérifie que réponses non vides :
    for (let i = 2; i > -1; i--) {
      if ((rep[i]) === '') {
        objReponse.aRepondu = false
        objReponse.focus_a_donner = stor.inputMqs[i]
      }
    }
    // A faire : laisser le choix du paramètre, le chercher, si différent (hors x, y, z bien sûr) on estime non répondu et on met un message particulier
    // vérification que les réponses sont des fonctions affines en t :
    objReponse.expr_affines = [true, true, true]
    for (let i = 2; i > -1; i--) {
      calc[i] = j3pEvalueExpression({ exp: j3pMathquillXcas(rep[i]), inc: ['t'] }, [1]) - j3pEvalueExpression({ exp: j3pMathquillXcas(rep[i]), inc: ['t'] }, [0])
      for (let j = 2; j < 11; j++) {
        if (Math.abs((j3pEvalueExpression({ exp: j3pMathquillXcas(rep[i]), inc: ['t'] }, [j]) - j3pEvalueExpression({ exp: j3pMathquillXcas(rep[i]), inc: ['t'] }, [0])) / j - calc[i]) > 0.000001) {
          objReponse.expr_affines[i] = false
          objReponse.focus_a_donner = stor.inputMqs[i]
          onContinue = false
        }
      }
    }
    // on regarde maintenant si les réponses sont exactes
    if (onContinue) { // on a bien que des fonctions affines, ça vaut le coup de continuer...
      for (let i = 0; i < 3; i++) {
        // coordonnée du point de l’élève:
        point[i] = j3pEvalueExpression({ exp: j3pMathquillXcas(rep[i]), inc: ['t'] }, [0])
        // on vérifie s’il est sur la droite :
        if (stor.coord_vecteur[i] !== 0) { // pour éviter la division par zéro
          t[i] = (point[i] - stor.coord[i]) / stor.coord_vecteur[i]
        }
        // coordonnée d’un second point de l’élève:
        point2[i] = j3pEvalueExpression({ exp: j3pMathquillXcas(rep[i]), inc: ['t'] }, [1])
        // on vérifie aussi s’il est sur la droite
        if (stor.coord_vecteur[i] !== 0) { // pour éviter la division par zéro
          t2[i] = (point2[i] - stor.coord[i]) / stor.coord_vecteur[i]
        }
      }
      // si un des coeffs du vecteur directeur est nul, on ne pas comparer la valeur de t correspondante à l’équation, donc on lui donne arbitrairement une des deux autres valeurs par contre on doit tester si l’on a bien la même réponse (par ex z=2)
      for (let i = 0; i < 3; i++) {
        if (stor.coord_vecteur[i] === 0) {
          for (let k = 0; k < 3; k++) {
            if (k !== i) {
              t[i] = t[k]
              t2[i] = t2[k]
            }
          }

          test = (test && (stor.coord[i] === point[i]) && (point[i] === point2[i]))
          // dernier test précédent : encore un cas trouvé en classe, si l’élève a une réponse affine et que par 'chance' avec t=0 ça marche on lui comptera bon, on va donc vérifier que non constante
        } else { // il reste un pb, si le coef du vect dir est nul mais que l’élève a répondu une constante, les tests précédents passeront
          if (t2[i] === t[i]) test = false
        }
      }
      if (test && Math.abs(t[0] - t[1]) < 0.00001 && Math.abs(t[0] - t[2]) < 0.00001 && Math.abs(t2[0] - t2[1]) < 0.00001 && Math.abs(t2[0] - t2[2]) < 0.00001) {
        objReponse.bon = true
      }
    }
    // Ajout de tests supplémentaires, pour si faute ne pas mettre les trois équations en rouge
    if (!objReponse.bon) {
      for (let i = 0; i < 3; i++) {
        // coordonnées du point de l’élève:
        point[i] = j3pEvalueExpression({ exp: j3pMathquillXcas(rep[i]), inc: ['t'] }, [0])
        point2[i] = j3pEvalueExpression({ exp: j3pMathquillXcas(rep[i]), inc: ['t'] }, [1])
        coord[i] = point2[i] - point[i]// coordonnées du vecteur directeur de l’élève
      }
      if ((point[0] === stor.coord[0] && point[1] === stor.coord[1] && point[2] === stor.coord[2]) || (point[0] === stor.coord2[0] && point[1] === stor.coord2[1] && point[2] === stor.coord2[2])) {
        // l’élève a choisi soit le point 1 soit le point 2
        objReponse.juste = 'coord_point'
      }
      if (((coord[0] === stor.coord_vecteur[0]) && (coord[1] === stor.coord_vecteur[1]) && (coord[2] === stor.coord_vecteur[2])) || ((coord[0] === -stor.coord_vecteur[0]) && (coord[1] === -stor.coord_vecteur[1]) && (coord[2] === -stor.coord_vecteur[2]))) {
        // l’élève a choisi vec(u) ou -vec(u) comme vecteur directeur.
        objReponse.juste = 'coord_vecteur'
      }

      me.logIfDebug('Réponses élèves (point 1, point 2, coord vecteur, t, t2)', point, point2, coord, t, t2)
    }
    return objReponse
  }

  function correctionDetaillee (bonneReponse) {
    const reponse = { x: '', y: '', z: '' }// on commence par définir les réponses 'naturelles'
    if (stor.coord[0] !== 0) reponse.x += stor.coord[0]
    if (stor.coord[1] !== 0) reponse.y += stor.coord[1]
    if (stor.coord[2] !== 0) reponse.z += stor.coord[2]
    if (stor.coord_vecteur[0] !== 0) {
      if (stor.coord_vecteur[0] > 0) {
        if (reponse.x !== '') reponse.x += '+'
        if (!(stor.coord_vecteur[0] === 1)) reponse.x += stor.coord_vecteur[0]
      } else {
        if (stor.coord_vecteur[0] === -1) {
          reponse.x += '-'
        } else {
          reponse.x += stor.coord_vecteur[0]
        }
      }
      reponse.x += 't'
    }
    if (stor.coord_vecteur[1] !== 0) {
      if (stor.coord_vecteur[1] > 0) {
        if (reponse.y !== '') reponse.y += '+'
        if (!(stor.coord_vecteur[1] === 1)) reponse.y += stor.coord_vecteur[1]
      } else {
        if (stor.coord_vecteur[1] === -1) {
          reponse.y += '-'
        } else {
          reponse.y += stor.coord_vecteur[1]
        }
      }
      reponse.y += 't'
    }
    if (stor.coord_vecteur[2] !== 0) {
      if (stor.coord_vecteur[2] > 0) {
        if (reponse.z !== '') reponse.z += '+'
        if (!(stor.coord_vecteur[2] === 1)) reponse.z += stor.coord_vecteur[2]
      } else {
        if (stor.coord_vecteur[2] === -1) {
          reponse.z += '-'
        } else {
          reponse.z += stor.coord_vecteur[2]
        }
      }
      reponse.z += 't'
    }
    // cas particuliers :
    if (stor.coord[0] === 0 && stor.coord_vecteur[0] === 0) reponse.x = 0
    if (stor.coord[1] === 0 && stor.coord_vecteur[1] === 0) reponse.y = 0
    if (stor.coord[2] === 0 && stor.coord_vecteur[2] === 0) reponse.z = 0
    const expli = j3pAddElt(stor.divEnonce, 'div', '', { style: me.styles.toutpetit.explications })
    if (bonneReponse) j3pStyle(expli, { color: me.styles.cbien })
    for (let i = 1; i <= 4; i++) stor['zoneExpli' + i] = j3pAddElt(expli, 'div')
    if (ds.type_droite[me.questionCourante - 1] === 'origine') {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.correction1bis, {})
      j3pAfficheSysteme(stor.zoneExpli2, '', ds.textes.correction2, [['$x=0+\\alpha t$', '$y=0+\\beta t$', '$z=0+\\gamma t$']], { inputmq1: {}, inputmq2: {}, inputmq3: {} }, expli.style.color)
    } else {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.correction1, {})
      j3pAfficheSysteme(stor.zoneExpli2, '', ds.textes.correction2, [['$x=x_A+\\alpha t$', '$y=y_A+\\beta t$', '$z=z_A+\\gamma t$']], { inputmq1: {}, inputmq2: {}, inputmq3: {} }, expli.style.color)
    }

    // on affiche la correction attendue, en remarque l’infinité de représentations param possible
    if (ds.type_droite[me.questionCourante - 1] === 'point_vecteur' || ds.type_droite[me.questionCourante - 1] === 'origine') {
      j3pAfficheSysteme(stor.zoneExpli3, '', ds.textes.correction3bis, [['$x=' + reponse.x + '$', '$y=' + reponse.y + '$', '$z=' + reponse.z + '$']], { inputmq1: {}, inputmq2: {}, inputmq3: {} }, expli.style.color)
    } else {
      j3pAfficheSysteme(stor.zoneExpli3, '', ds.textes.correction3, [['$x=' + reponse.x + '$', '$y=' + reponse.y + '$', '$z=' + reponse.z + '$']], { a: stor.coord_vecteur[0], b: stor.coord_vecteur[1], c: stor.coord_vecteur[2], inputmq1: {}, inputmq2: {}, inputmq3: {} }, expli.style.color)
    }
    j3pAffiche(stor.zoneExpli4, '', ds.textes.correction4, {})
  } // correctionDetaillee

  function getDonnees () {
    return {
      typesection: 'lycee', //

      nbrepetitions: 2,
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"
      type_droite: ['deux_points', 'deux_points'],
      textes: {
        titre_exo: 'Représentation paramétrique d’une droite',
        phrase1: 'Dans un repère orthonormé de l’espace, on considère les points $A(£a;£b;£c)$ et $B(£d;£e;£f)$.',
        phrase1_bis: 'Dans un repère orthonormé de l’espace, on considère une droite $D$ passant par $A(£a;£b;£c)$ et de vecteur directeur $\\vec{u}(£d;£e;£f)$.',
        phrase1_ter: 'Dans un repère orthonormé de l’espace d’origine O, on considère une droite $D$ passant par O et de vecteur directeur $\\vec{u}(£a;£b;£c)$.',
        phrase2: 'Détermine une représentation paramétrique de la droite $(AB)$ (on prendra $t$ pour paramètre)&nbsp;:',
        phrase2_bis: 'Détermine une représentation paramétrique de la droite $D$ (on prendra $t$ pour paramètre)&nbsp;:',
        phrase3: 'Il y a au moins une zone de saisie incorrecte.',
        correction1: 'La représentation paramétrique d’une droite passant par le point $A(x_A;y_A;z_A)$ et de vecteur directeur $\\vec{u}(\\alpha;\\beta;\\gamma)$ est de la forme :',
        correction1bis: 'La représentation paramétrique d’une droite passant par le point O et de vecteur directeur $\\vec{u}(\\alpha;\\beta;\\gamma)$ est de la forme :',
        correction2: '‡1‡, où $t$ décrit $\\R$.',
        correction3: 'On obtient donc ici : ‡1‡ avec $t\\in \\R$, puisque $\\vecteur{AB}(x_B-x_A;y_B-y_A;z_B-z_A)$ (soit $\\vecteur{AB}(£a;£b;£c)$) est un vecteur directeur de la droite $(AB)$.<br>',
        correction3bis: 'On obtient donc ici : ‡1‡ avec $t\\in \\R$.<br>',
        correction4: 'Remarque : il existe une infinité de représentations paramétriques, on peut donc trouver d’autres bonnes réponses (il suffit de choisir un autre vecteur directeur ou de prendre un autre point de référence).',
        correction5: 'Les coordonnées du point choisi sont cependant exactes.',
        correction6: 'Les coordonnées du vecteur directeur utilisé sont cependant exactes.'
        // correction2:"@sys1@sys, où $t$ décrit $\\R$."
      },
      pe: 0
    }
  }

  function enonceMain () {
    stor.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    stor.coord = []
    stor.coord2 = []
    stor.coord_vecteur = []
    let consigne1, consigne2
    switch (ds.type_droite[me.questionCourante - 1]) {
      case 'point_vecteur':
        consigne1 = ds.textes.phrase1_bis
        consigne2 = ds.textes.phrase2_bis
        while (true) { // on évite les 3 coordonnées du vecteur nulles
          stor.coord[0] = j3pGetRandomInt(-5, 5)
          stor.coord[1] = j3pGetRandomInt(-5, 5)
          stor.coord[2] = j3pGetRandomInt(-5, 5)
          stor.coord[3] = j3pGetRandomInt(-5, 5)
          stor.coord[4] = j3pGetRandomInt(-5, 5)
          stor.coord[5] = j3pGetRandomInt(-5, 5)
          let nbNonNul = 0
          for (const i of [3, 4, 5]) {
            if (stor.coord[i] !== 0) nbNonNul++
          }
          if (nbNonNul > 1) break
        }
        stor.coord_vecteur[0] = stor.coord[3]
        stor.coord_vecteur[1] = stor.coord[4]
        stor.coord_vecteur[2] = stor.coord[5]
        // second point :
        stor.coord2[0] = stor.coord_vecteur[0] + stor.coord[0]
        stor.coord2[1] = stor.coord_vecteur[1] + stor.coord[1]
        stor.coord2[2] = stor.coord_vecteur[2] + stor.coord[2]
        break

      case 'origine':
        consigne1 = ds.textes.phrase1_ter
        consigne2 = ds.textes.phrase2_bis
        while (true) { // on évite les 3 coordonnées du vecteur nulles
          stor.coord_vecteur[0] = j3pGetRandomInt(-5, 5)
          stor.coord_vecteur[1] = j3pGetRandomInt(-5, 5)
          stor.coord_vecteur[2] = j3pGetRandomInt(-5, 5)
          let nbNonNul = 0
          for (const i of [0, 1, 2]) {
            if (stor.coord_vecteur[i] !== 0) nbNonNul++
          }
          if (nbNonNul > 1) break
        }
        stor.coord[0] = 0
        stor.coord[1] = 0
        stor.coord[2] = 0
        // second point :
        stor.coord2[0] = stor.coord_vecteur[0] + stor.coord[0]
        stor.coord2[1] = stor.coord_vecteur[1] + stor.coord[1]
        stor.coord2[2] = stor.coord_vecteur[2] + stor.coord[2]
        break

      default:
        consigne1 = ds.textes.phrase1
        consigne2 = ds.textes.phrase2
        while (true) { // on évite A=B
          stor.coord[0] = j3pGetRandomInt(-5, 5)
          stor.coord[1] = j3pGetRandomInt(-5, 5)
          stor.coord[2] = j3pGetRandomInt(-5, 5)
          stor.coord[3] = j3pGetRandomInt(-5, 5)
          stor.coord[4] = j3pGetRandomInt(-5, 5)
          stor.coord[5] = j3pGetRandomInt(-5, 5)
          let nbNonNul = 0
          for (const i of [0, 1, 2]) {
            if (stor.coord[i] !== stor.coord[i + 3]) nbNonNul++
          }
          if (nbNonNul > 1) break
        }
        // stor.coord=[2,-3,-3,-4,-5,-3]
        stor.coord_vecteur[0] = stor.coord[3] - stor.coord[0]
        stor.coord_vecteur[1] = stor.coord[4] - stor.coord[1]
        stor.coord_vecteur[2] = stor.coord[5] - stor.coord[2]
        // second point :
        stor.coord2[0] = stor.coord[3]
        stor.coord2[1] = stor.coord[4]
        stor.coord2[2] = stor.coord[5]
    }
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.divEnonce, 'div')
    stor.divSysteme = j3pAddElt(stor.divEnonce, 'div', '', { style: me.styles.toutpetit.enonce })
    if (ds.type_droite[me.questionCourante - 1] === 'origine') {
      j3pAffiche(stor.zoneCons1, '', consigne1,
        {
          a: stor.coord_vecteur[0],
          b: stor.coord_vecteur[1],
          c: stor.coord_vecteur[2]
        })
    } else {
      j3pAffiche(stor.zoneCons1, '', consigne1,
        {
          a: stor.coord[0],
          b: stor.coord[1],
          c: stor.coord[2],
          d: stor.coord[3],
          e: stor.coord[4],
          f: stor.coord[5]
        })
    }
    j3pAffiche(stor.zoneCons2, '', consigne2, {})
    const elts = afficheSystemeEquations(stor.divSysteme, [
      { contenu: '$x=$&1&', options: { inputmq1: {} } },
      { contenu: '$y=$&2&', options: { inputmq2: {} } },
      { contenu: '$z=$&3&', options: { inputmq3: {} } }
    ])
    // console.log('elts', elts)
    stor.inputMqs = []
    for (const { inputmqList } of elts) {
      const inputMq = inputmqList[0]
      stor.inputMqs.push(inputMq)
      mqRestriction(inputMq, 't/\\d+-', { commandes: ['fraction'] })
    }
    j3pFocus(stor.inputMqs[0])
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '20px' }) })
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: stor.inputMqs
    })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      enonceMain()
      break // case "enonce":
    }

    case 'correction': {
      // On teste si une réponse a été saisie
      const repEleve1 = j3pValeurde(stor.inputMqs[0])
      const repEleve2 = j3pValeurde(stor.inputMqs[1])
      const repEleve3 = j3pValeurde(stor.inputMqs[2])
      // console.log(repEleve1+" et "+repEleve2+" et "+repEleve3);
      const rep = validation(repEleve1, repEleve2, repEleve3)
      if ((!rep.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        j3pFocus(rep.focus_a_donner)
        me.afficheBoutonValider()
      } else {
      // Une réponse a été saisie
      // Bonne réponse
        if (rep.bon) {
          me._stopTimer()
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          me.typederreurs[0]++
          me.cacheBoutonValider()
          // on affiche la correction détaillée quand même
          correctionDetaillee(true)
          for (let i = 0; i <= 2; i++) {
            stor.fctsValid.zones.bonneReponse[i] = true
            stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
          }
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
        // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          for (let i = 0; i <= 2; i++) {
            stor.fctsValid.zones.bonneReponse[i] = false
            stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
          }

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.cacheBoutonValider()
            correctionDetaillee(false)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            if (me.essaiCourant < ds.nbchances) {
              if (rep.juste === 'coord_point') {
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.correction5
              }
              if (rep.juste === 'coord_vecteur') {
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.correction6
              }
              stor.zoneCorr.innerHTML += '<br>' + ds.textes.phrase3
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
            // indication éventuelle ici
            } else {
            // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              if (rep.juste === 'coord_point') {
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.correction5
              }
              if (rep.juste === 'coord_vecteur') {
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.correction6
              }
              correctionDetaillee(false)
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
