import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pGetRandomElts, j3pElement, j3pEmpty, j3pGetNewId, j3pStyle } from 'src/legacy/core/functions'
import _3D from 'src/legacy/outils/3D/3D'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pCreeBoutonFenetre, j3pMasqueFenetre } from 'src/legacy/core/functionsJqDialog'
import { occurences, creerTableauDe, determinant, compareTabNum, creeTableauAleatoire, sommeAkBk, fonctionTab, tableauVrai, estEntier, sommeMonomes, vLatex, entierAlea } from 'src/legacy/outils/espace/fctsEspace'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Auteur Laurent Fréguin
    Date 22/10/2015
    Remarques savoir si 3 points sont alignés en utilisant la colinéarité de vecteurs
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['proba_alignes', 0.6, 'reel', 'probabilité d’obtenir 3 points alignés'],
    ['choix_correction', 2, 'entier', 'affiche 3 corrections possibles 1: u=k*v pour la colinéarité, 2: produits en croix, 3 : déterminant'],
    ['facteur_colinearite', [0.6, 0.4], 'array', '[proba_fractionnaire,proba_entier]']

  ]

  /*
    OU
    // dans le cas d’une section QUALITATIVE
    "pe":[
        {"pe_1":"toto"},
        {"pe_2":"tata"},
        {"pe_3":"titi"}
    ]
    */
}

/**
 * section Alignes
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (objSolution, exact) {
    if (exact) stor.zoneExpli.style.color = me.styles.cbien
    const u = objSolution.AB.nom
    const v = objSolution.AC.nom

    const A = objSolution.A.nom
    let a = objSolution.A.coord

    const B = objSolution.B.nom
    let b = objSolution.B.coord

    const C = objSolution.C.nom
    let c = objSolution.C.coord

    const x1 = objSolution.AB.coord[0]
    const y1 = objSolution.AB.coord[1]
    const z1 = objSolution.AB.coord[2]

    const x2 = objSolution.AC.coord[0]
    const y2 = objSolution.AC.coord[1]
    const z2 = objSolution.AC.coord[2]
    let xy
    let yx
    let yz
    let zy
    let xz
    let zx
    let det
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(stor.zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.phrase_correction_generale, { A, B, C, u, v })
    j3pAffiche(stor.zoneExpli2, '', ds.textes.phrase_correction_or, { u, a: x1, b: y1, c: z1, v, d: x2, e: y2, f: z2 })
    if (objSolution.alignes) {
      // Dans le cas où le facteur de colinéatiré est évident, ie entier ou inverse d’un entier, on ne prend pas en considération le choix de correc
      if (estEntier(objSolution.lambda) || estEntier(1 / objSolution.lambda)) {
        ds.choix_correction = 1
      }
      switch (ds.choix_correction) {
        case 1:
          {
            const complete = (estEntier(1 / objSolution.lambda))
              ? u + '=' + sommeMonomes([1 / objSolution.lambda], [v])
              : v + '=' + sommeMonomes([objSolution.lambda], [u])
            j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_col, { f: complete })
            j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_col, { u, v })
            j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_conclusion_al, { A, B, C })
          }
          break

        case 2:
          {
            xy = sommeAkBk([x1], [y2])
            yx = sommeAkBk([y1], [x2])
            a = 'x_{' + u + '} \\times y_{' + v + '}=' + xy.ecriture + '=' + xy.valeur
            b = 'y_{' + u + '}\\times x_{' + v + '}=' + yx.ecriture + '=' + yx.valeur
            c = 'x_{' + u + '} \\times y_{' + v + '}=' + 'y_{' + u + '}\\times x_{' + v + '}'

            yz = sommeAkBk([y1], [z2])
            zy = sommeAkBk([z1], [y2])
            const d = 'y_{' + u + '} \\times z_{' + v + '}=' + yz.ecriture + '=' + yz.valeur
            const e = 'z_{' + u + '}\\times y_{' + v + '}=' + zy.ecriture + '=' + zy.valeur
            const f = 'y_{' + u + '} \\times z_{' + v + '}=' + 'z_{' + u + '}\\times y_{' + v + '}'

            xz = sommeAkBk([x1], [z2])
            zx = sommeAkBk([z1], [x2])
            const g = 'x_{' + u + '} \\times z_{' + v + '}=' + xz.ecriture + '=' + xz.valeur
            const h = 'z_{' + u + '}\\times x_{' + v + '}=' + zx.ecriture + '=' + zx.valeur
            const i = 'x_{' + u + '} \\times z_{' + v + '}=' + 'z_{' + u + '}\\times x_{' + v + '}'

            j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_col_detail_alexis, {
              a,
              b,
              c,
              d,
              e,
              f,
              g,
              h,
              i
            })
            j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_col, { u, v })
            j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_conclusion_al, { A, B, C })
          }
          break

        case 3:
          {
            det = determinant([x1, y1], [x2, y2]).ecriture
            const det1 = 'x_{' + u + '} \\times y_{' + v + '}-y_{' + u + '}\\times x_{' + v + '}=' + det + '=0'

            det = determinant([y1, z1], [y2, z2]).ecriture
            const det2 = 'y_{' + u + '} \\times z_{' + v + '}-z_{' + u + '}\\times y_{' + v + '}=' + det + '=0'

            det = determinant([x1, z1], [x2, z2]).ecriture
            const det3 = 'x_{' + u + '} \\times z_{' + v + '}-z_{' + u + '}\\times x_{' + v + '}=' + det + '=0'

            j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_col_detail, {
              a: det1,
              b: det2,
              c: det3
            })
            j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_col, { u, v })
            j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_conclusion_al, {
              A,
              B,
              C
            })
          }
          break
      }
    } else {
      // Les points ne sont pas alignés.
      let detNonNul = ''
      det = 0
      let detCalcul = ''
      if (x1 * y2 - y1 * x2 !== 0) {
        // On distingue deux corrections possibles : celle d’Alexis et les autres.
        if (ds.choix_correction === 2) {
          xy = sommeAkBk([x1], [y2])
          yx = sommeAkBk([y1], [x2])
          a = 'x_{' + u + '} \\times y_{' + v + '}=' + xy.ecriture + '=' + xy.valeur
          b = 'y_{' + u + '}\\times x_{' + v + '}=' + yx.ecriture + '=' + yx.valeur
          c = 'x_{' + u + '} \\times y_{' + v + '}' + ' \\neq ' + 'y_{' + u + '}\\times x_{' + v + '}'
          j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col_alexis, { a, b, c })
        } else {
          det = x1 * y2 - y1 * x2
          detCalcul = determinant([x1, y1], [x2, y2]).ecriture
          detNonNul = 'x_{' + u + '} \\times y_{' + v + '}-y_{' + u + '}\\times x_{' + v + '}=' + detCalcul + '=' + det + ' \\neq 0'
          j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col, { a: detNonNul })
        }
      } else {
        if (y1 * z2 - z1 * y2 !== 0) {
          if (ds.choix_correction === 2) {
            yz = sommeAkBk([y1], [z2])
            zy = sommeAkBk([z1], [y2])
            a = 'y_{' + u + '} \\times z_{' + v + '}=' + yz.ecriture + '=' + yz.valeur
            b = 'z_{' + u + '}\\times y_{' + v + '}=' + zy.ecriture + '=' + zy.valeur
            c = 'y_{' + u + '} \\times z_{' + v + '} \\neq ' + 'z_{' + u + '}\\times y_{' + v + '}'
            j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col_alexis, { a, b, c })
          } else {
            det = y1 * z2 - z1 * y2
            detCalcul = determinant([y1, z1], [y2, z2]).ecriture
            detNonNul = 'y_{' + u + '} \\ times z_{' + v + '}-z_{' + u + '}\\times y_{' + v + '}' + detCalcul + '=' + det + ' \\neq 0'
            j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col, { a: detNonNul })
          }
        } else {
          if (ds.choix_correction === 2) {
            xz = sommeAkBk([x1], [z2])
            zx = sommeAkBk([z1], [x2])
            a = 'x_{' + u + '} \\times z_{' + v + '}=' + xz.ecriture + '=' + xz.valeur
            b = 'z_{' + u + '}\\times x_{' + v + '}=' + zx.ecriture + '=' + zx.valeur
            c = 'x_{' + u + '} \\times z_{' + v + '} \\neq ' + 'z_{' + u + '}\\times x_{' + v + '}'
            j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col_alexis, { a, b, c })
          } else {
            det = x1 * z2 - z1 * x2
            detCalcul = determinant([x1, z1], [x2, z2]).ecriture
            detNonNul = 'x_{' + u + '} \\times z_{' + v + '}-z_{' + u + '}\\times x_{' + v + '}' + detCalcul + '=' + det + ' \\neq 0'
            j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col, { a: detNonNul })
          }
        }
      }

      j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_non_col, { u, v })
      j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_conclusion_non_al, { A, B, C })
    }
    stor.zoom = 5
    a = objSolution.A.coord
    b = objSolution.B.coord
    c = objSolution.C.coord
    //
    // Création du plan (ABC)
    // On crée les sommets d’un parallélogramme centré en A:
    const lambda = 10
    const P = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]
    let mesVecteurs = [[x1, y1, z1], [x1, y1, z1], [x2, y2, z2], [x2, y2, z2]]
    if (objSolution.alignes) {
      mesVecteurs = [[x1, y1, z1], [x1, y1, z1], [0, x1 - y1, z1], [0, x1 - y1, z1]]
    }

    // var tab_vec=[0,0,1,1];
    for (let k = 0; k < 4; k++) {
      //            //var lequel=tab_vec[k];
      for (let m = 0; m < 3; m++) {
        P[k][m] = a[m] + Math.pow(-1, k) * lambda * mesVecteurs[k][m]
      }
    }
    //
    const tabObjetsATracer = [{ nom: A, type: 'point', coord: a, couleur: 'vert' }, { nom: B, type: 'point', coord: b, couleur: 'vert' }, { nom: C, type: 'point', coord: c, couleur: 'vert' }]
    tabObjetsATracer.push({ nom: 'u1', type: 'vecteur', ext1: A, ext2: B, pointilles: false, couleur: '#0000AA', visible: true })
    tabObjetsATracer.push({ nom: 'u2', type: 'vecteur', ext1: A, ext2: C, pointilles: false, couleur: '#0000AA', visible: true })
    const objets1 = {
      faces_cachees: false,
      dim: { larg: 600, haut: 300, zoom: stor.zoom },
      sommets: [[], P[0], P[2], P[1], P[3]],
      // sommets:[[],[1,1,1]],
      faces: [[1, 2, 3, 4]],
      // faces:[[1,1,1,1]],
      //                //nomsommets:["","a","b","c","d"],
      nomsommets: ['', 'a', 'b', 'c', 'd'],
      //                sommetsvisibles:["",false,false,false,false],
      // sommetsvisibles:["",false],
      mef: {
        typerotation: 'Ox',
        couleursommets: { defaut: '#F00', A: '#000' },
        couleurfaces: { defaut: '#92BBD8', f0: '#FF0000' }
      },
      //
      objets: tabObjetsATracer
      //
    }
    const div3D = j3pGetNewId('div3D')
    j3pAddElt(stor.fenetre3D, 'div', '', { id: div3D })
    stor.solide1 = new _3D(div3D, objets1) // _3D n’accepte qu’un id, pas d’elt html

    stor.solide1.rotation = { Ox: 0, Oy: 20, Oz: 30 }
    stor.solide1.arotation = stor.solide1.rotation
    stor.solide1.construit()
    j3pToggleFenetres(stor.Btns)
  }

  function entreBornes (x) {
    // renvoie true si abs(x)<=bornes_coordonnees
    return (Math.abs(x) <= stor.borneCoordonnees)
  }
  function getDonnees () {
    return {
      typesection: 'lycee', //

      nbrepetitions: 10,
      nbetapes: 1,
      indication: '',
      proba_alignes: 0.66,
      probas: [],
      choix_correction: 1,

      // Choix correction=1 donne correction minimaliste, avec simple affichage de l’égalité u=k*v ,k facteur de colinéarité.
      // Choix correction=2 donne correction avec égalité des produits en croix pour vérifier la colinéarité
      // Choix correction=3 donne correction avec calcul des déterminants pour vérifier la colinéarité
      facteur_colinearite: [1, 0], // 0.6 pour fractionnaire,0.4 pour entier
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      tab_fact_colinearite: [],
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      textes: {
        titre_exo: 'Points alignés ?<br>colinéarité de vecteurs',
        titre3D: 'Illustration dans l’espace',
        affichage_espace: 'Voir la figure',
        phrase_Enonce: 'Dans un repère de l’espace, on considère les points :<br>$£A(£a;£b;£c)$, $£B(£d;£e;£f)$ et $£C(£g;£h;£i)$.<br>Ces 3 points sont-ils alignés?',
        phrase_oui: 'oui',
        phrase_non: 'non',
        phrase_correction_generale: '$£A$, $£B$ et $£C$ sont alignés si et seulement si $£u$ et $£v$ sont colinéaires.',
        phrase_correction_or: 'Or $£u(£a;£b;£c)$ et $£v(£d;£e;£f)$.',
        phrase_correction_constat_col: 'On constate que $£f$',
        phrase_correction_constat_col_detail: 'On constate que : <br>$£a$ <br> $£b$ <br> $£c$ <br>',
        phrase_correction_constat_col_detail_alexis: 'On constate que : <br>$£a$  et  $£b$  donc  $£c$ <br> $£d$  et  $£e$  donc  $£f$ <br> $£g$  et  $£h$  donc  $£i$',
        phrase_correction_constat_non_col: 'On constate que :<br>$£a$',
        phrase_correction_constat_non_col_alexis: 'On constate que : <br>$£a$  et  $£b$  donc  $£c$',
        phrase_correction_conclusion_col: 'Ainsi, les vecteurs $£u$ et $£v$ sont colinéaires.',
        phrase_correction_conclusion_al: 'Donc les points $£A$, $£B$ et $£C$ sont alignés.',
        phrase_correction_conclusion_non_col: 'Ainsi, les vecteurs $£u$ et $£v$ ne sont pas colinéaires.',
        phrase_correction_conclusion_non_al: 'Donc les points $£A$, $£B$ et $£C$ ne sont pas alignés.'
      },
      pe: 0
    }
  }

  function enonceMain () {
    // On commence par générer deux point, et un facteur de colinéarité.
    // On souhaite tomber sur des coordonnées entières.
    // var proba_alignes=0.6;
    stor.borneCoordonnees = 12

    const borneFacteurColinearite = 30
    let alignes = true
    let facteurColinearite = 0

    let xA, yA, zA, coordA, xB, yB, zB, coordB, xC, yC, zC, coordC
    let coordCEntieres, coordEntreBornes
    if (stor.probas[me.questionCourante - 1]) {
      do {
        xA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordA = [xA, yA, zA]

        xB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordB = [xB, yB, zB]

        const u = [xB - xA, yB - yA, zB - zA]
        if (stor.tab_fact_colinearite[me.questionCourante - 1]) {
          // On le souhaite fractionnaire
          do {
            facteurColinearite = entierAlea(-borneFacteurColinearite, borneFacteurColinearite, [0, 6, -6]) / 6
          }
          while (estEntier(facteurColinearite) || estEntier(1 / facteurColinearite))
        } else {
          do {
            facteurColinearite = entierAlea(-borneFacteurColinearite, borneFacteurColinearite, [0, 6, -6]) / 6
          }
          while (!estEntier(facteurColinearite))
        }

        xC = xA + facteurColinearite * u[0]
        yC = yA + facteurColinearite * u[1]
        zC = zA + facteurColinearite * u[2]
        coordC = [xC, yC, zC]
        coordCEntieres = tableauVrai(fonctionTab(estEntier, [xC, yC, zC]))

        const toutesLesCoord = [xA, yA, zA, xB, yB, zB, xC, yC, zC]
        coordEntreBornes = tableauVrai(fonctionTab(entreBornes, toutesLesCoord))
      }
      while ((compareTabNum(coordA, coordB)) || (!coordCEntieres) || (!coordEntreBornes))
    } else {
      alignes = false
      do {
        xA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])

        xB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])

        xC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])

        coordA = [xA, yA, zA]
        coordB = [xB, yB, zB]
        coordC = [xC, yC, zC]
      }
      while ((compareTabNum(coordA, coordB)) || (compareTabNum(coordA, coordC)) || (compareTabNum(coordB, coordC)))
    }
    // On a terminé la gestion des coordonnées des points, alignés ou non, on passe à celle des noms
    const tabDesChoix = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'L', 'M', 'N', 'P', 'Q', 'R']
    const mesPoints = j3pGetRandomElts(tabDesChoix, 3)

    const A = {}
    A.nom = mesPoints[0]
    A.coord = [xA, yA, zA]

    const B = {}
    B.nom = mesPoints[1]
    B.coord = [xB, yB, zB]

    const C = {}
    C.nom = mesPoints[2]
    C.coord = [xC, yC, zC]

    const AB = {}
    AB.nom = vLatex(mesPoints[0] + mesPoints[1])
    AB.coord = [xB - xA, yB - yA, zB - zA]

    const AC = {}
    AC.nom = vLatex(mesPoints[0] + mesPoints[2])
    AC.coord = [xC - xA, yC - yA, zC - zA]

    stor.solution = {}
    stor.solution.A = A
    stor.solution.B = B
    stor.solution.C = C
    stor.solution.AB = AB
    stor.solution.AC = AC
    stor.solution.alignes = alignes
    stor.solution.lambda = facteurColinearite

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.phrase_Enonce, { A: A.nom, B: B.nom, C: C.nom, a: xA, b: yA, c: zA, d: xB, e: yB, f: zB, g: xC, h: yC, i: zC })
    j3pStyle(stor.zoneCons2, { paddingTop: '10px' })
    /// tentative bouton radio
    stor.idsRadio = [j3pGetNewId('radio1'), j3pGetNewId('radio2')]
    stor.nameRadio = j3pGetNewId('choix')
    j3pBoutonRadio(stor.zoneCons2, stor.idsRadio[0], stor.nameRadio, 0, 'oui')
    j3pBoutonRadio(stor.zoneCons2, stor.idsRadio[1], stor.nameRadio, 1, 'non')
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('toutpetit.explications', { paddingTop: '15px' }) })
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        ds.nbchances = 1
        if (ds.nbrepetitions > 0) {
          stor.probas = creeTableauAleatoire([true, false], [ds.proba_alignes, 1 - ds.proba_alignes], ds.nbrepetitions)
        }

        stor.tab_fact_colinearite = creerTableauDe(true, ds.nbrepetitions)
        // Dans le cas où il y a plus de deux alignements
        const occ = occurences(true, stor.probas)
        const remplace = creeTableauAleatoire([true, false], ds.facteur_colinearite, occ.nombre)
        for (let k = 0; k < occ.nombre; k++) {
          stor.tab_fact_colinearite[occ.emplacements[k]] = remplace[k]
        }
        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.75 })
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        // code de création des fenêtres
        stor.fenetreBtns = j3pGetNewId('fenetreBoutons')
        stor.fenetre3D = j3pGetNewId('fenetre3D')
        stor.la3D = j3pGetNewId('la3D')
        stor.Btns = j3pGetNewId('Btns')
        me.fenetresjq = [
          // Attention : positions par rapport à mepact
          { name: stor.Btns, title: ds.textes.titre3D, left: 660, top: 430, height: 120, id: stor.fenetreBtns },
          { name: stor.la3D, title: '3D', width: 730, top: 120, left: 90, height: 500, id: stor.fenetre3D }
        ]
        // Création mais sans affichage.
        j3pCreeFenetres(me)
        j3pCreeBoutonFenetre(stor.la3D, stor.fenetreBtns, { top: 10, left: 10 }, ds.textes.affichage_espace)
        j3pAfficheCroixFenetres(stor.la3D)
        // FIN BLOC1
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        j3pEmpty(stor.fenetre3D)
        j3pMasqueFenetre(stor.Btns)
        j3pMasqueFenetre(stor.la3D)
      }
      enonceMain()

      break // case "enonce":

    case 'correction':
      {
        let exact = true
        const objSol = stor.solution
        // On teste si une réponse a été saisie
        const aReponduQcm = (j3pElement(stor.idsRadio[0]).checked || j3pElement(stor.idsRadio[1]).checked)

        if (!aReponduQcm) {
          me.reponseManquante(stor.zoneCorr)
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
        // if (j3pElement("bill1").checked ==objSol.alignes) {
          if (j3pElement(stor.idsRadio[0]).checked === objSol.alignes) {
          // j3pElement("bill1").disabled=true;
          // j3pElement("bill2").disabled=true;
            j3pElement(stor.idsRadio[0]).disabled = true
            j3pElement(stor.idsRadio[1]).disabled = true
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien

            afficheCorrection(objSol, exact)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              } else {
              // Erreur au nème essai
                exact = false
                /// /j3pElement("bill1").disabled=true;
                // j3pElement("bill2").disabled=true;
                j3pElement(stor.idsRadio[0]).disabled = true
                j3pElement(stor.idsRadio[1]).disabled = true
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++
                if (objSol.alignes) {
                  j3pElement('label' + stor.idsRadio[1]).style.color = me.styles.cfaux
                  j3pBarre('label' + stor.idsRadio[1])
                } else {
                  j3pElement('label' + stor.idsRadio[0]).style.color = me.styles.cfaux
                  j3pBarre('label' + stor.idsRadio[0])
                }
                afficheCorrection(objSol, exact)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
