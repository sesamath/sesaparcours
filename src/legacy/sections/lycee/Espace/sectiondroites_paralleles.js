import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pGetRandomElts, j3pElement, j3pEmpty, j3pGetNewId } from 'src/legacy/core/functions'
import _3D from 'src/legacy/outils/3D/3D'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pCreeBoutonFenetre, j3pMasqueFenetre } from 'src/legacy/core/functionsJqDialog'
import { j3pAfficheSysteme } from 'src/legacy/core/functionsSysteme'
import { occurences, creerTableauDe, produitVectoriel, determinant, determinant3D, UcolV, compareTabNum, creeTableauAleatoire, sommeAkBk, fonctionTab, tableauVrai, fraction, estEntier, sommeMonomes, vLatex, entierAlea } from 'src/legacy/outils/espace/fctsEspace'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Auteur Laurent Fréguin
    Date 27/12/2015
    Tester si deux droites sont paralleles
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['choix_correction', 2, 'entier', 'affiche 3 corrections possibles 1: u=k*v pour la colinéarité, 2: produits en croix, 3 : déterminant'],
    ['facteur_colinearite', [0.6, 0.4], 'array', '[proba_fractionnaire,proba_entier]'],
    ['proba_paralleles', 0.6, 'reel', 'proba d’avoir deux droites parallèles'],
    ['forme', [0.34, 0.33, 0.33], 'array', 'chaque dte donnée par deux points,2 pts et param,param-param'],
    ['proba_secantes', 0.5, 'reel', 'proba sécantes vs non coplanaire'],
    ['proba_strict_paralleles', 0.8, 'reel', 'proba stric parallèles vs confondues'],
    ['permute_eq_param', 0.25, 'reel', 'ecrire b*t+a à la place de a+b*t']

  ]

  /*
    OU
    // dans le cas d’une section QUALITATIVE
    "pe":[
        {"pe_1":"toto"},
        {"pe_2":"tata"},
        {"pe_3":"titi"}
    ]
    */
}

/**
 * section droites_paralleles
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function entreBornes (x) {
    // renvoie true si abs(x)<=bornes_coordonnees
    return (Math.abs(x) <= stor.borneCoordonnees)
  }
  function afficheCorrection (objSolution, exact) {
    if (exact) stor.zoneExpli.style.color = me.styles.cbien
    const A = objSolution.A
    const B = objSolution.B
    const C = objSolution.C
    const D = objSolution.D

    const u = objSolution.u
    const v = objSolution.v
    const d1 = objSolution.d1
    const d2 = objSolution.d2
    let x1 = objSolution.u.coord[0]
    let y1 = objSolution.u.coord[1]
    let z1 = objSolution.u.coord[2]

    let x2 = objSolution.v.coord[0]
    let y2 = objSolution.v.coord[1]
    let z2 = objSolution.v.coord[2]

    const paralleles = objSolution.paralleles
    for (let i = 1; i <= 6; i++) stor['zoneExpli' + i] = j3pAddElt(stor.zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.phrase_correction_dirige, { E: d1, F: d2, u: u.nom, v: v.nom, a: u.coord[0], b: u.coord[1], c: u.coord[2], d: v.coord[0], e: v.coord[1], f: v.coord[2] })
    j3pAffiche(stor.zoneExpli2, '', ds.textes.phrase_correction_regle, { E: d1, F: d2, u: u.nom, v: v.nom })
    let xy, yx, a, b, c, d, e, f, g, h, i, yz, zy, xz, zx, det, det1, det2, det3
    if (paralleles) {
      // On gère les différentes corrections possibles
      let choixCorrection = ds.choix_correction
      if (estEntier(objSolution.lambda) || estEntier(1 / objSolution.lambda)) {
        choixCorrection = 1
      }

      switch (choixCorrection) {
        case 1:
          j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_colinearite, { p: v.nom + '=' + sommeMonomes([objSolution.lambda], [u.nom]) })
          j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_col, { u: u.nom, v: v.nom })
          j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_paralleles, { E: d1, F: d2 })
          j3pAffiche(stor.zoneExpli6, '', ds.textes.phrase_correction_precision, { P: objSolution.precision })
          break

        case 2:
          xy = sommeAkBk([x1], [y2])
          yx = sommeAkBk([y1], [x2])
          a = 'x_{' + u.nom + '} \\times y_{' + v.nom + '}=' + xy.ecriture + '=' + xy.valeur
          b = 'y_{' + u.nom + '}\\times x_{' + v.nom + '}=' + yx.ecriture + '=' + yx.valeur
          c = 'x_{' + u.nom + '} \\times y_{' + v.nom + '}=' + 'y_{' + u.nom + '}\\times x_{' + v.nom + '}'// var det1="x_{"+u+"} \\times y_{"+v+"}-y_{"+u+"}\\times x_{"+v+"}="+det+"=0";

          yz = sommeAkBk([y1], [z2])
          zy = sommeAkBk([z1], [y2])
          d = 'y_{' + u.nom + '} \\times z_{' + v.nom + '}=' + yz.ecriture + '=' + yz.valeur
          e = 'z_{' + u.nom + '}\\times y_{' + v.nom + '}=' + zy.ecriture + '=' + zy.valeur
          f = 'y_{' + u.nom + '} \\times z_{' + v.nom + '}=' + 'z_{' + u.nom + '}\\times y_{' + v.nom + '}'

          xz = sommeAkBk([x1], [z2])
          zx = sommeAkBk([z1], [x2])
          g = 'x_{' + u.nom + '} \\times z_{' + v.nom + '}=' + xz.ecriture + '=' + xz.valeur
          h = 'z_{' + u.nom + '}\\times x_{' + v.nom + '}=' + zx.ecriture + '=' + zx.valeur
          i = 'x_{' + u.nom + '} \\times z_{' + v.nom + '}=' + 'z_{' + u.nom + '}\\times x_{' + v.nom + '}'

          j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_col_detail_alexis, { a, b, c, d, e, f, g, h, i })
          j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_col, { u: u.nom, v: v.nom })
          j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_paralleles, { E: d1, F: d2 })
          j3pAffiche(stor.zoneExpli6, '', ds.textes.phrase_correction_precision, { P: objSolution.precision })
          break

        case 3:
          det = determinant([x1, y1], [x2, y2]).ecriture
          det1 = 'x_{' + u.nom + '} \\times y_{' + v.nom + '}-y_{' + u.nom + '}\\times x_{' + v.nom + '}=' + det + '=0'

          det = determinant([y1, z1], [y2, z2]).ecriture
          det2 = 'y_{' + u.nom + '} \\times z_{' + v.nom + '}-z_{' + u.nom + '}\\times y_{' + v.nom + '}=' + det + '=0'

          det = determinant([x1, z1], [x2, z2]).ecriture
          det3 = 'x_{' + u.nom + '} \\times z_{' + v.nom + '}-z_{' + u.nom + '}\\times x_{' + v.nom + '}=' + det + '=0'

          j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_col_detail, { a: det1, b: det2, c: det3 })
          j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_col, { u: u.nom, v: v.nom })
          j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_paralleles, { E: d1, F: d2 })
          j3pAffiche(stor.zoneExpli6, '', ds.textes.phrase_correction_precision, { P: objSolution.precision })
          break
      }
    } else {
      x1 = u.coord[0]
      y1 = u.coord[1]
      z1 = u.coord[2]

      x2 = v.coord[0]
      y2 = v.coord[1]
      z2 = v.coord[2]
      let detNonNul, detCalcul
      switch (ds.choix_correction) {
        case 1:
        case 3:
          det = 0
          detCalcul = ''
          if (x1 * y2 - y1 * x2 !== 0) {
            det = x1 * y2 - y1 * x2
            detCalcul = determinant([x1, y1], [x2, y2]).ecriture

            detNonNul = 'x_{' + u.nom + '} \\times y_{' + v.nom + '}-y_{' + u.nom + '}\\times x_{' + v.nom + '}=' + detCalcul + '=' + det + ' \\neq 0'
          } else {
            if (y1 * z2 - z1 * y2 !== 0) {
              det = y1 * z2 - z1 * y2
              detCalcul = determinant([y1, z1], [y2, z2]).ecriture
              detNonNul = 'y_{' + u.nom + '} \\ times z_{' + v.nom + '}-z_{' + u.nom + '}\\times y_{' + v.nom + '}' + detCalcul + '=' + det + ' \\neq 0'
            } else {
              det = x1 * z2 - z1 * x2
              detCalcul = determinant([x1, z1], [x2, z2]).ecriture
              detNonNul = 'x_{' + u.nom + '} \\times z_{' + v.nom + '}-z_{' + u.nom + '}\\times x_{' + v.nom + '}' + detCalcul + '=' + det + ' \\neq 0'
            }
          }
          j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_colinearite, { p: detNonNul })
          break

        case 2:
          if (x1 * y2 - y1 * x2 !== 0) {
            xy = sommeAkBk([x1], [y2])
            yx = sommeAkBk([y1], [x2])
            a = 'x_{' + u.nom + '} \\times y_{' + v.nom + '}=' + xy.ecriture + '=' + xy.valeur
            b = 'y_{' + u.nom + '}\\times x_{' + v.nom + '}=' + yx.ecriture + '=' + yx.valeur
            c = 'x_{' + u.nom + '} \\times y_{' + v.nom + '}' + ' \\neq ' + 'y_{' + u.nom + '}\\times x_{' + v.nom + '}'
            j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col_alexis, { a, b, c })
          } else {
            if (y1 * z2 - z1 * y2 !== 0) {
              yz = sommeAkBk([y1], [z2])
              zy = sommeAkBk([z1], [y2])
              a = 'y_{' + u + '} \\times z_{' + v + '}=' + yz.ecriture + '=' + yz.valeur
              b = 'z_{' + u + '}\\times y_{' + v + '}=' + zy.ecriture + '=' + zy.valeur
              c = 'y_{' + u + '} \\times z_{' + v + '} \\neq ' + 'z_{' + u + '}\\times y_{' + v + '}'
              j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col_alexis, { a, b, c })
            } else {
              det = y1 * z2 - z1 * y2
              detCalcul = determinant([y1, z1], [y2, z2]).ecriture
              detNonNul = 'y_{' + u + '} \\ times z_{' + v + '}-z_{' + u + '}\\times y_{' + v + '}' + detCalcul + '=' + det + ' \\neq 0'
              j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col, { a: detNonNul })
            }
          }
          break
      }

      j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_non_col, { u: u.nom, v: v.nom })
      j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_non_paralleles, { E: d1, F: d2 })
      j3pAffiche(stor.zoneExpli6, '', ds.textes.phrase_correction_precision, { P: objSolution.precision })
    }
    stor.zoom = -5
    // Création du plan (ABC)
    // On crée les sommets d’un parallélogramme centré en A:
    const lambda = 10
    const P = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]
    const vAB = u.coord
    const vAC = (objSolution.precision !== 'confondues')
      ? [C.coord[0] - A.coord[0], C.coord[1] - A.coord[1], C.coord[2] - A.coord[2]]
      : produitVectoriel(vAB, [10, 10, 10])
    const mesVecteurs = [vAB, vAB, vAC, vAC]

    for (let k = 0; k < 4; k++) {
      for (let m = 0; m < 3; m++) {
        P[k][m] = A.coord[m] + Math.pow(-1, k) * lambda * mesVecteurs[k][m]
      }
    }
    let affiche1 = true
    let affiche2 = true
    if (objSolution.cas === '1param') {
      affiche2 = false
    }
    if (objSolution.cas === '2param') {
      affiche1 = false
      affiche2 = false
    }

    const tabObjetsATracer = [{ nom: A.nom, type: 'point', coord: A.coord, couleur: 'vert', visible: affiche1 }]
    tabObjetsATracer.push({ nom: B.nom, type: 'point', coord: B.coord, couleur: 'vert', visible: affiche1 })
    tabObjetsATracer.push({ nom: C.nom, type: 'point', coord: C.coord, couleur: 'vert', visible: affiche2 })
    tabObjetsATracer.push({ nom: D.nom, type: 'point', coord: D.coord, couleur: 'vert', visible: affiche2 })
    tabObjetsATracer.push({ nom: 'AB', type: 'droite', ext1: A.nom, ext2: B.nom, couleur: '#0000AA', pointilles: false, visible: true })
    tabObjetsATracer.push({ nom: 'CD', type: 'droite', ext1: C.nom, ext2: D.nom, couleur: '#0000AA', pointilles: false, visible: true })
    // On rajoute les vecteurs
    tabObjetsATracer.push({ nom: 'u', type: 'vecteur', ext1: A.nom, ext2: B.nom, couleur: '#00FF00', pointilles: false, visible: true })
    tabObjetsATracer.push({ nom: 'v', type: 'vecteur', ext1: C.nom, ext2: D.nom, couleur: '#CCFF00', pointilles: false, visible: true })

    const objets1 = {
      faces_cachees: false,
      dim: { larg: 600, haut: 300, zoom: stor.zoom },
      sommets: [[], P[0], P[2], P[1], P[3]],
      //
      faces: [[1, 2, 3, 4]],
      //
      nomsommets: ['', 'a', 'b', 'c', 'd'],

      /// /
      mef: {
        typerotation: 'Ox',
        couleursommets: { defaut: '#F00', A: '#000' },
        couleurfaces: { defaut: '#92BBD8', f0: '#FF0000' }
      },
      /// ///
      objets: tabObjetsATracer
      /// ///
    }
    const div3D = j3pGetNewId('div3D')
    j3pAddElt(stor.fenetre3D, 'div', '', { id: div3D })
    stor.solide1 = new _3D(div3D, objets1)
    /// /
    stor.solide1.rotation = { Ox: 0, Oy: 20, Oz: 30 }
    stor.solide1.arotation = stor.solide1.rotation
    stor.solide1.construit()
    j3pToggleFenetres(stor.Btns)
  }

  function getDonnees () {
    return {
      typesection: 'lycee', //

      nbrepetitions: 4,
      nbetapes: 1,
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      proba_paralleles: 0.66,
      // forme=0.5;//Autant de chance de donner la droite par deux points que par sa représentation paramétrique
      forme: [0.34, 0.33, 0.33], // tableau de probas [les deux droites sont données par 2 point, 1 dte par deux points et l’autre  param, 2 param
      proba_secantes: 0.5,
      proba_strict_paralleles: 0.8,
      permute_eq_param: 0.25,
      choix_correction: 2,
      facteur_colinearite: [0.5, 0.5],
      // Choix correction=1 donne correction minimaliste, avec simple affichage de l’égalité u=k*v ,k facteur de colinéarité.
      // Choix correction=2 donne correction avec égalité des produits en croix pour vérifier la colinéarité
      // Choix correction=3 donne correction avec calcul des déterminants pour vérifier la colinéarité
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      textes: {
        titre_exo: 'Droites paralleles',
        titre3D: 'Illustration dans l’espace',
        affichage_espace: 'Voir la figure',
        phrase_enonce_points: 'Dans un repère de l’espace, on considère les points :<br> $£A(£a;£b;£c)$, $£B(£d;£e;£f)$ , $£C(£g;£h;£i)$ et $£D(£j;£k;£l)$ <br> Les droites $£E$ et $£F$ sont-elles paralleles?',
        phrase_enonce_param: 'Dans un repère orthonormé de l’espace, on considère les droites $d_1$ et $d_2$ d’équations paramétriques respectives ‡1‡, $£a \\in \\R$ et ‡2‡, $£b \\in \\R$',
        phrase_enonce_mixte: 'Dans un repère orthonormé de l’espace, on considère :<br>les points  $£A(£a;£b;£c)$, $£B(£d;£e;£f)$ et la droite $d$ d’équation paramétrique ‡1‡, $£g \\in \\R$',
        phrase_enonce_mixte2: 'Les droites $£E$ et $d$ sont-elles paralleles?',
        phrase_enonce_param2: 'Les droites $d_1$ et $d_2$ sont-elles paralleles?',
        phrase_oui: 'oui',
        phrase_non: 'non',
        phrase_correction_dirige: '$£E$ est dirigée par $£u(£a;£b;£c)$ et $£F$ est dirigée par $£v(£d;£e;£f)$.<br>',
        phrase_correction_regle: '$£E$ et $£F$ sont paralleles si et seulement si $£u$ et $£v$ sont colinéaires.',
        phrase_correction_colinearite: 'On constate que $£p$',
        phrase_correction_constat_col_detail: 'On constate que : <br>$£a$ <br> $£b$ <br> $£c$ <br>',
        phrase_correction_constat_col_detail_alexis: 'On constate que : <br>$£a$  et  $£b$  donc  $£c$ <br> $£d$  et  $£e$  donc  $£f$ <br> $£g$  et  $£h$  donc  $£i$',
        phrase_correction_conclusion_col: 'Ainsi, les vecteurs $£u$ et $£v$ sont colinéaires.',
        phrase_correction_paralleles: 'Donc les droites $£E$ et $£F$ sont paralleles',
        phrase_correction_constat_non_col_alexis: 'On constate que : <br>$£a$  et  $£b$  donc  $£c$',
        phrase_correction_conclusion_non_col: 'Ainsi, les vecteurs $£u$ et $£v$ ne sont pas colinéaires.',
        phrase_correction_non_paralleles: 'Donc les droites $£E$ et $£F$ ne sont pas paralleles',
        phrase_correction_precision: '(Ici, elles sont £P.)'
      },
      pe: 0
    }
  }

  function enonceMain () {
    // On souhaite tomber sur des coordonnées entières.
    stor.borneCoordonnees = 10
    let paralleles = true
    let xA, yA, zA, xB, yB, zB, xC, yC, zC, xU, yU, zU, xV, yV, zV, xD, yD, zD, xW, yW, zW
    let coordA, coordB, coordC, coordU, coordV, coordD, coordW
    let facteurColinearite, coordDEntieres, coordEntreBornes, precision, vAC, coordEntieres
    if (stor.tabProbaParalleles[me.questionCourante - 1]) {
      facteurColinearite = 0
      const borneFacteurColinearite = 30

      if (stor.tabStrictOuConf[me.questionCourante - 1]) {
        // les droites sont strictement parallèles
        // On crée 3 points A,B,C non aligés, puis D par C+facteurColinearite*AB
        do {
          xA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          yA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          zA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          coordA = [xA, yA, zA]

          xB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          yB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          zB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          coordB = [xB, yB, zB]

          xC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          yC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          zC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          coordC = [xC, yC, zC]
          vAC = [xC - xA, yC - yA, zC - zA]

          xU = xB - xA
          yU = yB - yA
          zU = zB - zA
          coordU = [xU, yU, zU]
          if (stor.tabFactColinearite[me.questionCourante - 1]) {
            // On le souhaite fractionnaire
            do {
              facteurColinearite = entierAlea(-borneFacteurColinearite, borneFacteurColinearite, [0, 6, -6]) / 6
            }
            while (estEntier(facteurColinearite) || estEntier(1 / facteurColinearite))
          } else {
            do {
              facteurColinearite = entierAlea(-borneFacteurColinearite, borneFacteurColinearite, [0, 6, -6]) / 6
            }
            while (!estEntier(facteurColinearite))
          }

          xD = xC + facteurColinearite * xU
          yD = yC + facteurColinearite * yU
          zD = zC + facteurColinearite * zU

          coordD = [xD, yD, zD]
          coordDEntieres = tableauVrai(fonctionTab(estEntier, [xD, yD, zD]))
          coordEntreBornes = tableauVrai(fonctionTab(entreBornes, coordD))

          xV = xD - xC
          yV = yD - yC
          zV = zD - zC
          coordV = [xV, yV, zV]
        } while (compareTabNum(coordA, coordB) || compareTabNum(coordA, coordC) || compareTabNum(coordA, coordD) || compareTabNum(coordB, coordC) || compareTabNum(coordB, coordD) || compareTabNum(coordC, coordD) || (!coordDEntieres) || (!coordEntreBornes) || UcolV(coordU, vAC))
        precision = 'strictement parallèles'
      } else {
        // Elles sont confondues
        // On commence par générer un point A :
        do {
          xA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          yA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          zA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          coordA = [xA, yA, zA]

          if (stor.tabFactColinearite[me.questionCourante - 1]) {
            // On génére un facteur de colinéarité non entier et d’inverse non entier
            do {
              facteurColinearite = entierAlea(-borneFacteurColinearite, borneFacteurColinearite, [0, 6, -6]) / 6
            }
            while (estEntier(facteurColinearite) || estEntier(1 / facteurColinearite))
          } else {
            do {
              facteurColinearite = entierAlea(-borneFacteurColinearite, borneFacteurColinearite, [0, 6, -6]) / 6
            }
            while (!estEntier(facteurColinearite))
          }

          const leNum = fraction(facteurColinearite).num
          const leDen = fraction(facteurColinearite).denom

          let i, j, k
          do {
            i = entierAlea(-10, 10, [0])
            j = entierAlea(-10, 10, [0])
            k = entierAlea(-10, 10, [0])
          }
          while (i === 0 && j === 0 && k === 0)

          xU = leDen * i
          yU = leDen * j
          zU = leDen * k
          coordU = [xU, yU, zU]

          xB = xA + xU
          yB = yA + yU
          zB = zA + zU
          coordB = [xB, yB, zB]

          // On choisit à présent un point C sur la droites (AB):
          const t1 = entierAlea(-10, 10, [0, 2]) / 2
          xC = xA + t1 * xU
          yC = yA + t1 * yU
          zC = zA + t1 * zU
          coordC = [xC, yC, zC]

          xD = xC + leNum * i
          yD = yC + leNum * j
          zD = zC + leNum * k
          coordD = [xD, yD, zD]
          // var vrai=tableauVrai(fonctionTab(estEntier,[xC,yC,zC,xD,yD,zD]))

          let aTester = coordB
          aTester = aTester.concat(coordC)
          aTester = aTester.concat(coordD)

          coordEntreBornes = tableauVrai(fonctionTab(entreBornes, aTester))
          coordEntieres = tableauVrai(fonctionTab(estEntier, aTester))
        }
        while (compareTabNum(coordA, coordD) || compareTabNum(coordB, coordD) || !coordEntreBornes || !coordEntieres)

        xV = xD - xC
        yV = yD - yC
        zV = zD - zC
        coordV = [xV, yV, zV]
        precision = 'confondues'
      }
    } else {
      // On crée 4 points A,B,C,D distincts tels que AB et CD ne soient pas colinéaires.
      // On aura deux cas : soit droites sécantes, soit non coplanaires
      paralleles = false

      do {
        xA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordA = [xA, yA, zA]

        xB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordB = [xB, yB, zB]

        xU = xB - xA
        yU = yB - yA
        zU = zB - zA
        coordU = [xU, yU, zU]
      } while (compareTabNum(coordA, coordB))

      // la construction de C et D dépend du choix de stor.tab_proba_secantes

      if (!stor.tab_proba_secantes[me.questionCourante - 1]) {
        // On veut deux droites non coplanaires
        do {
          xC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          yC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          zC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          coordC = [xC, yC, zC]

          xD = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          yD = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          zD = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          coordD = [xD, yD, zD]

          xV = xD - xC
          yV = yD - yC
          zV = zD - zC
          coordV = [xV, yV, zV]

          xW = xD - xA
          yW = yD - yA
          zW = zD - zA
          coordW = [xW, yW, zW]
          // on traduit A,B,C,D coplanaires par det(AB,CD,AD)=0
        }
        while (determinant3D(coordU, coordV, coordW) === 0 || compareTabNum(coordA, coordC) || compareTabNum(coordA, coordD) || compareTabNum(coordB, coordC) || compareTabNum(coordB, coordD))
        precision = 'non coplanaires'
      } else {
        // On veut (AB) et (CD) sécantes.
        // On choisit un point E distinct de A et B sur la droite (AB), puis un point C non alignés avec A et B.
        // D est enfin choisi distinct de C au hasard sur la droite (EC).
        do {
          const t1 = entierAlea(-10, 10, [0, 2]) / 2
          const xE = xA + t1 * xU
          const yE = yA + t1 * yU
          const zE = zA + t1 * zU

          xC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          yC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          zC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
          coordC = [xC, yC, zC]

          xW = xE - xC
          yW = yE - yC
          zW = zE - zC
          coordW = [xW, yW, zW]

          const t2 = entierAlea(-10, 10, [0]) / 2
          xD = xC + t2 * xW
          yD = yC + t2 * yW
          zD = zC + t2 * zW

          coordD = [xD, yD, zD]
          coordDEntieres = tableauVrai(fonctionTab(estEntier, [xD, yD, zD]))
          coordEntreBornes = tableauVrai(fonctionTab(entreBornes, coordD))

          xV = xD - xC
          yV = yD - yC
          zV = zD - zC
          coordV = [xV, yV, zV]
        }
        while (UcolV(coordU, coordV) || compareTabNum(coordA, coordC) || compareTabNum(coordA, coordD) || compareTabNum(coordB, coordC) || compareTabNum(coordB, coordD) || !coordDEntieres || !coordEntreBornes)
        precision = 'sécantes'
      }
    }

    const tabDesChoix = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'L', 'M', 'N', 'R']
    const mesPoints = j3pGetRandomElts(tabDesChoix, 4)

    const tabParametresPossibles = ['a', 'b', 'c', 'k', 'm', 'p', 'q', 's', 't', 'u', 'v']
    const parametre = j3pGetRandomElts(tabParametresPossibles, 2)
    const param1 = parametre[0]
    const param2 = parametre[1]

    const A = {}
    A.nom = mesPoints[0]
    A.coord = [xA, yA, zA]

    const B = {}
    B.nom = mesPoints[1]
    B.coord = [xB, yB, zB]

    const C = {}
    C.nom = mesPoints[2]
    C.coord = [xC, yC, zC]

    const D = {}
    D.nom = mesPoints[3]
    D.coord = [xD, yD, zD]

    const AB = {}
    AB.nom = vLatex(mesPoints[0] + mesPoints[1])
    AB.coord = coordU

    const u = {}
    u.nom = '\\vec{u}'
    u.coord = coordU

    const CD = {}
    CD.nom = vLatex(mesPoints[2] + mesPoints[3])
    CD.coord = coordV

    const v = {}
    v.nom = '\\vec{v}'
    v.coord = coordV

    const d1 = '(' + mesPoints[0] + mesPoints[1] + ')'
    const d2 = '(' + mesPoints[2] + mesPoints[3] + ')'

    const permut = 1 - ds.permute_eq_param

    const x1 = (Math.random() < permut)
      ? sommeMonomes([xA, xU], ['', param1])
      : sommeMonomes([xU, xA], [param1, ''])
    const y1 = (Math.random() < permut)
      ? sommeMonomes([yA, yU], ['', param1])
      : sommeMonomes([yU, yA], [param1, ''])
    const z1 = (Math.random() < permut)
      ? sommeMonomes([zA, zU], ['', param1])
      : sommeMonomes([zU, zA], [param1, ''])
    const x2 = (Math.random() < permut)
      ? sommeMonomes([xC, xV], ['', param2])
      : sommeMonomes([xV, xC], [param2, ''])
    const y2 = (Math.random() < permut)
      ? sommeMonomes([yC, yV], ['', param2])
      : sommeMonomes([yV, yC], [param2, ''])
    const z2 = (Math.random() < permut)
      ? sommeMonomes([zC, zV], ['', param2])
      : sommeMonomes([zV, zC], [param2, ''])

    stor.solution = {}
    stor.solution.A = A
    stor.solution.B = B
    stor.solution.C = C
    stor.solution.D = D
    stor.solution.AB = AB
    stor.solution.CD = CD
    stor.solution.u = u
    stor.solution.v = v
    stor.solution.lambda = facteurColinearite
    stor.solution.paralleles = paralleles
    stor.solution.d1 = d1
    stor.solution.d2 = d2
    stor.solution.parametre = parametre
    stor.solution.precision = precision

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (stor.probasForme[me.questionCourante - 1] === '0param') {
      // dte donnée par deux points
      stor.solution.cas = '0param'
      stor.solution.d1 = '(' + mesPoints[0] + mesPoints[1] + ')'
      stor.solution.d2 = '(' + mesPoints[2] + mesPoints[3] + ')'
      stor.solution.u.nom = AB.nom
      stor.solution.v.nom = CD.nom
      j3pAffiche(stor.zoneCons1, '', ds.textes.phrase_enonce_points, {
        A: A.nom,
        B: B.nom,
        C: C.nom,
        D: D.nom,
        a: xA,
        b: yA,
        c: zA,
        d: xB,
        e: yB,
        f: zB,
        g: xC,
        h: yC,
        i: zC,
        j: xD,
        k: yD,
        l: zD,
        E: d1,
        F: d2
      })
    } else {
      // droites données par équations paramétriques
      if (stor.probasForme[me.questionCourante - 1] === '1param') {
        stor.solution.cas = '1param'
        stor.solution.d2 = 'd'
        stor.solution.u.nom = AB.nom
        stor.solution.v.nom = '\\vec{u}'
        j3pAfficheSysteme(stor.zoneCons1, '', ds.textes.phrase_enonce_mixte, [['$x=' + x2 + '$', '$y=' + y2 + '$', '$z=' + z2 + '$']], {
          g: param2,
          A: A.nom,
          B: B.nom,
          a: xA,
          b: yA,
          c: zA,
          d: xB,
          e: yB,
          f: zB
        }, me.styles.toutpetit.enonce.color)
        j3pAffiche(stor.zoneCons2, '', ds.textes.phrase_enonce_mixte2, { E: d1 })
      } else {
        stor.solution.cas = '2param'
        stor.solution.d1 = 'd_1'
        stor.solution.d2 = 'd_2'
        j3pAfficheSysteme(stor.zoneCons1, '', ds.textes.phrase_enonce_param, [['$x=' + x1 + '$', '$y=' + y1 + '$', '$z=' + z1 + '$'], ['$x=' + x2 + '$', '$y=' + y2 + '$', '$z=' + z2 + '$']], {
          a: param1,
          b: param2
        }, me.styles.toutpetit.enonce.color)
        j3pAffiche(stor.zoneCons2, '', ds.textes.phrase_enonce_param2)
      }
    }
    // bouton radio
    stor.idsRadio = [j3pGetNewId('radio1'), j3pGetNewId('radio2')]
    stor.nameRadio = j3pGetNewId('choix')
    j3pBoutonRadio(stor.zoneCons3, stor.idsRadio[0], stor.nameRadio, 0, ' oui ')
    j3pBoutonRadio(stor.zoneCons3, stor.idsRadio[1], stor.nameRadio, 1, ' non ')

    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('toutpetit.explications', { paddingTop: '15px' }) })

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        ds.nbchances = 1
        if (ds.nbrepetitions > 1) {
          stor.tabProbaParalleles = creeTableauAleatoire([true, false], [ds.proba_paralleles, 1 - ds.proba_paralleles], ds.nbrepetitions)
          stor.probasForme = creeTableauAleatoire(['0param', '1param', '2param'], ds.forme, ds.nbrepetitions)
        }
        // Si il y a plusieurs cas de parallélisme, on fait en sorte d’avoir des facteurs de colinéarité entiers et fractionnaires.
        stor.tabFactColinearite = creerTableauDe(true, ds.nbrepetitions)

        let occ = occurences(true, stor.tabProbaParalleles)
        let remplace = creeTableauAleatoire([true, false], ds.facteur_colinearite, occ.nombre)
        for (let k = 0; k < occ.nombre; k++) {
          stor.tabFactColinearite[occ.emplacements[k]] = remplace[k]
        }

        // Si il y a plusieurs cas de parallélisme, on fait en sorte d’avoir des confondues et des strictes parallèles
        stor.tabStrictOuConf = creerTableauDe(true, ds.nbrepetitions)
        occ = occurences(true, stor.tabProbaParalleles)
        remplace = creeTableauAleatoire([true, false], [ds.proba_strict_paralleles, 1 - ds.proba_strict_paralleles], occ.nombre)
        for (let k = 0; k < occ.nombre; k++) {
          stor.tabStrictOuConf[occ.emplacements[k]] = remplace[k]
        }

        // Si il y a plusieurs cas non parallélisme, on fait en sorte d’avoir sécantes et non coplanaire
        stor.tab_proba_secantes = creerTableauDe(true, ds.nbrepetitions)
        occ = occurences(false, stor.tabProbaParalleles)
        remplace = creeTableauAleatoire([true, false], [ds.proba_secantes, 1 - ds.proba_secantes], occ.nombre)
        for (let k = 0; k < occ.nombre; k++) {
          stor.tab_proba_secantes[occ.emplacements[k]] = remplace[k]
        }
        // Construction de la page
        me.construitStructurePage(ds.structure)

        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // code de création des fenêtres
        // Creation automatique si le parametre tableau video est non vide
        stor.fenetreBtns = j3pGetNewId('fenetreBoutons')
        stor.fenetre3D = j3pGetNewId('fenetre3D')
        stor.la3D = j3pGetNewId('la3D')
        stor.Btns = j3pGetNewId('Btns')
        me.fenetresjq = [
          // Attention : positions par rapport à mepact
          { name: stor.Btns, title: ds.textes.titre3D, left: 660, top: 430, height: 120, id: stor.fenetreBtns },
          { name: stor.la3D, title: '3D', width: 730, top: 120, left: 90, height: 500, id: stor.fenetre3D }
        ]
        // Création mais sans affichage.
        j3pCreeFenetres(me)
        j3pCreeBoutonFenetre(stor.la3D, stor.fenetreBtns, { top: 10, left: 10 }, ds.textes.affichage_espace)
        j3pAfficheCroixFenetres(stor.la3D)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        j3pEmpty(stor.fenetre3D)
        j3pMasqueFenetre(stor.Btns)
        j3pMasqueFenetre(stor.la3D)
      }
      enonceMain()

      break // case "enonce":
    }

    case 'correction': {
      let exact = true
      const objSol = stor.solution
      // On teste si une réponse a été saisie
      let aReponduQcm = false
      // if (j3pElement("bill1").checked||j3pElement("bill2").checked){
      if (j3pElement(stor.idsRadio[0]).checked || j3pElement(stor.idsRadio[1]).checked) {
        aReponduQcm = true
      }

      if (!aReponduQcm) {
        me.reponseManquante(stor.zoneCorr)
        me.afficheBoutonValider()
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        // if (j3pElement("bill1").checked ==objSol.alignes) {
        if (j3pElement(stor.idsRadio[0]).checked === objSol.paralleles) {
          // j3pElement("bill1").disabled=true;
          // j3pElement("bill2").disabled=true;
          j3pElement(stor.idsRadio[0]).disabled = true
          j3pElement(stor.idsRadio[1]).disabled = true
          me._stopTimer()
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien

          afficheCorrection(objSol, exact)
          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()

            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux

            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
            } else {
              // Erreur au nème essai
              exact = false
              j3pElement(stor.idsRadio[0]).disabled = true
              j3pElement(stor.idsRadio[1]).disabled = true
              me._stopTimer()
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              me.typederreurs[2]++
              if (objSol.paralleles) {
                j3pBarre('label' + stor.idsRadio[1])
              } else {
                j3pBarre('label' + stor.idsRadio[0])
              }
              afficheCorrection(objSol, exact)
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
