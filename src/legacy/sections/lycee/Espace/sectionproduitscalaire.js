import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pShuffle, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pVirgule, j3pEmpty, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        août 2020
        Calcul du produit scalaire de deux vecteurs de l’espace dont on donne les coordonnées (dans un RON)
        Elle peut être prolongée par le calcul de l’angle
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuestion', 'angle', 'liste', 'On peut présenter cette section sous trois formes&nbsp;:<br/>-calcul d’un produit scalaire de deux vecteurs&nbsp;;<br/>-calcul de la norme d’un vecteur&nbsp;;<br/>- calcul d’un angle orienté (précédé d’un produit scalaire et de la norme des vecteurs).', ['produit scalaire', 'norme', 'angle']],
    ['seuilReussite', 0.7, 'reel', 'Pourcentage à partir duquel on considère que l’exercice est correctement réussi.'],
    ['seuilEchec', 0.7, 'reel', 'Pourcentage à partir duquel on identifie un type d’erreur pour renvoyer une phrase d’état (pe) particulière.']
  ],
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'problème avec le produit scalaire' },
    { pe_3: 'problème avec la norme' },
    { pe_4: 'problème avec la mesure de l’angle' },
    { pe_5: 'Insuffisant' },
    { pe_6: 'problème avec le produit scalaire et les normes' },
    { pe_7: 'problème avec le produit scalaire et l’angle' },
    { pe_8: 'problème avec les normes et ’angle' }
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo1: 'Produit scalaire dans l’espace',
  titre_exo2: 'Angle orienté de vecteurs de l’espace',
  titre_exo3: 'Norme d’un vecteur de l’espace',
  // on donne les phrases de la consigne
  consigne1: 'Dans un repère orthonormé $£r$, on définit les vecteurs $\\vec{£u}\\left(£{cu}\\right)$ et $\\vec{£v}\\left(£{cv}\\right)$.',
  consigne2_1: 'On a obtenu $\\vec{£u}\\cdot \\vec{£v}=£{prodscal}$',
  consigne2_2: 'De plus $\\left|\\left|\\vec{£u}\\right|\\right|=$&1& et $\\left|\\left|\\vec{£v}\\right|\\right|=$&2&.',
  consigne1bis: 'Dans un repère orthonormé $£r$, on définit le vecteur $\\vec{£u}\\left(£{cu}\\right)$.',
  consigne2bis: 'La norme de ce vecteur $\\vec{£u}$ vaut :',
  consigne3_1: 'On a obtenu $\\vec{£u}\\cdot \\vec{£v}=£{prodscal}$, $\\left|\\left|\\vec{£u}\\right|\\right|=£{nu}$ et $\\left|\\left|\\vec{£v}\\right|\\right|=£{nv}$.',
  consigne3_2: 'Donner une valeur de l’angle orienté $\\left(\\vec{£u};\\vec{£v}\\right)$ en degrés arrondi à 0,1.',
  indic: 'Donner le résultat sous forme réduite.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Le calcul est bon mais la réponse n’est pas réduite !',
  comment2: 'Les calculs sont bons mais les réponses doivent être réduites !',
  comment3: 'N’y auraut-il pas un problème d’arrondi&nbsp;?',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Rappel de cours&nbsp;:',
  corr1_2: 'Dans un repère orthonormé, le produit scalaire de deux vecteurs $\\vec{u}(x, y, z)$ et $\\vec{v}(x\', y\', z\')$ vaut :<br/>$\\vec{u}\\cdot \\vec{v}=xx\'+yy\'+zz\'$.',
  corr1_3: 'Ainsi $\\vec{£u}\\cdot \\vec{£v}=£c=£{prodscal}$.',
  corr2_2: 'Dans un repère orthonormé, la norme d’un vecteur $\\vec{u}(x, y, z)$ vaut :<br/>$\\left|\\left|\\vec{u}\\right|\\right|=\\sqrt{x^2+y^2+z^2}$.',
  corr2_3: 'Ainsi $\\left|\\left|\\vec{£u}\\right|\\right|=£{calcNormeU}=£{nu}$£{point}',
  corr2_4: 'et $\\left|\\left|\\vec{£v}\\right|\\right|=£{calcNormeV}=£{nv}$.',
  corr3_2: 'Pour tous vecteurs $\\vec{u}$ et $\\vec{v}$ de l’espace, on a l’égalité $\\vec{u}\\cdot\\vec{v}=\\left|\\left|\\vec{u}\\right|\\right|\\times \\left|\\left|\\vec{v}\\right|\\right|\\times \\cos\\left(\\vec{u};\\vec{v}\\right)$ ce qui équivaut à $\\cos\\left(\\vec{u};\\vec{v}\\right)=\\frac{\\vec{u}\\cdot\\vec{v}}{\\left|\\left|\\vec{u}\\right|\\right|\\times \\left|\\left|\\vec{v}\\right|\\right|}$.',
  corr3_3: 'Ainsi $\\cos\\left(\\vec{£u};\\vec{£v}\\right)=\\frac{£{prodscal}}{£{nu}\\times £{nv}}$ et en utilisant la touche $\\cos^{-1}$ ou arccos de la calculatrice, on obtient $\\left(\\vec{£u};\\vec{£v}\\right)\\approx £{angleArrondi}$°.'
}
/**
 * section produitscalaire
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAALgAAACVAAAAQEAAAAAAAAAAQAAAAb#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAE1AAAAAUAUAAAAAAAAAAAAAgD#####AAFiAAIxMAAAAAFAJAAAAAAAAAAAAAIA#####wADc29sAAlhKnNxcnQoYin#####AAAAAQAKQ09wZXJhdGlvbgL#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAAf####8AAAACAAlDRm9uY3Rpb24BAAAABAAAAAIAAAACAP####8AA3JlcAAKc3FydCgxMCkqNQAAAAMCAAAABQEAAAABQCQAAAAAAAAAAAABQBQAAAAAAAD#####AAAAAwAQQ1Rlc3RFcXVpdmFsZW5jZQD#####AAdlZ2FsaXRlAAAAAwAAAAQBAAAAAAE#8AAAAAAAAAH###############8='
  // stor.cListeObjets affecté au chargement de mtg, c’est un CListeObjets

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let i
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    if (stor.laQuestion.indexOf('norme') > -1) j3pDetruit(stor.zoneCons6)
    // explications contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    for (i = 1; i <= 2; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div', '')
    const leCours = []
    for (i = 1; i <= 2; i++) leCours.push(j3pAddElt(stor.zoneexplication1, 'div', ''))
    j3pAffiche(leCours[0], '', textes.corr1_1)
    leCours[0].style.textDecoration = 'underline'
    if (stor.laQuestion === 'produit scalaire') {
      j3pAffiche(leCours[1], '', textes.corr1_2)
      j3pAffiche(stor.zoneexplication2, '', textes.corr1_3, stor.objCons)
    } else if (stor.laQuestion.indexOf('norme') > -1) {
      stor.objCons.point = (stor.laQuestion === 'normes') ? '' : '.'
      j3pAffiche(leCours[1], '', textes.corr2_2)
      j3pAffiche(stor.zoneexplication2, '', textes.corr2_3, stor.objCons)
      stor.zoneexplication3 = j3pAddElt(explications, 'div', '')
      if (stor.laQuestion === 'normes') j3pAffiche(stor.zoneexplication3, '', textes.corr2_4, stor.objCons)
    } else {
      // l’angle
      j3pAffiche(leCours[1], '', textes.corr3_2)
      j3pAffiche(stor.zoneexplication2, '', textes.corr3_3, stor.objCons)
    }
    stor.zoneexplication1.style.border = 'thick double #32a1ce'
    stor.zoneexplication1.style.borderRadius = '7px'
    stor.zoneexplication2.style.paddingTop = '10px'
  }
  function ajoutParentheses (nb) {
    // pour les nombres négatifs présents dans un calcul, on ajoute des parenthèses
    if (nb < 0) {
      return '(' + j3pVirgule(nb) + ')'
    } else {
      return j3pVirgule(nb)
    }
  }
  function simplifieRadical (nb) {
    // Cette fonction renvoie \\sqrt{nb} sous forme réduite
    let diviseur = 2
    let copieNb = nb
    let radicalextrait = 1
    while (Math.pow(diviseur, 2) <= copieNb) {
      if (Math.abs(copieNb / Math.pow(diviseur, 2) - Math.round(copieNb / Math.pow(diviseur, 2))) < Math.pow(10, -12)) {
        radicalextrait *= diviseur
        copieNb /= Math.pow(diviseur, 2)
      } else {
        diviseur += 1
      }
    }
    return {
      expression: ((radicalextrait === 1) ? '' : radicalextrait) + ((copieNb === 1) ? '' : '\\sqrt{' + copieNb + '}'),
      a: radicalextrait,
      b: copieNb
    }
  }
  function ecoute (num) {
    if (stor.zoneInputMq[num].className.includes('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, stor.zoneInputMq[num], {
        liste: ['fraction', 'racine']
      })
    }
  }
  function genereDonnees () {
    const obj = {}
    obj.r = '\\left(O;\\vec{i}, \\vec{j}, \\vec{k}\\right)'
    let tabnomVect = ['u', 'v', 'w']
    tabnomVect = j3pShuffle(tabnomVect)
    obj.u = tabnomVect[0]
    obj.v = tabnomVect[1]
    let pbNbZero, pbProdScal, vecteursDifferents
    do {
      obj.coordu = []
      obj.coordv = []
      obj.normeU = 0
      obj.normeV = 0
      obj.prodscal = 0
      let nbzeroU = 0
      let nbzeroV = 0
      for (let i = 0; i < 3; i++) {
        obj.coordu.push(j3pGetRandomInt(-4, 4))
        obj.coordv.push(j3pGetRandomInt(-4, 4))
        obj.prodscal += obj.coordu[i] * obj.coordv[i]
        obj.normeU += Math.pow(obj.coordu[i], 2)
        obj.normeV += Math.pow(obj.coordv[i], 2)
        if (obj.coordu[i] === 0) nbzeroU++
        if (obj.coordv[i] === 0) nbzeroV++
      }
      pbNbZero = (nbzeroU > 1) || (nbzeroV > 1)
      pbProdScal = (stor.typeQuestion === 'angle') ? (Math.abs(obj.prodscal) < Math.pow(10, -12)) : false
      vecteursDifferents = obj.coordu.some((elt, index) => elt !== obj.coordv[index])
    } while ((Math.abs(obj.prodscal) > 40 || pbProdScal || pbNbZero || !vecteursDifferents))
    obj.cu = obj.coordu[0] + ';' + obj.coordu[1] + ';' + obj.coordu[2]
    obj.cv = obj.coordv[0] + ';' + obj.coordv[1] + ';' + obj.coordv[2]
    obj.radicalU = simplifieRadical(obj.normeU)
    obj.radicalV = simplifieRadical(obj.normeV)
    obj.nu = obj.radicalU.expression
    obj.nv = obj.radicalV.expression
    obj.normeU = Math.sqrt(obj.normeU)
    obj.normeV = Math.sqrt(obj.normeV)
    // calcul du produit scalaire
    obj.c = obj.coordu[0] + '\\times ' + ajoutParentheses(obj.coordv[0]) + '+' + ajoutParentheses(obj.coordu[1]) + '\\times ' + ajoutParentheses(obj.coordv[1]) + '+' + ajoutParentheses(obj.coordu[2]) + '\\times ' + ajoutParentheses(obj.coordv[2])
    obj.calcNormeU = '\\sqrt{' + ajoutParentheses(obj.coordu[0]) + '^2+' + ajoutParentheses(obj.coordu[1]) + '^2+' + ajoutParentheses(obj.coordu[2]) + '^2}'
    obj.calcNormeV = '\\sqrt{' + ajoutParentheses(obj.coordv[0]) + '^2+' + ajoutParentheses(obj.coordv[1]) + '^2+' + ajoutParentheses(obj.coordv[2]) + '^2}'
    obj.formuleCos = '\\vec{' + obj.u + '}\\cdot \\vec{' + obj.v + '}=\\|\\vec{' + obj.u + '}\\|\\times \\|\\vec{' + obj.v + '}\\|\\times \\cos\\left(\\vec{' + obj.u + '};\\vec{' + obj.v + '}\\right)'
    obj.formuleCos2 = '\\cos\\left(\\vec{' + obj.u + '};\\vec{' + obj.v + '}\\right)=\\frac{\\vec{' + obj.u + '}\\cdot \\vec{' + obj.v + '}}{\\|\\vec{' + obj.u + '}\\|\\times \\|\\vec{' + obj.v + '}}'
    obj.angle = Math.acos(obj.prodscal / (obj.normeU * obj.normeV)) * 180 / Math.PI
    obj.angleArrondi = Math.round(obj.angle * 10) / 10
    return obj
  }

  function enonceInitFirst () {
    me.surcharge({ nbetapes: (ds.typeQuestion === 'angle') ? 3 : 1 })
    // Construction de la page
    stor.pourcentage = 0.7
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: stor.pourcentage })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const leTitre = (ds.prodScalSeulement) ? textes.titre_exo1 : textes.titre_exo2
    me.afficheTitre(leTitre)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.cListeObjets = mtgAppLecteur.createList(txtFigure)
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    let mesZonesSaisie
    stor.laQuestion = (ds.typeQuestion === 'angle')
      ? (me.questionCourante % ds.nbetapes === 1)
          ? 'produit scalaire'
          : (me.questionCourante % ds.nbetapes === 2) ? 'normes' : 'angle'
      : ds.typeQuestion
    if (ds.nbetapes === 1 || me.questionCourante % ds.nbetapes === 1) stor.objCons = genereDonnees()

    me.logIfDebug('stor.objCons:', stor.objCons)
    stor.zoneInputMq = []
    if (stor.laQuestion === 'produit scalaire') {
      // On demande de calculer le produit scalaire
      j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objCons)
      const elt = j3pAffiche(stor.zoneCons3, '', '$\\vec{' + stor.objCons.u + '}\\cdot \\vec{' + stor.objCons.v + '}=$&1&', {
        inputmq1: { texte: '' }
      })
      stor.zoneInputMq.push(elt.inputmqList[0])
      mqRestriction(elt.inputmqList[0], '\\d-,.+*()')
      elt.inputmqList[0].solutionSimplifiee = ['non valide', 'non valide']
      elt.inputmqList[0].typeReponse = ['nombre', 'exact']
      elt.inputmqList[0].reponse = [stor.objCons.prodscal]
      mesZonesSaisie = [elt.inputmqList[0].id]
      j3pAffiche(stor.zoneCons4, '', textes.indic, { style: { fontSize: '0.9em', fontStyle: 'italic' } })
      stor.typeQuestion = 'produit scalaire'
    } else if (stor.laQuestion === 'normes') {
      // On demande les normes des deux vecteurs
      j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objCons)
      j3pAffiche(stor.zoneCons2, '', textes.consigne2_1, stor.objCons)
      const elt = j3pAffiche(stor.zoneCons4, '', textes.consigne2_2, {
        u: stor.objCons.u,
        v: stor.objCons.v,
        inputmq1: { texte: '' },
        inputmq2: { texte: '' }
      })
      stor.zoneInputMq.push(elt.inputmqList[0], elt.inputmqList[1])
      elt.inputmqList[0].typeReponse = ['nombre', 'exact']
      elt.inputmqList[1].typeReponse = ['nombre', 'exact']
      mesZonesSaisie = stor.zoneInputMq.map(elt => elt.id)
      for (let i = 0; i < mesZonesSaisie.length; i++) mqRestriction(mesZonesSaisie[i], '\\d\\-,.+*()', { commandes: ['fraction', 'racine'] })
      stor.zoneCons6.style.paddingTop = '10px'
      stor.laPalette = j3pAddElt(stor.zoneCons6, 'div')
      $(stor.zoneInputMq[0]).focusin(ecoute.bind(null, 0))
      $(stor.zoneInputMq[1]).focusin(ecoute.bind(null, 1))
      stor.zoneInputMq[0].reponse = [stor.objCons.normeU]
      stor.zoneInputMq[1].reponse = [stor.objCons.normeV]
      j3pAffiche(stor.zoneCons5, '', ' ' + textes.indic, { style: { fontSize: '0.9em', fontStyle: 'italic' } })
      stor.tentative = 0
      ecoute(0)
    } else if (stor.laQuestion === 'norme') {
      // On demande la norme d’un vecteur
      j3pAffiche(stor.zoneCons1, '', textes.consigne1bis, stor.objCons)
      j3pAffiche(stor.zoneCons2, '', textes.consigne2bis, stor.objCons)
      const elt = j3pAffiche(stor.zoneCons4, '', '$\\left|\\left|\\vec{' + stor.objCons.u + '}\\right|\\right|=$&1&', {
        inputmq1: { texte: '' }
      })
      stor.zoneInputMq.push(elt.inputmqList[0])
      mqRestriction(stor.zoneInputMq[0], '\\d-,.+*()', { commandes: ['fraction', 'racine'] })
      stor.zoneInputMq[0].typeReponse = ['nombre', 'exact']
      stor.zoneInputMq[0].reponse = [stor.objCons.normeU]
      mesZonesSaisie = [stor.zoneInputMq[0].id]
      stor.zoneCons6.style.paddingTop = '10px'
      stor.laPalette = j3pAddElt(stor.zoneCons6, 'div')
      ecoute(0)
      stor.tentative = 0
    } else {
      // on demande la mesure de l’angle
      j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objCons)
      j3pAffiche(stor.zoneCons2, '', textes.consigne3_1, stor.objCons)
      j3pAffiche(stor.zoneCons3, '', textes.consigne3_2, stor.objCons)
      const elt = j3pAffiche(stor.zoneCons4, '', '$\\left(\\vec{£u};\\vec{£v}\\right)\\approx$&1&°.', {
        u: stor.objCons.u,
        v: stor.objCons.v,
        inputmq1: { texte: '' }
      })
      stor.zoneInputMq.push(elt.inputmqList[0])
      mqRestriction(stor.zoneInputMq[0], '\\d-,.')
      mesZonesSaisie = [stor.zoneInputMq[0].id]
      stor.zoneInputMq[0].typeReponse = ['nombre', 'arrondi', 0.1]
      stor.zoneInputMq[0].reponse = [stor.objCons.angle]
    }
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    const zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCorr = j3pAddElt(zoneMD, 'div', '')
    stor.zoneCalc = j3pAddElt(zoneMD, 'div', '')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, j3pToggleFenetres.bind(null, 'Calculatrice'), { id: 'Calculatrice', className: 'MepBoutons', value: 'Calculatrice' })
    j3pAfficheCroixFenetres('Calculatrice')
    j3pFocus(mesZonesSaisie[0])
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })
    j3pFocus(mesZonesSaisie[0])
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        let reponse = {}
        let j
        let repEleve = []
        const bonneRepNonReduite = []
        let bonnesRepNonReduites = true
        let calculCorrect = false // utile pour identifier un calcul correct mais non simplifié
        let reponseNonReduite = false
        let pbArrondi = false
        if (stor.laQuestion.includes('norme')) {
          reponse.aRepondu = stor.fctsValid.valideReponses()
          reponse.bonneReponse = true
          // On vérifie si la réponse est égale à celle attendue
          for (j = 0; j < stor.zoneInputMq.length; j++) {
            repEleve.push(j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[j]))
            // On vérifie si les réponses sont bonnes (mais non réduites)
            stor.fctsValid.zones.bonneReponse[j] = (Math.abs(repEleve[j] - stor.zoneInputMq[j].reponse[0]) < Math.pow(10, -10))
            bonneRepNonReduite.push(stor.fctsValid.zones.bonneReponse[j])
            // Pour l’instant on vérifie juste que le calcul donne bien le résultat attendue
            // Il faut encore vérifier qu’il est réduit si la réponse est bien la bonne
            me.logIfDebug('bonneRepNonReduite:', bonneRepNonReduite)
            if (bonneRepNonReduite[j]) {
              const repSaisie = j3pMathquillXcas(stor.fctsValid.zones.reponseSaisie[j])
              const estNombre = /^(-?[0-9]+\.?[0-9]*)$/g.test(repSaisie)
              if (!estNombre) {
                const comparaison = (j === 0) ? stor.objCons.radicalU : stor.objCons.radicalV
                stor.cListeObjets.giveFormula2('a', String(comparaison.a))
                stor.cListeObjets.giveFormula2('b', String(comparaison.b))
                stor.cListeObjets.giveFormula2('rep', repSaisie)
                me.logIfDebug('comparaison:', comparaison)
                stor.cListeObjets.calculateNG(false)
                stor.fctsValid.zones.bonneReponse[j] = (stor.cListeObjets.valueOf('egalite') === 1)
                reponse.bonneReponse = reponse.bonneReponse && (stor.cListeObjets.valueOf('egalite') === 1)
                me.logIfDebug('stor.fctsValid.zones.bonneReponse[j]:', stor.fctsValid.zones.bonneReponse[j])
              }
            }
            bonnesRepNonReduites = (bonnesRepNonReduites || bonneRepNonReduite[j])
          }
          if (reponse.bonneReponse) {
            reponse = stor.fctsValid.validationGlobale()
            me.logIfDebug('reponse après vérif:', reponse)
          } else {
            reponseNonReduite = bonnesRepNonReduites
            me.logIfDebug('reponseNonReduite:', reponseNonReduite, 'tentative:', stor.tentative)
            if (reponseNonReduite && (stor.tentative === 0)) {
              me.essaiCourant--
            } else {
              for (j = 0; j < stor.fctsValid.zones.inputs.length; j++) stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[j])
            }
            if (stor.tentative === 0) {
              j3pDetruit(me.zoneRep)
              me.zoneRep = j3pAddElt(stor.zoneCons3, 'div', '')
              if (stor.laQuestion === 'norme') {
                j3pAffiche(me.zoneRep, '', '$\\left|\\left|\\vec{' + stor.objCons.u + '}\\right|\\right|=\\textcolor{' + (bonneRepNonReduite[0] ? String(me.styles.cbien) : String(me.styles.cfaux)) + '}{' + stor.fctsValid.zones.reponseSaisie[0].replace(/\*/g, '\\times ') + '}$')
              } else {
                j3pAffiche(me.zoneRep, '', '$\\left|\\left|\\vec{' + stor.objCons.u + '}\\right|\\right|=\\textcolor{' + (bonneRepNonReduite[0] ? String(me.styles.cbien) : String(me.styles.cfaux)) + '}{' + stor.fctsValid.zones.reponseSaisie[0].replace(/\*/g, '\\times ') + '}$ ; $\\left|\\left|\\vec{' + stor.objCons.v + '}\\right|\\right|=\\textcolor{' + (bonneRepNonReduite[1] ? String(me.styles.cbien) : String(me.styles.cfaux)) + '}{' + stor.fctsValid.zones.reponseSaisie[1].replace(/\*/g, '\\times ') + '}$')
              }
              if (bonnesRepNonReduites) stor.tentative = 1
            }
          }
        } else {
          reponse = stor.fctsValid.validationGlobale()
          if (stor.laQuestion === 'produit scalaire') {
            reponseNonReduite = !stor.fctsValid.zones.reponseSimplifiee[0][0]
            me.logIfDebug('reponseNonReduite après validation globale : ', reponseNonReduite)
            if (reponseNonReduite) {
            // réponse non réduite
              repEleve = j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[0])
              // On vérifie si les réponses sont bonnes (mais non réduites)
              bonnesRepNonReduites = (Math.abs(repEleve - stor.zoneInputMq[0].reponse[0]) < Math.pow(10, -10))
              if (bonnesRepNonReduites) {
                j3pAffiche(stor.zoneCons2, '', '$\\vec{' + stor.objCons.u + '}\\cdot\\vec{' + stor.objCons.v + '}=\\textcolor{' + String(me.styles.cbien) + '}{' + stor.fctsValid.zones.reponseSaisie[0].replace(/\*/g, '\\times ') + '}$')
                // for (j = 0; j < stor.fctsValid.zones.inputs.length; j++) {
                stor.zoneInputMq[0].solutionSimplifiee = ['reponse fausse', 'reponse fausse']
                stor.zoneInputMq[0].value = ''
                calculCorrect = true
              } else {
              // c’est faux
                calculCorrect = false
                reponse.aRepondu = true
                reponse.bonneReponse = false
                stor.fctsValid.zones.bonneReponse[0] = false
                stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
              }
            }
          }
        }
        if (stor.laQuestion === 'angle') {
          pbArrondi = (Math.abs(stor.zoneInputMq[0].reponse[0] - stor.fctsValid.zones.reponseSaisie[0]) < 1)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (calculCorrect) {
            msgReponseManquante = (stor.fctsValid.zones.inputs.length === 1) ? textes.comment1 : textes.comment2
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if ((calculCorrect && stor.laQuestion === 'produit scalaire') || (reponseNonReduite && (stor.laQuestion.indexOf('norme') > -1))) {
                stor.zoneCorr.innerHTML += '<br/>' + ((stor.fctsValid.zones.inputs.length === 1) ? textes.comment1 : textes.comment2)
              } else if (pbArrondi) {
                stor.zoneCorr.innerHTML += '<br/>' + textes.comment3
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                if (stor.laQuestion === 'produit scalaire') {
                  me.typederreurs[3]++
                } else if (stor.laQuestion.indexOf('norme') > -1) {
                  me.typederreurs[4]++
                } else { // erreur sur l’angle
                  me.typederreurs[5]++
                }
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentageReussite = me.score / ds.nbitems
        me.parcours.pe = ''
        if (pourcentageReussite >= ds.seuilReussite) {
          me.parcours.pe = ds.pe_1
        } else if (ds.typeQuestion === 'produit scalaire') {
          me.parcours.pe = ds.pe_2
        } else if (ds.typeQuestion === 'norme') {
          me.parcours.pe = ds.pe_3
        } else {
          me.logIfDebug('me.typederreurs:', me.typederreurs, pourcentageReussite)
          // Là c’est plus compliqué car dans ce type de question, il y a 3 questions consécutives
          const pbProdScal = ds.seuilEchec < me.typederreurs[3] / ds.nbrepetitions
          const pbNormes = ds.seuilEchec < me.typederreurs[4] / ds.nbrepetitions
          const pbAngle = ds.seuilEchec < me.typederreurs[5] / ds.nbrepetitions
          if (pbProdScal) {
            if (pbNormes) {
              if (pbAngle) {
                me.parcours.pe = ds.pe_5
              } else {
                me.parcours.pe = ds.pe_6
              }
            } else {
              if (pbAngle) {
                me.parcours.pe = ds.pe_7
              } else {
                me.parcours.pe = ds.pe_2
              }
            }
          } else {
            if (pbNormes) {
              if (pbAngle) {
                me.parcours.pe = ds.pe_8
              } else {
                me.parcours.pe = ds.pe_3
              }
            } else {
              if (pbAngle) {
                me.parcours.pe = ds.pe_4
              }
            }
          }
        }
        if (me.parcours.pe === '') me.parcours.pe = ds.pe_5
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
