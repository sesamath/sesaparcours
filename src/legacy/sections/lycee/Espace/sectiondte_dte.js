import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pGetRandomElts, j3pElement, j3pEmpty, j3pFocus, j3pMathquillXcas, j3pPaletteMathquill, j3pRemplace, j3pValeurde, j3pGetNewId, j3pStyle } from 'src/legacy/core/functions'
import _3D from 'src/legacy/outils/3D/3D'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pCreeBoutonFenetre, j3pMasqueFenetre } from 'src/legacy/core/functionsJqDialog'
import { j3pAfficheSysteme } from 'src/legacy/core/functionsSysteme'
import { occurences, creerTableauDe, produitVectoriel, determinant, determinant3D, compareTabNum, pgcdZarb, creeTableauAleatoire, sommeAkBk, fonctionTab, tableauVrai, fraction, estEntier, sommeMonomes, entierAlea, vLatex, UcolV } from 'src/legacy/outils/espace/fctsEspace'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, tempsDepasse } = textesGeneriques

/*
    Auteur Laurent Fréguin
    Date 27/12/2015
    etudier la position relative de deux droites
    Corrigé par Yves ES-LINT 20/04/2020
    Bugfixes de Rémi le 28/04/2020
    3D HS virée par Daniel le 28/04/2020
*/

/*
  *
  * @type type
  *         this.position=[0.1,0.2,0.4,0.3]// confondues, strict paralleles,sécantes, non cop
        this.facteurColinearite=[0.67,0.33]//En cas de colinéarité des vecteurs directeurs, le facteur est fractionnaire vs ent
        this.permute_eq_param=0.25;
        this.choixCorrection=1;
        this.systeme_simple=[0.5,0.5];//systeme simple ssi l’un au moins des coeffs est nul.//la première donnée est la proba de systeme simple et l’autre de systeme sans coeffs nuls

  */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['permute_eq_param', 0.25, 'reel', 'écrire b*t+a au lieu de a+b*t'],
    ['position', [0.1, 0.2, 0.4, 0.3], 'array', 'Probabilités des réponses confondues, strictement parallèles, sécantes ou non coplanaires'],
    ['facteurColinearite', [0.67, 0.33], 'array', 'En cas de colinéarité des vecteurs directeurs, le facteur est fractionnaire vs ent'],
    ['choixCorrection', 1, 'entier', 'Pour étudier la colinéarité, 1 (directement u =k*v), 2 (test des produits en croix) et 3 (déterminant)'],
    ['systeme_simple', [0.5, 0.5], 'array', 'Systeme simple ssi l’un au moins des coeffs est nul. La première donnée est la proba de systeme simple et l’autre de systeme sans coeffs nuls']

  ],
  // dans le cas d’une section QUANTITATIVE

  // OU
  // dans le cas d’une section QUALITATIVE

  pe: [
    { pe_1: 'bien' }, // si score
    { pe_2: 'pas bien' },
    { pe_3: 'pb_detection_parallelisme' },
    // Ligne suivante corrigée par Yves
    // pe_4: 'resol_systeme'
    { pe_4: 'pb_resol_systeme' }
  ]
}

/**
 * section dte_dte
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  // listeners
  function ecoute (num) {
    if (stor.laPalette) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput[num], { liste: ['fraction'] })
    }
  }

  function afficherPointInter () {
    if (stor.btnsActifs) {
      // on masque d’abord
      stor.mesZonesSaisie = []
      j3pEmpty(stor.zoneCons4)
      const radioSecantes = j3pElement(stor.idsRadio[2])
      if (radioSecantes && radioSecantes.checked) {
        const elt = j3pAffiche(stor.zoneCons4, '', ds.textes.pointInter, {
          inputmq1: {},
          inputmq2: {},
          inputmq3: {}
        })
        stor.zoneInput = elt.inputmqList.map(elt => elt)
        stor.laPalette = j3pAddElt(stor.zoneCons4, 'div')
        j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], { liste: ['fraction'] })
        for (let i = 1; i <= 3; i++) {
          mqRestriction(stor.zoneInput[i - 1], '\\d(+-)*,.', {
            commandes: ['fraction']
          })
          stor.zoneInput[i - 1].addEventListener('focusin', ecoute.bind(null, i - 1))
        }
        j3pFocus(stor.zoneInput[0], true)
        stor.mesZonesSaisie = stor.zoneInput.map(elt => elt.id)

        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: stor.mesZonesSaisie
        })
      }
    }
  }
  // fin listeners

  function substitution2 (expression, tabDesVariablesARemplacer, tabDesValeurs) {
    // expression est une chaine
    // on remplace les variables de tabDesVariablesARemplacer par les valeurs qui correspondent.
    // Il vaut mieux avoir préformatée la chaine avec sommeMonomes
    function lettreAvantOuApres (maChaine, n) {
      // sert à savoir sil le caractère n°n de maChaine est suivi ou précédé d’une lettre.
      return ((maChaine.charCodeAt(n - 1) >= 97 && maChaine.charCodeAt(n - 1) <= 122) || (maChaine.charCodeAt(n + 1) >= 97 && maChaine.charCodeAt(n + 1) <= 122))
    }

    let stock = expression
    for (let k = 0; k < tabDesVariablesARemplacer.length; k++) {
      let debut = 0
      let n = 1
      while (n !== -1) {
        n = stock.indexOf(tabDesVariablesARemplacer[k], debut)
        if (n !== -1 && !lettreAvantOuApres(stock, n)) {
          // k=k-1
          if (n === 0) {
            stock = j3pRemplace(stock, n, n + tabDesVariablesARemplacer[k].length - 1, tabDesValeurs[k])
          } else {
            if (stock.charAt(n - 1) === '+' || stock.charAt(n - 1) === '-') {
              if (tabDesValeurs[k] < 0 || tabDesValeurs[k].charAt(0) === '-') {
                stock = j3pRemplace(stock, n, n + tabDesVariablesARemplacer[k].length - 1, '(' + tabDesValeurs[k] + ')')
              } else {
                stock = j3pRemplace(stock, n, n + tabDesVariablesARemplacer[k].length - 1, tabDesValeurs[k])
              }
              // stock=j3pRemplace(stock,n,n+tabDesVariables[k].length-1,tabDesValeurs[k])
            } else {
              if (tabDesValeurs[k] < 0 || tabDesValeurs[k].charAt(0) === '-') {
                stock = j3pRemplace(stock, n, n + tabDesVariablesARemplacer[k].length - 1, '\\times(' + tabDesValeurs[k] + ')')
              } else {
                stock = j3pRemplace(stock, n, n + tabDesVariablesARemplacer[k].length - 1, '\\times' + tabDesValeurs[k])
              }
            }
          }
        } else {
          debut = n + 1
        }
      }
    }// fin if n
    return stock
  }

  function substitution (tabDesCoeffs, tabDesVariables, tabDesValeurs) {
    // cette fonction a pour but de remplacer x,y,z par les coordonnées d’un point DE FACON PROPRE
    // ex : 2x+y-z avec x=3,y=-2 et z=3 doit donner2*3+(-2)-(-3)
    // On prend l’expression obtenue après simplification via sommeMonomes
    let k
    let stock = sommeMonomes(tabDesCoeffs, tabDesVariables)
    // Puis on retire des tableaux la variable cte "" et le coeff associé
    const tabVarSansCte = []
    const tabCoeffsSansCte = []
    for (k = 0; k < tabDesVariables.length; k++) {
      if (tabDesVariables[k] !== '') {
        tabVarSansCte.push(tabDesVariables[k])
        tabCoeffsSansCte.push(tabDesCoeffs[k])
      }
    }

    // On essaie à présent de remplacer chaque variable par la valeur numérique correspondante.

    let n

    for (k = 0; k < tabVarSansCte.length; k++) {
      n = stock.indexOf(tabVarSansCte[k])
      if (n !== -1) {
        if (Number(tabCoeffsSansCte[k]) === 1 || Number(tabCoeffsSansCte[k]) === -1) {
          if (tabDesValeurs[k] < 0 || tabDesValeurs[k].charAt(0) === '-') {
            stock = j3pRemplace(stock, n, n + tabVarSansCte[k].length - 1, '(' + tabDesValeurs[k] + ')')
          } else {
            stock = j3pRemplace(stock, n, n + tabVarSansCte[k].length - 1, tabDesValeurs[k])
          }
          // stock=j3pRemplace(stock,n,n+tabDesVariables[k].length-1,tabDesValeurs[k])
        } else {
          if (tabDesValeurs[k] < 0 || tabDesValeurs[k].charAt(0) === '-') {
            stock = j3pRemplace(stock, n, n + tabVarSansCte[k].length - 1, '\\times(' + tabDesValeurs[k] + ')')
          } else {
            stock = j3pRemplace(stock, n, n + tabVarSansCte[k].length - 1, '\\times' + tabDesValeurs[k])
          }
        }
      }
    }
    return stock
  }

  function validation (reponseEleve, objSolution) {
    // reponseEleve est un objet qui a les pptés .QCM=réponse au QCM et .intersection=[coord du point d’inter] éventuel.
    // On y rajoute les erreurs de détection de parallelisme et de résolution de système et le nombre d’erreurs

    const analyseReponse = {}
    analyseReponse.juste = true
    analyseReponse.commentaire = ''
    analyseReponse.erreur_systeme = 0
    analyseReponse.erreur_paralleles = 0
    analyseReponse.erreur = 0
    if (reponseEleve.QCM !== objSolution.position) {
      analyseReponse.QCM = false
      // la réponse est donc fausse.
      analyseReponse.juste = false
      analyseReponse.erreur = 1
      // On analyse la capacité de l’élève à détecter le parallélisme ou non

      switch (objSolution.position) {
        case 'confondues':
          if (reponseEleve.QCM === 'sécantes' || reponseEleve.QCM === 'non coplanaires') {
            analyseReponse.commentaire = 'pb_detection_parallelisme'
            analyseReponse.erreur_paralleles = 1
          }

          break

        case 'strictement parallèles':

          if (reponseEleve.QCM === 'sécantes' || reponseEleve.QCM === 'non coplanaires') {
            analyseReponse.commentaire = 'pb_detection_parallelisme'
            analyseReponse.erreur_paralleles = 1
          }

          break

        case 'sécantes':
          if (reponseEleve.QCM === 'confondues' || reponseEleve.QCM === 'strictement parallèles') {
            analyseReponse.commentaire = 'pb_detection_parallelisme'
            analyseReponse.erreur_paralleles = 1
          }
          break

        case 'non coplanaires':

          if (reponseEleve.QCM === 'confondues' || reponseEleve.QCM === 'strictement parallèles') {
            analyseReponse.commentaire = 'pb_detection_parallelisme'
            analyseReponse.erreur_paralleles = 1
          }

          break
      }
      // On rajoute le pb de résolution de système
      if ((objSolution.position === 'sécantes' && reponseEleve.QCM === 'non coplanaires') || (objSolution.position === 'non coplanaires' && reponseEleve.QCM === 'sécantes')) {
        analyseReponse.erreur_systeme = 1
      }
    } else {
      analyseReponse.QCM = true
      if (objSolution.position === 'sécantes') {
        // if (reponseEleve.intersection[0]!=objSolution.E.coord[0] || reponseEleve.intersection[1]!=objSolution.E.coord[1] || reponseEleve.intersection[2]!=objSolution.E.coord[2])
        if (!compareTabNum(reponseEleve.intersection, objSolution.E.coord)) {
          analyseReponse.juste = false
          analyseReponse.commentaire = 'erreur_coord_inter'
          analyseReponse.erreur = 1
        }
      }
    }
    return analyseReponse
  }

  function indications (laValidation) {
    let phrase = ''
    if (laValidation.commentaire === 'pb_detection_parallelisme') {
      phrase = ds.textes.indications_test_parallelisme
    }
    if (laValidation.commentaire === 'erreur_coord_inter') {
      phrase = ds.textes.indications_erreur_pt_inter
    }

    return phrase
  }
  function ligneRestante (x, y) {
    // FOnction qui renvoie la ligne non utilisée
    if ((x === 1 && y === 2) || (y === 1 && x === 2)) {
      return 3
    }
    if ((x === 1 && y === 3) || (y === 1 && x === 3)) {
      return 2
    }
    if ((x === 2 && y === 3) || (y === 2 && x === 3)) {
      return 1
    }
  }
  // Ni param1, ni param2 ne sont extractibles.
  // Il faut donc choisir le sytème le plus simple pour trouver les param.
  // la fonction score est "censée" évaluer la simplicité des couples de coeff.
  // Plus le score est élevé, mieux est le couple
  function score (coeff1, coeff2) {
    // aucun élément n’est nul
    // retourne un objet ayant les pptés
    // .score
    const stock = {}
    const coefsAbs = [Math.abs(coeff1), Math.abs(coeff2)]
    const pgcd = pgcdZarb(coefsAbs)
    stock.score = pgcd / coefsAbs[0] + pgcd / coefsAbs[1] + 0.01 / (coefsAbs[0] + coefsAbs[1]) + 0.01 / (coefsAbs[0] * coefsAbs[1])
    let c1 = coeff2 / pgcd
    let c2 = -coeff1 / pgcd
    // On fait en sorte d’avoir c1L1+c2L2 avec c2 positif
    if (c2 < 0) {
      c2 = -c2
      c1 = -c1
    }
    stock.coeffs = [c1, c2]

    return stock
  }
  function afficheCorrection (laValidation, objSolution) {
    stor.btnsActifs = false
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.toutpetit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (laValidation.juste) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2, 3, 4, 5, 6, 7, 8, 9].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    let det, xy, yx, yz, zy, xz, zx, a, b, c, d, e, f, g, h, i, j, k, numeroLigneRestante, membre1, membre2, verification
    let plusPetitCoeff, report, reportLigne, valeurAutreParametre
    let paramDirect, lequel, resultat1, resultat2, k0, k1, extractible
    let lignesChoisies, maxScore, parametreADeterminer, autreParametre, leScore, coeffChoisis, devantParam, valeurParametreADeterminer
    let valeurParametre1, valeurParametre2, autre, combiLignes, isolParam, afficheResultatParam
    let ctesReduites
    const A = objSolution.A
    const B = objSolution.B
    const C = objSolution.C
    const D = objSolution.D
    const E = objSolution.E
    let L

    const u = objSolution.u
    const v = objSolution.v
    const AC = objSolution.AC
    const d1 = objSolution.d1
    const d2 = objSolution.d2
    const x1 = objSolution.u.coord[0]
    const y1 = objSolution.u.coord[1]
    const z1 = objSolution.u.coord[2]

    const x2 = objSolution.v.coord[0]
    const y2 = objSolution.v.coord[1]
    const z2 = objSolution.v.coord[2]

    const position = objSolution.position

    const expressionX1 = objSolution.expressionX1
    const expressionY1 = objSolution.expressionY1
    const expressionZ1 = objSolution.expressionZ1

    const parametre1 = objSolution.parametre[0]
    const parametre2 = objSolution.parametre[1]

    const expressionX2 = objSolution.expressionX2
    const expressionY2 = objSolution.expressionY2
    const expressionZ2 = objSolution.expressionZ2
    const tabExpression1 = [expressionX1, expressionY1, expressionZ1]
    const tabExpression2 = [expressionX2, expressionY2, expressionZ2]
    const tabPermut1 = objSolution.tabPermut1
    const tabPermut2 = objSolution.tabPermut2
    j3pAffiche(stor.zoneExpli1, '', ds.textes.phrase_correction_dirige, { E: d1, F: d2, u: u.nom, v: v.nom, a: u.coord[0], b: u.coord[1], c: u.coord[2], d: v.coord[0], e: v.coord[1], f: v.coord[2], i: A.coord[0], j: A.coord[1], k: A.coord[2], l: C.coord[0], m: C.coord[1], n: C.coord[2], A: A.nom, B: C.nom })
    j3pAffiche(stor.zoneExpli2, '', ds.textes.phrase_correction_regle, { E: d1, F: d2, u: u.nom, v: v.nom })
    const paralleles = ((position === 'confondues') || (position === 'strictement parallèles'))

    if (paralleles) {
      // On gère les différentes corrections possibles
      let choixCorrection = ds.choixCorrection
      if (estEntier(objSolution.lambda) || estEntier(1 / objSolution.lambda)) {
        choixCorrection = 1
      }
      let p, det1, det2, det3
      switch (choixCorrection) {
        case 1:

          p = v.nom + '=' + sommeMonomes([objSolution.lambda], [u.nom])
          j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_colinearite, { p })
          j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_col, { u: u.nom, v: v.nom })
          j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_paralleles, { E: d1, F: d2 })

          break

        case 2:

          xy = sommeAkBk([x1], [y2])
          yx = sommeAkBk([y1], [x2])
          a = 'x_{' + u.nom + '} \\times y_{' + v.nom + '}=' + xy.ecriture + '=' + xy.valeur
          b = 'y_{' + u.nom + '}\\times x_{' + v.nom + '}=' + yx.ecriture + '=' + yx.valeur
          c = 'x_{' + u.nom + '}\\times y_{' + v.nom + '}=' + 'y_{' + u.nom + '}\\times x_{' + v.nom + '}'// var det1="x_{"+u+"} \\times y_{"+v+"}-y_{"+u+"}\\times x_{"+v+"}="+det+"=0";

          yz = sommeAkBk([y1], [z2])
          zy = sommeAkBk([z1], [y2])
          d = 'y_{' + u.nom + '} \\times z_{' + v.nom + '}=' + yz.ecriture + '=' + yz.valeur
          e = 'z_{' + u.nom + '}\\times y_{' + v.nom + '}=' + zy.ecriture + '=' + zy.valeur
          f = 'y_{' + u.nom + '} \\times z_{' + v.nom + '}=' + 'z_{' + u.nom + '}\\times y_{' + v.nom + '}'

          xz = sommeAkBk([x1], [z2])
          zx = sommeAkBk([z1], [x2])
          g = 'x_{' + u.nom + '} \\times z_{' + v.nom + '}=' + xz.ecriture + '=' + xz.valeur
          h = 'z_{' + u.nom + '}\\times x_{' + v.nom + '}=' + zx.ecriture + '=' + zx.valeur
          i = 'x_{' + u.nom + '} \\times z_{' + v.nom + '}=' + 'z_{' + u.nom + '}\\times x_{' + v.nom + '}'

          j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_col_detail_alexis, { a, b, c, d, e, f, g, h, i })
          j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_col, { u: u.nom, v: v.nom })
          j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_paralleles, { E: d1, F: d2 })

          break

        case 3:
          det = determinant([x1, y1], [x2, y2]).ecriture
          det1 = 'x_{' + u.nom + '} \\times y_{' + v.nom + '}-y_{' + u.nom + '}\\times x_{' + v.nom + '}=' + det + '=0'

          det = determinant([y1, z1], [y2, z2]).ecriture
          det2 = 'y_{' + u.nom + '} \\times z_{' + v.nom + '}-z_{' + u.nom + '}\\times y_{' + v.nom + '}=' + det + '=0'

          det = determinant([x1, z1], [x2, z2]).ecriture
          det3 = 'x_{' + u.nom + '} \\times z_{' + v.nom + '}-z_{' + u.nom + '}\\times x_{' + v.nom + '}=' + det + '=0'

          j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_col_detail, { a: det1, b: det2, c: det3 })
          j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_col, { u: u.nom, v: v.nom })
          j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_paralleles, { E: d1, F: d2 })

          break
      }
      j3pAffiche(stor.zoneExpli6, '', ds.textes.phrase_correction_trancher_conf_strict, { A: A.nom })
      // On prépare la résolution du systeme d2=C
      const Lignes = ['', '', '']
      for (k = 0; k < 3; k++) {
        if (v.coord[k] !== 0) {
          Lignes[k] = '$' + parametre2 + '=' + fraction((A.coord[k] - C.coord[k]) / v.coord[k]).mathq + '$'
        } else {
          // soit il y a égalité, soit pas
          if (A.coord[k] === C.coord[k]) {
            Lignes[k] = 'toujours vérifié'
          } else {
            Lignes[k] = 'impossible'
          }
        }
      }

      j3pAfficheSysteme(stor.zoneExpli7, '', ds.textes.phrase_correction_resol_A_sur_d2, [['$' + A.coord[0] + '=' + expressionX2 + '$', '$' + A.coord[1] + '=' + expressionY2 + '$', '$' + A.coord[2] + '=' + expressionZ2 + '$'], [Lignes[0], Lignes[1], Lignes[2]]], {}, zoneExpli.style.color)
      if (position === 'confondues') {
        j3pAffiche(stor.zoneExpli8, '', ds.textes.phrase_correction_constat_A_sur_d2, { A: A.nom })
      } else {
        j3pAffiche(stor.zoneExpli8, '', ds.textes.phrase_correction_constat_A_pas_sur_d2, { A: A.nom })
      }
    } else {
      switch (ds.choixCorrection) {
        case 3:
          {
            let detNonNul = ''

            det = 0
            let detCalcul = ''
            if (x1 * y2 - y1 * x2 !== 0) {
              det = x1 * y2 - y1 * x2
              detCalcul = determinant([x1, y1], [x2, y2]).ecriture

              detNonNul = 'x_{' + u.nom + '} \\times y_{' + v.nom + '}-y_{' + u.nom + '}\\times x_{' + v.nom + '}=' + detCalcul + '=' + det + ' \\neq 0'
            } else {
              if (y1 * z2 - z1 * y2 !== 0) {
                det = y1 * z2 - z1 * y2
                detCalcul = determinant([y1, z1], [y2, z2]).ecriture
                detNonNul = 'y_{' + u.nom + '} \\ times z_{' + v.nom + '}-z_{' + u.nom + '}\\times y_{' + v.nom + '}' + detCalcul + '=' + det + ' \\neq 0'
              } else {
                det = x1 * z2 - z1 * x2
                detCalcul = determinant([x1, z1], [x2, z2]).ecriture
                detNonNul = 'x_{' + u.nom + '} \\times z_{' + v.nom + '}-z_{' + u.nom + '}\\times x_{' + v.nom + '}' + detCalcul + '=' + det + ' \\neq 0'
              }
            }
            j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_colinearite, { p: detNonNul })
          }
          break

        case 1:
        case 2:
          if (x1 * y2 - y1 * x2 !== 0) {
            xy = sommeAkBk([x1], [y2])
            yx = sommeAkBk([y1], [x2])
            a = 'x_{' + u.nom + '} \\times y_{' + v.nom + '}=' + xy.ecriture + '=' + xy.valeur
            b = 'y_{' + u.nom + '}\\times x_{' + v.nom + '}=' + yx.ecriture + '=' + yx.valeur
            c = 'x_{' + u.nom + '} \\times y_{' + v.nom + '}' + ' \\neq ' + 'y_{' + u.nom + '}\\times x_{' + v.nom + '}'
            j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col_alexis, { a, b, c })
          } else {
            if (y1 * z2 - z1 * y2 !== 0) {
              yz = sommeAkBk([y1], [z2])
              zy = sommeAkBk([z1], [y2])
              a = 'y_{' + u.nom + '} \\times z_{' + v.nom + '}=' + yz.ecriture + '=' + yz.valeur
              b = 'z_{' + u.nom + '}\\times y_{' + v.nom + '}=' + zy.ecriture + '=' + zy.valeur
              c = 'y_{' + u.nom + '} \\times z_{' + v.nom + '} \\neq ' + 'z_{' + u.nom + '}\\times y_{' + v.nom + '}'
              j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col_alexis, { a, b, c })
            } else {
              xz = sommeAkBk([x1], [z2])
              zx = sommeAkBk([z1], [x2])
              a = 'x_{' + u.nom + '} \\times z_{' + v.nom + '}=' + xz.ecriture + '=' + xz.valeur
              b = 'z_{' + u.nom + '}\\times x_{' + v.nom + '}=' + zx.ecriture + '=' + zx.valeur
              c = 'x_{' + u.nom + '} \\times z_{' + v.nom + '} \\neq ' + 'z_{' + u.nom + '}\\times x_{' + v.nom + '}'
              j3pAffiche(stor.zoneExpli3, '', ds.textes.phrase_correction_constat_non_col_alexis, { a, b, c })
            }
          }

          break
      }

      j3pAffiche(stor.zoneExpli4, '', ds.textes.phrase_correction_conclusion_non_col, { u: u.nom, v: v.nom })
      j3pAffiche(stor.zoneExpli5, '', ds.textes.phrase_correction_non_paralleles, { E: d1, F: d2 })
      j3pAffiche(stor.zoneExpli6, '', ds.textes.phrase_correction_trancher_sec_non_cop, { a: parametre1, b: parametre2 })
      let L1 = '$' + expressionX1 + '=' + expressionX2 + '$&nbsp;&nbsp;L1'
      let L2 = '$' + expressionY1 + '=' + expressionY2 + '$&nbsp;&nbsp;L2'
      let L3 = '$' + expressionZ1 + '=' + expressionZ2 + '$&nbsp;&nbsp;L3'

      // Il s’agit maintenant de résoudre ce système de façon optimale.
      // La rédation n’est pas la même selon le fait que les droites sont sécantes ou non coplanaires.
      // On commence par le cas non coplanaires
      if (position === 'non coplanaires') {
        // On commence par regarder s’il existe une ligne directmeent non vérifiée, c’est-à-dire dans laquelle u et v ont une coordonnées nulle en même temps.
        const ligneNonVerifiee = []
        for (k = 0; k < 3; k++) {
          if (u.coord[k] === 0 && v.coord[k] === 0) {
            ligneNonVerifiee.push(k)
          }
        }
        if (ligneNonVerifiee.length > 0) {
          j3pAfficheSysteme(stor.zoneExpli7, '', ds.textes.phrase_correction_systeme, [[L1, L2, L3]], {}, zoneExpli.style.color)
          // On se contente d’afficher que la ligne x est non vérifiée, sans effectuer de résolution.

          j3pAffiche(stor.zoneExpli8, '', ds.textes.phrase_correction_ligneNonVerifiee, { k: ligneNonVerifiee[0] + 1 })
        } else {
          // Sinon, aucune ligne est directement non vérifiée.
          // On peut avoir le cas où l’un des deux paramètres est calculable deux fois sans résoudre le système. Avec deux valeurs forcément différentes.
          // c’est-à-dire si il existe deux coordonnées nulles pour u ou pour v
          paramDirect = []

          lequel = ''
          resultat1 = 0
          resultat2 = 0
          k0 = 0
          k1 = 0

          for (k = 0; k < 3; k++) {
            if (v.coord[k] === 0) {
              paramDirect.push(k)
            }
          }

          if (paramDirect.length === 2) {
            lequel = parametre1
            k0 = paramDirect[0]
            k1 = paramDirect[1]

            resultat1 = fraction(AC.coord[k0] / u.coord[k0]).mathq
            resultat2 = fraction(AC.coord[k1] / u.coord[k1]).mathq
          } else {
            paramDirect = []
            for (k = 0; k < 3; k++) {
              if (u.coord[k] === 0) {
                paramDirect.push(k)
              }
            }
            if (paramDirect.length === 2) {
              lequel = parametre2
              k0 = paramDirect[0]
              k1 = paramDirect[1]
              resultat1 = fraction(-AC.coord[k0] / v.coord[k0]).mathq
              resultat2 = fraction(-AC.coord[k1] / v.coord[k1]).mathq
            }
          }

          if (lequel !== '') {
            j3pAfficheSysteme(stor.zoneExpli7, '', ds.textes.phrase_correction_systeme, [[L1, L2, L3]], {}, zoneExpli.style.color)
            // On tombe donc sur deux valeurs incompatibles pour le même paramètre
            j3pAffiche(stor.zoneExpli8, '', ds.textes.phrase_correction_parametres_incompatibles, { p: lequel, i: paramDirect[0] + 1, j: paramDirect[1] + 1, a: resultat1, b: resultat2 })
          } else {
            // on regarde si parametre1 peut être calculée grace à l’une des équations.
            paramDirect = []
            for (k = 0; k < 3; k++) {
              if (v.coord[k] === 0) {
                paramDirect.push(k)
              }
            }
            if (paramDirect.length === 1) {
              j3pAfficheSysteme(stor.zoneExpli7, '', ds.textes.phrase_correction_systeme, [[L1, L2, L3]], {}, zoneExpli.style.color)
              // C’est le cas.

              k0 = paramDirect[0]

              resultat1 = fraction(AC.coord[k0] / u.coord[k0])
              // resultat1 est un objet

              // Il faut à présent regarder si parametre2 peut être extraite de la même façon.
              paramDirect = []
              for (k = 0; k < 3; k++) {
                if (u.coord[k] === 0) {
                  paramDirect.push(k)
                }
              }

              if (paramDirect.length === 1) {
                // Le deuxième est extractible

                k1 = paramDirect[0]
                resultat2 = fraction(-AC.coord[k1] / v.coord[k1])
                // resultat2 est un objet
                // Reste alors à prouver que la ligne restante n’est pas vérifiée
                numeroLigneRestante = ligneRestante(k0 + 1, k1 + 1)

                // On remplace dans la ligne restante, en respectant l’odre des variables
                membre1 = ''
                membre2 = ''
                if (Number(tabPermut1[numeroLigneRestante - 1]) === 0) {
                  membre1 = substitution([A.coord[numeroLigneRestante - 1], u.coord[numeroLigneRestante - 1]], ['', parametre1], [String(resultat1.mathq)])
                } else {
                  membre1 = substitution([u.coord[numeroLigneRestante - 1], A.coord[numeroLigneRestante - 1]], [parametre1, ''], [String(resultat1.mathq)])
                }
                if (Number(tabPermut2[numeroLigneRestante - 1]) === 0) {
                  membre2 = substitution([C.coord[numeroLigneRestante - 1], v.coord[numeroLigneRestante - 1]], ['', parametre2], [String(resultat2.mathq)])
                } else {
                  membre2 = substitution([v.coord[numeroLigneRestante - 1], C.coord[numeroLigneRestante - 1]], [parametre2, ''], [String(resultat2.mathq)])
                }

                // var verification=substitution([A.coord[numeroLigneRestante-1],u.coord[numeroLigneRestante-1]],["",parametre1],[resultat1])+"\\neq"+substitution([C.coord[numeroLigneRestante-1],v.coord[numeroLigneRestante-1]],["",parametre1],[resultat2])
                verification = membre1 + '\\neq' + membre2

                j3pAffiche(stor.zoneExpli8, '', ds.textes.phrase_correction_les_deux_exctactibles, { p: parametre1 + '=' + resultat1.mathq, i: k0 + 1, j: k1 + 1, k: numeroLigneRestante, q: parametre2 + '=' + resultat2.mathq, r: verification })
              } else {
                // paramètre2 n’est pas extractible. Il faut alors choisir une ligne pour reporter parametre1 et en déduire parametre2
                // Il s’agit de choisir la ligne la plus simple parmi les deux lignes restantes différentes de k0
                // ie le coeff devant parametre2 doit être de valeur absolue la plus petite.
                // on chercher donc la ligne dans laquelle ce coeff est le plus petit

                plusPetitCoeff = [Math.pow(10, 99), 0]
                for (k = 0; k < 3; k++) {
                  if (k === k0) {
                    k = k + 1
                  }
                  if (Math.abs(v.coord[k]) < plusPetitCoeff[0]) {
                    plusPetitCoeff[0] = Math.abs(v.coord[k])
                    plusPetitCoeff[1] = k
                  }
                }
                // parametre2 est reporté dans Ligne k1
                k1 = plusPetitCoeff[1]

                numeroLigneRestante = ligneRestante(k0 + 1, k1 + 1)

                // Et on reporte dans Ligne restante pour vérifier non égalité.
                // Il faut vérifier dans quel ordre a été donné l’eq param
                report = ''
                if (Number(tabPermut1[k1]) === 0) {
                  report = substitution([A.coord[k1], u.coord[k1]], ['', parametre1], [String(resultat1.mathq)])
                } else {
                  report = substitution([u.coord[k1], A.coord[k1]], [parametre1, ''], [String(resultat1.mathq)])
                }

                report = report + '=' + tabExpression2[k1]

                resultat2 = fraction((-AC.coord[k1] + u.coord[k1] * resultat1.valeur) / v.coord[k1])
                // resultat 2 est un objet

                membre1 = ''
                membre2 = ''
                if (String(tabPermut1[numeroLigneRestante - 1]) === '0') {
                  membre1 = substitution([A.coord[numeroLigneRestante - 1], u.coord[numeroLigneRestante - 1]], ['', parametre1], [String(resultat1.mathq)])
                } else {
                  membre1 = substitution([u.coord[numeroLigneRestante - 1], A.coord[numeroLigneRestante - 1]], [parametre1, ''], [String(resultat1.mathq)])
                }

                if (String(tabPermut2[numeroLigneRestante - 1]) === '0') {
                  membre2 = substitution([C.coord[numeroLigneRestante - 1], v.coord[numeroLigneRestante - 1]], ['', parametre2], [String(resultat2.mathq)])
                } else {
                  membre2 = substitution([v.coord[numeroLigneRestante - 1], C.coord[numeroLigneRestante - 1]], [parametre2, ''], [String(resultat2.mathq)])
                }
                verification = membre1 + '\\neq' + membre2

                j3pAffiche(stor.zoneExpli8, '', ds.textes.phrase_correction_report_dans_ligne, { p: parametre1 + '=' + resultat1.mathq, i: k0 + 1, j: k1 + 1, k: numeroLigneRestante, q: report, r: parametre2 + '=' + resultat2.mathq, s: verification })
              }
            } else {
              // Le paramètre1 n’est pas extratible.
              // Il faut regarder si le param2 l’est

              paramDirect = []
              for (k = 0; k < 3; k++) {
                if (u.coord[k] === 0) {
                  paramDirect.push(k)
                }
              }

              if (paramDirect.length === 1) {
                // Il l’est. On trouve alors sa valeur qu’on reporte dans la ligne restante
                j3pAfficheSysteme(stor.zoneExpli7, '', ds.textes.phrase_correction_systeme, [[L1, L2, L3]], {}, zoneExpli.style.color)
                k0 = paramDirect[0]
                resultat2 = fraction(-AC.coord[k0] / v.coord[k0])
                plusPetitCoeff = [Math.pow(10, 99), 0]
                for (k = 0; k < 3; k++) {
                  if (k === k0) {
                    k = k + 1
                  }
                  if (Math.abs(u.coord[k]) < plusPetitCoeff[0]) {
                    plusPetitCoeff[0] = Math.abs(u.coord[k])
                    plusPetitCoeff[1] = k
                  }
                }
                // parametre2 est reporté dans Ligne k1
                k1 = plusPetitCoeff[1]

                numeroLigneRestante = ligneRestante(k0 + 1, k1 + 1)
                report = ''
                if (String(tabPermut2[k1]) === '0') {
                  report = substitution([C.coord[k1], v.coord[k1]], ['', parametre2], [String(resultat2.mathq)])
                } else {
                  report = substitution([v.coord[k1], C.coord[k1]], [parametre2, ''], [String(resultat2.mathq)])
                }

                report = tabExpression1[k1] + '=' + report
                resultat1 = fraction((AC.coord[k1] + v.coord[k1] * resultat2.valeur) / u.coord[k1])
                // resultat 2 est un objet

                membre1 = ''
                membre2 = ''

                if (String(tabPermut1[numeroLigneRestante - 1]) === '0') {
                  membre1 = substitution([A.coord[numeroLigneRestante - 1], u.coord[numeroLigneRestante - 1]], ['', parametre1], [String(resultat1.mathq)])
                } else {
                  membre1 = substitution([u.coord[numeroLigneRestante - 1], A.coord[numeroLigneRestante - 1]], [parametre1, ''], [String(resultat1.mathq)])
                }

                if (String(tabPermut2[numeroLigneRestante - 1]) === '0') {
                  membre2 = substitution([C.coord[numeroLigneRestante - 1], v.coord[numeroLigneRestante - 1]], ['', parametre2], [String(resultat2.mathq)])
                } else {
                  membre2 = substitution([v.coord[numeroLigneRestante - 1], C.coord[numeroLigneRestante - 1]], [parametre2, ''], [String(resultat2.mathq)])
                }
                verification = membre1 + '\\neq' + membre2

                j3pAffiche(stor.zoneExpli8, '', ds.textes.phrase_correction_report_dans_ligne, { p: parametre2 + '=' + resultat2.mathq, i: k0 + 1, j: k1 + 1, k: numeroLigneRestante, q: report, r: parametre1 + '=' + resultat1.mathq, s: verification })
                // FIn de param2 extractible
              } else {
                L1 = '$' + expressionX1 + '=' + expressionX2 + '$'
                L2 = '$' + expressionY1 + '=' + expressionY2 + '$'
                L3 = '$' + expressionZ1 + '=' + expressionZ2 + '$'
                // affichage du système
                L = []
                for (k = 0; k < 3; k++) {
                  L[k] = sommeMonomes([u.coord[k], -v.coord[k]], [parametre1, parametre2]) + '=' + AC.coord[k]
                }

                lignesChoisies = []
                maxScore = -1
                parametreADeterminer = ''
                autreParametre = ''
                leScore = 0
                coeffChoisis = []
                // var diviseur = 0
                devantParam = 0
                ctesReduites = 0
                valeurParametreADeterminer = 0
                // var det_retenu = 0
                valeurParametre1 = 0
                valeurParametre2 = 0

                for (k = 0; k < 3; k++) {
                  i = k % 2
                  j = ((k - 1) * (k - 2) + 2) % 3
                  det = u.coord[i] * v.coord[j] - u.coord[j] * v.coord[i]

                  // On cherche le couple de ligne aboutissant à un score max
                  if (det !== 0) {
                    leScore = score(u.coord[i], u.coord[j]).score
                    if (leScore > maxScore) {
                      // var det_retenu = det
                      valeurParametre1 = (v.coord[j] * AC.coord[i] - v.coord[i] * AC.coord[j]) / det
                      valeurParametre2 = (u.coord[j] * AC.coord[i] - u.coord[i] * AC.coord[j]) / det
                      lignesChoisies = [i + 1, j + 1]
                      coeffChoisis = score(u.coord[i], u.coord[j]).coeffs
                      maxScore = leScore
                      parametreADeterminer = parametre2// paramètre à déterminer
                      autreParametre = parametre1
                      valeurAutreParametre = valeurParametre1
                      // On réduit alors le parametre 2 à l’issue de cette combi linéaire
                      devantParam = -coeffChoisis[0] * v.coord[i] - coeffChoisis[1] * v.coord[j]
                      ctesReduites = coeffChoisis[0] * AC.coord[i] + coeffChoisis[1] * AC.coord[j]
                      valeurParametreADeterminer = fraction(ctesReduites / devantParam).mathq
                      // valeur du coeff pour le report de la valeur du parametre
                      reportLigne = i

                      if (Math.abs(v.coord[i]) > Math.abs(v.coord[j])) {
                        // On choisit la ligne j comme report
                        reportLigne = j
                      }

                      // [u.coord[report_lig
                      // On calcule
                    }
                    leScore = score(v.coord[i], v.coord[j]).score
                    if (leScore > maxScore) {
                      // det_retenu = det
                      valeurParametre1 = (v.coord[j] * AC.coord[i] - v.coord[i] * AC.coord[j]) / det
                      valeurParametre2 = (u.coord[j] * AC.coord[i] - u.coord[i] * AC.coord[j]) / det
                      lignesChoisies = [i + 1, j + 1]
                      coeffChoisis = score(v.coord[i], v.coord[j]).coeffs
                      maxScore = leScore
                      parametreADeterminer = parametre1// paramètre à déterminer
                      autreParametre = parametre2
                      valeurAutreParametre = valeurParametre2
                      devantParam = coeffChoisis[0] * u.coord[i] + coeffChoisis[1] * u.coord[j]
                      ctesReduites = coeffChoisis[0] * AC.coord[i] + coeffChoisis[1] * AC.coord[j]
                      valeurParametreADeterminer = fraction(ctesReduites / devantParam).mathq
                      reportLigne = i
                      if (Math.abs(u.coord[i]) > Math.abs(u.coord[j])) {
                        // On choisit la ligne j comme report
                        reportLigne = j
                      }
                    }
                  }
                }

                // On connait donc le couple de lignes à choisir, on définit les coeffs

                report = substitution2(L[reportLigne], [parametreADeterminer], [String(valeurParametreADeterminer)])

                combiLignes = sommeMonomes(coeffChoisis, ['L' + lignesChoisies[0], 'L' + lignesChoisies[1]])
                // var diviseur=coeffChoisis[0]
                isolParam = devantParam + parametreADeterminer + '=' + ctesReduites
                afficheResultatParam = parametreADeterminer + '=' + valeurParametreADeterminer
                valeurAutreParametre = autreParametre + '=' + fraction(valeurAutreParametre).mathq

                numeroLigneRestante = ligneRestante(lignesChoisies[0], lignesChoisies[1])

                verification = sommeMonomes([u.coord[numeroLigneRestante - 1], -v.coord[numeroLigneRestante - 1]], [parametre1, parametre2]) + '\\neq' + AC.coord[numeroLigneRestante - 1]

                verification = substitution2(verification, [parametre1, parametre2], [String(fraction(valeurParametre1).mathq), String(fraction(valeurParametre2).mathq)])

                j3pAfficheSysteme(stor.zoneExpli9, '', ds.textes.phrase_correction_mise_en_forme_systeme, [[L1, L2, L3], ['$' + L[0] + '$&nbsp;&nbsp; $L1$', '$' + L[1] + '$&nbsp;&nbsp; $L2$', '$' + L[2] + '$&nbsp;&nbsp;  $L3$']], {}, zoneExpli.style.color)
                // On calcule le premier paramètre
                ;[10, 11, 12].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
                j3pAffiche(stor.zoneExpli10, '', ds.textes.phrase_correction_combiLignes, { a: combiLignes, b: isolParam, c: afficheResultatParam })
                // Dont on reporte la valeur dans la ligne où le coefficient devant le parametre restant est le plus simple
                // phrase_correction_report:"En reportant dans L£j, on obtient $£q$"
                j3pAffiche(stor.zoneExpli11, '', ds.textes.phrase_correction_report, { j: reportLigne + 1, p: report, q: valeurAutreParametre })
                j3pAffiche(stor.zoneExpli12, '', ds.textes.phrase_correction_non_verifiee, { k: numeroLigneRestante, p: verification })
              }
            }
          }
        }
      }
      if (position === 'sécantes') {
        // On tente un bon vieux copier-coller des familles, mais pas mal de modifs en perspective...

        // ON définit les lignes sans isoler les inconnues du même coté pour l’instant
        L = []
        L[0] = expressionX1
        L[3] = expressionX2
        L[1] = expressionY1
        L[4] = expressionY2
        L[2] = expressionZ1
        L[5] = expressionZ2
        parametreADeterminer = ''
        // On va calculer la valeur de chaque paramètre.
        d = determinant([u.coord[0], u.coord[1]], [-v.coord[0], -v.coord[1]]).valeur
        let valeurParametre1, valeurParametre2
        report = ''
        if (d !== 0) {
          valeurParametre1 = determinant([AC.coord[0], AC.coord[1]], [-v.coord[0], -v.coord[1]]).valeur / d
          valeurParametre2 = determinant([u.coord[0], u.coord[1]], [AC.coord[0], AC.coord[1]]).valeur / d
        } else {
          d = determinant([u.coord[0], u.coord[2]], [-v.coord[0], -v.coord[2]]).valeur
          if (d !== 0) {
            valeurParametre1 = determinant([AC.coord[0], AC.coord[2]], [-v.coord[0], -v.coord[2]]).valeur / d
            valeurParametre2 = determinant([u.coord[0], u.coord[2]], [AC.coord[0], AC.coord[2]]).valeur / d
          } else {
            d = determinant([u.coord[1], u.coord[2]], [-v.coord[1], -v.coord[2]]).valeur
            valeurParametre1 = determinant([AC.coord[1], AC.coord[2]], [-v.coord[1], -v.coord[2]]).valeur / d
            valeurParametre2 = determinant([u.coord[1], u.coord[2]], [AC.coord[1], AC.coord[2]]).valeur / d
          }

          // deuxième détérminant possible
        }
        // On peut avoir le cas où l’un des deux paramètres est calculable deux fois sans résoudre le système. Avec deux valeurs forcément égales.
        // c’est-à-dire si il existe deux coordonnées nulles pour u ou pour v
        // On extraiera alors le deuxième paramètre avec la dernière.

        paramDirect = []

        lequel = ''
        resultat1 = 0
        resultat2 = 0
        k0 = 0
        k1 = 0
        extractible = false
        autre = ''

        for (k = 0; k < 3; k++) {
          if (v.coord[k] === 0) {
            paramDirect.push(k)
          }
        }

        if (paramDirect.length === 2) {
          lequel = parametre1
          autre = parametre2
          k0 = paramDirect[0]
          k1 = paramDirect[1]
          // On en profite pour tester l’extractibilité de l’autre paramètre :
          numeroLigneRestante = ligneRestante(k0 + 1, k1 + 1)
          if (String(u.coord[numeroLigneRestante - 1]) === '0') {
            extractible = true
          }

          resultat1 = fraction(valeurParametre1).mathq
          // On calcule la valeur du parametre restant.
          resultat2 = fraction(valeurParametre2).mathq
        } else {
          paramDirect = []
          for (k = 0; k < 3; k++) {
            if (String(u.coord[k]) === '0') {
              paramDirect.push(k)
            }
          }
          if (paramDirect.length === 2) {
            lequel = parametre2
            autre = parametre1
            k0 = paramDirect[0]
            k1 = paramDirect[1]
            // On en profite pour tester l’extractibilité de l’autre paramètre :
            numeroLigneRestante = ligneRestante(k0 + 1, k1 + 1)
            if (String(v.coord[numeroLigneRestante - 1]) === '0') {
              extractible = true
            }
            resultat1 = fraction(valeurParametre2).mathq
            // On calcule la valeur du parametre restant.
            resultat2 = fraction(valeurParametre1).mathq
          }
        }

        if (lequel !== '') {
          // On est dans le cas où un paramètre est extractible deux fois.
          // Soit le dernier paramètre est également extractible, soit il faut reporter

          j3pAfficheSysteme(stor.zoneExpli7, '', ds.textes.phrase_correction_systeme, [[L1, L2, L3]], {}, zoneExpli.style.color)
          j3pAffiche(stor.zoneExpli8, '', ds.textes.phrase_correction_secantes_1, { p: lequel, i: paramDirect[0] + 1, j: paramDirect[1] + 1, a: resultat1 })
          // Soit le deuxième paramètre est extractible aussi :

          if (extractible) {
            j3pAffiche(stor.zoneExpli9, '', ds.textes.phrase_correction_secantes_2, { i: numeroLigneRestante, p: autre, a: resultat2 })
          } else {
            // Soit on procède à un report :
            // report=
            report = substitution2(L[numeroLigneRestante - 1], [lequel], [String(resultat1)]) + '=' + substitution2(L[numeroLigneRestante + 2], [lequel], [String(resultat1)])

            j3pAffiche(stor.zoneExpli9, '', ds.textes.phrase_correction_secantes_3, { i: numeroLigneRestante, p: report, q: autre + '=' + resultat2 })
          }
        } else {
          // on regarde si parametre1 peut être calculée grace à l’une des équations.
          paramDirect = []
          for (k = 0; k < 3; k++) {
            // IL faut v[k]=0 sans que u[k] le soit
            if (String(v.coord[k]) === '0' && Number(u.coord[k]) !== 0) {
              paramDirect.push(k)
            }
          }

          if (paramDirect.length === 1) {
            j3pAfficheSysteme(stor.zoneExpli7, '', ds.textes.phrase_correction_systeme, [[L1, L2, L3]], {}, zoneExpli.style.color)
            // j3pAfficheSysteme("explications","div_e8",ds.textes.phrase_correction_systeme,[[L1,L2,L3]],{styletexte:{}},me.styles.toutpetit.enonce.color);
            // C’est le cas.

            k0 = paramDirect[0]

            resultat1 = fraction(AC.coord[k0] / u.coord[k0])
            // resultat1 est un objet

            // Il faut à présent regarder si parametre2 peut être extrait de la même façon.
            paramDirect = []
            for (k = 0; k < 3; k++) {
              if (Number(u.coord[k]) === 0) {
                paramDirect.push(k)
              }
            }

            if (paramDirect.length === 1) {
              // Le deuxième est extractible

              k1 = paramDirect[0]
              resultat2 = fraction(-AC.coord[k1] / v.coord[k1])
              // resultat2 est un objet
              // Reste alors à prouver que la ligne restante n’est pas vérifiée
              numeroLigneRestante = ligneRestante(k0 + 1, k1 + 1)

              // On remplace dans la ligne restante, en respectant l’odre des variables
              membre1 = ''
              membre2 = ''
              if (Number(tabPermut1[numeroLigneRestante - 1]) === 0) {
                membre1 = substitution([A.coord[numeroLigneRestante - 1], u.coord[numeroLigneRestante - 1]], ['', parametre1], [String(resultat1.mathq)])
              } else {
                membre1 = substitution([u.coord[numeroLigneRestante - 1], A.coord[numeroLigneRestante - 1]], [parametre1, ''], [String(resultat1.mathq)])
              }
              if (Number(tabPermut2[numeroLigneRestante - 1]) === 0) {
                membre2 = substitution([C.coord[numeroLigneRestante - 1], v.coord[numeroLigneRestante - 1]], ['', parametre2], [String(resultat2.mathq)])
              } else {
                membre2 = substitution([v.coord[numeroLigneRestante - 1], C.coord[numeroLigneRestante - 1]], [parametre2, ''], [String(resultat2.mathq)])
              }

              // var verification=substitution([A.coord[numeroLigneRestante-1],u.coord[numeroLigneRestante-1]],["",parametre1],[resultat1])+"\\neq"+substitution([C.coord[numeroLigneRestante-1],v.coord[numeroLigneRestante-1]],["",parametre1],[resultat2])
              verification = membre1 + '=' + membre2

              j3pAffiche(stor.zoneExpli8, '', ds.textes.phrase_correction_secantes_5, { p: parametre1 + '=' + resultat1.mathq, i: k0 + 1, j: k1 + 1, k: numeroLigneRestante, q: parametre2 + '=' + resultat2.mathq, r: verification })
            } else {
              // paramètre2 n’est pas extractible. Il faut alors choisir une ligne pour reporter parametre1 et en déduire parametre2
              // Il s’agit de choisir la ligne la plus simple parmi les deux lignes restantes différentes de k0
              // ie le coeff devant parametre2 doit être de valeur absolue la plus petite.
              // on chercher donc la ligne dans laquelle ce coeff est le plus petit

              plusPetitCoeff = [Math.pow(10, 99), 0]
              for (k = 0; k < 3; k++) {
                if (k === k0) {
                  k = k + 1
                }
                if (Math.abs(v.coord[k]) < plusPetitCoeff[0]) {
                  plusPetitCoeff[0] = Math.abs(v.coord[k])
                  plusPetitCoeff[1] = k
                }
              }
              // parametre2 est reporté dans Ligne k1
              k1 = plusPetitCoeff[1]

              numeroLigneRestante = ligneRestante(k0 + 1, k1 + 1)

              // Et on reporte dans Ligne restante pour vérifier non égalité.
              // Il faut vérifier dans quel ordre a été donné l’eq param
              report = ''
              if (Number(tabPermut1[k1]) === 0) {
                report = substitution([A.coord[k1], u.coord[k1]], ['', parametre1], [String(resultat1.mathq)])
              } else {
                report = substitution([u.coord[k1], A.coord[k1]], [parametre1, ''], [String(resultat1.mathq)])
              }

              report = report + '=' + tabExpression2[k1]
              resultat2 = fraction((-AC.coord[k1] + u.coord[k1] * resultat1.valeur) / v.coord[k1])
              // resultat 2 est un objet

              membre1 = ''
              membre2 = ''
              if (Number(tabPermut1[numeroLigneRestante - 1]) === 0) {
                membre1 = substitution([A.coord[numeroLigneRestante - 1], u.coord[numeroLigneRestante - 1]], ['', parametre1], [String(resultat1.mathq)])
              } else {
                membre1 = substitution([u.coord[numeroLigneRestante - 1], A.coord[numeroLigneRestante - 1]], [parametre1, ''], [String(resultat1.mathq)])
              }

              if (Number(tabPermut2[numeroLigneRestante - 1]) === 0) {
                membre2 = substitution([C.coord[numeroLigneRestante - 1], v.coord[numeroLigneRestante - 1]], ['', parametre2], [String(resultat2.mathq)])
              } else {
                membre2 = substitution([v.coord[numeroLigneRestante - 1], C.coord[numeroLigneRestante - 1]], [parametre2, ''], [String(resultat2.mathq)])
              }
              verification = membre1 + '=' + membre2

              j3pAffiche(stor.zoneExpli8, '', ds.textes.phrase_correction_secantes_6, { p: parametre1 + '=' + resultat1.mathq, i: k0 + 1, j: k1 + 1, k: numeroLigneRestante, q: report, r: parametre2 + '=' + resultat2.mathq, s: verification })
            }
          } else {
            // Le paramètre1 n’est pas extratible.
            // Il faut regarder si le param2 l’est

            paramDirect = []
            for (k = 0; k < 3; k++) {
              if (Number(u.coord[k]) === 0 && Number(v.coord[k]) !== 0) {
                paramDirect.push(k)
              }
            }

            if (paramDirect.length === 1) {
              j3pAfficheSysteme(stor.zoneExpli7, '', ds.textes.phrase_correction_systeme, [[L1, L2, L3]], {}, zoneExpli.style.color)
              // Il l’est. On trouve alors sa valeur qu’on reporte dans la ligne restante
              // j3pAfficheSysteme("explications","div_e7",ds.textes.phrase_correction_systeme,[[L1,L2,L3]],{styletexte:{}},me.styles.toutpetit.enonce.color);
              k0 = paramDirect[0]
              resultat2 = fraction(-AC.coord[k0] / v.coord[k0])
              plusPetitCoeff = [Math.pow(10, 99), 0]
              for (k = 0; k < 3; k++) {
                if (k === k0) {
                  k = k + 1
                }
                if (Math.abs(u.coord[k]) < plusPetitCoeff[0]) {
                  plusPetitCoeff[0] = Math.abs(u.coord[k])
                  plusPetitCoeff[1] = k
                }
              }
              // parametre2 est reporté dans Ligne k1
              k1 = plusPetitCoeff[1]

              numeroLigneRestante = ligneRestante(k0 + 1, k1 + 1)

              // Et on reporte dans Ligne restante pour vérifier non égalité.
              // Il faut vérifier dans quel ordre a été donné l’eq param
              report = ''
              if (Number(tabPermut2[k1]) === 0) {
                report = substitution([C.coord[k1], v.coord[k1]], ['', parametre2], [String(resultat2.mathq)])
              } else {
                report = substitution([v.coord[k1], C.coord[k1]], [parametre2, ''], [String(resultat2.mathq)])
              }

              report = tabExpression1[k1] + '=' + report
              resultat1 = fraction((AC.coord[k1] + v.coord[k1] * resultat2.valeur) / u.coord[k1])
              // resultat 2 est un objet

              membre1 = ''
              membre2 = ''

              if (Number(tabPermut1[numeroLigneRestante - 1]) === 0) {
                membre1 = substitution([A.coord[numeroLigneRestante - 1], u.coord[numeroLigneRestante - 1]], ['', parametre1], [String(resultat1.mathq)])
              } else {
                membre1 = substitution([u.coord[numeroLigneRestante - 1], A.coord[numeroLigneRestante - 1]], [parametre1, ''], [String(resultat1.mathq)])
              }

              if (Number(tabPermut2[numeroLigneRestante - 1]) === 0) {
                membre2 = substitution([C.coord[numeroLigneRestante - 1], v.coord[numeroLigneRestante - 1]], ['', parametre2], [String(resultat2.mathq)])
              } else {
                membre2 = substitution([v.coord[numeroLigneRestante - 1], C.coord[numeroLigneRestante - 1]], [parametre2, ''], [String(resultat2.mathq)])
              }

              verification = membre1 + '=' + membre2

              j3pAffiche(stor.zoneExpli8, '', ds.textes.phrase_correction_secantes_6, {
                p: parametre2 + '=' + resultat2.mathq,
                i: k0 + 1,
                j: k1 + 1,
                k: numeroLigneRestante,
                q: report,
                r: parametre1 + '=' + resultat1.mathq,
                s: verification
              })
              // FIn de param2 extractible
            } else {
              L1 = '$' + expressionX1 + '=' + expressionX2 + '$'
              L2 = '$' + expressionY1 + '=' + expressionY2 + '$'
              L3 = '$' + expressionZ1 + '=' + expressionZ2 + '$'
              // affichage du système
              L = []
              L[0] = L1
              L[1] = L2
              L[2] = L3
              for (k = 0; k < 3; k++) {
                // On ne touche pas à la ligne si elle est du type k=k
                if (Number(u.coord[k]) !== 0 || Number(v.coord[k]) !== 0) {
                  L[k] = sommeMonomes([u.coord[k], -v.coord[k]], [parametre1, parametre2]) + '=' + AC.coord[k]
                }
              }

              lignesChoisies = []
              maxScore = -1
              parametreADeterminer = ''
              autreParametre = ''
              leScore = 0
              coeffChoisis = []
              // var diviseur = 0
              devantParam = 0
              ctesReduites = 0
              valeurParametreADeterminer = 0
              // var det_retenu = 0
              valeurParametre1 = 0
              valeurParametre2 = 0

              for (k = 0; k < 3; k++) {
                i = k % 2
                j = ((k - 1) * (k - 2) + 2) % 3
                det = u.coord[i] * v.coord[j] - u.coord[j] * v.coord[i]

                // On cherche le couple de ligne aboutissant à un score max
                if (det !== 0) {
                  leScore = score(u.coord[i], u.coord[j]).score
                  if (leScore > maxScore) {
                    // det_retenu = det
                    valeurParametre1 = (v.coord[j] * AC.coord[i] - v.coord[i] * AC.coord[j]) / det
                    valeurParametre2 = (u.coord[j] * AC.coord[i] - u.coord[i] * AC.coord[j]) / det
                    lignesChoisies = [i + 1, j + 1]
                    coeffChoisis = score(u.coord[i], u.coord[j]).coeffs
                    maxScore = leScore
                    parametreADeterminer = parametre2// paramètre à déterminer
                    autreParametre = parametre1
                    valeurAutreParametre = valeurParametre1
                    // On réduit alors le parametre 2 à l’issue de cette combi linéaire
                    devantParam = -coeffChoisis[0] * v.coord[i] - coeffChoisis[1] * v.coord[j]
                    ctesReduites = coeffChoisis[0] * AC.coord[i] + coeffChoisis[1] * AC.coord[j]
                    valeurParametreADeterminer = fraction(ctesReduites / devantParam).mathq
                    // valeur du coeff pour le report de la valeur du parametre
                    reportLigne = i

                    if (Math.abs(v.coord[i]) > Math.abs(v.coord[j])) {
                      // On choisit la ligne j comme report
                      reportLigne = j
                    }

                    // [u.coord[report_lig
                    // On calcule
                  }
                  leScore = score(v.coord[i], v.coord[j]).score
                  if (leScore > maxScore) {
                    // det_retenu = det
                    valeurParametre1 = (v.coord[j] * AC.coord[i] - v.coord[i] * AC.coord[j]) / det
                    valeurParametre2 = (u.coord[j] * AC.coord[i] - u.coord[i] * AC.coord[j]) / det
                    lignesChoisies = [i + 1, j + 1]
                    coeffChoisis = score(v.coord[i], v.coord[j]).coeffs
                    maxScore = leScore
                    parametreADeterminer = parametre1// paramètre à déterminer
                    autreParametre = parametre2
                    valeurAutreParametre = valeurParametre2
                    devantParam = coeffChoisis[0] * u.coord[i] + coeffChoisis[1] * u.coord[j]
                    ctesReduites = coeffChoisis[0] * AC.coord[i] + coeffChoisis[1] * AC.coord[j]
                    valeurParametreADeterminer = fraction(ctesReduites / devantParam).mathq
                    reportLigne = i
                    if (Math.abs(u.coord[i]) > Math.abs(u.coord[j])) {
                      // On choisit la ligne j comme report
                      reportLigne = j
                    }
                    // var report=substitution2(L[reportLigne],[parametreADeterminer],[String(valeurParametreADeterminer)])//[u.coord[reportLigne],-v.coord[reportLigne]],[parametre1,parametre2],[String(valeur_parametre),parametre2])+"="+AC.coord[reportLigne]
                  }
                }
              }

              // On connait donc le couple de lignes à choisir, on définit les coeffs

              report = substitution2(L[reportLigne], [parametreADeterminer], [String(valeurParametreADeterminer)])

              combiLignes = sommeMonomes(coeffChoisis, ['L' + lignesChoisies[0], 'L' + lignesChoisies[1]])
              // var diviseur=coeffChoisis[0]
              isolParam = devantParam + parametreADeterminer + '=' + ctesReduites
              afficheResultatParam = parametreADeterminer + '=' + valeurParametreADeterminer
              valeurAutreParametre = autreParametre + '=' + fraction(valeurAutreParametre).mathq

              numeroLigneRestante = ligneRestante(lignesChoisies[0], lignesChoisies[1])

              // var verification=sommeMonomes([u.coord[numeroLigneRestante-1],-v.coord[numeroLigneRestante-1]],[parametre1,parametre2])
              verification = L[numeroLigneRestante - 1]
              verification = substitution2(verification, [parametre1, parametre2], [String(fraction(valeurParametre1).mathq), String(fraction(valeurParametre2).mathq)])// +"="+AC.coord[numeroLigneRestante-1]

              verification = substitution2(verification, [parametre1, parametre2], [String(fraction(valeurParametre1).mathq), String(fraction(valeurParametre2).mathq)])

              j3pAfficheSysteme(stor.zoneExpli9, '', ds.textes.phrase_correction_mise_en_forme_systeme, [[L1, L2, L3], ['$' + L[0] + '$&nbsp;&nbsp; $L1$', '$' + L[1] + '$&nbsp;&nbsp;$L2$', '$' + L[2] + '$&nbsp;&nbsp;$L3$']], {}, zoneExpli.style.color)
              ;[10, 11, 12].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
              // On calcule le premier paramètre
              j3pAffiche(stor.zoneExpli10, '', ds.textes.phrase_correction_combiLignes, { a: combiLignes, b: isolParam, c: afficheResultatParam })
              // Dont on reporte la valeur dans la ligne où le coefficient devant le parametre restant est le plus simple
              // phrase_correction_report:"En reportant dans L£j, on obtient $£q$"
              j3pAffiche(stor.zoneExpli11, '', ds.textes.phrase_correction_report, { j: reportLigne + 1, p: report, q: valeurAutreParametre })
              j3pAffiche(stor.zoneExpli12, '', ds.textes.phrase_correction_secantes_7, { k: numeroLigneRestante, p: verification })
            }
          }
        }
        // Dans tous les cas, il faut afficher la conclusion et les coordonnées du point d’intersection

        resultat1 = String(fraction(valeurParametre1).mathq)
        // On n’affiche pas le calcul s’il n’y a pas de substitution du paramètre 1 à faire dans l’expression de x, y ou z
        let expressionxE = 'x=' + expressionX1
        if (Number(u.coord[0]) !== 0) {
          expressionxE = 'x=' + substitution2(expressionX1, [parametre1], [resultat1]) + '=' + fraction(E.coord[0]).mathq
        }

        let expressionyE = 'y=' + expressionY1
        if (Number(u.coord[1]) !== 0) {
          expressionyE = 'y=' + substitution2(expressionY1, [parametre1], [resultat1]) + '=' + fraction(E.coord[1]).mathq
        }

        let expressionzE = 'z=' + expressionZ1
        if (Number(u.coord[2]) !== 0) {
          expressionzE = 'z=' + substitution2(expressionZ1, [parametre1], [resultat1]) + '=' + fraction(E.coord[2]).mathq
        }
        const zoneExpli13 = j3pAddElt(zoneExpli, 'div')
        // j3pAffiche("div_e13","span33",ds.textes.phrase_correction_secantes_7,{k:numeroLigneRestante,p:verification,styletexte:{}});
        j3pAfficheSysteme(zoneExpli13, '', ds.textes.phrase_correction_secantes_8, [['$' + expressionxE + '$', '$' + expressionyE + '$', '$' + expressionzE + '$']], { a: fraction(E.coord[0]).mathq, b: fraction(E.coord[1]).mathq, c: fraction(E.coord[2]).mathq }, zoneExpli.style.color)
      }// fin sécantes
    }
    stor.zoom = -5
    // Création du plan (ABC)
    //       //On crée les sommets d’un parallélogramme centré en A:
    const lambda = 10
    const P = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]
    const vAB = u.coord
    if (objSolution.precision !== 'confondues') {
      stor.vAC = [C.coord[0] - A.coord[0], C.coord[1] - A.coord[1], C.coord[2] - A.coord[2]]
    } else {
      stor.vAC = produitVectoriel(vAB, [10, 10, 10])
    }

    const mesVecteurs = [vAB, vAB, stor.vAC, stor.vAC]

    //
    /// /
    /// /
    for (k = 0; k < 4; k++) {
      /// /
      for (let m = 0; m < 3; m++) {
        /// /
        P[k][m] = A.coord[m] + Math.pow(-1, k) * lambda * mesVecteurs[k][m]
      }
    }
    let affiche = true
    if (objSolution.cas === 'param') {
      affiche = false
    }

    const tabObjetsATracer = [{ nom: A.nom, type: 'point', coord: A.coord, couleur: 'vert', visible: affiche }]
    tabObjetsATracer.push({ nom: B.nom, type: 'point', coord: B.coord, couleur: 'vert', visible: affiche })
    tabObjetsATracer.push({ nom: C.nom, type: 'point', coord: C.coord, couleur: 'vert', visible: affiche })
    tabObjetsATracer.push({ nom: D.nom, type: 'point', coord: D.coord, couleur: 'vert', visible: affiche })
    tabObjetsATracer.push({ nom: 'AB', type: 'droite', ext1: A.nom, ext2: B.nom, couleur: '#0000AA', pointilles: false, visible: true })
    tabObjetsATracer.push({ nom: 'CD', type: 'droite', ext1: C.nom, ext2: D.nom, couleur: '#0000AA', pointilles: false, visible: true })
    // On rajoute les vecteurs
    tabObjetsATracer.push({ nom: 'u', type: 'vecteur', ext1: A.nom, ext2: B.nom, couleur: '#00FF00', pointilles: false, visible: true })
    tabObjetsATracer.push({ nom: 'v', type: 'vecteur', ext1: C.nom, ext2: D.nom, couleur: '#CCFF00', pointilles: false, visible: true })

    const objets1 = {
      faces_cachees: false,
      dim: { larg: 600, haut: 300, zoom: stor.zoom },
      sommets: [[], P[0], P[2], P[1], P[3]],
      faces: [[1, 2, 3, 4]],
      nomsommets: ['', 'a', 'b', 'c', 'd'],
      mef: {
        typerotation: 'Ox',
        couleursommets: { defaut: '#F00', A: '#000' },
        couleurfaces: { defaut: '#92BBD8', f0: '#FF0000' }
      },
      objets: tabObjetsATracer
    }
    const div3D = j3pGetNewId('div3D')
    j3pAddElt(stor.fenetre3D, 'div', '', { id: div3D })
    stor.solide1 = new _3D(div3D, objets1)
    stor.solide1.rotation = { Ox: 0, Oy: 20, Oz: 30 }
    stor.solide1.arotation = stor.solide1.rotation
    stor.solide1.construit()
    j3pToggleFenetres(stor.Btns)
  }

  function getDonnees () {
    return {
      typesection: 'lycee', //

      nbrepetitions: 4,
      nbetapes: 1,
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      tab_paralleles: [], // Pour détecter le tableau des parallèles, pas un paramètre
      nb_paralleles: 0,
      tab_fact_colinearite: [],
      tab_systeme_simple: [],
      //
      // Autant de chance de donner la droite par deux points que par sa représentation paramétrique
      // probas_position=[]
      position: [0.1, 0.2, 0.4, 0.3], // confondues, strict paralleles,sécantes, non cop
      facteurColinearite: [0.67, 0.33], // En cas de colinéarité des vecteurs directeurs, le facteur est fractionnaire vs ent
      permute_eq_param: 0.25,
      choixCorrection: 1,
      systeme_simple: [0.5, 0.5], // systeme simple ssi l’un au moins des coeffs est nul.//la première donnée est la proba de systeme simple et l’autre de systeme sans coeffs nuls

      //

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,
      numero_essai: 0,
      nb_erreur_detection_parallelisme: 0,
      nb_erreur_resol_systeme: 0,
      nb_erreur: 0,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      textes: {
        titreExo: 'Positions relative de deux droites',
        titre3D: 'Illustration dans l’espace',
        affichage_espace: 'Voir la figure',
        phrase_enonce: 'Dans un repère de l’espace, on considère les droites $d_1$ et $d_2$ d’équations paramétriques respectives:',
        phrase_enonce2: '‡1‡, $£a \\in \\R$ et ‡2‡, $£b \\in \\R$',
        phrase_enonce3: 'Les droites $d_1$ et $d_2$ sont :',
        phrase_oui: 'oui',
        phrase_non: 'non',
        pointInter: 'Coordonnées du point d’intersection (&1&;&2&;&3&)',
        phrase_faux: 'C’est faux!<br>',
        indications_test_parallelisme: 'Les droites sont-elles parallèles?',
        indications_erreur_pt_inter: 'Les droites sont effectivement sécantes, mais les coordonnées du point d’intersection sont fausses.',
        phrase_correction_dirige: '$£E$ passe par $£A(£i;£j;£k)$ et est dirigée par $£u(£a;£b;£c)$.<br>$£F$ passe par $£B(£l;£m;£n)$ et est dirigée par $£v(£d;£e;£f)$.<br>',
        phrase_correction_regle: '$£E$ et $£F$ sont parallèles si et seulement si $£u$ et $£v$ sont colinéaires.',
        phrase_correction_colinearite: 'On constate que $£p$',
        phrase_correction_constat_col_detail: 'On constate que : <br>$£a$ <br> $£b$ <br> $£c$ <br>',
        phrase_correction_constat_col_detail_alexis: 'On constate que : <br>$£a$  et  $£b$  donc  $£c$ <br> $£d$  et  $£e$  donc  $£f$ <br> $£g$  et  $£h$  donc  $£i$',
        phrase_correction_conclusion_col: 'Ainsi, les vecteurs $£u$ et $£v$ sont colinéaires.',
        phrase_correction_paralleles: 'Donc les droites $£E$ et $£F$ sont parallèles',
        phrase_correction_constat_non_col_alexis: 'On constate que : <br>$£a$  et  $£b$  donc  $£c$',
        phrase_correction_conclusion_non_col: 'Ainsi, les vecteurs $£u$ et $£v$ ne sont pas colinéaires.',
        phrase_correction_non_paralleles: 'Donc les droites $£E$ et $£F$ ne sont pas parallèles.',
        phrase_correction_trancher_conf_strict: 'Reste à déterminer si elles sont confondues ou strictement parallèles.<br>C’est-à-dire si le point $£A$ appartient à la droite $d_2$:',
        phrase_correction_resol_A_sur_d2: 'Or ‡1‡ $\\Rightarrow$  ‡2‡',
        phrase_correction_constat_A_sur_d2: 'Ainsi, le point $£A$ appartient à $d_2$. Donc $d_1$ et $d_2$ sont confondues.',
        phrase_correction_constat_A_pas_sur_d2: 'Ainsi, le point $£A$ n’appartient pas à $d_2$. Donc $d_1$ et $d_2$ sont strictement parallèles.',
        phrase_correction_trancher_sec_non_cop: 'Reste à déterminer si elles sont sécantes ou non coplanaires.<br>C’est-à-dire si le système suivant, de 3 équations à 2 inconnues ($£a$ et $£b$) a ou non des solutions :',
        phrase_correction_systeme: '<br>‡1‡',
        phrase_correction_ligneNonVerifiee: '$L£k$ n’est pas vérifiée, donc les droites $d_1$ et $d_2$ sont non coplanaires.',
        phrase_correction_parametres_incompatibles: '$L£i$ donne $£p=£a$ et $L£j$ donne $£p=£b$, ce qui est impossible.<br>Donc $d_1$ et $d_2$ sont non coplanaires.',
        phrase_correction_les_deux_exctactibles: '$L£i$ donne $£p$.<br>L£j donne $£q$.<br>$L£k$ n’est pas vérifiée car $£r$.<br>Donc $d_1$ et $d_2$ sont non coplanaires.',
        phrase_correction_report_dans_ligne: '$L£i$ donne $£p$.<br>En reportant dans $L£j$, on obtient $£q$, d’où $£r$<br>$L£k$ n’est pas vérifiée car $£s$.<br>Donc $d_1$ et $d_2$ sont non coplanaires.',
        phrase_correction_mise_en_forme_systeme: '<br> ‡1‡ soit ‡2‡',
        phrase_correction_combiLignes: '$£a$ donne $£b$ d’où $£c$',
        phrase_correction_report: 'En reportant dans $L£j$, on obtient $£p$ d’où $£q$.',
        phrase_correction_non_verifiee: '$L£k$ n’est pas vérifiée car $£p$.<br>Donc $d_1$ et $d_2$ sont non coplanaires.',
        phrase_correction_secantes_1: '$L£i$ donne $£p=£a$ et $L£j$ donne $£p=£a$<br>',
        phrase_correction_secantes_2: '$L£i$ donne $£p=£a$.',
        phrase_correction_secantes_3: 'En reportant dans $L£i$ on obtient $£p$, d’où $£q$.<br>Le système admet donc une unique solution.<br>',
        phrase_correction_secantes_4: 'Le système a une unique solution. Les droites sont donc sécantes en $£A$',
        phrase_correction_secantes_5: '$L£i$ donne $£p$.<br>L£j donne $£q$.<br>L£k est vérifiée car $£r$.<br>Le système admet donc une unique solution.',
        phrase_correction_secantes_6: '$L£i$ donne $£p$.<br>En reportant dans $L£j$, on obtient $£q$, d’où $£r$<br>$L£k$ est vérifiée car $£s$.<br>Le système admet donc une unique solution.',
        phrase_correction_secantes_7: '$L£k$ est vérifiée car $£p$.<br>Le système admet donc une unique solution.<br>',
        phrase_correction_secantes_8: 'Ainsi $d_1$ et $d_2$ sont sécantes en :‡1‡, soit le point de coordonnées $(£a;£b;£c)$',
        zones_a_completer: 'Il faut compléter les zones de saisie',
        qcm_a_completer: 'Il faut choisir une réponse'
      },
      pe: 0
    }
  }

  function entreBornes (x) {
    // renvoie true si abs(x)<=bornes_coordonnees
    return (Math.abs(x) <= stor.borneCoordonnees)
  }
  function enonceMain () {
    // On souhaite tomber sur des coordonnées entières.
    stor.borneCoordonnees = 10
    const borneFacteurColinearite = 30
    let facteurColinearite = 0
    let xA, yA, zA, coordA, xU, yU, zU, coordU, xW, yW, zW, coordW, i, j
    let xB, yB, zB, coordB, xC, yC, zC, coordC, xD, yD, zD, coordD, xV, yV, zV, coordV, t1, t2
    let coordEntreBornes, position, coordDEntieres, conditionSysteme
    if (stor.probasPosition[me.questionCourante - 1] === 'confondues') {
      // Elles sont confondues
      let coordEntieres
      do {
        // On commence par générer un point A :

        xA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordA = [xA, yA, zA]

        if (stor.tabFactColinearite[me.questionCourante - 1]) {
          // On génére un facteur de colinéarité non entier et d’inverse non entier
          do {
            facteurColinearite = entierAlea(-borneFacteurColinearite, borneFacteurColinearite, [0, 6, -6]) / 6
          }
          while (estEntier(facteurColinearite) || estEntier(1 / facteurColinearite))
        } else {
          do {
            facteurColinearite = entierAlea(-borneFacteurColinearite, borneFacteurColinearite, [0, 6, -6]) / 6
          }
          while (!estEntier(facteurColinearite))
        }

        const leNum = fraction(facteurColinearite).num
        const leDen = fraction(facteurColinearite).denom
        let k
        do {
          i = entierAlea(-10, 10, [0])
          j = entierAlea(-10, 10, [0])
          k = entierAlea(-10, 10, [0])
        }
        while (i === 0 && j === 0 && k === 0)

        xU = leDen * i
        yU = leDen * j
        zU = leDen * k
        coordU = [xU, yU, zU]

        xB = xA + xU
        yB = yA + yU
        zB = zA + zU
        coordB = [xB, yB, zB]

        // On choisit à présent un point C sur la droites (AB):

        t1 = entierAlea(-10, 10, [0, 2]) / 2
        xC = xA + t1 * xU
        yC = yA + t1 * yU
        zC = zA + t1 * zU
        coordC = [xC, yC, zC]

        xD = xC + leNum * i
        yD = yC + leNum * j
        zD = zC + leNum * k
        coordD = [xD, yD, zD]
        // var vrai=tableauVrai(fonctionTab(estEntier,[xC,yC,zC,xD,yD,zD]))

        let aTester = coordB
        aTester = aTester.concat(coordC)
        aTester = aTester.concat(coordD)

        coordEntreBornes = tableauVrai(fonctionTab(entreBornes, aTester))

        coordEntieres = tableauVrai(fonctionTab(estEntier, aTester))
      } while (compareTabNum(coordA, coordD) || compareTabNum(coordB, coordD) || !coordEntreBornes || !coordEntieres)

      xV = xD - xC
      yV = yD - yC
      zV = zD - zC
      coordV = [xV, yV, zV]

      position = 'confondues'
    }

    if (stor.probasPosition[me.questionCourante - 1] === 'strictement parallèles') {
      do {
        xA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordA = [xA, yA, zA]

        xB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordB = [xB, yB, zB]

        xC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordC = [xC, yC, zC]
        stor.vAC = [xC - xA, yC - yA, zC - zA]

        xU = xB - xA
        yU = yB - yA
        zU = zB - zA
        coordU = [xU, yU, zU]

        if (stor.tabFactColinearite[me.questionCourante - 1]) {
          // On le souhaite fractionnaire

          do {
            facteurColinearite = entierAlea(-borneFacteurColinearite, borneFacteurColinearite, [0, 6, -6]) / 6
          }
          while (estEntier(facteurColinearite) || estEntier(1 / facteurColinearite))
        } else {
          do {
            facteurColinearite = entierAlea(-borneFacteurColinearite, borneFacteurColinearite, [0, 6, -6]) / 6
          }
          while (!estEntier(facteurColinearite))
        }

        xD = xC + facteurColinearite * xU
        yD = yC + facteurColinearite * yU
        zD = zC + facteurColinearite * zU

        coordD = [xD, yD, zD]
        coordDEntieres = tableauVrai(fonctionTab(estEntier, [xD, yD, zD]))
        coordEntreBornes = tableauVrai(fonctionTab(entreBornes, coordD))

        xV = xD - xC
        yV = yD - yC
        zV = zD - zC
        coordV = [xV, yV, zV]
      }
      while (compareTabNum(coordA, coordB) || compareTabNum(coordA, coordC) || compareTabNum(coordA, coordD) || compareTabNum(coordB, coordC) || compareTabNum(coordB, coordD) || compareTabNum(coordC, coordD) || (!coordDEntieres) || (!coordEntreBornes) || UcolV(coordU, stor.vAC))
      position = 'strictement parallèles'
    }
    let xE, yE, zE
    if (stor.probasPosition[me.questionCourante - 1] === 'sécantes') {
      // On veut (AB) et (CD) sécantes.
      // On choisit un point E distinct de A et B sur la droite (AB), puis un point C non alignés avec A et B.
      // D est enfin choisi distinct de C au hasard sur la droite (EC).

      do {
        xA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordA = [xA, yA, zA]

        xB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordB = [xB, yB, zB]

        xU = xB - xA
        yU = yB - yA
        zU = zB - zA
        coordU = [xU, yU, zU]
        t1 = entierAlea(-10, 10, [0, 2]) / 2
        xE = xA + t1 * xU
        yE = yA + t1 * yU
        zE = zA + t1 * zU
        // var coordE = [xE, yE, zE]

        xC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordC = [xC, yC, zC]

        xW = xE - xC
        yW = yE - yC
        zW = zE - zC
        coordW = [xW, yW, zW]

        t2 = entierAlea(-10, 10, [0]) / 2
        xD = xC + t2 * xW
        yD = yC + t2 * yW
        zD = zC + t2 * zW

        coordD = [xD, yD, zD]
        coordDEntieres = tableauVrai(fonctionTab(estEntier, [xD, yD, zD]))
        coordEntreBornes = tableauVrai(fonctionTab(entreBornes, coordD))

        xV = xD - xC
        yV = yD - yC
        zV = zD - zC
        coordV = [xV, yV, zV]
        // On rajoute un test eventuel de simplicité de système

        if (stor.tabSystemeSimple[me.questionCourante - 1]) {
          // On le veut donc simple.
          conditionSysteme = (xU * yU * zU * xV * yV * zV === 0)
        } else {
          conditionSysteme = (xU * yU * zU * xV * yV * zV !== 0)
        }
      }
      while (UcolV(coordU, coordV) || compareTabNum(coordA, coordB) || compareTabNum(coordA, coordC) || compareTabNum(coordA, coordD) || compareTabNum(coordB, coordC) || compareTabNum(coordB, coordD) || !coordDEntieres || !coordEntreBornes || !conditionSysteme)
      position = 'sécantes'
    }

    if (stor.probasPosition[me.questionCourante - 1] === 'non coplanaires') {
      do {
        xA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zA = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordA = [xA, yA, zA]

        xB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zB = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordB = [xB, yB, zB]

        xU = xB - xA
        yU = yB - yA
        zU = zB - zA
        coordU = [xU, yU, zU]

        xC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zC = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordC = [xC, yC, zC]

        xD = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        yD = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        zD = entierAlea(-stor.borneCoordonnees, stor.borneCoordonnees, [])
        coordD = [xD, yD, zD]

        xV = xD - xC
        yV = yD - yC
        zV = zD - zC
        coordV = [xV, yV, zV]

        xW = xD - xA
        yW = yD - yA
        zW = zD - zA
        coordW = [xW, yW, zW]
        // on traduit A,B,C,D coplanaires par det(AB,CD,AD)=0
        // On rajoute un test eventuel de simplicité de système

        if (stor.tabSystemeSimple[me.questionCourante - 1]) {
          // On le veut donc simple.
          conditionSysteme = (xU * yU * zU * xV * yV * zV === 0)
        } else {
          conditionSysteme = (xU * yU * zU * xV * yV * zV !== 0)
        }
      }
      while (determinant3D(coordU, coordV, coordW) === 0 || compareTabNum(coordA, coordB) || compareTabNum(coordA, coordC) || compareTabNum(coordA, coordD) || compareTabNum(coordB, coordC) || compareTabNum(coordB, coordD) || !conditionSysteme)
      position = 'non coplanaires'
    }
    const tabDesChoix = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'L', 'M', 'N', 'R']
    // Au cas où il y ait un point d’intersection
    const mesPoints = j3pGetRandomElts(tabDesChoix, 5)

    const tabParametresPossibles = ['a', 'b', 'c', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't']
    const parametre = j3pGetRandomElts(tabParametresPossibles, 2)
    const param1 = parametre[0]

    const param2 = parametre[1]

    // d1 est (AB) d2 est (CD) et le point d’interection éventuel est E.
    const A = {}
    A.nom = mesPoints[0]
    A.coord = [xA, yA, zA]
    // A.coord=[1,-2,4]

    const B = {}
    B.nom = mesPoints[1]
    B.coord = [xB, yB, zB]
    // B.coord=[-8,-2,-1]

    const C = {}
    C.nom = mesPoints[2]
    C.coord = [xC, yC, zC]
    // C.coord=[-8,-7,0]

    const D = {}
    D.nom = mesPoints[3]
    D.coord = [xD, yD, zD]
    // D.coord=[3,-7,-1]

    const E = {}
    E.nom = mesPoints[4]
    E.coord = [-1000000000, -1000000000, -1000000000]
    if (position === 'sécantes') {
      E.coord = [xE, yE, zE]
    }
    const AC = {}
    AC.nom = vLatex(mesPoints[0] + mesPoints[2])
    AC.coord = [C.coord[0] - A.coord[0], C.coord[1] - A.coord[1], C.coord[2] - A.coord[2]]

    const u = {}
    u.nom = '\\vec{u}'
    u.coord = coordU
    // u.coord=[3,2,2]
    // u.coord=[B.coord[0]-A.coord[0],B.coord[1]-A.coord[1],B.coord[2]-A.coord[2]]
    const v = {}
    v.nom = '\\vec{v}'
    v.coord = coordV
    // v.coord=[-2,0,-5]
    // v.coord=[D.coord[0]-C.coord[0],D.coord[1]-C.coord[1],D.coord[2]-C.coord[2]]

    let choix = Math.random()
    const permut = 1 - ds.permute_eq_param
    const tabPermut1 = []
    const tabPermut2 = []
    let x1, y1, z1, x2, y2, z2
    if (choix < permut) {
      // J’aurais du créer un objet x1... tant pis, ce sera pour la prochaine section :(
      x1 = sommeMonomes([A.coord[0], u.coord[0]], ['', param1])
      tabPermut1.push(0)
    } else {
      x1 = sommeMonomes([u.coord[0], A.coord[0]], [param1, ''])
      tabPermut1.push(1)
    }

    choix = Math.random()
    if (choix < permut) {
      y1 = sommeMonomes([A.coord[1], u.coord[1]], ['', param1])
      tabPermut1.push(0)
    } else {
      y1 = sommeMonomes([u.coord[1], A.coord[1]], [param1, ''])
      tabPermut1.push(1)
    }

    choix = Math.random()
    if (choix < permut) {
      z1 = sommeMonomes([A.coord[2], u.coord[2]], ['', param1])
      tabPermut1.push(0)
    } else {
      z1 = sommeMonomes([u.coord[2], A.coord[2]], [param1, ''])
      tabPermut1.push(1)
    }

    choix = Math.random()

    if (choix < permut) {
      x2 = sommeMonomes([C.coord[0], v.coord[0]], ['', param2])
      tabPermut2.push(0)
    } else {
      x2 = sommeMonomes([v.coord[0], C.coord[0]], [param2, ''])
      tabPermut2.push(1)
    }

    choix = Math.random()
    if (choix < permut) {
      y2 = sommeMonomes([C.coord[1], v.coord[1]], ['', param2])
      tabPermut2.push(0)
    } else {
      y2 = sommeMonomes([v.coord[1], C.coord[1]], [param2, ''])
      tabPermut2.push(1)
    }

    choix = Math.random()
    if (choix < permut) {
      z2 = sommeMonomes([C.coord[2], v.coord[2]], ['', param2])
      tabPermut2.push(0)
    } else {
      z2 = sommeMonomes([v.coord[2], C.coord[2]], [param2, ''])
      tabPermut2.push(1)
    }

    stor.solution = {}
    stor.solution.A = A
    stor.solution.B = B
    stor.solution.C = C
    stor.solution.D = D
    stor.solution.E = E
    stor.solution.AC = AC

    stor.solution.u = u
    stor.solution.v = v
    stor.solution.lambda = facteurColinearite
    stor.solution.position = position
    stor.solution.d1 = 'd_1'
    stor.solution.d2 = 'd_2'
    stor.solution.parametre = parametre
    stor.solution.expressionX1 = x1
    stor.solution.expressionY1 = y1
    stor.solution.expressionZ1 = z1
    stor.solution.expressionX2 = x2
    stor.solution.expressionY2 = y2
    stor.solution.expressionZ2 = z2
    stor.solution.tabPermut1 = tabPermut1
    stor.solution.tabPermut2 = tabPermut2

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let k = 1; k <= 4; k++) stor['zoneCons' + k] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.phrase_enonce)
    //
    j3pAfficheSysteme(stor.zoneCons2, '', ds.textes.phrase_enonce2, [['$x=' + x1 + '$', '$y=' + y1 + '$', '$z=' + z1 + '$'], ['$x=' + x2 + '$', '$y=' + y2 + '$', '$z=' + z2 + '$']], {
      a: param1,
      b: param2
    }, me.styles.toutpetit.enonce.color)
    j3pAffiche(stor.zoneCons3, '', ds.textes.phrase_enonce3)
    stor.idsRadio = []
    stor.nameRadio = j3pGetNewId('choix')
    stor.divRadio = []
    for (i = 1; i <= 4; i++) {
      stor.idsRadio.push(j3pGetNewId('radio' + i))
      stor.divRadio.push(j3pAddElt(stor.zoneCons3, 'div'))
    }
    j3pBoutonRadio(stor.divRadio[0], stor.idsRadio[0], stor.nameRadio, 0, ' confondues ')
    j3pBoutonRadio(stor.divRadio[1], stor.idsRadio[1], stor.nameRadio, 1, ' strictement parallèles ')
    j3pBoutonRadio(stor.divRadio[2], stor.idsRadio[2], stor.nameRadio, 2, ' sécantes ')
    j3pBoutonRadio(stor.divRadio[3], stor.idsRadio[3], stor.nameRadio, 4, ' non coplanaires ')
    stor.btnsActifs = true
    for (let k = 1; k <= 4; k++) {
      stor.divRadio[k - 1].addEventListener('click', afficherPointInter)
    }

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':

      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        stor.probasPosition = creeTableauAleatoire(['confondues', 'strictement parallèles', 'sécantes', 'non coplanaires'], ds.position, ds.nbrepetitions)
        // On détecte les cas de parallélisme
        for (let k = 0; k < ds.nbrepetitions; k++) {
          if (stor.probasPosition[k] === 'confondues' || stor.probasPosition[k] === 'strictement parallèles') {
            ds.tab_paralleles.push(true)
          } else {
            ds.tab_paralleles.push(false)
          }
        }

        // Si il y a plusieurs cas de parallélisme, on fait en sorte d’avoir des facteurs de colinéarité entiers et fractionnaires.
        stor.tabFactColinearite = creerTableauDe(true, ds.nbrepetitions)

        let occ = occurences(true, ds.tab_paralleles)

        let remplace = creeTableauAleatoire([true, false], ds.facteurColinearite, occ.nombre)

        for (let k = 0; k < occ.nombre; k++) {
          stor.tabFactColinearite[occ.emplacements[k]] = remplace[k]
        }

        // On crée les systèmes simples ou non à résoudre.
        stor.tabSystemeSimple = creerTableauDe(true, ds.nbrepetitions)
        // On détecte le non parallélisme
        occ = occurences(false, ds.tab_paralleles)
        ds.nb_paralleles = occ.nombre
        remplace = creeTableauAleatoire([true, false], ds.systeme_simple, occ.nombre)

        for (let k = 0; k < occ.nombre; k++) {
          stor.tabSystemeSimple[occ.emplacements[k]] = remplace[k]
        }

        // Construction de la page
        me.construitStructurePage(ds.structure)

        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        // Type d’erreurs recensées ;:
        // non detection parallélisme
        // Problème de résolution de système
        me.score = 0

        me.afficheTitre(ds.textes.titreExo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        stor.fenetreBtns = j3pGetNewId('fenetreBoutons')
        stor.fenetre3D = j3pGetNewId('fenetre3D')
        stor.la3D = j3pGetNewId('la3D')
        stor.Btns = j3pGetNewId('Btns')
        me.fenetresjq = [
          // Attention : positions par rapport à mepact
          { name: stor.Btns, title: ds.textes.titre3D, left: 660, top: 430, height: 120, id: stor.fenetreBtns },
          { name: stor.la3D, title: '3D', width: 730, top: 120, left: 90, height: 500, id: stor.fenetre3D }
        ]
        // Création mais sans affichage.
        j3pCreeFenetres(me)
        j3pCreeBoutonFenetre(stor.la3D, stor.fenetreBtns, { top: 10, left: 10 }, ds.textes.affichage_espace)
        j3pAfficheCroixFenetres(stor.la3D)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        j3pEmpty(stor.fenetre3D)
        j3pMasqueFenetre(stor.Btns)
        j3pMasqueFenetre(stor.la3D)
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////

      enonceMain()

      break // case "enonce":

    case 'correction':
      {
        let reponse = {
          aRepondu: false,
          bonneReponse: false
        }
        const objSol = stor.solution

        let repQCM = ''
        if (j3pElement(stor.idsRadio[0]).checked) repQCM = 'confondues'
        if (j3pElement(stor.idsRadio[1]).checked) repQCM = 'strictement parallèles'
        if (j3pElement(stor.idsRadio[2]).checked) repQCM = 'sécantes'
        if (j3pElement(stor.idsRadio[3]).checked) repQCM = 'non coplanaires'

        // En cas de sécantes :
        const reponseEleve = {}
        if (repQCM === 'sécantes') {
          let repEleveX = j3pValeurde(stor.zoneInput[0])
          repEleveX = j3pMathquillXcas(repEleveX)

          let repEleveY = j3pValeurde(stor.zoneInput[1])
          repEleveY = j3pMathquillXcas(repEleveY)

          let repEleveZ = j3pValeurde(stor.zoneInput[2])
          repEleveZ = j3pMathquillXcas(repEleveZ)
          reponseEleve.intersection = [repEleveX, repEleveY, repEleveZ]
        }

        reponseEleve.QCM = repQCM

        const laValidation = validation(reponseEleve, objSol)

        // On teste si une réponse a été saisie
        let aReponduQcm = false
        // if (j3pElement("bill1").checked||j3pElement("bill2").checked){
        if (j3pElement(stor.idsRadio[0]).checked || j3pElement(stor.idsRadio[1]).checked || j3pElement(stor.idsRadio[2]).checked || j3pElement(stor.idsRadio[3]).checked) {
          aReponduQcm = true
        }
        reponse.aRepondu = aReponduQcm
        if (aReponduQcm) {
          me.logIfDebug('laValidation.QCM=', laValidation.QCM)
          me.logIfDebug('objSol.position =', objSol.position)

          if (j3pElement(stor.idsRadio[2]).checked || (laValidation.QCM && objSol.position === 'sécantes')) {
            for (let i = 1; i <= 3; i++) {
              const eltInput = stor.zoneInput[i - 1]
              if (!Object.isFrozen(eltInput)) {
                eltInput.typeReponse = ['nombre', 'exact']
                eltInput.reponse = [objSol.E.coord[i - 1]]
              }
            }
            const fctsValid = stor.fctsValid
            reponse = fctsValid.validationGlobale()
          }
        }
        if (reponse.aRepondu && laValidation.QCM) {
          for (let k = 1; k <= 4; k++) {
            j3pElement(stor.idsRadio[k - 1]).disabled = true
            if (j3pElement(stor.idsRadio[k - 1]).checked) j3pElement('label' + stor.idsRadio[k - 1]).style.color = me.styles.cbien
          }
        }
        if (!reponse.aRepondu) {
          stor.zoneCorr.style.color = me.styles.cfaux
          if (!aReponduQcm) {
            stor.zoneCorr.innerHTML = ds.textes.qcm_a_completer
          // il faut répondre au QCM
          } else {
            stor.zoneCorr.innerHTML = ds.textes.zones_a_completer
          }
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie
          ds.numero_essai = ds.numero_essai + 1
          ds.nb_erreur = ds.nb_erreur + laValidation.erreur
          ds.nb_erreur_detection_parallelisme = ds.nb_erreur_detection_parallelisme + laValidation.erreur_paralleles
          ds.nb_erreur_resol_systeme = ds.nb_erreur_resol_systeme + laValidation.erreur_systeme

          // Bonne réponse
          // if (j3pElement("bill1").checked ==objSol.alignes) {
          if (laValidation.juste) {
            if (stor.laPalette) j3pEmpty(stor.laPalette)
            stor.idsRadio.forEach(id => { j3pElement(id).disabled = true })
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(laValidation, objSol)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              j3pEmpty(stor.laPalette)
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br>' + ds.textes.phrase2
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux

              if (me.essaiCourant < ds.nbchances) {
              // On regarde si on peut donner une indication

                stor.zoneCorr.innerHTML += '<br>' + indications(laValidation)

              //
              } else {
              // Erreur au nème essai
              // var exact = false // Supprimé par Yves
                if (stor.laPalette) j3pEmpty(stor.laPalette)
                stor.idsRadio.forEach(id => { j3pElement(id).disabled = true })
                for (let k = 1; k <= 4; k++) {
                  if (!laValidation.QCM && j3pElement(stor.idsRadio[k - 1]).checked) {
                    j3pElement('label' + stor.idsRadio[k - 1]).style.color = me.styles.cfaux
                    j3pBarre('label' + stor.idsRadio[k - 1])
                  }
                }
                const radioRep = ['confondues', 'strictement parallèles', 'sécantes', 'non coplanaires'].indexOf(stor.solution.position)
                j3pElement('label' + stor.idsRadio[radioRep]).style.color = me.styles.petit.correction.color
                me._stopTimer()
                me.cacheBoutonValider()
                me.typederreurs[2]++
                // console.log('la reponse de valid globale=', reponse)
                afficheCorrection(laValidation, objSol)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        // On commence par regarder le pb de détection de parallélisme
        const freqErreurDetectionParallelisme = ds.nb_erreur_detection_parallelisme / ds.numero_essai
        if (freqErreurDetectionParallelisme >= 0.5) {
          me.parcours.pe = 'pb_detection_parallelisme'
        } else {
          // On regarde à présent le pb de résolution de système:
          const freqErreurResolSysteme = ds.nb_erreur_resol_systeme / ds.numero_essai
          if (freqErreurResolSysteme >= 0.5) {
            me.parcours.pe = 'pb_resol_systeme'
          } else {
            // On regarde alors si le seuil de réussite est supérieur ou égal à 75%
            const freqErreur = ds.nb_erreur / ds.numero_essai
            if (freqErreur > 0.25) {
              me.parcours.pe = 'pas bien'
            } else {
              me.parcours.pe = 'bien'
            }
          }
        }

        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
