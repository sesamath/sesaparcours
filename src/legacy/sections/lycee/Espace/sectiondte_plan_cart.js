import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pGetRandomElts, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pValeurde, j3pGetNewId } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexOppose } from 'src/legacy/core/functionsLatex'
import { j3pAfficheSysteme } from 'src/legacy/core/functionsSysteme'
import { produitVectoriel, compareTabNum, creeTableauAleatoire, pgcdZarb, fonctionTab, tableauVrai, sommeMonomes, UcolV } from 'src/legacy/outils/espace/fctsEspace'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Auteur Laurent Fréguin
    Date 1/6/2016
    etudier la position relative d’un plan défini par son équation cartésienne, et d’une droite définie par son équation param
    En avril 2021, Rémi Deniaud l’a reprise et finalisée
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['permute_eq_param', 0.8, 'reel', 'probabilité d’écrire b*t+a au lieu de a+b*t']

  ],
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'problème dans la détection du parallélisme' },
    { pe_3: 'problème dans la résolution du système' },
    { pe_4: 'Insuffisant' }
  ]

}
/**
 * section dte_plan_cart
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function ecoute (num) {
    if (stor.divPalette && stor.zoneInput[num].className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.divPalette)
      j3pPaletteMathquill(stor.divPalette, stor.zoneInput[num], { liste: ['fraction'] })
    }
  }
  function afficherPointInter () {
    if (!stor.divPalette) {
      j3pEmpty(stor.zoneCons6)
      const enonce4 = j3pAddElt(stor.zoneCons6, 'div', '', { paddingTop: '5px' })
      stor.divPalette = j3pAddElt(stor.zoneCons6, 'div', '', { style: { paddingTop: '10px' } })
      const elt = j3pAffiche(enonce4, '', ds.textes.phrase_coord, { inputmq1: {}, inputmq2: {}, inputmq3: {} })
      elt.inputmqList.forEach(elt => stor.zoneInput.push(elt))
      for (let i = 1; i <= 3; i++) mqRestriction(stor.zoneInput[i - 1], '\\d(+-)*,./', { commandes: ['fraction'] })
      j3pPaletteMathquill(stor.divPalette, stor.zoneInput[0], { liste: ['fraction'] })
      for (let i = 0; i < 3; i++) {
        $(stor.zoneInput[i]).focusin(ecoute.bind(null, i))
        stor.zoneInput[i].typeReponse = ['nombre', 'exact']
        if (stor.solution.position === 'sécants') stor.zoneInput[i].reponse = [stor.solution.E.coord[i]]
      }
      me.logIfDebug('stor.solution.E:', stor.solution.E)
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: stor.zoneInput.map(elt => elt.id)
      })
      j3pFocus(stor.zoneInput[0])
    }
  }
  function cacherPointInter () {
    j3pEmpty(stor.zoneCons6)
    stor.zoneInput = []
    stor.divPalette = null
  }

  function validation (reponseEleve, objSolution) {
    // reponseEleve est un objet qui a les pptés .QCM=réponse au QCM et .intersection=[coord du point d’inter] éventuel.
    // On y rajoute les erreurs de détection de parallelisme et de résolution de système et le nombre d’erreurs

    const analyseReponse = {}
    analyseReponse.juste = true
    analyseReponse.commentaire = ''
    analyseReponse.erreur_systeme = 0
    analyseReponse.erreur_paralleles = 0
    analyseReponse.erreur = 0

    if (reponseEleve.QCM !== objSolution.position) {
      // la réponse est donc fausse.
      analyseReponse.juste = false
      analyseReponse.erreur = 1
      // On analyse la capacité de l’élève à détecter le parallélisme ou non

      switch (objSolution.position) {
        case 'incluse':
          if (reponseEleve.QCM === 'sécants') {
            analyseReponse.commentaire = 'pb_detection_parallelisme'
            analyseReponse.erreur_paralleles = 1
          }
          break

        case 'strictement parallèles':
          if (reponseEleve.QCM === 'sécants') {
            analyseReponse.commentaire = 'pb_detection_parallelisme'
            analyseReponse.erreur_paralleles = 1
          }
          break

        case 'sécants':
          if (reponseEleve.QCM !== 'sécants') {
            analyseReponse.commentaire = 'pb_detection_parallelisme'
            analyseReponse.erreur_paralleles = 1
          }
          break
      }
      if ((objSolution.position !== 'sécants') && (reponseEleve.QCM === 'sécants')) {
        // Il a répondu 'sécant' et ce n’est asp le cas, on met en rouge (et éventuellement on barre les zones de saisie)
        for (let i = 0; i < 3; i++) {
          stor.fctsValid.zones.bonneReponse[i] = false
          stor.fctsValid.coloreUneZone(stor.zoneInput[i].id)
        }
      }
    } else {
      if (objSolution.position === 'sécants') {
        if (!compareTabNum(reponseEleve.intersection, objSolution.E.coord)) {
          analyseReponse.juste = false
          analyseReponse.commentaire = 'erreur_coord_inter'
          analyseReponse.erreur = 1
          analyseReponse.erreur_systeme = 1
        }
        stor.fctsValid.validationGlobale()
      }
    }
    return analyseReponse
  }

  function indications (laValidation) {
    if (laValidation.commentaire === 'erreur_coord_inter') return ds.textes.indications_erreur_pt_inter
    if (laValidation.commentaire === 'pb_detection_parallelisme') return ds.textes.indications_test_parallelisme
    return ''
  }

  function afficheCorrection (laValidation, objSolution) {
    if (laValidation.juste) stor.explications.style.color = me.styles.cbien
    if (j3pElement(stor.idsRadio[2]).checked) {
      j3pDetruit(stor.divPalette)
      stor.divPalette = null
    }
    const A = objSolution.A
    const u = objSolution.u
    const position = objSolution.position

    let i
    for (i = 1; i <= 8; i++) stor['zoneExplication' + i] = j3pAddElt(stor.explications, 'div')
    j3pAffiche(stor.zoneExplication1, '', ds.textes.corr1, {
      n: stor.solution.n.nom + '(' + stor.solution.n.coord.join(';') + ')'
    })
    j3pAffiche(stor.zoneExplication2, '', ds.textes.phrase_correction_dirige, {
      a: A.nom + '(' + objSolution.ptDroite.join(';') + ')',
      u: u.nom + '(' + u.coord.join(';') + ')'
    })
    // console.log('u:', u, '   n:', objSolution.n)
    j3pAffiche(stor.zoneExplication3, '', ds.textes.phrase_correction_regle, { u: u.nom, n: objSolution.n.nom })
    j3pAffiche(stor.zoneExplication4, '', ds.textes.phrase_correction_pdt_scal, { u: u.nom, n: objSolution.n.nom, p: stor.solution.uscaln })

    stor.zoneExplication6.style.paddingTop = '10px'

    const paralleles = ((position === 'incluse') || (position === 'strictement parallèles'))
    if (paralleles) {
      j3pAffiche(stor.zoneExplication5, '', ds.textes.phrase_correction_paralleles)
      j3pAffiche(stor.zoneExplication6, '', ds.textes.phrase_correction_trancher_inclus_strict, {
        A: A.nom + '(' + objSolution.ptDroite.join(';') + ')'
      })

      let calculPtA = objSolution.eqPlan.substring(0, objSolution.eqPlan.length - 2)
      const lesVar = ['x', 'y', 'z']
      for (i = 0; i <= 2; i++) {
        if (Math.abs(objSolution.n.coord[i]) === 1) {
          calculPtA = calculPtA.replace(lesVar[i], ecrireAvecPar(objSolution.ptDroite[i]))
        } else {
          calculPtA = calculPtA.replace(lesVar[i], '\\times ' + ecrireAvecPar(objSolution.ptDroite[i]))
        }
      }
      const resCalculPtA = objSolution.n.coord[0] * objSolution.ptDroite[0] + objSolution.n.coord[1] * objSolution.ptDroite[1] + objSolution.n.coord[2] * objSolution.ptDroite[2] + objSolution.d
      calculPtA += '=' + resCalculPtA
      j3pAffiche(stor.zoneExplication7, '', '$' + calculPtA + '$.')
      const dernierePhrase = (position === 'incluse') ? ds.textes.phrase_correction_constat_A_sur_d2 : ds.textes.phrase_correction_constat_A_pas_sur_d2
      j3pAffiche(stor.zoneExplication8, '', dernierePhrase, { A: A.nom })
    } else {
      j3pAffiche(stor.zoneExplication5, '', ds.textes.phrase_correction_non_paralleles)
      j3pAffiche(stor.zoneExplication6, '', ds.textes.corrInter1, { t: objSolution.parametre[0] })
      j3pAfficheSysteme(stor.zoneExplication7, '', '‡1‡', stor.solution.tabSystInter, {}, stor.explications.style.color)
      j3pAffiche(stor.zoneExplication8, '', ds.textes.corrInter2, {
        e1: objSolution.eqParam,
        e2: objSolution.eqParam2,
        e3: objSolution.eqParam3
      })
      stor.zoneExplication9 = j3pAddElt(stor.explications, 'div')
      j3pAffiche(stor.zoneExplication9, '', ds.textes.corrInter3, {
        t: objSolution.parametre[0],
        k: objSolution.valParam,
        c: '(' + A.coord.join(';') + ')'
      })
    }
  }

  function entreBornes (x) {
    // renvoie true si abs(x)<=bornes_coordonnees
    return (Math.abs(x) <= stor.borne_coordonnees)
  }

  function ecrireAvecPar (nb) {
    // nb est un nombre qui sera écrit dans un calcul
    // on lui ajoute des parenthèses s’il est négatif
    return (nb < 0) ? '(' + String(nb) + ')' : String(nb)
  }
  function ecrireAvecParExpress (expres) {
    // expres est une expression affine
    // on lui ajoute des parenthèses si ce n’est aps réduit à un nombre positif ou à quelques chose de la forme ax où a>0
    return (String(expres).replace(/[0-9]*[x-z]?/, '') === '') ? String(expres) : '(' + String(expres) + ')'
  }

  function getDonnees () {
    return {
      typesection: 'lycee', //

      nbrepetitions: 4,
      nbetapes: 1,
      indication: '',

      // Autant de chance de donner la droite par deux points que par sa représentation paramétrique
      probas_forme: [], // true donne dte donnée par 2 points,et false par une eq param
      probas_position: [],
      position: [0.25, 0.25, 0.5], // [incluse,stict parallèle,sécants]
      tab_paralleles: [], // Pour détecter le tableau des parallèles
      nb_paralleles: 0,
      permute_eq_param: [0.25, 0.75], // 25% de chances d’écrire b*t+a au lieu de a+b*t

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      textes: {
        titre_exo: 'Positions relative d’un plan et d’une droite',
        phrase_enonce: 'Dans un repère de l’espace, on considère le plan $P$ d’équation $£p$ et la droite $d$ d’équation paramétrique: ‡1‡, $£a \\in \\R$.',
        choix_radio1: 'La droite est incluse dans le plan.',
        choix_radio2: 'La droite et le plan sont strictement parallèles.',
        choix_radio3: 'La droite et le plan sont sécants.',
        phrase_coord: 'Dans ce cas, les coordonnées du point d’intersection sont (&1&;&2&;&3&)',
        phrase_faux: 'C’est faux!<br>',
        commentChoixRadio: 'Il faut choisir l’une des propositions&nbsp;!',
        indications_test_parallelisme: 'Il y a un problème dans la détermination du parallélisme d’une droite et d’un plan&nbsp;!',
        indications_erreur_pt_inter: 'Problème dans la résolution du système&nbsp;!',
        corr1: '$P$ admet le vecteur $£n$ pour vecteur normal.',
        phrase_correction_dirige: '$d$ passe par $£a$ et est dirigée par $£u$',
        phrase_correction_regle: '$P$ et $d$ sont parallèles si et seulement si $£u$ et $£n$ sont orthogonaux, c’est-à-dire $£u\\cdot £n=0$.',
        phrase_correction_pdt_scal: 'Or $£u\\cdot £n=£p$.',
        phrase_correction_paralleles: 'Donc le plan $P$ et la droite $d$ sont parallèles.',
        phrase_correction_non_paralleles: 'Donc le plan $P$ et la droite $d$ ne sont pas parallèles. Ils sont donc sécants en un point à déterminer.',
        phrase_correction_trancher_inclus_strict: 'Reste à déterminer si $d$ est incluse dans $P$ ou strictement parallèle à $P$, c’est-à-dire si le point $£A$ appartient au plan $P$&nbsp;:',
        phrase_correction_constat_A_pas_sur_d2: 'Le point $£A$ n’est pas dans le plan, donc la droite et le plan sont strictement parallèles.',
        phrase_correction_constat_A_sur_d2: 'Le point $£A$ est dans le plan, donc la droite est incluse dans le plan.',
        corrInter1: 'On cherche alors la valeur de $£t$ vérifiant le système&nbsp;:',
        corrInter2: 'Ceci nous donne $£{e1}$ qui équivaut à $£{e2}$ et ainsi $£{e3}$.',
        corrInter3: 'On remplace alors $£t$ par $£k$ dans la représentation paramétrique de la droite pour trouver que le point d’intersection a pour coordonnées $£c$'
      },
      pe_1: 'Bien',
      pe_2: 'problème dans la détection du parallélisme',
      pe_3: 'problème dans la résolution du système',
      pe_4: 'Insuffisant'
    }
  }
  function enonceMain () {
    stor.borne_coordonnees = 6
    const facteurColinearite = 0

    const position = stor.probasPosition[me.questionCourante - 1]

    // On va commencer par générer 3 points A,B,C non alignés
    let xA, yA, zA, xB, yB, zB, xC, yC, zC
    let coordA, coordB, coordC, coordU, coordV
    let a, b, c, nbNuls, nbGrandcoef
    do {
      do {
        xA = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
        yA = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
        zA = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
        coordA = [xA, yA, zA]

        xB = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
        yB = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
        zB = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
        coordB = [xB, yB, zB]

        xC = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
        yC = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
        zC = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
        coordC = [xC, yC, zC]
        const xU = xB - xA
        const yU = yB - yA
        const zU = zB - zA
        coordU = [xU, yU, zU]

        const xV = xC - xA
        const yV = yC - yA
        const zV = zC - zA
        coordV = [xV, yV, zV]
      } while (compareTabNum(coordA, coordB) || compareTabNum(coordA, coordC) || compareTabNum(coordB, coordC) || UcolV(coordU, coordV))
      // On récupère les coeffs a,b,c du plan via u vectoriel v
      const normal = produitVectoriel(coordU, coordV)
      // Puis on détermine d:
      a = normal[0]
      b = normal[1]
      c = normal[2]
      nbNuls = 0
      nbGrandcoef = 0
      for (let i = 0; i < 3; i++) {
        if (Math.abs(normal[i]) < Math.pow(10, -12)) nbNuls += 1
        if (Math.abs(normal[i]) > 15) nbGrandcoef += 1
      }
    } while (nbNuls > 1 || nbGrandcoef > 0)
    let d = -a * xA - b * yA - c * zA
    // On simplifie l’équation du plan
    const lePgcd = pgcdZarb([a, b, c, d])
    a = a / lePgcd
    b = b / lePgcd
    c = c / lePgcd
    d = d / lePgcd
    stor.solution = {}
    stor.solution.eqPlan = (Math.abs(a) < Math.pow(10, -12))
      ? j3pGetLatexMonome(1, 1, b, 'y') + j3pGetLatexMonome(2, 1, c, 'z') + j3pGetLatexMonome(3, 0, d) + '=0'
      : j3pGetLatexMonome(1, 1, a, 'x') + j3pGetLatexMonome(2, 1, b, 'y') + j3pGetLatexMonome(3, 1, c, 'z') + j3pGetLatexMonome(4, 0, d) + '=0'
    // On génère maintenant la droite,selon les cas, par deux points E et F

    // Si incluse, on choisit par défaut E=A et F=B
    let coordE = [...coordA]
    // var coordF = coordB

    let xF, yF, zF
    let xt, yt, zt, coordEntreBornes
    if (position === 'strictement parallèles') {
      // On translate A et B d’un vecteur t quelconque
      do {
        do {
          xt = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
          yt = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
          zt = j3pGetRandomInt(-stor.borne_coordonnees, stor.borne_coordonnees)
        } while (Math.abs(a * xt + b * yt + c * zt) < Math.pow(10, -12))
        // var coord_t = [xt, yt, zt]

        const xE = xA + xt
        const yE = yA + yt
        const zE = zA + zt
        coordE = [xE, yE, zE]

        xF = xB + xt
        yF = yB + yt
        zF = zB + zt

        const toutesLesCoord = [xE, yE, zE, xF, yF, zF]
        coordEntreBornes = tableauVrai(fonctionTab(entreBornes, toutesLesCoord))
      } while (!coordEntreBornes)
    }
    if (position === 'sécants') {
      // On prend E=A et F hors plan (ABC)

      do {
        coordU = [j3pGetRandomInt(-3, 4), j3pGetRandomInt(-3, 4), j3pGetRandomInt(-3, 4)]
        const coefMult = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 3)
        xF = xA + coefMult * coordU[0]
        yF = yA + coefMult * coordU[1]
        zF = zA + coefMult * coordU[2]
      } while (a * xF + b * yF + c * zF + d === 0)
    }
    const tabDesChoix = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'L', 'M', 'N', 'R']
    // On choisit plusieurs points pour la représentation 3D
    const mesPoints = j3pGetRandomElts(tabDesChoix, 5)

    const tabParametresPossibles = ['a', 'b', 'c', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't']
    const parametre = j3pGetRandomElts(tabParametresPossibles, 1)
    const param1 = parametre[0]

    // le plan est (ABC) et (EF) est la droite d
    const A = {}
    A.nom = mesPoints[0]
    A.coord = [xA, yA, zA]
    // A.coord=[1,-2,4]

    const B = {}
    B.nom = mesPoints[1]
    B.coord = [xB, yB, zB]
    // B.coord=[-8,-2,-1]

    const C = {}
    C.nom = mesPoints[2]
    C.coord = [xC, yC, zC]
    // C.coord=[-8,-7,0]

    const E = {}
    E.nom = mesPoints[0]
    E.coord = coordE

    const u = {}
    u.nom = '\\vec{u}'
    u.coord = coordU
    // u.coord=[3,2,2]

    const n = {}
    n.nom = '\\vec{n}'
    n.coord = [a, b, c]

    let choix = Math.random()
    const permut = 1 - ds.permute_eq_param
    const tabPermut1 = []
    const tabPermut2 = []
    let x1, y1, z1
    const ptDroite = (position === 'incluse') ? A.coord : [xF, yF, zF]
    if (choix < permut) {
      // J’aurais du créer un objet x1... tant pis, ce sera pour la prochaine section :(
      x1 = sommeMonomes([ptDroite[0], u.coord[0]], ['', param1])
      tabPermut1.push(0)
    } else {
      x1 = sommeMonomes([u.coord[0], ptDroite[0]], [param1, ''])
      tabPermut1.push(1)
    }

    choix = Math.random()
    if (choix < permut) {
      y1 = sommeMonomes([ptDroite[1], u.coord[1]], ['', param1])
      tabPermut1.push(0)
    } else {
      y1 = sommeMonomes([u.coord[1], ptDroite[1]], [param1, ''])
      tabPermut1.push(1)
    }

    choix = Math.random()
    if (choix < permut) {
      z1 = sommeMonomes([ptDroite[2], u.coord[2]], ['', param1])
      tabPermut1.push(0)
    } else {
      z1 = sommeMonomes([u.coord[2], ptDroite[2]], [param1, ''])
      tabPermut1.push(1)
    }
    // calcul du produit scalaire u.n
    stor.solution.uscaln = u.coord[0] + '\\times' + ecrireAvecPar(n.coord[0]) + ' + ' + ecrireAvecPar(u.coord[1]) + '\\times' + ecrireAvecPar(n.coord[1]) + ' + ' + ecrireAvecPar(u.coord[2]) + '\\times' + ecrireAvecPar(n.coord[2])
    stor.solution.val_prodscal = u.coord[0] * n.coord[0] + u.coord[1] * n.coord[1] + u.coord[2] * n.coord[2]
    stor.solution.uscaln += '=' + stor.solution.val_prodscal
    if (Math.abs(stor.solution.val_prodscal) > Math.pow(10, -12)) stor.solution.uscaln += '\\neq 0'

    stor.solution.A = A
    stor.solution.B = B
    stor.solution.C = C
    // stor.solution.D = D
    stor.solution.E = E

    // stor.solution.AC = AC

    stor.solution.u = u
    stor.solution.n = n
    stor.solution.d = d
    stor.solution.lambda = facteurColinearite
    stor.solution.position = position
    stor.solution.parametre = parametre
    stor.solution.expression_x1 = x1
    stor.solution.expression_y1 = y1
    stor.solution.expression_z1 = z1
    stor.solution.tabPermut1 = tabPermut1
    stor.solution.tabPermut2 = tabPermut2
    stor.solution.ptDroite = ptDroite
    const LDroite = [x1, y1, z1]
    if (position === 'sécants') {
      stor.solution.tabSystInter = [['$' + stor.solution.eqPlan + '$', '$x=' + x1 + '$', '$y=' + y1 + '$', '$z=' + z1 + '$']]
      // Je crée ici la ligne de la forme a(xA+xUt)+b(yA+yUt),c(zA+zUt)+d=0
      stor.solution.eqParam = stor.solution.eqPlan.substring(0, stor.solution.eqPlan.length - 2)
      const lesVar = ['x', 'y', 'z']
      for (let i = 0; i <= 2; i++) {
        if (Math.abs(stor.solution.n.coord[i]) === 1) {
          stor.solution.eqParam = stor.solution.eqParam.replace(lesVar[i], ecrireAvecParExpress(LDroite[i]))
        } else {
          stor.solution.eqParam = stor.solution.eqParam.replace(lesVar[i], '\\times ' + ecrireAvecParExpress(LDroite[i]))
        }
      }
      stor.solution.eqParam += '=0'
      const coef0 = a * ptDroite[0] + b * ptDroite[1] + c * ptDroite[2] + d
      const coef1 = a * u.coord[0] + b * u.coord[1] + c * u.coord[2]
      stor.solution.eqParam2 = j3pGetLatexMonome(1, 1, coef1, parametre[0]) + j3pGetLatexMonome(2, 0, coef0) + '=0'
      stor.solution.valParam = j3pGetLatexQuotient(j3pGetLatexOppose(coef0), coef1)
      stor.solution.eqParam3 = parametre[0] + '=' + stor.solution.valParam
    }
    me.logIfDebug('solution:', stor.solution, 'position:', position)
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('toutpetit.enonce', { padding: '15px' }) }
    )
    j3pAddElt(stor.conteneur, 'div', '', { id: 'div_systeme', style: { paddingTop: '20px' } })
    for (let i = 2; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    j3pAfficheSysteme('div_systeme', 'mon_systeme', ds.textes.phrase_enonce, [['$x=' + x1 + '$', '$y=' + y1 + '$', '$z=' + z1 + '$']], { a: param1, p: stor.solution.eqPlan }, me.styles.toutpetit.enonce.color)
    stor.idsRadio = []
    stor.nameRadio = j3pGetNewId('choix')
    ;[1, 2, 3].forEach(i => {
      stor.idsRadio.push(j3pGetNewId('radio' + i))
      j3pBoutonRadio(stor['zoneCons' + (i + 2)], stor.idsRadio[i - 1], stor.nameRadio, i - 1, ds.textes['choix_radio' + i])
      if (i === 3) j3pElement(stor.idsRadio[i - 1]).onclick = afficherPointInter
      else j3pElement(stor.idsRadio[i - 1]).onclick = cacherPointInter
    })
    stor.zoneInput = []
    stor.verifParallelisme = false

    stor.explications = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('toutpetit.explications', { paddingTop: '15px' }) })
    stor.correction = j3pAddElt(me.zones.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':

      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        if (ds.nbrepetitions > 1) {
          stor.probasPosition = creeTableauAleatoire(['incluse', 'strictement parallèles', 'sécants'], ds.position, ds.nbrepetitions)
        }

        // On détecte les cas de parallélisme
        for (let k = 0; k < ds.nbrepetitions; k++) {
          if (stor.probasPosition[k] === 'incluse' || stor.probasPosition[k] === 'strictement parallèles') {
            ds.tab_paralleles.push(true)
          } else {
            ds.tab_paralleles.push(false)
          }
        }

        // Construction de la page
        me.construitStructurePage(ds.structure)

        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication !== '') me.indication(me.zones.IG, ds.indication)

        ds.nb_erreur = 0
        ds.nb_erreur_detection_parallelisme = 0
        ds.nb_erreur_resol_systeme = 0
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'correction':
      {
        const objSol = stor.solution
        // On teste si une réponse a été saisie
        let aRepondu = true
        let numrepQCM, repEleveX, repEleveY, repEleveZ, reponseEleve, laValidation
        const aReponduQcm = (j3pElement(stor.idsRadio[0]).checked || j3pElement(stor.idsRadio[1]).checked || j3pElement(stor.idsRadio[2]).checked)
        if (j3pElement(stor.idsRadio[2]).checked) {
          for (let i = 1; i <= 3; i++) {
            aRepondu = ((j3pValeurde(stor.zoneInput[0]) !== '') && (j3pValeurde(stor.zoneInput[1]) !== '') && (j3pValeurde(stor.zoneInput[2]) !== ''))
          }
        }
        if (aReponduQcm && aRepondu) {
          let repQCM = ''
          if (j3pElement(stor.idsRadio[0]).checked) {
            repQCM = 'incluse'
            numrepQCM = 1
          }
          if (j3pElement(stor.idsRadio[1]).checked) {
            repQCM = 'strictement parallèles'
            numrepQCM = 2
          }
          if (j3pElement(stor.idsRadio[2]).checked) {
            repQCM = 'sécants'
            numrepQCM = 3
          }

          // En cas de sécantes :
          if (repQCM === 'sécants') {
            repEleveX = j3pCalculValeur(j3pValeurde(stor.zoneInput[0]))
            repEleveY = j3pCalculValeur(j3pValeurde(stor.zoneInput[1]))
            repEleveZ = j3pCalculValeur(j3pValeurde(stor.zoneInput[2]))
          }
          reponseEleve = {}

          reponseEleve.QCM = repQCM
          reponseEleve.intersection = [repEleveX, repEleveY, repEleveZ]
          laValidation = validation(reponseEleve, objSol)
          me.logIfDebug('laValidation:', laValidation)
        }
        if (!aReponduQcm || !aRepondu) {
          let msgReponseManquante
          if (!aReponduQcm) {
            msgReponseManquante = ds.textes.commentChoixRadio
          } else {
            if (j3pElement(stor.idsRadio[2]).checked) {
              for (let i = 3; i >= 1; i--) {
                if ((j3pValeurde(stor.zoneInput[i]) === '')) j3pFocus(stor.zoneInput[i])
              }
            }
          }
          this.reponseManquante(stor.correction, msgReponseManquante)
        } else {
          ds.numero_essai = ds.numero_essai + 1
          // Bonne réponse
          const posBonneCase = ['incluse', 'strictement parallèles', 'sécants'].indexOf(stor.solution.position)

          if (laValidation.juste) {
            for (let i = 1; i <= 3; i++) j3pDesactive(stor.idsRadio[i - 1])
            me._stopTimer()
            me.score++
            stor.correction.style.color = me.styles.cbien
            stor.correction.innerHTML = cBien
            j3pElement('label' + stor.idsRadio[posBonneCase]).style.color = me.styles.cbien
            afficheCorrection(laValidation, objSol)

            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.correction.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.correction.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()

              afficheCorrection({ juste: false }, stor.solution)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.correction.innerHTML = cFaux
              if (!stor.verifParallelisme) {
                ds.nb_erreur_detection_parallelisme += laValidation.erreur_paralleles
                if (laValidation.erreur_paralleles) stor.verifParallelisme = true
              }

              if (me.essaiCourant < ds.nbchances) {
              // On regarde si on peut donner une indication

                stor.correction.innerHTML += '<br>' + indications(laValidation)
                stor.correction.innerHTML += '<br>' + essaieEncore
              } else {
              // Erreur au nème essai
                ds.nb_erreur += laValidation.erreur
                ds.nb_erreur_resol_systeme += laValidation.erreur_systeme

                stor.correction.innerHTML = cFaux
                for (let i = 1; i <= 3; i++) j3pDesactive(stor.idsRadio[i - 1])
                if (reponseEleve.QCM !== stor.solution.position) {
                  j3pBarre('label' + stor.idsRadio[numrepQCM - 1])
                  j3pElement('label' + stor.idsRadio[numrepQCM - 1]).style.color = me.styles.cfaux
                  j3pElement('label' + stor.idsRadio[posBonneCase]).style.color = me.styles.petit.explications.color
                  if (numrepQCM === 3) {
                    j3pBarre(stor.zoneCons6)
                    j3pDetruit(stor.divPalette)
                    stor.divPalette = null
                  }
                } else {
                  j3pElement('label' + stor.idsRadio[numrepQCM - 1]).style.color = me.styles.cbien
                }
                me._stopTimer()
                me.cacheBoutonValider()
                stor.correction.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++

                afficheCorrection(laValidation, objSol)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      me.logIfDebug('ds.nb_erreur_detection_parallelisme:', ds.nb_erreur_detection_parallelisme, 'erreur_syst:', ds.nb_erreur_resol_systeme)
      if (me.sectionTerminee()) {
        // me.parcours.pe = me.score / ds.nbitems
        const freqErreurDetectionParallelisme = ds.nb_erreur_detection_parallelisme / ds.nbitems
        if (freqErreurDetectionParallelisme >= 0.5) {
          me.parcours.pe = ds.pe_2
        } else {
          // On regarde à présent le pb de résolution de système:
          const freqErreurResolSysteme = ds.nb_erreur_resol_systeme / ds.nbitems
          if (freqErreurResolSysteme >= 0.5) {
            me.parcours.pe = ds.pe_3
          } else {
            // On regarde alors si le seuil de réussite est supérieur ou égal à 75%
            if (ds.nb_erreur / ds.nbitems > 0.25) {
              me.parcours.pe = ds.pe_4
            } else {
              me.parcours.pe = ds.pe_1
            }
          }
        }

        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
