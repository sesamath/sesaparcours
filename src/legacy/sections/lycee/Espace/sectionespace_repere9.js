import { j3pAddElt, j3pGetRandomElts, j3pDetruit, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pMathquillXcas, j3pValeurde } from 'src/legacy/core/functions'
import _3D from 'src/legacy/outils/3D/3D'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pCreeBoutonFenetre, j3pMasqueFenetre } from 'src/legacy/core/functionsJqDialog'
import { j3pAfficheSysteme } from 'src/legacy/core/functionsSysteme'
import { fraction, sommeMonomes, vLatex, compareTabNum } from 'src/legacy/outils/espace/fctsEspace'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
        Laurent Fréguin
        oct 2015
        Remarques : merci à Alexis pour son aide lors de cette première section!
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['type_plan', ['point_vecteurs', 'trois_points'], 'array', 'Tableau de même longueur que le nombre de répétitions, permettant de déterminer si le plan est défini par un point et deux vecteurs directeurs (point_vecteurs) ou par trois points non alignés (trois_points)']

  ]
}

const textes = {
  titre_exo: 'Equation paramétrique d’un plan',
  titre3D: 'Illustration dans l’espace',
  affichage_espace: 'Voir la figure',
  phrase1: 'Dans un repère de l’espace, voici 3 points non alignés :<br>$£A(£a;£b;£c)$, $£B(£d;£e;£f)$ et $£C(£g;£h;£i)$',
  phrase2: 'On considère le plan (P) passant par $£A(£a;£b;£c)$ et ayant pour vecteurs directeurs $\\vec{u}(£d;£e;£f)$ et $\\vec{v}(£g;£h;£i)$ ',
  phrase3: 'Donne une représentation paramétrique du plan $(£A£B£C)$&nbsp;:',
  phrase4: 'Donne une représentation paramétrique du plan (P).',
  indication_2var: 'Il faut 2 paramètres pour donner la représentation paramétrique d’un plan',
  indication_non_interpretable: 'Chaque expression doit être de la forme',
  indication_point_ref_dedans: 'Le point de référence choisi est bien dans le plan.',
  indication_point_ref_dehors: 'Mais le point de référence n’est pas dans le plan.',
  indication_syst_direct_oui: 'Les vecteurs choisis forment bien un système de vecteurs directeurs.',
  indication_syst_direct_non: 'Mais les vecteurs ne forment pas un système de vecteurs directeurs.',
  correction_gene: 'Une représentation paramétrique d’un plan passant par le point de référence $A(x_A;y_A;z_A)$ et de vecteurs directeurs $\\vec{u}$ et $\\vec{v}$ est&nbsp;:',
  correction_gene1: '‡1‡ avec $£a\\in \\R$, $£b\\in \\R$ .',
  // correction_gene:"Une représentation paramétrique d’un plan passant par le point de référence $A(x_A;y_A;z_A)$ et de vecteurs directeurs $\\vec{u}$ et $\\vec{v}$ est :",
  correction_point_un_des_trois: 'Tu as choisi le point de référence $£A$',
  correction_point_autre: 'Tu as choisi le point de référence $£A(£a)$ qui est en effet dans le plan',
  correction_vect_direct: 'et comme vecteurs directeurs $\\vec{u}=£A$ et $\\vec{v}= £B$.',
  correction_mauvaisPoint: 'Tu as choisi le point de référence $£A(£a;£b;£c)$ qui n’est pas un point du plan. Tu pouvais par exemple choisir $£B$.',
  correction_choix_point2: 'En choisissant par exemple le point de référence $£A$ et les vecteurs directeurs $\\vec{u}=£B(£b)$ et $\\vec{v}= £C(£c)$, on obtient&nbsp;:',
  correction_vecteurs: ' Tu as pris $\\vec{u}£A$ et $\\vec{v}£B$.',
  correction_un_des_deux: ' $\\vec{£A}$ est directeur, $\\vec{£B}$ ne l’est pas.',
  correction_aucun_des_deux: ' Ni $\\vec{u}$, ni $\\vec{v}$ ne sont directeurs.',
  correction_choix_vecteurs: 'Avec $\\vec{u}=£A$ et $\\vec{v}=£B$, on obtient&nbsp;:',
  correction_2_vecteurs_dirigent_mais: '$\\vec{u}$ est directeur et $\\vec{v}$ l’est aussi,',
  correction_mais_colineaires: ' mais ils ne forment pas un système de vecteurs directeurs car ils sont colinéaires&nbsp;! On a en effet : $\\vec{v}=£A\\vec{u}$',
  correction_conclusion: 'Finalement, avec le point de référence $£A$, $\\vec{u}=£B(£b)$ et $\\vec{v}=£C(£c)$, on obtient&nbsp;:',
  correction_point_vecteurs: 'En reportant les coordonnées du point de l’énoncé $£A$ et des vecteurs directeurs $\\vec{u}$ et $\\vec{v}$, on obtient :'

}

/**
 * section espace_repere9
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function arrondi (x, n) {
    return Math.round(x * Math.pow(10, n)) / Math.pow(10, n)
  }

  function variables (chaine, tabPossible) {
    // Cette fonction renvoie un objet :
    // le nombre de variables détectées dans le tablau tabPossible:  .nombre
    // leur nom sous forme de tableau : .nom
    let nbVar = 0
    const tabNomDesVar = []
    for (let i = 0; i < tabPossible.length; i++) {
      if (chaine.indexOf(tabPossible[i]) !== -1) {
        nbVar = nbVar + 1
        tabNomDesVar.push(tabPossible[i])
      }
    }

    return { nombre: nbVar, nom: tabNomDesVar }
  }

  function estAffine (strExpression, tabMesVariables) {
    // le tableau des variables est de taille 1 ou 2.
    // Cette fonction renvoie un objet dont les pptés sont les suivantes :
    // .affine=true si l’expression est affine par rapport aux variables du tableau tabMesVariables'
    // false sinon
    // .coeff = un tableau de coefficient devant chaque variable
    // En pratique, on part du principe que f(x,y)=a+b*x+c*y puis on retrouve a,b,et c.
    // Puis on compare strExpression à f(x,y)
    // On va commencer par détecter s’il existe une chaine du genre s12,t2 etc, car la fonction evelue evalue mal ces chaines'

    const stock = {}
    const tabChiffres = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    let sousChaine1 = ''
    let sousChaine2 = ''
    let chaineBizarre = 0
    for (let k = 0; k < 10; k++) {
      // sousChaine1="";
      // sousChaine2=""
      sousChaine1 = tabMesVariables[0] + tabChiffres[k]
      sousChaine2 = tabMesVariables[1] + tabChiffres[k]
      if (strExpression.indexOf(sousChaine1) !== -1 || strExpression.indexOf(sousChaine2) !== -1) {
        chaineBizarre = chaineBizarre + 1
        k = 10
      }
    }

    if (chaineBizarre === 0) {
      // Exctraction des a,b et c potentiels
      const arbre1 = new Tarbre(strExpression, tabMesVariables)

      let a = arbre1.evalue([0, 0])
      let b = arbre1.evalue([1, 0]) - a

      let c = arbre1.evalue([0, 1]) - a
      c = arrondi(c, 12)
      a = arrondi(a, 12)
      b = arrondi(b, 12)
      let difference = 0
      let affine = true
      // Comparaison à Str_expression sur un double tableau
      for (let i = 2; i < 10; i++) {
        for (let j = 2; j < 10; j++) {
          difference = Math.abs(arbre1.evalue([i, j]) - (a + b * i + c * j))
          if (difference > Math.pow(10, -9)) {
            affine = false
            i = 10
            j = 10
          }
        }
      }
      if (affine) {
        stock.affine = true
        stock.coeff = [a, b, c]
        stock.coeff_frac = [fraction(a).ch, fraction(b).ch, fraction(c).ch]
      } else {
        stock.affine = false
      }
    } else {
      stock.affine = false
    }

    return stock
  }

  function resoudreSysteme (a, b, c, d, e, f) {
    // Cette fonction résout le système ax+by=c dx+ey=f
    // .elle renvoie l’objet stock :
    // stock.resol=1 si solution unique, 0 sinon
    // stock.valeur donne le tableau de sol sous forme décimale
    // stock.ch renvoie le tableau de solutions sous forme de fraction. Ex :[3/4,2]
    // stock.mathq donne ce même tableau  sous forme de fraction version mathq
    const stock = {}
    stock.resol = true
    const determinant = a * e - b * d
    if (egal(determinant, 0)) {
      stock.resol = false
    } else {
      const x = (c * e - f * b) / determinant
      const y = (a * f - c * d) / determinant
      stock.valeur = [x, y]
      stock.ch = [fraction(x).ch, fraction(y).ch]
      stock.mathq = [fraction(x).mathq, fraction(y).mathq]
    }

    return stock
  }

  function egal (a, b) {
    // Renvoie true si la différence entre a et b est inférieure ou égale à 10^-9
    return Math.abs(a - b) < Math.pow(10, -9)
  }

  function compareTableauAuxAutres (monTableau, tableauDesAutresTableaux) {
    // cette fonction regarde si le tableau monTableau est égal à l’un des tableaux contenus dans tableauDesAutresTableaux
    // retourne l’index du tableau trouvé ou -1
    for (let k = 0; k < tableauDesAutresTableaux.length; k++) {
      if (compareTabNum(monTableau, tableauDesAutresTableaux[k])) return k
    }
    return -1
  }

  function tableauFrac (tabNumerique) {
    // cette fonction met chaque élément numérique du tableau sous forme fractionnaire et renvoie le tableau correspondant
    const stock = []
    for (let k = 0; k < tabNumerique.length; k++) {
      stock.push(fraction(tabNumerique[k]).mathq)
    }
    // return tabNumerique.map((nb) => { fraction(nb).mathq })
    return stock
  }
  function validation (rep1, rep2, rep3, objSolution) {
    // rep1,rep2,rep3 sont des chaines et objSolution un objet contenant les infos relatives à la solution
    const objetReponse = {}
    objetReponse.juste = true
    // console.log(rep1)
    rep1 = j3pMathquillXcas(rep1)
    rep2 = j3pMathquillXcas(rep2)
    rep3 = j3pMathquillXcas(rep3)

    const chaineGlobale = rep1 + rep2 + rep3
    const lettresPossibles = ['a', 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w']

    const lesVariables = variables(chaineGlobale, lettresPossibles)
    const nbDeVar = lesVariables.nombre
    const tabDesVariables = lesVariables.nom

    // On commence par regarder le nombre de paramètres. On en attend 2 pour la représentation paramétrique d’un plan.
    objetReponse.deux_variables = lesVariables.nombre
    objetReponse.interpretable = true

    if (nbDeVar !== 2) {
      objetReponse.juste = false
    } else {
      // Puis on regarde le caractère affine de chaque expression
      const paramAffineRep1 = estAffine(rep1, tabDesVariables)
      const paramAffineRep2 = estAffine(rep2, tabDesVariables)
      const paramAffineRep3 = estAffine(rep3, tabDesVariables)
      const xEstAffine = paramAffineRep1.affine
      const yEstAffine = paramAffineRep2.affine
      const zEstAffine = paramAffineRep3.affine
      objetReponse.xEstAffine = xEstAffine
      objetReponse.yEstAffine = yEstAffine
      objetReponse.zEstAffine = zEstAffine
      objetReponse.nom_des_variables = tabDesVariables
      let numero, a, b, c, d
      if (!xEstAffine || !yEstAffine || !zEstAffine) {
        objetReponse.interpretable = false
        objetReponse.juste = false
      } else {
        // Les expressions de x,y et z ont deux variables et sont affines.
        objetReponse.interpretable = true
        // Reste à déterminer on obtient effectivement une représentation paramétrique correcte.
        // On récupère le point de référence P choisi et les deux vecteurs directeurs U1 et U2  pout commencer

        const xP = paramAffineRep1.coeff[0]
        const yP = paramAffineRep2.coeff[0]
        const zP = paramAffineRep3.coeff[0]
        // Récupération des 2vecteurs directeurs choisis :
        const xU1 = paramAffineRep1.coeff[1]
        const yU1 = paramAffineRep2.coeff[1]
        const zU1 = paramAffineRep3.coeff[1]

        const xU2 = paramAffineRep1.coeff[2]
        const yU2 = paramAffineRep2.coeff[2]
        const zU2 = paramAffineRep3.coeff[2]
        // Cas particulier où les coeffs de U1 ou U2 valent 0. On a alors 0 ou 1 variable
        if ((xU1 === 0 && yU1 === 0 && zU1 === 0) || (xU2 === 0 && yU2 === 0 && zU2 === 0)) {
          objetReponse.juste = false
          objetReponse.deux_variables = 1
        }
        // Puis on regarde si le point P vaut A,B ou C ou si c’est un autre point du plan
        const tabP = [xP, yP, zP]
        numero = compareTableauAuxAutres(tabP, objSolution.mesPoints.coord)

        if (numero !== -1) {
          // objetReponse.point_reference=objSolution.mesPoints.nom[numero]
          objetReponse.point_reference = { nom: objSolution.mesPoints.nom[numero], autre: false, dans_le_plan: true, coord: objSolution.mesPoints.coord[numero], index: numero }
        } else {
          a = objSolution.coeff_a
          b = objSolution.coeff_b
          c = objSolution.coeff_c
          d = objSolution.coeff_d
          // le point référence n’est ni A,ni B,ni C.
          // On lui donne un nom
          objetReponse.point_reference = { nom: j3pGetRandomElts(objSolution.lettres_restantes, 1)[0], autre: true, coord: tabP, dans_le_plan: true }
          objetReponse.coord_point_reference = tabP

          // Et on regarde s’il est ou non dans le plan

          if (!egal(a * xP + b * yP + c * zP + d, 0)) {
            objetReponse.point_reference.dans_le_plan = false
            // objetReponse.point_reference.coord=tabP;

            objetReponse.juste = false
          }
        }
        // On regarde à présent si les vecteurs U1 et U2 sont effectivement deux vecteurs directeurs :
        // qu’on cherche à identifier d’abord parmi AB,BA,AC,CA,BC,CB
        // ou dont on cherche l’expression comme combinaison linéaire de AB et AC
        const tabU1 = [xU1, yU1, zU1]
        const tabU2 = [xU2, yU2, zU2]
        const tabU = [tabU1, tabU2]
        const ligneX = [xU1, xU2]
        const ligneY = [yU1, yU2]
        const ligneZ = [zU1, zU2]
        const lequel = ['U1', 'U2']
        for (let k = 0; k < 2; k++) {
          const ppte = lequel[k]
          objetReponse[ppte] = { coord: tabU[k] }
          numero = compareTableauAuxAutres(tabU[k], objSolution.mes_vecteurs.coord)
          if (numero !== -1) {
            // U1 ou U2 est l’un des 6 vecteurs
            objetReponse[ppte].nom = objSolution.mes_vecteurs.nom[numero]
            objetReponse[ppte].est_directeur = true
            objetReponse[ppte].autre = false
            // objetReponse.vecteur_U1=objSolution.mes_vecteurs.nom[numero]
          } else {
            // On regarde s’il s’agit d’une combi linéaire de AB et AC, ie resolution de x*AB+y*AC=U1

            a = objSolution.AB.x
            b = objSolution.AC.x
            d = objSolution.AB.y
            let e = objSolution.AC.y
            c = ligneX[k]
            let f = ligneY[k]
            let resultat = ligneZ

            let aTester = 'z'
            // On choisit le système pour réupérer la combinaison linéaire potentielle avec deux coordonnées
            if (egal(a * e - b * d, 0)) {
              d = objSolution.AB.z
              e = objSolution.AC.z
              f = ligneZ[k]
              resultat = ligneY
              aTester = 'y'
            }
            const combi = resoudreSysteme(a, b, c, d, e, f)
            if (combi.resol) {
              const x1 = combi.valeur[0]
              const y1 = combi.valeur[1]
              // On teste la ligne restante
              if (!egal(x1 * objSolution.AB[aTester] + y1 * objSolution.AC[aTester], resultat[k])) {
                objetReponse[ppte].est_directeur = false
                objetReponse.juste = false
              } else {
                objetReponse[ppte].autre = true
                objetReponse[ppte].est_directeur = true
                objetReponse[ppte].nom = sommeMonomes([x1, y1], [objSolution.AB.nom, objSolution.AC.nom])
              }
            } else {
              objetReponse[ppte].est_directeur = false
              objetReponse.juste = false
            }
          }
        }
        // Reste à tester la colinéarité eventuelle de U1 et U2
        if (egal(xU1 * yU2 - xU2 * yU1, 0) && egal(yU1 * zU2 - zU1 * yU2, 0) && egal(xU1 * zU2 - zU1 * xU2, 0)) {
          objetReponse.juste = false
          objetReponse.colineaires = true
          let rapport
          if (!egal(xU1, 0)) {
            rapport = fraction(xU2 / xU1).mathq
          } else {
            if (!egal(yU1, 0)) {
              rapport = fraction(yU2 / yU1).mathq
            } else {
              rapport = fraction(zU2 / zU1).mathq
            }
          }
          objetReponse.rapport_colinearite = rapport
        } else {
          objetReponse.colineaires = false
        }
        // On teste s’il s’agit finalement d’un système de vecteurs directeurs pour l’indication
        objetReponse.systeme_vect_direct = (objetReponse.U1.est_directeur && objetReponse.U2.est_directeur && !objetReponse.colineaires)
      }
    }

    return objetReponse
  }

  function indications (objetReponse, objetTextesIndications, tabPhrases) {
    // cette fonction sert à guider l’élève après la première erreur
    // On fait donc appel à cette fonction nécessairement dans le cas où objetReponse.juste=false
    let stock = ''

    if (objetReponse.deux_variables !== 2) {
      //
      stock = '<br>' + objetTextesIndications[tabPhrases[0]]
    } else {
      if (!objetReponse.interpretable) {
        stock = '<br>' + objetTextesIndications[tabPhrases[1]] + '$\\alpha+\\beta \\cdot ' + objetReponse.nom_des_variables[0] + '+\\gamma \\cdot ' + objetReponse.nom_des_variables[1] + '$'
      } else {
        if (objetReponse.point_reference.dans_le_plan) {
          stock = '<br>' + objetTextesIndications[tabPhrases[2]] + '<br>' + objetTextesIndications[tabPhrases[5]]
        } else {
          if (objetReponse.systeme_vect_direct) {
            stock = '<br>' + objetTextesIndications[tabPhrases[4]] + '<br>' + objetTextesIndications[tabPhrases[3]]
          }
        }
      }
    }
    return stock
  }

  function afficheCorrection (objetReponse, objetSolution, objetTextesCorrection, tabPhrases) {
    // objetReponse,objetSolution,objet_phrases,tabPhrases
    if (objetReponse.juste) stor.explications.style.color = me.styles.cbien
    for (let i = 1; i <= 12; i++) stor['zoneExpli' + i] = j3pAddElt(stor.explications, 'div')
    let tabVariables, paramX, paramY, paramZ, repParamType, nomPointRefChoisi, coordPointRefChoisi, lesVecteurs
    let nomU1, coordU1, nomU2, coordU2, tabCoeffX, tabCoeffY, tabCoeffZ
    let xSimplifie, ySimplifie, zSimplifie, repSyst, indexPointRef
    // Ajout dans le cas où un point et deux vecteurs directeurs, ben on prend ceux là ! (que ce soit juste ou pas, pas la peine de se faire de noeud au cerveau...)
    if (ds.type_plan[me.questionCourante - 1] === 'point_vecteurs') {
      tabVariables = ['s', 't']
      if (objetReponse.deux_variables === 2) {
        tabVariables = objetReponse.nom_des_variables
      }
      paramX = '$x=x_A+' + tabVariables[0] + ' \\times x_\\vec{u}+' + tabVariables[1] + ' \\times x_\\vec{v}$'
      paramY = '$y=y_A+' + tabVariables[0] + ' \\times y_\\vec{u}+' + tabVariables[1] + ' \\times y_\\vec{v}$'
      paramZ = '$z=z_A+' + tabVariables[0] + ' \\times z_\\vec{u}+' + tabVariables[1] + ' \\times z_\\vec{v}$'
      repParamType = [[paramX, paramY, paramZ]]
      j3pAffiche(stor.zoneExpli1, '', objetTextesCorrection.correction_gene)
      j3pAfficheSysteme(stor.zoneExpli2, '', objetTextesCorrection.correction_gene1, repParamType, { a: tabVariables[0], b: tabVariables[1], inputmq1: {}, inputmq2: {}, inputmq3: {} }, stor.explications.style.color)
      lesVecteurs = objetSolution.mes_vecteurs
      nomPointRefChoisi = objetSolution.mesPoints.nom[0]
      coordPointRefChoisi = objetSolution.mesPoints.coord[0]
      coordU1 = lesVecteurs.coord[0]
      coordU2 = lesVecteurs.coord[1]
      tabCoeffX = [coordPointRefChoisi[0], coordU1[0], coordU2[0]]
      tabCoeffY = [coordPointRefChoisi[1], coordU1[1], coordU2[1]]
      tabCoeffZ = [coordPointRefChoisi[2], coordU1[2], coordU2[2]]
      tabVariables = ['', tabVariables[0], tabVariables[1]]
      xSimplifie = '$x=' + sommeMonomes(tabCoeffX, tabVariables) + '$'
      ySimplifie = '$y=' + sommeMonomes(tabCoeffY, tabVariables) + '$'
      zSimplifie = '$z=' + sommeMonomes(tabCoeffZ, tabVariables) + '$'

      j3pAffiche(stor.zoneExpli3, 'zoneExpli', textes.correction_point_vecteurs, { A: nomPointRefChoisi })
      repSyst = [[xSimplifie, ySimplifie, zSimplifie]]
      j3pAfficheSysteme(stor.zoneExpli4, '', '‡1‡', repSyst, { inputmq1: {}, inputmq2: {}, inputmq3: {} }, stor.explications.style.color)

      return
    }
    if (objetReponse.juste) {
      paramX = '$x=x_A+' + objetReponse.nom_des_variables[0] + ' \\times x_\\vec{u}+' + objetReponse.nom_des_variables[1] + ' \\times x_\\vec{v}$'
      paramY = '$y=y_A+' + objetReponse.nom_des_variables[0] + ' \\times y_\\vec{u}+' + objetReponse.nom_des_variables[1] + ' \\times y_\\vec{v}$'
      paramZ = '$z=z_A+' + objetReponse.nom_des_variables[0] + ' \\times z_\\vec{u}+' + objetReponse.nom_des_variables[1] + ' \\times z_\\vec{v}$'
      repParamType = [[paramX, paramY, paramZ]]
      j3pAffiche(stor.zoneExpli5, '', objetTextesCorrection.correction_gene)
      j3pAfficheSysteme(stor.zoneExpli6, '', objetTextesCorrection.correction_gene1, repParamType, { a: objetReponse.nom_des_variables[0], b: objetReponse.nom_des_variables[1], inputmq1: {}, inputmq2: {}, inputmq3: {} }, stor.explications.style.color)

      if (!objetReponse.point_reference.autre) {
        j3pAffiche(stor.zoneExpli7, '', objetTextesCorrection[tabPhrases[1]], { A: objetReponse.point_reference.nom })
      } else {
        const lePoint = objetReponse.point_reference
        j3pAffiche(stor.zoneExpli7, '', objetTextesCorrection[tabPhrases[2]], { A: lePoint.nom, a: tableauFrac(lePoint.coord) })
      }
      const U1 = objetReponse.U1
      const U2 = objetReponse.U2
      coordPointRefChoisi = objetReponse.point_reference.coord
      coordU1 = objetReponse.U1.coord
      coordU2 = objetReponse.U2.coord

      j3pAffiche(stor.zoneExpli8, '', ' ' + objetTextesCorrection[tabPhrases[3]], { A: U1.nom, B: U2.nom })
    } else { // reponse fausse
      nomPointRefChoisi = ''
      coordPointRefChoisi = []
      lesVecteurs = objetSolution.mes_vecteurs
      nomU1 = ''
      coordU1 = []
      nomU2 = ''
      coordU2 = []

      // On commence par les cas les pires : pas le bon nombre de paramètres ou des fonctions non affines.
      // La correction est alors la plus générale possible
      tabVariables = ['s', 't']

      if (objetReponse.deux_variables === 2) {
        tabVariables = objetReponse.nom_des_variables
      }
      paramX = '$x=x_A+' + tabVariables[0] + ' \\times x_\\vec{u}+' + tabVariables[1] + ' \\times x_\\vec{v}$'
      paramY = '$y=y_A+' + tabVariables[0] + ' \\times y_\\vec{u}+' + tabVariables[1] + ' \\times y_\\vec{v}$'
      paramZ = '$z=z_A+' + tabVariables[0] + ' \\times z_\\vec{u}+' + tabVariables[1] + ' \\times z_\\vec{v}$'
      repParamType = [[paramX, paramY, paramZ]]
      j3pAffiche(stor.zoneExpli5, '', objetTextesCorrection.correction_gene)
      j3pAfficheSysteme(stor.zoneExpli6, '', objetTextesCorrection.correction_gene1, repParamType, { a: tabVariables[0], b: tabVariables[1], inputmq1: {}, inputmq2: {}, inputmq3: {} }, stor.explications.style.color)

      if (!objetReponse.interpretable || objetReponse.deux_variables !== 2) {
        // On choisit un point de référence au hasard parmi les 3
        indexPointRef = j3pGetRandomInt(0, 2)

        nomPointRefChoisi = objetSolution.mesPoints.nom[indexPointRef]
        coordPointRefChoisi = objetSolution.mesPoints.coord[indexPointRef]
        nomU1 = lesVecteurs.nom[2 * indexPointRef]
        coordU1 = lesVecteurs.coord[2 * indexPointRef]
        nomU2 = lesVecteurs.nom[2 * indexPointRef + 1]
        coordU2 = lesVecteurs.coord[2 * indexPointRef + 1]

        // var U1_choisi=objetSolution.mes_vecteurs//.nom[2*indexPointRef];
        // var U2_choisi=objetSolution.mes_vecteurs//.nom[2*indexPointRef+1];
        tabCoeffX = [coordPointRefChoisi[0], coordU1[0], coordU2[0]]

        tabCoeffY = [coordPointRefChoisi[1], coordU1[1], coordU2[1]]
        tabCoeffZ = [coordPointRefChoisi[2], coordU1[2], coordU2[2]]
        tabVariables = ['', tabVariables[0], tabVariables[1]]
        xSimplifie = '$x=' + sommeMonomes(tabCoeffX, tabVariables) + '$'
        ySimplifie = '$y=' + sommeMonomes(tabCoeffY, tabVariables) + '$'
        zSimplifie = '$z=' + sommeMonomes(tabCoeffZ, tabVariables) + '$'

        j3pAffiche(stor.zoneExpli7, '', objetTextesCorrection[tabPhrases[6]], { A: nomPointRefChoisi, B: nomU1, C: nomU2, b: tableauFrac(coordU1), c: tableauFrac(coordU2) })
        repSyst = [[xSimplifie, ySimplifie, zSimplifie]]
        j3pAfficheSysteme(stor.zoneExpli8, '', '‡1‡', repSyst, { inputmq1: {}, inputmq2: {}, inputmq3: {} }, stor.explications.style.color)
      } else { // le système est interprétable et il y a deux variables.
        if (objetReponse.point_reference.dans_le_plan) {
          nomPointRefChoisi = objetReponse.point_reference.nom
          coordPointRefChoisi = objetReponse.point_reference.coord
          indexPointRef = -1
          if (!objetReponse.point_reference.autre) {
            indexPointRef = objetReponse.point_reference.index
            j3pAffiche(stor.zoneExpli7, '', objetTextesCorrection.correction_point_un_des_trois, { A: nomPointRefChoisi })
          } else {
            j3pAffiche(stor.zoneExpli7, '', objetTextesCorrection.correction_point_autre, { A: nomPointRefChoisi, a: tableauFrac(coordPointRefChoisi) })
          }
        } else { //
          indexPointRef = j3pGetRandomInt(0, 2)
          const mauvaisPoint = objetReponse.point_reference.nom
          const coordMauvaisPoint = objetReponse.point_reference.coord
          nomPointRefChoisi = objetSolution.mesPoints.nom[indexPointRef]
          coordPointRefChoisi = objetSolution.mesPoints.coord[indexPointRef]
          j3pAffiche(stor.zoneExpli7, '', objetTextesCorrection.correction_mauvaisPoint, { A: mauvaisPoint, a: coordMauvaisPoint[0], b: coordMauvaisPoint[1], c: coordMauvaisPoint[2], B: nomPointRefChoisi })
        }
        if (objetReponse.U1.est_directeur) {
          if (objetReponse.U2.est_directeur) {
            j3pAffiche(stor.zoneExpli8, '', objetTextesCorrection.correction_vecteurs, { A: '=' + objetReponse.U1.nom, B: '=' + objetReponse.U2.nom })
          } else {
            j3pAffiche(stor.zoneExpli8, '', objetTextesCorrection.correction_vecteurs, { A: '=' + objetReponse.U1.nom, B: '(' + tableauFrac(objetReponse.U2.coord) + ')' })
          }
        } else {
          if (objetReponse.U2.est_directeur) {
            j3pAffiche(stor.zoneExpli8, '', objetTextesCorrection.correction_vecteurs, { A: '(' + tableauFrac(objetReponse.U1.coord) + ')', B: '=' + objetReponse.U2.nom })
          } else {
            j3pAffiche(stor.zoneExpli8, '', objetTextesCorrection.correction_vecteurs, { A: '(' + tableauFrac(objetReponse.U1.coord) + ')', B: '(' + tableauFrac(objetReponse.U2.coord) + ')' })
          }
        }
        if (objetReponse.U1.est_directeur && objetReponse.U2.est_directeur) {
          j3pAffiche(stor.zoneExpli9, '', objetTextesCorrection.correction_2_vecteurs_dirigent_mais)
          if (objetReponse.colineaires) {
            j3pAffiche(stor.zoneExpli10, '', objetTextesCorrection.correction_mais_colineaires, { A: objetReponse.rapport_colinearite })
          }
        }
        if (objetReponse.U1.est_directeur && !objetReponse.U2.est_directeur) {
          j3pAffiche(stor.zoneExpli9, '', objetTextesCorrection.correction_un_des_deux, { A: 'u', B: 'v' })
        }
        if (!objetReponse.U1.est_directeur && objetReponse.U2.est_directeur) {
          j3pAffiche(stor.zoneExpli9, '', objetTextesCorrection.correction_un_des_deux, { A: 'v', B: 'u' })
        }
        if (!objetReponse.U1.est_directeur && !objetReponse.U2.est_directeur) {
          j3pAffiche(stor.zoneExpli9, '', objetTextesCorrection.correction_aucun_des_deux, { A: 'u', B: 'v' })
        }
        // On choisit finalement comme vecteurs : soit ceux de l’élève s’ils sont bons, et on en génère deux autres s’ils ne sont pas bons:
        // soit en prenant ceux d’origine le point de référence de correction, soit le point de l’élève s’il est bon
        if (objetReponse.systeme_vect_direct) {
          // Le point de référence a déjà été choisi précédemment, on le complète avec les vect direct de l’élève
          nomU1 = objetReponse.U1.nom
          nomU2 = objetReponse.U2.nom
          coordU1 = objetReponse.U1.coord
          coordU2 = objetReponse.U2.coord
        } else { // le point de l’élève était le bon, mais pas de système de vecteur directeur, on le complète par 2 vecteurs directeurs
          if (indexPointRef === -1) {
            indexPointRef = j3pGetRandomInt(0, 2)
          }

          nomU1 = lesVecteurs.nom[2 * indexPointRef]
          coordU1 = lesVecteurs.coord[2 * indexPointRef]
          nomU2 = lesVecteurs.nom[2 * indexPointRef + 1]
          coordU2 = lesVecteurs.coord[2 * indexPointRef + 1]
        }
        tabCoeffX = [coordPointRefChoisi[0], coordU1[0], coordU2[0]]
        tabCoeffY = [coordPointRefChoisi[1], coordU1[1], coordU2[1]]
        tabCoeffZ = [coordPointRefChoisi[2], coordU1[2], coordU2[2]]
        tabVariables = ['', tabVariables[0], tabVariables[1]]
        xSimplifie = '$x=' + sommeMonomes(tabCoeffX, tabVariables) + '$'
        ySimplifie = '$y=' + sommeMonomes(tabCoeffY, tabVariables) + '$'
        zSimplifie = '$z=' + sommeMonomes(tabCoeffZ, tabVariables) + '$'

        j3pAffiche(stor.zoneExpli11, '', objetTextesCorrection[tabPhrases[6]], { A: nomPointRefChoisi, B: nomU1, C: nomU2, b: tableauFrac(coordU1), c: tableauFrac(coordU2) })
        repSyst = [[xSimplifie, ySimplifie, zSimplifie]]
        j3pAfficheSysteme(stor.zoneExpli12, '', '‡1‡ $' + tabVariables[1] + '\\in \\R,' + tabVariables[2] + '\\in \\R$', repSyst, { inputmq1: {}, inputmq2: {}, inputmq3: {} }, stor.explications.style.color)
      }
    }
    // dans tous les cas, on fait péter la 3D :
    stor.zoom = -10
    const A = objetSolution.mesPoints.nom[0]
    const B = objetSolution.mesPoints.nom[1]
    const C = objetSolution.mesPoints.nom[2]
    const a = objetSolution.mesPoints.coord[0]
    const b = objetSolution.mesPoints.coord[1]
    const c = objetSolution.mesPoints.coord[2]
    // Création du plan (ABC)
    // On crée les sommets d’un parallélogramme centré en A:
    const lambda = 10
    const P = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]
    const tabVec = [0, 0, 1, 1]
    for (let k = 0; k < 4; k++) {
      const lequel = tabVec[k]
      for (let m = 0; m < 3; m++) {
        P[k][m] = a[m] + Math.pow(-1, k) * lambda * objetSolution.mes_vecteurs.coord[lequel][m]
      }
    }
    // On crée à present un tableau d’objets à tracer, qui comprend de base les3 points qui définissent le plan
    // Et que l’on va compléter en fonction des réponses de l’élève
    const tabObjetsATracer = [{ nom: A, type: 'point', coord: a, couleur: 'vert' }, { nom: B, type: 'point', coord: b, couleur: 'vert' }, { nom: C, type: 'point', coord: c, couleur: 'vert' }]

    // Elements de correction toujours présents
    const xExtremiteU1 = coordPointRefChoisi[0] + coordU1[0]
    const yExtremiteU1 = coordPointRefChoisi[1] + coordU1[1]
    const zExtremiteU1 = coordPointRefChoisi[2] + coordU1[2]
    const coordExtremiteU1 = [xExtremiteU1, yExtremiteU1, zExtremiteU1]

    const xExtremiteU2 = coordPointRefChoisi[0] + coordU2[0]
    const yExtremiteU2 = coordPointRefChoisi[1] + coordU2[1]
    const zExtremiteU2 = coordPointRefChoisi[2] + coordU2[2]
    const coordExtremiteU2 = [xExtremiteU2, yExtremiteU2, zExtremiteU2]

    tabObjetsATracer.push({ nom: 'extremite_U1', type: 'point', coord: coordExtremiteU1, couleur: 'vert', visible: false })
    tabObjetsATracer.push({ nom: 'extremite_U2', type: 'point', coord: coordExtremiteU2, couleur: 'vert', visible: false })
    tabObjetsATracer.push({ nom: 'pt_ref_choisi', type: 'point', coord: coordPointRefChoisi, couleur: 'vert', visible: false })
    tabObjetsATracer.push({ nom: 'u1', type: 'vecteur', ext1: 'pt_ref_choisi', ext2: 'extremite_U1', pointilles: false, couleur: '#0000AA', visible: true })
    tabObjetsATracer.push({ nom: 'u2', type: 'vecteur', ext1: 'pt_ref_choisi', ext2: 'extremite_U2', pointilles: false, couleur: '#0000AA', visible: true })

    // On complète à présent la correction en fonction des données élèves si on a pu extraire le point de ref et 2 vecteurs
    // ie interpretable avec 2 variables

    if (objetReponse.interpretable && objetReponse.deux_variables === 2) {
      // On regarde à présent si le point choisi est l’un des points des 3, et le met en rouge s’il est faux, en bleu sinon
      if (objetReponse.point_reference.autre) {
        // On tracera les vecteurs choisis par l’élève depuis ce point
        const couleurPointRef = (objetReponse.point_reference.dans_le_plan) ? 'bleu' : 'rouge'
        tabObjetsATracer.push({ nom: objetReponse.point_reference.nom, type: 'point', coord: objetReponse.point_reference.coord, couleur: couleurPointRef })
      }

      // On regarde maintenant les vecteurs directeurs à tracer qui proviennent soit de la correction, soit de l’élève.
      // de façon à compléter tabObjetsATracer

      const xExtremiteEleveU1 = objetReponse.point_reference.coord[0] + objetReponse.U1.coord[0]
      const yExtremiteEleveU1 = objetReponse.point_reference.coord[1] + objetReponse.U1.coord[1]
      const zExtremiteEleveU1 = objetReponse.point_reference.coord[2] + objetReponse.U1.coord[2]
      const coordExtremiteEleveU1 = [xExtremiteEleveU1, yExtremiteEleveU1, zExtremiteEleveU1]

      const xExtremiteEleveU2 = objetReponse.point_reference.coord[0] + objetReponse.U2.coord[0]
      const yExtremiteEleveU2 = objetReponse.point_reference.coord[1] + objetReponse.U2.coord[1]
      const zExtremiteEleveU2 = objetReponse.point_reference.coord[2] + objetReponse.U2.coord[2]
      const coordExtremiteEleveU2 = [xExtremiteEleveU2, yExtremiteEleveU2, zExtremiteEleveU2]

      tabObjetsATracer.push({ nom: 'extremite_eleve_U1', type: 'point', coord: coordExtremiteEleveU1, couleur: 'vert', visible: false })
      tabObjetsATracer.push({ nom: 'extremite_eleve_U2', type: 'point', coord: coordExtremiteEleveU2, couleur: 'vert', visible: false })

      tabObjetsATracer.push({ nom: 'pt_ref_eleve', type: 'point', coord: objetReponse.point_reference.coord, couleur: 'vert', visible: false })

      let couleurVect = (objetReponse.U1.est_directeur) ? '#0000AA' : '#FFD700'
      tabObjetsATracer.push({ nom: 'u1_eleve', type: 'vecteur', ext1: 'pt_ref_eleve', ext2: 'extremite_eleve_U1', pointilles: false, couleur: couleurVect, visible: true })
      couleurVect = (objetReponse.U2.est_directeur) ? '#0000AA' : '#FFD700'
      tabObjetsATracer.push({ nom: 'u2_eleve', type: 'vecteur', ext1: 'pt_ref_eleve', ext2: 'extremite_eleve_U2', pointilles: false, couleur: couleurVect, visible: true })
    }
    /// /stor.construction_cube();
    // un pb de CSS du bouton bascule, pour l’instant non corrigé
    const objets1 = {
      faces_cachees: false,
      dim: { larg: 600, haut: 300, zoom: stor.zoom },
      sommets: [[], P[0], P[2], P[1], P[3]],
      faces: [[1, 2, 3, 4]],
      nomsommets: ['', 'a', 'b', 'c', 'd'],
      sommetsvisibles: ['', false, false, false, false],
      mef: {
        typerotation: 'Ox',
        couleursommets: { defaut: '#F00', A: '#000' },
        couleurfaces: { defaut: '#92BBD8', f0: '#FF0000' }
      },

      objets: tabObjetsATracer

    }
    const id3D = j3pGetNewId('div3D')
    j3pAddElt(stor.id3D, 'div', '', { id: id3D })

    stor.solide1 = new _3D(id3D, objets1)

    stor.solide1.rotation = { Ox: 0, Oy: 20, Oz: 30 }
    stor.solide1.arotation = stor.solide1.rotation
    stor.solide1.construit()
    j3pToggleFenetres(stor.nameBtns)
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    let xA, yA, zA, xB, yB, zB, xC, yC, zC, xU, yU, zU, xV, yV, zV, a, b, c
    do {
      xA = j3pGetRandomInt(-5, 5)
      yA = j3pGetRandomInt(-5, 5)
      zA = j3pGetRandomInt(-5, 5)

      xB = j3pGetRandomInt(-5, 5)
      yB = j3pGetRandomInt(-5, 5)
      zB = j3pGetRandomInt(-5, 5)

      xC = j3pGetRandomInt(-5, 5)
      yC = j3pGetRandomInt(-5, 5)
      zC = j3pGetRandomInt(-5, 5)

      // vecteurs directeurs

      xU = xB - xA
      yU = yB - yA
      zU = zB - zA
      xV = xC - xA
      yV = yC - yA
      zV = zC - zA

      a = yU * zV - zU * yV
      b = zU * xV - xU * zV
      c = xU * yV - yU * xV
    } while (a === 0 && b === 0 && c === 0)

    const d = -a * xA - b * yA - c * zA
    const xW = xC - xB
    const yW = yC - yB
    const zW = zC - zB

    const tabDesChoix = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'L', 'M', 'N', 'P', 'Q', 'R']
    const mesPoints = j3pGetRandomElts(tabDesChoix, 3)

    // Puis On supprime les lettres choisies dans le tableau de choix

    tabDesChoix.splice(tabDesChoix.indexOf(mesPoints[0]), 1)
    tabDesChoix.splice(tabDesChoix.indexOf(mesPoints[1]), 1)
    tabDesChoix.splice(tabDesChoix.indexOf(mesPoints[2]), 1)

    // Creation des objets points:
    const A = {}
    A.code = 'A'
    A.nom = mesPoints[0]
    A.coord = [xA, yA, zA]
    const B = {}
    B.code = 'B'
    B.nom = mesPoints[1]
    B.coord = [xB, yB, zB]
    const C = {}
    C.code = 'C'
    C.nom = mesPoints[2]
    C.coord = [xC, yC, zC]

    // calcul des 6 vecteurs possibles
    const AB = {}
    AB.code = 'AB'
    AB.nom = vLatex(mesPoints[0] + mesPoints[1])
    AB.coord = [xU, yU, zU]
    const AC = {}
    AC.code = 'AC'
    AC.nom = vLatex(mesPoints[0] + mesPoints[2])
    AC.coord = [xV, yV, zV]
    const BC = {}
    BC.code = 'BC'
    BC.nom = vLatex(mesPoints[1] + mesPoints[2])
    BC.coord = [xW, yW, zW]
    const BA = {}
    BA.code = 'BA'
    BA.nom = vLatex(mesPoints[1] + mesPoints[0])
    BA.coord = [-xU, -yU, -zU]
    const CA = {}
    CA.code = 'CA'
    CA.nom = vLatex(mesPoints[2] + mesPoints[0])
    CA.coord = [-xV, -yV, -zV]
    const CB = {}
    CB.code = 'CB'
    CB.nom = vLatex(mesPoints[2] + mesPoints[1])
    CB.coord = [-xW, -yW, -zW]

    // Récupération de toutes les infos dans un objet stor.solution
    stor.solution = {}
    stor.solution.lettres_restantes = tabDesChoix
    stor.solution.mesPoints = { code: ['A', 'B', 'C'], nom: [A.nom, B.nom, C.nom], coord: [A.coord, B.coord, C.coord] }
    stor.solution.mes_vecteurs = { code: ['AB', 'AC', 'BC', 'BA', 'CA', 'CB'], nom: [AB.nom, AC.nom, BC.nom, BA.nom, CA.nom, CB.nom], coord: [AB.coord, AC.coord, BC.coord, BA.coord, CA.coord, CB.coord] }
    stor.solution.AB = { x: xU, y: yU, z: zU, nom: AB.nom, coord: AB.coord }
    stor.solution.AC = { x: xV, y: yV, z: zV, nom: AC.nom, coord: AC.coord }
    stor.solution.coeff_a = a
    stor.solution.coeff_b = b
    stor.solution.coeff_c = c
    stor.solution.coeff_d = d
    stor.pb_interpretation = 0

    switch (ds.type_plan[me.questionCourante - 1]) {
      case 'point_vecteurs':
        //   phrase2 : "On considère le plan (P) passant par $A(£a,£b,£c)$ et ayant pour vecteurs directeurs $\\vec{u}(£d,£e,£f)$ et $\\vec{v}(£g,£h,£i)$ ",
        j3pAffiche(stor.zoneCons1, '', textes.phrase2, { A: A.nom, B: AB.nom, C: AC.nom, a: xA, b: yA, c: zA, d: AB.coord[0], e: AB.coord[1], f: AB.coord[2], g: AC.coord[0], h: AC.coord[1], i: AC.coord[2] })
        j3pAffiche(stor.zoneCons2, '', textes.phrase4)
        break

      default :// trois points
        j3pAffiche(stor.zoneCons1, '', textes.phrase1, { A: A.nom, B: B.nom, C: C.nom, a: xA, b: yA, c: zA, d: xB, e: yB, f: zB, g: xC, h: yC, i: zC })
        j3pAffiche(stor.zoneCons2, '', textes.phrase3, { A: A.nom, B: B.nom, C: C.nom })
        break
    }

    // exemples de div avec span et zones de saisie
    const elts = j3pAfficheSysteme(stor.zoneCons3, '', '‡1‡', [['$x=$&1&', '$y=$&2&', '$z=$&3&']], { inputmq1: {}, inputmq2: {}, inputmq3: {} }, me.styles.toutpetit.enonce.color)
    stor.inputMqs = []
    for (const elt of elts) {
      if (elt.inputmqList) {
        const inputMq = elt.inputmqList[0]
        stor.inputMqs.push(inputMq)
        mqRestriction(inputMq, 'abcdfghjklmnopqrstuvw\\d(+-)*,.')
        inputMq.typeReponse = ['texte']
      }
    }
    j3pFocus(stor.inputMqs[0])
    // Gestion en vue de la validation des réponse.
    // On définit les id zones et le type de réponses attendues
    // ici, bonneReponse n’est pas renseigné car toutes les réponses seront en évaluation perso'
    const mesZonesSaisie = stor.inputMqs.map(inputMq => inputMq.id)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesZonesSaisie })
    stor.explications = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.toutpetit.explications })
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '20px' }) })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':

      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.surcharge()

        // On regarde si les zones de saisie sont vides ou pas. On utilise la fonction de Rémi

        // Construction de la page
        me.construitStructurePage('presentation1')

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.idBtns = j3pGetNewId('fenetreBoutons')
        stor.nameBtns = j3pGetNewId('Boutons')
        stor.id3D = j3pGetNewId('fenetre3D')
        stor.name3D = j3pGetNewId('la_3D')
        me.fenetresjq = [
          // Attention : positions par rapport à mepact
          { name: stor.nameBtns, title: textes.titre3D, left: 660, top: 430, height: 100, id: stor.idBtns },
          { name: stor.name3D, title: '3D', width: 730, top: 120, left: 90, height: 500, id: stor.id3D }
        ]
        // Création mais sans affichage.
        j3pCreeFenetres(me)
        j3pCreeBoutonFenetre(stor.name3D, stor.idBtns, { top: 10, left: 10 }, textes.affichage_espace)
        j3pAfficheCroixFenetres(stor.name3D)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        j3pEmpty(stor.id3D)
        j3pMasqueFenetre(stor.nameBtns)
        j3pMasqueFenetre(stor.name3D)
      }

      enonceMain()

      break // case "enonce":

    case 'correction':
      {
        const objetTextes = textes
        const tabPhrasesIndications = ['indication_2var', 'indication_non_interpretable', 'indication_point_ref_dedans', 'indication_point_ref_dehors', 'indication_syst_direct_oui', 'indication_syst_direct_non']
        const tabPhrasesCorrection = ['correction_gene', 'correction_point_un_des_trois', 'correction_point_autre', 'correction_vect_direct', 'correction_vect_direct_2', 'correction_mauvaisPoint', 'correction_choix_point2']

        // On teste si une réponse a été saisie
        const repEleve1 = j3pValeurde(stor.inputMqs[0])
        const repEleve2 = j3pValeurde(stor.inputMqs[1])
        const repEleve3 = j3pValeurde(stor.inputMqs[2])

        const fctsValid = stor.fctsValid
        const reponse = fctsValid.validationGlobale()

        const objSol = stor.solution
        let valide
        if (reponse.aRepondu) {
        // l’élève a répondu, mais  réponse est fausse,on renvoie curseur emplacement faux
          valide = validation(repEleve1, repEleve2, repEleve3, objSol)
          reponse.bonneReponse = (reponse.bonneReponse && valide.juste)

          if (!valide.juste) {
          // On barre les zones fausses
            stor.fctsValid.zones.bonneReponse[0] = false
            stor.fctsValid.zones.bonneReponse[1] = false
            stor.fctsValid.zones.bonneReponse[2] = false
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // Cas où l’élève n’a pas tout complété
          me.reponseManquante(stor.zoneCorr)
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie
        // Bonne réponse avec correction éventuelle

          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            // me.styles.cbien
            // j3pElement("correction").innerHTML=cBien;
            stor.zoneCorr.innerHTML = cBien
            // On simplifie les écritures éventuelles :
            const tabCoeffX = [valide.point_reference.coord[0], valide.U1.coord[0], valide.U2.coord[0]]
            const tabCoeffY = [valide.point_reference.coord[1], valide.U1.coord[1], valide.U2.coord[1]]
            const tabCoeffZ = [valide.point_reference.coord[2], valide.U1.coord[2], valide.U2.coord[2]]
            const tabVariables = ['', valide.nom_des_variables[0], valide.nom_des_variables[1]]

            const xSimplifie = '$' + sommeMonomes(tabCoeffX, tabVariables) + '$'
            const ySimplifie = '$' + sommeMonomes(tabCoeffY, tabVariables) + '$'
            const zSimplifie = '$' + sommeMonomes(tabCoeffZ, tabVariables) + '$'
            j3pEmpty(stor.zoneCons3)
            j3pAfficheSysteme(stor.zoneCons3, '', '‡1‡', [['$x=$' + xSimplifie, '$y=$' + ySimplifie, '$z=$' + zSimplifie]], { style: { color: me.styles.cbien } }, me.styles.cbien)
            afficheCorrection(valide, objSol, objetTextes, tabPhrasesCorrection)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // mauvaise réponse

            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              for (const inputId of stor.fctsValid.zones.inputs) fctsValid.coloreUneZone(inputId)

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              // afficheCorrection(false,valide)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!valide.interpretable) {
                stor.pb_interpretation++
                if (stor.pb_interpretation < 2) {
                  ds.nbchances++
                }
              }
              for (const inputId of stor.fctsValid.zones.inputs) fctsValid.coloreUneZone(inputId)
              // On renvoie le focus à la première expression non affine détectée
              if (!valide.xEstAffine) {
                j3pFocus(stor.inputMqs[0])
              } else {
                if (!valide.yEstAffine) {
                  j3pFocus(stor.inputMqs[1])
                } else {
                  j3pFocus(stor.inputMqs[2])
                }
              }

              if (me.essaiCourant < ds.nbchances) {
              // cas où il reste une ou plusieurs chances pour répondre,on aide
              // Si l’une des variables n’est pas une fonction affine, ce n’est pas interprétable, on ne compte pas le premier essai
                if (stor.ajoutCorr) j3pDetruit(stor.ajoutCorr)
                if (!valide.interpretable) {
                  const chaine = indications(valide, objetTextes, tabPhrasesIndications)
                  stor.ajoutCorr = j3pAddElt(stor.zoneCorr, 'div')
                  j3pAffiche(stor.ajoutCorr, '', chaine, {
                    b: valide.nom_des_variables[0],
                    c: valide.nom_des_variables[1]
                  })
                } else {
                  stor.zoneCorr.innerHTML += indications(valide, objetTextes, tabPhrasesIndications)
                }
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici

              // Erreur au n ième essai
              } else {
                me._stopTimer()
                me.cacheBoutonValider()
                if (stor.ajoutCorr) j3pDetruit(stor.ajoutCorr)
                afficheCorrection(valide, objSol, objetTextes, tabPhrasesCorrection)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
