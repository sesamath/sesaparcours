import { j3pAddElt, j3pAjouteBouton, j3pEmpty, j3pFocus, j3pGetRandomElt, j3pGetRandomInt, j3pMonome, j3pPaletteMathquill, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import $ from 'jquery'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { afficheSystemeEquations } from 'src/lib/widgets/systemeEquations'
import { j3pAfficheSysteme } from 'src/legacy/core/functionsSysteme'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Programme python à compléter, la faire avec l’outil n’était pas possible, la validation telle qu’elle peut être faite n’étant pas permise avec de l’aléatoire
 * @author Rémi DENIAUD
 * @since janvier 2022
 * @fileOverview Cette section simule un échantillon avec choix aléatoire de deux valeurs
 */

/**
 * Les paramètres de la section, anciennement j3p.SectionXxx = {}
 */
export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {
    // Nombre de répétitions de l’exercice
    nbrepetitions: 2,
    nbchances: 2,
    nbetapes: 3,
    indication: '',
    textes: {
      titre_exo: 'Distance d’un point à un plan dans l’espace',
      consigne1: 'On définit dans un repère orthonormé de l’espace un plan $P$ d’équation cartésienne $£e$.',
      consigne2: 'Le point $£A$ de coordonnées $£c$ n’est pas inclus dans ce plan.',
      consigne3: 'On cherche à calculer en plusieurs étapes la distance de ce point $£A$ au plan $P$.',
      consigne4: 'Donner, dans un premier temps, une représentation paramétrique de la perpendiculaire à $P$ passant par $£A$.',
      consigne5: '‡1‡ avec $t\\in\\R$ est une représentation paramétrique de la droite $d$ perpendiculaire à $P$ passant par $£A$.',
      consigne6: 'Déterminer les coordonnées du point $H$ intersection du plan $P$ et de la droite $d$.',
      consigne7: 'Le point $H$, intersection du plan $P$ et de la droite $d$, a pour coordonnées $£h$.',
      consigne8: 'La distance du point $£A$ au plan $P$ vaut alors exactement &1&.',
      indic: 'On utilisera $t$ comme paramètre dans cette représentation.',
      comment1: 'Le paramètre t doit être présent.',
      pbVecteur: 'Le vecteur de la représentation proposée n’est pas correct&nbsp;!',
      pbPoint: 'Le point de la représentation proposée n’est pas correct&nbsp;!',
      pbPtVec: 'Le vecteur et le point de la représentation proposée ne sont pas corrects&nbsp;!',
      corr1: 'Un vecteur normal au plan $P$ a pour coordonnées $£n$. Ce vecteur est alors un vecteur directeur de la perpendiculaire à $P$.',
      corr2: 'De plus, cette droite passant par $£A$, on peut en déduire qu’une représentation paramétrique de la perpendiculaire au plan $P$ passant par $£A$ est ‡1‡.',
      corr3: 'On résout le système ‡1‡.',
      corr4: 'En remplaçant $x$, $y$ et $z$ par leurs expressions respectives dans l’équation du plan, on trouve&nbsp;:',
      corr4_2: 'La résolution de cette équation donne $t=£t$.',
      corr5: 'On remplace alors $t$ par $£t$ dans les expressions de $x$, $y$ et $z$, ce qui donne ‡1‡.',
      corr6: 'Donc le point $H$ a pour coordonnées $£h$.',
      corr7: 'La distance du point $£A$ au plan $P$ est finalement la distance $£AH$ qui vaut :'
    }
  }
} // getDonnees

function avecPar (nb) {
  // Cette fonction sert juste à ajouter des parenthèses à un nombre négatif (ce nombre étant nécessairement entier) pour le faire apparaître dans un calcul
  return (nb < 0) ? '(' + nb + ')' : nb
}
function simplifieRadical (nb) {
  // Cette fonction renvoie \\sqrt{nb} sous forme réduite
  let diviseur = 2
  let copieNb = nb
  let radicalextrait = 1
  while (Math.pow(diviseur, 2) <= copieNb) {
    if (Math.abs(copieNb / Math.pow(diviseur, 2) - Math.round(copieNb / Math.pow(diviseur, 2))) < Math.pow(10, -12)) {
      radicalextrait *= diviseur
      copieNb /= Math.pow(diviseur, 2)
    } else {
      diviseur += 1
    }
  }
  return {
    expression: ((radicalextrait === 1) ? '' : radicalextrait) + ((copieNb === 1) ? '' : '\\sqrt{' + copieNb + '}'),
    a: radicalextrait,
    b: copieNb
  }
}
/**
 * Retourne les données pour chaque sujet
 * @private
 * @return {Object}
 */
function genereAlea () {
  const obj = {}
  // nom du point A
  obj.A = j3pGetRandomElt(['A', 'B', 'C'])
  // valeurs a, b et c de l’équation ax+by+cy+d du plan (donc coordonnées du vecteur normal au plan)
  obj.coordn = []
  let pb
  do {
    do {
      obj.coordn[0] = j3pGetRandomElt([-1, 1]) * j3pGetRandomInt(1, 4)
      obj.coordn[1] = j3pGetRandomElt([-1, 1]) * j3pGetRandomInt(1, 4)
      obj.coordn[2] = j3pGetRandomElt([-1, 1]) * j3pGetRandomInt(1, 4)
    } while ((Math.abs(obj.coordn[0]) === Math.abs(obj.coordn[1])) && (Math.abs(obj.coordn[0]) === Math.abs(obj.coordn[2])))
    // Coordonnées du projeté orthogonal de A sur le plan
    obj.coordH = []
    for (let i = 1; i <= 3; i++) obj.coordH.push(j3pGetRandomElt([-1, 1]) * j3pGetRandomInt(1, 7))
    // coef qui va me permettre de positionner A : un entier entre -2 et 2 sauf 0, c’est la valeur du paramètre t
    obj.t = j3pGetRandomElt([-1, 1]) * j3pGetRandomInt(1, 2)
    // Coordonnées du point A
    obj.coordA = obj.coordH.map((coord, index) => {
      return coord - obj.t * obj.coordn[index]
    })
    let nb0 = 0
    obj.coordA.forEach(coord => {
      if (Math.abs(coord) < Math.pow(10, -10)) nb0++
    })
    pb = nb0 > 1
  } while (pb)
  // écriture des coordonnées de \vec{n}
  obj.n = '(' + obj.coordn[0] + ';' + obj.coordn[1] + ';' + obj.coordn[2] + ')'
  // écriture des coordonnées de H
  obj.h = '(' + obj.coordH[0] + ';' + obj.coordH[1] + ';' + obj.coordH[2] + ')'
  obj.d = -(obj.coordn[0] * obj.coordH[0] + obj.coordn[1] * obj.coordH[1] + obj.coordn[2] * obj.coordH[2])
  // écriture de l’équation
  obj.e = j3pMonome(1, 1, obj.coordn[0], 'x') + j3pMonome(2, 1, obj.coordn[1], 'y') + j3pMonome(3, 1, obj.coordn[2], 'z') + j3pMonome(4, 0, obj.d, 'x') + '=0'
  // écriture des coordonnées de A
  obj.c = '(' + obj.coordA[0] + ';' + obj.coordA[1] + ';' + obj.coordA[2] + ')'
  // Array pour la représentation paramétrique
  const reprx = j3pMonome(1, 0, obj.coordA[0]) + j3pMonome((obj.coordA[0] === 0) ? 1 : 2, 1, obj.coordn[0], 't')
  const repry = j3pMonome(1, 0, obj.coordA[1]) + j3pMonome((obj.coordA[1] === 0) ? 1 : 2, 1, obj.coordn[1], 't')
  const reprz = j3pMonome(1, 0, obj.coordA[2]) + j3pMonome((obj.coordA[2] === 0) ? 1 : 2, 1, obj.coordn[2], 't')
  obj.representation = ['$x=' + reprx + '$',
    '$y=' + repry + '$',
    '$z=' + reprz + '$'
  ]
  // Pour le calcul des coordonnées du point H, j’ai besoin d’écrire ax+by+cz+d=0 en remplaçant x, y et z par leurs expressions (celle de la représentation paramétrique)
  obj.eqAResoudre = (obj.coordn[0] === 1)
    ? reprx
    : j3pMonome(1, 1, obj.coordn[0], (reprx.includes('+') || reprx.includes('-')) ? '(' + reprx + ')' : (obj.coordn[0] === -1) ? reprx : '\\times ' + reprx)
  obj.eqAResoudre += j3pMonome(2, 1, obj.coordn[1], (repry.includes('+') || repry.includes('-')) ? '(' + repry + ')' : (obj.coordn[1] === -1) ? repry : '\\times ' + repry)
  obj.eqAResoudre += j3pMonome(2, 1, obj.coordn[2], (reprz.includes('+') || reprz.includes('-')) ? '(' + reprz + ')' : (obj.coordn[2] === -1) ? reprz : '\\times ' + reprz)
  obj.eqAResoudre += j3pMonome(4, 0, obj.d) + '=0'
  obj.calculDistance = obj.A + 'H=\\sqrt{(x_H-x_' + obj.A + ')^2+(y_H-y_' + obj.A + ')^2+(z_H-z_' + obj.A + ')^2}'
  obj.valCarre = Math.pow(obj.coordA[0] - obj.coordH[0], 2) + Math.pow(obj.coordA[1] - obj.coordH[1], 2) + Math.pow(obj.coordA[2] - obj.coordH[2], 2)
  obj.distance = Math.sqrt(obj.valCarre)
  obj.valDist = obj.A + 'H=\\sqrt{(' + obj.coordH[0] + '-' + avecPar(obj.coordA[0]) + ')^2+(' + obj.coordH[1] + '-' + avecPar(obj.coordA[1]) + ')^2+(' + obj.coordH[2] + '-' + avecPar(obj.coordA[2]) + ')^2}'
  obj.ccl = obj.A + 'H=\\sqrt{' + obj.valCarre + '}'
  // Peut-être peut-on simplifier ce radical
  const objSimplifieRadical = simplifieRadical(obj.valCarre)
  if (objSimplifieRadical.a > 1) obj.ccl += '=' + objSimplifieRadical.expression
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(me.donneesSection.textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  function ecoute (laZone, listePalette) {
    // listePalette est la liste des bouton de la palette
    if (laZone.className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, laZone, { liste: listePalette })
    }
  }
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
  me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]
  j3pCreeFenetres(me)
  j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
  j3pAfficheCroixFenetres('Calculatrice')
  if (me.questionCourante % ds.nbetapes === 1) stor.objDonnees = genereAlea()
  // on affiche l’énoncé
  for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
  // Consignes communes à chaque étape
  j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, stor.objDonnees)
  j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, stor.objDonnees)
  j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3, stor.objDonnees)
  stor.zoneInput = []
  const lstCommandes = []
  if (me.questionCourante % ds.nbetapes === 1) {
    // Recherche de la représentation paramétrique
    stor.divInfo = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px', fontStyle: 'italic', fontSize: '0.9em' } })
    j3pAffiche(stor.divInfo, '', ds.textes.indic)
    j3pAffiche(stor.zoneCons4, '', ds.textes.consigne4, stor.objDonnees)
    stor.divSysteme = j3pAddElt(stor.conteneur, 'div')
    const elts = afficheSystemeEquations(stor.divSysteme, [
      { contenu: '$x=$&1&', options: { inputmq1: {} } },
      { contenu: '$y=$&2&', options: { inputmq2: {} } },
      { contenu: '$z=$&3&', options: { inputmq3: {} } }
    ])
    lstCommandes.push('fraction')
    for (const { inputmqList } of elts) {
      const inputMq = inputmqList[0]
      stor.zoneInput.push(inputMq)
      mqRestriction(inputMq, 't/\\d+-', { commandes: lstCommandes })
    }
  } else {
    j3pAfficheSysteme(stor.zoneCons4, '', ds.textes.consigne5, [stor.objDonnees.representation], stor.objDonnees)
  }
  if (me.questionCourante % ds.nbetapes === 2) {
    // Recherche de l’intersection du plan et de la droite
    const zoneCons5 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons5, '', ds.textes.consigne6)
    const zoneCons6 = j3pAddElt(stor.conteneur, 'div')
    const elts = j3pAffiche(zoneCons6, '', 'H(&1&;&2&;&3&)', {
      inputmq1: {},
      inputmq2: {},
      inputmq3: {}
    })
    lstCommandes.push('fraction')
    elts.inputmqList.forEach((eltInput, index) => {
      stor.zoneInput.push(eltInput)
      mqRestriction(eltInput, '/\\d-', { commandes: lstCommandes })
      eltInput.typeReponse = ['nombre', 'exact']
      eltInput.reponse = [stor.objDonnees.coordH[index]]
    })
  } else if (me.questionCourante % ds.nbetapes === 0) {
    // Distance du point A au plan
    const zoneCons5 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons5, '', ds.textes.consigne7, stor.objDonnees)
    const zoneCons6 = j3pAddElt(stor.conteneur, 'div')
    const elts = j3pAffiche(zoneCons6, '', ds.textes.consigne8, stor.objDonnees, { inputmq1: {} })
    stor.zoneInput.push(elts.inputmqList[0])
    lstCommandes.push('fraction', 'puissance', 'racine')
    mqRestriction(stor.zoneInput[0], '\\d-^/', { commandes: lstCommandes })
    stor.zoneInput[0].typeReponse = ['nombre', 'exact']
    stor.zoneInput[0].reponse = [stor.objDonnees.distance]
  }
  stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
  stor.zoneInput.forEach(eltInput => $(eltInput).focusin(function () { ecoute(this, lstCommandes) }))
  ecoute(stor.zoneInput[0], lstCommandes)
  const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
  if (me.questionCourante % ds.nbetapes === 1) stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesZonesSaisie })
  else stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
  j3pFocus(stor.zoneInput[0])
  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  j3pEmpty(stor.laPalette)
  j3pEmpty(stor.zoneCalc)
  j3pDetruitFenetres('Calculatrice')
  // on affiche la correction dans la zone xxx
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  const zonesExpli = []
  for (let i = 1; i <= 2; i++) zonesExpli.push(j3pAddElt(zoneExpli, 'div'))
  if (me.questionCourante % ds.nbetapes === 1) {
    // représentation paramétrique de la droite
    j3pAffiche(zonesExpli[0], '', ds.textes.corr1, stor.objDonnees)
    j3pAfficheSysteme(zonesExpli[1], '', ds.textes.corr2, [stor.objDonnees.representation], stor.objDonnees, zoneExpli.style.color)
  } else if (me.questionCourante % ds.nbetapes === 2) {
    const systemeq2 = ['$' + stor.objDonnees.e + '$'].concat(stor.objDonnees.representation)
    j3pAfficheSysteme(zonesExpli[0], '', ds.textes.corr3, [systemeq2], stor.objDonnees, zoneExpli.style.color)
    j3pAffiche(zonesExpli[1], '', ds.textes.corr4, stor.objDonnees)
    for (let i = 1; i <= 4; i++) zonesExpli.push(j3pAddElt(zoneExpli, 'div'))
    j3pAffiche(zonesExpli[2], '', '$' + stor.objDonnees.eqAResoudre + '$')
    j3pAffiche(zonesExpli[3], '', ds.textes.corr4_2, stor.objDonnees)
    j3pAfficheSysteme(zonesExpli[4], '', ds.textes.corr5, [['$x=' + stor.objDonnees.coordH[0] + '$', '$y=' + stor.objDonnees.coordH[1] + '$', '$z=' + stor.objDonnees.coordH[2] + '$']], stor.objDonnees, zoneExpli.style.color)
    j3pAffiche(zonesExpli[5], '', ds.textes.corr6, stor.objDonnees)
  } else {
    // distance AH
    j3pAffiche(zonesExpli[0], '', ds.textes.corr7, stor.objDonnees)
    j3pAffiche(zonesExpli[1], '', '$' + stor.objDonnees.calculDistance + '$')
    for (let i = 1; i <= 2; i++) zonesExpli.push(j3pAddElt(zoneExpli, 'div'))
    j3pAffiche(zonesExpli[2], '', '$' + stor.objDonnees.valDist + '$')
    j3pAffiche(zonesExpli[3], '', '$' + stor.objDonnees.ccl + '$')
  }
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const ds = me.donneesSection
  const reponse = stor.fctsValid.validationGlobale()
  stor.presencet = true
  if (reponse.aRepondu) {
    if (me.questionCourante % ds.nbetapes === 1) {
      // On doit valider chaque zone à la main avec mtg32
      stor.presencet = stor.fctsValid.zones.reponseSaisie.some(rep => rep.includes('t'))
      if (!stor.presencet) return null
      // je cherche les coefs
      const coordPt = []
      const coordVec = []
      stor.fctsValid.zones.reponseSaisie.forEach(rep => {
        const unarbre = new Tarbre(rep, ['t'])
        coordPt.push(unarbre.evalue([0]))
        coordVec.push(unarbre.evalue([1]) - coordPt[coordPt.length - 1])
      })
      // On teste si le vecteur est colinéaire à notre vecteur normal
      const tabRapport = []
      for (let i = 0; i < 3; i++) tabRapport.push(coordVec[i] / stor.objDonnees.coordn[i])
      stor.bonVecteur = ((Math.abs(tabRapport[1] - tabRapport[0]) < Math.pow(10, -12)) && (Math.abs(tabRapport[2] - tabRapport[0]) < Math.pow(10, -12)))
      // Je vérifie si le point appartient à la droite cherchée
      const replacePt = []
      for (let i = 0; i < 3; i++) replacePt.push((coordPt[i] - stor.objDonnees.coordA[i]) / stor.objDonnees.coordn[i])
      stor.bonPoint = ((Math.abs(replacePt[1] - replacePt[0]) < Math.pow(10, -12)) && (Math.abs(replacePt[2] - replacePt[0]) < Math.pow(10, -12)))
      reponse.bonneReponse = (stor.bonVecteur && stor.bonPoint)
      if (!reponse.bonneReponse) {
        for (let i = 0; i < 3; i++) stor.fctsValid.zones.bonneReponse[i] = false
      }
      stor.fctsValid.zones.inputs.forEach(idInput => stor.fctsValid.coloreUneZone(idInput))
    }
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    if (me.questionCourante % ds.nbetapes === 1 && !stor.presencet) me.reponseManquante(stor.divCorrection, ds.textes.comment1)
    else me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
    if (me.questionCourante % ds.nbetapes === 1) {
      if (!stor.bonVecteur || !stor.bonPoint) j3pAddElt(stor.divCorrection, 'br')
      if (!stor.bonVecteur) {
        if (!stor.bonPoint) me.reponseKo(stor.divCorrection, ds.textes.pbPtVec, true)
        else me.reponseKo(stor.divCorrection, ds.textes.pbVecteur, true)
      } else {
        if (!stor.bonPoint) me.reponseKo(stor.divCorrection, ds.textes.pbPoint, true)
      }
    }
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
