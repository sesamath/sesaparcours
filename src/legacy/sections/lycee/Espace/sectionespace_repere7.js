import { j3pAddContent, j3pAddElt, j3pBarre, j3pBoutonRadio, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pRemplace, j3pShowError, j3pShuffle, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import { j3pCreeFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { pgcd } from 'src/lib/utils/number'
import { afficheSystemeEquations } from 'src/lib/widgets/systemeEquations'
import { j3pCalculValeur, j3pEvalue, j3pEvalueExpression } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/**
   Scénario : représentation paramétrique d’une droite donnée, on donne de plus un point ou un vecteur, est-ce que c’est un point de la droite ? (si oui, donner la valeur de t correspondante) ou est-ce que c’st un vecteur directeur de la droite ?
 * @author Alexis Lecomte
 * @since 2014-06
 * @fileOverview
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['chance_sup', true, 'boolean', 'Pour donner une chance supplémentaire si bon au QCM et qu’il faut renseigner le coefficient t.'],
    ['type_question', ['point', 'point', 'point'], 'array', "Tableau de même longueur que nbrepetitions contenant les valeurs 'point' ou 'vecteur' suivant la question souhaitée."],
    ['proba_appartient', [1, 0, 0.5], 'array', 'Tableau de même longueur que nbrepetitions, pour savoir si la réponse est oui (1) ou non (0) ou aléatoire paramétrable'],
    ['type_coef', ['de_tete', 'entier', 'fractionnaire'], 'array', 'Tableau de même longueur que nbrepetitions, entier ou fractionnaire ou de_tete (entier égal à 0 ou 1 pour le point, 1 ou -1 pour le vecteur)']
  ]
}
const textes = {
  titreFenetre: 'Correction détaillée :',
  titre_exo: 'Elements caractéristiques d’une droite',
  consigne1: 'Dans un repère orthonormé de l’espace, on considère la droite $D$ dont une représentation paramétrique est :',
  consigne2: 'avec $t\\in \\R$.',
  consignePoint: 'Le point $A(£a;£b;£c)$ appartient-il à cette droite ?',
  consigneVecteur: 'Le vecteur $\\vec{u}(£a;£b;£c)$ est-il un vecteur directeur de cette droite ?',
  consigne5: 'Valeur correspondante du paramètre $t$ : &1&',
  qcm_incomplet: 'Il faut choisir une réponse !',
  qcmbon: '<br>Tu as sélectionné la bonne proposition mais le coefficient est à corriger.',
  details: 'Regarde les explications détaillées.',
  correction3: 'Pour savoir si le point proposé appartient ou non à la droite, il faut résoudre le système suivant d’inconnue $t$ :',
  correction3_1: 'En remplaçant $t$ par 0 dans la représentation paramétrique, on obtient les coordonnnées $(£a;£b;£c)$.',
  correction3_2: 'En remplaçant $t$ par 1 dans la représentation paramétrique, on obtient les coordonnnées $(£a;£b;£c)$.',
  correction4: 'En utilisant les coefficients multipliant $t$ de la représentation paramétrique, on obtient comme vecteur directeur $\\vec{v}(£a;£b;£c)$.',
  correction4_1: 'Le vecteur $\\vec{u}$ proposé dans l’énoncé est donc bien un vecteur directeur de la droite.',
  correction4_10: 'Le vecteur $\\vec{u}(£a;£b;£c)$ proposé dans l’énoncé est donc bien un vecteur directeur de la droite.',
  correction4_11: 'Le vecteur $\\vec{u}(£a;£b;£c)$ proposé dans l’énoncé n’est donc pas un vecteur directeur de la droite.',
  correction4_2: 'Il faut donc déterminer si le vecteur proposé dans l’énoncé est colinéaire à $\\vec{v}$ ou non, en testant par exemple les égalités des produits en croix entre les coordonnées des deux vecteurs :',
  correction4_3: '$\\vec{u}(£a;£b;£c)$<br>$\\vec{v}(£d;£e;£f)$',
  correction4_4: 'On a au moins une égalité des produits en croix qui n’est pas vérifiée, les coefficients de ces deux vecteurs ne sont donc pas proportionnels. $\\vec{u}$ et $\\vec{v}$ ne sont donc pas colinéaires, ce dernier n’est donc pas un vecteur directeur de la droite.',
  correction4_5: 'Or, $£a \\times £b = £c \\times £d$<br>et $£c \\times £e = £f \\times £b$<br>',
  correction4_6: 'On peut aussi remarquer que $\\vec{u}=£a\\vec{v}$.',
  correction5_1: 'La première équation est toujours vraie.',
  correction5_2: 'La deuxième équation est toujours vraie.',
  correction5_3: 'La troisième équation est toujours vraie.',
  correction7_1: 'La première équation équivaut à $t=£a$.',
  correction7_2: 'La deuxième équation équivaut à $t=£a$.',
  correction7_3: 'La troisième équation équivaut à $t=£a$.',
  correction8_1: 'Le système a donc une unique solution, le point $A$ appartient bien à la droite $D$, avec $t=£a$.',
  correction8_2: 'Le système n’a donc pas de solution, le point $A$ n’est pas un point de la droite $D$.'
}

/**
 * section espace_repere7
 * @this {Parcours}
 */
export default function mainFunction () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  // FONCTIONS pour afficher/masquer les zones de saisies suivant la case du qcm cochée
  function afficheOnClickOui () {
    j3pEmpty(stor.zoneCons4)
    stor.elts = j3pAffiche(stor.zoneCons4, '', textes.consigne5, {
      inputmq1: { texte: '', correction: '"' }
    })
    stor.laPalette = j3pAddElt(stor.zoneCons4, 'div')
    j3pStyle(stor.laPalette, { paddingTop: '15px' })
    mqRestriction(stor.elts.inputmqList[0], '/\\d-,.', {
      commandes: ['fraction']
    })
    j3pPaletteMathquill(stor.laPalette, stor.elts.inputmqList[0], { liste: ['fraction'] })
    j3pFocus(stor.elts.inputmqList[0])
  }

  function afficheOnClickNon () {
    if (stor.btnOui.checked) {
      j3pEmpty(stor.zoneCons4)
      stor.elts = null
    }
  }

  /**
   * Retourne l’évaluation du qcm et du param (param undefined si y’avait pas de zone de saisie du param
   * @private
   * @return {{qcmOk: boolean, paramOk: boolean}}
   */
  function evalueReponse () {
    const bonneCase = stor.appartient ? stor.btnOui : stor.btnNon
    const rep = {
      qcmOk: bonneCase.checked
    }
    // faut répondre à la 2e question dans le cas point
    if (stor.elts?.inputmqList) {
      // on récupère la réponse de l’élève au paramètre demandé
      const reponse = j3pValeurde(stor.elts.inputmqList[0])
      rep.paramOk = (Math.abs(Number((j3pEvalue(j3pMathquillXcas(reponse) + '-(' + stor.solution + ')')))) < 0.0000001)
    } // sinon il n’y avait pas de zone de saisie
    return rep
  }

  function correctionDetaillee (bonneRep) {
    // on créé le div : (rq : remplacer par general_enonce si affichage dans MepMG)
    const explications = j3pAddElt(stor.explications, 'div', '', { style: me.styles.toutpetit.explications })
    if (bonneRep) j3pStyle(explications, { color: me.styles.cbien })
    // on affiche la fenetre et on la remplit ensuite.
    for (let i = 1; i <= 6; i++) stor['zoneExpli' + i] = j3pAddElt(explications, 'div')
    if (stor.questionCouranteEstPoint) {
      j3pAffiche(stor.zoneExpli1, '', textes.correction3)
      afficheSystemeEquations(stor.zoneExpli2, [{
        contenu: '$£a=' + stor.lignesSysteme.x + '$',
        options: { a: stor.coordA[0] }
      }, {
        contenu: '$£b=' + stor.lignesSysteme.y + '$',
        options: { b: stor.coordA[1] }
      }, {
        contenu: '$£c=' + stor.lignesSysteme.z + '$',
        options: { c: stor.coordA[2] }
      }])
      const exprConstante = stor.coordVecteur.map(coord => !coord) // transforme 0 en true et le reste en false
      if (exprConstante[0]) {
        j3pAffiche(stor.zoneExpli3, '', textes.correction5_1)
      } else {
        j3pAffiche(stor.zoneExpli3, '', textes.correction7_1, { a: stor.paramA[0] })
      }
      if (exprConstante[1]) {
        j3pAffiche(stor.zoneExpli4, '', textes.correction5_2)
      } else {
        j3pAffiche(stor.zoneExpli4, '', textes.correction7_2, { a: stor.paramA[1] })
      }
      if (exprConstante[2]) {
        j3pAffiche(stor.zoneExpli5, '', textes.correction5_3)
      } else {
        j3pAffiche(stor.zoneExpli5, '', textes.correction7_3, { a: stor.paramA[2] })
      }
      if (stor.appartient) {
        j3pAffiche(stor.zoneExpli6, '', textes.correction8_1, { a: stor.paramA[2] })
      } else {
        j3pAffiche(stor.zoneExpli6, '', textes.correction8_2)
      }
    } else {
      // on a demandé un vecteur
      j3pAffiche(stor.zoneExpli1, '', textes.correction4, {
        a: stor.coordVecteur[0],
        b: stor.coordVecteur[1],
        c: stor.coordVecteur[2]
      })
      if (ds.type_coef[me.questionCourante - 1] === 'de_tete') {
        if (stor.appartient) {
          if (stor.coordVecteur[0] === stor.coordA[0]) {
            j3pAffiche(stor.zoneExpli2, '', textes.correction4_1)
          } else {
            j3pAffiche(stor.zoneExpli2, '', textes.correction4_10, {
              a: stor.coordA[0],
              b: stor.coordA[1],
              c: stor.coordA[2]
            })
          }
        } else {
          j3pAffiche(stor.zoneExpli2, '', textes.correction4_11, {
            a: stor.coordA[0],
            b: stor.coordA[1],
            c: stor.coordA[2]
          })
        }
      } else {
        j3pAffiche(stor.zoneExpli2, '', textes.correction4_2)
        j3pAffiche(stor.zoneExpli3, '', textes.correction4_3, {
          a: stor.coordVecteur[0],
          b: stor.coordVecteur[1],
          c: stor.coordVecteur[2],
          d: stor.coordA[0],
          e: stor.coordA[1],
          f: stor.coordA[2]
        })
        const valB = j3pCalculValeur(stor.coordA[1])
        const valD = j3pCalculValeur(stor.coordA[0])
        const valE = j3pCalculValeur(stor.coordA[2])
        if (stor.appartient) { // tous les produits en croix seront justes
          j3pAffiche(stor.zoneExpli4, '', textes.correction4_5, {
            a: stor.coordVecteur[0],
            b: (valB < 0) ? '\\left(' + stor.coordA[1] + '\\right)' : stor.coordA[1],
            c: stor.coordVecteur[1],
            d: (valD < 0) ? '\\left(' + stor.coordA[0] + '\\right)' : stor.coordA[0],
            e: (valE < 0) ? '\\left(' + stor.coordA[2] + '\\right)' : stor.coordA[2],
            f: stor.coordVecteur[2]
          })
          if (ds.type_coef[me.questionCourante - 1] === 'entier') {
            j3pAffiche(stor.zoneExpli5, '', textes.correction4_6, { a: stor.choix })
          }
          j3pAffiche(stor.zoneExpli6, '', textes.correction4_1)
        } else { // au moins un produit en croix de faux...
          if (Math.abs(stor.coordVecteur[0] * valB - stor.coordVecteur[1] * valD) > 0.00001) {
            j3pAffiche(stor.zoneExpli4, '', '$£a \\times £b \\neq £c \\times £d$', {
              a: stor.coordVecteur[0],
              b: (valB < 0) ? '\\left(' + stor.coordA[1] + '\\right)' : stor.coordA[1],
              c: stor.coordVecteur[1],
              d: (valD < 0) ? '\\left(' + stor.coordA[0] + '\\right)' : stor.coordA[0]
            })
          } else {
            if (Math.abs(stor.coordVecteur[1] * valE - stor.coordVecteur[2] * valB) > 0.00001) {
              j3pAffiche(stor.zoneExpli4, '', '$£a \\times £b \\neq £c \\times £d$', {
                a: stor.coordVecteur[1],
                b: (valE < 0) ? '\\left(' + stor.coordA[2] + '\\right)' : stor.coordA[2],
                c: stor.coordVecteur[2],
                d: (valB < 0) ? '\\left(' + stor.coordA[1] + '\\right)' : stor.coordA[1]
              })
            } else { // dernière possibilité, ça coince entre les coordonnées 1 et 3 uniquement (par exemple si 0 en coord 2 pour les deux vecteurs, pas de bol mais ça m’est arrivé...)
              j3pAffiche(stor.zoneExpli4, '', '$£a \\times £b \\neq £c \\times £d$', {
                a: stor.coordVecteur[0],
                b: (valE < 0) ? '\\left(' + stor.coordA[2] + '\\right)' : stor.coordA[2],
                c: stor.coordVecteur[2],
                d: (valD < 0) ? '\\left(' + stor.coordA[0] + '\\right)' : stor.coordA[0]
              })
            }
          }
          j3pAffiche(stor.zoneExpli5, '', textes.correction4_4)
        }
      }
    }
  }

  function ajouteFois (expression) {
    const string = String(expression)
    let chaine = string
    if (string.indexOf('t') !== -1 && string.indexOf('t') !== 0) { // présent et pas en première place
      if (string.charAt(string.indexOf('t') - 1) !== '+' && string.charAt(string.indexOf('t') - 1) !== '-') {
        chaine = string.substring(0, string.indexOf('t')) + '*' + string.substring(string.indexOf('t'))
      }
    }
    me.logIfDebug('chaine=' + chaine)
    return chaine
  }

  function genereFraction () {
    const objet = {}
    const tab = ['2/3', '1/3', '1/9', '2/9', '4/3']// pour avoir des résultats avec des périodes décimales à 1 chiffre pour conversion facile...
    const index = j3pGetRandomInt(0, (tab.length - 1))
    objet.ligne = tab[index]
    objet.num = objet.ligne.substring(0, objet.ligne.indexOf('/'))
    objet.den = objet.ligne.substring(objet.ligne.indexOf('/') + 1)
    me.logIfDebug('fraction', objet)
    return objet
  }

  // convertit 1,33333333 en fraction simplifiée
  function conversionFraction (exp) {
    if (String(exp).indexOf('.') === -1) {
      return exp
    }
    const neg = (exp < 0)
    const string = String(Math.abs(exp))
    const entier = string.substring(0, string.indexOf('.'))
    me.logIfDebug('partie entière=' + entier)
    const decimal = string.substring(string.indexOf('.') + 1)
    me.logIfDebug('partie décimale=' + decimal)
    const num1 = decimal.substring(0, 1)// utilisation de la caractérisation de rationnel du type 0,aaaaa qui vaut a/9
    const den1 = 9
    const num2 = Number(entier) * den1 + Number(num1)
    const pg = pgcd(num2, den1)
    let num = num2 / pg
    const den = den1 / pg
    if (neg) {
      num = num * (-1)
    }
    return '\\frac{' + num + '}{' + den + '}'
  }

  function enonceInit () {
    // on vérifie que l’on a pas passé n’importe quoi dans type_question, et on complète s’il en manque
    if (!Array.isArray(ds.type_question)) {
      console.error(Error('paramètre type_question invalide'))
      ds.type_question = []
    }
    ds.type_question = ds.type_question.filter((t) => {
      const ok = ['point', 'vecteur'].includes(t)
      if (!ok) console.error(Error(`type_question ${t} invalide => ignoré`))
      return ok
    })
    if (ds.type_question.length < ds.nbrepetitions) {
      console.error(Error(`paramètre type_question invalide, il manque des éléments (${ds.nbrepetitions} repetitions et ${ds.type_question.length} type_question)`))
      while (ds.type_question.length < ds.nbrepetitions) ds.type_question.push('point')
    }

    me.construitStructurePage('presentation1')
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    stor.corDetail = j3pGetNewId('Cor_details')
    stor.detailFenetre = j3pGetNewId('details_fenetre')
    me.fenetresjq = [
      {
        name: stor.corDetail,
        title: textes.titreFenetre,
        width: 570,
        height: 450,
        left: 0,
        top: 150,
        id: stor.detailFenetre
      }
    ]
    j3pCreeFenetres(me)
  }

  function enonceMain () {
    /**
     * true si type_question vaut point, sinon c’est vecteur
     * @private
     * @type {boolean}
     */
    stor.questionCouranteEstPoint = ds.type_question[me.questionCourante - 1] === 'point'
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    stor.divEnonce = j3pAddElt(stor.conteneur, 'div')
    stor.explications = j3pAddElt(stor.conteneur, 'div', '', { paddingTop: '10px' })
    // définition de la droite de l’espace:
    stor.coord = []
    // stor.coord2=[];
    stor.coordVecteur = []
    // définition du point dont on parle :
    stor.coordA = []

    // Création des DIV
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.divEnonce, 'div')

    // définition des variables :
    // attention, il ne faut pas en avoir deux nuls, pose pb sinon dans le cas où l’on veut générer un point qui ne soit pas sur la droite
    // (en utilisant deux paramètres différents), éventuellement à vérifier si ça pose pas de pb pour les sections précédentes.
    let nbNuls = 2
    while (nbNuls > 1) {
      stor.coord[0] = j3pGetRandomInt(-5, 5)
      stor.coord[1] = j3pGetRandomInt(-5, 5)
      stor.coord[2] = j3pGetRandomInt(-5, 5)

      stor.coordVecteur[0] = j3pGetRandomInt(-5, 5)
      stor.coordVecteur[1] = j3pGetRandomInt(-5, 5)
      stor.coordVecteur[2] = j3pGetRandomInt(-5, 5)
      nbNuls = stor.coordVecteur.filter(n => n === 0).length
    }
    const reponse = { x: '', y: '', z: '' } // on définit le système à afficher
    if (stor.coord[0] !== 0) reponse.x += stor.coord[0]
    if (stor.coord[1] !== 0) reponse.y += stor.coord[1]
    if (stor.coord[2] !== 0) reponse.z += stor.coord[2]
    if (stor.coordVecteur[0] !== 0) {
      if (stor.coordVecteur[0] > 0) {
        if (reponse.x !== '') reponse.x += '+'
        if (!(stor.coordVecteur[0] === 1)) reponse.x += stor.coordVecteur[0]
      } else {
        if (stor.coordVecteur[0] === -1) reponse.x += '-'
        else reponse.x += stor.coordVecteur[0]
      }
      reponse.x += 't'
    }
    if (stor.coordVecteur[1] !== 0) {
      if (stor.coordVecteur[1] > 0) {
        if (reponse.y !== '') reponse.y += '+'
        if (!(stor.coordVecteur[1] === 1)) reponse.y += stor.coordVecteur[1]
      } else {
        if (stor.coordVecteur[1] === -1) reponse.y += '-'
        else reponse.y += stor.coordVecteur[1]
      }
      reponse.y += 't'
    }
    if (stor.coordVecteur[2] !== 0) {
      if (stor.coordVecteur[2] > 0) {
        if (reponse.z !== '') reponse.z += '+'
        if (!(stor.coordVecteur[2] === 1)) reponse.z += stor.coordVecteur[2]
      } else {
        if (stor.coordVecteur[2] === -1) reponse.z += '-'
        else reponse.z += stor.coordVecteur[2]
      }
      reponse.z += 't'
    }
    // cas particuliers :
    if (stor.coord[0] === 0 && stor.coordVecteur[0] === 0) reponse.x = '0'
    if (stor.coord[1] === 0 && stor.coordVecteur[1] === 0) reponse.y = '0'
    if (stor.coord[2] === 0 && stor.coordVecteur[2] === 0) reponse.z = '0'
    stor.lignesSysteme = reponse
    j3pAffiche(stor.zoneCons1, '', textes.consigne1)
    // j3pAfficheSysteme(stor.zoneCons2, '', consigne2, [['$x=' + reponse.x + '$', '$y=' + reponse.y + '$', '$z=' + reponse.z + '$']], {}, me.styles.toutpetit.enonce.color)
    afficheSystemeEquations(stor.zoneCons2, [
      { contenu: `$x=${reponse.x}$` },
      { contenu: `$y=${reponse.y}$` },
      { contenu: `$z=${reponse.z}$` }
    ])
    j3pAffiche(stor.zoneCons2, '', textes.consigne2, { style: { marginLeft: '40px' } })
    stor.paramA = []
    /**
     * True si le point appartient à la droite, ou si le vecteur est directeur
     * @private
     * @type {boolean}
     */
    stor.appartient = (Math.random() < ds.proba_appartient[me.questionCourante - 1])
    let val1, val2, val3, val1Bis, val2Bis, val3Bis

    switch (ds.type_coef[me.questionCourante - 1]) {
      case 'fractionnaire':

        if (stor.appartient) {
          stor.parametre1 = genereFraction()// renvoit une fraction
          stor.solution = stor.parametre1.ligne
        } else {
          stor.parametre1 = genereFraction()// renvoit une fraction
          do {
            stor.parametre2 = genereFraction()
          } while (stor.parametre2.ligne === stor.parametre1.ligne)
          me.logIfDebug('param1', stor.parametre1, 'param2', stor.parametre2)
        }
        // dans le cas du point il faut maintenant calculer ses coordonnées
        if (stor.questionCouranteEstPoint) {
          const str1 = ajouteFois(reponse.x)
          const str2 = ajouteFois(reponse.y)
          const str3 = ajouteFois(reponse.z)
          const exp1 = (str1.indexOf('t') !== -1) ? j3pRemplace(str1, str1.indexOf('t'), str1.indexOf('t') + 1, 'x/y') : str1
          me.logIfDebug('exp1=' + exp1)
          const exp2 = (str2.indexOf('t') !== -1) ? j3pRemplace(str2, str2.indexOf('t'), str2.indexOf('t') + 1, 'x/y') : str2
          me.logIfDebug('exp2=' + exp2)
          const exp3 = (str3.indexOf('t') !== -1) ? j3pRemplace(str3, str3.indexOf('t'), str3.indexOf('t') + 1, 'x/y') : str3
          me.logIfDebug('exp3=' + exp3)
          if (stor.appartient) { // ce sera bon
            stor.coordA[0] = conversionFraction(j3pEvalueExpression({
              exp: exp1,
              inc: ['x', 'y']
            }, [stor.parametre1.num, stor.parametre1.den]))
            stor.coordA[1] = conversionFraction(j3pEvalueExpression({
              exp: exp2,
              inc: ['x', 'y']
            }, [stor.parametre1.num, stor.parametre1.den]))
            stor.coordA[2] = conversionFraction(j3pEvalueExpression({
              exp: exp3,
              inc: ['x', 'y']
            }, [stor.parametre1.num, stor.parametre1.den]))
            // pour la correction détaillée :
            stor.paramA[0] = '\\frac{' + stor.parametre1.num + '}{' + stor.parametre1.den + '}'
            stor.paramA[1] = '\\frac{' + stor.parametre1.num + '}{' + stor.parametre1.den + '}'
            stor.paramA[2] = '\\frac{' + stor.parametre1.num + '}{' + stor.parametre1.den + '}'
          } else { // on prend deux paramètres différents, pas si simple car on peut avoir x=cste par exemple
            const typeChoix = [[1, 1, 2], [1, 2, 1], [2, 1, 1]]
            let leChoix, pb
            do {
              leChoix = typeChoix[j3pGetRandomInt(0, typeChoix.length - 1)]
              // IL faut faire attention que l’endroit où on met le coef différent ne soit pas un cas de figure où l’expression serait constante
              // Cela se produit quand l’expression (qui est une chaine) ne contient pas x (ou y)
              pb = (leChoix[0] === 2 && exp1.indexOf('x') === -1) || (leChoix[1] === 2 && exp2.indexOf('x') === -1) || (leChoix[2] === 2 && exp3.indexOf('x') === -1)
            } while (pb)
            if (leChoix[0] === 1) {
              stor.coordA[0] = conversionFraction(j3pEvalueExpression({
                exp: exp1,
                inc: ['x', 'y']
              }, [stor.parametre1.num, stor.parametre1.den]))
              stor.paramA[0] = '\\frac{' + stor.parametre1.num + '}{' + stor.parametre1.den + '}'
            } else {
              stor.coordA[0] = conversionFraction(j3pEvalueExpression({
                exp: exp1,
                inc: ['x', 'y']
              }, [stor.parametre2.num, stor.parametre2.den]))
              stor.paramA[0] = '\\frac{' + stor.parametre2.num + '}{' + stor.parametre2.den + '}'
            }
            if (leChoix[1] === 1) {
              stor.coordA[1] = conversionFraction(j3pEvalueExpression({
                exp: exp2,
                inc: ['x', 'y']
              }, [stor.parametre1.num, stor.parametre1.den]))
              stor.paramA[1] = '\\frac{' + stor.parametre1.num + '}{' + stor.parametre1.den + '}'
            } else {
              stor.coordA[1] = conversionFraction(j3pEvalueExpression({
                exp: exp2,
                inc: ['x', 'y']
              }, [stor.parametre2.num, stor.parametre2.den]))
              stor.paramA[1] = '\\frac{' + stor.parametre2.num + '}{' + stor.parametre2.den + '}'
            }
            if (leChoix[2] === 1) {
              stor.coordA[2] = conversionFraction(j3pEvalueExpression({
                exp: exp3,
                inc: ['x', 'y']
              }, [stor.parametre1.num, stor.parametre1.den]))
              stor.paramA[2] = '\\frac{' + stor.parametre1.num + '}{' + stor.parametre1.den + '}'
            } else {
              stor.coordA[2] = conversionFraction(j3pEvalueExpression({
                exp: exp3,
                inc: ['x', 'y']
              }, [stor.parametre2.num, stor.parametre2.den]))
              stor.paramA[2] = '\\frac{' + stor.parametre2.num + '}{' + stor.parametre2.den + '}'
            }
          }
        } else {
          // pour le vecteur, on mutiplie simplement les coordonnées par notre fraction pour avoir les nouvelles coordonnées
          if (stor.appartient) { // ce sera bon
            stor.coordA[0] = conversionFraction(j3pEvalue(stor.coordVecteur[0] + '*' + stor.parametre1.ligne))
            stor.coordA[1] = conversionFraction(j3pEvalue(stor.coordVecteur[1] + '*' + stor.parametre1.ligne))
            stor.coordA[2] = conversionFraction(j3pEvalue(stor.coordVecteur[2] + '*' + stor.parametre1.ligne))
          } else {
            // on fait attention car en cas de valeur nulle on peut avoir un pb, on vérifie donc que le triplet n’est pas correct
            val1 = conversionFraction(j3pEvalue(stor.coordVecteur[0] + '*' + stor.parametre1.ligne))
            val2 = conversionFraction(j3pEvalue(stor.coordVecteur[1] + '*' + stor.parametre1.ligne))
            val3 = conversionFraction(j3pEvalue(stor.coordVecteur[2] + '*' + stor.parametre1.ligne))
            val1Bis = conversionFraction(j3pEvalue(stor.coordVecteur[0] + '*' + stor.parametre2.ligne))
            val2Bis = conversionFraction(j3pEvalue(stor.coordVecteur[1] + '*' + stor.parametre2.ligne))
            val3Bis = conversionFraction(j3pEvalue(stor.coordVecteur[2] + '*' + stor.parametre2.ligne))
            me.logIfDebug('valeurs:', val1, val2, val3, val1Bis, val2Bis, val3Bis)
            const tabVal = [val1, val2, val3]
            const tabValBis = [val1Bis, val2Bis, val3Bis]
            while (true) { // on prend au hasard une des deux valeurs et on vérifie à la fin que l’on a pas de triplet identique
              const melangeTab = j3pShuffle([true, true, false])
              for (let i = 0; i < 3; i++) {
                stor.coordA[i] = (melangeTab[i]) ? tabVal[i] : tabValBis[i]
              }
              // Ne sachant pas comment sont calculées val et valBis, je relance la machine si je tombe sur un vecteur qui est vraiment colinéaire au vrai vecteur directeur
              // Cela ne devrait pas être nécessaire, mais sait-on jamais
              // Quoique, c’est sans doute possible si l’une des coordonnées est nulle donc je laisse comme ça
              if (!((stor.coordA[0] === val1) && (stor.coordA[1] === val2) && (stor.coordA[2] === val3)) || ((stor.coordA[0] === val1Bis) && (stor.coordA[1] === val2Bis) && (stor.coordA[2] === val3Bis))) {
                break
              }
            }
          }
        }

        break

      default :// param de_tete
        {
          let choix, choix2
          if (stor.questionCouranteEstPoint) {
            do {
              choix = j3pGetRandomInt(-1, 2)
            } while (Math.abs(choix) < Math.pow(10, -11))
            stor.solution = choix
            if (stor.appartient) {
              stor.coordA[0] = j3pEvalueExpression({ exp: String(reponse.x), inc: ['t'] }, [choix])
              stor.coordA[1] = j3pEvalueExpression({ exp: String(reponse.y), inc: ['t'] }, [choix])
              stor.coordA[2] = j3pEvalueExpression({ exp: String(reponse.z), inc: ['t'] }, [choix])
              // pour la correction détaillée :
              stor.paramA[0] = choix
              stor.paramA[1] = choix
              stor.paramA[2] = choix
            } else {
              let pb
              do {
                pb = false
                do {
                  choix2 = j3pGetRandomInt(-1, 2)
                } while (choix2 === choix)
                stor.paramA = j3pShuffle([choix, choix, choix2])
                const expressions = [String(reponse.x), String(reponse.y), String(reponse.z)]
                for (let i = 0; i < 3; i++) {
                  stor.coordA[i] = j3pEvalueExpression({ exp: expressions[i], inc: ['t'] }, [stor.paramA[i]])
                  if (stor.paramA[i] === choix2) {
                    pb = Math.abs(stor.coordA[i] - j3pEvalueExpression({
                      exp: expressions[i],
                      inc: ['t']
                    }, [choix])) < Math.pow(10, -11)
                  }
                }
              } while (pb)
            }
          } else { // vecteur
            choix = (ds.type_coef[me.questionCourante - 1] === 'entier')
              ? (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 4)
              : 2 * j3pGetRandomInt(0, 1) - 1
            if (stor.appartient) {
              stor.coordA[0] = choix * stor.coordVecteur[0]
              stor.coordA[1] = choix * stor.coordVecteur[1]
              stor.coordA[2] = choix * stor.coordVecteur[2]
              stor.choix = choix
            } else {
              if (ds.type_coef[me.questionCourante - 1] === 'entier') {
                do {
                  choix2 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 4)
                } while (choix2 === choix)
              } else {
                choix2 = -choix
              }
              val1 = choix * stor.coordVecteur[0]
              val2 = choix * stor.coordVecteur[1]
              val3 = choix * stor.coordVecteur[2]
              val1Bis = choix2 * stor.coordVecteur[0]
              val2Bis = choix2 * stor.coordVecteur[1]
              val3Bis = choix2 * stor.coordVecteur[2]
              do { // on prend au hasard une des deux valeurs et on vérifie à la fin que l’on a pas de triplet identique
                stor.coordA[0] = j3pGetRandomBool() ? val1 : val1Bis
                stor.coordA[1] = j3pGetRandomBool() ? val2 : val2Bis
                stor.coordA[2] = j3pGetRandomBool() ? val3 : val3Bis
              } while (((stor.coordA[0] === val1) && (stor.coordA[1] === val2) && (stor.coordA[2] === val3)) || ((stor.coordA[0] === val1Bis) && (stor.coordA[1] === val2Bis) && (stor.coordA[2] === val3Bis)))
            }
          }
        }
        break
    }
    j3pAffiche(stor.zoneCons3, '', (stor.questionCouranteEstPoint) ? textes.consignePoint : textes.consigneVecteur, {
      a: stor.coordA[0],
      b: stor.coordA[1],
      c: stor.coordA[2]
    })
    // j’affiche le QCM :
    const form = j3pAddElt(stor.zoneCons3, 'form', '', { style: { textAlign: 'right', width: '10rem' } })
    const nameRadio = j3pGetNewId('choix')
    stor.btnOui = j3pBoutonRadio(form, '', nameRadio, 'oui', 'oui')
    j3pAddElt(form, 'br')
    stor.btnNon = j3pBoutonRadio(form, '', nameRadio, 'nom', 'non')
    // ajoute les écouteurs aux cases à cocher pour afficher la zone de saisie du param t (point only)
    if (stor.questionCouranteEstPoint) {
      stor.btnOui.addEventListener('click', afficheOnClickOui)
      stor.btnNon.addEventListener('click', afficheOnClickNon)
    }
    // dans le cas où on attend un paramètre on peut donner une chance de plus
    if (stor.appartient && ds.chance_sup && stor.questionCouranteEstPoint) {
      ds.nbchances++
    }
    stor.divCorrection = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '20px' }) })

    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":
    }

    case 'correction': {
      const { qcmOk, paramOk } = evalueReponse()
      const paramNeeded = (stor.btnOui.checked && stor.questionCouranteEstPoint) // si true il a répondu oui et c'était le cas point => faut saisir un param
      const inputMq = stor.elts?.inputmqList?.[0]
      if (paramNeeded && !inputMq) {
        return j3pShowError(Error('Aucune zone de saisie trouvée, impossible d’évaluer la réponse'), { notifyData: { parcours: this } })
      }
      j3pEmpty(stor.divCorrection)
      // On teste si une réponse a été saisie
      const aReponduQcm = stor.btnOui.checked || stor.btnNon.checked
      let repEleve = ''
      if (inputMq) repEleve = j3pValeurde(inputMq)
      if (!me.isElapsed && (!aReponduQcm || (paramNeeded && !repEleve))) {
        let msgReponseManquante
        if (!aReponduQcm) msgReponseManquante = textes.qcm_incomplet
        if (paramNeeded && !repEleve) j3pFocus(inputMq)
        me.reponseManquante(stor.divCorrection, msgReponseManquante)
        return me.finCorrection()
      }

      // Bonne réponse
      if (qcmOk && (paramOk || !paramNeeded)) {
        // Bonne réponse, on désactive les cases à cocher :
        stor.btnOui.disabled = true
        stor.btnNon.disabled = true
        if (inputMq) {
          j3pEmpty(stor.laPalette)
          inputMq.style.color = me.styles.cbien
          j3pDesactive(inputMq)
        }
        me.score++
        stor.divCorrection.style.color = me.styles.cbien
        j3pAddContent(stor.divCorrection, cBien + '\n' + textes.details)
        correctionDetaillee(true)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // réponse KO
      stor.divCorrection.style.color = me.styles.cfaux

      if (me.isElapsed) {
        // A cause de la limite de temps :
        // on désactive les cases à cocher :
        stor.btnOui.disabled = true
        stor.btnNon.disabled = true
        j3pAddContent(stor.divCorrection, tempsDepasse, { replace: true })
        me.typederreurs[10]++
        correctionDetaillee(false)
        if (inputMq) {
          inputMq.style.color = me.styles.cfaux
          j3pBarre(inputMq)
        }
        return me.finCorrection('navigation', true)
      }

      // KO sans limite de temps
      j3pAddContent(stor.divCorrection, cFaux, { replace: true })
      if (qcmOk) j3pAddContent(stor.divCorrection, textes.qcmbon)

      // erreur mais il reste un essai
      if (me.essaiCourant < ds.nbchances) {
        j3pAddContent(stor.divCorrection, '<br>' + essaieEncore)
        me.typederreurs[1]++
        if (inputMq) {
          inputMq.style.color = me.styles.cfaux
          j3pFocus(inputMq)
        }
        return me.finCorrection() // on reste dans l’état correction, pas besoin de rappeler la section
      }

      // Erreur au dernier essai
      stor.btnOui.disabled = true
      stor.btnNon.disabled = true
      if (inputMq) {
        j3pEmpty(stor.laPalette)
        j3pDesactive(inputMq)
        inputMq.style.color = me.styles.cfaux
        j3pBarre(inputMq)
      }
      j3pAddContent(stor.divCorrection, '<br>' + regardeCorrection)
      correctionDetaillee(false)
      me.typederreurs[2]++
      me.etat = 'navigation'
      me.finCorrection('navigation', true)
      break
    }

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break
  }
}
