import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pChaine, j3pDetruit, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pImporteAnnexe, j3pMathquillXcas, j3pPaletteMathquill, j3pShowError, j3pShuffle, j3pStyle } from 'src/legacy/core/functions'
import loadWebXcas from 'src/legacy/outils/xcas/loadWebXcas'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// la fct d’évaluation des expressions par webXcas
let xcas

/*
 *
 *Section squelette pour le calcul de dérivées
 *
 *Alexis Lecomte septembre 2013
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['f', 'derivees1', 'string', 'Fichier js des variables de l’exercice (dans sectionsAnnexes/squelettes-derivation/ sans extension) ']
  ]
}

/**
 * section squelettederivation
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function egaliteFormelle (expression1, expression2) {
    return xcas('simplify(normal(' + expression1 + '-(' + expression2 + ')))') === '0'
  }

  function parentheseFermante (chaine, index) {
    // fonction qui donne la position de l’accolade fermante correspondant à l’accolade ouvrante positionnée en index de la chaine
    // penser à une condition d’arrêt pour ne pas faire planter le truc si la saisie est mal écrite
    // (ptet au debut de conversion mathquill : même nb d’accolades ouvrantes que fermantes)
    // pour l’instant 1 accolade ouvrante (à l’appel de la fonction)
    let indexaccolade = 1
    let indexvariable = index
    while (indexaccolade !== 0) {
      // je peux avoir des accolades internes (par groupe de 2 necessairement)
      // for (var indexvariable=index+1; indexvariable<chaine.length; indexvariable++){
      indexvariable++
      const char = chaine.charAt(indexvariable)
      if (char === '(') indexaccolade++
      else if (char === ')') indexaccolade--
    }
    return indexvariable
  }

  function afficheCorrection (bonneReponse) {
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    const chaine = me.stockage[20][me.questionCourante - 1].correction_detaillee
    j3pAffiche(zoneExpli, '', chaine, {
      a: me.stockage[20][me.questionCourante - 1].a,
      b: me.stockage[20][me.questionCourante - 1].b,
      c: me.stockage[20][me.questionCourante - 1].c,
      z: me.stockage[3],
      z2: stor.deriveeFactExpoLatex
    })
    j3pDetruit(stor.zoneCons4) // la palette
  }
  function signeFacteur (nombre) {
    if (nombre === 1) {
      return ''
    } else {
      if (nombre === -1) {
        return '-'
      } else {
        return nombre
      }
    }
  }
  // fonction qui renvoit un polynome au format latex de degré n
  function generePolynome (n) {
    let lePol = ''
    // premier facteur :
    let alea = j3pGetRandomInt(0, 1)
    if (alea === 1) {
      lePol += '-'
    }
    lePol += signeFacteur(j3pGetRandomInt(1, 15))
    for (let i = Number(n); i > 0; i--) {
      if (i === 1) {
        lePol += 'x'
      } else {
        lePol += 'x^{' + i + '}'
      }
      alea = j3pGetRandomInt(0, 1)
      if (alea === 1) {
        lePol += '+'
      } else {
        lePol += '-'
      }
      lePol += signeFacteur(j3pGetRandomInt(2, 15))
    }
    return lePol
  }
  function chargementDesDonnees (fichier) {
    function extractionDonnees (tableau, unobj, tabfinal) {
      unobj.expression = tableau[0].substring(12, tableau[0].length)
      // cas spécial si polynome
      let decalage
      let coeff
      while (unobj.expression.indexOf('polynome') !== -1) {
        // une seule lettre pour la variable dans un premier cas désignée par une variable
        if (unobj.expression.charAt(unobj.expression.indexOf('polynome') + 9) === '£') {
          decalage = 12
          const nomVariable = unobj.expression.charAt(unobj.expression.indexOf('polynome') + 10)
          let indice
          for (let i = 5; i < 15; i++) {
            try {
              if (String(nomVariable) === tableau[i].charAt(0)) indice = i
            } catch (e) {
              console.warn(e) // pas normal mais pas grave
            }
          }
          if (indice) {
            const [minInt, maxInt] = j3pGetBornesIntervalle(tableau[indice].substring(3, tableau[indice].length))
            coeff = j3pGetRandomInt(minInt, maxInt)
          } else {
            // garde fou en cas d’oubli de précision
            coeff = j3pGetRandomInt(3, 5)
          }
        } else {
          decalage = 11
          // on a demandé un polynome de degré bien précis
          const index1 = unobj.expression.indexOf('polynome') + 9
          // on détecte la fin de parenthèse
          const index2 = parentheseFermante(unobj.expression, index1)
          coeff = unobj.expression.substring(index1, index2)
        }
        // plutôt remplacer polynôme(£a) par son expression
        unobj.expression = unobj.expression.substring(0, unobj.expression.indexOf('polynome')) + generePolynome(coeff) + unobj.expression.substring(unobj.expression.indexOf('polynome') + decalage)
        me.logIfDebug(unobj.expression)
      }

      unobj.ensemble = tableau[1].substring(10, tableau[1].length)
      unobj.ensemble_derivee = tableau[2].substring(18, tableau[2].length)
      unobj.nom = tableau[3].substring(5, tableau[3].length)
      unobj.correction_detaillee = tableau[4].substring(22, tableau[4].length)
      if (tableau.length > 5 && tableau[5] !== '') {
        const [minInt, maxInt] = j3pGetBornesIntervalle(tableau[5].substring(3, tableau[5].length))
        unobj.a = j3pGetRandomInt(minInt, maxInt)
      }
      if (tableau.length > 6 && tableau[6] !== '') {
        const [minInt, maxInt] = j3pGetBornesIntervalle(tableau[6].substring(3, tableau[6].length))
        unobj.b = j3pGetRandomInt(minInt, maxInt)
      }
      if (tableau.length > 7 && tableau[7] !== '') {
        const [minInt, maxInt] = j3pGetBornesIntervalle(tableau[7].substring(3, tableau[7].length))
        unobj.c = j3pGetRandomInt(minInt, maxInt)
      }
      unobj.expression = j3pChaine(unobj.expression, unobj)
      me.logIfDebug(' unobj.expression=' + unobj.expression,
        '\n unobj.ensemble=' + unobj.ensemble,
        '\n unobj.ensemble_derivee=' + unobj.ensemble_derivee,
        '\n unobj.nom=' + unobj.nom,
        '\n unobj.correction_detaillee=' + unobj.correction_detaillee,
        '\n unobj.a=' + unobj.a,
        '\n unobj.b=' + unobj.b,
        '\n unobj.c=' + unobj.c
      )
      tabfinal.push(unobj)
    }
    // tableau d’objets {expression:"",ensemble:"",ensemble_derivee:"",nom:"",correction_detaillee:""}
    const tabgeneral = []
    const nbitems = Number(fichier.substring(0, fichier.indexOf('%')).substring(0, fichier.indexOf('dérivées')))

    fichier = fichier.substring(fichier.indexOf('%') + 1)
    // tableau de chaines contenant les différents items
    const letab = []
    let decal
    let ssch
    for (let k = 0; k < nbitems; k++) {
      if (k <= 8) decal = 1
      else decal = 2
      if (k < nbitems - 1) { ssch = fichier.substring(fichier.indexOf('dérivée' + (k + 1)) + 8 + decal, fichier.indexOf('dérivée' + (k + 2))) } else { ssch = fichier.substring(fichier.indexOf('dérivée' + (k + 1)) + 8 + decal) }
      letab.push(ssch)
    }
    let p
    let unobjet
    let tab
    for (p = 0; p < nbitems; p++) {
      unobjet = { expression: '', ensemble: '', ensemble_derivee: '', correction_detaillee: '' }
      tab = letab[p].split('%')
      extractionDonnees(tab, unobjet, tabgeneral)
    }
    // Cas particulier : si nbrepetitions>taille du tableau, on complète le tableau en y ajoutant des éléments déjà chargés...
    if (tabgeneral.length < ds.nbrepetitions) {
      for (p = nbitems; p < ds.nbrepetitions; p++) {
        unobjet = { expression: '', ensemble: '', ensemble_derivee: '', correction_detaillee: '' }
        tab = letab[(p - nbitems) % nbitems].split('%')
        // if (p<ds.nbrepetitions-1) tab.pop();
        extractionDonnees(tab, unobjet, tabgeneral)
      }
    }
    me.logIfDebug('tabgeneral=' + tabgeneral)
    return tabgeneral
  }

  function getDonnees () {
    return {
      typesection: 'lycee',

      nbrepetitions: 3,
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"
      f: 'derivees1',
      textes: {
        titreExo: 'Calculs de dérivées',
        question1: 'On définit la fonction $£w$ sur $£x$ par $£w(x)=£y$.',
        question2: 'Détermine l’expression de la fonction dérivée sur $£z$ :',
        correction: 'Étudie bien la correction :'
      },
      pe: 0
    }
  }
  function suite () {
    me.construitStructurePage(ds.structure)

    // par convention, me.stockage[0] contient la solution
    // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    me.afficheTitre(ds.textes.titreExo)
    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
    suite2()
  }
  function suite2 () {
    //  if ((me.questionCourante%ds.nbetapes) == 1){
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '15px' }) })
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // Description des variables :
    // a :ensemble de définition de la fonction
    // b :expression de la fonction
    // c :ensemble de définition de la fonction dérivée
    // f : nom de la fonction
    me.logIfDebug(
      'a=' + me.stockage[20][me.questionCourante - 1].ensemble,
      '\nb=' + me.stockage[20][me.questionCourante - 1].expression,
      '\nc=' + me.stockage[20][me.questionCourante - 1].ensemble_derivee,
      '\nf=' + me.stockage[20][me.questionCourante - 1].nom
    )

    j3pAffiche(stor.zoneCons1, '', ds.textes.question1,
      {
        x: me.stockage[20][me.questionCourante - 1].ensemble,
        y: me.stockage[20][me.questionCourante - 1].expression,
        w: me.stockage[20][me.questionCourante - 1].nom
      }
    )
    j3pAffiche(stor.zoneCons2, '', ds.textes.question2, { z: me.stockage[20][me.questionCourante - 1].ensemble_derivee })
    me.logIfDebug('a=' + me.stockage[20][me.questionCourante - 1].a,
      '\nb=' + me.stockage[20][me.questionCourante - 1].b,
      '\nc=' + me.stockage[20][me.questionCourante - 1].c
    )

    const eltCons3 = j3pAffiche(stor.zoneCons3, '', '$£f\'(x)=$&1&',
      {
        f: me.stockage[20][me.questionCourante - 1].nom
      }
    )
    stor.zoneInput = eltCons3.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.+-*/x()^', { commandes: ['racine', 'exp', 'fraction', 'puissance'] })
    // la fonction convertie au format xcas pour calculs
    me.stockage[10] = j3pChaine(me.stockage[20][me.questionCourante - 1].expression, me.stockage[20][me.questionCourante - 1])
    me.logIfDebug('me.stockage[10]=' + me.stockage[10])

    const formuleXcas = j3pMathquillXcas(me.stockage[10])
    me.logIfDebug('formuleXcas=' + formuleXcas)
    // la dérivée, au format xcas
    me.storage.solution = xcas('simplify(diff(' + formuleXcas + ',x))')
    me.logIfDebug('la solution=' + me.storage.solution)
    stor.deriveeFactExpoLatex = ''
    if (me.storage.solution.includes('exp(x)')) {
      stor.deriveeFactExpo = xcas('simplify((' + me.storage.solution + ')/exp(x))*exp(x)')
    } else if (me.storage.solution.includes('exp(')) {
      // là il faut que je trouve la puissance de l’expo
      const expo = me.storage.solution.match(/exp\([^)]*\)/)
      stor.deriveeFactExpo = xcas('simplify((' + me.storage.solution + ')/' + expo + ')*' + expo)
    }
    // la solution finale, au format latex, pour l’affichage de la correction détaillée :
    me.stockage[3] = xcas('latex(' + me.storage.solution + ')').replace(/\*/g, '').replace(/\\cdot/g, '')
    stor.deriveeFactExpoLatex = xcas('latex(' + stor.deriveeFactExpo + ')').replace(/\*/g, '').replace(/\\cdot/g, '')
    // la commande latex de xcas renvoit des guillemets en premier et dernier caractère à virer donc:
    me.stockage[3] = me.stockage[3].substring(1, me.stockage[3].length - 1)
    stor.deriveeFactExpoLatex = stor.deriveeFactExpoLatex.substring(1, stor.deriveeFactExpoLatex.length - 1)
    me.logIfDebug('la solution en latex' + me.stockage[3])
    j3pStyle(stor.zoneCons4, { paddingTop: '10px' })
    j3pPaletteMathquill(stor.zoneCons4, stor.zoneInput, { liste: ['racine', 'exp', 'fraction', 'puissance'] })
    j3pFocus(stor.zoneInput)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()

        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        j3pImporteAnnexe('squelettes-derivation/' + ds.f + '.js').then(function (datasSection) {
          me.stockage[0] = datasSection.txt
          me.stockage[20] = chargementDesDonnees(me.stockage[0])
          me.stockage[20] = j3pShuffle(me.stockage[20])
          // chargement webXcas
          return loadWebXcas()
        }).then((_xcas) => {
          xcas = _xcas
          suite()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les prérequis de cet exercice')
        })
      } else {
        // A chaque répétition, on nettoie la scène (ici partiellement car plusieurs étapes avec même consigne
        me.videLesZones()
        // ou dans le cas de presentation3
        suite2()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      $(stor.zoneInput).blur()
      {
      // on récupère on format latex les réponses eleve : repElevePrimitive pour la primitive et repEleve pour le résultat final
        const repEleve = $(stor.zoneInput).mathquill('latex')

        if (!repEleve && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie

          // on convertit au format xcas les réponses élève pour traitement
          const repEleveXcas = j3pMathquillXcas(repEleve)
          const reponseAnalysee = egaliteFormelle(repEleveXcas, me.storage.solution)
          // Bonne réponse
          if (reponseAnalysee) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :

              stor.zoneCorr.innerHTML = cFaux

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                stor.zoneInput.style.color = me.styles.cfaux
                j3pFocus(stor.zoneInput)
              // indication éventuelle ici
              } else {
              // Erreur au nème essai

                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                j3pFreezeElt(stor.zoneInput)
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":
      //

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
