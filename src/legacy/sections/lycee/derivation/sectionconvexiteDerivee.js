import { j3pAddElt, j3pAjouteCaseCoche, j3pBarre, j3pGetNewId, j3pGetRandomInt, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import tableauSignesVariations from 'src/legacy/outils/tableauSignesVariations'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2020
        Lien entre la convexité et le sens de variation d’une fonction
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
    j3pAffiche(stor.zoneExpli1, '', ds.textes['corr1_' + stor.choixConvexite], {
      f: stor.nomf,
      p: (stor.nomf === 'f') ? "\\text{ }'" : "'"
    })
    const laCorr2 = (stor.bonnesCases.length === 1) ? ds.textes['corr2_' + stor.choixConvexite] : ds.textes['corr3_' + stor.choixConvexite]
    const objCorr2 = {}
    objCorr2.f = stor.nomf
    objCorr2.i1 = stor.tabChoix[stor.bonnesCases[0]]
    if (stor.bonnesCases.length === 2) objCorr2.i2 = stor.tabChoix[stor.bonnesCases[1]]
    j3pAffiche(stor.zoneExpli2, '', laCorr2, objCorr2)
    // dans explication2, ce qui correspond à la deuxième question
  }

  function calculDonneesTab () {
    // ValX contiendra les abscisses et valY les ordonnées
    // tabVariations contient la monotonie ('croit' ou 'decroit' sur chaque intervalle)
    const valX = []
    const valY = []
    let tabVariations
    const casFigure = j3pGetRandomInt(1, 9)
    if (casFigure === 1) {
      // 2 intervalles
      valX.push((j3pGetRandomInt(0, 1) === 0) ? '-inf' : -j3pGetRandomInt(3, 10))
      valX.push((valX[0] === '-inf') ? j3pGetRandomInt(-5, 4) : valX[0] + j3pGetRandomInt(3, 7))
      valX.push((j3pGetRandomInt(0, 1) === 0) ? '+inf' : valX[1] + j3pGetRandomInt(3, 7))
      if (j3pGetRandomInt(0, 1) === 1) {
        valY.push((valX[0] === '-inf') ? '' : j3pGetRandomInt(3, 10))
        valY.push((valY[0] === '') ? j3pGetRandomInt(-10, 2) : valY[0] - j3pGetRandomInt(3, 10))
        valY.push((valX[2] === '+inf') ? '' : valY[1] + j3pGetRandomInt(3, 12))
        tabVariations = ['decroit', 'croit']
      } else {
        valY.push((valX[0] === '-inf') ? '' : j3pGetRandomInt(3, 10))
        valY.push((valY[0] === '') ? j3pGetRandomInt(-2, 10) : valY[0] + j3pGetRandomInt(3, 10))
        valY.push((valX[3] === '+inf') ? '' : valY[1] - j3pGetRandomInt(3, 12))
        tabVariations = ['croit', 'decroit']
      }
    } else if (casFigure <= 5) {
      // 3 intervalles
      valX.push((j3pGetRandomInt(0, 1) === 0) ? '-inf' : -j3pGetRandomInt(3, 10))
      valX.push((valX[0] === '-inf') ? j3pGetRandomInt(-5, 4) : valX[0] + j3pGetRandomInt(3, 7))
      valX.push(valX[1] + j3pGetRandomInt(3, 7))
      valX.push((j3pGetRandomInt(0, 1) === 0) ? '+inf' : valX[2] + j3pGetRandomInt(3, 7))
      if (casFigure <= 3) {
        valY.push((valX[0] === '-inf') ? '' : j3pGetRandomInt(3, 10))
        valY.push((valY[0] === '') ? j3pGetRandomInt(-10, 2) : valY[0] - j3pGetRandomInt(3, 10))
        valY.push(valY[1] + j3pGetRandomInt(4, 10))
        valY.push((valX[3] === '+inf') ? '' : valY[2] - j3pGetRandomInt(3, 12))
        tabVariations = ['decroit', 'croit', 'decroit']
      } else {
        valY.push((valX[0] === '-inf') ? '' : j3pGetRandomInt(3, 10))
        valY.push((valY[0] === '') ? j3pGetRandomInt(-2, 10) : valY[0] + j3pGetRandomInt(3, 10))
        valY.push(valY[1] - j3pGetRandomInt(4, 10))
        valY.push((valX[3] === '+inf') ? '' : valY[2] + j3pGetRandomInt(3, 12))
        tabVariations = ['croit', 'decroit', 'croit']
      }
    } else {
      // 4 intervalles
      valX.push((j3pGetRandomInt(0, 1) === 0) ? '-inf' : -j3pGetRandomInt(3, 10))
      valX.push((valX[0] === '-inf') ? j3pGetRandomInt(-5, 4) : valX[0] + j3pGetRandomInt(3, 7))
      valX.push(valX[1] + j3pGetRandomInt(3, 7))
      valX.push(valX[2] + j3pGetRandomInt(3, 7))
      valX.push((j3pGetRandomInt(0, 1) === 0) ? '+inf' : valX[3] + j3pGetRandomInt(3, 7))
      if (casFigure <= 7) {
        valY.push((valX[0] === '-inf') ? '' : j3pGetRandomInt(3, 10))
        valY.push((valY[0] === '') ? j3pGetRandomInt(-10, 2) : valY[0] - j3pGetRandomInt(3, 10))
        valY.push(valY[1] + j3pGetRandomInt(4, 10))
        valY.push(valY[2] - j3pGetRandomInt(4, 10))
        valY.push((valX[4] === '+inf') ? '' : valY[3] + j3pGetRandomInt(3, 12))
        tabVariations = ['decroit', 'croit', 'decroit', 'croit']
      } else {
        valY.push((valX[0] === '-inf') ? '' : j3pGetRandomInt(3, 10))
        valY.push((valY[0] === '') ? j3pGetRandomInt(2, 10) : valY[0] + j3pGetRandomInt(3, 10))
        valY.push(valY[1] - j3pGetRandomInt(4, 10))
        valY.push(valY[2] + j3pGetRandomInt(4, 10))
        valY.push((valX[4] === '+inf') ? '' : valY[3] - j3pGetRandomInt(3, 12))
        tabVariations = ['croit', 'decroit', 'croit', 'decroit']
      }
    }
    return {
      valX,
      valY,
      tabVariations
    }
  }

  function genereTableau (div, objDonnees) {
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ////////////////Construction de l’objet pour le tableau de variations
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    // div est la div dans laquelle on construit le tableau
    // objDonnees contient les données à mettre dans le tableau (valeurs et variations)
    const objetGlobal = {}
    // Tout ce qui concerne la mise en forme :
    objetGlobal.mef = {}
    // la largeur
    const largMin = (objDonnees.tabVariations.length >= 4) ? 800 : 600
    objetGlobal.mef.L = Math.max(85 * me.zonesElts.HG.offsetWidth / 100, largMin)
    // la hauteur de la ligne des x
    objetGlobal.mef.h = 50
    // la couleur
    objetGlobal.mef.macouleur = me.styles.toutpetit.enonce.color
    // le tableau principal, qui contiendra les infos de chaque ligne, il y aura donc autant de lignes (en plus de celle des x)
    objetGlobal.lignes = []
    // la première et unique ligne :
    objetGlobal.lignes[0] = {}
    // ligne de signes ou de variations ?
    objetGlobal.lignes[0].type = 'variations'
    // sa hauteur :
    objetGlobal.lignes[0].hauteur = 110
    objetGlobal.lignes[0].valeurs_charnieres = {}
    // j’ai  besoin des valeurs approchées pour les ordonner facilement... (souvent ce sont les mêmes car valeurs entières mais si ce sont des expressions mathquill...)
    objetGlobal.lignes[0].valeurs_charnieres.valeurs_approchees = []
    // Variations : (on peut mettre const)
    objetGlobal.lignes[0].variations = []
    // on peut ajouter des valeurs de x avec leurs images.
    objetGlobal.lignes[0].images = {}
    objetGlobal.lignes[0].imagesapprochees = []
    objetGlobal.lignes[0].expression = (stor.nomf === 'f') ? stor.nomf + "\\text{ }'" : stor.nomf + "'"
    // j’ai mis 0 comme valeur charnière centrale, mais c’est arbitraire ici, ça n’impacte en rien la suite
    // A adapter
    const valXChaine = []
    const valYChaine = []
    for (let i = 0; i < objDonnees.valX.length; i++) {
      valXChaine.push(String(objDonnees.valX[i]))
      valYChaine.push(String(objDonnees.valY[i]))
    }
    objetGlobal.lignes[0].valeurs_charnieres.valeurs_theoriques = [...valXChaine]
    // A adapter
    objetGlobal.lignes[0].valeurs_charnieres.valeurs_affichees = [...valXChaine]
    // on peut laisser...
    objetGlobal.lignes[0].valeurs_charnieres.valeurs_approchees = [...objDonnees.valX]
    // A adapter
    objetGlobal.lignes[0].variations = objDonnees.tabVariations
    objetGlobal.lignes[0].cliquable = [false, false]
    // les intervalles décrivant les variations de la fonction
    objetGlobal.lignes[0].intervalles = []
    for (let i = 1; i < valXChaine.length; i++) {
      objetGlobal.lignes[0].intervalles.push([valXChaine[i - 1], valXChaine[i]])
    }
    // A adapter
    objetGlobal.lignes[0].images = []
    for (let i = 0; i < valXChaine.length; i++) {
      objetGlobal.lignes[0].images.push([valXChaine[i], valYChaine[i]])
    }

    objetGlobal.lignes[0].imagesapprochees = [...objDonnees.valX]
    // on n’autorise pas l’ajout de lignes et d’intervalles
    objetGlobal.ajout_lignes_possible = false
    objetGlobal.ajout_intervalles_possible = false
    const idDivTab = j3pGetNewId('divTableau')
    tableauSignesVariations(div, idDivTab, objetGlobal, false)
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.
      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Lien entre convexité d’une fonction et sa dérivée',
        // on donne les phrases de la consigne
        consigne1: 'Soit $£f$ une fonction deux fois dérivable sur $£e$ dont on donne le tableau de variations de la dérivée $£f£p$ ci-dessous&nbsp;',
        consigne2_1: 'Parmi les intervalles ci-contre, préciser tous ceux sur lesquels la fonction $£f$ est convexe.',
        consigne2_2: 'Parmi les intervalles ci-contre, préciser tous ceux sur lesquels la fonction $£f$ est concave.',
        et: ' et ',
        rmq: 'Il ne peut pas y en avoir autant&nbsp;!',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Il y a une ou plusieurs erreur(s) - mauvaise(s) réponse(s) ou oubli(s)&nbsp;- mais tu peux te corriger&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'La fonction $£f$ est convexe lorsque sa dérivée $£f£p$ est croissante.',
        corr1_2: 'La fonction $£f$ est concave lorsque sa dérivée $£f£p$ est décroissante.',
        corr2_1: 'Donc $£f$ est convexe sur $£{i1}$.',
        corr2_2: 'Donc $£f$ est concave sur $£{i1}$.',
        corr3_1: 'Donc $£f$ est convexe sur $£{i1}$ et sur $£{i2}$.',
        corr3_2: 'Donc $£f$ est concave sur $£{i1}$ et sur $£{i2}$.'
      },
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    me.construitStructurePage({ structure: ds.structure })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    stor.numEssai = 1
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    stor.choixConvexite = j3pGetRandomInt(1, 2)
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })

    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    const tabnomF = ['f', 'g', 'h']
    stor.nomf = tabnomF[j3pGetRandomInt(0, 2)]
    stor.objDonnees = calculDonneesTab()
    stor.domaine = (stor.objDonnees.valX[0] === '-inf')
      ? (stor.objDonnees.valX[stor.objDonnees.valX.length - 1] === '+inf') ? '\\R' : ']-\\infty;' + stor.objDonnees.valX[stor.objDonnees.valX.length - 1] + ']'
      : (stor.objDonnees.valX[stor.objDonnees.valX.length - 1] === '+inf') ? '[' + stor.objDonnees.valX[0] + ';+\\infty[' : '[' + stor.objDonnees.valX[0] + ';' + stor.objDonnees.valX[stor.objDonnees.valX.length - 1] + ']'

    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1,
      {
        f: stor.nomf,
        p: (stor.nomf === 'f') ? "\\text{ }'" : "'",
        e: stor.domaine
      })
    genereTableau(stor.zoneCons2, stor.objDonnees)
    j3pAffiche(stor.zoneCons3, '', ds.textes['consigne2_' + stor.choixConvexite], { f: stor.nomf })
    stor.tabChoix = []
    let ecart = 0
    stor.bonnesCases = []
    while (ecart < stor.objDonnees.tabVariations.length) {
      for (let i = 0; i < stor.objDonnees.tabVariations.length - ecart; i++) {
        // pas de pb d’indice car stor.objDonnees.valX a une valeur de plus que stor.objDonnees.tabVariations
        if (stor.objDonnees.valX[i] === '-inf') {
          stor.tabChoix.push((stor.objDonnees.valX[i + 1 + ecart] === '+inf') ? ']-\\infty;+\\infty[' : ']-\\infty;' + stor.objDonnees.valX[i + 1 + ecart] + ']')
        } else if (stor.objDonnees.valX[i + 1 + ecart] === '+inf') {
          stor.tabChoix.push('[' + stor.objDonnees.valX[i] + ';+\\infty[')
        } else {
          stor.tabChoix.push('[' + stor.objDonnees.valX[i] + ';' + stor.objDonnees.valX[i + 1 + ecart] + ']')
        }
        if (ecart === 0) {
          if (stor.choixConvexite === 1 && stor.objDonnees.tabVariations[i] === 'croit') stor.bonnesCases.push(i) // cas convexe
          if (stor.choixConvexite === 2 && stor.objDonnees.tabVariations[i] === 'decroit') stor.bonnesCases.push(i) // cas concave
        }
      }
      ecart++
    }
    stor.nomInput = []
    // ici je vais positionner les propositions car j’ai besoin de place et sinon, ça descend trop bas
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    stor.zoneConsD1 = j3pAddElt(stor.conteneurD, 'div')
    for (let i = 1; i <= stor.tabChoix.length; i++) {
      const divD = j3pAddElt(stor.zoneConsD1, 'div')
      stor.nomInput.push(j3pAjouteCaseCoche(divD, { label: '$' + stor.tabChoix[i - 1] + '$' }))
    }
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = { aRepondu: false, bonneReponse: true }
        const reponseEleve = []
        for (let k = 0; k < stor.tabChoix.length; k++) {
          reponseEleve.push(stor.nomInput[k].checked)
          if (reponseEleve[k]) reponse.aRepondu = true
        }
        for (let k = 0; k < stor.tabChoix.length; k++) {
          reponse.bonneReponse = reponse.bonneReponse && ((stor.bonnesCases.includes(k)) ? reponseEleve[k] : !reponseEleve[k])
        }
        if (!reponse.bonneReponse && (stor.numEssai === ds.nbchances)) {
        // Quand je suis à la dernière possibilité et qu’il a faux, je barre les mauvaises réponses et les mets en rouge
          for (let k = 0; k < stor.tabChoix.length; k++) {
            if ((!stor.bonnesCases.includes(k)) && reponseEleve[k]) {
              stor.nomInput[k].label.style.color = this.styles.cfaux
              j3pBarre(stor.nomInput[k].label)
            }
            if (stor.bonnesCases.includes(k)) {
            // On la met dans la couleur de la correction
              stor.nomInput[k].label.style.color = this.styles.toutpetit.correction.color
            }
          }
        }
        if (stor.numEssai === ds.nbchances || reponse.bonneReponse) {
          for (let k = 0; k < stor.tabChoix.length; k++) j3pDesactive(stor.nomInput[k])
        }

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          this.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            for (let k = 0; k < stor.tabChoix.length; k++) {
              if (stor.bonnesCases.includes(k)) {
              // On la met dans la couleur de la correction
                stor.nomInput[k].label.style.color = this.styles.cbien
              }
            }
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              stor.numEssai++
              if (stor.numEssai <= ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
// console.log("fin du code : ",me.etat)
}
