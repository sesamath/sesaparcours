import { j3pAddContent, j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pPaletteMathquill, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import $ from 'jquery'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since juin 2022
 * @fileOverview Cette section permet de calculer une dérivée seconde et d’en déduire la convexité, puis d’en déduire une inégalité (position de la courbe par rapport à la tangente ou une sécante)
 */
export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre de tentatives pour la première question de chaque répétition.'],
    ['baremeQ1', [0.4, 0.4, 0.2], 'array', 'Partage du point de la première question de chaque étape (la somme doit valoir 1).']
  ]
}

/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {
    // Nombre de répétitions de l’exercice
    nbrepetitions: 3,
    indication: '',
    nbetapes: 2,
    nbchances: 2,
    baremeQ1: [0.4, 0.4, 0.2],
    textes: {
      titreExo: 'Convexité et inégalités',
      consigne1: 'Soit $£f$ la fonction définie sur $\\R$ par $£f(x)=£{fx}$.',
      consigne2: 'Cette fonction est deux fois dérivable sur $\\R$. Pour tout réel $x$,',
      consigne3: 'Cette fonction est alors #1# sur $\\R$.',
      convexite: 'convexe|concave',
      consigne4_1: 'On a montré que la fonction $£f$ est convexe sur $\\R$.',
      consigne4_2: 'On a montré que la fonction $£f$ est concave sur $\\R$.',
      consigne5_1: 'On en déduit que pour tous réels $x$ et $y$ de $\\R$, $£f\\left(\\frac{x+y}{2}\\right)$ #1# $\\frac{£f(x)+£f(y)}{2}$.',
      consigne5_2: 'L’équation réduite de la tangente à la courbe représentant $£f$ au point d’abscisse $£x$ est $£e$.',
      consigne6: 'Pour tout réel $x$, $£f(x)$ #1# $£t$.',
      comment1: 'Les expressions de la dérivée et de la dérivée seconde doivent être simplifiées.',
      comment2: 'Tu as une nouvelle tentative pour corriger.',
      comment3: 'La dernière réponse n’est alors pas prise en compte.',
      corr0: 'Rappel : $\\left(\\mathrm{e}^u\\right)^{\'}=u\'\\mathrm{e}^{u}$.',
      corr1: 'Pour tout réel $x$, $£f\'(x)=£{fprime}$ et $£f\'\'(x)=£{fseconde}$.',
      corr2_1: 'Comme $£f\'\'(x)> 0$ sur $\\R$, on conclut que la fonction $£f$ est convexe sur $\\R$.',
      corr2_2: 'Comme $£f\'\'(x)< 0$ sur $\\R$, on conclut que la fonction $£f$ est concave sur $\\R$.',
      corr3_1: 'La fonction $£f$ étant convexe sur $\\R$, sur tout intervalle $I$ de $\\R$, sa courbe représentative est en-dessous de ses sécantes sur $I$.',
      corr3_2: 'La fonction $£f$ étant concave sur $\\R$, sur tout intervalle $I$ de $\\R$, sa courbe représentative est au-dessus de ses sécantes sur $I$.',
      corr4: 'Ainsi $£i$.',
      corr5_1: 'La fonction $£f$ étant convexe sur $\\R$, sa courbe représentative est au-dessus de ses tangentes.',
      corr5_2: 'La fonction $£f$ étant concave sur $\\R$, sa courbe représentative est en-dessous de ses tangentes.',
      corr6: 'Ainsi $£j$.'
    }
  }
} // getDonnees

/**
 * Retourne les données pour chaque sujet
 * @private
 * @return {Object}utilise la propriété de concentration d’une variable aléatoire autour de son espérance
 */
function genereAlea (casFigure) {
  const obj = {}
  obj.f = j3pGetRandomElt(['f', 'g', 'h'])
  obj.x = j3pGetRandomElt([1, -1, 2])
  if (casFigure === 1) {
    // Fonction polynôme de degré 2 : ax^2+bx+c
    obj.a = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
    obj.b = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
    obj.c = j3pGetRandomInt(-4, 4)
    obj.fx = j3pMonome(1, 2, obj.a) + j3pMonome(2, 1, obj.b) + j3pMonome(3, 0, obj.c)
    obj.fprime = j3pMonome(1, 1, obj.a * 2) + j3pMonome(2, 0, obj.b)
    obj.fseconde = j3pMonome(1, 0, obj.a * 2)
    // équation de la tangente au point d’abscisse obj.x
    obj.fprimexA = 2 * obj.a * obj.x + obj.b
    const fdexA = obj.a * Math.pow(obj.x, 2) + obj.b * obj.x + obj.c
    obj.t = j3pMonome(1, 1, obj.fprimexA) + j3pMonome(2, 0, fdexA - obj.fprimexA * obj.x)
    obj.e = 'y=' + obj.t
    // pour effectuer les tests d’égalité
    obj.derivee = (2 * obj.a) + '*x+(' + obj.b + ')'
    obj.derivee2nde = String(2 * obj.a)
  } else {
    // Fonction exponentielle a*exp(kx)
    do {
      obj.a = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 3)
    } while (obj.a === 1)
    do {
      obj.k = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 3)
    } while (obj.k === 1)
    obj.fx = j3pMonome(1, 1, obj.a, '\\mathrm{e}^{' + j3pMonome(1, 1, obj.k) + '}')
    obj.fprime = j3pMonome(1, 1, obj.a * obj.k, '\\mathrm{e}^{' + j3pMonome(1, 1, obj.k) + '}')
    obj.fseconde = j3pMonome(1, 1, obj.a * obj.k * obj.k, '\\mathrm{e}^{' + j3pMonome(1, 1, obj.k) + '}')
    // équation de la tangente au point d’abscisse obj.x
    if (Math.abs(obj.k * obj.x - 1) < Math.pow(10, -12)) {
      obj.xfprimexA = j3pMonome(2, 1, -obj.a * obj.k * obj.x + obj.a, '\\mathrm{e}')
      obj.fprimexA = j3pMonome(1, 1, obj.a * obj.k, '\\mathrm{e}')
    } else {
      obj.xfprimexA = j3pMonome(2, 1, -obj.a * obj.k * obj.x + obj.a, '\\mathrm{e}^{' + j3pMonome(1, 0, obj.k * obj.x) + '}')
      obj.fprimexA = j3pMonome(1, 1, obj.a * obj.k, '\\mathrm{e}^{' + j3pMonome(1, 0, obj.k * obj.x) + '}')
    }
    obj.t = obj.fprimexA + 'x' + obj.xfprimexA
    obj.e = 'y=' + obj.t
    // pour effectuer les tests d’égalité
    obj.derivee = (obj.a * obj.k) + '*exp(' + obj.k + '*x)'
    obj.derivee2nde = (obj.a * obj.k * obj.k) + '*exp(' + obj.k + '*x)'
  }
  obj.i = obj.f + '\\left(\\frac{x+y}{2}\\right)' + ((obj.a > 0) ? '\\leq' : '\\geq') + '\\frac{' + obj.f + '(x)+' + obj.f + '(y)}{2}'
  obj.j = obj.f + '(x)' + ((obj.a > 0) ? '\\geq' : '\\leq') + obj.t
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(me.donneesSection.textes.titreExo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
  stor.listeQuest = []
  let typeQuest = [1, 2]
  const typeQuestInit = [1, 2]
  for (let i = 1; i <= me.donneesSection.nbrepetitions; i++) {
    const alea = j3pGetRandomInt(0, typeQuest.length - 1)
    stor.listeQuest.push(typeQuest[alea])
    typeQuest.splice(alea, 1)
    if (typeQuest.length === 0) typeQuest = [...typeQuestInit]
  }
  stor.nbchances = me.donneesSection.nbchances // C’est pour la première partie de la question, il n’y en a qu’une pour la seconde
  // Je vérifie le barème attribué pour la question 1 et remets la valeur par défaut si ça pose pb
  let total = 0
  me.donneesSection.baremeQ1.forEach(point => { total += point })
  if (Math.abs(total - 1) > Math.pow(10, -12)) me.donneesSection.baremeQ1 = [0.4, 0.4, 0.2]
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone puis énoncé
  // chargement mtg
  getMtgCore({ withMathJax: true })
    .then(
      // success
      (mtgAppLecteur) => {
        me.storage.mtgAppLecteur = mtgAppLecteur
        enonceEnd(me)
      },
      // failure
      (error) => {
        j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
      })
    // plantage dans le code de success
    .catch(j3pShowError)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  function ecoute (laZone, lstCommandes) {
    if (laZone.className.includes('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, laZone, { liste: lstCommandes })
    }
  }
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  let nbCons = 6
  if (me.etapeCourante % ds.nbetapes === 1) {
    // Nouvelle fonction
    stor.laQuest = stor.listeQuest[Math.floor(me.questionCourante / ds.nbetapes)]
    stor.objDonnees = genereAlea(stor.laQuest)
  } else {
    stor.choixQ2 = j3pGetRandomBool()
    nbCons = (stor.choixQ2) ? 3 : 4
  }
  // on affiche l’énoncé
  for (let i = 1; i <= nbCons; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
  j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, stor.objDonnees)
  if (me.etapeCourante % ds.nbetapes === 1) {
    stor.numeroEtape = 1
    j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, stor.objDonnees)
    const elt1 = j3pAffiche(stor.zoneCons3, '', '$£f\'(x)=$&1&', { f: stor.objDonnees.f })
    const elt2 = j3pAffiche(stor.zoneCons4, '', '$£f\'\'(x)=$&1&', { f: stor.objDonnees.f })
    // Je place la palette ici
    stor.laPalette = j3pAddElt(stor.zoneCons5, 'div')
    const elt3 = j3pAffiche(stor.zoneCons6, '', ds.textes.consigne3, {
      liste1: { texte: [''].concat(ds.textes.convexite.split('|')) }
    })
    stor.zoneInput = [elt1.inputmqList[0], elt2.inputmqList[0], elt3.selectList[0]]
    const lstCommandes = ['puissance', 'fraction']
    if (stor.laQuest === 2) lstCommandes.push('exp')
    j3pStyle(stor.laPalette, { paddingTop: '10px', paddingBottom: '40px' })
    ;[0, 1].forEach(i => {
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput[i], { liste: lstCommandes })
      stor.zoneInput[i].typeReponse = ['texte']
      mqRestriction(stor.zoneInput[i], '\\d,.+-^x', { commandes: lstCommandes })
      $(stor.zoneInput[i]).focusin(function () {
        ecoute(this, lstCommandes)
      })
    })
    stor.zoneInput[2].reponse = [(stor.objDonnees.a > 0) ? 1 : 2]
    ds.nbchances = stor.nbchances
  } else {
    stor.numeroEtape = 2
    ds.nbchances = 1
    const cons2 = (stor.objDonnees.a > 0) ? ds.textes.consigne4_1 : ds.textes.consigne4_2 // le signe de a donne la convexité de la fonction
    j3pAffiche(stor.zoneCons2, '', cons2, stor.objDonnees)
    let elt, numbonneRep
    if (stor.choixQ2) {
      elt = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne5_1, {
        f: stor.objDonnees.f,
        liste1: { texte: ['', '&le;', '&ge;'] }
      })
      numbonneRep = (stor.objDonnees.a > 0) ? 1 : 2
    } else {
      j3pAffiche(stor.zoneCons3, '', ds.textes.consigne5_2, stor.objDonnees)
      elt = j3pAffiche(stor.zoneCons4, '', ds.textes.consigne6, {
        f: stor.objDonnees.f,
        t: stor.objDonnees.t,
        liste1: { texte: ['', '&le;', '&ge;'] }
      })
      numbonneRep = (stor.objDonnees.a > 0) ? 2 : 1
    }
    stor.zoneInput = [elt.selectList[0]]
    stor.zoneInput[0].reponse = (numbonneRep)
  }
  const zonesSaisieId = stor.zoneInput.map(elt => elt.id)
  const validePersonnelles = (me.etapeCourante % ds.nbetapes === 1) ? zonesSaisieId.slice(0, 2) : []
  stor.fctsValid = new ValidationZones({
    parcours: me,
    zones: zonesSaisieId,
    validePerso: validePersonnelles
  })
  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
  if (stor.numeroEtape === 1) {
    // mise à jour de la figure mtg32:
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAPIAAACugAAAQEAAAAAAAAAAQAAABb#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAFrAAItMv####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQAAAAAAAAAAAAAACAP####8AC2Fmb2lza2NhcnJlAAVhKmteMv####8AAAABAApDT3BlcmF0aW9uAv####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAB#####wAAAAEACkNQdWlzc2FuY2UAAAAFAAAAAgAAAAFAAAAAAAAAAAAAAAIA#####wAGYWZvaXNrAANhKmsAAAAEAgAAAAUAAAABAAAABQAAAAIAAAACAP####8ABmFmb2lzMgADMiphAAAABAIAAAABQAAAAAAAAAAAAAAFAAAAAQAAAAIA#####wABYgACLTIAAAADAAAAAUAAAAAAAAAAAAAAAgD#####AAFjAAItMQAAAAMAAAABP#AAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wACZjEAC2EqeF4yK2IqeCtjAAAABAAAAAAEAAAAAAQCAAAABQAAAAEAAAAG#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAFAAAAAAAAAAAAAAAQCAAAABQAAAAYAAAAIAAAAAAAAAAUAAAAHAAF4AAAABwD#####AAJmMgAKYWZvaXMyKngrYgAAAAQAAAAABAIAAAAFAAAABQAAAAgAAAAAAAAABQAAAAYAAXgAAAAHAP####8AAmYzAAZhZm9pczIAAAAFAAAABQABeAAAAAcA#####wACZzEACmEqZXhwKGsqeCkAAAAEAgAAAAUAAAAB#####wAAAAIACUNGb25jdGlvbgcAAAAEAgAAAAUAAAACAAAACAAAAAAAAXgAAAAHAP####8AAmcyAA9hZm9pc2sqZXhwKGsqeCkAAAAEAgAAAAUAAAAEAAAACQcAAAAEAgAAAAUAAAACAAAACAAAAAAAAXgAAAAHAP####8AAmczABRhZm9pc2tjYXJyZSpleHAoayp4KQAAAAQCAAAABQAAAAMAAAAJBwAAAAQCAAAABQAAAAIAAAAIAAAAAAABeAAAAAcA#####wAEcmVwMQAJMipleHAoLXgpAAAABAIAAAABQAAAAAAAAAAAAAAJBwAAAAMAAAAIAAAAAAABeAAAAAcA#####wAEcmVwMgAFMyp4KzEAAAAEAAAAAAQCAAAAAUAIAAAAAAAAAAAACAAAAAAAAAABP#AAAAAAAAAAAXj#####AAAAAwAQQ1Rlc3RFcXVpdmFsZW5jZQD#####AAllZ2FsaXRlZjIAAAAJAAAADgEAAAAAAT#wAAAAAAAAAQAAAAoA#####wAJZWdhbGl0ZWYzAAAACgAAAA8BAAAAAAE#8AAAAAAAAAEAAAAKAP####8ACWVnYWxpdGVnMgAAAAwAAAAOAQAAAAABP#AAAAAAAAABAAAACgD#####AAllZ2FsaXRlZzMAAAANAAAADwEAAAAAAT#wAAAAAAAAAf####8AAAACAAZDTGF0ZXgA#####wAAAAABAAD#####EEBQAAAAAAAAQE9VVgAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAACNcRm9yU2ltcHtnM31cdGV4dHsgZXQgfVxGb3JTaW1we2cyfQAAAAsA#####wAAAAABAAD#####EEBQQAAAAAAAQF4HrhR64UgAAAAAAAAAAAAAAAAAAQAAAAAAAAAAACVcRm9yU2ltcHtmMn0gXHRleHR7IGV0IH0gXEZvclNpbXB7ZjN9################'
    stor.mtgList = stor.mtgAppLecteur.createList(txtFigure)
    // Dans mtg32, la fonction est de la forme k*exp(ax)+b
    if (stor.laQuest === 1) {
      stor.mtgList.giveFormula2('b', stor.objDonnees.b)
      stor.mtgList.giveFormula2('c', stor.objDonnees.c)
    } else {
      stor.mtgList.giveFormula2('k', stor.objDonnees.k)
    }
    stor.mtgList.giveFormula2('a', stor.objDonnees.a)
    stor.mtgList.calculateNG()
    stor.premiereChance = true // cela va me servir pour donner une tentative de plus si la réponse n’est pas simplifiée (mais une seule fois)
  }
  j3pFocus(stor.zoneInput[0])
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  j3pDetruit(stor.zoneCalc)
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  const zone0 = (stor.laQuest === 2) ? j3pAddElt(zoneExpli, 'div') : ''
  ;[1, 2].forEach(i => { stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div') })
  if (stor.numeroEtape === 1) {
    j3pEmpty(stor.zoneCons5)
    if (stor.laQuest === 2) j3pAffiche(zone0, '', ds.textes.corr0)
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, stor.objDonnees)
    const correction2 = (stor.objDonnees.a > 0) ? ds.textes.corr2_1 : ds.textes.corr2_2
    j3pAffiche(stor.zoneExpli2, '', correction2, stor.objDonnees)
  } else {
    const correction1 = (stor.choixQ2)
      ? (stor.objDonnees.a > 0) ? ds.textes.corr3_1 : ds.textes.corr3_2
      : (stor.objDonnees.a > 0) ? ds.textes.corr5_1 : ds.textes.corr5_2
    j3pAffiche(stor.zoneExpli1, '', correction1, stor.objDonnees)
    j3pAffiche(stor.zoneExpli2, '', (stor.choixQ2) ? ds.textes.corr4 : ds.textes.corr6, stor.objDonnees)
  }
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const ds = me.donneesSection
  let reponse = { aRepondu: false, bonneReponse: false }
  reponse.aRepondu = stor.fctsValid.valideReponses()
  if (reponse.aRepondu) {
    if (stor.numeroEtape === 1) {
      const bonneRepEleve = []
      const tabVerif = (stor.laQuest === 1) ? ['egalitef2', 'egalitef3'] : ['egaliteg2', 'egaliteg3']
      stor.egalite = [true, true]
      const lesFcts = [stor.objDonnees.derivee, stor.objDonnees.derivee2nde]
      const repEleve = []
      for (let i = 0; i < 2; i++) {
        repEleve.push(j3pMathquillXcas(stor.fctsValid.zones.reponseSaisie[i]))
        repEleve[i] = repEleve[i].replace(/e\^x/g, 'exp(x)')
        repEleve[i] = repEleve[i].replace(/e\^\(/g, 'exp(')
        repEleve[i] = repEleve[i].replace(/(\d)([xe])/g, '$1*$2')
        stor.mtgList.giveFormula2('rep' + (i + 1), repEleve[i])
      }
      stor.mtgList.calculateNG()
      for (let i = 0; i < 2; i++) {
        bonneRepEleve.push(stor.mtgList.valueOf(tabVerif[i]) === 1)
        if (!bonneRepEleve[i]) {
          // On vérifie si la problème n’est pas juste que la réponse n’est pas simplifiée
          let fctTest = repEleve[i] + '-(' + lesFcts[i] + ')'
          fctTest = j3pMathquillXcas(fctTest)
          const arbreChaine = new Tarbre(fctTest, 'x')
          for (let k = -4; k <= 4; k++) {
            stor.egalite[i] = stor.egalite[i] && (Math.abs(arbreChaine.evalue([k])) < Math.pow(10, -12))
          }
        }
      }
      if ((!bonneRepEleve[0] || !bonneRepEleve[1]) && (stor.egalite[0] && stor.egalite[1]) && stor.premiereChance) me.essaiCourant -= 1
      if (bonneRepEleve[0] && bonneRepEleve[1]) me.essaiCourant = ds.nbetapes // s’il s’est juste trompé sur la convexité, je ne lui redonne pas une nouvelle chance
      // Et pour la convexité :
      const convexiteChoisie = stor.zoneInput[2].selectedIndex
      bonneRepEleve.push(convexiteChoisie === stor.zoneInput[2].reponse[0])
      reponse.bonneReponse = true
      stor.derniereReponseValidee = true
      for (let i = 1; i >= 0; i--) {
        stor.fctsValid.zones.bonneReponse[i] = bonneRepEleve[i]
        reponse.bonneReponse = (reponse.bonneReponse && stor.fctsValid.zones.bonneReponse[i])
        stor.fctsValid.coloreUneZone(stor.zoneInput[i].id)
      }
      if (reponse.bonneReponse || (me.essaiCourant === ds.nbchances)) {
        // Je ne teste la liste déroulante que si les 2 premières réponses sont bonnes ou que c’est la dernière possibilité pour répondre
        stor.fctsValid.zones.bonneReponse[2] = bonneRepEleve[2]
        reponse.bonneReponse = (reponse.bonneReponse && stor.fctsValid.zones.bonneReponse[2])
        stor.fctsValid.coloreUneZone(stor.zoneInput[2].id)
      } else {
        stor.derniereReponseValidee = false
      }
    } else {
      reponse = stor.fctsValid.validationGlobale()
    }
    if (stor.numeroEtape === 1) {
      let leScore = 0
      leScore += (stor.fctsValid.zones.bonneReponse[0]) ? ds.baremeQ1[0] : 0
      leScore += (stor.fctsValid.zones.bonneReponse[1]) ? ds.baremeQ1[1] : 0
      if (Math.abs(leScore - 0.8) < Math.pow(10, -12)) leScore += (stor.fctsValid.zones.bonneReponse[2]) ? ds.baremeQ1[2] : 0
      return leScore
    } else {
      return (reponse.bonneReponse) ? 1 : 0
    }
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (score !== 1) {
    me.reponseKo(stor.divCorrection)
  }
  if (score !== 1 && me.essaiCourant < ds.nbchances) {
    if (stor.numeroEtape === 1) {
      const testEquivalence = (!stor.fctsValid.zones.bonneReponse[0] || !stor.fctsValid.zones.bonneReponse[1]) && (stor.egalite[0] && stor.egalite[1])
      if (testEquivalence) {
        j3pAddContent(stor.divCorrection, '<br>')
        me.reponseKo(stor.divCorrection, ds.textes.comment1, true)
      }
      if (!stor.derniereReponseValidee) {
        j3pAddContent(stor.divCorrection, '<br>')
        me.reponseKo(stor.divCorrection, ds.textes.comment3, true)
      }
      if (testEquivalence && stor.premiereChance) {
        j3pAddContent(stor.divCorrection, '<br>')
        me.reponseKo(stor.divCorrection, ds.textes.comment2, true)
        stor.premiereChance = false
      } else {
        me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
      }
    } else {
      me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    }
    return me.finCorrection()
  }
  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.score += score
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    me.score += score
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
