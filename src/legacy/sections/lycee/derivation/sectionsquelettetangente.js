import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pChaine, j3pDetruit, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pImporteAnnexe, j3pMathquillXcas, j3pPaletteMathquill, j3pShowError, j3pShuffle, j3pStyle } from 'src/legacy/core/functions'
import loadXcasRequete from 'src/legacy/outils/xcas/loadXcasRequete'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// la fct qui sera chargée par
let xcasRequete
/*
 *
 *Section squelette pour l’équation d’une tangente en un point
 *
 *Alexis Lecomte septembre 2013
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['f', 'tangentes1', 'string', 'Fichier js (dans sectionsAnnexes/squelettes-tangente sans l’extension) des variables de l’exercice']
  ]
}

/**
 * section squelettetangente
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function EgaliteFormelle (expression1, expression2) {
    return (xcasRequete('simplify(normal(' + expression1 + '-(' + expression2 + ')))') === '0')
  }

  function parentheseFermante (chaine, index) {
    // fonction qui donne la position de l’accolade fermante correspondant à l’accolade ouvrante positionnée en index de la chaine
    // penser à une condition d’arrêt pour ne pas faire planter le truc si la saisie est mal écrite
    // (ptet au debut de conversion mathquill : même nb d’accolades ouvrantes que fermantes)
    // pour l’instant 1 accolade ouvrante (à l’appel de la fonction)
    let indexaccolade = 1
    let indexvariable = index
    while (indexaccolade !== 0) {
      // je peux avoir des accolades internes (par groupe de 2 necessairement)
      // for (var indexvariable=index+1; indexvariable<chaine.length; indexvariable++){
      indexvariable++
      if (chaine.charAt(indexvariable) === '(') indexaccolade++
      if (chaine.charAt(indexvariable) === ')') indexaccolade--
    }
    return indexvariable
  }
  function afficheCorrection (bonneReponse) {
    j3pDetruit(stor.zoneCons4)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    const chaine = me.stockage[20][me.questionCourante - 1].correction_detaillee
    j3pAffiche(zoneExpli, '', chaine, {
      a: me.stockage[20][me.questionCourante - 1].a,
      b: me.stockage[20][me.questionCourante - 1].b,
      c: me.stockage[20][me.questionCourante - 1].c,
      x: me.stockage[20][me.questionCourante - 1].valeur,
      u: me.stockage[6],
      u2: (me.stockage[6].charAt(0) === '-') ? me.stockage[6] : '+' + me.stockage[6],
      y: me.stockage[4],
      v: me.stockage[5],
      w: me.stockage[20][me.questionCourante - 1].nom,
      z: me.stockage[3]
    })
  }
  function signeFacteur (nombre) {
    if (nombre === 1) {
      return ''
    } else {
      if (nombre === -1) {
        return '-'
      } else {
        return nombre
      }
    }
  }
  // fonction qui renvoit un polynome au format latex de degré n
  function generePolynome (n) {
    let lePol = ''
    // premier facteur :
    let alea = j3pGetRandomInt(0, 1)
    if (alea === 1) {
      lePol += '-'
    }
    lePol += signeFacteur(j3pGetRandomInt(1, 15))
    for (let i = Number(n); i > 0; i--) {
      if (i === 1) {
        lePol += 'x'
      } else {
        lePol += 'x^{' + i + '}'
      }
      alea = j3pGetRandomInt(0, 1)
      if (alea === 1) {
        lePol += '+'
      } else {
        lePol += '-'
      }
      lePol += signeFacteur(j3pGetRandomInt(2, 15))
    }
    return lePol
  }
  function chargementdesdonnees (fichier) {
    function extractionDonnees (tableau, unobj, tabfinal) {
      unobj.expression = tableau[0].substring(12, tableau[0].length)
      // cas spécial si polynome
      let coeff, decalage, nomVariable, indice
      while (unobj.expression.indexOf('polynome') !== -1) {
        // une seule lettre pour la variable dans un premier cas désignée par une variable
        if (unobj.expression.charAt(unobj.expression.indexOf('polynome') + 9) === '£') {
          decalage = 12
          nomVariable = unobj.expression.charAt(unobj.expression.indexOf('polynome') + 10)
          for (let i = 5; i < 15; i++) {
            try {
              if (nomVariable === tableau[i].charAt(0)) {
                indice = i
              }
            } catch (error) {
              console.error(error)
            }
          }
          if (indice) {
            const [minInt, maxInt] = j3pGetBornesIntervalle(tableau[indice].substring(3, tableau[indice].length))
            coeff = j3pGetRandomInt(minInt, maxInt)
          } else {
            // garde fou en cas d’oubli de précision
            coeff = j3pGetRandomInt(3, 5)
          }
        } else {
          decalage = 11
          // on a demandé un polynome de degré bien précis
          const index1 = unobj.expression.indexOf('polynome') + 9
          // on détecte la fin de parenthèse
          const index2 = parentheseFermante(unobj.expression, index1)
          coeff = unobj.expression.substring(index1, index2)
        }
        // plutôt remplacer polynôme(£a) par son expresionn
        unobj.expression = unobj.expression.substring(0, unobj.expression.indexOf('polynome')) + generePolynome(coeff) + unobj.expression.substring(unobj.expression.indexOf('polynome') + decalage)
      }
      unobj.ensemble = tableau[1].substring(10, tableau[1].length)
      unobj.valeur = tableau[2].substring(8, tableau[2].length)
      unobj.nom = tableau[3].substring(5, tableau[3].length)
      unobj.correction_detaillee = tableau[4].substring(22, tableau[4].length)
      if (tableau.length > 5 && tableau[5] !== '') {
        const [minInt, maxInt] = j3pGetBornesIntervalle(tableau[5].substring(3, tableau[5].length))
        unobj.a = j3pGetRandomInt(minInt, maxInt)
      }
      if (tableau.length > 6 && tableau[6] !== '') {
        const [minInt, maxInt] = j3pGetBornesIntervalle(tableau[6].substring(3, tableau[6].length))
        unobj.b = j3pGetRandomInt(minInt, maxInt)
      }
      if (tableau.length > 7 && tableau[7] !== '') {
        const [minInt, maxInt] = j3pGetBornesIntervalle(tableau[7].substring(3, tableau[7].length))
        unobj.c = j3pGetRandomInt(minInt, maxInt)
      }
      unobj.expression = j3pChaine(unobj.expression, unobj)
      me.logIfDebug(' unobj.expression=' + unobj.expression,
        '\n unobj.ensemble=' + unobj.ensemble,
        '\n unobj.valeur=' + unobj.valeur,
        '\n unobj.nom=' + unobj.nom,
        '\n unobj.correction_detaillee=' + unobj.correction_detaillee,
        '\n unobj.a=' + unobj.a,
        '\n unobj.b=' + unobj.b,
        '\n unobj.c=' + unobj.c
      )
      tabfinal.push(unobj)
    }
    // tableau d’objets {expression:"",ensemble:"",valeur:"",nom:"",correction_detaillee:""}
    const tabgeneral = []
    const nbitems = Number(fichier.substring(0, fichier.indexOf('%')).substring(0, fichier.indexOf('tangentes')))
    fichier = fichier.substring(fichier.indexOf('%') + 1)
    // tableau de chaines contenant les différents items
    const letab = []
    let decal, ssch
    for (let k = 0; k < nbitems; k++) {
      if (k <= 8) decal = 1
      else decal = 2
      if (k < nbitems - 1) {
        ssch = fichier.substring(fichier.indexOf('tangente' + (k + 1)) + 9 + decal, fichier.indexOf('tangente' + (k + 2)))
      } else {
        ssch = fichier.substring(fichier.indexOf('tangente' + (k + 1)) + 9 + decal)
      }
      letab.push(ssch)
    }
    let unobjet, tab
    for (let p = 0; p < nbitems; p++) {
      unobjet = { expression: '', ensemble: '', valeur: '', correction_detaillee: '' }
      tab = letab[p].split('%')
      //    if (p<nbitems-1) tab.pop();
      extractionDonnees(tab, unobjet, tabgeneral)
    }
    // Cas particulier : si nbrepetitions>taille du tableau, on complète le tableau en y ajoutant des éléments déjà chargés...
    if (tabgeneral.length < ds.nbrepetitions) {
      for (let p = nbitems; p < ds.nbrepetitions; p++) {
        unobjet = { expression: '', ensemble: '', valeur: '', correction_detaillee: '' }
        tab = letab[(p - nbitems) % nbitems].split('%')
        extractionDonnees(tab, unobjet, tabgeneral)
      }
    }
    return tabgeneral
  }

  function getDonnees () {
    return {
      typesection: 'lycee', //

      nbrepetitions: 3,
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"
      f: 'tangentes1',
      textes: {
        titreExo: 'Equations de tangentes',
        question1: 'On définit la fonction $£w$ sur $£x$ par $£w(x)=£y$.',
        question2: 'Détermine l’équation réduite de la tangente à sa courbe représentative au point d’abscisse $£z$ :',
        correction: 'Étudie bien la correction :'
      },
      pe: 0
    }
  }
  function suite () {
    me.construitStructurePage(ds.structure)

    // par convention, this.stockage[0] contient la solution
    // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    me.afficheTitre(ds.textes.titreExo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    suite2()
  }
  function suite2 () {
    function suite3 () {
      stor.solution[1] = xcasRequete('equation(LineTan(' + stor.formuleXcas + ',x=' + stor.a + ') ')

      xcasRequete('f:=unapply(simplify(diff(' + stor.formuleXcas + ',x)),x)')
      stor.solution[2] = xcasRequete('f(x)')
      stor.solution[2] = xcasRequete('simplify(diff(' + stor.formuleXcas + ',x))')
      // Je factorise dès lors que je n’ai pas une fonction affine
      me.stockage[4] = (/^\(?-?\d*\*?x[+-]\d+\)?$/.test(stor.solution[2]))
        ? xcasRequete('latex(' + stor.solution[2] + ')')
        : xcasRequete('latex(factor(' + stor.solution[2] + '))')
      me.stockage[4] = me.stockage[4].substring(1, me.stockage[4].length - 1)
      stor.solution[3] = xcasRequete('f(' + stor.a + ')')
      me.stockage[5] = xcasRequete('latex(' + stor.solution[3] + ')')
      me.stockage[5] = me.stockage[5].substring(1, me.stockage[5].length - 1)
      // me.stock6 doit contenir g(a)
      xcasRequete('g:=unapply(' + stor.formuleXcas + ',x)')
      me.stockage[6] = xcasRequete('g(' + stor.a + ')')
      me.stockage[6] = xcasRequete('latex(' + me.stockage[6] + ')')
      me.stockage[6] = me.stockage[6].substring(1, me.stockage[6].length - 1)
      // je vire le y=
      stor.solution[1] = stor.solution[1].substring(2, stor.solution[1].length)
      // la solution finale, au format latex, pour l’affichage de la correction détaillée : (on enlève les deux premiers caractères car c’est "y="'
      me.stockage[3] = xcasRequete('latex(' + stor.solution[1] + ')')
      // la commande latex de xcas renvoit des guillemets en premier et dernier caractère à virer donc:
      me.stockage[3] = me.stockage[3].substring(1, me.stockage[3].length - 1)

      // Obligatoire
      me.finEnonce()
    }

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '15px' }) })
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // Description des variables :
    // a :ensemble de définition de la fonction
    // b :expression de la fonction
    // c :ensemble de définition de la fonction dérivée
    // f : nom de la fonction
    me.logIfDebug('ensemble=' + me.stockage[20][me.questionCourante - 1].ensemble,
      '\nexpression=' + me.stockage[20][me.questionCourante - 1].expression,
      '\nvaleur=' + me.stockage[20][me.questionCourante - 1].valeur,
      '\nnom=' + me.stockage[20][me.questionCourante - 1].nom
    )
    me.stockage[20][me.questionCourante - 1].valeur = j3pChaine(me.stockage[20][me.questionCourante - 1].valeur, me.stockage[20][me.questionCourante - 1])
    j3pAffiche(stor.zoneCons1, '', ds.textes.question1, {
      x: me.stockage[20][me.questionCourante - 1].ensemble,
      y: me.stockage[20][me.questionCourante - 1].expression,
      w: me.stockage[20][me.questionCourante - 1].nom
    })
    j3pAffiche(stor.zoneCons2, '', ds.textes.question2, { z: me.stockage[20][me.questionCourante - 1].valeur })
    me.logIfDebug('a=' + me.stockage[20][me.questionCourante - 1].a,
      '\nb=' + me.stockage[20][me.questionCourante - 1].b,
      '\nc=' + me.stockage[20][me.questionCourante - 1].c,
      '\nme.stockage[20][me.questionCourante-1].valeur=' + me.stockage[20][me.questionCourante - 1].valeur
    )
    const eltCons2 = j3pAffiche(stor.zoneCons3, '', '$y=$&1&', me.stockage[20][me.questionCourante - 1])
    stor.zoneInput = eltCons2.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.+-*/x()^', { commandes: ['racine', 'exp', 'fraction', 'puissance'] })
    j3pStyle(stor.zoneCons4, { paddingTop: '10px' })
    j3pPaletteMathquill(stor.zoneCons4, stor.zoneInput, { liste: ['racine', 'exp', 'fraction', 'puissance'] })
    j3pFocus(stor.zoneInput)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // la fonction convertie au format xcas pour calculs
    me.stockage[10] = j3pChaine(me.stockage[20][me.questionCourante - 1].expression, me.stockage[20][me.questionCourante - 1])

    stor.formuleXcas = j3pMathquillXcas(me.stockage[10])
    // conversion au format XCAS
    stor.a = j3pMathquillXcas(j3pChaine(me.stockage[20][me.questionCourante - 1].valeur, me.stockage[20][me.questionCourante - 1]))
    me.logIfDebug('me.stockage[20][me.questionCourante-1].valeur=' + me.stockage[20][me.questionCourante - 1].valeur,
      '\nme.stockage[20][me.questionCourante-1].a=' + me.stockage[20][me.questionCourante - 1].a,
      '\nme.stockage[20][me.questionCourante-1].b=' + me.stockage[20][me.questionCourante - 1].b,
      '\nme.stockage[20][me.questionCourante-1].c=' + me.stockage[20][me.questionCourante - 1].c
    )
    // la dérivée, au format xcas
    setTimeout(function () {
      suite3()
    }, 500)
  }
  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        this.surcharge()

        this.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        j3pImporteAnnexe('squelettes-tangente/' + ds.f + '.js').then(function (datasSection) {
          me.stockage[0] = datasSection.txt
          me.stockage[20] = chargementdesdonnees(me.stockage[0])
          me.stockage[20] = j3pShuffle(me.stockage[20])
        })
          .then(loadXcasRequete)
          .then((_xcasRequete) => {
            xcasRequete = _xcasRequete
            suite()
          }).catch(error => {
            j3pShowError(error, { message: 'Impossible de charger les prérequis de cet exercice' })
          })
      } else {
        // A chaque répétition, on nettoie la scène (ici partiellement car plusieurs étapes avec même consigne
        me.videLesZones()
        suite2()
      }

      // OBLIGATOIRE

      break // case "enonce":

    case 'correction':

      // On teste si une réponse a été saisie
      $(stor.zoneInput).blur()
      {
        const repEleve = $(stor.zoneInput).mathquill('latex')
        // var repElevePrimitive = $(document.getElementById('reponse1inputmq1')).mathquill('latex');

        if (!repEleve && !this.isElapsed) {
          this.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
          this.afficheBoutonValider()
        } else {
        // on convertit au format xcas les réponses élève pour traitement
          const repEleveXcas = j3pMathquillXcas(repEleve)
          const reponseAnalysee = EgaliteFormelle(repEleveXcas, stor.solution[1])
          // Bonne réponse
          if (reponseAnalysee) {
            this._stopTimer()
            this.score++
            stor.zoneCorr.style.color = this.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            stor.zoneInput.style.color = this.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            afficheCorrection(true)
            this.typederreurs[0]++
            this.cacheBoutonValider()
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = this.styles.cfaux
            // A cause de la limite de temps :
            if (this.isElapsed) { // limite de temps
              this._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              this.typederreurs[10]++
              this.cacheBoutonValider()
              afficheCorrection(false)
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              stor.zoneCorr.innerHTML = cFaux
              stor.zoneInput.style.color = this.styles.cfaux

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                this.typederreurs[1]++
                j3pFocus(stor.zoneInput)
              // indication éventuelle ici
              } else {
                this._stopTimer()
                this.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorrection(false)
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                this.typederreurs[2]++
                this.etat = 'navigation'
                this.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
      //

    case 'navigation':
      if (this.sectionTerminee()) {
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
