import { j3pAddElt, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pMonome, j3pPaletteMathquill, j3pRandomTab, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import $ from 'jquery'
import { afficheSystemeEquations } from 'src/lib/widgets/systemeEquations'
import { j3pAfficheSysteme } from 'src/legacy/core/functionsSysteme'
import { j3pGetLatexMonome, j3pGetLatexQuotient } from 'src/legacy/core/functionsLatex'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since mai 2022
 * @fileOverview Cette section demande de déterminer des coefficients pour que la fonction proposée soit une solution d’une équation différentielle
 */

/**
 * Les paramètres de la section, anciennement j3p.SectionXxx = {}
 */
export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeFonction', 'polynome', 'liste', 'Type de fonction qui pourra être polynomiale ou de la forme (ax+b)exp(kx)', ['polynome', 'expo']]
  ]
}

/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {

    // Nombre de répétitions de l’exercice
    nbrepetitions: 3,
    nbchances: 2,
    indication: '',
    typeFonction: 'polynome',
    textes: {
      titre_exo: 'Solution particulière d’une équation différentielle',
      consigne1: 'On considère l’équation différentielle (E) : $£e$.',
      consigne2_1: 'Déterminer les réels $a$ et $b$ pour que la fonction $£f$ définie sur $\\R$ par $£f(x)=£{fx}$ soit solution de (E).',
      consigne2_2: 'Déterminer les réels $a$, $b$ et $c$ pour que la fonction $£f$ définie sur $\\R$ par $£f(x)=£{fx}$ soit solution de (E).',
      consigne3_1: '$a=$&1& et $b=$&2&.',
      consigne3_2: '$a=$&1& ; $b=$&2& et $c=$&3&.',
      comment1: '',
      corr1: 'On dérive la fonction $£f$ sur $\\R$, ce qui donne $£f\'(x)=£{fprimex}$.',
      corr2: 'Dans l’équation différentielle (E), on remplace $y$ par $£f(x)$ et $y\'$ par $£f\'(x)$ puis on réorganise le membre de gauche, ce qui donne $£g$.',
      corr3: 'Par identification des coefficients, on obtient alors le système&nbsp;:',
      corr4: 'En résolvant ce système, on obtient&nbsp;:<br/>‡1‡ et ainsi $£f(x)=£{fxcorr}$.'
    }
  }
} // getDonnees

/**
 * Retourne les données pour chaque sujet
 * @private
 * @param {Parcours} me
 * @param {String} typeFonction
 * @return {Object}
 */
function genereAlea (typeFonction) {
  const obj = {}
  obj.k = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 3)
  if (typeFonction === 'polynome') {
    // on aura y'+ky=mx^2+px
    do {
      obj.a = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 4)
    } while (obj.a === 1)
    obj.b = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 3)
    obj.c = -obj.b / obj.k
    obj.m = obj.a * obj.k
    obj.p = 2 * obj.a + obj.k * obj.b
    obj.e = 'y\'' + j3pMonome(2, 1, obj.k, 'y') + '=' + j3pMonome(1, 2, obj.m, 'x') + j3pMonome(2, 1, obj.p, 'x')
    obj.coefs = [obj.a, obj.b, obj.c]
    obj.fx = 'ax^2+bx+c'
    obj.fprimex = '2ax+b'
    obj.g = 'ax^2+(' + j3pMonome(1, 1, obj.k, 'b') + '+2a)x+b' + j3pMonome(2, 1, obj.k, 'c') + '=' + j3pMonome(1, 2, obj.m, 'x') + j3pMonome(2, 1, obj.p, 'x')
    obj.eqs = ['$' + j3pMonome(1, 1, obj.k, 'a') + '=' + obj.m + '$', '$' + j3pMonome(1, 1, obj.k, 'b') + '+2a' + '=' + obj.p + '$', '$b' + j3pMonome(2, 1, obj.k, 'c') + '=0$']
    obj.fxcorr = j3pMonome(1, 2, obj.a) + j3pMonome(2, 1, obj.b) + j3pGetLatexMonome(3, 0, j3pGetLatexQuotient(-obj.b, obj.k))
  } else if (typeFonction === 'expo') {
    // on aura y'+ky=(mx+p)e^{cx}
    obj.a = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 4)
    do {
      obj.c = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 4)
    } while (obj.c === 1 || obj.c === -obj.k)
    obj.p = j3pGetRandomInt(-1, 1)
    obj.m = obj.a * (obj.k + obj.c)
    const expo = (obj.p === 0)
      ? j3pMonome(1, 1, obj.m) + '\\mathrm{e}^{' + j3pMonome(1, 1, obj.c) + '}'
      : '(' + j3pMonome(1, 1, obj.m) + j3pMonome(2, 0, obj.p) + ')\\mathrm{e}^{' + j3pMonome(1, 1, obj.c) + '}'
    obj.e = 'y\'' + j3pMonome(2, 1, obj.k, 'y') + '=' + expo
    obj.b = (obj.p - obj.a) / (obj.c + obj.k)
    obj.coefs = [obj.a, obj.b]
    obj.fx = '(ax+b)\\mathrm{e}^{' + j3pMonome(1, 1, obj.c) + '}'
    const deriveePart1 = 'a\\mathrm{e}^{' + j3pMonome(1, 1, obj.c) + '} + (ax+b)\\times ' + ((obj.c > 0) ? j3pMonome(1, 1, obj.c, '\\mathrm{e}^{' + j3pMonome(1, 1, obj.c) + '}') : '\\left(' + j3pMonome(1, 1, obj.c, '\\mathrm{e}^{' + j3pMonome(1, 1, obj.c) + '}') + '\\right)')
    obj.fprimex = deriveePart1 + '=(' + j3pMonome(1, 1, obj.c, 'ax') + j3pMonome(2, 1, obj.c, 'b') + '+a)\\mathrm{e}^{' + j3pMonome(1, 1, obj.c) + '}'
    obj.g = '(' + j3pMonome(1, 1, obj.k + obj.c, 'ax') + j3pMonome(2, 1, obj.c + obj.k, 'b') + '+a' + ')\\mathrm{e}^{' + j3pMonome(1, 1, obj.c) + '}=' + expo
    obj.eqs = ['$' + j3pMonome(1, 1, obj.k + obj.c, 'a') + '=' + obj.m + '$', '$' + j3pMonome(1, 1, obj.c + obj.k, 'b') + '+a' + '=' + obj.p + '$']
    obj.fxcorr = '\\left(' + j3pMonome(1, 1, obj.a) + j3pGetLatexMonome(3, 0, j3pGetLatexQuotient(obj.p - obj.a, obj.k + obj.c)) + '\\right)\\mathrm{e}^{' + j3pMonome(1, 1, obj.c) + '}'
  } else {
    throw Error(`type de fonction non géré ${typeFonction}`)
  }
  obj.f = j3pRandomTab(['f', 'g', 'h'], [0.33, 0.33, 0.34])
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(me.donneesSection.textes.titre_exo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  function ecoute (laZone) {
    if (laZone.className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, laZone, { liste: ['fraction'] })
    }
  }
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.objDonnees = genereAlea(ds.typeFonction)
  // on affiche l’énoncé
  for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
  stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
  j3pAffiche(stor.zoneCons1, j3pGetNewId('enonce'), ds.textes.consigne1, stor.objDonnees)
  const consigne2 = (ds.typeFonction === 'polynome') ? ds.textes.consigne2_2 : ds.textes.consigne2_1
  j3pAffiche(stor.zoneCons2, j3pGetNewId('enonce'), consigne2, stor.objDonnees)
  const consigne3 = (ds.typeFonction === 'polynome') ? ds.textes.consigne3_2 : ds.textes.consigne3_1
  const elt = j3pAffiche(stor.zoneCons3, j3pGetNewId('enonce'), consigne3, stor.objDonnees)
  stor.zoneInput = elt.inputmqList.map(eltInput => eltInput)
  stor.zoneInput.forEach((eltInput, index) => {
    mqRestriction(eltInput, '\\d,./-', { commandes: ['fraction'] })
    eltInput.typeReponse = ['nombre', 'exact']
    eltInput.reponse = [stor.objDonnees.coefs[index]]
    $(eltInput).focusin(function () { ecoute(this) })
  })
  j3pStyle(stor.zoneCons4, { paddingTop: '10px' })
  stor.laPalette = j3pAddElt(stor.zoneCons4, 'div')
  j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], { liste: ['fraction'] })
  const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
  stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
  j3pFocus(stor.zoneInput[0])
  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  j3pEmpty(stor.laPalette)
  // on affiche la correction dans la zone xxx
  const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
  j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, stor.objDonnees)
  j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2, stor.objDonnees)
  j3pAffiche(stor.zoneExpli3, '', ds.textes.corr3)
  // Le système
  const equations = stor.objDonnees.eqs.map(elt => { return { contenu: elt } })
  afficheSystemeEquations(stor.zoneExpli4, equations)
  const equations2 = (ds.typeFonction === 'polynome')
    ? ['$a=' + stor.objDonnees.a + '$', '$b=' + stor.objDonnees.b + '$', '$c=' + j3pGetLatexQuotient(-stor.objDonnees.b, stor.objDonnees.k) + '$']
    : ['$a=' + stor.objDonnees.a + '$', '$b=' + j3pGetLatexQuotient(stor.objDonnees.p - stor.objDonnees.a, stor.objDonnees.k + stor.objDonnees.c) + '$']
  j3pAfficheSysteme(stor.zoneExpli5, '', ds.textes.corr4, [equations2], stor.objDonnees, zoneExpli.style.color)
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  if (reponse.aRepondu) {
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  stor.score = score
  if (score !== null) me.score += score
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
    if (stor.pbArrondi) {
      j3pAddElt(stor.divCorrection, 'br')
      me.reponseKo(stor.divCorrection, ds.textes.comment1, true)
    }
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
