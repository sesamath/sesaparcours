import { j3pAddElt, j3pBarre, j3pNombre, j3pVirgule, j3pDetruit, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pPaletteMathquill, j3pPGCD, j3pGetBornesIntervalle, j3pStyle, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import { j3pCreeRectangle, j3pCreeSegment } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    février 2013
    Tableau donnant le signe d’une fonction affine

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition (ramené à 1 si le zéro de la fonction est donné ou est correct)'],
    ['zerodonne', false, 'boolean', 'On peut choisir de donner le zéro de la fonction'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.5, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 50% des cas)'],
    ['zeroentier', false, 'boolean', "On peut imposer au zéro de la fonction d'être un entier"],
    ['a', '[-10;10]', 'string', 'valeur a dans l’expression ax+b (a différent de 0). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[1;1]|[2;4]|[-4;-2]" par exemple pour qu’il soit égal à 1 dans un premier temps, positif ensuite puis négatif'],
    ['b', '[-10;10]', 'string', 'valeur b dans l’expression ax+b (non paramétrable si zeroentier = true)'],
    ['ordre_affine', 'alea', 'string', "la fonction affine peut être de la forme 'ax+b', 'b+ax' ou 'alea'. On peut modifier l’ordre d’une répétition à l’autre, par exemple avec 'ax+b|b+ax|alea'."],
    ['type_sol', 'variations', 'liste', '"variations" ou "inequation", précise si la réponse est justifiée à l’aide des variations de la fonction affine ou à l’aide d’une inéquation', ['variations', 'inequation']],
    ['imposer_fct', '', 'string', "on peut imposer une ou plusieurs fonctions affines en écrivant par exemple '-x+3|2x-3' pour imposer seulement les deux premières. On peut aussi écrire 'ax' ou 'x+b', de même on peut avoir des coefficients fractionnaires ou sous la forme ...racine(...)."]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'problème avec les signes' },
    { pe_3: 'problème avec le zéro de la fonction' },
    { pe_4: 'insuffisant' }
  ]
}
const textes = {
  titre_exo: 'Signe d’une fonction affine',
  consigne1: 'Donner le signe de $£a$ sur $\\R$ en complétant le tableau ci-dessous.<br>',
  comment1: 'Il faut compléter tout le tableau&nbsp;!',
  comment1bis: 'Il faut choisir entre 0 et la barre verticale pour la liste déroulante&nbsp;!',
  corr1: 'On résout l’équation $£a$, ce qui donne $£b$.<br>',
  corr2: 'Comme $£a$, la fonction affine est strictement décroissante sur $\\R$ et on obtient $£b$ sur $]-\\infty;£c[$ et $£d$ sur $]£c;+\\infty[$.',
  corr3: 'Comme $£a$, la fonction affine est strictement croissante sur $\\R$ et on obtient $£b$ sur $]-\\infty;£c[$ et $£d$ sur $]£c;+\\infty[$.',
  corr4: 'On obtient le tableau de signes ci-dessous.',
  corr5: 'On résout l’inéquation $£a$, ce qui donne $£c$, c’est-à-dire $£b$.<br>',
  corr6: 'Ainsi $£b$ sur $]-\\infty;£c[$ et $£d$ sur $]£c;+\\infty[$.',
  corr7: 'Ainsi $£b$ sur $]-\\infty;£c[$ et $£d$ sur $]£c;+\\infty[$.',
  corr8: 'Ne pas oublier de préciser que la fonction s’annule !',
  corr9: 'La fonction ne s’annule pas en la valeur proposée !',
  corr10: 'Au moins l’un des signes est faux !'
}
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage

  function tableauSignesbis (nomdiv, expres, L, h, zeroTab, valZero, signeTabCorr, styleTxt) {
    // on crée un tableau de signes dans nomdiv
    // ici il ne s’agit que du tableau du signe d’une fonction affine
    // expres est celle d’une fonction affine (du style ax+b)
    // zeroTab[0] est un booléen. Il vaut true lorsque la valeur en laquelle s’annule l’expression n’est pas à remplir par l’élève
    // zeroTab[1] est un booléen. Il vaut true lorsque les signes sont complétés
    // valZero est la valeur en laquelle s’annule la fonction affine (c’est une chaine de caractère)'
    // signeTabCorr contient ["+","-"] ou ["-","+"].
    // styleTxt est le style du texte. On en extrait la couleur pour la couleur du tableau
    //
    // je crée mes textes au début - si jamais on se lance dans la traduction
    const txt2Txt = 'signe de $' + expres + '$'
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    svg.setAttribute('width', L)
    svg.setAttribute('height', h)
    nomdiv.appendChild(svg)
    j3pCreeRectangle(svg, { x: 1, y: 0, width: L - 1, height: h, couleur: '#000000', epaisseur: 1 })
    j3pCreeSegment(svg, { x1: 1, y1: 0, x2: 0, y2: h, couleur: '#000000', epaisseur: 1 })
    j3pCreeSegment(svg, { x1: 1, y1: h / 2, x2: L, y2: h / 2, couleur: '#000000', epaisseur: 1 })
    // placement de la ligne "signe de ax+b"
    const signeDe = j3pAddElt(nomdiv, 'div', '', { style: styleTxt })
    j3pStyle(signeDe, { position: 'absolute' })
    j3pAffiche(signeDe, '', txt2Txt)
    // pour placer le segment vertical, je cherche la largeur de la phrase "signe de ..."
    const largeurSignede = signeDe.getBoundingClientRect().width + 12 // petit correctif de 12 pour gérer la taille lorsqu’on passe à mathquill
    j3pCreeSegment(svg, { x1: largeurSignede, y1: 0, x2: largeurSignede, y2: h, couleur: '#000000', epaisseur: 1 })
    const posTxt = h / 2 + h / 4 - signeDe.getBoundingClientRect().height / 2
    signeDe.style.top = posTxt + 'px'
    signeDe.style.left = '2px'

    // placement du "x"
    const leX = j3pAddElt(nomdiv, 'div', '', { style: styleTxt })
    j3pStyle(leX, { position: 'absolute' })
    j3pAffiche(leX, '', '$x$')
    const posTxt2 = h / 4 - leX.getBoundingClientRect().height / 2
    leX.style.top = posTxt2 + 'px'
    const posTxt3 = largeurSignede / 2 - leX.getBoundingClientRect().width / 2
    leX.style.left = posTxt3 + 'px'

    // plus et moins l’infini'
    const moinsInf = j3pAddElt(nomdiv, 'div', '', { style: styleTxt })
    j3pStyle(moinsInf, { position: 'absolute' })
    const plusInf = j3pAddElt(nomdiv, 'div', '', { style: styleTxt })
    j3pStyle(plusInf, { position: 'absolute' })
    j3pAffiche(moinsInf, '', '$-\\infty$')
    moinsInf.style.left = (largeurSignede + 1) + 'px'
    moinsInf.style.top = (h / 4 - moinsInf.getBoundingClientRect().height / 2) + 'px'
    j3pAffiche(plusInf, '', '$+\\infty$')
    plusInf.style.left = (me.questionCourante === 1 && me.etat === 'enonce') ? ((L - plusInf.getBoundingClientRect().width * 1.3 - 2 + 1) + 'px') : ((L - plusInf.getBoundingClientRect().width - 2 + 1) + 'px')
    plusInf.style.top = (h / 4 - plusInf.getBoundingClientRect().height / 2) + 'px'

    const zoneNulle = j3pAddElt(nomdiv, 'div', '', { style: styleTxt })
    j3pStyle(zoneNulle, { position: 'absolute' })
    stor.zoneInput = []
    if (zeroTab[0]) {
      // l’endroit où s’annule la fonction est donné'
      // attention leZero doit être du type string (un entier ou un décimal ou un nb fractionnaire de la forme a/b
      const leZero = (/[0-9]*\.[0-9]/g.test(valZero)) ? j3pVirgule(valZero) : valZero
      let monZeroTxt
      if (leZero.includes('/')) {
        const tabZero = leZero.split('/')
        monZeroTxt = '\\frac{' + tabZero[0] + '}{' + tabZero[1] + '}'
      } else {
        monZeroTxt = leZero
      }
      j3pStyle(zoneNulle, { position: 'absolute' })
      j3pAffiche(zoneNulle, '', '$' + monZeroTxt + '$')
      zoneNulle.style.left = (largeurSignede + (L - largeurSignede) / 2 - zoneNulle.getBoundingClientRect().width / 2) + 'px'
      zoneNulle.style.top = (h / 4 - zoneNulle.getBoundingClientRect().height / 2) + 'px'
    } else {
      // dans le cas où la valeur en laquelle s’annule la fonction est à donner'
      const elt = j3pAffiche(zoneNulle, '', '&1&', { inputmq1: { texte: '', correction: valZero } })
      elt.inputmqList[0].leSpan = elt.parent.parentNode
      stor.zoneInput.push(elt.inputmqList[0])
      zoneNulle.style.left = (largeurSignede + (L - largeurSignede) / 2 - elt.inputmqList[0].getBoundingClientRect().width / 2) + 'px'
      zoneNulle.style.top = (h / 4 - elt.inputmqList[0].getBoundingClientRect().height / 2) + 'px'
      elt.inputmqList[0].addEventListener('input', function () {
        zoneNulle.style.left = (largeurSignede + (L - largeurSignede) / 2 - stor.zoneInput[0].getBoundingClientRect().width / 2) + 'px'
        zoneNulle.style.top = (h / 4 - stor.zoneInput[0].getBoundingClientRect().height / 2) + 'px'
      })
    }
    // barre verticale dans la deuxième ligne :
    j3pCreeSegment(svg, { x1: largeurSignede + (L - largeurSignede) / 2, y1: h / 2, x2: largeurSignede + (L - largeurSignede) / 2, y2: h, couleur: styleTxt.color, epaisseur: 1 })
    if (zeroTab[1]) {
      // on donne le signe de la fonction
      // le zéro
      const zeroFct = j3pAddElt(nomdiv, 'div', '0', { style: styleTxt })
      j3pStyle(zeroFct, { position: 'absolute' })
      zeroFct.style.left = largeurSignede + (L - largeurSignede) / 2 - zeroFct.getBoundingClientRect().width / 2 + 'px'
      zeroFct.style.top = 3 * h / 4 - zeroFct.getBoundingClientRect().height / 2 + 'px'
      // les signes
      for (let i = 1; i <= 2; i++) {
        const signeFct = j3pAddElt(nomdiv, 'div', '', { style: styleTxt })
        j3pStyle(signeFct, { position: 'absolute' })
        j3pAffiche(signeFct, '', '$' + signeTabCorr[i - 1] + '$')
        signeFct.style.left = largeurSignede + (2 * i - 1) * (L - largeurSignede) / 4 - signeFct.getBoundingClientRect().width / 2 + 'px'
        signeFct.style.top = 3 * h / 4 - signeFct.getBoundingClientRect().height / 2 + 'px'
      }
    } else {
      // le signe de la fonction est à compléter
      // emplacement pour le premier signe
      stor.zoneSigne = []
      const signe1 = j3pAddElt(nomdiv, 'div', '', { style: styleTxt })
      j3pStyle(signe1, { position: 'absolute' })
      const elt1 = j3pAffiche(signe1, '', '&2&', { inputmq2: { texte: '', maxchars: '1', restrict: /[+-]/ } })
      mqRestriction(elt1.inputmqList[0], '+-')
      signe1.style.left = (largeurSignede + (L - largeurSignede) / 4 - elt1.inputmqList[0].getBoundingClientRect().width / 2) + 'px'
      signe1.style.top = (h / 2 + h / 4 - elt1.inputmqList[0].getBoundingClientRect().height / 2) + 'px'
      // ajustement de la position de ce premier signe
      stor.zoneSigne.push(elt1.inputmqList[0])
      elt1.inputmqList[0].addEventListener('input', function () {
        signe1.style.left = (largeurSignede + (L - largeurSignede) / 4 - stor.zoneSigne[0].getBoundingClientRect().width / 2) + 'px'
        signe1.style.top = (h / 2 + h / 4 - stor.zoneSigne[0].getBoundingClientRect().height / 2) + 'px'
      })
      // emplacement pour le second signe
      const signe2 = j3pAddElt(nomdiv, 'div', '', { style: styleTxt })
      j3pStyle(signe2, { position: 'absolute' })
      const elt2 = j3pAffiche(signe2, '', '&3&', { inputmq3: { texte: '', maxchars: '1', restrict: /[+-]/ } })
      mqRestriction(elt2.inputmqList[0], '+\\-')
      signe2.style.left = (largeurSignede + 3 * (L - largeurSignede) / 4 - elt2.inputmqList[0].getBoundingClientRect().width / 2) + 'px'
      signe2.style.top = (h / 2 + h / 4 - elt2.inputmqList[0].getBoundingClientRect().height / 2) + 'px'
      // ajustement de la position de ce second signe
      stor.zoneSigne.push(elt2.inputmqList[0])
      elt2.inputmqList[0].addEventListener('input', function () {
        signe2.style.left = (largeurSignede + 3 * (L - largeurSignede) / 4 - stor.zoneSigne[1].getBoundingClientRect().width / 2) + 'px'
        signe2.style.top = (h / 2 + h / 4 - stor.zoneSigne[1].getBoundingClientRect().height / 2) + 'px'
      })
      if (!ds.zerodonne) stor.zoneSigne.forEach(eltInput => eltInput.addEventListener('focusin', () => j3pEmpty(stor.laPalette)))
      // liste déroulante
      const laListe = j3pAddElt(nomdiv, 'div', '', { style: styleTxt })
      j3pStyle(laListe, { position: 'absolute' })
      const elt3 = j3pAffiche(laListe, '', '#1#', { liste1: { texte: ['|', '0'], correction: 1 } })
      laListe.style.left = (largeurSignede + (L - largeurSignede) / 2 - 8) + 'px'
      laListe.style.top = (h - h / 4 - elt3.parent.getBoundingClientRect().height / 2) + 'px'
      stor.zoneSelect = elt3.selectList[0]
    }
  }
  function choixStyle (reponseCorrecte) {
    return (reponseCorrecte) ? me.styles.cbien : me.styles.cfaux
  }
  function afficheCorrection (bonneReponse) {
    j3pDetruit(stor.laPalette)
    // on affiche la correction
    // on cherche la position de la zone de correction en y
    stor.conteneurCorr = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px' } })
    stor.conteneurCorr.style.color = (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
    for (let i = 1; i <= 2; i++) stor['zoneCorr' + i] = j3pAddElt(stor.conteneurCorr, 'div')
    if ((ds.type_sol === 'inequation') || (ds.type_sol === 'inéquation') || (ds.type_sol === 'inéquations') || (ds.type_sol === 'inequations')) {
      if (me.stockage[20] < 0) {
        j3pAffiche(stor.zoneCorr1, '', textes.corr5, { a: me.stockage[22] + '>0', b: 'x<' + me.stockage[23], c: stor.ineq1 })
        j3pAffiche(stor.zoneCorr2, '', textes.corr6, { b: me.stockage[22] + '>0', c: me.stockage[23], d: me.stockage[22] + '<0' })
      } else {
        j3pAffiche(stor.zoneCorr1, '', textes.corr5, { a: me.stockage[22] + '>0', b: 'x>' + me.stockage[23], c: stor.ineq1 })
        j3pAffiche(stor.zoneCorr2, '', textes.corr7, { b: me.stockage[22] + '<0', c: me.stockage[23], d: me.stockage[22] + '>0' })
      }
    } else {
      if (stor.simplif_rep) {
        j3pAffiche(stor.zoneCorr1, '', textes.corr1, { a: me.stockage[22] + '=0', b: 'x=' + stor.quotient_rep + '=' + me.stockage[23] })
      } else {
        j3pAffiche(stor.zoneCorr1, '', textes.corr1, { a: me.stockage[22] + '=0', b: 'x=' + me.stockage[23] })
      }
      if (me.stockage[20] < 0) {
        j3pAffiche(stor.zoneCorr2, '', textes.corr2, { a: stor.coefALatex + '<0', b: me.stockage[22] + '>0', c: me.stockage[23], d: me.stockage[22] + '<0' })
      } else {
        j3pAffiche(stor.zoneCorr2, '', textes.corr3, { a: stor.coefALatex + '>0', b: me.stockage[22] + '<0', c: me.stockage[23], d: me.stockage[22] + '>0' })
      }
    }
    if (!bonneReponse) {
      const zoneCorr3 = j3pAddElt(stor.conteneurCorr, 'div')
      j3pAffiche(zoneCorr3, '', textes.corr4)
      const leTableauCorr = j3pAddElt(stor.conteneurCorr, 'div', '', { style: me.styles.etendre('petit.correction', { position: 'relative', color: me.styles.petit.correction.color, top: 0, left: 0 }) })
      tableauSignesbis(leTableauCorr, me.stockage[22], stor.larg_tab, 100, [true, true], me.stockage[23], [me.stockage[1], me.stockage[2]], me.styles.toutpetit.correction)
    }
  }
  function estChiffre (a) {
    // on souhaite savoir si le caractère a est un chiffre
    return ((a === 0) || (a === 1) || (a === 2) || (a === 3) || (a === 4) || (a === 5) || (a === 6) || (a === 7) || (a === 8) || (a === 9))
  }

  function transformationLatex (express) {
    let express1 = ''
    const chaineRacinefct = /(-?racine\([0-9.,]+\))/ig
    const expressRacineTab = express.split(chaineRacinefct)
    for (let i = 0; i < expressRacineTab.length; i++) {
      if (expressRacineTab[i].indexOf('racine') > -1) {
        // on a une racine carrée
        const expressRacine2 = /racine\(/ig
        const modifExpressRacine1 = expressRacineTab[i].replace(expressRacine2, '\\sqrt{')
        const expressRacine3 = /\)/ig
        express1 += modifExpressRacine1.replace(expressRacine3, '}')
      } else {
        express1 += expressRacineTab[i]
      }
    }
    me.logIfDebug('express1 après racine:' + express1)
    // on remplace .../... par \\frac{...}{...}
    const chaineFrac = /\(?([0-9,.]+\/[0-9,.]+)\)?/ig
    const tabExpressfraction2 = express1.split(chaineFrac)
    me.logIfDebug('tabExpressfraction2:' + tabExpressfraction2)
    let expressLatex = ''
    for (let i = 0; i < tabExpressfraction2.length; i++) {
      if (tabExpressfraction2[i].indexOf('/') > -1) {
        // on a une fraction
        const expressFrac = tabExpressfraction2[i].split('/')
        expressLatex += '\\frac{' + expressFrac[0] + '}{' + expressFrac[1] + '}'
      } else {
        expressLatex += tabExpressfraction2[i]
      }
    }
    return expressLatex
  }
  function simplificationQuotientLatex (num, den) {
    // ici num et den ne sont pas seulement des nombres entiers
    let i
    let simplRacine
    const presenceSomme = ((num.includes('+')) || (den.includes('+')))
    const presenceDifference = (((num.includes('-')) && (num.split('-')[0] !== '')) || ((den.includes('-')) && (den.split('-')[0] !== '')))
    let nbRacineDen, resteRacineDen
    if (presenceSomme || presenceDifference) {
      // là je ne vois pas trop comment je peux simplifier donc je balance le quotient sans simplification'
      if ((den.charAt(0) === '(') && (den.charAt(den.length - 1) === ')')) {
        simplRacine = '\\frac{' + num + '}{' + den.substring(1, den.length - 1) + '}'
      } else {
        simplRacine = '\\frac{' + num + '}{' + den + '}'
      }
    } else {
      // on gère d’abord la présence de racines carrées'
      const chaineRac = /racine\([0-9]+\)/ig
      // chaineRac permet de récupérer ce dont on prend la racine carrée
      let signeNum = '+'
      let signeDen = '+'
      let signeQuotient = 'positif'
      if (num.indexOf('racine') > -1) {
        const nbRacineNum = num.match(chaineRac)
        const resteRacineNum = num.split(chaineRac)
        if (resteRacineNum[0].charAt(0) === '-') {
          signeNum = '-'
          resteRacineNum[0] = resteRacineNum[0].substring(1, resteRacineNum[0].length)
        }
        // on enlève les parenthèse si elles existent encore
        if ((resteRacineNum[0].charAt(0) === '(') && (resteRacineNum[0].charAt(resteRacineNum[0].length - 1) === ')')) {
          resteRacineNum[0] = resteRacineNum[0].substring(1, resteRacineNum[0].length - 1)
        }
        me.logIfDebug('resteRacineNum:' + resteRacineNum)
        let valRacineNum = 1
        for (i = 0; i < nbRacineNum.length; i++) {
          nbRacineNum[i] = nbRacineNum[i].substring(7, nbRacineNum[i].length - 1)
          valRacineNum = valRacineNum * j3pNombre(nbRacineNum[i])
        }
        me.logIfDebug('nbRacineNum:' + nbRacineNum + '   valRacineNum:' + valRacineNum)
        if (den.indexOf('racine') > -1) {
          // on a une racine carrée au numérateur et au dénominateur
          nbRacineDen = den.match(chaineRac)
          resteRacineDen = den.split(chaineRac)
          if (resteRacineDen[0].charAt(0) === '-') {
            signeDen = '-'
            resteRacineDen[0] = resteRacineDen[0].substring(1, resteRacineDen[0].length)
          }
          // on enlève les parenthèses éventuelles
          if ((resteRacineDen[0].charAt(0) === '(') && (resteRacineDen[0].charAt(resteRacineDen[0].length - 1) === ')')) {
            resteRacineDen[0] = resteRacineDen[0].substring(1, resteRacineDen[0].length - 1)
          }
          if (signeNum !== signeDen) signeQuotient = 'negatif'
          me.logIfDebug('resteRacineDen:' + resteRacineDen)
          let valRacineDen = 1
          for (i = 0; i < nbRacineDen.length; i++) {
            nbRacineDen[i] = nbRacineDen[i].substring(7, nbRacineDen[i].length - 1)
            valRacineDen = valRacineDen * j3pNombre(nbRacineDen[i])
          }
          me.logIfDebug('nbRacineDen:' + nbRacineDen + '   valRacineDen:' + valRacineDen)
          if (Math.abs(Math.round(Math.sqrt(valRacineNum / valRacineDen)) - Math.sqrt(valRacineNum / valRacineDen)) < Math.pow(10, -12)) {
            // la racine carrée se simplifie et va disparaitre
            simplRacine = String(Math.sqrt(valRacineNum / valRacineDen))
          } else if (Math.abs(Math.round(valRacineNum / valRacineDen) - (valRacineNum / valRacineDen)) < Math.pow(10, -12)) {
            // on simplifie le quotient, mais la racine carrée demeure
            simplRacine = '\\sqrt{' + String(valRacineNum / valRacineDen) + '}'
          } else {
            // on vérifie si on peut simplifier le quotient
            if (((Math.abs(Math.round(valRacineNum) - valRacineNum) < Math.pow(10, -12))) && ((Math.abs(Math.round(valRacineDen) - valRacineDen) < Math.pow(10, -12)))) {
              // j’ai bien deux entiers'
              const pgcdQuotient = j3pPGCD(Math.abs(valRacineNum), Math.abs(valRacineDen))
              simplRacine = '\\sqrt{\\frac{' + String(valRacineNum / pgcdQuotient) + '}{' + String(valRacineDen / pgcdQuotient) + '}}'
            }
          }
          // gestion du quotient en dehors de la racine carrée
          if (resteRacineNum[0] === '') resteRacineNum[0] = '1'
          if (resteRacineDen[0] === '') resteRacineDen[0] = '1'
          if (Math.abs(Math.round(Math.sqrt(valRacineNum / valRacineDen)) - Math.sqrt(valRacineNum / valRacineDen)) < Math.pow(10, -12)) {
            if (resteRacineNum[0] === '') {
              if (resteRacineDen[0] !== '') {
                resteRacineNum[0] = simplRacine
                simplRacine = simpQuotientFractions(resteRacineNum[0], resteRacineDen[0])
              }
            } else {
              // si resteRacineNum[0] est une fraction, je dois multiplier le numérateur par simplRacine
              resteRacineNum[0] = simpQuotientFractions(resteRacineNum[0], '1/' + simplRacine)
              if (resteRacineDen[0] !== '') {
                simplRacine = simpQuotientFractions(resteRacineNum[0], resteRacineDen[0])
              } else {
                simplRacine = resteRacineNum[0]
              }
            }
          } else {
            simplRacine = simpQuotientFractions(resteRacineNum[0], resteRacineDen[0]) + simplRacine
          }
          simplRacine = transformationLatex(simplRacine)
        } else {
          if (resteRacineNum[0] === '') resteRacineNum[0] = '1'
          if (den.charAt(0) === '-') {
            signeDen = '-'
            den = den.substring(1, den.length)
          }
          if ((den.charAt(0) === '(') && (den.charAt(den.length - 1) === ')')) {
            den = den.substring(1, den.length - 1)
          }
          if (signeNum !== signeDen) signeQuotient = 'negatif'
          // il y a une racine carrée au numérateur, mais pas au dénominateur
          simplRacine = simpQuotientFractions(resteRacineNum[0], den) + '\\sqrt{' + nbRacineNum[0] + '}'
        }
        if (signeQuotient === 'negatif') {
          simplRacine = '-' + simplRacine
        }
        simplRacine = transformationLatex(simplRacine)
      } else {
        if (den.indexOf('racine') > -1) {
          // on a une racine carrée au numérateur et au dénominateur
          nbRacineDen = den.match(chaineRac)
          resteRacineDen = den.split(chaineRac)

          me.logIfDebug('resteRacineDen:' + resteRacineDen)
          if (resteRacineDen[0].charAt(0) === '-') {
            signeDen = '-'
            resteRacineDen[0] = resteRacineDen[0].substring(1, resteRacineDen[0].length)
          }
          // on enlève les parenthèses éventuelles
          if ((resteRacineDen[0].charAt(0) === '(') && (resteRacineDen[0].charAt(resteRacineDen[0].length - 1) === ')')) {
            resteRacineDen[0] = resteRacineDen[0].substring(1, resteRacineDen[0].length - 1)
          }
          if (resteRacineDen[0] === '') resteRacineDen[0] = '1'
          if ((num.charAt(0) === '(') && (num.charAt(num.length - 1) === ')')) {
            num = num.substring(1, num.length - 1)
          }
          if (signeNum !== signeDen) signeQuotient = 'negatif'
          // il y a une racine carrée au numérateur, mais pas au dénominateur
          simplRacine = simpQuotientFractions(num, resteRacineDen[0]) + '\\frac{1}{' + nbRacineDen[0] + '}'
          if (signeQuotient === 'negatif') simplRacine = '-' + simplRacine
        } else {
          if (num.charAt(0) === '-') {
            signeNum = '-'
            num = num.substring(1, num.length)
          }
          if (den.charAt(0) === '-') {
            signeDen = '-'
            den = den.substring(1, den.length)
          }
          if ((den.charAt(0) === '(') && (den.charAt(den.length - 1) === ')')) {
            den = den.substring(1, den.length - 1)
          }
          if ((num.charAt(0) === '(') && (num.charAt(num.length - 1) === ')')) {
            num = num.substring(1, num.length - 1)
          }
          if (num === '') num = '1'
          if (den === '') den = '1'
          if (signeNum !== signeDen) signeQuotient = 'negatif'
          simplRacine = simpQuotientFractions(num, den)
          if (simplRacine === '') simplRacine = '1'
          if (signeQuotient === 'negatif') simplRacine = '-' + simplRacine
        }
        simplRacine = transformationLatex(simplRacine)
      }
    }
    return simplRacine
  }
  function simpQuotientFractions (frac1, frac2) {
    // frac1 et frac2 sont de la forme a/b au format string ou bien a, toujours au format string
    let newNum
    let newDen
    let frac1Num, frac1Den, frac2Num, frac2Den
    if (frac1.indexOf('/') > -1) {
      frac1Num = j3pNombre(frac1.split('/')[0])
      frac1Den = j3pNombre(frac1.split('/')[1])
      if (frac2.indexOf('/') > -1) {
        frac2Num = j3pNombre(frac2.split('/')[0])
        frac2Den = j3pNombre(frac2.split('/')[1])
        newNum = frac1Num * frac2Den
        newDen = frac2Num * frac1Den
      } else {
        // ici frac2 est un nombre
        newNum = frac1Num
        newDen = j3pNombre(frac2) * frac1Den
      }
    } else {
      // ici frac1 est un nombre
      if (frac2.includes('/')) {
        frac2Num = j3pNombre(frac2.split('/')[0])
        frac2Den = j3pNombre(frac2.split('/')[1])
        newNum = j3pNombre(frac1) * frac2Den
        newDen = frac2Num
      } else {
        // frac2 est aussi un nombre
        newNum = j3pNombre(frac1)
        newDen = j3pNombre(frac2)
      }
    }
    if (Math.abs(newNum / newDen - Math.round(newNum / newDen)) < Math.pow(10, -12)) {
      // le quotient est un entier
      // s’il est égal à 1'
      if (Math.abs(newNum / newDen - 1) < Math.pow(10, -12)) {
        return ''
      } else if (Math.abs(newNum / newDen + 1) < Math.pow(10, -12)) {
        return '-'
      } else {
        return String(newNum / newDen)
      }
    } else {
      const pgcdQuot = j3pPGCD(Math.abs(newNum), Math.abs(newDen))
      if (newNum * newDen < 0) {
        return '-' + String(Math.abs(newNum / pgcdQuot)) + '/' + String(Math.abs(newDen / pgcdQuot))
      } else {
        return String(Math.abs(newNum / pgcdQuot)) + '/' + String(Math.abs(newDen / pgcdQuot))
      }
    }
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    let k
    const tabIntervalle = []
    if (texteIntervalle.includes('|')) {
      const tabA = texteIntervalle.split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (testIntervalleFermeEntiers.test(tabA[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(tabA[k])
        } else {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    } else {
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (testIntervalleFermeEntiers.test(texteIntervalle)) tabIntervalle.push(texteIntervalle)
        else tabIntervalle.push(intervalleDefaut)
      }
    }
    return tabIntervalle
  }

  function enonceMain () {
    if (stor.coefA[me.questionCourante - 1] === '[0;0]') {
      j3pShowError('le coefficient a ne peut être nul', { vanishAfter: 5 })
      stor.coefA[me.questionCourante - 1] = '[-10;10]'
    }
    let solEq, solEqCorr, coefA, coefB, lePgcd, arbreCalc
    let coefALatex
    // choix des coefs a et b
    let tabRacine
    if (stor.imposer_fct[me.questionCourante - 1] !== '') {
      if (stor.imposer_fct[me.questionCourante - 1] === 'ax') {
        do {
          const [borneInf, borneSup] = j3pGetBornesIntervalle(stor.coefA[me.questionCourante - 1])
          coefA = j3pGetRandomInt(borneInf, borneSup)
        } while ((Math.abs(coefA) < Math.pow(10, -12)) || (Math.abs(coefA - 1) < Math.pow(10, -12)))
        stor.imposer_fct[me.questionCourante - 1] = j3pMonome(1, 1, coefA)
        stor.simplif_rep = false
      } else if (stor.imposer_fct[me.questionCourante - 1] === 'x+b') {
        do {
          const [borneInf, borneSup] = j3pGetBornesIntervalle(ds.b)
          coefB = j3pGetRandomInt(borneInf, borneSup)
        } while (Math.abs(coefB) < Math.pow(10, -12))
        stor.imposer_fct[me.questionCourante - 1] = 'x' + j3pMonome(2, 0, coefB)
        stor.simplif_rep = false
      }
      let imposerFctNew = ''
      if ((stor.imposer_fct[me.questionCourante - 1].indexOf('racine') > -1) || (stor.imposer_fct[me.questionCourante - 1].indexOf('/') > -1)) {
        stor.simplif_rep = true
        // si j’ai ...racine(...), il faut que je mette un signe de multiplication avant racine'
        const chaineRacinenb = /([0-9)]racine\([0-9.,]+\))/ig
        const tabRacinenb = stor.imposer_fct[me.questionCourante - 1].split(chaineRacinenb)

        me.logIfDebug('tabRacinenb:' + tabRacinenb)
        for (let i = 0; i < tabRacinenb.length; i++) {
          const posRacine = tabRacinenb[i].indexOf('racine(')
          if ((posRacine > 0) && (tabRacinenb[i].charAt(posRacine - 1) !== '-') && (tabRacinenb[i].charAt(posRacine - 1) !== '+')) {
            // ici on ajoute "*" avant racine(...)
            imposerFctNew += tabRacinenb[i].split('racine')[0] + '*racine' + tabRacinenb[i].split('racine')[1]
          } else {
            imposerFctNew += tabRacinenb[i]
          }
        }
        me.logIfDebug('imposerFctNew après modif :' + imposerFctNew)
        const chaineRacine = /(-?racine\([0-9.,]+\))/ig
        tabRacine = stor.imposer_fct[me.questionCourante - 1].split(chaineRacine)
        // console.log("tabRacine:"+tabRacine)
        // on cherche les coefficients a (écrit au format latex) et b
        let coefAInit
        let coefBInit
        let coefBInitOppose
        if (stor.imposer_fct[me.questionCourante - 1].indexOf('x') === stor.imposer_fct[me.questionCourante - 1].length - 1) {
          // dans ce cas x est le dernier caractère (fonction écrite sous la forme b+ax)
          const dernierePosPlus = stor.imposer_fct[me.questionCourante - 1].lastIndexOf('+')
          const dernierePosMoins = stor.imposer_fct[me.questionCourante - 1].lastIndexOf('-')
          coefAInit = stor.imposer_fct[me.questionCourante - 1].substring(Math.max(dernierePosPlus, dernierePosMoins) + 1, stor.imposer_fct[me.questionCourante - 1].length - 1)
          if (dernierePosMoins > dernierePosPlus) {
            // on ajoute un signe moins
            coefAInit = '-' + coefAInit
            coefBInit = stor.imposer_fct[me.questionCourante - 1].substring(0, Math.max(dernierePosPlus, dernierePosMoins))
          }
        } else {
          // dans ce cas l’expression est sous la forme ax+b'
          const posX = stor.imposer_fct[me.questionCourante - 1].indexOf('x')
          coefAInit = stor.imposer_fct[me.questionCourante - 1].substring(0, posX)
          if (stor.imposer_fct[me.questionCourante - 1].charAt(posX + 1) === '+') {
            coefBInit = stor.imposer_fct[me.questionCourante - 1].substring(posX + 2, stor.imposer_fct[me.questionCourante - 1].length)
          } else {
            coefBInit = stor.imposer_fct[me.questionCourante - 1].substring(posX + 1, stor.imposer_fct[me.questionCourante - 1].length)
          }
        }
        if (coefBInit.charAt(0) === '-') {
          coefBInitOppose = coefBInit.substring(1, coefBInit.length)
        } else {
          coefBInitOppose = '-' + coefBInit
        }
        me.logIfDebug('coefAInit:' + coefAInit + '   coefBInit:' + coefBInit)
        coefALatex = transformationLatex(coefAInit)
        const coefBOpposeLatex = transformationLatex(coefBInitOppose)
        stor.quotient_rep = '\\frac{' + coefBOpposeLatex + '}{' + coefALatex + '}'
        // on cherche le zéro de la fonction affine écrit au format latex
        solEqCorr = simplificationQuotientLatex(coefBInitOppose, coefAInit)
        stor.ineq1 = coefALatex + 'x>' + coefBInitOppose
        stor.ineq2 = coefALatex + 'x<' + coefBInitOppose
        me.logIfDebug('coefALatex:' + coefALatex + '    coefBOpposeLatex:' + coefBOpposeLatex + '   solEqCorr:' + solEqCorr)
      } else {
        arbreCalc = new Tarbre(stor.imposer_fct[me.questionCourante - 1], ['x'])
        coefA = arbreCalc.evalue([1]) - arbreCalc.evalue([0])
        coefB = arbreCalc.evalue([0])
        coefALatex = coefA
        // coefB_latex = coefB;
        if (Math.abs(coefB / coefA - Math.round(coefB / coefA)) < Math.pow(10, -12)) {
          // la racine est un entier
          solEqCorr = String(-coefB / coefA)
        } else {
          lePgcd = j3pPGCD(Math.abs(coefB), Math.abs(coefA))
          if (coefA * coefB < 0) {
            solEqCorr = '\\frac{' + String(Math.abs(coefB) / lePgcd) + '}{' + String(Math.abs(coefA) / lePgcd) + '}'
          } else {
            solEqCorr = '\\frac{' + String(-Math.abs(coefB) / lePgcd) + '}{' + String(Math.abs(coefA) / lePgcd) + '}'
          }
        }
        imposerFctNew = stor.imposer_fct[me.questionCourante - 1]
        stor.ineq1 = coefALatex + 'x>' + (-coefB)
        stor.ineq2 = coefALatex + 'x<' + (-coefB)
      }
      me.logIfDebug('imposerFctNew:' + imposerFctNew)
      arbreCalc = new Tarbre(imposerFctNew, ['x'])
      coefA = arbreCalc.evalue([1]) - arbreCalc.evalue([0])
      coefB = arbreCalc.evalue([0])
      solEq = -coefB / coefA
      me.logIfDebug('coefA:' + coefA + '   coefB:' + coefB + '    ' + solEq)
    } else {
      if (ds.zeroentier) {
        do {
          const [borneInf, borneSup] = j3pGetBornesIntervalle(stor.coefA[me.questionCourante - 1])
          coefA = j3pGetRandomInt(borneInf, borneSup)
        } while ((Math.abs(coefA) < Math.pow(10, -13)) || (Math.abs(coefA - 1) < Math.pow(10, -12)))
        // j’ai empêché coefA de prendre les valeurs 0 ou 1
        solEq = j3pGetRandomInt(-6, 6)
        coefB = -coefA * solEq
        if (Math.abs(coefA + 1) < Math.pow(10, -12)) {
          stor.simplif_rep = false
        } else {
          stor.simplif_rep = true
          stor.quotient_rep = '\\frac{' + String(-coefB) + '}{' + String(coefA) + '}'
        }
        // solEqCorr sert pour la correction
        solEqCorr = String(solEq)
      } else {
        let valAbsCoefaEgal1, solutionEq
        do {
          valAbsCoefaEgal1 = false
          do {
            const [borneInf, borneSup] = j3pGetBornesIntervalle(stor.coefA[me.questionCourante - 1])
            coefA = j3pGetRandomInt(borneInf, borneSup)
            if ((stor.coefA[me.questionCourante - 1] === '[-1;-1]') || (stor.coefA[me.questionCourante - 1] === '[-1;1]') || (stor.coefA[me.questionCourante - 1] === '[1;1]')) {
              valAbsCoefaEgal1 = true
            }
          } while (Math.abs(coefA) < Math.pow(10, -13))
          const [borneInf, borneSup] = j3pGetBornesIntervalle(ds.b)
          coefB = j3pGetRandomInt(borneInf, borneSup)
          solutionEq = -coefB / coefA
        } while ((Math.abs(solutionEq - Math.round(solutionEq)) < 0.00001) && !valAbsCoefaEgal1)
        lePgcd = j3pPGCD(Math.abs(coefB), Math.abs(coefA))
        if (Math.abs(lePgcd - 1) < Math.pow(10, -12)) {
          stor.simplif_rep = false
        } else {
          stor.simplif_rep = true
          stor.quotient_rep = '\\frac{' + String(-coefB) + '}{' + String(coefA) + '}'
        }
        if (Math.abs(Math.round(-coefB / coefA) + coefB / coefA) < Math.pow(10, -12)) {
          solEq = String(Math.round(-coefB / coefA))
          solEqCorr = solEq
        } else {
          if (coefA * coefB < 0) {
            solEq = String(Math.abs(coefB) / lePgcd) + '/' + String(Math.abs(coefA) / lePgcd)
            solEqCorr = '\\frac{' + String(Math.abs(coefB) / lePgcd) + '}{' + String(Math.abs(coefA) / lePgcd) + '}'
          } else {
            solEq = String(-Math.abs(coefB) / lePgcd) + '/' + String(Math.abs(coefA) / lePgcd)
            solEqCorr = '\\frac{' + String(-Math.abs(coefB) / lePgcd) + '}{' + String(Math.abs(coefA) / lePgcd) + '}'
          }
        }
      }
      coefALatex = coefA
      stor.ineq1 = j3pMonome(1, 1, coefA) + '>' + (-coefB)
      stor.ineq2 = j3pMonome(1, 1, coefA) + '<' + (-coefB)
    }
    const solEqText = solEq
    me.logIfDebug('solEq : ' + solEq)
    let expressAffine
    let choixAlea
    if (stor.imposer_fct[me.questionCourante - 1] !== '') {
      if (stor.imposer_fct[me.questionCourante - 1] === 'ax') {
        expressAffine = j3pMonome(1, 1, coefA)
      } else if (stor.imposer_fct[me.questionCourante - 1] === 'x+b') {
        choixAlea = j3pGetRandomInt(0, 1)
        if (choixAlea === 0) {
          expressAffine = j3pMonome(1, 1, 1) + j3pMonome(2, 0, coefB)
        } else {
          expressAffine = j3pMonome(1, 0, coefB) + j3pMonome(2, 1, 1)
        }
      } else if ((stor.imposer_fct[me.questionCourante - 1].indexOf('racine') > -1) || (stor.imposer_fct[me.questionCourante - 1].indexOf('/') > -1)) {
        let expressAffine1 = ''
        // on remplace racine(...) par \\sqrt{...}
        for (let i = 0; i < tabRacine.length; i++) {
          if (tabRacine[i].indexOf('racine') > -1) {
            // on a une racine carrée
            const chaineRacine2 = /racine\(/ig
            const modifRacine1 = tabRacine[i].replace(chaineRacine2, '\\sqrt{')
            const chaineRacine3 = /\)/ig
            expressAffine1 += modifRacine1.replace(chaineRacine3, '}')
          } else {
            expressAffine1 += tabRacine[i]
          }
        }
        me.logIfDebug('expressAffine1 après racine:' + expressAffine1)
        // on remplace .../... par \\frac{...}{...}
        const chaineFraction = /\(?([0-9,.]+\/[0-9,.]+)\)?/ig
        const tabFraction2 = expressAffine1.split(chaineFraction)
        me.logIfDebug('tabFraction2:' + tabFraction2)
        expressAffine = ''
        for (let i = 0; i < tabFraction2.length; i++) {
          if (tabFraction2[i].indexOf('/') > -1) {
            // on a une fraction
            const tabFrac = tabFraction2[i].split('/')
            expressAffine += '\\frac{' + tabFrac[0] + '}{' + tabFrac[1] + '}'
          } else {
            expressAffine += tabFraction2[i]
          }
        }
        me.logIfDebug('expressAffine après fraction:' + expressAffine)
      } else {
        expressAffine = stor.imposer_fct[me.questionCourante - 1]
      }
    } else {
      let ordreAffine
      if (stor.ordreAffine[me.questionCourante - 1] === 'alea') {
        ordreAffine = (j3pGetRandomInt(0, 1) === 0) ? 'ax+b' : 'b+ax'
      } else {
        ordreAffine = stor.ordreAffine[me.questionCourante - 1]
      }
      expressAffine = (ordreAffine === 'ax+b')
        ? j3pMonome(1, 1, coefA) + j3pMonome(2, 0, coefB)
        : j3pMonome(1, 0, coefB) + j3pMonome(2, 1, coefA)
    }
    // si le signe "*" est présent, on le remplace par une vraie multiplication"
    me.logIfDebug('expressAffine:' + expressAffine)

    while (expressAffine.indexOf('*') > -1) {
      expressAffine = expressAffine.replace('*', '\\times ')
    }
    me.logIfDebug('modif multiplication expressAffine:' + expressAffine)
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingBottom: '10px' } })
    j3pAffiche(zoneCons1, '', textes.consigne1, { a: expressAffine })
    const leTableau1 = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.enonce', { position: 'relative', top: 0, left: 0 }) })
    me.stockage[0] = solEq // première réponse (endroit où s’annule la fonction)
    if (coefA < 0) {
      me.stockage[1] = '+' // stockage des signes réponses
      me.stockage[2] = '-'
    } else {
      me.stockage[1] = '-'
      me.stockage[2] = '+'
    }
    stor.larg_tab = Math.min(95 * me.zonesElts.HG.getBoundingClientRect().width / 100, 600)
    tableauSignesbis(leTableau1, expressAffine, stor.larg_tab, 100, [ds.zerodonne, false], solEqText, [me.stockage[1], me.stockage[2]], me.styles.toutpetit.enonce)
    // stockage de données pour les récupérer dans la correction
    me.stockage[20] = coefA
    me.stockage[21] = coefB
    me.stockage[22] = expressAffine
    me.stockage[23] = solEqCorr
    stor.coefALatex = coefALatex
    // stor.coefB_latex = coefB_latex;
    // construction de la palette
    if (ds.zerodonne) {
      j3pFocus(stor.zoneSigne[0])
    } else {
      stor.laPalette = j3pAddElt(stor.conteneur, 'div')
      stor.zoneInput[0].addEventListener('focusin', () => {
        j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], { liste: ['fraction', 'racine'] })
      })
      mqRestriction(stor.zoneInput[0], '\\d-+,./*', { commandes: ['fraction', 'racine'] })
      j3pFocus(stor.zoneInput[0])
    }
    stor.explications = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '10px' }) })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce': {
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.6 })
        me.stockage = [0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(textes.titre_exo)

        stor.coefA = constructionTabIntervalle(ds.a, '[-10;10]')
        stor.imposer_fct = []
        for (let i = 0; i < ds.nbrepetitions; i++) stor.imposer_fct.push('')
        if (ds.imposer_fct !== '') {
          // c’est qu’on souhaite imposer une ou plusieurs fonctions affines au cours des répétitions
          const tabImposerFct = ds.imposer_fct.split('|')
          for (let i = 0; i < tabImposerFct.length; i++) stor.imposer_fct[i] = tabImposerFct[i]
        }
        stor.ordreAffine = []
        for (let i = 0; i < ds.nbrepetitions; i++) stor.ordreAffine[i] = 'alea'
        if ((ds.ordre_affine === 'alea') || (ds.ordre_affine === 'ax+b') || (ds.ordre_affine === 'b+ax')) {
          for (let i = 0; i < ds.nbrepetitions; i++) stor.ordreAffine[i] = ds.ordre_affine
        } else if (ds.ordre_affine.indexOf('|') > -1) {
          const tabOrdreAffine = ds.ordre_affine.split('|')
          for (let i = 0; i < tabOrdreAffine.length; i++) stor.ordreAffine[i] = tabOrdreAffine[i]
        }

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        // j’ai besoin de la largeur de la colonne de gauche. Je mets ça dans stockage[50]'
        me.stockage[50] = 45 * 1200 / 100
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      enonceMain()
      break // case "enonce":
    }

    case 'correction':
      // obligatoire
      // je récupère toutes les réponses des élèves
      {
        let reponseEleve1, i
        if (!ds.zerodonne) {
          reponseEleve1 = j3pValeurde(stor.zoneInput[0])
          // gérer les cas pénibles : 3\frac{1}{2} ou \frac{1}{2}3
          // pour ceux là, xcas ne comprends pas, cela crée un erreur par la suite donc il faut ajouter la multiplication
          i = 2
          while (i < reponseEleve1.length - 1) {
            if ((reponseEleve1.charAt(i) === 'f') && estChiffre(reponseEleve1.charAt(i - 2))) {
              reponseEleve1 = reponseEleve1.substring(0, i - 1) + '*' + reponseEleve1.substring(i - 1)
              i += 1
            }
            i += 1
          }
          i = 1
          while (i < reponseEleve1.length - 1) {
            if ((reponseEleve1.charAt(i) === '}') && (estChiffre(reponseEleve1.charAt(i + 1)))) {
              reponseEleve1 = reponseEleve1.substring(0, i + 1) + '*' + reponseEleve1.substring(i + 1)
            }
            i += 1
          }
          me.logIfDebug('reponseEleve1 au début:' + reponseEleve1)
          reponseEleve1 = j3pMathquillXcas(reponseEleve1)
          me.logIfDebug('reponseEleve1:' + reponseEleve1)
        }
        const reponseEleve2 = j3pValeurde(stor.zoneSigne[0])
        const reponseEleve3 = j3pValeurde(stor.zoneSigne[1])
        const reponseIndex1 = stor.zoneSelect.selectedIndex
        // on teste si une réponse a été saisie
        let zonesIncompletes = false
        let aRepondu
        if (ds.zerodonne) {
        // le zero a été donné dans le tableau'
          aRepondu = ((reponseEleve2 !== '') && (reponseEleve3 !== ''))
          zonesIncompletes = ((reponseEleve2 === '') || (reponseEleve3 === ''))
        } else {
          aRepondu = ((reponseEleve1 !== '') && (reponseEleve2 !== '') && (reponseEleve3 !== ''))
          zonesIncompletes = ((reponseEleve1 === '') || (reponseEleve2 === '') || (reponseEleve3 === ''))
        }
        if (!aRepondu && (!me.isElapsed)) {
          stor.explications.style.color = me.styles.cfaux
          if (zonesIncompletes) {
            stor.explications.innerHTML = textes.comment1
            if (reponseEleve3 === '') j3pFocus(stor.zoneSigne[1])
            if (reponseEleve2 === '') j3pFocus(stor.zoneSigne[0])
            if (!ds.zerodonne) {
              if (reponseEleve1 === '') j3pFocus(stor.zoneInput[0])
            }
          } else {
            stor.explications.innerHTML = textes.comment1bis
            j3pFocus(stor.zoneSelect)
          }
          return me.finCorrection()
        } else { // une réponse a été saisie
          const bonSigne = []
          let bonZero = true
          if (!ds.zerodonne) {
          // je peux très bien avoir plusieurs virgules dans mon expression
            while (reponseEleve1.includes(',')) {
              reponseEleve1 = reponseEleve1.substring(0, reponseEleve1.indexOf(',')) + '.' + reponseEleve1.substring(reponseEleve1.indexOf(',') + 1)
            }
            // le pb vient alors lorsque j’ai une fraction'
            while (reponseEleve1.indexOf('sqrt') > -1) {
              reponseEleve1 = reponseEleve1.replace('sqrt', 'racine')
            }
            const arbreDif = new Tarbre(reponseEleve1 + '-(' + me.stockage[0] + ')', ['x'])
            const madif = arbreDif.evalue([0])
            me.logIfDebug('madif:' + madif)
            bonZero = (Math.abs(madif) < Math.pow(10, -13))
          }
          bonSigne[1] = (reponseEleve2 === me.stockage[1])
          bonSigne[2] = (reponseEleve3 === me.stockage[2])
          const barreOk = (reponseIndex1 === 1)
          const cbon = (bonZero && bonSigne[1] && bonSigne[2] && barreOk)
          if (bonSigne[1] && bonSigne[2]) {
            for (i = 1; i <= 2; i++) {
              j3pStyle(stor.zoneSigne[i - 1], { color: me.styles.cbien })
              j3pDesactive(stor.zoneSigne[i - 1])
              j3pFreezeElt(stor.zoneSigne[i - 1])
            }
          }
          // on désactive les zones exactes
          if (barreOk) {
            stor.zoneSelect.disabled = true
            j3pStyle(stor.zoneSelect, { color: me.styles.cbien })
          }
          if (bonZero && !ds.zerodonne) {
            j3pStyle(stor.zoneInput[0], { color: me.styles.cbien })
            j3pDesactive(stor.zoneInput[0])
            j3pFreezeElt(stor.zoneInput[0])
          }
          me.logIfDebug(bonZero + '  ' + bonSigne[1] + '  ' + bonSigne[2] + '  ' + barreOk + '  ' + cbon)
          if (cbon) {
            me.score++
            stor.explications.style.color = me.styles.cbien
            stor.explications.innerHTML = cBien
            // affichage de l’explication
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.explications.style.color = me.styles.cfaux
            for (i = 1; i <= 2; i++) j3pStyle(stor.zoneSigne[i - 1], { color: me.styles.cfaux })
            if (!bonZero) j3pStyle(stor.zoneInput[0], { color: me.styles.cfaux })
            if (!barreOk) j3pStyle(stor.zoneSelect, { color: me.styles.cfaux })
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.explications.innerHTML = tempsDepasse
              me.typederreurs[10]++
              /*
                            *   RECOPIER LA CORRECTION ICI !
                            */
              for (i = 1; i <= 2; i++) {
                j3pDesactive(stor.zoneSigne[i - 1])
                j3pFreezeElt(stor.zoneSigne[i - 1])
              }
              j3pDesactive(stor.zoneInput[0])
              j3pFreezeElt(stor.zoneInput[0])
              j3pDesactive(stor.zoneSelect)
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // réponse fausse :
              stor.explications.innerHTML = cFaux + '<br>'
              if (!bonZero) stor.explications.innerHTML += textes.corr9 + '<br>'
              if ((!bonSigne[1]) || (!bonSigne[2])) stor.explications.innerHTML += textes.corr10 + '<br>'
              if (!barreOk) stor.explications.innerHTML += textes.corr8
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances && !bonZero) {
                stor.explications.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                if (!barreOk) stor.zoneSelect.disabled = false
                // on redonne le focus
                for (i = 2; i >= 1; i--) {
                  if (!bonSigne[i]) j3pFocus(stor.zoneSigne[i - 1])
                }
                if (!bonZero) j3pFocus(stor.zoneInput[0])
                me.finCorrection()
              } else {
              // Erreur au second essai
                me.typederreurs[2]++
                // //on bloque les zones
                stor.zoneSelect.disabled = true
                if (!bonZero && !ds.zerodonne) j3pStyle(stor.zoneInput[0], { color: me.styles.cfaux })
                for (i = 1; i <= 2; i++) {
                  j3pStyle(stor.zoneSigne[i - 1], { color: choixStyle(bonSigne[i]) })
                  j3pDesactive(stor.zoneSigne[i - 1])
                }
                // on barre les mauvaises réponses
                for (i = 1; i <= 2; i++) {
                  if (bonSigne[i]) j3pFreezeElt(stor.zoneSigne[i - 1])
                  else j3pBarre(stor.zoneSigne[i - 1])
                }
                j3pDesactive(stor.zoneInput[0])
                if (!bonZero) {
                  j3pBarre(stor.zoneInput[0])
                  me.typederreurs[4]++
                } else {
                  j3pFreezeElt(stor.zoneInput[0])
                }
                if (!barreOk) j3pBarre(stor.zoneSelect)
                if (!bonSigne[1] || !bonSigne[2]) me.typederreurs[3]++
                stor.explications.innerHTML += '<br>' + regardeCorrection
                // affichage de l’explication
                afficheCorrection(false)
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          if (me.typederreurs[4] / ds.nbitems >= ds.seuilerreur) {
            // la proportion de pb du zéro de la fonction est supé au égal au seul fixé
            me.parcours.pe = ds.pe_3
          } else if (me.typederreurs[3] / ds.nbitems >= ds.seuilerreur) {
            // la proportion de pb de signes est supé au égal au seul fixé
            me.parcours.pe = ds.pe_2
          } else {
            me.parcours.pe = ds.pe_4
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
}
