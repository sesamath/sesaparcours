import { j3pAddElt, j3pBarre, j3pVirgule, j3pFocus, j3pGetRandomInt, j3pMonome, j3pPGCD, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeRectangle, j3pCreeSegment } from 'src/legacy/core/functionsSvg'
import { fctsEtudeOrdonneTabSansDoublon } from 'src/legacy/outils/fonctions/etude'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    février 2014
    On donne le signe de fonctos affines et on demande celui du produit ou du quoient
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['nbfacteurs_affine', 2, 'entier', 'nombre de facteurs affine (supérieur ou égal à 2 et limité à 4)'],
    ['type_expres', ['produit'], 'array', '["produit"] ou ["quotient","num","num","den"] pour préciser dans le cas du quotient quels sont les facteurs au numérateur ou au dénominateur'],
    ['a', '[-10;10]', 'string', 'valeur a dans les expressions ax+b (a différent de 0)'],
    ['b', '[-10;10]', 'string', 'valeur b dans les expressions ax+b (non paramétrable si zeroentier[0] = true)']
  ]
}
const textes = {
  titre_exo1: 'Signe du produit de fonctions affines',
  titre_exo2: 'Signe du quotient de fonctions affines',
  consigne1: 'Complète le tableau ci-dessous pour obtenir le signe du produit $£a$ sur $\\R$.<br>',
  consigne2: 'Complète le tableau ci-dessous pour obtenir le signe du quotient $£a$.<br>',
  comment1: 'Il faut compléter tout le tableau&nbsp;!',
  comment1bis: 'Il faut choisir entre 0 et la barre verticale pour les listes déroulantes&nbsp;!',
  corr1: 'Pour obtenir le signe d’un produit sur un intervalle, il suffit de compter le nombre de facteurs négatifs sur cet intervalle.<br>',
  corr2: 'Par exemple, sur $£a$, il y a $£b$ facteurs négatifs. $£b$ étant un nombre impair, le produit est négatif.',
  corr3: 'Par exemple, sur $£a$, il y a $£b$ facteurs négatifs. $£b$ étant un nombre pair, le produit est positif.',
  corr4: 'Par exemple, sur $£a$, il y a $£b$ facteur négatif. $£b$ étant un nombre impair, le produit est négatif.',
  corr5: 'Par exemple, sur $£a$, il y a $£b$ facteur négatif. $£b$ étant un nombre pair, le produit est positif.',
  corr6: '<br>De plus, quand un facteur est nul, le produit est nul.',
  corr7: '<br>De plus, toute valeur de $x$ annulant le dénominateur est interdite (ce qui explique la double barre). Sinon, le quotient vaut $0$.',
  corr8: 'Ne pas oublier de préciser que la fonction s’annule !',
  corr9: 'Au moins l’un des signes est faux.',
  signeProduit: 'signe du produit',
  signeQuotient: 'signe du quotient',
  signeDe: 'signe de '
}
/**
 * section signe_prodquot
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function positionTab (tableau, elt) {
    // renvoie la position de elt dans le tableau
    // ici elt est une valeur numérique (ou s’apparente à une telle valeur)'
    // renvoie -1 si elt n’est pas présent
    for (let i = 0; i < tableau.length; i++) {
      if (Math.abs(j3pCalculValeur(elt) - j3pCalculValeur(tableau[i])) < Math.pow(10, -12)) {
        return i
      }
    }
    return -1
  }

  function tabSignesProduit (obj, expresTab, tabDimension, prodQuot, zeroTab, valZeroTab, lesSignes, objStyleCouleur) {
    // on crée un tableau de signes dans obj
    // ici il s’agit du tableau de signe d’un produit ou d’un quotient
    // expresTab est un tableau contenant tous les facteurs
    // tab_position est un tableau de 2 éléments qui contiennent la largeur L et la hauteur h du tableau
    // prodQuot vaut ["produit"] ou un tableau ["quotient",[true, false, true, ..]]
    // où true signifie que le facteur associé de expresTab est au dénominateur
    // zeroTab[0] est un booléen. Il vaut true lorsque les valeurs en lesquelles s’annulent l’expression ne sont pas à remplir par l’élève
    // zeroTab[1] est un booléen. Il vaut true lorsque les signes du tableau ne sont pas à donner par l’élève
    // zeroTab[1] ne peut pas valoir false si zeroTab[0] vaut true;
    // val_zero est la valeur en laquelle s’annule la fonction affine'
    // les signes est un paramètres optionnel si on souhaite avoir des zones de saisies pour les signes (on peut aussi mettre false)
    // si on veut compléter entièrement le tableau, lesSignes sera un tableau contenant des éléments tels que ["+","-"] ou ["-","+"]
    // pour le signe du produit, on peut demander des zones de saisie dans ce cas le dernier élément de lesSignes vaut true
    // sinon ce dernier élément vaut false
    // objStyleCouleur peut contenir deux éléments : un style de texte, couleur si on ne définit par le style
    // je crée mes textes au début - si jamais on se lance dans la traduction
    const signeProdTxt = textes.signeProduit
    const signeQuotTxt = textes.signeQuotient
    // Je vais récupérer les div existants car j’en aurai besoin pour la correction
    const objDiv = {}
    let maCouleurTxt
    if (objStyleCouleur.couleur === undefined) {
      maCouleurTxt = objStyleCouleur.style.color
    } else {
      maCouleurTxt = objStyleCouleur.couleur
    }
    const macouleur = '#000000'
    const signeDeTabTxt = []
    for (let i = 0; i < expresTab.length; i++) {
      signeDeTabTxt[i] = textes.signeDe + '$' + expresTab[i] + '$'
    }
    const L = tabDimension[0] // Largeur du tableau
    const h = tabDimension[1] // hauteur du tableau
    const nomdiv = obj
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    svg.setAttribute('width', L)
    svg.setAttribute('height', h)
    obj.appendChild(svg)
    j3pCreeRectangle(svg, {
      x: 1,
      y: 0,
      width: L - 2,
      height: h,
      couleur: macouleur,
      epaisseur: 1
    })
    for (let i = 1; i < expresTab.length + 2; i++) {
      j3pCreeSegment(svg, {
        x1: 0,
        y1: i * h / (expresTab.length + 2),
        x2: L,
        y2: i * h / (expresTab.length + 2),
        couleur: macouleur,
        epaisseur: 1
      })
    }
    // Affichage des textes "signe de ax+b"
    let largeurSignede = 0
    const divsSigneDe = []
    objDiv.divGauche = []
    for (let i = 1; i <= expresTab.length; i++) {
      divsSigneDe.push(j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style }))
      j3pStyle(divsSigneDe[divsSigneDe.length - 1], { position: 'absolute' })
      j3pAffiche(divsSigneDe[divsSigneDe.length - 1], '', signeDeTabTxt[i - 1], { style: { color: maCouleurTxt } })
      largeurSignede = Math.max(largeurSignede, divsSigneDe[divsSigneDe.length - 1].getBoundingClientRect().width + 4)
      objDiv.divGauche.push(divsSigneDe[divsSigneDe.length - 1])
    }
    // texte "signe du produit" ou "signe du quotient"
    const signeProd = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
    j3pStyle(signeProd, { position: 'absolute' })
    j3pAffiche(signeProd, '', (prodQuot[0] === 'produit') ? signeProdTxt : signeQuotTxt, { style: { color: maCouleurTxt } })
    objDiv.divGauche.push(signeProd)
    // pour placer le segment vertical, je cherche la largeur de la phrase "signe de ..."
    largeurSignede = Math.max(largeurSignede, signeProd.getBoundingClientRect().width + 4)
    j3pCreeSegment(svg, {
      x1: largeurSignede,
      y1: 0,
      x2: largeurSignede,
      y2: h,
      couleur: macouleur,
      epaisseur: 1
    })
    let posTxt
    for (let i = 1; i <= expresTab.length; i++) {
      posTxt = (i + 0.5) * h / (expresTab.length + 2) - divsSigneDe[i - 1].getBoundingClientRect().height / 2
      divsSigneDe[i - 1].style.top = posTxt + 'px'
      divsSigneDe[i - 1].style.left = '2px'
    }
    const hauteurSigneProduit = signeProd.getBoundingClientRect().height
    posTxt = (expresTab.length + 1 + 0.5) * h / (expresTab.length + 2) - hauteurSigneProduit / 2
    signeProd.style.top = posTxt + 'px'
    signeProd.style.left = '2px'
    // placement du "x"
    const leX = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
    j3pStyle(leX, { position: 'absolute' })
    j3pAffiche(leX, '', '$x$', { style: { color: maCouleurTxt } })
    const posTxt2 = h / (2 * expresTab.length + 4) - leX.getBoundingClientRect().height / 2
    leX.style.top = posTxt2 + 'px'
    const posTxt3 = largeurSignede / 2 - leX.getBoundingClientRect().width / 2
    leX.style.left = posTxt3 + 'px'
    // plus et moins l’infini'
    const moinsInf = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
    j3pStyle(moinsInf, { position: 'absolute' })
    const plusInf = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
    j3pStyle(plusInf, { position: 'absolute' })
    j3pAffiche(moinsInf, '', '$-\\infty$', { style: { color: maCouleurTxt } })
    j3pAffiche(plusInf, '', '$+\\infty$', { style: { color: maCouleurTxt } })
    objDiv.inf = [moinsInf, plusInf]
    moinsInf.style.left = (largeurSignede + 1) + 'px'
    moinsInf.style.top = (h / (2 * expresTab.length + 4) - moinsInf.getBoundingClientRect().height / 2) + 'px'
    plusInf.style.left = (me.etat === 'enonce' && me.questionCourante === 1) ? (L - 1 - plusInf.getBoundingClientRect().width * 1.3 - 2 + 1) + 'px' : (L - 1 - plusInf.getBoundingClientRect().width - 2 + 1) + 'px'
    plusInf.style.top = (h / (2 * expresTab.length + 4) - plusInf.getBoundingClientRect().height / 2) + 'px'

    const tabZeroOrdonne = fctsEtudeOrdonneTabSansDoublon(valZeroTab)
    const nbValx = tabZeroOrdonne.length
    let monZeroTxt
    // les endroits où s’annulent la fonction sont donnés
    // attention les_zeros doivent être du type string (un entier ou un décimal ou un nb fractionnaire de la forme a/b
    // dans un premier temps, on ordonne les nombre du tableau valZeroTab (qui sont ls endroits où s’annulent notre produit)
    objDiv.divZeros = []
    for (let i = 0; i < nbValx; i++) {
      const leZero = typeof tabZeroOrdonne[i] === 'string' ? tabZeroOrdonne[i] : j3pVirgule(tabZeroOrdonne[i])
      if (leZero.includes('/')) {
        const tabZeroSplit = leZero.split('/')
        monZeroTxt = '\\frac{' + tabZeroSplit[0] + '}{' + tabZeroSplit[1] + '}'
      } else {
        monZeroTxt = leZero
      }
      const zoneNulle = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
      j3pStyle(zoneNulle, { position: 'absolute' })
      j3pAffiche(zoneNulle, '', '$' + monZeroTxt + '$', { style: { color: maCouleurTxt } })
      zoneNulle.style.left = (largeurSignede + (i + 1) * (L - largeurSignede) / (nbValx + 1) - zoneNulle.getBoundingClientRect().width / 2) + 'px'
      zoneNulle.style.top = (h / (2 * expresTab.length + 4) - zoneNulle.getBoundingClientRect().height / 2) + 'px'
      objDiv.divZeros.push(zoneNulle)
    }
    // on crée tout d’abord les segments verticaux
    const posValZero = []
    for (let i = 1; i <= nbValx; i++) {
      posValZero[i] = largeurSignede + i * (L - largeurSignede) / (nbValx + 1)
      j3pCreeSegment(svg, {
        x1: posValZero[i],
        y1: h / (expresTab.length + 2),
        x2: posValZero[i],
        y2: h,
        couleur: macouleur,
        epaisseur: 1
      })
    }
    let tabTextListe
    let numLigne, numColonne

    // On est dans le cas où tout le tableau va être affiché (signes et zéros), sauf peut-être le signe du produit/quotient
    // on s’occupe dans un premier temps du signe de chaque facteur (pas celui du produit)
    // je place tous les zeros
    objDiv.zeroFacteurs = []
    for (let i = 1; i <= expresTab.length; i++) {
      if (valZeroTab[i - 1] !== '') {
        const zeroFact = j3pAddElt(nomdiv, 'p', '0', { style: objStyleCouleur.style })
        j3pStyle(zeroFact, { position: 'absolute', color: maCouleurTxt })
        const numverticale = positionTab(tabZeroOrdonne, valZeroTab[i - 1])
        zeroFact.style.left = largeurSignede + (numverticale + 1) * (L - largeurSignede) / (tabZeroOrdonne.length + 1) - zeroFact.getBoundingClientRect().width / 2 + 'px'
        zeroFact.style.top = (i + 0.5) * h / (expresTab.length + 2) - zeroFact.getBoundingClientRect().height / 2 + 'px'
        objDiv.zeroFacteurs.push(zeroFact)
      }
    }
    const nbSigne = (nbValx + 1) * expresTab.length
    // le tableau suivant va servir pour le signe du produit ou du quotient
    const nbSignesMoinsTab = []
    for (let i = 1; i <= nbValx + 1; i++) nbSignesMoinsTab[i] = 0
    objDiv.signeLigne = []
    for (let i = 1; i <= nbSigne; i++) {
      const signeI = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
      j3pStyle(signeI, { position: 'absolute' })
      // je cherche l’endroit où s’annule mon expression (si elle s’annule)
      numLigne = Math.ceil(i / (nbValx + 1))
      numColonne = (i - 1) % (nbValx + 1) + 1
      if (valZeroTab[numLigne - 1] === '') {
        // on a le même signe sur toute la ligne
        j3pAffiche(signeI, '', '$' + lesSignes[numLigne - 1][0] + '$', { style: { color: maCouleurTxt } })
        if (lesSignes[numLigne - 1][0] === '-') nbSignesMoinsTab[numColonne] += 1
      } else {
        if (tabZeroOrdonne[numColonne - 1] === undefined) {
          j3pAffiche(signeI, '', '$' + lesSignes[numLigne - 1][1] + '$', { style: { color: maCouleurTxt } })
          if (lesSignes[numLigne - 1][1] === '-') nbSignesMoinsTab[numColonne] += 1
        } else {
          if (j3pCalculValeur(valZeroTab[numLigne - 1]) >= j3pCalculValeur(tabZeroOrdonne[numColonne - 1])) {
            j3pAffiche(signeI, '', '$' + lesSignes[numLigne - 1][0] + '$', { style: { color: maCouleurTxt } })
            if (lesSignes[numLigne - 1][0] === '-') nbSignesMoinsTab[numColonne] += 1
          } else {
            j3pAffiche(signeI, '', '$' + lesSignes[numLigne - 1][1] + '$', { style: { color: maCouleurTxt } })
            if (lesSignes[numLigne - 1][1] === '-') nbSignesMoinsTab[numColonne] += 1
          }
        }
      }
      signeI.style.left = (largeurSignede + (2 * numColonne - 1) * (L - largeurSignede) / (2 * nbValx + 2) - signeI.getBoundingClientRect().width / 2) + 'px'
      signeI.style.top = (numLigne * h / (expresTab.length + 2) + h / (2 * expresTab.length + 4) - signeI.getBoundingClientRect().height / 2) + 'px'
      objDiv.signeLigne.push(signeI)
    }
    objDiv.zoneInput = []
    if (!lesSignes[lesSignes.length - 1]) {
      // on demande de compléter des zones de saisie de la dernière ligne
      for (let i = 1; i <= nbValx + 1; i++) {
        const signeProd = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
        j3pStyle(signeProd, { position: 'absolute' })
        const elt = j3pAffiche(signeProd, '', '&1&', { inputmq1: { texte: '' } })
        objDiv.zoneInput.push(elt.inputmqList[0])
        elt.inputmqList[0].index = i
        elt.inputmqList[0].leSpan = elt.parent.parentNode
        mqRestriction(elt.inputmqList[0], '+-')
        // positionnement initial de toutes les zones
        numColonne = (i - 1) % (nbValx + 1) + 1
        signeProd.style.left = (largeurSignede + (2 * numColonne - 1) * (L - largeurSignede) / (2 * nbValx + 2) - signeProd.getBoundingClientRect().width / 2) + 'px'
        signeProd.style.top = ((expresTab.length + 1.5) * h / (expresTab.length + 2) - signeProd.getBoundingClientRect().height / 2) + 'px'
      }
      // ajustement de la position de chaque signe
      for (let i = 1; i <= nbValx + 1; i++) {
        objDiv.zoneInput[i - 1].addEventListener('input', function () {
          const idZoneNulle = this.index
          const laZone = objDiv.zoneInput[idZoneNulle - 1].leSpan
          numColonne = (idZoneNulle - 1) % (nbValx + 1) + 1
          laZone.style.left = (largeurSignede + (2 * numColonne - 1) * (L - largeurSignede) / (2 * nbValx + 2) - objDiv.zoneInput[idZoneNulle - 1].getBoundingClientRect().width / 2) + 'px'
          laZone.style.top = ((expresTab.length + 1.5) * h / (expresTab.length + 2) - signeProd.getBoundingClientRect().height / 2) + 'px'
        })
      }
      // listes de la dernière ligne*
      objDiv.zoneSelect = []
      for (let i = 1; i <= nbValx; i++) {
        const zoneSelect = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
        j3pStyle(zoneSelect, { position: 'absolute' })
        tabTextListe = (prodQuot[0] === 'produit') ? ['|', '0'] : ['|', '||', '0']
        const elt = j3pAffiche(zoneSelect, '', '#1#', { liste1: { texte: tabTextListe, correction: '0' } })
        objDiv.zoneSelect.push(elt.selectList[0])
        zoneSelect.style.left = (largeurSignede + i * (L - largeurSignede) / (nbValx + 1) - 8) + 'px'
        zoneSelect.style.top = (h - h / (2 * expresTab.length + 4) - zoneSelect.getBoundingClientRect().height / 2) + 'px'
      }
      j3pFocus(objDiv.zoneInput[0])
    } else {
      // on donne les signes et les zéros ou double barres(la réponse)
      if (prodQuot[0] === 'produit') {
        // tout d’abord les zéros'
        for (let i = 1; i <= nbValx; i++) {
          const divZero = j3pAddElt(nomdiv, 'p', '0', { style: objStyleCouleur.style })
          j3pStyle(divZero, { color: maCouleurTxt, position: 'absolute' })
          divZero.style.left = largeurSignede + (i) * (L - largeurSignede) / (tabZeroOrdonne.length + 1) - divZero.getBoundingClientRect().width / 2 + 'px'
          divZero.style.top = (expresTab.length + 1.5) * h / (expresTab.length + 2) - divZero.getBoundingClientRect().height / 2 + 'px'
        }
      } else {
        // tout d’abord les zéros ou double barres'
        for (let i = 1; i <= nbValx; i++) {
          const numElt = positionTab(valZeroTab, tabZeroOrdonne[i - 1])
          if (prodQuot[1][numElt]) {
            // on met une double barre
            j3pCreeSegment(svg, {
              x1: posValZero[i] + 3,
              y1: (expresTab.length + 1) * h / (expresTab.length + 2),
              x2: posValZero[i] + 3,
              y2: h,
              couleur: maCouleurTxt,
              epaisseur: 1
            })
            j3pCreeSegment(svg, {
              x1: posValZero[i],
              y1: (expresTab.length + 1) * h / (expresTab.length + 2),
              x2: posValZero[i],
              y2: h,
              couleur: maCouleurTxt,
              epaisseur: 1
            })
          } else {
            const divZero = j3pAddElt(nomdiv, 'p', '0', { style: objStyleCouleur.style })
            j3pStyle(divZero, { color: maCouleurTxt, position: 'absolute' })
            divZero.style.left = largeurSignede + (i) * (L - largeurSignede) / (tabZeroOrdonne.length + 1) - divZero.getBoundingClientRect().width / 2 + 'px'
            divZero.style.top = (expresTab.length + 1.5) * h / (expresTab.length + 2) - divZero.getBoundingClientRect().height / 2 + 'px'
          }
        }
      }
      // maintenant on gère les signes
      for (let i = 1; i <= nbValx + 1; i++) {
        const signeProd = j3pAddElt(nomdiv, 'p', '', { style: objStyleCouleur.style })
        if (nbSignesMoinsTab[i] % 2 === 0) {
          j3pAffiche(signeProd, '', '$+$', { style: { color: maCouleurTxt } })
        } else {
          j3pAffiche(signeProd, '', '$-$', { style: { color: maCouleurTxt } })
        }
        signeProd.style.left = (largeurSignede + (2 * i - 1) * (L - largeurSignede) / (2 * nbValx + 2) - signeProd.getBoundingClientRect().width / 2) + 'px'
        signeProd.style.top = ((expresTab.length + 1.5) * h / (expresTab.length + 2) - signeProd.getBoundingClientRect().height / 2) + 'px'
      }
    }
    return objDiv
  } // fin tabSignesProduit

  function tableauSigneRep (valZeroTab, lesSignes) {
    // valZeroTab est le tableau qui contient les endroits où les fonctions affines s’annulent
    // ici toutes ses valeurs sont différentes'
    // lesSignes est un tableau de tableaux de style ["+","-"] ou ["-","+"]"
    const nbSigne = (valZeroTab.length + 1) * (lesSignes.length - 1)
    // le tableau suivant va servir pour le signe du produit ou du quotient
    const nbSignesMoinsTab = []
    // le tableau suivant accueille tous les signes des zones de saisie
    const signeSaisie = []
    for (let i = 1; i <= valZeroTab.length + 1; i++) {
      nbSignesMoinsTab[i] = 0
    }
    const tabZeroOrdonne = fctsEtudeOrdonneTabSansDoublon(valZeroTab)
    for (let i = 1; i <= nbSigne; i++) {
      // je cherche l’endroit où s’annule mon expression (si elle s’annule)
      const numLigne = Math.ceil(i / (valZeroTab.length + 1))
      const numColonne = (i - 1) % (valZeroTab.length + 1) + 1
      if (valZeroTab[numColonne - 1] === undefined) {
        if (lesSignes[numLigne - 1][1] === '-') {
          nbSignesMoinsTab[numColonne] += 1
          signeSaisie[i] = '-'
        } else {
          signeSaisie[i] = '+'
        }
      } else {
        if (j3pCalculValeur(valZeroTab[numLigne - 1]) >= j3pCalculValeur(tabZeroOrdonne[numColonne - 1])) {
          if (lesSignes[numLigne - 1][0] === '-') {
            nbSignesMoinsTab[numColonne] += 1
            signeSaisie[i] = '-'
          } else {
            signeSaisie[i] = '+'
          }
        } else {
          if (lesSignes[numLigne - 1][1] === '-') {
            nbSignesMoinsTab[numColonne] += 1
            signeSaisie[i] = '-'
          } else {
            signeSaisie[i] = '+'
          }
        }
      }
    }
    // signes de la dernière ligne (celle du produit ou du quotient)
    for (let i = 1; i <= valZeroTab.length + 1; i++) {
      if (nbSignesMoinsTab[i] % 2 === 0) {
        signeSaisie[nbSigne + i] = '+'
      } else {
        signeSaisie[nbSigne + i] = '-'
      }
    }
    return signeSaisie
  }

  function afficheCorrection () {
    // on affiche la correction
    stor.maCorrection = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.maCorrection, { paddingTop: '15px', color: me.styles.toutpetit.correction.color })
    for (let i = 1; i <= 3; i++) stor['zoneExpli' + i] = j3pAddElt(stor.maCorrection, 'div')
    j3pAffiche(stor.zoneExpli1, '', textes.corr1)
    const intervalle = ']-\\infty;' + me.stockage[40][0] + '['
    let nbSigneMoins = 0
    for (let i = 1; i <= me.stockage[25].length; i++) {
      if (me.stockage[30][1 + (me.stockage[25].length + 1) * (i - 1)] === '-') nbSigneMoins++
    }
    if (me.stockage[30][me.stockage[25].length * (me.stockage[25].length + 1) + 1] === '-') {
      if (nbSigneMoins <= 1) {
        j3pAffiche(stor.zoneExpli2, '', textes.corr4, {
          a: intervalle,
          b: nbSigneMoins
        })
      } else {
        j3pAffiche(stor.zoneExpli2, '', textes.corr2, {
          a: intervalle,
          b: nbSigneMoins
        })
      }
    } else {
      if (nbSigneMoins <= 1) {
        j3pAffiche(stor.zoneExpli2, '', textes.corr5, {
          a: intervalle,
          b: nbSigneMoins
        })
      } else {
        j3pAffiche(stor.zoneExpli2, '', textes.corr3, {
          a: intervalle,
          b: nbSigneMoins
        })
      }
    }
    if (ds.type_expres[0] === 'produit') {
      j3pAffiche(stor.zoneExpli3, '', textes.corr6)
    } else {
      j3pAffiche(stor.zoneExpli3, '', textes.corr7)
    }
    // correction des signes qui sont faux
    for (let i = 1; i <= me.stockage[25].length + 1; i++) {
      if (!me.stockage[31][i]) {
        const signeCorr = j3pAddElt(stor.leTableau, 'div', '', { style: me.styles.toutpetit.correction })
        j3pStyle(signeCorr, { position: 'absolute' })
        j3pAffiche(signeCorr, '', '$' + me.stockage[30][i + (me.stockage[25].length + 1) * me.stockage[25].length] + '$')
        const posSigneLeft = Number(stor.objTab.zoneInput[i - 1].leSpan.style.left.split('px')[0]) + stor.objTab.zoneInput[i - 1].leSpan.getBoundingClientRect().width + 4
        const posSigneTop = stor.objTab.zoneInput[i - 1].leSpan.style.top
        signeCorr.style.left = String(posSigneLeft) + 'px'
        signeCorr.style.top = posSigneTop
      }
    }

    // j’encadre les signes qui me servent pour la justification'
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    // il faut calculer la largeur de mon rectangle (L) puis sa hauteur (h)
    const posxInit = Number(stor.objTab.inf[0].style.left.split('px')[0]) + 3 * stor.objTab.inf[0].getBoundingClientRect().width / 4
    const posxFin = Number(stor.objTab.divZeros[0].style.left.split('px')[0]) - stor.objTab.inf[0].getBoundingClientRect().width / 2
    const L = posxFin - posxInit
    const posyInit = Number(stor.objTab.signeLigne[0].style.top.split('px')[0]) - 3
    const numSigne = (me.stockage[25].length - 1) * (me.stockage[25].length + 1) + 1
    const posyFin = Number(stor.objTab.signeLigne[numSigne - 1].style.top.split('px')[0]) + stor.objTab.signeLigne[numSigne - 1].getBoundingClientRect().height + 2
    const h = posyFin - posyInit
    const posTabx = Number(stor.leTableau.style.left.split('px')[0])
    const posTaby = Number(stor.leTableau.style.top.split('px')[0])
    svg.setAttribute('width', me.stockage[50])
    svg.setAttribute('height', me.zonesElts.MG.getBoundingClientRect().width)
    const cadreCorr = j3pAddElt(stor.leTableau, 'div')
    j3pStyle(cadreCorr, { position: 'absolute', top: 0, left: 0 })
    cadreCorr.appendChild(svg)
    j3pCreeRectangle(svg, {
      x: posTabx + posxInit,
      y: posTaby + posyInit,
      width: L,
      height: h,
      couleur: me.styles.petit.correction.color,
      epaisseur: 1
    })
    // correction des listes
    const posZeroTop = Number(stor.objTab.divGauche[stor.objTab.divGauche.length - 1].style.top.split('px')[0]) + stor.objTab.divGauche[stor.objTab.divGauche.length - 1].getBoundingClientRect().height + 4
    for (let i = 1; i <= me.stockage[26].length; i++) {
      const posZeroLeft = Number(stor.objTab.divZeros[i - 1].style.left.split('px')[0]) + stor.objTab.divZeros[i - 1].getBoundingClientRect().width / 2 - 7
      if (!me.stockage[32][i]) {
        if (ds.type_expres[0] === 'produit') {
          const zeroCorr = j3pAddElt(stor.leTableau, 'p', '0', { style: me.styles.toutpetit.correction })
          j3pStyle(zeroCorr, { position: 'absolute', left: posZeroLeft + 'px', top: posZeroTop + 'px' })
        } else {
          const posTab = positionTab(me.stockage[25], me.stockage[26][i - 1])
          if (ds.type_expres[posTab + 1] === 'num') {
            const zeroCorr = j3pAddElt(stor.leTableau, 'p', '0', { style: me.styles.toutpetit.correction })
            j3pStyle(zeroCorr, { position: 'absolute', left: posZeroLeft + 'px', top: posZeroTop + 'px' })
          } else {
            const decalage = 17
            j3pCreeSegment(svg, {
              x1: posZeroLeft + decalage,
              y1: Number(stor.objTab.divGauche[stor.objTab.divGauche.length - 1].style.top.split('px')[0]) + posTaby - 7,
              x2: posZeroLeft + decalage,
              y2: Number(stor.objTab.divGauche[stor.objTab.divGauche.length - 1].style.top.split('px')[0]) + 30 + posTaby,
              couleur: me.styles.petit.correction.color,
              epaisseur: 2
            })
          }
        }
      }
    }
  }

  function enonceMain () {
    const tailleLigne = 50
    let k
    if (ds.type_expres[0] === 'quotient') {
      // on complète le tableau ds.type_expres s’il ne l’est pas totalement'
      for (let i = 1; i <= Math.min(ds.nbfacteurs_affine, 4); i++) {
        if ((ds.type_expres[i] === undefined) || ((ds.type_expres[i] !== 'num') && (ds.type_expres[i] !== 'den'))) {
          // si rien n’est précisé je mets "den" à ce facteur affine'
          // de toute façon, s’ils sont tous "den", le premier sera transformé en "num"'
          ds.type_expres[i] = 'den'
        }
      }
      // ensuite je m’assure qu’il y a bien un facteur affine au numérateur et un au dénominateur'
      // si ce n’est pas le cas, alors je force le premier à être un numérateur et la dernier à un un dénominateur'
      let numAffinePresent = false
      for (let i = 1; i < Math.min(ds.nbfacteurs_affine, 4); i++) {
        if (ds.type_expres[i] === 'num') numAffinePresent = true
      }
      if (!numAffinePresent) ds.type_expres[1] = 'num'
      let denAffinePresent = false
      for (let i = 1; i < Math.min(ds.nbfacteurs_affine, 4); i++) {
        if ((ds.type_expres[i] === undefined) || (ds.type_expres[i] !== 'num')) {
          ds.type_expres[i] = 'den'
          denAffinePresent = true
        }
      }
      if (!denAffinePresent) ds.type_expres[Math.min(ds.nbfacteurs_affine, 4)] = 'den'
    }
    const coefA = []
    const coefB = []
    const solEq = []
    const solutionEq = []
    const solEqLatex = []
    const lePgcd = []
    const choixEntier = []
    // j’ai besoin de la largeur de la colonne de gauche. Je mets ça dans stockage[50]'
    me.stockage[50] = Math.min(95 * me.zonesElts.HG.getBoundingClientRect().width / 100, 700)
    // choix des coefs a et b aléatoire
    // la racine de la première fonction affine n’est pas nécessairement entière
    choixEntier[0] = Boolean(j3pGetRandomInt(0, 1))
    choixEntier[1] = Boolean(j3pGetRandomInt(0, 1))
    if (choixEntier[0]) {
      do {
        coefA[0] = j3pGetRandomInt(ds.a)
      } while (Math.abs(coefA[0]) < Math.pow(10, -13))
      solEq[0] = j3pGetRandomInt(-6, 6)
      solEqLatex[0] = String(solEq[0])
      solutionEq[0] = solEq[0]
      coefB[0] = -coefA[0] * solEq[0]
    } else {
      do {
        do {
          coefA[0] = j3pGetRandomInt(ds.a)
        } while (Math.abs(coefA[0]) < Math.pow(10, -13))
        coefB[0] = j3pGetRandomInt(ds.b)
        solutionEq[0] = -coefB[0] / coefA[0]
      } while (Math.abs(solutionEq[0] - Math.round(solutionEq[0])) < 0.00001)
      lePgcd[0] = j3pPGCD(coefB[0], coefA[0], { returnOtherIfZero: true, negativesAllowed: true })
      if (Number(coefA[0]) * Number(coefB[0]) < 0) {
        solEq[0] = String(Math.abs(coefB[0]) / lePgcd[0]) + '/' + String(Math.abs(coefA[0]) / lePgcd[0])
        solEqLatex[0] = '\\frac{' + String(Math.abs(coefB[0]) / lePgcd[0]) + '}{' + String(Math.abs(coefA[0]) / lePgcd[0]) + '}'
      } else {
        solEq[0] = String(-Math.abs(coefB[0]) / lePgcd[0]) + '/' + String(Math.abs(coefA[0]) / lePgcd[0])
        solEqLatex[0] = '-\\frac{' + String(Math.abs(coefB[0]) / lePgcd[0]) + '}{' + String(Math.abs(coefA[0]) / lePgcd[0]) + '}'
      }
    }
    // la racine de la deuxième fonction affine n’est pas nécessairement entière
    do {
      if (choixEntier[1]) {
        do {
          do {
            coefA[1] = j3pGetRandomInt(ds.a)
          } while (Math.abs(coefA[1]) < Math.pow(10, -13))
          solEq[1] = j3pGetRandomInt(-6, 6)
          solEqLatex[1] = String(solEq[1])
          solutionEq[1] = solEq[1]
          coefB[1] = -coefA[1] * solutionEq[1]
        } while ((Math.abs(solutionEq[0] - solutionEq[1]) < Math.pow(10, -13)) || (Math.abs(solEq[1]) < Math.pow(10, -13)))
      } else {
        do {
          do {
            coefA[1] = j3pGetRandomInt(ds.a)
          } while (Math.abs(coefA[1]) < Math.pow(10, -13))
          coefB[1] = j3pGetRandomInt(ds.b)
          solutionEq[1] = -coefB[1] / coefA[1]
        } while ((Math.abs(solutionEq[1] - Math.round(solutionEq[1])) < 0.00001) || (Math.abs(solutionEq[0] - solutionEq[1]) < Math.pow(10, -13)))
        lePgcd[1] = j3pPGCD(coefB[1], coefA[1], { returnOtherIfZero: true, negativesAllowed: true })
        if (Number(coefA[1]) * Number(coefB[1]) < 0) {
          solEq[1] = String(Math.abs(coefB[1]) / lePgcd[1]) + '/' + String(Math.abs(coefA[1]) / lePgcd[1])
          solEqLatex[1] = '\\frac{' + String(Math.abs(coefB[1]) / lePgcd[1]) + '}{' + String(Math.abs(coefA[1]) / lePgcd[1]) + '}'
        } else {
          solEq[1] = String(-Math.abs(coefB[1]) / lePgcd[1]) + '/' + String(Math.abs(coefA[1]) / lePgcd[1])
          solEqLatex[1] = '-\\frac{' + String(Math.abs(coefB[1]) / lePgcd[1]) + '}{' + String(Math.abs(coefA[1]) / lePgcd[1]) + '}'
        }
      }
    } while (Math.abs(solutionEq[0] - solutionEq[1]) < Math.pow(10, -13))
    if (ds.nbfacteurs_affine > 2) {
      // il y a d’autres fonctions affines'
      // là je ne me préocupe plus de savoir si c’est un entier ou pas...'
      let k2
      for (k = 2; k < Math.min(ds.nbfacteurs_affine, 4); k++) {
        let solDifferente
        do {
          do {
            do {
              coefA[k] = j3pGetRandomInt(ds.a)
            } while (Math.abs(coefA[k]) < Math.pow(10, -13))
            coefB[k] = j3pGetRandomInt(ds.b)
            solutionEq[k] = -coefB[k] / coefA[k]
            // je veux que cette solution ne soit pas entière
          } while (Math.abs(solutionEq[k] - Math.round(solutionEq[k])) < 0.00001)
          lePgcd[k] = j3pPGCD(coefB[k], coefA[k], { returnOtherIfZero: true, negativesAllowed: true })
          if (Number(coefA[k]) * Number(coefB[k]) < 0) {
            solEq[k] = String(Math.abs(coefB[k]) / lePgcd[k]) + '/' + String(Math.abs(coefA[k]) / lePgcd[k])
            solEqLatex[k] = '\\frac{' + String(Math.abs(coefB[k]) / lePgcd[k]) + '}{' + String(Math.abs(coefA[k]) / lePgcd[k]) + '}'
          } else {
            solEq[k] = String(-Math.abs(coefB[k]) / lePgcd[k]) + '/' + String(Math.abs(coefA[k]) / lePgcd[k])
            solEqLatex[k] = '-\\frac{' + String(Math.abs(coefB[k]) / lePgcd[k]) + '}{' + String(Math.abs(coefA[k]) / lePgcd[k]) + '}'
          }
          // solution différente des précédentes
          solDifferente = true
          for (k2 = 0; k2 < k; k2++) {
            if (Math.abs(solutionEq[k] - solutionEq[k2]) < Math.pow(10, -10)) {
              solDifferente = false
            }
          }
        } while (!solDifferente)
      }
    }
    // fin de la détermination des coefficients
    // tableau pour mettre les différentes solutions
    const tabSolEqText = []
    for (let i = 0; i < Math.min(ds.nbfacteurs_affine, 4); i++) {
      tabSolEqText[i] = String(solEq[i])
    }
    // on gère alors l’écriture des facteurs affines'
    const ordreAffine = []
    const ordreAff = []
    const expressAffine = []
    for (let i = 0; i < Math.min(ds.nbfacteurs_affine, 4); i++) {
      ordreAff[i] = Boolean(j3pGetRandomInt(0, 1))
      if (ordreAff[i]) {
        ordreAffine[i] = 'ax+b'
      } else {
        ordreAffine[i] = 'b+ax'
      }
      if (ordreAffine[i] === 'ax+b') {
        expressAffine[i] = j3pMonome(1, 1, Number(coefA[i])) + j3pMonome(2, 0, Number(coefB[i]))
      } else {
        if (coefB[i] === 0) {
          expressAffine[i] = j3pMonome(1, 1, Number(coefA[i]))
        } else {
          expressAffine[i] = j3pMonome(1, 0, Number(coefB[i])) + j3pMonome(2, 1, Number(coefA[i]))
        }
      }
    }
    let expressProduit, expressQuotient
    if (ds.type_expres[0] === 'produit') {
      expressProduit = ''
      if (Math.abs(coefB[0]) < Math.pow(10, -13)) {
        expressProduit = expressAffine[0]
        for (let i = 1; i < Math.min(ds.nbfacteurs_affine, 4); i++) {
          expressProduit += '(' + expressAffine[i] + ')'
        }
      } else {
        for (let i = 0; i < Math.min(ds.nbfacteurs_affine, 4); i++) {
          expressProduit += '(' + expressAffine[i] + ')'
        }
      }
    } else {
      // partie à gérer pour identifier numérateur et denominateur
      let numQuotient = ''
      let denQuotient = ''
      let premierFacteurNum = true
      let premierFacteurDen = true
      // il faut que je fasse attention si je n’ai pas un seul facteur au num ou au den'
      // dans ce cas, il ne prend pas de parenthèses
      let nbNum = 0
      let nbDen = 0
      for (let i = 0; i < Math.min(ds.nbfacteurs_affine, 4); i++) {
        if (ds.type_expres[i + 1] === 'num') {
          nbNum++
        } else {
          nbDen++
        }
      }
      for (let i = 0; i < Math.min(ds.nbfacteurs_affine, 4); i++) {
        if (ds.type_expres[i + 1] === 'num') {
          // ce facteur affine est au numérateur
          if ((nbNum === 1) || (premierFacteurNum && (Math.abs(coefB[i]) < Math.pow(10, -13)))) {
            numQuotient += expressAffine[i]
            premierFacteurNum = false
          } else {
            numQuotient += '(' + expressAffine[i] + ')'
          }
        } else {
          // ce facteur affine est au dénominateur
          if ((nbDen === 1) || (premierFacteurDen && (Math.abs(coefB[i]) < Math.pow(10, -13)))) {
            denQuotient += expressAffine[i]
            premierFacteurDen = false
          } else {
            denQuotient += '(' + expressAffine[i] + ')'
          }
        }
      }
      // maintenant je déclare le quotient
      expressQuotient = '\\frac{' + numQuotient + '}{' + denQuotient + '}'
    }
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.zoneCons2, { padding: '10px 0' })
    if (ds.type_expres[0] === 'produit') {
      j3pAffiche(stor.zoneCons1, '', textes.consigne1, { a: expressProduit })
    } else {
      j3pAffiche(stor.zoneCons1, '', textes.consigne2, { a: expressQuotient })
    }
    // tableau qui va contenir le signe de chacun des facteurs (2 signes suffisent)
    const signesFacteurs = []
    for (let i = 0; i < Math.min(ds.nbfacteurs_affine, 4); i++) {
      if (coefA[i] < 0) {
        signesFacteurs[i] = ['+', '-']
      } else {
        signesFacteurs[i] = ['-', '+']
      }
    }
    // pour le signe du produit/quotient, on peut mettre des zones de saisie (false) ou les signes (true) dans le dernier élément du tableau
    signesFacteurs[Math.min(ds.nbfacteurs_affine, 4)] = false
    stor.leTableau = j3pAddElt(stor.zoneCons2, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { position: 'relative', top: 0, left: 0 }) })
    const typeProduitQuotient = []
    if (ds.type_expres[0] === 'produit') {
      typeProduitQuotient[0] = 'produit'
    } else {
      typeProduitQuotient[0] = 'quotient'
      typeProduitQuotient[1] = []
      for (let i = 1; i < ds.type_expres.length; i++) {
        typeProduitQuotient[1][i - 1] = (ds.type_expres[i] !== 'num')
      }
    }
    stor.objTab = tabSignesProduit(stor.leTableau, expressAffine, [me.stockage[50], tailleLigne * (expressAffine.length + 2)], typeProduitQuotient, [true, true], tabSolEqText, signesFacteurs, { style: me.styles.toutpetit.enonce })
    me.logIfDebug('solEq : ' + solEq)
    me.stockage[25] = tabSolEqText
    me.stockage[26] = fctsEtudeOrdonneTabSansDoublon(tabSolEqText)
    me.stockage[30] = tableauSigneRep(solEq, signesFacteurs)
    me.logIfDebug('tableau réponse des signes : ' + me.stockage[30])
    // tabSolEqLatexOrdonne est le tableau avec les réponses dans l’ordre au format latex (utile pour la correction)'
    const tabSolEqLatexOrdonne = []
    for (let i = 0; i < tabSolEqText.length; i++) {
      // je cherche la position de chaque élément dans me.stockage[26]
      for (k = 0; k < tabSolEqText.length; k++) {
        if (me.stockage[26][i] === tabSolEqText[k]) {
          tabSolEqLatexOrdonne[i] = solEqLatex[k]
        }
      }
    }
    me.logIfDebug('tabSolEqText:' + tabSolEqText + '  tabSolEqLatexOrdonne:' + tabSolEqLatexOrdonne)
    me.stockage[40] = tabSolEqLatexOrdonne
    me.finEnonce()

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '15px' }) })
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.construitStructurePage('presentation1bis')

        me.stockage = [0, 0, 0, 0]
        if (ds.type_expres[0] === 'produit') {
          me.afficheTitre(textes.titre_exo1)
        } else {
          me.afficheTitre(textes.titre_exo2)
        }

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()
      /// //////////////////////////////////////
      /* FIN DU CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////////

      break // case "enonce":

    case 'correction':
      {
        const reponseIndex = []
        // listes pour le signe du produit // i va de 1 à tabSolEqText.length
        // if (me.stockage[26] === undefined) console.log('stockage:', me.stockage)
        for (let i = 1; i <= me.stockage[26].length; i++) {
          reponseIndex[i] = stor.objTab.zoneSelect[i - 1].selectedIndex
        }
        let aRepondu = true
        // on considère qu’on répond aux listes par défaut'
        const reponseEleve = []
        let numZoneIncomplete = 0
        for (let i = me.stockage[26].length + 1; i >= 1; i--) {
          reponseEleve[i] = j3pValeurde(stor.objTab.zoneInput[i - 1])
          if (reponseEleve[i] === '') {
            aRepondu = false
            numZoneIncomplete = i
            j3pFocus(stor.objTab.zoneInput[i - 1])
          }
        }
        if (!aRepondu && (!me.isElapsed)) {
          stor.zoneCorr.style.color = me.styles.cfaux
          stor.zoneCorr.innerHTML = (numZoneIncomplete > 0) ? textes.comment1 : textes.comment1bis
          return me.finCorrection()
        } else { // une réponse a été saisie
          const bonneRep = []// pour toutes les zones de saisie
          // on crée le tableau accueillant les signes réponse (dans l’ordre)'
          const tabSigneRep = me.stockage[30]
          me.logIfDebug('tabSigneRep:' + tabSigneRep)
          for (let i = 1; i <= me.stockage[25].length + 1; i++) {
            bonneRep[i] = (reponseEleve[i] === tabSigneRep[i + (me.stockage[25].length + 1) * me.stockage[25].length])
          }
          me.logIfDebug('bonneRep', bonneRep, 'reponseIndex' + reponseIndex)
          const bonneListe = []
          // listes de la dernière ligne
          if (ds.type_expres[0] === 'produit') {
          // chaque liste doit contenir 0;
            for (let i = 1; i <= me.stockage[26].length; i++) {
              bonneListe[i] = (reponseIndex[i] === 1)
            }
          } else {
          // chaque liste doit contenir 0 sauf si la valeur est interdite
            for (let i = 1; i <= me.stockage[26].length; i++) {
              const posTab = positionTab(me.stockage[25], me.stockage[26][i - 1])
              if (ds.type_expres[posTab + 1] === 'num') {
                bonneListe[i] = (reponseIndex[i] === 2)// c’est-à-dire 0'
              } else {
                bonneListe[i] = (reponseIndex[i] === 1)// c’est-à-dire ||
              }
            }
          }
          me.logIfDebug('bonneListe:' + bonneListe)
          let cbon = true
          for (let i = 1; i <= me.stockage[25].length; i++) {
            cbon = (cbon && bonneRep[i])
          }
          for (let i = 1; i <= me.stockage[25].length; i++) {
            cbon = (cbon && bonneListe[i])
          }
          me.stockage[31] = bonneRep
          me.stockage[32] = bonneListe
          if (cbon) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // on remet toutes les zones et listes en noir :
            for (let i = 1; i <= me.stockage[25].length + 1; i++) {
              j3pDesactive(stor.objTab.zoneInput[i - 1])
              stor.objTab.zoneInput[i - 1].setAttribute('style', 'color:' + me.styles.cbien)
            }
            for (let i = 1; i <= me.stockage[25].length; i++) {
              stor.objTab.zoneSelect[i - 1].disabled = true
              stor.objTab.zoneSelect[i - 1].setAttribute('style', 'color:' + me.styles.cbien)
            }
            me.typederreurs[0]++// le tableau est correct
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            let signeOK = true
            let listesOK = true
            stor.zoneCorr.style.color = me.styles.cfaux
            for (let i = 1; i <= me.stockage[25].length + 1; i++) {
              if (!bonneRep[i]) {
                signeOK = false
                stor.objTab.zoneInput[i - 1].setAttribute('style', 'color:' + me.styles.cfaux)
              } else {
                stor.objTab.zoneInput[i - 1].setAttribute('style', 'color:' + me.styles.cbien)
              }
            }
            for (let i = 1; i <= me.stockage[25].length; i++) {
              if (!bonneListe[i]) {
                listesOK = false
                stor.objTab.zoneSelect[i - 1].setAttribute('style', 'color:' + me.styles.cfaux)
              }
            }
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              /*
                        *   RECOPIER LA CORRECTION ICI !
                        */
              afficheCorrection()
              me.finCorrection('navigation', true)
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux + '<br>'
              if (!signeOK) stor.zoneCorr.innerHTML += textes.corr9 + '<br>'
              if (!listesOK) stor.zoneCorr.innerHTML += textes.corr8
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                // on redonne le focus
                for (let i = me.stockage[25].length + 1; i >= 1; i--) {
                  if (!bonneRep[i]) j3pFocus(stor.objTab.zoneInput[i - 1])
                }
                me.typederreurs[1]++
                me.finCorrection()
              } else {
              // Erreur au second essai
                me.typederreurs[2]++
                // on bloque les zones
                for (let i = 1; i <= me.stockage[25].length + 1; i++) j3pDesactive(stor.objTab.zoneInput[i - 1])
                for (let i = 1; i <= me.stockage[25].length; i++) {
                  stor.objTab.zoneSelect[i - 1].disabled = true
                }
                for (let i = 1; i <= me.stockage[25].length + 1; i++) {
                  if (!bonneRep[i]) j3pBarre(stor.objTab.zoneInput[i - 1])
                }
                for (let i = 1; i <= me.stockage[25].length; i++) {
                  if (!bonneListe[i]) j3pBarre(stor.objTab.zoneSelect[i - 1])
                }
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // affichage de l’explication
                afficheCorrection()
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
}
