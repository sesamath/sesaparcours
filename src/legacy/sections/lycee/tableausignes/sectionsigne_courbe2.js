import { j3pAddElt, j3pNombre, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pMax, j3pMin, j3pPaletteMathquill, j3pRandomTab, j3pStyle, j3pAddContent, j3pGetNewId, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import { construireTableauSignes } from 'src/legacy/outils/fonctions/tableauSignes'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    août 2014
    À partir de la courbe représentative d’une fonction on demande de déterminer
    les valeurs interdites et son signe
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['nbvalremarquables', 'alea', 'string', 'cette variable est la somme du nombre de valeurs interdites et du nombre de zéros de la fonction : on peut imposer une valeur entre 1 et 4 ou bien alea'],
    ['domaineR', 'true', 'string', 'Par défaut le domaine de définition est l’ensemble des réels, mais on peut le limiter à l’intervalle où est trace la courbe, en mettant "false". On peut aussi le faire évoluer au fil des répétitions, par exemple "false|false|true" pour qu’on y mette l’ensemble des réels qu’à la troisième répétition'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.6, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'problème avec les zéros de la fonction ou les valeurs interdites' },
    { pe_3: 'problème avec le signe de la fonction' },
    { pe_4: 'insuffisant' }
  ]
}
const textes = {
  titre_exo: 'Signe d’une fonction à l’aide de la courbe',
  consigne1: 'Soit $£f$ une fonction dont on donne la courbe représentative ci-dessous.',
  consigne1bis: 'Soit $£f$ une fonction définie sur $£d\\setminus\\bracket{£i}$ dont on donne la courbe représentative ci-dessous.',
  consigne2: 'Préciser la (ou les) valeur(s) en laquelle (ou lesquelles) s’annule la fonction $£f$ :',
  consigne2bis: 'Préciser la (ou les) valeur(s) en laquelle (ou lesquelles) la fonction $£f$ n’est pas définie :',
  consigne3: 'Dans les deux cas :<br>- s’il y en a plusieurs, on séparera les valeurs par un point-virgule;',
  consigne4: '- s’il n’y en a pas, on écrira l’ensemble vide (palette de bouton).',
  consigne5: 'La fonction $£f$ ne s’annule pas sur $£d$.',
  consigne6: 'La fonction $£f$ s’annule en une valeur : $£v$.',
  consigne7: 'La fonction $£f$ s’annule en £n valeurs : $£v$.',
  consigne8: 'Complète alors le tableau de signes de la fonction $£f$ :',
  info1: "ATTENTION, tu n’as le droit qu'à un seul essai pour cette question !",
  comment1: 'Tu n’as pas le bon nombre de valeurs en lesquelles la fonction s’annule !',
  comment1bis: 'Tu n’as pas le bon nombre de valeurs en lesquelles la fonction n’est pas définie !',
  comment1ter: 'Tu n’as pas le bon nombre de valeurs en lesquelles la fonction s’annule, et où elle n’est pas définie !',
  comment2: 'Il y a pourtant une ou plusieurs bonne(s) valeur(s) !',
  corr1: 'La fonction s’annule lorsque la courbe coupe l’axe des abscisses et elle n’est pas définie en toute abscisse où la courbe ne passe pas (la courbe ne coupe pas la droite tracée en vert).',
  corr1bis: 'La fonction s’annule lorsque la courbe coupe l’axe des abscisses et elle n’est pas définie en toute abscisse où la courbe ne passe pas (la courbe ne coupe pas les droites tracées en vert).',
  cons_tab1: 'Signe <br>de $£f$',
  corr2: 'La fonction est négative lorsque la courbe est en-dessous de l’axe des abscisses et positive lorsque la courbe est au-dessus.'
}
/**
 * section signe_courbe2
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    if ((me.questionCourante % ds.nbetapes) === 1) {
      j3pDetruit(stor.laPalette)
      // on affiche la correction
      // pour les zéros de la fonction
      stor.zoneExpli1 = j3pAddElt(stor.zoneCons5, 'div', '', { style: { color: (stor.bonneReponseZeros) ? me.styles.cbien : me.styles.petit.correction.color } })
      stor.zoneExpli2 = j3pAddElt(stor.zoneCons8, 'div', '', { style: { color: (stor.bonneReponseValinterdites) ? me.styles.cbien : me.styles.petit.correction.color } })
    } else {
      // on affiche la correction
      stor.zoneExpli = j3pAddElt(stor.conteneurD, 'div', '', { style: { color: (bonneReponse) ? me.styles.cbien : me.styles.petit.correction.color } })
    }
    // on affiche la correction
    if ((me.questionCourante % ds.nbetapes) === 1) {
      if (!stor.bonneReponseZeros) {
        if (stor.nb_zero === 0) {
          j3pAffiche(stor.zoneExpli1, '', '$\\varnothing$\n')
        } else {
          const tabZeroOrdonne = stor.ensembleZeros.sort()
          const listeZero = tabZeroOrdonne.join(' ; ')
          j3pAffiche(stor.zoneExpli1, '', listeZero)
        }
      }
      if (!stor.bonneReponseValinterdites) {
        if (stor.nb_valinterdites === 0) {
          j3pAffiche(stor.zoneExpli2, '', '$\\varnothing$\n')
        } else {
          j3pAffiche(stor.zoneExpli2, '', stor.listeValinterdites)
        }
      }
      if (!bonneReponse) {
        j3pDetruit(stor.zoneCons9)
        j3pDetruit(stor.zoneCons10)
        stor.zoneExpli3 = j3pAddElt(stor.conteneurD, 'div', '', { style: { color: (bonneReponse) ? me.styles.cbien : me.styles.petit.correction.color } })
        if (stor.nb_valinterdites === 1) j3pAffiche(stor.zoneExpli3, '', textes.corr1)
        else j3pAffiche(stor.zoneExpli3, '', textes.corr1bis)
      }

      // J’ajoute les asymptotes verticales
      for (let i = 0; i < stor.ensembleValinterdites.length; i++) {
        stor.repere.add({ type: 'point', nom: 'A' + i, par1: stor.ensembleValinterdites[i], par2: -20, fixe: true, visible: false, etiquette: true, style: { couleur: '#00F', epaisseur: 2, taille: 18 } })
        stor.repere.add({ type: 'point', nom: 'B' + i, par1: stor.ensembleValinterdites[i], par2: 20, fixe: true, visible: false, etiquette: true, style: { couleur: '#00F', epaisseur: 2, taille: 18 } })
        stor.repere.add({ type: 'segment', nom: 's' + i, par1: 'A' + i, par2: 'B' + i, style: { couleur: '#336600', epaisseur: 2 } })
      }
      stor.repere.construit()
    } else {
      if (ds.nbchances > 1) j3pDetruit(stor.zoneInfo)
      if (!bonneReponse) {
        const zoneExpliRep = j3pAddElt(stor.repAttendue, 'div', '', { style: { paddingTop: '5px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
        stor.leTableauRep = j3pAddElt(zoneExpliRep, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { position: 'relative' }) })
        stor.objTableauCorr = Object.assign({}, stor.objTableau)
        stor.objTableauCorr.eltsAffiches = [true, true, true]
        stor.objTableauCorr.styleTxt = me.styles.toutpetit.correction
        stor.tabSigne = construireTableauSignes(stor.leTableauRep, stor.objTableauCorr)
      }
      j3pAffiche(stor.zoneExpli, '', textes.corr2)
    }
  }
  function ecoute (num) {
    if (stor.zoneInput[num].className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.zoneCons5)
      j3pEmpty(stor.zoneCons8)
      stor.laPalette = j3pAddElt(stor['zoneCons' + (5 + 3 * num)], 'div')
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput[num], { liste: ['vide'] })
    }
  }

  function enonceMain () {
    let nbvalremarquables, fdexinit, fdex
    let a, b, c, d, e
    let tabSigneRep, ensembleZeros, ensembleValinterdites
    let choixFct, calcArbre, nomF
    if ((me.questionCourante % ds.nbetapes) === 1) {
    // on définit la fonction
      const tabNomf = ['f', 'g', 'h']
      nomF = j3pRandomTab(tabNomf, [0.3333, 0.3333, 0.3334])
      if ((j3pNombre(String(ds.nbvalremarquables)) < 1) || (j3pNombre(String(ds.nbvalremarquables)) > 4)) {
        ds.nbvalremarquables = 'alea'
      }
      if (ds.nbvalremarquables === 'alea') {
        let tabNbValRemarquables
        if (me.questionCourante === 1) {
        // on départ on initialise le tableau des différentes possibilités
          tabNbValRemarquables = [1, 2, 3, 4]
        } else {
          if (stor.tabNbValRemarquables.length === 0) {
          // on réinitialise le tableau des différentes possibilités
            tabNbValRemarquables = [1, 2, 3, 4]
          } else {
            tabNbValRemarquables = stor.tabNbValRemarquables
          }
        }
        const pioche = Math.floor(Math.random() * tabNbValRemarquables.length)
        nbvalremarquables = tabNbValRemarquables[pioche]
        me.logIfDebug('tabNbValRemarquables:' + tabNbValRemarquables + '   pioche' + pioche)
        tabNbValRemarquables.splice(pioche, 1)
        stor.tabNbValRemarquables = tabNbValRemarquables
      } else {
        nbvalremarquables = j3pNombre(String(ds.nbvalremarquables))
      }
      me.logIfDebug('nbvalremarquables:' + nbvalremarquables)
      if (nbvalremarquables === 1) {
      // fct a/(x-b) ou a/(x-b)^2
        a = (Math.floor(Math.random() * 2) * 2 - 1) * (1 + j3pGetRandomInt(0, 10) * 0.2)// a est dans [-3;-1] ou [1;3] choix aléatoire avec un pas de 0.2
        b = j3pGetRandomInt(-3, 3)
        choixFct = j3pGetRandomInt(0, 1)
        fdexinit = (choixFct === 0) ? a + '/(x-(' + (b) + '))' : a + '/(x-(' + (b) + '))^2'
        if (choixFct === 0) {
          if (a > 0) {
            tabSigneRep = ['-', '+']
          } else {
            tabSigneRep = ['+', '-']
          }
        } else {
          if (a > 0) {
            tabSigneRep = ['+', '+']
          } else {
            tabSigneRep = ['-', '-']
          }
        }
        ensembleZeros = []
        ensembleValinterdites = [b]
      } else if (nbvalremarquables === 2) {
        choixFct = j3pGetRandomInt(1, 2)
        let choixFct2
        if (choixFct === 1) {
        // la fonction s’annule en 1 valeur et a une valeur interdite (fonction homographique)'
        // fct a*(x-c)/(x-b) ou a*(x-c)/(x-b)^2
          choixFct2 = j3pGetRandomInt(0, 1)
          do {
            a = (Math.floor(Math.random() * 2) * 2 - 1) * (j3pGetRandomInt(1, 6) * 0.5)// a est dans [-3;-0.5] ou [0.5;6] choix aléatoire avec un pas de 0.5
            do {
              b = j3pGetRandomInt(-3, 3)
              c = j3pGetRandomInt(-3, 3)
            } while (Math.abs(b - c) < 2.2)
            fdexinit = (choixFct2 === 0) ? a + '*(x-(' + c + '))/(x-(' + (b) + '))' : a + '*(x-(' + c + '))/(x-(' + (b) + '))^2'
            // fdexinit=a+"*(x-("+c+"))/(x-("+(b)+"))"

            calcArbre = new Tarbre(fdexinit, ['x'])
          } while ((Math.abs(calcArbre.evalue([b - 0.5])) > 8) || (Math.abs(calcArbre.evalue([b + 0.5])) > 8))

          ensembleZeros = [c]
          ensembleValinterdites = [b]
        } else {
        // la fonction ne s’annule pas et a 2 valeurs interdites'
        // fct a/(x-b)(x-c)
          a = (Math.floor(Math.random() * 2) * 2 - 1) * (j3pGetRandomInt(10, 30) * 0.1)// a est dans [-3;-1] ou [1;3] choix aléatoire avec un pas de 0.1
          do {
            b = j3pGetRandomInt(-3, 3)
            c = j3pGetRandomInt(-3, 3)
          } while (Math.abs(b - c) < 2.2)
          fdexinit = a + '/((x-(' + c + '))*(x-(' + (b) + ')))'
          ensembleZeros = []
          ensembleValinterdites = [b, c]
        }
        if ((choixFct === 1) && (choixFct2 === 1)) {
          if (a > 0) {
            if (b < c) {
              tabSigneRep = ['-', '-', '+']
            } else {
              tabSigneRep = ['-', '+', '+']
            }
          } else {
            if (b < c) {
              tabSigneRep = ['+', '+', '-']
            } else {
              tabSigneRep = ['+', '-', '-']
            }
          }
        } else {
          if (a > 0) {
            tabSigneRep = ['+', '-', '+']
          } else {
            tabSigneRep = ['-', '+', '-']
          }
        }
      } else if (nbvalremarquables === 3) {
        choixFct = j3pGetRandomInt(1, 3)
        if (choixFct === 1) {
        // la fonction s’annule en 2 valeur et a une valeur interdite
        // fct a(x-b)(x-c)/(x-d)'
          a = (Math.floor(Math.random() * 2) * 2 - 1) * (j3pGetRandomInt(5, 20) * 0.1)// a est dans [-2;-0.5] ou [0.5;2] choix aléatoire avec un pas de 0.1
          do {
            b = j3pGetRandomInt(-3, 3)
            c = j3pGetRandomInt(-3, 3)
            d = j3pGetRandomInt(-3, 3)
          } while ((Math.abs(b - c) < Math.pow(10, -12)) || (Math.abs(b - c) > 4.5) || (Math.abs(b - d) < Math.pow(10, -12)) || (Math.abs(d - c) < Math.pow(10, -12)) || ((d < b) && (d < c)) || ((d > b) && (d > c)))
          // on fait en sorte que d soit entre b et c
          fdexinit = a + '*(x-(' + (c) + '))*(x-(' + b + '))/(x-(' + d + '))'
          ensembleZeros = [b, c]
          ensembleValinterdites = [d]
        } else if (choixFct === 2) {
        // la fonction s’annule en 1 valeur et a deux valeurs interdites
        // fct a(x-b)/((x-c)(x-d))'
          a = (Math.floor(Math.random() * 2) * 2 - 1) * (j3pGetRandomInt(10, 40) * 0.1)// a est dans [-4;-1] ou [1;4] choix aléatoire avec un pas de 0.1
          do {
            b = j3pGetRandomInt(-3, 3)
            c = j3pGetRandomInt(-3, 3)
            d = j3pGetRandomInt(-3, 3)
          } while ((Math.abs(b - c) < Math.pow(10, -12)) || (Math.abs(b - d) < Math.pow(10, -12)) || (Math.abs(d - c) < 2.5) || ((d < b) && (d < c)) || ((d > b) && (d > c)))
          fdexinit = a + '*(x-(' + b + '))/((x-(' + (c) + '))*(x-(' + d + ')))'
          ensembleZeros = [b]
          ensembleValinterdites = [c, d]
        } else {
        // la fonction ne s’annule pas et a trois valeurs interdites'
        // fct a/((x-b)(x-c)(x-d))
          a = (Math.floor(Math.random() * 2) * 2 - 1) * (j3pGetRandomInt(10, 30) * 0.1)// a est dans [-3;-1] ou [1;3] choix aléatoire avec un pas de 0.1
          do {
            b = j3pGetRandomInt(-3, 3)
            c = j3pGetRandomInt(-4, 4)
            d = j3pGetRandomInt(-4, 4)
          } while ((Math.abs(b - c) < Math.pow(10, -12)) || (Math.abs(b - d) < Math.pow(10, -12)) || (Math.abs(d - c) < Math.pow(10, -12)) || (Math.abs(b - c) > 4) || (Math.abs(b - d) > 4) || (Math.abs(c - d) > 4))
          fdexinit = a + '/((x-(' + c + '))*(x-(' + (b) + '))*(x-(' + d + ')))'
          ensembleZeros = []
          ensembleValinterdites = [b, c, d]
        }
        if (a > 0) {
          tabSigneRep = ['-', '+', '-', '+']
        } else {
          tabSigneRep = ['+', '-', '+', '-']
        }
      } else if (nbvalremarquables === 4) {
        choixFct = j3pGetRandomInt(1, 3)
        if (choixFct === 1) {
        // la fonction s’annule en 3 valeurs et a une valeur interdite
        // fct a(x-b)(x-c)(x-d)/(x-e)'
          a = (Math.floor(Math.random() * 2) * 2 - 1) * (j3pGetRandomInt(1, 3) * 0.1)// a est dans [-0.3;-0.1] ou [0.1;0.3] choix aléatoire avec un pas de 0.1
          do {
            b = j3pGetRandomInt(-3, 3)
            c = j3pGetRandomInt(-4, 4)
            d = j3pGetRandomInt(-4, 4)
            e = j3pGetRandomInt(-3, 3)
          } while ((Math.abs(b - c) < 1.2) || (Math.abs(b - d) < 1.2) || (Math.abs(d - c) < 1.2) || (Math.abs(b - e) < Math.pow(10, -12)) || (Math.abs(e - c) < Math.pow(10, -12)) || (Math.abs(d - e) < Math.pow(10, -12)) || ((e < b) && (e < c) && (e < d)) || ((e > b) && (e > c) && (e > d)))
          // on fait en sorte que b soit entre b et c
          fdexinit = a + '*(x-(' + (c) + '))*(x-(' + b + '))*(x-(' + d + '))/(x-(' + e + '))'
          ensembleZeros = [b, c, d]
          ensembleValinterdites = [e]
        } else if (choixFct === 2) {
        // la fonction s’annule en 2 valeurs et a 2 valeurs interdites
        // fct a(x-b)(x-c)/((x-d)(x-e))'
          a = (Math.floor(Math.random() * 2) * 2 - 1) * (j3pGetRandomInt(2, 8) * 0.1)// a est dans [-0.8;-0.2] ou [0.2;0.8] choix aléatoire avec un pas de 0.1
          do {
            b = j3pGetRandomInt(-3, 3)
            c = j3pGetRandomInt(-3, 3)
            d = j3pGetRandomInt(-3, 3)
            e = j3pGetRandomInt(-3, 3)
          } while ((Math.abs(b - c) < Math.pow(10, -12)) || (Math.abs(b - d) < Math.pow(10, -12)) || (Math.abs(d - c) < 1e-12 || (Math.abs(b - e) < Math.pow(10, -12)) || (Math.abs(e - c) < Math.pow(10, -12)) || (Math.abs(d - e) < Math.pow(10, -12))) || (Math.abs(d - e) < 3) || (Math.abs(b - c) < 3))
          // on fait en sorte que les zéros soient assez éloignés, de même pour les valeurs interdites
          fdexinit = a + '*(x-(' + (c) + '))*(x-(' + b + '))/((x-(' + d + '))*(x-(' + e + ')))'
          ensembleZeros = [b, c]
          ensembleValinterdites = [d, e]
        } else if (choixFct === 3) {
        // la fonction s’annule en 1 valeur et a 3 valeurs interdites
        // fct a(x-b)/((x-c)(x-d)(x-e))'
          a = (Math.floor(Math.random() * 2) * 2 - 1) * (j3pGetRandomInt(10, 20) * 0.1)// a est dans [-2;-1] ou [1;2] choix aléatoire avec un pas de 0.1
          let minInterdites, maxInterdites
          do {
            b = j3pGetRandomInt(-3, 3)
            c = j3pGetRandomInt(-4, 4)
            d = j3pGetRandomInt(-4, 4)
            e = j3pGetRandomInt(-4, 4)
            minInterdites = j3pMin([c, d, e]).min
            maxInterdites = j3pMax([c, d, e]).max
          } while ((Math.abs(b - c) < Math.pow(10, -12)) || (Math.abs(b - d) < Math.pow(10, -12)) || (Math.abs(d - c) < 1e-12 || (Math.abs(b - e) < Math.pow(10, -12)) || (Math.abs(e - c) < Math.pow(10, -12)) || (Math.abs(d - e) < Math.pow(10, -12))) || ((b < d) && (b < c) && (b < e)) || ((b > c) && (b > d) && (b > e)) || (maxInterdites - minInterdites > 5.5))
          // on fait en sorte que b ne soit plus petit ou plus grand que chacune des trois autres valeurs c, d et e
          fdexinit = a + '*(x-(' + (b) + '))/((x-(' + c + '))*(x-(' + d + '))*(x-(' + e + ')))'
          ensembleZeros = [b]
          ensembleValinterdites = [c, d, e]
        }
        if (a > 0) {
          tabSigneRep = ['+', '-', '+', '-', '+']
        } else {
          tabSigneRep = ['-', '+', '-', '+', '-']
        }
      }
      fdex = fdexinit
      stor.tabSigneRep = tabSigneRep
      stor.fdex = fdex
      stor.nb_zero = ensembleZeros.length
      stor.nb_valinterdites = ensembleValinterdites.length
      stor.ensembleZeros = ensembleZeros
      stor.ensembleValinterdites = ensembleValinterdites
      stor.nomF = nomF
      const tabValinterditesOrdonne = stor.ensembleValinterdites.sort()
      stor.listeValinterdites = tabValinterditesOrdonne.join(' ; ')
      stor.nbvalremarquables = nbvalremarquables
    }
    me.logIfDebug('fdex:' + stor.fdex)
    me.logIfDebug('ensembleZeros:' + ensembleZeros + '   ensembleValinterdites:' + ensembleValinterdites)
    const tabBornes = []
    for (let i = 0; i < stor.ensembleValinterdites.length; i++) tabBornes.push(stor.ensembleValinterdites[i])
    stor.le_domaine = '\\R'
    stor.borne_inf = '-\\infty'
    stor.borne_sup = '+\\infty'
    let grady = 2
    let nbPixelsy = 40
    let divLargRepere = 12.5
    const ecartFaible = 0.2
    const ecartTresFaible = 0.05
    let absMin = -6
    let absMax = 6
    calcArbre = new Tarbre(stor.fdex, ['x'])
    if ((stor.nbvalremarquables === 3) && (stor.nb_valinterdites >= 2)) {
    // parfois dans ces cas, il faut limiter les abscisses au niveau de l’affichage de la courbe car la courbe peut être quasiment confondue avec l’xae des abscisses'
      while (Math.abs(calcArbre.evalue([absMin])) < ecartTresFaible) {
        absMin += 0.5
      }
      while (Math.abs(calcArbre.evalue([absMax])) < ecartTresFaible) {
        absMax -= 0.5
      }
      divLargRepere = (absMax - absMin + 1)
    }
    if (stor.domaineR_tab[Math.floor((me.questionCourante - 1) / 2)]) {
      tabBornes.push((absMin - 1), (absMax + 1))
    } else {
      tabBornes.push(absMin, absMax)
      stor.le_domaine = '[' + absMin + ';' + absMax + ']'
      stor.borne_inf = absMin
      stor.borne_sup = absMax
    }
    if ((Math.abs(calcArbre.evalue([absMin])) < ecartTresFaible) || (Math.abs(calcArbre.evalue([absMax])) < ecartTresFaible)) {
      nbPixelsy = 40
      grady = 1
    } else if ((Math.abs(calcArbre.evalue([absMin])) < ecartFaible) || (Math.abs(calcArbre.evalue([absMax])) < ecartFaible)) {
      nbPixelsy = 30
      grady = 1
    }
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if ((me.questionCourante % ds.nbetapes) === 1) {
      j3pAffiche(stor.zoneCons1, '', textes.consigne1,
        { f: stor.nomF })
    } else {
      j3pAffiche(stor.zoneCons1, '', textes.consigne1bis,
        { f: stor.nomF, i: stor.listeValinterdites, d: stor.le_domaine })
    }
    stor.leRepere = j3pAddElt(stor.zoneCons2, 'div')
    const hauteurRepere = 470
    const largeurRepere = 430
    me.logIfDebug('tabBornes:' + tabBornes + '      ensembleValinterdites:' + stor.ensembleValinterdites)
    const nbBornes = tabBornes.length
    const tabBornesOrdonnees = []
    for (let i = 0; i < nbBornes; i++) {
      const indiceMin = j3pMin(tabBornes).indice
      tabBornesOrdonnees.push(j3pMin(tabBornes).min)
      tabBornes.splice(indiceMin, 1)
    }
    me.logIfDebug('tabBornesOrdonnees:' + tabBornesOrdonnees)
    stor.idRepere = j3pGetNewId('unrepere')
    stor.repere = new Repere({
      idConteneur: stor.leRepere,
      idDivRepere: stor.idRepere,
      aimantage: false,
      visible: true,
      larg: largeurRepere,
      haut: hauteurRepere,

      pasdunegraduationX: 1,
      pixelspargraduationX: largeurRepere / divLargRepere,
      pasdunegraduationY: grady,
      pixelspargraduationY: nbPixelsy,
      debuty: 0,
      xO: largeurRepere * Math.abs(absMin) / (absMax - absMin),
      yO: hauteurRepere / 2,
      negatifs: true,
      fixe: true,
      trame: true,
      objets: []
    })
    for (let i = 0; i < tabBornesOrdonnees.length - 1; i++) {
      stor.repere.add({ type: 'courbe', nom: 'c' + i, par1: stor.fdex, par2: tabBornesOrdonnees[i] + 0.01, par3: tabBornesOrdonnees[i + 1] - 0.01, par4: 100, style: { couleur: '#FA5', epaisseur: 2 } })
    }
    stor.repere.construit()
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '5px' }) })
    for (let i = 3; i <= 10; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneurD, 'div')
    if ((me.questionCourante % ds.nbetapes) === 1) {
      j3pAffiche(stor.zoneCons3, '', textes.consigne2, { f: nomF })
      const elt = j3pAffiche(stor.zoneCons4, '', '&1&', { inputmq1: { texte: '' } })
      stor.zoneInput = [elt.inputmqList[0]]
      stor.laPalette = j3pAddElt(stor.zoneCons5, 'div')
      // palette MQ :
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], { liste: ['vide'] })
      stor.zoneInput[0].addEventListener('focusin', ecoute.bind(null, 0))
      j3pStyle(stor.zoneCons6, { paddingTop: '40px' }) // Pour être sous la palette des boutons
      j3pAffiche(stor.zoneCons6, '', textes.consigne2bis, { f: nomF })
      const elt2 = j3pAffiche(stor.zoneCons7, '', '&1&', { inputmq1: { texte: '' } })
      stor.zoneInput.push(elt2.inputmqList[0])
      stor.zoneInput[1].addEventListener('focusin', ecoute.bind(null, 1))
      j3pStyle(stor.zoneCons9, { paddingTop: '40px' }) // Pour être sous la palette des boutons
      j3pAffiche(stor.zoneCons9, '', textes.consigne3, { f: nomF })
      j3pAffiche(stor.zoneCons10, '', textes.consigne4, { f: nomF })
      mqRestriction(stor.zoneInput[0], '\\d,.;-', { commandes: ['vide'] })
      mqRestriction(stor.zoneInput[1], '\\d,.;-', { commandes: ['vide'] })
      j3pFocus(stor.zoneInput[0])
    } else {
    // dans le cas de la deuxième question (tableau de signes)
      nomF = stor.nomF
      if (stor.nb_zero === 0) {
        j3pAffiche(stor.zoneCons3, '', textes.consigne5, { f: nomF, d: stor.le_domaine })
      } else if (stor.nb_zero === 1) {
        j3pAffiche(stor.zoneCons3, '', textes.consigne6, { f: nomF, v: String(stor.ensembleZeros[0]) })
      } else {
        let zeroInit = []
        zeroInit = zeroInit.concat(stor.ensembleZeros)
        const zerosOrdonnes = []
        for (let i = 0; i < stor.ensembleZeros.length; i++) {
          const objMin = j3pMin(zeroInit)
          zerosOrdonnes[i] = objMin.min
          zeroInit.splice(objMin.indice, 1)
        }
        me.logIfDebug('zerosOrdonnes : ' + zerosOrdonnes)
        let listeZero = String(zerosOrdonnes[0])
        for (let i = 1; i < zerosOrdonnes.length; i++) listeZero += '\\quad;\\quad' + String(zerosOrdonnes[i])
        j3pAffiche(stor.zoneCons3, '', textes.consigne7, { f: nomF, n: stor.nb_zero, v: listeZero })
      }
      j3pAffiche(stor.zoneCons4, '', textes.consigne8, { f: nomF })
      stor.leTableau = j3pAddElt(stor.zoneCons5, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { position: 'relative', left: '5px' }) })
      stor.objTableau = {
        nomFct: stor.nomF,
        L: me.zonesElts.MD.getBoundingClientRect().width - 30,
        h: 100,
        tabZero: stor.ensembleZeros,
        tabValInterdite: stor.ensembleValinterdites,
        tabSignes: stor.tabSigneRep,
        eltsAffiches: [true, true, false],
        styleTxt: me.styles.toutpetit.enonce,
        signeDeTxt: textes.cons_tab1
      }
      stor.tabSigne = construireTableauSignes(stor.leTableau, stor.objTableau)
      stor.zoneInput = [...stor.tabSigne.signe]
      // stor.zoneInput = construireTableauSignes(stor.leTableau, stor.nomF, me.zonesElts.MD.getBoundingClientRect().width - 30, 100, stor.ensembleZeros, stor.ensembleValinterdites, stor.tabSigneRep, [true, true, false], me.styles.toutpetit.enonce)
      for (let i = 0; i <= stor.nb_zero + stor.nb_valinterdites; i++) {
        stor.zoneInput[i].typeReponse = ['texte']
        stor.zoneInput[i].reponse = [stor.tabSigneRep[i]]
      }
      if (ds.nbchances > 1) {
        stor.zoneInfo = j3pAddElt(stor.zoneCons6, 'p', '', { style: { fontStyle: 'italic' } })
        j3pAddContent(stor.zoneInfo, textes.info1)
      }
      stor.repAttendue = j3pAddElt(stor.conteneurD, 'div')
    }

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    let mesZonesSaisie
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    if ((me.questionCourante % ds.nbetapes) === 1) {
      mesZonesSaisie = stor.zoneInput.map(eltInput => eltInput.id)
      stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesZonesSaisie })
    } else {
      mesZonesSaisie = stor.zoneInput.map(eltInput => eltInput.id)
      stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
      me.essaiCourant = ds.nbchances
    }
    // Obligatoire
    me.finEnonce()
  }
  // console.log("debut du code : ",me.etat)
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.surcharge({ nbetapes: 2 })

        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // gestion du domaine de définition
        stor.domaineR_tab = []
        for (let k = 0; k < ds.nbrepetitions; k++) stor.domaineR_tab[k] = true
        if (ds.domaineR.includes('|')) {
          // ce ne sera pas le même d’une question à l’autre'
          const tabDomaineR = ds.domaineR.split('|')
          for (let k = 0; k < tabDomaineR.length; k++) {
            if (tabDomaineR[k] === 'false') stor.domaineR_tab[k] = false
          }
        } else {
          for (let k = 0; k < ds.nbrepetitions; k++) stor.domaineR_tab[k] = ds.domaineR !== 'false'
        }

        me.logIfDebug('stor.domaineR_tab:' + stor.domaineR_tab)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        let aRepondu, bonneReponse, bonneReponseZeros
        let presenceZero = false
        let bonneReponseValinterdites, bonNombreZero, bonNombreValInterdites
        if ((me.questionCourante % ds.nbetapes) === 1) {
          aRepondu = fctsValid.valideReponses()
          bonneReponseZeros = true
          bonneReponseValinterdites = true
          const repEleve1 = j3pValeurde(stor.zoneInput[0])
          const repEleve2 = j3pValeurde(stor.zoneInput[1])
          me.logIfDebug('repEleve : ' + repEleve1 + '   et   ' + repEleve2)
          bonNombreZero = true
          bonNombreValInterdites = true
          if (aRepondu) {
          // gestion des zéros
            if (stor.nb_zero === 0) {
              bonneReponseZeros = (repEleve1 === '\\varnothing')
              if (!bonneReponseZeros) bonNombreZero = false
            } else {
              const tabValeurs = repEleve1.split(';')
              bonNombreZero = (tabValeurs.length === stor.nb_zero)
              const bonneRep = []
              bonneReponseZeros = bonNombreZero
              for (let j = 0; j < stor.nb_zero; j++) {
                bonneRep[j] = false
                // bonneRep[j] permet de savoir si la j-ème valeur attendue est dans la liste des réponses
                for (let i = 0; i < tabValeurs.length; i++) {
                  bonneRep[j] = (bonneRep[j] || (Math.abs(Number(tabValeurs[i]) - stor.ensembleZeros[j]) < Math.pow(10, -12)))
                  if (Math.abs(Number(tabValeurs[i]) - stor.ensembleZeros[j]) < Math.pow(10, -12)) {
                    presenceZero = true
                  }
                }
                bonneReponseZeros = (bonneReponseZeros && bonneRep[j])
              }
              me.logIfDebug('presenceZero:' + presenceZero + '   stor.ensembleZeros:' + stor.ensembleZeros + '  bonneRep:' + bonneRep)
            }
            // gestions des valeurs interdites
            if (stor.nb_valinterdites === 0) {
              bonneReponseValinterdites = (repEleve2 === '\\varnothing')
              if (!bonneReponseValinterdites) bonNombreValInterdites = false
            } else {
              let presenceValInterdites = false
              const tabValeurs2 = repEleve2.split(';')
              bonNombreValInterdites = (tabValeurs2.length === stor.nb_valinterdites)
              const bonneRep2 = []
              bonneReponseValinterdites = bonNombreValInterdites
              for (let j = 0; j < stor.nb_valinterdites; j++) {
                bonneRep2[j] = false
                // bonneRep[j] permet de savoir si la j-ème valeur attendue est dans la liste des réponses
                for (let i = 0; i < tabValeurs2.length; i++) {
                  bonneRep2[j] = (bonneRep2[j] || (Math.abs(Number(tabValeurs2[i]) - stor.ensembleValinterdites[j]) < Math.pow(10, -12)))
                  if (Math.abs(Number(tabValeurs2[i]) - stor.ensembleValinterdites[j]) < Math.pow(10, -12)) {
                    presenceValInterdites = true
                  }
                }
                bonneReponseValinterdites = (bonneReponseValinterdites && bonneRep2[j])
              }
              me.logIfDebug('presenceValInterdites:' + presenceValInterdites + '   stor.ensembleValinterdites:' + stor.ensembleValinterdites + '  bonneRep2:' + bonneRep2)
            }
            bonneReponse = (bonneReponseZeros && bonneReponseValinterdites)
            stor.bonneReponseZeros = bonneReponseZeros
            stor.bonneReponseValinterdites = bonneReponseValinterdites
            if (bonneReponseZeros) {
              fctsValid.zones.bonneReponse[0] = true
              fctsValid.coloreUneZone(stor.zoneInput[0].id)
            }
            if (bonneReponseValinterdites) {
              fctsValid.zones.bonneReponse[1] = true
              fctsValid.coloreUneZone(stor.zoneInput[1].id)
            }
            me.logIfDebug('bonNombreZero:' + bonNombreZero + '   bonNombreValInterdites:' + bonNombreValInterdites)
          }
        } else {
          const reponse = fctsValid.validationGlobale()
          aRepondu = reponse.aRepondu
          bonneReponse = reponse.bonneReponse
          stor.fctsValid_q2 = fctsValid
        }
        if ((!aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = '<br><br>' + cBien
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            if ((me.questionCourante % ds.nbetapes) === 1) {
              if (!bonneReponseZeros) {
                fctsValid.zones.bonneReponse[0] = false
                fctsValid.coloreUneZone(stor.zoneInput[0].id)
              }
              if (!bonneReponseValinterdites) {
                fctsValid.zones.bonneReponse[1] = false
                fctsValid.coloreUneZone(stor.zoneInput[1].id)
              }
            }
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = '<br><br>' + tempsDepasse
              me.typederreurs[10]++
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = '<br><br>' + cFaux
              if ((me.questionCourante % ds.nbetapes) === 1) {
                if (!bonNombreZero) {
                  if (!bonNombreValInterdites) stor.zoneCorr.innerHTML += '<br>' + textes.comment1ter
                  else stor.zoneCorr.innerHTML += '<br>' + textes.comment1
                } else {
                  if (!bonNombreValInterdites) stor.zoneCorr.innerHTML += '<br>' + textes.comment1bis
                }
                if (presenceZero) stor.zoneCorr.innerHTML += '<br>' + textes.comment2
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.finCorrection()
              } else {
              // Erreur au nème essai
                if ((me.questionCourante % ds.nbetapes) === 1) {
                // nombre d’erreurs sur les zéros de la fonction'
                  me.typederreurs[3]++
                } else {
                // nombre d’erreurs sur le signe de la fonction'
                  me.typederreurs[4]++
                }
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++
                afficheCorrection(false)
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentagereussite = me.score / ds.nbitems
        me.logIfDebug('me.typederreurs:' + me.typederreurs + '  ds.nbrepetitions:' + ds.nbrepetitions)
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          if (me.typederreurs[3] / ds.nbrepetitions >= ds.seuilerreur) {
            // Le nombre d’erreurs sur les zéros de la fonction est très important
            // cela permet le retour sur la résolution graphique d’une équation'
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[4] / ds.nbrepetitions >= ds.seuilerreur) {
            // problème sur les signes dans le tableau
            me.parcours.pe = ds.pe_3
          } else {
            me.parcours.pe = ds.pe_4
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
