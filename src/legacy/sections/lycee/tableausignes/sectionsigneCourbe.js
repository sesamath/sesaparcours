import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pNombre, j3pRandomTab, j3pShowError, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { tableauSignesFct } from 'src/legacy/outils/fonctions/tableauSignes'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        novembre 2011
        Dans cette section, on doit, à partir de la courbe représentant une fonction construire directement un tableau de signes
        (pas comme dans signe_courbe1 où on passe par la recherche des zéros de la fct dans un premier temps)
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['nbzero', ['alea', 'alea', 'alea'], 'array', 'Pour chaque répétition, on peut imposer le nombre de zéros de la fonctions (entre 0 et 4). Par défaut, c’est "alea".'],
    ['zeroSansChgtSigne', false, 'boolean', 'En passant ce paramètre à true, on donne une fonction qui s’annule sans changer de signe.'],
    ['scoreLigne1', 0.75, 'reel', 'Score (décimal entre 0 et 1) de l’élève si la première ligne est correcte mais pas la seconde']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Signe d’une fonction à l’aide de la courbe',
  consigne1: 'Soit $£f$ une fonction dont on donne la courbe représentative ci-dessous.',
  consigne2: 'Complète le tableau de signes de la fonction $£f$ :',
  phrase1: 'Pour ajouter une valeur sur la première ligne : ',
  phrase2: 'Pour retirer une valeur sur la première ligne : ',
  comment1: 'Tu n’as pas le bon nombre de valeurs en lesquelles la fonction s’annule !',
  comment2: 'Il y a un souci sur les valeurs en lesquelles la fonction s’annule !',
  comment2_2: 'Attention à l’ensemble de définition de la fonction !',
  comment3: 'N’y aurait-il pas une confusion entre le signe et les variations de la fonction ?',
  comment4: 'Les zéros ont été oubliés dans la deuxième ligne du tableau !',
  comment5: 'Il y a un souci de signe(s) ou de zéro(s), certains pouvant être corrects !',
  cons_tab1: 'Signe de $£f(x)$',
  corr1: 'On identifie dans un premier temps les valeurs qui annulent la fonction (abscisses des points de la courbe situés sur l’axe des abscisses).',
  corr2: 'La fonction est négative lorsque la courbe est en-dessous de l’axe des abscisses et positive lorsque la courbe est au-dessus.',
  corr3: 'On obtient alors le tableau de signes ci-dessous :'
}
/**
 * section signeCourbe
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function fctsEtudeAfficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const laCouleur = (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
    const explications = j3pAddElt(stor.zoneCons6, 'div', '', { style: { paddingTop: '15px', color: laCouleur } })
    for (let i = 1; i <= 4; i++) stor['zoneExpli' + i] = j3pAddElt(explications, 'div')
    for (let i = 1; i <= 2; i++) j3pAffiche(stor['zoneExpli' + i], '', textes['corr' + i])
    if (!bonneReponse) {
      j3pAffiche(stor.zoneExpli3, '', textes.corr3)
      stor.leTableauCorr = j3pAddElt(stor.zoneExpli4, 'div', '', {
        style: me.styles.etendre('toutpetit.enonce', { position: 'relative', top: 0, left: 0 })
      })
      const donneesTabCorrection = Object.assign({}, stor.donneesTableau)
      donneesTabCorrection.tab_dimension[2] = donneesTabCorrection.val_zero_tab.length
      donneesTabCorrection.zero_tab = [true, true, true]
      donneesTabCorrection.obj_style_couleur.couleur = explications.style.color
      donneesTabCorrection.obj_style_couleur.style.color = explications.style.color
      tableauSignesFct(stor.leTableauCorr, donneesTabCorrection)
    }
  }

  function depart (callback) {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH2+v4BAP8BAAAAAAAAAAAC9gAAAjgAAAAAAAAAAAAAAAAAAAD6#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEACkNQb2ludEJhc2UA#####wAAAAAAEAABTwDALgAAAAAAAEAQAAAAAAAABQAAQHbQAAAAAABAaOAAAAAAAP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAAAAQAAABAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAEQAAFJAMAQAAAAAAAAQBAAAAAAAAAFAABAQuXLly5cuQAAAAL#####AAAAAQAJQ0Ryb2l0ZUFCAP####8AAAAAABAAAAEAAQAAAAEAAAAD#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wAAAAAAEAAAAQABAAAAAQAAAAT#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAEAAAABAAAAA#####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAFAAAABv####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAADAKAAAAAAAAMAQAAAAAAAABQABAAAABwAAAAkA#####wAAAAABEAABSgDAKAAAAAAAAMAQAAAAAAAABQACAAAAB#####8AAAACAAdDUmVwZXJlAP####8AgICAAQEAAAABAAAAAwAAAAkBAQAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABP#AAAAAAAAAAAAABP#AAAAAAAAD#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAABAAAAA#####8AAAABAAdDQ2FsY3VsAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAADAD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABdHcmFkdWF0aW9uQXhlc1JlcGVyZU5ldwAAABsAAAAIAAAAAwAAAAoAAAAMAAAADf####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAAA4ABWFic29yAAAACv####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAAA4ABW9yZG9yAAAACv####8AAAABAApDVW5pdGV4UmVwAAAAAA4ABnVuaXRleAAAAAr#####AAAAAQAKQ1VuaXRleVJlcAAAAAAOAAZ1bml0ZXkAAAAK#####wAAAAEAEENQb2ludERhbnNSZXBlcmUAAAAADgAAAAAADgAAAQUAAAAACv####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAPAAAAEwAAABAAAAASAAAAAA4AAAAAAA4AAAEFAAAAAAr#####AAAAAQAKQ09wZXJhdGlvbgAAAAATAAAADwAAABMAAAARAAAAEwAAABAAAAASAAAAAA4AAAAAAA4AAAEFAAAAAAoAAAATAAAADwAAABQAAAAAEwAAABAAAAATAAAAEv####8AAAABAAtDSG9tb3RoZXRpZQAAAAAOAAAAEwAAABMAAAAM#####wAAAAEAC0NQb2ludEltYWdlAAAAAA4AAAAAAA4AAAEFAAAAABQAAAAWAAAAFQAAAAAOAAAAEwAAABMAAAANAAAAFgAAAAAOAAAAAAAOAAABBQAAAAAVAAAAGP####8AAAABAAhDU2VnbWVudAAAAAAOAQAAAAANAAABAAEAAAAUAAAAFwAAABcAAAAADgEAAAAADQAAAQABAAAAFQAAABkAAAAEAAAAAA4BAAAAAAsAAVcAwBQAAAAAAADANAAAAAAAAAUAAT#cVniavN8OAAAAGv####8AAAACAAhDTWVzdXJlWAAAAAAOAAd4Q29vcmQxAAAACgAAABwAAAAMAAAAAA4ABWFic3cxAAd4Q29vcmQxAAAAEwAAAB3#####AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAAA4AZmZmAAAAHAAAABMAAAAMAAAAHAAAAAIAAAAcAAAAHAAAAAwAAAAADgAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAAUAQAAABQCAAAAAUAAAAAAAAAAAAAAEwAAAA8AAAATAAAAHgAAABIAAAAADgEAAAAACwAAAQUAAAAACgAAABMAAAAgAAAAEwAAABAAAAAZAQAAAA4AZmZmAAAAIQAAABMAAAAMAAAAHAAAAAUAAAAcAAAAHQAAAB4AAAAgAAAAIQAAAAQAAAAADgEAAAAACwABUgBAIAAAAAAAAMAgAAAAAAAABQABP9EbToG06B8AAAAb#####wAAAAIACENNZXN1cmVZAAAAAA4AB3lDb29yZDEAAAAKAAAAIwAAAAwAAAAADgAFb3JkcjEAB3lDb29yZDEAAAATAAAAJAAAABkBAAAADgBmZmYAAAAjAAAAEwAAAA0AAAAjAAAAAgAAACMAAAAjAAAADAAAAAAOAAVvcmRyMgANMipvcmRvci1vcmRyMQAAABQBAAAAFAIAAAABQAAAAAAAAAAAAAATAAAAEAAAABMAAAAlAAAAEgAAAAAOAQAAAAALAAABBQAAAAAKAAAAEwAAAA8AAAATAAAAJwAAABkBAAAADgBmZmYAAAAoAAAAEwAAAA0AAAAjAAAABQAAACMAAAAkAAAAJQAAACcAAAAo#####wAAAAIADENDb21tZW50YWlyZQAAAAAOAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAHAsAAfb6#gAAAAEAAAAAAAsjVmFsKGFic3cxKQAAABkBAAAADgBmZmYAAAAqAAAAAUA0AAAAAAAAAAAAHAAAAAQAAAAcAAAAHQAAAB4AAAAqAAAAGwAAAAAOAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAIQsAAfb6#gAAAAEAAAAAAAsjVmFsKGFic3cyKQAAABkBAAAADgBmZmYAAAAsAAAAAUA0AAAAAAAAAAAAHAAAAAYAAAAcAAAAHQAAAB4AAAAgAAAAIQAAACwAAAAbAAAAAA4BZmZmAMAgAAAAAAAAP#AAAAAAAAAAAAAjCwAB9vr+AAAAAgAAAAEACyNWYWwob3JkcjEpAAAAGQEAAAAOAGZmZgAAAC4AAAABQDQAAAAAAAAAAAAjAAAABAAAACMAAAAkAAAAJQAAAC4AAAAbAAAAAA4BZmZmAMAcAAAAAAAAAAAAAAAAAAAAAAAoCwAB9vr+AAAAAgAAAAEACyNWYWwob3JkcjIpAAAAGQEAAAAOAGZmZgAAADAAAAABQDQAAAAAAAAAAAAjAAAABgAAACMAAAAkAAAAJQAAACcAAAAoAAAAMAAAAAwA#####wACeDAAAi05#####wAAAAEADENNb2luc1VuYWlyZQAAAAFAIgAAAAAAAAAAAAwA#####wACeTAAATQAAAABQBAAAAAAAAAAAAAMAP####8AAngxAAItNwAAABwAAAABQBwAAAAAAAAAAAAMAP####8AAnkxAAEwAAAAAQAAAAAAAAAAAAAADAD#####AAJ4MgACLTMAAAAcAAAAAUAIAAAAAAAAAAAADAD#####AAJ5MgACLTMAAAAcAAAAAUAIAAAAAAAAAAAADAD#####AAJ4MwACLTEAAAAcAAAAAT#wAAAAAAAAAAAADAD#####AAJ5MwABMAAAAAEAAAAAAAAAAAAAAAwA#####wACeDQAATAAAAABAAAAAAAAAAAAAAAMAP####8AAnk0AAEyAAAAAUAAAAAAAAAAAAAADAD#####AAJ4NQABMgAAAAFAAAAAAAAAAAAAAAwA#####wACeTUAATAAAAABAAAAAAAAAAAAAAAMAP####8AAng2AAEzAAAAAUAIAAAAAAAAAAAADAD#####AAJ5NgACLTEAAAAcAAAAAT#wAAAAAAAAAAAADAD#####AAJ4NwABNgAAAAFAGAAAAAAAAAAAAAwA#####wACeTcAATAAAAABAAAAAAAAAAAAAAAMAP####8AAng4AAE5AAAAAUAiAAAAAAAAAAAADAD#####AAJ5OAABMwAAAAFACAAAAAAAAAAAABIA#####wAAAAABDwACQTAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAABMAAAAyAAAAEwAAADMAAAASAP####8BAAAAAA8AAkExAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAATAAAANAAAABMAAAA1AAAAEgD#####AQAAAAAPAAJBMgAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAEwAAADYAAAATAAAANwAAABIA#####wEAAAAADwACQTMAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAABMAAAA4AAAAEwAAADkAAAASAP####8BAAAAAA8AAkE0AAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAATAAAAOgAAABMAAAA7AAAAEgD#####AQAAAAAPAAJBNQAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAEwAAADwAAAATAAAAPQAAABIA#####wEAAAAADwACQTYAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAABMAAAA+AAAAEwAAAD8AAAASAP####8BAAAAAA8AAkE3AAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAATAAAAQAAAABMAAABBAAAAEgD#####AAAAAAEPAAJBOAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAEwAAAEIAAAATAAAAQwAAAAwA#####wACbTAABC0wLjUAAAAcAAAAAT#gAAAAAAAAAAAADAD#####AAJtMQACLTIAAAAcAAAAAUAAAAAAAAAAAAAADAD#####AAJtMgABMAAAAAEAAAAAAAAAAAAAAAwA#####wACbTMAAzEuNQAAAAE#+AAAAAAAAAAAAAwA#####wACbTQAATAAAAABAAAAAAAAAAAAAAAMAP####8AAm01AAQtMi4zAAAAHAAAAAFAAmZmZmZmZgAAAAwA#####wACbTYAATAAAAABAAAAAAAAAAAAAAAMAP####8AAm03AAMwLjcAAAABP+ZmZmZmZmYAAAAMAP####8AAm04AAMwLjYAAAABP+MzMzMzMzMAAAAMAP####8AA20xMQACbTAAAAATAAAATQAAAAwA#####wADbTIxAAJtMQAAABMAAABOAAAADQD#####ACBDb3VyYmVQYXIyUG9pbnRzQXZlY0NvZWZmRGlyVGFuZwAAAA4AAAACAAAABQAAAFYAAABXAAAACgAAAEQAAABFAAAAGAAAAABYAAd4Q29vcmQxAAAACgAAAEQAAAAaAAAAAFgAB3lDb29yZDEAAAAKAAAARAAAABgAAAAAWAAHeENvb3JkMQAAAAoAAABFAAAAGgAAAABYAAd5Q29vcmQxAAAACgAAAEUAAAAMAAAAAFgAAmExAEUobTExK20yMSkvKHhDb29yZDEteENvb3JkMSleMisyKih5Q29vcmQxLXlDb29yZDEpLyh4Q29vcmQxLXhDb29yZDEpXjMAAAAUAAAAABQDAAAAFAAAAAATAAAAVgAAABMAAABX#####wAAAAEACkNQdWlzc2FuY2UAAAAUAQAAABMAAABbAAAAEwAAAFkAAAABQAAAAAAAAAAAAAAUAwAAABQCAAAAAUAAAAAAAAAAAAAAFAEAAAATAAAAWgAAABMAAABcAAAAHQAAABQBAAAAEwAAAFsAAAATAAAAWQAAAAFACAAAAAAAAAAAAAwAAAAAWAACYjEARSgyKm0xMSttMjEpLyh4Q29vcmQxLXhDb29yZDEpKzMqKHlDb29yZDEteUNvb3JkMSkvKHhDb29yZDEteENvb3JkMSleMgAAABQAAAAAFAMAAAAUAAAAABQCAAAAAUAAAAAAAAAAAAAAEwAAAFYAAAATAAAAVwAAABQBAAAAEwAAAFkAAAATAAAAWwAAABQDAAAAFAIAAAABQAgAAAAAAAAAAAAUAQAAABMAAABcAAAAEwAAAFoAAAAdAAAAFAEAAAATAAAAWwAAABMAAABZAAAAAUAAAAAAAAAA#####wAAAAEABUNGb25jAQAAAFgAAmYwADlhMSoodC14Q29vcmQxKV4zK2IxKih0LXhDb29yZDEpXjIrbTExKih0LXhDb29yZDEpK3lDb29yZDEAAAAUAAAAABQAAAAAFAAAAAAUAgAAABMAAABdAAAAHQAAABQB#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAABMAAABZAAAAAUAIAAAAAAAAAAAAFAIAAAATAAAAXgAAAB0AAAAUAQAAAB8AAAAAAAAAEwAAAFkAAAABQAAAAAAAAAAAAAAUAgAAABMAAABWAAAAFAEAAAAfAAAAAAAAABMAAABZAAAAEwAAAFoAAXQAAAASAAAAAFgBAAAAAA0AAAEFAAAAAAoAAAATAAAAWQAAAAEAAAAAAAAAAAAAABIAAAAAWAEAAAAADQAAAQUAAAAACgAAABMAAABbAAAAAQAAAAAAAAAAAAAAFwAAAABYAQAAAAANAAABAAEAAABgAAAAYQAAAAQAAAAAWAEAAAAADQACeDEBBQABP81yFDENijAAAABiAAAAGAAAAABYAAd4Q29vcmQxAAAACgAAAGMAAAAMAAAAAFgAAng5AAd4Q29vcmQxAAAAEwAAAGQAAAAMAAAAAFgAAnk5AAZmMCh4OSn#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAABfAAAAEwAAAGUAAAASAAAAAFgBAAAAAA0AAAEFAAAAAAoAAAATAAAAZQAAABMAAABm#####wAAAAIADUNMaWV1RGVQb2ludHMBAAAAWAAAAAAAAQAAAGcAAAEsAAEAAABjAAAABQAAAGMAAABkAAAAZQAAAGYAAABnAAAADAD#####AANtMTIAAm0xAAAAEwAAAE4AAAAMAP####8AA20yMgACbTIAAAATAAAATwAAAA0A#####wAgQ291cmJlUGFyMlBvaW50c0F2ZWNDb2VmZkRpclRhbmcAAAAOAAAAAgAAAAUAAABpAAAAagAAAAoAAABFAAAARgAAABgAAAAAawAHeENvb3JkMQAAAAoAAABFAAAAGgAAAABrAAd5Q29vcmQxAAAACgAAAEUAAAAYAAAAAGsAB3hDb29yZDEAAAAKAAAARgAAABoAAAAAawAHeUNvb3JkMQAAAAoAAABGAAAADAAAAABrAAJhMQBFKG0xMittMjIpLyh4Q29vcmQxLXhDb29yZDEpXjIrMiooeUNvb3JkMS15Q29vcmQxKS8oeENvb3JkMS14Q29vcmQxKV4zAAAAFAAAAAAUAwAAABQAAAAAEwAAAGkAAAATAAAAagAAAB0AAAAUAQAAABMAAABuAAAAEwAAAGwAAAABQAAAAAAAAAAAAAAUAwAAABQCAAAAAUAAAAAAAAAAAAAAFAEAAAATAAAAbQAAABMAAABvAAAAHQAAABQBAAAAEwAAAG4AAAATAAAAbAAAAAFACAAAAAAAAAAAAAwAAAAAawACYjEARSgyKm0xMittMjIpLyh4Q29vcmQxLXhDb29yZDEpKzMqKHlDb29yZDEteUNvb3JkMSkvKHhDb29yZDEteENvb3JkMSleMgAAABQAAAAAFAMAAAAUAAAAABQCAAAAAUAAAAAAAAAAAAAAEwAAAGkAAAATAAAAagAAABQBAAAAEwAAAGwAAAATAAAAbgAAABQDAAAAFAIAAAABQAgAAAAAAAAAAAAUAQAAABMAAABvAAAAEwAAAG0AAAAdAAAAFAEAAAATAAAAbgAAABMAAABsAAAAAUAAAAAAAAAAAAAAHgEAAABrAAJmMQA5YTEqKHQteENvb3JkMSleMytiMSoodC14Q29vcmQxKV4yK20xMioodC14Q29vcmQxKSt5Q29vcmQxAAAAFAAAAAAUAAAAABQAAAAAFAIAAAATAAAAcAAAAB0AAAAUAQAAAB8AAAAAAAAAEwAAAGwAAAABQAgAAAAAAAAAAAAUAgAAABMAAABxAAAAHQAAABQBAAAAHwAAAAAAAAATAAAAbAAAAAFAAAAAAAAAAAAAABQCAAAAEwAAAGkAAAAUAQAAAB8AAAAAAAAAEwAAAGwAAAATAAAAbQABdAAAABIAAAAAawEAAAAADQAAAQUAAAAACgAAABMAAABsAAAAAQAAAAAAAAAAAAAAEgAAAABrAQAAAAANAAABBQAAAAAKAAAAEwAAAG4AAAABAAAAAAAAAAAAAAAXAAAAAGsBAAAAAA0AAAEAAQAAAHMAAAB0AAAABAAAAABrAQAAAAANAAJ4MQEFAAE#zXIUMQ2KMAAAAHUAAAAYAAAAAGsAB3hDb29yZDEAAAAKAAAAdgAAAAwAAAAAawADeDEwAAd4Q29vcmQxAAAAEwAAAHcAAAAMAAAAAGsAA3kxMAAHZjEoeDEwKQAAACAAAAByAAAAEwAAAHgAAAASAAAAAGsBAAAAAA0AAAEFAAAAAAoAAAATAAAAeAAAABMAAAB5AAAAIQEAAABrAAAAAAABAAAAegAAASwAAQAAAHYAAAAFAAAAdgAAAHcAAAB4AAAAeQAAAHoAAAAMAP####8AA20xMwACbTIAAAATAAAATwAAAAwA#####wADbTIzAAJtMwAAABMAAABQAAAADQD#####ACBDb3VyYmVQYXIyUG9pbnRzQXZlY0NvZWZmRGlyVGFuZwAAAA4AAAACAAAABQAAAHwAAAB9AAAACgAAAEYAAABHAAAAGAAAAAB+AAd4Q29vcmQxAAAACgAAAEYAAAAaAAAAAH4AB3lDb29yZDEAAAAKAAAARgAAABgAAAAAfgAHeENvb3JkMQAAAAoAAABHAAAAGgAAAAB+AAd5Q29vcmQxAAAACgAAAEcAAAAMAAAAAH4AAmExAEUobTEzK20yMykvKHhDb29yZDEteENvb3JkMSleMisyKih5Q29vcmQxLXlDb29yZDEpLyh4Q29vcmQxLXhDb29yZDEpXjMAAAAUAAAAABQDAAAAFAAAAAATAAAAfAAAABMAAAB9AAAAHQAAABQBAAAAEwAAAIEAAAATAAAAfwAAAAFAAAAAAAAAAAAAABQDAAAAFAIAAAABQAAAAAAAAAAAAAAUAQAAABMAAACAAAAAEwAAAIIAAAAdAAAAFAEAAAATAAAAgQAAABMAAAB#AAAAAUAIAAAAAAAAAAAADAAAAAB+AAJiMQBFKDIqbTEzK20yMykvKHhDb29yZDEteENvb3JkMSkrMyooeUNvb3JkMS15Q29vcmQxKS8oeENvb3JkMS14Q29vcmQxKV4yAAAAFAAAAAAUAwAAABQAAAAAFAIAAAABQAAAAAAAAAAAAAATAAAAfAAAABMAAAB9AAAAFAEAAAATAAAAfwAAABMAAACBAAAAFAMAAAAUAgAAAAFACAAAAAAAAAAAABQBAAAAEwAAAIIAAAATAAAAgAAAAB0AAAAUAQAAABMAAACBAAAAEwAAAH8AAAABQAAAAAAAAAAAAAAeAQAAAH4AAmYyADlhMSoodC14Q29vcmQxKV4zK2IxKih0LXhDb29yZDEpXjIrbTEzKih0LXhDb29yZDEpK3lDb29yZDEAAAAUAAAAABQAAAAAFAAAAAAUAgAAABMAAACDAAAAHQAAABQBAAAAHwAAAAAAAAATAAAAfwAAAAFACAAAAAAAAAAAABQCAAAAEwAAAIQAAAAdAAAAFAEAAAAfAAAAAAAAABMAAAB#AAAAAUAAAAAAAAAAAAAAFAIAAAATAAAAfAAAABQBAAAAHwAAAAAAAAATAAAAfwAAABMAAACAAAF0AAAAEgAAAAB+AQAAAAANAAABBQAAAAAKAAAAEwAAAH8AAAABAAAAAAAAAAAAAAASAAAAAH4BAAAAAA0AAAEFAAAAAAoAAAATAAAAgQAAAAEAAAAAAAAAAAAAABcAAAAAfgEAAAAADQAAAQABAAAAhgAAAIcAAAAEAAAAAH4BAAAAAA0AAngxAQUAAT#NchQxDYowAAAAiAAAABgAAAAAfgAHeENvb3JkMQAAAAoAAACJAAAADAAAAAB+AAN4MTEAB3hDb29yZDEAAAATAAAAigAAAAwAAAAAfgADeTExAAdmMih4MTEpAAAAIAAAAIUAAAATAAAAiwAAABIAAAAAfgEAAAAADQAAAQUAAAAACgAAABMAAACLAAAAEwAAAIwAAAAhAQAAAH4AAAAAAAEAAACNAAABLAABAAAAiQAAAAUAAACJAAAAigAAAIsAAACMAAAAjQAAAAwA#####wADbTE0AAJtMwAAABMAAABQAAAADAD#####AANtMjQAAm00AAAAEwAAAFEAAAANAP####8AIENvdXJiZVBhcjJQb2ludHNBdmVjQ29lZmZEaXJUYW5nAAAADgAAAAIAAAAFAAAAjwAAAJAAAAAKAAAARwAAAEgAAAAYAAAAAJEAB3hDb29yZDEAAAAKAAAARwAAABoAAAAAkQAHeUNvb3JkMQAAAAoAAABHAAAAGAAAAACRAAd4Q29vcmQxAAAACgAAAEgAAAAaAAAAAJEAB3lDb29yZDEAAAAKAAAASAAAAAwAAAAAkQACYTEARShtMTQrbTI0KS8oeENvb3JkMS14Q29vcmQxKV4yKzIqKHlDb29yZDEteUNvb3JkMSkvKHhDb29yZDEteENvb3JkMSleMwAAABQAAAAAFAMAAAAUAAAAABMAAACPAAAAEwAAAJAAAAAdAAAAFAEAAAATAAAAlAAAABMAAACSAAAAAUAAAAAAAAAAAAAAFAMAAAAUAgAAAAFAAAAAAAAAAAAAABQBAAAAEwAAAJMAAAATAAAAlQAAAB0AAAAUAQAAABMAAACUAAAAEwAAAJIAAAABQAgAAAAAAAAAAAAMAAAAAJEAAmIxAEUoMiptMTQrbTI0KS8oeENvb3JkMS14Q29vcmQxKSszKih5Q29vcmQxLXlDb29yZDEpLyh4Q29vcmQxLXhDb29yZDEpXjIAAAAUAAAAABQDAAAAFAAAAAAUAgAAAAFAAAAAAAAAAAAAABMAAACPAAAAEwAAAJAAAAAUAQAAABMAAACSAAAAEwAAAJQAAAAUAwAAABQCAAAAAUAIAAAAAAAAAAAAFAEAAAATAAAAlQAAABMAAACTAAAAHQAAABQBAAAAEwAAAJQAAAATAAAAkgAAAAFAAAAAAAAAAAAAAB4BAAAAkQACZjMAOWExKih0LXhDb29yZDEpXjMrYjEqKHQteENvb3JkMSleMittMTQqKHQteENvb3JkMSkreUNvb3JkMQAAABQAAAAAFAAAAAAUAAAAABQCAAAAEwAAAJYAAAAdAAAAFAEAAAAfAAAAAAAAABMAAACSAAAAAUAIAAAAAAAAAAAAFAIAAAATAAAAlwAAAB0AAAAUAQAAAB8AAAAAAAAAEwAAAJIAAAABQAAAAAAAAAAAAAAUAgAAABMAAACPAAAAFAEAAAAfAAAAAAAAABMAAACSAAAAEwAAAJMAAXQAAAASAAAAAJEBAAAAAA0AAAEFAAAAAAoAAAATAAAAkgAAAAEAAAAAAAAAAAAAABIAAAAAkQEAAAAADQAAAQUAAAAACgAAABMAAACUAAAAAQAAAAAAAAAAAAAAFwAAAACRAQAAAAANAAABAAEAAACZAAAAmgAAAAQAAAAAkQEAAAAADQACeDEBBQABP81yFDENijAAAACbAAAAGAAAAACRAAd4Q29vcmQxAAAACgAAAJwAAAAMAAAAAJEAA3gxMgAHeENvb3JkMQAAABMAAACdAAAADAAAAACRAAN5MTIAB2YzKHgxMikAAAAgAAAAmAAAABMAAACeAAAAEgAAAACRAQAAAAANAAABBQAAAAAKAAAAEwAAAJ4AAAATAAAAnwAAACEBAAAAkQAAAAAAAQAAAKAAAAEsAAEAAACcAAAABQAAAJwAAACdAAAAngAAAJ8AAACgAAAADAD#####AANtMTUAAm00AAAAEwAAAFEAAAAMAP####8AA20yNQACbTUAAAATAAAAUgAAAA0A#####wAgQ291cmJlUGFyMlBvaW50c0F2ZWNDb2VmZkRpclRhbmcAAAAOAAAAAgAAAAUAAACiAAAAowAAAAoAAABIAAAASQAAABgAAAAApAAHeENvb3JkMQAAAAoAAABIAAAAGgAAAACkAAd5Q29vcmQxAAAACgAAAEgAAAAYAAAAAKQAB3hDb29yZDEAAAAKAAAASQAAABoAAAAApAAHeUNvb3JkMQAAAAoAAABJAAAADAAAAACkAAJhMQBFKG0xNSttMjUpLyh4Q29vcmQxLXhDb29yZDEpXjIrMiooeUNvb3JkMS15Q29vcmQxKS8oeENvb3JkMS14Q29vcmQxKV4zAAAAFAAAAAAUAwAAABQAAAAAEwAAAKIAAAATAAAAowAAAB0AAAAUAQAAABMAAACnAAAAEwAAAKUAAAABQAAAAAAAAAAAAAAUAwAAABQCAAAAAUAAAAAAAAAAAAAAFAEAAAATAAAApgAAABMAAACoAAAAHQAAABQBAAAAEwAAAKcAAAATAAAApQAAAAFACAAAAAAAAAAAAAwAAAAApAACYjEARSgyKm0xNSttMjUpLyh4Q29vcmQxLXhDb29yZDEpKzMqKHlDb29yZDEteUNvb3JkMSkvKHhDb29yZDEteENvb3JkMSleMgAAABQAAAAAFAMAAAAUAAAAABQCAAAAAUAAAAAAAAAAAAAAEwAAAKIAAAATAAAAowAAABQBAAAAEwAAAKUAAAATAAAApwAAABQDAAAAFAIAAAABQAgAAAAAAAAAAAAUAQAAABMAAACoAAAAEwAAAKYAAAAdAAAAFAEAAAATAAAApwAAABMAAAClAAAAAUAAAAAAAAAAAAAAHgEAAACkAAJmNAA5YTEqKHQteENvb3JkMSleMytiMSoodC14Q29vcmQxKV4yK20xNSoodC14Q29vcmQxKSt5Q29vcmQxAAAAFAAAAAAUAAAAABQAAAAAFAIAAAATAAAAqQAAAB0AAAAUAQAAAB8AAAAAAAAAEwAAAKUAAAABQAgAAAAAAAAAAAAUAgAAABMAAACqAAAAHQAAABQBAAAAHwAAAAAAAAATAAAApQAAAAFAAAAAAAAAAAAAABQCAAAAEwAAAKIAAAAUAQAAAB8AAAAAAAAAEwAAAKUAAAATAAAApgABdAAAABIAAAAApAEAAAAADQAAAQUAAAAACgAAABMAAAClAAAAAQAAAAAAAAAAAAAAEgAAAACkAQAAAAANAAABBQAAAAAKAAAAEwAAAKcAAAABAAAAAAAAAAAAAAAXAAAAAKQBAAAAAA0AAAEAAQAAAKwAAACtAAAABAAAAACkAQAAAAANAAJ4MQEFAAE#zXIUMQ2KMAAAAK4AAAAYAAAAAKQAB3hDb29yZDEAAAAKAAAArwAAAAwAAAAApAADeDEzAAd4Q29vcmQxAAAAEwAAALAAAAAMAAAAAKQAA3kxMwAHZjQoeDEzKQAAACAAAACrAAAAEwAAALEAAAASAAAAAKQBAAAAAA0AAAEFAAAAAAoAAAATAAAAsQAAABMAAACyAAAAIQEAAACkAAAAAAABAAAAswAAASwAAQAAAK8AAAAFAAAArwAAALAAAACxAAAAsgAAALMAAAAMAP####8AA20xNgACbTUAAAATAAAAUgAAAAwA#####wADbTI2AAJtNgAAABMAAABTAAAADQD#####ACBDb3VyYmVQYXIyUG9pbnRzQXZlY0NvZWZmRGlyVGFuZwAAAA4AAAACAAAABQAAALUAAAC2AAAACgAAAEkAAABKAAAAGAAAAAC3AAd4Q29vcmQxAAAACgAAAEkAAAAaAAAAALcAB3lDb29yZDEAAAAKAAAASQAAABgAAAAAtwAHeENvb3JkMQAAAAoAAABKAAAAGgAAAAC3AAd5Q29vcmQxAAAACgAAAEoAAAAMAAAAALcAAmExAEUobTE2K20yNikvKHhDb29yZDEteENvb3JkMSleMisyKih5Q29vcmQxLXlDb29yZDEpLyh4Q29vcmQxLXhDb29yZDEpXjMAAAAUAAAAABQDAAAAFAAAAAATAAAAtQAAABMAAAC2AAAAHQAAABQBAAAAEwAAALoAAAATAAAAuAAAAAFAAAAAAAAAAAAAABQDAAAAFAIAAAABQAAAAAAAAAAAAAAUAQAAABMAAAC5AAAAEwAAALsAAAAdAAAAFAEAAAATAAAAugAAABMAAAC4AAAAAUAIAAAAAAAAAAAADAAAAAC3AAJiMQBFKDIqbTE2K20yNikvKHhDb29yZDEteENvb3JkMSkrMyooeUNvb3JkMS15Q29vcmQxKS8oeENvb3JkMS14Q29vcmQxKV4yAAAAFAAAAAAUAwAAABQAAAAAFAIAAAABQAAAAAAAAAAAAAATAAAAtQAAABMAAAC2AAAAFAEAAAATAAAAuAAAABMAAAC6AAAAFAMAAAAUAgAAAAFACAAAAAAAAAAAABQBAAAAEwAAALsAAAATAAAAuQAAAB0AAAAUAQAAABMAAAC6AAAAEwAAALgAAAABQAAAAAAAAAAAAAAeAQAAALcAAmY1ADlhMSoodC14Q29vcmQxKV4zK2IxKih0LXhDb29yZDEpXjIrbTE2Kih0LXhDb29yZDEpK3lDb29yZDEAAAAUAAAAABQAAAAAFAAAAAAUAgAAABMAAAC8AAAAHQAAABQBAAAAHwAAAAAAAAATAAAAuAAAAAFACAAAAAAAAAAAABQCAAAAEwAAAL0AAAAdAAAAFAEAAAAfAAAAAAAAABMAAAC4AAAAAUAAAAAAAAAAAAAAFAIAAAATAAAAtQAAABQBAAAAHwAAAAAAAAATAAAAuAAAABMAAAC5AAF0AAAAEgAAAAC3AQAAAAANAAABBQAAAAAKAAAAEwAAALgAAAABAAAAAAAAAAAAAAASAAAAALcBAAAAAA0AAAEFAAAAAAoAAAATAAAAugAAAAEAAAAAAAAAAAAAABcAAAAAtwEAAAAADQAAAQABAAAAvwAAAMAAAAAEAAAAALcBAAAAAA0AAngxAQUAAT#NchQxDYowAAAAwQAAABgAAAAAtwAHeENvb3JkMQAAAAoAAADCAAAADAAAAAC3AAN4MTQAB3hDb29yZDEAAAATAAAAwwAAAAwAAAAAtwADeTE0AAdmNSh4MTQpAAAAIAAAAL4AAAATAAAAxAAAABIAAAAAtwEAAAAADQAAAQUAAAAACgAAABMAAADEAAAAEwAAAMUAAAAhAQAAALcAAAAAAAEAAADGAAABLAABAAAAwgAAAAUAAADCAAAAwwAAAMQAAADFAAAAxgAAAAwA#####wADbTE3AAJtNgAAABMAAABTAAAADAD#####AANtMjcAAm03AAAAEwAAAFQAAAANAP####8AIENvdXJiZVBhcjJQb2ludHNBdmVjQ29lZmZEaXJUYW5nAAAADgAAAAIAAAAFAAAAyAAAAMkAAAAKAAAASgAAAEsAAAAYAAAAAMoAB3hDb29yZDEAAAAKAAAASgAAABoAAAAAygAHeUNvb3JkMQAAAAoAAABKAAAAGAAAAADKAAd4Q29vcmQxAAAACgAAAEsAAAAaAAAAAMoAB3lDb29yZDEAAAAKAAAASwAAAAwAAAAAygACYTEARShtMTcrbTI3KS8oeENvb3JkMS14Q29vcmQxKV4yKzIqKHlDb29yZDEteUNvb3JkMSkvKHhDb29yZDEteENvb3JkMSleMwAAABQAAAAAFAMAAAAUAAAAABMAAADIAAAAEwAAAMkAAAAdAAAAFAEAAAATAAAAzQAAABMAAADLAAAAAUAAAAAAAAAAAAAAFAMAAAAUAgAAAAFAAAAAAAAAAAAAABQBAAAAEwAAAMwAAAATAAAAzgAAAB0AAAAUAQAAABMAAADNAAAAEwAAAMsAAAABQAgAAAAAAAAAAAAMAAAAAMoAAmIxAEUoMiptMTcrbTI3KS8oeENvb3JkMS14Q29vcmQxKSszKih5Q29vcmQxLXlDb29yZDEpLyh4Q29vcmQxLXhDb29yZDEpXjIAAAAUAAAAABQDAAAAFAAAAAAUAgAAAAFAAAAAAAAAAAAAABMAAADIAAAAEwAAAMkAAAAUAQAAABMAAADLAAAAEwAAAM0AAAAUAwAAABQCAAAAAUAIAAAAAAAAAAAAFAEAAAATAAAAzgAAABMAAADMAAAAHQAAABQBAAAAEwAAAM0AAAATAAAAywAAAAFAAAAAAAAAAAAAAB4BAAAAygACZjYAOWExKih0LXhDb29yZDEpXjMrYjEqKHQteENvb3JkMSleMittMTcqKHQteENvb3JkMSkreUNvb3JkMQAAABQAAAAAFAAAAAAUAAAAABQCAAAAEwAAAM8AAAAdAAAAFAEAAAAfAAAAAAAAABMAAADLAAAAAUAIAAAAAAAAAAAAFAIAAAATAAAA0AAAAB0AAAAUAQAAAB8AAAAAAAAAEwAAAMsAAAABQAAAAAAAAAAAAAAUAgAAABMAAADIAAAAFAEAAAAfAAAAAAAAABMAAADLAAAAEwAAAMwAAXQAAAASAAAAAMoBAAAAAA0AAAEFAAAAAAoAAAATAAAAywAAAAEAAAAAAAAAAAAAABIAAAAAygEAAAAADQAAAQUAAAAACgAAABMAAADNAAAAAQAAAAAAAAAAAAAAFwAAAADKAQAAAAANAAABAAEAAADSAAAA0wAAAAQAAAAAygEAAAAADQACeDEBBQABP81yFDENijAAAADUAAAAGAAAAADKAAd4Q29vcmQxAAAACgAAANUAAAAMAAAAAMoAA3gxNQAHeENvb3JkMQAAABMAAADWAAAADAAAAADKAAN5MTUAB2Y2KHgxNSkAAAAgAAAA0QAAABMAAADXAAAAEgAAAADKAQAAAAANAAABBQAAAAAKAAAAEwAAANcAAAATAAAA2AAAACEBAAAAygAAAAAAAQAAANkAAAEsAAEAAADVAAAABQAAANUAAADWAAAA1wAAANgAAADZAAAADAD#####AANtMTgAAm03AAAAEwAAAFQAAAAMAP####8AA20yOAACbTgAAAATAAAAVQAAAA0A#####wAgQ291cmJlUGFyMlBvaW50c0F2ZWNDb2VmZkRpclRhbmcAAAAOAAAAAgAAAAUAAADbAAAA3AAAAAoAAABLAAAATAAAABgAAAAA3QAHeENvb3JkMQAAAAoAAABLAAAAGgAAAADdAAd5Q29vcmQxAAAACgAAAEsAAAAYAAAAAN0AB3hDb29yZDEAAAAKAAAATAAAABoAAAAA3QAHeUNvb3JkMQAAAAoAAABMAAAADAAAAADdAAJhMQBFKG0xOCttMjgpLyh4Q29vcmQxLXhDb29yZDEpXjIrMiooeUNvb3JkMS15Q29vcmQxKS8oeENvb3JkMS14Q29vcmQxKV4zAAAAFAAAAAAUAwAAABQAAAAAEwAAANsAAAATAAAA3AAAAB0AAAAUAQAAABMAAADgAAAAEwAAAN4AAAABQAAAAAAAAAAAAAAUAwAAABQCAAAAAUAAAAAAAAAAAAAAFAEAAAATAAAA3wAAABMAAADhAAAAHQAAABQBAAAAEwAAAOAAAAATAAAA3gAAAAFACAAAAAAAAAAAAAwAAAAA3QACYjEARSgyKm0xOCttMjgpLyh4Q29vcmQxLXhDb29yZDEpKzMqKHlDb29yZDEteUNvb3JkMSkvKHhDb29yZDEteENvb3JkMSleMgAAABQAAAAAFAMAAAAUAAAAABQCAAAAAUAAAAAAAAAAAAAAEwAAANsAAAATAAAA3AAAABQBAAAAEwAAAN4AAAATAAAA4AAAABQDAAAAFAIAAAABQAgAAAAAAAAAAAAUAQAAABMAAADhAAAAEwAAAN8AAAAdAAAAFAEAAAATAAAA4AAAABMAAADeAAAAAUAAAAAAAAAAAAAAHgEAAADdAAJmNwA5YTEqKHQteENvb3JkMSleMytiMSoodC14Q29vcmQxKV4yK20xOCoodC14Q29vcmQxKSt5Q29vcmQxAAAAFAAAAAAUAAAAABQAAAAAFAIAAAATAAAA4gAAAB0AAAAUAQAAAB8AAAAAAAAAEwAAAN4AAAABQAgAAAAAAAAAAAAUAgAAABMAAADjAAAAHQAAABQBAAAAHwAAAAAAAAATAAAA3gAAAAFAAAAAAAAAAAAAABQCAAAAEwAAANsAAAAUAQAAAB8AAAAAAAAAEwAAAN4AAAATAAAA3wABdAAAABIAAAAA3QEAAAAADQAAAQUAAAAACgAAABMAAADeAAAAAQAAAAAAAAAAAAAAEgAAAADdAQAAAAANAAABBQAAAAAKAAAAEwAAAOAAAAABAAAAAAAAAAAAAAAXAAAAAN0BAAAAAA0AAAEAAQAAAOUAAADmAAAABAAAAADdAQAAAAANAAJ4MQEFAAE#zXIUMQ2KMAAAAOcAAAAYAAAAAN0AB3hDb29yZDEAAAAKAAAA6AAAAAwAAAAA3QADeDE2AAd4Q29vcmQxAAAAEwAAAOkAAAAMAAAAAN0AA3kxNgAHZjcoeDE2KQAAACAAAADkAAAAEwAAAOoAAAASAAAAAN0BAAAAAA0AAAEFAAAAAAoAAAATAAAA6gAAABMAAADrAAAAIQEAAADdAAAAAAABAAAA7AAAASwAAQAAAOgAAAAFAAAA6AAAAOkAAADqAAAA6wAAAOwAAAAMAP####8AAnoxAAItNAAAABwAAAABQBAAAAAAAAAAAAAMAP####8AAnoyAAItMgAAABwAAAABQAAAAAAAAAAAAAAMAP####8AAnozAAEyAAAAAUAAAAAAAAAAAAAADAD#####AAJ6NAABNQAAAAFAFAAAAAAAAAAAABIA#####wAAAP8ADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAATAAAA7gAAAAEAAAAAAAAAAAAAABIA#####wAAAP8ADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAATAAAA7wAAAAEAAAAAAAAAAAAAABIA#####wAAAP8ADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAATAAAA8AAAAAEAAAAAAAAAAAAAABIA#####wAAAP8ADwAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAATAAAA8QAAAAEAAAAAAAAAAP####8AAAABAA9DVmFsZXVyQWZmaWNoZWUA#####wAAAP8AAAAAAAAAAABAIgAAAAAAAAAAAPIUAAH2+v4AAAABAAAAAAAAAAACAAAA7gAAACIA#####wAAAP8Av#AAAAAAAABAJAAAAAAAAAAAAPMUAAH2+v4AAAABAAAAAAAAAAACAAAA7wAAACIA#####wAAAP8AQAAAAAAAAABAJgAAAAAAAAAAAPQUAAH2+v4AAAABAAAAAAAAAAACAAAA8AAAACIA#####wAAAP8AP#AAAAAAAABAJgAAAAAAAAAAAPUUAAH2+v4AAAABAAAAAAAAAAACAAAA8QAAAAv##########w=='
    stor.mtgAppLecteur.addDoc('mtg32svg', txtFigure, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true);
    callback()
  }

  function modifFig (st) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété correction (booléen)
    if (st.correction) {
      // on affiche la correction
      for (let k = 1; k <= st.donnees.valZeros.length; k++) {
        stor.mtgAppLecteur.giveFormula2('mtg32svg', 'z' + k, String(st.donnees.valZeros[k]))
      }
    } else {
      for (let k = 0; k <= 8; k++) {
        stor.mtgAppLecteur.giveFormula2('mtg32svg', 'x' + k, String(st.donnees.abscisses[k]))
        stor.mtgAppLecteur.giveFormula2('mtg32svg', 'y' + k, String(st.donnees.ordonnees[k]))
        stor.mtgAppLecteur.giveFormula2('mtg32svg', 'm' + k, String(st.donnees.coefDir[k]))
      }
      for (let k = 0; k <= 4; k++) {
        stor.mtgAppLecteur.giveFormula2('mtg32svg', 'z' + k, '-15')
      }
    }
    stor.mtgAppLecteur.calculate('mtg32svg', true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display('mtg32svg')// on affiche la nouvelle figure
  }

  function suite () {
    stor.numEssai = 1
    // OBLIGATOIRE
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    /// /////////////////////////////////////// Pour MathGraph32
    const largFig = 729// on gère ici la largeur de la figure
    const hautFig = 375
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px', color: me.styles.petit.enonce.color }) })
    for (let i = 1; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.nom_f = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, { f: stor.nom_f })
    // création du div accueillant la figure mtg32 et enfin la zone svg (très importante car tout est dedans)
    j3pCreeSVG(stor.zoneCons2, { id: 'mtg32svg', width: largFig, height: hautFig })
    depart(suite2)
  }

  function creationDonnees (nbZeros) {
    // cette fonction génère les données suivant le nombre de zéros attendu
    // elle renvoie un objet nommé obj
    // obj.abscisses est une tableau contenant les abscisses de tous les points précisément placés
    // obj.ordonnees est une tableau contenant les ordonnées de tous les points précisément placés
    // obj.coefDir est un tableau contenant les coefficients directeurs utiles pour tracer la courbe
    // obj.signes est un tableau contenant tous les signes attendus dans la réponse
    // obj.valZeros est le tableau contenant tous les zéros de la fonction ainsi que les bornes du domaine(dans l’ordre croissant)
    const obj = {}
    let i, j, k, pioche
    obj.abscisses = []
    obj.ordonnees = []
    obj.coefDir = []
    obj.signes = []
    obj.valZeros = []
    obj.valVar = []// pour identifier la confusion avec le tableau de variation
    let absInit
    let absFin
    let pos
    let ecartMin
    let ecartMax
    let choixAbsInit = []
    let choixAbs = []

    function prendreAbscisses (num) {
      function remplirTableau (fin) {
        // come je vais faire ceci pour tous les cas de figure, autant en faire une fonction
        while (pos < fin) {
          pos++
          choixAbsInit.push(pos)
        }
      }

      if (num === 2) {
        do {
          absInit = j3pGetRandomInt(-6, -4)
          absFin = j3pGetRandomInt(4, 6)
          pos = absInit + 3
          choixAbsInit = [pos]
          remplirTableau(absFin - 2)
          choixAbs = [...choixAbsInit]
          obj.abscisses = [absInit, absFin]
          for (j = 1; j <= num; j++) {
            pioche = j3pGetRandomInt(0, (choixAbs.length - 1))
            obj.abscisses.push(choixAbs[pioche])
            choixAbs.splice(pioche, 1)
          }
        } while ((Math.abs(obj.abscisses[2] - obj.abscisses[3]) < 2) || (Math.abs(obj.abscisses[2] - obj.abscisses[3]) > 5))
      } else {
        let ecartMinAdmis, ecartMaxAdmis
        do {
          if (num === 3) {
            absInit = j3pGetRandomInt(-7, -5)
            absFin = j3pGetRandomInt(5, 8)
          } else if ((num === 4) || (num === 5)) {
            absInit = j3pGetRandomInt(-8, -6)
            absFin = j3pGetRandomInt(5, 8)
          } else if ((num === 6) || (num === 7)) {
            absInit = j3pGetRandomInt(-9, -7)
            absFin = j3pGetRandomInt(6, 9)
          }
          pos = (num >= 6) ? absInit + 1 : absInit + 3
          ecartMinAdmis = (num >= 6) ? 1 : 2
          ecartMaxAdmis = (num >= 6) ? 4 : 5
          choixAbsInit = [pos]
          remplirTableau(absFin - 2)
          choixAbs = [...choixAbsInit]
          obj.abscisses = [absInit, absFin]
          ecartMin = absFin - absInit
          ecartMax = 0
          for (j = 1; j <= num; j++) {
            pioche = j3pGetRandomInt(0, (choixAbs.length - 1))
            for (k = 0; k < obj.abscisses.length; k++) {
              ecartMin = Math.min(Math.abs(obj.abscisses[k] - choixAbs[pioche]), ecartMin)
            }
            obj.abscisses.push(choixAbs[pioche])
            choixAbs.splice(pioche, 1)
          }
          const tabOrdonne = obj.abscisses.sort(function (x, y) {
            return x - y
          })
          for (k = 0; k < tabOrdonne.length - 1; k++) {
            ecartMax = Math.max(tabOrdonne[k + 1] - tabOrdonne[k], ecartMax)
          }
        } while ((ecartMin < ecartMinAdmis) || (ecartMax > ecartMaxAdmis))
      }
      // console.log("obj.abscisses avant:",obj.abscisses)
      obj.abscisses = obj.abscisses.sort(function (x, y) {
        return x - y
      })
    }

    function multCoefMoinsUn () {
      for (let k = 0; k < obj.ordonnees.length; k++) {
        obj.ordonnees[k] = obj.ordonnees[k] * stor.coefMoinsUn
        obj.coefDir[k] = obj.coefDir[k] * stor.coefMoinsUn
      }
    }

    stor.coefMoinsUn = j3pGetRandomInt(0, 1) * 2 - 1
    let casFigure
    switch (nbZeros) {
      case 0:
        prendreAbscisses(2)
        obj.ordonnees.push(j3pGetRandomInt(5, 15) / 10, j3pGetRandomInt(25, 42) / 10, j3pGetRandomInt(5, 15) / 10, j3pGetRandomInt(25, 35) / 10)
        obj.coefDir.push(j3pGetRandomInt(2, 9) / 10, 0, 0, j3pGetRandomInt(4, 14) / 10)
        multCoefMoinsUn()
        if (stor.coefMoinsUn === 1) {
          obj.signes.push('+')
        } else {
          obj.signes.push('-')
        }
        obj.valVar = [absInit, absFin, obj.abscisses[2], obj.abscisses[3]].sort(function (x, y) {
          return x - y
        })
        // on a 4 points seulement
        // Tous les autres seront confondus avec le dernier
        break
      case 1:
        if (ds.zeroSansChgtSigne) {
          casFigure = j3pGetRandomInt(0, 1)
          prendreAbscisses(2)
          switch (casFigure) {
            case 0:
              obj.ordonnees.push(j3pGetRandomInt(30, 40) / 10, 0, j3pGetRandomInt(35, 45) / 10, j3pGetRandomInt(15, 23) / 10)
              obj.coefDir.push(j3pGetRandomInt(-14, -4) / 10, 0, 0, j3pGetRandomInt(-15, -7) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[1], obj.abscisses[2]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1])
              break
            default :
              obj.ordonnees.push(j3pGetRandomInt(12, 25) / 10, j3pGetRandomInt(35, 45) / 10, 0, j3pGetRandomInt(25, 40) / 10)
              obj.coefDir.push(j3pGetRandomInt(2, 9) / 10, 0, 0, j3pGetRandomInt(5, 15) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[1], obj.abscisses[2]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[2])
              break
          }
          if (stor.coefMoinsUn === 1) {
            obj.signes.push('+', '+')
          } else {
            obj.signes.push('-', '-')
          }
        } else {
          casFigure = j3pGetRandomInt(0, 3)
          switch (casFigure) {
            case 0:
              prendreAbscisses(2)
              obj.ordonnees.push(j3pGetRandomInt(-40, -30) / 10, 0, j3pGetRandomInt(35, 45) / 10, j3pGetRandomInt(8, 23) / 10)
              obj.coefDir.push(j3pGetRandomInt(2, 14) / 10, j3pGetRandomInt(5, 10) / 10, 0, j3pGetRandomInt(-15, -7) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[2]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1])
              break
            case 1:
              prendreAbscisses(2)
              obj.ordonnees.push(j3pGetRandomInt(-20, -10) / 10, j3pGetRandomInt(-40, -28) / 10, 0, j3pGetRandomInt(18, 40) / 10)
              obj.coefDir.push(j3pGetRandomInt(-12, -6) / 10, 0, j3pGetRandomInt(10, 19) / 10, j3pGetRandomInt(3, 12) / 10)
              obj.valVar = [absInit, absFin, obj.abscisses[1]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[2])
              multCoefMoinsUn()
              break
            case 2:
              prendreAbscisses(3)
              obj.ordonnees.push(j3pGetRandomInt(-40, -30) / 10, 0, j3pGetRandomInt(30, 40) / 10, j3pGetRandomInt(10, 12) / 10, j3pGetRandomInt(30, 45) / 10)
              obj.coefDir.push(j3pGetRandomInt(10, 14) / 10, j3pGetRandomInt(7, 14) / 10, 0, 0, j3pGetRandomInt(5, 13) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[2], obj.abscisses[3]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1])
              break
            default:
              prendreAbscisses(3)
              obj.ordonnees.push(j3pGetRandomInt(-40, -30) / 10, j3pGetRandomInt(-13, -6) / 10, j3pGetRandomInt(-38, -27) / 10, 0, j3pGetRandomInt(20, 40) / 10)
              obj.coefDir.push(j3pGetRandomInt(10, 14) / 10, 0, 0, j3pGetRandomInt(10, 19) / 10, j3pGetRandomInt(10, 25) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[1], obj.abscisses[2]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[3])
              break
          }
          if (stor.coefMoinsUn === 1) {
            obj.signes.push('-', '+')
          } else {
            obj.signes.push('+', '-')
          }
        }
        break
      case 2:
        if (ds.zeroSansChgtSigne) {
          casFigure = j3pGetRandomInt(0, 1)
          prendreAbscisses(3)
          switch (casFigure) {
            case 0:
              obj.ordonnees.push(j3pGetRandomInt(20, 35) / 10, 0, j3pGetRandomInt(15, 30) / 10, 0, j3pGetRandomInt(-40, -28) / 10)
              obj.coefDir.push(j3pGetRandomInt(-12, -6) / 10, 0, 0, j3pGetRandomInt(-14, -5) / 10, j3pGetRandomInt(-9, -6) / 10)
              obj.valVar = [absInit, absFin, obj.abscisses[1], obj.abscisses[2]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3])
              multCoefMoinsUn()
              if (stor.coefMoinsUn === 1) {
                obj.signes.push('+', '+', '-')
              } else {
                obj.signes.push('-', '-', '+')
              }
              break
            default:
              obj.ordonnees.push(j3pGetRandomInt(20, 35) / 10, 0, j3pGetRandomInt(-38, -29) / 10, 0, j3pGetRandomInt(-40, -25) / 10)
              obj.coefDir.push(j3pGetRandomInt(-12, -6) / 10, j3pGetRandomInt(-12, -6) / 10, 0, 0, j3pGetRandomInt(-8, -4) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[2], obj.abscisses[3]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3])
              if (stor.coefMoinsUn === 1) {
                obj.signes.push('+', '-', '-')
              } else {
                obj.signes.push('-', '+', '+')
              }
              break
          }
        } else {
          casFigure = j3pGetRandomInt(0, 2)
          switch (casFigure) {
            case 0:
              prendreAbscisses(3)
              obj.ordonnees.push(j3pGetRandomInt(30, 45) / 10, 0, j3pGetRandomInt(-37, -25) / 10, 0, j3pGetRandomInt(20, 36) / 10)
              obj.coefDir.push(j3pGetRandomInt(-10, -5) / 10, j3pGetRandomInt(-12, -5) / 10, 0, j3pGetRandomInt(5, 12) / 10, j3pGetRandomInt(4, 11) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[2]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3])
              break
            case 1:
              prendreAbscisses(4)
              obj.ordonnees.push(j3pGetRandomInt(20, 35) / 10, 0, j3pGetRandomInt(-35, -22) / 10, 0, j3pGetRandomInt(35, 42) / 10, j3pGetRandomInt(10, 20) / 10)
              obj.coefDir.push(j3pGetRandomInt(-12, -6) / 10, j3pGetRandomInt(-12, -6) / 10, 0, j3pGetRandomInt(8, 16) / 10, 0, j3pGetRandomInt(-10, -6) / 10)
              obj.valVar = [absInit, absFin, obj.abscisses[2], obj.abscisses[4]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3])
              multCoefMoinsUn()
              break
            default:
              prendreAbscisses(5)
              obj.ordonnees.push(j3pGetRandomInt(8, 12) / 10, j3pGetRandomInt(32, 40) / 10, 0, j3pGetRandomInt(-25, -15) / 10, 0, j3pGetRandomInt(30, 43) / 10, j3pGetRandomInt(10, 17) / 10)
              obj.coefDir.push(j3pGetRandomInt(8, 13) / 10, 0, j3pGetRandomInt(-13, -6) / 10, 0, j3pGetRandomInt(6, 15) / 10, 0, j3pGetRandomInt(-15, -6) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[1], obj.abscisses[3], obj.abscisses[5]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[2], obj.abscisses[4])
              break
          }
          if (stor.coefMoinsUn === 1) {
            obj.signes.push('+', '-', '+')
          } else {
            obj.signes.push('-', '+', '-')
          }
        }
        break
      case 3:
        if (ds.zeroSansChgtSigne) {
          casFigure = j3pGetRandomInt(0, 1)
          prendreAbscisses(5)
          switch (casFigure) {
            case 0:
              obj.ordonnees.push(j3pGetRandomInt(-40, -30) / 10, 0, j3pGetRandomInt(19, 30) / 10, 0, j3pGetRandomInt(19, 30) / 10, 0, j3pGetRandomInt(-40, -28) / 10)
              obj.coefDir.push(j3pGetRandomInt(5, 12) / 10, j3pGetRandomInt(5, 10) / 10, 0, 0, 0, j3pGetRandomInt(-14, -5) / 10, j3pGetRandomInt(-9, -6) / 10)
              obj.valVar = [absInit, absFin, obj.abscisses[2], obj.abscisses[3], obj.abscisses[4]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3], obj.abscisses[5])
              multCoefMoinsUn()
              if (stor.coefMoinsUn === 1) {
                obj.signes.push('-', '+', '+', '-')
              } else {
                obj.signes.push('+', '-', '-', '+')
              }
              break
            default:
              obj.ordonnees.push(j3pGetRandomInt(20, 35) / 10, 0, j3pGetRandomInt(20, 35) / 10, 0, j3pGetRandomInt(-40, -25) / 10, 0, j3pGetRandomInt(18, 30) / 10)
              obj.coefDir.push(j3pGetRandomInt(-12, -6) / 10, 0, 0, j3pGetRandomInt(-12, -6) / 10, 0, j3pGetRandomInt(4, 10) / 10, j3pGetRandomInt(4, 10) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[1], obj.abscisses[2], obj.abscisses[4]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3], obj.abscisses[5])
              if (stor.coefMoinsUn === 1) {
                obj.signes.push('+', '+', '-', '+')
              } else {
                obj.signes.push('-', '-', '+', '-')
              }
              break
          }
        } else {
          casFigure = j3pGetRandomInt(0, 2)
          switch (casFigure) {
            case 0:
              prendreAbscisses(5)
              obj.ordonnees.push(j3pGetRandomInt(30, 45) / 10, 0, j3pGetRandomInt(-25, -15) / 10, 0, j3pGetRandomInt(20, 36) / 10, 0, j3pGetRandomInt(-35, -20) / 10)
              obj.coefDir.push(j3pGetRandomInt(-10, -5) / 10, j3pGetRandomInt(-12, -8) / 10, 0, j3pGetRandomInt(5, 12) / 10, 0, j3pGetRandomInt(-20, -12) / 10, j3pGetRandomInt(-9, -4) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[2], obj.abscisses[4]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3], obj.abscisses[5])
              break
            case 1:
              prendreAbscisses(6)
              obj.ordonnees.push(j3pGetRandomInt(30, 45) / 10, 0, j3pGetRandomInt(-25, -15) / 10, 0, j3pGetRandomInt(20, 36) / 10, 0, j3pGetRandomInt(-40, -25) / 10, j3pGetRandomInt(-20, -10) / 10)
              obj.coefDir.push(j3pGetRandomInt(-10, -5) / 10, j3pGetRandomInt(-12, -8) / 10, 0, j3pGetRandomInt(5, 12) / 10, 0, j3pGetRandomInt(-20, -12) / 10, 0, j3pGetRandomInt(7, 15) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[2], obj.abscisses[4], obj.abscisses[6]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3], obj.abscisses[5])
              break
            default:
              prendreAbscisses(7)
              obj.ordonnees.push(j3pGetRandomInt(30, 45) / 10, 0, j3pGetRandomInt(-25, -15) / 10, 0, j3pGetRandomInt(30, 40) / 10, j3pGetRandomInt(14, 20) / 10, j3pGetRandomInt(30, 40) / 10, 0, j3pGetRandomInt(-35, -25) / 10)
              obj.coefDir.push(j3pGetRandomInt(-10, -5) / 10, j3pGetRandomInt(-12, -8) / 10, 0, j3pGetRandomInt(5, 12) / 10, 0, 0, 0, j3pGetRandomInt(-20, -12) / 10, j3pGetRandomInt(-9, -4) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[2], obj.abscisses[4], obj.abscisses[5], obj.abscisses[6]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3], obj.abscisses[7])
              break
          }
          if (stor.coefMoinsUn === 1) {
            obj.signes.push('+', '-', '+', '-')
          } else {
            obj.signes.push('-', '+', '-', '+')
          }
        }
        break
      default:
        // case=4
        prendreAbscisses(7)
        if (ds.zeroSansChgtSigne) {
          casFigure = j3pGetRandomInt(0, 2)
          switch (casFigure) {
            case 0:
              obj.ordonnees.push(j3pGetRandomInt(30, 45) / 10, 0, j3pGetRandomInt(25, 35) / 10, 0, j3pGetRandomInt(-25, -13) / 10, 0, j3pGetRandomInt(13, 20) / 10, 0, j3pGetRandomInt(-30, -22) / 10)
              obj.coefDir.push(j3pGetRandomInt(-14, -8) / 10, 0, 0, j3pGetRandomInt(-12, -5) / 10, 0, j3pGetRandomInt(4, 13) / 10, 0, j3pGetRandomInt(-11, -6) / 10, j3pGetRandomInt(-15, -9) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[1], obj.abscisses[2], obj.abscisses[4], obj.abscisses[6]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3], obj.abscisses[5], obj.abscisses[7])
              if (stor.coefMoinsUn === 1) {
                obj.signes.push('+', '+', '-', '+', '-')
              } else {
                obj.signes.push('-', '-', '+', '-', '+')
              }
              break
            case 1:
              obj.ordonnees.push(j3pGetRandomInt(30, 45) / 10, 0, j3pGetRandomInt(-27, -20) / 10, 0, j3pGetRandomInt(15, 23) / 10, 0, j3pGetRandomInt(17, 28) / 10, 0, j3pGetRandomInt(-30, -22) / 10)
              obj.coefDir.push(j3pGetRandomInt(-14, -8) / 10, j3pGetRandomInt(-12, -6) / 10, 0, j3pGetRandomInt(5, 12) / 10, 0, 0, 0, j3pGetRandomInt(-11, -6) / 10, j3pGetRandomInt(-15, -9) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[2], obj.abscisses[4], obj.abscisses[5], obj.abscisses[6]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3], obj.abscisses[5], obj.abscisses[7])
              if (stor.coefMoinsUn === 1) {
                obj.signes.push('+', '-', '+', '+', '-')
              } else {
                obj.signes.push('-', '+', '-', '-', '+')
              }
              break
            default :
              obj.ordonnees.push(j3pGetRandomInt(30, 45) / 10, 0, j3pGetRandomInt(-27, -20) / 10, 0, j3pGetRandomInt(15, 23) / 10, 0, j3pGetRandomInt(-22, -15) / 10, 0, j3pGetRandomInt(-30, -22) / 10)
              obj.coefDir.push(j3pGetRandomInt(-14, -8) / 10, j3pGetRandomInt(-12, -6) / 10, 0, j3pGetRandomInt(5, 12) / 10, 0, j3pGetRandomInt(-11, -6) / 10, 0, 0, j3pGetRandomInt(-15, -9) / 10)
              multCoefMoinsUn()
              obj.valVar = [absInit, absFin, obj.abscisses[2], obj.abscisses[4], obj.abscisses[6], obj.abscisses[7]].sort(function (x, y) {
                return x - y
              })
              obj.valZeros.push(obj.abscisses[1], obj.abscisses[3], obj.abscisses[5], obj.abscisses[7])
              if (stor.coefMoinsUn === 1) {
                obj.signes.push('+', '-', '+', '-', '-')
              } else {
                obj.signes.push('-', '+', '-', '+', '+')
              }
              break
          }
        } else {
          obj.ordonnees.push(j3pGetRandomInt(30, 45) / 10, 0, j3pGetRandomInt(-25, -15) / 10, 0, j3pGetRandomInt(25, 40) / 10, 0, j3pGetRandomInt(-25, -15) / 10, 0, j3pGetRandomInt(15, 30) / 10)
          obj.coefDir.push(j3pGetRandomInt(-10, -5) / 10, j3pGetRandomInt(-12, -8) / 10, 0, j3pGetRandomInt(5, 12) / 10, 0, j3pGetRandomInt(-15, -8) / 10, 0, j3pGetRandomInt(8, 15) / 10, j3pGetRandomInt(5, 13) / 10)
          multCoefMoinsUn()
          obj.valVar = [absInit, absFin, obj.abscisses[2], obj.abscisses[4], obj.abscisses[6]].sort(function (x, y) {
            return x - y
          })
          obj.valZeros.push(obj.abscisses[1], obj.abscisses[3], obj.abscisses[5], obj.abscisses[7])
          if (stor.coefMoinsUn === 1) {
            obj.signes.push('+', '-', '+', '-', '+')
          } else {
            obj.signes.push('-', '+', '-', '+', '-')
          }
        }
        break
    }
    me.logIfDebug('obj.valZeros:', obj.valZeros)
    const tabDernierPoint = [obj.abscisses[obj.abscisses.length - 1], obj.ordonnees[obj.ordonnees.length - 1], obj.coefDir[obj.coefDir.length - 1]]
    for (i = obj.abscisses.length; i < 9; i++) {
      obj.abscisses.push(tabDernierPoint[0])
      obj.ordonnees.push(tabDernierPoint[1])
      obj.coefDir.push(tabDernierPoint[2])
    }
    me.logIfDebug('obj.abscisses:', obj.abscisses, '  obj.ordonnees:', obj.ordonnees, '   obj.coefDir:', obj.coefDir)
    return obj
  }

  function suite2 () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    j3pAffiche(stor.zoneCons3, '', textes.consigne2, { f: stor.nom_f })

    const objDonnees = creationDonnees(stor.tabNbZeros[me.questionCourante - 1])
    modifFig({ correction: false, donnees: objDonnees })
    // création du tableau de signes
    stor.divAjoute = j3pAddElt(stor.zoneCons4, 'div')
    stor.divEnleve = j3pAddElt(stor.zoneCons4, 'div')
    constructionBtnAjoute()
    stor.donneesTableau = {}
    stor.donneesTableau.tab_dimension = [95 * me.zonesElts.HG.getBoundingClientRect().width / 100, 100, 0, [objDonnees.abscisses[0], objDonnees.abscisses[8]], [objDonnees.abscisses[0], objDonnees.abscisses[8], '[', ']']]
    stor.donneesTableau.obj_style_couleur = {
      texte: textes.cons_tab1,
      nom_f: stor.nom_f,
      style: me.styles.toutpetit.enonce
    }
    stor.donneesTableau.obj_style_couleur.style.color = me.styles.petit.enonce.color
    stor.donneesTableau.signe_tab_corr = objDonnees.signes
    stor.donneesTableau.val_zero_tab = objDonnees.valZeros
    stor.donneesTableau.valVar = objDonnees.valVar
    stor.donneesTableau.zero_tab = [false, false, false]
    j3pStyle(stor.zoneCons4, { paddingBottom: '10px' })
    stor.leTableau = j3pAddElt(stor.zoneCons5, 'div', '', {
      style: me.styles.etendre('toutpetit.enonce', { position: 'relative', top: 0, left: 0 })
    })
    stor.tabSigne = tableauSignesFct(stor.leTableau, stor.donneesTableau)
    stor.nb_valx = 0
    declarationZoneAValider()
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })//, coord:[0,0]

    // Obligatoire
    me.finEnonce()
  }

  function constructionBtnAjoute () {
    stor.ajoute = j3pAddElt(stor.divAjoute, 'div')
    j3pAffiche(stor.ajoute, '', textes.phrase1)
    j3pAjouteBouton(stor.ajoute, j3pGetNewId('bouton1'), 'MepBoutons2', '+', actionBtnPlus)
  }

  function constructionBtnEnleve () {
    stor.enleve = j3pAddElt(stor.divEnleve, 'div')
    j3pAffiche(stor.enleve, '', textes.phrase2)
    j3pAjouteBouton(stor.enleve, j3pGetNewId('bouton2'), 'MepBoutons2', '-', actionBtnMoins)
  }

  function actionBtnPlus () {
    // c’est fonction sera appelée pour recréer le tableau de signe avec une valeur de plus dans la première ligne
    stor.nb_valx++
    stor.donneesTableau.tab_dimension[2] = stor.nb_valx
    if (stor.nb_valx === 5) j3pDetruit(stor.ajoute)
    if (stor.nb_valx === 1) {
      // c’est que le btnEnleve n’était pas encore construit, donc on le fait.
      constructionBtnEnleve()
    }
    // on reconstruit ensuite le tableau de signes
    j3pEmpty(stor.zoneCons5)
    stor.leTableau = j3pAddElt(stor.zoneCons5, 'div', '', {
      style: me.styles.etendre('toutpetit.enonce', { position: 'relative', top: 0, left: 0 })
    })
    stor.tabSigne = tableauSignesFct(stor.leTableau, stor.donneesTableau)
    declarationZoneAValider()
  }

  function actionBtnMoins () {
    // c’est fonction sera appelée pour recréer le tableau de signe avec une valeur de moins dans la première ligne
    stor.nb_valx--
    stor.donneesTableau.tab_dimension[2] = stor.nb_valx
    if (stor.nb_valx === 0) j3pDetruit(stor.enleve)
    if (stor.nb_valx === 4) {
      // c’est que le btnAjoute n’était pas encore construit, donc on le fait.
      constructionBtnAjoute()
    }
    // on reconstruit ensuite le tableau de signes
    j3pEmpty(stor.zoneCons5)
    stor.leTableau = j3pAddElt(stor.zoneCons5, 'div', '', {
      style: me.styles.etendre('toutpetit.enonce', { position: 'relative', top: 0, left: 0 })
    })
    stor.tabSigne = tableauSignesFct(stor.leTableau, stor.donneesTableau)
    declarationZoneAValider()
  }

  function declarationZoneAValider () {
    // on récupère la liste avec le nom de toutes les zones de saisie ainsi que les listes déroulantes
    // stor.nb_valx+2 est le nombre de zones de saisie.
    const mesZonesSaisie = []
    const mesValidPerso = []
    stor.tabSigne.zone.forEach(elt => mesZonesSaisie.push(elt.id))
    stor.tabSigne.signe.forEach(elt => mesZonesSaisie.push(elt.id))
    stor.tabSigne.liste.forEach(elt => {
      mesZonesSaisie.push(elt.id)
      elt.typeReponse = [false]
    })
    j3pFocus(stor.tabSigne.zone[0], true)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesValidPerso })
  }

  function toutMettreEnRouge (objValid) {
    // je mets tout le tableau en rouge
    for (let i = 1; i <= stor.nb_valx + 1; i++) {
      me.logIfDebug('monsigne' + i + ':' + j3pValeurde(stor.tabSigne.zone[i]))
      objValid.zones.bonneReponse[stor.nb_valx + 1 + i] = false
      // j’ai mis toutes les zones de saisie accueillant un signe à bonneReponse = false
    }
    for (let i = 1; i <= stor.nb_valx; i++) {
      objValid.zones.bonneReponse[2 * stor.nb_valx + 2 + i] = false
      // je mets aussi toutes les listes à bonneReponse = false
    }
    // j’en profite pour tester ici s’il n’y a pas une confusion avec les variations en ne regardant que les valeurs complétées en première ligne
    let confusion = (stor.nb_valx + 2 === stor.donneesTableau.valVar.length)
    // je ne vérifie que les valeurs de la première ligne, les signes n’ont pas lieu d'être vérifiés
    if (confusion) {
      for (let i = 0; i <= stor.nb_valx + 1; i++) {
        confusion = (confusion && Math.abs(stor.donneesTableau.valVar[i] - j3pCalculValeur(objValid.zones.reponseSaisie[i])) < Math.pow(10, -13))
      }
    }
    return confusion
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        this.construitStructurePage('presentation1')
        this.afficheTitre(textes.titre_exo)
        if (ds.indication) this.indication(me.zonesElts.IG, ds.indication)
        ds.zeroSansChgtSigne = (String(ds.zeroSansChgtSigne) === 'true')
        stor.tabNbZeros = []
        if (isNaN(Number(ds.nbzero))) {
          // on n’a pas un nombre
          if (ds.nbzeros === 'alea') {
            // on met alors "alea" pour chacun des cas de figure
            for (let i = 0; i < ds.nbrepetitions; i++) {
              if (i === 0) {
                stor.tabNbZeros.push(j3pGetRandomInt(1, 2))
              } else if (i === 1) {
                let newChoix
                do {
                  newChoix = (ds.zeroSansChgtSigne) ? j3pRandomTab([1, 2, 3, 4], [0.2, 0.3, 0.3, 0.2]) : j3pRandomTab([0, 1, 2, 3, 4], [0.1, 0.2, 0.3, 0.3, 0.1])
                } while (Math.abs(stor.tabNbZeros[0] - newChoix) < Math.pow(10, -12))
                stor.tabNbZeros.push(newChoix)
              } else {
                const autreChoix = (ds.zeroSansChgtSigne) ? j3pRandomTab([1, 2, 3, 4], [0.2, 0.3, 0.3, 0.2]) : j3pRandomTab([0, 1, 2, 3, 4], [0.1, 0.2, 0.3, 0.3, 0.1])
                stor.tabNbZeros.push(autreChoix)
              }
            }
          } else {
            for (let i = 0; i < ds.nbrepetitions; i++) {
              if (isNaN(Number(ds.nbzero[i]))) {
                // c’est que les données sont aléatoires
                const monChoix = (ds.zeroSansChgtSigne) ? j3pRandomTab([1, 2, 3, 4], [0.2, 0.3, 0.3, 0.2]) : j3pRandomTab([0, 1, 2, 3, 4], [0.1, 0.2, 0.3, 0.3, 0.1])
                stor.tabNbZeros.push(monChoix)
              } else {
                stor.tabNbZeros.push(Number(ds.nbzero[i]))
              }
            }
          }
        } else {
          // dans ce cas, on met ce nombre pour chaque répétition
          for (let i = 0; i < ds.nbrepetitions; i++) {
            stor.tabNbZeros.push(Number(ds.nbzero))
          }
        }
        me.logIfDebug('stor.tabNbZeros:', stor.tabNbZeros)
        stor.scoreLigne1 = (typeof ds.scoreLigne1 === 'string') ? j3pNombre(ds.scoreLigne1) : ds.scoreLigne1
        if (stor.scoreLigne1 < 0 || stor.scoreLigne1 > 1) stor.scoreLigne1 = 0.75

        // chargement mtg
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              suite()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
        suite()
      }

      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const reponse = {}
      let confusionVariation = false
      const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      reponse.aRepondu = fctsValid.valideReponses()
      reponse.bonneReponse = false
      let bonNombreZeros, bonnePremiereLigne, bonEnsembleDefinition

      // on corrige s’il a répondu qqchose
      if (reponse.aRepondu) {
        bonNombreZeros = (stor.nb_valx === stor.donneesTableau.val_zero_tab.length)

        me.logIfDebug('stor.nb_valx:', stor.nb_valx, '  comparé à ', stor.donneesTableau.val_zero_tab.length, '  valeur des zéros :', stor.donneesTableau.val_zero_tab)
        if (bonNombreZeros) {
          // j’empêche alors l’ajout ou le retrait de valeurs
          // je m’occupe des réponses attendues dans les zones de la première ligne
          for (let i = 0; i <= stor.nb_valx + 1; i++) {
            if (i === 0) {
              // c’est la borne inf du domaine
              stor.tabSigne.zone[i].reponse = j3pCalculValeur(stor.donneesTableau.tab_dimension[3][0])
            } else if (i === stor.nb_valx + 1) {
              stor.tabSigne.zone[i].reponse = j3pCalculValeur(stor.donneesTableau.tab_dimension[3][1])
            } else {
              stor.tabSigne.zone[i].reponse = j3pCalculValeur(stor.donneesTableau.val_zero_tab[i - 1])
            }
            stor.tabSigne.zone[i].typeReponse = ['nombre', 'exact']
          }
          // et maintenant pour la deuxième ligne
          for (let i = 1; i <= stor.nb_valx + 1; i++) {
            stor.tabSigne.signe[i - 1].reponse = [stor.donneesTableau.signe_tab_corr[i - 1]]
            stor.tabSigne.signe[i - 1].typeReponse = ['texte']
          }
          // par défaut, les listes déroulantes de la dernière ligne vont contenir zéro
          for (let i = 1; i <= stor.nb_valx; i++) stor.tabSigne.liste[i - 1].reponse = 1
          // Je teste d’abord si la première ligne est correcte
          // si ce n’est pas le cas, je mets tout le tableau en rouge
          bonnePremiereLigne = true
          let bonSignes = true
          let bonMenusDeroulants = true
          bonEnsembleDefinition = true
          // var premiere_ligne = [];
          for (let i = 0; i <= stor.nb_valx + 1; i++) {
            fctsValid.zones.bonneReponse[i] = fctsValid.valideUneZone(stor.tabSigne.zone[i].id, stor.tabSigne.zone[i].reponse).bonneReponse
            if ((i === 0) || (i === stor.nb_valx + 1)) {
              bonEnsembleDefinition = (bonEnsembleDefinition && fctsValid.zones.bonneReponse[i])
            } else {
              bonnePremiereLigne = (bonnePremiereLigne && fctsValid.zones.bonneReponse[i])
            }
          }
          if (bonnePremiereLigne && bonEnsembleDefinition) {
            j3pEmpty(stor.zoneCons4)
            const deuxiemeLigne = []
            let bonneDeuxiemeLigne = true
            for (let i = 1; i <= stor.nb_valx + 1; i++) {
              deuxiemeLigne[i] = fctsValid.valideUneZone(stor.tabSigne.signe[i - 1].id, stor.tabSigne.signe[i - 1].reponse).bonneReponse
              bonSignes = (bonSignes && deuxiemeLigne[i])
              fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(stor.tabSigne.signe[i - 1].id)] = deuxiemeLigne[i]
            }
            // il y a aussi les listes déroulantes
            const deuxiemeLigneListe = []
            for (let i = 1; i <= stor.nb_valx; i++) {
              deuxiemeLigneListe[i] = fctsValid.valideUneZone(stor.tabSigne.liste[i - 1].id, stor.tabSigne.liste[i - 1].reponse).bonneReponse
              bonMenusDeroulants = (bonMenusDeroulants && deuxiemeLigneListe[i])
              fctsValid.zones.bonneReponse[fctsValid.zones.inputs.indexOf(stor.tabSigne.liste[i - 1])] = deuxiemeLigneListe[i]
            }
            bonneDeuxiemeLigne = (bonSignes && bonMenusDeroulants)
            reponse.bonneReponse = bonneDeuxiemeLigne
            // On enregistre tout ça dans le tableau fctsValid.zones.bonneReponse, pour mettre en rouge ou pas
            for (let i = 1; i <= stor.nb_valx + 1; i++) {
              const laZone = stor.tabSigne.signe[i - 1]
              // je trouve la position de cette zone de saisie dans le tableau des zones/listes à valider (utile pour bonneReponse)
              const index = fctsValid.zones.inputs.indexOf(laZone)
              fctsValid.zones.bonneReponse[index] = bonneDeuxiemeLigne
            }
            for (let i = 1; i <= stor.nb_valx; i++) {
              const laListe = stor.tabSigne.liste[i - 1]
              // je trouve la position de cette liste déroulante dans le tableau des zones/listes à valider (utile pour bonneReponse)
              const index = fctsValid.zones.inputs.indexOf(laListe)
              fctsValid.zones.bonneReponse[index] = bonneDeuxiemeLigne
            }
          } else {
            confusionVariation = toutMettreEnRouge(fctsValid)
            reponse.bonneReponse = false
            bonSignes = false
          }
        } else {
          confusionVariation = toutMettreEnRouge(fctsValid)
          reponse.bonneReponse = false
        }
        fctsValid.coloreLesZones()
        // on laisse la suite continuer
      } else if (!this.isElapsed) {
        // pas répondu et limite de temps pas atteinte, on lui affiche qu’il doit répondre
        // si limite de temps atteinte c’est géré plus loin
        this.reponseManquante(stor.zoneCorr)
        this.finCorrection()
        return
      }

      // Bonne réponse
      if (reponse.bonneReponse) {
        // Bonne réponse
        this.score++
        stor.zoneCorr.style.color = this.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        fctsEtudeAfficheCorrection(true)
        this.typederreurs[0]++
        this.finCorrection('navigation', true)
        return
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = this.styles.cfaux

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        this.typederreurs[10]++
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        fctsEtudeAfficheCorrection(false)
        this.finCorrection('navigation', true)
        return
      }
      // réponse KO sans limite de temps atteinte
      stor.zoneCorr.innerHTML = cFaux
      stor.numEssai++
      if (confusionVariation) {
        stor.zoneCorr.innerHTML += '<br/>' + textes.comment3
      } else if (!bonNombreZeros) {
        stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
      } else if (!bonnePremiereLigne) {
        stor.zoneCorr.innerHTML += '<br/>' + textes.comment2
      } else if (!bonEnsembleDefinition) {
        stor.zoneCorr.innerHTML += '<br/>' + textes.comment2_2
      }
      if (bonnePremiereLigne && bonEnsembleDefinition) {
        // si c’est un pb de signe(s), je ne vais pas lui donner une seconde chance
        // si les signes sont bons, mais c’est un pb de zéros, je lui laisse la possibilité de se corriger
        stor.zoneCorr.innerHTML += '<br/>' + textes.comment5
        // mais s’il n’y a qu’un signe, je ne lui redonne pas la main
        if (stor.nb_valx === 0) {
          stor.numEssai = ds.nbchances + 1
          fctsValid.coloreLesZones()
        }
      }
      if (stor.numEssai <= ds.nbchances) {
        // il lui reste des essais
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        this.typederreurs[1]++
        // il a encore la possibilité de se corriger, on reste dans l’état correction
        this.finCorrection()
        return
      }

      // Erreur au dernier essai
      if (bonnePremiereLigne && bonEnsembleDefinition) {
        // Toute la première ligne est correcte donc je lui mets 0.75 pts
        this.score += stor.scoreLigne1
      }
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      j3pEmpty(stor.zoneCons4)
      fctsEtudeAfficheCorrection(false)
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      // Obligatoire
      this.finNavigation(true)
      break // case "navigation":
  }
}
