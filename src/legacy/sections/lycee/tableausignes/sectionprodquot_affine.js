import { j3pAddElt, j3pArrondi, j3pBarre, j3pDetruit, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pPaletteMathquill, j3pPGCD, j3pRemplace, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeRectangle, j3pCreeSegment } from 'src/legacy/core/functionsSvg'
import { fctsEtudeOrdonneTabSansDoublon } from 'src/legacy/outils/fonctions/etude'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    février 2013
    Tableau donnant le signe d’un produit ou d’un quotient de fonctions affines (deux par défaut, mais on peut en demander plus)

*/
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition (ramené à 1 si le zéro de la fonction est donné ou est correct)'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.5, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)'],
    ['nbfacteurs', 2, 'entier', 'nombre de facteurs affine (supérieur ou égal à 2 et limité à 4)'],
    ['type_expres', ['produit'], 'array', '["produit"] ou ["quotient","num","num","den"] pour préciser dans le cas du quotient quels sont les facteurs au numérateur ou au dénominateur'],
    ['imposer_fct', ['', ''], 'array', 'on peut imposer l’une des fonctions affines ou toutes en écrivant par exemple "-x+3" ou "ax" ou "x+b". Pour de l’aléatoire sur les autres, écrire "". On peut aussi imposer des fonctions non affines, de la forme a (pour une constante), x^2-a^2, x^n, exp(x), ln(x) ou racines(x).\\nATTENTION, dans ce cas, il faut le préciser dans le paramètre facteurs_affines et ajouter les zéros dans zeros_facteurs_nonaffines'],
    ['imposer_signe', '', 'string', 'on peut imposer le signe : "<", ">", "<=" ou ">="'],
    ['a', '[-10;10]', 'string', 'valeur a dans les expressions ax+b (a différent de 0)'],
    ['b', '[-10;10]', 'string', 'valeur b dans les expressions ax+b (non paramétrable si zeroentier[0] = true)'],
    ['zeroentier', [false, false], 'array', "On peut imposer au zéro de la fonction d'être un entier, sinon c’est aléatoire"],
    ['ordreAffine', 'alea', 'string', 'les fonctions affines peuvent être de la forme "ax+b", "b+ax" ou "alea"'],
    ['signe_seulement', false, 'boolean', 'vaut true si on ne demande que le signe du produit/quotient, sans inéquation'],
    ['facteurs_affines', [true, true], 'array', 'tableau précisant si un facteur est affine (true par défaut), cela sert si on impose un facteur non affine'],
    ['zeros_facteurs_nonaffines', ['', ''], 'array', 'ce tableau contient les zéros des fonctions non affines sous la forme "-2/3" par exemple (si la fonction ne s’annule pas, écrire "", s’il y en a plusieurs les séparer par |, par exemple "-2/3|5". On peut aussi avoir des valeurs comme exp(2), ln(3 ou racine(2).'],
    ['domaine_def', ['-infini', '+infini', '', ''], 'array', 'ce tableau est utile si le domaine de la fonction n’est par ]-infini;+infini[. C’est un tableau à quatre éléments : les bornes et les crochets']
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'problème avec les signes des fonctions affines' },
    { pe_3: 'problème avec les zéros des fonctions affines' },
    { pe_4: 'les valeurs de la première ligne ont été inversées' },
    { pe_5: 'problème avec le signe d’un produit' },
    { pe_6: 'problème avec le signe d’un quotient' },
    { pe_7: 'problème avec les crochets des intervalles' },
    { pe_8: 'problème sur le signe de l’inégalité' },
    { pe_9: 'insuffisant' }
  ]
}
const textes = {
  titre_exo1: 'Signe d’un produit de fonctions affines',
  titre_exo1bis: 'Signe d’un produit de fonctions',
  titre_exo2: 'Signe d’un quotient de fonctions affines',
  titre_exo2bis: 'Signe d’un quotient de fonctions',
  consigne1: 'On souhaite résoudre l’inéquation $£b$.<br>Dans un premier temps, donner le signe du produit $£a$ sur $£d$ en complétant le tableau ci-dessous.<br>',
  consigne2: 'On souhaite résoudre l’inéquation $£b$.<br>Dans un premier temps, donner le signe du quotient $£a$ sur $£d$ en complétant le tableau ci-dessous.<br>',
  remarque1: 'Remarque :',
  remarque2: 'dans ce cas, les facteurs s’annulent en la même valeur.',
  remarque3: 'dans ce cas, au moins deux facteurs s’annulent en une même valeur.',
  remarque4: 'dans ce cas, tous les facteurs ne sont pas des fonctions affines non constantes.',
  // pour la question 2
  consigne3: 'Le tableau suivant donne le signe du produit $£a$ sur $£d$&nbsp;:',
  consigne4: 'Le tableau suivant donne le signe du quotient $£a$&nbsp;:',
  consigne5: 'L’ensemble des solutions de l’inéquation $£a$ est alors&nbsp;:<br>&1&',
  consigne6: 'Donner le signe du produit $£a$ sur $£d$ en complétant le tableau ci-dessous.<br>',
  consigne7: 'Donner le signe du quotient $£a$ en complétant le tableau ci-dessous.<br>',
  comment1: 'Il faut compléter tout le tableau&nbsp;!',
  comment1bis: 'Il faut choisir entre 0 et la barre verticale pour toutes les listes déroulantes&nbsp;!',
  comment2: 'Au moins une des valeurs de la première ligne est fausse&nbsp;!',
  comment3: 'Les valeurs en lesquelles s’annulent les fonctions ne sont pas dans le bon ordre&nbsp;!',
  comment4: 'Il y a un problème avec le signe des fonctions (signe et/ou zéro)&nbsp;!',
  comment5: 'Il y a un problème avec le signe du produit (signe et/ou zéro)&nbsp;!',
  comment6: 'Il y a un problème avec le signe du quotient (signe et/ou zéro)&nbsp;!',
  comment7: 'Tu n’as pas résolu la bonne inéquation&nbsp;!',
  comment8: 'Il y a un problème avec les crochets&nbsp;!',
  comment9: 'Attention à l’écriture d’un intervalle&nbsp;!',
  comment10: 'Le séparateur dans un intervalle doit être un point-virgule&nbsp;!',
  corr1: 'Voici le tableau attendu&nbsp;:',
  corr2: 'On cherche les réels $x$ pour lesquels le produit est strictement positif.<br>L’ensemble des solutions est donc $£a$.',
  corr3: 'On cherche les réels $x$ pour lesquels le produit est positif ou nul.<br>L’ensemble des solutions est donc $£a$.',
  corr4: 'On cherche les réels $x$ pour lesquels le produit est strictement négatif.<br>L’ensemble des solutions est donc $£a$.',
  corr5: 'On cherche les réels $x$ pour lesquels le produit est négatif ou nul.<br>L’ensemble des solutions est donc $£a$.'
}

/**
 * section prodquot_affine
 * @this {Parcours}
 */

export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  function decoupageCupCap (rep) {
    // cette fonction renvoit un tableau contenant les différentes parties séparées par \\cup ou par \\cap
    // ces différentes parties sont en principe des intervalles
    const cupCap = /\\cup|\\cap/g // new RegExp('\\\\cup|\\\\cap', 'g')
    return rep.split(cupCap)
  }

  function bonIntervalle (rep) {
    // rep est du texte (réponse de l’élève)
    // On teste si rep est bie un intervalle ou une réunon d’intervalle'
    // on vérifie aussi si l’élève ne met pas une virgule à la place du point-virgule quand il écrit un intervalle'
    let pbCrochet = false
    let pbPointVirgule = false
    const intervalleTab = decoupageCupCap(rep)
    for (let k = 0; k < intervalleTab.length; k++) {
      // pour chacun de ces éléments, on vérifie que c’est bien un intervalle'
      if (((intervalleTab[k].charAt(0) !== '[') && (intervalleTab[k].charAt(0) !== ']')) || ((intervalleTab[k].charAt(intervalleTab[k].length - 1) !== '[') && (intervalleTab[k].charAt(intervalleTab[k].length - 1) !== ']'))) {
        pbCrochet = true
      } else {
        if ((intervalleTab[k].indexOf(';') <= 1) || (intervalleTab[k].indexOf(';') >= intervalleTab[k].length - 2)) {
          // il n’y a pas de point-virgule bien placé, donc controlons s’il n’a pas mis une virgule à la place'
          if ((intervalleTab[k].indexOf(',') > 1) && (intervalleTab[k].indexOf(',') < intervalleTab[k].length - 2)) {
            // c’est donc qu’il met une virgule à la place d’un point-virgule'
            pbPointVirgule = true
          }
        }
      }
    }
    return { non_intervalle: pbCrochet, separateurVirgule: pbPointVirgule }
  }

  function estDansDomaine (nb, domaine) {
    // cette fonction vérifie si le nombre nb est dans le domaine de définition (au sens strict)
    // nb peut être un nombre ou une chaine de caractère correspodnant à un nombre : "2/3", "esp(5)-1", "ln(3)", "racine(5)" pa exemple
    // domaine est un tableau de deux éléments
    // domaine[0] représente un nombre du même style que nb ou -infini
    // domaine[1] représente un nombre du même style que nb ou +infini
    let appartientDomaine = true
    const nbval = calculQuot(nb)
    if (domaine[0] !== '-infini') {
      const borneInfVal = calculQuot(domaine[0])
      appartientDomaine = (nbval > borneInfVal)
    }
    if (domaine[1] !== '+infini') {
      const borneSupVal = calculQuot(domaine[1])
      appartientDomaine = (appartientDomaine && (nbval < borneSupVal))
    }
    return appartientDomaine
  }

  function positionTab (tableau, elt) {
    // renvoie la position de elt dans le tableau
    // ici elt est une valeur numérique (ou s’apparente à une telle valeur)'
    // renvoie -1 si elt n’est pas présent
    for (let i = 0; i < tableau.length; i++) {
      if (Math.abs(calculQuot(elt) - calculQuot(tableau[i])) < Math.pow(10, -12)) return i
    }
    return -1
  }

  function calculQuot (chaine) {
    // la chaine est de la forme num+"/"+den ou juste un nombre écrit au format string
    // cette fonction donne la valeur numérique de la chaine
    let chaineTxt = String(chaine)
    if (chaineTxt.indexOf('/') !== -1) {
      const num = chaineTxt.split('/')[0]
      const den = chaineTxt.split('/')[1]
      return calculQuot(num) / calculQuot(den)
    } else {
      // j’ai un pb si e est tout seul (sans exp ou ^)'
      let i = 0
      while (i < chaineTxt.length) {
        if ((chaineTxt[i] === 'e') && ((chaineTxt[i + 1] === '+') || (chaineTxt[i + 1] === '-') || (i === chaineTxt.length - 1))) {
          chaineTxt = chaineTxt.substring(0, i - 1) + 'exp(1)' + chaineTxt.substring(i + 1, chaineTxt.length)
          i += 5
        } else {
          i++
        }
      }
      const calcNumber = new Tarbre(chaineTxt, ['w'])
      return calcNumber.evalue([0])
    }
  }

  function tabsignesProduit (nomdiv, expresTab, tabDimension, prodQuot, zeroTab, valZeroTab, lesSignes, objStyleCouleur) {
    // on crée un tableau de signes dans nomdiv
    // ici il s’agit du tableau de signe d’un produit ou d’un quotient
    // expresTab est un tableau contenant tous les facteurs
    // tab_position est un tableau de 2 éléments qui contiennent la largeur L et la hauteur h du tableau
    // tabDimension[2] est un élément optionnel. S’il n’est pas undefined, c’est un tableau de deux éléments : les bornes du domaine de def de la fonction (écrire infini pour \\infty)'
    // prodQuot vaut ["produit"] ou un tableau ["quotient",[true, false, true, ..]]
    // où true signifie que le facteur associé de expresTab est au dénominateur
    // zeroTab[0] est un booléen. Il vaut true lorsque les valeurs en lesquelles s’annulent l’expression ne sont pas à remplir par l’élève
    // zeroTab[1] est un booléen. Il vaut true lorsque les signes du tableau ne sont pas à donner par l’élève
    // zeroTab[1] ne peut pas valoir false si zeroTab[0] vaut true;
    // valZeroTab est le tableau des valeurs en lesquelles s’annule au moins une fonction
    // les signes est un paramètres optionnel si on souhaite avoir des zones de saisies pour les signes (on peut aussi mettre false)
    // si on veut compléter entièrement le tableau, lesSignes sera un tableau contenant des éléments tels que ["+","-"] ou ["-","+"]
    // pour le signe du produit, on peut demander des zones de saisie dans ce cas le dernier élément de lesSignes vaut true
    // sinon ce dernier élément vaut false
    // objStyleCouleur peut contenir deux éléments : un style de texte, couleur si on ne définit par le style
    // je crée mes textes au début - si jamais on se lance dans la traduction
    const signeProdTxt = 'signe du produit'
    const signeQuotTxt = 'signe du quotient'
    const maCouleurTxt = (objStyleCouleur.couleur) ? objStyleCouleur.couleur : objStyleCouleur.style.color
    let i, i3
    const macouleur = '#000000'
    const signedeTabTxt = expresTab.map(elt => { return 'signe de $' + elt + '$' })
    const L = tabDimension[0] // Largeur du tableau
    const h = tabDimension[1] // hauteur du tableau
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    svg.setAttribute('width', L)
    svg.setAttribute('height', h)
    nomdiv.appendChild(svg)
    j3pCreeRectangle(svg, { x: 1, y: 0, width: L - 2, height: h, couleur: macouleur, epaisseur: 1 })
    for (i = 1; i < expresTab.length + 2; i++) {
      j3pCreeSegment(svg, {
        x1: 0,
        y1: i * h / (expresTab.length + 2),
        x2: L,
        y2: i * h / (expresTab.length + 2),
        couleur: macouleur,
        epaisseur: 1
      })
    }
    // Affichage des textes "signe de ax+b"
    const signeDe = []
    for (i = 1; i <= expresTab.length; i++) {
      const leDiv = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
      j3pStyle(leDiv, { position: 'absolute' })
      j3pAffiche(leDiv, '', signedeTabTxt[i - 1], { style: { color: maCouleurTxt } })
      signeDe.push(leDiv)
    }
    // texte "signe du produit" ou "signe du quotient"
    const signeDeProduit = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
    j3pStyle(signeDeProduit, { position: 'absolute' })
    j3pAffiche(signeDeProduit, '', (prodQuot[0] === 'produit') ? signeProdTxt : signeQuotTxt, { style: { color: maCouleurTxt } })
    // pour placer le segment vertical, je cherche la largeur de la phrase "signe de ..."
    let largeurSigneDe = signeDe[0].getBoundingClientRect().width + 12 // ce correctif de 12 est pour gérer le passage de mathquill
    for (i = 1; i <= expresTab.length; i++) largeurSigneDe = Math.max(largeurSigneDe, signeDe[i - 1].getBoundingClientRect().width + 12)
    largeurSigneDe = Math.max(largeurSigneDe, signeDeProduit.getBoundingClientRect().width + 4)
    j3pCreeSegment(svg, {
      x1: largeurSigneDe,
      y1: 0,
      x2: largeurSigneDe,
      y2: h,
      couleur: macouleur,
      epaisseur: 1
    })
    let posTxt, radical
    for (i = 0; i < expresTab.length; i++) {
      posTxt = (i + 1.5) * h / (expresTab.length + 2) - signeDe[i].getBoundingClientRect().height / 2
      signeDe[i].style.top = posTxt + 'px'
      signeDe[i].style.left = '2px'
    }
    const hauteurSigneProduit = signeDeProduit.getBoundingClientRect().height
    posTxt = (expresTab.length + 1.5) * h / (expresTab.length + 2) - hauteurSigneProduit / 2
    signeDeProduit.style.top = posTxt + 'px'
    signeDeProduit.style.left = '2px'
    // placement du "x"
    const divX = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
    j3pStyle(divX, { position: 'absolute' })
    j3pAffiche(divX, '', '$x$', { style: { color: maCouleurTxt } })
    const posTxt2 = h / (2 * expresTab.length + 4) - divX.getBoundingClientRect().height / 2
    divX.style.top = posTxt2 + 'px'
    const posTxt3 = largeurSigneDe / 2 - divX.getBoundingClientRect().width / 2
    divX.style.left = posTxt3 + 'px'
    // plus et moins l’infini'
    const divMoinsInf = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
    j3pStyle(divMoinsInf, { position: 'absolute' })
    const arbreExpressFacteur = []
    if (!tabDimension[2]) {
      j3pAffiche(divMoinsInf, '', '$-\\infty$', { style: { color: maCouleurTxt } })
    } else {
      let textBorneInf = String(tabDimension[2][0])
      if (textBorneInf.indexOf('infini') > -1) {
        textBorneInf = textBorneInf.replace('infini', '\\infty')
      } else {
        const posFrac = textBorneInf.indexOf('/')
        if (posFrac > -1) {
          // on a une fraction
          textBorneInf = '\\frac{' + textBorneInf.substring(0, posFrac) + '}{' + textBorneInf.substring(posFrac + 1, textBorneInf.length) + '}'
        }
      }
      j3pAffiche(divMoinsInf, '', '$' + textBorneInf + '$', { style: { color: maCouleurTxt } })
      // on peut aussi avoir à ajouter une double barre ou un zéro au niveau de cette borne
      let borneInfInterdite = false// permettra de mettre une double barre au produit/quotient au niveau de la borne inf
      const newExpressionFct = []
      for (i = 0; i < expresTab.length; i++) {
        const expressionFct = expresTab[i]
        // je dois enlever les \\ devant ln ou exp pour les calculs
        const expressionFctTab = expressionFct.split('\\')
        newExpressionFct[i] = ''
        for (let iFct = 0; iFct < expressionFctTab.length; iFct++) {
          newExpressionFct[i] = newExpressionFct[i] + expressionFctTab[iFct]
        }
        // enfin gestion de la racine carrée
        // il faut que je transforme sqrt en racine
        const chaineSqrt = /sqrt\{[0-9x.+\-^]+}/ig // new RegExp('sqrt\\{[0-9x,\\.+\\-\\^]{1,}\\}', 'ig')
        if (chaineSqrt.test(newExpressionFct[i])) {
          // dans ce cas sqrt{...} est présent et on le transforme en racine(...)
          const monExpresTab = newExpressionFct[i].match(chaineSqrt)
          for (i3 = 0; i3 < monExpresTab.length; i3++) {
            radical = monExpresTab[i3].substring(5, monExpresTab[i3].length - 1)
            newExpressionFct[i] = newExpressionFct[i].replace(monExpresTab[i3], 'racine(' + radical + ')')
          }
        }
        arbreExpressFacteur[i] = new Tarbre(newExpressionFct[i], ['x'])
      }
      if (textBorneInf.indexOf('infty') === -1) {
        // la borne inf n’est pas -infini'
        for (i = 0; i < expresTab.length; i++) {
          const imageBorneInf = arbreExpressFacteur[i].evalue([calculQuot(tabDimension[2][0])])
          if (Math.abs(imageBorneInf) < Math.pow(10, -12)) {
            // on met un zéro au niveau du facteur
            const imageBorneInf = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
            j3pAffiche(imageBorneInf, '' + i + 'txt1', '0', { style: { color: maCouleurTxt } })
            const posTxtBorneInf = (i + 1.5) * h / (expresTab.length + 2) - imageBorneInf.getBoundingClientRect().height / 2
            imageBorneInf.style.top = posTxtBorneInf + 'px'
            imageBorneInf.style.left = largeurSigneDe + 'px'
            if ((prodQuot[0] === 'quotient') && prodQuot[0][i]) {
              // on a un quotient et ce facteur est au dénominateur
              borneInfInterdite = true
            }
          } else if ((imageBorneInf === '-Infinity') || (imageBorneInf === '+Infinity')) {
            borneInfInterdite = true
            // on met une double barre au niveau du facteur
            j3pCreeSegment(svg, {
              x1: largeurSigneDe + 3,
              y1: (i + 1) * h / (expresTab.length + 2),
              x2: largeurSigneDe + 3,
              y2: (i + 2) * h / (expresTab.length + 2),
              couleur: maCouleurTxt,
              epaisseur: 1
            })
          }
        }
        if (borneInfInterdite) {
          j3pCreeSegment(svg, {
            x1: largeurSigneDe + 3,
            y1: (expresTab.length + 1) * h / (expresTab.length + 2),
            x2: largeurSigneDe + 3,
            y2: (expresTab.length + 2) * h / (expresTab.length + 2),
            couleur: maCouleurTxt,
            epaisseur: 1
          })
        }
      }
    }
    const divPlusInf = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
    j3pStyle(divPlusInf, { position: 'absolute' })
    if (!tabDimension[2]) {
      j3pAffiche(divPlusInf, '', '$+\\infty$', { style: { color: maCouleurTxt } })
    } else {
      let textBorneSup = String(tabDimension[2][1])
      if (textBorneSup.indexOf('infini') > -1) {
        textBorneSup = textBorneSup.replace('infini', '\\infty')
      } else {
        const posFrac2 = textBorneSup.indexOf('/')
        if (posFrac2 > -1) {
          // on a une fraction
          textBorneSup = '\\frac{' + textBorneSup.substring(0, posFrac2) + '}{' + textBorneSup.substring(posFrac2 + 1, textBorneSup.length) + '}'
        }
      }
      j3pAffiche(divPlusInf, '', '$' + textBorneSup + '$', { style: { color: maCouleurTxt } })
      // on peut aussi avoir à ajouter une double barre ou un zéro au niveau de cette borne
      let borneSupInterdite = false// permettra de mettre une double barre au produit/quotient au niveau de la borne sup
      if (textBorneSup.indexOf('infty') === -1) {
        // la borne sup n’est pas +infini'
        for (i = 0; i < expresTab.length; i++) {
          const imageBorneSup = arbreExpressFacteur[i].evalue([calculQuot(tabDimension[2][1])])
          if (Math.abs(imageBorneSup) < Math.pow(10, -12)) {
            // on met un zéro au niveau du facteur
            const imageBorneSup = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
            j3pStyle(imageBorneSup, { position: 'absolute' })
            j3pAffiche(imageBorneSup, '', '0', { style: { color: maCouleurTxt } })
            const posTxtborneSup = (i + 1.5) * h / (expresTab.length + 2) - imageBorneSup.getBoundingClientRect().height / 2
            imageBorneSup.style.top = posTxtborneSup + 'px'
            const posTxtborneSup2 = L - imageBorneSup.getBoundingClientRect().width
            imageBorneSup.style.left = posTxtborneSup2 + 'px'
            if ((prodQuot[0] === 'quotient') && prodQuot[0][i]) {
              // on a un quotient et ce facteur est au dénominateur
              borneSupInterdite = true
            }
          } else if ((imageBorneSup === '-Infinity') || (imageBorneSup === '+Infinity')) {
            borneSupInterdite = true
            // on met une double barre au niveau du facteur
            j3pCreeSegment(svg, {
              x1: L - 5,
              y1: (i + 1) * h / (expresTab.length + 2),
              x2: L - 5,
              y2: (i + 2) * h / (expresTab.length + 2),
              couleur: maCouleurTxt,
              epaisseur: 1
            })
          }
        }
        if (borneSupInterdite) {
          j3pCreeSegment(svg, {
            x1: L - 5,
            y1: (expresTab.length + 1) * h / (expresTab.length + 2),
            x2: L - 5,
            y2: (expresTab.length + 2) * h / (expresTab.length + 2),
            couleur: maCouleurTxt,
            epaisseur: 1
          })
        }
      }
    }
    divMoinsInf.style.left = (largeurSigneDe + 1) + 'px'
    divMoinsInf.style.top = (h / (2 * expresTab.length + 4) - divMoinsInf.getBoundingClientRect().height / 2) + 'px'
    divPlusInf.style.left = (me.etat === 'enonce' && me.questionCourante === 1) ? (L - 1 - divPlusInf.getBoundingClientRect().width * 1.33 - 2 + 1) + 'px' : (L - 1 - divPlusInf.getBoundingClientRect().width - 2 + 1) + 'px'
    // on a un pb de positionnement la première fois. Ensuite ça se positionne bien
    divPlusInf.style.top = (h / (2 * expresTab.length + 4) - divPlusInf.getBoundingClientRect().height / 2) + 'px'
    const tabZeroOrdonne = fctsEtudeOrdonneTabSansDoublon(valZeroTab)
    const nbValx = tabZeroOrdonne.length
    let monZeroTxt
    if (zeroTab[0]) {
      // les endroits où s’annulent la fonction sont donnés
      // attention lesZeros doivent être du type string (un entier ou un décimal ou un nb fractionnaire de la forme a/b
      // dans un premier temps, on ordonne les nombre du tableau valZeroTab (qui sont ls endroits où s’annulent notre produit)
      // Dans le cas où sont présents la fonction exp, la fonction ln ou la fonction racine, il va falloir gérer l’affichage'
      const chaineExp = /exp\([0-9x,.+\-^]+\)/ig // new RegExp('exp\\([0-9x,\\.+\\-\\^]{1,}\\)', 'ig')
      const chaineLn = /ln\([0-9x,.+\-^]+\)/ig // new RegExp('ln\\([0-9x,\\.+\\-\\^]{1,}\\)', 'ig')
      const chaineRacine = /racine\([0-9x,.+\-^]+\)/ig // new RegExp('racine\\([0-9x,\\.+\\-\\^]{1,}\\)', 'ig')
      let expressTabZero
      for (i = 0; i < nbValx; i++) {
        const leZero = String(tabZeroOrdonne[i]).replace(/\./g, ',')
        if (leZero.indexOf('/') !== -1) {
          const tabZeroSplit = leZero.split('/')
          monZeroTxt = '\\frac{' + tabZeroSplit[0] + '}{' + tabZeroSplit[1] + '}'
        } else {
          // gestion de exp et ln
          let expressionZero = leZero
          if (chaineExp.test(expressionZero)) {
            // dan ce cas exp(...) est présent et on le transforme en e^...
            expressTabZero = expressionZero.match(chaineExp)
            for (i3 = 0; i3 < expressTabZero.length; i3++) {
              const puissanceExp = expressTabZero[i3].substring(4, expressTabZero[i3].length - 1)
              if (puissanceExp === '1') {
                expressionZero = expressionZero.replace(expressTabZero[i3], '\\mathrm{e}')
              } else {
                expressionZero = expressionZero.replace(expressTabZero[i3], '\\mathrm{e}^{' + puissanceExp + '}')
              }
            }
          }
          // maintenant gestion d’une chaine avec ln'
          if (chaineLn.test(expressionZero)) {
            // dan ce cas ln(...) est présent et on le transforme en \\ln(...)
            expressTabZero = expressionZero.match(chaineLn)
            for (i3 = 0; i3 < expressTabZero.length; i3++) {
              const expressLn = expressTabZero[i3].substring(3, expressTabZero[i3].length - 1)
              expressionZero = expressionZero.replace(expressTabZero[i3], '\\ln(' + expressLn + ')')
            }
          }
          // et enfin gestion de la racine carrée
          if (chaineRacine.test(expressionZero)) {
            // dan ce cas racine(...) est présent et on le transforme en \\sqrt{...}
            expressTabZero = expressionZero.match(chaineRacine)
            for (i3 = 0; i3 < expressTabZero.length; i3++) {
              radical = expressTabZero[i3].substring(7, expressTabZero[i3].length - 1)
              expressionZero = expressionZero.replace(expressTabZero[i3], '\\sqrt{' + radical + '}')
            }
          }
          monZeroTxt = expressionZero
        }
        const zoneNulle = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
        j3pStyle(zoneNulle, { position: 'absolute' })
        j3pAffiche(zoneNulle, '', '$' + monZeroTxt + '$', { style: { color: maCouleurTxt } })
        zoneNulle.style.left = (largeurSigneDe + (i + 1) * (L - largeurSigneDe) / (nbValx + 1) - zoneNulle.getBoundingClientRect().width / 2) + 'px'
        zoneNulle.style.top = (h / (2 * expresTab.length + 4) - zoneNulle.getBoundingClientRect().height / 2) + 'px'
      }
    } else {
      // dans le cas où les valeur en lesquelles s’annulent le produit sont à donner
      // bien sûr dans ce cas, il y a autant de valeurs que de fonctions seulement si on n’a que des fonctions affines'
      stor.inputZoneNulle = []
      for (i = 1; i <= nbValx; i++) {
        const zoneNulle = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
        j3pStyle(zoneNulle, { position: 'absolute' })
        // j’ajoute cette propriété pour identifier sa position dans l’ensemble des zones de saisie
        // Cela me servira pour son replacement lors de la saisie
        const elt = j3pAffiche(zoneNulle, '', '&1&', { inputmq1: { texte: '' } })
        elt.inputmqList[0].index = i
        elt.inputmqList[0].leSpan = elt.parent.parentNode
        mqRestriction(elt.inputmqList[0], '^\\d.,/+-', { commandes: stor.listeBoutons })
        stor.inputZoneNulle.push(elt.inputmqList[0])
        zoneNulle.style.left = (largeurSigneDe + i * (L - largeurSigneDe) / (nbValx + 1) - elt.inputmqList[0].getBoundingClientRect().width / 2) + 'px'
        zoneNulle.style.top = (h / (2 * expresTab.length + 4) - elt.inputmqList[0].getBoundingClientRect().height / 2) + 'px'
      }
      for (i = 1; i <= nbValx; i++) {
        stor.inputZoneNulle[i - 1].addEventListener('input', function () {
          const idZoneNulle = this.index
          const laZone = stor.inputZoneNulle[idZoneNulle - 1].leSpan
          laZone.style.left = (largeurSigneDe + idZoneNulle * (L - largeurSigneDe) / (nbValx + 1) - stor.inputZoneNulle[idZoneNulle - 1].getBoundingClientRect().width / 2) + 'px'
          laZone.style.top = (h / (2 * expresTab.length + 4) - stor.inputZoneNulle[idZoneNulle - 1].getBoundingClientRect().height / 2) + 'px'
        })
      }
      if (tabZeroOrdonne.length > 0) j3pFocus(stor.inputZoneNulle[0])
    }
    // on crée tout d’abord les segments verticaux
    const posValZero = []
    for (i = 1; i <= nbValx; i++) {
      posValZero[i] = largeurSigneDe + i * (L - largeurSigneDe) / (nbValx + 1)
      j3pCreeSegment(svg, {
        x1: posValZero[i],
        y1: h / (expresTab.length + 2),
        x2: posValZero[i],
        y2: h,
        couleur: macouleur,
        epaisseur: 1
      })
    }
    let nbSigne, numLigne, numColonne, tabTextListe, numVerticale
    if (!zeroTab[1]) {
      // Dans ce cas, les signes et zéros sont à compléter
      // création de toutes les zones pour les signes
      nbSigne = (expresTab.length + 1) * (nbValx + 1)
      stor.inputZoneSigne = []
      for (i = 1; i <= nbSigne; i++) {
        const leSigne = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
        j3pStyle(leSigne, { position: 'absolute' })
        const elt = j3pAffiche(leSigne, '', '&1&', { inputmq1: { texte: '' } })
        mqRestriction(elt.inputmqList[0], '+-')
        elt.inputmqList[0].index = i
        elt.inputmqList[0].leSpan = elt.parent.parentNode
        // positionnement initial de toutes les zones
        numLigne = Math.ceil(i / (nbValx + 1))
        numColonne = (i - 1) % (nbValx + 1) + 1
        leSigne.style.left = (largeurSigneDe + (2 * numColonne - 1) * (L - largeurSigneDe) / (2 * nbValx + 2) - elt.inputmqList[0].getBoundingClientRect().width / 2) + 'px'
        leSigne.style.top = (numLigne * h / (expresTab.length + 2) + h / (2 * expresTab.length + 4) - elt.inputmqList[0].getBoundingClientRect().height / 2) + 'px'
        stor.inputZoneSigne.push(elt.inputmqList[0])
      }
      // ajustement de la position de chaque signe
      for (i = 1; i <= nbSigne; i++) {
        stor.inputZoneSigne[i - 1].addEventListener('input', function () {
          const idZoneSigne = this.index
          const laZone = stor.inputZoneSigne[idZoneSigne - 1].leSpan
          const numLigne = Math.ceil(idZoneSigne / (nbValx + 1))
          const numColonne = (idZoneSigne - 1) % (nbValx + 1) + 1
          laZone.style.left = (largeurSigneDe + (2 * numColonne - 1) * (L - largeurSigneDe) / (2 * nbValx + 2) - stor.inputZoneSigne[idZoneSigne - 1].getBoundingClientRect().width / 2) + 'px'
          laZone.style.top = (numLigne * h / (expresTab.length + 2) + h / (2 * expresTab.length + 4) - stor.inputZoneSigne[idZoneSigne - 1].getBoundingClientRect().height / 2) + 'px'
        })
        stor.inputZoneSigne[i - 1].addEventListener('focusin', () => { j3pEmpty(stor.btnsPalette) })
        // $(stor.inputZoneSigne[i - 1]).focusin(function () { j3pEmpty(stor.btnsPalette) })
      }
      // listes de la dernière ligne
      stor.inputSelect = []
      for (i = 1; i <= nbValx; i++) {
        const divListProduit = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
        j3pStyle(divListProduit, { position: 'absolute' })
        tabTextListe = (prodQuot[0] === 'produit') ? ['|', '0'] : ['|', '||', '0']
        const elt = j3pAffiche(divListProduit, '', '#1#', { liste1: { texte: tabTextListe } })
        divListProduit.style.left = (largeurSigneDe + i * (L - largeurSigneDe) / (nbValx + 1) - 8) + 'px'
        divListProduit.style.top = (h - h / (2 * expresTab.length + 4) - divListProduit.getBoundingClientRect().height / 2) + 'px'
        stor.inputSelect.push(elt.selectList[0])
      }
      // autres listes
      const nbAutresListes = nbValx * expresTab.length
      for (i = 1; i <= nbAutresListes; i++) {
        numLigne = Math.ceil(i / (nbValx))
        numColonne = (i - 1) % (nbValx) + 1
        const divList = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
        j3pStyle(divList, { position: 'absolute' })
        const elt = j3pAffiche(divList, '', '#1#', { liste1: { texte: ['|', '0'] } })
        divList.style.left = (largeurSigneDe + numColonne * (L - largeurSigneDe) / (nbValx + 1) - 8) + 'px'
        divList.style.top = ((2 * numLigne + 1) * h / (2 * expresTab.length + 4) - divList.getBoundingClientRect().height / 2) + 'px'
        stor.inputSelect.push(elt.selectList[0])
      }
      if (zeroTab[0]) {
        j3pFocus(stor.inputZoneSigne[0])
      } else {
        if (tabZeroOrdonne.length > 0) j3pFocus(stor.inputZoneNulle[0])
        else j3pFocus(stor.inputZoneSigne[0])
      }
    } else {
      // zeroTab[0] vaut nécessairement false
      // On est dans le cas où tout le tableau va être affiché (signes et zéros), sauf peut-être le signe du produit/quotient
      // on s’occupe dans un premier temps du signe de chaque facteur (pas celui du produit)
      // je place tous les zeros
      for (i = 1; i <= expresTab.length; i++) {
        if (valZeroTab[i - 1] !== '') {
          if (String(valZeroTab[i - 1]).indexOf('|') === -1) {
            // valZeroTab[i-1] ne contient qu’une seule valeur, mais il faut s’assurer qu’elle est bien dans le domaine'
            if (tabZeroOrdonne.indexOf(String(valZeroTab[i - 1])) > -1) {
              const zeroFacteurs = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
              j3pStyle(zeroFacteurs, { position: 'absolute' })
              j3pAffiche(zeroFacteurs, '', '0', { style: { color: maCouleurTxt } })
              numVerticale = positionTab(tabZeroOrdonne, valZeroTab[i - 1])
              zeroFacteurs.style.left = largeurSigneDe + (numVerticale + 1) * (L - largeurSigneDe) / (tabZeroOrdonne.length + 1) - zeroFacteurs.getBoundingClientRect().width / 2 + 'px'
              zeroFacteurs.style.top = (i + 0.5) * h / (expresTab.length + 2) - zeroFacteurs.getBoundingClientRect().height / 2 + 'px'
            }
          } else {
            const tabValZeroTabi = valZeroTab[i - 1].split('|')
            for (let k = 0; k < tabValZeroTabi.length; k++) {
              if (tabZeroOrdonne.indexOf(tabValZeroTabi[k]) > -1) {
                const zeroFacteurs = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
                j3pStyle(zeroFacteurs, { position: 'absolute' })
                j3pAffiche(zeroFacteurs, '', '0', { style: { color: maCouleurTxt } })
                numVerticale = positionTab(tabZeroOrdonne, tabValZeroTabi[k])
                zeroFacteurs.style.left = largeurSigneDe + (numVerticale + 1) * (L - largeurSigneDe) / (tabZeroOrdonne.length + 1) - zeroFacteurs.getBoundingClientRect().width / 2 + 'px'
                zeroFacteurs.style.top = (i + 0.5) * h / (expresTab.length + 2) - zeroFacteurs.getBoundingClientRect().height / 2 + 'px'
              }
            }
          }
        }
      }
      nbSigne = (nbValx + 1) * expresTab.length
      // le tableau suivant va servir pour le signe du produit ou du quotient
      const nbSignesMoinsTab = []
      for (i = 1; i <= nbValx + 1; i++) nbSignesMoinsTab[i] = 0
      for (i = 1; i <= nbSigne; i++) {
        const signeI = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
        j3pStyle(signeI, { position: 'absolute' })
        // je cherche l’endroit où s’annule mon expression (si elle s’annule)
        numLigne = Math.ceil(i / (nbValx + 1))
        numColonne = (i - 1) % (nbValx + 1) + 1
        let aucuneRacineDansDomaine
        if (valZeroTab[numLigne - 1] !== '') {
          // deux possibilités : soit valZeroTab[numLigne-1] ne possède qu’une valeur, soit plusieurs'
          if (valZeroTab[numLigne - 1].indexOf('|') === -1) {
            // une seule valeur
            aucuneRacineDansDomaine = (tabZeroOrdonne.indexOf(valZeroTab[numLigne - 1]) === -1)
          } else {
            const tabValeursZero = valZeroTab[numLigne - 1].split('|')
            aucuneRacineDansDomaine = true
            for (k = 0; k < tabValeursZero.length; k++) {
              aucuneRacineDansDomaine = (aucuneRacineDansDomaine && (tabZeroOrdonne.indexOf(tabValeursZero[k]) === -1))
            }
          }
        } else {
          aucuneRacineDansDomaine = true
        }
        if (aucuneRacineDansDomaine) {
          // on a le même signe sur toute la ligne
          j3pAffiche(signeI, '', '$' + lesSignes[numLigne - 1][0] + '$', { style: { color: maCouleurTxt } })
          if (lesSignes[numLigne - 1][0] === '-') {
            nbSignesMoinsTab[numColonne] += 1
          }
        } else {
          if (!tabZeroOrdonne[numColonne - 1]) {
            // pour le signe sur la dernière colonne
            j3pAffiche(signeI, '', '$' + lesSignes[numLigne - 1][lesSignes[numLigne - 1].length - 1] + '$', { style: { color: maCouleurTxt } })
            if (lesSignes[numLigne - 1][lesSignes[numLigne - 1].length - 1] === '-') {
              nbSignesMoinsTab[numColonne] += 1
            }
          } else {
            if (String(valZeroTab[numLigne - 1]).indexOf('|') === -1) {
              // la fonction de cette ligne ne s’annule pas plus d’une fois'
              if (calculQuot(valZeroTab[numLigne - 1]) >= calculQuot(tabZeroOrdonne[numColonne - 1])) {
                j3pAffiche(signeI, '', '$' + lesSignes[numLigne - 1][0] + '$', { style: { color: maCouleurTxt } })
                if (lesSignes[numLigne - 1][0] === '-') {
                  nbSignesMoinsTab[numColonne] += 1
                }
              } else {
                j3pAffiche(signeI, '', '$' + lesSignes[numLigne - 1][1] + '$', { style: { color: maCouleurTxt } })
                if (lesSignes[numLigne - 1][1] === '-') {
                  nbSignesMoinsTab[numColonne] += 1
                }
              }
            } else {
              k = 0
              let tableauValZeroNumLigne = valZeroTab[numLigne - 1].split('|')
              tableauValZeroNumLigne = fctsEtudeOrdonneTabSansDoublon(tableauValZeroNumLigne)
              me.logIfDebug('tableauValZeroNumLigne:' + tableauValZeroNumLigne)
              while (calculQuot(tabZeroOrdonne[numColonne - 1]) > calculQuot(tableauValZeroNumLigne[k])) {
                k++
              }
              j3pAffiche(signeI, '', '$' + lesSignes[numLigne - 1][k] + '$', { style: { color: maCouleurTxt } })
              if (lesSignes[numLigne - 1][k] === '-') nbSignesMoinsTab[numColonne] += 1
            }
          }
        }
        signeI.style.left = (largeurSigneDe + (2 * numColonne - 1) * (L - largeurSigneDe) / (2 * nbValx + 2) - signeI.getBoundingClientRect().width / 2) + 'px'
        signeI.style.top = (numLigne * h / (expresTab.length + 2) + h / (2 * expresTab.length + 4) - signeI.getBoundingClientRect().height / 2) + 'px'
      }
      stor.inputSigneProd = []
      if (!lesSignes[lesSignes.length - 1]) {
        // on demande de compléter des zones de saisie de la dernière ligne
        for (i = 1; i <= nbValx + 1; i++) {
          const signeProd = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
          j3pStyle({ position: 'absolute' })
          const elt = j3pAffiche(signeProd, '', '&1&', { inputmq1: { texte: '' } })
          mqRestriction(elt.inputmqList[0], '+-')
          elt.inputmqList[0].index = i
          elt.inputmqList[0].leSpan = elt.parent.parentNode
          // positionnement initial de toutes les zones
          numColonne = (i - 1) % (nbValx + 1) + 1
          signeProd.style.left = (largeurSigneDe + (2 * numColonne - 1) * (L - largeurSigneDe) / (2 * nbValx + 2) - elt.inputmqList[0].getBoundingClientRect().width / 2) + 'px'
          signeProd.style.top = ((expresTab.length + 1.5) * h / (expresTab.length + 2) - elt.inputmqList[0].getBoundingClientRect().height / 2) + 'px'
          stor.inputSigneProd.push(elt.inputmqList[0])
        }
        // ajustement de la position de chaque signe
        stor.inputSigneProd[i - 1].addEventListener('input', function () {
          const idZoneSigne = this.index
          const laZone = stor.inputSigneProd[idZoneSigne - 1].leSpan
          const numColonne = (idZoneSigne - 1) % (expresTab.length + 1) + 1
          laZone.style.left = (largeurSigneDe + (2 * numColonne - 1) * (L - largeurSigneDe) / (2 * nbValx + 2) - stor.inputSigneProd[idZoneSigne - 1].getBoundingClientRect().width / 2) + 'px'
          laZone.style.top = (numLigne * h / (expresTab.length + 2) + h / (2 * expresTab.length + 4) - stor.inputSigneProd[idZoneSigne - 1].getBoundingClientRect().height / 2) + 'px'
        })
        stor.inputSigneProd[i - 1].addEventListener('focusin', () => { j3pEmpty(stor.btnsPalette) })
        // $(stor.inputSigneProd[i - 1]).focusin(function () { j3pEmpty(stor.btnsPalette) })
        // listes de la dernière ligne
        stor.inputSelect = []
        for (i = 1; i <= nbValx; i++) {
          const divListProduit = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
          j3pStyle(divListProduit, { position: 'absolute' })
          tabTextListe = (prodQuot[0] === 'produit')
            ? ['|', '0']
            : ['|', '||', '0']
          const elt = j3pAffiche(divListProduit, '', '#1#', { liste1: { texte: tabTextListe, correction: '0' } })
          divListProduit.style.left = (largeurSigneDe + i * (L - largeurSigneDe) / (nbValx + 1) - 8) + 'px'
          divListProduit.style.top = (h - h / (2 * expresTab.length + 4) - divListProduit.getBoundingClientRect().height / 2) + 'px'
          stor.inputSelect.push(elt.inputSelect[0])
        }
        j3pFocus(stor.inputSigneProd[0])
      } else {
        // on donne les signes et les zéros ou double barres(la réponse)
        if (prodQuot[0] === 'produit') {
          // tout d’abord les zéros'
          for (i = 1; i <= nbValx; i++) {
            const zeroProd = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
            j3pStyle(zeroProd, { position: 'absolute' })
            j3pAffiche(zeroProd, '', '0', { style: { color: maCouleurTxt } })
            zeroProd.style.left = largeurSigneDe + (i) * (L - largeurSigneDe) / (tabZeroOrdonne.length + 1) - zeroProd.getBoundingClientRect().width / 2 + 'px'
            zeroProd.style.top = (expresTab.length + 1.5) * h / (expresTab.length + 2) - zeroProd.getBoundingClientRect().height / 2 + 'px'
          }
        } else {
          // tout d’abord les zéros ou double barres'
          for (i = 1; i <= nbValx; i++) {
            // je cherche la position de tabZeroOrdonne[i-1] dans valZeroTab (attention aux facteurs qui s’annule plus d’une fois, ce qui m’empêche d’utiliser positionTab)
            let numElt = -1
            k = valZeroTab.length - 1
            while (numElt < 0) {
              if (String(valZeroTab[k]).indexOf('|') === -1) {
                // valZeroTab[k] ne contient au max une valeur
                if (valZeroTab[k] !== '') {
                  if (Math.abs(calculQuot(valZeroTab[k]) - calculQuot(tabZeroOrdonne[i - 1])) < Math.pow(10, -12)) numElt = k
                }
              } else {
                const valZeroTabkTab = valZeroTab[k].split('|')
                for (let k4 = valZeroTabkTab.length - 1; k4 >= 0; k4--) {
                  if (Math.abs(calculQuot(valZeroTabkTab[k4]) - calculQuot(tabZeroOrdonne[i - 1])) < Math.pow(10, -12)) numElt = k
                }
              }
              k--
            }
            if (prodQuot[1][numElt]) {
              // on met une double barre
              j3pCreeSegment(svg, {
                x1: posValZero[i] + 3,
                y1: (expresTab.length + 1) * h / (expresTab.length + 2),
                x2: posValZero[i] + 3,
                y2: h,
                couleur: maCouleurTxt,
                epaisseur: 1
              })
              j3pCreeSegment(svg, {
                x1: posValZero[i],
                y1: (expresTab.length + 1) * h / (expresTab.length + 2),
                x2: posValZero[i],
                y2: h,
                couleur: maCouleurTxt,
                epaisseur: 1
              })
            } else {
              const zeroQuot = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
              j3pStyle(zeroQuot, { position: 'absolute' })
              j3pAffiche(zeroQuot, '', '0', { style: { color: maCouleurTxt } })
              zeroQuot.style.left = largeurSigneDe + (i) * (L - largeurSigneDe) / (tabZeroOrdonne.length + 1) - zeroQuot.getBoundingClientRect().width / 2 + 'px'
              zeroQuot.style.top = (expresTab.length + 1.5) * h / (expresTab.length + 2) - zeroQuot.getBoundingClientRect().height / 2 + 'px'
            }
          }
        }
        // maintenant on gère les signes
        for (i = 1; i <= nbValx + 1; i++) {
          const signeProd = j3pAddElt(nomdiv, 'div', '', { style: objStyleCouleur.style })
          j3pStyle(signeProd, { position: 'absolute' })
          if (nbSignesMoinsTab[i] % 2 === 0) {
            j3pAffiche(signeProd, '', '$+$', { style: { color: maCouleurTxt } })
          } else {
            j3pAffiche(signeProd, '', '$-$', { style: { color: maCouleurTxt } })
          }
          signeProd.style.left = (largeurSigneDe + (2 * i - 1) * (L - largeurSigneDe) / (2 * nbValx + 2) - signeProd.getBoundingClientRect().width / 2) + 'px'
          signeProd.style.top = ((expresTab.length + 1.5) * h / (expresTab.length + 2) - signeProd.getBoundingClientRect().height / 2) + 'px'
        }
      }
    }
  }

  function tableauSigneRep (valZeroTab, lesSignes) {
    // valZeroTab est le tableau qui contient les endroits où les fonctions s’annulent
    // ici toutes ses valeurs sont différentes'
    // lesSignes est un tableau de tableaux de style ["+","-"] ou ["-","+"]" voire ["+"] ou ["+","-","+"]
    const tabZeroOrdonne = fctsEtudeOrdonneTabSansDoublon(valZeroTab)
    const nbSigne = (tabZeroOrdonne.length + 1) * (lesSignes.length - 1)
    // le tableau suivant va servir pour le signe du produit ou du quotient
    const nbSignesMoinsTab = []
    // le tableau suivant accueille tous les signes des zones de saisie
    const signeSaisie = []
    let i
    for (i = 1; i <= tabZeroOrdonne.length + 1; i++) {
      nbSignesMoinsTab[i] = 0
    }
    for (i = 1; i <= nbSigne; i++) {
      // je cherche l’endroit où s’annule mon expression (si elle s’annule)
      const numLigne = Math.ceil(i / (tabZeroOrdonne.length + 1))
      const numColonne = (i - 1) % (tabZeroOrdonne.length + 1) + 1
      if (numColonne === tabZeroOrdonne.length + 1) {
        // dans ce cas, nous sommes sur la dernière colonne du tableau
        if (lesSignes[numLigne - 1][lesSignes[numLigne - 1].length - 1] === '-') {
          nbSignesMoinsTab[numColonne] += 1
          signeSaisie[i] = '-'
        } else {
          signeSaisie[i] = '+'
        }
      } else {
        if (lesSignes[numLigne - 1].length === 1) {
          // la fonction ne s’annule pas'
          if (lesSignes[numLigne - 1][0] === '-') {
            nbSignesMoinsTab[numColonne] += 1
            signeSaisie[i] = '-'
          } else {
            signeSaisie[i] = '+'
          }
        } else if (String(valZeroTab[numLigne - 1]).indexOf('|') === -1) {
          // la fonction de cette ligne ne s’annule pas plus d’une fois'
          if (calculQuot(valZeroTab[numLigne - 1]) >= calculQuot(tabZeroOrdonne[numColonne - 1])) {
            if (lesSignes[numLigne - 1][0] === '-') {
              nbSignesMoinsTab[numColonne] += 1
              signeSaisie[i] = '-'
            } else {
              signeSaisie[i] = '+'
            }
          } else {
            if (lesSignes[numLigne - 1][1] === '-') {
              nbSignesMoinsTab[numColonne] += 1
              signeSaisie[i] = '-'
            } else {
              signeSaisie[i] = '+'
            }
          }
        } else {
          let tableauValZeroNumLigne = valZeroTab[numLigne - 1].split('|')
          tableauValZeroNumLigne = fctsEtudeOrdonneTabSansDoublon(tableauValZeroNumLigne)
          me.logIfDebug('tableauValZeroNumLigne:' + tableauValZeroNumLigne)
          let k = 0
          while (calculQuot(tabZeroOrdonne[numColonne - 1]) > calculQuot(tableauValZeroNumLigne[k])) {
            k++
          }
          if (lesSignes[numLigne - 1][k] === '-') {
            nbSignesMoinsTab[numColonne] += 1
            signeSaisie[i] = '-'
          } else {
            signeSaisie[i] = '+'
          }
        }
      }
    }
    me.logIfDebug('nbSignesMoinsTab:' + nbSignesMoinsTab)
    // signes de la dernière ligne (celle du produit ou du quotient)
    for (i = 1; i <= tabZeroOrdonne.length + 1; i++) {
      if (nbSignesMoinsTab[i] % 2 === 0) {
        signeSaisie[nbSigne + i] = '+'
      } else {
        signeSaisie[nbSigne + i] = '-'
      }
    }
    return signeSaisie
  }

  function choixStyle (reponseCorrecte) {
    return (reponseCorrecte) ? me.styles.cbien : me.styles.cfaux
  }

  function afficheCorrection (bonneReponse) {
    // on affiche la correction
    if (me.etapeCourante === 1) {
      // on cherche la position de la zone de correction en y
      const laCorrection = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('toutpetit.correction', { paddingTop: '20px' }) })
      j3pAffiche(laCorrection, '', textes.corr1)
      const leTableauCorr = j3pAddElt(laCorrection, 'div', '', { style: me.styles.etendre('petit.correction', { position: 'relative', top: 0, left: 0 }) })
      stor.signesFacteurs[stor.signesFacteurs.length - 1] = true
      if ((ds.domaine_def[0] === '-infini') && (ds.domaine_def[1] === '+infini')) {
        tabsignesProduit(leTableauCorr, stor.expressAffine, [stor.largColGauche, stor.tailleLigne * (stor.expressAffine.length + 2)], stor.typeProdQuot, [true, true], stor.tabSolutionText, stor.signesFacteurs, {
          style: me.styles.toutpetit.enonce,
          couleur: me.styles.toutpetit.correction.color
        })
      } else {
        tabsignesProduit(leTableauCorr, stor.expressAffine, [stor.largColGauche, stor.tailleLigne * (stor.expressAffine.length + 2), ds.domaine_def], stor.typeProdQuot, [true, true], stor.tabSolutionText, stor.signesFacteurs, {
          style: me.styles.toutpetit.enonce,
          couleur: me.styles.toutpetit.correction.color
        })
      }
    } else {
      const divCorr = j3pAddElt(stor.conteneur, 'div')
      let commentCorr
      if (stor.signeInegalite === '>') {
        commentCorr = textes.corr2
      } else if (stor.signeInegalite === '\\geq') {
        commentCorr = textes.corr3
      } else if (stor.signeInegalite === '<') {
        commentCorr = textes.corr4
      } else {
        commentCorr = textes.corr5
      }
      const laCouleur = (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      j3pAffiche(divCorr, '', commentCorr, {
        a: stor.laRepLatex,
        style: { color: laCouleur }
      })
    }
  }

  function estChiffre (a) {
    // on souhaite savoir si le caractère a est un chiffre
    return ((a === 0) || (a === 1) || (a === 2) || (a === 3) || (a === 4) || (a === 5) || (a === 6) || (a === 7) || (a === 8) || (a === 9))
  }

  function ecoute (num) {
    if (stor.inputZoneNulle[num].className.includes('mq-editable-field')) {
      j3pEmpty(stor.btnsPalette)
      j3pPaletteMathquill(stor.btnsPalette, stor.inputZoneNulle[num], {
        liste: stor.listeBoutons
      })
    }
  }

  function convertirExpLnRacine (expression) {
    // expression est un texte qui peut contenir racine(...), exp(...) ou ln(...)
    // cette fonction renvoie l’expression en convertissant :'
    // racine(...) en \\sqrt{...}, exp(...) en \\mathrm{e}^{...} et ln(...) en \\ln(...)
    let expressTabLatex
    let expressLatex = expression
    const chaineExp = /exp\([0-9x,.+\-^]+\)/ig // new RegExp('exp\\([0-9x,\\.+\\-\\^]{1,}\\)', 'ig')
    const chaineLn = /ln\([0-9x,.+\-^]+\)/ig // new RegExp('ln\\([0-9x,\\.+\\-\\^]{1,}\\)', 'ig')
    const chaineRacine = /racine\([0-9x,.+\-^]+\)/ig // new RegExp('racine\\([0-9x,\\.+\\-\\^]{1,}\\)', 'ig')
    let i3
    if (chaineExp.test(expressLatex)) {
      // dans ce cas exp(...) est présent et on le transforme en e^...
      expressTabLatex = expressLatex.match(chaineExp)
      for (i3 = 0; i3 < expressTabLatex.length; i3++) {
        const puissanceExp = expressTabLatex[i3].substring(4, expressTabLatex[i3].length - 1)
        if (puissanceExp === '1') {
          expressLatex = expressLatex.replace(expressTabLatex[i3], '\\mathrm{e}')
        } else {
          expressLatex = expressLatex.replace(expressTabLatex[i3], '\\mathrm{e}^{' + puissanceExp + '}')
        }
      }
    }
    // maintenant gestion d’une chaine avec ln'
    if (chaineLn.test(expressLatex)) {
      // dans ce cas ln(...) est présent et on le transforme en \\ln(...)
      expressTabLatex = expressLatex.match(chaineLn)
      for (i3 = 0; i3 < expressTabLatex.length; i3++) {
        const expressLn = expressTabLatex[i3].substring(3, expressTabLatex[i3].length - 1)
        expressLatex = expressLatex.replace(expressTabLatex[i3], '\\ln(' + expressLn + ')')
      }
    }
    // enfin gestion de la racine carrée
    if (chaineRacine.test(expressLatex)) {
      // dans ce cas racine(...) est présent et on le transforme en \\sqrt{...}
      expressTabLatex = expressLatex.match(chaineRacine)
      for (i3 = 0; i3 < expressTabLatex.length; i3++) {
        const radical = expressTabLatex[i3].substring(7, expressTabLatex[i3].length - 1)
        expressLatex = expressLatex.replace(expressTabLatex[i3], '\\sqrt{' + radical + '}')
      }
    }
    return expressLatex
  }

  function enonceMain () {
    stor.tailleLigne = 55
    if (ds.type_expres[0] === 'quotient') {
      // on complète le tableau ds.type_expres s’il ne l’est pas totalement'
      for (i = 1; i <= Math.min(ds.nbfacteurs, 4); i++) {
        if ((!ds.type_expres[i]) || ((ds.type_expres[i] !== 'num') && (ds.type_expres[i] !== 'den'))) {
          // si rien n’est précisé je mets "den" à ce facteur affine'
          // de toute façon, s’ils sont tous "den", le premier sera transformé en "num"'
          ds.type_expres[i] = 'den'
        }
      }
      // ensuite je m’assure qu’il y a bien un facteur affine au numérateur et un au dénominateur'
      // si ce n’est pas le cas, alors je force le premier à être un numérateur et le dernier un dénominateur'
      let numAffinePresent = false
      for (i = 1; i < Math.min(ds.nbfacteurs, 4); i++) {
        if (ds.type_expres[i] === 'num') {
          numAffinePresent = true
        }
      }
      if (!numAffinePresent) {
        ds.type_expres[1] = 'num'
      }
      let denAffinePresent = false
      for (i = 1; i < Math.min(ds.nbfacteurs, 4); i++) {
        if ((!ds.type_expres[i]) || (ds.type_expres[i] !== 'num')) {
          ds.type_expres[i] = 'den'
          denAffinePresent = true
        }
      }
      if (!denAffinePresent) {
        ds.type_expres[Math.min(ds.nbfacteurs, 4)] = 'den'
      }
    }
    const coefA = []
    const coefB = []
    const solEq = []
    const solutionEq = []
    const solEqLatex = []
    const lePgcd = []
    const choixEntier = []
    const arbreCalc = []
    for (k = 0; k < ds.nbfacteurs; k++) {
      ds.imposer_fct[k] = ds.imposer_fct[k].replace(/²/g, '^2')
      if (!ds.imposer_fct[k]) ds.imposer_fct[k] = ''
      if (ds.facteurs_affines[k] === undefined) {
        // ATTENTION à ne pas remplacer ça par if (!ds.facteurs_affines[k]) car si c’est false, ça doit le rester
        ds.facteurs_affines[k] = true
      }
      if ((ds.imposer_fct[k] === 'exp(x)-a') || (ds.imposer_fct[k] === 'ln(x)+a') || (ds.imposer_fct[k] === 'x^2-a^2')) {
        ds.facteurs_affines[k] = false
      }
    }
    me.logIfDebug('ds.facteurs_affines:' + ds.facteurs_affines)
    let coefAExp, coefALn, coefAX2, numSol0, denSol0, solEq0Tab, arbreDifferenceSol0, maDifSol0, iSol0
    let numSol1, denSol1, kk, numSolk, denSolk
    let expressionFacteur, puissanceExp, posFrac, k4
    let textBorneInf, textBorneSup, crochetBorneInf, crochetBorneSup
    if ((me.etapeCourante === 1)) {
      // Tableau de signes à compléter
      stor.imposer_fct_init = []
      for (i = 0; i < ds.imposer_fct.length; i++) stor.imposer_fct_init[i] = ds.imposer_fct[i]
      // j’ai besoin de la largeur de la colonne de gauche. Je mets ça dans stockage[50]'
      stor.largColGauche = Math.min(95 * me.zonesElts.MG.getBoundingClientRect().width / 100, 700)
      if ((ds.imposer_fct[0] !== '') || (ds.imposer_fct[1] !== '')) {
        // l’une au moins des deux premières fonctions affines est imposée'
        if (ds.imposer_fct[0] !== '') {
          // si c’est le cas de la première'
          // Si dans le paramétrage, j’ai oublié de dire qu’il est affine dans les cas ci-dessous, alors je corrige
          if (['a', 'ax', 'x+b'].includes(ds.imposer_fct[0])) ds.facteurs_affines[0] = true
          if (ds.facteurs_affines[0]) {
            // ce facteur est bien affine
            let fctConstante = false
            if (ds.imposer_fct[0] === 'a') {
              fctConstante = true
              do {
                const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                coefB[0] = j3pGetRandomInt(minA, maxA)
              } while (coefB[0] === 0)
              ds.imposer_fct[0] = String(coefB[0])
              // Je ne le considère plus comme affine car il n’a pas de zéro
              ds.facteurs_affines[0] = false
              ds.zeros_facteurs_nonaffines[0] = ''
              solEq[0] = ''
            } else if (ds.imposer_fct[0] === 'ax') {
              do {
                const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                coefA[0] = j3pGetRandomInt(minA, maxA)
              } while ((Math.abs(coefA[0]) < Math.pow(10, -12)) || (Math.abs(coefA[0] - 1) < Math.pow(10, -12)))
              ds.imposer_fct[0] = j3pMonome(1, 1, coefA[0])
            } else if (ds.imposer_fct[0] === 'x+b') {
              do {
                const [minB, maxB] = j3pGetBornesIntervalle(ds.b)
                coefB[0] = j3pGetRandomInt(minB, maxB)
              } while (Math.abs(coefB[0]) < Math.pow(10, -12))
              ds.imposer_fct[0] = 'x' + j3pMonome(2, 0, coefB[0])
            }
            if (!fctConstante) {
              // Ce qui suit n’est valable que lorsque la fonction affine s’annule, pas quand elle est constante
              arbreCalc[0] = new Tarbre(ds.imposer_fct[0], ['x'])
              coefA[0] = arbreCalc[0].evalue([1]) - arbreCalc[0].evalue([0])
              coefB[0] = arbreCalc[0].evalue([0])
              me.logIfDebug('coefA[0]:' + coefA[0] + '  coefB[0]:' + coefB[0])
              solutionEq[0] = -Number(coefB[0]) / Number(coefA[0])
              if (Math.abs(solutionEq[0] - Math.round(solutionEq[0])) < Math.pow(10, -12)) {
                solEq[0] = String(solutionEq[0])
                solEqLatex[0] = solEq[0]
              } else {
                lePgcd[0] = j3pPGCD(coefB[0], coefA[0], { returnOtherIfZero: true, negativesAllowed: true })
                if (Number(coefA[0]) * Number(coefB[0]) < 0) {
                  solEq[0] = String(Math.abs(coefB[0]) / lePgcd[0]) + '/' + String(Math.abs(coefA[0]) / lePgcd[0])
                  solEqLatex[0] = '\\frac{' + String(Math.abs(coefB[0]) / lePgcd[0]) + '}{' + String(Math.abs(coefA[0]) / lePgcd[0]) + '}'
                } else {
                  solEq[0] = String(-Math.abs(coefB[0]) / lePgcd[0]) + '/' + String(Math.abs(coefA[0]) / lePgcd[0])
                  solEqLatex[0] = '-\\frac{' + String(Math.abs(coefB[0]) / lePgcd[0]) + '}{' + String(Math.abs(coefA[0]) / lePgcd[0]) + '}'
                }
              }
            }
          } else {
            // ce facteur n’est pas affine'
            if (ds.imposer_fct[0] === 'exp(x)-a') {
              // a est un entier positif aléatoire
              coefAExp = j3pGetRandomInt(2, 6)
              solEq[0] = 'ln(' + coefAExp + ')'
              ds.zeros_facteurs_nonaffines[0] = 'ln(' + coefAExp + ')'
              solEqLatex[0] = '\\ln(' + coefAExp + ')'
              ds.imposer_fct[0] = 'exp(x)-' + coefAExp
            } else if (ds.imposer_fct[0] === 'ln(x)+a') {
              // a est un entier aléatoire non nul et différent de -1
              do {
                coefALn = j3pGetRandomInt(-3, 5)
              } while ((Math.abs(coefALn) < Math.pow(10, -12)) || (Math.abs(coefALn + 1) < Math.pow(10, -12)))
              solEq[0] = 'exp(' + (-coefALn) + ')'
              ds.zeros_facteurs_nonaffines[0] = 'exp(' + (-coefALn) + ')'
              solEqLatex[0] = '\\mathrm{e}^{' + (-coefALn) + '}'
              ds.imposer_fct[0] = 'ln(x)' + j3pMonome(2, 0, coefALn)
            } else if (ds.imposer_fct[0] === 'x^2-a^2') {
              // a est un entier aléatoire de [1;5]
              coefAX2 = j3pGetRandomInt(1, 5)
              solEq[0] = [String(-coefAX2), String(coefAX2)].join('|')
              ds.zeros_facteurs_nonaffines[0] = solEq[0]
              solEqLatex[0] = solEq[0]
              ds.imposer_fct[0] = 'x^2-' + Math.pow(coefAX2, 2)
            } else {
              // on regarde le tableau des zéros
              solEq[0] = String(ds.zeros_facteurs_nonaffines[0])
              if (ds.zeros_facteurs_nonaffines[0] === '') {
                solEqLatex[0] = ''
              } else {
                if (String(ds.zeros_facteurs_nonaffines[0]).includes('|')) {
                  // la fonction s’annule en plusieurs valeurs'
                  const solFct0 = String(ds.zeros_facteurs_nonaffines[0]).split('|')
                  numSol0 = []
                  denSol0 = []
                  solEqLatex[0] = ''
                  for (k = 0; k < solFct0.length; k++) {
                    if (solFct0[k].includes('/')) {
                      numSol0[k] = solFct0[k].split('/')[0]
                      denSol0[k] = solFct0[k].split('/')[1]
                      let solEqLatex0 = ''
                      if (calculQuot(solFct0[k]) > 0) {
                        solEqLatex0 = '\\frac{' + String(Math.abs(Number(numSol0[k]))) + '}{' + String(Math.abs(Number(denSol0[k]))) + '}'
                      } else {
                        solEqLatex0 = '-\\frac{' + String(Math.abs(Number(numSol0[k]))) + '}{' + String(Math.abs(Number(denSol0[k]))) + '}'
                      }
                      solEqLatex[0] += solEqLatex0
                    } else {
                      solEqLatex[0] += solFct0[k]
                    }
                    if (k < solFct0.length - 1) {
                      solEqLatex[0] += '|'
                    }
                  }
                } else {
                  if (String(ds.zeros_facteurs_nonaffines[0]).includes('/')) {
                    // la solution est une fraction
                    numSol0 = String(ds.zeros_facteurs_nonaffines[0]).split('/')[0]
                    denSol0 = String(ds.zeros_facteurs_nonaffines[0]).split('/')[1]
                    if (calculQuot(ds.zeros_facteurs_nonaffines[0]) > 0) {
                      solEqLatex[0] = '\\frac{' + String(Math.abs(Number(numSol0))) + '}{' + String(Math.abs(Number(denSol0))) + '}'
                    } else {
                      solEqLatex[0] = '-\\frac{' + String(Math.abs(Number(numSol0))) + '}{' + String(Math.abs(Number(denSol0))) + '}'
                    }
                  } else {
                    solEqLatex[0] = solEq[0]
                  }
                }
              }
            }
            me.logIfDebug('solEqLatex[0]:' + solEqLatex[0])
          }
        } else {
          choixEntier[0] = Boolean(j3pGetRandomInt(0, 1))
          if (choixEntier[0]) {
            do {
              const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
              coefA[0] = j3pGetRandomInt(minA, maxA)
            } while (Math.abs(coefA[0]) < Math.pow(10, -13))
            solutionEq[0] = j3pGetRandomInt(-6, 6)
            solEqLatex[0] = String(solEq[0])
            solEq[0] = String(solutionEq[0])
            coefB[0] = -coefA[0] * solEq[0]
          } else {
            do {
              do {
                const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                coefA[0] = j3pGetRandomInt(minA, maxA)
              } while (Math.abs(coefA[0]) < Math.pow(10, -13))
              const [minB, maxB] = j3pGetBornesIntervalle(ds.b)
              coefB[0] = j3pGetRandomInt(minB, maxB)
              solutionEq[0] = -coefB[0] / coefA[0]
            } while (Math.abs(solutionEq[0] - Math.round(solutionEq[0])) < 0.00001)
            lePgcd[0] = j3pPGCD(coefB[0], coefA[0], { returnOtherIfZero: true, negativesAllowed: true })
            if (Number(coefA[0]) * Number(coefB[0]) < 0) {
              solEq[0] = String(Math.abs(coefB[0]) / lePgcd[0]) + '/' + String(Math.abs(coefA[0]) / lePgcd[0])
              solEqLatex[0] = '\\frac{' + String(Math.abs(coefB[0]) / lePgcd[0]) + '}{' + String(Math.abs(coefA[0]) / lePgcd[0]) + '}'
            } else {
              solEq[0] = String(-Math.abs(coefB[0]) / lePgcd[0]) + '/' + String(Math.abs(coefA[0]) / lePgcd[0])
              solEqLatex[0] = '-\\frac{' + String(Math.abs(coefB[0]) / lePgcd[0]) + '}{' + String(Math.abs(coefA[0]) / lePgcd[0]) + '}'
            }
          }
        }
        if (ds.imposer_fct[1] !== '') {
          // si c’est le cas de la deuxième'
          // Si dans le paramétrage, j’ai oublié de dire qu’il est affine dans les cas ci-dessous, alors je corrige
          if (['a', 'ax', 'x+b'].includes(ds.imposer_fct[1])) ds.facteurs_affines[1] = true
          if (ds.facteurs_affines[1]) {
            // ce facteur est bien affine
            let fctConstante = false
            if (ds.imposer_fct[1] === 'a') {
              fctConstante = true
              do {
                const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                coefB[1] = j3pGetRandomInt(minA, maxA)
              } while (coefB[1] === 0)
              ds.imposer_fct[1] = String(coefB[1])
              // Je ne le considère plus comme affine car il n’a pas de zéro
              ds.facteurs_affines[1] = false
              ds.zeros_facteurs_nonaffines[1] = ''
              solEq[1] = ''
            } else if (ds.imposer_fct[1] === 'ax') {
              do {
                const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                coefA[1] = j3pGetRandomInt(minA, maxA)
              } while ((Math.abs(coefA[1]) < Math.pow(10, -12)) || (Math.abs(coefA[1] - 1) < Math.pow(10, -12)))
              ds.imposer_fct[1] = j3pMonome(1, 1, coefA[1])
            } else if (ds.imposer_fct[1] === 'x+b') {
              do {
                const [minB, maxB] = j3pGetBornesIntervalle(ds.b)
                coefB[1] = j3pGetRandomInt(minB, maxB)
                // b doit être non nul et -b (racine de la fonction affine) ne doit pas être égal au précédent 0 de la fonction
              } while (Math.abs(coefB[1]) < 1e-12 || (Math.abs(solutionEq[0] + coefB[1]) < Math.pow(10, -10)))
              ds.imposer_fct[1] = 'x' + j3pMonome(2, 0, coefB[1])
            }
            if (!fctConstante) {
              arbreCalc[1] = new Tarbre(ds.imposer_fct[1], ['x'])
              coefA[1] = arbreCalc[1].evalue([1]) - arbreCalc[1].evalue([0])
              coefB[1] = arbreCalc[1].evalue([0])
              me.logIfDebug('coefA[1]:' + coefA[1] + '  coefB[1]:' + coefB[1])
              solutionEq[1] = -Number(coefB[1]) / Number(coefA[1])
              if (Math.abs(solutionEq[1] - Math.round(solutionEq[1])) < Math.pow(10, -12)) {
                // la solution est entière
                solEq[1] = String(solutionEq[1])
                solEqLatex[1] = solEq[1]
              } else {
                lePgcd[1] = j3pPGCD(coefB[1], coefA[1], { returnOtherIfZero: true, negativesAllowed: true })
                if (Number(coefA[1]) * Number(coefB[1]) < 0) {
                  solEq[1] = String(Math.abs(coefB[1]) / lePgcd[1]) + '/' + String(Math.abs(coefA[1]) / lePgcd[1])
                  solEqLatex[1] = '\\frac{' + String(Math.abs(coefB[1]) / lePgcd[1]) + '}{' + String(Math.abs(coefA[1]) / lePgcd[1]) + '}'
                } else {
                  solEq[1] = String(-Math.abs(coefB[1]) / lePgcd[1]) + '/' + String(Math.abs(coefA[1]) / lePgcd[1])
                  solEqLatex[1] = '-\\frac{' + String(Math.abs(coefB[1]) / lePgcd[1]) + '}{' + String(Math.abs(coefA[1]) / lePgcd[1]) + '}'
                }
              }
            }
          } else {
            // ce facteur n’est pas affine'
            if (ds.imposer_fct[1] === 'exp(x)-a') {
              // a est un entier positif aléatoire
              do {
                coefAExp = j3pGetRandomInt(2, 6)
                // je fais en sorte que la valeur du zéro de cette fonction soit différent du ou des précédents
                solEq0Tab = solEq[0].split('|')
                arbreDifferenceSol0 = []
                maDifSol0 = false
                for (iSol0 = 0; iSol0 < solEq0Tab.length; iSol0++) {
                  arbreDifferenceSol0[iSol0] = new Tarbre(solEq0Tab[iSol0], ['x'])
                  maDifSol0 = (maDifSol0 || (Math.abs(arbreDifferenceSol0[iSol0].evalue([0]) - Math.log(coefAExp)) < Math.pow(10, -12)))
                }
              } while (maDifSol0)
              solEq[1] = 'ln(' + coefAExp + ')'
              ds.zeros_facteurs_nonaffines[1] = 'ln(' + coefAExp + ')'
              solEqLatex[1] = '\\ln(' + coefAExp + ')'
              ds.imposer_fct[1] = 'exp(x)-' + coefAExp
            } else if (ds.imposer_fct[1] === 'ln(x)+a') {
              // a est un entier aléatoire sauf 0 ou -1
              do {
                do {
                  coefALn = j3pGetRandomInt(-3, 5)
                } while ((Math.abs(coefALn) < Math.pow(10, -12)) || (Math.abs(coefALn + 1) < Math.pow(10, -12)))
                // je fais en sorte que la valeur du zéro de cette fonction soit différent du ou des précédents
                solEq0Tab = solEq[0].split('|')
                arbreDifferenceSol0 = []
                maDifSol0 = false
                for (iSol0 = 0; iSol0 < solEq0Tab.length; iSol0++) {
                  arbreDifferenceSol0[iSol0] = new Tarbre(solEq0Tab[iSol0], ['x'])
                  maDifSol0 = (maDifSol0 || (Math.abs(arbreDifferenceSol0[iSol0].evalue([0]) - Math.exp(-coefALn)) < Math.pow(10, -12)))
                }
              } while (maDifSol0)
              solEq[1] = 'exp(' + (-coefALn) + ')'
              ds.zeros_facteurs_nonaffines[1] = 'exp(' + (-coefALn) + ')'
              solEqLatex[1] = '\\mathrm{e}^{' + (-coefALn) + '}'
              ds.imposer_fct[1] = 'ln(x)' + j3pMonome(2, 0, coefALn)
            } else {
              // on regarde le tableau des zéros
              solEq[1] = String(ds.zeros_facteurs_nonaffines[1])
              if (ds.zeros_facteurs_nonaffines[1] === '') {
                solEqLatex[1] = ''
              } else {
                if (String(ds.zeros_facteurs_nonaffines[1]).includes('|')) {
                  // la fonction s’annule en plusieurs valeurs'
                  const solFct1 = String(ds.zeros_facteurs_nonaffines[1]).split('|')
                  numSol1 = []
                  denSol1 = []
                  solEqLatex[1] = ''
                  for (k = 0; k < solFct1.length; k++) {
                    if (solFct1[k].indexOf('/') > -1) {
                      numSol1[k] = solFct1[k].split('/')[0]
                      denSol1[k] = solFct1[k].split('/')[1]
                      let solEqLatex1 = ''
                      if (calculQuot(solFct1[k]) > 0) {
                        solEqLatex1 = '\\frac{' + String(Math.abs(Number(numSol1[k]))) + '}{' + String(Math.abs(Number(denSol1[k]))) + '}'
                      } else {
                        solEqLatex1 = '-\\frac{' + String(Math.abs(Number(numSol1[k]))) + '}{' + String(Math.abs(Number(denSol1[k]))) + '}'
                      }
                      solEqLatex[1] += solEqLatex1
                    } else {
                      solEqLatex[1] += solFct1[k]
                    }
                    if (k < solFct1.length - 1) {
                      solEqLatex[1] += '|'
                    }
                  }
                } else {
                  if (String(ds.zeros_facteurs_nonaffines[1]).includes('/')) {
                    // la solution est une fraction
                    numSol1 = String(ds.zeros_facteurs_nonaffines[1]).split('/')[0]
                    denSol1 = String(ds.zeros_facteurs_nonaffines[1]).split('/')[1]
                    if (calculQuot(ds.zeros_facteurs_nonaffines[1]) > 0) {
                      solEqLatex[1] = '\\frac{' + String(Math.abs(Number(numSol1))) + '}{' + String(Math.abs(Number(denSol1))) + '}'
                    } else {
                      solEqLatex[1] = '-\\frac{' + String(Math.abs(Number(numSol1))) + '}{' + String(Math.abs(Number(denSol1))) + '}'
                    }
                  } else {
                    solEqLatex[1] = solEq[1]
                  }
                }
              }
            }
            me.logIfDebug('solEqLatex[1]:' + solEqLatex[1])
          }
        } else {
          choixEntier[1] = Boolean(j3pGetRandomInt(0, 1))
          let egaliteRacine
          // la racine de la deuxième fonction affine n’est pas nécessairement entière
          do {
            if (choixEntier[1]) {
              do {
                do {
                  const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                  coefA[1] = j3pGetRandomInt(minA, maxA)
                } while (Math.abs(coefA[1]) < Math.pow(10, -13))
                solEq[1] = j3pGetRandomInt(-6, 6)
                solEqLatex[1] = String(solEq[1])
                solutionEq[1] = solEq[1]
                coefB[1] = -coefA[1] * solutionEq[1]
              } while (Math.abs(solEq[1]) < Math.pow(10, -13))
            } else {
              do {
                do {
                  const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                  coefA[1] = j3pGetRandomInt(minA, maxA)
                } while (Math.abs(coefA[1]) < Math.pow(10, -13))
                const [minB, maxB] = j3pGetBornesIntervalle(ds.b)
                coefB[1] = j3pGetRandomInt(minB, maxB)
                solutionEq[1] = -coefB[1] / coefA[1]
              } while (Math.abs(solutionEq[1] - Math.round(solutionEq[1])) < 0.00001)
              lePgcd[1] = j3pPGCD(coefB[1], coefA[1], { returnOtherIfZero: true, negativesAllowed: true })
              if (Number(coefA[1]) * Number(coefB[1]) < 0) {
                solEq[1] = String(Math.abs(coefB[1]) / lePgcd[1]) + '/' + String(Math.abs(coefA[1]) / lePgcd[1])
                solEqLatex[1] = '\\frac{' + String(Math.abs(coefB[1]) / lePgcd[1]) + '}{' + String(Math.abs(coefA[1]) / lePgcd[1]) + '}'
              } else {
                solEq[1] = String(-Math.abs(coefB[1]) / lePgcd[1]) + '/' + String(Math.abs(coefA[1]) / lePgcd[1])
                solEqLatex[1] = '-\\frac{' + String(Math.abs(coefB[1]) / lePgcd[1]) + '}{' + String(Math.abs(coefA[1]) / lePgcd[1]) + '}'
              }
            }
            if (String(solEq[0].indexOf('|')) === -1) {
              egaliteRacine = (Math.abs(Number(solEq[0]) - solutionEq[1]) < Math.pow(10, -13))
            } else {
              const tabSolEq0 = String(solEq[0]).split('|')
              egaliteRacine = false
              for (k = 0; k < tabSolEq0.length; k++) {
                egaliteRacine = (egaliteRacine || (Math.abs(Number(tabSolEq0[k]) - solutionEq[1]) < Math.pow(10, -13)))
              }
            }
          } while (egaliteRacine)
        }
      } else {
        // choix des coefs a, b, c et d aléatoire
        if (ds.zeroentier[0]) {
          // la racine de la première fonction affine est entière
          do {
            const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
            coefA[0] = j3pGetRandomInt(minA, maxA)
          } while (Math.abs(coefA[0]) < Math.pow(10, -13))
          solEq[0] = j3pGetRandomInt(-8, 8)
          solEqLatex[0] = String(solEq[0])
          solutionEq[0] = solEq[0]
          coefB[0] = -coefA[0] * solEq[0]
          if (ds.zeroentier[1]) {
            // la racine de la deuxième fonction affine est entière
            do {
              do {
                const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                coefA[1] = j3pGetRandomInt(minA, maxA)
              } while (Math.abs(coefA[1]) < Math.pow(10, -13))
              solEq[1] = j3pGetRandomInt(-6, 6)
              solEqLatex[1] = String(solEq[1])
              solutionEq[1] = solEq[1]
              coefB[1] = -coefA[1] * solEq[1]
            } while ((Math.abs(coefA[0] - coefA[1]) < Math.pow(10, -13)) || (Math.abs(solEq[0] - solEq[1]) < Math.pow(10, -13)) || (Math.abs(solEq[1]) < Math.pow(10, -13)))
          } else {
            // la racine de la deuxième fonction affine n’est pas nécessairement entière
            choixEntier[1] = Boolean(j3pGetRandomInt(0, 1))
            if (choixEntier[1]) {
              do {
                do {
                  const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                  coefA[1] = j3pGetRandomInt(minA, maxA)
                } while (Math.abs(coefA[1]) < Math.pow(10, -13))
                solEq[1] = j3pGetRandomInt(-6, 6)
                solEqLatex[1] = String(solEq[1])
                solutionEq[1] = solEq[1]
                coefB[1] = -coefA[1] * solutionEq[1]
              } while ((Math.abs(solEq[0] - solEq[1]) < Math.pow(10, -13)) || (Math.abs(solEq[1]) < Math.pow(10, -13)))
            } else {
              do {
                do {
                  const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                  coefA[1] = j3pGetRandomInt(minA, maxA)
                } while (Math.abs(coefA[1]) < Math.pow(10, -13))
                const [minB, maxB] = j3pGetBornesIntervalle(ds.b)
                coefB[1] = j3pGetRandomInt(minB, maxB)
                solutionEq[1] = -coefB[1] / coefA[1]
              } while (Math.abs(solutionEq[1] - Math.round(solutionEq[1])) < 0.00001)
              lePgcd[1] = j3pPGCD(coefB[1], coefA[1], { returnOtherIfZero: true, negativesAllowed: true })
              if (Number(coefA[1]) * Number(coefB[1]) < 0) {
                solEq[1] = String(Math.abs(coefB[1]) / lePgcd[1]) + '/' + String(Math.abs(coefA[1]) / lePgcd[1])
                solEqLatex[1] = '\\frac{' + String(Math.abs(coefB[1]) / lePgcd[1]) + '}{' + String(Math.abs(coefA[1]) / lePgcd[1]) + '}'
              } else {
                solEq[1] = String(-Math.abs(coefB[1]) / lePgcd[1]) + '/' + String(Math.abs(coefA[1]) / lePgcd[1])
                solEqLatex[1] = '-\\frac{' + String(Math.abs(coefB[1]) / lePgcd[1]) + '}{' + String(Math.abs(coefA[1]) / lePgcd[1]) + '}'
              }
            }
          }
        } else {
          // la racine de la première fonction affine n’est pas nécessairement entière
          choixEntier[0] = Boolean(j3pGetRandomInt(0, 1))
          choixEntier[1] = Boolean(j3pGetRandomInt(0, 1))
          if (choixEntier[0]) {
            do {
              const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
              coefA[0] = j3pGetRandomInt(minA, maxA)
            } while (Math.abs(coefA[0]) < Math.pow(10, -13))
            solEq[0] = j3pGetRandomInt(-8, 8)
            solEqLatex[0] = String(solEq[0])
            solutionEq[0] = solEq[0]
            coefB[0] = -coefA[0] * solEq[0]
          } else {
            do {
              do {
                const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                coefA[0] = j3pGetRandomInt(minA, maxA)
              } while (Math.abs(coefA[0]) < Math.pow(10, -13))
              const [minB, maxB] = j3pGetBornesIntervalle(ds.b)
              coefB[0] = j3pGetRandomInt(minB, maxB)
              solutionEq[0] = -coefB[0] / coefA[0]
            } while (Math.abs(solutionEq[0] - Math.round(solutionEq[0])) < 0.00001)
            lePgcd[0] = j3pPGCD(coefB[0], coefA[0], { returnOtherIfZero: true, negativesAllowed: true })
            if (Number(coefA[0]) * Number(coefB[0]) < 0) {
              solEq[0] = String(Math.abs(coefB[0]) / lePgcd[0]) + '/' + String(Math.abs(coefA[0]) / lePgcd[0])
              solEqLatex[0] = '\\frac{' + String(Math.abs(coefB[0]) / lePgcd[0]) + '}{' + String(Math.abs(coefA[0]) / lePgcd[0]) + '}'
            } else {
              solEq[0] = String(-Math.abs(coefB[0]) / lePgcd[0]) + '/' + String(Math.abs(coefA[0]) / lePgcd[0])
              solEqLatex[0] = '-\\frac{' + String(Math.abs(coefB[0]) / lePgcd[0]) + '}{' + String(Math.abs(coefA[0]) / lePgcd[0]) + '}'
            }
          }
          if (ds.zeroentier[1]) {
            // la racine de la deuxième fonction affine est entière
            do {
              do {
                const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                coefA[1] = j3pGetRandomInt(minA, maxA)
              } while (Math.abs(coefA[1]) < Math.pow(10, -13))
              solEq[1] = j3pGetRandomInt(-6, 6)
              solEqLatex[1] = String(solEq[1])
              solutionEq[1] = solEq[1]
              coefB[1] = -coefA[1] * solEq[1]
            } while ((Math.abs(coefA[0] - coefA[1]) < Math.pow(10, -13)) || (Math.abs(solEq[0] - solEq[1]) < Math.pow(10, -13)) || (Math.abs(solEq[1]) < Math.pow(10, -13)))
          } else {
            // la racine de la deuxième fonction affine n’est pas nécessairement entière
            do {
              if (choixEntier[1]) {
                do {
                  do {
                    const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                    coefA[1] = j3pGetRandomInt(minA, maxA)
                  } while (Math.abs(coefA[1]) < Math.pow(10, -13))
                  solEq[1] = j3pGetRandomInt(-6, 6)
                  solEqLatex[1] = String(solEq[1])
                  solutionEq[1] = solEq[1]
                  coefB[1] = -coefA[1] * solutionEq[1]
                } while ((Math.abs(solutionEq[0] - solutionEq[1]) < Math.pow(10, -13)) || (Math.abs(solEq[1]) < Math.pow(10, -13)))
              } else {
                do {
                  do {
                    const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                    coefA[1] = j3pGetRandomInt(minA, maxA)
                  } while (Math.abs(coefA[1]) < Math.pow(10, -13))
                  const [minB, maxB] = j3pGetBornesIntervalle(ds.b)
                  coefB[1] = j3pGetRandomInt(minB, maxB)
                  solutionEq[1] = -coefB[1] / coefA[1]
                } while (Math.abs(solutionEq[1] - Math.round(solutionEq[1])) < 0.00001)
                lePgcd[1] = j3pPGCD(coefB[1], coefA[1], { returnOtherIfZero: true, negativesAllowed: true })
                if (Number(coefA[1]) * Number(coefB[1]) < 0) {
                  solEq[1] = String(Math.abs(coefB[1]) / lePgcd[1]) + '/' + String(Math.abs(coefA[1]) / lePgcd[1])
                  solEqLatex[1] = '\\frac{' + String(Math.abs(coefB[1]) / lePgcd[1]) + '}{' + String(Math.abs(coefA[1]) / lePgcd[1]) + '}'
                } else {
                  solEq[1] = String(-Math.abs(coefB[1]) / lePgcd[1]) + '/' + String(Math.abs(coefA[1]) / lePgcd[1])
                  solEqLatex[1] = '-\\frac{' + String(Math.abs(coefB[1]) / lePgcd[1]) + '}{' + String(Math.abs(coefA[1]) / lePgcd[1]) + '}'
                }
              }
            } while (Math.abs(solutionEq[0] - solutionEq[1]) < Math.pow(10, -13))
          }
        }
      }
      if (ds.nbfacteurs > 2) {
        // il y a d’autres fonctions affines'
        // là je ne me préocupe plus de savoir si c’est un entier ou pas...'
        let k2
        for (k = 2; k < Math.min(ds.nbfacteurs, 4); k++) {
          if (ds.imposer_fct[k] !== '') {
            // Si dans le paramétrage, j’ai oublié de dire qu’il est affine dans les cas ci-dessous, alors je corrige
            if (['a', 'ax', 'x+b'].includes(ds.imposer_fct[k])) ds.facteurs_affines[k] = true
            if (ds.facteurs_affines[k]) {
              // ce facteur est bien affine
              let fctConstante = false
              if (ds.imposer_fct[k] === 'a') {
                fctConstante = true
                do {
                  const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                  coefB[k] = j3pGetRandomInt(minA, maxA)
                } while (coefB[k] === 0)
                ds.imposer_fct[k] = String(coefB[k])
                // Je ne le considère plus comme affine car il n’a pas de zéro
                ds.facteurs_affines[k] = false
                ds.zeros_facteurs_nonaffines[k] = ''
                solEq[k] = ''
              } else if (ds.imposer_fct[k] === 'ax') {
                do {
                  const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                  coefA[k] = j3pGetRandomInt(minA, maxA)
                } while ((Math.abs(coefA[k]) < Math.pow(10, -12)) || (Math.abs(coefA[k] - 1) < Math.pow(10, -12)))
                ds.imposer_fct[k] = j3pMonome(1, 1, coefA[k])
              } else if (ds.imposer_fct[k] === 'x+b') {
                let pbEgalite = false
                do {
                  const [minB, maxB] = j3pGetBornesIntervalle(ds.b)
                  coefB[k] = j3pGetRandomInt(minB, maxB)
                  for (let j = 0; j < k; j++) {
                    if (Math.abs(solutionEq[j] + coefB[k]) < Math.pow(10, -12)) pbEgalite = true
                  }
                } while (Math.abs(coefB[k]) < 1e-12 || pbEgalite)
                ds.imposer_fct[k] = 'x' + j3pMonome(2, 0, coefB[k])
              }
              if (!fctConstante) {
                arbreCalc[k] = new Tarbre(ds.imposer_fct[k], ['x'])
                coefA[k] = arbreCalc[k].evalue([1]) - arbreCalc[k].evalue([0])
                coefB[k] = arbreCalc[k].evalue([0])
                solutionEq[k] = -Number(coefB[k]) / Number(coefA[k])
                if (Math.abs(solutionEq[k] - Math.round(solutionEq[k])) < Math.pow(10, -12)) {
                  solEq[k] = String(solutionEq[k])
                  solEqLatex[k] = solEq[k]
                } else {
                  lePgcd[k] = j3pPGCD(coefB[k], coefA[k], { returnOtherIfZero: true, negativesAllowed: true })
                  if (Number(coefA[k]) * Number(coefB[k]) < 0) {
                    solEq[k] = String(Math.abs(coefB[k]) / lePgcd[k]) + '/' + String(Math.abs(coefA[k]) / lePgcd[k])
                    solEqLatex[k] = '\\frac{' + String(Math.abs(coefB[k]) / lePgcd[k]) + '}{' + String(Math.abs(coefA[k]) / lePgcd[k]) + '}'
                  } else {
                    solEq[k] = String(-Math.abs(coefB[k]) / lePgcd[k]) + '/' + String(Math.abs(coefA[k]) / lePgcd[k])
                    solEqLatex[k] = '-\\frac{' + String(Math.abs(coefB[k]) / lePgcd[k]) + '}{' + String(Math.abs(coefA[k]) / lePgcd[k]) + '}'
                  }
                }
              }
            } else {
              // ce facteur n’est pas affine'
              if (ds.imposer_fct[k] === 'exp(x)-a') {
                // a est un entier positif aléatoire
                do {
                  coefAExp = j3pGetRandomInt(2, 6)
                  // je fais en sorte que la valeur du zéro de cette fonction soit différent du ou des précédents
                  maDifSol0 = false
                  arbreDifferenceSol0 = []
                  for (kk = 0; kk < k; kk++) {
                    solEq0Tab = solEq[kk].split('|')
                    arbreDifferenceSol0 = []
                    for (iSol0 = 0; iSol0 < solEq0Tab.length; iSol0++) {
                      arbreDifferenceSol0[iSol0] = new Tarbre(solEq0Tab[iSol0], ['x'])
                      maDifSol0 = (maDifSol0 || (Math.abs(arbreDifferenceSol0[iSol0].evalue([0]) - Math.log(coefAExp)) < Math.pow(10, -12)))
                    }
                  }
                } while (maDifSol0)
                solEq[k] = 'ln(' + coefAExp + ')'
                ds.zeros_facteurs_nonaffines[k] = 'ln(' + coefAExp + ')'
                solEqLatex[k] = '\\ln(' + coefAExp + ')'
                ds.imposer_fct[k] = 'exp(x)-' + coefAExp
              } else if (ds.imposer_fct[k] === 'ln(x)+a') {
                // a est un entier aléatoire sauf 0 ou 1
                do {
                  do {
                    coefALn = j3pGetRandomInt(-3, 5)
                  } while ((Math.abs(coefALn) < Math.pow(10, -12)) || (Math.abs(coefALn - 1) < Math.pow(10, -12)))
                  // je fais en sorte que la valeur du zéro de cette fonction soit différent du ou des précédents
                  maDifSol0 = false
                  arbreDifferenceSol0 = []
                  for (kk = 0; kk < k; kk++) {
                    solEq0Tab = solEq[kk].split('|')
                    arbreDifferenceSol0 = []
                    for (iSol0 = 0; iSol0 < solEq0Tab.length; iSol0++) {
                      arbreDifferenceSol0[iSol0] = new Tarbre(solEq0Tab[iSol0], ['x'])
                      maDifSol0 = (maDifSol0 || (Math.abs(arbreDifferenceSol0[iSol0].evalue([0]) - Math.exp(-coefALn)) < Math.pow(10, -12)))
                    }
                  }
                } while (maDifSol0)
                solEq[k] = 'exp(' + (-coefALn) + ')'
                ds.zeros_facteurs_nonaffines[k] = 'exp(' + (-coefALn) + ')'
                solEqLatex[k] = '\\mathrm{e}^{' + (-coefALn) + '}'
                ds.imposer_fct[k] = 'ln(x)' + j3pMonome(2, 0, coefALn)
              } else {
                // on regarde le tableau des zéros
                solEq[k] = String(ds.zeros_facteurs_nonaffines[k])
                if (ds.zeros_facteurs_nonaffines[k] === '') {
                  solEqLatex[k] = ''
                } else {
                  if (String(ds.zeros_facteurs_nonaffines[k]).includes('|')) {
                    // la fonction s’annule en plusieurs valeurs'
                    const solFctk = String(ds.zeros_facteurs_nonaffines[k]).split('|')
                    numSolk = []
                    denSolk = []
                    solEqLatex[k] = ''
                    for (k2 = 0; k2 < solFctk.length; k2++) {
                      if (solFctk[k2].indexOf('/') > -1) {
                        numSolk[k2] = solFctk[k2].split('/')[0]
                        denSolk[k2] = solFctk[k2].split('/')[1]
                        let solEqLatexk = ''
                        if (calculQuot(solFctk[k2]) > 0) {
                          solEqLatexk = '\\frac{' + String(Math.abs(Number(numSolk[k2]))) + '}{' + String(Math.abs(Number(denSol1[k]))) + '}'
                        } else {
                          solEqLatexk = '-\\frac{' + String(Math.abs(Number(numSolk[k2]))) + '}{' + String(Math.abs(Number(denSol1[k]))) + '}'
                        }
                        solEqLatex[k] += solEqLatexk
                      } else {
                        solEqLatex[k] += solFctk[k2]
                      }
                      if (k2 < solFctk.length - 1) {
                        solEqLatex[k] += '|'
                      }
                    }
                  } else {
                    if (String(ds.zeros_facteurs_nonaffines[k]).includes('/')) {
                      // la solution est une fraction
                      numSolk = String(ds.zeros_facteurs_nonaffines[k]).split('/')[0]
                      denSolk = String(ds.zeros_facteurs_nonaffines[k]).split('/')[1]
                      if (calculQuot(ds.zeros_facteurs_nonaffines[k]) > 0) {
                        solEqLatex[k] = '\\frac{' + String(Math.abs(Number(numSolk))) + '}{' + String(Math.abs(Number(denSolk))) + '}'
                      } else {
                        solEqLatex[k] = '-\\frac{' + String(Math.abs(Number(numSolk))) + '}{' + String(Math.abs(Number(denSolk))) + '}'
                      }
                    } else {
                      solEqLatex[k] = solEq[k]
                    }
                  }
                }
              }
              me.logIfDebug('solEqLatex[' + k + ']:' + solEqLatex[k])
            }
          } else {
            let solDifferente
            do {
              do {
                do {
                  const [minA, maxA] = j3pGetBornesIntervalle(ds.a)
                  coefA[k] = j3pGetRandomInt(minA, maxA)
                } while (Math.abs(coefA[k]) < Math.pow(10, -13))
                const [minB, maxB] = j3pGetBornesIntervalle(ds.b)
                coefB[k] = j3pGetRandomInt(minB, maxB)
                solutionEq[k] = -coefB[k] / coefA[k]
              // je veux que cette solution ne soit pas entière
              } while (Math.abs(solutionEq[k] - Math.round(solutionEq[k])) < 0.00001)
              lePgcd[k] = j3pPGCD(coefB[k], coefA[k], { returnOtherIfZero: true, negativesAllowed: true })
              if (Number(coefA[k]) * Number(coefB[k]) < 0) {
                solEq[k] = String(Math.abs(coefB[k]) / lePgcd[k]) + '/' + String(Math.abs(coefA[k]) / lePgcd[k])
                solEqLatex[k] = '\\frac{' + String(Math.abs(coefB[k]) / lePgcd[k]) + '}{' + String(Math.abs(coefA[k]) / lePgcd[k]) + '}'
              } else {
                solEq[k] = String(-Math.abs(coefB[k]) / lePgcd[k]) + '/' + String(Math.abs(coefA[k]) / lePgcd[k])
                solEqLatex[k] = '-\\frac{' + String(Math.abs(coefB[k]) / lePgcd[k]) + '}{' + String(Math.abs(coefA[k]) / lePgcd[k]) + '}'
              }
              // solution différente des précédentes
              solDifferente = true
              for (k2 = 0; k2 < k; k2++) {
                if (Math.abs(solutionEq[k] - solutionEq[k2]) < Math.pow(10, -10)) solDifferente = false
              }
            } while (!solDifferente)
          }
        }
      }
      // fin de la détermination des coefficients
      // tableau pour mettre les différentes solutions
      const tabSolEqText = []
      for (i = 0; i < Math.min(ds.nbfacteurs, 4); i++) tabSolEqText[i] = String(solEq[i])
      // on gère alors l’écriture des facteurs (affines ou pas)
      const ordreAffine = []
      const ordreAff = []
      const expressAffine = []
      const expressAffineInit = []
      let i3
      let expressTabFacteur
      const chaineExp = /exp\([0-9x,.+\-^]+\)/ig // new RegExp('exp\\([0-9x,\\.+\\-\\^]{1,}\\)', 'ig')
      const chaineLn = /ln\([0-9x,.+\-^]+\)/ig // new RegExp('ln\\([0-9x,\\.+\\-\\^]{1,}\\)', 'ig')
      const chaineRacine = /racine\([0-9x,.+\-^]+\)/ig // new RegExp('racine\\([0-9x,\\.+\\-\\^]{1,}\\)', 'ig')
      for (i = 0; i < Math.min(ds.nbfacteurs, 4); i++) {
        if (ds.ordreAffine === 'alea') {
          ordreAff[i] = Boolean(j3pGetRandomInt(0, 1))
          if (ordreAff[i]) {
            ordreAffine[i] = 'ax+b'
          } else {
            ordreAffine[i] = 'b+ax'
          }
        } else {
          ordreAffine[i] = ds.ordreAffine
        }
        if (ds.facteurs_affines[i]) {
          if (ordreAffine[i] === 'ax+b') {
            expressAffine[i] = j3pMonome(1, 1, Number(coefA[i])) + j3pMonome(2, 0, Number(coefB[i]))
          } else {
            if (coefB[i] === 0 || !coefB[i]) {
              expressAffine[i] = j3pMonome(1, 1, Number(coefA[i]))
            } else {
              expressAffine[i] = j3pMonome(1, 0, Number(coefB[i])) + j3pMonome(2, 1, Number(coefA[i]))
            }
          }
          expressAffineInit[i] = expressAffine[i]
        }
        // si la fonction est imposée alors on remet ce qui est écrit :
        if ((ds.imposer_fct[i] !== undefined) && (ds.imposer_fct[i] !== '')) {
          // petite variante si on a la fonction exp ou la fonction ln
          expressionFacteur = ds.imposer_fct[i]
          expressAffineInit[i] = ds.imposer_fct[i]
          if (chaineExp.test(expressionFacteur)) {
            // dan ce cas exp(...) est présent et on le transforme en e^...
            expressTabFacteur = expressionFacteur.match(chaineExp)
            for (i3 = 0; i3 < expressTabFacteur.length; i3++) {
              puissanceExp = expressTabFacteur[i3].substring(4, expressTabFacteur[i3].length - 1)
              if (puissanceExp === '1') {
                expressionFacteur = expressionFacteur.replace(expressTabFacteur[i3], '\\mathrm{e}')
              } else {
                expressionFacteur = expressionFacteur.replace(expressTabFacteur[i3], '\\mathrm{e}^{' + puissanceExp + '}')
              }
            }
          }
          // maintenant gestion d’une chaine avec ln'
          if (chaineLn.test(expressionFacteur)) {
            // dan ce cas ln(...) est présent et on le transforme en \\ln(...)
            expressTabFacteur = expressionFacteur.match(chaineLn)
            for (i3 = 0; i3 < expressTabFacteur.length; i3++) {
              expressLn = expressTabFacteur[i3].substring(3, expressTabFacteur[i3].length - 1)
              expressionFacteur = expressionFacteur.replace(expressTabFacteur[i3], '\\ln(' + expressLn + ')')
            }
          }
          // enfin gestion de la racine carrée
          if (chaineRacine.test(expressionFacteur)) {
            // dan ce cas racine(...) est présent et on le transforme en \\sqrt{...}
            expressTabFacteur = expressionFacteur.match(chaineRacine)
            for (i3 = 0; i3 < expressTabFacteur.length; i3++) {
              const radical = expressTabFacteur[i3].substring(7, expressTabFacteur[i3].length - 1)
              expressionFacteur = expressionFacteur.replace(expressTabFacteur[i3], '\\sqrt{' + radical + '}')
            }
          }
          expressAffine[i] = expressionFacteur
        }
      }
      for (i = 2; i < Math.min(ds.nbfacteurs, 4); i++) {
        if (ds.imposer_fct[i] !== undefined) {
          if (ds.imposer_fct[i] !== '') {
            // petite variante si on a la fonction exp ou la fonction ln
            expressionFacteur = ds.imposer_fct[i]
            expressAffineInit[i] = ds.imposer_fct[i]
            if (chaineExp.test(expressionFacteur)) {
              // dan ce cas exp(...) est présent et on le transforme en e^...
              expressTabFacteur = expressionFacteur.match(chaineExp)
              for (i3 = 0; i3 < expressTabFacteur.length; i3++) {
                puissanceExp = expressTabFacteur[i3].substring(4, expressTabFacteur[i3].length - 1)
                expressionFacteur = expressionFacteur.replace(expressTabFacteur[i3], '\\mathrm{e}^{' + puissanceExp + '}')
              }
            }
            // maintenant gestion d’une chaine avec ln'
            if (chaineLn.test(expressionFacteur)) {
              // dan ce cas ln(...) est présent et on le transforme en \\ln(...)
              expressTabFacteur = expressionFacteur.match(chaineLn)
              for (i3 = 0; i3 < expressTabFacteur.length; i3++) {
                expressLn = expressTabFacteur[i3].substring(3, expressTabFacteur[i3].length - 1)
                expressionFacteur = expressionFacteur.replace(expressTabFacteur[i3], '\\ln(' + expressLn + ')')
              }
            }
            expressAffine[i] = expressionFacteur
          }
        }
      }
      let expressProduit = ''
      let expressQuotient = ''
      if (ds.type_expres[0] === 'produit') {
        if (expressAffine[0].indexOf('(') === 0) {
          // le facteur a déjà des parenthèses
          expressProduit = expressAffine[0]
        } else {
          if (expressAffine[0].split('+').length > 1) {
            // il y a une somme donc je vais ajouter des parenthèses
            expressProduit = '(' + expressAffine[0] + ')'
          } else {
            if ((expressAffine[0].split('-').length > 2) || ((expressAffine[0].split('-').length === 2) && (expressAffine[0].split('-')[0] !== ''))) {
              // il y a une différence
              expressProduit = '(' + expressAffine[0] + ')'
            } else {
              expressProduit = expressAffine[0]
            }
          }
        }
        // et maintenant pour les autres facteurs
        for (i = 1; i < Math.min(ds.nbfacteurs, 4); i++) {
          if (expressAffine[i].indexOf('(') === 0) {
            // le facteur a déjà des parenthèses
            expressProduit += expressAffine[i]
          } else {
            expressProduit += '(' + expressAffine[i] + ')'
          }
        }
      } else {
        // partie à gérer pour identifier numérateur et denominateur
        let numQuotient = ''
        let denQuotient = ''
        let premierFacteurNum = true
        let premierFacteurDen = true
        // il faut que je fasse attention si je n’ai pas un seul facteur au num ou au den'
        // dans ce cas, il ne prend pas de parenthèses
        let nbNum = 0
        let nbDen = 0
        for (i = 0; i < Math.min(ds.nbfacteurs, 4); i++) {
          if (ds.type_expres[i + 1] === 'num') {
            nbNum++
          } else {
            nbDen++
          }
        }
        for (i = 0; i < Math.min(ds.nbfacteurs, 4); i++) {
          if (ds.type_expres[i + 1] === 'num') {
            // ce facteur est au numérateur
            if (nbNum === 1) {
              numQuotient += expressAffine[i]
            } else {
              if (premierFacteurNum) {
                if (expressAffine[i].indexOf('(') === 0) {
                  // le facteur a déjà des parenthèses
                  numQuotient += expressAffine[i]
                } else {
                  if (expressAffine[i].split('+').length > 1) {
                    // il y a une somme donc je vais ajouter des parenthèses
                    numQuotient = '(' + expressAffine[i] + ')'
                  } else {
                    if ((expressAffine[i].split('-').length > 2) || ((expressAffine[i].split('-').length === 2) && (expressAffine[i].split('-')[0] !== ''))) {
                      // il y a une différence
                      numQuotient = '(' + expressAffine[i] + ')'
                    } else {
                      numQuotient = expressAffine[i]
                    }
                  }
                }
                premierFacteurNum = false
              } else {
                if (expressAffine[i].indexOf('(') === 0) {
                  // le facteur a déjà des parenthèses
                  numQuotient += expressAffine[i]
                } else {
                  numQuotient += '(' + expressAffine[i] + ')'
                }
              }
            }
          } else {
            // ce facteur est au dénominateur
            if (nbDen === 1) {
              denQuotient += expressAffine[i]
            } else {
              if (premierFacteurDen) {
                if (expressAffine[i].indexOf('(') === 0) {
                  // le facteur a déjà des parenthèses
                  denQuotient += expressAffine[i]
                } else {
                  if (expressAffine[i].split('+').length > 1) {
                    // il y a une somme donc je vais ajouter des parenthèses
                    denQuotient = '(' + expressAffine[i] + ')'
                  } else {
                    if ((expressAffine[i].split('-').length > 2) || ((expressAffine[i].split('-').length === 2) && (expressAffine[i].split('-')[0] !== ''))) {
                      // il y a une différence
                      denQuotient = '(' + expressAffine[i] + ')'
                    } else {
                      denQuotient = expressAffine[i]
                    }
                  }
                }
                premierFacteurDen = false
              } else {
                if (expressAffine[i].indexOf('(') === 0) {
                  // le facteur a déjà des parenthèses
                  denQuotient += expressAffine[i]
                } else {
                  denQuotient += '(' + expressAffine[i] + ')'
                }
              }
            }
          }
        }
        // maintenant je déclare le quotient
        expressQuotient = '\\frac{' + numQuotient + '}{' + denQuotient + '}'
      }
      const choixineq = j3pGetRandomInt(0, 3)
      const lesSignes = ['<', '>', '\\leq', '\\geq']
      let signeInegalite = lesSignes[choixineq]
      if ((ds.imposer_signe === '<') || (ds.imposer_signe === '>')) {
        signeInegalite = ds.imposer_signe
      } else if (ds.imposer_signe === '<=') {
        signeInegalite = '\\leq'
      } else if (ds.imposer_signe === '>=') {
        signeInegalite = '\\geq'
      }
      stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
      stor.zoneCons1 = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingBottom: '10px' } })
      let domaineText = '\\R'
      if ((ds.domaine_def[0] !== '-infini') || (ds.domaine_def[1] !== '+infini')) {
        // domaine_def est un intervalle inclus dans \\R
        textBorneInf = String(ds.domaine_def[0])
        if (textBorneInf.indexOf('infini') > -1) {
          textBorneInf = textBorneInf.replace('infini', '\\infty')
        } else {
          posFrac = textBorneInf.indexOf('/')
          if (posFrac > -1) {
            // on a une fraction
            textBorneInf = '\\frac{' + textBorneInf.substring(0, posFrac) + '}{' + textBorneInf.substring(posFrac + 1, textBorneInf.length) + '}'
          }
        }
        textBorneSup = String(ds.domaine_def[1])
        if (textBorneSup.includes('infini')) {
          textBorneSup = textBorneSup.replace('infini', '\\infty')
        } else {
          posFrac = textBorneSup.indexOf('/')
          if (posFrac > -1) {
            // on a une fraction
            textBorneSup = '\\frac{' + textBorneSup.substring(0, posFrac) + '}{' + textBorneSup.substring(posFrac + 1, textBorneSup.length) + '}'
          }
        }
        crochetBorneInf = ']'
        if ((ds.domaine_def[2] !== undefined) && (ds.domaine_def[2] !== '')) {
          crochetBorneInf = ds.domaine_def[2]
        }
        crochetBorneSup = '['
        if ((ds.domaine_def[3] !== undefined) && (ds.domaine_def[3] !== '')) {
          crochetBorneSup = ds.domaine_def[3]
        }
        domaineText = crochetBorneInf + textBorneInf + ';' + textBorneSup + crochetBorneSup
      } else {
        textBorneInf = '-\\infty'
        crochetBorneInf = ']'
        textBorneSup = '+\\infty'
        crochetBorneSup = '['
      }
      me.logIfDebug('textBorneInf:' + textBorneInf + '    textBorneSup:' + textBorneSup)
      stor.domaineText = domaineText
      stor.textBorneInf = textBorneInf
      stor.textBorneSup = textBorneSup
      stor.crochetBorneInf = crochetBorneInf
      stor.crochetBorneSup = crochetBorneSup
      if (ds.nbetapes === 2) {
        if (ds.type_expres[0] === 'produit') {
          j3pAffiche(stor.zoneCons1, '', textes.consigne1, {
            a: expressProduit,
            b: expressProduit + signeInegalite + '0',
            d: domaineText
          })
        } else {
          j3pAffiche(stor.zoneCons1, '', textes.consigne2, {
            a: expressQuotient,
            b: expressQuotient + signeInegalite + '0',
            d: domaineText
          })
        }
      } else {
        if (ds.type_expres[0] === 'produit') {
          j3pAffiche(stor.zoneCons1, '', textes.consigne6, {
            a: expressProduit,
            d: domaineText
          })
        } else {
          j3pAffiche(stor.zoneCons1, '', textes.consigne7, {
            a: expressQuotient
          })
        }
      }
      // tableau qui va contenir le signe de chacun des facteurs (juste les chgts de signe)
      const signesFacteurs = []
      const arbreFacteursNonAffines = []
      let calculImage
      const calculImageTab = []
      // je vais avoir besoin dans certains cas d’une valeur du domaine'
      let valeurDomaine
      let borneInfDomaine
      let borneSupDomaine
      // ces deux variables vont me servir pour le signe des fonctions
      // je prendrais Math.pow(10,12) pour infini
      if (ds.domaine_def[0] === '-infini') {
        borneInfDomaine = -Math.pow(10, 12)
        if (ds.domaine_def[1] === '+infini') {
          borneSupDomaine = Math.pow(10, 12)
          valeurDomaine = 0
        } else {
          valeurDomaine = (calculQuot(ds.domaine_def[1]) - 1)
          borneSupDomaine = calculQuot(ds.domaine_def[1])
        }
      } else {
        borneInfDomaine = calculQuot(ds.domaine_def[0])
        if (ds.domaine_def[1] === '+infini') {
          borneSupDomaine = Math.pow(10, 12)
          valeurDomaine = (calculQuot(ds.domaine_def[0]) + 1)
        } else {
          borneSupDomaine = calculQuot(ds.domaine_def[1])
          valeurDomaine = (calculQuot(ds.domaine_def[0]) + calculQuot(ds.domaine_def[1])) / 2
        }
      }
      me.logIfDebug('borneInfDomaine:' + borneInfDomaine + '   borneSupDomaine:' + borneSupDomaine + '    valeurDomaine:' + valeurDomaine)
      for (i = 0; i < Math.min(ds.nbfacteurs, 4); i++) {
        if (ds.facteurs_affines[i]) {
          // le facteur est affine
          if (estDansDomaine(solutionEq[i], ds.domaine_def)) {
            // dans ce cas, je ne me préoccupe pas car la racine de la fonction affine est dans le domaine de def
            signesFacteurs[i] = (coefA[i] < 0) ? ['+', '-'] : ['-', '+']
          } else {
            // le signe de l’image de de valeurDomaine par la fonction affine me donnera le signe de la fonction sur tout le domaine
            const arbreAffine = new Tarbre(expressAffine[i], ['x'])
            signesFacteurs[i] = (arbreAffine.evalue([valeurDomaine]) > 0) ? ['+'] : ['-']
          }
          me.logIfDebug('fonction affine : ' + expressAffine[i] + '  et ses signes:' + signesFacteurs[i])
        } else {
          // le facteur n’est pas affine'
          arbreFacteursNonAffines[i] = new Tarbre(expressAffineInit[i], ['x'])
          let zeroValFctNonAffine
          if (ds.zeros_facteurs_nonaffines[i] === '') {
            // la fonction ne s’annule jamais'
            calculImage = arbreFacteursNonAffines[i].evalue([valeurDomaine])
            signesFacteurs[i] = [(calculImage > 0) ? '+' : '-']
          } else if (!String(ds.zeros_facteurs_nonaffines[i]).includes('|')) {
            // la fonction ne s’annule qu’en une seule valeur'
            // on prend la valeur éventuellement approchée de ce zéro
            zeroValFctNonAffine = calculQuot(ds.zeros_facteurs_nonaffines[i])
            if ((ds.domaine_def[0] === '-infini') && (ds.domaine_def[1] === '+infini')) {
              calculImageTab[0] = arbreFacteursNonAffines[i].evalue([zeroValFctNonAffine - 1])
              calculImageTab[1] = arbreFacteursNonAffines[i].evalue([zeroValFctNonAffine + 1])
              signesFacteurs[i] = []
              for (k = 0; k < 2; k++) signesFacteurs[i].push((calculImageTab[k] > 0) ? '+' : '-')
            } else {
              if (estDansDomaine(zeroValFctNonAffine, ds.domaine_def)) {
                if (zeroValFctNonAffine - 1 > borneInfDomaine) {
                  calculImageTab[0] = arbreFacteursNonAffines[i].evalue([zeroValFctNonAffine - 1])
                } else {
                  calculImageTab[0] = arbreFacteursNonAffines[i].evalue([(zeroValFctNonAffine + borneInfDomaine) / 2])
                }
                if (zeroValFctNonAffine + 1 < borneSupDomaine) {
                  calculImageTab[1] = arbreFacteursNonAffines[i].evalue([zeroValFctNonAffine + 1])
                } else {
                  calculImageTab[1] = arbreFacteursNonAffines[i].evalue([(zeroValFctNonAffine + borneSupDomaine) / 2])
                }
                signesFacteurs[i] = []
                for (k = 0; k < 2; k++) {
                  signesFacteurs[i].push((calculImageTab[k] > 0) ? '+' : '-')
                }
              } else {
                calculImageTab[0] = arbreFacteursNonAffines[i].evalue([valeurDomaine])
                signesFacteurs[i] = [(calculImageTab[0] > 0) ? '+' : '-']
              }
            }
          } else {
            // la fonction s’annule en plusieurs valeurs'
            const zeroFctNonAffineTab = ds.zeros_facteurs_nonaffines[i].split('|')
            let zeroValFctNonAffineTab = []
            for (k = 0; k < zeroFctNonAffineTab.length; k++) {
              if (estDansDomaine(zeroFctNonAffineTab[k], ds.domaine_def)) {
                zeroValFctNonAffineTab.push(calculQuot(zeroFctNonAffineTab[k]))
              }
            }
            if (zeroValFctNonAffineTab.length > 1) {
              zeroValFctNonAffineTab = fctsEtudeOrdonneTabSansDoublon(zeroValFctNonAffineTab)
            }
            signesFacteurs[i] = []
            me.logIfDebug('zeroValFctNonAffineTab:' + zeroValFctNonAffineTab)
            if (zeroValFctNonAffineTab.length === 0) {
              // la fonction ne s’annule finalement pas sur le domaine
              calculImage = arbreFacteursNonAffines[i].evalue([valeurDomaine])
              signesFacteurs[i] = [(calculImage > 0) ? '+' : '-']
            } else {
              for (k = 0; k <= zeroValFctNonAffineTab.length; k++) {
                if (k === 0) {
                  if (Number(zeroValFctNonAffineTab[k]) - 1 > borneInfDomaine) {
                    calculImageTab[k] = arbreFacteursNonAffines[i].evalue([Number(zeroValFctNonAffineTab[k]) - 1])
                  } else {
                    calculImageTab[k] = arbreFacteursNonAffines[i].evalue([(Number(zeroValFctNonAffineTab[k]) + borneInfDomaine) / 2])
                  }
                } else if (k === zeroValFctNonAffineTab.length) {
                  if (Number(zeroValFctNonAffineTab[k - 1]) + 1 < borneSupDomaine) {
                    calculImageTab[k] = arbreFacteursNonAffines[i].evalue([Number(zeroValFctNonAffineTab[k - 1]) + 1])
                  } else {
                    calculImageTab[k] = arbreFacteursNonAffines[i].evalue([(Number(zeroValFctNonAffineTab[k - 1]) + borneSupDomaine) / 2])
                  }
                } else {
                  calculImageTab[k] = arbreFacteursNonAffines[i].evalue([(Number(zeroValFctNonAffineTab[k - 1]) + Number(zeroValFctNonAffineTab[k])) / 2])
                }
                signesFacteurs[i].push((calculImageTab[k] > 0) ? '+' : '-')
              }
            }
          }
          me.logIfDebug('fonction non affine : ' + expressAffine[i] + '  et ses signes:' + signesFacteurs[i])
        }
      }
      // pour le signe du produit/quotient, on peut mettre des zones de saisie (false) ou les signes (true) dans le dernier élément du tableau
      signesFacteurs[Math.min(ds.nbfacteurs, 4)] = false
      stor.leTableau1 = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.enonce', { position: 'relative', top: 0, left: 0 }) })
      const typeProduitQuotient = []
      if (ds.type_expres[0] === 'produit') {
        typeProduitQuotient[0] = 'produit'
      } else {
        typeProduitQuotient[0] = 'quotient'
        typeProduitQuotient[1] = []
        for (i = 1; i < ds.type_expres.length; i++) {
          typeProduitQuotient[1][i - 1] = (ds.type_expres[i] !== 'num')
        }
      }
      stor.typeProdQuot = typeProduitQuotient
      if ((ds.domaine_def[0] === '-infini') && (ds.domaine_def[1] === '+infini')) {
        tabsignesProduit(stor.leTableau1, expressAffine, [stor.largColGauche, stor.tailleLigne * (expressAffine.length + 2)], typeProduitQuotient, [false, false], tabSolEqText, signesFacteurs, { style: me.styles.toutpetit.enonce })
      } else {
        tabsignesProduit(stor.leTableau1, expressAffine, [stor.largColGauche, stor.tailleLigne * (expressAffine.length + 2), ds.domaine_def], typeProduitQuotient, [false, false], tabSolEqText, signesFacteurs, { style: me.styles.toutpetit.enonce })
      }
      me.logIfDebug('expressAffine : ' + expressAffine)
      me.logIfDebug('solEq : ' + solEq)
      stor.solEq = solEq
      // stor.coefA = coefA
      // stor.coefB = coefB
      stor.expressAffine = expressAffine
      stor.tabSolutionText = tabSolEqText
      stor.tabSolutionOrdonnee = fctsEtudeOrdonneTabSansDoublon(tabSolEqText)
      me.logIfDebug('stor.tabSolutionOrdonnee:', stor.tabSolutionOrdonnee, 'valeurs ordonnées de ', tabSolEqText)
      stor.signesFacteurs = signesFacteurs
      stor.typeExpression = (ds.type_expres[0] === 'produit') ? expressProduit : expressQuotient
      stor.signeInegalite = signeInegalite
      stor.tabSigneRep = tableauSigneRep(stor.tabSolutionText, stor.signesFacteurs)
      me.logIfDebug('tabSolEqText:' + tabSolEqText + '   ' + stor.tabSolutionOrdonnee)
      me.logIfDebug('tableau réponse des signes : ' + stor.tabSigneRep)
      // tabSolEqLatexOrdonne est le tableau avec les réponses dans l’ordre au format latex (utile pour la correction)'
      const tabSolEqLatexOrdonne = []
      for (i = 0; i < stor.tabSolutionOrdonnee.length; i++) {
        // je mets au format latex toutes les valeurs ordonnées où l’une au moins des fonctions s’annule
        if (String(stor.tabSolutionOrdonnee[i]).indexOf('/') === -1) {
          // on a un entier, un nombre décimal ou bien un radical voire une exp ou un ln
          tabSolEqLatexOrdonne[i] = String(stor.tabSolutionOrdonnee[i]).replace(/\./g, ',')
          tabSolEqLatexOrdonne[i] = convertirExpLnRacine(tabSolEqLatexOrdonne[i])
        } else {
          const numstockage26 = String(stor.tabSolutionOrdonnee[i]).split('/')[0]
          const denstockage26 = String(stor.tabSolutionOrdonnee[i]).split('/')[1]
          if (Number(numstockage26) * Number(denstockage26) > 0) {
            // on a un nb positif
            tabSolEqLatexOrdonne[i] = '\\frac{' + Math.abs(Number(numstockage26)) + '}{' + Math.abs(Number(denstockage26)) + '}'
          } else {
            tabSolEqLatexOrdonne[i] = '-\\frac{' + Math.abs(Number(numstockage26)) + '}{' + Math.abs(Number(denstockage26)) + '}'
          }
        }
      }
      me.logIfDebug('tabSolEqText:' + tabSolEqText + '  tabSolEqLatexOrdonne:' + tabSolEqLatexOrdonne)
      stor.tabSolLatexOrdonne = tabSolEqLatexOrdonne
      // on regarde si l’un des facteurs n’est pas affine'
      facteursTousAffines = true
      for (i = 0; i < ds.nbfacteurs; i++) {
        if (!ds.facteurs_affines[i]) facteursTousAffines = false
      }
      stor.remarque = j3pAddElt(stor.conteneur, 'div')
      if (facteursTousAffines) {
        if (solEq.length > stor.tabSolutionOrdonnee.length) {
          if (stor.tabSolutionOrdonnee.length === 1) {
            j3pAffiche(stor.remarque, '', '<u>' + textes.remarque1 + '</u> ' + textes.remarque2)
          } else {
            j3pAffiche(stor.remarque, '', '<u>' + textes.remarque1 + '</u> ' + textes.remarque3)
          }
        }
      } else {
        j3pAffiche(stor.remarque, '', '<u>' + textes.remarque1 + '</u> ' + textes.remarque4)
      }
      // dans certains cas, on doit mettre la palette de boutons avec des boutons particuliers (sqrt, exp, ln)
      const affichagePalette = true // finalement dans tous les cas, on met une palette (elle ne conteint qu’une fraction dans le cas simple)
      const listeBoutons = ['fraction']
      for (i = 0; i < ds.imposer_fct.length; i++) {
        if (ds.imposer_fct[i] !== undefined) {
          if (ds.imposer_fct[i].indexOf('^') > -1) {
            if (listeBoutons.indexOf('racine') === -1) listeBoutons.push('racine')
          }
          if ((ds.imposer_fct[i].indexOf('exp') > -1) || (ds.imposer_fct[i].indexOf('ln') > -1)) {
            // affichagePalette = true;
            if (listeBoutons.indexOf('exp') === -1) listeBoutons.push('exp', 'ln')
          }
        }
      }
      stor.affichagePalette = affichagePalette
      stor.listeBoutons = listeBoutons
      if (affichagePalette) {
        stor.listeBoutons = listeBoutons
        stor.laPalette = j3pAddElt(stor.conteneur, 'div')
        stor.btnsPalette = j3pAddElt(stor.laPalette, 'span')
        // palette MQ :
        j3pPaletteMathquill(stor.btnsPalette, stor.inputZoneNulle[0], { liste: listeBoutons })
        for (i = 1; i <= tabSolEqLatexOrdonne.length; i++) {
          stor.inputZoneNulle[i - 1].addEventListener('focusin', ecoute.bind(null, i - 1))
          // $(stor.inputZoneNulle[i - 1]).focusin(ecoute.bind(null, i - 1))
        }
      }
      for (i = 0; i < ds.imposer_fct.length; i++) ds.imposer_fct[i] = stor.imposer_fct_init[i]
    }
    if (me.etapeCourante === 2) {
      // deuxième question : inéquation à résoudre
      stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
      stor.zoneCons1 = j3pAddElt(stor.conteneur, 'div')
      if (ds.type_expres[0] === 'produit') {
        j3pAffiche(stor.zoneCons1, '', textes.consigne3, {
          a: stor.typeExpression,
          d: stor.domaineText
        })
      } else {
        j3pAffiche(stor.zoneCons1, '', textes.consigne4, { a: stor.typeExpression })
      }
      stor.leTableau1 = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.enonce', { position: 'relative', top: 0, left: 0, paddingTop: '5px' }) })

      stor.signesFacteurs[stor.signesFacteurs.length - 1] = true
      if ((ds.domaine_def[0] === '-infini') && (ds.domaine_def[1] === '+infini')) {
        tabsignesProduit(stor.leTableau1, stor.expressAffine, [stor.largColGauche, stor.tailleLigne * (stor.expressAffine.length + 2)], stor.typeProdQuot, [true, true], stor.tabSolutionText, stor.signesFacteurs, { style: me.styles.toutpetit.enonce })
      } else {
        tabsignesProduit(stor.leTableau1, stor.expressAffine, [stor.largColGauche, stor.tailleLigne * (stor.expressAffine.length + 2), ds.domaine_def], stor.typeProdQuot, [true, true], stor.tabSolutionText, stor.signesFacteurs, { style: me.styles.toutpetit.enonce })
      }
      stor.zoneCons2 = j3pAddElt(stor.conteneur, 'div')
      const elt = j3pAffiche(stor.zoneCons2, '', textes.consigne5, {
        a: stor.typeExpression + stor.signeInegalite + '0',
        inputmq1: { texte: '', maxchars: '20' }
      })
      stor.zoneInput = [elt.inputmqList[0]]
      const boutonsInequation = [...stor.listeBoutons]
      boutonsInequation.push('inf', 'inter', 'union', 'vide')
      mqRestriction(stor.zoneInput[0], '\\d.,+-[]^/;', { commandes: boutonsInequation })
      const boutonsInequationPalette = [...boutonsInequation].concat(['[', ']']) // les crochets ne sont pas des commandes mq donc on les traite différemment dans la palette que dans mqRestriction
      stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px' } })
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], { liste: boutonsInequationPalette })
      // construction de tous les intervalles présents dans le tableau
      const tabIntervallesRep = []
      // construction des intervalles avec le code latex (pour la correction)
      const tabLatex = []
      for (i = 0; i <= stor.tabSolutionOrdonnee.length; i++) {
        if (i === 0) {
          if (stor.tabSolutionOrdonnee.length === 0) {
            tabIntervallesRep[i] = stor.textBorneInf + ';' + stor.textBorneSup
            tabLatex[i] = stor.textBorneInf + ';' + stor.textBorneSup
          } else {
            tabIntervallesRep[i] = stor.textBorneInf + ';' + j3pArrondi(calculQuot(stor.tabSolutionOrdonnee[0]), 10)
            tabLatex[i] = stor.textBorneInf + ';' + stor.tabSolLatexOrdonne[0]
          }
        } else if (i === stor.tabSolutionOrdonnee.length) {
          tabIntervallesRep[i] = j3pArrondi(calculQuot(stor.tabSolutionOrdonnee[stor.tabSolutionOrdonnee.length - 1]), 10) + ';' + stor.textBorneSup
          tabLatex[i] = stor.tabSolLatexOrdonne[stor.tabSolLatexOrdonne.length - 1] + ';' + stor.textBorneSup
        } else {
          tabIntervallesRep[i] = j3pArrondi(calculQuot(stor.tabSolutionOrdonnee[i - 1]), 10) + ';' + j3pArrondi(calculQuot(stor.tabSolutionOrdonnee[i]), 10)
          tabLatex[i] = stor.tabSolLatexOrdonne[i - 1] + ';' + stor.tabSolLatexOrdonne[i]
        }
      }
      // me.stockage[31] = tabIntervallesRep
      me.logIfDebug('tabIntervallesRep:' + tabIntervallesRep + '   tabLatex:' + tabLatex)
      // maintenant, je ne garde plus que les intervalles solutions (avec les bons crochets)
      const tabIntervallesSol = []
      const tabIntervallesComplementaire = []
      let leSigneTab
      const signeAttendu = ((stor.signeInegalite === '<') || (stor.signeInegalite === '\\leq')) ? 'negatif' : 'positif'
      const tabLatexSol = []
      for (i = 1; i <= stor.tabSolutionOrdonnee.length + 1; i++) {
        leSigneTab = stor.tabSigneRep[stor.expressAffine.length * (stor.tabSolutionOrdonnee.length + 1) + i]// stor.expressAffine.length est le nb de lignes et stor.tabSolutionOrdonnee.length+1 est le nb de colonnes
        if (((leSigneTab === '-') && (signeAttendu === 'negatif')) || ((leSigneTab === '+') && (signeAttendu === 'positif'))) {
          tabIntervallesSol.push(tabIntervallesRep[i - 1])
          tabLatexSol.push(tabLatex[i - 1])
        } else {
          tabIntervallesComplementaire.push(tabIntervallesRep[i - 1])
        }
      }
      const tabInterReponse = []
      const tabInterContraire = []
      let valGauche// pour récupérer la valeur de gauche de l’intervalle'
      let valDroite// pour récupérer la valeur de droite de l’intervalle/
      let valInterdite// booléen qui porte bien son nom
      for (i = 0; i < tabIntervallesSol.length; i++) {
        // gestion du crochet à gauche
        tabInterReponse[i] = tabIntervallesSol[i]
        if (tabInterReponse[i].indexOf(stor.textBorneInf) !== -1) {
          tabInterReponse[i] = stor.crochetBorneInf + String(tabIntervallesSol[i])
          tabLatexSol[i] = stor.crochetBorneInf + tabLatexSol[i]
        } else {
          // on vérife si le crochet est ouvert ou pas
          if (ds.type_expres[0] === 'quotient') {
            // pb avec le quotient car je dois vérifier si la valeur est interdite
            valGauche = tabIntervallesSol[i].substring(0, tabIntervallesSol[i].indexOf(';'))
            valInterdite = false
            for (k = 0; k < stor.tabSolutionText.length; k++) {
              if (String(stor.tabSolutionText[k]).indexOf('|') === -1) {
                // il y a au max une valeur qui annule la fonction
                if (Math.abs(calculQuot(stor.tabSolutionText[k]) - Number(valGauche)) < Math.pow(10, -9)) {
                  // je regarde si c’est une valeur interdite'
                  valInterdite = (valInterdite || (ds.type_expres[k + 1] !== 'num'))
                }
              } else {
                // la fonction s’annule en plus d’une valeur'
                stockage25kTab = stor.tabSolutionText[k].split('|')
                for (k4 = 0; k4 < stockage25kTab.length; k4++) {
                  if (Math.abs(calculQuot(stockage25kTab[k4]) - Number(valGauche)) < Math.pow(10, -9)) {
                    // je regarde si c’est une valeur interdite'
                    valInterdite = (valInterdite || (ds.type_expres[k + 1] !== 'num'))
                  }
                }
              }
            }
          } else {
            valInterdite = false
          }
          if ((stor.signeInegalite === '<') || (stor.signeInegalite === '>')) {
            tabInterReponse[i] = ']' + String(tabIntervallesSol[i])
            tabLatexSol[i] = ']' + tabLatexSol[i]
          } else {
            if (valInterdite) {
              tabInterReponse[i] = ']' + String(tabIntervallesSol[i])
              tabLatexSol[i] = ']' + tabLatexSol[i]
            } else {
              tabInterReponse[i] = '[' + String(tabIntervallesSol[i])
              tabLatexSol[i] = '[' + tabLatexSol[i]
            }
          }
        }
        // gestion du crochet à droite
        if (tabInterReponse[i].indexOf(stor.textBorneSup) !== -1) {
          tabInterReponse[i] += stor.crochetBorneSup
          tabLatexSol[i] = tabLatexSol[i] + stor.crochetBorneSup
        } else {
          // on vérife si le crochet est ouvert ou pas
          if (ds.type_expres[0] === 'quotient') {
            // pb avec le quotient car je dois vérifier si la valeur est interdite
            valDroite = tabIntervallesSol[i].substring(tabIntervallesSol[i].indexOf(';') + 1)
            valInterdite = false
            for (k = 0; k < stor.tabSolutionText.length; k++) {
              if (String(stor.tabSolutionText[k]).indexOf('|') === -1) {
                // il y a au max une valeur qui annule la fonction
                if (Math.abs(calculQuot(stor.tabSolutionText[k]) - Number(valDroite)) < Math.pow(10, -9)) {
                  // je regarde si c’est une valeur interdite'
                  valInterdite = (valInterdite || (ds.type_expres[k + 1] !== 'num'))
                }
              } else {
                // la fonction s’annule en plus d’une valeur'
                stockage25kTab = stor.tabSolutionText[k].split('|')
                for (k4 = 0; k4 < stockage25kTab.length; k4++) {
                  if (Math.abs(calculQuot(stockage25kTab[k4]) - Number(valDroite)) < Math.pow(10, -9)) {
                    // je regarde si c’est une valeur interdite'
                    valInterdite = (valInterdite || (ds.type_expres[k + 1] !== 'num'))
                  }
                }
              }
            }
          } else {
            valInterdite = false
          }
          if ((stor.signeInegalite === '<') || (stor.signeInegalite === '>')) {
            tabInterReponse[i] += '['
            tabLatexSol[i] = tabLatexSol[i] + '['
          } else {
            if (valInterdite) {
              tabInterReponse[i] += '['
              tabLatexSol[i] = tabLatexSol[i] + '['
            } else {
              tabInterReponse[i] += ']'
              tabLatexSol[i] = tabLatexSol[i] + ']'
            }
          }
        }
      }
      for (i = 0; i < tabIntervallesComplementaire.length; i++) {
        tabInterContraire[i] = tabIntervallesComplementaire[i]
        if (tabInterContraire[i].indexOf('-\\infty') !== -1) {
          tabInterContraire[i] = ']' + String(tabIntervallesComplementaire[i])
        } else {
          // on vérife si le crochet est ouvert ou pas
          if (ds.type_expres[0] === 'quotient') {
            // pb avec le quotient car je dois vérifier si la valeur est interdite
            valGauche = tabIntervallesComplementaire[i].substring(0, tabIntervallesComplementaire[i].indexOf(';'))
            // valGauche = tabIntervallesSol[i].split(";")[0];
            valInterdite = false
            for (k = 0; k < stor.tabSolutionText.length; k++) {
              if (Math.abs(calculQuot(stor.tabSolutionText[k]) - Number(valGauche)) < Math.pow(10, -9)) {
                // je regarde si c’est une valeur interdite'
                valInterdite = (ds.type_expres[k + 1] !== 'num')
              }
            }
          } else {
            valInterdite = false
          }
          if ((stor.signeInegalite === '<') || (stor.signeInegalite === '>')) {
            tabInterContraire[i] = ']' + String(tabIntervallesComplementaire[i])
          } else {
            if (valInterdite) {
              tabInterContraire[i] = ']' + String(tabIntervallesComplementaire[i])
            } else {
              tabInterContraire[i] = '[' + String(tabIntervallesComplementaire[i])
            }
          }
        }
        if (tabInterContraire[i].indexOf('+\\infty') !== -1) {
          tabInterContraire[i] += '['
        } else {
          // on vérife si le crochet est ouvert ou pas
          if (ds.type_expres[0] === 'quotient') {
            // pb avec le quotient car je dois vérifier si la valeur est interdite
            valDroite = tabIntervallesComplementaire[i].substring(tabIntervallesComplementaire[i].indexOf(';') + 1)
            valInterdite = false
            for (k = 0; k < stor.tabSolutionText.length; k++) {
              if (Math.abs(calculQuot(stor.tabSolutionText[k]) - Number(valDroite)) < Math.pow(10, -9)) {
                // je regarde si c’est une valeur interdite'
                valInterdite = (ds.type_expres[k + 1] !== 'num')
              }
            }
          } else {
            valInterdite = false
          }
          if ((stor.signeInegalite === '<') || (stor.signeInegalite === '>')) {
            tabInterContraire[i] += '['
          } else {
            if (valInterdite) {
              tabInterContraire[i] += '['
            } else {
              tabInterContraire[i] += ']'
            }
          }
        }
      }
      me.logIfDebug('tabIntervallesSol', tabIntervallesSol, '\ntabIntervallesComplementaire', tabIntervallesComplementaire, '\ntabInterReponse,', tabInterReponse, '\ntabInterContraire', tabInterContraire)
      stor.tabIntervalleSol = tabIntervallesSol
      // me.stockage[33] = tabIntervallesComplementaire
      // me.stockage[34] = tabInterReponse
      // Pour finir, je crée toutes les réunions possibles d’intervalles
      // je vais considérer qu’il ne peut pas y avoir plus de 3 intervalles dans l’ensemble des sols on l’ensemble contraire'
      const reunionPossiblesReponse = []
      if (tabInterReponse.length === 1) {
        reunionPossiblesReponse.push(tabInterReponse[0])
      } else if (tabInterReponse.length === 2) {
        reunionPossiblesReponse.push(tabInterReponse[0] + '\\cup' + tabInterReponse[1], tabInterReponse[1] + '\\cup' + tabInterReponse[0])
      } else if (tabInterReponse.length === 3) {
        reunionPossiblesReponse.push(tabInterReponse[0] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[2], tabInterReponse[0] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[1], tabInterReponse[1] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[2], tabInterReponse[1] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[0], tabInterReponse[2] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[1], tabInterReponse[2] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[0])
      } else if (tabInterReponse.length === 4) {
        reunionPossiblesReponse.push(tabInterReponse[0] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[3], tabInterReponse[0] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[2], tabInterReponse[0] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[3], tabInterReponse[0] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[1], tabInterReponse[0] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[2], tabInterReponse[0] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[1], tabInterReponse[1] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[3], tabInterReponse[1] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[2], tabInterReponse[1] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[3], tabInterReponse[1] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[0], tabInterReponse[2] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[3], tabInterReponse[2] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[1], tabInterReponse[2] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[3], tabInterReponse[2] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[0], tabInterReponse[2] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[1], tabInterReponse[2] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[0], tabInterReponse[3] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[2], tabInterReponse[3] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[1], tabInterReponse[3] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[2], tabInterReponse[3] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[0], tabInterReponse[3] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[1], tabInterReponse[3] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[0], tabInterReponse[1] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[2], tabInterReponse[1] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[0])
      }
      // gestion du cas particulier d’une inégalité large où on peut réunir deux intervalles'
      if ((stor.signeInegalite === '\\geq') || (stor.signeInegalite === '\\leq')) {
        // valable seulement si l’inégalité est large
        const posPointVirgule = []
        const borneSupInter = []
        const borneInfInter = []
        if (tabInterReponse.length === 2) {
          posPointVirgule[0] = tabInterReponse[0].indexOf(';')
          posPointVirgule[1] = tabInterReponse[1].indexOf(';')
          borneSupInter[0] = tabInterReponse[0].substring(posPointVirgule[0] + 1, tabInterReponse[0].length - 1)
          borneInfInter[1] = tabInterReponse[1].substring(1, posPointVirgule[1])
          me.logIfDebug('borneSupInter:' + borneSupInter + '   borneInfInter:' + borneInfInter)
          if ((Math.abs(borneSupInter[0] - borneInfInter[1]) < Math.pow(10, -13)) && ((tabInterReponse[0].charAt(tabInterReponse[0].length - 1) === ']') && (tabInterReponse[1].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            reunionPossiblesReponse.push(tabInterReponse[0].substring(0, posPointVirgule[0]) + ';' + tabInterReponse[1].substring(posPointVirgule[1] + 1, tabInterReponse[1].length))
          }
        } else if (tabInterReponse.length === 3) {
          for (i = 0; i <= 2; i++) {
            posPointVirgule[i] = tabInterReponse[i].indexOf(';')
            borneSupInter[i] = tabInterReponse[i].substring(posPointVirgule[i] + 1, tabInterReponse[i].length - 1)
            borneInfInter[i] = tabInterReponse[i].substring(1, posPointVirgule[i])
          }
          me.logIfDebug('borneSupInter:' + borneSupInter + '   borneInfInter:' + borneInfInter)
          if ((Math.abs(borneSupInter[0] - borneInfInter[1]) < Math.pow(10, -13)) && ((tabInterReponse[0].charAt(tabInterReponse[0].length - 1) === ']') && (tabInterReponse[1].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            reunionPossiblesReponse.push(tabInterReponse[0].substring(0, posPointVirgule[0]) + ';' + tabInterReponse[1].substring(posPointVirgule[1] + 1, tabInterReponse[1].length) + '\\cup' + tabInterReponse[2], tabInterReponse[2] + '\\cup' + tabInterReponse[0].substring(0, posPointVirgule[0]) + ';' + tabInterReponse[1].substring(posPointVirgule[1] + 1, tabInterReponse[1].length))
          }
          if ((Math.abs(borneSupInter[1] - borneInfInter[2]) < Math.pow(10, -13)) && ((tabInterReponse[1].charAt(tabInterReponse[1].length - 1) === ']') && (tabInterReponse[2].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            reunionPossiblesReponse.push(tabInterReponse[1].substring(0, posPointVirgule[1]) + ';' + tabInterReponse[2].substring(posPointVirgule[2] + 1, tabInterReponse[2].length) + '\\cup' + tabInterReponse[0], tabInterReponse[0] + '\\cup' + tabInterReponse[1].substring(0, posPointVirgule[1]) + ';' + tabInterReponse[2].substring(posPointVirgule[2] + 1, tabInterReponse[2].length))
          }
        } else if (tabInterReponse.length === 4) {
          for (i = 0; i <= 3; i++) {
            posPointVirgule[i] = tabInterReponse[i].indexOf(';')
            borneSupInter[i] = tabInterReponse[i].substring(posPointVirgule[i] + 1, tabInterReponse[i].length - 1)
            borneInfInter[i] = tabInterReponse[i].substring(1, posPointVirgule[i])
          }
          me.logIfDebug('borneSupInter:' + borneSupInter + '   borneInfInter:' + borneInfInter)
          if ((Math.abs(borneSupInter[0] - borneInfInter[1]) < Math.pow(10, -13)) && ((tabInterReponse[0].charAt(tabInterReponse[0].length - 1) === ']') && (tabInterReponse[1].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            reunionPossiblesReponse.push(tabInterReponse[0].substring(0, posPointVirgule[0]) + ';' + tabInterReponse[1].substring(posPointVirgule[1] + 1, tabInterReponse[1].length) + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[3], tabInterReponse[0].substring(0, posPointVirgule[0]) + ';' + tabInterReponse[1].substring(posPointVirgule[1] + 1, tabInterReponse[1].length) + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[2], tabInterReponse[2] + '\\cup' + tabInterReponse[0].substring(0, posPointVirgule[0]) + ';' + tabInterReponse[1].substring(posPointVirgule[1] + 1, tabInterReponse[1].length) + '\\cup' + tabInterReponse[3], tabInterReponse[2] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[0].substring(0, posPointVirgule[0]) + ';' + tabInterReponse[1].substring(posPointVirgule[1] + 1, tabInterReponse[1].length), tabInterReponse[3] + '\\cup' + tabInterReponse[2] + '\\cup' + tabInterReponse[0].substring(0, posPointVirgule[0]) + ';' + tabInterReponse[1].substring(posPointVirgule[1] + 1, tabInterReponse[1].length), tabInterReponse[3] + '\\cup' + tabInterReponse[0].substring(0, posPointVirgule[0]) + ';' + tabInterReponse[1].substring(posPointVirgule[1] + 1, tabInterReponse[1].length) + '\\cup' + tabInterReponse[2])
          }
          if ((Math.abs(borneSupInter[1] - borneInfInter[2]) < Math.pow(10, -13)) && ((tabInterReponse[1].charAt(tabInterReponse[1].length - 1) === ']') && (tabInterReponse[2].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            reunionPossiblesReponse.push(tabInterReponse[1].substring(0, posPointVirgule[1]) + ';' + tabInterReponse[2].substring(posPointVirgule[2] + 1, tabInterReponse[2].length) + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[3], tabInterReponse[0] + '\\cup' + tabInterReponse[1].substring(0, posPointVirgule[1]) + ';' + tabInterReponse[2].substring(posPointVirgule[2] + 1, tabInterReponse[2].length) + '\\cup' + tabInterReponse[3], tabInterReponse[1].substring(0, posPointVirgule[1]) + ';' + tabInterReponse[2].substring(posPointVirgule[2] + 1, tabInterReponse[2].length) + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[0], tabInterReponse[0] + '\\cup' + tabInterReponse[3] + '\\cup' + tabInterReponse[1].substring(0, posPointVirgule[1]) + ';' + tabInterReponse[2].substring(posPointVirgule[2] + 1, tabInterReponse[2].length), tabInterReponse[3] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[1].substring(0, posPointVirgule[1]) + ';' + tabInterReponse[2].substring(posPointVirgule[2] + 1, tabInterReponse[2].length), tabInterReponse[3] + '\\cup' + tabInterReponse[1].substring(0, posPointVirgule[1]) + ';' + tabInterReponse[2].substring(posPointVirgule[2] + 1, tabInterReponse[2].length) + '\\cup' + tabInterReponse[0])
          }
          if ((Math.abs(borneSupInter[2] - borneInfInter[3]) < Math.pow(10, -13)) && ((tabInterReponse[2].charAt(tabInterReponse[2].length - 1) === ']') && (tabInterReponse[3].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            reunionPossiblesReponse.push(tabInterReponse[2].substring(0, posPointVirgule[2]) + ';' + tabInterReponse[3].substring(posPointVirgule[3] + 1, tabInterReponse[3].length) + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[0], tabInterReponse[1] + '\\cup' + tabInterReponse[2].substring(0, posPointVirgule[2]) + ';' + tabInterReponse[3].substring(posPointVirgule[3] + 1, tabInterReponse[3].length) + '\\cup' + tabInterReponse[0], tabInterReponse[2].substring(0, posPointVirgule[2]) + ';' + tabInterReponse[3].substring(posPointVirgule[3] + 1, tabInterReponse[3].length) + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[1], tabInterReponse[1] + '\\cup' + tabInterReponse[0] + '\\cup' + tabInterReponse[2].substring(0, posPointVirgule[2]) + ';' + tabInterReponse[3].substring(posPointVirgule[3] + 1, tabInterReponse[3].length), tabInterReponse[0] + '\\cup' + tabInterReponse[1] + '\\cup' + tabInterReponse[2].substring(0, posPointVirgule[2]) + ';' + tabInterReponse[3].substring(posPointVirgule[3] + 1, tabInterReponse[3].length), tabInterReponse[0] + '\\cup' + tabInterReponse[2].substring(0, posPointVirgule[2]) + ';' + tabInterReponse[3].substring(posPointVirgule[3] + 1, tabInterReponse[3].length) + '\\cup' + tabInterReponse[1])
          }
        }
      }
      // de même pour la solution contraire
      const reunionPossiblesContraire = []
      if (tabInterContraire.length === 1) {
        reunionPossiblesContraire.push(tabInterContraire[0])
      } else if (tabInterContraire.length === 2) {
        reunionPossiblesContraire.push(tabInterContraire[0] + '\\cup' + tabInterContraire[1], tabInterContraire[1] + '\\cup' + tabInterContraire[0])
      } else if (tabInterContraire.length === 3) {
        reunionPossiblesContraire.push(tabInterContraire[0] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[2], tabInterContraire[0] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[1], tabInterContraire[1] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[2], tabInterContraire[1] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[0], tabInterContraire[2] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[1], tabInterContraire[2] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[0])
      } else if (tabInterContraire.length === 4) {
        reunionPossiblesContraire.push(tabInterContraire[0] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[3], tabInterContraire[0] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[2], tabInterContraire[0] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[3], tabInterContraire[0] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[1], tabInterContraire[0] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[2], tabInterContraire[0] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[1], tabInterContraire[1] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[3], tabInterContraire[1] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[2], tabInterContraire[1] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[3], tabInterContraire[1] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[0], tabInterContraire[2] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[3], tabInterContraire[2] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[1], tabInterContraire[2] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[3], tabInterContraire[2] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[0], tabInterContraire[2] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[1], tabInterContraire[2] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[0], tabInterContraire[3] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[2], tabInterContraire[3] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[1], tabInterContraire[3] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[2], tabInterContraire[3] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[0], tabInterContraire[3] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[1], tabInterContraire[3] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterContraire[1] + '\\cup' + tabInterContraire[0], tabInterContraire[1] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[0] + '\\cup' + tabInterContraire[2], tabInterContraire[1] + '\\cup' + tabInterContraire[3] + '\\cup' + tabInterContraire[2] + '\\cup' + tabInterReponse[0])
      }
      stor.unionPossible = reunionPossiblesReponse
      me.logIfDebug('reunionPossiblesReponse:' + reunionPossiblesReponse)
      stor.unionContraire = reunionPossiblesContraire
      // la réponse telle qu’elle sera affichée'
      stor.laRepLatex = tabLatexSol[0]
      if (tabLatexSol.length > 1) {
        for (i = 1; i < tabLatexSol.length; i++) stor.laRepLatex += '\\cup' + tabLatexSol[i]
      }
      // gestion du cas particulier d’une inégalité large où on peut réunir deux intervalles'
      if ((stor.signeInegalite === '\\geq') || (stor.signeInegalite === '\\leq')) {
        const posPointVirgulelatex = []
        const borneSupInterlatex = []
        const borneInfInterlatex = []
        if (tabLatexSol.length === 2) {
          posPointVirgulelatex[0] = tabLatexSol[0].indexOf(';')
          posPointVirgulelatex[1] = tabLatexSol[1].indexOf(';')
          borneSupInterlatex[0] = tabLatexSol[0].substring(posPointVirgulelatex[0] + 1, tabLatexSol[0].length - 1)
          borneInfInterlatex[1] = tabLatexSol[1].substring(1, posPointVirgulelatex[1])
          me.logIfDebug('borneSupInterlatex:' + borneSupInterlatex + '   borneInfInterlatex:' + borneInfInterlatex)
          if ((borneSupInterlatex[0] === borneInfInterlatex[1]) && ((tabLatexSol[0].charAt(tabLatexSol[0].length - 1) === ']') && (tabLatexSol[1].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            stor.laRepLatex = tabLatexSol[0].substring(0, posPointVirgulelatex[0]) + ';' + tabLatexSol[1].substring(posPointVirgulelatex[1] + 1, tabLatexSol[1].length)
          }
        } else if (tabLatexSol.length === 3) {
          for (i = 0; i <= 2; i++) {
            posPointVirgulelatex[i] = tabLatexSol[i].indexOf(';')
            borneSupInterlatex[i] = tabLatexSol[i].substring(posPointVirgulelatex[i] + 1, tabLatexSol[i].length - 1)
            borneInfInterlatex[i] = tabLatexSol[i].substring(1, posPointVirgulelatex[i])
          }
          me.logIfDebug('borneSupInterlatex:' + borneSupInterlatex + '   borneInfInterlatex:' + borneInfInterlatex)
          if ((borneSupInterlatex[0] === borneInfInterlatex[1]) && ((tabLatexSol[0].charAt(tabLatexSol[0].length - 1) === ']') && (tabLatexSol[1].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            stor.laRepLatex = tabLatexSol[0].substring(0, posPointVirgulelatex[0]) + ';' + tabLatexSol[1].substring(posPointVirgulelatex[1] + 1, tabLatexSol[1].length) + '\\cup' + tabLatexSol[2]
          }
          if ((borneSupInterlatex[1] === borneInfInterlatex[2]) && ((tabLatexSol[1].charAt(tabLatexSol[1].length - 1) === ']') && (tabLatexSol[2].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            stor.laRepLatex = tabLatexSol[0] + '\\cup' + tabLatexSol[1].substring(0, posPointVirgulelatex[1]) + ';' + tabLatexSol[2].substring(posPointVirgulelatex[2] + 1, tabLatexSol[2].length)
          }
        } else if (tabLatexSol.length === 4) {
          for (i = 0; i <= 3; i++) {
            posPointVirgulelatex[i] = tabLatexSol[i].indexOf(';')
            borneSupInterlatex[i] = tabLatexSol[i].substring(posPointVirgulelatex[i] + 1, tabLatexSol[i].length - 1)
            borneInfInterlatex[i] = tabLatexSol[i].substring(1, posPointVirgulelatex[i])
          }
          me.logIfDebug('borneSupInterlatex:' + borneSupInterlatex + '   borneInfInterlatex:' + borneInfInterlatex)
          if ((borneSupInterlatex[0] === borneInfInterlatex[1]) && ((tabLatexSol[0].charAt(tabLatexSol[0].length - 1) === ']') && (tabLatexSol[1].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            stor.laRepLatex = tabLatexSol[0].substring(0, posPointVirgulelatex[0]) + ';' + tabLatexSol[1].substring(posPointVirgulelatex[1] + 1, tabLatexSol[1].length) + '\\cup' + tabLatexSol[2] + '\\cup' + tabLatexSol[3]
          }
          if ((borneSupInterlatex[1] === borneInfInterlatex[2]) && ((tabLatexSol[1].charAt(tabLatexSol[1].length - 1) === ']') && (tabLatexSol[2].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            stor.laRepLatex = tabLatexSol[0] + '\\cup' + tabLatexSol[1].substring(0, posPointVirgulelatex[1]) + ';' + tabLatexSol[2].substring(posPointVirgulelatex[2] + 1, tabLatexSol[2].length) + '\\cup' + tabLatexSol[3]
          }
          if ((borneSupInterlatex[2] === borneInfInterlatex[3]) && ((tabLatexSol[2].charAt(tabLatexSol[2].length - 1) === ']') && (tabLatexSol[3].charAt(0) === '['))) {
            // dans ce cas, on peut regrouper la réunion en un seul intervalle
            stor.laRepLatex = tabLatexSol[0] + '\\cup' + tabLatexSol[1] + '\\cup' + tabLatexSol[2].substring(0, posPointVirgulelatex[2]) + ';' + tabLatexSol[3].substring(posPointVirgulelatex[3] + 1, tabLatexSol[3].length)
          }
        }
      }
      j3pFocus(stor.zoneInput[0])
      me.logIfDebug('ensemble solution au format latex : ' + stor.laRepLatex)
      // Obligatoire
    }
    stor.explications = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '10px' }) })
    me.finEnonce()
  }
  let i, j, k, facteursTousAffines, stockage25kTab, expressLn
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.construitStructurePage('presentation1bis')
        me.stockage = [0, 0, 0, 0]
        if (!ds.signe_seulement) {
          // par défaut c'est 1, et c'est pas paramétrable donc faut passer par surcharge pour le modifier-
          me.surcharge({ nbetapes: 2 })
        }
        // Pour les paramètres de type array, ils peuvent être mal récupérés quand le graphe est construit à partir de l’éditeur qui va renoyer des strings et non des array
        const tabArrayParam = ['type_expres', 'imposer_fct', 'zero_entier', 'facteurs_affines', 'zeros_facteurs_nonaffines', 'domaine_def']
        tabArrayParam.forEach(tab => {
          if (typeof ds[tab] === 'string') {
            ds[tab] = JSON.parse(ds[tab])
          }
        })
        // on vérifie si tous les facteurs sont bien affines :
        facteursTousAffines = true
        for (let nbFact = 0; nbFact < ds.nbfacteurs; nbFact++) {
          if (ds.facteurs_affines[nbFact]) {
            facteursTousAffines = (facteursTousAffines && ds.facteurs_affines[nbFact])
          }
        }
        if (ds.type_expres[0] === 'produit') {
          if (facteursTousAffines) {
            me.afficheTitre(textes.titre_exo1)
          } else {
            me.afficheTitre(textes.titre_exo1bis)
          }
        } else {
          if (facteursTousAffines) {
            me.afficheTitre(textes.titre_exo2)
          } else {
            me.afficheTitre(textes.titre_exo2bis)
          }
        }
        for (let i = 0; i < ds.nbfacteurs; i++) {
          if (ds.imposer_fct[i] === undefined) ds.imposer_fct[i] = ''
        }
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()
      /// //////////////////////////////////////
      /* FIN DU CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////////
      break // case "enonce":

    case 'correction':
      {
        let reponseEleve, reponseIndex, aRepondu, zoneSaisieComplete, reponseEleve1, testIntervalle, bonneRep, bonneRepZeros
        let bonneLigne, bonneListe, cbon, inequationContraire, pbCrochets, valeursInversees, signesAffineOK
        if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
        // première question
        // zones contenant les zéros des fonctions :
          const solutionEleve = []
          for (k = 0; k < stor.tabSolutionOrdonnee.length; k++) {
            solutionEleve[k] = j3pValeurde(stor.inputZoneNulle[k])
            // gérer les cas pénibles : 3\frac{1}{2} ou \frac{1}{2}3
            // pour ceux là, xcas ne comprends pas, cela crée un erreur par la suite donc il faut ajouter la multiplication
            i = 2
            while (i < solutionEleve[k].length - 1) {
              if ((solutionEleve[k].charAt(i) === 'f') && estChiffre(solutionEleve[k].charAt(i - 2))) {
                solutionEleve[k] = solutionEleve[k].substring(0, i - 1) + '*' + solutionEleve[k].substring(i - 1)
                i += 1
              }
              i += 1
            }
            while (i < solutionEleve[k].length - 1) {
              if ((solutionEleve[k].charAt(i) === '}') && (estChiffre(solutionEleve[k].charAt(i + 1)))) {
                solutionEleve[k] = solutionEleve[k].substring(0, i + 1) + '*' + solutionEleve[k].substring(i + 1)
              }
              i += 1
            }
          }
          me.logIfDebug('reponses des eleve' + solutionEleve)
          reponseEleve = []
          for (k = 0; k < stor.tabSolutionOrdonnee.length; k++) {
            reponseEleve[k + 1] = j3pMathquillXcas(solutionEleve[k])
          // le décalage d’indice est dû à une première version de l’algo que je n’ai pas eu envie de modifier'
          }
          me.logIfDebug('reponses eleve:' + reponseEleve)
          for (i = 1; i <= (stor.tabSolutionOrdonnee.length + 1) * (stor.expressAffine.length + 1); i++) {
          // zones contenant les signes sur les lignes des fonctions affines// i va de 1 à (tabSolEqText.length+1)*nbExpress_affine
          // zones contenant les signes du produit// i va de (tabSolEqText.length+1)*nbExpress_affine+1 à (tabSolEqText.length+1)*(nbExpress_affine+1)
            reponseEleve[i + stor.tabSolutionOrdonnee.length] = j3pValeurde(stor.inputZoneSigne[i - 1])
          }
          me.logIfDebug('reponseEleve:' + reponseEleve)
          reponseIndex = []
          // listes pour le signe du produit // i va de 1 à tabSolEqText.length
          //  i va de tabSolEqText.length à (tabSolEqText.length+1)*nbExpress_affine
          for (i = 1; i <= stor.tabSolutionOrdonnee.length * (stor.expressAffine.length + 1); i++) reponseIndex[i] = stor.inputSelect[i - 1].selectedIndex
          aRepondu = true
          zoneSaisieComplete = true
          for (i = stor.tabSolutionText.length + (stor.tabSolutionText.length + 1) * (stor.expressAffine.length + 1); i >= 1; i--) {
            if (reponseEleve[i] === '') {
              aRepondu = false
              zoneSaisieComplete = false
              if (i <= stor.tabSolutionText.length) {
                j3pFocus(stor.inputZoneNulle[i - 1])
              } else {
                j3pFocus(stor.inputZoneSigne[i - stor.tabSolutionText.length - 1])
              }
              zoneSaisieComplete = false
            }
          }
        }
        if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 2)) {
        // deuxième question
          reponseEleve1 = j3pValeurde(stor.zoneInput[0])

          // ce qui est écrit n’est pas un intervalle ou une réunion d’intervalles ou le séparateur de l’intervalle n’est asp ; mais ,'
          testIntervalle = bonIntervalle(reponseEleve1)
          // testIntervalle a alors deux propriétés : non_intervalle et separateurVirgule
          if (reponseEleve1.indexOf(':') > -1) {
            const partition = reponseEleve1.split(':')
            reponseEleve1 = ''
            for (i = 0; i < partition.length; i++) {
              reponseEleve1 += partition[i]
            }
          }
          me.logIfDebug('valeur avant modification virgule-point : ' + reponseEleve1)
          // je convertis les virgules de la réponse en points comme séparateur des décimaux
          const reponseEleve1Point = reponseEleve1.split(',')
          reponseEleve1 = reponseEleve1Point[0]
          if (reponseEleve1Point.length > 1) {
            for (let l = 1; l < reponseEleve1Point.length; l++) {
              reponseEleve1 += '.' + reponseEleve1Point[l]
            }
          }
          me.logIfDebug('valeur : ' + reponseEleve1)
          aRepondu = (reponseEleve1 !== '')
        }
        if (!aRepondu && !me.isElapsed) {
          let msgReponseManquante
          if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 2)) {
            j3pFocus(stor.zoneInput[0])
          } else {
            if (zoneSaisieComplete) {
            // dans ce cas toutes les zones de saisie sont complètes, par contre une liste ne l’est pas'
              msgReponseManquante = textes.comment1bis
            } else {
              msgReponseManquante = textes.comment1
            }
          }
          me.reponseManquante(stor.explications, msgReponseManquante)
          return me.finCorrection()
        } else { // une réponse a été saisie
          if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
          // pour la première question
            bonneRep = []// pour toutes les zones de saisie
            bonneRepZeros = true// pour savoir si tous les zeros sont bons
            // on vérifie d’abord les zéros' bonneRep[i]
            // il faut gérer le type de réponse
            // je peux très bien avoir plusieurs virgules dans mes zéros
            const arbreDif = []
            let madif
            for (i = 1; i <= stor.tabSolutionOrdonnee.length; i++) {
              while (reponseEleve[i].indexOf(',') !== -1) {
                reponseEleve[i] = reponseEleve[i].substring(0, reponseEleve[i].indexOf(',')) + '.' + reponseEleve[i].substring(reponseEleve[i].indexOf(',') + 1)
              }
              // j’ai un pb si e est tout seul (sans exp ou ^)'
              let iExp = 0
              while (iExp < reponseEleve[i].length) {
                if ((reponseEleve[i][iExp] === 'e') && ((reponseEleve[i][iExp + 1] === '+') || (reponseEleve[i][iExp + 1] === '-') || (iExp === reponseEleve[i].length - 1))) {
                  reponseEleve[i] = reponseEleve[i].substring(0, iExp - 1) + 'exp(1)' + reponseEleve[i].substring(iExp + 1, reponseEleve[i].length)
                  iExp += 5
                } else {
                  iExp++
                }
              }
              if (reponseEleve[i].indexOf('e^') > -1) {
              // l’expoentielle est présente donc je dois la modifier'
                reponseEleve[i] = reponseEleve[i].replace('e^', 'exp')
              }
              arbreDif[i] = new Tarbre(reponseEleve[i] + '-(' + stor.tabSolutionOrdonnee[i - 1] + ')', ['x'])
              madif = arbreDif[i].evalue([0])
              me.logIfDebug('madif:' + madif)
              bonneRep[i] = (Math.abs(madif) < Math.pow(10, -13))
              bonneRepZeros = (bonneRepZeros && bonneRep[i])
            }
            me.logIfDebug('bonneRep des zero :' + bonneRepZeros + '  ' + bonneRep)
            // on crée le tableau accueillant les signes réponse (dans l’ordre)'
            me.logIfDebug('stor.tabSigneRep:' + stor.tabSigneRep)
            // je contrôle chaque ligne
            bonneLigne = []
            for (i = 1; i <= stor.tabSolutionText.length + 1; i++) {
              bonneLigne[i] = true
              for (j = 1; j <= stor.tabSolutionOrdonnee.length + 1; j++) {
                bonneRep[(i - 1) * (stor.tabSolutionOrdonnee.length + 1) + j + stor.tabSolutionOrdonnee.length] = (reponseEleve[(i - 1) * (stor.tabSolutionOrdonnee.length + 1) + j + stor.tabSolutionOrdonnee.length] === stor.tabSigneRep[(i - 1) * (stor.tabSolutionOrdonnee.length + 1) + j])
                bonneLigne[i] = (bonneLigne[i] && bonneRep[(i - 1) * (stor.tabSolutionOrdonnee.length + 1) + j + stor.tabSolutionOrdonnee.length])
              }
            }
            me.logIfDebug('bonneRep', bonneRep, '\nbonneLigne', bonneLigne, '\nreponseIndex', reponseIndex)
            bonneListe = []// pour toutes les listes
            // lignes des signes des fonctions affines
            for (i = 1; i <= stor.tabSolutionText.length; i++) {
              for (j = 1; j <= stor.tabSolutionOrdonnee.length; j++) {
                if (ds.facteurs_affines[i - 1]) {
                // si la fonction est affine
                  if (stor.tabSigneRep[(i - 1) * (stor.tabSolutionOrdonnee.length + 1) + j] === stor.tabSigneRep[(i - 1) * (stor.tabSolutionOrdonnee.length + 1) + j + 1]) {
                  // pas de changement de signe, donc dans la liste, on doit avoir |
                    bonneListe[i * stor.tabSolutionOrdonnee.length + j] = (reponseIndex[i * stor.tabSolutionOrdonnee.length + j] === 0)
                  } else {
                  // changement de signe, donc dans la liste, on doit avoir 0
                    bonneListe[i * stor.tabSolutionOrdonnee.length + j] = (reponseIndex[i * stor.tabSolutionOrdonnee.length + j] === 1)
                  }
                } else {
                  if (stor.solEq[i - 1].indexOf(stor.tabSolutionOrdonnee[j - 1]) > -1) {
                  // en gros si le zéro de la j-ème colonne est dans la liste des zéros du facteur de la i-ème ligne
                  // alors on doit voir un zéro
                    bonneListe[i * stor.tabSolutionOrdonnee.length + j] = (reponseIndex[i * stor.tabSolutionOrdonnee.length + j] === 1)
                  } else {
                  // sinon on doit voir une barre verticale
                    bonneListe[i * stor.tabSolutionOrdonnee.length + j] = (reponseIndex[i * stor.tabSolutionOrdonnee.length + j] === 0)
                  }
                }
              }
            }
            // listes de la dernière ligne
            if (ds.type_expres[0] === 'produit') {
            // chaque liste doit contenir 0;
              for (i = 1; i <= stor.tabSolutionOrdonnee.length; i++) {
                bonneListe[i] = (reponseIndex[i] === 1)
              }
            } else {
            // chaque liste doit contenir 0 sauf si la valeur est interdite
              for (i = 1; i <= stor.tabSolutionOrdonnee.length; i++) {
                let posTab = -1
                k = stor.tabSolutionText.length - 1
                do {
                // je regarde si stor.tabSolutionOrdonnee[i-1] est l’une des valeurs de stor.tabSolutionText[k];
                  if (String(stor.tabSolutionText[k]).indexOf('|') === -1) {
                  // stor.tabSolutionText[k] ne contient qu’une seule valeur'
                    if (Math.abs(calculQuot(stor.tabSolutionOrdonnee[i - 1]) - calculQuot(stor.tabSolutionText[k])) < Math.pow(10, -12)) posTab = k
                  } else {
                    stockage25kTab = stor.tabSolutionText[k].split('|')
                    for (let k3 = 0; k3 < stockage25kTab.length; k3++) {
                      if (Math.abs(calculQuot(stor.tabSolutionOrdonnee[i - 1]) - calculQuot(stockage25kTab[k3])) < Math.pow(10, -12)) posTab = k
                    }
                  }
                  k--
                // en principe k ne doit pas descendre en dessous de 0
                } while ((posTab < 0) && (k >= 0))
                // la ligne suivante a été remplacée pour gérer les facteurs avec plusieurs zéros
                if (ds.type_expres[posTab + 1] === 'num') bonneListe[i] = (reponseIndex[i] === 2)// c’est-à-dire 0'
                else bonneListe[i] = (reponseIndex[i] === 1)// c’est-à-dire ||
              }
            }
            me.logIfDebug('bonneListe:' + bonneListe)
            cbon = true
            for (i = 1; i <= stor.tabSolutionOrdonnee.length + (stor.tabSolutionOrdonnee.length + 1) * (stor.expressAffine.length + 1); i++) {
              cbon = (cbon && bonneRep[i])
            }
            for (i = 1; i <= stor.tabSolutionOrdonnee.length * (stor.expressAffine.length + 1); i++) {
              cbon = (cbon && bonneListe[i])
            }
          }
          if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 2)) {
          // reponseEleve1 gestion des fraction qu’il va falloir convertir'
            while (reponseEleve1.indexOf('frac') > -1) {
              let numTxt = ''
              let denTxt = ''
              // frac est présent dans la réponse
              let index = reponseEleve1.indexOf('frac') + 5
              // index est au début l’index de l’accollade {'
              while (reponseEleve1.charAt(index) !== '}') {
                numTxt += reponseEleve1.charAt(index)
                index += 1
              }
              // nous sommes arrivé à la deuxième accollade {
              index += 2
              while (reponseEleve1.charAt(index) !== '}') {
                denTxt += reponseEleve1.charAt(index)
                index += 1
              }
              // on remplace \\frac{..}{..} par j3pArrondi(calculQuot(../..),10)
              reponseEleve1 = j3pRemplace(reponseEleve1, reponseEleve1.indexOf('frac') - 1, index, String(j3pArrondi(calculQuot(numTxt + '/' + denTxt), 10)))
            }
            const rechercheExp = /e\^\{[0-9,.-]+}|e\^[0-9]/ig // new RegExp('e\\^\\{[0-9.,\\-]{1,}\\}|e\\^[0-9]', 'ig')
            while (rechercheExp.test(reponseEleve1)) {
              const expressExp = reponseEleve1.match(rechercheExp)[0]
              const puissExp = (expressExp.indexOf('e^{') > -1)
                ? expressExp.substring(3, expressExp.length - 1)
                : expressExp.substring(2, expressExp.length)
              reponseEleve1 = reponseEleve1.replace(expressExp, 'exp(' + puissExp + ')')
            }
            // On vire les left et right devant des parenthèses
            reponseEleve1 = reponseEleve1.replace(/\\left\(/g, '(')
            reponseEleve1 = reponseEleve1.replace(/\\right\)/g, ')')
            reponseEleve1 = reponseEleve1.replace(/\\ln/g, 'ln')
            const rechercheRacine = /\\sqrt\{[0-9,.-]+}/ig // new RegExp('\\\\sqrt\\{[0-9.,\\-]{1,}\\}', 'ig')
            while (rechercheRacine.test(reponseEleve1)) {
              const expressRacine = reponseEleve1.match(rechercheRacine)[0]
              const radicalRacine = expressRacine.substring(6, expressRacine.length - 1)
              reponseEleve1 = reponseEleve1.replace(expressRacine, 'racine(' + radicalRacine + ')')
            }

            me.logIfDebug('new_reponseEleve1  ' + reponseEleve1)
            // le pb c’est qu’il faut maintenant convertir tous mes nombres avec exp, ln ou racine avec des valeurs arrondies à 10^{-10} près'
            const tabInterRep1 = reponseEleve1.split('\\cup')
            let newRep1Eleve = ''
            let borneRep1Inf, borneRep1Sup
            let crochetRep1Inf, crochetRep1Sup
            for (k = 0; k < tabInterRep1.length; k++) {
              borneRep1Inf = tabInterRep1[k].split(';')[0]
              crochetRep1Inf = ''
              if (borneRep1Inf.indexOf('infty') === -1) {
                if ((borneRep1Inf.charAt(0) === ']') || (borneRep1Inf.charAt(0) === '[')) {
                  crochetRep1Inf = borneRep1Inf.charAt(0)
                  borneRep1Inf = borneRep1Inf.substring(1, borneRep1Inf.length)
                }
                borneRep1Inf = String(j3pArrondi(calculQuot(borneRep1Inf), 10))
              }
              borneRep1Sup = tabInterRep1[k].split(';')[1]
              if (!borneRep1Sup) borneRep1Sup = ''
              crochetRep1Sup = ''
              if (borneRep1Sup.indexOf('infty') === -1) {
                if ((borneRep1Sup.charAt(borneRep1Sup.length - 1) === ']') || (borneRep1Sup.charAt(borneRep1Sup.length - 1) === '[')) {
                  crochetRep1Sup = borneRep1Sup.charAt(borneRep1Sup.length - 1)
                  borneRep1Sup = borneRep1Sup.substring(0, borneRep1Sup.length - 1)
                }
                if (borneRep1Sup !== '') {
                  borneRep1Sup = String(j3pArrondi(calculQuot(borneRep1Sup), 10))
                }
              }
              if (k === 0) {
                newRep1Eleve += crochetRep1Inf + borneRep1Inf + ';' + borneRep1Sup + crochetRep1Sup
              } else {
                newRep1Eleve += '\\cup' + crochetRep1Inf + borneRep1Inf + ';' + borneRep1Sup + crochetRep1Sup
              }
            }
            reponseEleve1 = newRep1Eleve
            me.logIfDebug('new_reponseEleve1 après modif sur exp, racine et ln  ' + reponseEleve1)
            cbon = false
            for (i = 0; i < stor.unionPossible.length; i++) {
              if (reponseEleve1 === stor.unionPossible[i]) {
                cbon = true
              }
            }
            inequationContraire = false
            for (i = 0; i < stor.unionContraire.length; i++) {
              if (reponseEleve1 === stor.unionContraire[i]) {
                inequationContraire = true
              }
            }
            pbCrochets = true
            for (i = 0; i < stor.tabIntervalleSol.length; i++) {
              pbCrochets = (pbCrochets && (reponseEleve1.indexOf(stor.tabIntervalleSol[i]) !== -1))
            }
          }
          if (cbon) {
            me.score++
            stor.explications.style.color = me.styles.cbien
            stor.explications.innerHTML = cBien
            if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
              if (stor.affichagePalette) j3pDetruit(stor.laPalette)
              for (i = 1; i <= stor.tabSolutionOrdonnee.length; i++) {
                j3pStyle(stor.inputZoneNulle[i - 1], { color: me.styles.cbien })
                j3pDesactive(stor.inputZoneNulle[i - 1])
                j3pFreezeElt(stor.inputZoneNulle[i - 1])
              }
              for (i = 1; i <= (stor.tabSolutionOrdonnee.length + 1) * (stor.tabSolutionText.length + 1); i++) {
                j3pStyle(stor.inputZoneSigne[i - 1], { color: me.styles.cbien })
                j3pDesactive(stor.inputZoneSigne[i - 1])
                j3pFreezeElt(stor.inputZoneSigne[i - 1])
              }
              for (i = 1; i <= stor.tabSolutionOrdonnee.length * (stor.tabSolutionText.length + 1); i++) {
                stor.inputSelect[i - 1].disabled = true
                j3pStyle(stor.inputSelect[i - 1], { color: me.styles.cbien })
              }
              me.typederreurs[0]++// le tableau est correct
            }
            if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 2)) {
              j3pDetruit(stor.laPalette)
              // on désactive la zone de saisie
              j3pStyle(stor.zoneInput[0], { color: choixStyle(true) })
              j3pDesactive(stor.zoneInput[0])
              j3pFreezeElt(stor.zoneInput[0])
              // on affiche l’explication'
              afficheCorrection(true)
              me.typederreurs[1]++// bon ensemble des solutions
            }
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.explications.style.color = me.styles.cfaux
            // recherche de la nature de l’erreur'
            if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
            // on vérifie si les premières zones de saisie ont été inversées
            // je vérifie en fait si tous les zéros des fonctions affines sont présents
              const zeroPresent = []
              let tousLesZeros = true
              const arbreDif2 = []
              let madif2
              for (i = 0; i < stor.tabSolutionOrdonnee.length; i++) {
                zeroPresent[i] = false
                for (k = 0; k < stor.tabSolutionOrdonnee.length; k++) {
                  arbreDif2[i] = new Tarbre(reponseEleve[i + 1] + '-(' + stor.tabSolutionOrdonnee[k] + ')', ['x'])
                  madif2 = arbreDif2[i].evalue([0])
                  me.logIfDebug('madif2:' + madif2)
                  if (Math.abs(madif2) < Math.pow(10, -12)) {
                    zeroPresent[i] = true
                  }
                }
                tousLesZeros = (tousLesZeros && zeroPresent[i])
              }
              valeursInversees = (tousLesZeros)
              me.logIfDebug('valeursInversees:' + valeursInversees + '  zeroPresent:' + zeroPresent)
              // les zéros sont bons mais le signe de l’une des fonctions affine est faux'
              // c’est dans bonneLigne[i] pour i allant de 1 à stor.tabSolutionText.lentgh
              // on ajoute cependant les pbs éventuels de 0 (tableau bonneListe)
              signesAffineOK = true
              const bonSigneAffine = []
              for (i = 1; i <= stor.tabSolutionText.length; i++) {
                bonSigneAffine[i] = bonneLigne[i]
                for (j = 1; j <= stor.tabSolutionOrdonnee.length; j++) {
                  bonSigneAffine[i] = (bonSigneAffine[i] && bonneListe[(i - 1) * stor.tabSolutionOrdonnee.length + j + stor.tabSolutionOrdonnee.length])
                }
                signesAffineOK = (signesAffineOK && bonSigneAffine[i])
              }
              me.logIfDebug('bonSigneAffine:' + bonSigneAffine + '   signesAffineOK:' + signesAffineOK)
              // pb avec le signe du produit
              let bonSigneProduit = bonneLigne[stor.tabSolutionText.length + 1]
              for (i = 1; i <= stor.tabSolutionText.length; i++) {
                bonSigneProduit = (bonSigneProduit && bonneListe[i])
              }
              // mise en rouge des zones et listes fausses
              if (!bonneRepZeros) {
              // on met la (ou les) zone(s) de zéro en rouge ainsi que tous les signes et les listes
                for (i = 1; i <= stor.tabSolutionOrdonnee.length; i++) {
                  if (!bonneRep[i]) j3pStyle(stor.inputZoneNulle[i - 1], { color: me.styles.cfaux })
                  else j3pStyle(stor.inputZoneNulle[i - 1], { color: me.styles.cbien })
                }
                for (i = 1; i <= (stor.tabSolutionOrdonnee.length + 1) * (stor.tabSolutionText.length + 1); i++) {
                  j3pStyle(stor.inputZoneSigne[i - 1], { color: me.styles.cfaux })
                }
                for (i = 1; i <= stor.tabSolutionOrdonnee.length * (stor.tabSolutionText.length + 1); i++) {
                  j3pStyle(stor.inputSelect[i - 1], { color: me.styles.cfaux })
                }
              } else {
              // les premières zones sont bonnes donc on les remet en noir
                for (i = 1; i <= stor.tabSolutionOrdonnee.length; i++) {
                  j3pDesactive(stor.inputZoneNulle[i - 1])
                  j3pStyle(stor.inputZoneNulle[i - 1], { color: choixStyle(true) })
                  j3pFreezeElt(stor.inputZoneNulle[i - 1])
                }
                if (!signesAffineOK) {
                // si le pb vient du signe d’une fonction affine'
                  for (i = 1; i <= (stor.tabSolutionOrdonnee.length + 1) * (stor.tabSolutionText.length + 1); i++) {
                    j3pStyle(stor.inputZoneSigne[i - 1], { color: me.styles.cfaux })
                  }
                  for (i = 1; i <= stor.tabSolutionOrdonnee.length * (stor.tabSolutionText.length + 1); i++) {
                    j3pStyle(stor.inputSelect[i - 1], { color: me.styles.cfaux })
                  }
                } else {
                // les signes des fonctions affines sont corrects, on remet donc tout en noir
                  for (i = 1; i <= (stor.tabSolutionOrdonnee.length + 1) * (stor.tabSolutionText.length); i++) {
                    j3pStyle(stor.inputZoneSigne[i - 1], { color: me.styles.cbien })
                  }
                  for (i = stor.tabSolutionOrdonnee.length + 1; i <= stor.tabSolutionOrdonnee.length * (stor.tabSolutionText.length + 1); i++) {
                    j3pStyle(stor.inputSelect[i - 1], { color: me.styles.cbien })
                  }
                  if (!bonSigneProduit) {
                  // il y a un pb sur le signe du produit
                    for (i = (stor.tabSolutionOrdonnee.length + 1) * (stor.tabSolutionText.length) + 1; i <= (stor.tabSolutionOrdonnee.length + 1) * (stor.tabSolutionText.length + 1); i++) {
                      j3pStyle(stor.inputZoneSigne[i - 1], { color: me.styles.cfaux })
                    }
                    for (i = 1; i <= stor.tabSolutionOrdonnee.length; i++) {
                      j3pStyle(stor.inputSelect[i - 1], { color: me.styles.cfaux })
                    }
                  }// pas de sinon
                // car dans l’autre cas, tout est bon
                // donc ce n’est pas possible car on sait qu’il y a au moins une erreur ici'
                }
              }
            }
            if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 2)) {
              j3pStyle(stor.zoneInput[0], { color: me.styles.cfaux })
            }
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.explications.innerHTML = tempsDepasse
              me.typederreurs[10]++
              /*
                        *   RECOPIER LA CORRECTION ICI !
                        */
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // réponse fausse :
              stor.explications.innerHTML = cFaux
              if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
                if (!bonneRep[1] || !bonneRep[2]) {
                  if (!valeursInversees) {
                  // les zéros des fonctions affines ne sont pas corrects mais ce n’est pas une inversion de l’ordre des valeurs'
                    stor.explications.innerHTML += '<br>' + textes.comment2
                    for (i = 1; i <= stor.tabSolutionOrdonnee.length; i++) {
                      if (bonneRep[i]) {
                        j3pStyle(stor.inputZoneNulle[i - 1], { color: me.styles.cbien })
                        j3pDesactive(stor.inputZoneNulle[i - 1])
                        j3pFreezeElt(stor.inputZoneNulle[i - 1])
                      }
                    }
                    me.typederreurs[3]++
                  } else {
                    me.typederreurs[2]++
                    stor.explications.innerHTML += '<br>' + textes.comment3
                  }
                } else {
                // les zéros des fonctions affines sont les bons et sont dans le bon ordre
                  if (!signesAffineOK) {
                  // si le pb vient du signe d’une fonction affine'
                    stor.explications.innerHTML += '<br>' + textes.comment4
                    me.typederreurs[4]++
                  } else {
                  // sinon le pb vient du signe du produit ou du quotient
                    me.typederreurs[5]++
                    if (ds.type_expres[0] === 'produit') {
                      stor.explications.innerHTML += '<br>' + textes.comment5
                    } else {
                      stor.explications.innerHTML += '<br>' + textes.comment6
                    }
                  }
                }
              }
              if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 2)) {
                if (testIntervalle.non_intervalle) {
                  stor.explications.innerHTML += '<br>' + textes.comment9
                } else if (testIntervalle.separateurVirgule) {
                  stor.explications.innerHTML += '<br>' + textes.comment10
                } else if (inequationContraire) {
                  me.typederreurs[6]++
                  stor.explications.innerHTML += '<br>' + textes.comment7
                } else if (pbCrochets) {
                  me.typederreurs[7]++
                  stor.explications.innerHTML += '<br>' + textes.comment8
                }
              }
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.explications.innerHTML += '<br>' + essaieEncore
                // on redonne le focus
                if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
                  for (i = stor.tabSolutionOrdonnee.length + (stor.tabSolutionOrdonnee.length + 1) * (stor.expressAffine.length + 1); i >= 1; i--) {
                    if (!bonneRep[i]) {
                      if (i <= stor.tabSolutionOrdonnee.length) j3pFocus(stor.inputZoneNulle[i - 1])
                      else j3pFocus(stor.inputZoneSigne[i - stor.tabSolutionOrdonnee.length - 1])
                    }
                  }
                }
                if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 2)) j3pFocus(stor.zoneInput[0])
                me.finCorrection()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
                // on bloque les zones
                  for (i = 1; i <= stor.tabSolutionOrdonnee.length; i++) {
                    j3pStyle(stor.inputZoneNulle[i - 1], { color: choixStyle(bonneRep[i]) })
                    j3pDesactive(stor.inputZoneNulle[i - 1])
                    if (bonneRep[i]) j3pFreezeElt(stor.inputZoneNulle[i - 1])
                  }
                  for (i = 1; i <= (stor.tabSolutionOrdonnee.length + 1) * (stor.tabSolutionText.length + 1); i++) {
                    j3pStyle(stor.inputZoneSigne[i - 1], { color: choixStyle(bonneRep[i + stor.tabSolutionOrdonnee.length]) })
                    j3pDesactive(stor.inputZoneSigne[i - 1])
                    if (bonneRep[i + stor.tabSolutionOrdonnee.length]) j3pFreezeElt(stor.inputZoneSigne[i - 1])
                  }
                  for (i = 1; i <= stor.tabSolutionOrdonnee.length * (stor.tabSolutionText.length + 1); i++) stor.inputSelect[i - 1].disabled = true
                  for (i = 1; i <= stor.tabSolutionOrdonnee.length + (stor.tabSolutionOrdonnee.length + 1) * (stor.tabSolutionText.length + 1); i++) {
                    if (!bonneRep[i]) {
                      if (i <= stor.tabSolutionOrdonnee.length) j3pBarre(stor.inputZoneNulle[i - 1])
                      else j3pBarre(stor.inputZoneSigne[i - stor.tabSolutionOrdonnee.length - 1])
                    }
                  }
                  for (i = 1; i <= stor.tabSolutionOrdonnee.length * (stor.tabSolutionText.length + 1); i++) {
                    if (!bonneListe[i]) j3pBarre(stor.inputSelect[i - 1])
                    else j3pStyle(stor.inputSelect[i - 1], { color: me.styles.cbien })
                  }
                  // on cache l’éventuelle remarque :'
                  j3pDetruit(stor.remarque)
                  if (stor.affichagePalette) j3pDetruit(stor.laPalette)
                }
                if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 2)) {
                  j3pDesactive(stor.zoneInput[0])
                  j3pStyle(stor.zoneInput[0], { color: choixStyle(cbon) })
                  j3pBarre(stor.zoneInput[0])
                  j3pDetruit(stor.laPalette)
                }
                stor.explications.innerHTML += '<br>' + regardeCorrection
                // affichage de l’explication
                afficheCorrection(false)
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          if (ds.nbetapes === 2) {
            // dans ce cas on a aussi l’inéquation'
            if (me.typederreurs[4] / ds.nbrepetitions >= ds.seuilerreur / 2) {
              // pb de zéro des fonctions affines
              me.parcours.pe = ds.pe_2
            } else if (me.typederreurs[3] / ds.nbrepetitions >= ds.seuilerreur / 2) {
              // pb sur le signe d’une fonction affine (pas vraiment sur le zéro)
              me.parcours.pe = ds.pe_3
            } else if (me.typederreurs[2] / ds.nbrepetitions >= ds.seuilerreur / 2) {
              // valeurs inversées
              me.parcours.pe = ds.pe_4
            } else if (me.typederreurs[5] / ds.nbrepetitions >= ds.seuilerreur / 2) {
              // pb sur le signe du produit ou du quotient
              if (ds.type_expres[0] === 'produit') {
                me.parcours.pe = ds.pe_5
              } else {
                me.parcours.pe = ds.pe_6
              }
            } else if (me.typederreurs[6] / ds.nbrepetitions >= ds.seuilerreur / 2) {
              // pb sur le sens de l’inégalité'
              me.parcours.pe = ds.pe_8
            } else if (me.typederreurs[7] / ds.nbrepetitions >= ds.seuilerreur / 2) {
              // pb de crochets
              me.parcours.pe = ds.pe_4
            } else {
              me.parcours.pe = ds.pe_9
            }
          } else {
            // on ne demande que le tableau de signes
            if (me.typederreurs[4] / ds.nbrepetitions >= ds.seuilerreur) {
              // pb de zéro des fonctions affines
              me.parcours.pe = ds.pe_2
            } else if (me.typederreurs[3] / ds.nbrepetitions >= ds.seuilerreur) {
              // pb sur le signe d’une fonction affine (pas vraiment sur le zéro)
              me.parcours.pe = ds.pe_3
            } else if (me.typederreurs[2] / ds.nbrepetitions >= ds.seuilerreur) {
              // valeurs inversées
              me.parcours.pe = ds.pe_4
            } else if (me.typederreurs[5] / ds.nbrepetitions >= ds.seuilerreur) {
              // pb sur le signe du produit ou du quotient
              if (ds.type_expres[0] === 'produit') {
                me.parcours.pe = ds.pe_5
              } else {
                me.parcours.pe = ds.pe_6
              }
            } else {
              me.parcours.pe = ds.pe_9
            }
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
}
