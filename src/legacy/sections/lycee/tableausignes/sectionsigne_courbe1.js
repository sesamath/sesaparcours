import { j3pNombre, j3pDetruit, j3pDiv, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pMin, j3pPaletteMathquill, j3pRandomTab, j3pStyle, j3pAddElt, j3pGetNewId, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import { construireTableauSignes } from 'src/legacy/outils/fonctions/tableauSignes'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    juillet 2014
    À partir de la courbe représentative d’une fonction on demande de déterminer son signe

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['nbzero', 'alea', 'string', 'nombre de valeurs où s’annule la fonction : on peut préciser une valeur entre 0 et 3 ou bien alea'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.6, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'problème avec les zéros de la fonction' },
    { pe_3: 'problème avec le signe de la fonction' },
    { pe_4: 'insuffisant' }
  ]
}

const textes = {
  titre_exo: 'Signe d’une fonction à l’aide de la courbe',
  consigne1: 'Soit $£f$ une fonction définie sur $£i$ dont on donne la courbe représentative ci-dessous.',
  consigne2: 'Préciser la (ou les) valeur(s) en laquelle (ou lesquelles) s’annule la fonction $£f$ :',
  consigne3: '- s’il y en a plusieurs, on séparera les valeurs par un point-virgule;',
  consigne4: '- s’il n’y en a pas, on écrira l’ensemble vide (palette de bouton ci-dessous).',
  consigne5: 'La fonction $£f$ ne s’annule pas sur $\\R$.',
  consigne6: 'La fonction $£f$ s’annule en une valeur : $£v$.',
  consigne7: 'La fonction $£f$ s’annule en £n valeurs : $£v$.',
  consigne8: 'Complète alors le tableau de signes de la fonction $£f$ :',
  info1: "ATTENTION, tu n’as le droit qu'à un seul essai pour cette question !",
  comment1: 'Tu n’as pas le bon nombre de valeurs en lesquelles la fonction s’annule !',
  comment2: 'Il y a pourtant une ou plusieurs bonne(s) valeur(s) !',
  corr1: 'On regarde l’abscisse des points d’intersection de la courbe avec l’axe des abscisses.',
  cons_tab1: 'Signe <br>de $£f$',
  corr2: 'La fonction est négative lorsque la courbe est en-dessous de l’axe des abscisses et positive lorsque la courbe est au-dessus.'
}
/**
 * section signe_courbe1
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    if ((me.questionCourante % ds.nbetapes) === 1) j3pDetruit(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })

    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    const zoneExpliRep = j3pAddElt(stor.repAttendue, 'div', '', { style: { paddingTop: '5px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    if ((me.questionCourante % ds.nbetapes) === 1) {
      if (!bonneReponse) {
        if (stor.nbZero === 0) {
          j3pAffiche(zoneExpliRep, '', '$\\varnothing$')
        } else {
          const tabZeroOrdonne = stor.ensembleZeros.sort()
          let listeZero = String(tabZeroOrdonne[0])
          if (tabZeroOrdonne.length > 1) {
            for (let i = 1; i < tabZeroOrdonne.length; i++) listeZero += ' ; ' + tabZeroOrdonne[i]
          }
          j3pAffiche(zoneExpliRep, '', listeZero)
        }
      }
      j3pAffiche(stor.zoneExpli2, '', textes.corr1)
    } else {
      if (ds.nbchances > 1) j3pEmpty(stor.zoneCons5)
      if (!bonneReponse) {
        stor.leTableauRep = j3pAddElt(zoneExpliRep, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { position: 'relative' }) })
        stor.objTableauCorr = Object.assign({}, stor.objTableau)
        stor.objTableauCorr.eltsAffiches = [true, true, true]
        stor.objTableauCorr.styleTxt = me.styles.toutpetit.correction
        stor.tabSigne = construireTableauSignes(stor.leTableauRep, stor.objTableauCorr)
      }
      j3pAffiche(stor.zoneExpli1, '', textes.corr2)
    }
  }

  function enonceMain () {
    let nbZero
    let coefplusmoins1
    let fdexinit, fdex
    let a, b, c, d, e
    let tabSigneRep
    let ensembleZeros
    if ((me.questionCourante % ds.nbetapes) === 1) {
      // on définit la fonction
      const tabNomf = ['f', 'g', 'h']
      stor.nom_f = j3pRandomTab(tabNomf, [0.3333, 0.3333, 0.3334])
      let choixFct
      if ((j3pNombre(String(ds.nbzero)) < 0) || (j3pNombre(String(ds.nbzero)) > 3)) {
        ds.nbzero = 'alea'
      }
      if (ds.nbzero === 'alea') {
        let tabNbZero
        if (me.questionCourante === 1) {
          // on départ on initialise le tableau des différentes possibilités
          tabNbZero = [0, 1, 2, 3]
        } else {
          if (stor.tabNbZero.length === 0) {
            // on réinitialise le tableau des différentes possibilités
            tabNbZero = [0, 1, 2, 3]
          } else {
            tabNbZero = stor.tabNbZero
          }
        }
        const pioche = Math.floor(Math.random() * tabNbZero.length)
        nbZero = tabNbZero[pioche]
        me.logIfDebug('tabNbZero:' + tabNbZero + '   pioche' + pioche)
        tabNbZero.splice(pioche, 1)
        stor.tabNbZero = tabNbZero
      } else {
        nbZero = Number(ds.nbzero)
      }
      me.logIfDebug('nbZero:' + nbZero)
      coefplusmoins1 = j3pGetRandomInt(0, 1) * 2 - 1
      if (nbZero === 0) {
        choixFct = j3pGetRandomInt(1, 7)
        if (choixFct === 1) {
          b = -2
          c = -3
          d = 0.5 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
          a = 2.5 + j3pGetRandomInt(0, 2) * 0.5
        } else if (choixFct === 2) {
          b = -1
          c = -2
          d = 0.5 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
          a = 2 + j3pGetRandomInt(0, 2) * 0.5
        } else if (choixFct === 3) {
          b = 0
          c = -2
          d = 0.7 + j3pGetRandomInt(0, 5) * 0.1
          e = 0.3 + j3pGetRandomInt(0, 4) * 0.1
          a = 3.5 + j3pGetRandomInt(0, 2) * 0.5
        } else if (choixFct === 4) {
          b = 1
          c = -2
          d = 0.5 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
          a = 4 + j3pGetRandomInt(0, 2) * 0.5
        } else if (choixFct === 5) {
          b = 2
          c = 0
          d = 0.5 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
          a = 3 + j3pGetRandomInt(0, 2) * 0.5
        } else if (choixFct === 6) {
          b = 2
          c = 1
          d = 1 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
          a = 2.5 + j3pGetRandomInt(0, 2) * 0.5
        } else if (choixFct === 7) {
          b = 3
          c = 2
          d = 1 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
          a = 3 + j3pGetRandomInt(0, 2) * 0.5
        }
        fdexinit = '((' + d + ')*x^2+(' + e + '))*(x-(' + b + '))*(x-(' + c + '))+(' + a + ')'
        if (coefplusmoins1 === 1) {
          tabSigneRep = ['+']
        } else {
          tabSigneRep = ['-']
        }
        ensembleZeros = []
      } else if (nbZero === 1) {
        do {
          b = j3pGetRandomInt(-3, 3)
        } while (Math.abs(b) <= 1)
        d = 0.3 + j3pGetRandomInt(0, 5) * 0.1
        e = 0.1 + j3pGetRandomInt(0, 3) * 0.1
        fdexinit = '((' + d + ')*x^2+(' + e + '))*(x-(' + b + '))'
        if (coefplusmoins1 === 1) {
          tabSigneRep = ['-', '+']
        } else {
          tabSigneRep = ['+', '-']
        }
        ensembleZeros = [b]
      } else if (nbZero === 2) {
        choixFct = j3pGetRandomInt(1, 7)
        if (choixFct === 1) {
          b = -2
          c = -3
          d = 0.5 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
        } else if (choixFct === 2) {
          b = -1
          c = -2
          d = 0.5 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
        } else if (choixFct === 3) {
          b = 0
          c = -2
          d = 0.7 + j3pGetRandomInt(0, 5) * 0.1
          e = 0.3 + j3pGetRandomInt(0, 4) * 0.1
        } else if (choixFct === 4) {
          b = 1
          c = -2
          d = 0.5 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
        } else if (choixFct === 5) {
          b = 2
          c = 0
          d = 0.5 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
        } else if (choixFct === 6) {
          b = 2
          c = 1
          d = 1 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
        } else if (choixFct === 7) {
          b = 3
          c = 2
          d = 1 + j3pGetRandomInt(0, 3) * 0.1
          e = 0.1 + j3pGetRandomInt(0, 2) * 0.1
        }
        fdexinit = '((' + d + ')*x^2+(' + e + '))*(x-(' + b + '))*(x-(' + c + '))'
        ensembleZeros = [b, c]
        if (coefplusmoins1 === 1) {
          tabSigneRep = ['+', '-', '+']
        } else {
          tabSigneRep = ['-', '+', '-']
        }
      } else {
        let max, min, egalite
        do {
          a = j3pGetRandomInt(-4, 4)
          b = j3pGetRandomInt(-4, 4)
          c = j3pGetRandomInt(-4, 4)
          max = Math.max(Math.max(a, b), c)
          min = Math.min(Math.min(a, b), c)
          egalite = ((Math.abs(a - b) < Math.pow(10, -12)) || (Math.abs(a - c) < Math.pow(10, -12)) || (Math.abs(c - b) < Math.pow(10, -12)))
        } while ((max - min > 3) || egalite)
        fdexinit = '(x-(' + a + '))*(x-(' + b + '))*(x-(' + c + '))'
        ensembleZeros = [a, b, c]
        if (coefplusmoins1 === 1) {
          tabSigneRep = ['-', '+', '-', '+']
        } else {
          tabSigneRep = ['+', '-', '+', '-']
        }
      }
      fdex = coefplusmoins1 + '*(' + fdexinit + ')'
      stor.tabSigneRep = tabSigneRep
      stor.fdex = fdex
      stor.nbZero = nbZero
      stor.ensembleZeros = ensembleZeros
    }
    me.logIfDebug('fdex:' + stor.fdex)
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '5px' }) })
    stor.zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, { f: stor.nom_f, i: '\\R' })
    stor.leRepere = j3pDiv(stor.conteneur, 'div')
    const hauteurRepere = 350
    const largeurRepere = 430
    stor.idRepere = j3pGetNewId('unrepere')
    const repere = new Repere({
      idConteneur: stor.leRepere,
      idDivRepere: stor.idRepere,
      aimantage: false,
      visible: true,
      larg: largeurRepere,
      haut: hauteurRepere,

      pasdunegraduationX: 1,
      pixelspargraduationX: largeurRepere / 10.5,
      pasdunegraduationY: 1,
      pixelspargraduationY: 20,
      debuty: 0,
      xO: largeurRepere / 2,
      yO: hauteurRepere / 2,
      negatifs: true,
      fixe: true,
      trame: true,
      objets: [
        {
          type: 'courbe',
          nom: 'c1',
          par1: stor.fdex,
          par2: -7,
          par3: 7,
          par4: 100,
          style: { couleur: '#FA5', epaisseur: 2 }
        }
      ]
    })
    repere.construit()

    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '5px' }) })
    for (let i = 2; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneurD, 'div')

    if ((me.questionCourante % ds.nbetapes) === 1) {
      j3pAffiche(stor.zoneCons2, '', textes.consigne2, { f: stor.nom_f })
      j3pAffiche(stor.zoneCons3, '', textes.consigne3, { f: stor.nom_f })
      j3pAffiche(stor.zoneCons4, '', textes.consigne4, { f: stor.nom_f })
      const elt = j3pAffiche(stor.zoneCons5, '', '&1&', { inputmq1: { texte: '' } })
      stor.zoneInput = elt.inputmqList[0]
      mqRestriction(stor.zoneInput, '\\d,.;/\\-', { commandes: ['vide'] })

      stor.laPalette = j3pPaletteMathquill(stor.conteneurD, stor.zoneInput, { liste: ['vide'] })
    } else {
      // dans le cas de la deuxième question (tableau de signes)
      if (stor.nbZero === 0) {
        j3pAffiche(stor.zoneCons2, '', textes.consigne5,
          { f: stor.nom_f })
      } else if (stor.nbZero === 1) {
        j3pAffiche(stor.zoneCons2, '', textes.consigne6,
          { f: stor.nom_f, v: String(stor.ensembleZeros[0]) })
      } else {
        const zeroInit = [...stor.ensembleZeros]
        const zerosOrdonnes = []
        for (let i = 0; i < stor.ensembleZeros.length; i++) {
          const objMin = j3pMin(zeroInit)
          zerosOrdonnes[i] = objMin.min
          zeroInit.splice(objMin.indice, 1)
        }
        me.logIfDebug('zerosOrdonnes : ' + zerosOrdonnes)
        let listeZero = String(zerosOrdonnes[0])
        for (let i = 1; i < zerosOrdonnes.length; i++) {
          listeZero += '\\quad;\\quad' + String(zerosOrdonnes[i])
        }
        j3pAffiche(stor.zoneCons2, '', textes.consigne7, { f: stor.nom_f, n: stor.nbZero, v: listeZero })
      }
      j3pAffiche(stor.zoneCons3, '', textes.consigne8, { f: stor.nom_f })
      stor.leTableau = j3pAddElt(stor.zoneCons4, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { position: 'relative' }) })
      stor.objTableau = {
        nomFct: stor.nom_f,
        L: 420,
        h: 100,
        tabZero: stor.ensembleZeros,
        tabValInterdite: [],
        tabSignes: stor.tabSigneRep,
        eltsAffiches: [true, true, false],
        styleTxt: me.styles.toutpetit.enonce,
        signeDeTxt: textes.cons_tab1
      }
      stor.tabSigne = construireTableauSignes(stor.leTableau, stor.objTableau)
      stor.tabSigne.signe.forEach((elt, i) => {
        elt.typeReponse = ['texte']
        elt.reponse = [stor.tabSigneRep[i]]
      })
      j3pStyle(stor.zoneCons5, { fontStyle: 'italic' })
      if (ds.nbchances > 1) j3pAffiche(stor.zoneCons5, '', textes.info1)
    }
    stor.repAttendue = j3pAddElt(stor.conteneurD, 'div')
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    let mesZonesSaisie
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    j3pStyle(stor.zoneCorr, { paddingTop: '45px' })
    if ((me.questionCourante % ds.nbetapes) === 1) {
      mesZonesSaisie = [stor.zoneInput.id]
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: mesZonesSaisie,
        validePerso: [stor.zoneInput.id]
      })
      j3pFocus(stor.zoneInput)
    } else {
      mesZonesSaisie = stor.tabSigne.signe.map(elt => elt.id)
      stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
      me.essaiCourant = ds.nbchances
    }
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.surcharge({ nbetapes: 2 })

        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()
      break // case "enonce":
    }
    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        let aRepondu, bonneReponse, bonNombreZero, presenceZero
        if ((me.questionCourante % ds.nbetapes) === 1) {
          aRepondu = fctsValid.valideReponses()
          bonneReponse = false
          const repEleve = j3pValeurde(stor.zoneInput)
          me.logIfDebug('repEleve : ' + repEleve)
          bonNombreZero = true
          if (aRepondu) {
            if (stor.nbZero === 0) {
              bonneReponse = (repEleve === '\\varnothing')
              if (!bonneReponse) bonNombreZero = false
            } else {
              presenceZero = false
              const tabValeurs = repEleve.split(';')
              bonNombreZero = (tabValeurs.length === stor.nbZero)
              bonneReponse = bonNombreZero
              const bonneRep = []
              for (let j = 0; j < stor.nbZero; j++) {
                bonneRep[j] = false
                // bonneRep[j] permet de savoir si la j-ème valeur attendue est dans la liste des réponses
                for (let i = 0; i < tabValeurs.length; i++) {
                  bonneRep[j] = (bonneRep[j] || (Math.abs(Number(tabValeurs[i]) - stor.ensembleZeros[j]) < Math.pow(10, -12)))
                  if (Math.abs(Number(tabValeurs[i]) - stor.ensembleZeros[j]) < Math.pow(10, -12)) {
                    presenceZero = true
                  }
                }
                bonneReponse = (bonneReponse && bonneRep[j])
              }
              me.logIfDebug('presenceZero:' + presenceZero + '   stor.ensembleZeros:' + stor.ensembleZeros + '  bonneRep:' + bonneRep)
            }
          }
        } else {
          // Pour cette question, j’ai besoin de récupérer la position de chacun des signes
          stor.positionSigne = stor.tabSigne.signe.map(elt => [elt.style.left, elt.style.top, elt.getBoundingClientRect().height])
          const reponse = fctsValid.validationGlobale()
          aRepondu = reponse.aRepondu
          bonneReponse = reponse.bonneReponse
          stor.fctsValid_q2 = fctsValid
        }
        if ((!aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            if ((me.questionCourante % ds.nbetapes) === 1) {
              fctsValid.zones.bonneReponse[0] = true
              fctsValid.coloreUneZone(stor.zoneInput.id)
            }
            afficheCorrection(true)
            me.typederreurs[0]++
            me.finCorrection('navigation', true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            if ((me.questionCourante % ds.nbetapes) === 1) {
              fctsValid.zones.bonneReponse[0] = false
              fctsValid.coloreUneZone(stor.zoneInput.id)
            }
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              afficheCorrection(false)
              me.finCorrection('navigation', true)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if ((me.questionCourante % ds.nbetapes) === 1) {
                if (!bonNombreZero) stor.zoneCorr.innerHTML += '<br>' + textes.comment1
                if (presenceZero) stor.zoneCorr.innerHTML += '<br>' + textes.comment2
              } else {
                me.essaiCourant = ds.nbchances + 1
                fctsValid.coloreLesZones()
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                if ((me.questionCourante % ds.nbetapes) === 1) j3pFocus(stor.zoneInput.id)
                me.typederreurs[1]++
                // indication éventuelle ici
                me.finCorrection()
              } else {
              // Erreur au nème essai
                if ((me.questionCourante % ds.nbetapes) === 1) {
                // nombre d’erreurs sur les zéros de la fonction'
                  me.typederreurs[3]++
                } else {
                // nombre d’erreurs sur le signe de la fonction'
                  me.typederreurs[4]++
                }
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++
                afficheCorrection(false)
                me.finCorrection('navigation', true)
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          if (me.typederreurs[3] / ds.nbrepetitions >= ds.seuilerreur) {
            // Le nombre d’erreurs sur les zéros de la fonction est très important
            // cela permet le retour sur la résolution graphique d’une équation'
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[4] / ds.nbrepetitions >= ds.seuilerreur) {
            // problème sur les signes dans le tableau
            me.parcours.pe = ds.pe_3
          } else {
            me.parcours.pe = ds.pe_4
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
