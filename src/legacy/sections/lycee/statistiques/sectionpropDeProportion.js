import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pNombre, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Rémi DENIAUD
        février 2019
        On demande un pourcentage de pourcentage
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
    // ["typeQuest","les deux","liste","Dans toutes les répétitions, on a un ensemble A inclus dans B lui-même inclus dans C.<br/>Si le paramètre vaut \"résultat final\" (ce qui est le cas par défaut), on demande la proportion de A dans C, s’il vaut \"résultat intermédiaire\", ce sera une des deux autres proportions et si c’est \"les deux\", on peut avoir les deux types de questions.",["résultat final", "résultat intermédiaire", "les deux"]]
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Proportion de proportion',
  // on donne les phrases de la consigne
  consigne1_1: 'Dans un train de la ligne Paris-Bordeaux, £a&nbsp;% des voyageurs ont acheté un e-billet. Parmi ceux là, £b&nbsp;% n’ont pas imprimé le billet sur papier, mais sont passés par l’application smartphone.',
  consigne1_2: 'Dans un lycée, £{a1}&nbsp;% des élèves de Terminale ont été reçus au baccalauréat. Parmi ces reçus, £{b1}&nbsp;% ont eu une mention.',
  consigne1_3: 'Sur un parking, £{a1}&nbsp;% des véhicules sont des véhicules diesels. Parmi ces diesels, £{b1}&nbsp;% sont récents (moins de 2 ans).',
  consigne1_4: 'Dans une forêt de l’est de la France, £{a1}&nbsp;% des arbres sont des feuillus. Parmi ces feuillus, £{b1}&nbsp;% sont des chênes.',
  consigne1_5: 'Un magasin d’ameublement propose £{a1}&nbsp;% de ses meubles en bois massif. £{b1}&nbsp;% d’entre eux sont conçus avec un bois exotique.',
  consigne1_6: 'Dans un club au début de la nouvelle saison, £{a1}&nbsp;% des licenciés étaient déjà au club l’année précédente. Parmi eux, £{b1}&nbsp;% sont au club depuis plus de quatre ans.',
  consigne1_7: 'Une chaîne de télévision diffuse de très nombreuses séries. £{a1}&nbsp;% de ces séries sont récentes (moins de 3 ans). Parmi ces séries récentes, £{b1}&nbsp;% sont américaines.',
  consigne1_8: '£{a1}&nbsp;% des confitures vendues dans un magasin sont fabriquées à partir de fruits issus de l’agriculture biologique. Parmi ces "fruits bio", £{b1}&nbsp;% sont locaux (issus de vergers dans un rayon de 50&nbsp;km).',
  consigne2_1: 'Ainsi £e&1& % des voyageurs sur cette ligne ont utilisé l’application smartphone pour avoir un billet.',
  consigne2_2: 'Ainsi £e&1& % des élèves de Terminale de ce lycée ont eu une mention au baccalauréat.',
  consigne2_3: 'Ainsi £e&1& % des véhicules présents sur ce parking sont des diesels récents.',
  consigne2_4: 'Ainsi £e&1& % des arbres de cette forêt sont des chênes.',
  consigne2_5: 'Ainsi £e&1& % des meubles de ce magasin sont fabriqués avec du bois exotique.',
  consigne2_6: 'Ainsi £e&1& % des licenciés pour cette nouvelle saison sont au club depuis plus de quatre ans.',
  consigne2_7: 'Ainsi £e&1& % des séries diffusées par cette chaîne sont des séries américaines récentes.',
  consigne2_8: 'Ainsi £e&1& % des fruits utilisés pour la fabrication des confitures vendues dans ce magasin sont des fruits biologiques locaux.',
  // Les consignes suivantes sont pour les résultats intermédiaires
  consigne5: 'Arrondir à 0,1&nbsp;%.',
  consigne5_2: 'On demande la valeur exacte !',
  environ: 'environ ',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Peut-être est-ce un problème d’arrondi&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Les voyageurs qui ont utilisé l’application smartphone pour avoir un billet représentent £b&nbsp;% de £a&nbsp;% de l’ensemble des voyageurs de cette ligne.',
  corr1_2: 'Dans ce lycée, les élèves qui ont eu une mention au baccalauréat représentent £{b1}&nbsp;% de £{a1}&nbsp;% des élèves de Terminale.',
  corr1_3: 'Sur ce parking, les véhicules diesels récents représentent £{b1}&nbsp;% de £{a1}&nbsp;% de l’ensemble des véhicules.',
  corr1_4: 'Dans cette forêt, les chênes représentent £{b1}&nbsp;% de £{a1}&nbsp;% de l’ensemble des arbres.',
  corr1_5: 'Dans ce magasin, les meubles en bois exotique représentent £{b1}&nbsp;% de £{a1}&nbsp;% de l’ensemble des meubles.',
  corr1_6: 'Dans ce club, les membres licenciés depuis plus de quatre ans représentent £{b1}&nbsp;% de £{a1}&nbsp;% de l’ensemble des licenciés du club pour la nouvelle saison.',
  corr1_7: 'Sur cette chaîne, les séries américaines récentes représentent £{b1}&nbsp;% de £{a1}&nbsp;% de l’ensemble des séries diffusées.',
  corr1_8: 'Dans ce magasin, les fruits biologiques locaux représentent £{b1}&nbsp;% de £{a1}&nbsp;% de l’ensemble des fruits utilisés pour fabriquer les confitures vendues.',
  corr2: 'Or $\\frac{£{b1}}{100}\\times \\frac{£{a1}}{100}= £{r1}$ soit £e£{rep}&nbsp;%.',
  corr3_1: 'Donc £e£{rep}&nbsp;% des voyageurs de la ligne ont utilisé l’application smartphone pour avoir un billet.',
  corr3_2: 'Donc £e£{rep}&nbsp;% des élèves de Terminale de ce lycée ont eu une mention au baccalauréat.',
  corr3_3: 'Donc £e£{rep}&nbsp;% des véhicules garés sur ce parking sont des diesels récents.',
  corr3_4: 'Donc £e£{rep}&nbsp;% des arbres de cette forêt sont des chênes.',
  corr3_5: 'Donc £e£{rep}&nbsp;% des meubles de ce magasin sont fabriqués avec du bois exotique.',
  corr3_6: 'Donc £e£{rep}&nbsp;% des licenciés de ce club le sont depuis plus de quatre ans.',
  corr3_7: 'Donc £e£{rep}&nbsp;% des séries diffusées par cette chaîne sont des séries américaines récentes.',
  corr3_8: 'Donc £e£{rep}&nbsp;% des fruits utilisés pour fabriquer les confitures vendues dans ce magasin sont des fruits biologiques locaux.'
  // corr4_1 : "L’ensemble des utilisateurs de smartphone est inclu dans l’ensemble des détenteurs d’un e-billets, lui-même inclus dans l’ensemble des voyageurs de cette ligne.",
  // corr5_1 : "On cherche donc $p$ tel que $\\frac{£b}{100}\\times p=\\frac{£a}{100}$, ce qui donne $p=\\frac{£a}{£b}£{signe}£{r2}$ soit £e£{rep}&nbsp;%."
}
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    // explications contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    for (let i = 1; i <= 3; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div')
    stor.obj.signe = (stor.elt.inputmqList[0].typeReponse[1] === 'arrondi') ? '\\approx' : '='
    stor.obj.rep = (stor.elt.inputmqList[0].typeReponse[1] === 'arrondi') ? j3pVirgule(Math.round(10 * stor.elt.inputmqList[0].reponse[0]) / 10) : j3pVirgule(Math.round(1000 * stor.elt.inputmqList[0].reponse[0]) / 1000)
    j3pAffiche(stor.zoneexplication1, '', textes['corr1_' + stor.choixSujet], stor.obj)
    j3pAffiche(stor.zoneexplication2, '', textes.corr2, stor.obj)
    j3pAffiche(stor.zoneexplication3, '', textes['corr3_' + stor.choixSujet], stor.obj)
  }

  function suite () {
    // Je génère des valeurs correspondant à un contexte particulier
    if (isNaN(ds.numSujet)) ds.numSujet = -1
    if (ds.numSujet > -1) {
      stor.choixSujet = ds.numSujet
    } else {
      if (stor.tabChoixSujet.length === 0) {
        stor.tabChoixSujet = [...stor.tabChoixSujetInit]
      }
      const alea = Math.floor(j3pGetRandomInt(0, (stor.tabChoixSujet.length - 1)))
      stor.choixSujet = stor.tabChoixSujet[alea]
      stor.tabChoixSujet.splice(alea, 1)
    }
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    stor.obj = {}

    switch (stor.choixSujet) {
      case 1:
        stor.obj.a = j3pGetRandomInt(69, 83)
        do {
          stor.obj.b = j3pGetRandomInt(41, 59)
        } while (stor.obj.b === 50)
        stor.obj.r1 = stor.obj.a * stor.obj.b / 10000
        stor.obj.r = Math.round(stor.obj.a * stor.obj.b) / 10000
        break
      case 2:
        do {
          stor.obj.a = j3pGetRandomInt(789, 854) / 10
        } while (Math.abs(stor.obj.a - Math.round(stor.obj.a)) < Math.pow(10, -12))
        do {
          stor.obj.b = j3pGetRandomInt(410, 590) / 10
        } while (Math.abs(stor.obj.b - Math.round(stor.obj.b)) < Math.pow(10, -12))
        stor.obj.r1 = stor.obj.a * stor.obj.b / 10000
        stor.obj.r = Math.round(stor.obj.a * stor.obj.b / 10) / 1000
        break
      case 3:
        do {
          stor.obj.a = j3pGetRandomInt(560, 680) / 10
        } while (Math.abs(stor.obj.a - Math.round(stor.obj.a)) < Math.pow(10, -12))
        do {
          stor.obj.b = j3pGetRandomInt(210, 300) / 10
        } while (Math.abs(stor.obj.b - Math.round(stor.obj.b)) < Math.pow(10, -12))
        stor.obj.r1 = stor.obj.a * stor.obj.b / 10000
        stor.obj.r = Math.round(stor.obj.a * stor.obj.b / 10) / 1000
        break
      case 4:
        do {
          stor.obj.a = j3pGetRandomInt(400, 600) / 10
        } while (Math.abs(stor.obj.a - Math.round(stor.obj.a)) < Math.pow(10, -12))
        do {
          stor.obj.b = j3pGetRandomInt(41, 59)
        } while (stor.obj.b % 5 === 0)
        stor.obj.r1 = stor.obj.a * stor.obj.b / 10000
        stor.obj.r = Math.round(stor.obj.a * stor.obj.b / 10) / 1000
        break
      case 5:
        do {
          stor.obj.a = j3pGetRandomInt(250, 420) / 10
        } while (Math.abs(stor.obj.a - Math.round(stor.obj.a)) < Math.pow(10, -12))
        do {
          stor.obj.b = j3pGetRandomInt(600, 700) / 10
        } while (Math.abs(stor.obj.b - Math.round(stor.obj.b)) < Math.pow(10, -12))
        stor.obj.r1 = stor.obj.a * stor.obj.b / 10000
        stor.obj.r = Math.round(stor.obj.a * stor.obj.b / 10) / 1000
        break
      case 6:
        do {
          stor.obj.a = j3pGetRandomInt(750, 850) / 10
        } while (Math.abs(stor.obj.a - Math.round(stor.obj.a)) < Math.pow(10, -12))
        do {
          stor.obj.b = j3pGetRandomInt(500, 700) / 10
        } while (Math.abs(stor.obj.b - Math.round(stor.obj.b)) < Math.pow(10, -12))
        stor.obj.r1 = stor.obj.a * stor.obj.b / 10000
        stor.obj.r = Math.round(stor.obj.a * stor.obj.b / 10) / 1000
        break
      case 7:
        do {
          stor.obj.a = j3pGetRandomInt(350, 550) / 10
        } while (Math.abs(stor.obj.a - Math.round(stor.obj.a)) < Math.pow(10, -12))
        do {
          stor.obj.b = j3pGetRandomInt(500, 700) / 10
        } while (Math.abs(stor.obj.b - Math.round(stor.obj.b)) < Math.pow(10, -12))
        stor.obj.r1 = stor.obj.a * stor.obj.b / 10000
        stor.obj.r = Math.round(stor.obj.a * stor.obj.b / 10) / 1000
        break
      default:
        do {
          stor.obj.a = j3pGetRandomInt(300, 500) / 10
        } while (Math.abs(stor.obj.a - Math.round(stor.obj.a)) < Math.pow(10, -12))
        do {
          stor.obj.b = j3pGetRandomInt(700, 800) / 10
        } while (Math.abs(stor.obj.b - Math.round(stor.obj.b)) < Math.pow(10, -12))
        stor.obj.r1 = stor.obj.a * stor.obj.b / 10000
        stor.obj.r = Math.round(stor.obj.a * stor.obj.b / 10) / 1000
        break
    }
    stor.obj.a1 = j3pVirgule(stor.obj.a)
    stor.obj.b1 = j3pVirgule(stor.obj.b)
    stor.obj.r1 = j3pArrondi(stor.obj.r1, 10)// pour éviter des soucis d’arrondis
    stor.obj.e = (stor.obj.r === stor.obj.r1) ? '' : textes.environ
    const laConsigne1 = textes['consigne1_' + stor.choixSujet]
    const laConsigne2 = textes['consigne2_' + stor.choixSujet]
    j3pAffiche(stor.zoneCons1, '', laConsigne1, stor.obj)
    stor.obj.inputmq1 = { texte: '', dynamique: true, width: '12px' }
    stor.elt = j3pAffiche(stor.zoneCons2, '', laConsigne2, stor.obj)
    mqRestriction(stor.elt.inputmqList[0], '\\d.,')
    stor.elt.inputmqList[0].typeReponse = (stor.obj.e === '') ? ['nombre', 'exact'] : ['nombre', 'arrondi', 0.1]
    stor.elt.inputmqList[0].reponse = [stor.obj.r1 * 100]
    const laCons3 = (stor.elt.inputmqList[0].typeReponse[1] === 'arrondi') ? textes.consigne5 : textes.consigne5_2
    j3pAffiche(stor.zoneCons3, '', laCons3)
    me.logIfDebug('réponse:', stor.elt.inputmqList[0].reponse[0])
    const mesZonesSaisie = [stor.elt.inputmqList[0].id]
    const validePerso = []
    j3pFocus(stor.elt.inputmqList[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvent dans le cadre MD)
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.zoneD, 'div', '')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]//, left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '')
    // Paramétrage de la validation
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

        me.afficheTitre(textes.titre_exo)
        ds.nbitems = ds.nbetapes * ds.nbrepetitions

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        stor.tabChoixSujet = [1, 2, 3, 4, 5]
        stor.tabChoixSujetInit = [1, 2, 3, 4, 5]
        suite()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (stor.elt.inputmqList[0].typeReponse[1] === 'arrondi') {
                if (Math.abs(stor.elt.inputmqList[0].reponse[0] - j3pNombre(fctsValid.zones.reponseSaisie[0])) <= 0.2) {
                // pb d’arrondi
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment1
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const tauxEchecTableur = (ds.questTableur) ? me.typederreurs[3] / (ds.nbitems / 2) : 0
        const tauxEchecCalculs = (ds.questTableur) ? me.typederreurs[4] / (ds.nbitems / 2) : me.typederreurs[4] / ds.nbitems
        if (me.score / ds.nbitems >= ds.seuilReussite) {
          me.parcours.pe = ds.pe_1
        } else if ((tauxEchecTableur > ds.seuilErreur) && (tauxEchecCalculs <= ds.seuilErreur)) {
          me.parcours.pe = ds.pe_2
        } else if ((tauxEchecCalculs > ds.seuilErreur) && (tauxEchecTableur <= ds.seuilErreur)) {
          me.parcours.pe = ds.pe_3
        } else {
          me.parcours.pe = ds.pe_4
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
