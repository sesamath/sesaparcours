import { j3pAddElt, j3pArrondi, j3pDetruit, j3pVirgule, j3pNombre, j3pFocus, j3pStyle, j3pGetRandomInt, j3pAjouteBouton, j3pEmpty } from 'src/legacy/core/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog.js'
import { mqRestriction } from 'src/lib/mathquill/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { phraseBloc, afficheBloc } from 'src/legacy/core/functionsPhrase'
import textesGeneriques from 'src/lib/core/textes'

import './tableauIndices.css'

const { essaieEncore, tempsDepasse, msgReponseManquante } = textesGeneriques

/**
 * Mettre ici une description de la section
 * @author Rémi DENIAUD
 * @since octobre 2024
 * @fileOverview calcul d'indices sous la forme d'un tableau de proportionnalité
 */

// nos constantes
const structure = 'presentation1'

/**
 * Les paramètres de la section
 */
export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titreExo: 'Utilisation des indices',
  // on donne les phrases de la consigne
  consigne1: 'Un dirigeant souhaite présenter l\'évolution du chiffre d\'affaires de son entreprise. Il le fait en utilisant les indices, l\'année £a étant l\'année de référence.',
  consigne2: 'Ces indices sont présentés pour quelques années dans le tableau ci-dessous.',
  indice: 'Indice du chiffre d\'affaires',
  annee: 'Année',
  question1: 'Quelle est le pourcentage d\'évolution du chiffre d\'affaires entre les années £a et £{a2}?',
  question2: 'Puis entre les années £a et £{a3}?',
  question3: 'Quelle est le pourcentage d\'évolution du chiffre d\'affaires entre les années £{a4} et £{a5}?',
  // les différents commentaires envisagés suivant les réponses fausses de l’élève
  comment1: 'Pour chaque réponse, il faudra ajouter un signe + pour une augmentation et un signe - pour une diminution.',
  comment2: 'Il faudra ajouter un signe + pour une augmentation et un signe - pour une diminution.',
  comment3: 'La réponse sera éventuellement arrondie au dixième.',
  pbSigne: 'Il ne faut pas oublier de saisir le signe correspondant à une augmentation ou une diminution.',
  pbNombre: 'La réponse fournie ne correspond pas à un nombre.',
  pbArrondi: 'Peut-être y a-t-il un souci au niveau des arrondis&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'L\'évolution étant demandée par rapport à l\'année de référence (d\'indice 100), la simple différence des deux indices permet de conclure.',
  corr2: 'Donc l\'évolution du chiffre d\'affaires de cette entreprise est de £{c1}&nbsp;% entre £a et £{a2} et de £{c2}&nbsp;% entre £a et £{a3}.',
  corr3: 'Le taux d\'évolution du chiffre d\'affaires de l\'entreprise est égal à celui des indices. On effectue alors le calcul suivant&nbsp;:',
  corr4_1: 'Donc l\'évolution du chiffre d\'affaires de cette entreprise est de £{c1}&nbsp;% entre £{a4} et £{a5}.',
  corr4_2: 'Donc l\'évolution du chiffre d\'affaires de cette entreprise est d\'environ £{c1}&nbsp;% entre £{a4} et £{a5}.'
}

/**
 * La fonction principale, rappelée à chaque étape
 * Son this sera l’instance courante de Parcours
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  const stor = me.storage
  // Construction de la page
  me.construitStructurePage(structure) // L’argument devient {structure:structure, ratioGauche:ratioGauche} si ratioGauche est défini comme variable globale
  me.afficheTitre(textes.titreExo)
  me.surcharge({ nbetapes: 2 })
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone

  // stockage de variables, par exemples celles qui vont servir pour la pe
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Retourne les données de l’exercice
 * @private
 * @param {Parcours} me
 * @return {Object}
 */
function genereAlea (me) {
  /**
   * données de l’exercice qui peuvent être les variables présentes dans les textes (consignes ou corrections)
   * ou autre données utiles à la validation
   * @private
   * @type {Object} obj
   */
  const obj = {}
  const annees = [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023]
  obj.annees = []
  // J'extraie 4 années
  for (let i = 1; i <= 4; i++) {
    const alea = j3pGetRandomInt(0, annees.length - 1)
    obj.annees.push(annees[alea])
    annees.splice(alea, 1)
  }
  obj.annees.sort()
  obj.indices = [100]
  for (let i = 1; i <= 3; i++) {
    let newIndice, pb
    do {
      newIndice = 100 + j3pGetRandomInt(-100, 100) / 10
      pb = obj.indices.some(elt => (Math.abs(elt - newIndice) < 2))
    } while (pb)
    obj.indices.push(newIndice)
  }
  obj.a = obj.annees[0]
  const tabChoix = [1, 2, 3] // Pour la première étape
  const tabChoix2 = [1, 2, 3] // Pour la deuxième étape où je prend deux années qui ne sont pas à l'indice 100
  const lesChoix = []
  const lesChoix2 = []
  for (let i = 1; i <= 2; i++) {
    const alea = j3pGetRandomInt(0, tabChoix.length - 1)
    const alea2 = j3pGetRandomInt(0, tabChoix.length - 1)
    lesChoix.push(tabChoix[alea])
    lesChoix2.push(tabChoix2[alea2])
    tabChoix.splice(alea, 1)
    tabChoix2.splice(alea2, 1)
  }
  lesChoix.sort()
  lesChoix2.sort()
  obj.a2 = obj.annees[lesChoix[0]]
  obj.a3 = obj.annees[lesChoix[1]]
  obj.a4 = obj.annees[lesChoix2[0]]
  obj.a5 = obj.annees[lesChoix2[1]]
  obj.lesReponses = [Math.round(100 * (obj.indices[lesChoix[0]] - 100)) / 100, Math.round(100 * (obj.indices[lesChoix[1]] - 100)) / 100, (obj.indices[lesChoix2[1]] - obj.indices[lesChoix2[0]]) * 100 / obj.indices[lesChoix2[0]]]
  obj.calcQ2 = '\\frac{' + j3pVirgule(obj.indices[lesChoix2[1]]) + '-' + j3pVirgule(obj.indices[lesChoix2[0]]) + '}{' + j3pVirgule(obj.indices[lesChoix2[0]]) + '}'
  me.logIfDebug('données utiles dans l\'exercice :', obj)
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // on affiche l’énoncé
  // La style de base (ici petit.enonce) peut être complété par d’autres propriétés css. On peut aussi utiliser j3pStyle
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.enonce })
  j3pStyle(stor.elts.divEnonce, { padding: '10px' })
  // On génère les données de l’exo
  stor.objVariables = genereAlea(me)
  phraseBloc(stor.elts.divEnonce, textes.consigne1, stor.objVariables)
  phraseBloc(stor.elts.divEnonce, textes.consigne2)
  const tableIndices = j3pAddElt(stor.elts.divEnonce, 'table')
  tableIndices.className = 'tableauIndices' // Je me sers de la même mise en forme du tableau que pour la section tableauIndices
  const ligne1 = j3pAddElt(tableIndices, 'tr')
  const thCase = j3pAddElt(ligne1, 'th', textes.annee)
  thCase.className = 'tableauIndicesCaseth'
  stor.objVariables.annees.forEach(elt => {
    const cellule = j3pAddElt(ligne1, 'td', elt)
    cellule.className = 'tableauIndicesCase'
  })
  const ligne2 = j3pAddElt(tableIndices, 'tr')
  const thCase2 = j3pAddElt(ligne2, 'th', textes.indice)
  thCase2.className = 'tableauIndicesCaseth'
  stor.objVariables.indices.forEach(elt => {
    const cellule = j3pAddElt(ligne2, 'td', j3pVirgule(elt))
    cellule.className = 'tableauIndicesCase'
  })
  let zonesInput
  if (me.questionCourante % ds.nbetapes === 1) {
    // on demande l'évalution avec l'année de référence
    phraseBloc(stor.elts.divEnonce, textes.question1, stor.objVariables)
    const elt1 = afficheBloc(stor.elts.divEnonce, '&1&%', {
      inputmq1: { texte: '' }
    })
    phraseBloc(stor.elts.divEnonce, textes.question2, stor.objVariables)
    const elt2 = afficheBloc(stor.elts.divEnonce, '&1&%', {
      inputmq1: { texte: '' }
    })
    zonesInput = [elt1.inputmqList[0], elt2.inputmqList[0]]
    zonesInput.forEach((zone, index) => {
      zone.typeReponse = ['nombre', 'exact']
      zone.reponse = [stor.objVariables.lesReponses[index]]
    })
    stor.precisionArrondi = afficheBloc(stor.elts.divEnonce, textes.comment1, { style: { paddingTop: '15px', fontStyle: 'italic', fontSize: '90%' } })
  } else {
    phraseBloc(stor.elts.divEnonce, textes.question3, stor.objVariables)
    const elt1 = afficheBloc(stor.elts.divEnonce, '&1&%', {
      inputmq1: { texte: '' }
    })
    zonesInput = [elt1.inputmqList[0]]
    zonesInput[0].typeReponse = (Math.abs(10 * stor.objVariables.lesReponses[2] - Math.round(10 * stor.objVariables.lesReponses[2])) < Math.pow(10, -12))
      ? ['nombre', 'exact']
      : ['nombre', 'arrondi', 0.1]
    zonesInput[0].reponse = [stor.objVariables.lesReponses[2]]
    stor.precisionArrondi = j3pAddElt(stor.elts.divEnonce, 'div', '', { style: { paddingTop: '15px', fontStyle: 'italic', fontSize: '90%' } })
    phraseBloc(stor.precisionArrondi, textes.comment2)
    phraseBloc(stor.precisionArrondi, textes.comment3)
  }
  zonesInput.forEach(zone => mqRestriction(zone, '+\\-\\d,.'))
  stor.zoneCalc = j3pAddElt(me.zonesElts.MD, 'div', { style: { padding: '5px' } })
  me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
  j3pCreeFenetres(me)
  j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
  j3pAfficheCroixFenetres('Calculatrice')
  stor.fctsValid = new ValidationZones({
    zones: zonesInput,
    parcours: me
  })
  // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvent dans le cadre MD)
  // Il n’est ainsi construit qu’une seule fois et vidé à chaque passage dans correction()
  stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.moyen.correction })
  j3pStyle(stor.zoneCorr, { padding: '10px' })
  j3pFocus(zonesInput[0])
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me, bonneReponse = false) {
  // on affiche la correction
  const stor = me.storage
  const ds = me.donneesSection
  j3pDetruit(stor.zoneCalc)
  j3pDetruitFenetres('Calculatrice')
  j3pEmpty(stor.precisionArrondi)
  const zoneExpli = j3pAddElt(stor.elts.divEnonce, 'div', '', { style: me.styles.petit.correction })
  // La couleur de la correction sera fonction de la qualité de la réponse de l’élève
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  if (me.questionCourante % ds.nbetapes === 1) {
    stor.objVariables.c1 = (stor.objVariables.lesReponses[0] > 0)
      ? '+' + j3pVirgule(stor.objVariables.lesReponses[0])
      : j3pVirgule(stor.objVariables.lesReponses[0])
    stor.objVariables.c2 = (stor.objVariables.lesReponses[1] > 0)
      ? '+' + j3pVirgule(stor.objVariables.lesReponses[1])
      : j3pVirgule(stor.objVariables.lesReponses[1])
    phraseBloc(zoneExpli, textes.corr1, stor.objVariables)
    phraseBloc(zoneExpli, textes.corr2, stor.objVariables)
  } else {
    phraseBloc(zoneExpli, textes.corr3, stor.objVariables)
    stor.objVariables.c1 = (stor.objVariables.lesReponses[2] > 0)
      ? '+' + j3pVirgule(j3pArrondi(stor.objVariables.lesReponses[2], 1))
      : j3pVirgule(j3pArrondi(stor.objVariables.lesReponses[2], 1))
    const estArrondi = (Math.abs(10 * stor.objVariables.c1 - Math.round(10 * stor.objVariables.c1)) > Math.pow(10, -12))
    const leCalcul = stor.objVariables.calcQ2 + '\\times 100' + ((estArrondi) ? '\\approx' : '=') + stor.objVariables.c1
    afficheBloc(zoneExpli, '$' + leCalcul + '$')
    phraseBloc(zoneExpli, (estArrondi) ? textes.corr4_2 : textes.corr4_1, stor.objVariables)
  }
}
/**
 * Vérifie si la réponse saisie est bien égale à l'arrondi de valeur à la précison attendue
 * @param {number} repEleve
 * @param {number} valeur
 * @param {number} precision c'est un nombre décimal de la forme 10^-...
 * @return {boolean}
 */
function valideBonArrondi (repEleve, valeur, precision) {
  const nbRep1 = Math.floor(valeur / precision) * precision
  const nbRep2 = Math.ceil(valeur / precision) * precision
  // ordrePuissance de nbRep1 est le nombre n tel que 10^n<= nbRep1 < 10^{n+1}
  let puisPrecision, ordrePuissance
  if (nbRep1 === 0) {
    puisPrecision = 15
  } else {
    ordrePuissance = String(Math.abs(valeur)).indexOf('.')
    // ordrePuissance = Math.floor(Math.log(Math.abs(nbRep1))/Math.log(10))+1;
    puisPrecision = (ordrePuissance === '-1') ? 15 - String(Math.abs(valeur)).length : 15 - ordrePuissance
  }
  const bonArrondi = (Math.round((valeur - nbRep1) * Math.pow(10, puisPrecision)) / Math.pow(10, puisPrecision) < precision / 2)
    ? nbRep1
    : nbRep2

  // console.log(bonArrondi, puisPrecision, repEleve) // ne pas laisser ça en prod ! ça donne la solution en console
  return (Math.abs(repEleve - bonArrondi) < Math.pow(10, -puisPrecision))
}
/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number|null}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const ds = me.donneesSection
  const reponse = {}
  reponse.aRepondu = stor.fctsValid.valideReponses()
  // Si les zones sont bien remplies, je vérifie alors que j'ai bien un signe comme premier caractère et que j'ai bien écrit un nombre
  const signeRep = []
  const numberRep = []
  let score = 0
  if (reponse.aRepondu) {
    stor.pasDeReponse = false
    stor.signeAvant = true// pour vérifier si ce qui est saisi est bien précédé d'un signe
    stor.reponseNombre = true // pour vérifier si on a bien saisi un nombre (en plus du signe)
    for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
      const maReponse = stor.fctsValid.zones.reponseSaisie[i]
      if (['+', '-'].includes(maReponse[0])) signeRep.push(maReponse[0])
      else stor.signeAvant = false
      if (stor.signeAvant) { // je regarde si ce qui suit est bien un nombre
        if (/^\d+,?\d*$/.test(maReponse.slice(1))) numberRep.push(j3pNombre(maReponse.slice(1)))
        else stor.reponseNombre = false
      }
    }
    reponse.aRepondu = stor.signeAvant && stor.reponseNombre
  } else {
    stor.pasDeReponse = true
  }
  if (reponse.aRepondu) {
    for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
      const repAttendue = stor.fctsValid.zones.inputs[i].reponse[0]
      const signeReponse = (repAttendue > 0) ? '+' : '-'
      const bonSigne = signeReponse === signeRep[i]
      let bonNombre
      if (me.questionCourante % ds.nbetapes === 1) { // réponses exactes
        bonNombre = Math.abs(Math.abs(repAttendue) - numberRep[i]) < Math.pow(10, -12)
      } else {
        bonNombre = valideBonArrondi(numberRep[i], Math.abs(repAttendue), stor.fctsValid.zones.inputs[i].typeReponse[2])
        if (!bonNombre) {
          // on vérifie s'il n'y a pas de problème d'arrondi(s)
          if (Math.abs(j3pNombre(numberRep[i]) - repAttendue) < stor.fctsValid.zones.inputs[i].typeReponse[2] * 2) stor.pbArrondi = true
        }
      }
      stor.fctsValid.zones.bonneReponse[i] = bonSigne && bonNombre
      stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
      if (stor.fctsValid.zones.bonneReponse[i]) {
        if (me.questionCourante % ds.nbetapes === 1) { // il y a deux zones à compléter
          score += 0.5
        } else {
          score += 1
        }
      }
    }
    return j3pArrondi(score, 2) // attention à la gestion des flottants
  }
  return null
}

function correction (me) {
  const stor = me.storage
  const ds = me.donneesSection
  const score = getScore(me) // null si pas répondu
  if ((score === 1) || (me.essaiCourant >= ds.nbchances)) me.score += score
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    if (stor.pasDeReponse) me.reponseManquante(stor.zoneCorr, msgReponseManquante)
    else if (!stor.signeAvant) me.reponseManquante(stor.zoneCorr, '<br>' + textes.pbSigne)
    else if (!stor.reponseNombre) me.reponseManquante(stor.zoneCorr, '<br>' + textes.pbNombre)
    return
  }
  if (score !== 1) {
    // On écrit c’est faux
    // Cas particulier ici où on modifie le commentaire suivant que la réponse est totalement ou partiellement fausse
    if (score === 0) me.reponseKo(stor.zoneCorr)
    else me.reponseKo(stor.zoneCorr, textes.erreur)
    if (stor.pbArrondi) me.reponseKo(stor.zoneCorr, '<br>' + textes.pbArrondi, true)
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  // score pouvant être un nombre entre 0 et 1, !score n’est pas équivalent à (score !== 1)
  if ((score !== 1) && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.zoneCorr, '<br>' + essaieEncore, true)
    return
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.zoneCorr)
    // Même dans le cas d’une bonne réponse, on peut afficher la correction qui aurait dans ce cas d’éventuels compléments d’information
    afficheCorrection(me, true)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, tempsDepasse, true)
    }
    afficheCorrection(me)
  }
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) {
    me.etat = 'enonce'
  }
  // Ici on a rien à afficher de particulier après la dernière répétition, donc on passe true à finNavigation
  // pour que le modèle affiche directement le bouton "section suivante" (sans devoir cliquer sur 'suite' avant)
  me.finNavigation(true)
}
