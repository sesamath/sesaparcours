import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pChaine, j3pNombre, j3pShuffle, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pStyle, j3pVirgule, j3pGetBornesIntervalle, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { remplaceCalcul } from 'src/legacy/outils/fonctions/gestionParametres'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
   Exemple de Graphe de test
   j3p.html?graphe=[1,"proportion",[{pe:">=0",nn:"2",conclusion:"Etape 2"}]];[2,"proportion",[{pe:">=0",nn:"fin",conclusion:"fin"}]];
 */
/*
        DENIAUD Rémi
        décembre 2018
        Dans cette section, on demande de calculer une proportion au format décimal ou au format pourcentage
 */
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo1: 'Calcul d’une proportion (en %)',
  titre_exo2: 'Calcul d’une proportion',
  titre_exo3: 'Calcul d’un effectif à l’aide d’une proportion',
  titre_exo4: 'Proportion et effectif',
  titre_exo5: 'Proportion et effectif total',
  titre_exo6: 'Calcul de proportions et d’effectifs',
  // il faut parfois gérer la présence de valeurs approchées et donc ajouter ces mots...
  txt1: 'environ ',
  txt2: '',
  // les consignes :
  cons1: '',
  // les différents commentaires envisagés suivant les réponses fausses de l’élève
  comment1: 'N’y aurait-il pas un problème d’arrondi ?',
  comment2: 'On rappelle que pour arrondir un pourcentage à £a %, il faut arrondir la valeur décimale à £b.',
  // et les phrases utiles pour les explications de la réponse
  corr1: ''
}

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['formatPourcentage', true, 'boolean', 'Si ce paramètre vaut true, alors l’élève devra donner un pourcentage. Sinon ce sera une valeur décimale entre 0 et 1.'],
    ['calculDemande', false, 'boolean', 'Lorsque ce paramètre vaut true, alors on impose à l’élève de donner le calcul avant la réponse.'],
    ['typeQuestion', 'Les deux', 'liste', 'Ce paramètre permet de préciser le type de question posé:<br/>- "Effectif" : la proportion est donnée et on demande l’effectif associé ;<br/>- "Proportion" : l’effectif est donné et on demande un pourcentage ou une proportion ;<br/>- "Les deux" : on peut demander un effectif ou une proportion d’une question à l’autre ;<br/>- "Effectif total" : on doit retrouver l’effectif total à l’aide de la proportion d’un critère ;<br/>- "Les trois" : on peut proposer chacune des trois configurations.', ['Effectif', 'Proportion', 'Les deux', 'Effectif total', 'Les trois']],
    ['seuilReussite', 0.8, 'reel', 'seuil au-dessus duquel on considère que l’élève a réussi l’exercice'],
    ['seuilErreur', 0.4, 'reel', 'seuil en-dessous duquel on considère que la notion n’est pas acquise']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'Problème d’arrondis' },
    { pe_3: 'Insuffisant' }
  ]
}
/**
 * section proportion
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })

    if (ds.calculDemande) j3pDetruit(stor.laPalette)

    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    stor.obj.r2 = j3pVirgule(stor.obj.r)
    for (let i = 1; i <= stor['correction' + stor.leType]['enonce' + stor.sujetChoisi].length; i++) {
      const div = j3pAddElt(explications, 'div')
      j3pAffiche(div, '', stor['correction' + stor.leType]['enonce' + stor.sujetChoisi][i - 1], stor.obj)
    }
  }

  function suite () {
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    let numConsigne = 0
    // on choisit un nouveau sujet
    // on choisit à chaque repetition un sujet (parmi ceux qui ne l’ont pas encore été (ou parmi tous les sujets s’ils ont tous été traités)
    // on peut forcer le choix du sujet (paramètre non renseigné car il sert juste pour effectuer les tests)
    if (ds.numSujet === undefined || isNaN(ds.numSujet)) ds.numSujet = -1
    const tabTypeSujet = ['Prop', 'Eff', 'Total']
    for (let k = 0; k < tabTypeSujet.length; k++) {
      if (stor['listeSujets' + tabTypeSujet[k]].length === 0) {
        stor['listeSujets' + tabTypeSujet[k]] = [...stor['listeSujets' + tabTypeSujet[k] + 'Init']]
      }
    }
    me.logIfDebug('stor.listeSujets (Prop, Eff et Total):', stor.listeSujetsProp, '   ', stor.listeSujetsEff, '   ', stor.listeSujetsTotal)
    stor.typeSujet = stor.listeTypeQuest[me.questionCourante - 1]
    const leType = tabTypeSujet[['Proportion', 'Effectif', 'Total'].indexOf(stor.typeSujet)]
    // ce qui suit sert pour les tests afin de choisir le sujet souhaité
    if (ds.numSujet > -1) {
      stor.sujetChoisi = ds.numSujet
    } else {
      const choix = j3pGetRandomInt(0, stor['listeSujets' + leType].length - 1)
      stor.sujetChoisi = stor['listeSujets' + leType][choix]
      stor['listeSujets' + leType].splice(choix, 1)
    }
    // gestion des variables dans cet énoncé :
    stor.nomVariables = []
    stor.variables = []
    stor.obj = {}
    for (let i = 0; i < stor.mesVariables['enonce' + stor.sujetChoisi].length; i++) {
      stor.nomVariables[i] = stor.mesVariables['enonce' + stor.sujetChoisi][i][0]
      const [minInt, maxInt] = j3pGetBornesIntervalle(stor.mesVariables['enonce' + stor.sujetChoisi][i][1])
      stor.variables[i] = (stor.mesVariables['enonce' + stor.sujetChoisi][i].length === 2)
        ? j3pGetRandomInt(minInt, maxInt)
        : j3pGetRandomInt(minInt, maxInt) / Number(stor.mesVariables['enonce' + stor.sujetChoisi][i][2])
    }
    let lettre // a|b|c
    for (let i = 0; i < stor.nomVariables.length; i++) {
      lettre = stor.nomVariables[i]
      stor.obj[lettre] = stor.variables[i]
      stor.obj[lettre + '2'] = j3pVirgule(stor.obj[lettre])
    }

    // calcul des réponse
    const reponses = stor.reponses['enonce' + stor.sujetChoisi]
    let leResultat
    if (stor.typeSujet === 'Proportion') {
      leResultat = (stor.formatPourcentage)
        ? remplaceCalcul({ text: reponses.pourcent, obj: stor.obj })
        : remplaceCalcul({ text: reponses.proport, obj: stor.obj })
    } else if (stor.typeSujet === 'Effectif') {
      leResultat = remplaceCalcul({ text: reponses.part, obj: stor.obj })
    } else {
      leResultat = remplaceCalcul({ text: reponses.total, obj: stor.obj })
    }
    me.logIfDebug('leResultat:', leResultat)
    const larrondi = (stor.typeSujet === 'Proportion')
      ? (stor.formatPourcentage)
          ? stor.arrondis['enonce' + stor.sujetChoisi][0]
          : stor.arrondis['enonce' + stor.sujetChoisi][1]
      : stor.arrondis['enonce' + stor.sujetChoisi][0]
    const resultatExact = (Math.abs(Math.round(leResultat / larrondi) * larrondi - leResultat) < Math.pow(10, -10))
    const resultatExact2 = (Math.abs(Math.round(leResultat / (larrondi / 1000)) * (larrondi / 1000) - leResultat) < Math.pow(10, -10))
    stor.obj.e = resultatExact ? '' : textes.txt1
    stor.obj.s = resultatExact2 ? '=' : '\\approx'
    // la réponse à l’arrondi demandé (utile pour la correction):
    stor.obj.r = Math.round(leResultat / larrondi) * larrondi
    stor.obj.r1 = Math.round(100 * leResultat / larrondi) * larrondi / 100
    stor.obj.styletexte = {}

    // attention car obj.r peut être mal arrondi à cause de la variable larrondi qui peut être un décimal
    stor.obj.r = Math.round(stor.obj.r * Math.pow(10, 6)) / Math.pow(10, 6)
    stor.obj.r1 = Math.round(stor.obj.r1 * Math.pow(10, 6)) / Math.pow(10, 6)
    for (let i = 0; i < stor.consignes['enonce' + stor.sujetChoisi].length; i++) {
      stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
      j3pAffiche(stor['zoneCons' + i], '', stor.consignes['enonce' + stor.sujetChoisi][i],
        stor.obj)
      numConsigne++
    }
    stor.leType = (stor.typeSujet === 'Proportion')
      ? (stor.formatPourcentage) ? 'Pourc' : 'Prop'
      : leType
    stor.listeZoneInput = []
    if (ds.calculDemande) {
      // dans ce cas, on doit traiter deux zones de saisie : la première avec le calcul, la deuxième avec le résultat
      stor.obj.inputmq1 = { texte: '' }
      stor.obj.inputmq2 = { texte: '' }
      for (let i = 1; i < stor['question' + stor.leType]['enonce' + stor.sujetChoisi].length; i++) {
        stor['zoneCons' + numConsigne] = j3pAddElt(stor.conteneur, 'div', '')
        const elt = j3pAffiche(stor['zoneCons' + numConsigne], '', stor['question' + stor.leType]['enonce' + stor.sujetChoisi][i], stor.obj)
        if (elt.inputmqList.length > 0) stor.listeZoneInput.push(elt.inputmqList[0], elt.inputmqList[1])
        numConsigne++
      }
      stor.listeZoneInput[1].reponse = [leResultat]
    } else {
      stor['zoneCons' + numConsigne] = j3pAddElt(stor.conteneur, 'div', '')
      stor.obj.inputmq1 = { texte: '' }
      const elt = j3pAffiche(stor['zoneCons' + numConsigne], '', stor['question' + stor.leType]['enonce' + stor.sujetChoisi][0], stor.obj)
      stor.listeZoneInput.push(elt.inputmqList[0])
      stor.listeZoneInput[0].reponse = [leResultat]
      numConsigne++
    }
    if (!resultatExact) {
      stor['zoneCons' + numConsigne] = j3pAddElt(stor.conteneur, 'div', '')
      const pos = (stor.typeSujet === 'Proportion') ? (stor.formatPourcentage) ? 0 : 1 : 0
      j3pAffiche(stor['zoneCons' + numConsigne], '', stor.arrondisTxt['enonce' + stor.sujetChoisi][pos], stor.obj)
    }
    if (ds.calculDemande) {
      stor.laPalette = j3pAddElt(stor.conteneur, 'div')
      // création de la palette
      $(stor.listeZoneInput[0]).focusin(function () {
        j3pEmpty(stor.laPalette)
        j3pPaletteMathquill(stor.laPalette, stor.listeZoneInput[0], {
          liste: ['fraction']
        })
      })
      $(stor.listeZoneInput[1]).focusin(function () {
        j3pEmpty(stor.laPalette)
      })
    }
    let mesZonesSaisie, mesvalidePerso
    if (ds.calculDemande) {
      mqRestriction(stor.listeZoneInput[0], '\\d.,/*+-', { commandes: ['fraction'] })
      mqRestriction(stor.listeZoneInput[1], '\\d.,')
      stor.listeZoneInput[0].typeReponse = ['texte']
      if (resultatExact) stor.listeZoneInput[1].typeReponse = ['nombre', 'exact']
      else stor.listeZoneInput[1].typeReponse = ['nombre', 'arrondi', larrondi]
      mesZonesSaisie = stor.listeZoneInput.map(elt => elt.id)
      mesvalidePerso = [stor.listeZoneInput[0].id]
    } else {
      mqRestriction(stor.listeZoneInput[0], '\\d.,')
      if (resultatExact) stor.listeZoneInput[0].typeReponse = ['nombre', 'exact']
      else stor.listeZoneInput[0].typeReponse = ['nombre', 'arrondi', larrondi]
      mesZonesSaisie = [stor.listeZoneInput[0].id]
      mesvalidePerso = []
    }
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesvalidePerso })
    stor.numConsigne = numConsigne - 1
    stor.larrondi = larrondi
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvent dans le cadre MD)
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.zoneD, 'div')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '')
    j3pStyle(stor.zoneCorr, { paddingTop: '10px' })
    j3pFocus(stor.listeZoneInput[0])
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.6 })
        stor.formatPourcentage = ds.formatPourcentage
        let titreExo
        // on gère le titre de l’exo mais aussi le type de question posée à chaque répétition
        stor.listeTypeQuest = []
        let tabChoixQuest
        switch (ds.typeQuestion) {
          case 'Proportion':
            titreExo = (stor.formatPourcentage) ? textes.titre_exo1 : textes.titre_exo2
            for (let i = 0; i < ds.nbrepetitions; i++) {
              stor.listeTypeQuest.push('Proportion')
            }
            break
          case 'Effectif':
            titreExo = textes.titre_exo3
            for (let i = 0; i < ds.nbrepetitions; i++) {
              stor.listeTypeQuest.push('Effectif')
            }
            break
          case 'Les deux':
            titreExo = textes.titre_exo4
            // choix entre Proportion et Effectif
            tabChoixQuest = []
            for (let k = 0; k < Math.floor(ds.nbrepetitions / 2); k++) {
              tabChoixQuest.push('Effectif', 'Proportion')
            }
            if (ds.nbrepetitions % 2 === 1) {
              const dernierChoix = (Math.floor(Math.random() * 2) === 0) ? 'Effectif' : 'Proportion'
              tabChoixQuest.push(dernierChoix)
            }
            stor.listeTypeQuest = j3pShuffle(tabChoixQuest)
            break
          case 'Effectif total':
            titreExo = textes.titre_exo5
            for (let i = 0; i < ds.nbrepetitions; i++) {
              stor.listeTypeQuest.push('Total')
            }
            break
          default :// les trois
            titreExo = textes.titre_exo6
            // choix entre Proportion, Effectif et Effectif total
            tabChoixQuest = []
            for (let k = 0; k < Math.floor(ds.nbrepetitions / 3); k++) {
              tabChoixQuest.push('Effectif', 'Proportion', 'Total')
            }
            {
              const tabChoix = ['Effectif', 'Proportion', 'Total']
              for (let k = 3 * Math.floor(ds.nbrepetitions / 3); k < ds.nbrepetitions; k++) {
                const choixAlea = Math.floor(Math.random() * tabChoix.length)
                tabChoixQuest.push(tabChoix[choixAlea])
                tabChoix.splice(choixAlea, 1)
              }
            }
            stor.listeTypeQuest = j3pShuffle(tabChoixQuest)
            break
        }
        me.afficheTitre(titreExo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        // On initialise le nombre d’erreur à chaque type de question à 0
        stor.erreurArrondi = 0
        // quand on demande une proportion ou un pourcentage
        stor.listeSujetsProp = []
        stor.listeSujetsPropInit = []
        // quand on demande de retrouver un effectif à l’aide d’une proportion
        stor.listeSujetsEff = []
        stor.listeSujetsEffInit = []
        // quand on demande de retrouver l’effectif total
        stor.listeSujetsTotal = []
        stor.listeSujetsTotalInit = []
        import('../../../sectionsAnnexes/Consignesexos/proportion.js').then(function parseAnnexe ({ default: datasSection }) {
          let j
          stor.nb_sujets = datasSection.sujets.length
          stor.typeSujet = {}
          stor.consignes = {}
          stor.mesVariables = {}
          stor.questionPourc = {}
          stor.questionProp = {}
          stor.questionEff = {} // Quand on connait la proportion
          stor.questionTotal = {} // Quand on cherche l’effectif total
          stor.reponses = {}
          stor.arrondisTxt = {}
          stor.arrondis = {}
          stor.correctionPourc = {}
          stor.correctionProp = {}
          stor.correctionEff = {} // Quand on connait la proportion
          stor.correctionTotal = {} // Quand on cherche l’effectif total
          stor.valeurs = {}
          for (j = 0; j < stor.nb_sujets; j++) {
            stor.typeSujet['enonce' + j] = datasSection.sujets[j].typeSujet
            stor.consignes['enonce' + j] = datasSection.sujets[j].consignes
            stor.mesVariables['enonce' + j] = datasSection.sujets[j].variables
            switch (stor.typeSujet['enonce' + j]) {
              case 'calculProportion':
                stor.questionPourc['enonce' + j] = datasSection.sujets[j].questionPourc
                stor.questionProp['enonce' + j] = datasSection.sujets[j].questionProp
                stor.correctionPourc['enonce' + j] = datasSection.sujets[j].correctionPourc
                stor.correctionProp['enonce' + j] = datasSection.sujets[j].correctionProp
                stor.listeSujetsProp.push(j)
                stor.listeSujetsPropInit.push(j)
                break
              case 'calculEffectif':
                stor.questionEff['enonce' + j] = datasSection.sujets[j].questionEff
                stor.correctionEff['enonce' + j] = datasSection.sujets[j].correctionEff
                stor.listeSujetsEff.push(j)
                stor.listeSujetsEffInit.push(j)
                break
              default :
                stor.questionTotal['enonce' + j] = datasSection.sujets[j].questionTotal
                stor.correctionTotal['enonce' + j] = datasSection.sujets[j].correctionTotal
                stor.listeSujetsTotal.push(j)
                stor.listeSujetsTotalInit.push(j)
                break
            }
            stor.arrondis['enonce' + j] = datasSection.sujets[j].arrondis
            stor.arrondisTxt['enonce' + j] = datasSection.sujets[j].arrondisTxt
            stor.reponses['enonce' + j] = datasSection.sujets[j].reponses
            stor.valeurs['enonce' + j] = datasSection.sujets[j].valeurs
          }
          suite()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        let pbArrondi = false

        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si la zone a bien été complétée
        // reponse.bonneReponse qui teste si la réponse est la bonne
        // Ici, la zone de calcul devra être testée manuellement
        if (reponse.aRepondu && ds.calculDemande) {
          const repCalculLatex = fctsValid.zones.reponseSaisie[0]
          // On doit avoir une fraction
          if (repCalculLatex.indexOf('frac') > -1) {
          // on cherche à extraire le numérateur et le dénominateur
          // var fracRegExp = /\\\\frac\\{[0-9]{1,}\\}\\{[0-9]{1,}\\}/g
            let repCalcul = j3pMathquillXcas(repCalculLatex)
            const produitRegExp = /[0-9.]+[*+-]?[0-9.]+/g
            let tabProduit = repCalcul.match(produitRegExp)
            // console.log(tabProduit)

            if (tabProduit === null) tabProduit = []
            for (let k = 0; k < tabProduit.length; k++) {
            // On remplace tous les produits par le résultat du calcul
              repCalcul = repCalcul.replace(tabProduit[k], j3pCalculValeur(tabProduit[k]))
            }
            me.logIfDebug('repCalcul:', repCalcul)
            const fracRegExp = /\([0-9]+\.?[0-9]*\)\/\([0-9]+\.?[0-9]*\)/i
            const tabFrac = repCalcul.match(fracRegExp)
            const valResultat = j3pCalculValeur(repCalcul)// on détermine le résultat obtenu avec ce calcul et on va le comparer vace le résultats attendu
            let resultatOK
            if (stor.typeSujet === 'Proportion') {
              if (stor.formatPourcentage) {
              // dans ce cas j’accepte aussi bien la proportion que la proportion*100 (qui est al réponse attendue)
                resultatOK = (Math.abs(valResultat - stor.listeZoneInput[1].reponse[0]) < Math.pow(10, -9))
                resultatOK = (resultatOK || (Math.abs(valResultat - stor.listeZoneInput[1].reponse[0] / 100) < Math.pow(10, -9)))
              } else {
              // dans ce cas j’accepte aussi bien la proportion (qui est al réponse attendue) que la proportion*100
                resultatOK = (Math.abs(valResultat - stor.listeZoneInput[1].reponse[0]) < Math.pow(10, -9))
                resultatOK = (resultatOK || (Math.abs(valResultat - stor.listeZoneInput[1].reponse[0] * 100) < Math.pow(10, -9)))
              }
            } else if (stor.typeSujet === 'Effectif') {
              resultatOK = (Math.abs(valResultat - stor.listeZoneInput[1].reponse[0]) < Math.pow(10, -9))
            } else {
            // Dans le cas où on cherche l’effectif total, on accepte de ne pas multiplier par 100 dans le calcul
              resultatOK = (Math.abs(valResultat - stor.listeZoneInput[1].reponse[0]) < Math.pow(10, -9))
              resultatOK = (resultatOK || (Math.abs(valResultat - stor.listeZoneInput[1].reponse[0] / 100) < Math.pow(10, -9)))
            }
            me.logIfDebug('valeurs : ', valResultat, '   ', stor.listeZoneInput[1].reponse[0], '   resultatOK:', resultatOK, '  ', fracRegExp.test(repCalcul), '  tabFrac:', tabFrac)
            fctsValid.zones.bonneReponse[0] = (fracRegExp.test(repCalcul) && resultatOK)// En d’autres termes, si j’ai un quotient dans le calcul et que le résultat du calcul est le bon, alors je considère son calcul comme correct
          } else {
          // je n’ai pas de fraction, donc le calcul ne peut être considéré comme acceptables
            fctsValid.zones.bonneReponse[0] = false
          }
          fctsValid.coloreUneZone(fctsValid.zones.inputs[0])
          reponse.bonneReponse = (reponse.bonneReponse && fctsValid.zones.bonneReponse[0])
        }
        // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
        // Si c’est la dernière tentative de répondre, on barre également les zones fausses
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // on regarde s’il n’y a pas un problème d’arrondi
            if (stor.listeZoneInput[0].typeReponse[1] === 'arrondi') {
            // On vérifie si c’est une valeur approchée
              let reponseSaisie = (stor.numQuest === 2) ? fctsValid.zones.reponseSaisie[1] : fctsValid.zones.reponseSaisie[0]
              reponseSaisie = j3pNombre(reponseSaisie)
              pbArrondi = (Math.abs(stor.listeZoneInput[0].reponse[0] - reponseSaisie) < stor.larrondi * 10)
            }
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (pbArrondi) {
                stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
                // gestion de la phrase où on a des variables
                if (stor.typeSujet === 'Proportion') {
                  const phraseComment = j3pChaine(textes.comment2, {
                    a: j3pVirgule(stor.larrondi),
                    b: j3pVirgule(Math.round(stor.larrondi * Math.pow(10, 3)) / Math.pow(10, 5))
                  })
                  stor.zoneCorr.innerHTML += '<br/>' + phraseComment
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                // On relève alors les 3 types de fautes commise
                if (pbArrondi) {
                  stor.erreurArrondi++
                }
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const tauxEchecArrondi = stor.erreurArrondi / ds.nbitems
        // J’ai le taux d’erreur de chaque type de question
        /* me.pe_1="Bien";
        me.pe_2="Gros soucis avec les arrondis";
        me.pe_3="Insuffisant" */
        if (tauxEchecArrondi > ds.seuilErreur) {
          me.parcours.pe = ds.pe_2
        } else if (me.score / ds.nbitems >= ds.seuilReussite) {
          me.parcours.pe = ds.pe_1
        } else {
          me.parcours.pe = ds.pe_3
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
