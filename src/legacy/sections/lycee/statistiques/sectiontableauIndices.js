import { j3pAddElt, j3pArrondi, j3pDetruit, j3pVirgule, j3pNombre, j3pFocus, j3pStyle, j3pGetRandomInt, j3pAjouteBouton } from 'src/legacy/core/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog.js'
import { mqRestriction } from 'src/lib/mathquill/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { phraseBloc, afficheBloc } from 'src/legacy/core/functionsPhrase'
import textesGeneriques from 'src/lib/core/textes'

import './tableauIndices.css'

const { essaieEncore, tempsDepasse, msgReponseManquante } = textesGeneriques

/**
 * Mettre ici une description de la section
 * @author Rémi DENIAUD
 * @since octobre 2024
 * @fileOverview calcul d'indices sous la forme d'un tableau de proportionnalité
 */

// nos constantes
const structure = 'presentation1'

/**
 * Les paramètres de la section
 */
export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['onlyIndices', true, 'boolean', 'Si la valeur true est passée à ce paramètre, seul le calcul d\'indices sera demandé (sinon, ce sera un indice et une valeur)']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titreExo: 'Indices dans un tableau',
  // on donne les phrases de la consigne
  consigne1: 'Compléter le tableau suivant par les indices manquants.',
  consigne2: 'Compléter le tableau suivant par les valeurs ou indices manquants.',
  indice: 'Indice du prix|Indice de la quantité|Indice de la valeur',
  valeur: 'Prix|Quantité|Valeur',
  annee: 'Année',
  // les différents commentaires envisagés suivant les réponses fausses de l’élève
  comment1: 'Les réponses seront éventuellement arrondies au dixième.',
  erreur: 'L\'une des deux réponses est fausse.',
  pbArrondi: 'Peut-être y a-t-il un souci au niveau des arrondis&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Nous sommes dans un tableau de proportionnalité.',
  corr2: 'Pour compléter la deuxième colonne, on effectue le calcul :',
  corr3_1: 'Donc l\'indice de la deuxième colonne vaut £{r1}.',
  corr3_2: 'Donc l\'indice de la deuxième colonne vaut environ £{r1}.',
  corr4: 'Pour compléter la troisième colonne, on effectue le calcul :',
  corr5_1_1: 'Donc l\'indice de la troisième colonne vaut £{r2}.',
  corr5_1_2: 'Donc l\'indice de la troisième colonne vaut environ £{r2}.',
  corr5_2_1: 'Donc la valeur attendue dans la troisième colonne est £{r2}.',
  corr5_2_2: 'Donc la valeur attendue dans la troisième colonne est environ £{r2}.'
}

/**
 * La fonction principale, rappelée à chaque étape
 * Son this sera l’instance courante de Parcours
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  const stor = me.storage
  // Construction de la page
  me.construitStructurePage(structure) // L’argument devient {structure:structure, ratioGauche:ratioGauche} si ratioGauche est défini comme variable globale
  me.afficheTitre(textes.titreExo)
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone

  // stockage de variables, par exemples celles qui vont servir pour la pe
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Retourne les données de l’exercice
 * @private
 * @param {Parcours} me
 * @return {Object}
 */
function genereAlea (me) {
  /**
   * données de l’exercice qui peuvent être les variables présentes dans les textes (consignes ou corrections)
   * ou autre données utiles à la validation
   * @private
   * @type {Object} obj
   */
  const obj = {}
  // Je choisis les différentes années
  const annees = [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023]
  obj.tabAnnee = []
  // J'extraie 4 années
  for (let i = 1; i <= 3; i++) {
    const alea = j3pGetRandomInt(0, annees.length - 1)
    obj.tabAnnee.push(annees[alea])
    annees.splice(alea, 1)
  }
  obj.tabAnnee.sort()
  obj.tabVal = []
  for (let i = 1; i <= 3; i++) {
    let newVal, pb
    let pb2 = false // si je demande la valeur de la dernière colonnne, comme on a une division par 100,
    // je ne veux pas que le deuxième chiffre après la virgule soit égal à 5 (ça va être pénible pour l'arrondi)
    do {
      newVal = j3pGetRandomInt(200, 360)
      pb = obj.tabVal.some(val => Math.abs(val - newVal) < 15)
      if (!me.donneesSection.onlyIndices && i === 3) {
        const indice = Math.round(newVal * 100 / obj.tabVal[0])
        const laVal = obj.tabVal[0] * indice / 100
        pb2 = Math.abs(Math.abs(laVal * 10 - Math.round(laVal * 10)) - 0.5) < Math.pow(10, -10)
      }
    } while (pb || pb2)
    obj.tabVal.push(newVal)
    obj.tabIndices = obj.tabVal.map(val => (val * 100) / obj.tabVal[0])
  }
  if (!me.donneesSection.onlyIndices) {
    // je donne un indice entier pour la dernière colonne
    const val = Math.round(obj.tabIndices[obj.tabIndices.length - 1])
    obj.tabIndices[obj.tabIndices.length - 1] = val
    obj.tabVal[obj.tabVal.length - 1] = obj.tabVal[0] * val / 100
  }
  me.logIfDebug('valeurs :', obj)
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // on affiche l’énoncé
  // La style de base (ici petit.enonce) peut être complété par d’autres propriétés css. On peut aussi utiliser j3pStyle
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.enonce })
  j3pStyle(stor.elts.divEnonce, { padding: '10px' })
  // On génère les données de l’exo
  stor.objVariables = genereAlea(me)
  phraseBloc(stor.elts.divEnonce, (ds.onlyIndices) ? textes.consigne1 : textes.consigne2, stor.objVariables)
  const tableIndices = j3pAddElt(stor.elts.divEnonce, 'table')
  tableIndices.className = 'tableauIndices'
  const ligne0 = j3pAddElt(tableIndices, 'tr')
  const thCase0 = j3pAddElt(ligne0, 'th', textes.annee)
  thCase0.className = 'tableauIndicesCaseth'
  stor.objVariables.tabAnnee.forEach(elt => {
    const an = j3pAddElt(ligne0, 'td', elt)
    an.className = 'tableauIndicesCase'
  })
  const ligne1 = j3pAddElt(tableIndices, 'tr')
  const tabLegende1 = textes.valeur.split('|')
  const choixAlea = j3pGetRandomInt(0, tabLegende1.length - 1)
  const thCase = j3pAddElt(ligne1, 'th', tabLegende1[choixAlea])
  thCase.className = 'tableauIndicesCaseth'
  const divVal = []
  stor.objVariables.tabVal.forEach((elt, index) => {
    divVal.push((index === stor.objVariables.tabVal.length - 1 && !ds.onlyIndices)
      ? j3pAddElt(ligne1, 'td')
      : j3pAddElt(ligne1, 'td', j3pVirgule(elt))
    )
    divVal[index].className = 'tableauIndicesCase'
  })
  const ligne2 = j3pAddElt(tableIndices, 'tr')
  const thCase2 = j3pAddElt(ligne2, 'th', textes.indice.split('|')[choixAlea])
  thCase2.className = 'tableauIndicesCaseth'
  const divIndices = []
  stor.objVariables.tabIndices.forEach((elt, index) => {
    if (index === 0) {
      divIndices.push(j3pAddElt(ligne2, 'td', j3pVirgule(elt)))
    } else if (index === 1) {
      divIndices.push(j3pAddElt(ligne2, 'td'))
    } else {
      divIndices.push((ds.onlyIndices)
        ? j3pAddElt(ligne2, 'td')
        : j3pAddElt(ligne2, 'td', j3pVirgule(elt))
      )
    }
    divIndices[index].className = 'tableauIndicesCase'
  })
  // case du tableau à compléter (cela servira pour la correction)
  stor.casesTab = [divIndices[1], (ds.onlyIndices) ? divIndices[2] : divVal[2]]
  const divZones = (ds.onlyIndices) ? divIndices.slice(1, 3) : [divIndices[1], divVal[2]]
  const elt1 = afficheBloc(divZones[0], '&1&', {
    inputmq1: { texte: '' }
  })
  const elt2 = afficheBloc(divZones[1], '&1&', {
    inputmq1: { texte: '' }
  })
  const zonesInput = [elt1.inputmqList[0], elt2.inputmqList[0]]
  zonesInput.forEach(zone => mqRestriction(zone, '\\d,.'))
  stor.pbArrondi = afficheBloc(stor.elts.divEnonce, textes.comment1, { style: { paddingTop: '15px', fontStyle: 'italic', fontSize: '90%' } })
  me.logIfDebug('stor.objVariables:', stor.objVariables)
  stor.zoneCalc = j3pAddElt(me.zonesElts.MD, 'div', { style: { padding: '5px' } })
  me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
  j3pCreeFenetres(me)
  j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
  j3pAfficheCroixFenetres('Calculatrice')
  zonesInput.forEach((zone, index) => {
    zone.typeReponse = ['nombre', 'arrondi', 0.1]
    if (index === 0) {
      zone.reponse = [stor.objVariables.tabIndices[1]]
    } else {
      zone.reponse = [(ds.onlyIndices) ? stor.objVariables.tabIndices[2] : stor.objVariables.tabVal[2]]
    }
  })
  stor.fctsValid = new ValidationZones({
    zones: zonesInput,
    parcours: me
  })
  j3pFocus(zonesInput[0])
  // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvent dans le cadre MD)
  // Il n’est ainsi construit qu’une seule fois et vidé à chaque passage dans correction()
  stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.moyen.correction })
  j3pStyle(stor.zoneCorr, { padding: '10px' })
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me, bonneReponse = false) {
  // on affiche la correction
  const stor = me.storage
  const ds = me.donneesSection
  j3pDetruit(stor.zoneCalc)
  j3pDetruitFenetres('Calculatrice')
  const zoneExpli = j3pAddElt(stor.elts.divEnonce, 'div', '', { style: me.styles.petit.correction })
  // La couleur de la correction sera fonction de la qualité de la réponse de l’élève
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })

  // premier calcul :
  const calc1 = '$\\frac{' + stor.objVariables.tabVal[1] + '\\times 100}{' + stor.objVariables.tabVal[0] + '}$'
  const calc2 = ds.onlyIndices
    ? '$\\frac{' + stor.objVariables.tabVal[2] + '\\times 100}{' + stor.objVariables.tabVal[0] + '}$'
    : '$\\frac{' + stor.objVariables.tabIndices[2] + '\\times ' + stor.objVariables.tabVal[0] + '}{100}$'
  ;[1, 2].forEach(i => {
    phraseBloc(zoneExpli, textes['corr' + i], stor.objVariables)
  })
  afficheBloc(zoneExpli, calc1)
  const arrondi1 = Math.round(stor.objVariables.tabIndices[1] * 10) / 10
  stor.objVariables.r1 = j3pVirgule(arrondi1)
  phraseBloc(zoneExpli, (Math.abs(arrondi1 - stor.objVariables.tabIndices[1]) < Math.pow(10, -10)) ? textes.corr3_1 : textes.corr3_2, stor.objVariables)
  phraseBloc(zoneExpli, textes.corr4, stor.objVariables)
  afficheBloc(zoneExpli, calc2)
  if (ds.onlyIndices) {
    const arrondi2 = Math.round(stor.objVariables.tabIndices[2] * 10) / 10
    stor.objVariables.r2 = j3pVirgule(arrondi2)
    phraseBloc(zoneExpli, (Math.abs(arrondi2 - stor.objVariables.tabIndices[2]) < Math.pow(10, -10)) ? textes.corr5_1_1 : textes.corr5_1_2, stor.objVariables)
  } else {
    const arrondi2 = Math.round(stor.objVariables.tabVal[2] * 10) / 10
    stor.objVariables.r2 = j3pVirgule(arrondi2)
    phraseBloc(zoneExpli, (Math.abs(arrondi2 - stor.objVariables.tabVal[2]) < Math.pow(10, -10)) ? textes.corr5_2_1 : textes.corr5_2_2, stor.objVariables)
  }
  for (let i = 0; i < 2; i++) {
    if (!stor.fctsValid.zones.bonneReponse[i]) {
      // On affiche la réponse arrondie dans le tableau
      j3pAddElt(stor.casesTab[i], 'div', j3pVirgule(Math.round(10 * stor.fctsValid.zones.inputs[i].reponse[0]) / 10), { style: me.styles.petit.correction })
    }
  }
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number|null}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  let score = 0
  if (reponse.aRepondu) {
    for (let i = 0; i <= 1; i++) {
      if (stor.fctsValid.zones.bonneReponse[i]) score += 0.5
    }
    stor.pbArrondi = false
    if (score < 0.9) {
      // on vérifie s'il n'y a pas de problème d'arrondi(s)
      for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
        if (!stor.fctsValid.zones.bonneReponse[i]) {
          if (Math.abs(j3pNombre(stor.fctsValid.zones.reponseSaisie[i]) - stor.fctsValid.zones.inputs[i].reponse[0]) < 0.2) stor.pbArrondi = true
        }
      }
    }
    return j3pArrondi(score, 2) // attention à la gestion des flottants
  }
  return null
}

function correction (me) {
  const stor = me.storage
  const ds = me.donneesSection
  const score = getScore(me) // null si pas répondu
  if ((score === 1) || (me.essaiCourant >= ds.nbchances)) me.score += score
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    me.reponseManquante(stor.zoneCorr, msgReponseManquante)
    return
  }
  if (score !== 1) {
    // On écrit c’est faux
    // Cas particulier ici où on modifie le commentaire suivant que la réponse est totalement ou partiellement fausse
    if (score === 0) me.reponseKo(stor.zoneCorr)
    else me.reponseKo(stor.zoneCorr, textes.erreur)
    if (stor.pbArrondi) me.reponseKo(stor.zoneCorr, '<br>' + textes.pbArrondi, true)
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  // score pouvant être un nombre entre 0 et 1, !score n’est pas équivalent à (score !== 1)
  if ((score !== 1) && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.zoneCorr, '<br>' + essaieEncore, true)
    return
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.zoneCorr)
    // Même dans le cas d’une bonne réponse, on peut afficher la correction qui aurait dans ce cas d’éventuels compléments d’information
    afficheCorrection(me, true)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, tempsDepasse, true)
    }
    afficheCorrection(me)
  }
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) {
    me.etat = 'enonce'
  }
  // Ici on a rien à afficher de particulier après la dernière répétition, donc on passe true à finNavigation
  // pour que le modèle affiche directement le bouton "section suivante" (sans devoir cliquer sur 'suite' avant)
  me.finNavigation(true)
}
