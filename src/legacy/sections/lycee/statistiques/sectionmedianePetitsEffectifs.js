import $ from 'jquery'
import { j3pAddElt, j3pFocus, j3pGetRandomInt, j3pRemplacePoint, j3pRestriction, j3pShowError, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

import './medianePetitsEffectifs.css'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2019
        On demande de déterminer la médiane d’une série de valeur dans le cas où on a un petit effectif
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeDonnees', 'liste', 'liste', 'Les données peuvent être sous la forme d’une liste de valeur ou d’un tableau (mais avec petits effectifs), voire les deux', ['liste', 'tableau', 'les deux']]
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Médiane pour de petites séries',
  // on donne les phrases de la consigne
  consigne1_1: 'On considère la série statistique suivante contenant £e valeurs&nbsp;:',
  consigne1_2: 'Voici la liste des £e dernières notes obtenue par Myriam en mathématiques&nbsp;:',
  consigne1_3: 'On considère la liste du nombre de points marqués au cours des £e derniers matchs d’une équipe de rugby&nbsp;:',
  consigne1_4: 'Rémi a relevé le nombre de fautes d’orthographe qu’il a commises dans ses £e dernières copies de français&nbsp;:',
  consigne1_5: 'On a relevé la quantité de pluie (en mm) tombée dans une région au cours des £e dernières semaines&nbsp;:',
  consignebis1_1: 'Tony va pêcher tous les week-ends. Il a noté dans le tableau ci-dessous le nombre de poissons pêchés au cours des £e derniers week-ends&nbsp;:',
  consignebis1_2: 'Chaque semaine, Medhy compte le nombre de fois où il mange à la cantine.<br/>Le tableau ci-dessous résume ces données sur les £e dernières semaines&nbsp;:',
  consignebis1_3: 'Voici, dans le tableau ci-dessous, le relevé du nombre de courriels journaliers reçus par un internaute au cours de £e derniers jours&nbsp;:',
  consignebis1_4: 'Le tableau ci-dessous donne, par jour, le nombre de produits abîmés au cours des £e derniers jours dans une petite supérette&nbsp;:',
  consignebis1_5: 'Une petite boutique répare et revend du matériel électronique. Chaque jour, elle note le nombre de smartphones qu’elle a vendus.<br/>Ces résultats sont consignés dans le tableau ci-dessous&nbsp;:',
  tableaubis1_1: 'Nombre de poissons',
  tableaubis1_2: 'Nombre de repas à la cantine',
  tableaubis1_3: 'Nombre de courriels',
  tableaubis1_4: 'Nombre de produits abîmés',
  tableaubis1_5: 'Nombre de smartphones',
  tableau2: 'Effectifs',
  consigne2: 'Quelle est la médiane de cette série statistique&nbsp;?',
  consigne3: 'Médiane : @1@.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Fais une figure en respectant l’ordre des sommets et tu trouveras facilement les diagonales !',
  comment2: 'La fraction doit être irréductible !',
  comment3: 'Simplifie l’écriture de ta réponse !',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Il faut tout d’abord ranger les valeurs dans l’ordre croissant&nbsp;:',
  corrbis1: 'Les valeurs étant déjà rangées dans l’ordre croissant, on peut les écrire sous la forme d’une liste en prenant en compte les effectifs&nbsp;:',
  corr2: 'Cette liste contient £e valeurs. On prend alors les £f plus petites (en violet) et les £f plus grandes (en marron).',
  corr3_1: 'La médiane est alors la valeur centrale de cette liste soit £m.',
  corr3_2: 'Comme il n’y a pas une valeur centrale dans cette liste, on prend pour médiane la moyenne entre les deux valeurs centrales, soit $\\frac{£{m1}+£{m2}}{2}=£m$.'
}

/**
 * section medianePetitsEffectifs
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })

    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    const laCorr1 = (stor.tabQuestions[me.questionCourante - 1] === 'liste')
      ? textes.corr1
      : textes.corrbis1
    j3pAffiche(stor.zoneExpli1, '', laCorr1)
    j3pAffiche(stor.zoneExpli2, '', stor.listeOrdonne)
    j3pAffiche(stor.zoneExpli3, '', textes.corr2,
      {
        e: stor.nbElements,
        f: Math.floor(stor.nbElements / 2)
      })
    const listeCouleur = stor.listePetites + '&nbsp;;&nbsp;' + stor.listeGrandes
    j3pAffiche(stor.zoneExpli4, '', listeCouleur)
    if (stor.nbElements % 2 === 0) {
      j3pAffiche(stor.zoneExpli5, '', textes.corr3_2,
        {
          m: j3pVirgule(stor.mediane),
          m1: j3pVirgule(stor.m1),
          m2: j3pVirgule(stor.m2)
        })
    } else {
      j3pAffiche(stor.zoneExpli5, '', textes.corr3_1,
        {
          m: j3pVirgule(stor.mediane)
        })
    }
  }

  function getDonneesExo (num, type) {
    // num correspond au choix du sujet
    // type vaut "tableau" ou "liste"
    let nbElements; const tabDonnees = []; let mediane
    let listePetites
    let listeGrandes
    let i, listeInit, pioche, m1, m2, listeDonnees
    if (type === 'liste') {
      nbElements = j3pGetRandomInt(5, 8)
      listeDonnees = ''
      switch (num) {
        case 1:
          for (i = 1; i <= nbElements; i++) {
            tabDonnees.push(j3pGetRandomInt(2, 20))
          }
          break
        case 2:
          listeInit = [10, 11, 12, 13, 13, 14, 15, 16, 17, 17, 18, 19]
          break
        case 3:
          for (i = 1; i <= nbElements; i++) {
            tabDonnees.push(j3pGetRandomInt(12, 35))
          }
          break
        case 4:
          listeInit = [0, 1, 2, 3, 4, 4, 5, 6, 7, 7, 8, 9, 10]
          break
        default :
          listeInit = [0, 4, 5, 8, 9, 11, 13, 14, 15, 19, 20, 21, 24, 25, 30, 32]
          break
      }
      if (num === 2 || num >= 4) {
        for (i = 1; i <= nbElements; i++) {
          pioche = j3pGetRandomInt(0, (listeInit.length - 1))
          tabDonnees.push(listeInit[pioche])
          listeInit.splice(pioche, 1)
        }
      }
      const tabOrdonne = [...tabDonnees]
        .sort((a, b) => a - b)
      mediane = (nbElements % 2 === 1)
        ? tabOrdonne[(nbElements - 1) / 2]
        : Math.round(1000 * (tabOrdonne[nbElements / 2 - 1] + tabOrdonne[nbElements / 2]) / 2) / 1000
      // la liste initiale en string
      listeDonnees = j3pVirgule(tabDonnees[0])
      // la liste ordonnée en string
      let listeOrdonnee = j3pVirgule(tabOrdonne[0])
      for (i = 1; i < nbElements; i++) {
        listeDonnees += '&nbsp;;&nbsp;' + j3pVirgule(tabDonnees[i])
        listeOrdonnee += '&nbsp;;&nbsp;' + j3pVirgule(tabOrdonne[i])
      }
      // Je souhaite aussi identifier les plus petites valeurs et les plus grandes
      listePetites = '<span class="liste1">' + j3pVirgule(tabOrdonne[0]) + '</span>'
      for (i = 1; i <= nbElements / 2 - 1; i++) {
        listePetites += '&nbsp;;&nbsp;<span class="liste1">' + j3pVirgule(tabOrdonne[i]) + '</span>'
      }
      // Ces deux variables servent à donner la valeur max du premier paquet et la valeur max du 2ème
      m1 = tabOrdonne[Math.floor(nbElements / 2 - 1)]
      m2 = tabOrdonne[Math.ceil((nbElements - 1) / 2)]
      if (nbElements % 2 === 0) {
        listeGrandes = '<span class="liste2">' + j3pVirgule(tabOrdonne[Math.ceil((nbElements - 1) / 2)]) + '</span>'
      } else {
        listeGrandes = '<span class="liste2">' + j3pVirgule(tabOrdonne[Math.ceil((nbElements - 1) / 2)]) + '</span>'
      }
      for (i = Math.ceil((nbElements - 1) / 2) + 1; i < nbElements; i++) {
        listeGrandes += '&nbsp;;&nbsp;<span class="liste2">' + j3pVirgule(tabOrdonne[i]) + '</span>'
      }
      return {
        tabDonnees,
        tabOrdonne,
        mediane,
        m1,
        m2,
        listeInit: listeDonnees,
        listePetites,
        listeGrandes,
        listeOrdonne: listeOrdonnee,
        nbElements
      }
    }

    // tabDonnees[0] contiendra les valeurs tabDonnees[1] les effectifs de chacune de ces valeurs
    nbElements = j3pGetRandomInt(4, 5)
    tabDonnees[0] = []
    tabDonnees[1] = []
    switch (num) {
      case 1:
        listeInit = [0, 1, 2, 3, 4, 5, 6]
        break
      case 2:
        listeInit = [0, 1, 2, 3, 4, 5]
        break
      case 3:
        listeInit = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        break
      case 4:
        listeInit = [2, 3, 4, 5, 6, 7, 8, 9, 10]
        break
      default :
        listeInit = [1, 2, 3, 4, 5, 6, 7]
        break
    }

    for (i = 1; i <= nbElements; i++) {
      pioche = j3pGetRandomInt(0, (listeInit.length - 1))
      tabDonnees[0].push(listeInit[pioche])
      listeInit.splice(pioche, 1)
    }
    tabDonnees[0].sort(function (a, b) {
      return a - b
    })
    let nbUn = 0
    let valEgales = true
    let newEff
    for (i = 0; i < nbElements; i++) {
      if (nbUn === 2) {
        newEff = j3pGetRandomInt(2, 3)
      } else {
        newEff = j3pGetRandomInt(1, 3)
        if (newEff === 1) nbUn++
      }
      if ((i > 1) && (newEff !== tabDonnees[1][i - 1])) valEgales = false
      if (valEgales && (i === tabDonnees[1].length - 1)) {
        do {
          newEff = j3pGetRandomInt(2, 3)
        } while (newEff === tabDonnees[1][i - 1])
      }
      tabDonnees[1].push(newEff)
    }
    // Maintenant, je vais créer la liste des valeurs ordonnées (avec doublons donc) :
    tabDonnees[2] = []
    for (i = 0; i < nbElements; i++) {
      for (let j = 0; j < tabDonnees[1][i]; j++) {
        tabDonnees[2].push(tabDonnees[0][i])
      }
    }
    // Je souhaite aussi identifier les plus petites valeurs et les plus grandes
    listePetites = '<span class="liste1">' + j3pVirgule(tabDonnees[2][0]) + '</span>'
    for (i = 1; i <= tabDonnees[2].length / 2 - 1; i++) {
      listePetites += '&nbsp;;&nbsp;<span class="liste1">' + j3pVirgule(tabDonnees[2][i]) + '</span>'
    }
    if (tabDonnees[2].length % 2 === 0) {
      listeGrandes = '<span class="liste2">' + j3pVirgule(tabDonnees[2][Math.ceil((tabDonnees[2].length - 1) / 2)]) + '</span>'
    } else {
      listeGrandes = j3pVirgule(tabDonnees[2][Math.ceil((tabDonnees[2].length - 1) / 2)])
    }
    for (i = Math.ceil((tabDonnees[2].length - 1) / 2) + 1; i < tabDonnees[2].length; i++) {
      listeGrandes += '&nbsp;;&nbsp;<span class="liste2">' + j3pVirgule(tabDonnees[2][i]) + '</span>'
    }
    mediane = (tabDonnees[2].length % 2 === 1) ? tabDonnees[2][(tabDonnees[2].length - 1) / 2] : Math.round(1000 * (tabDonnees[2][tabDonnees[2].length / 2 - 1] + tabDonnees[2][tabDonnees[2].length / 2]) / 2) / 1000
    m1 = tabDonnees[2][Math.floor(tabDonnees[2].length / 2 - 1)]
    m2 = tabDonnees[2][Math.ceil((tabDonnees[2].length - 1) / 2)]// Ces deux variables servent à donner la valeur max du premier paquet et la valeur max du 2ème
    listeDonnees = j3pVirgule(tabDonnees[2][0])
    for (i = 1; i < tabDonnees[2].length; i++) {
      listeDonnees += '&nbsp;;&nbsp;' + j3pVirgule(tabDonnees[2][i])
    }
    return {
      tabDonnees,
      mediane,
      m1,
      m2,
      listeOrdonne: listeDonnees,
      listePetites,
      listeGrandes,
      nbElements: tabDonnees[2].length
    }
  }

  function tableauDonneesEffectifs ({ parent, largeur, labels, valeurs, effectifs }) {
    // mesDonnees est un objet
    // parent est le nom du div dans lequel est créé le tableau
    // largeur est la largeur du tableau (utile pour configurer la largeur de chaque colonne)
    // labels est un tableau de 3 éléments max : "Valeurs", Effectifs" ou des équivalents à saisir
    // valeurs : tableau des valeurs
    // effectifs : tableaux des effectifs
    function creerLigneTab (elt, valeurs, l1, l2) {
      // elt est l’élément html dans lequel construire la ligne
      // l1 et l2 sont respectivement la largeur de la première colonne et des suivantes
      const ligne = j3pAddElt(elt, 'tr')
      j3pAddElt(ligne, 'td', valeurs[0], { width: l1 + 'px' })
      for (let i = 1; i < valeurs.length; i++) {
        j3pAddElt(ligne, 'td', String(valeurs[i]).replace(/\./g, ','), { align: 'center', width: l2 + 'px' })
      }
    }

    const leTableau = j3pAddElt(parent, 'div')
    leTableau.setAttribute('width', largeur + 'px')
    if (valeurs.length === effectifs.length) {
      let largeurTexte = 100
      for (let i = 0; i < labels.length; i++) {
        const tabBR = ['<BR>', '</BR>', '<BR/>', '<br>', '<br>']
        for (let k = 0; k < tabBR.length; k++) {
          if (labels[i].includes(tabBR[k])) {
            labels[i] = labels[i].replace(tabBR[k], '<br/>')
          }
        }
        const tabDonneesTexte = labels[i].split('<br/>')
        for (let j = 0; j < tabDonneesTexte.length; j++) {
          largeurTexte = Math.max(largeurTexte, $(leTableau).textWidth(tabDonneesTexte[j], me.styles.petit.enonce.fontFamily, me.styles.petit.enonce.fontSize))
        }
      }
      largeurTexte += 10
      const largeurColonne = (largeur - largeurTexte - 5) / effectifs.length
      const table = j3pAddElt(leTableau, 'table', { width: '100%', borderColor: 'black', border: 2, cellSpacing: 0 })
      creerLigneTab(table, [labels[0]].concat(valeurs), largeurTexte, largeurColonne)
      creerLigneTab(table, [labels[1]].concat(effectifs), largeurTexte, largeurColonne)
      // on renvoie un objet contenant le nom des zones de saisie (lorsqu’elles existent) et le tableau des effectifs cumulés croissants
      return { largeurCol1: largeurTexte, largeurAutreCol: largeurColonne }
    }
    j3pShowError('pb sur le nombre de valeurs dans les deux tableaux', { vanishAfter: 5 })
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }), className: 'medianePetitsEffectifs' })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // Je génère des valeurs correspondant à un contexte particulier
    const numType = (stor.tabQuestions[me.questionCourante - 1] === 'liste') ? 1 : 2
    if (stor['tabChoixSujet' + numType].length === 0) {
      stor['tabChoixSujet' + numType] = [...stor['tabChoixSujetInit' + numType]]
    }
    const alea = j3pGetRandomInt(0, stor['tabChoixSujet' + numType].length - 1)
    const choixSujet = stor['tabChoixSujet' + numType][alea]
    stor['tabChoixSujet' + numType].splice(alea, 1)
    stor.choixSujet = choixSujet
    Object.assign(stor, getDonneesExo(choixSujet, stor.tabQuestions[me.questionCourante - 1]))

    me.logIfDebug('stor après affectation getDonneesExo:', stor)

    const laConsigne1 = (stor.tabQuestions[me.questionCourante - 1] === 'liste')
      ? textes['consigne1_' + stor.choixSujet]
      : textes['consignebis1_' + stor.choixSujet]
    j3pAffiche(stor.zoneCons1, '', laConsigne1,
      {
        e: stor.nbElements
      })
    if (stor.tabQuestions[me.questionCourante - 1] === 'liste') {
      // Les données sont sous la forme d’une liste
      j3pAffiche(stor.zoneCons2, '', stor.listeInit)
    } else {
      // Là on construit un tableau
      stor.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
      const valeurs = {
        parent: stor.zoneCons2,
        labels: [textes['tableaubis1_' + stor.choixSujet], textes.tableau2],
        valeurs: stor.tabDonnees[0],
        effectifs: stor.tabDonnees[1],
        largeur: stor.largeurTab
      }
      stor.objmedianePetitsEffectifs = tableauDonneesEffectifs(valeurs)
    }
    j3pAffiche(stor.zoneCons3, '', textes.consigne2)
    const elt = j3pAffiche(stor.zoneCons4, '', textes.consigne3,
      {
        input1: { texte: '', dynamique: true }
      })
    elt.inputList[0].addEventListener('input', j3pRemplacePoint)
    // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
    // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche
    j3pRestriction(elt.inputList[0], '0-9,.')
    elt.inputList[0].typeReponse = ['nombre', 'exact']
    elt.inputList[0].reponse = [stor.mediane]
    j3pFocus(elt.inputList[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: [elt.inputList[0].id] })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage('presentation1')
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // code de création des fenêtres
        stor.tabQuestions = []
        const listeQuest = ['liste', 'tableau']
        for (let j = 1; j <= ds.nbrepetitions; j++) {
          if (ds.typeDonnees === 'les deux') {
            const num = j3pGetRandomInt(0, 1)
            stor.tabQuestions.push(listeQuest[num])
            stor.tabQuestions.push(listeQuest[(num + 1) % 2])
          } else {
            stor.tabQuestions.push(ds.typeDonnees)
          }
        }

        // pour les valeurs écrites sous la forme d’une liste
        stor.tabChoixSujet1 = [1, 2, 3, 4, 5]
        stor.tabChoixSujetInit1 = [1, 2, 3, 4, 5]
        // Pour les valeurs écrites sous la forme d’un tableau
        stor.tabChoixSujet2 = [1, 2, 3, 4, 5]
        stor.tabChoixSujetInit2 = [1, 2, 3, 4, 5]
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie

      const reponse = fctsValid.validationGlobale()

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        return me.finCorrection()
      }

      // Une réponse a été saisie
      // Bonne réponse
      if (reponse.bonneReponse) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        afficheCorrection(true)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
      // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // indication éventuelle ici
        // ici il a encore la possibilité de se corriger
        return me.finCorrection()
      }

      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(false)
      me.typederreurs[2]++
      return me.finCorrection('navigation', true)
    }

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
