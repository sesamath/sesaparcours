import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pNombre, j3pRandomTab, j3pShowError, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Décembre 2018
        Dans cette section, on demande de calculer une moyenne pondérée ou un écart-type (voire les deux) à l’aide d’un tableau de valeurs (ou intervalles) avec effectifs
        On peut aussi ajouter la détermination de l’étendue
        Le caractère quantitatif peut être discret ou continu
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeCaractere', 'discret', 'liste', 'prend les valeurs "discret" (par défaut) ou "continu" suivant le type de caractère proposé.', ['discret', 'continu']],
    ['typeQuest', 'moyenne', 'liste', 'Cette section permet de tester des calculs de moyenne, d’écart-type et d’étendue. Il est possibile de choisir une ou plusieurs de ces questions.', ['moyenne', 'ecart type', 'etendue', 'moyenne+ecart type', 'moyenne+etendue', 'ecart type+etendue', 'les trois']],
    ['petitEffectif', false, 'boolean', 'Dans le cas où la question sur le tableur n’est pas posée, on peut proposer des calculs sur des petits effectifs. Dans ce cas, l’affichage se fait sous la forme d’une liste, pas d’une feuille de calcul.']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo1: 'Calcul de moyenne',
  titre_exo2: 'Calcul de l’écart-type',
  titre_exo3: 'Calcul de l’étendue',
  titre_exo4: 'Moyenne et écart-type',
  titre_exo5: 'Moyenne et étendue',
  titre_exo6: 'Ecart-type et étendue',
  titre_exo7: 'Moyenne, écart-type et étendue',
  // on donne les phrases de la consigne
  consigne1_1: 'L’étude d’un caractère quantitatif discret sur une population de £e éléments donne les résultats regroupés dans le tableau ci-dessous.',
  consignebis1_1: 'L’étude d’un caractère quantitatif continu sur une population de £e éléments donne les résultats regroupés dans le tableau ci-dessous.',
  // et maintenant des cas concrets
  consigne1_2: 'On s’intéresse au nombre d’absences d’élèves d’un lycée. Pour cela, on interroge £e élèves et on consigne dans le tableau ci-dessous le nombre d’absences regroupées par effectifs.',
  consignebis1_2: 'On a relevé la durée totale des retards (en minutes) sur un trimestre de £e élèves d’un lycée. Ces éléments ont été regroupés dans le tableau ci-dessous.',
  consigne1_3: 'On relève la pointure des chaussures vendues par un magasin. Sur la période considérée, £e clients ont effectué un achat&nbsp;; on consigne dans le tableau ci-dessous les différentes pointures choisies, regroupées par effectif.',
  consignebis1_3: 'Un magasin de vente de tissu au détail relève la quantité prise par chaque client (exprimée en mètres) au cours d’une période. Le tableau regroupe les résultats pour £e clients.',
  consigne1_4: 'On relève le nombre de jours de pluie au cours d’un hiver dans £e villes de France. Les résultats ont été consignés dans le tableau ci-dessous où l’effectif correspond au nombre de villes considéré.',
  consignebis1_4: 'On relève la quantité de pluie (exprimée en mm) relevée dans une région pendant £e jours. Ces éléments ont été regroupés dans le tableau ci-dessous.',
  consigne1_5: 'On s’intéresse aux photos retouchées d’un magazine. On a relevé sur £e numéros le nombre de photos retouchées et on a regroupé ces résultats sous la forme d’effectifs dans le tableau ci-dessous.',
  consignebis1_5: 'On s’intéresse au prix de gâteaux d’une boulangerie pendant la période de Noël. Dans le tableau ci-dessous on a relevé le prix de £e gâteaux de cette boulangerie.',
  // Dans le cas où on a de petits effectifs
  consignePetit1_1: 'On a regroupé les valeurs d’une série statistique discrète de £e valeurs dans le tableau ci-dessous&nbsp;:',
  consignePetit1_2: 'On a regroupé le nombre de poissons pêchés au cours de £e jours de pêche dans le tableau ci-dessous&nbsp;:',
  consignePetit1_3: 'On a regroupé le nombre d’essais marqués par une équipe au cours des £e derniers matchs dans le tableau ci-dessous&nbsp;:',
  consignePetit1_4: 'On a regroupé les notes d’un bon élève à ses £e derniers devoirs dans le tableau ci-dessous&nbsp;:',
  consignePetit1_5: 'On a regroupé le nombre d’absents d’une classe au cours des £e derniers jours de cours dans le tableau ci-dessous&nbsp;:',
  consignePetitbis1_1: 'On a regroupé les valeurs d’une série statistique continue de £e valeurs dans le tableau ci-dessous&nbsp;:',
  consignePetitbis1_2: 'On a regroupé le temps passé (en heures) devant la télévision par £e jeunes dans le tableau ci-dessous&nbsp;:',
  consignePetitbis1_3: 'On a regroupé le prix payé (en euros) par une personne au cours de ses £e derniers repas au restaurant dans le tableau ci-dessous&nbsp;:',
  consignePetitbis1_4: 'On a regroupé la quantité de pluie tombée (en mm) au cours des £e dernières semaines dans le tableau ci-dessous&nbsp;:',
  consignePetitbis1_5: 'On a regroupé le temps (en heures) des £e dernières courses d’un cycliste dans le tableau ci-dessous&nbsp;:',
  consigne2_1: 'Quelle est la moyenne des valeurs de ce caractère&nbsp;?',
  consignebis2_1: 'Donner une bonne approximation de la moyenne des valeurs de ce caractère.',
  consigne2_2: 'Quelle est le nombre d’absences moyen de ces élèves&nbsp;?',
  consignebis2_2: 'Donner une bonne approximation de la durée moyenne des retards de ces élèves.',
  consigne2_3: 'Quelle est la moyenne des pointures des clients de ce magasin&nbsp;?',
  consignebis2_3: 'Donner une bonne approximation de la longueur moyenne de tissu acheté par ces clients.',
  consigne2_4: 'Quelle est le nombre moyen de jours de pluie de ces différentes villes&nbsp;?',
  consignebis2_4: 'Donner une bonne approximation de la quantité moyenne de pluie tombée sur cette période.',
  consigne2_5: 'Quelle est le nombre moyen de photos retouchées dans les exemplaires de ce magazine&nbsp;?',
  consignebis2_5: 'Donner une bonne approximation du prix moyen des gâteaux de cette boulangerie.',
  consigne3_1: 'Quelle est l’écart-type des valeurs de ce caractère&nbsp;?',
  consignebis3_1: 'Donner une bonne approximation de l’écart-type des valeurs de ce caractère.',
  consigne3_2: 'Quelle est l’écart-type du nombre d’absences de ces élèves&nbsp;?',
  consignebis3_2: 'Donner une bonne approximation de l’écart-type de la durée des retards de ces élèves.',
  consigne3_3: 'Quelle est l’écart-type des pointures des clients de ce magasin&nbsp;?',
  consignebis3_3: 'Donner une bonne approximation de l’écart-type de la longueur de tissu acheté par ces clients.',
  consigne3_4: 'Quelle est l’écart-type du nombre de jours de pluie de ces différentes villes&nbsp;?',
  consignebis3_4: 'Donner une bonne approximation de l’écart-type de la quantité moyenne de pluie tombée sur cette période.',
  consigne3_5: 'Quelle est l’écart-type du nombre de photos retouchées dans les exemplaires de ce magazine&nbsp;?',
  consignebis3_5: 'Donner une bonne approximation de l’écart-type du prix des gâteaux de cette boulangerie.',
  consignePetit2_1: 'Quelle est la moyenne des valeurs de ce caractère&nbsp;?',
  consignePetit2_2: 'Quelle est le nombre moyen de poissons pêchés au cours de ces jours de pêche&nbsp;?',
  consignePetit2_3: 'Quelle est le nombre moyen d’essais marqués par cette équipe au cours de ces matchs&nbsp;?',
  consignePetit2_4: 'Quelle est la moyenne des notes de cet élève sur ces devoirs&nbsp;?',
  consignePetit2_5: 'Quelle est le nombre moyen d’absents dans cette classe&nbsp;?',
  consignePetitbis2_1: 'Donner une bonne approximation de la moyenne des valeurs de ce caractère.',
  consignePetitbis2_2: 'Donner une bonne approximation du temps moyen passé devant la télévision par ces jeunes.',
  consignePetitbis2_3: 'Donner une bonne approximation du prix moyen d’un repas pris par cette personne au restaurant',
  consignePetitbis2_4: 'Donner une bonne approximation de la quantité moyenne de pluie tombée au cours de ces semaines.',
  consignePetitbis2_5: 'Donner une bonne approximation de la durée moyenne des courses de ce cycliste.',
  consignePetit3_1: 'Quelle est la moyenne des valeurs de ce caractère&nbsp;?',
  consignePetit3_2: 'Quelle est l’écart-type du nombre de poissons pêchés au cours de ces jours de pêche&nbsp;?',
  consignePetit3_3: 'Quelle est l’écart-type du nombre d’essais marqués par cette équipe au cours de ces matchs&nbsp;?',
  consignePetit3_4: 'Quelle est l’écart-type des notes de cet élève sur ces devoirs&nbsp;?',
  consignePetit3_5: 'Quelle est l’écart-type du nombre d’absents dans cette classe&nbsp;?',
  consignePetitbis3_1: 'Donner une bonne approximation de l’écart-type des valeurs de ce caractère.',
  consignePetitbis3_2: 'Donner une bonne approximation de l’écart-type du temps passé devant la télévision par ces jeunes.',
  consignePetitbis3_3: 'Donner une bonne approximation de l’écart-type du prix d’un repas pris par cette personne au restaurant',
  consignePetitbis3_4: 'Donner une bonne approximation de l’écart-type de la quantité de pluie tombée au cours de ces semaines.',
  consignePetitbis3_5: 'Donner une bonne approximation de l’écart-type de la durée des courses de ce cycliste.',
  consigne4_1: 'Moyenne : &1&.',
  consigne4_2: 'Ecart-type : &1&.',
  consigne4_3: 'Etendue : &1&',
  consigne5: 'Arrondir à 0,1.',
  // Ajout des consignes sur l’étendue
  consigne6_1: 'Quelle est l’étendue des valeurs de ce caractère&nbsp;?',
  consigne6_2: 'Quelle est l’étendue du nombre d’absences de ces élèves&nbsp;?',
  consigne6_3: 'Quelle est l’étendue des pointures des clients de ce magasin&nbsp;?',
  consigne6_4: 'Quelle est l’étendue du nombre de jours de pluie de ces différentes villes&nbsp;?',
  consigne6_5: 'Quelle est l’étendue du nombre de photos retouchées dans les exemplaires de ce magazine&nbsp;?',
  consignebis6_1: 'Quelle est l’étendue des valeurs de ce caractère&nbsp;?',
  consignebis6_2: 'Quelle est l’étendue de la durée des retards de ces élèves&nbsp;?',
  consignebis6_3: 'Quelle est l’étendue de la longueur de tissu acheté par ces clients&nbsp;?',
  consignebis6_4: 'Quelle est l’étendue de la quantité de pluie tombée sur cette période&nbsp;?',
  consignebis6_5: 'Quelle est l’étendue du prix des gâteaux de cette boulangerie&nbsp;?',
  consignePetit6_1: 'Quelle est l’étendue des valeurs de ce caractère&nbsp;?',
  consignePetit6_2: 'Quelle est l’étendue du nombre de poissons pêchés sur ces jours de pêche&nbsp;?',
  consignePetit6_3: 'Quelle est l’étendue du nombre d’essais marqués au cours de ces matchs&nbsp;?',
  consignePetit6_4: 'Quelle est l’étendue des notes de cet élève&nbsp;?',
  consignePetit6_5: 'Quelle est l’étendue du nombre d’absents de cette classe&nbsp;?',
  consignePetitbis6_1: 'Quelle est l’étendue des valeurs de ce caractère&nbsp;?',
  consignePetitbis6_2: 'Quelle est l’étendue du temps passé (en heures) devant la télévision par ces jeunes&nbsp;?',
  consignePetitbis6_3: 'Quelle est l’étendue du prix payé par cet personne pour manger au restaurant&nbsp;?',
  consignePetitbis6_4: 'Quelle est l’étendue de la quantité de pluie tombée au cours de ces semaines&nbsp;?',
  consignePetitbis6_5: 'Quelle est l’étendue des temps de cours de ce cycliste&nbsp;?',
  tableau1_1: 'Valeurs',
  tableauPetit1_1: 'Valeurs',
  tableaubis1_1: 'Valeurs',
  tableauPetitbis1_1: 'Valeurs',
  tableau1_2: 'Nombre<br/>d’absences',
  tableauPetit1_2: 'Nombre<br/>de poissons',
  tableaubis1_2: 'Durée<br/>des retards',
  tableauPetitbis1_2: 'Temps<br/>passé',
  tableau1_3: 'Pointure<br/>des chaussures',
  tableauPetit1_3: 'Nombre<br/>d’essais',
  tableaubis1_3: 'Longueur<br/>de tissu',
  tableauPetitbis1_3: 'Prix<br/>payé',
  tableau1_4: 'Nombre<br/>de jours',
  tableauPetit1_4: 'Note',
  tableaubis1_4: 'Quantité<br/>de pluie',
  tableauPetitbis1_4: 'Quantité<br/>de pluie',
  tableau1_5: 'Nombre de photos<br/>retouchées',
  tableauPetit1_5: 'Nombre<br/>d’absents',
  tableaubis1_5: 'Prix des<br/>gâteaux',
  tableauPetitbis1_5: 'Temps',
  tableau2: 'Effectifs',
  tableau3: 'Centres',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Peut-être est-ce un problème d’arrondi&nbsp;!',
  comment2: 'As-tu pris la bonne valeur sur ta calculatrice&nbsp;?',
  // et les phrases utiles pour les explications de la réponse
  corr0_1: 'Pour calculer la moyenne, on remplace chaque classe par son centre.',
  corr0_2: 'Pour calculer l’écart-type, on remplace chaque classe par son centre.',
  corr1_1: 'Le calcul de la moyenne peut s’effectuer par $£c£e£{r2}$ mais cela peut directement être donné par la calculatrice&nbsp;:',
  corr1_2: 'Le calcul de l’écart-type se fait à la calculatrice (comme pour la moyenne)&nbsp;:',
  corr1_3: 'La valeur maximale de la série vaut £{max} et la valeur minimale £{min}.<br/>L’étendue vaut alors $£{max}-£{min}=£{r2}$.',
  corrPetit1: 'Le calcul de la moyenne s’effectue par $£c$.',
  corrPetit2: 'On obtient $\\overline{x}£s£{r1}$ et la moyenne vaut £e£{r2}.',
  corr2_1: '- dans la partie "Statistiques", on saisit les données : les valeurs dans la liste 1 et les effectifs dans la liste 2&nbsp;;',
  corr2_2: '- dans la partie "Statistiques", on saisit les données : les valeurs (ou les centres ici) dans la liste 1 et les effectifs dans la liste 2&nbsp;;',
  corr3: '- on paramètre ensuite la calculatrice pour que les valeurs prises en compte pour le calcul soient celles de la liste 1 et les effectifs celles de la liste 2&nbsp;;',
  corr4: "- il ne reste plus qu'à demander à la calculatrice de faire les calculs&nbsp;;",
  corr5: '- la moyenne correspond alors à la variable $\\overline{x}$.',
  corr6: 'Dans cet exemple, on obtient $\\overline{x}£s£{r1}$ et la moyenne vaut £e£{r2}.',
  corr7: '- l’écart-type correspond (suivant les modèles) à $\\sigma X$ ou $x\\sigma n$.',
  corr8: 'Dans cet exemple, on obtient $\\sigma£s£{r1}$ et l’écart-type vaut £e£{r2}.',
  corr10: 'environ '
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    if ((stor.typeCaractere === 'continu') && (stor.maQuestion !== 'etendue')) {
      stor.zoneexplication0 = j3pAddElt(explications, 'div', '')
      const laConsigne0 = (stor.maQuestion === 'moyenne') ? textes.corr0_1 : textes.corr0_2
      j3pAffiche(stor.zoneexplication0, '', laConsigne0)
    }
    for (let i = 1; i <= 7; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div')
    const environSigne = (stor.elt.inputmqList[0].typeReponse[1] === 'arrondi') ? '\\approx' : '='
    const environ = (stor.elt.inputmqList[0].typeReponse[1] === 'arrondi') ? textes.corr10 : ''
    const valArrondie = (stor.elt.inputmqList[0].typeReponse[1] === 'arrondi') ? j3pVirgule(Math.round(10 * stor.elt.inputmqList[0].reponse[0]) / 10) : j3pVirgule(stor.elt.inputmqList[0].reponse[0])
    const valArrondie2 = (stor.elt.inputmqList[0].typeReponse[1] === 'arrondi') ? j3pVirgule(Math.round(1000 * stor.elt.inputmqList[0].reponse[0]) / 1000) : j3pVirgule(stor.elt.inputmqList[0].reponse[0])
    if (stor.maQuestion === 'moyenne') {
      if (ds.petitEffectif) {
        j3pAffiche(stor.zoneexplication1, '', textes.corrPetit1, {
          c: stor.texteMoyenne,
          e: environSigne,
          r2: valArrondie
        })
        j3pAffiche(stor.zoneexplication2, '', textes.corrPetit2, {
          r1: valArrondie2,
          s: environSigne,
          r2: valArrondie,
          e: environ
        })
        const tabOverline = document.querySelectorAll('.mq-overline')
        tabOverline.forEach(elt => {
          const laCouleur = elt.style.color
          j3pStyle(elt, { borderTop: '1px solid ' + laCouleur })
        })
      } else {
        j3pAffiche(stor.zoneexplication1, '', textes.corr1_1, {
          c: stor.texteMoyenne,
          e: environSigne,
          r2: valArrondie
        })
      }
    } else if (stor.maQuestion === 'ecart type') {
      j3pAffiche(stor.zoneexplication1, '', textes.corr1_2)
    } else {
      j3pAffiche(stor.zoneexplication1, '', textes.corr1_3, {
        min: j3pVirgule(stor.minVal),
        max: j3pVirgule(stor.maxVal),
        r2: j3pVirgule(stor.elt.inputmqList[0].reponse[0])
      })
    }
    if (stor.maQuestion !== 'etendue') {
      if ((stor.maQuestion === 'ecart type') || !ds.petitEffectif) {
        if (stor.typeCaractere === 'discret') j3pAffiche(stor.zoneexplication2, '', textes.corr2_1)
        else j3pAffiche(stor.zoneexplication2, '', textes.corr2_2)
        for (let i = 3; i <= 4; i++) j3pAffiche(stor['zoneexplication' + i], '', textes['corr' + i])
      }
      if ((stor.maQuestion === 'moyenne') && !ds.petitEffectif) {
        j3pAffiche(stor.zoneexplication5, '', textes.corr5)
        j3pAffiche(stor.zoneexplication6, '', textes.corr6,
          {
            e: environ,
            s: (environ === '') ? '=' : '\\approx',
            r1: valArrondie2,
            r2: valArrondie
          })
        const tabOverline = document.querySelectorAll('.mq-overline')
        tabOverline.forEach(elt => {
          const laCouleur = elt.style.color
          j3pStyle(elt, { borderTop: '1px solid ' + laCouleur })
        })
      } else if (stor.maQuestion === 'ecart type') {
        j3pAffiche(stor.zoneexplication5, '', textes.corr7)
        j3pAffiche(stor.zoneexplication6, '', textes.corr8,
          {
            e: environ,
            s: (environ === '') ? '=' : '\\approx',
            r1: valArrondie2,
            r2: valArrondie
          })
      }
      if (stor.typeCaractere === 'continu') {
        // je reconstruis le tableau avec les centres :
        j3pEmpty(stor.objValeurs.parent)
        stor.objValeurs.textes.push(textes.tableau3)
        stor.objValeurs.centreCouleur = explications.style.color
        tableauDonneesEffectifs(stor.objValeurs)
      }
    }
  }
  const captureIntervalle = /^([[\]])(-?[0-9,.]+);(-?[0-9,.]+)([[\]])$/ // cela me servira à mettre une virgule si l’une des bornes est un décimal
  function tableauDonneesEffectifs (mesDonnees) {
    // mesDonnees est un objet
    // mesDonnees.parent est le nom du div dans lequel est créé le tableau
    // mesDonnees.largeur est la largeur du tableau (utile pour configurer la largeur de chaque colonne)
    // mesDonnees.textes est un tableau de 3 éléments max : "Valeurs", Effectifs", "E.C.C" ou des équivalents à saisir
    // mesDonnees.valeurs : tableau des valeurs
    // mesDonnees.effectifs : tableaux des effectifs
    // mesDonnees.centres : tableaux des centres des classes quand nous sommes dans le cas d’un caractère quantitatif continu
    me.logIfDebug('mesDonnees:', mesDonnees)
    function creerLigneTab (elt, valeurs, l1, l2) {
      // elt est l’élément html dans lequel créer la ligne
      // l1 et l2 sont respectivement la largeur de la première colonne et des suivantes
      const ligne = j3pAddElt(elt, 'tr')
      if (valeurs[0] === 'Centres') j3pStyle(ligne, { color: mesDonnees.centreCouleur })
      j3pAddElt(ligne, 'td', valeurs[0], { width: l1 + 'px' })
      for (let i = 1; i < valeurs.length; i++) {
        if ((valeurs[i] === 0) || valeurs[i]) { // c’est pour éviter le undefined (mais 0, je le garde)
          const chunk = captureIntervalle.exec(valeurs[i])
          // Je ne sais pas si dans les bornes de l’intervalle je peux avoir des décimaux, mais au cas où...
          if (chunk)j3pAddElt(ligne, 'td', valeurs[i].replace(chunk[0], chunk[1] + j3pVirgule(chunk[2]) + ';' + j3pVirgule(chunk[3]) + chunk[4]), { align: 'center', width: l2 + 'px' })
          else j3pAddElt(ligne, 'td', j3pVirgule(valeurs[i]), { align: 'center', width: l2 + 'px' })
        }
      }
    }
    const leTableau = j3pAddElt(mesDonnees.parent, 'div')
    leTableau.setAttribute('width', mesDonnees.largeur + 'px')
    if (mesDonnees.valeurs.length === mesDonnees.effectifs.length) {
      let largeurTexte = 0
      for (let i = 0; i < mesDonnees.textes.length; i++) {
        // largeurTexte = Math.max(largeurTexte, mesDonnees.textes[i].textWidth(me.styles.petit.enonce.fontFamily, me.styles.petit.enonce.fontSize.substring(0, me.styles.petit.enonce.fontSize.length - 2)))
        const tabBR = ['<BR>', '</BR>', '<BR/>', '<br>', '<br>']
        for (let k = 0; k < tabBR.length; k++) {
          if (mesDonnees.textes[i].includes(tabBR[k])) {
            mesDonnees.textes[i] = mesDonnees.textes[i].replace(tabBR[k], '<br/>')
          }
        }
        const tabDonneesTexte = mesDonnees.textes[i].split('<br/>')
        for (let j = 0; j < tabDonneesTexte.length; j++) {
          largeurTexte = Math.max(largeurTexte, $(leTableau).textWidth(tabDonneesTexte[j], me.styles.petit.enonce.fontFamily, me.styles.petit.enonce.fontSize))
        }
      }
      largeurTexte += 10
      const largeurColonne = (mesDonnees.largeur - largeurTexte - 5) / mesDonnees.effectifs.length
      const table = j3pAddElt(leTableau, 'table', { width: '100%', borderColor: 'black', border: 2, cellSpacing: 0 })
      creerLigneTab(table, [mesDonnees.textes[0]].concat(mesDonnees.valeurs), largeurTexte, largeurColonne)
      creerLigneTab(table, [mesDonnees.textes[1]].concat(mesDonnees.effectifs), largeurTexte, largeurColonne)
      const tabECC = [mesDonnees.effectifs[0]]// tableau qui va récupérer les E.C.C.
      for (let i = 1; i < mesDonnees.effectifs.length; i++) {
        tabECC.push(j3pArrondi(tabECC[i - 1] + mesDonnees.effectifs[i], 4))
      }
      if (mesDonnees.textes[2] === textes.tableau3) {
        creerLigneTab(table, [mesDonnees.textes[2]].concat(mesDonnees.centres), largeurTexte, largeurColonne)
      }
      // on renvoie un objet contenant le nom des zones de saisie (lorsqu’elles existent) et le tableau des effectifs cumulés croissants
      return { tabECC, largeurCol1: largeurTexte, largeurAutreCol: largeurColonne }
    }
    j3pShowError('pb sur le nombre de valeurs dans les deux tableaux', { vanishAfter: 5 })
  }

  function enonceMain () {
    // A chaque répétition, je renouvelle les données
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      stor.nbValeurs = (ds.petitEffectif) ? j3pGetRandomInt(4, 5) : j3pGetRandomInt(6, 9)// valable aussi bien dans un cas discret que continu
      stor.mesValeurs = []

      // Je génère des valeurs correspondant à un contexte particulier
      if (stor.tabChoixSujet.length === 0) {
        stor.tabChoixSujet = [...stor.tabChoixSujetInit]
      }
      const alea = Math.floor(j3pGetRandomInt(0, (stor.tabChoixSujet.length - 1)))
      const choixSujet = stor.tabChoixSujet[alea]
      stor.tabChoixSujet.splice(alea, 1)
      stor.choixSujet = choixSujet
      let facteur, numAjout2
      switch (choixSujet) {
        case 1:
          // Pas de contexte
          if (ds.petitEffectif) {
            stor.mesValeurs.push(j3pGetRandomInt(1, 2))
            for (let i = 1; i < stor.nbValeurs; i++) {
              stor.mesValeurs.push(stor.mesValeurs[i - 1] + j3pGetRandomInt(1, 2))
            }
          } else {
            facteur = j3pRandomTab([2, 5, 10, 20], [0.25, 0.25, 0.25, 0.25])
            stor.mesValeurs.push(j3pGetRandomInt(1, 3) * facteur)
            for (let i = 1; i < stor.nbValeurs; i++) {
              stor.mesValeurs.push(stor.mesValeurs[i - 1] + j3pRandomTab([1, 2], [0.7, 0.3]) * facteur)
            }
          }
          break
        case 2:
          if (ds.petitEffectif) {
            stor.mesValeurs.push(0)
            for (let i = 1; i < stor.nbValeurs; i++) {
              stor.mesValeurs.push(stor.mesValeurs[i - 1] + 1)
            }
          } else {
            if (stor.typeCaractere === 'discret') {
              // nombre d’absences
              stor.mesValeurs.push(1)
              for (let i = 1; i < stor.nbValeurs; i++) {
                stor.mesValeurs.push(stor.mesValeurs[i - 1] + j3pRandomTab([1, 2], [0.8, 0.2]))
              }
            } else {
              facteur = j3pRandomTab([5, 10], [0.5, 0.5])
              stor.mesValeurs.push(0)
              for (let i = 1; i < stor.nbValeurs; i++) {
                stor.mesValeurs.push(stor.mesValeurs[i - 1] + j3pRandomTab([1, 2], [0.7, 0.3]) * facteur)
              }
            }
          }
          break
        case 3:
          if (ds.petitEffectif) {
            if (stor.typeCaractere === 'discret') {
              stor.mesValeurs.push(2)
              numAjout2 = j3pGetRandomInt(2, 4)
              for (let i = 1; i < stor.nbValeurs; i++) {
                if (i === numAjout2) {
                  stor.mesValeurs.push(stor.mesValeurs[i - 1] + 2)
                } else {
                  stor.mesValeurs.push(stor.mesValeurs[i - 1] + 1)
                }
              }
            } else {
              stor.mesValeurs.push(12)
              for (let i = 1; i < stor.nbValeurs; i++) stor.mesValeurs.push(stor.mesValeurs[i - 1] + 4)
            }
          } else {
            if (stor.typeCaractere === 'discret') {
              stor.mesValeurs.push(36)
              for (let i = 1; i < stor.nbValeurs; i++) stor.mesValeurs.push(stor.mesValeurs[i - 1] + j3pRandomTab([1, 2], [0.9, 0.1]))
            } else {
              stor.mesValeurs.push(0)
              stor.mesValeurs.push(2)
              for (let i = 2; i < stor.nbValeurs; i++) stor.mesValeurs.push(stor.mesValeurs[i - 1] + j3pRandomTab([1, 2], [0.8, 0.2]))
            }
          }
          break
        case 4:
          if (ds.petitEffectif) {
            if (stor.typeCaractere === 'discret') {
              stor.mesValeurs.push(13)
              numAjout2 = j3pGetRandomInt(2, 4)
              for (let i = 1; i < stor.nbValeurs; i++) {
                if (i === numAjout2) {
                  stor.mesValeurs.push(stor.mesValeurs[i - 1] + 2)
                } else {
                  stor.mesValeurs.push(stor.mesValeurs[i - 1] + 1)
                }
              }
            } else {
              stor.mesValeurs.push(0)
              for (let i = 1; i < stor.nbValeurs; i++) stor.mesValeurs.push(stor.mesValeurs[i - 1] + 5)
            }
          } else {
            if (stor.typeCaractere === 'discret') {
              stor.mesValeurs.push(30)
              for (let i = 1; i < stor.nbValeurs; i++) stor.mesValeurs.push(stor.mesValeurs[i - 1] + j3pRandomTab([1, 2, 3, 4], [0.25, 0.25, 0.25, 0.25]))
            } else {
              stor.mesValeurs.push(0)
              stor.mesValeurs.push(10)
              for (let i = 2; i < stor.nbValeurs; i++) stor.mesValeurs.push(stor.mesValeurs[i - 1] + j3pRandomTab([1, 2], [0.8, 0.2]) * 5)
            }
          }
          break
        default:
          if (ds.petitEffectif) {
            if (stor.typeCaractere === 'discret') {
              stor.mesValeurs.push(0)
              for (let i = 1; i < stor.nbValeurs; i++) stor.mesValeurs.push(stor.mesValeurs[i - 1] + 1)
            } else {
              stor.mesValeurs.push(1)
              for (let i = 1; i < stor.nbValeurs; i++) stor.mesValeurs.push(stor.mesValeurs[i - 1] + 0.5)
            }
          } else {
            if (stor.typeCaractere === 'discret') {
              stor.mesValeurs.push(10)
              for (let i = 1; i < stor.nbValeurs; i++) stor.mesValeurs.push(stor.mesValeurs[i - 1] + j3pRandomTab([1, 2, 3, 4], [0.25, 0.25, 0.25, 0.25]))
            } else {
              stor.mesValeurs.push(10)
              stor.mesValeurs.push(15)
              for (let i = 2; i < stor.nbValeurs; i++) stor.mesValeurs.push(stor.mesValeurs[i - 1] + 2)
            }
          }
          break
      }
      me.logIfDebug('stor.mesValeurs:', stor.mesValeurs)
      if (stor.typeCaractere === 'continu') {
        // il faut que j’ajoute une valeur (borne supérieure du dernier intervalle)
        // stor.mesValeurs.push(stor.mesValeurs[stor.mesValeurs.length-1]+j3pRandomTab([1,2],[0.7,0.3])*facteur)
        stor.mesIntervalles = []
        for (let i = 0; i < stor.nbValeurs - 1; i++) stor.mesIntervalles.push('[' + j3pVirgule(stor.mesValeurs[i]) + ';' + j3pVirgule(stor.mesValeurs[i + 1]) + '[')
        switch (choixSujet) {
          case 1:
            stor.borneSupIntervalles = (ds.petitEffectif)
              ? stor.mesValeurs[stor.mesValeurs.length - 1] + j3pGetRandomInt(1, 2)
              : (stor.mesValeurs[stor.mesValeurs.length - 1] + j3pRandomTab([1, 2], [0.7, 0.3]) * facteur)
            break
          case 2:
            // durée des retards
            stor.borneSupIntervalles = (ds.petitEffectif)
              ? stor.mesValeurs[stor.mesValeurs.length - 1] + 1
              : (stor.mesValeurs[stor.mesValeurs.length - 1] + j3pRandomTab([1, 2], [0.7, 0.3]) * facteur)
            break
          case 3:
            // longueur de tissu
            stor.borneSupIntervalles = (ds.petitEffectif)
              ? stor.mesValeurs[stor.mesValeurs.length - 1] + 4
              : (stor.mesValeurs[stor.mesValeurs.length - 1] + 2)
            break
          case 4:
            // quantité de pluie
            stor.borneSupIntervalles = (ds.petitEffectif)
              ? stor.mesValeurs[stor.mesValeurs.length - 1] + 5
              : (stor.mesValeurs[stor.mesValeurs.length - 1] + 10)
            break
          default :
            // prix des gâteaux
            stor.borneSupIntervalles = (ds.petitEffectif)
              ? stor.mesValeurs[stor.mesValeurs.length - 1] + 0.5
              : (stor.mesValeurs[stor.mesValeurs.length - 1] + 5)
            break
        }
        stor.mesIntervalles.push('[' + j3pVirgule(stor.mesValeurs[stor.mesValeurs.length - 1]) + ';' + j3pVirgule(stor.borneSupIntervalles) + '[')
      }
      // maintenant je dois gérer mes effectifs
      stor.mesEffectifs = []
      stor.effTotal = 0
      let nbUn = 0
      let // uniquement pour les petits effectifs
        valEgales = true
      for (let i = 0; i < stor.mesValeurs.length; i++) {
        let newEff = j3pGetRandomInt(2, 11)
        if (ds.petitEffectif) {
          if (nbUn === 2) {
            newEff = j3pGetRandomInt(2, 3)
          } else {
            newEff = j3pGetRandomInt(1, 3)
            if (newEff === 1) nbUn++
          }
          if ((i > 1) && (newEff !== stor.mesEffectifs[i - 1])) valEgales = false
          if (valEgales && (i === stor.mesValeurs.length - 1)) {
            do {
              newEff = j3pGetRandomInt(2, 3)
            } while (newEff === stor.mesEffectifs[i - 1])
          }
        }
        stor.effTotal += newEff
        stor.mesEffectifs.push(newEff)
      }
    }
    me.logIfDebug('mes effectifs :', stor.mesEffectifs, '   et l’effectif total:', stor.effTotal)

    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )// Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 8; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    const laConsigne1 = (ds.petitEffectif)
      ? (stor.typeCaractere === 'discret') ? textes['consignePetit1_' + stor.choixSujet] : textes['consignePetitbis1_' + stor.choixSujet]
      : (stor.typeCaractere === 'discret')
          ? textes['consigne1_' + stor.choixSujet]
          : textes['consignebis1_' + stor.choixSujet]
    j3pAffiche(stor.zoneCons1, '', laConsigne1, { e: stor.effTotal })

    stor.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
    const typeVal = (ds.petitEffectif)
      ? (stor.typeCaractere === 'discret')
          ? textes['tableauPetit1_' + stor.choixSujet]
          : textes['tableauPetitbis1_' + stor.choixSujet]
      : (stor.typeCaractere === 'discret')
          ? textes['tableau1_' + stor.choixSujet]
          : textes['tableaubis1_' + stor.choixSujet]
    const objValeurs = {
      parent: stor.zoneCons2,
      textes: [typeVal, textes.tableau2],
      valeurs: stor.mesValeurs,
      effectifs: stor.mesEffectifs,
      largeur: stor.largeurTab
    }
    if (stor.typeCaractere === 'continu') {
      me.logIfDebug('stor.mesIntervalles:', stor.mesIntervalles)
      objValeurs.valeurs = stor.mesIntervalles
    }
    stor.objmoyenneEcartType = tableauDonneesEffectifs(objValeurs)
    stor.maQuestion = stor.tabQuestions[me.questionCourante - 1]
    let sommeVal, moyenneVal, ecartTypeVal, ecartTypeVal2, texteMoyenne, numTxt
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      if (stor.typeCaractere === 'discret') {
        // calcul de la moyenne :
        sommeVal = 0
        for (let i = 0; i < stor.mesValeurs.length; i++) sommeVal += stor.mesValeurs[i] * stor.mesEffectifs[i]
        moyenneVal = sommeVal / stor.effTotal
        if (ds.petitEffectif) {
          // l’effectif étant petit, on écrit tout le calcul de la moyenne
          numTxt = stor.mesValeurs[0] + '\\times ' + stor.mesEffectifs[0]
          for (let i = 1; i < stor.mesValeurs.length; i++) numTxt += '+' + stor.mesValeurs[i] + '\\times ' + stor.mesEffectifs[i]
          texteMoyenne = '\\frac{' + numTxt + '}{' + stor.effTotal + '}'
        } else {
          texteMoyenne = '\\frac{' + stor.mesValeurs[0] + '\\times ' + stor.mesEffectifs[0] + '+' + stor.mesValeurs[1] + '\\times ' + stor.mesEffectifs[1] + '+\\ldots+' + stor.mesValeurs[stor.mesValeurs.length - 1] + '\\times ' + stor.mesEffectifs[stor.mesValeurs.length - 1] + '}{' + stor.effTotal + '}'
        }
        // calcul de l’écart-type
        sommeVal = 0
        for (let i = 0; i < stor.mesValeurs.length; i++) sommeVal += Math.pow(stor.mesValeurs[i], 2) * stor.mesEffectifs[i]
        ecartTypeVal = Math.sqrt(sommeVal / stor.effTotal - Math.pow(moyenneVal, 2))
        ecartTypeVal2 = Math.sqrt(stor.effTotal / (stor.effTotal - 1)) * ecartTypeVal
      } else {
        // caractère continu
        stor.centres = []
        for (let i = 0; i < stor.mesValeurs.length - 1; i++) stor.centres.push((stor.mesValeurs[i] + stor.mesValeurs[i + 1]) / 2)
        stor.centres.push((stor.mesValeurs[stor.mesValeurs.length - 1] + stor.borneSupIntervalles) / 2)
        sommeVal = 0
        for (let i = 0; i < stor.centres.length; i++) sommeVal += stor.centres[i] * stor.mesEffectifs[i]
        moyenneVal = sommeVal / stor.effTotal
        if (ds.petitEffectif) {
          // l’effectif étant petit, on écrit tout le calcul de la moyenne
          numTxt = stor.centres[0] + '\\times ' + stor.mesEffectifs[0]
          for (let i = 1; i < stor.centres.length; i++) numTxt += '+' + stor.centres[i] + '\\times ' + stor.mesEffectifs[i]
          texteMoyenne = '\\frac{' + numTxt + '}{' + stor.effTotal + '}'
        } else {
          texteMoyenne = '\\frac{' + stor.centres[0] + '\\times ' + stor.mesEffectifs[0] + '+' + stor.centres[1] + '\\times ' + stor.mesEffectifs[1] + '+\\ldots+ ' + stor.centres[stor.centres.length - 1] + '\\times ' + stor.mesEffectifs[stor.centres.length - 1] + '}{' + stor.effTotal + '}'
        }
        // calcul de l’écart-type
        sommeVal = 0
        for (let i = 0; i < stor.centres.length; i++) sommeVal += Math.pow(stor.centres[i], 2) * stor.mesEffectifs[i]
        ecartTypeVal = Math.sqrt(sommeVal / stor.effTotal - Math.pow(moyenneVal, 2))
        ecartTypeVal2 = Math.sqrt(stor.effTotal / (stor.effTotal - 1)) * ecartTypeVal
        objValeurs.centres = stor.centres
      }
      stor.minVal = stor.mesValeurs[0]
      stor.maxVal = (stor.typeCaractere === 'discret')
        ? stor.mesValeurs[stor.mesValeurs.length - 1]
        : stor.borneSupIntervalles
      stor.etendueVal = Math.round((stor.maxVal - stor.minVal) * 1000) / 1000
      // on récupère les résultats pour les avoir sur les deux étapes sans avoir à les calculer une deuxième fois
      stor.moyenneVal = moyenneVal
      stor.ecartTypeVal = ecartTypeVal
      stor.ecartTypeVal2 = ecartTypeVal2
      stor.texteMoyenne = texteMoyenne
      me.logIfDebug('moyenne:', moyenneVal, '  écart-type:', ecartTypeVal, '   écart-type{n-1}:', ecartTypeVal2, '   min:', stor.minVal, '   max:', stor.maxVal)
      stor.objValeurs = objValeurs
    }
    // Dans objValeurs, j’ai des choses qui sont recrées à chaque question, donc il faut que je les récupère pour les remettre dans stor.objValeurs
    for (const prop in objValeurs) stor.objValeurs[prop] = objValeurs[prop]

    const laConsigne2 = (ds.petitEffectif)
      ? (stor.maQuestion === 'moyenne')
          ? (stor.typeCaractere === 'discret') ? textes['consignePetit2_' + stor.choixSujet] : textes['consignePetitbis2_' + stor.choixSujet]
          : (stor.maQuestion === 'ecart type')
              ? (stor.typeCaractere === 'discret') ? textes['consignePetit3_' + stor.choixSujet] : textes['consignePetitbis3_' + stor.choixSujet]
              : (stor.typeCaractere === 'discret') ? textes['consignePetit6_' + stor.choixSujet] : textes['consignePetitbis6_' + stor.choixSujet]
      : (stor.maQuestion === 'moyenne')
          ? (stor.typeCaractere === 'discret') ? textes['consigne2_' + stor.choixSujet] : textes['consignebis2_' + stor.choixSujet]
          : (stor.maQuestion === 'ecart type')
              ? (stor.typeCaractere === 'discret') ? textes['consigne3_' + stor.choixSujet] : textes['consignebis3_' + stor.choixSujet]
              : (stor.typeCaractere === 'discret') ? textes['consigne6_' + stor.choixSujet] : textes['consignebis6_' + stor.choixSujet]
    j3pAffiche(stor.zoneCons3, '', laConsigne2)
    const laConsigne3 = (stor.maQuestion === 'moyenne')
      ? textes.consigne4_1
      : (stor.maQuestion === 'ecart type')
          ? textes.consigne4_2
          : textes.consigne4_3
    stor.elt = j3pAffiche(stor.zoneCons4, '', laConsigne3,
      { inputmq1: { texte: '', dynamique: true, maxchars: 6, width: '12px' } })
    stor.repQuest = (stor.maQuestion === 'moyenne')
      ? stor.moyenneVal
      : (stor.maQuestion === 'ecart type') ? stor.ecartTypeVal : stor.etendueVal
    stor.elt.inputmqList[0].typeReponse = (Math.abs(stor.repQuest * 1000 - Math.round(stor.repQuest * 1000)) < Math.pow(10, -12)) ? ['nombre', 'exact'] : ['nombre', 'arrondi', 0.1]
    stor.elt.inputmqList[0].reponse = [stor.repQuest]
    if (stor.elt.inputmqList[0].typeReponse[1] === 'arrondi') {
      j3pAffiche(stor.zoneCons5, '', textes.consigne5)
    }
    me.logIfDebug('réponse :', stor.elt.inputmqList[0].reponse)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.zoneD, 'div', '')
    if (stor.maQuestion === 'moyenne') {
      me.fenetresjq = [{
        name: 'Calculatrice',
        title: 'Calculatrice'
      }]//, left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
      j3pAfficheCroixFenetres('Calculatrice')
    }
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '')

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.elt.inputmqList[0].id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    mqRestriction(stor.elt.inputmqList[0], '\\d,.')
    j3pFocus(stor.elt.inputmqList[0])
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70

        me.construitStructurePage({ structure: 'presentation1', ratioGauche: stor.pourc / 100 })
        const leTitre = (ds.typeQuest === 'moyenne')
          ? textes.titre_exo1
          : (ds.typeQuest === 'ecart type')
              ? textes.titre_exo2
              : (ds.typeQuest === 'etendue')
                  ? textes.titre_exo3
                  : (ds.typeQuest === 'moyenne+ecart type')
                      ? textes.titre_exo4
                      : (ds.typeQuest === 'moyenne+etendue')
                          ? textes.titre_exo5
                          : (ds.typeQuest === 'ecart type+etendue') ? textes.titre_exo6 : textes.titre_exo7
        me.afficheTitre(leTitre)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.typeCaractere = (ds.typeCaractere.toLowerCase().indexOf('continu') > -1) ? 'continu' : 'discret'
        if (ds.typeQuest === 'les deux') {
          ds.nbetapes = 2
        }
        const listeQuest = ['moyenne', 'ecart type', 'etendue']
        ds.nbetapes = (ds.typeQuest === 'les trois')
          ? 3
          : (listeQuest.indexOf(ds.typeQuest) === -1) ? 2 : 1
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        stor.tabQuestions = []
        for (let j = 1; j <= ds.nbrepetitions; j++) {
          switch (ds.typeQuest) {
            case 'les trois':
              for (let i = 0; i < listeQuest.length; i++) stor.tabQuestions.push(listeQuest[i])
              break
            case 'moyenne+ecart type':
              stor.tabQuestions.push('moyenne', 'ecart type')
              break
            case 'moyenne+etendue':
              stor.tabQuestions.push('moyenne', 'etendue')
              break
            case 'ecart type+etendue':
              stor.tabQuestions.push('ecart type', 'etendue')
              break
            default:
              stor.tabQuestions.push(ds.typeQuest)
              break
          }
        }
        me.logIfDebug('stor.tabQuestions:', stor.tabQuestions)

        stor.tabChoixSujet = [1, 2, 3, 4, 5]
        stor.tabChoixSujetInit = [1, 2, 3, 4, 5]
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si les 4 zones ont bien été complétées ou non
        // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
        // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
        // Si c’est la dernière tentative de répondre, on barre également les zones fausses

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
              if (stor.elt.inputmqList[0].typeReponse[1] === 'arrondi') {
                if (Math.abs(stor.elt.inputmqList[0].reponse[0] - j3pNombre(fctsValid.zones.reponseSaisie[0])) <= 0.2) {
                // pb d’arrondi
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment1
                }
              }
              // pour l’écart-type, on peut s’interroger s’il n’a pas pris la mauvaise valeur de la calculatrice
              if (stor.maQuestion === 'ecart type') {
              // Je vérifie si l’élève n’a pas pris l’autre valeur (xsigma_{n-1})
                if (Math.abs(j3pNombre(fctsValid.zones.reponseSaisie[0]) - stor.ecartTypeVal2) < 0.1) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment2
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
