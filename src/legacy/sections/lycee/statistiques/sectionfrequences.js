import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pNombre, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pRandomTab, j3pRemplacePoint, j3pRestriction, j3pVirgule, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Octobre 2017
        Dans cette section, on demande de calculer des fréquences ou des FCC à partir d’un tableau d’effectifs
        Le caractère quantitatif peut être discret ou continu
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeCaractere', 'discret', 'liste', 'prend les valeurs "discret" (par défaut) ou "continu" suivant le type de caractère proposé.', ['discret', 'continu']],
    ['typeFreq', 'les deux', 'liste', 'peut prendre les valeurs "simple" (pour la fréquence d’une valeur ou d’une classe), "cumulee" (pour une fréquence cumulée croissante ou décroissante) ou "les deux" (par défaut, dans ce cas deux calculs sont demandés par répétition).', ['simple', 'cumulee', 'les deux']],
    ['ecritureFreq', 'les deux', 'liste', 'La fréquence sera présentée sous la forme d’un pourcentage si ce paramètre vaut "pourcentage", d’une valeur décimale s’il vaut "decimal" ou de manière aléatoire sous l’une ou l’autre des deux formes en lui donnant la valeur "les deux".', ['pourcentage', 'decimal', 'les deux']]
  ]

}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Calcul de fréquences',
  // on donne les phrases de la consigne
  consigne1_1: 'L’étude d’un caractère d’une population de £e éléments donne les résultats regroupés dans le tableau ci-dessous.',
  consigne1_2: 'L’étude d’un caractère d’une population de £e éléments donne les résultats présentés dans le tableau ci-dessous (les valeurs ont été regroupées dans des intervalles).',
  consigne2_1: 'Quelle est la fréquence des valeurs de ce caractère égales à £v&nbsp;?',
  consigne2_2: 'Quelle est la fréquence (arrondie à 0,001) des valeurs de ce caractère égales à £v&nbsp;?',
  consigne2_3: 'Quelle est la fréquence (en pourcentage) des valeurs de ce caractère égales à £v&nbsp;?',
  consigne2_4: 'Quelle est la fréquence (en pourcentage arrondi à 0,1%) des valeurs de ce caractère égales à £v&nbsp;?',
  consigne2_5: 'Quelle est la fréquence des valeurs de ce caractère comprises entre £v et £w&nbsp;?',
  consigne2_6: 'Quelle est la fréquence (arrondie à 0,001) des valeurs de ce caractère comprises entre £v et £w&nbsp;?',
  consigne2_7: 'Quelle est la fréquence (en pourcentage) des valeurs de ce caractère comprises entre £v et £w&nbsp;?',
  consigne2_8: 'Quelle est la fréquence (en pourcentage arrondi à 0,1%) des valeurs de ce caractère comprises entre £v et £w&nbsp;?',
  consigne3_1: 'Quelle est la fréquence des valeurs de ce caractère au moins égales à £v&nbsp;?',
  consigne3_2: 'Quelle est la fréquence (arrondie à 0,001) des valeurs de ce caractère au moins égales à £v&nbsp;?',
  consigne3_3: 'Quelle est la fréquence (en pourcentage) des valeurs de ce caractère au moins égales à £v&nbsp;?',
  consigne3_4: 'Quelle est la fréquence (en pourcentage arrondi à 0,1%) des valeurs de ce caractère au moins égales à £v&nbsp;?',
  consigne3_5: 'Quelle est la fréquence des valeurs de ce caractère égales à £v ou plus&nbsp;?',
  consigne3_6: 'Quelle est la fréquence (arrondie à 0,001) des valeurs de ce caractère égales à £v ou plus&nbsp;?',
  consigne3_7: 'Quelle est la fréquence (en pourcentage) des valeurs de ce caractère égales à £v ou plus&nbsp;?',
  consigne3_8: 'Quelle est la fréquence (en pourcentage arrondi à 0,1%) des valeurs de ce caractère égales à £v ou plus&nbsp;?',
  consigne3_9: 'Quelle est la fréquence des valeurs de ce caractère égales à plus de £v&nbsp;?',
  consigne3_10: 'Quelle est la fréquence (arrondie à 0,001) des valeurs de ce caractère égales à plus de £v&nbsp;?',
  consigne3_11: 'Quelle est la fréquence (en pourcentage) des valeurs de ce caractère égales à plus de £v&nbsp;?',
  consigne3_12: 'Quelle est la fréquence (en pourcentage arrondi à 0,1%) des valeurs de ce caractère égales à plus de £v&nbsp;?',
  consigne4_1: 'Quelle est la fréquence des valeurs de ce caractère au plus égales à £v&nbsp;?',
  consigne4_2: 'Quelle est la fréquence (arrondie à 0,001) des valeurs de ce caractère au plus égales à £v&nbsp;?',
  consigne4_3: 'Quelle est la fréquence (en pourcentage) des valeurs de ce caractère au plus égales à £v&nbsp;?',
  consigne4_4: 'Quelle est la fréquence (en pourcentage arrondi à 0,1%) des valeurs de ce caractère au plus égales à £v&nbsp;?',
  consigne4_1bis: 'Quelle est la fréquence des valeurs de ce caractère strictement inférieures à £v&nbsp;?',
  consigne4_2bis: 'Quelle est la fréquence (arrondie à 0,001) des valeurs de ce caractère strictement inférieures à £v&nbsp;?',
  consigne4_3bis: 'Quelle est la fréquence (en pourcentage) des valeurs de ce caractère strictement inférieures à £v&nbsp;?',
  consigne4_4bis: 'Quelle est la fréquence (en pourcentage arrondi à 0,1%) des valeurs de ce caractère strictement inférieures à £v&nbsp;?',
  consigne4_5: 'Quelle est la fréquence des valeurs de ce caractère égales à £v ou moins&nbsp;?',
  consigne4_6: 'Quelle est la fréquence (arrondie à 0,001) des valeurs de ce caractère égales à £v ou moins&nbsp;?',
  consigne4_7: 'Quelle est la fréquence (en pourcentage) des valeurs de ce caractère égales à £v ou moins&nbsp;?',
  consigne4_8: 'Quelle est la fréquence (en pourcentage arrondi à 0,1%) des valeurs de ce caractère égales à £v ou moins&nbsp;?',
  consigne4_9: 'Quelle est la fréquence des valeurs de ce caractère égales à moins de £v&nbsp;?',
  consigne4_10: 'Quelle est la fréquence (arrondie à 0,001) des valeurs de ce caractère égales à moins de £v&nbsp;?',
  consigne4_11: 'Quelle est la fréquence (en pourcentage) des valeurs de ce caractère égales à moins de £v&nbsp;?',
  consigne4_12: 'Quelle est la fréquence (en pourcentage arrondi à 0,1%) des valeurs de ce caractère égales à moins de £v&nbsp;?',
  consigne5: 'Fréquence : @1@£u.',
  tableau1: 'Valeurs',
  tableau2: 'Effectifs',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Peut-être est-ce un problème d’arrondi&nbsp;!',
  comment2: 'On demande un pourcentage&nbsp;!',
  comment3: 'On demande une fréquence sous la forme d’un nombre décimal compris entre 0 et 1&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'La population a un effectif total de £e éléments.',
  corr2_1: 'L’effectif de la valeur £v vaut £n.',
  corr2_2: 'Le nombre d’éléments dont la valeur est au plus £v est le nombre d’éléments dont la valeur est inférieure ou égale à £v c’est-à-dire $£c$.',
  corr2_2bis: 'Le nombre d’éléments dont la valeur est strictement inférieure à £v est $£c$.',
  corr2_3: 'Le nombre d’éléments dont la valeur est au moins £v est le nombre d’éléments dont la valeur est supérieure ou égale à £v c’est-à-dire $£c$.',
  corr2_4: '£n éléments ont une valeur comprise dans l’intervalle $[£v;£w[$.',
  corr2_5: '1 élément a une valeur comprise dans l’intervalle $[£v;£w[$.',
  corr2_6: 'Le nombre d’éléments dont la valeur est égale £v ou moins est le nombre d’éléments dont la valeur est inférieure ou égale à £v c’est-à-dire $£c$.',
  corr2_7: 'Le nombre d’éléments dont la valeur est égale à moins de £v est le nombre d’éléments dont la valeur est inférieure à £v c’est-à-dire $£c$.',
  corr2_8: 'Le nombre d’éléments dont la valeur est égale à £v ou plus est le nombre d’éléments dont la valeur est supérieure ou égale à £v c’est-à-dire $£c$.',
  corr2_9: 'Le nombre d’éléments dont la valeur est égale à plus de £v est le nombre d’éléments dont la valeur est supérieure à £v c’est-à-dire $£c$.',
  corr3_1: 'Donc la fréquence cherchée vaut $£d$.',
  corr3_3: 'Donc la fréquence en pourcentage vaut $£d$ soit $£p$%.',
  corr3_4: 'Donc la fréquence en pourcentage vaut $£d$ soit environ $£p$%.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let laCorrection3
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    for (let i = 1; i <= 3; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div', '')

    j3pAffiche(stor.zoneexplication1, '', textes.corr1, { e: stor.effTotal })
    let objCorr
    if (stor.maQuestion === 'simple') {
      if (stor.typeCaractere === 'discret') {
        objCorr = {
          v: stor.valFreq,
          n: stor.effValeur,
          d: '\\frac{' + stor.effValeur + '}{' + stor.effTotal + '}'
        }
        j3pAffiche(stor.zoneexplication2, '', textes.corr2_1, objCorr)
      } else {
        objCorr = {
          v: stor.valFreq1,
          w: stor.valFreq2,
          n: stor.effValeur,
          d: '\\frac{' + stor.effValeur + '}{' + stor.effTotal + '}'
        }
        if (stor.effValeur === 1) {
          j3pAffiche(stor.zoneexplication2, '', textes.corr2_5, objCorr)
        } else {
          j3pAffiche(stor.zoneexplication2, '', textes.corr2_4, objCorr)
        }
      }
    } else {
      objCorr = {
        v: stor.valFreq,
        c: stor.calculEff + '=' + stor.valEffCumule,
        d: '\\frac{' + stor.valEffCumule + '}{' + stor.effTotal + '}'
      }
      if (stor.infSup === 'inf') {
        if (stor.typeConsigne) {
          if (stor.typeCaractere === 'discret') {
            j3pAffiche(stor.zoneexplication2, '', textes.corr2_2, objCorr)
          } else {
            j3pAffiche(stor.zoneexplication2, '', textes.corr2_2bis, objCorr)
          }
        } else {
          if (stor.typeCaractere === 'discret') {
            j3pAffiche(stor.zoneexplication2, '', textes.corr2_6, objCorr)
          } else {
            j3pAffiche(stor.zoneexplication2, '', textes.corr2_7, objCorr)
          }
        }
      } else {
        if (stor.typeConsigne) {
          j3pAffiche(stor.zoneexplication2, '', textes.corr2_3, objCorr)
        } else {
          if (stor.typeCaractere === 'discret') {
            j3pAffiche(stor.zoneexplication2, '', textes.corr2_8, objCorr)
          } else {
            j3pAffiche(stor.zoneexplication2, '', textes.corr2_9, objCorr)
          }
        }
      }
    }
    if (stor.pourcentage) {
      let signeegal = '='
      if (Math.abs(stor.repFreq * 10 - Math.round(10 * stor.repFreq)) < Math.pow(10, -12)) {
        laCorrection3 = textes.corr3_3
      } else {
        laCorrection3 = textes.corr3_4
        signeegal = '\\approx '
      }
      objCorr.p = j3pVirgule(Math.round(stor.repFreq * 10) / 10)
      objCorr.d += signeegal + (Math.round(stor.repFreq * 10) / 1000) + '=\\frac{' + objCorr.p + '}{100}'
    } else {
      laCorrection3 = textes.corr3_1
      if (Math.abs(stor.repFreq * 1000 - Math.round(1000 * stor.repFreq)) < Math.pow(10, -12)) {
        objCorr.d += '='
      } else {
        objCorr.d += '\\approx '
      }
      objCorr.d += j3pVirgule(Math.round(stor.repFreq * 1000) / 1000)
    }
    j3pAffiche(stor.zoneexplication3, '', laCorrection3, objCorr)
  }

  function tableauDonneesEffectifs (mesDonnees) {
    // mesDonnees est un objet
    // mesDonnees.parent est le nom du div dans lequel est créé le tableau
    // mesDonnees.largeur est la largeur du tableau (utile pour configurer la largeur de chaque colonne)
    // mesDonnees.textes est un tableau de 3 éléments max : "Valeurs", Effectifs", "E.C.C" ou des équivalents à saisir
    // mesDonnees.valeurs : tableau des valeurs
    // mesDonnees.effectifs : tableaux des effectifs
    const captureIntervalle = /^([[\]])(-?[0-9,.]+);(-?[0-9,.]+)([[\]])$/ // cela me servira à mettre une virgule si l’une des bornes est un décimal
    function creerLigneTab (elt, valeurs, l1, l2) {
      // elt est l’élément html dans lequel créer la ligne
      // l1 et l2 sont respectivement la largeur de la première colonne et des suivantes
      const ligne = j3pAddElt(elt, 'tr')
      j3pAddElt(ligne, 'td', valeurs[0], { width: l1 + 'px' })
      for (let i = 1; i < valeurs.length; i++) {
        if ((valeurs[i] === 0) || valeurs[i]) { // c’est pour éviter le undefined (mais 0, je le garde)
          const chunk = captureIntervalle.exec(valeurs[i])
          // Je ne sais pas si dans les bornes de l’intervalle je peux avoir des décimaux, mais au cas où...
          if (chunk)j3pAddElt(ligne, 'td', valeurs[i].replace(chunk[0], chunk[1] + j3pVirgule(chunk[2]) + ';' + j3pVirgule(chunk[3]) + chunk[4]), { align: 'center', width: l2 + 'px' })
          else j3pAddElt(ligne, 'td', j3pVirgule(valeurs[i]), { align: 'center', width: l2 + 'px' })
        }
      }
    }

    const leTableau = j3pAddElt(mesDonnees.parent, 'div')
    leTableau.setAttribute('width', mesDonnees.largeur + 'px')
    if (mesDonnees.valeurs.length === mesDonnees.effectifs.length) {
      let largeurTexte = 0
      for (let i = 0; i < mesDonnees.textes.length; i++) {
        largeurTexte = Math.max(largeurTexte, $(leTableau).textWidth(mesDonnees.textes[i], me.styles.petit.enonce.fontFamily, me.styles.petit.enonce.fontSize))
      }
      largeurTexte += 10
      const largeurColonne = (mesDonnees.largeur - largeurTexte - 5) / mesDonnees.effectifs.length
      const table = j3pAddElt(leTableau, 'table', { width: '100%', borderColor: 'black', border: 2, cellSpacing: 0 })
      creerLigneTab(table, [mesDonnees.textes[0]].concat(mesDonnees.valeurs), largeurTexte, largeurColonne)
      creerLigneTab(table, [mesDonnees.textes[1]].concat(mesDonnees.effectifs), largeurTexte, largeurColonne)
      const tabECC = [mesDonnees.effectifs[0]]// tableau qui va récupérer les E.C.C.
      for (let i = 1; i < mesDonnees.effectifs.length; i++) {
        tabECC.push(j3pArrondi(tabECC[i - 1] + mesDonnees.effectifs[i], 4))
      }
      // on renvoie un objet contenant le nom des zones de saisie (lorsqu’elles existent) et le tableau des effectifs cumulés croissants
      return { tabECC, largeurCol1: largeurTexte, largeurAutreCol: largeurColonne }
    }
    j3pShowError('pb sur le nombre de valeurs dans les deux tableaux')
  }

  function enonceMain () {
    // A chaque répétition, je renouvelle les données
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      stor.nbValeurs = j3pGetRandomInt(6, 9)// valable aussi bien dans un cas discret que continu
      stor.mesValeurs = []
      const facteur = j3pRandomTab([2, 5, 10, 20], [0.25, 0.25, 0.25, 0.25])
      stor.mesValeurs.push(j3pGetRandomInt(1, 3) * facteur)
      for (let i = 1; i < stor.nbValeurs; i++) {
        stor.mesValeurs.push(stor.mesValeurs[i - 1] + j3pRandomTab([1, 2], [0.7, 0.3]) * facteur)
      }
      me.logIfDebug('stor.mesValeurs:', stor.mesValeurs)
      if (stor.typeCaractere === 'continu') {
        // il faut que j’ajoute une valeur (borne supérieure du dernier intervalle)
        // stor.mesValeurs.push(stor.mesValeurs[stor.mesValeurs.length-1]+j3pRandomTab([1,2],[0.7,0.3])*facteur)
        stor.mesIntervalles = []
        for (let i = 0; i < stor.nbValeurs - 1; i++) {
          stor.mesIntervalles.push('[' + stor.mesValeurs[i] + ';' + stor.mesValeurs[i + 1] + '[')
        }
        stor.mesIntervalles.push('[' + stor.mesValeurs[stor.mesValeurs.length - 1] + ';' + (stor.mesValeurs[stor.mesValeurs.length - 1] + j3pRandomTab([1, 2], [0.7, 0.3]) * facteur) + '[')
      }
      // maintenant je dois gérer mes effectifs
      stor.mesEffectifs = []
      stor.effTotal = 0
      let newEff
      for (let i = 0; i < stor.mesValeurs.length - 1; i++) {
        newEff = j3pGetRandomInt(2, 11)
        stor.effTotal += newEff
        stor.mesEffectifs.push(newEff)
      }
      if (stor.effTotal % 10 === 0) {
        stor.mesEffectifs.push(10)
        stor.effTotal += 10
      } else {
        newEff = Math.round((Math.floor(stor.effTotal / 10) + 1) * 10 - stor.effTotal)
        stor.effTotal += newEff
        stor.mesEffectifs.push(newEff)
      }
      // l’effectif total sera alors nécessairement un multiple de 10. Cela permettra par moment d’avoir des fréquences exactes.
    }
    me.logIfDebug('mes effectifs :', stor.mesEffectifs, '   et l’effectif total:', stor.effTotal)

    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    for (let i = 1; i <= 8; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')

    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    const laConsigne1 = (stor.typeCaractere === 'discret') ? textes.consigne1_1 : textes.consigne1_2
    j3pAffiche(stor.zoneCons1, '', laConsigne1, { e: stor.effTotal })

    stor.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
    const objValeurs = {
      parent: stor.zoneCons2,
      textes: [textes.tableau1, textes.tableau2],
      valeurs: stor.mesValeurs,
      effectifs: stor.mesEffectifs,
      largeur: stor.largeurTab
    }
    if (stor.typeCaractere === 'continu') {
      me.logIfDebug('stor.mesIntervalles:', stor.mesIntervalles)
      objValeurs.valeurs = stor.mesIntervalles
    }
    stor.objFrequences = tableauDonneesEffectifs(objValeurs)
    if (stor.typeFreq === 'les deux') {
      // dans ce cas j’ai deux questions
      if ((me.questionCourante - 1) % ds.nbetapes === 0) {
        stor.maQuestion = 'simple'
      } else {
        stor.maQuestion = 'cumulee'
      }
    } else {
      stor.maQuestion = stor.typeFreq
    }
    let laConsigne2
    let pos
    if (stor.maQuestion === 'simple') {
      if (stor.typeCaractere === 'discret') {
        pos = j3pGetRandomInt(0, (stor.mesValeurs.length - 1))
        stor.valFreq = stor.mesValeurs[pos]
        stor.repFreq = stor.mesEffectifs[pos] / stor.effTotal
        stor.effValeur = stor.mesEffectifs[pos]
        stor.pourcentage = (ds.ecritureFreq === 'les deux')
          ? (j3pGetRandomInt(0, 1))
          : (ds.ecritureFreq === 'pourcentage') ? 1 : 0
        laConsigne2 = (stor.pourcentage)
          ? (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
              ? textes.consigne2_3
              : textes.consigne2_4
          : (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
              ? textes.consigne2_1
              : textes.consigne2_2
        j3pAffiche(stor.zoneCons3, '', laConsigne2, { v: stor.valFreq })
      } else {
        pos = j3pGetRandomInt(0, (stor.mesIntervalles.length - 1))
        stor.valFreq1 = stor.mesValeurs[pos]
        stor.valFreq2 = j3pNombre(stor.mesIntervalles[pos].substring(stor.mesIntervalles[pos].indexOf(';') + 1, stor.mesIntervalles[pos].length - 1))
        stor.repFreq = stor.mesEffectifs[pos] / stor.effTotal
        stor.effValeur = stor.mesEffectifs[pos]
        stor.pourcentage = (ds.ecritureFreq === 'les deux')
          ? (j3pGetRandomInt(0, 1))
          : (ds.ecritureFreq === 'pourcentage') ? 1 : 0
        laConsigne2 = (stor.pourcentage)
          ? (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
              ? textes.consigne2_7
              : textes.consigne2_8
          : (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
              ? textes.consigne2_5
              : textes.consigne2_6
        j3pAffiche(stor.zoneCons3, '', laConsigne2, {
          v: stor.valFreq1,
          w: stor.valFreq2
        })
      }
    } else {
      pos = j3pGetRandomInt(2, (stor.mesValeurs.length - 3))
      stor.infSup = j3pRandomTab(['inf', 'sup'], [0.5, 0.5])
      stor.valFreq = stor.mesValeurs[pos]
      stor.pourcentage = (ds.ecritureFreq === 'les deux')
        ? (j3pGetRandomInt(0, 1))
        : (ds.ecritureFreq === 'pourcentage') ? 1 : 0
      let j
      if (stor.infSup === 'sup') {
        // c’est que la consigne demande "au moins valFreq" donc consigne3
        stor.calculEff = stor.mesEffectifs[pos]
        for (j = pos + 1; j < stor.mesValeurs.length; j++) {
          stor.calculEff += '+' + stor.mesEffectifs[j]
        }
        stor.valEffCumule = Math.round(stor.effTotal - stor.objFrequences.tabECC[pos - 1])
        stor.repFreq = stor.valEffCumule / stor.effTotal
        stor.typeConsigne = j3pGetRandomInt(0, 1)
        if (stor.typeConsigne) {
          laConsigne2 = (stor.pourcentage)
            ? (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                ? textes.consigne3_3
                : textes.consigne3_4
            : (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                ? textes.consigne3_1
                : textes.consigne3_2
        } else {
          if (stor.typeCaractere === 'discret') {
            laConsigne2 = (stor.pourcentage)
              ? (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                  ? textes.consigne3_7
                  : textes.consigne3_8
              : (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                  ? textes.consigne3_5
                  : textes.consigne3_6
          } else {
            laConsigne2 = (stor.pourcentage)
              ? (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                  ? textes.consigne3_11
                  : textes.consigne3_12
              : (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                  ? textes.consigne3_9
                  : textes.consigne3_10
          }
        }
      } else {
        // c’est que la consigne demande "au plus valFreq" donc consigne4
        stor.calculEff = stor.mesEffectifs[0]
        if (stor.typeCaractere === 'discret') {
          for (j = 1; j <= pos; j++) {
            stor.calculEff += '+' + stor.mesEffectifs[j]
          }
          stor.valEffCumule = stor.objFrequences.tabECC[pos]
        } else {
          for (j = 1; j < pos; j++) {
            stor.calculEff += '+' + stor.mesEffectifs[j]
          }
          stor.valEffCumule = stor.objFrequences.tabECC[pos - 1]
        }
        stor.repFreq = stor.valEffCumule / stor.effTotal
        stor.typeConsigne = j3pGetRandomInt(0, 1)
        if (stor.typeConsigne) {
          laConsigne2 = (stor.pourcentage)
            ? (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                ? (stor.typeCaractere === 'discret') ? textes.consigne4_3 : textes.consigne4_3bis
                : (stor.typeCaractere === 'discret') ? textes.consigne4_4 : textes.consigne4_4bis
            : (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                ? (stor.typeCaractere === 'discret') ? textes.consigne4_1 : textes.consigne4_1bis
                : (stor.typeCaractere === 'discret') ? textes.consigne4_2 : textes.consigne4_2bis
        } else {
          if (stor.typeCaractere === 'discret') {
            laConsigne2 = (stor.pourcentage)
              ? (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                  ? textes.consigne4_7
                  : textes.consigne4_8
              : (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                  ? textes.consigne4_5
                  : textes.consigne4_6
          } else {
            laConsigne2 = (stor.pourcentage)
              ? (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                  ? textes.consigne4_11
                  : textes.consigne4_12
              : (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12))
                  ? textes.consigne4_9
                  : textes.consigne4_10
          }
        }
      }
      j3pAffiche(stor.zoneCons3, '', laConsigne2, { v: stor.valFreq })
      me.logIfDebug('stor.calculEff:', stor.calculEff, '   stor.repFreq:', stor.repFreq)
    }
    if (stor.pourcentage) stor.repFreq = stor.repFreq * 100
    stor.unite = (stor.pourcentage) ? '%' : ''
    stor.elt = j3pAffiche(stor.zoneCons4, '', textes.consigne5,
      {
        u: stor.unite,
        input1: { texte: '', dynamique: true, maxchars: 6, width: '12px' }
      })
    j3pRestriction(stor.elt.inputList[0], '0-9.,')
    stor.elt.inputList[0].addEventListener('input', j3pRemplacePoint)
    stor.elt.inputList[0].typeReponse = (Math.abs(stor.repFreq * 1000 - Math.round(stor.repFreq * 1000)) < Math.pow(10, -12)) ? ['nombre', 'exact'] : (stor.pourcentage) ? ['nombre', 'arrondi', 0.1] : ['nombre', 'arrondi', 0.001]
    stor.elt.inputList[0].reponse = [stor.repFreq]
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.zoneD, 'div')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]//, left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div')
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.elt.inputList[0].id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    j3pFocus(stor.elt.inputList[0])
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 70

        me.construitStructurePage({ structure: 'presentation1', ratioGauche: stor.pourc / 100 })
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.typeCaractere = (ds.typeCaractere.toLowerCase().includes('continu')) ? 'continu' : 'discret'
        ds.nbetapes = 0
        if (ds.typeFreq.toLowerCase().includes('simple')) {
          ds.nbetapes++
          stor.typeFreq = 'simple'
        } else if (ds.typeFreq.toLowerCase().includes('cumul')) {
          ds.nbetapes++
          stor.typeFreq = 'cumulee'
        } else {
          ds.nbetapes += 2
          stor.typeFreq = 'les deux'
        }
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
              const precision = (stor.pourcentage) ? 1 : 0.01
              if (Math.abs(stor.elt.inputList[0].reponse[0] - j3pNombre(fctsValid.zones.reponseSaisie[0])) <= precision) {
              // pb d’arrondi
                stor.zoneCorr.innerHTML += '<br>' + textes.comment1
              } else {
                if (stor.pourcentage && (j3pNombre(fctsValid.zones.reponseSaisie[0])) < 1) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment2
                } else if (!stor.pourcentage && (j3pNombre(fctsValid.zones.reponseSaisie[0])) > 1) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment3
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
