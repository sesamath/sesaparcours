import $ from 'jquery'
import { j3pAddElt, j3pArrondi, j3pFocus, j3pGetRandomInt, j3pRemplacePoint, j3pRestriction, j3pShowError, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        octobre 2017
        Dans cette section, on peut (mais c’est optionnel) demander le tableau des effectifs commulés croissants
        Ensuite on demande la médiane puis les quartiles (dans les deux cas je demande le rang)
        on peut bien sûr n’imposer que la médiane ou les quartiles
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeExo', 'Les deux', 'liste', 'Prend les valeurs "mediane", "quartiles" ou "Les deux" (par défaut) suivant ce qui est demandé.', ['mediane', 'quartiles', 'Les deux']],
    ['Frequences', false, 'boolean', 'Par défaut l’exercice est avec les effectifs, mais si ce paramètre vaut true, on aura des fréquences.'],
    ['ECCdemandes', true, 'boolean', 'Vaut true si en première question on demande de compléter le tableau des effectifs cumulés croissants (ou des fréquences cumulées croissantes).'],
    ['RangDemande', true, 'boolean', 'Par défaut, on demande le rang de la médiane et des quartiles lorsqu’on a des effectifs. En lui donnant la valeur false, cela ne sera plus le cas.']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo1: 'Médiane d’une série statistique',
  titre_exo2: 'Quartiles d’une série statistique',
  titre_exo3: 'Médiane et quartiles d’une série statistique',
  // on donne les phrases de la consigne
  consigne1: 'L’étude d’un caractère quantitatif discret sur un échantillon de £e valeurs donne les résultats regroupés dans le tableau ci-dessous.',
  consigne2: 'Complète le tableau par les effectifs cumulés croissants (E.C.C.).',
  consigne2_2: 'Complète le tableau par les fréquences cumulées croissantes (F.C.C.).',
  consigne3: 'La médiane de cette série de données se situe au rang @1@ et vaut @2@.',
  consigne3_2: 'La médiane de cette série de données vaut @1@.',
  consigne4: 'Le premier quartile de cette série de données se situe au rang @1@ et vaut @2@.',
  consigne4_2: 'Le premier quartile de cette série de données vaut @1@.',
  consigne5: 'Le troisième quartile de cette série de données se situe au rang @1@ et vaut @2@.',
  consigne5_2: 'Le troisième quartile de cette série de données vaut @1@.',
  tableau1: 'Valeurs',
  tableau2: 'Effectifs',
  tableau2_2: 'Fréquences',
  tableau3: 'E.C.C.',
  tableau3_2: 'F.C.C.',
  tableau3bis: 'Effectifs Cumulés Croissants',
  tableau3bis_2: 'Fréquences Cumulées Croissantes',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: '',
  comment2: '',
  comment3: '',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'On complète le tableau précédent par les effectifs cumulés croissants (E.C.C).',
  corr1_2: 'On complète le tableau précédent par les fréquences cumulées croissantes (F.C.C).',
  corr2: 'L’effectif total est de £e. Or $£f$ donc la médiane est la £g$^{\\text{ème}}$ valeur.',
  corr3: 'En utilisant le tableau des effectifs cumulés croissants, on voit que la £g$^{\\text{ème}}$ valeur vaut £m. Donc la médiane est égale à £m.',
  corr4: 'L’effectif total est de £e. Or $£f$ donc le premier quartile est la £g$^{\\text{ème}}$ valeur.',
  corr5: 'En utilisant le tableau des effectifs cumulés croissants, on voit que la £g$^{\\text{ème}}$ valeur vaut £q. Donc le premier quartile est égal à £q.',
  corr6: 'De même $£d$ donc le troisième quartile est la £h$^{\\text{ème}}$ valeur.',
  corr7: 'La £h$^{\\text{ème}}$ valeur vaut £r. Donc le troisième quartile est égal à £r.',
  // et avec les fréquences :
  corr8: 'La médiane étant caractérisée par une fréquence cumulée croissante de $0,5$, on l’identifie dans la dernière ligne du tableau et on prend la valeur associée.',
  corr9: 'La médiane est ainsi la valeur £m.',
  corr10: 'La premier quartile étant caractérisé par une fréquence cumulée croissante de $0,25$, on l’identifie dans la dernière ligne du tableau et on prend la valeur associée.',
  corr11: 'Le premier quartile est ainsi la valeur £q.',
  corr12: 'La démarche est identique mais avec une fréquence cumulée croissante de $0,75$.',
  corr13: 'Le troisième quartile est ainsi la valeur £r.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    if (((stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'ECC') && !bonneReponse) ||
      (!ds.ECCdemandes && (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] !== 'ECC'))) {
      // on vient ici dans le cas où on a demandé de remplir le tableau des ECC
      // ou lorsqu’on demande la médiane et quartiles alors qu’on n’a pas demandé de compléter ce tableau
      const numZone = (ds.ECCdemandes) ? 4 : 3
      stor['zoneCons' + numZone].style.color = (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      const ligneSup = j3pAddElt(stor['zoneCons' + numZone], 'div')
      stor['zoneCons' + numZone].setAttribute('width', stor.largeurTab + 'px')

      const table = j3pAddElt(ligneSup, 'table', { width: '100%', borderColor: stor['zoneCons' + numZone].style.color, border: 2, cellSpacing: 0 })
      const td = j3pAddElt(table, 'td', { width: stor.largCol1 + 'px' })
      j3pAddElt(td, 'abbr', (ds.Frequences) ? textes.tableau3_2 : textes.tableau3, {
        title: (ds.Frequences) ? textes.tableau3bis_2 : textes.tableau3bis
      })
      for (const ecc of stor.objECC.tabECC) {
        j3pAddElt(table, 'td', j3pVirgule(ecc), { align: 'center', width: stor.largAutreCol + 'px' })
      }
    }
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    if (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] !== 'ECC') {
      for (let k = 0; k < 4; k++) stor['zoneExpli' + k] = j3pAddElt(zoneExpli, 'div')
      if (!ds.ECCdemandes) {
        // les effectifs cumulés n’ont pas été demandés, mais je les affiche car ils vont m’aider à justifier la médiane ou les quartiles
        if (ds.Frequences) j3pAffiche(stor.zoneExpli1, '', textes.corr1_2)
        else j3pAffiche(stor.zoneExpli1, '', textes.corr1)
      }
    }
    let objCorr
    if (ds.Frequences) {
      if (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'Mediane') {
        j3pAffiche(stor.zoneExpli2, '', textes.corr8)
        j3pAffiche(stor.zoneExpli3, '', textes.corr9, {
          m: stor.mediane
        })
      } else if (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'Quartiles') {
        j3pAffiche(stor.zoneExpli2, '', textes.corr10)
        j3pAffiche(stor.zoneExpli3, '', textes.corr11, {
          q: stor.Q1
        })
        for (let k = 4; k <= 5; k++) stor['zoneExpli' + k] = j3pAddElt(zoneExpli, 'div')
        j3pAffiche(stor.zoneExpli4, '', textes.corr12)
        j3pAffiche(stor.zoneExpli5, '', textes.corr13, {
          r: stor.Q3
        })
      }
    } else {
      if (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'Mediane') {
        objCorr = {
          e: stor.effectifTotal,
          f: '\\frac{' + stor.effectifTotal + '}{2}=' + (Math.round(10 * stor.effectifTotal / 2) / 10),
          g: Math.ceil(stor.effectifTotal / 2),
          m: stor.mediane
        }
        j3pAffiche(stor.zoneExpli2, '', textes.corr2, objCorr)
        j3pAffiche(stor.zoneExpli3, '', textes.corr3, objCorr)
        // corr2 : "L’effectif total est de £e. Or $£f$ donc la médiane est la £g$\up{\text{ème}}$ valeur.",
        // corr3 : "En utilisant le tableau des effectifs cumulés croissants, on voit que la £g$\up{\text{ème}}$ valeur vaut £m. Donc la médiane est égale à £m$."
      } else if (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'Quartiles') {
        objCorr = {
          e: stor.effectifTotal,
          f: '\\frac{' + stor.effectifTotal + '}{4}=' + (Math.round(100 * stor.effectifTotal / 4) / 100),
          g: Math.ceil(stor.effectifTotal / 4),
          q: stor.Q1,
          d: '\\frac{3\\times ' + stor.effectifTotal + '}{4}=' + (Math.round(100 * 3 * stor.effectifTotal / 4) / 100),
          h: Math.ceil(3 * stor.effectifTotal / 4),
          r: stor.Q3
        }
        j3pAffiche(stor.zoneExpli2, '', textes.corr4, objCorr)
        j3pAffiche(stor.zoneExpli3, '', textes.corr5, objCorr)
        for (let k = 4; k <= 5; k++) stor['zoneExpli' + k] = j3pAddElt(zoneExpli, 'div')
        j3pAffiche(stor.zoneExpli4, '', textes.corr6, objCorr)
        j3pAffiche(stor.zoneExpli5, '', textes.corr7, objCorr)
      }
    }
  }

  function tableauDonneesEffectifs (mesDonnees) {
    // mesDonnees est un objet
    // mesDonnees.parent est le nom du div dans lequel est créé le tableau
    // mesDonnees.largeur est la largeur du tableau (utile pour configurer la largeur de chaque colonne)
    // mesDonnees.textes est un tableau de 3 éléments max : "Valeurs", Effectifs", "E.C.C" ou des équivalents à saisir
    // mesDonnees.valeurs : tableau des valeurs
    // mesDonnees.effectifs : tableaux des effectifs
    // mesDonnees.complet est un booléen qui prend true si les E.C.C. sont donnés dans le tableau. Si false, ce sont des zones de saisie
    function creerLigneTab (elt, valeurs, l1, l2) {
      // elt est l’élément html dans lequel construire la ligne
      // l1 et l2 sont respectivement la largeur de la première colonne et des suivantes
      const ligne = j3pAddElt(elt, 'tr')
      j3pAddElt(ligne, 'td', valeurs[0], { width: l1 + 'px' })
      for (let i = 1; i < valeurs.length; i++) {
        j3pAddElt(ligne, 'td', String(valeurs[i]).replace(/\./g, ','), { align: 'center', width: l2 + 'px' })
      }
    }

    const leTableau = j3pAddElt(mesDonnees.parent, 'div')
    leTableau.setAttribute('width', mesDonnees.largeur + 'px')
    if (mesDonnees.valeurs.length === mesDonnees.effectifs.length) {
      let largeurTexte = 0
      for (let i = 0; i < mesDonnees.textes.length; i++) {
        largeurTexte = Math.max(largeurTexte, $(leTableau).textWidth(mesDonnees.textes[i], me.styles.petit.enonce.fontFamily, me.styles.petit.enonce.fontSize))
      }
      largeurTexte += 20
      const largeurColonne = (mesDonnees.largeur - largeurTexte - 5) / mesDonnees.effectifs.length
      const table = j3pAddElt(leTableau, 'table', { width: '100%', borderColor: 'black', border: 2, cellSpacing: 0 })
      creerLigneTab(table, [mesDonnees.textes[0]].concat(mesDonnees.valeurs), largeurTexte, largeurColonne)
      creerLigneTab(table, [mesDonnees.textes[1]].concat(mesDonnees.effectifs), largeurTexte, largeurColonne)
      const tabECC = [mesDonnees.effectifs[0]]// tableau qui va récupérer les E.C.C.
      for (let i = 1; i < mesDonnees.effectifs.length; i++) {
        tabECC.push(j3pArrondi(tabECC[i - 1] + mesDonnees.effectifs[i], 4))
      }
      // console.log("tabECC:",tabECC);
      const idECC = []
      if (mesDonnees.textes.length === 3) {
        // c’est qu’on a les effectifs cumulés croissants (ou les FCC)
        const titreComplet = ds.Frequences ? textes.tableau3bis_2 : textes.tableau3bis
        const ligne = j3pAddElt(table, 'tr')
        const td1 = j3pAddElt(ligne, 'td', { width: largeurTexte + 'px' })
        j3pAddElt(td1, 'abbr', mesDonnees.textes[2], { title: titreComplet })
        if (mesDonnees.complet) {
          tabECC.forEach(eff => j3pAddElt(ligne, 'td', eff, { align: 'center' }))
        } else {
          tabECC.forEach(eff => idECC.push(j3pAddElt(ligne, 'td', { align: 'center' })))
        }
      }
      const tabZones = []
      if (mesDonnees.textes.length === 3) {
        if (!mesDonnees.complet) {
          for (let i = 0; i < mesDonnees.valeurs.length; i++) {
            me.logIfDebug(idECC[i])
            const elt = j3pAffiche(idECC[i], '', '@1@', {
              input1: { texte: '', dynamique: true, width: '12px', maxchars: '3' }
            })
            j3pRestriction(elt.inputList[0], ',.0-9')
            if (ds.Frequences) {
              elt.inputList[0].setAttribute('maxlength', 4)
              elt.inputList[0].onkeyup = function () {
                if (this.value.includes('.')) this.value = this.value.replace('.', ',')
              }
            }
            tabZones.push(elt.inputList[0])
            elt.inputList[0].typeReponse = ['nombre', 'exact']
            elt.inputList[0].reponse = [tabECC[i]]
          }
          j3pFocus(tabZones[0])
        }
      }
      // on renvoie un objet contenant le nom des zones de saisie (lorsqu’elles existent) et le tableau des effectifs cumulés croissants
      return { zonesSaisie: tabZones, tabECC, largeurCol1: largeurTexte, largeurAutreCol: largeurColonne }
    }
    j3pShowError('pb sur le nombre de valeurs dans les deux tableaux', { vanishAfter: 5 })
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // gestion de l’aléatoire ici
    let k, j, choix
    if (me.questionCourante === 1) {
      // on ne passe ici que la toute première fois.
      // Je génère aléatoirement l’ensemble des données pour chaque répétition
      const nbValeurs = [3 * 2 + 1]
      const effTotal = ((ds.typeExo === 'mediane') || (ds.typeExo === 'les deux')) ? [j3pGetRandomInt(40, 60) * 2 + 1] : [j3pGetRandomInt(70, 110)]
      const tabEff = []
      const tabVal = []
      const tabFreq = []
      for (k = 1; k < ds.nbrepetitions; k++) {
        nbValeurs.push(j3pGetRandomInt(4, 6) * 2 + 1)
        if ((ds.typeExo === 'mediane') || (ds.typeExo === 'les deux')) {
          effTotal.push(j3pGetRandomInt(50, 80) * 2 + 1)
        } else {
          effTotal.push(j3pGetRandomInt(70, 110))
        }
        tabEff.push([])
      }
      me.logIfDebug('nbValeurs:' + nbValeurs, '   effTotal:', effTotal)
      for (k = 0; k < ds.nbrepetitions; k++) {
        let ok, suivant
        do {
          ok = true
          choix = j3pGetRandomInt(0, 1)
          let monEff = 0// taille de l’effectif déjà choisi
          // on empêche la médiane d'être la (nbValeurs[k]+1)/2 -ème valeur
          tabEff[k] = []
          for (j = 0; j < nbValeurs[k]; j++) {
            if (j < nbValeurs[k] - 1) {
              if (Math.abs(j - (nbValeurs[k] + 1) / 2) < Math.pow(10, -12)) {
                tabEff[k].push(suivant)
              } else {
                tabEff[k].push(j3pGetRandomInt(
                  Math.floor(0.5 * effTotal[k] / nbValeurs[k]),
                  Math.ceil(1.6 * effTotal[k] / nbValeurs[k])
                ))
              }
              monEff += tabEff[k][j]
            } else if (monEff >= effTotal[k]) {
              ok = false
            } else {
              tabEff[k].push(Math.floor(effTotal[k] - monEff))
            }
            // console.log("dans la boucle j:",j,"   monEff:",monEff,"   tabEff[k]:",tabEff[k])
            if (Math.abs(j + 1 - (nbValeurs[k] + 1) / 2) < Math.pow(10, -12)) {
              // il faut que je vérifie si cela ne devient pas la médiane de la série
              // console.log("j:",j,"    monEff:",monEff,"   effTotal[k]:",effTotal[k],"\ntabEff[k]:",tabEff[k])
              suivant = j3pGetRandomInt(Math.floor(0.5 * effTotal[k] / nbValeurs[k]), Math.ceil(1.6 * effTotal[k] / nbValeurs[k]))
              if ((monEff >= effTotal[k] / 2) && (monEff + suivant < effTotal[k] / 2) && (monEff - tabEff[k][j - 1] < effTotal[k] / 2)) {
                ok = false
                break
              }
            }
          }
          if (ok) {
            tabFreq[k] = []
            let sommeFreq = 0
            for (j = 0; j < tabEff[k].length - 1; j++) {
              tabFreq[k].push(Math.round(100 * tabEff[k][j] / effTotal[k]) / 100)
              if (tabFreq[k][j] < Math.pow(10, -12)) {
                ok = false
                break
              } else {
                sommeFreq += tabFreq[k][j]
              }
            }
            if (sommeFreq < 1) {
              tabFreq[k].push(Math.round(Math.pow(10, 5) * (1 - sommeFreq)) / Math.pow(10, 5))
              if (Math.abs(tabFreq[k][tabFreq[k].length - 1]) < Math.pow(10, -10)) {
                ok = false
              }
            } else {
              ok = false
            }
            // console.log("tabEff[" + k + "]=" + tabEff[k], "\ntabFreq[k]=", tabFreq[k])
          }
        } while (!ok)
      }
      stor.tabEff = tabEff
      stor.tabFreq = tabFreq
      stor.tabVal = []
      // il faut enfin que je définisse les valeurs que je vais mettre (même si cela n’a aucune incidence sur le résultat)
      for (k = 0; k < ds.nbrepetitions; k++) {
        // j’initialise l’ensemble de toutes les valeurs possibles
        tabVal[k] = []
        const tabInit = [5, 7, 11, 12, 14, 15, 18, 19, 20, 23, 25, 27, 29, 32, 35, 36, 37, 41, 43, 50, 52, 53, 56, 58, 59, 60, 68, 69, 72, 73, 75]
        for (j = 0; j < nbValeurs[k]; j++) {
          choix = j3pGetRandomInt(0, (tabInit.length - 1))
          tabVal[k].push(tabInit[choix])
          tabInit.splice(choix, 1)
        }
        // console.log("tabVal[k]:",tabVal[k])
        stor.tabVal[k] = tabVal[k].sort(function (a, b) {
          return a - b
        })// j’ai mis une fonction de comparaison qui permet de gérer l’ordre des nombres et non plus de chaînes de caractères :
        // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/sort
      }
    }
    if ((me.questionCourante - 1) % stor.nbetapes === 0) {
      stor.valeursSerie = []
      stor.valeursEffectifs = []
      stor.valeursFrequences = []
      stor.valeursSerie = stor.tabVal[(me.questionCourante - 1) / stor.nbetapes]
      stor.valeursEffectifs = stor.tabEff[(me.questionCourante - 1) / stor.nbetapes]
      stor.valeursFrequences = stor.tabFreq[(me.questionCourante - 1) / stor.nbetapes]
      stor.effectifTotal = 0
      for (let i = 0; i < stor.valeursEffectifs.length; i++) {
        stor.effectifTotal += stor.valeursEffectifs[i]
      }
      // recherche de la médiane et des quartiles
      stor.mediane = stor.valeursSerie[0]
      stor.Q1 = stor.valeursSerie[0]
      stor.Q3 = stor.valeursSerie[0]
      let medianeTrouvee = false
      let Q1Trouve = false
      let Q3Trouve = false
      if (ds.Frequences) {
        // j’ai été obligé de faire avec les fréquences cumulées croissantes plutôt que les effectifs dans le cas où je donne les fréquences à cause de mes arrondis
        let freqInter = 0
        for (let i = 0; i < stor.valeursFrequences.length; i++) {
          // console.log("effectifs cumulés croissants :", effInter + stor.valeursEffectifs[i])
          freqInter += j3pArrondi(stor.valeursFrequences[i], 4)
          if ((freqInter >= 0.25) && !Q1Trouve) {
            stor.Q1 = stor.valeursSerie[i]
            Q1Trouve = true
          }
          if ((freqInter >= 0.5) && !medianeTrouvee) {
            stor.mediane = stor.valeursSerie[i]
            medianeTrouvee = true
          }
          if ((freqInter >= 0.75) && !Q3Trouve) {
            stor.Q3 = stor.valeursSerie[i]
            Q3Trouve = true
          }
        }
      } else {
        let effInter = 0
        for (let i = 0; i < stor.valeursEffectifs.length; i++) {
          // console.log("effectifs cumulés croissants :", effInter + stor.valeursEffectifs[i])
          effInter += stor.valeursEffectifs[i]
          if ((effInter >= stor.effectifTotal / 4) && !Q1Trouve) {
            stor.Q1 = stor.valeursSerie[i]
            Q1Trouve = true
          }
          if ((effInter >= stor.effectifTotal / 2) && !medianeTrouvee) {
            stor.mediane = stor.valeursSerie[i]
            medianeTrouvee = true
          }
          if ((effInter >= 3 * stor.effectifTotal / 4) && !Q3Trouve) {
            stor.Q3 = stor.valeursSerie[i]
            Q3Trouve = true
          }
        }
      }
    }
    me.logIfDebug('Q1:', stor.Q1, '     mediane:', stor.mediane, '     Q3:', stor.Q3, '   Effectif total:', stor.effectifTotal)
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, { e: stor.effectifTotal })
    stor.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
    let lesTextes2, tableauECC
    if (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'ECC') {
      // on demande les Effectifs cumulés croissants
      const laConsigne2 = (ds.Frequences) ? textes.consigne2_2 : textes.consigne2
      lesTextes2 = (ds.Frequences) ? [textes.tableau1, textes.tableau2_2] : [textes.tableau1, textes.tableau2]
      j3pAffiche(stor.zoneCons2, '', laConsigne2)

      tableauECC = {
        parent: stor.zoneCons3,
        textes: lesTextes2,
        valeurs: stor.valeursSerie,
        effectifs: stor.valeursEffectifs,
        complet: false,
        largeur: stor.largeurTab
      }
      if (ds.Frequences) {
        // on donne les fréquences plutôt que les effectifs
        tableauECC.effectifs = stor.valeursFrequences
      }
      if (ds.ECCdemandes) {
        if (ds.Frequences)tableauECC.textes.push(textes.tableau3_2)
        else tableauECC.textes.push(textes.tableau3)
      }
      stor.objECC = tableauDonneesEffectifs(tableauECC)
      stor.largCol1 = stor.objECC.largeurCol1
      stor.largAutreCol = stor.objECC.largeurAutreCol
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: stor.objECC.zonesSaisie
      })
    } else {
      lesTextes2 = (ds.Frequences) ? [textes.tableau1, textes.tableau2_2] : [textes.tableau1, textes.tableau2]
      tableauECC = {
        parent: stor.zoneCons2,
        textes: lesTextes2,
        valeurs: stor.valeursSerie,
        effectifs: stor.valeursEffectifs,
        complet: true,
        largeur: stor.largeurTab
      }
      if (ds.Frequences) {
        // on donne les fréquences plutôt que les effectifs
        tableauECC.effectifs = stor.valeursFrequences
      }
      if (ds.ECCdemandes) {
        if (ds.Frequences) tableauECC.textes.push(textes.tableau3_2)
        else tableauECC.textes.push(textes.tableau3)
      }
      stor.objECC = tableauDonneesEffectifs(tableauECC)
      stor.largCol1 = stor.objECC.largeurCol1
      stor.largAutreCol = stor.objECC.largeurAutreCol

      stor.RangDemande = (!ds.Frequences && ds.RangDemande)
      let laConsigne, mesZonesSaisie
      if (stor.RangDemande) {
        laConsigne = (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'Mediane') ? textes.consigne3 : textes.consigne4
        stor.elts = j3pAffiche(stor.zoneCons4, '', laConsigne,
          {
            input1: { texte: '', dynamique: true, width: '12px', maxchars: '3' },
            input2: { texte: '', dynamique: true, width: '12px', maxchars: '4' }
          })
        mesZonesSaisie = [...stor.elts.inputList]
        stor.elts.inputList.forEach(eltInput => {
          eltInput.typeReponse = ['nombre', 'exact']
          j3pRestriction(eltInput, '0-9,.')
        })
        // mon effectif total devrait vraiment être impair
        if (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'Mediane') {
          stor.elts.inputList[0].reponse = [stor.effectifTotal / 2 + 1 / 2]
          stor.elts.inputList[1].reponse = [stor.mediane]
        } else {
          stor.elts.inputList[0].reponse = [Math.ceil(stor.effectifTotal / 4)]
          stor.elts.inputList[1].reponse = [stor.Q1]
        }
        if (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'Quartiles') {
          stor.elts2 = j3pAffiche(stor.zoneCons5, '', textes.consigne5,
            {
              input1: { texte: '', dynamique: true, width: '12px', maxchars: '3' },
              input2: { texte: '', dynamique: true, width: '12px', maxchars: '5' }
            })

          stor.elts2.inputList.forEach(eltInput => {
            mesZonesSaisie.push(eltInput)
            eltInput.typeReponse = ['nombre', 'exact']
            j3pRestriction(eltInput, '0-9,.')
          })
          stor.elts2.inputList[0].reponse = [Math.ceil(3 * stor.effectifTotal / 4)]
          stor.elts2.inputList[1].reponse = [stor.Q3]
        }
      } else {
        laConsigne = (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'Mediane') ? textes.consigne3_2 : textes.consigne4_2
        const elt1 = j3pAffiche(stor.zoneCons4, '', laConsigne,
          {
            input1: { texte: '', dynamique: true, width: '12px', maxchars: '4' }
          })
        mesZonesSaisie = [elt1.inputList[0]]
        elt1.inputList[0].typeReponse = ['nombre', 'exact']
        // mon effectif total devrait vraiment être impair
        if (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'Mediane') {
          elt1.inputList[0].reponse = [stor.mediane]
        } else {
          elt1.inputList[0].reponse = [stor.Q1]
        }
        j3pRestriction(elt1.inputList[0], '0-9,.')
        if (stor.typeQuest[(me.questionCourante - 1) % stor.nbetapes] === 'Quartiles') {
          const elt2 = j3pAffiche(stor.zoneCons5, '', textes.consigne5_2,
            {
              input1: { texte: '', dynamique: true, width: '12px', maxchars: '5' }
            })
          mesZonesSaisie.push(elt2.inputList[0])
          elt2.inputList[0].typeReponse = ['nombre', 'exact']
          elt2.inputList[0].reponse = [stor.Q3]
          j3pRestriction(elt2.inputList[0], '0-9,.')
        }
      }
      j3pFocus(mesZonesSaisie[0])
      // console.log('mesZonesSaisie:', mesZonesSaisie)
      for (const zone of mesZonesSaisie) zone.addEventListener('input', j3pRemplacePoint)
      stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    }

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        stor.pourcentage = 70
        if ((ds.typeExo === 'Mediane') || (ds.typeExo.toLowerCase() === 'mediane') || (ds.typeExo === 'Médiane') || (ds.typeExo.toLowerCase() === 'médiane')) {
          me.afficheTitre(textes.titre_exo1)
          ds.typeExo = 'mediane'
        } else if ((ds.typeExo.toLowerCase() === 'quartile') || (ds.typeExo.toLowerCase() === 'quartiles')) {
          me.afficheTitre(textes.titre_exo2)
          ds.typeExo = 'quartiles'
        } else {
          me.afficheTitre(textes.titre_exo3)
          ds.typeExo = 'les deux'
        }

        stor.nbetapes = 1
        stor.typeQuest = []
        if (ds.ECCdemandes) {
          stor.nbetapes++
          stor.typeQuest.push('ECC')
        }
        if (ds.typeExo === 'les deux') stor.nbetapes++
        switch (ds.typeExo) {
          case 'mediane':
            stor.typeQuest.push('Mediane')
            break
          case 'quartiles':
            stor.typeQuest.push('Quartiles')
            break
          default :
            stor.typeQuest.push('Mediane', 'Quartiles')
            break
        }
        ds.nbetapes = stor.nbetapes
        ds.nbitems = stor.nbetapes * ds.nbrepetitions

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si les zones ont bien été complétées ou non
        // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
        // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
        // Si c’est la dernière tentative de répondre, on barre également les zones fausses

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
