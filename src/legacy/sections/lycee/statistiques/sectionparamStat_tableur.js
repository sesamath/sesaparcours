import { j3pAddElt, j3pNombre, j3pShuffle, j3pFocus, j3pGetRandomInt, j3pMax, j3pMin, j3pRandomTab, j3pRemplacePoint, j3pRestriction, j3pVirgule, j3pAutoSizeInput, j3pSetProps, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Rémi DENIAUD
        décembre 2018
        On présente des données dans un tableur
        Il est possible de demander quelle formule saisir mais aussi la valeur de la moyenne et de l’écart-type des données
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuest', 'moyenne', 'liste', 'Cette section permet de tester des calculs de moyenne, d’écart-type et d’étendue. Il est possibile de choisir une ou plusieurs de ces questions.', ['moyenne', 'ecart type', 'etendue', 'moyenne+ecart type', 'moyenne+etendue', 'ecart type+etendue', 'les trois']],
    ['questTableur', true, 'boolean', 'Si ce paramètre vaut true (ce qui est le cas par défaut), alors avant le calcul de la moyenne et/ou de l’écart-type, on demande la formule à saisir dans une feuille de calcul.'],
    ['lignesCompletes', false, 'boolean', 'Dans le cas où une question sur le tableur est posée, on peut imposer que toutes les lignes soient complètes (ce qui n’est pas le cas par défaut).'],
    ['petitEffectif', false, 'boolean', 'On peut proposer des calculs sur des petits effectifs, dans le cas la question sur le tableur n’est pas posée car l’affichage se fait sous la forme d’une liste, pas d’une feuille de calcul.'],
    ['seuilReussite', 0.8, 'reel', 'seuil au-dessus duquel on considère que l’élève a réussi la notion'],
    ['seuilErreur', 0.4, 'reel', 'seuil au-dessus duquel on considère que la notion n’est pas acquise']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'Formules du tableur non maîtrisées mais calculs corrects dans l’ensemble' },
    { pe_3: 'Calculs non maîtrisés mais formules de tableur correctes dans l’ensemble' },
    { pe_4: 'Insuffisant' }
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo1: 'Calcul de moyenne',
  titre_exo2: 'Calcul de l’écart-type',
  titre_exo3: 'Calcul de l’étendue',
  titre_exo4: 'Moyenne et écart-type',
  titre_exo5: 'Moyenne et étendue',
  titre_exo6: 'Ecart-type et étendue',
  titre_exo7: 'Moyenne, écart-type et étendue',
  // on donne les phrases de la consigne
  consigne1_1: 'L’étude d’un caractère quantitatif discret sur une population de £e éléments donne les résultats regroupés dans la feuille de calcul (inactive) ci-dessous.',
  consigne1_2: 'On a relevé dans la feuille de calcul (inactive) ci-dessous les notes du baccalauréat de mathématiques de £e élèves d’un lycée.',
  consigne1_3: 'Un magasin de chaussures a relevé les pointures des £e clients ayant effectué un achat au cours d’une matinée de soldes et a consigné les résultats dans la feuille de calcul (inactive) ci-dessous.',
  consigne1_4: 'Un club de rugby professionnel a relevé dans la feuille de calcul (inactive) ci-dessous le nombre de matchs joués par chacun des £e joueurs au cours d’une saison.',
  consigne1_5: 'Une société d’autoroute a relevé dans la feuille de calcul (inactive) ci-dessous le nombre de véhicules empruntant une sortie pendant £e jours.',
  // Dans le cas où on a de petits effectifs
  consignePetit1_1: 'On considère la série statistique suivante contenant £e valeurs&nbsp;:',
  consignePetit1_2: 'Voici les relevés annuels de la consommation d’eau (en m$^3$) d’un foyer au cours des £e dernières années&nbsp;:',
  consignePetit1_3: 'Un automobiliste a relevé les distances annuelles parcourues avec sa voiture au cours de £e derniers mois&nbsp;:',
  consignePetit1_4: 'Yann a relevé au cours des £e derniers jours le nombre de pas journaliers à l’aide d’un podomètre&nbsp;:',
  consignePetit1_5: 'Voici les distances parcourues par Franck au cours de ses £e dernières courses à pied&nbsp;:',
  table1: 'moyenne',
  table2: 'écart-type',
  table3: 'étendue',
  consigne2_1: 'Quelle formule (utilisant une fonction du tableur) faut-il écrire en £c pour obtenir la moyenne des valeurs présentes dans la feuille de calcul&nbsp;?',
  consigne2_2: 'Quelle formule (utilisant une fonction du tableur) faut-il écrire en £c pour obtenir l’écart-type des valeurs présentes dans la feuille de calcul&nbsp;?',
  consigne2_3: 'Quelle formule (utilisant des fonctions du tableur) faut-il écrire en £c pour obtenir l’étendue des valeurs présentes dans la feuille de calcul&nbsp;?',
  consigne3: 'Formule : @1@',
  consigne4_1: 'Quelle est la moyenne des valeurs de ce caractère&nbsp;?',
  consigne4_2: 'Quelle est la moyenne des notes de mathématiques de ces élèves&nbsp;?',
  consigne4_3: 'Quelle est la moyenne des pointures de ces clients&nbsp;?',
  consigne4_4: 'Quelle est le nombre moyen de matchs joués par les joueurs de cette équipe&nbsp;?',
  consigne4_5: 'Quelle est le nombre moyen de véhicules ayant empruntés cette sortie&nbsp;?',
  consigne5_1: 'Quelle est l’écart-type des valeurs de ce caractère&nbsp;?',
  consigne5_2: 'Quelle est l’écart-type des notes de mathématiques de ces élèves&nbsp;?',
  consigne5_3: 'Quelle est l’écart-type des pointures de ces clients&nbsp;?',
  consigne5_4: 'Quelle est l’écart-type du nombre de matchs joués par les joueurs de cette équipe&nbsp;?',
  consigne5_5: 'Quelle est l’écart-type du nombre de véhicules ayant empruntés cette sortie&nbsp;?',
  consignePetit4_1: 'Quelle est la moyenne des valeurs de cette série&nbsp;?',
  consignePetit4_2: 'Quelle est la consommation d’eau moyenne (en m$^3$) de ce foyer au cours de ces £e années&nbsp;?',
  consignePetit4_3: 'Quelle est la distance annuelle moyenne parcourue par cet automobiliste&nbsp;?',
  consignePetit4_4: 'Quelle est le nombre de pas moyens effectués par Yann au cours de ces £e jours&nbsp;?',
  consignePetit4_5: 'Quelle distance moyenne a-t-il parcourue au cours de ces £e courses&nbsp;?',
  consignePetit5_1: 'Quelle est l’écart-type des valeurs de cette série&nbsp;?',
  consignePetit5_2: 'Quelle est l’écart-type de la consommation d’eau de ce foyer au cours de ces £e années&nbsp;?',
  consignePetit5_3: 'Quelle est l’écart-type des distances mensuelles parcourues par cet automobilitse&nbsp;?',
  consignePetit5_4: 'Quelle est l’écart-type du nombre de pas effectués par Yann au cours de ces £e jours&nbsp;?',
  consignePetit5_5: 'Quelle est l’écart-type des distances des £e courses auxquelles a participé Franck&nbsp;?',
  // Jai ajouté les questions sur l’étendue après coup (c’est pourquoi elles sont au numéro 8)
  consigne8_1: 'Quelle est l’étendue des valeurs de ce caractère&nbsp;?',
  consigne8_2: 'Quelle est l’étendue des notes de mathématiques de ces élèves&nbsp;?',
  consigne8_3: 'Quelle est l’étendue des pointures de ces clients&nbsp;?',
  consigne8_4: 'Quelle est l’étendue du nombre de matchs joués par les joueurs de cette équipe&nbsp;?',
  consigne8_5: 'Quelle est l’étendue du nombre de véhicules ayant empruntés cette sortie&nbsp;?',
  consignePetit8_1: 'Quelle est l’étendue des valeurs de cette série&nbsp;?',
  consignePetit8_2: 'Quelle est l’étendue de la consommation d’eau de ce foyer au cours de ces £e années&nbsp;?',
  consignePetit8_3: 'Quelle est l’étendue des distances annuelles parcourues par cet automobilitse&nbsp;?',
  consignePetit8_4: 'Quelle est l’étendue du nombre de pas effectués par Yann au cours de ces £e jours&nbsp;?',
  consignePetit8_5: 'Quelle est l’étendue des distances des £e courses auxquelles a participé Franck&nbsp;?',
  consigne6_1: 'Moyenne : @1@.',
  consigne6_2: 'Ecart-type : @1@.',
  consigne6_3: 'Etendue : @1@.',
  consigne7: 'Arrondir à 0,1.',
  consigne7_2: 'Arrondir à l’unité',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Attention : quelle est la première chose à écrire pour une formule du tableur ?',
  comment2: 'La fonction utilisée n’est pas la bonne !',
  comment3: 'Ce n’est pas la bonne fonction pour l’écart-type !',
  comment4: 'Problème de séparateur de la plage de données !',
  comment5: 'La plage de données proposée n’est pas la bonne !',
  comment6: 'Peut-être est-ce un problème d’arrondi&nbsp;!',
  comment7: 'As-tu pris la bonne valeur sur ta calculatrice&nbsp;?',
  comment8: 'La fonction ETENDUE n’existe pas dans le tableur&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Les données se situent dans le rectangle dont les sommets opposés sont entre les cellules A1 et £c donc la plage de cellules à renseigner dans la formule est £d.',
  corr1_2: 'Les données se situent dans le rectangle dont les sommets opposés sont entre les cellules A1 et £c donc la plage de cellules à renseigner dans la formule est £d (les cases vides n’étant pas prises en compte).',
  corr2_1: 'Comme on cherche à calculer la moyenne des données, la formule à écrire en £b est £f.',
  corr2_2: 'Comme on cherche à calculer l’écart-type des données, la formule à écrire en £b est £f.',
  corr2_3: 'La fonction ETENDUE n’étant pas présente dans le tableur, on revient à sa définition en utilisant les fonctions MAX et MIN.<br/>Ainsi la formule à écrire en £b est £f.',
  corr3_1: 'Le calcul de la moyenne peut s’effectuer par $£c£e£{r2}$ mais cela peut directement être donné par la calculatrice&nbsp;:',
  corr3_2: 'Le calcul de l’écart-type se fait à la calculatrice (comme pour la moyenne)&nbsp;:',
  corr3_3: 'La valeur maximale de la série vaut £{max} et la valeur minimale £{min}.<br/>L’étendue vaut alors $£{max}-£{min}=£{r2}$.',
  corrPetit3_1: 'Le calcul de la moyenne s’effectue par $£c$.',
  corrPetit4_1: 'On obtient $\\overline{x}£s£{r1}$ et la moyenne vaut £e£{r2}.',
  corr4: '- dans la partie "Statistiques", on saisit les données dans la liste 1&nbsp;;',
  corr5: '- on paramètre ensuite la calculatrice pour que les valeurs prises en compte pour le calcul soient celles de la liste 1 et les effectifs soient de 1&nbsp;;',
  corr6: "- il ne reste plus qu'à demander à la calculatrice de faire les calculs&nbsp;;",
  corr7_1: '- la moyenne correspond alors à la variable $\\overline{x}$.',
  corr8_1: 'Dans cet exemple, on obtient $\\overline{x}£s£{r1}$ et la moyenne vaut £e£{r2}.',
  corr7_2: '- l’écart-type correspond (suivant les modèles) à $\\sigma X$ ou $x\\sigma n$.',
  corr8_2: 'Dans cet exemple, on obtient $\\sigma£s£{r1}$ et l’écart-type vaut £e£{r2}.',
  corr10: 'environ ',
  // info calculatrice pour trouver simplement le min et le max
  corr9_1: 'Pour trouver simplement les valeurs minimales et maximales d’une série, on peut utiliser la calculatrice&nbsp;:',
  corr9_2: '- on utilise alors un outil appelé SORT, SRT ou TRI (selon les modèles) qui permet de ranger les valeurs de la série dans l’ordre croissant ou décroissant&nbsp;',
  corr9_3: '- on identifie alors directement la valeur maximale et la valeur minimale.',
  corr9_4: 'En calculant la moyenne, on peut aussi retrouver la valeur maximale ou minimale d’une série dans l’ensemble des résultats donnés par la calculatrice.'

}
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function creationTab (obj) {
    // obj.parent es l’id dans lequel construire la table
    // obj.tabDonnees est le tableau contenant toutes les valeurs
    // obj.lignes est le nombre de lignes
    // obj.texte correspond à ce qui sera demandé
    // obj.largTableur correspond à la largeur totale de la table
    const largCol1 = 35
    const largAutresColonnes = (obj.largTableur - largCol1) / Math.floor(obj.tabDonnees.length / obj.lignes)
    const table = j3pAddElt(obj.parent, 'table')
    j3pSetProps(table, { width: '100%', borderColor: 'black', border: 2, cellSpacing: 0, lineHeight: 1 })
    const ligne1 = j3pAddElt(table, 'tr')
    j3pAddElt(ligne1, 'th')
    for (let j = 0; j < Math.ceil(obj.tabDonnees.length / obj.lignes); j++) {
      j3pAddElt(ligne1, 'th', String.fromCharCode(65 + j))
    }
    const derniereColonne = String.fromCharCode(65 + Math.ceil(obj.tabDonnees.length / obj.lignes) - 1)
    for (let i = 0; i < obj.lignes; i++) {
      // 1ère case de chaque ligne en gris (c’est le numéro de la ligne du tableur)
      const ligne = j3pAddElt(table, 'tr')
      j3pAddElt(ligne, 'th', String(i + 1))
      for (let j = 0; j < Math.ceil(obj.tabDonnees.length / obj.lignes); j++) {
        let td
        if (!obj.tabDonnees[i * Math.ceil(obj.tabDonnees.length / obj.lignes) + j]) {
          td = j3pAddElt(ligne, 'td')
        } else {
          td = j3pAddElt(ligne, 'td', obj.tabDonnees[i * Math.ceil(obj.tabDonnees.length / obj.lignes) + j])
        }
        j3pSetProps(td, { width: largAutresColonnes + 'px', align: 'center' })
      }
    }
    const ligne = j3pAddElt(table, 'tr')
    j3pAddElt(ligne, 'th', String(obj.lignes + 1))
    j3pAddElt(ligne, 'td', obj.texte)
    const eltFormule = j3pAddElt(ligne, 'td')
    for (let j = 2; j < Math.ceil(obj.tabDonnees.length / obj.lignes); j++) {
      j3pAddElt(ligne, 'td')
    }
    const th = document.querySelectorAll('th')
    th.forEach(elt => {
      j3pStyle(elt, { width: largCol1 + 'px', backgroundColor: '#BABABA' })
    })
    j3pStyle(document.querySelector('table'), { fontSize: '0.8em' })
    return { nomCel: eltFormule, lastCol: derniereColonne }
  }

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    for (let i = 1; i <= 3; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div')

    // le style n’est pas le même suivant que la réponse soit bonne ou fausse'
    let valArrondie, valArrondie2
    if (ds.questTableur && (((me.questionCourante - 1) % ds.nbetapes) % 2 === 0)) {
      // questions sur le tableur
      const laCorr1 = (stor.objTable.tabDonnees.length % stor.objTable.lignes === 0) ? textes.corr1_1 : textes.corr1_2
      const laCorr2 = (stor.maQuestion === 'moyenne')
        ? textes.corr2_1
        : (stor.maQuestion === 'ecart type')
            ? textes.corr2_2
            : textes.corr2_3
      j3pAffiche(stor.zoneexplication1, '', laCorr1,
        {
          c: stor.lastCol + stor.objTable.lignes,
          d: stor.plagesCellules[0]
        })
      j3pAffiche(stor.zoneexplication2, '', laCorr2,
        {
          b: 'B' + (stor.objTable.lignes + 1),
          f: stor.elt.inputList[0].reponse[0]
        })
    } else {
      for (let i = 4; i <= 6; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div', '')
      const environSigne = (stor.elt.inputList[0].typeReponse[1] === 'arrondi') ? '\\approx' : '='
      const environ = (stor.elt.inputList[0].typeReponse[1] === 'arrondi') ? textes.corr10 : ''
      const puis10 = Math.round(1 / stor.elt.inputList[0].typeReponse[2])
      valArrondie = (stor.elt.inputList[0].typeReponse[1] === 'arrondi') ? j3pVirgule(Math.round(puis10 * stor.elt.inputList[0].reponse[0]) / puis10) : j3pVirgule(stor.elt.inputList[0].reponse[0])
      valArrondie2 = (stor.elt.inputList[0].typeReponse[1] === 'arrondi') ? j3pVirgule(Math.round(1000 * stor.elt.inputList[0].reponse[0]) / 1000) : j3pVirgule(stor.elt.inputList[0].reponse[0])
      if (stor.maQuestion === 'moyenne') {
        if (ds.petitEffectif) {
          j3pAffiche(stor.zoneexplication1, '', textes.corrPetit3_1, {
            c: stor.texteMoyenne,
            e: environSigne,
            r2: valArrondie
          })
        } else {
          j3pAffiche(stor.zoneexplication1, '', textes.corr3_1, {
            c: stor.texteMoyenne,
            e: environSigne,
            r2: valArrondie
          })
        }
      } else if (stor.maQuestion === 'ecart type') {
        j3pAffiche(stor.zoneexplication1, '', textes.corr3_2)
      } else {
        j3pAffiche(stor.zoneexplication1, '', textes.corr3_3, {
          min: j3pVirgule(stor.mini),
          max: j3pVirgule(stor.maxi),
          r2: valArrondie
        })
      }
      if (stor.maQuestion === 'etendue') {
        if (!ds.petitEffectif) {
          const listeCorr = ['corr9_1', 'corr4', 'corr9_2', 'corr9_3', 'corr9_4']
          for (let i = 2; i <= 6; i++) j3pAffiche(stor['zoneexplication' + i], '', textes[listeCorr[i - 2]])
        }
      } else {
        if (stor.maQuestion === 'moyenne') {
          if (ds.petitEffectif) {
            j3pAffiche(stor.zoneexplication2, '', textes.corrPetit4_1, {
              e: environ,
              s: (environ === '') ? '=' : '\\approx',
              r1: valArrondie2,
              r2: valArrondie
            })
          } else {
            for (let i = 4; i <= 6; i++) j3pAffiche(stor['zoneexplication' + (i - 2)], '', textes['corr' + i])
            j3pAffiche(stor.zoneexplication5, '', textes.corr7_1)
            j3pAffiche(stor.zoneexplication6, '', textes.corr8_1,
              {
                e: environ,
                s: (environ === '') ? '=' : '\\approx',
                r1: valArrondie2,
                r2: valArrondie
              })
          }
          const tabOverline = document.querySelectorAll('.mq-overline')
          tabOverline.forEach(elt => {
            const laCouleur = elt.style.color
            j3pStyle(elt, { borderTop: '1px solid ' + laCouleur })
          })
        } else {
          for (let i = 4; i <= 6; i++) j3pAffiche(stor['zoneexplication' + (i - 2)], '', textes['corr' + i])
          j3pAffiche(stor.zoneexplication5, '', textes.corr7_2)
          j3pAffiche(stor.zoneexplication6, '', textes.corr8_2,
            {
              e: environ,
              s: (environ === '') ? '=' : '\\approx',
              r1: valArrondie2,
              r2: valArrondie
            })
        }
      }
    }
  }

  function suite () {
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      // Je génère des valeurs correspondant à un contexte particulier
      if (stor.tabChoixSujet.length === 0) {
        stor.tabChoixSujet = [...stor.tabChoixSujetInit]
      }
      const alea = Math.floor(j3pGetRandomInt(0, (stor.tabChoixSujet.length - 1)))
      const choixSujet = stor.tabChoixSujet[alea]
      stor.tabChoixSujet.splice(alea, 1)
      stor.choixSujet = choixSujet
    }
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      let tabDonnees = []
      let nbDonnees = (ds.petitEffectif) ? j3pGetRandomInt(5, 8) : j3pGetRandomInt(21, 35)
      let newDonnees = j3pGetRandomInt(3, 15)
      let // plus petite de toutes les valeurs de la liste
        sommeDonnees = 0
      let sommeCarreDonnees = 0
      if (ds.petitEffectif) {
        for (let i = 0; i < nbDonnees; i++) {
          switch (stor.choixSujet) {
            case 1:
              do {
                newDonnees = j3pGetRandomInt(5, 25)
              } while (tabDonnees.includes(newDonnees))
              break
            case 2:
              do {
                newDonnees = j3pGetRandomInt(55, 80)
              } while (tabDonnees.includes(newDonnees))
              break
            case 3:
              do {
                newDonnees = 100 * j3pGetRandomInt(150, 220)
              } while (tabDonnees.includes(newDonnees))
              break
            case 4:
              do {
                newDonnees = 10 * j3pGetRandomInt(800, 1200)
              } while (tabDonnees.includes(newDonnees))
              break
            default:
              do {
                newDonnees = j3pGetRandomInt(10, 25)
              } while (tabDonnees.includes(newDonnees))
              break
          }
          tabDonnees.push(newDonnees)
          sommeDonnees += newDonnees
          sommeCarreDonnees += Math.pow(newDonnees, 2)
        }
      } else {
        stor.nbLignes = (ds.lignesCompletes) ? j3pGetRandomInt(3, 5) : 4
        if (ds.lignesCompletes) {
          nbDonnees = stor.nbLignes * j3pGetRandomInt(5, 7)
        } else {
          while (nbDonnees % stor.nbLignes === 0) { // dans la feuille de calcul, on a stor.nbLignes lignes ; on fait en sorte que la dernière ne soit aps complète
            nbDonnees = j3pGetRandomInt(21, 35)
          }
        }
        let arrayProba
        let tabNb
        let tabPointures
        if (stor.choixSujet === 0) {
          sommeDonnees += newDonnees
          sommeCarreDonnees += Math.pow(newDonnees, 2)
          stor.nbColonnes = Math.ceil(nbDonnees / ds.lignesCompletes)
          tabDonnees.push(newDonnees)
          for (let i = 1; i < nbDonnees; i++) {
            const ajoutDonnees = j3pRandomTab([0, 1, 2, 3, 4], [0.3, 0.2, 0.2, 0.2, 0.1])
            newDonnees += ajoutDonnees
            tabDonnees.push(newDonnees)
            sommeDonnees += newDonnees
            sommeCarreDonnees += Math.pow(newDonnees, 2)
          }
        } else {
          // Dans les autres cas, on crée un tableau de valeurs possibles et on pioche au hasard dedans
          const tabDonneesInitiales = []
          switch (stor.choixSujet) {
            case 1:
              {
                const arrayNotes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
                arrayProba = [0.00002, 0.0002, 0.00105, 0.0048, 0.0145, 0.0365, 0.0735, 0.12, 0.16, 0.17905, 0.16, 0.12, 0.0735, 0.0365, 0.0145, 0.0046, 0.00105, 0.0002, 0.00002, 0.00001]
                for (let i = 0; i < nbDonnees; i++) {
                  tabDonneesInitiales.push(j3pRandomTab(arrayNotes, arrayProba))
                }
              }
              break
            case 2:
              tabPointures = [36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46]
              tabNb = [15, 20, 25, 40, 50, 60, 65, 25, 20, 15, 10]
              for (let i = 1; i < tabPointures.length; i++) {
                for (let j = 0; j < tabNb[i]; j++) {
                  tabDonneesInitiales.push(tabPointures[i])
                }
              }
              break
            case 3:
              {
                const arrayMatchs = [10, 12, 14, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 29, 30, 32, 35, 36]
                arrayProba = [0.00002, 0.0002, 0.00105, 0.0048, 0.0145, 0.0365, 0.0735, 0.12, 0.16, 0.17905, 0.16, 0.12, 0.0735, 0.0365, 0.0145, 0.0046, 0.00105, 0.0002, 0.00002, 0.00001]
                for (let i = 0; i < nbDonnees; i++) {
                  tabDonneesInitiales.push(j3pRandomTab(arrayMatchs, arrayProba))
                }
              }
              break
            default:
              tabPointures = [22, 24, 26, 27, 29, 30, 32, 38, 39, 40, 41, 45, 47, 48, 49, 55, 57]
              tabNb = [3, 4, 5, 7, 10, 11, 12, 15, 20, 20, 21, 15, 15, 13, 6, 7, 2]
              for (let i = 1; i < tabPointures.length; i++) {
                for (let j = 0; j < tabNb[i]; j++) {
                  tabDonneesInitiales.push(tabPointures[i])
                }
              }
              break
          }
          me.logIfDebug('tabDonneesInitiales:', tabDonneesInitiales)
          for (let i = 0; i < nbDonnees; i++) {
            const pioche = j3pGetRandomInt(0, (tabDonneesInitiales.length - 1))
            newDonnees = tabDonneesInitiales[pioche]
            tabDonneesInitiales.splice(pioche, 1)
            tabDonnees.push(newDonnees)
            sommeDonnees += newDonnees
            sommeCarreDonnees += Math.pow(newDonnees, 2)
          }
        }
      }
      stor.moyenneVal = sommeDonnees / nbDonnees
      stor.ecartTypeVal = Math.sqrt(sommeCarreDonnees / nbDonnees - Math.pow(stor.moyenneVal, 2))
      stor.mini = j3pMin(tabDonnees).min
      stor.maxi = j3pMax(tabDonnees).max
      stor.etendueVal = Math.round((stor.maxi - stor.mini) * 1000) / 1000
      me.logIfDebug('moyenneVal', stor.moyenneVal, '   Ecart-type', stor.ecartTypeVal, 'tabDonnees rangées', tabDonnees, '  mini', stor.mini, '   maxi', stor.maxi)
      tabDonnees = j3pShuffle(tabDonnees)
      if (ds.petitEffectif) {
        // l’effectif étant petit, on écrit tout le calcul de la moyenne
        let numTxt = tabDonnees[0]
        for (let i = 1; i < tabDonnees.length; i++) {
          numTxt += '+' + tabDonnees[i]
        }
        stor.texteMoyenne = '\\frac{' + numTxt + '}{' + nbDonnees + '}'
      } else {
        stor.texteMoyenne = '\\frac{' + tabDonnees[0] + '+' + tabDonnees[1] + '+\\ldots+' + tabDonnees[tabDonnees.length - 1] + '}{' + nbDonnees + '}'
      }
      stor.tabDonnees = tabDonnees
    }
    stor.maQuestion = stor.tabQuestions[me.questionCourante - 1]
    if (ds.petitEffectif) {
      j3pAffiche(stor.zoneCons1, '', textes['consignePetit1_' + stor.choixSujet], {
        e: stor.tabDonnees.length
      })
      // Liste des valeurs :
      let listeVal = stor.tabDonnees[0]
      for (let i = 1; i < stor.tabDonnees.length; i++) {
        listeVal += '\\qquad;\\qquad' + stor.tabDonnees[i]
      }
      j3pAffiche(stor.zoneCons2, '', '$' + listeVal + '$.')
    } else {
      j3pAffiche(stor.zoneCons1, '', textes['consigne1_' + stor.choixSujet], { e: stor.tabDonnees.length })
      // la table ne sera que pour les effectifs qui ne seront pas petits
      const largTableur = me.zonesElts.MG.getBoundingClientRect().width
      stor.objTable = {
        parent: stor.zoneCons2,
        tabDonnees: stor.tabDonnees,
        lignes: stor.nbLignes,
        texte: (stor.maQuestion === 'moyenne')
          ? textes.table1
          : (stor.maQuestion === 'ecart type')
              ? textes.table2
              : textes.table3,
        largTableur
      }
      const maTable = creationTab(stor.objTable)
      stor.laCellule = maTable.nomCel
      stor.lastCol = maTable.lastCol
      stor.plagesCellules = []
      stor.plagesCellules.push('A1:' + stor.lastCol + stor.objTable.lignes)
      stor.plagesCellules.push('A' + stor.objTable.lignes + ':' + stor.lastCol + '1')
      stor.plagesCellules.push(stor.lastCol + stor.objTable.lignes + ':A1')
      stor.plagesCellules.push(stor.lastCol + '1:A' + stor.objTable.lignes)
      stor.mauvaisePlagesCellules = ['A1;' + stor.lastCol + stor.objTable.lignes]
      stor.mauvaisePlagesCellules.push('A' + stor.objTable.lignes + ';' + stor.lastCol + '1')
      stor.mauvaisePlagesCellules.push(stor.lastCol + '1;A' + stor.objTable.lignes)
      stor.mauvaisePlagesCellules.push(stor.lastCol + stor.objTable.lignes + ';A1')
    }
    let laConsigne2, laConsigne3
    let mesZonesSaisie, validePerso
    if (ds.questTableur && (((me.questionCourante - 1) % ds.nbetapes) % 2 === 0)) {
      // Question sur le tableur
      laConsigne2 = (stor.maQuestion === 'moyenne')
        ? textes.consigne2_1
        : (stor.maQuestion === 'ecart type')
            ? textes.consigne2_2
            : textes.consigne2_3
      j3pAffiche(stor.zoneCons3, '', laConsigne2, { c: 'B' + (stor.objTable.lignes + 1) })
      stor.elt = j3pAffiche(stor.zoneCons4, '', textes.consigne3, { input1: { dynamique: true, texte: '' } })
      mesZonesSaisie = [stor.elt.inputList[0].id]
      validePerso = [stor.elt.inputList[0].id]
      stor.elt.inputList[0].typeReponse = ['texte']
      stor.elt.inputList[0].reponse = []
      if (stor.maQuestion === 'moyenne') {
        for (let i = 0; i < stor.plagesCellules.length; i++) stor.elt.inputList[0].reponse.push('=MOYENNE(' + stor.plagesCellules[i] + ')')
        for (let i = 0; i < stor.plagesCellules.length; i++) stor.elt.inputList[0].reponse.push('=SOMME(' + stor.plagesCellules[i] + ')/' + stor.tabDonnees.length)
      } else if (stor.maQuestion === 'ecart type') {
        for (let i = 0; i < stor.plagesCellules.length; i++) stor.elt.inputList[0].reponse.push('=ECARTYPEP(' + stor.plagesCellules[i] + ')')
      } else { // c’est l’étendue
        for (let i = 0; i < stor.plagesCellules.length; i++) {
          const texteInit = '=MAX(' + stor.plagesCellules[i] + ')'
          for (let j = 0; j < stor.plagesCellules.length; j++) stor.elt.inputList[0].reponse.push(texteInit + '-MIN(' + stor.plagesCellules[j] + ')')
        }
      }
      me.logIfDebug('reponse:', stor.elt.inputList[0].reponse)
    } else {
      if (stor.maQuestion === 'moyenne') {
        // on demande la moyenne
        laConsigne2 = (ds.petitEffectif) ? textes['consignePetit4_' + stor.choixSujet] : textes['consigne4_' + stor.choixSujet]
        laConsigne3 = textes.consigne6_1
      } else if (stor.maQuestion === 'ecart type') {
        // on demande l’écart-type
        laConsigne2 = (ds.petitEffectif) ? textes['consignePetit5_' + stor.choixSujet] : textes['consigne5_' + stor.choixSujet]
        laConsigne3 = textes.consigne6_2
      } else {
        // on demande l’étendue
        laConsigne2 = (ds.petitEffectif) ? textes['consignePetit8_' + stor.choixSujet] : textes['consigne8_' + stor.choixSujet]
        laConsigne3 = textes.consigne6_3
      }
      j3pAffiche(stor.zoneCons3, '', laConsigne2, {
        e: stor.tabDonnees.length
      })
      stor.elt = j3pAffiche(stor.zoneCons4, '', laConsigne3,
        {
          input1: { dynamique: true, texte: '' }
        })
      j3pRestriction(stor.elt.inputList[0], '0-9.,')
      stor.repQuest = (stor.maQuestion === 'moyenne')
        ? stor.moyenneVal
        : (stor.maQuestion === 'ecart type')
            ? stor.ecartTypeVal
            : stor.etendueVal
      stor.elt.inputList[0].addEventListener('input', j3pRemplacePoint)
      stor.elt.inputList[0].typeReponse = (Math.abs(stor.repQuest * 1000 - Math.round(stor.repQuest * 1000)) < Math.pow(10, -12)) ? ['nombre', 'exact'] : ['nombre', 'arrondi', 0.1]
      if (ds.petitEffectif) {
        if ((stor.choixSujet === 3) || (stor.choixSujet === 4)) {
          // l’arrondi est alors à l’unité
          stor.elt.inputList[0].typeReponse[2] = 1
        }
      }
      stor.elt.inputList[0].reponse = [stor.repQuest]
      if (stor.elt.inputList[0].typeReponse[1] === 'arrondi') {
        stor.zoneCons5 = j3pAddElt(stor.conteneur, 'div')
        if (ds.petitEffectif && ((stor.choixSujet === 3) || (stor.choixSujet === 4))) {
          j3pAffiche(stor.zoneCons5, '', textes.consigne7_2)
        } else {
          j3pAffiche(stor.zoneCons5, '', textes.consigne7)
        }
      }
      mesZonesSaisie = [stor.elt.inputList[0].id]
      validePerso = []
    }
    j3pFocus(stor.elt.inputList[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // Le paramètre définit la largeur relative de la première colonne
        stor.pourcMG = 70

        me.construitStructurePage({ structure: 'presentation1', ratioGauche: stor.pourcMG / 100 })
        const leTitre = (ds.typeQuest === 'moyenne')
          ? textes.titre_exo1
          : (ds.typeQuest === 'ecart type')
              ? textes.titre_exo2
              : (ds.typeQuest === 'etendue')
                  ? textes.titre_exo3
                  : (ds.typeQuest === 'moyenne+ecart type')
                      ? textes.titre_exo4
                      : (ds.typeQuest === 'moyenne+etendue')
                          ? textes.titre_exo5
                          : (ds.typeQuest === 'ecart type+etendue') ? textes.titre_exo6 : textes.titre_exo7
        if (ds.petitEffectif) {
          // Si on veut du tableur, on ne peut pas avoir un petit effectif, cela n’a aucun intérêt.
          ds.questTableur = false
        }
        me.afficheTitre(leTitre)
        const listeQuest = ['moyenne', 'ecart type', 'etendue']
        if (ds.typeQuest === 'les trois') {
          ds.nbetapes = (ds.questTableur) ? 6 : 3
        } else if (listeQuest.indexOf(ds.typeQuest) === -1) {
          ds.nbetapes = (ds.questTableur) ? 4 : 2
        } else {
          ds.nbetapes = (ds.questTableur) ? 2 : 1
        }
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        // tableau contenant toutes les questions successives qui seront posées
        stor.tabQuestions = []
        for (let j = 1; j <= ds.nbrepetitions; j++) {
          switch (ds.typeQuest) {
            case 'les trois':
              for (let i = 0; i < listeQuest.length; i++) {
                if (ds.questTableur) stor.tabQuestions.push(listeQuest[i])
                stor.tabQuestions.push(listeQuest[i])
              }
              break
            case 'moyenne+ecart type':
              if (ds.questTableur) stor.tabQuestions.push('moyenne')
              stor.tabQuestions.push('moyenne')
              if (ds.questTableur) stor.tabQuestions.push('ecart type')
              stor.tabQuestions.push('ecart type')
              break
            case 'moyenne+etendue':
              if (ds.questTableur) stor.tabQuestions.push('moyenne')
              stor.tabQuestions.push('moyenne')
              if (ds.questTableur) stor.tabQuestions.push('etendue')
              stor.tabQuestions.push('etendue')
              break
            case 'ecart type+etendue':
              if (ds.questTableur) stor.tabQuestions.push('ecart type')
              stor.tabQuestions.push('ecart type')
              if (ds.questTableur) stor.tabQuestions.push('etendue')
              stor.tabQuestions.push('etendue')
              break
            default:
              stor.tabQuestions.push(ds.typeQuest)
              break
          }
        }
        // console.log("stor.tabQuestions:",stor.tabQuestions)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        stor.tabChoixSujet = [1, 2, 3, 4, 5]
        stor.tabChoixSujetInit = [1, 2, 3, 4, 5]
        suite()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        let reponse
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        let presenceEgal, bonnePlage, pbPlagePointVirgule, bonneFonction, autreFctECARTYPE, presenceETENDUE, plage
        if (ds.questTableur && (((me.questionCourante - 1) % ds.nbetapes) % 2 === 0)) {
        // question sur le tableur
          reponse = { aRepondu: false, bonneReponse: false }
          reponse.aRepondu = fctsValid.valideReponses()
          if (reponse.aRepondu) {
            let reponseDonnee = fctsValid.zones.reponseSaisie[0].replace(/é/g, 'e')
            reponseDonnee = reponseDonnee.replace(/\s/g, '')
            reponseDonnee = reponseDonnee.toUpperCase()
            presenceEgal = (reponseDonnee[0] === '=')
            bonnePlage = (reponseDonnee.indexOf(stor.plagesCellules[0]) > -1)
            pbPlagePointVirgule = (reponseDonnee.indexOf(stor.mauvaisePlagesCellules[0]) > -1)
            bonneFonction = (stor.maQuestion === 'moyenne') ? (reponseDonnee.indexOf('MOYENNE') > -1) : (reponseDonnee.indexOf('ECARTYPEP') > -1)
            autreFctECARTYPE = (stor.maQuestion === 'moyenne') ? true : ((reponseDonnee.indexOf('ECARTYPEP') === -1) && (reponseDonnee.indexOf('ECARTYPE') > -1))
            presenceETENDUE = (stor.maQuestion === 'etendue') ? (reponseDonnee.indexOf('ETENDUE') > -1) : false
            // recherche de la plage de données éventuellement saisie
            plage = reponseDonnee.substring(reponseDonnee.indexOf('(') + 1, reponseDonnee.lastIndexOf(')'))
            for (let i = 1; i <= 3; i++) {
              bonnePlage = (bonnePlage || (reponseDonnee.indexOf(stor.plagesCellules[i]) > -1))
              pbPlagePointVirgule = (pbPlagePointVirgule || (reponseDonnee.includes(stor.mauvaisePlagesCellules[i])))
            }
            me.logIfDebug('reponseDonnee:', reponseDonnee, '  reponses attendues :', stor.elt.inputList[0].reponse)
            for (let i = 0; i < stor.elt.inputList[0].reponse.length; i++) {
              reponse.bonneReponse = (reponse.bonneReponse || (String(reponseDonnee) === String(stor.elt.inputList[0].reponse[i])))
            }
            if (reponse.bonneReponse) {
              stor.elt.inputList[0].value = reponseDonnee
              j3pAutoSizeInput(stor.elt.inputList[0])
            }
            fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
            fctsValid.coloreUneZone(stor.elt.inputList[0].id)
          }
        } else {
          reponse = fctsValid.validationGlobale()
        }
        // A cet instant reponse contient deux éléments :

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (ds.questTableur && (((me.questionCourante - 1) % ds.nbetapes) % 2 === 0)) {
                if (!presenceEgal) {
                  stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
                }
                if (stor.maQuestion === 'moyenne') {
                  if (!bonneFonction) {
                    stor.zoneCorr.innerHTML += '<br/>' + textes.comment2
                  }
                } else if (stor.maQuestion === 'ecart type') {
                  if (autreFctECARTYPE) {
                    stor.zoneCorr.innerHTML += '<br/>' + textes.comment3
                  } else if (!bonneFonction) {
                    stor.zoneCorr.innerHTML += '<br/>' + textes.comment2
                  }
                } else {
                  if (presenceETENDUE) {
                    stor.zoneCorr.innerHTML += '<br/>' + textes.comment8
                  }
                }
                if (pbPlagePointVirgule) {
                  stor.zoneCorr.innerHTML += '<br/>' + textes.comment4
                } else if ((plage !== '') && !bonnePlage) {
                  stor.zoneCorr.innerHTML += '<br/>' + textes.comment5
                }
              } else {
                if (stor.elt.inputList[0].typeReponse[1] === 'arrondi') {
                  if (Math.abs(stor.elt.inputList[0].reponse[0] - j3pNombre(fctsValid.zones.reponseSaisie[0])) <= 2 * stor.elt.inputList[0].typeReponse[2]) {
                  // pb d’arrondi
                    stor.zoneCorr.innerHTML += '<br>' + textes.comment6
                  }
                }
                // pour l’écart-type, on peut s’interroger s’il n’a pas pris la mauvaise valeur de la calculatrice
                if (stor.maQuestion === 'ecart type') {
                // Je vérifie si l’élève n’a pas pris l’autre valeur (xsigma_{n-1})
                  if (Math.abs(j3pNombre(fctsValid.zones.reponseSaisie[0]) - stor.ecartTypeVal2) < 0.1) {
                    stor.zoneCorr.innerHTML += '<br>' + textes.comment7
                  }
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                if (ds.questTableur && (((me.questionCourante - 1) % ds.nbetapes) % 2 === 0)) {
                  me.typederreurs[3]++// On comptabilise le nombre de fautes aux formules du tableur
                } else {
                  me.typederreurs[4]++// On comptabilise le nombre de fautes sur les données chiffrées
                }
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const tauxEchecTableur = (ds.questTableur) ? me.typederreurs[3] / (ds.nbitems / 2) : 0
        const tauxEchecCalculs = (ds.questTableur) ? me.typederreurs[4] / (ds.nbitems / 2) : me.typederreurs[4] / ds.nbitems
        if (me.score / ds.nbitems >= ds.seuilReussite) {
          me.parcours.pe = ds.pe_1
        } else if ((tauxEchecTableur > ds.seuilErreur) && (tauxEchecCalculs <= ds.seuilErreur)) {
          me.parcours.pe = ds.pe_2
        } else if ((tauxEchecCalculs > ds.seuilErreur) && (tauxEchecTableur <= ds.seuilErreur)) {
          me.parcours.pe = ds.pe_3
        } else {
          me.parcours.pe = ds.pe_4
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
