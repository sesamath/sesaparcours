import $ from 'jquery'
import { j3pAddElt, j3pNombre, j3pEmpty, j3pExtraireCoefsFctAffine, j3pFocus, j3pGetRandomBool, j3pGetRandomInt, j3pMonome, j3pVirgule, j3pStyle, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2020
        Cette section permet le calcul des coordonnées du point moyen dans le cas d’une série statistique double
 */

const typeQuestDefault = [1]

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuest', typeQuestDefault, 'array', 'Tableau contenant l’ensemble des questions souhaitées dans cet exercice\n· 1 : point moyen\n· 2 : covariance\n· 3 : coefficient de corrélation\n· 4 : droite de régression\n· 5 : interpolation utilisant la droite']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Statistiques à deux variables',
  titre_exo1: 'Recherche du point moyen',
  titre_exo2: 'Covariance',
  titre_exo3: 'Coefficient de corrélation',
  titre_exo4: 'Droite de regression',
  // on donne les phrases de la consigne
  // Inspiré de La Réunion juin 2002 ES
  consigne1_1: 'Un coureur régulier note ses temps de passage lors d’un entrainement.',
  // Inspiré de Amérique du Sud nov 2002 ES
  consigne2_1: 'Un négociant en vins a fait mener une étude visant à déterminer à quel prix maximal ses clients sont prêts à acheter une bouteille de vin.',
  consigne2_2: 'Les résultats sont regroupés dans le tableau suivant&nbsp;:',
  // inspiré d’Antilles juin 2003 ES
  consigne3_1: 'La production nette d’électricité nucléaire d’un pays, en milliards de kWh, est donnée par le tableau suivant (où les années sont données à partir de l’an 2000)&nbsp;:',
  // inspiré de bac STT Reunion sept 2006
  consigne4_1: 'Dans une entreprise qui fabrique et vent un seul produit, le relevé des ventes mensuelles et des charges (en centaines d’euros) donne le tableau suivant&nbsp;:',
  // Inspiré de France Septembre 2008 STT
  consigne5_1: 'Les rations journalières conseillées sur des sacs de croquettes pour chien sont données ci-dessous&nbsp;:',
  // Inspiré de STG Mercatique La réunion Sept 2008
  consigne6_1: 'Selon l’institut national de la statistique et des études économiques (INSEE) un indice des prix a suivi, en France, l’évolution suivante&nbsp;:',
  // inspiré de Nouvelle Calédonie novembre 2009 STG
  consigne7_1: 'Un particulier s’intéresse à la revente de son véhicule.',
  consigne7_2: 'Souhaitant connaître le prix auquel il pourra le revendre, il consulte l’Argus afin de connaître sa cote et obtient le tableau suivant&nbsp;:',
  // Inspiré de nouvelle Calédonie Mars 2012 STG
  consigne8_1: 'Le tableau suivant donne la superficie et le prix d’appartements anciens vendus récemment dans le centre d’une petite ville&nbsp;:',
  // inspiré de mercatique Pondichery 2012 STG
  consigne9_1: 'Un site est spécialisé dans la diffusion de vidéos courtes sur Internet.',
  consigne9_2: 'Le responsable du site a constaté que la durée de chargement des vidéos évoluait en fonction du nombre d’internautes connectés simultanément.',
  consigne9_3: 'Le tableau ci-dessous représente les mesures constatées&nbsp;:',
  consigneptMoyen1: 'Cette série peut être représentée sous la forme d’un nuage de points.',
  consigneptMoyen2: 'Les coordonnées du point moyen de ce nuage sont (&1&;&2&).',
  consigneCov1_1: 'Le point moyen du nuage a pour coordonnées $(£x;£y)$.',
  consigneCov1_2: 'Le point moyen du nuage a pour coordonnées $(£x;£y)$ (même s’il faudra plutôt utiliser les valeurs exactes).',
  consigneCov2: 'La covariance de $x$ et $y$ vaut £e&1&.',
  consignecoefCorr1: 'A l’aide de la calculatrice, le coefficient de corrélation linéaire de la série statistique vaut environ &1&.',
  consignecoefCorr2: 'Un ajustement affine de la série statistique #1# judicieux.',
  consigneDroiteReg1_1: 'Donner (en utilisant la calculatrice) l’équation réduite de la droite d’ajustement affine obtenue par la méthode des moindres carrés&nbsp;:',
  consigneDroiteReg1_2: 'Un ajustement affine étant judicieux, donner (en utilisant la calculatrice) l’équation réduite de la droite d’ajustement affine obtenue par la méthode des moindres carrés&nbsp;:',
  consigneDroiteReg2: 'Les coefficients seront arrondis à £a.',
  consigneInter1: 'La droite de régression de $y$ en $x$ a pour équation $£e$.',
  consigneInter1_1: 'Si le coureur conserve un rythme régulier, son temps de passage à £a mètres sera de &1& s.',
  consigneInter1_2: 'Si le prix de la bouteille s’élève à £a euros, &1& % des clients de ce négociants seront prêts à acheter cette bouteille.',
  consigneInter1_3: 'Si l’évolution de la production reste la même, l’année de rang £a, on peut prévoir &1& milliards de kWh.',
  consigneInter1_4: 'Si on vend £a produits, on peut prévoir &1& centaines d’euros de charges.',
  consigneInter1_5: 'Pour un chien de £a&nbsp;kg, la ration journalière conseillée est de &1& g.',
  consigneInter1_6: 'On suppose que le modèle reste valable les années suivantes, l’année de rang £a, on peut ainsi prévoir un indice &1&.',
  consigneInter1_7: 'Si le modèle reste le même à l’avenir, à l’âge de £a&nbsp;ans, la cote de son véhicule sera de &1& euros.',
  consigneInter1_8: 'Selon ce modèle, on peut estimer un appartement de £a&nbsp;m$^2$ au prix de &1& centaines d’euros.',
  consigneInter1_9: 'Selon ce modèle, on peut estimer qu’il faudra &1& s pour télécharger la vidéo si £a milliers d’internautes sont connectés.',
  liste1: 'semble',
  liste2: 'ne semble pas',
  legendeX1: 'Distance parcourue $x_i$ (en mètres)',
  legendeY1: 'Temps de passage $y_i$ (en secondes)',
  legendeX2: 'Prix maximal $x_i$\nen euros de la bouteille',
  legendeY2: 'Pourcentage $y_i$\nd’acheteurs potentiels',
  legendeX3: 'Année $x_i$',
  legendeY3: 'Production $y_i$',
  legendeX4: 'Nombre de ventes $x_i$',
  legendeY4: 'Montant des charges $y_i$\n(en centaines d’euros)',
  legendeX5: 'Poids du chien $x_i$ (kg)',
  legendeY5: 'Ration journalière conseillée$y_i$ (en g)',
  legendeX6: 'Rang de l’année $x_i$',
  legendeY6: 'Indice $y_i$',
  legendeX7: 'Age $x_i$',
  legendeY7: 'Cote $y_i$',
  legendeX8: 'Superficie $x_i$ en m²',
  legendeY8: 'Prix $y_i$ (en\ncentaines d’euros)',
  legendeX9: 'Nombre d’internautes connectés\n$x_i$ (en milliers)',
  legendeY9: 'Durée de chargement de la vidéo\n$y_i$ (en secondes)',
  comment1: 'On donnera les valeurs arrondies à $£a$.',
  comment2: 'On donnera les valeurs arrondies à l’unité.',
  comment3: 'On donnera la valeur arrondie à $£a$.',
  comment4: 'On donnera la valeur arrondie à l’unité.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  arrondi: 'Peut-être est-ce un problème d’arrondi&nbsp;?',
  environ: 'environ ',
  yEgal: 'Il faut donner une équation réduite de droite, c’est-à-dire une équation de la forme $y=ax+b$.',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'On effectue la moyenne des valeurs de $x$ et celle des valeurs de $y$.',
  corr1_3: 'Donc les coordonnées du point moyen de ce nuage sont £e$(£x;£y)$.',
  corr2: '$\\overline{x}=\\frac{£{s1}}{£n}£{e1}£{rx}$ et $\\overline{y}=\\frac{£{s2}}{£n}£{e2}£{ry}$.',
  corr3: 'On rappelle la formule de la covariance&nbsp;:',
  corr4: 'Sa détermination peut se faire à l’aide de la calculatrice&nbsp;:',
  corr5: 'Dans la partie statistique de la calculatrice (ou Regression suivant les modèles), on complète les colonnes par les données. On peut ensuite demander les paramètres statistiques.',
  corr6: 'Pour certains modèles, on trouve directement la valeur de la covariance, pour d’autres on devra utiliser les valeurs $\\Sigma xy$, $\\overline{x}$ et $\\overline{y}$.',
  corr7: 'La covariance vaut alors £e$£c$ (ce que donne le calcul $\\frac{£s}{£n}-£{xbarre}\\times £{ybarre}$).',
  corr8: 'Attention à prendre des valeurs très précises de $\\overline{x}$ et $\\overline{y}$ pour limiter les erreurs sur la covariance.',
  corr9: 'Dans la partie statistique (ou Regression suivant les modèles), on complète les colonnes par les données.',
  corr10: 'On demande alors les paramètres de la droite de régression.',
  corr11: 'Le coefficient de corrélation noté $r$ vaut alors environ £r.',
  corr12_1: 'Ce coefficient étant proche de £u, un ajustement affine du nuage de points est judicieux.',
  corr12_2: 'Ce coefficient étant trop éloigné de £u, un ajustement affine du nuage de points n’est pas judicieux.',
  corr13: 'On obtient $a£{e1}£a$ et $b£{e2}£b$ et l’équation réduite de la droite de régression de $y$ en $x$ est $£{eq}$.',
  corr14: 'On pose $x=£x$ et dans ce cas $£c$.',
  corr15_1: 'Donc il peut prévoir de parcourir £x&nbsp;mètres en £r secondes.',
  corr15_2: 'Donc s’il fixe le prix à £x&nbsp;euros, il peut prévoir que £r&nbsp;% de ses clients vont acheter la bouteille.',
  corr15_3: 'Donc on peut prévoir une production de £r&nbsp;kWh l’année de rang £x.',
  corr15_4: 'Donc on peut prévoir £r centaines (soit £{r2}) euros de charges pour £x produits vendus.',
  corr15_5: 'Donc on peut prévoir une ration journalière de £r&nbsp;g pour un chien de £x&nbsp;kg.',
  corr15_6: 'Donc on peut prévoir un indice £r l’année de rang £x.',
  corr15_7: 'Donc son véhicule sera coté £r&nbsp;euros quand il aura £x&nbsp;ans.',
  corr15_8: 'Donc un appartement de £x&nbsp;m$^2$ devrait se vendre £r centaines (soit £{r2}) euros selon ce modèle.',
  corr15_9: 'Donc il faudra £r&nbsp;s pour télécharger la vidéo si £x milliers d’internautes sont connectés'
}

/**
 * section statsDeuxVariables
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    let nbZoneMax
    if (stor.questCourante === 2) {
      nbZoneMax = 7
    } else if (stor.questCourante === 3) {
      nbZoneMax = 4
    } else {
      nbZoneMax = 3
    }
    for (let i = 1; i <= nbZoneMax; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')

    switch (stor.questCourante) {
      case 1: {
        j3pAffiche(stor.zoneExpli1, '', textes.corr1_1)
        const objCorr2 = {}
        objCorr2.s1 = stor.objParam.sommeXtxt
        objCorr2.s2 = stor.objParam.sommeYtxt
        objCorr2.n = stor.x.length
        objCorr2.rx = (stor.zonesInput[0].typeReponse[1] === 'exact') ? stor.objParam.Gx : Math.round(stor.objParam.Gx / stor.zonesInput[0].typeReponse[2]) / Math.round(1 / stor.zonesInput[0].typeReponse[2])
        objCorr2.ry = (stor.zonesInput[1].typeReponse[1] === 'exact') ? stor.objParam.Gy : Math.round(stor.objParam.Gy / stor.zonesInput[1].typeReponse[2]) / Math.round(1 / stor.zonesInput[1].typeReponse[2])
        objCorr2.e1 = (Math.abs(objCorr2.rx - stor.objParam.Gx) < Math.pow(10, -10)) ? '=' : '\\approx'
        objCorr2.e2 = (Math.abs(objCorr2.ry - stor.objParam.Gy) < Math.pow(10, -10)) ? '=' : '\\approx'
        j3pAffiche(stor.zoneExpli2, '', textes.corr2, objCorr2)
        j3pAffiche(stor.zoneExpli3, '', textes.corr1_3, {
          x: (stor.zonesInput[0].typeReponse[1] === 'exact') ? stor.objParam.Gx : objCorr2.rx,
          y: (stor.zonesInput[1].typeReponse[1] === 'exact') ? stor.objParam.Gy : objCorr2.ry,
          e: ((objCorr2.e1 === '=') && (objCorr2.e2 === '=')) ? '' : textes.environ
        })
        break
      }

      case 2:
        j3pAffiche(stor.zoneExpli1, '', textes.corr3)
        j3pAffiche(stor.zoneExpli2, '', '$\\mathrm{Cov}(x,y)=\\frac{1}{' + stor.x.length + '}\\Sigma xy-\\overline{x}\\cdot\\overline{y}$')
        j3pAffiche(stor.zoneExpli3, '', textes.corr4)
        j3pAffiche(stor.zoneExpli4, '', textes.corr5)
        j3pAffiche(stor.zoneExpli5, '', textes.corr6)
        j3pAffiche(stor.zoneExpli6, '', textes.corr7, {
          c: (stor.zonesInput[0].typeReponse[1] === 'exact') ? stor.objParam.Covxy : Math.round(stor.objParam.Covxy / stor.zonesInput[0].typeReponse[2]) / Math.round(1 / stor.zonesInput[0].typeReponse[2]),
          s: j3pVirgule(stor.objParam.Sommexy),
          n: stor.x.length,
          xbarre: j3pVirgule(Math.round(10000 * stor.objParam.Gx) / 10000),
          ybarre: j3pVirgule(Math.round(10000 * stor.objParam.Gy) / 10000),
          e: (stor.zonesInput[0].typeReponse[1] === 'exact') ? '' : textes.environ
        })
        if ((Math.abs(stor.objParam.GxArrondi - stor.objParam.Gx) > Math.pow(10, -10)) || (Math.abs(stor.objParam.GyArrondi - stor.objParam.Gy) > Math.pow(10, -10))) {
          j3pAffiche(stor.zoneExpli7, '', textes.corr8, {
            style: { fontStyle: 'italic', fontSize: '0.95em' }
          })
        }
        break
      case 3:
        j3pAffiche(stor.zoneExpli1, '', textes.corr9)
        j3pAffiche(stor.zoneExpli2, '', textes.corr10)
        j3pAffiche(stor.zoneExpli3, '', textes.corr11, {
          r: j3pVirgule(stor.objParam.coefCorArrondi)
        })
        {
          me.logIfDebug('stor.zonesInput[0].reponse[0]:', stor.zonesInput[0].reponse[0], stor.zonesInput[1].reponse)
          const ajuste = (stor.zonesInput[1].reponse[0] === 1) ? textes.corr12_1 : textes.corr12_2
          const un = (stor.zonesInput[0].reponse[0] > 0) ? '1' : '-1'
          j3pAffiche(stor.zoneExpli4, '', ajuste, { u: un })
        }
        break
      case 4:
        // stor.objParam.coefRegArrondi
        j3pAffiche(stor.zoneExpli1, '', textes.corr9)
        j3pAffiche(stor.zoneExpli2, '', textes.corr10)
        {
          const equation = 'y=' + j3pMonome(1, 1, stor.objParam.coefRegArrondi[0]) + j3pMonome(2, 0, stor.objParam.coefRegArrondi[1])
          j3pAffiche(stor.zoneExpli3, '', textes.corr13, {
            a: stor.objParam.coefRegArrondi[0],
            b: stor.objParam.coefRegArrondi[1],
            e1: (Math.abs(stor.objParam.coefReg[0] - stor.objParam.coefRegArrondi[0]) < Math.pow(10, -10)) ? '=' : '\\approx',
            e2: (Math.abs(stor.objParam.coefReg[1] - stor.objParam.coefRegArrondi[1]) < Math.pow(10, -10)) ? '=' : '\\approx',
            eq: equation
          })
        }
        break
      case 5:
        {
          const leSigne = (stor.zonesInput[0].typeReponse[1] === 'exact') ? '=' : '\\approx'
          const laReponse = (stor.zonesInput[0].typeReponse[1] === 'exact') ? Math.round(10000 * stor.objParam.yInter) / 10000 : stor.objParam.yInterArrondi
          const leCalcul = stor.objParam.coefRegArrondi[0] + '\\times ' + stor.objParam.xInter + j3pMonome(2, 0, stor.objParam.coefRegArrondi[1]) + leSigne + laReponse
          j3pAffiche(stor.zoneExpli1, '', textes.corr14, {
            x: stor.objParam.xInter,
            c: leCalcul
          })
          const laReponse2 = ([4, 8].indexOf(stor.quest) > -1) ? laReponse * 100 : ''
          j3pAffiche(stor.zoneExpli2, '', textes['corr15_' + stor.quest], {
            r: j3pVirgule(laReponse),
            x: stor.objParam.xInter,
            r2: j3pVirgule(laReponse2)
          })
        }
        break
    }
    const tabOverline = document.querySelectorAll('.mq-overline')
    tabOverline.forEach(elt => {
      const laCouleur = elt.style.color
      j3pStyle(elt, { borderTop: '1px solid ' + laCouleur })
    })
  }

  function creationTableau (mesDonnees) {
    // mesDonnees est un objet
    // mesDonnees.parent est le nom du div dans lequel est créé le tableau
    // mesDonnees.obj est le nom du div créé (celui du tableau)
    // mesDonnees.largeur est la largeur du tableau (utile pour configurer la largeur de chaque colonne)
    // mesDonnees.textes est un tableau de 3 éléments max : "legende pour x", "legende pour y", expression de z (fonction de y)
    // mesDonnees.x : tableau des abscisses
    // mesDonnees.y : tableaux des ordonnées
    // mesDonnees.z : tableaux des valeurs de z si on demande un chgt de var --> A voir s’il ne faut pas prévoir des zones de saisie
    function creerLigneTab (elt, valeurs, l1, l2) {
      // elt est l’élément html dans lequel construire la ligne
      // l1 et l2 sont respectivement la largeur de la première colonne et des suivantes
      const ligne = j3pAddElt(elt, 'tr')
      const eltLegende = j3pAddElt(ligne, 'td', { width: l1 + 'px' })
      for (let i = 1; i < valeurs.length; i++) {
        j3pAddElt(ligne, 'td', String(valeurs[i]).replace(/\./g, ','), { align: 'center', width: l2 + 'px' })
      }
      return eltLegende
    }
    const eltsLegende = []

    mesDonnees.parent.style.paddingTop = '10px'
    const leTableau = j3pAddElt(mesDonnees.parent, 'div')
    leTableau.setAttribute('width', mesDonnees.largeur + 'px')
    if (mesDonnees.x.length === mesDonnees.y.length) {
      let largeurTexte = 0
      for (let i = 0; i < mesDonnees.textes.length; i++) {
        // largeurTexte = Math.max(largeurTexte, mesDonnees.textes[i].textWidth(j3p.styles.petit.enonce.fontFamily, j3p.styles.petit.enonce.fontSize.substring(0, j3p.styles.petit.enonce.fontSize.length - 2)))
        const tabDonneesTexte = mesDonnees.textes[i].split('\n')
        for (let j = 0; j < tabDonneesTexte.length; j++) {
          largeurTexte = Math.max(largeurTexte, $(leTableau).textWidth(tabDonneesTexte[j], me.styles.petit.enonce.fontFamily, me.styles.petit.enonce.fontSize))
        }
      }
      largeurTexte += 10
      const largeurColonne = (mesDonnees.largeur - largeurTexte - 5) / mesDonnees.y.length
      const table = j3pAddElt(leTableau, 'table', { width: '100%', borderColor: 'black', border: 2, cellSpacing: 0 })
      eltsLegende.push(creerLigneTab(table, [mesDonnees.textes[0]].concat(mesDonnees.x), largeurTexte, largeurColonne))
      eltsLegende.push(creerLigneTab(table, [mesDonnees.textes[1]].concat(mesDonnees.y), largeurTexte, largeurColonne))
      if (mesDonnees.z) eltsLegende.push(creerLigneTab(table, [mesDonnees.textes[2]].concat(mesDonnees.z), largeurTexte, largeurColonne, 3))
      eltsLegende.forEach((elt, index) => j3pAffiche(elt, '', mesDonnees.textes[index]))
      // on renvoie un objet contenant le nom des zones de saisie (lorsqu’elles existent) et le tableau des effectifs cumulés croissants
      return { largeurCol1: largeurTexte, largeurAutreCol: largeurColonne }
    }
    j3pShowError('pb sur le nombre de valeurs dans les deux tableaux')
  }

  function genereDonnees (num) {
    // num est le numéro du cas de figure
    // Cette fonction renvoie un objet contenant les données (sous forme de deux tableaux), mais aussi les coordonnées du point moyen
    // la covariance, la variance, le coef de corrélation et les coef a et b de l’équation de la droite de regression
    let pbNegatif
    let valx, valy
    let objArrondi
    do {
      valx = []
      valy = []
      const lgDonnees = (num === 8) ? j3pGetRandomInt(5, 7) : j3pGetRandomInt(5, 6)
      let a
      let b
      let i
      objArrondi = {}
      // objArrondis contient des propriétés permettant de calculer les arrondis intermédiaires des différentes valeurs
      // objArrondis contient les propriétés ptMoyen, Covxy, Varx, Vary, coefCor, coefReg
      // Les valeurs par défaut des arrondis
      objArrondi.ptMoyen = 0.01
      objArrondi.Covxy = 1
      objArrondi.Varx = 1
      objArrondi.Vary = 1
      objArrondi.coefCor = 0.0001
      objArrondi.coefReg = 0.001
      objArrondi.Inter = 1
      let val1, pas, bornesDecalage
      const alea = j3pGetRandomBool()
      let plusmoins = (j3pGetRandomInt(0, 1) * 2 - 1)
      let decimale = 1// Cela peut servir pour ajouter des valeurs décimales (comme le cas N°6)
      let OK
      switch (num) {
        case 1:
          val1 = 150 + 50 * j3pGetRandomInt(0, 2)
          pas = 50 * j3pGetRandomInt(1, 2)
          valx.push(val1)
          for (i = 1; i < lgDonnees; i++) {
            valx.push(valx[i - 1] + pas)
          }
          // Dans l’exo, on avait y=0.214x-3.4
          a = j3pGetRandomInt(207, 218) / 1000
          b = j3pGetRandomInt(31, 37) / 10
          bornesDecalage = [2, 7]
          if (stor.typeQuest.length === 1 && stor.typeQuest[0] === 3) {
            // Nous sommes dans le cas où on demande le coef de corrélation seulement
            if (alea) { // on fait en sorte qu’on n’ait pas d’ajustement affine
              bornesDecalage[0] = j3pGetRandomInt(8, 12)
              bornesDecalage[1] = bornesDecalage[0] + j3pGetRandomInt(8, 13)
            }
          }
          objArrondi.xInter = valx[valx.length - 1] + 50 * j3pGetRandomInt(2, 4)// j’ai mis ça dans objArrondi pour facilement y accéder dans calculParametres (sans faire de chgt)
          break
        case 2:
          val1 = (j3pGetRandomInt(0, 1) === 0) ? 5 : 6
          pas = (val1 === 5) ? 5 : 4
          valx.push(val1)
          for (i = 1; i < lgDonnees; i++) {
            valx.push(valx[i - 1] + pas)
          }
          // Dans l’exo, on avait y=-3.2228x+90.0667
          a = -j3pGetRandomInt(317, 325) / 100
          b = j3pGetRandomInt(890, 920) / 10
          bornesDecalage = [2, 7]
          if (stor.typeQuest.length === 1 && stor.typeQuest[0] === 3) {
            // Nous sommes dans le cas où on demande le coef de corrélation seulement
            if (alea) { // on fait en sorte qu’on n’ait pas d’ajustement affine
              bornesDecalage[0] = j3pGetRandomInt(8, 12)
              bornesDecalage[1] = bornesDecalage[0] + j3pGetRandomInt(8, 13)
            }
          }
          objArrondi.Inter = 0.1
          do {
            OK = true
            objArrondi.xInter = j3pGetRandomInt(val1 + 5, valx[valx.length - 1] - 5)// j’ai mis ça dans objArrondi pour facilement y accéder dans calculParametres (sans faire de chgt)
            for (i = 1; i < valx.length - 1; i++) {
              if (Math.abs(valx[i] - objArrondi.xInter) < 2) OK = false
            }
          } while (!OK)
          break
        case 3:
          do {
            val1 = j3pGetRandomInt(3, 6)
            valx.push(val1)
            for (i = 1; i < lgDonnees; i++) {
              valx.push(valx[i - 1] + j3pGetRandomInt(1, 3))
            }
          } while (valx[valx.length - 1] > 19)
          // Dans l’exo, on avait y=12.136x+226.45 (en repartant de 05 et non de 85)
          a = j3pGetRandomInt(1200, 1250) / 100
          b = j3pGetRandomInt(215, 230)
          bornesDecalage = [10, 20]
          if (stor.typeQuest.length === 1 && stor.typeQuest[0] === 3) {
            // Nous sommes dans le cas où on demande le coef de corrélation seulement
            if (alea) { // on fait en sorte qu’on n’ait pas d’ajustement affine
              bornesDecalage[0] = j3pGetRandomInt(15, 20)
              bornesDecalage[1] = bornesDecalage[0] + j3pGetRandomInt(10, 17)
            }
          }
          objArrondi.xInter = valx[valx.length - 1] + j3pGetRandomInt(2, 4)// j’ai mis ça dans objArrondi pour facilement y accéder dans calculParametres (sans faire de chgt)
          break
        case 4:
          val1 = j3pGetRandomInt(10, 28)
          valx.push(val1)
          for (i = 1; i < lgDonnees; i++) {
            valx.push(j3pGetRandomInt(10, 28))
          }
          // Dans l’exo, on avait y=0.6023x+5.22
          a = j3pGetRandomInt(600, 610) / 1000
          b = j3pGetRandomInt(500, 550) / 100
          bornesDecalage = [0, 3]
          if (stor.typeQuest.length === 1 && stor.typeQuest[0] === 3) {
            // Nous sommes dans le cas où on demande le coef de corrélation seulement
            if (alea) { // on fait en sorte qu’on n’ait pas d’ajustement affine
              bornesDecalage[0] = j3pGetRandomInt(2, 3)
              bornesDecalage[1] = bornesDecalage[0] + j3pGetRandomInt(2, 4)
            }
          }
          {
            let leMax = val1
            for (i = 1; i < valx.length; i++) {
              if (valx[i] > leMax) leMax = valx[i]
            }
            objArrondi.Inter = 0.1
            objArrondi.xInter = leMax + j3pGetRandomInt(3, 6)// j’ai mis ça dans objArrondi pour facilement y accéder dans calculParametres (sans faire de chgt)
          }
          break
        case 5:
          val1 = 5 + 5 * j3pGetRandomInt(0, 1)
          pas = 5 * j3pGetRandomInt(1, 2)
          valx.push(val1)
          for (i = 1; i < lgDonnees; i++) {
            valx.push(valx[i - 1] + pas)
          }
          // Dans l’exo, on avait y=5.198x+36.37
          a = j3pGetRandomInt(515, 522) / 100
          b = j3pGetRandomInt(330, 370) / 10
          bornesDecalage = [4, 10]
          if (stor.typeQuest.length === 1 && stor.typeQuest[0] === 3) {
            // Nous sommes dans le cas où on demande le coef de corrélation seulement
            if (alea) { // on fait en sorte qu’on n’ait pas d’ajustement affine
              bornesDecalage[0] = j3pGetRandomInt(6, 10)
              bornesDecalage[1] = bornesDecalage[0] + j3pGetRandomInt(7, 15)
            }
          }
          objArrondi.Inter = 0.1
          objArrondi.xInter = valx[valx.length - 1] + j3pGetRandomInt(3, 8)// j’ai mis ça dans objArrondi pour facilement y accéder dans calculParametres (sans faire de chgt)
          break
        case 6:
          val1 = 1
          pas = 1
          valx.push(val1)
          for (i = 1; i < lgDonnees; i++) {
            valx.push(valx[i - 1] + pas)
          }
          // Dans l’exo, on avait y=2.164x+96.81
          a = j3pGetRandomInt(210, 220) / 100
          b = j3pGetRandomInt(950, 975) / 10
          bornesDecalage = [6, 15]
          if (stor.typeQuest.length === 1 && stor.typeQuest[0] === 3) {
            // Nous sommes dans le cas où on demande le coef de corrélation seulement
            if (alea) { // on fait en sorte qu’on n’ait pas d’ajustement affine
              bornesDecalage[0] = j3pGetRandomInt(10, 15)
              bornesDecalage[1] = bornesDecalage[0] + j3pGetRandomInt(7, 15)
            }
          }
          decimale = 0.1
          objArrondi.Inter = 0.1
          objArrondi.xInter = valx[valx.length - 1] + j3pGetRandomInt(3, 6)// j’ai mis ça dans objArrondi pour facilement y accéder dans calculParametres (sans faire de chgt)
          break
        case 7:
          objArrondi.ptMoyen = 0.1
          objArrondi.coefReg = 1
          val1 = 1
          pas = 1
          valx.push(val1)
          for (i = 1; i < lgDonnees; i++) {
            valx.push(valx[i - 1] + pas)
          }
          // Dans l’exo, on avait y=-2029x+17600
          a = -j3pGetRandomInt(2000, 2100)
          b = j3pGetRandomInt(1700, 1800) * 10
          bornesDecalage = [4, 9]
          if (stor.typeQuest.length === 1 && stor.typeQuest[0] === 3) {
            // Nous sommes dans le cas où on demande le coef de corrélation seulement
            if (alea) { // on fait en sorte qu’on n’ait pas d’ajustement affine
              bornesDecalage[0] = j3pGetRandomInt(16, 20)
              bornesDecalage[1] = bornesDecalage[0] + j3pGetRandomInt(10, 15)
            }
          }
          // console.log('valx:', valx, 'a:', a, 'b:', b)
          decimale = 100
          objArrondi.xInter = valx[valx.length - 1] + j3pGetRandomInt(2, 4)// j’ai mis ça dans objArrondi pour facilement y accéder dans calculParametres (sans faire de chgt)
          break
        case 8:
          val1 = 25 + j3pGetRandomInt(0, 5)
          valx.push(val1)
          for (i = 1; i < lgDonnees; i++) {
            valx.push(valx[i - 1] + j3pGetRandomInt(3, 7))
          }
          // Dans l’exo, on avait y=9.11x+46.2
          a = j3pGetRandomInt(900, 950) / 100
          b = j3pGetRandomInt(450, 480) / 10
          bornesDecalage = [7, 18]
          if (stor.typeQuest.length === 1 && stor.typeQuest[0] === 3) {
            // Nous sommes dans le cas où on demande le coef de corrélation seulement
            if (alea) { // on fait en sorte qu’on n’ait pas d’ajustement affine
              bornesDecalage[0] = j3pGetRandomInt(30, 40)
              bornesDecalage[1] = bornesDecalage[0] + j3pGetRandomInt(10, 17)
            }
          }
          objArrondi.xInter = valx[valx.length - 1] + j3pGetRandomInt(10, 15)// j’ai mis ça dans objArrondi pour facilement y accéder dans calculParametres (sans faire de chgt)
          break
        case 9:
          val1 = 1
          valx.push(val1)
          for (i = 1; i < lgDonnees; i++) {
            valx.push(valx[i - 1] + ((j3pGetRandomInt(0, 2) === 0) ? 2 : 1))
          }
          // Dans l’exo, on avait y=0.438x-0.19
          a = j3pGetRandomInt(420, 450) / 1000
          b = -j3pGetRandomInt(15, 20) / 100
          bornesDecalage = [1, 4]
          if (stor.typeQuest.length === 1 && stor.typeQuest[0] === 3) {
            // Nous sommes dans le cas où on demande le coef de corrélation seulement
            if (alea) { // on fait en sorte qu’on n’ait pas d’ajustement affine
              bornesDecalage[0] = j3pGetRandomInt(0, 3)
              bornesDecalage[1] = bornesDecalage[0] + j3pGetRandomInt(10, 15)
            }
          }
          objArrondi.Inter = 0.1
          objArrondi.xInter = valx[valx.length - 1] + j3pGetRandomInt(2, 3)// j’ai mis ça dans objArrondi pour facilement y accéder dans calculParametres (sans faire de chgt)
          decimale = 0.1
          break
      }
      let valSuivante, passage
      pbNegatif = false
      for (i = 0; i < lgDonnees; i++) {
        plusmoins = (num === 9) ? 1 : (j3pGetRandomInt(0, 1) * 2 - 1)
        passage = 0
        do {
          valSuivante = (decimale < 1)
            ? (Math.round((a * valx[i] + b) / decimale) / Math.round(1 / decimale) + plusmoins * j3pGetRandomInt(bornesDecalage[0], bornesDecalage[1]) * decimale)
            : (Math.round((a * valx[i] + b) / decimale) * decimale + plusmoins * j3pGetRandomInt(bornesDecalage[0], bornesDecalage[1]) * decimale)
          passage++
          if (passage === 50) break
        } while (valSuivante < 0)
        const newValy = Math.round(1000 * valSuivante) / 1000
        // console.log('newValy:', newValy, valSuivante)
        if (newValy <= 0) pbNegatif = true
        valy.push(newValy)
      }
    } while (pbNegatif)
    return { valx, valy, objArrondi }
  }

  function calculParametres (valx, valy, approx) {
    // valx et valy sont les valeurs sous la forme de deux tableaux dans le cas d’une série statistique double
    // Je calcule ici tous les paramètres
    // approx est un objet contenant la précision pour chacun des cas de figure
    const objParametres = {}
    let sommex = 0
    let sommey = 0
    let sommeCarrex = 0
    let sommeCarrey = 0
    let sommexy = 0
    let sommeXtxt = ''
    let sommeYtxt = ''
    for (let i = 0; i < valx.length; i++) {
      sommex += valx[i]
      sommey += valy[i]
      sommexy += valx[i] * valy[i]
      sommeCarrex += Math.pow(valx[i], 2)
      sommeCarrey += Math.pow(valy[i], 2)
      if (i === 0) {
        sommeXtxt += j3pVirgule(valx[i])
        sommeYtxt += j3pVirgule(valy[i])
      } else {
        sommeXtxt += '+' + j3pVirgule(valx[i])
        sommeYtxt += '+' + j3pVirgule(valy[i])
      }
    }
    objParametres.sommeXtxt = sommeXtxt
    objParametres.sommeYtxt = sommeYtxt
    objParametres.Gx = sommex / valx.length
    objParametres.Gy = sommey / valx.length
    objParametres.Sommexy = sommexy
    objParametres.Covxy = Math.round(Math.pow(10, 7) * (sommexy / valx.length - objParametres.Gx * objParametres.Gy)) / Math.pow(10, 7)
    objParametres.Varx = sommeCarrex / valx.length - Math.pow(objParametres.Gx, 2)
    objParametres.Vary = sommeCarrey / valx.length - Math.pow(objParametres.Gy, 2)
    objParametres.coefCor = objParametres.Covxy / Math.sqrt(objParametres.Varx * objParametres.Vary)
    objParametres.coefReg = [objParametres.Covxy / objParametres.Varx]
    objParametres.coefReg.push(objParametres.Gy - objParametres.Gx * objParametres.coefReg[0])

    objParametres.GxArrondi = Math.round(objParametres.Gx / approx.ptMoyen) / Math.round(1 / approx.ptMoyen)
    objParametres.GyArrondi = Math.round(objParametres.Gy / approx.ptMoyen) / Math.round(1 / approx.ptMoyen)
    objParametres.CovxyArrondi = Math.round(objParametres.Covxy / approx.Covxy) / Math.round(1 / approx.Covxy)
    objParametres.VarxArrondi = Math.round(objParametres.Varx / approx.Varx) / Math.round(1 / approx.Varx)
    objParametres.VaryArrondi = Math.round(objParametres.Vary / approx.Vary) / Math.round(1 / approx.Vary)
    objParametres.coefCorArrondi = Math.round(objParametres.coefCor / approx.coefCor) / Math.round(1 / approx.coefCor)
    objParametres.coefRegArrondi = [Math.round(objParametres.coefReg[0] / approx.coefReg) / Math.round(1 / approx.coefReg),
      Math.round(objParametres.coefReg[1] / approx.coefReg) / Math.round(1 / approx.coefReg)]
    // pour l’interpolation, j’utilise les valeurs approchées pour le calcul
    objParametres.xInter = approx.xInter
    objParametres.yInter = approx.xInter * objParametres.coefRegArrondi[0] + objParametres.coefRegArrondi[1]
    if (objParametres.yInter < 0) {
      do {
        objParametres.xInter--
        objParametres.yInter = objParametres.xInter * objParametres.coefRegArrondi[0] + objParametres.coefRegArrondi[1]
      } while (objParametres.yInter < 0)
    }
    objParametres.yInterArrondi = Math.round(objParametres.yInter / approx.Inter) / Math.round(1 / approx.Inter)
    return objParametres
  }

  function enonceInitFirst () {
    ds = me.donneesSection
    // faut contrôler les types
    if (typeof ds.typeQuest === 'string') {
      try {
        ds.typeQuest = JSON.parse(ds.typeQuest)
      } catch (error) {
        console.error(error)
        j3pShowError(
          Error(`paramètre typeQuest invalide : ${ds.typeQuest} => ${JSON.stringify(typeQuestDefault)} imposé`),
          { mustNotify: true, notifyData: { parcours: this } }
        )
        ds.typeQuest = typeQuestDefault
      }
    }
    if (!Array.isArray(ds.typeQuest)) {
      j3pShowError(
        Error(`paramètre typeQuest invalide : (${typeof ds.typeQuest}) ${JSON.stringify(ds.typeQuest)} => ${JSON.stringify(typeQuestDefault)} imposé`),
        { mustNotify: true, notifyData: { parcours: this } }
      )
      ds.typeQuest = typeQuestDefault
    }
    // reste à filtrer les valeurs
    if (!ds.typeQuest.every(n => Number.isInteger(n) && n > 0 && n < 6)) {
      let errMsg = `paramètre typeQuest invalide, il ne doit contenir que des entiers entre 1 et 5 (${JSON.stringify(ds.typeQuest)}`
      ds.typeQuest = ds.typeQuest.map(n => Number.isInteger(n) && n > 0 && n < 6)
      if (ds.typeQuest.length < 1) {
        errMsg += ` => ${JSON.stringify(typeQuestDefault)} imposé`
      }
      j3pShowError(Error(errMsg), { mustNotify: true, notifyData: { parcours: this } })
    }
    // on est sûr d’avoir un array non vide d’entiers valides dans stor
    stor.typeQuest = ds.typeQuest

    // Construction de la page
    stor.pourcentage = 0.7
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: stor.pourcentage })
    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    // code de création des fenêtres
    stor.tabQuest = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    stor.tabQuestInit = [...stor.tabQuest]
    ds.nbetapes = stor.typeQuest.length
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    // détermination du titre
    let leTitreExo
    if (stor.typeQuest.length === 1) {
      if (stor.typeQuest[0] === 1) {
        leTitreExo = textes.titre_exo1
      } else if (stor.typeQuest[0] === 2) {
        leTitreExo = textes.titre_exo2
      } else if (stor.typeQuest[0] === 3) {
        leTitreExo = textes.titre_exo3
      } else {
        leTitreExo = textes.titre_exo4
      }
    } else if (stor.typeQuest.length === 2 && stor.typeQuest.indexOf(4) > -1 && stor.typeQuest.indexOf(5) > -1) {
      leTitreExo = textes.titre_exo4
    } else {
      leTitreExo = textes.titre_exo
    }
    me.afficheTitre(leTitreExo)

    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.largeurTab = me.zonesElts.MG.getBoundingClientRect().width
    if (ds.nbetapes === 1 || me.questionCourante % ds.nbetapes === 1) {
      // Nouveau cas de figure
      if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
      const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
      stor.quest = stor.tabQuest[pioche]
      stor.tabQuest.splice(pioche, 1)
      let OK
      do {
        OK = true
        stor.lesDonnees = genereDonnees(stor.quest)
        stor.x = stor.lesDonnees.valx
        stor.y = stor.lesDonnees.valy
        stor.objParam = calculParametres(stor.x, stor.y, stor.lesDonnees.objArrondi)
        // Si on ne demande que le coef de corrélation et que celui-ci est trop proche de 0.9, je préfère modifier les données
        if (stor.typeQuest.length === 1 && stor.typeQuest[0] === 3) {
          OK = (Math.abs(stor.objParam.coefCor) > 0.95 || (Math.abs(stor.objParam.coefCor) < 0.87 && Math.abs(stor.objParam.coefCor) > 0.5))
        } else {
          OK = (Math.abs(stor.objParam.coefCor) > 0.98)
        }
        // console.log('stor.objParam.coefCor:', stor.objParam.coefCor)
      } while (!OK)
      stor.questTraitee = [] // tableau contenant la liste des questions déjà traitées (cela sera rappelé dans la questions suivantes)
    }

    stor.objValeurs = {
      parent: stor.zoneCons2,
      textes: [textes['legendeX' + stor.quest], textes['legendeY' + stor.quest]],
      x: stor.x,
      y: stor.y,
      largeur: stor.largeurTab
    }
    stor.objtabDonnees = creationTableau(stor.objValeurs)
    stor.questCourante = stor.typeQuest[(me.questionCourante - 1) % ds.nbetapes]
    // Dans zoneCons1 je mets des div avec toutes les consignes de la forme consignei_j
    for (let i = 1; i <= 4; i++) {
      if (textes['consigne' + stor.quest + '_' + i]) {
        stor['EnonceCons' + i] = j3pAddElt(stor.zoneCons1, 'div')
        j3pAffiche(stor['EnonceCons' + i], '', textes['consigne' + stor.quest + '_' + i])
      }
    }
    // console.log('stor.objValeurs:', stor.objValeurs)
    // console.log('objParam:', stor.objParam)
    let valExacte = true
    let larrondi
    stor.zonesInput = []
    switch (stor.questCourante) {
      case 1:
        // Point moyen
        j3pAffiche(stor.zoneCons3, '', textes.consigneptMoyen1)
        {
          const elt = j3pAffiche(stor.zoneCons4, '', textes.consigneptMoyen2, {
            inputmq1: { texte: '', dynamique: true },
            inputmq2: { texte: '', dynamique: true }
          })
          stor.zonesInput.push(elt.inputmqList[0], elt.inputmqList[1])
          // On vérifie si on attend une valeur exacte ou une valeur arrondie
          larrondi = stor.lesDonnees.objArrondi.ptMoyen
          valExacte = valExacte && Math.abs(stor.objParam.Gx - stor.objParam.GxArrondi) < Math.pow(10, -10)
          valExacte = valExacte && Math.abs(stor.objParam.Gy - stor.objParam.GyArrondi) < Math.pow(10, -10)
          stor.zonesInput[0].typeReponse = ['nombre', 'exact']
          stor.zonesInput[1].typeReponse = ['nombre', 'exact']
          if (!valExacte) {
            stor.zonesInput[0].typeReponse[1] = 'arrondi'
            stor.zonesInput[0].typeReponse.push(stor.lesDonnees.objArrondi.ptMoyen)
            stor.zonesInput[1].typeReponse[1] = 'arrondi'
            stor.zonesInput[1].typeReponse.push(stor.lesDonnees.objArrondi.ptMoyen)
          }
        }
        stor.zonesInput[0].reponse = [stor.objParam.Gx]
        stor.zonesInput[1].reponse = [stor.objParam.Gy]
        stor.mesZonesSaisie = stor.zonesInput.map(elt => elt.id)
        break
      case 2:
        // Covariance
        larrondi = stor.lesDonnees.objArrondi.Covxy
        valExacte = Math.abs(stor.objParam.Covxy - stor.objParam.CovxyArrondi) < Math.pow(10, -10)
        if (stor.questTraitee.indexOf(1) > -1) {
          // On a traité la question sur le point moyen, donc on rappelle le résultat
          const consigneCov = ((Math.abs(stor.objParam.Gx - stor.objParam.GxArrondi) < Math.pow(10, -10)) && Math.abs(stor.objParam.Gy - stor.objParam.GyArrondi) < Math.pow(10, -10))
            ? textes.consigneCov1_1
            : textes.consigneCov1_2
          j3pAffiche(stor.zoneCons3, '', consigneCov, {
            x: stor.objParam.GxArrondi,
            y: stor.objParam.GyArrondi
          })
        }
        {
          const elt = j3pAffiche(stor.zoneCons4, '', textes.consigneCov2, {
            e: (valExacte) ? '' : textes.environ,
            inputmq1: { texte: '', dynamique: true }
          })
          stor.zonesInput.push(elt.inputmqList[0])
          stor.zonesInput[0].typeReponse = ['nombre', 'exact']
          if (!valExacte) {
            stor.zonesInput[0].typeReponse[1] = 'arrondi'
            stor.zonesInput[0].typeReponse.push(stor.lesDonnees.objArrondi.Covxy)
          }
          stor.zonesInput[0].reponse = [stor.objParam.Covxy]
        }
        stor.mesZonesSaisie = stor.zonesInput.map(elt => elt.id)
        break
      case 3:
        {
        // Coef de corrélation
          const elt = j3pAffiche(stor.zoneCons3, '', textes.consignecoefCorr1, {
            inputmq1: { texte: '', dynamique: true }
          })
          stor.zonesInput.push(elt.inputmqList[0])
          larrondi = stor.lesDonnees.objArrondi.coefCor
          valExacte = false
          stor.zonesInput[0].typeReponse = ['nombre', 'arrondi', stor.lesDonnees.objArrondi.coefCor]
          stor.zonesInput[0].reponse = [stor.objParam.coefCor]
          const elt2 = j3pAffiche(stor.zoneCons4, '', textes.consignecoefCorr2, {
            liste1: { texte: ['', textes.liste1, textes.liste2] }
          })
          stor.zonesInput.push(elt2.selectList[0])
          stor.mesZonesSaisie = [stor.zonesInput[0].id, elt2.selectList[0].id]
          elt2.selectList[0].reponse = [(Math.abs(stor.objParam.coefCor) > 0.9) ? 1 : 2]
        }
        break
      case 4:
        {
        // droite de régression
          const consigneDroiteReg = (stor.questTraitee.indexOf(3) > -1) ? textes.consigneDroiteReg1_2 : textes.consigneDroiteReg1_1
          j3pAffiche(stor.zoneCons3, '', consigneDroiteReg)
          const elt = j3pAffiche(stor.zoneCons4, '', '&1&', {
            inputmq1: { texte: '' }
          })
          stor.zonesInput.push(elt.inputmqList[0])
          if ((Math.abs(stor.objParam.coefReg[0] - stor.objParam.coefRegArrondi[0]) > Math.pow(10, -10)) && (Math.abs(stor.objParam.coefReg[1] - stor.objParam.coefRegArrondi[1]) > Math.pow(10, -10))) {
          // c’est que les coef ne sont pas exacts
            j3pAffiche(stor.zoneCons5, '', textes.consigneDroiteReg2, {
              a: j3pVirgule(stor.lesDonnees.objArrondi.coefReg),
              style: { fontStyle: 'italic', fontSize: '0.9em' }
            })
          }
        }
        stor.mesZonesSaisie = [stor.zonesInput[0].id]
        break
      case 5:
        larrondi = stor.lesDonnees.objArrondi.Inter
        // interpolation utilisant la droite
        {
          const equation = 'y=' + j3pMonome(1, 1, stor.objParam.coefRegArrondi[0]) + j3pMonome(2, 0, stor.objParam.coefRegArrondi[1])
          j3pAffiche(stor.zoneCons3, '', textes.consigneInter1, { e: equation })
          const elt = j3pAffiche(stor.zoneCons4, '', textes['consigneInter1_' + stor.quest], {
            a: stor.objParam.xInter,
            inputmq1: { texte: '', dynamique: true }
          })
          stor.zonesInput.push(elt.inputmqList[0])
          valExacte = (Math.abs(stor.objParam.yInter - Math.round(stor.objParam.yInter / (stor.lesDonnees.objArrondi.Inter / 10)) * (stor.lesDonnees.objArrondi.Inter / 10)) < Math.pow(10, -10))
          if (valExacte) {
            stor.zonesInput[0].typeReponse = ['nombre', 'exact']
          } else {
            stor.zonesInput[0].typeReponse = ['nombre', 'arrondi', stor.lesDonnees.objArrondi.Inter]
          }
          stor.zonesInput[0].reponse = [stor.objParam.yInter]
        }
        stor.mesZonesSaisie = [stor.zonesInput[0].id]
        break
    }

    stor.questTraitee.push(stor.questCourante)
    let validePerso = []
    if (stor.questCourante === 4) {
      validePerso = [stor.zonesInput[0].id]
    } else {
      // Gestion de l’affichage de l’arrondi (de la forme 10^{-...} dès lors qu’on a 4 chiffres
      const puis = (String(larrondi).match(/0/g) || []).length
      if (puis >= 3) larrondi = '10^{-' + puis + '}'
      const commentArrondi = (stor.mesZonesSaisie.length === 1)
        ? (puis === 0) ? textes.comment4 : textes.comment3
        : (puis === 0) ? textes.comment2 : textes.comment1
      if (!valExacte) {
        j3pAffiche(stor.zoneCons5, '', commentArrondi, {
          a: larrondi,
          style: { fontStyle: 'italic', fontSize: '0.9em' }
        })
      }
    }
    stor.mesZonesSaisie.forEach(zoneInput => {
      const restric = (stor.questCourante === 4)
        ? '\\d,.yx+-*=ab'
        : (stor.questCourante === 3) ? '\\d,.-' : '\\d,.'
      if (zoneInput.includes('input')) mqRestriction(zoneInput, restric) // pour ne pas le faire avec une liste
    })
    j3pFocus(stor.zonesInput[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: stor.mesZonesSaisie,
      validePerso
    })
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      let i, yEgal, fctAffine
      const reponse = stor.fctsValid.validationGlobale()
      let pbArrondi = true
      if (stor.questCourante === 4) {
        // On vérifie qu’on a bien une équation reduite de droite
        if (reponse.aRepondu) {
          const repEleve = stor.fctsValid.zones.reponseSaisie[0]
          yEgal = repEleve.split('=').length === 2
          if (repEleve.split('=')[0] === 'y') {
            fctAffine = repEleve.split('=')[1]
          } else if (repEleve.split('=')[1] === 'y') {
            fctAffine = repEleve.split('=')[0]
          } else {
            yEgal = false
          }
          reponse.aRepondu = yEgal
        }
        if (reponse.aRepondu) {
          // Je cherche les coeff
          const tabCoef = j3pExtraireCoefsFctAffine(fctAffine)
          tabCoef[0] = j3pNombre(tabCoef[0])
          tabCoef[1] = j3pNombre(tabCoef[1])
          reponse.bonneReponse = true
          for (i = 0; i < 2; i++) {
            reponse.bonneReponse = reponse.bonneReponse && (Math.abs(tabCoef[i] - stor.objParam.coefReg[i]) < stor.lesDonnees.objArrondi.coefReg)
          }
          if (!reponse.bonneReponse) {
            // on vérifie si ce n’est aps un pb d’arrondi
            pbArrondi = (Math.abs(tabCoef[0] - stor.objParam.coefReg[0]) < 5 * stor.lesDonnees.objArrondi.coefReg) && (Math.abs(tabCoef[1] - stor.objParam.coefReg[1]) < 5 * stor.lesDonnees.objArrondi.coefReg)
          }
          stor.fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
          stor.fctsValid.coloreUneZone(stor.zonesInput[0].id)
        }
      }

      // A cet instant reponse contient deux éléments :
      if (reponse.aRepondu && !reponse.bonneReponse) {
        // on va vérifier si ce n’est aps un problème d’arrondi
        if (stor.questCourante !== 4) {
          for (i = 0; i < stor.mesZonesSaisie.length; i++) {
            if (!stor.fctsValid.zones.bonneReponse[i]) {
              pbArrondi = pbArrondi && (Math.abs(j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[i]) - stor.zonesInput[i].reponse[0]) < 5 * stor.zonesInput[i].typeReponse[2])
            }
          }
        }
      }

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // On peut ajouter un commentaire particulier.
        if (stor.questCourante === 4 && !yEgal) {
          me.reponseManquante() // appel sans zone, pour l’incrémentation correcte des indices sans rien afficher
          // y’a du LaTeX dedans, on passe par j3pAffiche
          j3pEmpty(stor.zoneCorr)
          j3pAffiche(stor.zoneCorr, '', textes.yEgal)
        } else {
          me.reponseManquante(stor.zoneCorr)
        }
        return me.finCorrection()
      }

      // Une réponse a été saisie

      // Bonne réponse
      if (reponse.bonneReponse) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        afficheCorrection(true)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse sans temps limite atteint
      stor.zoneCorr.innerHTML = cFaux
      if (pbArrondi) stor.zoneCorr.innerHTML += '<br/>' + textes.arrondi

      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // indication éventuelle ici
        // ici il a encore la possibilité de se corriger, on reste dans l’état correction
        return me.finCorrection()
      }

      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(false)
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation' :
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)
      break // case "navigation":
  }
}
