import { j3pAddElt, j3pBarre, j3pElement, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pNombre, j3pPaletteMathquill, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import Repere from 'src/legacy/outils/geometrie/Repere'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import { j3pCreeTexte } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Rémi DENIAUD
        Mars 2013
        retrouver la forme canonique d’une fonction polynôme de degré 2 à l’aide de la courbe
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[-3;3]', 'string', 'coefficient a dans l’écriture a(x-alpha)^2+beta. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[1;1]|[2;4]|[-4;-2]" par exemple pour qu’il soit égal à 1 dans un premier temps, positif ensuite puis négatif'],
    ['alpha', '[-4;4]', 'string', 'coefficient alpha dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre'],
    ['beta', '[-4;4]', 'string', 'coefficient beta dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.5, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],

  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'Problème de signes' },
    { pe_3: 'confusion abscisse et ordonnée' },
    { pe_4: 'Problème sur la forme canonique' },
    { pe_5: 'insuffisant' }
  ]
}
const textes = {
  titre_exo: 'Parabole et forme canonique (2)',
  consigne1: '$£f$ est une fonction polynôme de degré 2 définie sur $\\R$ et dont on donne la courbe représentative ci-dessous :',
  consigne2: 'Compléter $£f(x)$ sous forme canonique :',
  phrase_erreur1: 'Il y a un ou plusieurs problème(s) de signe !',
  phrase_erreur3: 'Tu confonds l’abscisse et l’ordonnée du sommet !',
  phrase_erreur4: 'Ce n’est pas la forme canonique du trinôme !',
  corr1: '$£f(x)$ est de la forme $a(x-\\alpha)^2+\\beta$ où $(\\alpha;\\beta)=\\left(£a;£b\\right)$ sont les coordonnées du sommet de la parabole.',
  corr2: 'De plus $£f(£x)=£y$ d’après la courbe et la résolution de cette équation nous donne $a=£a$.<br>On obtient ainsi $£g$.'
}
/**
 * section courbe_canonique2
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function testexpressions (poly1, poly2, variable) {
    // cette fonction vérifie si deux polynômes sont égaux
    // variable est la variable présente dans le polynôme
    const unarbre1 = new Tarbre(poly1, [variable])
    const unarbre2 = new Tarbre(poly2, [variable])
    let egaliteExp = true
    for (let i = 0; i <= 9; i++) egaliteExp = (egaliteExp && (Math.abs(unarbre1.evalue([i]) - unarbre2.evalue([i])) < Math.pow(10, -12)))
    return egaliteExp
  }
  function afficheCorrection (nomFct, bonneRep) {
    const alpha = me.stockage[12]
    const beta = me.stockage[13]
    const couleurCorr = (bonneRep) ? me.styles.cbien : me.styles.petit.correction.color
    j3pEmpty(stor.laPalette)
    j3pEmpty(stor.repere.idDivRepere)
    stor.repere.add({ type: 'point', nom: 'A', par1: alpha, par2: 0, fixe: true, visible: false, etiquette: false, style: { couleur: couleurCorr, epaisseur: 2, taille: 18 } })
    stor.repere.add({ type: 'point', nom: 'B', par1: alpha, par2: beta, fixe: true, visible: false, etiquette: false, style: { couleur: couleurCorr, epaisseur: 2, taille: 18 } })
    stor.repere.add({ type: 'point', nom: 'C', par1: 0, par2: beta, fixe: true, visible: false, etiquette: false, style: { couleur: couleurCorr, epaisseur: 2, taille: 18 } })
    stor.repere.add({ type: 'segment', nom: 's1', par1: 'A', par2: 'B', style: { couleur: couleurCorr, epaisseur: 2 } })
    stor.repere.add({ type: 'segment', nom: 's2', par1: 'B', par2: 'C', style: { couleur: couleurCorr, epaisseur: 2 } })
    stor.repere.construit()
    j3pElement('segmentBC').setAttribute('stroke-dasharray', '5,5')
    j3pElement('segmentAB').setAttribute('stroke-dasharray', '5,5')
    // J’ajoute les coordonnées du sommet
    const posAlphay = (beta > 0)
      ? me.stockage[21] + (0.8 / me.stockage[22]) * me.stockage[23]
      : me.stockage[21] - 6
    const posAlphax = me.stockage[20] + (me.stockage[12] / me.stockage[22]) * me.stockage[23] - 5
    const posBetax = (alpha > 0)
      ? me.stockage[20] - (0.6 / me.stockage[22]) * me.stockage[23] - 5
      : me.stockage[20] + (0.3 / me.stockage[22]) * me.stockage[23] - 5
    const posBetay = me.stockage[21] - (me.stockage[13] / me.stockage[22]) * me.stockage[23] + 6
    j3pCreeTexte(stor.repere.svg, { x: posAlphax, y: posAlphay, texte: me.stockage[12], taille: 12, couleur: '#800080', italique: false, fonte: 'times new roman', bold: true })
    j3pCreeTexte(stor.repere.svg, { x: posBetax, y: posBetay, texte: me.stockage[13], taille: 12, couleur: '#800080', italique: false, fonte: 'times new roman', bold: true })
    // et maintenant la phrase qui donne la réponse
    const formeCanonique = (Math.abs(me.stockage[12]) < Math.pow(10, -14))
      ? nomFct + '(x)=' + me.stockage[14] + 'x^2' + j3pMonome(2, 0, me.stockage[13])
      : nomFct + '(x)=' + me.stockage[14] + '(x' + j3pMonome(2, 0, -1 * me.stockage[12]) + ')^2' + j3pMonome(2, 0, me.stockage[13])
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { color: couleurCorr } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1,
      {
        f: nomFct,
        a: me.stockage[12],
        b: me.stockage[13]
      })
    j3pAffiche(zoneExpli2, '', textes.corr2,
      {
        f: nomFct,
        g: formeCanonique,
        x: stor.alphaplus1,
        y: stor.betaplusa,
        a: me.stockage[11]
      })
  }
  function constructionTabIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).includes('|')) {
      const tabA = texteIntervalle.split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (testIntervalleFermeEntiers.test(tabA[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(tabA[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if (!tabA[k] || (tabA[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(tabA[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tabA[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(tabA[k]))) {
            // on a un nombre
            tabIntervalle.push(tabA[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if (!texteIntervalle || (texteIntervalle === '')) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (testIntervalleFermeEntiers.test(texteIntervalle)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    return tabIntervalle
  }

  function enonceMain () {
    const o = me.styles.toutpetit.enonce
    const alpha = stor.coef_alpha[me.questionCourante - 1]
    const beta = stor.coef_beta[me.questionCourante - 1]
    if (stor.coef_a[me.questionCourante - 1] === '[0;0]') {
      j3pShowError('le coefficient a ne peut être nul', { vanishAfter: 5 })
      stor.coef_a[me.questionCourante - 1] = '[-4;4]'
    }

    let coefDifferents = true// en principe les coefs alpha et beta doivent être différents, sauf si les variables alpha et beta de l’exo sont égales'
    let coeffA, coeffAlpha, coeffBeta
    let pb2
    do {
      const [lemin1, lemax1] = j3pGetBornesIntervalle(stor.coef_a[me.questionCourante - 1])
      coeffA = j3pGetRandomInt(lemin1, lemax1)
      const [lemin2, lemax2] = j3pGetBornesIntervalle(alpha)
      coeffAlpha = j3pGetRandomInt(lemin2, lemax2)
      do {
        const [lemin, lemax] = j3pGetBornesIntervalle(beta)
        coeffBeta = j3pGetRandomInt(lemin, lemax)
        if (alpha !== beta) {
          coefDifferents = true
        } else {
          // on vérifie que alpha et beta ne sont pas imposés
          const valMinAlpha = alpha.split(';')[0].substring(1)
          const finAlpha = alpha.split(';')[1]
          const valMaxAlpha = finAlpha.substring(0, finAlpha.length - 1)
          if (valMinAlpha === valMaxAlpha) {
            coefDifferents = false
          }
        }
      } while ((coeffAlpha === coeffBeta) && coefDifferents)
      pb2 = coeffA === 0 || ((coeffAlpha === 0) && (alpha !== '[0;0]')) || ((coeffBeta === 0) && (beta !== '[0;0]')) || (Math.abs(coeffBeta + coeffA) > 9)
    } while (pb2)
    let coeffATxt
    if (coeffA === -1) {
      coeffATxt = '-'
    } else if (coeffA === 1) {
      coeffATxt = ''
    } else {
      coeffATxt = coeffA
    }
    me.stockage[11] = coeffA
    me.stockage[12] = coeffAlpha
    stor.alphaplus1 = j3pGetLatexSomme(coeffAlpha, '1')
    stor.betaplusa = j3pGetLatexSomme(coeffBeta, coeffA)
    me.stockage[13] = coeffBeta
    me.stockage[14] = coeffATxt
    const tabNomFct = ['f', 'g', 'h']
    const nomFct = tabNomFct[j3pGetRandomInt(0, 2)]
    // début de la consigne
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons1, '', textes.consigne1, { f: nomFct })

    const largZoneMG = Math.min(me.zonesElts.MG.getBoundingClientRect().width, 450)
    // construction du repère
    const absMin = Math.min(coeffAlpha - 2, -6)
    const absMax = Math.max(coeffAlpha + 2, 6)
    let ordMax, ordMin
    if (coeffBeta > 0) {
      if (coeffA > 0) {
        ordMax = Math.max(coeffBeta + coeffA + 2, 6)
        ordMin = Math.min(ordMax - 12, -1)
      } else {
        ordMax = Math.max(coeffBeta + 1, 6)
        ordMin = Math.min(ordMax - 12, -1)
      }
    } else {
      if (coeffA > 0) {
        ordMin = Math.min(coeffBeta - 1, -6)
        ordMax = Math.max(ordMin + 12, 1)
      } else {
        ordMin = Math.min(coeffBeta + coeffA - 2, -6)
        ordMax = Math.max(ordMin + 12, 1)
      }
    }
    const uniterep = (largZoneMG - 30) / (absMax - absMin)
    const largeurRepere = ((absMax - absMin) + 0.5) * uniterep
    const hauteurRepere = ((ordMax - ordMin) + 0.5) * uniterep
    const posOx = (0.25 - absMin) * uniterep
    const posOy = (0.25 + ordMax) * uniterep
    me.stockage[20] = posOx
    me.stockage[21] = posOy
    me.stockage[22] = 1
    me.stockage[23] = uniterep
    me.stockage[24] = nomFct
    // je crée le tableau de valeurs pour tracer la courbe
    const tabValeurCb = []
    const lepas = (absMax - absMin) / 300
    for (let i = 0; i < 300; i++) {
      tabValeurCb[i] = [absMin + lepas * i, coeffA * Math.pow(((absMin + lepas * i) - coeffAlpha), 2) + coeffBeta]
    }
    const leRepere = j3pAddElt(stor.conteneur, 'div')
    stor.repere = new Repere({
      keepWeaksIds: true, // @todo virer ça dès qu’on pourra
      idConteneur: leRepere,
      aimantage: false,
      visible: true,
      larg: largeurRepere,
      haut: hauteurRepere,

      pasdunegraduationX: 1,
      pixelspargraduationX: uniterep,
      pasdunegraduationY: 1,
      pixelspargraduationY: uniterep,
      debuty: 0,
      xO: posOx,
      yO: posOy,
      negatifs: true,
      fixe: true,
      trame: true,
      objets: [
        { type: 'courbe_tabvaleur', nom: 'c1', tab_val: tabValeurCb, style: { couleur: '#F00', epaisseur: 1 } }
      ]
    })
    stor.repere.construit()
    // zone question (dans zones.MD)
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    const zoneCons2 = j3pAddElt(stor.conteneurD, 'div')
    const zoneCons3 = j3pAddElt(stor.conteneurD, 'div')
    j3pAffiche(zoneCons2, '', textes.consigne2, { f: nomFct })
    // les réponses :
    me.stockage[1] = coeffAlpha
    me.stockage[2] = coeffBeta
    me.stockage[3] = Math.abs(coeffA)
    // bonne réponse
    me.stockage[4] = coeffA + '*(x' + j3pMonome(2, 0, -1 * coeffAlpha) + ')^2' + j3pMonome(2, 0, coeffBeta)
    // mauvaises solutions
    // pb de signe
    me.stockage[5] = coeffA + '*(x' + j3pMonome(2, 0, coeffAlpha) + ')^2' + j3pMonome(2, 0, coeffBeta)
    me.stockage[6] = coeffA + '*(x' + j3pMonome(2, 0, -coeffAlpha) + ')^2' + j3pMonome(2, 0, -coeffBeta)
    me.stockage[7] = coeffA + '*(x' + j3pMonome(2, 0, coeffAlpha) + ')^2' + j3pMonome(2, 0, -coeffBeta)
    // confusion dans les coordonnées du sommet
    me.stockage[8] = coeffA + '*(x' + j3pMonome(2, 0, -1 * coeffBeta) + ')^2' + j3pMonome(2, 0, coeffAlpha)
    me.stockage[9] = coeffA + '*(x' + j3pMonome(2, 0, coeffBeta) + ')^2' + j3pMonome(2, 0, coeffAlpha)
    me.stockage[10] = coeffA + '*(x' + j3pMonome(2, 0, -1 * coeffBeta) + ')^2' + j3pMonome(2, 0, -coeffAlpha)
    const elt = j3pAffiche(zoneCons3, '', '$£f(x)=$ &1&',
      {
        f: nomFct,
        inputmq1: { texte: '', taillepolice: o.taillepolice, couleur: o.color }
      })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d.,()^2²+x-', { commandes: ['puissance'] })
    stor.laPalette = j3pAddElt(stor.conteneurD, 'div', '', { style: { paddingTop: '10px' } })
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['puissance'] })
    j3pFocus(stor.zoneInput)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':

      if (me.debutDeLaSection) {
        me.construitStructurePage({ structure: 'presentation1bis', ratioGauche: 0.5 })

        me.stockage = [0, 0, 0, 0]
        // me.videLesZones();
        me.afficheTitre(textes.titre_exo)

        stor.coef_a = constructionTabIntervalle(ds.a, '[-4;4]')
        stor.coef_alpha = constructionTabIntervalle(ds.alpha, '[-5;5]')
        stor.coef_beta = constructionTabIntervalle(ds.beta, '[-5;5]')

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      enonceMain()

      break // case "enonce":

    case 'correction':
      // on teste si une réponse a été saisie
      {
        const repEleve1 = j3pValeurde(stor.zoneInput)
        me.logIfDebug('repEleve1:' + repEleve1)
        const aRepondu = (repEleve1 !== '')
        if ((!aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
          return me.finCorrection()
        } else {
        // une réponse a été saisie
          const bonneRep = []
          // on vérifie si le polynôme écrit est correct
          const repEleveXcas1 = j3pMathquillXcas(repEleve1)
          bonneRep[1] = testexpressions(repEleveXcas1, me.stockage[4], ['x'])
          // on vérifie s’il est bien sous forme canonique'
          let listeAccept1, listeAccept2, listeAccept3, listeAccept4
          if (Math.abs(me.stockage[1]) > Math.pow(10, -13)) {
          // les longueurs des deux expressions doivent être proches
            bonneRep[2] = (Math.abs(repEleveXcas1.length - me.stockage[4].length) <= 3)
            // (x-alpha)^2 est-il présent ?
            listeAccept1 = ['(x' + j3pMonome(2, 0, -me.stockage[1]) + ')^2', '(x' + j3pMonome(2, 0, -me.stockage[1]) + ')²', '\\left(x' + j3pMonome(2, 0, -me.stockage[1]) + '\\right)^2', '\\left(x' + j3pMonome(2, 0, -me.stockage[1]) + '\\right)²']
            listeAccept1.push('(' + (-me.stockage[1]) + '+x)^2', '(' + (-me.stockage[1]) + '+x)^2', '\\left(' + (-me.stockage[1]) + '+x\\right)^2', '\\left(' + (-me.stockage[1]) + '+x\\right)^2')
            listeAccept1.push('(-x' + j3pMonome(2, 0, me.stockage[1]) + ')^2', '(-x' + j3pMonome(2, 0, me.stockage[1]) + ')²', '\\left(-x' + j3pMonome(2, 0, me.stockage[1]) + ' \right)^2', '\\left(-x' + j3pMonome(2, 0, me.stockage[1]) + '\\right)²')
            listeAccept1.push('(' + (me.stockage[1]) + '-x)^2', '(' + (me.stockage[1]) + '-x)^2', '\\left(' + (me.stockage[1]) + '-x\\right)^2', '\\left(' + (me.stockage[1]) + '-x\\right)^2')
            // (x+alpha)^2 est-il présent ?
            listeAccept2 = ['(x' + j3pMonome(2, 0, me.stockage[1]) + ')^2', '(x' + j3pMonome(2, 0, me.stockage[1]) + ')²', '\\left(x' + j3pMonome(2, 0, me.stockage[1]) + '\\right)^2', '\\left(x' + j3pMonome(2, 0, me.stockage[1]) + '\\right)²']
            listeAccept2.push('(' + (me.stockage[1]) + '+x)^2', '(' + (me.stockage[1]) + '+x)^2', '\\left(' + (me.stockage[1]) + '+x\\right)^2', '\\left(' + (me.stockage[1]) + '+x\\right)^2')
            listeAccept2.push('(x' + j3pMonome(2, 0, me.stockage[1]) + ')^2', '(x' + j3pMonome(2, 0, me.stockage[1]) + ')²', '\\left(x' + j3pMonome(2, 0, me.stockage[1]) + '\\right)^2', '\\left(x' + j3pMonome(2, 0, me.stockage[1]) + '\\right)²')
            listeAccept2.push('(' + (me.stockage[1]) + 'x)^2', '(' + (me.stockage[1]) + '+x)^2', '\\left(' + (me.stockage[1]) + '+x\\right)^2', '\\left(' + (me.stockage[1]) + '+x\\right)^2')
          } else {
          // les longueurs des deux expressions doivent être proches
            bonneRep[2] = (Math.abs(repEleveXcas1.length - me.stockage[4].length) <= 4)
            // ax^2 est-il présent?
            if (Math.abs(me.stockage[11] - 1) < Math.pow(10, -14)) {
            // a==1
              listeAccept1 = ['(x)^2', 'x^2']
              listeAccept2 = ['(x)^2', 'x^2']
            } else if (Math.abs(me.stockage[11] + 1) < Math.pow(10, -14)) {
            // a==-1
              listeAccept1 = ['-(x)^2', '-x^2']
              listeAccept2 = ['-(x)^2', '-x^2']
            } else {
            // |a|!=1
              listeAccept1 = [me.stockage[11] + '(x)^2', me.stockage[11] + 'x^2']
              listeAccept2 = [me.stockage[11] + '(x)^2', me.stockage[11] + 'x^2']
            }
          }
          // utile pour tester la confusion entre alpha et beta
          if (Math.abs(me.stockage[2]) > Math.pow(10, -13)) {
          // (x-beta)^2 est-il présent ?
            listeAccept3 = ['(x' + j3pMonome(2, 0, -me.stockage[2]) + ')^2', '(x' + j3pMonome(2, 0, -me.stockage[2]) + ')²', '\\left(x' + j3pMonome(2, 0, -me.stockage[2]) + '\\right)^2', '\\left(x' + j3pMonome(2, 0, -me.stockage[2]) + '\\right)²']
            listeAccept3.push('(' + (-me.stockage[2]) + '+x)^2', '(' + (-me.stockage[2]) + '+x)^2', '\\left(' + (-me.stockage[2]) + '+x\\right)^2', '\\left(' + (-me.stockage[2]) + '+x\\right)^2')
            listeAccept3.push('(-x' + j3pMonome(2, 0, me.stockage[2]) + ')^2', '(-x' + j3pMonome(2, 0, me.stockage[2]) + ')²', '\\left(-x' + j3pMonome(2, 0, me.stockage[2]) + '\\right)^2', '\\left(-x' + j3pMonome(2, 0, me.stockage[2]) + '\\right)²')
            listeAccept3.push('(' + (me.stockage[2]) + '-x)^2', '(' + (me.stockage[2]) + '-x)^2', '\\left(' + (me.stockage[2]) + '-x\\right)^2', '\\left(' + (me.stockage[2]) + '-x\\right)^2')
            // (x+beta)^2 est-il présent ?
            listeAccept4 = ['(x' + j3pMonome(2, 0, me.stockage[2]) + ')^2', '(x' + j3pMonome(2, 0, me.stockage[2]) + ')²', '\\left(x' + j3pMonome(2, 0, me.stockage[2]) + '\\right)^2', '\\left(x' + j3pMonome(2, 0, me.stockage[2]) + '\\right)²']
            listeAccept4.push('(' + (me.stockage[2]) + '+x)^2', '(' + (me.stockage[2]) + '+x)^2', '\\left(' + (me.stockage[2]) + '+x\\right)^2', '\\left(' + (me.stockage[2]) + '+x\\right)^2')
            listeAccept4.push('(x' + j3pMonome(2, 0, me.stockage[2]) + ')^2', '(x' + j3pMonome(2, 0, me.stockage[2]) + ')²', '\\left(x' + j3pMonome(2, 0, me.stockage[2]) + '\\right)^2', '\\left(x' + j3pMonome(2, 0, me.stockage[2]) + '\\right)²')
            listeAccept4.push('(' + (me.stockage[2]) + 'x)^2', '(' + (me.stockage[2]) + '+x)^2', '\\left(' + (me.stockage[2]) + '+x\\right)^2', '\\left(' + (me.stockage[2]) + '+x\\right)^2')
          } else {
          // ax^2 est-il présent?
            if (Math.abs(me.stockage[11] - 1) < Math.pow(10, -14)) {
            // a==1
              listeAccept3 = ['(x)^2', 'x^2']
              listeAccept4 = ['(x)^2', 'x^2']
            } else if (Math.abs(me.stockage[11] + 1) < Math.pow(10, -14)) {
            // a==-1
              listeAccept3 = ['-(x)^2', '-x^2']
              listeAccept4 = ['-(x)^2', '-x^2']
            } else {
            // |a|!=1
              listeAccept3 = [me.stockage[11] + '(x)^2', me.stockage[11] + 'x^2']
              listeAccept4 = [me.stockage[11] + '(x)^2', me.stockage[11] + 'x^2']
            }
          }
          bonneRep[3] = false
          for (let i = 0; i < listeAccept1.length; i++) {
            bonneRep[3] = (bonneRep[3] || (repEleveXcas1.includes(listeAccept1[i])))
          }
          bonneRep[4] = false
          for (let i = 0; i < listeAccept2.length; i++) {
            bonneRep[4] = (bonneRep[4] || (repEleveXcas1.includes(listeAccept2[i])))
          }
          bonneRep[5] = false
          for (let i = 0; i < listeAccept3.length; i++) {
            bonneRep[5] = (bonneRep[5] || (repEleveXcas1.includes(listeAccept3[i])))
          }
          bonneRep[6] = false
          for (let i = 0; i < listeAccept4.length; i++) {
            bonneRep[6] = (bonneRep[6] || (repEleveXcas1.includes(listeAccept4[i])))
          }
          me.logIfDebug('bonneRep : ' + bonneRep)
          const bonneReponse = (bonneRep[1] && bonneRep[2] && bonneRep[3])
          if (bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = '<br><br>' + cBien
            me.typederreurs[0]++
            // on désactive la zone de saisie
            stor.zoneInput.setAttribute('style', 'color:' + me.styles.cbien)
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            // on affiche l’explication'
            afficheCorrection(me.stockage[24], true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // on met en rouge ce qui est faux
            stor.zoneInput.setAttribute('color', me.styles.cfaux)
            let numErreur = 0
            if (bonneRep[1]) {
            // cas d’une bonne forme développée mais d’une fausse forme canonique
              numErreur = 1
            } else {
            // pb de signe
              const pbSigne1 = testexpressions(repEleveXcas1, me.stockage[5], ['x'])
              const pbSigne2 = testexpressions(repEleveXcas1, me.stockage[6], ['x'])
              const pbSigne3 = testexpressions(repEleveXcas1, me.stockage[7], ['x'])
              me.logIfDebug('pbSigne:' + pbSigne1 + '  ' + pbSigne2 + '  ' + pbSigne3)
              if (((pbSigne1 || pbSigne3) && bonneRep[4]) || (pbSigne2 && bonneRep[3])) {
                numErreur = 2
              }
              const pbCoord1 = testexpressions(repEleveXcas1, me.stockage[8], ['x'])
              const pbCoord2 = testexpressions(repEleveXcas1, me.stockage[9], ['x'])
              const pbCoord3 = testexpressions(repEleveXcas1, me.stockage[10], ['x'])
              if ((pbCoord1 || pbCoord2 || pbCoord3) && (bonneRep[6] || bonneRep[5])) {
                numErreur = 3
              }
              me.logIfDebug('pbCoord:' + pbCoord1 + '  ' + pbCoord2 + '  ' + pbCoord3)
            }
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = '<br><br>' + tempsDepasse
              me.typederreurs[10]++
              afficheCorrection(me.stockage[24], false)
              j3pDesactive(stor.zoneInput)
            } else { // réponse fausse :
              stor.zoneCorr.innerHTML = '<br><br>' + cFaux
              if (numErreur === 1) {
              // problème car ce n’est pas la forme canonique qui est donnée (pourtant c’est le bon polynôme)'
                stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur4
              } else if (numErreur === 2) {
              // pb avec un ou plusieurs signes dans les coefs
                stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur1
              } else if (numErreur === 3) {
              // confusion entre l’abscisse et l’ordonnée du sommet'
                stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur3
              }
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                return me.finCorrection()
              } else {
              // Erreur au second essai
                me.typederreurs[2]++
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // on désactive la zone de saisie
                stor.zoneInput.setAttribute('style', 'color:' + me.styles.cfaux)
                j3pDesactive(stor.zoneInput)
                if (numErreur === 1) {
                  me.typederreurs[3]++
                } else if (numErreur === 2) {
                  me.typederreurs[4]++
                } else if (numErreur === 3) {
                  me.typederreurs[5]++
                } else {
                  me.typederreurs[6]++
                }
                // on barre les mauvaises réponses
                j3pBarre(stor.zoneInput)
                afficheCorrection(me.stockage[24], false)
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          const nbErreurs = ds.nbitems - me.score
          if (me.typederreurs[3] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de forme (non reconnaissance de la forme canonique) est supé au égal au seul fixé
            me.parcours.pe = ds.pe_4
          } else if (me.typederreurs[4] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de signes est supé au égal au seul fixé
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[5] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de confusion abscisse/ordonnée du sommet est supé au égal au seul fixé
            me.parcours.pe = ds.pe_3
          } else {
            me.parcours.pe = ds.pe_5
          }
        }
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
