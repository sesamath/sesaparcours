import { j3pAddElt, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pMonome, j3pRandomTab, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    octobre 2014
    On fournit l’expression d’une fonction trinôme sous la forme canonique. On demande de replacer deux points permettant de construire la courbe représentative
 */

/* global MathJax */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[-3;3]', 'string', 'Intervalle où se trouve la valeur a dans l’expression a(x-alpha)^2+beta (bien sûr a non nul).'],
    ['alpha', '[-4;4]', 'string', 'Intervalle où se trouve la valeur alpha dans l’expression a(x-alpha)^2+beta. Pour qu’il soit nul, on écrit "[0;0]"'],
    ['beta', '[-3;3]', 'string', 'Intervalle où se trouve la valeur betaa dans l’expression a(x-alpha)^2+beta. Pour qu’il soit nul, on écrit "[0;0]".'],
    ['a_different1', false, 'boolean', 'On peut imposer à a d’être différent de 1.'],
    ['sommet_mobile_seulement', false, 'boolean', 'On demande dans ce cas juste une translation de la courbe en déplaçant le sommet. alpha et beta ne sont pas tous les deux nuls dans ce cas.'],
    ['discriminant_nul', false, 'boolean', 'On peut proposer dans l’énoncé un trinôme de discriminant nul et deux points symétriques par l’axe de symétrie de la courbe'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.6, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'problème sur le sommet de la parabole' },
    { pe_3: 'Bien pour le sommet de la parabole, mais pas sur l’autre point à placer' },
    { pe_4: 'Insuffisant' }
  ]
}
const textes = {
  titre_exo: 'Forme canonique et courbe',
  consigne1: 'A et B sont deux points mobiles permettant de modifier l’aspect de la parabole.',
  consigne1bis: 'B est un point mobile permettant de modifier la parabole.',
  consigne2: 'Déplace ces deux points pour que la parabole représente la fonction $£f$ définie sur $\\R$ par $£f(x)=£g$.',
  consigne2bis: 'Déplace ce point pour que la parabole représente la fonction $£f$ définie sur $\\R$ par $£f(x)=£g$.',
  consigne3: 'Déplace ces deux points pour que la parabole soit la représentation d’un trinôme $£f(x)$ de discriminant nul et tel que $£f(£b)=£c$ et $£f(£d)=£c$.',
  comment1: 'Il faut déplacer au moins l’un des deux points !',
  comment1bis: 'Il faut déplacer le point B !',
  comment2: 'Le sommet de la parabole (point B) est bien placé, mais le point A ne l’est pas !',
  comment3: 'Il y a au moins le point B (sommet de la parabole) qui est mal placé !',
  comment4: 'Le point B, sommet de la parabole, est mal placé.',
  comment5: 'La courbe proposée n’est plus une parabole !',
  corr1: "$£f(x)$ s'écrit sous la forme $a(x-\\alpha)^2+\\beta$ où $a=£a$, $\\alpha=£b$ et $\\beta=£c$.",
  corr2: 'Le sommet de la parabole a alors pour coordonnées $(\\alpha;\\beta)=(£b;£c)$. Ce sont les coordonnées du point B.',
  corr3: 'De plus $£f(£x)=£y$ donc on peut placer le point A$(£x;£y)$.',
  corr4: 'Le discriminant du trinôme étant nul, la fonction s’annule en une seule valeur et le sommet de la parabole est sur l’axe des abscisses.',
  corr5: 'Comme $£f(£b)=£f(£c)$, on en déduit que l’axe de symétrie de la courbe a pour équation $£e$. B a donc pour coordonnées $£g$',
  corr6: 'Enfin on peut placer A de coordonnées $£c$.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    const zoneExpli = j3pAddElt(stor.conteneurG, 'div', '', { style: { color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    for (let i = 1; i <= 3; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    if (ds.discriminant_nul) {
      j3pAffiche(stor.zoneExpli1, '', textes.corr4)
      j3pAffiche(stor.zoneExpli2, '', textes.corr5,
        {
          f: stor.nom_f,
          b: stor.x1,
          c: stor.x2,
          e: 'x=' + (stor.x1 + stor.x2) / 2,
          g: '(' + (stor.x1 + stor.x2) / 2 + ';0)'
        })
      j3pAffiche(stor.zoneExpli3, '', textes.corr6, { c: '(' + stor.x1 + ';' + stor.y1 + ')' })
      modifFig({ num: 1, correction: true })
    } else {
      j3pAffiche(stor.zoneExpli1, '', textes.corr1,
        {
          f: stor.nom_f,
          a: stor.param.a,
          b: stor.param.alpha,
          c: stor.param.beta
        })
      j3pAffiche(stor.zoneExpli2, '', textes.corr2, { b: stor.param.alpha, c: stor.param.beta })
      if (ds.sommet_mobile_seulement) {
        modifFig({ num: 2, correction: true })
      } else {
        modifFig({ num: 1, correction: true })
        j3pAffiche(stor.zoneExpli3, '', textes.corr3,
          {
            f: stor.nom_f,
            x: stor.param.xA,
            y: stor.param.yA
          })
      }
    }
    // on rend maintenant inactive la figure mtg32
    stor.mtgAppLecteur.setActive(stor.mtg32svg, false)
  }

  function init () {
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  // appelé par enonceMain
  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = (ds.sommet_mobile_seulement)
      ? 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH2+v4BAP8BAAAAAAAAAAAC9gAAAjgAAAAAAAAAAAAAAAEAAABO#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEAB0NDYWxjdWwA#####wAIbWF1dmFpc0IAATAAAAABAAAAAAAAAAD#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAMAAFPAMAuAAAAAAAAQBAAAAAAAAAFAAFAZqAAAAAAAEBjoAAAAAAA#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAIBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8AAAAAAQwAAUkAwBAAAAAAAABAEAAAAAAAAAUAAUA9AAAAAAAAAAAAA#####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAADAAAAQABAAAAAgAAAAT#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AAAAAAAQAAABAAEAAAACAAAABf####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAIAAAAE#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAYAAAAH#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAMAoAAAAAAAAwBAAAAAAAAAFAAEAAAAIAAAACgD#####AAAAAAEMAAFKAMAoAAAAAAAAwBAAAAAAAAAFAAIAAAAI#####wAAAAIAB0NSZXBlcmUA#####wCAgIABAQAAAAIAAAAEAAAACgEBAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAP####8AAAABAAlDTG9uZ3VldXIA#####wAAAAIAAAAEAAAAAgD#####AAduYmdyYWR4AAIyMAAAAAFANAAAAAAAAAAAAAIA#####wAHbmJncmFkeQACMjAAAAABQDQAAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAXR3JhZHVhdGlvbkF4ZXNSZXBlcmVOZXcAAAAbAAAACAAAAAMAAAALAAAADQAAAA7#####AAAAAQATQ0Fic2Npc3NlT3JpZ2luZVJlcAAAAAAPAAVhYnNvcgAAAAv#####AAAAAQATQ09yZG9ubmVlT3JpZ2luZVJlcAAAAAAPAAVvcmRvcgAAAAv#####AAAAAQAKQ1VuaXRleFJlcAAAAAAPAAZ1bml0ZXgAAAAL#####wAAAAEACkNVbml0ZXlSZXAAAAAADwAGdW5pdGV5AAAAC#####8AAAABABBDUG9pbnREYW5zUmVwZXJlAAAAAA8AAAAAABAAAAEFAAAAAAv#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAEAAAABMAAAARAAAAEgAAAAAPAAAAAAAQAAABBQAAAAAL#####wAAAAEACkNPcGVyYXRpb24AAAAAEwAAABAAAAATAAAAEgAAABMAAAARAAAAEgAAAAAPAAAAAAAQAAABBQAAAAALAAAAEwAAABAAAAAUAAAAABMAAAARAAAAEwAAABP#####AAAAAQALQ0hvbW90aGV0aWUAAAAADwAAABQAAAATAAAADf####8AAAABAAtDUG9pbnRJbWFnZQAAAAAPAAAAAAAQAAABBQAAAAAVAAAAFwAAABUAAAAADwAAABQAAAATAAAADgAAABYAAAAADwAAAAAAEAAAAQUAAAAAFgAAABn#####AAAAAQAIQ1NlZ21lbnQAAAAADwEAAAAACQAAAQABAAAAFQAAABgAAAAXAAAAAA8BAAAAAAkAAAEAAQAAABYAAAAaAAAABQAAAAAPAQAAAAAKAAFXAMAUAAAAAAAAwDQAAAAAAAAFAAE#3FZ4mrzfDgAAABv#####AAAAAgAIQ01lc3VyZVgAAAAADwAGeENvb3JkAAAACwAAAB0AAAACAAAAAA8ABWFic3cxAA94Q29vcmQoVyxPLEksSikAAAATAAAAHv####8AAAACABJDTGlldU9iamV0UGFyUHRMaWUBAAAADwBmZmYAAAAdAAAAEwAAAA0AAAAdAAAAAgAAAB0AAAAdAAAAAgAAAAAPAAVhYnN3MgANMiphYnNvci1hYnN3MQAAABQBAAAAFAIAAAABQAAAAAAAAAAAAAATAAAAEAAAABMAAAAfAAAAEgAAAAAPAQAAAAAQAAABBQAAAAALAAAAEwAAACEAAAATAAAAEQAAABkBAAAADwBmZmYAAAAiAAAAEwAAAA0AAAAdAAAABQAAAB0AAAAeAAAAHwAAACEAAAAiAAAABQAAAAAPAQAAAAAKAAFSAEAgAAAAAAAAwCAAAAAAAAAFAAE#0RtOgbToHwAAABz#####AAAAAgAIQ01lc3VyZVkAAAAADwAGeUNvb3JkAAAACwAAACQAAAACAAAAAA8ABW9yZHIxAA95Q29vcmQoUixPLEksSikAAAATAAAAJQAAABkBAAAADwBmZmYAAAAkAAAAEwAAAA4AAAAkAAAAAgAAACQAAAAkAAAAAgAAAAAPAAVvcmRyMgANMipvcmRvci1vcmRyMQAAABQBAAAAFAIAAAABQAAAAAAAAAAAAAATAAAAEQAAABMAAAAmAAAAEgAAAAAPAQAAAAAQAAABBQAAAAALAAAAEwAAABAAAAATAAAAKAAAABkBAAAADwBmZmYAAAApAAAAEwAAAA4AAAAkAAAABQAAACQAAAAlAAAAJgAAACgAAAAp#####wAAAAIADENDb21tZW50YWlyZQAAAAAPAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAHQoAAfb6#gAAAAEAAAAAAAsjVmFsKGFic3cxKQAAABkBAAAADwBmZmYAAAArAAAAAUA0AAAAAAAAAAAAHQAAAAQAAAAdAAAAHgAAAB8AAAArAAAAGwAAAAAPAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAIgoAAfb6#gAAAAEAAAAAAAsjVmFsKGFic3cyKQAAABkBAAAADwBmZmYAAAAtAAAAAUA0AAAAAAAAAAAAHQAAAAYAAAAdAAAAHgAAAB8AAAAhAAAAIgAAAC0AAAAbAAAAAA8BZmZmAMAgAAAAAAAAP#AAAAAAAAAAAAAkCgAB9vr+AAAAAgAAAAEACyNWYWwob3JkcjEpAAAAGQEAAAAPAGZmZgAAAC8AAAABQDQAAAAAAAAAAAAkAAAABAAAACQAAAAlAAAAJgAAAC8AAAAbAAAAAA8BZmZmAMAcAAAAAAAAAAAAAAAAAAAAAAApCgAB9vr+AAAAAgAAAAEACyNWYWwob3JkcjIpAAAAGQEAAAAPAGZmZgAAADEAAAABQDQAAAAAAAAAAAAkAAAABgAAACQAAAAlAAAAJgAAACgAAAApAAAAMQAAAAIA#####wABYQACLTH#####AAAAAQAMQ01vaW5zVW5haXJlAAAAAT#wAAAAAAAA#####wAAAAEADUNQb2ludEJhc2VFbnQA#####wAAAAAADAABQgEFAAEAAAALAAAAAAAAAAAAAAAAAAAAAAEBAAAAGAD#####AAZ4Q29vcmQAAAALAAAANAAAABoA#####wAGeUNvb3JkAAAACwAAADQAAAACAP####8ABWFscGhhAA94Q29vcmQoQixPLEksSikAAAATAAAANQAAAAIA#####wAEYmV0YQAPeUNvb3JkKEIsTyxJLEopAAAAEwAAADb#####AAAAAQAFQ0ZvbmMA#####wABZgASYSooeC1hbHBoYSleMitiZXRhAAAAFAAAAAAUAgAAABMAAAAz#####wAAAAEACkNQdWlzc2FuY2UAAAAUAf####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAATAAAANwAAAAFAAAAAAAAAAAAAABMAAAA4AAF4AAAABQD#####AQAAAAAMAAJ4MQEFAAFAAXLoqYpNCAAAAAUAAAAYAP####8AB3hDb29yZDEAAAALAAAAOgAAAAIA#####wACeDEAEHhDb29yZCh4MSxPLEksSikAAAATAAAAOwAAAAIA#####wACeTEABWYoeDEp#####wAAAAEADkNBcHBlbEZvbmN0aW9uAAAAOQAAABMAAAA8AAAAEgD#####AQAAAAAQAAABBQAAAAALAAAAEwAAADwAAAATAAAAPf####8AAAACAA1DTGlldURlUG9pbnRzAP####8AAAAAAAEAAAA+AAAB9AABAAAAOgAAAAUAAAA6AAAAOwAAADwAAAA9AAAAPgAAAAIA#####wAGYWxwaGEyAAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAViZXRhMgABMAAAAAEAAAAAAAAAAAAAAAIA#####wAHYWZmaWNoZQABMAAAAAEAAAAAAAAAAAAAAB4A#####wABZwAgMS9hZmZpY2hlKihhKih4LWFscGhhMileMitiZXRhMikAAAAUAgAAABQDAAAAAT#wAAAAAAAAAAAAEwAAAEIAAAAUAAAAABQCAAAAEwAAADMAAAAfAAAAFAEAAAAgAAAAAAAAABMAAABAAAAAAUAAAAAAAAAAAAAAEwAAAEEAAXgAAAAFAP####8BAAAAAAwAAngyAQUAAb#6dW#z3pGeAAAABQAAABgA#####wAHeENvb3JkMgAAAAsAAABEAAAAAgD#####AAJ4MgAQeENvb3JkKHgyLE8sSSxKKQAAABMAAABFAAAAAgD#####AAJ5MgAFZyh4MikAAAAhAAAAQwAAABMAAABGAAAAEgD#####AQAAAAAQAAABBQAAAAALAAAAEwAAAEYAAAATAAAARwAAACIA#####wD#AAAAAQAAAEgAAAH0AAEAAABEAAAABQAAAEQAAABFAAAARgAAAEcAAABIAAAAEgD#####AP8AAAAQAAABBQAAAAALAAAAEwAAAEAAAAAUAgAAABQCAAAAFAIAAAAUAwAAAAE#8AAAAAAAAAAAABMAAAABAAAAFAMAAAABP#AAAAAAAAAAAAATAAAAQgAAABQDAAAAAT#wAAAAAAAAAAAAFAUAAAATAAAAMwAAAAEAAAAAAAAAAAAAABMAAABBAAAAGwD#####AP8AAABAGAAAAAAAAAAAAAAAAAAAAAAASgwAAAAAAAEAAAAAAAFCAAAAEgD#####AP8AAAAQAAABBQAAAAALAAAAEwAAAEAAAAAUAgAAABQCAAAAFAIAAAAUAwAAAAE#8AAAAAAAAAAAABMAAAABAAAAFAMAAAABP#AAAAAAAAAAAAATAAAAQgAAABQDAAAAAT#wAAAAAAAAAAAAFAQAAAATAAAAMwAAAAEAAAAAAAAAAAAAABMAAABBAAAAGwD#####AP8AAADAEAAAAAAAAAAAAAAAAAAAAAAATAwAAAAAAAEAAAACAAFCAAAADP##########'
      : 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH2+v4BAP8BAAAAAAAAAAAC9gAAAjgAAAAAAAAAAAAAAAEAAABa#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEAB0NDYWxjdWwA#####wAIbWF1dmFpc0EAATEAAAABP#AAAAAAAAAAAAACAP####8ACG1hdXZhaXNCAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAduYmdyYWR4AAIyMAAAAAFANAAAAAAAAAAAAAIA#####wAHYWZmaWNoZQABMAAAAAEAAAAAAAAAAP####8AAAABAApDUG9pbnRCYXNlAP####8AAAAAAAwAAU8AwC4AAAAAAABAEAAAAAAAAAUAAUBlwAAAAAAAQGOAAAAAAAD#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAACgAAAQABAAAABQE#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wAAAAABDAABSQDAEAAAAAAAAEAQAAAAAAAABQABQD0AAAAAAAAAAAAG#####wAAAAEACUNEcm9pdGVBQgD#####AAAAAAAMAAABAAEAAAAFAAAAB#####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8AAAAAAAoAAAEAAQAAAAUAAAAI#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAABAAAABQAAAAf#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAACQAAAAr#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAAMAAAAwCgAAAAAAADAEAAAAAAAAAUAAQAAAAsAAAAKAP####8AAAAAAQwAAUoAwCgAAAAAAADAEAAAAAAAAAUAAgAAAAv#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAAFAAAABwAAAAIA#####wAHbmJncmFkeQACMjAAAAABQDQAAAAAAAAAAAAFAP####8BAAAAAAwAAngxAQUAAb#DV2vXEMYAAAAACAAAAAIA#####wACYTIAATEAAAABP#AAAAAAAAAAAAACAP####8ABmFscGhhMgABMAAAAAEAAAAAAAAAAAAAAAIA#####wAFYmV0YTIAATAAAAABAAAAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wABZwAhMS9hZmZpY2hlKihhMiooeC1hbHBoYTIpXjIrYmV0YTIp#####wAAAAEACkNPcGVyYXRpb24CAAAADQMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAABAAAAA0AAAAADQIAAAAOAAAAEf####8AAAABAApDUHVpc3NhbmNlAAAADQH#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAADgAAABIAAAABQAAAAAAAAAAAAAAOAAAAEwABeAAAAAUA#####wEAAAAADAACeDIBBQABQAMLb8clpBgAAAAI#####wAAAAIAB0NSZXBlcmUA#####wCAgIABAQAAAAUAAAAHAAAADQEBAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABdHcmFkdWF0aW9uQXhlc1JlcGVyZU5ldwAAABsAAAAIAAAAAwAAABYAAAADAAAAD#####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAABcABWFic29yAAAAFv####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAABcABW9yZG9yAAAAFv####8AAAABAApDVW5pdGV4UmVwAAAAABcABnVuaXRleAAAABb#####AAAAAQAKQ1VuaXRleVJlcAAAAAAXAAZ1bml0ZXkAAAAW#####wAAAAEAEENQb2ludERhbnNSZXBlcmUAAAAAFwAAAAAADgAAAQUAAAAAFgAAAA4AAAAYAAAADgAAABkAAAAXAAAAABcAAAAAAA4AAAEFAAAAABYAAAANAAAAAA4AAAAYAAAADgAAABoAAAAOAAAAGQAAABcAAAAAFwAAAAAADgAAAQUAAAAAFgAAAA4AAAAYAAAADQAAAAAOAAAAGQAAAA4AAAAb#####wAAAAEAC0NIb21vdGhldGllAAAAABcAAAAcAAAADgAAAAP#####AAAAAQALQ1BvaW50SW1hZ2UAAAAAFwAAAAAADgAAAQUAAAAAHQAAAB8AAAAYAAAAABcAAAAcAAAADgAAAA8AAAAZAAAAABcAAAAAAA4AAAEFAAAAAB4AAAAh#####wAAAAEACENTZWdtZW50AAAAABcBAAAAAAkAAAEAAQAAAB0AAAAgAAAAGgAAAAAXAQAAAAAJAAABAAEAAAAeAAAAIgAAAAUAAAAAFwEAAAAACgABVwDAFAAAAAAAAMA0AAAAAAAABQABP9xWeJq83w4AAAAj#####wAAAAIACENNZXN1cmVYAAAAABcABnhDb29yZAAAABYAAAAlAAAAAgAAAAAXAAVhYnN3MQAPeENvb3JkKFcsTyxJLEopAAAADgAAACb#####AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAABcAZmZmAAAAJQAAAA4AAAADAAAAJQAAAAIAAAAlAAAAJQAAAAIAAAAAFwAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABgAAAAOAAAAJwAAABcAAAAAFwEAAAAACgAAAQUAAAAAFgAAAA4AAAApAAAADgAAABkAAAAcAQAAABcAZmZmAAAAKgAAAA4AAAADAAAAJQAAAAUAAAAlAAAAJgAAACcAAAApAAAAKgAAAAUAAAAAFwEAAAAACgABUgBAIAAAAAAAAMAgAAAAAAAABQABP9EbToG06B8AAAAk#####wAAAAIACENNZXN1cmVZAAAAABcABnlDb29yZAAAABYAAAAsAAAAAgAAAAAXAAVvcmRyMQAPeUNvb3JkKFIsTyxJLEopAAAADgAAAC0AAAAcAQAAABcAZmZmAAAALAAAAA4AAAAPAAAALAAAAAIAAAAsAAAALAAAAAIAAAAAFwAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABkAAAAOAAAALgAAABcAAAAAFwEAAAAACgAAAQUAAAAAFgAAAA4AAAAYAAAADgAAADAAAAAcAQAAABcAZmZmAAAAMQAAAA4AAAAPAAAALAAAAAUAAAAsAAAALQAAAC4AAAAwAAAAMf####8AAAACAAxDQ29tbWVudGFpcmUAAAAAFwFmZmYAAAAAAAAAAABAGAAAAAAAAAAAACUKAAH2+v4AAAABAAAAAAALI1ZhbChhYnN3MSkAAAAcAQAAABcAZmZmAAAAMwAAAAFANAAAAAAAAAAAACUAAAAEAAAAJQAAACYAAAAnAAAAMwAAAB4AAAAAFwFmZmYAAAAAAAAAAABAGAAAAAAAAAAAACoKAAH2+v4AAAABAAAAAAALI1ZhbChhYnN3MikAAAAcAQAAABcAZmZmAAAANQAAAAFANAAAAAAAAAAAACUAAAAGAAAAJQAAACYAAAAnAAAAKQAAACoAAAA1AAAAHgAAAAAXAWZmZgDAIAAAAAAAAD#wAAAAAAAAAAAALAoAAfb6#gAAAAIAAAABAAsjVmFsKG9yZHIxKQAAABwBAAAAFwBmZmYAAAA3AAAAAUA0AAAAAAAAAAAALAAAAAQAAAAsAAAALQAAAC4AAAA3AAAAHgAAAAAXAWZmZgDAHAAAAAAAAAAAAAAAAAAAAAAAMQoAAfb6#gAAAAIAAAABAAsjVmFsKG9yZHIyKQAAABwBAAAAFwBmZmYAAAA5AAAAAUA0AAAAAAAAAAAALAAAAAYAAAAsAAAALQAAAC4AAAAwAAAAMQAAADn#####AAAAAQANQ1BvaW50QmFzZUVudAD#####AAAAAAAMAAFBAQUAAQAAABY#8AAAAAAAAD#wAAAAAAAAAQEAAAAfAP####8AAAAAAAwAAUIBBQABAAAAFgAAAAAAAAAAAAAAAAAAAAABAQAAABsA#####wAGeENvb3JkAAAAFgAAADsAAAAdAP####8ABnlDb29yZAAAABYAAAA7AAAAHQD#####AAd5Q29vcmQxAAAAFgAAADwAAAAbAP####8AB3hDb29yZDEAAAAWAAAAPAAAAAIA#####wAFYWxwaGEAD3hDb29yZChCLE8sSSxKKQAAAA4AAABAAAAAAgD#####AARiZXRhAA95Q29vcmQoQixPLEksSikAAAAOAAAAPwAAAAIA#####wABYQAwKHlDb29yZChBLE8sSSxKKS1iZXRhKS8oeENvb3JkKEEsTyxJLEopLWFscGhhKV4yAAAADQMAAAANAQAAAA4AAAA+AAAADgAAAEIAAAAPAAAADQEAAAAOAAAAPQAAAA4AAABBAAAAAUAAAAAAAAAAAAAADAD#####AAFmABJhKih4LWFscGhhKV4yK2JldGEAAAANAAAAAA0CAAAADgAAAEMAAAAPAAAADQEAAAAQAAAAAAAAAA4AAABBAAAAAUAAAAAAAAAAAAAADgAAAEIAAXgAAAAbAP####8AB3hDb29yZDIAAAAWAAAAEAAAAAIA#####wACeDEAEHhDb29yZCh4MSxPLEksSikAAAAOAAAARQAAAAIA#####wACeTEABWYoeDEp#####wAAAAEADkNBcHBlbEZvbmN0aW9uAAAARAAAAA4AAABGAAAAFwD#####AQAAAAAMAAABBQAAAAAWAAAADgAAAEYAAAAOAAAAR#####8AAAACAA1DTGlldURlUG9pbnRzAP####8AAAAAAAEAAABIAAAB9AABAAAAEAAAAAUAAAAQAAAARQAAAEYAAABHAAAASAAAABsA#####wAHeENvb3JkMwAAABYAAAAVAAAAAgD#####AAJ4MgAQeENvb3JkKHgyLE8sSSxKKQAAAA4AAABKAAAAAgD#####AAJ5MgAFZyh4MikAAAAgAAAAFAAAAA4AAABLAAAAFwD#####AQAAAAAMAAABBQAAAAAWAAAADgAAAEsAAAAOAAAATAAAACEA#####wD#AAAAAQAAAE0AAAH0AAEAAAAVAAAABQAAABUAAABKAAAASwAAAEwAAABNAAAABQD#####AQAAAAAMAAJ4MwEFAAHAEMI24vayGgAAAAgAAAAbAP####8AB3hDb29yZDQAAAAWAAAATwAAAAIA#####wACeDMAEHhDb29yZCh4MyxPLEksSikAAAAOAAAAUAAAAAIA#####wACeEEAD3hDb29yZChBLE8sSSxKKQAAAA4AAAA9AAAAAgD#####AAJ5QQAPeUNvb3JkKEEsTyxJLEopAAAADgAAAD4AAAAXAP####8A#wAAAA4AAAEFAAAAABYAAAAOAAAAEgAAAA0CAAAADQIAAAANAgAAAA0DAAAAAT#wAAAAAAAAAAAADgAAAAIAAAANAwAAAAE#8AAAAAAAAAAAAA4AAAAEAAAADQMAAAABP#AAAAAAAAAAAAANBQAAAA4AAAARAAAAAQAAAAAAAAAAAAAADgAAABMAAAAeAP####8A#wAAAMAYAAAAAAAAv#AAAAAAAAAAAABUDAAAAAAAAQAAAAAAAUIAAAAXAP####8A#wAAAA4AAAEFAAAAABYAAAAOAAAAEgAAAA0CAAAADQIAAAANAgAAAA0DAAAAAT#wAAAAAAAAAAAADgAAAAIAAAANAwAAAAE#8AAAAAAAAAAAAA4AAAAEAAAADQMAAAABP#AAAAAAAAAAAAANBAAAAA4AAAARAAAAAQAAAAAAAAAAAAAADgAAABMAAAAeAP####8A#wAAAEAUAAAAAAAAAAAAAAAAAAAAAABWDAAAAAAAAQAAAAIAAUIAAAAXAP####8A#wAAAA4AAAEFAAAAABYAAAANAAAAAA4AAAASAAAAAT#wAAAAAAAAAAAADQIAAAANAwAAAAE#8AAAAAAAAAAAAA4AAAABAAAAIAAAABQAAAANAAAAAA4AAAASAAAAAT#wAAAAAAAAAAAAHgD#####AP8AAABAEAAAAAAAAD#wAAAAAAAAAAAAWAwAAAAAAAAAAAABAAFBAAAADv##########'

    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
  } // depart

  function modifFig (st) {
    const mtgAppLecteur = stor.mtgAppLecteur
    // st contiet des propriétés :
    // num qui vaut 1 lorsqu’il y a deux moins mobile, 2 losqu’il n’y a que le sommet'
    // ici les figures ne sont pas rechargées au début. Par contre, elles le seront pour la correction
    if (st.correction) {
      const affiche = '1'// servira à afficher la courbe solution
      const a2 = stor.param.a
      const alpha2 = stor.param.alpha
      const beta2 = stor.param.beta
      const bonB = (stor.bon_sommet) ? 0 : 1
      const bonA = (stor.bonA) ? 0 : 1
      mtgAppLecteur.giveFormula2(stor.mtg32svg, 'mauvaisB', String(bonB))
      mtgAppLecteur.giveFormula2(stor.mtg32svg, 'mauvaisA', String(bonA))
      mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a2', String(a2))
      mtgAppLecteur.giveFormula2(stor.mtg32svg, 'alpha2', String(alpha2))
      mtgAppLecteur.giveFormula2(stor.mtg32svg, 'beta2', String(beta2))
      mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche', String(affiche))
    } else {
      if (st.num === 2) {
        // affichage de la question
        const a = stor.param.a
        mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(a))
      }
      // on ne recharge pas la figure dans le premier cas de figure (deux points mobiles)
    }
    mtgAppLecteur.calculate(stor.mtg32svg, true)
    mtgAppLecteur.display(stor.mtg32svg)
  }

  function enonceMain () {
    const largFig = 361
    stor.conteneurG = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '5px' }) })
    const zoneFig = j3pAddElt(stor.conteneurG, 'div', '', { style: { textAlign: 'center' } })
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(zoneFig, { id: stor.mtg32svg, width: largFig, height: 325 })
    depart()
    /// //////////////////////////////////////Fin de l’initialisation de la figure MathGraph32'
    stor.conteneur = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '5px' }) })
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    // j3pDiv(me.zonesElts.MD,{id:le_nom+"zone_consigne1",contenu:"",coord:[0,0],style:me.styles.etendre('petit.enonce',{padding:"5px"})});
    let txtConsigne1 = textes.consigne1
    let txtConsigne2 = textes.consigne2
    if (ds.sommet_mobile_seulement) {
      txtConsigne1 = textes.consigne1bis
      txtConsigne2 = textes.consigne2bis
    }
    j3pAffiche(zoneCons1, '', txtConsigne1)
    stor.nom_f = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
    // expression de la forme canonique demandée
    let nbPassage = 0
    let xA, yA, estDansFigure
    let a, alpha, beta
    do {
      estDansFigure = true
      let nonAutorise = false
      do {
        const [lemin, lemax] = j3pGetBornesIntervalle(ds.a)
        a = j3pGetRandomInt(lemin, lemax)
        if (ds.a_different1) {
          nonAutorise = (Math.abs(a - 1) < Math.pow(10, -12))
          if (ds.a === '[1;1]') {
            j3pShowError("Vous ne pouvez pas imposer à a d'être différent de 1 et de le choisir dans [1;1], je redéfinis donc a")
            ds.a = '[-3;3]'
          }
        }
      } while ((Math.abs(a) < Math.pow(10, -12)) || nonAutorise)
      let recommence = false
      do {
        const [lemin, lemax] = j3pGetBornesIntervalle(ds.alpha)
        alpha = j3pGetRandomInt(lemin, lemax)
        if (ds.discriminant_nul) {
          beta = 0
        } else {
          const [lemin, lemax] = j3pGetBornesIntervalle(ds.beta)
          beta = j3pGetRandomInt(lemin, lemax)
        }
        recommence = false
        if (ds.sommet_mobile_seulement) {
          if ((ds.alpha === '[0;0]') && (ds.beta === '[0;0]')) {
            j3pShowError('alpha et beta ne doivent pas être tous les deux nuls !')
            ds.alpha = '[-4;4]'
            ds.beta = '[-3;3]'
            recommence = true
          } else {
            if (Math.abs(Math.abs(alpha) - Math.abs(beta)) < Math.pow(10, -12)) {
              // on ne peut avoir alpha et beta nuls en même temps
              recommence = true
            }
          }
        } else {
          if ((ds.alpha === '[0;0]') && (ds.beta === '[0;0]') && (Math.abs(Math.abs(alpha) - Math.abs(beta)) < Math.pow(10, -12))) {
            // c’est-à-dire que alpha et beta sont tous les deux de même valeur absolue alors qu’on ne l’a pas imposé'
            recommence = true
          }
        }
      } while (recommence)
      xA = alpha + 1
      yA = beta + a
      estDansFigure = (((alpha > -5) && (alpha < 5)) && ((beta > -4) && (beta < 4)) && ((xA > -6) && (xA < 6)) && ((yA > -6) && (yA < 6)))
      nbPassage++
      if (nbPassage === 50) {
        // on considère que les contraintes données sont trop fortes donc on reprends les valeurs par défaut de a, alpha et beta
        ds.a = '[-3;3]'
        ds.alpha = '[-4;4]'
        ds.beta = '[-3;3]'
        nbPassage = 0
      }
    } while (!estDansFigure)
    stor.param = { alpha, beta, a, xA, yA }

    if (ds.sommet_mobile_seulement) {
      // il faut recharger la figure avec le nouvel a
      // FIXME il faut se passer de MathJax.Hub.Queue (le virer simplement devrait suffire, mtg gère le chargement de MathJax, mais faut tester)
      MathJax.Hub.Queue(function () {
        modifFig({ num: 2, correction: false })
      })
    }
    // maintenant on écrit f(x)
    let fdex
    if (Math.abs(alpha) < Math.pow(10, -12)) {
      if (Math.abs(a - 1) < Math.pow(10, -12)) {
        // si a vaut 1
        fdex = 'x^2' + j3pMonome(2, 0, beta)
      } else if (Math.abs(a + 1) < Math.pow(10, -12)) {
        // si a vaut -1
        fdex = '-x^2' + j3pMonome(2, 0, beta)
      } else {
        fdex = a + 'x^2' + j3pMonome(2, 0, beta)
      }
    } else {
      if (Math.abs(a - 1) < Math.pow(10, -12)) {
        // si a vaut 1
        fdex = '(x' + j3pMonome(2, 0, -alpha) + ')^2' + j3pMonome(2, 0, beta)
      } else if (Math.abs(a + 1) < Math.pow(10, -12)) {
        // si a vaut -1
        fdex = '-(x' + j3pMonome(2, 0, -alpha) + ')^2' + j3pMonome(2, 0, beta)
      } else {
        fdex = a + '(x' + j3pMonome(2, 0, -alpha) + ')^2' + j3pMonome(2, 0, beta)
      }
    }
    if (ds.discriminant_nul) {
      // on cherche les abscisses qui auront la même image
      let x1, y1
      do {
        x1 = alpha + j3pGetRandomInt(1, 3)
        y1 = a * Math.pow(x1 - alpha, 2) + beta
      } while ((y1 > 5) || (y1 < -5))
      j3pAffiche(zoneCons1, '', ' ' + textes.consigne3,
        {
          f: stor.nom_f, b: x1, c: y1, d: (2 * alpha - x1)
        })
      stor.x1 = x1
      stor.x2 = 2 * alpha - x1
      stor.y1 = y1
    } else {
      j3pAffiche(zoneCons1, '', ' ' + txtConsigne2, { f: stor.nom_f, g: fdex })
    }

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px' } })

    // Obligatoire
    me.finEnonce()
  }
  // console.log("debut du code : ",me.etat)
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourcentage = 0.62

        me.construitStructurePage({ structure: 'presentation1', ratioGauche: stor.pourcentage })
        me.afficheTitre(textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        init()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      {
        const mtgAppLecteur = stor.mtgAppLecteur
        // On teste si une réponse a été saisie
        const reponse = { reponse: false, bonneReponse: false }
        const alphaRep = mtgAppLecteur.valueOf(stor.mtg32svg, 'alpha')
        const betaRep = mtgAppLecteur.valueOf(stor.mtg32svg, 'beta')
        let aRep, bonneAbsSommet, bonneOrdSommet, plusParabole
        if (!ds.sommet_mobile_seulement) {
          aRep = mtgAppLecteur.valueOf(stor.mtg32svg, 'a')
          me.logIfDebug('aRep : ' + aRep + '   alphaRep:' + alphaRep + '   betaRep:' + betaRep)
          // on vérifie que l’un des deux points a bien bougé'
          // au début a=1, alpha=0 et beta=0
          // gestion des cas de figure où on n’a plus de parabole'
          const xA = mtgAppLecteur.valueOf(stor.mtg32svg, 'xA')
          const yA = mtgAppLecteur.valueOf(stor.mtg32svg, 'yA')
          plusParabole = ((Math.abs(xA - alphaRep) < Math.pow(10, -12)) || (Math.abs(yA - betaRep) < Math.pow(10, -12)))
          reponse.aRepondu = (((Math.abs(aRep - 1) >= Math.pow(10, -12)) || (Math.abs(alphaRep) >= Math.pow(10, -12)) || (Math.abs(betaRep) >= Math.pow(10, -12))) && !plusParabole)
        } else {
          me.logIfDebug('alphaRep:' + alphaRep + '   betaRep:' + betaRep)
          // on vérifie que l’un des deux points a bien bougé'
          // au début alpha=0 et beta=0
          reponse.aRepondu = ((Math.abs(alphaRep) >= Math.pow(10, -12)) || (Math.abs(betaRep) >= Math.pow(10, -12)))
        }
        if (reponse.aRepondu) {
        // je teste ici la réponse de l’élève
          bonneAbsSommet = (Math.abs(alphaRep - stor.param.alpha) < Math.pow(10, -12))
          bonneOrdSommet = (Math.abs(betaRep - stor.param.beta) < Math.pow(10, -12))
          let bonA = true
          if (!ds.sommet_mobile_seulement) {
            bonA = (Math.abs(aRep - stor.param.a) < Math.pow(10, -12))
          }
          reponse.bonneReponse = (bonneAbsSommet && bonneOrdSommet && bonA)
          stor.bon_sommet = (bonneAbsSommet && bonneOrdSommet)
          if (stor.bon_sommet) stor.bonA = bonA
          else stor.bonA = false
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          stor.zoneCorr.style.color = me.styles.cfaux
          if (plusParabole) {
            stor.zoneCorr.innerHTML = textes.comment5
          } else {
            if (ds.sommet_mobile_seulement) stor.zoneCorr.innerHTML = textes.comment1bis
            else stor.zoneCorr.innerHTML = textes.comment1
          }
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              /*
             *   RECOPIER LA CORRECTION ICI !
             */
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (ds.sommet_mobile_seulement) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment4
              } else {
                if (!bonneAbsSommet || !bonneOrdSommet) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment3
                  me.typederreurs[3]++
                } else {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment2
                  me.typederreurs[4]++
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          const nbErreurs = ds.nbitems - me.score
          if (me.typederreurs[3] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb sur le sommet est supé au égal au seul fixé
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[4] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb sur l’autre point est supé au égal au seul fixé
            me.parcours.pe = ds.pe_3
          } else {
            me.parcours.pe = ds.pe_4
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
