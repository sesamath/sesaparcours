import { j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pPaletteMathquill, j3pRandomTab, j3pRestriction } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexProduit, j3pSimplificationRacineTrinome, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import Repere from 'src/legacy/outils/geometrie/Repere'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    juillet 2015
    Dans cette section, on demande à l’élève de résoudre une équation où l’un des membres est un trinôme et l’autre une constante, une fonction affine ou un trinôme
    On peut aussi formuler la question sous la forme du point d’intersection de deux courbes.
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['fonctions1', ['', '', ''], 'array', 'Pour chaque répétition, on peut imposer la fonction placée à gauche de l’égalité (on peut aussi écrire "trinome", "affine" ou "constante" pour imposer le type si fonctions2[i] n’est pas imposée).'],
    ['fonctions2', ['', '', ''], 'array', 'Pour chaque répétition, on peut imposer la fonction placée à droite de l’égalité (on peut aussi écrire "trinome", "affine" ou "constante" pour imposer le type si fonctions1[i] n’est pas imposée)).'],
    ['intersectionCourbes', false, 'boolean', 'Plutôt que de se contenter de demander de résoudre une équation, on peut demander les coordonnées des points d’intersection des deux courbes.']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Égalité de fonctions',
  titre_exobis: 'Intersection de deux courbes',
  // on donne les phrases de la consigne
  consigne1: 'On souhaite résoudre l’équation $£e$.',
  consigne2: 'Cette équation admet @1@ solution(s).',
  consigne3_1: 'Cette solution vaut &1&.',
  consigne3_2: 'Ces solutions valent &1& et &2&.',
  consigne3_3: 'L’ensemble des solutions de l’équation est {&1&',
  consigne4_1: 'Soient $P_1$ et $P_2$ les paraboles d’équations respectives $y=£e$ et $y=£f$.',
  consigne4_2: 'Soient $P$ la parabole d’équation $y=£e$ et $d$ la droite d’équation $y=£f$.',
  consigne4_3: 'Soient $d$ la droite d’équation $y=£f$ et $P$ la parabole d’équation $y=£e$.',
  consigne5_1: 'Les deux paraboles admettent @1@ point(s) d’intersection.',
  consigne5_2: 'La parabole et la droite admettent @1@ point(s) d’intersection.',
  consigne5_3: 'La droite et le parabole admettent @1@ point(s) d’intersection.',
  consigne6_1: 'Ce point a pour coordonnées (&1&;&2&).',
  consigne6_2: 'Ces points ont pour coordonnées (&1&;&2&) et (&3&;&4&).',
  consigne6_3: 'Ces points ont pour coordonnées :<br>(&1&;&2&)',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Les deux valeurs données sont égales ! Recommence !',
  comment1_2: 'Les deux abscisses sont égales ! Recommence !',
  comment2_1: 'Le nombre de solutions donné n’est pas correct !',
  comment2_2: 'Le nombre de points donné n’est pas correct !',
  comment3: 'Il faut simplifier le résultat !',
  comment3_2: 'La fraction aurait tout de même pu être réduite !',
  comment4: 'Il faut simplifier les résultats !',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'L’équation équivaut à $£e$.',
  corr1_2: 'Le problème revient à résoudre $£e$ ce qui équivaut à $£f$.',
  corr2: 'Le trinôme est de la forme $ax^2+bx+c$ où $a=£a$, $b=£b$ et $c=£c$. Son discriminant vaut $\\Delta=b^2-4ac=£d$.',
  corr3_1: 'Comme $\\Delta>0$, l’équation admet deux solutions réelles : $x_1=\\frac{-b-\\sqrt{\\Delta}}{2a}=£x$ et $x_2=\\frac{-b+\\sqrt{\\Delta}}{2a}=£y$.',
  corr3_2: 'Comme $\\Delta=0$, l’équation admet une unique solution réelle : $x_0=\\frac{-b}{2a}=£x$.',
  corr3_3: 'Comme $\\Delta<0$, l’équation n’admet pas de solution réelle.',
  corr4_1: 'On calcule enfin l’image de la solution par l’une des deux fonctions initiales.',
  corr4_2: 'On calcule enfin les images des solutions par l’une des deux fonctions initiales.',
  corr5_1: 'Donc les points d’intersection des deux paraboles ont pour coordonnées $\\left(£x;£y\\right)$ et $\\left(£z;£w\\right)$.',
  corr5_2: 'Donc le point d’intersection des deux paraboles a pour coordonnées $\\left(£x;£y\\right)$.',
  corr5_3: 'Donc les deux paraboles n’ont pas de point d’intersection.',
  corr6_1: 'Donc les points d’intersection de la parabole et de la droite ont pour coordonnées $\\left(£x;£y\\right)$ et $\\left(£z;£w\\right)$.',
  corr6_2: 'Donc le point d’intersection de la parabole et de la droite a pour coordonnées $\\left(£x;£y\\right)$.',
  corr6_3: 'Donc la parabole et de la droite n’ont pas de point d’intersection.',
  corr7_1: 'Donc les points d’intersection de la droite et de la parabole ont pour coordonnées $\\left(£x;£y\\right)$ et $\\left(£z;£w\\right)$.',
  corr7_2: 'Donc les points d’intersection de la droite et de la parabole a pour coordonnées $\\left(£x;£y\\right)$.',
  corr7_3: 'Donc la droite et de la parabole n’ont pas de point d’intersection.',
  corr8: 'On peut contrôler ce résultat en traçant les courbes représentatives des deux fonctions.',
  courbe: 'Courbe représentative des deux fonctions'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    if (ds.intersectionCourbes) {
      const equaInit = stor.objetPolynome.polyGauche + ' = ' + stor.objetPolynome.polyDroite
      j3pAffiche(zoneExpli1, '', textes.corr1_2, { e: equaInit, f: stor.objetPolynome.trinome + '=0' })
    } else {
      j3pAffiche(zoneExpli1, '', textes.corr1_1, { e: stor.objetPolynome.trinome + '=0' })
    }
    // {polyGauche:poly1,polyDroite:poly2,trinome:polyDif,tabRacines:tabSol,lesImages:tabImage,tabCoefs1:[c1,b1,a1],tabCoefs2:[c2,b2,a2],tabCoefsDif:[cDif,bDif,aDif,deltaTxt]};
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli2, '', textes.corr2, {
      a: stor.objetPolynome.tabCoefsDif[2],
      b: stor.objetPolynome.tabCoefsDif[1],
      c: stor.objetPolynome.tabCoefsDif[0],
      d: stor.objetPolynome.tabCoefsDif[3]
    })
    const zoneExpli3 = j3pAddElt(zoneExpli, 'div')
    if (stor.objetPolynome.tabRacines.length === 2) {
      j3pAffiche(zoneExpli3, '', textes.corr3_1, {
        x: stor.objetPolynome.tabRacines[0],
        y: stor.objetPolynome.tabRacines[1]
      })
    } else if (stor.objetPolynome.tabRacines.length === 1) {
      j3pAffiche(zoneExpli3, '', textes.corr3_2, {
        x: stor.objetPolynome.tabRacines[0]
      })
    } else {
      j3pAffiche(zoneExpli3, '', textes.corr3_3)
    }
    if (ds.intersectionCourbes) {
      if ((stor.objetPolynome.polyGauche.includes('x')) && (stor.objetPolynome.polyDroite.includes('x'))) {
        // aucune des deux fonctions n’est constante
        let zoneExpli4
        if (stor.objetPolynome.tabRacines.length >= 1) {
          zoneExpli4 = j3pAddElt(zoneExpli, 'div')
        }
        if (stor.objetPolynome.tabRacines.length === 2) {
          j3pAffiche(zoneExpli4, '', textes.corr4_2)
        } else if (stor.objetPolynome.tabRacines.length === 1) {
          j3pAffiche(zoneExpli4, '', textes.corr4_1)
        }
      }
      const zoneExpli5 = j3pAddElt(zoneExpli, 'div')
      if (stor.objetPolynome.tabRacines.length === 2) {
        if (stor.typeCourbes === 'deux paraboles') {
          j3pAffiche(zoneExpli5, '', textes.corr5_1, {
            x: stor.objetPolynome.tabRacines[0],
            y: stor.objetPolynome.lesImages[0],
            z: stor.objetPolynome.tabRacines[1],
            w: stor.objetPolynome.lesImages[1]
          })
        } else if (stor.typeCourbes === 'parabole et droite') {
          j3pAffiche(zoneExpli5, '', textes.corr6_1, {
            x: stor.objetPolynome.tabRacines[0],
            y: stor.objetPolynome.lesImages[0],
            z: stor.objetPolynome.tabRacines[1],
            w: stor.objetPolynome.lesImages[1]
          })
        } else {
          j3pAffiche(zoneExpli5, '', textes.corr7_1, {
            x: stor.objetPolynome.tabRacines[0],
            y: stor.objetPolynome.lesImages[0],
            z: stor.objetPolynome.tabRacines[1],
            w: stor.objetPolynome.lesImages[1]
          })
        }
      } else if (stor.objetPolynome.tabRacines.length === 1) {
        if (stor.typeCourbes === 'deux paraboles') {
          j3pAffiche(zoneExpli5, '', textes.corr5_2, {
            x: stor.objetPolynome.tabRacines[0],
            y: stor.objetPolynome.lesImages[0]
          })
        } else if (stor.typeCourbes === 'parabole et droite') {
          j3pAffiche(zoneExpli5, '', textes.corr6_2, {
            x: stor.objetPolynome.tabRacines[0],
            y: stor.objetPolynome.lesImages[0]
          })
        } else {
          j3pAffiche(zoneExpli5, '', textes.corr7_2, {
            x: stor.objetPolynome.tabRacines[0],
            y: stor.objetPolynome.lesImages[0]
          })
        }
      } else {
        if (stor.typeCourbes === 'deux paraboles') {
          j3pAffiche(zoneExpli5, '', textes.corr5_3)
        } else if (stor.typeCourbes === 'parabole et droite') {
          j3pAffiche(zoneExpli5, '', textes.corr6_3)
        } else {
          j3pAffiche(zoneExpli5, '', textes.corr7_3)
        }
      }
    }
    const zoneExpli6 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli6, '', textes.corr8)
    // affichage de la courbe
    creationCourbe()
    j3pToggleFenetres('Courbe')
  }

  function suiteEnonce () {
    const nbRacine = Number(stor.zoneInput[0].value)
    j3pEmpty(stor.zoneCons3)
    j3pEmpty(stor.laPalette)
    stor.zoneInput = stor.zoneInput.slice(0, 1)
    if (stor.zoneInput[0].value === '') {
      j3pFocus(stor.zoneInput[0])
      stor.mesZonesSaisie = [stor.zoneInput[0]]
      stor.fctsValid = new ValidationZones({ parcours: me, zones: stor.mesZonesSaisie })
    } else {
      let laConsigne3 = textes.consigne3_1
      if (nbRacine === 0) {
        stor.mesZonesSaisie = [stor.zoneInput[0]]
      } else if (nbRacine === 1) {
        if (ds.intersectionCourbes) {
          laConsigne3 = textes.consigne6_1
          const elt = j3pAffiche(stor.zoneCons3, '', laConsigne3,
            {
              inputmq1: { texte: '' },
              inputmq2: { texte: '' }
            })
          stor.zoneInput.push(elt.inputmqList[0], elt.inputmqList[1])
          mqRestriction(stor.zoneInput[2], '\\d,./-', { commandes: ['fraction', 'racine'] })
        } else {
          const elt = j3pAffiche(stor.zoneCons3, '', laConsigne3,
            {
              inputmq1: { texte: '' }
            })
          stor.zoneInput.push(elt.inputmqList[0])
        }
        mqRestriction(stor.zoneInput[1], '\\d,./-', { commandes: ['fraction', 'racine'] })
        stor.zoneInput[1].typeReponse = ['nombre', 'exact']
        if (ds.intersectionCourbes) {
          stor.zoneInput[2].typeReponse = ['nombre', 'exact']
        }
        j3pFocus(stor.zoneInput[1])
        if (stor.objetPolynome.tabRacines.length === 1) {
          stor.zoneInput[1].reponse = [stor.objetPolynome.valeurRacines[0]]
          stor.zoneInput[1].solutionSimplifiee = ['non valide', 'non valide']// on peut mettre "valide et on accepte", "non valide" ou "reponse fausse";
          if (ds.intersectionCourbes) {
            stor.zoneInput[2].reponse = [stor.objetPolynome.valeurImages[0]]
            stor.zoneInput[2].solutionSimplifiee = ['non valide', 'non valide']// on peut mettre "valide et on accepte", "non valide" ou "reponse fausse";
          }
        }
        if (ds.intersectionCourbes) {
          // palette MQ :
          for (let i = 1; i <= 2; i++) {
            stor.zoneInput[i].addEventListener('focusin', ecoute.bind(null, i))
          }
        }
        ecoute(1)
      } else if (nbRacine === 2) {
        // nbRacine vaut 2
        laConsigne3 = textes.consigne3_2
        if (ds.intersectionCourbes) laConsigne3 = textes.consigne6_2
        const elt = j3pAffiche(stor.zoneCons3, '', laConsigne3,
          {
            inputmq1: { texte: '' },
            inputmq2: { texte: '' },
            inputmq3: { texte: '' },
            inputmq4: { texte: '' }
          })
        stor.zoneInput = stor.zoneInput.concat(elt.inputmqList)
        for (let i = 1; i < stor.zoneInput.length; i++) {
          if (stor.zoneInput[i]) mqRestriction(stor.zoneInput[i], '\\d,.+/-', { commandes: ['fraction', 'racine'] })
        }
        elt.inputmqList[0].typeReponse = ['nombre', 'exact']
        elt.inputmqList[1].typeReponse = ['nombre', 'exact']
        if (ds.intersectionCourbes) {
          elt.inputmqList[2].typeReponse = ['nombre', 'exact']
          elt.inputmqList[3].typeReponse = ['nombre', 'exact']
        }
        j3pFocus(elt.inputmqList[0])
        if (stor.objetPolynome.tabRacines.length === 2) {
          elt.inputmqList[0].reponse = [stor.objetPolynome.valeurRacines[0]]
          elt.inputmqList[1].reponse = [stor.objetPolynome.valeurRacines[1]]
          elt.inputmqList[0].solutionSimplifiee = ['valide et on accepte', 'valide et on accepte']// on peut mettre "valide et on accepte", "non valide" ou "reponse fausse";
          elt.inputmqList[1].solutionSimplifiee = ['valide et on accepte', 'valide et on accepte']// on peut mettre "valide et on accepte", "non valide" ou "reponse fausse";
        }
        if (ds.intersectionCourbes) {
          elt.inputmqList[2].reponse = [stor.objetPolynome.valeurImages[0]]
          elt.inputmqList[2].solutionSimplifiee = ['non valide', 'non valide']// on peut mettre "valide et on accepte", "non valide" ou "reponse fausse";
          elt.inputmqList[3].reponse = [stor.objetPolynome.valeurImages[1]]
          elt.inputmqList[3].solutionSimplifiee = ['non valide', 'non valide']// on peut mettre "valide et on accepte", "non valide" ou "reponse fausse";
        }
        if (ds.intersectionCourbes) {
          // palette MQ :
          for (let i = 1; i <= 4; i++) {
            stor.zoneInput[i].addEventListener('focusin', ecoute.bind(null, i))
          }
        } else {
          // palette MQ :
          for (let i = 1; i <= 2; i++) {
            stor.zoneInput[i].addEventListener('focusin', ecoute.bind(null, i))
          }
        }
        ecoute(1)
      } else {
        // nbRacine est supérieur à 2
        let textFinLigne = ''
        if (ds.intersectionCourbes) {
          for (let i = 2; i <= nbRacine; i++) {
            textFinLigne += ' ; (&' + (2 * i - 1) + '&;&' + (2 * i) + '&)'
          }
          laConsigne3 = textes.consigne6_3 + textFinLigne + '.'
        } else {
          for (let i = 2; i <= nbRacine; i++) {
            textFinLigne += ' ; &' + i + '&'
          }
          laConsigne3 = textes.consigne3_3 + textFinLigne + '}.'
        }
        const elt = j3pAffiche(stor.zoneCons3, '', laConsigne3,
          {
            inputmq1: { texte: '' },
            inputmq2: { texte: '' },
            inputmq3: { texte: '' },
            inputmq4: { texte: '' },
            inputmq5: { texte: '' },
            inputmq6: { texte: '' },
            inputmq7: { texte: '' },
            inputmq8: { texte: '' },
            inputmq9: { texte: '' },
            inputmq10: { texte: '' },
            inputmq11: { texte: '' },
            inputmq12: { texte: '' },
            inputmq13: { texte: '' },
            inputmq14: { texte: '' },
            inputmq15: { texte: '' },
            inputmq16: { texte: '' },
            inputmq17: { texte: '' },
            inputmq18: { texte: '' }
          })

        stor.zoneInput = stor.zoneInput.concat(elt.inputmqList)
        if (!ds.intersectionCourbes) {
          // palette MQ :
          for (let i = 1; i <= nbRacine; i++) {
            stor.zoneInput[i].numero = i
            stor.zoneInput[i].addEventListener('focusin', ecoute.bind(null, i))
            stor.zoneInput[i].typeReponse = ['nombre', 'exact']
            mqRestriction(stor.zoneInput[i], '\\d,.+/-', { commandes: ['fraction', 'racine'] })
          }
        } else {
          // palette MQ :
          for (let i = 1; i <= 2 * nbRacine; i++) {
            stor.zoneInput[i].addEventListener('focusin', ecoute.bind(null, i))
            stor.zoneInput[i].typeReponse = ['nombre', 'exact']
            mqRestriction(stor.zoneInput[i], '\\d,.+/-', { commandes: ['fraction', 'racine'] })
          }
        }
        ecoute(1)
        j3pFocus(stor.zoneInput[1])
      }
      stor.mesZonesSaisie = [...stor.zoneInput]
      if (ds.intersectionCourbes) {
        if ((nbRacine === 2) && (stor.objetPolynome.tabRacines.length === 2)) {
          stor.fctsValid = new ValidationZones({ parcours: me, zones: stor.mesZonesSaisie, validePerso: stor.mesZonesSaisie.slice(1) })
        } else {
          stor.fctsValid = new ValidationZones({ parcours: me, zones: stor.mesZonesSaisie })
        }
      } else {
        if ((nbRacine === 2) && (stor.objetPolynome.tabRacines.length === 2)) {
          stor.fctsValid = new ValidationZones({ parcours: me, zones: stor.mesZonesSaisie, validePerso: stor.mesZonesSaisie.slice(1) })
        } else {
          stor.fctsValid = new ValidationZones({ parcours: me, zones: stor.mesZonesSaisie })
        }
      }
    }
  }
  function ecoute (num) {
    if (stor.zoneInput[num].className.includes('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput[num], { liste: ['fraction', 'racine'] })
    }
  }
  function ecrireFct (fctTxt) {
    let fctLatex = fctTxt
    const regFrac1 = /\(-?[0-9]+\)\/\([0-9]+\)/ig // new RegExp('\\(\\-?[0-9]{1,}\\)/\\([0-9]{1,}\\)', 'ig')
    const regFrac2 = /\(-?[0-9]+\/[0-9]+\)/ig // new RegExp('\\(\\-?[0-9]{1,}/[0-9]{1,}\\)', 'ig')
    const regFrac3 = /[0-9]+\/[0-9]+/ig // new RegExp('[0-9]{1,}/[0-9]{1,}', 'ig')
    let num, den
    while (regFrac1.test(fctLatex)) {
      // on trouve (..)/(..)
      const tabFrac1 = fctLatex.match(regFrac1)
      for (let i = 0; i < tabFrac1.length; i++) {
        const tabSeparateur = tabFrac1[i].split('/')
        num = tabSeparateur[0].substring(1, tabSeparateur[0].length - 1)
        den = tabSeparateur[1].substring(1, tabSeparateur[1].length - 1)
        fctLatex = fctLatex.replace(tabFrac1[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    while (regFrac2.test(fctLatex)) {
      // on trouve (../..)
      const tabFrac2 = fctLatex.match(regFrac2)
      for (let i = 0; i < tabFrac2.length; i++) {
        const tabSeparateur = tabFrac2[i].split('/')
        num = tabSeparateur[0].substring(1, tabSeparateur[0].length)
        den = tabSeparateur[1].substring(0, tabSeparateur[1].length - 1)
        fctLatex = fctLatex.replace(tabFrac2[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    while (regFrac3.test(fctLatex)) {
      // on trouve ../..
      const tabFrac3 = fctLatex.match(regFrac3)
      for (let i = 0; i < tabFrac3.length; i++) {
        const tabSeparateur = tabFrac3[i].split('/')
        num = tabSeparateur[0].substring(0, tabSeparateur[0].length)
        den = tabSeparateur[1].substring(0, tabSeparateur[1].length)
        fctLatex = fctLatex.replace(tabFrac3[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    // on enlève signe * devant x
    while (fctLatex.includes('*x')) {
      fctLatex = fctLatex.replace('*x', 'x')
    }
    if (fctLatex.includes('x²')) {
      fctLatex = fctLatex.replace('x²', 'x^2')
    }
    // fonction bx^2+cx+d avec coefs au format latex
    const posSigne = [0]
    for (let i = 1; i < fctLatex.length; i++) {
      if ((fctLatex[i] === '+') || (fctLatex[i] === '-')) {
        posSigne.push(i)
      }
    }
    posSigne.push(fctLatex.length)
    // je découpe alors l’expression à l’aide de ces signes, cela me donnera les monômes
    const tabMonome = []
    for (let i = 1; i < posSigne.length; i++) {
      tabMonome.push(fctLatex.substring(posSigne[i - 1], posSigne[i]))
    }
    const tabCoefPolynome = ['0', '0', '0']
    // tabCoefPolynome[0] est le coef constant, tabCoefPolynome[1], celui devant x, ...
    // je fais un correctif si j’ai un monome avec un coef de la forme \\frac{-...}{...}
    let posi = 0
    while (posi < tabMonome.length) {
      if ((tabMonome[posi] === '\\frac{') || (tabMonome[posi] === '+\\frac{') || (tabMonome[posi] === '-\\frac{')) {
        tabMonome[posi + 1] = tabMonome[posi] + tabMonome[posi + 1]
        tabMonome.splice(posi, 1)
      } else {
        posi++
      }
    }
    for (let i = 0; i < tabMonome.length; i++) {
      // tabMonome.length<=3 c’est le nombre de monomes présents
      if ((tabMonome[i].includes('x^{2}')) || (tabMonome[i].includes('x^2'))) {
        // c’est le monome ax^2
        if (tabMonome[i].charAt(0) === '+') {
          tabMonome[i] = tabMonome[i].substring(1)
        }
        if (tabMonome[i].includes('x^{2}')) {
          tabCoefPolynome[2] = tabMonome[i].substring(0, tabMonome[i].length - 5)
        } else {
          tabCoefPolynome[2] = tabMonome[i].substring(0, tabMonome[i].length - 3)
        }
        if (tabCoefPolynome[2] === '') {
          tabCoefPolynome[2] = '1'
        } else if (tabCoefPolynome[2] === '-') {
          tabCoefPolynome[2] = '-1'
        }
      } else if (tabMonome[i].includes('x')) {
        // c’est le monome ax
        if (tabMonome[i].charAt(0) === '+') {
          tabMonome[i] = tabMonome[i].substring(1)
        }
        tabCoefPolynome[1] = tabMonome[i].substring(0, tabMonome[i].length - 1)
        if (tabCoefPolynome[1] === '') {
          tabCoefPolynome[1] = '1'
        } else if (tabCoefPolynome[1] === '-') {
          tabCoefPolynome[1] = '-1'
        }
      } else {
        // c’est le monome a
        if (tabMonome[i].charAt(0) === '+') {
          tabCoefPolynome[0] = tabMonome[i].substring(1)
        } else {
          tabCoefPolynome[0] = tabMonome[i]
        }
      }
    }
    return { fctLatex, tab_coef: tabCoefPolynome }
  }
  function rechercheRacine (tab1, tab2) {
    // tab1 et tab2 sont 2 tableaux de 2 éléments représentant les 2 fonctions de chaque côté de l’égalité
    // tab1[0] est l’expression de la fonction polynomiale au format latex
    // tab1[1] est le tableau des coefficients du polynôme (lorsqu’ils existent)
    // tab1[2] est le type de fonction ("trinome", "affine", "constante"). Cela donne la forme de la fonction générée de manière aléatoire.
    // idem pour tab2[0] et tab2[1]
    // cette fonction génère les polynômes manquant et calcule les solutions éventuelles de l’équation tab1[0]=tab2[0]
    // dans le cas où les deux polynômes ne sont pas donnés,
    let poly1
    let poly2
    let a, b, c, d, aDif, bDif, cDif
    const fctTxt1 = tab1[0]
    const fctTxt2 = tab2[0]
    let a1, b1, c1, a2, b2, c2
    if ((fctTxt1 === '') || (fctTxt2 === '')) {
      // au moins une des deux fonctions est aléatoire.
      // Dans ce cas, on écrit le trinôme différence sous la forme (ax+b)(cx+d) s’il admet 2 racines
      // on l’écrit (ax+b)^2 s’il admet 2 racines et (ax+b)^2+c (c>0) s’il n’admet pas de racine
      const nbSol = j3pRandomTab([0, 1, 2], [0.25, 0.125, 0.625])
      // la proba d’avoir 0 racine est 1/4, celle d’en avoir 1 est 1/8 et celle d’en avoir 2 est 5/8
      if (nbSol === 0) {
        // polynôme (ax+b)^2+c=a^2x^2+2abx+b^2+c ou son opposé
        do {
          do {
            a = j3pGetRandomInt(-3, 3)
          } while (Math.abs(a) < Math.pow(10, -12))
          do {
            b = j3pGetRandomInt(-5, 5)
          } while (Math.abs(b) < Math.pow(10, -12))
          c = j3pGetRandomInt(1, 7)
          aDif = Math.pow(a, 2)
          bDif = 2 * a * b
          cDif = Math.pow(b, 2) + c
          if (j3pGetRandomBool()) {
            aDif = -aDif
            bDif = -bDif
            cDif = -cDif
          }
        } while (Math.abs(bDif) < Math.pow(10, -12))
      } else if (nbSol === 1) {
        // polynôme (ax+b)^2=a^2x^2+2abx+b^2 ou son opposé
        do {
          a = j3pGetRandomInt(-3, 3)
        } while (Math.abs(a) < Math.pow(10, -12))
        do {
          b = j3pGetRandomInt(-5, 5)
        } while (Math.abs(b) < Math.pow(10, -12))
        aDif = Math.pow(a, 2)
        bDif = 2 * a * b
        cDif = Math.pow(b, 2)
        if (j3pGetRandomBool()) {
          aDif = -aDif
          bDif = -bDif
          cDif = -cDif
        }
      } else {
        const choixDelta = j3pGetRandomInt(0, 2)// dans 1 cas sur 3, delta ne sera pas un carré parfait (seulement lorsqu’on ne demande pas l’intersection des deux courbes)
        if ((choixDelta === 0) && (!ds.intersectionCourbes)) {
          let deltaVal
          do {
            aDif = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
            bDif = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 9)
            cDif = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 9)
            deltaVal = Math.pow(bDif, 2) - 4 * aDif * cDif
          } while (Math.abs(Math.sqrt(deltaVal) - Math.round(Math.sqrt(deltaVal))) < Math.pow(10, -12))
        } else {
          // (ax+b)(cx+d)=acx^2+(ad+bc)x+bd
          do {
            do {
              a = j3pGetRandomInt(-3, 3)
            } while (Math.abs(a) < Math.pow(10, -12))
            do {
              b = j3pGetRandomInt(-5, 5)
            } while (Math.abs(b) < Math.pow(10, -12))
            do {
              c = j3pGetRandomInt(-3, 3)
            } while (Math.abs(c) < Math.pow(10, -12))
            do {
              d = j3pGetRandomInt(-5, 5)
            } while (Math.abs(d) < Math.pow(10, -12))
          } while (Math.abs(Math.abs(-b / a) - Math.abs(-d / c)) < Math.pow(10, -12))
          aDif = a * c
          bDif = a * d + b * c
          cDif = b * d
        }
      }
    } else {
      a1 = (tab1[1][2] === '') ? '0' : tab1[1][2]
      b1 = (tab1[1][1] === '') ? '0' : tab1[1][1]
      c1 = (tab1[1][0] === '') ? '0' : tab1[1][0]
      a2 = (tab2[1][2] === '') ? '0' : tab2[1][2]
      b2 = (tab2[1][1] === '') ? '0' : tab2[1][1]
      c2 = (tab2[1][0] === '') ? '0' : tab2[1][0]
      aDif = j3pGetLatexSomme(a1, j3pGetLatexProduit('-1', a2))
      bDif = j3pGetLatexSomme(b1, j3pGetLatexProduit('-1', b2))
      cDif = j3pGetLatexSomme(c1, j3pGetLatexProduit('-1', c2))
    }
    const polyDif = j3pGetLatexMonome(1, 2, String(aDif)) + j3pGetLatexMonome(2, 1, String(bDif)) + j3pGetLatexMonome(2, 0, String(cDif))
    me.logIfDebug('polyDif:' + polyDif)

    if (tab2[2] === '') {
      // on choisit de manière aléatoire entre fonction affine et trinôme
      tab2[2] = (j3pGetRandomBool()) ? 'trinome' : 'affine'
    }
    if (fctTxt1 === '') {
      if (fctTxt2 === '') {
        if ((tab1[2] === '') || (tab1[2] === 'trinome')) {
          if ((tab2[2] === '') || (tab2[2] === 'trinome')) {
            // on a 2 trinômes
            do {
              a1 = j3pGetRandomInt(-4, 4)
            } while ((Math.abs(a1) < Math.pow(10, -12)) || (Math.abs(a1 - aDif) < Math.pow(10, -12)))
            b1 = j3pGetRandomInt(-5, 5)
            c1 = j3pGetRandomInt(-6, 6)
          } else if (tab2[2] === 'affine') {
            a1 = aDif
            do {
              b1 = j3pGetRandomInt(-6, 6)
            } while ((Math.abs(b1) < Math.pow(10, -12)) || (Math.abs(b1 - bDif) < Math.pow(10, -12)))
            c1 = j3pGetRandomInt(-6, 6)
          } else {
            // tab2[2] est constante
            a1 = aDif
            b1 = bDif
            do {
              c1 = j3pGetRandomInt(-8, 8)
            } while ((Math.abs(c1) < Math.pow(10, -12)) || (Math.abs(c1 - cDif) < Math.pow(10, -12)))
          }
        } else if (tab1[2] === 'affine') {
          // le 2ème membre de l’égalité est alors nécessairement un trinôme
          a1 = 0
          do {
            b1 = j3pGetRandomInt(-6, 6)
          } while ((Math.abs(b1) < Math.pow(10, -12)) || (Math.abs(b1 - bDif) < Math.pow(10, -12)))
          c1 = j3pGetRandomInt(-6, 6)
        } else {
          // le 1er membre de l’égalité est une constante et le 2ème membre est nécessairement un trinôme
          a1 = 0
          b1 = 0
          do {
            c1 = j3pGetRandomInt(-6, 6)
          } while ((Math.abs(c1) < Math.pow(10, -12)) || (Math.abs(c1 - cDif) < Math.pow(10, -12)))
        }
      } else {
        // la deuxième fonction est imposée, les coefs sont dans le tableau tab2[1]
        a2 = tab2[1][2]
        b2 = tab2[1][1]
        c2 = tab2[1][0]
        a1 = j3pGetLatexSomme(aDif, a2)
        b1 = j3pGetLatexSomme(bDif, b2)
        c1 = j3pGetLatexSomme(cDif, c2)
      }
    } else {
      if (fctTxt2 === '') {
        a1 = tab1[1][2]
        b1 = tab1[1][1]
        c1 = tab1[1][0]
      }
    }
    if ((fctTxt1 === '') || (fctTxt2 === '')) {
      a2 = j3pGetLatexSomme(a1, j3pGetLatexProduit('-1', aDif))
      b2 = j3pGetLatexSomme(b1, j3pGetLatexProduit('-1', bDif))
      c2 = j3pGetLatexSomme(c1, j3pGetLatexProduit('-1', cDif))
      if (Math.abs(j3pCalculValeur(a1)) < Math.pow(10, -12)) {
        if (Math.abs(j3pCalculValeur(b1)) < Math.pow(10, -12)) {
          if (Math.abs(j3pCalculValeur(c1)) < Math.pow(10, -12)) {
            poly1 = '0'
          } else {
            poly1 = j3pGetLatexMonome(1, 0, c1)
          }
        } else {
          poly1 = j3pGetLatexMonome(1, 1, b1) + j3pGetLatexMonome(2, 0, c1)
        }
      } else {
        poly1 = j3pGetLatexMonome(1, 2, a1) + j3pGetLatexMonome(2, 1, b1) + j3pGetLatexMonome(2, 0, c1)
      }
      if (Math.abs(j3pCalculValeur(a2)) < Math.pow(10, -12)) {
        if (Math.abs(j3pCalculValeur(b2)) < Math.pow(10, -12)) {
          if (Math.abs(j3pCalculValeur(c2)) < Math.pow(10, -12)) {
            poly2 = '0'
          } else {
            poly2 = j3pGetLatexMonome(1, 0, c2)
          }
        } else {
          poly2 = j3pGetLatexMonome(1, 1, b2) + j3pGetLatexMonome(2, 0, c2)
        }
      } else {
        poly2 = j3pGetLatexMonome(1, 2, a2) + j3pGetLatexMonome(2, 1, b2) + j3pGetLatexMonome(2, 0, c2)
      }
    } else {
      poly1 = tab1[0]
      poly2 = tab2[0]
      a1 = tab1[1][2]
      b1 = tab1[1][1]
      c1 = tab1[1][0]
      a2 = tab2[1][2]
      b2 = tab2[1][1]
      c2 = tab2[1][0]
    }
    const deltaTxt = j3pGetLatexSomme(j3pGetLatexProduit(bDif, bDif), j3pGetLatexProduit('-4', j3pGetLatexProduit(aDif, cDif)))
    const delta = j3pCalculValeur(deltaTxt)
    const tabSol = []
    const tabImage = []// utile pour les coordonnées du point d’intersection de deux courbes
    const tabValSol = []
    const tabValImage = []
    if (Math.abs(delta) < Math.pow(10, -12)) {
      tabSol.push(j3pGetLatexQuotient(j3pGetLatexProduit('-1', bDif), j3pGetLatexProduit('2', aDif)))
      tabValSol.push(j3pCalculValeur(tabSol[0]))
      tabImage.push(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(a1, j3pGetLatexProduit(tabSol[0], tabSol[0])), j3pGetLatexProduit(b1, tabSol[0])), c1))
      tabValImage.push(j3pCalculValeur(tabImage[0]))
    } else if (delta > 0) {
      tabSol.push(j3pSimplificationRacineTrinome(bDif, '-', deltaTxt, aDif))
      tabSol.push(j3pSimplificationRacineTrinome(bDif, '+', deltaTxt, aDif))
      tabValSol.push(j3pCalculValeur(tabSol[0]), j3pCalculValeur(tabSol[1]))
      if (!tabSol[0].includes('sqrt')) {
        // les racines sont simples (sans radical)
        tabImage.push(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(a1, j3pGetLatexProduit(tabSol[0], tabSol[0])), j3pGetLatexProduit(b1, tabSol[0])), c1))
        tabImage.push(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(a1, j3pGetLatexProduit(tabSol[1], tabSol[1])), j3pGetLatexProduit(b1, tabSol[1])), c1))
        tabValImage.push(j3pCalculValeur(tabImage[0]), j3pCalculValeur(tabImage[1]))
      } else {
        if (tab1[2] === 'constante') {
          tabImage.push(c1, c1)
          tabValImage.push(j3pCalculValeur(c2), j3pCalculValeur(c1))
        } else if (tab2[2] === 'constante') {
          tabImage.push(c2, c2)
          tabValImage.push(j3pCalculValeur(c2), j3pCalculValeur(c2))
        } else {
          // là c’est plus compliqué
          // l’image de (-b+/-sqrt(delta))/(2a) par le trinôme a1x^2+b1x+c1 est (-(-1*((a_1*b^2+a1*delta)/(2a)+2ac_1-b_1b)+/-sqrt(delta*(a_1b/a-b_1)^2))/2a
          const a1bcarrePlusa1deltaSur2a = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexProduit(a1, j3pGetLatexProduit(bDif, bDif)), j3pGetLatexProduit(a1, deltaTxt)), j3pGetLatexProduit('2', aDif))
          const deuxAc1Moinsb1b = j3pGetLatexSomme(j3pGetLatexProduit(j3pGetLatexProduit(aDif, c1), '2'), j3pGetLatexProduit('-1', j3pGetLatexProduit(b1, bDif)))
          const newb = j3pGetLatexProduit('-1', j3pGetLatexSomme(a1bcarrePlusa1deltaSur2a, deuxAc1Moinsb1b))
          const coefDevantSqrtTxt = j3pGetLatexSomme(j3pGetLatexQuotient(j3pGetLatexProduit(a1, bDif), aDif), j3pGetLatexProduit('-1', b1))
          const newDelta = j3pGetLatexProduit(deltaTxt, j3pGetLatexProduit(coefDevantSqrtTxt, coefDevantSqrtTxt))
          const image1Txt = j3pSimplificationRacineTrinome(newb, '-', newDelta, aDif)
          const image2Txt = j3pSimplificationRacineTrinome(newb, '+', newDelta, aDif)
          // maintenant il faut mettre les images dans le bon ordre
          const valA = j3pCalculValeur(a1)
          const valADif = j3pCalculValeur(aDif)
          const valB = j3pCalculValeur(b1)
          const valBDif = j3pCalculValeur(bDif)
          const valC = j3pCalculValeur(c1)
          const valSol1 = (-valBDif - Math.sqrt(delta)) / (2 * valADif)
          const valSol2 = (-valBDif + Math.sqrt(delta)) / (2 * valADif)
          // tabValSol.push(valSol1,valSol2);
          const valImageSol1 = valA * Math.pow(valSol1, 2) + valB * valSol1 + valC
          const valImageSol2 = valA * Math.pow(valSol2, 2) + valB * valSol2 + valC
          tabValImage.push(valImageSol1, valImageSol2)
          const valDelta = j3pCalculValeur(newDelta)
          const valCalc1 = (-j3pCalculValeur(newb) - Math.sqrt(valDelta)) / (2 * valADif)

          if (Math.abs(valCalc1 - valImageSol1) < Math.pow(10, -12)) {
            tabImage.push(image1Txt, image2Txt)
          } else {
            tabImage.push(image2Txt, image1Txt)
          }
        }
      }
    }
    me.logIfDebug('tabRacines:' + tabSol + '   tabImage:' + tabImage + '   tabValSol:' + tabValSol + '   tabValImage:' + tabValImage)
    return { polyGauche: poly1, polyDroite: poly2, trinome: polyDif, tabRacines: tabSol, lesImages: tabImage, tabCoefs1: [c1, b1, a1], tabCoefs2: [c2, b2, a2], tabCoefsDif: [cDif, bDif, aDif, deltaTxt], valeurRacines: tabValSol, valeurImages: tabValImage }
  }
  function creationCourbe () {
    const posCourbe = me.zonesElts.HD.offsetHeight + 85
    const largeurRep = 400
    const hauteurRep = 320
    const fenetreCourbe = j3pGetNewId('fenetreCours')
    me.fenetresjq = [
      { name: 'Courbe', title: textes.courbe, top: posCourbe, left: me.zonesElts.MG.offsetWidth + 10, width: largeurRep + 30, id: fenetreCourbe }
    ]
    // Création mais sans affichage.
    j3pCreeFenetres(me)
    // il faut que je gère la position de l’origine du repère et les graduations pour qu’on voit assez bien les courbes
    let borneInf, borneSup, imageInf, imageSup, lgAbs, lgOrd
    if (stor.objetPolynome.valeurRacines.length === 2) {
      // 2 points d’intersection,
      borneInf = Math.min(Math.min(0, stor.objetPolynome.valeurRacines[0]), stor.objetPolynome.valeurRacines[1])
      borneSup = Math.max(Math.max(0, stor.objetPolynome.valeurRacines[0]), stor.objetPolynome.valeurRacines[1])
      imageInf = Math.min(Math.min(0, stor.objetPolynome.valeurImages[0]), stor.objetPolynome.valeurImages[1])
      imageSup = Math.max(Math.max(0, stor.objetPolynome.valeurImages[0]), stor.objetPolynome.valeurImages[1])
      lgAbs = borneSup - borneInf
      lgOrd = imageSup - imageInf
      borneInf = borneInf - lgAbs / 4
      borneSup = borneSup + lgAbs / 4
      imageInf = imageInf - lgOrd / 4
      imageSup = imageSup + lgOrd / 4
    } else if (stor.objetPolynome.valeurRacines.length === 1) {
      // 1 points d’intersection,
      borneInf = Math.min(0, stor.objetPolynome.valeurRacines[0])
      borneSup = Math.max(0, stor.objetPolynome.valeurRacines[0])
      imageInf = Math.min(0, stor.objetPolynome.valeurImages[0])
      imageSup = Math.max(0, stor.objetPolynome.valeurImages[0])
      lgAbs = borneSup - borneInf
      lgOrd = imageSup - imageInf
      borneInf = borneInf - lgAbs
      borneSup = borneSup + lgAbs
      imageInf = imageInf - lgOrd
      imageSup = imageSup + lgOrd
    } else {
      let xSommet1 = 0
      let xSommet2 = 0
      if (Math.abs(stor.objetPolynome.tabCoefs1[2]) > Math.pow(10, -12)) {
        // la première fonction est bien un pol de degré 2
        // je cherche la position de son sommet (et la valeur de l’extremum)
        xSommet1 = -stor.objetPolynome.tabCoefs1[1] / (2 * stor.objetPolynome.tabCoefs1[2])
      }
      if (Math.abs(stor.objetPolynome.tabCoefs2[2]) > Math.pow(10, -12)) {
        // la deuxième fonction est bien un pol de degré 2
        // je cherche la position de son sommet (et la valeur de l’extremum)
        xSommet2 = -stor.objetPolynome.tabCoefs2[1] / (2 * stor.objetPolynome.tabCoefs2[2])
      }
      if (Math.abs(stor.objetPolynome.tabCoefs1[2]) < Math.pow(10, -12)) {
        // la première fonction est affine, donc la deuxième est un pol de degré 2 dont j’ai déjà le sommet
        xSommet1 = xSommet2
      }
      if (Math.abs(stor.objetPolynome.tabCoefs2[2]) < Math.pow(10, -12)) {
        // la deuxième fonction est affine, donc la première est un pol de degré 2 dont j’ai déjà le sommet
        xSommet2 = xSommet1
      }
      const ySommet1 = j3pCalculValeur(stor.objetPolynome.tabCoefs1[2]) * Math.pow(xSommet1, 2) + j3pCalculValeur(stor.objetPolynome.tabCoefs1[1]) * xSommet1 + j3pCalculValeur(stor.objetPolynome.tabCoefs1[0])
      const ySommet2 = j3pCalculValeur(stor.objetPolynome.tabCoefs2[2]) * Math.pow(xSommet2, 2) + j3pCalculValeur(stor.objetPolynome.tabCoefs2[1]) * xSommet2 + j3pCalculValeur(stor.objetPolynome.tabCoefs2[0])
      borneInf = Math.min(Math.min(-0.5, xSommet1), xSommet2)
      borneSup = Math.max(Math.max(0.5, xSommet1), xSommet2)
      imageInf = Math.min(Math.min(-0.5, ySommet1), ySommet2)
      imageSup = Math.max(Math.max(0.5, ySommet1), ySommet2)
      lgAbs = borneSup - borneInf
      lgOrd = imageSup - imageInf
      borneInf = borneInf - lgAbs * 4
      borneSup = borneSup + lgAbs * 4
      imageInf = imageInf - lgOrd * 4
      imageSup = imageSup + lgOrd * 4
    }
    const uniteRepx = largeurRep / (borneSup - borneInf)
    const uniteRepy = hauteurRep / (imageSup - imageInf)
    const posyO = hauteurRep / 2 + (imageSup + imageInf) * uniteRepy / 2
    const posxO = largeurRep / 2 - (borneSup + borneInf) * uniteRepx / 2
    const tabGrad = [0.5, 2, 5, 10, 20, 50, 100, 200, 500, 1000]
    let gradx = 1
    let grady = 1
    // on gère la graduation des x

    const bonneTailleGrad = 50
    let difx = Math.abs(uniteRepx * gradx - bonneTailleGrad)
    for (let i = 0; i < tabGrad.length; i++) {
      const newDifx = Math.abs(uniteRepx * tabGrad[i] - bonneTailleGrad)
      if (newDifx < difx) {
        // on change de graduation
        difx = newDifx
        gradx = tabGrad[i]
      }
    }
    // on gère la graduation des y
    let dify = Math.abs(uniteRepy * grady - bonneTailleGrad)
    for (let i = 0; i < tabGrad.length; i++) {
      const newDify = Math.abs(uniteRepy * tabGrad[i] - bonneTailleGrad)
      if (newDify < dify) {
        // on change de graduation
        dify = newDify
        grady = tabGrad[i]
      }
    }
    // création de la courbe :
    const courbeCours = j3pAddElt(fenetreCourbe, 'div')
    const repere = new Repere({
      idConteneur: courbeCours,
      aimantage: true,
      visible: true,
      trame: true,
      fixe: true,
      larg: largeurRep,
      haut: hauteurRep,

      pasdunegraduationX: gradx,
      pixelspargraduationX: uniteRepx * gradx,
      pasdunegraduationY: grady,
      pixelspargraduationY: uniteRepy * grady,
      xO: posxO,
      yO: posyO,
      debuty: 0,
      negatifs: true,
      objets: [
        { type: 'courbe', nom: 'c1', par1: stor.fonction1, par2: -10, par3: 10, par4: largeurRep, style: { couleur: '#F00', epaisseur: 1 } },
        { type: 'courbe', nom: 'c2', par1: stor.fonction2, par2: -10, par3: 10, par4: largeurRep, style: { couleur: '#00F', epaisseur: 1 } }
      ]
    })

    repere.construit()
    courbeCours.style.position = 'relative'
  }

  function enonceMain () {
    const maFonction1 = ds.fonctions1[me.questionCourante - 1]
    const maFonction2 = ds.fonctions2[me.questionCourante - 1]
    let fctTxt1 = ''
    let coefs1 = []
    let fctTxt2 = ''
    let coefs2 = []
    if ((maFonction1 !== undefined) && (maFonction1 !== '') && (maFonction1 !== 'trinome') && (maFonction1 !== 'affine') && (maFonction1 !== 'constante')) {
      // la fonction 1 est imposée
      const maFonctionDonnee1 = ecrireFct(maFonction1)
      fctTxt1 = maFonctionDonnee1.fctLatex
      coefs1 = maFonctionDonnee1.tab_coef
    }
    if ((maFonction2 !== undefined) && (maFonction2 !== '') && (maFonction2 !== 'trinome') && (maFonction2 !== 'affine') && (maFonction2 !== 'constante')) {
      // la fonction 2 est imposée
      const maFonctionDonnee2 = ecrireFct(maFonction2)
      fctTxt2 = maFonctionDonnee2.fctLatex
      coefs2 = maFonctionDonnee2.tab_coef
    }
    stor.objetPolynome = rechercheRacine([fctTxt1, coefs1, maFonction1], [fctTxt2, coefs2, maFonction2])
    // objet_polynôme gère les polynômes présents dans l’égalité
    // il renvoie les deux polynômes (aléatoires ou fixés)
    // on a aussi la différence qui ramène l’égalité à ax^2+bx+c=0
    // ensuite, on récupère les racines de ce trinôme
    // et pour l’intersection des 2 courbes, j’ai aussi besoin des images de chaque racine par un polynôme de base
    // {polyGauche:poly1,polyDroite:poly2,trinome:polyDif,tabRacines:tabSol,lesImages:tabImage,tabCoefs1:[c1,b1,a1],tabCoefs2:[c2,b2,a2],tabCoefsDif:[cDif,bDif,aDif,deltaTxt],valeurRacines:tabValSol,valeurImages:tabValImage};
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    if (ds.intersectionCourbes) {
      let laConsigne1 = textes.consigne4_1
      if ((!stor.objetPolynome.polyGauche.includes('x^2')) && (stor.objetPolynome.polyDroite.includes('x^2'))) {
        laConsigne1 = textes.consigne4_3
      } else if ((stor.objetPolynome.polyGauche.includes('x^2')) && (!stor.objetPolynome.polyDroite.includes('x^2'))) {
        laConsigne1 = textes.consigne4_2
      }
      j3pAffiche(zoneCons1, '', laConsigne1, { e: stor.objetPolynome.polyGauche, f: stor.objetPolynome.polyDroite })
    } else {
      const equation = stor.objetPolynome.polyGauche + '=' + stor.objetPolynome.polyDroite
      j3pAffiche(zoneCons1, '', textes.consigne1, { e: equation })
    }
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    let elt
    if (ds.intersectionCourbes) {
      stor.typeCourbes = 'deux paraboles'
      let laConsigne2 = textes.consigne5_1
      if ((!stor.objetPolynome.polyGauche.includes('x^2')) && (stor.objetPolynome.polyDroite.includes('x^2'))) {
        stor.typeCourbes = 'droite et parabole'
        laConsigne2 = textes.consigne5_3
      } else if ((stor.objetPolynome.polyGauche.includes('x^2')) && (!stor.objetPolynome.polyDroite.includes('x^2'))) {
        stor.typeCourbes = 'parabole et droite'
        laConsigne2 = textes.consigne5_2
      }
      elt = j3pAffiche(zoneCons2, '', laConsigne2,
        {
          input1: { texte: '', dynamique: true, width: '12px', maxchars: 1 }
        })
    } else {
      elt = j3pAffiche(zoneCons2, '', textes.consigne2,
        {
          input1: { texte: '', dynamique: true, width: '12px', maxchars: 1 }
        })
    }
    stor.zoneInput = [elt.inputList[0]]
    j3pRestriction(stor.zoneInput[0], '0-9')
    stor.zoneInput[0].typeReponse = ['nombre', 'exact']
    stor.zoneInput[0].reponse = stor.objetPolynome.tabRacines.length
    stor.zoneInput[0].addEventListener('input', suiteEnonce, true)
    if (stor.objetPolynome.tabRacines.length === 1) {
      // delta est nul, une seule racine
      stor.zoneInput[0].reponse = [1]
      stor.x0Val = j3pCalculValeur(stor.objetPolynome.tabRacines[0])
      stor.x0Image = j3pCalculValeur(stor.objetPolynome.lesImages[0])
    } else if (stor.objetPolynome.tabRacines.length === 2) {
      stor.zoneInput[0].reponse = [2]
      stor.x1Val = j3pCalculValeur(stor.objetPolynome.tabRacines[0])
      stor.x2Val = j3pCalculValeur(stor.objetPolynome.tabRacines[1])
      stor.x1Image = j3pCalculValeur(stor.objetPolynome.lesImages[0])
      stor.x2Image = j3pCalculValeur(stor.objetPolynome.lesImages[1])
    } else {
      stor.zoneInput[0].reponse = [0]
    }
    j3pFocus(stor.zoneInput[0])
    stor.fonction1 = j3pCalculValeur(stor.objetPolynome.tabCoefs1[0]) + '+(' + j3pCalculValeur(stor.objetPolynome.tabCoefs1[1]) + ')*x+(' + j3pCalculValeur(stor.objetPolynome.tabCoefs1[2]) + ')*x^2'
    stor.fonction2 = j3pCalculValeur(stor.objetPolynome.tabCoefs2[0]) + '+(' + j3pCalculValeur(stor.objetPolynome.tabCoefs2[1]) + ')*x+(' + j3pCalculValeur(stor.objetPolynome.tabCoefs2[2]) + ')*x^2'
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div')
    stor.laPalette = j3pAddElt(stor.conteneur, 'div')
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    stor.mesZonesSaisie = [stor.zoneInput[0]]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: stor.mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.surcharge({ nbetapes: 1 })

        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        if (ds.intersectionCourbes) {
          me.afficheTitre(textes.titre_exobis)
        } else {
          me.afficheTitre(textes.titre_exo)
        }

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        j3pDetruitFenetres('Courbe')
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        let bonNbSol = true
        let valeursIdentiques = false
        let reponseSimplifiee = true
        let radicalAVirer, rep1
        if (reponse.aRepondu) {
        // il faut aussi que je teste les deux zones dont la validation est perso
          if (!fctsValid.zones.bonneReponse[0]) {
            bonNbSol = false
          }
          const bonnesRacines = [true, true, true, true]
          radicalAVirer = false
          const valeurDelta = j3pCalculValeur(stor.objetPolynome.tabCoefsDif[3])
          if (bonNbSol) {
            if (fctsValid.zones.reponseSaisie[0] === '1') {
            // il y a une solution et c’est bien ce qu’a dit l’élève
              if (reponse.bonneReponse) {
                const rep0 = j3pCalculValeur(fctsValid.zones.reponseSaisie[1])
                bonnesRacines[0] = (Math.abs(rep0 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12))
                if (ds.intersectionCourbes) {
                  rep1 = j3pCalculValeur(fctsValid.zones.reponseSaisie[2])
                  bonnesRacines[1] = (Math.abs(rep1 - stor.objetPolynome.valeurImages[0]) < Math.pow(10, -12))
                  reponse.bonneReponse = (bonnesRacines[0] && bonnesRacines[1])
                } else {
                  reponse.bonneReponse = bonnesRacines[0]
                }
                // si elle est bonne on vérifie que le résultat est simplifié.
                if (ds.intersectionCourbes) {
                  if (!fctsValid.zones.reponseSimplifiee[2][0] || !fctsValid.zones.reponseSimplifiee[2][1]) {
                  // c’est que la réponse n’est pas simplifiée
                    reponseSimplifiee = false
                    if ((Math.abs(Math.sqrt(valeurDelta) - Math.round(Math.sqrt(valeurDelta))) < Math.pow(10, -12)) && (fctsValid.zones.reponseSaisie[2].indexOf('sqrt') > -1)) {
                    // la réponse n’est vraiment pas simplifiée. Il faut le faire
                      reponse.aRepondu = false
                      j3pFocus(stor.zoneInput[2])
                    }
                  } else {
                    fctsValid.zones.bonneReponse[2] = true
                  }
                }
                if (!fctsValid.zones.reponseSimplifiee[1][0] || !fctsValid.zones.reponseSimplifiee[1][1]) {
                // c’est que la réponse n’est pas simplifiée
                  reponseSimplifiee = false
                  if ((Math.abs(Math.sqrt(valeurDelta) - Math.round(Math.sqrt(valeurDelta))) < Math.pow(10, -12)) && (fctsValid.zones.reponseSaisie[1].indexOf('sqrt') > -1)) {
                  // la réponse n’est vraiment pas simplifiée. Il faut le faire
                    reponse.aRepondu = false
                    j3pFocus(stor.zoneInput[1])
                  }
                } else {
                  fctsValid.zones.bonneReponse[1] = true
                }
              } else {
                fctsValid.zones.bonneReponse[1] = false
                if (ds.intersectionCourbes) {
                  fctsValid.zones.bonneReponse[2] = false
                }
              }
              if (reponse.aRepondu) {
                fctsValid.coloreUneZone(stor.zoneInput[1])
                if (ds.intersectionCourbes) {
                  fctsValid.coloreUneZone(stor.zoneInput[2])
                }
              }
            } else if (fctsValid.zones.reponseSaisie[0] === '2') {
            // il y a deux solutions et c’est bien ce qu’a dit l’élève
            // on vérifie déjà si les réponses données sont bonnes
              rep1 = j3pCalculValeur(fctsValid.zones.reponseSaisie[1])
              const rep2 = j3pCalculValeur(fctsValid.zones.reponseSaisie[2])
              let bonneRep1, bonneRep2
              let rep3, rep4
              if (ds.intersectionCourbes) {
                rep3 = j3pCalculValeur(fctsValid.zones.reponseSaisie[3])
                rep4 = j3pCalculValeur(fctsValid.zones.reponseSaisie[4])
                // présence du premier couple (coordonnées du premier point)
                bonneRep1 = ((Math.abs(rep1 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) && (Math.abs(rep2 - stor.objetPolynome.valeurImages[0]) < Math.pow(10, -12)))
                const bonneRep1bis = ((Math.abs(rep3 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) && (Math.abs(rep4 - stor.objetPolynome.valeurImages[0]) < Math.pow(10, -12)))
                // présence du deuxième couple (coordonnées du deuxième point)
                bonneRep2 = ((Math.abs(rep1 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12)) && (Math.abs(rep2 - stor.objetPolynome.valeurImages[1]) < Math.pow(10, -12)))
                const bonneRep2bis = ((Math.abs(rep3 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12)) && (Math.abs(rep4 - stor.objetPolynome.valeurImages[1]) < Math.pow(10, -12)))
                // même réponses
                reponse.bonneReponse = ((bonneRep1 || bonneRep1bis) && (bonneRep2 || bonneRep2bis))
              } else {
                bonneRep1 = ((Math.abs(rep1 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) && (Math.abs(rep2 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12)))
                bonneRep2 = ((Math.abs(rep1 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12)) && (Math.abs(rep2 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)))
                reponse.bonneReponse = (bonneRep1 || bonneRep2)
              }
              if (ds.intersectionCourbes) {
                me.logIfDebug('rep1 : ' + rep1 + '   rep2:' + rep2 + '   rep3 : ' + rep3 + '   rep4:' + rep4 + '   racines attendues : stor.x1Val:' + stor.objetPolynome.valeurRacines[0] + '   stor.x1Image:' + stor.objetPolynome.valeurImages[0] + '   stor.x2Val:' + stor.objetPolynome.valeurRacines[1] + '   stor.x2Image:' + stor.objetPolynome.valeurImages[1])
              } else {
                me.logIfDebug('rep1 : ' + rep1 + '   rep2:' + rep2 + '   racines attendues : stor.x1Val:' + stor.objetPolynome.valeurRacines[0] + '  stor.x2Val:' + stor.objetPolynome.valeurRacines[1])
              }
              if (reponse.bonneReponse) {
              // on vérifie que les réponses données sont simplifiées
                let nbZones = 2
                if (ds.intersectionCourbes) {
                  nbZones = 4
                  reponseSimplifiee = (fctsValid.zones.reponseSimplifiee[1][0] && fctsValid.zones.reponseSimplifiee[1][1] && fctsValid.zones.reponseSimplifiee[2][0] && fctsValid.zones.reponseSimplifiee[2][1] && fctsValid.zones.reponseSimplifiee[3][0] && fctsValid.zones.reponseSimplifiee[3][1] && fctsValid.zones.reponseSimplifiee[4][0] && fctsValid.zones.reponseSimplifiee[4][1])
                } else {
                  reponseSimplifiee = (fctsValid.zones.reponseSimplifiee[1][0] && fctsValid.zones.reponseSimplifiee[1][1] && fctsValid.zones.reponseSimplifiee[2][0] && fctsValid.zones.reponseSimplifiee[2][1])
                }
                if (!reponseSimplifiee) {
                // c’est que la réponse n’est pas simplifiée
                // je peux aussi regarder si la réponse est fractionnaire et qu’il donne encore une racine carrée
                  if (Math.abs(Math.sqrt(valeurDelta) - Math.round(Math.sqrt(valeurDelta))) < Math.pow(10, -12)) {
                    for (let k = nbZones; k > 0; k--) {
                      if ((fctsValid.zones.reponseSaisie[k].includes('sqrt'))) {
                        reponse.aRepondu = false
                        radicalAVirer = true
                        j3pFocus(stor.zoneInput[k])
                      }
                    }
                  }
                } else {
                // c’est bon
                  fctsValid.zones.bonneReponse[1] = true
                  fctsValid.zones.bonneReponse[2] = true
                  if (ds.intersectionCourbes) {
                    fctsValid.zones.bonneReponse[3] = true
                    fctsValid.zones.bonneReponse[4] = true
                  }
                }
              // {polyGauche:poly1,polyDroite:poly2,trinome:polyDif,tabRacines:tabSol,lesImages:tabImage,tabCoefs1:[c1,b1,a1],tabCoefs2:[c2,b2,a2],tabCoefsDif:[cDif,bDif,aDif,deltaTxt],valeurRacines:tabValSol}
              } else {
              // on vérifie tout de même que l’élève ne donne pas 2 fois la même valeur (correcte)
                if (ds.intersectionCourbes) {
                  valeursIdentiques = (((Math.abs(rep1 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) && (Math.abs(rep1 - rep3) < Math.pow(10, -12)) && (Math.abs(rep2 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12))) || ((Math.abs(rep1 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12)) && (Math.abs(rep1 - rep3) < Math.pow(10, -12)) && (Math.abs(rep2 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12))))
                } else {
                  valeursIdentiques = (((Math.abs(rep1 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) && (Math.abs(rep1 - rep2) < Math.pow(10, -12))) || ((Math.abs(rep1 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12)) && (Math.abs(rep1 - rep2) < Math.pow(10, -12))))
                }
                if (valeursIdentiques) {
                  if (ds.intersectionCourbes) {
                    fctsValid.zones.bonneReponse[1] = true
                    fctsValid.zones.bonneReponse[2] = true
                    fctsValid.zones.bonneReponse[3] = false
                    fctsValid.zones.bonneReponse[4] = false
                  } else {
                    fctsValid.zones.bonneReponse[1] = true
                    fctsValid.zones.bonneReponse[2] = false
                  }
                  reponse.aRepondu = false
                } else {
                // on regarde si l’une des réponse n’est pas exacte malgré tout
                  if ((Math.abs(rep1 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) || (Math.abs(rep1 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12))) {
                  // la première réponse est bonne (et pas la deuxième)
                    fctsValid.zones.bonneReponse[1] = true
                    if (ds.intersectionCourbes) {
                      fctsValid.zones.bonneReponse[2] = (((Math.abs(rep1 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) && (Math.abs(rep2 - stor.objetPolynome.valeurImages[0]) < Math.pow(10, -12))) || ((Math.abs(rep1 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12)) && (Math.abs(rep2 - stor.objetPolynome.valeurImages[1]) < Math.pow(10, -12))))
                      fctsValid.zones.bonneReponse[3] = false
                      fctsValid.zones.bonneReponse[4] = false
                      if (((Math.abs(rep3 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) || (Math.abs(rep3 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12))) && (Math.abs(rep3 - rep1) > Math.pow(10, -12))) {
                        fctsValid.zones.bonneReponse[3] = true
                        if (((Math.abs(rep3 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) && (Math.abs(rep4 - stor.objetPolynome.valeurImages[0]) < Math.pow(10, -12))) || ((Math.abs(rep3 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12)) && (Math.abs(rep4 - stor.objetPolynome.valeurImages[1]) < Math.pow(10, -12)))) {
                          fctsValid.zones.bonneReponse[4] = true
                        }
                      }
                    } else {
                      fctsValid.zones.bonneReponse[2] = false
                    }
                  // {polyGauche:poly1,polyDroite:poly2,trinome:polyDif,tabRacines:tabSol,lesImages:tabImage,tabCoefs1:[c1,b1,a1],tabCoefs2:[c2,b2,a2],tabCoefsDif:[cDif,bDif,aDif,deltaTxt],valeurRacines:tabValSol,valeurImages:tabValImage};
                  } else if (ds.intersectionCourbes && ((Math.abs(rep3 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) || (Math.abs(rep3 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12)))) {
                  // la troisème réponse est bonne (et pas les 2 premières)
                    fctsValid.zones.bonneReponse[3] = true
                    fctsValid.zones.bonneReponse[4] = (((Math.abs(rep3 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) && (Math.abs(rep4 - stor.objetPolynome.valeurImages[0]) < Math.pow(10, -12))) || ((Math.abs(rep3 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12)) && (Math.abs(rep4 - stor.objetPolynome.valeurImages[1]) < Math.pow(10, -12))))
                    fctsValid.zones.bonneReponse[1] = false
                    fctsValid.zones.bonneReponse[2] = false
                  } else if (!ds.intersectionCourbes && ((Math.abs(rep2 - stor.objetPolynome.valeurRacines[0]) < Math.pow(10, -12)) || (Math.abs(rep2 - stor.objetPolynome.valeurRacines[1]) < Math.pow(10, -12)))) {
                  // la deuxième réponse est bonne (et pas la première)
                    fctsValid.zones.bonneReponse[1] = false
                    fctsValid.zones.bonneReponse[2] = true
                  } else {
                  // les réponses sont fausses
                    fctsValid.zones.bonneReponse[1] = false
                    fctsValid.zones.bonneReponse[2] = false
                    if (ds.intersectionCourbes) {
                      fctsValid.zones.bonneReponse[3] = false
                      fctsValid.zones.bonneReponse[4] = false
                    }
                  }
                }
              }
              if (reponse.aRepondu) {
                fctsValid.coloreUneZone(stor.zoneInput[1])
                fctsValid.coloreUneZone(stor.zoneInput[2])
                if (ds.intersectionCourbes) {
                  fctsValid.coloreUneZone(stor.zoneInput[3])
                  fctsValid.coloreUneZone(stor.zoneInput[4])
                }
              }
            }
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (bonNbSol && (fctsValid.zones.reponseSaisie[0] === '2') && valeursIdentiques) {
            msgReponseManquante = textes.comment1
          } else if (!reponseSimplifiee) {
            if (fctsValid.zones.reponseSaisie[0] === '1') {
              if (ds.intersectionCourbes) {
                msgReponseManquante = textes.comment4
              } else {
                if (fctsValid.zones.reponseSaisie[1].includes('sqrt')) {
                  msgReponseManquante = textes.comment3_2
                } else {
                  msgReponseManquante = textes.comment3
                }
              }
            } else if (fctsValid.zones.reponseSaisie[0] === '2') {
              if (radicalAVirer) {
                msgReponseManquante = textes.comment4
              }
            }
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              j3pDesactive(stor.zoneInput[0])
              afficheCorr(false)
            } else {
              stor.zoneCorr.innerHTML = cFaux
              if (!bonNbSol) {
                if (ds.intersectionCourbes) {
                  stor.zoneCorr.innerHTML = textes.comment2_2
                } else {
                  stor.zoneCorr.innerHTML = textes.comment2_1
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
              }
            }
          }
        }
        // Obligatoire
        me.finCorrection('navigation', true)
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
