import { j3pAddElt, j3pBarre, j3pNombre, j3pFocus, j3pFreezeElt, j3pMathquillXcas, j3pMonome, j3pRandomTab, j3pGetBornesIntervalle, j3pGetRandomInt, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
 * détermination de l’extremum à l’aide de la forme canonique a(x-alpha)^2+beta
 *  Développeur : Rémi Deniaud
 *02/2013
 *
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[-5;5]', 'string', 'coefficient a dans l’écriture a(x-alpha)^2+beta. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;4]|[-4;-2]" pour qu’il soit égal à 2 dans un premier temps, positif ensuite puis négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier ou fractionnaire.'],
    ['alpha', '[-5;5]', 'string', 'coefficient alpha dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['beta', '[-5;5]', 'string', 'coefficient beta dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.5, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'confusion min et max' },
    { pe_3: 'confusion abscisse et ordonnée' },
    { pe_4: 'Problème avec l’abscisse' },
    { pe_5: 'insuffisant' }
  ]
}

const textes = {
  titre_exo: 'Forme canonique et extremum',
  phrase_erreur1: 'Tu confonds le maximum et le minimum&nbsp;!',
  phrase_erreur2: 'Tu confonds l’abscisse et l’ordonnée&nbsp;!',
  phrase_erreur3: 'Attention au signe de l’abscisse&nbsp;!',
  consigne1: 'Sur $\\R$, on définit une fonction $£f$ mise sous forme canonique : $£g$',
  consigne2: 'La fonction $£f$ admet un #1# sur $\\R$ égal à &1&.<br>',
  consigne3: 'Il est atteint pour $x=$&2&.',
  phrase_corr1: '$£f(x)$ est sous la forme $a(x-\\alpha)^2+\\beta$ où $a=£a$, $\\alpha=£b$ et $\\beta=£c$.',
  phrase_corr2: 'Comme $a<0$, $£f$ admet un maximum égal à $\\beta=£c$ et atteint pour $x=\\alpha=£b$.',
  phrase_corr3: 'Comme $a>0$, $£f$ admet un minimum égal à $\\beta=£c$ et atteint pour $x=\\alpha=£b$.',
  prop1: 'minimum',
  prop2: 'maximum'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage
  stor.signesA = [] // ceci va me servir à faire en sorte que si a peut être aussi positif que négatif alors je vais garder en mémoire les signes déjà obtenus et m’assurer qu’on ait au moins 2 signes contraires
  function afficheCorrection (monA, monB, monC, bonneReponse) {
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.petit.correction.color } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.phrase_corr1,
      {
        a: monA,
        b: monB,
        c: monC,
        f: stor.nomF
      }
    )
    if (monA < 0) {
      j3pAffiche(zoneExpli2, '', textes.phrase_corr2,
        {
          b: monB,
          c: monC,
          f: stor.nomF
        }
      )
    } else {
      j3pAffiche(zoneExpli2, '', textes.phrase_corr3,
        {
          b: monB,
          c: monC,
          f: stor.nomF
        }
      )
    }
  }
  function constructionTabIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).includes('|')) {
      const tabA = texteIntervalle.split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (testIntervalleFermeEntiers.test(tabA[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(tabA[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if (!tabA[k] || (tabA[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(tabA[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tabA[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(tabA[k]))) {
            // on a un nombre
            tabIntervalle.push(tabA[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if (!texteIntervalle || (texteIntervalle === '')) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (testIntervalleFermeEntiers.test(texteIntervalle)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    return tabIntervalle
  }
  function lgIntervalle (intervalle) {
    // cette fonction renvoie la longueur de l’intervalle'
    // si ce n’est pas un intervalle, alors elle renvoie 0'
    let lg = 0
    if (testIntervalleFermeEntiers.test(intervalle)) {
      // on a bien un intervalle
      const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
      const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
      lg = borneSup - borneInf
    }
    return lg
  }
  function enonceInitFirst () {
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.67 })
    me.stockage = [0, 0, 0, 0]
    for (let i = 0; i <= 15; i++) {
      me.typederreurs[i] = 0
    }

    // me.videLesZones();
    me.afficheTitre(textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    stor.coef_a = constructionTabIntervalle(ds.a, '[-5;5]')
    stor.coef_alpha = constructionTabIntervalle(ds.alpha, '[-5;5]')
    stor.coef_beta = constructionTabIntervalle(ds.beta, '[-5;5]')
  }

  function enonceMain () {
    const nomF = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
    stor.nomF = nomF
    if ((stor.coef_a[me.questionCourante - 1] === '[0;0]') || (stor.coef_a[me.questionCourante - 1] === '0')) {
      j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à a de valoir 0', { vanishAfter: 5 })
      stor.coef_a[me.questionCourante - 1] = '[-5;5]'
    }
    const leA = stor.coef_a[me.questionCourante - 1]
    let coefAlphaTxt
    let coefBetaTxt
    let coefATxt
    let alpha, beta, coeffA, coeffAlpha, coeffBeta
    do {
      alpha = stor.coef_alpha[me.questionCourante - 1]
      beta = stor.coef_beta[me.questionCourante - 1]
      me.logIfDebug('leA:' + leA + '   alpha:' + alpha + '   beta:' + beta)
      if (leA.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffA = j3pNombre(leA.split('|')[1])
        coefATxt = leA.split('|')[0]
      } else if (!isNaN(j3pNombre(leA))) {
        coeffA = j3pNombre(leA)
      } else {
        const [lemin, lemax] = j3pGetBornesIntervalle(leA)
        coeffA = j3pGetRandomInt(lemin, lemax)
        if (lemin * lemax < 0) {
          const [signeA, signeAContraire] = (coeffA < 0) ? ['-', '+'] : ['+', '-']
          if (stor.signesA.length === 1 && stor.signesA.includes(signeA)) {
            stor.signesA.push(signeAContraire)
            coeffA *= -1
          } else {
            stor.signesA.push(signeA)
          }
        }
      }
      if (alpha.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffAlpha = j3pNombre(alpha.split('|')[1])
        coefAlphaTxt = alpha.split('|')[0]
      } else if (!isNaN(j3pNombre(alpha))) {
        coeffAlpha = j3pNombre(alpha)
      } else {
        const [lemin, lemax] = j3pGetBornesIntervalle(alpha)
        coeffAlpha = j3pGetRandomInt(lemin, lemax)
      }
      let pb1
      do {
        if (beta.includes('frac')) {
          // c’est un nb en écriture fractionnaire'
          coeffBeta = j3pNombre(beta.split('|')[1])
          coefBetaTxt = beta.split('|')[0]
        } else if (!isNaN(j3pNombre(beta))) {
          coeffBeta = j3pNombre(beta)
        } else {
          const [lemin, lemax] = j3pGetBornesIntervalle(beta)
          coeffBeta = j3pGetRandomInt(lemin, lemax)
        }
        pb1 = (coeffAlpha === coeffBeta) && !((alpha === beta) && (lgIntervalle(alpha) === 0))
      } while (pb1)
    } while ((coeffA === 0) || ((coeffAlpha === 0) && ((alpha !== '[0;0]') && (alpha !== '0') && (alpha !== 0))) || ((coeffBeta === 0) && ((beta !== '[0;0]') && (beta !== '0') && (beta !== 0))))
    let ecritureA
    if (leA.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
      if (coeffA < 0) {
        ecritureA = '+' + coefATxt
      } else {
        ecritureA = '-' + coefATxt
      }
    } else {
      if (coeffA === -1) {
        ecritureA = '-'
      } else if (coeffA === 1) {
        ecritureA = ''
      } else {
        ecritureA = coeffA
      }
    }
    const maphrase1 = textes.consigne1
    let ecritureBeta = j3pMonome(2, 0, coeffBeta)
    let ecritureAlpha = j3pMonome(2, 0, -coeffAlpha)
    if (alpha.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
      if (coeffAlpha < 0) {
        ecritureAlpha = '+' + coefAlphaTxt
      } else {
        ecritureAlpha = '-' + coefAlphaTxt
      }
    }
    if (beta.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
      if (coeffBeta > 0) {
        ecritureBeta = '+' + coefBetaTxt
      } else {
        ecritureBeta = '-' + coefBetaTxt
      }
    }
    me.logIfDebug('ecritureA:' + ecritureA + '   ecritureAlpha:' + ecritureAlpha + '   ecritureBeta:' + ecritureBeta)
    const maformeCano = (coeffAlpha === 0)
      ? nomF + '(x)=' + ecritureA + 'x^2' + ecritureBeta
      : nomF + '(x)=' + ecritureA + '\\left(x' + ecritureAlpha + '\\right)^2' + ecritureBeta
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', maphrase1, { f: nomF, g: maformeCano })

    if (coeffA < 0) {
      me.stockage[0] = textes.prop2
      me.stockage[15] = 2
    } else {
      me.stockage[0] = textes.prop1
      me.stockage[15] = 1
    }
    stor.signe_coefs = []
    stor.signe_coefs[0] = (coeffAlpha < 0) ? '-' : ''
    stor.signe_coefs[1] = (coeffBeta < 0) ? '-' : ''
    stor.signe_coefs[2] = (coeffA < 0) ? '-' : ''
    if (beta.includes('frac')) {
      me.stockage[1] = stor.signe_coefs[1] + coefBetaTxt
    } else {
      me.stockage[1] = coeffBeta
    }
    stor.coeffAlpha = coeffAlpha
    if (alpha.includes('frac')) {
      me.stockage[2] = stor.signe_coefs[0] + coefAlphaTxt
    } else {
      me.stockage[2] = coeffAlpha
    }
    if (leA.includes('frac')) {
      me.stockage[3] = stor.signe_coefs[2] + coefATxt
    } else {
      me.stockage[3] = coeffA
    }
    stor.coeffBeta = coeffBeta
    stor.tab_extremum = ['', textes.prop1, textes.prop2]
    const elt1 = j3pAffiche(stor.zoneCons2, '', textes.consigne2,
      {
        f: nomF,
        inputmq1: { texte: '', maxchars: '4', restrict: /[0-9,.-]/, correction: me.stockage[1] },
        liste1: { texte: stor.tab_extremum, correction: me.stockage[15] }
      })
    const elt2 = j3pAffiche(stor.zoneCons3, '', textes.consigne3,
      {
        f: nomF,
        inputmq2: { texte: '', maxchars: '4', restrict: /[0-9,.-]/, correction: me.stockage[2] }
      })
    stor.zoneInput = [elt1.inputmqList[0], elt2.inputmqList[0], elt1.selectList[0]]
    mqRestriction(stor.zoneInput[0], '\\d.,-/')
    mqRestriction(stor.zoneInput[1], '\\d.,-/')
    j3pFocus(stor.zoneInput[0])
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()
      break

    case 'correction':
      {
        // je récupère les réponses entrées
        const reponseIndex1 = stor.zoneInput[2].selectedIndex
        let reponseEleve1 = j3pValeurde(stor.zoneInput[0])
        let reponseEleve2 = j3pValeurde(stor.zoneInput[1])
        me.logIfDebug('reponseEleve1:' + reponseEleve1 + '  et reponseEleve2:' + reponseEleve2)
        const aRepondu = ((reponseEleve1 !== '') && (reponseEleve2 !== '') && (reponseIndex1 !== 0))
        if (!aRepondu && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput[0])
          return me.finCorrection()
        } else {
        // on bloque les listes
          stor.zoneInput[2].disabled = true
          // on vérifie toutes les réponses des élèves
          const bonneRep = []
          bonneRep[1] = (reponseIndex1 === me.stockage[15])

          // validation des zones de saisie
          const arbre1 = new Tarbre(j3pMathquillXcas(reponseEleve1), [])
          reponseEleve1 = arbre1.evalue([])
          const arbre2 = new Tarbre(j3pMathquillXcas(reponseEleve2), [])
          reponseEleve2 = arbre2.evalue([])

          me.logIfDebug('reponseEleve1:' + reponseEleve1 + '  et reponseEleve2:' + reponseEleve2)
          me.logIfDebug('stor.coeffAlpha:' + stor.coeffAlpha + '  stor.coeffBeta:' + stor.coeffBeta)

          bonneRep[2] = (Math.abs(reponseEleve1 - stor.coeffBeta) < Math.pow(10, -12))
          bonneRep[3] = (Math.abs(reponseEleve2 - stor.coeffAlpha) < Math.pow(10, -12))
          let bonneReponse = true
          me.logIfDebug('bonneRep:' + bonneRep)
          for (let i = 1; i <= 3; i++) bonneReponse = (bonneReponse && bonneRep[i])
          // on bloque les zones si la réponse est bonne
          for (let i = 2; i >= 1; i--) {
            if (bonneRep[i + 1]) {
              stor.zoneInput[i - 1].setAttribute('style', 'color:' + me.styles.cbien)
              j3pDesactive(stor.zoneInput[i - 1])
              j3pFreezeElt(stor.zoneInput[i - 1])
            } else {
              stor.zoneInput[i - 1].setAttribute('style', 'color:' + me.styles.cfaux)
              j3pFocus(stor.zoneInput[i - 1])
            }
          }
          if (bonneRep[1]) {
            stor.zoneInput[2].setAttribute('style', 'color:' + me.styles.cbien + ';font-size:20px')
          } else {
            stor.zoneInput[2].setAttribute('style', 'font-size:20px ;color:' + me.styles.cfaux)
          }
          if (bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            afficheCorrection(me.stockage[3], me.stockage[2], me.stockage[1], true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              for (let i = 1; i <= 2; i++) {
                j3pDesactive(stor.zoneInput[i - 1])
                j3pFreezeElt(stor.zoneInput[i - 1])
              }
              j3pDesactive(stor.zoneInput[2])
              afficheCorrection(me.stockage[3], me.stockage[2], me.stockage[1], false)
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // on réactive la liste
                stor.zoneInput[2].disabled = false
                if (bonneRep[2] && bonneRep[3] && !bonneRep[1]) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur1
                } else if (bonneRep[1] && ((Math.abs(reponseEleve1 - j3pCalculValeur(me.stockage[2])) < Math.pow(10, -12)) && (Math.abs(reponseEleve2 - j3pCalculValeur(me.stockage[1])) < Math.pow(10, -12)))) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur2
                } else if (bonneRep[1] && bonneRep[2] && (Math.abs(reponseEleve2 + j3pCalculValeur(me.stockage[2])) < Math.pow(10, -12))) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur3
                }
                return me.finCorrection()
              } else {
              // Erreur au second essai
              // on barre les mauvaises réponses
                me.typederreurs[2]++
                // on bloque aussi les zones fausses
                for (let i = 1; i <= 2; i++) {
                  if (!bonneRep[i + 1]) {
                    stor.zoneInput[i - 1].setAttribute('style', 'color:' + me.styles.cfaux)
                    j3pDesactive(stor.zoneInput[i - 1])
                  }
                }
                if (!bonneRep[1]) j3pBarre(stor.zoneInput[2])
                if (!bonneRep[2]) j3pBarre(stor.zoneInput[0])
                if (!bonneRep[3]) j3pBarre(stor.zoneInput[1])
                if (bonneRep[2] && bonneRep[3] && !bonneRep[1]) {
                  me.typederreurs[3]++
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur1
                } else if (bonneRep[1] && ((reponseEleve1 === me.stockage[2]) && (reponseEleve2 === me.stockage[1]))) {
                  me.typederreurs[4]++
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur2
                } else if (bonneRep[1] && bonneRep[2] && reponseEleve2 === -me.stockage[2]) {
                  me.typederreurs[5]++
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur3
                } else {
                  me.typederreurs[6]++
                }
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorrection(me.stockage[3], me.stockage[2], me.stockage[1], false)
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // la section est terminée, il faut déterminer la phrase d’état
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          const nbErreurs = ds.nbitems - me.score
          if (me.typederreurs[3] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de forme (non reconnaissance de la forme canonique) est supé au égal au seul fixé
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[4] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de forme (non reconnaissance de la forme canonique) est supé au égal au seul fixé
            me.parcours.pe = ds.pe_3
          } else if (me.typederreurs[5] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de forme (non reconnaissance de la forme canonique) est supé au égal au seul fixé
            me.parcours.pe = ds.pe_4
          } else {
            me.parcours.pe = ds.pe_5
          }
        }
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
