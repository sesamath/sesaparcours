import { j3pAddElt, j3pNombre, j3pFocus, j3pMathquillXcas, j3pMonome, j3pRandomTab, j3pGetBornesIntervalle, j3pGetRandomInt, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    janvier 2015
    Dans cette section, on donne la forme canonique d’une fonction pol de degré 2 et on demande de donner deux images égales
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[-5;5]', 'string', 'coefficient a dans l’écriture a(x-alpha)^2+beta. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;4]|[-4;-2]" pour qu’il soit égal à 2 dans un premier temps, positif ensuite puis négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier ou fractionnaire.'],
    ['alpha', '[-5;5]', 'string', 'coefficient alpha dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['beta', '[-5;5]', 'string', 'coefficient beta dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.']
  ]
}
const textes = {
  titre_exo: 'Forme canonique et même image',
  consigne1: 'On considère une fonction $£f$ définie sur $\\R$ par<br>$£g$.',
  consigne2: 'Sans calcul, compléter l’égalité suivante par deux nombres différents :',
  comment1: 'Les deux valeurs doivent être différentes !',
  corr1: '$£f(x)$ est écrit sous forme canonique $a(x-\\alpha)^2+\\beta$ avec $a=£a$, $\\alpha=£b$ et $\\beta=£c$. Or la droite d’équation $x=\\alpha$, c’est-à-dire $x=£b$ est l’axe de symétrie de la courbe représentant $£f$.',
  corr2: 'Il suffit, par exemple, de choisir les valeurs $\\alpha-1$ et $\\alpha+1$.<br>Ainsi $£f(£n)=£f(£m)$.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1,
      {
        f: stor.nomF,
        a: stor.val_a,
        b: stor.val_alpha,
        c: stor.val_beta
      })

    j3pAffiche(zoneExpli2, '', textes.corr2,
      {
        f: stor.nomF,
        n: stor.val_inf,
        m: stor.val_sup
      })
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).includes('|')) {
      const tabA = texteIntervalle.split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (testIntervalleFermeEntiers.test(tabA[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(tabA[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if (!tabA[k] || (tabA[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(tabA[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tabA[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(tabA[k]))) {
            // on a un nombre
            tabIntervalle.push(tabA[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if (!texteIntervalle || (texteIntervalle === '')) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (testIntervalleFermeEntiers.test(texteIntervalle)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    return tabIntervalle
  }
  function lgIntervalle (intervalle) {
    // cette fonction renvoie la longueur de l’intervalle'
    // si ce n’est pas un intervalle, alors elle renvoie 0'
    let lg = 0
    if (testIntervalleFermeEntiers.test(intervalle)) {
      // on a bien un intervalle
      const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
      const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
      lg = borneSup - borneInf
    }
    return lg
  }

  // console.log("debut du code : ",me.etat)
  function enonceMain () {
    const nomF = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
    stor.nomF = nomF
    const alpha = String(stor.coef_alpha[me.questionCourante - 1])
    const beta = String(stor.coef_beta[me.questionCourante - 1])
    if ((stor.coef_a[me.questionCourante - 1] === '[0;0]') || (stor.coef_a[me.questionCourante - 1] === '0')) {
      j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à a de valoir 0')
      stor.coef_a[me.questionCourante - 1] = '[-4;4]'
    }
    const leA = String(stor.coef_a[me.questionCourante - 1])
    let coefAlphaTxt
    let coefBetaTxt
    let coefATxt
    me.logIfDebug('leA:' + leA + '   alpha:' + alpha + '   beta:' + beta)
    let coeffA, coeffAlpha, coeffBeta
    let pb2
    do {
      coeffA = 0
      if (leA.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffA = j3pNombre(leA.split('|')[1])
        coefATxt = leA.split('|')[0]
      } else if (!isNaN(j3pNombre(leA))) {
        coeffA = j3pNombre(leA)
      } else {
        const [lemin, lemax] = j3pGetBornesIntervalle(leA)
        coeffA = j3pGetRandomInt(lemin, lemax)
      }
      coeffAlpha = 0
      coeffBeta = 0
      if (alpha.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffAlpha = j3pNombre(alpha.split('|')[1])
        coefAlphaTxt = alpha.split('|')[0]
        // je vais aussi chercher les valeurs de alpha-1 et alpha+1
        const tabCoefAlpha = coefAlphaTxt.split('}{')
        const monNum = tabCoefAlpha[0].substring(tabCoefAlpha[0].indexOf('frac') + 5, tabCoefAlpha[0].length)
        const monDen = tabCoefAlpha[1].substring(0, tabCoefAlpha[1].length - 1)
        const alpha1Num = (coeffAlpha >= 0) ? (Math.round(j3pNombre(monNum) - j3pNombre(monDen))) : (Math.round(-j3pNombre(monNum) - j3pNombre(monDen)))
        const alpha2Num = (coeffAlpha >= 0) ? (Math.round(j3pNombre(monNum) + j3pNombre(monDen))) : (Math.round(-j3pNombre(monNum) + j3pNombre(monDen)))
        stor.val_inf = (alpha1Num >= 0) ? '\\frac{' + alpha1Num + '}{' + monDen + '}' : '-\\frac{' + Math.abs(alpha1Num) + '}{' + monDen + '}'
        stor.val_sup = (alpha2Num >= 0) ? '\\frac{' + alpha2Num + '}{' + monDen + '}' : '-\\frac{' + Math.abs(alpha2Num) + '}{' + monDen + '}'
      } else if (!isNaN(j3pNombre(alpha))) {
        coeffAlpha = j3pNombre(alpha)
        stor.val_inf = coeffAlpha - 1
        stor.val_sup = coeffAlpha + 1
      } else {
        const [lemin, lemax] = j3pGetBornesIntervalle(alpha)
        coeffAlpha = j3pGetRandomInt(lemin, lemax)
        stor.val_inf = coeffAlpha - 1
        stor.val_sup = coeffAlpha + 1
      }
      let pb1
      do {
        if (beta.includes('frac')) {
          // c’est un nb en écriture fractionnaire'
          coeffBeta = j3pNombre(beta.split('|')[1])
          coefBetaTxt = beta.split('|')[0]
        } else if (!isNaN(j3pNombre(beta))) {
          coeffBeta = j3pNombre(beta)
        } else {
          const [lemin, lemax] = j3pGetBornesIntervalle(beta)
          coeffBeta = j3pGetRandomInt(lemin, lemax)
        }
        pb1 = (coeffAlpha === coeffBeta) && !((alpha === beta) && (lgIntervalle(alpha) === 0))
      } while (pb1)
      pb2 = (coeffA === 0) || ((coeffAlpha === 0) && ((alpha !== '[0;0]') && (alpha !== '0') && (alpha !== 0))) || ((coeffBeta === 0) && ((beta !== '[0;0]') && (beta !== '0') && (beta !== 0)))
    } while (pb2)
    let ecritureA
    if (leA.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
      if (coeffA < 0) {
        ecritureA = '+' + coefATxt
      } else {
        ecritureA = '-' + coefATxt
      }
    } else {
      if (coeffA === -1) {
        ecritureA = '-'
      } else if (coeffA === 1) {
        ecritureA = ''
      } else {
        ecritureA = coeffA
      }
      coefATxt = coeffA
    }
    let ecritureBeta = j3pMonome(2, 0, coeffBeta)
    let ecritureAlpha = j3pMonome(2, 0, -coeffAlpha)
    if (alpha.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
      if (coeffAlpha < 0) {
        ecritureAlpha = '+' + coefAlphaTxt
      } else {
        ecritureAlpha = '-' + coefAlphaTxt
      }
    } else {
      coefAlphaTxt = Math.abs(coeffAlpha)
    }
    if (beta.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
      if (coeffBeta > 0) {
        ecritureBeta = '+' + coefBetaTxt
      } else {
        ecritureBeta = '-' + coefBetaTxt
      }
    } else {
      coefBetaTxt = Math.abs(coeffBeta)
    }
    me.logIfDebug('ecritureA:' + ecritureA + '   ecritureAlpha:' + ecritureAlpha + '   ecritureBeta:' + ecritureBeta)
    const maFormeCano = (coeffAlpha === 0)
      ? nomF + '(x)=' + ecritureA + 'x^2' + ecritureBeta
      : nomF + '(x)=' + ecritureA + '\\left(x' + ecritureAlpha + '\\right)^2' + ecritureBeta

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '5px' }) })
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1,
      {
        f: nomF,
        g: maFormeCano
      })
    j3pAffiche(stor.zoneCons2, '', textes.consigne2)
    const elt = j3pAffiche(stor.zoneCons3, '', '$£f$(&1&)=$£f$(&2&)',
      {
        f: nomF,
        inputmq1: { texte: '' },
        inputmq2: { texte: '' }
      })
    stor.zoneInput = [...elt.inputmqList]
    mqRestriction(stor.zoneInput[0], '\\d,./+-*')
    mqRestriction(stor.zoneInput[1], '\\d,./+-*')
    j3pFocus(stor.zoneInput[0])
    stor.coeffA = coeffA
    stor.coeffAlpha = coeffAlpha
    stor.coeffBeta = coeffBeta
    stor.val_a = coefATxt
    stor.val_alpha = (coeffAlpha >= 0) ? coefAlphaTxt : '-' + coefAlphaTxt
    stor.val_beta = (coeffBeta >= 0) ? coefBetaTxt : '-' + coefBetaTxt
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    const mesZonesSaisie = stor.zoneInput.map(eltInput => eltInput.id)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesZonesSaisie })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection

        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        stor.coef_a = constructionTabIntervalle(ds.a, '[-5;5]')
        stor.coef_alpha = constructionTabIntervalle(ds.alpha, '[-5;5]')
        stor.coef_beta = constructionTabIntervalle(ds.beta, '[-5;5]')

        me.logIfDebug('stor.coef_alpha:' + stor.coef_alpha)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        let reponseEleve1, reponseEleve2
        let reponsesEgales = false
        if (reponse.aRepondu) {
        // il faut aussi que je teste que les deux valeurs renseignées sont bien différentes
          reponseEleve1 = j3pValeurde(stor.zoneInput[0])
          reponseEleve2 = j3pValeurde(stor.zoneInput[1])
          // validation des zones de saisie
          const arbre1 = new Tarbre(j3pMathquillXcas(reponseEleve1), [])
          reponseEleve1 = arbre1.evalue([])
          const arbre2 = new Tarbre(j3pMathquillXcas(reponseEleve2), [])
          reponseEleve2 = arbre2.evalue([])
          reponsesEgales = (Math.abs(reponseEleve1 - reponseEleve2) < Math.pow(10, -13))
          reponse.aRepondu = !reponsesEgales
        }
        if (reponse.aRepondu) {
        // il faut aussi que je teste les deux zones dont la validation est perso
          let bonneReponse2 = false
          bonneReponse2 = (Math.abs((reponseEleve1 + reponseEleve2) / 2 - stor.coeffAlpha) < Math.pow(10, -13))
          me.logIfDebug('reponseEleve1:' + reponseEleve1 + '   reponseEleve2:' + reponseEleve2 + '  alpha:' + stor.coeffAlpha)
          reponse.bonneReponse = (reponse.bonneReponse && bonneReponse2)
          if (!bonneReponse2) {
            fctsValid.zones.bonneReponse[0] = false
            fctsValid.zones.bonneReponse[1] = false
          }
          // on appelle de nouveau la fonction qui va mettre en couleur les réponses bonnes ou fausses
          fctsValid.coloreUneZone(stor.zoneInput[0].id)
          fctsValid.coloreUneZone(stor.zoneInput[1].id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          if (reponsesEgales) me.reponseManquante(stor.zoneCorr, textes.comment1)
          else me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              j3pDesactive(stor.zoneInput[0])
              j3pDesactive(stor.zoneInput[1])
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++
                afficheCorrection(false)
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
