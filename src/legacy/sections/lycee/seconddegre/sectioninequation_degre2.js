import { j3pAddElt, j3pArrondi, j3pDetruit, j3pFocus, j3pGetRandomBool, j3pGetRandomInt, j3pPaletteMathquill, j3pRandomTab, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexProduit, j3pSimplificationRacineTrinome, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    juillet 2015
    Dans cette section on demande de résoudre une inéquation qui se ramène à l’étude du signe d’un polynôme de degré 2
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['inequation_simple', true, 'boolean', 'Ce paramètre vaut true si on veut avoir 0 à droite de l’inégalité'],
    ['fonctions1', ['', '', ''], 'array', 'Pour chaque répétition, on peut imposer la fonction placée à gauche de l’inégalité. On peut aussi écrire "trinome" (ce qui sera le cas par défaut), "affine" ou "constante" pour imposer le type si fonctions2[i] n’est pas imposée et à condition que l’inéquation ait un membre de droite non nul (paramètre inequation_simple à false).'],
    ['fonctions2', ['', '', ''], 'array', 'Pour chaque répétition, on peut imposer la fonction placée à droite de l’inégalité, par défaut c’est 0. On peut aussi écrire "trinome" (ou ""), "affine" ou "constante" pour imposer le type si fonctions1[i] n’est pas imposée et à condition que l’inéquation ait un membre de droite non nul (paramètre inequation_simple à false).'],
    ['signe_ineq', ['alea', 'alea', 'alea'], 'array', 'Pour chaque répétition on peut imposer le signe d’inégalité ou le laisser aléatoire.'],
    ['nbracines', ['alea', 'alea', 'alea'], 'array', 'Pour chaque répétition, on peut imposer le nombre de racine du trinôme lorsqu’il est aléatoire (0, 1 ou 2), sinon mettre "alea" ou ""'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.5, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'Bien' },
    { pe_2: 'Problème dans le calcul des racines éventuelles' },
    { pe_3: 'Le calcul des racines éventuelles est correct, mais il y a un problème sur l’ensemble des solutions' }
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Résolution d’inéquations<br>Signe d’un trinôme',
  // on donne les phrases de la consigne
  consigne1: 'L’ensemble des solutions de l’inéquation <br>$£i$ est :<br>&1&.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Le séparateur dans un intervalle est un point-virgule et non une virgule !',
  comment2: 'La réponse ne correspond pas à un ensemble de nombres !',
  comment3: 'Les valeurs numériques données sont pourtant correctes !',
  comment4: 'La valeur numérique donnée est pourtant correcte !',
  comment5: 'Une seule des deux valeurs numériques données est correcte !',
  comment6: 'Les valeurs numériques données ne sont pas correctes !',
  comment7: 'Revoir le calcul des racines éventuelles du trinôme !',
  comment8: 'Attention au symbole union !',
  comment9: 'Attention au nombre de racines du trinôme !',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'L’inéquation équivaut à $£i$.',
  corr2: 'Le trinôme est de la forme $ax^2+bx+c$ où $a=£a$, $b=£b$ et $c=£c$. Son discriminant vaut $\\Delta=b^2-4ac=£d$.',
  corr3_1: 'Comme $\\Delta>0$, le trinôme admet deux racines réelles : $x_1=\\frac{-b-\\sqrt{\\Delta}}{2a}=£x$ et $x_2=\\frac{-b+\\sqrt{\\Delta}}{2a}=£y$.',
  corr3_2: 'Comme $\\Delta=0$, le trinôme admet une unique racine réelle : $x_0=\\frac{-b}{2a}=£x$.',
  corr3_3: 'Comme $\\Delta<0$, le trinôme n’admet pas de racine réelle.',
  corr4_1: 'De plus $a=£a>0$, le trinôme est <b>positif</b> à l’extérieur des racines.',
  corr4_2: 'De plus $a=£a<0$, le trinôme est <b>négatif</b> à l’extérieur des racines.',
  corr5_1: 'De plus $a=£a>0$, le trinôme est <b>strictement positif</b> sur $\\R$, sauf en la valeur où il s’annule.',
  corr5_2: 'De plus $a=£a<0$, le trinôme est <b>strictement négatif</b> sur $\\R$, sauf en la valeur où il s’annule.',
  corr6_1: 'De plus $a=£a>0$, le trinôme est <b>strictement positif</b> sur $\\R$.',
  corr6_2: 'De plus $a=£a<0$, le trinôme est <b>strictement négatif</b> sur $\\R$.',
  corr7: 'L’ensemble des solutions de l’inéquation est donc $£s$.'

}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function decoupageCupCap (rep) {
    // cette fonction renvoit un tableau contenant les différentes parties séparées par \\cup ou par \\cap
    // ces différentes parties sont en principe des intervalles
    const cupCap = /\\cup|\\cap/g // new RegExp('\\\\cup|\\\\cap', 'g')
    return rep.split(cupCap)
  }
  function bonIntervalle (rep) {
    // rep est du texte (réponse de l’élève)
    // On teste si rep est bien un intervalle ou une réunon d’intervalle'
    // on vérifie aussi si l’élève ne met pas une virgule à la place du point-virgule quand il écrit un intervalle'
    let pbCrochet = false
    let pbPointVirgule = false
    const intervalleTab = decoupageCupCap(rep)
    for (let k = 0; k < intervalleTab.length; k++) {
      // pour chacun de ces éléments, on vérifie que c’est bien un intervalle'
      if (((intervalleTab[k].charAt(0) !== '[') && (intervalleTab[k].charAt(0) !== ']')) || ((intervalleTab[k].charAt(intervalleTab[k].length - 1) !== '[') && (intervalleTab[k].charAt(intervalleTab[k].length - 1) !== ']'))) {
        pbCrochet = true
      } else {
        if ((intervalleTab[k].indexOf(';') <= 1) || (intervalleTab[k].indexOf(';') >= intervalleTab[k].length - 2)) {
          // il n’y a pas de point-virgule bien placé, donc controlons s’il n’a pas mis une virgule à la place'
          if ((intervalleTab[k].indexOf(',') > 1) && (intervalleTab[k].indexOf(',') < intervalleTab[k].length - 2)) {
            // c’est donc qu’il met une virgule à la place d’un point-virgule'
            pbPointVirgule = true
          }
        }
      }
    }
    return { non_intervalle: pbCrochet, separateur_virgule: pbPointVirgule }
  }
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.petit.correction.color } })
    j3pDetruit(stor.laPalette)
    // le div le_nom+"explications" contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    if ((stor.objet_polynome.poly_gauche !== '0') && (stor.objet_polynome.poly_droite !== '0')) {
      const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
      j3pAffiche(zoneExpli1, '', textes.corr1,
        { i: stor.objet_polynome.trinome + stor.signe_ineq + '0' })
    }
    // dans explication1, on a l’explication correspondant à la première question
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli2, '', textes.corr2, {
      a: stor.objet_polynome.tab_coefsDif[2],
      b: stor.objet_polynome.tab_coefsDif[1],
      c: stor.objet_polynome.tab_coefsDif[0],
      d: stor.objet_polynome.tab_coefsDif[3]
    })
    const zoneExpli3 = j3pAddElt(zoneExpli, 'div')
    const zoneExpli4 = j3pAddElt(zoneExpli, 'div')
    if (stor.objet_polynome.tabRacines.length === 0) {
      j3pAffiche(zoneExpli3, '', textes.corr3_3)
      if (j3pCalculValeur(stor.objet_polynome.tab_coefsDif[2]) > 0) {
        j3pAffiche(zoneExpli4, '', textes.corr6_1, {
          a: stor.objet_polynome.tab_coefsDif[2]
        })
      } else {
        j3pAffiche(zoneExpli4, '', textes.corr6_2, {
          a: stor.objet_polynome.tab_coefsDif[2]
        })
      }
    } else if (stor.objet_polynome.tabRacines.length === 1) {
      j3pAffiche(zoneExpli3, '', textes.corr3_2, {
        x: stor.objet_polynome.tabRacines[0]
      })
      if (j3pCalculValeur(stor.objet_polynome.tab_coefsDif[2]) > 0) {
        j3pAffiche(zoneExpli4, '', textes.corr5_1, {
          a: stor.objet_polynome.tab_coefsDif[2]
        })
      } else {
        j3pAffiche(zoneExpli4, '', textes.corr5_2, {
          a: stor.objet_polynome.tab_coefsDif[2]
        })
      }
    } else {
      j3pAffiche(zoneExpli3, '', textes.corr3_1, {
        x: stor.objet_polynome.tabRacines[0],
        y: stor.objet_polynome.tabRacines[1]
      })
      if (j3pCalculValeur(stor.objet_polynome.tab_coefsDif[2]) > 0) {
        j3pAffiche(zoneExpli4, '', textes.corr4_1, {
          a: stor.objet_polynome.tab_coefsDif[2]
        })
      } else {
        j3pAffiche(zoneExpli4, '', textes.corr4_2, {
          a: stor.objet_polynome.tab_coefsDif[2]
        })
      }
    }
    const zoneExpli5 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli5, '', textes.corr7, {
      s: stor.ensemble_solution
    })
  }
  function ecrireFonction (fctTxt) {
    let fctLatex = fctTxt
    const regFrac1 = /\(-?[0-9]+\)\/\([0-9]+\)/g // new RegExp('\\(\\-?[0-9]{1,}\\)/\\([0-9]{1,}\\)', 'ig')
    const regFrac2 = /\(-?[0-9]+\/[0-9]+\)/g // new RegExp('\\(\\-?[0-9]{1,}/[0-9]{1,}\\)', 'ig')
    const regFrac3 = /[0-9]+\/[0-9]+/g // new RegExp('[0-9]{1,}/[0-9]{1,}', 'ig')
    let num, den
    while (regFrac1.test(fctLatex)) {
      // on trouve (..)/(..)
      const tabFrac1 = fctLatex.match(regFrac1)
      for (let i = 0; i < tabFrac1.length; i++) {
        const tabSeparateur = tabFrac1[i].split('/')
        num = tabSeparateur[0].substring(1, tabSeparateur[0].length - 1)
        den = tabSeparateur[1].substring(1, tabSeparateur[1].length - 1)
        fctLatex = fctLatex.replace(tabFrac1[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    while (regFrac2.test(fctLatex)) {
      // on trouve (../..)
      const tabFrac2 = fctLatex.match(regFrac2)
      for (let i = 0; i < tabFrac2.length; i++) {
        const tabSeparateur = tabFrac2[i].split('/')
        num = tabSeparateur[0].substring(1, tabSeparateur[0].length)
        den = tabSeparateur[1].substring(0, tabSeparateur[1].length - 1)
        fctLatex = fctLatex.replace(tabFrac2[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    while (regFrac3.test(fctLatex)) {
      // on trouve ../..
      const tabFrac3 = fctLatex.match(regFrac3)
      for (let i = 0; i < tabFrac3.length; i++) {
        const tabSeparateur = tabFrac3[i].split('/')
        num = tabSeparateur[0].substring(0, tabSeparateur[0].length)
        den = tabSeparateur[1].substring(0, tabSeparateur[1].length)
        fctLatex = fctLatex.replace(tabFrac3[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    // on enlève signe * devant x
    while (fctLatex.includes('*x')) {
      fctLatex = fctLatex.replace('*x', 'x')
    }
    if (fctLatex.includes('x²')) {
      fctLatex = fctLatex.replace('x²', 'x^2')
    }
    // fonction bx^2+cx+d avec coefs au format latex
    const posSigne = [0]
    for (let i = 1; i < fctLatex.length; i++) {
      if ((fctLatex[i] === '+') || (fctLatex[i] === '-')) posSigne.push(i)
    }
    posSigne.push(fctLatex.length)
    // je découpe alors l’expression à l’aide de ces signes, cela me donnera les monômes
    const tabMonome = []
    for (let i = 1; i < posSigne.length; i++) tabMonome.push(fctLatex.substring(posSigne[i - 1], posSigne[i]))
    const tabCoefPolynome = ['0', '0', '0']
    // tabCoefPolynome[0] est le coef constant, tabCoefPolynome[1], celui devant x, ...
    // je fais un correctif si j’ai un monome avec un coef de la forme \\frac{-...}{...}
    let posI = 0
    while (posI < tabMonome.length) {
      if ((tabMonome[posI] === '\\frac{') || (tabMonome[posI] === '+\\frac{') || (tabMonome[posI] === '-\\frac{')) {
        tabMonome[posI + 1] = tabMonome[posI] + tabMonome[posI + 1]
        tabMonome.splice(posI, 1)
      } else {
        posI++
      }
    }
    for (let i = 0; i < tabMonome.length; i++) {
      // tabMonome.length<=3 c’est le nombre de monomes présents
      if ((tabMonome[i].includes('x^{2}')) || (tabMonome[i].includes('x^2'))) {
        // c’est le monome ax^2
        if (tabMonome[i].charAt(0) === '+') {
          tabMonome[i] = tabMonome[i].substring(1)
        }
        if (tabMonome[i].includes('x^{2}')) {
          tabCoefPolynome[2] = tabMonome[i].substring(0, tabMonome[i].length - 5)
        } else {
          tabCoefPolynome[2] = tabMonome[i].substring(0, tabMonome[i].length - 3)
        }
        if (tabCoefPolynome[2] === '') {
          tabCoefPolynome[2] = '1'
        } else if (tabCoefPolynome[2] === '-') {
          tabCoefPolynome[2] = '-1'
        }
      } else if (tabMonome[i].includes('x')) {
        // c’est le monome ax
        if (tabMonome[i].charAt(0) === '+') {
          tabMonome[i] = tabMonome[i].substring(1)
        }
        tabCoefPolynome[1] = tabMonome[i].substring(0, tabMonome[i].length - 1)
        if (tabCoefPolynome[1] === '') {
          tabCoefPolynome[1] = '1'
        } else if (tabCoefPolynome[1] === '-') {
          tabCoefPolynome[1] = '-1'
        }
      } else {
        // c’est le monome a
        if (tabMonome[i].charAt(0) === '+') {
          tabCoefPolynome[0] = tabMonome[i].substring(1)
        } else {
          tabCoefPolynome[0] = tabMonome[i]
        }
      }
    }
    return { fctLatex, tab_coef: tabCoefPolynome }
  }
  function rechercheRacine (tab1, tab2) {
    // tab1 et tab2 sont 2 tableaux de 3 éléments représentant les 2 fonctions de chaque côté de l’égalité
    // tab1[0] est l’expression de la fonction polynomiale au format latex
    // tab1[1] est le tableau des coefficients du polynôme (lorsqu’ils existent)
    // tab1[2] est le type de fonction ("trinome", "affine", "constante"). Cela donne la forme de la fonction générée de manière aléatoire.
    // idem pour tab2[0] et tab2[1]
    // cette fonction génère les polynômes manquant et calcule les solutions éventuelles de l’équation tab1[0]=tab2[0]
    // dans le cas où les deux polynômes ne sont pas donnés,
    let poly1, poly2// pour accueillir le trinôme égal à la différence des deux polynômes
    let a, b, c, d, aDif, bDif, cDif
    const fctTxt1 = tab1[0]
    const fctTxt2 = tab2[0]
    let a1, b1, c1, a2, b2, c2
    if ((fctTxt1 === '') || (fctTxt2 === '')) {
      // au moins une des deux fonctions est aléatoire.
      // Dans ce cas, on écrit le trinôme différence sous la forme (ax+b)(cx+d) s’il admet 2 racines
      // on l’écrit (ax+b)^2 s’il admet 2 racines et (ax+b)^2+c (c>0) s’il n’admet pas de racine
      let nbSol
      if (stor.nbRacinesImposees === 'alea') {
        nbSol = j3pRandomTab([0, 1, 2], [0.25, 0.125, 0.625])
      } else {
        nbSol = Number(stor.nbRacinesImposees)
      }
      // la proba d’avoir 0 racine est 1/4, celle d’en avoir 1 est 1/8 et celle d’en avoir 2 est 5/8
      if (nbSol === 0) {
        // polynôme (ax+b)^2+c=a^2x^2+2abx+b^2+c ou son opposé
        do {
          do {
            a = j3pGetRandomInt(-3, 3)
          } while (Math.abs(a) < Math.pow(10, -12))
          do {
            b = j3pGetRandomInt(-5, 5)
          } while (Math.abs(b) < Math.pow(10, -12))
          c = j3pGetRandomInt(1, 7)
          aDif = Math.pow(a, 2)
          bDif = 2 * a * b
          cDif = Math.pow(b, 2) + c
          if (j3pGetRandomBool()) {
            aDif = -aDif
            bDif = -bDif
            cDif = -cDif
          }
        } while (Math.abs(bDif) < Math.pow(10, -12))
      } else if (nbSol === 1) {
        // polynôme (ax+b)^2=a^2x^2+2abx+b^2 ou son opposé
        do {
          a = j3pGetRandomInt(-3, 3)
        } while (Math.abs(a) < Math.pow(10, -12))
        do {
          b = j3pGetRandomInt(-5, 5)
        } while (Math.abs(b) < Math.pow(10, -12))
        aDif = Math.pow(a, 2)
        bDif = 2 * a * b
        cDif = Math.pow(b, 2)
        if (j3pGetRandomBool()) {
          aDif = -aDif
          bDif = -bDif
          cDif = -cDif
        }
      } else {
        const choixDelta = j3pGetRandomInt(0, 2)// dans 1 cas sur 3, delta ne sera pas un carré parfait (seulement lorsqu’on ne demande pas l’intersection des deux courbes)
        if ((choixDelta === 0) && (!ds.intersection_courbes)) {
          let deltaVal
          do {
            aDif = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
            bDif = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 9)
            cDif = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 9)
            deltaVal = Math.pow(bDif, 2) - 4 * aDif * cDif
          } while (Math.abs(Math.sqrt(deltaVal) - Math.round(Math.sqrt(deltaVal))) < Math.pow(10, -12))
        } else {
          // (ax+b)(cx+d)=acx^2+(ad+bc)x+bd
          do {
            do {
              a = j3pGetRandomInt(-3, 3)
            } while (Math.abs(a) < Math.pow(10, -12))
            do {
              b = j3pGetRandomInt(-5, 5)
            } while (Math.abs(b) < Math.pow(10, -12))
            do {
              c = j3pGetRandomInt(-3, 3)
            } while (Math.abs(c) < Math.pow(10, -12))
            do {
              d = j3pGetRandomInt(-5, 5)
            } while (Math.abs(d) < Math.pow(10, -12))
          } while (Math.abs(Math.abs(-b / a) - Math.abs(-d / c)) < Math.pow(10, -12))
          aDif = a * c
          bDif = a * d + b * c
          cDif = b * d
        }
      }
    } else {
      a1 = (tab1[1][2] === '') ? '0' : tab1[1][2]
      b1 = (tab1[1][1] === '') ? '0' : tab1[1][1]
      c1 = (tab1[1][0] === '') ? '0' : tab1[1][0]
      a2 = (tab2[1][2] === '') ? '0' : tab2[1][2]
      b2 = (tab2[1][1] === '') ? '0' : tab2[1][1]
      c2 = (tab2[1][0] === '') ? '0' : tab2[1][0]
      aDif = j3pGetLatexSomme(a1, j3pGetLatexProduit('-1', a2))
      bDif = j3pGetLatexSomme(b1, j3pGetLatexProduit('-1', b2))
      cDif = j3pGetLatexSomme(c1, j3pGetLatexProduit('-1', c2))
    }
    const polyDif = j3pGetLatexMonome(1, 2, String(aDif)) + j3pGetLatexMonome(2, 1, String(bDif)) + j3pGetLatexMonome(2, 0, String(cDif))
    me.logIfDebug('polyDif:' + polyDif)
    if (tab2[2] === '') {
      // on choisit de manière aléatoire entre fonction affine et trinôme
      tab2[2] = (j3pGetRandomBool()) ? 'trinome' : 'affine'
    }
    if (fctTxt1 === '') {
      if (fctTxt2 === '') {
        if ((tab1[2] === '') || (tab1[2] === 'trinome')) {
          if ((tab2[2] === '') || (tab2[2] === 'trinome')) {
            // on a 2 trinômes
            do {
              a1 = j3pGetRandomInt(-4, 4)
            } while ((Math.abs(a1) < Math.pow(10, -12)) || (Math.abs(a1 - aDif) < Math.pow(10, -12)))
            b1 = j3pGetRandomInt(-5, 5)
            c1 = j3pGetRandomInt(-6, 6)
          } else if (tab2[2] === 'affine') {
            a1 = aDif
            do {
              b1 = j3pGetRandomInt(-6, 6)
            } while ((Math.abs(b1) < Math.pow(10, -12)) || (Math.abs(b1 - bDif) < Math.pow(10, -12)))
            c1 = j3pGetRandomInt(-6, 6)
          } else {
            // tab2[2] est constante
            a1 = aDif
            b1 = bDif
            do {
              c1 = j3pGetRandomInt(-8, 8)
            } while ((Math.abs(c1) < Math.pow(10, -12)) || (Math.abs(c1 - cDif) < Math.pow(10, -12)))
          }
        } else if (tab1[2] === 'affine') {
          // le 2ème membre de l’égalité est alors nécessairement un trinôme
          a1 = 0
          do {
            b1 = j3pGetRandomInt(-6, 6)
          } while ((Math.abs(b1) < Math.pow(10, -12)) || (Math.abs(b1 - bDif) < Math.pow(10, -12)))
          c1 = j3pGetRandomInt(-6, 6)
        } else {
          // le 1er membre de l’égalité est une constante et le 2ème membre est nécessairement un trinôme
          a1 = 0
          b1 = 0
          do {
            c1 = j3pGetRandomInt(-6, 6)
          } while ((Math.abs(c1) < Math.pow(10, -12)) || (Math.abs(c1 - cDif) < Math.pow(10, -12)))
        }
      } else {
        // la deuxième fonction est imposée, les coefs sont dans le tableau tab2[1]
        a2 = tab2[1][2]
        b2 = tab2[1][1]
        c2 = tab2[1][0]
        a1 = j3pGetLatexSomme(aDif, a2)
        b1 = j3pGetLatexSomme(bDif, b2)
        c1 = j3pGetLatexSomme(cDif, c2)
      }
    } else {
      if (fctTxt2 === '') {
        a1 = tab1[1][2]
        b1 = tab1[1][1]
        c1 = tab1[1][0]
      }
    }
    if ((fctTxt1 === '') || (fctTxt2 === '')) {
      a2 = j3pGetLatexSomme(a1, j3pGetLatexProduit('-1', aDif))
      b2 = j3pGetLatexSomme(b1, j3pGetLatexProduit('-1', bDif))
      c2 = j3pGetLatexSomme(c1, j3pGetLatexProduit('-1', cDif))
      if (Math.abs(j3pCalculValeur(a1)) < Math.pow(10, -12)) {
        if (Math.abs(j3pCalculValeur(b1)) < Math.pow(10, -12)) {
          if (Math.abs(j3pCalculValeur(c1)) < Math.pow(10, -12)) {
            poly1 = '0'
          } else {
            poly1 = j3pGetLatexMonome(1, 0, c1)
          }
        } else {
          poly1 = j3pGetLatexMonome(1, 1, b1) + j3pGetLatexMonome(2, 0, c1)
        }
      } else {
        poly1 = j3pGetLatexMonome(1, 2, a1) + j3pGetLatexMonome(2, 1, b1) + j3pGetLatexMonome(2, 0, c1)
      }
      if (Math.abs(j3pCalculValeur(a2)) < Math.pow(10, -12)) {
        if (Math.abs(j3pCalculValeur(b2)) < Math.pow(10, -12)) {
          if (Math.abs(j3pCalculValeur(c2)) < Math.pow(10, -12)) {
            poly2 = '0'
          } else {
            poly2 = j3pGetLatexMonome(1, 0, c2)
          }
        } else {
          poly2 = j3pGetLatexMonome(1, 1, b2) + j3pGetLatexMonome(2, 0, c2)
        }
      } else {
        poly2 = j3pGetLatexMonome(1, 2, a2) + j3pGetLatexMonome(2, 1, b2) + j3pGetLatexMonome(2, 0, c2)
      }
    } else {
      poly1 = tab1[0]
      poly2 = tab2[0]
      a1 = tab1[1][2]
      b1 = tab1[1][1]
      c1 = tab1[1][0]
      a2 = tab2[1][2]
      b2 = tab2[1][1]
      c2 = tab2[1][0]
    }
    const deltaTxt = j3pGetLatexSomme(j3pGetLatexProduit(bDif, bDif), j3pGetLatexProduit('-4', j3pGetLatexProduit(aDif, cDif)))
    const delta = j3pCalculValeur(deltaTxt)
    const tabSol = []
    const tabImage = []// utile pour les coordonnées du point d’intersection de deux courbes
    const tabValSol = []
    const tabValImage = []
    if (Math.abs(delta) < Math.pow(10, -12)) {
      tabSol.push(j3pGetLatexQuotient(j3pGetLatexProduit('-1', bDif), j3pGetLatexProduit('2', aDif)))
      tabValSol.push(j3pCalculValeur(tabSol[0]))
      tabImage.push(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(a1, j3pGetLatexProduit(tabSol[0], tabSol[0])), j3pGetLatexProduit(b1, tabSol[0])), c1))
      tabValImage.push(j3pCalculValeur(tabImage[0]))
    } else if (delta > 0) {
      tabSol.push(j3pSimplificationRacineTrinome(bDif, '-', deltaTxt, aDif))
      tabSol.push(j3pSimplificationRacineTrinome(bDif, '+', deltaTxt, aDif))
      tabValSol.push(j3pCalculValeur(tabSol[0]), j3pCalculValeur(tabSol[1]))
      if (tabSol[0].indexOf('sqrt') === -1) {
        // les racines sont simples (sans radical)
        tabImage.push(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(a1, j3pGetLatexProduit(tabSol[0], tabSol[0])), j3pGetLatexProduit(b1, tabSol[0])), c1))
        tabImage.push(j3pGetLatexSomme(j3pGetLatexSomme(j3pGetLatexProduit(a1, j3pGetLatexProduit(tabSol[1], tabSol[1])), j3pGetLatexProduit(b1, tabSol[1])), c1))
        tabValImage.push(j3pCalculValeur(tabImage[0]), j3pCalculValeur(tabImage[1]))
      } else {
        if (tab1[2] === 'constante') {
          tabImage.push(c1, c1)
          tabValImage.push(j3pCalculValeur(c2), j3pCalculValeur(c1))
        } else if (tab2[2] === 'constante') {
          tabImage.push(c2, c2)
          tabValImage.push(j3pCalculValeur(c2), j3pCalculValeur(c2))
        } else {
          // là c’est plus compliqué
          // l’image de (-b+/-sqrt(delta))/(2a) par le trinôme a1x^2+b1x+c1 est (-(-1*((a_1*b^2+a1*delta)/(2a)+2ac_1-b_1b)+/-sqrt(delta*(a_1b/a-b_1)^2))/2a
          const a1bcarrePlusa1deltaSur2a = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexProduit(a1, j3pGetLatexProduit(bDif, bDif)), j3pGetLatexProduit(a1, deltaTxt)), j3pGetLatexProduit('2', aDif))
          const deuxac1Moinsb1b = j3pGetLatexSomme(j3pGetLatexProduit(j3pGetLatexProduit(aDif, c1), '2'), j3pGetLatexProduit('-1', j3pGetLatexProduit(b1, bDif)))
          const newB = j3pGetLatexProduit('-1', j3pGetLatexSomme(a1bcarrePlusa1deltaSur2a, deuxac1Moinsb1b))
          const coefDevantSqrtTxt = j3pGetLatexSomme(j3pGetLatexQuotient(j3pGetLatexProduit(a1, bDif), aDif), j3pGetLatexProduit('-1', b1))
          const newDelta = j3pGetLatexProduit(deltaTxt, j3pGetLatexProduit(coefDevantSqrtTxt, coefDevantSqrtTxt))
          const image1Txt = j3pSimplificationRacineTrinome(newB, '-', newDelta, aDif)
          const image2Txt = j3pSimplificationRacineTrinome(newB, '+', newDelta, aDif)
          // maintenant il faut mettre les images dans le bon ordre
          const valA = j3pCalculValeur(a1)
          const valADif = j3pCalculValeur(aDif)
          const valB = j3pCalculValeur(b1)
          const valBDif = j3pCalculValeur(bDif)
          const valC = j3pCalculValeur(c1)
          const valSol1 = (-valBDif - Math.sqrt(delta)) / (2 * valADif)
          const valSol2 = (-valBDif + Math.sqrt(delta)) / (2 * valADif)
          // tabValSol.push(valSol1,valSol2);
          const valImageSol1 = valA * Math.pow(valSol1, 2) + valB * valSol1 + valC
          const valImageSol2 = valA * Math.pow(valSol2, 2) + valB * valSol2 + valC
          tabValImage.push(valImageSol1, valImageSol2)
          const valDelta = j3pCalculValeur(newDelta)
          const valCalc1 = (-j3pCalculValeur(newB) - Math.sqrt(valDelta)) / (2 * valADif)
          // console.log("valCalc1:"+valCalc1+"  valCalc2:"+valCalc2+"   valImageSol1:"+valImageSol1+"   valImageSol2:"+valImageSol2)

          if (Math.abs(valCalc1 - valImageSol1) < Math.pow(10, -12)) {
            tabImage.push(image1Txt, image2Txt)
          } else {
            tabImage.push(image2Txt, image1Txt)
          }
        }
      }
    }
    me.logIfDebug('tabRacines:' + tabSol + '   tabImage:' + tabImage + '   tabValSol:' + tabValSol + '   tabValImage:' + tabValImage)
    return { poly_gauche: poly1, poly_droite: poly2, trinome: polyDif, tabRacines: tabSol, les_images: tabImage, tab_coefs1: [c1, b1, a1], tab_coefs2: [c2, b2, a2], tab_coefsDif: [cDif, bDif, aDif, deltaTxt], valeur_racines: tabValSol, valeur_images: tabValImage }
  }

  function tableauSansDoublon (tab) {
    // cette fonction renvoie un tableau pour lequel aucune valeur n’apparait 2 fois
    let newTab = []
    if (tab.length <= 1) {
      newTab = newTab.concat(tab)
    } else {
      newTab.push(tab[0])
      for (let i = 1; i < tab.length; i++) {
        if (newTab.indexOf(tab[i]) === -1) {
          newTab.push(tab[i])
        }
      }
    }
    return newTab
  }

  function enonceMain () {
    const maFonction1 = ds.fonctions1[me.questionCourante - 1]
    let maFonction2 = ds.fonctions2[me.questionCourante - 1]
    const choixSigne = ds.signe_ineq[me.questionCourante - 1]
    if (choixSigne === '<') {
      stor.signe_ineq = '<'
    } else if (choixSigne === '>') {
      stor.signe_ineq = '>'
    } else if (choixSigne === '<=') {
      stor.signe_ineq = '\\leq'
    } else if (choixSigne === '>=') {
      stor.signe_ineq = '\\geq'
    } else {
      // c’est un signe aléatoire
      stor.signe_ineq = j3pRandomTab(['<', '>', '\\leq', '\\geq'], [0.25, 0.25, 0.25, 0.25])
    }
    let fctTxt1 = ''
    let coefs1 = []
    let fctTxt2 = ''
    let coefs2 = []
    if ((maFonction1 !== undefined) && (maFonction1 !== '') && (maFonction1 !== 'trinome') && (maFonction1 !== 'affine') && (maFonction1 !== 'constante')) {
      // la fonction 1 est imposée
      const maFonctionDonnee1 = ecrireFonction(maFonction1)
      fctTxt1 = maFonctionDonnee1.fctLatex
      coefs1 = maFonctionDonnee1.tab_coef
    }
    if (ds.inequation_simple) {
      maFonction2 = '0'
    }
    if ((maFonction2 !== undefined) && (maFonction2 !== '') && (maFonction2 !== 'trinome') && (maFonction2 !== 'affine') && (maFonction2 !== 'constante')) {
      // la fonction 2 est imposée
      const maFonctionDonnee2 = ecrireFonction(maFonction2)
      fctTxt2 = maFonctionDonnee2.fctLatex
      coefs2 = maFonctionDonnee2.tab_coef
    }
    // ce qui suit n’est valable que si le trinôme n’est pas imposé
    const nbRacinesImposees = ds.nbracines[me.questionCourante - 1]
    if ((nbRacinesImposees === 0) || (nbRacinesImposees === '0') || (nbRacinesImposees === 1) || (nbRacinesImposees === '1') || (nbRacinesImposees === 2) || (nbRacinesImposees === '2')) {
      stor.nbRacinesImposees = String(nbRacinesImposees)
    } else {
      stor.nbRacinesImposees = 'alea'
    }
    stor.objet_polynome = rechercheRacine([fctTxt1, coefs1, maFonction1], [fctTxt2, coefs2, maFonction2])
    // objet_polynôme gère les polynômes présents dans l’égalité
    // il renvoie les deux polynômes (aléatoires ou fixés)
    // on a aussi la différence qui ramène l’égalité à ax^2+bx+c=0
    // ensuite, on récupère les racines de ce trinôme
    // et pour l’intersection des 2 courbes, j’ai aussi besoin des images de chaque racine par un polynôme de base
    // {poly_gauche:poly1,poly_droite:poly2,trinome:polyDif,tabRacines:tabSol,les_images:tabImage,tab_coefs1:[c1,b1,a1],tab_coefs2:[c2,b2,a2],tab_coefsDif:[cDif,bDif,aDif,deltaTxt],valeur_racines:tabValSol,valeur_images:tabValImage};
    // création du conteneur dans lequel se trouveront toutes les conaignes
    // dans le cas où le trinôme n’admet qu’une seule racine et qu’on n’impose pas le signe de l’inégalité, on évite le cas où l’ensemble des solutions est un singleton
    const nbRacine = stor.objet_polynome.tabRacines.length
    const valeurCoefA = j3pCalculValeur(stor.objet_polynome.tab_coefsDif[2])
    if (nbRacine === 1) {
      if ((ds.signe_ineq[me.questionCourante - 1] !== '<') && (ds.signe_ineq[me.questionCourante - 1] !== '>') && (ds.signe_ineq[me.questionCourante - 1] !== '<=') && (ds.signe_ineq[me.questionCourante - 1] !== '>=')) {
        // on n’a pas imposé le signe de l’inégalité
        if ((valeurCoefA > 0) && (stor.signe_ineq === '\\leq')) {
          // on change le signe de l’inégalité
          stor.signe_ineq = j3pRandomTab(['<', '>', '\\geq'], [0.333, 0.333, 0.334])
        } else if ((valeurCoefA < 0) && (stor.signe_ineq === '\\geq')) {
          // on change le signe de l’inégalité
          stor.signe_ineq = j3pRandomTab(['<', '>', '\\leq'], [0.333, 0.333, 0.334])
        }
      }
    }
    me.logIfDebug('stor.objet_polynome.poly_gauche:' + stor.objet_polynome.poly_gauche + '   stor.objet_polynome.poly_droite:' + stor.objet_polynome.poly_droite + '   stor.signe_ineq:' + stor.signe_ineq)
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    stor.mon_inequation = stor.objet_polynome.poly_gauche + stor.signe_ineq + ' ' + stor.objet_polynome.poly_droite
    const elt = j3pAffiche(zoneCons1, '', textes.consigne1,
      {
        i: stor.mon_inequation,
        inputmq1: { texte: '' }
      })
    stor.zoneInput = elt.inputmqList[0]
    stor.zoneInput.typeReponse = ['texte']
    mqRestriction(stor.zoneInput, '\\d,.;+][()/-{}', { commandes: ['fraction', 'racine', 'inf', 'inter', 'union', 'vide', 'R', 'setminus'] })
    stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
    // création de la palette
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['fraction', 'racine', 'inf', 'inter', 'union', 'vide', 'R', '[', ']', '{', '}', 'setminus'] })

    stor.tabIntervalle_rep = []
    stor.autre_reponse_possible = []// pour certains cas particuliers, on peut accepter des réponses sous une autre forme que des réunions d’intervalles : \\R ou \\R\\setminus\\{...\\}

    if (nbRacine === 0) {
      // pas de racine réelle. On regarde le signe du coef devant x^2
      if (valeurCoefA > 0) {
        if ((stor.signe_ineq === '>') || (stor.signe_ineq === '\\geq')) {
          stor.tabIntervalle_rep.push(']-\\infty;+\\infty[')
          stor.autre_reponse_possible.push('\\mathbb{R}')
          stor.ensemble_solution = '\\R'
        } else {
          stor.tabIntervalle_rep.push('\\varnothing')
          stor.ensemble_solution = '\\emptyset'
        }
      } else {
        if ((stor.signe_ineq === '<') || (stor.signe_ineq === '\\leq')) {
          stor.tabIntervalle_rep.push(']-\\infty;+\\infty[')
          stor.autre_reponse_possible.push('\\mathbb{R}')
          stor.ensemble_solution = '\\R'
        } else {
          stor.tabIntervalle_rep.push('\\varnothing')
          stor.ensemble_solution = '\\emptyset'
        }
      }
    } else if (nbRacine === 1) {
      // une seule racine réelle. On regarde le signe du coef devant x^2
      stor.objet_polynome.valeur_racines[0] = j3pArrondi(stor.objet_polynome.valeur_racines[0], 10)
      if (valeurCoefA > 0) {
        if (stor.signe_ineq === '>') {
          stor.tabIntervalle_rep.push(']-\\infty;' + stor.objet_polynome.valeur_racines[0] + '[', ']' + stor.objet_polynome.valeur_racines[0] + ';+\\infty[')
          stor.autre_reponse_possible.push('\\mathbb{R}\\setminus{' + stor.objet_polynome.valeur_racines[0] + '}', '\\mathbb{R}-{' + stor.objet_polynome.valeur_racines[0] + '}', '\\mathbb{R}\\backslash{' + stor.objet_polynome.valeur_racines[0] + '}', '\\mathbb{R}\\{' + stor.objet_polynome.valeur_racines[0] + '}')
          stor.ensemble_solution = '\\R\\setminus\\opencurlybrace' + stor.objet_polynome.tabRacines[0] + '\\closecurlybrace'
        } else if (stor.signe_ineq === '\\geq') {
          stor.tabIntervalle_rep.push(']-\\infty;+\\infty[')
          stor.ensemble_solution = '\\R'
          stor.autre_reponse_possible.push('\\mathbb{R}')
        } else if (stor.signe_ineq === '\\leq') {
          stor.tabIntervalle_rep.push('{' + stor.objet_polynome.valeur_racines[0] + '}')
          stor.ensemble_solution = '\\opencurlybrace' + stor.objet_polynome.tabRacines[0] + '\\closecurlybrace'
        } else if (stor.signe_ineq === '<') {
          stor.tabIntervalle_rep.push('\\varnothing')
          stor.ensemble_solution = '\\emptyset'
        }
      } else {
        if (stor.signe_ineq === '<') {
          stor.tabIntervalle_rep.push(']-\\infty;' + stor.objet_polynome.valeur_racines[0] + '[', ']' + stor.objet_polynome.valeur_racines[0] + ';+\\infty[')
          stor.autre_reponse_possible.push('\\mathbb{R}\\setminus{' + stor.objet_polynome.valeur_racines[0] + '}', '\\mathbb{R}-{' + stor.objet_polynome.valeur_racines[0] + '}', '\\mathbb{R}\\backslash{' + stor.objet_polynome.valeur_racines[0] + '}', '\\mathbb{R}\\{' + stor.objet_polynome.valeur_racines[0] + '}')
          stor.ensemble_solution = '\\R\\setminus\\opencurlybrace' + stor.objet_polynome.tabRacines[0] + '\\closecurlybrace'
        } else if (stor.signe_ineq === '\\leq') {
          stor.tabIntervalle_rep.push(']-\\infty;+\\infty[')
          stor.ensemble_solution = '\\R'
          stor.autre_reponse_possible.push('\\mathbb{R}')
        } else if (stor.signe_ineq === '\\geq') {
          stor.tabIntervalle_rep.push('{' + stor.objet_polynome.valeur_racines[0] + '}')
          stor.ensemble_solution = '\\opencurlybrace' + stor.objet_polynome.tabRacines[0] + '\\closecurlybrace'
        } else if (stor.signe_ineq === '>') {
          stor.tabIntervalle_rep.push('\\varnothing')
          stor.ensemble_solution = '\\emptyset'
        }
      }
    } else {
      // deux racines réelles. On regarde le signe du coef devant x^2
      stor.objet_polynome.valeur_racines[0] = j3pArrondi(stor.objet_polynome.valeur_racines[0], 10)
      stor.objet_polynome.valeur_racines[1] = j3pArrondi(stor.objet_polynome.valeur_racines[1], 10)
      if (valeurCoefA > 0) {
        if (stor.signe_ineq === '>') {
          if (stor.objet_polynome.valeur_racines[0] < stor.objet_polynome.valeur_racines[1]) {
            stor.tabIntervalle_rep.push(']-\\infty;' + stor.objet_polynome.valeur_racines[0] + '[', ']' + stor.objet_polynome.valeur_racines[1] + ';+\\infty[')
            stor.autre_reponse_possible.push('\\mathbb{R}\\setminus[' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + ']', '\\mathbb{R}-[' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + ']', '\\mathbb{R}\\backslash[' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + ']', '\\mathbb{R}\\[' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + ']')
            stor.ensemble_solution = ']-\\infty;' + stor.objet_polynome.tabRacines[0] + '[\\cup]' + stor.objet_polynome.tabRacines[1] + ';+\\infty['
          } else {
            stor.tabIntervalle_rep.push(']-\\infty;' + stor.objet_polynome.valeur_racines[1] + '[', ']' + stor.objet_polynome.valeur_racines[0] + ';+\\infty[')
            stor.autre_reponse_possible.push('\\mathbb{R}\\setminus[' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + ']', '\\mathbb{R}-[' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + ']', '\\mathbb{R}\\backslash[' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + ']', '\\mathbb{R}\\[' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + ']')
            stor.ensemble_solution = ']-\\infty;' + stor.objet_polynome.tabRacines[1] + '[\\cup]' + stor.objet_polynome.tabRacines[0] + ';+\\infty['
          }
        } else if (stor.signe_ineq === '\\geq') {
          if (stor.objet_polynome.valeur_racines[0] < stor.objet_polynome.valeur_racines[1]) {
            stor.tabIntervalle_rep.push(']-\\infty;' + stor.objet_polynome.valeur_racines[0] + ']', '[' + stor.objet_polynome.valeur_racines[1] + ';+\\infty[')
            stor.autre_reponse_possible.push('\\mathbb{R}\\setminus]' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + '[', '\\mathbb{R}-]' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + '[', '\\mathbb{R}\\backslash]' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + '[', '\\mathbb{R}\\]' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + '[')
            stor.ensemble_solution = ']-\\infty;' + stor.objet_polynome.tabRacines[0] + ']\\cup[' + stor.objet_polynome.tabRacines[1] + ';+\\infty['
          } else {
            stor.tabIntervalle_rep.push(']-\\infty;' + stor.objet_polynome.valeur_racines[1] + ']', '[' + stor.objet_polynome.valeur_racines[0] + ';+\\infty[')
            stor.autre_reponse_possible.push('\\mathbb{R}\\setminus]' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + '[', '\\mathbb{R}-]' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + '[', '\\mathbb{R}\\backslash]' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + '[', '\\mathbb{R}\\]' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + '[')
            stor.ensemble_solution = ']-\\infty;' + stor.objet_polynome.tabRacines[1] + ']\\cup[' + stor.objet_polynome.tabRacines[0] + ';+\\infty['
          }
        } else if (stor.signe_ineq === '<') {
          if (stor.objet_polynome.valeur_racines[0] < stor.objet_polynome.valeur_racines[1]) {
            stor.tabIntervalle_rep.push(']' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + '[')
            stor.ensemble_solution = ']' + stor.objet_polynome.tabRacines[0] + ';' + stor.objet_polynome.tabRacines[1] + '['
          } else {
            stor.tabIntervalle_rep.push(']' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + '[')
            stor.ensemble_solution = ']' + stor.objet_polynome.tabRacines[1] + ';' + stor.objet_polynome.tabRacines[0] + '['
          }
        } else if (stor.signe_ineq === '\\leq') {
          if (stor.objet_polynome.valeur_racines[0] < stor.objet_polynome.valeur_racines[1]) {
            stor.tabIntervalle_rep.push('[' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + ']')
            stor.ensemble_solution = '[' + stor.objet_polynome.tabRacines[0] + ';' + stor.objet_polynome.tabRacines[1] + ']'
          } else {
            stor.tabIntervalle_rep.push('[' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + ']')
            stor.ensemble_solution = '[' + stor.objet_polynome.tabRacines[1] + ';' + stor.objet_polynome.tabRacines[0] + ']'
          }
        }
      } else {
        if (stor.signe_ineq === '<') {
          if (stor.objet_polynome.valeur_racines[0] < stor.objet_polynome.valeur_racines[1]) {
            stor.tabIntervalle_rep.push(']-\\infty;' + stor.objet_polynome.valeur_racines[0] + '[', ']' + stor.objet_polynome.valeur_racines[1] + ';+\\infty[')
            stor.autre_reponse_possible.push('\\mathbb{R}\\setminus[' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + ']', '\\mathbb{R}-[' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + ']', '\\mathbb{R}\\backslash[' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + ']', '\\mathbb{R}\\[' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + ']')
            stor.ensemble_solution = ']-\\infty;' + stor.objet_polynome.tabRacines[0] + '[\\cup]' + stor.objet_polynome.tabRacines[1] + ';+\\infty['
          } else {
            stor.tabIntervalle_rep.push(']-\\infty;' + stor.objet_polynome.valeur_racines[1] + '[', ']' + stor.objet_polynome.valeur_racines[0] + ';+\\infty[')
            stor.autre_reponse_possible.push('\\mathbb{R}\\setminus[' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + ']', '\\mathbb{R}-[' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + ']', '\\mathbb{R}\\backslash[' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + ']', '\\mathbb{R}\\[' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + ']')
            stor.ensemble_solution = ']-\\infty;' + stor.objet_polynome.tabRacines[1] + '[\\cup]' + stor.objet_polynome.tabRacines[0] + ';+\\infty['
          }
        } else if (stor.signe_ineq === '\\leq') {
          if (stor.objet_polynome.valeur_racines[0] < stor.objet_polynome.valeur_racines[1]) {
            stor.tabIntervalle_rep.push(']-\\infty;' + stor.objet_polynome.valeur_racines[0] + ']', '[' + stor.objet_polynome.valeur_racines[1] + ';+\\infty[')
            stor.autre_reponse_possible.push('\\mathbb{R}\\setminus]' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + '[', '\\mathbb{R}-]' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + '[', '\\mathbb{R}\\backslash]' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + '[', '\\mathbb{R}\\]' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + '[')
            stor.ensemble_solution = ']-\\infty;' + stor.objet_polynome.tabRacines[0] + ']\\cup[' + stor.objet_polynome.tabRacines[1] + ';+\\infty['
          } else {
            stor.tabIntervalle_rep.push(']-\\infty;' + stor.objet_polynome.valeur_racines[1] + ']', '[' + stor.objet_polynome.valeur_racines[0] + ';+\\infty[')
            stor.autre_reponse_possible.push('\\mathbb{R}\\setminus]' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + '[', '\\mathbb{R}-]' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + '[', '\\mathbb{R}\\backslash]' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + '[', '\\mathbb{R}\\]' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + '[')
            stor.ensemble_solution = ']-\\infty;' + stor.objet_polynome.tabRacines[1] + ']\\cup[' + stor.objet_polynome.tabRacines[0] + ';+\\infty['
          }
        } else if (stor.signe_ineq === '>') {
          if (stor.objet_polynome.valeur_racines[0] < stor.objet_polynome.valeur_racines[1]) {
            stor.tabIntervalle_rep.push(']' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + '[')
            stor.ensemble_solution = ']' + stor.objet_polynome.tabRacines[0] + ';' + stor.objet_polynome.tabRacines[1] + '['
          } else {
            stor.tabIntervalle_rep.push(']' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + '[')
            stor.ensemble_solution = ']' + stor.objet_polynome.tabRacines[1] + ';' + stor.objet_polynome.tabRacines[0] + '['
          }
        } else if (stor.signe_ineq === '\\geq') {
          if (stor.objet_polynome.valeur_racines[0] < stor.objet_polynome.valeur_racines[1]) {
            stor.tabIntervalle_rep.push('[' + stor.objet_polynome.valeur_racines[0] + ';' + stor.objet_polynome.valeur_racines[1] + ']')
            stor.ensemble_solution = '[' + stor.objet_polynome.tabRacines[0] + ';' + stor.objet_polynome.tabRacines[1] + ']'
          } else {
            stor.tabIntervalle_rep.push('[' + stor.objet_polynome.valeur_racines[1] + ';' + stor.objet_polynome.valeur_racines[0] + ']')
            stor.ensemble_solution = '[' + stor.objet_polynome.tabRacines[1] + ';' + stor.objet_polynome.tabRacines[0] + ']'
          }
        }
      }
    }
    me.logIfDebug('stor.tabIntervalle_rep:' + stor.tabIntervalle_rep + '   stor.ensemble_solution:' + stor.ensemble_solution)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    j3pFocus(stor.zoneInput)
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesZonesSaisie })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si les 4 zones ont bien été complétées ou non
        // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
        // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
        // Si c’est la dernière tentative de répondre, on barre également les zones fausses
        let testIntervalle, valeursPresentes, interPresent, reponseIncomprise, typeComment
        if (reponse.aRepondu) {
        // il faut aussi que je teste la réponse
          let reponseEleve1 = j3pValeurde(stor.zoneInput)
          // on vire les espaces éventuels
          reponseEleve1 = reponseEleve1.replace(/\s/g, '')
          // ce qui est écrit n’est pas un intervalle ou une réunion d’intervalles ou le séparateur de l’intervalle n’est asp ; mais ,'
          testIntervalle = bonIntervalle(reponseEleve1)
          // testIntervalle a alors deux propriétés : non_intervalle et separateur_virgule
          if (reponseEleve1.includes(':')) {
            const partition = reponseEleve1.split(':')
            reponseEleve1 = ''
            for (let i = 0; i < partition.length; i++) reponseEleve1 += partition[i]
          }
          me.logIfDebug('testIntervalle:' + testIntervalle.non_intervalle + '   ' + testIntervalle.separateur_virgule + '     valeur avant modification virgule-point : ' + reponseEleve1)
          // je convertis les virgules de la réponse en points comme séparateur des décimaux
          reponseEleve1 = reponseEleve1.replace(/,/g, '.')
          me.logIfDebug('valeur : ' + reponseEleve1)
          let reponseEleveApprochee = reponseEleve1// dans cette variable, on retrouve la réponse de l’élève dans laquelle on remplace les nombre par leurs valeurs approchées à 10^{-10} près
          interPresent = false
          reponse.bonneReponse = true
          if (reponseEleveApprochee.includes('cap')) {
            interPresent = true
            reponse.bonneReponse = false
            while (reponseEleveApprochee.includes('cap')) {
              reponseEleveApprochee = reponseEleveApprochee.replace('cap', 'cup')
            }
          }
          const separateurReg = /;|\\cup/g // new RegExp(';|\\\\cup', 'ig')
          const tabRep = reponseEleveApprochee.split(separateurReg)

          // console.log("tabRep:"+tabRep);
          for (let i = 0; i < tabRep.length; i++) {
            if (tabRep[i].indexOf('\\setminus') > -1) {
              tabRep[i] = tabRep[i].replace('\\setminus', '')
            }
            if (tabRep[i].indexOf('\\backslash') > -1) {
              tabRep[i] = tabRep[i].replace('\\backslash', '')
            }
            if (tabRep[i].indexOf('\\mathbb{R}\\') > -1) {
              tabRep[i] = tabRep[i].replace('\\mathbb{R}\\', '')
            }
            if (tabRep[i].indexOf('\\mathbb{R}-') > -1) {
              tabRep[i] = tabRep[i].replace('\\mathbb{R}-', '')
            }
            if (tabRep[i].indexOf('\\mathbb{R}') > -1) {
              tabRep[i] = tabRep[i].replace('\\mathbb{R}', '')
            }
            if (tabRep[i].indexOf('infty') > -1) {
            // c’est qu’on doit avoir un intervalle (ou plusieurs) et dans cette borne on a l’infini, donc on ne lui fait rien
            } else if (tabRep[i] === '') {
            // l’ensemble des réels est présent, donc on ne fait rien
            } else if ((tabRep[i] === '\\emptyset') || (tabRep[i] === '\\varnothing')) {
            // l’ensemble des solutions est l’ensemble vide, donc on ne fait rien
            } else if ((tabRep[i].charAt(0) === ']') || (tabRep[i].charAt(0) === '[')) {
            // on a un intervalle et c’est la borne inf
              const borneInf = tabRep[i].substring(1)
              if (!isNaN(j3pCalculValeur(borneInf))) {
                reponseEleveApprochee = reponseEleveApprochee.replace(borneInf, j3pArrondi(j3pCalculValeur(borneInf), 10))
              }
            } else if ((tabRep[i].charAt(tabRep[i].length - 1) === ']') || (tabRep[i].charAt(tabRep[i].length - 1) === '[')) {
            // on a un intervalle et c’est la borne sup
              const borneSup = tabRep[i].substring(0, tabRep[i].length - 1)
              if (!isNaN(j3pCalculValeur(borneSup))) {
                reponseEleveApprochee = reponseEleveApprochee.replace(borneSup, j3pArrondi(j3pCalculValeur(borneSup), 10))
              }
            } else if ((tabRep[i].charAt(0) === '{') || (tabRep[i].charAt(tabRep[i].length - 1) === '}')) {
            // c’est le cas où la réponse n’est qu’une et une seule valeur
            // ou bien que c’est une valeur (au format latex avec frac ou sqrt) suivie de }
            // ou enfin une valeur précédée de {
              const valeur = tabRep[i].substring(1, tabRep[i].length - 1)
              const valeur2 = tabRep[i].substring(0, tabRep[i].length - 1)
              const valeur3 = tabRep[i].substring(1)
              if (!isNaN(j3pCalculValeur(valeur2))) {
                reponseEleveApprochee = reponseEleveApprochee.replace(valeur2, j3pArrondi(j3pCalculValeur(valeur2), 10))
              } else if (!isNaN(j3pCalculValeur(valeur3))) {
                reponseEleveApprochee = reponseEleveApprochee.replace(valeur3, j3pArrondi(j3pCalculValeur(valeur3), 10))
              } else if (!isNaN(j3pCalculValeur(valeur))) {
                reponseEleveApprochee = reponseEleveApprochee.replace(valeur, j3pArrondi(j3pCalculValeur(valeur), 10))
              }
            } else if (!isNaN(j3pCalculValeur(tabRep[i]))) {
              reponseEleveApprochee = reponseEleveApprochee.replace(tabRep[i], j3pArrondi(j3pCalculValeur(tabRep[i]), 10))
            } else {
            // à cet instant, pour que cela ressemble à un cas de figure non traité, il faut qu’on ait un nombre entre accolades
              let laRacine
              if (tabRep[i].charAt(0) === '{') {
                const nbAccOuvrante = tabRep[i].split('{').length - 1
                const nbAccFermante = tabRep[i].split('}').length - 1
                if (nbAccOuvrante === nbAccFermante) {
                  laRacine = tabRep[i].substring(1, tabRep[i].length - 1)
                  const tabRacine = laRacine.split(';')
                  for (i = 0; i < tabRacine.length; i++) {
                    if (!isNaN(j3pCalculValeur(tabRacine[i]))) {
                      reponseEleveApprochee = reponseEleveApprochee.replace(tabRacine[i], j3pArrondi(j3pCalculValeur(tabRacine[i]), 10))
                    }
                  }
                } else {
                  reponseEleveApprochee = reponseEleveApprochee.replace(tabRep[i].substring(1), j3pArrondi(j3pCalculValeur(tabRep[i].substring(1)), 10))
                }
              } else if (tabRep[i].charAt(tabRep[i].length - 1) === '}') {
                if (!isNaN(j3pCalculValeur(tabRep[i].substring(0, tabRep[i].length - 1)))) {
                  reponseEleveApprochee = reponseEleveApprochee.replace(tabRep[i].substring(0, tabRep[i].length - 1), j3pArrondi(j3pCalculValeur(tabRep[i].substring(0, tabRep[i].length - 1)), 10))
                }
              } else if ((tabRep[i].charAt(0) === '[') || (tabRep[i].charAt(0) === ']')) {
                laRacine = tabRep[i].substring(1)
                if (!isNaN(j3pCalculValeur(laRacine))) {
                  reponseEleveApprochee = reponseEleveApprochee.replace(laRacine, j3pArrondi(j3pCalculValeur(laRacine), 10))
                }
              }
            }
            me.logIfDebug('après transformation des nombres, reponseEleveApprochee:' + reponseEleveApprochee)
          }
          reponseIncomprise = false
          typeComment = 0

          // console.log("testIntervalle.non_intervalle:"+testIntervalle.non_intervalle+"   testIntervalle.separateur_virgule:"+testIntervalle.separateur_virgule)
          // on regarde s’il n’y a pas un nombre pair de crochets
          const nbCrochetsOuverts = reponseEleveApprochee.split('[').length - 1
          const nbCrochetsFermes = reponseEleveApprochee.split(']').length - 1
          if ((nbCrochetsOuverts + nbCrochetsFermes) % 2 === 0) {
            if (nbCrochetsOuverts + nbCrochetsFermes > 2) {
            // on a plus de 2 crochets (donc plus de 2 intervalles), il faut donc qu’on ait \\cup ou \\cap
              const cupcapReg = /\\cup|\\cap/g // new RegExp('\\\\cap|\\\\cup', 'ig')
              const tabInter = reponseEleveApprochee.split(cupcapReg)
              for (let i = 0; i < tabInter.length; i++) {
                const nbCOuverts = tabInter[i].split('[').length - 1
                const nbCFermes = tabInter[i].split(']').length - 1
                if (nbCOuverts + nbCFermes !== 2) {
                // on n’a pas d’intervalles séparés par cup ou cap donc la réponse n’est aps comprise
                  reponseIncomprise = true
                  reponse.bonneReponse = false
                }
              }
            }
          } else {
            reponse.bonneReponse = false
            reponseIncomprise = true
          }
          if (!testIntervalle.non_intervalle && testIntervalle.separateur_virgule) {
          // le séparateur dans l’intervalle est une virgule et non un point-virgule
            reponse.bonneReponse = false
            reponseIncomprise = true
          } else if (testIntervalle.non_intervalle) {
          // la réponse n’est pas un intervalle. On vérifie alors s’il est de le forme R\{...} ou {...} voire l’ensemble vide
            let analyseReponse = reponseEleveApprochee// cela va me servir à vérifier si la réponse entrée est une réponse acceptable (autre qu’un intervalle)
            if ((analyseReponse === '\\emptyset') || (analyseReponse === '\\varnothing')) {
            // l’élève a donné l’ensemble vide, donc on comprend
              reponseIncomprise = false
            } else {
              if (analyseReponse.indexOf('\\setminus') > -1) {
                analyseReponse = analyseReponse.replace('\\setminus', '')
              }
              if (analyseReponse.indexOf('\\backslash') > -1) {
                analyseReponse = analyseReponse.replace('\\backslash', '')
              }
              if (analyseReponse.indexOf('\\mathbb{R}\\') > -1) {
                analyseReponse = analyseReponse.replace('\\mathbb{R}\\', '')
              }
              if (analyseReponse.indexOf('\\mathbb{R}-') > -1) {
                analyseReponse = analyseReponse.replace('\\mathbb{R}-', '')
              }
              if (analyseReponse.indexOf('\\mathbb{R}') > -1) {
                analyseReponse = analyseReponse.replace('\\mathbb{R}', '')
              }
              // console.log("analyseReponse:"+analyseReponse)
              // maintenant, soit il ne reste rien, soit un ensemble de la forme {...} ou bien un intervalle (si l’élève a écrit R\]...[)
              if (analyseReponse === '') {
              // l’élève a répondu \\R, donc on comprend...
                reponseIncomprise = false
              } else if (!bonIntervalle(analyseReponse).non_intervalle) {
              // l’élève a répondu \\R\]...] ou avec un intervalle de ce style, bilan on comprend
                reponseIncomprise = false
              } else if ((analyseReponse.charAt(0) === '{') && (analyseReponse.charAt(analyseReponse.length - 1) === '}')) {
              // il ne reste qu’un ensemble de la forme {...}, donc on vérifie si ce qui est à l’intérieur est bien un nombre
                const tabAnalyseReponse = analyseReponse.substring(1, analyseReponse.length - 1).split(';')
                for (let i = 0; i < tabAnalyseReponse.length; i++) {
                  if (isNaN(tabAnalyseReponse[i])) {
                    reponseIncomprise = false
                  }
                }
              } else {
              // on a rien reconnu d’acceptable
                reponseIncomprise = true
              }
              // console.log("reponseIncomprise:"+reponseIncomprise)
              if (reponseIncomprise) {
                reponse.bonneReponse = false
              }
            }
          }
          // on regarde si les valeurs numériques données sont correctes
          // typeComment vaudra :
          // 1 si le trinôme admet 2 racines et que ces 2 racines sont correctes (comment3)
          // 2 si le trinôme admet une seule racine et que celle-ci est correcte (comment4)
          // 3 si le trinôme admet 2 racines et que l’un d’entre elles est correcte (comment5)
          // 4 si le nombre de racines données par l’élève n’est pas correct mais que l’une des racines proposées est correcte (comment9)
          // 5 aucune des racines n’est correcte (comment6)
          // 6 si le nombre de racines données par l’élève n’est pas correct et qu’aucune racine proposée n’est correcte (comment7)

          if (!reponseIncomprise) {
            const nombreReg = /-?[0-9]+\.?[0-9]*/g // new RegExp('\\-?[0-9]{1,}\\.?[0-9]{0,}', 'gi')
            if (nombreReg.test(reponseEleveApprochee)) {
              valeursPresentes = reponseEleveApprochee.match(nombreReg)
              valeursPresentes = tableauSansDoublon(valeursPresentes)
            } else {
              valeursPresentes = []
            }
            // on comprend la réponse de l’élève, on vérifie alors si elle est la bonne
            if (stor.objet_polynome.tabRacines.length === 0) {
            // pas de racine
              reponse.bonneReponse = (reponse.bonneReponse && (stor.tabIntervalle_rep[0] === reponseEleveApprochee))
              if (stor.autre_reponse_possible.length === 1) {
                reponse.bonneReponse = (reponse.bonneReponse || (stor.autre_reponse_possible[0] === reponseEleveApprochee))
              }
              if (!reponse.bonneReponse) {
                typeComment = (valeursPresentes.length > 0) ? 4 : 0
              }
            } else if (stor.objet_polynome.tabRacines.length >= 1) {
            // une seule racine
              if (stor.tabIntervalle_rep.length === 2) {
                reponse.bonneReponse = (reponse.bonneReponse && ((stor.tabIntervalle_rep[0] + '\\cup' + stor.tabIntervalle_rep[1] === reponseEleveApprochee) || (stor.tabIntervalle_rep[1] + '\\cup' + stor.tabIntervalle_rep[0] === reponseEleveApprochee)))
              } else {
                reponse.bonneReponse = (reponse.bonneReponse && (stor.tabIntervalle_rep[0] === reponseEleveApprochee))
              }
              if (stor.autre_reponse_possible.length >= 1) {
                for (let i = 0; i < stor.autre_reponse_possible.length; i++) {
                  reponse.bonneReponse = (reponse.bonneReponse || (stor.autre_reponse_possible[i] === reponseEleveApprochee))
                }
              }
              if (!reponse.bonneReponse) {
                if (stor.objet_polynome.tabRacines.length === 1) {
                  if (valeursPresentes.length !== 1) {
                    typeComment = 6
                    if (valeursPresentes.length > 0) {
                      for (let i = 0; i < valeursPresentes.length; i++) {
                        if (Math.abs(valeursPresentes[i] - stor.objet_polynome.valeur_racines[0]) < Math.pow(10, -10)) {
                          typeComment = 4
                        }
                      }
                    }
                  } else {
                    if (Math.abs(valeursPresentes[0] - stor.objet_polynome.valeur_racines[0]) < Math.pow(10, -10)) {
                      typeComment = 2
                    }
                  }
                } else {
                // 2 racines
                  if (valeursPresentes.length !== 2) {
                    typeComment = 6
                    if (valeursPresentes.length > 0) {
                    // valeursPresentes ne contient qu’une seule valeur
                      for (let i = 0; i < stor.objet_polynome.valeur_racines.length; i++) {
                        if (Math.abs(valeursPresentes[0] - stor.objet_polynome.valeur_racines[i]) < Math.pow(10, -10)) {
                          typeComment = 4
                        }
                      }
                    }
                  } else {
                    if ((Math.abs(stor.objet_polynome.valeur_racines[0] - valeursPresentes[0]) < Math.pow(10, -10)) || (Math.abs(stor.objet_polynome.valeur_racines[1] - valeursPresentes[0]) < Math.pow(10, -10))) {
                    // la première valeur donnée par l’élève est bien une racine du trinôme
                      if ((Math.abs(stor.objet_polynome.valeur_racines[1] - valeursPresentes[0]) < Math.pow(10, -10)) || (Math.abs(stor.objet_polynome.valeur_racines[1] - valeursPresentes[1]) < Math.pow(10, -10))) {
                      // la deuxième aussi
                        typeComment = 1
                      } else {
                        typeComment = 3
                      }
                    } else {
                      if ((Math.abs(stor.objet_polynome.valeur_racines[1] - valeursPresentes[0]) < Math.pow(10, -10)) || (Math.abs(stor.objet_polynome.valeur_racines[1] - valeursPresentes[1]) < Math.pow(10, -10))) {
                        typeComment = 3
                      } else {
                        typeComment = 5
                      }
                    }
                  }
                }
              }
            }
          }
          if (!reponse.bonneReponse) {
            fctsValid.zones.bonneReponse[0] = false
          }
          fctsValid.coloreUneZone(stor.zoneInput.id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
              stor.zoneCorr.innerHTML = cFaux
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
              if (!testIntervalle.non_intervalle && testIntervalle.separateur_virgule) {
              // l’élève a donné un ou plusieurs intervalles mais le séparateur utilisé est la virgule et non le point-virgule
                stor.zoneCorr.innerHTML += '<br>' + textes.comment1
              } else if (reponseIncomprise) {
              // on ne comprend pas ce qu’a écrit l’élève
                stor.zoneCorr.innerHTML += '<br>' + textes.comment2
              } else if (interPresent) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment8
              } else if (typeComment === 1) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment3
              } else if (typeComment === 2) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment4
              } else if (typeComment === 3) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment5
              } else if (typeComment === 4) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment9
              } else if (typeComment === 5) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment6
              } else if (typeComment === 6) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment7
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
                if ((typeComment === 1) || (typeComment === 2)) {
                  me.typederreurs[3]++
                } else {
                  me.typederreurs[4]++
                }
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        // me.parcours.pe = me.score / ds.nbitems;
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          const nbErreurs = ds.nbitems - me.score
          if (me.typederreurs[3] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb sur l’ensemble des solutions avec de bonnes racines est supérieur au égal au seul fixé
            me.parcours.pe = ds.pe_3
          } else {
            me.parcours.pe = ds.pe_2
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
