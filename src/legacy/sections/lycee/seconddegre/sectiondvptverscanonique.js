import { j3pAddElt, j3pBarre, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMonome, j3pNombre, j3pVirgule, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    février 2014
    On donne une fonction polynôme de degré 2 sous forme développée et on demande la forme canonique

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['a', '[-4;4]', 'string', 'coefficient a dans l’écriture a(x-alpha)^2+beta, a différent de 0. Si a est aléatoire, il sera différent de 1 ou -1, s’il vaut l’une des deux valeurs, il y aura une zone de saisie en moins. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;4]|[-4;-2]" pour qu’il soit égal à 2 dans un premier temps, positif ensuite puis négatif.'],
    ['alpha', '[-5;5]', 'string', 'coefficient alpha dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre.'],
    ['beta', '[-5;5]', 'string', 'coefficient beta dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre.'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.5, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'Problème de signes' },
    { pe_3: 'Problème avec l’extremum' },
    { pe_4: 'Problème avec coefficient a' },
    { pe_5: 'insuffisant' }
  ]
}

const textes = {
  titre_exo: 'Forme développée et canonique',
  consigne1: '$£f$ est une fonction polynôme de degré 2 définie sur $\\R$ par $£g$.',
  consigne2: 'Compléter $£f(x)$ sous forme canonique :',
  phrase_erreur1: 'Il y a un ou plusieurs problème(s) de signe&nbsp;!',
  phrase_erreur2: 'Les coordonnées de l’extremum sont inexactes&nbsp;!',
  phrase_erreur3: 'Tu confonds l’abscisse et l’ordonnée du sommet&nbsp;!',
  phrase_erreur4: 'Il y a un problème dans la première zone&nbsp;!',
  corr1: '$£f(x)=ax^2+bx+c$ avec $a=£a$, $b=£b$ et $c=£c$.',
  corr2: "$£f(x)$ s'écrit sous la forme $a(x-\\alpha)^2+\\beta$ où $a=£a$ et $(\\alpha;\\beta)$ sont les coordonnées du sommet de la parabole.",
  corr3: 'On a $\\alpha=\\frac{-b}{2a}=£a$ et $\\beta=£f(\\alpha)=£b$.',
  corr4: 'Ainsi $£f(x)=£c$.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.petit.correction.color } })
    for (let i = 1; i <= 3; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', textes.corr1,
      {
        f: me.stockage[18],
        a: me.stockage[11],
        b: me.stockage[16],
        c: me.stockage[17]
      })
    j3pAffiche(stor.zoneExpli2, '', textes.corr2,
      {
        f: me.stockage[18],
        a: me.stockage[11]
      })
    const formeCano = me.stockage[14] + '(x' + j3pMonome(2, 0, -me.stockage[12]) + ')^2' + j3pMonome(2, 0, me.stockage[13])
    j3pAffiche(stor.zoneExpli3, '', textes.corr3 + '\n' + textes.corr4,
      {
        a: me.stockage[12],
        b: me.stockage[13],
        f: me.stockage[18],
        c: formeCano
      })
  }
  function constructionTabIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).includes('|')) {
      const tabA = texteIntervalle.split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (testIntervalleFermeEntiers.test(tabA[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(tabA[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if (!tabA[k] || (tabA[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(tabA[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tabA[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(tabA[k]))) {
            // on a un nombre
            tabIntervalle.push(tabA[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if (!texteIntervalle || (texteIntervalle === '')) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (testIntervalleFermeEntiers.test(texteIntervalle)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    return tabIntervalle
  }

  function enonceMain () {
    let alpha = stor.coef_alpha[me.questionCourante - 1]
    let beta = stor.coef_beta[me.questionCourante - 1]
    const interA = stor.coef_a[me.questionCourante - 1]
    if ((interA === '[0;0]') || (interA === '[0;1]') || (interA === '[-1;0]') || (interA === '[-1;1]')) {
      j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à a de valoir 0 ; 1 ou -1', { vanishAfter: 5 })
      // Toutefois, on laisse si on impose à a de valoir 1 ou -1, même si ce n’est pas l’idée originale de l’exo
      stor.coef_a[me.questionCourante - 1] = '[-4;4]'
    }
    if (alpha === '[0;0]') {
      j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à alpha de valoir 0', { vanishAfter: 5 })
      alpha = '[-5;5]'
    }
    if (beta === '[0;0]') {
      j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à beta de valoir 0', { vanishAfter: 5 })
      beta = '[-5;5]'
    }
    let coeffA, coeffAlpha, coeffBeta
    let pb2
    do {
      do {
        if (!isNaN(Number(stor.coef_a[me.questionCourante - 1]))) {
          coeffA = Number(stor.coef_a[me.questionCourante - 1])
        } else {
          const [lemin, lemax] = j3pGetBornesIntervalle(stor.coef_a[me.questionCourante - 1])
          coeffA = j3pGetRandomInt(lemin, lemax)
        }
      } while ((Math.abs(Math.abs(coeffA)) <= 1 && !['[-1;-1]', '[1;1]', '-1', '1'].includes(stor.coef_a[me.questionCourante - 1])))
      do {
        const [lemin, lemax] = j3pGetBornesIntervalle(alpha)
        coeffAlpha = j3pGetRandomInt(lemin, lemax)
      } while (Math.abs(coeffAlpha) < Math.pow(10, -12))
      let pb1
      do {
        const [lemin, lemax] = j3pGetBornesIntervalle(beta)
        coeffBeta = j3pGetRandomInt(lemin, lemax)
        pb1 = (Math.abs(Math.abs(coeffAlpha) - Math.abs(coeffBeta)) < Math.pow(10, -12)) && (alpha !== beta)
      } while (pb1)
      pb2 = ((coeffAlpha === 0) && (alpha !== '[0;0]')) || ((coeffBeta === 0) && (beta !== '[0;0]')) || (Math.abs(coeffBeta + coeffA) > 9)
    } while (pb2)
    const coeffB = -2 * coeffA * coeffAlpha
    const coeffC = coeffA * Math.pow(coeffAlpha, 2) + coeffBeta
    let coeffATxt
    if (coeffA === -1) {
      coeffATxt = '-'
    } else if (coeffA === 1) {
      coeffATxt = ''
    } else {
      coeffATxt = coeffA
    }
    me.stockage[11] = coeffA
    me.stockage[12] = coeffAlpha
    me.stockage[13] = coeffBeta
    me.stockage[14] = coeffATxt
    // stockage[15] contient le polynome sous développée
    me.stockage[15] = j3pMonome(1, 2, coeffA) + j3pMonome(2, 1, coeffB) + j3pMonome(3, 0, coeffC)
    me.stockage[16] = coeffB
    me.stockage[17] = coeffC
    const tabNomFct = ['f', 'g', 'h']
    const nomFct = tabNomFct[j3pGetRandomInt(0, 2)]
    me.stockage[18] = nomFct
    // début de la consigne
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1,
      {
        f: nomFct,
        g: nomFct + '(x)=' + me.stockage[15]
      })
    if (coeffAlpha > 0) {
      me.stockage[3] = '$-$'
      me.stockage[5] = 2
    } else {
      me.stockage[3] = '$+$'
      me.stockage[5] = 1
    }
    if (coeffBeta > 0) {
      me.stockage[4] = '$+$'
      me.stockage[6] = 1
    } else {
      me.stockage[4] = '$-$'
      me.stockage[6] = 2
    }
    stor.deuxZones = [-1, 1].includes(coeffA) // Si a vaut 1 ou -1, alors je n’ai que 2 zones
    if (stor.deuxZones) {
      const elt = j3pAffiche(stor.zoneCons2, '', textes.consigne2 + '\n$£f(x)=(x$ #1# &1&$)^2$ #2# &2&',
        {
          f: nomFct,
          inputmq1: { texte: '' },
          inputmq2: { texte: '' },
          liste1: { texte: ['', '+', '-'], correction: me.stockage[3], taillepolice: me.styles.petit.enonce.fontSize },
          liste2: { texte: ['', '+', '-'], correction: me.stockage[4], taillepolice: me.styles.petit.enonce.fontSize }
        })
      stor.zoneInput = elt.inputmqList.concat(elt.selectList)
    } else {
      const elt = j3pAffiche(stor.zoneCons2, '', textes.consigne2 + '\n$£f(x)=$&1&$(x$ #1# &2&$)^2$ #2# &3&',
        {
          f: nomFct,
          inputmq1: { texte: '' },
          inputmq2: { texte: '' },
          inputmq3: { texte: '' },
          liste1: {
            texte: ['', '+', '-'],
            correction: me.stockage[3],
            taillepolice: me.styles.petit.enonce.fontSize
          },
          liste2: { texte: ['', '+', '-'], correction: me.stockage[4], taillepolice: me.styles.petit.enonce.fontSize }
        })
      stor.zoneInput = elt.inputmqList.concat(elt.selectList)
    }
    const tabIdInput = (stor.deuxZones) ? stor.zoneInput.slice(1, 2) : stor.zoneInput.slice(1, 3)
    tabIdInput.forEach(eltInput => mqRestriction(eltInput, '\\d.,'))
    if (![-1, 1].includes(coeffA)) mqRestriction(stor.zoneInput[0], '-\\d.,')
    j3pFocus(stor.zoneInput[(stor.deuxZones) ? 2 : 0])

    me.logIfDebug('coeffA:' + coeffA + '   coeffAlpha:' + coeffAlpha + '   coeffBeta:' + coeffBeta + '   coeffB:' + coeffB + '   coeffC:' + coeffC)

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '10px' }) })

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // Obligatoire
    me.finEnonce()
  }
  // console.log("debut du code : ",me.etat)
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1' })
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.coef_a = constructionTabIntervalle(ds.a, '[-4;4]')
        stor.coef_alpha = constructionTabIntervalle(ds.alpha, '[-5;5]')
        stor.coef_beta = constructionTabIntervalle(ds.beta, '[-5;5]')

        // console.log('stor.coef_alpha:' + stor.coef_alpha + '   stor.coef_beta:' + stor.coef_beta)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      {
        const repEleve = []
        // On teste si une réponse a été saisie
        // dan les zones de saisie
        const nbZones = (stor.deuxZones) ? 2 : 3
        for (let i = 1; i <= nbZones; i++) {
          repEleve[i] = j3pValeurde(stor.zoneInput[i - 1])
        }
        me.logIfDebug('repEleve:' + repEleve)
        // dans les listes déroulantes
        const reponseIndex1 = stor.zoneInput[nbZones].selectedIndex
        const reponseIndex2 = stor.zoneInput[nbZones + 1].selectedIndex
        const aRepondu = ((repEleve[1] !== '') && (repEleve[2] !== '') && (repEleve[3] !== '') && (reponseIndex1 !== 0) && (reponseIndex2 !== 0))
        if (!aRepondu && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          for (let i = nbZones; i >= 1; i--) {
            if (repEleve[i] === '') j3pFocus(stor.zoneInput[i - 1])
          }
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // on bloque les listes
          stor.zoneInput[nbZones].disabled = true
          stor.zoneInput[nbZones + 1].disabled = true
          const bonneRep = []
          // pour coeffA
          if (!stor.deuxZones) {
            if (me.stockage[11] === -1) {
              bonneRep[1] = ((repEleve[1] === '-') || (repEleve[1] === '-1'))
            } else {
              bonneRep[1] = (repEleve[1] === j3pVirgule(me.stockage[11]))
            }
          }
          bonneRep[nbZones - 1] = (repEleve[nbZones - 1] === j3pVirgule(Math.abs(me.stockage[12])))
          bonneRep[nbZones] = (repEleve[nbZones] === j3pVirgule(Math.abs(me.stockage[13])))
          bonneRep[nbZones + 1] = (reponseIndex1 === me.stockage[5])
          bonneRep[nbZones + 2] = (reponseIndex2 === me.stockage[6])

          me.logIfDebug('bonneRep : ' + bonneRep)

          let bonneReponse = true
          for (let i = 1; i < bonneRep.length; i++) bonneReponse = (bonneReponse && bonneRep[i])
          // on met dans la bonne couleur ce qui est correct
          for (let i = 1; i <= nbZones; i++) {
            if (bonneRep[i]) {
              stor.zoneInput[i - 1].style.color = me.styles.cbien
              j3pDesactive(stor.zoneInput[i - 1])
              j3pFreezeElt(stor.zoneInput[i - 1])
            }
          }
          for (let i = 1; i <= 2; i++) {
            if (bonneRep[i + nbZones]) {
              stor.zoneInput[i + nbZones - 1].style.color = me.styles.cbien
              j3pDesactive(stor.zoneInput[i + nbZones - 1])
            }
          }
          // Bonne réponse
          if (bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            afficheCorrection(true)
            for (let i = 1; i <= nbZones; i++) stor.zoneInput[i - 1].style.color = me.styles.cbien
            for (let i = 1; i <= 2; i++) stor.zoneInput[i + nbZones - 1].style.color = me.styles.cbien
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
            // on met en rouge ce qui est faux
            for (let i = 1; i <= nbZones; i++) {
              if (!bonneRep[i]) stor.zoneInput[i - 1].style.color = me.styles.cfaux
            }
            for (let i = 1; i <= 2; i++) {
              if (!bonneRep[i + nbZones]) stor.zoneInput[i + nbZones - 1].style.color = me.styles.cfaux
            }
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              for (let i = 1; i <= nbZones; i++) j3pDesactive(stor.zoneInput[i - 1])
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneInput[nbZones].disabled = false
                stor.zoneInput[nbZones + 1].disabled = false
                if ((!bonneRep[nbZones + 1] || !bonneRep[nbZones + 2]) && (bonneRep[1] && ((stor.deuxZones) ? true : bonneRep[2]))) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur1
                } else if ((repEleve[nbZones - 1] === j3pVirgule(Math.abs(me.stockage[13]))) && (repEleve[nbZones] === j3pVirgule(Math.abs(me.stockage[12])))) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur3
                } else if (!stor.deuxZones && (!bonneRep[1] && bonneRep[nbZones - 1] && bonneRep[nbZones] && bonneRep[nbZones + 1] && bonneRep[nbZones + 2])) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur4
                } else if (!stor.deuxZones && bonneRep[1]) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur2
                }
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                return me.finCorrection()
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                me.typederreurs[2]++
                if ((!bonneRep[nbZones + 1] || !bonneRep[nbZones + 2]) && ((stor.deuxZones || bonneRep[1]) && bonneRep[nbZones - 1] && bonneRep[nbZones])) {
                // problèmes de signes
                  me.typederreurs[3]++
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur1
                } else if ((stor.deuxZones || bonneRep[1]) && ((repEleve[nbZones - 1] === j3pVirgule(Math.abs(me.stockage[13]))) && (repEleve[nbZones] === j3pVirgule(Math.abs(me.stockage[12]))))) {
                // confusion entre abscisse et ordonnée de l’extremum'
                  me.typederreurs[4]++
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur3
                } else if (!stor.deuxZones && ((!bonneRep[1]) && bonneRep[nbZones - 1] && bonneRep[nbZones] && bonneRep[nbZones + 1] && bonneRep[nbZones + 2])) {
                  me.typederreurs[5]++
                  // problème avec le coefficient a
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur4
                } else if (!stor.deuxZones && bonneRep[1]) {
                // problème avec les coordonnées de l’extremum'
                  me.typederreurs[6]++
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur2
                } else {
                // tous les autres cas de figure
                  me.typederreurs[7]++
                }
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                for (let i = 1; i <= nbZones; i++) {
                  j3pDesactive(stor.zoneInput[i - 1])
                  if (!bonneRep[i]) {
                    stor.zoneInput[i - 1].style.color = me.styles.cfaux
                  } else {
                    stor.zoneInput[i - 1].style.color = me.styles.cbien
                  }
                }
                // on barre les mauvaises réponses
                for (let i = 1; i <= nbZones; i++) {
                  if (!bonneRep[i]) j3pBarre(stor.zoneInput[i - 1])
                }
                for (let i = 1; i <= 2; i++) {
                  if (!bonneRep[nbZones + i]) j3pBarre(stor.zoneInput[i + nbZones - 1])
                }
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          const nbErreurs = ds.nbitems - me.score
          if (me.typederreurs[3] / nbErreurs >= ds.seuilerreur) {
            // problèmes de signes
            me.parcours.pe = ds.pe_2
          } else if ((me.typederreurs[4] + me.typederreurs[4]) / nbErreurs >= ds.seuilerreur) {
            // confusion entre abscisse et ordonnée de l’extremum' ou pb avec les coord de l’extremum'
            me.parcours.pe = ds.pe_3
          } else if (me.typederreurs[5] / nbErreurs >= ds.seuilerreur) {
            // problème avec le coefficient a
            me.parcours.pe = ds.pe_4
          } else {
            me.parcours.pe = ds.pe_5
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
