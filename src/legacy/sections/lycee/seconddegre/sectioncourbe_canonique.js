import { j3pAddElt, j3pBarre, j3pElement, j3pEmpty, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMonome, j3pNombre, j3pNombreBienEcrit, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import { j3pCreeTexte } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Rémi DENIAUD
        Mars 2013
        retrouver la forme canonique d’une fonction polynôme de degré 2 à l’aide de la courbe
        dans cette section, l’écriture de la forme canonique est guidée
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', ' d’essais par répétition'],
    ['a', '[-3;3]', 'string', 'coefficient a dans l’écriture a(x-alpha)^2+beta. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[1;1]|[2;4]|[-4;-2]" par exemple pour qu’il soit égal à 1 dans un premier temps, positif ensuite puis négatif'],
    ['alpha', '[-4;4]', 'string', 'coefficient alpha dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre'],
    ['beta', '[-3;3]', 'string', 'coefficient beta dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.5, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],

  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'Problème de signes' },
    { pe_3: 'Problème avec l’extremum' },
    { pe_4: 'confusion abscisse et ordonnée' },
    { pe_5: 'insuffisant' }
  ]
}

const textes = {
  titre_exo: 'Parabole et forme canonique',
  consigne1: '$£f$ est une fonction polynôme de degré 2 définie sur $\\R$ et dont on donne la courbe représentative ci-dessous :',
  consigne2: 'Compléter $£f(x)$ sous forme canonique :',
  phrase_erreur1: 'Il y a un ou plusieurs problème(s) de signe !',
  phrase_erreur2: 'Les coordonnées de l’extremum sont inexactes !',
  phrase_erreur3: 'Tu confonds l’abscisse et l’ordonnée du sommet !',
  corr1: '$£f(x)$ est de la forme $a(x-\\alpha)^2+\\beta$ où $a=£a$ et $(\\alpha;\\beta)$ sont les coordonnées du sommet de la parabole.',
  corr2: 'On obtient ainsi $£g$.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (nomFct, bonneRep) {
    const alpha = me.stockage[12]
    const beta = me.stockage[13]
    let couleurCorr = me.styles.cbien
    if (!bonneRep) couleurCorr = me.styles.petit.correction.color
    j3pEmpty(stor.repere.idDivRepere)
    stor.repere.add({ type: 'point', nom: 'A', par1: alpha, par2: 0, fixe: true, visible: false, etiquette: false, style: { couleur: couleurCorr, epaisseur: 2, taille: 18 } })
    stor.repere.add({ type: 'point', nom: 'B', par1: alpha, par2: beta, fixe: true, visible: false, etiquette: false, style: { couleur: couleurCorr, epaisseur: 2, taille: 18 } })
    stor.repere.add({ type: 'point', nom: 'C', par1: 0, par2: beta, fixe: true, visible: false, etiquette: false, style: { couleur: couleurCorr, epaisseur: 2, taille: 18 } })
    stor.repere.add({ type: 'segment', nom: 's1', par1: 'A', par2: 'B', style: { couleur: couleurCorr, epaisseur: 2 } })
    stor.repere.add({ type: 'segment', nom: 's2', par1: 'B', par2: 'C', style: { couleur: couleurCorr, epaisseur: 2 } })
    stor.repere.construit()
    j3pElement('segmentBC').setAttribute('stroke-dasharray', '5,5')
    j3pElement('segmentAB').setAttribute('stroke-dasharray', '5,5')
    // J’ajoute les coordonnées du sommet
    const posAlphay = (beta > 0)
      ? me.stockage[21] + (0.8 / me.stockage[22]) * me.stockage[23]
      : me.stockage[21] - 6
    const posAlphax = me.stockage[20] + (me.stockage[12] / me.stockage[22]) * me.stockage[23] - 5
    const posBetax = (alpha > 0)
      ? me.stockage[20] - (0.6 / me.stockage[22]) * me.stockage[23] - 5
      : me.stockage[20] + (0.3 / me.stockage[22]) * me.stockage[23] - 5
    const posBetay = me.stockage[21] - (me.stockage[13] / me.stockage[22]) * me.stockage[23] + 6
    j3pCreeTexte(stor.repere.svg, { x: posAlphax, y: posAlphay, texte: me.stockage[12], taille: 12, couleur: couleurCorr, italique: false, fonte: 'times new roman', bold: true })
    j3pCreeTexte(stor.repere.svg, { x: posBetax, y: posBetay, texte: me.stockage[13], taille: 12, couleur: couleurCorr, italique: false, fonte: 'times new roman', bold: true })
    // et maintenant la phrase qui donne la réponse
    const formeCanonique = nomFct + '(x)=' + me.stockage[14] + '(x' + j3pMonome(2, 0, -1 * me.stockage[12]) + ')^2' + j3pMonome(2, 0, me.stockage[13])
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneRep) ? me.styles.cbien : me.styles.petit.correction.color } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1, { f: nomFct, a: me.stockage[11] })
    j3pAffiche(zoneExpli2, '', textes.corr2, { g: formeCanonique })
  }
  // @todo à mutualiser avec les autres sections qui utilisent aussi cette fct
  function constructionTabIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).includes('|')) {
      const tabA = texteIntervalle.split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (testIntervalleFermeEntiers.test(tabA[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(tabA[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if (!tabA[k] || (tabA[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(tabA[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tabA[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(tabA[k]))) {
            // on a un nombre
            tabIntervalle.push(tabA[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if (!texteIntervalle || (texteIntervalle === '')) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (testIntervalleFermeEntiers.test(texteIntervalle)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    return tabIntervalle
  }
  function lgIntervalle (intervalle) {
    // cette fonction renvoie la longueur de l’intervalle'
    // si ce n’est pas un intervalle, alors elle renvoie 0'
    let lg = 0
    if (testIntervalleFermeEntiers.test(intervalle)) {
      // on a bien un intervalle
      const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
      const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
      lg = borneSup - borneInf
    }
    return lg
  }

  function enonceMain () {
    let alpha = stor.coef_alpha[me.questionCourante - 1]
    let beta = stor.coef_beta[me.questionCourante - 1]
    if (stor.coef_a[me.questionCourante - 1] === '[0;0]') {
      j3pShowError('le coefficient a ne peut être nul', { vanishAfter: 5 })
      stor.coef_a[me.questionCourante - 1] = '[-4;4]'
    }
    if (alpha === '[0;0]') {
      j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à alpha de valoir 0', { vanishAfter: 5 })
      alpha = '[-5;5]'
    }
    if (beta === '[0;0]') {
      j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à beta de valoir 0', { vanishAfter: 5 })
      beta = '[-5;5]'
    }
    let coeffA, coeffAlpha, coeffBeta, pb2
    do {
      const [lemin1, lemax1] = j3pGetBornesIntervalle(stor.coef_a[me.questionCourante - 1])
      coeffA = j3pGetRandomInt(lemin1, lemax1)
      const [lemin2, lemax2] = j3pGetBornesIntervalle(alpha)
      coeffAlpha = j3pGetRandomInt(lemin2, lemax2)
      const infoLgAlpha = lgIntervalle(alpha)
      const infoLgBeta = lgIntervalle(beta)
      const alphaBetaEgaux = ((infoLgAlpha.lg_inter === 0) && (infoLgBeta.lg_inter === 0) && (infoLgBeta.valeur === infoLgAlpha.valeur))
      let pb1
      do {
        const [lemin, lemax] = j3pGetBornesIntervalle(beta)
        coeffBeta = j3pGetRandomInt(lemin, lemax)
        pb1 = (coeffAlpha === coeffBeta) && (!alphaBetaEgaux)
      } while (pb1)
      // si nb_essai vaut 50, c’est que alpha et beta ne peuvent faire autrement que d'être égaux'
      pb2 = coeffA === 0 || ((coeffAlpha === 0) && (alpha !== '[0;0]')) || ((coeffBeta === 0) && (beta !== '[0;0]')) || (Math.abs(coeffBeta + coeffA) > 9)
    } while (pb2)
    if (coeffAlpha === coeffBeta) {
      j3pShowError('est-ce judicieux d’imposer à alpha et beta d’être égaux ?', { vanishAfter: 5 })
    }
    let coeffATxt
    if (coeffA === -1) {
      coeffATxt = '-'
    } else if (coeffA === 1) {
      coeffATxt = ''
    } else {
      coeffATxt = coeffA
    }
    me.stockage[11] = coeffA
    me.stockage[12] = coeffAlpha
    me.stockage[13] = coeffBeta
    me.stockage[14] = coeffATxt
    const tabNomFct = ['f', 'g', 'h']
    const nomFct = tabNomFct[j3pGetRandomInt(0, 2)]
    // début de la consigne
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons1, '', textes.consigne1,
      {
        f: nomFct
      })

    const largZoneMG = Math.min(me.zonesElts.MG.getBoundingClientRect().width, 450)
    // construction du repère
    const absMin = Math.min(coeffAlpha - 2, -6)
    const absMax = Math.max(coeffAlpha + 2, 6)
    let ordMax, ordMin
    if (coeffBeta > 0) {
      if (coeffA > 0) {
        ordMax = Math.max(coeffBeta + coeffA + 2, 6)
        ordMin = Math.min(ordMax - 12, -1)
      } else {
        ordMax = Math.max(coeffBeta + 1, 6)
        ordMin = Math.min(ordMax - 12, -1)
      }
    } else {
      if (coeffA > 0) {
        ordMin = Math.min(coeffBeta - 1, -6)
        ordMax = Math.max(ordMin + 12, 1)
      } else {
        ordMin = Math.min(coeffBeta + coeffA - 2, -6)
        ordMax = Math.max(ordMin + 12, 1)
      }
    }
    const uniterep = (largZoneMG - 30) / (absMax - absMin)
    const largeurRepere = ((absMax - absMin) + 0.5) * uniterep
    const hauteurRepere = ((ordMax - ordMin) + 0.5) * uniterep
    const posOX = (0.25 - absMin) * uniterep
    const posOY = (0.25 + ordMax) * uniterep
    me.stockage[20] = posOX
    me.stockage[21] = posOY
    me.stockage[22] = 1
    me.stockage[23] = uniterep
    me.stockage[24] = nomFct
    // je crée le tableau de valeurs pour tracer la courbe
    const tabValeurCb = []
    const lepas = (absMax - absMin) / 300
    for (let i = 0; i < 300; i++) tabValeurCb[i] = [absMin + lepas * i, coeffA * Math.pow(((absMin + lepas * i) - coeffAlpha), 2) + coeffBeta]
    stor.leRepere = j3pAddElt(stor.conteneur, 'div')
    stor.repere = new Repere({
      keepWeaksIds: true, // @todo virer ça dès qu’on pourra
      idConteneur: stor.leRepere,
      aimantage: false,
      visible: true,
      larg: largeurRepere,
      haut: hauteurRepere,

      pasdunegraduationX: 1,
      pixelspargraduationX: uniterep,
      pasdunegraduationY: 1,
      pixelspargraduationY: uniterep,
      debuty: 0,
      xO: posOX,
      yO: posOY,
      negatifs: true,
      fixe: true,
      trame: true,
      objets: [
        { type: 'courbe_tabvaleur', nom: 'c1', tab_val: tabValeurCb, style: { couleur: '#F00', epaisseur: 1 } }
      ]
    })
    stor.repere.construit()
    stor.leRepere.style.marginTop = '10px'

    // les réponses :
    me.stockage[1] = Math.abs(coeffAlpha)
    if (coeffAlpha > 0) {
      me.stockage[3] = '$-$'
      me.stockage[5] = 2
    } else {
      me.stockage[3] = '$+$'
      me.stockage[5] = 1
    }
    me.stockage[2] = Math.abs(coeffBeta)
    if (coeffBeta > 0) {
      me.stockage[4] = '$+$'
      me.stockage[6] = 1
    } else {
      me.stockage[4] = '$-$'
      me.stockage[6] = 2
    }
    // zone question (dans zones.MD)
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    const zoneCons2 = j3pAddElt(stor.conteneurD, 'div')
    const elt = j3pAffiche(zoneCons2, '', textes.consigne2 + '\n$£f(x)=' + coeffATxt + '(x$ #1# &1&$)^2$ #2# &2&',
      {
        f: nomFct,
        inputmq1: { texte: '' },
        inputmq2: { texte: '' },
        liste1: { texte: ['', '+', '-'], correction: me.stockage[3] },
        liste2: { texte: ['', '+', '-'], correction: me.stockage[4] }
      })
    stor.zoneInput = elt.inputmqList.concat(elt.selectList)
    stor.zoneInput.slice(0, 2).forEach(eltInput => {
      mqRestriction(eltInput, '\\d,.')
      eltInput.setAttribute('maxLength', 3)
    })
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div', '', { style: { paddingTop: '20px' } })
    j3pFocus(stor.zoneInput[2])

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':

      if (me.debutDeLaSection) {
        ds = me.donneesSection
        me.construitStructurePage({ structure: 'presentation1bis', ratioGauche: 0.5 })

        me.stockage = [0, 0, 0, 0]

        // me.videLesZones();
        me.afficheTitre(textes.titre_exo)

        stor.coef_a = constructionTabIntervalle(ds.a, '[-4;4]')
        stor.coef_alpha = constructionTabIntervalle(ds.alpha, '[-5;5]')
        stor.coef_beta = constructionTabIntervalle(ds.beta, '[-5;5]')

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      enonceMain()

      break // case "enonce":

    case 'correction':
      // on teste si une réponse a été saisie
      {
        const repEleve1 = j3pValeurde(stor.zoneInput[0])
        const repEleve2 = j3pValeurde(stor.zoneInput[1])
        me.logIfDebug('repEleve1:' + repEleve1 + '  et repEleve2:' + repEleve2)
        const reponseIndex1 = stor.zoneInput[2].selectedIndex
        const reponseIndex2 = stor.zoneInput[3].selectedIndex
        const aRepondu = ((repEleve1 !== '') && (repEleve2 !== '') && (reponseIndex1 !== 0) && (reponseIndex2 !== 0))
        let bonneReponse
        if ((!aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput[0])
          return me.finCorrection()
        } else {
        // une réponse a été saisie
        // on bloque les listes
          stor.zoneInput[2].disabled = true
          stor.zoneInput[3].disabled = true
          const bonneRep = []
          bonneRep[1] = (repEleve1 === j3pNombreBienEcrit(me.stockage[1]))
          bonneRep[2] = (repEleve2 === j3pNombreBienEcrit(me.stockage[2]))
          bonneRep[3] = (reponseIndex1 === me.stockage[5])
          bonneRep[4] = (reponseIndex2 === me.stockage[6])
          // on bloque les zones de saisie quand elles sont correctes
          for (let i = 2; i >= 1; i--) {
            if (bonneRep[i]) {
              j3pDesactive(stor.zoneInput[i - 1])
              stor.zoneInput[i - 1].setAttribute('style', 'color:' + me.styles.cbien)
            } else {
              j3pFocus(stor.zoneInput[i - 1])
            }
          }
          bonneReponse = (bonneRep[1] && bonneRep[2] && bonneRep[3] && bonneRep[4])
          // on met dans la bonne couleur ce qui est correct
          for (let i = 1; i <= 2; i++) {
            if (bonneRep[i]) {
              stor.zoneInput[i + 1].setAttribute('style', 'color:' + me.styles.cbien)
            }
            if (bonneRep[i + 2]) {
              stor.zoneInput[i + 1].setAttribute('style', 'color:' + me.styles.cbien + ';font-size:20px')
            }
          }
          if (bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            afficheCorrection(me.stockage[24], true)
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // on met en rouge ce qui est faux
            for (let i = 1; i <= 2; i++) {
              if (!bonneRep[i]) {
                stor.zoneInput[i - 1].setAttribute('style', 'color:' + me.styles.cfaux)
              }
            }
            if (!bonneReponse) {
              for (let i = 1; i <= 2; i++) {
                if (!bonneRep[i + 2]) {
                  stor.zoneInput[i + 1].setAttribute('style', 'font-size:20px ;color:' + me.styles.cfaux)
                }
              }
            }
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              afficheCorrection(me.stockage[24], false)
              for (let i = 1; i <= 2; i++) {
                j3pDesactive(stor.zoneInput[i - 1])
                j3pDesactive(stor.zoneInput[i + 1])
              }
            } else { // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                me.typederreurs[1]++
                // indication éventuelle ici
                stor.zoneInput[2].disabled = false
                stor.zoneInput[3].disabled = false
                if ((!bonneRep[3] || !bonneRep[4]) && (bonneRep[1] && bonneRep[2])) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur1
                } else if ((repEleve1 === j3pNombreBienEcrit(me.stockage[2])) && (repEleve2 === j3pNombreBienEcrit(me.stockage[1]))) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur3
                } else if ((!bonneRep[1] || !bonneRep[2]) && (bonneRep[3] && bonneRep[4])) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur2
                }
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                return me.finCorrection()
              } else {
              // Erreur au second essai
                me.typederreurs[2]++
                if ((!bonneRep[3] || !bonneRep[4]) && (bonneRep[1] && bonneRep[2])) {
                  me.typederreurs[3]++
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur1
                } else if ((repEleve1 === j3pNombreBienEcrit(me.stockage[2])) && (repEleve2 === j3pNombreBienEcrit(me.stockage[1]))) {
                  me.typederreurs[5]++
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur3
                } else if ((!bonneRep[1] || !bonneRep[2]) && (bonneRep[3] && bonneRep[4])) {
                  me.typederreurs[4]++
                  stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur2
                } else {
                  me.typederreurs[4]++
                }
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorrection(me.stockage[24], false)
                for (let i = 1; i <= 2; i++) {
                  if (!bonneRep[i]) {
                    j3pDesactive(stor.zoneInput[i - 1])
                    stor.zoneInput[i - 1].setAttribute('style', 'color:' + me.styles.cfaux)
                  }
                }
                // on barre les mauvaises réponses
                for (let i = 1; i <= 2; i++) {
                  if (!bonneRep[i]) j3pBarre(stor.zoneInput[i - 1])
                }
                for (let i = 1; i <= 2; i++) {
                  if (!bonneRep[i + 2]) j3pBarre(stor.zoneInput[i + 1])
                }
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          const nbErreurs = ds.nbitems - me.score
          if (me.typederreurs[3] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de forme (non reconnaissance de la forme canonique) est supé au égal au seul fixé
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[4] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de forme (non reconnaissance de la forme canonique) est supé au égal au seul fixé
            me.parcours.pe = ds.pe_3
          } else if (me.typederreurs[5] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de forme (non reconnaissance de la forme canonique) est supé au égal au seul fixé
            me.parcours.pe = ds.pe_4
          } else {
            me.parcours.pe = ds.pe_5
          }
        }
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation(true)

      break // case "navigation":
  }
}
