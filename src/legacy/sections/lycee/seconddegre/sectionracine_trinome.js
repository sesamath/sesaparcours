import { j3pAjouteBouton, j3pEmpty, j3pNombre, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pRandomTab, j3pRestriction, j3pGetBornesIntervalle, j3pAddElt, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexProduit, j3pSimplificationRacineTrinome, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    juin 2015
    Cette section permet de déterminer les racines d’un trinôme (ou solution d’une équation de degré 2)
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeQuest', ['racines', 'racines', 'equation'], 'array', 'On peut choisir le type de question : racine d’un trinôme ou résolution d’une équation du type ax^2+bx+c=0'],
    ['typeSol', 'quelconque', 'liste', "On peut imposer aux solutions d'être des nombres entiers, des nombres sans radical (éventuellement fractionnaires) ou de pouvoir contenir un radical (quelconque)", ['quelconque', 'entiers', 'sans radical']],
    ['a', '[-4;4]', 'string', 'coefficient a dans l’écriture ax^2+bx+c lorsque typeSol vaut "quelconque". On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;4]|[-4;-2]" pour qu’il soit égal à 2 dans un premier temps, positif ensuite puis négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier ou fractionnaire.'],
    ['b', '[-7;7]', 'string', 'coefficient b dans l’écriture ax^2+bx+c lorsque typeSol vaut "quelconque". Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['c', '[-7;7]', 'string', 'coefficient c dans l’écriture ax^2+bx+c lorsque typeSol vaut "quelconque". Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Racines d’un trinôme',
  // on donne les phrases de la consigne
  consigne1: 'Soit $£f$ la fonction trinôme définie sur $\\R$ par $£f(x)=£g$.',
  consigne2_1: 'L’équation $£f(x)=0$ admet @1@ solution(s).',
  consigne2_2: 'Le trinôme $£f(x)$ admet @1@ racine(s).',
  consigne3_1: 'Cette solution vaut &1&.',
  consigne3_2: 'Ces solutions valent &1& et &2&.',
  consigne3_3: 'Cette racine vaut &1&.',
  consigne3_4: 'Ces racines valent &1& et &2&.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment0: 'Il faut compléter la zone&nbsp;!',
  comment0_bis: 'Il faut compléter les zones&nbsp;!',
  comment1: 'Les deux valeurs données sont égales&nbsp;! Recommence&nbsp;!',
  comment2: 'Le nombre de solutions donné n’est pas correct, calcule de nouveau le discriminant&nbsp;!',
  comment2_2: 'Le nombre de racines donné n’est pas correct, calcule de nouveau le discriminant&nbsp;!',
  comment3: 'Il faut simplifier le résultat&nbsp;!',
  comment3_2: 'La fraction aurait tout de même pu être réduite&nbsp;!',
  comment4: 'Il faut simplifier les résultats (notamment les radicaux quand c’est possible)&nbsp;!',
  comment4Bis: 'Il faut simplifier les résultats&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Le trinôme est de la forme $ax^2+bx+c$ où $a=£a$, $b=£b$ et $c=£c$.',
  corr2: 'Le discriminant du trinôme vaut $\\Delta=b^2-4ac=£d$.',
  corr3_1: 'Comme $\\Delta>0$, l’équation $£f(x)=0$ admet deux solutions réelles : $x_1=\\frac{-b-\\sqrt{\\Delta}}{2a}=£x$ et $x_2=\\frac{-b+\\sqrt{\\Delta}}{2a}=£y$.',
  corr3_2: 'Comme $\\Delta>0$, le trinôme $£g$ admet deux racines réelles : $x_1=\\frac{-b-\\sqrt{\\Delta}}{2a}=£x$ et $x_2=\\frac{-b+\\sqrt{\\Delta}}{2a}=£y$.',
  corr4_1: 'Comme $\\Delta=0$, l’équation $£f(x)=0$ admet une unique solution réelle : $x_0=\\frac{-b}{2a}=£x$.',
  corr4_2: 'Comme $\\Delta=0$, le trinôme $£g$ admet une unique racine réelle : $x_0=\\frac{-b}{2a}=£x$.',
  corr5_1: 'Comme $\\Delta<0$, l’équation $£f(x)=0$ n’admet pas de solution réelle.',
  corr5_2: 'Comme $\\Delta<0$, le trinôme $£g$ n’admet pas de racine réelle.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function afficheCorr (bonneRep) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px', color: (bonneRep) ? me.styles.cbien : me.styles.petit.correction.color } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    j3pAffiche(zoneExpli1, '', textes.corr1,
      {
        a: stor.moncoefA[0],
        b: stor.moncoefB[0],
        c: stor.moncoefC[0]
      })
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli2, '', textes.corr2, {
      d: stor.delta[0]
    })
    const zoneExpli3 = j3pAddElt(zoneExpli, 'div')
    if (stor.typeQuest === 'racines') {
      if (Math.abs(stor.delta[1]) < Math.pow(10, -12)) {
        // une racine réelle
        j3pAffiche(zoneExpli3, '', textes.corr4_2, {
          g: stor.trinome,
          x: stor.x0[0]
        })
      } else if (stor.delta[1] > 0) {
        // deux racines réelles
        j3pAffiche(zoneExpli3, '', textes.corr3_2, {
          g: stor.trinome,
          x: stor.x1[0],
          y: stor.x2[0]
        })
      } else {
        // delta<0
        j3pAffiche(zoneExpli3, '', textes.corr5_2, {
          g: stor.trinome
        })
      }
    } else {
      if (Math.abs(stor.delta[1]) < Math.pow(10, -12)) {
        // une racine réelle
        j3pAffiche(zoneExpli3, '', textes.corr4_1, {
          f: stor.nomf,
          x: stor.x0[0]
        })
      } else if (stor.delta[1] > 0) {
        // deux racines réelles
        j3pAffiche(zoneExpli3, '', textes.corr3_1, {
          f: stor.nomf,
          x: stor.x1[0],
          y: stor.x2[0]
        })
      } else {
        // delta<0
        j3pAffiche(zoneExpli3, '', textes.corr5_1, {
          f: stor.nomf
        })
      }
    }
  }

  function suiteEnonce () {
    const nbRacine = Number(stor.zoneInput[0].value)
    j3pEmpty(stor.zoneCons3)
    j3pEmpty(stor.laPalette)
    if (stor.zoneInput[0].value === '') {
      j3pFocus(stor.zoneInput[0])
      stor.zoneInput = [stor.zoneInput[0]]
      stor.mesZonesSaisie = [stor.zoneInput[0].id]
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: stor.mesZonesSaisie
      })
    } else {
      let laConsigne3 = textes.consigne3_1
      if (nbRacine === 0) {
        stor.zoneInput = [stor.zoneInput[0]]
        stor.mesZonesSaisie = [stor.zoneInput[0].id]
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: stor.mesZonesSaisie
        })
      } else if (nbRacine === 1) {
        if (stor.typeQuest === 'racines') laConsigne3 = textes.consigne3_3
        const elt = j3pAffiche(stor.zoneCons3, '', laConsigne3,
          {
            inputmq1: { texte: '' }
          })
        stor.zoneInput = [stor.zoneInput[0], elt.inputmqList[0]]
        mqRestriction(stor.zoneInput[1], '\\d,./-', { commandes: ['fraction', 'racine'] })
        stor.zoneInput[1].typeReponse = ['nombre', 'exact']
        stor.mesZonesSaisie = stor.zoneInput.map(eltInput => eltInput.id)
        if (Math.abs(stor.delta[1]) < Math.pow(10, -12)) {
          stor.zoneInput[1].reponse = [stor.x0[1]]
          stor.zoneInput[1].solutionSimplifiee = ['non valide', 'non valide']// on peut mettre "valide et on accepte", "non valide" ou "reponse fausse";
        }
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: stor.mesZonesSaisie,
          validePerso: [stor.zoneInput[1].id]
        })
        // palette MQ :
        j3pPaletteMathquill(stor.laPalette, stor.zoneInput[1], {
          liste: ['fraction', 'racine']
        })
        j3pFocus(stor.zoneInput[1])
      } else if (nbRacine === 2) {
        // nbRacine vaut 2
        laConsigne3 = textes.consigne3_2
        if (stor.typeQuest === 'racines') laConsigne3 = textes.consigne3_4
        const elt = j3pAffiche(stor.zoneCons3, '', laConsigne3,
          {
            inputmq1: { texte: '' },
            inputmq2: { texte: '' }
          })
        stor.zoneInput = [stor.zoneInput[0]].concat(elt.inputmqList)
        mqRestriction(stor.zoneInput[1], '\\d,.+/-', { commandes: ['fraction', 'racine'] })
        mqRestriction(stor.zoneInput[2], '\\d,.+/-', { commandes: ['fraction', 'racine'] })
        stor.zoneInput[1].typeReponse = ['nombre', 'exact']
        stor.zoneInput[2].typeReponse = ['nombre', 'exact']
        stor.mesZonesSaisie = stor.zoneInput.map(eltInput => eltInput.id)
        if (stor.delta[1] > 0) {
          stor.zoneInput[1].reponse = [stor.x1[1]]
          stor.zoneInput[2].reponse = [stor.x2[1]]
          stor.zoneInput[1].solutionSimplifiee = ['non valide', 'non valide']// on peut mettre "valide et on accepte", "non valide" ou "reponse fausse";
          stor.zoneInput[2].solutionSimplifiee = ['non valide', 'non valide']// on peut mettre "valide et on accepte", "non valide" ou "reponse fausse";
        }
        j3pPaletteMathquill(stor.laPalette, stor.zoneInput[1], {
          liste: ['fraction', 'racine']
        })
        for (let i = 1; i <= 2; i++) {
          stor.zoneInput[i].addEventListener('focusin', ecoute.bind(null, i))
        }
        j3pFocus(stor.zoneInput[1])
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: stor.mesZonesSaisie,
          validePerso: [stor.zoneInput[1].id, stor.zoneInput[2].id]
        })
      }
    }
  }

  function ecoute (num) {
    if (stor.zoneInput[num].className.includes('mq-editable-field')) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput[num], {
        liste: ['fraction', 'racine']
      })
    }
  }

  function constrTabIntervalle (texteIntervalle, defaultInterval) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    const intervalleReg = testIntervalleFermeEntiers // /\[-?[0-9]+;-?[0-9]+]/i // new RegExp('\\[\\-?[0-9]{1,};\\-?[0-9]{1,}\\]', 'i')
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    const tabIntervalle = []
    let tabDecomp
    let monnum
    let monden
    let valFrac
    if (texteIntervalle.includes('|')) {
      const taba = texteIntervalle.split('|')
      for (let k = 0; k < ds.nbrepetitions; k++) {
        if (intervalleReg.test(taba[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(taba[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if ((taba[k] === undefined) || (taba[k] === '')) {
            tabIntervalle.push(defaultInterval)
          } else if (nbFracReg.test(taba[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = taba[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(taba[k]))) {
            // on a un nombre
            tabIntervalle.push(taba[k])
          } else {
            tabIntervalle.push(defaultInterval)
          }
        }
      }
    } else {
      if ((texteIntervalle === '') || (texteIntervalle === undefined)) {
        for (let k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(defaultInterval)
        }
      } else if (intervalleReg.test(texteIntervalle)) {
        for (let k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        for (let k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(texteIntervalle))) {
        // on a un nombre
        for (let k = 0; k < ds.nbrepetitions; k++) tabIntervalle.push(texteIntervalle)
      } else {
        for (let k = 0; k < ds.nbrepetitions; k++) tabIntervalle.push(defaultInterval)
      }
    }
    return tabIntervalle
  }

  function enonceMain () {
    stor.typeQuest = ds.typeQuest[me.questionCourante - 1]
    if (me.questionCourante === 1) {
      stor.lastCasFigure = -1//
      // cette variable retient le cas de figure rencontré à la question précédente
      // si ce n’est pas imposé, on fera en sorte de ne pas retomber dessus à la question suivante
    }
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    stor.nomf = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
    let leA, leB, leC, choixNbSol, sol1, sol2
    if (ds.typeSol === 'quelconque') {
      leB = stor.coefB[me.questionCourante - 1]
      leC = stor.coefC[me.questionCourante - 1]
      if ((stor.coefA[me.questionCourante - 1] === '[0;0]') || (stor.coefA[me.questionCourante - 1] === '0') || (stor.coefA[me.questionCourante - 1] === 0)) {
        j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à a de valoir 0', { vanishAfter: 5 })
        stor.coefA[me.questionCourante - 1] = '[-4;4]'
      }
      leA = stor.coefA[me.questionCourante - 1]
    } else if (ds.typeSol === 'entiers') {
      choixNbSol = j3pRandomTab([0, 1, 2], [0.1, 0.15, 0.75])
      if (choixNbSol === 0) {
        do {
          leA = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 6)
          leB = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
          leC = Math.round(leA / Math.abs(leA)) * j3pGetRandomInt(2, 6)
        } while (Math.pow(leB, 2) - 4 * leA * leC > 0)
      } else {
        if (choixNbSol === 1) {
          sol1 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
          sol2 = sol1
        } else {
          do {
            sol1 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
            sol2 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
          } while (Math.abs(Math.abs(sol1) - Math.abs(sol2)) < Math.pow(10, -12))
        }
        leA = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
        leB = Math.round(-leA * (sol1 + sol2))
        leC = Math.round(leA * sol1 * sol2)
      }
    } else {
      choixNbSol = j3pRandomTab([0, 1, 2], [0.1, 0.15, 0.75])
      if (choixNbSol === 0) {
        do {
          leA = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 6)
          leB = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
          leC = Math.round(leA / Math.abs(leA)) * j3pGetRandomInt(2, 6)
        } while (Math.pow(leB, 2) - 4 * leA * leC > 0)
      } else {
        const listeFrac = ['\\frac{1}{3}', '\\frac{2}{3}', '\\frac{4}{3}', '\\frac{5}{3}', '\\frac{7}{3}', '\\frac{1}{6}', '\\frac{5}{6}', '\\frac{7}{6}', '\\frac{11}{6}', '\\frac{1}{7}', '\\frac{2}{7}', '\\frac{3}{7}', '\\frac{9}{7}', '\\frac{8}{7}']
        const listeDen = [3, 3, 3, 3, 3, 6, 6, 6, 6, 7, 7, 7, 7, 7]
        const pioche = j3pGetRandomInt(0, (listeDen.length - 1))
        leA = (2 * j3pGetRandomInt(0, 1) - 1) * listeDen[pioche]

        if (choixNbSol === 1) {
          leA = Math.pow(leA, 2)
          sol1 = j3pGetLatexProduit(2 * j3pGetRandomInt(0, 1) - 1, listeFrac[pioche])
          sol2 = sol1
        } else {
          sol1 = j3pGetLatexProduit(2 * j3pGetRandomInt(0, 1) - 1, listeFrac[pioche])
          sol2 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
        }
        leB = j3pGetLatexProduit('-1', j3pGetLatexProduit(leA, j3pGetLatexSomme(sol1, sol2)))
        leC = j3pGetLatexProduit(j3pGetLatexProduit(leA, sol1), sol2)
      }
    }
    // Quand mes coef sont des entiers fixés, il faut les convertir en chaîne pour que le traitement suivant ne plante pas (c’est dû à la version initiale de la section)
    leA = String(leA)
    leB = String(leB)
    leC = String(leC)
    let coeffA = 0
    let coeffB = 0
    let coeffC = 0
    let coeffATxt = ''
    let coeffBTxt = ''
    let coeffCTxt = ''
    let deltaVal
    let nbTentative = 0// dans un cadre aléatoire, je teste pour que b et c ne soient pas tous les 2 nuls
    // mais si c’est imposé par l’utilisateur je ne veux pas que ça plante, donc j’utilise cette variable nbTentative
    let newCasFigure// newCasFigure correspond en fait au nombre de racines avec ces valeurs de a, b et c
    let sameCase
    do {
      if (leA.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffA = j3pNombre(leA.split('|')[1])
        coeffATxt = leA.split('|')[0]
      } else if (!isNaN(j3pNombre(leA))) {
        coeffA = j3pNombre(leA)
        coeffATxt = coeffA
      } else {
        const [lemin, lemax] = j3pGetBornesIntervalle(leA)
        coeffA = j3pGetRandomInt(lemin, lemax)
        coeffATxt = coeffA
      }
      if (leB.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffB = j3pNombre(leB.split('|')[1])
        coeffBTxt = leB.split('|')[0]
      } else if (!isNaN(j3pNombre(leB))) {
        coeffB = j3pNombre(leB)
        coeffBTxt = coeffB
      } else {
        let nbTentativeB = 0
        do {
          const [lemin, lemax] = j3pGetBornesIntervalle(leB)
          coeffB = j3pGetRandomInt(lemin, lemax)
          nbTentativeB++
          // en gros, si je peux, je fais en sorte que b soit non nul
        } while ((Math.abs(coeffB) < Math.pow(10, -12)) && (nbTentativeB < 50))
        coeffBTxt = coeffB
      }
      if (leC.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffC = j3pNombre(leC.split('|')[1])
        coeffCTxt = leC.split('|')[0]
      } else if (!isNaN(j3pNombre(leC))) {
        coeffC = j3pNombre(leC)
        coeffCTxt = coeffC
      } else {
        let nbTentativeC = 0
        do {
          const [lemin, lemax] = j3pGetBornesIntervalle(leC)
          coeffC = j3pGetRandomInt(lemin, lemax)
          nbTentativeC++
          // en gros, si je peux, je fais en sorte que c soit non nul
        } while ((Math.abs(coeffC) < Math.pow(10, -12)) && (nbTentativeC < 50))
        coeffCTxt = coeffC
      }
      deltaVal = Math.pow(coeffB, 2) - 4 * coeffA * coeffC

      if (Math.abs(deltaVal) < Math.pow(10, -12)) {
        newCasFigure = 1
      } else if (deltaVal > 0) {
        newCasFigure = 2
      } else {
        newCasFigure = 0
      }
      sameCase = (newCasFigure === stor.lastCasFigure)
      nbTentative++
    } while ((Math.abs(coeffA) < Math.pow(10, -12)) || (sameCase && (nbTentative < 50)))
    stor.lastCasFigure = newCasFigure
    stor.trinome = j3pGetLatexMonome(1, 2, coeffATxt) + j3pGetLatexMonome(2, 1, coeffBTxt) + j3pGetLatexMonome(2, 0, coeffCTxt)
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons1, '', textes.consigne1,
      { f: stor.nomf, g: stor.trinome })
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    let laConsigne2 = textes.consigne2_2
    if (stor.typeQuest === 'equation') {
      laConsigne2 = textes.consigne2_1
    }
    const elt = j3pAffiche(zoneCons2, '', laConsigne2,
      {
        f: stor.nomf,
        input1: { texte: '', dynamique: true, width: '12px', maxchars: 1 }
      })
    stor.zoneInput = [elt.inputList[0]]
    j3pRestriction(stor.zoneInput[0], '0-2')

    stor.zoneInput[0].addEventListener('keyup', suiteEnonce, true)
    stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div')
    stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
    const deltaTxt = j3pGetLatexSomme(j3pGetLatexProduit(coeffBTxt, coeffBTxt), j3pGetLatexProduit(j3pGetLatexProduit('-4', coeffATxt), coeffCTxt))
    stor.delta = [deltaTxt, deltaVal]
    me.logIfDebug('delta:' + stor.delta)
    stor.moncoefA = [coeffATxt, coeffA]
    stor.moncoefB = [coeffBTxt, coeffB]
    stor.moncoefC = [coeffCTxt, coeffC]
    stor.zoneInput[0].typeReponse = ['nombre', 'exact']
    stor.x0 = []
    stor.x1 = []
    stor.x2 = []
    if (Math.abs(deltaVal) < Math.pow(10, -12)) {
      // delta est nul, une seule racine
      stor.zoneInput[0].reponse = [1]
      const x0Val = -coeffB / (2 * coeffA)
      const x0Txt = j3pGetLatexQuotient(j3pGetLatexProduit('-1', coeffBTxt), j3pGetLatexProduit('2', coeffATxt))
      me.logIfDebug('x0Txt:' + x0Txt)
      stor.x0 = [x0Txt, x0Val]
    } else if (deltaVal > 0) {
      stor.zoneInput[0].reponse = [2]
      const x1Val = (-coeffB - Math.sqrt(deltaVal)) / (2 * coeffA)
      const x2Val = (-coeffB + Math.sqrt(deltaVal)) / (2 * coeffA)
      const x1Txt = j3pSimplificationRacineTrinome(coeffBTxt, '-', deltaTxt, coeffATxt)
      const x2Txt = j3pSimplificationRacineTrinome(coeffBTxt, '+', deltaTxt, coeffATxt)
      stor.x1 = [x1Txt, x1Val]
      stor.x2 = [x2Txt, x2Val]
      me.logIfDebug('x1Txt:' + x1Txt + '   x2Txt:' + x2Txt)
    } else {
      stor.zoneInput[0].reponse = [0]
    }
    j3pFocus(stor.zoneInput[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.zoneMD, 'div')
    stor.zoneCorr = j3pAddElt(stor.zoneMD, 'div', '', { style: { paddingTop: '10px' } })

    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    stor.mesZonesSaisie = [stor.zoneInput[0].id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: stor.mesZonesSaisie
    })
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        if ((ds.typeQuest === 'racines') || (ds.typeQuest === 'racine')) {
          ds.typeQuest = ['racines']
        } else if ((ds.typeQuest === 'équation') || (ds.typeQuest === 'equation')) {
          ds.typeQuest = ['equation']
        }
        if ((ds.typeQuest === undefined) || (typeof ds.typeQuest === 'string')) {
          ds.typeQuest = []
        }
        for (let i = 0; i < ds.nbrepetitions; i++) {
          if (i === 0) {
            if (ds.typeQuest[i] === undefined) {
              ds.typeQuest[i] = 'racines'
            }
          } else {
            if (ds.typeQuest[i] === undefined) {
              ds.typeQuest[i] = ds.typeQuest[0]
            }
          }
        }
        stor.coefA = constrTabIntervalle(ds.a, '[-4;4]')
        stor.coefB = constrTabIntervalle(ds.b, '[-7;7]')
        stor.coefC = constrTabIntervalle(ds.c, '[-7;7]')
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = { aRepondu: false, bonneReponse: false }
        reponse.aRepondu = stor.fctsValid.valideReponses()
        let bonNbSol = true
        let aRepondu = true
        for (let i = 0; i < stor.fctsValid.zones.aRepondu.length; i++) aRepondu = (aRepondu && (stor.fctsValid.zones.reponseSaisie[i] !== ''))
        let repSimplifiee = true
        let radicalAVirer = false
        let valIdentiques
        // A cet instant reponse contient deux éléments :
        if (stor.fctsValid.zones.aRepondu[0] && aRepondu) {
        // il faut aussi que je teste les deux zones dont la validation est perso
          stor.fctsValid.zones.bonneReponse[0] = stor.fctsValid.valideUneZone(stor.fctsValid.zones.inputs[0], stor.zoneInput[0].reponse[0]).bonneReponse
          if (!stor.fctsValid.zones.bonneReponse[0]) {
            bonNbSol = false
          }
          if (bonNbSol) {
            stor.fctsValid.zones.bonneReponse[0] = true
            if (String(stor.fctsValid.zones.reponseSaisie[0]) === '1') {
            // il y a une solution et c’est bien ce qu’a dit l’élève
              stor.fctsValid.valideUneZone(stor.zoneInput[0].id, stor.zoneInput[0].reponse[0])
              const rep0 = j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[1])
              reponse.bonneReponse = (Math.abs(rep0 - stor.x0[1]) < Math.pow(10, -12))
              if (reponse.bonneReponse) {
              // si elle est bonne on vérifie que le résultat est simplifié.
                if (!stor.fctsValid.zones.reponseSimplifiee[1][0] || !stor.fctsValid.zones.reponseSimplifiee[1][1]) {
                // c’est que la réponse n’est pas simplifiée
                  repSimplifiee = false
                  if (stor.fctsValid.zones.reponseSaisie[1].includes('sqrt')) {
                  // la réponse n’est vraiment pas simplifiée. Il faut le faire
                    reponse.aRepondu = false
                  }
                } else {
                  stor.fctsValid.zones.bonneReponse[1] = true
                }
              } else {
                stor.fctsValid.zones.bonneReponse[1] = false
              }
              if (reponse.aRepondu) {
                reponse.bonneReponse = (stor.fctsValid.zones.bonneReponse[0] && stor.fctsValid.zones.bonneReponse[1])
                stor.fctsValid.coloreUneZone(stor.zoneInput[1].id)
              }
            } else if (stor.fctsValid.zones.reponseSaisie[0] === '2') {
            // il y a deux solutions et c’est bien ce qu’a dit l’élève
            // on vérifie déjà si les réponses données sont bonnes
              const rep1 = j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[1])
              const rep2 = j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[2])
              const bonneRep1 = ((Math.abs(rep1 - stor.x1[1]) < Math.pow(10, -12)) && (Math.abs(rep2 - stor.x2[1]) < Math.pow(10, -12)))
              const bonneRep2 = ((Math.abs(rep1 - stor.x2[1]) < Math.pow(10, -12)) && (Math.abs(rep2 - stor.x1[1]) < Math.pow(10, -12)))
              me.logIfDebug('rep1 : ' + rep1 + '   rep2:' + rep2 + '   racines attendues : stor.x1[1]:' + stor.x1[1] + '  stor.x2[1]:' + stor.x2[1])
              me.logIfDebug('bonneRep1:', bonneRep1, '   bonneRep2:', bonneRep2)
              reponse.bonneReponse = (bonneRep1 || bonneRep2)
              if (reponse.bonneReponse) {
              // on vérifie que les réponses données sont simplifiées
                if (!stor.fctsValid.zones.reponseSimplifiee[1][0] || !stor.fctsValid.zones.reponseSimplifiee[1][1] || !stor.fctsValid.zones.reponseSimplifiee[2][0] || !stor.fctsValid.zones.reponseSimplifiee[2][1]) {
                // c’est que la réponse n’est pas simplifiée
                  repSimplifiee = false
                  // je peux aussi regarder si la réponse est fractionnaire et qu’il donne encore une racine carrée
                  if (Math.abs(Math.sqrt(stor.delta[1]) - Math.round(Math.sqrt(stor.delta[1]))) < Math.pow(10, -12)) {
                    if ((stor.fctsValid.zones.reponseSaisie[1].includes('sqrt')) || (stor.fctsValid.zones.reponseSaisie[2].includes('sqrt'))) {
                      reponse.aRepondu = false
                      radicalAVirer = true
                      if (stor.fctsValid.zones.reponseSaisie[2].includes('sqrt')) {
                        j3pFocus(stor.fctsValid.zones.inputs[2])
                      } else {
                        j3pFocus(stor.fctsValid.zones.inputs[1])
                      }
                    }
                  }
                } else {
                // c’est bon
                  stor.fctsValid.zones.bonneReponse[1] = true
                  stor.fctsValid.zones.bonneReponse[2] = true
                }
              } else {
              // on vérifie tout de même que l’élève ne donne pas 2 fois la même valeur (correcte)
                valIdentiques = false
                stor.fctsValid.zones.bonneReponse[1] = false
                stor.fctsValid.zones.bonneReponse[2] = false
                if (((Math.abs(rep1 - stor.x1[1]) < Math.pow(10, -12)) && (Math.abs(rep1 - rep2) < Math.pow(10, -12))) || ((Math.abs(rep1 - stor.x2[1]) < Math.pow(10, -12)) && (Math.abs(rep1 - rep2) < Math.pow(10, -12)))) {
                  valIdentiques = true
                  stor.fctsValid.zones.bonneReponse[1] = true
                // reponse.aRepondu = false;
                } else {
                // on regarde si l’une des réponse n’est pas exacte malgré tout
                  if ((Math.abs(rep1 - stor.x1[1]) < Math.pow(10, -12)) || (Math.abs(rep1 - stor.x2[1]) < Math.pow(10, -12))) {
                  // la première réponse est bonne (et pas la deuxième)
                  // Je vérifie quand même qu’elle est simplifiée
                    if (!stor.fctsValid.zones.reponseSimplifiee[1][0] || !stor.fctsValid.zones.reponseSimplifiee[1][1]) {
                      repSimplifiee = false
                      // je peux aussi regarder si la réponse est fractionnaire et qu’il donne encore une racine carrée
                      if (Math.abs(Math.sqrt(stor.delta[1]) - Math.round(Math.sqrt(stor.delta[1]))) < Math.pow(10, -12)) {
                        if (stor.fctsValid.zones.reponseSaisie[1].includes('sqrt')) {
                          reponse.aRepondu = false
                          radicalAVirer = true
                          j3pFocus(stor.fctsValid.zones.inputs[1])
                        }
                      }
                    } else {
                      stor.fctsValid.zones.bonneReponse[1] = true
                    }
                    stor.fctsValid.zones.bonneReponse[2] = false
                  } else if ((Math.abs(rep2 - stor.x1[1]) < Math.pow(10, -12)) || (Math.abs(rep2 - stor.x2[1]) < Math.pow(10, -12))) {
                  // la deuxième réponse est bonne (et pas la première)
                    stor.fctsValid.zones.bonneReponse[1] = false
                    // Je vérifie quand même qu’elle est simplifiée
                    if (!stor.fctsValid.zones.reponseSimplifiee[2][0] || !stor.fctsValid.zones.reponseSimplifiee[2][1]) {
                      repSimplifiee = false
                      // je peux aussi regarder si la réponse est fractionnaire et qu’il donne encore une racine carrée
                      if (Math.abs(Math.sqrt(stor.delta[1]) - Math.round(Math.sqrt(stor.delta[1]))) < Math.pow(10, -12)) {
                        if (stor.fctsValid.zones.reponseSaisie[2].includes('sqrt')) {
                          reponse.aRepondu = false
                          radicalAVirer = true
                          j3pFocus(stor.fctsValid.zones.inputs[2])
                        }
                      }
                    } else {
                      stor.fctsValid.zones.bonneReponse[2] = true
                    }
                  } else {
                  // les deux réponses sont fausses
                    stor.fctsValid.zones.bonneReponse[1] = false
                    stor.fctsValid.zones.bonneReponse[2] = false
                  }
                }
              }
              if (reponse.aRepondu) {
                reponse.bonneReponse = (stor.fctsValid.zones.bonneReponse[0] && stor.fctsValid.zones.bonneReponse[1] && stor.fctsValid.zones.bonneReponse[2])
                stor.fctsValid.coloreUneZone(stor.zoneInput[0].id)
                stor.fctsValid.coloreUneZone(stor.zoneInput[1].id)
                stor.fctsValid.coloreUneZone(stor.zoneInput[2].id)
              }
            } else {
              reponse.bonneReponse = true
              stor.fctsValid.coloreUneZone(stor.zoneInput[0].id)
            }
          } else {
            for (let i = stor.fctsValid.zones.inputs.length - 1; i >= 0; i--) {
              stor.fctsValid.zones.bonneReponse[i] = false
              stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
            }
          }
        }

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (!aRepondu) {
            if (stor.zoneInput[0].value === '') {
              msgReponseManquante = textes.comment0
            } else {
              if (stor.fctsValid.zones.reponseSaisie[0] === '2') {
                msgReponseManquante = textes.comment0_bis
              }
            }
          } else if (!repSimplifiee) {
            if (stor.fctsValid.zones.reponseSaisie[0] === '1') {
              if (stor.fctsValid.zones.reponseSaisie[1].includes('sqrt')) {
                msgReponseManquante = textes.comment3_2
              } else {
                msgReponseManquante = textes.comment3
              }
            } else if (stor.fctsValid.zones.reponseSaisie[0] === '2') {
              if (radicalAVirer) {
                msgReponseManquante = textes.comment4
                for (let i = 2; i >= 1; i--) {
                  if (stor.fctsValid.zones.reponseSaisie[i].includes('sqrt')) {
                    j3pFocus(stor.fctsValid.zones.inputs[i])
                  }
                }
              } else {
                msgReponseManquante = textes.comment4Bis
                for (let i = 2; i >= 1; i--) {
                  if (!stor.fctsValid.zones.reponseSimplifiee[i][0] || !stor.fctsValid.zones.reponseSimplifiee[i][1]) {
                    j3pFocus(stor.fctsValid.zones.inputs[i])
                  }
                }
              }
            }
          } else {
            msgReponseManquante = textes.comment4Bis
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
          } else {
            stor.zoneCorr.style.color = me.styles.cfaux
            // Pas de bonne réponse
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              j3pDesactive(stor.zoneInput[0])
              afficheCorr(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!bonNbSol) {
                if (stor.typeQuest === 'racines') {
                  stor.zoneCorr.innerHTML = textes.comment2_2
                } else {
                  stor.zoneCorr.innerHTML = textes.comment2
                }
              } else if (bonNbSol && (stor.fctsValid.zones.reponseSaisie[0] === '2') && valIdentiques) {
                stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
              }

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
