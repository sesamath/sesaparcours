import { j3pAddElt, j3pNombre, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pMonome, j3pStyle, j3pGetBornesIntervalle, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import tableauSignesVariations from 'src/legacy/outils/tableauSignesVariations'
import { mqNormalise } from 'src/lib/outils/conversion/casFormat'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import { j3pGetLatexMonome, j3pGetLatexProduit, j3pSimplificationRacineTrinome, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { afficheBloc } from 'src/legacy/core/functionsPhrase'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    mars 2014
    On donne la forme canonique d’une fonction polynôme de degré 2
    On demande à l’élève de compléter le tableau de variations

*/
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[-4;4]', 'string', 'coefficient a dans l’écriture a(x-alpha)^2+beta. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[1;1]|[2;4]|[-4;-4]" par exemple pour qu’il soit égal à 1 dans un premier temps, positif ensuite puis négatif'],
    ['alpha', '[-5;5]', 'string', 'coefficient alpha dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre'],
    ['beta', '[-5;5]', 'string', 'coefficient beta dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre'],
    ['forme_init', ['canonique', 'canonique', 'canonique'], 'array', 'Pour chaque répétition, l’élément du tableau vaut "canonique", "factorisee" ou "developpee" suivant la forme de la fonction donnée. Pour la forme factorisée, l’aléatoire ne prend pas en compte les bornes paramétrées pour alpha et beta.'],
    ['typeCorrection', 'canonique', 'liste', 'On peut proposer une correction avec la forme canonique ou bien en utilisant la dérivée (dans ce cas, forme_init sera égale à "developpee").', ['canonique', 'derivee']],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10 (par défaut)'],
    ['seuilerreur', 0.5, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 50% des cas par défaut)']
  ],
  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'Problème de signes' },
    { pe_3: 'Problème avec l’extremum' },
    { pe_4: 'Problème avec les variations' },
    { pe_5: 'insuffisant' }
  ]
}

const textes = {
  titre_exo: 'Tableau de variation d’un trinôme',
  consigne1: 'On définit sur $\\R$ la fonction $£f$, fonction polynôme de degré 2 mise sous forme canonique, par : $£g$.',
  consigne1_2: 'On définit sur $\\R$ la fonction $£f$, fonction polynôme de degré 2 mise sous forme développée, par : $£g$.',
  consigne1_3: 'On définit sur $\\R$ la fonction $£f$, fonction polynôme de degré 2 mise sous forme factorisée, par : $£g$.',
  consigne2: 'Complète le tableau ci-dessous décrivant les variations de $£f$ (il faut cliquer sur les flèches pour modifier les variations) :',
  info: 'Tu n’as le droit qu’à un seul essai.',
  // Corrigé pour la forme canonique
  corr1: '$£f(x)$ est de la forme $a(x-\\alpha)^2+\\beta$ où $a=£a$, $\\alpha=£c$ et $\\beta=£b$. ',
  // Corrigé pour la forme développée
  corr1_2: "$£f(x)$ est sous la forme $ax^2+bx+c$ où $a=£a$, $b=£b$ et $c=£c$. Elle s'écrit sous forme canonique $a(x-\\alpha)^2+\\beta$ où $\\alpha=\\frac{-b}{2a}=£d$ et $\\beta=f(\\alpha)=£e$.",
  // corrigé pour la forme factorisée
  corr1_3: "$£f(x)$ est sous la forme $a(x-x_1)(x-x_2)$ où $x_1=£x$ et $x_2=£{x2}$. $x_1$ et $x_2$ sont alors les deux solutions de l’équation $£f(x)=0$. Ainsi $£f(x)$ s'écrit sous forme canonique $a(x-\\alpha)^2+\\beta$ où $\\alpha=\\frac{x_1+x_2}{2}=£d$ (propriété de symétrie de la parabole) et $\\beta=f(\\alpha)=£e$.",
  corr2: '$(\\alpha;\\beta)$ sont les coordonnées du sommet de la parabole.',
  corr3: 'Comme $a=£a>0$, la fonction est décroissante sur le premier intervalle et croissante sur le second (comme la fonction carrée).',
  corr4: 'Comme $a=£a<0$, la fonction est croissante sur le premier intervalle et décroissante sur le second (à l’inverse de la fonction carrée).',
  corr5: 'On obtient alors le tableau de variation suivant :',
  corrDeriv1: '$£f$ est dérivable sur $\\R$ et pour tout réel $x$, $£f\'(x)=£{fprime}$.',
  corrDeriv2: 'On résout $£{fprime}>0$ ce qui donne $£{ineq}$. On en déduit de la dérivée est strictement positive sur $£i$.',
  corrDeriv3: 'De plus $£f(£x)=£y$.',
  phrase_erreur1: 'Les coordonnées de l’extremum sont permutées&nbsp;!',
  phrase_erreur2: 'Les variations ne sont pas les bonnes&nbsp;!',
  phrase_erreur3: 'Il y a un problème de signe&nbsp;!'
}
/**
 * section canonique_tabvar
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  function ajouteTabCorrection (bonneReponse) {
    // le tableau de variation :
    stor.zoneCorr.style.color = (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
    j3pDetruit(stor.infoDiv)
    const divParent = stor.zoneCorr
    if (ds.typeCorrection === 'derivee') {
      afficheBloc(divParent, textes.corrDeriv1, { f: stor.stockage[8], fprime: stor.fprime })
      // quand stor.signeCoefs[2] vaut '-', c’est que a<0
      stor.ineq = (stor.signeCoefs[2] === '-') ? 'x<' + stor.stockage[5] : 'x>' + stor.stockage[5]
      stor.intPositive = (stor.signeCoefs[2] === '-') ? ']-\\infty;' + stor.stockage[5] + '[' : ']' + stor.stockage[5] + ';+\\infty['
      afficheBloc(divParent, textes.corrDeriv2, { ineq: stor.ineq, fprime: stor.fprime, i: stor.intPositive })
      afficheBloc(divParent, textes.corrDeriv3, { f: stor.stockage[8], x: stor.stockage[5], y: stor.stockage[6] })
    } else {
      if (stor.forme_init === 'canonique') {
        j3pAffiche(divParent, '', textes.corr1 + '\n',
          {
            f: stor.stockage[8],
            a: stor.stockage[7],
            c: stor.stockage[5],
            b: stor.stockage[6]
          })
      } else if (stor.forme_init === 'developpee') {
        j3pAffiche(divParent, '', textes.corr1_2 + '\n',
          {
            f: stor.stockage[8],
            a: stor.coefATxt,
            c: stor.coefCTxt,
            b: stor.coefBTxt,
            d: stor.stockage[5],
            e: stor.stockage[6]
          })
      } else if (stor.forme_init === 'factorisee') {
        let valX1 = stor.x1Txt
        let valX2 = stor.x2Txt
        if ((stor.x1Txt.includes('sqrt')) && (!stor.x1Txt.includes('frac'))) {
          valX1 = stor.x2Txt
          valX2 = stor.x1Txt
        }
        j3pAffiche(divParent, '', textes.corr1_3 + '\n',
          {
            f: stor.stockage[8],
            a: stor.coefATxt,
            x: valX1,
            x2: valX2,
            d: stor.stockage[5],
            e: stor.stockage[6]
          })
      }
      j3pAffiche(divParent, '', textes.corr2 + '\n')
      if (j3pCalculValeur(stor.stockage[7]) > 0) {
        j3pAffiche(divParent, '', textes.corr3 + '\n',
          {
            a: stor.stockage[7]
          })
      } else {
        j3pAffiche(divParent, '', textes.corr4 + '\n',
          {
            a: stor.stockage[7]
          })
      }
    }
    if (!bonneReponse) {
      j3pAffiche(divParent, '', textes.corr5)
      // cet objet local comportera toutes les informations générales du tableau
      const tabDefLoc = {}
      // Tout ce qui concerne la mise en forme :
      tabDefLoc.mef = {}
      // la largeur
      tabDefLoc.mef.L = stor.largTab
      // la hauteur de la ligne des x
      tabDefLoc.mef.h = 50
      // la couleur
      tabDefLoc.mef.macouleur = me.styles.toutpetit.correction.color
      // le tableau principal, qui contiendra les infos de chaque ligne, il y aura donc autant de lignes (en plus de celle des x)
      tabDefLoc.lignes = []
      let indexVar = 0
      if (ds.typeCorrection === 'derivee') indexVar = 1// On aura la ligne avec la signe de la dérivée
      for (let i = 0; i <= indexVar; i++) {
        tabDefLoc.lignes[i] = {}
        tabDefLoc.lignes[i].valeurs_charnieres = {}
        // j’ai  besoin des valeurs approchées pour les ordonner facilement... (souvent ce sont les mêmes car valeurs entières mais si ce sont des expressions mathquill...)
        tabDefLoc.lignes[i].valeurs_charnieres.valeurs_approchees = []
        // Variations : (on peut mettre const)
        tabDefLoc.lignes[i].variations = []
        // on peut ajouter des valeurs de x avec leurs images.
        tabDefLoc.lignes[i].images = {}
        tabDefLoc.lignes[i].imagesapprochees = []
        tabDefLoc.lignes[i].valeurs_charnieres.valeurs_theoriques = ['-inf', String(stor.stockage[5]), '+inf']
        tabDefLoc.lignes[i].valeurs_charnieres.valeurs_affichees = ['-inf', String(stor.stockage[5]), '+inf']
        tabDefLoc.lignes[i].valeurs_charnieres.valeurs_approchees = ['-inf', stor.stockage[5], '+inf']
      }
      if (ds.typeCorrection === 'derivee') {
        tabDefLoc.lignes[0].type = 'signes'
        tabDefLoc.lignes[0].hauteur = 50
        tabDefLoc.lignes[0].images = [['-inf', ''], [String(stor.stockage[5]), '0'], ['+inf', '']]
        tabDefLoc.lignes[indexVar].expression = tabDefLoc.lignes[0].expression = stor.stockage[8] + "\\quad'"
        tabDefLoc.lignes[0].signes = (stor.signeCoefs[2] === '-') ? ['+', '-'] : ['-', '+']
        tabDefLoc.lignes[0].imagesapprochees = ['-inf', stor.stockage[5], '+inf']
      }
      // la première et unique ligne :

      // ligne de signes ou de variations ?
      tabDefLoc.lignes[indexVar].type = 'variations'
      // sa hauteur :
      tabDefLoc.lignes[indexVar].hauteur = 80
      tabDefLoc.lignes[indexVar].expression = stor.stockage[8]
      // A adapter
      if (j3pCalculValeur(stor.stockage[7]) > 0 > 0) {
        tabDefLoc.lignes[indexVar].variations = ['decroit', 'croit']
      } else {
        tabDefLoc.lignes[indexVar].variations = ['croit', 'decroit']
      }
      tabDefLoc.lignes[indexVar].cliquable = [false, false]
      // les intervalles décrivant les variations de la fonction
      tabDefLoc.lignes[indexVar].intervalles = [['-inf', String(stor.stockage[5])], [String(stor.stockage[5]), '+inf']]
      // A adapter
      tabDefLoc.lignes[indexVar].images = [['-inf', ''], [String(stor.stockage[5]), String(stor.stockage[6])], ['+inf', '']]
      tabDefLoc.lignes[indexVar].imagesapprochees = ['-inf', stor.stockage[5], '+inf']

      // on n’autorise pas l’ajout de lignes et d’intervalles
      tabDefLoc.ajout_lignes_possible = false
      tabDefLoc.ajout_intervalles_possible = false
      const correction = false
      tableauSignesVariations(divParent, j3pGetNewId('tableau2'), tabDefLoc, correction)
    }
  } // ajoute tabCorrection

  function constructionIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    const intervalleReg = testIntervalleFermeEntiers // new RegExp('\\[\\-?[0-9]{1,};\\-?[0-9]{1,}\\]', 'i')
    const nbFracReg = /-?[0-9]+\/[0-9]+/ // ?new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp, monnum, monden, valFrac
    if (texteIntervalle.indexOf('|') > -1) {
      const tabA = texteIntervalle.split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (intervalleReg.test(tabA[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(tabA[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if (!tabA[k] || (tabA[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(tabA[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tabA[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(tabA[k]))) {
            // on a un nombre
            tabIntervalle.push(tabA[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if (!texteIntervalle || (texteIntervalle === '')) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (intervalleReg.test(texteIntervalle)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(texteIntervalle))) {
        // on a un nombre
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    return tabIntervalle
  }
  function lgIntervalle (intervalle) {
    // cette fonction renvoie la longueur de l’intervalle'
    // si ce n’est pas un intervalle, alors elle renvoie 0'
    let lg = 0
    // const intervalleReg = testIntervalleFermeEntiers // new RegExp('\\[\\-?[0-9]{1,};\\-?[0-9]{1,}\\]', 'i')
    if (testIntervalleFermeEntiers.test(intervalle)) {
      // on a bien un intervalle
      const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
      const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
      lg = borneSup - borneInf
    }
    return lg
  }

  function suite () {
    stor.largTab = Math.min(85 * me.zonesElts.HG.getBoundingClientRect().width / 100, 600)
    const alpha = stor.coefAlpha[me.questionCourante - 1]
    let beta = stor.coefBeta[me.questionCourante - 1]
    const leA = stor.coefA[me.questionCourante - 1]
    if ((stor.coefA[me.questionCourante - 1] === '[0;0]') || (stor.coefA[me.questionCourante - 1] === '0')) {
      j3pShowError('le coefficient a ne peut être nul', { vanishAfter: 5 })
      stor.coefA[me.questionCourante - 1] = '[-4;4]'
    }
    let coefATxt
    let coefAlphaTxt
    let coefBetaTxt
    me.logIfDebug('leA:' + leA + '   alpha:' + alpha + '   beta:' + beta)
    let pbFormeFact = false
    let nbPbFormeFact = 0
    let coeffA, coeffAlpha, coeffBeta, pb2
    do {
      pbFormeFact = false
      coeffA = 0
      if (leA.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
        coeffA = j3pNombre(leA.split('|')[1])
        coefATxt = leA.split('|')[0]
      } else if (!isNaN(j3pNombre(leA))) {
        coeffA = j3pNombre(leA)
      } else {
        const [minA, maxA] = j3pGetBornesIntervalle(leA)
        coeffA = j3pGetRandomInt(minA, maxA)
      }
      if (!coefATxt) coefATxt = String(coeffA)
      coeffAlpha = 0
      coeffBeta = 0
      if (alpha.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
        coeffAlpha = j3pNombre(alpha.split('|')[1])
        coefAlphaTxt = alpha.split('|')[0]
      } else if (!isNaN(j3pNombre(alpha))) {
        coeffAlpha = j3pNombre(alpha)
      } else {
        const [minA, maxA] = j3pGetBornesIntervalle(alpha)
        coeffAlpha = j3pGetRandomInt(minA, maxA)
      }
      let pb1
      do {
        if (beta.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
          coeffBeta = j3pNombre(beta.split('|')[1])
          coefBetaTxt = beta.split('|')[0]
        } else if (!isNaN(j3pNombre(beta))) {
          coeffBeta = j3pNombre(beta)
        } else {
          const [minB, maxB] = j3pGetBornesIntervalle(beta)
          coeffBeta = j3pGetRandomInt(minB, maxB)
        }
        pb1 = (coeffAlpha === coeffBeta) && !((alpha === beta) && (lgIntervalle(alpha) === 0))
      } while (pb1)
      if ((((coeffA < 0) && (coeffBeta < 0)) || ((coeffA > 0) && (coeffBeta > 0))) && (stor.forme_init === 'factorisee')) {
        pbFormeFact = true
        nbPbFormeFact++// si nbPbFormeFact dépasse 50, c’est qu’on a imposé des coefs qui ne peuvent pas donner une forme factorisée
        if (nbPbFormeFact >= 50) {
          j3pShowError('avec les coefficients imposés, la forme factorisée n’existe pas !', { vanishAfter: 5 })
        }
      }
      pb2 = ((coeffA === 0) || ((coeffAlpha === 0) && ((alpha !== '[0;0]') && (alpha !== '0') && (alpha !== 0))) || ((coeffBeta === 0) && ((beta !== '[0;0]') && (beta !== '0') && (beta !== 0)))) || (pbFormeFact && (nbPbFormeFact < 50))
    } while (pb2)
    let racine1, racine2, x1, x2
    // Si j’ai une forme factorisée, je dois m’arranger pour que les racines ne soient pas avec un radical
    if (stor.forme_init === 'factorisee') {
      const coefb = -2 * coeffA * coeffAlpha
      const coefc = coeffA * Math.pow(coeffAlpha, 2) + coeffBeta
      const deltaInit = Math.pow(coefb, 2) - 4 * coeffA * coefc
      x1 = (-coefb - Math.sqrt(deltaInit)) / (2 * coeffA)
      x2 = (-coefb + Math.sqrt(deltaInit)) / (2 * coeffA)
      const nonEntier = (Math.abs(x1 - Math.round(x1)) > Math.pow(10, -10)) || (Math.abs(x2 - Math.round(x2)) > Math.pow(10, -10))
      if ((deltaInit <= 0) || isNaN(x1) || nonEntier || (Math.abs(x1) < Math.pow(10, -10)) || (Math.abs(x2) < Math.pow(10, -10))) {
        // J’ai des racines qui ne sont pas entières donc ça complique la tâche
        // Dans ce cas, je force les choses pour que mes racines soient entières
        racine1 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 8)
        do {
          racine2 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 8)
          racine2 += (Math.abs(racine1 % 2) === Math.abs(racine2 % 2)) ? 0 : 1
        } while (Math.abs(Math.abs(racine1) - Math.abs(racine2)) < Math.pow(10, -10) || (Math.abs(racine2) < Math.pow(10, -10)))
        coeffAlpha = Math.round(racine1 + racine2) / 2
        coeffBeta = Math.round(coeffA * (coeffAlpha - racine1) * (coeffAlpha - racine2))
        x1 = racine1
        x2 = racine2
        coefBetaTxt = j3pGetLatexProduit(coefATxt, j3pGetLatexProduit(j3pGetLatexSomme(coeffAlpha, j3pGetLatexProduit('-1', x1)), j3pGetLatexSomme(coeffAlpha, j3pGetLatexProduit('-1', x2))))
        const betaInter = coefBetaTxt.replace(/-/g, '')
        coefBetaTxt = betaInter
        beta = (j3pCalculValeur(coefBetaTxt) < 0) ? '-' + betaInter : betaInter
      }
    }
    let monCoefATxt, ecritureA
    if (leA.includes('frac')) {
    // c’est un nb en écriture fractionnaire'
      if (coeffA > 0) {
        ecritureA = coefATxt
        monCoefATxt = coefATxt
      } else {
        ecritureA = '-' + coefATxt
        monCoefATxt = '-' + coefATxt
      }
    } else {
      if (coeffA === -1) {
        ecritureA = '-'
      } else if (coeffA === 1) {
        ecritureA = ''
      } else {
        ecritureA = coeffA
      }
      monCoefATxt = coeffA
    }
    let ecritureBeta = j3pMonome(2, 0, coeffBeta)
    let ecritureAlpha = j3pMonome(2, 0, -coeffAlpha)
    let monCoefAlphaTxt = ecritureAlpha

    let monCoefBetaTxt = ecritureBeta
    if (alpha.includes('frac')) {
    // c’est un nb en écriture fractionnaire'
      if (coeffAlpha < 0) {
        ecritureAlpha = '+' + coefAlphaTxt
        monCoefAlphaTxt = coefAlphaTxt
      } else {
        ecritureAlpha = '-' + coefAlphaTxt
        monCoefAlphaTxt = j3pGetLatexProduit('-1', coefAlphaTxt)
      }
    }
    if (beta.includes('frac')) {
    // c’est un nb en écriture fractionnaire'
      if (coeffBeta > 0) {
        ecritureBeta = '+' + coefBetaTxt
        monCoefBetaTxt = coefBetaTxt
      } else {
        ecritureBeta = '-' + coefBetaTxt
        monCoefBetaTxt = '-' + coefBetaTxt
      }
    }
    me.logIfDebug(
      'ecritureA:' + ecritureA + '   ecritureAlpha:' + ecritureAlpha + '   ecritureBeta:' + ecritureBeta,
      '\nmonCoefATxt:' + monCoefATxt + '   monCoefAlphaTxt:' + monCoefAlphaTxt + '   monCoefBetaTxt:' + monCoefBetaTxt
    )
    const maFormeCano = (coeffAlpha === 0)
      ? ecritureA + 'x^2' + ecritureBeta
      : ecritureA + '\\left(x' + ecritureAlpha + '\\right)^2' + ecritureBeta
    stor.maFormeCano = maFormeCano
    const tabNomFct = ['f', 'g', 'h']
    const nomFct = tabNomFct[j3pGetRandomInt(0, 2)]
    stor.divConteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '15px' }) })
    stor.divConsigne1 = j3pAddElt(stor.divConteneur, 'div')
    // coefs utiles pour la forme développée
    const coefbTxt = (coeffAlpha === 0) ? '0' : j3pGetLatexProduit(monCoefATxt, j3pGetLatexProduit(monCoefAlphaTxt, '2'))
    const coefcTxt = (coeffAlpha === 0)
      ? monCoefBetaTxt
      : j3pGetLatexSomme(j3pGetLatexProduit(monCoefATxt, j3pGetLatexProduit(monCoefAlphaTxt, monCoefAlphaTxt)), monCoefBetaTxt)
    stor.formeDev = j3pGetLatexMonome(1, 2, monCoefATxt) + j3pGetLatexMonome(2, 1, coefbTxt) + j3pGetLatexMonome(3, 0, coefcTxt)
    stor.fprime = j3pGetLatexMonome(1, 1, j3pGetLatexProduit(monCoefATxt, '2')) + j3pGetLatexMonome(2, 0, coefbTxt)
    // coefs utiles pour la forme factorisée
    stor.coefATxt = monCoefATxt
    stor.coefBTxt = coefbTxt
    stor.coefCTxt = coefcTxt
    const deltaTxt = j3pGetLatexSomme(j3pGetLatexProduit(coefbTxt, coefbTxt), j3pGetLatexProduit(j3pGetLatexProduit('-4', monCoefATxt), coefcTxt))
    const deltaVal = j3pCalculValeur(deltaTxt)

    if (deltaVal > 0) {
    // deux racines
      const x1Txt = (stor.forme_init === 'factorisee') ? String(x1) : j3pSimplificationRacineTrinome(coefbTxt, '+', deltaTxt, monCoefATxt)
      const x2Txt = (stor.forme_init === 'factorisee') ? String(x2) : j3pSimplificationRacineTrinome(coefbTxt, '-', deltaTxt, monCoefATxt)
      stor.x1Txt = x1Txt
      stor.x2Txt = x2Txt
      if (x1Txt.includes('sqrt')) {
        if (x1Txt.includes('frac')) {
        // on a une racinde de la forme (-b+sqrt(delta))/2a
          stor.formeFact = ecritureA + '\\left(x-' + x1Txt + '\\right)\\left(x-' + x2Txt + '\\right)'
        } else {
        // le dénominatateur vaut 1, donc on n’a plus que -b+sqrt(delta)
          if (x1Txt.charAt(0) === '-') {
          // là il faut faire attention
            stor.formeFact = ecritureA + '\\left(x+' + x1Txt.substring(1) + '\\right)\\left(x+' + x2Txt.substring(1) + '\\right)'
          } else {
            stor.formeFact = ecritureA + '\\left(x-' + x1Txt + '\\right)\\left(x-' + x2Txt + '\\right)'
          }
        }
      } else {
        stor.formeFact = ecritureA + '\\left(x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', x1Txt)) + '\\right)\\left(x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', x2Txt)) + '\\right)'
      }
    } else if (Math.abs(deltaVal) < Math.pow(10, -12)) {
      stor.formeFact = maFormeCano
    } else {
      if (stor.forme_init === 'factorisee') {
        j3pShowError('Il n’y a pas de forme factorisée', { vanishAfter: 5 })
      }
    }
    me.logIfDebug('stor.formeDev:' + stor.formeDev + '   stor.formeFact:' + stor.formeFact)

    // écriture de la fonction polynôme sous forme canonique
    stor.stockage[10] = nomFct + '(x)=' + maFormeCano
    if (stor.forme_init === 'developpee') {
      stor.stockage[10] = nomFct + '(x)=' + stor.formeDev
    } else if (stor.forme_init === 'factorisee') {
      stor.stockage[10] = nomFct + '(x)=' + stor.formeFact
    }
    // console.log("forme développée:"+stor.formeDev+"   stor.formeFact:"+stor.formeFact);
    stor.signeCoefs = []
    stor.signeCoefs[0] = (coeffAlpha < 0) ? '-' : ''
    stor.signeCoefs[1] = (coeffBeta < 0) ? '-' : ''
    stor.signeCoefs[2] = (coeffA < 0) ? '-' : ''
    if (beta.includes('frac')) {
      stor.stockage[6] = stor.signeCoefs[1] + coefBetaTxt
    } else {
      stor.stockage[6] = coeffBeta
    }
    stor.coeffAlpha = coeffAlpha
    stor.coeffBeta = coeffBeta
    if (alpha.includes('frac')) {
      stor.stockage[5] = stor.signeCoefs[0] + coefAlphaTxt
    } else {
      stor.stockage[5] = coeffAlpha
    }
    if (leA.includes('frac')) {
      stor.stockage[7] = stor.signeCoefs[2] + coefATxt
    } else {
      stor.stockage[7] = coeffA
    }
    stor.stockage[8] = nomFct
    let laConsigne1 = textes.consigne1
    if (stor.forme_init === 'developpee') {
      laConsigne1 = textes.consigne1_2
    } else if (stor.forme_init === 'factorisee') {
      laConsigne1 = textes.consigne1_3
    }
    j3pAffiche(stor.divConsigne1, '', laConsigne1,
      {
        f: nomFct,
        g: stor.stockage[10]
      })
    j3pAffiche(stor.divConsigne1, '', '\n' + textes.consigne2 + '\n',
      {
        f: nomFct
      })
    // le tableau de variation :
    stor.divConsigne2 = j3pAddElt(stor.divConteneur, 'div')
    stor.divTableau = j3pAddElt(stor.divConsigne2, 'div')
    const divParent = stor.divTableau

    // ce gros objet comporte toutes les informations générales du tableau
    // stocké dans stor.tabDef pour pouvoir le réutiliser en correction... (test des réponses élèves, réaffichage du tableau...)
    stor.tabDef = {
      // Tout ce qui concerne la mise en forme :
      mef: {
        // la largeur
        L: stor.largTab,
        // la hauteur de la ligne des x
        h: 50,
        // la couleur
        macouleur: 'rgb(40,120,120)'
      },

      // ce qu’on mettra dans la palette de boutons Mathquill
      liste_boutons: ['fraction'],

      // le tableau principal, qui contiendra les infos de chaque ligne, il y aura donc autant de lignes (en plus de celle des x)
      lignes: [
        // la première et unique ligne :
        {
          // ligne de signes ou de variations ?
          type: 'variations',
          // sa hauteur :
          hauteur: 80,
          valeurs_charnieres: {
            // j’ai  besoin des valeurs approchées pour les ordonner facilement... (souvent ce sont les mêmes car valeurs entières mais si ce sont des expressions mathquill...)
            valeurs_approchees: ['-inf', 0, '+inf'], // le 0 en number et pas string c’est normal ?
            // j’ai mis 0 comme valeur charnière centrale, mais c’est arbitraire ici, ça n’impacte en rien la suite
            valeurs_theoriques: ['-inf', '0', '+inf'],
            valeurs_affichees: ['-inf', '?', '+inf']
          },
          // Variations : (on peut mettre const)
          variations: ['const', 'const'],
          // on peut ajouter des valeurs de x avec leurs images.
          images: [['-inf', ''], ['0', '?', 'I'], ['+inf', '']],
          // On attend rien comme images de +-inf, pour l’image de '0' c’est une zone de saisie, le troisième élément (I ici comme interdite - vient d’un vieux code) n’a pas d’incidence
          // c'était une anticipation d’une éventuelle correction dans le tableau, pas implémentée...
          imagesapprochees: ['-inf', 0, '+inf'],
          expression: nomFct,
          cliquable: [true, true],
          // les intervalles décrivant les variations de la fonction
          intervalles: [['-inf', '0'], ['0', '+inf']]
        }
      ],

      // on n’autorise pas l’ajout de lignes et d’intervalles (marche pas bien de toute manière...)
      ajout_lignes_possible: false,
      ajout_intervalles_possible: false
    }
    stor.idTableau1 = j3pGetNewId('tableau1')

    stor.divExplications = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '10px' }) })
    stor.zoneCorr = j3pAddElt(stor.divConteneur, 'div', '', { style: { paddingTop: '20px' } })
    tableauSignesVariations(divParent, stor.idTableau1, stor.tabDef, false, false, genereZone)
  } // suite

  function genereZone () {
    if (stor.tabDef.perso.mqList.length < 2) {
      // pourquoi ce if ?
      // on le laisse mais avec une protection contre toute boucle infinie
      if (stor.nbGenereZoneCalls == null) stor.nbGenereZoneCalls = 0
      stor.nbGenereZoneCalls++
      if (stor.nbGenereZoneCalls > 10) throw Error('Impossible de tracer les zones de saisies, le tracé du tableau a dû échouer')
      setTimeout(genereZone, 100)
    } else {
      const zonesIds = stor.tabDef.perso.mqList.map(elt => elt.id)
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: zonesIds,
        validePerso: zonesIds
      })
      // il faut aussi l’ajouter à tabDef pour que tableauSignesVariations puisse réaffecter les input s’il les détruit / recrée
      stor.tabDef.validationZonesInputs = stor.fctsValid.zones.inputs
      if (ds.nbchances === 1) {
        stor.infoDiv = j3pAddElt(stor.divConteneur, 'div')
        j3pAffiche(stor.infoDiv, '', textes.info)
        j3pStyle(stor.infoDiv, { fontSize: '0.9em', fontStyle: 'italic', paddingTop: '35px' })
      }
      j3pFocus(stor.tabDef.perso.mqList[0])
      me.finEnonce()
    }
  } // genereZone

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.surcharge({ nbchances: 1 })

        // Construction de la page
        me.construitStructurePage('presentation1bis')

        // par convention, stor.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        stor.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        stor.formeInit = ds.forme_init.map(forme => {
          if (/^d[ée]velopp[ée]e?$/.test(forme)) return 'developpee'
          else if (/^factoris[ée]e?$/.test(forme)) return 'factorisee'
          else return 'canonique'
        })
        me.afficheTitre(textes.titre_exo)
        // Quand on veut une correction avec la dérivée, alors on impose la forme développée à la fonction
        if (ds.typeCorrection === 'derivee') stor.formeInit.forEach((elt, index) => { stor.formeInit[index] = 'developpee' })
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        stor.coefA = constructionIntervalle(ds.a, '[-4;4]')
        stor.coefAlpha = constructionIntervalle(ds.alpha, '[-5;5]')
        stor.coefBeta = constructionIntervalle(ds.beta, '[-5;5]')
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      stor.forme_init = stor.formeInit[me.questionCourante - 1]
      suite()
      break // case "enonce":

    case 'correction': {
      // on récupère les réponses élèves ainsi
      me.logIfDebug(' stor.tabDef.lignes[0].valeurs_charnieres.reponses[1]:' + stor.tabDef.lignes[0].valeurs_charnieres.reponses[1],
        '\nstor.tabDef.lignes[0].imagesreponses[1]:' + stor.tabDef.lignes[0].imagesreponses[1],
        '\nstor.tabDef.lignes[0].variations[0]:' + stor.tabDef.lignes[0].variations[0],
        '\nstor.tabDef.lignes[0].variations[1]:' + stor.tabDef.lignes[0].variations[1]
      )
      let correction
      // On teste si une réponse a été saisie
      // on récupère les réponses des deux zones de saisie
      const repX = stor.tabDef.lignes[0].valeurs_charnieres.reponses[1]
      const repY = stor.tabDef.lignes[0].imagesreponses[1]

      const aRepondu = stor.fctsValid.valideReponses()
      if (!aRepondu && (!me.isElapsed)) {
        me.reponseManquante(stor.divExplications)
        return this.finCorrection()
      }

      // Une réponse a été saisie
      // on teste si la réponse est bonne
      const unArbre1 = new Tarbre(mqNormalise(repX), ['x'])
      const repXNb = unArbre1.evalue([0])
      const unArbre2 = new Tarbre(mqNormalise(repY), ['x'])
      const repYNb = unArbre2.evalue([0])

      me.logIfDebug('repXNb:' + repXNb + '  repYNb:' + repYNb + '  stor.coeffBeta:' + stor.coeffBeta + '   stor.coeffAlpha:' + stor.coeffAlpha)
      const bonneRep = []
      bonneRep[1] = (Math.abs(repXNb - stor.coeffAlpha) < Math.pow(10, -12))
      bonneRep[2] = (Math.abs(repYNb - stor.coeffBeta) < Math.pow(10, -12))
      // puis les variations
      const maVar1 = stor.tabDef.lignes[0].variations[0]
      const maVar2 = stor.tabDef.lignes[0].variations[1]
      if (j3pCalculValeur(stor.stockage[7]) > 0) {
        bonneRep[3] = ((maVar1 === 'decroit') && (maVar2 === 'croit'))
      } else {
        bonneRep[3] = ((maVar1 === 'croit') && (maVar2 === 'decroit'))
      }

      let bonneReponse = true
      for (let i = 1; i <= 3; i++) {
        bonneReponse = (bonneReponse && bonneRep[i])
      }
      // confusion entre alpha et beta
      const confusionAbsOrd = ((Math.abs(repXNb - stor.stockage[6]) < Math.pow(10, -12)) && (Math.abs(repYNb - stor.stockage[5]) < Math.pow(10, -12)))
      const pbSigneAbs = (Math.abs(repXNb + stor.stockage[5]) < Math.pow(10, -12))
      if (bonneReponse) {
        me.score++
        stor.divExplications.style.color = me.styles.cbien
        stor.divExplications.innerHTML = cBien
        j3pEmpty(stor.divTableau)
        // je le reconstruis : (sans les zones de saisies)
        stor.tabDef.mef.macouleur = me.styles.cbien
        correction = true
        tableauSignesVariations(stor.divTableau, stor.idTableau1, stor.tabDef, correction)
        ajouteTabCorrection(true)
        me.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.divExplications.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.divExplications.innerHTML = tempsDepasse
        me.typederreurs[10]++
        // je le reconstruis : (sans les zones de saisies)
        stor.tabDef.mef.macouleur = me.styles.cfaux
        correction = true
        j3pEmpty(stor.divTableau)
        tableauSignesVariations(stor.divTableau, stor.idTableau1, stor.tabDef, correction)
        // on peut ajouter un tableau avec les solutions :
        ajouteTabCorrection()
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.divExplications.innerHTML = cFaux
      if (confusionAbsOrd) {
        stor.divExplications.innerHTML += '<br/>' + textes.phrase_erreur1
      } else if (pbSigneAbs) {
        stor.divExplications.innerHTML += '<br/>' + textes.phrase_erreur3
        me.typederreurs[3]++
      } else if (!bonneRep[3]) {
        // problème avec les variations
        stor.divExplications.innerHTML += '<br/>' + textes.phrase_erreur2
        me.typederreurs[5]++
      }
      if (!bonneRep[1] || !bonneRep[2]) {
        me.typederreurs[4]++
      }
      if (me.essaiCourant < ds.nbchances) {
        stor.divExplications.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      stor.divExplications.innerHTML += '<br>' + regardeCorrection
      // je le reconstruis : (sans les zones de saisies)
      stor.tabDef.mef.macouleur = me.styles.cfaux
      correction = true
      j3pEmpty(stor.divTableau)
      tableauSignesVariations(stor.divTableau, stor.idTableau1, stor.tabDef, correction)
      // on peut ajouter un tableau avec les solutions :
      ajouteTabCorrection(false)

      me.typederreurs[2]++
      return this.finCorrection('navigation', true)
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          const nbErreurs = ds.nbitems - me.score
          if (me.typederreurs[3] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb sur le signe de l’abscisse est supé au égal au seul fixé
            me.parcours.pe = ds.pe_2
          } else if (me.typederreurs[4] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb sur l’extremum est supé au égal au seul fixé
            me.parcours.pe = ds.pe_3
          } else if (me.typederreurs[5] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb sur les variations est supé au égal au seul fixé
            me.parcours.pe = ds.pe_4
          } else {
            me.parcours.pe = ds.pe_5
          }
        }
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
