import { j3pNombre, j3pVirgule, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pRandomTab, j3pAddElt, j3pGetBornesIntervalle, j3pGetNewId, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import tableauSignesVariations from 'src/legacy/outils/tableauSignesVariations'
import { j3pGetLatexMonome, j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    août 2015
    On donne le tableau de variation d’une fonction polynôme de degré 2 ainsi que l’image d’un nombre
    On cherche l’expression de la fonction
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[-5;5]', 'string', 'coefficient a dans l’écriture a(x-alpha)^2+beta. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[1;1]|[2;4]|[-4;-4]" par exemple pour qu’il soit égal à 1 dans un premier temps, positif ensuite puis négatif'],
    ['alpha', '[-5;5]', 'string', 'coefficient alpha dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre'],
    ['beta', '[-5;5]', 'string', 'coefficient beta dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre'],
    ['imposer_nb', ['', '', ''], 'array', 'Pour chaque répétition, on peut imposer le nombre dont on donne l’image']
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Retrouver un trinôme à l’aide du tableau de variations',
  // on donne les phrases de la consigne
  consigne1: 'Soit $£f$ la fonction polynôme de degré 2 dont on donne le tableau de variations ci-dessous :',
  consigne2: 'On sait de plus que $£f$.',
  consigne3: 'Pour tout réel $x$, l’expression de $£f(x)$ est alors :<br>$£f(x)=$&1&.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Tu t’es trompé dès le début dans l’utilisation des valeurs du tableau !',
  comment2: 'Tu as fait dès le début une erreur de signe dans l’utilisation de l’une des valeurs du tableau !',
  comment3: 'L’utilisation des valeurs du tableau est correcte. Tu as par contre fait une erreur de calcul par la suite !',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'La fonction admet un maximum égal à $£b$ et atteint pour $x=£a$.',
  corr1_2: 'La fonction admet un minimum égal à $£b$ et atteint pour $x=£a$.',
  corr2: "Pour tout réel $x$, $£f(x)$ s'écrit alors sous la forme $a\\left(x-\\alpha\\right)^2+\\beta$ où $\\alpha=£a$ et $\\beta=£b$. On obtient ainsi $£f(x)=a\\left(x£c\\right)^2£d$.",
  corr2_2: "Pour tout réel $x$, $£f(x)$ s'écrit alors sous la forme $a\\left(x-\\alpha\\right)^2+\\beta$ où $\\alpha=£a$ et $\\beta=£b$. On obtient ainsi $£f(x)=ax^2£d$.",
  corr3: 'De plus $£f(£x)=£y$ ce qui donne $a\\left(£x£a\\right)^2£b=£y$ c’est-à-dire $£ca=£d$ et $a=£e$.',
  corr3_2: 'De plus $£f(£x)=£y$ ce qui donne $a\\left(£x£a\\right)^2£b=£y$ c’est-à-dire $a=£e$.',
  corr4: 'Ainsi $£f(x)=£g$ ce qui donne sous forme développée $£f(x)=£h$.',
  corr4_2: 'Ainsi $£f(x)=£h$.'
}
/**
 * section tabvar_trinome
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function testexpressions (poly1, poly2, variable) {
    // cette fonction vérifie si deux polynômes sont égaux
    // variable est la variable présente dans le polynôme
    const unarbre1 = new Tarbre(poly1, [variable])
    const unarbre2 = new Tarbre(poly2, [variable])
    let egaliteExp = true
    for (let i = 0; i <= 9; i++) {
      egaliteExp = (egaliteExp && (Math.abs(unarbre1.evalue([i]) - unarbre2.evalue([i])) < Math.pow(10, -12)))
    }
    return egaliteExp
  }
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.petit.correction.color } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    const laCorr1 = (stor.tab_valeurcoefs[0] > 0) ? textes.corr1_2 : textes.corr1_1
    j3pAffiche(zoneExpli1, '', laCorr1, {
      b: stor.tab_coefs[2],
      a: stor.tab_coefs[1]
    })
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    if (Math.abs(stor.tab_valeurcoefs[1]) < Math.pow(10, -12)) {
      j3pAffiche(zoneExpli2, '', textes.corr2_2, {
        f: stor.nom_f,
        b: stor.tab_coefs[2],
        a: stor.tab_coefs[1],
        c: j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', stor.tab_coefs[1])),
        d: j3pGetLatexMonome(2, 0, stor.tab_coefs[2])
      })
    } else {
      j3pAffiche(zoneExpli2, '', textes.corr2, {
        f: stor.nom_f,
        b: stor.tab_coefs[2],
        a: stor.tab_coefs[1],
        c: j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', stor.tab_coefs[1])),
        d: j3pGetLatexMonome(2, 0, stor.tab_coefs[2])
      })
    }
    const zoneExpli3 = j3pAddElt(zoneExpli, 'div')
    // calcul de (nombre-alpha)^2
    const coefDevanta = j3pGetLatexProduit(j3pGetLatexSomme(stor.nombre, j3pGetLatexProduit('-1', stor.tab_coefs[1])), j3pGetLatexSomme(stor.nombre, j3pGetLatexProduit('-1', stor.tab_coefs[1])))
    let ecritureCoefDevanta = coefDevanta
    if (Math.abs(j3pCalculValeur(coefDevanta) - 1) < Math.pow(10, -12)) {
      ecritureCoefDevanta = ''
    } else if (Math.abs(j3pCalculValeur(coefDevanta) + 1) < Math.pow(10, -12)) {
      ecritureCoefDevanta = '-'
    }
    const ymoinsbeta = j3pGetLatexSomme(stor.image, j3pGetLatexProduit('-1', stor.tab_coefs[2]))
    if (ecritureCoefDevanta === '') {
      j3pAffiche(zoneExpli3, '', textes.corr3_2, {
        f: stor.nom_f,
        x: stor.nombre,
        y: stor.image,
        b: j3pGetLatexMonome(2, 0, stor.tab_coefs[2]),
        a: j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', stor.tab_coefs[1])),
        e: stor.tab_coefs[0]
      })
    } else {
      j3pAffiche(zoneExpli3, '', textes.corr3, {
        f: stor.nom_f,
        x: stor.nombre,
        y: stor.image,
        b: j3pGetLatexMonome(2, 0, stor.tab_coefs[2]),
        a: j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', stor.tab_coefs[1])),
        c: ecritureCoefDevanta,
        d: ymoinsbeta,
        e: stor.tab_coefs[0]
      })
    }
    const zoneExpli4 = j3pAddElt(zoneExpli, 'div')
    if (Math.abs(j3pCalculValeur(coefDevanta) - 1) < Math.pow(10, -12)) {
      j3pAffiche(zoneExpli4, '', textes.corr4_2, {
        f: stor.nom_f,
        h: stor.forme_dev
      })
    } else {
      j3pAffiche(zoneExpli4, '', textes.corr4, {
        f: stor.nom_f,
        g: stor.forme_cano,
        h: stor.forme_dev
      })
    }
    // corr4 : "Ainsi £f(x)=£g$ ce qui donne sous forme développée $£f(x)=£h$."
  }
  function constructionTabIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).includes('|')) {
      const tabA = texteIntervalle.split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (testIntervalleFermeEntiers.test(tabA[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(tabA[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if (!tabA[k] || (tabA[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(tabA[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tabA[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(tabA[k]))) {
            // on a un nombre
            tabIntervalle.push(tabA[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if (!texteIntervalle || (texteIntervalle === '')) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (testIntervalleFermeEntiers.test(texteIntervalle)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    return tabIntervalle
  }
  function lgIntervalle (intervalle) {
    // cette fonction renvoie la longueur de l’intervalle'
    // si ce n’est pas un intervalle, alors elle renvoie 0'
    let lg = 0
    if (testIntervalleFermeEntiers.test(intervalle)) {
      // on a bien un intervalle
      const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
      const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
      lg = borneSup - borneInf
    }
    return lg
  }

  function enonceMain () {
    stor.nom_f = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
    const alpha = String(stor.coef_alpha[me.questionCourante - 1])
    const beta = String(stor.coef_beta[me.questionCourante - 1])
    if ((stor.coef_a[me.questionCourante - 1] === '[0;0]') || (stor.coef_a[me.questionCourante - 1] === '0')) {
      j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à a de valoir 0', { vanishAfter: 5 })
      stor.coef_a[me.questionCourante - 1] = '[-4;4]'
    }
    const leA = String(stor.coef_a[me.questionCourante - 1])
    let coefAlphaTxt
    let coefBetaTxt
    let coefATxt
    me.logIfDebug('leA:' + leA + '   alpha:' + alpha + '   beta:' + beta)
    let pb2, coeffA, coeffAlpha, coeffBeta
    do {
      if (leA.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffA = j3pNombre(leA.split('|')[1])
        coefATxt = leA.split('|')[0]
      } else if (!isNaN(j3pNombre(leA))) {
        coeffA = j3pNombre(leA)
        coefATxt = coeffA
      } else {
        const [lemin, lemax] = j3pGetBornesIntervalle(leA)
        coeffA = j3pGetRandomInt(lemin, lemax)
        coefATxt = coeffA
      }
      if (alpha.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffAlpha = j3pNombre(alpha.split('|')[1])
        coefAlphaTxt = alpha.split('|')[0]
      } else if (!isNaN(j3pNombre(alpha))) {
        coeffAlpha = j3pNombre(alpha)
        coefAlphaTxt = String(alpha)
      } else {
        const [lemin, lemax] = j3pGetBornesIntervalle(alpha)
        coeffAlpha = j3pGetRandomInt(lemin, lemax)
        coefAlphaTxt = String(coeffAlpha)
      }
      let pb1
      do {
        if (beta.includes('frac')) {
          // c’est un nb en écriture fractionnaire'
          coeffBeta = j3pNombre(beta.split('|')[1])
          coefBetaTxt = beta.split('|')[0]
        } else if (!isNaN(j3pNombre(beta))) {
          coeffBeta = j3pNombre(beta)
          coefBetaTxt = String(beta)
        } else {
          const [lemin, lemax] = j3pGetBornesIntervalle(beta)
          coeffBeta = j3pGetRandomInt(lemin, lemax)
          coefBetaTxt = String(coeffBeta)
        }
        pb1 = (coeffAlpha === coeffBeta) && !((alpha === beta) && (lgIntervalle(alpha) === 0))
      } while (pb1)
      pb2 = ((coeffA === 0) || ((coeffAlpha === 0) && ((alpha !== '[0;0]') && (alpha !== '0') && (alpha !== 0))) || ((coeffBeta === 0) && ((beta !== '[0;0]') && (beta !== '0') && (beta !== 0))))
    } while (pb2)
    let ecritureA
    if (leA.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
      ecritureA = coefATxt
    } else {
      if (coeffA === -1) {
        ecritureA = '-'
      } else if (coeffA === 1) {
        ecritureA = ''
      } else {
        ecritureA = coeffA
      }
    }
    stor.tab_valeurcoefs = [coeffA, coeffAlpha, coeffBeta]
    stor.tab_coefs = [coefATxt, coefAlphaTxt, coefBetaTxt, ecritureA]

    // console.log("stor.tab_valeurcoefs:"+stor.tab_valeurcoefs+"  et "+stor.tab_coefs)

    stor.forme_cano = ecritureA + '\\left(x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', coefAlphaTxt)) + '\\right)^2' + j3pGetLatexMonome(2, 0, coefBetaTxt)
    stor.forme_cano_decimale = coeffA + '*(x-(' + coeffAlpha + '))^2+(' + coeffBeta + ')'
    stor.forme_dev_init = ecritureA + '\\left(x^2' + j3pGetLatexMonome(2, 1, j3pGetLatexProduit('-2', coefAlphaTxt)) + j3pGetLatexMonome(3, 0, j3pGetLatexProduit(coefAlphaTxt, coefAlphaTxt)) + '\\right)' + j3pGetLatexMonome(2, 0, coefBetaTxt)
    const coefBTxt = j3pGetLatexProduit(coefATxt, j3pGetLatexProduit('-2', coefAlphaTxt))
    const coefCTxt = j3pGetLatexSomme(j3pGetLatexProduit(coefATxt, j3pGetLatexProduit(coefAlphaTxt, coefAlphaTxt)), coefBetaTxt)
    stor.forme_dev = j3pGetLatexMonome(1, 2, coefATxt) + j3pGetLatexMonome(2, 1, coefBTxt) + j3pGetLatexMonome(3, 0, coefCTxt)
    me.logIfDebug('la forme canonique:' + stor.forme_cano + ' et ' + stor.forme_cano_decimale + '    forme développée:' + stor.forme_dev)

    // création du conteneur dans lequel se trouveront toutes les conaignes

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons1, '', textes.consigne1, { f: stor.nom_f })
    // Construction du tableau de variation

    // cet objet local comportera toutes les informations générales du tableau
    const tabDef = {
      // Tout ce qui concerne la mise en forme :
      mef: {
        // 85% de la largeur de la fenêtre MG
        L: 85 * me.zonesElts.HG.getBoundingClientRect().width / 100,
        // la hauteur de la ligne des x
        h: 50,
        macouleur: me.styles.toutpetit.enonce.color
      },
      // le tableau principal, qui contiendra les infos de chaque ligne, il y aura donc autant de lignes (en plus de celle des x)
      lignes: [
        // la première et unique ligne :
        {
          // ligne de signes ou de variations ?
          type: 'variations',
          // sa hauteur :
          hauteur: 80,
          cliquable: [false, false],
          // les intervalles décrivant les variations de la fonction
          intervalles: [['-inf', String(coefAlphaTxt)], [String(coefAlphaTxt), '+inf']],
          valeurs_charnieres: {
            // j’ai  besoin des valeurs approchées pour les ordonner facilement...
            // (souvent ce sont les mêmes car valeurs entières mais si ce sont des expressions mathquill...)
            valeurs_approchees: ['-inf', coeffAlpha, '+inf'],
            // j’ai mis 0 comme valeur charnière centrale, mais c’est arbitraire ici, ça n’impacte en rien la suite
            // A adapter
            valeurs_theoriques: ['-inf', String(coefAlphaTxt), '+inf'],
            // A adapter
            valeurs_affichees: ['-inf', String(coefAlphaTxt), '+inf']

          },
          // Variations : (on peut mettre const)
          variations: coeffA > 0 ? ['decroit', 'croit'] : ['croit', 'decroit'],
          // on peut ajouter des valeurs de x avec leurs images.
          images: [['-inf', ''], [String(coefAlphaTxt), String(coefBetaTxt)], ['+inf', '']],
          imagesapprochees: ['-inf', coeffAlpha, '+inf'],
          expression: stor.nom_f
        }
      ],
      // on n’autorise pas l’ajout de lignes et d’intervalles
      ajout_lignes_possible: false,
      ajout_intervalles_possible: false
    }

    const correction = false
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div', { style: { position: 'relative' } })
    const divId = j3pGetNewId('tabvar')
    tableauSignesVariations(zoneCons2, divId, tabDef, correction)

    // on cherche de plus un nombre dont on donnera l’image
    const nbImpose = ds.imposer_nb[me.questionCourante - 1]
    let valNombre
    if ((nbImpose === '') || (nbImpose === 'alea')) {
      stor.nombre = Math.round(coeffAlpha) + (j3pGetRandomInt(0, 1) * 2 - 1) * (j3pGetRandomInt(1, 4))
      valNombre = stor.nombre
    } else {
      const typeDec = /-?[0-9]+\.[0-9]+/g // new RegExp('\\-?[0-9]{1,}\\.[0-9]{1,}', 'ig')
      const typeNb = /-?[0-9]+/g // new RegExp('\\-?[0-9]{1,}', 'ig')
      const regFrac1 = /\(-?[0-9]+\)\/\([0-9]+\)/g // new RegExp('\\(\\-?[0-9]{1,}\\)/\\([0-9]{1,}\\)', 'ig')
      const regFrac2 = /\(-?[0-9]+\/[0-9]+\)/g // new RegExp('\\(\\-?[0-9]{1,}/[0-9]{1,}\\)', 'ig')
      const regFrac3 = /[0-9]+\/[0-9]+/g // new RegExp('[0-9]{1,}/[0-9]{1,}', 'ig')
      let num, den, tabFrac
      if (typeDec.test(nbImpose)) {
        // c’est un nombre décimal
        stor.nombre = j3pVirgule(nbImpose)
      } else if (typeNb.test(nbImpose)) {
        // c’est un nombre entier
        stor.nombre = j3pVirgule(nbImpose)
      } else if (regFrac1.test(nbImpose)) {
        // c’est un nombre fractionnaire (...)/(...)
        tabFrac = regFrac1.split('/')
        num = tabFrac[0].substring(1, tabFrac[0].length - 1)
        den = tabFrac[1].substring(1, tabFrac[1].length - 1)
        stor.nombre = '\\frac{' + num + '}{' + den + '}'
      } else if (regFrac2.test(nbImpose)) {
        // c’est un nombre fractionnaire (.../...)
        tabFrac = regFrac1.split('/')
        num = tabFrac[0].substring(1)
        den = tabFrac[1].substring(0, tabFrac[1].length - 1)
        stor.nombre = '\\frac{' + num + '}{' + den + '}'
      } else if (regFrac3.test(nbImpose)) {
        // c’est un nombre fractionnaire .../...
        tabFrac = regFrac1.split('/')
        num = tabFrac[0]
        den = tabFrac[1]
        stor.nombre = '\\frac{' + num + '}{' + den + '}'
      }
      valNombre = j3pCalculValeur(stor.nombre)
    }
    // calcul de l’image:
    stor.image_val = coeffA * Math.pow(valNombre - coeffAlpha, 2) + coeffBeta
    stor.image = j3pGetLatexSomme(j3pGetLatexProduit(coefATxt, j3pGetLatexProduit(j3pGetLatexSomme(stor.nombre, j3pGetLatexProduit('-1', coefAlphaTxt)), j3pGetLatexSomme(stor.nombre, j3pGetLatexProduit('-1', coefAlphaTxt)))), coefBetaTxt)

    // gestion des erreurs possibles
    // si on confond alpha et beta, la valeur de a obtenue doit être adaptée
    const coeffAAlphabeta = (stor.image_val - coeffAlpha) / Math.pow(stor.nombre - coeffBeta, 2)
    stor.confusion_alphabeta_decimale = coeffAAlphabeta + '*(x-(' + coeffBeta + '))^2+(' + coeffAlpha + ')'
    // s’il y a un pb de signe sur alpha, la valeur de a obtenue doit être adaptée
    const coeffASignealpha = (stor.image_val - coeffBeta) / Math.pow(stor.nombre + coeffAlpha, 2)
    stor.signe_alpha_decimale = coeffASignealpha + '*(x+(' + coeffAlpha + '))^2+(' + coeffBeta + ')'

    me.logIfDebug('confusion_alphabeta_decimale:' + stor.confusion_alphabeta_decimale + '   signe_alpha_decimale:' + stor.signe_alpha_decimale)

    const zoneCons3 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons3, '', textes.consigne2,
      { f: stor.nom_f + '\\left(' + stor.nombre + '\\right)=' + stor.image })
    const zoneCons4 = j3pAddElt(stor.conteneur, 'div')
    const elt = j3pAffiche(zoneCons4, '', textes.consigne3,
      {
        f: stor.nom_f,
        inputmq1: { texte: '' }
      })
    stor.zoneInput = elt.inputmqList[0]
    stor.zoneInput.typeReponse = ['texte']
    mqRestriction(stor.zoneInput, '\\d,.+-/x^*()', { commandes: ['fraction', 'puissance'] })
    stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
    // création de la palette
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['fraction', 'puissance'] })
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesZonesSaisie })
    // pas besoin de filer les zones à tabDef car y’a pas d’inputs dans le tableau
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.coef_a = constructionTabIntervalle(ds.a, '[-5;5]')
        stor.coef_alpha = constructionTabIntervalle(ds.alpha, '[-5;5]')
        stor.coef_beta = constructionTabIntervalle(ds.beta, '[-5;5]')

        me.logIfDebug('stor.coef_alpha:' + stor.coef_alpha)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////

      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = {}
        reponse.aRepondu = false
        reponse.bonneReponse = false
        reponse.aRepondu = fctsValid.valideReponses()
        fctsValid.zones.bonneReponse[0] = true
        let numErreur
        if (reponse.aRepondu) {
          const repEleve1 = j3pValeurde(stor.zoneInput)
          // on vérifie si le polynôme écrit est correct
          const repEleveXcas1 = j3pMathquillXcas(repEleve1)
          reponse.bonneReponse = testexpressions(repEleveXcas1, stor.forme_cano_decimale, ['x'])
          numErreur = 0
          if (!reponse.bonneReponse) {
            fctsValid.zones.bonneReponse[0] = false
            if (testexpressions(repEleveXcas1, stor.confusion_alphabeta_decimale, ['x'])) {
              numErreur = 1
            } else if (testexpressions(repEleveXcas1, stor.signe_alpha_decimale, ['x'])) {
              numErreur = 2
            } else {
            // je vérifie si l’élève a bien utilisé alpha et beta et se serait trompé sur a
            // j’identifie la valeur de a donnée par l’élève
              let pbA = false
              if (repEleveXcas1.includes('x')) {
                const coefADonne = '(' + repEleveXcas1 + '-(' + stor.tab_coefs[2] + '))/((x-(' + stor.tab_coefs[1] + '))^2)'
                const unarbreCoefa = new Tarbre(coefADonne, ['x'])
                const valCoefaEleve = unarbreCoefa.evalue([0])
                pbA = true
                for (let i = 1; i <= 3; i++) {
                  const val2 = unarbreCoefa.evalue([i])
                  if (Math.abs(val2 - valCoefaEleve) > Math.pow(10, -10)) pbA = false
                }
              }
              if (pbA) {
                numErreur = 3
              }
            }
          }
          fctsValid.coloreUneZone(stor.zoneInput.id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
              stor.zoneCorr.innerHTML = cFaux
              if (numErreur === 1) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment1
              } else if (numErreur === 2) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment2
              } else if (numErreur === 3) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment3
              }
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
