import { j3pAjouteBouton, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pRandomTab, j3pAddElt, j3pGetNewId, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexProduit, j3pSimplificationRacineTrinome, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { tableauSignesFct } from 'src/legacy/outils/fonctions/tableauSignes'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    août 2015
    Dans cette section, l’élève doit donner (sous la forme d’un tableau) le signe d’un trinôme
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['trinomes', ['', '', ''], 'array', 'Pour chaque répétition, on peut imposer le trinôme dont on cherche le signe'],
    ['nbracines', ['alea', 'alea', 'alea'], 'array', 'Pour chaque répétition, on peut imposer le nombre de racine du trinôme lorsqu’il est aléatoire (0, 1 ou 2), sinon mettre "alea" ou ""'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.5, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],

  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe1: 'Bien' },
    { pe_2: 'Problème dans le calcul des racines éventuelles' },
    { pe_3: 'Insuffisant' }
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Signe d’un trinôme',
  // on donne les phrases de la consigne
  consigne1: 'Complète ci-dessous le tableau donnant le signe du trinôme $£t$ sur $\\R$.',
  signe: 'signe du trinôme',
  phrase5: 'Pour ajouter une valeur sur la première ligne : ',
  phrase6: 'Pour retirer une valeur sur la première ligne : ',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  tableau_incomplet: 'Il faut compléter entièrement le tableau&nbsp;!',
  comment1: 'Le nombre de racines n’est pas correct&nbsp;!',
  comment2: 'Les racines données ne sont pas correctes&nbsp;!',
  comment3: 'L’une des deux racines n’est pas correcte&nbsp;!',
  comment4: 'La racine donnée n’est pas correcte&nbsp;!',
  comment5: 'Les racines sont correctes, mais il y a un problème sur les signes&nbsp;!',
  comment6: 'La racine est correcte, mais il y a un problème sur les signes&nbsp;!',
  comment7: 'Le trinôme n’a effectivement pas de racine, mais le signe n’est pas correct&nbsp;!',
  comment8: 'Attention à mettre les racines dans le bon ordre&nbsp;!',
  comment10: 'Tu n’as pas le droit à une chance supplémentaire&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Le trinôme est de la forme $ax^2+bx+c$ où $a=£a$, $b=£b$ et $c=£c$. Son discriminant vaut $\\Delta=b^2-4ac=£d$.',
  corr2_1: 'Comme $\\Delta>0$, le trinôme admet deux racines réelles : $x1=\\frac{-b-\\sqrt{\\Delta}}{2a}=£x$ et $x_2=\\frac{-b+\\sqrt{\\Delta}}{2a}=£y$.',
  corr2_2: 'Comme $\\Delta=0$, le trinôme admet une unique racine réelle : $x_0=\\frac{-b}{2a}=£x$.',
  corr2_3: 'Comme $\\Delta<0$, le trinôme n’admet pas de racine réelle.',
  corr3_1: 'De plus $a=£a>0$, donc le trinôme est <b>positif</b> à l’extérieur des racines.',
  corr3_2: 'De plus $a=£a<0$, donc le trinôme est <b>négatif</b> à l’extérieur des racines.',
  corr4_1: 'De plus $a=£a>0$, donc le trinôme est <b>strictement positif</b> sur $\\R$, sauf en la valeur où il s’annule.',
  corr4_2: 'De plus $a=£a<0$, donc le trinôme est <b>strictement négatif</b> sur $\\R$, sauf en la valeur où il s’annule.',
  corr5_1: 'De plus $a=£a>0$, donc le trinôme est <b>strictement positif</b> sur $\\R$.',
  corr5_2: 'De plus $a=£a<0$, donc le trinôme est <b>strictement négatif</b> sur $\\R$.',
  corr6: 'On obtient ainsi le signe du trinôme :'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = this.storage
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pEmpty(stor.div5)
    j3pEmpty(stor.div6)
    j3pEmpty(stor.objTabSigne.palette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.petit.correction.color } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1, {
      a: stor.objet_polynome.tab_coefs1[2],
      b: stor.objet_polynome.tab_coefs1[1],
      c: stor.objet_polynome.tab_coefs1[0],
      d: stor.objet_polynome.tab_coefs1[3]
    })
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    const zoneExpli3 = j3pAddElt(zoneExpli, 'div')
    if (stor.objet_polynome.tab_racines.length === 0) {
      j3pAffiche(zoneExpli2, '', textes.corr2_3)
      if (j3pCalculValeur(stor.objet_polynome.tab_coefs1[2]) > 0) {
        j3pAffiche(zoneExpli3, '', textes.corr5_1, { a: stor.objet_polynome.tab_coefs1[2] })
      } else {
        j3pAffiche(zoneExpli3, '', textes.corr5_2, { a: stor.objet_polynome.tab_coefs1[2] })
      }
    } else if (stor.objet_polynome.tab_racines.length === 1) {
      j3pAffiche(zoneExpli2, '', textes.corr2_2, { x: stor.objet_polynome.tab_racines[0] })
      if (j3pCalculValeur(stor.objet_polynome.tab_coefs1[2]) > 0) {
        j3pAffiche(zoneExpli3, '', textes.corr4_1, { a: stor.objet_polynome.tab_coefs1[2] })
      } else {
        j3pAffiche(zoneExpli3, '', textes.corr4_2, { a: stor.objet_polynome.tab_coefs1[2] })
      }
    } else {
      j3pAffiche(zoneExpli2, '', textes.corr2_1, { x: stor.objet_polynome.tab_racines[0], y: stor.objet_polynome.tab_racines[1] })
      if (j3pCalculValeur(stor.objet_polynome.tab_coefs1[2]) > 0) {
        j3pAffiche(zoneExpli3, '', textes.corr3_1, { a: stor.objet_polynome.tab_coefs1[2] })
      } else {
        j3pAffiche(zoneExpli3, '', textes.corr3_2, { a: stor.objet_polynome.tab_coefs1[2] })
      }
    }
    if (!bonneReponse) {
      const zoneExpli4 = j3pAddElt(zoneExpli, 'div')
      const zoneExpli5 = j3pAddElt(zoneExpli, 'div')
      j3pAffiche(zoneExpli4, '', textes.corr6)
      const objTabCorr = Object.assign(stor.objTab)
      objTabCorr.zero_tab = [true, true, true]
      objTabCorr.obj_style_couleur.style = me.styles.toutpetit.correction
      objTabCorr.obj_style_couleur.couleur = (bonneReponse) ? me.styles.cbien : me.styles.petit.correction.color
      objTabCorr.tab_dimension[2] = objTabCorr.val_zero_tab.length
      const leTableau = j3pAddElt(zoneExpli5, 'div', '', {
        style: me.styles.etendre('toutpetit.enonce', { position: 'relative', top: 0, left: 0 })
      })
      tableauSignesFct(leTableau, objTabCorr)
    }
  }

  function ecrireFonction (fctTxt) {
    let fctLatex = fctTxt
    const regFrac1 = /\(-?[0-9]+\)\/\([0-9]+\)/ig
    const regFrac2 = /\(-?[0-9]+\/[0-9]+\)/ig
    const regFrac3 = /[0-9]+\/[0-9]+/ig
    let num, den, i, tabSeparateur
    while (regFrac1.test(fctLatex)) {
      // on trouve (..)/(..)
      const tabFrac1 = fctLatex.match(regFrac1)
      for (i = 0; i < tabFrac1.length; i++) {
        tabSeparateur = tabFrac1[i].split('/')
        num = tabSeparateur[0].substring(1, tabSeparateur[0].length - 1)
        den = tabSeparateur[1].substring(1, tabSeparateur[1].length - 1)
        fctLatex = fctLatex.replace(tabFrac1[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    while (regFrac2.test(fctLatex)) {
      // on trouve (../..)
      const tabFrac2 = fctLatex.match(regFrac2)
      for (i = 0; i < tabFrac2.length; i++) {
        tabSeparateur = tabFrac2[i].split('/')
        num = tabSeparateur[0].substring(1, tabSeparateur[0].length)
        den = tabSeparateur[1].substring(0, tabSeparateur[1].length - 1)
        fctLatex = fctLatex.replace(tabFrac2[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    while (regFrac3.test(fctLatex)) {
      // on trouve ../..
      const tabFrac3 = fctLatex.match(regFrac3)
      for (i = 0; i < tabFrac3.length; i++) {
        tabSeparateur = tabFrac3[i].split('/')
        num = tabSeparateur[0].substring(0, tabSeparateur[0].length)
        den = tabSeparateur[1].substring(0, tabSeparateur[1].length)
        fctLatex = fctLatex.replace(tabFrac3[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    // on enlève signe * devant x
    while (fctLatex.includes('*x')) {
      fctLatex = fctLatex.replace('*x', 'x')
    }
    if (fctLatex.includes('x²')) {
      fctLatex = fctLatex.replace('x²', 'x^2')
    }
    // fonction bx^2+cx+d avec coefs au format latex
    const posSigne = [0]
    for (i = 1; i < fctLatex.length; i++) {
      if ((fctLatex[i] === '+') || (fctLatex[i] === '-')) posSigne.push(i)
    }
    posSigne.push(fctLatex.length)
    // je découpe alors l’expression à l’aide de ces signes, cela me donnera les monômes
    const tabMonome = []
    for (i = 1; i < posSigne.length; i++) {
      tabMonome.push(fctLatex.substring(posSigne[i - 1], posSigne[i]))
    }
    const tabCoefPolynome = ['0', '0', '0']
    // tabCoefPolynome[0] est le coef constant, tabCoefPolynome[1], celui devant x, ...
    // je fais un correctif si j’ai un monome avec un coef de la forme \\frac{-...}{...}
    let posI = 0
    while (posI < tabMonome.length) {
      if ((tabMonome[posI] === '\\frac{') || (tabMonome[posI] === '+\\frac{') || (tabMonome[posI] === '-\\frac{')) {
        tabMonome[posI + 1] = tabMonome[posI] + tabMonome[posI + 1]
        tabMonome.splice(posI, 1)
      } else {
        posI++
      }
    }
    for (i = 0; i < tabMonome.length; i++) {
      // tabMonome.length<=3 c’est le nombre de monomes présents
      if ((tabMonome[i].includes('x^{2}')) || (tabMonome[i].includes('x^2'))) {
        // c’est le monome ax^2
        if (tabMonome[i].charAt(0) === '+') {
          tabMonome[i] = tabMonome[i].substring(1)
        }
        if (tabMonome[i].includes('x^{2}')) {
          tabCoefPolynome[2] = tabMonome[i].substring(0, tabMonome[i].length - 5)
        } else {
          tabCoefPolynome[2] = tabMonome[i].substring(0, tabMonome[i].length - 3)
        }
        if (tabCoefPolynome[2] === '') {
          tabCoefPolynome[2] = '1'
        } else if (tabCoefPolynome[2] === '-') {
          tabCoefPolynome[2] = '-1'
        }
      } else if (tabMonome[i].includes('x')) {
        // c’est le monome ax
        if (tabMonome[i].charAt(0) === '+') {
          tabMonome[i] = tabMonome[i].substring(1)
        }
        tabCoefPolynome[1] = tabMonome[i].substring(0, tabMonome[i].length - 1)
        if (tabCoefPolynome[1] === '') {
          tabCoefPolynome[1] = '1'
        } else if (tabCoefPolynome[1] === '-') {
          tabCoefPolynome[1] = '-1'
        }
      } else {
        // c’est le monome a
        if (tabMonome[i].charAt(0) === '+') {
          tabCoefPolynome[0] = tabMonome[i].substring(1)
        } else {
          tabCoefPolynome[0] = tabMonome[i]
        }
      }
    }
    return { fctLatex, tab_coef: tabCoefPolynome }
  }

  function rechercheRacine (tab1) {
    // tab1 est un tableaux de 3 éléments représentant la fonction trinôme
    // tab1[0] est l’expression de la fonction polynomiale au format latex
    // tab1[1] est le tableau des coefficients du polynôme (lorsqu’ils existent)
    // cette fonction génère le polynôme s’il n’est pas imposé et calcule les solutions éventuelles de l’équation tab1[0]=0
    let a, b, c, d, a1, b1, c1
    const fctTxt1 = tab1[0]
    let nbSol
    if (fctTxt1 === '') {
      // la fonctions est aléatoire.
      // Dans ce cas, on écrit le trinôme différence sous la forme (ax+b)(cx+d) s’il admet 2 racines
      // on l’écrit (ax+b)^2 s’il admet 2 racines et (ax+b)^2+c (c>0) s’il n’admet pas de racine
      if (stor.nbRacinesImposees === 'alea') {
        nbSol = j3pRandomTab([0, 1, 2], [0.25, 0.125, 0.625])
      } else {
        nbSol = Number(stor.nbRacinesImposees)
      }
      // la proba d’avoir 0 racine est 1/4, celle d’en avoir 1 est 1/8 et celle d’en avoir 2 est 5/8
      if (nbSol === 0) {
        // polynôme (ax+b)^2+c=a^2x^2+2abx+b^2+c ou l’opposé
        do {
          do {
            a = j3pGetRandomInt(-3, 3)
          } while (Math.abs(a) < Math.pow(10, -12))
          do {
            b = j3pGetRandomInt(-5, 5)
          } while (Math.abs(b) < Math.pow(10, -12))
          c = j3pGetRandomInt(1, 7)
          a1 = Math.pow(a, 2)
          b1 = 2 * a * b
          c1 = Math.pow(b, 2) + c
          if (j3pGetRandomInt(0, 1) === 0) {
            a1 = -a1
            b1 = -b1
            c1 = -c1
          }
        } while (Math.abs(b1) < Math.pow(10, -12))
      } else if (nbSol === 1) {
        // polynôme (ax+b)^2=a^2x^2+2abx+b^2 ou son opposé
        do {
          a = j3pGetRandomInt(-3, 3)
        } while (Math.abs(a) < Math.pow(10, -12))
        do {
          b = j3pGetRandomInt(-5, 5)
        } while (Math.abs(b) < Math.pow(10, -12))
        a1 = Math.pow(a, 2)
        b1 = 2 * a * b
        c1 = Math.pow(b, 2)
        if (j3pGetRandomInt(0, 1) === 0) {
          a1 = -a1
          b1 = -b1
          c1 = -c1
        }
      } else {
        const choixDelta = j3pGetRandomInt(0, 2)// dans 1 cas sur 3, delta ne sera pas un carré parfait
        if (choixDelta === 0) {
          let deltaVal
          do {
            a1 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
            b1 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 9)
            c1 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 9)
            deltaVal = Math.pow(b1, 2) - 4 * a1 * c1
          } while (Math.abs(Math.sqrt(deltaVal) - Math.round(Math.sqrt(deltaVal))) < Math.pow(10, -12))
        } else {
          // (ax+b)(cx+d)=acx^2+(ad+bc)x+bd
          do {
            do {
              a = j3pGetRandomInt(-3, 3)
            } while (Math.abs(a) < Math.pow(10, -12))
            do {
              b = j3pGetRandomInt(-5, 5)
            } while (Math.abs(b) < Math.pow(10, -12))
            do {
              c = j3pGetRandomInt(-3, 3)
            } while (Math.abs(c) < Math.pow(10, -12))
            do {
              d = j3pGetRandomInt(-5, 5)
            } while (Math.abs(d) < Math.pow(10, -12))
          } while (Math.abs(Math.abs(-b / a) - Math.abs(-d / c)) < Math.pow(10, -12))
          a1 = a * c
          b1 = a * d + b * c
          c1 = b * d
        }
      }
    } else {
      a1 = (tab1[1][2] === '') ? '0' : tab1[1][2]
      b1 = (tab1[1][1] === '') ? '0' : tab1[1][1]
      c1 = (tab1[1][0] === '') ? '0' : tab1[1][0]
    }
    const poly1 = j3pGetLatexMonome(1, 2, String(a1)) + j3pGetLatexMonome(2, 1, String(b1)) + j3pGetLatexMonome(2, 0, String(c1))
    me.logIfDebug('poly1:' + poly1)
    const deltaTxt = j3pGetLatexSomme(j3pGetLatexProduit(b1, b1), j3pGetLatexProduit('-4', j3pGetLatexProduit(a1, c1)))
    const delta = j3pCalculValeur(deltaTxt)
    const tabSol = []
    const tabValSol = []
    if (Math.abs(delta) < Math.pow(10, -12)) {
      tabSol.push(j3pGetLatexQuotient(j3pGetLatexProduit('-1', b1), j3pGetLatexProduit('2', a1)))
      tabValSol.push(j3pCalculValeur(tabSol[0]))
    } else if (delta > 0) {
      // console.log("b1:",b1,"  deltaTxt:",deltaTxt,"  a1:",a1)
      tabSol.push(j3pSimplificationRacineTrinome(b1, '-', deltaTxt, a1))
      tabSol.push(j3pSimplificationRacineTrinome(b1, '+', deltaTxt, a1))
      tabValSol.push((-j3pCalculValeur(b1) - Math.sqrt(delta)) / (2 * j3pCalculValeur(a1)), (-j3pCalculValeur(b1) + Math.sqrt(delta)) / (2 * j3pCalculValeur(a1)))
    }
    me.logIfDebug('tab_racines:' + tabSol + '   tabValSol:' + tabValSol)
    return {
      polynome: poly1,
      tab_racines: tabSol,
      tab_coefs1: [c1, b1, a1, deltaTxt],
      valeur_racines: tabValSol
    }
  }

  function constructionDiv5 () {
    j3pAffiche(stor.div5, '', textes.phrase5)
    j3pAjouteBouton(stor.div5, j3pGetNewId('bouton1'), 'MepBoutons2', '+', actionBtnPlus)
  }

  function constructionDiv6 () {
    j3pAffiche(stor.div6, '', textes.phrase6)
    j3pAjouteBouton(stor.div6, j3pGetNewId('bouton2'), 'MepBoutons2', '-', actionBtnMoins)
  }

  function declarationZoneAValider () {
    // on récupère la liste avec le nom de toutes les zones de saisie
    const mesValidPerso = []
    let mesZonesSaisie = stor.objTabSigne.zone.map(elt => elt.id)
    mesZonesSaisie = mesZonesSaisie.concat(stor.objTabSigne.signe.map(elt => elt.id))
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesValidPerso
    })
    if (stor.nbValx === 0) j3pFocus(stor.objTabSigne.signe[0])
    else j3pFocus(stor.objTabSigne.zone[0])
  }

  function actionBtnPlus () {
    // c’est fonction sera appelée pour recréer le tableau de signe avec une valeur de plus dans la première ligne
    stor.nbValx++
    stor.objTab.tab_dimension[2] = stor.nbValx
    if (stor.nbValx === 5) j3pEmpty(stor.div5)
    if (stor.nbValx === 1) {
      // c’est que le div 6 n’était pas encore construit, donc on le fait.
      constructionDiv6()
    }
    // on reconstruit ensuite le tableau de signes
    j3pEmpty(stor.leTableau)
    stor.objTabSigne = tableauSignesFct(stor.leTableau, stor.objTab)
    j3pFocus(stor.objTabSigne.zone[0])
    declarationZoneAValider()
  }
  function actionBtnMoins () {
    // c’est fonction sera appelée pour recréer le tableau de signe avec une valeur de moins dans la première ligne
    stor.nbValx--
    stor.objTab.tab_dimension[2] = stor.nbValx
    if (stor.nbValx === 0) j3pEmpty(stor.div6)
    if (stor.nbValx === 4) {
      // c’est que le div 6 n’était pas encore construit, donc on le fait.
      constructionDiv5()
    }
    // on reconstruit ensuite le tableau de signes
    j3pEmpty(stor.leTableau)
    stor.objTabSigne = tableauSignesFct(stor.leTableau, stor.objTab)
    if (stor.nbValx > 0)j3pFocus(stor.objTabSigne.zone[0])
    else j3pFocus(stor.objTabSigne.signe[0])
    declarationZoneAValider()
  }

  function minTab (tab) {
    // tab est un tableau de valeurs (au format nombre ou latex)
    // cette fonction renvoie la valeur minimale (dans le format initial) des éléments de tab
    let lemin = tab[0]
    let indice = 0
    for (let i = 1; i < tab.length; i++) {
      if (j3pCalculValeur(tab[i]) < j3pCalculValeur(lemin)) {
        indice = i
        lemin = tab[i]
      }
    }
    return { min: lemin, indice }
  }

  function ordonneTabSansDoublon (tab) {
    // cette fonction renvoie un tableau contenant les éléments de tab rangés dans l’ordre croissant
    // si tab contient deux éléments égaux, un seul apparaîtra dans le tableau final
    // pour cette section, je vérifie aussi que les valeurs sont bien dans le domaine de définition
    let tabOrd = []
    const copieTab = []// je ne veux pas changer le tableau initial, d’où cette copie
    const copieTabBis = []// deuxième copie
    let i, k, j
    if (tab.length > 0) {
      for (i = 0; i < tab.length; i++) {
        if (tab[i] !== '') {
          if (String(tab[i]).includes('|')) {
            // il y a plusieurs valeurs à ajouter dans le tableau
            const tableauTab = tab[i].split('|')
            for (k = 0; k < tableauTab.length; k++) {
              if (tableauTab[k] !== '') {
                // je ne prends pas la valeur si elle n’est pas dans le domaine de def'
                copieTab.push(j3pCalculValeur(String(tableauTab[k])))
                copieTabBis.push(tableauTab[k])
              }
            }
          } else {
            // là il n’y a qu’une valeur à ajouter'
            copieTab.push(j3pCalculValeur(String(tab[i])))
            copieTabBis.push(tab[i])
          }
        }
      }
      if (copieTab.length > 0) {
        j = 0
        do {
          tabOrd[j] = copieTabBis[minTab(copieTab).indice]
          // on supprime alors dans copieTab toutes les occurences de tabOrd[j]
          let pos = 0
          while (pos < copieTabBis.length) {
            if (Math.abs(j3pCalculValeur(copieTabBis[pos]) - j3pCalculValeur(tabOrd[j])) < Math.pow(10, -12)) {
              // c’est que c’est un doublon
              copieTab.splice(pos, 1)
              copieTabBis.splice(pos, 1)
            } else {
              pos++
            }
          }
          j++
        } while (copieTab.length !== 0)
        // dans le cas de valeurs approchées, je remets la valeur exacte
        for (i = 0; i < tabOrd.length; i++) {
          j = 0
          let ok = false
          do {
            if (tab[j] !== '') {
              if (!String(tab[j]).includes('|')) {
                // une et une seule valeur
                if (Math.abs(j3pCalculValeur(String(tabOrd[i])) - j3pCalculValeur(String(tab[j]))) < Math.pow(10, -13)) {
                  tabOrd[i] = tab[j]
                  ok = true
                }
              } else {
                // plusieurs valeurs dans tab[j]
                const tableauTab = tab[j].split('|')
                for (k = 0; k < tableauTab.length; k++) {
                  if (Math.abs(j3pCalculValeur(String(tabOrd[i])) - j3pCalculValeur(String(tableauTab[k]))) < Math.pow(10, -13)) {
                    tabOrd[i] = tableauTab[k]
                    ok = true
                  }
                }
              }
            }
            j++
          } while (!ok || (j > tab.length))
        }
      } else {
        tabOrd = []
      }
      return tabOrd
    } else {
      return []
    }
  }

  function enonceInitFirst () {
    ds = me.donneesSection
    // Construction de la page
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

    me.afficheTitre(textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
  }

  function enonceMain () {
    const maFonction1 = ds.trinomes[me.questionCourante - 1]
    let fctTxt1 = ''
    let coefs1 = []

    if (maFonction1 && (maFonction1 !== '') && (maFonction1 !== 'trinome') && (maFonction1 !== 'affine') && (maFonction1 !== 'constante')) {
      // le trinôme est imposée
      const maFonctionDonnee1 = ecrireFonction(maFonction1)
      fctTxt1 = maFonctionDonnee1.fctLatex
      coefs1 = maFonctionDonnee1.tab_coef
    }
    // ce qui suit n’est valable que si le trinôme n’est pas imposé
    const nbRacinesImposees = ds.nbracines[me.questionCourante - 1]
    if ((nbRacinesImposees === 0) || (nbRacinesImposees === '0') || (nbRacinesImposees === 1) || (nbRacinesImposees === '1') || (nbRacinesImposees === 2) || (nbRacinesImposees === '2')) {
      stor.nbRacinesImposees = String(nbRacinesImposees)
    } else {
      stor.nbRacinesImposees = 'alea'
    }
    stor.objet_polynome = rechercheRacine([fctTxt1, coefs1, maFonction1])
    // cette fonction renvoie le polynôme (aléatoire ou imposé), ses coefficients (avec le discriminant au format latex)
    // la fonction donne également les racines éventuelles du trinôme (format latex et format décimal)
    // {polynome:poly1,tab_racines:tabSol,tab_coefs1:[c1,b1,a1,deltaTxt],valeur_racines:tabValSol};
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    stor.div5 = j3pAddElt(stor.conteneur, 'div')
    stor.div6 = j3pAddElt(stor.conteneur, 'div')
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons1, '', textes.consigne1, { t: stor.objet_polynome.polynome })
    // construction du tableau
    stor.largeur_tab = 95 * me.zonesElts.HG.getBoundingClientRect().width / 100
    stor.taille_ligne = 60
    stor.hauteur_tab = stor.taille_ligne * 2
    // recherche du signe du trinome (sous la forme d’un tableau vace les différents signes)
    const valCoefa = j3pCalculValeur(stor.objet_polynome.tab_coefs1[2])
    if (stor.objet_polynome.tab_racines.length === 0) {
      // aucune racine
      stor.les_signes = (valCoefa > 0) ? ['+'] : ['-']
    } else if (stor.objet_polynome.tab_racines.length === 1) {
      // une seule racine
      stor.les_signes = (valCoefa > 0) ? ['+', '+'] : ['-', '-']
    } else {
      // deux racines
      stor.les_signes = (valCoefa > 0) ? ['+', '-', '+'] : ['-', '+', '-']
    }
    stor.nbValx = 0
    // création du tableau
    stor.objTab = {
      tab_dimension: [stor.largeur_tab, stor.hauteur_tab, 0, ['-infini', '+infini']],
      zero_tab: [false, false, true],
      afficheBornes: true,
      val_zero_tab: stor.objet_polynome.tab_racines,
      signe_tab_corr: stor.les_signes,
      palette: ['fraction', 'racine'],
      obj_style_couleur: {
        style: me.styles.toutpetit.enonce,
        texte: textes.signe
      }
    }
    stor.leTableau = j3pAddElt(zoneCons2, 'div', '', {
      style: me.styles.etendre('toutpetit.enonce', { position: 'relative', top: 0, left: 0 })
    })
    stor.objTabSigne = tableauSignesFct(stor.leTableau, stor.objTab)
    constructionDiv5()
    declarationZoneAValider()

    me.logIfDebug('stor.objet_polynome.tab_racines:' + stor.objet_polynome.tab_racines + '   stor.les_signes:' + stor.les_signes)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = {}
        reponse.aRepondu = true
        reponse.bonneReponse = true
        reponse.aRepondu = fctsValid.valideReponses()
        let numErreur
        if (reponse.aRepondu) {
          numErreur = 0
          const tabRacinesOrdonnees = ordonneTabSansDoublon(stor.objet_polynome.tab_racines)
          const nbRacines = stor.objet_polynome.tab_racines.length
          if (tabRacinesOrdonnees.length !== stor.nbValx) {
          // c’est qu’il n’y a pas le bon nombre de valeurs dans la première ligne
            if (stor.nbValx > 0) {
              for (let i = 1; i < stor.nbValx; i++) {
                fctsValid.zones.bonneReponse[i - 1] = false
              }
            }
            for (let i = 1; i < stor.nbValx + 1; i++) {
              fctsValid.zones.bonneReponse[stor.nbValx + i - 1] = false
            }
            numErreur = 1
            reponse.bonneReponse = false
          } else {
            let valeur1
            if (nbRacines === 1) {
            // valeurs données
              valeur1 = j3pCalculValeur(j3pValeurde(stor.objTabSigne.zone[0]))
              const valeurAttendue = j3pCalculValeur(tabRacinesOrdonnees[0])
              me.logIfDebug('valeur1 : ' + valeur1 + '   valeurAttendue:' + valeurAttendue)
              fctsValid.zones.bonneReponse[0] = (Math.abs(valeur1 - valeurAttendue) < Math.pow(10, -12))
              if (fctsValid.zones.bonneReponse[0]) {
                reponse.bonneReponse = true
                // la racine est bonne. Vérifions les signes
                for (let i = 1; i <= stor.nbValx + 1; i++) {
                  fctsValid.zones.bonneReponse[stor.nbValx + i - 1] = (j3pValeurde(stor.objTabSigne.signe[i - 1]) === stor.les_signes[i - 1])
                  reponse.bonneReponse = (reponse.bonneReponse && fctsValid.zones.bonneReponse[stor.nbValx + i - 1])
                }
                if (!reponse.bonneReponse)numErreur = 6
              } else {
                reponse.bonneReponse = false
                for (let i = 1; i < stor.nbValx + 1; i++) fctsValid.zones.bonneReponse[stor.nbValx + i - 1] = false
                numErreur = 4
              }
            } else if (nbRacines === 2) {
              valeur1 = j3pCalculValeur(j3pValeurde(stor.objTabSigne.zone[0]))
              const valeur2 = j3pCalculValeur(j3pValeurde(stor.objTabSigne.zone[1]))
              const tabRac1Presente = [false, false]
              const tabRac2Presente = [false, false]

              if (Math.abs(valeur1 - j3pCalculValeur(tabRacinesOrdonnees[0])) < Math.pow(10, -12)) {
                tabRac1Presente[0] = true
              } else if (Math.abs(valeur1 - j3pCalculValeur(tabRacinesOrdonnees[1])) < Math.pow(10, -12)) {
                tabRac1Presente[1] = true
              }
              if (Math.abs(valeur2 - j3pCalculValeur(tabRacinesOrdonnees[0])) < Math.pow(10, -12)) {
                tabRac2Presente[0] = true
              } else if (Math.abs(valeur2 - j3pCalculValeur(tabRacinesOrdonnees[1])) < Math.pow(10, -12)) {
                tabRac2Presente[1] = true
              }
              me.logIfDebug('tabRac1Presente:' + tabRac1Presente + '   tabRac2Presente:' + tabRac2Presente)
              if (!tabRac1Presente[0]) {
                reponse.bonneReponse = false
                fctsValid.zones.bonneReponse[0] = false
                if (!tabRac2Presente[1]) {
                  fctsValid.zones.bonneReponse[1] = false
                  // aucune des deux réponses n’est correcte. Mais n’auraient-elles pas été inversées
                  if (tabRac1Presente[1] && tabRac2Presente[0]) {
                    numErreur = 8
                  } else if (tabRac1Presente[1] || tabRac2Presente[0]) {
                    numErreur = 3
                  } else {
                    numErreur = 2
                  }
                } else {
                  numErreur = 3
                }
              } else {
                fctsValid.zones.bonneReponse[0] = true
                if (!tabRac2Presente[1]) {
                  reponse.bonneReponse = false
                  fctsValid.zones.bonneReponse[1] = false
                  numErreur = 3
                } else {
                  fctsValid.zones.bonneReponse[1] = true
                }
              }
              if (reponse.bonneReponse) {
              // on teste alors les signes
                for (let i = 1; i <= stor.nbValx + 1; i++) {
                  fctsValid.zones.bonneReponse[stor.nbValx + i - 1] = (j3pValeurde(stor.objTabSigne.signe[i - 1]) === stor.les_signes[i - 1])
                  reponse.bonneReponse = (reponse.bonneReponse && fctsValid.zones.bonneReponse[stor.nbValx + i - 1])
                }
                if (!reponse.bonneReponse) numErreur = 5
              } else {
              // on met tous les signes à false
                for (let i = 1; i < stor.nbValx + 1; i++) fctsValid.zones.bonneReponse[stor.nbValx + i - 1] = false
              }
            } else {
              fctsValid.zones.bonneReponse[0] = (j3pValeurde(stor.objTabSigne.signe[0]) === stor.les_signes[0])
              reponse.bonneReponse = fctsValid.zones.bonneReponse[0]
              if (!reponse.bonneReponse) {
              // pas de racine mais une erreur dans le signe
                numErreur = 7
              }
            }
          }
          fctsValid.coloreLesZones()
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          stor.zoneCorr.style.color = me.styles.cfaux
          stor.zoneCorr.innerHTML = textes.tableau_incomplet
          // On peut ajouter un commentaire particulier.
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (numErreur === 1) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment1
              } else if (numErreur === 2) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment2
              } else if (numErreur === 3) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment3
              } else if (numErreur === 4) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment4
              } else if (numErreur === 5) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment5
              } else if (numErreur === 6) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment6
              } else if (numErreur === 7) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment7
              } else if (numErreur === 8) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment8
              }
              if ((numErreur >= 5) && (numErreur <= 7)) {
                me.essaiCourant = ds.nbchances
                stor.zoneCorr.innerHTML += '<br>' + textes.comment10
                fctsValid.coloreLesZones()
              }

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
              // on compte le nombre de fois où il y a un pb sur les racines du trinôme
                if (numErreur <= 4) {
                  me.typederreurs[3]++
                }
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe1
        } else {
          const nbErreurs = ds.nbitems - me.score
          if (me.typederreurs[3] / nbErreurs >= ds.seuilerreur) {
            // la proportion de pb de forme (non reconnaissance de la forme canonique) est supé au égal au seul fixé
            me.parcours.pe = ds.pe_2
          } else {
            me.parcours.pe = ds.pe_3
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
