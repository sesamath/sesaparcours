import { j3pAddElt, j3pNombre, j3pFocus, j3pMathquillXcas, j3pMonome, j3pRandomTab, j3pGetBornesIntervalle, j3pGetRandomInt, j3pGetNewId, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexMonome, j3pGetLatexProduit, j3pSimplificationRacineTrinome, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import Repere from 'src/legacy/outils/geometrie/Repere'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    janvier 2015
    Dans cette section, on donne la forme canonique d’une fonction pol de degré 2 et on demande de donner l’axe de symétrie et les coordonnées du sommet
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[-5;5]', 'string', 'coefficient a dans l’écriture a(x-alpha)^2+beta. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;4]|[-4;-2]" pour qu’il soit égal à 2 dans un premier temps, positif ensuite puis négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier ou fractionnaire.'],
    ['alpha', '[-5;5]', 'string', 'coefficient alpha dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['beta', '[-5;5]', 'string', 'coefficient beta dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['forme_init', ['canonique', 'canonique', 'canonique'], 'array', 'Pour chaque répétition, l’élément du tableau vaut "canonique", "factorisee" ou "developpee" suivant la forme de la fonction donnée.']
  ]

}
const textes = {
  titre_exo: 'Forme canonique, axe de symétrie et sommet',
  consigne1: 'On considère une fonction $£f$ définie sur $\\R$ par<br>$£g$.',
  consigne2: 'La courbe représentant $£f$ admet pour axe de symétrie la droite d’équation &1&.',
  consigne3: 'Le sommet de cette courbe a de plus pour coordonnées :<br>$($&1&$;$&2&$)$.',
  comment1: 'Pour qu’on ait une équation de droite, il faut une égalité !',
  comment2: 'Les coordonnées du sommet ne sont pas correctes !',
  comment3: 'Attention : l’équation donnée est celle d’une droite horizontale et non verticale !',
  // corrigé pour la forme canonique
  corr1: '$£f(x)$ est écrit sous forme canonique $a(x-\\alpha)^2+\\beta$ avec $a=£a$, $\\alpha=£b$ et $\\beta=£c$.<br>La parabole représentant $£f$ a donc pour axe de symétrie la droite d’équation $x=\\alpha$, c’est-à-dire $x=£b$.',
  // Corrigé pour la forme développée
  corr1_2: "$£f(x)$ est sous la forme $ax^2+bx+c$ où $a=£a$, $b=£b$ et $c=£c$. Elle s'écrit sous forme canonique $a(x-\\alpha)^2+\\beta$ où $\\alpha=\\frac{-b}{2a}=£d$ et $\\beta=f(\\alpha)=£e$.<br>La parabole représentant $£f$ a donc pour axe de symétrie la droite d’équation $x=\\alpha$, c’est-à-dire $x=£d$.",
  // corrigé pour la forme factorisée
  corr1_3: "$£f(x)$ est sous la forme $a(x-x_1)(x-x_2)$ où $x_1=£x$ et $x_2=£y$. $x_1$ et $x_2$ sont alors les deux solutions de l’équation $£f(x)=0$. Ainsi $£f(x)$ s'écrit sous forme canonique $a(x-\\alpha)^2+\\beta$ où $\\alpha=\\frac{x_1+x_2}{2}=£d$ (propriété de symétrie de la parabole) et $\\beta=f(\\alpha)=£e$.<br>La parabole représentant $£f$ a donc pour axe de symétrie la droite d’équation $x=\\alpha$, c’est-à-dire $x=£d$.",
  corr2: 'De plus le sommet de cette parabole a pour coordonnées $(\\alpha;\\beta)$, c’est-à-dire $(£a;£b)$.',
  courbe: 'Courbe représentative de la fonction'
}
/**
 * section canonique_symetrie2
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    if (stor.forme_init === 'canonique') {
      j3pAffiche(zoneExpli1, '', textes.corr1 + '\n',
        {
          f: stor.nomF,
          a: stor.val_a,
          c: stor.val_beta,
          b: stor.val_alpha
        })
    } else if (stor.forme_init === 'developpee') {
      j3pAffiche(zoneExpli1, '', textes.corr1_2 + '\n',
        {
          f: stor.nomF,
          a: stor.val_a,
          c: stor.coefCTxt,
          b: stor.coefBTxt,
          d: stor.val_alpha,
          e: stor.val_beta
        })
    } else if (stor.forme_init === 'factorisee') {
      let valX1 = stor.x1Txt
      let valX2 = stor.x2Txt
      if ((stor.x1Txt.includes('sqrt')) && (!stor.x1Txt.includes('frac'))) {
        valX1 = stor.x2Txt
        valX2 = stor.x1Txt
      }
      j3pAffiche(zoneExpli1, '', textes.corr1_3 + '\n',
        {
          f: stor.nomF,
          a: stor.val_a,
          x: valX1,
          y: valX2,
          d: stor.val_alpha,
          e: stor.val_beta
        })
    }
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli2, '', '\n' + textes.corr2,
      {
        a: stor.val_alpha,
        b: stor.val_beta
      })
    // affichage de la courbe
    creationCourbe()
    j3pToggleFenetres('Courbe')
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).includes('|')) {
      const tabA = texteIntervalle.split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (testIntervalleFermeEntiers.test(tabA[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(tabA[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if (!tabA[k] || (tabA[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(tabA[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tabA[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(tabA[k]))) {
            // on a un nombre
            tabIntervalle.push(tabA[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if (!texteIntervalle || (texteIntervalle === '')) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (testIntervalleFermeEntiers.test(texteIntervalle)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    return tabIntervalle
  }
  function lgIntervalle (intervalle) {
    // cette fonction renvoie la longueur de l’intervalle'
    // si ce n’est pas un intervalle, alors elle renvoie 0'
    let lg = 0
    if (testIntervalleFermeEntiers.test(intervalle)) {
      // on a bien un intervalle
      const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
      const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
      lg = borneSup - borneInf
    }
    return lg
  }
  function creationCourbe () {
    const posCourbe = me.zonesElts.HD.getBoundingClientRect().height + 85
    const largeurRep = 400
    const hauteurRep = 320
    const fenetreCourbe = j3pGetNewId('fenetreCourbe')
    me.fenetresjq = [
      { name: 'Courbe', title: textes.courbe, top: posCourbe, left: me.zonesElts.MG.offsetWidth + 10, width: largeurRep + 30, id: fenetreCourbe }
    ]
    // Création mais sans affichage.
    j3pCreeFenetres(me)
    // j3pToggleFenetres("Boutons");

    // création de la courbe :
    const coursCourbe = j3pAddElt(fenetreCourbe, 'div')
    stor.unRepere = j3pGetNewId('unrepere')
    const posYO = hauteurRep / 2
    const posXO = largeurRep / 2
    me.repere = new Repere({
      idConteneur: coursCourbe,
      idDivRepere: stor.unRepere,
      aimantage: true,
      visible: true,
      trame: true,
      fixe: true,
      larg: largeurRep,
      haut: hauteurRep,

      pasdunegraduationX: 1,
      pixelspargraduationX: 20,
      pasdunegraduationY: 1,
      pixelspargraduationY: 20,
      xO: posXO,
      yO: posYO,
      debuty: 0,
      negatifs: true,
      objets: [
        { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: -10, par3: 10, par4: largeurRep, style: { couleur: '#F00', epaisseur: 2 } }
      ]
    })

    me.repere.construit()
    coursCourbe.style.position = 'relative'
  }

  // console.log("debut du code : ",me.etat)
  function enonceMain () {
    if ((ds.forme_init[me.questionCourante - 1] === 'developpée') || (ds.forme_init[me.questionCourante - 1] === 'developpee') || (ds.forme_init[me.questionCourante - 1] === 'développée') || (ds.forme_init[me.questionCourante - 1] === 'développé') || (ds.forme_init[me.questionCourante - 1] === 'developpe') || (ds.forme_init[me.questionCourante - 1] === 'developpé') || (ds.forme_init[me.questionCourante - 1] === 'développee')) {
      stor.forme_init = 'developpee'
    } else if ((ds.forme_init[me.questionCourante - 1] === 'factorisée') || (ds.forme_init[me.questionCourante - 1] === 'factorisee') || (ds.forme_init[me.questionCourante - 1] === 'factorisé') || (ds.forme_init[me.questionCourante - 1] === 'factorise')) {
      stor.forme_init = 'factorisee'
    } else {
      stor.forme_init = 'canonique'
    }
    const nomF = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
    stor.nomF = nomF
    const alpha = String(stor.coef_alpha[me.questionCourante - 1])
    const beta = String(stor.coef_beta[me.questionCourante - 1])
    if ((stor.coef_a[me.questionCourante - 1] === '[0;0]') || (stor.coef_a[me.questionCourante - 1] === '0')) {
      j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à a de valoir 0', { vanishAfter: 5 })
      stor.coef_a[me.questionCourante - 1] = '[-4;4]'
    }
    const leA = String(stor.coef_a[me.questionCourante - 1])
    let coefAlphaTxt
    let coefBetaTxt
    let coefATxt
    me.logIfDebug('leA:' + leA + '   alpha:' + alpha + '   beta:' + beta)
    let pbFormeFact = false
    let nbPbFormeFact = 0
    let coeffA, coeffAlpha, coeffBeta
    let pb2
    do {
      pbFormeFact = false
      coeffA = 0
      if (leA.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffA = j3pNombre(leA.split('|')[1])
        coefATxt = leA.split('|')[0]
      } else if (!isNaN(j3pNombre(leA))) {
        coeffA = j3pNombre(leA)
      } else {
        const [lemin, lemax] = j3pGetBornesIntervalle(leA)
        coeffA = j3pGetRandomInt(lemin, lemax)
      }
      coeffAlpha = 0
      coeffBeta = 0
      if (alpha.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffAlpha = j3pNombre(alpha.split('|')[1])
        coefAlphaTxt = alpha.split('|')[0]
        // je vais aussi chercher les valeurs de alpha-1 et alpha+1
        const tabCoefAlpha = coefAlphaTxt.split('}{')
        const monNum = tabCoefAlpha[0].substring(tabCoefAlpha[0].indexOf('frac') + 5, tabCoefAlpha[0].length)
        const monDen = tabCoefAlpha[1].substring(0, tabCoefAlpha[1].length - 1)
        const alpha1Num = (coeffAlpha >= 0) ? (Math.round(j3pNombre(monNum) - j3pNombre(monDen))) : (Math.round(-j3pNombre(monNum) - j3pNombre(monDen)))
        const alpha2Num = (coeffAlpha >= 0) ? (Math.round(j3pNombre(monNum) + j3pNombre(monDen))) : (Math.round(-j3pNombre(monNum) + j3pNombre(monDen)))
        stor.val_inf = (alpha1Num >= 0) ? '\\frac{' + alpha1Num + '}{' + monDen + '}' : '-\\frac{' + Math.abs(alpha1Num) + '}{' + monDen + '}'
        stor.val_sup = (alpha2Num >= 0) ? '\\frac{' + alpha2Num + '}{' + monDen + '}' : '-\\frac{' + Math.abs(alpha2Num) + '}{' + monDen + '}'
      } else if (!isNaN(j3pNombre(alpha))) {
        coeffAlpha = j3pNombre(alpha)
        stor.val_inf = coeffAlpha - 1
        stor.val_sup = coeffAlpha + 1
      } else {
        const [lemin, lemax] = j3pGetBornesIntervalle(alpha)
        coeffAlpha = j3pGetRandomInt(lemin, lemax)
        stor.val_inf = coeffAlpha - 1
        stor.val_sup = coeffAlpha + 1
      }
      let pb1
      do {
        if (beta.includes('frac')) {
          // c’est un nb en écriture fractionnaire'
          coeffBeta = j3pNombre(beta.split('|')[1])
          coefBetaTxt = beta.split('|')[0]
        } else if (!isNaN(j3pNombre(beta))) {
          coeffBeta = j3pNombre(beta)
        } else {
          const [lemin, lemax] = j3pGetBornesIntervalle(beta)
          coeffBeta = j3pGetRandomInt(lemin, lemax)
        }
        pb1 = (coeffAlpha === coeffBeta) && !((alpha === beta) && (lgIntervalle(alpha) === 0))
      } while (pb1)
      if ((((coeffA < 0) && (coeffBeta < 0)) || ((coeffA > 0) && (coeffBeta > 0))) && (stor.forme_init === 'factorisee')) {
        pbFormeFact = true
        nbPbFormeFact++// si nbPbFormeFact dépasse 50, c’est qu’on a imposé des coefs qui ne peuvent pas donner une forme factorisée
        if (nbPbFormeFact >= 50) {
          j3pShowError('avec les coefficients imposés, la forme factorisée n’existe pas !', { vanishAfter: 5 })
        }
      }
      pb2 = ((coeffA === 0) || ((coeffAlpha === 0) && ((alpha !== '[0;0]') && (alpha !== '0') && (alpha !== 0))) || ((coeffBeta === 0) && ((beta !== '[0;0]') && (beta !== '0') && (beta !== 0)))) || (pbFormeFact && (nbPbFormeFact < 50))
    } while (pb2)
    let ecritureA
    let monCoefATxt
    if (leA.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
      if (coeffA > 0) {
        ecritureA = coefATxt
        monCoefATxt = coefATxt
      } else {
        ecritureA = '-' + coefATxt
        monCoefATxt = '-' + coefATxt
      }
    } else {
      if (coeffA === -1) {
        ecritureA = '-'
      } else if (coeffA === 1) {
        ecritureA = ''
      } else {
        ecritureA = coeffA
      }
      coefATxt = coeffA
      monCoefATxt = coeffA
    }
    let ecritureBeta = j3pMonome(2, 0, coeffBeta)
    let ecritureAlpha = j3pMonome(2, 0, -coeffAlpha)
    let monCoefAlphaTxt = ecritureAlpha
    let monCoefBetaTxt = ecritureBeta
    if (alpha.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
      if (coeffAlpha < 0) {
        ecritureAlpha = '+' + coefAlphaTxt
        monCoefAlphaTxt = coefAlphaTxt
      } else {
        ecritureAlpha = '-' + coefAlphaTxt
        monCoefAlphaTxt = j3pGetLatexProduit('-1', coefAlphaTxt)
      }
    } else {
      coefAlphaTxt = Math.abs(coeffAlpha)
    }
    if (beta.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
      if (coeffBeta > 0) {
        ecritureBeta = '+' + coefBetaTxt
        monCoefBetaTxt = coefBetaTxt
      } else {
        ecritureBeta = '-' + coefBetaTxt
        monCoefBetaTxt = '-' + coefBetaTxt
      }
    } else {
      coefBetaTxt = Math.abs(coeffBeta)
    }
    me.logIfDebug(
      'ecritureA:' + ecritureA + '   ecritureAlpha:' + ecritureAlpha + '   ecritureBeta:' + ecritureBeta,
      '\nmonCoefATxt:' + monCoefATxt + '   monCoefAlphaTxt:' + monCoefAlphaTxt + '   monCoefBetaTxt:' + monCoefBetaTxt
    )
    const maFormeCano = (coeffAlpha === 0)
      ? ecritureA + 'x^2' + ecritureBeta
      : ecritureA + '\\left(x' + ecritureAlpha + '\\right)^2' + ecritureBeta
    stor.maFormeCano = maFormeCano
    // coefs utiles pour la forme développée
    const coefBTxt = j3pGetLatexProduit(monCoefATxt, j3pGetLatexProduit(monCoefAlphaTxt, '2'))
    const coefCTxt = j3pGetLatexSomme(j3pGetLatexProduit(monCoefATxt, j3pGetLatexProduit(monCoefAlphaTxt, monCoefAlphaTxt)), monCoefBetaTxt)
    stor.forme_dev = j3pGetLatexMonome(1, 2, monCoefATxt) + j3pGetLatexMonome(2, 1, coefBTxt) + j3pGetLatexMonome(3, 0, coefCTxt)
    // coefs utiles pour la forme factorisée
    stor.coefATxt = monCoefATxt
    stor.coefBTxt = coefBTxt
    stor.coefCTxt = coefCTxt
    const deltaTxt = j3pGetLatexSomme(j3pGetLatexProduit(coefBTxt, coefBTxt), j3pGetLatexProduit(j3pGetLatexProduit('-4', monCoefATxt), coefCTxt))
    const deltaVal = j3pCalculValeur(deltaTxt)
    if (deltaVal > 0) {
      // deux racines
      const x1Txt = j3pSimplificationRacineTrinome(coefBTxt, '+', deltaTxt, monCoefATxt)
      const x2Txt = j3pSimplificationRacineTrinome(coefBTxt, '-', deltaTxt, monCoefATxt)
      stor.x1Txt = x1Txt
      stor.x2Txt = x2Txt
      if (x1Txt.includes('sqrt')) {
        if (x1Txt.includes('frac')) {
          // on a une racinde de la forme (-b+sqrt(delta))/2a
          stor.forme_fact = ecritureA + '\\left(x-' + x1Txt + '\\right)\\left(x-' + x2Txt + '\\right)'
        } else {
          // le dénominatateur vaut 1, donc on n’a plus que -b+sqrt(delta)
          if (x1Txt.charAt(0) === '-') {
            // là il faut faire attention
            stor.forme_fact = ecritureA + '\\left(x+' + x1Txt.substring(1) + '\\right)\\left(x+' + x2Txt.substring(1) + '\\right)'
          } else {
            stor.forme_fact = ecritureA + '\\left(x-' + x1Txt + '\\right)\\left(x-' + x2Txt + '\\right)'
          }
        }
      } else {
        stor.forme_fact = ecritureA + '\\left(x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', x1Txt)) + '\\right)\\left(x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', x2Txt)) + '\\right)'
      }
    } else if (Math.abs(deltaVal) < Math.pow(10, -12)) {
      stor.forme_fact = maFormeCano
    } else {
      if (stor.forme_init === 'factorisee') {
        j3pShowError('Il n’y a pas de forme factorisée', { vanishAfter: 5 })
      }
    }
    me.logIfDebug('stor.forme_dev:' + stor.forme_dev + '   stor.forme_fact:' + stor.forme_fact)
    // écriture de la fonction polynôme sous forme canonique
    me.stockage[10] = maFormeCano
    if (stor.forme_init === 'developpee') {
      me.stockage[10] = stor.forme_dev
    } else if (stor.forme_init === 'factorisee') {
      me.stockage[10] = stor.forme_fact
    }
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1,
      {
        f: nomF,
        g: nomF + '(x)=' + me.stockage[10]
      })
    const elt1 = j3pAffiche(stor.zoneCons2, '', textes.consigne2,
      {
        f: nomF,
        inputmq1: { texte: '' }
      })
    stor.zoneInput = [elt1.inputmqList[0]]
    mqRestriction(stor.zoneInput[0], '\\d,./+-*=xy')
    stor.coeffAlpha = coeffAlpha
    stor.coeffBeta = coeffBeta
    const elt2 = j3pAffiche(stor.zoneCons3, '', textes.consigne3,
      {
        f: nomF,
        inputmq1: { texte: '' },
        inputmq2: { texte: '' }
      })
    stor.zoneInput.push(elt2.inputmqList[0], elt2.inputmqList[1])
    mqRestriction(stor.zoneInput[1], '\\d,./+-')
    mqRestriction(stor.zoneInput[2], '\\d,./+-')
    stor.zoneInput[0].typeReponse = ['texte']
    stor.zoneInput[1].typeReponse = ['nombre', 'exact']
    stor.zoneInput[2].typeReponse = ['nombre', 'exact']
    stor.zoneInput[1].reponse = coeffAlpha
    stor.zoneInput[2].reponse = coeffBeta
    j3pFocus(stor.zoneInput[0])
    stor.coeffA = coeffA
    stor.coeffAlpha = coeffAlpha
    stor.coeffBeta = coeffBeta
    stor.val_a = coefATxt
    stor.val_alpha = (coeffAlpha >= 0) ? coefAlphaTxt : '-' + coefAlphaTxt
    stor.val_alpha_oppose = (coeffAlpha > 0) ? '-' + coefAlphaTxt : '+' + coefAlphaTxt
    stor.val_beta = (coeffBeta >= 0) ? coefBetaTxt : '-' + coefBetaTxt
    stor.fonction = coeffA + '*(x-(' + coeffAlpha + '))^2+(' + coeffBeta + ')'
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.petit.correction })

    const mesZonesSaisie = stor.zoneInput.map(eltInput => eltInput.id)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: [stor.zoneInput[0].id] })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection

        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

        // par convention, me.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        stor.coef_a = constructionTabIntervalle(ds.a, '[-5;5]')
        stor.coef_alpha = constructionTabIntervalle(ds.alpha, '[-5;5]')
        stor.coef_beta = constructionTabIntervalle(ds.beta, '[-5;5]')

        me.logIfDebug('stor.coef_alpha:' + stor.coef_alpha)
      } else {
        // A chaque répétition, on nettoie la scène
        j3pDetruitFenetres('Courbe')
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        let egalitePresente = true
        let bonneCoord, confusionXY, bonneRep1
        if (reponse.aRepondu) {
        // pour l’instant, on n’a testé que les coordonnées du sommet'
          bonneCoord = reponse.bonneReponse
          // il faut aussi que je teste l’équation de la droite'
          const reponseEleve1 = j3pValeurde(stor.zoneInput[0])
          egalitePresente = (reponseEleve1.split('=').length === 2)
          confusionXY = false// pour vérifier si l’élève n’a pas écrit y=alpha au lieu de x=alpha
          if (egalitePresente) {
            confusionXY = !!(((reponseEleve1 === 'y=' + stor.val_alpha) || (reponseEleve1 === stor.val_alpha + '=y') || (reponseEleve1 === 'y' + stor.val_alpha_oppose + '=0') || (reponseEleve1 === '0=y' + stor.val_alpha_oppose) || (reponseEleve1 === stor.val_alpha + '-y=0') || (reponseEleve1 === '0=' + stor.val_alpha + '-y')))
            const tabEgalite = reponseEleve1.split('=')
            const expressDif = tabEgalite[0] + '-(' + tabEgalite[1] + ')-(x-(' + stor.coeffAlpha + '))'

            me.logIfDebug('expressDif:' + expressDif)
            // validation de la zone de saisie1
            const arbre1 = new Tarbre(j3pMathquillXcas(expressDif), ['x', 'y'])
            bonneRep1 = true
            for (let j = 0; j < 5; j++) {
              bonneRep1 = (bonneRep1 && (Math.abs(arbre1.evalue([stor.coeffAlpha, j])) < Math.pow(10, -12)))
            }
            me.logIfDebug('reponseEleve1:' + reponseEleve1 + '   bonneRep1:' + bonneRep1 + '  alpha:' + stor.coeffAlpha + '  beta:' + stor.coeffBeta)
            if (!bonneRep1) fctsValid.zones.bonneReponse[0] = false
          } else {
            fctsValid.zones.bonneReponse[0] = false
            bonneRep1 = false
          }
          fctsValid.coloreUneZone(stor.zoneInput[0].id)
          reponse.bonneReponse = (reponse.bonneReponse && bonneRep1)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (!egalitePresente) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment1
              } else if (confusionXY) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment3
              }
              if (!bonneCoord) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment2
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++
                afficheCorrection(false)
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }

      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
