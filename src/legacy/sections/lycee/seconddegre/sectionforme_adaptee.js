import { j3pNombre, j3pEmpty, j3pShuffle, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pPaletteMathquill, j3pPGCD, j3pRandomTab, j3pStyle, j3pGetBornesIntervalle, j3pAddElt, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexMonome, j3pGetLatexProduit, j3pGetLatexQuotient, j3pGetLatexSomme, j3pExtraireNumDen } from 'src/legacy/core/functionsLatex'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    février 2015
    Dans cette section, on donne un polynôme de degré 2 sous forme canonique, développée (voire factorisée si possible).
    On demande alors de calculer quelques images.
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['nbetapes', 3, 'entier', 'Nombre d’images à calculer par répétition'],
    ['type_quest', 'image', 'liste', 'On peut demander de faire des calculs d’images (écrire "image"), de résoudre une équation (écrire "equation") ou de déterminer des antécédents (écrire "antecedent").<br>Dans les 2 derniers cas, nbetapes sera forcément égal à 3 et on aura toujours la forme factorisée (ajout_fact vaudra 1 ou 2).', ['image', 'antecedent', 'equation']],
    ['ajout_fact', 0, 'entier', 'Il vaut 0 si on ne donne pas la forme factorisée (dans les calculs d’images). Dans ce cas on détermine les valeurs a, alpha et beta ;<br>1 si la forme factorisée s’écrit (ax+b)(cx+d). Dans ce cas on détermine a, b, c et d ;<br>2 si la forme factorisée s’écrit a(x+b)(x+d).Dans ce cas on détermine a, b et d.'],
    ['a', '[-5;5]', 'string', 'coefficient a de l’écriture a(x-alpha)^2+beta ou ax^2+bx+c ou a(x+b)(x+d) ou (ax+b)(cx+d). On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;4]|[-4;-2]" pour qu’il soit égal à 2 dans un premier temps, positif ensuite puis négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier ou fractionnaire.'],
    ['alpha', '[-5;5]', 'string', 'coefficient alpha dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['beta', '[-5;5]', 'string', 'coefficient beta dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['b', '[-5;5]', 'string', 'coefficient b dans l’écriture (ax+b)(cx+d) ou a(a+b)(x+d). Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['c', '[-5;5]', 'string', 'coefficient c dans l’écriture (ax+b)(cx+d). Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['d', '[-5;5]', 'string', 'coefficient d dans l’écriture (ax+b)(cx+d) ou a(a+b)(x+d). Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['imposer_nbs', '[]', 'string', 'Pour chaque répétition et chaque étape, on peut imposer le nombre dont on cherche l’image. Par exemple avec "[1,2/3,-5]|[0,-racine(2),1/2]|", on demande l’image de 1, 2/3 puis -5 par la première fonction, puis l’image de 0, -racine(2) et 1/2 par la 2ème. Pour la 3ème, c’est aléatoire. ']

  ]
}
const textes = {
  titre_exo: 'Forme la plus adaptée',
  consigne1: 'On considère une fonction $£f$ définie sur $\\R$.',
  consigne2: 'Cette fonction s’exprime de £n façons : ',
  consigne3: 'Après avoir cliqué sur la forme qui paraît la plus adaptée, déterminer l’image de $£x$ par $£f$.',
  consigne4: 'Après avoir cliqué sur la forme qui paraît la plus adaptée, donner la (ou les) solution(s) de l’équation $£f(x)=£x$. (S’il y en a plusieurs, les séparer par un point-virgule.)',
  consigne5: 'Après avoir cliqué sur la forme qui paraît la plus adaptée, donner le (ou les) antécédent(s) de $£x$ par $£f$. (S’il y en a plusieurs, les séparer par un point-virgule.)',
  comment1: 'Sélectionne la forme choisie pour calculer l’image !',
  comment1bis: 'Sélectionne la forme choisie pour résoudre l’équation !',
  comment1ter: 'Sélectionne la forme choisie pour déterminer le (ou les) antécédent(s) !',
  comment2: 'Pourtant la forme choisie n’est pas la plus judicieuse pour ce calcul d’image !',
  comment2bis: 'Pourtant la forme choisie n’est pas la plus judicieuse pour résoudre cette équation !',
  comment2ter: 'Pourtant la forme choisie n’est pas la plus judicieuse pour déterminer le (ou les) antécédents de ce nombre !',
  comment3: 'L’expression choisie n’est pas la plus pratique pour effectuer ce calcul d’image.',
  comment3bis: 'L’expression choisie n’est pas la plus pratique pour résoudre cette équation.',
  comment3ter: 'L’expression choisie n’est pas la plus pratique pour déterminer le (ou les) antécédents de ce nombre.',
  comment4: 'Les propositions ne sont pas toutes différentes&nbsp;!',
  comment5: 'Il manque une solution à l’équation&nbsp;!',
  comment6: 'Il manque un antécédent&nbsp;!',
  comment7: 'La réponse donnée doit être simplifiée&nbsp;!',
  comment8: 'Les fractions doivent être simplifiées&nbsp;!',
  comment9: 'Je ne comprends pas ce qui est écrit&nbsp;!',
  corr1: 'La forme canonique est très efficace pour ce calcul :',
  corr2: 'La forme développée est très efficace pour ce calcul :',
  corr3: 'La forme factorisée est très efficace pour ce calcul :',
  corr4: 'La forme développée est peut-être encore celle qui rend les calculs les moins difficiles :',
  corr5: 'La forme factorisée est très efficace pour cette équation :',
  corr6: 'La forme développée est très efficace pour cette équation :',
  corr7: 'La forme canonique est très efficace pour cette équation :',
  corr8: 'Déterminer les antécédents de $£x$ par $£f$ revient à résoudre l’équation $£f(x)=£x$. ',
  corr9: 'L’équation devient $£e$ c’est-à-dire $£f$.',
  corr9bis: 'L’équation devient $£e$.',
  corr10: 'On obtient $£e$ et donc $£f$.',
  corr10bis: 'On obtient $£e$ ou $£f$ et donc $£g$ ou $£h$.',
  corr11: 'La solution de l’équation $£f(x)=£x$ est donc $£s$.',
  corr11bis: 'L’antécédent de $£x$ par $£f$ est donc $£s$.',
  corr12: 'Les solutions de l’équation $£f(x)=£x$ sont donc $£s$ et $£t$.',
  corr12bis: 'Les antécédents de $£x$ par $£f$ sont donc $£s$ et $£t$.',
  corr13: 'Voici les calculs avec la forme développée :',
  corr13bis: 'Voici les calculs avec la forme canonique :',
  corr13ter: 'Voici les calculs avec la forme factorisée :'
}
/**
 * section forme_adaptee
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function surbrillanceSurvol () {
    if (me.etat === 'correction') {
      const numDiv = this.numero
      if (stor.divselect !== numDiv) {
        this.style.backgroundColor = 'rgba(200,200,200,0.5)'
      }
    }
  }
  function surbrillanceSelect () {
    if (me.etat === 'correction') {
      const numDiv = this.numero
      if (stor.divselect !== numDiv) {
        // d’abord je remets 'sans fond' tous les div'
        for (let i = 2; i <= 1 + stor.nb_formes; i++) {
          stor['zoneCons' + i].style.backgroundColor = 'rgba(200,200,200,0)'
        }
        this.style.backgroundColor = 'rgba(200,200,200,1)'
        stor.divselect = numDiv
        j3pFocus(stor.zoneInput)
      } else {
        this.style.backgroundColor = 'rgba(200,200,200,0)'
        stor.divselect = 0
        stor.zoneInput.blur()
      }
    }
  }
  function surbrillanceSuppr () {
    if (me.etat === 'correction') {
      const numDiv = this.numero
      if (stor.divselect !== numDiv) {
        this.style.backgroundColor = 'rgba(200,200,200,0)'
      }
    }
  }
  function EcrireCoefDansCalc (rang, puis, coef) {
    // rang est la position dans laquelle apparait le coef dans le calcul
    // puis est important seulement s’il vaut 0 (dans ce cas, même si coef vaut 1, il faudra l’écrire)'
    // coef est sa valeur (au format latex s’il est fractionnaire)'
    let expressCoef
    if (!String(coef).includes('frac')) {
      // coef est un nombre non fractionnaire
      if (Math.abs(j3pNombre(String(coef))) < Math.pow(10, -12)) {
        // coef vaut 0
        if (rang === 1) {
          expressCoef = '0'
        } else {
          expressCoef = '+0'
        }
      } else if (Math.abs(j3pNombre(String(coef)) - 1) < Math.pow(10, -12)) { // coef vaut 1
        if (rang === 1) {
          if (puis === 0) {
            expressCoef = '1'
          } else {
            expressCoef = ''
          }
        } else {
          if (puis === 0) {
            expressCoef = '+1'
          } else {
            expressCoef = '+'
          }
        }
      } else if (Math.abs(j3pNombre(String(coef)) + 1) < Math.pow(10, -12)) { // coef vaut -1
        if (puis === 0) {
          expressCoef = '-1'
        } else {
          expressCoef = '-'
        }
      } else { // dans tous les autres cas
        if ((rang > 1) && (j3pNombre(String(coef)) > 0)) {
          expressCoef = '+' + coef
        } else {
          expressCoef = coef
        }
      }
    } else { // on a une fraction
      const tabCoef = j3pExtraireNumDen(coef)
      if (tabCoef[1].charAt(0) === '-') {
        // coef est négatif
        expressCoef = '-\\frac{' + tabCoef[1].substring(1) + '}{' + tabCoef[2] + '}'
      } else {
        if (rang === 1) {
          expressCoef = coef
        } else {
          expressCoef = '+' + coef
        }
      }
    }
    return expressCoef
  }
  function calcMonome (rang, puis, coef, valeurX) {
    // rang est la position du monome
    // puis est la puissance à laquelle x est élevé
    // coef est la valeur du coefficient du monome (éventuellement de la forme \\frac{...}{...}
    // valeurX est le nombre qui va remplacer x (éventuellement de la forme \\frac{...}{...}
    // la fonction renvoie le calcul bien écrit, avec les parenthèses si besoin
    const ecritureCoef = EcrireCoefDansCalc(rang, puis, coef)
    let ecritureCalc = ecritureCoef
    if (String(valeurX).includes('frac')) {
      // valeurX est une fraction
      if (puis === 1) {
        if ((ecritureCoef === '+') || (ecritureCoef === '-') || (ecritureCoef === '')) {
          ecritureCalc += valeurX
        } else {
          ecritureCalc += '\\times ' + valeurX
        }
      } else if (puis >= 2) {
        if ((ecritureCoef === '+') || (ecritureCoef === '-') || (ecritureCoef === '')) {
          ecritureCalc += '\\left(' + valeurX + '\\right)^{' + puis + '}'
        } else {
          ecritureCalc += '\\times\\left(' + valeurX + '\\right)^{' + puis + '}'
        }
      }
    } else { // c’est un nombre entier décimal ou avec un radical'
      if (puis === 1) {
        if ((ecritureCoef === '+') || (ecritureCoef === '-') || (ecritureCoef === '')) {
          if (j3pNombre(String(valeurX)) >= 0) {
            ecritureCalc += valeurX
          } else {
            ecritureCalc += '\\left(' + valeurX + '\\right)'
          }
        } else {
          if (j3pNombre(String(valeurX)) >= 0) {
            ecritureCalc += '\\times ' + valeurX
          } else {
            ecritureCalc += '\\times \\left(' + valeurX + '\\right)'
          }
        }
      } else if (puis >= 2) {
        if ((ecritureCoef === '+') || (ecritureCoef === '-') || (ecritureCoef === '')) {
          if (j3pNombre(String(valeurX)) >= 0) {
            ecritureCalc += valeurX + '^{' + puis + '}'
          } else {
            ecritureCalc += '\\left(' + valeurX + '\\right)^{' + puis + '}'
          }
        } else {
          if (j3pNombre(String(valeurX)) >= 0) {
            ecritureCalc += '\\times ' + valeurX + '^{' + puis + '}'
          } else {
            ecritureCalc += '\\times\\left(' + valeurX + '\\right)^{' + puis + '}'
          }
        }
      }
    }
    return ecritureCalc
  }
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    // var pos_y = stor.pos_palette;
    j3pEmpty(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.petit.correction.color } })
    let texteCorr1 = ''
    let typeForme = 'dev'
    if (ds.type_quest === 'image') {
      if (Math.abs(stor.valeurX) < Math.pow(10, -12)) {
        // valeurX vaut 0 donc on prend la forme développée
        texteCorr1 = textes.corr2
      } else if (stor.meilleure_forme === 2) {
        // valeurX vaut alpha, donc on prend la forme canonique
        texteCorr1 = textes.corr1
        typeForme = 'cano'
      } else if (((ds.ajout_fact === 1) || (ds.ajout_fact === 2)) && (stor.meilleure_forme === 4)) {
        // la fonction est factorisable et valeurX est un des deux zéros de la fonction
        texteCorr1 = textes.corr3
        typeForme = 'fact'
      } else if (stor.meilleure_forme === 3) {
        // je prends la forme développée
        texteCorr1 = textes.corr4
      } else {
        // on prend la forme sélectionnée par l’élève
        if (stor.divselect === 2) {
          // forme canonique
          texteCorr1 = textes.corr13bis
          typeForme = 'cano'
        } else if (stor.divselect === 4) {
          // forme factorisée
          texteCorr1 = textes.corr13ter
          typeForme = 'fact'
        } else {
          // forme développée
          texteCorr1 = textes.corr13
        }
      }
    } else {
      if ((ds.type_quest !== 'equation') && (ds.type_quest !== 'équation')) {
        texteCorr1 = textes.corr8 + ' '
      }
      if (stor.meilleure_forme === 2) {
        // valeurX vaut beta, donc on prend la forme canonique
        texteCorr1 += textes.corr7
      } else if (stor.meilleure_forme === 4) {
        // valeurX vaut 0 donc on prend la forme factorisée
        texteCorr1 += textes.corr5
      } else if (stor.meilleure_forme === 3) {
        // dans tous les autres cas, je prends la forme développée
        texteCorr1 += textes.corr6
      }
    }
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', texteCorr1,
      {
        f: stor.nomFct,
        x: stor.leX
      })
    let ligne1, ligne2
    const resultat = []
    if (ds.type_quest === 'image') {
      if (typeForme === 'dev') {
        ligne1 = calcMonome(1, 2, stor.moncoef_a[1], stor.leX) + calcMonome(2, 1, stor.moncoef_b[1], stor.leX) + calcMonome(3, 0, stor.moncoefC[1], stor.leX)
        // attention au cas de figure avec des racines carrées qui doit être traité à part
        if (String(stor.leX).includes('sqrt')) {
          const tabRadical = extraireRadical(stor.leX)
          if (tabRadical[0] === '') {
            resultat[0] = j3pGetLatexProduit(stor.moncoef_a[1], tabRadical[1])
            resultat[1] = EcrireCoefDansCalc(2, 0, stor.moncoef_b[1]) + '\\sqrt{' + tabRadical[1] + '}'
          } else {
            resultat[0] = j3pGetLatexProduit(stor.moncoef_a[1], j3pGetLatexProduit(j3pGetLatexProduit(tabRadical[0], tabRadical[0]), tabRadical[1]))
            resultat[1] = EcrireCoefDansCalc(2, 0, j3pGetLatexProduit(stor.moncoef_b[1], tabRadical[0])) + '\\sqrt{' + tabRadical[1] + '}'
          }
          resultat[2] = j3pGetLatexSomme(resultat[0], stor.moncoefC[1]) + resultat[1]
          ligne2 = EcrireCoefDansCalc(1, 0, resultat[0]) + resultat[1] + EcrireCoefDansCalc(3, 0, stor.moncoefC[1]) + '=' + resultat[2]
        } else {
          resultat[0] = j3pGetLatexProduit(stor.moncoef_a[1], j3pGetLatexProduit(stor.leX, stor.leX))
          resultat[1] = j3pGetLatexProduit(stor.moncoef_b[1], stor.leX)
          resultat[2] = j3pGetLatexSomme(j3pGetLatexSomme(resultat[0], resultat[1]), stor.moncoefC[1])
          // ligne2 = calcMonome(1, 0, resultat[0],0)+calcMonome(2, 0, resultat[1],0)+calcMonome(3, 0, stor.moncoefC[1], stor.leX)+"="+resultat[2];
          ligne2 = EcrireCoefDansCalc(1, 0, resultat[0]) + EcrireCoefDansCalc(2, 0, resultat[1]) + EcrireCoefDansCalc(3, 0, stor.moncoefC[1]) + '=' + resultat[2]
        }
      } else if (typeForme === 'cano') {
        const ecritureBeta = j3pGetLatexProduit(stor.signeBeta, stor.monbeta)
        ligne1 = EcrireCoefDansCalc(1, 2, stor.moncoef_a[1]) + '\\left(' + EcrireCoefDansCalc(1, 0, stor.leX) + EcrireCoefDansCalc(2, 0, j3pGetLatexProduit(-1, stor.monalpha)) + '\\right)^2' + EcrireCoefDansCalc(2, 0, ecritureBeta)
        if (stor.meilleure_forme === 2) {
          // c’est qu’on cherche l’image du nb qui annule le carré'
          ligne2 = calcMonome(1, 2, stor.moncoef_a[1], 0) + EcrireCoefDansCalc(2, 0, ecritureBeta) + '=' + EcrireCoefDansCalc(1, 0, ecritureBeta)
        } else {
          // on cherche l’image d’un nombre qui n’annule pas le carré'
          const calcCarre = j3pGetLatexProduit(j3pGetLatexSomme(stor.leX, j3pGetLatexProduit(-1, stor.monalpha)), j3pGetLatexSomme(stor.leX, j3pGetLatexProduit(-1, stor.monalpha)))
          const resultatCalc = j3pGetLatexSomme(j3pGetLatexProduit(stor.moncoef_a[1], calcCarre), ecritureBeta)
          ligne2 = calcMonome(1, 2, stor.moncoef_a[1], j3pGetLatexSomme(stor.leX, j3pGetLatexProduit(-1, stor.monalpha))) + EcrireCoefDansCalc(2, 0, ecritureBeta) + '=' + calcMonome(1, 1, stor.moncoef_a[1], calcCarre) + EcrireCoefDansCalc(2, 0, ecritureBeta) + '=' + resultatCalc
        }
      } else { // c’est la forme factorisée
        let facteur1, facteur2
        if (ds.ajout_fact === 1) {
          ligne1 = '\\left(' + calcMonome(1, 1, stor.coefaTxt, stor.leX) + EcrireCoefDansCalc(2, 0, stor.coefbTxt) + '\\right)\\left(' + calcMonome(1, 1, stor.coefCTxt, stor.leX) + EcrireCoefDansCalc(2, 0, stor.coefDTxt) + '\\right)'
          facteur1 = j3pGetLatexSomme(j3pGetLatexProduit(stor.coefaTxt, stor.leX), stor.coefbTxt)
          facteur2 = j3pGetLatexSomme(j3pGetLatexProduit(stor.coefCTxt, stor.leX), stor.coefDTxt)
          if (stor.meilleure_forme === 4) {
            ligne2 = calcMonome(1, 1, facteur1, facteur2) + ' = 0'
          } else {
            ligne2 = calcMonome(1, 1, facteur1, facteur2) + '=' + j3pGetLatexProduit(facteur1, facteur2)
          }
        } else {
          // sous entendu stor.ajout_frac === 2
          ligne1 = EcrireCoefDansCalc(1, 1, stor.coefaTxt) + '\\left(' + EcrireCoefDansCalc(1, 0, stor.leX) + EcrireCoefDansCalc(2, 0, stor.coefbTxt) + '\\right)\\left(' + EcrireCoefDansCalc(1, 0, stor.leX) + EcrireCoefDansCalc(2, 0, stor.coefDTxt) + '\\right)'
          facteur1 = j3pGetLatexSomme(stor.leX, stor.coefbTxt)
          facteur2 = j3pGetLatexSomme(stor.leX, stor.coefDTxt)
          if (String(facteur2).indexOf('frac') > -1) {
            if (stor.meilleure_forme === 4) {
              ligne2 = calcMonome(1, 1, stor.coefaTxt, facteur1) + '  \\times ' + facteur2 + '= 0'
            } else {
              ligne2 = calcMonome(1, 1, stor.coefaTxt, facteur1) + '  \\times ' + facteur2 + '=' + j3pGetLatexProduit(j3pGetLatexProduit(stor.coefaTxt, facteur1), facteur2)
            }
          } else {
            if (!isNaN(j3pNombre(String(facteur2)))) {
              // facteur2 est un nombre. Il faut que je fasse attention s’il est positif ou négatif'
              if (j3pNombre(String(facteur2)) < 0) {
                if (stor.meilleure_forme === 4) {
                  ligne2 = calcMonome(1, 1, stor.coefaTxt, facteur1) + '  \\times (' + facteur2 + ')= 0'
                } else {
                  ligne2 = calcMonome(1, 1, stor.coefaTxt, facteur1) + '  \\times (' + facteur2 + ')=' + j3pGetLatexProduit(j3pGetLatexProduit(stor.coefaTxt, facteur1), facteur2)
                }
              } else {
                if (stor.meilleure_forme === 4) {
                  ligne2 = calcMonome(1, 1, stor.coefaTxt, facteur1) + '  \\times ' + facteur2 + '= 0'
                } else {
                  ligne2 = calcMonome(1, 1, stor.coefaTxt, facteur1) + '  \\times ' + facteur2 + '=' + j3pGetLatexProduit(j3pGetLatexProduit(stor.coefaTxt, facteur1), facteur2)
                }
              }
            }
          }
        }
      }
      j3pAffiche(stor.zoneExpli2, '', '$' + stor.nomFct + '\\left(' + stor.leX + '\\right)=' + ligne1 + '$')
      j3pAffiche(stor.zoneExpli3, '', '$' + stor.nomFct + '\\left(' + stor.leX + '\\right)=' + ligne2 + '$')
    } else {
      const eqTab = []
      // le style n’est pas le même suivant que la réponse soit bonne ou fausse'
      if (stor.meilleure_forme === 2) {
        // forme canonique
        eqTab[1] = stor.ecritureA + '\\left(x' + stor.ecritureAlpha + '\\right)^2=0'
        eqTab[2] = '\\left(x' + stor.ecritureAlpha + '\\right)^2=0'
        eqTab[3] = 'x' + stor.ecritureAlpha + '=0'
        eqTab[4] = 'x=' + stor.monalpha
        if (stor.ecritureA === '') {
          j3pAffiche(stor.zoneExpli2, '', textes.corr9bis,
            {
              e: eqTab[2]
            })
        } else {
          j3pAffiche(stor.zoneExpli3, '', textes.corr9,
            {
              e: eqTab[1],
              f: eqTab[2]
            })
        }
        j3pAffiche(stor.zoneExpli4, '', textes.corr10,
          {
            e: eqTab[3],
            f: eqTab[4]
          })
      } else if (stor.meilleure_forme === 3) {
        // forme développée
        eqTab[1] = j3pGetLatexMonome(1, 2, stor.moncoef_a[1]) + j3pGetLatexMonome(2, 1, stor.moncoef_b[1]) + '=0'
        eqTab[2] = 'x\\left(' + j3pGetLatexMonome(1, 1, stor.moncoef_a[1]) + EcrireCoefDansCalc(2, 0, stor.moncoef_b[1]) + '\\right)=0'
        eqTab[3] = 'x=0'
        eqTab[4] = j3pGetLatexMonome(1, 1, stor.moncoef_a[1]) + EcrireCoefDansCalc(2, 0, stor.moncoef_b[1]) + '=0'
        eqTab[5] = 'x=' + j3pGetLatexQuotient(j3pGetLatexProduit(-1, stor.moncoef_b[1]), stor.moncoef_a[1])
        j3pAffiche(stor.zoneExpli2, '', textes.corr9,
          {
            e: eqTab[1],
            f: eqTab[2]
          })
        j3pAffiche(stor.zoneExpli3, '', textes.corr10bis,
          {
            e: eqTab[3],
            f: eqTab[4],
            g: eqTab[3],
            h: eqTab[5]
          })
      } else if (stor.meilleure_forme === 4) {
        // forme factorisée
        eqTab[1] = stor.maformeFact.substring(5) + '=0'
        if (ds.ajout_fact === 1) {
          // (ax+b)(cx+d)=0
          eqTab[2] = j3pGetLatexMonome(1, 1, stor.coefaTxt) + j3pGetLatexMonome(2, 0, stor.coefbTxt) + '=0'
          eqTab[3] = j3pGetLatexMonome(1, 1, stor.coefCTxt) + j3pGetLatexMonome(2, 0, stor.coefDTxt) + '=0'
          eqTab[4] = 'x=' + j3pGetLatexQuotient(j3pGetLatexProduit(-1, stor.coefbTxt), stor.coefaTxt)
          eqTab[5] = 'x=' + j3pGetLatexQuotient(j3pGetLatexProduit(-1, stor.coefDTxt), stor.coefCTxt)
        } else {
          // a(x+b)(x+d)=0
          eqTab[2] = 'x' + j3pGetLatexMonome(2, 0, stor.coefbTxt) + '=0'
          eqTab[3] = 'x' + j3pGetLatexMonome(2, 0, stor.coefDTxt) + '=0'
          eqTab[4] = 'x=' + j3pGetLatexProduit(-1, stor.coefbTxt)
          eqTab[5] = 'x=' + j3pGetLatexProduit(-1, stor.coefDTxt)
        }
        j3pAffiche(stor.zoneExpli2, '', textes.corr9bis,
          {
            e: eqTab[1]
          })
        j3pAffiche(stor.zoneExpli3, '', textes.corr10bis,
          {
            e: eqTab[2],
            f: eqTab[3],
            g: eqTab[4],
            h: eqTab[5]
          })
      }
      // le style n’est pas le même suivant que la réponse soit bonne ou fausse'
      if (stor.meilleure_forme === 2) {
        // il y a une seule solution
        if ((ds.type_quest === 'equation') || (ds.type_quest === 'équation')) {
          j3pAffiche(stor.zoneExpli5, '', textes.corr11,
            {
              f: stor.nomFct,
              x: stor.leX,
              s: stor.tab_solutions[0]
            })
        } else {
          j3pAffiche(stor.zoneExpli5, '', textes.corr11bis,
            {
              f: stor.nomFct,
              x: stor.leX,
              s: stor.tab_solutions[0]
            })
        }
      } else {
        // il y a 2 solutions
        if ((ds.type_quest === 'equation') || (ds.type_quest === 'équation')) {
          j3pAffiche(stor.zoneExpli4, '', textes.corr12,
            {
              f: stor.nomFct,
              x: stor.leX,
              s: stor.tab_solutions[0],
              t: stor.tab_solutions[1]
            })
        } else {
          j3pAffiche(stor.zoneExpli4, '', textes.corr12bis,
            {
              f: stor.nomFct,
              x: stor.leX,
              s: stor.tab_solutions[0],
              t: stor.tab_solutions[1]
            })
        }
      }
    }
  }
  function extraireRadical (nb) {
    // nb peut être sous la forme \\sqrt{...} ou bien ...\\sqrt{...}
    // la fonction renvoie un tableau à 2 éléments. Le premier est le coef devant la radical et le 2nd le nb sous le radical
    const radicalReg = /[0-9]*\\sqrt{[0-9]+}/g // new RegExp('[0-9]{0,}\\\\sqrt\\{[0-9]{1,}\\}', 'ig')
    const tabRadical = []
    if (radicalReg.test(nb)) {
      const tabRac = nb.split('\\sqrt{')
      tabRadical[0] = tabRac[0]
      if (tabRadical[0] === '-') {
        tabRadical[0] = '-1'
      }
      tabRadical[1] = tabRac[1].substring(0, tabRac[1].length - 1)
    } else {
      j3pShowError('le nombre ' + nb + ' n’est pas de la forme racine (b) ou a*racine(b)', { vanishAfter: 5 })
    }
    return tabRadical
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    // si le nombre est en écriture fractionnaire, alors on perd l’éventuel signe -'
    const intervalleReg = testIntervalleFermeEntiers // new RegExp('\\[\\-?[0-9]{1,};\\-?[0-9]{1,}\\]', 'i')
    const nbFracReg = /-?[0-9]+\/[0-9]+/ // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).includes('|')) {
      const taba = String(texteIntervalle).split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (intervalleReg.test(taba[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(taba[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if ((taba[k] === undefined) || (taba[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(taba[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = taba[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(taba[k]))) {
            // on a un nombre
            tabIntervalle.push(taba[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if ((texteIntervalle === '') || (texteIntervalle === undefined)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (intervalleReg.test(texteIntervalle)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    return tabIntervalle
  }
  function lgIntervalle (intervalle) {
    // cette fonction renvoie la longueur de l’intervalle'
    // si ce n’est pas un intervalle, alors elle renvoie 0'
    let lg = 0
    if (testIntervalleFermeEntiers.test(intervalle)) {
      // on a bien un intervalle
      const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
      const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
      lg = borneSup - borneInf
    }
    return lg
  }
  function generationRadical () {
    let radicalTxt, radical, radical2
    if (Math.abs(j3pGetRandomInt(0, 1)) < Math.pow(10, -12)) {
      // je choisis \\sqrt{...}
      do {
        radical = j3pGetRandomInt(2, 7)
      } while (Math.abs(Math.sqrt(radical) - Math.round(Math.sqrt(radical))) < Math.pow(10, -12))
      radicalTxt = '\\sqrt{' + radical + '}'
    } else {
      // je choisis ...\\sqrt{...}
      let val1
      do {
        do {
          radical2 = j3pGetRandomInt(2, 7)
        } while (Math.abs(Math.sqrt(radical2) - Math.round(Math.sqrt(radical2))) < Math.pow(10, -12))
        val1 = j3pGetRandomInt(2, 5)
      } while (Math.abs(radical2 - val1) < Math.pow(10, -12))
      const plusmoins1 = j3pGetRandomInt(0, 1) * 2 - 1
      val1 = plusmoins1 * val1
      radicalTxt = val1 + '\\sqrt{' + radical2 + '}'
    }
    return radicalTxt
  }
  function generationFraction () {
    let num, den
    do {
      num = j3pGetRandomInt(1, 8)
      den = j3pGetRandomInt(2, 8)
    } while (j3pPGCD(num, den) !== 1)
    num = num * (j3pGetRandomInt(0, 1) * 2 - 1)
    return '\\frac{' + num + '}{' + den + '}'
  }
  function valPresente (nb, tab) {
    // cette fonction vérifie si nb est déjà présent dans un tableau de valeurs
    // nb tout comme tab[i] peut être écrit sousla forme d’un nombre (entier ou décimal), d’une fraction a/b ou \\frac{a}{b} ou avec un radical a*racine(b) ou a*\\sqrt{b}'
    let newNb = j3pMathquillXcas(nb)
    while (newNb.includes('sqrt')) {
      newNb = newNb.replace('sqrt', 'racine')
    }
    let estPresent = false
    for (let k = 0; k < tab.length; k++) {
      if (tab[k] !== '') {
        let newVal = j3pMathquillXcas(tab[k])
        while (newVal.includes('sqrt')) {
          newVal = newVal.replace('sqrt', 'racine')
        }
        const arbreDif = new Tarbre(newVal + '-(' + newNb + ')', [])
        const maDif = arbreDif.evalue([])
        if (Math.abs(maDif) < Math.pow(10, -12)) {
          estPresent = true// c’est que dans ce cas, nb est déjà dans le tableau'
        }
      }
    }
    return estPresent
  }

  function texteVal (leNb) {
    // leNb est issu de construction intervalle. Il peut être sous la forme d’un tableau avec \\frac{...}{...}|valeur ou seulement sous la forme d’un nombre'
    // il revoie la valeur de leNb et son écriture au format latex
    let coeffNb// valeur de nb (forme décimale éventuellement arrondie)
    let coefNbTxt// forme latex
    if (leNb.includes('frac')) {
      // c’est un nb en écriture fractionnaire'
      coeffNb = j3pNombre(leNb.split('|')[1])
      coefNbTxt = leNb.split('|')[0]
    } else if (!isNaN(j3pNombre(leNb))) {
      coeffNb = j3pNombre(leNb)
      coefNbTxt = coeffNb
    } else {
      const [borneInf, borneSup] = j3pGetBornesIntervalle(leNb)
      coeffNb = j3pGetRandomInt(borneInf, borneSup)
      coefNbTxt = coeffNb
    }
    return { valeur: coeffNb, nb_latex: coefNbTxt }
  }

  function enonceMain () {
    if ((me.questionCourante % ds.nbetapes === 1) || (ds.nbetapes === 1)) {
      // on ne génère la fonction qu'à chaque répétition'
      // Cela reste la même d’une étape à l’autre'
      const nomFct = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
      stor.nomFct = nomFct
      if ((stor.coef_a[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '[0;0]') || (stor.coef_a[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '0') || (stor.coef_a[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === 0)) {
        j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à a de valoir 0', { vanishAfter: 5 })
        stor.coef_a[Math.floor((me.questionCourante - 1) / ds.nbetapes)] = '[-5;5]'
      }
      const leA = String(stor.coef_a[Math.floor((me.questionCourante - 1) / ds.nbetapes)])
      let coefaTxt
      let coefbTxt
      let coefCTxt
      let coefDTxt
      let coefAlphaTxt
      let coefBetaTxt
      const tabZeros = []
      const tabValZeros = []
      let coeffA = 0
      let coeffB = 0
      let coeffC = 0
      let coeffD = 0
      let coeffAlpha = 0
      let coeffBeta = 0
      let alpha, beta
      if (ds.type_quest !== 'image') {
        // on demande donc les solutions de l’équation ou la recherche d’anétécédents'
        // dans ce cas, nbetapes vaut entre 1 et 3 (par défaut 2)
        if (ds.nbetapes > 3) {
          ds.nbetapes = 2
        }
        // ajout_fact doit valoir 1 ou 2
        if ((ds.ajout_fact !== 1) && (ds.ajout_fact !== 2) && (ds.ajout_fact !== '1') && (ds.ajout_fact !== '2')) {
          // pb il faut que je lui donne la valeur 1 ou 2
          ds.ajout_fact = j3pGetRandomInt(1, 2)
        }
      }
      let leD, leB
      if (ds.ajout_fact === 1) {
        // on donne la forme factorisée sous la forme (ax+b)(cx+d)
        if ((stor.coefC[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '[0;0]') || (stor.coefC[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '0') || (stor.coefC[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === 0)) {
          j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à c de valoir 0', { vanishAfter: 5 })
          stor.coefC[Math.floor((me.questionCourante - 1) / ds.nbetapes)] = '[-5;5]'
        }
        const leC = String(stor.coefC[Math.floor((me.questionCourante - 1) / ds.nbetapes)])
        if ((stor.coef_d[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '[0;0]') || (stor.coef_d[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '0') || (stor.coef_d[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === 0)) {
          j3pShowError('d ne peut pas prendre la valeur 0 : en effet, dans ce cas les formes factorisée et canonique sont aussi efficaces pour l’image de 0', { vanishAfter: 5 })
          stor.coef_d[Math.floor((me.questionCourante - 1) / ds.nbetapes)] = '[-5;5]'
        }
        if ((stor.coef_b[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '[0;0]') || (stor.coef_b[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '0') || (stor.coef_b[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === 0)) {
          j3pShowError('b ne peut pas prendre la valeur 0 : en effet, dans ce cas les formes factorisée et canonique sont aussi efficaces pour l’image de 0', { vanishAfter: 5 })
          stor.coef_b[Math.floor((me.questionCourante - 1) / ds.nbetapes)] = '[-5;5]'
        }
        leD = String(stor.coef_d[Math.floor((me.questionCourante - 1) / ds.nbetapes)])
        leB = String(stor.coef_b[Math.floor((me.questionCourante - 1) / ds.nbetapes)])
        let valZero0, valZero1, monAlphaCoef
        do {
          // je génère les coefficients a, b, c et d
          const genereA = texteVal(leA)
          coeffA = genereA.valeur
          coefaTxt = genereA.nb_latex
          const genereB = texteVal(leB)
          coeffB = genereB.valeur
          coefbTxt = genereB.nb_latex
          const genereC = texteVal(leC)
          coeffC = genereC.valeur
          coefCTxt = genereC.nb_latex
          const genereD = texteVal(leD)
          coeffD = genereD.valeur
          coefDTxt = genereD.nb_latex
          valZero0 = 0
          valZero1 = 0
          if (coeffA !== 0) {
            valZero0 = -coeffB / coeffA
          }
          if (coeffC !== 0) {
            valZero1 = -coeffD / coeffC
          }
          monAlphaCoef = -(coeffA * coeffD + coeffB * coeffC) / (2 * coeffA * coeffD)
        } while ((coeffA === 0) || (coeffB === 0) || (coeffC === 0) || (coeffD === 0) || (Math.abs(valZero0 - valZero1) < Math.pow(10, -12)) || (Math.abs(monAlphaCoef) < Math.pow(10, -12)))
        tabZeros[0] = j3pGetLatexQuotient(j3pGetLatexProduit(-1, coefbTxt), coefaTxt)
        tabZeros[1] = j3pGetLatexQuotient(j3pGetLatexProduit(-1, coefDTxt), coefCTxt)
        tabValZeros[0] = -coeffB / coeffA
        tabValZeros[1] = -coeffD / coeffC

        me.logIfDebug('coef_a:' + coeffA + '   coef_b:' + coeffB + '   coefC:' + coeffC + '   coef_d:' + coeffD)
        me.logIfDebug('coefaTxt:' + coefaTxt + '   coefbTxt:' + coefbTxt + '   coefCTxt:' + coefCTxt + '   coefDTxt:' + coefDTxt)
        const denAlpha = j3pGetLatexProduit(j3pGetLatexProduit(-2, coefaTxt), coefCTxt)
        const numAlpha = j3pGetLatexSomme(j3pGetLatexProduit(coefaTxt, coefDTxt), j3pGetLatexProduit(coefbTxt, coefCTxt))
        coefAlphaTxt = j3pGetLatexQuotient(numAlpha, denAlpha)
        coefBetaTxt = j3pGetLatexProduit(j3pGetLatexSomme(j3pGetLatexProduit(coefaTxt, coefAlphaTxt), coefbTxt), j3pGetLatexSomme(j3pGetLatexProduit(coefCTxt, coefAlphaTxt), coefDTxt))
        coeffAlpha = (coeffA * coeffD + coeffB * coeffC) / (-2 * coeffA * coeffC)
        coeffBeta = (coeffA * coeffAlpha + coeffB) * (coeffC * coeffAlpha + coeffD)
        stor.coefaTxt = coefaTxt
        stor.coefbTxt = coefbTxt
        stor.coefCTxt = coefCTxt
        stor.coefDTxt = coefDTxt
      } else if (ds.ajout_fact === 2) {
        // on donne la forme factorisée sous la forme a(x+b)(x+d)
        if ((stor.coef_d[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '[0;0]') || (stor.coef_d[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '0') || (stor.coef_d[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === 0)) {
          j3pShowError('d ne peut pas prendre la valeur 0 : en effet, dans ce cas les formes factorisée et canonique sont aussi efficaces pour l’image de 0', { vanishAfter: 5 })
          stor.coef_d[Math.floor((me.questionCourante - 1) / ds.nbetapes)] = '[-5;5]'
        }
        if ((stor.coef_b[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '[0;0]') || (stor.coef_b[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '0') || (stor.coef_b[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === 0)) {
          j3pShowError('b ne peut pas prendre la valeur 0 : en effet, dans ce cas les formes factorisée et canonique sont aussi efficaces pour l’image de 0', { vanishAfter: 5 })
          stor.coef_b[Math.floor((me.questionCourante - 1) / ds.nbetapes)] = '[-5;5]'
        }
        leD = String(stor.coef_d[Math.floor((me.questionCourante - 1) / ds.nbetapes)])
        leB = String(stor.coef_b[Math.floor((me.questionCourante - 1) / ds.nbetapes)])
        let valZero0, valZero1, monAlphaCoef
        do {
          // je génère les coefficients a, b et d
          const genereA = texteVal(leA)
          coeffA = genereA.valeur
          coefaTxt = genereA.nb_latex
          const genereB = texteVal(leB)
          coeffB = genereB.valeur
          coefbTxt = genereB.nb_latex
          const genereD = texteVal(leD)
          coeffD = genereD.valeur
          coefDTxt = genereD.nb_latex
          valZero0 = -coeffB
          valZero1 = -coeffD
          monAlphaCoef = -(coeffD + coeffB) / 2
        } while ((coeffA === 0) || (coeffB === 0) || (coeffD === 0) || (Math.abs(valZero0 - valZero1) < Math.pow(10, -12)) || (Math.abs(monAlphaCoef) < Math.pow(10, -12)))
        tabZeros[0] = j3pGetLatexProduit(-1, coefbTxt)
        tabZeros[1] = j3pGetLatexProduit(-1, coefDTxt)
        tabValZeros[0] = -coeffB
        tabValZeros[1] = -coeffD

        me.logIfDebug('coef_a:' + coeffA + '   coef_b:' + coeffB + '   coef_d:' + coeffD)
        me.logIfDebug('coefaTxt:' + coefaTxt + '   coefbTxt:' + coefbTxt + '   coefDTxt:' + coefDTxt)
        coefAlphaTxt = j3pGetLatexQuotient(j3pGetLatexSomme(coefbTxt, coefDTxt), -2)
        coefBetaTxt = j3pGetLatexProduit(coefaTxt, j3pGetLatexProduit(j3pGetLatexSomme(coefAlphaTxt, coefbTxt), j3pGetLatexSomme(coefAlphaTxt, coefDTxt)))
        coeffAlpha = (coeffD + coeffB) / (-2)
        coeffBeta = coeffA * (coeffAlpha + coeffB) * (coeffAlpha + coeffD)
        stor.coefaTxt = coefaTxt
        stor.coefbTxt = coefbTxt
        stor.coefDTxt = coefDTxt
      } else {
        // on définit la fonction sous forme canonique. La forme factorisée n’est pas donnée'
        alpha = String(stor.coefAlpha[Math.floor((me.questionCourante - 1) / ds.nbetapes)])
        beta = String(stor.coef_beta[Math.floor((me.questionCourante - 1) / ds.nbetapes)])
        if ((stor.coefAlpha[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '[0;0]') || (stor.coefAlpha[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '0') || (stor.coefAlpha[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === 0)) {
          j3pShowError('pour que l’écriture demandée ait un sens, on n’impose pas à alpha de valoir 0', { vanishAfter: 5 })
          stor.coefAlpha[Math.floor((me.questionCourante - 1) / ds.nbetapes)] = '[-5;5]'
        }

        me.logIfDebug('leA:' + leA + '   alpha:' + alpha + '   beta:' + beta)
        let pbAlea
        do {
          if (leA.includes('frac')) {
            // c’est un nb en écriture fractionnaire'
            coeffA = j3pNombre(leA.split('|')[1])
            coefaTxt = leA.split('|')[0]
          } else if (!isNaN(j3pNombre(leA))) {
            coeffA = j3pNombre(String(leA))
            coefaTxt = coeffA
          } else {
            const [borneInf, borneSup] = j3pGetBornesIntervalle(leA)
            coeffA = j3pGetRandomInt(borneInf, borneSup)
            coefaTxt = coeffA
          }
          if (alpha.includes('frac')) {
            // c’est un nb en écriture fractionnaire'
            coeffAlpha = j3pNombre(alpha.split('|')[1])
            coefAlphaTxt = alpha.split('|')[0]
          } else if (!isNaN(j3pNombre(alpha))) {
            coeffAlpha = j3pNombre(alpha)
            coefAlphaTxt = coeffAlpha
          } else {
            const [borneInf, borneSup] = j3pGetBornesIntervalle(alpha)
            coeffAlpha = j3pGetRandomInt(borneInf, borneSup)
            coefAlphaTxt = coeffAlpha
          }
          let pb
          do {
            if (beta.includes('frac')) {
              // c’est un nb en écriture fractionnaire'
              coeffBeta = j3pNombre(beta.split('|')[1])
              coefBetaTxt = beta.split('|')[0]
            } else if (!isNaN(j3pNombre(beta))) {
              coeffBeta = j3pNombre(beta)
              coefBetaTxt = coeffBeta
            } else {
              const [borneInf, borneSup] = j3pGetBornesIntervalle(beta)
              coeffBeta = j3pGetRandomInt(borneInf, borneSup)
              coefBetaTxt = coeffBeta
            }
            pb = (coeffAlpha === coeffBeta) && !((alpha === beta) && (lgIntervalle(alpha) === 0))
          } while (pb)
          pbAlea = ((coeffA === 0) || ((coeffAlpha === 0) && ((alpha !== '[0;0]') && (alpha !== '0') && (alpha !== 0))) || ((coeffBeta === 0) && ((beta !== '[0;0]') && (beta !== '0') && (beta !== 0))))
        } while (pbAlea)
      }
      let ecritureA
      let vraiCoefA// celui-ci récupère le signe avec coefaTxt (pour la forme canonique)
      if (ds.ajout_fact === 1) {
        ecritureA = j3pGetLatexProduit(coefaTxt, coefCTxt)
        vraiCoefA = ecritureA
        ecritureA = (ecritureA === '1') ? '' : (ecritureA === '-1') ? '-' : ecritureA
      } else {
        if (leA.includes('frac')) {
          // c’est un nb en écriture fractionnaire'
          if (coeffA < 0) {
            ecritureA = '-' + coefaTxt
            vraiCoefA = '-' + coefaTxt
          } else {
            ecritureA = coefaTxt
            vraiCoefA = coefaTxt
          }
        } else {
          if (coeffA === -1) {
            ecritureA = '-'
          } else if (coeffA === 1) {
            ecritureA = ''
          } else {
            ecritureA = coeffA
          }
          coefaTxt = coeffA
          vraiCoefA = coefaTxt
        }
      }
      let signeBeta = 1
      let ecritureAlpha, ecritureBeta
      if ((ds.ajout_fact === 1) || (ds.ajout_fact === 2)) {
        if (coeffAlpha < 0) {
          ecritureAlpha = '+' + j3pGetLatexProduit(-1, coefAlphaTxt)
        } else {
          ecritureAlpha = '-' + coefAlphaTxt
        }
        if (coeffBeta > 0) {
          ecritureBeta = '+' + coefBetaTxt
        } else {
          ecritureBeta = '-' + j3pGetLatexProduit(-1, coefBetaTxt)
        }
      } else {
        ecritureBeta = j3pMonome(2, 0, coeffBeta)
        ecritureAlpha = j3pMonome(2, 0, -coeffAlpha)
        if (alpha.includes('frac')) {
          // c’est un nb en écriture fractionnaire'
          if (coeffAlpha < 0) {
            ecritureAlpha = '+' + coefAlphaTxt
          } else {
            ecritureAlpha = '-' + coefAlphaTxt
          }
        } else {
          coefAlphaTxt = Math.abs(coeffAlpha)
        }
        if (beta.includes('frac')) {
          // c’est un nb en écriture fractionnaire'
          if (coeffBeta > 0) {
            ecritureBeta = '+' + coefBetaTxt
          } else {
            signeBeta = -1
            ecritureBeta = '-' + coefBetaTxt
          }
        } else {
          if (coeffBeta < 0) {
            signeBeta = -1
          }
          coefBetaTxt = Math.abs(coeffBeta)
        }
        me.logIfDebug('coef_a:' + coeffA + '   coefAlpha:' + coeffAlpha + '   coef_beta:' + coeffBeta)
        me.logIfDebug('coefaTxt:' + coefaTxt + '   coefAlphaTxt:' + coefAlphaTxt + '   coefBetaTxt:' + coefBetaTxt)
      }
      me.logIfDebug('ecritureA:' + ecritureA + '   ecritureAlpha:' + ecritureAlpha + '   ecritureBeta:' + ecritureBeta + '    coeffAlpha:' + coeffAlpha)
      let maformeCano
      let maformeFact = ''
      if (coeffAlpha === 0) {
        maformeCano = nomFct + '(x)=' + ecritureA + 'x^2' + ecritureBeta
      } else {
        maformeCano = nomFct + '(x)=' + ecritureA + '\\left(x' + ecritureAlpha + '\\right)^2' + ecritureBeta
      }
      let lebDev, lecDev
      if (ds.ajout_fact === 1) {
        lebDev = j3pGetLatexSomme(j3pGetLatexProduit(coefaTxt, coefDTxt), j3pGetLatexProduit(coefbTxt, coefCTxt))
        lecDev = j3pGetLatexProduit(coefbTxt, coefDTxt)
        if (Math.abs(coeffB) < Math.pow(10, -12)) {
          maformeFact = nomFct + '(x)=' + j3pGetLatexMonome(1, 1, coefaTxt) + '(' + j3pGetLatexMonome(1, 1, coefCTxt) + j3pGetLatexMonome(2, 0, coefDTxt) + ')'
        } else {
          maformeFact = nomFct + '(x)=(' + j3pGetLatexMonome(1, 1, coefaTxt) + j3pGetLatexMonome(2, 0, coefbTxt) + ')(' + j3pGetLatexMonome(1, 1, coefCTxt) + j3pGetLatexMonome(2, 0, coefDTxt) + ')'
        }
      } else if (ds.ajout_fact === 2) {
        lebDev = j3pGetLatexSomme(j3pGetLatexProduit(coefaTxt, coefbTxt), j3pGetLatexProduit(coefaTxt, coefDTxt))
        lecDev = j3pGetLatexProduit(coefaTxt, j3pGetLatexProduit(coefbTxt, coefDTxt))
        if (Math.abs(coeffB) < Math.pow(10, -12)) {
          maformeFact = nomFct + '(x)=' + j3pGetLatexMonome(1, 1, vraiCoefA) + '(x' + j3pGetLatexMonome(2, 0, coefDTxt) + ')'
        } else {
          maformeFact = nomFct + '(x)=' + ecritureA + '(x' + j3pGetLatexMonome(2, 0, coefbTxt) + ')(x' + j3pGetLatexMonome(2, 0, coefDTxt) + ')'
        }
      } else {
        lebDev = j3pGetLatexProduit(2, j3pGetLatexProduit(vraiCoefA, ecritureAlpha))
        lecDev = j3pGetLatexSomme(j3pGetLatexProduit(vraiCoefA, j3pGetLatexProduit(coefAlphaTxt, coefAlphaTxt)), ecritureBeta)
      }
      const maformeDev = nomFct + '(x)=' + j3pGetLatexMonome(1, 2, vraiCoefA) + j3pGetLatexMonome(2, 1, lebDev) + j3pGetLatexMonome(3, 0, lecDev)
      stor.maformeCano = maformeCano
      stor.maformeDev = maformeDev
      stor.maformeFact = maformeFact
      if (ds.ajout_fact === 1) {
        stor.express_fct = '(' + coeffA + '*x+(' + coeffB + '))*(' + coeffC + '*x+(' + coeffD + '))'
      } else if (ds.ajout_fact === 2) {
        stor.express_fct = coeffA + '*(x+(' + coeffB + '))*(x+(' + coeffD + '))'
      } else {
        stor.express_fct = coeffA + '*(x-(' + coeffAlpha + '))^2+(' + coeffBeta + ')'
      }
      me.logIfDebug('stor.express_fct:' + stor.express_fct)
      stor.coeffAlpha = coeffAlpha
      stor.coeffBeta = coeffBeta
      stor.coeffA = coeffA
      stor.moncoef_a = [coeffA, vraiCoefA]
      stor.moncoef_b = [-2 / coeffA * coeffAlpha, lebDev]
      stor.moncoefC = [coeffA * Math.pow(coeffAlpha, 2) + coeffBeta, lecDev]
      stor.ecritureA = ecritureA
      stor.ecritureAlpha = ecritureAlpha
      if ((ds.ajout_fact === 1) || (ds.ajout_fact === 2)) {
        stor.monalpha = coefAlphaTxt
      } else {
        stor.monalpha = (coeffAlpha < 0) ? j3pGetLatexProduit(-1, coefAlphaTxt) : coefAlphaTxt
      }
      stor.monbeta = coefBetaTxt
      stor.signeBeta = String(signeBeta)
      if (coeffBeta / coeffA < 0) {
        // la fonction peut être factorisée
        stor.zero1 = coeffAlpha + Math.sqrt(-coeffBeta / coeffA)
        stor.zero2 = coeffAlpha - Math.sqrt(-coeffBeta / coeffA)
      }

      const mesValx = []
      // maintenant, je gère les nombres dont je vais chercher l’image ou les antécédents
      let j = 0
      let nbValImposees
      if (ds.type_quest === 'image') {
        // là nous sommes dans le cas du calcul d’images'
        let zeroPresent = false
        let alphaPresent = false
        let entiernonnulPresent = false
        let fractionPresent = false
        let radicalPresent = false
        // dans le cas où on a la forme factorisée, il serait bon d’avoir un zéro de la fonction'
        let racinePresente = true
        if ((ds.ajout_fact === 1) || (ds.ajout_fact === 2)) {
          racinePresente = false
        }
        // var ordre_hasard = false;
        if (stor.nbs_x === undefined) {
          stor.nbs_x = ''
        }
        const racineReg = /-?[0-9]*\*?racine\([0-9]+\)/g // new RegExp('\\-?[0-9]{0,}\\*?racine\\([0-9]{1,}\\)', 'i')
        const fracReg = /-[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}', 'i')
        if (stor.nbs_x === '') {
          // ordre_hasard = true;
        } else if (stor.nbs_x[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === '') {
          // ordre_hasard = true;
        } else {
          let recuperationDonnees = stor.nbs_x[Math.floor((me.questionCourante - 1) / ds.nbetapes)]
          if (recuperationDonnees.charAt(0) === '[') {
            recuperationDonnees = recuperationDonnees.substring(1)
          }
          if (recuperationDonnees.charAt(recuperationDonnees.length - 1) === ']') {
            recuperationDonnees = recuperationDonnees.substring(0, recuperationDonnees.length - 1)
          }
          const choixValeurs = recuperationDonnees.split(',')
          for (let i = 0; i < ds.nbetapes; i++) {
            if (racineReg.test(choixValeurs[i])) {
              j++
              mesValx.push(choixValeurs[i])
              radicalPresent = true
            } else if (fracReg.test(choixValeurs[i])) {
              j++
              mesValx.push(choixValeurs[i])
              fractionPresent = true
            } else if (!isNaN(j3pNombre(String(choixValeurs[i])))) {
              j++
              mesValx.push(choixValeurs[i])
              if (Math.abs(j3pNombre(String(choixValeurs[i]))) < Math.pow(10, -12)) {
                zeroPresent = true
              } else {
                entiernonnulPresent = true
              }
            }
          }
        }
        for (let i = 0; i < mesValx.length; i++) {
          const arbreCalcAlpha = new Tarbre(mesValx[i], [])
          if (Math.abs(arbreCalcAlpha.evalue([]) - coeffAlpha) < Math.pow(10, -12)) {
            alphaPresent = true
          }
          for (let k = 0; k < 2; k++) {
            if (Math.abs(arbreCalcAlpha.evalue([]) - tabValZeros[k]) < Math.pow(10, -12)) {
              racinePresente = true
            }
          }
        }
        nbValImposees = j// c’est le nombre de valeurs que l’utilisateur a imposé'
        // Si j<nbetapes, alors on devra ajouter d’autres valeurs prises au hasard, puis ensuite les mélanger'
        for (let i = j; i < ds.nbetapes; i++) {
          mesValx[i] = ''
        }
        for (let i = 0; i < ds.nbetapes; i++) {
          me.logIfDebug('avant mesValx[' + i + ']=' + mesValx[i])
          if (mesValx[i] !== '') {
            if (racineReg.test(mesValx[i])) {
              // j’ai une racine carrée'
              // j’en resors l’expression sous la forme latex'
              mesValx[i] = mesValx[i].replace('racine(', '\\sqrt{')
              mesValx[i] = mesValx[i].substring(0, mesValx[i].length - 1) + '}'
            } else if (fracReg.test(mesValx[i])) {
              // j’ai une fraction'
              // j’en resors l’expression sous la forme latex'
              const tabValx = mesValx[i].split('/')
              mesValx[i] = '\\frac{' + tabValx[0] + '}{' + tabValx[1] + '}'
            } else if (!isNaN(j3pNombre(String(mesValx[i])))) {
              // j’ai un nombre, donc je ne modifie pas mesValx[i]
            }
          } else {
            let nouvelleValeur
            if (!alphaPresent) {
              mesValx[i] = stor.monalpha
              alphaPresent = true
              if (String(mesValx[i]).includes('frac')) {
                fractionPresent = true
              } else if (String(mesValx[i]).includes('sqrt')) {
                radicalPresent = true
              } else if (!isNaN(j3pNombre(String(mesValx[i])))) {
                if (Math.abs(Math.round(j3pNombre(String(mesValx[i]))) - j3pNombre(String(mesValx[i])))) {
                  entiernonnulPresent = true
                }
              }
              // console.log("je suis venu au niveau alpha + "+mesValx[i])
            } else if (!racinePresente) {
              mesValx[i] = tabZeros[j3pGetRandomInt(0, 1)]
              racinePresente = true
              if (String(mesValx[i]).includes('frac')) {
                fractionPresent = true
              } else if (String(mesValx[i]).includes('sqrt')) {
                radicalPresent = true
              } else if (!isNaN(j3pNombre(String(mesValx[i])))) {
                if (Math.abs(Math.round(j3pNombre(String(mesValx[i]))) - j3pNombre(String(mesValx[i])))) {
                  if (Math.abs(j3pNombre(String(mesValx[i]))) < Math.pow(10, -12)) {
                    zeroPresent = true
                  } else {
                    entiernonnulPresent = true
                  }
                }
              }
            } else if (!entiernonnulPresent && !zeroPresent) {
              if ((Math.abs(j3pGetRandomInt(0, 1)) < Math.pow(10, -12))) {
                mesValx[i] = 0
                zeroPresent = true
              } else {
                do {
                  const plusmoins1 = Math.round(j3pGetRandomInt(0, 1) * 2 - 1)
                  nouvelleValeur = plusmoins1 * j3pGetRandomInt(1, 5)
                } while (valPresente(nouvelleValeur, mesValx))
                mesValx[i] = nouvelleValeur
                entiernonnulPresent = true
              }
            } else if (!entiernonnulPresent && zeroPresent && fractionPresent && radicalPresent) {
              do {
                const plusmoins1 = Math.round(j3pGetRandomInt(0, 1) * 2 - 1)
                nouvelleValeur = plusmoins1 * j3pGetRandomInt(1, 5)
              } while (valPresente(nouvelleValeur, mesValx))
              mesValx[i] = nouvelleValeur
              entiernonnulPresent = true
              // console.log("je suis venu au niveau nombre entier + "+mesValx[i])
            } else if (!fractionPresent) {
              if (!radicalPresent) {
                if (Math.abs(j3pGetRandomInt(0, 1)) < Math.pow(10, -12)) {
                  // dans ce cas, je mets un radical
                  do {
                    nouvelleValeur = generationRadical()
                  } while (valPresente(nouvelleValeur, mesValx))
                  mesValx[i] = nouvelleValeur
                  radicalPresent = true
                  // console.log("je suis venu au niveau radical (choix avec frac) + "+mesValx[i])
                } else {
                  // dans ce cas, je mets une fraction
                  do {
                    nouvelleValeur = generationFraction()
                  } while (valPresente(nouvelleValeur, mesValx))
                  mesValx[i] = nouvelleValeur
                  fractionPresent = true
                  // console.log("je suis venu au niveau frac (choix avec radical) + "+mesValx[i])
                }
              } else {
                do {
                  nouvelleValeur = generationFraction()
                } while (valPresente(nouvelleValeur, mesValx))
                mesValx[i] = nouvelleValeur
                fractionPresent = true
                // console.log("je suis venu au niveau fract (pas de choix) + "+mesValx[i])
              }
            } else if (!radicalPresent) {
              do {
                nouvelleValeur = generationRadical()
              } while (valPresente(nouvelleValeur, mesValx))
              mesValx[i] = nouvelleValeur
              radicalPresent = true
              // console.log("je suis venu au niveau radical + "+mesValx[i])
            }
            if (alphaPresent && fractionPresent && entiernonnulPresent && radicalPresent) {
              // je remets tout à zero (sauf zéro et alpha)
              entiernonnulPresent = false
              fractionPresent = false
              radicalPresent = false
              // console.log("j’ai tout remis à zéro")
            }
          }
        }
      } else {
        // donc on résout une équation ou on recherche des antécédents
        let newTableau
        if (me.questionCourante === 1) {
          // c’est la première fonction'
          stor.tab_choixval = [0, 1, 2]
          // 0 pour la valeur 0, 1 pour beta et 2 pour coefC
          newTableau = j3pShuffle(stor.tab_choixval)
        } else {
          if (stor.tab_choixval.length > 0) {
            newTableau = j3pShuffle(stor.tab_choixval)
          } else {
            newTableau = []
          }
          const lgTab = newTableau.length
          for (let i = lgTab; i < 3; i++) {
            let newChoix
            do {
              newChoix = j3pGetRandomInt(0, 2)
            } while (newTableau.indexOf(newChoix) > -1)
            newTableau.push(newChoix)
          }
        }
        for (let i = 0; i < ds.nbetapes; i++) {
          if (newTableau[i] === 0) {
            mesValx[i] = 0
          } else if (newTableau[i] === 1) {
            mesValx[i] = stor.monbeta
          } else {
            mesValx[i] = stor.moncoefC[1]
          }
        }
        stor.tab_choixval = []
        if (ds.nbetapes < 3) {
          for (let i = ds.nbetapes; i < 3; i++) {
            stor.tab_choixval.push(newTableau[i])
          }
          // on récupère dans stor.tab_choixval les numéro des questions qui n’ont pas été posées'
        }
        nbValImposees = 0
      }
      me.logIfDebug('mesValx avant mélange :' + mesValx)
      let tabValAjoutees = []
      if (ds.type_quest === 'image') {
        // je vais mélanger les premières valeurs dont je cherche l’image
        // si nbetapes est supérieur à 2 ou 3 (selon qu’on ait la forme frac ou pas), j’ajoute une fraction puis si besoin un radical
        const nbetapesMin = ((ds.ajout_fact === 1) || (ds.ajout_fact === 2)) ? 3 : 2
        const tabPremieresValeurs = []
        let numValPremiereEtape = 0
        // il faut quand même que je conserve les valeurs imposées
        while (nbValImposees + tabPremieresValeurs.length < Math.min(ds.nbetapes, nbetapesMin)) {
          tabPremieresValeurs.push(mesValx[nbValImposees + numValPremiereEtape])
          numValPremiereEtape++
        }
        tabValAjoutees = tabValAjoutees.concat(j3pShuffle(tabPremieresValeurs))
        const tabValeursSuivantes = []
        let numValEtapeSuiv = 0
        while (nbValImposees + tabValAjoutees.length + tabValeursSuivantes.length < ds.nbetapes) {
          tabValeursSuivantes.push(mesValx[nbValImposees + tabValAjoutees.length + numValEtapeSuiv])
          numValEtapeSuiv++
        }
        tabValAjoutees = tabValAjoutees.concat(j3pShuffle(tabValeursSuivantes))
      } else {
        // enfin je mélange toutes les valeurs à partir de nbValImposees
        for (let i = nbValImposees; i < ds.nbetapes; i++) {
          tabValAjoutees.push(mesValx[i])
        }
        // ici si
        tabValAjoutees = j3pShuffle(tabValAjoutees)
      }
      for (let i = nbValImposees; i < ds.nbetapes; i++) {
        mesValx[i] = tabValAjoutees[i - nbValImposees]
      }
      me.logIfDebug('mesValx après mélange :' + mesValx)
      stor.mesValx = mesValx
      stor.tabZeros = tabZeros
    }
    me.logIfDebug('forme canonique :' + stor.maformeCano + '   forme développée :' + stor.maformeDev + '   forme factorisée :' + stor.maformeFact)
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.nb_formes = 2
    if ((ds.ajout_fact === 1) || (ds.ajout_fact === 2)) {
      stor.nb_formes = 3
    }
    j3pAffiche(stor.zoneCons1, '', textes.consigne1 + '\n' + textes.consigne2,
      {
        f: stor.nomFct,
        n: stor.nb_formes
      })
    j3pAffiche(stor.zoneCons2, '', '$' + stor.maformeCano + '$')
    j3pAffiche(stor.zoneCons3, '', '$' + stor.maformeDev + '$')
    if ((ds.ajout_fact === 1) || (ds.ajout_fact === 2)) {
      j3pAffiche(stor.zoneCons4, '', '$' + stor.maformeFact + '$')
    }
    // zone de gestion des nombres dont on va chercher les images
    const leX = stor.mesValx[Math.floor((me.questionCourante - 1) % ds.nbetapes)]

    let leXEvalue = j3pMathquillXcas(leX)
    while (leXEvalue.indexOf('sqrt') > -1) {
      leXEvalue = leXEvalue.replace('sqrt', 'racine')
    }
    const arbreX = new Tarbre(leXEvalue, [])
    const valeurX = arbreX.evalue([])

    if (ds.type_quest === 'image') {
      // stor.meilleure_forme est le numéro de la meilleure forme (2 pour canonique, 3 pour développée, 4 pour factorisée)
      if (valPresente(leX, [stor.coeffAlpha])) {
        // leX est alpha, donc on choisit plutôt la forme canonique
        stor.meilleure_forme = 2
      } else if ((valPresente(leX, stor.tabZeros)) && ((ds.ajout_fact === 1) || (ds.ajout_fact === 2))) {
        // avec la forme factorisée, on a les zéros de la fonction dans tabZeros'
        // si leX est l’une de ces valeurs, alors on utilise la forme factorisée'
        stor.meilleure_forme = 4
      } else if ((leX === 0) || (leX === '0') || (String(leX).indexOf('sqrt') > -1)) {
        // dans cas cas on prend la forme développée
        stor.meilleure_forme = 3
      } else {
        // dans tous les autres cas, il n’y a pas vraiment de meilleure forme pour faire le calcul'
        stor.meilleure_forme = 5
      }
    } else {
      if ((leX === '0') || (leX === 0)) {
        // on résout f(x)=0 donc il faut prendre la forme factorisée
        stor.meilleure_forme = 4
        stor.tab_solutions = stor.tabZeros
      } else if (leX === stor.moncoefC[1]) {
        // L’équaton est de la forme ax^2+bx+c=c, donc on prend la forme développée'
        stor.meilleure_forme = 3
        stor.tab_solutions = [0, j3pGetLatexQuotient(j3pGetLatexProduit(-1, stor.moncoef_b[1]), stor.moncoef_a[1])]
      } else {
        // on résout a(x-alpha)^2+beta=beta donc on prend la forme canonique
        stor.meilleure_forme = 2
        stor.tab_solutions = [stor.monalpha]
      }
    }
    let laConsigne = textes.consigne5// valable pour la recherche d’antécédents'
    if (ds.type_quest === 'image') {
      laConsigne = textes.consigne3
    } else if ((ds.type_quest === 'equation') || (ds.type_quest === 'équation')) {
      laConsigne = textes.consigne4
    }
    j3pAffiche(stor.zoneCons5, '', laConsigne,
      {
        f: stor.nomFct,
        x: leX
      })
    stor.divselect = 0
    // Les numéros vont me permettre d’identifier la zone au clic
    stor.zoneCons2.numero = 2
    stor.zoneCons3.numero = 3
    stor.zoneCons4.numero = 4
    stor.zoneCons2.addEventListener('mousemove', surbrillanceSurvol, false)
    stor.zoneCons2.addEventListener('mouseout', surbrillanceSuppr, false)
    stor.zoneCons2.addEventListener('click', surbrillanceSelect, false)
    stor.zoneCons3.addEventListener('mousemove', surbrillanceSurvol, false)
    stor.zoneCons3.addEventListener('mouseout', surbrillanceSuppr, false)
    stor.zoneCons3.addEventListener('click', surbrillanceSelect, false)
    if ((ds.ajout_fact === 1) || (ds.ajout_fact === 2)) {
      stor.zoneCons4.addEventListener('mousemove', surbrillanceSurvol, false)
      stor.zoneCons4.addEventListener('mouseout', surbrillanceSuppr, false)
      stor.zoneCons4.addEventListener('click', surbrillanceSelect, false)
    }
    let elt
    if (ds.type_quest === 'image') {
      elt = j3pAffiche(stor.zoneCons6, '', '$' + stor.nomFct + '\\left(£x\\right)=$&1&',
        {
          x: leX,
          inputmq1: { texte: '' }
        })
      stor.zoneInput = elt.inputmqList[0]
      mqRestriction(stor.zoneInput, '\\d.,/-+*')
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      const arbreExpress = new Tarbre(stor.express_fct, ['x'])
      const valeurImage = arbreExpress.evalue([valeurX])
      stor.zoneInput.reponse = valeurImage
      stor.valeurX = valeurX
      stor.valeurImage = valeurImage
      me.logIfDebug('valeurImage:' + valeurImage)
    } else {
      elt = j3pAffiche(stor.zoneCons6, '', '&1&',
        {
          inputmq1: { texte: '' }
        })
      stor.zoneInput = elt.inputmqList[0]
      mqRestriction(stor.zoneInput, '\\d.,/-+*;', {
        commandes: ['racine', 'fraction']
      })
    }
    stor.leX = leX
    stor.laPalette = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.laPalette, { paddingTop: '10px' })
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['racine', 'fraction'] })
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.correction', { padding: '5px' }) })

    stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.zoneInput.id], validePerso: [stor.zoneInput.id] })
    stor.zoneInput.blur()
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        if (isNaN(ds.nbetapes)) ds.nbetapes = 0
        if (ds.nbetapes <= 0) {
          // c’est que cela n’a pas été surchargé'
          if (ds.type_quest === 'image') ds.nbetapes = 3
          else ds.nbetapes = 2
        }
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        // coef a pour toutes les formes
        stor.coef_a = constructionTabIntervalle(ds.a, '[-5;5]')
        // coefs alpha et beta pour la forme canonique (cad s’il n’y a pas de forme factorisée)'
        stor.coefAlpha = constructionTabIntervalle(ds.alpha, '[-5;5]')
        stor.coef_beta = constructionTabIntervalle(ds.beta, '[-5;5]')
        // coefs b, c et d seulement valables si on donne la forme factorisée
        stor.coef_b = constructionTabIntervalle(ds.b, '[-5;5]')
        stor.coefC = constructionTabIntervalle(ds.c, '[-5;5]')
        stor.coef_d = constructionTabIntervalle(ds.d, '[-5;5]')
        if ((ds.imposer_nbs === '') || (ds.imposer_nbs === '[]')) {
          stor.nbs_x = ''
        } else {
          stor.nbs_x = ds.imposer_nbs.split('|')
        }
        me.logIfDebug('stor.coefAlpha:' + stor.coefAlpha)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        let reponse = { bonneReponse: false }
        const fctsValid = stor.fctsValid
        let aRepondu = false
        let reponseSimplifiee = fctsValid.reponseSimplifiee(stor.zoneInput.id)
        const reponseSimple = reponseSimplifiee[0]
        const valPresentes = []
        let reponseInitiale, valDifferentes, tabRep
        if (stor.divselect > 0) {
          aRepondu = true
          // ce qui suit sert pour la validation de toutes les zones
          // le tableau contenant toutes les zones de saisie
          reponse = fctsValid.validationGlobale()// c’est juste pour vérifier que la zone est bien complétée'
          aRepondu = reponse.aRepondu
          if (ds.type_quest === 'image') {
            reponse = fctsValid.valideUneZone(stor.zoneInput.id, stor.valeurImage)
            reponseInitiale = reponse.bonneReponse
            if ((!reponseSimple && reponse.bonneReponse) || !reponse.bonneReponse) {
            // c’est que ce qui est écrit n’est pas sous forme simplifiée'
            // donc je considère la réponse comme fausse
              reponse.bonneReponse = false
              fctsValid.zones.bonneReponse[0] = false
            } else {
              fctsValid.zones.bonneReponse[0] = true
              if (!reponseSimplifiee[1]) {
                aRepondu = false
                reponse.aRepondu = false
                fctsValid.zones.bonneReponse[0] = false
              }
            }
            if (aRepondu) {
              fctsValid.coloreUneZone(stor.zoneInput.id)
            }
          } else {
          // dans ce cas reponse.bonneReponse vaut true. On n’a pas encore validé la réponse. Cela doit se faire à la main'
            const repSaisieInit = j3pValeurde(stor.zoneInput).split(';')
            tabRep = []// tabRep contient toutes les valeurs non vides
            for (let i = 0; i < repSaisieInit.length; i++) {
              if (repSaisieInit[i] !== '') {
                try {
                  tabRep.push(j3pMathquillXcas(repSaisieInit[i]).replace('sqrt', 'racine'))
                } catch (e) {
                  console.warn(e) // pas normal mais pas grave
                }
              }
            }
            // toutes les valeurs devraient être différentes
            valDifferentes = true
            if (tabRep.length > 1) {
              for (let i = 1; i < tabRep.length; i++) {
                for (let j = 0; j < i; j++) {
                  const arbreDifference = new Tarbre(tabRep[i] + '-(' + tabRep[j] + ')', [])
                  const maDifference = arbreDifference.evalue([])
                  if (Math.abs(maDifference) < Math.pow(10, -12)) {
                  // c’est que j’ai deux valeurs identiques'
                    valDifferentes = false
                  }
                }
              }
            }
            // toutes les réponses attendues sont-elles données
            for (let i = 0; i < stor.tab_solutions.length; i++) {
              valPresentes[i] = false
              for (let j = 0; j < tabRep.length; j++) {
                const arbreDifference = new Tarbre(j3pMathquillXcas(stor.tab_solutions[i]) + '-(' + tabRep[j] + ')', [])
                try {
                  if (Math.abs(arbreDifference.evalue([])) < Math.pow(10, -12)) {
                  // c’est que la solution attendue est bien donnée par l’élève'
                    valPresentes[i] = true
                  }
                } catch (error) {
                  console.error(error)
                }
              }
            }
            reponse.bonneReponse = (valDifferentes && (tabRep.length === stor.tab_solutions.length))
            for (let i = 0; i < stor.tab_solutions.length; i++) {
              reponse.bonneReponse = (reponse.bonneReponse && valPresentes[i])
            }
            if (!reponse.bonneReponse) {
              fctsValid.zones.bonneReponse[0] = false
            } else {
            // on vérifie que les fractions sont simplifiées
              reponseSimplifiee = true
              for (let i = 0; i < tabRep.length; i++) {
                if (!fctsValid.estNbFrac(tabRep[i])[1]) {
                  reponseSimplifiee = false
                }
              }
            }
            if (reponseSimplifiee) {
              fctsValid.coloreUneZone(stor.zoneInput.id)
            } else {
              aRepondu = false
            }
          }
        }
        if ((!aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (stor.divselect === 0) {
            if (ds.type_quest === 'image') {
              msgReponseManquante = textes.comment1
            } else if ((ds.type_quest === 'equation') || (ds.type_quest === 'équation')) {
              msgReponseManquante = textes.comment1bis
            } else {
              msgReponseManquante = textes.comment1ter
            }
          } else {
            if (!reponseSimplifiee[1]) {
              msgReponseManquante = textes.comment8
              j3pFocus(stor.zoneInput)
            }
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
          if (reponse.bonneReponse) {
          // Bonne réponse
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            if ((stor.meilleure_forme !== stor.divselect) && (stor.meilleure_forme < 5)) {
              if (ds.type_quest === 'image') {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment2
              } else if ((ds.type_quest === 'equation') || (ds.type_quest === 'équation')) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment2bis
              } else {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment2ter
              }
            }
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              afficheCorrection(false)
            } else {
              // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if ((stor.meilleure_forme !== stor.divselect) && (stor.meilleure_forme < 5)) {
                if (ds.type_quest === 'image') {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment3
                } else if ((ds.type_quest === 'equation') || (ds.type_quest === 'équation')) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment3bis
                } else {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment3ter
                }
              }
              if (ds.type_quest !== 'image') {
                if (!valDifferentes) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment4
                }
                if ((valPresentes.length === 2) && (tabRep.length === 1) && ((valPresentes[0] && !valPresentes[1]) || (!valPresentes[0] && valPresentes[1]))) {
                // dans ce cas, une seule des deux solutions a été donnée
                  if ((ds.type_quest === 'equation') || (ds.type_quest === 'équation')) {
                    stor.zoneCorr.innerHTML += '<br>' + textes.comment5
                  } else {
                    stor.zoneCorr.innerHTML += '<br>' + textes.comment6
                  }
                }
              } else {
                if (reponseInitiale) {
                // le calcul donné par l’élève rend bien la bonne réponse mais ce qu’il a écrit doit être simplifié'
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment7
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                return me.finCorrection()
              } else {
                // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
