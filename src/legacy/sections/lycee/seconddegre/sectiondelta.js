import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetRandomBool, j3pGetRandomInt, j3pPaletteMathquill, j3pRandomTab, j3pRestriction } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexMonome, j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    section intiale par ??? reprise par Rémi DENIAUD
    août 2015
    Dans cette section, l’élève doit donner la valeur du discriminant d’un trinôme et éventuellement le nombre de racines
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['nbetapes', 2, 'entier', 'Pour chaque trinôme, on peut proposer 2 questions ou seulement la première : valeur du discriminant puis nombre de racines du trinôme.'],
    ['trinomes', ['', '', ''], 'array', 'Pour chaque répétition, on peut imposer le trinôme dont on cherche le signe'],
    ['nbracines', ['alea', 'alea', 'alea'], 'array', 'Pour chaque répétition, on peut imposer le nombre de racine du trinôme lorsqu’il est aléatoire (0, 1 ou 2), sinon mettre "alea" ou ""'],
    ['ecriturepolynome', 'Desordre', 'string', 'Ecriture du polynôme : OrdonneCroissant ou OrdonneDecroissant ou Desordre'],
    ['seuilreussite', 0.7, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 7/10'],
    ['seuilerreur', 0.4, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 40% des cas)']
  ],

  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'Problème dans le calcul du discriminant' },
    { pe_3: 'Le cours sur le nombre de racines d’un trinôme n’est pas connu' },
    { pe_4: 'Insuffisant' }
  ]
}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Discriminant d’un trinôme',
  // on donne les phrases de la consigne
  consigne1: 'Le discriminant du trinôme $£t$ vaut :<br>$\\Delta=$&1&.',
  consigne2: 'Le discriminant du trinôme $£t$ vaut $\\Delta=£d$.',
  consigne3: 'Ce trinôme admet alors @1@ racine(s).',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Ne pas confondre le carré et le double dans le calcul de b²&nbsp;!',
  comment2: 'Les coefficients a, b et c n’ont pas été correctement identifiés&nbsp;!',
  comment3: 'Il semble y avoir une erreur de signe&nbsp;!',
  comment4: 'La réponse doit être donnée sous la forme d’un nombre simplifié et non un calcul&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: "Le trinôme s'écrit sous la forme $ax^2+bx+c$ où $a=£a$, $b=£b$ et $c=£c$ (attention à l’ordre des termes).",
  corr2: 'Le discriminant vaut alors $\\Delta=b^2-4ac$.',
  corr3: 'Ainsi $\\Delta=£c=£d$.',
  corr4_1: 'Comme $\\Delta>0$, le trinôme admet 2 racines, c’est-à-dire que l’équation $£t=0$ admet 2 solutions.',
  corr4_2: 'Comme $\\Delta=0$, le trinôme admet une seule racine, c’est-à-dire que l’équation $£t=0$ admet une unique solution.',
  corr4_3: 'Comme $\\Delta<0$, le trinôme n’admet pas de racine, c’est-à-dire que l’équation $£t=0$ n’admet pas de solution.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    if ((ds.nbetapes === 1) || ((ds.nbetapes === 2) && (me.questionCourante % 2 === 1))) {
      j3pDetruit(stor.laPalette)
      // le div le_nom+"explications" contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
      const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
      j3pAffiche(zoneExpli1, '', textes.corr1, {
        a: stor.objet_polynome.tab_coefs1[2],
        b: stor.objet_polynome.tab_coefs1[1],
        c: stor.objet_polynome.tab_coefs1[0]
      })
      const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
      const zoneExpli3 = j3pAddElt(zoneExpli, 'div')
      j3pAffiche(zoneExpli2, '', textes.corr2)
      const calcDelta = afficherelatif(stor.objet_polynome.tab_coefs1[1], true) + '^2-4\\times' + afficherelatif(stor.objet_polynome.tab_coefs1[2], false) + '\\times' + afficherelatif(stor.objet_polynome.tab_coefs1[0], false)
      j3pAffiche(zoneExpli3, '', textes.corr3, {
        c: calcDelta,
        d: stor.objet_polynome.tab_coefs1[3]
      })
    } else {
      // le div le_nom+"explications" contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
      const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
      const valeurDelta = j3pCalculValeur(stor.objet_polynome.tab_coefs1[3])
      let consigneCorr1 = textes.corr4_1
      if (Math.abs(valeurDelta) < Math.pow(10, -12)) {
        consigneCorr1 = textes.corr4_2
      } else if (valeurDelta < 0) {
        consigneCorr1 = textes.corr4_3
      }
      j3pAffiche(zoneExpli1, '', consigneCorr1, {
        t: stor.objet_polynome.polynome,
        d: stor.objet_polynome.tab_coefs1[3]
      })
    }
  }
  function ecrireFonction (fctTxt) {
    let fctLatex = fctTxt
    const regFrac1 = /\(-?[0-9]+\)\/\([0-9]+\)/g // new RegExp('\\(\\-?[0-9]{1,}\\)/\\([0-9]{1,}\\)', 'ig')
    const regFrac2 = /\(-?[0-9]+\/[0-9]+\)/g // new RegExp('\\(\\-?[0-9]{1,}/[0-9]{1,}\\)', 'ig')
    const regFrac3 = /[0-9]+\/[0-9]/g // new RegExp('[0-9]{1,}/[0-9]{1,}', 'ig')
    let num, den, i, tabSeparateur
    while (regFrac1.test(fctLatex)) {
      // on trouve (..)/(..)
      const tabFrac1 = fctLatex.match(regFrac1)
      for (i = 0; i < tabFrac1.length; i++) {
        tabSeparateur = tabFrac1[i].split('/')
        num = tabSeparateur[0].substring(1, tabSeparateur[0].length - 1)
        den = tabSeparateur[1].substring(1, tabSeparateur[1].length - 1)
        fctLatex = fctLatex.replace(tabFrac1[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    while (regFrac2.test(fctLatex)) {
      // on trouve (../..)
      const tabFrac2 = fctLatex.match(regFrac2)
      for (i = 0; i < tabFrac2.length; i++) {
        tabSeparateur = tabFrac2[i].split('/')
        num = tabSeparateur[0].substring(1, tabSeparateur[0].length)
        den = tabSeparateur[1].substring(0, tabSeparateur[1].length - 1)
        fctLatex = fctLatex.replace(tabFrac2[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    while (regFrac3.test(fctLatex)) {
      // on trouve ../..
      const tabFrac3 = fctLatex.match(regFrac3)
      for (i = 0; i < tabFrac3.length; i++) {
        tabSeparateur = tabFrac3[i].split('/')
        num = tabSeparateur[0].substring(0, tabSeparateur[0].length)
        den = tabSeparateur[1].substring(0, tabSeparateur[1].length)
        fctLatex = fctLatex.replace(tabFrac3[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    // on enlève signe * devant x
    while (fctLatex.includes('*x')) {
      fctLatex = fctLatex.replace('*x', 'x')
    }
    if (fctLatex.includes('x²')) {
      fctLatex = fctLatex.replace('x²', 'x^2')
    }
    // fonction bx^2+cx+d avec coefs au format latex
    const posSigne = [0]
    for (i = 1; i < fctLatex.length; i++) {
      if ((fctLatex[i] === '+') || (fctLatex[i] === '-')) posSigne.push(i)
    }
    posSigne.push(fctLatex.length)
    // je découpe alors l’expression à l’aide de ces signes, cela me donnera les monômes
    const tabMonome = []
    for (i = 1; i < posSigne.length; i++) tabMonome.push(fctLatex.substring(posSigne[i - 1], posSigne[i]))
    const tabCoefPolynome = ['0', '0', '0']
    // tabCoefPolynome[0] est le coef constant, tabCoefPolynome[1], celui devant x, ...
    // je fais un correctif si j’ai un monome avec un coef de la forme \\frac{-...}{...}
    let posI = 0
    while (posI < tabMonome.length) {
      if ((tabMonome[posI] === '\\frac{') || (tabMonome[posI] === '+\\frac{') || (tabMonome[posI] === '-\\frac{')) {
        tabMonome[posI + 1] = tabMonome[posI] + tabMonome[posI + 1]
        tabMonome.splice(posI, 1)
      } else {
        posI++
      }
    }
    for (i = 0; i < tabMonome.length; i++) {
      // tabMonome.length<=3 c’est le nombre de monomes présents
      if ((tabMonome[i].indexOf('x^{2}') > -1) || (tabMonome[i].indexOf('x^2') > -1)) {
        // c’est le monome ax^2
        if (tabMonome[i].charAt(0) === '+') {
          tabMonome[i] = tabMonome[i].substring(1)
        }
        if (tabMonome[i].includes('x^{2}')) {
          tabCoefPolynome[2] = tabMonome[i].substring(0, tabMonome[i].length - 5)
        } else {
          tabCoefPolynome[2] = tabMonome[i].substring(0, tabMonome[i].length - 3)
        }
        if (tabCoefPolynome[2] === '') {
          tabCoefPolynome[2] = '1'
        } else if (tabCoefPolynome[2] === '-') {
          tabCoefPolynome[2] = '-1'
        }
      } else if (tabMonome[i].includes('x')) {
        // c’est le monome ax
        if (tabMonome[i].charAt(0) === '+') {
          tabMonome[i] = tabMonome[i].substring(1)
        }
        tabCoefPolynome[1] = tabMonome[i].substring(0, tabMonome[i].length - 1)
        if (tabCoefPolynome[1] === '') {
          tabCoefPolynome[1] = '1'
        } else if (tabCoefPolynome[1] === '-') {
          tabCoefPolynome[1] = '-1'
        }
      } else {
        // c’est le monome a
        if (tabMonome[i].charAt(0) === '+') {
          tabCoefPolynome[0] = tabMonome[i].substring(1)
        } else {
          tabCoefPolynome[0] = tabMonome[i]
        }
      }
    }
    return { fctLatex, tab_coef: tabCoefPolynome }
  }
  function rechercheRacine (tab1) {
    // tab1 est un tableaux de 3 éléments représentant la fonction trinôme
    // tab1[0] est l’expression de la fonction polynomiale au format latex
    // tab1[1] est le tableau des coefficients du polynôme (lorsqu’ils existent)
    // cette fonction génère le polynôme s’il n’est pas imposé et calcule les solutions éventuelles de l’équation tab1[0]=0
    let poly1 = ''
    let a, b, c, d, a1, b1, c1
    const fctTxt1 = tab1[0]
    if (fctTxt1 === '') {
      // la fonctions est aléatoire.
      // Dans ce cas, on écrit le trinôme différence sous la forme (ax+b)(cx+d) s’il admet 2 racines
      // on l’écrit (ax+b)^2 s’il admet 2 racines et (ax+b)^2+c (c>0) s’il n’admet pas de racine
      const nbSol = (stor.nbRacinesImposees === 'alea')
        ? j3pRandomTab([0, 1, 2], [0.25, 0.125, 0.625])
        : Number(stor.nbRacinesImposees)
      // la proba d’avoir 0 racine est 1/4, celle d’en avoir 1 est 1/8 et celle d’en avoir 2 est 5/8
      if (nbSol === 0) {
        // polynôme (ax+b)^2+c=a^2x^2+2abx+b^2+c ou l’opposé
        do {
          do {
            a = j3pGetRandomInt(-3, 3)
          } while (Math.abs(a) < Math.pow(10, -12))
          do {
            b = j3pGetRandomInt(-5, 5)
          } while (Math.abs(b) < Math.pow(10, -12))
          c = j3pGetRandomInt(1, 7)
          a1 = Math.pow(a, 2)
          b1 = 2 * a * b
          c1 = Math.pow(b, 2) + c
          if (j3pGetRandomBool()) {
            a1 = -a1
            b1 = -b1
            c1 = -c1
          }
        } while (Math.abs(b1) < Math.pow(10, -12))
      } else if (nbSol === 1) {
        // polynôme (ax+b)^2=a^2x^2+2abx+b^2 ou son opposé
        do {
          a = j3pGetRandomInt(-3, 3)
        } while (Math.abs(a) < Math.pow(10, -12))
        do {
          b = j3pGetRandomInt(-5, 5)
        } while (Math.abs(b) < Math.pow(10, -12))
        a1 = Math.pow(a, 2)
        b1 = 2 * a * b
        c1 = Math.pow(b, 2)
        if (j3pGetRandomBool()) {
          a1 = -a1
          b1 = -b1
          c1 = -c1
        }
      } else {
        const choixDelta = j3pGetRandomInt(0, 2)// dans 1 cas sur 3, delta ne sera pas un carré parfait
        if (choixDelta === 0) {
          let deltaVal
          do {
            a1 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
            b1 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 9)
            c1 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 9)
            deltaVal = Math.pow(b1, 2) - 4 * a1 * c1
          } while (Math.abs(Math.sqrt(deltaVal) - Math.round(Math.sqrt(deltaVal))) < Math.pow(10, -12))
        } else {
          // (ax+b)(cx+d)=acx^2+(ad+bc)x+bd
          do {
            do {
              a = j3pGetRandomInt(-3, 3)
            } while (Math.abs(a) < Math.pow(10, -12))
            do {
              b = j3pGetRandomInt(-5, 5)
            } while (Math.abs(b) < Math.pow(10, -12))
            do {
              c = j3pGetRandomInt(-3, 3)
            } while (Math.abs(c) < Math.pow(10, -12))
            do {
              d = j3pGetRandomInt(-5, 5)
            } while (Math.abs(d) < Math.pow(10, -12))
          } while (Math.abs(Math.abs(-b / a) - Math.abs(-d / c)) < Math.pow(10, -12))
          a1 = a * c
          b1 = a * d + b * c
          c1 = b * d
        }
      }
    } else {
      a1 = (tab1[1][2] === '') ? '0' : tab1[1][2]
      b1 = (tab1[1][1] === '') ? '0' : tab1[1][1]
      c1 = (tab1[1][0] === '') ? '0' : tab1[1][0]
    }
    if (stor.fonction_imposee === '') {
      switch (ds.ecriturepolynome.toLowerCase()) {
        case 'ordonnedecroissant':
          poly1 = j3pGetLatexMonome(1, 2, String(a1)) + j3pGetLatexMonome(2, 1, String(b1)) + j3pGetLatexMonome(2, 0, String(c1))
          break
        case 'ordonnecroissant':
          poly1 = j3pGetLatexMonome(1, 0, String(b1)) + j3pGetLatexMonome(2, 1, String(b1)) + j3pGetLatexMonome(3, 2, String(a1))
          break
        default:
          {
            const SensAleatoire = j3pGetRandomInt(0, 5)
            switch (SensAleatoire) {
            // ax²+bx+c
              case 0:
                poly1 = j3pGetLatexMonome(1, 2, String(a1)) + j3pGetLatexMonome(2, 1, String(b1)) + j3pGetLatexMonome(3, 0, String(c1))
                break
                // ax²+c+bx
              case 1:
                poly1 = j3pGetLatexMonome(1, 2, String(a1)) + j3pGetLatexMonome(2, 0, String(c1)) + j3pGetLatexMonome(3, 1, String(b1))
                break
                // bx+ax²+c
              case 2:
                poly1 = j3pGetLatexMonome(1, 1, String(b1)) + j3pGetLatexMonome(2, 2, String(a1)) + j3pGetLatexMonome(3, 0, String(c1))
                break
                // bx+c+ax²
              case 3:
                poly1 = j3pGetLatexMonome(1, 1, String(b1)) + j3pGetLatexMonome(2, 0, String(c1)) + j3pGetLatexMonome(3, 2, String(a1))
                break
                // c+bx+ax²
              case 4:
                poly1 = j3pGetLatexMonome(1, 0, String(c1)) + j3pGetLatexMonome(2, 1, String(b1)) + j3pGetLatexMonome(3, 2, String(a1))
                break
                // c+ax²+bx
              case 5:
                poly1 = j3pGetLatexMonome(1, 0, String(c1)) + j3pGetLatexMonome(2, 2, String(a1)) + j3pGetLatexMonome(3, 1, String(b1))
                break
            }
          }
          break
      }
    } else {
      poly1 = stor.fonction_imposee
    }

    me.logIfDebug('poly1:' + poly1)
    const deltaTxt = j3pGetLatexSomme(j3pGetLatexProduit(b1, b1), j3pGetLatexProduit('-4', j3pGetLatexProduit(a1, c1)))
    me.logIfDebug('delta:' + deltaTxt)
    return { polynome: poly1, tab_coefs1: [c1, b1, a1, deltaTxt] }
  }
  /* Sert à afficher un nombre relatif dans une expression
    * si le nombre est négatif il faut y mettre des parenthèses
    * sinon on le laisse tel quel
    * nb : nombre à afficher
    */
  function afficherelatif (nb, avecpar) {
    // nb est un nombre, éventuellement une fraction
    // lorsque nb est un fraction, le paramètre avecpar peut forcer l’utilisation de paranthèses (noatemment pour le carré) alors que pour les autres calculs ce n’est pas nécessaire (produit par exemple)
    let reponse
    if (String(nb).includes('frac')) {
      // le nombre est une fraction
      if (avecpar) {
        reponse = '\\left(' + nb + '\\right)'
      } else {
        if (String(nb).charAt(0) === '-') {
          // la fraction est de la forme -\\frac{...}{...}
          reponse = '\\left(' + nb + '\\right)'
        } else {
          reponse = nb
        }
      }
    } else {
      if (Number(nb) < 0) {
        reponse = '\\left(' + nb + '\\right)'
      } else {
        reponse = nb
      }
    }
    return reponse
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    if ((ds.nbetapes === 1) || ((ds.nbetapes === 2) && (me.questionCourante % 2 === 1))) {
      const maFonction1 = (ds.nbetapes === 1)
        ? ds.trinomes[me.questionCourante - 1]
        : ds.trinomes[(me.questionCourante - 1) / 2]
      let coefs1 = []
      stor.fonction_imposee = ''
      if ((maFonction1 !== undefined) && (maFonction1 !== '') && (maFonction1 !== 'trinome') && (maFonction1 !== 'affine') && (maFonction1 !== 'constante')) {
        // le trinôme est imposée
        const maFonctionDonnee1 = ecrireFonction(maFonction1)
        stor.fonction_imposee = maFonctionDonnee1.fctLatex
        coefs1 = maFonctionDonnee1.tab_coef
      }
      // ce qui suit n’est valable que si le trinôme n’est pas imposé
      const nbRacinesImposees = ds.nbracines[me.questionCourante - 1]
      if ((nbRacinesImposees === 0) || (nbRacinesImposees === '0') || (nbRacinesImposees === 1) || (nbRacinesImposees === '1') || (nbRacinesImposees === 2) || (nbRacinesImposees === '2')) {
        stor.nbRacinesImposees = String(nbRacinesImposees)
      } else {
        stor.nbRacinesImposees = 'alea'
      }
      stor.objet_polynome = rechercheRacine([stor.fonction_imposee, coefs1, maFonction1])

      // cette fonction renvoie le polynôme (aléatoire ou imposé), ses coefficients (avec le discriminant au format latex)
      // {polynome:poly1,tab_coefs1:[c1,b1,a1,deltaTxt]};
      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      const zoneExpli1 = j3pAddElt(stor.conteneur, 'div')
      const elt = j3pAffiche(zoneExpli1, '', textes.consigne1,
        {
          t: stor.objet_polynome.polynome,
          inputmq1: { texte: '' }
        })
      stor.zoneInput = elt.inputmqList[0]
      mqRestriction(stor.zoneInput, '\\d.,/-+', { commandes: ['fraction', 'racine'] })
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      stor.zoneInput.reponse = [j3pCalculValeur(stor.objet_polynome.tab_coefs1[3])]
      stor.zoneInput.solutionSimplifiee = ['non valide', 'non valide']
      // si delta est écrit avec une somme, je compte faux (il ne faut pas déconner...)
      // s’il me donne une fraction réductible, je lui demande de simplifier (sans prendre en compte sa réponse)
      stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
      // création de la palette
      j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['fraction', 'racine'] })

      const a = j3pCalculValeur(stor.objet_polynome.tab_coefs1[2])
      const b = j3pCalculValeur(stor.objet_polynome.tab_coefs1[1])
      const c = j3pCalculValeur(stor.objet_polynome.tab_coefs1[0])
      //            me.pe_2="calcul du carre"; si resultat 2b-4ac ou 2b+4ac à partir de 30%
      me.stockage[3] = 2 * b - 4 * a * c
      me.stockage[4] = 2 * b + 4 * a * c
      //          me.pe_3="identification a b c"; si resultat a²-4bc ou c²-4ab à partir de 30%
      me.stockage[5] = a * a - 4 * b * c
      me.stockage[6] = c * c - 4 * a * b
      //            me.pe_4="calcul relatifs";  si resultat b²+4ac ou -b²-4ac ou -b²+4ac à partir de 30%
      me.stockage[7] = b * b + 4 * a * c
      me.stockage[8] = -b * b - 4 * a * c
      me.stockage[9] = -b * b + 4 * a * c

      /// //////////////////////////////////////
      /* FIN DU CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////////
      stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.zoneInput.id] })
    } else {
      // on est dans le cas où on a 2 étapes et on est à la deuxième étape
      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      const zoneExpli1 = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(zoneExpli1, '', textes.consigne2,
        { t: stor.objet_polynome.polynome, d: stor.objet_polynome.tab_coefs1[3] })
      const zoneExpli2 = j3pAddElt(stor.conteneur, 'div')
      const elt = j3pAffiche(zoneExpli2, '', textes.consigne3,
        {
          input1: { texte: '', dynamique: true, width: '12px', maxchars: 2 }
        })
      stor.zoneInput = elt.inputList[0]
      j3pRestriction(stor.zoneInput, '0-9')
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      const valeurDelta = j3pCalculValeur(stor.objet_polynome.tab_coefs1[3])
      if (Math.abs(valeurDelta) < Math.pow(10, -12)) {
        stor.zoneInput.reponse = [1]
      } else if (valeurDelta > 0) {
        stor.zoneInput.reponse = [2]
      } else {
        stor.zoneInput.reponse = [0]
      }
      j3pFocus(stor.zoneInput)
      stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.zoneInput.id] })
    }
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.zoneMD, 'div')
    stor.zoneCorr = j3pAddElt(stor.zoneMD, 'div')

    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':

      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        if (ds.nbetapes !== 1) {
          ds.nbetapes = 2
        }
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        // par convention, me.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////

      enonceMain()

      break // case "enonce":

    case 'correction':
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (!fctsValid.zones.reponseSimplifiee[0][0] || !fctsValid.zones.reponseSimplifiee[0][1]) {
            msgReponseManquante = textes.comment4
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                if ((ds.nbetapes === 1) || ((ds.nbetapes === 2) && (me.questionCourante % 2 === 1))) {
                  const valeurRep = j3pCalculValeur(fctsValid.zones.reponseSaisie[0])
                  if ((me.stockage[1] !== 2 && (Math.abs(valeurRep - me.stockage[3]) < Math.pow(10, -12))) || (me.stockage[1] !== 2 && (Math.abs(valeurRep - me.stockage[3]) < Math.pow(10, -12)))) {
                    stor.zoneCorr.innerHTML += '<br>' + textes.comment1
                  } else if ((Math.abs(valeurRep - me.stockage[5]) < Math.pow(10, -12)) || (Math.abs(valeurRep - me.stockage[6]) < Math.pow(10, -12))) {
                    stor.zoneCorr.innerHTML += '<br>' + textes.comment2
                  } else if ((Math.abs(valeurRep - me.stockage[7]) < Math.pow(10, -12)) || (Math.abs(valeurRep - me.stockage[8]) < Math.pow(10, -12)) || (Math.abs(valeurRep - me.stockage[9]) < Math.pow(10, -12))) {
                    stor.zoneCorr.innerHTML += '<br>' + textes.comment3
                  }
                  me.typederreurs[3]++
                } else {
                  me.typederreurs[4]++
                }
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)

      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems

        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bon
          me.parcours.pe = ds.pe_1
        } else {
          // on essaie de renvoyer l’erreur la plus commise qui dépasse le seuil
          if ((ds.nbetapes * me.typederreurs[3] / ds.nbitems) >= ds.seuilerreur) {
            me.parcours.pe = ds.pe_2
          } else if ((ds.nbetapes * me.typederreurs[4] / ds.nbitems) >= ds.seuilerreur) {
            me.parcours.pe = ds.pe_3
          } else {
            me.parcours.pe = ds.pe_4
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)
      break // case "navigation":
  }
}
