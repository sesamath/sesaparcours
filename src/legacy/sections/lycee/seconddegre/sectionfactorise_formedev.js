import { j3pAjouteFoisDevantX, j3pBoutonRadio, j3pBoutonRadioChecked, j3pNombre, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pRandomTab, j3pAddElt, j3pGetBornesIntervalle, j3pStyle, j3pGetNewId, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pExtraireNumDen, j3pGetLatexProduit, j3pSimplificationRacineTrinome, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    juin 2015
    Dans cette section on donne un trinôme sous forme développée.
    On demande s’il est factorisable et auquel cas, on demande la forme factorisée
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['a', '[-5;5]', 'string', 'coefficient a dans l’écriture ax^2+bx+c. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;4]|[-4;-2]" pour qu’il soit égal à 2 dans un premier temps, positif ensuite puis négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier ou fractionnaire.'],
    ['b', '[-10;10]', 'string', 'coefficient b dans l’écriture ax^2+bx+c. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['c', '[-15;15]', 'string', 'coefficient c dans l’écriture ax^2+bx+c. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['racines_simples', [true, true, false], 'array', 'Pour chaque répétition, on peut demander à ce que les racines (si elles existent) soient au pire fractionnaires en écrivant true. C’est seulement valable lorsque les coefficients ne sont pas imposés']
  ]
}
const textes = {
  titre_exo: 'Factoriser un trinôme',
  consigne1: 'Soit $£f$ la fonction définie sur $\\R$ par $£f(x)=£g$.',
  consigne2: 'Ce trinôme est-il factorisable (par autre chose qu’une constante) ?',
  oui: 'OUI       ',
  non: 'NON',
  consigne3: 'Dans ce cas, une forme factorisée du trinôme est :<br>$£f(x)=$&1&.',
  comment1: 'Il faut sélectionner l’une des deux propositions !',
  comment2: 'Ce trinôme n’est pas sous forme factorisée !',
  comment3: 'Calcule bien le discriminant !',
  comment4: 'La réponse est bien égale au polynôme attendu, mais elle n’est pas sous forme factorisée !',
  corr1: 'Le discriminant du trinôme vaut $\\Delta=b^2-4\\times a\\times c$ avec $a=£a$, $b=£b$ et $c=£c$.<br>On obtient alors $\\Delta=£d$.',
  corr2_1: 'Comme $\\Delta<0$ le trinôme n’admet pas de racine réelle. Il n’est donc pas factorisable.',
  corr2_2: 'Comme $\\Delta=0$, le trinôme admet une unique racine : $£x$.<br>Le trinôme est alors factorisable et on obtient $£f(x)=£g$.',
  corr2_3: 'Comme $\\Delta>0$, le trinôme admet deux racines réelles distinctes : $x_1=£x$ et $x_2=£y$.<br>Le trinôme est alors factorisable et s’écrit sous la forme $a(x-x_1)(x-x_2)$.<br>On obtient donc $£f(x)=£g$.',
  corr3: '<u>Remarque :</u> On pouvait se passer du discriminant ici car $x$ est clairement un facteur commun dans l’expression.'
}
/**
 * section factorise_formedev
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.laPalette)
    // la correction sera écrite en-dessous de la figure mtg32
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.petit.correction.color } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1,
      {
        d: stor.delta_txt,
        a: stor.coeffATxt,
        b: stor.coeffBTxt,
        c: stor.coeffCTxt
      })
    if (stor.delta < 0) {
      j3pAffiche(zoneExpli2, '', textes.corr2_1)
    } else if (Math.abs(stor.delta) < Math.pow(10, -12)) {
      j3pAffiche(zoneExpli2, '', textes.corr2_2,
        {
          x: stor.racine0,
          f: stor.nom_f,
          g: stor.polynome_rep
        })
    } else {
      j3pAffiche(zoneExpli2, '', textes.corr2_3,
        {
          x: stor.racine1,
          y: stor.racine2,
          f: stor.nom_f,
          g: stor.polynome_rep
        })
    }
    if ((Math.abs(j3pCalculValeur(stor.racine1)) < Math.pow(10, -12)) || (Math.abs(j3pCalculValeur(stor.racine2)) < Math.pow(10, -12))) {
      const zoneExpli3 = j3pAddElt(zoneExpli, 'div')
      j3pAffiche(zoneExpli3, '', textes.corr3)
    }
  }

  function testexpressions (poly1, poly2, variable) {
    // cette fonction vérifie si deux polynômes sont égaux
    // variable est la variable présente dans le polynôme
    const unarbre1 = new Tarbre(poly1, [variable])
    const unarbre2 = new Tarbre(poly2, [variable])
    let egaliteExp = true
    for (let i = 0; i <= 9; i++) {
      egaliteExp = (egaliteExp && (Math.abs(unarbre1.evalue([i]) - unarbre2.evalue([i])) < Math.pow(10, -12)))
    }
    return egaliteExp
  }

  function constructionTabIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).includes('|')) {
      const tabA = texteIntervalle.split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (testIntervalleFermeEntiers.test(tabA[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(tabA[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if (!tabA[k] || (tabA[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(tabA[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tabA[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(tabA[k]))) {
            // on a un nombre
            tabIntervalle.push(tabA[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if (!texteIntervalle || (texteIntervalle === '')) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (testIntervalleFermeEntiers.test(texteIntervalle)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    return tabIntervalle
  }
  function lgIntervalle (intervalle) {
    // cette fonction renvoie la longueur de l’intervalle'
    // si ce n’est pas un intervalle, alors elle renvoie 0'
    let lg = 0
    if (testIntervalleFermeEntiers.test(intervalle)) {
      // on a bien un intervalle
      const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
      const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
      lg = borneSup - borneInf
    }
    return lg
  }

  function depart () {
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAAA0+TMzNAANmcmH###8BAP8BAAAAAAAAAAAAAAAAAAAAAq4AAAGQAAAAAAAAAAAAAAABAAAAMf####8AAAABABFvYmpldHMuQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABABFvYmpldHMuQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEADm9iamV0cy5DQ2FsY3VsAP####8ABGFudW0AATIAAAABQAAAAAAAAAAAAAACAP####8ABGFkZW4AATEAAAABP#AAAAAAAAAAAAACAP####8AB3JhY2luZWEAFHNxcnQoYWJzKGFudW0vYWRlbikp#####wAAAAIAEG9iamV0cy5DRm9uY3Rpb24BAAAAAwD#####AAAAAQARb2JqZXRzLkNPcGVyYXRpb24D#####wAAAAEAFm9iamV0cy5DUmVzdWx0YXRWYWxldXIAAAABAAAABQAAAAIAAAACAP####8ABGJudW0AAi04#####wAAAAEAE29iamV0cy5DTW9pbnNVbmFpcmUAAAABQCAAAAAAAAAAAAACAP####8ABGJkZW4AATEAAAABP#AAAAAAAAAAAAACAP####8ABGNudW0AAjE1AAAAAUAuAAAAAAAAAAAAAgD#####AARjZGVuAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAxkZWx0YW51bWluaXQAI2JudW1eMiphZGVuKmNkZW4tNCphbnVtKmNudW0qYmRlbl4yAAAABAEAAAAEAgAAAAQC#####wAAAAEAEW9iamV0cy5DUHVpc3NhbmNlAAAABQAAAAQAAAABQAAAAAAAAAAAAAAFAAAAAgAAAAUAAAAHAAAABAIAAAAEAgAAAAQCAAAAAUAQAAAAAAAAAAAABQAAAAEAAAAFAAAABgAAAAcAAAAFAAAABQAAAAFAAAAAAAAAAAAAAAIA#####wAMZGVsdGFkZW5pbml0ABBiZGVuXjIqYWRlbipjZGVuAAAABAIAAAAEAgAAAAcAAAAFAAAABQAAAAFAAAAAAAAAAAAAAAUAAAACAAAABQAAAAcAAAACAP####8ACXBnY2RkZWx0YQApcGdjZChhYnMoZGVsdGFudW1pbml0KSxhYnMoZGVsdGFkZW5pbml0KSn#####AAAAAQAUb2JqZXRzLkNGb25jdGlvbjJWYXICAAAAAwAAAAAFAAAACAAAAAMAAAAABQAAAAkAAAACAP####8ACGRlbHRhbnVtABZkZWx0YW51bWluaXQvcGdjZGRlbHRhAAAABAMAAAAFAAAACAAAAAUAAAAKAAAAAgD#####AAhkZWx0YWRlbgAWZGVsdGFkZW5pbml0L3BnY2RkZWx0YQAAAAQDAAAABQAAAAkAAAAFAAAACgAAAAIA#####wACeDIAMigtYm51bS9iZGVuK3NxcnQoZGVsdGFudW0vZGVsdGFkZW4pKS8oMiphbnVtL2FkZW4pAAAABAMAAAAEAAAAAAYAAAAEAwAAAAUAAAAEAAAABQAAAAUAAAADAQAAAAQDAAAABQAAAAsAAAAFAAAADAAAAAQDAAAABAIAAAABQAAAAAAAAAAAAAAFAAAAAQAAAAUAAAACAAAAAgD#####AANheDIADGFudW0qeDIvYWRlbgAAAAQDAAAABAIAAAAFAAAAAQAAAAUAAAANAAAABQAAAAIAAAACAP####8AAngxADIoLWJudW0vYmRlbi1zcXJ0KGRlbHRhbnVtL2RlbHRhZGVuKSkvKDIqYW51bS9hZGVuKQAAAAQDAAAABAEAAAAGAAAABAMAAAAFAAAABAAAAAUAAAAFAAAAAwEAAAAEAwAAAAUAAAALAAAABQAAAAwAAAAEAwAAAAQCAAAAAUAAAAAAAAAAAAAABQAAAAEAAAAFAAAAAgAAAAIA#####wADYXgxAAxhbnVtKngxL2FkZW4AAAAEAwAAAAQCAAAABQAAAAEAAAAFAAAADwAAAAUAAAACAAAAAgD#####AAFhAAlhbnVtL2FkZW4AAAAEAwAAAAUAAAABAAAABQAAAAIAAAACAP####8AAWIACWJudW0vYmRlbgAAAAQDAAAABQAAAAQAAAAFAAAABQAAAAIA#####wABYwAJY251bS9jZGVuAAAABAMAAAAFAAAABgAAAAUAAAAHAAAAAgD#####AAVkZWx0YQAJYl4yLTQqYSpjAAAABAEAAAAHAAAABQAAABIAAAABQAAAAAAAAAAAAAAEAgAAAAQCAAAAAUAQAAAAAAAAAAAABQAAABEAAAAFAAAAEwAAAAIA#####wACeDAAGigtYm51bS9iZGVuKS8oMiphbnVtL2FkZW4pAAAABAMAAAAGAAAABAMAAAAFAAAABAAAAAUAAAAFAAAABAMAAAAEAgAAAAFAAAAAAAAAAAAAAAUAAAABAAAABQAAAAL#####AAAAAQAMb2JqZXRzLkNGb25jAP####8ABWZhY3QxABdhbnVtL2FkZW4qKHgteDEpKih4LXgyKQAAAAQCAAAABAIAAAAEAwAAAAUAAAABAAAABQAAAAIAAAAEAf####8AAAACABhvYmpldHMuQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAABQAAAA8AAAAEAQAAAAoAAAAAAAAABQAAAA0AAXgAAAAJAP####8ABWZhY3QyABgoYW51bS9hZGVuKngtYXgxKSooeC14MikAAAAEAgAAAAQBAAAABAIAAAAEAwAAAAUAAAABAAAABQAAAAIAAAAKAAAAAAAAAAUAAAAQAAAABAEAAAAKAAAAAAAAAAUAAAANAAF4AAAACQD#####AAVmYWN0MwAYKHgteDEpKihhbnVtL2FkZW4qeC1heDIpAAAABAIAAAAEAQAAAAoAAAAAAAAABQAAAA8AAAAEAQAAAAQCAAAABAMAAAAFAAAAAQAAAAUAAAACAAAACgAAAAAAAAAFAAAADgABeAAAAAkA#####wAGZmFjdDAxABJhbnVtL2FkZW4qKHgteDApXjIAAAAEAgAAAAQDAAAABQAAAAEAAAAFAAAAAgAAAAcAAAAEAQAAAAoAAAAAAAAABQAAABUAAAABQAAAAAAAAAAAAXgAAAACAP####8ABXhiaXMwABdzcXJ0KGFicyhhbnVtL2FkZW4pKSp4MAAAAAQCAAAAAwEAAAADAAAAAAQDAAAABQAAAAEAAAAFAAAAAgAAAAUAAAAVAAAAAgD#####AAZzaWduZWEAGmFicyhhbnVtL2FkZW4pLyhhbnVtL2FkZW4pAAAABAMAAAADAAAAAAQDAAAABQAAAAEAAAAFAAAAAgAAAAQDAAAABQAAAAEAAAAFAAAAAgAAAAkA#####wAGZmFjdDAyABpzaWduZWEqKHJhY2luZWEqeC14YmlzMCleMgAAAAQCAAAABQAAABsAAAAHAAAABAEAAAAEAgAAAAUAAAADAAAACgAAAAAAAAAFAAAAGgAAAAFAAAAAAAAAAAABeAAAAAkA#####wADcmVwABEyKih4LTMvMikqKHgtNS8yKQAAAAQCAAAABAIAAAABQAAAAAAAAAAAAAAEAQAAAAoAAAAAAAAABAMAAAABQAgAAAAAAAAAAAABQAAAAAAAAAAAAAAEAQAAAAoAAAAAAAAABAMAAAABQBQAAAAAAAAAAAABQAAAAAAAAAAAAXj#####AAAAAQAXb2JqZXRzLkNUZXN0RXF1aXZhbGVuY2UA#####wAIZWdhbGl0ZTEAAAAWAAAAHQEAAAAACwD#####AAhlZ2FsaXRlMgAAABcAAAAdAQAAAAALAP####8ACGVnYWxpdGUzAAAAGAAAAB0BAAAAAAsA#####wAJZWdhbGl0ZTAxAAAAGQAAAB0BAAAAAAsA#####wAJZWdhbGl0ZTAyAAAAHAAAAB0BAP####8AAAACABNvYmpldHMuQ0NvbW1lbnRhaXJlAP####8AAAAAAAIAAAH#####AkBEgAAAAAAAQFZAAAAAAAAAAAAAAAAAAAAAAFNlZ2FsaXTDqSAxIDogI1ZhbChlZ2FsaXRlMSkKZWdhbGl0w6kgMiA6ICNWYWwoZWdhbGl0ZTIpCmVnYWxpdMOpIDMgOiAjVmFsKGVnYWxpdGUzKf####8AAAACAA1vYmpldHMuQ0xhdGV4AP####8AAAAAAAIAAAH#####AkBqYAAAAAAAQFpAAAAAAAAAAAAAAAAAAAAAABRyZXAoeCk9XEZvclNpbXB7cmVwfQAAAA0A#####wAAAAAAAgAAAf####8CQGogAAAAAABARQAAAAAAAAAAAAAAAAAAAAAAGGZhY3QxKHgpPVxGb3JTaW1we2ZhY3QxfQAAAAkA#####wAIZmFjdGJpczEAd2FudW0vYWRlbiooeC0oLWJudW0vYmRlbi1zcXJ0KGRlbHRhbnVtL2RlbHRhZGVuKSkvKDIqYW51bS9hZGVuKSkqKHgtKC1ibnVtL2JkZW4rc3FydChkZWx0YW51bS9kZWx0YWRlbikpLygyKmFudW0vYWRlbikpAAAABAIAAAAEAgAAAAQDAAAABQAAAAEAAAAFAAAAAgAAAAQBAAAACgAAAAAAAAAEAwAAAAQBAAAABgAAAAQDAAAABQAAAAQAAAAFAAAABQAAAAMBAAAABAMAAAAFAAAACwAAAAUAAAAMAAAABAMAAAAEAgAAAAFAAAAAAAAAAAAAAAUAAAABAAAABQAAAAIAAAAEAQAAAAoAAAAAAAAABAMAAAAEAAAAAAYAAAAEAwAAAAUAAAAEAAAABQAAAAUAAAADAQAAAAQDAAAABQAAAAsAAAAFAAAADAAAAAQDAAAABAIAAAABQAAAAAAAAAAAAAAFAAAAAQAAAAUAAAACAAF4AAAADQD#####AAAAAAACAAAB#####wJAayAAAAAAAEBnIAAAAAAAAAAAAAAAAAAAAAAgZmFjdGJpczEoeCkgPSBcRm9yU2ltcHtmYWN0YmlzMX0AAAALAP####8AC2VnYWxpdGViaXMxAAAAJgAAAB0BAQAAAAwA#####wAAAAAAAgAAAf####8CQEyAAAAAAABAZ0AAAAAAAAAAAAAAAAAAAAAAH2VnYWxpdGViaXMxIDogI1ZhbChlZ2FsaXRlYmlzMSkAAAACAP####8ABXhudW0xACItYm51bS9iZGVuLXNxcnQoZGVsdGFudW0vZGVsdGFkZW4pAAAABAEAAAAGAAAABAMAAAAFAAAABAAAAAUAAAAFAAAAAwEAAAAEAwAAAAUAAAALAAAABQAAAAwAAAACAP####8ABXhudW0yACItYm51bS9iZGVuK3NxcnQoZGVsdGFudW0vZGVsdGFkZW4pAAAABAAAAAAGAAAABAMAAAAFAAAABAAAAAUAAAAFAAAAAwEAAAAEAwAAAAUAAAALAAAABQAAAAwAAAACAP####8ABHhkZW4ACzIqYW51bS9hZGVuAAAABAMAAAAEAgAAAAFAAAAAAAAAAAAAAAUAAAABAAAABQAAAAIAAAAJAP####8ACGZhY3R0ZXIxACdhbnVtL2FkZW4qKHgteG51bTEveGRlbikqKHgteG51bTIveGRlbikAAAAEAgAAAAQCAAAABAMAAAAFAAAAAQAAAAUAAAACAAAABAEAAAAKAAAAAAAAAAQDAAAABQAAACoAAAAFAAAALAAAAAQBAAAACgAAAAAAAAAEAwAAAAUAAAArAAAABQAAACwAAXgAAAANAP####8AAAAAAAIAAAH#####AkBsIAAAAAAAQHHwAAAAAAAAAAAAAAAAAAAAACBmYWN0dGVyMSh4KSA9IFxGb3JTaW1we2ZhY3R0ZXIxfQAAAAsA#####wALZWdhbGl0ZXRlcjEAAAAtAAAAHQEBAAAADAD#####AAAAAAACAAAB#####wJASYAAAAAAAEBysAAAAAAAAAAAAAAAAAAAAAAfZWdhbGl0ZXRlcjEgOiAjVmFsKGVnYWxpdGV0ZXIxKf###############w=='
    stor.liste = stor.mtgAppLecteur.createList(txtFigure)
  }

  function transformeExp (texte, type) {
    // dans texte, si type = "fraction", cette fonction transforme tout ce qui est de la forme (a)/(b) en le résultat du quotient a/b au formal décimal
    // si type = "racine_trinome", cette fonction transforme tout ce qui est de la forme (a+sqrt(b))/c en son résultat au format décimal
    // si type = "sqrt", cette fonction transforme tout ce qui est de la forme sqrt(a) en son résultat au format décimal
    let newTexte = texte
    let expressionCherchee
    if (type === 'fraction') {
      expressionCherchee = /\(-?[0-9.,]+\)\/\([0-9]+\)/g // new RegExp('\\(\\-?[0-9\\.,]{1,}\\)/\\([0-9]{1,}\\)', 'ig')
    } else if ((type === 'racine_trinome') || (type === 'racine trinome')) {
      expressionCherchee = /\(-?[0-9.]+[+-][0-9]*sqrt\([0-9.]+\)\)\/\([0-9.]+\)/g // new RegExp('\\(\\-?[0-9\\.]{1,}[\\+|\\-][0-9]{0,}sqrt\\([0-9\\.]{1,}\\)\\)/\\([0-9\\.]{1,}\\)', 'ig')
    } else if ((type === 'racine') || (type === 'sqrt')) {
      expressionCherchee = /sqrt\([0-9]+\)/g // new RegExp('sqrt\\([0-9]{1,}\\)', 'ig')
    }
    if (expressionCherchee.test(texte)) {
      const tabExp = texte.match(expressionCherchee)
      for (let i = 0; i < tabExp.length; i++) {
        newTexte = newTexte.replace(tabExp[i], String(j3pCalculValeur(tabExp[i])))
      }
    }
    return newTexte
  }

  function enonceMain () {
    depart()
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // dans ce type de section, je mets plutôt les consignes à droite
    const leB = stor.coef_b[me.questionCourante - 1]
    const leC = stor.coef_c[me.questionCourante - 1]
    const leA = stor.coef_a[me.questionCourante - 1]
    if ((leA === '[0;0]') || (leA === 0) || (leA === '0')) {
      j3pShowError('le coefficient a ne peut être nul')
      stor.coef_a[me.questionCourante - 1] = '[-5;5]'
    }
    let coeffA, coeffB, coeffC
    let coeffATxt
    let coeffBTxt
    let coeffCTxt
    let aImpose = !testIntervalleFermeEntiers.test(leA)
    if (!aImpose) aImpose = (lgIntervalle(leA) <= 1)
    let bImpose = !testIntervalleFermeEntiers.test(leB)
    if (!bImpose) bImpose = (lgIntervalle(leB) <= 1)
    let cImpose = !testIntervalleFermeEntiers.test(leC)
    if (!cImpose) cImpose = (lgIntervalle(leC) <= 1)
    let delta
    if (!aImpose && !bImpose && !cImpose && ds.racines_simples[me.questionCourante - 1] && !stor.trinome_sansRacine[me.questionCourante - 1]) {
      // on impose aucun coef, donc on va plutôt chercher des racines de manière aléatoire, puis calculer les coefs a, b et c (entiers)
      const maRacine1 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 4)
      let maRacine2
      if (j3pGetRandomInt(0, 7) === 0) {
        // une chance sur 8 que ce soit une racine double
        maRacine2 = maRacine1
      } else {
        do {
          maRacine2 = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 4)
          // je veux quand même 2 racines de valeurs absolues différentes
        } while (Math.abs(Math.abs(maRacine1) - Math.abs(maRacine2)) < Math.pow(10, -12))
      }
      do {
        coeffA = j3pGetRandomInt(-4, 4)
      } while (Math.abs(coeffA) < Math.pow(10, -12))
      coeffATxt = String(coeffA)
      coeffB = coeffA * maRacine2 + maRacine1
      coeffBTxt = j3pGetLatexSomme(j3pGetLatexProduit(String(coeffATxt), String(maRacine2)), String(maRacine1))
      coeffC = maRacine1 * maRacine2
      coeffCTxt = j3pGetLatexProduit(String(maRacine1), String(maRacine2))
      delta = Math.pow(coeffB, 2) - 4 * coeffA * coeffC
    } else {
      let recommence
      do {
        let mauvaisTrinome
        do {
          let sansRacine = false
          do {
            if (leA.includes('frac')) {
              // c’est un nb en écriture fractionnaire'
              coeffA = j3pNombre(leA.split('|')[1])
              coeffATxt = leA.split('|')[0]
            } else if (!isNaN(j3pNombre(leA))) {
              coeffA = j3pNombre(leA)
              coeffATxt = coeffA
            } else {
              const [lemin, lemax] = j3pGetBornesIntervalle(leA)
              coeffA = j3pGetRandomInt(lemin, lemax)
              coeffATxt = coeffA
            }
          } while (Math.abs(coeffA) < Math.pow(10, -12))
          if (leB.includes('frac')) {
            // c’est un nb en écriture fractionnaire'
            coeffB = j3pNombre(leB.split('|')[1])
            coeffBTxt = leB.split('|')[0]
          } else if (!isNaN(j3pNombre(leB))) {
            coeffB = j3pNombre(leB)
            coeffBTxt = coeffB
          } else {
            const [lemin, lemax] = j3pGetBornesIntervalle(leB)
            coeffB = j3pGetRandomInt(lemin, lemax)
            coeffBTxt = coeffB
          }
          if (leC.includes('frac')) {
            // c’est un nb en écriture fractionnaire'
            coeffC = j3pNombre(leC.split('|')[1])
            coeffCTxt = leC.split('|')[0]
          } else if (!isNaN(j3pNombre(leC))) {
            coeffC = j3pNombre(leC)
            coeffCTxt = coeffC
          } else {
            const [lemin, lemax] = j3pGetBornesIntervalle(leC)
            coeffC = j3pGetRandomInt(lemin, lemax)
            coeffCTxt = coeffC
          }
          delta = Math.pow(coeffB, 2) - 4 * coeffA * coeffC
          if (!aImpose && !bImpose && !cImpose) {
            if (delta < 0) {
              sansRacine = true
            }
          }
          mauvaisTrinome = (sansRacine && !stor.trinome_sansRacine[me.questionCourante - 1])
        } while (mauvaisTrinome)
        let sqrtPresente = false
        // si le trinôme admet 2 racines, on vérifie si elles sont compliquées
        if (delta > 0) {
          if (Math.abs(Math.sqrt(delta) - Math.round(Math.sqrt(delta))) > Math.pow(10, -12)) {
            sqrtPresente = true
          }
        }
        recommence = (((coeffB === 0) && ((leB !== '[0;0]') && (leB !== '0') && (leB !== 0))) || ((coeffC === 0) && ((leC !== '[0;0]') && (leC !== '0') && (leC !== 0))))

        if (!aImpose || !bImpose || !cImpose) {
          // tous les coefs ne sont pas imposés donc je regarde si les racines doivent être simples ou non
          if (ds.racines_simples[me.questionCourante - 1]) {
            // on veut des racines simples
            if (delta > 0) {
              if (sqrtPresente) {
                // la racine n’est pas simple
                recommence = true
              }
            }
          } else {
            // on veut des racines plus compliquées (avec un radical)
            if (delta > 0) {
              if (!sqrtPresente) {
                // la racine n’est pas simple
                recommence = true
              }
            }
          }
        }
      } while (recommence)
    }
    if (delta > 0) {
      stor.racine1 = j3pSimplificationRacineTrinome(coeffBTxt, '+', delta, coeffATxt)
      stor.racine2 = j3pSimplificationRacineTrinome(coeffBTxt, '-', delta, coeffATxt)
      me.logIfDebug('racine1 : ' + stor.racine1 + '  racine2 : ' + stor.racine2)
    } else if (Math.abs(delta) < Math.pow(10, -12)) {
      stor.racine0 = j3pGetLatexQuotient(j3pGetLatexProduit(-1, coeffBTxt), j3pGetLatexProduit(2, coeffATxt))
      me.logIfDebug('racine0 : ' + stor.racine0)
    }
    stor.nom_f = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
    stor.fdex = j3pGetLatexMonome(1, 2, coeffATxt) + j3pGetLatexMonome(2, 1, coeffBTxt) + j3pGetLatexMonome(3, 0, coeffCTxt)
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons1, '', textes.consigne1, { f: stor.nom_f, g: stor.fdex })
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(zoneCons2, '', textes.consigne2)
    const tabA = j3pExtraireNumDen(coeffATxt)
    const aNum = (tabA[0]) ? tabA[1] : coeffA
    const aDen = (tabA[0]) ? tabA[2] : 1
    stor.liste.giveFormula2('anum', String(aNum))
    stor.liste.giveFormula2('aden', String(aDen))
    const tabB = j3pExtraireNumDen(coeffBTxt)
    const bNum = (tabB[0]) ? tabB[1] : coeffB
    const bDen = (tabB[0]) ? tabB[2] : 1
    stor.liste.giveFormula2('bnum', String(bNum))
    stor.liste.giveFormula2('bden', String(bDen))
    const tabC = j3pExtraireNumDen(coeffCTxt)
    const cNum = (tabC[0]) ? tabC[1] : coeffC
    const cDen = (tabC[0]) ? tabC[2] : 1
    stor.liste.giveFormula2('cnum', String(cNum))
    stor.liste.giveFormula2('cden', String(cDen))
    if (Math.abs(coeffA - 1) < Math.pow(10, -12)) {
      stor.polynome_rep = ''
    } else if (Math.abs(coeffA + 1) < Math.pow(10, -12)) {
      stor.polynome_rep = '-'
    } else {
      stor.polynome_rep = coeffATxt
    }
    if (delta > 0) {
      if (stor.racine1.includes('sqrt')) {
        stor.polynome_rep += '\\left(x-' + stor.racine1 + '\\right)'
        stor.polynome_rep += '\\left(x-' + stor.racine2 + '\\right)'
      } else {
        if (Math.abs(j3pCalculValeur(stor.racine1)) < Math.pow(10, -12)) {
          stor.polynome_rep += 'x'
        } else {
          stor.polynome_rep += '\\left(x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', stor.racine1)) + '\\right)'
        }
        stor.polynome_rep += '\\left(x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', stor.racine2)) + '\\right)'
      }
      stor.polynome_reponse = stor.polynome_rep
    } else if (Math.abs(delta) < Math.pow(10, -12)) {
      // stor.racine0 = j3pGetLatexQuotient(j3pGetLatexProduit(-1, coeffBTxt),j3pGetLatexProduit(2,coeffATxt));
      // me.logIfDebug("racine0 : "+stor.racine0);
      stor.polynome_rep += '\\left(x' + j3pGetLatexMonome(2, 0, stor.racine0) + '\\right)^2'
      stor.polynome_reponse = stor.polynome_rep
    }
    if (delta >= 0) {
      while (stor.polynome_reponse.includes('\\left')) {
        stor.polynome_reponse = stor.polynome_reponse.replace('\\left', '')
      }
      while (stor.polynome_reponse.includes('\\right')) {
        stor.polynome_reponse = stor.polynome_reponse.replace('\\right', '')
      }
    }
    stor.delta_txt = j3pGetLatexSomme(j3pGetLatexProduit(coeffBTxt, coeffBTxt), j3pGetLatexProduit(-4, j3pGetLatexProduit(coeffATxt, coeffCTxt)))
    stor.delta = delta
    stor.coeffATxt = coeffATxt
    stor.coeffBTxt = coeffBTxt
    stor.coeffCTxt = coeffCTxt

    stor.liste.calculateNG()

    // console.log("valeur de a dans mtg32:"+stor.liste.valueOf("a")+"   b:"+stor.liste.valueOf("b")+"   c:"+stor.liste.valueOf("c"));

    const zoneCons3 = j3pAddElt(stor.conteneur, 'div', '&nbsp;&nbsp;&nbsp;&nbsp;')
    stor.zoneCons4 = j3pAddElt(stor.conteneur, 'div')
    stor.nameRadio = j3pGetNewId('choix')
    stor.idsRadio = [1, 2].map(i => j3pGetNewId('radio' + i))
    j3pBoutonRadio(zoneCons3, stor.idsRadio[0], stor.nameRadio, 0, textes.oui)
    j3pBoutonRadio(zoneCons3, stor.idsRadio[1], stor.nameRadio, 1, textes.non)
    stor.zone_reponse = false
    stor.laPalette = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.laPalette, { paddingTop: '10px' })
    j3pElement(stor.idsRadio[0]).onclick = function () {
      if (!stor.zone_reponse) {
        j3pEmpty(stor.zoneCons4)
        const elt = j3pAffiche(stor.zoneCons4, '', textes.consigne3,
          {
            f: stor.nom_f,
            inputmq1: { texte: '' }
          })
        stor.zoneInput = elt.inputmqList[0]
        j3pEmpty(stor.laPalette)
        j3pPaletteMathquill(stor.laPalette, stor.zoneInput, {
          liste: ['puissance', 'fraction', 'racine']
        })
        mqRestriction(stor.zoneInput, '\\d,./-*x^+)(', { commandes: ['puissance', 'fraction', 'racine'] })
        const mesZonesSaisie = [stor.zoneInput.id]
        stor.fctsValid = new ValidationZones({
          parcours: me,
          zones: mesZonesSaisie,
          validePerso: mesZonesSaisie
        })
        stor.zone_reponse = true
        j3pFocus(stor.zoneInput)
      }
    }
    j3pElement(stor.idsRadio[1]).onclick = function () {
      j3pEmpty(stor.zoneCons4)
      j3pEmpty(stor.laPalette)
      stor.zone_reponse = false
    }
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.coef_a = constructionTabIntervalle(ds.a, '[-5;5]')
        stor.coef_b = constructionTabIntervalle(ds.b, '[-10;10]')
        stor.coef_c = constructionTabIntervalle(ds.c, '[-15;15]')
        // c’est ici qu’on doit chercher à quel endroit on peut mettre éventuellement un trinôme sans racine
        stor.trinome_sansRacine = []
        for (let i = 0; i < ds.nbrepetitions; i++) {
          stor.trinome_sansRacine.push(false)
        }
        if (ds.nbrepetitions >= 3) {
          // on essaie dans ce cas de mettre un polynôme sans racine si les coefs ne sont pas imposés
          stor.tabCoefImposes = []
          let nbCoefImposes = 0
          let aImpose, bImpose, cImpose
          for (let i = 0; i < ds.nbrepetitions; i++) {
            if (ds.racines_simples[i] === undefined) ds.racines_simples[i] = true
            aImpose = !testIntervalleFermeEntiers.test(stor.coef_a[i])
            if (!aImpose) {
              aImpose = (lgIntervalle(stor.coef_a[i]) <= 1)
            }
            bImpose = !testIntervalleFermeEntiers.test(stor.coef_b[i])
            if (!bImpose) {
              bImpose = (lgIntervalle(stor.coef_b[i]) <= 1)
            }
            cImpose = !testIntervalleFermeEntiers.test(stor.coef_c[i])
            if (!cImpose) {
              cImpose = (lgIntervalle(stor.coef_c[i]) <= 1)
            }
            if (!aImpose && !bImpose && !cImpose) {
              stor.tabCoefImposes[i] = ds.racines_simples[i]
            } else {
              stor.tabCoefImposes[i] = true
            }
            if (!stor.tabCoefImposes[i]) {
              nbCoefImposes++
            }
          }
          // console.log("stor.tabCoefImposes:"+stor.tabCoefImposes)
          if (nbCoefImposes <= ds.nbrepetitions - 2) {
            // dans au moins 2 cas, les coefs ne sont pas imposés
            // Donc on peut se permettre de mettre sur l’un d’eux un trinôme sans racine
            let alea
            do {
              alea = j3pGetRandomInt(0, (ds.nbrepetitions - 1))
            } while (!stor.tabCoefImposes[alea])
            stor.trinome_sansRacine[alea] = true
          }
        }
        // console.log("stor.trinome_sansRacine:"+stor.trinome_sansRacine)

        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        let reponse = {
          aRepondu: false,
          bonneReponse: false
        }
        reponse.aRepondu = (j3pBoutonRadioChecked(stor.nameRadio)[0] > -1)
        const boutonCoche = reponse.aRepondu
        let repeleve
        if (reponse.aRepondu) {
        // je teste ici la réponse de l’élève
        // un bouton radio a été sélectionné
          stor.erreur_ouinon = false
          if (j3pBoutonRadioChecked(stor.nameRadio)[0] === 0) {
            const fctsValid = stor.fctsValid
            reponse = fctsValid.validationGlobale()
            if (reponse.aRepondu) {
            // c’est que la zone de saisie a été complétée
              if (stor.delta < 0) {
              // le trinôme n’est pas factorisable, donc la réponse est nécessairement fausse.
                stor.erreur_ouinon = true
                reponse.bonneReponse = false
              } else {
              // le polynôme est bien factorisable
                repeleve = j3pMathquillXcas(j3pValeurde(stor.zoneInput))
                me.logIfDebug('repeleve:', repeleve)
                // si j’ai 1(...), il ne va pas l’accepter, or j’aimerais bien qu’il le fasse...
                while (repeleve.includes('1*(')) {
                  repeleve = repeleve.replace('1*(', '(')
                }
                me.logIfDebug('repeleve après:', repeleve)

                repeleve = j3pAjouteFoisDevantX(repeleve, 'x')
                repeleve = transformeExp(repeleve, 'fraction')
                // console.log("repeleve après frac:"+repeleve)
                if (repeleve.includes('sqrt')) {
                // dans les racines, il y a une racine carrée
                  repeleve = transformeExp(repeleve, 'racine_trinome')
                  while (repeleve.includes('--')) {
                    repeleve = repeleve.replace('--', '+')
                  }
                  // console.log('repeleve:', repeleve)
                  // malgré tout je peux avoir d’autres soucis, donc j’y regarde de plus près
                  const facteurs = []
                  const newFacteurs = []
                  let parInit = -1
                  let nbParOuvertes = 0
                  for (let i = 0; i < repeleve.length; i++) {
                    if (repeleve.charAt(i) === '(') {
                      nbParOuvertes++
                      if (parInit === -1) parInit = i
                    }
                    if (repeleve.charAt(i) === ')') {
                      nbParOuvertes--
                      if (nbParOuvertes === 0) {
                      // c’est que cette parenthèse ferme la parenthèse qui désigne un facteur (même s’il faudra que je teste la présence de x)
                        facteurs.push(repeleve.substring(parInit + 1, i))
                        parInit = -1
                      }
                    }
                  }
                  // pour chaque facteur, je recherche le monome de degré 1
                  const monomeDegre1 = /[+-]?[0-9.]*\*?x/ // new RegExp('[\\+|\\-]?[0-9\\.]{0,}\\*?x', 'i')// de la forme ax+/-...
                  const monomeDegre2 = /-?sqrt\([0-9]\)\*?x/ // new RegExp('\\-?sqrt\\([0-9]\\)\\*x', 'i')// de la forme sqrt(a)x+/-...
                  const monomeDegre3 = /-?\([0-9]\)\/\([0-9]\)\*?x/ // new RegExp('\\-?\\([0-9]\\)/\\([0-9]\\)\\*x', 'i')// de la forme (a/b)x+/-...
                  for (let i = 0; i < facteurs.length; i++) {
                    if (((monomeDegre1.test(facteurs[i])) || (monomeDegre2.test(facteurs[i])) || (monomeDegre3.test(facteurs[i]))) && !(facteurs[i].includes('x^'))) {
                      let tabMonome
                      if (monomeDegre1.exec(facteurs[i])) {
                        tabMonome = facteurs[i].match(monomeDegre1)
                      } else if (monomeDegre2.test(facteurs[i])) {
                        tabMonome = facteurs[i].match(monomeDegre2)
                      } else if (monomeDegre3.test(facteurs[i])) {
                        tabMonome = facteurs[i].match(monomeDegre3)
                      }
                      let resteMonome = facteurs[i]
                      newFacteurs[i] = ''
                      for (let j = 0; j < tabMonome.length; j++) {
                        newFacteurs[i] += tabMonome[j]
                        resteMonome = resteMonome.replace(tabMonome[j], '')
                      }
                      resteMonome = String(j3pCalculValeur(resteMonome))
                      if (resteMonome.charAt(0) !== '-') {
                      // c’est que le nombre est positif
                        newFacteurs[i] += '+'
                      }
                      newFacteurs[i] += resteMonome
                    } else {
                      newFacteurs[i] = facteurs[i]
                    }
                  }
                  me.logIfDebug('facteurs:' + facteurs + '    newFacteurs:' + newFacteurs)
                  for (let i = 0; i < facteurs.length; i++) {
                    repeleve = repeleve.replace(facteurs[i], transformeExp(newFacteurs[i], 'sqrt'))
                  }
                }
                me.logIfDebug('repeleve après gestion signes:' + repeleve)
                stor.liste.giveFormula2('rep', repeleve)
                stor.liste.calculateNG()
                me.logIfDebug('reponse de l’élève envoyée à mtg32:' + stor.liste.getFormula('rep'))
                if (Math.abs(stor.delta) < Math.pow(10, -12)) {
                // le trinôme n’admet qu’une seule racine
                  reponse.bonneReponse = !!(((stor.liste.valueOf('egalite01') === 1) || (stor.liste.valueOf('egalite02') === 1)))
                  me.logIfDebug('reponse attendue par mtg32:' + stor.liste.getForSimp('fact01') + '  ou   ' + stor.liste.getForSimp('fact02'))
                } else {
                // le trinôme admet 2 racines
                  reponse.bonneReponse = !!(((stor.liste.valueOf('egalite1') === 1) || (stor.liste.valueOf('egalite2') === 1) || (stor.liste.valueOf('egalite3') === 1)))
                  me.logIfDebug('reponse attendue par mtg32:' + stor.liste.getForSimp('fact1') + '  ou  ' + stor.liste.getForSimp('fact2') + '  ou  ' + stor.liste.getForSimp('fact3'))
                }
              }
              if (!reponse.bonneReponse) {
                fctsValid.zones.bonneReponse[0] = false
              }
              fctsValid.coloreUneZone(stor.zoneInput.id)
            }
          } else {
            if (stor.delta >= 0) {
            // le trinôme est factorisable, donc la réponse est nécessairement fausse.
              stor.erreur_ouinon = true
              reponse.bonneReponse = false
            } else {
              reponse.bonneReponse = true
            }
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (!boutonCoche) msgReponseManquante = textes.comment1
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            j3pDesactive(stor.idsRadio[0])
            j3pDesactive(stor.idsRadio[1])
            afficheCorrection(true)// affichage de la correction
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              j3pDesactive(stor.idsRadio[0])
              j3pDesactive(stor.idsRadio[1])
              afficheCorrection(false)// affichage de la correction
            } else {
              // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // on peut affiner le commentaire si on veut
              if (stor.erreur_ouinon) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment3
              } else {
                if ((stor.delta >= 0) && (j3pBoutonRadioChecked(stor.nameRadio)[0] === 0)) {
                  if (testexpressions(repeleve, j3pMathquillXcas(stor.polynome_reponse), 'x')) {
                    stor.zoneCorr.innerHTML += '<br>' + textes.comment4
                  }
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                return me.finCorrection()
              } else {
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                j3pDesactive(stor.idsRadio[0])
                j3pDesactive(stor.idsRadio[1])
                afficheCorrection(false)// affichage de la correction
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
