import { j3pAddElt, j3pAjouteFoisDevantX, j3pBarre, j3pNombre, j3pDetruit, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pStyle, j3pGetBornesIntervalle, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pExtraireNumDen, j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { testIntervalleFermeEntiers } from 'src/lib/utils/regexp'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    février 2014
    On donne une fonction polynôme de degré 2 sous forme développée et on demande la forme canonique
    Ici on ne donne qu’une zone de saisie contrairement à dvptverscanonique
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['nbVerif', 4, 'entier', 'Nombre de vérification d’étapes intermédiaires'],
    ['typeCorrection', 'alpha', 'liste', 'La correction peut être présentée sous deux formes&nbsp;:<br/>- en reconnaissant le début du développement d’une identité remarquable (en donnant la valeur "IR")&nbsp;;- à l’aide de la formule alpha=-b/(2a) (en donnant la valeur "alpha").', ['IR', 'alpha']],
    ['a', '[-4;4]', 'string', 'coefficient a dans l’écriture a(x-alpha)^2+beta, a différent de 0. On peut aussi imposer des intervalles différents d’une répétition à l’autre en écrivant par exemple "[2;2]|[2;4]|[-4;-2]" pour qu’il soit égal à 2 dans un premier temps, positif ensuite puis négatif. A la place d’un intervalle, on peut aussi mettre un nombre entier ou fractionnaire.'],
    ['alpha', '[-5;5]', 'string', 'coefficient alpha dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['beta', '[-5;5]', 'string', 'coefficient beta dans l’écriture a(x-alpha)^2+beta. Comme pour a, on peut imposer des intervalles différents d’une répétition à l’autre, de même qu’un nombre.'],
    ['seuilreussite', 0.8, 'reel', 'on considère que l’élève a réussi car il a une note supérieure ou égale à 8/10'],
    ['seuilerreur', 0.5, 'reel', 'pour repérer les erreurs les plus courantes (dans plus de 60% des cas)']
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'Problème de signes' },
    { pe_3: 'Problème avec l’extremum' },
    { pe_4: 'Problème avec coefficient a' },
    { pe_5: 'insuffisant' }
  ]
}

const textes = {
  titre_exo: 'Forme développée et canonique',
  consigne1: '$£f$ est une fonction polynôme de degré 2 définie sur $\\R$ par $£g$.',
  consigne2: 'Compléter $£f(x)$ sous forme canonique.',
  phrase_erreur1: 'Il y a un ou plusieurs problème(s) de signe&nbsp;!',
  phrase_erreur3: 'Tu confonds l’abscisse et l’ordonnée du sommet de la parabole représentant la fonction&nbsp;!',
  phrase_erreur4: 'Ce n’est pas la forme canonique du trinôme ou elle peut être simplifiée&nbsp;!',
  phrase_entrer: 'Cette expression est bien égale au polynôme initial, mais ce n’est pas encore la forme canonique&nbsp;!',
  comment: 'Il est possible de vérifier des étapes intermédiaires avec la touche Entrée (le bouton OK valide la réponse finale).',
  verif: 'Il reste £v vérification.',
  verif2: 'Il reste £v vérifications.',
  corr1: '$£f(x)=ax^2+bx+c$ avec $a=£a$, $b=£b$ et $c=£c$.',
  corr2: "$£f(x)$ s'écrit sous la forme $a(x-\\alpha)^2+\\beta$ où $a=£a$ et $(\\alpha;\\beta)$ sont les coordonnées du sommet de la parabole.",
  corr3: 'On a $\\alpha=\\frac{-b}{2a}=£a$ et $\\beta=£f(\\alpha)=£b$.',
  corr4: 'Ainsi $£f(x)=£c$.',
  corr10: 'On factorise tout d’abord $£f(x)$ par $£a$&nbsp;:',
  corr11: 'On reconnait alors le début du développement d’une identité remarquable&nbsp;:',
  corr11_2: 'On reconnait dans $£f(x)$ le début du développement d’une identité remarquable&nbsp;:',
  corr12: 'Ainsi $£f(x)=£{c1}=£c$.'
}
/**
 * section dvptverscanonique2
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function testexpressions (poly1, poly2, variable) {
    // cette fonction vérifie si deux polynômes sont égaux
    // variable est la variable présente dans le polynôme
    const unarbre1 = new Tarbre(poly1, [variable])
    const unarbre2 = new Tarbre(poly2, [variable])
    let egaliteExp = true
    for (let i = 0; i <= 9; i++) {
      egaliteExp = (egaliteExp && (Math.abs(unarbre1.evalue([i]) - unarbre2.evalue([i])) < Math.pow(10, -12)))
    }
    return egaliteExp
  }
  function afficheCorrection (bonneReponse) {
    j3pDetruit(stor.laPalette)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.petit.correction.color } })
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    if (ds.typeCorrection === 'IR') {
      if (Math.abs(me.stockage[11] - 1) < Math.pow(10, -12)) { // a vaut 1, donc il n’y a pas de factorisation
        j3pAffiche(stor.zoneExpli1, '', textes.corr11_2,
          {
            f: me.stockage[18]
          })
      } else {
        j3pAffiche(stor.zoneExpli1, '', textes.corr10,
          {
            f: me.stockage[18],
            a: me.stockage[11]
          })
        j3pAffiche(stor.zoneExpli2, '', '$£f(x)=' + stor.factorisationa + '$', { f: me.stockage[18] })
        j3pAffiche(stor.zoneExpli3, '', textes.corr11)
      }
      j3pAffiche(stor.zoneExpli4, '', '$' + stor.debutdevIR + '=' + stor.debutdevIR2 + '=' + stor.debutdevIR3 + '$')
      j3pAffiche(stor.zoneExpli5, '', textes.corr12,
        {
          f: me.stockage[18],
          c: me.stockage[20],
          c1: stor.formeCanoniqueInit
        })
    } else {
      j3pAffiche(stor.zoneExpli1, '', textes.corr1,
        {
          f: me.stockage[18],
          a: me.stockage[11],
          b: me.stockage[16],
          c: me.stockage[17]
        })
      j3pAffiche(stor.zoneExpli2, '', textes.corr2,
        {
          f: me.stockage[18],
          a: me.stockage[11]
        })
      const formeCano = me.stockage[20]
      j3pAffiche(stor.zoneExpli3, '', textes.corr3 + '\n' + textes.corr4,
        {
          a: me.stockage[12],
          b: me.stockage[13],
          f: me.stockage[18],
          c: formeCano
        })
    }
  }
  function constructionTabIntervalle (texteIntervalle, intervalleDefaut) {
    // cette fonction sera appliquée à a, alpha et beta pour avoir les intervalles dans lesquels les chercher d’une question à l’autre'
    const nbFracReg = /-?[0-9]+\/[0-9]+/g // new RegExp('\\-?[0-9]{1,}/[0-9]{1,}')
    let k
    const tabIntervalle = []
    let tabDecomp, monnum, monden, valFrac
    if (String(texteIntervalle).includes('|')) {
      const tabA = texteIntervalle.split('|')
      for (k = 0; k < ds.nbrepetitions; k++) {
        if (testIntervalleFermeEntiers.test(tabA[k])) {
          // c’est bien un intervalle'
          tabIntervalle.push(tabA[k])
        } else {
          // peut être est-ce tout simplement un nombre :
          if (!tabA[k] || (tabA[k] === '')) {
            tabIntervalle.push(intervalleDefaut)
          } else if (nbFracReg.test(tabA[k])) {
            // on a un nb fractionnaire
            // on récupère numérateur et dénominateur
            tabDecomp = tabA[k].split('/')
            monnum = j3pNombre(tabDecomp[0])
            monden = j3pNombre(tabDecomp[1])
            valFrac = monnum / monden
            if (monnum < 0) {
              monnum = -monnum
            }
            tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
          } else if (!isNaN(j3pNombre(tabA[k]))) {
            // on a un nombre
            tabIntervalle.push(tabA[k])
          } else {
            tabIntervalle.push(intervalleDefaut)
          }
        }
      }
    } else {
      if (!texteIntervalle || (texteIntervalle === '')) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      } else if (testIntervalleFermeEntiers.test(texteIntervalle)) {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else if (nbFracReg.test(texteIntervalle)) {
        // on a un nb fractionnaire
        // on récupère numérateur et dénominateur
        tabDecomp = texteIntervalle.split('/')
        monnum = j3pNombre(tabDecomp[0])
        monden = j3pNombre(tabDecomp[1])
        valFrac = monnum / monden
        if (monnum < 0) {
          monnum = -monnum
        }
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push('\\frac{' + monnum + '}{' + monden + '}|' + valFrac)
        }
      } else if (!isNaN(j3pNombre(String(texteIntervalle)))) {
        // on a un nombre
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(texteIntervalle)
        }
      } else {
        for (k = 0; k < ds.nbrepetitions; k++) {
          tabIntervalle.push(intervalleDefaut)
        }
      }
    }
    return tabIntervalle
  }
  function lgIntervalle (intervalle) {
    // cette fonction renvoie la longueur de l’intervalle'
    // si ce n’est pas un intervalle, alors elle renvoie 0'
    let lg = 0
    if (testIntervalleFermeEntiers.test(intervalle)) {
      // on a bien un intervalle
      const borneInf = j3pNombre(intervalle.substring(1, intervalle.indexOf(';')))
      const borneSup = j3pNombre(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.length - 1))
      lg = borneSup - borneInf
    }
    return lg
  }
  function fraction (nbre) {
    // Cette fonction retourne un objet
    // .ch :"a/b" avec a/b mise en fraction irréductible du nbre
    // .mathq : fraction éditée dans mathquill
    // .num : numérateur
    // .denom:dénominateur
    const stock = {}
    let q = Math.abs(nbre)
    const x = q
    let b = 1
    let d = 1
    let s = 0
    let n = Math.floor(q)
    const signeNbre = (nbre < 0) ? -1 : 1
    if (x <= Math.pow(10, -12)) {
      stock.ch = '0'
      // fraction.mathq="$\\vecteur{"+mes_points[0]+mes_points[1]+"}$"
      // stock.mathq="$0$";
      stock.mathq = '0'
      stock.num = 0
      stock.denom = 1
    } else {
      while (Math.abs(x * d - n) > Math.pow(10, -8)) {
        s = q - b * Math.floor(q / b)
        n = Math.floor(x / b)
        d = Math.floor(1 / b)
        q = b
        b = s
      }
      if (d === 1) {
        n = signeNbre * n
        stock.ch = n
        stock.num = n
        stock.denom = 1
        if (n < 0) {
          // stock.mathq="$-$"+Math.abs(n);
          stock.mathq = '-' + Math.abs(n)
        } else {
          stock.mathq = n
        }
      } else {
        if (nbre < 0) {
          // n=-Math.abs(n);
          // stock.mathq="$-\\frac{"+n+"}{"+d+"}$";
          stock.mathq = '-\\frac{' + n + '}{' + d + '}'
        } else {
          // stock.mathq="$\\frac{"+n+"}{"+d+"}$";
          stock.mathq = '\\frac{' + n + '}{' + d + '}'
        }
        stock.num = signeNbre * n
        stock.denom = d
        stock.ch = signeNbre * n + '/' + d
      }
    }
    return stock
  }
  function depart () {
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABQAA2ZyYf###wEA#wEAAAAAAAAAAANaAAACOAAAAAAAAAAAAAAAAQAAABj#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AARhbnVtAAItMf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABP#AAAAAAAAAAAAACAP####8ABGFkZW4AATEAAAABP#AAAAAAAAAAAAACAP####8ACGFscGhhbnVtAAE1AAAAAUAUAAAAAAAAAAAAAgD#####AAhhbHBoYWRlbgABMQAAAAE#8AAAAAAAAAAAAAIA#####wAHYmV0YW51bQACLTEAAAADAAAAAT#wAAAAAAAAAAAAAgD#####AAdiZXRhZGVuAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAhibnVtaW5pdAASYW51bSooLTIqYWxwaGFudW0p#####wAAAAEACkNPcGVyYXRpb24C#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAEAAAADAAAABAIAAAABQAAAAAAAAAAAAAAFAAAAAwAAAAIA#####wAIYmRlbmluaXQADWFkZW4qYWxwaGFkZW4AAAAEAgAAAAUAAAACAAAABQAAAAQAAAACAP####8ABXBnY2RiACFwZ2NkKGFicyhibnVtaW5pdCksYWJzKGJkZW5pbml0KSn#####AAAAAQANQ0ZvbmN0aW9uMlZhcgL#####AAAAAgAJQ0ZvbmN0aW9uAAAAAAUAAAAHAAAABwAAAAAFAAAACAAAAAIA#####wAEYm51bQAOYm51bWluaXQvcGdjZGIAAAAEAwAAAAUAAAAHAAAABQAAAAkAAAACAP####8ABGJkZW4ADmJkZW5pbml0L3BnY2RiAAAABAMAAAAFAAAACAAAAAUAAAAJAAAAAgD#####AAhjbnVtaW5pdAA2YW51bSphbHBoYW51bSphbHBoYW51bSpiZXRhZGVuK2JldGFudW0qKGFkZW4qYWxwaGFkZW4pAAAABAAAAAAEAgAAAAQCAAAABAIAAAAFAAAAAQAAAAUAAAADAAAABQAAAAMAAAAFAAAABgAAAAQCAAAABQAAAAUAAAAEAgAAAAUAAAACAAAABQAAAAQAAAACAP####8ACGNkZW5pbml0ABVhZGVuKmFscGhhZGVuKmJldGFkZW4AAAAEAgAAAAQCAAAABQAAAAIAAAAFAAAABAAAAAUAAAAGAAAAAgD#####AAVwZ2NkYwAhcGdjZChhYnMoY251bWluaXQpLGFicyhjZGVuaW5pdCkpAAAABgIAAAAHAAAAAAUAAAAMAAAABwAAAAAFAAAADQAAAAIA#####wAEY251bQAOY251bWluaXQvcGdjZGMAAAAEAwAAAAUAAAAMAAAABQAAAA4AAAACAP####8ABGNkZW4ADmNkZW5pbml0L3BnY2RjAAAABAMAAAAFAAAADQAAAAUAAAAO#####wAAAAEABUNGb25jAP####8AAWYAMWFudW0vYWRlbiooeC1hbHBoYW51bS9hbHBoYWRlbileMitiZXRhbnVtL2JldGFkZW4AAAAEAAAAAAQCAAAABAMAAAAFAAAAAQAAAAUAAAAC#####wAAAAEACkNQdWlzc2FuY2UAAAAEAf####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAAEAwAAAAUAAAADAAAABQAAAAQAAAABQAAAAAAAAAAAAAAEAwAAAAUAAAAFAAAABQAAAAYAAXgAAAAIAP####8AA3JlcAAKLSh4KzUpXjItMQAAAAQBAAAAAwAAAAkAAAAEAAAAAAoAAAAAAAAAAUAUAAAAAAAAAAAAAUAAAAAAAAAAAAAAAT#wAAAAAAAAAAF4#####wAAAAQAEENUZXN0RXF1aXZhbGVuY2UA#####wAIZWdhbGl0ZTEAAAARAAAAEgEAAAAAAT#wAAAAAAAAAQEBAAAACAD#####AAJmMgAxYW51bS9hZGVuKih4K2FscGhhbnVtL2FscGhhZGVuKV4yK2JldGFudW0vYmV0YWRlbgAAAAQAAAAABAIAAAAEAwAAAAUAAAABAAAABQAAAAIAAAAJAAAABAAAAAAKAAAAAAAAAAQDAAAABQAAAAMAAAAFAAAABAAAAAFAAAAAAAAAAAAAAAQDAAAABQAAAAUAAAAFAAAABgABeAAAAAsA#####wAIZWdhbGl0ZTIAAAAUAAAAEgEAAAAAAT#wAAAAAAAAAQEBAAAACAD#####AAJmMwAxYW51bS9hZGVuKih4LWJldGFudW0vYmV0YWRlbileMithbHBoYW51bS9hbHBoYWRlbgAAAAQAAAAABAIAAAAEAwAAAAUAAAABAAAABQAAAAIAAAAJAAAABAEAAAAKAAAAAAAAAAQDAAAABQAAAAUAAAAFAAAABgAAAAFAAAAAAAAAAAAAAAQDAAAABQAAAAMAAAAFAAAABAABeAAAAAsA#####wAIZWdhbGl0ZTMAAAAWAAAAEgEAAAAAAT#wAAAAAAAAAQEB################'
    stor.liste = stor.mtgAppLecteur.createList(txtFigure)
  }

  function enonceMain () {
    // Chargement de la figure mtg32
    depart()
    const alpha = stor.coef_alpha[me.questionCourante - 1]
    const beta = stor.coef_beta[me.questionCourante - 1]
    if (stor.coef_a[me.questionCourante - 1] === '[0;0]') {
      j3pShowError('le coefficient a ne peut être nul', { vanishAfter: 5 })
      stor.coef_a[me.questionCourante - 1] = '[-4;4]'
    }
    const leA = stor.coef_a[me.questionCourante - 1]
    let coeffATxt
    let coeffAlphaTxt
    let coeffBetaTxt
    let again
    let encore
    let coeffA, coeffAlpha, coeffBeta
    do {
      coeffA = 0
      if (leA.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffA = j3pNombre(leA.split('|')[1])
        coeffATxt = leA.split('|')[0]
      } else if (!isNaN(j3pNombre(leA))) {
        coeffA = j3pNombre(leA)
        coeffA = fraction(coeffA).mathq
        coeffATxt = coeffA
      } else {
        const [lemin, lemax] = j3pGetBornesIntervalle(leA)
        coeffA = j3pGetRandomInt(lemin, lemax)
        coeffATxt = coeffA
      }
      coeffAlpha = 0
      coeffBeta = 0
      if (alpha.includes('frac')) {
        // c’est un nb en écriture fractionnaire'
        coeffAlpha = j3pNombre(alpha.split('|')[1])
        coeffAlphaTxt = alpha.split('|')[0]
      } else if (!isNaN(j3pNombre(alpha))) {
        coeffAlpha = j3pNombre(alpha)
        // ici on pourrait avoir des valeurs décimales
        coeffAlpha = fraction(coeffAlpha).mathq
        coeffAlphaTxt = coeffAlpha
      } else {
        const [lemin, lemax] = j3pGetBornesIntervalle(alpha)
        coeffAlpha = j3pGetRandomInt(lemin, lemax)
        coeffAlphaTxt = coeffAlpha
      }
      do {
        if (beta.includes('frac')) {
          // c’est un nb en écriture fractionnaire'
          coeffBeta = j3pNombre(beta.split('|')[1])
          coeffBetaTxt = beta.split('|')[0]
          coeffBetaTxt = j3pGetLatexQuotient(j3pGetLatexProduit(10000, coeffBetaTxt), 10000)
        } else if (!isNaN(j3pNombre(beta))) {
          coeffBeta = j3pNombre(beta)
          coeffBeta = fraction(coeffBeta).mathq
          coeffBetaTxt = j3pGetLatexQuotient(j3pGetLatexProduit(10000, coeffBeta), 10000)
        } else {
          const [lemin, lemax] = j3pGetBornesIntervalle(beta)
          coeffBeta = j3pGetRandomInt(lemin, lemax)
          coeffBetaTxt = j3pGetLatexQuotient(j3pGetLatexProduit(10000, coeffBeta), 10000)
        }
        again = (coeffAlpha === coeffBeta) && !((alpha === beta) && (lgIntervalle(alpha) === 0))
      } while (again)
      encore = ((coeffA === 0) || ((coeffAlpha === 0) && ((alpha !== '[0;0]') && (alpha !== '0') && (alpha !== 0))) || ((coeffBeta === 0) && ((beta !== '[0;0]') && (beta !== '0') && (beta !== 0))))
    } while (encore)
    const coeffB = j3pGetLatexProduit(j3pGetLatexProduit(-2, coeffATxt), coeffAlphaTxt)
    const coeffC = j3pGetLatexSomme(j3pGetLatexProduit(coeffATxt, j3pGetLatexProduit(coeffAlphaTxt, coeffAlphaTxt)), coeffBetaTxt)
    me.logIfDebug('coeffATxt:' + coeffATxt + '   coeffAlphaTxt:' + coeffAlphaTxt + '   coeffBetaTxt:' + coeffBetaTxt)
    me.stockage[11] = coeffA
    me.stockage[12] = coeffAlphaTxt
    me.stockage[13] = coeffBetaTxt
    me.stockage[14] = coeffATxt
    // stockage[15] contient le polynome sous développée
    // me.stockage[15] = j3pMonome(1,2,coeffA)+j3pMonome(2,1,coeffB)+j3pMonome(3,0,coeffC);
    me.stockage[15] = j3pGetLatexMonome(1, 2, coeffATxt) + j3pGetLatexMonome(2, 1, coeffB) + j3pGetLatexMonome(2, 0, coeffC)
    me.stockage[16] = coeffB
    me.stockage[17] = coeffC
    // Je déterminer l’expression factorisée par a (lorsque a <> 1)
    const coefb2 = j3pGetLatexQuotient(coeffB, coeffA)
    const coefc2 = j3pGetLatexQuotient(coeffC, coeffA)
    stor.factorisationa = (Math.abs(coeffA + 1) < Math.pow(10, -12))// a vaut -1
      ? '-\\left(x^2' + j3pGetLatexMonome(2, 1, coefb2, 'x') + j3pGetLatexMonome(3, 0, coefc2, 'x') + '\\right)'
      : coeffA + '\\left(x^2' + j3pGetLatexMonome(2, 1, coefb2, 'x') + j3pGetLatexMonome(3, 0, coefc2, 'x') + '\\right)'
    stor.debutdevIR = 'x^2' + j3pGetLatexMonome(2, 1, coefb2, 'x')
    const signe = (j3pCalculValeur(coefb2) > 0) ? '+' : '-'
    const coefbPos = Math.abs(coefb2)
    const coefb3 = j3pGetLatexQuotient(coefbPos, '2')
    const coefb3ter = j3pGetLatexQuotient(coefb2, '2')
    // console.log('coefb3:', coefb3, 'coefbPos:', coefbPos, 'coefb2:', coefb2)
    const coefc3 = j3pGetLatexQuotient(j3pGetLatexProduit(coefb2, coefb2), '4')
    const coefc3Bis = j3pGetLatexMonome(2, 0, Math.abs(coefb3ter), 'x') + '^2' // (j3pCalculValeur(coefb3ter) > 0) ? j3pGetLatexMonome(2, 0, coefb3, 'x') + '^2' : '+\\left(' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit(coefb3, -1), 'x') + '\\right)^2'
    const coefb4 = (j3pCalculValeur(coefb3) > 0)
      ? j3pGetLatexMonome(1, 1, coefb3, 'x')
      : '\\left(' + j3pGetLatexMonome(1, 1, coefb3, 'x') + '\\right)'
    stor.debutdevIR2 = 'x^2' + signe + '2\\times ' + coefb4 + coefc3Bis + j3pGetLatexMonome(4, 0, j3pGetLatexProduit(coefc3, '-1'), 'x')
    stor.debutdevIR3 = '\\left(x' + j3pGetLatexMonome(2, 0, coefb3ter, 'x') + '\\right)^2' + j3pGetLatexMonome(3, 0, j3pGetLatexProduit(coefc3, '-1'), 'x')
    stor.formeCanoniqueInit = (Math.abs(coeffA - 1) < Math.pow(10, -12))// a vaut 1
      ? stor.debutdevIR3 + j3pGetLatexMonome(3, 0, coefc2, 'x')
      : (Math.abs(coeffA + 1) < Math.pow(10, -12))// a vaut -1
          ? '-\\left(' + stor.debutdevIR3 + j3pGetLatexMonome(3, 0, coefc2, 'x') + '\\right)'
          : coeffA + '\\left(' + stor.debutdevIR3 + j3pGetLatexMonome(3, 0, coefc2, 'x') + '\\right)'

    const tabNomFct = ['f', 'g', 'h']
    const nomFct = tabNomFct[j3pGetRandomInt(0, 2)]
    me.stockage[18] = nomFct
    // début de la consigne
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.zoneCons4, { color: me.styles.cfaux }) // div accueillant les mauvaises réponses
    stor.zoneVerif = j3pAddElt(me.conteneur, 'div', '', { style: { color: 'blue' } })
    j3pAffiche(stor.zoneCons1, '', textes.consigne1,
      {
        f: nomFct,
        g: nomFct + '(x)=' + me.stockage[15]
      })
    if (coeffAlpha > 0) {
      me.stockage[3] = '$-$'
      me.stockage[5] = 2
    } else {
      me.stockage[3] = '$+$'
      me.stockage[5] = 1
    }
    if (coeffBeta > 0) {
      me.stockage[4] = '$+$'
      me.stockage[6] = 1
    } else {
      me.stockage[4] = '$-$'
      me.stockage[6] = 2
    }
    // bonne réponse
    if (Math.abs(coeffAlpha) < Math.pow(10, -12)) {
      me.stockage[20] = me.stockage[15]// dans ce cas la forme développée est la forme canonique
    } else {
      if (Math.abs(coeffA - 1) < Math.pow(10, -12)) {
        me.stockage[20] = '(x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit(-1, coeffAlphaTxt)) + ')^2' + j3pGetLatexMonome(2, 0, coeffBetaTxt)
      } else if (Math.abs(coeffA + 1) < Math.pow(10, -12)) {
        me.stockage[20] = '-(x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit(-1, coeffAlphaTxt)) + ')^2' + j3pGetLatexMonome(2, 0, coeffBetaTxt)
      } else {
        me.stockage[20] = coeffATxt + '(x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit(-1, coeffAlphaTxt)) + ')^2' + j3pGetLatexMonome(2, 0, coeffBetaTxt)
      }
    }
    const tabFracA = j3pExtraireNumDen(coeffATxt)
    const tabFracAlpha = j3pExtraireNumDen(coeffAlphaTxt)
    const tabFracBeta = j3pExtraireNumDen(coeffBetaTxt)
    const aNum = (tabFracA[0]) ? tabFracA[1] : coeffATxt
    const aDen = (tabFracA[0]) ? tabFracA[2] : 1
    const alphaNum = (tabFracAlpha[0]) ? tabFracAlpha[1] : coeffAlphaTxt
    const alphaDen = (tabFracAlpha[0]) ? tabFracAlpha[2] : 1
    const betaNum = (tabFracBeta[0]) ? tabFracBeta[1] : coeffBetaTxt
    const betaDen = (tabFracBeta[0]) ? tabFracBeta[2] : 1
    stor.liste.giveFormula2('anum', String(aNum))
    stor.liste.giveFormula2('aden', String(aDen))
    stor.liste.giveFormula2('alphanum', String(alphaNum))
    stor.liste.giveFormula2('alphaden', String(alphaDen))
    stor.liste.giveFormula2('betanum', String(betaNum))
    stor.liste.giveFormula2('betaden', String(betaDen))
    j3pAffiche(stor.zoneCons2, '', textes.consigne2, { f: nomFct })
    j3pAffiche(stor.zoneCons3, '', textes.comment)
    j3pStyle(stor.zoneCons3, { fontStyle: 'italic', fontSize: '0.9em' })
    const elt = j3pAffiche(stor.zoneCons5, '', '$£f(x)=$&1&',
      {
        f: nomFct,
        inputmq1: { texte: '' }
      })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '[\\d.,()^2²+x-/]', { commandes: ['puissance', 'fraction'] })
    j3pFocus(stor.zoneInput)

    stor.nbVerif = ds.nbVerif
    const txtverif = (stor.nbVerif > 1) ? textes.verif2 : textes.verif
    j3pAffiche(stor.zoneVerif, '', txtverif, { v: stor.nbVerif })
    me.logIfDebug('coeffA:' + coeffA + '   coeffAlpha:' + coeffAlpha + '   coeffBeta:' + coeffBeta + '   coeffB:' + coeffB + '   coeffC:' + coeffC)

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.petit.explications })
    stor.zoneCorr.style.padding = '10px'
    stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['puissance', 'fraction'] })

    stor.toucheEntreePress = false

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // Obligatoire
    me.finEnonce()
  } // enonceMain

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.coef_a = constructionTabIntervalle(ds.a, '[-4;4]')
        stor.coef_alpha = constructionTabIntervalle(ds.alpha, '[-5;5]')
        stor.coef_beta = constructionTabIntervalle(ds.beta, '[-5;5]')

        document.addEventListener('keyup', function (e) {
          if (e.code === 'Enter' && stor.nbVerif > 0) {
            stor.toucheEntreePress = true
          }
        })
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      let repEleve = j3pValeurde(stor.zoneInput)
      me.logIfDebug('repEleve:' + repEleve)
      const aRepondu = (repEleve !== '')

      if (!aRepondu && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        j3pFocus(stor.zoneInput)
        return me.finCorrection()
      } else {
        // une réponse a été saisie
        // on vérifie si le polynôme écrit est correct
        const decimalRegExp = /[0-9]+,[0-9]+/g // new RegExp('[0-9]{1,}\\,[0-9]{1,}', 'gi')
        if (decimalRegExp.test(repEleve)) {
          const tabDecimaux = repEleve.match(decimalRegExp)
          for (let i = 0; i < tabDecimaux.length; i++) {
            repEleve = repEleve.replace(tabDecimaux[i], fraction(j3pNombre(tabDecimaux[i])).mathq)
          }
        }

        let repEleveXcas = j3pMathquillXcas(repEleve)
        const puissanceDouble = /\^\(\^(\(?[^}]+\)?)\)/g // ceci gère les cas où l’élève aura écrit x^{^2} au lieu de x^2
        while (puissanceDouble.test(repEleveXcas)) {
          repEleveXcas = repEleveXcas.replace(puissanceDouble, '^$1')
        }
        // console.log('repEleveXcas:', repEleveXcas)
        repEleveXcas = j3pAjouteFoisDevantX(repEleveXcas, 'x')
        stor.liste.giveFormula2('rep', repEleveXcas)
        stor.liste.calculateNG()

        me.logIfDebug('repEleve:' + repEleve + '  repEleveXcas1:' + repEleveXcas + '   me.stockage[20]:' + me.stockage[20])
        // le polynôme entré par l’élève est repEleve
        // le polynôme attendu en tant que réponse est me.stockage[20]'
        const bonneReponse = (stor.liste.valueOf('egalite1') === 1)
        me.logIfDebug('bonneReponse : ' + bonneReponse)
        if (bonneReponse) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          me.typederreurs[0]++
          // on désactive la zone de saisie
          j3pDesactive(stor.zoneInput)
          stor.zoneInput.setAttribute('style', 'color:' + me.styles.cbien)
          j3pFreezeElt(stor.zoneInput)
          // on affiche l’explication'
          afficheCorrection(true)
        } else {
          // Pas de bonne réponse
          let numErreur = 0
          if (testexpressions(repEleveXcas, j3pMathquillXcas(me.stockage[20]), ['x'])) {
            // cas d’une bonne forme développée mais d’une fausse forme canonique
            numErreur = 1
          } else {
            // pb de signe
            if (stor.liste.valueOf('egalite2') === 1) {
              numErreur = 2
            }
            // confusion entre alpha et beta
            if (stor.liste.valueOf('egalite3') === 1) {
              numErreur = 3
            }
            me.logIfDebug('numErreur:' + numErreur)
          }
          stor.zoneCorr.style.color = me.styles.cfaux
          // on met en rouge ce qui est faux
          stor.zoneInput.style.color = me.styles.cfaux
          if (!me.isElapsed) {
            stor.zoneCorr.innerHTML = cFaux
            if (numErreur === 1) {
              if (stor.toucheEntreePress) {
                // c’est bien égal au polynôme initial mais ce n’est aps la forme canonique
                stor.zoneCorr.innerHTML = textes.phrase_entrer
                stor.zoneCorr.style.color = me.styles.cbien
              } else {
                // problème car ce n’est pas la forme canonique qui est donnée (pourtant c’est le bon polynôme)'
                stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur4
              }
            } else if (numErreur === 2) {
              // pb avec un ou plusieurs signes dans les coefs
              stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur1
            } else if (numErreur === 3) {
              // confusion entre l’abscisse et l’ordonnée du sommet'
              stor.zoneCorr.innerHTML += '<br>' + textes.phrase_erreur3
            }
          }
          const repEnter = j3pAddElt(stor.zoneCons4, 'div')
          const lesigneEgalite = (numErreur === 1) ? '=' : '\\neq'
          j3pAffiche(repEnter, '', '$£f(x) ' + lesigneEgalite + repEleve + '$',
            { f: me.stockage[18] })
          if (numErreur === 1) repEnter.setAttribute('style', 'color:' + me.styles.cbien)
          stor.nbVerif -= 1
          if (stor.toucheEntreePress) {
            stor.toucheEntreePress = false
            const txtverifcorr = (stor.nbVerif > 1) ? textes.verif2 : textes.verif
            j3pEmpty(stor.zoneVerif)
            j3pAffiche(stor.zoneVerif, '', txtverifcorr, { v: stor.nbVerif })
            return me.finCorrection()
          } else {
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              j3pDesactive(stor.zoneInput)
              afficheCorrection(false)
            } else { // réponse fausse :
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                return me.finCorrection()
              } else {
                // Erreur au second essai
                me.typederreurs[2]++
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // on désactive la zone de saisie
                j3pDesactive(stor.zoneInput)
                stor.zoneInput.setAttribute('style', 'color:' + me.styles.cfaux)
                if (numErreur === 1) {
                  me.typederreurs[3]++
                } else if (numErreur === 2) {
                  me.typederreurs[4]++
                } else if (numErreur === 3) {
                  me.typederreurs[5]++
                } else {
                  me.typederreurs[6]++
                }
                // on barre les mauvaises réponses
                j3pBarre(stor.zoneInput)
                afficheCorrection(false)
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          // c’est bien :
          me.parcours.pe = ds.pe_1
        } else {
          const nbErreurs = ds.nbitems - me.score
          if (me.typederreurs[3] / nbErreurs >= ds.seuilerreur) {
            // problèmes de signes
            me.parcours.pe = ds.pe_2
          } else if ((me.typederreurs[4] + me.typederreurs[6]) / nbErreurs >= ds.seuilerreur) {
            // confusion entre abscisse et ordonnée de l’extremum' ou pb avec les coord de l’extremum'
            me.parcours.pe = ds.pe_3
          } else if (me.typederreurs[5] / nbErreurs >= ds.seuilerreur) {
            // problème avec le coefficient a
            me.parcours.pe = ds.pe_4
          } else {
            me.parcours.pe = ds.pe_5
          }
        }
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
