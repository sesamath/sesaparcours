import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pElement, j3pGetNewId, j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
import loadWebXcas from 'src/legacy/outils/xcas/loadWebXcas'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import { afficheVec, coords, setWebXcas } from 'src/legacy/sections/lycee/ProduitScalaire/helper'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, regardeCorrection, tempsDepasse } = textesGeneriques

// la fct d’évaluation des expressions par webXcas
let xcas

/*
        Auteur : Christian Buso
        Date : 15/06/15
        Remarques : après 3 sections, voici uen version qui se veut moins brouillonne!
 */

/*
    Cette variable renseigne :
        - les outils utilisés par la section
        - les paramètres de la section (nom,valeur par défaut,type,description

    dim : 2 ou 3 (plan ou espace) : peut être surchargée
    n_nbDecimalesCoordonnees : typiquement 0 (coordonnées entières) ou 1 (décimle). On peut mettre plus mais à quoi bon!.  peut être surchargée

    minCoord : valeur minimale d’une coordonnée :(entière). peut être surchargée
    maxCoord : valeur maximale d’une coordonnée :(entière). peut être surchargée

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['dim', 2, 'entier', 'Dimension de l’espace : 2 ou 3'],
    ['sensCoordVect', 'V', 'string', 'Sens de l’affichage des coordonnées (dim 2 seulement) : V ou H'],
    ['typeCoord', 'fraction', 'liste', 'Type des coordonnées : fraction (réduite) ou entier ou decimal ou racineSimple (A*racB) ou racineAffine (A+D*racB)', ['entier', 'decimal', 'racineSimple', 'racineAffine', 'fraction']],
    ['n_nbDecimalesCoordonnees', 1, 'entier', 'Nombre de décimales pour les coordonnées des vecteurs : 0 ou 1'],
    ['minCoord', -5, 'entier', 'minimum pour les coordonnées : choisir un entier, utilisé pour les coordonnées entières'],
    ['maxCoord', 6, 'entier', 'minimum pour les coordonnées : choisir un entier, utilisé pour les coordonnées entières'],
    ['sansA', 1, 'entier', 'Mettre la valeur 1 pour que  le coef a (dans a/b et/ou a*Rac(b)) vaille 1 (sans A donc!),pour toute autre valeur a ne sera ni égal à 1 ni nul'],
    ['sansD', 1, 'entier', 'Mettre la valeur 1 pour que  le coef d (a+d*Rac(b)) vaille 1 (sans D donc!),pour toute autre valeur d ne sera ni égal à 1 ni nul'],
    ['intCoefA', '[-5;5]', 'string', 'intervalle dans lequel sera choisi l’entier a de a/b ou a*Rac(b) ou a+d*Rac(b)'],
    ['intCoefB', '[2;5]', 'string', 'intervalle dans lequel sera choisi l’entier b de a/b ou a*Rac(b) ou a+d*Rac(b) : choisir une borne inférieure au moins à 2 (pas de négatif ni 0 ni 1)'],
    ['intCoefD', '[-5;5]', 'string', 'intervalle dans lequel sera choisi l’entier d de  a+d*Rac(b)'],
    ['effectif', 1, 'entier', 'effectif des coordonnées spéciales : racine ou fraction. Cela peut-être 1 ou 2 (ou 3 en dimension 3)']

  ]
}

const textes = {
  phraseEnonce: 'Dans un repère orthonormé, on donne deux vecteurs : <br>$£u \\qquad$ et $\\qquad £v$. <br>Ces vecteurs sont-ils orthogonaux?',
  phraseTitre: 'Produit scalaire<br>dans un repère orthonormé',
  phraseSolutionSiOrtho: 'On a :<br> $\\vec{u} \\qquad &middot; \\qquad \\vec{v} =£p$ <br>$\\vec{u} \\qquad &middot; \\qquad  \\vec{v} =£s$<br>$\\vec{u} \\qquad &middot; \\qquad  \\vec{v} =£r$.<br> Le produit scalaire est nul,<br> donc les vecteurs sont orthogonaux.',
  phraseSolutionSiPasOrtho: 'On a :<br> $\\vec{u} \\qquad &middot; \\qquad \\vec{v} =£p$ <br>$\\vec{u} \\qquad &middot; \\qquad  \\vec{v} =£s$<br>$\\vec{u} \\qquad &middot; \\qquad  \\vec{v} =£r$.<br> Le produit scalaire n’est pas nul,<br> donc les vecteurs ne sont pas orthogonaux.',
  oui: 'OUI',
  non: 'NON'
}

/**
 * section ProduitScalaireOrtho
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  //  fontion permettant d’afficher la correction expliquée version Rémi
  function afficheCorrection (bonneRep) {
    let zoneExplications
    if (bonneRep) {
      zoneExplications = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.enonce })
      zoneExplications.style.color = me.styles.cbien
    } else {
      zoneExplications = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    }
    const phrase = (stor.r === 0) ? textes.phraseSolutionSiOrtho : textes.phraseSolutionSiPasOrtho
    const { r, s, p } = stor
    j3pAffiche(zoneExplications, '', phrase, { r, s, p })
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    // les coordonnées
    let infosVecteur
    switch (ds.typeCoord) {
      case 'entier':
        infosVecteur = ['entier', '[' + ds.minCoord + ';' + ds.maxCoord + ']']
        break
      case 'decimal':
        infosVecteur = ['decimal', '[' + ds.minCoord + ';' + ds.maxCoord + ']', ds.n_nbDecimalesCoordonnees]
        break
      case 'racineSimple' :
        infosVecteur = ['racineSimple', ds.effectif, ds.intCoefA, ds.intCoefB, ds.sansA]
        break
      case 'racineAffine' :
        infosVecteur = ['racineAffine', ds.effectif, ds.intCoefA, ds.intCoefD, ds.intCoefB, ds.sansD]
        break
      case 'fraction' :
        infosVecteur = ['fraction', ds.effectif, ds.intCoefA, ds.intCoefB, ds.sansA]
        break
    }
    const lesVecteursCalc = []
    const lesVecteursAff = []
    let unVecteur = []

    for (let k = 0; k < 2; k++) {
      unVecteur = coords(ds.dim, infosVecteur)
      lesVecteursCalc[k] = unVecteur[0]
      lesVecteursAff[k] = unVecteur[1]
    }

    // on chenge une des coordoonées afin d’avoir de temps en temps des vecteurs orthogonaux (6 chances sur 11)

    const ortho = j3pGetRandomInt(0, 10)
    let nouvCoord
    if (ortho < 7 & lesVecteursCalc[1][0] !== 0) {
      if (ds.dim === 2) {
        lesVecteursCalc[0][0] = xcas('simplify((' + lesVecteursCalc[0][1] + ') *(' + lesVecteursCalc[1][1] + ')' + '/((-1)*(' + lesVecteursCalc[1][0] + ')))')
        nouvCoord = xcas('latex(simplify((' + lesVecteursCalc[0][1] + ') *(' + lesVecteursCalc[1][1] + ')' + '/((-1)*(' + lesVecteursCalc[1][0] + '))))')
        lesVecteursAff[0][0] = nouvCoord.substring(1, nouvCoord.length - 1)
      } else {
        lesVecteursCalc[0][0] = xcas('simplify(((' + lesVecteursCalc[0][1] + ') *(' + lesVecteursCalc[1][1] + ')' + '(' + lesVecteursCalc[0][2] + ') *(' + lesVecteursCalc[1][2] + ')' + ')/((-1)*(' + lesVecteursCalc[1][0] + ')))')
        nouvCoord = xcas('latex(simplify(((' + lesVecteursCalc[0][1] + ') *(' + lesVecteursCalc[1][1] + ')' + '(' + lesVecteursCalc[0][2] + ') *(' + lesVecteursCalc[1][2] + ')' + ')/((-1)*(' + lesVecteursCalc[1][0] + '))))')
        lesVecteursAff[0][0] = nouvCoord.substring(1, nouvCoord.length - 1)
      }
    }

    const direction = (ds.dim === 2) ? ds.sensCoordVect : 'H'

    j3pAffiche(stor.zoneCons1, '', textes.phraseEnonce, {
      u: afficheVec('u', lesVecteursAff[0], direction),
      v: afficheVec('v', lesVecteursAff[1], direction)
    })
    stor.idsRadio = [j3pGetNewId('radio1'), j3pGetNewId('radio2')]
    stor.nameRadio = j3pGetNewId('choix')
    j3pBoutonRadio(stor.zoneCons2, stor.idsRadio[0], stor.nameRadio, 0, textes.oui)
    j3pAffiche(stor.zoneCons2, '', '&nbsp; &nbsp; &nbsp;')
    j3pBoutonRadio(stor.zoneCons2, stor.idsRadio[1], stor.nameRadio, 1, textes.non)

    // construction de la solution
    let laSolution = ''
    let eltSol1 = ''
    let eltSol2 = ''
    let ch = ''
    let coordu = ''
    let coordv = ''
    for (let k = 0; k < ds.dim; k++) {
      coordu = lesVecteursAff[0][k] + ''
      coordv = lesVecteursAff[1][k] + ''
      laSolution += '+normal((' + lesVecteursCalc[0][k] + ')*(' + lesVecteursCalc[1][k] + '))'
      if ((coordu.charCodeAt(0) === 45 && k > 0) || (ds.typeCoord === 'racineAffine' && coordu.includes('sqrt'))) {
        coordu = '(' + coordu + ')'
      }
      if (coordv.charCodeAt(0) === 45 || (ds.typeCoord === 'racineAffine' & coordv.includes('sqrt'))) {
        coordv = '(' + coordv + ')'
      }
      eltSol1 = eltSol1 + '+' + coordu + ' \\times ' + coordv
      ch = xcas('latex(simplify((' + lesVecteursCalc[0][k] + ') *(' + lesVecteursCalc[1][k] + ')))')
      ch = ch.substring(1, ch.length - 1)
      if (ch.charCodeAt(0) === 45 && k > 0) {
        eltSol2 = eltSol2 + '+(' + ch + ')'
      } else {
        eltSol2 = eltSol2 + '+' + ch
      }
    }
    laSolution = laSolution.substring(1)
    const result = xcas('latex(simplify(' + laSolution + '))')
    stor.r = j3pCalculValeur(result.substring(1, result.length - 1))
    if (stor.r === 0) {
      stor.solution = 'o'
      stor.n = ' est nul '
      stor.w = ' sont orthogonaux'
    } else {
      stor.solution = 'n'
      stor.n = ' n’est pas nul '
      stor.w = ' ne sont pas orthogonaux'
    }
    stor.p = eltSol1.substring(1)
    stor.s = eltSol2.substring(1)

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1' })
        me.afficheTitre(textes.phraseTitre)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // chargement webXcas
        loadWebXcas()
          .then(
            // success
            (_xcas) => {
              xcas = _xcas
              setWebXcas(_xcas)
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'xCas n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":
    }

    case 'correction': {
      const reponse = {}
      reponse.aRepondu = (j3pBoutonRadioChecked(stor.nameRadio)[0] > -1)
      if (reponse.aRepondu) {
        if (j3pCalculValeur(stor.r) === 0) {
          reponse.bonneReponse = (j3pBoutonRadioChecked(stor.nameRadio)[0] === 0)
        } else {
          reponse.bonneReponse = (j3pBoutonRadioChecked(stor.nameRadio)[0] === 1)
        }
      }

      // On teste si une réponse a été saisie
      if (!reponse.aRepondu && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        return me.finCorrection() // on reste en correction
      }

      j3pDesactive(stor.idsRadio[0])
      j3pDesactive(stor.idsRadio[1])
      if (reponse.bonneReponse) {
        // Bonne réponse
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        j3pElement('label' + stor.idsRadio[j3pBoutonRadioChecked(stor.nameRadio)[0]]).style.color = me.styles.cbien
        // afficheCorrection(true);
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux
      j3pBarre('label' + stor.idsRadio[j3pBoutonRadioChecked(stor.nameRadio)[0]])
      j3pElement('label' + stor.idsRadio[j3pBoutonRadioChecked(stor.nameRadio)[0]]).style.color = me.styles.cfaux

      if (me.isElapsed) {
        // A cause de la limite de temps :
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        afficheCorrection(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      // Erreur au nème essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      afficheCorrection(false)
      me.typederreurs[2]++
      return me.finCorrection('navigation', true)
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)
      break // case "navigation":
  }
}
