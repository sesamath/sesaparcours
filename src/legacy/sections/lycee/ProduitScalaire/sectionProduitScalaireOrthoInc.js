import { j3pAddElt, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import loadWebXcas from 'src/legacy/outils/xcas/loadWebXcas'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { afficheVec, coords, setWebXcas } from 'src/legacy/sections/lycee/ProduitScalaire/helper'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// la fct d’évaluation des expressions par webXcas
let xcas

/**
 * Trouver une coordonnées manquante d’un vecteur pour qu’il soit orthogonal à un autre
 * @fileOverview
 * @author Christian Buso
 * @since 2015-06-15
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['dim', 2, 'entier', 'Dimension de l’espace : 2 ou 3'],
    ['sensCoordVect', 'V', 'string', 'Sens de l’affichage des coordonnées (dim 2 seulement) : V ou H'],
    ['typeCoord', 'fraction', 'liste', 'Type des coordonnées : fraction (réduite) ou entier ou decimal ou racineSimple (A*racB) ou racineAffine (A+D*racB)', ['entier', 'decimal', 'racineSimple', 'racineAffine', 'fraction']],
    ['n_nbDecimalesCoordonnees', 1, 'entier', 'Nombre de décimales pour les coordonnées des vecteurs : 0 ou 1'],
    ['minCoord', -5, 'entier', 'minimum pour les coordonnées : choisir un entier, utilisé pour les coordonnées entières'],
    ['maxCoord', 6, 'entier', 'minimum pour les coordonnées : choisir un entier, utilisé pour les coordonnées entières'],
    ['sansA', 1, 'entier', 'Mettre la valeur 1 pour que  le coef a (dans a/b et/ou a*Rac(b)) vaille 1 (sans A donc!),pour toute autre valeur a ne sera ni égal à 1 ni nul'],
    ['sansD', 1, 'entier', 'Mettre la valeur 1 pour que  le coef d (a+d*Rac(b)) vaille 1 (sans D donc!),pour toute autre valeur d ne sera ni égal à 1 ni nul'],
    ['intCoefA', '[-5;5]', 'string', 'intervalle dans lequel sera choisi l’entier a de a/b ou a*Rac(b) ou a+d*Rac(b)'],
    ['intCoefB', '[2;5]', 'string', 'intervalle dans lequel sera choisi l’entier b de a/b ou a*Rac(b) ou a+d*Rac(b) : choisir une borne inférieure au moins à 2 (pas de négatif ni 0 ni 1)'],
    ['intCoefD', '[-5;5]', 'string', 'intervalle dans lequel sera choisi l’entier d de  a+d*Rac(b)'],
    ['effectif', 1, 'entier', 'effectif des coordonnées spéciales : racine ou fraction. Cela peut-être 1 ou 2 (ou 3 en dimension 3)']
  ]
}
const textes = {
  phraseTitre: 'Produit scalaire<br>dans un repère orthonormé',
  phraseEnonce: 'Dans un repère orthonormé, on donne deux vecteurs ORTHOGONAUX : <br>$£u \\qquad$ et $\\qquad £v$ .<br> Calcule la coordonnée inconnue $£i$ :',
  phraseSolution: 'Voici la solution : <br>$\\vec{u} \\cdot \\vec{v} =£p$ <br>$\\iff £r=£s$<br> $\\iff £i=£x$',
  phraseSaisie: ' $£i =\\qquad$ &1&.',
  phraseNbChances: 'Il te reste £e  essai(s)'
}
/**
 * section ProduitScalaireOrthoInc
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  //  fontion permettant d’afficher la correction expliquée version Rémi
  function afficheCorrection (bonneRep) {
    j3pDetruit(stor.zoneCons4)
    j3pDetruit(stor.zoneCons3)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneRep) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    j3pAffiche(zoneExpli, '', textes.phraseSolution,
      {
        r: stor.r,
        s: stor.s,
        p: stor.p,
        i: stor.i,
        x: stor.x
      })
  }

  /* fonction qui calcule le rpoduit scalaire de deux vecteurs (dim 2 ou 3)
 et qui retourn un tableau :
    élément 0 : le résultat
    élément 1 : l’affichage du résultat
 en entrée les coordonnées de deux vecteurs sous frome de tableau
*/
  function prodScal (coordVec1, coordVec2) {
    let rep = ''
    for (let i = 0; i < coordVec1.length; i++) {
      rep = rep + '+(' + coordVec1[i] + ')*(' + coordVec2[i] + ')'
    }
    rep = rep.substring(1)
    let repAff = xcas('latex(simplify(' + rep + '))')
    repAff = repAff.substring(1, repAff.length - 1)
    rep = xcas('simplify(' + rep + ')')
    return ([rep, repAff])
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '15px' }) })
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    // les coordonnées
    let infosVecteur
    switch (ds.typeCoord) {
      case 'entier':
        infosVecteur = ['entier', '[' + ds.minCoord + ';' + ds.maxCoord + ']']
        break
      case 'decimal':
        infosVecteur = ['decimal', '[' + ds.minCoord + ';' + ds.maxCoord + ']', ds.n_nbDecimalesCoordonnees]
        break
      case 'racineSimple' :
        infosVecteur = ['racineSimple', ds.effectif, ds.intCoefA, ds.intCoefB, ds.sansA]
        break
      case 'racineAffine' :
        infosVecteur = ['racineAffine', ds.effectif, ds.intCoefA, ds.intCoefD, ds.intCoefB, ds.sansD]
        break
      case 'fraction' :
        infosVecteur = ['fraction', ds.effectif, ds.intCoefA, ds.intCoefB, ds.sansA]
        break
    }
    const lesVecteursCalc = []
    const lesVecteursAff = []
    let unVecteur = []

    for (let k = 0; k < 2; k++) {
      do {
        unVecteur = coords(ds.dim, infosVecteur)
        // Rémi 29/01/22
        // Il y a un problème dans la gestion d’une coordonnées nulle.
        // JE ne sais pas pourquoi, donc je bourrine un peu et j’empêche ce cas de figure
      } while (unVecteur[0].includes(0) || unVecteur[0].includes(1))
      lesVecteursCalc[k] = unVecteur[0]
      lesVecteursAff[k] = unVecteur[1]
    }
    if (lesVecteursCalc[1][0] !== 0) {
      if (ds.dim === 2) {
        lesVecteursCalc[0][0] = xcas('simplify((' + lesVecteursCalc[0][1] + ') *(' + lesVecteursCalc[1][1] + ')' + '/((-1)*(' + lesVecteursCalc[1][0] + ')))')
        const nouvCoord = xcas('latex(simplify((' + lesVecteursCalc[0][1] + ') *(' + lesVecteursCalc[1][1] + ')' + '/((-1)*(' + lesVecteursCalc[1][0] + '))))')
        lesVecteursAff[0][0] = nouvCoord.substring(1, nouvCoord.length - 1)
      } else {
        lesVecteursCalc[0][0] = xcas('simplify(((' + lesVecteursCalc[0][1] + ') *(' + lesVecteursCalc[1][1] + ')' + '(' + lesVecteursCalc[0][2] + ') *(' + lesVecteursCalc[1][2] + ')' + ')/((-1)*(' + lesVecteursCalc[1][0] + ')))')
        const nouvCoord = xcas('latex(simplify(((' + lesVecteursCalc[0][1] + ') *(' + lesVecteursCalc[1][1] + ')' + '(' + lesVecteursCalc[0][2] + ') *(' + lesVecteursCalc[1][2] + ')' + ')/((-1)*(' + lesVecteursCalc[1][0] + '))))')
        lesVecteursAff[0][0] = nouvCoord.substring(1, nouvCoord.length - 1)
      }
    } else {
      lesVecteursCalc[0][1] = 0
    }
    const direction = (ds.dim === 2) ? ds.sensCoordVect : 'H'
    const soluce = prodScal(lesVecteursCalc[0], lesVecteursCalc[1])
    stor.r = soluce[1]

    // On cache une des coordonnées de deux vecteurs
    const h = j3pGetRandomInt(0, (ds.dim - 1))
    const k = j3pGetRandomInt(0, 1)
    let inc
    switch (h) {
      case 0:
        stor.x = lesVecteursAff[k][h]
        stor.solution = lesVecteursCalc[k][h]
        lesVecteursAff[k][h] = 'x'
        lesVecteursCalc[k][h] = 'x'
        inc = 'x'
        break
      case 1:
        stor.x = lesVecteursAff[k][h]
        stor.solution = lesVecteursCalc[k][h]
        lesVecteursAff[k][h] = 'y'
        lesVecteursCalc[k][h] = 'y'
        inc = 'y'
        break
      case 2:
        stor.x = lesVecteursAff[k][h]
        stor.solution = lesVecteursCalc[k][h]
        lesVecteursAff[k][h] = 'z'
        lesVecteursCalc[k][h] = 'z'
        inc = 'z'
        break
    }
    stor.i = inc
    j3pAffiche(stor.zoneCons1, '', textes.phraseEnonce, {
      u: afficheVec('u', lesVecteursAff[0], direction),
      v: afficheVec('v', lesVecteursAff[1], direction),
      i: inc,
      r: soluce[1]
    })

    // construction de la solution
    // let laSolution = ''
    let eltSol1 = ''
    let eltSol2 = ''
    let ch = ''
    let coordu = ''
    let coordv = ''
    for (let k = 0; k < ds.dim; k++) {
      coordu = lesVecteursAff[0][k] + ''
      coordv = lesVecteursAff[1][k] + ''
      // laSolution = laSolution + '+normal((' + lesVecteursCalc[0][k] + ')*(' + lesVecteursCalc[1][k] + '))'
      if ((coordu.charCodeAt(0) === 45 & k > 0) || (ds.typeCoord === 'racineAffine' && coordu.includes('sqrt'))) {
        coordu = '(' + coordu + ')'
      }
      if (coordv.charCodeAt(0) === 45 || (ds.typeCoord === 'racineAffine' && coordv.includes('sqrt'))) {
        coordv = '(' + coordv + ')'
      }
      eltSol1 = eltSol1 + '+' + coordu + ' \\times ' + coordv
      ch = xcas('latex(simplify((' + lesVecteursCalc[0][k] + ') *(' + lesVecteursCalc[1][k] + ')))')
      ch = ch.substring(1, ch.length - 1)
      if (ch.charCodeAt(0) === 45 && k > 0) {
        eltSol2 = eltSol2 + '+(' + ch + ')'
      } else {
        eltSol2 = eltSol2 + '+' + ch
      }
    }
    // laSolution = laSolution.substring(1)
    stor.p = eltSol1.substring(1)
    stor.s = eltSol2.substring(1)

    // Zone de saisie
    stor.elt = j3pAffiche(stor.zoneCons2, '', textes.phraseSaisie, {
      i: inc,
      inputmq1: { texte: '' }
    })
    j3pStyle(stor.zoneCons3, { padding: '10px 0' })
    j3pAffiche(stor.zoneCons3, '', textes.phraseNbChances, { e: ds.nbchances })

    // on affiche la palette lorsque c’est nécessaire
    if (ds.typeCoord === 'racineSimple' | ds.typeCoord === 'racineAffine' || ds.typeCoord === 'fraction') {
      j3pStyle(stor.zoneCons4, { paddingTop: '10px' })
      j3pPaletteMathquill(stor.zoneCons4, stor.elt.inputmqList[0], {
        liste: ['racine', 'fraction', 'puissance', 'pi', 'exp']
      })
    }
    mqRestriction(stor.elt.inputmqList[0], /[\d,/^-]/, { commandes: ['racine', 'fraction', 'puissance', 'pi', 'exp'] })
    // pour la correction
    stor.elt.inputmqList[0].typeReponse = ['nombre', 'exact']
    stor.elt.inputmqList[0].reponse = [j3pCalculValeur(stor.solution)]
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.elt.inputmqList[0].id] })
    j3pFocus(stor.elt.inputmqList[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // on garanti ce type (pas sûr que la surcharge ne renvoie pas une string…)
        if (typeof ds.dim === 'string') {
          console.error(Error('surcharge retourne une string pour un param entier (dim)'))
          ds.dim = Number(ds.dim)
        }
        if (!Number.isInteger(ds.dim) || ![2, 3].includes(ds.dim)) {
          console.error(Error('dim invalide'), ds.dim)
          ds.dim = 2
        }
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1' })
        me.afficheTitre(textes.phraseTitre)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // chargement webXcas
        loadWebXcas()
          .then(
            // success
            (_xcas) => {
              xcas = _xcas
              setWebXcas(_xcas)
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'xCas n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()

        enonceMain()
      }
      break // case "enonce":
    }
    case 'correction': {
      const fctsValid = stor.fctsValid
      const reponse = fctsValid.validationGlobale()

      if (!reponse.aRepondu && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        return me.finCorrection()
      } else {
        // Bonne réponse
        if (reponse.bonneReponse) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          afficheCorrection(true)
          me.typederreurs[0]++
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            afficheCorrection(false)
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux

            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              // changement affichage du nombre d’essais
              const nbEsssais = ds.nbchances - me.essaiCourant
              j3pEmpty(stor.zoneCons3)
              j3pAffiche(stor.zoneCons3, '', textes.phraseNbChances, { e: nbEsssais })
              return me.finCorrection()
            } else {
              // Erreur au nème essai
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              afficheCorrection(false)
              me.typederreurs[2]++
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
