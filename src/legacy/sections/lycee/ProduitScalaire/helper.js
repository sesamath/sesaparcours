import { j3pGetRandomInt, j3pRandomdec } from 'src/legacy/core/functions'

// la fct d’évaluation des expressions par webXcas (initialisé par setWebXcas)
let xcas

/**
 * retourne une fraction  sous la forme a sur b où
 * a entier choisi dans Interv1
 * b entier positif choisi dans Interv2
 * (fraction est réduite par webxcas)
 *
 * Le retour est assuré sous forme d’un tableau où
 * - le 1er élément (0) est le nombre utilisable avec WebXcas
 * - le 2nd élément est la version affichable du nombre sous forme latex (avec mathquill)
 * @param interv1
 * @param interv2
 * @param aveca
 * @return {string[]}
 */
function aSurb (interv1, interv2, aveca) {
  if (typeof xcas !== 'function') throw Error('Il faut appeler setWebXcas avant de l’utiliser')
  let a, b
  if (Number(aveca) === 1) {
    const hasard = j3pGetRandomInt(1, 3)
    a = (hasard < 2) ? -1 : 1
  } else {
    do {
      a = j3pGetRandomInt(interv1)
    } while (a === 0 || a === 1)
  }
  do {
    b = j3pGetRandomInt(interv2)
  } while (b === 0 || b === 1)
  const rep = []
  rep[0] = xcas('simplify(' + a + '/' + b + ')')
  // on débarasse la réponse  àla commande latex(simplify... des guillemts placés autour
  let formeL = xcas('latex(simplify(' + a + '/' + b + '))')
  formeL = formeL.substring(1, formeL.length - 1)
  rep[1] = formeL

  return rep
} // aSurb

/**
 * retourne un nombe sous la forme a * Rac(b) où
 * a entier choisit dans Interv1
 * b entier positif choisit dans Interv2
 * le retour est aussuré sous forme de tableau où
 * le 1er élément (0) est le nombre utilisable avec WebXcas
 * le 2nd élément est la version affcihable du nombre sous forme latex (avec mathquill)
 * de plus la racine est réduite par webxcas
 * interv2 doit ABSOLUMENT être un intervalle de nombres positifs
 * @param interv1
 * @param interv2
 * @param aveca
 * @return {*[]}
 */
export function aRacb (interv1, interv2, aveca) {
  if (typeof xcas !== 'function') throw Error('Il faut appeler setWebXcas avant de l’utiliser')
  let a, b, rac
  if (Number(aveca) === 1) {
    const hasard = j3pGetRandomInt(1, 3)
    a = (hasard < 2) ? -1 : 1
  } else {
    do {
      a = j3pGetRandomInt(interv1)
    } while (a === 0 || a === 1)
  }
  do {
    b = j3pGetRandomInt(interv2)
    rac = Math.round(Math.sqrt(b), 0)
    rac = Math.pow(rac, 2)
  } while (rac === b || b === 1)
  const rep = []
  rep[0] = xcas('simplify(' + a + '*sqrt(' + b + ')')
  let formeL = xcas('latex(simplify(' + a + '*sqrt(' + b + '))')
  formeL = formeL.substring(1, formeL.length - 1)
  rep[1] = formeL
  return rep
} // fin fonction aRacb

/**
 * retourne un nombe sous la forme a + b * Rac(d) où
 * a entier choisi dans Interv1
 * b entier choisi dans Interv2
 * d entier positif choisit dans Interv3
 * d
 * le retour est assuré sous forme de tableau où
 * le 1er élément (0) est le nombre utilisable avec WebXcas
 * le 2nd élément est la version affcihabkle du nombre sous forme latex (avec mathquill)
 * interv3 doit ABSOLUMENT être un intervalle de nombres positifs
 * coefb : o pourindiquer que le b est à 1
 * @param interv1
 * @param interv2
 * @param interv3
 * @param coefd
 * @return {*[]}
 */
function aPlusdRacb (interv1, interv2, interv3, coefd) {
  if (typeof xcas !== 'function') throw Error('Il faut appeler setWebXcas avant de l’utiliser')
  const a = j3pGetRandomInt(interv1)
  const rep = []
  const d = (Number(coefd) === 1) ? 1 : j3pGetRandomInt(interv2)
  let rac, b
  do {
    b = j3pGetRandomInt(interv3)
    rac = Math.round(Math.sqrt(b), 0)
    rac = Math.pow(rac, 2)
  } while (rac === b || b === 1)
  const r0 = (d > 0)
    ? a + '+' + d + '*sqrt(' + b + ')'
    : a + '-' + Math.abs(d) + '*sqrt(' + b + ')'
  rep[0] = xcas('simplify(' + r0 + ')')
  let formeL = xcas('latex(simplify(' + r0 + '))')
  formeL = formeL.substring(1, formeL.length - 1)
  rep[1] = formeL
  return rep
} // aPlusdRacb

/**
 * sert à produire une syntaxe au format latex pour afficher un vecteur
 * @param nomVec Nom du vecteur
 * @param {number[]} coordVec
 * @param {string} [sensCoord] Passer V pour le sens vertical (ignoré si dimension ≠ 2)
 * @return {string}
 */
export function afficheVec (nomVec, coordVec, sensCoord) {
  const afficheVec = (nomVec.length === 1) ? '\\vec{' + nomVec + '}' : '\\vecteur{' + nomVec + '}'
  if (coordVec.length === 2) {
    const [x, y] = coordVec
    if (sensCoord === 'V') {
      return afficheVec + ' \\quad \\binom{' + x + '}{' + y + '}'
    }
    return afficheVec + ' \\quad \\left(' + x + '\\quad ; \\quad ' + y + '\\right)'
  } else if (coordVec.length > 2) {
    const [x, y, z] = coordVec
    return afficheVec + ' \\quad \\left(' + x + '\\quad ; \\quad ' + y + '\\quad ; \\quad ' + z + '\\right)'
  }
  console.error(Error('afficheVec appelé avec des paramètres invalides'), nomVec, coordVec, sensCoord)
  throw Error('paramètres invalides')
} // fin fonction afficheVec

/**
 * Retourne la string LaTeX avec le nom du point et ses coordonnées (entre parenthèses à la bonne hauteur)
 * @param {string} nomPt
 * @param {number[]|string[]} coordPt Les coordonées du point (dimension quelconque)
 * @return {string}
 */
export function affichePt (nomPt, coordPt) {
  return nomPt + ' \\quad \\left(' + coordPt.join('\\quad ; \\quad ') + '\\right)'
} // fin fonction affichePt

/**
 * Calcule les coordonnées d’un vecteur défini par deux points.
 * Retourne deux arrays contenant chaucun les coordonnées du vecteur AB, le premier au format webXcas et le 2nd au format Latex
 * @param {number[]} coordPtA
 * @param {number[]} coordPtB
 * @return {Array[]} deux tableaux de strings
 */
export function vecAB (coordPtA, coordPtB) {
  if (typeof xcas !== 'function') throw Error('Il faut appeler setWebXcas avant de l’utiliser')
  const rep1 = []
  const rep2 = []
  let coordAB, affVec
  for (let i = 0; i < coordPtA.length; i++) {
    if (isNaN(coordPtB[i] - coordPtA[i])) {
      coordAB = '(' + coordPtB[i] + ')-(' + coordPtA[i] + ')'
    } else {
      coordAB = coordPtB[i] - coordPtA[i]
    }
    rep1[i] = xcas('simplify(' + coordAB + ')')
    affVec = xcas('latex(simplify(' + coordAB + '))')
    affVec = affVec.substring(1, affVec.length - 1)
    rep2[i] = affVec
  }

  return [rep1, rep2]
} // fin fonction vecAB

/**
 * renvoie les coordonnées aléatoires (vecteur ou point)
 * mais pas le vecteur nul ou l’origine du repère
 * infosType est un array  qui donne les infos
 * rang 0 : le type qui doit être choisi parmi : entier, decimal ou racineSimple ou racineAffine ou fraction
 * rang suivants dépendent du type choisi :
 * pour entier : rang 1 l’intervalle
 * pour décimal : rang 1 l’intervalle et rang 2 le nombre de decimales
 * pour racineSimple (a*racb) :
 *      rang 1 : effetif des coordonnées avec racine
 *      rang 2 : intervalle pour a
 *      rang 3 : intervalle pour b
 *      rang 4 : sans le a si 0, avec a sinon
 * pour racineAffine (a+d*racb) :
 *      rang 1 : effetif des coorodnnées avec racine
 *      rang 2 : intervalle pour a
 *      rang 3 : intervalle pour b
 *      rang 4 : intervalle pour d
 *      rang 5 : sans le d si 0, avec d sinon
 * pour fraction :
 *      rang 1 : effetif des coorodnnées avec fraction
 *      rang 2 : intervalle pour a
 *      rang 3 intervalle pour b
 * l’effectif : 1 ou 2 ou 3 (selon dim) pour laisser la possibilité de faire varier la difficulté
 * @param dim
 * @param infosType
 * @return {*[][]}
 */
export function coords (dim, infosType) {
  if (typeof xcas !== 'function') throw Error('Il faut appeler setWebXcas avant de l’utiliser')
  const rep1 = [] // contiendra les coordonnées (façon Xcas si nécessaire)
  const rep2 = [] // contiendra les coordonnées (façon latex)
  const type = infosType[0]
  let intervalle, nbNuls, nbDecimales, i, j, k
  switch (type) {
    case 'entier':
      intervalle = infosType[1]
      do {
        nbNuls = 0
        for (i = 0; i < dim; i++) {
          rep1[i] = j3pGetRandomInt(intervalle)
          rep2[i] = rep1[i]
          if (rep1[i] === 0) { nbNuls++ }
        }
      } while (nbNuls === dim)
      break
    case 'decimal':
      intervalle = infosType[1]
      nbDecimales = infosType[2]
      do {
        nbNuls = 0
        for (i = 0; i < dim; i++) {
          rep1[i] = j3pRandomdec(intervalle + ',' + nbDecimales)
          rep2[i] = rep1[i]
          if (rep1[i] === 0) { nbNuls++ }
        }
      } while (nbNuls === dim)
      break
    case 'racineSimple': {
      const effectif = infosType[1]
      const interv1 = infosType[2]
      const interv2 = infosType[3]
      const coefa = infosType[4]
      let nb = []
      let tabTemp1, tabTemp2
      switch (effectif) {
        case dim :
          for (i = 0; i < dim; i++) {
            nb = aRacb(interv1, interv2, coefa)
            rep1[i] = nb[0]
            rep2[i] = nb[1]
          }

          break
        case dim - 1:
          tabTemp1 = []
          tabTemp2 = []
          for (i = 0; i < dim - 1; i++) {
            nb = aRacb(interv1, interv2, coefa)
            tabTemp1[i] = nb[0]
            tabTemp2[i] = nb[1]
          }

          tabTemp1[dim - 1] = j3pGetRandomInt(interv1)
          tabTemp2[dim - 1] = tabTemp1[dim - 1]
          j = j3pGetRandomInt(0, dim)
          i = 0
          for (k = j; k < j + dim; k++) {
            rep1[i] = tabTemp1[k % dim]
            rep2[i] = tabTemp2[k % dim]
            i++
          }

          break
        case 1 :
          tabTemp1 = []
          tabTemp2 = []
          for (i = 0; i < dim - 1; i++) {
            tabTemp1[i] = j3pGetRandomInt(interv1)
            tabTemp2[i] = tabTemp1[i]
          }
          nb = aRacb(interv1, interv2, coefa)
          tabTemp1[dim - 1] = nb[0]
          tabTemp2[dim - 1] = nb[1]
          j = j3pGetRandomInt(0, dim)
          i = 0
          for (k = j; k < j + dim; k++) {
            rep1[i] = tabTemp1[k % dim]
            rep2[i] = tabTemp2[k % dim]
            i++
          }
      }
      break
    }
    case 'racineAffine': {
      const effectif = infosType[1]
      const interv1 = infosType[2]
      const interv2 = infosType[3]
      const interv3 = infosType[4]
      const coefd = infosType[5]
      let nb
      switch (effectif) {
        case dim :
          for (i = 0; i < dim; i++) {
            nb = aPlusdRacb(interv1, interv2, interv3, coefd)
            rep1[i] = nb[0]
            rep2[i] = nb[1]
          }
          break
        case dim - 1: {
          const tabTemp1 = []
          const tabTemp2 = []
          for (i = 0; i < dim - 1; i++) {
            nb = aPlusdRacb(interv1, interv2, interv3, coefd)
            tabTemp1[i] = nb[0]
            tabTemp2[i] = nb[1]
          }
          tabTemp1[dim - 1] = j3pGetRandomInt(interv1)
          tabTemp2[dim - 1] = tabTemp1[dim - 1]
          j = j3pGetRandomInt(0, dim)
          i = 0
          for (k = j; k < j + dim; k++) {
            rep1[i] = tabTemp1[k % dim]
            rep2[i] = tabTemp2[k % dim]
            i++
          }
          break
        } // dim -1

        case 1 : {
          const tabTemp1 = []
          const tabTemp2 = []
          for (i = 0; i < dim - 1; i++) {
            tabTemp1[i] = j3pGetRandomInt(interv1)
            tabTemp2[i] = tabTemp1[i]
          }
          nb = aPlusdRacb(interv1, interv2, interv3, coefd)
          tabTemp1[dim - 1] = nb[0]
          tabTemp2[dim - 1] = nb[1]
          j = j3pGetRandomInt(0, dim)
          i = 0
          for (k = j; k < j + dim; k++) {
            rep1[i] = tabTemp1[k % dim]
            rep2[i] = tabTemp2[k % dim]
            i++
          }
          break
        }
        default:
          console.error(Error(`Cas effectif=${effectif} non géré`))
      } // switch effectif
      break
    } // racineAffine

    case 'fraction': {
      // le 1er est le type, osef
      const [, effectif, interv1, interv2, coefa] = infosType
      const tabTemp1 = []
      const tabTemp2 = []
      switch (effectif) {
        case dim :
          for (i = 0; i < dim; i++) {
            const nb = aSurb(interv1, interv2, coefa)
            rep1[i] = nb[0]
            rep2[i] = nb[1]
          }
          break

        case dim - 1:
          for (i = 0; i < dim - 1; i++) {
            const [xcasExpr, latex] = aSurb(interv1, interv2, coefa)
            tabTemp1[i] = xcasExpr
            tabTemp2[i] = latex
          }
          tabTemp1[dim - 1] = j3pGetRandomInt(interv1)
          tabTemp2[dim - 1] = tabTemp1[dim - 1]
          j = j3pGetRandomInt(0, dim)
          i = 0
          for (let k = j; k < j + dim; k++) {
            rep1[i] = tabTemp1[k % dim]
            rep2[i] = tabTemp2[k % dim]
            i++
          }
          break

        case 1 : {
          for (i = 0; i < dim - 1; i++) {
            tabTemp1[i] = j3pGetRandomInt(interv1)
            tabTemp2[i] = tabTemp1[i]
          }
          const [xcasExpr, latex] = aSurb(interv1, interv2, coefa)
          tabTemp1[dim - 1] = xcasExpr
          tabTemp2[dim - 1] = latex
          j = j3pGetRandomInt(0, dim)
          i = 0
          for (k = j; k < j + dim; k++) {
            rep1[i] = tabTemp1[k % 3]
            rep2[i] = tabTemp2[k % 3]
            i++
          }
          break
        }

        default:
          console.error(Error(`Effectif ${effectif} non géré`))
      } // switch effectif
      break
    } // fraction

    default:
      console.error(Error(`Type ${type} non géré`))
  } // switch type
  return [rep1, rep2]
} // fin fonction coordonnees

/**
 * Affecte la fonction d’évaluation de webxcas (qui doit être chargé avec loadWebXcas par la section)
 * @param _xcas
 */
export function setWebXcas (_xcas) {
  xcas = _xcas
}
