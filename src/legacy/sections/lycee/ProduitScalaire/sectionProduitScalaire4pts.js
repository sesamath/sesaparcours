import { j3pAddElt, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import loadWebXcas from 'src/legacy/outils/xcas/loadWebXcas'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { affichePt, afficheVec, coords, setWebXcas, vecAB } from 'src/legacy/sections/lycee/ProduitScalaire/helper'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// la fct d’évaluation des expressions par webXcas
let xcas

/*
        Auteur : Christian Buso
        Date : 15/06/15
        Remarques : après 3 sections, voici uen version qui se veut moins brouillonne!
 */

/*
    Cette variable renseigne :
        - les outils utilisés par la section
        - les paramètres de la section (nom,valeur par défaut,type,description

    dim : 2 ou 3 (plan ou espace) : peut être surchargée
    n_nbDecimalesCoordonnees : typiquement 0 (coordonnées entières) ou 1 (décimle). On peut mettre plus mais à quoi bon!.  peut être surchargée

    minCoord : valeur minimale d’une coordonnée :(entière). peut être surchargée
    maxCoord : valeur maximale d’une coordonnée :(entière). peut être surchargée

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['dim', 2, 'entier', 'Dimension de l’espace : 2 ou 3'],
    ['sensCoordVect', 'V', 'string', 'Sens de l’affichage des coordonnées (dim 2 seulement) : V ou H'],
    ['typeCoord', 'entier', 'liste', 'Type des coordonnées : fraction (réduite) ou entier ou decimal ou racineSimple (A*racB) ou racineAffine (A+D*racB)', ['entier', 'decimal', 'racineSimple', 'racineAffine', 'fraction']],
    ['n_nbDecimalesCoordonnees', 1, 'entier', 'Nombre de décimales pour les coordonnées des vecteurs : 0 ou 1'],
    ['minCoord', -5, 'entier', 'minimum pour les coordonnées : choisir un entier, utilisé pour les coordonnées entières'],
    ['maxCoord', 6, 'entier', 'minimum pour les coordonnées : choisir un entier, utilisé pour les coordonnées entières'],
    ['sansA', 1, 'entier', 'Mettre la valeur 1 pour que  le coef a (dans a/b et/ou a*Rac(b)) vaille 1 (sans A donc!),pour toute autre valeur a ne sera ni égal à 1 ni nul'],
    ['sansD', 1, 'entier', 'Mettre la valeur 1 pour que  le coef d (a+d*Rac(b)) vaille 1 (sans D donc!),pour toute autre valeur d ne sera ni égal à 1 ni nul'],
    ['intCoefA', '[-5;5]', 'string', 'intervalle dans lequel sera choisi l’entier a de a/b ou a*Rac(b) ou a+d*Rac(b)'],
    ['intCoefB', '[2;5]', 'string', 'intervalle dans lequel sera choisi l’entier b de a/b ou a*Rac(b) ou a+d*Rac(b) : choisir une borne inférieure au moins à 2 (pas de négatif ni 0 ni 1)'],
    ['intCoefD', '[-5;5]', 'string', 'intervalle dans lequel sera choisi l’entier d de  a+d*Rac(b)'],
    ['effectif', 1, 'entier', 'effectif des coordonnées spéciales : racine ou fraction. Cela peut-être 1 ou 2 (ou 3 en dimension 3)']

  ]
}
const textes = {
  phraseEnonce: 'Dans un repère orthonormé, on donne trois points : <br>$£a \\qquad$ , $\\qquad £b$ , $\\qquad £c$ et $\\qquad £d$.<br> Calcule le produit scalaire :',
  phraseTitre: 'Produit scalaire<br>dans un repère orthonormé',
  phraseSolution1: 'On a :  $ £u \\quad $ et $ \\quad £v $',
  phraseSolution2: 'Donc:<br>$\\vecteur{£u} \\quad &middot; \\quad \\vecteur{£v } =£p$ <br>$\\vecteur{£u} \\quad &middot; \\quad \\vecteur{£v }=£s$<br>$\\vecteur{£u} \\quad &middot; \\quad \\vecteur{£v }=£r$',
  phraseSaisie: ' $\\vecteur{£u} \\quad &middot; \\quad \\vecteur{£v }=$ &1&.',
  phraseNbChances: 'Il te reste £e essai(s).'
}
/**
 * section ProduitScalaire4pts
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  //  fontion permettant d’afficher la correction expliquée version Rémi
  function afficheCorrection (bonneRep) {
    for (let i = 3; i <= 4; i++) j3pEmpty(stor['zoneCons' + i])
    let div3
    if (bonneRep) {
      div3 = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.enonce })
      div3.style.color = me.styles.cbien
    } else {
      div3 = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    }
    const sens = (ds.sensCoordVect === 'V' && ds.dim === 2) ? 'V' : 'H'
    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(div3, 'div')
    j3pAffiche(stor.zoneExpli1, '', textes.phraseSolution1,
      {
        u: afficheVec(stor.u[0], stor.u[1][1], sens),
        v: afficheVec(stor.v[0], stor.v[1][1], sens)
      })
    j3pAffiche(stor.zoneExpli2, '', textes.phraseSolution2,
      {
        u: stor.u[0],
        v: stor.v[0],
        r: stor.r,
        s: stor.s,
        p: stor.p
      })
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.zoneCons2, { padding: '10px 0' })
    j3pStyle(stor.zoneCons4, { padding: '10px 0' })

    // les coordonnées des points
    let infosPts
    switch (ds.typeCoord) {
      case 'entier':
        infosPts = ['entier', '[' + ds.minCoord + ';' + ds.maxCoord + ']']
        break
      case 'decimal':
        infosPts = ['decimal', '[' + ds.minCoord + ';' + ds.maxCoord + ']', ds.n_nbDecimalesCoordonnees]
        break
      case 'racineSimple' :
        infosPts = ['racineSimple', ds.effectif, ds.intCoefA, ds.intCoefB, ds.sansA]
        break
      case 'racineAffine' :
        infosPts = ['racineAffine', ds.effectif, ds.intCoefA, ds.intCoefD, ds.intCoefB, ds.sansD]
        break
      case 'fraction' :
        infosPts = ['fraction', ds.effectif, ds.intCoefA, ds.intCoefB, ds.sansA]
        break
    }
    const lesPtsCalc = []
    const lesPtsAff = []
    let unPt = []
    let nomsPts = []

    for (let k = 0; k < 4; k++) {
      unPt = coords(ds.dim, infosPts)
      lesPtsCalc[k] = unPt[0]
      lesPtsAff[k] = unPt[1]
    }

    const choixNomsPts = [['A', 'B', 'C', 'D'], ['E', 'F', 'G', 'H'], ['P', 'Q', 'R', 'S'], ['K', 'L', 'M', 'N']]
    let choix = j3pGetRandomInt(0, 12)
    switch (choix) {
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
        nomsPts = choixNomsPts[0]
        break
      case 5:
      case 6:
      case 7 :
        nomsPts = choixNomsPts[1]
        break
      case 8:
      case 9:
      case 10:
        nomsPts = choixNomsPts[2]
        break
      case 11:
      case 12:
      case 13:
        nomsPts = choixNomsPts[3]
        break
    }
    let indice = j3pGetRandomInt(0, 4)
    indice = indice % 4
    const lePtA = [nomsPts[indice], lesPtsCalc[indice], lesPtsAff[indice]]
    stor.a = lePtA
    indice = (indice + 1) % 4
    const lePtB = [nomsPts[indice], lesPtsCalc[indice], lesPtsAff[indice]]
    stor.b = lePtB
    indice = (indice + 1) % 4
    const lePtC = [nomsPts[indice], lesPtsCalc[indice], lesPtsAff[indice]]
    stor.c = lePtC
    indice = (indice + 1) % 4
    const lePtD = [nomsPts[indice], lesPtsCalc[indice], lesPtsAff[indice]]
    stor.d = lePtD

    choix = j3pGetRandomInt(0, 5)
    switch (choix) {
      case 0:
        stor.u = [lePtA[0] + lePtB[0], vecAB(lePtA[1], lePtB[1])]
        stor.v = [lePtD[0] + lePtC[0], vecAB(lePtD[1], lePtC[1])]
        break
      case 1:
        stor.u = [lePtA[0] + lePtB[0], vecAB(lePtA[1], lePtB[1])]
        stor.v = [lePtC[0] + lePtD[0], vecAB(lePtC[1], lePtD[1])]
        break
      case 2:
        stor.u = [lePtB[0] + lePtA[0], vecAB(lePtB[1], lePtA[1])]
        stor.v = [lePtC[0] + lePtD[0], vecAB(lePtC[1], lePtD[1])]
        break
      case 3:
        stor.u = [lePtB[0] + lePtA[0], vecAB(lePtB[1], lePtA[1])]
        stor.v = [lePtD[0] + lePtC[0], vecAB(lePtD[1], lePtC[1])]

        break
      default :
        stor.u = [lePtA[0] + lePtB[0], vecAB(lePtA[1], lePtB[1])]
        stor.v = [lePtC[0] + lePtD[0], vecAB(lePtC[1], lePtD[1])]
    }
    j3pAffiche(stor.zoneCons1, '', textes.phraseEnonce, {
      a: affichePt(lePtA[0], lePtA[2]),
      b: affichePt(lePtB[0], lePtB[2]),
      c: affichePt(lePtC[0], lePtC[2]),
      d: affichePt(lePtD[0], lePtD[2])
    })
    const elt = j3pAffiche(stor.zoneCons2, '', textes.phraseSaisie, {
      u: stor.u[0],
      v: stor.v[0],
      inputmq1: { texte: '' }
    })
    // construction de la solution
    let laSolution = ''
    let eltSol1 = ''
    let eltSol2 = ''
    let ch = ''
    let coordu = ''
    let coordv = ''
    for (let k = 0; k < ds.dim; k++) {
      coordu = stor.u[1][1][k] + ''
      coordv = stor.v[1][1][k] + ''
      laSolution = laSolution + '+normal((' + stor.u[1][0][k] + ')*(' + stor.v[1][0][k] + '))'
      if ((coordu.charCodeAt(0) === 45 & k > 0) | (ds.typeCoord === 'racineAffine' & coordu.includes('sqrt')) | (ds.typeCoord === 'racineSimple' & coordu.includes('sqrt'))) { coordu = '(' + coordu + ')' }

      if (coordv.charCodeAt(0) === 45 | (ds.typeCoord === 'racineAffine' && coordv.includes('sqrt')) | (ds.typeCoord === 'racineSimple' & coordv.includes('sqrt'))) { coordv = '(' + coordv + ')' }
      eltSol1 = eltSol1 + '+' + coordu + ' \\times ' + coordv
      ch = xcas('latex(simplify((' + stor.u[1][0][k] + ') *(' + stor.v[1][0][k] + ')))')
      ch = ch.substring(1, ch.length - 1)
      if ((ch.charCodeAt(0) === 45 && k > 0)) {
        eltSol2 = eltSol2 + '+(' + ch + ')'
      } else {
        eltSol2 = eltSol2 + '+' + ch
      }
    }
    laSolution = laSolution.substring(1)

    stor.r = xcas('latex(simplify(' + laSolution + '))')
    stor.r = stor.r.substring(1, stor.r.length - 1)
    stor.solution = xcas('simplify(' + laSolution + ')')

    stor.p = eltSol1.substring(1)
    stor.s = eltSol2.substring(1)
    stor.zoneInput = elt.inputmqList[0]
    // Zone de saisie
    j3pAffiche(stor.zoneCons3, '', textes.phraseNbChances, { e: ds.nbchances })

    // on affiche la palette lorsque c’est nécessaire
    if (ds.typeCoord === 'racineSimple' | ds.typeCoord === 'racineAffine' || ds.typeCoord === 'fraction') {
      j3pPaletteMathquill(stor.zoneCons4, stor.zoneInput, {
        liste: ['racine', 'fraction', 'puissance', 'pi', 'exp']
      })
      mqRestriction(stor.zoneInput, '\\d,.-/', { commandes: ['racine', 'fraction', 'puissance', 'pi', 'exp'] })
    } else {
      mqRestriction(stor.zoneInput, '\\d,.-')
    }
    // pour la correction
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [j3pCalculValeur(stor.solution)]
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.zoneInput.id] })
    j3pFocus(stor.zoneInput)
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1' })
        me.afficheTitre(textes.phraseTitre)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // chargement webXcas
        loadWebXcas()
          .then(
            // success
            (_xcas) => {
              xcas = _xcas
              setWebXcas(_xcas)
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'xCas n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break
    } // case "enonce":

    case 'correction':
      {
        const fctsValid = stor.fctsValid
        const reponse = fctsValid.validationGlobale()

        // console.log('bonne reponse attendue'+reponse.bonneReponse);

        if (!reponse.aRepondu && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // afficheCorrection(true);
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // changement affichage du nombre d’essais
                const nbEsssais = ds.nbchances - me.essaiCourant
                j3pEmpty(stor.zoneCons3)
                j3pAffiche(stor.zoneCons3, '', textes.phraseNbChances, { e: nbEsssais })
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
        // Obligatoire
        me.finCorrection('navigation', true)
      }
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      // Obligatoire
      me.finNavigation(true)
      break // case "navigation":
  }
}
