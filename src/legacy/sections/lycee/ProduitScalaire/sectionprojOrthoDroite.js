import { j3pAddElt, j3pNombre, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pMonome, j3pPGCD } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pAfficheSysteme } from 'src/legacy/core/functionsSysteme'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        février 2020
        L’objectif de cette section est de déterminer les coordonnées du projeté orthogonal d’un point sur une droite dont on a l’équation cartésienne
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Projeté orthogonal d’un point sur une droite',
  // on donne les phrases de la consigne
  consigne1: 'Dans un repère orthonormé du plan, on considère la droite $d$ d’équation $£e$ et le point $£A$ de coordonnées $£{cA}$.',
  consigne2: 'Un vecteur normal à cette droite a pour coordonnées $\\matrixtwoone{\\editable{}}{\\editable{}}$.', // $($&1&$;$&2&$)$.',
  consigne3: 'Une équation de la droite perpendiculaire à $d$ passant par $£A$ a alors pour équation',
  consigne4: '$\\vec{n}\\binom{£a}{£b}$ est un vecteur normal à $d$ et la droite $d’$ perpendiculaire à $d$ passant par $£A$ a pour équation $£f$.',
  consigne5: 'Le projeté orthogonal de $£A$ sur $d$ a alors pour coordonnées $($&1&$;$&2&$)$.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Une équation de droite contient nécessairement le signe "="&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: '$d$ ayant pour équation cartésienne $£e$, un vecteur normal à cette droite a pour coordonnées $\\matrixtwoone{£a}{£b}$.',
  corr2: 'Ce vecteur est alors un vecteur directeur de la droite $d’$ perpendiculaire à $d$ passant par $£A$ et ainsi $d’$ a une équation de la forme $£{f1}$.',
  corr3: 'De plus $£A£{cA}$ est sur cette droite ce qui donne $£{f2}$ et ainsi $c=£{c2}$. Donc une équation de $d’$ est $£f$.',
  corr4: 'Le projeté orthogonal de $£A$ sur $d$ est alors l’intersection de $d$ et $d’$.',
  corr5: 'On résout alors le système ‡1‡.',
  corr6: 'La combinaison $£l$ nous permet d’obtenir le système équivalent ‡1‡ $\\iff$ ‡2‡.',
  corr7: 'On obtient donc ‡1‡ et les coordonnées du point d’intersection du projeté orthogonal de $£A$ sur $d$ sont $£{cB}$.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const explications = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    if (me.questionCourante % ds.nbetapes === 1) {
      for (let i = 1; i <= 3; i++) {
        const div = j3pAddElt(explications, 'div')
        j3pAffiche(div, '', textes['corr' + i], stor.objCons)
      }
    } else {
      let div = j3pAddElt(explications, 'div')
      j3pAffiche(div, '', textes.corr4, stor.objCons)
      div = j3pAddElt(explications, 'div')
      const color = explications.style.color
      let tab = [['$' + stor.objCons.e + '$', '$' + stor.objCons.f + '$']]
      let objAffiche = {}
      j3pAfficheSysteme(div, '', textes.corr5, tab, objAffiche, color)
      div = j3pAddElt(explications, 'div')
      tab = [['$' + stor.objCons.e + '$', '$' + stor.objCons.comb1 + '$'], ['$' + stor.objCons.e + '$', '$y=' + stor.objCons.ordB + '$']]
      objAffiche = { l: stor.objCons.l }
      j3pAfficheSysteme(div, '', textes.corr6, tab, objAffiche, color)
      div = j3pAddElt(explications, 'div')
      tab = [['$x=' + stor.objCons.absB + '$', '$y=' + stor.objCons.ordB + '$']]
      objAffiche = { A: stor.objCons.A, cB: stor.objCons.cB }
      j3pAfficheSysteme(div, '', textes.corr7, tab, objAffiche, explications.style.color)
    }
  }

  function convertitNb (str) {
    // str est une chaîne de caractères (coef devant une inconnue). On la convertit sous les forme d’un nombre
    let nombre
    if (str === '') {
      nombre = 1
    } else if (str === '-') {
      nombre = -1
    } else {
      nombre = j3pNombre(str)
    }
    return nombre
  }

  function enonceInitFirst () {
    ds = me.donneesSection
    me.surcharge({ nbetapes: 2 })
    // Construction de la page
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
    me.afficheTitre(textes.titre_exo)
    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    if (me.questionCourante % ds.nbetapes === 1) {
      // équation de la forme ax+by+c=0
      stor.objCons = {}
      stor.objCons.A = String.fromCharCode(65 + j3pGetRandomInt(0, 5))
      stor.objCons.a = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 6)
      do {
        stor.objCons.b = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 6)
      } while (Math.abs(stor.objCons.a) === Math.abs(stor.objCons.b))
      const pgcdab = j3pPGCD(Math.abs(stor.objCons.a), Math.abs(stor.objCons.b))
      stor.objCons.a = stor.objCons.a / pgcdab
      stor.objCons.b = stor.objCons.b / pgcdab
      // On détermine d’abord les coordonnées du projeté orthogonal
      stor.objCons.absB = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 8)
      stor.objCons.ordB = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 8)
      stor.objCons.cB = '(' + stor.objCons.absB + ';' + stor.objCons.ordB + ')'
      // Je peux enfin trouver le dernier paramètre dans l’équation ax+by+c=0
      stor.objCons.c = -stor.objCons.a * stor.objCons.absB - stor.objCons.b * stor.objCons.ordB
      // Maintenant je cherche un point A de coordonnées simple de sorte que B soit son projeté ortho sur d
      const pgcdVectNorm = j3pPGCD(Math.abs(stor.objCons.a), Math.abs(stor.objCons.b))
      const coefMult = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 4)
      stor.objCons.absA = stor.objCons.absB + coefMult * stor.objCons.a / pgcdVectNorm
      stor.objCons.ordA = stor.objCons.ordB + coefMult * stor.objCons.b / pgcdVectNorm
      stor.objCons.cA = '(' + stor.objCons.absA + ';' + stor.objCons.ordA + ')'
      // La droite d a pour équation ax+by+c=0
      // La perpendiculaire à d passant par A a pour équation bx-ay+c2=0 où c2 est calculé à l’aide des coordonnées de A
      stor.objCons.c2 = -stor.objCons.b * stor.objCons.absA + stor.objCons.a * stor.objCons.ordA
      stor.objCons.c2 = Math.round(stor.objCons.c2 * 1000) / 1000 // Au cas où je n’aurais pas que des entiers
      stor.objCons.e = j3pMonome(1, 1, stor.objCons.a, 'x') + j3pMonome(2, 1, stor.objCons.b, 'y') + j3pMonome(3, 0, stor.objCons.c) + '=0'
      stor.objCons.f = j3pMonome(1, 1, stor.objCons.b, 'x') + j3pMonome(2, 1, -stor.objCons.a, 'y') + j3pMonome(3, 0, stor.objCons.c2) + '=0'
      stor.objCons.f1 = j3pMonome(1, 1, stor.objCons.b, 'x') + j3pMonome(2, 1, -stor.objCons.a, 'y') + '+c=0'
      // f2 est plus compliqué car c’est le calcul en remplaçant x et y par les coordonnées de A donc c’est plus pénible à écrire
      stor.objCons.f2 = (stor.objCons.b === 1)
        ? stor.objCons.absA
        : (stor.objCons.b === -1)
            ? (stor.objCons.absA >= 0) ? '-' + stor.objCons.absA : '-(' + stor.objCons.absA + ')'
            : (stor.objCons.absA >= 0) ? stor.objCons.b + '\\times ' + stor.objCons.absA : stor.objCons.b + '\\times (' + stor.objCons.absA + ')'
      const membreCalc2 = (stor.objCons.a === -1)
        ? (stor.objCons.ordA >= 0) ? '+' + stor.objCons.ordA : stor.objCons.ordA
        : (stor.objCons.a === 1)
            ? (stor.objCons.ordA >= 0) ? '-' + stor.objCons.ordA : '-(' + stor.objCons.ordA + ')'
            : (stor.objCons.a < 0)
                ? (stor.objCons.ordA >= 0) ? '+' + (-stor.objCons.a) + '\\times ' + stor.objCons.ordA : '+' + (-stor.objCons.a) + '\\times (' + stor.objCons.ordA + ')'
                : (stor.objCons.ordA >= 0) ? (-stor.objCons.a) + '\\times ' + stor.objCons.ordA : (-stor.objCons.a) + '\\times (' + stor.objCons.ordA + ')'
      stor.objCons.f2 += membreCalc2 + '+c=0'
      // Pour la résolution du système, je dois effectuer une combinaison linéaire sur les lignes
      stor.objCons.l = j3pMonome(1, 1, stor.objCons.b, 'L1') + j3pMonome(2, 1, -stor.objCons.a, 'L2')
      stor.objCons.comb1 = j3pMonome(1, 1, Math.round(stor.objCons.b * stor.objCons.b + stor.objCons.a * stor.objCons.a), 'y') + j3pMonome(2, 0, Math.round(stor.objCons.c * stor.objCons.b - stor.objCons.a * stor.objCons.c2)) + '=0'
    }
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    const nbDiv = (me.questionCourante % ds.nbetapes === 1) ? 4 : 3
    for (let i = 1; i <= nbDiv; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, stor.objCons)
    if (me.questionCourante % ds.nbetapes === 1) {
      const elt1 = j3pAffiche(stor.zoneCons2, '', textes.consigne2)
      j3pAffiche(stor.zoneCons3, '', textes.consigne3, { A: stor.objCons.A })
      const elt2 = j3pAffiche(stor.zoneCons4, '', '&1&', { inputmq1: { texte: '' } })
      stor.zoneInput = [elt1.inputmqefList[0], elt1.inputmqefList[1], elt2.inputmqList[0]]
      mqRestriction(stor.zoneInput[0], '\\d.,-')
      mqRestriction(stor.zoneInput[1], '\\d.,-')
      mqRestriction(stor.zoneInput[2], '\\d.,-=+xy/')
      stor.zoneInput[0].typeReponse = ['nombre', 'exact']
      stor.zoneInput[1].typeReponse = ['nombre', 'exact']
      stor.zoneInput[2].typeReponse = ['texte']
      stor.zoneInput[0].reponse = [stor.objCons.a]
      stor.zoneInput[1].reponse = [stor.objCons.b]
      // En fait dans ces deux cas, on peut utiliser un multiple de ces valeurs (pas nécessairement entiers)
      j3pFocus(stor.zoneInput[0])
      me.logIfDebug('équation attendue:', stor.objCons.f)
    } else {
      j3pAffiche(stor.zoneCons2, '', textes.consigne4, stor.objCons)
      const elt = j3pAffiche(stor.zoneCons3, '', textes.consigne5, {
        A: stor.objCons.A,
        inputmq1: { texte: '' },
        inputmq2: { texte: '' }
      })
      stor.zoneInput = [elt.inputmqList[0], elt.inputmqList[1]]
      stor.zoneInput.forEach(eltInput => {
        mqRestriction(eltInput, '\\d.,-')
        eltInput.typeReponse = ['nombre', 'exact']
      })
      stor.zoneInput[0].reponse = [stor.objCons.absB]
      stor.zoneInput[1].reponse = [stor.objCons.ordB]
      j3pFocus(stor.zoneInput[0])
    }
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    const mesZonesSaisie = stor.zoneInput.map(eltInput => eltInput.id)
    if (me.questionCourante % ds.nbetapes === 1) {
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: mesZonesSaisie,
        validePerso: mesZonesSaisie
      })
    } else {
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: mesZonesSaisie
      })
    }
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        let i, k, unSigneEgal
        if (reponse.aRepondu && (me.questionCourante % ds.nbetapes === 1)) {
        // Servira pour valider les coordonnées d’un vecteur normal
          const rep1 = j3pNombre(stor.fctsValid.zones.reponseSaisie[0])
          const rep2 = j3pNombre(stor.fctsValid.zones.reponseSaisie[1])
          const rep3 = stor.fctsValid.zones.reponseSaisie[2]
          const bonneReponse1 = Math.abs(stor.objCons.a * rep2 - stor.objCons.b * rep1) < Math.pow(10, -10)
          // Maintenant, je veux valider une équation de la droite perpendiculaire à d passant par B
          let bonneReponse2 = false
          const membresEgalite = rep3.split('=')
          unSigneEgal = rep3.split('=').length === 2
          if (unSigneEgal) {
          // On teste si l’égalité est équivalente à l’équation attendue
            let lecoefx = 0
            let lecoefy = 0
            for (i = 0; i <= 1; i++) {
            // console.log('membresEgalite:', membresEgalite[i], /(\-?[0-9]*\,?[0-9]*x)/g.test(membresEgalite[i]))
              const coefDevantx = /(-?[0-9]*,?[0-9]*x)/g // eslint-no-useless escape
              const coefDevanty = /(-?[0-9]*,?[0-9]*y)/g // eslint-no-useless escape
              const coefax = (coefDevantx.test(membresEgalite[i])) ? membresEgalite[i].match(coefDevantx) : []
              const coefby = (coefDevanty.test(membresEgalite[i])) ? membresEgalite[i].match(coefDevanty) : []
              // console.log('coefax:', coefax, 'coefby:', coefby)
              if (i === 0) {
              // membre de gauche
                if (coefax.length > 0) {
                  for (k = 0; k < coefax.length; k++) {
                    lecoefx += convertitNb(coefax[k].substring(0, coefax[k].length - 1))
                  }
                }
                if (coefby.length > 0) {
                  for (k = 0; k < coefby.length; k++) {
                    lecoefy += convertitNb(coefby[k].substring(0, coefby[k].length - 1))
                  }
                }
              } else {
              // membre de droite
                if (coefax.length > 0) {
                  for (k = 0; k < coefax.length; k++) {
                    lecoefx -= convertitNb(coefax[k].substring(0, coefax[k].length - 1))
                  }
                }
                if (coefby.length > 0) {
                  for (k = 0; k < coefby.length; k++) {
                    lecoefy -= convertitNb(coefby[k].substring(0, coefby[k].length - 1))
                  }
                }
              }
              me.logIfDebug('coefficient devant x et y :', lecoefx, lecoefy)
            }
            const coefMult = (Math.abs(lecoefx) > Math.abs(stor.objCons.b) || lecoefx === 0) ? lecoefx / stor.objCons.b : stor.objCons.b / lecoefx
            let membreTest = (membresEgalite[1] === '0')
              ? membresEgalite[0]
              : membresEgalite[0] + '-(' + membresEgalite[1] + ')'
            if (Math.abs(lecoefx) > Math.abs(stor.objCons.b)) {
              membreTest = '(' + membreTest + ')/(' + coefMult + ')'
            } else {
              membreTest = '(' + membreTest + ')*(' + coefMult + ')'
            }
            const arbreTest = new Tarbre(j3pMathquillXcas(stor.objCons.f.split('=')[0]), ['x', 'y'])
            const arbreEleve = new Tarbre(j3pMathquillXcas(membreTest), ['x', 'y'])
            bonneReponse2 = true
            for (i = 0; i <= 5; i++) {
              for (let j = 0; j <= 5; j++) {
              // console.log(stor.objCons.f, arbreTest.evalue([i, j]), ' comparé avec ', membreTest, arbreEleve.evalue([i, j]))
                bonneReponse2 = bonneReponse2 && (Math.abs(arbreTest.evalue([i, j]) - arbreEleve.evalue([i, j])) < Math.pow(10, -12))
              }
            }
          }

          reponse.bonneReponse = (bonneReponse1 && bonneReponse2)
          if (!bonneReponse1) {
            stor.fctsValid.zones.bonneReponse[0] = false
            stor.fctsValid.zones.bonneReponse[1] = false
          // si les 2 zones de validePerso ne sont pas bonnes, on met false dans stor.fctsValid.zones.bonneReponse[i] où i est la position de la zone dans le tableau mesZonesSaisie
          }
          stor.fctsValid.zones.bonneReponse[2] = bonneReponse2
          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          stor.fctsValid.zones.inputs.forEach(idInput => stor.fctsValid.coloreUneZone(idInput))
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          return me.finCorrection()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.questionCourante % ds.nbetapes === 1 && !unSigneEgal) stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                // ici il a encore la possibilité de se corriger
                return me.finCorrection()
              } else {
              // Erreur au nème essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
              }
            }
          }
        }
      }
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
