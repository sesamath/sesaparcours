import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pDetruit, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pPGCD, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/**
 * FIXME remplacer les appels xcas par l’usage de l’outil mathgraph
 * Cf outils/README_mathgraph.md
 *
 * Utilisé par ces ressources au 03/12/2019
 * https://bibliotheque.sesamath.net/ressource/apercevoir/32422
 * https://bibliotheque.sesamath.net/ressource/apercevoir/53881
 * https://bibliotheque.sesamath.net/ressource/apercevoir/53885
 * https://bibliotheque.sesamath.net/ressource/apercevoir/53887
 * https://bibliotheque.sesamath.net/ressource/apercevoir/53889
 * https://bibliotheque.sesamath.net/ressource/apercevoir/53891
 * https://bibliotheque.sesamath.net/ressource/apercevoir/53893
 *
 * https://commun.sesamath.net/ressource/apercevoir/530
 * https://commun.sesamath.net/ressource/apercevoir/5ca30785d7768872856ff943
 */
// section fortement paramétrable sur le calcul d’intégrales en TS :
// intégrale entre a et b de f(x) où f(x)=
// a) u’exp(u)
// b) uprime/u
// c) u'/racine(u)
// avec ou non coefficient multiplicateur
// avec ou non inversion de l’ordre de a et de b
// avec u affine ou de degré 2.

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['formules', ['uprimeexp(u)', 'uprime/u', 'uprime/racine(u)', 'uprime*u^n'], 'array', 'tableau des formules possibles'],
    ['coeff', '[0;1]', 'string', '[0;0](sans coeff multiplicateur) [1;1](avec) ou [0;1](aléatoire)'],
    ['degre_u', '[1;2]', 'string', ' [1;1] (affine), [2;2] (deg2) ou [1;2] (alea)']
  ],
  config: {
  }
}

const textes = {
  titre_exo: 'Calculs d’intégrales',
  question: 'Calcule la valeur exacte de l’intégrale suivante :',
  correction1_1: 'La fonction à intégrer est de la forme $u\'\\times \\mathrm{e}^{u}$, une primitive est donc $\\mathrm{e}^{u}$, ainsi l’intégrale est égale à :',
  correction1_2: 'La fonction à intégrer est de la forme $\\frac{u\'\\times \\mathrm{e}^{u}}{£a}$, une primitive est donc $\\frac{\\mathrm{e}^{u}}{£a}$, ainsi l’intégrale est égale à :',
  correction1_3: 'La fonction à intégrer est de la forme $£a \\times u\'\\times \\mathrm{e}^{u}$, une primitive est donc $£a \\times \\mathrm{e}^{u}$, ainsi l’intégrale est donc égale à :',
  correction2_1: 'La fonction à intégrer est de la forme $\\frac{u\'}{u}$ avec $u>0$ sur l’intervalle, une primitive est donc $\\mathrm{ln}(u)$, ainsi l’intégrale est égale à :',
  correction2_2: 'La fonction à intégrer est de la forme $\\frac{1}{£a}\\times \\frac{u\'}{u}$ avec $u>0$ sur l’intervalle, une primitive est donc $\\frac{\\mathrm{ln}(u)}{£a}$, ainsi l’intégrale est égale à :',
  correction2_3: 'La fonction à intégrer est de la forme $£a \\times \\frac{u\'}{u}$ avec $u>0$ sur l’intervalle, une primitive est donc $£a \\times \\mathrm{ln}(u)$, ainsi l’intégrale est égale à :',
  correction3_1: 'La fonction à intégrer est de la forme $\\frac{u\'}{\\sqrt{u}}$ avec $u>0$ sur l’intervalle, une primitive est donc $2\\sqrt{u}$, ainsi l’intégrale est égale à :',
  correction3_2: 'La fonction à intégrer est de la forme $\\frac{1}{£a}\\times \\frac{u\'}{\\sqrt{u}}$ avec $u>0$ sur l’intervalle, une primitive est donc $\\frac{1}{£a}\\times 2\\sqrt{u}$, ainsi l’intégrale est égale à :',
  correction3_3: 'La fonction à intégrer est de la forme $£a \\times \\frac{u\'}{\\sqrt{u}}$ avec $u>0$ sur l’intervalle, une primitive est donc $£a \\times 2\\sqrt{u}$, ainsi l’intégrale est égale à :',
  correction4_1: 'La fonction à intégrer est de la forme $u\'\\times u^n$, une primitive est donc $\\frac{u^{n+1}}{n+1}$, ainsi l’intégrale est égale à :',
  correction4_2: 'La fonction à intégrer est de la forme $\\frac{1}{£a}\\times u\'\\times u^n$, une primitive est donc $\\frac{1}{£a}\\times \\frac{u^{n+1}}{n+1}$, ainsi l’intégrale est égale à :',
  correction4_3: 'La fonction à intégrer est de la forme $£a \\times u\'\\times u^n$, une primitive est donc $£a \\times \\frac{u^{n+1}}{n+1}$, ainsi l’intégrale est égale à :',
  correction: 'Étudie bien la correction :',
  phrase_correction1: 'La primitive est fausse ainsi que le résultat final.',
  phrase_correction2: 'La primitive est juste mais pas le résultat final.',
  phrase_correction3: 'Le résultat final est juste mais pas la primitive.'

}

/**
 * section 750
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  // cette fonction (notre section)

  function changePalette1 () {
    j3pEmpty(stor.laPalette)
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], { liste: ['racine', 'exp', 'ln', 'fraction', 'puissance'] })
  }

  function changePalette2 () {
    j3pEmpty(stor.laPalette)
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput[1], { liste: ['racine', 'exp', 'ln', 'fraction', 'puissance'] })
  }

  function egaliteFormellePrimitive (expression1, expression2) {
    const liste = stor.listePourCalc
    const dif = stor.laBorneSup - stor.laBorneInf
    liste.giveFormula2('x1', stor.laBorneInf + '+' + String(dif) + '*0.121331')
    liste.giveFormula2('x2', stor.laBorneInf + '+' + String(dif) + '*0.213217')
    liste.giveFormula2('x3', stor.laBorneInf + '+' + String(dif) + '*0.330765')
    liste.giveFormula2('f', liste.traiteSignesMult(expression1))
    liste.giveFormula2('g', liste.traiteSignesMult(expression2))
    liste.calculateNG()
    return liste.valueOf('test') === 1
  }

  // Fonction qui retourne a/b simplifiée au format latex, sous forme d’entier si besoin
  function simplifieFrac (a, b) {
    // console.log("Dans simplifie frac, a="+a+" et b="+b)
    let expression
    if (a % b === 0) {
      expression = a / b + ''
    } else {
      const numerateur = a / j3pPGCD(Math.abs(a), Math.abs(b))
      const denominateur = b / j3pPGCD(Math.abs(a), Math.abs(b))
      expression = '\\frac{' + numerateur + '}{' + denominateur + '}'
    }
    // //console.log("expression:"+expression)
    return expression
  }

  function macorrection (objetReponseEleve, bonneReponse) {
    const unechaine = stor.phrase[0]
    j3pDetruit(stor.laPalette)
    // console.log('objetReponseEleve.simplifie=' + objetReponseEleve.simplifie)
    if (objetReponseEleve.simplifie) {
      // console.log('simplifié')
      me.typederreurs[1]++
    }
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    const expl1 = j3pAddElt(zoneExpli, 'div')
    const expl2 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(expl1, '', unechaine, { a: stor.phrase[1] })
    j3pAffiche(expl2, '', '$' + stor.etapes[0] + '$')
  }

  function mathquillMtg32 (chaine) {
    // console.log('chaine dans mathquillMtg32=' + chaine)

    function accoladeFermante (chaine, index) {
      // fonction qui donne la position de l’accolade fermante correspondant à l’accolade ouvrante positionnée en index de la chaine
      // penser à une condition d’arrêt pour ne pas faire planter le truc si la saisie est mal écrite
      // (ptet au debut de conversion mathquill : même nb d’accolades ouvrantes que fermantes)
      // pour l’instant 1 accolade ouvrante (à l’appel de la fonction)
      let indexaccolade = 1
      // A CORRIGER SI text{}
      let indexvariable = index
      while (indexaccolade !== 0) {
        // je peux avoir des accolades internes (par groupe de 2 necessairement)
        indexvariable++
        if (chaine.charAt(indexvariable) === '{') {
          indexaccolade++
        }
        if (chaine.charAt(indexvariable) === '}') {
          indexaccolade--
        }
      }
      return indexvariable
    }

    function necessiteFois (chaine, index) {
      // fonction qui renvoit "*" si à l’index courant on a un chiffre ou i (renvoit une chaine vide sinon)
      // fonction utile à la conversion d’une chaine mathquill (pour laquelle e^{2i\pi} est compréhensible) vers xcas (qui ne comprend pas e^(2i(PI) alors que e^i(PI) oui...)
      let texte = ''
      if (chaine.charAt(index) === '0' || chaine.charAt(index) === '1' || chaine.charAt(index) === '2' || chaine.charAt(index) === '3' || chaine.charAt(index) === '4' || chaine.charAt(index) === '5' || chaine.charAt(index) === '6' || chaine.charAt(index) === '7' || chaine.charAt(index) === '8' || chaine.charAt(index) === '9' || chaine.charAt(index) === 'i') {
        texte = '*'
      }
      return texte
    }

    function estParentheseFermante (chaine, index) {
      return (chaine.charAt(index) === ')')
    }

    function estParentheseOuvrante (chaine, index) {
      return (chaine.charAt(index) === '(')
    }

    let chaineMtg32 = chaine
    let pos
    // je vire d’abord les espaces :
    while (chaineMtg32.indexOf(':') !== -1) {
      pos = chaineMtg32.indexOf(':')
      chaineMtg32 = chaineMtg32.substring(0, pos - 1) + chaineMtg32.substring(pos + 1)
    }
    // je vire un éventuel signe multiplié
    while (chaineMtg32.indexOf('\\cdot') !== -1) {
      pos = chaineMtg32.indexOf('\\cdot')
      chaineMtg32 = chaineMtg32.substring(0, pos) + '*' + chaineMtg32.substring(pos + 5)
    }
    // je vire un espace inutile parfois présent (cad qu’on trouve dans le code mathquill \cdot  avec ou sans espace juste après
    while (chaineMtg32.indexOf(' ') !== -1) {
      pos = chaineMtg32.indexOf(' ')
      chaineMtg32 = chaineMtg32.substring(0, pos) + chaineMtg32.substring(pos + 1)
    }

    // je transforme la virgule (possible avec Mathquill) en point :
    while (chaineMtg32.indexOf(',') !== -1) {
      pos = chaineMtg32.indexOf(',')
      chaineMtg32 = chaineMtg32.substring(0, pos) + '.' + chaineMtg32.substring(pos + 1)
    }

    // si le mot clé \frac est présent, on l’enlève et on convertit \frac{A}{B} en A/B
    // CODE QUI SUIT PEUT ETRE MIS DANS UNE FONCTION RECURSIVE (en remplaçant while par if) : si tous les if ne donnent rien, cette fonction renvoit le string passé en paramètre
    let pos2, pos3, mult, mult2, i
    while (chaineMtg32.indexOf('\\frac') !== -1) {
      // il y a une fraction
      pos = chaineMtg32.indexOf('\\frac')
      // on vire le frac, pour 2+\frac{3}{4}, il reste ensuite 2+{3}{4}
      chaineMtg32 = chaineMtg32.substring(0, pos) + chaineMtg32.substring(pos + 5)
      // je detecte la fin du numérateur pour extraire A :
      pos2 = accoladeFermante(chaineMtg32, pos)
      // AVEC LA FONCTION RECURSIVE : var chaineMtg32A=recur(chaineMtg32.substring(pos+1,pos2))
      const chaineMtg32A = chaineMtg32.substring(pos + 1, pos2)
      // pareil pour B :
      pos3 = accoladeFermante(chaineMtg32, pos2 + 1)
      // AVEC LA FONCTION RECURSIVE : var chaineMtg32B=recur(chaineMtg32.substring(pos2+1,pos3))
      const chaineMtg32B = chaineMtg32.substring(pos2 + 2, pos3)
      chaineMtg32 = chaineMtg32.substring(0, pos) + '(' + chaineMtg32A + ')/(' + chaineMtg32B + ')' + chaineMtg32.substring(pos3 + 1)
    }
    while (chaineMtg32.includes('\\sqrt')) {
      // il y a une racine, il faut remplacer \sqrt{A} par sqrt(A)
      // on teste aussi s’il faut un * avant car xcas ne comprend pas 2sqrt(3)
      pos = chaineMtg32.indexOf('\\sqrt')
      pos2 = pos + 4
      if (necessiteFois(chaineMtg32, pos - 1) === '*' || estParentheseFermante(chaineMtg32, pos - 1)) {
        mult = '*'
        pos2++
      } else {
        mult = ''
      }
      // je vire le \ en ajoutant le signe * si besoin
      chaineMtg32 = chaineMtg32.substring(0, pos) + mult + chaineMtg32.substring(pos + 1)
      // je remplace { par (
      chaineMtg32 = chaineMtg32.substring(0, pos2) + '(' + chaineMtg32.substring(pos2 + 1)
      // je detecte la fin de ce qu’il y a dans la racine'
      pos3 = accoladeFermante(chaineMtg32, pos2)
      chaineMtg32 = chaineMtg32.substring(0, pos3) + ')' + chaineMtg32.substring(pos3 + 1)
    }

    // A VIRER AUSSI : \left et \right pour les parenthèses
    while (chaineMtg32.includes('\\left')) {
      pos = chaineMtg32.indexOf('\\left')
      if (necessiteFois(chaineMtg32, pos - 1) === '*' || estParentheseFermante(chaineMtg32, pos - 1)) {
        mult = '*'
      } else {
        mult = ''
      }
      chaineMtg32 = chaineMtg32.substring(0, pos) + mult + chaineMtg32.substring(pos + 5)
    }
    while (chaineMtg32.includes('\\right')) {
      pos = chaineMtg32.indexOf('\\right')
      chaineMtg32 = chaineMtg32.substring(0, pos) + chaineMtg32.substring(pos + 6)
    }

    // un soucis ici : si un seul terme dans l’exponentielle, MQ ne met pas d’accolade donc il faut refaire le test avec 'e^' pour savoir si necessite fois...
    while (chaineMtg32.includes('e^{')) {
      // il y a un exponentielle, il faut remplacer e^{A} par e^(A) + test si signe * necessaire
      pos = chaineMtg32.indexOf('e^{')
      pos2 = accoladeFermante(chaineMtg32, pos + 2)
      if (necessiteFois(chaineMtg32, pos - 1) === '*' || estParentheseFermante(chaineMtg32, pos - 1)) {
        mult = '*'
      } else {
        mult = ''
      }
      if (necessiteFois(chaineMtg32, pos2 + 1) === '*' || estParentheseOuvrante(chaineMtg32, pos2 + 1)) {
        mult2 = '*'
      } else {
        mult2 = ''
      }
      const valPuissance = (chaineMtg32.substring(pos + 3, pos2) !== '') ? chaineMtg32.substring(pos + 3, pos2) : '1'
      chaineMtg32 = chaineMtg32.substring(0, pos) + mult + chaineMtg32.substring(pos, pos + 1) + 'xp(' + valPuissance + ')' + mult2 + chaineMtg32.substring(pos2 + 1)
      // console.log('chaine xcas exp=' + chaineMtg32)
    }
    // je traite le cas particulier :
    for (i = 0; i < chaineMtg32.length; i++) {
      if (chaineMtg32.charAt(i) === 'e' && chaineMtg32.charAt(i + 1) === '^') {
        if (necessiteFois(chaineMtg32, i - 1) === '*' || estParentheseFermante(chaineMtg32, i - 1)) {
          mult = '*'
        } else {
          mult = ''
        }
        chaineMtg32 = chaineMtg32.substring(0, i) + mult + 'exp(' + chaineMtg32.charAt(i + 2) + ')' + chaineMtg32.substring(i + 3)
        // console.log('on est là')
        i = i + 3
        // console.log('le caractère suivant:' + chaineMtg32.charAt(i))
      }
    }
    // j’autorise la saisie en direct de exp().. du coup il faut tester s’il faut rajouter un signe fois avant :
    for (i = 0; i < chaineMtg32.length; i++) {
      if (chaineMtg32.substring(i, i + 3) === 'exp' && (necessiteFois(chaineMtg32, i - 1) === '*' || estParentheseFermante(chaineMtg32, i - 1))) {
        chaineMtg32 = chaineMtg32.substring(0, i) + '*' + chaineMtg32.substring(i)
        i++
      }
    }
    // maintenant il ne doit plus rester de e^{ ou de 'e' sans 'xp' derrière... sauf si l’élève a rentré le nombre e ! (à convertir en exp(1) pour mtg32
    for (i = 0; i < chaineMtg32.length; i++) {
      if (chaineMtg32.charAt(i) === 'e' && chaineMtg32.charAt(i + 1) !== 'x') {
        if (necessiteFois(chaineMtg32, i - 1) === '*' || estParentheseFermante(chaineMtg32, i - 1)) {
          mult = '*'
        } else {
          mult = ''
        }
        chaineMtg32 = chaineMtg32.substring(0, i) + mult + 'exp(1)' + chaineMtg32.substring(i + 1)
        // console.log('on est là !!!!!')
        i = i + 3
        // console.log('le caractère suivant:' + chaineMtg32.charAt(i))
      }
    }

    while (chaineMtg32.includes('\\pi')) {
      pos = chaineMtg32.indexOf('\\pi')
      // je remplace le \ par (
      if (necessiteFois(chaineMtg32, pos - 1) === '*' || estParentheseFermante(chaineMtg32, pos - 1)) {
        mult = '*'
      } else {
        mult = ''
      }
      chaineMtg32 = chaineMtg32.substring(0, pos) + mult + '(PI)' + chaineMtg32.substring(pos + 3)
    }

    // pour gérer un pb  : (2+racine(3))i n’est pas compris par xcas, j’ajoute un fois...
    for (i = 0; i <= chaineMtg32.length; i++) {
      if (chaineMtg32.charAt(i) === 'i' && (necessiteFois(chaineMtg32, i - 1) === '*' || estParentheseFermante(chaineMtg32, i - 1))) {
        chaineMtg32 = chaineMtg32.substring(0, i) + '*' + 'i' + chaineMtg32.substring(i + 1)
        i++
      }
    }

    // même genre de pb  : i3 n’est pas compris par xcas, j’ajoute un fois...(mais pas à "i)" d’où la modif de code pour estParentheseFermante
    for (i = 0; i <= chaineMtg32.length; i++) {
      if (chaineMtg32.charAt(i) === 'i' && necessiteFois(chaineMtg32, i + 1) === '*') {
        chaineMtg32 = chaineMtg32.substring(0, i) + 'i' + '*' + chaineMtg32.substring(i + 1)
        i++
      }
    }
    while (chaineMtg32.includes('{')) {
      // reste à remplacer d’éventuelle accolades (puissances) par des parenthèses
      pos = chaineMtg32.indexOf('{')
      pos2 = pos + 1
      chaineMtg32 = chaineMtg32.substring(0, pos) + '(' + chaineMtg32.substring(pos + 1)
      // je detecte la fin de ce qu’il y a dans l’accolade
      pos3 = accoladeFermante(chaineMtg32, pos2)
      chaineMtg32 = chaineMtg32.substring(0, pos3) + ')' + chaineMtg32.substring(pos3 + 1)
    }
    return chaineMtg32
  }

  function egaliteFormelleMtg32 (expr1, expr2) {
    // TODO, remplacer par une méthode de mtgAppLecteur, y’a bien createList mais il veut une figure en argument
    // et ensuite les méthodes sur liste sont celles de CListeObjets, y’a pas de ajouteCalcul(…)
    const liste = stor.listePourCalc
    liste.giveFormula2('c', liste.traiteSignesMult(expr1))
    // var calcul_expr2 = liste.ajouteCalcul('d', liste.traiteSignesMult(expr2))
    liste.giveFormula2('d', liste.traiteSignesMult(expr2))
    // liste.positionne()
    liste.calculateNG()
    const ecritureCorrecte = stor.listePourCalc.verifieSyntaxe(expr2)
    // liste.positionne()
    if (!ecritureCorrecte) {
      return false
    } else {
      // console.log('=====expr1======' + calcul_expr1.rendValeur() + ' et expr2=' + calcul_expr2.rendValeur())
      // var max = Math.max(Math.abs(calcul_expr1.rendValeur()), Math.abs(calcul_expr2.rendValeur()))
      const max = Math.max(Math.abs(liste.valueOf('c')), Math.abs(liste.valueOf('d')))
      // console.log('max=' + max)
      // var calc1 = liste.ajouteCalcul('a', '(' + liste.traiteSignesMult(expr1) + '-(' + liste.traiteSignesMult(expr2) + '))')
      liste.giveFormula2('a', '(' + liste.traiteSignesMult(expr1) + '-(' + liste.traiteSignesMult(expr2) + '))')
      // var calc2 = liste.ajouteCalcul("b", "a-1");
      // liste.positionne()
      liste.calculateNG()
      // if (calc1.rendValeur() === 0) {
      if (liste.valueOf('a') === 0) {
        return 0
      } else {
        // var calc2 = liste.ajouteCalcul('b', 'a/' + max)
        liste.giveFormula2('b', 'a/' + max)
        // liste.positionne()
        liste.calculateNG()
        return liste.valueOf('b')
      }
    }
  }

  function analyseFormelleMtg32 (reponseEleve) {
    const retour = {}
    const valeurDifference = Math.abs(egaliteFormelleMtg32(stor.solution[1], reponseEleve))
    retour.bon = (valeurDifference < Math.pow(10, -12))
    // A FAIRE : adapter suivant la formule
    let nbE = 0
    for (let i = 0; i <= reponseEleve.length; i++) {
      if (reponseEleve.charAt(i) === 'e') nbE++
    }
    retour.simplifie = (nbE <= 2)
    // console.log('reponse finale exacte=' + retour.bon + ' et retour.simplifie=' + retour.simplifie)
    return retour
  }

  function tirageCalculUPrimeRacineU (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv, borneInf, borneSup) {
    function simplifieRac (n) {
      // fonction qui renvoie un tableau de deux valeurs, la première correspond au coeff devant la racine, la seconde est le nb sous le radical
      const facteur = []
      facteur[1] = 1
      facteur[2] = n
      for (let i = 2; i <= Math.floor(n / 2); i++) {
        const k = i * i
        while (true) {
          if (facteur[2] % k === 0) {
            facteur[1] *= k
            facteur[2] /= k
          } else {
            break
          }
        }
      }
      facteur[1] = Math.sqrt(facteur[1])
      return facteur
    }

    function definieCorrection () {
      let entier1, racine1, entier2, racine2, leFact, facto, fact1, fact2, termine
      if (coeffMultiplicateur === 1) {
        stor.etapes[0] = '[2\\sqrt{' + u1X + '}]_{' + borneInf + '}^{' + borneSup + '}='
        stor.etapes[0] += '2\\times' + chaineTemp1 + '-2\\times' + chaineTemp2
        // simplification éventuelle...je récupère les décompositions des racines
        entier1 = simplifieRac(Math.abs(exp1))[1]
        racine1 = simplifieRac(Math.abs(exp1))[2]
        entier2 = simplifieRac(Math.abs(exp2))[1]
        racine2 = simplifieRac(Math.abs(exp2))[2]
        if (racine1 === 1) {
          // carré parfait pour le premier
          if (racine2 === 1) {
            // et carré parfait pour le second !
            stor.etapes[0] += '=' + (2 * (entier1 - entier2))
          } else {
            // juste carré parfait pour le premier
            stor.etapes[0] += '=' + 2 * entier1 + '-' + (2 * entier2) + '\\sqrt{' + racine2 + '}'
          }
        } else {
          // le premier n’est pas un carré parfait :
          stor.etapes[0] += '=' + (2 * entier1) + '\\sqrt{' + racine1 + '}'
          if (racine2 === 1) {
            // le second est un carré parfait
            stor.etapes[0] += '-' + (2 * entier2)
          } else {
            // pas un carré parfait, on pourra au moins facotriser par 2
            stor.etapes[0] += '-' + (2 * entier2) + '\\sqrt{' + racine2 + '}'
            facto = j3pPGCD(2 * entier1, 2 * entier2)
            fact1 = ((2 * entier1) / facto)
            if (fact1 === 1) {
              fact1 = ''
            }
            fact2 = ((2 * entier2) / facto)
            if (fact2 === 1) {
              fact2 = ''
            }
            stor.etapes[0] += '=' + facto + '(' + fact1 + '\\sqrt{' + racine1 + '}-' + fact2 + '\\sqrt{' + racine2 + '})'
            // on ajoute un test si c la même racine...
            termine = false
            if (racine1 === racine2) {
              if (fact1 === '') {
                if (fact2 === '') {
                  // la différence fera 0
                  stor.etapes[0] += '=0'
                  termine = true
                } else {
                  leFact = 1 - fact2
                }
              } else {
                if (fact2 === '') {
                  leFact = fact1 - 1
                } else {
                  leFact = fact1 - fact2
                }
              }
              if (!termine) {
                stor.etapes[0] += '=' + facto + '\\times' + (leFact) + '\\sqrt{' + racine1 + '}=' + (facto * leFact) + '\\sqrt{' + racine1 + '}'
              }
            }
          }
        }

        stor.phrase[0] = textes.correction3_1
        // la solution attendue :
        stor.solution[1] = '2*(sqrt(' + exp1 + ')-sqrt(' + exp2 + '))'
      } else {
        if (multOuDiv === 'div') {
          stor.etapes[0] = '[\\frac{1}{' + coeffMultiplicateur + '}\\times2\\sqrt{' + u1X + '}]_{' + borneInf + '}^{' + borneSup + '}='
          stor.etapes[0] += '' + simplifieFrac(2, coeffMultiplicateur) + '\\times' + chaineTemp1 + '-' + simplifieFrac(2, coeffMultiplicateur) + '\\times' + chaineTemp2
          // simplification éventuelle...je récupère les décompositions des racines
          entier1 = simplifieRac(Math.abs(exp1))[1]
          racine1 = simplifieRac(Math.abs(exp1))[2]
          entier2 = simplifieRac(Math.abs(exp2))[1]
          racine2 = simplifieRac(Math.abs(exp2))[2]
          // A modifier : 3ème égalité à enlever si entier1==1 et entier2==1
          if (racine1 === 1) {
            // carré parfait pour le premier
            if (racine2 === 1) {
              // et carré parfait pour le second !
              stor.etapes[0] += '=' + (simplifieFrac(2 * (entier1 - entier2), coeffMultiplicateur))
            } else {
              // juste carré parfait pour le premier
              let facteur = simplifieFrac(2 * entier2, coeffMultiplicateur)
              if (facteur === '1') {
                facteur = ''
              }
              stor.etapes[0] += '=' + simplifieFrac(2 * entier1, coeffMultiplicateur) + '-' + facteur + '\\sqrt{' + racine2 + '}'
            }
          } else {
            // le premier n’est pas un carré parfait :
            if (racine2 === 1) {
              // le second est un carré parfait
              if (simplifieFrac(2 * entier1, coeffMultiplicateur) === '1') {
                stor.etapes[0] += '=\\sqrt{' + racine1 + '}'
              } else {
                stor.etapes[0] += '=' + simplifieFrac(2 * entier1, coeffMultiplicateur) + '\\sqrt{' + racine1 + '}'
              }
              stor.etapes[0] += '-' + simplifieFrac(2 * entier2, coeffMultiplicateur) + ''
            } else {
              // pas un carré parfait, on pourra au moins factoriser par 2
              if (!(entier1 === 1 && entier2 === 1)) {
                stor.etapes[0] += '=' + simplifieFrac(2 * entier1, coeffMultiplicateur) + '\\sqrt{' + racine1 + '}'
                stor.etapes[0] += '-' + simplifieFrac(2 * entier2, coeffMultiplicateur) + '\\sqrt{' + racine2 + '}'
              }
              facto = j3pPGCD(2 * entier1, 2 * entier2)
              fact1 = ((2 * entier1) / facto)
              if (fact1 === 1) {
                fact1 = ''
              }
              fact2 = ((2 * entier2) / facto)
              if (fact2 === 1) {
                fact2 = ''
              }
              if (simplifieFrac(facto, coeffMultiplicateur) === '1') {
                stor.etapes[0] += '=' + fact1 + '\\sqrt{' + racine1 + '}-' + fact2 + '\\sqrt{' + racine2 + '}'
              } else {
                stor.etapes[0] += '=' + simplifieFrac(facto, coeffMultiplicateur) + '(' + fact1 + '\\sqrt{' + racine1 + '}-' + fact2 + '\\sqrt{' + racine2 + '})'
              }
              // on ajoute un test si c la même racine...
              termine = false
              if (racine1 === racine2) {
                if (fact1 === '') {
                  if (fact2 === '') {
                    // la différence fera 0
                    stor.etapes[0] += '=0'
                    termine = true
                  } else {
                    leFact = 1 - fact2
                  }
                } else {
                  if (fact2 === '') {
                    leFact = fact1 - 1
                  } else {
                    leFact = fact1 - fact2
                  }
                }
                if (!termine) {
                  if (simplifieFrac(facto * leFact, coeffMultiplicateur) === '1') {
                    stor.etapes[0] += '=' + simplifieFrac(facto, coeffMultiplicateur) + '\\times' + (leFact) + '\\sqrt{' + racine1 + '}=\\sqrt{' + racine1 + '}'
                  } else {
                    stor.etapes[0] += '=' + simplifieFrac(facto, coeffMultiplicateur) + '\\times' + (leFact) + '\\sqrt{' + racine1 + '}=' + (simplifieFrac(facto * leFact, coeffMultiplicateur)) + '\\sqrt{' + racine1 + '}'
                  }
                }
              }
            }
          }

          stor.phrase[0] = textes.correction3_2
          stor.phrase[1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = '2*(sqrt(' + exp1 + ')-sqrt(' + exp2 + '))/' + coeffMultiplicateur
        } else {
          stor.etapes[0] = '[' + coeffMultiplicateur + '\\times 2\\sqrt{' + u1X + '}]_{' + borneInf + '}^{' + borneSup + '}='
          stor.etapes[0] += (2 * coeffMultiplicateur) + '\\times' + chaineTemp1 + '-' + (2 * coeffMultiplicateur) + '\\times' + chaineTemp2
          // simplification éventuelle...je récupère les décompositions des racines
          entier1 = simplifieRac(Math.abs(exp1))[1]
          racine1 = simplifieRac(Math.abs(exp1))[2]
          entier2 = simplifieRac(Math.abs(exp2))[1]
          racine2 = simplifieRac(Math.abs(exp2))[2]
          // console.log('entier1=' + entier1 + ' et racine1=' + racine1)
          // console.log('entier2=' + entier2 + ' et racine2=' + racine2)
          if (racine1 === 1) {
            // carré parfait pour le premier
            if (racine2 === 1) {
              // et carré parfait pour le second !
              stor.etapes[0] += '=' + (2 * coeffMultiplicateur * (entier1 - entier2))
            } else {
              // juste carré parfait pour le premier
              stor.etapes[0] += '=' + 2 * coeffMultiplicateur * entier1 + '-' + (2 * coeffMultiplicateur * entier2) + '\\sqrt{' + racine2 + '}'
            }
          } else {
            // le premier n’est pas un carré parfait :
            if (racine2 === 1) {
              // le second est un carré parfait
              stor.etapes[0] += '=' + (2 * coeffMultiplicateur * entier1) + '\\sqrt{' + racine1 + '}'
              stor.etapes[0] += '-' + (2 * coeffMultiplicateur * entier2)
            } else {
              // pas un carré parfait, on pourra au moins facotriser par 2
              if (!(entier1 === 1 && entier2 === 1)) {
                stor.etapes[0] += '=' + (2 * coeffMultiplicateur * entier1) + '\\sqrt{' + racine1 + '}'
                stor.etapes[0] += '-' + (2 * coeffMultiplicateur * entier2) + '\\sqrt{' + racine2 + '}'
              }
              facto = j3pPGCD(2 * coeffMultiplicateur * entier1, 2 * coeffMultiplicateur * entier2)
              fact1 = ((2 * coeffMultiplicateur * entier1) / facto)
              if (fact1 === 1) {
                fact1 = ''
              }
              fact2 = ((2 * coeffMultiplicateur * entier2) / facto)
              if (fact2 === 1) {
                fact2 = ''
              }
              stor.etapes[0] += '=' + facto + '(' + fact1 + '\\sqrt{' + racine1 + '}-' + fact2 + '\\sqrt{' + racine2 + '})'
            }
          }
          stor.phrase[0] = textes.correction3_3
          stor.phrase[1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = coeffMultiplicateur + '*2*(sqrt(' + exp1 + ')-sqrt(' + exp2 + '))'
        }
      }
    }

    // console.log('uprime/racine(u) ' + 'tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv)
    let a, b, c, u1X, u1Prime, chaineTemp1, chaineTemp2, operation1, exp1, exp2
    if (tabCoefsU.length === 2) {
      // console.log('degré 1 !! avec a<>1')
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      u1X = j3pGetLatexMonome(1, 1, a) + j3pGetLatexMonome(2, 0, b)
      u1Prime = tabCoefsUprime[0]
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\int_{' + borneInf + '}^{' + borneSup + '} \\frac{' + u1Prime + '}{\\sqrt{' + u1X + '}} \\mathrm dx'
      exp1 = a * borneSup + b
      exp2 = a * borneInf + b
      // les solutions finales
      chaineTemp1 = '\\sqrt{' + exp1 + '}'
      if (Math.round(Math.sqrt(exp1)) === Math.sqrt(exp1)) {
        // si entier
        chaineTemp1 = Math.sqrt(exp1)
      }
      chaineTemp2 = '\\sqrt{' + exp2 + '}'
      if (Math.round(Math.sqrt(exp2)) === Math.sqrt(exp2)) {
        // si entier
        chaineTemp2 = Math.sqrt(exp2)
      }
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      definieCorrection()
      stor.operation1 = operation1
    } else {
      // console.log('degré2 !! avec a<>1')
      c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      u1X = j3pGetLatexMonome(1, 2, a) + j3pGetLatexMonome(2, 1, b) + j3pGetLatexMonome(3, 0, c)
      u1Prime = j3pGetLatexMonome(1, 1, tabCoefsUprime[1]) + j3pGetLatexMonome(2, 0, tabCoefsUprime[0])
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\int_{' + borneInf + '}^{' + borneSup + '} \\frac{' + u1Prime + '}{\\sqrt{' + u1X + '}} \\mathrm dx'
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      stor.operation1 = operation1
      exp1 = a * borneSup * borneSup + b * borneSup + c
      exp2 = a * borneInf * borneInf + b * borneInf + c
      // les solutions finales
      chaineTemp1 = '\\sqrt{' + exp1 + '}'
      if (Math.round(Math.sqrt(exp1)) === Math.sqrt(exp1)) {
        // si entier
        chaineTemp1 = Math.sqrt(exp1)
      }
      chaineTemp2 = '\\sqrt{' + exp2 + '}'
      if (Math.round(Math.sqrt(exp2)) === Math.sqrt(exp2)) {
        // si entier
        chaineTemp2 = Math.sqrt(exp2)
      }
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      definieCorrection()
    }
    if (coeffMultiplicateur === 1) {
      stor.solution[2] = '2*sqrt(' + u1X + ')'
    } else {
      if (multOuDiv === 'div') {
        stor.solution[2] = '2*sqrt(' + u1X + ')/' + coeffMultiplicateur
      } else {
        stor.solution[2] = coeffMultiplicateur + '*2*sqrt(' + u1X + ')'
      }
    }
  }

  function tirageCalculUPrimeUN (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv, borneInf, borneSup, n) {
    function gereParentheses (nb) {
      if (nb < 0) return '(' + nb + ')'
      else return nb
    }

    function definieCorrection () {
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      if (coeffMultiplicateur === 1) {
        stor.etapes[0] = '[ \\frac{(' + u1X + ')^' + (n + 1) + '}{' + (n + 1) + '}]_{' + borneInf + '}^{' + borneSup + '}='
        stor.etapes[0] += chaineTemp1 + '-' + chaineTemp2
        stor.phrase[0] = textes.correction4_1
        // la solution attendue :
        stor.solution[1] = '((' + exp1 + ')^(' + (n + 1) + ')-(' + exp2 + ')^(' + (n + 1) + '))/(' + (n + 1) + ')'
      } else {
        if (multOuDiv === 'div') {
          stor.etapes[0] = '[\\frac{1}{' + coeffMultiplicateur + '}\\times \\frac{(' + u1X + ')^' + (n + 1) + '}{' + (n + 1) + '}]_{' + borneInf + '}^{' + borneSup + '}='
          stor.etapes[0] += '\\frac{1}{' + coeffMultiplicateur + '}(' + chaineTemp1 + '-' + chaineTemp2 + ')'
          if (exp1 === exp2) {
            stor.etapes[0] += '=0'
          } else {
            stor.etapes[0] += '=\\frac{1}{' + ((n + 1) * coeffMultiplicateur) + '}(' + gereParentheses(exp1) + '^' + (n + 1) + '-' + gereParentheses(exp2) + '^' + (n + 1) + ')'
            stor.etapes[0] += '=\\frac{1}{' + ((n + 1) * coeffMultiplicateur) + '}(' + (Math.pow(exp1, n + 1) - Math.pow(exp2, n + 1)) + ')'
            // A voir si pertinent...
            if (j3pPGCD((n + 1) * coeffMultiplicateur, ((exp1) ^ (n + 1) - (exp2) ^ (n + 1)), { negativesAllowed: true }) !== 1) {
              stor.etapes[0] += '=' + simplifieFrac((Math.pow(exp1, n + 1) - Math.pow(exp2, n + 1)), ((n + 1) * coeffMultiplicateur))
            } else {
              stor.etapes[0] += '=\\frac{' + (Math.pow(exp1, n + 1) - Math.pow(exp2, n + 1)) + '}{' + ((n + 1) * coeffMultiplicateur) + '}'
            }
            if (Math.pow(exp1, n + 1) === Math.pow(exp2, n + 1)) stor.etapes[0] += '=0'
          }
          stor.phrase[0] = textes.correction4_2
          stor.phrase[1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = '((' + exp1 + ')^(' + (n + 1) + ')-(' + exp2 + ')^(' + (n + 1) + '))/(' + (coeffMultiplicateur * (n + 1)) + ')'
        } else {
          stor.etapes[0] = '[' + coeffMultiplicateur + '\\times \\frac{(' + u1X + ')^' + (n + 1) + '}{' + (n + 1) + '}]_{' + borneInf + '}^{' + borneSup + '}='
          stor.etapes[0] += coeffMultiplicateur + '(' + chaineTemp1 + '-' + chaineTemp2 + ')'
          if (exp1 === exp2) {
            stor.etapes[0] += '=0'
          } else {
            if (n + 1 === coeffMultiplicateur) {
              stor.etapes[0] += '=' + gereParentheses(exp1) + '^' + (n + 1) + '-' + gereParentheses(exp2) + '^' + (n + 1)
              if (exp1 === 0) {
                stor.etapes[0] += '=-' + gereParentheses(exp2) + '^' + (n + 1)
              }
              if (exp2 === 0) {
                stor.etapes[0] += '=' + gereParentheses(exp1) + '^' + (n + 1)
              }
            } else {
              stor.etapes[0] += '=' + simplifieFrac(coeffMultiplicateur, n + 1) + '(' + gereParentheses(exp1) + '^' + (n + 1) + '-' + gereParentheses(exp2) + '^' + (n + 1) + ')'
              if (exp1 === 0) {
                stor.etapes[0] += '=-' + gereParentheses(exp2) + '^' + (n + 1) + ')'
              }
              if (exp2 === 0) {
                stor.etapes[0] += '=' + simplifieFrac(coeffMultiplicateur, n + 1) + '(' + gereParentheses(exp1) + '^' + (n + 1)
              }
            }
            if (Math.pow(exp1, n + 1) === Math.pow(exp2, n + 1)) stor.etapes[0] += '=0'
          }
          stor.phrase[0] = textes.correction4_3
          stor.phrase[1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = coeffMultiplicateur + '*((' + exp1 + ')^(' + (n + 1) + ')-(' + exp2 + ')^(' + (n + 1) + '))/(' + (n + 1) + ')'
        }
      }
    }

    // console.log('tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv + ' et n=' + n)
    let a, b, c, u1X, u1Prime, chaineTemp1, chaineTemp2, operation1, exp1, exp2
    if (tabCoefsU.length === 2) {
      // console.log('degré 1 !! avec a<>1')
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      u1X = j3pGetLatexMonome(1, 1, a) + j3pGetLatexMonome(2, 0, b)
      u1Prime = tabCoefsUprime[0]
      // un autre cas particulier :
      if (tabCoefsUprime[0] === 1) {
        u1Prime = ''
      } else if (tabCoefsUprime[0] === -1) {
        u1Prime = '-'
      }
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\int_{' + borneInf + '}^{' + borneSup + '} ' + u1Prime + '(' + u1X + ')^' + n + ' \\mathrm dx'
      exp1 = a * borneSup + b
      exp2 = a * borneInf + b
    } else {
      // console.log('degré2 !! avec a<>1')
      c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      u1X = j3pGetLatexMonome(1, 2, a) + j3pGetLatexMonome(2, 1, b) + j3pGetLatexMonome(3, 0, c)
      u1Prime = j3pGetLatexMonome(1, 1, tabCoefsUprime[1]) + j3pGetLatexMonome(2, 0, tabCoefsUprime[0])
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\int_{' + borneInf + '}^{' + borneSup + '} (' + u1Prime + ')\\times (' + u1X + ')^' + n + ' \\mathrm dx'
      exp1 = a * borneSup * borneSup + b * borneSup + c
      exp2 = a * borneInf * borneInf + b * borneInf + c
    }
    // les solutions finales
    if (exp1 < 0) {
      chaineTemp1 = '\\frac{(' + exp1 + ')^' + (n + 1) + '}{' + (n + 1) + '}'
    } else {
      chaineTemp1 = '\\frac{' + exp1 + '^' + (n + 1) + '}{' + (n + 1) + '}'
    }
    if (exp2 < 0) {
      chaineTemp2 = '\\frac{(' + exp2 + ')^' + (n + 1) + '}{' + (n + 1) + '}'
    } else {
      chaineTemp2 = '\\frac{' + exp2 + '^' + (n + 1) + '}{' + (n + 1) + '}'
    }
    definieCorrection()
    // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
    // j3p.stockage[1] = operation1
    stor.operation1 = operation1
    if (coeffMultiplicateur === 1) {
      stor.solution[2] = '(' + u1X + ')^' + (n + 1) + '/' + (n + 1)
    } else {
      if (multOuDiv === 'div') {
        stor.solution[2] = '((' + u1X + ')^' + (n + 1) + ')/' + ((n + 1) * coeffMultiplicateur)
      } else {
        stor.solution[2] = coeffMultiplicateur + '*(' + u1X + ')^' + (n + 1) + '/' + (n + 1)
      }
    }
  }

  function tirageCalculUPrimeU (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv, borneInf, borneSup) {
    function definieCorrection () {
      if (coeffMultiplicateur === 1) {
        stor.etapes[0] = '[ \\mathrm{ln}(' + u1X + ')]_{' + borneInf + '}^{' + borneSup + '}='
        if (exp2 !== 1) {
          stor.etapes[0] += chaineTemp1 + '-' + chaineTemp2
        } else {
          if (exp1 === 1) {
            stor.etapes[0] += 0
          } else {
            stor.etapes[0] += '$' + chaineTemp1 + '$'
          }
        }
        // simplification éventuelle... pas si exp1==1
        if (exp1 !== 1 && exp2 !== 1) {
          stor.etapes[0] += '=\\mathrm{ln}\\left(\\frac{' + exp1 + '}{' + exp2 + '}\\right)'
          if (j3pPGCD(exp1, exp2) !== 1) {
            stor.etapes[0] += '=\\mathrm{ln}\\left(' + simplifieFrac(exp1, exp2) + '\\right)'
          }
        }
        stor.phrase[0] = textes.correction2_1
        // la solution attendue :
        stor.solution[1] = 'ln(' + exp1 + ')-ln(' + exp2 + ')'
      } else {
        if (multOuDiv === 'div') {
          stor.etapes[0] = '[ \\frac{\\mathrm{ln}(' + u1X + ')}{' + coeffMultiplicateur + '}]_{' + borneInf + '}^{' + borneSup + '}='
          if (exp2 !== 1) {
            stor.etapes[0] += '\\frac{' + chaineTemp1 + '-' + chaineTemp2 + '}{' + coeffMultiplicateur + '}'
          } else {
            if (exp1 === 1) {
              stor.etapes[0] += 0
            } else {
              stor.etapes[0] += '\\frac{' + chaineTemp1 + '}{' + coeffMultiplicateur + '}'
            }
          }
          // simplification éventuelle... pas si exp1==1
          if (exp1 !== 1 && exp2 !== 1) {
            stor.etapes[0] += '=\\frac{1}{' + coeffMultiplicateur + '} \\mathrm{ln}\\left(\\frac{' + exp1 + '}{' + exp2 + '}\\right)'
            if (j3pPGCD(exp1, exp2) !== 1) {
              stor.etapes[0] += '=\\frac{1}{' + coeffMultiplicateur + '} \\mathrm{ln}\\left(' + simplifieFrac(exp1, exp2) + '\\right)'
              if (simplifieFrac(exp1, exp2) === 1) {
                stor.etapes[0] += '=0'
              }
            }
          }
          //
          stor.phrase[0] = textes.correction2_2
          stor.phrase[1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = '(ln(' + exp1 + ')-ln(' + exp2 + '))/' + coeffMultiplicateur
        } else {
          stor.etapes[0] = '[' + coeffMultiplicateur + '\\times \\mathrm{ln}(' + u1X + ')]_{' + borneInf + '}^{' + borneSup + '}='
          if (exp2 !== 1) {
            if (exp1 !== 1) {
              stor.etapes[0] += coeffMultiplicateur + '(' + chaineTemp1 + '-' + chaineTemp2 + ')'
            } else {
              stor.etapes[0] += '-' + coeffMultiplicateur + chaineTemp2
            }
          } else {
            if (exp1 === 1) {
              stor.etapes[0] += 0
            } else {
              stor.etapes[0] += coeffMultiplicateur + chaineTemp1
            }
          }
          // simplification éventuelle... pas si exp1==1
          if (exp1 !== 1 && exp2 !== 1) {
            stor.etapes[0] += '=' + coeffMultiplicateur + '\\mathrm{ln}(\\frac{' + exp1 + '}{' + exp2 + '})'
            if (j3pPGCD(exp1, exp2) !== 1) {
              stor.etapes[0] += '=' + coeffMultiplicateur + '\\mathrm{ln}\\left(' + simplifieFrac(exp1, exp2) + '\\right)'
              if (simplifieFrac(exp1, exp2) === 1) {
                stor.etapes[0] += '=0'
              }
            }
          }

          stor.phrase[0] = textes.correction2_3
          stor.phrase[1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = coeffMultiplicateur + '*(ln(' + exp1 + ')-ln(' + exp2 + '))'
        }
      }
      if (exp1 === exp2) stor.etapes[0] += '=0'
    }

    // console.log('uprime/u ' + 'tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv)
    let a, b, c, u1X, u1Prime, chaineTemp1, chaineTemp2, operation1, exp1, exp2
    if (tabCoefsU.length === 2) {
      // console.log('degré 1 !! avec a<>1')
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      u1X = j3pGetLatexMonome(1, 1, a) + j3pGetLatexMonome(2, 0, b)
      u1Prime = tabCoefsUprime[0]
      // console.log('!!!!!u1Prime=' + u1Prime)
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\int_{' + borneInf + '}^{' + borneSup + '} \\frac{' + u1Prime + '}{' + u1X + '} \\mathrm dx'
      exp1 = a * borneSup + b
      exp2 = a * borneInf + b
      // les solutions finales
      chaineTemp1 = '\\mathrm{ln}(' + exp1 + ')'
      if (String(exp1) === '1') {
        chaineTemp1 = ''
      }
      if (exp1 === 'e') {
        chaineTemp1 = '1'
      }
      chaineTemp2 = '\\mathrm{ln}(' + exp2 + ')'
      if (String(exp2) === '1') {
        chaineTemp2 = ''
      }
      if (exp2 === 'e') {
        chaineTemp2 = '1'
      }
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      definieCorrection()
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      // j3p.stockage[1] = operation1
      stor.operation1 = operation1
    } else {
      // console.log('degré2 !! avec a<>1')
      c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      u1X = j3pGetLatexMonome(1, 2, a) + j3pGetLatexMonome(2, 1, b) + j3pGetLatexMonome(3, 0, c)
      u1Prime = j3pGetLatexMonome(1, 1, tabCoefsUprime[1]) + j3pGetLatexMonome(2, 0, tabCoefsUprime[0])
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\int_{' + borneInf + '}^{' + borneSup + '} \\frac{' + u1Prime + '}{' + u1X + '} \\mathrm dx'
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      // j3p.stockage[1] = operation1
      stor.operation1 = operation1
      exp1 = a * borneSup * borneSup + b * borneSup + c
      exp2 = a * borneInf * borneInf + b * borneInf + c
      // les solutions finales
      chaineTemp1 = '\\mathrm{ln}(' + exp1 + ')'
      if (String(exp1) === '1') {
        chaineTemp1 = ''
      }
      if (exp1 === 'e') {
        chaineTemp1 = '1'
      }
      chaineTemp2 = '\\mathrm{ln}(' + exp2 + ')'
      if (String(exp2) === '1') {
        chaineTemp2 = ''
      }
      if (exp2 === 'e') {
        chaineTemp2 = '1'
      }
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      definieCorrection()
    }
    if (coeffMultiplicateur === 1) {
      stor.solution[2] = 'ln(' + u1X + ')'
    } else {
      if (multOuDiv === 'div') {
        stor.solution[2] = '(ln(' + u1X + '))/' + coeffMultiplicateur
      } else {
        stor.solution[2] = coeffMultiplicateur + '*ln(' + u1X + ')'
      }
    }
  }

  function tirageCalculUPrimeExpu (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv, borneInf, borneSup) {
    // console.log('tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv)
    let a, b, c, u1X, u1Prime, chaineTemp1, chaineTemp2, operation1, exp1, exp2
    if (tabCoefsU.length === 2) {
      // console.log('degré 1 !! avec a<>1')
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      u1X = j3pGetLatexMonome(1, 1, a) + j3pGetLatexMonome(2, 0, b)
      if (tabCoefsUprime[0] === -1) {
        u1Prime = '-'
      } else {
        u1Prime = tabCoefsUprime[0]
      }
      // un autre cas particulier :
      if (tabCoefsUprime[0] === 1) {
        u1Prime = ''
      }
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\int_{' + borneInf + '}^{' + borneSup + '} ' + u1Prime + '\\mathrm{e}^{' + u1X + '} \\mathrm dx'
      exp1 = a * borneSup + b
      exp2 = a * borneInf + b
      // les solutions finales
      chaineTemp1 = '\\mathrm{e}^{' + exp1 + '}'
      if (String(exp1) === '1') chaineTemp1 = 'e'
      if (String(exp1) === '0') chaineTemp1 = '1'
      chaineTemp2 = '\\mathrm{e}^{' + exp2 + '}'
      if (String(exp2) === '1') chaineTemp2 = 'e'
      if (String(exp2) === '0') chaineTemp2 = '1'
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      if (coeffMultiplicateur === 1) {
        stor.etapes[0] = '[ \\mathrm{e}^{' + u1X + '}]_{' + borneInf + '}^{' + borneSup + '}='
        stor.etapes[0] += chaineTemp1 + '-' + chaineTemp2
        if (exp1 === exp2) stor.etapes[0] += '=0'
        stor.phrase[0] = textes.correction1_1
        // la solution attendue :
        stor.solution[1] = 'exp(' + exp1 + ')-exp(' + exp2 + ')'
      } else {
        if (multOuDiv === 'div') {
          stor.etapes[0] = '[ \\frac{\\mathrm{e}^{' + u1X + '}}{' + coeffMultiplicateur + '}]_{' + borneInf + '}^{' + borneSup + '}='
          stor.etapes[0] += '\\frac{' + chaineTemp1 + '-' + chaineTemp2 + '}{' + coeffMultiplicateur + '}'
          stor.phrase[0] = textes.correction1_2
          stor.phrase[1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = '(exp(' + exp1 + ')-exp(' + exp2 + '))/' + coeffMultiplicateur
        } else {
          stor.etapes[0] = '[' + coeffMultiplicateur + '\\times \\mathrm{e}^{' + u1X + '}]_{' + borneInf + '}^{' + borneSup + '}='
          stor.etapes[0] += coeffMultiplicateur + '(' + chaineTemp1 + '-' + chaineTemp2 + ')'
          stor.phrase[0] = textes.correction1_3
          stor.phrase[1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = coeffMultiplicateur + '*(exp(' + exp1 + ')-exp(' + exp2 + '))'
        }
      }
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      // j3p.stockage[1] = operation1
      stor.operation1 = operation1
    } else {
      // console.log('degré2 !! avec a<>1')
      c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      u1X = j3pGetLatexMonome(1, 2, a) + j3pGetLatexMonome(2, 1, b) + j3pGetLatexMonome(3, 0, c)
      u1Prime = j3pGetLatexMonome(1, 1, tabCoefsUprime[1]) + j3pGetLatexMonome(3, 0, tabCoefsUprime[0])
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\int_{' + borneInf + '}^{' + borneSup + '} (' + u1Prime + ')\\mathrm{e}^{' + u1X + '} \\mathrm dx'
      exp1 = a * borneSup * borneSup + b * borneSup + c
      exp2 = a * borneInf * borneInf + b * borneInf + c
      // les solutions finales
      chaineTemp1 = '\\mathrm{e}^{' + exp1 + '}'
      if (String(exp1) === '1') {
        chaineTemp1 = 'e'
      }
      if (String(exp1) === '0') {
        chaineTemp1 = '1'
      }
      chaineTemp2 = '\\mathrm{e}^{' + exp2 + '}'
      if (String(exp2) === '1') {
        chaineTemp2 = 'e'
      }
      if (String(exp2) === '0') {
        chaineTemp2 = '1'
      }
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      if (coeffMultiplicateur === 1) {
        stor.etapes[0] = '[ \\mathrm{e}^{' + u1X + '}]_{' + borneInf + '}^{' + borneSup + '}='
        stor.etapes[0] += chaineTemp1 + '-' + chaineTemp2
        stor.phrase[0] = textes.correction1_1
        // la solution attendue :
        stor.solution[1] = 'exp(' + exp1 + ')-exp(' + exp2 + ')'
      } else {
        if (multOuDiv === 'div') {
          stor.etapes[0] = '[ \\frac{\\mathrm{e}^{' + u1X + '}}{' + coeffMultiplicateur + '}]_{' + borneInf + '}^{' + borneSup + '}='
          stor.etapes[0] += '\\frac{' + chaineTemp1 + '-' + chaineTemp2 + '}{' + coeffMultiplicateur + '}'
          stor.phrase[0] = textes.correction1_2
          stor.phrase[1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = '(exp(' + exp1 + ')-exp(' + exp2 + '))/' + coeffMultiplicateur
        } else {
          stor.etapes[0] = '[' + coeffMultiplicateur + '\\times \\mathrm{e}^{' + u1X + '}]_{' + borneInf + '}^{' + borneSup + '}='
          stor.etapes[0] += coeffMultiplicateur + '(' + chaineTemp1 + '-' + chaineTemp2 + ')'
          stor.phrase[0] = textes.correction1_3
          stor.phrase[1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = coeffMultiplicateur + '*(exp(' + exp1 + ')-exp(' + exp2 + '))'
        }
      }
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      // j3p.stockage[1] = operation1
      stor.operation1 = operation1
    }
    if (exp1 === exp2) stor.etapes[0] += '=0'
    if (coeffMultiplicateur === 1) {
      stor.solution[2] = 'exp(' + u1X + ')'
    } else {
      if (multOuDiv === 'div') {
        stor.solution[2] = '(exp(' + u1X + '))/' + coeffMultiplicateur
      } else {
        stor.solution[2] = coeffMultiplicateur + '*exp(' + u1X + ')'
      }
    }
  }

  function enonceMain () {
    // dénomination claire :
    const coeffsU = []
    // moins claire, car ça peut être lambda*u'
    const coeffsUprime = []
    // détermination du degré de u (1 ou 2)
    let degreDeU, presenceCoeff, laBorneInf, laBorneSup, leCoef, multdiv, alea
    if (ds.degre_u !== '[1;1]' && ds.degre_u !== '[2;2]') {
      degreDeU = j3pGetRandomInt(1, 2)
    } else {
      const [lemin, lemax] = j3pGetBornesIntervalle(ds.degre_u)
      degreDeU = j3pGetRandomInt(lemin, lemax)
    }
    /// ///////////////
    // pour mes tests//
    /// ///////////////
    // var degreDeU=2;
    /// ///////////////
    // détermination de la présence d’un coef multiplicateur ou non
    if (ds.coeff !== '[1;1]' && ds.coeff !== '[0;0]') {
      presenceCoeff = j3pGetRandomInt(0, 1)
    } else {
      const [lemin, lemax] = j3pGetBornesIntervalle(ds.coeff)
      presenceCoeff = j3pGetRandomInt(lemin, lemax)
    }
    // détermination du type de formule (voir un switch/case plus loin pour son usage)
    const nbCasPossibles = ds.formules.length
    const typeFormule = j3pGetRandomInt(0, (nbCasPossibles - 1))

    // je génère mes bornes de l’intégrales, j’ai un bug d’affichage à étudier si la borneSup<0 (cause symbole moins ?)
    // A noter que ces valeurs seront écrasées après suivant la formule utilisée... (car on a des conditions supplémentaires suivant le formule (ln(exp1)>0 par exemple))
    do {
      laBorneInf = j3pGetRandomInt(-4, 4)
      laBorneSup = j3pGetRandomInt(-4, 4)
      // A insérer : gestion de inversion_bornes, sachant que pb mathquill si négatif en haut de la borne...
      // il suffira d’intervertir leurs valeurs dans les appels aux fonctions tirage
    } while (laBorneInf >= laBorneSup)

    switch (degreDeU) {
      case 1:
        // console.log('degré 1')
        if (presenceCoeff === 0) {
          // pas de coeff multiplicateur
          if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
            // cas où il nous faut u>0 sur l’intervalle... (avec u affine c pas compliqué...)
            do {
              coeffsU[1] = j3pGetRandomInt(-7, 7)
              coeffsU[0] = j3pGetRandomInt(2, 10)
              laBorneInf = j3pGetRandomInt(-4, 4)
              laBorneSup = j3pGetRandomInt(-4, 4)
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
          } else {
            do {
              coeffsU[1] = j3pGetRandomInt(-7, 7)
              coeffsU[0] = j3pGetRandomInt(2, 10)
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[1] * laBorneSup + coeffsU[0] > 40)
          }
          // u'(x)=a
          coeffsUprime[0] = coeffsU[1]
          leCoef = 1
          multdiv = ''
        } else {
          // coeff multiplicateur
          alea = j3pGetRandomInt(0, 1)
          if (alea === 0) {
            // coef>1
            leCoef = j3pGetRandomInt(2, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
              // cas où il nous faut u>0 sur l’intervalle... (avec u affine c pas compliqué...)
              do {
                coeffsU[1] = j3pGetRandomInt(-7, 7)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[1] = j3pGetRandomInt(-7, 7)
                coeffsU[0] = j3pGetRandomInt(2, 10)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }
            // de la forme lambda*u'(x)*exp(u) avec lambda=leCoeff et u'(x)=a
            coeffsUprime[0] = leCoef * coeffsU[1]
            multdiv = 'mult'
          } else {
            // coeff<1
            leCoef = j3pGetRandomInt(2, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
              // cas où il nous faut u>0 sur l’intervalle... (avec u affine c pas compliqué...)
              do {
                coeffsU[1] = j3pGetRandomInt(-7, 7)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] % leCoef !== 0 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[1] = j3pGetRandomInt(-7, 7)
                coeffsU[0] = j3pGetRandomInt(2, 10)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] % leCoef !== 0 || coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }
            // de la forme u'(x)*exp(u)/lambda avec lambda=leCoeff et u'(x)=a
            coeffsUprime[0] = coeffsU[1] / leCoef
            multdiv = 'div'
          }
        }
        break

      case 2:
        // console.log('degré 2')
        if (presenceCoeff === 0) {
          // pas de coeff multiplicateur
          if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
            // cas où il nous faut u>0 sur l’intervalle... pour simplifier je prends donc a<0 et u(borneInf)>0 et u(borneSup)>0
            do {
              coeffsU[2] = j3pGetRandomInt(-7, -1)
              coeffsU[1] = j3pGetRandomInt(2, 10)
              coeffsU[0] = j3pGetRandomInt(2, 10)
              laBorneInf = j3pGetRandomInt(-4, 4)
              laBorneSup = j3pGetRandomInt(-4, 4)
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
          } else {
            do {
              coeffsU[2] = j3pGetRandomInt(-7, 7)
              coeffsU[1] = j3pGetRandomInt(2, 10)
              coeffsU[0] = j3pGetRandomInt(2, 10)
              // cas particulier pour exp, pour eviter u bug avec mtg32 - pas d’ecriture scientifique - pas plus d’exp(40)'
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] > 40)
          }
          // u'(x)=2ax+b
          coeffsUprime[1] = 2 * coeffsU[2]
          coeffsUprime[0] = coeffsU[1]
          leCoef = 1
          multdiv = ''
        } else {
          // coeff multiplicateur
          alea = j3pGetRandomInt(0, 1)
          if (alea === 0) {
            // coef>1
            leCoef = j3pGetRandomInt(2, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
              // cas où il nous faut u>0 sur l’intervalle... pour simplifier je prends donc a<0 et u(borneInf)>0 et u(borneSup)>0
              do {
                coeffsU[2] = j3pGetRandomInt(-7, -1)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[2] = j3pGetRandomInt(-7, 7)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                // cas particulier pour exp, pour eviter u bug avec mtg32 - pas d’ecriture scientifique - pas plus d’exp(40)'
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }

            // de la forme lambda*u'(x)*exp(u) avec lambda=leCoeff et u'(x)=2ax+b
            coeffsUprime[1] = leCoef * 2 * coeffsU[2]
            coeffsUprime[0] = leCoef * coeffsU[1]
            multdiv = 'mult'
          } else {
            // coeff<1
            leCoef = j3pGetRandomInt(2, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
              // cas où il nous faut u>0 sur l’intervalle... pour simplifier je prends donc a<0 et u(borneInf)>0 et u(borneSup)>0
              do {
                coeffsU[2] = j3pGetRandomInt(-7, -1)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[1] % leCoef !== 0 || (2 * coeffsU[2]) % leCoef !== 0 || coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[2] = j3pGetRandomInt(-7, 7)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                // cas particulier pour exp, pour eviter u bug avec mtg32 - pas d’ecriture scientifique - pas plus d’exp(40)'
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || coeffsU[1] % leCoef !== 0 || (2 * coeffsU[2]) % leCoef !== 0 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }
            // de la forme u'(x)*exp(u)/lambda avec lambda=leCoeff et u'(x)=2ax+b
            coeffsUprime[0] = coeffsU[1] / leCoef
            coeffsUprime[1] = 2 * coeffsU[2] / leCoef
            multdiv = 'div'
          }
        }
        break
    } // switch (degreDeU)

    // stockage[2] contiendra toutes les étapes de la correction,
    // stockage[3][0] la phrase de correction
    // stockage[3][1] sa variable
    // j3p.stockage[2] = []
    stor.etapes = []
    // j3p.stockage[3] = []
    stor.phrase = []
    switch (ds.formules[typeFormule]) {
      case 'uprimeexp(u)':
        // pas de condition particulière sur les bornes
        tirageCalculUPrimeExpu(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup)
        break

      case 'uprime/u':
        // console.log('uprime/u')
        // condition sur les bornes : il faut que u>0 sur l’intervalle :-(
        tirageCalculUPrimeU(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup)
        break

      case 'uprime/racine(u)':
        // console.log('uprime/racine(u)')
        tirageCalculUPrimeRacineU(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup)
        break

      case 'uprime*u^n':
        tirageCalculUPrimeUN(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup, j3pGetRandomInt(2, 4))
        break
    }

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div', '', { id: 'grandPere' })
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div', '', { id: 'pere', style: { paddingTop: '10px' } })
    j3pAffiche(zoneCons1, '', textes.question)
    const elt = j3pAffiche(zoneCons2, '', '$' + stor.operation1 + '=[$&1&$]_{' + laBorneInf + '}^{' + laBorneSup + '}=$&2&',
      {
        inputmq1: { texte: '', maxchars: '5' },
        inputmq2: { texte: '', maxchars: '5' }
      }
    )
    stor.zoneInput = [elt.inputmqList[0], elt.inputmqList[1]]
    mqRestriction(stor.zoneInput[0], '\\d,.-/^+*ex()', { commandes: ['racine', 'exp', 'ln', 'fraction', 'puissance'] })
    mqRestriction(stor.zoneInput[1], '\\d,.-/^+*e()', { commandes: ['racine', 'exp', 'ln', 'fraction', 'puissance'] })
    // Modifié par Yves pour méméoriser les bornes
    stor.laBorneInf = laBorneInf
    stor.laBorneSup = laBorneSup

    $(stor.zoneInput[0]).blur()
    $(stor.zoneInput[1]).blur()
    // j’ai toujours un pb à la tabulation'
    // Problèmes résolus cidessous par Yves
    stor.zoneInput[0].addEventListener('mouseup', changePalette1, false)
    $(stor.zoneInput[0]).focusin(function () {
      changePalette1.call(this)
    })
    stor.zoneInput[1].addEventListener('mouseup', changePalette2, false)
    $(stor.zoneInput[1]).focusin(function () {
      changePalette2.call(this)
    })

    stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px' } })
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput[0], { liste: ['racine', 'exp', 'fraction', 'puissance'] })

    me.logIfDebug('la solution :' + stor.solution[1] + ' et la primitive=' + stor.solution[2])
    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.enonce', { paddingTop: '45px' }) })
  } // enonceMain

  switch (this.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.construitStructurePage('presentation2')
        me.stockage = [0, 0, 0, 0]
        me.storage.solution = []
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // on appelle d’abord l’énoncé (il init des zones dont on a besoin ensuite
        enonceMain()
        // puis on charge mathgraph
        getMtgCore()
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              // Une liste qui servira pour les calculs.
              stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAPZAAACpQAAAQEAAAAAAAAAAQAAABH#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABYwABMAAAAAEAAAAAAAAAAAAAAAIA#####wABZAABMAAAAAEAAAAAAAAAAAAAAAIA#####wABYQABMAAAAAEAAAAAAAAAAP####8AAAABAAVDRm9uYwD#####AAFmAAF4#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAABeAAAAAQA#####wABZwABMAAAAAEAAAAAAAAAAAABeAAAAAIA#####wACaTEAATAAAAABAAAAAAAAAAAAAAACAP####8AAmkyAAEwAAAAAQAAAAAAAAAA#####wAAAAEACENEZXJpdmVlAP####8AAmYnAAAABQAAAAYA#####wACZycAAAAGAAAAAgD#####AAJ4MQABMQAAAAE#8AAAAAAAAAAAAAIA#####wACeDIAATIAAAABQAAAAAAAAAAAAAACAP####8AAngzAAEzAAAAAUAIAAAAAAAAAAAABAD#####AAR6ZXJvABJhYnMoeCk8MC4wMDAwMDAwMDH#####AAAAAQAKQ09wZXJhdGlvbgQAAAADAAAAAAUAAAAAAAAAAT4RLgvoJtaVAAF4AAAAAgD#####AAR0ZXN0ADt6ZXJvKGYnKHgxKS1nJyh4MSkpJnplcm8oZicoeDIpLWcnKHgyKSkmemVybyhmJyh4MyktZycoeDMpKQAAAAcKAAAABwr#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAAAOAAAABwEAAAAIAAAACf####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAALAAAACAAAAAoAAAAJAAAACwAAAAgAAAAOAAAABwEAAAAIAAAACQAAAAkAAAAMAAAACAAAAAoAAAAJAAAADAAAAAgAAAAOAAAABwEAAAAIAAAACQAAAAkAAAANAAAACAAAAAoAAAAJAAAADQAAAAIA#####wABYgABMAAAAAEAAAAAAAAAAP###############w==')
              j3pFocus(stor.zoneInput[0])
              me.finEnonce()
            },
            // failure
            (error) => j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
          )
          // plantage le code de success
          .catch(j3pShowError)
      } else {
        me.videLesZones()
        enonceMain()
        me.finEnonce()
      }
      break // case "enonce":

    case 'correction': {
      $(stor.zoneInput[0]).blur()
      $(stor.zoneInput[1]).blur()
      // on teste si une réponse a été saisie
      const repEleve = $(stor.zoneInput[1]).mathquill('latex')
      const repElevePrimitive = $(stor.zoneInput[0]).mathquill('latex')
      stor.bonneRep = [] // Stockage de la qualité de la réponse (substitut à reponseAnalysee)
      if ((repEleve === '' || repElevePrimitive === '') && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        if (repEleve !== '') j3pFocus(stor.zoneInput[1])
        else j3pFocus(stor.zoneInput[0])
        me.afficheBoutonValider()
      } else { // une réponse a été saisie
        // var repEleve_xcas=j3pMathquillXcas(repEleve)
        // var reponseAnalysee=analyse_formelle(repEleve_xcas)
        // if (reponseAnalysee.bon && reponseAnalysee.simplifie){
        // Avec MTG32 :
        const repEleveNettoyee = mathquillMtg32(repEleve)
        // console.log('repEleve_mtg32=' + repEleveNettoyee)
        const reponseAnalysee = analyseFormelleMtg32(repEleveNettoyee)
        // var repEleveNettoyeePrim=mathquillMtg32(repElevePrimitive)
        // console.log("repEleve_mtg32_prim="+repEleveNettoyeePrim)
        const repEleveNettoyeePrim = j3pMathquillXcas(repElevePrimitive)
        // console.log('repEleveNettoyeePrim:', repEleveNettoyeePrim, repElevePrimitive)
        // console.log('repEleve=' + repEleve, repEleveNettoyee, reponseAnalysee)
        const reponseAnalysee2 = egaliteFormellePrimitive(repEleveNettoyeePrim, stor.solution[2])
        // console.log('reponseAnalysee2=' + reponseAnalysee2)
        stor.bonneRep[0] = reponseAnalysee2
        stor.bonneRep[1] = reponseAnalysee.bon
        if (reponseAnalysee.bon && reponseAnalysee2 && reponseAnalysee.simplifie) {
          me.score++
          // section602Quotcorrection_detaillee(j3p.stockage[1],j3p.stockage[2],j3p.stockage[3],j3p.stockage[4]);
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          stor.zoneCorr.innerHTML += '<br>'
          stor.zoneCorr.innerHTML += textes.correction
          macorrection(reponseAnalysee, true)
          stor.zoneInput.forEach(eltInput => {
            eltInput.style.color = me.styles.cbien
            j3pDesactive(eltInput)
            j3pFreezeElt(eltInput)
          })
          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          // à cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            // me.typederreurs[10]++;
            macorrection(reponseAnalysee, false)
            stor.zoneInput.forEach((eltInput, index) => {
              eltInput.style.color = me.styles.cfaux
              j3pDesactive(eltInput)
              if (!stor.bonneRep[index]) j3pBarre(eltInput)
              j3pFreezeElt(eltInput)
            })
            // je classe cette erreur comme 'autre'
            me.typederreurs[10]++
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // S’il y a deux chances,on appelle à nouveau le bouton Valider
            // console.log('repAnalysee', reponseAnalysee, reponseAnalysee2)
            stor.zoneInput[1].style.color = (reponseAnalysee.bon) ? me.styles.cbien : me.styles.cfaux
            stor.zoneInput[0].style.color = (reponseAnalysee2) ? me.styles.cbien : me.styles.cfaux
            if (reponseAnalysee.bon) j3pDesactive(stor.zoneInput[1])
            if (reponseAnalysee2) j3pDesactive(stor.zoneInput[0])
            if (me.essaiCourant < ds.nbchances) {
              if (!reponseAnalysee.bon) j3pFocus(stor.zoneInput[1])
              if (!reponseAnalysee2)j3pFocus(stor.zoneInput[0])
              if (!reponseAnalysee.bon && !reponseAnalysee2) {
                stor.zoneCorr.innerHTML = textes.phrase_correction1
              } else {
                if (!reponseAnalysee.bon) {
                  stor.zoneCorr.innerHTML = textes.phrase_correction2
                } else {
                  stor.zoneCorr.innerHTML = textes.phrase_correction3
                }
              }
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              me.etat = 'correction'
              // j3pFocus("affiche1input1");
              me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
            } else {
              // Erreur au second essai
              stor.zoneCorr.innerHTML = cFaux
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              macorrection(reponseAnalysee, false)
              stor.zoneInput.forEach((eltInput, index) => {
                if (!stor.bonneRep[index]) {
                  j3pDesactive(eltInput)
                  j3pBarre(stor.zoneInput[index])
                } // autrement elle a déjà été désactivée
                j3pFreezeElt(eltInput)
              })
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      me.finCorrection()
      break
    } // case "correction"

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
