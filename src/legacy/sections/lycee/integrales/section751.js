import { j3pAddElt, j3pAjouteBouton, j3pAjouteCaseCoche, j3pBarre, j3pCompareTab, j3pCompareTab2, j3pElement, j3pEmpty, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pPaletteMathquill, j3pRandomTab, j3pShuffleMulti } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pCreeBoutonFenetre, j3pDetruitToutesLesFenetres } from 'src/legacy/core/functionsJqDialog'
import Repere from 'src/legacy/outils/geometrie/Repere'

import { ajouteVideo } from 'src/lib/utils/dom/main'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
Intégrales et vidéos
Dev : Alexis Lecomte
Date : Avril-Juin 2013
Remarques : section-test d’usage de vidéo, en etape 1, suivie d’une etape QCM puis 2 ou 3 etapes de calcul intégrale par calcul d’aires.
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['seuilreussite', 0.5, 'reel', 'Si le QCM est bon on a un seuil de réussite pour les autres questions pour déterminer la pe'],
    ['seuilreussite2', 0.5, 'reel', 'Si le QCM est faux on a un seuil de réussite pour les autres questions pour déterminer la pe'],
    ['affine_possible', true, 'boolean', 'Pour savoir si l’on ne demande que intégrales de fonctions constantes ou avec des fonctions affines, auquel cas la vidéo n’est pas la même (un exemple supplémentaire)']
  ],

  // dans le cas d’une section QUALITATIVE
  pe: [
    { pe_1: 'QCM bien et plus de seuilreussite aux autres questions' },
    { pe_2: 'QCM bien et moins de seuilreussite aux autres questions' },
    { pe_3: 'Au moins une faute au QCM et plus de seuilreussite2 aux autres questions' },
    { pe_4: 'Au moins une faute au QCM et moins de seuilreussite2 aux autres questions' }
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  let ds = me.donneesSection
  // code Rémi, pour renvoyer + 3 pour 3 et - 5 pour -5
  function signe (nombre) {
  // nombre = Number(nombre);
    const absNombre = Math.abs(nombre)
    return (nombre < 0) ? ' - ' + absNombre : ' + ' + absNombre
  }
  // fonction qui retourne une expression latex d’une intégrale qu’on ne sait pas calculer
  function integralePasCalculable () {
    const choix = j3pGetRandomInt(1, 4)
    let a, b, expr, coeff1, coeff2, coeff3
    switch (choix) {
      case 1:// constante positive mais inversion des bornes
        do {
          a = j3pGetRandomInt(-8, 8)
          b = j3pGetRandomInt(-8, 8)
          // A voir si l’on ne met pas plus compliqué, à savoir des racines ou pi...
          expr = j3pGetRandomInt(1, 8)
        } while (a <= b)
        break

      case 2: // constante négative
        do {
          a = j3pGetRandomInt(-8, 8)
          b = j3pGetRandomInt(-8, 8)
          // A voir si l’on ne met pas plus compliqué, à savoir des racines ou pi...
          expr = j3pGetRandomInt(-8, -1)
        } while (a >= b)
        break

      case 3: // polynome de degré 2
        do {
          a = j3pGetRandomInt(-8, 8)
          b = j3pGetRandomInt(-8, 8)
          coeff1 = j3pGetRandomInt(-8, 8)
          coeff2 = j3pGetRandomInt(-8, 8)
          coeff3 = j3pGetRandomInt(-8, 8)
        } while (a >= b || coeff1 === 0 || coeff1 === 1 || coeff1 === -1 || coeff2 === 0 || coeff2 === 1 || coeff1 === -1 || coeff3 === 0)
        expr = coeff1 + 'x^2' + signe(coeff2) + 'x' + signe(coeff3)
        break

      case 4: // avec une exponentielle
        do {
          a = j3pGetRandomInt(-8, 8)
          b = j3pGetRandomInt(-8, 8)
          coeff1 = j3pGetRandomInt(-8, 8)
          coeff2 = j3pGetRandomInt(-8, 8)
        } while (a >= b || coeff1 === 0 || coeff1 === 1 || coeff1 === -1 || coeff2 === 0 || coeff2 === 1)
        expr = 'e^{' + coeff1 + 'x' + signe(coeff2) + '}'
        break
    }
    return '$\\int_{' + a + '}^{' + b + '} ' + expr + ' \\mathrm dx$'
  }
  // fonction qui retourne une expression latex d’une intégrale qu’on sait  calculer, on garde le résultat de l’intégrale en mémoire pour l’étape 3
  function integraleCalculable () {
    let choix
    if (ds.affine_possible) {
      // var choix=j3pGetRandomInt(1, 2)
      choix = j3pRandomTab([1, 2], [0.2, 0.8])
      // var choix=2;
    } else {
      choix = 1
    }
    let a, b, expression, valeur, leNb, coeff1, coeff2, petiteBase, grandeBase, expr
    switch (choix) {
      case 1:
        // constante positive
        do {
          a = j3pGetRandomInt(-8, 8)
          b = j3pGetRandomInt(-8, 8)
          // A voir si l’on ne met pas plus compliqué, à savoir des racines ou pi...
          leNb = j3pGetRandomInt(1, 8)
        } while ((a - b >= 0))
        expression = '\\int_{' + a + '}^{' + b + '} ' + leNb + ' \\mathrm dx'
        valeur = (b - a) * leNb
        me.storage.solution[1].push(valeur)
        me.stockage[2].push(expression)
        me.stockage[5].push(b - a)
        me.stockage[6].push(leNb)
        // necessaire pour ne pas créer de décalage dans les tableaux 7 et 8
        me.stockage[7].push('')
        me.stockage[8].push('')
        me.stockage[30].push(false)
        me.stockage[9].push(a)
        me.stockage[11].push(b)
        me.stockage[12].push(leNb)
        me.stockage[13].push(leNb)
        break

      case 2:
        // fonction affine
        // pour la correction
        me.stockage[30].push(true)
        do {
          a = j3pGetRandomInt(-8, 8)
          b = j3pGetRandomInt(-8, 8)
          coeff1 = j3pGetRandomInt(-8, 8)
          coeff2 = j3pGetRandomInt(-8, 8)
          petiteBase = coeff1 * a + coeff2
          grandeBase = coeff1 * b + coeff2
        } while (a >= b || coeff1 === 0 || coeff1 === 1 || coeff1 === -1 || coeff2 === 0 || coeff2 === 1 || petiteBase <= 0 || grandeBase <= 0 || petiteBase > 8 || grandeBase > 8)
        expr = coeff1 + 'x' + signe(coeff2)
        expression = '\\int_{' + a + '}^{' + b + '} ' + expr + ' \\mathrm dx'
        valeur = (b - a) * (petiteBase + grandeBase) / 2
        // a changer
        // correction2bis:"$£a$ est l’aire (en unité d’aire) d’un trapèze égale à la somme de l’aire d’un rectangle de dimensions $£b$ et $£c$ et d’un triangle rectangle de dimensions $£b$ et $£d$ , cette intégrale est donc égale à $£e$ soit $£f$."
        // f :
        me.storage.solution[1].push(valeur)
        // a
        me.stockage[2].push(expression)
        // b
        me.stockage[5].push(b - a)
        //
        {
          let largeurRectangle
          let largeurTriangle
          if (petiteBase < grandeBase) {
            largeurRectangle = petiteBase
            largeurTriangle = grandeBase - petiteBase
          } else {
            largeurRectangle = grandeBase
            largeurTriangle = petiteBase - grandeBase
          }
          // c
          me.stockage[6].push(largeurRectangle)
          // d
          me.stockage[7].push(largeurTriangle)
          // e le calcul
          me.stockage[8].push((b - a) + '\\times' + largeurRectangle + ' + \\frac{' + (b - a) + '\\times' + largeurTriangle + '}{2}')
        }
        me.stockage[9].push(a)
        me.stockage[11].push(b)
        me.stockage[12].push(petiteBase)
        me.stockage[13].push(grandeBase)
        break

      case 3:
        break
    }

    return expression
  }
  function determinePhraseDetat () {
    const nbIntegralesACalculer = ds.nbetapes - 2
    let laPe
    if (me.stockage[31]) {
      // bon au QCM, on regarde si le nb de bonnes réponses aux intégrales est >= seuilreussite
      if (me.stockage[32] / nbIntegralesACalculer > ds.seuilreussite) {
        laPe = ds.pe_1
      } else {
        laPe = ds.pe_2
      }
    } else {
      if (me.stockage[32] / nbIntegralesACalculer > ds.seuilreussite2) {
        laPe = ds.pe_3
      } else {
        laPe = ds.pe_4
      }
    }
    return laPe
  }
  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 1,
      nbetapes: 3,
      indication: '',
      // surchargeindication: false;  pour le cas de presentation1bis
      nbchances: 2,
      affine_possible: true,
      // aucun intérêt à surcharger, car le QCM est aléatoire
      chamboule: true,
      // pe=0;
      // Si le QCM est bon on a un seuil de réussite pour les autres questions
      seuilreussite: 0.5,
      // Si le QCM est faux on a un autre seuil de réussite pour les autres questions
      seuilreussite2: 0.5,
      pe_1: 'QCM bien et plus de seuilreussite aux autres questions',
      pe_2: 'QCM bien et moins de seuilreussite aux autres questions',
      pe_3: 'Au moins une faute au QCM et plus de seuilreussite2 aux autres questions',
      pe_4: 'Au moins une faute au QCM et moins de seuilreussite2 aux autres questions',
      structure: 'presentation2', //  || "presentation2" || "presentation3"  || "presentation1bis"
      //   limite=5;

      textes: {
        titreExo: 'Introduction aux intégrales',
        question1: 'Parmi les intégrales suivantes, quelles sont celles que l’on peut calculer à partir des explications précédentes ?',
        revoir: 'Revoir la vidéo',
        demarrer: 'Démarrer',
        enonce1: 'Le bouton ci dessous permet d’accéder à une vidéo de présentation des intégrales. Une fois la vidéo terminée, il faut cliquer sur le bouton Suite pour accéder à un QCM portant sur la vidéo.<br><br>',
        question2: 'Complète maintenant la valeur de cette intégrale :',
        // question2: "Complète maintenant la valeur de ces intégrales : (tu peux t’aider du repère et déplacer les points)",
        correction: 'A l’aide des explications précédentes, on peut calculer les intégrales de la forme $\\int_{a}^{b}f(x)\\mathrm dx$, avec $a < b$ et $f(x)$ une constante positive ',
        correctionbis: 'A l’aide des explications précédentes, on peut calculer les intégrales de la forme $\\int_{a}^{b}f(x)\\mathrm dx$, avec $a < b$ et $f(x)$ une fonction affine positive sur l’intervalle $[a;b]$. ',
        correction1: '$£a$ cases ont été cochées par erreur.',
        correction1v2: '1 case a été cochée par erreur.',
        correction1bis: 'Tu as oublié de cocher $£a$ bonnes réponses.',
        correction1bisv2: 'Tu as oublié de cocher une bonne réponse.',
        correction2: '$£a$ est l’aire (en unité d’aire) d’un rectangle de dimensions $£b$ et $£c$, cette intégrale est donc égale à $£d$.',
        correction2bis: '$£a$ est l’aire (en unité d’aire) d’un trapèze égale à la somme de l’aire d’un rectangle de dimensions $£b$ et $£c$ et d’un triangle rectangle de dimensions $£b$ et $£d$ , cette intégrale est donc égale à $£e$ soit $£f$.',
        aideOutils: 'Aide & outils',
        aide: 'Une aide',
        calc: 'Une calculatrice',
        outils: 'Les outils'
      }
    }
  }

  function afficheQuestion () {
    afficheVideo()
    me.etat = 'enonce'
    me.afficheBoutonSuite()
  }

  function afficheVideo (inDialog) {
    const conteneur = inDialog ? j3pElement(stor.fenetreVideo) : me.zonesElts.MG
    let mp4, webm
    if (ds.affine_possible) {
      mp4 = `${j3pBaseUrl}static/videos/integrales_2.mp4`
      webm = `${j3pBaseUrl}static/videos/integrales_2.webm`
    } else {
      mp4 = `${j3pBaseUrl}static/videos/integrales_1_600.mp4`
      webm = `${j3pBaseUrl}static/videos/integrales_1_600.mp4`
    }
    ajouteVideo(conteneur, { mp4, webm, width: '870px', height: '428px' })
  }

  function afficheRepereCorrection (xA, xB, xC, yC, xD, yD) {
    // je nettoie l’ancienne figure:
    j3pEmpty(stor.leGraphe)
    let xE, yE
    if (yC > yD) {
      xE = xC
      yE = yD
    } else {
      xE = xD
      yE = yC
    }
    const repere = new Repere({
      idConteneur: stor.leGraphe,
      aimantage: false,
      visible: true,
      trame: false,
      fixe: false,
      larg: 550,
      haut: 350,

      pasdunegraduationX: 1,
      pixelspargraduationX: 30,
      pasdunegraduationY: 1,
      pixelspargraduationY: 30,
      xO: 275,
      yO: 250,
      debuty: 0,
      negatifs: true,
      objets: [
        // les deux points mobiles :
        { type: 'point', nom: 'A', par1: xA, par2: 0, fixe: true, visible: true, etiquette: false, style: { couleur: '#456', epaisseur: 2, taille: 18, taillepoint: 5 } },
        { type: 'point', nom: 'B', par1: xB, par2: 0, fixe: true, visible: true, etiquette: false, style: { couleur: '#456', epaisseur: 2, taille: 18, taillepoint: 5 } },
        { type: 'point', nom: 'C', par1: xC, par2: yC, fixe: true, visible: true, etiquette: false, style: { couleur: '#456', epaisseur: 2, taille: 18, taillepoint: 5 } },
        { type: 'point', nom: 'D', par1: xD, par2: yD, fixe: true, visible: true, etiquette: false, style: { couleur: '#456', epaisseur: 2, taille: 18, taillepoint: 5 } },
        { type: 'polygone', nom: 'pol1', par1: ['A', 'C', 'D', 'B'], style: { couleur: '#FA5', couleurRemplissage: '#FA5', opaciteRemplissage: 0.3 } },
        { type: 'droite', nom: 'd4', par1: 'C', par2: 'D', style: { couleur: '#FA5', epaisseur: 2 } },
        { type: 'point', nom: 'E', par1: xE, par2: yE, fixe: true, visible: false, etiquette: false, style: { couleur: '#456', epaisseur: 2, taille: 18, taillepoint: 5 } },
        { type: 'segment', nom: 's', par1: 'E', par2: 'D', style: { couleur: '#456', epaisseur: 2 } }
      ]
    })
    repere.construit()
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    // pour savoir combien il y aura d’intégrales à calculer et donc combien d’étapes puisqu’il y a une étape par intégrale...
    const nbBonnesReponses = j3pGetRandomInt(2, 3)
    ds.nbetapes = nbBonnesReponses + 2
    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
    me.storage.solution = []
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(ds.textes.titreExo)

    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    me.stockage[10] = 0
    me.stockage[20] = nbBonnesReponses
  }
  function enonceMain () {
    me.stockage[0] = '1'
    let nbBonnesReponses
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '20px' }) })
    switch (me.questionCourante % ds.nbetapes) {
      case 1:
      {
        // le texte du début
        const divConsigne = j3pAddElt(stor.conteneur, 'div')
        const laconsigne = ds.textes.enonce1
        j3pAffiche(divConsigne, '', laconsigne, { a: ds.NombreMin, b: ds.NombreMax })
        const divBouton = j3pAddElt(stor.conteneur, 'div')
        j3pAjouteBouton(divBouton, afficheQuestion, { className: 'MepBoutons', value: ds.textes.demarrer })
        break
      }
      case 2: {
        // une seule chance sur cette étape :
        me.essaiCourant = ds.nbchances - 1
        me.stockage[0] = []
        me.storage.solution[0] = []
        me.storage.solution[1] = []
        me.stockage[2] = []
        me.stockage[5] = []
        me.stockage[6] = []
        me.stockage[7] = []
        me.stockage[8] = []
        me.stockage[9] = []
        me.stockage[11] = []
        me.stockage[12] = []
        me.stockage[13] = []
        me.stockage[30] = []
        // pour la PE finale : (comptera le nb de bonnes réponses au calcul d’intégrales
        me.stockage[32] = 0
        const enonceTxt = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingBottom: '15px' } })
        j3pAffiche(enonceTxt, '', ds.textes.question1)
        // mon tableau de reponses :
        nbBonnesReponses = me.stockage[20]
        // utile en étape 3
        const nbReponses = 6
        const nbReponsesFausses = nbReponses - nbBonnesReponses
        let tabRep = []
        let bonneExpression
        for (let i = 0; i < nbBonnesReponses; i++) {
          if (i === 0) {
            bonneExpression = integraleCalculable()
            tabRep.push('$' + bonneExpression + '$')
          } else {
            do {
              bonneExpression = integraleCalculable()
            } while (tabRep.includes(bonneExpression))
            tabRep.push('$' + bonneExpression + '$')
          }
          // je garde en mémoire les bonnes réponses pour l’étape 3:
          me.stockage[0].push(bonneExpression)
          // la solution :
          me.storage.solution[0].push(1)
        }
        let expression
        for (let i = 0; i < nbReponsesFausses; i++) {
          if (i === 0) {
            expression = integralePasCalculable()
            tabRep.push(expression)
          } else {
            expression = integralePasCalculable()
            tabRep.push(expression)
          }
          // la solution
          me.storage.solution[0].push(0)
        }
        const tabsMelanges = j3pShuffleMulti(tabRep, me.storage.solution[0])
        tabRep = tabsMelanges[0]
        me.storage.solution[0] = tabsMelanges[1]
        // C’est là que je construit la liste des réponses
        // je vais stocker le nom des cases à cocher
        stor.nomInput = []
        for (let i = 0; i < tabRep.length; i++) {
          const div = j3pAddElt(stor.conteneur, 'div', '', { style: { padding: '5px 0' } })
          stor.nomInput.push(j3pAjouteCaseCoche(div, {
            label: tabRep[i]
          }))
        }
        stor.fenetreVideo = j3pGetNewId('fenetreVideo')
        // stor.fenetreAide = j3pGetNewId('fenetreAide')
        stor.fenetreBoutons = j3pGetNewId('fenetreBoutons')
        stor.nameBoutons = j3pGetNewId('Boutons')
        // stor.nameAide = j3pGetNewId('Aide')
        stor.nameVideo = j3pGetNewId('Video')
        /* me.fenetresjq = [
          { name: stor.nameBoutons, title: ds.textes.aideOutils, left: 650, top: 130, id: stor.fenetreBoutons },
          { name: 'Calculatrice', title: ds.textes.calc, left: 50, top: 130 },
          { name: stor.nameAide, title: ds.textes.aide, top: 170, left: 90, width: 500, id: stor.fenetreAide }
        ] */

        // on laisse la possibilité de revoir la video :
        me.fenetresjq = [
          // Attention : positions par rapport à mepact
          { name: stor.nameBoutons, title: ds.textes.outils, left: 660, top: 430, height: 120, id: stor.fenetreBoutons },
          { name: stor.nameVideo, title: ds.textes.revoir, width: 730, top: 120, left: 90, id: stor.fenetreVideo }
        ]
        // Création mais sans affichage.
        j3pCreeFenetres(me)
        // création de la video dans le div fenetreVideo
        afficheVideo(true)
        j3pCreeBoutonFenetre(stor.nameVideo, stor.fenetreBoutons, { top: 10, left: 10 }, ds.textes.revoir)
        j3pToggleFenetres(stor.nameBoutons)
        j3pAfficheCroixFenetres(stor.nameVideo)
        break
      }

      default :
        j3pDetruitToutesLesFenetres()
        stor.zoneCons1 = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingBottom: '10px' } })
        j3pAffiche(stor.zoneCons1, '', ds.textes.question2)
        stor.zoneCons2 = j3pAddElt(stor.conteneur, 'div')
        {
          const i = me.stockage[10]

          // affichage des zones de saisies :
          const elt = j3pAffiche(stor.zoneCons2, '', '$' + me.stockage[2][i] + '=$&1&', { inputmq1: { texte: '' } })
          stor.zoneInput = elt.inputmqList[0]
          stor.leGraphe = j3pAddElt(stor.conteneur, 'div')
          stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.zoneInput.id] })
          stor.zoneInput.typeReponse = ['nombre', 'exact']
          stor.zoneInput.reponse = [me.storage.solution[1][i]]
          // palette MQ :
          stor.zoneCons4 = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '5px', paddingBottom: '40px' } })
          mqRestriction(stor.zoneInput, '\\d,./-', { commandes: ['fraction'] })
          j3pPaletteMathquill(stor.zoneCons4, stor.zoneInput.id, { liste: ['fraction'] })
          j3pCreeFenetres(me)
          j3pFocus(stor.zoneInput)
          me.stockage[10]++
        }
        break
    }
    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
    // Obligatoire
    me.finEnonce()
    if (me.questionCourante % ds.nbetapes === 1) me.cacheBoutonValider()
  }
  switch (me.etat) {
    case 'enonce': {
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":
    }

    case 'correction': {
      {
        j3pEmpty(stor.zoneCorr)
        // on teste si une réponse a été saisie
        let reponse = { aRepondu: false, bonneReponse: false }
        let cbon, i
        const reponseEleve = []
        switch (me.questionCourante % ds.nbetapes) {
          case 2:
            // if ((me.questionCourante%ds.nbetapes) === 2){
            // stor.ferme_video();
            reponse.aRepondu = false
            for (let k = 0; k < me.storage.solution[0].length; k++) {
              reponseEleve[k] = stor.nomInput[k].checked
              if (reponseEleve[k]) reponse.aRepondu = true
            }
            break
          default :
            i = me.stockage[10] - 1
            reponse = stor.fctsValid.validationGlobale()
            break
        }
        let oubli, nbErreur
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else { // une réponse a été saisie
          if ((me.questionCourante % ds.nbetapes) === 2) {
            cbon = j3pCompareTab(me.storage.solution[0], reponseEleve)
            // pour définir la PE finale
            me.stockage[31] = cbon
            oubli = 0
            nbErreur = 0
            for (let compteur = 0; compteur < me.storage.solution[0].length; compteur++) {
              if (me.storage.solution[0][compteur] && !reponseEleve[compteur]) {
                // l’élève a pas coché alors que c'était juste
                oubli++
              }
              if (!me.storage.solution[0][compteur] && reponseEleve[compteur]) {
                // l’élève a coché alors que c'était faux
                nbErreur++
              }
              if (me.storage.solution[0][compteur] && reponseEleve[compteur]) stor.nomInput[compteur].label.style.color = me.styles.cbien
              j3pDesactive(stor.nomInput[compteur])
            }
          } else {
            cbon = reponse.bonneReponse
            if (cbon) me.stockage[32]++
          }
          if (cbon) {
            me.score++
            me._stopTimer()
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            if ((me.questionCourante - 1) % ds.nbetapes > 1) j3pEmpty(stor.zoneCons4)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneCorr.style.color = me.styles.petit.correction.color
            let tempo
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = '<br>' + tempsDepasse
              me._stopTimer()
              me.typederreurs[10]++
              // la correction
              if ((me.questionCourante % ds.nbetapes) === 2) {
                tempo = j3pCompareTab2(me.storage.solution[0], reponseEleve)
                for (let k = 0; k < tempo.length; k++) {
                  if (!me.storage.solution[0][k] && reponseEleve[k]) {
                    stor.nomInput[k].label.style.color = this.styles.cfaux
                    j3pBarre(stor.nomInput[k].label)
                  }
                  if (me.storage.solution[0][k]) {
                    // On la met dans la couleur de la correction
                    stor.nomInput[k].label.style.color = this.styles.toutpetit.correction.color
                  }
                }
                if (ds.affine_possible) {
                  j3pAffiche(stor.zoneExpli, '', ds.textes.correctionbis)
                } else {
                  j3pAffiche(stor.zoneExpli, '', ds.textes.correction)
                }
              } else {
                if (!reponse.bonneReponse) {
                  const divExpli = j3pAddElt(stor.zoneExpli, 'div')
                  j3pAffiche(divExpli, '', ds.textes.correction2, {
                    a: me.stockage[2][i],
                    b: me.stockage[5][i],
                    c: me.stockage[6][i],
                    d: me.storage.solution[1][i]
                  })
                }
                j3pEmpty(stor.zoneCons4)
              }
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
              // réponse fausse :
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML = '<br>' + cFaux
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
                me.typederreurs[2]++
                // la correction
                if ((me.questionCourante % ds.nbetapes) === 2) {
                  // A faire : ajouter tests suivants oubli et nbErreur
                  const divExpli1 = j3pAddElt(stor.zoneExpli, 'div')
                  if (nbErreur > 0) {
                    if (nbErreur === 1) {
                      j3pAffiche(divExpli1, '', ds.textes.correction1v2)
                    } else {
                      j3pAffiche(divExpli1, '', ds.textes.correction1, { a: nbErreur })
                    }
                  }
                  const divExpli2 = j3pAddElt(stor.zoneExpli, 'div')
                  if (oubli > 0) {
                    if (oubli === 1) {
                      j3pAffiche(divExpli2, '', ds.textes.correction1bisv2)
                    } else {
                      j3pAffiche(divExpli2, '', ds.textes.correction1bis, { a: oubli })
                    }
                  }
                  tempo = j3pCompareTab2(me.storage.solution[0], reponseEleve)
                  for (let k = 0; k < tempo.length; k++) {
                    if (!me.storage.solution[0][k] && reponseEleve[k]) {
                      stor.nomInput[k].label.style.color = this.styles.cfaux
                      j3pBarre(stor.nomInput[k].label)
                    }
                    if (me.storage.solution[0][k]) {
                      // On la met dans la couleur de la correction
                      stor.nomInput[k].label.style.color = this.styles.toutpetit.correction.color
                    }
                  }
                  const divExpli3 = j3pAddElt(stor.zoneExpli, 'div')
                  if (ds.affine_possible) {
                    j3pAffiche(divExpli3, '', ds.textes.correctionbis)
                  } else {
                    j3pAffiche(divExpli3, '', ds.textes.correction)
                  }
                  // Erreur au second essai
                  me._stopTimer()
                } else {
                  afficheRepereCorrection(me.stockage[9][i], me.stockage[11][i], me.stockage[9][i], me.stockage[12][i], me.stockage[11][i], me.stockage[13][i])
                  stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                  const divExpli = j3pAddElt(stor.zoneExpli, 'div')
                  if (me.stockage[30][i]) {
                    j3pAffiche(divExpli, '', ds.textes.correction2bis, {
                      a: me.stockage[2][i],
                      b: me.stockage[5][i],
                      c: me.stockage[6][i],
                      d: me.stockage[7][i],
                      e: me.stockage[8][i],
                      f: me.storage.solution[1][i]
                    })
                  } else {
                    j3pAffiche(divExpli, '', ds.textes.correction2, {
                      a: me.stockage[2][i],
                      b: me.stockage[5][i],
                      c: me.stockage[6][i],
                      d: me.storage.solution[1][i]
                    })
                  }
                  j3pEmpty(stor.zoneCons4)
                }
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = determinePhraseDetat()
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
