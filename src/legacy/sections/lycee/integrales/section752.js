import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pDetruit, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pPGCD, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// section fortement paramétrable sur le calcul de primitives en TS :
// primitive de f sur [a;b] où f(x)=
// a) u’exp(u)
// b) uprime/u
// c) u'/racine(u)
// d) u'*u^n
// avec ou non coefficient multiplicateur
// avec u affine ou de degré 2.

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['formules', ['uprimeexp(u)', 'uprime/u', 'uprime/racine(u)', 'uprime*u^n'], 'array', 'tableau des formules possibles uprimeexp(u) uprime/u uprime/racine(u) et uprime*u^n'],
    ['coeff', '[1;1]', 'string', '[0;0](sans coeff multiplicateur) [1;1](avec) ou [0;1](aléatoire)'],
    ['degre_u', '[1;2]', 'string', ' [1;1] (affine), [2;2] (deg2) ou [1;2] (alea)']
  ]

}

/**
 * section 752
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = this.storage

  function enonceMain () {
    depart()
    const debuthaut = 50
    // dénomination claire :
    const coeffsU = []
    // moins claire, car ça peut être lambda*u'
    const coeffsUprime = []
    // détermination du degré de u (1 ou 2)
    let degreDeU, presenceCoeff
    if (ds.degre_u !== '[1;1]' && ds.degre_u !== '[2;2]') {
      degreDeU = j3pGetRandomInt(1, 2)
    } else {
      const [intMin, intMax] = j3pGetBornesIntervalle(ds.degre_u)
      degreDeU = j3pGetRandomInt(intMin, intMax)
    }
    /// ///////////////
    // détermination de la présence d’un coef multiplicateur ou non
    if (ds.coeff !== '[1;1]' && ds.coeff !== '[0;0]') {
      presenceCoeff = j3pGetRandomInt(0, 1)
    } else {
      const [intMin, intMax] = j3pGetBornesIntervalle(ds.coeff)
      presenceCoeff = j3pGetRandomInt(intMin, intMax)
    }
    // détermination du type de formule (voir un switch/case plus loin pour son usage)
    const nbCasPossibles = ds.formules.length
    const typeFormule = j3pGetRandomInt(0, (nbCasPossibles - 1))
    /// ///////////////
    // pour mes tests//
    //   me.formules=["uprimeexp(u)","uprime/u","uprime/racine(u)","uprime*u^n"]
    /// ///////////////
    // var typeFormule=2;
    // var presenceCoeff=1;
    /// ///////////////////
    // je génère mes bornes de l’intégrales, j’ai un bug d’affichage à étudier si la borneSup<0 (cause symbole moins ?)
    // A noter que ces valeurs seront écrasées après suivant la formule utilisée... (car on a des conditions supplémentaires suivant le formule (ln(exp1)>0 par exemple))
    let laBorneInf, laBorneSup, leCoef, multdiv, alea
    do {
      laBorneInf = j3pGetRandomInt(-4, 4)
      laBorneSup = j3pGetRandomInt(-4, 4)
      // A insérer : gestion de inversion_bornes, sachant que pb mathquill si négatif en haut de la borne...
      // il suffira d’intervertir leurs valeurs dans les appels aux fonctions tirage
    } while (laBorneInf >= laBorneSup)

    switch (degreDeU) {
      case 1:
        if (presenceCoeff === 0) {
          // pas de coeff multiplicateur
          if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
            // cas où il nous faut u>0 sur l’intervalle... (avec u affine c pas compliqué...)
            do {
              coeffsU[1] = j3pGetRandomInt(-7, 7)
              coeffsU[0] = j3pGetRandomInt(2, 10)
              laBorneInf = j3pGetRandomInt(-4, 4)
              laBorneSup = j3pGetRandomInt(-4, 4)
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
          } else {
            do {
              coeffsU[1] = j3pGetRandomInt(-7, 7)
              coeffsU[0] = j3pGetRandomInt(2, 10)
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[1] * laBorneSup + coeffsU[0] > 40)
          }
          // u'(x)=a
          coeffsUprime[0] = coeffsU[1]
          leCoef = 1
          multdiv = ''
        } else {
          // coeff multiplicateur
          alea = j3pGetRandomInt(0, 1)
          if (alea === 0) {
            // coef>1
            leCoef = j3pGetRandomInt(2, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
              // cas où il nous faut u>0 sur l’intervalle... (avec u affine c pas compliqué...)
              do {
                coeffsU[1] = j3pGetRandomInt(-7, 7)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[1] = j3pGetRandomInt(-7, 7)
                coeffsU[0] = j3pGetRandomInt(2, 10)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }
            // de la forme lambda*u'(x)*exp(u) avec lambda=leCoeff et u'(x)=a
            coeffsUprime[0] = leCoef * coeffsU[1]
            multdiv = 'mult'
          } else {
            // coeff<1
            leCoef = j3pGetRandomInt(2, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
              // cas où il nous faut u>0 sur l’intervalle... (avec u affine c pas compliqué...)
              do {
                coeffsU[1] = j3pGetRandomInt(-7, 7)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] % leCoef !== 0 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[1] = j3pGetRandomInt(-7, 7)
                coeffsU[0] = j3pGetRandomInt(2, 10)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] % leCoef !== 0 || coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }
            // de la forme u'(x)*exp(u)/lambda avec lambda=leCoeff et u'(x)=a
            coeffsUprime[0] = coeffsU[1] / leCoef
            multdiv = 'div'
          }
        }
        break

      case 2:
        if (presenceCoeff === 0) {
          // pas de coeff multiplicateur
          if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
            // cas où il nous faut u>0 sur l’intervalle... pour simplifier je prends donc a<0 et u(borneInf)>0 et u(borneSup)>0
            do {
              coeffsU[2] = j3pGetRandomInt(-7, -1)
              coeffsU[1] = j3pGetRandomInt(2, 10)
              coeffsU[0] = j3pGetRandomInt(2, 10)
              laBorneInf = j3pGetRandomInt(-4, 4)
              laBorneSup = j3pGetRandomInt(-4, 4)
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
          } else {
            do {
              coeffsU[2] = j3pGetRandomInt(-7, 7)
              coeffsU[1] = j3pGetRandomInt(2, 10)
              coeffsU[0] = j3pGetRandomInt(2, 10)
              // cas particulier pour exp, pour eviter u bug avec mtg32 - pas d’ecriture scientifique - pas plus d’exp(40)'
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] > 40)
          }
          // u'(x)=2ax+b
          coeffsUprime[1] = 2 * coeffsU[2]
          coeffsUprime[0] = coeffsU[1]
          leCoef = 1
          multdiv = ''
        } else {
          // coeff multiplicateur
          alea = j3pGetRandomInt(0, 1)
          if (alea === 0) {
            // coef>1
            leCoef = j3pGetRandomInt(2, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
              // cas où il nous faut u>0 sur l’intervalle... pour simplifier je prends donc a<0 et u(borneInf)>0 et u(borneSup)>0
              do {
                coeffsU[2] = j3pGetRandomInt(-7, -1)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[2] = j3pGetRandomInt(-7, 7)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                // cas particulier pour exp, pour eviter u bug avec mtg32 - pas d’ecriture scientifique - pas plus d’exp(40)'
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }

            // de la forme lambda*u'(x)*exp(u) avec lambda=leCoeff et u'(x)=2ax+b
            coeffsUprime[1] = leCoef * 2 * coeffsU[2]
            coeffsUprime[0] = leCoef * coeffsU[1]
            multdiv = 'mult'
          } else {
            // coeff<1
            leCoef = j3pGetRandomInt(2, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
              // cas où il nous faut u>0 sur l’intervalle... pour simplifier je prends donc a<0 et u(borneInf)>0 et u(borneSup)>0
              do {
                coeffsU[2] = j3pGetRandomInt(-7, -1)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[1] % leCoef !== 0 || (2 * coeffsU[2]) % leCoef !== 0 || coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[2] = j3pGetRandomInt(-7, 7)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                // cas particulier pour exp, pour eviter u bug avec mtg32 - pas d’ecriture scientifique - pas plus d’exp(40)'
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || coeffsU[1] % leCoef !== 0 || (2 * coeffsU[2]) % leCoef !== 0 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }
            // de la forme u'(x)*exp(u)/lambda avec lambda=leCoeff et u'(x)=2ax+b
            coeffsUprime[0] = coeffsU[1] / leCoef
            coeffsUprime[1] = 2 * coeffsU[2] / leCoef
            multdiv = 'div'
          }
        }
        break
    }
    stor.etapes = []
    stor.phrase = []

    // uprime/racine(u) tabCoefsU=6,3,-1 et tabCoefsUprime=6,-4 et coeffMultiplicateur=2 et multOuDiv=mult
    /* typeFormule = 2
    degreDeU = 2
    coeffsU = [6, 3, -1]
    multdiv = 'mult'
    leCoef = 2
    coeffsUprime = [6, -4]
    laBorneInf = -1
    laBorneSup = 2 */

    switch (ds.formules[typeFormule]) {
      case 'uprimeexp(u)':
        // pas de condition particulière sur les bornes
        tirageCalculUprimeExpu(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup)
        break

      case 'uprime/u':
        // condition sur les bornes : il faut que u>0 sur l’intervalle :-(
        tirageCalculUprimeU(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup)
        break

      case 'uprime/racine(u)':
        tirageCalculUprimeRacineU(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup)
        break

      case 'uprime*u^n':
        // tirageCalculUprimeRacineU(coeffsU,coeffsUprime,leCoef,multdiv,laBorneInf,laBorneSup);
        tirageCalculUprimeUn(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup, j3pGetRandomInt(2, 4))
        break
    }
    // Modifié par Yves pour méméoriser les bornes
    stor.laBorneInf = laBorneInf
    stor.laBorneSup = laBorneSup
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { paddingTop: debuthaut + 'px', paddingLeft: '20px' }) })
    // j3pDiv(me.zonesElts.MG, { id: 'Enonce', contenu: '', style: me.styles.etendre('petit.enonce', { paddingTop: debuthaut + 'px', paddingLeft: '20px' }) })
    stor.zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    stor.zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
    j3pAffiche(stor.zoneCons1, '', ds.textes.question, { a: laBorneInf, b: laBorneSup })
    j3pAffiche(stor.zoneCons2, '', '$f(x)=' + stor.operation1 + '$')
    const elt = j3pAffiche(stor.zoneCons3, '', ds.textes.enonce + '&1&',
      {
        inputmq1: { texte: '' }
      }
    )
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.\\-/^+*x()', { commandes: ['racine', 'exp', 'fraction', 'puissance', 'ln'] })

    stor.laPalette = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '15px', paddingBottom: '35px' } })
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['racine', 'exp', 'fraction', 'puissance', 'ln'] })
    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
    j3pFocus(stor.zoneInput)
    me.finEnonce()
  }
  function depart () {
    // Une liste qui servira pour les calculs.
    stor.listePourCalc = stor.mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAPZAAACpQAAAQEAAAAAAAAAAQAAABH#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABYwABMAAAAAEAAAAAAAAAAAAAAAIA#####wABZAABMAAAAAEAAAAAAAAAAAAAAAIA#####wABYQABMAAAAAEAAAAAAAAAAP####8AAAABAAVDRm9uYwD#####AAFmAAF4#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAABeAAAAAQA#####wABZwABMAAAAAEAAAAAAAAAAAABeAAAAAIA#####wACaTEAATAAAAABAAAAAAAAAAAAAAACAP####8AAmkyAAEwAAAAAQAAAAAAAAAA#####wAAAAEACENEZXJpdmVlAP####8AAmYnAAAABQAAAAYA#####wACZycAAAAGAAAAAgD#####AAJ4MQABMQAAAAE#8AAAAAAAAAAAAAIA#####wACeDIAATIAAAABQAAAAAAAAAAAAAACAP####8AAngzAAEzAAAAAUAIAAAAAAAAAAAABAD#####AAR6ZXJvABJhYnMoeCk8MC4wMDAwMDAwMDH#####AAAAAQAKQ09wZXJhdGlvbgQAAAADAAAAAAUAAAAAAAAAAT4RLgvoJtaVAAF4AAAAAgD#####AAR0ZXN0ADt6ZXJvKGYnKHgxKS1nJyh4MSkpJnplcm8oZicoeDIpLWcnKHgyKSkmemVybyhmJyh4MyktZycoeDMpKQAAAAcKAAAABwr#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAAAOAAAABwEAAAAIAAAACf####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAALAAAACAAAAAoAAAAJAAAACwAAAAgAAAAOAAAABwEAAAAIAAAACQAAAAkAAAAMAAAACAAAAAoAAAAJAAAADAAAAAgAAAAOAAAABwEAAAAIAAAACQAAAAkAAAANAAAACAAAAAoAAAAJAAAADQAAAAIA#####wABYgABMAAAAAEAAAAAAAAAAP###############w==')
  }
  function egaliteFormellePrimitive (expression1, expression2) {
    const liste = stor.listePourCalc
    const dif = stor.laBorneSup - stor.laBorneInf
    me.logIfDebug('stor.laBorneInf:', stor.laBorneInf, stor.laBorneSup)
    liste.giveFormula2('x1', stor.laBorneInf + '+' + String(dif) + '*0.121331')
    liste.giveFormula2('x2', stor.laBorneInf + '+' + String(dif) + '*0.213217')
    liste.giveFormula2('x3', stor.laBorneInf + '+' + String(dif) + '*0.330765')
    liste.giveFormula2('f', liste.traiteSignesMult(expression1))
    liste.giveFormula2('g', liste.traiteSignesMult(expression2))
    liste.calculateNG()
    return liste.valueOf('test') === 1
  }

  function macorrection (bonneReponse) {
    j3pDetruit(stor.laPalette)
    const unechaine = stor.phrase[0]
    const explications = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    j3pAffiche(explications, '', unechaine, { a: stor.phrase[1], p: stor.etapes[0] })
  }

  function tirageCalculUprimeRacineU (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv) {
    let u1X, u1Prime, operation1, a, b // , exp1, exp2, chaine_temp1, chaine_temp2
    function definieCorrection () {
      if (coeffMultiplicateur === 1) {
        stor.etapes[0] = '2\\sqrt{' + u1X + '}'
        stor.phrase[0] = ds.textes.correction3_1
      } else {
        if (multOuDiv === 'div') {
          stor.etapes[0] = '\\frac{1}{' + coeffMultiplicateur + '}\\times2\\sqrt{' + u1X + '}'
          if (j3pPGCD(coeffMultiplicateur, 2) === 2) {
            // on peut simplifier par 2 :
            if (coeffMultiplicateur === 2) {
              stor.etapes[0] += '=\\sqrt{' + u1X + '}'
            } else {
              stor.etapes[0] += '=\\frac{1}{' + (coeffMultiplicateur / 2) + '}\\times\\sqrt{' + u1X + '}'
            }
          } else {
            stor.etapes[0] += '=\\frac{2}{' + coeffMultiplicateur + '}\\sqrt{' + u1X + '}'
          }
          stor.phrase[0] = ds.textes.correction3_2
          stor.phrase[1] = coeffMultiplicateur
        } else {
          stor.etapes[0] = coeffMultiplicateur + '\\times 2\\sqrt{' + u1X + '}'
          stor.etapes[0] += '=' + (coeffMultiplicateur * 2) + '\\sqrt{' + u1X + '}'
          stor.phrase[0] = ds.textes.correction3_3
          stor.phrase[1] = coeffMultiplicateur
        }
      }
    }

    me.logIfDebug('uprime/racine(u) ' + 'tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv)
    if (tabCoefsU.length === 2) {
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      u1X = j3pGetLatexMonome(1, 1, a) + j3pGetLatexMonome(2, 0, b)
      u1Prime = tabCoefsUprime[0]
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\frac{' + u1Prime + '}{\\sqrt{' + u1X + '}}'
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      definieCorrection()
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      stor.operation1 = operation1
    } else {
      const c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      u1X = j3pGetLatexMonome(1, 2, a) + j3pGetLatexMonome(2, 1, b) + j3pGetLatexMonome(3, 0, c)
      u1Prime = j3pGetLatexMonome(1, 1, tabCoefsUprime[1]) + j3pGetLatexMonome(2, 0, tabCoefsUprime[0])
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\frac{' + u1Prime + '}{\\sqrt{' + u1X + '}}'
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      stor.operation1 = operation1
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      definieCorrection()
    }
    if (coeffMultiplicateur === 1) {
      me.storage.solution[2] = '2*sqrt(' + u1X + ')'
    } else {
      if (multOuDiv === 'div') {
        me.storage.solution[2] = '2*sqrt(' + u1X + ')/' + coeffMultiplicateur
      } else {
        me.storage.solution[2] = coeffMultiplicateur + '*2*sqrt(' + u1X + ')'
      }
    }

    me.logIfDebug('la primitive=' + me.storage.solution[2])
  }

  function tirageCalculUprimeUn (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv, borneInf, borneSup, n) {
    let u1X, u1Prime, operation1, a, b// , exp1, exp2, chaine_temp1, chaine_temp2
    function definieCorrection () {
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      if (coeffMultiplicateur === 1) {
        stor.etapes[0] = '\\frac{(' + u1X + ')^' + (n + 1) + '}{' + (n + 1) + '}'
        stor.phrase[0] = ds.textes.correction4_1
        // la solution attendue :
      } else {
        if (multOuDiv === 'div') {
          stor.etapes[0] = '\\frac{1}{' + coeffMultiplicateur + '}\\times \\frac{(' + u1X + ')^' + (n + 1) + '}{' + (n + 1) + '}'
          stor.etapes[0] += '=\\frac{1}{' + (coeffMultiplicateur * (n + 1)) + '}\\times(' + u1X + ')^' + (n + 1)
          stor.phrase[0] = ds.textes.correction4_2
          stor.phrase[1] = coeffMultiplicateur
        } else {
          stor.etapes[0] = coeffMultiplicateur + '\\times \\frac{(' + u1X + ')^' + (n + 1) + '}{' + (n + 1) + '}'
          if (j3pPGCD(coeffMultiplicateur, n + 1) !== 1) {
            if (coeffMultiplicateur === n + 1) {
              stor.etapes[0] += '=(' + u1X + ')^' + (n + 1) + ''
            } else {
              stor.etapes[0] += '=\\frac{' + coeffMultiplicateur / j3pPGCD(coeffMultiplicateur, n + 1) + '}{' + (n + 1) / j3pPGCD(coeffMultiplicateur, n + 1) + '}\\times (' + u1X + ')^' + (n + 1)
            }
          }
          stor.phrase[0] = ds.textes.correction4_3
          stor.phrase[1] = coeffMultiplicateur
        }
      }
    }
    // console.log('tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv + ' et n=' + n)
    if (tabCoefsU.length === 2) {
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      u1X = j3pGetLatexMonome(1, 1, a) + j3pGetLatexMonome(2, 0, b)
      u1Prime = tabCoefsUprime[0]
      // un autre cas particulier :
      if (tabCoefsUprime[0] === 1) {
        u1Prime = ''
      }
      // A différencier suivant la fonction à intégrer:
      operation1 = u1Prime + '(' + u1X + ')^' + n
    } else {
      const c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      u1X = j3pGetLatexMonome(1, 2, a) + j3pGetLatexMonome(2, 1, b) + j3pGetLatexMonome(3, 0, c)
      u1Prime = j3pGetLatexMonome(1, 1, tabCoefsUprime[1]) + j3pGetLatexMonome(2, 0, tabCoefsUprime[0])
      // A différencier suivant la fonction à intégrer:
      operation1 = '(' + u1Prime + ')\\times (' + u1X + ')^' + n
    }
    definieCorrection()
    // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
    stor.operation1 = operation1
    if (coeffMultiplicateur === 1) {
      me.storage.solution[2] = '(' + u1X + ')^' + (n + 1) + '/' + (n + 1)
    } else {
      if (multOuDiv === 'div') {
        me.storage.solution[2] = '((' + u1X + ')^' + (n + 1) + ')/(' + coeffMultiplicateur + '*(' + (n + 1) + '))'
      } else {
        me.storage.solution[2] = coeffMultiplicateur + '*(' + u1X + ')^' + (n + 1) + '/(' + (n + 1) + ')'
      }
    }

    me.logIfDebug('la primitive=' + me.storage.solution[2])
    me.logIfDebug('l’opération : ' + operation1)
  }

  function tirageCalculUprimeU (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv) {
    let u1X, u1Prime, operation1, a, b, c//, exp1, exp2, chaine_temp1, chaine_temp2
    function definieCorrection () {
      if (coeffMultiplicateur === 1) {
        stor.etapes[0] = '\\ln(' + u1X + ')'
        stor.phrase[0] = ds.textes.correction2_1
      } else {
        if (multOuDiv === 'div') {
          stor.etapes[0] = '\\frac{\\ln(' + u1X + ')}{' + coeffMultiplicateur + '}'
          stor.phrase[0] = ds.textes.correction2_2
          stor.phrase[1] = coeffMultiplicateur
        } else {
          stor.etapes[0] = coeffMultiplicateur + '\\times \\ln(' + u1X + ')'
          stor.phrase[0] = ds.textes.correction2_3
          stor.phrase[1] = coeffMultiplicateur
        }
      }
    }

    me.logIfDebug('uprime/u ' + 'tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv)
    if (tabCoefsU.length === 2) {
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      u1X = j3pGetLatexMonome(1, 1, a) + j3pGetLatexMonome(2, 0, b)
      u1Prime = tabCoefsUprime[0]
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\frac{' + u1Prime + '}{' + u1X + '}'
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      definieCorrection()
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      stor.operation1 = operation1
    } else {
      c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      u1X = j3pGetLatexMonome(1, 2, a) + j3pGetLatexMonome(2, 1, b) + j3pGetLatexMonome(3, 0, c)
      u1Prime = j3pGetLatexMonome(1, 1, tabCoefsUprime[1]) + j3pGetLatexMonome(2, 0, tabCoefsUprime[0])
      // A différencier suivant la fonction à intégrer:
      operation1 = '\\frac{' + u1Prime + '}{' + u1X + '}'
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      stor.operation1 = operation1
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      definieCorrection()
    }
    if (coeffMultiplicateur === 1) {
      me.storage.solution[2] = 'ln(' + u1X + ')'
    } else {
      if (multOuDiv === 'div') {
        me.storage.solution[2] = '(ln(' + u1X + '))/' + coeffMultiplicateur
      } else {
        me.storage.solution[2] = coeffMultiplicateur + '*ln(' + u1X + ')'
      }
    }
    me.logIfDebug(' la primitive=' + me.storage.solution[2])
  }

  function tirageCalculUprimeExpu (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv) {
    let u1X, u1Prime, operation1, a, b//, exp1, exp2, chaine_temp1, chaine_temp2
    me.logIfDebug('tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv)

    if (tabCoefsU.length === 2) {
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      u1X = j3pGetLatexMonome(1, 1, a) + j3pGetLatexMonome(2, 0, b)
      if (tabCoefsUprime[0] === -1) {
        u1Prime = '-'
      } else {
        u1Prime = tabCoefsUprime[0]
      }
      // un autre cas particulier :
      if (tabCoefsUprime[0] === 1) {
        u1Prime = ''
      }
      // A différencier suivant la fonction à intégrer:
      operation1 = u1Prime + '\\mathrm{e}^{' + u1X + '}'
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      if (coeffMultiplicateur === 1) {
        stor.etapes[0] = '\\mathrm{e}^{' + u1X + '}'
        stor.phrase[0] = ds.textes.correction1_1
      } else {
        if (multOuDiv === 'div') {
          stor.etapes[0] = '\\frac{\\mathrm{e}^{' + u1X + '}}{' + coeffMultiplicateur + '}'
          stor.phrase[0] = ds.textes.correction1_2
          stor.phrase[1] = coeffMultiplicateur
        } else {
          stor.etapes[0] = coeffMultiplicateur + '\\times \\mathrm{e}^{' + u1X + '}'
          stor.phrase[0] = ds.textes.correction1_3
          stor.phrase[1] = coeffMultiplicateur
        }
      }
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      stor.operation1 = operation1
    } else {
      const c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      u1X = j3pGetLatexMonome(1, 2, a) + j3pGetLatexMonome(2, 1, b) + j3pGetLatexMonome(3, 0, c)
      u1Prime = j3pGetLatexMonome(1, 1, tabCoefsUprime[1]) + j3pGetLatexMonome(2, 0, tabCoefsUprime[0])
      // A différencier suivant la fonction à intégrer:
      operation1 = '(' + u1Prime + ')\\mathrm{e}^{' + u1X + '}'
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      if (coeffMultiplicateur === 1) {
        stor.etapes[0] = '\\mathrm{e}^{' + u1X + '}'
        stor.phrase[0] = ds.textes.correction1_1
      } else {
        if (multOuDiv === 'div') {
          stor.etapes[0] = '\\frac{\\mathrm{e}^{' + u1X + '}}{' + coeffMultiplicateur + '}'
          stor.phrase[0] = ds.textes.correction1_2
          stor.phrase[1] = coeffMultiplicateur
        } else {
          stor.etapes[0] = coeffMultiplicateur + '\\times \\mathrm{e}^{' + u1X + '}'
          stor.phrase[0] = ds.textes.correction1_3
          stor.phrase[1] = coeffMultiplicateur
        }
      }
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      stor.operation1 = operation1
    }
    if (coeffMultiplicateur === 1) {
      me.storage.solution[2] = 'exp(' + u1X + ')'
    } else {
      if (multOuDiv === 'div') {
        me.storage.solution[2] = '(exp(' + u1X + '))/' + coeffMultiplicateur
      } else {
        me.storage.solution[2] = coeffMultiplicateur + '*exp(' + u1X + ')'
      }
    }
    me.logIfDebug('la primitive=' + me.storage.solution[2])
  }

  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 1,
      nbetapes: 1,
      indication: '',
      // surchargeindication: false;  pour le cas de presentation1bis
      nbchances: 2,
      formules: ['uprimeexp(u)', 'uprime/u', 'uprime/racine(u)', 'uprime*u^n'],
      coeff: '[1;1]',
      // [1;1] (affine), [2;2] (deg2) ou [1;2] (alea)
      degre_u: '[1;2]',
      // rend caduque le paramètre précédent : (pas encore implémenté)
      u_avec_exp: false,
      u_avec_trig: false,
      // interversion possible de l’ordre croissant des bornes: (pas implémenté, pas forcément pertinent à cause du pb mq si borne négative)
      // inversion_bornes=false;
      structure: 'presentation2', //  || "presentation2" || "presentation3"  || "presentation1bis"
      // limite=5;
      textes: {
        titre_exo: 'Calculs de primitives',
        question: 'Détermine une primitive sur l’intervalle [$£a$;$£b$] de la fonction suivante :',
        enonce: 'Primitive : fonction $F$ définie par $F(x)=$',
        correction1_1: "La fonction $f$ est de la forme $u'\\times e^{u}$, une primitive est donc $e^{u}$,<br> ainsi $F(x)=£p$",
        correction1_2: "La fonction $f$ est de la forme $\\frac{u'\\times e^{u}}{£a}$, une primitive est donc $\\frac{e^{u}}{£a}$,<br> ainsi $F(x)=£p$",
        correction1_3: "La fonction $f$ est de la forme $£a \\times u'\\times e^{u}$, une primitive est donc $£a \\times e^{u}$,<br> ainsi $F(x)=£p$",
        correction2_1: "La fonction $f$ est de la forme $\\frac{u'}{u}$ avec $u>0$ sur l’intervalle, une primitive est donc $\\ln(u)$,<br> ainsi $F(x)=£p$",
        correction2_2: "La fonction $f$ est de la forme $\\frac{1}{£a}\\times \\frac{u'}{u}$ avec $u>0$ sur l’intervalle, une primitive est donc $\\frac{\\ln(u)}{£a}$,<br> ainsi $F(x)=£p$",
        correction2_3: "La fonction $f$ est de la forme $£a \\times \\frac{u'}{u}$ avec $u>0$ sur l’intervalle, une primitive est donc $£a \\times \\ln(u)$,<br> ainsi $F(x)=£p$",
        correction3_1: "La fonction $f$ est de la forme $\\frac{u'}{\\sqrt{u}}$ avec $u>0$ sur l’intervalle, une primitive est donc $2\\sqrt{u}$,<br> ainsi $F(x)=£p$",
        correction3_2: "La fonction $f$ est de la forme $\\frac{1}{£a}\\times \\frac{u'}{\\sqrt{u}}$ avec $u>0$ sur l’intervalle, une primitive est donc $\\frac{1}{£a}\\times 2\\sqrt{u}$,<br> ainsi $F(x)=£p$",
        correction3_3: "La fonction $f$ est de la forme $£a \\times \\frac{u'}{\\sqrt{u}}$ avec $u>0$ sur l’intervalle, une primitive est donc $£a \\times 2\\sqrt{u}$,<br> ainsi $F(x)=£p$",
        correction4_1: "La fonction $f$ est de la forme $u'\\times u^n$, une primitive est donc $\\frac{u^{n+1}}{n+1}$,<br> ainsi $F(x)=£p$",
        correction4_2: "La fonction $f$ est de la forme $\\frac{1}{£a}\\times u'\\times u^n$, une primitive est donc $\\frac{1}{£a}\\times \\frac{u^{n+1}}{n+1}$,<br> ainsi $F(x)=£p$",
        correction4_3: "La fonction $f$ est de la forme $£a \\times u'\\times u^n$, une primitive est donc $£a \\times \\frac{u^{n+1}}{n+1}$,<br> ainsi $F(x)=£p$",
        correction: 'Étudie bien la correction :',
        phrase_correction: 'La primitive est fausse.'
      }
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
    me.typederreurs = [0, 0, 0, 0, 0, 0]
    me.afficheTitre(ds.textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
    getMtgCore({ withMathjax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
      )
      .catch(j3pShowError)
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      {
        stor.zoneCorr.innerHTML = ''
        $(stor.zoneInput).blur()

        // on teste si une réponse a été saisie

        const repElevePrimitive = $(stor.zoneInput).mathquill('latex')
        if ((repElevePrimitive === '') && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
        } else { // une réponse a été saisie
          const repEleveNettoyeePrim = j3pMathquillXcas(repElevePrimitive)
          // console.log('repEleve_xcas_prim=' + repEleveNettoyeePrim)
          const reponseAnalysee2 = egaliteFormellePrimitive(repEleveNettoyeePrim, me.storage.solution[2])
          // console.log('reponseAnalysee2=' + reponseAnalysee2)
          if (reponseAnalysee2) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            stor.zoneCorr.innerHTML += '<br>'
            stor.zoneCorr.innerHTML += ds.textes.correction
            // A MODIFIER
            macorrection(true)
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            me.typederreurs[0]++
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              // me.typederreurs[10]++;
              // A MODIFIER
              macorrection(false)
              stor.zoneInput.style.color = me.styles.faux
              j3pDesactive(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)
              // je classe cette erreur comme 'autre'
              me.typederreurs[10]++
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneInput.style.color = me.styles.cfaux
                $(stor.zoneInput).focus()
                stor.zoneCorr.innerHTML = ds.textes.phrase_correction
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                // j3pFocus("affiche1input1");
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                stor.zoneCorr.innerHTML = cFaux
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // A MODIFIER
                macorrection(false)
                stor.zoneInput.style.color = me.styles.faux
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                j3pFreezeElt(stor.zoneInput)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
