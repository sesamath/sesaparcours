import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pDetruit, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pPaletteMathquill, j3pPGCD, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// section fortement paramétrable sur le calcul d’intégrales en TS : (juste les crochets)
// intégrale entre a et b de f(x) où f(x)=
// a) u’exp(u)
// b) uprime/u
// c) u'/racine(u)
// avec ou non coefficient multiplicateur
// avec ou non inversion de l’ordre de a et de b
// avec u affine ou de degré 2.
// code passablement compliqué car issu de la section 750 plus complète (code non oprtimisé ici...)
// Dév : Alexis Lecomte / avril 2013

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['formules', ['uprimeexp(u)', 'uprime/u', 'uprime/racine(u)', 'uprime*u^n'], 'array', 'tableau des formules possibles'],
    ['coeff', '[0;1]', 'string', '[0;0](sans coeff multiplicateur) [1;1](avec) ou [0;1](aléatoire)'],
    ['degre_u', '[1;2]', 'string', ' [1;1] (affine), [2;2] (deg2) ou [1;2] (alea)']
  ]

}

/**
 * section 753
 * @this {Parcours}
 */
/**
 * section 753
 * @this {Parcours}
 */
export default function main () {
  const me = this
  // cette fonction (notre section)
  const stor = me.storage
  let ds = me.donneesSection

  // Fonction qui retourne a/b simplifiée au format latex, sous forme d’entier si besoin
  function simplifieFrac (a, b) {
    let expression
    if (a % b === 0) {
      expression = a / b + ''
    } else {
      const numerateur = a / j3pPGCD(Math.abs(a), Math.abs(b))
      const denominateur = b / j3pPGCD(Math.abs(a), Math.abs(b))
      expression = '\\frac{' + numerateur + '}{' + denominateur + '}'
    }
    return expression
  }

  function macorrection (objetReponseEleve) {
    j3pDetruit(stor.laPalette)
    if (objetReponseEleve.simplifie)me.typederreurs[1]++
    const expl1 = j3pAddElt(stor.zoneExpli, 'div')
    j3pAffiche(expl1, '', '$' + me.stockage[2][0] + '$')
  }

  function mathquillMtg32 (chaine) {
    // console.log('chaine dans mathquillMtg32=' + chaine)

    function accoladeFermante (chaine, index) {
      // fonction qui donne la position de l’accolade fermante correspondant à l’accolade ouvrante positionnée en index de la chaine
      // penser à une condition d’arrêt pour ne pas faire planter le truc si la saisie est mal écrite
      // (ptet au debut de conversion mathquill : même nb d’accolades ouvrantes que fermantes)
      // pour l’instant 1 accolade ouvrante (à l’appel de la fonction)
      let indexaccolade = 1
      // A CORRIGER SI text{}
      let indexvariable = index
      while (indexaccolade !== 0) {
        // je peux avoir des accolades internes (par groupe de 2 necessairement)
        indexvariable++

        if (chaine.charAt(indexvariable) === '{') {
          indexaccolade++
        }
        if (chaine.charAt(indexvariable) === '}') {
          indexaccolade--
        }
      }
      return indexvariable
    }

    function necessiteFois (chaine, index) {
      // fonction qui renvoit "*" si à l’index courant on a un chiffre ou i (renvoit une chaine vide sinon)
      // fonction utile à la conversion d’une chaine mathquill (pour laquelle e^{2i\pi} est compréhensible) vers xcas (qui ne comprend pas e^(2i(PI) alors que e^i(PI) oui...)
      let texte = ''
      if (chaine.charAt(index) === '0' || chaine.charAt(index) === '1' || chaine.charAt(index) === '2' || chaine.charAt(index) === '3' || chaine.charAt(index) === '4' || chaine.charAt(index) === '5' || chaine.charAt(index) === '6' || chaine.charAt(index) === '7' || chaine.charAt(index) === '8' || chaine.charAt(index) === '9' || chaine.charAt(index) === 'i') {
        texte = '*'
      }
      return texte
    }

    function estParentheseFermante (chaine, index) {
      return (chaine.charAt(index) === ')')
    }

    function estParentheseOuvrante (chaine, index) {
      return (chaine.charAt(index) === '(')
    }

    let chaineMtg32 = chaine
    let pos
    let i
    // je vire d’abord les espaces :
    while (chaineMtg32.includes(':')) {
      pos = chaineMtg32.indexOf(':')
      chaineMtg32 = chaineMtg32.substring(0, pos - 1) + chaineMtg32.substring(pos + 1)
    }
    // je vire un éventuel signe multiplié
    chaineMtg32 = chaineMtg32.replace(/\\times/g, '*')
    while (chaineMtg32.includes('\\cdot')) {
      pos = chaineMtg32.indexOf('\\cdot')
      chaineMtg32 = chaineMtg32.substring(0, pos) + '*' + chaineMtg32.substring(pos + 5)
    }
    // je vire un espace inutile parfois présent (cad qu’on trouve dans le code mathquill \cdot  avec ou sans espace juste après
    while (chaineMtg32.includes(' ')) {
      pos = chaineMtg32.indexOf(' ')
      chaineMtg32 = chaineMtg32.substring(0, pos) + chaineMtg32.substring(pos + 1)
    }

    // je transforme la virgule (possible avec Mathquill) en point :
    while (chaineMtg32.includes(',')) {
      pos = chaineMtg32.indexOf(',')
      chaineMtg32 = chaineMtg32.substring(0, pos) + '.' + chaineMtg32.substring(pos + 1)
    }
    /* while(chaineMtg32.indexOf('\\text{')!=-1){
          var pos =chaineMtg32.indexOf('\\text{')
      } */
    // si le mot clé \frac est présent, on l’enlève et on convertit \frac{A}{B} en A/B
    // CODE QUI SUIT PEUT ETRE MIS DANS UNE FONCTION RECURSIVE (en remplaçant while par if) : si tous les if ne donnent rien, cette fonction renvoit le string passé en paramètre
    let pos2, pos3, mult
    while (chaineMtg32.includes('\\frac')) {
      // il y a une fraction
      pos = chaineMtg32.indexOf('\\frac')
      // on vire le frac, pour 2+\frac{3}{4}, il reste ensuite 2+{3}{4}
      chaineMtg32 = chaineMtg32.substring(0, pos) + chaineMtg32.substring(pos + 5)
      // je detecte la fin du numérateur pour extraire A :
      pos2 = accoladeFermante(chaineMtg32, pos)
      const chaineMtg32A = chaineMtg32.substring(pos + 1, pos2)
      // pareil pour B :
      pos3 = accoladeFermante(chaineMtg32, pos2 + 1)
      const chaineMtg32B = chaineMtg32.substring(pos2 + 2, pos3)
      chaineMtg32 = chaineMtg32.substring(0, pos) + '(' + chaineMtg32A + ')/(' + chaineMtg32B + ')' + chaineMtg32.substring(pos3 + 1)
    }
    while (chaineMtg32.includes('\\sqrt')) {
      // il y a une racine, il faut remplacer \sqrt{A} par sqrt(A)
      // on teste aussi s’il faut un * avant car xcas ne comprend pas 2sqrt(3)
      pos = chaineMtg32.indexOf('\\sqrt')
      pos2 = pos + 4
      if (necessiteFois(chaineMtg32, pos - 1) === '*' || estParentheseFermante(chaineMtg32, pos - 1)) {
        mult = '*'
        pos2++
      } else {
        mult = ''
      }
      // je vire le \ en ajoutant le signe * si besoin
      chaineMtg32 = chaineMtg32.substring(0, pos) + mult + chaineMtg32.substring(pos + 1)
      // je remplace { par (
      chaineMtg32 = chaineMtg32.substring(0, pos2) + '(' + chaineMtg32.substring(pos2 + 1)
      // je detecte la fin de ce qu’il y a dans la racine'
      pos3 = accoladeFermante(chaineMtg32, pos2)
      chaineMtg32 = chaineMtg32.substring(0, pos3) + ')' + chaineMtg32.substring(pos3 + 1)
    }

    // A VIRER AUSSI : \left et \right pour les parenthèses
    while (chaineMtg32.includes('\\left')) {
      pos = chaineMtg32.indexOf('\\left')
      if (necessiteFois(chaineMtg32, pos - 1) === '*' || estParentheseFermante(chaineMtg32, pos - 1)) {
        mult = '*'
      } else {
        mult = ''
      }
      chaineMtg32 = chaineMtg32.substring(0, pos) + mult + chaineMtg32.substring(pos + 5)
    }
    while (chaineMtg32.includes('\\right')) {
      pos = chaineMtg32.indexOf('\\right')
      chaineMtg32 = chaineMtg32.substring(0, pos) + chaineMtg32.substring(pos + 6)
    }

    let mult2
    // un soucis ici : si un seul terme dans l’exponentielle, MQ ne met pas d’accolade donc il faut refaire le test avec 'e^' pour savoir si necessite fois...
    while (chaineMtg32.includes('e^{')) {
      // il y a un exponentielle, il faut remplacer e^{A} par e^(A) + test si signe * necessaire
      pos = chaineMtg32.indexOf('e^{')
      pos2 = accoladeFermante(chaineMtg32, pos + 2)
      if (necessiteFois(chaineMtg32, pos - 1) === '*' || estParentheseFermante(chaineMtg32, pos - 1)) {
        mult = '*'
      } else {
        mult = ''
      }
      if (necessiteFois(chaineMtg32, pos2 + 1) === '*' || estParentheseOuvrante(chaineMtg32, pos2 + 1)) {
        mult2 = '*'
      } else {
        mult2 = ''
      }
      chaineMtg32 = chaineMtg32.substring(0, pos) + mult + chaineMtg32.substring(pos, pos + 1) + 'xp(' + chaineMtg32.substring(pos + 3, pos2) + ')' + mult2 + chaineMtg32.substring(pos2 + 1)
      // console.log('chaine xcas exp=' + chaineMtg32)
    }
    // je traite le cas particulier :
    for (i = 0; i < chaineMtg32.length; i++) {
      if (chaineMtg32.charAt(i) === 'e' && chaineMtg32.charAt(i + 1) === '^') {
        if (necessiteFois(chaineMtg32, i - 1) === '*' || estParentheseFermante(chaineMtg32, i - 1)) {
          mult = '*'
        } else {
          mult = ''
        }
        chaineMtg32 = chaineMtg32.substring(0, i) + mult + 'exp(' + chaineMtg32.charAt(i + 2) + ')' + chaineMtg32.substring(i + 3)
        i = i + 3
      }
    }
    // j’autorise la saisie en direct de exp().. du coup il faut tester s’il faut rajouter un signe fois avant :
    for (i = 0; i < chaineMtg32.length; i++) {
      if (chaineMtg32.substring(i, i + 3) === 'exp' && (necessiteFois(chaineMtg32, i - 1) === '*' || estParentheseFermante(chaineMtg32, i - 1))) {
        chaineMtg32 = chaineMtg32.substring(0, i) + '*' + chaineMtg32.substring(i)
        i++
      }
    }
    // maintenant il ne doit plus rester de e^{ ou de 'e' sans 'xp' derrière... sauf si l’élève a rentré le nombre e ! (à convertir en exp(1) pour mtg32
    for (i = 0; i < chaineMtg32.length; i++) {
      if (chaineMtg32.charAt(i) === 'e' && chaineMtg32.charAt(i + 1) !== 'x') {
        if (necessiteFois(chaineMtg32, i - 1) === '*' || estParentheseFermante(chaineMtg32, i - 1)) {
          mult = '*'
        } else {
          mult = ''
        }
        chaineMtg32 = chaineMtg32.substring(0, i) + mult + 'exp(1)' + chaineMtg32.substring(i + 1)
        i = i + 3
      }
    }

    while (chaineMtg32.includes('\\pi')) {
      pos = chaineMtg32.indexOf('\\pi')
      // je remplace le \ par (
      if (necessiteFois(chaineMtg32, pos - 1) === '*' || estParentheseFermante(chaineMtg32, pos - 1)) {
        mult = '*'
      } else {
        mult = ''
      }
      chaineMtg32 = chaineMtg32.substring(0, pos) + mult + '(PI)' + chaineMtg32.substring(pos + 3)
    }

    // pour gérer un pb  : (2+racine(3))i n’est pas compris par xcas, j’ajoute un fois...
    for (i = 0; i <= chaineMtg32.length; i++) {
      if (chaineMtg32.charAt(i) === 'i' && (necessiteFois(chaineMtg32, i - 1) === '*' || estParentheseFermante(chaineMtg32, i - 1))) {
        chaineMtg32 = chaineMtg32.substring(0, i) + '*' + 'i' + chaineMtg32.substring(i + 1)
        i++
      }
    }

    // même genre de pb  : i3 n’est pas compris par xcas, j’ajoute un fois...(mais pas à "i)" d’où la modif de code pour estParentheseFermante
    for (i = 0; i <= chaineMtg32.length; i++) {
      if (chaineMtg32.charAt(i) === 'i' && necessiteFois(chaineMtg32, i + 1) === '*') {
        chaineMtg32 = chaineMtg32.substring(0, i) + 'i' + '*' + chaineMtg32.substring(i + 1)
        i++
      }
    }
    while (chaineMtg32.includes('{')) {
      // reste à remplacer d’éventuelle accolades (puissances) par des parenthèses
      pos = chaineMtg32.indexOf('{')
      pos2 = pos + 1
      chaineMtg32 = chaineMtg32.substring(0, pos) + '(' + chaineMtg32.substring(pos + 1)
      // je detecte la fin de ce qu’il y a dans l’accolade
      pos3 = accoladeFermante(chaineMtg32, pos2)
      chaineMtg32 = chaineMtg32.substring(0, pos3) + ')' + chaineMtg32.substring(pos3 + 1)
    }
    chaineMtg32 = chaineMtg32.replace(/\\ln/g, 'ln')
    // il reste à gérer des écritures de la forme 2ln(3) pour mettre une multiplication devant ln
    chaineMtg32 = chaineMtg32.replace(/([0-9]|\))ln/g, '$1*ln')
    return chaineMtg32
  }

  function egaliteFormelleMtg32 (expr1, expr2) {
    const liste = stor.listePourCalc
    liste.giveFormula2('c', liste.traiteSignesMult(expr1))
    liste.giveFormula2('d', liste.traiteSignesMult(expr2))
    liste.calculateNG()
    const ecritureCorrecte = liste.verifieSyntaxe(expr2)
    if (!ecritureCorrecte) {
      return 1000
    } else {
      // console.log('=====expr1======' + calcul_expr1.rendValeur() + ' et expr2=' + calcul_expr2.rendValeur())
      const max = Math.max(Math.abs(liste.valueOf('c')), Math.abs(liste.valueOf('d')))
      liste.giveFormula2('a', '(' + liste.traiteSignesMult(expr1) + '-(' + liste.traiteSignesMult(expr2) + '))')
      liste.calculateNG()
      if (liste.valueOf('a') === 0) {
        return 0
      } else {
        liste.giveFormula2('b', 'a/' + max)
        liste.calculateNG()
        return liste.valueOf('b')
      }
    }
  }

  function analyseFormelleMtg32 (reponseEleve) {
    const retour = {}
    me.logIfDebug('stor.solution[1]=' + stor.solution[1] + ' et reponseEleve=' + reponseEleve)
    const valeurDifference = Math.abs(egaliteFormelleMtg32(stor.solution[1], reponseEleve))
    // console.log('valeurDifference:', valeurDifference)
    if (valeurDifference < Math.pow(10, -12)) {
      // console.log('JUSTE')
      retour.bon = true
    } else {
      // console.log('FAUX')
      retour.bon = false
    }
    // A FAIRE : adapter suivant la formule
    let nbE = 0
    for (let i = 0; i <= reponseEleve.length; i++) {
      if (reponseEleve.charAt(i) === 'e') {
        nbE++
      }
    }
    retour.simplifie = (nbE <= 2)
    me.logIfDebug('reponse finale exacte=' + retour.bon + ' et retour.simplifie=' + retour.simplifie)
    return retour
  }

  function tirageCalculUPrimeRacineU (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv, borneInf, borneSup) {
    function simplifieRac (n) {
      // fonction qui renvoie un tableau de deux valeurs, la première correspond au coeff devant la racine, la seconde est le nb sous le radical
      const facteur = []
      facteur[1] = 1
      facteur[2] = n
      for (let i = 2; i <= Math.floor(n / 2); i++) {
        const k = i * i
        while (true) {
          if (facteur[2] % k === 0) {
            facteur[1] *= k
            facteur[2] /= k
          } else {
            break
          }
        }
      }
      facteur[1] = Math.sqrt(facteur[1])
      return facteur
    }

    function definieCorrection () {
      let operation, entier1, racine1, entier2, racine2, facto, fact1, fact2, termine, leFact
      if (coeffMultiplicateur === 1) {
        me.stockage[2][0] = '[2\\sqrt{' + u1x + '}]_{' + borneInf + '}^{' + borneSup + '}='
        operation = '[2\\sqrt{' + u1x + '}]_{' + borneInf + '}^{' + borneSup + '}='
        me.stockage[2][0] += '2\\times' + chaineTemp1 + '-2\\times' + chaineTemp2
        // simplification éventuelle...je récupère les décompositions des racines
        entier1 = simplifieRac(Math.abs(exp1))[1]
        racine1 = simplifieRac(Math.abs(exp1))[2]
        entier2 = simplifieRac(Math.abs(exp2))[1]
        racine2 = simplifieRac(Math.abs(exp2))[2]
        if (racine1 === 1) {
          // carré parfait pour le premier
          if (racine2 === 1) {
            // et carré parfait pour le second !
            me.stockage[2][0] += '=' + (2 * (entier1 - entier2))
          } else {
            // juste carré parfait pour le premier
            me.stockage[2][0] += '=' + 2 * entier1 + '-' + (2 * entier2) + '\\sqrt{' + racine2 + '}'
          }
        } else {
          // le premier n’est pas un carré parfait :
          me.stockage[2][0] += '=' + (2 * entier1) + '\\sqrt{' + racine1 + '}'
          if (racine2 === 1) {
            // le second est un carré parfait
            me.stockage[2][0] += '-' + (2 * entier2)
          } else {
            // pas un carré parfait, on pourra au moins facotriser par 2
            me.stockage[2][0] += '-' + (2 * entier2) + '\\sqrt{' + racine2 + '}'
            facto = j3pPGCD(2 * entier1, 2 * entier2, { negativesAllowed: true })
            fact1 = ((2 * entier1) / facto)
            if (fact1 === 1) fact1 = ''
            fact2 = ((2 * entier2) / facto)
            if (fact2 === 1) fact2 = ''
            me.stockage[2][0] += '=' + facto + '(' + fact1 + '\\sqrt{' + racine1 + '}-' + fact2 + '\\sqrt{' + racine2 + '})'
            // on ajoute un test si c la même racine...
            termine = false
            if (racine1 === racine2) {
              if (fact1 === '') {
                if (fact2 === '') {
                  // la différence fera 0
                  me.stockage[2][0] += '=0'
                  termine = true
                } else {
                  leFact = 1 - fact2
                }
              } else {
                if (fact2 === '') {
                  leFact = fact1 - 1
                } else {
                  leFact = fact1 - fact2
                }
              }
              if (!termine) {
                me.stockage[2][0] += '=' + facto + '\\times' + (leFact) + '\\sqrt{' + racine1 + '}$=$' + (facto * leFact) + '\\sqrt{' + racine1 + '}'
              }
            }
          }
        }

        me.stockage[3][0] = ds.textes.correction
        // la solution attendue :
        stor.solution[1] = '2*(sqrt(' + exp1 + ')-sqrt(' + exp2 + '))'
      } else {
        if (multOuDiv === 'div') {
          // var operation="$[\\frac{1}{"+coeffMultiplicateur+"}\\times2\\sqrt{"+u1x+"}]_{"+borneInf+"}^{"+borneSup+"}$=";
          operation = '[' + simplifieFrac(2, coeffMultiplicateur) + '\\times\\sqrt{' + u1x + '}]_{' + borneInf + '}^{' + borneSup + '}='
          me.stockage[2][0] = operation
          me.stockage[2][0] += simplifieFrac(2, coeffMultiplicateur) + '\\times' + chaineTemp1 + '-' + simplifieFrac(2, coeffMultiplicateur) + '\\times' + chaineTemp2
          // simplification éventuelle...je récupère les décompositions des racines
          entier1 = simplifieRac(Math.abs(exp1))[1]
          racine1 = simplifieRac(Math.abs(exp1))[2]
          entier2 = simplifieRac(Math.abs(exp2))[1]
          racine2 = simplifieRac(Math.abs(exp2))[2]
          // A modifier : 3ème égalité à enlever si entier1==1 et entier2==1
          if (racine1 === 1) {
            // carré parfait pour le premier
            if (racine2 === 1) {
              // et carré parfait pour le second !
              me.stockage[2][0] += '=' + (simplifieFrac(2 * (entier1 - entier2), coeffMultiplicateur))
            } else {
              // juste carré parfait pour le premier
              let facteur = simplifieFrac(2 * entier2, coeffMultiplicateur)
              if (facteur === '1') {
                facteur = ''
              }
              me.stockage[2][0] += '=' + simplifieFrac(2 * entier1, coeffMultiplicateur) + '-' + facteur + '\\sqrt{' + racine2 + '}'
            }
          } else {
            // le premier n’est pas un carré parfait :
            if (racine2 === 1) {
              // le second est un carré parfait
              if (simplifieFrac(2 * entier1, coeffMultiplicateur) === '1') {
                me.stockage[2][0] += '=\\sqrt{' + racine1 + '}'
              } else {
                me.stockage[2][0] += '=' + simplifieFrac(2 * entier1, coeffMultiplicateur) + '\\sqrt{' + racine1 + '}'
              }
              me.stockage[2][0] += '-' + simplifieFrac(2 * entier2, coeffMultiplicateur) + ''
            } else {
              // pas un carré parfait, on pourra au moins factoriser par 2
              if (!(entier1 === 1 && entier2 === 1)) {
                me.stockage[2][0] += '=' + simplifieFrac(2 * entier1, coeffMultiplicateur) + '\\sqrt{' + racine1 + '}'
                me.stockage[2][0] += '-' + simplifieFrac(2 * entier2, coeffMultiplicateur) + '\\sqrt{' + racine2 + '}'
              }
              facto = j3pPGCD(2 * entier1, 2 * entier2, { negativesAllowed: true })
              fact1 = ((2 * entier1) / facto)
              if (fact1 === 1) {
                fact1 = ''
              }
              fact2 = ((2 * entier2) / facto)
              if (fact2 === 1) {
                fact2 = ''
              }
              if (simplifieFrac(facto, coeffMultiplicateur) === '1') {
                me.stockage[2][0] += '=' + fact1 + '\\sqrt{' + racine1 + '}-' + fact2 + '\\sqrt{' + racine2 + '}'
              } else {
                me.stockage[2][0] += '=' + simplifieFrac(facto, coeffMultiplicateur) + '(' + fact1 + '\\sqrt{' + racine1 + '}-' + fact2 + '\\sqrt{' + racine2 + '})'
              }
              // on ajoute un test si c la même racine...
              termine = false
              if (racine1 === racine2) {
                if (fact1 === '') {
                  if (fact2 === '') {
                    // la différence fera 0
                    me.stockage[2][0] += '=0'
                    termine = true
                  } else {
                    leFact = 1 - fact2
                  }
                } else {
                  if (fact2 === '') {
                    leFact = fact1 - 1
                  } else {
                    leFact = fact1 - fact2
                  }
                }
                if (!termine) {
                  if (simplifieFrac(facto * leFact, coeffMultiplicateur) === '1') {
                    me.stockage[2][0] += '=' + simplifieFrac(facto, coeffMultiplicateur) + '\\times' + (leFact) + '\\sqrt{' + racine1 + '}$=$\\sqrt{' + racine1 + '}'
                  } else {
                    me.stockage[2][0] += '=' + simplifieFrac(facto, coeffMultiplicateur) + '\\times' + (leFact) + '\\sqrt{' + racine1 + '}$=$' + (simplifieFrac(facto * leFact, coeffMultiplicateur)) + '\\sqrt{' + racine1 + '}'
                  }
                }
              }
            }
          }

          me.stockage[3][0] = ds.textes.correction
          me.stockage[3][1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = '2*(sqrt(' + exp1 + ')-sqrt(' + exp2 + '))/' + coeffMultiplicateur
        } else {
          operation = '[' + (2 * coeffMultiplicateur) + '\\times\\sqrt{' + u1x + '}]_{' + borneInf + '}^{' + borneSup + '}='
          me.stockage[2][0] = operation
          me.stockage[2][0] += (2 * coeffMultiplicateur) + '\\times' + chaineTemp1 + '-' + (2 * coeffMultiplicateur) + '\\times' + chaineTemp2
          // simplification éventuelle...je récupère les décompositions des racines
          entier1 = simplifieRac(Math.abs(exp1))[1]
          racine1 = simplifieRac(Math.abs(exp1))[2]
          entier2 = simplifieRac(Math.abs(exp2))[1]
          racine2 = simplifieRac(Math.abs(exp2))[2]
          // console.log('entier1=' + entier1 + ' et racine1=' + racine1)
          // console.log('entier2=' + entier2 + ' et racine2=' + racine2)
          if (racine1 === 1) {
            // carré parfait pour le premier
            if (racine2 === 1) {
              // et carré parfait pour le second !
              me.stockage[2][0] += '=' + (2 * coeffMultiplicateur * (entier1 - entier2))
            } else {
              // juste carré parfait pour le premier
              me.stockage[2][0] += '=' + 2 * coeffMultiplicateur * entier1 + '-' + (2 * coeffMultiplicateur * entier2) + '\\sqrt{' + racine2 + '}'
            }
          } else {
            // le premier n’est pas un carré parfait :
            if (racine2 === 1) {
              // le second est un carré parfait
              me.stockage[2][0] += '=' + (2 * coeffMultiplicateur * entier1) + '\\sqrt{' + racine1 + '}'
              me.stockage[2][0] += '-' + (2 * coeffMultiplicateur * entier2)
            } else {
              // pas un carré parfait, on pourra au moins facotriser par 2
              if (!(entier1 === 1 && entier2 === 1)) {
                me.stockage[2][0] += '=' + (2 * coeffMultiplicateur * entier1) + '\\sqrt{' + racine1 + '}'
                me.stockage[2][0] += '-' + (2 * coeffMultiplicateur * entier2) + '\\sqrt{' + racine2 + '}'
              }
              facto = j3pPGCD(2 * coeffMultiplicateur * entier1, 2 * coeffMultiplicateur * entier2, { negativesAllowed: true })
              fact1 = ((2 * coeffMultiplicateur * entier1) / facto)
              if (fact1 === 1) {
                fact1 = ''
              }
              fact2 = ((2 * coeffMultiplicateur * entier2) / facto)
              if (fact2 === 1) {
                fact2 = ''
              }
              me.stockage[2][0] += '=' + facto + '(' + fact1 + '\\sqrt{' + racine1 + '}-' + fact2 + '\\sqrt{' + racine2 + '})'
            }
          }
          me.stockage[3][0] = ds.textes.correction
          me.stockage[3][1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = coeffMultiplicateur + '*2*(sqrt(' + exp1 + ')-sqrt(' + exp2 + '))'
        }
      }
      return operation
    }
    // console.log('uprime/racine(u) ' + 'tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv)
    let a, b, c, u1x, exp1, exp2, chaineTemp1, chaineTemp2, operation1
    if (tabCoefsU.length === 2) {
      // console.log('degré 1 !! avec a<>1')
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      if (a === -1) {
        u1x = '-x+' + b
      } else {
        u1x = a + 'x+' + b
      }
      exp1 = a * borneSup + b
      exp2 = a * borneInf + b
      // les solutions finales
      chaineTemp1 = '\\sqrt{' + exp1 + '}'
      if (Math.round(Math.sqrt(exp1)) === Math.sqrt(exp1)) {
        // si entier
        chaineTemp1 = Math.sqrt(exp1)
      }
      chaineTemp2 = '\\sqrt{' + exp2 + '}'
      if (Math.round(Math.sqrt(exp2)) === Math.sqrt(exp2)) {
        // si entier
        chaineTemp2 = Math.sqrt(exp2)
      }
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      operation1 = definieCorrection()
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
    } else {
      // console.log('degré2 !! avec a<>1')
      c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      if (a === -1) {
        u1x = '-x^2+' + b + 'x+' + c
      } else {
        u1x = a + 'x^2+' + b + 'x+' + c
      }
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.

      exp1 = a * borneSup * borneSup + b * borneSup + c
      exp2 = a * borneInf * borneInf + b * borneInf + c
      // les solutions finales
      chaineTemp1 = '\\sqrt{' + exp1 + '}'
      if (Math.round(Math.sqrt(exp1)) === Math.sqrt(exp1)) {
        // si entier
        chaineTemp1 = Math.sqrt(exp1)
      }
      chaineTemp2 = '\\sqrt{' + exp2 + '}'
      if (Math.round(Math.sqrt(exp2)) === Math.sqrt(exp2)) {
        // si entier
        chaineTemp2 = Math.sqrt(exp2)
      }
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      operation1 = definieCorrection()
    }
    me.stockage[1] = '$' + operation1 + '$'
    if (coeffMultiplicateur === 1) {
      stor.solution[2] = '2*sqrt(' + u1x + ')'
    } else {
      if (multOuDiv === 'div') {
        stor.solution[2] = '2*sqrt(' + u1x + ')/' + coeffMultiplicateur
      } else {
        stor.solution[2] = coeffMultiplicateur + '*2*sqrt(' + u1x + ')'
      }
    }

    me.logIfDebug('la solution :' + stor.solution[1] + ' et la primitive=' + stor.solution[2])
  }

  function tirageCalculUPrimeUn (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv, borneInf, borneSup, n) {
    function gereParentheses (nb) {
      if (nb < 0) {
        return '(' + nb + ')'
      } else {
        return nb
      }
    }

    function definieCorrection () {
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      let operation, facteur
      if (coeffMultiplicateur === 1) {
        operation = '[ \\frac{(' + u1x + ')^' + (n + 1) + '}{' + (n + 1) + '}]_{' + borneInf + '}^{' + borneSup + '}='
        me.stockage[2][0] = '[ \\frac{(' + u1x + ')^' + (n + 1) + '}{' + (n + 1) + '}]_{' + borneInf + '}^{' + borneSup + '}='
        me.stockage[2][0] += chaineTemp1 + '-' + chaineTemp2
        if (exp1 === exp2) {
          me.stockage[2][0] += '=0'
        }
        me.stockage[3][0] = ds.textes.correction
        // la solution attendue :
        stor.solution[1] = '((' + exp1 + ')^(' + (n + 1) + ')-(' + exp2 + ')^(' + (n + 1) + '))/(' + (n + 1) + ')'
      } else {
        if (multOuDiv === 'div') {
          operation = '[\\frac{1}{' + (coeffMultiplicateur * (n + 1)) + '}\\times (' + u1x + ')^' + (n + 1) + ']_{' + borneInf + '}^{' + borneSup + '}='
          me.stockage[2][0] = operation
          me.stockage[2][0] += '\\frac{1}{' + coeffMultiplicateur + '}(' + chaineTemp1 + '-' + chaineTemp2 + ')'
          if (exp1 === exp2) {
            me.stockage[2][0] += '=0'
          } else {
            me.stockage[2][0] += '=\\frac{1}{' + ((n + 1) * coeffMultiplicateur) + '}\\left(' + gereParentheses(exp1) + '^' + (n + 1) + '-' + gereParentheses(exp2) + '^' + (n + 1) + '\\right)'
            me.stockage[2][0] += '=\\frac{1}{' + ((n + 1) * coeffMultiplicateur) + '}(' + (Math.pow(exp1, n + 1) - Math.pow(exp2, n + 1)) + ')'
            // A voir si pertinent...
            if (j3pPGCD((n + 1) * coeffMultiplicateur, ((exp1) ^ (n + 1) - (exp2) ^ (n + 1)), { negativesAllowed: true }) !== 1) {
              me.stockage[2][0] += '=' + simplifieFrac((Math.pow(exp1, n + 1) - Math.pow(exp2, n + 1)), ((n + 1) * coeffMultiplicateur)) + ''
            } else {
              me.stockage[2][0] += '=\\frac{' + (Math.pow(exp1, n + 1) - Math.pow(exp2, n + 1)) + '}{' + ((n + 1) * coeffMultiplicateur) + '}'
            }
            if (Math.abs(Math.pow(exp1, n + 1) - Math.pow(exp2, n + 1)) < Math.pow(10, -12)) me.stockage[2][0] += '=0'
          }
          me.stockage[3][0] = ds.textes.correction
          me.stockage[3][1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = '((' + exp1 + ')^(' + (n + 1) + ')-(' + exp2 + ')^(' + (n + 1) + '))/(' + (coeffMultiplicateur * (n + 1)) + ')'
        } else {
          operation = '[' + simplifieFrac(coeffMultiplicateur, (n + 1)) + '\\times (' + u1x + ')^' + (n + 1) + ']_{' + borneInf + '}^{' + borneSup + '}'
          me.stockage[2][0] = operation
          operation += '='
          facteur = simplifieFrac(coeffMultiplicateur, n + 1)
          // CAS PARTICULIER SI coeff=n+1
          if (coeffMultiplicateur === n + 1) {
            operation = '[(' + u1x + ')^' + (n + 1) + ']_{' + borneInf + '}^{' + borneSup + '}'
            me.stockage[2][0] = operation
            operation += '='
            facteur = ''
          }
          if (exp1 === exp2) {
            me.stockage[2][0] += '0'
          } else {
            if (n + 1 === coeffMultiplicateur) {
              me.stockage[2][0] += '=' + gereParentheses(exp1) + '^' + (n + 1) + '-' + gereParentheses(exp2) + '^' + (n + 1)
              if (exp1 === 0) {
                me.stockage[2][0] += '=-' + gereParentheses(exp2) + '^' + (n + 1)
              }
              if (exp2 === 0) {
                me.stockage[2][0] += '=' + gereParentheses(exp1) + '^' + (n + 1)
              }
            } else {
              me.stockage[2][0] += '=' + facteur + '\\left(' + gereParentheses(exp1) + '^' + (n + 1) + '-' + gereParentheses(exp2) + '^' + (n + 1) + '\\right)'
              if (exp1 === 0) {
                me.stockage[2][0] += '=-' + gereParentheses(exp2) + '^' + (n + 1) + ')'
              }
              if (exp2 === 0) {
                me.stockage[2][0] += '=' + facteur + '\\left(' + gereParentheses(exp1) + '^' + (n + 1) + '\\right)'
              }
            }
            if (Math.abs(Math.pow(exp1, n + 1) - Math.pow(exp2, n + 1)) < Math.pow(10, -12)) me.stockage[2][0] += '=0'
          }

          me.stockage[3][0] = ds.textes.correction
          me.stockage[3][1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = coeffMultiplicateur + '*((' + exp1 + ')^(' + (n + 1) + ')-(' + exp2 + ')^(' + (n + 1) + '))/(' + (n + 1) + ')'
        }
      }
      return operation
    }

    // console.log('tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv + ' et n=' + n)
    let a, b, u1x, exp1, exp2, c
    if (tabCoefsU.length === 2) {
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      if (a === -1) {
        u1x = '-x+' + b
      } else {
        u1x = a + 'x+' + b
      }
      exp1 = a * borneSup + b
      exp2 = a * borneInf + b
    } else {
      c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      if (a === -1) {
        u1x = '-x^2+' + b + 'x+' + c
      } else {
        u1x = a + 'x^2+' + b + 'x+' + c
      }
      // A différencier suivant la fonction à intégrer:
      exp1 = a * borneSup * borneSup + b * borneSup + c
      exp2 = a * borneInf * borneInf + b * borneInf + c
    }
    // les solutions finales
    let chaineTemp1, chaineTemp2
    if (exp1 < 0) {
      chaineTemp1 = '\\frac{(' + exp1 + ')^' + (n + 1) + '}{' + (n + 1) + '}'
    } else {
      chaineTemp1 = '\\frac{' + exp1 + '^' + (n + 1) + '}{' + (n + 1) + '}'
    }

    if (exp2 < 0) {
      chaineTemp2 = '\\frac{(' + exp2 + ')^' + (n + 1) + '}{' + (n + 1) + '}'
    } else {
      chaineTemp2 = '\\frac{' + exp2 + '^' + (n + 1) + '}{' + (n + 1) + '}'
    }
    const operation1 = definieCorrection()
    // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
    me.stockage[1] = '$' + operation1 + '$'
    if (coeffMultiplicateur === 1) {
      stor.solution[2] = '(' + u1x + ')^' + n
    } else {
      if (multOuDiv === 'div') {
        stor.solution[2] = '((' + u1x + ')^' + n + ')/' + coeffMultiplicateur
      } else {
        stor.solution[2] = coeffMultiplicateur + '*(' + u1x + ')^' + n
      }
    }

    me.logIfDebug('la solution :' + stor.solution[1] + ' et la primitive=' + stor.solution[2])
  }

  function tirageCalculUPrimeU (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv, borneInf, borneSup) {
    function definieCorrection () {
      let operation
      if (coeffMultiplicateur === 1) {
        operation = '[ \\mathrm{ln}(' + u1x + ')]_{' + borneInf + '}^{' + borneSup + '}='
        me.stockage[2][0] = operation
        if (exp2 !== 1) {
          me.stockage[2][0] += chaineTemp1 + '-' + chaineTemp2
        } else {
          if (exp1 === 1) {
            me.stockage[2][0] += '0'
          } else {
            me.stockage[2][0] += chaineTemp1
          }
        }
        // simplification éventuelle... pas si exp1==1
        if (exp1 !== 1 && exp2 !== 1) {
          me.stockage[2][0] += '=\\mathrm{ln}\\left(\\frac{' + exp1 + '}{' + exp2 + '}\\right)'
          if (exp1 === exp2) {
            me.stockage[2][0] += '=\\mathrm{ln}(1)=0'
          } else if (j3pPGCD(exp1, exp2, { negativesAllowed: true }) !== 1) {
            me.stockage[2][0] += '=\\mathrm{ln}\\left(' + simplifieFrac(exp1, exp2) + '\\right)'
          }
        }
        me.stockage[3][0] = ds.textes.correction
        // la solution attendue :
        stor.solution[1] = 'ln(' + exp1 + ')-ln(' + exp2 + ')'
      } else {
        if (multOuDiv === 'div') {
          operation = '[ \\frac{\\mathrm{ln}(' + u1x + ')}{' + coeffMultiplicateur + '}]_{' + borneInf + '}^{' + borneSup + '}='
          me.stockage[2][0] = operation
          if (exp2 !== 1) {
            me.stockage[2][0] += '\\frac{' + chaineTemp1 + '-' + chaineTemp2 + '}{' + coeffMultiplicateur + '}'
          } else {
            if (exp1 === 1) {
              me.stockage[2][0] += 0
            } else {
              me.stockage[2][0] += '\\frac{' + chaineTemp1 + '}{' + coeffMultiplicateur + '}'
            }
          }
          // simplification éventuelle... pas si exp1==1
          if (exp1 !== 1 && exp2 !== 1) {
            me.stockage[2][0] += '=\\frac{1}{' + coeffMultiplicateur + '} \\mathrm{ln}\\left(\\frac{' + exp1 + '}{' + exp2 + '}\\right)'
            if (j3pPGCD(exp1, exp2, { negativesAllowed: true }) !== 1) {
              me.stockage[2][0] += '=\\frac{1}{' + coeffMultiplicateur + '} \\mathrm{ln}\\left(' + simplifieFrac(exp1, exp2) + '\\right)'
              if (String(simplifieFrac(exp1, exp2)) === '1') {
                me.stockage[2][0] += '=0'
              }
            }
          }
          //
          me.stockage[3][0] = ds.textes.correction
          me.stockage[3][1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = '(ln(' + exp1 + ')-ln(' + exp2 + '))/' + coeffMultiplicateur
        } else {
          operation = '[' + coeffMultiplicateur + '\\times \\mathrm{ln}(' + u1x + ')]_{' + borneInf + '}^{' + borneSup + '}='
          me.stockage[2][0] = operation
          if (exp2 !== 1) {
            if (exp1 !== 1) {
              me.stockage[2][0] += coeffMultiplicateur + '(' + chaineTemp1 + '-' + chaineTemp2 + ')'
            } else {
              me.stockage[2][0] += '-' + coeffMultiplicateur + chaineTemp2
            }
          } else {
            if (exp1 === 1) {
              me.stockage[2][0] += 0
            } else {
              me.stockage[2][0] += coeffMultiplicateur + chaineTemp1
            }
          }
          // simplification éventuelle... pas si exp1==1
          if (exp1 !== 1 && exp2 !== 1) {
            me.stockage[2][0] += '=' + coeffMultiplicateur + '\\mathrm{ln}(\\frac{' + exp1 + '}{' + exp2 + '})'
            if (j3pPGCD(exp1, exp2, { negativesAllowed: true }) !== 1) {
              me.stockage[2][0] += '=' + coeffMultiplicateur + '\\mathrm{ln}(' + simplifieFrac(exp1, exp2) + ')'
              if (String(simplifieFrac(exp1, exp2)) === '1') {
                me.stockage[2][0] += '=0'
              }
            }
          }
          me.stockage[3][0] = ds.textes.correction
          me.stockage[3][1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = coeffMultiplicateur + '*(ln(' + exp1 + ')-ln(' + exp2 + '))'
        }
      }
      return operation
    }

    // console.log('uprime/u ' + 'tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv)
    let a, b, c, u1x, exp1, exp2, chaineTemp1, chaineTemp2, operation1
    if (tabCoefsU.length === 2) {
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      if (a === -1) {
        u1x = '-x+' + b
      } else {
        u1x = a + 'x+' + b
      }
      // A différencier suivant la fonction à intégrer:
      exp1 = a * borneSup + b
      exp2 = a * borneInf + b
      // les solutions finales
      chaineTemp1 = '\\mathrm{ln}(' + exp1 + ')'
      if (exp1 === 1) {
        chaineTemp1 = ''
      }
      if (exp1 === 'e') {
        chaineTemp1 = '1'
      }
      chaineTemp2 = '\\mathrm{ln}(' + exp2 + ')'
      if (exp2 === 1) {
        chaineTemp2 = ''
      }
      if (exp2 === 'e') {
        chaineTemp2 = '1'
      }
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      operation1 = definieCorrection()
    } else {
      c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      if (a === -1) {
        u1x = '-x^2+' + b + 'x+' + c
      } else {
        u1x = a + 'x^2+' + b + 'x+' + c
      }
      // A différencier suivant la fonction à intégrer:
      exp1 = a * borneSup * borneSup + b * borneSup + c
      exp2 = a * borneInf * borneInf + b * borneInf + c
      // les solutions finales
      chaineTemp1 = '\\mathrm{ln}(' + exp1 + ')'
      if (exp1 === 1) {
        chaineTemp1 = ''
      }
      if (exp1 === 'e') {
        chaineTemp1 = '1'
      }
      chaineTemp2 = '\\mathrm{ln}(' + exp2 + ')'
      if (exp2 === 1) {
        chaineTemp2 = ''
      }
      if (exp2 === 'e') {
        chaineTemp2 = '1'
      }
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      operation1 = definieCorrection()
    }
    // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
    me.stockage[1] = '$' + operation1 + '$'

    if (coeffMultiplicateur === 1) {
      stor.solution[2] = '\\mathrm{ln}(' + u1x + ')'
    } else {
      if (multOuDiv === 'div') {
        stor.solution[2] = '(\\mathrm{ln}(' + u1x + '))/' + coeffMultiplicateur
      } else {
        stor.solution[2] = coeffMultiplicateur + '*\\mathrm{ln}(' + u1x + ')'
      }
    }
    me.logIfDebug('la solution :' + stor.solution[1] + ' et la primitive=' + stor.solution[2])
  }

  function tirageCalculUPrimeExpu (tabCoefsU, tabCoefsUprime, coeffMultiplicateur, multOuDiv, borneInf, borneSup) {
    // console.log('tabCoefsU=' + tabCoefsU + ' et tabCoefsUprime=' + tabCoefsUprime + ' et coeffMultiplicateur=' + coeffMultiplicateur + ' et multOuDiv=' + multOuDiv)
    let a, b, c, u1x, exp1, exp2, chaineTemp1, chaineTemp2, operation1
    if (tabCoefsU.length === 2) {
      // console.log('degré 1 !! avec a<>1')
      b = tabCoefsU[0]
      a = tabCoefsU[1]
      if (a === -1) {
        u1x = '-x+' + b
      } else {
        u1x = a + 'x+' + b
      }
      // A différencier suivant la fonction à intégrer:
      exp1 = a * borneSup + b
      exp2 = a * borneInf + b
      // les solutions finales
      chaineTemp1 = '\\mathrm{e}^{' + exp1 + '}'
      if (exp1 === 1) {
        chaineTemp1 = 'e'
      }
      if (exp1 === 0) {
        chaineTemp1 = '1'
      }
      chaineTemp2 = '\\mathrm{e}^{' + exp2 + '}'
      if (exp2 === 1) {
        chaineTemp2 = 'e'
      }
      if (exp2 === 0) {
        chaineTemp2 = '1'
      }
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      if (coeffMultiplicateur === 1) {
        operation1 = '[ \\mathrm{e}^{' + u1x + '}]_{' + borneInf + '}^{' + borneSup + '}='
        me.stockage[2][0] = operation1
        me.stockage[2][0] += chaineTemp1 + '-' + chaineTemp2
        if (exp1 === exp2) me.stockage[2][0] += '=0'
        me.stockage[3][0] = ds.textes.correction
        // la solution attendue :
        stor.solution[1] = 'exp(' + exp1 + ')-exp(' + exp2 + ')'
      } else {
        if (multOuDiv === 'div') {
          operation1 = '[ \\frac{\\mathrm{e}^{' + u1x + '}}{' + coeffMultiplicateur + '}]_{' + borneInf + '}^{' + borneSup + '}='
          me.stockage[2][0] = operation1
          me.stockage[2][0] += '\\frac{' + chaineTemp1 + '-' + chaineTemp2 + '}{' + coeffMultiplicateur + '}'
          if (exp1 === exp2) me.stockage[2][0] += '=0'
          me.stockage[3][0] = ds.textes.correction
          me.stockage[3][1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = '(exp(' + exp1 + ')-exp(' + exp2 + '))/' + coeffMultiplicateur
        } else {
          operation1 = '[' + coeffMultiplicateur + '\\times \\mathrm{e}^{' + u1x + '}]_{' + borneInf + '}^{' + borneSup + '}='
          me.stockage[2][0] = operation1
          me.stockage[2][0] += coeffMultiplicateur + '(' + chaineTemp1 + '-' + chaineTemp2 + ')'
          if (exp1 === exp2) me.stockage[2][0] += '=0'
          me.stockage[3][0] = ds.textes.correction
          me.stockage[3][1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = coeffMultiplicateur + '*(exp(' + exp1 + ')-exp(' + exp2 + '))'
        }
      }
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      me.stockage[1] = '$' + operation1 + '$'
    } else {
      // console.log('degré2 !! avec a<>1')
      c = tabCoefsU[0]
      b = tabCoefsU[1]
      a = tabCoefsU[2]
      if (a === -1) {
        u1x = '-x^2+' + b + 'x+' + c
      } else {
        u1x = a + 'x^2+' + b + 'x+' + c
      }
      // A différencier suivant la fonction à intégrer:
      exp1 = a * borneSup * borneSup + b * borneSup + c
      exp2 = a * borneInf * borneInf + b * borneInf + c
      // les solutions finales
      chaineTemp1 = '\\mathrm{e}^{' + exp1 + '}'
      if (exp1 === 1) {
        chaineTemp1 = 'e'
      }
      if (exp1 === 0) {
        chaineTemp1 = '1'
      }
      chaineTemp2 = '\\mathrm{e}^{' + exp2 + '}'
      if (exp2 === 1) {
        chaineTemp2 = 'e'
      }
      if (exp2 === 0) {
        chaineTemp2 = '1'
      }
      // la solution du calcul, ce qu’il y a dans les crochets' (petits crochets cause PB Mathquill)
      if (coeffMultiplicateur === 1) {
        operation1 = '[ \\mathrm{e}^{' + u1x + '}]_{' + borneInf + '}^{' + borneSup + '}='
        me.stockage[2][0] = operation1
        me.stockage[2][0] += chaineTemp1 + '-' + chaineTemp2
        if (exp1 === exp2) me.stockage[2][0] += '=0'
        me.stockage[3][0] = ds.textes.correction
        // la solution attendue :
        stor.solution[1] = 'exp(' + exp1 + ')-exp(' + exp2 + ')'
      } else {
        if (multOuDiv === 'div') {
          operation1 = '[ \\frac{\\mathrm{e}^{' + u1x + '}}{' + coeffMultiplicateur + '}]_{' + borneInf + '}^{' + borneSup + '}='
          me.stockage[2][0] = operation1
          me.stockage[2][0] += '\\frac{' + chaineTemp1 + '-' + chaineTemp2 + '}{' + coeffMultiplicateur + '}'
          if (exp1 === exp2) me.stockage[2][0] += '=0'
          me.stockage[3][0] = ds.textes.correction
          me.stockage[3][1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = '(exp(' + exp1 + ')-exp(' + exp2 + '))/' + coeffMultiplicateur
        } else {
          operation1 = '[' + coeffMultiplicateur + '\\times \\mathrm{e}^{' + u1x + '}]_{' + borneInf + '}^{' + borneSup + '}='
          me.stockage[2][0] = operation1
          me.stockage[2][0] += coeffMultiplicateur + '(' + chaineTemp1 + '-' + chaineTemp2 + ')'
          if (exp1 === exp2) me.stockage[2][0] += '=0'
          me.stockage[3][0] = ds.textes.correction
          me.stockage[3][1] = coeffMultiplicateur
          // la solution attendue :
          stor.solution[1] = coeffMultiplicateur + '*(exp(' + exp1 + ')-exp(' + exp2 + '))'
        }
      }
      // on stocke l’opération maintenant, afin de la réutiliser dans en consigne et en correction.
      me.stockage[1] = '$' + operation1 + '$'
    }
    if (coeffMultiplicateur === 1) {
      stor.solution[2] = 'exp(' + u1x + ')'
    } else {
      if (multOuDiv === 'div') {
        stor.solution[2] = '(exp(' + u1x + '))/' + coeffMultiplicateur
      } else {
        stor.solution[2] = coeffMultiplicateur + '*exp(' + u1x + ')'
      }
    }
    me.logIfDebug('la solution :' + stor.solution[1] + ' et la primitive=' + stor.solution[2])
  }

  function getDonnees () {
    return {

      typesection: 'lycee',
      nbrepetitions: 1,
      nbetapes: 1,
      indication: '',
      // surchargeindication: false;  pour le cas de presentation1bis
      nbchances: 2,
      formules: ['uprimeexp(u)', 'uprime/u', 'uprime/racine(u)', 'uprime*u^n'],
      // [0;0](sans coeff multiplicateur) [1;1](avec) ou [0;1](aléatoire)
      coeff: '[0;1]',
      // [1;1] (affine), [2;2] (deg2) ou [1;2] (alea)
      degre_u: '[1;2]',
      // rend caduque le paramètre précédent : (pas encore implémenté)
      u_avec_exp: false,
      u_avec_trig: false,
      // interversion possible de l’ordre croissant des bornes: (pas implémenté, pas forcément pertinent à cause du pb mq si borne négative)
      // inversion_bornes=false;
      structure: 'presentation2', //  || "presentation2" || "presentation3"  || "presentation1bis"
      // limite=5;
      textes: {
        titre_exo: 'Calculs d’expressions entre crochets',
        question: 'Calcule la valeur exacte de l’expression suivante :',
        correction: 'Voici la correction détaillée :',
        correction2: 'Étudie bien la correction :',
        phrase_correction: 'La réponse est fausse.'

      }
    }
  }
  function enonceMain () {
    const debuthaut = 50
    // dénomination claire :
    const coeffsU = []
    // moins claire, car ça peut être lambda*u'
    const coeffsUprime = []
    // détermination du degré de u (1 ou 2)
    let degreDeU
    if (ds.degre_u !== '[1;1]' && ds.degre_u !== '[2;2]') {
      degreDeU = j3pGetRandomInt(1, 2)
    } else {
      const [minInt, maxInt] = j3pGetBornesIntervalle(ds.degre_u)
      degreDeU = j3pGetRandomInt(minInt, maxInt)
    }
    /// ///////////////
    // pour mes tests//
    /// ///////////////
    // var degreDeU=1;
    /// ///////////////
    // détermination de la présence d’un coef multiplicateur ou non
    let presenceCoeff
    if (ds.coeff !== '[1;1]' && ds.coeff !== '[0;0]') {
      presenceCoeff = j3pGetRandomInt(0, 1)
    } else {
      const [minInt, maxInt] = j3pGetBornesIntervalle(ds.coeff)
      presenceCoeff = j3pGetRandomInt(minInt, maxInt)
    }
    // détermination du type de formule (voir un switch/case plus loin pour son usage)
    const nbCasPossibles = ds.formules.length
    const typeFormule = j3pGetRandomInt(0, (nbCasPossibles - 1))

    // console.log('typeFormule=' + typeFormule, ds.formules)

    // je génère mes bornes de l’intégrales, j’ai un bug d’affichage à étudier si la borneSup<0 (cause symbole moins ?)
    // A noter que ces valeurs seront écrasées après suivant la formule utilisée... (car on a des conditions supplémentaires suivant le formule (ln(exp1)>0 par exemple))
    let laBorneInf, laBorneSup, leCoef, multdiv, alea
    do {
      laBorneInf = j3pGetRandomInt(-4, 4)
      laBorneSup = j3pGetRandomInt(-4, 4)
    // A insérer : gestion de inversion_bornes, sachant que pb mathquill si négatif en haut de la borne...
    // il suffira d’intervertir leurs valeurs dans les appels aux fonctions tirage
    } while (laBorneInf >= laBorneSup)

    switch (degreDeU) {
      case 1:
        // console.log('degré 1')
        if (presenceCoeff === 0) {
        // pas de coeff multiplicateur
          if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
          // cas où il nous faut u>0 sur l’intervalle... (avec u affine c pas compliqué...)
            do {
              coeffsU[1] = j3pGetRandomInt(-7, 7)
              coeffsU[0] = j3pGetRandomInt(2, 10)
              laBorneInf = j3pGetRandomInt(-4, 4)
              laBorneSup = j3pGetRandomInt(-4, 4)
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
          } else {
            do {
              coeffsU[1] = j3pGetRandomInt(-7, 7)
              coeffsU[0] = j3pGetRandomInt(2, 10)
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[1] * laBorneSup + coeffsU[0] > 40)
          }
          // u'(x)=a
          coeffsUprime[0] = coeffsU[1]
          leCoef = 1
          multdiv = ''
        } else {
        // coeff multiplicateur
          alea = j3pGetRandomInt(0, 1)
          if (alea === 0) {
          // coef>1
            leCoef = j3pGetRandomInt(2, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
            // cas où il nous faut u>0 sur l’intervalle... (avec u affine c pas compliqué...)
              do {
                coeffsU[1] = j3pGetRandomInt(-7, 7)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[1] = j3pGetRandomInt(-7, 7)
                coeffsU[0] = j3pGetRandomInt(2, 10)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }
            // de la forme lambda*u'(x)*exp(u) avec lambda=leCoeff et u'(x)=a
            coeffsUprime[0] = leCoef * coeffsU[1]
            multdiv = 'mult'
          } else {
          // coeff<1
            leCoef = j3pGetRandomInt(2, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
            // cas où il nous faut u>0 sur l’intervalle... (avec u affine c pas compliqué...)
              do {
                do {
                  coeffsU[1] = j3pGetRandomInt(-7, 7)
                } while (ds.formules[typeFormule] === 'uprime/racine(u)' && (coeffsU[1] === 2))
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] % leCoef !== 0 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[1] = j3pGetRandomInt(-7, 7)
                coeffsU[0] = j3pGetRandomInt(2, 10)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[1] === 1 || coeffsU[1] % leCoef !== 0 || coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }
            // de la forme u'(x)*exp(u)/lambda avec lambda=leCoeff et u'(x)=a
            coeffsUprime[0] = coeffsU[1] / leCoef
            multdiv = 'div'
          }
        }
        break

      case 2:
        // console.log('degré 2')
        if (presenceCoeff === 0) {
        // pas de coeff multiplicateur
          if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
          // cas où il nous faut u>0 sur l’intervalle... pour simplifier je prends donc a<0 et u(borneInf)>0 et u(borneSup)>0
            do {
              coeffsU[2] = j3pGetRandomInt(-7, -1)
              coeffsU[1] = j3pGetRandomInt(2, 10)
              coeffsU[0] = j3pGetRandomInt(2, 10)
              laBorneInf = j3pGetRandomInt(-4, 4)
              laBorneSup = j3pGetRandomInt(-4, 4)
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
          } else {
            do {
              coeffsU[2] = j3pGetRandomInt(-7, 7)
              coeffsU[1] = j3pGetRandomInt(2, 10)
              coeffsU[0] = j3pGetRandomInt(2, 10)
            // cas particulier pour exp, pour eviter u bug avec mtg32 - pas d’ecriture scientifique - pas plus d’exp(40)'
            } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] > 40)
          }
          // u'(x)=2ax+b
          coeffsUprime[1] = 2 * coeffsU[2]
          coeffsUprime[0] = coeffsU[1]
          leCoef = 1
          multdiv = ''
        } else {
        // coeff multiplicateur
          alea = j3pGetRandomInt(0, 1)
          if (alea === 0) {
          // coef>1
            leCoef = j3pGetRandomInt(2, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
            // cas où il nous faut u>0 sur l’intervalle... pour simplifier je prends donc a<0 et u(borneInf)>0 et u(borneSup)>0
              do {
                coeffsU[2] = j3pGetRandomInt(-7, -1)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[2] = j3pGetRandomInt(-7, 7)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
              // cas particulier pour exp, pour eviter u bug avec mtg32 - pas d’ecriture scientifique - pas plus d’exp(40)'
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }

            // de la forme lambda*u'(x)*exp(u) avec lambda=leCoeff et u'(x)=2ax+b
            coeffsUprime[1] = leCoef * 2 * coeffsU[2]
            coeffsUprime[0] = leCoef * coeffsU[1]
            multdiv = 'mult'
          } else {
          // coeff<1
            leCoef = j3pGetRandomInt(3, 5)
            if (ds.formules[typeFormule] === 'uprime/u' || ds.formules[typeFormule] === 'uprime/racine(u)') {
            // cas où il nous faut u>0 sur l’intervalle... pour simplifier je prends donc a<0 et u(borneInf)>0 et u(borneSup)>0
              do {
                coeffsU[2] = j3pGetRandomInt(-7, -1)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
                laBorneInf = j3pGetRandomInt(-4, 4)
                laBorneSup = j3pGetRandomInt(-4, 4)
              } while (coeffsU[1] % leCoef !== 0 || (2 * coeffsU[2]) % leCoef !== 0 || coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || laBorneInf >= laBorneSup || laBorneSup < 0 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] <= 0 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] <= 0)
            } else {
              do {
                coeffsU[2] = j3pGetRandomInt(-7, 7)
                coeffsU[1] = j3pGetRandomInt(2, 10)
                coeffsU[0] = j3pGetRandomInt(2, 10)
              // cas particulier pour exp, pour eviter u bug avec mtg32 - pas d’ecriture scientifique - pas plus d’exp(40)'
              } while (coeffsU[0] === 0 || coeffsU[1] === 0 || coeffsU[2] === 0 || coeffsU[2] === 1 || coeffsU[1] % leCoef !== 0 || (2 * coeffsU[2]) % leCoef !== 0 || coeffsU[2] === 1 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] < -40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] < -40 || coeffsU[2] * laBorneInf * laBorneInf + coeffsU[1] * laBorneInf + coeffsU[0] > 40 || coeffsU[2] * laBorneSup * laBorneSup + coeffsU[1] * laBorneSup + coeffsU[0] > 40)
            }
            // de la forme u'(x)*exp(u)/lambda avec lambda=leCoeff et u'(x)=2ax+b
            coeffsUprime[0] = coeffsU[1] / leCoef
            coeffsUprime[1] = 2 * coeffsU[2] / leCoef
            multdiv = 'div'
          }
        }
        break
    }
    // stockage[2] contiendra toutes les étapes de la correction, stockage[3][0] la phrase de correction et stockage[3][1] sa variable

    me.stockage[2] = []
    me.stockage[3] = []
    me.logIfDebug('formule : ', ds.formules[typeFormule])
    switch (ds.formules[typeFormule]) {
      case 'uprimeexp(u)':
      // pas de condition particulière sur les bornes
        tirageCalculUPrimeExpu(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup)
        break

      case 'uprime/u':
      // condition sur les bornes : il faut que u>0 sur l’intervalle :-(
        tirageCalculUPrimeU(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup)
        break

      case 'uprime/racine(u)':
        tirageCalculUPrimeRacineU(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup)
        break

      case 'uprime*u^n':
      // tirageCalculUPrimeRacineU(coeffsU,coeffsUprime,leCoef,multdiv,laBorneInf,laBorneSup);
        {
          const puissance = j3pGetRandomInt(2, 4)
          tirageCalculUPrimeUn(coeffsU, coeffsUprime, leCoef, multdiv, laBorneInf, laBorneSup, puissance)
        }
        break
    }
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { paddingLeft: '20px', paddingTop: debuthaut + 'px' }) })
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.question)
    const elt = j3pAffiche(stor.zoneCons2, '', me.stockage[1] + '&2&', { inputmq2: { texte: '', correction: '', maxchars: '5' } })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d.,\\-+,/()*')

    stor.laPalette = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.laPalette, { paddingTop: '10px', paddingBottom: '40px' })
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, { liste: ['racine', 'exp', 'ln', 'fraction', 'puissance'] })
    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })
  } // enonceMain

  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
        me.stockage = [0, 0, 0, 0]
        me.storage.solution = []
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication) this.indication(me.zonesElts.IG, ds.indication)
        enonceMain()
        getMtgCore()
          .then(
            // success
            mtgAppLecteur => {
              stor.mtgAppLecteur = mtgAppLecteur
              // Une liste qui servira pour les calculs.
              stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAPZAAACpQAAAQEAAAAAAAAAAQAAABH#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABYwABMAAAAAEAAAAAAAAAAAAAAAIA#####wABZAABMAAAAAEAAAAAAAAAAAAAAAIA#####wABYQABMAAAAAEAAAAAAAAAAP####8AAAABAAVDRm9uYwD#####AAFmAAF4#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAABeAAAAAQA#####wABZwABMAAAAAEAAAAAAAAAAAABeAAAAAIA#####wACaTEAATAAAAABAAAAAAAAAAAAAAACAP####8AAmkyAAEwAAAAAQAAAAAAAAAA#####wAAAAEACENEZXJpdmVlAP####8AAmYnAAAABQAAAAYA#####wACZycAAAAGAAAAAgD#####AAJ4MQABMQAAAAE#8AAAAAAAAAAAAAIA#####wACeDIAATIAAAABQAAAAAAAAAAAAAACAP####8AAngzAAEzAAAAAUAIAAAAAAAAAAAABAD#####AAR6ZXJvABJhYnMoeCk8MC4wMDAwMDAwMDH#####AAAAAQAKQ09wZXJhdGlvbgQAAAADAAAAAAUAAAAAAAAAAT4RLgvoJtaVAAF4AAAAAgD#####AAR0ZXN0ADt6ZXJvKGYnKHgxKS1nJyh4MSkpJnplcm8oZicoeDIpLWcnKHgyKSkmemVybyhmJyh4MyktZycoeDMpKQAAAAcKAAAABwr#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAAAOAAAABwEAAAAIAAAACf####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAALAAAACAAAAAoAAAAJAAAACwAAAAgAAAAOAAAABwEAAAAIAAAACQAAAAkAAAAMAAAACAAAAAoAAAAJAAAADAAAAAgAAAAOAAAABwEAAAAIAAAACQAAAAkAAAANAAAACAAAAAoAAAAJAAAADQAAAAIA#####wABYgABMAAAAAEAAAAAAAAAAP###############w==')
              me.finEnonce()
              j3pFocus(stor.zoneInput)
            },
            // failure
            (error) => j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
          )
          .catch(j3pShowError)
      } else {
        me.videLesZones()
        enonceMain()
        me.finEnonce()
      }
      break // case "enonce":

    case 'correction':
      {
        j3pEmpty(stor.zoneCorr)
        $(stor.zoneInput).blur()

        // on teste si une réponse a été saisie

        const repEleve = $(stor.zoneInput).mathquill('latex')
        if ((repEleve === '') && (!this.isElapsed)) {
          this.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
          this.afficheBoutonValider()
        } else { // une réponse a été saisie
        // Avec MTG32 :
          const repEleveNettoyee = mathquillMtg32(repEleve)
          // console.log('repEleve_mtg32=' + repEleveNettoyee)
          const reponseAnalysee = analyseFormelleMtg32(repEleveNettoyee)
          if (reponseAnalysee.bon && reponseAnalysee.simplifie) {
            this.score++
            // section602Quotcorrection_detaillee(me.stockage[1],me.stockage[2],me.stockage[3],me.stockage[4]);
            stor.zoneCorr.style.color = this.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            stor.zoneExpli.innerHTML += ds.textes.correction2
            macorrection(reponseAnalysee, repEleveNettoyee)
            j3pDesactive(stor.zoneInput)
            stor.zoneInput.style.color = this.styles.cbien
            j3pFreezeElt(stor.zoneInput)
            this.typederreurs[0]++
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = this.styles.cfaux
            // à cause de la limite de temps :
            if (this.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              // this.typederreurs[10]++;
              macorrection(reponseAnalysee, repEleveNettoyee)
              j3pDesactive(stor.zoneInput)
              stor.zoneInput.style.color = this.styles.cfaux
              j3pBarre(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)
              // je classe cette erreur comme 'autre'
              this.typederreurs[10]++
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                if (!reponseAnalysee.bon) {
                  stor.zoneInput.style.color = this.styles.cfaux
                  j3pFocus(stor.zoneInput)
                }
                if (!reponseAnalysee.bon) {
                  stor.zoneCorr.innerHTML = ds.textes.phrase_correction
                }
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                this.typederreurs[1]++
                // indication éventuelle ici
                this.etat = 'correction'
                // j3pFocus("affiche1input1");
                this.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                stor.zoneCorr.innerHTML = cFaux
                stor.zoneExpli.innerHTML += '<br>' + regardeCorrection
                macorrection(reponseAnalysee, repEleveNettoyee)
                j3pDesactive(stor.zoneInput)
                stor.zoneInput.style.color = this.styles.cfaux
                j3pBarre(stor.zoneInput)
                j3pFreezeElt(stor.zoneInput)
                this.etat = 'navigation'
                this.sectionCourante()
              }
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
