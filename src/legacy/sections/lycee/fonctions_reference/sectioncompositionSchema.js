import $ from 'jquery'
import { j3pAddElt, j3pAjouteFoisDevantX, j3pDetruit, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pMathquillXcas, j3pPaletteMathquill, j3pPGCD, j3pPolynome, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pGetLatexProduit } from 'src/legacy/core/functionsLatex'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Rémi DENIAUD
        février 2020
        Décomposition d’une fonction sous la forme de la composée de deux fonctions à déterminer
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQPAAADpgAAAQEAAAAAAAAAAQAAABj#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAItMf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABP#AAAAAAAAAAAAACAP####8AAWIAATIAAAABQAAAAAAAAAAAAAACAP####8AAWMAATYAAAABQBgAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wABdQALYSp4XjIrYip4K2P#####AAAAAQAKQ09wZXJhdGlvbgAAAAAFAAAAAAUC#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAH#####AAAAAQAKQ1B1aXNzYW5jZf####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAAABQAAAAAAAAAAAAAAFAgAAAAYAAAACAAAACAAAAAAAAAAGAAAAAwABeAAAAAQA#####wACdjEAB3NxcnQoeCn#####AAAAAgAJQ0ZvbmN0aW9uAQAAAAgAAAAAAAF4AAAABAD#####AAJ2MgADMS94AAAABQMAAAABP#AAAAAAAAAAAAAIAAAAAAABeAAAAAQA#####wACdjMABmV4cCh4KQAAAAkHAAAACAAAAAAAAXgAAAAEAP####8AAnY0AAN4XjIAAAAHAAAACAAAAAAAAAABQAAAAAAAAAAAAXgAAAAEAP####8AAnY1AAN4XjMAAAAHAAAACAAAAAAAAAABQAgAAAAAAAAAAXgAAAAEAP####8AAnY2AAN4XjQAAAAHAAAACAAAAAAAAAABQBAAAAAAAAAAAXgAAAAEAP####8AAnY3AAZjb3MoeCkAAAAJBAAAAAgAAAAAAAF4AAAABAD#####AAJ2OAAGc2luKHgpAAAACQMAAAAIAAAAAAABeAAAAAQA#####wAEcmVwMQAFMip4LTEAAAAFAQAAAAUCAAAAAUAAAAAAAAAAAAAACAAAAAAAAAABP#AAAAAAAAAAAXgAAAAEAP####8ABHJlcDIAB3NxcnQoeCkAAAAJAQAAAAgAAAAAAAF4#####wAAAAMAEENUZXN0RXF1aXZhbGVuY2UA#####wAIZWdhbGl0ZTAAAAAEAAAADQEAAAAAAT#wAAAAAAAAAQAAAAoA#####wAIZWdhbGl0ZTEAAAAFAAAADgEAAAAAAT#wAAAAAAAAAQAAAAoA#####wAIZWdhbGl0ZTIAAAAGAAAADgEAAAAAAT#wAAAAAAAAAQAAAAoA#####wAIZWdhbGl0ZTMAAAAHAAAADgEAAAAAAT#wAAAAAAAAAQAAAAoA#####wAIZWdhbGl0ZTQAAAAIAAAADgEAAAAAAT#wAAAAAAAAAQAAAAoA#####wAIZWdhbGl0ZTUAAAAJAAAADgEAAAAAAT#wAAAAAAAAAQAAAAoA#####wAIZWdhbGl0ZTYAAAAKAAAADgEAAAAAAT#wAAAAAAAAAQAAAAoA#####wAIZWdhbGl0ZTcAAAALAAAADgEAAAAAAT#wAAAAAAAAAQAAAAoA#####wAIZWdhbGl0ZTgAAAAMAAAADgEAAAAAAT#wAAAAAAAAAf###############w=='
    stor.mtgList = stor.mtgAppLecteur.createList(txtFigure)
  }

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let i
    j3pDetruit(stor.zoneCons6)
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    for (i = 1; i <= 2; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div')
    j3pAffiche(stor.zoneexplication1, '', ds.textes['corr1_' + stor.numCorr], stor.objCons)
    j3pAffiche(stor.zoneexplication2, '', ds.textes.corr2, stor.objCons)
  }

  function ecoute (num) {
    if (stor.zoneInput[num - 1].className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.zoneCons6)
      j3pPaletteMathquill(stor.zoneCons6, stor.zoneInput[num - 1], {
        liste: ['puissance', 'fraction', 'racine', 'cos', 'sin', 'exp']
      })
    }
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.
      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Composition de fonctions',
        // on donne les phrases de la consigne
        consigne1: 'Soit $£f$ la fonction définie sur $£d$ par $£f(x)=£e$.',
        consigne2: 'Cette fonction peut-être écrite sous la forme $£f(x)=v\\quad\\circ\\quad u(x)$ où $v$ est une fonction de référence et $u$ une fonction relativement simple.',
        consigne3: 'Donner l’expression de chacune de ces deux fonctions&nbsp;:',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'La fonction doit être écrite sous forme simplifiée&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'Pour tout $x\\in £d$, $£f(x)$ est la racine carrée de $£{ux}$.',
        corr1_2: 'Pour tout $x\\in £d$, $£f(x)$ est l’inverse de $£{ux}$.',
        corr1_3: 'Pour tout $x\\in £d$, $£f(x)$ est l’exponentielle de $£{ux}$.',
        corr1_4: 'Pour tout $x\\in £d$, $£f(x)$ est le carrée de $£{ux}$.',
        corr1_5: 'Pour tout $x\\in £d$, $£f(x)$ est le cube de $£{ux}$.',
        corr1_6: 'Pour tout $x\\in £d$, $£f(x)$ est le quadruple de $£{ux}$.',
        corr1_7: 'Pour tout $x\\in £d$, $£f(x)$ est le cosinus de $£{ux}$.',
        corr1_8: 'Pour tout $x\\in £d$, $£f(x)$ est le sinus de $£{ux}$.',
        corr2: 'Ainsi $v(x)=£{vx}$ et $u(x)=£{ux}$.'
      },
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()

    // Construction de la page

    // Le paramètre définit la largeur relative de la première colonne
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
    stor.tabQuest = [1, 2, 3, 4, 5]
    stor.tabQuestInit = [...stor.tabQuest]
    stor.listeFonctionsv = ['\\sqrt{x}', '\\frac{1}{x}', '\\mathrm{e}^x', 'x^2', 'x^3', 'x^4', '\\cos(x)', '\\sin(x)']
    stor.listeFonctionsvTest = ['racine(x)', '(1)/(x)', 'exp(x)', 'x^2', 'x^3', 'x^4', 'cos(x)', 'sin(x)']
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    depart()
    let i
    const tabNomFct = ['f', 'g', 'h', 'k']
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    stor.quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.objCons = {}
    stor.objCons.f = tabNomFct[j3pGetRandomInt(0, 3)]
    // On va choisir ici l’expression de u(x)
    const choixux = j3pGetRandomInt(1, 3)
    stor.choixCoef = []
    stor.numCorr = (stor.quest <= 3)
      ? stor.quest
      : (stor.quest === 4) ? j3pGetRandomInt(4, 6) : j3pGetRandomInt(7, 8)
    if (choixux === 1) {
      // Fonction de la forme ax^2+b avec a et b positifs (comme ça, pas de pb sur le domaine
      stor.choixCoef[1] = 0
      stor.choixCoef[2] = j3pGetRandomInt(1, 4)
      do {
        stor.choixCoef[0] = j3pGetRandomInt(1, 7)
      } while (j3pPGCD(stor.choixCoef[0], stor.choixCoef[2]) !== 1)
      stor.objCons.d = '\\R'
    } else {
      // Fonction affine
      stor.choixCoef[1] = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 4)
      do {
        stor.choixCoef[0] = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 7)
      } while (j3pPGCD(Math.abs(stor.choixCoef[0]), Math.abs(stor.choixCoef[1])) !== 1)
      stor.choixCoef[2] = 0
      const borne = j3pGetLatexQuotient(j3pGetLatexProduit(-1, stor.choixCoef[0]), stor.choixCoef[1])
      if (stor.numCorr === 1) {
        // il faut que choixCoef[1]x+choixCoef[0]>=0
        stor.objCons.d = (stor.choixCoef[1] > 0) ? '[' + borne + ';+\\infty[' : ']-\\infty;' + borne + ']'
      } else if (stor.numCorr === 2) {
        stor.objCons.d = '\\R\\setminus${$' + borne + '$}$'
      } else {
        stor.objCons.d = '\\R'
      }
    }
    stor.objCons.ux = (stor.choixCoef[2] === 0)
      ? j3pPolynome(stor.choixCoef.slice(0, 2).reverse())
      : j3pPolynome([...stor.choixCoef].reverse())
    // Expression de v(x) et de f(x)
    stor.objCons.vx = stor.listeFonctionsv[stor.numCorr - 1]
    stor.objCons.e = (stor.numCorr === 1)
      ? '\\sqrt{' + stor.objCons.ux + '}'
      : (stor.numCorr === 2)
          ? '\\frac{1}{' + stor.objCons.ux + '}'
          : (stor.numCorr === 3)
              ? '\\mathrm{e}^{' + stor.objCons.ux + '}'
              : (stor.numCorr === 4)
                  ? '\\left(' + stor.objCons.ux + '\\right)^2'
                  : (stor.numCorr === 5)
                      ? '\\left(' + stor.objCons.ux + '\\right)^3'
                      : (stor.numCorr === 6)
                          ? '\\left(' + stor.objCons.ux + '\\right)^4'
                          : (stor.numCorr === 7) ? '\\cos\\left(' + stor.objCons.ux + '\\right)' : '\\sin\\left(' + stor.objCons.ux + '\\right)'
    stor.mtgList.giveFormula2('a', String(stor.choixCoef[2]))
    stor.mtgList.giveFormula2('b', String(stor.choixCoef[1]))
    stor.mtgList.giveFormula2('c', String(stor.choixCoef[0]))

    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (i = 1; i <= 3; i++) {
      stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
      j3pAffiche(stor['zoneCons' + i], '', ds.textes['consigne' + i], stor.objCons)
    }
    for (i = 4; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    const elt1 = j3pAffiche(stor.zoneCons4, '', '$v(x)=$&1&', {
      inputmq1: { texte: '' }
    })
    const elt2 = j3pAffiche(stor.zoneCons5, '', '$u(x)=$&1&', {
      inputmq1: { texte: '' }
    })
    stor.zoneInput = [elt1.inputmqList[0], elt2.inputmqList[0]]
    j3pStyle(stor.zoneCons6, { paddingTop: '10px' })
    $(stor.zoneInput[0]).focusin(ecoute.bind(null, 1))
    $(stor.zoneInput[1]).focusin(ecoute.bind(null, 2))
    ecoute(1)
    mqRestriction(stor.zoneInput[0], '\\d,.+-x/*^()', {
      commandes: ['puissance', 'fraction', 'racine', 'cos', 'sin', 'exp']
    })
    mqRestriction(stor.zoneInput[1], '\\d,.+-x/*^()', {
      commandes: ['puissance', 'fraction', 'racine', 'cos', 'sin', 'exp']
    })
    j3pFocus(stor.zoneInput[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    const mesZonesSaisie = stor.zoneInput.map(eltInput => eltInput.id)
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        let i, repEleveXcas
        const reponse = stor.fctsValid.validationGlobale()
        if (reponse.aRepondu) {
          repEleveXcas = []
          for (i = 0; i < 2; i++) {
            repEleveXcas[i] = j3pAjouteFoisDevantX(j3pMathquillXcas(stor.fctsValid.zones.reponseSaisie[i]), 'x')
            // Je dois gérer la place des fonctions trigo qui sont mal gérées :
            repEleveXcas[i] = repEleveXcas[i].replace(/(\\?cos \(x\))/g, 'cos(x)')
            repEleveXcas[i] = repEleveXcas[i].replace(/(\\?s\*?i\*?n ?\(x\))/g, 'sin(x)')// Des signes de multiplication apparaissent autour de i. Donc je les enlève
            // Mais aussi l’exponentielle
            repEleveXcas[i] = repEleveXcas[i].replace(/(e\^x)/g, 'exp(x)')
            stor.mtgList.giveFormula2('rep' + (2 - i), repEleveXcas[i])
            stor.mtgList.calculateNG()
          }
          stor.fctsValid.zones.bonneReponse[0] = stor.mtgList.valueOf('egalite' + stor.numCorr) === 1
          stor.fctsValid.zones.bonneReponse[1] = stor.mtgList.valueOf('egalite0') === 1
          reponse.bonneReponse = stor.fctsValid.zones.bonneReponse[0] && stor.fctsValid.zones.bonneReponse[1]

          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          stor.fctsValid.coloreUneZone(stor.zoneInput[0].id)
          stor.fctsValid.coloreUneZone(stor.zoneInput[1].id)
        }
        if ((!reponse.aRepondu) && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // On vérifie si ce n’est aps à cause d’une fonction non simplifiée
              const fctsTest = [stor.listeFonctionsvTest[stor.numCorr - 1], stor.choixCoef[2] + '*x^2+(' + stor.choixCoef[1] + '*x)+(' + stor.choixCoef[0] + ')']
              let arbreRepEleve
              let arbreLaReponse
              let imagesEgales
              const repEgales = [false, false]
              for (i = 1; i >= 0; i--) {
                if (!stor.fctsValid.zones.bonneReponse[i]) {
                  arbreRepEleve = new Tarbre(j3pMathquillXcas(repEleveXcas[i]), ['x'])
                  arbreLaReponse = new Tarbre(j3pMathquillXcas(fctsTest[i]), ['x'])
                  imagesEgales = true
                  for (let j = 2; j <= 10; j++) imagesEgales = (imagesEgales && (Math.abs(arbreLaReponse.evalue([j]) - arbreRepEleve.evalue([j])) < Math.pow(10, -10)))
                  repEgales[i] = imagesEgales
                }
              }
              if (repEgales[0] || repEgales[1]) stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              for (i = 1; i >= 0; i--) {
                if (!repEgales[i]) j3pFocus(stor.zoneInput[i])
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
