import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pShuffle, j3pElement, j3pGetRandomInt, j3pGetNewId, j3pStyle, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juillet 2019
        Identifier une fonction paire ou impaire à l’aide d’une courbe
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes']
  ]
}

/**
 * section courbeParite
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const explications = j3pAddElt(stor.conteneurD, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(explications, 'div')
    const correction1 = (stor.typeFct === 'paire') ? ds.textes.corr1_1 : ds.textes.corr1_2
    const numCb = (stor.typeFct === 'paire') ? stor.lesDonnees.numPaire : stor.lesDonnees.numImpaire
    j3pAffiche(stor.zoneExpli1, '', correction1)
    j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2, { n: numCb })
    modifFig({ correction: true, donnees: stor.lesDonnees })
  }

  function depart (callback) {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAXeAAADnAAAAQEAAAAAAAAAAAAAAFX#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAFAAFAawAAAAAAAEBrAUeuFHrh#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAAA4AAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8AAAAAAQ4AAkkxAMAYAAAAAAAAAAAAAAAAAAAFAAFAQIAAAAAAAAAAAAL#####AAAAAQAJQ0Ryb2l0ZUFCAP####8AAAAAABAAAAEAAQAAAAEAAAAD#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wAAAAAADgAAAQABAAAAAQAAAAT#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAEAAAABAAAAA#####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAFAAAABv####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAAA4AAAEFAAEAAAAHAAAACQD#####AQAAAAEOAAJKMQDAKAAAAAAAAMAQAAAAAAAABQACAAAAB#####8AAAACAAdDUmVwZXJlAP####8A5ubmAAEAAAABAAAAAwAAAAkBAQAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABP#AAAAAAAAAAAAABP#AAAAAAAAD#####AAAAAQAKQ1VuaXRleFJlcAD#####AAR1bml0AAAACv####8AAAABAAtDSG9tb3RoZXRpZQD#####AAAAAf####8AAAABAApDT3BlcmF0aW9uAwAAAAE#8AAAAAAAAP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAL#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAAAABAAAlciAQEAAAAAAwAAAAz#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAABAAAADf####8AAAABAAdDQ2FsY3VsAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAAEQD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABRHcmFkdWF0aW9uQXhlc1JlcGVyZQAAABsAAAAIAAAAAwAAAAoAAAAPAAAAEP####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAABEABWFic29yAAAACv####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAABEABW9yZG9yAAAACgAAAAsAAAAAEQAGdW5pdGV4AAAACv####8AAAABAApDVW5pdGV5UmVwAAAAABEABnVuaXRleQAAAAr#####AAAAAQAQQ1BvaW50RGFuc1JlcGVyZQAAAAARAAAAAAAOAAABBQAAAAAKAAAADgAAABIAAAAOAAAAEwAAABYAAAAAEQAAAAAADgAAAQUAAAAACgAAAA0AAAAADgAAABIAAAAOAAAAFAAAAA4AAAATAAAAFgAAAAARAAAAAAAOAAABBQAAAAAKAAAADgAAABIAAAANAAAAAA4AAAATAAAADgAAABUAAAAMAAAAABEAAAAWAAAADgAAAA8AAAAPAAAAABEAAAAAAA4AAAEFAAAAABcAAAAZAAAADAAAAAARAAAAFgAAAA4AAAAQAAAADwAAAAARAAAAAAAOAAABBQAAAAAYAAAAG#####8AAAABAAhDU2VnbWVudAAAAAARAQAAAAAQAAABAAEAAAAXAAAAGgAAABcAAAAAEQEAAAAAEAAAAQABAAAAGAAAABwAAAAEAAAAABEBAAAAAAsAAVcAwBQAAAAAAADANAAAAAAAAAUAAT#cVniavN8OAAAAHf####8AAAACAAhDTWVzdXJlWAAAAAARAAZ4Q29vcmQAAAAKAAAAHwAAABEAAAAAEQAFYWJzdzEABnhDb29yZAAAAA4AAAAg#####wAAAAIAEkNMaWV1T2JqZXRQYXJQdExpZQEAAAARAWZmZgAAAB8AAAAOAAAADwAAAB8AAAACAAAAHwAAAB8AAAARAAAAABEABWFic3cyAA0yKmFic29yLWFic3cxAAAADQEAAAANAgAAAAFAAAAAAAAAAAAAAA4AAAASAAAADgAAACEAAAAWAAAAABEBAAAAAA4AAAEFAAAAAAoAAAAOAAAAIwAAAA4AAAATAAAAGQEAAAARAWZmZgAAACQAAAAOAAAADwAAAB8AAAAFAAAAHwAAACAAAAAhAAAAIwAAACQAAAAEAAAAABEBAAAAAAsAAVIAQCAAAAAAAADAIAAAAAAAAAUAAT#RG06BtOgfAAAAHv####8AAAACAAhDTWVzdXJlWQAAAAARAAZ5Q29vcmQAAAAKAAAAJgAAABEAAAAAEQAFb3JkcjEABnlDb29yZAAAAA4AAAAnAAAAGQEAAAARAWZmZgAAACYAAAAOAAAAEAAAACYAAAACAAAAJgAAACYAAAARAAAAABEABW9yZHIyAA0yKm9yZG9yLW9yZHIxAAAADQEAAAANAgAAAAFAAAAAAAAAAAAAAA4AAAATAAAADgAAACgAAAAWAAAAABEBAAAAAA4AAAEFAAAAAAoAAAAOAAAAEgAAAA4AAAAqAAAAGQEAAAARAWZmZgAAACsAAAAOAAAAEAAAACYAAAAFAAAAJgAAACcAAAAoAAAAKgAAACv#####AAAAAgAMQ0NvbW1lbnRhaXJlAAAAABEBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAfCwAB####AAAAAQAAAAAAAAABAAAAAAAAAAAACyNWYWwoYWJzdzEpAAAAGQEAAAARAWZmZgAAAC0AAAAOAAAADwAAAB8AAAAEAAAAHwAAACAAAAAhAAAALQAAABsAAAAAEQFmZmYAAAAAAAAAAABAGAAAAAAAAAAAACQLAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MikAAAAZAQAAABEBZmZmAAAALwAAAA4AAAAPAAAAHwAAAAYAAAAfAAAAIAAAACEAAAAjAAAAJAAAAC8AAAAbAAAAABEBZmZmAMAgAAAAAAAAP#AAAAAAAAAAAAAmCwAB####AAAAAgAAAAEAAAABAAAAAAAAAAAACyNWYWwob3JkcjEpAAAAGQEAAAARAWZmZgAAADEAAAAOAAAAEAAAACYAAAAEAAAAJgAAACcAAAAoAAAAMQAAABsAAAAAEQFmZmYAwBwAAAAAAAAAAAAAAAAAAAAAACsLAAH###8AAAACAAAAAQAAAAEAAAAAAAAAAAALI1ZhbChvcmRyMikAAAAZAQAAABEBZmZmAAAAMwAAAA4AAAAQAAAAJgAAAAYAAAAmAAAAJwAAACgAAAAqAAAAKwAAADP#####AAAAAQAFQ0ZvbmMA#####wACZjEACTEveCswLjMqeAAAAA0AAAAADQMAAAABP#AAAAAAAAD#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAADQIAAAABP9MzMzMzMzMAAAAdAAAAAAABeAAAAAQA#####wEAAAAAEAABeAAAAAAAAAAAAEAIAAAAAAAABQABv#jn7jrkcQAAAAAEAAAAGAD#####AAJ4MQAAAAoAAAA2AAAAEQD#####AAJ5MQAGZjEoeDEp#####wAAAAEADkNBcHBlbEZvbmN0aW9uAAAANQAAAA4AAAA3AAAAFgD#####AQAAAAAOAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAA4AAAA3AAAADgAAADj#####AAAAAgANQ0xpZXVEZVBvaW50cwD#####AP8AAAACAAAAOQAAAfQAAQAAADYAAAAEAAAANgAAADcAAAA4AAAAOQAAABwA#####wACZjIACTAuNSp4XjItMwAAAA0BAAAADQIAAAABP+AAAAAAAAD#####AAAAAQAKQ1B1aXNzYW5jZQAAAB0AAAAAAAAAAUAAAAAAAAAAAAAAAUAIAAAAAAAAAAF4AAAABAD#####AQAAAAAQAAJ4MQAAAAAAAAAAAEAIAAAAAAAABQABwBr9g9Dr7TQAAAAEAAAAGAD#####AAJ4MgAAAAoAAAA8AAAAEQD#####AAJ5MgAGZjIoeDIpAAAAHgAAADsAAAAOAAAAPQAAABYA#####wEAAAAADgAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAAOAAAAPQAAAA4AAAA+AAAAHwD#####AAAA#wACAAAAPwAAAfQAAQAAADwAAAAEAAAAPAAAAD0AAAA+AAAAPwAAABwA#####wACZjMADTAuMip4XjMrMC4xKngAAAANAAAAAA0CAAAAAT#JmZmZmZmaAAAAIAAAAB0AAAAAAAAAAUAIAAAAAAAAAAAADQIAAAABP7mZmZmZmZoAAAAdAAAAAAABeAAAAAQA#####wEAAAAAEAACeDIAAAAAAAAAAABACAAAAAAAAAUAAUAOyS71eaUwAAAABAAAABgA#####wACeDMAAAAKAAAAQgAAABEA#####wACeTMABmYzKHgzKQAAAB4AAABBAAAADgAAAEMAAAAWAP####8BAAAAAA4AAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAADgAAAEMAAAAOAAAARAAAAB8A#####wB#f38AAgAAAEUAAAH0AAEAAABCAAAABAAAAEIAAABDAAAARAAAAEUAAAAcAP####8AAmY0ABQoZXhwKHgpK2V4cCgteCkpLzQuNQAAAA0DAAAADQD#####AAAAAgAJQ0ZvbmN0aW9uBwAAAB0AAAAAAAAAIQf#####AAAAAQAMQ01vaW5zVW5haXJlAAAAHQAAAAAAAAABQBIAAAAAAAAAAXgAAAAEAP####8BAAAAABAAAngzAAAAAAAAAAAAQAgAAAAAAAAFAAHAF+j#yUlCFwAAAAQAAAAYAP####8AAng0AAAACgAAAEgAAAARAP####8AAnk0AAZmNCh4NCkAAAAeAAAARwAAAA4AAABJAAAAFgD#####AQAAAAAOAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAA4AAABJAAAADgAAAEoAAAAfAP####8AfwAAAAIAAABLAAAB9AABAAAASAAAAAQAAABIAAAASQAAAEoAAABLAAAAHAD#####AARmcmVwAAEzAAAAAUAIAAAAAAAAAAF4AAAABAD#####AQAAAAAQAAJ4NAAAAAAAAAAAAEAIAAAAAAAABQABv+Nr32g8HVAAAAAEAAAAGAD#####AAJ4NQAAAAoAAABOAAAAEQD#####AAJ5NQAIZnJlcCh4NSkAAAAeAAAATQAAAA4AAABPAAAAFgD#####AQAAAAAOAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAA4AAABPAAAADgAAAFAAAAAfAP####8AAH8AAAQAAABRAAAB9AABAAAATgAAAAQAAABOAAAATwAAAFAAAABRAAAAFgD#####AAAAAAAQAAFJAMAIAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABP#AAAAAAAAAAAAABAAAAAAAAAAAAAAAWAP####8AAAAAAA4AAUoAwCgAAAAAAADAIAAAAAAAAAUAAAAACgAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAA7##########w=='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true);
    callback()
  }

  function modifFig (st) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété correction (booléen)
    // st contient aussi une propriété donnees qui prend en compte les valeurs à mettre à jour
    if (st.correction) {
      // on affiche la correction
      if (stor.typeFct === 'paire') {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'frep', st.donnees.fctPaire)
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'frep', st.donnees.fctImpaire)
      }
      stor.mtgAppLecteur.setColor(stor.mtg32svg, 82, stor.codesRgb[0], stor.codesRgb[1], stor.codesRgb[2], false)
    } else {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'f' + st.donnees.numPaire, st.donnees.fctPaire)
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'f' + st.donnees.numImpaire, st.donnees.fctImpaire)
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'f' + st.donnees.num1, st.donnees.fctQcq1)
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'f' + st.donnees.num2, st.donnees.fctQcq2)
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'frep', '50')
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // me.surchargeindication = false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
      Les textes présents dans la section
      Sont donc accessibles dans le code par ds.textes.consigne1
      Possibilité d’écrire dans le code :
      var lestextes = ds.textes;
      puis accès à l’aide de lestextes.consigne1
      */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Identifier graphiquement une fonction paire ou impaire',
        // on donne les phrases de la consigne
        consigne1: 'On a tracé dans le repère ci-contre les courbes représentatives de quatre fonctions.',
        consigne2_1: 'Une seule représente une fonction paire. Laquelle&nbsp;?',
        consigne2_2: 'Une seule représente une fonction impaire. Laquelle&nbsp;?',
        courbe: 'courbe',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: '',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'La courbe représentative d’une fonction paire admet l’axe des ordonnées comme axe de symétrie.',
        corr1_2: 'La courbe représentative d’une fonction impaire admet l’origine du repère comme centre de symétrie.',
        corr2: 'Donc la courbe cherchée est la courbe numéro £n.'
      },
      pe: 0
    }
  }

  function enonceMain () {
    const largFig = 430// on gère ici la largeur de la figure
    const hautFig = 430
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    const divMtg32 = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(divMtg32, { textAlign: 'center' })
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
    stor.lesDonnees = {}
    let numFcts = [1, 2, 3, 4]
    let pioche = j3pGetRandomInt(0, numFcts.length - 1)// pour choisir la fonction paire
    stor.lesDonnees.numPaire = numFcts[pioche]
    numFcts.splice(pioche, 1)
    pioche = j3pGetRandomInt(0, numFcts.length - 1)// pour choisir la fonction impaire
    stor.lesDonnees.numImpaire = numFcts[pioche]
    numFcts.splice(pioche, 1)
    numFcts = j3pShuffle(numFcts)
    stor.lesDonnees.num1 = numFcts[0]
    stor.lesDonnees.num2 = numFcts[1]

    // choix de la fonction paire
    // fonction carrée, chainette ou cosinus
    const choixFctPaire = []
    choixFctPaire.push((2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(3, 6) / 10 + '*x^2+(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5) * 0.5 + ')')
    const coefchainette = j3pGetRandomInt(8, 11) / 10
    choixFctPaire.push('(exp(' + coefchainette + '*x)+exp(-' + coefchainette + '*x))/' + j3pGetRandomInt(5, 9) * 0.5)
    choixFctPaire.push(j3pGetRandomInt(7, 9) * 0.5 + '*cos(x/' + j3pGetRandomInt(5, 9) * 0.5 + ')+(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 3) * 0.5 + ')')
    stor.lesDonnees.fctPaire = choixFctPaire[j3pGetRandomInt(0, 2)]
    // choix de la fonction impaire
    // fonction cube + linéaire ou fonction inverse + linéaire ou fonction sinus
    const choixFctImpaire = []
    choixFctImpaire.push((2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(8, 11) / 10 + '/x+(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(3, 7) / 10 + '*x)')
    choixFctImpaire.push((2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 3) / 10 + '*x^3+(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(3, 7) / 10 + '*x)')
    choixFctImpaire.push(j3pGetRandomInt(7, 9) * 0.5 + '*sin(x/' + j3pGetRandomInt(5, 9) * 0.5 + ')')
    stor.lesDonnees.fctImpaire = choixFctImpaire[j3pGetRandomInt(0, 2)]
    // Fonctions ni paires ni impaires
    const choixFctQcq = []
    choixFctQcq.push((2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(3, 6) / 10 + '*(x-(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 4) / 2 + '))^2+(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5) * 0.5 + ')')
    choixFctQcq.push((2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(8, 11) / 10 + '/(x-(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 4) / 2 + '))+(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5) * 0.5 + ')')
    choixFctQcq.push(j3pGetRandomInt(7, 9) * 0.5 + '*cos(x/' + j3pGetRandomInt(5, 9) * 0.5 + ')+(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5) / 10 + '*x)')
    choixFctQcq.push(j3pGetRandomInt(7, 9) * 0.5 + '*sin(x/' + j3pGetRandomInt(5, 9) * 0.5 + ')+(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5) * 0.5 + ')')
    choixFctQcq.push((2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 3) / 10 + '*(x-(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 4) / 2 + '))^3+(' + (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5) * 0.5 + ')')

    me.logIfDebug('choixFctPaire:', choixFctPaire, 'choixFctImpaire:', choixFctImpaire, 'choixFctQcq:', choixFctQcq)
    pioche = j3pGetRandomInt(0, choixFctQcq.length - 1)// pour choisir la 1ere fonction quelconque
    stor.lesDonnees.fctQcq1 = choixFctQcq[pioche]
    choixFctQcq.splice(pioche, 1)
    pioche = j3pGetRandomInt(0, choixFctQcq.length - 1)// pour choisir la éème fonction quelconque
    stor.lesDonnees.fctQcq2 = choixFctQcq[pioche]

    // on affiche la figure
    depart(function () {
      // on la modifie avec nos fonctions
      modifFig({ correction: false, donnees: stor.lesDonnees })
      me.logIfDebug('stor.casFigure:', stor.casFigure)
      // et on peut continuer avec le texte de l’énoncé
      // dans ce type de section, je mets plutôt les consignes à droite
      stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '5px' }) })
      stor.typeFct = stor.casFigure[me.questionCourante - 1]
      for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneurD, 'div')
      j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1)
      const consigne2 = (stor.typeFct === 'paire') ? ds.textes.consigne2_1 : ds.textes.consigne2_2
      j3pAffiche(stor.zoneCons1, '', consigne2)

      stor.couleurscb = ['red', 'blue', 'gray', 'maroon']
      const codesRgbInit = [[255, 0, 0], [0, 0, 255], [127, 127, 127], [127, 0, 0]]
      stor.idsRadio = []
      stor.idsLabelRadio = []
      for (let i = 1; i <= 4; i++) {
        stor['zoneRadio' + i] = j3pAddElt(stor.zoneCons3, 'div')
        stor.idsRadio.push(j3pGetNewId('radio' + i))
        stor.idsLabelRadio.push(j3pGetNewId('labelRadio' + i))
      }
      stor.nameRadio = j3pGetNewId('choix')
      j3pBoutonRadio(stor.zoneRadio1, stor.idsRadio[0], stor.nameRadio, 1, ds.textes.courbe + '&nbsp;1')
      j3pBoutonRadio(stor.zoneRadio2, stor.idsRadio[1], stor.nameRadio, 2, ds.textes.courbe + '&nbsp;2')
      j3pBoutonRadio(stor.zoneRadio3, stor.idsRadio[2], stor.nameRadio, 3, ds.textes.courbe + '&nbsp;3')
      j3pBoutonRadio(stor.zoneRadio4, stor.idsRadio[3], stor.nameRadio, 4, ds.textes.courbe + '&nbsp;4')
      stor.codesRgb = (stor.typeFct === 'paire') ? codesRgbInit[stor.lesDonnees.numPaire - 1] : codesRgbInit[stor.lesDonnees.numImpaire - 1]

      for (let i = 0; i < stor.couleurscb.length; i++) j3pElement('label' + stor.idsRadio[i]).style.color = stor.couleurscb[i]
      /// //////////////////////////////////////
      /* FIN DU CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////////

      // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
      stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')

      // Obligatoire
      me.finEnonce()
    })
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.5 })

        // par convention, me.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

        // création de la zone mtg32
        const tabParite = ['paire', 'impaire']
        stor.casFigure = []
        for (let i = 1; i < ds.nbrepetitions / 2; i++) stor.casFigure = stor.casFigure.concat(tabParite)
        if (ds.nbrepetitions % 2 === 1) {
          const pioche = j3pGetRandomInt(0, tabParite.length - 1)
          stor.casFigure.push(tabParite[pioche])
        }
        stor.casFigure = j3pShuffle(stor.casFigure)
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const reponse = { aRepondu: false, bonneReponse: false }
      stor.repDonnee = j3pBoutonRadioChecked(stor.nameRadio)[0]
      reponse.aRepondu = (stor.repDonnee > -1)
      let bonNumRep
      if (reponse.aRepondu) {
        // on vérifie si la réponse choisie est correcte :
        bonNumRep = (stor.typeFct === 'paire') ? stor.lesDonnees.numPaire : stor.lesDonnees.numImpaire
        me.logIfDebug('bonNumRep:', bonNumRep, ' btnChecked :', j3pBoutonRadioChecked(stor.nameRadio))
        reponse.bonneReponse = (bonNumRep === stor.repDonnee + 1)
      }

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        this.reponseManquante(stor.zoneCorr)
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (reponse.bonneReponse) {
          me._stopTimer()
          me.score++
          for (let i = 1; i <= 4; i++) j3pDesactive(stor.idsRadio[i - 1])
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          afficheCorrection(true)
          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()

            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            for (let i = 1; i <= 4; i++) j3pDesactive(stor.idsRadio[i - 1])
            me.cacheBoutonValider()
            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(false)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              for (let i = 1; i <= 4; i++) j3pDesactive(stor.idsRadio[i - 1])
              j3pBarre('label' + stor.idsRadio[stor.repDonnee])
              // je souligne (voire en gras) les bonnes réponses
              const txtInit = j3pElement('label' + stor.idsRadio[bonNumRep - 1]).innerHTML
              const posInit = txtInit.indexOf('>') + 1
              const posFin = txtInit.indexOf('&')
              j3pElement('label' + stor.idsRadio[bonNumRep - 1]).innerHTML = txtInit.substring(0, posInit) + '<b><u>' + txtInit.substring(posInit, posFin) + '</u></b>' + txtInit.substring(posFin)
              afficheCorrection(false)
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
