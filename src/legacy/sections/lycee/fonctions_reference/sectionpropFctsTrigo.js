import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pDetruit, j3pElement, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomInt, j3pMonome, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @author Rémi DENIAUD
 * @since juin 2022
 * @fileOverview Cette section est juste un QCM la parité et la périodicité de fonctions trigo
 */

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes']
  ]
}

/**
 * section simulation
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {

    // Nombre de répétitions de l’exercice
    nbrepetitions: 3,
    indication: '',
    textes: {
      titreExo: 'Parité et périodicité de fonctions',
      consigne1: 'Soit $£f$ la fonction définie sur $£d$ par $£f(x)=£{fx}$.',
      consigne2: 'Cette fonction est #1#.',
      choixParite: 'paire|impaire|ni&nbsp;paire,&nbsp;ni&nbsp;impaire',
      consigne3: 'De plus, est-elle périodique de période $£p$&nbsp;?',
      ouiNon: 'oui|non',
      comment1: '',
      corr1_1: 'Pour tout réel $x$,',
      corr1_2: 'Pour tout réel $x$ non nul,',
      corr2_1: 'Pour $x=£{contreExPaire}$, $£{fpaire1}$ et $£{fpaire2}$ ainsi $£{pbPaire}$ et la fonction n’est pas paire.',
      corr2_2: 'Pour $x=£{contreExImpaire}$, $£{fimpaire1}$ et $£{fimpaire2}$ ainsi $£{pbImpaire}$ et la fonction n’est pas impaire.',
      corr2_3: 'Pour $x=£{contreExPaire}$, $£{fpaire1}$ et $£{fpaire2}$ ainsi $£{pbPaire}$ et $£{pbImpaire}$. Donc la fonction n’est ni paire ni impaire.',
      corr3: 'Pour $x=£{contreExPeriode}$, $£{e1}$ et $£{e2}$ ainsi $£{pbPeriode}$ et la fonction n’est pas périodique de période $£p$.',
      corr4_1: 'Donc la fonction $£f$ est paire.',
      corr4_2: 'Donc la fonction $£f$ est impaire.'
    }
  }
} // getDonnees

/**
 * Retourne les données pour chaque sujet
 * @private
 * @return {Object}utilise la propriété de concentration d’une variable aléatoire autour de son espérance
 */
function genereAlea (casFigure) {
  const obj = {}
  obj.f = j3pGetRandomElt(['f', 'g', 'h'])
  obj.d = '\\R'
  // obj.parite prendra la valeur 0, 1 ou 2 selon que la fonction est paire, impaire ou ni l’un, ni l’autre
  // obj.periodicite prendra la valeur 0 ou 1 selon que la fonction est périodique ou non
  obj.contreExPaire = ''
  obj.contreExImpaire = ''
  obj.contreExPeriode = ''
  switch (casFigure) {
    case 1:
      // f(x)=sin^2(kx)
      {
        const k = j3pGetRandomElt([3, 2])
        obj.fx = '\\sin^2(' + j3pMonome(1, 1, k) + ')'
        obj.p = '\\pi'
        obj.parite = 0
        obj.periodicite = 0
        obj.fMoinsx = [
          obj.f + '(-x)=\\sin^2(' + j3pMonome(1, 1, -k) + ')=\\left(\\sin(' + j3pMonome(1, 1, -k) + ')\\right)^2',
          obj.f + '(-x)=\\left(-\\sin(' + j3pMonome(1, 1, k) + ')\\right)^2=' + obj.f + '(x)'
        ]
        obj.fxPlusPi = [
          obj.f + '(x+\\pi)=\\sin^2(' + j3pMonome(1, 1, k) + j3pMonome(2, 1, k, '\\pi') + ')=\\left(\\sin(' + j3pMonome(1, 1, k) + j3pMonome(2, 1, k, '\\pi') + ')\\right)^2',
          obj.f + '(x+\\pi)=\\left(' + ((k === 2) ? '' : '-') + '\\sin(' + j3pMonome(1, 1, k) + ')\\right)^2=' + ((k === 2) ? obj.f + '(x)' : '\\left(\\sin(' + j3pMonome(1, 1, k) + ')\\right)^2=' + obj.f + '(x)')
        ]
      }
      break
    case 2:
      // f(x)=cos(2x)
      obj.fx = '\\cos(2x)'
      obj.p = '\\pi'
      obj.parite = 0
      obj.periodicite = 0
      obj.fMoinsx = [
        obj.f + '(-x)=\\cos(' + j3pMonome(1, 1, -2) + ')=\\cos(2x)=' + obj.f + '(x)'
      ]
      obj.fxPlusPi = [
        obj.f + '(x+\\pi)=\\cos(2x+2\\pi)=\\cos(2x)=' + obj.f + '(x)'
      ]
      break
    case 3:
      // f(x)= sin(x)/x ou cos(x)/x
      {
        const choixQuest = j3pGetRandomBool()
        obj.fx = (choixQuest) ? '\\frac{\\sin x}{x}' : '\\frac{\\cos x}{x}'
        obj.p = '2\\pi'
        obj.parite = (choixQuest) ? 0 : 1
        obj.periodicite = 1
        obj.fMoinsx = [
          obj.f + '(-x)=\\frac{' + ((choixQuest) ? '\\sin' : '\\cos') + '(-x)}{-x}=\\frac{' + ((choixQuest) ? '-\\sin' : '\\cos') + ' x}{-x}=' + ((choixQuest) ? '\\frac{\\sin x}{x}=' + obj.f + '(x)' : '-\\frac{\\cos x}{x}=-' + obj.f + '(x)')
        ]
        obj.fxPlusPi = [
          obj.f + '(x+2\\pi)=\\frac{' + ((choixQuest) ? '\\sin' : '\\cos') + '(x+2\\pi)}{x+2\\pi}=\\frac{' + ((choixQuest) ? '\\sin' : '\\cos') + ' x}{x+2\\pi}'
        ]
        obj.contreExPeriode = (choixQuest) ? '\\frac{\\pi}{2}' : '\\pi'
        obj.val2 = (choixQuest) ? '\\frac{5\\pi}{2}' : '3\\pi'
        obj.pbPeriode = obj.f + '\\left(' + obj.contreExPeriode + '\\right)\\neq ' + obj.f + '\\left(' + obj.val2 + '\\right)'
        if (choixQuest) {
          obj.e1 = obj.f + '\\left(\\frac{\\pi}{2}\\right)=\\frac{1}{\\frac{\\pi}{2}}=\\frac{2}{\\pi}'
          obj.e2 = obj.f + '\\left(\\frac{5\\pi}{2}\\right)=\\frac{1}{\\frac{5\\pi}{2}}=\\frac{2}{5\\pi}'
        } else {
          obj.e1 = obj.f + '\\left(\\pi\\right)=\\frac{-1}{\\pi}'
          obj.e2 = obj.f + '\\left(3\\pi\\right)=\\frac{-1}{3\\pi}'
        }
      }
      obj.d = '\\R^*'
      break
    case 4:
      // f(x)= sin(kx+fraction de pi) ou cos(kx+fraction de pi)
      {
        const choixQuest = j3pGetRandomBool()
        const k = j3pGetRandomInt(1, 2)
        const choixFracPi = ['\\frac{\\pi}{3}', '\\frac{2\\pi}{3}', '\\frac{\\pi}{4}', '\\frac{3\\pi}{4}']
        // On ajoute ou soustrait pi/2
        // const addPiSur2 = ['\\frac{5\\pi}{6}', '\\frac{7\\pi}{6}', '\\frac{3\\pi}{4}', '\\frac{5\\pi}{4}']
        const sinAddPiSur2 = ['\\frac{1}{2}', '-\\frac{1}{2}', '\\frac{\\sqrt{2}}{2}', '-\\frac{\\sqrt{2}}{2}']
        const sinAddPiSur3ou4 = ['\\frac{\\sqrt{3}}{2}', '0', '1', '-1']
        const cosAddPiSur2 = ['-\\frac{\\sqrt{3}}{2}', '-\\frac{\\sqrt{3}}{2}', '-\\frac{\\sqrt{2}}{2}', '-\\frac{\\sqrt{2}}{2}']
        const cosAddPiSur3ou4 = ['\\frac{1}{2}', '-1', '0', '-1']
        // const subPiSur2 = ['-\\frac{\\pi}{6}', '\\frac{\\pi}{6}', '-\\frac{\\pi}{4}', '\\frac{\\pi}{4}']
        const sinSubPiSur2 = ['-\\frac{1}{2}', '\\frac{1}{2}', '-\\frac{\\sqrt{2}}{2}', '\\frac{\\sqrt{2}}{2}']
        const cosSubPiSur2 = ['\\frac{\\sqrt{3}}{2}', '\\frac{\\sqrt{3}}}{2}', '\\frac{\\sqrt{2}}{2}', '\\frac{\\sqrt{2}}{2}']
        const sinSubPiSur3ou4 = ['0', '\\frac{\\sqrt{3}}{2}', '0', '1']
        const cosSubPiSur3ou4 = ['1', '\\frac{1}{2}', '1', '0']
        const pioche = j3pGetRandomInt(0, choixFracPi.length - 1)
        const fracPi = choixFracPi[pioche]
        obj.fx = ((choixQuest) ? '\\sin' : '\\cos') + '\\left(' + j3pMonome(1, 1, k) + '+' + fracPi + '\\right)'
        obj.p = '\\pi'
        obj.parite = 2
        obj.periodicite = (k === 1) ? 1 : 0
        obj.fMoinsx = [
          obj.f + '(-x)=' + ((choixQuest) ? '\\sin' : '\\cos') + '\\left(' + j3pMonome(1, 1, -k) + '+' + fracPi + '\\right)'
        ]
        obj.contreExPaire = (k === 1) ? '\\frac{\\pi}{2}' : '\\frac{\\pi}{4}'
        obj.contreExImpaire = (pioche <= 1) ? '\\frac{\\pi}{' + ((k === 1) ? 3 : 6) + '}' : '\\frac{\\pi}{' + ((k === 1) ? 4 : 8) + '}' // (k === 1) ? '\\frac{\\pi}{2}' : '\\frac{\\pi}{4}'
        obj.valpaire = '-' + obj.contreExPaire
        obj.valimpaire = '-' + obj.contreExImpaire
        if (choixQuest) {
          obj.fpaire1 = obj.f + '\\left(' + obj.contreExPaire + '\\right)=\\sin\\left(' + ((k === 2) ? '2\\times \\left(' : '') + obj.contreExPaire + ((k === 2) ? '\\right)' : '') + '+' + fracPi + '\\right)=' + sinAddPiSur2[pioche]
          obj.fpaire2 = obj.f + '\\left(' + obj.valpaire + '\\right)=\\sin\\left(' + ((k === 2) ? '2\\times \\left(' : '') + obj.valpaire + ((k === 2) ? '\\right)' : '') + '+' + fracPi + '\\right)=' + sinSubPiSur2[pioche]
          obj.fimpaire1 = obj.f + '\\left(' + obj.contreExImpaire + '\\right)=\\sin\\left(' + ((k === 2) ? '2\\times \\left(' : '') + obj.contreExImpaire + ((k === 2) ? '\\right)' : '') + '+' + fracPi + '\\right)=' + sinAddPiSur3ou4[pioche]
          obj.fimpaire2 = obj.f + '\\left(' + obj.valimpaire + '\\right)=\\sin\\left(' + ((k === 2) ? '2\\times \\left(' : '') + obj.valimpaire + ((k === 2) ? '\\right)' : '') + '+' + fracPi + '\\right)=' + sinSubPiSur3ou4[pioche]
        } else {
          obj.fpaire1 = obj.f + '\\left(' + obj.contreExPaire + '\\right)=\\cos\\left(' + ((k === 2) ? '2\\times \\left(' : '') + obj.contreExPaire + ((k === 2) ? '\\right)' : '') + '+' + fracPi + '\\right)=' + cosAddPiSur2[pioche]
          obj.fpaire2 = obj.f + '\\left(' + obj.valpaire + '\\right)=\\cos\\left(' + ((k === 2) ? '2\\times \\left(' : '') + obj.valpaire + ((k === 2) ? '\\right)' : '') + '+' + fracPi + '\\right)=' + cosSubPiSur2[pioche]
          obj.fimpaire1 = obj.f + '\\left(' + obj.contreExImpaire + '\\right)=\\cos\\left(' + ((k === 2) ? '2\\times \\left(' : '') + obj.contreExImpaire + ((k === 2) ? '\\right)' : '') + '+' + fracPi + '\\right)=' + cosAddPiSur3ou4[pioche]
          obj.fimpaire2 = obj.f + '\\left(' + obj.valimpaire + '\\right)=\\cos\\left(' + ((k === 2) ? '2\\times \\left(' : '') + obj.valimpaire + ((k === 2) ? '\\right)' : '') + '+' + fracPi + '\\right)=' + cosSubPiSur3ou4[pioche]
        }
        obj.pbPaire = obj.f + '\\left(' + obj.valpaire + '\\right)\\neq ' + obj.f + '\\left(' + obj.contreExPaire + '\\right)'
        obj.pbImpaire = obj.f + '\\left(' + obj.valimpaire + '\\right)\\neq -' + obj.f + '\\left(' + obj.contreExImpaire + '\\right)'
        obj.fxPlusPi = [
          obj.f + '(x+\\pi)=' + ((choixQuest) ? '\\sin' : '\\cos') + '\\left(' + j3pMonome(1, 1, k) + j3pMonome(2, 1, k, '\\pi') + '+' + fracPi + '\\right)'
        ]
        if (k === 1) {
          // La fonction n’est pas pi-périodique
          const sinFrac = ['\\frac{\\sqrt{3}}{2}', '\\frac{\\sqrt{3}}{2}', '\\frac{\\sqrt{2}}{2}', '\\frac{\\sqrt{2}}{2}']
          const cosFrac = ['\\frac{1}{2}', '-\\frac{1}{2}', '\\frac{\\sqrt{2}}{2}', '-\\frac{\\sqrt{2}}{2}']
          const addPi = ['\\frac{4\\pi}{3}', '\\frac{5\\pi}{3}', '\\frac{5\\pi}{4}', '\\frac{7\\pi}{4}']
          const sinAddPi = ['-\\frac{\\sqrt{3}}{2}', '-\\frac{\\sqrt{3}}{2}', '-\\frac{\\sqrt{2}}{2}', '-\\frac{\\sqrt{2}}{2}']
          const cosAddPi = ['-\\frac{1}{2}', '\\frac{1}{2}', '-\\frac{\\sqrt{2}}{2}', '\\frac{\\sqrt{2}}{2}']
          obj.contreExPeriode = '0'
          obj.val2 = '\\pi'
          if (choixQuest) {
            obj.e1 = obj.f + '\\left(' + obj.contreExPeriode + '\\right)=\\sin\\left(' + fracPi + '\\right)=' + sinFrac[pioche]
            obj.e2 = obj.f + '\\left(' + obj.val2 + '\\right)=\\sin\\left(' + obj.val2 + '+' + fracPi + '\\right)=\\sin\\left(' + addPi[pioche] + '\\right)=' + sinAddPi[pioche]
          } else {
            obj.e1 = obj.f + '\\left(' + obj.contreExPeriode + '\\right)=\\cos\\left(' + fracPi + '\\right)=' + cosFrac[pioche]
            obj.e2 = obj.f + '\\left(' + obj.val2 + '\\right)=\\cos\\left(' + obj.val2 + '+' + fracPi + '\\right)=\\cos\\left(' + addPi[pioche] + '\\right)=' + cosAddPi[pioche]
          }
          obj.pbPeriode = obj.f + '\\left(' + obj.contreExPeriode + '\\right)\\neq ' + obj.f + '\\left(' + obj.val2 + '\\right)'
        } else {
          // elle est pi-périodique
          obj.fxPlusPi[0] += '=' + ((choixQuest) ? '\\sin' : '\\cos') + '\\left(' + j3pMonome(1, 1, k) + '+' + fracPi + '\\right)=' + obj.f + '(x)'
        }
      }
      break
    case 5:
      // f(x)= sin(x)-x ou cos(x)-x
      {
        const choixQuest = j3pGetRandomBool()
        obj.fx = (choixQuest) ? '\\sin(x)-x' : '\\cos(x)-x'
        obj.p = '2\\pi'
        obj.parite = (choixQuest) ? 1 : 2
        obj.periodicite = 1
        obj.fMoinsx = [
          obj.f + '(-x)=' + ((choixQuest)
            ? '\\sin(-x)-(-x)=-sin(x)+x=-' + obj.f + '(x)'
            : '\\cos(-x)-(-x)=\\cos(x)+x')
        ]
        if (!choixQuest) {
          obj.contreExPaire = '\\pi'
          obj.fpaire1 = obj.f + '(\\pi)=\\cos(\\pi)-\\pi=-1-\\pi'
          obj.fpaire2 = obj.f + '(-\\pi)=\\cos(-\\pi)-(-\\pi)=-1+\\pi'
          obj.pbPaire = obj.f + '(-\\pi)\\neq ' + obj.f + '(\\pi)'
          obj.pbImpaire = obj.f + '(-\\pi)\\neq -' + obj.f + '(\\pi)'
        }
        obj.fxPlusPi = [
          obj.f + '(x+2\\pi)=' + ((choixQuest) ? '\\sin' : '\\cos') + '(x+2\\pi) -x-2\\pi=' + ((choixQuest) ? '\\sin' : '\\cos') + ' x-x-2\\pi'
        ]
        obj.contreExPeriode = '0'
        obj.val2 = '2\\pi'
        obj.pbPeriode = obj.f + '\\left(' + obj.contreExPeriode + '\\right)\\neq ' + obj.f + '\\left(' + obj.val2 + '\\right)'
        if (choixQuest) {
          obj.e1 = obj.f + '(0)=\\sin(0)-0=0'
          obj.e2 = obj.f + '(2\\pi)=\\sin(2\\pi)-2\\pi=-2\\pi'
        } else {
          obj.e1 = obj.f + '(0)=\\cos(0)-0=1'
          obj.e2 = obj.f + '(2\\pi)=\\cos(2\\pi)-2\\pi=1-2\\pi'
        }
      }
      break
    case 6:
      // f(x)= sin(x)-cos(x) ou cos(x)-sin(x)
      {
        const choixQuest = j3pGetRandomBool()
        obj.fx = (choixQuest) ? '\\sin(x)-\\cos(x)' : '\\cos(x)\\sin(x)'
        obj.p = '\\pi'
        obj.parite = (choixQuest) ? 2 : 1
        obj.periodicite = (choixQuest) ? 1 : 0
        obj.fMoinsx = [
          obj.f + '(-x)=' + ((choixQuest)
            ? '\\sin(-x)-\\cos(-x)=-sin(x)-\\cos(x)'
            : '\\cos(-x)\\sin(-x)=\\cos(x)\\left(-\\sin(x)\\right)=-\\cos(x)\\sin(x)=-' + obj.f + '(x)')
        ]
        if (choixQuest) {
          // Fonction ni paire ni impaire
          obj.contreExPaire = '\\frac{\\pi}{4}'
          obj.fpaire1 = obj.f + '\\left(\\frac{\\pi}{4}\\right)=\\sin\\left(\\frac{\\pi}{4}\\right)-\\cos\\left(\\frac{\\pi}{4}\\right)=\\frac{\\sqrt{2}}{2}-\\frac{\\sqrt{2}}{2}=0'
          obj.fpaire2 = obj.f + '\\left(-\\frac{\\pi}{4}\\right)=\\sin\\left(-\\frac{\\pi}{4}\\right)-\\cos\\left(-\\frac{\\pi}{4}\\right)=-\\frac{\\sqrt{2}}{2}-\\frac{\\sqrt{2}}{2}=-\\sqrt{2}'
          obj.pbPaire = obj.f + '\\left(-\\frac{\\pi}{4}\\right)\\neq ' + obj.f + '\\left(\\frac{\\pi}{4}\\right)'
          obj.pbImpaire = obj.f + '\\left(-\\frac{\\pi}{4}\\right)\\neq -' + obj.f + '\\left(\\frac{\\pi}{4}\\right)'
        }
        obj.fxPlusPi = [
          obj.f + '(x+\\pi)=' + ((choixQuest)
            ? '\\sin(x+\\pi)-\\cos(x+\\pi)=-sin(x)+\\cos(x)'
            : '\\cos(x+\\pi)\\sin(x+\\pi)=-\\cos(x)\\left(-\\sin(x)\\right)=\\cos(x)\\sin(x)=' + obj.f + '(x)')
        ]
        if (choixQuest) {
          // Fonction non pi-périodique
          obj.contreExPeriode = '0'
          obj.val2 = '\\pi'
          obj.pbPeriode = obj.f + '\\left(' + obj.contreExPeriode + '\\right)\\neq ' + obj.f + '\\left(' + obj.val2 + '\\right)'
          obj.e1 = obj.f + '(0)=\\sin(0)-\\cos(0)=0-1=-1'
          obj.e2 = obj.f + '(\\pi)=\\sin(\\pi)-\\cos(\\pi)=0-(-1)=1'
        }
      }
      break
  }
  // console.log('obj:', obj)
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  // Construction de la page
  me.construitStructurePage()
  me.afficheTitre(me.donneesSection.textes.titreExo)
  const stor = me.storage
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
  stor.listeQuest = []
  let typeQuest = [1, 2, 3, 4, 5, 6]
  const typeQuestInit = [1, 2, 3, 4, 5, 6]
  for (let i = 1; i <= me.donneesSection.nbrepetitions; i++) {
    const alea = j3pGetRandomInt(0, typeQuest.length - 1)
    stor.listeQuest.push(typeQuest[alea])
    typeQuest.splice(alea, 1)
    if (typeQuest.length === 0) typeQuest = [...typeQuestInit]
  }
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // puis de nos éléments dans le dom
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.conteneur = j3pAddElt(stor.elts.divEnonce, 'p')
  stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
  stor.laQuest = stor.listeQuest[me.questionCourante - 1]
  stor.objDonnees = genereAlea(stor.laQuest)
  // on affiche l’énoncé
  ;[1, 2].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })
  j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, stor.objDonnees)
  const tabParite = [''].concat(ds.textes.choixParite.split('|'))
  const elt = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, {
    liste1: { texte: tabParite }
  })
  stor.zoneInput = elt.selectList[0]
  ;[4, 5, 6].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div') })
  j3pAffiche(stor.zoneCons5, '', ds.textes.consigne3, stor.objDonnees)
  const tabOuiNon = ds.textes.ouiNon.split('|')
  // zoneCons5, je mets les boutons radio
  stor.nameRadio2 = j3pGetNewId('choixPeriode')
  stor.idsRadio2 = ['1', '2'].map(i => j3pGetNewId('radioPeriode' + i))
  const espacetxt = '&nbsp;&nbsp;&nbsp;&nbsp;'
  ;[0, 1].forEach(i => {
    j3pAffiche(stor.zoneCons6, '', espacetxt)
    j3pBoutonRadio(stor.zoneCons6, stor.idsRadio2[i], stor.nameRadio2, i, tabOuiNon[i])
  })

  stor.divCorrection = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '10px' }) })

  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me) {
  const stor = me.storage
  const ds = me.donneesSection
  j3pDetruit(stor.zoneCalc)
  const zoneExpliParite = j3pAddElt(stor.zoneCons4, 'div', '', { style: me.styles.petit.correction })
  const zoneExpliPeriode = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
  j3pStyle(zoneExpliParite, { paddingTop: '10px', color: (stor.bonneRepParite) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  j3pStyle(zoneExpliPeriode, { paddingTop: '10px', color: (stor.bonneRepPeriode) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  const phrase1 = j3pAddElt(zoneExpliParite, 'div')
  j3pAffiche(phrase1, '', (stor.laQuest === 3) ? ds.textes.corr1_2 : ds.textes.corr1_1)
  stor.objDonnees.fMoinsx.forEach((ligne, i) => {
    stor['zoneExpliParite' + i] = j3pAddElt(zoneExpliParite, 'div')
    j3pAffiche(stor['zoneExpliParite' + i], '', '$' + ligne + '$')
  })
  if (stor.objDonnees.contreExPaire !== '') {
    if (stor.objDonnees.contreExImpaire === '') {
      // on a le même contre-exemple pour nier le côté paire et le côté impair
      const zoneContreEx1 = j3pAddElt(zoneExpliParite, 'div')
      j3pAffiche(zoneContreEx1, '', ds.textes.corr2_3, stor.objDonnees)
    } else {
      const zoneContreEx1 = j3pAddElt(zoneExpliParite, 'div')
      j3pAffiche(zoneContreEx1, '', ds.textes.corr2_1, stor.objDonnees)
      const zoneContreEx2 = j3pAddElt(zoneExpliParite, 'div')
      j3pAffiche(zoneContreEx2, '', ds.textes.corr2_2, stor.objDonnees)
    }
  } else {
    // C’est que la fonction est paire ou impaire
    const corrParite = ds.textes['corr4_' + (stor.objDonnees.parite + 1)]
    const zoneParite = j3pAddElt(zoneExpliParite, 'div')
    j3pAffiche(zoneParite, '', corrParite, stor.objDonnees)
  }
  const phrase2 = j3pAddElt(zoneExpliPeriode, 'div')
  j3pAffiche(phrase2, '', (stor.laQuest === 3) ? ds.textes.corr1_2 : ds.textes.corr1_1)
  stor.objDonnees.fxPlusPi.forEach((ligne, i) => {
    stor['zoneExpliPeriode' + i] = j3pAddElt(zoneExpliPeriode, 'div')
    j3pAffiche(stor['zoneExpliPeriode' + i], '', '$' + ligne + '$')
  })
  if (stor.objDonnees.contreExPeriode !== '') {
    const zoneContreEx2 = j3pAddElt(zoneExpliPeriode, 'div')
    j3pAffiche(zoneContreEx2, '', ds.textes.corr3, stor.objDonnees)
  }
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  stor.choixRadio = stor.zoneInput.selectedIndex
  stor.choixRadio2 = j3pBoutonRadioChecked(stor.nameRadio2)[0]
  const reponse = { aRepondu: false, bonneReponse: false }
  reponse.aRepondu = (stor.choixRadio > 0) && (stor.choixRadio2 > -1)
  if (reponse.aRepondu) {
    stor.bonneRepParite = stor.choixRadio === stor.objDonnees.parite + 1
    stor.bonneRepPeriode = stor.choixRadio2 === stor.objDonnees.periodicite
    reponse.bonneReponse = stor.bonneRepParite && stor.bonneRepPeriode
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
} // getScore

function correction (me) {
  const score = getScore(me) // true|false|null, null si pas répondu
  const stor = me.storage
  const ds = me.donneesSection
  if (score !== null) {
    let lescore = (stor.bonneRepParite) ? 0.5 : 0
    lescore += (stor.bonneRepPeriode) ? 0.5 : 0
    me.score += lescore
  }
  j3pEmpty(stor.divCorrection)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.divCorrection)
    return me.finCorrection()
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if (!score) {
    me.reponseKo(stor.divCorrection)
  }
  if (!score && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.divCorrection, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }
  stor.zoneInput.style.color = (stor.bonneRepParite) ? me.styles.cbien : me.styles.cfaux
  j3pElement('label' + stor.idsRadio2[stor.choixRadio2]).style.color = (stor.bonneRepPeriode)
    ? me.styles.cbien
    : me.styles.cfaux
  stor.idsRadio2.forEach(elt => j3pDesactive(elt))
  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.divCorrection)
    j3pAddElt(stor.divCorrection, 'br')
  } else {
    // ko, temps dépassé ou dernier essai
    if (!stor.bonneRepParite) j3pBarre(stor.zoneInput)
    if (!stor.bonneRepPeriode) {
      j3pBarre('label' + stor.idsRadio2[stor.choixRadio2])
      j3pElement('label' + stor.idsRadio2[stor.objDonnees.periodicite]).style.color = me.styles.moyen.correction.color
    }
    if (me.isElapsed) {
      me.reponseKo(stor.divCorrection, tempsDepasse)
      // on ajoute cfaux s’il a répondu qqchose
      if (score !== null) {
        j3pAddElt(stor.divCorrection, 'br')
        me.reponseKo(stor.divCorrection, null, true)
      }
    } else {
      me.reponseKo(stor.divCorrection)
    }
  }
  j3pDesactive(stor.zoneInput)
  afficheCorrection(me)
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) me.etat = 'enonce'
  // rien à faire quand la section est terminée.
  me.finNavigation(true)
}
