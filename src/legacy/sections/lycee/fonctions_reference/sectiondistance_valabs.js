import { j3pAddElt, j3pDetruit, j3pExtraireCoefsFctAffine, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexMonome, j3pGetLatexProduit } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    février 2016
    Dans cette section, on traduit une distance entre x et un nombre comme une valeur absolue
    Deux cas de figure peuvent être présentés par cette section
        - juste la distance entre x et a
        - la distance entre x et a égale, inférieure ou supérieure à un nombre
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['type_quest', 1, 'entier', 'Deux types de questions peuvent être posés :<br/>- 1 : on ne demande que la distance entre x et un nombre ;<br/>- 2 : on demande de plus que cette distance soit égale, inférieure ou supérieure à un nombre.'],
    ['imposer_nbs', ['', '', ''], 'array', 'Pour chaque répétition, on peut imposer les nombres dont on cherche la distance à x.'],
    ['imposer_eg_ineg', ['', '', ''], 'array', 'Pour chaque répétition, on peut imposer l’(in)égalité attendue, éventuellement suivie du nombre.<br/>Par exemple "=", "&gt;=", voire, si on veut préciser la valeur, : "=1" ou "&lt;1.3".']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Traduire la distance entre deux réels',
  // on donne les phrases de la consigne
  consigne1_1: 'Traduire la distance décrite ci-dessous à l’aide d’une valeur absolue :',
  consigne1_2: 'Traduire la phrase suivante à l’aide d’une valeur absolue.',
  consigne2_0: '"la distance de $x$ à $£a$ " :',
  consigne2_1: 'La distance de $x$ à $£a$ vaut $£b$.',
  consigne2_2: 'La distance de $x$ à $£a$ est strictement inférieure à $£b$.',
  consigne2_3: 'La distance de $x$ à $£a$ est strictement supérieure à $£b$.',
  consigne2_4: 'La distance de $x$ à $£a$ est inférieure ou égale à $£b$.',
  consigne2_5: 'La distance de $x$ à $£a$ est supérieure ou égale à $£b$.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Il devrait y avoir des valeurs absolues dans la réponse !',
  comment2: 'Il ne devrait y avoir que deux valeurs absolues !',
  comment3: 'L’expression à l’intérieur des valeurs absolue n’est pas correcte !',
  comment4: 'L’expression à l’intérieur des valeurs absolues est par contre correcte !',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'La distance entre deux nombre réels est la valeur absolue de leur différence.',
  corr2: 'Ainsi la réponse est $£r$.',
  corr3_1: 'Comme cette distance doit être égale à $£v$, la réponse est $£r$.',
  corr3_2: 'Comme cette distance doit être strictement inférieure à $£v$, la réponse est $£r$.',
  corr3_3: 'Comme cette distance doit être strictement supérieure à $£v$, la réponse est $£r$.',
  corr3_4: 'Comme cette distance doit être inférieure ou égale à $£v$, la réponse est $£r$.',
  corr3_5: 'Comme cette distance doit être supérieure ou égale à $£v$, la réponse est $£r$.'
}
/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCons4)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    const zoneExpli2 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1, {
      styletexte: {}
    })
    if (ds.type_quest === 1) {
      j3pAffiche(zoneExpli2, '', textes.corr2,
        { r: '|x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', stor.nb1)) + '|' })
    } else {
      const signe = stor.signe
      const maCorrection3 = (signe === '=') ? textes.corr3_1 : (signe === '<') ? textes.corr3_2 : (signe === '>') ? textes.corr3_3 : (signe === '\\leq') ? textes.corr3_4 : textes.corr3_5
      j3pAffiche(zoneExpli2, '', maCorrection3, {
        v: stor.nb2,
        r: '|x' + j3pGetLatexMonome(2, 0, j3pGetLatexProduit('-1', stor.nb1)) + '|' + signe + stor.nb2
      })
    }
  }

  function enonceMain () {
    const nb1 = stor.nbs_imposes[me.questionCourante - 1] !== '' ? stor.nbs_imposes[me.questionCourante - 1] : (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 7)
    let signe, nb2
    if (ds.type_quest === 2) {
      signe = stor.imposer_eg_ineg[me.questionCourante - 1]
      if (signe === '<=') signe = '\\le'
      if (signe === '>=') signe = '\\ge'
      const tabSignes = ['=', '>', '<', '\\le', '\\ge']
      if (signe === '') signe = tabSignes[j3pGetRandomInt(0, (tabSignes.length - 1))]
      if (stor.imposer_nbdist[me.questionCourante - 1] !== '') {
        nb2 = stor.imposer_nbdist[me.questionCourante - 1]
      } else {
        do {
          nb2 = j3pGetRandomInt(1, 7)
        } while (Math.abs(Math.abs(nb1) - Math.abs(nb2)) < Math.pow(10, -12))
      }
    } else {
      signe = ''
      nb2 = ''
    }
    stor.nb1 = nb1
    stor.signe = signe
    stor.nb2 = nb2
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 4; ++i) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    //
    const maConsigne1 = (ds.type_quest === 1) ? textes.consigne1_1 : textes.consigne1_2
    j3pAffiche(stor.zoneCons1, '', maConsigne1, {
      styletexte: {}
    })
    const tabIneq = ['=', '<', '>', '\\le', '\\ge']
    const maConsigne2 = (ds.type_quest === 1) ? textes.consigne2_0 : textes['consigne2_' + (tabIneq.indexOf(signe) + 1)]
    // cette fois-ci, je n’ai qu’une zone de saisie à compléter, c’est une zone mathquill : inputmq1
    j3pAffiche(stor.zoneCons2, '', maConsigne2, {
      a: nb1,
      b: nb2
    })
    const elt = j3pAffiche(stor.zoneCons3, '', '&1&', {
      inputmq1: { texte: '' }
    })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, 'x\\-+*/|=<>\\d', { commandes: ['infegal', 'supegal'] }) // on n’autorise que les lettres majuscules
    stor.zoneCons4.style.paddingTop = '15px'
    j3pPaletteMathquill(stor.zoneCons4, stor.zoneInput, {
      liste: ['|', '=', '>', '<', 'infegal', 'supegal']
    })
    stor.zoneInput.typeReponse = ['texte']
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire 'à la main'.
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        ds = me.donneesSection

        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

        me.afficheTitre(textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        // type_quest doit valoir 1 ou 2 donc je m’assure que c’est bien le cas
        ds.type_quest = (ds.type_quest === 2) ? 2 : 1
        // je fais en sorte que les nombres éventuellement imposés soient bien compris
        stor.nbs_imposes = []
        stor.imposer_eg_ineg = [] // Dans ce tableau on ne récupère que les signes d’égalité ou inégalité
        stor.imposer_nbdist = [] // Dans celui-ci on ne récupère que la distance
        for (let i = 0; i < ds.nbrepetitions; i++) {
          stor.nbs_imposes[i] = isNaN(Number(j3pCalculValeur(ds.imposer_nbs[i]))) ? '' : ds.imposer_nbs[i]
          ds.imposer_eg_ineg[i] = (ds.imposer_eg_ineg[i] === undefined) ? '' : ds.imposer_eg_ineg[i]
          // et maintenant pour les éventuelles inégalités imposées
          if (ds.type_quest === 2) {
            let signeEgIneg = ''
            let nbImpose = ''
            const lesSignes = ['>=', '<=', '<', '>', '=']
            lesSignes.forEach(function (monSigne) {
              if (ds.imposer_eg_ineg[i].indexOf(monSigne) > -1 && signeEgIneg === '') {
                // on a trouvé le symbole d’inégalité ou d’égalité
                signeEgIneg = monSigne
                // et je regarde s’il y a un nombre après
                const nb = ds.imposer_eg_ineg[i].replace(signeEgIneg, '')
                nbImpose = isNaN(Number(j3pCalculValeur(nb))) ? '' : nb
              }
            })
            stor.imposer_eg_ineg[i] = (signeEgIneg === '') ? '' : signeEgIneg
            stor.imposer_nbdist[i] = (nbImpose === '') ? '' : nbImpose
          }
        }
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case 'enonce':

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid // ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        // var reponse = {aRepondu:false, bonneReponse:false}
        // reponse.aRepondu = fctsValid.valideReponses()
        const reponse = fctsValid.validationGlobale()
        let manqueValabs, tropValabs, bonneFctAffine
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si la zone a bien été complétée ou non
        // il faut maintenant que je gère la réponse car pour l’instant rien n’a été fait (la zone est dans validePerso)
        if (reponse.aRepondu) {
          let maRep = fctsValid.zones.reponseSaisie[0]
          while (maRep.indexOf('\\left|') > -1) {
            maRep = maRep.replace('\\left|', '|')
          }
          while (maRep.indexOf('\\right|') > -1) {
            maRep = maRep.replace('\\right|', '|')
          }
          manqueValabs = (maRep.split('|').length < 3)
          tropValabs = (maRep.split('|').length > 3)
          if (manqueValabs || tropValabs) {
            reponse.bonneReponse = false
          } else {
          // position des valeurs absolues
            const posInf = maRep.indexOf('|')
            const posSup = maRep.lastIndexOf('|')
            const difference = maRep.substring(posInf + 1, posSup)
            // le contenu de cette zone doit être une fonction affine de la forme x-nb1 ou nb1-x
            const tabCoefAffine = j3pExtraireCoefsFctAffine(difference, 'x')
            bonneFctAffine = (tabCoefAffine[0] === '1') && (Math.abs(j3pCalculValeur(tabCoefAffine[1]) + j3pCalculValeur(stor.nb1)) < Math.pow(10, -12)) // sous la forme |x-a|
            bonneFctAffine = bonneFctAffine || ((tabCoefAffine[0] === '-1') && (Math.abs(j3pCalculValeur(tabCoefAffine[1]) - j3pCalculValeur(stor.nb1)) < Math.pow(10, -12))) // sous la forme |a-x|
            if (ds.type_quest === 1) {
            // le premier et le dernier éléments sont des valeurs absolue et on doit avoir une fonction fonction affine
              reponse.bonneReponse = (maRep.indexOf('|') === 0) && (maRep.lastIndexOf('|') === maRep.length - 1) && bonneFctAffine
            } else {
            // là fje dois nécessairement avoir un signe d’égalité ou d’inégalité
              const valabs = maRep.substring(posInf, posSup + 1)
              const bonneValabs = (valabs[0] === '|') && (valabs[valabs.length - 1] === '|') && bonneFctAffine
              let bonSigne = false
              let bonNb2 = false
              const lesSignesCorr = ['\\ge', '\\le', '>', '<', '=']
              lesSignesCorr.forEach(function (monSigne) {
                if (maRep.indexOf(monSigne) > -1) {
                // j’ai bien un signe d’inégalité ou d’égalité
                  const termes = maRep.split(monSigne)
                  if (termes[0].indexOf('|') > -1) {
                  // la valeur absolue est au début
                    bonNb2 = (Math.abs(j3pCalculValeur(termes[1]) - j3pCalculValeur(stor.nb2)) < Math.pow(10, -12))
                    bonSigne = monSigne === stor.signe
                  // console.log('test monSigne:',monSigne, 'stor.signe:',stor.signe)
                  } else {
                  // la valeur absolue est à la fin
                    bonNb2 = (Math.abs(j3pCalculValeur(termes[0]) - j3pCalculValeur(stor.nb2)) < Math.pow(10, -12))
                    if (monSigne === '=') {
                      bonSigne = monSigne
                    } else if (monSigne === '\\ge') {
                      bonSigne = (stor.signe === '\\le')
                    } else if (monSigne === '\\le') {
                      bonSigne = (stor.signe === '\\ge')
                    } else if (monSigne === '>') {
                      bonSigne = (stor.signe === '<')
                    } else if (monSigne === '<') {
                      bonSigne = (stor.signe === '>')
                    }
                  }
                  me.logIfDebug('bonSigne:', bonSigne, 'bonneValabs:', bonneValabs, 'bonNb2:', bonNb2, 'monSigne:', monSigne, 'termes:', termes)
                }
              })
              reponse.bonneReponse = (bonSigne && bonneValabs && bonNb2)
            }
          }
          fctsValid.zones.bonneReponse[0] = (reponse.bonneReponse)
          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          fctsValid.coloreUneZone(stor.zoneInput.id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (manqueValabs) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment1
              } else if (tropValabs) {
                stor.zoneCorr.innerHTML += '<br>' + textes.comment2
              } else {
                if (bonneFctAffine) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment4
                } else {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment3
                }
              }
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              // Je prévois donc une remarque particulière pour que l’élève sache que le résultat du calcul donné est bien le bon mais qu’il est prié de réduire ça
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection(me.etat, true)
      break // case 'correction':

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case 'navigation':
  }
  // console.log('fin du code : ',me.etat)
}
