import { j3pAddElt, j3pArrondi, j3pDetruit, j3pFocus, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pNombre, j3pPaletteMathquill, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexQuotient, j3pGetLatexMonome, j3pGetLatexOppose, j3pGetLatexProduit, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    février 2016
    Cette section propose de résoudre des équations du type |ax+b|=k ou |ax+b|=|cx+d| ou des inéquations du type |ax+b|<k
*/
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Résolution d’équations avec valeur absolue',
  titre_exobis: 'Résolution d’inéquations avec valeur absolue',
  // on donne les phrases de la consigne
  consigne1: 'On souhaite résoudre l’équation $£e$.',
  consigne1bis: 'On souhaite résoudre l’inéquation $£e$',
  consigne2: 'Donner la (ou les) solution(s) de cette équation.<br>&1&',
  consigne2bis: 'L’ensemble des solutions de l’inéquation est :<br>&1&',
  consigne3: '- S’il y en a plusieurs, on séparera les solutions par un point-virgule&nbsp;;<br>- s’il n’y en a pas, on écrira l’ensemble vide (palette de boutons).',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Tu as cependant donné une solution de l’équation !',
  comment2: 'Le nombre de solutions proposé n’est pas correct !',
  comment3: 'Tu as écrit une intersection, ce qui ne peut être la réponse attendue !',
  comment4: 'Il y a un problème de crochet(s) !',
  comment5: 'Attention à l’inéquation qui doit être résolue !',
  comment6: 'La réponse donnée n’est pas un ensemble de nombres (intervalle ou autre) !',
  comment10: 'Il faut donner une réponse simplifiée !',
  // et les phrases utiles pour les explications de la réponse
  corr1: '$£v$ est la distance entre $£a$ et $£b$.',
  corr1bis: '$£v$ est la distance entre $£a$ et $£b$ et $£w$ la distance entre $£c$ et $£d$.',
  corr2: 'On veut que cette distance soit égale à $£k$.',
  corr2bis: 'On veut que ces deux distances soient égales',
  corr2_1: 'On veut que cette distance soit strictement inférieure à $£k$.',
  corr2_2: 'On veut que cette distance soit strictement supérieure à $£k$.',
  corr2_3: 'On veut que cette distance soit inférieure ou égale à $£k$.',
  corr2_4: 'On veut que cette distance soit supérieure ou égale à $£k$.',
  corr3: 'On en déduit que $£e$ et donc $£f$.',
  corr3bis: 'On en déduit que $£e$ c’est-à-dire $£f$ ou que $£g$ c’est-à-dire $£h$.',
  corr3ter: 'On en déduit que $£e$.',
  corr3qua: 'On en déduit que $£e$ ou que $£f$.',
  corr4: 'Donc la solution de l’équation est $£s$.',
  corr4bis: 'Donc les solutions de l’équation sont $£s$ et $£t$.',
  corr4ter: 'Donc l’ensemble des solutions de l’inéquation est $£s$.',
  corr5: 'La valeur absolue d’un nombre réel étant supérieure ou égale à 0, l’équation n’admet pas de solution.',
  corr5bis: 'La valeur absolue d’un nombre réel étant supérieure ou égale à 0, l’inéquation n’admet pas de solution.',
  corr5ter: 'La valeur absolue d’un nombre réel étant supérieure ou égale à 0, l’inégalité est toujours vraie et l’ensemble des solutions de l’inéquation est $]-\\infty;+\\infty[=\\R$.',
  corr6: 'La valeur absolue d’un nombre réel n’est nulle que si ce nombre est nul.<br>L’équation équivaut alors à $£e$ ce qui donne $£f$.<br>Donc la solution de l’équation est $£g$.',
  corr6bis: 'La valeur absolue d’un nombre réel étant supérieure ou égale à 0, elle ne peut être strictement négative et l’inéquation n’admet donc pas de solution (l’ensemble de ses solutions est $\\varnothing$).',
  corr6ter: 'La valeur absolue d’un nombre réel étant supérieure ou égale à 0, l’inégalité n’est vérifiée que lorsque $£e$ ce qui donne $£f$ et ainsi $£g$.<br>Donc l’ensemble des solutions de cette inéquation est $£h$.',
  corr6qua: 'La valeur absolue d’un nombre réel étant supérieure ou égale à 0, pour tout nombre réel l’inégalité est vérifiée.<br>Donc l’ensemble des solutions de l’inéquation est $]-\\infty;+\\infty[=\\R$.'
}

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['type_exo', 'equation', 'liste', 'Type de résolution demandée ("equation" ou "inequation")', ['equation', 'inequation']],
    ['type_eq', [1, 1, 1], 'array', 'Pour chaque répétition, on peut imposer le type d’équation à résoudre :\n-|x+b|&lt;k pour le type 1;\n-|x+b|=|x+d| pour le type 2.\nPar défaut ce sera du type 1. Cela n’a pas d’effet pour une inéquation.'],
    ['imposer_eq_ineq', ['', '', ''], 'array', 'Pour chaque répétition, on peut imposer l’(in)équation. Par exemple "x-1=2" ou "2x-3>=1" (la valeur absolue sera ajoutée sur la fonction affine). Par défaut le coef devant x sera égal à 1, mais on peut lui imposer une autre valeur comme dans l’exemple 2.\nDans le cas où type_eq vaut 2, les 2 coefs devant x devront être égaux.\nUn nombre peut aussi être remplacé par un intervalle (d’entiers), par exemple x-2=[1;3], cet intervalle étant remplacé par un entier aléatoire y appartenant.'],
    ['coef1_devantx', [true, true, true], 'array', "Pour chaque répétition où le choix de l'(in)égalité est aléatoire, on peut faire en sorte que le coefficient devant x soit différent de 1."]
  ]
}

/**
 * Simplifie l’écriture d’une intersection et / ou reunion d’intervalles (moyennant 1000 lignes de code…)
 * @param chMathquill
 */
function simplifieUnionIntersection (chMathquill) {
  // On commence par une série de fonctions internes
  function estDansUnDesIntervalles (tabAk, tabBk, tabCrochetG, tabCrochetD, x) {
    const stock = {}
    const n = tabAk.length
    let indice = -1
    for (let k = 0; k < n; k++) {
      const gauche = (tabCrochetG[k] === '<')
        ? tabAk[k] < x
        : (tabCrochetG[k] === '>')
            ? tabAk[k] > x
            : (tabCrochetG[k] === '<=')
                ? tabAk[k] <= x
                : (tabCrochetG[k] === '>=')
                    ? tabAk[k] >= x
                    : false
      const droit = (tabCrochetD[k] === '<')
        ? x < tabBk[k]
        : (tabCrochetD[k] === '>')
            ? x > tabBk[k]
            : (tabCrochetD[k] === '<=')
                ? x <= tabBk[k]
                : (tabCrochetD[k] === '>=')
                    ? x >= tabBk[k]
                    : false
      if (gauche && droit) {
        indice = k
        k = n
      }
    }
    stock.indice = indice
    // Si x n’est pas dedans,on cherche à le localiser:
    // Les possibilités sont les suivantes :
    // Soit il est sur l’un des ak ou l’un des bk,avec un crochet ouvert
    // soit strictement avant a0
    // soit après b_n
    // soit entre b(k) et a(k+1)
    // On teste la localisation dans tous les cas
    let cas = false
    stock.localisation = {}
    stock.localisation.code = ''
    // 2 possibilités : soit suraksurbk, ce qui signifie qu’on a aura un intervalle fermé du type singleton dans l’union comme [1;1]
    //                 soit surbksurak, ce qui signifie qu’on a un cas du genre ]...;1[U]1;...
    for (let k = 0; k < n; k++) {
      if (x === tabAk[k]) {
        stock.localisation.code = stock.localisation.code + 'sur ak'
        stock.localisation.index = k
        cas = true
      }
      if (x === tabBk[k]) {
        stock.localisation.code = stock.localisation.code + 'sur bk'// Dans le cas où on tombe et sur ak et sur b(k+1)
        stock.localisation.index = k
        cas = true
      }
    }

    // avant a0
    if (x < tabAk[0]) {
      stock.localisation.code = 'avant'
      cas = true
    }

    // Ou quelque part entre un  b(k) et a(k+1)
    if (!cas) {
      let k = 0
      while (tabBk[k] < x && k < n) k++
      const l = k - 1
      stock.localisation.code = 'apres bk'
      stock.localisation.index = l
    }

    return stock
  }

  function intersecteDeuxIntervalles (objIntervalle1, objIntervalle2) {
    let resultatIntersection = {}
    // objIntervalle1 et objIntervalle1 sont des objets ayant comme ppté:
    //      .vide=true or false
    // .min .max .crochetG et .crochetD

    if (objIntervalle1.vide || objIntervalle2.vide) {
      resultatIntersection.vide = true
    } else {
      // les 8 éléments ci-dessous sont des tableaux
      const a1 = objIntervalle1.min
      const b1 = objIntervalle1.max
      const cg1 = objIntervalle1.crochetG
      const cd1 = objIntervalle1.crochetD

      const a2 = objIntervalle2.min
      const b2 = objIntervalle2.max
      const cg2 = objIntervalle2.crochetG
      const cd2 = objIntervalle2.crochetD

      // Et voici des réels :

      const valeurA1 = j3pNombre(a1[0])
      const valeurB1 = j3pNombre(b1[0])
      const valeurA2 = j3pNombre(a2[0])
      const valeurB2 = j3pNombre(b2[0])

      const a2Infos = estDansUnDesIntervalles(a1, b1, cg1, cd1, valeurA2)
      const b2Infos = estDansUnDesIntervalles(a1, b1, cg1, cd1, valeurB2)

      if (a2Infos.indice !== -1) {
        // a2 est  dans (a1,b1)
        if (b2Infos.indice !== -1) {
          // b2 est aussi dedans
          resultatIntersection = objIntervalle2
        } else {
          // Dans le cas où a2 est sur b1, l’intersection est vide
          if (a2Infos.localisation.code === 'sur bk' || a2Infos.localisation.code === 'sur aksur bk') {
            // comme b2 nest pas dedans, le crochet cd1 est ouvert, l’intersection donne un singleton si cg2 fermé, vide sinon
            if (cg2[0] === '<') {
              resultatIntersection.vide = true
            } else {
              resultatIntersection.vide = false
              resultatIntersection.min = [valeurB1]
              resultatIntersection.crochetG = [cd1[0]]
              resultatIntersection.max = [valeurB1]
              resultatIntersection.crochetD = [cd1[0]]
            }
          } else {
            // soit a2,b1
            resultatIntersection.vide = false
            resultatIntersection.min = [valeurA2]
            resultatIntersection.crochetG = [cg2[0]]
            resultatIntersection.max = [valeurB1]
            resultatIntersection.crochetD = [cd1[0]]
          }
        }
      } else {
        // a2 n’est pas dedans
        switch (a2Infos.localisation.code) {
          case 'apres bk':

            // l’intersection est vide
            resultatIntersection.vide = true

            break

          case 'sur bk':
            // le cg1 est donc ouvert l’intersection est vide aussi

            resultatIntersection.vide = true

            break

          case 'sur ak':
            // le cg1 est alors ouvert

            if (valeurB2 === valeurA2) {
              resultatIntersection.vide = true
            } else {
              resultatIntersection.vide = false
              resultatIntersection.min = [valeurA2]
              resultatIntersection.crochetG = ['<']
              // l’intersection donne a2 b2 si b2 est dedans, ou a2,b1
              if (b2Infos.indice !== -1) {
                resultatIntersection.max = [valeurB2]
                resultatIntersection.crochetD = [objIntervalle2.crochetD[0]]
              } else {
                resultatIntersection.max = [valeurB1]
                resultatIntersection.crochetD = [objIntervalle1.crochetD[0]]
              }
            }

            break

          case 'avant':

            if (b2Infos.indice !== -1) {
              if (valeurA1 === valeurB1) {
                if (cd2[0] === '<') {
                  resultatIntersection.vide = true
                } else {
                  resultatIntersection.vide = false
                  resultatIntersection.min = [valeurA1]
                  resultatIntersection.crochetG = ['<=']
                  resultatIntersection.max = [valeurB1]
                  resultatIntersection.crochetD = ['<=']
                }
              } else {
                if (b2Infos.localisation.code === 'sur ak') {
                  if (cd2[0] === '<') {
                    resultatIntersection.vide = true
                  } else {
                    resultatIntersection.vide = false
                    resultatIntersection.min = [valeurA1]
                    resultatIntersection.crochetG = ['<=']
                    resultatIntersection.max = [valeurA1]
                    resultatIntersection.crochetD = ['<=']
                  }
                } else {
                  resultatIntersection.vide = false
                  resultatIntersection.min = [valeurA1]
                  resultatIntersection.crochetG = [cg1[0]]
                  resultatIntersection.max = [valeurB2]
                  resultatIntersection.crochetD = [cd2[0]]
                }
              }
            } else {
              switch (b2Infos.localisation.code) {
                case 'avant':
                  // l’intersection est vide
                  resultatIntersection.vide = true
                  break

                case 'sur ak':
                  // l’intersection donne soit le vide soit un singleton

                  if (cg1[0] === '<' || cd2[0] === '<') {
                    resultatIntersection.vide = true
                  } else {
                    // On obtient un singleton
                    resultatIntersection.vide = false
                    resultatIntersection.min = [valeurA1]
                    resultatIntersection.crochetG = ['<=']
                    resultatIntersection.max = [valeurA1]
                    resultatIntersection.crochetD = ['<=']
                  }
                  break

                case 'sur bk':
                  resultatIntersection.vide = false
                  resultatIntersection.vide = false
                  resultatIntersection.min = [valeurA1]
                  resultatIntersection.crochetG = [cg1[0]]
                  resultatIntersection.max = [valeurB1]
                  if (cd1[0] === '<' || cg2[0] === '<') {
                    // On obtient (a1,b1 ouvert ou fermé
                    resultatIntersection.crochetD = ['<']
                  } else {
                    resultatIntersection.crochetD = ['<=']
                  }

                  break

                case 'apres bk':
                  // On obtient (a1,b1)
                  resultatIntersection = objIntervalle1
                  break
              }
            }
            break
        }
      }
    }
    return resultatIntersection
  }

  function construisNouvelleUnionIntervalle (objUnionIk) {
    // L’objet obj_union_intervalle est  un objet représentant une union d’intervalles disjoints ak;bk avec:
    // a(k)<=b(k)<a(k+1)<=b(k+1)
    // Cet objet les propriétés :
    // .min qui renvoie le tableau des ak
    // .max qui renvoie le tableau des bk
    // .crochetG qui renvoie le tableau des crochet associés aux ak
    // .crochetD qui renvoie le tableau des crochet associés aux bk
    // Cette fonction va créer un nouvel objet ayant les mêmes pptés, et des méthodes :
    // .appartient(x) qui renvoie l’index de l’intervalle (ak,bk) contentant x ou -1 si non appartenance
    // .union(obj_intervalle),avec obj_intervalle representant un intervalle de taille 1,qui renvoie un nouvel objet de type union d’intervalle
    // .intersection(obj_union_intervalle) qui renvoie un nouvel objet de typeunion d’intervalle
    return {
      // méthodes :
      union: function (objInPlus1) {
        function oppose (x) {
          return -x
        }

        // Cette fonction va retourner un objet de type union d’intervalle réunissant Obj_union_Ik et I_n_plus_1
        // objUnionIk a une ppté.vide=true ou false
        // Si true, alors l’action de l’union sur I_n_plus_1renvoie :
        // soit le vide si I_n_plus_1 l’est aussi, soit
        // On commence par gérer le cas du vide:
        let objNouvelleUnionIntervalle = {}
        if (objUnionIk.vide) {
          // On peut avoir le cas I_n_plus_1 vide lui aussi
          if (objInPlus1.vide) {
            objNouvelleUnionIntervalle.vide = true
          } else {
            // On renvoie I_n_plus_1

            objNouvelleUnionIntervalle.vide = false
            objNouvelleUnionIntervalle.min = objInPlus1.min
            objNouvelleUnionIntervalle.crochetG = objInPlus1.crochetG
            objNouvelleUnionIntervalle.max = objInPlus1.max
            objNouvelleUnionIntervalle.crochetD = objInPlus1.crochetD
          }
        } else {
          if (objInPlus1.vide) {
            // L’union ne change pas
            objNouvelleUnionIntervalle.vide = false
            objNouvelleUnionIntervalle.min = objUnionIk.min
            objNouvelleUnionIntervalle.crochetG = objUnionIk.crochetG
            objNouvelleUnionIntervalle.max = objUnionIk.max
            objNouvelleUnionIntervalle.crochetD = objUnionIk.crochetD
          } else {
            objNouvelleUnionIntervalle.vide = false

            const a = j3pNombre(objInPlus1.min[0])
            const b = j3pNombre(objInPlus1.max[0])
            const cg = objInPlus1.crochetG[0]
            const cd = objInPlus1.crochetD[0]

            const tabAk = objUnionIk.min
            const tabBk = objUnionIk.max
            const tabCrochetG = objUnionIk.crochetG
            const tabCrochetD = objUnionIk.crochetD
            const aInfos = estDansUnDesIntervalles(tabAk, tabBk, tabCrochetG, tabCrochetD, a)
            const bInfos = estDansUnDesIntervalles(tabAk, tabBk, tabCrochetG, tabCrochetD, b)

            const n = tabAk.length
            let k0 = aInfos.indice
            const k1 = bInfos.indice

            if (k0 !== -1) {
              if (k1 !== -1) {
                // a et b sont dans l’union

                objUnionIk.min.splice(k0 + 1, k1 - k0)
                objNouvelleUnionIntervalle.min = objUnionIk.min

                objUnionIk.crochetG.splice(k0 + 1, k1 - k0)
                objNouvelleUnionIntervalle.crochetG = objUnionIk.crochetG

                objUnionIk.max.splice(k0, k1 - k0)
                objNouvelleUnionIntervalle.max = objUnionIk.max

                objUnionIk.crochetD.splice(k0, k1 - k0)
                objNouvelleUnionIntervalle.crochetD = objUnionIk.crochetD
              } else {
                // b n’est pas dans l’un intervalles, mais a oui
                // b ne peut etre que sur l’un des bornes ouvertes, ou apres bk ou apres, ou à la fois sur bk et ak
                let k11
                switch (bInfos.localisation.code) {
                  case 'sur ak':
                    // b est sur une borne ouverte ak
                    k11 = bInfos.localisation.index
                    if (cd === '<=') {
                      objUnionIk.min.splice(k0 + 1, k11 - k0)
                      objNouvelleUnionIntervalle.min = objUnionIk.min
                      objUnionIk.crochetG.splice(k0 + 1, k11 - k0)
                      objNouvelleUnionIntervalle.crochetG = objUnionIk.crochetG
                      objUnionIk.max.splice(k0, k11 - k0)
                      objNouvelleUnionIntervalle.max = objUnionIk.max
                      objUnionIk.crochetD.splice(k0, k11 - k0)
                      objNouvelleUnionIntervalle.crochetD = objUnionIk.crochetD
                    } else {
                      objUnionIk.min.splice(k0 + 1, k11 - k0 - 1)
                      objNouvelleUnionIntervalle.min = objUnionIk.min
                      objUnionIk.crochetG.splice(k0 + 1, k11 - k0 - 1)
                      objNouvelleUnionIntervalle.crochetG = objUnionIk.crochetG
                      objUnionIk.max.splice(k0, k11 - k0 - 1)
                      objUnionIk.max[k0] = b
                      objNouvelleUnionIntervalle.max = objUnionIk.max
                      objUnionIk.crochetD.splice(k0, k11 - k0 - 1)
                      objNouvelleUnionIntervalle.crochetD = objUnionIk.crochetD
                    }
                    break

                  case 'sur bk':
                    // sur bk seulement.
                    k11 = bInfos.localisation.index
                    objUnionIk.min.splice(k0 + 1, k11 - k0)
                    objNouvelleUnionIntervalle.min = objUnionIk.min
                    objUnionIk.crochetG.splice(k0 + 1, k11 - k0)
                    objNouvelleUnionIntervalle.crochetG = objUnionIk.crochetG
                    objUnionIk.max.splice(k0, k11 - k0)
                    objNouvelleUnionIntervalle.max = objUnionIk.max
                    if (cd === '<=') {
                      objUnionIk.crochetD[k11] = '<='
                    }
                    objUnionIk.crochetD.splice(k0, k11 - k0)
                    objNouvelleUnionIntervalle.crochetD = objUnionIk.crochetD
                    break

                  case 'sur bksur ak':
                    // b est sur a(k+1) et bk bornes ouvertes. Si b borne ouverte,alors rien ne change
                    k11 = bInfos.localisation.index
                    if (cd === '<=') {
                      objUnionIk.min.splice(k0 + 1, k11 - k0)
                      objNouvelleUnionIntervalle.min = objUnionIk.min
                      objUnionIk.crochetG.splice(k0 + 1, k11 - k0)
                      objNouvelleUnionIntervalle.crochetG = objUnionIk.crochetG
                      objUnionIk.max.splice(k0, k11 - k0)
                      objNouvelleUnionIntervalle.max = objUnionIk.max
                      objUnionIk.crochetD.splice(k0, k11 - k0)
                      objNouvelleUnionIntervalle.crochetD = objUnionIk.crochetD
                    } else {
                      objUnionIk.min.splice(k0 + 1, k11 - k0 - 1)
                      objNouvelleUnionIntervalle.min = objUnionIk.min
                      objUnionIk.crochetG.splice(k0 + 1, k11 - k0 - 1)
                      objNouvelleUnionIntervalle.crochetG = objUnionIk.crochetG
                      objUnionIk.max.splice(k0, k11 - k0 - 1)
                      objNouvelleUnionIntervalle.max = objUnionIk.max
                      objUnionIk.crochetD.splice(k0, k11 - k0 - 1)
                      objNouvelleUnionIntervalle.crochetD = objUnionIk.crochetD
                    }
                    break

                  case 'apres bk':
                    // il est donc >bk, inferieur à a(k+1) ou après a(k+1)
                    // Il faut donc étudier le cas où b est après le dernier bk
                    k11 = bInfos.localisation.index
                    objUnionIk.min.splice(k0 + 1, k11 - k0)
                    objNouvelleUnionIntervalle.min = objUnionIk.min
                    objUnionIk.crochetG.splice(k0 + 1, k11 - k0)
                    objNouvelleUnionIntervalle.crochetG = objUnionIk.crochetG
                    objUnionIk.max.splice(k0, k11 - k0 + 1, b)
                    objNouvelleUnionIntervalle.max = objUnionIk.max
                    objUnionIk.crochetD.splice(k0, k11 - k0 + 1, cd)
                    objNouvelleUnionIntervalle.crochetD = objUnionIk.crochetD
                    break
                }
              }
            } else {
              // a n’est pas dans l’union
              if (k1 === -1) {
                let minAremettre, crochetGaRemettre, maxARemettre, crochetDARemettre, aCompleter
                // b n’est pas non plus dans l’union
                switch (aInfos.localisation.code) {
                  case 'avant':
                    // si b est avant, on insere l’intervalle (a;b),
                    if (bInfos.localisation.code === 'avant') {
                      objUnionIk.min.splice(0, 0, a)
                      objNouvelleUnionIntervalle.min = objUnionIk.min
                      objUnionIk.crochetG.splice(0, 0, cg)
                      objNouvelleUnionIntervalle.crochetG = objUnionIk.crochetG
                      objUnionIk.max.splice(0, 0, b)
                      objNouvelleUnionIntervalle.max = objUnionIk.max
                      objUnionIk.crochetD.splice(0, 0, cd)
                      objNouvelleUnionIntervalle.crochetD = objUnionIk.crochetD
                    } else {
                      // sinon, on inserera a avant a0 avec borne fermée et un b quelconque entre a et a0, puis on retourne
                      const c = (a + j3pNombre(objUnionIk.min[0])) / 2
                      objUnionIk.min.splice(0, 0, a)
                      objUnionIk.crochetG.splice(0, 0, '<=')
                      objUnionIk.max.splice(0, 0, c)
                      objUnionIk.crochetD.splice(0, 0, '<')
                      const nouveau = {
                        min: [a],
                        max: [b],
                        crochetG: ['<='],
                        crochetD: [cd]
                      }
                      objNouvelleUnionIntervalle = construisNouvelleUnionIntervalle(objUnionIk).union(nouveau)
                      objNouvelleUnionIntervalle.crochetG[0] = cg
                    }
                    break

                  case 'sur ak':
                    // Le ak est donc ouvert
                    // On peut donc le récupérer si cg fermé.
                    // Dans ce cas,on revient au cas où a apparient à union des aK
                    k0 = aInfos.localisation.index
                    objUnionIk.crochetG[k0] = '<='
                    objNouvelleUnionIntervalle = construisNouvelleUnionIntervalle(objUnionIk).union(objInPlus1)
                    if (cg === '<') objNouvelleUnionIntervalle.crochetG[k0] = '<'
                    break

                  case 'sur bk':
                    // Le bk est donc ouvert
                    // On peut donc le récupérer si cg fermé.
                    // Dans ce cas,on revient au cas où a apparient à union des aK
                    k0 = aInfos.localisation.index
                    if (cg === '<=') {
                      objUnionIk.crochetD[k0] = '<='
                      objNouvelleUnionIntervalle = construisNouvelleUnionIntervalle(objUnionIk).union(objInPlus1)
                    } else {
                      minAremettre = objUnionIk.min.splice(0, k0 + 1)
                      crochetGaRemettre = objUnionIk.crochetG.splice(0, k0 + 1)
                      objUnionIk.min.splice(0, 0, a)
                      objUnionIk.crochetG.splice(0, 0, '<=')
                      maxARemettre = objUnionIk.max.splice(0, k0 + 1)
                      crochetDARemettre = objUnionIk.crochetD.splice(0, k0 + 1)
                      const c = (a + b) / 2
                      objUnionIk.max.splice(0, 0, c)
                      objUnionIk.crochetD.splice(0, 0, '<=')
                      // On fait maintenant agir l’union [ak0;bk0] U... sur In+1
                      aCompleter = construisNouvelleUnionIntervalle(objUnionIk).union(objInPlus1)
                      aCompleter.crochetG[0] = '<'
                      objNouvelleUnionIntervalle.min = minAremettre.concat(aCompleter.min)
                      objNouvelleUnionIntervalle.crochetG = crochetGaRemettre.concat(aCompleter.crochetG)
                      objNouvelleUnionIntervalle.max = maxARemettre.concat(aCompleter.max)
                      objNouvelleUnionIntervalle.crochetD = crochetDARemettre.concat(aCompleter.crochetD)
                    }
                    break

                  case 'sur bksur ak':
                    // ak et bk sont donc ouverts
                    // On peut donc le récupérer si cg fermé.
                    // Dans ce cas,on revient au cas où a apparient à union des aK
                    k0 = aInfos.localisation.index
                    if (cg === '<=') {
                      objUnionIk.crochetD[k0 - 1] = '<='
                      objNouvelleUnionIntervalle = construisNouvelleUnionIntervalle(objUnionIk).union(objInPlus1)
                    } else {
                      minAremettre = objUnionIk.min.splice(0, k0)
                      crochetGaRemettre = objUnionIk.crochetG.splice(0, k0)
                      // objUnionIk.min.splice(0,0,a);
                      objUnionIk.crochetG[0] = '<='
                      maxARemettre = objUnionIk.max.splice(0, k0)
                      crochetDARemettre = objUnionIk.crochetD.splice(0, k0)
                      // On fait maintenant agir l’union [ak0;bk0] U... sur In+1
                      aCompleter = construisNouvelleUnionIntervalle(objUnionIk).union(objInPlus1)

                      aCompleter.crochetD[0] = '<'
                      aCompleter.crochetG[0] = '<'

                      objNouvelleUnionIntervalle.min = minAremettre.concat(aCompleter.min)
                      objNouvelleUnionIntervalle.crochetG = crochetGaRemettre.concat(aCompleter.crochetG)
                      objNouvelleUnionIntervalle.max = maxARemettre.concat(aCompleter.max)
                      objNouvelleUnionIntervalle.crochetD = crochetDARemettre.concat(aCompleter.crochetD)
                    }

                    break

                  case 'apres bk':

                    // Si a est après le dernier n, il faut rajouter l’intervalle (a,b) :
                    k0 = aInfos.localisation.index
                    if (k0 === n - 1) {
                      const lesMin = objUnionIk.min
                      const lesMax = objUnionIk.max
                      const lesCg = objUnionIk.crochetG
                      const lesCd = objUnionIk.crochetD
                      lesMin.push(a)
                      lesMax.push(b)
                      lesCg.push(cg)
                      lesCd.push(cd)

                      objNouvelleUnionIntervalle.min = lesMin

                      objNouvelleUnionIntervalle.crochetG = lesCg
                      objNouvelleUnionIntervalle.max = lesMax

                      objNouvelleUnionIntervalle.crochetD = lesCd
                    } else {
                      minAremettre = objUnionIk.min.splice(0, k0 + 1)

                      crochetGaRemettre = objUnionIk.crochetG.splice(0, k0 + 1)
                      objUnionIk.min[0] = a
                      objUnionIk.crochetG[0] = cg

                      maxARemettre = objUnionIk.max.splice(0, k0 + 1)

                      crochetDARemettre = objUnionIk.crochetD.splice(0, k0 + 1)

                      // On fait maintenant agir l’union [a;b(k0+1)] U... sur In+1
                      aCompleter = construisNouvelleUnionIntervalle(objUnionIk).union(objInPlus1)

                      objNouvelleUnionIntervalle.min = minAremettre.concat(aCompleter.min)
                      objNouvelleUnionIntervalle.crochetG = crochetGaRemettre.concat(aCompleter.crochetG)
                      objNouvelleUnionIntervalle.max = maxARemettre.concat(aCompleter.max)
                      objNouvelleUnionIntervalle.crochetD = crochetDARemettre.concat(aCompleter.crochetD)
                    }

                    break
                }
              } else {
                // b est dans l’union:
                // On va alors opposer tous les min qui deviendront des max, tous les max qui deviendront des min
                // inverser tous les tableaux
                // et inverser l’intervalle (a,b)
                // puis faire agir cette untion inversée sur (b,a)
                let nouveauMin = objUnionIk.max.reverse()
                nouveauMin = nouveauMin.map(oppose) // fonctionTab(oppose, nouveauMin)

                const nouveauCrochetG = objUnionIk.crochetD.reverse()

                let nouveauMax = objUnionIk.min.reverse()
                nouveauMax = nouveauMax.map(oppose) // fonctionTab(oppose, nouveauMax)

                const nouveauCrochetD = objUnionIk.crochetG.reverse()
                const objUnionIkOppose = {
                  min: nouveauMin,
                  max: nouveauMax,
                  crochetG: nouveauCrochetG,
                  crochetD: nouveauCrochetD
                }
                const InPlus1Oppose = {
                  min: [-b],
                  max: [-a],
                  crochetG: [cd],
                  crochetD: [cg]
                }

                const aOpposer = construisNouvelleUnionIntervalle(objUnionIkOppose).union(InPlus1Oppose)
                // Et oppose tout pour finir
                objNouvelleUnionIntervalle.min = aOpposer.max.reverse().map(oppose) // fonctionTab(oppose, aOpposer.max.reverse())
                objNouvelleUnionIntervalle.crochetG = aOpposer.crochetD.reverse()
                objNouvelleUnionIntervalle.max = aOpposer.min.reverse().map(oppose) // fonctionTab(oppose, aOpposer.min.reverse())
                objNouvelleUnionIntervalle.crochetD = aOpposer.crochetG.reverse()
              }
            }
          }
        }

        return objNouvelleUnionIntervalle
      },
      intersection: function (objInPlus1) {
        // Cette fonction va retourner un objet de type union d’intervalle via l’interesction de Obj_union_Ik et de I_n_plus_1

        const objNouvelleUnionIntervalle = {}

        if (objUnionIk.vide) {
          // l’interesction est vide
          objNouvelleUnionIntervalle.vide = true
        } else {
          const tabAk = objUnionIk.min
          const tabBk = objUnionIk.max
          const tabCrochetG = objUnionIk.crochetG
          const tabCrochetD = objUnionIk.crochetD
          const n = tabAk.length
          const tabInf = []
          const tabSup = []
          const tabCg = []
          const tabCd = []
          let interVide = true
          for (let k = 0; k < n; k++) {
            // on utilise le fait que (U Ik) inter I_(n+1)=U (Ik inter I_(n+1))
            const Ik = {}
            Ik.vide = false
            Ik.min = [tabAk[k]]
            Ik.max = [tabBk[k]]
            Ik.crochetG = [tabCrochetG[k]]
            Ik.crochetD = [tabCrochetD[k]]
            const resultInterIkEtInPlus1 = intersecteDeuxIntervalles(Ik, objInPlus1)

            if (!resultInterIkEtInPlus1.vide) {
              interVide = false
              tabInf.push(resultInterIkEtInPlus1.min[0])
              tabSup.push(resultInterIkEtInPlus1.max[0])
              tabCg.push(resultInterIkEtInPlus1.crochetG[0])
              tabCd.push(resultInterIkEtInPlus1.crochetD[0])
            }
          }
          if (!interVide) {
            objNouvelleUnionIntervalle.vide = false
            objNouvelleUnionIntervalle.min = tabInf
            objNouvelleUnionIntervalle.crochetG = tabCg
            objNouvelleUnionIntervalle.max = tabSup
            objNouvelleUnionIntervalle.crochetD = tabCd
          } else {
            objNouvelleUnionIntervalle.vide = true
          }
        }
        return objNouvelleUnionIntervalle
      }
    }
  }

  function analyseIntervalle (chIntervalle) {
    // Cette fonction analyse les intervalles normalement récupérés entre chaque untion et intersection.
    // Elle renvoie l’objet stock ayant les pptés suivantes :
    // stock.valide=true si l’écriture est valide
    // stock.valide=false si l’écriture n’est pas valide
    // Dans ce cas, on a stock.erreur qui est une string qui renvoie le type d’erreur
    // stock.erreur="illisible" si rien n’est interprétable
    // stock.erreur="inversion_bornes" si l’intervalle est écrit de façon inversée
    // stock.erreur=infini_inclus si l’une des bornes infinies est incluse
    // stock.ecriture renvoie l’intervalle pour que la fonction résultat puisse fonctionner

    const stock = {}
    const n = chIntervalle.length
    stock.valide = true
    stock.simplifie = true

    if (chIntervalle === '\\varnothing') {
      stock.ecriture = '\\varnothing'
    } else {
      // On commence par regarder si le premier caractère est bien un crochet :
      if (chIntervalle.charAt(0) !== ']' && chIntervalle.charAt(0) !== '[') {
        stock.valide = false
        stock.erreur = 'manque_crochet'
      } else {
        // le premier caractère est un crochet. On analyse le dernier caractère
        const crochetG = chIntervalle.charAt(0)
        if (chIntervalle.charAt(n - 1) !== '[' && chIntervalle.charAt(n - 1) !== ']') {
          stock.valide = false
          stock.erreur = 'manque_crochet'
        } else {
          // Les premiers et derniers caractères sont donc des crochets. On regarde la présence d’un point virgule, séparateur des bornes.
          const crochetD = chIntervalle.charAt(n - 1)
          const p = chIntervalle.indexOf(';')
          if (p === -1) {
            stock.valide = false
            stock.erreur = 'illisible'
          } else {
            // Il y a un point virgule.Il faut savoir si ce qui est entre le premier caractère et le point virgule est un nombre
            const borne1 = chIntervalle.substring(1, p)
            // On convertit la borne1 en nombre et on rajoute une chaine pour voir s’il s’agit d’un nombre
            const chBorne1 = j3pNombre(borne1) + 'a'
            if (chBorne1.charAt(0) === 'N') {
              // borne1 n’est pas un nombre
              stock.valide = false
              stock.erreur = 'illisible'
            } else {
              // borne1 est donc un nombre. On teste donc la borne2.
              const borne2 = chIntervalle.substring(p + 1, n - 1)
              // On convertit la borne2 en nombre et on rajoute une chaine pour voir s’il s’agit d’un nombre
              // l’infini a déjà été transformé en 10^12

              const chBorne2 = j3pNombre(borne2) + 'a'
              if (chBorne2.charAt(0) === 'N') {
                // borne2 n’est pas un nombre
                stock.valide = false
                stock.erreur = 'illisible'
              } else {
                // borne1 et borne2 sont des nombres. On regarde si borne1 et borne2 sont dans le bon sens

                if (borne2 - borne1 < 0) {
                  stock.valide = false
                  stock.erreur = 'inversion_bornes'
                } else {
                  // Les bornes sont dans le bon sens
                  // On regarde si l’infini est l’une d’elles, et s’il est bien exclus dans ce cas.
                  // On regarde si l’intervalle est du type +inf;+inf ou -inf;-inf auquel cas, on renvoie l’érreur :
                  // stock.ecriture=inf_inf
                  if ((borne1 === 1000000000000 && borne2 === 1000000000000) || (borne1 === -1000000000000 && borne2 === -1000000000000)) {
                    stock.valide = false
                    stock.erreur = 'inf_inf'
                  } else {
                    // On doit vérifier que si l’une des bornes est infinie, elle  n’est pas fermé
                    if ((borne1 === -1000000000000 && crochetG === '[') || (borne2 === 1000000000000 && crochetD === ']')) {
                      stock.valide = false
                      stock.erreur = 'infini_inclus'
                    } else {
                      // L’intervalle est valide.
                      // On doit remplacer l’intervalle ouvert éventuel ]a;a[ ou ]a;a] ou [a;a[ par le vide.
                      stock.valide = true
                      if (borne1 === borne2 && (crochetG === ']' || crochetD === '[')) {
                        stock.simplifie = false
                        stock.ecriture = '\\varnothing'
                      } else {
                        stock.ecriture = chIntervalle
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    return stock
  }

  // On remplace les virgules par des points, et les infinis par 10^12
  chMathquill = chMathquill
    .replace(/,/g, '.')
    .replace(/\+\\infty/, '1000000000000')
    .replace(/-\\infty/, '-1000000000000')

  const stock = {}
  stock.valide = true
  let lesIntervalles = chMathquill.split('\\cup')
  // var lesIntervalles=chMathquill.split("cup");
  let lesIntervallesSansRien = lesIntervalles.toString()
  lesIntervalles = lesIntervallesSansRien.split('\\cap')
  // lesIntervalles=lesIntervallesSansRien.split("cap");
  lesIntervallesSansRien = lesIntervalles.toString()
  lesIntervalles = lesIntervallesSansRien.split(',')

  // lesIntervalles est alors un tableau d’intervalles ou de vide
  const n = lesIntervalles.length
  // On va étudier la validité du tableau récupéré.
  // On peut signaler : intervalle renversé, infini inclus.
  // Il faudra remplacer le singleton ouvert par le vide.
  //
  // On va construire un tableau d’objet pour agir par récursivité
  const tabObjIntervalles = []
  // On récupère les infos relatives aux intervalles
  let simplifie
  for (let k = 0; k < n; k++) {
    let ch = lesIntervalles[k]
    // On lance l’analyse d’intervalle.
    // Dès la première erreur, on sort de la boucle
    const testCh = analyseIntervalle(ch)
    simplifie = true
    if (!testCh.valide) {
      stock.valide = false
      stock.erreur = testCh.erreur
      k = n
    } else {
      ch = testCh.ecriture
      simplifie = simplifie && testCh.simplifie
      const n1 = ch.length
      const k1 = ch.indexOf(';')
      const objIk = {}
      // soit on tombe sur le vide, soit sur un intervalle

      if (ch === '\\varnothing') {
        objIk.vide = true
      } else {
        // L’intervalle est non vide
        objIk.vide = false

        if (ch.charAt(0) === '[') {
          objIk.crochetG = ['<=']
        } else {
          objIk.crochetG = ['<']
        }
        const a = j3pNombre(ch.substring(1, k1))
        const b = j3pNombre(ch.substring(k1 + 1, n1 - 1))

        objIk.min = [a]
        objIk.max = [b]

        if (ch.charAt(n1 - 1) === '[') {
          objIk.crochetD = ['<']
        } else {
          objIk.crochetD = ['<=']
        }
      }
      tabObjIntervalles.push(objIk)
    }
  }
  // Recupération des unions et des intersections entre chaque intervalle si stock.valide toujours égal à true
  if (stock.valide) {
    const tabDesActions = []

    for (let k = 0; k < chMathquill.length - 4; k++) {
      if (chMathquill.substring(k, k + 3) === 'cap') tabDesActions.push('intersection')
      if (chMathquill.substring(k, k + 3) === 'cup') tabDesActions.push('union')
    }

    // On fait maintenant agir récursivement les fonctions préalablement construites :

    let result = tabObjIntervalles[0]
    for (let k = 0; k < tabDesActions.length; k++) {
      const action = tabDesActions[k]
      result = construisNouvelleUnionIntervalle(result)[action](tabObjIntervalles[k + 1])
    }

    // Reconstruction de la chaine mathquill correspondante
    // On va traiter le cas des singletons, et de R

    if (result.vide) {
      stock.resultat = '\\varnothing'
      stock.affichage = '\\varnothing'
      // On regarde si l’intervalle a bien été simplifié
      stock.simplifie = (n === 1 && simplifie)
    } else {
      const lesMin = result.min
      const lesG = result.crochetG
      const lesD = result.crochetD
      const lesMax = result.max

      stock.min = lesMin
      stock.max = lesMax
      stock.crochetG = lesG
      stock.crochetD = lesD

      stock.resultat = ''
      stock.affichage = ''
      const m = lesMin.length
      // On regarde si tout a été simplfié
      if (n !== lesMin.length) {
        simplifie = false
      }
      stock.simplifie = simplifie
      if (m === 1 & lesMin[0] === -1000000000000 && lesMax[0] === 1000000000000) {
        // On renvoie R
        stock.resultat = ']-\\infty;+\\infty['
        stock.affichage = '\\mathbf{R}'
      } else {
        for (let k = 0; k < m; k++) {
          if (lesG[k] === '<') {
            stock.resultat = stock.resultat + ']'
          } else {
            stock.resultat = stock.resultat + '['
          }
          if (lesMin[k] === -1000000000000) {
            stock.resultat = stock.resultat + '-\\infty;'
          } else {
            stock.resultat = stock.resultat + lesMin[k] + ';'
          }
          if (lesMax[k] === 1000000000000) {
            stock.resultat = stock.resultat + '+\\infty'
          } else {
            stock.resultat = stock.resultat + lesMax[k]
          }

          if (lesD[k] === '<') {
            stock.resultat = stock.resultat + '['
          } else {
            stock.resultat = stock.resultat + ']'
          }
          if (lesMin[k] === lesMax[k]) {
            // On a affaire à un singleton, on change l’affichage, mais pas l’écriture
            stock.affichage = stock.affichage + '${$' + lesMin[k] + '$}$'
          } else {
            stock.affichage = stock.resultat
          }

          if (k !== m - 1) {
            stock.resultat = stock.resultat + '\\cup'
            stock.affichage = stock.affichage + '\\cup'
          }
        }
      }
    }
  }

  return stock
} // simplifieUnionIntersection

/**
 * section resolution_valabs
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function depart (st) {
    // st contient des propriétés servant à reconstruire la figure
    // st.num pour savoir quelle figure charger
    stor.mtgAppLecteur.removeAllDoc()
    let txtFigure
    // txt html de la figure
    if (st.num === 1) {
      txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH2+v4BAP8BAAAAAAAAAAACKQAAAFAAAAAAAAAAAAAAAAAAAAAd#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEACkNQb2ludEJhc2UA#####wAAAAAADQAAAQUAAUBxcAAAAAAAQDwAAAAAAAD#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wAAAAAADQAAAQABAAAAAQE#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wAAAAAADQAAAQUAAcBqgAAAAAAAAAAAAv####8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAAAf####8AAAABAAtDUG9pbnRJbWFnZQD#####AAAAAAANAAABBQAAAAADAAAABAAAAAMA#####wEAAAAADQAAAQABAAAAAwA#8AAAAAAAAAAAAAMA#####wEAAAAADQAAAQABAAAAAQA#8AAAAAAAAAAAAAMA#####wEAAAAADQAAAQABAAAABQA#8AAAAAAAAAAAAAQA#####wEAAAAADQAAAQUAAUAmAAAAAAAAAAAABgAAAAMA#####wEAAAAADQAAAQABAAAACQE#8AAAAAAAAP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAAA0AAAEFAAAAAAoAAAAHAAAABwD#####AQAAAAANAAABBQAAAAAKAAAACP####8AAAABAAhDVmVjdGV1cgD#####AAAAAAAKAAABAAEAAAALAAAACQIAAAAIAP####8AAAAAAAoAAAEAAQAAAAkAAAALAgAAAAgA#####wAAAAAACgAAAQABAAAACwAAAAwCAAAACAD#####AAAAAAAKAAABAAEAAAAMAAAACwL#####AAAAAQAHQ0NhbGN1bAD#####AAZzb2xtaW4AAi0x#####wAAAAEADENNb2luc1VuYWlyZQAAAAE#8AAAAAAAAAAAAAkA#####wAGc29sbWF4AAExAAAAAT#wAAAAAAAAAAAACQD#####AANtb3kAATAAAAABAAAAAAAAAAD#####AAAAAgAGQ0xhdGV4AP####8AAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAJEQAAAAAAAQAAAAAADFxWYWx7c29sbWlufQAAAAsA#####wAAAAAAAAAAAAAAAABAGAAAAAAAAAAAAAsRAAAAAAABAAAAAAAJXFZhbHttb3l9AAAACwD#####AAAAAAAAAAAAAAAAAEAYAAAAAAAAAAAADBEAAAAAAAEAAAAAAAxcVmFse3NvbG1heH3#####AAAAAQAHQ01pbGlldQD#####AQAAAAANAAABBQAAAAADAAAAAQAAAAwA#####wAAAAAADQAAAQUAAAAAAQAAAAEAAAAMAP####8BAAAAAA0AAAEFAAAAAAEAAAAFAAAACQD#####AAJsZwABMQAAAAE#8AAAAAAAAAAAAAsA#####wAAAAAAAAAAAAAAAABAJAAAAAAAAAAAABcRAAAAAAABAAAAAAAIXFZhbHtsZ30AAAALAP####8AAAAAAAAAAAAAAAAAQCQAAAAAAAAAAAAZEQAAAAAAAQAAAAAACFxWYWx7bGd9################'
    } else if (st.num === 2) {
      txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH2+v4BAP8BAAAAAAAAAAACJwAAAFEAAAAAAAAAAAAAAAAAAAAX#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEAB0NDYWxjdWwA#####wAGdmFsbWF4AAE4AAAAAUAgAAAAAAAAAAAAAgD#####AAZ2YWxtaW4AAi01#####wAAAAEADENNb2luc1VuYWlyZQAAAAFAFAAAAAAAAAAAAAIA#####wAHbW95ZW5uZQARKHZhbG1pbit2YWxtYXgpLzL#####AAAAAQAKQ09wZXJhdGlvbgMAAAAEAP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAACAAAABQAAAAEAAAABQAAAAAAAAAAAAAACAP####8AB21hc29tbWUADXZhbG1pbit2YWxtYXgAAAAEAAAAAAUAAAACAAAABQAAAAH#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAANAAABBQABQFPAAAAAAABAOgAAAAAAAP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AAAAAAANAAABAAEAAAAFAT#wAAAAAAAA#####wAAAAIABkNMYXRleAD#####AAAAAAAAAAAAAAAAAEAYAAAAAAAAAAAABREAAAAAAAEAAAAAAAxcVmFse3ZhbG1pbn3#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8AAAAAAA0AAAEFAAFAdeAAAAAAAAAAAAYAAAAIAP####8AAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAIEQAAAAAAAQAAAAAADFxWYWx7dmFsbWF4fQAAAAIA#####wAFY29lZjEAAzAuNgAAAAE#4zMzMzMzMwAAAAIA#####wAFY29lZjIAATUAAAABQBQAAAAAAAD#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAUAAAAFAAAACv####8AAAABAAtDUG9pbnRJbWFnZQD#####AAAAAAANAAABBQAAAAAIAAAADAAAAAoA#####wAAAAUAAAAFAAAACwAAAAsA#####wAAAAAADQAAAQUAAAAACAAAAA4AAAAIAP####8AAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAANEQAAAAAAAQAAAAAADVxWYWx7bW95ZW5uZX0AAAAIAP####8AAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAPEQAAAAAAAQAAAAAAF1xmcmFje1xWYWx7bWFzb21tZX19ezJ9#####wAAAAEAB0NNaWxpZXUA#####wAAAAAADQAAAQUAAAAABQAAAAj#####AAAAAQAIQ1NlZ21lbnQA#####wAAAAAACgAAAQACAAAABQAAABIAAAANAP####8AAAAAAAoAAAEAAgAAABIAAAAI#####wAAAAEADkNNYXJxdWVTZWdtZW50AP####8AAAAAAAEBAAAAEwAAAA4A#####wAAAAAAAQEAAAAU################'
    } else if (st.num === 3) {
      txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAANmcmH2+v4BAP8BAAAAAAAAAAACKQAAAFoAAAAAAAAAAAAAAAAAAABo#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEAB0NDYWxjdWwA#####wAEcmFwMgAEMC4wOAAAAAE#tHrhR64UewAAAAIA#####wAKcmFwcG9ydHNpbQAEMC4xNQAAAAE#wzMzMzMzM#####8AAAABAApDUG9pbnRCYXNlAP####8AAAAAAA4AAAEFAAFAcXAAAAAAAEA8AAAAAAAA#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8AAAAAAA4AAAEAAQAAAAMBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8AAAAAAA4AAAEFAAHAYGAAAAAAAAAAAAT#####AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAAAP#####AAAAAQALQ1BvaW50SW1hZ2UA#####wAAAAAADgAAAQUAAAAABQAAAAYAAAAEAP####8BAAAAAA4AAAEAAQAAAAUAP#AAAAAAAAAAAAAEAP####8BAAAAAA4AAAEAAQAAAAMAP#AAAAAAAAAAAAAEAP####8BAAAAAA4AAAEAAQAAAAcAP#AAAAAAAAAAAAAFAP####8BAAAAAA4AAAEFAAFAJgAAAAAAAAAAAAgAAAAEAP####8BAAAAAA4AAAEAAQAAAAsBP#AAAAAAAAD#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAOAAABBQAAAAAMAAAACQAAAAgA#####wEAAAAADgAAAQUAAAAADAAAAAr#####AAAAAQAIQ1ZlY3RldXIA#####wAAAAAACgAAAQABAAAADQAAAAsCAAAACQD#####AAAAAAAKAAABAAEAAAALAAAADQIAAAAJAP####8AAAAAAAoAAAEAAQAAAA0AAAAOAgAAAAkA#####wAAAAAACgAAAQABAAAADgAAAA0CAAAAAgD#####AAZzb2xtaW4AAi0x#####wAAAAEADENNb2luc1VuYWlyZQAAAAE#8AAAAAAAAAAAAAIA#####wAGc29sbWF4AAExAAAAAT#wAAAAAAAAAAAAAgD#####AANtb3kAATAAAAABAAAAAAAAAAD#####AAAAAgAGQ0xhdGV4AP####8AAAAAAMAAAAAAAAAAQCgAAAAAAAAAAAALEQAAAAAAAQAAAAAADFxWYWx7c29sbWlufQAAAAsA#####wAAAAAAAAAAAAAAAABAGAAAAAAAAAAAAA0RAAAAAAABAAAAAAAJXFZhbHttb3l9AAAACwD#####AAAAAABAAAAAAAAAAEAkAAAAAAAAAAAADhEAAAAAAAEAAAAAAAxcVmFse3NvbG1heH3#####AAAAAQAHQ01pbGlldQD#####AQAAAAAOAAABBQAAAAAFAAAAAwAAAAwA#####wAAAAAADgAAAQUAAAAAAwAAAAMAAAAMAP####8BAAAAAA4AAAEFAAAAAAMAAAAHAAAAAgD#####AAJsZwABMQAAAAE#8AAAAAAAAAAAAAsA#####wAAAAAAAAAAAAAAAABAJAAAAAAAAAAAABkRAAAAAAABAAAAAAAIXFZhbHtsZ30AAAALAP####8AAAAAAAAAAAAAAAAAQCQAAAAAAAAAAAAbEQAAAAAAAQAAAAAACFxWYWx7bGd9AAAAAgD#####AAZwb3NpbnQAATcAAAABQBwAAAAAAAAAAAACAP####8ACHBvc3VuaW9uAAIxMgAAAAFAKAAAAAAAAP####8AAAABAAtDSG9tb3RoZXRpZQD#####AAAAA#####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAfAAAABwD#####AQAAAAAOAAABBQAAAAANAAAAIQAAAA0A#####wAAAAMAAAAOAAAAIAAAAAcA#####wEAAAAADgAAAQUAAAAADQAAACP#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAADwAAAAcA#####wEAAAAADgAAAQUAAAAAIgAAACUAAAAPAP####8AAAARAAAABwD#####AQAAAAAOAAABBQAAAAAiAAAAJwAAAAcA#####wEAAAAADgAAAQUAAAAAJAAAACcAAAAHAP####8BAAAAAA4AAAEFAAAAACQAAAAlAAAAAgD#####AAhoY3JvY2hldAABMwAAAAFACAAAAAAAAP####8AAAABAAtDU2ltaWxpdHVkZQD#####AAAAJv####8AAAABAApDT3BlcmF0aW9uAwAAAA4AAAAAAAAAAUAAAAAAAAAAAAAADgAAAAIAAAAHAP####8BAAAAAA4AAAEFAAAAACIAAAAsAAAABgD#####AAAAJgAAAAcA#####wEAAAAADgAAAQUAAAAALQAAAC4AAAAQAP####8AAAAoAAAACgAAABEDAAAADgAAAAAAAAABQAAAAAAAAAAAAAAOAAAAAgAAAAcA#####wEAAAAADgAAAQUAAAAAIgAAADAAAAAQAP####8AAAApAAAACgAAABEDAAAADgAAAAAAAAABQAAAAAAAAAAAAAAOAAAAAgAAAAcA#####wEAAAAADgAAAQUAAAAAJAAAADIAAAAQAP####8AAAAqAAAAEQMAAAAOAAAAAAAAAAFAAAAAAAAAAAAAAA4AAAACAAAABwD#####AQAAAAAOAAABBQAAAAAkAAAANAAAAAYA#####wAAACoAAAAHAP####8BAAAAAA4AAAEFAAAAADUAAAA2AAAABgD#####AAAAKQAAAAcA#####wEAAAAADgAAAQUAAAAAMwAAADgAAAAGAP####8AAAAoAAAABwD#####AQAAAAAOAAABBQAAAAAxAAAAOv####8AAAABAAhDU2VnbWVudAD#####AAB#AAAKAAABAAMAAAAtAAAALwAAABIA#####wAAfwAACgAAAQADAAAANQAAADcAAAASAP####8AAH8AAAoAAAEAAwAAADMAAAA5AAAAEgD#####AAB#AAAKAAABAAMAAAA7AAAAMQAAAAIA#####wAJcGx1c21vaW5zAAItMQAAAAoAAAABP#AAAAAAAAAAAAANAP####8AAAAoAAAAEQIAAAAOAAAAQAAAAA4AAAABAAAABwD#####AQB#AAAOAAABBQAAAAAiAAAAQQAAAA0A#####wAAACkAAAARAgAAAA4AAABAAAAADgAAAAEAAAAHAP####8BAH8AAA4AAAEFAAAAACQAAABDAAAADQD#####AAAAKgAAABECAAAADgAAAEAAAAAOAAAAAQAAAAcA#####wEAfwAADgAAAQUAAAAAJAAAAEUAAAANAP####8AAAAmAAAAEQIAAAAOAAAAQAAAAA4AAAABAAAABwD#####AQB#AAAOAAABBQAAAAAiAAAARwAAAAkA#####wEAfwAACgAAAQACAAAAJgAAAEgCAAAACQD#####AQB#AAAKAAABAAIAAAAqAAAARgIAAAAJAP####8BAH8AAAoAAAEAAgAAACkAAABEAgAAAAkA#####wEAfwAACgAAAQACAAAAKAAAAEICAAAADwD#####AAAASQAAAAcA#####wEAfwAADgAAAQUAAAAALQAAAE0AAAAHAP####8BAH8AAA4AAAEFAAAAAC8AAABNAAAADwD#####AAAASgAAAAcA#####wEAfwAADgAAAQUAAAAANwAAAFAAAAAHAP####8BAH8AAA4AAAEFAAAAADUAAABQAAAADwD#####AAAATAAAAAcA#####wEAfwAADgAAAQUAAAAAMQAAAFMAAAAHAP####8BAH8AAA4AAAEFAAAAADsAAABTAAAADwD#####AAAASwAAAAcA#####wEAfwAADgAAAQUAAAAAMwAAAFYAAAAHAP####8BAH8AAA4AAAEFAAAAADkAAABWAAAAEgD#####AAB#AAAKAAABAAMAAAA3AAAAUQAAABIA#####wAAfwAACgAAAQADAAAAUgAAADUAAAASAP####8AAH8AAAoAAAEAAwAAAC8AAABPAAAAEgD#####AAB#AAAKAAABAAMAAABOAAAALQAAABIA#####wAAfwAACgAAAQADAAAAVAAAADEAAAASAP####8AAH8AAAoAAAEAAwAAADsAAABVAAAAEgD#####AAB#AAAKAAABAAMAAABXAAAAMwAAABIA#####wAAfwAACgAAAQADAAAAOQAAAFgAAAASAP####8AAH8AAAoAAAEAAwAAACYAAAAoAAAADQD#####AAAAKgAAAAoAAAARAgAAAAFAAAAAAAAAAAAAAA4AAAABAAAABwD#####AQB#AAANAAABBQAAAAAkAAAAYgAAAA0A#####wAAACkAAAAKAAAAEQIAAAABQAAAAAAAAAAAAAAOAAAAAQAAAAcA#####wEAfwAADQAAAQUAAAAAJAAAAGT#####AAAAAQANQ0RlbWlEcm9pdGVPQQD#####AAB#AAAKAAABAAMAAAAqAAAAYwAAABMA#####wAAfwAACgAAAQADAAAAKQAAAGX###############8='
    }
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true);
  }

  function modifFig (st) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété val qui est un tableau donnant (dans l’ordre croissant) les valeurs à afficher sur l’axe
    // et une propriété ecart
    // st contient aussi aussi une propriété num
    if ((st.num === 1) || (st.num === 3)) {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'solmin', String(st.val[0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'solmax', String(st.val[2]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'moy', String(st.val[1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'lg', String(st.val[3]))
    }
    if (st.num === 2) {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'valmin', String(st.val[0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'valmax', String(st.val[2]))
      if (Math.abs(Math.round((j3pCalculValeur(st.val[0]) + j3pCalculValeur(st.val[2])) / 2) - (j3pCalculValeur(st.val[0]) + j3pCalculValeur(st.val[2])) / 2) < Math.pow(10, -12)) {
        // moyenne entière
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'moyenne', String(st.val[1]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'coef1', String(0.5))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'coef2', '5')
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'somme', j3pGetLatexSomme(st.val[0], st.val[2]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'coef1', String(5))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'coef2', '0.5')
      }
    }
    if (st.num === 3) {
      // c’est le cas de l’inéquation
      // posint vaudra 0 si on veut un intervalle, 10 sinon
      // posunion vaudra 0 si on veut une réunion d’intervalles, 10 sinon
      // plusmoins sert à modifier la nature de l’intervalle (ou l’union) : fermé ou ouvert
      // ordre est pour savoir si l’inéquation est sous la forme |ax+b|<k (true) ou non
      if (st.ordre) {
        if ((st.signe === '\\geq') || (st.signe === '>')) { // on a une réunion
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posunion', '0')
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posint', '10')
        } else {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posunion', '10')
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posint', '0')
        }
        if ((st.signe === '\\geq') || (st.signe === '<')) {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'plusmoins', '-1')
        } else {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'plusmoins', '1')
        }
      } else {
        if ((st.signe === '\\leq') || (st.signe === '<')) { // on a une réunion
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posunion', '0')
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posint', '10')
        } else {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posunion', '10')
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'posint', '0')
        }
        if ((st.signe === '\\leq') || (st.signe === '>')) {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'plusmoins', '-1')
        } else {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'plusmoins', '1')
        }
      }
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function enleveCrochetsIntervalle (intTxt) {
    // intTxt est un intervalle
    // cette fonction renvoie le même intervalle mais en y ayant enlevé les crochets
    // cela permet de tester si l’élève a fait seulement une erreur de crochets
    let intSansCrochets = ''
    if ((intTxt.charAt(0) === ']') || (intTxt.charAt(0) === '[')) {
      intSansCrochets = intTxt.substring(1)
    }
    const dernierCaractere = intSansCrochets.charAt(intSansCrochets.length - 1)
    if ((dernierCaractere === ']') || (dernierCaractere === '[')) {
      intSansCrochets = intSansCrochets.substring(0, intSansCrochets.length - 1)
    }
    return intSansCrochets
  }

  function extraireCoefsFctAffine (fctAffine, variable) {
    // on donne une fonction affine : fctAffine
    // on revoie un tableau de 2 éléments : coefA et coefB de l’écriture ax+b au format latex
    // variable est optionnel (par défaut x)
    const nomVar = (variable) || 'x'
    let coefA, coefB
    if ((fctAffine[0] === '(') && (fctAffine[fctAffine.length - 1] === ')')) {
      fctAffine = fctAffine.substring(1, fctAffine.length - 1)
    }
    if ((fctAffine.substring(1).indexOf('+') === -1) && (fctAffine.substring(1).indexOf('-') === -1)) {
      // c’est qu’il n’y a qu’un seul terme dans la fonction affine
      if (fctAffine.indexOf(nomVar) === -1) {
        // c’est une fonction constante
        coefA = '0'
        coefB = fctAffine
      } else {
        // c’est une fonction linéaire
        coefA = fctAffine.substring(0, fctAffine.length - nomVar.length)
        if (coefA === '') {
          coefA = '1'
        } else if (coefA === '-') {
          coefA = '-1'
        }
        coefB = '0'
      }
    } else {
      const posDecoup = Math.max(fctAffine.lastIndexOf('+'), fctAffine.lastIndexOf('-'))// cela me donne la position du signe entre b et ax
      if (posDecoup <= 0) {
        // c’est que c’est de la forme ax donc b vaut 0'
        coefA = fctAffine.substring(0, fctAffine.length - 1)
        if (coefA === '') {
          coefA = '1'
        } else if (coefA === '-') {
          coefA = '-1'
        }
        coefB = '0'
      } else {
        if (fctAffine.substring(fctAffine.length - nomVar.length) === nomVar) {
          // fctAffine est écrit sous la forme b+ax
          coefB = fctAffine.substring(0, posDecoup)
          if (fctAffine[posDecoup] === '-') {
            coefA = fctAffine.substring(posDecoup, fctAffine.length - nomVar.length)
          } else {
            coefA = fctAffine.substring(posDecoup + 1, fctAffine.length - nomVar.length)
          }
          if (coefA === '') {
            coefA = '1'
          } else if (coefA === '-') {
            coefA = '-1'
          }
        } else {
          // fctAffine est écrit sous la forme ax+b
          coefA = fctAffine.substring(0, posDecoup - nomVar.length)
          if (fctAffine[posDecoup] === '-') {
            coefB = fctAffine.substring(posDecoup)
          } else {
            coefB = fctAffine.substring(posDecoup + 1)
          }
          if (coefA === '') {
            coefA = '1'
          } else if (coefA === '-') {
            coefA = '-1'
          }
        }
      }
    }
    // si on avait a*x au lieu de ax, il faut se débarrasser du signe de multiplication
    if (coefA.charAt(coefA.length - 1) === '*') {
      coefA = coefA.substring(0, coefA.length - 1)
    }
    coefA = fractionLatex(coefA)
    coefB = fractionLatex(coefB)
    return [coefA, coefB]
  }

  function fractionLatex (texte) {
    // texte est une expression dans laquelle on peut retrouver une fraction sous la forme .../... ou (.../...)
    // cette fonction renvoie le texte au format latex
    const fractReg1 = /\([0-9+\-*a-z]+\)\/\([0-9+\-*a-z]+\)/g // new RegExp('\\([0-9+\\-\\*a-z]{1,}\\)/\\([0-9\\+\\-\\*a-z]{1,}\\)', 'ig')// on cherche (...)/(...)
    const fractReg2 = /\([0-9*a-z]+\/[0-9*a-z]+\)/g // new RegExp('\\([0-9\\*a-z]{1,}/[0-9\\*a-z]{1,}\\)', 'ig')// on cherche (.../...)
    let texteLatex = texte
    let i, tabNumDen, num, den
    if (fractReg1.test(texteLatex)) {
      // l’expression (...)/(...) est présente'
      const tabFract1 = texteLatex.match(fractReg1)
      for (i = 0; i < tabFract1.length; i++) {
        tabNumDen = tabFract1[i].split('/')
        if (tabNumDen[0][tabNumDen[0].length - 1] === ')') {
          num = tabNumDen[0].substring(1, tabNumDen[0].length - 1)
        } else {
          num = tabNumDen[0].substring(1, tabNumDen[0].length)
        }
        if (tabNumDen[1][0] === '(') {
          den = tabNumDen[1].substring(1, tabNumDen[1].length - 1)
        } else {
          den = tabNumDen[1].substring(0, tabNumDen[1].length - 1)
        }
        texteLatex = texteLatex.replace(tabFract1[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    if (fractReg2.test(texteLatex)) {
      // l’expression (...)/(...) est présente'
      const tabFract2 = texteLatex.match(fractReg2)
      for (i = 0; i < tabFract2.length; i++) {
        tabNumDen = tabFract2[i].split('/')
        num = tabNumDen[0].substring(1)
        den = tabNumDen[1].substring(0, tabNumDen[1].length - 1)
        texteLatex = texteLatex.replace(tabFract2[i], '\\frac{' + num + '}{' + den + '}')
      }
    }
    if (String(texteLatex).indexOf('/') > -1) {
      // il reste encore une fraction à gérer à la main, sans doute du style .../...
      const signeReg = /[+\-()*]/g // new RegExp('[\\+\\-\\(\\)\\*]{1}', 'ig')
      const tabSansSigne = texteLatex.split(signeReg)
      for (i = 0; i < tabSansSigne.length; i++) {
        if (tabSansSigne[i].indexOf('/') > -1) {
          const newTab = tabSansSigne[i].split('/')
          texteLatex = texteLatex.replace(newTab[0] + '/' + newTab[1], '\\frac{' + newTab[0] + '}{' + newTab[1] + '}')
        }
      }
    }
    // console.log("texteLatex 2 :"+texteLatex)
    return texteLatex
  }

  function decimalFrac (nbTxt) {
    // nbTxt est un nombre, peu importe son format.
    // S’il est décimal, je le transforme en valeur fractionnaire. C’est juste pour mieux gérer le calcul sur les décimaux qui peuvent entrainer des erreurs à 10^{-15} près
    // le séparateur des décimaux est une virgule
    const decimalRegexp = /[0-9]+[.,][0-9]+/g // new RegExp('[0-9]{1,}(\\.|,)[0-9]{1,}', 'ig')
    const tabDecimaux = String(nbTxt).match(decimalRegexp)
    // console.log("tabDecimaux:"+tabDecimaux);
    let newNbTxt = String(nbTxt)
    if (tabDecimaux !== null) {
      for (let i = 0; i < tabDecimaux.length; i++) {
        const nb = tabDecimaux[i]
        const posVirgule = Math.max(nb.indexOf(','), nb.indexOf('.'))
        const nbChiffresApresVirgule = nb.length - posVirgule - 1
        const puissance10 = String(Math.pow(10, nbChiffresApresVirgule))
        const newNb = j3pGetLatexQuotient(j3pGetLatexProduit(nb, puissance10), puissance10)
        newNbTxt = newNbTxt.replace(nb, newNb)
      }
    }
    return newNbTxt
  }

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCons3)
    j3pDetruit(stor.zoneCons4)
    // le style n’est pas le même suivant que la réponse soit bonne ou fausse'
    j3pStyle(stor.zoneExpli, { color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    // pour le repère :
    let a = stor.coefsmembreGauche[me.questionCourante - 1][0]
    let b = stor.coefsmembreGauche[me.questionCourante - 1][1]
    const c = stor.coefsmembreDroite[me.questionCourante - 1][0]
    const d = stor.coefsmembreDroite[me.questionCourante - 1][1]
    let solMin
    let solMax
    let a1, b1, c1, d1, egalite1, egalite2, egalite3, borneInf, borneSup, ensembleSol
    let e1, f1, k, tabCoef1, tabCoef2, g1, h1, membreGauche, coefDevantX, membreDroite
    let fctAffineGauche, membreAbs, valCentrale, fctAffineDroite
    let casFigureSansCoef, maborne, permut
    let maCorrection2, valC, valA
    if ((ds.type_exo === 'equation') && (ds.type_eq[me.questionCourante - 1] === 2)) {
      // équation |ax+b|=|cx+d|
      membreGauche = '|' + stor.membreGauche[me.questionCourante - 1] + '|'
      fctAffineGauche = stor.membreGauche[me.questionCourante - 1]
      if ((fctAffineGauche.indexOf('x') === fctAffineGauche.length - 1) && (b !== '0')) {
        // de la forme b+ax
        if (j3pCalculValeur(a) > 0) {
          membreGauche = '|' + stor.membreGauche[me.questionCourante - 1] + '|=|' + j3pGetLatexMonome(1, 0, b) + '-(' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + ')|'
        }
        a1 = b
        b1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a))
        coefDevantX = j3pGetLatexOppose(a)
        solMin = b
      } else {
        // de la forme ax+b
        if (j3pCalculValeur(b) > 0) {
          membreGauche = '|' + stor.membreGauche[me.questionCourante - 1] + '|=|' + j3pGetLatexMonome(1, 1, a) + '-(' + j3pGetLatexMonome(1, 0, j3pGetLatexOppose(b)) + ')|'
        }
        a1 = j3pGetLatexMonome(1, 1, a)
        b1 = j3pGetLatexMonome(1, 0, j3pGetLatexOppose(b))
        coefDevantX = a
        solMin = j3pGetLatexOppose(b)
      }
      membreDroite = '|' + stor.membreDroite[me.questionCourante - 1] + '|'
      fctAffineDroite = stor.membreDroite[me.questionCourante - 1]
      if ((fctAffineDroite.indexOf('x') === fctAffineDroite.length - 1) && (d !== '0')) {
        // de la forme b+ax
        if (j3pCalculValeur(c) > 0) {
          membreDroite = '|' + stor.membreDroite[me.questionCourante - 1] + '|=|' + j3pGetLatexMonome(1, 0, d) + '-(' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + ')|'
        }
        c1 = d
        d1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c))
        solMax = d
      } else {
        // de la forme ax+b
        if (j3pCalculValeur(d) > 0) {
          membreDroite = '|' + stor.membreDroite[me.questionCourante - 1] + '|=|' + j3pGetLatexMonome(1, 1, c) + '-(' + j3pGetLatexMonome(1, 0, j3pGetLatexOppose(d)) + ')|'
        }
        c1 = j3pGetLatexMonome(1, 1, c)
        d1 = j3pGetLatexOppose(d)
        solMax = j3pGetLatexOppose(d)
      }
      // corr1bis : "$£v$ est la distance entre $£a$ et $£b$ et $£w$ la distance entre $£c$ et $£d$.",
      j3pAffiche(stor.zoneExpli1, '', textes.corr1bis, {
        v: membreGauche, a: a1, b: b1, w: membreDroite, c: c1, d: d1
      })
      // corr2bis : "On veut que ces deux distances soient égales",
      j3pAffiche(stor.zoneExpli2, '', textes.corr2bis)
      if (j3pCalculValeur(solMin) > j3pCalculValeur(solMax)) {
        permut = solMin
        solMin = solMax
        solMax = permut
      }
      const moyenne = j3pGetLatexQuotient(j3pGetLatexSomme(solMin, solMax), '2')
      e1 = j3pGetLatexMonome(1, 1, a) + '=' + j3pGetLatexMonome(1, 0, moyenne)
      f1 = 'x=' + stor.tab_sol[0]
      if (String(coefDevantX) === '1') {
        j3pAffiche(stor.zoneExpli4, '', textes.corr3ter, { e: f1 })
      } else {
        j3pAffiche(stor.zoneExpli4, '', textes.corr3, { e: e1, f: f1 })
      }
      // corr3 : "On en déduit que $£e$ et donc $£f$.",
      // corr3ter : "On en déduit que $£e$.",
      j3pAffiche(stor.zoneExpli5, '', textes.corr4, { s: stor.tab_sol[0] })
      // corr4 : "Donc la solution de l’équation est $£s$.",

      modifFig({ val: [solMin, moyenne, solMax], num: 2 })
    } else {
      if (stor.type_nbconstant[me.questionCourante - 1] === 'negatif') {
        // On gère seulement ici le cas des équations ou inéquations particulières où k est négatif
        if (ds.type_exo === 'equation') {
          j3pAffiche(stor.zoneExpli1, '', textes.corr5)
        } else {
          // c’est une inéquation
          if (stor.tab_sol[0] === ']-\\infty;+\\infty[') {
            j3pAffiche(stor.zoneExpli1, '', textes.corr5ter)
          } else {
            j3pAffiche(stor.zoneExpli1, '', textes.corr5bis)
          }
        }
        stor.tab_sol.push(']-\\infty;+\\infty[')
      } else if (stor.type_nbconstant[me.questionCourante - 1] === 'nul') {
        if (ds.type_exo === 'equation') {
          // corr6 : "La valeur absolue d’un nombre réel étant supérieure ou égale à 0, l’équation équivaut à $£e$ ce qui donne $£f$.<br>Donc la solution de l’équation est $£g$.",
          if (stor.membreGauche[me.questionCourante - 1].indexOf('x') >= 0) {
            // équation du type |ax+b|=k
            egalite1 = stor.membreGauche[me.questionCourante - 1] + '=0'
          } else {
            // équation du type k=|ax+b|
            egalite1 = stor.membreDroite[me.questionCourante - 1] + '=0'
          }
          egalite2 = 'x=' + stor.tab_sol[0]
          j3pAffiche(stor.zoneExpli1, '', textes.corr6, { e: egalite1, f: egalite2, g: stor.tab_sol[0] })
        } else {
          // c’est une inéquation
          /* corr6bis : "La valeur absolue d’un nombre réel étant supérieure ou égale à 0, elle ne peut être strictement négative et l’inéquation n’admet donc pas de solution (l’ensemble de ses solutions est $\\varnothing$).",:
            corr6ter : "La valeur absolue d’un nombre réel étant supérieure ou égale à 0, l’inégalité n’est vérifiée que lorsque $£e$ ce qui donne $£f$ et ainsi $£g$.<br>Donc l’ensemble des solutions de cette inéquation est $£h$."
                     corr6qua : "La valeur absolue d’un nombre réel étant supérieure ou égale à 0, pour tout nombre réel l’inégalité est vérifiée.<br>Donc l’ensemble des solutions de l’inéquation est $]-\\infty;+\\infty[=\\R$." */
          if (stor.tab_sol[0] === '\\varnothing') {
            j3pAffiche(stor.zoneExpli1, '', textes.corr6bis)
          } else if (stor.tab_sol[0] === ']-\\infty;+\\infty[') {
            j3pAffiche(stor.zoneExpli1, '', textes.corr6qua)
          } else {
            // dans ce cas l’inéquation se résume finalement à l’équation et l’ensemble des solutions est un singleton ou \\R privé d’un singleton
            let singleton
            if (stor.membreGauche[me.questionCourante - 1].indexOf('x') >= 0) {
              // équation du type |ax+b|=k
              singleton = false
              if (stor.signe_eq_ineq[me.questionCourante - 1] === '\\leq') {
                // là c’est un singleton
                singleton = true
                egalite1 = '|' + stor.membreGauche[me.questionCourante - 1] + '|=0'
                egalite2 = stor.membreGauche[me.questionCourante - 1] + '=0'
              } else {
                egalite1 = '|' + stor.membreGauche[me.questionCourante - 1] + '|\\neq 0'
                egalite2 = stor.membreGauche[me.questionCourante - 1] + '\\neq 0'
              }
            } else {
              if (stor.signe_eq_ineq[me.questionCourante - 1] === '\\geq') {
                // là c’est un singleton
                singleton = true
                egalite1 = '|' + stor.membreDroite[me.questionCourante - 1] + '|=0'
                egalite2 = stor.membreDroite[me.questionCourante - 1] + '=0'
              } else {
                egalite1 = '|' + stor.membreDroite[me.questionCourante - 1] + '|\\neq 0'
                egalite2 = stor.membreDroite[me.questionCourante - 1] + '\\neq 0'
              }
            }
            // stor.tab_sol[0] est la solution encadrée par des accolades
            if (singleton) {
              egalite3 = 'x=' + stor.tab_sol[0]// .substring(1,stor.tab_sol[0].length-1);
              ensembleSol = '\\opencurlybrace' + stor.tab_sol[0] + '\\closecurlybrace'
            } else {
              egalite3 = 'x\\neq ' + stor.tab_sol[0].substring(stor.tab_sol[0].indexOf(';') + 1, stor.tab_sol[0].length - 1)
              ensembleSol = stor.tab_sol[0] + '\\cup ' + stor.tab_sol[1]
            }
            j3pAffiche(stor.zoneExpli1, '', textes.corr6ter, {
              e: egalite1, f: egalite2, g: egalite3, h: ensembleSol
            })
          }
        }
      } else {
        if (stor.membreGauche[me.questionCourante - 1].indexOf('x') >= 0) {
          // équation du type |ax+b|=k
          a = stor.coefsmembreGauche[me.questionCourante - 1][0]
          b = stor.coefsmembreGauche[me.questionCourante - 1][1]
          membreAbs = '|' + stor.membreGauche[me.questionCourante - 1] + '|'
          fctAffineGauche = stor.membreGauche[me.questionCourante - 1]
          if ((fctAffineGauche.indexOf('x') === fctAffineGauche.length - 1) && (b !== '0')) {
            // de la forme b+ax
            if (j3pCalculValeur(a) > 0) {
              membreAbs = '|' + stor.membreGauche[me.questionCourante - 1] + '|=|' + j3pGetLatexMonome(1, 0, b) + '-(' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + ')|'
            }
            a1 = b
            b1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a), 'x')
            valCentrale = a1
          } else {
            // de la forme ax+b
            if (j3pCalculValeur(b) > 0) {
              membreAbs = '|' + stor.membreGauche[me.questionCourante - 1] + '|=|' + j3pGetLatexMonome(1, 1, a) + '-(' + j3pGetLatexMonome(1, 0, j3pGetLatexOppose(b)) + ')|'
            }
            a1 = j3pGetLatexMonome(1, 1, a)
            b1 = j3pGetLatexOppose(b)
            valCentrale = b1
          }
          k = stor.membreDroite[me.questionCourante - 1]
        } else {
          // équation du type k=|ax+b|
          a = stor.coefsmembreDroite[me.questionCourante - 1][0]
          b = stor.coefsmembreDroite[me.questionCourante - 1][1]
          membreAbs = '|' + stor.membreDroite[me.questionCourante - 1] + '|'
          fctAffineDroite = stor.membreDroite[me.questionCourante - 1]
          if ((fctAffineDroite.indexOf('x') === fctAffineDroite.length - 1) && (d !== '0')) {
            // de la forme b+ax
            if (j3pCalculValeur(a) > 0) {
              membreAbs = '|' + stor.membreDroite[me.questionCourante - 1] + '|=|' + j3pGetLatexMonome(1, 0, b) + '-(' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + ')|'
            }
            a1 = d
            b1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c))
            valCentrale = a1
          } else {
            // de la forme ax+b
            if (j3pCalculValeur(b) > 0) {
              membreAbs = '|' + stor.membreDroite[me.questionCourante - 1] + '|=|' + j3pGetLatexMonome(1, 1, a) + '-(' + j3pGetLatexMonome(1, 0, j3pGetLatexOppose(b)) + ')|'
            }
            a1 = j3pGetLatexMonome(1, 1, c)
            b1 = j3pGetLatexOppose(d)
            valCentrale = b1
          }
          k = stor.membreGauche[me.questionCourante - 1]
        }
        // corr1 : "$£v$ est la distance entre $£a$ et $£b$.",
        j3pAffiche(stor.zoneExpli1, '', textes.corr1, { v: membreAbs, a: a1, b: b1 })
        tabCoef1 = stor.coefsmembreGauche[me.questionCourante - 1]
        tabCoef2 = stor.coefsmembreDroite[me.questionCourante - 1]
        casFigureSansCoef = false
        // par défaut on considère que le coef devant x est différent de 1
        if (String(tabCoef1[0]) === '0') {
          // k=|ax+b| ou k=|b+ax|
          if (stor.membreDroite[me.questionCourante - 1].indexOf('x') === stor.membreDroite[me.questionCourante - 1].length - 1) {
            // k=|b+ax|
            casFigureSansCoef = (String(c) === '-1')
          } else {
            // k=|ax+b|
            casFigureSansCoef = (String(c) === '1')
          }
        } else if (String(tabCoef2[0]) === '0') {
          // |ax+b|=k ou |b+ax|=k
          if (stor.membreGauche[me.questionCourante - 1].indexOf('x') === stor.membreGauche[me.questionCourante - 1].length - 1) {
            // k=|b+ax|
            casFigureSansCoef = (String(a) === '-1')
          } else {
            // k=|ax+b|
            casFigureSansCoef = (String(a) === '1')
          }
        }
        if (ds.type_exo === 'equation') {
          // corr2 : "On veut que cette distance soit égale à $£k$.",
          j3pAffiche(stor.zoneExpli2, '', textes.corr2, { k })
          if (String(tabCoef1[0]) === '0') {
            // k=|ax+b|
            f1 = 'x=' + j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), j3pGetLatexOppose(tabCoef1[1])), c)
            h1 = 'x=' + j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), tabCoef1[1]), c)
            fctAffineDroite = stor.membreDroite[me.questionCourante - 1]
            if ((fctAffineDroite.indexOf('x') === fctAffineDroite.length - 1) && (d !== '0')) {
              // de la forme b+ax
              solMin = j3pGetLatexOppose(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), j3pGetLatexOppose(tabCoef1[1])))
              solMax = j3pGetLatexOppose(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), tabCoef1[1]))
              e1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '=' + solMin
              g1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '=' + solMax
            } else {
              solMin = j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), j3pGetLatexOppose(tabCoef1[1]))
              solMax = j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), tabCoef1[1])
              e1 = j3pGetLatexMonome(1, 1, c) + '=' + solMin
              g1 = j3pGetLatexMonome(1, 1, c) + '=' + solMax
            }
          } else {
            // |ax+b|=k
            borneInf = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef1[1]), j3pGetLatexOppose(tabCoef2[1])), tabCoef1[0])
            borneSup = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef1[1]), tabCoef2[1]), tabCoef1[0])
            if (j3pCalculValeur(tabCoef1[0]) < 0) {
              maborne = borneInf
              borneInf = borneSup
              borneSup = maborne
            }
            f1 = 'x=' + borneInf
            h1 = 'x=' + borneSup
            fctAffineGauche = stor.membreGauche[me.questionCourante - 1]
            if ((fctAffineGauche.indexOf('x') === fctAffineGauche.length - 1) && (b !== '0')) {
              // de la forme b+ax
              solMin = j3pGetLatexOppose(j3pGetLatexProduit(borneInf, a))
              solMax = j3pGetLatexOppose(j3pGetLatexProduit(borneSup, a))
              e1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '=' + solMin
              g1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '=' + solMax
            } else {
              solMin = j3pGetLatexProduit(borneInf, a)
              solMax = j3pGetLatexProduit(borneSup, a)
              e1 = j3pGetLatexMonome(1, 1, a) + '=' + solMin
              g1 = j3pGetLatexMonome(1, 1, a) + '=' + solMax
            }
          }
          // corr3bis : "On en déduit que $£e$ c’est-à-dire $£f$ ou que $£g$ c’est-à-dire $£h$.",
          // corr3qua : "On en déduit que $£e$ ou que $£f$.",
          if (casFigureSansCoef) {
            j3pAffiche(stor.zoneExpli4, '', textes.corr3qua, { e: f1, f: h1 })
          } else {
            j3pAffiche(stor.zoneExpli4, '', textes.corr3bis, { e: e1, f: f1, g: g1, h: h1 })
          }
          j3pAffiche(stor.zoneExpli5, '', textes.corr4bis, { s: stor.tab_sol[0], t: stor.tab_sol[1] })
          // corr4bis : "Donc les solutions de l’équation sont $£s$ et $£t$.*/
          if (j3pCalculValeur(solMin) > j3pCalculValeur(solMax)) {
            permut = solMin
            solMin = solMax
            solMax = permut
          }
          modifFig({ val: [solMin, valCentrale, solMax, k], num: 1 })
        } else {
          // on a une inéquation
          maCorrection2 = textes.corr2_1
          tabCoef1 = stor.coefsmembreGauche[me.questionCourante - 1]
          tabCoef2 = stor.coefsmembreDroite[me.questionCourante - 1]
          if ((String(tabCoef1[0]) === '0') || (String(tabCoef1[0]) === 0)) {
            // de la forme k=|ax+b|
            if (stor.signe_eq_ineq[me.questionCourante - 1] === '<') {
              maCorrection2 = textes.corr2_2
            } else if (stor.signe_eq_ineq[me.questionCourante - 1] === '\\leq') {
              maCorrection2 = textes.corr2_4
            } else if (stor.signe_eq_ineq[me.questionCourante - 1] === '\\geq') {
              maCorrection2 = textes.corr2_3
            }
          } else {
            if (stor.signe_eq_ineq[me.questionCourante - 1] === '>') {
              maCorrection2 = textes.corr2_2
            } else if (stor.signe_eq_ineq[me.questionCourante - 1] === '\\leq') {
              maCorrection2 = textes.corr2_3
            } else if (stor.signe_eq_ineq[me.questionCourante - 1] === '\\geq') {
              maCorrection2 = textes.corr2_4
            }
          }
          const leSigne = stor.signe_eq_ineq[me.questionCourante - 1]
          j3pAffiche(stor.zoneExpli2, '', maCorrection2, { k })
          valC = j3pCalculValeur(tabCoef2[0])
          valA = j3pCalculValeur(tabCoef1[0])
          if ((String(tabCoef1[0]) === '0') || (String(tabCoef1[0]) === 0)) {
            // k=|ax+b|
            borneInf = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), j3pGetLatexOppose(tabCoef1[1])), c)
            borneSup = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), tabCoef1[1]), c)
            if (j3pCalculValeur(borneInf) > j3pCalculValeur(borneSup)) {
              maborne = borneInf
              borneInf = borneSup
              borneSup = maborne
            }
            if (leSigne === '>') {
              f1 = borneInf + ' < x < ' + borneSup
            } else if (leSigne === '\\geq') {
              f1 = borneInf + '\\leq x\\leq ' + borneSup
            } else if (leSigne === '<') {
              f1 = 'x<' + borneInf
              h1 = 'x>' + borneSup
            } else if (leSigne === '\\leq') {
              f1 = 'x\\leq ' + borneInf
              h1 = 'x\\geq ' + borneSup
            }
            fctAffineDroite = stor.membreDroite[me.questionCourante - 1]
            if ((fctAffineDroite.indexOf('x') === fctAffineDroite.length - 1) && (d !== '0')) {
              // de la forme b+ax
              solMin = j3pGetLatexOppose(j3pGetLatexProduit(borneInf, c))
              solMax = j3pGetLatexOppose(j3pGetLatexProduit(borneSup, c))
              if (leSigne === '>') {
                if (valC < 0) {
                  e1 = solMin + '<' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '<' + solMax
                } else {
                  e1 = solMax + '<' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '<' + solMin
                }
              } else if (leSigne === '\\geq') {
                if (valC < 0) {
                  e1 = solMin + '\\leq ' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '\\leq ' + solMax
                } else {
                  e1 = solMax + '\\leq ' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '\\leq ' + solMin
                }
              } else if (leSigne === '<') {
                if (valC < 0) {
                  e1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '<' + solMin
                  g1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '>' + solMax
                } else {
                  e1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '>' + solMin
                  g1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '<' + solMax
                }
              } else if (leSigne === '\\leq') {
                if (valC < 0) {
                  e1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '\\leq ' + solMin
                  g1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '\\geq ' + solMax
                } else {
                  e1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '\\geq ' + solMin
                  g1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(c)) + '\\leq ' + solMax
                }
              }
            } else {
              solMin = j3pGetLatexProduit(borneInf, c)
              solMax = j3pGetLatexProduit(borneSup, c)
              if (leSigne === '>') {
                if (valC < 0) {
                  e1 = solMax + '<' + j3pGetLatexMonome(1, 1, c) + '<' + solMin
                } else {
                  e1 = solMin + '<' + j3pGetLatexMonome(1, 1, c) + '<' + solMax
                }
              } else if (leSigne === '\\geq') {
                if (valC < 0) {
                  e1 = solMax + '\\leq ' + j3pGetLatexMonome(1, 1, c) + '\\leq ' + solMin
                } else {
                  e1 = solMin + '\\leq ' + j3pGetLatexMonome(1, 1, c) + '\\leq ' + solMax
                }
              } else if (leSigne === '<') {
                if (valC < 0) {
                  e1 = j3pGetLatexMonome(1, 1, c) + '>' + solMin
                  g1 = j3pGetLatexMonome(1, 1, c) + '<' + solMax
                } else {
                  e1 = j3pGetLatexMonome(1, 1, c) + '<' + solMin
                  g1 = j3pGetLatexMonome(1, 1, c) + '>' + solMax
                }
              } else if (leSigne === '\\leq') {
                if (valC < 0) {
                  e1 = j3pGetLatexMonome(1, 1, c) + '\\geq ' + solMin
                  g1 = j3pGetLatexMonome(1, 1, c) + '\\leq ' + solMax
                } else {
                  e1 = j3pGetLatexMonome(1, 1, c) + '\\leq ' + solMin
                  g1 = j3pGetLatexMonome(1, 1, c) + '\\geq ' + solMax
                }
              }
            }
          } else {
            // |ax+b|=k
            borneInf = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef1[1]), j3pGetLatexOppose(tabCoef2[1])), tabCoef1[0])
            borneSup = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef1[1]), tabCoef2[1]), tabCoef1[0])
            if (j3pCalculValeur(tabCoef1[0]) < 0) {
              maborne = borneInf
              borneInf = borneSup
              borneSup = maborne
            }
            me.logIfDebug('borneInf:' + borneInf + '   borneSup:' + borneSup)
            if (leSigne === '<') {
              f1 = borneInf + ' < x < ' + borneSup
            } else if (leSigne === '\\leq') {
              f1 = borneInf + '\\leq x\\leq ' + borneSup
            } else if (leSigne === '>') {
              f1 = 'x<' + borneInf
              h1 = 'x>' + borneSup
            } else if (leSigne === '\\geq') {
              f1 = 'x\\leq ' + borneInf
              h1 = 'x\\geq ' + borneSup
            }
            fctAffineGauche = stor.membreGauche[me.questionCourante - 1]
            if ((fctAffineGauche.indexOf('x') === fctAffineGauche.length - 1) && (b !== '0')) {
              // de la forme b+ax
              solMin = j3pGetLatexOppose(j3pGetLatexProduit(borneInf, a))
              solMax = j3pGetLatexOppose(j3pGetLatexProduit(borneSup, a))
              if (leSigne === '<') {
                if (valA > 0) {
                  e1 = solMax + '<' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '<' + solMin
                } else {
                  e1 = solMin + '<' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '<' + solMax
                }
              } else if (leSigne === '\\leq') {
                if (valA > 0) {
                  e1 = solMax + '\\leq ' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '\\leq ' + solMin
                } else {
                  e1 = solMin + '\\leq ' + j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '\\leq ' + solMax
                }
              } else if (leSigne === '>') {
                if (valA > 0) {
                  e1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '>' + solMin
                  g1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '<' + solMax
                } else {
                  e1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '<' + solMin
                  g1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '>' + solMax
                }
              } else if (leSigne === '\\geq') {
                if (valA > 0) {
                  e1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '\\geq ' + solMin
                  g1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '\\leq ' + solMax
                } else {
                  e1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '\\leq ' + solMin
                  g1 = j3pGetLatexMonome(1, 1, j3pGetLatexOppose(a)) + '\\geq ' + solMax
                }
              }
            } else {
              solMin = j3pGetLatexProduit(borneInf, a)
              solMax = j3pGetLatexProduit(borneSup, a)
              if (leSigne === '<') {
                if (valA < 0) {
                  e1 = solMax + '<' + j3pGetLatexMonome(1, 1, a) + '<' + solMin
                } else {
                  e1 = solMin + '<' + j3pGetLatexMonome(1, 1, a) + '<' + solMax
                }
              } else if (leSigne === '\\leq') {
                if (valA < 0) {
                  e1 = solMax + '\\leq ' + j3pGetLatexMonome(1, 1, a) + '\\leq ' + solMin
                } else {
                  e1 = solMin + '\\leq ' + j3pGetLatexMonome(1, 1, a) + '\\leq ' + solMax
                }
              } else if (leSigne === '>') {
                if (valA < 0) {
                  e1 = j3pGetLatexMonome(1, 1, a) + '>' + solMin
                  g1 = j3pGetLatexMonome(1, 1, a) + '<' + solMax
                } else {
                  e1 = j3pGetLatexMonome(1, 1, a) + '<' + solMin
                  g1 = j3pGetLatexMonome(1, 1, a) + '>' + solMax
                }
              } else if (leSigne === '\\geq') {
                if (valA < 0) {
                  e1 = j3pGetLatexMonome(1, 1, a) + '\\geq ' + solMin
                  g1 = j3pGetLatexMonome(1, 1, a) + '\\leq ' + solMax
                } else {
                  e1 = j3pGetLatexMonome(1, 1, a) + '\\leq ' + solMin
                  g1 = j3pGetLatexMonome(1, 1, a) + '\\geq ' + solMax
                }
              }
            }
          }
          if ((String(tabCoef1[0]) === '0') || (String(tabCoef1[0]) === 0)) {
            if (casFigureSansCoef) {
              if ((leSigne === '>') || (leSigne === '\\geq')) {
                j3pAffiche(stor.zoneExpli4, '', textes.corr3ter, { e: f1 })
              } else {
                j3pAffiche(stor.zoneExpli4, '', textes.corr3qua, { e: f1, f: h1 })
              }
            } else {
              if ((leSigne === '>') || (leSigne === '\\geq')) {
                j3pAffiche(stor.zoneExpli4, '', textes.corr3, { e: e1, f: f1 })
              } else {
                j3pAffiche(stor.zoneExpli4, '', textes.corr3bis, { e: e1, f: f1, g: g1, h: h1 })
              }
            }
          } else {
            if (casFigureSansCoef) {
              if ((leSigne === '<') || (leSigne === '\\leq')) {
                j3pAffiche(stor.zoneExpli4, '', textes.corr3ter, { e: f1 })
              } else {
                j3pAffiche(stor.zoneExpli4, '', textes.corr3qua, { e: f1, f: h1 })
              }
            } else {
              if ((leSigne === '<') || (leSigne === '\\leq')) {
                j3pAffiche(stor.zoneExpli4, '', textes.corr3, { e: e1, f: f1 })
              } else {
                j3pAffiche(stor.zoneExpli4, '', textes.corr3bis, { e: e1, f: f1, g: g1, h: h1 })
              }
            }
          }
          // j'écris l’ensemble des solutions
          if (stor.tab_sol.length === 1) {
            // ce n’est qu’un intervalle
            ensembleSol = stor.tab_sol[0]
          } else {
            ensembleSol = stor.tab_sol[0] + '\\cup ' + stor.tab_sol[1]
          }
          // corr4ter : "Donc l’ensemble des solutions de l’inéquation est $£s$."
          j3pAffiche(stor.zoneExpli5, '', textes.corr4ter, { s: ensembleSol })

          if (j3pCalculValeur(solMin) > j3pCalculValeur(solMax)) {
            const chgt = solMin
            solMin = solMax
            solMax = chgt
          }
          if (String(tabCoef1[0]) === '0') {
            modifFig({
              val: [solMin, valCentrale, solMax, k],
              num: 3,
              signe: leSigne,
              ordre: false
            })
          } else {
            modifFig({
              val: [solMin, valCentrale, solMax, k],
              num: 3,
              signe: leSigne,
              ordre: true
            })
          }
        }
      }
    }
  }

  function enonceMain () {
    // Pour chaque répétition, on récupère l'(in)égalité imposée dans le cas où elle existe (et est compréhensible), sinon je la génère
    // dans ce second cas, dans le cas d’une égalité, je fais attention au type imposé.

    if (ds.imposer_eq_ineq[me.questionCourante - 1] === '') {
      // je génère l'(in)égalité
      if (ds.type_exo === 'inequation') {
        tabSigne = ['<', '>', '\\leq', '\\geq']
        const choix = j3pGetRandomInt(0, (tabSigne.length - 1))
        stor.signe_eq_ineq[me.questionCourante - 1] = tabSigne[choix]
      } else {
        stor.signe_eq_ineq[me.questionCourante - 1] = '='
      }
      let c, d
      if ((ds.type_eq[me.questionCourante - 1] === 1) || (ds.type_exo === 'inequation')) {
        // égalité du style |ax+b|=k ou inégalité |ax+b|<k
        k = j3pGetRandomInt(3, 12)
        stor.membreDroite[me.questionCourante - 1] = String(k)
        stor.coefsmembreDroite[me.questionCourante - 1] = ['0', String(k)]
      } else {
        c = stor.coef1_devantx[me.questionCourante - 1] ? 1 : (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
        d = j3pGetRandomInt(-6, 6)
        stor.membreDroite[me.questionCourante - 1] = j3pGetLatexMonome(1, 1, c) + j3pGetLatexMonome(2, 0, d)
        stor.coefsmembreDroite[me.questionCourante - 1] = [String(c), String(d)]
        k = -d / c
      }
      let donneesOK, b
      do {
        // var a = (j3pGetRandomInt(0, 1)*2-1)*j3pGetRandomInt(1, 5)
        a = ((ds.type_eq[me.questionCourante - 1] === 2) && (ds.type_exo === 'equation')) ? c : stor.coef1_devantx[me.questionCourante - 1] ? 1 : (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
        b = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 6)
        donneesOK = true
        if ((ds.type_eq[me.questionCourante - 1] === 2) && (ds.type_exo === 'equation')) {
          // on fait en sorte que -b/a soit différent de -d/c
          donneesOK = (Math.abs(-b / a + d / c) > Math.pow(10, -12))
        }
      } while (!donneesOK)
      stor.membreGauche[me.questionCourante - 1] = j3pGetLatexMonome(1, 1, a) + j3pGetLatexMonome(2, 0, b)
      stor.coefsmembreGauche[me.questionCourante - 1] = [String(a), String(b)]// comme pour la fonction qui récupère les coef d’une fonction affine
      if (me.questionCourante > 2) {
        // on en est au moins à la troisième répétition
        // on peut alors avoir |b+ax| au lieu de |ax+b|
        const choixEcriture = j3pGetRandomInt(0, 1)
        if (choixEcriture === 1) {
          stor.membreGauche[me.questionCourante - 1] = j3pGetLatexMonome(1, 0, b) + j3pGetLatexMonome(2, 1, a)
        }
      }
    }
    if (stor.membreGauche[me.questionCourante - 1].includes('x')) {
      // le membre de gauche est une fonction affine
      stor.moneq = '|' + stor.membreGauche[me.questionCourante - 1] + '|'
    } else {
      // c’est une constante
      stor.moneq = stor.membreGauche[me.questionCourante - 1]
    }
    stor.moneq += stor.signe_eq_ineq[me.questionCourante - 1]
    if (stor.membreDroite[me.questionCourante - 1].indexOf('x') >= 0) {
      // le membre de droite est une fonction affine
      stor.moneq += '|' + stor.membreDroite[me.questionCourante - 1] + '|'
    } else {
      // c’est une constante
      stor.moneq += stor.membreDroite[me.questionCourante - 1]
    }
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    let elt
    if (ds.type_exo === 'equation') {
      j3pAffiche(zoneCons1, '', textes.consigne1, { e: stor.moneq })
      elt = j3pAffiche(zoneCons2, '', textes.consigne2,
        {
          inputmq1: { texte: '' }
        })
    } else {
      j3pAffiche(zoneCons1, '', textes.consigne1bis, { e: stor.moneq })
      elt = j3pAffiche(zoneCons2, '', textes.consigne2bis,
        {
          inputmq1: { texte: '' }
        })
    }
    stor.zoneInput = elt.inputmqList[0]
    stor.zoneCons3 = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '10px' } })
    if (ds.type_exo === 'equation') {
      j3pPaletteMathquill(stor.zoneCons3, stor.zoneInput, {
        liste: ['fraction', 'vide']
      })
    } else {
      j3pPaletteMathquill(stor.zoneCons3, stor.zoneInput, {
        liste: ['fraction', 'vide', 'union', 'inter', 'inf', ']', '[', 'R']
      })
    }
    mqRestriction(stor.zoneInput, '\\d,.;/\\-[\\]+{}', {
      commandes: (ds.type_exo === 'equation') ? ['fraction', 'vide'] : ['fraction', 'vide', 'union', 'inter', 'inf', 'R']
    })
    stor.zoneCons4 = j3pAddElt(stor.conteneur, 'div', { style: { paddingTop: '50px' } })
    if (ds.type_exo === 'equation') {
      j3pAffiche(stor.zoneCons4, '', textes.consigne3)
    }
    // JE récupère les solutions :
    stor.tab_sol = []// tableau avec les valeurs au format latex
    stor.tab_valsol = []// tableau avec les valeurs au format décimal
    const tabCoef1 = stor.coefsmembreGauche[me.questionCourante - 1]
    const tabCoef2 = stor.coefsmembreDroite[me.questionCourante - 1]
    if (ds.type_exo === 'equation') {
      if (ds.type_eq[me.questionCourante - 1] === 1) {
        // équation |ax+b|=k
        // La distance de ax à -b vaut k. Donc ax vaut -b+k ou -b-k
        // si k est négatif, il n’y a pas de solution
        if (stor.type_nbconstant[me.questionCourante - 1] === 'negatif') {
          // pas de solution
          stor.tab_sol.push('\\varnothing')
        } else if (String(tabCoef1[0]) === '0') {
          // l’équation est de la forme k=|ax+b|
          if (stor.type_nbconstant[me.questionCourante - 1] === 'nul') {
            // il y a alors une et une seule solution : -b/a
            stor.tab_sol.push(j3pGetLatexQuotient(j3pGetLatexOppose(tabCoef2[1]), tabCoef2[0]))
            stor.tab_valsol.push(j3pCalculValeur(stor.tab_sol[0]))
          } else {
            stor.tab_sol.push(j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), tabCoef1[1]), tabCoef2[0]))
            stor.tab_valsol.push(j3pCalculValeur(stor.tab_sol[0]))
            if (String(tabCoef1[1]) !== '0') {
              stor.tab_sol.push(j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), j3pGetLatexOppose(tabCoef1[1])), tabCoef2[0]))
              stor.tab_valsol.push(j3pCalculValeur(stor.tab_sol[1]))
            }
          }
        } else {
          // l’équation est de la forme |ax+b|=|cx+d|
          if (stor.type_nbconstant[me.questionCourante - 1] === 'nul') {
            // il y a alors une et une seule solution : -b/a
            stor.tab_sol.push(j3pGetLatexQuotient(j3pGetLatexOppose(tabCoef1[1]), tabCoef1[0]))
            stor.tab_valsol.push(j3pCalculValeur(stor.tab_sol[0]))
          } else {
            stor.tab_sol.push(j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef1[1]), tabCoef2[1]), tabCoef1[0]))
            stor.tab_valsol.push(j3pCalculValeur(stor.tab_sol[0]))
            if (String(tabCoef2[1]) !== '0') {
              stor.tab_sol.push(j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef1[1]), j3pGetLatexOppose(tabCoef2[1])), tabCoef1[0]))
              stor.tab_valsol.push(j3pCalculValeur(stor.tab_sol[1]))
            }
          }
        }
      } else {
        const mbGauche = stor.membreGauche[me.questionCourante - 1]
        const mbDroite = stor.membreDroite[me.questionCourante - 1]
        if (((mbGauche.charAt(mbGauche.length - 1) === 'x') && (mbDroite.charAt(mbDroite.length - 1) === 'x')) || ((mbGauche.charAt(mbGauche.length - 1) !== 'x') && (mbDroite.charAt(mbDroite.length - 1) !== 'x'))) {
          // equation |ax+b|=|cx+d| où a=c
          // une seule solution : ((-b-d)/2)/a
          stor.tab_sol.push(j3pGetLatexQuotient(j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef1[1]), j3pGetLatexOppose(tabCoef2[1])), '2'), tabCoef1[0]))
        } else {
          if (mbGauche.charAt(mbGauche.length - 1) === 'x') {
            // équation |b-ax|=|ax+d|
            stor.tab_sol.push(j3pGetLatexQuotient(j3pGetLatexQuotient(j3pGetLatexSomme(tabCoef1[1], j3pGetLatexOppose(tabCoef2[1])), '2'), tabCoef2[0]))
          } else {
            // équation |ax+b|=|d-ax|
            stor.tab_sol.push(j3pGetLatexQuotient(j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef1[1]), tabCoef2[1]), '2'), tabCoef1[0]))
          }
        }
        stor.tab_valsol.push(j3pCalculValeur(stor.tab_sol[0]))
      }

      me.logIfDebug('stor.tab_sol:' + stor.tab_sol + '   stor.tab_valsol:' + stor.tab_valsol)
    } else {
      // j’ai une inéquation de la forme |ax+b|<k ou k<|ax+b|
      stor.tab_complement = []
      stor.tab_solapproche = []
      stor.tab_complementapproche = []
      // ce tableau contiendra les intervalles (sans crochet) complémentaires de la réponse attendue
      const leSigne = stor.signe_eq_ineq[me.questionCourante - 1]
      if ((String(tabCoef1[0]) === '0') || (String(tabCoef1[0]) === 0)) {
        // k<|ax+b|
        borneInf = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), j3pGetLatexOppose(tabCoef1[1])), tabCoef2[0])
        borneSup = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef2[1]), tabCoef1[1]), tabCoef2[0])
        if (j3pCalculValeur(tabCoef2[0]) < 0) {
          a = borneInf
          borneInf = borneSup
          borneSup = a
        }
        if ((leSigne === '<') || (leSigne === '\\leq')) {
          if (stor.type_nbconstant[me.questionCourante - 1] === 'negatif') {
            // L’ensemble des solutions est \\R' car une valeur absolue est toujours positive
            stor.tab_sol.push(']-\\infty;+\\infty[')
            stor.tab_solapproche.push(']-\\infty;+\\infty[')
            stor.tab_complement.push('\varnothing')
            stor.tab_complementapproche.push('\\varnothing')
          } else if (stor.type_nbconstant[me.questionCourante - 1] === 'nul') {
            if (leSigne === '\\leq') {
              // L’ensemble des solutions est \\R' car une valeur absolue est toujours positive
              stor.tab_sol.push(']-\\infty;+\\infty[')
              stor.tab_solapproche.push(']-\\infty;+\\infty[')
              stor.tab_complement.push('\\varnothing')
              stor.tab_complementapproche.push('\\varnothing')
            } else {
              stor.tab_sol.push(']-\\infty;' + borneInf + '[')
              stor.tab_sol.push(']' + borneSup + ';+\\infty[')
              stor.tab_solapproche.push(']-\\infty;' + j3pArrondi(j3pCalculValeur(borneInf), 10) + '[')
              stor.tab_solapproche.push(']' + j3pArrondi(j3pCalculValeur(borneSup), 10) + ';+\\infty[')
              stor.tab_complement.push('{' + borneInf + '}')
              stor.tab_complementapproche.push(j3pArrondi(j3pCalculValeur(borneInf), 10))
            }
          } else {
            // L’ensemble des solutions est alors une réunion de deux intervalles
            stor.tab_sol.push(']-\\infty;' + borneInf)
            stor.tab_sol.push(borneSup + ';+\\infty[')
            stor.tab_solapproche.push(']-\\infty;' + j3pArrondi(j3pCalculValeur(borneInf), 10))
            stor.tab_solapproche.push(j3pArrondi(j3pCalculValeur(borneSup), 10) + ';+\\infty[')
            if (leSigne === '\\leq') {
              stor.tab_sol[0] += ']'
              stor.tab_solapproche[0] += ']'
              stor.tab_sol[1] = '[' + stor.tab_sol[1]
              stor.tab_solapproche[1] = '[' + stor.tab_solapproche[1]
            } else {
              stor.tab_sol[0] += '['
              stor.tab_solapproche[0] += '['
              stor.tab_sol[1] = ']' + stor.tab_sol[1]
              stor.tab_solapproche[1] = ']' + stor.tab_solapproche[1]
            }
            stor.tab_complement.push(borneInf + ';' + borneSup)
            stor.tab_complementapproche.push(j3pArrondi(j3pCalculValeur(borneInf), 10) + ';' + j3pArrondi(j3pCalculValeur(borneSup), 10))
          }
        } else {
          // ((leSigne === ">") || (leSigne === "\\geq"))
          if (stor.type_nbconstant[me.questionCourante - 1] === 'negatif') {
            // Une valeur absolue ne pouvant être négative, l’inéquation n’admet pas de solution
            stor.tab_sol.push('\\varnothing')
            stor.tab_solapproche.push('\\varnothing')
            stor.tab_complement.push('-\\infty;+\\infty')
            stor.tab_complementapproche.push('-\\infty;+\\infty')
          } else if (stor.type_nbconstant[me.questionCourante - 1] === 'nul') {
            if (leSigne === '\\geq') {
              // il n’y a qu’une seule solution (finalement c’est comme si on résolvait l’équation)
              // Dans ce cas borneInf et borneSup sont égale
              stor.tab_sol.push('{' + borneInf + '}')
              stor.tab_solapproche.push('{' + j3pArrondi(j3pCalculValeur(borneInf), 10) + '}')
              stor.tab_complement.push('-\\infty;' + borneInf, borneSup + ';+\\infty')
              stor.tab_complementapproche.push('-\\infty;' + j3pArrondi(j3pCalculValeur(borneInf), 10), j3pArrondi(j3pCalculValeur(borneSup), 10) + ';+\\infty')
            } else {
              // pas de solution
              stor.tab_sol.push('\\varnothing')
              stor.tab_solapproche.push('\\varnothing')
              stor.tab_complement.push('-\\infty;+\\infty')
              stor.tab_complementapproche.push('-\\infty;+\\infty')
            }
          } else {
            // L’ensemble des solutions est un seul intervalle'
            stor.tab_sol.push(borneInf + ';' + borneSup)
            stor.tab_solapproche.push(j3pArrondi(j3pCalculValeur(borneInf), 10) + ';' + j3pArrondi(j3pCalculValeur(borneSup), 10))
            if (leSigne === '\\geq') {
              stor.tab_sol[0] = '[' + stor.tab_sol[0] + ']'
              stor.tab_solapproche[0] = '[' + stor.tab_solapproche[0] + ']'
            } else {
              stor.tab_sol[0] = ']' + stor.tab_sol[0] + '['
              stor.tab_solapproche[0] = ']' + stor.tab_solapproche[0] + '['
            }
            stor.tab_complement.push('-\\infty;' + borneInf, borneSup + ';+\\infty')
            stor.tab_complementapproche.push('-\\infty;' + j3pArrondi(j3pCalculValeur(borneInf), 10), j3pArrondi(j3pCalculValeur(borneSup), 10) + ';+\\infty')
          }
        }
      } else {
        // |ax+b|<k
        borneInf = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef1[1]), j3pGetLatexOppose(tabCoef2[1])), tabCoef1[0])
        borneSup = j3pGetLatexQuotient(j3pGetLatexSomme(j3pGetLatexOppose(tabCoef1[1]), tabCoef2[1]), tabCoef1[0])
        if (j3pCalculValeur(tabCoef1[0]) < 0) {
          a = borneInf
          borneInf = borneSup
          borneSup = a
        }
        if ((leSigne === '<') || (leSigne === '\\leq')) {
          if (stor.type_nbconstant[me.questionCourante - 1] === 'negatif') {
            // Une valeur absolue ne pouvant être négative, l’inéquation n’admet pas de solution
            stor.tab_sol.push('\\varnothing')
            stor.tab_solapproche.push('\\varnothing')
            stor.tab_complement.push('-\\infty;+\\infty')
            stor.tab_complementapproche.push('-\\infty;+\\infty')
          } else if (stor.type_nbconstant[me.questionCourante - 1] === 'nul') {
            if (leSigne === '\\leq') {
              // il n’y a qu’une seule solution (finalement c’est comme si on résolvait l’équation)
              // Dans ce cas borneInf et borneSup sont égale
              stor.tab_sol.push('{' + borneInf + '}')
              stor.tab_solapproche.push('{' + j3pArrondi(j3pCalculValeur(borneInf), 10) + '}')
              stor.tab_complement.push('-\\infty;' + borneInf, borneSup + ';+\\infty')
              stor.tab_complementapproche.push('-\\infty;' + j3pArrondi(j3pCalculValeur(borneInf), 10), j3pArrondi(j3pCalculValeur(borneSup), 10) + ';+\\infty')
            } else {
              // pas de solution
              stor.tab_sol.push('\\varnothing')
              stor.tab_solapproche.push('\\varnothing')
              stor.tab_complement.push('-\\infty;+\\infty')
              stor.tab_complementapproche.push('-\\infty;+\\infty')
            }
          } else {
            // L’ensemble des solutions est un seul intervalle'
            stor.tab_sol.push(borneInf + ';' + borneSup)
            stor.tab_solapproche.push(j3pArrondi(j3pCalculValeur(borneInf), 10) + ';' + j3pArrondi(j3pCalculValeur(borneSup), 10))
            if (leSigne === '\\leq') {
              stor.tab_sol[0] = '[' + stor.tab_sol[0] + ']'
              stor.tab_solapproche[0] = '[' + stor.tab_solapproche[0] + ']'
            } else {
              stor.tab_sol[0] = ']' + stor.tab_sol[0] + '['
              stor.tab_solapproche[0] = ']' + stor.tab_solapproche[0] + '['
            }
            stor.tab_complement.push('-\\infty;' + borneInf, borneSup + ';+\\infty')
            stor.tab_complementapproche.push('-\\infty;' + j3pArrondi(j3pCalculValeur(borneInf), 10), j3pArrondi(j3pCalculValeur(borneSup), 10) + ';+\\infty')
          }
        } else {
          if (stor.type_nbconstant[me.questionCourante - 1] === 'negatif') {
            // L’ensemble des solutions est \\R' car une valeur absolue est toujours positive
            stor.tab_sol.push(']-\\infty;+\\infty[')
            stor.tab_solapproche.push(']-\\infty;+\\infty[')
            stor.tab_complement.push('\\varnothing')
            stor.tab_complementapproche.push('\\varnothing')
          } else if (stor.type_nbconstant[me.questionCourante - 1] === 'nul') {
            if (leSigne === '\\geq') {
              // L’ensemble des solutions est \\R' car une valeur absolue est toujours positive
              stor.tab_sol.push(']-\\infty;+\\infty[')
              stor.tab_solapproche.push(']-\\infty;+\\infty[')
              stor.tab_complement.push('\\varnothing')
              stor.tab_complementapproche.push('\\varnothing')
            } else {
              stor.tab_sol.push(']-\\infty;' + borneInf + '[')
              stor.tab_sol.push(']' + borneSup + ';+\\infty[')
              stor.tab_solapproche.push(']-\\infty;' + j3pArrondi(j3pCalculValeur(borneInf), 10) + '[')
              stor.tab_solapproche.push(']' + j3pArrondi(j3pCalculValeur(borneSup), 10) + ';+\\infty[')
              stor.tab_complement.push(borneInf + ';' + borneSup)
              stor.tab_complementapproche.push(j3pArrondi(j3pCalculValeur(borneInf), 10) + ';' + j3pArrondi(j3pCalculValeur(borneSup), 10))
            }
          } else {
            // L’ensemble des solutions est alors une réunion de deux intervalles
            stor.tab_sol.push(']-\\infty;' + borneInf)
            stor.tab_sol.push(borneSup + ';+\\infty[')
            stor.tab_solapproche.push(']-\\infty;' + j3pArrondi(j3pCalculValeur(borneInf), 10))
            stor.tab_solapproche.push(j3pArrondi(j3pCalculValeur(borneSup), 10) + ';+\\infty[')
            if (leSigne === '\\geq') {
              stor.tab_sol[0] += ']'
              stor.tab_solapproche[0] += ']'
              stor.tab_sol[1] = '[' + stor.tab_sol[1]
              stor.tab_solapproche[1] = '[' + stor.tab_solapproche[1]
            } else {
              stor.tab_sol[0] += '['
              stor.tab_solapproche[0] += '['
              stor.tab_sol[1] = ']' + stor.tab_sol[1]
              stor.tab_solapproche[1] = ']' + stor.tab_solapproche[1]
            }
            stor.tab_complement.push(borneInf + ';' + borneSup)
            stor.tab_complementapproche.push(j3pArrondi(j3pCalculValeur(borneInf), 10) + ';' + j3pArrondi(j3pCalculValeur(borneSup), 10))
          }
        }
      }
      me.logIfDebug('stor.tab_sol:' + stor.tab_sol + '    stor.tab_complement:' + stor.tab_complement)
    }

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les zones 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
    // Donc la validationd e cette zone devra se faire "à la main".
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })
    j3pFocus(stor.zoneInput, true)

    // je prépare les divs de la correction : c’est pour y construire la figure mtg32 qui ne sera pas affichée dans un premier temps
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px' } })
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(stor.zoneExpli, 'div')
    const divMtg32 = j3pAddElt(stor.zoneExpli3, 'div')
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(divMtg32, { id: stor.mtg32svg, width: 550, height: 77 })
    let casFigure = 1// équation |ax+b|=k
    if ((ds.type_exo === 'equation') && (ds.type_eq[me.questionCourante - 1] === 2)) {
      casFigure = 2
    } else if (ds.type_exo === 'inequation') {
      casFigure = 3
    }
    // ce qui suit lance la création initiale de la figure
    depart({ num: casFigure })

    // Obligatoire
    me.finEnonce()
  }
  let tabSigne, k, i, a, borneInf, borneSup, j
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })
        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        if (ds.type_exo === 'inequation') {
          me.afficheTitre(textes.titre_exobis)
        } else {
          me.afficheTitre(textes.titre_exo)
        }

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        // je vérifie si le tableau type_exo est bien rempli (seulement pour une équation)
        ds.type_exo = ((ds.type_exo === 'inequation') || (ds.type_exo === 'inéquation')) ? 'inequation' : 'equation'

        for (i = 0; i < ds.repetitions; i++) {
          if (ds.type_exo === 'equation') {
            ds.type_eq[i] = ((String(ds.type_eq[i]) === '2') || (String(ds.type_eq[i]) === 2)) ? 2 : 1
          } else {
            ds.type_eq[i] = 1
          }
        }

        // pour chaque répétition, on fait attention si coef1_devantx est bien rempli par des booleens
        stor.coef1_devantx = []
        // Pour chaque répétition, on vérifie si la fonction est imposée ou non
        stor.membreGauche = [] // ce tableau regroupe les membres de gauche des (in)égalités imposées, "" pour celles qui ne le sont pas ou son mal écrites
        stor.membreDroite = [] // ce tableau regroupe les membres de droite des (in)égalités imposées, "" pour celles qui ne le sont pas ou son mal écrites
        stor.signe_eq_ineq = [] // ce tableau regroupe les différents signes d'(in)égalité lors qu’elles sont imposées
        stor.coefsmembreGauche = []
        stor.coefsmembreDroite = []
        stor.type_nbconstant = []// ce tableau de booléen sert à prendre en compte le cas où un membre est négatif
        let tabIntervalle
        for (i = 0; i < ds.nbrepetitions; i++) {
          stor.coef1_devantx[i] = typeof ds.coef1_devantx[i] === 'boolean'
            ? ds.coef1_devantx[i]
            : ds.coef1_devantx[i] !== 'false'
          stor.membreGauche.push('')
          stor.membreDroite.push('')
          stor.coefsmembreGauche.push([])
          stor.coefsmembreDroite.push([])
          stor.signe_eq_ineq.push('')
          stor.type_nbconstant.push('positif')
          if (ds.imposer_eq_ineq[i] !== undefined) {
            tabSigne = ['=']
            if (ds.type_exo === 'inequation') {
              tabSigne = ['<=', '>=', '<', '>']
            }
            const intervalleReg = /\[-?[0-9]+;-?[0-9]+]/gi
            if (ds.imposer_eq_ineq[i] !== '') {
              // l'(in)égalité semble avoir été imposée. Je vais vérifier si elle est comprise
              const egalite = ds.imposer_eq_ineq[i]// juste pour ne pas tout écrire à chaque fois
              for (j = 0; j < tabSigne.length; j++) {
                const positionSigne = egalite.indexOf(tabSigne[j])
                if (positionSigne > -1) {
                  // j’ai trouvé le signe d’égalité ou d’inégalité
                  // je regarde alors le membre de gauche et celui de droite
                  let membreGauche = egalite.substring(0, positionSigne)
                  let membreDroite = egalite.substring(positionSigne + tabSigne[j].length)
                  // l’un des membres peut contenir un intervalle
                  while (intervalleReg.test(membreGauche)) {
                    tabIntervalle = membreGauche.match(intervalleReg)
                    for (k = 0; k < tabIntervalle.length; k++) {
                      const [lemin, lemax] = j3pGetBornesIntervalle(tabIntervalle[k])
                      membreGauche = membreGauche.replace(tabIntervalle[k], j3pGetRandomInt(lemin, lemax))
                    }
                  }
                  while (intervalleReg.test(membreDroite)) {
                    tabIntervalle = membreDroite.match(intervalleReg)
                    for (k = 0; k < tabIntervalle.length; k++) {
                      const [lemin, lemax] = j3pGetBornesIntervalle(tabIntervalle[k])
                      membreDroite = membreDroite.replace(tabIntervalle[k], j3pGetRandomInt(lemin, lemax))
                    }
                  }
                  const tabCoefsGauche = extraireCoefsFctAffine(membreGauche, 'x')
                  const tabCoefsDroite = extraireCoefsFctAffine(membreDroite, 'x')
                  const membreGaucheOK = (!isNaN(j3pCalculValeur(tabCoefsGauche[0])) && !isNaN(j3pCalculValeur(tabCoefsGauche[1])))
                  const membreDroiteOK = (!isNaN(j3pCalculValeur(tabCoefsDroite[0])) && !isNaN(j3pCalculValeur(tabCoefsDroite[1])))
                  tabCoefsGauche[0] = decimalFrac(tabCoefsGauche[0])
                  tabCoefsGauche[1] = decimalFrac(tabCoefsGauche[1])
                  tabCoefsDroite[0] = decimalFrac(tabCoefsDroite[0])
                  tabCoefsDroite[1] = decimalFrac(tabCoefsDroite[1])
                  me.logIfDebug('tabCoefsGauche:' + tabCoefsGauche + '   tabCoefsDroite:' + tabCoefsDroite)
                  let eq2EgaliteCoefsDevantx = true
                  if (membreGaucheOK && membreDroiteOK && (tabCoefsGauche[0] !== '0') && (tabCoefsDroite[0] !== '0')) {
                    // on a une équation de la forme |ax+b|=|cx+d|
                    if (((membreGauche.charAt(membreGauche.length - 1) === 'x') && (membreDroite.charAt(membreDroite.length - 1) === 'x')) || ((membreGauche.charAt(membreGauche.length - 1) !== 'x') && (membreDroite.charAt(membreDroite.length - 1) !== 'x'))) {
                      // si les membres sont tous les deux écrits ax+b ou tous les deux b+ax, alors on n’accepte que les cas où les coefs a et c sont égaux
                      eq2EgaliteCoefsDevantx = (tabCoefsGauche[0] === tabCoefsDroite[0])
                    } else {
                      // si les membres sont pour l’un de la forme ax+b et l’autre d+cx, là on n’accepte que les cas où les nombres a et c sont opposés
                      eq2EgaliteCoefsDevantx = (tabCoefsGauche[0] === j3pGetLatexOppose(tabCoefsDroite[0]))
                    }
                  }
                  if (!membreGaucheOK || !membreDroiteOK) {
                    j3pShowError('je ne comprends pas l’(in)équation proposée', { vanishAfter: 5 })
                    ds.imposer_eq_ineq[i] = ''
                  } else {
                    if ((tabSigne[j] !== '=') && (tabCoefsGauche[0] !== '0') && (tabCoefsDroite[0] !== '0')) {
                      // on propose une inéquation avec un membre non constant, ce que je n’ai pas prévu
                      j3pShowError('ce cas de figure n’est pas pris en compte dans l’exercice', { vanishAfter: 5 })
                      ds.imposer_eq_ineq[i] = ''
                    } else if (!eq2EgaliteCoefsDevantx) {
                      // En d’autres termes, j’ai une égalité |ax+b|=|cx+d| avec a et c différents, ce qui n’est pas non plus prévu
                      j3pShowError('Dans le cas où type_eq vaut 2, les deux coefficients devant "x" doivent être égaux', { vanishAfter: 5 })
                    } else {
                      // je fais tout de même attention qu’il n’y a pas d’erreur de type_eq avec l’équation imposée
                      if (ds.type_exo === 'equation') {
                        if ((tabCoefsGauche[0] !== '0') && (tabCoefsDroite[0] !== '0')) {
                          // |ax+b|=|cx+d|
                          ds.type_eq[i] = 2
                        } else {
                          // |ax+b|=k
                          ds.type_eq[i] = 1
                        }
                      }
                      if ((String(tabCoefsGauche[0]) === '0') || (String(tabCoefsDroite[0]) === '0')) {
                        // le réel k doit être positif
                        if (((String(tabCoefsGauche[0]) === '0') && (j3pCalculValeur(tabCoefsGauche[1]) <= 0)) || ((String(tabCoefsDroite[0]) === '0') && (j3pCalculValeur(tabCoefsDroite[1]) <= 0))) {
                          if (String(tabCoefsGauche[0]) === '0') {
                            // c’est le membre de gauche qui est constant inférieur ou égal à 0
                            if (Math.abs(j3pCalculValeur(tabCoefsGauche[1])) < Math.pow(10, -12)) {
                              // c’est que ce membre est nul
                              stor.type_nbconstant[i] = 'nul'
                            } else {
                              stor.type_nbconstant[i] = 'negatif'
                            }
                          } else {
                            // c’est le mebre de droite qui est constant inférieur ou égal à 0
                            if (Math.abs(j3pCalculValeur(tabCoefsDroite[1])) < Math.pow(10, -12)) {
                              // c’est que ce membre est nul
                              stor.type_nbconstant[i] = 'nul'
                            } else {
                              stor.type_nbconstant[i] = 'negatif'
                            }
                          }
                        }
                      }
                      stor.membreGauche[i] = membreGauche
                      stor.membreDroite[i] = membreDroite
                      stor.coefsmembreGauche[i] = tabCoefsGauche
                      stor.coefsmembreDroite[i] = tabCoefsDroite
                      if (tabSigne[j] === '<=') {
                        stor.signe_eq_ineq[i] = '\\leq'
                      } else if (tabSigne[j] === '>=') {
                        stor.signe_eq_ineq[i] = '\\geq'
                      } else {
                        stor.signe_eq_ineq[i] = tabSigne[j]
                      }
                    }
                  }
                  j = tabSigne.length
                }
              }
            }
          } else {
            ds.imposer_eq_ineq[i] = ''
          }
        }
        // chargement mtg
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = { aRepondu: false, bonneReponse: false }
        reponse.aRepondu = fctsValid.valideReponses()
        let pos = 0
        let bonneRep1
        let intervalleSansCrochetsTrouve, uneBonneReponseSurDeux, bonNbSol, bonneRep2
        let intersection, bonneReponseSansCrochets, reponseNonComprise, reponseSimplifiee, lesIntervalles, reponseComplement
        if (reponse.aRepondu) {
        // je dois récupérer le contenu de la zone
          if (ds.type_exo === 'equation') {
            const tabSolutions = fctsValid.zones.reponseSaisie[0].split(';')
            while (pos < tabSolutions.length) {
              if (tabSolutions[pos] === '') {
                tabSolutions.splice(pos, 1)
              } else {
                pos += 1
              }
            }
            if (stor.tab_valsol.length === 0) {
              reponse.bonneReponse = ((tabSolutions.length === 1) && (tabSolutions[0] === '\\varnothing'))
            } else {
            // J’ai récupéré les seules solutions proposées qui sont exploitables
              const tabReponsesCorrectes = []
              const mesSolutions = [...stor.tab_valsol]
              // mesSolutions est une copie du tableau stor.tab_valsol. LA différence, c’est que le premer sera modifié (en fonction des solutions trouvées) et pas le deuxième
              me.logIfDebug('mesSolutions:' + mesSolutions + '   les solutions proposées:' + tabSolutions)
              for (i = 0; i < tabSolutions.length; i++) {
                tabReponsesCorrectes.push(false)
              }
              bonneRep2 = true
              for (i = 0; i < tabSolutions.length; i++) {
                j = 0
                while (j < mesSolutions.length) {
                  if (Math.abs(j3pCalculValeur(tabSolutions[i]) - mesSolutions[j]) < Math.pow(10, -12)) {
                    tabReponsesCorrectes[i] = true
                    mesSolutions.splice(j, 1)
                  } else {
                    j += 1
                  }
                }
                bonneRep2 = (bonneRep2 && tabReponsesCorrectes[i])
              }
              // A cet instant, je sais si toutes les solutions ont été trouvée : tabReponsesCorrectes ne contient que des true et mesSolutions est vide
              bonneRep1 = (mesSolutions.length === 0)
              reponse.bonneReponse = (bonneRep1 && bonneRep2)
              me.logIfDebug('bonneRep1:' + bonneRep1 + '   bonneRep2:' + bonneRep2 + '   réponses correctes :' + tabReponsesCorrectes)
              bonNbSol = (tabSolutions.length === stor.tab_valsol.length)
              uneBonneReponseSurDeux = false
              // console.log("stor.tab_valsol:"+stor.tab_valsol)

              if (stor.tab_valsol.length > 1) {
              // peut-être y a-t-il une bonne réponse parmi les réponses proposées
                if (!reponse.bonneReponse) {
                  for (i = 0; i < tabReponsesCorrectes.length; i++) {
                    uneBonneReponseSurDeux = (uneBonneReponseSurDeux || tabReponsesCorrectes[i])
                  }
                }
              }
            }
          } else {
          // début de la gestion de la réponse dans le cas d’une inéquation
            let reponseEleve = fctsValid.zones.reponseSaisie[0]
            // première étape : je convertis les virgules des nombres décimaux en points
            while (reponseEleve.indexOf(',') > -1) {
              reponseEleve = reponseEleve.replace(',', '.')
            }
            // console.log("avant reponseEleve: "+reponseEleve+"   avec remlacement :"+reponseEleve.replace("\\mathbb{R}",""))
            // ayant autorisé le bouton \\R, il faut que je gère une réponse de la forme \\R-{...}Je l’écris alors comme une réunion d’intervalles
            if (reponseEleve.indexOf('\\mathbb{R}-') === 0) {
            // c’est que l’élève a écrit un ensemble de la forme \\R-{...}
              const posAccDebut = reponseEleve.replace('\\mathbb{R}', '').indexOf('{') + 10
              const posAccFin = reponseEleve.replace('\\mathbb{R}', '').lastIndexOf('}') + 10
              const valeurTab = reponseEleve.substring(posAccDebut + 1, posAccFin).split(';')
              let newRepEleve = ']-\\infty;' + valeurTab[0] + '[\\cup'
              while (pos < valeurTab.length) {
                newRepEleve += ']' + valeurTab[pos - 1] + ';' + valeurTab[pos] + '[\\cup'
                pos += 1
              }
              newRepEleve += ']' + valeurTab[pos - 1] + ';+\\infty['
              reponseEleve = newRepEleve
            }
            // par ailleurs l’ensemble \\R n’est pas compris par la fonction simplifieUnionIntersection
            // j'écris donc à la place ]-\\infty;+\\infty[
            reponseEleve = reponseEleve.replace('\\mathbb{R}', ']-\\infty;+\\infty[')
            const fracReg = /[-+]?\\frac\{-?[0-9]+}\{[0-9]+}/g // new RegExp('(\\-|\\+)?\\\\frac\\{\\-?[0-9]{1,}\\}\\{[0-9]{1,}\\}', 'ig')
            let tabFrac = reponseEleve.match(fracReg)
            while (tabFrac) {
              const valeurFrac = j3pArrondi(j3pCalculValeur(tabFrac[0]), 10)
              reponseEleve = reponseEleve.replace(tabFrac[0], String(valeurFrac))
              tabFrac = reponseEleve.match(fracReg)
            }
            me.logIfDebug('reponseEleve:' + reponseEleve)

            const solReduite = simplifieUnionIntersection(reponseEleve)
            const intervalleTrouve = []
            for (i = 0; i < stor.tab_solapproche.length; i++) {
              intervalleTrouve.push(false)
            }
            intersection = false
            bonneReponseSansCrochets = false

            // stock.valide=true si l’écriture est valide
            // stock.valide=false si l’écriture n’est pas valide
            // Dans ce cas, on a stock.erreur qui est une string qui renvoie le type d’erreur
            // stock.erreur="illisible" si rien n’est interprétable
            // stock.erreur="inversion_bornes" si l’intervalle est écrit de façon inversée
            // stock.erreur=infini_inclus si l’une des bornes infinies est incluse
            // stock.ecriture renvoie l’intervalle pour que la fonction résultat puisse fonctionner

            // console.log(solReduite.valide+"   "+solReduite.erreur+"   "+solReduite.ecriture)
            // console.log(solReduite+"    solReduite.affichage:"+solReduite.affichage+"   resultat:"+solReduite.resultat)
            // il reste un pb à gérer :
            // si on donne l’ensemble {... ; ...}, il n’est aps compris par la fonction simplifieUnionIntersection
            let ensembleAvecAccolades = false
            if ((reponseEleve.charAt(0) === '{') && (reponseEleve.charAt(reponseEleve.length - 1) === '}')) {
              ensembleAvecAccolades = true
              solReduite.valide = true
              // je vérifie tout de même qu’après ce sont bien des nombres qui sont dans l’accolade
              const lesValeursTab = reponseEleve.substring(1, reponseEleve.length - 1).split(';')
              for (i = 0; i < lesValeursTab.length; i++) {
                if (isNaN(j3pCalculValeur(lesValeursTab[i]))) solReduite.valide = false
              }
            }
            if (!solReduite.simplifie) solReduite.simplifie = true
            reponseNonComprise = false
            reponseSimplifiee = true
            if (solReduite.valide) {
              if (!solReduite.simplifie) {
                reponseSimplifiee = false
                reponse.aRepondu = false
              } else {
                if (solReduite.affichage === '\\mathbf{R}') {
                  solReduite.affichage = ']-\\infty;+\\infty['
                }
                if (!ensembleAvecAccolades) {
                  lesIntervalles = solReduite.affichage.split('\\cup')
                }
                me.logIfDebug('stor.tab_solapproche:' + stor.tab_solapproche + '    lesIntervalles:' + lesIntervalles + '   solReduite.affichage:' + solReduite.affichage)
                if (fctsValid.zones.reponseSaisie[0].indexOf('cap') > -1) {
                  intersection = true
                } else if (ensembleAvecAccolades) {
                // l’ensemble donné est de la forme {...}
                // Le seul cas de figure est le cas d’un singleton
                  reponse.bonneReponse = (stor.tab_solapproche[0] === reponseEleve)
                } else if (solReduite.affichage.indexOf('cup') > -1) {
                // on a une réunion d’intervalles
                  bonneRep1 = (lesIntervalles.length === stor.tab_solapproche.length)
                  bonneRep2 = true
                  if (bonneRep1) {
                    for (i = 0; i < stor.tab_solapproche.length; i++) {
                      for (j = 0; j < lesIntervalles.length; j++) {
                        if (stor.tab_solapproche[i] === lesIntervalles[j]) {
                          intervalleTrouve[i] = true// le i-ème intervalle solution est dans la réponse donnée
                        }
                      }
                      bonneRep2 = (bonneRep2 && intervalleTrouve[i])
                    }
                  }
                  // console.log("bonneRep1:"+bonneRep1+"   bonneRep2:"+bonneRep2+"   intervalleTrouve:"+intervalleTrouve)

                  reponse.bonneReponse = (bonneRep1 && bonneRep2)
                  if (!bonneRep2 && bonneRep1) {
                  // on vérifie si ce n’est pas qu’un pb de crochets
                    bonneReponseSansCrochets = true
                    intervalleSansCrochetsTrouve = []
                    for (i = 0; i < stor.tab_solapproche.length; i++) {
                      intervalleSansCrochetsTrouve.push(false)
                    }
                    for (i = 0; i < stor.tab_solapproche.length; i++) {
                      for (j = 0; j < lesIntervalles.length; j++) {
                      // console.log("enleveCrochetsIntervalle(stor.tab_solapproche[i]:"+enleveCrochetsIntervalle(stor.tab_solapproche[i])+"   enleveCrochetsIntervalle(lesIntervalles[j]):"+enleveCrochetsIntervalle(lesIntervalles[j]))

                        if (enleveCrochetsIntervalle(stor.tab_solapproche[i]) === enleveCrochetsIntervalle(lesIntervalles[j])) {
                          intervalleSansCrochetsTrouve[i] = true// le i-ème intervalle solution est dans la réponse donnée
                        }
                      }
                      bonneReponseSansCrochets = (bonneReponseSansCrochets && intervalleSansCrochetsTrouve[i])
                    }
                  }
                } else {
                // console.log("non union stor.tab_solapproche:"+stor.tab_solapproche+"    lesIntervalles:"+lesIntervalles)
                // on ne devrait avoir qu’un seul intervalle
                  bonneRep1 = (stor.tab_solapproche.length === 1)
                  bonneRep2 = true
                  if (bonneRep1) {
                    if (stor.tab_solapproche[0] === lesIntervalles[0]) {
                      intervalleTrouve[0] = true// le i-ème intervalle solution est dans la réponse donnée
                    }
                    bonneRep2 = (bonneRep2 && intervalleTrouve[0])
                  }
                  reponse.bonneReponse = (bonneRep1 && bonneRep2)
                  if (bonneRep1 && !bonneRep2 && (reponseEleve !== '\\varnothing')) {
                    bonneReponseSansCrochets = (enleveCrochetsIntervalle(stor.tab_solapproche[0]) === enleveCrochetsIntervalle(lesIntervalles[0]))
                    if (solReduite.affichage.charAt(solReduite.affichage.length - 2) === '}') {
                    // C’est qu’on a un singleton ou R privé d’un nb
                      bonneReponseSansCrochets = false
                    }
                  }
                }
                // console.log("intervalleTrouve : "+intervalleTrouve+"   bonneRep2:"+bonneRep2)

                reponseComplement = false
                if (!reponse.bonneReponse && !bonneReponseSansCrochets && (fctsValid.zones.reponseSaisie[0].indexOf('cap') === -1)) {
                // soit la réponse n’est pas bonne et on n’a pas juste un pb de crochets
                // l’élève peut s'être trompé dans l’inégalité
                  intervalleSansCrochetsTrouve = []
                  reponseComplement = true
                  for (i = 0; i < stor.tab_complementapproche.length; i++) {
                    intervalleSansCrochetsTrouve.push(false)
                  }

                  // console.log("stor.tab_complementapproche:"+stor.tab_complementapproche+"   lesIntervalles:"+lesIntervalles)

                  for (i = 0; i < stor.tab_complementapproche.length; i++) {
                    for (j = 0; j < lesIntervalles.length; j++) {
                    // console.log("stor.tab_complementapproche[i]:"+stor.tab_complementapproche[i]+"     enleveCrochetsIntervalle(lesIntervalles[j]):"+enleveCrochetsIntervalle(lesIntervalles[j]))
                      if (stor.tab_complementapproche[i] === enleveCrochetsIntervalle(lesIntervalles[j])) {
                        intervalleSansCrochetsTrouve[i] = true// le i-ème intervalle solution est dans la réponse donnée
                      }
                    }
                    reponseComplement = (reponseComplement && intervalleSansCrochetsTrouve[i])
                  }
                }
                me.logIfDebug('solReduite:' + solReduite.affichage + '   ' + solReduite.resultat)
              }
            } else {
              reponseNonComprise = true
              reponse.bonneReponse = false
            }
          }
          fctsValid.zones.bonneReponse[0] = (reponse.bonneReponse)
          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          fctsValid.coloreUneZone(stor.zoneInput.id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // On peut ajouter un commentaire particulier.
          let msgReponseManquante
          if (!reponseSimplifiee) {
          // l’élève a bien répondu mais on lui demande de simplifier ce qu’il écrit
            msgReponseManquante = textes.comment10
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (ds.type_exo === 'equation') {
                if (!bonNbSol) stor.zoneCorr.innerHTML += '<br>' + textes.comment2
                if (uneBonneReponseSurDeux) stor.zoneCorr.innerHTML += '<br>' + textes.comment1
              } else {
                if (intersection) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment3
                } else if (bonneReponseSansCrochets) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment4
                } else if (reponseComplement) {
                // l’élève a donné les intervalles complémentaires de la réponse attendue (je n’ai pas pris en compte les crochets ici)
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment5
                } else if (reponseNonComprise) {
                  stor.zoneCorr.innerHTML += '<br>' + textes.comment6
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore

                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
