import { j3pAddElt, j3pFocus, j3pGetRandomInt, j3pGetNewId, j3pMathquillXcas, j3pNombre, j3pPaletteMathquill, j3pPGCD, j3pShuffle, j3pShuffleMulti, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pExtraireNumDen, j3pGetLatexProduit } from 'src/legacy/core/functionsLatex'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        juillet 2019
        encadrement de f(x) où f peut être la fonction carrée ou la fonction inverse
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeFonction', 'carree', 'liste', 'Choix de la fonction de référence utilisée dans l’inéquation (carree par défaut)', ['carree', 'inverse', 'racine carree', 'cube']]
  ]
}

/**
 * section encadreFctRef
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = (ds.typeFonction === 'carree')
      ? 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV8AAADnAAAAQEAAAAAAAAAAQAAADf#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAFAAFAY2AAAAAAAEB0MKPXCj1x#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAAA4AAUkAwBgAAAAAAAAAAAAAAAAAAAUAAUBHGdsi0OVgAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAAEAAAAQABAAAAAQAAAAP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AAAAAAAQAAABAAEAAAABAAAABP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQUAAQAAAAcAAAAJAP####8BAAAAAA4AAUoAwCgAAAAAAADAEAAAAAAAAAUAAgAAAAf#####AAAAAgAHQ1JlcGVyZQD#####AObm5gABAAAAAQAAAAMAAAAJAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEBAAAAAAMAAAAM#####wAAAAEACUNMb25ndWV1cgD#####AAAAAQAAAA3#####AAAAAQAFQ0ZvbmMA#####wABZgADeF4y#####wAAAAEACkNQdWlzc2FuY2X#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAAAUAAAAAAAAAAAAF4AAAABAD#####AQAAAAAQAAF4AAAAAAAAAAAAQAgAAAAAAAAFAAFAI3O4UIrTxQAAAAT#####AAAAAgAIQ01lc3VyZVgA#####wACeDEAAAAKAAAAEP####8AAAABAAdDQ2FsY3VsAP####8AAnkxAAVmKHgxKf####8AAAABAA5DQXBwZWxGb25jdGlvbgAAAA8AAAAOAAAAEf####8AAAABABBDUG9pbnREYW5zUmVwZXJlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAADgAAABEAAAAOAAAAEv####8AAAACAA1DTGlldURlUG9pbnRzAP####8AAAAAAAEAAAATAAAB9AABAAAAEAAAAAQAAAAQAAAAEQAAABIAAAATAAAAFQD#####AARhYnMxAAQtMC44#####wAAAAEADENNb2luc1VuYWlyZQAAAAE#6ZmZmZmZmgAAABUA#####wAEYWJzMgAELTEuNQAAABkAAAABP#gAAAAAAAAAAAAVAP####8ABG9yZDEAB2YoYWJzMSkAAAAWAAAADwAAAA4AAAAVAAAAFQD#####AARvcmQyAAdmKGFiczIpAAAAFgAAAA8AAAAOAAAAFgAAABcA#####wEAAAAAEAACQTEAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAA4AAAAVAAAAAQAAAAAAAAAAAAAAFwD#####AQAAAAAQAAJBMgAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAADgAAABUAAAAOAAAAFwAAABcA#####wEAAAAAEAACQTMAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAEAAAAAAAAAAAAAAA4AAAAXAAAAFwD#####AQAAAAAQAAJCMQAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAADgAAABYAAAABAAAAAAAAAAAAAAAXAP####8BAAAAABAAAkIyAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAAOAAAAFgAAAA4AAAAYAAAAFwD#####AQAAAAAQAAJCMwAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAQAAAAAAAAAAAAAADgAAABj#####AAAAAQAIQ1NlZ21lbnQA#####wAAAP8AEAAAAQECAAAAHgAAAB0AAAAaAP####8AAAD#ABAAAAEBAgAAAB0AAAAcAAAAGgD#####AAAA#wAQAAABAQIAAAAZAAAAGgAAABoA#####wAAAP8AEAAAAQECAAAAGgAAABsAAAAaAP####8AAH8AABAAAAEAAwAAABkAAAAcAAAABAD#####AQAAAAAQAAJ4MQAAAAAAAAAAAEAIAAAAAAAABQABAAAAAAAAAAAAAAAjAAAAFAD#####AAJ4MgAAAAoAAAAkAAAAFQD#####AAJ5MgAFZih4MikAAAAWAAAADwAAAA4AAAAlAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAA4AAAAlAAAADgAAACYAAAAYAP####8AAAD#AAMAAAAnAAAB9AABAAAAJAAAAAQAAAAkAAAAJQAAACYAAAAn#####wAAAAEAFUNQb2ludExpZUxpZXVQYXJQdExpZQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAUAAcAEBFI7xk4wAAAAFMAEBFI7xk4w#####wAAAAIABkNMYXRleAD#####AAAAAADANgAAAAAAAMA4AAAAAAAKAAAAKRABAf###wAAAAAAAAAAAAAAAQAAAAAAAAAAAAV5PXheMgAAABUA#####wACYTEAATEAAAABP#AAAAAAAAAAAAAVAP####8AAmEyAAMxLjUAAAABP#gAAAAAAAAAAAAVAP####8AAmIxAAEyAAAAAUAAAAAAAAAAAAAAFQD#####AAJiMgACLTkAAAAZAAAAAUAiAAAAAAAA#####wAAAAIADENDb21tZW50YWlyZQD#####AAB#AAAAAAAAAAAAAEAIAAAAAAAAAAAAGRAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAgjVmFsKGExKQAAAB0A#####wAAfwAAv#AAAAAAAABACAAAAAAAAAAAABwQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAAII1ZhbChhMikAAAAVAP####8ABXRlc3QxAAZhYnMxPjAAAAANBQAAAA4AAAAVAAAAAQAAAAAAAAAAAAAAHAD#####AAB#AADACAAAAAAAAL#wAAAAAAAAAAAAGxAAAAAAAAIAAAABAAAAAQAAAAAAAAAAABZcSWZ7dGVzdDF9e1xWYWx7YjF9fXt9AAAAHAD#####AAB#AADAEAAAAAAAAD#wAAAAAAAAAAAAHhAAAAAAAAIAAAABAAAAAQAAAAAAAAAAABZcSWZ7dGVzdDF9e1xWYWx7YjJ9fXt9AAAAGgD#####AAB#AAAQAAABAAMAAAAeAAAAGwAAABwA#####wAAfwAAQBwAAAAAAAAAAAAAAAAAAAAAABsQAAAAAAAAAAAAAQAAAAEAAAAAAAAAAAAWXElme3Rlc3QxfXt9e1xWYWx7YjF9fQAAABwA#####wAAfwAAQAgAAAAAAAC#8AAAAAAAAAAAAB4QAAAAAAAAAAAAAQAAAAEAAAAAAAAAAAAWXElme3Rlc3QxfXt9e1xWYWx7YjJ9fQAAAA7##########w=='
      : (ds.typeFonction === 'inverse')
          ? 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV8AAADnAAAAQEAAAAAAAAAAQAAAEX#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAFAAFAbqAAAAAAAEBtQUeuFHrh#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAAA4AAUkAwBgAAAAAAAAAAAAAAAAAAAUAAUBHGdsi0OVgAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAAEAAAAQABAAAAAQAAAAP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AAAAAAAQAAABAAEAAAABAAAABP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQUAAQAAAAcAAAAJAP####8BAAAAAA4AAUoAwCgAAAAAAADAEAAAAAAAAAUAAgAAAAf#####AAAAAgAHQ1JlcGVyZQD#####AObm5gABAAAAAQAAAAMAAAAJAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEBAAAAAAMAAAAM#####wAAAAEACUNMb25ndWV1cgD#####AAAAAQAAAA3#####AAAAAQAFQ0ZvbmMA#####wABZgADMS94AAAADQMAAAABP#AAAAAAAAD#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAF4AAAABAD#####AQAAAAAQAAF4AAAAAAAAAAAAQAgAAAAAAAAFAAFAI3O4UIrTxQAAAAT#####AAAAAgAIQ01lc3VyZVgA#####wACeDEAAAAKAAAAEP####8AAAABAAdDQ2FsY3VsAP####8AAnkxAAVmKHgxKf####8AAAABAA5DQXBwZWxGb25jdGlvbgAAAA8AAAAOAAAAEf####8AAAABABBDUG9pbnREYW5zUmVwZXJlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAADgAAABEAAAAOAAAAEv####8AAAACAA1DTGlldURlUG9pbnRzAP####8AAAAAAAEAAAATAAAB9AABAAAAEAAAAAQAAAAQAAAAEQAAABIAAAATAAAAFAD#####AARhYnMxAAMwLjgAAAABP+mZmZmZmZoAAAAUAP####8ABGFiczIAAzEuNQAAAAE#+AAAAAAAAAAAABQA#####wAEb3JkMQAHZihhYnMxKQAAABUAAAAPAAAADgAAABUAAAAUAP####8ABG9yZDIAB2YoYWJzMikAAAAVAAAADwAAAA4AAAAWAAAAFgD#####AQAAAAAQAAJBMQAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAADgAAABUAAAABAAAAAAAAAAAAAAAWAP####8BAAAAABAAAkEyAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAAOAAAAFQAAAA4AAAAXAAAAFgD#####AQAAAAAQAAJBMwAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAQAAAAAAAAAAAAAADgAAABcAAAAWAP####8BAAAAABAAAkIxAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAAOAAAAFgAAAAEAAAAAAAAAAAAAABYA#####wEAAAAAEAACQjIAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAA4AAAAWAAAADgAAABgAAAAWAP####8BAAAAABAAAkIzAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABAAAAAAAAAAAAAAAOAAAAGP####8AAAABAAhDU2VnbWVudAD#####AAAA#wAQAAABAQIAAAAeAAAAHQAAABgA#####wAAAP8AEAAAAQECAAAAHQAAABwAAAAYAP####8AAAD#ABAAAAEBAgAAABkAAAAaAAAAGAD#####AAAA#wAQAAABAQIAAAAaAAAAGwAAABgA#####wAAfwAAEAAAAQADAAAAGQAAABwAAAAEAP####8BAAAAABAAAngxAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAAAAAAAAAAAACMAAAATAP####8AAngyAAAACgAAACQAAAAUAP####8AAnkyAAVmKHgyKQAAABUAAAAPAAAADgAAACUAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAADgAAACUAAAAOAAAAJgAAABcA#####wAAAP8AAwAAACcAAAH0AAEAAAAkAAAABAAAACQAAAAlAAAAJgAAACf#####AAAAAQAVQ1BvaW50TGllTGlldVBhclB0TGllAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQABwAQEUjvGTjAAAAAUwAQEUjvGTjD#####AAAAAgAGQ0xhdGV4AP####8AAAAAAEBeAAAAAAAAwG7AAAAAAAEAAAApEAEB####AAAAAAAAAAAAAAABAAAAAAAAAAAADXk9XGZyYWN7MX17eH0AAAAUAP####8AAmExAAExAAAAAT#wAAAAAAAAAAAAFAD#####AAJhMgADMS41AAAAAT#4AAAAAAAAAAAAFAD#####AAJiMQABMgAAAAFAAAAAAAAAAAAAABQA#####wACYjIAAi05#####wAAAAEADENNb2luc1VuYWlyZQAAAAFAIgAAAAAAAAAAABQA#####wAFdGVzdDEABmFiczE+MAAAAA0FAAAADgAAABUAAAABAAAAAAAAAAAAAAAYAP####8AAH8AABAAAAEAAwAAAB4AAAAbAAAAFAD#####AARudW0xAAIxNQAAAAFALgAAAAAAAAAAABQA#####wAEZGVuMQACMTYAAAABQDAAAAAAAAAAAAAUAP####8ABG51bTIAAjE3AAAAAUAxAAAAAAAAAAAAFAD#####AARkZW4yAAIxOAAAAAFAMgAAAAAAAAAAABQA#####wAIZXN0RnJhYzEAATAAAAABAAAAAAAAAAAAAAAUAP####8ACGVzdEZyYWMyAAExAAAAAT#wAAAAAAAAAAAAGgD#####AAB#AAAAAAAAAAAAAEAIAAAAAAAAAAAAGRAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAERcSWZ7dGVzdDF9e1xJZntlc3RGcmFjMX17XGZyYWN7XFZhbHtudW0xfX17XFZhbHtkZW4xfX19e1xWYWx7YTF9fX17fQAAABoA#####wAAfwAAAAAAAAAAAABAAAAAAAAAAAAAABwQAAAAAAABAAAAAAAAAAEAAAAAAAAAAABEXElme3Rlc3QxfXtcSWZ7ZXN0RnJhYzJ9e1xmcmFje1xWYWx7bnVtMn19e1xWYWx7ZGVuMn19fXtcVmFse2EyfX19e30AAAAaAP####8AAH8AAAAAAAAAAAAAwAAAAAAAAAAAAAAcEAAAAAAAAQAAAAIAAAABAAAAAAAAAAAARFxJZnt0ZXN0MX17fXtcSWZ7ZXN0RnJhYzJ9e1xmcmFje1xWYWx7bnVtMn19e1xWYWx7ZGVuMn19fXtcVmFse2EyfX19AAAAGgD#####AAB#AAC#8AAAAAAAAMAAAAAAAAAAAAAAGRAAAAAAAAEAAAACAAAAAQAAAAAAAAAAAERcSWZ7dGVzdDF9e317XElme2VzdEZyYWMxfXtcZnJhY3tcVmFse251bTF9fXtcVmFse2RlbjF9fX17XFZhbHthMX19fQAAABQA#####wAFbnVtbzEAAjE5AAAAAUAzAAAAAAAAAAAAFAD#####AAVkZW5vMQACMjAAAAABQDQAAAAAAAAAAAAUAP####8ABW51bW8yAAIyMQAAAAFANQAAAAAAAAAAABQA#####wAFZGVubzIAAjIyAAAAAUA2AAAAAAAAAAAAFAD#####AAhlc3RGcmFjMwABMAAAAAEAAAAAAAAAAAAAABQA#####wAIZXN0RnJhYzQAATAAAAABAAAAAAAAAAAAAAAaAP####8AAH8AAMAwAAAAAAAAAAAAAAAAAAAAAAAbEAAAAAAAAgAAAAEAAAABAAAAAAAAAAAARlxJZnt0ZXN0MX17XElme2VzdEZyYWMzfXtcZnJhY3tcVmFse251bW8xfX17XFZhbHtkZW5vMX19fXtcVmFse2IxfX19e30AAAAaAP####8AAH8AAMAAAAAAAAAAAAAAAAAAAAAAAAAeEAAAAAAAAgAAAAEAAAABAAAAAAAAAAAARlxJZnt0ZXN0MX17XElme2VzdEZyYWM0fXtcZnJhY3tcVmFse251bW8yfX17XFZhbHtkZW5vMn19fXtcVmFse2IyfX19e30AAAAaAP####8AAH8AAEAuAAAAAAAAP#AAAAAAAAAAAAAbEAAAAAAAAAAAAAEAAAABAAAAAAAAAAAARlxJZnt0ZXN0MX17fXtcSWZ7ZXN0RnJhYzN9e1xmcmFje1xWYWx7bnVtbzF9fXtcVmFse2Rlbm8xfX19e1xWYWx7YjF9fX0AAAAaAP####8AAH8AAD#wAAAAAAAAv#AAAAAAAAAAAAAeEAAAAAAAAAAAAAEAAAABAAAAAAAAAAAARlxJZnt0ZXN0MX17fXtcSWZ7ZXN0RnJhYzR9e1xmcmFje1xWYWx7bnVtbzJ9fXtcVmFse2Rlbm8yfX19e1xWYWx7YjJ9fX0AAAAO##########8='
          : (ds.typeFonction === 'racine carree')
              ? 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV8AAADnAAAAQEAAAAAAAAAAQAAAEH#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAhlc3RTcXJ0MgABMQAAAAE#8AAAAAAAAAAAAAIA#####wAIZXN0U3FydDEAATEAAAABP#AAAAAAAAD#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAFAAFAUAAAAAAAAEBrQUeuFHrh#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8AAAAAABAAAAEAAQAAAAMBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAAA4AAUkAwBgAAAAAAAAAAAAAAAAAAAUAAUBHGdsi0OVgAAAABP####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAAEAAAAQABAAAAAwAAAAX#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AAAAAAAQAAABAAEAAAADAAAABv####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAMAAAAF#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAcAAAAI#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQUAAQAAAAkAAAAKAP####8BAAAAAA4AAUoAwCgAAAAAAADAEAAAAAAAAAUAAgAAAAn#####AAAAAgAHQ1JlcGVyZQD#####AObm5gABAAAAAwAAAAUAAAALAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAz#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAP#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAADf####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEBAAAAAAUAAAAO#####wAAAAEACUNMb25ndWV1cgD#####AAAAAwAAAA######AAAAAQAFQ0ZvbmMA#####wABZgAHc3FydCh4Kf####8AAAACAAlDRm9uY3Rpb24B#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAABeAAAAAUA#####wEAAAAAEAABeAAAAAAAAAAAAEAIAAAAAAAABQABwAQEUjvGTjAAAAAG#####wAAAAIACENNZXN1cmVYAP####8AAngxAAAADAAAABIAAAACAP####8AAnkxAAVmKHgxKf####8AAAABAA5DQXBwZWxGb25jdGlvbgAAABEAAAAPAAAAE#####8AAAABABBDUG9pbnREYW5zUmVwZXJlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAADwAAABMAAAAPAAAAFP####8AAAACAA1DTGlldURlUG9pbnRzAP####8AAAAAAAEAAAAVAAAB9AABAAAAEgAAAAQAAAASAAAAEwAAABQAAAAVAAAAAgD#####AARhYnMxAAMwLjgAAAABP+mZmZmZmZoAAAACAP####8ABGFiczIAAzIuNQAAAAFABAAAAAAAAAAAAAIA#####wAEb3JkMQAHZihhYnMxKQAAABYAAAARAAAADwAAABcAAAACAP####8ABG9yZDIAB2YoYWJzMikAAAAWAAAAEQAAAA8AAAAYAAAAFwD#####AQAAAAAQAAJBMQAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAADwAAABcAAAABAAAAAAAAAAAAAAAXAP####8BAAAAABAAAkEyAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAAPAAAAFwAAAA8AAAAZAAAAFwD#####AQAAAAAQAAJBMwAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAAQAAAAAAAAAAAAAADwAAABkAAAAXAP####8BAAAAABAAAkIxAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAAPAAAAGAAAAAEAAAAAAAAAAAAAABcA#####wEAAAAAEAACQjIAAAAAAAAAAABACAAAAAAAAAUAAAAADAAAAA8AAAAYAAAADwAAABoAAAAXAP####8BAAAAABAAAkIzAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAABAAAAAAAAAAAAAAAPAAAAGv####8AAAABAAhDU2VnbWVudAD#####AAAA#wAQAAABAQIAAAAgAAAAHwAAABkA#####wAAAP8AEAAAAQECAAAAHwAAAB4AAAAZAP####8AAAD#ABAAAAEBAgAAABsAAAAcAAAAGQD#####AAAA#wAQAAABAQIAAAAcAAAAHQAAABkA#####wAAfwAAEAAAAQADAAAAGwAAAB4AAAAFAP####8BAAAAABAAAngxAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAAAAAAAAAAAACUAAAAVAP####8AAngyAAAADAAAACYAAAACAP####8AAnkyAAVmKHgyKQAAABYAAAARAAAADwAAACcAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAADwAAACcAAAAPAAAAKAAAABgA#####wAAAP8AAwAAACkAAAH0AAEAAAAmAAAABAAAACYAAAAnAAAAKAAAACn#####AAAAAQAVQ1BvaW50TGllTGlldVBhclB0TGllAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQABwAQEUjvGTjAAAAAWwAQEUjvGTjD#####AAAAAgAGQ0xhdGV4AP####8AAAAAAMA2AAAAAAAAwDgAAAAAAAoAAAArEAEB####AAAAAAAAAAAAAAABAAAAAAAAAAAABXk9eF4yAAAAAgD#####AAJhMQABMQAAAAE#8AAAAAAAAAAAAAIA#####wACYTIAAzEuNQAAAAE#+AAAAAAAAAAAAAIA#####wACYjEAATIAAAABQAAAAAAAAAAAAAACAP####8AAmIyAAItOf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQCIAAAAAAAD#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8AAH8AAAAAAAAAAAAAQAgAAAAAAAAAAAAbEAAAAAAAAQAAAAAAAAABAAAAAAAAAAAACCNWYWwoYTEpAAAAHQD#####AAB#AAC#8AAAAAAAAEAIAAAAAAAAAAAAHhAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAgjVmFsKGEyKQAAAAIA#####wAFdGVzdDEABmFiczE+MAAAAA4FAAAADwAAABcAAAABAAAAAAAAAAAAAAAbAP####8AAH8AAMAIAAAAAAAAv#AAAAAAAAAAAAAdEAAAAAAAAgAAAAEAAAABAAAAAAAAAAAANlxJZnt0ZXN0MX17XElme2VzdFNxcnQxfXtcc3FydHtcVmFse2ExfX19e1xWYWx7YjF9fX17fQAAABsA#####wAAfwAAwDoAAAAAAAAAAAAAAAAAAAAAACAQAAAAAAACAAAAAQAAAAEAAAAAAAAAAAA2XElme3Rlc3QxfXtcSWZ7ZXN0U3FydDJ9e1xzcXJ0e1xWYWx7YTJ9fX17XFZhbHtiMn19fXt9AAAAGQD#####AAB#AAAQAAABAAMAAAAgAAAAHQAAABsA#####wAAfwAAQBwAAAAAAAAAAAAAAAAAAAAAAB0QAAAAAAAAAAAAAQAAAAEAAAAAAAAAAAAWXElme3Rlc3QxfXt9e1xWYWx7YjF9fQAAABsA#####wAAfwAAQAgAAAAAAAC#8AAAAAAAAAAAACAQAAAAAAAAAAAAAQAAAAEAAAAAAAAAAAAWXElme3Rlc3QxfXt9e1xWYWx7YjJ9fQAAABcA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAFAAAAAAwAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAXAP####8BAAAAABAAAABACAAAAAAAAAAAAAAAAAAABQAAAAAMAAAAAT#gAAAAAAAAAAAAAQAAAAAAAAAAAAAAGQD#####AAAAAAAQAAABAAEAAAA5AAAAOgAAAAUA#####wEAAAAAEAACeDIAAAAAAAAAAABACAAAAAAAAAUAAQAAAAAAAAAAAAAAOwAAABUA#####wACeDMAAAAMAAAAPAAAAAIA#####wACeTMABWYoeDMpAAAAFgAAABEAAAAPAAAAPQAAABcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAAPAAAAPQAAAA8AAAA+AAAAGAD#####AAAAAAABAAAAPwAAAfQAAQAAADwAAAAEAAAAPAAAAD0AAAA+AAAAPwAAABD##########w=='
              : (ds.typeFonction === 'cube')
                  ? 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAV8AAADnAAAAQEAAAAAAAAAAQAAAEL#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AARhYnMyAAQtMS41#####wAAAAEADENNb2luc1VuYWlyZQAAAAE#+AAAAAAAAAAAAAIA#####wAFdGVzdDIABmFiczI+MP####8AAAABAApDT3BlcmF0aW9uBf####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAABAAAAAQAAAAAAAAAA#####wAAAAEACkNQb2ludEJhc2UA#####wAAAAAADgABTwDAKAAAAAAAAAAAAAAAAAAABQABQGTAAAAAAABAbsFHrhR64f####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AAAAAAAQAAABAAEAAAADAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFJAMAYAAAAAAAAAAAAAAAAAAAFAAFARxnbItDlYAAAAAT#####AAAAAQAJQ0Ryb2l0ZUFCAP####8AAAAAABAAAAEAAQAAAAMAAAAF#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wAAAAAAEAAAAQABAAAAAwAAAAb#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAEAAAADAAAABf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAHAAAACP####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAEFAAEAAAAJAAAADQD#####AQAAAAAOAAFKAMAoAAAAAAAAwBAAAAAAAAAFAAIAAAAJ#####wAAAAIAB0NSZXBlcmUA#####wDm5uYAAQAAAAMAAAAFAAAACwAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAP####8AAAABAApDVW5pdGV4UmVwAP####8ABHVuaXQAAAAM#####wAAAAEAC0NIb21vdGhldGllAP####8AAAADAAAABAMAAAABP#AAAAAAAAAAAAAFAAAADf####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEBAAAAAAUAAAAO#####wAAAAEACUNMb25ndWV1cgD#####AAAAAwAAAA######AAAAAQAFQ0ZvbmMA#####wABZgADeF4z#####wAAAAEACkNQdWlzc2FuY2X#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAAAUAIAAAAAAAAAAF4AAAACAD#####AQAAAAAQAAF4AAAAAAAAAAAAQAgAAAAAAAAFAAHABARSO8ZOMAAAAAb#####AAAAAgAIQ01lc3VyZVgA#####wACeDEAAAAMAAAAEgAAAAIA#####wACeTEABWYoeDEp#####wAAAAEADkNBcHBlbEZvbmN0aW9uAAAAEQAAAAUAAAAT#####wAAAAEAEENQb2ludERhbnNSZXBlcmUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAAFAAAAEwAAAAUAAAAU#####wAAAAIADUNMaWV1RGVQb2ludHMA#####wAAAAAAAQAAABUAAAH0AAEAAAASAAAABAAAABIAAAATAAAAFAAAABUAAAACAP####8ABGFiczEABC0wLjgAAAADAAAAAT#pmZmZmZmaAAAAAgD#####AARvcmQxAAdmKGFiczEpAAAAFwAAABEAAAAFAAAAFwAAAAIA#####wAEb3JkMgAHZihhYnMyKQAAABcAAAARAAAABQAAAAEAAAAYAP####8BAAAAABAAAkExAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAAFAAAAFwAAAAEAAAAAAAAAAAAAABgA#####wEAAAAAEAACQTIAAAAAAAAAAABACAAAAAAAAAUAAAAADAAAAAUAAAAXAAAABQAAABgAAAAYAP####8BAAAAABAAAkEzAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAABAAAAAAAAAAAAAAAFAAAAGAAAABgA#####wEAAAAAEAACQjEAAAAAAAAAAABACAAAAAAAAAUAAAAADAAAAAUAAAABAAAAAQAAAAAAAAAAAAAAGAD#####AQAAAAAQAAJCMgAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAABQAAAAEAAAAFAAAAGQAAABgA#####wEAAAAAEAACQjMAAAAAAAAAAABACAAAAAAAAAUAAAAADAAAAAEAAAAAAAAAAAAAAAUAAAAZ#####wAAAAEACENTZWdtZW50AP####8AAAD#ABAAAAEBAgAAAB8AAAAeAAAAGgD#####AAAA#wAQAAABAQIAAAAeAAAAHQAAABoA#####wAAAP8AEAAAAQECAAAAGgAAABsAAAAaAP####8AAAD#ABAAAAEBAgAAABsAAAAcAAAAGgD#####AAB#AAAQAAABAAMAAAAaAAAAHQAAAAgA#####wEAAAAAEAACeDEAAAAAAAAAAABACAAAAAAAAAUAAQAAAAAAAAAAAAAAJAAAABYA#####wACeDIAAAAMAAAAJQAAAAIA#####wACeTIABWYoeDIpAAAAFwAAABEAAAAFAAAAJgAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAAFAAAAJgAAAAUAAAAnAAAAGQD#####AAAA#wADAAAAKAAAAfQAAQAAACUAAAAEAAAAJQAAACYAAAAnAAAAKP####8AAAABABVDUG9pbnRMaWVMaWV1UGFyUHRMaWUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAHABARSO8ZOMAAAABbABARSO8ZOMP####8AAAACAAZDTGF0ZXgA#####wAAAAAAwDYAAAAAAADAOAAAAAAACgAAACoQAQH###8AAAAAAAAAAAAAAAEAAAAAAAAAAAAFeT14XjIAAAACAP####8AAmExAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAJhMgADMS41AAAAAT#4AAAAAAAAAAAAAgD#####AAJiMQABMgAAAAFAAAAAAAAAAAAAAAIA#####wACYjIAAi05AAAAAwAAAAFAIgAAAAAAAAAAAAIA#####wAFdGVzdDEABmFiczE+MAAAAAQFAAAABQAAABcAAAABAAAAAAAAAAAAAAAcAP####8AAH8AAMAIAAAAAAAAv#AAAAAAAAAAAAAcEAAAAAAAAgAAAAEAAAABAAAAAAAAAAAAFlxJZnt0ZXN0MX17XFZhbHtiMX19e30AAAAcAP####8AAH8AAMA6AAAAAAAAAAAAAAAAAAAAAAAfEAAAAAAAAgAAAAEAAAABAAAAAAAAAAAAFlxJZnt0ZXN0Mn17XFZhbHtiMn19e30AAAAaAP####8AAH8AABAAAAEAAwAAAB8AAAAcAAAAHAD#####AAB#AABAHAAAAAAAAAAAAAAAAAAAAAAAHBAAAAAAAAAAAAABAAAAAQAAAAAAAAAAABZcSWZ7dGVzdDF9e317XFZhbHtiMX19AAAAHAD#####AAB#AABACAAAAAAAAL#wAAAAAAAAAAAAHxAAAAAAAAAAAAABAAAAAQAAAAAAAAAAABZcSWZ7dGVzdDJ9e317XFZhbHtiMn19AAAAGAD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAUAAAAADAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAABgA#####wEAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAFAAAAAAwAAAABP+AAAAAAAAAAAAABAAAAAAAAAAAAAAAaAP####8AAAAAABAAAAEAAQAAADYAAAA3AAAACAD#####AQAAAAAQAAJ4MgAAAAAAAAAAAEAIAAAAAAAABQABAAAAAAAAAAAAAAA4AAAAFgD#####AAJ4MwAAAAwAAAA5AAAAAgD#####AAJ5MwAFZih4MykAAAAXAAAAEQAAAAUAAAA6AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAADAAAAAUAAAA6AAAABQAAADsAAAAZAP####8AAAAAAAEAAAA8AAAB9AABAAAAOQAAAAQAAAA5AAAAOgAAADsAAAA8AAAAHAD#####AAB#AAC#8AAAAAAAAEAIAAAAAAAAAAAAGhAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAABZcSWZ7dGVzdDF9e1xWYWx7YTF9fXt9AAAAHAD#####AAB#AAA#8AAAAAAAAEAIAAAAAAAAAAAAHRAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAABZcSWZ7dGVzdDJ9e1xWYWx7YTJ9fXt9AAAAHAD#####AAB#AAEAAAAaEAAAAAAAAQAAAAIAAAABAAAAAAAAAAAAFlxJZnt0ZXN0MX17fXtcVmFse2ExfX0AAAAcAP####8AAH8AAQAAAB0QAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAWXElme3Rlc3QyfXt9e1xWYWx7YTJ9fQAAABD##########w=='
                  : ''
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    // stor.mtgAppLecteur.calculateAndDisplayAll(true);
  }

  function modifFig () {
    // cette fonction sert à modifier la figure de base
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'ord', String(stor.donnees.valeursInit[me.questionCourante - 1]))
    let i, absFigMax, newAbs1, newAbs2
    if (ds.typeFonction === 'carree') {
      // Je choisis une échelle pour ne pas m’embêter à placer les points
      absFigMax = 2.5
      if (stor.donnees.valeursInit[me.questionCourante - 1][0] > 0) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs2', String(absFigMax))
        newAbs1 = stor.donnees.valeursInit[me.questionCourante - 1][0] * absFigMax / stor.donnees.valeursInit[me.questionCourante - 1][1]
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs1', String(newAbs1))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a1', String(stor.donnees.valeursInit[me.questionCourante - 1][0]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a2', String(stor.donnees.valeursInit[me.questionCourante - 1][1]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b1', String(stor.donnees.borne[me.questionCourante - 1][0]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b2', String(stor.donnees.borne[me.questionCourante - 1][1]))
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs1', String(-absFigMax))
        newAbs2 = stor.donnees.valeursInit[me.questionCourante - 1][1] * (-absFigMax) / stor.donnees.valeursInit[me.questionCourante - 1][0]
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs2', String(newAbs2))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a2', String(stor.donnees.valeursInit[me.questionCourante - 1][1]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a1', String(stor.donnees.valeursInit[me.questionCourante - 1][0]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b2', String(stor.donnees.borne[me.questionCourante - 1][1]))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b1', String(stor.donnees.borne[me.questionCourante - 1][0]))
      }
    } else if (ds.typeFonction === 'inverse') {
      for (i = 1; i <= 2; i++) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs' + i, String(j3pCalculValeur(stor.donnees.valeursInit[me.questionCourante - 1][i - 1])))
        if (String(stor.donnees.valeursInit[me.questionCourante - 1][i - 1]).indexOf('frac') > -1) {
          const fracInit = j3pExtraireNumDen(stor.donnees.valeursInit[me.questionCourante - 1][i - 1])
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'estFrac' + i, '1')
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'num' + i, String(fracInit[1]))
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'den' + i, String(fracInit[2]))
        } else {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'estFrac' + i, '0')
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a' + i, String(stor.donnees.valeursInit[me.questionCourante - 1][i - 1]))
        }
        if (String(stor.donnees.borne[me.questionCourante - 1][i - 1]).indexOf('frac') > -1) {
          const fracRep = j3pExtraireNumDen(stor.donnees.borne[me.questionCourante - 1][i - 1])
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'estFrac' + (2 + i), '1')
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'numo' + i, String(fracRep[1]))
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'deno' + i, String(fracRep[2]))
        } else {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'estFrac' + (2 + i), '0')
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b' + i, String(stor.donnees.borne[me.questionCourante - 1][i - 1]))
        }
      }
    } else if (ds.typeFonction === 'racine carree') {
      absFigMax = 6.5
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs2', String(absFigMax))
      newAbs1 = stor.donnees.valeursInit[me.questionCourante - 1][0] * absFigMax / stor.donnees.valeursInit[me.questionCourante - 1][1]
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs1', String(newAbs1))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a1', String(stor.donnees.valeursInit[me.questionCourante - 1][0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a2', String(stor.donnees.valeursInit[me.questionCourante - 1][1]))
      for (i = 1; i <= 2; i++) {
        if (String(stor.donnees.borne[me.questionCourante - 1][i - 1]).indexOf('sqrt') > -1) {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'estSqrt' + i, '1')
        } else {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'estSqrt' + i, '0')
        }
      }
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b1', String(stor.donnees.borne[me.questionCourante - 1][0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b2', String(stor.donnees.borne[me.questionCourante - 1][1]))
    } else if (ds.typeFonction === 'cube') {
      absFigMax = 1.7
      if (Math.abs(stor.donnees.valeursInit[me.questionCourante - 1][0]) > Math.abs(stor.donnees.valeursInit[me.questionCourante - 1][1])) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs1', String(-absFigMax))
        newAbs2 = stor.donnees.valeursInit[me.questionCourante - 1][1] * (-absFigMax) / stor.donnees.valeursInit[me.questionCourante - 1][0]
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs2', String(newAbs2))
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs2', String(absFigMax))
        newAbs1 = stor.donnees.valeursInit[me.questionCourante - 1][0] * absFigMax / stor.donnees.valeursInit[me.questionCourante - 1][1]
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs1', String(newAbs1))
      }
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a1', String(stor.donnees.valeursInit[me.questionCourante - 1][0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a2', String(stor.donnees.valeursInit[me.questionCourante - 1][1]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b1', String(stor.donnees.borne[me.questionCourante - 1][0]))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b2', String(stor.donnees.borne[me.questionCourante - 1][1]))
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function afficheCorr (bonneRep) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    stor.laPalette.innerHTML = ''
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneRep) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    const divMtg32 = j3pAddElt(stor.zoneCorr, 'div')
    const largFig = (ds.typeFonction === 'carree') ? 321 : (ds.typeFonction === 'racine carree') ? 560 : (ds.typeFonction === 'cube') ? 342 : 495
    const hautFig = (ds.typeFonction === 'carree')
      ? 356
      : (ds.typeFonction === 'inverse') ? 432 : (ds.typeFonction === 'cube') ? 500 : 266
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(divMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
    for (let i = 1; i <= 4; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    if (ds.typeFonction === 'carree') {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1_1)
      const corr2 = (stor.positif) ? ds.textes.corr2_1 : ds.textes.corr2_12
      j3pAffiche(stor.zoneExpli2, '', corr2, {
        s: stor.encadreInit
      })
    } else if (ds.typeFonction === 'inverse') {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1_2)
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2_2, {
        s: stor.encadreInit,
        i: stor.domaine
      })
    } else if (ds.typeFonction === 'racine carree') {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1_3)
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2_3, {
        s: stor.encadreInit
      })
    } else if (ds.typeFonction === 'cube') {
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1_4)
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr2_4, {
        s: stor.encadreInit
      })
    }
    let repAffichee = stor.zoneInput.reponse[0].replace(/</g, '< ')
    repAffichee = repAffichee.replace(/>/g, '> ')
    j3pAffiche(stor.zoneExpli3, '', ds.textes.corr3, {
      r: repAffichee
    })
    depart()
    modifFig()
  }

  function irreductibleFrac (frac) {
    // frac est un nombre écrit sous la forme \\frac{...}{...}
    // cette fonction renvoie un booléen selon que la fraction est irréductible ou non
    const tabFrac = j3pExtraireNumDen(frac)
    if (tabFrac[1] === 0) { return false } else {
      if ((Math.abs(Math.round(tabFrac[1]) - tabFrac[1]) > Math.pow(10, -10)) || (Math.abs(Math.round(tabFrac[2]) - tabFrac[2]) > Math.pow(10, -10))) { return true }
      try {
        const lePGCD = j3pPGCD(Math.abs(tabFrac[1]), Math.abs(tabFrac[2]))
        return (lePGCD === 1)
      } catch (e) {
        return true
      }
    }
  }

  function choixValeursInverse (nb) {
    // nb est le nombre de valeurs à générer
    let tabEntiers = []
    let tabEntiersInverse = []
    let tabFrac = []
    let tabFracInverse = []
    let pioche; let pioche2
    for (let i = 1; i <= 5; i++) {
      tabEntiers.push(i)
      if (i === 1) {
        tabEntiersInverse.push(i)
      } else {
        tabEntiersInverse.push('\\frac{1}{' + i + '}')
      }
    }
    tabFrac.push('\\frac{1}{2}', '\\frac{1}{3}', '\\frac{1}{4}', '\\frac{2}{3}', '\\frac{3}{4}', '\\frac{4}{3}', '\\frac{5}{3}')
    tabFracInverse.push(2, 3, 4, '\\frac{3}{2}', '\\frac{4}{3}', '\\frac{3}{4}', '\\frac{3}{5}')
    const tabEntiersInit = [...tabEntiers]
    const tabEntiersInverseInit = [...tabEntiersInverse]
    const tabFracInit = [...tabFrac]
    const tabFracInverseInit = [...tabFracInverse]
    do {
      pioche = j3pGetRandomInt(0, (tabEntiers.length - 1))
      pioche2 = j3pGetRandomInt(0, (tabEntiers.length - 1))
    } while (Math.abs(pioche - pioche2) < Math.pow(10, -12))
    // On ordonne maintenant ces deux nombres
    let numMin = (tabEntiers[pioche] < tabEntiers[pioche2]) ? pioche : pioche2
    let numMax = (tabEntiers[pioche] < tabEntiers[pioche2]) ? pioche2 : pioche
    const premierNb = tabEntiers[numMin]
    const deuxiemeNb = tabEntiers[numMax]
    const premierInverse = tabEntiersInverse[numMin]
    const deuxiemeInverse = tabEntiersInverse[numMax]
    tabEntiers.splice(pioche, 1)
    tabEntiersInverse.splice(pioche, 1)
    tabEntiers.splice(pioche2, 1)
    tabEntiersInverse.splice(pioche2, 1)
    const tabInterm1 = []
    const tabInterm2 = []
    let choixSigne
    for (let i = 1; i < nb; i++) {
      if (i % 2 === 1) { // je prends des entiers dans l’encadrement initial
        do {
          pioche = j3pGetRandomInt(0, (tabEntiers.length - 1))
          pioche2 = j3pGetRandomInt(0, (tabEntiers.length - 1))
        } while (Math.abs(pioche - pioche2) < Math.pow(10, -12))
        // On ordonne maintenant ces deux nombres
        numMin = (tabEntiers[pioche] < tabEntiers[pioche2]) ? pioche : pioche2
        numMax = (tabEntiers[pioche] < tabEntiers[pioche2]) ? pioche2 : pioche
        choixSigne = j3pGetRandomInt(0, 1)
        if (choixSigne) {
          tabInterm1.push([-tabEntiers[numMax], -tabEntiers[numMin]])
          tabInterm2.push([j3pGetLatexProduit('-1', tabEntiersInverse[numMax]), j3pGetLatexProduit('-1', tabEntiersInverse[numMin])])
        } else {
          tabInterm1.push([tabEntiers[numMin], tabEntiers[numMax]])
          tabInterm2.push([tabEntiersInverse[numMin], tabEntiersInverse[numMax]])
        }
        tabEntiers.splice(pioche, 1)
        tabEntiersInverse.splice(pioche, 1)
        tabEntiers.splice(pioche2, 1)
        tabEntiersInverse.splice(pioche2, 1)
      } else { // je prends des fractions dans l’encadrement initial
        do {
          pioche = j3pGetRandomInt(0, (tabFrac.length - 1))
          pioche2 = j3pGetRandomInt(0, (tabFrac.length - 1))
        } while (Math.abs(pioche - pioche2) < Math.pow(10, -12))
        // On ordonne maintenant ces deux nombres
        numMin = (j3pCalculValeur(tabFrac[pioche]) < j3pCalculValeur(tabFrac[pioche2])) ? pioche : pioche2
        numMax = (j3pCalculValeur(tabFrac[pioche]) < j3pCalculValeur(tabFrac[pioche2])) ? pioche2 : pioche
        choixSigne = j3pGetRandomInt(0, 1)
        if (choixSigne) {
          tabInterm1.push([j3pGetLatexProduit('-1', tabFrac[numMax]), j3pGetLatexProduit('-1', tabFrac[numMin])])
          tabInterm2.push([j3pGetLatexProduit('-1', tabFracInverse[numMax]), j3pGetLatexProduit('-1', tabFracInverse[numMin])])
        } else {
          tabInterm1.push([tabFrac[numMin], tabFrac[numMax]])
          tabInterm2.push([tabFracInverse[numMin], tabFracInverse[numMax]])
        }
        tabFrac.splice(pioche, 1)
        tabFracInverse.splice(pioche, 1)
        tabFrac.splice(pioche2, 1)
        tabFracInverse.splice(pioche2, 1)
      }
      if (tabEntiers.length <= 1) {
        tabEntiers = [...tabEntiersInit]
        tabEntiersInverse = [...tabEntiersInverseInit]
      }
      if (tabFrac.length <= 1) {
        tabFrac = [...tabFracInit]
        tabFracInverse = [...tabFracInverseInit]
      }
    }
    const [tabValeurs, tabInverse] = j3pShuffleMulti(tabInterm1, tabInterm2)
    tabValeurs.unshift([premierNb, deuxiemeNb])
    tabInverse.unshift([premierInverse, deuxiemeInverse])
    return { valeursInit: tabValeurs, borne: tabInverse }// borne ne contient que la borne important dans la réponse
  }

  function choixValeursRacine (nb) {
    // nb est le nombre de valeurs à générer
    // si radical vaut true, alors on peut mettre des nombres qui ne seraient pas des carrés parfaits
    // la première valeur est nécessairement un carré parfait
    const tabValeurs = []; const tabRacine = []
    let tabCarreesEntiers = []
    let tabCarreNonEntier = []; let tabCarreRacine = []
    let pioche2
    for (let i = 1; i <= 6; i++) {
      tabCarreesEntiers.push(i)
    }
    tabCarreNonEntier.push(3, 5, 7, 2, 13)
    tabCarreRacine.push('\\sqrt{3}', '\\sqrt{5}', '\\sqrt{7}', '\\sqrt{2}', '\\sqrt{13}')
    const tabCarreEntiersInit = [...tabCarreesEntiers]
    const tabCarreNonEntierInit = [...tabCarreNonEntier]
    const tabCarreRacineInit = [...tabCarreRacine]
    // les premiers est un carré parfait
    const pioche = j3pGetRandomInt(0, (tabCarreesEntiers.length - 1))
    do {
      pioche2 = j3pGetRandomInt(0, (tabCarreesEntiers.length - 1))
    } while (pioche === pioche2)
    if (tabCarreesEntiers[pioche] > tabCarreesEntiers[pioche2]) {
      tabValeurs.push([Math.pow(tabCarreesEntiers[pioche2], 2), Math.pow(tabCarreesEntiers[pioche], 2)])
      tabRacine.push([tabCarreesEntiers[pioche2], tabCarreesEntiers[pioche]])
    } else {
      tabValeurs.push([Math.pow(tabCarreesEntiers[pioche], 2), Math.pow(tabCarreesEntiers[pioche2], 2)])
      tabRacine.push([tabCarreesEntiers[pioche], tabCarreesEntiers[pioche2]])
    }
    tabCarreesEntiers.splice(pioche, 1)
    tabCarreesEntiers.splice(pioche2, 1)
    let choix = j3pGetRandomInt(0, 1)
    for (let k = 2; k <= nb; k++) {
      const newDonnees = []
      const newDonnees2 = []
      const tabPioche = []
      for (let i = 1; i <= 2; i++) {
        choix = (choix + 1) % 2
        if (choix === 0) {
          // On prend un carré parfait
          if (i === 2) {
            // Il ne faut pas que je me retrouve avec la même valeur que la précédente
            do {
              tabPioche[i - 1] = j3pGetRandomInt(0, (tabCarreesEntiers.length - 1))
            } while (Math.abs(newDonnees[i - 2] - Math.pow(tabCarreesEntiers[tabPioche[i - 1]], 2)) < Math.pow(10, -10))
          } else {
            tabPioche[i - 1] = j3pGetRandomInt(0, (tabCarreesEntiers.length - 1))
          }
          newDonnees.push(Math.pow(tabCarreesEntiers[tabPioche[i - 1]], 2))
          newDonnees2.push(tabCarreesEntiers[tabPioche[i - 1]])
        } else {
          // On en prend un qui n’est pas un carré parfait
          if (i === 2) {
            // Il ne faut pas que je me retrouve avec la même valeur que la précédente
            do {
              tabPioche[i - 1] = j3pGetRandomInt(0, (tabCarreNonEntier.length - 1))
            } while (Math.abs(newDonnees[i - 2] - tabCarreNonEntier[tabPioche[i - 1]]) < Math.pow(10, -10))
          } else {
            tabPioche[i - 1] = j3pGetRandomInt(0, (tabCarreNonEntier.length - 1))
          }
          newDonnees.push(tabCarreNonEntier[tabPioche[i - 1]])
          newDonnees2.push(tabCarreRacine[tabPioche[i - 1]])
        }
      }
      // Je permute les valeurs si nécessaire pour qu’elles soient dans l’ordre croissant
      if (newDonnees[0] > newDonnees[1]) {
        let permut = newDonnees[0]
        newDonnees[0] = newDonnees[1]
        newDonnees[1] = permut
        permut = newDonnees2[0]
        newDonnees2[0] = newDonnees2[1]
        newDonnees2[1] = permut
      }
      tabValeurs.push(newDonnees)
      tabRacine.push(newDonnees2)
      if (tabCarreesEntiers.length <= 1) {
        tabCarreesEntiers = [...tabCarreEntiersInit]
      }
      if (tabCarreNonEntier.length <= 1) {
        tabCarreNonEntier = [...tabCarreNonEntierInit]
        tabCarreRacine = [...tabCarreRacineInit]
      }
    }

    me.logIfDebug('tabValeurs:', tabValeurs, '   tabRacine:', tabRacine)
    return { valeursInit: tabValeurs, borne: tabRacine }
  }

  function choixValeursCarree (nb) {
    const tabValeurs = []; const tabCarre = []
    let pioche; let pioche2; let i
    let tabChoix = [2, 3, 4, 5, 6, 7, 8, 9, 10]
    let choixSigne = 1
    for (i = 0; i < nb; i++) {
      if (tabChoix.length <= 1) {
        tabChoix = [2, 3, 4, 5, 6, 7, 8, 9, 10]
      }
      do {
        pioche = j3pGetRandomInt(0, (tabChoix.length - 1))
        pioche2 = j3pGetRandomInt(0, (tabChoix.length - 1))
      } while (Math.abs(pioche - pioche2) < Math.pow(10, -12))
      const numMin = (tabChoix[pioche] < tabChoix[pioche2]) ? pioche : pioche2
      const numMax = (tabChoix[pioche] < tabChoix[pioche2]) ? pioche2 : pioche
      // LEs valeurs peuvent être négatives ou positives
      if (i > 0) {
        choixSigne = (choixSigne + 1) % 2
        if (choixSigne) {
          tabValeurs.push([-tabChoix[numMax], -tabChoix[numMin]])
          tabCarre.push([Math.pow(tabChoix[numMax], 2), Math.pow(tabChoix[numMin], 2)])
        } else {
          tabValeurs.push([tabChoix[numMin], tabChoix[numMax]])
          tabCarre.push([Math.pow(tabChoix[numMin], 2), Math.pow(tabChoix[numMax], 2)])
        }
      } else {
        tabValeurs.push([tabChoix[numMin], tabChoix[numMax]])
        tabCarre.push([Math.pow(tabChoix[numMin], 2), Math.pow(tabChoix[numMax], 2)])
      }

      tabChoix.splice(pioche, 1)
      tabChoix.splice(pioche2, 1)
    }
    return { valeursInit: tabValeurs, borne: tabCarre }// borne ne contient que la borne important dans la réponse
  }

  function choixValeursCube (nb) {
    const tabValeurs = []; const tabCube = []
    let pioche; let pioche2; let i
    let tabChoix = [1, 2, 3, 4, 5]
    let choixSignes = [j3pGetRandomInt(0, 1), j3pGetRandomInt(0, 1)]
    for (i = 0; i < nb; i++) {
      if (tabChoix.length <= 1) {
        tabChoix = [1, 2, 3, 4, 5]
      }
      do {
        pioche = j3pGetRandomInt(0, (tabChoix.length - 1))
        pioche2 = j3pGetRandomInt(0, (tabChoix.length - 1))
      } while (Math.abs(pioche - pioche2) < Math.pow(10, -12))
      const numMin = (tabChoix[pioche] < tabChoix[pioche2]) ? pioche : pioche2
      const numMax = (tabChoix[pioche] < tabChoix[pioche2]) ? pioche2 : pioche
      if (i > 1) {
        if (i % 2 === 0) {
          for (let k = 0; k <= 1; k++) {
            choixSignes[k] = (choixSignes[k] + 1) % 2
          }
        } else {
          choixSignes = [j3pGetRandomInt(0, 1), j3pGetRandomInt(0, 1)]
        }
        if (choixSignes[0]) {
          if (choixSignes[1]) {
            tabValeurs.push([-tabChoix[numMax], -tabChoix[numMin]])
            tabCube.push([-Math.pow(tabChoix[numMax], 3), -Math.pow(tabChoix[numMin], 3)])
          } else {
            tabValeurs.push([-tabChoix[numMin], tabChoix[numMax]])
            tabCube.push([-Math.pow(tabChoix[numMin], 3), Math.pow(tabChoix[numMax], 3)])
          }
        } else {
          if (choixSignes[1]) {
            tabValeurs.push([-tabChoix[numMax], tabChoix[numMin]])
            tabCube.push([-Math.pow(tabChoix[numMax], 3), Math.pow(tabChoix[numMin], 3)])
          } else {
            tabValeurs.push([tabChoix[numMin], tabChoix[numMax]])
            tabCube.push([Math.pow(tabChoix[numMin], 3), Math.pow(tabChoix[numMax], 3)])
          }
        }
      } else {
        tabValeurs.push([tabChoix[numMin], tabChoix[numMax]])
        tabCube.push([Math.pow(tabChoix[numMin], 3), Math.pow(tabChoix[numMax], 3)])
      }
      tabChoix.splice(pioche, 1)
      tabChoix.splice(pioche2, 1)
    }
    return { valeursInit: tabValeurs, borne: tabCube }// borne ne contient que la borne important dans la réponse
  }

  function getDonnees () {
    return {
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 5,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      typeFonction: 'carree',
      /*
      Les textes présents dans la section
      Sont donc accessibles dans le code par donneesSection.textes.consigne1
      Possibilité d’écrire dans le code :
      var lestextes: donneesSection.textes;
      puis accès à l’aide de lestextes.consigne1
      */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo1: 'Encadrement et fonction carrée',
        titre_exo2: 'Encadrement et fonction inverse',
        titre_exo3: 'Encadrement et fonction racine carrée',
        titre_exo4: 'Encadrement et fonction cube',
        // on donne les phrases de la consigne
        consigne1: 'Donner le meilleur encadrement possible de $£f$ lorsque $x$ vérifie $£e$.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Il ne faut pas utiliser de valeur approchée&nbsp;!',
        comment2: 'Il est tout de même préférable de simplifier les nombres quand on le peut&nbsp;!',
        comment3: 'La fonction à encadrer doit être présente dans la réponse&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'On peut s’aider de la courbe représentative de la fonction carrée.',
        corr1_2: 'On peut s’aider de la courbe représentative de la fonction inverse.',
        corr1_3: 'On peut s’aider de la courbe représentative de la fonction racine carrée.',
        corr1_4: 'On peut s’aider de la courbe représentative de la fonction cube.',
        corr2_1: 'La fonction carrée étant strictement croissante sur l’intervalle $[0;+\\infty[$, on obtient&nbsp;:<br/>$£s$',
        corr2_12: 'La fonction carrée étant strictement décroissante sur l’intervalle $]-\\infty;0]$, on obtient&nbsp;:<br/>$£s$',
        corr2_2: 'La fonction inverse étant strictement décroissante sur $£i$, on obtient&nbsp;:<br/>$£s$',
        corr2_3: 'La fonction racine carrée étant strictement croissante sur $[0;+\\infty[$, on obtient&nbsp;:<br/>$£s$',
        corr2_4: 'La fonction cube étant strictement croissante sur $\\R$, on obtient&nbsp;:<br/>$£s$',
        corr3: 'On conclut alors par l’encadrement&nbsp;:<br/>$£r$'
      },

      pe: 0
    }
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    // Pour chacune d’elles, au début, elle sont vides, d’où texte="". La largeur de 12px est juste pour qu’elles soient un peu plus large au départ
    const objVar = {}
    objVar.f = (ds.typeFonction === 'carree')
      ? 'x^2'
      : (ds.typeFonction === 'inverse')
          ? '\\frac{1}{x}'
          : (ds.typeFonction === 'racine carree') ? '\\sqrt{x}' : 'x^3'
    // Création de l’encadrement initial
    objVar.e = stor.donnees.valeursInit[me.questionCourante - 1][0] + stor.signeIneq[me.questionCourante - 1] + ' x' + stor.signeIneq[me.questionCourante - 1] + stor.donnees.valeursInit[me.questionCourante - 1][1]
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, objVar)

    const elt = j3pAffiche(stor.zoneCons2, '', '&1&',
      {
        inputmq1: { texte: '' }
      })
    stor.zoneInput = elt.inputmqList[0]
    const listePalette = ((ds.typeFonction === 'carree') || (ds.typeFonction.indexOf('racine') > -1))
      ? ['fraction', 'puissance', 'racine', 'infegal', 'supegal']
      : ['fraction', 'puissance', 'infegal', 'supegal']
    stor.laPalette = j3pAddElt(stor.zoneCons3, 'div', '', { style: { paddingTop: '10px' } })
    j3pPaletteMathquill(stor.laPalette, stor.zoneInput, {
      liste: listePalette
    })
    stor.zoneInput.typeReponse = ['texte']
    // Je crée l’ensemble solution :
    let rep
    let borne0, borne1
    let signeIneq
    if (ds.typeFonction === 'carree') {
      // liste des encadrements qui sont attendus dans la réponse
      const positif = (String(j3pCalculValeur(stor.donnees.valeursInit[me.questionCourante - 1][0]))[0] !== '-')
      if (positif) {
        rep = (stor.signeIneq[me.questionCourante - 1] === '\\leq')
          ? [stor.donnees.borne[me.questionCourante - 1][0] + '\\le x^2\\le' + stor.donnees.borne[me.questionCourante - 1][1],
              stor.donnees.borne[me.questionCourante - 1][1] + '\\ge x^2\\ge' + stor.donnees.borne[me.questionCourante - 1][0]]
          : [stor.donnees.borne[me.questionCourante - 1][0] + '<x^2<' + stor.donnees.borne[me.questionCourante - 1][1],
              stor.donnees.borne[me.questionCourante - 1][1] + '>x^2>' + stor.donnees.borne[me.questionCourante - 1][0]]
        signeIneq = stor.signeIneq[me.questionCourante - 1]
      } else {
        rep = (stor.signeIneq[me.questionCourante - 1] === '\\leq')
          ? [stor.donnees.borne[me.questionCourante - 1][1] + '\\le x^2\\le' + stor.donnees.borne[me.questionCourante - 1][0],
              stor.donnees.borne[me.questionCourante - 1][0] + '\\ge x^2\\ge' + stor.donnees.borne[me.questionCourante - 1][1]]
          : [stor.donnees.borne[me.questionCourante - 1][1] + '<x^2<' + stor.donnees.borne[me.questionCourante - 1][0],
              stor.donnees.borne[me.questionCourante - 1][0] + '>x^2>' + stor.donnees.borne[me.questionCourante - 1][1]]
        signeIneq = (stor.signeIneq[me.questionCourante - 1] === '\\leq') ? '\\geq' : '>'
      }
      if (stor.donnees.valeursInit[me.questionCourante - 1][0] > 0) {
        stor.encadreInit = stor.donnees.valeursInit[me.questionCourante - 1][0] + '^2' + signeIneq + ' x^2' + signeIneq + ' ' + stor.donnees.valeursInit[me.questionCourante - 1][1] + '^2'
      } else {
        stor.encadreInit = '(' + stor.donnees.valeursInit[me.questionCourante - 1][0] + ')^2' + signeIneq + ' x^2' + signeIneq + ' (' + stor.donnees.valeursInit[me.questionCourante - 1][1] + ')^2'
      }
      stor.positif = positif
      stor.encadrement = [...rep]
    } else if (ds.typeFonction === 'inverse') {
      rep = (stor.signeIneq[me.questionCourante - 1] === '\\leq')
        ? [stor.donnees.borne[me.questionCourante - 1][1] + '\\le\\frac{1}{x}\\le' + stor.donnees.borne[me.questionCourante - 1][0],
            stor.donnees.borne[me.questionCourante - 1][0] + '\\ge\\frac{1}{x}\\ge' + stor.donnees.borne[me.questionCourante - 1][1]]
        : [stor.donnees.borne[me.questionCourante - 1][1] + '<\\frac{1}{x}<' + stor.donnees.borne[me.questionCourante - 1][0],
            stor.donnees.borne[me.questionCourante - 1][0] + '>\\frac{1}{x}>' + stor.donnees.borne[me.questionCourante - 1][1]]
      // Même encadrement mais cette fois-ci avec des bornes arronies lorsque c’est nécessaire
      borne0 = Math.round(Math.pow(10, 10) * j3pCalculValeur(stor.donnees.borne[me.questionCourante - 1][0])) / Math.pow(10, 10)
      borne1 = Math.round(Math.pow(10, 10) * j3pCalculValeur(stor.donnees.borne[me.questionCourante - 1][1])) / Math.pow(10, 10)
      stor.encadrement = (stor.signeIneq[me.questionCourante - 1] === '\\leq')
        ? [borne1 + '\\le\\frac{1}{x}\\le' + borne0,
            borne0 + '\\ge\\frac{1}{x}\\ge' + borne1]
        : [borne1 + '<\\frac{1}{x}<' + borne0,
            borne0 + '>\\frac{1}{x}>' + borne1]
      signeIneq = (stor.signeIneq[me.questionCourante - 1] === '\\leq') ? '\\geq' : '>'
      stor.encadreInit = '\\frac{1}{' + stor.donnees.valeursInit[me.questionCourante - 1][0] + '}' + signeIneq + ' \\frac{1}{x}' + signeIneq + ' \\frac{1}{' + stor.donnees.valeursInit[me.questionCourante - 1][1] + '}'
      stor.domaine = (j3pCalculValeur(stor.donnees.valeursInit[me.questionCourante - 1][0]) < 0)
        ? ']-\\infty;0['
        : ']0;+\\infty['
    } else if (ds.typeFonction === 'racine carree') {
      rep = (stor.signeIneq[me.questionCourante - 1] === '\\leq')
        ? [stor.donnees.borne[me.questionCourante - 1][0] + '\\le\\sqrt{x}\\le' + stor.donnees.borne[me.questionCourante - 1][1],
            stor.donnees.borne[me.questionCourante - 1][1] + '\\ge\\sqrt{x}\\ge' + stor.donnees.borne[me.questionCourante - 1][0]]
        : [stor.donnees.borne[me.questionCourante - 1][0] + '<\\sqrt{x}<' + stor.donnees.borne[me.questionCourante - 1][1],
            stor.donnees.borne[me.questionCourante - 1][1] + '>\\sqrt{x}>' + stor.donnees.borne[me.questionCourante - 1][0]]
      borne0 = j3pCalculValeur(stor.donnees.borne[me.questionCourante - 1][0])
      borne1 = j3pCalculValeur(stor.donnees.borne[me.questionCourante - 1][1])
      // Même encadrement mais cette fois-ci avec des bornes arrondies lorsque c’est nécessaire
      stor.encadrement = (stor.signeIneq[me.questionCourante - 1] === '\\leq')
        ? [borne0 + '\\le\\sqrt{x}\\le' + borne1,
            borne1 + '\\ge\\sqrt{x}\\ge' + borne0]
        : [borne0 + '<\\sqrt{x}<' + borne1,
            borne1 + '>\\sqrt{x}>' + borne0]
      signeIneq = stor.signeIneq[me.questionCourante - 1]
      stor.encadreInit = '\\sqrt{' + stor.donnees.valeursInit[me.questionCourante - 1][0] + '}' + signeIneq + ' \\sqrt{x}' + signeIneq + ' \\sqrt{' + stor.donnees.valeursInit[me.questionCourante - 1][1] + '}'
    } else if (ds.typeFonction === 'cube') {
      rep = (stor.signeIneq[me.questionCourante - 1] === '\\leq')
        ? [stor.donnees.borne[me.questionCourante - 1][0] + '\\le x^3\\le' + stor.donnees.borne[me.questionCourante - 1][1],
            stor.donnees.borne[me.questionCourante - 1][1] + '\\ge x^3\\ge' + stor.donnees.borne[me.questionCourante - 1][0]]
        : [stor.donnees.borne[me.questionCourante - 1][0] + '<x^3<' + stor.donnees.borne[me.questionCourante - 1][1],
            stor.donnees.borne[me.questionCourante - 1][1] + '>x^3>' + stor.donnees.borne[me.questionCourante - 1][0]]
      stor.encadrement = [...rep]
      signeIneq = stor.signeIneq[me.questionCourante - 1]
      stor.encadreInit = stor.donnees.valeursInit[me.questionCourante - 1][0] + '^3' + signeIneq + ' x^3' + signeIneq + ' ' + stor.donnees.valeursInit[me.questionCourante - 1][1] + '^3'
    }
    stor.zoneInput.reponse = [...rep]
    if (ds.typeFonction === 'carree') {
      mqRestriction(stor.zoneInput, '\\d.,x\\^\\/+\\-<>²', { commandes: listePalette })
    } else {
      mqRestriction(stor.zoneInput, '\\d.,x\\^\\/+\\-<>', { commandes: listePalette })
    }
    j3pFocus(stor.zoneInput)

    me.logIfDebug('rep :', stor.zoneInput.reponse)

    stor.objVar = objVar
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCorrTxt = j3pAddElt(stor.zoneCorr, 'div')
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.55 })

        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // Je génère les données ici
        if (ds.typeFonction === 'carree') {
          stor.donnees = choixValeursCarree(ds.nbrepetitions, !ds.sansRadical)
          me.afficheTitre(ds.textes.titre_exo1)
        } else if (ds.typeFonction === 'inverse') {
          stor.donnees = choixValeursInverse(ds.nbrepetitions)
          me.afficheTitre(ds.textes.titre_exo2)
        } else if (ds.typeFonction === 'racine carree') {
          stor.donnees = choixValeursRacine(ds.nbrepetitions)
          me.afficheTitre(ds.textes.titre_exo3)
        } else if (ds.typeFonction === 'cube') {
          stor.donnees = choixValeursCube(ds.nbrepetitions)
          me.afficheTitre(ds.textes.titre_exo4)
        }
        // signe dans l’inégalité
        const signeIneq = [1, -1]
        let choixSigne = []
        const avecEgal = []
        for (let i = 1; i <= ds.nbrepetitions / 2; i++) {
          choixSigne = choixSigne.concat(signeIneq)
          avecEgal.push(false, true)
        }
        if (ds.nbrepetitions % 2 !== 0) {
          choixSigne.push(signeIneq[j3pGetRandomInt(0, 1)])
          const choix = j3pGetRandomInt(0, 1) !== 0
          avecEgal.push(choix)
        }
        const derangeEgal = j3pShuffle(avecEgal)
        stor.signeIneq = []
        for (let i = 0; i < ds.nbrepetitions; i++) {
          const leSigne = (derangeEgal[i]) ? '\\leq' : '<'
          stor.signeIneq.push(leSigne)
        }
        me.logIfDebug('donnees:', stor.donnees)
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        let nbsReduits = true
        let fctPresente
        if (reponse.aRepondu) {
          let repEleve = stor.fctsValid.zones.reponseSaisie[0]
          // On vérifie que l’expression à encadrer est bien dans la réponse de l’élève
          fctPresente = (repEleve.indexOf(stor.objVar.f) > -1)
          if (fctPresente) {
            reponse.bonneReponse = ((repEleve === stor.zoneInput.reponse[0]) || (repEleve === stor.zoneInput.reponse[1]))
            if (!reponse.bonneReponse) {
            // j’identifie tout nombre de la forme sqrt{...} ou frac{..}{..} ou a^b
              const FracRegExp = /\\frac{-?[0-9]+([,.][0-9]+)?}{-?[0-9]+([,.][0-9]+)?}/g
              const sqrtRegExp = /\\sqrt{[0-9]+([,.][0-9]+)?}/g
              const puissance = /[0-9]+\^[0-9]+/g
              let nbTests = 0
              while ((repEleve.indexOf('sqrt') > -1 || repEleve.indexOf('frac') > -1) && (nbTests < 10)) {
                if (FracRegExp.test(repEleve)) {
                  repEleve = repEleve.replace(FracRegExp, function (x) {
                    nbsReduits = irreductibleFrac(x)
                    const valeur = j3pCalculValeur(j3pMathquillXcas(x))
                    if (Math.abs(Math.round(valeur) - valeur) < Math.pow(10, -12)) {
                      nbsReduits = false// c’est qu’il a écrit une fraction alors qu’on pouvait la réduire sous la forme d’un entier
                    }
                    return valeur
                  })
                }
                if (sqrtRegExp.test(repEleve)) {
                  repEleve = repEleve.replace(sqrtRegExp, function (x) {
                    const valeur = j3pCalculValeur(j3pMathquillXcas(x))
                    if (Math.abs(Math.round(valeur) - valeur) < Math.pow(10, -12)) {
                      nbsReduits = false// c’est qu’il a écrit ce nombre avec un radical alors qu’on pouvait le réduire sous la forme d’un entier
                    }
                    return valeur
                  })
                }
                nbTests++
              }
              // Maintenant, je m’occupe des puissances restantes
              while (puissance.test(repEleve)) {
                repEleve = repEleve.replace(puissance, function (x) {
                  nbsReduits = false
                  return j3pCalculValeur(j3pMathquillXcas(x))
                })
              }

              // J’arrondis enfin tous les décimaux à 10^{-10} pour comparer avec ma solution
              const decRegExp = /[0-9]+[,.][0-9]+/g
              if (decRegExp.test(repEleve)) {
                repEleve = repEleve.replace(decRegExp, function (x) {
                  return Math.round(Math.pow(10, 10) * j3pNombre(x)) / Math.pow(10, 10)
                })
              }
              // Je vire enfin les signes +inutiles
              repEleve = repEleve.replace(/([^0-9])\+([0-9])/g, '$1$2')

              me.logIfDebug('repEleve après decimal:', repEleve)
              // console.log("encadrement avec décimaux :", stor.encadrement)
              // Je vérifie maintenant si j’ai bien l’encadrement attendu
              let bonEncadrement = false
              for (let i = 0; i < stor.encadrement.length; i++) {
                bonEncadrement = (bonEncadrement || (repEleve === stor.encadrement[i]))
              }
              reponse.bonneReponse = bonEncadrement
            }
          } else {
            reponse.bonneReponse = false
          }

          if (!reponse.bonneReponse) {
            stor.fctsValid.zones.bonneReponse[0] = false
          }
          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          stor.fctsValid.coloreUneZone(stor.zoneInput.id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorrTxt)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorrTxt.style.color = me.styles.cbien
            stor.zoneCorrTxt.innerHTML = cBien
            if (!nbsReduits) stor.zoneCorrTxt.innerHTML += '<br/>' + ds.textes.comment2
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorr(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorrTxt.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorrTxt.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorrTxt.innerHTML = cFaux
              if (!fctPresente) {
                stor.zoneCorrTxt.innerHTML += '<br/>' + ds.textes.comment3
              } else if (!nbsReduits) {
                stor.zoneCorrTxt.innerHTML += '<br/>' + ds.textes.comment2
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorrTxt.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorrTxt.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
        // Obligatoire
        me.finCorrection()
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
