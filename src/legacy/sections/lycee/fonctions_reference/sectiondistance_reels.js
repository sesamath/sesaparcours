import { j3pAddElt, j3pNombre, j3pVirgule, j3pDetruit, j3pFocus, j3pFractionLatex, j3pGetRandomInt, j3pPaletteMathquill, j3pPGCD } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pGetLatexMonome, j3pGetLatexOppose, j3pGetLatexSomme } from 'src/legacy/core/functionsLatex'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    avril 2016
    Dans cette section, on demande de calculer la distance entre deux nombres réels
    C’est proposé sous la forme d’une phrase ou de la valeur absolue de la différence

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['type_quest', 1, 'entier', 'Deux façons de poser la question :n- 1 : sous la forme d’une phrase ;\n2 : en demandant la valeur absolue de la différence.'],
    ['imposer_nombres', ['', '', ''], 'array', 'Pour chaque répétition, on peut imposer les deux nombres entre guillemets et séparés par |, sous la forme : ["2|racine(3)","3pi|10", "10^-2|0.2".']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function convertNbs (nb) {
    let newNb = String(nb)
    const racineRegexp = /racine\([\d.]+\)/g // new RegExp('racine\\([0-9\\.]{1,}\\)', 'gi')
    const puissanceRegexp = /\^\(?-?\d+\)?/gi // new RegExp('\\^\\(?\\-?[0-9]{1,}\\)?', 'gi')
    const piRegexp = /pi/gi // new RegExp('pi', 'gi') // dans un premier temps je remplacerai pi par 'grec', c’est juste pour réussir à mettre \\pi en remplacement
    const pilatexRegexp = /grec/gi // new RegExp('grec', 'gi') // je remplacerai le mot grec par \\pi
    let j
    if (racineRegexp.test(newNb)) {
      const tabRacine = newNb.match(racineRegexp)
      for (j = 0; j < tabRacine.length; j++) {
        // je dois récupérer le radical
        const radical = tabRacine[j].substring(7, tabRacine[j].length - 1)
        newNb = newNb.replace(tabRacine[j], '\\sqrt{' + radical + '}')
      }
    }
    if (puissanceRegexp.test(newNb)) {
      const tabPuis = newNb.match(puissanceRegexp)
      for (j = 0; j < tabPuis.length; j++) {
        // je dois récupérer le radical
        let puissance = tabPuis[j].substring(1)
        puissance = (puissance.charAt(0) === '(') ? puissance.substring(1) : puissance
        puissance = (puissance.charAt(puissance.length - 1) === ')') ? puissance.substring(0, puissance.length - 1) : puissance
        newNb = newNb.replace(tabPuis[j], '^{' + puissance + '}')
      }
    }
    while (piRegexp.test(newNb)) {
      newNb = newNb.replace('pi', 'grec')
    }
    while (pilatexRegexp.test(newNb)) {
      newNb = newNb.replace('grec', '\\pi')
    }
    while (newNb.indexOf('*') > -1) {
      newNb = newNb.replace('*', '\\times ')
    }
    newNb = j3pFractionLatex(newNb)
    return newNb
  }

  function choixRadicalNonEntier (bornes) {
    // bornes est un objet qui contient les propriétés inf et sup
    let radical
    do {
      radical = j3pGetRandomInt(bornes.inf, bornes.sup)
    } while (Math.abs(Math.round(Math.sqrt(radical)) - Math.sqrt(radical)) < Math.pow(10, -12))
    return radical
  }

  function ecrireDifference (nb1, nb2) {
    // nb1 et nb2 sont des nombres écrits au format latex
    // on vérifie si la différence est un nombre décimal et si c’est le cas, on renvoit le résultat de la différence
    // cette fonction renvoie un objet nommé obj
    // si les deux nombres sont rationnels, elle revoit la propriété difference où on a le résultat sous forme rationnelle
    // sinon, on renvoit comme résultat le calcul de la différence
    // par ailleurs l’objet contient la propriété simplif qui vaudra true si la différence a bien été simplifiée
    function estfraction (nb) {
      const fractionRegexp = /-?\\frac{-?\d+}{\d+}/g // new RegExp('\\-?\\\\frac\\{\\-?[0-9]{1,}\\}\\{[0-9]{1,}\\}', 'gi')
      let estfracnb = false
      if (fractionRegexp.test(String(nb))) {
        // cette fonction vérifie si nb est une fraction écrite au format latex
        // du coup, est-ce seulement une fraction ?
        const tabFrac = String(nb).match(fractionRegexp)
        estfracnb = ((tabFrac.length === 1) && (String(nb).replace(tabFrac[0], '') === ''))
      }
      return estfracnb
    }

    const obj = {}
    if (!isNaN(j3pNombre(String(nb1))) && !isNaN(j3pNombre(String(nb2)))) {
      // nb1 et nb2 sont des nombres décimaux
      obj.difference = j3pVirgule(Math.round((j3pNombre(String(nb1)) - j3pNombre(String(nb2))) * Math.pow(10, 5)) / Math.pow(10, 5))
      obj.simplif = true
    } else if (!isNaN(j3pNombre(String(nb1))) && estfraction(nb2)) {
      // nb1 est décimal et nb2 est une fraction
      obj.difference = j3pGetLatexSomme(nb1, j3pGetLatexOppose(nb2))
      obj.simplif = true
    } else if (!isNaN(j3pNombre(String(nb2))) && estfraction(nb1)) {
      // nb2 est décimal et nb1 est une fraction
      obj.difference = j3pGetLatexSomme(nb1, j3pGetLatexOppose(nb2))
      obj.simplif = true
    } else if (estfraction(nb1) && estfraction(nb2)) {
      // nb1 et nb2 sont des fractions
      obj.difference = j3pGetLatexSomme(nb1, j3pGetLatexOppose(nb2))
      obj.simplif = true
    } else {
      obj.difference = (String(nb2).charAt(0) === '-') ? String(nb1) + '+' + String(nb2).substring(1) : String(nb1) + '-' + String(nb2)
      obj.simplif = false
    }
    return obj
  }

  function ChoixFracIrre () {
    // cette fonction choisit une fraction irréductible
    // elle renvoit un objet nommé obj
    // obj.decimal est sa valeur approchée au format décimal (cela évite de refaire le calcul)
    // obj.latex est son écriture au format latex
    // obj.num et obj.den renvoient respectivement le numérateur et le dénominateur
    const obj = {}
    let num1, den1
    do {
      num1 = j3pGetRandomInt(1, 8)
      den1 = j3pGetRandomInt(3, 7)
    } while (Math.abs(Math.round(num1 * Math.pow(10, 1) / den1) / Math.pow(10, 1) - num1 / den1) < Math.pow(10, -12))
    // je n’autorise pas un seul chiffre après la virgule dans l’écriture décimale, mais plus c’est possible
    const pgcd = j3pPGCD(num1, den1)
    num1 = num1 / pgcd
    den1 = den1 / pgcd
    obj.decimal = num1 / den1
    obj.latex = '\\frac{' + num1 + '}{' + den1 + '}'
    obj.num = num1
    obj.den = den1
    return obj
  }

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCons3)
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color } })
    // le div le_nom+"explications" contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    const zoneExpli1 = j3pAddElt(zoneExpli, 'p')
    const difference = (String(stor.nb2).charAt(0) === '-') ? stor.nb1 + '-(' + stor.nb2 + ')' : stor.nb1 + '-' + stor.nb2
    const objDiffSimplifiee = ecrireDifference(stor.nb1, stor.nb2)
    let egalite2
    if (ds.type_quest === 1) {
      // corr1 : 'La distance entre $£a$ et $£b$ s’écrit $\\left|£d\\right|£e$.',
      if (objDiffSimplifiee.simplif) {
        egalite2 = '=\\left|' + objDiffSimplifiee.difference + '\\right|'
      } else {
        egalite2 = (String(stor.nb2).charAt(0) === '-') ? '=\\left|' + objDiffSimplifiee.difference + '\\right|' : ''
      }
      j3pAffiche(zoneExpli1, '', ds.textes.corr1,
        {
          a: stor.nb1,
          b: stor.nb2,
          d: difference,
          e: egalite2
        })
    } else {
      if (objDiffSimplifiee.simplif) {
        // Dans ce cas doit apparaître une phrase avec l’expression sous forme de valeur absolue simplifiée
        j3pAffiche(zoneExpli1, '', '$\\left|£d\\right|=\\left|£e\\right|$',
          { d: stor.difference, e: objDiffSimplifiee.difference })
      }
    }
    const laCorrection2 = (stor.valnb1 - stor.valnb2 > 0) ? ds.textes.corr2 : ds.textes.corr2bis
    // dans explication1, on a l’explication correspondant à la première question

    const zoneExpli2 = j3pAddElt(zoneExpli, 'p')
    j3pAffiche(zoneExpli2, '', laCorrection2, {
      d: objDiffSimplifiee.difference,
      e: stor.ecrire_reponse
    })
    // dans explication2, ce qui correspond à la deuxième question
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      type_quest: 1,
      imposer_nombres: ['', '', ''],

      /*
      Les textes présents dans la section
      Sont donc accessibles dans le code par donneesSection.textes.consigne1
      Possibilité d’écrire dans le code :
      var lestextes: donneesSection.textes;
      puis accès à l’aide de lestextes.consigne1
      */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Calculer la distance entre deux réels',
        // on donne les phrases de la consigne
        consigne1: 'Soient $a=£a$ et $b=£b$.',
        consigne1bis: 'Calcule l’expression suivante :',
        consigne2: 'La distance entre les réels $a$ et $b$ vaut : &1&.',
        consigne2bis: '$\\left|£d\\right|=$&1&',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: '',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'La distance entre $£a$ et $£b$ s’écrit $\\left|£d\\right|£e$.',
        corr2: 'Comme $£d>0$, $\\left|£d\\right|=£e$',
        corr2bis: 'Comme $£d<0$, $\\left|£d\\right|=-\\left(£d\\right)=£e$'
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      /*
      Phrase d’état renvoyée par la section
      Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
      Dans le cas où la section est qualitative, lister les différents pe renvoyées :
          pe_1: "toto"
          pe_2: "tata"
          etc
      Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
      Par exemple,
      parcours.pe: donneesSection.pe_2
      */
      pe: 0
    }
  }

  function enonceMain () {
    let num0
    // là il va falloir gérer le cas où les nombres ne sont pas imposés
    if (stor.nb_alea.length === 0) {
      // tableau des choix aléatoires
      const tabNbsAlea = []
      for (let i = 0; i <= 8; i++) tabNbsAlea[i] = [] // chaque tableau contiendra deux nombres
      // avec des décimaux
      tabNbsAlea[0][0] = j3pGetRandomInt(15, 55) / 10
      do {
        tabNbsAlea[0][1] = -j3pGetRandomInt(15, 55) / 10
      } while (Math.abs(tabNbsAlea[0][0] + tabNbsAlea[0][1]) < Math.pow(10, -12))
      tabNbsAlea[0][2] = tabNbsAlea[0][0]
      tabNbsAlea[0][3] = tabNbsAlea[0][1]
      tabNbsAlea[0][4] = Math.abs(tabNbsAlea[0][2] - tabNbsAlea[0][3])
      tabNbsAlea[0][5] = Math.round(tabNbsAlea[0][4] * Math.pow(10, 5)) / Math.pow(10, 5)
      // avec des entiers
      tabNbsAlea[1][0] = j3pGetRandomInt(3, 9)
      do {
        tabNbsAlea[1][1] = -j3pGetRandomInt(3, 9)
      } while (Math.abs(tabNbsAlea[1][0] + tabNbsAlea[1][1]) < Math.pow(10, -12))
      tabNbsAlea[1][2] = tabNbsAlea[1][0]
      tabNbsAlea[1][3] = tabNbsAlea[1][1]
      tabNbsAlea[1][4] = Math.abs(tabNbsAlea[1][2] - tabNbsAlea[1][3])
      tabNbsAlea[1][5] = Math.round(tabNbsAlea[1][4] * Math.pow(10, 5)) / Math.pow(10, 5)
      // avec pi
      const coefPi = j3pGetRandomInt(1, 4)
      tabNbsAlea[2][0] = j3pGetLatexMonome(1, 1, coefPi, '\\pi')
      tabNbsAlea[2][1] = Math.floor(coefPi * Math.PI) + j3pGetRandomInt(0, 1)
      tabNbsAlea[2][2] = coefPi * Math.PI
      tabNbsAlea[2][3] = tabNbsAlea[2][1]
      tabNbsAlea[2][4] = Math.abs(tabNbsAlea[2][2] - tabNbsAlea[2][3])
      tabNbsAlea[2][5] = (tabNbsAlea[2][2] > tabNbsAlea[2][3]) ? j3pGetLatexMonome(1, 1, coefPi, '\\pi') + '-' + String(tabNbsAlea[2][1]) : String(tabNbsAlea[2][1]) + j3pGetLatexMonome(2, 1, -coefPi, '\\pi')
      // avec un radical
      do {
        num0 = choixRadicalNonEntier({ inf: 2, sup: 15 })
      } while (num0 % 4 === 0)
      tabNbsAlea[3][0] = '\\sqrt{' + num0 + '}'
      tabNbsAlea[3][1] = Math.floor(Math.sqrt(num0)) + j3pGetRandomInt(0, 1)
      tabNbsAlea[3][2] = Math.sqrt(num0)
      tabNbsAlea[3][3] = tabNbsAlea[3][1]
      tabNbsAlea[3][4] = Math.abs(tabNbsAlea[3][2] - tabNbsAlea[3][3])
      tabNbsAlea[3][5] = (tabNbsAlea[3][2] > tabNbsAlea[3][3]) ? '\\sqrt{' + num0 + '}-' + String(tabNbsAlea[3][1]) : String(tabNbsAlea[3][1]) + '-\\sqrt{' + num0 + '}'
      // avec deux radicaux, mais j'évite que l’un d’eux soit simplifiable (donc j’exclu 12, 8, 4
      let num1, num2
      do {
        num1 = choixRadicalNonEntier({ inf: 2, sup: 15 })
      } while (num1 % 4 === 0)
      do {
        num2 = choixRadicalNonEntier({ inf: 2, sup: 15 })
      } while (Math.abs(num1 - num2) < 1e-12 || (num2 % 4 === 0))
      tabNbsAlea[4][0] = '\\sqrt{' + num1 + '}'
      tabNbsAlea[4][1] = '-\\sqrt{' + num2 + '}'
      tabNbsAlea[4][2] = Math.sqrt(num1)
      tabNbsAlea[4][3] = -Math.sqrt(num2)
      tabNbsAlea[4][4] = Math.abs(tabNbsAlea[4][2] - tabNbsAlea[4][3])
      tabNbsAlea[4][5] = '\\sqrt{' + num1 + '}+\\sqrt{' + num2 + '}'
      // puissance de 10 négative
      const puis10Neg = j3pGetRandomInt(-6, -3)
      tabNbsAlea[5][0] = '10^{' + puis10Neg + '}'
      tabNbsAlea[5][1] = '10^{' + (puis10Neg + 1) + '}'
      tabNbsAlea[5][2] = Math.pow(10, puis10Neg)
      tabNbsAlea[5][3] = Math.pow(10, puis10Neg + 1)
      tabNbsAlea[5][4] = Math.abs(tabNbsAlea[5][2] - tabNbsAlea[5][3])
      tabNbsAlea[5][5] = '10^{' + (puis10Neg + 1) + '}-10^{' + (puis10Neg) + '}'
      // puissance de 10 positive
      const puis10Pos = j3pGetRandomInt(2, 4)
      const coefDevant = j3pGetRandomInt(3, 9)
      tabNbsAlea[6][0] = coefDevant + '\\times 10^{' + puis10Pos + '}'
      tabNbsAlea[6][1] = '10^{' + (puis10Pos + 1) + '}'
      tabNbsAlea[6][2] = coefDevant * Math.pow(10, puis10Pos)
      tabNbsAlea[6][3] = Math.pow(10, puis10Pos + 1)
      tabNbsAlea[6][4] = Math.abs(tabNbsAlea[6][2] - tabNbsAlea[6][3])
      tabNbsAlea[6][5] = '10^{' + (puis10Pos + 1) + '}-' + coefDevant + '\\times 10^{' + puis10Pos + '}'
      stor.nb_alea = [...tabNbsAlea]
      // fractions positives
      let fraction1, fraction2
      do {
        fraction1 = ChoixFracIrre()
        fraction2 = {
          num: fraction1.num + 1,
          den: fraction1.den - 1,
          latex: '\\frac{' + (fraction1.num + 1) + '}{' + (fraction1.den - 1) + '}',
          decimal: (fraction1.num + 1) / (fraction1.den - 1)
        }
        const pgcd2 = j3pPGCD(fraction2.num, fraction2.den)
        fraction2.num = fraction2.num / pgcd2
        fraction2.den = fraction2.den / pgcd2
      } while (Math.abs(Math.round(fraction2.num * Math.pow(10, 1) / fraction2.den) / Math.pow(10, 1) - fraction2.num / fraction2.den) < Math.pow(10, -12))
      tabNbsAlea[7][0] = fraction1.latex
      tabNbsAlea[7][1] = fraction2.latex
      tabNbsAlea[7][2] = fraction1.decimal
      tabNbsAlea[7][3] = fraction2.decimal
      tabNbsAlea[7][4] = Math.abs(tabNbsAlea[7][2] - tabNbsAlea[7][3])
      tabNbsAlea[7][5] = j3pGetLatexSomme(tabNbsAlea[7][1], j3pGetLatexOppose(tabNbsAlea[7][0]))
      do {
        fraction1 = ChoixFracIrre()
        tabNbsAlea[8][1] = Math.floor(fraction1.decimal) + j3pGetRandomInt(0, 1)
      } while (Math.abs(tabNbsAlea[8][1]) < Math.pow(10, -12))
      tabNbsAlea[8][0] = fraction1.latex
      tabNbsAlea[8][2] = fraction1.decimal
      tabNbsAlea[8][3] = tabNbsAlea[8][1]
      tabNbsAlea[8][4] = Math.abs(tabNbsAlea[8][2] - tabNbsAlea[8][3])
      tabNbsAlea[8][5] = (tabNbsAlea[8][3] > tabNbsAlea[8][2]) ? j3pGetLatexSomme(tabNbsAlea[8][1], j3pGetLatexOppose(tabNbsAlea[8][0])) : j3pGetLatexSomme(tabNbsAlea[8][0], j3pGetLatexOppose(tabNbsAlea[8][1]))
    }
    if (stor.nbs_imposes[me.questionCourante - 1][0] === '') {
      // je choisis un rang aléatoirement
      const choix = j3pGetRandomInt(0, (stor.nb_alea.length - 1))

      const nbsAlea = stor.nb_alea[choix]
      stor.nb_alea.splice(choix, 1)
      // c’est que je dois choisir les donnnées aléatoirement
      num0 = j3pGetRandomInt(0, 1)
      stor.nb1 = nbsAlea[num0]
      stor.nb2 = nbsAlea[1 - num0]
      stor.valnb1 = nbsAlea[2 + num0]
      stor.valnb2 = nbsAlea[3 - num0]
      stor.reponse = nbsAlea[4]
      stor.ecrire_reponse = nbsAlea[5]

      me.logIfDebug('stor.reponse:', stor.reponse + '   stor.ecrire_reponse:', stor.ecrire_reponse)
    } else {
      // les données sont imposées
      stor.nb1 = stor.nbs_imposes[me.questionCourante - 1][0]
      stor.nb2 = stor.nbs_imposes[me.questionCourante - 1][1]
      stor.valnb1 = stor.nbs_imposes[me.questionCourante - 1][2]
      stor.valnb2 = stor.nbs_imposes[me.questionCourante - 1][3]
      stor.reponse = stor.nbs_imposes[me.questionCourante - 1][4]
      stor.ecrire_reponse = stor.nbs_imposes[me.questionCourante - 1][5]
    }
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    if (ds.type_quest === 1) {
      // le consigne est donnée sous la forme d’une phrase
      j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1,
        {
          a: stor.nb1, b: stor.nb2
        })
      // Dans cette section, j’ai une deuxième phrase à compléter
      stor.elt = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2,
        {
          a: stor.nb1,
          b: stor.nb2,
          inputmq1: { texte: '' }
        })
    } else {
      j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1bis,
        {
          styletexte: {}
        })
      // je regarde comment écrire la différence
      if (Math.abs(stor.valnb1) < Math.pow(10, -12)) {
        stor.difference = (String(stor.nb2).charAt(0) === '-') ? String(stor.nb2).substring(1) : '-' + stor.nb2
      } else if (Math.abs(stor.valnb2) < Math.pow(10, -12)) {
        stor.difference = stor.nb
      } else {
        stor.difference = (String(stor.nb2).charAt(0) === '-') ? stor.nb1 + '+' + String(stor.nb2).substring(1) : stor.nb1 + '-' + stor.nb2
      }
      stor.elt = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2bis,
        {
          d: stor.difference,
          inputmq1: { texte: '' }
        })
    }
    stor.zoneInput = stor.elt.inputmqList[0]
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [stor.reponse]
    stor.zoneCons3.style.paddingTop = '10px'
    j3pPaletteMathquill(stor.zoneCons3, stor.zoneInput, {
      liste: ['fraction', 'racine', 'puissance', 'pi']
    })
    mqRestriction(stor.zoneInput, '\\d-+^/,.', { commandes: ['fraction', 'racine', 'puissance', 'pi'] })
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    // validePerso est souvant un tableau vide
    // Par contre on vérifiera si elles sont remplies ou non.
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie,
      validePerso: mesZonesSaisie
    })

    // Obligatoire
    me.finEnonce()
  }
  // console.log("debut du code : ",me.etat)
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // je gère ici les éventuels nombres imposés
        stor.nbs_imposes = []
        stor.valeurs_nbs = []
        let k
        for (let i = 0; i < ds.nbrepetitions; i++) {
          stor.nbs_imposes.push(['', '']) // on considère au départ qu’il n’y en a pas
          stor.valeurs_nbs[i] = []
          ds.imposer_nombres[i] = (!ds.imposer_nombres[i]) ? '' : ds.imposer_nombres[i]
          if (ds.imposer_nombres[i] !== '') {
            // on vérifie ce qu’il y a dedans
            ds.imposer_nombres[i] = (ds.imposer_nombres[i].indexOf('|') === -1) ? '' : ds.imposer_nombres[i]
            if (ds.imposer_nombres[i] !== '') {
              const nbs = ds.imposer_nombres[i].split('|')
              let deuxNombres = true // je ne peux convertir qu’un nombre sur les deux, je les considère tous les deux comme non imposés
              for (k = 0; k <= 1; k++) {
                stor.nbs_imposes[i][k] = convertNbs(nbs[k])
                // il faut que je vérifie que c’est bien un nombre
                // je peux avoir un pb avec pi qui peut ne pas être reconnu
                let nbPourCalc = stor.nbs_imposes[i][k]
                nbPourCalc = nbPourCalc.replace(/\\pi/g, '(' + Math.PI + ')')
                stor.valeurs_nbs[i][k] = j3pCalculValeur(nbPourCalc)
                stor.valeurs_nbs[i][k] = (isNaN(stor.valeurs_nbs[i][k])) ? '' : stor.valeurs_nbs[i][k]
                if (isNaN(stor.valeurs_nbs[i][k])) {
                  deuxNombres = false
                }
              }
              for (k = 0; k <= 1; k++) {
                stor.valeurs_nbs[i][k] = (deuxNombres) ? stor.valeurs_nbs[i][k] : ''
                stor.nbs_imposes[i][k] = (deuxNombres) ? stor.nbs_imposes[i][k] : ''
              }
              let ecritureDifference
              if (deuxNombres) {
                // c’est que j’ai bien mes deux nombres imposés
                stor.nbs_imposes[i][2] = stor.valeurs_nbs[i][0]
                stor.nbs_imposes[i][3] = stor.valeurs_nbs[i][1]
                stor.nbs_imposes[i][4] = Math.abs(stor.nbs_imposes[i][2] - stor.nbs_imposes[i][3])
                if (stor.nbs_imposes[i][2] - stor.nbs_imposes[i][3] > 0) {
                  ecritureDifference = (String(stor.nbs_imposes[i][1]).charAt(0) === '-') ? stor.nbs_imposes[i][0] + '+' + String(stor.nbs_imposes[i][1]).substring(1) : stor.nbs_imposes[i][0] + '-' + String(stor.nbs_imposes[i][1])
                } else {
                  ecritureDifference = (String(stor.nbs_imposes[i][0]).charAt(0) === '-') ? stor.nbs_imposes[i][1] + '+' + String(stor.nbs_imposes[i][0]).substring(1) : stor.nbs_imposes[i][1] + '-' + String(stor.nbs_imposes[i][0])
                }
                stor.nbs_imposes[i][5] = (Math.abs(Math.round(stor.nbs_imposes[i][4] * Math.pow(10, 3)) - stor.nbs_imposes[i][4] * Math.pow(10, 3)) < Math.pow(10, -12)) ? Math.round(stor.nbs_imposes[i][4] * Math.pow(10, 5)) / Math.pow(10, 5) : ecritureDifference
              }
            }
          }
        }
        stor.nb_alea = []
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si les 4 zones ont bien été complétées ou non
        // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
        if (reponse.aRepondu) {
          let reponseEleve = fctsValid.zones.reponseSaisie[0]
          reponseEleve = reponseEleve.replace(/\\pi/g, '(' + Math.PI + ')')
          me.logIfDebug('reponseEleve après :', reponseEleve, '   et la réponse:', stor.reponse)
          reponse.bonneReponse = (Math.abs(j3pCalculValeur(reponseEleve) - stor.reponse) < Math.pow(10, -12))
          if (!reponse.bonneReponse) {
            fctsValid.zones.bonneReponse[0] = false
          }
          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          fctsValid.coloreUneZone(stor.zoneInput.id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
             *   RECOPIER LA CORRECTION ICI !
             */
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
