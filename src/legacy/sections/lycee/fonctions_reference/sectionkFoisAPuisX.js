import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pRandomTab, j3pShowError, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import { afficheBloc } from 'src/legacy/core/functionsPhrase'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { mqRestriction } from 'src/lib/mathquill/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pDetruitFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Mettre ici une description de la section
 * @author DENIAUD Rémi
 * @since juillet 2022
 * @fileOverview On donne la courbe représentant x-->k*a^x et on demande d’identifier k et a
 */
// nos constantes
const structure = 'presentation1'
const ratioGauche = 0.5
// N’utiliser cette variable que si ce ratio est différent
// (utilisé dans ce cas dans me.construitStructurePage(structure, ratioGauche) de initSection()
export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['kEgal1', false, 'boolean', 'La fonction est de la forme k*a^x. Lorsque ce paramètre vaut true, alors, k vaut 1 et on ne demande que la valeur de a']
  ]
}

/**
 * La fonction principale, rappelée à chaque étape
 * Son this sera l’instance courante de Parcours
 * @this {Parcours}
 */
export default function maSection () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini (en général dans enonceEnd(me)), sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {

    // Nombre de répétitions de l’exercice
    nbrepetitions: 3,
    indication: '',
    nbchances: 2,
    kEgal1: false,
    textes: {
      // le mieux est de mettre également le texte correspondant au titre de l’exo ici
      titreExo: 'Représentation graphique d’une fonction exponentielle',
      // on donne les phrases de la consigne
      consigne1_1: 'La courbe ci-contre représente une fonction exponentielle $£f$ définie par $£f(x)=k\\times a^x$',
      consigne1_2: 'La courbe ci-contre représente une fonction exponentielle $£f$ définie par $£f(x)=a^x$',
      consigne2_1: 'En s’aidant de la courbe, donner les valeurs de $k$ et de $a$.',
      consigne2_2: 'En s’aidant de la courbe, donner la valeur de $a$.',
      consigne3: '$k=$&1& et $a=$&2&.',
      info: 'Le point de coordonnées $(0;0)$ et $(0;1)$ sont mobiles et permettent de faciliter la lecture.',
      // et les phrases utiles pour les explications de la réponse
      corr1: 'La courbe passe par le point de coordonnées $(0;£{kTxt})$ et $£f(0)=k$, ainsi $k=£{kTxt}$.',
      corr2_1: 'De plus, la courbe passe par le point de coordonnées $(1; £{ka})$ et $£f(1)=k\\times a$.',
      corr2_2: 'La courbe passe par le point de coordonnées $(1; £{aTxt})$ et $£f(1)=a$, donc $a=£{aTxt}$.',
      corr3: 'On en déduit que $a=£{aTxt}$.'
    }
  }
} // getDonnees

// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance courante de Parcours (objet this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  const stor = me.storage
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  // Construction de la page
  me.construitStructurePage({ structure, ratioGauche }) // ajouter un deuxième argument ratioGauche s’il est défini en tant que variable globale au début
  me.afficheTitre(me.donneesSection.textes.titreExo)
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // des variables qui vont me servir pour ne pas avoir toujours le même cas de figure (cas où k=1)
  me.storage.nbInf1 = 0
  me.storage.nbSup1 = 0
  // on peut faire ici du chargement asynchrone
  getMtgCore({ withMathJax: true })
    .then(
      // success
      (mtgAppLecteur) => {
        me.storage.mtgAppLecteur = mtgAppLecteur
        enonceEnd(me)
      },
      // failure
      (error) => {
        j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
      })
    // plantage dans le code de success
    .catch(j3pShowError)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}
/**
 * Création de la figure mtg32
 * @private
 * @param stor
 */
function depart (stor) {
  stor.mtgAppLecteur.removeAllDoc()
  const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAARFAAACoQAAAQEAAAAAAAAAAQAAAGP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAAAAUAAUBuIAAAAAAAQHeB64UeuFL#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAEOAAFJAMAYAAAAAAAAAAAAAAAAAAAAAAUAAUBMAAAAAAAAAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAAEAAAAQAAAAEAAAABAAAAA#####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8AAAAAABAAAAEAAAABAAAAAQAAAAT#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQAABQABAAAABwAAAAkA#####wAAAAABDgABSgDAKAAAAAAAAMAQAAAAAAAAAAAFAAIAAAAH#####wAAAAIAB0NSZXBlcmUA#####wDm5uYAAAABAAAAAQAAAAMAAAAJAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEAAAEAAAAAAwAAAAz#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAABAAAADf####8AAAABAAdDQ2FsY3VsAP####8ABHhtaW4AAy0xNf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQC4AAAAAAAAAAAARAP####8ABHhtYXgAAjE1AAAAAUAuAAAAAAAAAAAAEQD#####AAR5bWluAAMtMTIAAAASAAAAAUAoAAAAAAAAAAAAEQD#####AAR5bWF4AAIxMgAAAAFAKAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####AAhQYXBNaWxsaQAAAA0AAAAEAAAABQAAAAoAAAAPAAAAEAAAABEAAAAS#####wAAAAEAEENQb2ludERhbnNSZXBlcmUAAAAAEwEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACgAAAA4AAAAPAAAADgAAABEAAAAUAAAAABMAAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAoAAAAOAAAAEAAAAA4AAAAR#####wAAAAEACENTZWdtZW50AAAAABMAAAAAABAAAAEAAAABAAAAFAAAABUAAAAEAAAAABMAAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#p7hTMNFEXgAAABb#####AAAAAQAUQ1RyYW5zbGF0aW9uUGFyQ29vcmQAAAAAEwAAAAoAAAABAAAAAAAAAAAAAAANAQAAAA4AAAASAAAADgAAABEAAAAPAAAAABMBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABcAAAAYAAAAFQAAAAATAQAAAAAQAAABAAAAAQAAABcAAAAZ#####wAAAAIAEkNMaWV1T2JqZXRQYXJQdExpZQEAAAATAObm5gAAAAAAGgAAAA0AAAAADQIAAAANAQAAAA4AAAAQAAAADgAAAA8AAAABQCQAAAAAAAAAAAABP#AAAAAAAAAAAAAXAAAAAwAAABcAAAAZAAAAGgAAABcBAAAAEwCkpKQAAAAAABoAAAANAAAAAA0BAAAADgAAABAAAAAOAAAADwAAAAE#8AAAAAAAAAAAABcAAAADAAAAFwAAABkAAAAaAAAAFAAAAAATAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAKAAAADgAAAA8AAAAOAAAAEgAAABUAAAAAEwEAAAAAEAAAAQAAAAEAAAAUAAAAHQAAAAQAAAAAEwEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#mW0VIvujIAAAAHgAAABYAAAAAEwAAAAoAAAANAQAAAA4AAAAQAAAADgAAAA8AAAABAAAAAAAAAAAAAAAPAAAAABMBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAB8AAAAgAAAAFQAAAAATAQAAAAAQAAABAAAAAQAAAB8AAAAhAAAAFwEAAAATAObm5gAAAAAAIgAAAA0AAAAADQIAAAANAQAAAA4AAAASAAAADgAAABEAAAABQCQAAAAAAAAAAAABP#AAAAAAAAAAAAAfAAAAAwAAAB8AAAAhAAAAIgAAABcBAAAAEwC9vb0AAAAAACIAAAANAAAAAA0BAAAADgAAABIAAAAOAAAAEQAAAAE#8AAAAAAAAAAAAB8AAAADAAAAHwAAACEAAAAiAAAAEQD#####AAduYmdyYWR4AAIyMAAAAAFANAAAAAAAAAAAABEA#####wAHbmJncmFkeQACMjAAAAABQDQAAAAAAAAAAAATAP####8AFEdyYWR1YXRpb25BeGVzUmVwZXJlAAAAGwAAAAgAAAADAAAACgAAACUAAAAm#####wAAAAEAE0NBYnNjaXNzZU9yaWdpbmVSZXAAAAAAJwAFYWJzb3IAAAAK#####wAAAAEAE0NPcmRvbm5lZU9yaWdpbmVSZXAAAAAAJwAFb3Jkb3IAAAAKAAAACwAAAAAnAAZ1bml0ZXgAAAAK#####wAAAAEACkNVbml0ZXlSZXAAAAAAJwAGdW5pdGV5AAAACgAAABQAAAAAJwAAAAAAEAAAAQAABQAAAAAKAAAADgAAACgAAAAOAAAAKQAAABQAAAAAJwAAAAAAEAAAAQAABQAAAAAKAAAADQAAAAAOAAAAKAAAAA4AAAAqAAAADgAAACkAAAAUAAAAACcAAAAAABAAAAEAAAUAAAAACgAAAA4AAAAoAAAADQAAAAAOAAAAKQAAAA4AAAArAAAADAAAAAAnAAAALAAAAA4AAAAlAAAADwAAAAAnAAAAAAAQAAABAAAFAAAAAC0AAAAvAAAADAAAAAAnAAAALAAAAA4AAAAmAAAADwAAAAAnAAAAAAAQAAABAAAFAAAAAC4AAAAxAAAAFQAAAAAnAQAAAAAQAAABAAAAAQAAAC0AAAAwAAAAFQAAAAAnAQAAAAAQAAABAAAAAQAAAC4AAAAyAAAABAAAAAAnAQAAAAALAAFXAMAUAAAAAAAAwDQAAAAAAAAAAAUAAT#cVniavN8OAAAAM#####8AAAACAAhDTWVzdXJlWAAAAAAnAAZ4Q29vcmQAAAAKAAAANQAAABEAAAAAJwAFYWJzdzEABnhDb29yZAAAAA4AAAA2AAAAFwEAAAAnAGZmZgAAAAAANQAAAA4AAAAlAAAANQAAAAIAAAA1AAAANQAAABEAAAAAJwAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAACgAAAAOAAAANwAAABQAAAAAJwEAAAAAEAAAAQAABQAAAAAKAAAADgAAADkAAAAOAAAAKQAAABcBAAAAJwBmZmYAAAAAADoAAAAOAAAAJQAAADUAAAAFAAAANQAAADYAAAA3AAAAOQAAADoAAAAEAAAAACcBAAAAAAsAAVIAQCAAAAAAAADAIAAAAAAAAAAABQABP9EbToG06B8AAAA0#####wAAAAIACENNZXN1cmVZAAAAACcABnlDb29yZAAAAAoAAAA8AAAAEQAAAAAnAAVvcmRyMQAGeUNvb3JkAAAADgAAAD0AAAAXAQAAACcAZmZmAAAAAAA8AAAADgAAACYAAAA8AAAAAgAAADwAAAA8AAAAEQAAAAAnAAVvcmRyMgANMipvcmRvci1vcmRyMQAAAA0BAAAADQIAAAABQAAAAAAAAAAAAAAOAAAAKQAAAA4AAAA+AAAAFAAAAAAnAQAAAAAQAAABAAAFAAAAAAoAAAAOAAAAKAAAAA4AAABAAAAAFwEAAAAnAGZmZgAAAAAAQQAAAA4AAAAmAAAAPAAAAAUAAAA8AAAAPQAAAD4AAABAAAAAQf####8AAAACAAxDQ29tbWVudGFpcmUAAAAAJwFmZmYAAAAAAAAAAABAGAAAAAAAAAAAAAAANQsAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAsjVmFsKGFic3cxKQAAABcBAAAAJwBmZmYAAAAAAEMAAAAOAAAAJQAAADUAAAAEAAAANQAAADYAAAA3AAAAQwAAAB0AAAAAJwFmZmYAAAAAAAAAAABAGAAAAAAAAAAAAAAAOgsAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAsjVmFsKGFic3cyKQAAABcBAAAAJwBmZmYAAAAAAEUAAAAOAAAAJQAAADUAAAAGAAAANQAAADYAAAA3AAAAOQAAADoAAABFAAAAHQAAAAAnAWZmZgDAIAAAAAAAAD#wAAAAAAAAAAAAAAA8CwAAAAAAAgAAAAEAAAABAAAAAAAAAAAACyNWYWwob3JkcjEpAAAAFwEAAAAnAGZmZgAAAAAARwAAAA4AAAAmAAAAPAAAAAQAAAA8AAAAPQAAAD4AAABHAAAAHQAAAAAnAWZmZgDAHAAAAAAAAAAAAAAAAAAAAAAAAABBCwAAAAAAAgAAAAEAAAABAAAAAAAAAAAACyNWYWwob3JkcjIpAAAAFwEAAAAnAGZmZgAAAAAASQAAAA4AAAAmAAAAPAAAAAYAAAA8AAAAPQAAAD4AAABAAAAAQQAAAEn#####AAAAAQAOQ09iamV0RHVwbGlxdWUA#####wAAAAAAAAAAAAQAAAAeAP####8AAAAAAAAAAAAFAAAAHgD#####AAAAAAAAAAAAAQAAAB4A#####wAAAAAAAAAAAAMAAAAeAP####8AAAAAAAAAAAAJAAAAEQD#####AAFrAAEyAAAAAUAAAAAAAAAAAAAAEQD#####AAFhAAMwLjQAAAABP9mZmZmZmZr#####AAAAAQAFQ0ZvbmMA#####wABZgAFayphXngAAAANAgAAAA4AAABQ#####wAAAAEACkNQdWlzc2FuY2UAAAAOAAAAUf####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAXgAAAAEAP####8BAAAAABAAAXgAAAAAAAAAAABACAAAAAAAAAAABQABQAsaWwD93QIAAAAEAAAAGwD#####AAJ4MQAAAAoAAABTAAAAEQD#####AAJ5MQAFZih4MSn#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAABSAAAADgAAAFQAAAAUAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAoAAAAOAAAAVAAAAA4AAABV#####wAAAAIADUNMaWV1RGVQb2ludHMA#####wAAAAAAAAABAAAAVgAAAfQAAQAAAFMAAAAEAAAAUwAAAFQAAABVAAAAVgAAABEA#####wAEYWJzMQABMAAAAAEAAAAAAAAAAAAAABQA#####wAAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAACgAAAA4AAABYAAAAIgAAAFIAAAAOAAAAWAAAABEA#####wAEYWJzMgABMQAAAAE#8AAAAAAAAAAAABQA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAACgAAAA4AAABaAAAAAQAAAAAAAAAAAAAAFAD#####AAB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAKAAAADgAAAFoAAAAiAAAAUgAAAA4AAABaAAAAFAD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAKAAAAAQAAAAAAAAAAAAAAIgAAAFIAAAAOAAAAWgAAABUA#####wAAfwAAEAAAAQAAAgMAAABdAAAAXAAAABUA#####wAAfwAAEAAAAQAAAgMAAABcAAAAWwAAABEA#####wACa2EAA2sqYQAAAA0CAAAADgAAAFAAAAAOAAAAUQAAAB0A#####wAAfwAAwDMAAAAAAAAAAAAAAAAAAAAAAAAAXRAAAAAAAAIAAAABAAAAAQAAAAAAAAAAAAgjVmFsKGthKQAAAB0A#####wAAfwAAwDEAAAAAAADACAAAAAAAAAAAAAAAWRAAAAAAAAIAAAABAAAAAQAAAAAAAAAAAAcjVmFsKGspAAAADv##########'
  stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
  // stor.mtgAppLecteur.calculateAndDisplayAll(true) // Ne pas mettre cette ligne lorsqu’on a modifFig (sinon, les objets sont en double)
}
/**
 * Actualisation de la figure mtg32
 * @private
 * @param {Object} stor Permet de récupérer les données de l’exercice
 * @param {Boolean} modeCorrection l’actualisation de la figure se fait en mode énoncé et en mode correction
 */
function modifFig (stor, modeCorrection) {
  if (modeCorrection) {
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs2', '1')
    if (stor.objVariables.k !== 1) {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs1', '0')
    }
  } else {
    // Je cache les affichages à la correction
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'k', String(stor.objVariables.k))
    stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(stor.objVariables.a))
    if (stor.objVariables.a > 1) {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs2', '100')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs1', '100')
    } else {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs2', '-100')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'abs1', '-100')
    }
  }
  stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
  stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
}
/**
 * Retourne les données de l’exercice
 * @private
 * @param {boolean} kEgal1 s’il faut true, alors on impose à la valeur de k de l’expression k*a^x à valoir 1
 * @param me
 * @return {Object}
 */
function genereAlea (kEgal1, me) {
  /**
   * données de l’exercice qui peuvent être les variables présentes dans les textes (consignes ou corrections)
   * ou autre données utiles à la validation
   * @private
   * @type {Object} obj
   */
  const obj = {}
  obj.f = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
  // On ne prendra que des valeurs positives de k
  let pb
  do {
    if (kEgal1) {
      obj.k = 1
    } else {
      do {
        obj.k = j3pGetRandomInt(1, 8) / 2
      } while (Math.abs(obj.k - 1) < Math.pow(10, -12))
    }
    if (kEgal1) {
      // Je n’ai pas envie qu’on ait toujours le même cas de figure
      if (me.storage.nbInf1 === me.donneesSection.nbrepetitions - 1) {
        obj.a = j3pGetRandomInt(6, 30) / 5
      } else if (me.storage.nbSup1 === me.donneesSection.nbrepetitions - 1) {
        obj.a = j3pGetRandomInt(2, 7) / 10
      } else {
        if (j3pGetRandomBool()) {
          me.storage.nbInf1++
          obj.a = j3pGetRandomInt(2, 7) / 10
        } else {
          me.storage.nbSup1++
          obj.a = j3pGetRandomInt(6, 30) / 5
        }
      }
    } else {
      const aTest = j3pGetRandomInt(2, 6) / obj.k
      obj.a = Math.round(aTest * 5) / 5
    }
    pb = obj.a * obj.k > 6 // Sinon, on ne peut plus lire l’image de 1
    pb = pb || (Math.abs(obj.a - obj.k) < Math.pow(10, -12)) // Je veux deux valeurs différentes
    pb = pb || Math.abs(obj.a - 1) < 1e-12 // a ne doit pas valoir 1
    pb = pb || (Math.abs(Math.round(obj.k * obj.a * 10) / 10 - obj.k * obj.a) > Math.pow(10, -12)) // sinon on ne peut pas lire correctement l’image de 1
  } while (pb)
  obj.ka = j3pVirgule(Math.round(100 * obj.k * obj.a) / 100)
  obj.kTxt = j3pVirgule(obj.k)
  obj.aTxt = (kEgal1) ? j3pVirgule(obj.a) : '\\frac{' + obj.ka + '}{' + obj.kTxt + '}=' + j3pVirgule(obj.a)
  return obj
}
/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // on affiche l’énoncé
  // La style de base (ici petit.enonce) peut être complété par d’autres propriétés css. C’est fait ici avec j3pStyle
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.enonce })
  j3pStyle(stor.elts.divEnonce, { padding: '10px' })
  // On génère les données de l’exo (stockées dans un objet dont on se sert entre autre pour les phrases de l’énoncé)
  stor.objVariables = genereAlea(ds.kEgal1, me)
  const consigne1 = (ds.kEgal1) ? ds.textes.consigne1_2 : ds.textes.consigne1_1
  const consigne2 = (ds.kEgal1) ? ds.textes.consigne2_2 : ds.textes.consigne2_1
  // On écrit la première phrase (dans un bloc pour assurer un retour à la ligne)
  afficheBloc(stor.elts.divEnonce, consigne1, stor.objVariables)
  afficheBloc(stor.elts.divEnonce, consigne2)
  const consigne3 = (ds.kEgal1) ? '$a=$&1&' : ds.textes.consigne3
  const eltCons3 = afficheBloc(stor.elts.divEnonce, consigne3)
  stor.zonesInput = [...eltCons3.inputmqList]
  for (const zone of stor.zonesInput) {
    zone.typeReponse = ['nombre', 'exact']
    mqRestriction(zone, '\\d,.-')
  }
  if (ds.kEgal1) {
    stor.zonesInput[0].reponse = [stor.objVariables.a]
  } else {
    stor.zonesInput[0].reponse = [stor.objVariables.k]
    stor.zonesInput[1].reponse = [stor.objVariables.a]
  }
  const zoneInfo = afficheBloc(stor.elts.divEnonce, ds.textes.info)
  j3pStyle(zoneInfo.parent, { paddingTop: '10px', fontStyle: 'italic', fontSize: '0.9em' })
  stor.fctsValid = new ValidationZones({
    zones: stor.zonesInput,
    parcours: me
  })
  j3pFocus(stor.zonesInput[0])
  // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvent dans le cadre MD)
  // Il n’est ainsi construit qu’une seule fois et vidé à chaque passage dans correction()
  stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.moyen.correction })
  j3pStyle(stor.zoneD, { padding: '10px' })
  stor.zoneCorr = j3pAddElt(stor.zoneD, 'div')
  j3pStyle(stor.zoneCorr, { paddingBottom: '10px' })
  if (!ds.kEgal1) {
    // On propose une calculatrice
    stor.zoneCalc = j3pAddElt(stor.zoneD, 'div')
    stor.calc = j3pGetNewId('Calculatrice')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, j3pToggleFenetres.bind(null, 'Calculatrice'), { id: stor.calc, className: 'MepBoutons', value: 'Calculatrice' })
    j3pAfficheCroixFenetres('Calculatrice')
    j3pStyle(stor.zoneCalc, { paddingBottom: '10px' })
  }
  /// /////////////////////////////////////// Pour MathGraph32
  const largFig = 480// on gère ici la largeur de la figure
  const hautFig = 560
  // création du div accueillant la figure mtg32
  stor.figMtg32 = j3pAddElt(stor.zoneD, 'div')
  j3pStyle(stor.figMtg32, { textAlign: 'center' })
  stor.mtg32svg = j3pGetNewId('mtg32svg')
  // et enfin la zone svg (très importante car tout est dedans)
  j3pCreeSVG(stor.figMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
  depart(stor)
  modifFig(stor, false)
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me, bonneReponse = false) {
  const stor = me.storage
  const ds = me.donneesSection
  if (!ds.kEgal1) {
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
  }
  modifFig(stor, true)
  const zoneExpli = j3pAddElt(stor.elts.divEnonce, 'div', '', { style: me.styles.petit.correction })
  // La couleur de la correction sera fonction de la qualité de la réponse de l’élève
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  if (!ds.kEgal1) afficheBloc(zoneExpli, ds.textes.corr1, stor.objVariables)
  const corr2 = (ds.kEgal1) ? ds.textes.corr2_2 : ds.textes.corr2_1
  afficheBloc(zoneExpli, corr2, stor.objVariables)
  if (!ds.kEgal1) afficheBloc(zoneExpli, ds.textes.corr3, stor.objVariables)
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number|null}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  if (reponse.aRepondu) {
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
}

function correction (me) {
  const stor = me.storage
  const ds = me.donneesSection
  const score = getScore(me) // null si pas répondu
  if ((score === 1) || (me.essaiCourant >= ds.nbchances)) me.score += score
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.zoneCorr)
    return
  }
  if (score !== 1) {
    // On écrit c’est faux
    me.reponseKo(stor.zoneCorr)
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if ((score !== 1) && me.essaiCourant < me.donneesSection.nbchances) {
    me.reponseKo(stor.zoneCorr, '<br>' + essaieEncore, true)
    return
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.zoneCorr)
    // Même dans le cas d’une bonne réponse, on peut afficher la correction qui aurait dans ce cas d’éventuels compléments d’information
    afficheCorrection(me, true)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, tempsDepasse, true)
    }
    afficheCorrection(me)
  }
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) {
    me.etat = 'enonce'
    // on laisse le bouton suite mis par le finCorrection() précédent
  }
  me.finNavigation(true)
  // Si on a rien à afficher de particulier après la dernière répétition, donc on passe true à finNavigation
  // pour que le modèle affiche directement le bouton "section suivante" (sans devoir cliquer sur 'suite' avant)
  // me.finNavigation(true)
}
