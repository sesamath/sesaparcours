import { j3pActive, j3pAjouteTableau, j3pAjouteZoneTexte, j3pArrondi, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomBool, j3pGetRandomInt, j3pNombre, j3pRandom, j3pRemplacePoint, j3pRestriction, j3pValeurde, j3pVirgule } from 'src/legacy/core/functions'
import Tarbre from 'src/legacy/outils/calculatrice/Tarbre'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { tempsDepasse } = textesGeneriques

/*
  Dév : Alexis Lecomte
  Juillet 2014
  Motivation : complément manuel 2nde page 112, résolution équation par dichotomie
  Paramètres : précision, nb de chances
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 0, 'entier', 'Nombre d’essais par répétition, mettre 0 pour illimité'],
    ['precision', 2, 'entier', 'Puissance négative de la précision (par exemple 3 donne une précision de 0,001, on évitera au delà...)'],
    ['degre', '3', 'string', 'La fonction peut être de degré 3 ou alea'],
    ['variation', 'croissante', 'string', 'fonction peut être croissante sur l’intervalle, décroissante (plus dur) ou alea']
  ]
}

const textes = {
  consigne: 'L’équation $£a=£k$ a une seule solution sur l’intervalle $[£b;£c]$, l’objectif est de trouver par tâtonnements successifs un encadrement d’amplitude $£d$ de la solution (tu disposes de £e essais).',
  consignebis: 'L’équation $£a=£k$ a une seule solution sur l’intervalle $[£b;£c]$, l’objectif est de trouver par tâtonnements successifs un encadrement d’amplitude $£d$ de la solution (tu disposes d’autant d’essais que nécessaire).',
  consigne2: 'Proposition pour $x$ : @1@',
  consigne3: 'On appelle $f$ la fonction qui à $x$ associe $£a$.',
  intervalle: 'Intervalle actuel : $[£a ; £b]$',
  amplitude: 'Amplitude : £a',
  intervalles: 'Liste des intervalles [a;b] obtenus contenant la solution et images des bornes par $f$ :',
  endehors: 'La valeur proposée n’est pas dans l’intervalle ! Modifie ta proposition.',
  image: 'Voici une valeur approchée de son image par $f$ : £a.',
  etape2: 'Déduis-en un nouvel intervalle contenant la solution, en t’aidant du tableau existant.',
  erreur: 'Le nouvel intervalle doit être constitué d’une des deux bornes précédentes et de ta proposition.',
  erreur2: 'Tu n’as pas choisi les bornes correctes, la solution n’appartient plus à cet intervalle.',
  mauvaischoix: 'Tu dois modifier une des bornes de l’intervalle précédent en utilisant ta proposition.',
  suite: 'L’étape est correcte, l’intervalle n’a pas encore l’amplitude demandée, il faut donc poursuivre le processus.',
  fin: 'C’est bien ! Tu as trouvé en £a étapes.<br>La solution de l’équation est bien comprise entre £b et £c.',
  ordre: 'Dans un intervalle [a;b], la borne a doit être inférieure à la borne b...',
  echec: 'Tu n’as pas réussi à trouver un intervalle contenant la solution avec la précision demandée.',
  chances: 'Il te reste £a chance(s).'
}

/**
 * section equation_dichotomie
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage

  // code Rémi
  function signe (nombre) {
  // nombre = Number(nombre);
    const absNombre = Math.abs(nombre)
    return ((nombre < 0) ? '-' : '+') + absNombre
  }

  function signeCoefX (nombre) {
  // nombre = Number(nombre);
    const absNombre = Math.abs(nombre)
    let nombre1
    if (nombre < 0) {
      if (Math.abs(nombre + 1) < Math.pow(10, -12)) {
        nombre1 = '-'
      } else {
        nombre1 = '-' + absNombre
      }
    } else {
      if (Math.abs(nombre - 1) < Math.pow(10, -12)) {
        nombre1 = '+'
      } else {
        nombre1 = '+' + absNombre
      }
    }
    return nombre1
  }
  function ecrireCoefX (nb) {
  // cette fonction tranforme nb en texte pour que ce soit le coef devant x
  // entre autre, 1 n’apparait pas
  // nb est un nombre
    if (Math.abs(nb - 1) < Math.pow(10, -12)) {
      return ''
    } else if (Math.abs(nb + 1) < Math.pow(10, -12)) {
      return '-'
    } else {
    // return _root.fonctions.virgule(Math.round(Math.pow(10, 10)*(nb))/Math.pow(10, 10));
      return nb
    }
  }

  function genereVariables () {
    if (stor.degre === '2') {
      // Equation de degré 2 :
      let a, b, c, k, delta
      do {
        // le premier calcul
        a = (stor.variation === 'croissante')
          ? j3pGetRandomInt(1, 9)
          : j3pGetRandomInt(-9, -1)
        b = j3pGetRandomInt(-2, 2)
        b = 2 * a * b// pour que b/2a soit entier
        c = j3pGetRandomInt(-9, 9)
        delta = b * b - 4 * a * c
        k = j3pGetRandomInt(2, 9)
        // les données doivent être différentes et non nulles  (pas un non plus) et mes coefs finaux non nuls, je demande également b/2a entier
      } while (delta <= 0 || b === 0 || c === 0 || c === (-k))// || simplifie_rac(Math.abs(delta))[1]!==1)//|| Math.sqrt(racine)===Math.round(Math.sqrt(racine)))

      // f(x)=0 équivaut à f(x)+k=k

      stor.fonction = ecrireCoefX(a) + 'x^2' + signeCoefX(b) + 'x' + signe(c + k)
      // le signe de a détermine la solution max
      const sol = (a > 0)
        ? (-b + Math.sqrt(delta)) / (2 * a)
        : (-b - Math.sqrt(delta)) / (2 * a)
      stor.a_init = (-b / (2 * a))// pour être sûr
      // console.log('a_init=' + stor.a_init)
      stor.b_init = Math.round(sol + 10)
      stor.k = k
      stor.arbre = new Tarbre(stor.fonction, ['x'])
    } else { // Degré 3
      let a, b, c, d, delta, deltaprime, k
      do {
        a = (stor.variation === 'croissante')
          ? j3pGetRandomInt(1, 9)
          : j3pGetRandomInt(-9, -1)
        b = Number(j3pRandom('[-9;9]\\{0}'))
        c = Number(j3pRandom('[-9;9]\\{0}'))
        d = Number(j3pRandom('[-9;9]\\{0}'))
        delta = b * b * c * c + 18 * a * b * c * d - 27 * a * a * d * d - 4 * a * c * c * c - 4 * b * b * b * d
        deltaprime = b * b - 3 * a * c
        k = j3pGetRandomInt(2, 9)
        // les données doivent être différentes et non nulles  (pas un non plus) et mes coefs finaux non nuls, je demande également b/2a entier
      } while (delta <= 0 || deltaprime <= 0 || d === (-k))// || simplifie_rac(Math.abs(delta))[1]!==1)//|| Math.sqrt(racine)===Math.round(Math.sqrt(racine)))
      // f(x)=0 équivaut à f(x)+k=k
      stor.fonction = ecrireCoefX(a) + 'x^3' + signeCoefX(b) + 'x^2' + signeCoefX(c) + 'x' + signe(d + k)
      stor.k = k
      // reste à faire : obtenir un intervalle encadrant la solution ou f est strictement monotone
      const absDuMin = (a > 0)
        ? (-b + Math.sqrt(deltaprime)) / (3 * a)
        : (-b - Math.sqrt(deltaprime)) / (3 * a)
      let borneInf = Math.ceil(absDuMin)// entier sup ou égal, si son image est négative on le garde sinon on voit plus petit...
      let borneSup
      // console.log('absDuMin' + absDuMin + ' et borneInf=' + borneInf)
      stor.arbre = new Tarbre(stor.fonction, ['x'])
      let decimale = 0
      if (a > 0) {
        while (stor.arbre.evalue([borneInf]) >= k) {
          decimale++
          const pas = (borneInf - absDuMin) / 10
          borneInf = j3pArrondi(absDuMin + pas, decimale)
        }
        borneSup = 10// arbitraire
        while (stor.arbre.evalue([borneSup]) <= k) {
          borneSup = borneSup + 10
        }
      } else {
        while (stor.arbre.evalue([borneInf]) <= k) {
          decimale++
          const pas = (borneInf - absDuMin) / 10
          borneInf = j3pArrondi(absDuMin + pas, decimale)
        }
        borneSup = 10 // arbitraire
        while (stor.arbre.evalue([borneSup]) >= k) {
          borneSup = borneSup + 10
        }
      }
      stor.a_init = borneInf
      stor.b_init = borneSup
    }
  }

  function completeImages (i) {
    j3pAffiche(stor.tableau_id[i][2], 'affiche_tab_' + (i + 1) + '_3', '' + j3pVirgule(j3pArrondi(stor.f_a[i - 1], 5)), {})
    j3pAffiche(stor.tableau_id[i][3], 'affiche_tab_' + (i + 1) + '_4', '' + j3pVirgule(j3pArrondi(stor.f_b[i - 1], 5)), {})
  }

  function creerTableau () {
    // je détruis le tableau s’il existe déjà.
    j3pDetruit('untab')
    // je le reconstruis
    j3pAjouteTableau('div_tab', { id: 'untab', nbli: stor.nb_lignes, nbcol: 4, tabid: stor.tableau_id, tabcss: [] })
    // je le complète :
    j3pAffiche(stor.tableau_id[0][0], 'affiche_tab_1_1', '$a$', {})
    j3pAffiche(stor.tableau_id[0][1], 'affiche_tab_1_2', '$b$', {})
    j3pAffiche(stor.tableau_id[0][2], 'affiche_tab_1_3', '$f(a)$', {})
    j3pAffiche(stor.tableau_id[0][3], 'affiche_tab_1_4', '$f(b)$', {})
    for (let i = 1; i < stor.tableau_id.length; i++) {
      j3pElement(stor.tableau_id[i][0]).style.width = '70px'
      j3pElement(stor.tableau_id[i][1]).style.width = '70px'
      j3pElement(stor.tableau_id[i][2]).style.width = '70px'
      j3pElement(stor.tableau_id[i][3]).style.width = '70px'
      // console.log('avant=' + i + ' avec tableau_id[i][0]=' + stor.tableau_id[i][0] + ' et a[i-1]=' + stor.a[i - 1])
      if (('' + stor.a[i - 1]).indexOf('@') !== -1) {
        // on met une zone de saisie
        j3pAjouteZoneTexte(stor.tableau_id[i][0], { id: stor.tableau_id[i][0] + 'reponse1', maxchars: '8', restrict: /[0-9,.-]/, texte: '', tailletexte: me.styles.toutpetit.enonce.taille, width: 70 })
        j3pAjouteZoneTexte(stor.tableau_id[i][1], { id: stor.tableau_id[i][1] + 'reponse1', maxchars: '8', restrict: /[0-9,.-]/, texte: '', tailletexte: me.styles.toutpetit.enonce.taille, width: 70 })
        j3pElement(stor.tableau_id[i][0] + 'reponse1').addEventListener('input', j3pRemplacePoint)
        j3pElement(stor.tableau_id[i][1] + 'reponse1').addEventListener('input', j3pRemplacePoint)
        j3pFocus(stor.tableau_id[i][0] + 'reponse1')
      } else {
        j3pAffiche(stor.tableau_id[i][0], 'affiche_tab_' + (i + 1) + '_1', '' + j3pVirgule(stor.a[i - 1]), {})
        j3pAffiche(stor.tableau_id[i][1], 'affiche_tab_' + (i + 1) + '_2', '' + j3pVirgule(stor.b[i - 1]), {})
      }
      // console.log('apres=' + i)
      if (i !== stor.tableau_id.length - 1) {
        completeImages(i)
      }
    }
  }

  function etapeSuivante () {
    // 'desactivation' des zones de saisies.
    const rep1 = (j3pValeurde(stor.tableau_id[stor.tableau_id.length - 1][0] + 'reponse1'))
    const rep2 = (j3pValeurde(stor.tableau_id[stor.tableau_id.length - 1][1] + 'reponse1'))
    j3pElement(stor.tableau_id[stor.tableau_id.length - 1][0]).innerHTML = (j3pVirgule(j3pNombre(rep1)))
    j3pElement(stor.tableau_id[stor.tableau_id.length - 1][1]).innerHTML = (j3pVirgule(j3pNombre(rep2)))
    // on rajoute les images par f
    stor.f_a.push(stor.arbre.evalue([j3pNombre(rep1)]))
    stor.f_b.push(stor.arbre.evalue([j3pNombre(rep2)]))
    completeImages(stor.tableau_id.length - 1)
    // retour en partie 1
    stor.partie = 1
    j3pActive('affiche5input1')
    j3pElement('affiche5input1').value = ''// nettoyage de la zone de saisie
    j3pDetruit('affiche7')// on supprime les commentaires et l’affichage de l’image en fin de div enonce
    j3pDetruit('affiche6')
    j3pFocus('affiche5input1')
    j3pEmpty('details')
    // on change les valeurs des dernieres bornes (avant c'était @1@)
    stor.a[stor.a.length - 1] = rep1
    stor.b[stor.b.length - 1] = rep2
    // on modifie l’affichage de l’amplitude et de l’intervalle courant
    stor.amplitude = j3pVirgule(j3pArrondi(Math.abs(j3pNombre(rep2) - j3pNombre(rep1)), 5))
    j3pAffiche('details', 'affiche3', '\n' + textes.intervalle + '\n', { a: stor.a[stor.a.length - 1], b: stor.b[stor.b.length - 1] })
    j3pAffiche('details', 'affiche4', '\n' + textes.amplitude + '\n', { a: stor.amplitude })
  }

  function afficheSuite (x) {
    const resultat = stor.arbre.evalue([j3pNombre(x)])
    // on arrondit à 10^5 (arbitraire...) pour éviter les 0,1499999999 et cie
    const image = j3pVirgule(j3pArrondi(resultat, 5))
    j3pAffiche('enonce', 'affiche6', '\n' + textes.image, { a: image })
    j3pAffiche('enonce', 'affiche7', '\n' + textes.etape2, {})
    const i = stor.tableau_id.length + 1
    stor.tableau_id.push(['a' + i + '1', 'a' + i + '2', 'a' + i + '3', 'a' + i + '4'])
    stor.nb_lignes++
    stor.a[i - 2] = '@1@'
    stor.b[i - 2] = '@1@'
    j3pDesactive('affiche5input1')
    creerTableau()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.construitStructurePage('presentation1')
        this.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        this.afficheTitre('Résolution d’équation par tests')
        if (this.donneesSection.indication) this.indication(this.zones.IG, this.donneesSection.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
        stor.numEssai = 1
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      if (me.donneesSection.degre === 'alea') {
        const alea = j3pGetRandomInt(0, 1)
        if (alea === 0) {
          stor.degre = '2'
        } else {
          stor.degre = '3'
        }
      } else {
        stor.degre = me.donneesSection.degre
      }
      if (me.donneesSection.variation === 'alea') {
        stor.variation = j3pGetRandomBool() ? 'croissante' : 'decroissante'
      } else {
        stor.variation = me.donneesSection.variation
      }
      stor.erreur = 0// les erreurs sur le principe
      genereVariables()
      stor.partie = 1
      stor.precision_reelle = Math.pow(10, -me.donneesSection.precision)
      // ceux là variront au cours de l’exo
      stor.a = []
      stor.b = []
      stor.a[0] = stor.a_init
      stor.b[0] = stor.b_init
      stor.amplitude = stor.b_init - stor.a_init
      stor.f_a = []
      stor.f_b = []

      stor.f_a[0] = stor.arbre.evalue([stor.a_init])
      stor.f_b[0] = stor.arbre.evalue([stor.b_init])

      j3pDiv(this.zones.MG, {
        id: 'laconsigne',
        contenu: '',
        style: this.styles.etendre('toutpetit.enonce', { padding: '20px' })
      }
      )
      j3pDiv('laconsigne', 'enonce', '')
      j3pDiv('laconsigne', 'commentaires', '')

      // j3pAjouteZoneTexte(this.zones.MG,{id:"equation_dichotomie_reponse",maxchars:"5",restrict:"0-9,",texte:"",tailletexte:18,width:80,top:130,left:260});
      // j3pFocus("equation_dichotomie_reponse")
      if (me.donneesSection.nbchances === 0) {
        // illimité
        j3pAffiche('enonce', 'affiche1', textes.consignebis + '\n', { a: stor.fonction, b: stor.a_init, c: stor.b_init, d: Math.pow(10, -me.donneesSection.precision), k: stor.k })
      } else {
        j3pAffiche('enonce', 'affiche1', textes.consigne + '\n', { a: stor.fonction, b: stor.a_init, c: stor.b_init, d: Math.pow(10, -me.donneesSection.precision), e: j3pNombre(me.donneesSection.nbchances), k: stor.k })
      }
      j3pAffiche('enonce', 'affiche2', textes.consigne3 + '\n', { a: stor.fonction })
      j3pDiv('enonce', 'details', '')

      j3pAffiche('details', 'affiche3', '\n' + textes.intervalle + '\n', { a: stor.a_init, b: stor.b_init })

      j3pAffiche('details', 'affiche4', '\n' + textes.amplitude + '\n', { a: j3pVirgule(stor.amplitude) })

      this.fenetresjq = [
        { name: 'tab_intervalles', title: 'Intervalles :', width: 450, height: 450, left: 700, top: 100, id: 'tableau' }
      ]
      j3pCreeFenetres(this)
      j3pDiv('dialogtab_intervalles', 'texte', '')
      j3pAffiche('texte', 'titre_Affiche', textes.intervalles, {})
      j3pToggleFenetres('tab_intervalles')
      j3pAffiche('enonce', 'affiche5', '\n' + textes.consigne2, { input1: { width: '', correction: '', maxchars: '8', dynamique: true } })
      j3pFocus('affiche5input1')
      j3pRestriction('affiche5input1', '-0-9,\\.')
      j3pElement('affiche5input1').addEventListener('input', j3pRemplacePoint)

      j3pDiv('dialogtab_intervalles', 'div_tab', '')
      // Données intiales du tableau des intervalles :
      stor.nb_lignes = 2
      stor.tableau_id = []
      stor.tableau_id[0] = []
      stor.tableau_id[1] = []
      stor.tableau_id[0] = ['a11', 'a12', 'a13', 'a14']
      stor.tableau_id[1] = ['a21', 'a22', 'a23', 'a24']

      creerTableau()
      completeImages(1)

      j3pDiv(this.zones.MG, { id: 'explications', contenu: '', coord: [50, 300], style: this.styles.moyen.explications })
      j3pDiv('commentaires', { id: 'correction', contenu: '', style: this.styles.moyen.correction })
      // Obligatoire
      this.finEnonce()

      break // case "enonce":

    case 'correction':
      // Si partie 1 ok, on ne change pas l’état, fonction afficheSuite et on change la variable partie
      // On teste si une réponse a été saisie
      if (stor.partie === 1) {
        j3pElement('correction').innerHTML = ''
        const repEleve = j3pValeurde('affiche5input1')
        if ((!repEleve && !this.isElapsed) ||
          (j3pNombre(repEleve) < j3pNombre(stor.a[stor.a.length - 1])) ||
          (j3pNombre(repEleve) > j3pNombre(stor.b[stor.b.length - 1]))
        ) {
          if (!repEleve && !this.isElapsed) {
            this.reponseManquante('correction')
          } else { // pas dans l’intervalle
            j3pElement('correction').innerHTML = '<br>' + textes.endehors
          }
          j3pFocus('affiche5input1')
        } else {
          // on affiche l’image de x, on reste en case correction
          stor.partie = 2
          stor.repeleve = repEleve
          afficheSuite(repEleve)
        }
      } else {
        const repEleve1 = j3pValeurde(stor.tableau_id[stor.tableau_id.length - 1][0] + 'reponse1')
        const repEleve2 = j3pValeurde(stor.tableau_id[stor.tableau_id.length - 1][1] + 'reponse1')
        if ((repEleve1 === '' || repEleve2 === '') && (!this.isElapsed)) {
          this.reponseManquante('correction')
          j3pFocus(stor.tableau_id[stor.tableau_id.length - 1][0] + 'reponse1')
        } else {
          // Une réponse a été saisie
          // on teste si l’écart entre les deux est inférieur à la valeur et si f(repEleve1)-k et f(repEleve2)-k sont de signes contraires
          // on doit d’abord tester si l’intervalle est ok avec les précédentes valeurs, cad reprend une des deux bornes précédentes et la réponse de l’élève pour l’autre borne (si erreur dans la borne gardé alors f(repEleve1)-k et f(repEleve2)-k ne seront pas de signes contraires)
          const val1 = stor.arbre.evalue([j3pNombre(repEleve1)]) - stor.k
          const val2 = stor.arbre.evalue([j3pNombre(repEleve2)]) - stor.k
          const repriseValeurs = (('' + j3pNombre(repEleve1)) === ('' + j3pNombre(stor.a[stor.a.length - 2])) && ('' + j3pNombre(repEleve2)) === ('' + j3pNombre(stor.repeleve))) || (('' + j3pNombre(repEleve1)) === ('' + j3pNombre(stor.repeleve)) && ('' + j3pNombre(repEleve2)) === ('' + j3pNombre(stor.b[stor.b.length - 2])))
          const test = Boolean((val1 * val2) <= 0 && repriseValeurs)
          const test2 = (j3pArrondi(Math.abs(j3pNombre(repEleve1) - j3pNombre(repEleve2)), 5) <= stor.precision_reelle)
          if (test && test2) {
            // Bonne réponse
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            // j3pElement("correction").innerHTML=textes.fin;
            j3pAffiche('correction', 'cor_affiche', textes.fin, { a: stor.numEssai, b: repEleve1, c: repEleve2 })
            this.typederreurs[0]++
            this.cacheBoutonValider()
            j3pDesactive(stor.tableau_id[stor.tableau_id.length - 1][0] + 'reponse1')
            j3pDesactive(stor.tableau_id[stor.tableau_id.length - 1][1] + 'reponse1')
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Pas encore trouvé
            j3pElement('correction').style.color = this.styles.cfaux
            // A cause de la limite de temps :
            if (this.isElapsed) { // limite de temps
              this._stopTimer()
              j3pElement('correction').innerHTML = tempsDepasse
              this.typederreurs[10]++
              this.cacheBoutonValider()
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              // Réponse fausse :
              if (j3pNombre(repEleve1) > j3pNombre(repEleve2)) { // intervalle dans le mauvais ordre
                j3pElement('correction').innerHTML = '<br>' + textes.ordre
                stor.numEssai++// on compte une tentative...
                j3pFocus(stor.tableau_id[stor.tableau_id.length - 1][0] + 'reponse1')
              } else {
                if (test) {
                  // le principe de la dichotomie est compris mais on n’a pas encore la précision voulue (on a forcément !test2), on continue, cad
                  // on gèle les zones de saisie, on affiche les images, on nettoie le contenu de la zone de saisie et du commentaire suivant, on met un message pour continuer
                  stor.numEssai++
                  j3pElement('correction').style.color = this.styles.cbien// pas d’erreur en fait, c’est juste que c’est pas fini.
                  j3pElement('correction').innerHTML = '<br>' + textes.suite
                  etapeSuivante()
                } else {
                  // pb, le principe de la dichotomie n’est pas compris
                  // Attention : la variable test regarde si l’élève a repris une des bornes précédentes ET si elles encadrent la solution, elle peut donc être fausse si l’élève s’est juste trompé dans les bornes...
                  if (repriseValeurs) {
                    // l’élève a bien repris une valeur précédente et sa réponse mais pas la bonne borne
                    console.error('PAS LES BONNES VALEURS')
                    j3pElement('correction').innerHTML = '<br>' + textes.erreur2
                    stor.numEssai++// on compte une tentative...
                  } else {
                    // principe non compris
                    j3pElement('correction').innerHTML = '<br>' + textes.erreur
                    // on comptabilise ce type d’erreur pour si nb chances illimité
                    stor.erreur++
                  }
                  j3pFocus(stor.tableau_id[stor.tableau_id.length - 1][0] + 'reponse1')
                }
              }

              if (stor.numEssai <= this.donneesSection.nbchances || (this.donneesSection.nbchances === 0 && stor.erreur < 6)) {
                this.typederreurs[1]++
              } else {
                // Erreur au nème essai
                this._stopTimer()
                this.cacheBoutonValider()
                j3pElement('correction').style.color = this.styles.cfaux
                j3pElement('correction').innerHTML = ''
                j3pDesactive('affiche5input1')
                j3pAffiche('correction', 'cor_affiche2', textes.echec, { a: j3pNombre(this.donneesSection.nbchances) })
                this.typederreurs[2]++
                this.etat = 'navigation'
                this.sectionCourante()
              }
            }
            if (me.donneesSection.nbchances !== 0 && (j3pNombre(this.donneesSection.nbchances) + 1 - stor.numEssai) !== 0) {
              j3pDiv('correction', 'details_cor', '')
              j3pAffiche('details_cor', 'details_cor_affiche', textes.chances, { a: (j3pNombre(this.donneesSection.nbchances) + 1 - stor.numEssai) })
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.desactiveReturn()
        this.focus('boutoncontinuer')
      }
      this.finNavigation()
      break // case "navigation":
  }
}
