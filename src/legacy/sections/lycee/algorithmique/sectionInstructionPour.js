import $ from 'jquery'
import { j3pAjouteBouton, j3pChaine, j3pVirgule, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pMonome, j3pRandomTab, j3pRemplacePoint, j3pRestriction, j3pAddElt, j3pStyle, j3pSetProps, j3pAddTxt } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Mai 2019
        Déterminer la valeur de variables à l’issue d’une boucle pour
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['formatPython', false, 'boolean', 'L’algorithme est écrit par défaut en langage naturel, mais cela peut être fait dans le langage python.'],
    ['avecFonction', false, 'boolean', 'Si ce paramètre vaut true, alors on présentera l’algorithme sous la forme d’une fonction (en python si le paramètre formatPython vaut aussi true).'],
    ['avecIDansBoucle', false, 'boolean', 'Lorsque que ce paramètre vaut true, alors on trouvera le compteur de la boucle "pour" dans le calcul du terme.'],
    ['avecArgument', false, 'boolean', 'Le nombre de passages dans la boucle sera présenté comme un argument de la fonction (si avecFonction vaut true) lorsque ce paramètre vaut true.']
  ]
}
/**
 * section InstructionPour
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.toutpetit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    for (let i = 1; i <= 4; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1)
    // Construction du tableau :
    const largeurTab = me.zonesElts.MG.getBoundingClientRect().width
    const largeurTexte = 0
    const largeurColonne = (largeurTab - largeurTexte - 5) / (stor.DonneesTab[0].length + 1)
    stor.zoneExpli2.setAttribute('width', largeurTab + 'px')
    const span = j3pAddElt(stor.zoneExpli2, 'span')
    const table = j3pAddElt(span, 'table')
    j3pSetProps(table, { width: '100%', borderColor: stor.zoneCons3.style.color, border: 2, cellSpacing: 0 })
    const divLegendes = []
    for (let i = 0; i < 2; i++) {
      const ligne = j3pAddElt(table, 'tr')
      const td = j3pAddElt(ligne, 'td')
      j3pSetProps(td, { align: 'center', width: largeurColonne + 'px' })
      divLegendes.push(j3pAddElt(td, 'div'))
      for (let j = 0; j < stor.DonneesTab[0].length; j++) {
        const td = j3pAddElt(ligne, 'td')
        j3pSetProps(td, { align: 'center', width: largeurColonne + 'px' })
        j3pAddTxt(td, stor.DonneesTab[i][j])
      }
    }
    if (ds.formatPython) {
      j3pAffiche(divLegendes[0], '', stor.objTexte.v)
      j3pAffiche(divLegendes[1], '', stor.objTexte.compteur)
    } else {
      j3pAffiche(divLegendes[0], '', '$' + stor.objTexte.v + '$')
      j3pAffiche(divLegendes[1], '', '$' + stor.objTexte.compteur + '$')
    }
    const laCorr2 = (ds.avecFonction) ? ds.textes.corr2_2 : ds.textes.corr2_1
    const variablev = (ds.formatPython) ? '\\text{' + stor.objTexte.v + '}' : stor.objTexte.v
    j3pAffiche(stor.zoneExpli3, '', laCorr2, {
      v: variablev,
      r: stor.zoneInput.reponse[0]
    })
    // Compléments avec les explications pour remplir le tableau:
    j3pStyle(stor.zoneExpli4, { fontSize: '0.9em', backgroundColor: 'white', border: 'thick double #32a1ce' })
    for (let i = 1; i <= 7; i++)stor['comp' + i] = j3pAddElt(stor.zoneExpli4, 'div')
    j3pAffiche(stor.comp1, '', ds.textes.corr3)
    const ligne1 = (ds.avecFonction) ? 2 : 1
    const objComp = {
      vInit: stor.DonneesTab[0][0],
      v: variablev,
      init: stor.DonneesTab[1][1],
      initPlusUn: stor.DonneesTab[1][2],
      fin: stor.DonneesTab[1][stor.DonneesTab[1].length - 1],
      l1: ligne1,
      l2: ligne1 + 1,
      l3: ligne1 + 2
    }
    objComp.boucle = (ds.formatPython) ? 'for' : 'pour'
    objComp.i = (ds.formatPython) ? '\\text{' + stor.objTexte.compteur + '}' : stor.objTexte.compteur
    objComp.c = stor.lesCalculs[0]
    j3pAffiche(stor.comp2, '', ds.textes.corr4, objComp)
    j3pAffiche(stor.comp3, '', ds.textes.corr5, objComp)
    j3pAffiche(stor.comp4, '', ds.textes.corr6, objComp)
    j3pAffiche(stor.comp5, '', ds.textes.corr7, objComp)
    objComp.c = stor.lesCalculs[1]
    j3pAffiche(stor.comp6, '', ds.textes.corr6, objComp)
    j3pAffiche(stor.comp7, '', ds.textes.corr8, objComp)
  }

  function avecParenthese (nb) {
    // écrit bien le nombre dans un calcul (cad avec parenthèses si ce nombre est négatif)
    if (j3pCalculValeur(nb) < 0) {
      return '(' + nb + ')'
    } else {
      return nb
    }
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      formatPython: false,
      avecFonction: false,
      avecIDansBoucle: false,
      avecArgument: false,
      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Interpréter une boucle bornée',
        // on donne les phrases de la consigne
        consigne1_1: 'On propose ci-contre un algorithme&nbsp;:',
        consigne1_2: 'On propose ci-contre un algorithme sous la forme d’une fonction&nbsp;:',
        consigne1_3: 'On propose ci-contre un programme en python&nbsp;:',
        consigne1_4: 'On propose ci-contre un programme en python écrit sous la forme d’une fonction&nbsp;:',
        consigne2_1: 'Quelle est la valeur de la variable $£{vCons}$ à l’issue de cet algorithme&nbsp;?',
        consigne2_2: 'Quelle valeur est renvoyée par l’algorithme lorsqu’on écrit £{formule}&nbsp;?',
        consigne2_4: 'Quelle valeur est renvoyée par python lorsqu’on écrit £{formule}&nbsp;?',
        consigne3: 'Réponse : @1@.',
        nomf: 'calculTerme',
        txtFonction: 'fonction',
        txtReturn: 'renvoyer',
        txtPour: 'Pour $£i$ allant de £{init} à $£{fin}$:',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'On attend une réponse sous forme simplifiée (un nombre), pas un calcul&nbsp;!',
        comment2: "Attention car <span class='purple'>range</span>(£i) signifie que les valeurs vont de 0 à £j",
        // et les phrases utiles pour les explications de la réponse
        corr1: 'On peut présenter l’évolution des valeurs des variables de cet algorithme dans un tableau',
        corr2_1: 'Donc $£v$ a pour valeur £r à l’issue de l’algorithme.',
        corr2_2: 'Donc la valeur renvoyée est £r.',
        // Phrases pour expliquer la construction du tableau
        corr3: '<u>Explications pour compléter le tableau :</u>',
        corr4: 'ligne £{l1} : $£v$ vaut $£{vInit}$&nbsp;;',
        corr5: 'ligne £{l2} : on entre dans la boucle "£{boucle}" et $£i$ vaut £{init} au premier passage&nbsp;;',
        corr6: 'ligne £{l3} : $£v$ devient $£c$&nbsp;;',
        corr7: 'on revient à la ligne £{l2} avec $£i=£{initPlusUn}$&nbsp;;',
        corr8: 'ainsi de suite jusqu’au dernier calcul avec $£i=£{fin}$...'

      },
      pe: 0
    }
  }
  function enonceMain () {
  // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.algo = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')

    j3pStyle(stor.zoneCalc, { padding: '15px 0px' })
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    const laCons1 = (ds.formatPython)
      ? (ds.avecFonction) ? ds.textes.consigne1_4 : ds.textes.consigne1_3
      : (ds.avecFonction) ? ds.textes.consigne1_2 : ds.textes.consigne1_1
    const laCons2 = (ds.formatPython)
      ? (ds.avecFonction) ? ds.textes.consigne2_4 : ds.textes.consigne2_1
      : (ds.avecFonction) ? ds.textes.consigne2_2 : ds.textes.consigne2_1
    j3pAffiche(stor.zoneCons1, '', laCons1)
    let a, b, nFinal, ecrireCDansCalc, c
    if (ds.avecIDansBoucle) {
    // u=au+ci+b où a et c sont de signe contraire
      a = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 4)
      c = (a < 0) ? j3pGetRandomInt(1, 3) : -j3pGetRandomInt(1, 3)
      nFinal = j3pGetRandomInt(3, 4)
      do {
        b = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
      } while (Math.abs(a - b) < Math.pow(10, -12))
      ecrireCDansCalc = (c === 1)
        ? '+'
        : (c === -1)
            ? '-'
            : (c < 0) ? c + '\\times ' : '+' + c + '\\times '
    } else {
    // u=au+b
      do {
        a = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 4)
        nFinal = j3pGetRandomInt(3, 4)
        do {
          b = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
        } while (Math.abs(a - b) < Math.pow(10, -12))
      } while (Math.abs(a - b / (1 - a)) < Math.pow(10, -12))
    }
    const objTexte = {}
    // Nom de la variables présente dans l’algor et qui contiendra la réponse
    objTexte.v = j3pRandomTab(['a', 'b', 'u', 'v', 't'], [0.2, 0.2, 0.2, 0.2, 0.2])
    objTexte.vCons = (ds.formatPython) ? '\\text{' + objTexte.v + '}' : objTexte.v
    objTexte.compteur = j3pRandomTab(['i', 'j', 'k', 'h'], [0.25, 0.25, 0.25, 0.25])
    objTexte.nomArg = j3pRandomTab(['n', 'N', 'm', 'M'], [0.25, 0.25, 0.25, 0.25])
    objTexte.nomArgTxt = (ds.avecFonction && ds.avecArgument) ? objTexte.nomArg : ''
    objTexte.nomFct = ds.textes.nomf
    objTexte.valInit = j3pGetRandomInt(-3, 3)
    objTexte.nFinal = (ds.avecFonction && ds.avecArgument) ? objTexte.nomArg : nFinal
    objTexte.formule = (ds.avecFonction)
      ? (ds.avecArgument) ? objTexte.nomFct + '(' + nFinal + ')' : objTexte.nomFct + '()'
      : ''
    // Création de l’algo
    const lignesAlgo = []
    let espaceTab = ''
    for (let i = 1; i <= 5; i++) {
      espaceTab += '&nbsp;'
    }
    const DonneesTab = []
    if (!ds.formatPython) {
      stor.maVarSortie = '\\text{' + stor.maVarSortie + '}'
    }
    const espaceTab1 = (ds.avecFonction) ? espaceTab : ''
    let affichei
    if (ds.formatPython) {
      if (ds.avecFonction) {
        lignesAlgo.push("<span class='bleu'>def</span> " + objTexte.nomFct + '(' + objTexte.nomArgTxt + '):')
      }
      lignesAlgo.push(espaceTab1 + objTexte.v + '=' + objTexte.valInit)
      lignesAlgo.push(espaceTab1 + "<span class='bleu'>for</span> " + objTexte.compteur + " <span class='bleu'>in </span><span class='purple'>range</span>(" + objTexte.nFinal + '):')
      if (ds.avecIDansBoucle) {
        affichei = (Math.abs(Math.abs(c) - 1) < Math.pow(10, -12))
          ? j3pMonome(2, 1, c, objTexte.compteur)
          : (c < 0) ? c + '*' + objTexte.compteur : '+' + c + '*' + objTexte.compteur
        lignesAlgo.push(espaceTab1 + espaceTab + objTexte.v + '=' + a + '*' + objTexte.v + affichei + j3pMonome(2, 0, b, 'x'))
      } else {
        lignesAlgo.push(espaceTab1 + espaceTab + objTexte.v + '=' + a + '*' + objTexte.v + j3pMonome(2, 0, b, 'x'))
      }
      if (ds.avecFonction) {
        lignesAlgo.push(espaceTab1 + "<span class='bleu'>return</span> " + objTexte.v)
      }
    } else {
      if (ds.avecFonction) {
        lignesAlgo.push(ds.textes.txtFonction + ' ' + objTexte.nomFct + '$(' + objTexte.nomArgTxt + ')$:')
      }
      lignesAlgo.push(espaceTab1 + '$' + objTexte.v + '\\leftarrow ' + objTexte.valInit + '$')
      j3pChaine(ds.textes.txtPour, { i: objTexte.compteur, init: 1, fin: objTexte.nFinal })
      if (ds.avecArgument) {
        lignesAlgo.push(espaceTab1 + j3pChaine(ds.textes.txtPour, {
          i: objTexte.compteur,
          init: 1,
          fin: objTexte.nFinal
        }))
      } else {
        lignesAlgo.push(espaceTab1 + j3pChaine(ds.textes.txtPour, {
          i: objTexte.compteur,
          init: 1,
          fin: '\\text{' + objTexte.nFinal + '}'
        }))
      }
      if (ds.avecIDansBoucle) {
        affichei = (Math.abs(Math.abs(c) - 1) < Math.pow(10, -12))
          ? j3pMonome(2, 1, c, objTexte.compteur)
          : (c < 0) ? c + '\\times ' + objTexte.compteur : '+' + c + '\\times ' + objTexte.compteur
        lignesAlgo.push(espaceTab1 + espaceTab + '$' + objTexte.v + '\\leftarrow ' + a + '\\times ' + objTexte.v + affichei + j3pMonome(2, 0, b, 'x') + '$')
      } else {
        lignesAlgo.push(espaceTab1 + espaceTab + '$' + objTexte.v + '\\leftarrow ' + a + '\\times ' + objTexte.v + j3pMonome(2, 0, b, 'x') + '$')
      }
      if (ds.avecFonction) {
        lignesAlgo.push(espaceTab1 + ds.textes.txtReturn + ' $' + objTexte.v + '$')
      }
    }
    let laReponse = objTexte.valInit
    stor.laReponseFausse = laReponse// Ceci ne servira que pour un programme en python où le compteur de la boucle servira au calcul des termes
    DonneesTab[0] = [objTexte.valInit]
    DonneesTab[1] = ds.formatPython ? ['', 0] : ['', 1]
    for (let i = 1; i < nFinal; i++) {
      DonneesTab[1].push(DonneesTab[1][i] + 1)
    }
    for (let i = 0; i < nFinal; i++) {
      if (ds.avecIDansBoucle) {
        laReponse = a * laReponse + b + c * DonneesTab[1][i + 1]
        if (ds.formatPython) {
          stor.laReponseFausse = a * stor.laReponseFausse + b + c * (DonneesTab[1][i + 1] + 1)
          me.logIfDebug('laReponseFausse:', stor.laReponseFausse)
        }
      } else {
        laReponse = a * laReponse + b
      }
      DonneesTab[0].push(laReponse)
    }
    const lesCalculs = []
    if (ds.avecIDansBoucle) {
      lesCalculs[0] = a + '\\times ' + avecParenthese(DonneesTab[0][0]) + ecrireCDansCalc + DonneesTab[1][1] + j3pMonome(2, 0, b, '') + '=' + DonneesTab[0][1]
      lesCalculs[1] = a + '\\times ' + avecParenthese(DonneesTab[0][1]) + ecrireCDansCalc + DonneesTab[1][2] + j3pMonome(2, 0, b, '') + '=' + DonneesTab[0][2]
    } else {
      lesCalculs[0] = a + '\\times ' + avecParenthese(DonneesTab[0][0]) + j3pMonome(2, 0, b, '') + '=' + DonneesTab[0][1]
      lesCalculs[1] = a + '\\times ' + avecParenthese(DonneesTab[0][1]) + j3pMonome(2, 0, b, '') + '=' + DonneesTab[0][2]
    }

    me.logIfDebug('DonneesTab:', DonneesTab)
    for (let i = 0; i < lignesAlgo.length; i++) {
      const ligneAlgo = j3pAddElt(stor.algo, 'p')
      j3pAffiche(ligneAlgo, '', lignesAlgo[i])
    }
    j3pStyle(stor.algo, { backgroundColor: 'white' })
    $('.purple').css('color', 'rgb(60,76,114)')
    j3pAffiche(stor.zoneCons2, '', laCons2, objTexte)
    const elt = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3, { input1: { texte: '', dynamique: true } })
    stor.zoneInput = elt.inputList[0]
    j3pRestriction(stor.zoneInput, '0-9.,\\-')
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [laReponse]
    stor.zoneInput.solutionSimplifiee = ['non valide']
    stor.objTexte = objTexte
    stor.DonneesTab = DonneesTab
    stor.lesCalculs = lesCalculs
    stor.zoneInput.addEventListener('input', j3pRemplacePoint)
    j3pFocus(stor.zoneInput)
    me.logIfDebug('laReponse:', laReponse)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne
        stor.pourc = 50

        me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourc / 100 })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        let pbUtilisationCompteur = false
        if (reponse.aRepondu && !reponse.bonneReponse && ds.avecIDansBoucle && ds.formatPython) {
          pbUtilisationCompteur = (Math.abs(stor.laReponseFausse - j3pVirgule(fctsValid.zones.reponseSaisie[0])) < Math.pow(10, -12))
        }
        // A cet instant reponse contient deux éléments :
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée
          let msgReponseManquante
          if (!fctsValid.zones.reponseSimplifiee[0][0]) {
          // c’est que la fraction de la quatrième zone de saisie est réductible
            msgReponseManquante = ds.textes.comment1
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (pbUtilisationCompteur) {
                const commentRange = j3pChaine(ds.textes.comment2, {
                  i: stor.DonneesTab[1][stor.DonneesTab[1].length - 1] + 1,
                  j: stor.DonneesTab[1][stor.DonneesTab[1].length - 1]
                })
                stor.zoneCorr.innerHTML += '<br/>' + commentRange
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
