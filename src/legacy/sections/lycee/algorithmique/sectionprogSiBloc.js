import $ from 'jquery'

import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pImporteAnnexe, j3pRandomTab, j3pRestriction, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import Blockly from 'src/lib/outils/blockly/original'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2019
        Section avec blockly pour tester une instruction contionnelle simple
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition']
  ]
}

/**
 * section progSiBloc
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.toutpetit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    for (let i = 1; i <= 2; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      j3pDetruit(stor.zoneCalc)
      j3pDetruitFenetres('Calculatrice')
    }
    const objCorr = {}
    objCorr.x = stor.obj.tabnomvar[0]
    objCorr.y = stor.obj.tabnomvar[1]
    objCorr.xval = stor.nb
    objCorr.e = j3pGetLatexMonome(1, 1, stor.obj.tabvar[0], stor.obj.tabnomvar[0]) + j3pGetLatexMonome(2, 0, stor.tabCoefInit[1], stor.obj.tabnomvar[0])
    objCorr.s = stor.tabCoefInit[0] * stor.nb + stor.tabCoefInit[1]
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1, objCorr)
    const laCorr2 = (objCorr.s === stor.nb2) ? ds.textes.corr2_2 : ds.textes.corr2_1
    objCorr.yval = stor.nb2
    objCorr.r = stor.zoneInput.reponse[0]
    j3pAffiche(stor.zoneExpli2, '', laCorr2, objCorr)
  }

  async function chargeExemple () {
    // on récupère un XMLDocument, mais vu qu’on le modifie à chaque question, on ne stocke que la string
    // (pour recréer ensuite un xmlDoc à chaque question)
    const xmlDoc = await j3pImporteAnnexe('blockly/BlocInstrucCond.xml')
    stor.xmlString = (new XMLSerializer()).serializeToString(xmlDoc)
  }

  function changeExemple (objChgt, tabType) {
    // monUrl est l’adresse du fichier xml d’origine
    // Cette fonction va le charger et modifier les valeurs de variables et les signes d’opération
    // obj.tabvar contient la valeur des variables présentes dans le XML sous la forme (var1), (var2), ... L’ordre a de l’importance
    // obj.tabsigne contient le signe qu’il faut mettre dans le XML à la place de (SIGNE1), (SIGNE2), ...
    // Ces signes sont "ADD","MINUS","MULTIPLY" et "DIVIDE"
    // obj.tabtexte contient les textes qu’il faut mettre dans le XML à la place de (texte1), (texte2), ...
    // obj.tabnomVar contient les noms des variables qu’il faut mettre dans le XML à la place de (nomVar1), (nomVar2), ...
    // tabType peut contenir les valeurs signe, var, texte, nomvar
    function gererFils (xml) {
      for (const element of xml.childNodes) {
        if (element.nodeType === 1) {
          // ELEMENT_NODE
          gererFils(element)
        } else if (element.nodeType === 3 || element.nodeType === 4) {
          // TEXT_NODE ou CDATA_SECTION_NODE
          for (const type of tabType) {
            const typeRegExp = new RegExp('\\(' + type + '([0-9]+)\\)', 'g')
            if (typeRegExp.test(element.nodeValue)) {
              element.nodeValue = element.nodeValue.replace(typeRegExp, function (match, num) {
                const nb = Number(num)
                const value = objChgt['tab' + type][nb - 1]
                if (value == null) console.error(`Paramètres incohérents avec le programme utilisé (pas trouvé ${match} dans le tableau passé en paramètre, mais il est dans le xml)`)
                return value
              })
            }
          }
        }
      }
      return xml
    }

    const parser = new window.DOMParser()
    const xmlDoc = parser.parseFromString(stor.xmlString, 'text/xml')

    const xml = gererFils(xmlDoc)
    let x = xml.firstChild
    while (x.nodeType !== 1 && x.nextSibling) {
      x = x.nextSibling
    }
    try {
      Blockly.getMainWorkspace().clear()
      Blockly.Xml.domToWorkspace(x, Blockly.getMainWorkspace())
    } catch (error) {
      j3pShowError(error, { message: 'Programme invalide' })
    }
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Programme de calcul',
        // on donne les phrases de la consigne
        consigne1: 'On donne ci-dessous un programme réalisé avec Blockly.',
        consigne2: 'Si on saisit la valeur $£n$ pour £x et la valeur $£m$ pour £y, le texte renvoyé par le programme est @1@.',
        var3: 'TexteReponse',
        texte1: 'Bravo|Bien',
        texte2: 'Perdu|Echec',
        // indic pour le format de la réponse
        info: 'Il faut faire attention à respecter les minuscules et majuscules.',
        info2: 'Il n’y a qu’une chance pour répondre&nbsp;!',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: '',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'Si $£x=£{xval}$, alors $£e=£s$.',
        corr2_1: 'Donc si on donne à $£y$ la valeur $£{yval}$, alors l’égalité n’est pas vérifiée et le texte renvoyé par le programme est £r.',
        corr2_2: 'Donc si on donne à $£y$ la valeur $£{yval}$, alors l’égalité est vérifiée et le texte renvoyé par le programme est £r.'
      },

      pe: 0
    }
  }

  async function initSection () {
    await chargeExemple()
    enonceMain()
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
      j3pAfficheCroixFenetres('Calculatrice')
    }
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.divBlockly = j3pAddElt(stor.zoneCons2, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1)
    const monXml = '<xml id="toolbox" style="display: none"></xml>'
    Blockly.inject(stor.divBlockly, {
      // media: "../keys_new/media/",
      zoom: {
        controls: false,
        wheel: false,
        startScale: 1,
        maxScale: 2,
        minScale: 0.5,
        scaleSpeed: 1.1
      },
      toolbox: $(monXml).find('#toolbox')[0], // telechargement du menu : ici il n’y en a pas
      readOnly: true
    })
    stor.divBlockly.style.height = '230px'
    stor.divBlockly.style.width = '670px'
    Blockly.svgResize(Blockly.mainWorkspace)

    const tabCoef = []
    tabCoef[0] = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(2, 6)
    do {
      tabCoef[1] = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
    } while (Math.abs(Math.abs(tabCoef[0]) - Math.abs(tabCoef[1])) < Math.pow(10, -12))
    tabCoef.push('+')
    stor.tabCoefInit = [...tabCoef]
    const tabSignes = ['ADD']
    if (tabCoef[1] < 0) {
      tabSignes[0] = 'MINUS'
      tabCoef[1] = -tabCoef[1]
      tabCoef[2] = '-'
    }
    stor.obj = {}
    stor.obj.tabvar = [...tabCoef]
    stor.obj.tabsigne = tabSignes
    stor.obj.tabnomvar = j3pRandomTab([['x', 'y'], ['t', 'y'], ['a', 'b'], ['m', 'p']], [0.25, 0.25, 0.25, 0.25])
    stor.obj.tabnomvar.push(ds.textes.var3)
    stor.obj.tabtexte = []
    stor.obj.tabtexte.push(ds.textes.texte1.split('|')[j3pGetRandomInt(0, 1)])
    stor.obj.tabtexte.push(ds.textes.texte2.split('|')[j3pGetRandomInt(0, 1)])

    me.logIfDebug('obj:', stor.obj)

    changeExemple(stor.obj, ['signe', 'var', 'texte', 'nomvar'])

    // Début de la question
    stor.nb = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
    stor.nb2 = tabCoef[0] * stor.nb + stor.tabCoefInit[1]
    const egalite = j3pGetRandomInt(0, 1)
    if (egalite === 0) {
      stor.nb2 = j3pRandomTab([-stor.nb2, tabCoef[0] * stor.nb - stor.tabCoefInit[1], tabCoef[0] + stor.nb + stor.tabCoefInit[1]], [0.3333, 0.3333, 0.3334])
    }
    const laReponse = (egalite === 0) ? stor.obj.tabtexte[1] : stor.obj.tabtexte[0]
    const elt = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2,
      {
        n: stor.nb,
        m: stor.nb2,
        x: stor.obj.tabnomvar[0],
        y: stor.obj.tabnomvar[1],
        input1: { texte: '', dynamique: true, maxchars: 7 }
      })
    stor.zoneInput = elt.inputList[0]
    const info1 = j3pAddElt(stor.zoneCons3, 'div')
    const info2 = j3pAddElt(stor.zoneCons3, 'div')
    j3pAffiche(info1, '', ds.textes.info)
    j3pStyle(stor.zoneCons3, { fontStyle: 'italic', fontSize: '0.9em', paddingTop: '10px' })
    if (ds.nbchances === 1) {
      j3pAffiche(info2, '', ds.textes.info2)
    }
    stor.zoneInput.typeReponse = ['texte']
    stor.zoneInput.reponse = [laReponse, laReponse + ' ']
    j3pRestriction(stor.zoneInput, 'a-zA-Z')
    const mesZonesSaisie = [stor.zoneInput.id]
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)

    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        // Construction de la page
        me.construitStructurePage({ structure: ds.structure })
        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        initSection()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      const reponse = fctsValid.validationGlobale()
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        me.reponseManquante(stor.zoneCorr)
        // on reste dans l’état correction
        return me.finCorrection()
      }

      // Une réponse a été saisie
      if (reponse.bonneReponse) {
        // Bonne réponse
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        afficheCorrection(true)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        me.cacheBoutonValider()
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // ici il a encore la possibilité de se corriger
        return me.finCorrection()
      }

      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(false)
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break
    } // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
