import $ from 'jquery'
import { j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pArrondi, j3pChaine, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pMonome, j3pRandomTab, j3pRemplacePoint, j3pRestriction, j3pSetProps, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Mai 2019
        Déterminer la valeur de variables à l’issue d’une boucle pour
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['formatPython', false, 'boolean', 'L’algorithme est écrit par défaut en langage naturel, mais cela peut être fait dans le langage python.'],
    ['avecFonction', false, 'boolean', 'Si ce paramètre vaut true, alors on présentera l’algorithme sous la forme d’une fonction (en python si le paramètre formatPython vaut aussi true).'],
    ['avecNDansBoucle', false, 'boolean', 'Lorsque que ce paramètre vaut true, alors on trouvera le compteur de la boucle "tant que" dans le calcul du terme.'],
    ['avecArgument', false, 'boolean', 'Le nombre de passages dans la boucle sera présenté comme un argument de la fonction (si avecFonction vaut true) lorsque ce paramètre vaut true.']
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.toutpetit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1)
    // Construction du tableau :
    const largeurTab = me.zonesElts.MG.getBoundingClientRect().width
    const largeurTexte = $(stor.zoneExpli1).textWidth(stor.condition, me.styles.toutpetit.correction.fontFamily, me.styles.toutpetit.correction.fontSize)
    const largeurColonne = (largeurTab - largeurTexte - 5) / (stor.DonneesTab[0].length)
    j3pSetProps(stor.zoneExpli2, { width: largeurTab + 'px' })
    const span = j3pAddElt(stor.zoneExpli2, 'span')
    const table = j3pAddElt(span, 'table')
    j3pSetProps(table, { width: '100%', borderColor: stor.zoneCons3.style.color, border: 2, cellSpacing: 0 })
    const divLegendes = []
    for (let i = 0; i < 3; i++) {
      const ligne = j3pAddElt(table, 'tr')
      const td = j3pAddElt(ligne, 'td')
      j3pSetProps(td, { align: 'center', width: largeurColonne + 'px' })
      divLegendes.push(j3pAddElt(td, 'div'))
      for (let j = 0; j < stor.DonneesTab[0].length; j++) {
        const td = j3pAddElt(ligne, 'td')
        j3pSetProps(td, { align: 'center', width: largeurTexte + 'px' })
        if (i === 2) {
          j3pAddTxt(td, (j === stor.DonneesTab[0].length - 1) ? ds.textes.faux : ds.textes.vrai)
        } else {
          j3pAddTxt(td, (i === 0) ? stor.DonneesTab[i][j] : j3pVirgule(j3pArrondi(stor.DonneesTab[1][j], stor.donneesArrondies)))
        }
      }
    }
    if (ds.formatPython) {
      j3pAffiche(divLegendes[0], '', stor.objTexte.n)
      j3pAffiche(divLegendes[1], '', stor.objTexte.v)
      j3pAffiche(divLegendes[2], '', '$' + stor.condition + '$')
    } else {
      j3pAffiche(divLegendes[0], '', '$' + stor.objTexte.n + '$')
      j3pAffiche(divLegendes[1], '', '$' + stor.objTexte.v + '$')
      j3pAffiche(divLegendes[2], '', '$' + stor.condition + '$')
    }
    j3pStyle(divLegendes[2], { fontSize: '0.9em' })
    const laCorr2 = (ds.avecFonction) ? ds.textes.corr2_2 : ds.textes.corr2_1
    const variablen = (ds.formatPython) ? '\\text{' + stor.objTexte.n + '}' : stor.objTexte.n
    const variablev = (ds.formatPython) ? '\\text{' + stor.objTexte.v + '}' : stor.objTexte.v
    if (stor.donneesArrondies > 0) {
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr3, { p: stor.donneesArrondies })
    }
    j3pAffiche(stor.zoneExpli4, '', laCorr2, {
      v: variablen,
      r: stor.zoneInput.reponse[0]
    })
    // Compléments avec les explications pour remplir le tableau:
    j3pStyle(stor.zoneExpli5, { fontSize: '0.9em', backgroundColor: 'white', border: 'thick double #32a1ce' })
    for (let i = 1; i <= 10; i++)stor['comp' + i] = j3pAddElt(stor.zoneExpli5, 'div')
    j3pAffiche(stor.comp1, '', ds.textes.corr4)
    const ligne1 = (ds.avecFonction) ? 2 : 1
    const objComp = {
      vInit: stor.DonneesTab[1][0],
      v: variablev,
      nInit: stor.DonneesTab[0][0],
      nPlusUn: stor.DonneesTab[0][1],
      nCons: variablen,
      cond: stor.condition,
      l1: ligne1,
      l2: ligne1 + 1,
      l3: ligne1 + 2,
      l4: ligne1 + 3,
      l5: ligne1 + 4
    }
    objComp.boucle = (ds.formatPython) ? 'while' : 'tant que'
    objComp.i = (ds.formatPython) ? '\\text{' + stor.objTexte.compteur + '}' : stor.objTexte.compteur
    objComp.c = stor.lesCalculs[0]
    j3pAffiche(stor.comp2, '', ds.textes.corr5, objComp)
    j3pAffiche(stor.comp3, '', ds.textes.corr6, objComp)
    j3pAffiche(stor.comp4, '', ds.textes.corr7, objComp)
    j3pAffiche(stor.comp5, '', ds.textes.corr8, objComp)
    j3pAffiche(stor.comp6, '', ds.textes.corr9, objComp)
    j3pAffiche(stor.comp7, '', ds.textes.corr10, objComp)
    objComp.c = stor.lesCalculs[1]
    objComp.nPlusUn = stor.DonneesTab[0][2]
    j3pAffiche(stor.comp8, '', ds.textes.corr8, objComp)
    j3pAffiche(stor.comp9, '', ds.textes.corr9, objComp)
    j3pAffiche(stor.comp10, '', ds.textes.corr11, objComp)
  }

  function avecParenthese (nb) {
    // écrit bien le nombre dans un calcul (cad avec parenthèses si ce nombre est négatif)
    if (j3pCalculValeur(nb) < 0) {
      return '(' + nb + ')'
    } else {
      return nb
    }
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      formatPython: false,
      avecFonction: false,
      avecNDansBoucle: false,
      avecArgument: false,
      /*
      Les textes présents dans la section
      Sont donc accessibles dans le code par donneesSection.textes.consigne1
      Possibilité d’écrire dans le code :
      var lestextes: donneesSection.textes;
      puis accès à l’aide de lestextes.consigne1
      */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Interpréter une boucle non bornée',
        // on donne les phrases de la consigne
        consigne1_1: 'On propose ci-contre un algorithme&nbsp;:',
        consigne1_2: 'On propose ci-contre un algorithme sous la forme d’une fonction&nbsp;:',
        consigne1_3: 'On propose ci-contre un programme en python&nbsp;:',
        consigne1_4: 'On propose ci-contre un programme en python écrit sous la forme d’une fonction&nbsp;:',
        consigne2_1: 'Quelle est la valeur de la variable $£{nCons}$ à l’issue de cet algorithme&nbsp;?',
        consigne2_2: 'Quelle valeur est renvoyée par l’algorithme lorsqu’on écrit £{formule}&nbsp;?',
        consigne2_4: 'Quelle valeur est renvoyée par python lorsqu’on écrit £{formule}&nbsp;?',
        consigne3: 'Réponse : @1@.',
        nomf: 'seuil',
        txtFonction: 'fonction',
        txtReturn: 'renvoyer',
        txtTantque: 'Tant que £i:',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'La réponse attendue devrait être un entier !',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'On peut présenter l’évolution des valeurs des variables de cet algorithme dans un tableau',
        corr2_1: 'Donc $£v$ a pour valeur £r à l’issue de l’algorithme.',
        corr2_2: 'Donc la valeur renvoyée est £r.',
        corr3: 'Les valeurs sont éventuellement arrondies à $10^{-£p}$.',
        vrai: 'Vrai',
        faux: 'Faux',
        // Phrases pour expliquer la construction du tableau
        corr4: '<u>Explications pour compléter le tableau :</u>',
        corr5: 'ligne £{l1} : $£{nCons}$ vaut $£{nInit}$&nbsp;;',
        corr6: 'ligne £{l2} : $£v$ vaut $£{vInit}$&nbsp;;',
        corr7: 'ligne £{l3} : on entre dans la boucle "£{boucle}" car $£{cond}$&nbsp;;',
        corr8: 'ligne £{l4} : $£v$ devient $£c$&nbsp;;',
        corr9: 'ligne £{l5} : $£{nCons}$ devient £{nPlusUn}&nbsp;;',
        corr10: 'on revient à la ligne £{l3} car $£{cond}$&nbsp;;',
        corr11: "ainsi de suite jusqu'à ce que la condition $£{cond}$ ne soit plus vérifiée..."
      },
      pe: 0
    }
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.algo = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')

    j3pStyle(stor.zoneCalc, { padding: '15px 0px' })
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    j3pAfficheCroixFenetres('Calculatrice')
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })
    const laCons1 = (ds.formatPython)
      ? (ds.avecFonction) ? ds.textes.consigne1_4 : ds.textes.consigne1_3
      : (ds.avecFonction) ? ds.textes.consigne1_2 : ds.textes.consigne1_1
    const laCons2 = (ds.formatPython)
      ? (ds.avecFonction) ? ds.textes.consigne2_4 : ds.textes.consigne2_1
      : (ds.avecFonction) ? ds.textes.consigne2_2 : ds.textes.consigne2_1
    j3pAffiche(stor.zoneCons1, '', laCons1)
    let puis, a, uInit, seuil, suiteCroissante, b, u, c, ecrireCDansCalc
    if (ds.avecNDansBoucle) {
      // u=au+ci+b a est positif, uInit et c sont du même signe de sorte que u tende vers +/- infini selon le signe de uInit (il faut juste faire attention à b
      do {
        a = j3pGetRandomInt(2, 6)
        c = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 4)
        uInit = (c < 0) ? j3pGetRandomInt(-5, 1) : j3pGetRandomInt(-1, 5)
        u = uInit
        do {
          b = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(1, 5)
        } while (Math.abs(a - b) < Math.pow(10, -12))
        // Si au bout de 3 terme, u n’est pas du signe de c, alors je peux avoir un pb
        for (let i = 0; i <= 2; i++) {
          u = a * u + c * i + b
        }
      } while (u * c < 0)
      suiteCroissante = (c > 0)
      seuil = uInit
      for (let i = 1; i <= j3pGetRandomInt(3, 5); i++) {
        seuil = a * seuil + c * (i - 1) + b
      }
      puis = 0
      while (Math.abs(seuil) > Math.pow(10, puis)) {
        puis++
      }
      me.logIfDebug('a:', a, '  c:', c, '  b:', b, '   uInit:', uInit, '   seuil:' + seuil, '   puis:', puis)
      seuil = Math.pow(10, puis)
      seuil = (suiteCroissante) ? seuil : -seuil
      ecrireCDansCalc = (c === 1)
        ? '+'
        : (c === -1)
            ? '-'
            : (c < 0) ? c + '\\times ' : '+' + c + '\\times '
    } else {
      // u=au+b
      const choixa = j3pGetRandomInt(0, 1)
      a = (choixa === 0) ? j3pRandomTab([0.8, 0.5, 0.9, 0.75], [0.25, 0.25, 0.25, 0.25]) : j3pGetRandomInt(2, 6)
      uInit = j3pGetRandomInt(0, 5)
      seuil = uInit
      suiteCroissante = true
      let ecart, borne
      if (choixa === 0) {
        // La raison de la suite géo intermédiaire est comprise entre 0 et 1
        borne = (1 - a) * uInit
        const croissante = j3pGetRandomInt(0, 1)// Si b>borne, alors (u_n) est croissante. Sinon, elle est décroissante
        if (croissante === 0) {
          suiteCroissante = false
          // suite décroissante, donc il faut que je détermine un seuil supérieur à b/a-1
          b = Math.floor(borne) - j3pGetRandomInt(3, 7) // b doit être inférieur à (1-a)*u_0
          for (let i = 1; i <= j3pGetRandomInt(3, 5); i++) {
            seuil = a * seuil + b
          }
          ecart = Math.abs(seuil - b / (1 - a))
          puis = 0
          while (ecart < Math.pow(10, puis)) {
            puis--
          }
          me.logIfDebug('   seuil:' + seuil, '   puis:', puis)
          seuil = (puis === 0) ? Math.round(b / (1 - a) + Math.floor(ecart)) : Math.round(Math.pow(10, 8) * (Math.round(b / (1 - a)) + Math.pow(10, puis))) / Math.pow(10, 8)
        } else {
          // suite croissante, donc il faut que je détermine un seuil inférieur à b/a-1
          b = Math.ceil(borne) + j3pGetRandomInt(3, 7)// b doit être supérieur à (1-a)*u_0
          for (let i = 1; i <= j3pGetRandomInt(3, 5); i++) {
            seuil = a * seuil + b
          }
          ecart = Math.abs(seuil - b / (1 - a))
          puis = 0
          while (ecart < Math.pow(10, puis)) {
            puis--
          }
          me.logIfDebug('   seuil:' + seuil, '   puis:', puis)
          seuil = (puis === 0) ? Math.round(b / (1 - a) - Math.floor(ecart)) : Math.round(Math.pow(10, 8) * (Math.round(b / (1 - a)) - Math.pow(10, puis))) / Math.pow(10, 8)
        }
      } else {
        // raison supérieure à 1 donc la suite tend vers +infty/ On fera en sorte que la suite soit croissante (b>(1-a)*u_0
        borne = (1 - a) * uInit
        b = Math.ceil(borne) + j3pGetRandomInt(3, 7)
        for (let i = 1; i <= j3pGetRandomInt(3, 5); i++) {
          seuil = a * seuil + b
        }
        puis = 0
        while (seuil > Math.pow(10, puis)) {
          puis++
        }
        me.logIfDebug('   seuil:' + seuil, '   puis:', puis)
        seuil = Math.pow(10, puis)
      }
    }
    const DonneesTab = []
    const objTexte = {}
    // Nom de la variables présente dans l’algor et qui contiendra la réponse
    objTexte.v = j3pRandomTab(['a', 'b', 'u', 'v', 't'], [0.2, 0.2, 0.2, 0.2, 0.2])
    objTexte.n = j3pRandomTab(['n', 'm', 'p'], [0.333, 0.333, 0.334])
    objTexte.nCons = (ds.formatPython) ? '\\text{' + objTexte.n + '}' : objTexte.n
    objTexte.vCons = (ds.formatPython) ? '\\text{' + objTexte.v + '}' : objTexte.v
    objTexte.rangInit = j3pGetRandomInt(0, 1)
    objTexte.nomArg = j3pRandomTab(['s', 'S'], [0.5, 0.5])
    objTexte.nomArgTxt = (ds.avecFonction && ds.avecArgument) ? objTexte.nomArg : ''
    objTexte.nomFct = ds.textes.nomf
    objTexte.valInit = uInit
    objTexte.seuil = (ds.avecFonction && ds.avecArgument) ? '\\text{' + objTexte.nomArg + '}' : seuil
    objTexte.formule = (ds.avecFonction)
      ? (ds.avecArgument)
          ? (ds.formatPython)
              ? objTexte.nomFct + '(' + seuil + ')'
              : objTexte.nomFct + '(' + j3pVirgule(seuil) + ')'
          : objTexte.nomFct + '()'
      : ''

    DonneesTab[0] = [objTexte.rangInit]
    DonneesTab[1] = [objTexte.valInit]
    if (suiteCroissante) {
      while (DonneesTab[1][DonneesTab[1].length - 1] < seuil) {
        DonneesTab[0].push(DonneesTab[0][DonneesTab[0].length - 1] + 1)
        if (ds.avecNDansBoucle) {
          DonneesTab[1].push(a * DonneesTab[1][DonneesTab[1].length - 1] + c * DonneesTab[0][DonneesTab[1].length - 1] + b)
        } else {
          DonneesTab[1].push(a * DonneesTab[1][DonneesTab[1].length - 1] + b)
        }
      }
    } else {
      while (DonneesTab[1][DonneesTab[1].length - 1] > seuil) {
        DonneesTab[0].push(DonneesTab[0][DonneesTab[0].length - 1] + 1)
        if (ds.avecNDansBoucle) {
          DonneesTab[1].push(a * DonneesTab[1][DonneesTab[1].length - 1] + c * DonneesTab[0][DonneesTab[1].length - 1] + b)
        } else {
          DonneesTab[1].push(a * DonneesTab[1][DonneesTab[1].length - 1] + b)
        }
      }
    }
    stor.donneesArrondies = (String(DonneesTab[1][DonneesTab[1].length - 1]).indexOf('.') === -1) ? 0 : Math.min(3, String(DonneesTab[1][DonneesTab[1].length - 1]).split('.')[1].length)

    // Création de l’algo
    const lignesAlgo = []
    let espaceTab = ''
    for (let i = 1; i <= 5; i++) {
      espaceTab += '&nbsp;'
    }
    const signeIneg = (suiteCroissante) ? '<' : '>'
    if (!ds.formatPython) {
      stor.maVarSortie = '\\text{' + stor.maVarSortie + '}'
    }
    const espaceTab1 = (ds.avecFonction) ? espaceTab : ''
    stor.condition = objTexte.vCons + signeIneg + '&nbsp;' + objTexte.seuil
    let affichen
    if (ds.formatPython) {
      if (ds.avecFonction) {
        lignesAlgo.push("<span class='bleu'>def</span> " + objTexte.nomFct + '(' + objTexte.nomArgTxt + '):')
      }
      lignesAlgo.push(espaceTab1 + objTexte.n + '=' + objTexte.rangInit)
      lignesAlgo.push(espaceTab1 + objTexte.v + '=' + objTexte.valInit)
      lignesAlgo.push(espaceTab1 + "<span class='bleu'>while</span> $" + stor.condition + '$:')
      if (ds.avecNDansBoucle) {
        affichen = (Math.abs(Math.abs(c) - 1) < Math.pow(10, -12))
          ? j3pMonome(2, 1, c, objTexte.n)
          : (c < 0) ? c + '*' + objTexte.n : '+' + c + '*' + objTexte.n
        lignesAlgo.push(espaceTab1 + espaceTab + objTexte.v + '=' + a + '*' + objTexte.v + affichen + j3pMonome(2, 0, b, 'x'))
      } else {
        lignesAlgo.push(espaceTab1 + espaceTab + objTexte.v + '=' + a + '*' + objTexte.v + j3pMonome(2, 0, b, 'x'))
      }
      lignesAlgo.push(espaceTab1 + espaceTab + objTexte.n + '=' + objTexte.n + '+1')
      if (ds.avecFonction) {
        lignesAlgo.push(espaceTab1 + "<span class='bleu'>return</span> " + objTexte.n)
      }
    } else {
      if (ds.avecFonction) {
        lignesAlgo.push(ds.textes.txtFonction + ' ' + objTexte.nomFct + '$(' + objTexte.nomArgTxt + ')$:')
      }
      lignesAlgo.push(espaceTab1 + '$' + objTexte.n + '\\leftarrow ' + objTexte.rangInit + '$')
      lignesAlgo.push(espaceTab1 + '$' + objTexte.v + '\\leftarrow ' + objTexte.valInit + '$')
      if (ds.avecArgument) {
        lignesAlgo.push(espaceTab1 + j3pChaine(ds.textes.txtTantque, { i: '$' + stor.condition + '$' }))
      } else {
        lignesAlgo.push(espaceTab1 + j3pChaine(ds.textes.txtTantque, { i: '$' + objTexte.vCons + signeIneg + objTexte.seuil + '$' }))
      }
      if (ds.avecNDansBoucle) {
        affichen = (Math.abs(Math.abs(c) - 1) < Math.pow(10, -12))
          ? j3pMonome(2, 1, c, objTexte.n)
          : (c < 0) ? c + '\\times ' + objTexte.n : '+' + c + '\\times ' + objTexte.n
        lignesAlgo.push(espaceTab1 + espaceTab + '$' + objTexte.v + '\\leftarrow ' + a + '\\times ' + objTexte.v + affichen + j3pMonome(2, 0, b, 'x') + '$')
      } else {
        lignesAlgo.push(espaceTab1 + espaceTab + '$' + objTexte.v + '\\leftarrow ' + a + '\\times ' + objTexte.v + j3pMonome(2, 0, b, 'x') + '$')
      }
      lignesAlgo.push(espaceTab1 + espaceTab + '$' + objTexte.n + '\\leftarrow ' + objTexte.n + '+1$')
      if (ds.avecFonction) {
        lignesAlgo.push(espaceTab1 + ds.textes.txtReturn + ' $' + objTexte.n + '$')
      }
    }
    const laReponse = DonneesTab[0][DonneesTab[0].length - 1]
    const lesCalculs = []
    if (ds.avecNDansBoucle) {
      lesCalculs[0] = a + '\\times ' + avecParenthese(DonneesTab[1][0]) + ecrireCDansCalc + DonneesTab[0][0] + j3pMonome(2, 0, b, '') + '=' + j3pArrondi(DonneesTab[1][1], 5)
      lesCalculs[1] = a + '\\times ' + avecParenthese(DonneesTab[1][1]) + ecrireCDansCalc + DonneesTab[0][1] + j3pMonome(2, 0, b, '') + '=' + j3pArrondi(DonneesTab[1][2], 5)
    } else {
      lesCalculs[0] = a + '\\times ' + avecParenthese(DonneesTab[1][0]) + j3pMonome(2, 0, b, '') + '=' + j3pArrondi(DonneesTab[1][1], 5)
      lesCalculs[1] = a + '\\times ' + avecParenthese(DonneesTab[1][1]) + j3pMonome(2, 0, b, '') + '=' + j3pArrondi(DonneesTab[1][2], 5)
    }
    me.logIfDebug('DonneesTab:', DonneesTab, '\nlaReponse:', laReponse)
    for (let i = 0; i < lignesAlgo.length; i++) {
      const ligne = j3pAddElt(stor.algo, 'div')
      j3pAffiche(ligne, '', lignesAlgo[i])
    }
    j3pStyle(stor.algo, { backgroundColor: 'white' })
    $('.purple').css('color', 'rgb(60,76,114)')
    j3pAffiche(stor.zoneCons2, '', laCons2, objTexte)
    const elt = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3, { input1: { texte: '', dynamique: true } })
    stor.zoneInput = elt.inputList[0]
    j3pRestriction(stor.zoneInput, '0-9.,\\-')
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [laReponse]
    stor.zoneInput.solutionSimplifiee = ['non valide']
    stor.objTexte = objTexte
    stor.DonneesTab = DonneesTab
    stor.lesCalculs = lesCalculs
    stor.zoneInput.addEventListener('input', j3pRemplacePoint)
    j3pFocus(stor.zoneInput)
    me.logIfDebug('laReponse:', laReponse)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page

        // Le paramètre définit la largeur relative de la première colonne

        me.construitStructurePage({ structure: ds.structure })

        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        let estEntier = true
        if (reponse.aRepondu && !reponse.bonneReponse) {
          estEntier = /[0-9]/g.test(fctsValid.zones.reponseSaisie[0])
        }
        // A cet instant reponse contient deux éléments :
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            if (!estEntier) {
              stor.zoneCorr.innerHTML = ds.textes.comment1
            }
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
