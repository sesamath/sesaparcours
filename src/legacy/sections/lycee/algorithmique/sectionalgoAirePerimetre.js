import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pChaine, j3pDetruit, j3pElement, j3pGetNewId, j3pGetRandomInt, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { init, desactiverEditeur, getTextAce, runPython, runPythonCode, setTextAce, verifDeuxPoints } from 'src/legacy/outils/algo/algo_python'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitToutesLesFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/* global ace */
/*
  DENIAUD Rémi
  mai 2019
  section pour compléter un algorithme en python afin de calculer l’aire ou le périmètre de figures de base
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    // ["nbrepetitions",3,"entier","Nombre de répétitions de la section"],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['remplissage', 'partiel', 'liste', 'Ce paramètre peut prendre trois valeurs distinctes&nbsp;:<br>- "vide" : le programme est à compléter entièrement&nbsp;;<br>- "partiel" : (par défaut) on donne la construction avec la présence des instructions conditionnelles&nbsp;;<br>- "complet" : on ne demande que les formules des aires et périmètres, le reste étant complété.', ['vide', 'partiel', 'complet']],
    ['aideConsole', true, 'boolean', 'Lorsque ce paramètre vaut true, la console est préremplie avec une utilisation de la fonction. Sinon c’est à l’élève d’écrire un test possible.']
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Algorithmes de calculs d’aire et de périmètre',
  // on donne les phrases de la consigne
  consigne1: 'Compléter la fonction ci-contre écrite dans le langage python de sorte que&nbsp;:',
  consigne2_1: '- si le paramètre choix vaut "aire", alors la fonction renvoie l’aire d’un carré de côté c&nbsp;;',
  consigne2_2: '- si le paramètre choix vaut "aire", alors la fonction renvoie l’aire d’un disque de rayon r&nbsp;;',
  consigne2_3: '- si le paramètre choix vaut "aire", alors la fonction renvoie l’aire d’un rectangle de longueur L et de largeur l&nbsp;;',
  consigne3: '- si le paramètre choix vaut "périmètre", alors la fonction renvoie son périmètre&nbsp;;',
  consigne3Bis: '- sinon, la fonction renvoie son périmètre.',
  consigne4: '- dans les autres cas, elle renvoie un message d’erreur.',
  consigne5: 'Ne pas hésiter à tester plusieurs appels de la fonction grâce à la console.',
  consigneErreurChoix: 'le choix proposé n’est pas reconnu',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'L’algorithme doit être complété&nbsp;!',
  comment2: 'A la fin de chaque ligne contenant un mot clé, on devrait avoir deux points (par exemple ligne £l)&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Voici dans la fenêtre ci-contre un programme répondant à la question.',
  correctionAlgo: 'Programme possible',
  pythonErreur: 'Ton programme a bogué pour',
  pythonDiag1: 'Le résultat est faux lorsqu’on teste avec les valeurs ',
  pythonDiag2: 'Avec ton code, on obtient&nbsp;: ',
  pythonDiag3: 'alors qu’on devrait obtenir&nbsp;: ',
  sortie: 'Sortie',
  executer: 'Exécuter',
  reinitialiser: 'Réinitialiser',
  editeur: 'Editeur :',
  console: 'Console :'
}

/**
 * section algoAirePerimetre
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let laConsole
  // ---------------------------------------------------------------------------
  // Vérification de code
  // ---------------------------------------------------------------------------

  function execute () {
    runPython(stor.editor, laConsole)
  }

  function reinitialiser () {
    setTextAce(stor.editor, stor.algoInit)
  }

  function verifierCodePython (algoEleve, algoSecret, entrees, typeSortie) {
    me.logIfDebug('algoEleve=', algoEleve, '\nalgoSecret=', algoSecret, '\nentrees=', entrees)
    let lignes, ligne
    // on ajoute les entrees à tester
    let debutTest = ''
    let cle
    let valeur
    for (let k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      debutTest = debutTest + cle + ' = ' + valeur + '\n'
    }
    me.logIfDebug('debutTest', debutTest, '\nalgoEleve avant', algoEleve)
    lignes = algoEleve.split('\n')
    let algoEleveTest = debutTest
    for (let i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.includes('input(')) continue
      algoEleveTest = algoEleveTest + ligne + '\n'
    }
    me.logIfDebug('algoEleve apres', algoEleveTest, '\nalgo secret avant', algoSecret)
    lignes = algoSecret.split('\n')
    let algoSecretTest = debutTest
    for (let i = 0; i < (lignes.length); i++) {
      ligne = lignes[i]
      if (ligne === null) continue
      if (ligne.includes('input(')) continue
      algoSecretTest = algoSecretTest + ligne + '\n'
    }
    me.logIfDebug('algo secret apres', algoSecretTest)
    // on exécute algoSecretTest
    runPythonCode(algoSecretTest)
    let sortieSecret = stor.pre.innerHTML
    me.logIfDebug('sortieSecret=(', sortieSecret + ')')
    // on exécute algoEleveTest
    runPythonCode(algoEleveTest)
    let sortieEleve = stor.pre.innerHTML
    me.logIfDebug('sortieEleve=(', sortieEleve + ')')
    // on regarde si le test élève s’exécute
    let diagnostique
    if (sortieEleve.includes('Error')) {
      diagnostique = textes.pythonErreur
      for (let k = 0; k < Object.keys(entrees).length; k++) {
        cle = Object.keys(entrees)[k]
        valeur = Object.values(entrees)[k]
        diagnostique = diagnostique + ' ' + cle + '=' + valeur
      }
      stor.pre.innerHTML = diagnostique + '\n' + stor.pre.innerHTML
      return false
    }
    // on compare les 2 exécutions
    sortieSecret = sortieSecret.split('\n').join('')
    sortieSecret = sortieSecret.split('<br>').join('')
    sortieSecret.replace(/\r\n/g, '')
    sortieEleve = sortieEleve.split('\n').join('')
    sortieEleve = sortieEleve.split('<br>').join('')
    sortieEleve.replace(/\r\n/g, '')
    const identique = (typeSortie === 'number') ? (Math.abs(Number(sortieSecret) - Number(sortieEleve)) < Math.pow(10, -12)) : (sortieSecret === sortieEleve)

    if (identique) {
      stor.pre.innerHTML = ''
      return true
    }
    // on explique l’échec
    diagnostique = textes.pythonDiag1
    for (let k = 0; k < Object.keys(entrees).length; k++) {
      cle = Object.keys(entrees)[k]
      valeur = Object.values(entrees)[k]
      diagnostique = diagnostique + ' ' + cle + '=' + valeur
    }
    diagnostique = diagnostique + '\n' + textes.pythonDiag2 + sortieEleve
    diagnostique = diagnostique + '\n' + textes.pythonDiag3 + sortieSecret
    stor.pre.innerHTML = diagnostique
    return false
  }

  function creationEditeurPython () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')

    // je m’occupe d’abord de la construction de l’éditeur
    j3pAffiche(stor.zoneCons1, '', textes.editeur)
    stor.idEditor = j3pGetNewId('idEditor')
    stor.divEditor = j3pAddElt(stor.zoneCons2, 'div', '', { id: stor.idEditor })
    j3pStyle(stor.divEditor, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    stor.editor = ace.edit(stor.idEditor)
    stor.divEditor.style.height = '170px'
    stor.divEditor.style.width = '95%'
    stor.editor.resize()
    stor.editor.getSession().setUseSoftTabs(false)
    // stor.editor.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    stor.editor.getSession().setMode('ace/mode/python')
    stor.editor.setFontSize('12pt')
    stor.editor.focus()
    // Sk.editor = stor.editor
    // et maintenant, place à la console
    stor.idConsole = j3pGetNewId('idConsole')
    stor.divConsole = j3pAddElt(stor.zoneCons4, 'div', '', { id: stor.idConsole })
    j3pStyle(stor.divConsole, { border: 'solid 1px gray', height: '200px', width: '400px', fontSize: '10pt' })
    laConsole = ace.edit(stor.idConsole)
    stor.divConsole.style.height = '20px'
    stor.divConsole.style.width = '95%'
    stor.divConsole.style.marginTop = '10px'
    stor.divConsole.style.marginBottom = '10px'
    // laConsole.getSession().setMode(j3pBaseUrl+"/ace/mode/python");
    laConsole.getSession().setMode('ace/mode/python')
    laConsole.setFontSize('12pt')

    /* console.log(stor.divConsole.childNodes)
    console.log(stor.divConsole.childNodes[1].childNodes)
    console.log(stor.divConsole.childNodes[1].childNodes[0].childNodes) */
    stor.avecConsole = true
    try {
      // J’ai eu un signalement où cet objet HTML n’existait pas ce qui faisait planter la section
      // Je ne vois pas comment ce peut être possible
      stor.divConsole.childNodes[1].childNodes[0].childNodes[0].innerHTML = '>>>'
      stor.divConsole.childNodes[1].childNodes[0].childNodes[0].style.paddingLeft = '10px'
    } catch (e) {
    // Je vire la console car il y a un pb pour son affichage :
      stor.avecConsole = false
      j3pDetruit(stor.divConsole)
      setTextAce(laConsole, '')
      console.warn('soucis avec l’élément HTML censé contenir les chevrons')
    }

    stor.divBtns = j3pAddElt(stor.zoneCons5, 'div')
    stor.divBtns.style.textAlign = 'right'
    stor.idExecuter = j3pGetNewId('executer')
    stor.idReinitialiser = j3pGetNewId('reinitialiser')
    j3pAjouteBouton(stor.divBtns, stor.idExecuter, 'MepBoutons', textes.executer, '')
    j3pAjouteBouton(stor.divBtns, stor.idReinitialiser, 'MepBoutons', textes.reinitialiser, '')
    j3pElement(stor.idExecuter).addEventListener('click', execute)
    // Et enfin la sortie de l’algo
    if (stor.avecConsole) {
      j3pAffiche(stor.zoneCons3, '', textes.console)

      j3pAffiche(stor.zoneCons6, '', textes.consigne5)
      j3pStyle(stor.zoneCons6, { fontSize: '0.9em', fontStyle: 'italic' })
    }
    const underline = j3pAddElt(stor.zoneCons7, 'u')
    j3pAddTxt(underline, textes.sortie)
    stor.output = 'output'
    stor.pre = j3pAddElt(stor.zoneCons7, 'pre', { id: stor.output })
    j3pStyle(stor.pre, { color: 'black' })
  }

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    desactiverEditeur(stor.editor, stor.idEditor, stor.algoEleveInit)
    const zoneExpli1 = j3pAddElt(zoneExpli, 'div')
    j3pAffiche(zoneExpli1, '', textes.corr1)
    const nameAide = j3pGetNewId('Aide')
    const fenetreCorr = j3pGetNewId('fenetreCorr')
    me.fenetresjq = [{
      name: nameAide,
      title: textes.correctionAlgo,
      top: 110,
      left: 5,
      width: 550,
      id: fenetreCorr
    }]
    j3pCreeFenetres(me)
    const algoCorr = j3pAddElt(fenetreCorr, 'div', '', { style: me.styles.etendre('toutpetit.explications', { fontSize: '1.2em' }) })
    const tabRep = stor.algoReponse.split('\n')
    const espaceTab = '&nbsp;&nbsp;&nbsp;&nbsp;'
    for (let i = 0; i < tabRep.length - 1; i++) {
      try {
        tabRep[i] = tabRep[i].replace(/\t/g, espaceTab)
        j3pAffiche(algoCorr, '', tabRep[i])
        j3pAddContent(algoCorr, '<br>')
      } catch (e) {
        console.warn(e) // pas normal mais pas grave
      }
    }
    j3pToggleFenetres(nameAide)
    j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
    j3pDetruit(stor.idReinitialiser)
  }

  async function enonceInit () {
    try {
      me.surcharge({ nbrepetitions: 3 })
      me.validOnEnter = false
      // Construction de la page
      me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
      me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      me.afficheTitre(textes.titre_exo)
      if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      await init()
      enonceMain()
    } catch (error) {
      j3pShowError(error)
    }
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les consignes

    if (me.questionCourante === 1) {
      stor.tabQuest = [1, 2, 3]
      stor.tabQuestInit = [...stor.tabQuest]
    }
    creationEditeurPython()
    if (stor.tabQuest.length === 0) stor.tabQuest = [...stor.tabQuestInit]
    const pioche = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
    const quest = stor.tabQuest[pioche]
    stor.tabQuest.splice(pioche, 1)
    stor.quest = quest
    let algoInit = ''
    let consoleInit = ''
    algoReponse = ''
    switch (quest) {
      case 1:// aire ou périmètre d’un carré
        algoInit = 'def calculCarre(choix,c):\n'
        consoleInit = 'calculCarre("aire",3)'
        algoReponse = algoInit +
            '\tif choix=="aire":\n' +
            '\t\tresultat=c**2\n'
        if (ds.remplissage === 'vide') {
          algoReponse += '\telse:\n'
        } else {
          algoReponse += '\telif choix=="périmètre":\n'
        }
        algoReponse += '\t\tresultat=4*c\n'
        break
      case 2:// aire ou périmètre d’un disque
        algoInit = 'from math import pi\n' +
            'def calculDisque(choix,r):\n'
        consoleInit = 'calculDisque("aire",3)'
        algoReponse = algoInit +
            '\tif choix=="aire":\n' +
            '\t\tresultat=pi*r**2\n'
        if (ds.remplissage === 'vide') {
          algoReponse += '\telse:\n'
        } else {
          algoReponse += '\telif choix=="périmètre":\n'
        }
        algoReponse += '\t\tresultat=2*pi*r\n'
        break
      case 3:// aire ou périmètre d’un rectangle
        algoInit = 'def calculRectangle(choix,L,l):\n'
        consoleInit = 'calculRectangle("aire",6,2)'
        algoReponse = algoInit +
            '\tif choix=="aire":\n' +
            '\t\tresultat=L*l\n'
        if (ds.remplissage === 'vide') {
          algoReponse += '\telse:\n'
        } else {
          algoReponse += '\telif choix=="périmètre":\n'
        }
        algoReponse += '\t\tresultat=2*(L+l)\n'
        break
    }
    if (ds.remplissage === 'vide') {
      algoInit += '\t\n'
      algoInit += '\treturn \n'
    } else {
      // pas vide
      if (ds.remplissage === 'complet') {
        algoInit += '\tif choix==="aire":\n'
      } else {
        algoInit += '\tif choix==\n'
      }
      algoInit += '\t\tresultat=\n'
      if (ds.remplissage === 'complet') {
        algoInit += '\telif choix=="périmètre":\n'
      } else {
        algoInit += '\telif choix==\n'
      }
      algoInit += '\t\tresultat=\n' +
          '\telse:\n' +
          '\t\tresultat="' + textes.consigneErreurChoix + '"\n' +
          '\treturn resultat\n'
      algoReponse += '\telse:\n' +
          '\t\tresultat="' + textes.consigneErreurChoix + '"\n'
    }
    algoReponse += '\treturn resultat\n'

    stor.algoInit = algoInit
    setTextAce(stor.editor, algoInit)
    if (stor.avecConsole) {
      if (!ds.aideConsole) consoleInit = ''
      setTextAce(laConsole, consoleInit)
    }

    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 4; i++) stor['zoneConsD' + i] = j3pAddElt(stor.conteneurD, 'div')
    j3pAffiche(stor.zoneConsD1, '', textes.consigne1)
    j3pAffiche(stor.zoneConsD2, '', textes['consigne2_' + quest])
    if (ds.remplissage === 'vide') {
      j3pAffiche(stor.zoneConsD3, '', textes.consigne3Bis)
    } else {
      j3pAffiche(stor.zoneConsD3, '', textes.consigne3)
      j3pAffiche(stor.zoneConsD4, '', textes.consigne4)
    }

    stor.algoReponse = algoReponse

    j3pElement(stor.idReinitialiser).addEventListener('click', reinitialiser)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvent dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    // Obligatoire
    me.finEnonce()
  }

  let algoReponse
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInit()
      } else {
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const reponse = { aRepondu: false, bonneReponse: true }
      stor.algoEleveInit = getTextAce(stor.editor)
      reponse.aRepondu = (stor.algoInit !== stor.algoEleveInit)
      let objDeuxPts
      if (reponse.aRepondu) {
        // JE vérifie qu’il y a bien : à la fin de chaque ligne contenant un mot clé
        objDeuxPts = verifDeuxPoints(stor.algoEleveInit)
        // {presents:deuxPointsPresents,numLigne:numLigne};
        me.logIfDebug('objDeuxPts:', objDeuxPts)
        if (objDeuxPts.presents) {
          const objTests = []// liste des objets pour les tests
          let consoleTest = ''
          switch (stor.quest) {
            case 1:// aire ou périmètre d’un carré
              consoleTest = 'calculCarre(choix,c)'
              objTests.push({ choix: '"aire"', c: 3 }, { choix: '"aire"', c: 7 }, { choix: '"aire"', c: 13 })
              objTests.push({ choix: '"perimetre"', c: 3 }, { choix: '"perimetre"', c: 7 }, {
                choix: '"perimetre"',
                c: 13
              })
              objTests.push({ choix: '"autre"', c: 7 })
              break
            case 2:// aire ou périmètre d’un disque
              consoleTest = 'calculDisque(choix,r)'
              objTests.push({ choix: '"aire"', r: 3 }, { choix: '"aire"', r: 7 }, { choix: '"aire"', r: 13 })
              objTests.push({ choix: '"perimetre"', r: 3 }, { choix: '"perimetre"', r: 7 }, {
                choix: '"perimetre"',
                r: 13
              })
              objTests.push({ choix: '"autre"', r: 7 })
              break
            case 3:// aire ou périmètre d’un rectangle
              consoleTest = 'calculRectangle(choix,L,l)'
              objTests.push({ choix: '"aire"', L: 3, l: 1.5 }, { choix: '"aire"', L: 7, l: 5.5 }, {
                choix: '"aire"',
                L: 13,
                l: 10
              })
              objTests.push({ choix: '"perimetre"', L: 3, l: 1.5 }, {
                choix: '"perimetre"',
                L: 7,
                l: 5.5
              }, { choix: '"perimetre"', L: 13, l: 10 })
              objTests.push({ choix: '"autre"', L: 13, l: 10 })
              break
          }
          let algoEleve = stor.algoEleveInit + '\nprint(' + consoleTest + ')'
          if (ds.remplissage !== 'vide') {
            // Dans l’algo de l’élève, je me méfis s’il ne s’amuse pas à me changer le texte
            const tabLignes = algoEleve.split('\n')
            let numLigneErreurTxt = -1
            for (let i = 0; i < tabLignes.length; i++) {
              if (tabLignes[i].indexOf('else') > -1) {
                numLigneErreurTxt = i + 1
              }
              // J’en profite pour vérifier à chaque fin de ligne contenant une instruction qu’il y a bien deux-points
            }
            if (numLigneErreurTxt >= 0) {
              const txtErreur = tabLignes[numLigneErreurTxt].substring(tabLignes[numLigneErreurTxt].indexOf('"'))
              algoEleve = algoEleve.replace(txtErreur, '"' + textes.consigneErreurChoix + '"')
            }
          }

          algoEleve = algoEleve.replace(/p[éeè]{1}rim[éèe]{1}tre/g, 'périmètre')
          algoReponse = stor.algoReponse.replace(/p[éeè]{1}rim[éèe]{1}tre/g, 'périmètre')
          algoReponse += 'print(' + consoleTest + ')'
          me.logIfDebug('algoEleve:', algoEleve, '   algoReponse:', algoReponse)
          let numTests = 0
          while (reponse.bonneReponse && numTests < objTests.length) {
            const typeRep = (objTests[numTests].choix === '"autre"') ? 'string' : 'number'
            reponse.bonneReponse = verifierCodePython(algoEleve, algoReponse, objTests[numTests], typeRep)
            numTests++
          }
        } else {
          reponse.bonneReponse = false
        }
      }

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.zoneCorr.style.color = me.styles.cfaux
        // On peut ajouter un commentaire particulier.
        stor.zoneCorr.innerHTML = textes.comment1
        // on reste dans l’état correction
        return me.finCorrection()
      }

      // Une réponse a été saisie
      if (reponse.bonneReponse) {
        // Bonne réponse
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        // afficheCorr(true);
        desactiverEditeur(stor.editor, stor.divEditor, stor.algoEleveInit)
        j3pElement(stor.idReinitialiser).removeEventListener('click', reinitialiser)
        j3pDetruit(stor.idReinitialiser)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorr(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      if (!objDeuxPts.presents) {
        // c’est que l’expression entrée dans la quatrième zone de saisie est simplifiable
        stor.zoneCorr.innerHTML += '<br>' + j3pChaine(textes.comment2, { l: objDeuxPts.numLigne })
      }
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // ici il a encore la possibilité de se corriger, on reste en correction
        return me.finCorrection()
      }
      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorr(false)
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.buttonsElts.sectionSuivante?.addEventListener('click', j3pDetruitToutesLesFenetres)
        j3pElement(stor.idExecuter).removeEventListener('click', execute)
      } else {
        me.buttonsElts.suite?.addEventListener('click', j3pDetruitToutesLesFenetres)
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
