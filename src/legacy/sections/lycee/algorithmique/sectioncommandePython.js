import $ from 'jquery'
import { j3pAddElt, j3pAutoSizeInput, j3pFocus, j3pGetRandomInt, j3pRandomTab, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mai 2019
        Traduction d’une syntaxe en langage naturel en une syntaxe python
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeCommande', [1, 2, 3, 4, 5, 6, 7, 8], 'array', 'liste des commandes qui seront proposées dans un ordre aléatoire dans l’exercice (un cas de figure peut être proposé plusieurs fois)&nbsp;:<br/>-1 : affectation<br/>-2 : test égalité<br/>-3 : test de différence<br/>-4 : réel aléatoire entre 0 et 1<br/>-5 : entier aléatoire dans un intervalle<br/>-6 : élever à une puissance<br/>-7 : écrire une boucle bornée<br/>-8 :écrire une boucle non bornée']
  ]

}

/**
 * section commandePython
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    for (let i = 1; i <= (((stor.quest === 4) || (stor.quest === 5)) ? 6 : 1); i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    if ((stor.quest === 4) || (stor.quest === 5)) {
      const espaceTab = '&nbsp;&nbsp;&nbsp;&nbsp;'
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr1)
      if (stor.quest === 4) {
        j3pAffiche(stor.zoneExpli2, '', ds.textes.corr3bis, {
          l: 'from random import *',
          l2: 'from random import random'
        })
      } else {
        j3pAffiche(stor.zoneExpli2, '', ds.textes.corr3bis, {
          l: 'from random import *',
          l2: 'from random import randint'
        })
      }
      j3pAffiche(stor.zoneExpli3, '', espaceTab + "<span class='code1'>" + stor.zoneInput.reponse[1] + '</span>')
      j3pAffiche(stor.zoneExpli5, '', ds.textes.corr3, { l: 'import random' })
      j3pAffiche(stor.zoneExpli6, '', espaceTab + "<span class='code1'>" + stor.zoneInput.reponse[0] + '</span>')
    } else {
      j3pAffiche(stor.zoneExpli1, '', "<span class='code1'>" + stor.zoneInput.reponse[0] + '</span>')
    }
    const leCode = $('.code1')
    leCode.css('font-family', 'Lucida Console')
    leCode.css('font-size', '0.9em')
    leCode.css('background-color', '#FFFFFF')
    leCode.css('color', zoneExpli.style.color)
  }
  function bonIntervalle (elt) {
    // Pour vérifier si l’élément est bien dans l’intervalle [1;8] (pour avoir uniquement les cas de figure de typeCommande
    return ((Number(elt) >= 1) && (Number(elt) <= 8))
  }
  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      typeCommande: [1, 2, 3, 4, 5, 6, 7, 8],

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Traduire des commandes en python',
        // on donne les phrases de la consigne
        consigne1: 'Une commande étant donnée en langage naturel, l’écrire dans le langage python&nbsp;:',
        consigne2: 'En langage naturel&nbsp;:',
        consigne3: 'Traduction en python &nbsp;:',
        complement1: 'On ne demande qu’une ligne, celle permettant d’importer un module étant omise ici.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Attention tout de même car en python, il faut terminer la ligne par ":"&nbsp!',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'Deux réponses sont possibles suivant la façon d’importer le module random&nbsp;:',
        corr2: 'ou bien&nbsp;:',
        corr3: "- si on importe le module par la ligne <span class='code1'>£l</span>, alors on écrira :",
        corr3bis: "- si on importe le module par la ligne <span class='code1'>£l</span> ou <span class='code1'>£{l2}</span>, alors on écrira :"
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      /*
          Phrase d’état renvoyée par la section
          Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              pe_1: "toto"
              pe_2: "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
          Par exemple,
          parcours.pe: donneesSection.pe_2
          */
      pe: 0
    }
  }
  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 5; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1)
    // Choix de la commande en langage naturel

    const quest = Number(stor.typeCommande[me.questionCourante - 1])
    stor.quest = quest
    let choixVar
    let langageNaturel
    const lesReponses = []
    if (quest !== 7) {
      do {
        choixVar = String.fromCharCode(97 + j3pGetRandomInt(0, 25))
      } while (['l', 'o'].includes(choixVar))
    }
    let nb = j3pGetRandomInt(1, 20)
    let nb2, puis, compt, signe
    switch (quest) {
      case 1:
        // -1 : affectation
        langageNaturel = choixVar + '$\\leftarrow $' + nb
        lesReponses.push(choixVar + '=' + nb)
        break
      case 2:
        // -2 : test égalité
        langageNaturel = 'Si ' + choixVar + '=' + nb + ':'
        lesReponses.push('if ' + choixVar + '==' + nb + ':', 'if (' + choixVar + '==' + nb + '):', 'if ' + choixVar + '==' + nb, 'if (' + choixVar + '==' + nb + ')')
        break
      case 3:
        // -3 : test de différence
        langageNaturel = 'Si ' + choixVar + '$\\neq $' + nb + ':'
        lesReponses.push('if ' + choixVar + '!=' + nb + ':', 'if (' + choixVar + '!=' + nb + '):', 'if ' + choixVar + '!=' + nb, 'if (' + choixVar + '!=' + nb + ')')
        break
      case 4:
        // -4 : réel aléatoire entre 0 et 1
        langageNaturel = choixVar + ' $\\leftarrow$ réel de l’intervalle [0;1]'
        lesReponses.push(choixVar + '=random.random()', choixVar + '=random()', choixVar + '=uniform(0,1)')
        break
      case 5:
        // -5 : entier aléatoire dans un intervalle
        nb2 = nb + j3pGetRandomInt(5, 15)
        langageNaturel = choixVar + ' $\\leftarrow$ entier de l’intervalle [' + nb + ';' + nb2 + ']'
        lesReponses.push(choixVar + '=random.randint(' + nb + ',' + nb2 + ')', choixVar + '=randint(' + nb + ',' + nb2 + ')', choixVar + '=int(uniform(' + nb + ',' + (nb2 + 1) + '))')
        break
      case 6:
        // -6 : élever à une puissance
        puis = j3pGetRandomInt(2, 5)
        langageNaturel = choixVar + '$^{' + puis + '}$'
        lesReponses.push(choixVar + '**' + puis)
        break
      case 7:
        // -7 : écrire une boucle bornée
        compt = j3pRandomTab(['i', 'j', 'k', 'n'], [0.25, 0.25, 0.25, 0.25])
        nb = j3pGetRandomInt(5, 10)
        langageNaturel = 'Pour ' + compt + ' allant de 0 à ' + nb + ':'
        lesReponses.push('for ' + compt + ' in range(' + (nb + 1) + '):', 'for ' + compt + ' in range(' + (nb + 1) + ')', 'for ' + compt + ' in range(0,' + (nb + 1) + '):', 'for ' + compt + ' in range(0,' + (nb + 1) + ')')
        break
      case 8:
        // -8 : écrire une boucle non bornée
        signe = j3pRandomTab(['>', '<'], [0.5, 0.5])
        nb = (signe === '<') ? j3pGetRandomInt(10, 15) : j3pGetRandomInt(0, 5)
        langageNaturel = 'Tant que ' + choixVar + signe + nb + ':'
        lesReponses.push('while ' + choixVar + signe + nb + ':', 'while (' + choixVar + signe + nb + '):', 'while ' + choixVar + signe + nb, 'while (' + choixVar + signe + nb + ')')
        break
    }
    j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2)
    j3pAffiche(stor.zoneCons3, '', langageNaturel)
    j3pAffiche(stor.zoneCons4, '', ds.textes.consigne3)
    const elt = j3pAffiche(stor.zoneCons5, '', '@1@', { input1: { texte: '', dynamique: true } })
    stor.zoneInput = elt.inputList[0]
    stor.zoneInput.typeReponse = ['texte']
    stor.zoneInput.reponse = lesReponses
    me.logIfDebug('lesReponses:', lesReponses)
    j3pFocus(stor.zoneInput)
    if ((quest === 4) || (quest === 5)) {
      const zoneCons6 = j3pAddElt(stor.conteneur, 'div')
      j3pStyle(zoneCons6, { fontSize: '0.9em', fontStyle: 'italic' })
      j3pAffiche(zoneCons6, '', ds.textes.complement1)
    }
    j3pFocus(stor.zoneInput)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesZonesSaisie })
    // Avant de pouvoir valider la réponse, je vire les espaces inutiles
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        // On vérifie que les nombres indiqués dans typeCommande sont tous entre 1 et 8 (il peut y avoir des doublons si l’utilisateur le souhaite)
        /* for (var i=0;i<ds.typeCommande.length;i++){
                    if (Number(ds.typeCommande[i]))
                } */
        ds.typeCommande = ds.typeCommande.filter(bonIntervalle)
        if (ds.typeCommande.length === 0) {
          ds.typeCommande = [1, 2, 3, 4, 5, 6, 7, 8]
        }
        ds.nbrepetitions = ds.typeCommande.length
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        // Construction de la page
        me.construitStructurePage({ structure: ds.structure })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        // Je mélange les élements du tableau ds.typeCommande
        stor.typeCommande = []
        while (ds.typeCommande.length !== 0) {
          const pioche = j3pGetRandomInt(0, (ds.typeCommande.length - 1))
          stor.typeCommande.push(ds.typeCommande[pioche])
          ds.typeCommande.splice(pioche, 1)
        }
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        const reponse = { aRepondu: false, bonneReponse: false }
        reponse.aRepondu = fctsValid.valideReponses()
        let repEleve, tabMotsCles
        if (reponse.aRepondu) {
          repEleve = fctsValid.zones.reponseSaisie[0]
          tabMotsCles = ['if', 'while', 'for']
          // Je vais virer toutes les espaces mais avant il faut que je gère les mots clés qui peuevnt être glissés dans la ligne comme une commande ou une variable
          for (let i = 0; i < tabMotsCles.length; i++) {
            repEleve = repEleve.replace(tabMotsCles[i] + ' ', tabMotsCles[i] + '!!!')
          }
          repEleve = repEleve.replace(/\s/g, '')// Là j’ai viré toutes les espaces
          // Et enfin je remets mes mots clés de belle manière
          for (let i = 0; i < tabMotsCles.length; i++) {
            repEleve = repEleve.replace(tabMotsCles[i] + '!!!', tabMotsCles[i] + ' ')
          }
          repEleve = repEleve.replace('inrange', ' in range')
          reponse.bonneReponse = false
          for (let i = 0; i < stor.zoneInput.reponse.length; i++) {
            reponse.bonneReponse = (reponse.bonneReponse || (repEleve === stor.zoneInput.reponse[i]))
          }
          fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
          if (reponse.bonneReponse) {
          // on met la réponse attendue (y’a parfois un : en plus par ex, ou x=random() devient x=random.random())
            fctsValid.zones.inputs[0].value = stor.zoneInput.reponse[0]
            j3pAutoSizeInput(fctsValid.zones.inputs[0])
          }
          fctsValid.coloreUneZone(stor.zoneInput.id)
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            let motCle = false
            for (let i = 0; i < tabMotsCles.length; i++) {
              motCle = (motCle || (repEleve.indexOf(tabMotsCles[i] + ' ') > -1))
            }
            if (motCle) {
            // On vérifie si le dernier caractère est :
              if (repEleve[repEleve.length - 1] !== ':') {
                stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              }
            }
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            if ([4, 5].indexOf(stor.quest) > -1) {
              afficheCorrection(true)
            }
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
