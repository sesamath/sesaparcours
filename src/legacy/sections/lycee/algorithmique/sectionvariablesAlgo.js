import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pRandomTab, j3pRestriction, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Avril 2019
        Déterminer la valeur de variables à l’issue d’un algo
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['formatPython', false, 'boolean', 'L’algorithme est écrit par défaut en langage naturel, mais cela peut être fait dans le langage python.']
  ]

}
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.toutpetit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    for (let i = 1; i <= 3; i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    const objCorr = {}
    objCorr.a = stor.tabVariables[0]
    objCorr.b = stor.tabVariables[1]
    objCorr.e1 = stor.tabCalc[0]
    objCorr.e2 = stor.tabCalc[1]
    objCorr.r1 = stor.zoneInput[0].reponse[0]
    objCorr.r2 = stor.zoneInput[1].reponse[0]
    for (let i = 1; i <= 3; i++) {
      j3pAffiche(stor['zoneExpli' + i], '', ds.textes['corr' + i], objCorr)
    }
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      formatPython: false,
      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Variables dans un algorithme',
        // on donne les phrases de la consigne
        consigne1_1: 'On propose l’algorithme ci-dessous&nbsp;:',
        consigne1_2: 'On propose le programme ci-dessous écrit en python&nbsp;:',
        consigne2: 'Quelles valeurs contiennent les variables $£x$ et $£y$ à l’issue de cet algorithme&nbsp;?',
        consigne3: '$£x=$@1@ et $£y=$@2@.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'On attend une réponse sous forme simplifiée (un nombre), pas un calcul&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'A l’issue de la première ligne, $£a$ vaut&nbsp;: $£{e1}=£{r1}$.',
        corr2: 'A l’issue de la deuxième ligne, $£b$ vaut&nbsp;: $£{e2}=£{r2}$',
        corr3: 'Donc à l’issue de l’algorithme $£a=£{r1}$ et $£b=£{r2}$'
      },
      pe: 0
    }
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    const laCons1 = (ds.formatPython) ? ds.textes.consigne1_2 : ds.textes.consigne1_1
    j3pAffiche(stor.zoneCons1, '', laCons1)
    const tabVariables = j3pRandomTab([['a', 'b'], ['c', 'd'], ['x', 'y'], ['m', 'p'], ['t', 'z']], [0.2, 0.2, 0.2, 0.2, 0.2])
    const tabValInit = []
    tabValInit.push(j3pGetRandomInt(2, 5))
    let x2
    do {
      x2 = (2 * j3pGetRandomInt(0, 1) - 1) * j3pGetRandomInt(2, 5)
    } while (Math.abs(Math.abs(x2) - tabValInit[0]) < Math.pow(10, -12))
    tabValInit.push(x2)
    const lignesAlgo = []
    const lignesPython = []
    lignesAlgo.push('$' + tabVariables[0] + '\\leftarrow' + tabValInit[0] + '$')
    lignesAlgo.push('$' + tabVariables[1] + '\\leftarrow' + tabValInit[1] + '$')
    lignesPython.push(tabVariables[0] + '=' + tabValInit[0])
    lignesPython.push(tabVariables[1] + '=' + tabValInit[1])
    const tabValFin = []
    const tabCalc = []
    const tabValDansCalc = []
    let coef
    let coef2
    if (me.questionCourante === ds.nbrepetitions) {
      do {
        coef = j3pGetRandomInt(3, 5)
      } while (Math.abs(Math.pow(tabValInit[0], 2) - coef * tabValInit[1] - tabValInit[0]) < Math.pow(10, -10))
      lignesAlgo.push('$' + tabVariables[0] + '\\leftarrow ' + tabVariables[0] + '^{\\wedge}2-' + coef + '\\times ' + tabVariables[1] + '$')
      lignesPython.push(tabVariables[0] + '=' + tabVariables[0] + '**2-' + coef + '*' + tabVariables[1])
      tabValFin.push(Math.pow(tabValInit[0], 2) - coef * tabValInit[1])
      tabValDansCalc[0] = (tabValInit[0] >= 0) ? tabValInit[0] : '(' + tabValInit[0] + ')'
      tabValDansCalc[1] = (-coef > 0) ? '+' + (-coef) : (-coef)
      tabValDansCalc[2] = (tabValInit[1] >= 0) ? tabValInit[1] : '(' + tabValInit[1] + ')'
      tabCalc.push(tabValDansCalc[0] + '^2' + tabValDansCalc[1] + '\\times ' + tabValDansCalc[2])
      do {
        coef2 = j3pGetRandomInt(2, 5)
      } while (Math.abs(coef - coef2) < Math.pow(10, -12))
      lignesAlgo.push('$' + tabVariables[1] + '\\leftarrow -' + coef2 + '\\times ' + tabVariables[0] + '-' + tabVariables[1] + '^{\\wedge}2$')
      lignesPython.push(tabVariables[1] + '=-' + coef2 + '*' + tabVariables[0] + '-' + tabVariables[1] + '**2')
      tabValFin.push(-coef2 * tabValFin[0] - Math.pow(tabValInit[1], 2))
      tabValDansCalc[0] = (tabValFin[0] >= 0) ? tabValFin[0] : '(' + tabValFin[0] + ')'
      tabValDansCalc[1] = (-coef2 > 0) ? '+' + (-coef2) : (-coef2)
      tabCalc.push((-coef2) + '\\times ' + tabValDansCalc[0] + '-' + tabValDansCalc[2] + '^2')
    } else {
      const tabSignes = []
      const tabSigneVal = []
      do {
        coef = j3pGetRandomInt(3, 7)
        do {
          coef2 = j3pGetRandomInt(2, 6)
        } while (Math.abs(coef - coef2) < Math.pow(10, -12))
        for (let i = 1; i <= 4; i++) {
          tabSigneVal.push(j3pGetRandomInt(0, 1) * 2 - 1)
          const leSigne = (tabSigneVal[i - 1] === 1) ? ((i - 1) % 2 === 0) ? '' : '+' : '-'
          tabSignes.push(leSigne)
        }
        me.logIfDebug('valeurs: ', tabSigneVal[0] * coef * tabValInit[0] + tabSigneVal[1] * coef2 * tabValInit[1], tabValInit[0])
      } while (Math.abs(tabSigneVal[0] * coef * tabValInit[0] + tabSigneVal[1] * coef2 * tabValInit[1] - tabValInit[0]) < Math.pow(10, -12))
      let coef4
      const coef3 = j3pGetRandomInt(3, 7)
      do {
        coef4 = j3pGetRandomInt(2, 6)
      } while (Math.abs(coef3 - coef4) < Math.pow(10, -12))
      lignesAlgo.push('$' + tabVariables[0] + '\\leftarrow ' + tabSignes[0] + coef + '\\times ' + tabVariables[0] + tabSignes[1] + coef2 + '\\times ' + tabVariables[1] + '$')
      lignesPython.push(tabVariables[0] + '=' + tabSignes[0] + coef + '*' + tabVariables[0] + tabSignes[1] + coef2 + '*' + tabVariables[1])
      tabValFin.push(tabSigneVal[0] * coef * tabValInit[0] + tabSigneVal[1] * coef2 * tabValInit[1])
      tabValDansCalc[0] = (tabValInit[0] >= 0) ? tabValInit[0] : '(' + tabValInit[0] + ')'
      tabValDansCalc[1] = (tabSigneVal[1] * coef2 > 0) ? '+' + (tabSigneVal[1] * coef2) : (tabSigneVal[1] * coef2)
      tabValDansCalc[2] = (tabValInit[1] >= 0) ? tabValInit[1] : '(' + tabValInit[1] + ')'
      tabCalc.push((tabSigneVal[0] * coef) + '\\times ' + tabValDansCalc[0] + tabValDansCalc[1] + '\\times ' + tabValDansCalc[2])
      lignesAlgo.push('$' + tabVariables[1] + '\\leftarrow ' + tabSignes[2] + coef3 + '\\times ' + tabVariables[0] + tabSignes[3] + coef4 + '\\times ' + tabVariables[1] + '$')
      lignesPython.push(tabVariables[1] + '=' + tabSignes[2] + coef3 + '*' + tabVariables[0] + tabSignes[3] + coef4 + '*' + tabVariables[1])
      tabValFin.push(tabSigneVal[2] * coef3 * tabValFin[0] + tabSigneVal[3] * coef4 * tabValInit[1])
      tabValDansCalc[0] = (tabValFin[0] >= 0) ? tabValFin[0] : '(' + tabValFin[0] + ')'
      tabValDansCalc[1] = (tabSigneVal[3] * coef4 > 0) ? '+' + (tabSigneVal[3] * coef4) : (tabSigneVal[3] * coef4)
      // tabValDansCalc[2] = (tabValInit[1]>0)?tabValInit[1]:"("+tabValInit[1]+")";
      tabCalc.push((tabSigneVal[2] * coef3) + '\\times ' + tabValDansCalc[0] + tabValDansCalc[1] + '\\times ' + tabValDansCalc[2])
    }
    if (ds.formatPython) {
      // Je mets les variables au format texte :
      for (let i = 0; i <= 1; i++) {
        tabVariables[i] = '\\text{' + tabVariables[i] + '}'
      }
    }
    for (let i = 0; i < lignesAlgo.length; i++) {
      const ligneAlgo = j3pAddElt(stor.zoneCons2, 'div')
      if (ds.formatPython) {
        j3pAffiche(ligneAlgo, '', lignesPython[i])
      } else {
        j3pAffiche(ligneAlgo, '', lignesAlgo[i])
      }
    }
    j3pStyle(stor.zoneCons2, { backgroundColor: 'white' })
    j3pAffiche(stor.zoneCons3, '', ds.textes.consigne2, {
      x: tabVariables[0],
      y: tabVariables[1]
    })
    const elt = j3pAffiche(stor.zoneCons4, '', ds.textes.consigne3, {
      x: tabVariables[0],
      y: tabVariables[1],
      input1: { texte: '', dynamique: true, maxchars: 5 },
      input2: { texte: '', dynamique: true, maxchars: 5 }
    })
    stor.zoneInput = [...elt.inputList]
    for (let i = 0; i <= 1; i++) {
      j3pRestriction(stor.zoneInput[i], '0-9\\-')
      stor.zoneInput[i].typeReponse = ['nombre', 'exact']
      stor.zoneInput[i].reponse = [tabValFin[i]]
      stor.zoneInput[i].solutionSimplifiee = ['non valide']
    }
    stor.tabCalc = tabCalc
    stor.tabVariables = tabVariables
    j3pFocus(stor.zoneInput[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage({ structure: ds.structure })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée
          let msgReponseManquante
          stor.zoneCorr.style.color = me.styles.cfaux
          if (!fctsValid.zones.reponseSimplifiee[0][0] || !fctsValid.zones.reponseSimplifiee[1][0]) {
          // c’est que la fraction de la quatrième zone de saisie est réductible
            msgReponseManquante = ds.textes.comment1
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
