import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pFocus, j3pGetRandomBool, j3pGetRandomInt, j3pRandomTab, j3pRemplacePoint, j3pRestriction, j3pStyle, j3pVirgule } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        Avril 2019
        Déterminer la valeur de variables à l’issue d’un algo
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['formatPython', false, 'boolean', 'L’algorithme est écrit par défaut en langage naturel, mais cela peut être fait dans le langage python.'],
    ['avecFonction', false, 'boolean', 'Si ce paramètre vaut true, alors on présentera l’algorithme sous la forme d’une fonction (en python si le paramètre formatPython vaut aussi true).']
  ]
}

/**
 * section InstructionSi
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.toutpetit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    j3pDetruit(stor.zoneCalc)
    j3pDetruitFenetres('Calculatrice')
    j3pAffiche(zoneExpli, '', ds.textes.corr1, {
      i: stor.objCons.inegalite,
      v2: (ds.formatPython) ? '\\text{' + stor.maVarSortie + '}' : stor.maVarSortie,
      c: stor.objCons.calcul,
      r: j3pVirgule(stor.zoneInput.reponse[0])
    })
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      formatPython: false,
      avecFonction: false,
      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Instruction conditionnelle',
        // on donne les phrases de la consigne
        consigne1_1: 'Un site Internet de commande en ligne propose une réduction à ses clients&nbsp;:',
        consigne2_1: '- baisse de £{t1}&nbsp;% pour les £p premiers euros de la commande&nbsp;;',
        consigne3_1: '- baisse de £{t2}&nbsp;% sur le montant de la commande supérieur à £p&nbsp;euros.',
        consigneAlgo_1: 'L’algorithme ci-contre permet de déterminer le montant final de la commande en fonction du prix initial $p$ saisi&nbsp;:',
        consigneFct_1: 'L’algorithme ci-contre, écrit sous la forme d’une fonction permet de déterminer le montant final de la commande en fonction du prix initial $p$, argument de cette fonction&nbsp;:',
        consigneAlgoPython_1: 'L’algorithme ci-contre, écrit dans le langage python, permet de déterminer le montant final de la commande en fonction du prix initial p saisi&nbsp;:',
        consigneFctPython_1: 'L’algorithme ci-contre, écrit sous la forme d’une fonction dans le langage python, permet de déterminer le montant final de la commande en fonction du prix initial p, argument de cette fonction&nbsp;:',
        consigne1_2: "Une agence propose des véhicules à la location.<br/>La location journalière s'élève à £p&nbsp;€ auquel on ajoute&nbsp;:",
        consigne2_2: "- £{p1}&nbsp;€ par km parcouru jusqu'à £d&nbsp;km&nbsp;;",
        consigne3_2: '- £{p2}&nbsp;€ par km pour les kilomètres au-dessus de £d.',
        consigneAlgo_2: 'L’algorithme ci-contre permet de déterminer le montant de la location en fonction de la distance $d$ saisie&nbsp;:',
        consigneFct_2: 'L’algorithme ci-contre, écrit sous la forme d’une fonction permet de déterminer le montant de la location en fonction de la distance $d$, argument de cette fonction&nbsp;:',
        consigneAlgoPython_2: 'L’algorithme ci-contre, écrit dans le langage python, permet de déterminer le montant de la location en fonction de la distance d saisie&nbsp;:',
        consigneFctPython_2: 'L’algorithme ci-contre, écrit sous la forme d’une fonction dans le langage python, permet de déterminer le montant de la location en fonction de la distance d, argument de cette fonction&nbsp;:',
        consigne1_3: 'Un employeur décide d’octroyer une prime exceptionnelle à ses salariés selon le mode de calcul suivant&nbsp;:',
        consigne2_3: '- prime égale à £{t1}&nbsp;% pour tout salaire inférieur à £p&nbsp;€&nbsp;;',
        consigne3_3: '- prime de £{p1}&nbsp;€ à laquelle s’ajoute £{t2}&nbsp;% sur la partie du salaire qui dépasse £p&nbsp;€.',
        consigneAlgo_3: 'L’algorithme ci-contre permet de déterminer le montant de la prime en fonction du salaire $s$ saisi&nbsp;:',
        consigneFct_3: 'L’algorithme ci-contre, écrit sous la forme d’une fonction permet de déterminer le montant de la prime en fonction du salaire $s$, argument de cette fonction&nbsp;:',
        consigneAlgoPython_3: 'L’algorithme ci-contre, écrit dans le langage python, permet de déterminer le montant de la prime en fonction du salaire s saisi&nbsp;:',
        consigneFctPython_3: 'L’algorithme ci-contre, écrit sous la forme d’une fonction dans le langage python, permet de déterminer le montant de la prime en fonction du salaire s, argument de cette fonction&nbsp;:',
        consigne1_4: 'On souhaite repeindre une pièce. Un artisan propose un devis où le calcul du montant diffère suivant la surface à peindre&nbsp;:',
        consigne2_4: '- forfait de £{f1}&nbsp;€ auquel on ajoute £{p1}&nbsp;€ par mètre carré pour toute surface inférieure à £s&nbsp;m²&nbsp;;',
        consigne3_4: '- forfait de £{f2}&nbsp;€ auquel on ajoute £{p2}&nbsp;€ par mètre carré pour toute surface dépassant £s&nbsp;m².',
        consigneAlgo_4: 'L’algorithme ci-contre permet de déterminer le montant de la facture en fonction de la surface à peindre $s$ saisie&nbsp;:',
        consigneFct_4: 'L’algorithme ci-contre, écrit sous la forme d’une fonction permet de déterminer le montant de la facture en fonction de la surface à peindre $s$, argument de cette fonction&nbsp;:',
        consigneAlgoPython_4: 'L’algorithme ci-contre, écrit dans le langage python, permet de déterminer le montant de la facture en fonction de la surface à peindre s saisie&nbsp;:',
        consigneFctPython_4: 'L’algorithme ci-contre, écrit sous la forme d’une fonction dans le langage python, permet de déterminer le montant de la facture en fonction de la surface à peindre s, argument de cette fonction&nbsp;:',
        consigne1_5: 'Un coureur de fond calcule la distance qu’il peut parcourir suivant la durée de son parcours.',
        consigne2_5: 'La première heure, il court à la vitesse moyenne de £{v1}&nbsp;km/h puis, le reste du temps à la vitesse moyenne de £{v2}&nbsp;km/h.',
        consigne3_5: '',
        consigneAlgo_5: 'L’algorithme ci-contre permet de déterminer la distance parcourue en fonction de la durée $h$ de sa course (en heures) saisie&nbsp;:',
        consigneFct_5: 'L’algorithme ci-contre, écrit sous la forme d’une fonction permet de déterminer la distance parcourue en fonction de la durée $h$ de sa course (en heures), argument de cette fonction&nbsp;:',
        consigneAlgoPython_5: 'L’algorithme ci-contre, écrit dans le langage python, permet de déterminer la distance parcourue en fonction de la durée h de sa course (en heures) saisie&nbsp;:',
        consigneFctPython_5: 'L’algorithme ci-contre, écrit sous la forme d’une fonction dans le langage python, permet de déterminer la distance parcourue en fonction de la durée h de sa course (en heures), argument de cette fonction&nbsp;:',

        nomArg: 'p|d|s|s|h',
        nomVar: 'prix|montant|prime|facture|distance',
        nomFct: 'MontantCommande|MontantLocation|MontantPrime|MontantFacture|DistanceParcourue',
        txtFonction: 'fonction',
        txtReturn: 'Renvoyer',
        siInstructions: 'Si|Alors|Sinon',
        consigneAlgo5_1: 'Quelle valeur contient la variable prix à la fin de cet algorithme si on donne à $£v$ la valeur £x&nbsp;?',
        consigneFct5_1: 'Quelle valeur est renvoyée lorsqu’on écrit MontantCommande(£x)&nbsp;?',
        consigneAlgo5_2: 'Quelle valeur contient la variable montant à la fin de cet algorithme si on donne à $£v$ la valeur £x&nbsp;?',
        consigneFct5_2: 'Quelle valeur est renvoyée lorsqu’on écrit MontantLocation(£x)&nbsp;?',
        consigneAlgo5_3: 'Quelle valeur contient la variable prime à la fin de cet algorithme si on donne à $£v$ la valeur £x&nbsp;?',
        consigneFct5_3: 'Quelle valeur est renvoyée lorsqu’on écrit MontantPrime(£x)&nbsp;?',
        consigneAlgo5_4: 'Quelle valeur contient la variable facture à la fin de cet algorithme si on donne à $£v$ la valeur £x&nbsp;?',
        consigneFct5_4: 'Quelle valeur est renvoyée lorsqu’on écrit MontantFacture(£x)&nbsp;?',
        consigneAlgo5_5: 'Quelle valeur contient la variable distance à la fin de cet algorithme si on donne à $£v$ la valeur £x&nbsp;?',
        consigneFct5_5: 'Quelle valeur est renvoyée lorsqu’on écrit DistanceParcourue(£x)&nbsp;?',
        consigne6: 'Réponse : @1@.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'On attend une réponse sous forme simplifiée (un nombre), pas un calcul&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'Comme $£i$, la variable $£{v2}$ prendra la valeur&nbsp;:<br/>$£c=£r$.',
        corr2: 'Donc la valeur renvoyée est £r.'
      },
      pe: 0
    }
  }
  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div')
    j3pStyle(stor.conteneur, me.styles.etendre('toutpetit.enonce', { padding: '10px' }))
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 7; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div')
    j3pStyle(stor.conteneurD, me.styles.etendre('toutpetit.enonce', { padding: '10px' }))
    stor.algo = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
    j3pStyle(stor.zoneCalc, { padding: '15px 0px' })
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAfficheCroixFenetres('Calculatrice')
    j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    // on peut forcer le choix du sujet (paramètre numSujet non renseigné car il sert juste pour effectuer les tests)
    if (isNaN(ds.numSujet)) ds.numSujet = -1
    if (stor.tabQuest.length === 0) {
      stor.tabQuest = [...stor.tabQuestInit]
    }
    me.logIfDebug('stor.tabQuest:', stor.tabQuest)
    let quest
    if (ds.numSujet > -1) {
      quest = ds.numSujet
    } else {
      const choix = j3pGetRandomInt(0, (stor.tabQuest.length - 1))
      quest = stor.tabQuest[choix]
      stor.tabQuest.splice(choix, 1)
    }

    stor.quest = quest
    const laConsTab = []
    const objCons = {}
    const choixAlea = (j3pGetRandomBool())
    switch (quest) {
      case 1:
        objCons.p = j3pGetRandomInt(2, 5) * 10
        objCons.t1 = j3pGetRandomInt(2, 4) * 2
        objCons.t2 = objCons.t1 + j3pGetRandomInt(3, 5)
        objCons.x = (choixAlea === 0)
          ? objCons.p - (j3pGetRandomInt(4, 7) * 2)
          : objCons.p + (j3pGetRandomInt(4, 7) * 4)
        objCons.reponse = (choixAlea === 0)
          ? Math.round(100 * objCons.x * (1 - objCons.t1 / 100)) / 100
          : Math.round(100 * (objCons.p * (1 - objCons.t1 / 100) + (objCons.x - objCons.p) * (1 - objCons.t2 / 100))) / 100
          // Calcul qui va apparaitre dans la correction :
        objCons.calcul = (choixAlea === 0)
          ? objCons.x + '\\times \\left(1-\\frac{' + objCons.t1 + '}{100}\\right)'
          : objCons.p + '\\times \\left(1-\\frac{' + objCons.t1 + '}{100}\\right)+(' + objCons.x + '-' + objCons.p + ')\\times \\left(1-\\frac{' + objCons.t2 + '}{100}\\right)'
        objCons.inegalite = (choixAlea === 0)
          ? objCons.x + '\\leq ' + objCons.p
          : objCons.x + '> ' + objCons.p
        break
      case 2:
        objCons.p = j3pGetRandomInt(5, 7) * 10
        objCons.d = j3pGetRandomInt(2, 3) * 50
        objCons.p1Val = j3pGetRandomInt(5, 8) * 5 / 100
        objCons.p2Val = Math.round(100 * (objCons.p1Val + j3pGetRandomInt(1, 2) / 10)) / 100
        objCons.p1 = j3pVirgule(objCons.p1Val)
        objCons.p2 = j3pVirgule(objCons.p2Val)
        objCons.x = (choixAlea === 0)
          ? objCons.d - (j3pGetRandomInt(2, 5) * 10)
          : objCons.d + (j3pGetRandomInt(4, 9) * 10)
        objCons.reponse = (choixAlea === 0)
          ? Math.round(100 * (objCons.p + objCons.x * objCons.p1Val)) / 100
          : Math.round(100 * (objCons.p + objCons.d * objCons.p1Val + (objCons.x - objCons.d) * objCons.p2Val)) / 100
          // Calcul qui va apparaitre dans la correction :
        objCons.calcul = (choixAlea === 0)
          ? objCons.p + '+' + objCons.x + '\\times ' + objCons.p1
          : objCons.p + '+' + objCons.d + '\\times ' + objCons.p1 + '+(' + objCons.x + '-' + objCons.d + ')\\times ' + objCons.p2
        objCons.inegalite = (choixAlea === 0)
          ? objCons.x + '\\leq ' + objCons.d
          : objCons.x + '> ' + objCons.d
        break
      case 3:
        objCons.p = j3pGetRandomInt(9, 11) * 200
        objCons.t1Val = j3pGetRandomInt(5, 7) * 0.5
        objCons.t2Val = Math.round(100 * (objCons.t1Val - j3pGetRandomInt(1, 2) * 0.5)) / 100
        objCons.t1 = j3pVirgule(objCons.t1Val)
        objCons.t2 = j3pVirgule(objCons.t2Val)
        objCons.p1 = Math.round(100 * (objCons.p * objCons.t1Val / 100)) / 100
        objCons.x = (choixAlea === 0)
          ? objCons.p - (j3pGetRandomInt(2, 5) * 100)
          : objCons.p + (j3pGetRandomInt(4, 9) * 100)
        objCons.reponse = (choixAlea === 0)
          ? Math.round(100 * objCons.x * objCons.t1Val / 100) / 100
          : Math.round(100 * (objCons.p1 + (objCons.x - objCons.p) * objCons.t2Val / 100)) / 100
          // Calcul qui va apparaitre dans la correction :
        objCons.calcul = (choixAlea === 0)
          ? objCons.x + '\\times \\frac{' + objCons.t1 + '}{100}'
          : objCons.p1 + '+(' + objCons.x + '-' + objCons.p + ')\\times \\frac{' + objCons.t2 + '}{100}'
        objCons.inegalite = (choixAlea === 0)
          ? objCons.x + '\\leq ' + objCons.p
          : objCons.x + '> ' + objCons.p
        break
      case 4:
        objCons.f1 = j3pGetRandomInt(6, 10) * 20
        objCons.s = j3pGetRandomInt(4, 6) * 5
        objCons.p1Val = j3pGetRandomInt(27, 29) * 0.5
        objCons.p2Val = Math.round(100 * (objCons.p1Val - j3pGetRandomInt(1, 2))) / 100
        objCons.p1 = j3pVirgule(objCons.p1Val)
        objCons.p2 = j3pVirgule(objCons.p2Val)
        objCons.f2 = Math.round(100 * (objCons.f1 + (objCons.p1Val - objCons.p2Val) * objCons.s)) / 100
        objCons.x = (choixAlea === 0)
          ? objCons.s - (j3pGetRandomInt(4, 7) * 2)
          : objCons.s + (j3pGetRandomInt(2, 7) * 5)
        objCons.reponse = (choixAlea === 0)
          ? Math.round(100 * (objCons.f1 + objCons.x * objCons.p1Val)) / 100
          : Math.round(100 * (objCons.f2 + objCons.x * objCons.p2Val)) / 100
          // Calcul qui va apparaitre dans la correction :
        objCons.calcul = (choixAlea === 0)
          ? objCons.f1 + '+' + objCons.x + '\\times ' + objCons.p1
          : objCons.f2 + '+' + objCons.x + '\\times ' + objCons.p2
        objCons.inegalite = (choixAlea === 0)
          ? objCons.x + '\\leq ' + objCons.s
          : objCons.x + '> ' + objCons.s
        break
      case 5:
        objCons.v1Val = j3pGetRandomInt(24, 28) * 0.5
        objCons.v2Val = objCons.v1Val - j3pGetRandomInt(2, 4) * 0.5
        objCons.v1 = j3pVirgule(objCons.v1Val)
        objCons.v2 = j3pVirgule(objCons.v2Val)
        objCons.d = 1
        objCons.xVal = (choixAlea === 0)
          ? objCons.d - 1 / j3pRandomTab([4, 5, 10], [0.333, 0.333, 0.334])
          : objCons.d + (j3pGetRandomInt(2, 8) * 0.25)
        objCons.x = j3pVirgule(objCons.xVal)
        objCons.reponse = (choixAlea === 0)
          ? Math.round(1000 * objCons.xVal * objCons.v1Val) / 1000
          : Math.round(1000 * (objCons.d * objCons.v1Val + (objCons.xVal - objCons.d) * objCons.v2Val)) / 1000
          // Calcul qui va apparaitre dans la correction :
        objCons.calcul = (choixAlea === 0)
          ? objCons.x + '\\times ' + objCons.v1
          : (objCons.d * objCons.v1Val) + '+(' + objCons.x + '-' + objCons.d + ')\\times ' + objCons.v2
        objCons.inegalite = (choixAlea === 0)
          ? objCons.x + '\\leq ' + objCons.d
          : objCons.x + '> ' + objCons.d
        break
    }
    for (let i = 1; i <= 3; i++) {
      laConsTab.push(ds.textes['consigne' + i + '_' + quest])
      j3pAffiche(stor['zoneCons' + i], '', laConsTab[i - 1], objCons)
    }
    const laCons4 = (ds.formatPython)
      ? (ds.avecFonction) ? ds.textes['consigneFctPython_' + quest] : ds.textes['consigneAlgoPython_' + quest]
      : (ds.avecFonction) ? ds.textes['consigneFct_' + quest] : ds.textes['consigneAlgo_' + quest]
    laConsTab.push(laCons4)
    j3pAffiche(stor.zoneCons4, '', laConsTab[3])
    // Nom de la variables présente dans l’algor et qui contiendra la réponse
    stor.maVarSortie = ds.textes.nomVar.split('|')[quest - 1]
    stor.nomArg = ds.textes.nomArg.split('|')[quest - 1]
    stor.nomFct = ds.textes.nomFct.split('|')[quest - 1]
    // Création de l’algo
    const lignesAlgo = []
    let espaceTab = ''
    for (let i = 1; i <= 5; i++) {
      espaceTab += '&nbsp;'
    }
    if (!ds.formatPython) {
      stor.maVarSortie = '\\text{' + stor.maVarSortie + '}'
    }
    const espaceTab1 = (ds.avecFonction) ? espaceTab : ''
    const instrSi = ds.textes.siInstructions.split('|')
    if (ds.formatPython) {
      if (ds.avecFonction) {
        lignesAlgo.push("<span class='bleu'>def</span> " + stor.nomFct + '(' + stor.nomArg + '):')
      }
      switch (quest) {
        case 1:
          lignesAlgo.push(espaceTab1 + "<span class='bleu'>if</span> " + stor.nomArg + '<=' + objCons.p + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + stor.maVarSortie + '=' + stor.nomArg + '*(1-' + objCons.t1 + '/100)')
          lignesAlgo.push(espaceTab1 + "<span class='bleu'>else</span>:")
          lignesAlgo.push(espaceTab1 + espaceTab + stor.maVarSortie + '=' + objCons.p + '*(1-' + objCons.t1 + '/100)+(' + stor.nomArg + '-' + objCons.p + ')*(1-' + objCons.t2 + '/100)')
          break
        case 2:
          lignesAlgo.push(espaceTab1 + "<span class='bleu'>if</span> " + stor.nomArg + '<=' + objCons.d + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + stor.maVarSortie + '=' + objCons.p + '+' + stor.nomArg + '*' + objCons.p1)
          lignesAlgo.push(espaceTab1 + "<span class='bleu'>else</span>:")
          lignesAlgo.push(espaceTab1 + espaceTab + stor.maVarSortie + '=' + objCons.p + '+' + objCons.d + '*' + objCons.p1 + '+(' + stor.nomArg + '-' + objCons.d + ')*' + objCons.p2)
          break
        case 3:
          lignesAlgo.push(espaceTab1 + "<span class='bleu'>if</span> " + stor.nomArg + '<=' + objCons.p + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + stor.maVarSortie + '=' + stor.nomArg + '*' + objCons.t1 + '/100')
          lignesAlgo.push(espaceTab1 + "<span class='bleu'>else</span>:")
          lignesAlgo.push(espaceTab1 + espaceTab + stor.maVarSortie + '=' + objCons.p1 + '+(' + stor.nomArg + '-' + objCons.p + ')*' + objCons.t2 + '/100')
          break
        case 4:
          lignesAlgo.push(espaceTab1 + "<span class='bleu'>if</span> " + stor.nomArg + '<=' + objCons.s + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + stor.maVarSortie + '=' + objCons.f1 + '+' + stor.nomArg + '*' + objCons.p1)
          lignesAlgo.push(espaceTab1 + "<span class='bleu'>else</span>:")
          lignesAlgo.push(espaceTab1 + espaceTab + stor.maVarSortie + '=' + objCons.f2 + '+' + stor.nomArg + '*' + objCons.p2)
          break
        case 5:
          lignesAlgo.push(espaceTab1 + "<span class='bleu'>if</span> " + stor.nomArg + '<=' + objCons.d + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + stor.maVarSortie + '=' + stor.nomArg + '*' + objCons.v1)
          lignesAlgo.push(espaceTab1 + "<span class='bleu'>else</span>:")
          lignesAlgo.push(espaceTab1 + espaceTab + stor.maVarSortie + '=' + (objCons.d * objCons.v1Val) + '+(' + stor.nomArg + '-' + objCons.d + ')*' + objCons.v2)
          break
      }
      if (ds.avecFonction) {
        lignesAlgo.push(espaceTab1 + "<span class='bleu'>return</span> " + stor.maVarSortie)
      }
    } else {
      if (ds.avecFonction) {
        lignesAlgo.push(ds.textes.txtFonction + ' ' + stor.nomFct + '$(' + stor.nomArg + ')$:')
      }
      switch (quest) {
        case 1:
          lignesAlgo.push(espaceTab1 + instrSi[0] + ' $' + stor.nomArg + '\\leq ' + objCons.p + '$ ' + instrSi[1] + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + '$' + stor.maVarSortie + '\\leftarrow ' + stor.nomArg + '\\times \\left(1-\\frac{' + objCons.t1 + '}{100}\\right)$')
          lignesAlgo.push(espaceTab1 + instrSi[2] + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + '$' + stor.maVarSortie + '\\leftarrow ' + objCons.p + '\\times \\left(1-\\frac{' + objCons.t1 + '}{100}\\right)+(' + stor.nomArg + '-' + objCons.p + ')\\times \\left(1-\\frac{' + objCons.t2 + '}{100}\\right)$')
          break
        case 2:
          lignesAlgo.push(espaceTab1 + instrSi[0] + ' $' + stor.nomArg + '\\leq ' + objCons.d + '$ ' + instrSi[1] + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + '$' + stor.maVarSortie + '\\leftarrow ' + objCons.p + '+' + stor.nomArg + '\\times ' + objCons.p1 + '$')
          lignesAlgo.push(espaceTab1 + instrSi[2] + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + '$' + stor.maVarSortie + '\\leftarrow ' + objCons.p + '+' + objCons.d + '\\times ' + objCons.p1 + '+(' + stor.nomArg + '-' + objCons.d + ')\\times ' + objCons.p2 + '$')
          break
        case 3:
          lignesAlgo.push(espaceTab1 + instrSi[0] + ' $' + stor.nomArg + '\\leq ' + objCons.p + '$ ' + instrSi[1] + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + '$' + stor.maVarSortie + '\\leftarrow ' + stor.nomArg + '\\times \\frac{' + objCons.t1 + '}{100}$')
          lignesAlgo.push(espaceTab1 + instrSi[2] + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + '$' + stor.maVarSortie + '\\leftarrow ' + objCons.p1 + '+(' + stor.nomArg + '-' + objCons.p + ')\\times \\frac{' + objCons.t2 + '}{100}$')
          break
        case 4:
          lignesAlgo.push(espaceTab1 + instrSi[0] + ' $' + stor.nomArg + '\\leq ' + objCons.s + '$ ' + instrSi[1] + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + '$' + stor.maVarSortie + '\\leftarrow ' + objCons.f1 + '+' + stor.nomArg + '\\times ' + objCons.p1 + '$')
          lignesAlgo.push(espaceTab1 + instrSi[2] + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + '$' + stor.maVarSortie + '\\leftarrow ' + objCons.f2 + '+' + stor.nomArg + '\\times ' + objCons.p2 + '$')
          break
        case 5:
          lignesAlgo.push(espaceTab1 + instrSi[0] + ' $' + stor.nomArg + '\\leq ' + objCons.d + '$ ' + instrSi[1] + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + '$' + stor.maVarSortie + '\\leftarrow ' + stor.nomArg + '\\times ' + objCons.v1 + '$')
          lignesAlgo.push(espaceTab1 + instrSi[2] + ':')
          lignesAlgo.push(espaceTab1 + espaceTab + '$' + stor.maVarSortie + '\\leftarrow ' + (objCons.d * objCons.v1Val) + '+(' + stor.nomArg + '-' + objCons.d + ')\\times ' + objCons.v2 + '$')
          break
      }
      if (ds.avecFonction) {
        lignesAlgo.push(espaceTab1 + ds.textes.txtReturn + ' $' + stor.maVarSortie + '$')
      }
    }
    objCons.v = (ds.formatPython) ? '\\text{' + stor.nomArg + '}' : stor.nomArg
    for (let i = 0; i < lignesAlgo.length; i++) {
      const ligneAlgo = j3pAddElt(stor.algo, 'p')
      j3pAffiche(ligneAlgo, '', lignesAlgo[i])
    }
    j3pStyle(stor.algo, { backgroundColor: 'white' })
    if (ds.avecFonction) {
      j3pAffiche(stor.zoneCons5, '', ds.textes['consigneFct5_' + quest], objCons)
    } else {
      j3pAffiche(stor.zoneCons5, '', ds.textes['consigneAlgo5_' + quest], objCons)
    }
    const elt = j3pAffiche(stor.zoneCons6, '', ds.textes.consigne6, { input1: { texte: '', dynamique: true, maxchars: 6 } })
    stor.zoneInput = elt.inputList[0]
    j3pRestriction(stor.zoneInput, '0-9.,\\-')
    stor.zoneInput.typeReponse = ['nombre', 'exact']
    stor.zoneInput.reponse = [objCons.reponse]
    stor.zoneInput.solutionSimplifiee = ['non valide']
    stor.objCons = objCons
    stor.zoneInput.addEventListener('input', j3pRemplacePoint)
    j3pFocus(stor.zoneInput)
    me.logIfDebug('laReponse:', objCons.reponse)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.5 })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zones.IG, ds.indication)

        stor.tabQuest = [1, 2, 3, 4, 5]
        stor.tabQuestInit = [...stor.tabQuest]
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        // A cet instant reponse contient deux éléments :
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée
          let msgReponseManquante
          if (!fctsValid.zones.reponseSimplifiee[0][0]) {
          // c’est que la fraction de la quatrième zone de saisie est réductible
            msgReponseManquante = ds.textes.comment1
          }
          me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // Toujours pour la zone 4, je considére la réponse comme fause si elle n’est pas réduite
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
