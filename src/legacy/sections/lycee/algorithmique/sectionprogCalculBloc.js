import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pAjouteFoisDevantX, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pImporteAnnexe, j3pRandomTab, j3pRestriction, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import Blockly from 'src/lib/outils/blockly/original'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import { mqNormalise } from 'src/lib/outils/conversion/casFormat'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['avecDeuxVariables', false, 'boolean', 'Par défaut, ce paramètre valant false, il n’y aura qu’une seule variable dans l’algorithme. S’il vaut true, alors il y en aura deux.']
  ]
}

/**
 * section progCalculBloc
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.toutpetit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    for (let i = 1; i <= (((me.questionCourante - 1) % ds.nbetapes === 0) ? 4 : 1); i++) stor['zoneExpli' + i] = j3pAddElt(zoneExpli, 'div')
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      j3pDetruit(stor.zoneCalc)
      j3pDetruitFenetres('Calculatrice')
    }
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      const objCorr = {}
      objCorr.styletexte = {}
      objCorr.n = stor.nb
      objCorr.x1 = stor.obj.tabnomvar[0]
      objCorr.x2 = stor.obj.tabnomvar[1]
      objCorr.c1 = (stor.nb > 0)
        ? stor.tabCoefInit[0] + '\\times ' + stor.nb
        : stor.tabCoefInit[0] + '\\times (' + stor.nb + ')'
      objCorr.r1 = stor.tabCoefInit[0] * stor.nb
      objCorr.c2 = (stor.tabCoefInit[1] > 0)
        ? objCorr.r1 + '+' + stor.tabCoef[1]
        : objCorr.r1 + '-' + stor.tabCoef[1]
      objCorr.r2 = stor.zoneInput.reponse[0]
      for (let i = 1; i <= 4; i++) {
        j3pAffiche(stor['zoneExpli' + i], '', ds.textes['corr1_' + i], objCorr)
      }
    } else {
      j3pDetruit(stor.zoneCons3)
      j3pAffiche(stor.zoneExpli1, '', ds.textes.corr2, {
        f: stor.nomf,
        r: stor.zoneInput.reponse[0]
      })
    }
  }

  function resetFigMtg () {
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAV8AAADnAAAAQEAAAAAAAAAAQAAAAn#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAVjb2VmYQABMQAAAAE#8AAAAAAAAAAAAAIA#####wAFY29lZmIAATIAAAABQAAAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wABZgANY29lZmEqeCtjb2VmYv####8AAAABAApDT3BlcmF0aW9uAAAAAAQC#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAH#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAABQAAAAIAAXgAAAADAP####8AA3JlcAAFMisxKngAAAAEAAAAAAFAAAAAAAAAAAAAAAQCAAAAAT#wAAAAAAAAAAAABgAAAAAAAXj#####AAAAAwAQQ1Rlc3RFcXVpdmFsZW5jZQD#####AAdlZ2FsaXRlAAAAAwAAAAQBAQAAAAE#8AAAAAAAAAH#####AAAAAgAGQ0xhdGV4AP####8AAAAAAf####8QQFLAAAAAAABATIUeuFHrhQAAAAAAAAAAAAAAAAABAAAAAAAAAAAAEGYoeCk9XEZvclNpbXB7Zn0AAAAIAP####8AAAAAAf####8QQFJAAAAAAABAWYKPXCj1wgAAAAAAAAAAAAAAAAABAAAAAAAAAAAAFHJlcCh4KT1cRm9yU2ltcHtyZXB9#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQD#####AAAAAAH#####EEBuwAAAAAAAQE+FHrhR64QAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAhlZ2FsaXRlPQAAAgAAAAX###############8='
    stor.liste = stor.mtgAppLecteur.createList(txtFigure)
  }

  async function chargeExemple () {
    // on récupère un XMLDocument, mais vu qu’on le modifie à chaque question, on ne stocke que la string
    // (pour recréer ensuite un xmlDoc à chaque question)
    const xmlDoc = await j3pImporteAnnexe('blockly/GenererFctAffine.xml')
    stor.xmlString = (new XMLSerializer()).serializeToString(xmlDoc)
  }

  function changeExemple (objChgt, tabType) {
    /**
     * Modifie les TEXT_NODE ou CDATA_SECTION_NODE du xml pour y substituer du contenu
     * (valeurs de variables, les signes d’opération, les noms des variables, les textes)
     * obj.tabvar contient la valeur des variables présentes dans le XML sous la forme (var1), (var2), ... L’ordre a de l’importance
     * obj.tabsigne contient le signe qu’il faut mettre dans le XML à la place de (SIGNE1), (SIGNE2), ...
     *              Ces signes sont "ADD","MINUS","MULTIPLY" et "DIVIDE"
     * obj.tabtexte contient les textes qu’il faut mettre dans le XML à la place de (texte1), (texte2), ...
     * obj.tabnomVar contient les noms des variables qu’il faut mettre dans le XML à la place de (nomVar1), (nomVar2), ...
     *
     * tabType peut contenir les valeurs signe, var, texte, nomvar : ce sont les différentes valeurs présentes dans le xml sous la forme (var1) et qui seront à modifier en fonction de l’aléatoire de la section
     * @inner
     * @param xml
     * @returns {*}
     */
    function gererFils (xml) {
      for (const element of xml.childNodes) {
        if (element.nodeType === 1) {
          // ELEMENT_NODE
          gererFils(element)
        } else if (element.nodeType === 3 || element.nodeType === 4) {
          // TEXT_NODE ou CDATA_SECTION_NODE
          for (const type of tabType) {
            const typeRegExp = new RegExp('\\(' + type + '([0-9]+)\\)', 'g')
            if (typeRegExp.test(element.nodeValue)) {
              element.nodeValue = element.nodeValue.replace(typeRegExp, function (match, num) {
                const nb = Number(num)
                // console.log('replace', type, match, num, objChgt)
                const value = objChgt['tab' + type][nb - 1]
                if (value == null) console.error(`Paramètres incohérents avec le programme utilisé (pas trouvé ${match} dans le tableau passé en paramètre, mais il est dans le xml)`)
                return value
              })
            }
          }
        }
      }
      return xml
    }

    const parser = new window.DOMParser()
    const xmlDoc = parser.parseFromString(stor.xmlString, 'text/xml')
    const xml = gererFils(xmlDoc)
    let x = xml.firstChild
    while (x.nodeType !== 1 && x.nextSibling) {
      x = x.nextSibling
    }
    try {
      Blockly.getMainWorkspace().clear()
      Blockly.Xml.domToWorkspace(x, Blockly.getMainWorkspace())
    } catch (error) {
      j3pShowError(error, { message: 'Programme invalide' })
    }
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 2,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      avecDeuxVariables: false,
      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.consigne1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.consigne1
          */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Programme de calcul',
        // on donne les phrases de la consigne
        consigne1: 'On donne ci-dessous un programme de calcul réalisé avec Blockly.',
        consigne2: 'Si on saisit $£n$ lorsqu’un nombre est demandé (première ligne), la valeur renvoyée à la fin de ce programme est @1@.',
        consigne3: 'Ce programme permet d’afficher l’image d’un nombre par la fonction $£f$ définie par :<br/>$£f(x)=$&1&.',
        // indic pour le format de la réponse
        info: 'L’expression devra être donnée sous forme réduite.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'On attend une réponse sous forme simplifiée (un nombre), pas un calcul&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'Ligne 1 : £{x1} vaut $£n$&nbsp;;',
        corr1_2: 'Ligne 2 : £{x2} vaut $£{c1}=£{r1}$&nbsp;;',
        corr1_3: 'Ligne 3 : £{x2} vaut $£{c2}=£{r2}$&nbsp;;',
        corr1_4: 'La valeur renvoyée est donc $£{r2}$.',
        corr2: '$£f$ est une fonction affine définie par $£f(x)=£r$.'
      }
    }
  }

  async function initSection () {
    stor.mtgAppLecteur = await getMtgCore({ withMathjax: true })
    await chargeExemple()
    enonceMain()
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      stor.zoneCalc = j3pAddElt(stor.conteneurD, 'div')
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.zoneCalc, 'Calculatrice', 'MepBoutons', 'Calculatrice', j3pToggleFenetres.bind(null, 'Calculatrice'))
    }
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div')
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.divBlockly = j3pAddElt(stor.zoneCons2, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1)
    const monXml = '<xml id="toolbox" style="display: none"></xml>'
    Blockly.inject(stor.divBlockly, {
      // media: "../keys_new/media/",
      zoom: {
        controls: false,
        wheel: false,
        startScale: 1,
        maxScale: 2,
        minScale: 0.5,
        scaleSpeed: 1.1
      },
      toolbox: $(monXml).find('#toolbox')[0], // telechargement du menu : ici il n’y en a pas
      readOnly: true
    })
    stor.divBlockly.style.height = '150px'
    stor.divBlockly.style.width = '700px'
    Blockly.svgResize(Blockly.mainWorkspace)
    let mesZonesSaisie

    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      // Pour la première étape, on détermine la valeur des variables
      const tabCoef = []
      tabCoef[0] = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(2, 6)
      do {
        tabCoef[1] = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
      } while (Math.abs(Math.abs(tabCoef[0]) - Math.abs(tabCoef[1])) < Math.pow(10, -12))
      stor.tabCoefInit = [...tabCoef]
      const tabSignes = ['MULTIPLY', 'ADD']
      if (tabCoef[1] < 0) {
        tabSignes[1] = 'MINUS'
        tabCoef[1] = -tabCoef[1]
      }
      stor.tabCoef = [...tabCoef]
      stor.tabSignes = tabSignes
      stor.obj = {}
      stor.obj.tabvar = tabCoef
      stor.obj.tabsigne = tabSignes
      // FIXME si on passe [x, x] ça plante dans Blockly avec `Variable "x" is already in use…`
      stor.obj.tabnomvar = (ds.avecDeuxVariables) ? ['x', 'y'] : ['x', 'x']
      // stor.obj.tabnomvar = ['x', 'y']
    }

    changeExemple(stor.obj, ['signe', 'var', 'nomvar'])

    if ((me.questionCourante - 1) % ds.nbetapes === 0) {
      // On choisit un nombre au hasard
      stor.nb = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
      const elt = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2,
        {
          n: stor.nb,
          input1: { texte: '', dynamique: true, maxchars: 5 }
        })
      stor.zoneInput = elt.inputList[0]
      stor.zoneInput.typeReponse = ['nombre', 'exact']
      stor.zoneInput.reponse = [stor.nb * stor.tabCoefInit[0] + stor.tabCoefInit[1]]
      j3pRestriction(stor.zoneInput, '0-9\\-')
      mesZonesSaisie = [stor.zoneInput.id]
      stor.zoneInput.solutionSimplifiee = ['non valide']// on peut mettre "valide et on accepte", "non valide" ou "reponse fausse";
      j3pFocus(stor.zoneInput)
    } else {
      stor.nomf = j3pRandomTab(['f', 'g', 'h'], [0.333, 0.333, 0.334])
      const elt = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne3,
        {
          f: stor.nomf,
          inputmq1: { texte: '' }
        })
      stor.zoneInput = elt.inputmqList[0]
      stor.zoneInput.typeReponse = ['texte']
      stor.zoneInput.reponse = [j3pGetLatexMonome(1, 1, stor.tabCoefInit[0], 'x') + j3pGetLatexMonome(2, 0, stor.tabCoefInit[1], 'x')]
      mqRestriction(stor.zoneInput, '\\d\\-x+*')
      mesZonesSaisie = [stor.zoneInput.id]
      resetFigMtg()
      j3pAffiche(stor.zoneCons3, '', ds.textes.info)
      j3pStyle(stor.zoneCons3, { fontStyle: 'italic', fontSize: '0.9em' })
      stor.liste.giveFormula2('coefa', String(stor.tabCoefInit[0]))
      stor.liste.giveFormula2('coefb', String(stor.tabCoefInit[1]))
      stor.liste.calculateNG()
      j3pFocus(stor.zoneInput)
    }

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)

    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage({ structure: ds.structure })

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

        initSection()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      let reponse
      if ((me.questionCourante - 1) % ds.nbetapes === 0) {
        reponse = fctsValid.validationGlobale()
      } else {
        reponse = { aRepondu: false, bonneReponse: true }
        reponse.aRepondu = fctsValid.valideReponses()
        const repeleve = fctsValid.zones.reponseSaisie[0]
        let repEleveXcas = mqNormalise(repeleve)
        repEleveXcas = j3pAjouteFoisDevantX(repEleveXcas, 'x')
        if (reponse.aRepondu) {
          stor.liste.giveFormula2('rep', repEleveXcas)
          stor.liste.calculateNG()
          reponse.bonneReponse = (stor.liste.valueOf('egalite') === 1)
          fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
          fctsValid.coloreUneZone(stor.zoneInput.id)
        }
      }

      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée
        let msgReponseManquante
        if ((me.questionCourante - 1) % ds.nbetapes === 0) {
          if (!fctsValid.zones.reponseSimplifiee[0][0]) {
            // c’est que la réponse est sous la forme d’un calcul et non un nombre
            msgReponseManquante = ds.textes.comment1
          }
        }
        me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        return me.finCorrection()
      }

      // Une réponse a été saisie
      if (reponse.bonneReponse) {
        // Bonne réponse
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        afficheCorrection(true)
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(false)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        // ici il a encore la possibilité de se corriger
        return me.finCorrection()
      }
      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(false)
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
