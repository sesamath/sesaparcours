import $ from 'jquery'
import { j3pShuffle, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pMonome, j3pPaletteMathquill, j3pAddElt } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
      DENIAUD Rémi
      mars 2020
      On demande de calculer certaines limites de fonction en précisant l’opération qui permet de conclure
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['typeLim', 'inf', 'liste', 'On peut préciser en quel type de valeur on cherche la limte : "inf" pour +infini ou -infini, "reel" pour la limite en un réel ou bien "les deux".', ['inf', 'reel', 'les deux']],
    ['avecCompo', false, 'boolean', 'Lorsque ce paramètre vaut true, alors "composition" apparait dans les propositions.']
  ]
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCons4)
    j3pDetruit(stor.zoneCons5)
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    // anciens divs zoneexplication(1|2|3)
    let div = j3pAddElt(explications, 'div')
    j3pAffiche(div, '', ds.textes['corr1_' + stor.elt2.selectList[0].reponse])
    div = j3pAddElt(explications, 'div')
    j3pAffiche(div, '', ds.textes.corr2, stor.objCons)
    div = j3pAddElt(explications, 'div')
    j3pAffiche(div, '', ds.textes['corr3_' + stor.elt2.selectList[0].reponse], stor.objCons)
  }

  function donneesFonction (num) {
    // num est le cas de figure
    // Lorsqu’il est entre 1 et 10, on a une limite en l’infini. S’il est entre 11 et 20, on a une limite en un réel
    // Cette fonction renvoie un objet contenant différentes propriétés de la suite (son expression, sa limite, ...)
    let n, m, a, b, c, signe
    const obj = {}
    const cote = j3pGetRandomInt(0, 1) // pour la limite en un réel, on choisit à gauche ou à droite
    obj.tabLim1 = []
    obj.tabLim2 = []
    obj.tabLim = [] // ces trois tableaux peuvent servir à accepter des limites qui ne seraient pas les premiers choix (par exemple 1^+ alors que 1 suffirait)
    if (num === 1) {
      // x^n+exp(x)
      // limite en xlim égal à +\\infty ou -\\infty
      obj.xlim = (j3pGetRandomInt(0, 1) === 0) ? '+\\infty' : '-\\infty'
      n = (obj.xlim === '-\\infty') ? j3pGetRandomInt(1, 3) : 1 + j3pGetRandomInt(0, 1) * 2
      obj.fdex = ((n === 1) ? 'x' : 'x^' + n) + '+\\mathrm{e}^x'
      obj.f1 = ((n === 1) ? 'x' : 'x^' + n)
      obj.f2 = '\\mathrm{e}^x'
      obj.lim1 = (obj.xlim === '+\\infty')
        ? '+\\infty'
        : (n % 2 === 0) ? '+\\infty' : '-\\infty'
      obj.lim2 = (obj.xlim === '+\\infty') ? '+\\infty' : '0'
      if (obj.xlim === '-\\infty') obj.tabLim2.push('0^+', '0+')
      obj.lim = obj.lim1
      obj.numOpe = 1 // somme
    } else if (num === 2) {
      // (ax+b)exp(x)
      // limite en xlim égal à +\\infty
      obj.xlim = '+\\infty'
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(Math.abs(a) - Math.abs(b)) < Math.pow(10, -12))
      obj.fdex = '(' + j3pMonome(1, 1, a) + j3pMonome(2, 0, b) + ')\\mathrm{e}^x'
      obj.f1 = j3pMonome(1, 1, a) + j3pMonome(2, 0, b)
      obj.f2 = '\\mathrm{e}^x'
      obj.lim1 = (a > 0) ? '+\\infty' : '-\\infty'
      obj.lim2 = '+\\infty'
      obj.lim = obj.lim1
      obj.numOpe = 2 // produit
    } else if (num === 3) {
      // (ax+b)/exp(x)
      // limite en xlim égal à -\\infty
      obj.xlim = '-\\infty'
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(Math.abs(a) - Math.abs(b)) < Math.pow(10, -12))
      obj.fdex = '\\frac{' + j3pMonome(1, 1, a) + j3pMonome(2, 0, b) + '}{\\mathrm{e}^x}'
      obj.f1 = j3pMonome(1, 1, a) + j3pMonome(2, 0, b)
      obj.f2 = '\\mathrm{e}^x'
      obj.lim1 = (a > 0) ? '-\\infty' : '+\\infty'
      obj.lim2 = '0^+'
      obj.tabLim2.push('0+')
      obj.lim = obj.lim1
      obj.numOpe = 3 // quotient
    } else if (num === 4) {
      // exp(x)+1/(ax^n+b)
      // limite en xlim égal à +\\infty ou -\\infty
      obj.xlim = (j3pGetRandomInt(0, 1) === 0) ? '+\\infty' : '-\\infty'
      n = j3pGetRandomInt(1, 3)
      signe = (2 * j3pGetRandomInt(0, 1) - 1)
      a = j3pGetRandomInt(1, 5) * signe
      do {
        b = j3pGetRandomInt(1, 5) * (-signe)
      } while (Math.abs(Math.abs(a) - Math.abs(b)) < Math.pow(10, -12))
      obj.fdex = '\\mathrm{e}^x+\\frac{1}{' + j3pMonome(1, n, a) + j3pMonome(2, 0, b) + '}'
      obj.f1 = '\\mathrm{e}^x'
      obj.f2 = '\\frac{1}{' + j3pMonome(1, n, a) + j3pMonome(2, 0, b) + '}'
      obj.lim1 = (obj.xlim === '+\\infty') ? '+\\infty' : '0'
      obj.tabLim1.push('0^+', '0+')
      obj.lim2 = '0'
      const limitePrecise2 = (n % 2 === 0)
        ? (a > 0) ? '0^+' : '0^-'
        : (obj.xlim === '+\\infty')
            ? (a > 0) ? '0^+' : '0^-'
            : (a > 0) ? '0^-' : '0^+'
      obj.tabLim2.push(limitePrecise2, '0' + limitePrecise2[2])
      if (limitePrecise2 === '0^+') obj.tabLim.push('0^+', '0+')
      obj.lim = obj.lim1
      obj.numOpe = 1 // somme
    } else if (num === 5) {
      // (aexp(x)+b)/exp(x)
      // limite en xlim égal à -\\infty
      obj.xlim = '-\\infty'
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(Math.abs(a) - Math.abs(b)) < Math.pow(10, -12))
      obj.fdex = '\\frac{' + j3pMonome(1, 1, a, '\\mathrm{e}^x') + j3pMonome(2, 0, b) + '}{\\mathrm{e}^x}'
      obj.f1 = j3pMonome(1, 1, a, '\\mathrm{e}^x') + j3pMonome(2, 0, b)
      obj.f2 = '\\mathrm{e}^x'
      obj.lim1 = String(b)
      obj.tabLim1.push((a > 0) ? String(b) + '^+' : String(b) + '^-')
      obj.tabLim1.push((a > 0) ? String(b) + '+' : String(b) + '-')
      obj.lim2 = '0^+'
      obj.tabLim2.push('0+')
      obj.lim = (b > 0) ? '+\\infty' : '-\\infty'
      obj.numOpe = 3 // quotient
    } else if (num === 6) {
      // ax^n+b+c/x^m
      // limite en xlim égal à +\\infty ou -\\infty
      obj.xlim = (j3pGetRandomInt(0, 1) === 0) ? '+\\infty' : '-\\infty'
      n = j3pGetRandomInt(1, 3)
      m = j3pGetRandomInt(1, 3)
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(Math.abs(a) - Math.abs(b)) < Math.pow(10, -12))
      c = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      obj.fdex = j3pMonome(1, n, a, 'x') + j3pMonome(2, 0, b) + ((c > 0) ? '+' : '-') + '\\frac{' + Math.abs(c) + '}{' + ((m === 1) ? 'x' : 'x^' + m) + '}'
      obj.f1 = j3pMonome(1, n, a, 'x') + j3pMonome(2, 0, b)
      obj.f2 = ((c > 0) ? '' : '-') + '\\frac{' + Math.abs(c) + '}{' + ((m === 1) ? 'x' : 'x^' + m) + '}'
      obj.lim1 = (obj.xlim === '+\\infty')
        ? (a > 0) ? '+\\infty' : '-\\infty'
        : (n % 2 === 0)
            ? (a > 0) ? '+\\infty' : '-\\infty'
            : (a > 0) ? '-\\infty' : '+\\infty'
      obj.lim2 = '0'
      obj.tabLim2.push((c > 0) ? '0^+' : '0^-')
      obj.tabLim2.push((c > 0) ? '0+' : '0-')
      obj.lim = obj.lim1
      obj.numOpe = 1 // somme
    } else if (num === 7) {
      // exp(x)/(ax+b)
      // limite en xlim égal à -\\infty
      obj.xlim = '-\\infty'
      signe = (2 * j3pGetRandomInt(0, 1) - 1)
      a = j3pGetRandomInt(1, 5) * signe
      do {
        b = j3pGetRandomInt(1, 5) * (-signe)
      } while (Math.abs(Math.abs(a) - Math.abs(b)) < Math.pow(10, -12))
      obj.fdex = '\\frac{\\mathrm{e}^x}{' + j3pMonome(1, 1, a) + j3pMonome(2, 0, b) + '}'
      obj.f1 = '\\mathrm{e}^x'
      obj.f2 = j3pMonome(1, 1, a) + j3pMonome(2, 0, b)
      obj.lim1 = '0'
      obj.tabLim1.push('0^+', '0+')
      obj.lim2 = (a > 0) ? '-\\infty' : '+\\infty'
      obj.lim = '0'
      obj.tabLim.push((a > 0) ? '0^-' : '0^+')
      obj.tabLim.push((a > 0) ? '0-' : '0+')
      obj.numOpe = 3 // quotient
    } else if (num === 8) {
      // \\sqrt{x}+ax^n+b (a>0)
      // limite en xlim égal à +\\infty
      obj.xlim = '+\\infty'
      a = j3pGetRandomInt(2, 5)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(Math.abs(a) - Math.abs(b)) < Math.pow(10, -12))
      n = j3pGetRandomInt(1, 3)
      obj.fdex = '\\sqrt{x}' + j3pMonome(2, n, a) + j3pMonome(3, 0, b)
      obj.f1 = '\\sqrt{x}'
      obj.f2 = j3pMonome(1, n, a) + j3pMonome(2, 0, b)
      obj.lim1 = '+\\infty'
      obj.lim2 = '+\\infty'
      obj.lim = '+\\infty'
      obj.numOpe = 1 // somme
    } else if (num === 11) {
      // (ax+b)/(x+c)
      // limite en xlim égal à -c^+ ou -c^-
      signe = (2 * j3pGetRandomInt(0, 1) - 1)
      a = j3pGetRandomInt(1, 5) * signe
      do {
        b = j3pGetRandomInt(1, 5) * (-signe)
      } while (Math.abs(Math.abs(a) - Math.abs(b)) < Math.pow(10, -12))
      c = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      obj.xlim = -c + '^' + ((cote === 0) ? '+' : '-')
      obj.xlim1 = -c
      obj.xlim2 = obj.xlim
      obj.fdex = '\\frac{' + j3pMonome(1, 1, a) + j3pMonome(2, 0, b) + '}{x' + j3pMonome(2, 0, c) + '}'
      obj.f1 = j3pMonome(1, 1, a) + j3pMonome(2, 0, b)
      obj.f2 = 'x' + j3pMonome(2, 0, c)
      obj.lim1 = String(a * (-c) + b)
      obj.lim2 = '0^' + ((cote === 0) ? '+' : '-')
      obj.tabLim2.push('0' + ((cote === 0) ? '+' : '-'))
      obj.lim = (obj.lim1[0] === '-')
        ? (cote === 0) ? '-\\infty' : '+\\infty'
        : (cote === 0) ? '+\\infty' : '-\\infty'
      obj.numOpe = 3 // quotient
    } else if (num === 12) {
      // ax/((x+b)^2)
      // limite en xlim égal à -b
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(Math.abs(a) - Math.abs(b)) < Math.pow(10, -12))
      obj.xlim = -b
      obj.xlim1 = -b
      obj.xlim2 = obj.xlim
      obj.fdex = '\\frac{' + j3pMonome(1, 1, a) + '}{(x' + j3pMonome(2, 0, b) + ')^2}'
      obj.f1 = j3pMonome(1, 1, a)
      obj.f2 = '(x' + j3pMonome(2, 0, b) + ')^2'
      obj.lim1 = String(a * (-b))
      obj.lim2 = '0^+'
      obj.tabLim2.push('0+')
      obj.lim = (a * (-b) > 0) ? '+\\infty' : '-\\infty'
      obj.numOpe = 3 // quotient
    } else if (num === 13) {
      // \\sqrt{x-a}exp(x)
      // limite en xlim égal à -b
      a = j3pGetRandomInt(1, 5)
      obj.xlim = a
      obj.xlim1 = a
      obj.xlim2 = obj.xlim
      obj.fdex = '\\sqrt{x' + j3pMonome(2, 0, -a) + '}\\mathrm{e}^x'
      obj.f1 = '\\sqrt{x' + j3pMonome(2, 0, -a) + '}'
      obj.f2 = '\\mathrm{e}^x'
      obj.lim1 = '0'
      obj.lim2 = '\\mathrm{e}^' + a
      obj.lim = '0'
      obj.numOpe = 2 // produit
    } else if (num === 14) {
      // a/x+exp(x)
      // limite en xlim égal à 0^+ ou 0^-
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      obj.xlim = '0^' + ((cote === 0) ? '+' : '-')
      obj.xlim1 = obj.xlim
      obj.xlim2 = '0'
      obj.fdex = '\\frac{' + a + '}{x}+\\mathrm{e}^x'
      obj.f1 = '\\frac{' + a + '}{x}'
      obj.f2 = '\\mathrm{e}^x'
      obj.lim1 = (a > 0)
        ? (cote === 0) ? '+\\infty' : '-\\infty'
        : (cote === 0) ? '-\\infty' : '+\\infty'
      obj.lim2 = '1'
      obj.lim = obj.lim1
      obj.numOpe = 1 // somme
    } else if (num === 15) {
      // aexp(x)/sqrt(x)
      // limite en xlim égal à 0^+
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      obj.xlim = '0^+'
      obj.xlim1 = '0'
      obj.xlim2 = '0^+'
      obj.fdex = '\\frac{' + j3pMonome(1, 1, a, '\\mathrm{e}^x') + '}{\\sqrt{x}}'
      obj.f1 = j3pMonome(1, 1, a, '\\mathrm{e}^x')
      obj.f2 = '\\sqrt{x}'
      obj.lim1 = a
      obj.lim2 = '0^+'
      obj.tabLim2.push('0+')
      obj.lim = (a > 0) ? '+\\infty' : '-\\infty'
      obj.numOpe = 3 // quotient
    } else if (num === 16) {
      // exp(x)/(x-a) ou exp(x)/(a-x)
      // limite en xlim égal à a^+ ou a^-
      const choixfct = j3pGetRandomInt(0, 1)
      a = j3pGetRandomInt(2, 6)
      obj.xlim = (cote === 0) ? String(a) + '^+' : String(a) + '^-'
      obj.xlim1 = String(a)
      obj.xlim2 = obj.xlim
      obj.fdex = (choixfct === 0) ? '\\frac{\\mathrm{e}^x}{x-' + String(a) + '}' : '\\frac{\\mathrm{e}^x}{' + String(a) + '-x}'
      obj.f1 = '\\mathrm{e}^x'
      obj.f2 = (choixfct === 0) ? 'x-' + String(a) : '' + String(a) + '-x'
      obj.lim1 = '\\mathrm{e}^' + String(a)
      obj.lim2 = (choixfct === 0)
        ? (cote === 0) ? '0^+' : '0^-'
        : (cote === 0) ? '0^-' : '0^+'
      obj.tabLim2.push(obj.lim2.replace('^', ''))
      obj.lim = (choixfct === 0)
        ? (cote === 0) ? '+\\infty' : '-\\infty'
        : (cote === 0) ? '-\\infty' : '+\\infty'
      obj.numOpe = 3 // quotient
    } else if (num === 17) {
      // a/(x-b)+c/(x-b)^2 les deux quotient devant tendre vers le même infini
      // limite en xlim égal à b^+ ou b^-
      signe = (2 * j3pGetRandomInt(0, 1) - 1)
      a = j3pGetRandomInt(1, 6) * signe
      b = j3pGetRandomInt(1, 5)
      do {
        c = (cote === 0) ? j3pGetRandomInt(1, 6) * signe : j3pGetRandomInt(1, 6) * (-signe)
      } while (Math.abs(Math.abs(a) - Math.abs(c)) < Math.pow(10, -12))
      obj.xlim = (cote === 0) ? String(b) + '^+' : String(b) + '^-'
      obj.xlim1 = obj.xlim
      obj.xlim2 = obj.xlim
      obj.fdex = '\\frac{' + String(a) + '}{x-' + String(b) + '}' + ((c > 0) ? '+' : '-') + '\\frac{' + Math.abs(c) + '}{(x-' + String(b) + ')^2}'
      obj.f1 = '\\frac{' + String(a) + '}{x-' + String(b) + '}'
      obj.f2 = ((c > 0) ? '' : '-') + '\\frac{' + Math.abs(c) + '}{(x-' + String(b) + ')^2}'
      obj.lim2 = (c > 0) ? '+\\infty' : '-\\infty'
      obj.lim1 = obj.lim2
      obj.lim = obj.lim1
      obj.numOpe = 1 // somme
    }
    if (num <= 10) {
      obj.xlim1 = obj.xlim // xlim1 sert pour les limites en un réel; On n’a pas besoin de préciser parfois de quel côté on se trouve
      obj.xlim2 = obj.xlim // xlim1 sert pour les limites en un réel; On n’a pas besoin de préciser parfois de quel côté on se trouve
    }
    obj.l = '\\limite{x}{' + obj.xlim + '}{' + obj.fdex + '}'
    obj.l1 = '\\limite{x}{' + obj.xlim1 + '}{' + obj.f1 + '}'
    obj.l2 = '\\limite{x}{' + obj.xlim2 + '}{' + obj.f2 + '}'
    return obj
  }

  function ecoute (nom) {
    if (nom.className.indexOf('mq-editable-field') > -1) {
      stor.zoneCons5.innerHTML = ''
      j3pPaletteMathquill(stor.zoneCons5, nom, {
        liste: ['inf', 'fraction', 'puissance', 'racine', 'exp']
      })
    }
  }

  function choixElt (tab, tabInit, leTableau) {
    // Cette fonction complète leTableau par une valeur choisie au hasard dans tab.
    // Si tab est vide, elle fait une copie de tabInit dans tab auparavant
    if (tab.length === 0) tab = [...tabInit]
    const pioche = j3pGetRandomInt(0, (tab.length - 1))
    leTableau.push(tab[pioche])
    tab.splice(pioche, 1)
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 5,
      nbetapes: 1,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.
      typeLim: 'inf',
      avecCompo: false,
      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'limite d’une fonction',
        // on donne les phrases de la consigne
        consigne1: 'Calculer $£l$ en suivant les étapes&nbsp;:',
        consigne2: '$£{l1}=$&1& et $£{l2}=$&2&.',
        consigne3: 'Ainsi par #1#, on conclut que $£l=$&1&.',
        consigne4: '|somme|produit|quotient',
        composition: 'composition',
        remarque: 'Tu n’as le droit qu’à une seule tentative.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Pour l’infini, il faut préciser le signe&nbsp;!',
        comment2: 'Toutes les limites doivent être données sous une forme simple&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'La fonction dont on cherche la limite est une somme de deux fonctions admettant des limites.',
        corr1_2: 'La fonction dont on cherche la limite est un produit de deux fonctions admettant des limites.',
        corr1_3: 'La fonction dont on cherche la limite est un quotient de deux fonctions admettant des limites.',
        corr1_4: 'La fonction dont on cherche la limite est une composition de deux fonctions admettant des limites.',
        corr2: '$£{l1}=£{lim1}$ et $£{l2}=£{lim2}$.',
        corr3_1: 'Le somme ne conduit pas à une forme indéterminée et on conclut que $£l=£{lim}$.',
        corr3_2: 'Le produit ne conduit pas à une forme indéterminée et on conclut que $£l=£{lim}$.',
        corr3_3: 'Le quotient ne conduit pas à une forme indéterminée et on conclut que $£l=£{lim}$.',
        corr3_4: 'Par la propriété sur la limite des fonctions composées, on conclut que $£l=£{lim}$.'
      },
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    // Construction de la page
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })

    // par convention, me.stockage[0] contient la solution
    // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zones.IG, ds.indication)

    const tabQuestInf = []
    const tabQuestReel = []
    let k
    stor.tabQuest = []
    if (ds.typeLim !== 'reel') {
      for (k = 1; k <= 8; k++) {
        tabQuestInf.push(k)
      }
    }
    const tabQuestInfInit = [...tabQuestInf]
    if (ds.typeLim !== 'inf') {
      for (k = 11; k <= 17; k++) {
        tabQuestReel.push(k)
      }
    }
    const tabQuestReelInit = [...tabQuestReel]
    if (ds.typeLim === 'inf') {
      // Je ne choisis quand dans les limites en l’infini
      for (k = 1; k <= ds.nbrepetitions; k++) choixElt(tabQuestInf, tabQuestInfInit, stor.tabQuest)
    } else if (ds.typeLim === 'reel') {
      // Je ne choisis quand dans les limites en un réel
      for (k = 1; k <= ds.nbrepetitions; k++) choixElt(tabQuestReel, tabQuestReelInit, stor.tabQuest)
    } else {
      // je dois choisir dans les deux types
      const choix1 = j3pGetRandomInt(0, 1)
      for (k = 1; k <= ds.nbrepetitions; k++) {
        if ((choix1 + k) % 2 === 0) {
          choixElt(tabQuestInf, tabQuestInfInit, stor.tabQuest)
        } else {
          choixElt(tabQuestReel, tabQuestReelInit, stor.tabQuest)
        }
      }
      stor.tabQuest = j3pShuffle(stor.tabQuest)
    }

    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    let i
    stor.quest = stor.tabQuest[me.questionCourante - 1]
    stor.objCons = {}
    Object.assign(stor.objCons, donneesFonction(stor.quest))
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(me.zones.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (i = 1; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, stor.objCons)
    stor.elt1 = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, {
      l1: stor.objCons.l1,
      l2: stor.objCons.l2,
      inputmq1: { texte: '' },
      inputmq2: { texte: '' }
    })
    const listeChoix = ds.textes.consigne4.split('|')
    if (ds.avecCompo) listeChoix.push(ds.textes.composition)
    stor.elt2 = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3, {
      l: stor.objCons.l,
      liste1: { texte: listeChoix },
      inputmq1: { texte: '' }
    })
    if (ds.nbchances === 1) {
      j3pAffiche(stor.zoneCons4, '', ds.textes.remarque, {
        style: { fontStyle: 'italic', fontSize: '0.9em' }
      })
    }
    stor.inputMqList = stor.elt1.inputmqList.concat(stor.elt2.inputmqList)
    stor.inputMqList.forEach(eltInput => {
      $(eltInput).focusin(ecoute.bind(null, eltInput))
      mqRestriction(eltInput, '\\d,+./^-', { commandes: ['inf', 'fraction', 'puissance', 'racine', 'exp'] })
      eltInput.typeReponse = ['texte']
    })
    const tabLim = []
    let numLim = 0
    const tabLimites = [stor.objCons.lim1, stor.objCons.lim2, stor.objCons.lim]
    tabLimites.forEach(function (x) {
      tabLim[numLim] = [String(x)]
      if (['+', '-'].indexOf(String(x).charAt(String(x).length - 1)) > -1) {
        // On a une limite de la forme a^+ ou a^- donc j’accepte comme limite a
        tabLim[numLim].push(String(x).substring(0, String(x).length - 2))
      }
      if (String(x).indexOf('mathrm{e}') > -1) {
        // on a une limite avec exponentielle, donc on doit la transformer sous la forme e^a pour accepter la valeur
        tabLim[numLim].push(String(x).replace('\\mathrm{e}', 'e'))
      }
      numLim++
    })
    j3pFocus(stor.elt1.inputmqList[0])
    stor.elt1.inputmqList[0].reponse = tabLim[0].concat(stor.objCons.tabLim1)
    stor.elt1.inputmqList[1].reponse = tabLim[1].concat(stor.objCons.tabLim2)
    stor.elt2.inputmqList[0].reponse = tabLim[2].concat(stor.objCons.tabLim)
    stor.elt2.selectList[0].reponse = stor.objCons.numOpe

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zones.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = stor.inputMqList.map(elt => elt.id).concat([stor.elt2.selectList[0].id])
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        let reponse = {}
        let i
        let infini = false
        let nbInf = -1
        let estCalcul = false
        let nbCalcul = -1
        reponse.aRepondu = stor.fctsValid.valideReponses()
        if (reponse.aRepondu) {
        // JE regarde si l’élève n’a écrit que infini sans signe
          for (i = 2; i >= 0; i--) {
            if (stor.fctsValid.zones.reponseSaisie[i] === '\\infty') {
              infini = true
              nbInf = i
            } else if (!String(stor.fctsValid.zones.reponseSaisie[i]).includes('\\infty')) {
              let estExpo = false
              const valSaisie = stor.fctsValid.zones.reponseSaisie[i]
              estExpo = (estExpo || /e\^[1-9]/.test(valSaisie))
              estExpo = (estExpo || /e\^({-?[1-9]+)/.test(valSaisie))
              if (!/\^[+-]/.test(valSaisie) && !estExpo && ['+', '-'].indexOf(valSaisie.charAt(valSaisie.length - 1)) === -1) {
              // console.log(j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[i]),stor.fctsValid.zones.reponseSaisie[i])
                const valeur = j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[i])
                estCalcul = ((String(valeur) !== String(stor.fctsValid.zones.reponseSaisie[i])) && Math.abs(Math.round(valeur) - valeur) < Math.pow(10, -12))
                nbCalcul = i
              }
            }
          }
          if (infini || estCalcul) {
            reponse.aRepondu = false
          } else {
            reponse = stor.fctsValid.validationGlobale()
          }
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          let msgReponseManquante
          if (infini) {
            msgReponseManquante = ds.textes.comment1
            if (nbInf === 2) j3pFocus(stor.elt1.inputmqList[0])
            else j3pFocus(stor.elt1.inputmqList[nbInf])
          } else if (estCalcul) {
            msgReponseManquante = ds.textes.comment2
            if (nbCalcul === 2) j3pFocus(stor.elt1.inputmqList[0])
            else j3pFocus(stor.elt1.inputmqList[nbCalcul])
          }
          this.reponseManquante(stor.zoneCorr, msgReponseManquante)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
