import { j3pShuffle, j3pElement, j3pFocus, j3pGetRandomInt, j3pMonome, j3pPaletteMathquill, j3pAjouteBouton, j3pAddElt, j3pStyle, j3pEmpty, j3pDetruit, j3pGetNewId } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitFenetres, j3pAfficheCroixFenetres } from 'src/legacy/core/functionsJqDialog'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    avril 2014
    cette section permet de se tester sur des limites avec la fonction ex^p
    Nous sommes en présence d’une composition
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['tab_choix', [1, 2, 3, 4, 5], 'array', 'tableau contenant la liste des limites demandées (on peut en mettre moins voire doubler certains) :<br>1 pour a/(b+exp(c*x)),<br>2 pour ax*exp(-ax),<br>3 pour a(1-exp(-x/b)),<br>4 pour (a/x)*exp(a/x) avec a>0,<br>5 pour (a/x)*exp(a/x) avec a<0']

  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function purge (tableauChoix) {
    // cette fonction va me permettre de prévenir un éventuel mauvais remplissage du tableau sur le choix des limites
    const tableauPurge = []
    for (let j = 0; j < tableauChoix.length; j++) {
      if (!isNaN(Number(tableauChoix[j]))) {
        if ((tableauChoix[j] === 1) || (tableauChoix[j] === 2) || (tableauChoix[j] === 3) || (tableauChoix[j] === 4) || (tableauChoix[j] === 5)) {
          tableauPurge.push(tableauChoix[j])
        }
      }
    }
    // à cet instant si tableau_epure est vide, je lui mets un élément : 1;
    if (tableauPurge.length === 0) {
      tableauPurge.push(1)
    }
    return tableauPurge
  }
  function aide (num) {
    // num correspond au numéro de la limite demandée (dans le tableau tab_choix)
    // 1 pour a/(b+exp(cx)), 2 pour (a+bln(x))/x, 3 pour ax+bln(x), 4 pour (a+bln(x))/x^2, 5 pour ax^2+b+cln(x)
    const titreAide = ds.textes.aide
    stor.fenetreAide = j3pGetNewId('fenetreAide')
    stor.fenetreCourbe = j3pGetNewId('fenetreCourbe')
    me.fenetresjq = [{ name: 'Aide', title: titreAide, width: Number(me.zonesElts.MG.offsetWidth), left: 50, top: 50, id: stor.fenetreAide },
      { name: 'Courbe', title: ds.textes.courbe, top: me.zonesElts.HD.offsetHeight + 70, left: me.zonesElts.MG.offsetWidth + 10, width: 280, id: stor.fenetreCourbe }]
    j3pCreeFenetres(me)
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneBtn, 'Aide', 'MepBoutons', titreAide, j3pToggleFenetres.bind(null, 'Aide'))
    j3pAfficheCroixFenetres('Aide')
    stor.coursDebut = j3pAddElt(stor.fenetreAide, 'div')
    let monAide
    let monFI
    if (num === 2) {
      if (stor.coefA > 0) {
        monFI = ds.textes.FI2
      } else {
        monFI = ds.textes.FI1
      }
    } else if ((num === 4) || (num === 5)) {
      if (stor.coefA > 0) {
        monFI = ds.textes.FI0
      } else {
        monFI = ds.textes.FI3
      }
    } else {
      monFI = ds.textes.FI0
    }
    if ((num === 1) || (num === 3)) {
      monAide = ds.textes.aide1
    } else if ((num === 4) || (num === 5)) {
      if (stor.coefA > 0) {
        monAide = ds.textes.aide1
      } else {
        monAide = ds.textes.aide2
      }
    } else {
      monAide = ds.textes.aide2
    }
    j3pAffiche(stor.coursDebut, '', monFI + '\n' + monAide)
  }
  function creationCourbe () {
    // création de la courbe :
    stor.courbeCours = j3pAddElt(stor.fenetreCourbe, 'div')
    let par2Init, par2Fin
    stor.idRepere = j3pGetNewId('unrepere')
    if (stor.num_quest === 1) {
      if (stor.coefB > 0) {
        me.repere = new Repere({
          idConteneur: stor.courbeCours,
          idDivRepere: stor.idRepere,
          aimantage: true,
          visible: true,
          trame: true,
          fixe: true,
          larg: 240,
          haut: 350,

          pasdunegraduationX: 1,
          pixelspargraduationX: 20,
          pasdunegraduationY: 1,
          pixelspargraduationY: 50,
          xO: 120,
          yO: 175,
          debuty: 0,
          negatifs: true,
          objets: [
            { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: -10, par3: 10, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
          ]
        })
      } else {
        const zeroFct = Math.log(-stor.coefB) / stor.coefC
        me.repere = new Repere({
          idConteneur: stor.courbeCours,
          idDivRepere: stor.idRepere,
          aimantage: true,
          visible: true,
          trame: true,
          fixe: true,
          larg: 240,
          haut: 350,

          pasdunegraduationX: 1,
          pixelspargraduationX: 20,
          pasdunegraduationY: 1,
          pixelspargraduationY: 50,
          xO: 120,
          yO: 175,
          debuty: 0,
          negatifs: true,
          objets: [
            { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: -10, par3: zeroFct - 0.01, par4: 300, style: { couleur: '#F00', epaisseur: 2 } },
            { type: 'courbe', nom: 'c2', par1: stor.fonction, par2: zeroFct + 0.01, par3: 10, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
          ]
        })
      }
    } else if (stor.num_quest === 2) {
      if (stor.coefA > 0) {
        par2Init = -2
        par2Fin = 10
      } else {
        par2Init = -10
        par2Fin = 2
      }
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 240,
        haut: 300,

        pasdunegraduationX: 1,
        pixelspargraduationX: 30,
        pasdunegraduationY: 1,
        pixelspargraduationY: 60,
        xO: 120,
        yO: 100,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: par2Init, par3: par2Fin, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else if (stor.num_quest === 3) {
      let grady = 5
      if (stor.coefA >= 20) {
        grady = 10
      }
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 260,
        haut: 300,

        pasdunegraduationX: 5,
        pixelspargraduationX: 40,
        pasdunegraduationY: grady,
        pixelspargraduationY: 40,
        xO: 100,
        yO: 150,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: -30, par3: 50, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else if ((stor.num_quest === 4) || (stor.num_quest === 5)) {
      par2Init = 0.05
      par2Fin = 10
      let posYO = 250
      let grady = 2
      let pixelsY = 40
      if (stor.coefA < 0) {
        grady = 0.5
        pixelsY = 80
        posYO = 100
      } else {
        par2Init = 0.5
      }
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 260,
        haut: 300,

        pasdunegraduationX: 1,
        pixelspargraduationX: 30,
        pasdunegraduationY: grady,
        pixelspargraduationY: pixelsY,
        xO: 50,
        yO: posYO,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: par2Init, par3: par2Fin, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    }
    me.repere.construit()
    j3pElement(stor.idRepere).style.position = 'relative'
  }
  function ecoute (eltInput) {
    if (eltInput.className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, eltInput, { liste: ['racine', 'fraction', 'puissance', 'inf'] })
    }
  }
  function afficheCorrection (num, bonneRep) {
    j3pDetruit(stor.zoneBtn)
    let maLimite1
    let maLimite2
    let maLimite3
    let quotient
    j3pStyle(stor.zoneExpli, { color: (bonneRep) ? me.styles.cbien : me.styles.petit.correction.color })
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(stor.zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr0)
    if (num === 1) {
      // \\frac{a}{b+exp(cx)}
      if (stor.coefC > 0) {
        maLimite1 = '-\\infty'
        maLimite2 = '0'
        quotient = Math.abs(stor.coefA) / Math.abs(stor.coefB)
        if (Math.abs(quotient - Math.round(quotient)) < Math.pow(10, -12)) {
          // quotient entier
          maLimite3 = stor.coefA / stor.coefB
        } else {
          // je fais attention au signe du quotient
          if (stor.coefA * stor.coefB > 0) {
            maLimite3 = '\\frac{' + Math.abs(stor.coefA) + '}{' + Math.abs(stor.coefB) + '}'
          } else {
            maLimite3 = '\\frac{-' + Math.abs(stor.coefA) + '}{' + Math.abs(stor.coefB) + '}'
          }
        }
      } else {
        maLimite1 = '+\\infty'
        maLimite2 = '+\\infty'
        maLimite3 = '0'
      }
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr1,
        {
          f: stor.nomFct,
          c: j3pMonome(1, 1, stor.coefC),
          l: maLimite1,
          m: maLimite2,
          n: maLimite3
        })
      if (stor.coefC < 0) {
        maLimite1 = '-\\infty'
        maLimite2 = '0'
        quotient = Math.abs(stor.coefA) / Math.abs(stor.coefB)
        if (Math.abs(quotient - Math.round(quotient)) < Math.pow(10, -12)) {
          // quotient entier
          maLimite3 = stor.coefA / stor.coefB
        } else {
          // je fais attention au signe du quotient
          if (stor.coefA * stor.coefB > 0) {
            maLimite3 = '\\frac{' + Math.abs(stor.coefA) + '}{' + Math.abs(stor.coefB) + '}'
          } else {
            maLimite3 = '\\frac{-' + Math.abs(stor.coefA) + '}{' + Math.abs(stor.coefB) + '}'
          }
        }
      } else {
        maLimite1 = '+\\infty'
        maLimite2 = '+\\infty'
        maLimite3 = '0'
      }
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr2,
        {
          f: stor.nomFct,
          c: j3pMonome(1, 1, stor.coefC),
          l: maLimite1,
          m: maLimite2,
          n: maLimite3
        })
    } else if (num === 2) {
      // ax*exp(-ax)
      let textCorr1
      let textCorr2

      if (stor.coefA > 0) {
        textCorr1 = ds.textes.corr4
        textCorr2 = ds.textes.corr5
      } else {
        textCorr1 = ds.textes.corr5bis
        textCorr2 = ds.textes.corr4
      }
      j3pAffiche(stor.zoneExpli2, '', textCorr1,
        {
          f: stor.nomFct,
          x: '-\\infty',
          a: j3pMonome(1, 1, stor.coefA),
          b: j3pMonome(1, 1, -stor.coefA)
        })
      j3pAffiche(stor.zoneExpli3, '', textCorr2,
        {
          f: stor.nomFct,
          x: '+\\infty',
          a: j3pMonome(1, 1, stor.coefA),
          b: j3pMonome(1, 1, -stor.coefA)
        })
    } else if (num === 3) {
      // a(1-exp(-x/b))
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr6,
        {
          f: stor.nomFct,
          b: stor.coefB
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr7,
        {
          f: stor.nomFct,
          a: stor.coefA,
          b: stor.coefB
        })
    } else if ((num === 4) || (num === 5)) {
      // frac{a}{x}*exp(\\frac{a}{x})
      if (stor.coefA > 0) {
        j3pAffiche(stor.zoneExpli2, '', ds.textes.corr8,
          {
            f: stor.nomFct,
            a: stor.coefA
          })
      } else {
        j3pAffiche(stor.zoneExpli2, '', ds.textes.corr8bis,
          {
            f: stor.nomFct,
            a: stor.coefA
          })
      }
      j3pAffiche(stor.zoneExpli3, '', '\n' + ds.textes.corr9,
        {
          f: stor.nomFct,
          a: stor.coefA
        })
    }
    j3pAffiche(stor.zoneExpli4, '', '\n' + ds.textes.corr3)
    creationCourbe()
    j3pToggleFenetres('Courbe')
  }
  function getDonnees () {
    return {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 5,
      nbetapes: 1,
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,
      tab_choix: [1, 2, 3, 4, 5],
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"
      // Paramètres de la section, avec leurs valeurs par défaut.
      // aide_cours: true;
      textes: {
        titre_exo: 'Fonction exponentielle<br>Calcul de limites (1)',
        consigne1: 'Soit $£f$ la fonction définie sur $£i$ par :<br>$£f(x)=£g$.',
        consigne1bis: 'Soit $£f$ la fonction définie sur $£i\\opencurlybrace £j\\closecurlybrace$ par :<br>$£f(x)=£g$.',
        consigne2: 'Déterminer les limites suivantes.',
        consigne3: '$£l=$&1& <br>et $£m=$&2&',
        comment1: 'Ta réponse est imprécise !',
        comment2: 'Passe à la suite !',
        aide: 'Indication',
        FI0: 'Il n’y a pas de forme indéterminée.',
        FI1: 'Il y a une forme indéterminée en $-\\infty$, mais pas en $+\\infty$.',
        FI2: 'Il y a une forme indéterminée en $+\\infty$, mais pas en $-\\infty$.',
        FI3: 'Il y a une forme indéterminée en 0, mais pas en $+\\infty$.',
        aide1: 'Ne pas oublier des limites du cours : $\\limite{x}{+\\infty}{\\mathrm{e}^x}=+\\infty$ et $\\limite{x}{-\\infty}{\\mathrm{e}^x}=0$.',
        aide2: 'Ne pas oublier les limites du cours : $\\limite{x}{+\\infty}{\\frac{\\mathrm{e}^x}{x}}=+\\infty$ et $\\limite{x}{-\\infty}{x\\mathrm{e}^x}=0$.<br>On dit que $\\mathrm{e}^x$ "l’emporte" sur $x$ en l’infini.',
        // limite de a/(b+exp(cx)
        courbe: 'Courbe représentative de la fonction',
        corr0: 'Voici la justification qui serait attendue :',
        corr1: 'En $-\\infty$ : $\\limite{x}{-\\infty}{£c}=£l$ et $\\limite{X}{£l}{\\mathrm{e}^X}=£m$ d’où $\\limite{x}{-\\infty}{\\mathrm{e}^{£c}}=£m$.<br>Donc $\\limite{x}{-\\infty}{£f(x)}=£n$.',
        corr2: 'En $+\\infty$ : $\\limite{x}{+\\infty}{£c}=£l$ et $\\limite{X}{£l}{\\mathrm{e}^X}=£m$ d’où $\\limite{x}{+\\infty}{\\mathrm{e}^{£c}}=£m$.<br>Donc $\\limite{x}{+\\infty}{£f(x)}=£n$.',
        corr3: 'On peut aussi vérifier la réponse sur la courbe représentative de la fonction.',
        // limite de ax*exp(-ax)
        corr4: 'En $£x$ : $\\limite{x}{£x}{£b}=+\\infty$ et $\\limite{X}{+\\infty}{\\mathrm{e}^X}=+\\infty$ d’où $\\limite{x}{£x}{\\mathrm{e}^{£b}}=+\\infty$.<br>De plus $\\limite{x}{£x}{£a}=-\\infty$ donc $\\limite{x}{£x}{£f(x)}=-\\infty$.',
        corr5: 'En $+\\infty$ : $£f(x)=\\frac{£a}{\\mathrm{e}^{£a}}$. Or $\\limite{x}{£x}{£a}=+\\infty$ et $\\limite{X}{+\\infty}{\\frac{\\mathrm{e}^X}{X}}=+\\infty$<br>d’où $\\limite{x}{£x}{\\frac{\\mathrm{e}^{£a}}{£a}}=+\\infty$. Donc $\\limite{x}{£x}{£f(x)}=0$.',
        corr5bis: 'En $-\\infty$ : $\\limite{x}{£x}{£b}=-\\infty$ et $\\limite{X}{-\\infty}{X\\mathrm{e}^X}=0$.<br>Donc $\\limite{x}{-\\infty}{£f(x)}=0$.',
        // limite de a(1-exp(-x/b))
        corr6: 'En $-\\infty$ : $\\limite{x}{-\\infty}{-\\frac{x}{£b}}=+\\infty$ et $\\limite{X}{+\\infty}{\\mathrm{e}^X}=+\\infty$ d’où $\\limite{x}{-\\infty}{\\mathrm{e}^{-\\frac{x}{£b}}}=+\\infty$.<br>Donc $\\limite{x}{-\\infty}{£f(x)}=-\\infty$.',
        corr7: 'En $+\\infty$ : $\\limite{x}{+\\infty}{-\\frac{x}{£b}}=-\\infty$ et $\\limite{X}{-\\infty}{\\mathrm{e}^X}=0$ d’où $\\limite{x}{+\\infty}{\\mathrm{e}^{-\\frac{x}{£b}}}=0$.<br>Donc $\\limite{x}{+\\infty}{£f(x)}=£a$.',
        // limite de (a/x)*exp(a/x)
        corr8: 'En 0 : $\\limite{x}{0^+}{\\frac{£a}{x}}=+\\infty$ et $\\limite{X}{+\\infty}{\\mathrm{e}^X}=+\\infty$ d’où $\\limite{x}{0^+}{\\mathrm{e}^{\\frac{£a}{x}}}=+\\infty$.<br>Donc $\\limite{x}{0}{£f(x)}=+\\infty$.',
        corr8bis: 'En 0 : $\\limite{x}{0^+}{\\frac{£a}{x}}=-\\infty$ et $\\limite{X}{-\\infty}{X\\mathrm{e}^X}=0$ d’où $\\limite{x}{0^+}{\\mathrm{e}^{\\frac{£a}{x}}}=0$.<br>Donc $\\limite{x}{0}{£f(x)}=0$.',
        corr9: 'En $+\\infty$ : $\\limite{x}{+\\infty}{\\frac{£a}{x}}=0$ et $\\limite{X}{0}{\\mathrm{e}^X}=1$ d’où $\\limite{x}{+\\infty}{\\mathrm{e}^{\\frac{£a}{x}}}=1$.<br>Donc $\\limite{x}{+\\infty}{£f(x)}=0$.'
      },
      pe: 0
    }
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        ds.tab_choix = purge(ds.tab_choix)
        ds.nbrepetitions = ds.tab_choix.length
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.65 })
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      {
        let i
        let coefA
        let coefB
        let coefC
        let fDeX
        let domaineDef
        let maConsigne1
        let valInterdite
        let limiteRep1// réponse pour la limite en -\\infty
        let limiteRep2// réponse pour la limite en +\\infty
        if ((me.questionCourante - 1) % ds.nbrepetitions === 0) {
        // on construit un tableau où chaque élément contiendra la fonction, ce vers quoi va tendre x et la limite attendue
          stor.tab_quest_ordonne = j3pShuffle(ds.tab_choix)
        }
        me.logIfDebug('tab_quest_ordonne:' + stor.tab_quest_ordonne)
        const j = (me.questionCourante - 1) % ds.nbrepetitions
        stor.num_quest = stor.tab_quest_ordonne[j]
        switch (stor.num_quest) {
          case 1:
          // fonction a/(b+exp(c*x))
            do {
              coefA = j3pGetRandomInt(-3, 3)
            } while (Math.abs(coefA) < Math.pow(10, -12))
            do {
              coefB = j3pGetRandomInt(-3, 3)
            } while ((Math.abs(coefB) < Math.pow(10, -12)) || (Math.abs(Math.abs(coefB) - Math.abs(coefA)) < Math.pow(10, -12)))
            do {
              coefC = j3pGetRandomInt(-3, 3)
            } while ((Math.abs(coefC) < Math.pow(10, -12)) || (Math.abs(coefC - 1) < Math.pow(10, -12)))
            fDeX = '\\frac{' + coefA + '}{' + coefB + '+\\mathrm{e}^{' + j3pMonome(1, 1, coefC) + '}}'
            if (coefB > 0) {
              domaineDef = '\\R'
              maConsigne1 = ds.textes.consigne1
            } else {
              domaineDef = '\\R\\setminus'
              if (Math.abs(coefB + 1) < Math.pow(10, -12)) {
                valInterdite = '0'
              } else {
                if (Math.abs(coefC + 1) < Math.pow(10, -12)) {
                  valInterdite = '-\\mathrm{ln} ' + (-coefB)
                } else if (coefC > 0) {
                  valInterdite = '\\frac{\\mathrm{ln} ' + (-coefB) + '}{' + coefC + '}'
                } else {
                  valInterdite = '-\\frac{\\mathrm{ln} ' + (-coefB) + '}{' + (-coefC) + '}'
                }
              }
              maConsigne1 = ds.textes.consigne1bis
            }
            if (coefC < 0) {
              limiteRep1 = 0
            } else {
              limiteRep1 = coefA / coefB
            }
            if (coefC > 0) {
              limiteRep2 = 0
            } else {
              limiteRep2 = coefA / coefB
            }
            stor.fonction = coefA + '/(' + coefB + '+exp(' + coefC + '*x))'
            stor.coefC = coefC
            break

          case 2:
          // ax*exp(-ax)
            do {
              coefA = j3pGetRandomInt(-4, 4)
            } while (Math.abs(coefA) < 1 + Math.pow(10, -12))
            fDeX = j3pMonome(1, 1, coefA) + '\\mathrm{e}^{' + j3pMonome(1, 1, -coefA) + '}'
            maConsigne1 = ds.textes.consigne1
            domaineDef = '\\R'
            if (coefA < 0) {
              limiteRep1 = '0'
              limiteRep2 = '-\\infty'
            } else {
              limiteRep1 = '-\\infty'
              limiteRep2 = '0'
            }
            stor.fonction = coefA + '*x*exp(-(' + coefA + '*x))'
            break

          case 3:
          // a(1-exp(-x/b)) a et b positifs
            coefA = 5 * j3pGetRandomInt(1, 6)
            do {
              coefB = 5 * j3pGetRandomInt(1, 4)
            } while (Math.abs(Math.abs(coefB) - Math.abs(coefA)) < Math.pow(10, -12))
            fDeX = coefA + '\\left(1 -  \\mathrm{e}^{-\\frac{x}{' + coefB + '}}\\right)'
            maConsigne1 = ds.textes.consigne1
            domaineDef = '\\R'
            limiteRep1 = '-\\infty'
            limiteRep2 = String(coefA)
            stor.fonction = coefA + '*(1-exp(-x/' + coefB + '))'
            break

          default:
          // (a/x)*exp(a/x)
            if (stor.num_quest === 4) {
              coefA = j3pGetRandomInt(1, 3)
            } else {
              coefA = j3pGetRandomInt(-3, -1)
            }
            fDeX = '\\frac{' + coefA + '}{x}\\mathrm{e}^{\\frac{' + coefA + '}{x}}'
            maConsigne1 = ds.textes.consigne1
            domaineDef = ']0;+\\infty['
            if (coefA > 0) {
              limiteRep1 = '+\\infty'
            } else {
              limiteRep1 = '0'
            }
            limiteRep2 = '0'
            stor.fonction = '(' + coefA + '/x)*exp(' + coefA + '/x)'
            break
        }
        stor.coefA = coefA
        stor.coefB = coefB
        stor.limiteRep1 = limiteRep1
        stor.limiteRep2 = limiteRep2
        const tabNomFct = ['f', 'g', 'h']
        const nomFct = tabNomFct[j3pGetRandomInt(0, 2)]
        stor.nomFct = nomFct// pour être réutilisé dans la correction
        stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
        for (i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
        if (maConsigne1 === ds.textes.consigne1) {
          j3pAffiche(stor.zoneCons1, '', maConsigne1,
            {
              f: nomFct,
              g: fDeX,
              i: domaineDef
            })
        } else {
          j3pAffiche(stor.zoneCons1, '', maConsigne1,
            {
              f: nomFct,
              g: fDeX,
              i: domaineDef,
              j: valInterdite
            })
        }

        j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2)
        if ((stor.num_quest === 4) || (stor.num_quest === 5)) {
          stor.elts = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3,
            {
              l: '\\limite{x}{0}{' + nomFct + '(x)}',
              m: '\\limite{x}{+\\infty}{' + nomFct + '(x)}',
              inputmq1: { texte: '' },
              inputmq2: { texte: '' }
            })
        } else {
          stor.elts = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3,
            {
              l: '\\limite{x}{-\\infty}{' + nomFct + '(x)}',
              m: '\\limite{x}{+\\infty}{' + nomFct + '(x)}',
              inputmq1: { texte: '' },
              inputmq2: { texte: '' }
            })
        }
        j3pStyle(stor.zoneCons4, { paddingTop: '20px' })
        stor.laPalette = j3pAddElt(stor.zoneCons4, 'div')
        // palette MQ :
        j3pPaletteMathquill(stor.laPalette, stor.elts.inputmqList[0], {
          liste: ['racine', 'fraction', 'puissance', 'inf']
        })

        stor.elts.inputmqList.forEach((zoneInput) => zoneInput.addEventListener('focusin', ecoute.bind(null, zoneInput)))

        for (let i = 1; i <= 2; i++) mqRestriction(stor.elts.inputmqList[i - 1], '\\d,.+-/', { commandes: ['racine', 'fraction', 'puissance', 'inf'] })
        if (stor.num_quest === 1) {
          stor.elts.inputmqList[0].typeReponse = ['nombre', 'exact']
          stor.elts.inputmqList[1].typeReponse = ['nombre', 'exact']
        } else {
          stor.elts.inputmqList[0].typeReponse = ['texte']
          stor.elts.inputmqList[1].typeReponse = ['texte']
        }
        stor.elts.inputmqList[0].reponse = [limiteRep1]
        stor.elts.inputmqList[1].reponse = [limiteRep2]
        j3pFocus(stor.elts.inputmqList[0])

        stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px' } })

        me.logIfDebug('reponse1 : ' + stor.elts.inputmqList[0].reponse[0] + '   et réponse2 : ' + stor.elts.inputmqList[1].reponse[0])

        stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
        stor.zoneBtn = j3pAddElt(stor.zoneD, 'div')
        stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '', { style: { paddingTop: '10px' } })
        aide(stor.tab_quest_ordonne[j])

        const mesZonesSaisie = stor.elts.inputmqList.map(elt => elt.id)
        stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
      }
      // Obligatoire
      me.finEnonce()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
          // Une réponse a été saisie

          // Bonne réponse
          if (reponse.bonneReponse && (!me.isElapsed)) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(stor.num_quest, true)
            j3pEmpty(stor.laPalette)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
              me.typederreurs[10]++
              me.cacheBoutonValider()
              afficheCorrection(stor.num_quest, false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
              // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                j3pEmpty(stor.laPalette)
                afficheCorrection(stor.num_quest, false)
                me._stopTimer()
                me.cacheBoutonValider()
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      j3pDetruitFenetres('Boutons')
      j3pDetruitFenetres('Aide')
      me.finNavigation()
      break // case "navigation":
  }
}
