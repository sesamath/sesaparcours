import { j3pAddElt, j3pAjouteBouton, j3pShuffle, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pMonome, j3pPaletteMathquill, j3pStyle, j3pGetNewId } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    avril 2014
    cette section permet de se tester sur des limites avec la fonctin ln
    il y a un cas de forme indéterminée
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['tab_choix', [1, 2, 3, 4, 5], 'array', 'tableau contenant la liste des limites demandées (on peut en mettre moins voire en doubler certains) :<br>1 pour ax/ln(x),<br>2 pour (a+bln(x))/x,<br>3 pour ax+bln(x),<br>4 pour (a+bln(x))/x^2,<br>5 pour ax^2+b+cln(x)']

  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function purge (tableauChoix) {
    // cette fonction va me permettre de prévenir un éventuel mauvais remplissage du tableau sur le choix des limites
    const tableauPurge = []
    for (let j = 0; j < tableauChoix.length; j++) {
      if (!isNaN(Number(tableauChoix[j]))) {
        if ((tableauChoix[j] === 1) || (tableauChoix[j] === 2) || (tableauChoix[j] === 3) || (tableauChoix[j] === 4) || (tableauChoix[j] === 5)) {
          tableauPurge.push(tableauChoix[j])
        }
      }
    }
    // à cet instant si tableau_epure est vide, je lui mets un élément : 1;
    if (tableauPurge.length === 0) {
      tableauPurge.push(1)
    }
    return tableauPurge
  }
  function aide (num) {
    // num correspond au numéro de la limite demandée (dans le tableau tab_choix)
    // 1 pour ax/ln(x), 2 pour (a+bln(x))/x, 3 pour ax+bln(x), 4 pour (a+bln(x))/x^2, 5 pour ax^2+b+cln(x)
    const titreAide = (num === 1) ? ds.textes.aide : ds.textes.aidebis
    stor.fenetreAide = j3pGetNewId('fenetreAide')
    stor.fenetreCourbe = j3pGetNewId('fenetreCourbe')
    me.fenetresjq = [{ name: 'Aide', title: titreAide, width: Number(me.zonesElts.MG.offsetWidth), left: 50, top: 50, id: stor.fenetreAide },
      { name: 'Courbe', title: ds.textes.courbe, top: me.zonesElts.HD.offsetHeight + 70, left: me.zonesElts.MG.offsetWidth + 10, width: 280, id: stor.fenetreCourbe }]
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneBtn, 'Aide', 'MepBoutons', titreAide, j3pToggleFenetres.bind(null, 'Aide'))
    j3pAfficheCroixFenetres('Aide')
    const coursDebut = j3pAddElt(stor.fenetreAide, 'div')
    let monAide
    const monFI = ds.textes.FI2
    if (num === 1) {
      monAide = ds.textes.aide1
    } else if (num === 2) {
      monAide = ds.textes.aide2
    } else if (num === 3) {
      monAide = ds.textes.aide3
    } else if (num === 4) {
      monAide = ds.textes.aide4
    } else if (num === 5) {
      monAide = ds.textes.aide3
    }
    j3pAffiche(coursDebut, '', monFI + '\n' + monAide)
  }
  function creationCourbe () {
    // création de la courbe :
    stor.courbeCours = j3pAddElt(stor.fenetreCourbe, 'div')
    let pasGradx, repere
    stor.idRepere = j3pGetNewId('unrepere')
    if (stor.numQuest === 1) {
      const posyO = (stor.coefA > 0) ? 200 : 100
      repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: false,
        visible: true,
        trame: true,
        fixe: true,
        larg: 240,
        haut: 400,

        pasdunegraduationX: 1,
        pixelspargraduationX: 20,
        pasdunegraduationY: 1,
        pixelspargraduationY: 20,
        xO: 20,
        yO: posyO,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: 0.01, par3: 0.99, par4: 100, style: { couleur: '#F00', epaisseur: 2 } },
          { type: 'courbe', nom: 'c2', par1: stor.fonction, par2: 1.01, par3: 15, par4: 100, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else if (stor.numQuest === 3) {
      pasGradx = 40
      if (stor.coefA * stor.coefB < 0) {
        pasGradx = 20
      }
      repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 240,
        haut: 300,

        pasdunegraduationX: 1,
        pixelspargraduationX: pasGradx,
        pasdunegraduationY: 1,
        pixelspargraduationY: 20,
        xO: 20,
        yO: 150,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: 0.0001, par3: 15, par4: 200, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else {
      let debutX = 0.02
      if (stor.numQuest === 4) {
        pasGradx = 40
        debutX = 0.1
      } else if (stor.numQuest === 5) {
        pasGradx = 50
      } else {
        pasGradx = 20
      }
      repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 240,
        haut: 300,

        pasdunegraduationX: 1,
        pixelspargraduationX: pasGradx,
        pasdunegraduationY: 1,
        pixelspargraduationY: 20,
        xO: 20,
        yO: 150,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: debutX, par3: 15, par4: 200, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    }
    repere.construit()
    j3pElement(stor.idRepere).style.position = 'relative'
  }
  function ecoute (eltInput) {
    if (eltInput.className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, eltInput, { liste: ['racine', 'fraction', 'puissance', 'inf'] })
    }
  }
  function afficheCorrection (num, bonneRep) {
    j3pDetruit(stor.zoneBtn)
    let coefB
    let coefB2
    stor.explications.style.color = (bonneRep) ? me.styles.cbien : me.styles.petit.correction.color
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(stor.explications, 'div')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr0)
    if (num === 1) {
      // \frac{ax}{ln x}
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr1,
        {
          f: stor.nomFct,
          a: j3pMonome(1, 1, stor.coefA)
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr2,
        {
          f: stor.nomFct,
          l: stor.limiteRep2
        })
    } else if (num === 2) {
      // frac{a+bln x}{x}
      if (Math.abs(stor.coefB - 1) < Math.pow(10, -12)) {
        coefB = ' + '
        coefB2 = ' + '
      } else if (Math.abs(stor.coefB + 1) < Math.pow(10, -12)) {
        coefB = ' - '
        coefB2 = ' - '
      } else if (stor.coefB < 0) {
        coefB = ' - ' + String(Math.abs(stor.coefB)) + '\\times '
        coefB2 = ' - ' + String(Math.abs(stor.coefB))
      } else {
        coefB = ' + ' + String(Math.abs(stor.coefB)) + '\\times '
        coefB2 = ' + ' + String(Math.abs(stor.coefB))
      }
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr4,
        {
          f: stor.nomFct,
          a: stor.coefA,
          b: coefB2,
          l: stor.limiteRep1
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr5,
        {
          f: stor.nomFct,
          a: stor.coefA,
          b: coefB
        })
    } else if (num === 3) {
      // ax+b lnx
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr6,
        {
          f: stor.nomFct,
          a: j3pMonome(1, 1, stor.coefA),
          l: stor.limiteRep1
        })
      if (Math.abs(stor.coefB - 1) < Math.pow(10, -12)) {
        coefB = ' + '
      } else if (Math.abs(stor.coefB + 1) < Math.pow(10, -12)) {
        coefB = ' - '
      } else if (stor.coefB < 0) {
        coefB = ' - ' + String(Math.abs(stor.coefB))
      } else {
        coefB = ' + ' + String(Math.abs(stor.coefB))
      }
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr7,
        {
          f: stor.nomFct,
          a: stor.coefA,
          b: coefB,
          l: stor.limiteRep2
        })
    } else if (num === 4) {
      // frac{a+bln x}{x^2}
      if (Math.abs(stor.coefB - 1) < Math.pow(10, -12)) {
        coefB = ' + '
        coefB2 = ' + '
      } else if (Math.abs(stor.coefB + 1) < Math.pow(10, -12)) {
        coefB = ' - '
        coefB2 = ' - '
      } else if (stor.coefB < 0) {
        coefB = ' - ' + String(Math.abs(stor.coefB)) + '\\times '
        coefB2 = ' - ' + String(Math.abs(stor.coefB))
      } else {
        coefB = ' + ' + String(Math.abs(stor.coefB)) + '\\times '
        coefB2 = ' + ' + String(Math.abs(stor.coefB))
      }
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr8,
        {
          f: stor.nomFct,
          a: stor.coefA,
          b: coefB2,
          l: stor.limiteRep1
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr9,
        {
          f: stor.nomFct,
          a: stor.coefA,
          b: coefB
        })
    } else if (num === 5) {
      // ax^2+b+cln(x)
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr10,
        {
          f: stor.nomFct,
          a: j3pMonome(1, 2, stor.coefA) + j3pMonome(2, 0, stor.coefB),
          b: stor.coefB,
          l: stor.limiteRep1
        })
      let signeB
      if (stor.coefB < 0) {
        signeB = ' - '
      } else {
        signeB = ' + '
      }
      let coefC
      if (Math.abs(stor.coefC - 1) < Math.pow(10, -12)) {
        coefC = ' + '
      } else if (Math.abs(stor.coefC + 1) < Math.pow(10, -12)) {
        coefC = ' - '
      } else if (stor.coefC < 0) {
        coefC = ' - ' + String(Math.abs(stor.coefC))
      } else {
        coefC = ' + ' + String(Math.abs(stor.coefC))
      }
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr11,
        {
          f: stor.nomFct,
          a: j3pMonome(1, 1, stor.coefA) + signeB,
          b: Math.abs(stor.coefB),
          c: coefC,
          l: stor.limiteRep2
        })
    }
    j3pAffiche(stor.zoneExpli4, '', ds.textes.corr3)
    // creationCourbe()
    j3pToggleFenetres('Courbe')
  }
  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 5,
      nbetapes: 1,
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      tab_choix: [1, 2, 3, 4, 5],
      structure: 'presentation1bis', //  || "presentation2" || "presentation3"  || "presentation1bis"
      // Paramètres de la section, avec leurs valeurs par défaut.
      // aide_cours: true;
      textes: {
        titre_exo: 'Fonction logarithme népérien<br>Calcul de limites (1)',
        consigne1: 'Soit $£f$ la fonction définie sur $£i$ par :<br>$£f(x)=£g$.',
        consigne2: 'Déterminer les limites suivantes.',
        consigne3: '$£l=$&1& <br>et $£m=$&2&',
        comment1: 'Ta réponse est imprécise !',
        comment2: 'Passe à la suite !',
        aide: 'Indication',
        aidebis: 'Rappel de cours et méthode',
        FI1: 'Il y a une forme indéterminée en 0, mais pas en $+\\infty$.',
        FI2: 'Il y a une forme indéterminée en $+\\infty$, mais pas en $0$.',
        FI3: 'Il y a une forme indéterminée en 0 et en $+\\infty$.',
        aide1: 'Ne pas oublier l’une des limites du cours : $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$.<br>On dit que $x$ "l’emporte" sur $\\ln x$ en $+\\infty$.',
        aide2: 'Ne pas oublier l’une des limites du cours : $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$.<br>On dit que $x$ "l’emporte" sur $\\ln x$ en $+\\infty$.<br>Dans cet exemple, la fonction peut être écrite comme la somme de deux quotients.',
        aide3: 'Ne pas oublier l’une des limites du cours : $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$.<br>On dit que $x$ "l’emporte" sur $\\ln x$ en $+\\infty$.<br>Dans cet exemple, on pourra penser à une factorisation.',
        aide4: 'Ne pas oublier l’une des limites du cours : $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$.<br>On dit que $x$ "l’emporte" sur $\\ln x$ en $+\\infty$.<br>Dans cet exemple, on fera apparaître la somme de deux quotient puis l’expression $\\frac{\\ln x}{x}$.',
        // limite de ax/lnx
        courbe: 'Courbe représentative de la fonction',
        corr0: 'Voici la justification qui serait attendue :',
        corr1: 'En 0 : $£f(x)=£a\\times \\frac{1}{\\ln x}$ où $\\limite{x}{0}{£a}=0$.<br>De plus $\\limite{x}{0}{\\ln x}=-\\infty$ d’où $\\limite{x}{0}{\\frac{1}{\\ln x}}=0$. Ainsi $\\limite{x}{0}{£f(x)}=0$.',
        corr2: 'En $+\\infty$, $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0^+$ d’où $\\limite{x}{+\\infty}{\\frac{x}{\\ln x}}=+\\infty$.<br>Donc $\\limite{x}{+\\infty}{£f(x)}=£l$.',
        corr3: 'On peut aussi vérifier la réponse sur la courbe représentative de la fonction.',
        // limite de (a+b lnx)/x
        corr4: 'En 0 : $£f(x)=(£a£b\\ln x)\\times \\frac{1}{x}$.<br>Or $\\limite{x}{0}{\\ln x}=-\\infty$ d’où $\\limite{x}{0}{£a£b\\ln x}=£l$.<br>De plus $\\limite{x}{0^+}{\\frac{1}{x}}=+\\infty$. Donc $\\limite{x}{0}{£f(x)}=£l$.',
        corr5: 'En $+\\infty$, $£f(x)=\\frac{£a}{x}£b\\frac{\\ln x}{x}$. Or $\\limite{x}{+\\infty}{\\frac{£a}{x}}=0$ et $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$.<br>Donc $\\limite{x}{+\\infty}{£f(x)}=0$.',
        // limite de ax+b lnx
        corr6: 'En 0 : $\\limite{x}{0}{£a}=0$ et $\\limite{x}{0}{\\ln x}=-\\infty$.<br>Donc $\\limite{x}{0}{£f(x)}=£l$.',
        corr7: 'En $+\\infty$, $£f(x)=x\\left(£a£b\\frac{\\ln x}{x}\\right)$.<br>Or $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$ d’où $\\limite{x}{+\\infty}{£a£b\\frac{\\ln x}{x}}=£a$.<br>Donc $\\limite{x}{+\\infty}{£f(x)}=£l$.',
        // limite de (a+bln(x))/x^2
        corr8: 'En 0 : $£f(x)=(£a£b\\ln x)\\times \\frac{1}{x^2}$.<br>Or $\\limite{x}{0}{\\ln x}=-\\infty$ d’où $\\limite{x}{0}{£a£b\\ln x}=£l$.<br>De plus $\\limite{x}{0}{\\frac{1}{x^2}}=+\\infty$. Donc $\\limite{x}{0}{£f(x)}=£l$.',
        corr9: 'En $+\\infty$, $£f(x)=\\frac{£a}{x^2}£b\\frac{\\ln x}{x}\\times\\frac{1}{x}$.<br>Or $\\limite{x}{+\\infty}{\\frac{£a}{x^2}}=0$ ; $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$ et $\\limite{x}{+\\infty}{\\frac{1}{x}}=0$.<br>Donc $\\limite{x}{+\\infty}{£f(x)}=0$.',
        // limite de ax^2+b+cln(x)
        corr10: 'En 0 : $\\limite{x}{0}{£a}=£b$ et $\\limite{x}{0}{\\ln x}=-\\infty$.<br>Donc $\\limite{x}{0}{£f(x)}=£l$.',
        corr11: 'En $+\\infty$, $£f(x)=x\\left(£a\\frac{£b}{x}£c\\frac{\\ln x}{x}\\right)$.<br>Or $\\limite{x}{+\\infty}{£a\\frac{£b}{x}}=£l$ et $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$ d’où $\\limite{x}{+\\infty}{£a\\frac{£b}{x}£c\\frac{\\ln x}{x}}=£l$.<br>Donc $\\limite{x}{+\\infty}{£f(x)}=£l$.'
      },
      pe: 0
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    ds.tab_choix = purge(ds.tab_choix)
    ds.nbrepetitions = ds.tab_choix.length
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.65 })
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      {
        let i
        let coefA
        let coefB
        let coefC
        let fDeX
        let domaineDef
        let limiteRep1// réponse pour la limite en 0
        let limiteRep2// réponse pour la limite en +\\infty
        if ((me.questionCourante - 1) % ds.nbrepetitions === 0) {
        // on construit un tableau où chaque élément contiendra la fonction, ce vers quoi va tendre x et la limite attendue
          stor.tab_quest_ordonne = j3pShuffle(ds.tab_choix)
        }
        me.logIfDebug('tab_quest_ordonne:' + stor.tab_quest_ordonne)
        const j = (me.questionCourante - 1) % ds.nbrepetitions
        stor.numQuest = stor.tab_quest_ordonne[j]
        switch (stor.numQuest) {
          case 1:
          // fonction ax/ln x
            do {
              coefA = j3pGetRandomInt(-3, 3)
            } while (Math.abs(coefA) < Math.pow(10, -12))
            fDeX = '\\frac{' + j3pMonome(1, 1, coefA) + '}{\\ln x}'
            domaineDef = ']0;1[\\cup]1;+\\infty['
            limiteRep1 = '0'
            if (coefA > 0) {
              limiteRep2 = '+\\infty'
            } else {
              limiteRep2 = '-\\infty'
            }
            stor.fonction = coefA + '*x/ln(x)'
            break

          case 2:
          // (a+bln(x))/x
            do {
              coefA = j3pGetRandomInt(-3, 3)
            } while (Math.abs(coefA) < Math.pow(10, -12))
            do {
              coefB = j3pGetRandomInt(-3, 3)
            } while ((Math.abs(coefB) < Math.pow(10, -12)) || (Math.abs(Math.abs(coefB) - Math.abs(coefA)) < Math.pow(10, -12)))
            if (Math.abs(coefB - 1) < Math.pow(10, -12)) {
              fDeX = '\\frac{' + String(coefA) + ' +  \\ln x}{x}'
            } else if (Math.abs(coefB + 1) < Math.pow(10, -12)) {
              fDeX = '\\frac{' + String(coefA) + ' - \\ln x}{x}'
            } else if (coefB < 0) {
              fDeX = '\\frac{' + String(coefA) + ' - ' + String(Math.abs(coefB)) + '\\ln x}{x}'
            } else {
              fDeX = '\\frac{' + String(coefA) + ' + ' + String(Math.abs(coefB)) + '\\ln x}{x}'
            }
            domaineDef = ']0;+\\infty['
            if (coefB > 0) {
              limiteRep1 = '-\\infty'
            } else {
              limiteRep1 = '+\\infty'
            }
            limiteRep2 = '0'
            stor.fonction = '(' + coefA + '+' + coefB + '*ln(x))/x'
            break

          case 3:
          // ax+bln(x)
            do {
              coefA = j3pGetRandomInt(-3, 3)
            } while (Math.abs(coefA) < Math.pow(10, -12))
            do {
              coefB = j3pGetRandomInt(-3, 3)
            } while ((Math.abs(coefB) < Math.pow(10, -12)) || (Math.abs(Math.abs(coefB) - Math.abs(coefA)) < Math.pow(10, -12)) || (coefA * coefB > 0))
            if (Math.abs(coefB - 1) < Math.pow(10, -12)) {
              fDeX = j3pMonome(1, 1, coefA) + ' +  \\ln x'
            } else if (Math.abs(coefB + 1) < Math.pow(10, -12)) {
              fDeX = j3pMonome(1, 1, coefA) + ' - \\ln x'
            } else if (coefB < 0) {
              fDeX = j3pMonome(1, 1, coefA) + ' - ' + String(Math.abs(coefB)) + '\\ln x'
            } else {
              fDeX = j3pMonome(1, 1, coefA) + ' + ' + String(Math.abs(coefB)) + '\\ln x'
            }
            domaineDef = ']0;+\\infty['
            if (coefB > 0) {
              limiteRep1 = '-\\infty'
            } else {
              limiteRep1 = '+\\infty'
            }
            if (coefA > 0) {
              limiteRep2 = '+\\infty'
            } else {
              limiteRep2 = '-\\infty'
            }
            stor.fonction = coefA + '*x+' + coefB + '*ln(x)'
            break

          case 4:
          // (a+bln(x))/x^2
            do {
              coefA = j3pGetRandomInt(-3, 3)
            } while (Math.abs(coefA) < Math.pow(10, -12))
            do {
              coefB = j3pGetRandomInt(-3, 3)
            } while ((Math.abs(coefB) < Math.pow(10, -12)) || (Math.abs(Math.abs(coefB) - Math.abs(coefA)) < Math.pow(10, -12)))
            if (Math.abs(coefB - 1) < Math.pow(10, -12)) {
              fDeX = '\\frac{' + String(coefA) + ' +  \\ln x}{x^2}'
            } else if (Math.abs(coefB + 1) < Math.pow(10, -12)) {
              fDeX = '\\frac{' + String(coefA) + ' - \\ln x}{x^2}'
            } else if (coefB < 0) {
              fDeX = '\\frac{' + String(coefA) + ' - ' + String(Math.abs(coefB)) + '\\ln x}{x^2}'
            } else {
              fDeX = '\\frac{' + String(coefA) + ' + ' + String(Math.abs(coefB)) + '\\ln x}{x^2}'
            }
            domaineDef = ']0;+\\infty['
            if (coefB > 0) {
              limiteRep1 = '-\\infty'
            } else {
              limiteRep1 = '+\\infty'
            }
            limiteRep2 = '0'
            stor.fonction = '(' + coefA + '+' + coefB + '*ln(x))/(x^2)'
            break

          default:
          // ax^2+b+cln(x)
            do {
              coefA = j3pGetRandomInt(-4, 4)
            } while (Math.abs(coefA) < Math.pow(10, -12))
            do {
              coefB = j3pGetRandomInt(-4, 4)
            } while ((Math.abs(coefB) < Math.pow(10, -12)) || (Math.abs(Math.abs(coefB) - Math.abs(coefA)) < Math.pow(10, -12)))
            do {
              coefC = j3pGetRandomInt(-4, 4)
            } while ((Math.abs(coefC) < Math.pow(10, -12)) || (Math.abs(Math.abs(coefC) - Math.abs(coefA)) < Math.pow(10, -12)) || (Math.abs(Math.abs(coefC) - Math.abs(coefB)) < Math.pow(10, -12)))
            if (Math.abs(coefC - 1) < Math.pow(10, -12)) {
              fDeX = j3pMonome(1, 2, coefA) + j3pMonome(2, 0, coefB) + ' +  \\ln x'
            } else if (Math.abs(coefC + 1) < Math.pow(10, -12)) {
              fDeX = j3pMonome(1, 2, coefA) + j3pMonome(2, 0, coefB) + ' - \\ln x'
            } else if (coefC < 0) {
              fDeX = j3pMonome(1, 2, coefA) + j3pMonome(2, 0, coefB) + ' - ' + String(Math.abs(coefC)) + '\\ln x'
            } else {
              fDeX = j3pMonome(1, 2, coefA) + j3pMonome(2, 0, coefB) + ' + ' + String(Math.abs(coefC)) + '\\ln x'
            }
            domaineDef = ']0;+\\infty['
            if (coefC > 0) {
              limiteRep1 = '-\\infty'
            } else {
              limiteRep1 = '+\\infty'
            }
            if (coefA > 0) {
              limiteRep2 = '+\\infty'
            } else {
              limiteRep2 = '-\\infty'
            }
            stor.coefC = coefC
            stor.fonction = coefA + '*x^2+' + coefB + '+' + coefC + '*ln(x)'
            break
        }
        stor.coefA = coefA
        stor.coefB = coefB
        stor.limiteRep1 = limiteRep1
        stor.limiteRep2 = limiteRep2
        const tabNomFct = ['f', 'g', 'h']
        const nomFct = tabNomFct[j3pGetRandomInt(0, 2)]
        stor.nomFct = nomFct// pour être réutilisé dans la correction
        stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
        for (i = 1; i <= 5; i++) stor['zoneCorr' + i] = j3pAddElt(stor.conteneur, 'div')
        j3pAffiche(stor.zoneCorr1, '', ds.textes.consigne1,
          {
            f: nomFct,
            g: fDeX,
            i: domaineDef
          })

        j3pAffiche(stor.zoneCorr2, '', ds.textes.consigne2)
        stor.elts = j3pAffiche(stor.zoneCorr3, '', ds.textes.consigne3,
          {
            l: '\\limite{x}{0}{' + nomFct + '(x)}',
            m: '\\limite{x}{+\\infty}{' + nomFct + '(x)}',
            inputmq1: { texte: '' },
            inputmq2: { texte: '' }
          })
        for (let i = 0; i < 2; i++) mqRestriction(stor.elts.inputmqList[i], '\\d+-,./^', { commandes: ['racine', 'fraction', 'puissance', 'inf'] })
        j3pStyle(stor.zoneCorr5, { paddingTop: '20px' })
        stor.laPalette = j3pAddElt(stor.zoneCorr5, 'div')
        // palette MQ :
        j3pPaletteMathquill(stor.laPalette, stor.elts.inputmqList[0], {
          liste: ['racine', 'fraction', 'puissance', 'inf']
        })
        stor.elts.inputmqList.forEach((zoneInput) => zoneInput.addEventListener('focusin', ecoute.bind(null, zoneInput)))
        stor.elts.inputmqList[0].typeReponse = ['texte']
        stor.elts.inputmqList[0].reponse = [limiteRep1]
        stor.elts.inputmqList[1].typeReponse = ['texte']
        stor.elts.inputmqList[1].reponse = [limiteRep2]

        stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
        stor.zoneBtn = j3pAddElt(stor.zoneD, 'div')
        stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '', { style: { paddingTop: '10px' } })
        const mesZonesSaisie = stor.elts.inputmqList.map(elt => elt.id)
        stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
        aide(stor.tab_quest_ordonne[j])
        j3pFocus(stor.elts.inputmqList[0])
        stor.explications = j3pAddElt(stor.conteneur, 'div', { style: { paddingTop: '20px' } })
      }
      creationCourbe()
      me.finEnonce()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie

        const reponse = stor.fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse && (!me.isElapsed)) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(stor.numQuest, true)
            j3pEmpty(stor.laPalette)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
              me.typederreurs[10]++
              me.cacheBoutonValider()
              /*
                       *   RECOPIER LA CORRECTION ICI !
                       */
              afficheCorrection(stor.numQuest, false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (((stor.elts.inputmqList[0].reponse[0].includes('infty')) && (stor.fctsValid.zones.reponseSaisie[0] === '\\infty')) || ((stor.elts.inputmqList[1].reponse[0].includes('infty')) && (stor.fctsValid.zones.reponseSaisie[1] === '\\infty'))) stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              // l’infini est présent dans la réponse'
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                j3pEmpty(stor.laPalette)
                afficheCorrection(stor.numQuest, false)
                me.cacheBoutonValider()
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      j3pDetruitFenetres('Boutons')
      j3pDetruitFenetres('Aide')
      me.finNavigation()
      break // case "navigation":
  }
}
