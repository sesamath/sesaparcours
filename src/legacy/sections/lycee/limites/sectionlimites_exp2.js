import { j3pShuffle, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pMonome, j3pPaletteMathquill, j3pDetruit, j3pStyle, j3pAddElt, j3pAjouteBouton, j3pGetNewId } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitFenetres, j3pAfficheCroixFenetres } from 'src/legacy/core/functionsJqDialog'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    avril 2014
    cette section permet de se tester sur des limites avec la fonction exp
    Nous sommes en présence d’une forme indéterminée
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 6, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['tab_choix', [1, 2, 3, 4, 5, 6], 'array', 'tableau contenant la liste des limites demandées (on peut en mettre moins voire doubler certains) :<br>1 pour ax*exp(x-b),<br>2 pour exp(x)-ax+b,<br>3 pour (x+a)exp(bx),<br>4 pour (exp(x)+a)/(exp(x)-x) avec a>0,<br>5 pour exp(x)+ax*exp(x)+b,<br>6 pour a*exp(x)/(exp(x)+b)']

  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function purge (tableauChoix) {
    // cette fonction va me permettre de prévenir un éventuel mauvais remplissage du tableau sur le choix des limites
    const tableauPurge = []
    for (let j = 0; j < tableauChoix.length; j++) {
      if (!isNaN(Number(tableauChoix[j]))) {
        if ((tableauChoix[j] === 1) || (tableauChoix[j] === 2) || (tableauChoix[j] === 3) || (tableauChoix[j] === 4) || (tableauChoix[j] === 5) || (tableauChoix[j] === 6)) {
          tableauPurge.push(tableauChoix[j])
        }
      }
    }
    // à cet instant si tableau_epure est vide, je lui mets un élément : 1;
    if (tableauPurge.length === 0) {
      tableauPurge.push(1)
    }
    return tableauPurge
  }
  function aide (num) {
    // num correspond au numéro de la limite demandée (dans le tableau tab_choix)
    const titreAide = ds.textes.aide
    me.fenetresjq = [{ name: 'Aide', title: titreAide, width: Number(me.zonesElts.MG.offsetWidth), left: 50, top: 50, id: 'fenetreAide' },
      { name: 'Courbe', title: ds.textes.courbe, top: me.zonesElts.HD.offsetHeight + 70, left: me.zonesElts.MG.offsetWidth + 10, width: 280, id: 'fenetreCourbe' }]
    j3pCreeFenetres(me)
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneBtn, 'Aide', 'MepBoutons', titreAide, j3pToggleFenetres.bind(null, 'Aide'))
    j3pAfficheCroixFenetres('Aide')
    stor.coursDebut = j3pAddElt('fenetreAide', 'div')
    let monFI
    if ((num === 1) || (num === 3)) {
      monFI = ds.textes.FI1
    } else if (num === 5) {
      monFI = ds.textes.FI3
    } else {
      monFI = ds.textes.FI2
    }
    const monAide = ds.textes.aide2
    j3pAffiche(stor.coursDebut, '', monFI + '\n' + monAide)
  }
  function creationCourbe () {
    // création de la courbe :
    stor.courbeCours = j3pAddElt('fenetreCourbe', 'div')
    if (stor.num_quest === 1) {
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 240,
        haut: 350,

        pasdunegraduationX: 1,
        pixelspargraduationX: 30,
        pasdunegraduationY: 1,
        pixelspargraduationY: 30,
        xO: 150,
        yO: 175,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: -10, par3: 3, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else if (stor.num_quest === 2) {
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 240,
        haut: 300,

        pasdunegraduationX: 1,
        pixelspargraduationX: 30,
        pasdunegraduationY: 2,
        pixelspargraduationY: 30,
        xO: 120,
        yO: 200,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: -7, par3: 7, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else if (stor.num_quest === 3) {
      let posyO = 150
      let grady = 1
      if (stor.coefA < -1) {
        grady = 5
        posyO = 100
      }
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 260,
        haut: 300,

        pasdunegraduationX: 1,
        pixelspargraduationX: 40,
        pasdunegraduationY: grady,
        pixelspargraduationY: 40,
        xO: 130,
        yO: posyO,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: -7, par3: 3, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else if (stor.num_quest === 4) {
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 260,
        haut: 300,

        pasdunegraduationX: 2,
        pixelspargraduationX: 30,
        pasdunegraduationY: 1,
        pixelspargraduationY: 30,
        xO: 130,
        yO: 200,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: -10, par3: 10, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else if (stor.num_quest === 5) {
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 260,
        haut: 300,

        pasdunegraduationX: 1,
        pixelspargraduationX: 20,
        pasdunegraduationY: 1,
        pixelspargraduationY: 30,
        xO: 130,
        yO: 150,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: -10, par3: 10, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else if (stor.num_quest === 6) {
      if (stor.coefB < 0) {
        const absInit = Math.log(-stor.coefB) + 0.02
        const absFin = Math.log(-stor.coefB) - 0.02
        me.repere = new Repere({
          idConteneur: stor.courbeCours,
          idDivRepere: stor.idRepere,
          aimantage: true,
          visible: true,
          trame: true,
          fixe: true,
          larg: 260,
          haut: 300,

          pasdunegraduationX: 1,
          pixelspargraduationX: 20,
          pasdunegraduationY: 1,
          pixelspargraduationY: 20,
          xO: 130,
          yO: 150,
          debuty: 0,
          negatifs: true,
          objets: [
            { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: -10, par3: absFin, par4: 300, style: { couleur: '#F00', epaisseur: 2 } },
            { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: absInit, par3: 10, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
          ]
        })
      } else {
        me.repere = new Repere({
          idConteneur: stor.courbeCours,
          idDivRepere: stor.idRepere,
          aimantage: true,
          visible: true,
          trame: true,
          fixe: true,
          larg: 260,
          haut: 300,

          pasdunegraduationX: 1,
          pixelspargraduationX: 20,
          pasdunegraduationY: 1,
          pixelspargraduationY: 40,
          xO: 130,
          yO: 150,
          debuty: 0,
          negatifs: true,
          objets: [
            { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: -10, par3: 10, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
          ]
        })
      }
    }
    me.repere.construit()
    j3pElement(stor.idRepere).style.position = 'relative'
  }
  function ecoute (eltInput) {
    if (eltInput.className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, eltInput, { liste: ['racine', 'fraction', 'puissance', 'inf'] })
    }
  }
  function afficheCorrection (num, bonneRep) {
    let maCorr1, monCoefA
    j3pDetruit(stor.zoneBtn)
    j3pStyle(stor.zoneExpli, { color: (bonneRep) ? me.styles.cbien : me.styles.petit.correction.color })
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(stor.zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr0)
    if (num === 1) {
      // ax*exp(x-b)
      maCorr1 = ds.textes.corr1
      j3pAffiche(stor.zoneExpli2, '', maCorr1,
        {
          f: stor.nomFct,
          a: j3pMonome(1, 1, stor.coefA),
          b: -stor.coefB
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr2,
        {
          f: stor.nomFct,
          a: j3pMonome(1, 1, stor.coefA),
          l: stor.limiteRep2
        })
    } else if (num === 2) {
      // exp(x)-ax+b
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr4,
        {
          f: stor.nomFct,
          a: j3pMonome(1, 1, -stor.coefA),
          l: stor.limiteRep1
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr5,
        {
          f: stor.nomFct,
          a: j3pMonome(1, 0, -stor.coefA),
          b: stor.coefB
        })
    } else if (num === 3) {
      // (x+a)exp(bx)
      monCoefA = stor.coefA
      if (Math.abs(stor.coefA - 1) < Math.pow(10, -12)) {
        monCoefA = '+'
      } else if (Math.abs(stor.coefA + 1) < Math.pow(10, -12)) {
        monCoefA = '-'
      } else if (stor.coefA > 0) {
        monCoefA = '+' + stor.coefA
      }
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr6,
        {
          f: stor.nomFct,
          a: monCoefA,
          b: stor.coefB
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr7,
        {
          f: stor.nomFct,
          a: j3pMonome(2, 0, stor.coefA),
          b: j3pMonome(1, 1, stor.coefB)
        })
    } else if (num === 4) {
      // (exp(x)+a)/(exp(x)-x)
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr8,
        {
          f: stor.nomFct,
          a: j3pMonome(2, 0, stor.coefA),
          b: stor.coefA
        })
      let coefAbis = stor.coefA
      if (Math.abs(stor.coefA - 1) < Math.pow(10, -12)) {
        coefAbis = '+'
      } else if (Math.abs(stor.coefA + 1) < Math.pow(10, -12)) {
        coefAbis = '-'
      } else if (stor.coefA > 0) {
        coefAbis = '+' + stor.coefA
      }
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr9,
        {
          f: stor.nomFct,
          a: j3pMonome(2, 0, stor.coefA),
          b: coefAbis
        })
    } else if (num === 5) {
      // exp(x)-ax*exp(x)+b
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr10,
        {
          f: stor.nomFct,
          b: stor.coefB
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr11,
        {
          f: stor.nomFct,
          a: j3pMonome(1, 1, stor.coefA),
          b: j3pMonome(2, 0, stor.coefB),
          l: stor.limiteRep2
        })
    } else if (num === 6) {
      // a*exp(x)/(exp(x)+b)
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr12,
        {
          f: stor.nomFct
        })
      monCoefA = stor.coefA + '\\mathrm{e}^x'
      if (Math.abs(stor.coefA - 1) < Math.pow(10, -12)) {
        monCoefA = '\\mathrm{e}^x'
      } else if (Math.abs(stor.coefA + 1) < Math.pow(10, -12)) {
        monCoefA = '\\left(-\\mathrm{e}^x\\right)'
      } else if (stor.coefA < 0) {
        monCoefA = '\\left(' + stor.coefA + '\\mathrm{e}^x\\right)'
      }
      let monCoefB = stor.coefB
      if (Math.abs(stor.coefB - 1) < Math.pow(10, -12)) {
        monCoefB = '+'
      } else if (Math.abs(stor.coefB + 1) < Math.pow(10, -12)) {
        monCoefB = '-'
      } else if (stor.coefB > 0) {
        monCoefB = '+' + stor.coefB
      }
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr13,
        {
          f: stor.nomFct,
          e: monCoefA,
          b: j3pMonome(2, 0, stor.coefB),
          c: stor.coefA,
          d: monCoefB
        })
    }
    j3pAffiche(stor.zoneExpli4, '', ds.textes.corr3)
    stor.idRepere = j3pGetNewId(stor.idRepere)
    creationCourbe()
    j3pToggleFenetres('Courbe')
  }
  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 6,
      nbetapes: 1,
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,
      tab_choix: [1, 2, 3, 4, 5, 6],
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"
      // Paramètres de la section, avec leurs valeurs par défaut.
      // aide_cours: true;
      textes: {
        titre_exo: 'Fonction exponentielle<br>Calcul de limites (2)',
        consigne1: 'Soit $£f$ la fonction définie sur $£i$ par :<br>$£f(x)=£g$.',
        consigne1bis: 'Soit $£f$ la fonction définie sur $£i\\opencurlybrace £j\\closecurlybrace $ par :<br>$£f(x)=£g$.',
        consigne2: 'Déterminer les limites suivantes.',
        consigne3: '$£l=$&1& <br>et $£m=$&2&',
        comment1: 'Ta réponse est imprécise !',
        comment2: 'Passe à la suite !',
        aide: 'Indication',
        FI0: 'Il n’y a pas de forme indéterminée.',
        FI1: 'Il y a une forme indéterminée en $-\\infty$, mais pas en $+\\infty$.',
        FI2: 'Il y a une forme indéterminée en $+\\infty$, mais pas en $-\\infty$.',
        FI3: 'Il y a une forme indéterminée en $-\\infty$ et en $+\\infty$.',
        aide1: 'Ne pas oublier des limites du cours : $\\limite{x}{+\\infty}{\\mathrm{e}^x}=+\\infty$ et $\\limite{x}{-\\infty}{\\mathrm{e}^x}=0$.',
        aide2: 'Ne pas oublier les limites du cours : $\\limite{x}{+\\infty}{\\frac{\\mathrm{e}^x}{x}}=+\\infty$ et $\\limite{x}{-\\infty}{x\\mathrm{e}^x}=0$.<br>On dit que $\\mathrm{e}^x$ "l’emporte" sur $x$ en l’infini.',
        // limite de ax*exp(x-b)
        courbe: 'Courbe représentative de la fonction',
        corr0: 'Voici la justification qui serait attendue :',
        corr1: 'En $-\\infty$ : $£f(x)=£a\\mathrm{e}^x\\times\\mathrm{e}^{£b}$. Or $\\limite{x}{-\\infty}{x\\mathrm{e}^x}=0$ donc $\\limite{x}{-\\infty}{£f(x)}=0$.',
        corr2: 'En $+\\infty$ : $\\limite{x}{+\\infty}{£a}=£l$ et $\\limite{x}{+\\infty}{\\mathrm{e}^x}=+\\infty$ donc $\\limite{x}{+\\infty}{£f(x)}=£l$.',
        corr3: 'On peut aussi vérifier la réponse sur la courbe représentative de la fonction.',
        // limite de exp(x)-ax+b
        corr4: 'En $-\\infty$ : $\\limite{x}{-\\infty}{\\mathrm{e}^x}=0$ et $\\limite{x}{-\\infty}{£a}=£l$ donc $\\limite{x}{-\\infty}{£f(x)}=£l$.',
        corr5: 'En $+\\infty$ : $£f(x)=x\\left(\\frac{\\mathrm{e}^x}{x}£a\\right)+£b$.<br>Or $\\limite{x}{+\\infty}{\\frac{\\mathrm{e}^x}{x}}=+\\infty$ d’où $\\limite{x}{+\\infty}{\\frac{\\mathrm{e}^x}{x}£a}=+\\infty$. Donc $\\limite{x}{+\\infty}{£f(x)}=+\\infty$.',
        // limite de (x+a)exp(bx)
        corr6: 'En $-\\infty$ : $£f(x)=x\\mathrm{e}^{£bx}£a\\mathrm{e}^{£bx}=\\frac{1}{£b}\\times £bx\\mathrm{e}^{£bx}£a\\mathrm{e}^{£bx}$. Or $\\limite{x}{-\\infty}{£bx}=-\\infty$ et $\\limite{X}{-\\infty}{X\\mathrm{e}^X}=0$ d’où $\\limite{x}{-\\infty}{£bx\\mathrm{e}^{£bx}}=0$.<br>De plus $\\limite{X}{-\\infty}{\\mathrm{e}^X}=0$ d’où $\\limite{x}{-\\infty}{\\mathrm{e}^{£bx}}=0$. Donc $\\limite{x}{-\\infty}{£f(x)}=0$.',
        corr7: 'En $+\\infty$ : $\\limite{x}{+\\infty}{x£a}=+\\infty$ de plus $\\limite{x}{+\\infty}{£b}=+\\infty$ et $\\limite{X}{+\\infty}{\\mathrm{e}^X}=+\\infty$ <br>d’où $\\limite{x}{+\\infty}{\\mathrm{e}^{£b}}=+\\infty$. Donc $\\limite{x}{+\\infty}{£f(x)}=+\\infty$.',
        // limite de (exp(x)+a)/(exp(x)-x)
        corr8: 'En $-\\infty$ : $\\limite{x}{-\\infty}{\\mathrm{e}^x}=0$ d’où $\\limite{x}{-\\infty}{\\mathrm{e}^x£a}=£b$ et $\\limite{x}{-\\infty}{\\mathrm{e}^{x}-x}=+\\infty$.<br>Donc $\\limite{x}{-\\infty}{£f(x)}=0$.',
        corr9: 'En $+\\infty$ : $£f(x)=\\frac{\\mathrm{e}^{-x}(\\mathrm{e}^x£a)}{\\mathrm{e}^{-x}(\\mathrm{e}^x-x)}=\\frac{1£b\\mathrm{e}^{-x}}{1-\\frac{x}{\\mathrm{e}^x}}$.<br>Or $\\limite{x}{+\\infty}{\\mathrm{e}^{-x}}=0$ et $\\limite{x}{+\\infty}{\\frac{\\mathrm{e}^x}{x}}=+\\infty$ d’où $\\limite{x}{+\\infty}{\\frac{x}{\\mathrm{e}^x}}=0$.<br>Donc $\\limite{x}{+\\infty}{£f(x)}=1$.',
        // limite de exp(x)+ax*exp(x)+b
        corr10: 'En $-\\infty$ : $\\limite{x}{-\\infty}{\\mathrm{e}^x}=0$ et $\\limite{x}{-\\infty}{x\\mathrm{e}^x}=0$ donc $\\limite{x}{-\\infty}{£f(x)}=£b$.',
        corr11: 'En $+\\infty$ : $£f(x)=\\mathrm{e}^x\\left(1£a\\right)£b$.<br>Or $\\limite{x}{+\\infty}{\\mathrm{e}^x}=+\\infty$ et $\\limite{x}{+\\infty}{1£a}=£l$ donc $\\limite{x}{+\\infty}{£f(x)}=£l$.',
        // limite de a*exp(x)/(exp(x)+b)
        corr12: 'En $-\\infty$ : $\\limite{x}{-\\infty}{\\mathrm{e}^x}=0$ donc $\\limite{x}{-\\infty}{£f(x)}=0$.',
        corr13: 'En $+\\infty$ : $£f(x)=\\frac{\\mathrm{e}^{-x}\\times£e}{\\mathrm{e}^{-x}\\left(\\mathrm{e}^x£b\\right)}=\\frac{£c}{1£d\\mathrm{e}^{-x}}$.<br>Or $\\limite{X}{-\\infty}{\\mathrm{e}^X}=0$ d’où $\\limite{x}{+\\infty}{\\mathrm{e}^{-x}}=0$ donc $\\limite{x}{+\\infty}{£f(x)}=£c$.'
      },
      pe: 0
    }
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        ds.tab_choix = purge(ds.tab_choix)
        ds.nbrepetitions = ds.tab_choix.length
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.65 })
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      {
        let i
        let coefA
        let coefB
        let fDeX
        let domaineDef
        let valInterdite
        let limiteRep1// réponse pour la limite en -\\infty
        let limiteRep2// réponse pour la limite en +\\infty
        if ((me.questionCourante - 1) % ds.nbrepetitions === 0) {
        // on construit un tableau où chaque élément contiendra la fonction, ce vers quoi va tendre x et la limite attendue
          stor.tab_quest_ordonne = j3pShuffle(ds.tab_choix)
        }
        me.logIfDebug('tab_quest_ordonne:' + stor.tab_quest_ordonne)
        const j = (me.questionCourante - 1) % ds.nbrepetitions
        stor.num_quest = stor.tab_quest_ordonne[j]
        switch (stor.num_quest) {
          case 1:
          // fonction ax*exp(x-b)
            do {
              coefA = j3pGetRandomInt(-3, 3)
            } while (Math.abs(coefA) < Math.pow(10, -12))
            do {
              coefB = j3pGetRandomInt(1, 3)
            } while (Math.abs(Math.abs(coefB) - Math.abs(coefA)) < Math.pow(10, -12))
            fDeX = j3pMonome(1, 1, coefA) + '\\mathrm{e}^{x' + j3pMonome(1, 0, -coefB) + '}'
            domaineDef = '\\R'
            limiteRep1 = '0'
            if (coefA > 0) {
              limiteRep2 = '+\\infty'
            } else {
              limiteRep2 = '-\\infty'
            }
            stor.fonction = coefA + '*x*exp(x-' + coefB + ')'
            break

          case 2:
          // exp(x)-ax+b
            coefA = j3pGetRandomInt(1, 4)
            do {
              coefB = j3pGetRandomInt(-3, 3)
            } while ((Math.abs(coefB) < Math.pow(10, -12)) || (Math.abs(Math.abs(coefA) - Math.abs(coefB)) < Math.pow(10, -12)))
            fDeX = '\\mathrm{e}^x' + j3pMonome(1, 1, -coefA) + j3pMonome(2, 0, coefB)
            domaineDef = '\\R'
            limiteRep1 = '+\\infty'
            limiteRep2 = '+\\infty'
            stor.fonction = 'exp(x)-' + coefA + '*x+' + coefB
            break

          case 3:
          // (x+a)exp(bx)
            do {
              coefA = j3pGetRandomInt(-3, 3)
            } while (Math.abs(coefA) < Math.pow(10, -12))
            do {
              coefB = j3pGetRandomInt(2, 3)
            } while (Math.abs(Math.abs(coefB) - Math.abs(coefA)) < Math.pow(10, -12))
            fDeX = '\\left(x' + j3pMonome(2, 0, coefA) + '\\right)\\mathrm{e}^{' + j3pMonome(1, 1, coefB) + '}'
            domaineDef = '\\R'
            limiteRep1 = '0'
            limiteRep2 = '+\\infty'
            stor.fonction = '(x+' + coefA + ')*exp(' + coefB + '*x)'
            break

          case 4:
          // (exp(x)+a)/(exp(x)-x)
            do {
              coefA = j3pGetRandomInt(-3, 3)
            } while (Math.abs(coefA) < Math.pow(10, -12))
            fDeX = '\\frac{\\mathrm{e}^x' + j3pMonome(2, 0, coefA) + '}{\\mathrm{e}^x-x}'
            domaineDef = '\\R'
            limiteRep1 = '0'
            limiteRep2 = '1'
            stor.fonction = '(exp(x)+' + coefA + ')/(exp(x)-x)'
            break

          case 5:
          // exp(x)+ax*exp(x)+b
            do {
              coefA = j3pGetRandomInt(-3, -1)
            } while (Math.abs(coefA) < 1 + Math.pow(10, -12))
            do {
              coefB = j3pGetRandomInt(-3, 3)
            } while ((Math.abs(coefB) < Math.pow(10, -12)) || (Math.abs(Math.abs(coefA) - Math.abs(coefB)) < Math.pow(10, -12)))
            fDeX = '\\mathrm{e}^x' + j3pMonome(2, 1, coefA) + '\\mathrm{e}^x' + j3pMonome(2, 0, coefB)
            domaineDef = '\\R'
            limiteRep1 = coefB
            if (coefA > 0) {
              limiteRep2 = '+\\infty'
            } else {
              limiteRep2 = '-\\infty'
            }
            stor.fonction = 'exp(x)+' + coefA + '*x*exp(x)+' + coefB
            break

          case 6:
          // a*exp(x)/(exp(x)+b)
            do {
              coefA = j3pGetRandomInt(-3, 3)
            } while (Math.abs(coefA) < Math.pow(10, -12))
            do {
              coefB = j3pGetRandomInt(-3, 3)
            } while ((Math.abs(coefB) < Math.pow(10, -12)) || (Math.abs(Math.abs(coefA) - Math.abs(coefB)) < Math.pow(10, -12)))
            if (Math.abs(coefA - 1) < Math.pow(10, -12)) {
              fDeX = '\\frac{\\mathrm{e}^x}{\\mathrm{e}^x' + j3pMonome(2, 0, coefB) + '}'
            } else if (Math.abs(coefA + 1) < Math.pow(10, -12)) {
              fDeX = '\\frac{-\\mathrm{e}^x}{\\mathrm{e}^x' + j3pMonome(2, 0, coefB) + '}'
            } else {
              fDeX = '\\frac{' + coefA + '\\mathrm{e}^x}{\\mathrm{e}^x' + j3pMonome(2, 0, coefB) + '}'
            }
            if (coefB > 0) {
              domaineDef = '\\R'
            } else {
              domaineDef = '\\R\\setminus'
              if (Math.abs(coefB + 1) < Math.pow(10, -12)) {
                valInterdite = '0'
              } else {
                valInterdite = '\\mathrm{ln}' + (-coefB)
              }
            }
            limiteRep1 = '0'
            limiteRep2 = String(coefA)
            stor.fonction = coefA + '*exp(x)/(exp(x)+' + coefB + ')'
            break
        }
        stor.coefA = coefA
        stor.coefB = coefB
        stor.limiteRep1 = limiteRep1
        stor.limiteRep2 = limiteRep2
        const tabNomFct = ['f', 'g', 'h']
        const nomFct = tabNomFct[j3pGetRandomInt(0, 2)]
        stor.nomFct = nomFct// pour être réutilisé dans la correction
        stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
        for (i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
        let maConsigne1
        if ((stor.num_quest === 6) && (coefB < 0)) {
          maConsigne1 = ds.textes.consigne1bis
          j3pAffiche(stor.zoneCons1, '', maConsigne1,
            {
              f: nomFct,
              g: fDeX,
              i: domaineDef,
              j: valInterdite
            })
        } else {
          maConsigne1 = ds.textes.consigne1
          j3pAffiche(stor.zoneCons1, '', maConsigne1,
            {
              f: nomFct,
              g: fDeX,
              i: domaineDef
            })
        }

        j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2)
        stor.elts = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3,
          {
            l: '\\limite{x}{-\\infty}{' + nomFct + '(x)}',
            m: '\\limite{x}{+\\infty}{' + nomFct + '(x)}',
            inputmq1: { texte: '' },
            inputmq2: { texte: '' }
          })

        j3pStyle(stor.zoneCons4, { paddingTop: '20px' })
        stor.laPalette = j3pAddElt(stor.zoneCons4, 'div')
        // palette MQ :
        j3pPaletteMathquill(stor.laPalette, stor.elts.inputmqList[0], {
          liste: ['racine', 'fraction', 'puissance', 'inf']
        })

        stor.elts.inputmqList.forEach((zoneInput) => zoneInput.addEventListener('focusin', ecoute.bind(null, zoneInput)))
        for (let i = 1; i <= 2; i++) mqRestriction(stor.elts.inputmqList[i - 1], '\\d,.+-/^', { commandes: ['racine', 'fraction', 'puissance', 'inf'] })
        stor.elts.inputmqList[0].typeReponse = ['texte']
        stor.elts.inputmqList[1].typeReponse = ['texte']
        if (stor.num_quest === 5) {
          stor.elts.inputmqList[0].typeReponse = ['nombre', 'exact']
        }
        if (stor.num_quest === 6) {
          stor.elts.inputmqList[1].typeReponse = ['nombre', 'exact']
        }
        stor.elts.inputmqList[0].reponse = [limiteRep1]
        stor.elts.inputmqList[1].reponse = [limiteRep2]
        j3pFocus(stor.elts.inputmqList[0])

        stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px' } })

        me.logIfDebug('reponse1 : ' + stor.elts.inputmqList[0].reponse[0] + '   et réponse2 : ' + stor.elts.inputmqList[1].reponse[0])

        stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
        stor.zoneBtn = j3pAddElt(stor.zoneD, 'div')
        stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '', { style: { paddingTop: '10px' } })
        aide(stor.tab_quest_ordonne[j])

        const mesZonesSaisie = stor.elts.inputmqList.map(elt => elt.id)
        stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
      }
      // Obligatoire
      me.finEnonce()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (reponse.bonneReponse && (!me.isElapsed)) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(stor.num_quest, true)
            j3pEmpty(stor.laPalette)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
              me.typederreurs[10]++
              me.cacheBoutonValider()
              afficheCorrection(stor.num_quest, false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
              // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                j3pEmpty(stor.laPalette)
                afficheCorrection(stor.num_quest, false)
                me.cacheBoutonValider()
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      j3pDetruitFenetres('Boutons')
      j3pDetruitFenetres('Aide')
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
