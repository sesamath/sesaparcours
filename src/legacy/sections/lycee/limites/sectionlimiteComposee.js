import $ from 'jquery'
import { j3pShuffle, j3pDetruit, j3pFocus, j3pGetRandomInt, j3pMonome, j3pPaletteMathquill, j3pPGCD, j3pAddElt } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCalculValeur } from 'src/legacy/core/functionsTarbre'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        mars 2020
        Calcul de la limite d’une fonction composée
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDetruit(stor.zoneCons4)
    j3pDetruit(stor.zoneCons5)
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    for (let i = 1; i <= 3; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div')
    j3pAffiche(stor.zoneexplication1, '', ds.textes.corr1)
    j3pAffiche(stor.zoneexplication2, '', ds.textes.corr2, stor.objCons)
    j3pAffiche(stor.zoneexplication3, '', ds.textes.corr3, stor.objCons)
  }

  function ecoute (nom) {
    if (nom.className.indexOf('mq-editable-field') > -1) {
      stor.zoneCons5.innerHTML = ''
      j3pPaletteMathquill(stor.zoneCons5, nom, {
        liste: ['inf', 'fraction', 'puissance', 'racine', 'exp']
      })
    }
  }

  function choixElt (tab, tabInit, leTableau) {
    // Cette fonction complète leTableau par une valeur choisie au hasard dans tab.
    // Si tab est vide, elle fait une copie de tabInit dans tab auparavant
    if (tab.length === 0) tab = [...tabInit]
    const pioche = j3pGetRandomInt(0, (tab.length - 1))
    leTableau.push(tab[pioche])
    tab.splice(pioche, 1)
  }

  function donneesFonction (num) {
    // num est le cas de figure
    // Lorsqu’il est entre 1 et 10, on a une limite en l’infini. S’il est entre 11 et 20, on a une limite en un réel
    // Cette fonction renvoie un objet contenant différentes propriétés de la suite (son expression, sa limite, ...)
    let n, a, b, c, signe, choix
    const obj = {}
    const cote = j3pGetRandomInt(0, 1) // pour la limite en un réel, on choisit à gauche ou à droite
    obj.tabLim1 = []
    obj.tabLim2 = []
    obj.tabLim = [] // ces trois tableaux peuvent servir à accepter des limites qui ne seraient pas les premiers choix (par exemple 1^+ alors que 1 suffirait)
    if (num === 1) {
      // exp(1/(x+a))
      // limite en xlim égal à +\\infty ou -\\infty
      obj.xlim = (j3pGetRandomInt(0, 1) === 0) ? '+\\infty' : '-\\infty'
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      obj.fdex = '\\mathrm{e}^{\\frac{1}{x' + j3pMonome(2, 0, a, 'x') + '}}'
      obj.f1 = '\\frac{1}{x' + j3pMonome(2, 0, a, 'x') + '}'
      obj.f2 = '\\mathrm{e}^X'
      obj.lim1 = '0'
      obj.tabLim1.push((obj.xlim === '+\\infty') ? '0^+' : '0^-')
      obj.tabLim1.push((obj.xlim === '+\\infty') ? '0+' : '0-')
      obj.lim2 = '1'
      obj.tabLim2.push((obj.xlim === '+\\infty') ? '1^+' : '1^-')
      obj.tabLim2.push((obj.xlim === '+\\infty') ? '1+' : '1-')
      obj.tabLim.push((obj.xlim === '+\\infty') ? '1^+' : '1^-')
      obj.tabLim.push((obj.xlim === '+\\infty') ? '1+' : '1-')
    } else if (num === 2) {
      // sqrt(ax^2+b)
      // limite en xlim égal à +\\infty ou -\\infty
      obj.xlim = (j3pGetRandomInt(0, 1) === 0) ? '+\\infty' : '-\\infty'
      a = j3pGetRandomInt(1, 5)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(a - Math.abs(b)) < Math.pow(10, -12))
      obj.fdex = '\\sqrt{' + j3pMonome(1, 2, a, 'x') + j3pMonome(2, 0, b) + '}'
      obj.f1 = j3pMonome(1, 2, a, 'x') + j3pMonome(2, 0, b)
      obj.f2 = '\\sqrt{X}'
      obj.lim1 = '+\\infty'
      obj.lim2 = '+\\infty'
    } else if (num === 3) {
      // exp(ax+b)
      // limite en xlim égal à +\\infty ou -\\infty
      obj.xlim = (j3pGetRandomInt(0, 1) === 0) ? '+\\infty' : '-\\infty'
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (Math.abs(Math.abs(a) - Math.abs(b)) < Math.pow(10, -12))
      obj.fdex = '\\mathrm{e}^{' + j3pMonome(1, 1, a, 'x') + j3pMonome(2, 0, b) + '}'
      obj.f1 = j3pMonome(1, 1, a, 'x') + j3pMonome(2, 0, b)
      obj.f2 = '\\mathrm{e}^{X}'
      obj.lim1 = (((obj.xlim === '+\\infty') && (a > 0)) || ((obj.xlim === '-\\infty') && (a < 0))) ? '+\\infty' : '-\\infty'
      obj.lim2 = (((obj.xlim === '+\\infty') && (a > 0)) || ((obj.xlim === '-\\infty') && (a < 0))) ? '+\\infty' : '0'
      if (obj.lim2 === '0') {
        obj.tabLim2.push('0^+', '0+')
        obj.tabLim.push('0^+', '0+')
      }
    } else if (num === 4) {
      // exp(1/x-a) ou exp(1/(a-x))
      // limite en xlim égal à a
      a = j3pGetRandomInt(1, 5)
      obj.xlim = (cote === 0) ? a + '^+' : a + '^-'
      choix = j3pGetRandomInt(0, 1)
      obj.fdex = (choix === 0) ? '\\mathrm{e}^{\\frac{1}{x' + j3pMonome(2, 0, -a) + '}}' : '\\mathrm{e}^{\\frac{1}{' + a + '-x}}'
      obj.f1 = (choix === 0) ? '\\frac{1}{x' + j3pMonome(2, 0, -a) + '}' : '\\frac{1}{' + a + '-x}'
      obj.f2 = '\\mathrm{e}^{X}'
      obj.lim1 = (cote === 0)
        ? (choix === 0) ? '+\\infty' : '-\\infty'
        : (choix === 0) ? '-\\infty' : '+\\infty'
      obj.lim2 = (obj.lim1 === '+\\infty') ? '+\\infty' : '0'
      if (obj.lim2 === '0') {
        obj.tabLim2.push('0^+', '0+')
        obj.tabLim.push('0^+', '0+')
      }
    } else if (num === 5) {
      // 1/(aexp(x)+1) ou 1/(aexp(x)-1)
      // limite en xlim égal à +\\infty ou -\\infty
      obj.xlim = (j3pGetRandomInt(0, 1) === 0) ? '+\\infty' : '-\\infty'
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (j3pPGCD(Math.abs(a), Math.abs(b)) !== 1)
      signe = j3pGetRandomInt(0, 1) * 2 - 1
      obj.fdex = '\\frac{1}{' + j3pMonome(1, 1, a, '\\mathrm{e}^x') + j3pMonome(2, 0, signe) + '}'
      obj.f1 = j3pMonome(1, 1, a, '\\mathrm{e}^x') + j3pMonome(2, 0, signe)
      obj.f2 = '\\frac{1}{X}'
      obj.lim1 = (obj.xlim === '+\\infty')
        ? (a > 0) ? '+\\infty' : '-\\infty'
        : String(signe)
      if (obj.xlim === '-\\infty') {
        if (a > 0) {
          obj.tabLim1.push(String(signe) + '^+', String(signe) + '+')
        } else {
          obj.tabLim1.push(String(signe) + '^-', String(signe) + '-')
        }
      }
      obj.lim2 = (obj.lim1.indexOf('\\infty') > -1) ? '0' : String(signe)
      if (obj.lim2 === '0') {
        if (a > 0) {
          obj.tabLim2.push('0^+', '0+')
          obj.tabLim.push('0^+', '0+')
        } else {
          obj.tabLim2.push('0^-', '0-')
          obj.tabLim.push('0^-', '0-')
        }
      } else {
        if (a > 0) {
          obj.tabLim2.push(String(signe) + '^-', String(signe) + '-')
          obj.tabLim.push(String(signe) + '^-', String(signe) + '-')
        } else {
          obj.tabLim2.push(String(signe) + '^+', String(signe) + '+')
          obj.tabLim.push(String(signe) + '^+', String(signe) + '+')
        }
      }
    } else if (num === 6) {
      // a/bsqrt(x)+c
      // limite en xlim égal à +\\infty
      obj.xlim = '+\\infty'
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        c = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (j3pPGCD(Math.abs(c), Math.abs(b)) !== 1)
      obj.fdex = '\\frac{' + a + '}{' + j3pMonome(1, 1, b, '\\sqrt{x}') + j3pMonome(2, 0, c) + '}'
      obj.f1 = j3pMonome(1, 1, b, '\\sqrt{x}') + j3pMonome(2, 0, c)
      obj.f2 = '\\frac{' + a + '}{X}'
      obj.lim1 = (b > 0) ? '+\\infty' : '-\\infty'
      obj.lim2 = '0'
      if (b > 0) {
        obj.tabLim2.push('0^+', '0+')
        obj.tabLim.push('0^+', '0+')
      } else {
        obj.tabLim2.push('0^-', '0-')
        obj.tabLim.push('0^-', '0-')
      }
    } else if (num === 7) {
      // cos(1/(ax+b)) ou sin(1/(ax+b))
      // limite en xlim égal à +\\infty ou -\\infty
      obj.xlim = (j3pGetRandomInt(0, 1) === 0) ? '+\\infty' : '-\\infty'
      choix = j3pGetRandomInt(0, 1)
      signe = (2 * j3pGetRandomInt(0, 1) - 1)
      a = j3pGetRandomInt(1, 5) * signe
      do {
        b = j3pGetRandomInt(1, 5) * (-signe)
      } while (j3pPGCD(Math.abs(a), Math.abs(b)) !== 1)
      obj.fdex = ((choix === 0) ? '\\sin' : '\\cos') + '\\left(\\frac{1}{' + j3pMonome(1, 1, a, 'x') + j3pMonome(2, 0, b) + '}\\right)'
      obj.f1 = '\\frac{1}{' + j3pMonome(1, 1, a, 'x') + j3pMonome(2, 0, b) + '}'
      obj.f2 = ((choix === 0) ? '\\sin' : '\\cos') + '(X)'
      obj.lim1 = '0'
      if ((obj.xlim === '+\\infty' && a > 0) || (obj.xlim === '-\\infty' && a < 0)) {
        obj.tabLim1.push('0^+', '0+')
      } else {
        obj.tabLim1.push('0^-', '0-')
      }
      obj.lim2 = (choix === 1) ? '1' : '0'
      if (choix === 1) {
        obj.tabLim2.push('1^-', '1-')
        obj.tabLim.push('1^-', '1-')
      } else {
        if ((obj.xlim === '+\\infty' && a > 0) || (obj.xlim === '-\\infty' && a < 0)) {
          obj.tabLim2.push('0^+', '0+')
          obj.tabLim.push('0^+', '0+')
        } else {
          obj.tabLim2.push('0^-', '0-')
          obj.tabLim.push('0^-', '0-')
        }
      }
    } else if (num === 8) {
      // sqrt(exp(ax+b))
      // limite en xlim égal à +\\infty ou -\\infty
      obj.xlim = (j3pGetRandomInt(0, 1) === 0) ? '+\\infty' : '-\\infty'
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (j3pPGCD(Math.abs(a), Math.abs(b)) !== 1)
      obj.fdex = '\\sqrt{\\mathrm{e}^{' + j3pMonome(1, 1, a, 'x') + j3pMonome(2, 0, b) + '}}'
      obj.f1 = '\\mathrm{e}^{' + j3pMonome(1, 1, a, 'x') + j3pMonome(2, 0, b) + '}'
      obj.f2 = '\\sqrt{X}'
      obj.lim1 = (obj.xlim === '+\\infty') ? (a > 0) ? '+\\infty' : '0' : (a > 0) ? '0' : '+\\infty'
      if (obj.lim1 === '0') obj.tabLim1.push('0^+', '0+')
      obj.lim2 = (obj.lim1 === '0') ? '0' : '+\\infty'
      if (obj.lim1 === '0') {
        obj.tabLim2.push('0^+', '0+')
        obj.tabLim.push('0^+', '0+')
      }
    } else if (num === 9) {
      // (aexp(x)+b)^n
      // limite en xlim égal à +\\infty
      obj.xlim = '+\\infty'
      a = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      do {
        b = j3pGetRandomInt(1, 5) * (2 * j3pGetRandomInt(0, 1) - 1)
      } while (j3pPGCD(Math.abs(a), Math.abs(b)) !== 1)
      n = j3pGetRandomInt(2, 3)
      obj.fdex = '\\left(' + j3pMonome(1, 1, a, '\\mathrm{e}^x') + j3pMonome(2, 0, b) + '\\right)^' + n
      obj.f1 = j3pMonome(1, 1, a, '\\mathrm{e}^x') + j3pMonome(2, 0, b)
      obj.f2 = 'X\\quad^' + n
      obj.lim1 = (a > 0) ? '+\\infty' : '-\\infty'
      obj.lim2 = (n % 2 === 0)
        ? '+\\infty'
        : (obj.lim1 === '+\\infty') ? '+\\infty' : '-\\infty'
    }
    if (num <= 10) {
      obj.xlim1 = obj.xlim // xlim1 sert pour les limites en un réel; On n’a pas besoin de préciser parfois de quel côté on se trouve
    }
    obj.lim = obj.lim2
    obj.l = '\\limite{x}{' + obj.xlim + '}{' + obj.fdex + '}'
    obj.l1 = '\\limite{x}{' + obj.xlim1 + '}{' + obj.f1 + '}'
    obj.l2 = '\\limite{X}{' + obj.lim1 + '}{' + obj.f2 + '}'
    return obj
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 5,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Limite d’une fonction composée',
        // on donne les phrases de la consigne
        consigne1: 'Calculer $£l$ en suivant les étapes&nbsp;:',
        consigne2: '$£{l1}=$&1& et $\\limite{X}{\\editable{}}{£{f2}}=$&3&.',
        consigne3: 'La propriété sur les limites de fonctions composées nous permet de conclure que $£l=$&1&',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'Pour l’infini, il faut préciser le signe&nbsp;!',
        comment2: 'Toutes les limites doivent être données sous une forme simple&nbsp;!',
        // et les phrases utiles pour les explications de la réponse
        corr1: 'La fonction dont on cherche la limite est la composée de deux fonctions admettant des limites.',
        corr2: '$£{l1}=£{lim1}$ et $£{l2}=£{lim2}$.',
        corr3: 'On conclut alors que $£l=£{lim}$.'
      },

      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    // Construction de la page
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zones.IG, ds.indication)

    const tabQuest = []
    let k
    stor.tabQuest = []
    for (k = 1; k <= 9; k++) {
      tabQuest.push(k)
    }
    const tabQuestInit = [...tabQuest]
    for (k = 1; k <= ds.nbrepetitions; k++) {
      choixElt(tabQuest, tabQuestInit, stor.tabQuest)
    }
    stor.tabQuest = j3pShuffle(stor.tabQuest)
    enonceMain()
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    // console.log('tabQuest', stor.tabQuest)
    stor.quest = stor.tabQuest[me.questionCourante - 1]
    stor.objCons = Object.assign({}, donneesFonction(stor.quest))
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 1; i <= 6; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // console.log('objCons', stor.objCons)
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, stor.objCons)
    const elt1 = j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, {
      l1: stor.objCons.l1,
      f2: stor.objCons.f2,
      inputmq1: { texte: '' },
      inputmq3: { texte: '' }
    })
    const elt2 = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3, {
      l: stor.objCons.l,
      inputmq1: { texte: '' }
    })
    if (ds.nbchances === 1) {
      j3pAffiche(stor.zoneCons4, '', ds.textes.remarque, {
        style: { fontStyle: 'italic', fontSize: '0.9em' }
      })
    }
    stor.zoneInput = [elt1.inputmqList[0], elt1.inputmqefList[0], elt1.inputmqList[1], elt2.inputmqList[0]]
    stor.zoneInput.forEach(eltInput => {
      $(eltInput).focusin(ecoute.bind(null, eltInput))
      mqRestriction(eltInput, '\\d,+./^-', { commandes: ['inf', 'fraction', 'puissance', 'racine', 'exp'] })
      eltInput.typeReponse = ['texte']
    })

    const tabLim = []
    let numLim = 0
    const tabLimites = [stor.objCons.lim1, stor.objCons.lim2, stor.objCons.lim]
    tabLimites.forEach(function (x) {
      tabLim[numLim] = [String(x)]
      if (['+', '-'].indexOf(String(x).charAt(String(x).length - 1)) > -1) {
        // On a une limite de la forme a^+ ou a^- donc j’accepte comme limite a
        tabLim[numLim].push(String(x).substring(0, String(x).length - 2))
      }
      if (String(x).indexOf('mathrm{e}') > -1) {
        // on a une limite avec exponentielle, donc on doit la transformer sous la forme e^a pour accepter la valeur
        tabLim[numLim].push(String(x).replace('\\mathrm{e}', 'e'))
      }
      numLim++
    })
    j3pFocus(stor.zoneInput[0])
    stor.zoneInput[0].reponse = tabLim[0].concat(stor.objCons.tabLim1)
    stor.zoneInput[1].reponse = tabLim[0].concat(stor.objCons.tabLim1)
    stor.zoneInput[2].reponse = tabLim[1].concat(stor.objCons.tabLim2)
    stor.zoneInput[3].reponse = tabLim[2].concat(stor.objCons.tabLim)
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zones.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Paramétrage de la validation
    // zonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)

    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: stor.zoneInput.map(elt => elt.id)
    })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie

      let reponse = {}
      let infini = false
      let nbInf = -1
      let estCalcul = false
      let nbCalcul = -1
      reponse.aRepondu = stor.fctsValid.valideReponses()
      if (reponse.aRepondu) {
        // JE regarde si l’élève n’a écrit que infini sans signe
        for (let i = 2; i >= 0; i--) {
          if (stor.fctsValid.zones.reponseSaisie[i] === '\\infty') {
            infini = true
            nbInf = i
          } else if (!stor.fctsValid.zones.reponseSaisie[i].includes('\\infty')) {
            let estExpo = false
            // console.log(/e\^[1-9]/.test(stor.fctsValid.zones.reponseSaisie[i]), /e\^(\{\-?[1-9]+)/.test(stor.fctsValid.zones.reponseSaisie[i]))
            const valSaisie = stor.fctsValid.zones.reponseSaisie[i]
            estExpo = (estExpo || /e\^[1-9]/.test(valSaisie))
            estExpo = (estExpo || /e\^({-?[1-9]+)/.test(valSaisie))
            if (!/\^[+-]/.test(valSaisie) && !estExpo && ['+', '-'].indexOf(valSaisie.charAt(valSaisie.length - 1)) === -1) {
              // console.log(j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[i]),stor.fctsValid.zones.reponseSaisie[i])
              const valeur = j3pCalculValeur(stor.fctsValid.zones.reponseSaisie[i])
              estCalcul = ((String(valeur) !== String(stor.fctsValid.zones.reponseSaisie[i])) && Math.abs(Math.round(valeur) - valeur) < Math.pow(10, -12))
              nbCalcul = i
            }
          }
        }
        if (infini || estCalcul) {
          reponse.aRepondu = false
        } else {
          reponse = stor.fctsValid.validationGlobale()
        }
      }
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        let msgReponseManquante
        if (infini) {
          msgReponseManquante = ds.textes.comment1
          j3pFocus(stor.zoneInput[nbInf])
        } else if (estCalcul) {
          msgReponseManquante = ds.textes.comment2
          j3pFocus(stor.zoneInput[nbCalcul])
        }
        this.reponseManquante(stor.zoneCorr, msgReponseManquante)
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (reponse.bonneReponse) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          afficheCorrection(true)
          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.cacheBoutonValider()
            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(false)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
            } else {
              // Erreur au nème essai
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              afficheCorrection(false)
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
