import { j3pAddElt, j3pAjouteBouton, j3pShuffle, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pFreezeElt, j3pPaletteMathquill, j3pShowError, j3pGetNewId } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    avril 2014
    cette section permet de se tester sur les limites de base des fonctions exponentielle et logarithme népérien
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 7, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 15, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['ajout_xlnx', false, 'boolean', 'Pour ceux qui souhaitent ajouter cette limite en 0 dans le cours'],
    ['ln_seulement', false, 'boolean', 'Mettre true si on ne veut que les limites de la fonction ln'],
    ['exp_seulement', false, 'boolean', 'Mettre true si on ne veut que les limites de la fonction exp']

  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = this.storage

  function afficheCours (typeFct) {
    // typeFct vaut "ln" ou "exp"
    if (stor.premiereFois) {
      // je n’ai pas encore affiché de cours donc je dois tout créer
      stor.fenetreAide = j3pGetNewId('fenetreAide')
      me.fenetresjq = [
        { name: 'Aide', title: ds.textes.aide1, top: me.zonesElts.MG.offsetTop, left: 0, width: 500, id: stor.fenetreAide }
      ]
      // Création mais sans affichage.
      j3pCreeFenetres(me)

      stor.premiereFois = false
    } else {
      // j’ai affiché le premier cours, il faut donc que je l’efface'
      /* j3pDetruit(stor.coursDebut)
      j3pDetruit(stor.coursCourbe)
      j3pDetruit(stor.coursSuite) */
      j3pEmpty(stor.fenetreAide)
    }
    stor.coursDebut = j3pAddElt(stor.fenetreAide, 'div')
    stor.coursCourbe = j3pAddElt(stor.fenetreAide, 'div')
    stor.coursSuite = j3pAddElt(stor.fenetreAide, 'div')
    stor.idRepere = j3pGetNewId('unrepere')
    if (typeFct === 'ln') {
      // pour la fonction ln
      j3pAffiche(stor.coursDebut, '', stor.cours1) // première partie du cours
      // création de la courbe :
      stor.coursDebut = j3pAddElt(stor.fenetreAide, 'div')
      const repere = new Repere({
        idConteneur: stor.coursCourbe,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 400,
        haut: 300,
        pasdunegraduationX: 1,
        pixelspargraduationX: 30,
        pasdunegraduationY: 1,
        pixelspargraduationY: 30,
        xO: 50,
        yO: 150,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: stor.borne_inf, par3: stor.borne_sup, par4: stor.nb_points, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
      repere.construit()
      j3pElement(stor.idRepere).style.position = 'relative'
      for (let i = 2; i <= 4; i++) stor['coursPartie' + i] = j3pAddElt(stor.coursSuite, 'div')
      j3pAffiche(stor.coursPartie2, '', stor.cours2)
      j3pAffiche(stor.coursPartie3, '', stor.cours3)
      j3pAffiche(stor.coursPartie4, '', stor.cours4)
      j3pDetruit(stor.bntLn)
      if (ds.ln_seulement) {
        // J’affiche le bouton "section suivante" car c’est la fin du cours'
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        j3pAjouteBouton(stor.zoneBtn, stor.bntExp, 'MepBoutonsFenetre', ds.textes.bouton2, afficheCours.bind(null, 'exp'))
      }
      j3pToggleFenetres('Aide')
    } else if (typeFct === 'exp') {
      // pour la fonction exponentielle
      j3pAffiche(stor.coursDebut, '', stor.coursbis1) // première partie du cours
      // création de la courbe :
      const repere = new Repere({
        idConteneur: stor.coursCourbe,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 400,
        haut: 300,
        pasdunegraduationX: 1,
        pixelspargraduationX: 30,
        pasdunegraduationY: 1,
        pixelspargraduationY: 30,
        xO: 250,
        yO: 250,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonctionbis, par2: stor.borne_infbis, par3: stor.borne_supbis, par4: stor.nb_pointsbis, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
      repere.construit()
      j3pElement(stor.idRepere).style.position = 'relative'
      for (let i = 2; i <= 3; i++) stor['coursPartie' + i] = j3pAddElt(stor.coursSuite, 'div')
      j3pAffiche(stor.coursPartie2, '', stor.coursbis2)
      j3pAffiche(stor.coursPartie3, '', stor.coursbis3)
      // J’affiche le bouton "section suivante" car c’est la fin du cours'
      me.afficheBoutonSectionSuivante()
      me.focus('sectioncontinuer')
      j3pDetruit(stor.bntExp)
      if (ds.exp_seulement) {
        j3pToggleFenetres('Aide')
      }
    }
  }
  function lancementCours () {
    // on considère que l’élève ne connait pas bien son cours
    // on le lui montre donc de nouveau'
    j3pDetruit(stor.btnAfficheCours)
    j3pDetruit(stor.zoneCons1)
    j3pDetruit(stor.zoneCons2)
    j3pDetruit(stor.zoneCorr)
    let commentCours
    if (ds.ln_seulement) {
      commentCours = ds.textes.comment4
    } else if (ds.exp_seulement) {
      commentCours = ds.textes.comment5
    } else {
      commentCours = ds.textes.comment3
    }
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', commentCours, { style: me.styles.moyen.explications })
    stor.zoneBtn = j3pAddElt(me.zonesElts.MD, 'div', '', { style: { padding: '15px' } })
    stor.bntExp = j3pGetNewId('btnExp')
    stor.bntLn = j3pGetNewId('btnLn')
    if (ds.exp_seulement) {
      j3pAjouteBouton(stor.zoneBtn, stor.bntExp, 'MepBoutonsFenetre', ds.textes.bouton2, afficheCours.bind(null, 'exp'))
    } else {
      j3pAjouteBouton(stor.zoneBtn, stor.bntLn, 'MepBoutonsFenetre', ds.textes.bouton1, afficheCours.bind(null, 'ln'))
    }
    me.cacheBoutonSectionSuivante()
    me.lancerCours = false
  }

  function getDonnees () {
    return {
      // "primaire" || "lycee" || "college"
      // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 7,

      nbetapes: 1,
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,
      ln_seulement: false, // pour ne demander que les limites de la fonction ln
      exp_seulement: false, // pour ne demander que les limites de la fonction exp
      ajout_xlnx: false, // onn peut ajouter la limite de cette fonction en 0
      structure: 'presentation1bis', //  || "presentation2" || "presentation3"  || "presentation1bis"
      // Paramètres de la section, avec leurs valeurs par défaut.
      aide_cours: true,
      textes: {
        titre_exo1: 'Fonction logarithme népérien<br>Limites du cours',
        titre_exo2: 'Fonction exponentielle<br>Limites du cours',
        titre_exo3: 'Fonctions exponentielle et logarithme népérien<br>Limites du cours',
        consigne2: 'Donne la limite suivante issue de ton cours :',
        comment1: 'Ta réponse est imprécise !',
        comment2: 'Passe à la suite !',
        comment3: 'Regarde le cours sur les limites des fonctions logarithme népérien et exponentielle car il n’est pas suffisamment connu.',
        comment4: 'Regarde le cours sur les limites de la fonction logarithme népérien car il n’est pas suffisamment connu.',
        comment5: 'Regarde le cours sur les limites de la fonction exponentielle car il n’est pas suffisamment connu.',
        bouton1: 'Limites de la fonction ln',
        bouton2: 'Limites de la fonction exp',
        aide1: 'Le cours de base',
        aide2: 'Suite'
      },
      limite: 15,
      f1: 'c_limites1',
      f2: 'c_limites2'
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    if (ds.ln_seulement) {
      if (ds.ajout_xlnx) {
        // limite{x}{0}{xln x} est aussi demandée, donc j’ai une étape de plus'
        ds.nbrepetitions = 4
      } else {
        ds.nbrepetitions = 3
      }
      ds.nbitems = ds.nbetapes * ds.nbrepetitions
    } else if (ds.exp_seulement) {
      ds.nbrepetitions = 4
      ds.nbitems = ds.nbetapes * ds.nbrepetitions
    } else {
      if (ds.ajout_xlnx) {
        // limite{x}{0}{xln x} est aussi demandée, donc j’ai une étape de plus'
        ds.nbrepetitions = 8
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
      }
    }
    // ds.limite+=5;
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    // nos alias marchent pas avec les imports dynamiques, faut démarrer avec ./ ou ../
    import('../../../sectionsAnnexes/cours/' + ds.f1 + '.js').then(function ({ default: datasSection }) {
      stor.cours1 = datasSection.partie1
      stor.cours2 = datasSection.partie2
      stor.cours3 = datasSection.partie3
      stor.cours4 = datasSection.partie4
      stor.fonction = datasSection.fonction
      stor.borne_inf = datasSection.borne_inf
      stor.borne_sup = datasSection.borne_sup
      stor.nb_points = datasSection.nb_points
      suite1()
    }).catch(error => {
      console.error(error)
      j3pShowError('Impossible de charger les données de cette exercice')
    })
  }
  function suite1 () {
    import('../../../sectionsAnnexes/cours/' + ds.f2 + '.js').then(function ({ default: datasSection }) {
      stor.coursbis1 = datasSection.partie1
      stor.coursbis2 = datasSection.partie2
      stor.coursbis3 = datasSection.partie3
      stor.fonctionbis = datasSection.fonction
      stor.borne_infbis = datasSection.borne_inf
      stor.borne_supbis = datasSection.borne_sup
      stor.nb_pointsbis = datasSection.nb_points
      suite()
    }).catch(error => {
      console.error(error)
      j3pShowError('Impossible de charger les données de cette exercice')
    })
  }
  function suite () {
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    if (ds.ln_seulement) {
      me.afficheTitre(ds.textes.titre_exo1)
    } else if (ds.exp_seulement) {
      me.afficheTitre(ds.textes.titre_exo2)
    } else {
      me.afficheTitre(ds.textes.titre_exo3)
    }

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
    stor.premiereFois = true
    suite2()
  }
  function suite2 () {
    let i
    let tabLimites
    let tabLimites2
    if (me.questionCourante % ds.nbrepetitions === 1) {
      // on construit un tableau où chaque élément contiendra la fonction, ce vers quoi va tendre x et la limite attendue
      if (ds.ln_seulement) {
        // on ne demande que les limites du cours de la fonction ln
        tabLimites = [['\\ln x', '0', '-\\infty'], ['\\ln x', '+\\infty', '+\\infty']]
        tabLimites2 = [['\\frac{\\ln x}{x}', '+\\infty', '0']]
        if (ds.ajout_xlnx) {
          tabLimites2.push(['x\\ln x', '0', '0'])
        }
      } else if (ds.exp_seulement) {
        // on ne demande que les limites du cours de la fonction exp
        tabLimites = [['\\mathrm{e}^x', '-\\infty', '0'], ['\\mathrm{e}^x', '+\\infty', '+\\infty']]
        tabLimites2 = [['\\frac{\\mathrm{e}^x}{x}', '+\\infty', '+\\infty'], ['x\\mathrm{e}^x', '-\\infty', '0']]
      } else {
        // on demande les limites du cours des fonctions ln et exp
        tabLimites = [['\\ln x', '0', '-\\infty'], ['\\ln x', '+\\infty', '+\\infty'], ['\\mathrm{e}^x', '-\\infty', '0'], ['\\mathrm{e}^x', '+\\infty', '+\\infty']]
        tabLimites2 = [['\\frac{\\ln x}{x}', '+\\infty', '0'], ['\\frac{\\mathrm{e}^x}{x}', '+\\infty', '+\\infty'], ['x\\mathrm{e}^x', '-\\infty', '0']]
        if (ds.ajout_xlnx) {
          tabLimites2.push(['x\\ln x', '0', '0'])
        }
      }
      stor.ordreLimites = j3pShuffle(tabLimites)// pour récupérer l’ordre des limites à demander au cours des différents items'
      const ordreLimites2 = j3pShuffle(tabLimites2)
      for (i = 0; i < ordreLimites2.length; i++) {
        stor.ordreLimites.push(ordreLimites2[i])
      }
    }
    me.logIfDebug('ordreLimites:' + stor.ordreLimites + '   ' + ds.nbetapes)
    const ordreLimites = stor.ordreLimites
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne2)
    const j = (me.questionCourante - 1) % ds.nbrepetitions
    stor.elts = j3pAffiche(stor.zoneCons1, '', '$\\limite{x}{' + ordreLimites[j][1] + '}{' + ordreLimites[j][0] + '}=$&1&',
      {
        inputmq1: { texte: '' }
      })
    stor.laPalette = j3pAddElt(stor.zoneCons3, 'div')
    mqRestriction(stor.elts.inputmqList[0], '\\d,.+-/', { commandes: ['racine', 'fraction', 'puissance', 'inf'] })
    j3pPaletteMathquill(stor.laPalette, stor.elts.inputmqList[0], { liste: ['racine', 'fraction', 'puissance', 'inf'] })
    stor.elts.inputmqList[0].typeReponse = ['texte']
    stor.elts.inputmqList[0].reponse = [ordreLimites[j][2]]
    j3pFocus(stor.elts.inputmqList[0])

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    const mesZonesSaisie = [stor.elts.inputmqList[0].id]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        suite2()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse && (!me.isElapsed)) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien

            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
            stor.laPalette.innerHTML = ''
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.laPalette.innerHTML = ''
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              j3pDesactive(stor.elts.inputmqList[0])
              j3pFreezeElt(stor.elts.inputmqList[0])
              stor.zoneCorr.innerHTML = tempsDepasse
              stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
              me.typederreurs[10]++
              me.cacheBoutonValider()
              me.sectionCourante('navigation')
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (stor.elts.inputmqList[0].reponse[0].indexOf('infty') > -1) {
              // l’infini est présent dans la réponse'
                if (fctsValid.zones.reponseSaisie[0] === '\\infty') stor.zoneCorr.innerHTML += ds.textes.comment1
              }
              // En principe, il n’y a pas de deuxième chance, mais cela peut être surchargé
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                me.typederreurs[2]++
                me.sectionCourante('navigation')
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        if (me.parcours.pe < (ds.nbrepetitions - 1) / ds.nbrepetitions) {
          me.lancerCours = true
          stor.btnAfficheCours = j3pGetNewId('btnAfficheCours')
          j3pAjouteBouton('BoutonsJ3P', stor.btnAfficheCours, 'big suite', ds.textes.aide2, lancementCours)
          j3pElement(stor.btnAfficheCours).focus()
        } else {
          me.afficheBoutonSectionSuivante()
          me.focus('sectioncontinuer')
        }
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      if (me.lancerCours) me.cacheBoutonSectionSuivante()
      break // case "navigation":
  }
}
