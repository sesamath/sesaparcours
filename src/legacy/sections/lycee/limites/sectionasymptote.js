import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pShuffle, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomFloat, j3pGetRandomInt, j3pPaletteMathquill, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        août 2020
        On donne une limite et on demande l’équation de l’asymptote éventuelle
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['casFigure', 'alea', 'liste', 'On peut imposer de n’avoir qu’un type d’asymptote éventuelle en choisissant parmi "verticale" ou "horizontale", sinon, on laisse "alea" (par défaut).', ['alea', 'verticale', 'horizontale']]
  ]

}

/**
 * section asymptote
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let i
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    if (stor.dejaReponduOui) j3pDetruit(stor.zoneCons6)
    // explications contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    for (i = 1; i <= 3; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div', '')
    const corr = (stor.objParam.typeAsymptote === 'verticale')
      ? (stor.admetAsymptote) ? ds.textes.corr1_1 : ds.textes.corr1_2
      : (stor.admetAsymptote) ? ds.textes.corr2_1 : ds.textes.corr2_2
    j3pAffiche(stor.zoneexplication1, '', corr, stor.objParam)
    if ((stor.objParam.typeAsymptote === 'verticale') && (stor.objParam.xlim === stor.objParam.x) && !stor.admetAsymptote) j3pAffiche(stor.zoneexplication2, '', ds.textes.corr1_3, stor.objParam)
    j3pAffiche(stor.zoneexplication3, '', ds.textes.corr3)
    /// /////////////////////////////////////// Pour MathGraph32
    const largFig = 480// on gère ici la largeur de la figure
    const hautFig = 560
    // création du div accueillant la figure mtg32
    stor.figMtg32 = j3pAddElt(stor.zoneCorr, 'div')
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    // et enfin la zone svg (très importante car tout est dedans)
    j3pCreeSVG(stor.figMtg32, { id: stor.mtg32svg, width: largFig, height: hautFig })
    // ce qui suit lance la création initiale de la figure
    depart()
    modifFig()
  }
  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAVZAAADnAAAAQEAAAAAAAAAAQAAAKz#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFjAAItNP####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQBAAAAAAAAD#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAFAAFAbmAAAAAAAEBx4KPXCj1x#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAQAAAAIBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8AAAAAAQ4AAUkAwBgAAAAAAAAAAAAAAAAAAAUAAUBAgAAAAAAAAAAAA#####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAAEAAAAQABAAAAAgAAAAT#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AAAAAAAQAAABAAEAAAACAAAABf####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAIAAAAE#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAYAAAAH#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQUAAQAAAAgAAAALAP####8AAAAAAQ4AAUoAwCgAAAAAAADAEAAAAAAAAAUAAgAAAAj#####AAAAAgAHQ1JlcGVyZQD#####AObm5gABAAAAAgAAAAQAAAAKAQEAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAv#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAL#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAADP####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEBAAAAAAQAAAAN#####wAAAAEACUNMb25ndWV1cgD#####AAAAAgAAAA4AAAACAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAAAgD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABRHcmFkdWF0aW9uQXhlc1JlcGVyZQAAABsAAAAIAAAAAwAAAAsAAAAQAAAAEf####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAABIABWFic29yAAAAC#####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAABIABW9yZG9yAAAACwAAAA0AAAAAEgAGdW5pdGV4AAAAC#####8AAAABAApDVW5pdGV5UmVwAAAAABIABnVuaXRleQAAAAv#####AAAAAQAQQ1BvaW50RGFuc1JlcGVyZQAAAAASAAAAAAAQAAABBQAAAAALAAAAEAAAABMAAAAQAAAAFAAAABcAAAAAEgAAAAAAEAAAAQUAAAAACwAAAA8AAAAAEAAAABMAAAAQAAAAFQAAABAAAAAUAAAAFwAAAAASAAAAAAAQAAABBQAAAAALAAAAEAAAABMAAAAPAAAAABAAAAAUAAAAEAAAABYAAAAOAAAAABIAAAAXAAAAEAAAABAAAAARAAAAABIAAAAAABAAAAEFAAAAABgAAAAaAAAADgAAAAASAAAAFwAAABAAAAARAAAAEQAAAAASAAAAAAAQAAABBQAAAAAZAAAAHP####8AAAABAAhDU2VnbWVudAAAAAASAQAAAAAQAAABAAEAAAAYAAAAGwAAABgAAAAAEgEAAAAAEAAAAQABAAAAGQAAAB0AAAAGAAAAABIBAAAAAAsAAVcAwBQAAAAAAADANAAAAAAAAAUAAT#cVniavN8OAAAAHv####8AAAACAAhDTWVzdXJlWAAAAAASAAZ4Q29vcmQAAAALAAAAIAAAAAIAAAAAEgAFYWJzdzEABnhDb29yZAAAABAAAAAh#####wAAAAIAEkNMaWV1T2JqZXRQYXJQdExpZQEAAAASAGZmZgAAACAAAAAQAAAAEAAAACAAAAACAAAAIAAAACAAAAACAAAAABIABWFic3cyAA0yKmFic29yLWFic3cxAAAADwEAAAAPAgAAAAFAAAAAAAAAAAAAABAAAAATAAAAEAAAACIAAAAXAAAAABIBAAAAABAAAAEFAAAAAAsAAAAQAAAAJAAAABAAAAAUAAAAGgEAAAASAGZmZgAAACUAAAAQAAAAEAAAACAAAAAFAAAAIAAAACEAAAAiAAAAJAAAACUAAAAGAAAAABIBAAAAAAsAAVIAQCAAAAAAAADAIAAAAAAAAAUAAT#RG06BtOgfAAAAH#####8AAAACAAhDTWVzdXJlWQAAAAASAAZ5Q29vcmQAAAALAAAAJwAAAAIAAAAAEgAFb3JkcjEABnlDb29yZAAAABAAAAAoAAAAGgEAAAASAGZmZgAAACcAAAAQAAAAEQAAACcAAAACAAAAJwAAACcAAAACAAAAABIABW9yZHIyAA0yKm9yZG9yLW9yZHIxAAAADwEAAAAPAgAAAAFAAAAAAAAAAAAAABAAAAAUAAAAEAAAACkAAAAXAAAAABIBAAAAABAAAAEFAAAAAAsAAAAQAAAAEwAAABAAAAArAAAAGgEAAAASAGZmZgAAACwAAAAQAAAAEQAAACcAAAAFAAAAJwAAACgAAAApAAAAKwAAACz#####AAAAAgAMQ0NvbW1lbnRhaXJlAAAAABIBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAgCwAB9vr+AAAAAQAAAAAAAAABAAAAAAAAAAAACyNWYWwoYWJzdzEpAAAAGgEAAAASAGZmZgAAAC4AAAAQAAAAEAAAACAAAAAEAAAAIAAAACEAAAAiAAAALgAAABwAAAAAEgFmZmYAAAAAAAAAAABAGAAAAAAAAAAAACULAAH2+v4AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MikAAAAaAQAAABIAZmZmAAAAMAAAABAAAAAQAAAAIAAAAAYAAAAgAAAAIQAAACIAAAAkAAAAJQAAADAAAAAcAAAAABIBZmZmAMAgAAAAAAAAP#AAAAAAAAAAAAAnCwAB9vr+AAAAAgAAAAEAAAABAAAAAAAAAAAACyNWYWwob3JkcjEpAAAAGgEAAAASAGZmZgAAADIAAAAQAAAAEQAAACcAAAAEAAAAJwAAACgAAAApAAAAMgAAABwAAAAAEgFmZmYAwBwAAAAAAAAAAAAAAAAAAAAAACwLAAH2+v4AAAACAAAAAQAAAAEAAAAAAAAAAAALI1ZhbChvcmRyMikAAAAaAQAAABIAZmZmAAAANAAAABAAAAARAAAAJwAAAAYAAAAnAAAAKAAAACkAAAArAAAALAAAADQAAAACAP####8ACGFmZmljaGUxAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAhhZmZpY2hlMgABMQAAAAE#8AAAAAAAAAAAAAIA#####wAIYWZmaWNoZTMAATEAAAABP#AAAAAAAAAAAAACAP####8AAWEAAi0zAAAAAwAAAAFACAAAAAAAAAAAAAIA#####wABYgABMgAAAAFAAAAAAAAAAP####8AAAABAAVDRm9uYwD#####AAJmMQAUKGMvKHgtYSkrYikvYWZmaWNoZTEAAAAPAwAAAA8AAAAADwMAAAAQAAAAAQAAAA8B#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAABAAAAA5AAAAEAAAADoAAAAQAAAANgABeAAAAB0A#####wACZjIAFChjLyh4LWEpK2IpL2FmZmljaGUyAAAADwMAAAAPAAAAAA8DAAAAEAAAAAEAAAAPAQAAAB4AAAAAAAAAEAAAADkAAAAQAAAAOgAAABAAAAA3AAF4AAAAHQD#####AAJmMwAUKGMvKHgtYSkrYikvYWZmaWNoZTMAAAAPAwAAAA8AAAAADwMAAAAQAAAAAQAAAA8BAAAAHgAAAAAAAAAQAAAAOQAAABAAAAA6AAAAEAAAADgAAXgAAAACAP####8ABXJwaXgxAAE2AAAAAUAYAAAAAAAAAAAAFwD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAUAAAAACwAAABAAAAA5AAAAAQAAAAAAAAAA#####wAAAAEADENUcmFuc2xhdGlvbgD#####AAAABAAAAAIAAAARAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAABQAAAAA#AAAAQP####8AAAABAA1DRGVtaURyb2l0ZU9BAP####8AAAAAAA0AAAEAAQAAAD8AAABBAAAABgD#####AQAAAAAQAAJ4MQAAAAAAAAAAAEAIAAAAAAAABQABAAAAAAAAAAAAAABCAAAAGQD#####AAJ4MQAAAAsAAABDAAAAAgD#####AAJ5MQAGZjEoeDEp#####wAAAAEADkNBcHBlbEZvbmN0aW9uAAAAOwAAABAAAABEAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACwAAABAAAABEAAAAEAAAAEX#####AAAAAgANQ0xpZXVEZVBvaW50cwD#####AAAAAAACAAAARgAAAfQAAQAAAEMAAAAEAAAAQwAAAEQAAABFAAAARgAAAB8A#####wAAAAIAAAAEAAAAEQD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAUAAAAAPwAAAEgAAAAgAP####8AAAAAAA0AAAEAAQAAAD8AAABJAAAABgD#####AQAAAAAQAAJ4MgAAAAAAAAAAAEAIAAAAAAAABQABAAAAAAAAAAAAAABKAAAAGQD#####AAJ4MgAAAAsAAABLAAAAAgD#####AAJ5MgAGZjIoeDIpAAAAIQAAADwAAAAQAAAATAAAABcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAsAAAAQAAAATAAAABAAAABNAAAAIgD#####AAAAAAACAAAATgAAAfQAAQAAAEsAAAAEAAAASwAAAEwAAABNAAAATgAAAAYA#####wEAAAAAEAACeDMAAAAAAAAAAABACAAAAAAAAAUAAUAnI2X3u++YAAAABQAAABkA#####wACeDMAAAALAAAAUAAAAAIA#####wACeTMABmYzKHgzKQAAACEAAAA9AAAAEAAAAFEAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAALAAAAEAAAAFEAAAAQAAAAUgAAACIA#####wAAAAAAAgAAAFMAAAH0AAEAAABQAAAABAAAAFAAAABRAAAAUgAAAFP#####AAAAAgAMQ0Ryb2l0ZVBhckVxAP####8AAAD#ABAAAAEAAQAAAAsAAAAPCAAAAB4AAAAAAAAAEAAAADkAAAAPAQAAAB4AAAAAAAAAEAAAADkAAAAGAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQBwAAAAAAAEAAABVAAAAHAD#####AAAA#wEAAABWEAAAAAAAAQAAAAIAAAABAAAAAAAAAAAADyNJeCAjTj0gI1ZhbChhKQAAAAIA#####wAIYWZmaWNoZTQAATEAAAABP#AAAAAAAAAAAAACAP####8AAmEyAAItMgAAAAMAAAABQAAAAAAAAAAAAAACAP####8ABGNvZWYAAi0xAAAAAwAAAAE#8AAAAAAAAAAAAB0A#####wACZjQAKihjKmNvZWYqKHgtYTIpKmxuKGNvZWYqKHgtYTIpKStiKS9hZmZpY2hlNAAAAA8DAAAADwAAAAAPAgAAAA8CAAAADwIAAAAQAAAAAQAAABAAAABaAAAADwEAAAAeAAAAAAAAABAAAABZ#####wAAAAIACUNGb25jdGlvbgYAAAAPAgAAABAAAABaAAAADwEAAAAeAAAAAAAAABAAAABZAAAAEAAAADoAAAAQAAAAWAABeAAAAAYA#####wEAAAAAEAABeAAAAAAAAAAAAEAIAAAAAAAABQABQDy3JldcceAAAAAFAAAAGQD#####AAJ4NAAAAAsAAABcAAAAAgD#####AAJ5NAAGZjQoeDQpAAAAIQAAAFsAAAAQAAAAXQAAABcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAsAAAAQAAAAXQAAABAAAABeAAAAIgD#####AAAAAAACAAAAXwAAB9AAAQAAAFwAAAAEAAAAXAAAAF0AAABeAAAAXwAAAAIA#####wACYTMAEm1pbihhMixhMis1MCpjb2VmKf####8AAAABAA1DRm9uY3Rpb24yVmFyAQAAABAAAABZAAAADwAAAAAQAAAAWQAAAA8CAAAAAUBJAAAAAAAAAAAAEAAAAFoAAAACAP####8AAmE0ABJtYXgoYTIsYTIrNTAqY29lZikAAAAlAAAAABAAAABZAAAADwAAAAAQAAAAWQAAAA8CAAAAAUBJAAAAAAAAAAAAEAAAAFoAAAACAP####8ABXJwaXgyAAE2AAAAAUAYAAAAAAAAAAAAEwD#####ABJDb3VyYmUgZGUgZm9uY3Rpb24AAAAdAAAAAwAAAAUAAAALAAAAWwAAAGEAAABiAAAAY#####8AAAABAAhDRGVyaXZlZQAAAABkAAJmJwAAAFsAAAAXAAAAAGQBAAAAABAAAAEFAAAAAAsAAAAQAAAAYQAAACEAAABbAAAAEAAAAGH#####AAAAAQAJQ0Ryb2l0ZU9tAAAAAGQBAAAAABAAAAEAAQAAAAsAAABmAAAAIQAAAGUAAAAQAAAAYf####8AAAACAAlDQ2VyY2xlT1IAAAAAZAEAAAAAAQAAAGYAAAAQAAAAYwEAAAAKAAAAAGQAAABnAAAAaAAAAAsAAAAAZAEAAAAADQACdzEBBQABAAAAaQAAAAgAAAAAZAEAAAAAEAAAAQABAAAAZgAAAGcAAAAKAAAAAGQAAABrAAAAaAAAAAsAAAAAZAEAAAAAEAAAAQUAAQAAAGwAAAALAAAAAGQBAAAAABAAAAEFAAIAAABsAAAAGQAAAABkAAN4dzEAAAALAAAAagAAABcAAAAAZAEAAAAAEAAAAQUAAAAACwAAABAAAABiAAAAIQAAAFsAAAAQAAAAYgAAACcAAAAAZAEAAAAAEAAAAQABAAAACwAAAHAAAAAhAAAAZQAAABAAAABiAAAAKAAAAABkAQAAAAABAAAAcAAAABAAAABjAQAAAAoAAAAAZAAAAHEAAAByAAAACwAAAABkAQAAAAANAAJ3MgEFAAIAAABzAAAACAAAAABkAQAAAAAQAAABAAEAAABwAAAAcQAAAAoAAAAAZAAAAHUAAAByAAAACwAAAABkAQAAAAAQAAABBQABAAAAdgAAAAsAAAAAZAEAAAAAEAAAAQUAAgAAAHYAAAAZAAAAAGQAA3h3MgAAAAsAAAB0AAAAFwAAAABkAAAAAAAQAAABBQAAAAALAAAAEAAAAG8AAAABAAAAAAAAAAAAAAAXAAAAAGQAAAAAABAAAAEFAAAAAAsAAAAQAAAAeQAAAAEAAAAAAAAAAAAAABgAAAAAZAAAAAAAEAAAAQABAAAAegAAAHsAAAAGAAAAAGQAAAAAAA0AAng0AQUAAT#oVVVVVVVbAAAAfAAAABkAAAAAZAAHeENvb3JkMQAAAAsAAAB9AAAAAgAAAABkAAF4AAd4Q29vcmQxAAAAEAAAAH4AAAACAAAAAGQAAXkABWY0KHgpAAAAIQAAAFsAAAAQAAAAfwAAABcAAAAAZAAAAAAAEAAAAQUAAAAACwAAABAAAAB#AAAAEAAAAIAAAAAiAQAAAGQAAAAAAAEAAACBAAABLAABAAAAfQAAAAUAAAB9AAAAfgAAAH8AAACAAAAAgf####8AAAABAAxDQXJjRGVDZXJjbGUBAAAAZAAAAAAAAQAAAGYAAABtAAAAbgAAACkBAAAAZAAAAAAAAQAAAHAAAAB4AAAAdwAAAAIA#####wAIYWZmaWNoZTUAATEAAAABP#AAAAAAAAAAAAACAP####8AAWQAAzEuMgAAAAE#8zMzMzMzMwAAAAIA#####wABZQABMgAAAAFAAAAAAAAAAAAAAAIA#####wACYjIAATIAAAABQAAAAAAAAAAAAAAdAP####8AAmY1ADYoYyooeF40Lyh4LWIyKSkqc3FydChjb2VmKih4LWIyKSkqZXhwKGQqeCkrZSkvYWZmaWNoZTUAAAAPAwAAAA8AAAAADwIAAAAPAgAAAA8CAAAAEAAAAAEAAAAPA#####8AAAABAApDUHVpc3NhbmNlAAAAHgAAAAAAAAABQBAAAAAAAAAAAAAPAQAAAB4AAAAAAAAAEAAAAIgAAAAkAQAAAA8CAAAAEAAAAFoAAAAPAQAAAB4AAAAAAAAAEAAAAIgAAAAkBwAAAA8CAAAAEAAAAIYAAAAeAAAAAAAAABAAAACHAAAAEAAAAIUAAXgAAAAGAP####8BAAAAABAAAng0AAAAAAAAAAAAQAgAAAAAAAAFAAFAHVgwkn89WwAAAAUAAAAZAP####8AAng1AAAACwAAAIoAAAACAP####8AAnk1AAZmNSh4NSkAAAAhAAAAiQAAABAAAACLAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACwAAABAAAACLAAAAEAAAAIwAAAAiAP####8AAAAAAAIAAACNAAAB9AABAAAAigAAAAQAAACKAAAAiwAAAIwAAACNAAAAIwD#####AAAA#wAQAAABAAEAAAALAAAADwgAAAAeAAAAAQAAABAAAACHAAAADwEAAAAeAAAAAQAAABAAAACHAAAABgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAUAAcAWLoui6LovAAAAjwAAABwA#####wAAAP8BAAAAkBAAAAAAAAEAAAACAAAAAQAAAAAAAAAAAA8jSXkjTiA9ICNWYWwoZSkAAAACAP####8ACGFmZmljaGU2AAExAAAAAT#wAAAAAAAAAAAAAgD#####AAhhZmZpY2hlNwABMQAAAAE#8AAAAAAAAAAAAB0A#####wACZjYAGShjKngqZXhwKGQqeCkrZSkvYWZmaWNoZTYAAAAPAwAAAA8AAAAADwIAAAAPAgAAABAAAAABAAAAHgAAAAAAAAAkBwAAAA8CAAAAEAAAAIYAAAAeAAAAAAAAABAAAACHAAAAEAAAAJIAAXgAAAAGAP####8BAAAAABAAAng1AAAAAAAAAAAAQAgAAAAAAAAFAAFAF+nxmbPY#wAAAAUAAAAZAP####8AAng2AAAACwAAAJUAAAACAP####8AAnk2AAZmNih4NikAAAAhAAAAlAAAABAAAACWAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACwAAABAAAACWAAAAEAAAAJcAAAAiAP####8AAAAAAAIAAACYAAAB9AABAAAAlQAAAAQAAACVAAAAlgAAAJcAAACYAAAAAgD#####AAJjMgABMQAAAAE#8AAAAAAAAAAAAAIA#####wACYzMAATEAAAABP#AAAAAAAAAAAAAGAP####8BAAAAABAAAng2AAAAAAAAAAAAQAgAAAAAAAAFAAFAKdBmuyqf1gAAAAUAAAAZAP####8AAng3AAAACwAAAJwAAAACAP####8ACGFmZmljaGU4AAExAAAAAT#wAAAAAAAAAAAAAgD#####AAJjNAABMgAAAAFAAAAAAAAAAAAAAAIA#####wAFY29lZjIAAi0xAAAAAwAAAAE#8AAAAAAAAAAAAB0A#####wACZjcALGNvZWYyKjAuMiooeC1jMikqZXhwKGNvZWYqMC40KngtYzMpL2FmZmljaGU3AAAADwMAAAAPAgAAAA8CAAAADwIAAAAQAAAAoAAAAAE#yZmZmZmZmgAAAA8BAAAAHgAAAAAAAAAQAAAAmgAAACQHAAAADwEAAAAPAgAAAA8CAAAAEAAAAFoAAAABP9mZmZmZmZoAAAAeAAAAAAAAABAAAACbAAAAEAAAAJMAAXgAAAACAP####8AAnk3AAZmNyh4NykAAAAhAAAAoQAAABAAAACdAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACwAAABAAAACdAAAAEAAAAKIAAAAiAP####8AAAAAAAIAAACjAAAB9AABAAAAnAAAAAQAAACcAAAAnQAAAKIAAACjAAAAAgD#####AAVjb2VmMwABMQAAAAE#8AAAAAAAAAAAAB0A#####wACZjgANGNvZWYyL3NxcnQoY29lZjMqKHgtYzIpKSpleHAoY29lZiowLjQqeC1jMykvYWZmaWNoZTgAAAAPAwAAAA8CAAAADwMAAAAQAAAAoAAAACQBAAAADwIAAAAQAAAApQAAAA8BAAAAHgAAAAAAAAAQAAAAmgAAACQHAAAADwEAAAAPAgAAAA8CAAAAEAAAAFoAAAABP9mZmZmZmZoAAAAeAAAAAAAAABAAAACbAAAAEAAAAJ4AAXgAAAAGAP####8BAAAAABAAAng3AAAAAAAAAAAAQAgAAAAAAAAFAAFAPT07+YrGzAAAAAUAAAAZAP####8AAng4AAAACwAAAKcAAAACAP####8AAnk4AAZmOCh4OCkAAAAhAAAApgAAABAAAACoAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACwAAABAAAACoAAAAEAAAAKkAAAAiAP####8AAAAAAAIAAACqAAAB9AABAAAApwAAAAQAAACnAAAAqAAAAKkAAACqAAAAD###########'
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
  }
  function modifFig () {
    // cette fonction sert à modifier la figure de base
    // num est le numéro de la question (1 ou 3) selon qu’on cherche la valeur finale ou la valeur initiale
    for (let k = 1; k <= 8; k++) {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche' + k, '0')
    }
    let c
    let coef = (stor.objParam.d.indexOf('-\\infty') > -1) ? -1 : 1
    let coef2
    if (stor.objParam.typeAsymptote === 'verticale') {
      c = j3pGetRandomFloat(0.5, 1.5)
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'e', String(50))
      if (stor.admetAsymptote) {
        // Dans ce cas de figure, j’ai mis une fonction de la forme 1/(x-a)+b
        if (stor.objParam.d.indexOf('\\R') > -1) {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche3', '1')
          if (((stor.objParam.xlim.charAt(stor.objParam.xlim.length - 1) === '-') && (stor.objParam.l === '+\\infty')) || ((stor.objParam.xlim.charAt(stor.objParam.xlim.length - 1) === '+') && (stor.objParam.l === '-\\infty'))) {
            c = -c
          }
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(j3pGetRandomFloat(-2, 2)))
        } else if (stor.objParam.d.indexOf('-\\infty') > -1) {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche1', '1')
          if (stor.objParam.l === '+\\infty') c = -c
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(j3pGetRandomFloat(0.5, 2)))
        } else {
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche2', '1')
          if (stor.objParam.l === '-\\infty') c = -c
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(j3pGetRandomFloat(-2, -0.5)))
        }
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'c', String(c))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(stor.objParam.x))
      } else {
        // fonction de la forme c*coef*(x-a2)*ln(coef*(x-a2))+b
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche4', '1')
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(50))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a2', String(stor.objParam.x))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(stor.objParam.l))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'c', String((j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomFloat(0.8, 2.8)))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'coef', String(coef))
      }
    } else {
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(50))
      if (stor.admetAsymptote) {
        c = j3pGetRandomFloat(0.2, 0.8)
        let d = j3pGetRandomFloat(1, 1.5)
        if (stor.objParam.x === '+\\infty') d = -d
        if (stor.objParam.d === '\\R') {
          // fonction de la forme (c*x*exp(d*x)+e)/affiche6
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche6', '1')
        } else {
          // fonction de la forme (c*(x^2/(x-b2))*sqrt(coef*(x-b2))*exp(d*x)+e)/affiche5  (sqrt est pour le domaine)
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche5', '1')
        }
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'd', String(d))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'c', String(c))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'e', String(stor.objParam.l))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b2', String(stor.objParam.borne))
        me.logIfDebug('c:', c, 'd:', d, 'coef:', coef)
      } else {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'e', String(50))
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'c3', String(j3pGetRandomFloat(-2, 2)))
        coef = 1
        coef2 = 1
        let coef3 = 1
        if (stor.objParam.x === '-\\infty') coef = -1
        if (stor.objParam.d === '\\R') {
          // fonction de la forme coef2*(x-c2)*exp(coef*0.1*x-c3)/affiche7
          if (stor.objParam.l === '+\\infty') coef2 = -1
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche7', '1')
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'c2', String(j3pGetRandomFloat(-2, 2)))
        } else {
          // fonction de la forme coef2/sqrt(coef3*(x-c2))*exp(coef*0.4*x-c3)/affiche8
          if (stor.objParam.l === '-\\infty') coef2 = -1
          if (stor.objParam.d.indexOf('-\\infty') > -1) coef3 = -1
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche8', '1')
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'c2', String(stor.objParam.borne))
          stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'coef3', String(coef3))
        }
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'coef2', String(coef2))
      }
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'coef', String(coef))
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }
  function derniereConsigne () {
    if (!stor.dejaReponduOui) {
      stor.dejaReponduOui = true
      for (let j = 4; j <= 6; j++) stor['zoneCons' + j] = j3pAddElt(stor.repOui, 'div', '')
      j3pAffiche(stor.zoneCons4, '', ds.textes.consigne3)
      stor.elt = j3pAffiche(stor.zoneCons5, '', '&1&', { inputmq1: { texte: '' } })
      const mesZonesSaisie = [stor.elt.inputmqList[0].id]
      j3pPaletteMathquill(stor.zoneCons6, stor.elt.inputmqList[0], { liste: ['inf'] })
      stor.zoneCons6.style.paddingTop = '10px'
      stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie, validePerso: mesZonesSaisie })
      mqRestriction(stor.elt.inputmqList[0], 'xy=+-\\d')
      j3pFocus(stor.elt.inputmqList[0])
    }
  }
  function sansDerniereConsigne () {
    if (stor.dejaReponduOui) {
      j3pEmpty(stor.repOui)
      stor.dejaReponduOui = false
    }
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.
      casFigure: 'alea',
      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Asymptote à partir d’une limite',
        // on donne les phrases de la consigne
        consigne1: '$£f$ est une fonction définie sur $£d$ vérifiant $\\limite{x}{£{xlim}}{£f(x)}=£l$.',
        consigne2: 'Cette limite permet-elle d’affirmer que la courbe représentative de $£f$ admet une asymptote&nbsp;?',
        consigne3: 'Dans ce cas l’équation réduite de l’asymptote est&nbsp;:',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: 'On demande une équation de droite&nbsp;!',
        comment2: 'L’équation proposée n’est pas une équation réduite de droite&nbsp;!',
        cocher1: 'oui',
        cocher2: 'non',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: '$£x$ est un réel, $£f(x)$ tendant vers une infinité en $£x$, cela implique que la courbe représentant $£f$ admet une asymptote verticale d’équation $x=£x$.',
        corr1_2: '$£x$ est un réel, $£f(x)$ ne tendant pas vers une infinité en $£x$, cela implique que cette limite ne permet d’affirmer que la courbe représentant $£f$ admet une asymptote.',
        corr1_3: 'Par contre il est certain que la droite d’équation $x=£x$ ne peut être une asymptote à cette courbe.',
        corr2_1: '$£f(x)$ tendant vers la constante réelle $£l$ en $£x$, cela implique que la courbe représentant $£f$ admet une asymptote horizontale d’équation $y=£l$ en $£x$.',
        corr2_2: '$£f(x)$ ne tendant pas vers une constante réelle en $£x$ ($£l$ dans ce cas de figure), cela implique que cette limite ne permet d’affirmer que la courbe représentant $£f$ admet une asymptote. Par contre il est certain que la courbe n’admet pas d’asymptote horizontale en $£x$.',
        corr3: 'Voici ci-contre un cas de figure représentant cette situation.'
      },
      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    // Je choisis les cas de figures
    stor.tabCasFigure = []
    let i
    if (ds.casFigure === 'alea') {
      for (i = 0; i < Math.floor(ds.nbrepetitions / 2); i++) {
        stor.tabCasFigure.push('verticale', 'horizontale')
      }
      if (ds.nbrepetitions % 2 !== 0) {
        stor.tabCasFigure.push(j3pGetRandomElt(['verticale', 'horizontale']))
      }
      stor.tabCasFigure = j3pShuffle(stor.tabCasFigure)
    } else {
      for (i = 0; i < ds.nbrepetitions; i++) {
        stor.tabCasFigure.push(ds.casFigure)
      }
    }
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    let i
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('petit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.admetAsymptote = j3pGetRandomInt(1, 4) > 1 // 1 chance sur 4 de ne pas avoir d’asymptote
    stor.objParam = { typeAsymptote: stor.tabCasFigure[me.questionCourante - 1] }
    let choixDomaine = j3pGetRandomInt(1, 3)
    if (stor.objParam.typeAsymptote === 'verticale') {
      stor.objParam.x = j3pGetRandomInt(-4, 4)
      do {
        stor.objParam.l = stor.admetAsymptote
          ? j3pGetRandomBool() ? '+\\infty' : '-\\infty'
          : j3pGetRandomInt(-6, 6)
      } while (stor.objParam.x === stor.objParam.l)
      if (!stor.admetAsymptote) choixDomaine = j3pGetRandomInt(2, 3)
      stor.objParam.d = (choixDomaine === 1)
        ? '\\R\\setminus\\bracket{' + stor.objParam.x + '}'
        : (choixDomaine === 2) ? ']-\\infty;' + stor.objParam.x + '[' : ']' + stor.objParam.x + ';+\\infty['
    } else {
      stor.objParam.x = j3pGetRandomBool() ? '+\\infty' : '-\\infty'
      do {
        stor.objParam.l = stor.admetAsymptote
          ? j3pGetRandomInt(-4, 4)
          : j3pGetRandomBool() ? '+\\infty' : '-\\infty'
      } while (stor.objParam.x === stor.objParam.l)
      do {
        stor.objParam.borne = j3pGetRandomInt(-2, 2)
      } while ((stor.objParam.borne === stor.objParam.l) || (stor.objParam.borne === stor.objParam.x))
      stor.objParam.d = (choixDomaine === 1)
        ? '\\R'
        : (stor.objParam.x === '+\\infty') ? ']' + stor.objParam.borne + ';+\\infty[' : ']-\\infty;' + stor.objParam.borne + '['
    }
    stor.objParam.xlim = stor.objParam.x
    if (stor.objParam.typeAsymptote === 'verticale' && choixDomaine === 1) {
      // Domaine égal à R privé de x, donc pour la limite, il faut préciser à gauche ou à droite
      stor.objParam.xlim = stor.objParam.x + (j3pGetRandomBool ? '^+' : '^-')
    }
    stor.objParam.f = j3pGetRandomElt(['f', 'g', 'h'])
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, stor.objParam)
    j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2, stor.objParam)
    const espacetxt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
    j3pAffiche(stor.zoneCons3, '', espacetxt)
    stor.nameRadio = j3pGetNewId('choix')
    stor.idsRadio = ['1', '2'].map(i => j3pGetNewId('radio' + i))
    j3pBoutonRadio(stor.zoneCons3, stor.idsRadio[0], stor.nameRadio, 0, ds.textes.cocher1)
    j3pAffiche(stor.zoneCons3, '', espacetxt + espacetxt)
    j3pBoutonRadio(stor.zoneCons3, stor.idsRadio[1], stor.nameRadio, 1, ds.textes.cocher2)
    stor.repOui = j3pAddElt(stor.conteneur, 'div')
    j3pElement(stor.idsRadio[0]).onclick = derniereConsigne
    j3pElement(stor.idsRadio[1]).onclick = sansDerniereConsigne
    stor.dejaReponduOui = false
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '10px' }) })

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        let reponse = { aRepondu: false, bonneReponse: false }
        const choixRadio = j3pBoutonRadioChecked(stor.nameRadio)
        reponse.aRepondu = (choixRadio[0] > -1)
        if (choixRadio[0] === 0) reponse = stor.fctsValid.validationGlobale()
        let bonChoixRadio, egalPresent, eqReduite
        if (reponse.aRepondu) {
          bonChoixRadio = ((choixRadio[0] === 0) && stor.admetAsymptote) || ((choixRadio[0] === 1) && !stor.admetAsymptote)
          let bonneEquation = true

          // console.log('bonChoixRadio:', bonChoixRadio, 'choixRadio:', choixRadio, 'stor.admetAsymptote:', stor.admetAsymptote)
          if (bonChoixRadio) {
            if (stor.admetAsymptote) {
            // Le plus délicat est ici car il faut vérifier que l’équation écrite est la bonne.
              const repSaisie = stor.fctsValid.zones.reponseSaisie[0]
              const tabRepSaisie = repSaisie.split('=')
              egalPresent = (tabRepSaisie.length === 2 && tabRepSaisie[0] !== '' && tabRepSaisie[1] !== '')
              if (egalPresent) {
              // liste des réponses sous forme d’équations réduites
                const listeEqReduites = [
                  /[xy]=-?[0-9]+/g,
                  /-?[0-9]+=[xy]/g,
                  /[xy][+-][0-9]+=0/g,
                  /-?[0-9]+[+-][xy]=0/g,
                  /0=[xy][+-][0-9]+/g,
                  /0=-?[0-9]+[+-][xy]/g]
                eqReduite = false
                const lavar = (stor.objParam.typeAsymptote === 'verticale') ? 'x' : 'y'
                const lavaleur = (stor.objParam.typeAsymptote === 'verticale') ? stor.objParam.x : stor.objParam.l
                bonneEquation = false
                for (let k = 0; k < listeEqReduites.length; k++) {
                  if (listeEqReduites[k].test(repSaisie) && (repSaisie.replace(listeEqReduites[k], '') === '')) {
                  // On a une équation réduite, on vérifie si c’est la bonne
                    eqReduite = true
                    // console.log('eqReduite:', eqReduite, k)
                    switch (k) {
                      case 0:
                      // x=k ou y=k
                        bonneEquation = (repSaisie === lavar + '=' + lavaleur)
                        break
                      case 1:
                      // k=x ou k=y
                        bonneEquation = (repSaisie === lavaleur + '=' + lavar)
                        break
                      case 2:
                      // x-k=0 ou y-k=0
                        bonneEquation = (lavaleur === 0)
                          ? (repSaisie === lavar + '=0')
                          : (lavaleur > 0) ? (repSaisie === lavar + '-' + lavaleur + '=0') : (repSaisie === lavar + '+' + -lavaleur + '=0')
                        break
                      case 3:
                      // -k+x=0 ou -k+y=0 ou k-x=0 ou k-y=0
                        bonneEquation = (lavaleur === 0)
                          ? (repSaisie === lavar + '=0')
                          : (lavaleur > 0) ? (repSaisie === '-' + lavaleur + '+' + lavar + '=0') : (repSaisie === -lavaleur + '+' + lavar + '=0')
                        bonneEquation = bonneEquation || ((lavaleur === 0)
                          ? (repSaisie === '0=' + lavar)
                          : (lavaleur > 0) ? (repSaisie === lavaleur + '-' + lavar + '=0') : (repSaisie === lavaleur + '-' + lavar + '=0'))
                        break
                      case 4:
                      // 0=x-k ou y-k=0
                        bonneEquation = (lavaleur === 0)
                          ? (repSaisie === '0=' + lavar)
                          : (lavaleur > 0) ? (repSaisie === '0=' + lavar + '-' + lavaleur) : (repSaisie === '0=' + lavar + '+' + -lavaleur)
                        break
                      case 5:
                      // 0=-k+x ou 0=-k+y ou 0=k-x ou 0=k-y
                        bonneEquation = (lavaleur === 0)
                          ? (repSaisie === '0=' + lavar)
                          : (lavaleur > 0) ? (repSaisie === '0=-' + lavaleur + '+' + lavar) : (repSaisie === '0=' + -lavaleur + '+' + lavar)
                        bonneEquation = bonneEquation || ((lavaleur === 0)
                          ? (repSaisie === '0=' + lavar)
                          : (lavaleur > 0) ? (repSaisie === '0=' + lavaleur + '-' + lavar) : (repSaisie === '0=' + lavaleur + '-' + lavar))
                        break
                    }
                  }
                }
              } else {
                bonneEquation = false
              }
              stor.fctsValid.zones.bonneReponse[0] = bonneEquation
              stor.fctsValid.coloreUneZone(stor.elt.inputmqList[0].id)
            }
          } else {
            me.essaiCourant = ds.nbchances
            if (choixRadio[0] === 0) {
              stor.fctsValid.zones.bonneReponse[0] = false
              stor.fctsValid.coloreUneZone(stor.elt.inputmqList[0].id)
            }
            j3pElement('label' + stor.idsRadio[choixRadio[0]]).style.color = me.styles.cfaux
            j3pElement('label' + stor.idsRadio[(choixRadio[0] + 1) % 2]).style.color = me.styles.toutpetit.correction.color
          }
          reponse.bonneReponse = bonChoixRadio && bonneEquation
        }
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            stor.idsRadio.forEach(elt => j3pDesactive(elt))
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (bonChoixRadio) {
                if (!egalPresent) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
                } else if (!eqReduite) {
                  stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
                }
              }
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                stor.idsRadio.forEach(elt => j3pDesactive(elt))
                if (!bonChoixRadio) j3pBarre('label' + stor.idsRadio[choixRadio[0]])
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
