import { j3pAddElt, j3pShuffle, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pPaletteMathquill, j3pStyle, j3pAjouteBouton, j3pDetruit, j3pGetNewId } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pDetruitFenetres, j3pAfficheCroixFenetres } from 'src/legacy/core/functionsJqDialog'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Rémi DENIAUD
    mai 2014
    cette section permet de se tester sur des limites avec la fonctin ln
    la connaissance des limites du cours suffit pour répondre
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['tab_choix', [1, 2, 3, 4, 5], 'array', 'tableau contenant la liste des limites demandées (on peut en mettre moins voire en doubler certains, elles resteront par contre dans cet ordre) :<br>1 pour 1/ln(x) sur ]0;1[,<br>2 pour 1/ln(x) sur ]1;+infini[,<br>3 pour x/ln(x) sur ]0;1[,<br>4 pour x/ln(x) sur ]1;+infini[,<br>5 pour ln(1/x) sur ]0;+infini[']

  ]

}

/**
 * section limites_ln2
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function purge (tableauChoix) {
    // cette fonction va me permettre de prévenir un éventuel mauvais remplissage du tableau sur le choix des limites
    const tableauPurge = []
    for (let j = 0; j < tableauChoix.length; j++) {
      if (!isNaN(Number(tableauChoix[j]))) {
        if ((tableauChoix[j] === 1) || (tableauChoix[j] === 2) || (tableauChoix[j] === 3) || (tableauChoix[j] === 4) || (tableauChoix[j] === 5)) {
          tableauPurge.push(tableauChoix[j])
        }
      }
    }
    // à cet instant si tableau_epure est vide, je lui mets un élément : 1;
    if (tableauPurge.length === 0) {
      tableauPurge.push(1)
    }
    return tableauPurge
  }
  function aide (num) {
    // num correspond au numéro de la limite demandée (dans le tableau tab_choix)
    // 1 pour 1/ln(x) sur ]0;1[, 2 pour 1/ln(x) sur ]1;+infini[, 3 pour x/ln(x) sur ]0;1[, 4 pour x/ln(x) sur ]1;+infini[, 5 pour ln(1/x) sur ]0;+infini[
    const titreAide = ((num === 3) || (num === 4)) ? ds.textes.aidebis : ds.textes.aide
    stor.fenetreAide = j3pGetNewId('fenetreAide')
    stor.fenetreCourbe = j3pGetNewId('fenetreCourbe')
    me.fenetresjq = [{ name: 'Aide', title: titreAide, width: Number(me.zonesElts.MG.offsetWidth), left: 50, top: 50, id: stor.fenetreAide },
      { name: 'Courbe', title: ds.textes.courbe, top: me.zonesElts.HD.offsetHeight + 70, left: me.zonesElts.MG.offsetWidth + 10, width: 280, id: stor.fenetreCourbe }]
    j3pCreeFenetres(me)
    j3pCreeFenetres(me)
    j3pAjouteBouton(stor.zoneBtn, 'Aide', 'MepBoutons', titreAide, j3pToggleFenetres.bind(null, 'Aide'))
    j3pAfficheCroixFenetres('Aide')
    stor.coursDebut = j3pAddElt(stor.fenetreAide, 'div')
    let monAide
    let monFI
    monFI = ds.textes.FI1
    if (num === 4) {
      monFI = ds.textes.FI2
    }
    if ((num === 3) || (num === 4)) {
      monAide = ds.textes.aide2
    } else {
      monAide = ds.textes.aide1
    }
    j3pAffiche(stor.coursDebut, '', monFI + '\n' + monAide)
  }
  function creationCourbe () {
    // création de la courbe :
    stor.courbeCours = j3pAddElt(stor.fenetreCourbe, 'div')
    stor.idRepere = j3pGetNewId('unrepere')
    if (stor.numQuest === 1) {
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 240,
        haut: 400,

        pasdunegraduationX: 1,
        pixelspargraduationX: 100,
        pasdunegraduationY: 1,
        pixelspargraduationY: 20,
        xO: 20,
        yO: 100,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: 0.000001, par3: 0.999, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else if (stor.numQuest === 2) {
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 240,
        haut: 300,
        pasdunegraduationX: 1,
        pixelspargraduationX: 30,
        pasdunegraduationY: 1,
        pixelspargraduationY: 20,
        xO: 20,
        yO: 200,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: 1.0001, par3: 10, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else if (stor.numQuest === 3) {
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 240,
        haut: 300,
        pasdunegraduationX: 1,
        pixelspargraduationX: 100,
        pasdunegraduationY: 1,
        pixelspargraduationY: 20,
        xO: 20,
        yO: 100,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: 0.01, par3: 0.999, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else if (stor.numQuest === 4) {
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 240,
        haut: 300,
        pasdunegraduationX: 1,
        pixelspargraduationX: 20,
        pasdunegraduationY: 1,
        pixelspargraduationY: 20,
        xO: 20,
        yO: 200,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: 1.01, par3: 15, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    } else {
      me.repere = new Repere({
        idConteneur: stor.courbeCours,
        idDivRepere: stor.idRepere,
        aimantage: true,
        visible: true,
        trame: true,
        fixe: true,
        larg: 240,
        haut: 300,

        pasdunegraduationX: 5,
        pixelspargraduationX: 40,
        pasdunegraduationY: 1,
        pixelspargraduationY: 30,
        xO: 20,
        yO: 150,
        debuty: 0,
        negatifs: true,
        objets: [
          { type: 'courbe', nom: 'c1', par1: stor.fonction, par2: 0.0001, par3: 40, par4: 300, style: { couleur: '#F00', epaisseur: 2 } }
        ]
      })
    }
    me.repere.construit()
    j3pElement(stor.idRepere).style.position = 'relative'
  }
  function ecoute (eltInput) {
    if (eltInput.className.indexOf('mq-editable-field') > -1) {
      j3pEmpty(stor.laPalette)
      j3pPaletteMathquill(stor.laPalette, eltInput, { liste: ['racine', 'fraction', 'puissance', 'inf'] })
    }
  }
  function afficheCorrection (num, bonneRep) {
    j3pDetruit(stor.zoneBtn)
    j3pStyle(stor.zoneExpli, { color: (bonneRep) ? me.styles.cbien : me.styles.petit.correction.color })
    for (let i = 1; i <= 5; i++) stor['zoneExpli' + i] = j3pAddElt(stor.zoneExpli, 'div')
    j3pAffiche(stor.zoneExpli1, '', ds.textes.corr0)
    if (num === 1) {
      // \frac{1}{ln x}
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr1,
        {
          f: stor.nomFct
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr2,
        {
          f: stor.nomFct
        })
    } else if (num === 2) {
      // \frac{1}{ln x}
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr4,
        {
          f: stor.nomFct
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr5,
        {
          f: stor.nomFct
        })
    } else if (num === 3) {
      // \frac{x}{ln x}
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr6,
        {
          f: stor.nomFct
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr7,
        {
          f: stor.nomFct
        })
    } else if (num === 4) {
      // \frac{x}{ln x}
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr8,
        {
          f: stor.nomFct
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr9,
        {
          f: stor.nomFct
        })
    } else if (num === 5) {
      // \\ln \\frac{1}{x}
      j3pAffiche(stor.zoneExpli2, '', ds.textes.corr10,
        {
          f: stor.nomFct
        })
      j3pAffiche(stor.zoneExpli3, '', ds.textes.corr11,
        {
          f: stor.nomFct
        })
    }
    j3pAffiche(stor.zoneExpli4, '', ds.textes.corr3)
    creationCourbe()
    j3pToggleFenetres('Courbe')
  }
  function getDonnees () {
    return {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
      typesection: 'lycee', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 5,
      nbetapes: 1,
      indication: '',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,
      tab_choix: [1, 2, 3, 4, 5],
      structure: 'presentation1bis', //  || "presentation2" || "presentation3"  || "presentation1bis"
      // Paramètres de la section, avec leurs valeurs par défaut.
      // aide_cours: true;
      textes: {
        titre_exo: 'Fonction logarithme népérien<br>Calcul de limites (2)',
        consigne1: 'Soit $£f$ la fonction définie sur $£i$ par :<br>$£f(x)=£g$.',
        consigne2: 'Déterminer les limites suivantes.',
        consigne3: '$£l=$&1& <br>et $£m=$&2&',
        comment1: 'Ta réponse est imprécise !',
        comment2: 'Passe à la suite !',
        aide: 'Indication',
        aidebis: 'Rappel de cours et méthode',
        FI1: 'Il n’y a pas de forme indéterminée.',
        FI2: 'Il y a une forme indéterminée en $+\\infty$, mais pas en $1$.',
        aide1: 'Ne pas oublier les limites du cours : $\\limite{x}{0}{\\ln x}=-\\infty$ et $\\limite{x}{+\\infty}{\\ln x}=+\\infty$',
        aide2: 'Ne pas oublier les limites du cours : $\\limite{x}{0}{\\ln x}=-\\infty$ et $\\limite{x}{+\\infty}{\\ln x}=+\\infty$.<br>De plus $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$ : on dit que $x$ "l’emporte" sur $\\ln x$ en $+\\infty$.<br>',
        // limite de 1/lnx sur ]0;1[
        courbe: 'Courbe représentative de la fonction',
        corr0: 'Voici la justification qui serait attendue :',
        corr1: 'En 0 : $\\limite{x}{0}{\\ln x}=-\\infty$.<br> Or $£f(x)$ est l’inverse de $\\ln x$ et $\\limite{X}{+\\infty}{\\frac{1}{X}}=0$. Donc $\\limite{x}{0}{£f(x)}=0$.',
        corr2: 'En $1$ : $\\limite{x}{1^-}{\\ln x}=0^-$ (sur $]0;1[$, $\\ln x<0$).<br> Or $\\limite{X}{0^-}{\\frac{1}{X}}=-\\infty$. Donc $\\limite{x}{1}{£f(x)}=-\\infty$.',
        corr3: 'On peut aussi vérifier la réponse sur la courbe représentative de la fonction.',
        // limite de 1/lnx sur ]1;+\\infty[
        corr4: 'En $1$ : $\\limite{x}{1^+}{\\ln x}=0^+$ (sur $]1;+\\infty[$, $\\ln x>0$).<br> Or $£f(x)$ est l’inverse de $\\ln x$ et $\\limite{X}{0^+}{\\frac{1}{X}}=+\\infty$. Donc $\\limite{x}{1}{£f(x)}=+\\infty$.',
        corr5: 'En $+\\infty$ : $\\limite{x}{+\\infty}{\\ln x}=+\\infty$.<br>Or $\\limite{X}{+\\infty}{\\frac{1}{X}}=0$. Donc $\\limite{x}{+\\infty}{£f(x)}=0$.',
        // limite de x/lnx sur ]0;1[
        corr6: 'En 0 : $£f(x)=x\\times \\frac{1}{\\ln x}$. Or $\\limite{x}{0}{\\ln x}=-\\infty$ d’où $\\limite{x}{0}{\\frac{1}{\\ln x}}=0$.<br>Ainsi $\\limite{x}{0}{£f(x)}=0$.',
        corr7: 'En 1 : $\\limite{x}{1^-}{\\ln x}=0^-$ ($\\ln x<0$ sur $]0;1[$). Donc $\\limite{x}{1}{£f(x)}=-\\infty$.',
        // limite de x/lnx sur ]1;+\\infty[
        corr8: 'En 1 : $\\limite{x}{1^+}{\\ln x}=0^+$. Donc $\\limite{x}{1}{£f(x)}=+\\infty$.',
        corr9: 'En $+\\infty$, $\\limite{x}{+\\infty}{\\frac{\\ln x}{x}}=0$. Or $£f(x)$ est l’inverse de $\\frac{\\ln x}{x}$ et $\\limite{X}{0^+}{\\frac{1}{X}}=+\\infty$.<br>Donc $\\limite{x}{+\\infty}{£f(x)}=+\\infty$.',
        // limite de ln(1/x) sur ]0;+\\infty[
        corr10: 'En 0 : $\\limite{x}{0}{\\frac{1}{x}}=+\\infty$ et $\\limite{X}{+\\infty}{\\ln X}=+\\infty$.<br>Donc $\\limite{x}{0}{£f(x)}=+\\infty$.',
        corr11: 'En $+\\infty$ : $\\limite{x}{+\\infty}{\\frac{1}{x}}=0$ et $\\limite{X}{0}{\\ln X}=-\\infty$.<br>Donc $\\limite{x}{+\\infty}{£f(x)}=-\\infty$.'
      },
      pe: 0
    }
  }

  function enonceMain () {
    let i
    let fDeX
    let domaineDef
    let borneInf
    let borneSup
    let limiteRep1// réponse pour la limite en 0
    let limiteRep2// réponse pour la limite en +\\infty
    if ((me.questionCourante - 1) % ds.nbrepetitions === 0) {
      // on construit un tableau où chaque élément contiendra la fonction, ce vers quoi va tendre x et la limite attendue
      stor.tab_quest_ordonne = j3pShuffle(ds.tab_choix)
    }
    stor.tab_quest_ordonne.sort()
    me.logIfDebug('tab_quest_ordonne:' + stor.tab_quest_ordonne)
    const j = (me.questionCourante - 1) % ds.nbrepetitions
    stor.numQuest = stor.tab_quest_ordonne[j]
    if (stor.numQuest <= 2) {
      // fonction 1/ln x
      fDeX = '\\frac{1}{\\ln x}'
      if (stor.numQuest === 1) {
        domaineDef = ']0;1['
        borneInf = '0'
        borneSup = '1'
        limiteRep1 = '0'
        limiteRep2 = '-\\infty'
      } else {
        domaineDef = ']1;+\\infty['
        borneInf = '1'
        borneSup = '+\\infty'
        limiteRep1 = '+\\infty'
        limiteRep2 = '0'
      }
      stor.fonction = '1/ln(x)'
    } else if (stor.numQuest <= 4) {
      // fonction x/ln x
      fDeX = '\\frac{x}{\\ln x}'
      if (stor.numQuest === 3) {
        domaineDef = ']0;1['
        borneInf = '0'
        borneSup = '1'
        limiteRep1 = '0'
        limiteRep2 = '-\\infty'
      } else {
        domaineDef = ']1;+\\infty['
        borneInf = '1'
        borneSup = '+\\infty'
        limiteRep1 = '+\\infty'
        limiteRep2 = '+\\infty'
      }
      stor.fonction = 'x/ln(x)'
    } else {
      // fonction ln (1/x)
      fDeX = '\\ln \\frac{1}{x}'
      domaineDef = ']0;+\\infty['
      borneInf = '0'
      borneSup = '+\\infty'
      limiteRep1 = '+\\infty'
      limiteRep2 = '-\\infty'
      stor.fonction = 'ln(1/x)'
    }
    stor.limiteRep1 = limiteRep1
    stor.limiteRep2 = limiteRep2
    const tabNomFct = ['f', 'g', 'h']
    const nomFct = tabNomFct[j3pGetRandomInt(0, 2)]
    stor.nomFct = nomFct// pour être réutilisé dans la correction
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '13px' }) })
    for (i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1,
      {
        f: nomFct,
        g: fDeX,
        i: domaineDef
      })

    j3pAffiche(stor.zoneCons2, '', ds.textes.consigne2)
    stor.elts = j3pAffiche(stor.zoneCons3, '', ds.textes.consigne3,
      {
        l: '\\limite{x}{' + borneInf + '}{' + nomFct + '(x)}',
        m: '\\limite{x}{' + borneSup + '}{' + nomFct + '(x)}',
        inputmq1: { texte: '' },
        inputmq2: { texte: '' }
      })
    for (let i = 1; i <= 2; i++) mqRestriction(stor.elts.inputmqList[i - 1], '\\d+-,./^', { commandes: ['racine', 'fraction', 'puissance', 'inf'] })

    stor.laPalette = j3pAddElt(stor.zoneCons4, 'div')
    // palette MQ :
    j3pPaletteMathquill(stor.laPalette, stor.elts.inputmqList[0], {
      liste: ['racine', 'fraction', 'puissance', 'inf']
    })
    stor.elts.inputmqList.forEach((zoneInput) => zoneInput.addEventListener('focusin', ecoute.bind(null, zoneInput)))
    stor.elts.inputmqList[0].typeReponse = ['texte']
    stor.elts.inputmqList[0].reponse = [limiteRep1]
    stor.elts.inputmqList[1].typeReponse = ['texte']
    stor.elts.inputmqList[1].reponse = [limiteRep2]

    j3pFocus(stor.elts.inputmqList[0])
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px' } })

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneBtn = j3pAddElt(stor.zoneD, 'div')
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '', { style: { paddingTop: '10px' } })
    aide(stor.tab_quest_ordonne[j])

    const mesZonesSaisie = stor.elts.inputmqList.map(elt => elt.id)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        ds.tab_choix = purge(ds.tab_choix)
        ds.nbrepetitions = ds.tab_choix.length
        ds.nbitems = ds.nbetapes * ds.nbrepetitions
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.65 })
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie

        const reponse = fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (reponse.bonneReponse && (!me.isElapsed)) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            afficheCorrection(stor.numQuest, true)
            j3pEmpty(stor.laPalette)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment2
              me.typederreurs[10]++
              me.cacheBoutonValider()
              afficheCorrection(stor.numQuest, false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
              // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (((stor.elts.inputmqList[0].reponse[0].includes('infty')) && (stor.fctsValid.zones.reponseSaisie[0] === '\\infty')) || ((stor.elts.inputmqList[1].reponse[0].includes('infty')) && (stor.fctsValid.zones.reponseSaisie[1] === '\\infty'))) stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                j3pEmpty(stor.laPalette)
                afficheCorrection(stor.numQuest, false)
                me.cacheBoutonValider()
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      j3pDetruitFenetres('Boutons')
      j3pDetruitFenetres('Aide')
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
