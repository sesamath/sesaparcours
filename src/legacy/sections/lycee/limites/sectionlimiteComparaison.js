import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pBoutonRadio, j3pBoutonRadioChecked, j3pChaine, j3pDetruit, j3pElement, j3pFocus, j3pGetNewId, j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomInt, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive, mqRestriction, traiteMathQuillSansMultImplicites } from 'src/lib/mathquill/functions'
import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        août 2020
        Cette section permet, en plusieurs étapes de déterminer la limite d’une fonction en utilisant un théorème de comparaison ou le théorème des gendarmes
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['typeTh', 'alea', 'liste', 'type de théorème appliqué pour la suite proposée', ['alea', 'comparaison', 'gendarmes']]
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titre_exo: 'Limite d’une fonction à l’aide d’un théorème de comparaison',
  consigne1: 'On définit sur $£d$ la fonction $£f$ par&nbsp;: $£f(x)=£{fx}$.',
  consigne1_2: 'On cherche à déterminer la limite de $£f(x)$ en $£x$.',
  consigne2: 'Pour tout $£{cond}$, en partant du plus petit encadrement possible de $£g$, déterminer le meilleur encadrement de $£{fx}$.',
  consigne3: 'On a montré que, pour tout $£{cond}$, $£e$.',
  consigne4: 'Cliquer ci-dessous sur la (ou les) limite(s) indispensable(s) pour conclure sur la limite de $£f(x)$&nbsp;:',
  consigne5: 'seulement $\\limite{x}{£{xlim}}{£h}$',
  consigne6: 'deux limites : $\\limite{x}{£{xlim}}{£h}$ et $\\limite{x}{£{xlim}}{£k}$',
  consigne7: 'Comme $\\limite{x}{£{xlim}}{£h}=$&1&, en déduit que $\\limite{x}{£{xlim}}{£{fx}}=$&2&.',
  consigne8: 'Comme $\\limite{x}{£{xlim}}{£{h1}}=$&1& et que $\\limite{x}{£{xlim}}{£{h2}}=$&2&,<br/> on en déduit que $\\limite{x}{£{xlim}}{£f(x)}=$&3&.',
  indic: 'Les termes de l’encadrement seront donnés sous une forme qui permettra ensuite de calculer leurs limites.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élèves
  comment1: 'Il faut sélectionner l’une des propositions&nbsp;!',
  comment2: 'Cette limite ne permet pas de répondre à la question&nbsp;!',
  comment3: 'Cette limite ne suffit pas pour répondre à la question&nbsp;!',
  comment4: 'Ces deux limites ne sont pas indispensables pour répondre à la question&nbsp;!',
  comment5: 'Il y a un problème avec les inégalités&nbsp;!',
  comment6: 'Au moins une expression n’est pas sous la forme attendue, c’est-à-dire simplifiée ou permettant de calculer sa limite&nbsp;!',
  comment7: 'De plus, un signe d’inégalité peut être faux, voire les deux.',
  // et les phrases utiles pour les explications de la réponse
  corr1_1: 'Sachant que pour tout $x\\in£d$, $-1\\leq £g\\leq 1$, on obtient $£{e2}$ (on rappelle que $£{cond}$) ce qui équivaut à $£{e3}$',
  corr1_1_1: 'Sachant que pour tout $x\\in£d$, $-1\\leq £g\\leq 1$, on obtient $£{e2}$.',
  corr1_1_2: 'Comme $£{cond}$, on a $£{e4}$ ce qui équivaut à $£{e3}$.',
  corr1_1_3: 'Ceci devient $£{e4}\\iff £{e3}$.',
  corr1_2: 'Sachant que pour tout $x\\in£d$, $-1\\leq £g\\leq 1$, on obtient $£{e3}$',
  corr2: 'Comme $\\limite{x}{£{xlim}}{£h}=+\\infty$ et que $£{fx}\\geq £h$ pour tout $£{cond}$, on conclut que $\\limite{x}{£{xlim}}{£{fx}}=+\\infty$.',
  corr3: 'Comme $\\limite{x}{£{xlim}}{£h}=-\\infty$ et que $£{fx}\\leq £h$ pour tout $£{cond}$, on conclut que $\\limite{x}{£{xlim}}{£{fx}}=-\\infty$.',
  corr4: 'Comme $\\limite{x}{£{xlim}}{£{h1}}=£l$, que $\\limite{x}{£{xlim}}{£{h2}}=£l$ et que $£{h1}\\leq £f(x)\\leq £{h2}$ pour tout $£{cond}$, on conclut, d’après le théorème des gendarmes, que $\\limite{x}{£{xlim}}{£f(x)}=£l$.'
}

/**
 * section limiteComparaison
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAALgAAACVAAAAQEAAAAAAAAAAQAAACX#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAFiAAItM#####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQAgAAAAAAAAAAAACAP####8AAWMAAi0yAAAAAwAAAAFAAAAAAAAAAAAAAAIA#####wABZAACLTgAAAADAAAAAUAgAAAAAAAAAAAAAgD#####AAFlAAItMQAAAAMAAAABP#AAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wABZgAVYSp4XjIrYip4K2MrZC94K2UveF4y#####wAAAAEACkNPcGVyYXRpb24AAAAABQAAAAAFAAAAAAUAAAAABQL#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAAf####8AAAABAApDUHVpc3NhbmNl#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAFAAAAAAAAAAAAAAAUCAAAABgAAAAIAAAAIAAAAAAAAAAYAAAADAAAABQMAAAAGAAAABAAAAAgAAAAAAAAABQMAAAAGAAAABQAAAAcAAAAIAAAAAAAAAAFAAAAAAAAAAAABeAAAAAQA#####wADcmVwAA4oLTIqeF4yKzMpL3heMgAAAAUDAAAABQAAAAADAAAABQIAAAABQAAAAAAAAAAAAAAHAAAACAAAAAAAAAABQAAAAAAAAAAAAAABQAgAAAAAAAAAAAAHAAAACAAAAAAAAAABQAAAAAAAAAAAAXj#####AAAAAwAQQ1Rlc3RFcXVpdmFsZW5jZQD#####AAplcXVpdmFsZW50AAAABgAAAAcBAAAAAAE#8AAAAAAAAAEAAAACAP####8AAmExAAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAJiMQABMAAAAAEAAAAAAAAAAAAAAAIA#####wACYzEAAi0yAAAAAwAAAAFAAAAAAAAAAAAAAAIA#####wACZDEAATAAAAABAAAAAAAAAAAAAAACAP####8AAmUxAAEzAAAAAUAIAAAAAAAAAAAABAD#####AAFnABphMSp4XjIrYjEqeCtjMStkMS94K2UxL3heMgAAAAUAAAAABQAAAAAFAAAAAAUAAAAABQIAAAAGAAAACQAAAAcAAAAIAAAAAAAAAAFAAAAAAAAAAAAAAAUCAAAABgAAAAoAAAAIAAAAAAAAAAYAAAALAAAABQMAAAAGAAAADAAAAAgAAAAAAAAABQMAAAAGAAAADQAAAAcAAAAIAAAAAAAAAAFAAAAAAAAAAAABeAAAAAQA#####wAEcmVwMQADMyp4AAAABQIAAAABQAgAAAAAAAAAAAAIAAAAAAABeAAAAAkA#####wALZXF1aXZhbGVudDEAAAAOAAAADwEAAAAAAT#wAAAAAAAAAQAAAAIA#####wACbjEAATUAAAABQBQAAAAAAAAAAAACAP####8ACGVnYWxpdGUxARJhYnMoZihuMSktcmVwKG4xKSk8MTBeKC04KSZhYnMoZihuMSsxKS1yZXAobjErMSkpPDEwXigtOCkmYWJzKGYobjErMiktcmVwKG4xKzIpKTwxMF4oLTgpJmFicyhmKG4xKzMpLXJlcChuMSszKSk8MTBeKC04KSZhYnMoZihuMSs0KS1yZXAobjErNCkpPDEwXigtOCkmYWJzKGYobjErNSktcmVwKG4xKzUpKTwxMF4oLTgpJmFicyhmKG4xKzYpLXJlcChuMSs2KSk8MTBeKC04KSZhYnMoZihuMSs3KS1yZXAobjErNykpPDEwXigtOCkmYWJzKGYobjErOCktcmVwKG4xKzgpKTwxMF4oLTgpAAAABQoAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUE#####wAAAAIACUNGb25jdGlvbgAAAAAFAf####8AAAABAA5DQXBwZWxGb25jdGlvbgAAAAYAAAAGAAAAEQAAAAsAAAAHAAAABgAAABEAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAGAAAABQAAAAAGAAAAEQAAAAE#8AAAAAAAAAAAAAsAAAAHAAAABQAAAAAGAAAAEQAAAAE#8AAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAAAYAAAAFAAAAAAYAAAARAAAAAUAAAAAAAAAAAAAACwAAAAcAAAAFAAAAAAYAAAARAAAAAUAAAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAABgAAAAUAAAAABgAAABEAAAABQAgAAAAAAAAAAAALAAAABwAAAAUAAAAABgAAABEAAAABQAgAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAGAAAABQAAAAAGAAAAEQAAAAFAEAAAAAAAAAAAAAsAAAAHAAAABQAAAAAGAAAAEQAAAAFAEAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAAAYAAAAFAAAAAAYAAAARAAAAAUAUAAAAAAAAAAAACwAAAAcAAAAFAAAAAAYAAAARAAAAAUAUAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAABgAAAAUAAAAABgAAABEAAAABQBgAAAAAAAAAAAALAAAABwAAAAUAAAAABgAAABEAAAABQBgAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAGAAAABQAAAAAGAAAAEQAAAAFAHAAAAAAAAAAAAAsAAAAHAAAABQAAAAAGAAAAEQAAAAFAHAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAAAYAAAAFAAAAAAYAAAARAAAAAUAgAAAAAAAAAAAACwAAAAcAAAAFAAAAAAYAAAARAAAAAUAgAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAACAP####8ACGVnYWxpdGUyARthYnMoZyhuMSktcmVwMShuMSkpPDEwXigtOCkmYWJzKGcobjErMSktcmVwMShuMSsxKSk8MTBeKC04KSZhYnMoZyhuMSsyKS1yZXAxKG4xKzIpKTwxMF4oLTgpJmFicyhnKG4xKzMpLXJlcDEobjErMykpPDEwXigtOCkmYWJzKGcobjErNCktcmVwMShuMSs0KSk8MTBeKC04KSZhYnMoZyhuMSs1KS1yZXAxKG4xKzUpKTwxMF4oLTgpJmFicyhnKG4xKzYpLXJlcDEobjErNikpPDEwXigtOCkmYWJzKGcobjErNyktcmVwMShuMSs3KSk8MTBeKC04KSZhYnMoZyhuMSs4KS1yZXAxKG4xKzgpKTwxMF4oLTgpAAAABQoAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUEAAAACgAAAAAFAQAAAAsAAAAOAAAABgAAABEAAAALAAAADwAAAAYAAAARAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAADgAAAAUAAAAABgAAABEAAAABP#AAAAAAAAAAAAALAAAADwAAAAUAAAAABgAAABEAAAABP#AAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAOAAAABQAAAAAGAAAAEQAAAAFAAAAAAAAAAAAAAAsAAAAPAAAABQAAAAAGAAAAEQAAAAFAAAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAAA4AAAAFAAAAAAYAAAARAAAAAUAIAAAAAAAAAAAACwAAAA8AAAAFAAAAAAYAAAARAAAAAUAIAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAADgAAAAUAAAAABgAAABEAAAABQBAAAAAAAAAAAAALAAAADwAAAAUAAAAABgAAABEAAAABQBAAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAOAAAABQAAAAAGAAAAEQAAAAFAFAAAAAAAAAAAAAsAAAAPAAAABQAAAAAGAAAAEQAAAAFAFAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAAA4AAAAFAAAAAAYAAAARAAAAAUAYAAAAAAAAAAAACwAAAA8AAAAFAAAAAAYAAAARAAAAAUAYAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAADgAAAAUAAAAABgAAABEAAAABQBwAAAAAAAAAAAALAAAADwAAAAUAAAAABgAAABEAAAABQBwAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAOAAAABQAAAAAGAAAAEQAAAAFAIAAAAAAAAAAAAAsAAAAPAAAABQAAAAAGAAAAEQAAAAFAIAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAAAgD#####AAJhMgABMAAAAAEAAAAAAAAAAAAAAAIA#####wACYjIAATAAAAABAAAAAAAAAAAAAAACAP####8AAmMyAAEzAAAAAUAIAAAAAAAAAAAAAgD#####AAJkMgABNAAAAAFAEAAAAAAAAAAAAAIA#####wACZTIAATAAAAABAAAAAAAAAAAAAAACAP####8AAngxAAItMgAAAAMAAAABQAAAAAAAAAAAAAAEAP####8AAWgAJGEyKnheMitiMip4K2MyK2QyLyh4K3gxKStlMi8oeCt4MSleMgAAAAUAAAAABQAAAAAFAAAAAAUAAAAABQIAAAAGAAAAFAAAAAcAAAAIAAAAAAAAAAFAAAAAAAAAAAAAAAUCAAAABgAAABUAAAAIAAAAAAAAAAYAAAAWAAAABQMAAAAGAAAAFwAAAAUAAAAACAAAAAAAAAAGAAAAGQAAAAUDAAAABgAAABgAAAAHAAAABQAAAAAIAAAAAAAAAAYAAAAZAAAAAUAAAAAAAAAAAAF4AAAAAgD#####AAJhMwABMAAAAAEAAAAAAAAAAAAAAAIA#####wACYjMAATAAAAABAAAAAAAAAAAAAAACAP####8AAmMzAAItMwAAAAMAAAABQAgAAAAAAAAAAAACAP####8AAmQzAAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAJlMwACLTUAAAADAAAAAUAUAAAAAAAAAAAABAD#####AAFrACRhMyp4XjIrYjMqeCtjMytkMy8oeCt4MSkrZTMvKHgreDEpXjIAAAAFAAAAAAUAAAAABQAAAAAFAAAAAAUCAAAABgAAABsAAAAHAAAACAAAAAAAAAABQAAAAAAAAAAAAAAFAgAAAAYAAAAcAAAACAAAAAAAAAAGAAAAHQAAAAUDAAAABgAAAB4AAAAFAAAAAAgAAAAAAAAABgAAABkAAAAFAwAAAAYAAAAfAAAABwAAAAUAAAAACAAAAAAAAAAGAAAAGQAAAAFAAAAAAAAAAAABeAAAAAkA#####wALZXF1aXZhbGVudDIAAAAaAAAABwEAAAAAAT#wAAAAAAAAAQAAAAkA#####wALZXF1aXZhbGVudDMAAAAgAAAADwEAAAAAAT#wAAAAAAAAAQAAAAIA#####wAIZWdhbGl0ZTMBEmFicyhoKG4xKS1yZXAobjEpKTwxMF4oLTgpJmFicyhoKG4xKzEpLXJlcChuMSsxKSk8MTBeKC04KSZhYnMoaChuMSsyKS1yZXAobjErMikpPDEwXigtOCkmYWJzKGgobjErMyktcmVwKG4xKzMpKTwxMF4oLTgpJmFicyhoKG4xKzQpLXJlcChuMSs0KSk8MTBeKC04KSZhYnMoaChuMSs1KS1yZXAobjErNSkpPDEwXigtOCkmYWJzKGgobjErNiktcmVwKG4xKzYpKTwxMF4oLTgpJmFicyhoKG4xKzcpLXJlcChuMSs3KSk8MTBeKC04KSZhYnMoaChuMSs4KS1yZXAobjErOCkpPDEwXigtOCkAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUKAAAABQQAAAAKAAAAAAUBAAAACwAAABoAAAAGAAAAEQAAAAsAAAAHAAAABgAAABEAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAaAAAABQAAAAAGAAAAEQAAAAE#8AAAAAAAAAAAAAsAAAAHAAAABQAAAAAGAAAAEQAAAAE#8AAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAABoAAAAFAAAAAAYAAAARAAAAAUAAAAAAAAAAAAAACwAAAAcAAAAFAAAAAAYAAAARAAAAAUAAAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAAGgAAAAUAAAAABgAAABEAAAABQAgAAAAAAAAAAAALAAAABwAAAAUAAAAABgAAABEAAAABQAgAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAaAAAABQAAAAAGAAAAEQAAAAFAEAAAAAAAAAAAAAsAAAAHAAAABQAAAAAGAAAAEQAAAAFAEAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAABoAAAAFAAAAAAYAAAARAAAAAUAUAAAAAAAAAAAACwAAAAcAAAAFAAAAAAYAAAARAAAAAUAUAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAAGgAAAAUAAAAABgAAABEAAAABQBgAAAAAAAAAAAALAAAABwAAAAUAAAAABgAAABEAAAABQBgAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAaAAAABQAAAAAGAAAAEQAAAAFAHAAAAAAAAAAAAAsAAAAHAAAABQAAAAAGAAAAEQAAAAFAHAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAABoAAAAFAAAAAAYAAAARAAAAAUAgAAAAAAAAAAAACwAAAAcAAAAFAAAAAAYAAAARAAAAAUAgAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAACAP####8ACGVnYWxpdGU0ARthYnMoayhuMSktcmVwMShuMSkpPDEwXigtOCkmYWJzKGsobjErMSktcmVwMShuMSsxKSk8MTBeKC04KSZhYnMoayhuMSsyKS1yZXAxKG4xKzIpKTwxMF4oLTgpJmFicyhrKG4xKzMpLXJlcDEobjErMykpPDEwXigtOCkmYWJzKGsobjErNCktcmVwMShuMSs0KSk8MTBeKC04KSZhYnMoayhuMSs1KS1yZXAxKG4xKzUpKTwxMF4oLTgpJmFicyhrKG4xKzYpLXJlcDEobjErNikpPDEwXigtOCkmYWJzKGsobjErNyktcmVwMShuMSs3KSk8MTBeKC04KSZhYnMoayhuMSs4KS1yZXAxKG4xKzgpKTwxMF4oLTgpAAAABQoAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUKAAAABQoAAAAFCgAAAAUEAAAACgAAAAAFAQAAAAsAAAAgAAAABgAAABEAAAALAAAADwAAAAYAAAARAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAAIAAAAAUAAAAABgAAABEAAAABP#AAAAAAAAAAAAALAAAADwAAAAUAAAAABgAAABEAAAABP#AAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAgAAAABQAAAAAGAAAAEQAAAAFAAAAAAAAAAAAAAAsAAAAPAAAABQAAAAAGAAAAEQAAAAFAAAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAACAAAAAFAAAAAAYAAAARAAAAAUAIAAAAAAAAAAAACwAAAA8AAAAFAAAAAAYAAAARAAAAAUAIAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAAIAAAAAUAAAAABgAAABEAAAABQBAAAAAAAAAAAAALAAAADwAAAAUAAAAABgAAABEAAAABQBAAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAgAAAABQAAAAAGAAAAEQAAAAFAFAAAAAAAAAAAAAsAAAAPAAAABQAAAAAGAAAAEQAAAAFAFAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAAAAAABQQAAAAKAAAAAAUBAAAACwAAACAAAAAFAAAAAAYAAAARAAAAAUAYAAAAAAAAAAAACwAAAA8AAAAFAAAAAAYAAAARAAAAAUAYAAAAAAAAAAAABwAAAAFAJAAAAAAAAAAAAAMAAAABQCAAAAAAAAAAAAAFBAAAAAoAAAAABQEAAAALAAAAIAAAAAUAAAAABgAAABEAAAABQBwAAAAAAAAAAAALAAAADwAAAAUAAAAABgAAABEAAAABQBwAAAAAAAAAAAAHAAAAAUAkAAAAAAAAAAAAAwAAAAFAIAAAAAAAAAAAAAUEAAAACgAAAAAFAQAAAAsAAAAgAAAABQAAAAAGAAAAEQAAAAFAIAAAAAAAAAAAAAsAAAAPAAAABQAAAAAGAAAAEQAAAAFAIAAAAAAAAAAAAAcAAAABQCQAAAAAAAAAAAADAAAAAUAgAAAAAAAA################'
  // stor.cListeObjets affecté affecté au chargement de mtg, c’est un CListeObjets
  function traiteMathQuill (ch) {
    ch = traiteMathQuillSansMultImplicites(ch)
    ch = stor.cListeObjets.addImplicitMult(ch)// Traitement des multiplications implicites
    return ch
  }
  function afficheCorrection (bonneReponse) {
    // console.log('afficheCorrection', bonneReponse)
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let i
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    // explications contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    for (i = 1; i <= 2; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div', '')
    j3pDetruit(stor.laPalette)
    if (me.etapeCourante === 1) {
      if ((stor.typeQuest[Math.floor((me.questionCourante - 1) / ds.nbetapes)] === 'comparaison') && (me.choixCasFigure === 1) && (Math.abs(stor.objCons.c - 1) < Math.pow(10, -10))) {
        j3pAffiche(stor.zoneexplication1, '', textes.corr1_2, stor.objCons)
      } else {
        if (stor.objCons.deuxLignesCorr) {
          j3pAffiche(stor.zoneexplication1, '', textes.corr1_1_1, stor.objCons)
          const ligne2Corr = (stor.objCons.versionCarre) ? textes.corr1_1_3 : textes.corr1_1_2
          j3pAffiche(stor.zoneexplication2, '', ligne2Corr, stor.objCons)
        } else {
          j3pAffiche(stor.zoneexplication1, '', textes.corr1_1, stor.objCons)
        }
      }
    } else {
      let correction2
      correction2 = textes.corr2
      if (stor.typeTh === 'comparaison') {
        if (stor.objCons.bonChoix === 2) correction2 = textes.corr3
      } else {
        correction2 = textes.corr4
      }
      j3pAffiche(stor.zoneexplication1, '', correction2, stor.objCons)
    }
  }
  function ecoute (elt) {
    const numZone = (me.etapeCourante === 1) ? 3 : 4
    const listePalette = (numZone === 3)
      ? ['puissance', 'fraction']
      : ['inf', 'fraction']
    if (elt.classList.contains('mq-editable-field')) {
      stor.laPalette.innerHTML = ''
      j3pPaletteMathquill(stor.laPalette, elt, { liste: listePalette })
    }
  }
  function genereDonnees (objDonnees) {
    // objDonnees.lim vaut +inf, -inf ou reel
    // objDonnees.fct vaut cos ou sin
    // objDonnees.th vaut "comparaison" ou "gendarmes"
    // objDonnees.f est le nom de la fonction
    const obj = { f: objDonnees.f }
    obj.coefmtgGauche = []// liste des coefficients du membre de gauche a, b, c, d et e de l’expression an^2+bn+c+d/n+e/n^2
    obj.coefmtgDroite = []// liste des coefficients du membre de droite  a, b, c, d et e de l’expression an^2+bn+c+d/n+e/n^2
    obj.deuxLignesCorr = false
    obj.versionCarre = false
    let lesigne1, lesigne2
    if (objDonnees.lim === 'reel') {
      obj.x = '0'
      if (objDonnees.th === 'comparaison') {
        // L’exemple choisi sera de la forme a/x^k+bx^{k2}sin(1/x) où sin(1/x) peut être remplacé par cos(1/x),
        obj.xlim = obj.x + '^+'
        obj.d = ']0;+\\infty['
        obj.k = j3pGetRandomInt(1, 2)
        obj.k2 = j3pGetRandomInt(1, 2)
        const cdot = (obj.k2 === 1) ? '\\cdot ' : ''
        obj.a = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
        obj.cond = 'x>0'
        obj.b = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(2, 4)
        obj.g = ((objDonnees.fct === 'sin') ? '\\mathrm{sin}' : '\\mathrm{cos}') + '\\left(\\frac{1}{x}\\right)'
        obj.fx = (obj.a > 0 ? '' : '-') + '\\frac{' + Math.abs(obj.a) + '}{' + j3pGetLatexMonome(1, obj.k, 1, 'x') + '}' + j3pGetLatexMonome(2, obj.k2, obj.b, 'x') + cdot + obj.g
        obj.membreGauche = (obj.b > 0)
          ? (obj.a > 0 ? '' : '-') + '\\frac{' + Math.abs(obj.a) + '}{' + j3pGetLatexMonome(1, obj.k, 1, 'x') + '}' + j3pGetLatexMonome(2, obj.k2, -obj.b, 'x')
          : (obj.a > 0 ? '' : '-') + '\\frac{' + Math.abs(obj.a) + '}{' + j3pGetLatexMonome(1, obj.k, 1, 'x') + '}' + j3pGetLatexMonome(2, obj.k2, obj.b, 'x')
        obj.membreDroite = (obj.b < 0)
          ? (obj.a > 0 ? '' : '-') + '\\frac{' + Math.abs(obj.a) + '}{' + j3pGetLatexMonome(1, obj.k, 1, 'x') + '}' + j3pGetLatexMonome(2, obj.k2, -obj.b, 'x')
          : (obj.a > 0 ? '' : '-') + '\\frac{' + Math.abs(obj.a) + '}{' + j3pGetLatexMonome(1, obj.k, 1, 'x') + '}' + j3pGetLatexMonome(2, obj.k2, obj.b, 'x')
        obj.e2 = (obj.b > 0)
          ? j3pGetLatexMonome(1, obj.k2, -obj.b, 'x') + '\\leq ' + j3pGetLatexMonome(1, obj.k2, obj.b, 'x') + cdot + obj.g + '\\leq ' + j3pGetLatexMonome(1, obj.k2, obj.b, 'x')
          : j3pGetLatexMonome(1, obj.k2, -obj.b, 'x') + '\\geq ' + j3pGetLatexMonome(1, obj.k2, obj.b, 'x') + cdot + obj.g + '\\geq ' + j3pGetLatexMonome(1, obj.k2, obj.b, 'x') + '\\iff' + j3pGetLatexMonome(1, obj.k2, obj.b, 'x') + '\\leq ' + j3pGetLatexMonome(2, obj.k2, obj.b, 'x') + cdot + obj.g + '\\leq ' + j3pGetLatexMonome(1, obj.k2, -obj.b, 'x')
        obj.e3 = obj.membreGauche + '\\leq ' + objDonnees.f + '(x)' + '\\leq ' + obj.membreDroite
        obj.coefmtgGauche[0] = (obj.k2 === 2) ? -Math.abs(obj.b) : 0
        obj.coefmtgGauche[1] = (obj.k2 === 2) ? 0 : -Math.abs(obj.b)
        obj.coefmtgGauche[2] = 0
        obj.coefmtgGauche[3] = (obj.k === 2) ? 0 : obj.a
        obj.coefmtgGauche[4] = (obj.k === 2) ? obj.a : 0
        obj.coefmtgDroite[0] = (obj.k2 === 2) ? Math.abs(obj.b) : 0
        obj.coefmtgDroite[1] = (obj.k2 === 2) ? 0 : Math.abs(obj.b)
        obj.coefmtgDroite[2] = 0
        obj.coefmtgDroite[3] = (obj.k === 2) ? 0 : obj.a
        obj.coefmtgDroite[4] = (obj.k === 2) ? obj.a : 0
        obj.h = (obj.a > 0) ? obj.membreGauche : obj.membreDroite
        obj.bonChoix = (obj.h === obj.membreGauche) ? 1 : 2 // membre à choisir pour réussir à conclure sur la limite (réponse à l’étape 2)
      } else {
        // L’exemple choisi sera de la forme ax^k+b+cx^{k2}sin(1/x) où sin(1/x) peut être remplacé par cos(1/x),
        obj.xlim = obj.x
        obj.d = ']0;+\\infty['
        obj.k = j3pGetRandomInt(1, 2)
        obj.k2 = 3 - obj.k
        const cdot = (obj.k2 === 1) ? '\\cdot ' : ''
        obj.a = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
        obj.cond = 'x>0'
        obj.b = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
        obj.c = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(2, 4)
        obj.g = ((objDonnees.fct === 'sin') ? '\\mathrm{sin}' : '\\mathrm{cos}') + '\\left(\\frac{1}{x}\\right)'
        obj.l = obj.b
        obj.fx = j3pGetLatexMonome(1, obj.k, obj.a, 'x') + j3pGetLatexMonome(2, 0, obj.b, 'x') + j3pGetLatexMonome(3, obj.k2, obj.c, 'x') + cdot + obj.g
        obj.membreGauche = (obj.k > obj.k2)
          ? j3pGetLatexMonome(1, obj.k, obj.a, 'x') + j3pGetLatexMonome(2, obj.k2, -Math.abs(obj.c), 'x') + j3pGetLatexMonome(3, 0, obj.b, 'x')
          : j3pGetLatexMonome(1, obj.k2, -Math.abs(obj.c), 'x') + j3pGetLatexMonome(2, obj.k, obj.a, 'x') + j3pGetLatexMonome(3, 0, obj.b, 'x')
        obj.membreDroite = (obj.k > obj.k2)
          ? j3pGetLatexMonome(1, obj.k, obj.a, 'x') + j3pGetLatexMonome(2, obj.k2, Math.abs(obj.c), 'x') + j3pGetLatexMonome(3, 0, obj.b, 'x')
          : j3pGetLatexMonome(1, obj.k2, Math.abs(obj.c), 'x') + j3pGetLatexMonome(2, obj.k, obj.a, 'x') + j3pGetLatexMonome(3, 0, obj.b, 'x')
        obj.e2 = (obj.c > 0)
          ? j3pGetLatexMonome(1, obj.k2, -obj.c, 'x') + '\\leq ' + j3pGetLatexMonome(1, obj.k2, obj.c, 'x') + cdot + obj.g + '\\leq ' + j3pGetLatexMonome(1, obj.k2, obj.c, 'x')
          : j3pGetLatexMonome(1, obj.k2, -obj.c, 'x') + '\\geq ' + j3pGetLatexMonome(1, obj.k2, obj.c, 'x') + cdot + obj.g + '\\geq ' + j3pGetLatexMonome(1, obj.k2, obj.c, 'x') + '\\iff' + j3pGetLatexMonome(1, obj.k2, obj.c, 'x') + '\\leq ' + j3pGetLatexMonome(1, obj.k2, obj.c, 'x') + cdot + obj.g + '\\leq ' + j3pGetLatexMonome(1, obj.k2, -obj.c, 'x')
        obj.e3 = obj.membreGauche + '\\leq ' + objDonnees.f + '(x)' + '\\leq ' + obj.membreDroite
        obj.coefmtgGauche[0] = (obj.k === 2) ? obj.a : -Math.abs(obj.c)
        obj.coefmtgGauche[1] = (obj.k === 2) ? -Math.abs(obj.c) : obj.a
        obj.coefmtgGauche[2] = obj.b
        obj.coefmtgGauche[3] = 0
        obj.coefmtgGauche[4] = 0
        obj.coefmtgDroite[0] = (obj.k === 2) ? obj.a : Math.abs(obj.c)
        obj.coefmtgDroite[1] = (obj.k === 2) ? Math.abs(obj.c) : obj.a
        obj.coefmtgDroite[2] = obj.b
        obj.coefmtgDroite[3] = 0
        obj.coefmtgDroite[4] = 0
        obj.h1 = obj.membreGauche
        obj.h2 = obj.membreDroite
        obj.bonChoix = 3 // membre à choisir pour réussir à conclure sur la limite (réponse à l’étape 2)
      }
    } else {
      obj.x = (stor.typeLim === '+inf') ? '+\\infty' : '-\\infty'
      obj.xlim = obj.x
      const decalage = (j3pGetRandomInt(1, 5) === 1)
        ? '-\\pi'
        : (j3pGetRandomBool() ? '-' : '+') + j3pGetRandomInt(1, 3)
      obj.g = (objDonnees.fct === 'sin') ? '\\mathrm{sin}(x' + decalage + ')' : '\\mathrm{cos}(x' + decalage + ')'
      if (objDonnees.th === 'comparaison') {
        obj.choixCasFigure = j3pGetRandomInt(1, 2)
        if (obj.choixCasFigure === 1) {
          // L’exemple choisi sera de la forme a*n^k+b*n^{k-1}+c*sin(x+-..) où sin(x+6..) peut être remplacé par cos(x+-..),
          // si k=2, a et b seront du même signe (b pouvant être nul) si lim==='+inf', de signe contraire sinon
          // si k=1, on s’en moque
          obj.d = '\\R'
          obj.k = j3pGetRandomInt(1, 2)
          obj.a = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
          obj.cond = 'x\\in\\R'
          obj.b = (obj.k === 2)
            ? ((objDonnees.lim === '+inf') ? 1 : -1) * (obj.a / Math.abs(obj.a)) * j3pGetRandomInt(0, 3)
            : (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 6)
          obj.c = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 4)
          obj.fx = j3pGetLatexMonome(1, obj.k, obj.a, 'x') + j3pGetLatexMonome(2, obj.k - 1, obj.b, 'x') + j3pGetLatexMonome(3, 1, obj.c, obj.g)
          if (obj.k === 2) {
            obj.membreGauche = (obj.c > 0)
              ? j3pGetLatexMonome(1, obj.k, obj.a, 'x') + j3pGetLatexMonome(2, obj.k - 1, obj.b, 'x') + j3pGetLatexMonome(3, 0, -obj.c, 'x')
              : j3pGetLatexMonome(1, obj.k, obj.a, 'x') + j3pGetLatexMonome(2, obj.k - 1, obj.b, 'x') + j3pGetLatexMonome(3, 0, obj.c, 'x')
            obj.membreDroite = (obj.c > 0)
              ? j3pGetLatexMonome(1, obj.k, obj.a, 'x') + j3pGetLatexMonome(2, obj.k - 1, obj.b, 'x') + j3pGetLatexMonome(3, 0, obj.c, 'x')
              : j3pGetLatexMonome(1, obj.k, obj.a, 'x') + j3pGetLatexMonome(2, obj.k - 1, obj.b, 'x') + j3pGetLatexMonome(3, 0, -obj.c, 'x')
          } else {
            obj.membreGauche = (obj.c > 0)
              ? j3pGetLatexMonome(1, 1, obj.a, 'x') + j3pGetLatexMonome(2, 0, obj.b - obj.c, 'x')
              : j3pGetLatexMonome(1, 1, obj.a, 'x') + j3pGetLatexMonome(2, 0, obj.b + obj.c, 'x')
            obj.membreDroite = (obj.c > 0)
              ? j3pGetLatexMonome(1, 1, obj.a, 'x') + j3pGetLatexMonome(2, 0, obj.b + obj.c, 'x')
              : j3pGetLatexMonome(1, 1, obj.a, 'x') + j3pGetLatexMonome(2, 0, obj.b - obj.c, 'x')
          }
          obj.e2 = (obj.c > 0)
            ? (-obj.c) + '\\leq ' + j3pGetLatexMonome(1, 1, obj.c, obj.g) + '\\leq ' + obj.c
            : (-obj.c) + '\\geq ' + j3pGetLatexMonome(1, 1, obj.c, obj.g) + '\\geq ' + obj.c + '\\iff' + obj.c + '\\leq ' + j3pGetLatexMonome(1, 1, obj.c, obj.g) + '\\leq ' + (-obj.c)
          obj.e3 = obj.membreGauche + '\\leq ' + objDonnees.f + '(x)' + '\\leq ' + obj.membreDroite
          obj.coefmtgGauche[0] = (obj.k === 2) ? obj.a : 0
          obj.coefmtgGauche[1] = (obj.k === 2) ? obj.b : obj.a
          obj.coefmtgGauche[2] = (obj.k === 2) ? -Math.abs(obj.c) : obj.b - Math.abs(obj.c)
          obj.coefmtgGauche[3] = 0
          obj.coefmtgGauche[4] = 0
          obj.coefmtgDroite[0] = (obj.k === 2) ? obj.a : 0
          obj.coefmtgDroite[1] = (obj.k === 2) ? obj.b : obj.a
          obj.coefmtgDroite[2] = (obj.k === 2) ? Math.abs(obj.c) : obj.b + Math.abs(obj.c)
          obj.coefmtgDroite[3] = 0
          obj.coefmtgDroite[4] = 0
        } else {
          // L’exemple choisi sera de la forme a*n^k+(b*sin(x+-..))/x^k2' où sin(x+-..) peut être remplacé par cos(x+-..),
          // k>=1 et k2>=1 (ils peuvent être égaux ou non)
          obj.d = '\\R^*'
          obj.k = j3pGetRandomInt(1, 2)
          obj.k2 = j3pGetRandomInt(1, 2)
          obj.versionCarre = obj.k2 === 2
          obj.a = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
          do {
            obj.b = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
          } while ((Math.abs(obj.b - 1) < Math.pow(10, -12)))
          obj.cond = (obj.k2 === 2) ? 'x\\in\\R^*' : (objDonnees.lim === '+inf') ? 'x>0' : 'x<0'
          obj.fx = j3pGetLatexMonome(1, obj.k, obj.a, 'x')
          if (obj.b < 0) {
            obj.fx += '-\\frac{' + j3pGetLatexMonome(1, 1, Math.abs(obj.b), obj.g) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}'
          } else {
            obj.fx += '+\\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}'
          }
          obj.deuxLignesCorr = true
          lesigne1 = (obj.k2 === 2 || stor.typeLim === '+inf') ? '-' : '+'
          lesigne2 = (obj.k2 === 2 || stor.typeLim === '+inf') ? '+' : '-'
          obj.membreGauche = j3pGetLatexMonome(1, obj.k, obj.a, 'x') + lesigne1 + '\\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}'
          obj.membreDroite = j3pGetLatexMonome(1, obj.k, obj.a, 'x') + lesigne2 + '\\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}'
          obj.e2 = (obj.b > 0)
            ? (-obj.b) + '\\leq ' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '\\leq ' + obj.b
            : (-obj.b) + '\\geq ' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '\\geq ' + obj.b + '\\iff' + obj.b + '\\leq ' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '\\leq ' + (-obj.b)
          if (obj.k2 === 1) {
            if (stor.typeLim === '+inf') {
              obj.e4 = (obj.b > 0)
                ? '-\\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\leq \\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}'
                : '-\\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\leq \\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}'
            } else {
              obj.e4 = (obj.b > 0)
                ? '-\\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\geq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\geq \\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\iff \\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\leq -\\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}'
                : '-\\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\geq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\geq \\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\iff \\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\leq -\\frac{' + -obj.b + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}'
            }
          } else {
            obj.e4 = '-\\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}\\leq \\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k2, 1, 'x') + '}'
          }
          obj.e3 = obj.membreGauche + '\\leq ' + obj.f + '(x)' + '\\leq ' + obj.membreDroite
          obj.coefmtgGauche[0] = (obj.k === 2) ? obj.a : 0
          obj.coefmtgGauche[1] = (obj.k === 2) ? 0 : obj.a
          obj.coefmtgGauche[2] = 0
          obj.coefmtgGauche[3] = (obj.k2 === 2) ? 0 : ((stor.typeLim === '+inf') ? -Math.abs(obj.b) : Math.abs(obj.b))
          obj.coefmtgGauche[4] = (obj.k2 === 2) ? -Math.abs(obj.b) : 0
          obj.coefmtgDroite[0] = (obj.k === 2) ? obj.a : 0
          obj.coefmtgDroite[1] = (obj.k === 2) ? 0 : obj.a
          obj.coefmtgDroite[2] = 0
          obj.coefmtgDroite[3] = (obj.k2 === 2) ? 0 : ((stor.typeLim === '+inf') ? Math.abs(obj.b) : -Math.abs(obj.b))
          obj.coefmtgDroite[4] = (obj.k2 === 2) ? Math.abs(obj.b) : 0
        }
        if (objDonnees.lim === '+inf') {
          obj.h = (obj.a > 0) ? obj.membreGauche : obj.membreDroite
          obj.bonChoix = (obj.h === obj.membreGauche) ? 1 : 2 // membre à choisir pour réussir à conclure sur la limite (réponse à l’étape 2)
        } else {
          obj.h = (obj.a > 0 && obj.k === 2) || (obj.a < 0 && obj.k === 1) ? obj.membreGauche : obj.membreDroite
          obj.bonChoix = (obj.h === obj.membreGauche) ? 1 : 2 // membre à choisir pour réussir à conclure sur la limite (réponse à l’étape 2)
        }
      } else {
        // L’exemple choisi sera de la forme a+(b*sin(x+-..))/(x+c)^k où sin(x+-..) peut être remplacé par cos(x+-..),
        // k>=1
        obj.k = j3pGetRandomInt(1, 2)
        obj.a = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
        do {
          obj.b = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 5)
        } while ((Math.abs(obj.b - 1) < Math.pow(10, -12)))
        obj.c = (j3pGetRandomInt(0, 1) * 2 - 1) * j3pGetRandomInt(1, 3)
        obj.cond = (obj.k === 2) ? 'x\\in\\R\\setminus\\bracket{' + -obj.c + '}' : (objDonnees.lim === '+inf') ? 'x>' + -obj.c : 'x<' + -obj.c
        obj.fx = j3pGetLatexMonome(1, 0, obj.a, 'x')
        obj.d = '\\R\\setminus\\bracket{' + -obj.c + '}'
        obj.deuxLignesCorr = true
        const mavar = (obj.k === 1)
          ? 'x' + ((obj.c > 0) ? '+' : '-') + Math.abs(obj.c)
          : '(x' + ((obj.c > 0) ? '+' : '-') + Math.abs(obj.c) + ')'
        if (obj.b < 0) {
          obj.fx += '-\\frac{' + j3pGetLatexMonome(1, 1, Math.abs(obj.b), obj.g) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}'
        } else {
          obj.fx += '+\\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}'
        }
        obj.l = obj.a
        lesigne1 = (obj.k === 2 || stor.typeLim === '+inf') ? '-' : '+'
        lesigne2 = (obj.k === 2 || stor.typeLim === '+inf') ? '+' : '-'
        obj.membreGauche = j3pGetLatexMonome(1, 0, obj.a, 'x') + lesigne1 + '\\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}'
        obj.membreDroite = j3pGetLatexMonome(1, 0, obj.a, 'x') + lesigne2 + '\\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}'

        obj.e2 = (obj.b > 0)
          ? (-obj.b) + '\\leq ' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '\\leq ' + obj.b
          : (-obj.b) + '\\geq ' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '\\geq ' + obj.b + '\\iff' + obj.b + '\\leq ' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '\\leq ' + (-obj.b)
        if (obj.k === 2 || stor.typeLim === '+inf') {
          obj.e4 = (obj.b > 0)
            ? '-\\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\leq \\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}'
            : '-\\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\leq \\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}'
        } else {
          obj.e4 = (obj.b > 0)
            ? '-\\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\geq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\geq \\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\iff \\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\leq -\\frac{' + obj.b + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}'
            : '-\\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\geq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\geq \\frac{' + -obj.b + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\iff \\frac{' + Math.abs(obj.b) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\leq \\frac{' + j3pGetLatexMonome(1, 1, obj.b, obj.g) + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}\\leq -\\frac{' + -obj.b + '}{' + j3pGetLatexMonome(1, obj.k, 1, mavar) + '}'
        }
        obj.e3 = obj.membreGauche + '\\leq ' + obj.f + '(x)' + '\\leq ' + obj.membreDroite
        obj.h1 = obj.membreGauche
        obj.h2 = obj.membreDroite
        obj.bonChoix = 3 // membre à choisir pour réussir à conclure sur la limite (réponse à l’étape 2)
        obj.coefmtgGauche[0] = 0
        obj.coefmtgGauche[1] = 0
        obj.coefmtgGauche[2] = obj.a
        obj.coefmtgGauche[3] = (obj.k === 2) ? 0 : ((stor.typeLim === '+inf') ? -Math.abs(obj.b) : Math.abs(obj.b))
        obj.coefmtgGauche[4] = (obj.k === 2) ? -Math.abs(obj.b) : 0
        obj.coefmtgDroite[0] = 0
        obj.coefmtgDroite[1] = 0
        obj.coefmtgDroite[2] = obj.a
        obj.coefmtgDroite[3] = (obj.k === 2) ? 0 : ((stor.typeLim === '+inf') ? Math.abs(obj.b) : -Math.abs(obj.b))
        obj.coefmtgDroite[4] = (obj.k === 2) ? Math.abs(obj.b) : 0
      }
    }
    // console.log('obj:', obj)
    return obj
  }

  function enonceInitFirst () {
    me.surcharge({ nbetapes: 2 })
    // Construction de la page
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.7 })

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    stor.typeQuest = []
    let alea, i
    if (ds.typeTh === 'alea') {
      const tabChoixInit = ['comparaison', 'gendarmes']
      let tabChoix = tabChoixInit.slice(0)
      for (i = 1; i <= ds.nbrepetitions; i++) {
        if (tabChoix.length === 0) {
          tabChoix = tabChoixInit.slice(0)
        }
        alea = j3pGetRandomInt(0, tabChoix.length - 1)
        stor.typeQuest.push(tabChoix[alea])
        tabChoix.splice(alea, 1)
      }
    } else {
      for (i = 1; i <= ds.nbrepetitions; i++) {
        stor.typeQuest.push(ds.typeTh)
      }
    }
    stor.choixFct = []
    let choixFct = ['sin', 'cos']
    const choixFctInit = choixFct.slice(0)
    for (i = 1; i <= ds.nbrepetitions; i++) {
      if (choixFct.length === 0) {
        choixFct = choixFctInit.slice(0)
      }
      alea = j3pGetRandomInt(0, choixFct.length - 1)
      stor.choixFct.push(choixFct[alea])
      choixFct.splice(alea, 1)
    }
    // Choix type de limite (en l’infini ou en un réel)
    let choixLim = ['+inf', '-inf', 'reel']
    const choixLimInit = choixFct.slice(0)
    stor.choixLim = []
    for (i = 1; i <= ds.nbrepetitions; i++) {
      if (choixLim.length === 0) {
        choixLim = choixLimInit.slice(0)
      }
      alea = j3pGetRandomInt(0, choixLim.length - 1)
      stor.choixLim.push(choixLim[alea])
      choixLim.splice(alea, 1)
    }
    // chargement mtg
    getMtgCore()
      .then(
        // success
        (mtgAppLecteur) => {
          stor.cListeObjets = mtgAppLecteur.createList(txtFigure)
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pAddElt(
      me.zonesElts.MG,
      'div',
      '',
      { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) }
    )
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (let i = 0; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    if (me.etapeCourante === 1) {
      // Nouvelle fonction donc je dois regénérer les données
      const indexQuestion = me.repetitionCourante - 1 // on veut démarrer à 0
      stor.typeTh = stor.typeQuest[indexQuestion]
      stor.typeLim = stor.choixLim[indexQuestion]
      stor.fctEncadree = stor.choixFct[indexQuestion]
      // Pour imposer un cas de figure
      /* stor.typeTh = 'gendarmes'
      stor.typeLim = 'reel'
      stor.fctEncadree = 'cos'
      */
      const nomf = j3pGetRandomElt(['f', 'g', 'h'])
      stor.objCons = genereDonnees({ lim: stor.typeLim, th: stor.typeTh, fct: stor.fctEncadree, f: nomf })
      me.logIfDebug('stor.objCons:', stor.objCons)
    }
    j3pAffiche(stor.zoneCons0, '', textes.consigne1, stor.objCons)
    j3pAffiche(stor.zoneCons1, '', textes.consigne1_2, stor.objCons)

    if (me.etapeCourante === 1) {
      j3pAffiche(stor.zoneCons2, '', textes.consigne2, stor.objCons)
      const ligneRep = '&1& #1# $£{fx}$ #2# &2&'
      stor.zoneIndic = j3pAddElt(stor.zoneCons3, 'div')
      stor.elts = j3pAffiche(stor.zoneCons3, '', ligneRep, {
        fx: stor.objCons.fx,
        inputmq1: { texte: '' },
        liste1: { texte: ['', '<', '&le;', '>', '&ge;'] },
        liste2: { texte: ['', '<', '&le;', '>', '&ge;'] },
        inputmq2: { texte: '' }
      })
      j3pAffiche(stor.zoneIndic, '', textes.indic, { style: { fontSize: '0.9em', fontStyle: 'italic' } })
      stor.elts.inputmqList[0].typeReponse = ['texte']
      stor.elts.inputmqList[1].typeReponse = ['texte']
      const zones = [stor.elts.inputmqList[0].id, stor.elts.selectList[0].id, stor.elts.selectList[1].id, stor.elts.inputmqList[1].id]
      j3pFocus(stor.elts.inputmqList[0])
      // Paramétrage de la validation
      stor.fctsValid = new ValidationZones({ parcours: me, zones, validePerso: [...zones] })
      stor.zoneCons4.style.paddingTop = '15px'
      stor.laPalette = j3pAddElt(stor.zoneCons4, 'div')
      $(stor.elts.inputmqList[0]).focusin(ecoute.bind(null, stor.elts.inputmqList[0]))
      $(stor.elts.inputmqList[1]).focusin(ecoute.bind(null, stor.elts.inputmqList[1]))
      for (let i = 0; i <= 1; i++) {
        mqRestriction(stor.elts.inputmqList[i], '\\d,.+-*/^()abcdefghijklmnopqrstuvwxyz', { commandes: ['fraction', 'puissance'] })
      }
      ecoute(stor.elts.inputmqList[1])
      stor.nbChancesInit = ds.nbchances
      j3pFocus(stor.elts.inputmqList[0])
    } else {
      // 2e étape
      const tabChoix = []
      stor.objCons.e = j3pChaine(stor.objCons.membreGauche + '\\leq £f(x)\\leq ' + stor.objCons.membreDroite, { f: stor.objCons.f })
      j3pAffiche(stor.zoneCons2, '', textes.consigne3, stor.objCons)
      j3pAffiche(stor.zoneCons3, '', textes.consigne4, stor.objCons)
      tabChoix.push(j3pChaine(textes.consigne5, { h: stor.objCons.membreGauche, xlim: stor.objCons.xlim }))
      tabChoix.push(j3pChaine(textes.consigne5, { h: stor.objCons.membreDroite, xlim: stor.objCons.xlim }))
      tabChoix.push(j3pChaine(textes.consigne6, {
        h: stor.objCons.membreGauche,
        k: stor.objCons.membreDroite,
        xlim: stor.objCons.xlim
      }))
      stor.btnRadioSelect = 0
      stor.nameRadio = j3pGetNewId('choix')
      stor.idsRadio = []
      for (let k = 1; k <= 3; k++) {
        stor['btnRadio' + k] = j3pAddElt(stor.zoneCons4, 'div', '')
        stor.idsRadio.push(j3pGetNewId('radio' + k))
        j3pBoutonRadio(stor['btnRadio' + k], stor.idsRadio[k - 1], stor.nameRadio, k - 1, '')
        j3pAffiche('label' + stor.idsRadio[k - 1], '', tabChoix[k - 1])
        j3pElement(stor.idsRadio[k - 1]).addEventListener('click', function () {
          const numRadio = j3pBoutonRadioChecked(stor.nameRadio)[0] + 1
          if (numRadio !== stor.btnRadioSelect) {
            const zones = []
            if (stor.btnRadioSelect > 0) {
              j3pDetruit(stor.zoneCons5)
              j3pDetruit(stor.laPalette)
            }
            stor.zoneCons5 = j3pAddElt(stor.conteneur, 'div')
            stor.laPalette = j3pAddElt(stor.conteneur, 'div')
            if (numRadio <= 2) {
              stor.elts = j3pAffiche(stor.zoneCons5, '', textes.consigne7, {
                inputmq1: { texte: '' },
                inputmq2: { texte: '' },
                fx: stor.objCons.fx,
                xlim: stor.objCons.xlim,
                h: (numRadio === 1) ? stor.objCons.membreGauche : stor.objCons.membreDroite
              })
            } else {
              stor.elts = j3pAffiche(stor.zoneCons5, '', textes.consigne8, {
                inputmq1: { texte: '' },
                inputmq2: { texte: '' },
                inputmq3: { texte: '' },
                f: stor.objCons.f,
                xlim: stor.objCons.xlim,
                h1: stor.objCons.membreGauche,
                h2: stor.objCons.membreDroite
              })
            }
            stor.elts.inputmqList.forEach(eltInput => {
              eltInput.typeReponse = ['texte']
              zones.push(eltInput)
              $(eltInput).focusin(ecoute.bind(null, eltInput))
              mqRestriction(eltInput, '\\d,.+-/', { commandes: ['fraction', 'inf'] })
            })
            stor.btnRadioSelect = numRadio
            ecoute(stor.elts.inputmqList[0])
            stor.fctsValid = new ValidationZones({ parcours: me, zones, validePerso: [...zones] })
          }
          j3pFocus(stor.elts.inputmqList[0])
        })
      }
    }

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense d ela réponse de l’élève (le plus souvant dans le cadre MD)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    me.finEnonce()
  }

  function corrige () {
    // On teste si une réponse a été saisie
    // ce qui suit sert pour la validation de toutes les zones
    // le tableau contenant toutes les zones de saisie
    let reponse = { aRepondu: false, bonneReponse: false }
    let pbsInegalites, btnRadioNonCoche, bonChoixRadio, reponsesEgales, expressionsSimplifiees

    // analyse de la réponse
    if (me.etapeCourante === 1) {
      reponse.aRepondu = stor.fctsValid.valideReponses()
      pbsInegalites = true
      if (reponse.aRepondu) {
        // validation du cas où on aurait écrit zone1<=u_n<=zone2
        if (stor.typeTh === 'gendarmes') {
          stor.cListeObjets.giveFormula2('a2', stor.objCons.coefmtgGauche[0])
          stor.cListeObjets.giveFormula2('b2', stor.objCons.coefmtgGauche[1])
          stor.cListeObjets.giveFormula2('c2', stor.objCons.coefmtgGauche[2])
          stor.cListeObjets.giveFormula2('d2', stor.objCons.coefmtgGauche[3])
          stor.cListeObjets.giveFormula2('e2', stor.objCons.coefmtgGauche[4])
          stor.cListeObjets.giveFormula2('a3', stor.objCons.coefmtgDroite[0])
          stor.cListeObjets.giveFormula2('b3', stor.objCons.coefmtgDroite[1])
          stor.cListeObjets.giveFormula2('c3', stor.objCons.coefmtgDroite[2])
          stor.cListeObjets.giveFormula2('d3', stor.objCons.coefmtgDroite[3])
          stor.cListeObjets.giveFormula2('e3', stor.objCons.coefmtgDroite[4])
          if (stor.typeLim === 'reel') {
            stor.cListeObjets.giveFormula2('x1', String(0))
          } else {
            stor.cListeObjets.giveFormula2('x1', String(stor.objCons.c))
          }
        } else {
          stor.cListeObjets.giveFormula2('a', stor.objCons.coefmtgGauche[0])
          stor.cListeObjets.giveFormula2('b', stor.objCons.coefmtgGauche[1])
          stor.cListeObjets.giveFormula2('c', stor.objCons.coefmtgGauche[2])
          stor.cListeObjets.giveFormula2('d', stor.objCons.coefmtgGauche[3])
          stor.cListeObjets.giveFormula2('e', stor.objCons.coefmtgGauche[4])
          stor.cListeObjets.giveFormula2('a1', stor.objCons.coefmtgDroite[0])
          stor.cListeObjets.giveFormula2('b1', stor.objCons.coefmtgDroite[1])
          stor.cListeObjets.giveFormula2('c1', stor.objCons.coefmtgDroite[2])
          stor.cListeObjets.giveFormula2('d1', stor.objCons.coefmtgDroite[3])
          stor.cListeObjets.giveFormula2('e1', stor.objCons.coefmtgDroite[4])
        }
        const repeleveGauche = $(stor.elts.inputmqList[0]).mathquill('latex')
        const repeleveDroite = $(stor.elts.inputmqList[1]).mathquill('latex')
        const expEleveGauche = traiteMathQuill(repeleveGauche)
        const expEleveDroite = traiteMathQuill(repeleveDroite)
        const inegIndex1 = stor.elts.selectList[0].selectedIndex
        const inegIndex2 = stor.elts.selectList[1].selectedIndex
        // Premier cas de figure d’une bonne réponse :
        // membregauche<=f(x)<=membreDroite
        stor.cListeObjets.giveFormula2('rep', expEleveGauche)
        stor.cListeObjets.giveFormula2('rep1', expEleveDroite)
        stor.cListeObjets.giveFormula2('n1', '7')
        stor.cListeObjets.calculateNG(false)
        const bonneZoneGauche = (stor.typeTh === 'comparaison') ? (stor.cListeObjets.valueOf('equivalent') === 1) : stor.cListeObjets.valueOf('equivalent2') === 1
        const bonneZoneDroite = (stor.typeTh === 'comparaison') ? (stor.cListeObjets.valueOf('equivalent1') === 1) : stor.cListeObjets.valueOf('equivalent3') === 1
        let bonneZoneGauche1
        let bonneZoneDroite1
        let premiereCas = bonneZoneGauche
        let deuxiemeCas = false
        premiereCas = (premiereCas && inegIndex1 === 2)
        premiereCas = (premiereCas && bonneZoneDroite)
        premiereCas = (premiereCas && inegIndex2 === 2)

        me.logIfDebug('bonneZoneGauche:', bonneZoneGauche, bonneZoneDroite, premiereCas)
        reponse.bonneReponse = premiereCas
        // Si la réponse n’est pas celle attendue, je cherche tout de même à savoir si l’élève n’a pas donné des réponses égales (mais non équivalentes en écriture) à ce qui est attendu :
        // Ce sont les variables mtg32 egalite1 et egalite2 qui vont servir à la place de bonneZoneGauche et bonneZoneDroite
        let egaliteZoneGauche = (stor.typeTh === 'comparaison')
          ? stor.cListeObjets.valueOf('egalite1') === 1
          : stor.cListeObjets.valueOf('egalite3') === 1
        let egaliteZoneDroite = (stor.typeTh === 'comparaison')
          ? stor.cListeObjets.valueOf('egalite2') === 1
          : stor.cListeObjets.valueOf('egalite4') === 1
        reponsesEgales = egaliteZoneGauche && inegIndex1 === 2 && egaliteZoneDroite && inegIndex2 === 2
        expressionsSimplifiees = !(egaliteZoneGauche && egaliteZoneDroite)
        if (stor.typeLim === 'reel' && reponsesEgales) reponse.bonneReponse = true
        // On accepte toute réponse égale à ce que j’attends (on ne demande pas un truc équivalent à ce qui semble le plus naturel car tout permet de calculer la limite ensuite)
        if (!reponse.bonneReponse && !reponsesEgales) {
          stor.cListeObjets.giveFormula2('rep1', expEleveGauche)
          stor.cListeObjets.giveFormula2('rep', expEleveDroite)
          stor.cListeObjets.calculateNG(false)
          bonneZoneDroite1 = (stor.typeTh === 'comparaison') ? (stor.cListeObjets.valueOf('equivalent') === 1) : stor.cListeObjets.valueOf('equivalent2') === 1
          bonneZoneGauche1 = (stor.typeTh === 'comparaison') ? (stor.cListeObjets.valueOf('equivalent1') === 1) : stor.cListeObjets.valueOf('equivalent3') === 1
          deuxiemeCas = bonneZoneDroite1
          deuxiemeCas = (deuxiemeCas && inegIndex1 === 4)
          deuxiemeCas = (deuxiemeCas && bonneZoneGauche1)
          deuxiemeCas = (deuxiemeCas && inegIndex2 === 4)

          me.logIfDebug('bonneZoneGauche1:', bonneZoneGauche1, bonneZoneDroite1, deuxiemeCas)
          reponse.bonneReponse = deuxiemeCas
          // Si la réponse n’est pas celle attendue, je cherche tout de même à savoir si l’élève n’a pas donné des réponses égales (mais non équivalentes en écriture) à ce qui est attendu :
          egaliteZoneGauche = (stor.typeTh === 'comparaison')
            ? stor.cListeObjets.valueOf('egalite1') === 1
            : stor.cListeObjets.valueOf('egalite3') === 1
          egaliteZoneDroite = (stor.typeTh === 'comparaison')
            ? stor.cListeObjets.valueOf('egalite2') === 1
            : stor.cListeObjets.valueOf('egalite4') === 1
          reponsesEgales = egaliteZoneGauche && inegIndex1 === 4 && egaliteZoneDroite && inegIndex2 === 4
          expressionsSimplifiees = expressionsSimplifiees || !(egaliteZoneGauche && egaliteZoneDroite)
          if (stor.typeLim === 'reel' && reponsesEgales) reponse.bonneReponse = true
        }
        for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
          stor.fctsValid.zones.bonneReponse[i] = true
        }
        if (reponse.bonneReponse) {
          for (let i = 0; i < 4; i++) {
            stor.fctsValid.zones.bonneReponse[i] = true
            stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
          }
        } else {
          pbsInegalites = (inegIndex1 !== inegIndex2)
          // Il faut que je gère ce qui devra être mis en rouge
          if (pbsInegalites) {
            stor.fctsValid.zones.bonneReponse[1] = false
            stor.fctsValid.zones.bonneReponse[2] = false
          }
          if (inegIndex1 % 2 === 1) stor.fctsValid.zones.bonneReponse[1] = false
          if (inegIndex2 % 2 === 1) stor.fctsValid.zones.bonneReponse[2] = false
          stor.fctsValid.zones.bonneReponse[0] = (bonneZoneGauche || bonneZoneGauche1)
          if (bonneZoneGauche) stor.fctsValid.zones.bonneReponse[3] = bonneZoneDroite
          if (bonneZoneGauche && bonneZoneDroite) {
            for (const i of [1, 2]) stor.fctsValid.zones.bonneReponse[i] = premiereCas
          }
          if (bonneZoneGauche1) stor.fctsValid.zones.bonneReponse[3] = bonneZoneDroite1
          if (bonneZoneGauche1 && bonneZoneDroite1) {
            for (const i of [1, 2]) stor.fctsValid.zones.bonneReponse[i] = deuxiemeCas
          }
          if (!bonneZoneGauche && !bonneZoneGauche1) stor.fctsValid.zones.bonneReponse[3] = (bonneZoneDroite || bonneZoneDroite1)
          stor.inegalitesExactes = !((!bonneZoneGauche || !bonneZoneDroite) && (!bonneZoneGauche1 || !bonneZoneDroite1))
          if (!stor.inegalitesExactes) {
            for (const i of [1, 2]) stor.fctsValid.zones.bonneReponse[i] = false
          }
          if (reponsesEgales) {
            // Les réponses données ne sont pas sous la bonne forme, mais elles sont correctes, donc je donne une chance de plus
            // Par contre, je le fais au max 2 fois :
            if (ds.nbchances < stor.nbChancesInit + 2) ds.nbchances++
          }
          if (me.essaiCourant < ds.nbchances) {
            // On ne valide rien à cet instant car l’encadrement doit pouvoir être modifié
            // Par contre on met en couleur
            for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
              stor.fctsValid.zones.inputs[i].style.color = (stor.fctsValid.zones.bonneReponse[i]) ? me.styles.cbien : me.styles.cfaux
            }
          } else {
            for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[i])
          }
        }
      }
    } else {
      // 2e et dernière étape
      stor.btnRadioSelect = j3pBoutonRadioChecked(stor.nameRadio)[0] + 1
      reponse.aRepondu = (stor.btnRadioSelect > 0)
      btnRadioNonCoche = (stor.btnRadioSelect === 0)
      bonChoixRadio = true
      if (reponse.aRepondu) {
        reponse.aRepondu = stor.fctsValid.valideReponses()
        // On vérifie si le bouton coché est le bon
        bonChoixRadio = (String(stor.btnRadioSelect) === String(stor.objCons.bonChoix))
        if (bonChoixRadio) {
          const zones = []
          if (stor.objCons.bonChoix === 1) {
            // le membre de gauche permet de répondre, c’est que la limite est +\infty
            stor.elts.inputmqList.forEach(eltInput => {
              eltInput.reponse = ['+\\infty']
              zones.push(eltInput)
            })
          } else if (stor.objCons.bonChoix === 2) {
            // le membre de droite permet de répondre, c’est que la limite est -\infty
            stor.elts.inputmqList.forEach(eltInput => {
              eltInput.reponse = ['-\\infty']
              zones.push(eltInput)
            })
          } else {
            stor.elts.inputmqList.forEach(eltInput => {
              eltInput.typeReponse = ['nombre', 'exact']
              eltInput.reponse = [stor.objCons.l]
              zones.push(eltInput)
            })
          }
          stor.fctsValid = new ValidationZones({ parcours: me, zones })
          reponse = stor.fctsValid.validationGlobale()
        } else {
          reponse.bonneReponse = false
        }
      }
    } // fin analyse de la réponse

    // on passe au traitement de cette réponse
    if ((!reponse.aRepondu) && (!me.isElapsed)) {
      // On peut ajouter un commentaire particulier.
      let msgReponseManquante
      if (me.etapeCourante === 2 && btnRadioNonCoche) {
        msgReponseManquante = textes.comment1
      } // sinon ce sera le message par défaut
      me.reponseManquante(stor.zoneCorr, msgReponseManquante)
      return me.finCorrection() // on reste en correction
    }

    // Une réponse a été saisie
    let j
    if (reponse.bonneReponse) {
      // Bonne réponse
      me.score++
      stor.zoneCorr.style.color = me.styles.cbien
      stor.zoneCorr.innerHTML = cBien
      // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
      if (me.etapeCourante === 2) {
        for (j = 1; j <= 3; j++) j3pDesactive(stor.idsRadio[j - 1])
      }
      afficheCorrection(true)
      me.typederreurs[0]++
      return me.finCorrection('navigation', true)
    }

    // Pas de bonne réponse
    stor.zoneCorr.style.color = me.styles.cfaux

    // A cause de la limite de temps :
    if (me.isElapsed) { // limite de temps
      stor.zoneCorr.innerHTML = tempsDepasse
      me.typederreurs[10]++
      // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
      if (me.etapeCourante === 2) {
        for (j = 1; j <= 3; j++) j3pDesactive(stor.idsRadio[j - 1])
        $('#label' + stor.idsRadio[stor.objCons.bonChoix - 1]).css('color', me.styles.toutpetit.correction.color)
      }
      afficheCorrection(false)
      return me.finCorrection('navigation', true)
    }

    // Réponse fausse :
    let comment
    if (me.etapeCourante === 1) {
      if (pbsInegalites) {
        comment = textes.comment5
      } else if (!expressionsSimplifiees) {
        comment = textes.comment6
      }
    } else {
      if (!bonChoixRadio) {
        if (stor.objCons.bonChoix === 3) {
          comment = textes.comment3
        } else if (stor.btnRadioSelect === 3) {
          comment = textes.comment4
        } else {
          comment = textes.comment2
        }
      }
    }
    if (!comment) comment = cFaux
    if (me.etapeCourante === 1 && !stor.inegalitesExactes) comment += '<br>' + textes.comment7
    stor.zoneCorr.innerHTML = comment

    if (me.essaiCourant < ds.nbchances) {
      stor.zoneCorr.innerHTML += '<br>' + essaieEncore
      me.typederreurs[1]++
      // ici il a encore la possibilité de se corriger, on reste en correction
      return me.finCorrection()
    }

    // Erreur au dernier essai
    stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
    // Là il ne peut plus se corriger. On lui affiche alors la solution
    if (me.etapeCourante === 2) {
      for (j = 1; j <= 3; j++) j3pDesactive(stor.idsRadio[j - 1])
      if (!bonChoixRadio) {
        j3pBarre('label' + stor.idsRadio[j3pBoutonRadioChecked(stor.nameRadio)[0]])
        for (let i = 0; i < stor.fctsValid.zones.inputs.length; i++) {
          j3pDesactive(stor.fctsValid.zones.inputs[i])
          j3pBarre(stor.zoneCons5)
        }
      }
      $('#label' + stor.idsRadio[stor.objCons.bonChoix - 1]).css('color', me.styles.toutpetit.correction.color)
      $(stor.zoneCons5).css('color', me.styles.cfaux)
    }
    afficheCorrection(false)
    me.typederreurs[2]++
    me.finCorrection('navigation', true)
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      corrige()
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
