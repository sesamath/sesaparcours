import { j3pAddElt, j3pShuffle, j3pDetruit, j3pFocus, j3pGetRandomFloat, j3pGetRandomInt, j3pPaletteMathquill, j3pStyle, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        DENIAUD Rémi
        avril 2020
        Lire graphiquement une limite d’une fonction
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['typeLim', 'quelconque', 'liste', 'Ce paramètre de se limiter à des limites en l’infini, en un réel ou avoir les deux cas de figure.', ['quelconque', 'infini', 'reel']]
  ]
}

/**
 * section limiteGraphique
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    let i
    j3pDetruit(stor.zoneCons3)
    j3pDetruit(stor.zoneCons4)
    const explications = j3pAddElt(stor.conteneur, 'div', '', {
      style: {
        paddingTop: '15px',
        color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color
      }
    })
    for (i = 1; i <= 3; i++) stor['zoneexplication' + i] = j3pAddElt(explications, 'div')
    const corr1 = (stor.nbLim === '+\\infty')
      ? (stor.limiteRep === '+\\infty')
          ? ds.textes.corr3
          : (stor.limiteRep === '-\\infty') ? ds.textes.corr4 : ds.textes.corr5
      : (stor.nbLim === '-\\infty')
          ? (stor.limiteRep === '+\\infty')
              ? ds.textes.corr6
              : (stor.limiteRep === '-\\infty') ? ds.textes.corr7 : ds.textes.corr8
          : (String(stor.nbLim).charAt(String(stor.nbLim).length - 1) === '+')
              ? (stor.limiteRep === '+\\infty') ? ds.textes.corr1_2 : ds.textes.corr2_2
              : (String(stor.nbLim).charAt(String(stor.nbLim).length - 1) === '-')
                  ? (stor.limiteRep === '+\\infty') ? ds.textes.corr1_3 : ds.textes.corr2_3
                  : (stor.limiteRep === '+\\infty') ? ds.textes.corr1_1 : ds.textes.corr2_1
    const a1 = ((String(stor.nbLim).charAt(String(stor.nbLim).length - 1) === '+') || (String(stor.nbLim).charAt(String(stor.nbLim).length - 1) === '-'))
      ? String(stor.nbLim).substring(0, String(stor.nbLim).length - 2)
      : String(stor.nbLim)
    j3pAffiche(stor.zoneexplication1, '', corr1, {
      a: stor.nbLim,
      l: stor.limiteRep,
      f: stor.nomf,
      a1
    })
    j3pAffiche(stor.zoneexplication2, '', ds.textes.corr9, {
      a: stor.nbLim,
      l: stor.limiteRep,
      f: stor.nomf
    })
    j3pAffiche(stor.zoneexplication3, '', ds.textes.corr10)
    modifFig({ correction: true })
  }

  function depart () {
    stor.mtgAppLecteur.removeAllDoc()
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQLAAADqQAAAQEAAAAAAAAAAAAAAJv#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFkAAItM#####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQAgAAAAAAAAAAAACAP####8ACGFmZmljaGU1AAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAhhZmZpY2hlNAABMAAAAAEAAAAAAAAAAAAAAAIA#####wAIYWZmaWNoZTMAATAAAAABAAAAAAAAAAAAAAACAP####8ACGFmZmljaGUyAAEwAAAAAQAAAAAAAAAAAAAAAgD#####AAhhZmZpY2hlMQABMQAAAAE#8AAAAAAAAP####8AAAABAApDUG9pbnRCYXNlAP####8AAAAAAA4AAU8AwCgAAAAAAAAAAAAAAAAAAAUAAEBxCAAAAAAAQHDszMzMzM3#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQABAAAABwE#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wAAAP8BDgABSQDAGAAAAAAAAAAAAAAAAAAABQAAQEWAAAAAAAAAAAAI#####wAAAAEACUNEcm9pdGVBQgD#####AAAAAAAQAAABAAEAAAAHAAAACf####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8AAAAAABAAAAEAAQAAAAcAAAAK#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAABAAAABwAAAAn#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAACwAAAAz#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAAQAAABBQABAAAADQAAAAsA#####wAAAAABDgABSgDAKAAAAAAAAMAQAAAAAAAABQACAAAADf####8AAAACAAdDUmVwZXJlAP####8A5ubmAAEAAAAHAAAACQAAAA8BAQAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABP#AAAAAAAAAAAAABP#AAAAAAAAD#####AAAAAQAKQ1VuaXRleFJlcAD#####AAR1bml0AAAAEP####8AAAABAAtDSG9tb3RoZXRpZQD#####AAAAB#####8AAAABAApDT3BlcmF0aW9uAwAAAAE#8AAAAAAAAP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAR#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAAAABAAAlciAQEAAAAACQAAABL#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAAHAAAAEwAAAAIA#####wAHbmJncmFkeAACMjAAAAABQDQAAAAAAAAAAAACAP####8AB25iZ3JhZHkAAjIwAAAAAUA0AAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AFEdyYWR1YXRpb25BeGVzUmVwZXJlAAAAGwAAAAgAAAADAAAAEAAAABUAAAAW#####wAAAAEAE0NBYnNjaXNzZU9yaWdpbmVSZXAAAAAAFwAFYWJzb3IAAAAQ#####wAAAAEAE0NPcmRvbm5lZU9yaWdpbmVSZXAAAAAAFwAFb3Jkb3IAAAAQAAAADQAAAAAXAAZ1bml0ZXgAAAAQ#####wAAAAEACkNVbml0ZXlSZXAAAAAAFwAGdW5pdGV5AAAAEP####8AAAABABBDUG9pbnREYW5zUmVwZXJlAAAAABcAAAAAABAAAAEFAAAAABAAAAAQAAAAGAAAABAAAAAZAAAAFwAAAAAXAAAAAAAQAAABBQAAAAAQAAAADwAAAAAQAAAAGAAAABAAAAAaAAAAEAAAABkAAAAXAAAAABcAAAAAABAAAAEFAAAAABAAAAAQAAAAGAAAAA8AAAAAEAAAABkAAAAQAAAAGwAAAA4AAAAAFwAAABwAAAAQAAAAFQAAABEAAAAAFwAAAAAAEAAAAQUAAAAAHQAAAB8AAAAOAAAAABcAAAAcAAAAEAAAABYAAAARAAAAABcAAAAAABAAAAEFAAAAAB4AAAAh#####wAAAAEACENTZWdtZW50AAAAABcBAAAAABAAAAEAAQAAAB0AAAAgAAAAGAAAAAAXAQAAAAAQAAABAAEAAAAeAAAAIgAAAAYAAAAAFwEAAAAACwABVwDAFAAAAAAAAMA0AAAAAAAABQABP9xWeJq83w4AAAAj#####wAAAAIACENNZXN1cmVYAAAAABcABnhDb29yZAAAABAAAAAlAAAAAgAAAAAXAAVhYnN3MQAGeENvb3JkAAAAEAAAACb#####AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAABcAZmZmAAAAJQAAABAAAAAVAAAAJQAAAAIAAAAlAAAAJQAAAAIAAAAAFwAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAAPAQAAAA8CAAAAAUAAAAAAAAAAAAAAEAAAABgAAAAQAAAAJwAAABcAAAAAFwEAAAAAEAAAAQUAAAAAEAAAABAAAAApAAAAEAAAABkAAAAaAQAAABcAZmZmAAAAKgAAABAAAAAVAAAAJQAAAAUAAAAlAAAAJgAAACcAAAApAAAAKgAAAAYAAAAAFwEAAAAACwABUgBAIAAAAAAAAMAgAAAAAAAABQABP9EbToG06B8AAAAk#####wAAAAIACENNZXN1cmVZAAAAABcABnlDb29yZAAAABAAAAAsAAAAAgAAAAAXAAVvcmRyMQAGeUNvb3JkAAAAEAAAAC0AAAAaAQAAABcAZmZmAAAALAAAABAAAAAWAAAALAAAAAIAAAAsAAAALAAAAAIAAAAAFwAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAAPAQAAAA8CAAAAAUAAAAAAAAAAAAAAEAAAABkAAAAQAAAALgAAABcAAAAAFwEAAAAAEAAAAQUAAAAAEAAAABAAAAAYAAAAEAAAADAAAAAaAQAAABcAZmZmAAAAMQAAABAAAAAWAAAALAAAAAUAAAAsAAAALQAAAC4AAAAwAAAAMf####8AAAACAAxDQ29tbWVudGFpcmUAAAAAFwFmZmYAAAAAAAAAAABAGAAAAAAAAAAAACULAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MSkAAAAaAQAAABcAZmZmAAAAMwAAABAAAAAVAAAAJQAAAAQAAAAlAAAAJgAAACcAAAAzAAAAHAAAAAAXAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAKgsAAf###wAAAAEAAAAAAAAAAQAAAAAAAAAAAAsjVmFsKGFic3cyKQAAABoBAAAAFwBmZmYAAAA1AAAAEAAAABUAAAAlAAAABgAAACUAAAAmAAAAJwAAACkAAAAqAAAANQAAABwAAAAAFwFmZmYAwCAAAAAAAAA#8AAAAAAAAAAAACwLAAH###8AAAACAAAAAQAAAAEAAAAAAAAAAAALI1ZhbChvcmRyMSkAAAAaAQAAABcAZmZmAAAANwAAABAAAAAWAAAALAAAAAQAAAAsAAAALQAAAC4AAAA3AAAAHAAAAAAXAWZmZgDAHAAAAAAAAAAAAAAAAAAAAAAAMQsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKG9yZHIyKQAAABoBAAAAFwBmZmYAAAA5AAAAEAAAABYAAAAsAAAABgAAACwAAAAtAAAALgAAADAAAAAxAAAAOQAAAAIA#####wABYQACLTMAAAADAAAAAUAIAAAAAAAAAAAAAgD#####AAFiAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFjAAItMgAAAAMAAAABQAAAAAAAAAAAAAAEAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAABQAAQIgcAAAAAABAd0zMzMzMzQAAAAIA#####wAFbWluaTEAAi00AAAAAwAAAAFAEAAAAAAAAAAAAAIA#####wAFbWF4aTEAATQAAAABQBAAAAAAAAAAAAACAP####8ABHBhczEAATEAAAABP#AAAAAAAAAAAAATAP####8AB0N1cnNldXIAAAAKAAAABgAAAAQAAAA#AAAAQAAAAEEAAAA+AAAABQAAAABCAQAAAAAQAAABAAEAAAA+AT#wAAAAAAAAAAAABgEAAABCAH9#fwAQAAAAAAAAAAAAAABACAAAAAAAAAUAAEBhgAAAAAAAAAAAQwAAAAIAAAAAQgAHbmJncmFkMAAdaW50KChtYXhpMS1taW5pMSkvcGFzMSswLjUpKzEAAAAPAP####8AAAACAAlDRm9uY3Rpb24CAAAADwAAAAAPAwAAAA8BAAAAEAAAAEAAAAAQAAAAPwAAABAAAABBAAAAAT#gAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEAC0NQb2ludENsb25lAAAAAEIBAAAAAAsAAk8xAMA4AAAAAAAAP#AAAAAAAAABAAAAAD4AAAAYAQAAAEIAf39#ABAAAAEAAQAAAEYAAABEAAAACAAAAABCAQAAAAAQAAABAAEAAABGAAAARwAAAAYAAAAAQgEAAAAACwACSjEAwDYAAAAAAADALgAAAAAAAAEAAb++GRTHsP1ZAAAASAAAAA4AAAAAQgAAAEYAAAAPAwAAABAAAABBAAAADwEAAAAQAAAAQAAAABAAAAA#AAAAEQAAAABCAQAAAAALAAJJMQAAAAAAAAAAAEAAAAAAAAAAAQAAAABEAAAASgAAAAwAAAAAQgAAAAABAQAAAEYAAABLAAAASQAAAAAAABAAAAA#AAAAAQAAAAAAAAAAAAAAEAAAAEEAAAABP#AAAAAAAAAAAAAGAAAAAEIBAAAAAAsAAlcxAMAgAAAAAAAAwDsAAAAAAAAFAAE#4RgxTm5FOgAAAEcAAAAaAQAAAEIAAAAAAAAATQAAABAAAABFAAAATQAAAAIAAABNAAAATf####8AAAABAA1DUG9pbnRCYXNlRW50AQAAAEIAf39#ARAAAlQxAAAAAAAAAAAAQAgAAAAAAAABAAEAAABMQAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAD8AAAAQAAAAQAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAABkAAAAAQgAFbWVzYWIAAABMAAAATwAAAAIBAAAAQgACYTEAJGludChtZXNhYioxMDAwMDAwMDAwKzAuNSkvMTAwMDAwMDAwMAAAAA8DAAAAHQIAAAAPAAAAAA8CAAAAEAAAAFAAAAABQc3NZQAAAAAAAAABP+AAAAAAAAAAAAABQc3NZQAAAAD#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAAEIAf39#AAAAAAAAAAAAwCAAAAAAAAAAAABPDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAmE9AAAJAAAAUQAAAAQA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAFAABAiCQAAAAAAEB6zMzMzMzNAAAAAgD#####AAVtaW5pMgACLTUAAAADAAAAAUAUAAAAAAAAAAAAAgD#####AAVtYXhpMgABNQAAAAFAFAAAAAAAAAAAABMA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAFQAAABVAAAAUwAAAAUAAAAAVgEAAAAAEAAAAQABAAAAUwE#8AAAAAAAAAAAAAYBAAAAVgB#f38AEAAAAMAIAAAAAAAAP#AAAAAAAAAFAABAXUAAAAAAAAAAAFcAAAAOAAAAAFYAAABTAAAADwMAAAAQAAAAVAAAAA8BAAAAEAAAAFQAAAAQAAAAVQAAABEAAAAAVgEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAUAAAAAWAAAAFkAAAAOAAAAAFYAAABTAAAADwMAAAAPAQAAAAE#8AAAAAAAAAAAABAAAABUAAAADwEAAAAQAAAAVQAAABAAAABUAAAAEQAAAABWAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAABQAAAABYAAAAWwAAABgBAAAAVgB#f38AEAAAAQABAAAAUwAAAFgAAAAGAQAAAFYAf39#ARAAAmsxAMAAAAAAAAAAQAAAAAAAAAABAAE#4yUyUyUyUwAAAF3#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAQAAAFYAAmIxAAAAWgAAAFwAAABeAAAAIAEAAABWAH9#fwAAAAAAAAAAAMAYAAAAAAAAAAAAXg8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAJiPQAAAgAAAF8AAAAEAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAABQAAQIgkAAAAAABAfczMzMzMzQAAAAIA#####wAFbWluaTMAAi01AAAAAwAAAAFAFAAAAAAAAAAAAAIA#####wAFbWF4aTMAATUAAAABQBQAAAAAAAAAAAATAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAABiAAAAYwAAAGEAAAAFAAAAAGQBAAAAABAAAAEAAQAAAGEBP#AAAAAAAAAAAAAGAQAAAGQAAAAAABAAAADACAAAAAAAAD#wAAAAAAAABQAAQF1AAAAAAAAAAABlAAAADgAAAABkAAAAYQAAAA8DAAAAEAAAAGIAAAAPAQAAABAAAABiAAAAEAAAAGMAAAARAAAAAGQBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAFAAAAAGYAAABnAAAADgAAAABkAAAAYQAAAA8DAAAADwEAAAABP#AAAAAAAAAAAAAQAAAAYgAAAA8BAAAAEAAAAGMAAAAQAAAAYgAAABEAAAAAZAEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAUAAAAAZgAAAGkAAAAYAQAAAGQAAAAAABAAAAEAAQAAAGEAAABmAAAABgEAAABkAAAAAAEQAAFrAMAAAAAAAAAAQAAAAAAAAAAFAAE#6fufufufugAAAGsAAAAhAQAAAGQAAmMxAAAAaAAAAGoAAABsAAAAIAEAAABkAAAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAbA8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAJjPQAAAgAAAG3#####AAAAAQAFQ0ZvbmMA#####wACZjEAIChiLyh4LWEpK2Mqc3FydCh4LWEpK2QpL2FmZmljaGUxAAAADwMAAAAPAAAAAA8AAAAADwMAAAAQAAAAPAAAAA8B#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAABAAAAA7AAAADwIAAAAQAAAAPQAAAB0BAAAADwEAAAAjAAAAAAAAABAAAAA7AAAAEAAAAAEAAAAQAAAABgABeAAAAAYA#####wEAAAAAEAABeAAAAAAAAAAAAEAIAAAAAAAABQABv9clhBpi+nAAAAAKAAAAGQD#####AAJ4MQAAABAAAABwAAAAAgD#####AAJ5MQAGZjEoeDEp#####wAAAAEADkNBcHBlbEZvbmN0aW9uAAAAbwAAABAAAABxAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAEAAAABAAAABxAAAAEAAAAHL#####AAAAAgANQ0xpZXVEZVBvaW50cwD#####AAAAAAABAAAAcwAAAfQAAQAAAHAAAAAEAAAAcAAAAHEAAAByAAAAcwAAAAIA#####wAIYm9ybmVJbmYAAi0xAAAAAwAAAAE#8AAAAAAAAAAAAAIA#####wAIYm9ybmVTdXAAATIAAAABQAAAAAAAAAAAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAQAAAAEAAAAHUAAAABAAAAAAAAAAAAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAQAAAAEAAAAHYAAAABAAAAAAAAAAAAAAAYAP####8AAAD#ABAAAAEAAgAAAHcAAAB4AAAABgD#####AAAA#wEQAAFBAAAAAAAAAAAAQAgAAAAAAAAFAAE#wdxHcR3EdwAAAHkAAAAcAP####8AAAD#AL#wAAAAAAAAQCQAAAAAAAAAAAB6EgAAAAAAAQAAAAAAAAABAAAAAAAAAAAAAyNJeAAAACIA#####wACZjIAFihiLyh4LWEpXjIrYykvYWZmaWNoZTIAAAAPAwAAAA8AAAAADwMAAAAQAAAAPP####8AAAABAApDUHVpc3NhbmNlAAAADwEAAAAjAAAAAAAAABAAAAA7AAAAAUAAAAAAAAAAAAAAEAAAAD0AAAAQAAAABQABeAAAAAYA#####wEAAAAAEAACeDEAAAAAAAAAAABACAAAAAAAAAUAAb#5wiAHc22yAAAACgAAABkA#####wACeDIAAAAQAAAAfQAAAAIA#####wACeTIABmYyKHgyKQAAACQAAAB8AAAAEAAAAH4AAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAQAAAAEAAAAH4AAAAQAAAAfwAAACUA#####wAAAAAAAQAAAIAAAAH0AAEAAAB9AAAABAAAAH0AAAB+AAAAfwAAAIAAAAAiAP####8AAmYzABooZXhwKC14K2EpK2IqeCtjKS9hZmZpY2hlMwAAAA8DAAAADwAAAAAPAAAAAB0HAAAADwAAAAADAAAAIwAAAAAAAAAQAAAAOwAAAA8CAAAAEAAAADwAAAAjAAAAAAAAABAAAAA9AAAAEAAAAAQAAXgAAAAGAP####8BAAAAABAAAngyAAAAAAAAAAAAQAgAAAAAAAAFAAG#6ytvaUl#sAAAAAoAAAAZAP####8AAngzAAAAEAAAAIMAAAACAP####8AAnkzAAZmMyh4MykAAAAkAAAAggAAABAAAACEAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAEAAAABAAAACEAAAAEAAAAIUAAAAlAP####8AAAAAAAEAAACGAAAB9AABAAAAgwAAAAQAAACDAAAAhAAAAIUAAACGAAAAIgD#####AAJmNAAdKGIqZXhwKHgtYSkvKHgtYSkrYykvYWZmaWNoZTQAAAAPAwAAAA8AAAAADwMAAAAPAgAAABAAAAA8AAAAHQcAAAAPAQAAACMAAAAAAAAAEAAAADsAAAAPAQAAACMAAAAAAAAAEAAAADsAAAAQAAAAPQAAABAAAAADAAF4AAAABgD#####AQAAAAAQAAJ4MwAAAAAAAAAAAEAIAAAAAAAABQABQCNHgajTI2wAAAAKAAAAGQD#####AAJ4NAAAABAAAACJAAAAAgD#####AAJ5NAAGZjQoeDQpAAAAJAAAAIgAAAAQAAAAigAAABcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAABAAAAAQAAAAigAAABAAAACLAAAAJQD#####AAAAAAABAAAAjAAAAfQAAQAAAIkAAAAEAAAAiQAAAIoAAACLAAAAjAAAACIA#####wACZjUAHShjKmNvcyh4LWEpLyh4LWEpK2IpL2FmZmljaGU1AAAADwMAAAAPAAAAAA8DAAAADwIAAAAQAAAAPQAAAB0EAAAADwEAAAAjAAAAAAAAABAAAAA7AAAADwEAAAAjAAAAAAAAABAAAAA7AAAAEAAAADwAAAAQAAAAAgABeAAAAAYA#####wEAAAAAEAACeDQAAAAAAAAAAABACAAAAAAAAAUAAUAglngS2P1ZAAAACgAAABkA#####wACeDUAAAAQAAAAjwAAAAIA#####wACeTUABmY1KHg1KQAAACQAAACOAAAAEAAAAJAAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAQAAAAEAAAAJAAAAAQAAAAkQAAACUA#####wAAAAAAAQAAAJIAAAH0AAEAAACPAAAABAAAAI8AAACQAAAAkQAAAJIAAAAZAP####8ABHhyZXAAAAAQAAAAegAAAAIA#####wAIYWZmaWNoZXgAATEAAAABP#AAAAAAAAAAAAACAP####8ABHlyZXAAgHNpKGFmZmljaGV4LHNpKGFmZmljaGUxLGYxKHhyZXApLHNpKGFmZmljaGUyLGYyKHhyZXApLHNpKGFmZmljaGUzLGYzKHhyZXApLHNpKGFmZmljaGU0LGY0KHhyZXApLHNpKGFmZmljaGU1LGY1KHhyZXApLDApKSkpKSwtNTAp#####wAAAAEADUNGb25jdGlvbjNWYXIAAAAAEAAAAJUAAAAnAAAAABAAAAAGAAAAJAAAAG8AAAAQAAAAlAAAACcAAAAAEAAAAAUAAAAkAAAAfAAAABAAAACUAAAAJwAAAAAQAAAABAAAACQAAACCAAAAEAAAAJQAAAAnAAAAABAAAAADAAAAJAAAAIgAAAAQAAAAlAAAACcAAAAAEAAAAAIAAAAkAAAAjgAAABAAAACUAAAAAQAAAAAAAAAAAAAAAwAAAAFASQAAAAAAAAAAABcA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAABAAAAAQAAAAlAAAABAAAACWAAAAFwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAEAAAAAEAAAAAAAAAAAAAABAAAACWAAAAGAD#####AAAA#wAQAAABAQIAAAB6AAAAlwAAABgA#####wAAAP8AEAAAAQECAAAAlwAAAJgAAAAU##########8='
    stor.mtgAppLecteur.addDoc(stor.mtg32svg, txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
  }

  function modifFig (st) {
    // cette fonction sert à modifier la figure de base
    // st contient une propriété correction (booléen)
    if (st.correction) {
      // on affiche la correction
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affichex', '1')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'borneInf', String(stor.bornesFig.borneInf))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'borneSup', String(stor.bornesFig.borneSup))
    } else {
      // ceci sert si jamais on souhaite modifier la courbe initiale dans la question
      for (let i = 1; i < 6; i++) {
        stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affiche' + i, String(stor.tabAfficheCb[i - 1]))
      }
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'a', String(stor.donneesCb.a))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'b', String(stor.donneesCb.b))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'c', String(stor.donneesCb.c))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'd', String(stor.donneesCb.d))
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'borneInf', '-50')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'borneSup', '-50')
      stor.mtgAppLecteur.giveFormula2(stor.mtg32svg, 'affichex', '0')
    }
    stor.mtgAppLecteur.calculate(stor.mtg32svg, true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display(stor.mtg32svg)// on affiche la nouvelle figure
  }

  function genereDonnees (numCb) {
    const obj = {}
    const choix = j3pGetRandomInt(0, 1)
    const choix2 = j3pGetRandomInt(0, 1)
    obj.tabLimReel = [] // ce tableau contiendra des tableaux de deux éléments : l’endroit où on cherche la limite et la valeur de cette limite (limite en un réel ici)
    obj.tabLimInf = [] // ce tableau contiendra des tableaux de deux éléments : l’endroit où on cherche la limite et la valeur de cette limite (limite en +inf ou -inf ici)
    for (let i = 1; i < 6; i++) {
      stor.tabAfficheCb.push(0)
    }
    stor.tabAfficheCb[numCb - 1] = 1
    switch (numCb) {
      case 1:
        // f(x)=b/(x-a)+csqrt(x-a)+d
        obj.a = j3pGetRandomInt(-4, -1)
        obj.d = 0
        if (choix === 0) {
          obj.b = j3pGetRandomFloat(0.5, 1)
          obj.c = (choix2 === 0) ? j3pGetRandomFloat(3, 4) : j3pGetRandomFloat(-3, -2)
          obj.tabLimReel.push([obj.a, '+\\infty'])
          if (choix2 === 0) {
            obj.tabLimInf.push(['+\\infty', '+\\infty'])
            obj.d = j3pGetRandomInt(-5, -3)
          } else {
            obj.tabLimInf.push(['+\\infty', '-\\infty'])
          }
        } else {
          obj.b = j3pGetRandomFloat(-1, -0.5)
          obj.c = (choix2 === 0) ? j3pGetRandomFloat(1.5, 2.5) : j3pGetRandomFloat(-2.5, -1.8)
          obj.tabLimReel.push([obj.a, '-\\infty'])
          if (choix2 === 0) {
            obj.tabLimInf.push(['+\\infty', '+\\infty'])
            obj.d = j3pGetRandomInt(-5, -3)
          } else {
            obj.tabLimInf.push(['+\\infty', '-\\infty'])
          }
        }
        break
      case 2:
        // f(x)=b/(x-a)^2+c
        obj.a = j3pGetRandomInt(-3, 3)
        if (choix === 0) {
          obj.b = j3pGetRandomFloat(-2, -0.5)
          obj.c = j3pGetRandomInt(1, 4)
          obj.tabLimReel.push([obj.a + '^+', '-\\infty'], [obj.a + '^-', '-\\infty'])
        } else {
          obj.b = j3pGetRandomFloat(0.5, 2)
          obj.c = j3pGetRandomInt(-4, -1)
          obj.tabLimReel.push([obj.a + '^+', '+\\infty'], [obj.a + '^-', '+\\infty'])
        }
        obj.tabLimInf.push(['+\\infty', String(obj.c)], ['-\\infty', String(obj.c)])
        break
      case 3:
        // f(x)=exp(-x+a)+bx+c
        obj.a = j3pGetRandomFloat(-3, 1)
        if (choix === 0) {
          obj.b = j3pGetRandomFloat(0.5, 1.2)
          obj.c = j3pGetRandomFloat(-3, 1)
          obj.tabLimInf.push(['+\\infty', '+\\infty'], ['-\\infty', '+\\infty'])
        } else {
          obj.b = j3pGetRandomFloat(-1, -0.5)
          obj.c = j3pGetRandomFloat(-1, 2)
          obj.tabLimInf.push(['+\\infty', '-\\infty'], ['-\\infty', '+\\infty'])
        }
        obj.tabLimReel.push()
        break
      case 4:
        // f(x)=bexp(x-a)/(x-a)+c
        obj.a = j3pGetRandomInt(-3, 2)
        if (choix === 0) {
          obj.b = j3pGetRandomFloat(-1, -0.5)
          obj.c = j3pGetRandomInt(-3, -1)
          obj.tabLimReel.push([obj.a + '^+', '-\\infty'], [obj.a + '^-', '+\\infty'])
          obj.tabLimInf.push(['+\\infty', '-\\infty'], ['-\\infty', obj.c])
        } else {
          obj.b = j3pGetRandomFloat(0.5, 1)
          obj.c = j3pGetRandomInt(1, 3)
          obj.tabLimReel.push([obj.a + '^+', '+\\infty'], [obj.a + '^-', '-\\infty'])
          obj.tabLimInf.push(['+\\infty', '+\\infty'], ['-\\infty', obj.c])
        }
        obj.tabLimReel.push()
        break
      case 5:
        // f(x)=c*cos(x-a)/(x-a)+b
        obj.a = j3pGetRandomInt(-2, 2)
        obj.b = j3pGetRandomInt(-2, 2)
        if (choix === 0) {
          obj.c = j3pGetRandomFloat(-0.6, -0.4)
          obj.tabLimReel.push([obj.a + '^+', '-\\infty'], [obj.a + '^-', '+\\infty'])
          if (obj.a === 0) obj.tabLimInf.push(['+\\infty', obj.b], ['-\\infty', obj.b])
          else if (obj.a > 0) obj.tabLimInf.push(['-\\infty', obj.b])
          else obj.tabLimInf.push(['+\\infty', obj.b])
        } else {
          obj.c = j3pGetRandomFloat(0.4, 0.6)
          obj.tabLimReel.push([obj.a + '^+', '+\\infty'], [obj.a + '^-', '-\\infty'])
          if (obj.a === 0) obj.tabLimInf.push(['+\\infty', obj.b], ['-\\infty', obj.b])
          else if (obj.a > 0) obj.tabLimInf.push(['-\\infty', obj.b])
          else obj.tabLimInf.push(['+\\infty', obj.b])
        }
        obj.tabLimReel.push()
        break
    }
    me.logIfDebug('obj:', obj, 'numCb:', numCb)
    return obj
  }
  function f (x, num, obj) {
    // x est la valeur dont on cherche l’image
    // num est le numéro du cas de figure
    // obj contient les variables a, b et c
    if (num === 1) {
      // f(x)=b/(x-a)+csqrt(x-a)
      return obj.b / (x - obj.a) + obj.c * Math.sqrt(x - obj.a)
    } else if (num === 2) {
      // f(x)=b/(x-a)^2+c
      return obj.b / Math.pow((x - obj.a), 2) + obj.c
    } else if (num === 3) {
      // f(x)=exp(-x+a)+bx+c
      return Math.exp(-x + obj.a) + obj.b * x + obj.c
    } else if (num === 4) {
      // f(x)=bexp(x-a)/(x-a)+c
      return obj.b * Math.exp(x - obj.a) / (x - obj.a) + obj.c
    } else if (num === 5) {
      // f(x)=c*cos(x-a)/(x-a)+b
      return obj.c * Math.cos(x - obj.a) / (x - obj.a) + obj.b
    }
  }
  function genereBornes (num, xlim) {
    // num est le cas de figure
    // xlim est l’endroit où on demande de déterminer la limite
    // cette fonction renvoie un objet avec les propriétés borneInf et borneSup : intervalle sur lequel se déplacera $x$ pendant la correction
    const obj = {}
    let valx
    const pas = 0.02
    if (String(xlim).indexOf('infty') === -1) {
      if (String(xlim).charAt(String(xlim).length - 1) === '-') {
        obj.borneSup = Number(String(xlim).substring(0, String(xlim).length - 2))
        obj.borneInf = obj.borneSup - 0.7
      } else if (String(xlim).charAt(String(xlim).length - 1) === '+') {
        obj.borneInf = Number(String(xlim).substring(0, String(xlim).length - 2))
        obj.borneSup = obj.borneInf + 0.7
      } else {
        obj.borneInf = Number(xlim + 0.05)
        obj.borneSup = obj.borneInf + 0.7
      }
    } else if (xlim === '+\\infty') {
      valx = 6.2
      while (Math.abs(f(valx, num, stor.donneesCb)) > 6.3) {
        valx -= pas
      }
      obj.borneInf = valx - 0.7
      obj.borneSup = valx + 0.2
    } else {
      // xlim vaut -\\infty
      valx = -6.2
      while (Math.abs(f(valx, num, stor.donneesCb)) > 6.3) {
        valx += pas
      }
      obj.borneInf = valx - 0.2
      obj.borneSup = valx + 0.7
    }
    return obj
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 5,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zonesElts.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]
      video: [],
      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1',

      // Paramètres de la section, avec leurs valeurs par défaut.
      typeLim: 'quelconque',
      /*
  Les textes présents dans la section
  Sont donc accessibles dans le code par ds.textes.consigne1
  Possibilité d’écrire dans le code :
  var lestextes = ds.textes;
  puis accès à l’aide de lestextes.consigne1
  */
      textes: {
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        titre_exo: 'Lecture graphique d’une limite',
        // on donne les phrases de la consigne
        consigne1: 'Soit $£f$ la fonction dont on donne une représentation graphique ci-contre.',
        consigne2_1: 'A l’aide de la courbe, $£l$ semble valoir &1&.',
        consigne2_2: 'Sachant que le prolongement de la courbe se fait dans la continuité de la partie apparente, $£l$ semble valoir &1&.',
        rmq: 'Tu n’as le droit qu’à un seul essai.',
        // les différents commentaires envisagés suivant les réponses fausses de l’élèves
        comment1: '',
        // et les phrases utiles pour les explications de la réponse
        corr1_1: 'Quand $x$ est proche de $£a$, les images par $£f$ deviennent très grandes, la fonction tend ainsi vers $+\\infty$.',
        corr2_1: 'Quand $x$ est proche de $£a$, les images par $£f$ deviennent très petites, la fonction tend ainsi vers $+\\infty$.',
        corr1_2: 'Quand $x$ est proche de $£{a1}$ en restant supérieur à $£{a1}$, les images par $£f$ deviennent très grandes, la fonction tend ainsi vers $+\\infty$.',
        corr2_2: 'Quand $x$ est proche de $£{a1}$ en restant supérieur à $£{a1}$, les images par $£f$ deviennent très petites, la fonction tend ainsi vers $-\\infty$.',
        corr1_3: 'Quand $x$ est proche de $£{a1}$ en restant inférieur à $£{a1}$, les images par $£f$ deviennent très grandes, la fonction tend ainsi vers $+\\infty$.',
        corr2_3: 'Quand $x$ est proche de $£{a1}$ en restant inférieur à $£{a1}$, les images par $£f$ deviennent très petites, la fonction tend ainsi vers $-\\infty$.',
        corr3: 'Quand $x$ devient très grand (tend vers $+\\infty$), les images par $£f$ deviennent très grandes, la fonction tend ainsi vers $+\\infty$.',
        corr4: 'Quand $x$ devient très grand (tend vers $+\\infty$), les images par $£f$ deviennent très petites, la fonction tend ainsi vers $-\\infty$.',
        corr5: 'Quand $x$ devient très grand (tend vers $+\\infty$), les images par $£f$ deviennent très proches de $£l$, la fonction tend ainsi vers $£l$.',
        corr6: 'Quand $x$ devient très petit (tend vers $-\\infty$), les images par $£f$ deviennent très grandes, la fonction tend ainsi vers $+\\infty$.',
        corr7: 'Quand $x$ devient très petit (tend vers $-\\infty$), les images par $£f$ deviennent très petites, la fonction tend ainsi vers $-\\infty$.',
        corr8: 'Quand $x$ devient très petit (tend vers $-\\infty$), les images par $£f$ deviennent très proches de $£l$, la fonction tend ainsi vers $£l$.',
        corr9: 'Donc $\\limite{x}{£a}{£f(x)}=£l$.',
        corr10: 'Il suffit de déplacer le point de coordonnées $(x\\quad ;\\quad 0)$ pour s’en rendre compte.'
      },

      pe: 0
    }
  }

  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    // Construction de la page

    // Le paramètre définit la largeur relative de la première colonne
    stor.pourcentage = 0.5
    // j’ai choisi de mettre ce pourcentage en paramètre pour centrer la figure par la suite

    me.construitStructurePage({ structure: ds.structure, ratioGauche: stor.pourcentage })

    /*
     Par convention,`
    `   stor.typederreurs[0] = nombre de bonnes réponses
        stor.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        stor.typederreurs[2] = nombre de mauvaises réponses
        stor.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    stor.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)

    let tabCb = [1, 2, 3, 4, 5]
    stor.listeCb = []
    for (let j = 0; j < ds.nbrepetitions; j++) {
      const pioche = j3pGetRandomInt(0, tabCb.length - 1)
      stor.listeCb.push(tabCb[pioche])
      tabCb.splice(pioche, 1)
      if (tabCb.length === 0) tabCb = [1, 2, 3, 4, 5]
    }
    // choix du type de limite
    if ((ds.typeLim !== 'reel') && (ds.typeLim !== 'infini')) {
      const tabTypeLim = ['reel', 'infini']
      stor.tabTypeLim = []
      for (let j = 0; j < Math.ceil(ds.nbrepetitions / 2); j++) {
        stor.tabTypeLim = stor.tabTypeLim.concat(tabTypeLim)
      }
      stor.tabTypeLim = j3pShuffle(stor.tabTypeLim)
    } else {
      stor.typeLim = ds.typeLim
    }
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function enonceInitThen () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    enonceMain()
  }

  function enonceMain () {
    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    /// /////////////////////////////////////// Pour MathGraph32
    const largFig = 540// on gère ici la largeur de la figure
    const hautFig = 540
    // création du div accueillant la figure mtg32
    stor.zoneMD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneCorr = j3pAddElt(stor.zoneMD, 'div')
    stor.zoneMtg32 = j3pAddElt(stor.zoneMD, 'div')

    // et enfin la zone svg (très importante car tout est dedans)
    stor.mtg32svg = j3pGetNewId('mtg32svg')
    j3pCreeSVG(stor.zoneMtg32, {
      id: stor.mtg32svg,
      width: largFig,
      height: hautFig
    })
    if ((ds.typeLim !== 'reel') && (ds.typeLim !== 'infini')) {
      stor.typeLim = stor.tabTypeLim[me.questionCourante - 1]
    }
    if (stor.typeLim === 'reel' && stor.listeCb[me.questionCourante - 1] === 3) {
      // Dans le cas N°3, je n’ai pas de limite en un réel, donc il faut prendre un autre cas de figure
      let alea
      do {
        alea = j3pGetRandomInt(1, 5)
      } while (alea === 3)
      stor.listeCb[me.questionCourante - 1] = alea
    }
    stor.tabAfficheCb = []
    stor.donneesCb = genereDonnees(stor.listeCb[me.questionCourante - 1])
    depart(stor.donneesCb)

    /// //////////////////////////////////////Fin de l’initialisation de la figure MathGraph32'
    let i
    const tabnomF = ['f', 'g', 'h']
    stor.nomf = tabnomF[j3pGetRandomInt(0, 2)]
    // création du conteneur dans lequel se trouveront toutes les conaignes
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    let pos
    if (stor.typeLim === 'reel') {
      pos = j3pGetRandomInt(0, stor.donneesCb.tabLimReel.length - 1)
      stor.nbLim = stor.donneesCb.tabLimReel[pos][0]
      stor.limiteRep = stor.donneesCb.tabLimReel[pos][1]
    } else {
      pos = j3pGetRandomInt(0, stor.donneesCb.tabLimInf.length - 1)
      stor.nbLim = stor.donneesCb.tabLimInf[pos][0]
      stor.limiteRep = stor.donneesCb.tabLimInf[pos][1]
    }
    stor.bornesFig = genereBornes(stor.listeCb[me.questionCourante - 1], stor.nbLim)
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    for (i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', ds.textes.consigne1, { f: stor.nomf })
    const laConsigne2 = (String(stor.nbLim).indexOf('infty') > -1) ? ds.textes.consigne2_2 : ds.textes.consigne2_1
    stor.elt = j3pAffiche(stor.zoneCons2, '', laConsigne2,
      {
        l: '\\limite{x}{' + stor.nbLim + '}{' + stor.nomf + '(x)}',
        inputmq1: { texte: '' }
      })
    mqRestriction(stor.elt.inputmqList[0], '-+\\d.,', {
      commandes: ['inf']
    })
    j3pStyle(stor.zoneCons3, { paddingTop: '10px' })
    j3pPaletteMathquill(stor.zoneCons3, stor.elt.inputmqList[0], { liste: ['inf'] })
    stor.zoneCons4.style.marginTop = '40px'
    j3pAffiche(stor.zoneCons4, '', ds.textes.rmq, {
      style: {
        fontStyle: 'italic',
        fontSize: '0.9em'
      }
    })
    modifFig({ correction: false })

    stor.elt.inputmqList[0].typeReponse = ['texte']
    stor.elt.inputmqList[0].reponse = [String(stor.limiteRep)]
    const mesZonesSaisie = [stor.elt.inputmqList[0].id]
    stor.fctsValid = new ValidationZones({
      parcours: me,
      zones: mesZonesSaisie
    })
    j3pFocus(stor.elt.inputmqList[0])
    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    // Obligatoire
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        enonceInitThen()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      {
        const reponse = stor.fctsValid.validationGlobale()
        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          this.reponseManquante(stor.zoneCorr)
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            afficheCorrection(true)
            stor.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              stor.typederreurs[10]++
              me.cacheBoutonValider()
              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                stor.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorrection(false)
                stor.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
