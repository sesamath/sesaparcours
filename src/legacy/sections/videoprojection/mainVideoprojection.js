import { j3pAddElt, j3pSetProps, j3pShowError } from 'src/legacy/core/functions'
import { j3pMathsAjouteDans } from 'src/lib/mathquill/functions'
import loadMathquill from 'src/lib/mathquill/loadMathquill'

/**
 * Module commun à toutes les sections de vidéoprojections
 * getMainFunction retourne la fonction main que chacune devra utiliser.
 * Le fonctionnement est un peu différent car
 * - il y a un message d’attente avant d’afficher le premier énoncé (avec demande du temps limite ou pas suivant
 *   C’est l’appui sur une touche ou un clic|touch qui déclenchera l’énoncé.
 * - on affiche tous les énoncés à la suite (touche ou clic|touch)
 * - après le dernier énoncé, si y’a pas de correction à afficher (type_presentation=aucune) on affiche le bouton section suivante,
 *   sinon on attend un déclenchement et on affiche les corrections, soit toutes ensemble (rapide), soit une par une (details),
 *   et dans ce cas en effaçant ou pas la correction précédente (param nettoiecorrection)
 * Cette gestion oblige à faire des choses peu orthodoxes, comme appeler des méthodes de parcours
 * qui d’habitude ne sont pas sensées l'être par les sections (_stopTimer, afficher/cacher les boutons).
 * @module legacy/sections/videoprojection/mainVideoprojection
 */

const color1 = '#a82000'
const color2 = '#0a0a93'
const padding = '15px'
const propsUn = { style: { color: color1, padding } }
const propsDeux = { style: { color: color2, padding } }

// le style qui sera appliqué par défaut à MG (& MD)
const defaultMainStyle = {
  fontSize: '1.2em',
  padding: '1rem'
}

const msgStartEnonce = 'Appuyer sur une touche ou toucher l’écran tactile pour démarrer (puis changer de question).'
const msgElapsed = 'Temps écoulé, appuyez sur une touche ou toucher l’écran tactile pour passer à la suite.'
const msgStartCorrection = 'Appuyer sur une touche ou toucher l’écran tactile pour afficher les corrections.'
const msgTempsLimite = 'On peut imposer un temps limite en secondes pour chaque question (0 signifiera que le temps ne sera pas limité) : '

/**
 * Les paramètres communs à toutes les sections de videoprojection
 * @return LegacySectionParamsParametres
 */
export function getParametres ({
  nbrepetitions = 2,
  demandeLimite = false,
  limite = 120,
  // eslint-disable-next-line camelcase
  nb_calcul = 2,
  // eslint-disable-next-line camelcase
  type_presentation = 'rapide',
  nettoiecorrection = false
} = {}) {
  return [
    ['nbrepetitions', nbrepetitions, 'entier', 'Le nombre de questions'],
    ['demandeLimite', demandeLimite, 'boolean', 'Mettre true pour demander le temps limite sur le premier écran'],
    ['limite', limite, 'number', 'Le temps limite d’affichage (en s), mettre 0 pour ne pas en avoir'],
    // eslint-disable-next-line camelcase
    ['nb_calcul', nb_calcul, 'entier', 'Mettre 2 pour afficher 2 calculs en même temps'],
    // eslint-disable-next-line camelcase
    ['type_presentation', type_presentation, 'liste', 'présentation possible pour la correction : details ou rapide ou aucune', ['rapide', 'details', 'aucune']],
    ['nettoiecorrection', nettoiecorrection, 'boolean', 'Dans le cas détails, pour effacer les corrections à chaque étape (sinon elles sont affichées progressivement à la suite les unes des autres)']
  ]
}

export function getMainFunction ({ getEnoncesCorrections, surcharge }) {
  /**
   * @this {Parcours}
   */
  function main () {
    this.setPassive()
    /** @type {Parcours} */
    const me = this

    function addListeners () {
      window.addEventListener('keydown', next)
      me.conteneur.addEventListener('touchstart', next, { passive: true })
      me.conteneur.addEventListener('click', next)
    }

    function clearListeners () {
      window.removeEventListener('keydown', next)
      me.conteneur.removeEventListener('touchstart', next, { passive: true })
      me.conteneur.removeEventListener('click', next)
    }

    /**
     * Listener générique pour avancer d’un cran (rappelle la section courante,
     * sauf la première fois où ça appelle firstEnonce)
     * @private
     */
    function next () {
      if (me.storage.isFirstEnonce) firstEnonce()
      else me.sectionCourante()
    }

    function init () {
      if (surcharge) {
        me.surcharge(surcharge)
      }
      // imposé pour toutes les sections videoprojection, car notre code se fie à nbrepetitions
      // et pas nbitems, plusieurs étapes n’auraient pas de sens ici.
      me.donneesSection.nbetapes = 1
      // on transforme ce param en booléen
      me.storage.isDouble = Number(me.donneesSection.nb_calcul) === 2
      // compatibilité ascendante, temps_affichage était utilisé comme paramètre pour toutes ces sections à la place de limite
      if (me.donneesSection.temps_affichage) me.donneesSection.limite = me.donneesSection.temps_affichage
      if (typeof me.donneesSection.limite === 'string') me.donneesSection.limite = Number(me.donneesSection.limite)

      const structure = me.storage.isDouble ? 'presentation1ter' : 'presentation2'
      me.construitStructurePage({ structure })
      if (me.donneesSection.titre) me.afficheTitre(me.donneesSection.titre, true)
      // style MG/MD
      const style = me.donneesSection.mainStyle || defaultMainStyle
      j3pSetProps(me.zonesElts.MG, { style })
      if (me.storage.isDouble) j3pSetProps(me.zonesElts.MD, { style })
      // on récupère les valeurs
      const { enonces, corrections } = getEnoncesCorrections(me.donneesSection)
      me.storage.enonces = enonces
      me.storage.corrections = corrections
      // un flag pour que next sache qu’il doit appeler afficheEnonce plutôt que la section
      me.storage.isFirstEnonce = true
      // init index des enoncés, faut gérer le notre car repetitionCourante est incrémenté à chaque appel de sectionCourante,
      // et avec nos messages d’attente le nb d’appels correspond pas
      me.storage.enonceCourant = 0
      // init index des corrections
      me.storage.correctionCourante = -1
      // pour compter le nb d’appels en état navigation
      me.storage.nbNavigationCalls = 0

      // et pour finir, la limite dynamique (on le demande ici) ou pas
      if (me.donneesSection.demandeLimite) {
        // on affiche un input pour saisir une limite
        const p = j3pAddElt(me.zonesElts.MG, 'p', msgTempsLimite)
        const form = j3pAddElt(p, 'form') // display block par défaut, ça ira à la ligne et ça nous arrange
        const input = j3pAddElt(form, 'input', '', { type: 'number', min: 0, max: 1200, value: me.donneesSection.limite })
        j3pAddElt(form, 'input', '', { type: 'submit', value: 'Démarrer', style: { margin: '0 1rem' } })
        form.addEventListener('submit', (event) => {
          event.preventDefault()
          const limite = Number(input.value)
          if (Number.isFinite(limite) && limite >= 0) me.donneesSection.limite = limite
          addListeners()
          // et on lance directement le premier énoncé
          firstEnonce()
        })
      } else {
        // le message d’attente
        j3pAddElt(me.zonesElts.MG, 'span', msgStartEnonce)
        // aj des listeners pour appeler next
        addListeners()
      }

      // on lance le chargement de mathquill dès maintenant
      loadMathquill().catch(error => {
        console.error(error)
        j3pShowError('Le chargement a échoué')
        // si on était dans le cas demandeLimite les listener ont pas forcément été mis, pas grave
        clearListeners()
      })
    }

    function firstEnonce () {
      // premier appel, le chargement de mathquill n’est pas forcément terminé
      me.videLesZones()
      j3pAddElt(me.zonesElts.MG, 'div', 'Chargement en cours…', { style: { padding: '1rem' } })
      loadMathquill().then(() => {
        me.storage.isFirstEnonce = false
        afficheEnonce()
      }).catch(error => {
        console.error(error)
        j3pShowError('Le chargement a échoué')
        clearListeners()
      })
    }

    function afficheEnonce () {
      // c’est normalement finCorrection qui appelle _stopTimer, mais on enchaîne les énoncés sans passer par la correction, il faut le faire ici
      me._stopTimer()
      me.videLesZones()
      const indexEnonce = me.storage.enonceCourant
      const numQuestion = indexEnonce + 1
      const [enonce1, enonce2] = me.storage.enonces[indexEnonce]

      // Le cas où on a été interrompu par le timer
      if (me.isElapsed) {
        j3pAddElt(me.zonesElts.MG, 'div', msgElapsed, propsUn)
        // si c'était la dernière question faut passer en correction
        if (indexEnonce >= me.donneesSection.nbrepetitions) me.etat = 'correction'
        me.isElapsed = false // sinon on repasserait là au prochain appel
        return
      }

      // affichage de la question courante
      const div1 = j3pAddElt(me.zonesElts.MG, 'div', '', propsUn)
      j3pMathsAjouteDans(div1, { content: `${numQuestion}) ${enonce1}` })
      // s’il y a deux calculs, affiche le second dans la zone MD (qui est sous MG dans cette structure particulière)
      if (me.storage.isDouble) {
        const div2 = j3pAddElt(me.zonesElts.MD, 'div', '', propsDeux)
        j3pMathsAjouteDans(div2, { content: `${numQuestion}) ${enonce2}` })
      }
      me.storage.enonceCourant++
      me.finEnonce() // il lance le _startTimer

      // mais on reste dans l’état énoncé s’il en reste à afficher (sinon l’appel de finEnonce ci-dessus nous a passé en état navigation car on est une section passive, sinon il nous aurait passé en correction)
      if (me.storage.enonceCourant < me.donneesSection.nbrepetitions) {
        me.etat = 'enonce'
      } else {
        // dans une section passive finEnonce passe d'office en navigation avec le bouton sectionSuivante
        me.etat = 'correction'
      }
    }

    function afficheCorrection () {
      if (me.donneesSection.type_presentation === 'aucune') {
        // pas de correction à afficher, c’est terminé
        return me.finCorrection('navigation', true)
      }
      if (me.storage.correctionCourante < 0) {
        // on affiche le message d’attente pour les corrections
        me.videLesZones()
        me._stopTimer()
        me.donneesSection.limite = 0 // sinon il laisse affiché le temps qui restait
        me._rafraichirEtat()
        j3pAddElt(me.zonesElts.MG, 'p', msgStartCorrection, { style: { padding } })
        me.storage.correctionCourante++
        // et on reste dans l'état correction
        return me.finCorrection()
      }

      // faut afficher une correction
      if (me.donneesSection.type_presentation === 'rapide') {
        // on affiche toute les corrections d’un coup
        me.videLesZones()
        for (let i = 0; i < me.donneesSection.nbrepetitions; i++) {
          const p = j3pAddElt(me.zonesElts.MG, 'p', '', propsUn)
          const [correction1, correction2] = me.storage.corrections[i]
          const numQuestion = i + 1
          j3pMathsAjouteDans(p, { content: `${numQuestion}) ${correction1}` })
          if (me.storage.isDouble) {
            const p = j3pAddElt(me.zonesElts.MD, 'p', '', propsDeux)
            j3pMathsAjouteDans(p, { content: `${numQuestion}) ${correction2}` })
          }
        }
        return me.finCorrection('navigation')
      }

      // on affiche les corrections une par une, à l’appui de la barre espace
      const indexCorrection = me.storage.correctionCourante
      if (indexCorrection === 0 || me.donneesSection.nettoiecorrection) {
        me.videLesZones()
      }
      if (indexCorrection >= me.donneesSection.nbrepetitions) {
        return me.finCorrection('navigation', true)
      }
      // on affiche cette correction
      const [correction1, correction2] = me.storage.corrections[indexCorrection]
      const numQuestion = indexCorrection + 1
      const p = j3pAddElt(me.zonesElts.MG, 'p', '', { style: { color: color1 } })
      j3pMathsAjouteDans(p, { content: `${numQuestion}) ${correction1}` })
      if (me.storage.isDouble) {
        const p = j3pAddElt(me.zonesElts.MD, 'p', '', { style: { color: color2 } })
        j3pMathsAjouteDans(p, { content: `${numQuestion}) ${correction2}` })
      }
      me.storage.correctionCourante++
      me.finCorrection()
    } // afficheCorrection

    switch (this.etat) {
      case 'enonce':
        if (this.debutDeLaSection) {
          init()
        } else {
          afficheEnonce()
        }
        break // case "enonce":

      case 'correction':
        afficheCorrection()
        break
    }
  } // main

  return main
}
