import { j3pChaine, j3pGetRandomInt, j3pMonome, j3pPolynome } from 'src/legacy/core/functions'
import { signeFois } from 'src/lib/utils/number'
import { getMainFunction, getParametres } from './mainVideoprojection'

/*
 *SECTION POUR VIDEOPROJECTION niveau TS : Détermination de f'(x) où f(est un polynome de degré 2)x)=(ax^+bx+c)\\mathrm{e}^(dx+e).
 * On peut paramétrer a et b, le temps limite et le type d’affichage de la correction
 * Développeur : Alexis Lecomte
 * septembre 2012
 */

export const params = {
  // pas d’autres params que ceux par défaut pour toutes les sections de vidéoproj (cf 701 pour en ajouter un)
  /** @type {LegacySectionParamsParametres} */
  parametres: getParametres({
    titre: 'Dérivée d’un produit',
    limite: 180,
    type_presentation: 'details',
    nettoiecorrection: true
  })
}

// même bornes pour tout le monde, a|b|c|d|e tous dans [-7, 7]
const aMin = -7
const aMax = 7

const maxTries = 1000

const txtQuestion = "Soit $g$ la fonction définie sur $\\R$ par $g(x)=£{gx}$.\nAprès avoir expliqué pourquoi $g$ est dérivable sur $\\R$, détermine l’expression de $g'(x)$."
const txtCorrection = "La fonction $g$ est définie sur $\\R$ par $g(x)=£{gx}$. Les polynômes et la fonction exponentielle étant dérivables sur $\\R$, $g$ est dérivable sur $\\R$.\nDe plus, $g$ étant un produit, on utilise la formule $\\left(uv\\right)' = u’v+v’u$ avec $u(x)=£{ux}$ et $v(x)=£{vx}$. On a alors $u'(x)=£{uPrim}$ et $v'(x)=£{vPrim}$. On obtient :\n$g'(x)=£{gPrim1}$\nEn factorisant : $g'(x)=£{gPrim2}$\n $g'(x)=£{gPrim3}$\n $g'(x)=£{gPrim4}$.\n"

function getEnoncesCorrections (ds) {
  const a1 = []
  const a2 = []
  const b1 = []
  const b2 = []
  const c1 = []
  const c2 = []
  const d1 = []
  const d2 = []
  const e1 = []
  const e2 = []
  let nbTries = 0
  for (let i = 0; i < ds.nbrepetitions; i++) {
    do {
      nbTries++
      // le premier calcul f(x)=ax^2+bx+c et on demande la dérivée en d
      a1[i] = j3pGetRandomInt(aMin, aMax)
      b1[i] = j3pGetRandomInt(aMin, aMax)
      c1[i] = j3pGetRandomInt(aMin, aMax)
      d1[i] = j3pGetRandomInt(aMin, aMax)
      e1[i] = j3pGetRandomInt(aMin, aMax)
      // le second calcul a et b, si l’on choisit d’afficher deux calculs côte à côte
      a2[i] = j3pGetRandomInt(aMin, aMax)
      b2[i] = j3pGetRandomInt(aMin, aMax)
      c2[i] = j3pGetRandomInt(aMin, aMax)
      d2[i] = j3pGetRandomInt(aMin, aMax)
      e2[i] = j3pGetRandomInt(aMin, aMax)
      // les données doivent être différentes et non nulles  (pas un non plus) et mes coefs finaux non nuls
    } while (nbTries < maxTries && (
      2 * a1[i] + d1[i] * b1[i] === 0 ||
      2 * a2[i] + d2[i] * b2[i] === 0 ||
      b1[i] + d1[i] * c1[i] === 0 ||
      b2[i] + d2[i] * c2[i] === 0 ||
      a1[i] === a2[i] || a1[i] === 0 ||
      a1[i] === 1 || a2[i] === 0 ||
      a2[i] === 1 || d1[i] === 0 ||
      d2[i] === 0 || d1[i] === d2[i] ||
      d1[i] === 1 || d2[i] === 1 ||
      b1[i] === 0 || b2[i] === 0 ||
      c1[i] === 0 || c2[i] === 0 ||
      e1[i] === 0 || e2[i] === 0
    ))
    if (nbTries >= maxTries) {
      throw Error(`Impossible de trouver suffisamment d’énoncés, le nombre de répétitions (${ds.nbrepetitions}) est probablement trop élèvé`)
    }
    for (let j = Math.max(0, i - 2); j < i; j++) {
      // je détecte si un calcul identique a déjà été trouvé dans un des deux précédents :
      if (((a1[j] === a1[i])) || ((a2[j] === a2[i])) || ((d1[j] === d1[i])) || ((d2[j] === d2[i]))) { i-- }
    }
  }

  // tous nos nombres aléatoires sont tirés, 2e boucle pour générer énoncés et corrections
  const enonces = []
  const corrections = []
  for (let i = 0; i < ds.nbrepetitions; i++) {
    // pour me faciliter la vie...
    let a = a1[i]
    let b = b1[i]
    let c = c1[i]
    let d = d1[i]
    let e = e1[i]
    let dxPluse = j3pPolynome([d, e])
    let ax2PluxbxPlusc = j3pPolynome([a, b, c])
    let uPrimex = j3pPolynome([2 * a, b])
    const params1 = {
      gx: '(' + j3pPolynome([a, b, c]) + ')\\mathrm{e}^{' + j3pPolynome([d, e]) + '}', // ex stockage1
      // u(x)
      ux: j3pPolynome([a, b, c]), // ex stockage3
      // v(x)
      vx: '\\mathrm{e}^{' + j3pPolynome([d, e]) + '}', // ex stockage5
      // Tout ce qui va servir en correction :
      // u'(x)
      uPrim: j3pPolynome([2 * a, b]),
      // v'(x)
      vPrim: d + '\\times \\mathrm{e}^{' + j3pPolynome([d, e]) + '}',
      gPrim1: '(' + uPrimex + ')\\mathrm{e}^{' + dxPluse + '} + ' + '(' + ax2PluxbxPlusc + ')\\times' + signeFois(d) + '\\times \\mathrm{e}^{' + dxPluse + '}',
      gPrim2: '\\mathrm{e}^{' + dxPluse + '}[(' + uPrimex + ') + ' + signeFois(d) + '(' + ax2PluxbxPlusc + ')]',
      gPrim3: '\\mathrm{e}^{' + dxPluse + '}(' + uPrimex + j3pMonome(2, 2, d * a) + j3pMonome(3, 1, d * b) + j3pMonome(2, 0, d * c) + ')',
      gPrim4: '\\mathrm{e}^{' + dxPluse + '}(' + j3pPolynome([d * a, 2 * a + d * b, b + d * c]) + ')'
    }

    // pour me faciliter la vie...
    a = a2[i]
    b = b2[i]
    c = c2[i]
    d = d2[i]
    e = e2[i]
    dxPluse = j3pPolynome([d, e])
    ax2PluxbxPlusc = j3pPolynome([a, b, c])
    uPrimex = j3pPolynome([2 * a, b])
    const params2 = {
      gx: '(' + ax2PluxbxPlusc + ')\\mathrm{e}^{' + dxPluse + '}',
      ux: ax2PluxbxPlusc,
      vx: '\\mathrm{e}^{' + dxPluse + '}',
      uPrim: uPrimex,
      vPrim: d + '\\times \\mathrm{e}^{' + dxPluse + '}',
      gPrim1: '(' + uPrimex + ')\\mathrm{e}^{' + dxPluse + '} + ' + '(' + ax2PluxbxPlusc + ')\\times' + signeFois(d) + '\\times \\mathrm{e}^{' + dxPluse + '}',
      gPrim2: '\\mathrm{e}^{' + dxPluse + '}[(' + uPrimex + ') + ' + signeFois(d) + '(' + ax2PluxbxPlusc + ')]',
      gPrim3: '\\mathrm{e}^{' + dxPluse + '}(' + uPrimex + j3pMonome(2, 2, d * a) + j3pMonome(3, 1, d * b) + j3pMonome(2, 0, d * c) + ')',
      gPrim4: '\\mathrm{e}^{' + dxPluse + '}(' + j3pPolynome([d * a, 2 * a + d * b, b + d * c]) + ')'
    }

    enonces[i] = [
      j3pChaine(txtQuestion, { gx: params1.gx }),
      j3pChaine(txtQuestion, { gx: params2.gx })
    ]
    corrections[i] = [
      j3pChaine(txtCorrection, params1),
      j3pChaine(txtCorrection, params2)
    ]
  }
  return { enonces, corrections }
}

const main = getMainFunction({ getEnoncesCorrections })

// section V705
export default main
