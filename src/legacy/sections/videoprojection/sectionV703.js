import { j3pChaine, j3pGetRandomInt, j3pMonome, j3pPolynome } from 'src/legacy/core/functions'
import { pgcd, signeFois } from 'src/lib/utils/number'
import { getMainFunction, getParametres } from './mainVideoprojection'

/*
 *SECTION POUR VIDEOPROJECTION niveau 1èreS : ensemble de dérivabilité et fonction dérivée de f(x)=(cx+d)/(ax+b)
 * On peut paramétrer le temps limite et le type d’affichage de la correction
 * Développeur : Alexis Lecomte
 * Septembre 2012
 */

const aMin = -7
const aMax = 7
const bMin = 2
const bMax = 10
const cMin = -7
const cMax = 7
const dMin = 2
const dMax = 10

const maxTries = 1000

const txtQuestion = 'Déterminer l’ensemble de définition, de dérivabilité de la fonction suivante et donner l’expression de sa fonction dérivée : $g(x) = £{gx}$'
const txtCorrection = 'La fonction $g$ définie par $g(x)=£{gx}$ est un quotient de deux fonctions dérivables sur $\\R$ et son dénominateur s’annule pour $x=£{valInterdite}$, elle est donc définie et dérivable sur $\\R-$' + '{$£{valInterdite}$}.\nÀ l’aide de la formule $\\left(\\frac{u}{v}\\right)^{’} = \\frac{u’v-uv’}{v^2}$, on obtient :\n$g’(x)=£{gPrimUn}=£{gPrimDeux}$\ndonc $g’(x)=£{gPrimTrois}$.'

export const params = {
  // pas d’autres params que ceux par défaut pour toutes les sections de vidéoproj (cf 701 pour en ajouter un)
  /** @type {LegacySectionParamsParametres} */
  parametres: getParametres({
    titre: 'Dérivée d’un quotient',
    type_presentation: 'details',
    nettoiecorrection: true
  })
}

function getEnoncesCorrections (ds) {
  const a1 = []
  const a2 = []
  const b1 = []
  const b2 = []
  const c1 = []
  const c2 = []
  const d1 = []
  const d2 = []
  let nbTries = 0
  for (let i = 0; i < ds.nbrepetitions; i++) {
    do {
      nbTries++
      // le premier calcul a et b
      a1[i] = j3pGetRandomInt(aMin, aMax)
      b1[i] = j3pGetRandomInt(bMin, bMax)
      c1[i] = j3pGetRandomInt(cMin, cMax)
      d1[i] = j3pGetRandomInt(dMin, dMax)
      // le second calcul a et b, si l’on choisit d’afficher deux calculs côte à côte
      a2[i] = j3pGetRandomInt(aMin, aMax)
      b2[i] = j3pGetRandomInt(bMin, bMax)
      c2[i] = j3pGetRandomInt(cMin, cMax)
      d2[i] = j3pGetRandomInt(dMin, dMax)
      // les fonctions doivent être différentes et non nulles et non constantes (pas un non plus)
    } while (nbTries < maxTries && (
      a1[i] * d1[i] === b1[i] * c1[i] ||
        a2[i] * d2[i] === b2[i] * c2[i] ||
        a1[i] === a2[i] ||
        a1[i] === 0 ||
        a1[i] === 1 ||
        a2[i] === 0 ||
        a2[i] === 1 ||
        c1[i] === 0 ||
        c2[i] === 0 ||
        c1[i] === 1 ||
        c2[i] === 1 ||
        b1[i] === 0 ||
        b2[i] === 0 ||
        d1[i] === 0 ||
        d2[i] === 0
    ))
    if (nbTries >= maxTries) {
      throw Error(`Impossible de trouver suffisamment d’énoncés, le nombre de répétitions (${ds.nbrepetitions}) est probablement trop élèvé`)
    }
    for (let j = Math.max(0, i - 2); j < i; j++) {
      // je détecte si un calcul identique a déjà été trouvé dans un des deux précédents :
      if (((a1[j] === a1[i])) || ((a2[j] === a2[i]))) {
        i--
      }
    }
  }

  // tous nos nombres aléatoires sont tirés, 2e boucle pour générer énoncés et corrections
  const enonces = []
  const corrections = []
  for (let i = 0; i < ds.nbrepetitions; i++) {
    // pour me faciliter la vie...
    let a = a1[i]
    let b = b1[i]
    let c = c1[i]
    let d = d1[i]

    const gx1 = '\\frac{' + j3pPolynome([c, d]) + '}{' + j3pPolynome([a, b]) + '}'
    let valInterdite1
    if (Math.round(-b / a) === (-b / a)) {
      valInterdite1 = -b / a
    } else {
      const m = pgcd(Math.abs(a), Math.abs(b))
      if (a * b < 0) {
        // -b/a est positif
        valInterdite1 = '\\frac{' + Math.abs(b / m) + '}{' + Math.abs(a / m) + '}'
      } else {
        valInterdite1 = '-\\frac{' + Math.abs(b / m) + '}{' + Math.abs(a / m) + '}'
      }
    }
    const gPrimUn1 = '\\frac{' + c + '(' + j3pPolynome([a, b]) + ')-(' + j3pPolynome([c, d]) + ')\\times ' + signeFois(a) + '}{(' + j3pPolynome([a, b]) + ')^2}'
    const gPrimDeux1 = '\\frac{' + j3pMonome(1, 1, a * c) + j3pMonome(2, 0, c * b) + j3pMonome(3, 1, -a * c) + j3pMonome(4, 0, -a * d) + '}{(' + j3pPolynome([a, b]) + ')^2}'
    const gPrimTrois1 = '\\frac{' + (c * b - a * d) + '}{(' + j3pPolynome([a, b]) + ')^2}'

    // pour me faciliter la vie...
    a = a2[i]
    b = b2[i]
    c = c2[i]
    d = d2[i]
    const gx2 = '\\frac{' + j3pPolynome([c, d]) + '}{' + j3pPolynome([a, b]) + '}'
    enonces[i] = [
      j3pChaine(txtQuestion, { gx: gx1 }),
      j3pChaine(txtQuestion, { gx: gx2 })
    ]

    let valInterdite2
    if (Math.round(-b / a) === (-b / a)) {
      valInterdite2 = -b / a
    } else {
      const m = pgcd(Math.abs(a), Math.abs(b))

      if (a * b < 0) {
        // -b/a est positif
        valInterdite2 = '\\frac{' + Math.abs(b / m) + '}{' + Math.abs(a / m) + '}'
      } else {
        valInterdite2 = '-\\frac{' + Math.abs(b / m) + '}{' + Math.abs(a / m) + '}'
      }
    }
    const gPrimUn2 = '\\frac{' + c + '(' + j3pPolynome([a, b]) + ')-(' + j3pPolynome([c, d]) + ')\\times ' + signeFois(a) + '}{(' + j3pPolynome([a, b]) + ')^2}'
    const gPrimDeux2 = '\\frac{' + j3pMonome(1, 1, a * c) + j3pMonome(2, 0, c * b) + j3pMonome(2, 1, -a * c) + j3pMonome(2, 0, -a * d) + '}{(' + j3pPolynome([a, b]) + ')^2}'
    const gPrimTrois2 = '\\frac{' + (c * b - a * d) + '}{(' + j3pPolynome([a, b]) + ')^2}'

    corrections[i] = [
      j3pChaine('\n' + txtCorrection, { gx: gx1, valInterdite: valInterdite1, gPrimUn: gPrimUn1, gPrimDeux: gPrimDeux1, gPrimTrois: gPrimTrois1 }),
      j3pChaine('\n' + txtCorrection, { gx: gx2, valInterdite: valInterdite2, gPrimUn: gPrimUn2, gPrimDeux: gPrimDeux2, gPrimTrois: gPrimTrois2 })
    ]
  }
  return { enonces, corrections }
}

const main = getMainFunction({ getEnoncesCorrections })

// section V703
export default main
