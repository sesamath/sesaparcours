import { j3pChaine, j3pGetRandomInt, j3pPGCD, j3pPolynome } from 'src/legacy/core/functions'
import { getMainFunction, getParametres } from './mainVideoprojection'

/*
 *SECTION POUR VIDEOPROJECTION niveau 1èreS : ensemble de dérivabilité et fonction dérivée de f(x)=1/(ax+b)
 * On peut paramétrer le temps limite et le type d’affichage de la correction
 * Développeur : Alexis Lecomte
 * Septembre 2012
 */

export const params = {
  // pas d’autres params que ceux par défaut pour toutes les sections de vidéoproj (cf 701 pour en ajouter un)
  /** @type {LegacySectionParamsParametres} */
  parametres: getParametres({
    titre: 'Dérivée d’un quotient (2)',
    type_presentation: 'details',
    nettoiecorrection: true
  })
}

const aMin = -7
const aMax = 7
const bMin = 2
const bMax = 10

const maxTries = 1000

const txtQuestion = 'Déterminer l’ensemble de définition, de dérivabilité de la fonction suivante et donner l’expression de sa fonction dérivée : $g(x) = £{gx}$.'
const txtCorrection = "La fonction $g$ définie par $g(x)=£{gx}$ est l’inverse d’une fonction dérivable sur $\\R$ qui s’annule pour $x=£{valInterdite}$, elle est donc définie et dérivable sur $\\R-$ {$£{valInterdite}$}.\nÀ l’aide de la formule $\\left(\\frac{1}{v}\\right)^'=\\frac{-v'}{v^2}$, on obtient :\n$g'(x)=£{gPrim}$."

function getEnoncesCorrections (ds) {
  const a1 = []
  const a2 = []
  const b1 = []
  const b2 = []
  // une boucle pour générer tous les nombres aléatoires (on recule si tirage trop proche du précédent)
  let nbTries = 0
  for (let i = 0; i < ds.nbrepetitions; i++) {
    do {
      nbTries++
      // le premier calcul a et b
      a1[i] = j3pGetRandomInt(aMin, aMax)
      b1[i] = j3pGetRandomInt(bMin, bMax)
      // le second calcul a et b, si l’on choisit d’afficher deux calculs côte à côte
      a2[i] = j3pGetRandomInt(aMin, aMax)
      b2[i] = j3pGetRandomInt(bMin, bMax)
      // les fonctions doivent être différentes et non nulles  (pas un non plus)
    } while (nbTries < maxTries && (
      a1[i] === a2[i] ||
      a1[i] === 0 ||
      a1[i] === 1 ||
      a2[i] === 0 ||
      a2[i] === 1 ||
      b1[i] === 0 ||
      b2[i] === 0
    ))
    if (nbTries >= maxTries) {
      throw Error(`Impossible de trouver suffisamment d’énoncés, le nombre de répétitions (${ds.nbrepetitions}) est probablement trop élèvé`)
    }
    for (let j = Math.max(0, i - 2); j < i; j++) {
      // je détecte si un calcul identique a déjà été trouvé dans un des deux précédents :
      if (((a1[j] === a1[i])) || ((a2[j] === a2[i]))) { i-- }
    }
  }

  // 2e boucle pour générer enoncés et corrections
  const enonces = []
  const corrections = []
  for (let i = 0; i < ds.nbrepetitions; i++) {
    // pour me faciliter la vie...
    let a = a1[i]
    let b = b1[i]
    const gx1 = '\\frac{1}{' + j3pPolynome([a, b]) + '}'
    let valInterdite1, valInterdite2
    if (Math.round(-b / a) === (-b / a)) {
      valInterdite1 = -b / a
    } else {
      const d = j3pPGCD(Math.abs(a), Math.abs(b))
      if (a * b < 0) {
        // -b/a est positif
        valInterdite1 = '\\frac{' + Math.abs(b / d) + '}{' + Math.abs(a / d) + '}'
      } else {
        valInterdite1 = '-\\frac{' + Math.abs(b / d) + '}{' + Math.abs(a / d) + '}'
      }
    }
    const gPrim1 = '\\frac{' + (-a) + '}{(' + j3pPolynome([a, b]) + ')^2}'

    // pour me faciliter la vie...
    a = a2[i]
    b = b2[i]
    const gx2 = '\\frac{1}{' + j3pPolynome([a, b]) + '}'

    // on stocke l’expression de la fonction maintenant, afin de la réutiliser dans la fonction afficheCalcul et en correction.
    enonces[i] = [
      j3pChaine(txtQuestion, { gx: gx1 }),
      j3pChaine(txtQuestion, { gx: gx2 })
    ]

    if (Math.round(-b / a) === (-b / a)) {
      valInterdite2 = -b / a
    } else {
      const d = j3pPGCD(Math.abs(a), Math.abs(b))

      if (a * b < 0) {
        // -b/a est positif
        valInterdite2 = '\\frac{' + Math.abs(b / d) + '}{' + Math.abs(a / d) + '}'
      } else {
        valInterdite2 = '-\\frac{' + Math.abs(b / d) + '}{' + Math.abs(a / d) + '}'
      }
    }
    const gPrim2 = '\\frac{' + (-a) + '}{(' + j3pPolynome([a, b]) + ')^2}'

    corrections[i] = [
      j3pChaine('\n' + txtCorrection, { gx: gx1, valInterdite: valInterdite1, gPrim: gPrim1 }),
      j3pChaine('\n' + txtCorrection, { gx: gx2, valInterdite: valInterdite2, gPrim: gPrim2 })
    ]
  }
  return { enonces, corrections }
}

const main = getMainFunction({ getEnoncesCorrections })

// section V704
export default main
