import { j3pGetRandomInt, j3pMonome, j3pPolynome } from 'src/legacy/core/functions'
import { getMainFunction, getParametres } from './mainVideoprojection'

/**
 * SECTION POUR VIDEOPROJECTION niveau TS : calcul d’intégrale de fonctions de la forme u'(x)\mathrm{e}^u(x) avec u(x)=ax+b.
 * On peut paramétrer a et b, le temps limite et le type d’affichage de la correction
 * @author Alexis Lecomte
 * @since Mai 2012
 * @module legacy/sections/videoprojection/sectionV700
 */

export const params = {
  // pas d’autres params que ceux par défaut pour toutes les sections de vidéoproj (cf 701 pour en ajouter un)
  /** @type {LegacySectionParamsParametres} */
  parametres: getParametres({
    titre: 'Intégrales et exponentielles',
    limite: 180,
    // pour savoir si l’on veut 1 ou 2 calculs en même temps
    nb_calcul: 2,
    // présentation possible pour la correction : details ou rapide ou aucune
    type_presentation: 'details',
    // pour savoir si l’on efface les corrections à chaque étape
    nettoiecorrection: true
  })
}

const txtQuestion = 'Calcule : '
const txtCorrection = "La fonction à intégrer est de la forme $u'\\times \\mathrm{e}^u$, elle a donc pour primitive $\\mathrm{e}^u$, on en déduit directement que "

const aMin = -7
const aMax = 7
const bMin = 2
const bMax = 10

const maxTries = 1000

function getEnoncesCorrections (ds) {
  // faut des tableaux car on regarde les tirages précédents (d’où les deux boucles)
  const a1 = []
  const a2 = []
  const b1 = []
  const b2 = []
  // détermination de tous ces coefs pour tous les items
  let nbTries = 0
  for (let i = 0; i < ds.nbrepetitions; i++) {
    do {
      nbTries++
      // le premier calcul a et b
      a1[i] = j3pGetRandomInt(aMin, aMax)
      b1[i] = j3pGetRandomInt(bMin, bMax)
      // le second calcul a et b, si l’on choisit d’afficher deux calculs côte à côte
      a2[i] = j3pGetRandomInt(aMin, aMax)
      b2[i] = j3pGetRandomInt(bMin, bMax)
      // les dérivées doivent être différentes et non nulles  (pas un non plus)
    } while (nbTries < maxTries && (a1[i] === a2[i] || a1[i] === 0 || a1[i] === 1 || a2[i] === 0 || a2[i] === 1))
    if (nbTries >= maxTries) {
      throw Error(`Impossible de trouver suffisamment d’énoncés, le nombre de répétitions (${ds.nbrepetitions}) est probablement trop élèvé`)
    }
    // si un calcul identique a déjà été trouvé dans un des deux précédents on recommence
    for (let j = Math.max(0, i - 2); j < i; j++) {
      if (a1[j] === a1[i] || a2[j] === a2[i]) i--
    }
  }

  // tous nos nombres aléatoires sont tirés, 2e boucle pour générer énoncés et corrections
  const enonces = []
  const corrections = []
  for (let i = 0; i < ds.nbrepetitions; i++) {
    // je génère mes bornes de l’intégrale dans [-4;4]
    const borneInf1 = j3pGetRandomInt(-4, 3)
    const borneSup1 = j3pGetRandomInt(Math.max(borneInf1 + 1, 1), 4)
    const borneInf2 = j3pGetRandomInt(-4, 3)
    const borneSup2 = j3pGetRandomInt(Math.max(borneInf2 + 1, 1), 4)
    const u1x = j3pPolynome([a1[i], b1[i]])

    const operation1 = '$\\int_{' + borneInf1 + '}^{' + borneSup1 + '}' + j3pMonome(1, 1, a1[i], '\\mathrm{e}^{' + u1x + '}') + ' \\mathrm dx$'
    const u2x = j3pPolynome([a2[i], b2[i]])

    const operation2 = '$\\int_{' + borneInf2 + '}^{' + borneSup2 + '}' + j3pMonome(1, 1, a2[i], '\\mathrm{e}^{' + u2x + '}') + ' \\mathrm dx$'
    // la solution du calcul de gauche, ce qu’il y a dans les crochets' - petits crochets cause PB Mathquill
    let correction1 = `${txtCorrection}\n${operation1} = ` + '$[ \\mathrm{e}^{' + u1x + '}]_{' + borneInf1 + '}^{' + borneSup1 + '}$'
    // la solution du calcul de droite
    let correction2 = `${txtCorrection}\n${operation2} = ` + '$[ \\mathrm{e}^{' + u2x + '}]_{' + borneInf2 + '}^{' + borneSup2 + '}$'
    const exp1 = a1[i] * borneSup1 + b1[i]
    const exp1bis = a1[i] * borneInf1 + b1[i]
    const exp2 = a2[i] * borneSup2 + b2[i]
    const exp2bis = a2[i] * borneInf2 + b2[i]
    // les solutions finales
    let chTmp1
    if (exp1 === 1) chTmp1 = '\\mathrm{e}'
    else if (exp1 === 0) chTmp1 = '1'
    else chTmp1 = '\\mathrm{e}^{' + exp1 + '}'
    let chTmp2
    if (exp1bis === 1) chTmp2 = '\\mathrm{e}'
    else if (exp1bis === 0) chTmp2 = '1'
    else chTmp2 = '\\mathrm{e}^{' + exp1bis + '}'

    correction1 += ' = $' + chTmp1 + '-' + chTmp2 + '$'

    if (exp2 === 1) chTmp1 = '\\mathrm{e}'
    else if (exp2 === 0) chTmp1 = '1'
    else chTmp1 = '\\mathrm{e}^{' + exp2 + '}'
    if (exp2bis === 1) chTmp2 = '\\mathrm{e}'
    else if (exp2bis === 0) chTmp2 = '1'
    else chTmp2 = '\\mathrm{e}^{' + exp2bis + '}'
    correction2 += ' = $' + chTmp1 + '-' + chTmp2 + '$'

    enonces[i] = [
      txtQuestion + operation1,
      txtQuestion + operation2
    ]
    corrections[i] = [correction1, correction2]
  }
  return { enonces, corrections }
} // getDatas

const main = getMainFunction({ getEnoncesCorrections })

// section V700
export default main
