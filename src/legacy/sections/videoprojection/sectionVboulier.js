import { j3pAddElt, j3pStyle } from 'src/legacy/core/functions'
import Boulier from 'src/legacy/outils/boulier/boulier'

/*
        DENIAUD Rémi
        novembre 2020
        section avec un boulier utilisée uniquement en vidéoprojection (pas de question)
 */
export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['type', 'japonais', 'liste', 'type de boulier', ['japonais', 'chinois']],
    ['nbtiges', 4, 'entier', 'nombre de tiges (1 à 12)'],
    ['couleur', 'rouge', 'liste', 'couleur des boules', ['rouge', 'bleu']]
  ]
}

const titreExo = 'Le boulier'
const consigne = 'Le nombre inscrit sur le boulier ci-contre est :'
const structure = 'presentation1bis'

/**
 * section Vboulier
 * @this {Parcours}
 */
export default function main () {
  const me = this
  me.setPassive()
  const ds = me.donneesSection
  if (me.etat === 'enonce') {
    if (typeof ds.nbtiges === 'string') ds.nbtiges = Number(ds.nbtiges)
    if (!Number.isInteger(ds.nbtiges) || ds.nbtiges < 1 || ds.nbtiges > 12) {
      console.error(Error(`Argument invalide : le nombre de tiges doit être entre 1 et 12 compris, or ici on en demande ${ds.nbtiges}.`))
      ds.nbtiges = 4
    }
    me.construitStructurePage({ structure, ratioGauche: 0.5 })
    me.afficheTitre(titreExo)
    const conteneur = j3pAddElt(me.zones.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    const eltBoulier = j3pAddElt(conteneur, 'div')
    const conteneurD = j3pAddElt(me.zones.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    j3pAddElt(conteneurD, 'div', consigne)
    const leNombre = j3pAddElt(conteneurD, 'div')
    j3pStyle(leNombre, { color: '#FF0000', fontWeight: 'bold', fontSize: '30px' })
    // eslint-disable-next-line no-new
    new Boulier(
      {
        isChinese: ds.type !== 'japonais',
        outputElement: leNombre,
        width: 494,
        epCadre: 15,
        nbTiges: ds.nbtiges,
        diametre: 30,
        epTige: 4
      },
      eltBoulier,
      {
        isBlue: ds.couleur === 'bleu',
        hasResetButton: true
      }
    )
    me.finEnonce()
  }
  // section passive, pas de case correction ni navigation
}
