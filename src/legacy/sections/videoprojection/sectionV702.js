import { j3pChaine, j3pGetRandomInt, j3pMonome, j3pPolynome } from 'src/legacy/core/functions'
import { signeFois } from 'src/lib/utils/number'
import { getMainFunction, getParametres } from './mainVideoprojection'

const aMin = -7
const aMax = 7

const maxTries = 1000

const txtQuestion = 'Soit $f$ la fonction définie sur $\\R$ par $f(x)=£{fx}$.\nDétermine une équation de la tangente à la courbe représentative de $f$ en $£a$'
const txtCorrection = "La fonction $f$ est définie sur $\\R$ par $f(x)=£{fx}$. On demande une équation de la tangente à sa courbe en $a=£a$.\nLa formule est $y=f'(a)(x-a)+f(a)$, or $f'(x)=£{fPrim}$ donc $f'(£a)=£{fPrima}$ et $f(£a)=£{fa}$.\nOn obtient donc comme équation $£{eqUn}$ soit $£{eqDeux}$ comme équation réduite."

/*
 *SECTION POUR VIDEOPROJECTION niveau 1S (ou révisions terminales) : Détermination de l’équation de la tangente de f en a où f est un polynome de degré 2.
 * On peut paramétrer a et b, le temps limite et le type d’affichage de la correction
 * Développeur : Alexis Lecomte
 * septembre 2012
 */

export const params = {
  // pas d’autres params que ceux par défaut pour toutes les sections de vidéoproj (cf 701 pour en ajouter un)
  /** @type {LegacySectionParamsParametres} */
  parametres: getParametres({
    titre: 'Équation de la tangente à une parabole',
    limite: 120,
    type_presentation: 'details',
    nettoiecorrection: true
  })
}

function getEnoncesCorrections (ds) {
  const a1 = []
  const a2 = []
  const b1 = []
  const b2 = []
  const c1 = []
  const c2 = []
  const d1 = []
  const d2 = []
  let nbTries = 0
  for (let i = 0; i < ds.nbrepetitions; i++) {
    do {
      nbTries++
      // le premier calcul f(x)=ax^2+bx+c et on demande la dérivée en d
      a1[i] = j3pGetRandomInt(aMin, aMax)
      b1[i] = j3pGetRandomInt(aMin, aMax)
      c1[i] = j3pGetRandomInt(aMin, aMax)
      d1[i] = j3pGetRandomInt(aMin, aMax)
      // le second calcul a et b, si l’on choisit d’afficher deux calculs côte à côte
      a2[i] = j3pGetRandomInt(aMin, aMax)
      b2[i] = j3pGetRandomInt(aMin, aMax)
      c2[i] = j3pGetRandomInt(aMin, aMax)
      d2[i] = j3pGetRandomInt(aMin, aMax)
      // les données doivent être différentes et non nulles  (pas un non plus)
    } while (nbTries < maxTries && (
      a1[i] === a2[i] ||
      a1[i] === 0 ||
      a1[i] === 1 ||
      a2[i] === 0 ||
      a2[i] === 1 ||
      d1[i] === 0 ||
      d2[i] === 0 ||
      d1[i] === d2[i] ||
      b1[i] === 0 ||
      b2[i] === 0 ||
      c1[i] === 0 ||
      c2[i] === 0
    ))
    if (nbTries >= maxTries) {
      throw Error(`Impossible de trouver suffisamment d’énoncés, le nombre de répétitions (${ds.nbrepetitions}) est probablement trop élèvé`)
    }
    for (let j = Math.max(0, i - 2); j < i; j++) {
      // je détecte si un calcul identique a déjà été trouvé dans un des deux précédents :
      if (
        a1[j] === a1[i] ||
        a2[j] === a2[i] ||
        d1[j] === d1[i] ||
        d2[j] === d2[i]
      ) {
        i--
        // on va donc écraser ci-dessous les calculs déjà faits pour le rang précédent…
      }
    }
  }

  // tous nos nombres aléatoires sont tirés, 2e boucle pour générer énoncés et corrections
  const enonces = []
  const corrections = []
  for (let i = 0; i < ds.nbrepetitions; i++) {
    // pour me faciliter la vie...
    let a = a1[i]
    let b = b1[i]
    let c = c1[i]
    let d = d1[i]

    const fx1 = j3pPolynome([a, b, c])
    const entier1 = d

    // Tout ce qui va servir en correction :
    const fPrim1 = j3pPolynome([2 * a, b])
    const fPrima1 = 2 * a + '\\times' + signeFois(d) + j3pMonome(2, 0, b) + ' = ' + (2 * a * d + b)
    const paramE1 = a + '\\times' + signeFois(d) + '^2' + j3pMonome(2, 0, b) + '\\times' + signeFois(d) + '' + j3pMonome(2, 0, c) + '=' + (a * d * d) + j3pMonome(2, 0, b * d) + j3pMonome(2, 0, c) + '=' + (a * d * d + b * d + c)
    const eqUn1 = 'y=' + j3pMonome(1, 1, 2 * a * d + b, '(x' + j3pMonome(2, 0, -d) + ')') + j3pMonome(2, 0, a * d * d + b * d + c) // ecrireCoefx(2 * a * d + b) + '(x' + signe(-d) + ')' + signe(a * d * d + b * d + c)
    const eqDeux1 = (2 * a * d + b) === 0
      ? 'y=' + ((2 * a * d + b) * (-d) + a * d * d + b * d + c)
      : 'y=' + j3pPolynome([2 * a * d + b, (2 * a * d + b) * (-d) + a * d * d + b * d + c])

    // pour me faciliter la vie...
    a = a2[i]
    b = b2[i]
    c = c2[i]
    d = d2[i]
    const fx2 = j3pPolynome([a, b, c])
    const entier2 = d
    enonces[i] = [
      j3pChaine(txtQuestion, { fx: fx1, a: entier1 }),
      j3pChaine(txtQuestion, { fx: fx2, a: entier1 })
    ]

    // Tout ce qui va servir en correction :
    const fPrim2 = j3pPolynome([2 * a, b])
    const fPrima2 = 2 * a + '\\times' + signeFois(d) + j3pMonome(2, 0, b) + ' = ' + (2 * a * d + b)
    const paramE2 = a + '\\times' + signeFois(d) + '^2' + j3pMonome(2, 0, b) + '\\times' + signeFois(d) + '' + j3pMonome(2, 0, c) + '=' + (a * d * d) + j3pMonome(2, 0, b * d) + j3pMonome(2, 0, c) + '=' + (a * d * d + b * d + c)
    const eqUn2 = 'y=' + j3pMonome(1, 1, 2 * a * d + b, '(x' + j3pMonome(2, 0, -d) + ')') + j3pMonome(2, 0, a * d * d + b * d + c)
    const eqDeux2 = (2 * a * d + b) === 0
      ? 'y=' + ((2 * a * d + b) * (-d) + a * d * d + b * d + c)
      : 'y=' + j3pPolynome([2 * a * d + b, (2 * a * d + b) * (-d) + a * d * d + b * d + c])
    corrections[i] = [
      j3pChaine('\n' + txtCorrection, { fx: fx1, a: entier1, fPrim: fPrim1, fPrima: fPrima1, fa: paramE1, eqUn: eqUn1, eqDeux: eqDeux1 }),
      j3pChaine('\n' + txtCorrection, { fx: fx2, a: entier2, fPrim: fPrim2, fPrima: fPrima2, fa: paramE2, eqUn: eqUn2, eqDeux: eqDeux2 })
    ]
  } // for

  return { enonces, corrections }
}

const main = getMainFunction({ getEnoncesCorrections })

// section V702
export default main
