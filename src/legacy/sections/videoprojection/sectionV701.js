import { j3pChaine, j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pGetLatexMonome } from 'src/legacy/core/functionsLatex'
import { getMainFunction, getParametres } from './mainVideoprojection'

/*
 *SECTION POUR VIDEOPROJECTION niveau 1S (ou révisions terminales) : Détermination du nombre dérivée de f en a où f est un polynome de degré 2.
 * On peut paramétrer a et b, le temps limite et le type d’affichage de la correction
 * on peut aussi paramétrer la forme de la correction, soit en utilisant la fonction dérivée (réglage de base) soit en utilisant uniquement un taux d’accroissement
 * en surchargeant la variable correction_type à "sansderivée".
 * Développeur : Alexis Lecomte
 * septembre 2012 et juin 2013
 * reprise par Rémi DENIAUD en juin 2021
 */

// tous les intervalles sont les mêmes
const aMin = -7
const aMax = 7

const maxTries = 1000

const textes = {
  themeExo1: 'Pour chaque question de cet exercice, on demande de déterminer un nombre dérivé en utilisant un taux d’accroissement d’une fonction.',
  themeExo2: 'Pour chaque question de cet exercice, on demande de déterminer un nombre dérivé en calculant auparavant la dérivée de la fonction proposée.',
  question: 'Soit $g$ la fonction définie sur $\\R$ par $g(x)=£{gx}$.\nDétermine le nombre dérivé de $g$ en $£a$',
  correctionAvecDerivee: "La fonction $g$ définie sur $\\R$ par $g(x)=£{gx}$ est dérivable sur $\\R$ avec $g'(x) = £{gPrim}$ donc $g'(£a)=£{gPrimaUn}=£{gPrimaDeux}$",
  correctionSansDerivee: "Avec $g(x)=£{gx}$ pour tout réel $x$, pour tout $h$ non nul, on a : $\\frac{g(£a+h)-g(£a)}{h} = £{tauxAccrUn} = £{tauxAccrDeux} = £{tauxAccrTrois} = £{tauxAccrQuatre}$\nLorsque $h$ tend vers 0, cette quantité tend vers $£{gPrima}$ donc $g$ est dérivable en $£a$ et $g'(£a)=£{gPrima}$"
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ...getParametres({
      titre: 'Nombre dérivé d’un polynôme',
      limite: 0
    }),
    // et on ajoute ce param
    /** @type {LegacySectionParamsParametre} */
    ['correction_type', 'sansderivée', 'liste', 'avecderivée ou sansderivée si l’on veut (ou pas) utiliser la fonction dérivée en correction', ['avecderivée', 'sansderivée']]
  ]
}

function signeFois (nombre) {
  return (nombre < 0) ? '(' + nombre + ')' : String(nombre)
}
function getEnoncesCorrections (ds) {
  const a1 = []
  const a2 = []
  const b1 = []
  const b2 = []
  const c1 = []
  const c2 = []
  const d1 = []
  const d2 = []
  let nbTries = 0
  for (let i = 0; i < ds.nbrepetitions; i++) {
    do {
      nbTries++
      // le premier calcul f(x)=ax^2+bx+c et on demande la dérivée en d
      a1[i] = j3pGetRandomInt(aMin, aMax)
      b1[i] = j3pGetRandomInt(aMin, aMax)
      c1[i] = j3pGetRandomInt(aMin, aMax)
      d1[i] = j3pGetRandomInt(aMin, aMax)
      // le second calcul a et b, si l’on choisit d’afficher deux calculs côte à côte
      a2[i] = j3pGetRandomInt(aMin, aMax)
      b2[i] = j3pGetRandomInt(aMin, aMax)
      c2[i] = j3pGetRandomInt(aMin, aMax)
      d2[i] = j3pGetRandomInt(aMin, aMax)
      // les données doivent être différentes et non nulles (pas un non plus)
    } while (nbTries < maxTries && (
      a1[i] === a2[i] ||
      a1[i] === 0 ||
      a1[i] === 1 ||
      a2[i] === 0 ||
      a2[i] === 1 ||
      d1[i] === d2[i] ||
      b1[i] === 0 ||
      b2[i] === 0 ||
      c1[i] === 0 ||
      c2[i] === 0 ||
      d1[i] === 0 ||
      d2[i] === 0
    ))
    if (nbTries >= maxTries) {
      throw Error(`Impossible de trouver suffisamment d’énoncés, le nombre de répétitions (${ds.nbrepetitions}) est probablement trop élèvé`)
    }
    for (let j = Math.max(0, i - 2); j < i; j++) {
      // je détecte si un calcul identique a déjà été trouvé dans un des deux précédents :
      if (
        a1[j] === a1[i] ||
        a2[j] === a2[i] ||
        d1[j] === d1[i] ||
        d2[j] === d2[i]
      ) {
        i--
      }
    }
  }

  // tous nos nombres aléatoires sont tirés, 2e boucle pour générer énoncés et corrections
  const enonces = []
  const corrections = []
  for (let i = 0; i < ds.nbrepetitions; i++) {
    const gx1 = j3pGetLatexMonome(1, 2, a1[i], 'x') + j3pGetLatexMonome(2, 1, b1[i], 'x') + j3pGetLatexMonome(3, 0, c1[i], 'x')
    const entier1 = d1[i]
    const gx2 = j3pGetLatexMonome(1, 2, a2[i], 'x') + j3pGetLatexMonome(2, 1, b2[i], 'x') + j3pGetLatexMonome(3, 0, c2[i], 'x')
    const entier2 = d2[i]
    enonces[i] = [
      j3pChaine(textes.question, { gx: gx1, a: entier1 }),
      j3pChaine(textes.question, { gx: gx2, a: entier2 })
    ]

    // la solution :
    const gPrimaDeux1 = 2 * a1[i] * d1[i] + b1[i]
    const gPrimaDeux2 = 2 * a2[i] * d2[i] + b2[i]

    // en cas de correction sans dérivée (juste taux d’accroissement, je stocke d’autres éléments :
    if (ds.correction_type === 'sansderivée') {
      const tauxAccrUn1 = '\\frac{' + j3pGetLatexMonome(1, 2, a1[i], '(' + d1[i] + '+h)') + j3pGetLatexMonome(2, 1, b1[i], '(' + d1[i] + '+h)') + j3pGetLatexMonome(3, 0, c1[i]) + '-(' + signeFois(a1[i]) + '\\times' + signeFois(d1[i]) + '^2' + j3pGetLatexMonome(2, 0, b1[i]) + '\\times' + signeFois(d1[i]) + j3pGetLatexMonome(2, 0, c1[i]) + ')}{h}'
      const tauxAccrDeux1 = '\\frac{' + j3pGetLatexMonome(1, 1, a1[i], '(' + signeFois(d1[i]) + '^2+2\\times' + signeFois(d1[i]) + '\\times h + h^2)') + j3pGetLatexMonome(2, 0, b1[i] * d1[i], 'h') + j3pGetLatexMonome(3, 1, b1[i], 'h') + j3pGetLatexMonome(4, 0, c1[i]) + j3pGetLatexMonome(5, 0, -a1[i]) + '\\times' + signeFois(d1[i]) + '^2' + j3pGetLatexMonome(7, 0, -b1[i] * d1[i]) + j3pGetLatexMonome(8, 0, -c1[i]) + '}{h}'
      const tauxAccrTrois1 = '\\frac{' + j3pGetLatexMonome(1, 1, 2 * a1[i] * d1[i], 'h') + j3pGetLatexMonome(2, 2, a1[i], 'h') + j3pGetLatexMonome(3, 1, b1[i], 'h') + '}{h}'
      const tauxAccrQuatre1 = j3pGetLatexMonome(1, 0, 2 * a1[i] * d1[i] + b1[i]) + j3pGetLatexMonome(2, 1, a1[i], 'h')
      const tauxAccrUn2 = '\\frac{' + j3pGetLatexMonome(1, 2, a2[i], '(' + d2[i] + '+h)') + j3pGetLatexMonome(2, 1, b2[i], '(' + d2[i] + '+h)') + j3pGetLatexMonome(3, 0, c2[i]) + '-(' + signeFois(a2[i]) + '\\times' + signeFois(d2[i]) + '^2' + j3pGetLatexMonome(2, 0, b2[i]) + '\\times' + signeFois(d2[i]) + j3pGetLatexMonome(2, 0, c2[i]) + ')}{h}'
      const tauxAccrDeux2 = '\\frac{' + j3pGetLatexMonome(1, 2, a2[i], '(' + signeFois(d2[i]) + '^2+2\\times' + signeFois(d2[i]) + '\\times h + h^2)') + j3pGetLatexMonome(2, 0, b2[i] * d2[i], 'h') + j3pGetLatexMonome(3, 1, b2[i], 'h') + j3pGetLatexMonome(4, 0, c2[i]) + j3pGetLatexMonome(5, 0, -a2[i]) + '\\times' + signeFois(d2[i]) + '^2' + j3pGetLatexMonome(7, 0, -b2[i] * d2[i]) + j3pGetLatexMonome(8, 0, -c2[i]) + '}{h}'
      const tauxAccrTrois2 = '\\frac{' + j3pGetLatexMonome(1, 1, 2 * a2[i] * d2[i], 'h') + j3pGetLatexMonome(2, 2, a2[i], 'h') + j3pGetLatexMonome(3, 1, b2[i], 'h') + '}{h}'
      const tauxAccrQuatre2 = j3pGetLatexMonome(1, 0, 2 * a2[i] * d2[i] + b2[i]) + j3pGetLatexMonome(2, 1, a2[i], 'h')

      // on a tous les morceaux de la correction
      corrections[i] = [
        j3pChaine('\n' + textes.correctionSansDerivee, { tauxAccrUn: tauxAccrUn1, a: entier1, tauxAccrDeux: tauxAccrDeux1, tauxAccrTrois: tauxAccrTrois1, gx: gx1, tauxAccrQuatre: tauxAccrQuatre1, gPrima: gPrimaDeux1 }),
        j3pChaine('\n' + textes.correctionSansDerivee, { tauxAccrUn: tauxAccrUn2, a: entier2, tauxAccrDeux: tauxAccrDeux2, tauxAccrTrois: tauxAccrTrois2, gx: gx2, tauxAccrQuatre: tauxAccrQuatre2, gPrima: gPrimaDeux2 })
      ]
    } else {
      // avec dérivée
      const gPrim1 = j3pGetLatexMonome(1, 1, 2 * a1[i], 'x') + j3pGetLatexMonome(2, 0, b1[i], 'x')
      const gPrim2 = j3pGetLatexMonome(1, 1, 2 * a2[i], 'x') + j3pGetLatexMonome(2, 0, b2[i], 'x')
      const gPrimaUn1 = String(2 * a1[i]) + '\\times' + signeFois(d1[i]) + j3pGetLatexMonome(2, 0, b1[i])
      const gPrimaUn2 = String(2 * a2[i]) + '\\times' + signeFois(d2[i]) + j3pGetLatexMonome(2, 0, b2[i])
      corrections[i] = [
        j3pChaine('\n' + textes.correctionAvecDerivee, { gPrim: gPrim1, a: entier1, gPrimaUn: gPrimaUn1, gx: gx1, gPrimaDeux: gPrimaDeux1 }),
        j3pChaine('\n' + textes.correctionAvecDerivee, { gPrim: gPrim2, a: entier2, gPrimaUn: gPrimaUn2, gx: gx2, gPrimaDeux: gPrimaDeux2 })
      ]
    }
  } // for
  return { enonces, corrections }
}

const main = getMainFunction({ getEnoncesCorrections })

// section V701
export default main
