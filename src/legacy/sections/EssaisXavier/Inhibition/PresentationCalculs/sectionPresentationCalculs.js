import { correction, enonceReset, navigation } from 'src/legacy/outils/rexams/main'

/// ////////////////////////////////////////////////////////////////////////////////////////
//
//               Section Présentation des calculs
//
/// ////////////////////////////////////////////////////////////////////////////////////////

/*
        Auteur  Xavier JEROME
        Date    09/08/2020
        Basée sur l’intégration de R-exams dans J3P par Patrick Raffinat
        Adaptation de http://www.r-exams.org/templates/dist3/

        exercices d’inhibition des erreurs fréquentes

 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['titre', 'Présentation des calculs', 'string', 'Le titre de l’exercice'],
    ['indication', '', 'string', 'Indication en bas à gauche.'], // Texte que l’élève peut faire apparaître en bas de l’écran
    ['nbrepetitions', 10, 'entier', 'Nombre de répétitions de la section']
  ]
}

// on prépare ici la liste des fichiers (dans sectionsAnnexes/qcmRexams/) que l’outil rexams ira piocher (avec j3pImporteAnnexe)
const fichiersHtmlAnnexes = []
for (let i = 1; i < 36; i++) {
  fichiersHtmlAnnexes.push(`mathjax${i}.html`)
}

/**
 * section PresentationCalculs
 * @this {Parcours}
 */
export default function main () {
  const ds = this.donneesSection
  const stor = this.storage
  const me = this

  function init () {
    // On ajoute les fichiers possibles (dans sectionsAnnexes) que l’outil rexams ira piocher
    stor.fichiersHtmlAnnexes = fichiersHtmlAnnexes
    // on impose ce type de qcm
    stor.typeExo = 'singleChoice'
    me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
    me.afficheTitre(ds.titre)
    // Affiche l’indication en bas à gauche
    if (ds.indication !== '') me.indication(this.zones.IG, ds.indication)
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        init()
      }
      enonceReset(this)
      break

    case 'correction' :
      correction(this)
      break

    case 'navigation' :
      navigation(this)
      break
  }
}
