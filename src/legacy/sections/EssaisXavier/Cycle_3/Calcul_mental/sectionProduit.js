import { j3pBarre, j3pDiv, j3pElement, j3pFocus, j3pGetRandomInt, j3pRestriction, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Auteur  Xavier JEROME
        Date    06/08/2018
        Ecrire un nombre entier sous la forme d’un produit
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', '', 'string', 'Indication en bas à gauche'], // Texte que l’élève peut faire apparaître en bas de l’écran
    ['nbrepetitionsmax', 10, 'entier', 'Nombre de répétitions maximum de la section.<BR>Il peut être inférieur si on répond juste à toutes les questions'], // Obligatoire: nombre de questions posées pendant l’exercice
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'], // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'], // S’il n’est pas vide, le temps de réponse sera limité
    ['s_difficulte', '1;4', 'string', "Une suite de nombres indiquant les cas autorisés parmi<br>1 pour un produit de deux nombres premiers dans une table<br>2 pour un produit quelconque dans une table<br>3 pour un nombre pair jusqu'à 100<br>4 pour un multiple de 10 ou de 100<br>5 pour un multiple de 5 jusqu'à 200"],
    ['n_aide', 1, 'entier', '0 pour aucune aide<BR>1 pour une explication des erreurs'] // Niveau d’aide autorisé
  ],
  pe: [
    { pe_1: 'Notion totalement maitrisée.' },
    { pe_2: 'Notion comprise.' },
    { pe_3: 'Notion fragile.' },
    { pe_4: 'Notion non comprise.' }
  ]
}

const textes = {
  phraseTitre: 'Ecrire un entier comme un produit.', //  Titre de la section
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  phraseEnonce: 'Ecris £r comme un produit de deux nombres entiers différents de 1.<br>&1&$\\times$&2& = £r',
  phraseSolution1: 'Tu avais le choix entre ',
  phraseSolution2: ' = £a',
  Aide01: 'Attention:  £a$\\times$£b est égal à £r ',
  Aide02: 'C’est juste, mais tu n’as pas le droit d’utiliser le nombre 1.'
}
const idPrefix = 'produit'

/**
 * section Produit
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage

  let n, i, j

  function _Donnees () {
    /*      "presentation1"   6 cadres complets
        "presentation1bis"   manque le cadre indication en bas à droite
        "presentation2"   manque le cadre réponse à droite
        "presentation3"   un seul grand cadre en plus des deux entêtes
*/
    this.structure = 'presentation1'

    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    // Cette indication peut être changée par l’utilisateur dans la page paramètres
    this.indication = 'Commence par chercher si ce nombre est dans une table.' //  N’est utilisé que si le paramètre passé en entrée est vide
    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    this.video = [] // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

    // Nombre de répétitions de l’exercice
    this.nbrepetitionsmax = 10 // N’est utilisé que si le paramètre passé en entrée est vide
    this.nbetapes = 1 // Si chaque question est divisée en plusieurs étapes
    // Nombre de chances dont dispose l’élève pour répondre à la question
    this.nbchances = 2
    // Par défaut, pas de temps limite.
    this.limite = 0 //  N’est utilisé que si le paramètre passé en entrée est vide

    this.s_difficulte = '1;4'

    this.n_aide = 1
    /*
        Phrase d’état renvoyée par la section
        */
    this.pe_1 = 'Notion totalement maitrisée'
    this.pe_2 = 'Notion comprise'
    this.pe_3 = 'Notion fragile.'
    this.pe_4 = 'Notion non comprise.'

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //               Ajouter ici toutes les constantes nécessaires à la section
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////
  }

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Ajouter ici toutes les fonctions nécessaires à la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Transforme une chaine d’intervalles en un tableau de valeurs
  function intervalles_to_tab (intervalles) {
    const valeurs = []
    const tableau = intervalles.match(/\d*/g)
    for (i = 0; i < tableau.length; i++) {
      if (tableau[i].length != 0) { valeurs.push(parseFloat(tableau[i])) }
    }
    return valeurs
  } // intervalles_to_tab

  //   Affiche la correction sous l’énoncé
  function affiche_correction (fin) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse

    j3pElement(idPrefix + 'explication1').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
    if (!reponse.bonneReponse) {
      if (fin) { //  On affiche la bonne réponse s’il n’a pas trouvé au dernier essai
        if (stor.nb_reponse == 1) {
          j3pAffiche(idPrefix + 'explication1', idPrefix + 'corr11', stor.reponse + textes.phraseSolution2,
            {
              style: me.styles.grand.correction,
              a: stor.nombre
            })
        } else {
          j3pAffiche(idPrefix + 'explication1', idPrefix + 'corr11', textes.phraseSolution1 + stor.reponse,
            { style: me.styles.grand.correction })
        }
      } // fin

      if ((me.donneesSection.n_aide >= 1) && (me.facteur1 != 0) && (me.facteur2 != 0) && reponse.aRepondu) {
        j3pElement(idPrefix + 'explication2').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
        if (me.facteur1 * me.facteur2 != stor.nombre) {
          j3pAffiche(idPrefix + 'explication2', idPrefix + 'corr21', textes.Aide01,
            {
              style: me.styles.grand.correction,
              a: me.facteur1,
              b: me.facteur2,
              r: me.facteur1 * me.facteur2
            })
        }

        j3pElement(idPrefix + 'explication3').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
        if ((me.facteur1 * me.facteur2 == stor.nombre) && ((me.facteur1 == 1) || (me.facteur2 == 1))) {
          j3pAffiche(idPrefix + 'explication3', idPrefix + 'corr31', textes.Aide02,
            { style: me.styles.grand.correction })
        }
      } // Aide
    } // Bonne réponse
  } //  affiche_correction

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  function affiche_chances () {
    j3pElement(idPrefix + 'zone_chances').innerHTML = ''
    if (me.donneesSection.nbchances > 1) {
      j3pAffiche(idPrefix + 'zone_chances', idPrefix + 'chances', textes.phraseNbChances,
        {
          e: me.donneesSection.nbchances - me.essaiCourant,
          p: (me.donneesSection.nbchances - me.essaiCourant > 1 ? 's' : '')
        })
    } // Pour le pluriel
  }

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Déroulement de la section Enonce, Correction, navigation
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Permet de désigner la section
  const la_section = me.SectionProduit // Modification du nom n°4

  switch (this.etat) {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'enonce': {
      //  Construit l’énoncé: exécuté au début de chaque question
      if (this.debutDeLaSection) {
      //  Code exécuté uniquement au lancement de la section
        //  Partie à ne pas changer
        // Construit les données de la section
        this.donneesSection = new _Donnees()

        this.surcharge()

        // Construction de la page
        this.construitStructurePage(this.donneesSection.structure)

        this.donneesSection.nbrepetitions = this.donneesSection.nbrepetitionsmax // C’est cette valeur qui est affichée dans l’entête
        this.donneesSection.nbitems = this.donneesSection.nbetapes * this.donneesSection.nbrepetitionsmax // Initialisation à ne pas modifier

        this.score = 0

        this.afficheTitre(textes.phraseTitre)

        // Affiche l’indication en bas à gauche
        if (this.donneesSection.indication) this.indication(this.zones.IG, this.donneesSection.indication)
        //  Paramètres à renseigner
        /*
                 Par convention,`
                `   this.typederreurs[0] = nombre de bonnes réponses
                    this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    this.typederreurs[2] = nombre de mauvaises réponses
                    this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        // Permet l’utilisation du compte à rebours si un temps est défini dans les paramètre

        //  Paramètres et variables particuliers à la section
        // Transforme l’intervalle pour qu’il corresponde au codage du nombre
        stor.t_difficulte = intervalles_to_tab(me.donneesSection.s_difficulte).sort()

        //  Tableaux de tableaux donnant tous les produits possibles par difficulté
        stor.produits = []
        stor.produits[1] = [] // 1 Tous les produits de tables avec 2 nombres premiers
        const t_premiers = [2, 3, 5, 7]
        for (i = 0; i <= 3; i++) {
          for (j = i; j <= 3; j++) { stor.produits[1].push(t_premiers[i] * t_premiers[j]) }
        }

        stor.produits[2] = [] // 2 Tous les produits de tables sans doublons
        for (i = 2; i <= 9; i++) {
          for (j = i; j <= 9; j++) {
            if (!stor.produits[2].includes(i * j)) { stor.produits[2].push(i * j) }
          }
        }

        stor.produits[3] = [] // 3 Tous les doubles de 10 jusqu'à 100
        for (i = 5; i <= 50; i++) { stor.produits[3].push(i * 2) }

        stor.produits[4] = [] // 4 Tous les multiples de 10 et 100
        for (i = 2; i <= 10; i++) {
          stor.produits[4].push(i * 10)
          stor.produits[4].push(i * 100)
        }

        stor.produits[5] = [] // 5 Tous les nombres se terminant par 5 jusqu'à 200
        for (i = 3; i <= 39; i += 2) { stor.produits[5].push(i * 5) }

        //  Tableaux permettant de ne pas poser 2 fois la même question
        stor.questions = [] // Contient les question qui n’ont pas encore été posées
        stor.difficulte_en_cours = 0
        stor.questions = stor.produits[stor.t_difficulte[0]].slice()

        stor.erreurs = [] // Contient questions qui ont provoqué une erreur pour les représenter en priorité
      } else {
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
      }
      //  Partie exécutée à chaque essai
      /// ////////////////////////////////////////////////////////////////////////////////////////
      //
      //               Création de l’énoncé
      //
      /// ////////////////////////////////////////////////////////////////////////////////////////

      //  Création de l’énoncé
      n = j3pGetRandomInt(0, stor.questions.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
      stor.nombre = stor.questions[n] // Une question est un triplet rang arrondi difficulté
      stor.questions.splice(n, 1) // On la retire des questions à poser

      stor.reponse = '' // Contient toutes les réponses possibles
      stor.nb_reponse = 0 // pour adapter la phrase réponse
      for (i = 2; i <= Math.sqrt(stor.nombre); i++) {
        if (stor.nombre % i == 0) {
          stor.nb_reponse++
          if (stor.nb_reponse > 1) stor.reponse += '; '
          stor.reponse += i + '$\\times$' + Math.round(stor.nombre / i)
        }
      }

      //  Création des conteneurs
      // Création du conteneur dans lequel se trouveront toutes les consignes
      j3pDiv(this.zones.MG, { id: idPrefix + 'consignes', contenu: '', coord: [0, 0], style: me.styles.etendre('petit.enonce', { padding: '10px', marginTop: '25px' }) })

      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      j3pDiv(idPrefix + 'consignes', idPrefix + 'zone_consigne1', '')

      // Affiche le nombre de chances qu’il reste
      j3pDiv(idPrefix + 'consignes', { id: idPrefix + 'zone_chances', contenu: '', style: me.styles.etendre('moyen.enonce', { marginTop: '30px' }) })
      affiche_chances()

      // Si on veut rajouter des explications, en cas de réponse fausse par exemple
      j3pDiv(this.zones.MG, { id: idPrefix + 'explications', contenu: '', coord: [10, 200], style: me.styles.petit.explications })
      j3pDiv(idPrefix + 'explications', idPrefix + 'explication1', '')
      j3pDiv(idPrefix + 'explications', idPrefix + 'explication2', '')
      j3pDiv(idPrefix + 'explications', idPrefix + 'explication3', '')
      j3pElement(idPrefix + 'explication2').style.marginTop = '20px'
      j3pElement(idPrefix + 'explication3').style.marginTop = '20px'

      // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
      j3pDiv(this.zones.MD, { id: idPrefix + 'correction', contenu: '', coord: [0, 0], style: me.styles.etendre('petit.correction', { padding: '10px' }) })
      //  Création des conteneurs

      //  Initialisation des zones de saisie
      j3pAffiche(idPrefix + 'zone_consigne1', idPrefix + 'consigne1', textes.phraseEnonce,
        {
          style: me.styles.moyen.enonce,
          r: stor.nombre,
          inputmq1: { width: '12px', correction: '' },
          inputmq2: { width: '12px', correction: '' }
        })

      // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
      // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche

      // On rajoute les contraintes de saisie
      j3pRestriction(idPrefix + 'consigne1inputmq1', '0-9') // on n’autorise que les chiffres
      j3pRestriction(idPrefix + 'consigne1inputmq2', '0-9') // on n’autorise que les chiffres

      //  Permet de refaire passer la zone de saisie en noir lors du deuxième essai
      j3pElement(idPrefix + 'consigne1inputmq1').addEventListener('keypress', function () {
        j3pElement(idPrefix + 'consigne1inputmq1').style.color = ''
      }, false)
      j3pElement(idPrefix + 'consigne1inputmq2').addEventListener('keypress', function () {
        j3pElement(idPrefix + 'consigne1inputmq2').style.color = ''
      }, false)

      // On rajoute toutes les informations qui permettront de valider la saisie
      j3pElement(idPrefix + 'consigne1inputmq1').typeReponse = ['nombre', 'exact']
      j3pElement(idPrefix + 'consigne1inputmq2').typeReponse = ['nombre', 'exact']

      // Paramétrage de la validation
      // mes_zones_saisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
      const mes_zones_saisie = [idPrefix + 'consigne1inputmq1', idPrefix + 'consigne1inputmq2']
      // validePerso est souvant un tableau vide
      // Mais parfois certaines zones ne peuvent pas être validée de manière automatique
      // Donc la validation de ces zones devra se faire "à la main".
      // Par contre on vérifiera si elles sont remplies ou non.
      const mes_zones_valide_perso = [idPrefix + 'consigne1inputmq1', idPrefix + 'consigne1inputmq2']

      // fcts_valid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
      la_section.fcts_valid = new ValidationZones({ zones: mes_zones_saisie, validePerso: mes_zones_valide_perso, parcours: me })
      //  Initialisation des zones de saisie

      j3pFocus(idPrefix + 'consigne1inputmq1')

      this.finEnonce() // Obligatoire
      //  Partie exécutée à chaque essai
      break
    } // case "enonce":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'correction': {
      // Permet de nommer facilement les conteneurs
      const fcts_valid = la_section.fcts_valid // ici je crée juste une variable pour raccourcir le nom de l’object fcts_valid

      //  Efface le nombre de chances qui restent
      j3pElement(idPrefix + 'zone_chances').innerHTML = ''

      //  Efface les éléments de correction
      j3pElement(idPrefix + 'explication1').innerHTML = ''
      j3pElement(idPrefix + 'explication2').innerHTML = ''
      j3pElement(idPrefix + 'explication3').innerHTML = ''

      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      var reponse = fcts_valid.validationGlobale() // Ne pas modifier
      // A cet instant reponse contient deux éléments :
      // reponse.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
      // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses

      // On teste manuellement si la réponse est bonne
      if (reponse.aRepondu) {
        this.facteur1 = j3pValeurde(idPrefix + 'consigne1inputmq1')
        this.facteur2 = j3pValeurde(idPrefix + 'consigne1inputmq2')
        let juste = (this.facteur1 * this.facteur2 == stor.nombre)
        reponse.bonneReponse = reponse.bonneReponse && juste
        fcts_valid.zones.bonneReponse[0] = juste
        fcts_valid.zones.bonneReponse[1] = juste

        juste = (this.facteur1 != 1) && (this.facteur2 != 1)
        reponse.bonneReponse = reponse.bonneReponse && juste
        fcts_valid.zones.bonneReponse[0] = fcts_valid.zones.bonneReponse[0] && juste
        fcts_valid.zones.bonneReponse[1] = fcts_valid.zones.bonneReponse[1] && juste // Obligé d’unvalider les deux pour pouvoir corriger.             if (this.facteur2==1) fcts_valid.zones.bonneReponse[0]=true;

        fcts_valid.coloreUneZone(idPrefix + 'consigne1inputmq1')
        fcts_valid.coloreUneZone(idPrefix + 'consigne1inputmq2')
      } // Test manuel

      // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
      if ((!reponse.aRepondu) && (!this.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        this.reponseManquante(idPrefix + 'correction') //  On rend la main pour attendre une réponse
        affiche_chances() //  affiche le nombre de chances qui restent
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (reponse.bonneReponse) {
          this._stopTimer()
          this.score++
          j3pElement(idPrefix + 'correction').style.color = this.styles.cbien
          j3pElement(idPrefix + 'correction').innerHTML = cBien

          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          // affiche_correction(true);

          // Augmente le niveau de difficulté
          // On rajoute les nombres de la difficulté suivante
          if (stor.difficulte_en_cours < stor.t_difficulte.length - 1) {
            stor.difficulte_en_cours++
            for (i = 0; i <= stor.produits[stor.t_difficulte[stor.difficulte_en_cours]].length - 1; i++) {
              if (!stor.questions.includes(stor.produits[stor.t_difficulte[stor.difficulte_en_cours]][i])) { stor.questions.push(stor.produits[stor.t_difficulte[stor.difficulte_en_cours]][i]) }
            }
          } // On augmente la difficulté

          this.typederreurs[0]++ //  Compte les bonnes réponses
          this.cacheBoutonValider() //  Plus de réponse à donner
          this.etat = 'navigation'
          this.sectionCourante() //  Passe à la question suivante
        } else {
        // Mauvaise réponse
          stor.erreurs.push([this.facteur1, this.facteur2]) // Mémorise l’erreur
          if (this.donneesSection.nbchances == 1) stor.erreurs.push([this.facteur1, this.facteur2]) // Il faudra réussir 2 fois pour confirmer son erreur

          j3pElement(idPrefix + 'correction').style.color = this.styles.cfaux // Commentaires à droite en rouge

          // Fini à cause de la limite de temps
          if (this.isElapsed) {
            j3pElement(idPrefix + 'correction').innerHTML = tempsDepasse // Commentaires à droite

            j3pDesactive(idPrefix + 'consigne1inputmq1')
            j3pDesactive(idPrefix + 'consigne1inputmq2')
            if (!fcts_valid.zones.bonneReponse[0]) j3pBarre(idPrefix + 'consigne1inputmq1')
            if (!fcts_valid.zones.bonneReponse[1]) j3pBarre(idPrefix + 'consigne1inputmq2')

            this.typederreurs[10]++ //  Compte les temps dépassés

            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            affiche_correction(true)

            this.etat = 'navigation'
            this.sectionCourante() //  Passe à la question suivante
          } else {
          // Réponse fausse
            j3pElement(idPrefix + 'correction').innerHTML = cFaux // Commentaires à droite en rouge

            if (this.essaiCourant < this.donneesSection.nbchances) {
              // Il reste des essais
              j3pElement(idPrefix + 'correction').innerHTML += '<br>' + essaieEncore // Commentaires à droite

              this.typederreurs[1]++ //  Compte les essais ratés

              affiche_chances() //  affiche le nombre de chances qui restent

              // indication éventuelle ici
              affiche_correction(false)
            } else {
              // Il ne reste plus d’essais
              j3pElement(idPrefix + 'correction').innerHTML += '<br>' + regardeCorrection // Commentaires à droite
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              affiche_correction(true)
              this.typederreurs[2]++ //  Compte les réponses fausses
              this.etat = 'navigation'
              this.sectionCourante() //  Passe à la question suivante
            } // Il ne reste plus d’essais
          } // Réponse fausse
        } // Mauvaise réponse
      } // Une réponse a été saisie

      // Obligatoire
      this.finCorrection()
      break }// case "correction":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'navigation': {
      //  Si on a posé toutes les questions, on rajoute les erreurs et on continue
      if (stor.questions.length == 0) {
        stor.questions = stor.questions.concat(stor.erreurs)
        stor.erreurs.length = 0 // Et on vide le tableau des erreurs
      }
      //    Arrête les questions si toutes les réponses sont justes
      if (stor.questions.length == 0) me.donneesSection.nbitems = me.questionCourante

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((this.typederreurs[1] == 0) && (this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) {
          //  Aucune erreur: Notion comprise
          if (this.donneesSection.limite != 0) this.parcours.pe = me.donneesSection.pe_1 //  Aucune erreur en temps limité: Notion totalement maitrisée
          else this.parcours.pe = me.donneesSection.pe_2
        } else if (
          ((this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) ||
          ((me.donneesSection.nbchances <= 1) && ((this.typederreurs[2] + this.typederreurs[10]) <= 1))
        ) {
          //  Aucune erreur sans temps limité: Notion comprise
          this.parcours.pe = me.donneesSection.pe_3
        } else {
          //  A pu corriger ses erreurs: Notion fragile
          this.parcours.pe = me.donneesSection.pe_4
        }
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()
      break
    } // case "navigation":
  } //  switch etat
}
