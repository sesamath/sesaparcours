import { j3pBarre, j3pDiv, j3pElement, j3pFocus, j3pGetRandomInt, j3pRestriction, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Auteur  Xavier JEROME
        Date    16/07/2018
        Tables de multiplication et d’addition
        Remarques
           Fonctionnement identique à une section présente sur labomep
              dont je n’ai pas retrouvé les sources Section501
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['titre', 'Calcul mental à une opération.', 'string', 'Titre de l’exercice.'], // Modifie le titre pour tenir compte des paramètres
    ['indication', 'Utilise ta mémoire pour aller vite.<br>Prends des repères dans la même table pour être sûr de ta réponse.', 'string', 'Indication en bas à gauche.'], // Texte que l’élève peut faire apparaître en bas de l’écran
    ['nbrepetitionsmax', 10, 'entier', 'Nombre de répétitions maximum de la section.<BR>Il peut être inférieur si on répond juste à toutes les questions'], // Obligatoire: nombre de questions posées pendant l’exercice
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'], // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'], // S’il n’est pas vide, le temps de réponse sera limité
    ['s_operation', 'Multiplication', 'liste', 'Choix de l’opération.', ['Multiplication', 'Addition', 'Soustraction']], //  Opération à réviser
    ['s_facteur1', '2-5;9', 'string', 'Nombres entiers possibles pour la première opérande<br>Exemple: "2-5;10;100" ce qui signifie de 2 à 5, 10 et 100'],
    ['s_facteur2', '2-9', 'string', 'Nombres entiers possibles pour la deuxième opérande<br>Exemple: "2-5;10;100" ce qui signifie de 2 à 5, 10 et 100'],
    ['b_aide', true, 'boolean', '<u>True</u> pour une explication des erreurs dans le cas de la multiplication.']
  ],
  pe: [
    { pe_1: 'Notion totalement maitrisée.' },
    { pe_2: 'Notion comprise.' },
    { pe_3: 'Notion fragile.' },
    { pe_4: 'Notion non comprise.' }
  ]
}

export function upgradeParametres (parametres) {
  if (parametres.s_operation === 'fois') parametres.s_operation = 'Multiplication'
}

const textes = {
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  phraseEnonceFois: '£a$\\times$£b = @1@',
  phraseEnoncePlus: '£a £o £b = @1@',
  Aide01: 'Attention: c’est £a$\\times$£b qui est égal à £r '
}
const idPrefix = 'tables'

/**
 * section Tables
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = this.donneesSection

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Ajouter ici toutes les fonctions nécessaires à la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Transforme une chaine d’intervalles en un tableau de valeurs
  function intervallesToTableau (intervalles) {
    const valeurs = []
    const tableau = intervalles.match(/\d*-\d*|\d*/g)
    for (let i = 0; i < tableau.length; i++) {
      if (tableau[i].length != 0) {
        if (tableau[i].indexOf('-') == -1) { valeurs.push(parseFloat(tableau[i])) } else {
          for (j = parseFloat(tableau[i].substring(0, tableau[i].indexOf('-'))); j <= tableau[i].substring(tableau[i].indexOf('-') + 1); j++) { valeurs.push(j) }
        }
      }
    }
    return valeurs
  } // intervalles_to_tab

  //   Affiche la correction sous l’énoncé
  function afficheCorrection (fin) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse

    const reponseEleve = parseFloat(j3pValeurde(idPrefix + 'consigne1input1'))

    // Pas utilisé dans cette section        j3pElement(idPrefix + "explication1").innerHTML = ""
    //  Pour effacer  un éventuel précédent affichage
    j3pElement(idPrefix + 'explication2').innerHTML = '' //  Pour effacer  un éventuel précédent affichage

    if (!me.storage.reponse.bonneReponse) {
      if (fin) { //  On affiche la bonne réponse s’il n’a pas trouvé au dernier essai
        j3pAffiche(idPrefix + 'zone_consigne1', idPrefix + 'corr11', me.storage.laBonneReponse.toString(),
          { style: me.styles.moyen.correction })
        j3pElement(idPrefix + 'corr11').style.paddingLeft = '30px'
      }
      // fin

      j3pDiv(idPrefix + 'explication2', idPrefix + 'corr21', '')
      if (ds.b_aide && (reponseEleve != 0) && (ds.s_operation != 'Addition') && (ds.s_operation != 'Soustraction') && me.storage.reponse.aRepondu) {
        const hypothese1 = Math.round(reponseEleve / me.storage.facteur2)
        if (hypothese1 * me.storage.facteur2 == reponseEleve) {
          j3pAffiche(idPrefix + 'corr21', idPrefix + 'corr210', textes.Aide01,
            {
              style: me.styles.moyen.correction,
              a: hypothese1,
              b: me.storage.facteur2,
              r: reponseEleve
            })
        }

        j3pDiv(idPrefix + 'explication2', idPrefix + 'corr22', '')
        const hypothese2 = Math.round(reponseEleve / me.storage.facteur1)
        if ((hypothese2 * me.storage.facteur1 == reponseEleve) && (me.storage.facteur1 != me.storage.facteur2) && me.storage.reponse.aRepondu) {
          j3pAffiche(idPrefix + 'corr22', idPrefix + 'corr220', textes.Aide01,
            {
              style: me.styles.moyen.correction,
              a: me.storage.facteur1,
              b: hypothese2,
              r: reponseEleve
            })
        }
      }
      // Aide
    }
    // Bonne réponse
  } //  affiche_correction

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  function afficheChances () {
    j3pElement(idPrefix + 'zone_chances').innerHTML = ''
    if (ds.nbchances > 1) {
      j3pAffiche(idPrefix + 'zone_chances', idPrefix + 'chances', textes.phraseNbChances,
        {
          style: me.styles.moyen.enonce,
          e: ds.nbchances - me.essaiCourant,
          p: (ds.nbchances - me.essaiCourant > 1 ? 's' : '')
        })
    }
    // Pour le pluriel
  }

  //  Les variables des travail de la section
  let i, j, n

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Déroulement de la section Enonce, Correction, navigation
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  switch (this.etat) {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'enonce': {
      //  Construit l’énoncé: exécuté au début de chaque question
      if (this.debutDeLaSection) {
        //  Code exécuté uniquement au lancement de la section
        // Construction de la page
        this.construitStructurePage('presentation1')
        ds.nbrepetitions = ds.nbrepetitionsmax // C’est cette valeur qui est affichée dans l’entête
        this.afficheTitre(ds.titre)
        // Affiche l’indication en bas à gauche
        if (ds.indication !== '') this.indication(this.zones.IG, ds.indication)

        //  Paramètres à renseigner
        /*
                     Par convention,`
                    `   this.typederreurs[0] = nombre de bonnes réponses
                        this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                        this.typederreurs[2] = nombre de mauvaises réponses
                        this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                     */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        // Permet l’utilisation du compte à rebours si un temps est défini dans les paramètre

        //  Paramètres et variables particuliers à la section
        // Transforme l’intervalle pour qu’il corresponde au codage du nombre
        me.storage.t_facteur1 = intervallesToTableau(ds.s_facteur1)
        me.storage.t_facteur2 = intervallesToTableau(ds.s_facteur2)

        //  Teste la validité des paramètres
        if (me.storage.t_facteur1.length === 0) j3pShowError('Le paramètre facteur 1 n’est pas correct')
        if (me.storage.t_facteur2.length === 0) j3pShowError('Le paramètre facteur 2 n’est pas correct')

        //  Tableaux permettant de ne pas poser 2 fois la même question
        me.storage.questions = [] // Contient les question qui n’ont pas encore été posées
        for (i = 0; i <= me.storage.t_facteur1.length - 1; i++) {
          for (j = 0; j <= me.storage.t_facteur2.length - 1; j++) {
            if (!(ds.s_operation == 'Soustraction' && me.storage.t_facteur1[i] <= me.storage.t_facteur2[j])) { me.storage.questions.push([me.storage.t_facteur1[i], me.storage.t_facteur2[j]]) }
          }
        }

        me.storage.erreurs = [] // Contient questions qui ont provoqué une erreur pour les représenter en priorité
        me.storage.repeter = false // Bascule permettant de prendre alternativement des questions nouvelles et des anciennes erreurs
        // Paramètres et variables particuliers à la section
        //  Début de section
      } else {
        //  Code exécuté à chaque nouvelle question: Partie à ne pas modifier
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
      } // Code exécuté à chaque nouvelle question: Partie à ne pas modifier

      //  Partie exécutée à chaque essai

      //  Partie à ne pas modifier

      //  Partie à ne pas modifier

      /// ////////////////////////////////////////////////////////////////////////////////////////
      //
      //               Création de l’énoncé
      //
      /// ////////////////////////////////////////////////////////////////////////////////////////

      //  Création de l’énoncé
      // codeQuestion contient les informations permettant de construire la question
      if (me.storage.repeter && me.storage.erreurs.length !== 0) { // On choisit la question une fois sur deux dans les erreurs passées
        n = j3pGetRandomInt(0, me.storage.erreurs.length - 1) // La question est choisie parmi les erreurs passées
        me.storage.codeQuestion = me.storage.erreurs[n]
        me.storage.erreurs.splice(n, 1) // On la retire des questions à poser
        me.storage.repeter = false // La prochaine fois on prendra une nouvelle question
      } else {
        n = j3pGetRandomInt(0, me.storage.questions.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
        me.storage.codeQuestion = me.storage.questions[n]
        me.storage.questions.splice(n, 1) // On la retire des questions à poser
        me.storage.repeter = true
      }
      me.storage.facteur1 = me.storage.codeQuestion[0] // Une question est une paire (facteur 1, facteur 2)
      me.storage.facteur2 = me.storage.codeQuestion[1]

      switch (ds.s_operation) {
        case 'Addition': {
          me.storage.laBonneReponse = me.storage.facteur1 + me.storage.facteur2
          me.storage.operation = '+'
          break
        }
        case 'Soustraction': {
          me.storage.laBonneReponse = me.storage.facteur1 - me.storage.facteur2
          me.storage.operation = '–'
          break
        }
        default: {
          me.storage.laBonneReponse = me.storage.facteur1 * me.storage.facteur2
          break
        }
      }
      // On met les réponses possibles
      // Création de l’énoncé

      //  Création des conteneurs
      // Création du conteneur dans lequel se trouveront toutes les consignes
      j3pDiv(this.zones.MG, {
        id: idPrefix + 'consignes',
        contenu: '',
        style: me.styles.etendre('moyen.enonce', { padding: '10px', marginTop: '25px' })
      })

      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      j3pDiv(idPrefix + 'consignes', idPrefix + 'zone_consigne1', '')

      // Affiche le nombre de chances qu’il reste
      j3pDiv(idPrefix + 'consignes', {
        id: idPrefix + 'zone_chances',
        contenu: '',
        style: me.styles.etendre('moyen.enonce', { marginTop: '30px' })
      })
      afficheChances()

      // Si on veut rajouter des explications, en cas de réponse fausse par exemple
      j3pDiv(this.zones.MG, {
        id: idPrefix + 'explications',
        contenu: '',
        style: me.styles.etendre('moyen.explications', { padding: '10px', marginTop: '15px' })
      })
      // Pas utilisé dans cette section j3pDiv(idPrefix + "explications", idPrefix + "explication1", "")
      // Contient la correction
      j3pDiv(idPrefix + 'explications', idPrefix + 'explication2', '')

      // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
      j3pDiv(this.zones.MD, {
        id: idPrefix + 'correction',
        contenu: '',
        coord: [0, 0],
        style: me.styles.etendre('petit.correction', { padding: '10px' })
      })
      //  Création des conteneurs

      //  Initialisation des zones de saisie
      j3pAffiche(idPrefix + 'zone_consigne1', idPrefix + 'consigne1', (ds.s_operation == 'Addition' || ds.s_operation == 'Soustraction' ? textes.phraseEnoncePlus : textes.phraseEnonceFois),
        {
          style: me.styles.moyen.enonce,
          a: me.storage.facteur1,
          b: me.storage.facteur2,
          o: me.storage.operation,
          input1: { dynamique: true, width: '12px', correction: me.storage.laBonneReponse }
        })

      // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
      // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche

      // On rajoute les contraintes de saisie
      j3pRestriction(idPrefix + 'consigne1input1', '0-9') // on n’autorise que les chiffres

      //  Limite la zone de saisie
      j3pElement(idPrefix + 'consigne1input1').maxLength = 10

      // On rajoute toutes les informations qui permettront de valider la saisie
      j3pElement(idPrefix + 'consigne1input1').typeReponse = ['nombre', 'exact']
      j3pElement(idPrefix + 'consigne1input1').reponse = me.storage.laBonneReponse

      // Paramétrage de la validation
      // zonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
      const zonesSaisie = [idPrefix + 'consigne1input1']
      // validePerso est souvant un tableau vide
      // Mais parfois certaines zones ne peuvent pas être validée de manière automatique
      // Donc la validation de ces zones devra se faire "à la main".
      // Par contre on vérifiera si elles sont remplies ou non.
      const mes_zones_valide_perso = []

      // validationZones est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
      me.storage.validationZones = new ValidationZones({ zones: zonesSaisie, validePerso: mes_zones_valide_perso })
      //  Initialisation des zones de saisie

      j3pFocus(idPrefix + 'consigne1input1')

      this.finEnonce() // Obligatoire
      //  Partie exécutée à chaque essai
      break
    } // case "enonce":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'correction': {
      const validationZones = me.storage.validationZones // ici je crée juste une variable pour raccourcir le nom de l’object validationZones

      //  Efface le nombre de chances qui restent
      j3pElement(idPrefix + 'zone_chances').innerHTML = ''

      //  Efface les éléments de correction
      //  Pas utilisé dans cette section            j3pElement(idPrefix + "explication1").innerHTML = ""
      j3pElement(idPrefix + 'explication2').innerHTML = ''

      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      me.storage.reponse = validationZones.validationGlobale() // Ne pas modifier
      // A cet instant reponse contient deux éléments :
      // reponse.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
      // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses

      // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
      if (!me.storage.reponse.aRepondu && !this.isElapsed) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée
        this.reponseManquante(idPrefix + 'correction')
        afficheChances() //  affiche le nombre de chances qui restent
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (me.storage.reponse.bonneReponse) {
          this._stopTimer()
          this.score++
          j3pElement(idPrefix + 'correction').style.color = this.styles.cbien
          j3pElement(idPrefix + 'correction').innerHTML = cBien

          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          // Pas utilisé dans cette section  me.storage.afficheCorrection(true)

          this.typederreurs[0]++ //  Compte les bonnes réponses
          this.cacheBoutonValider() //  Plus de réponse à donner
          this.etat = 'navigation'
          this.sectionCourante() //  Passe à la question suivante
          // Bonne réponse
        } else {
          // Mauvaise réponse
          if (!me.storage.erreurs.includes(me.storage.codeQuestion)) {
            me.storage.questions.push(me.storage.codeQuestion) // On reposera la question
            me.storage.erreurs.push(me.storage.codeQuestion)
          }
          // Mémorise l’erreur
          me.storage.repeter = false // On ne repose pas la question au coup d’après

          j3pElement(idPrefix + 'correction').style.color = this.styles.cfaux // Commentaires à droite en rouge

          if (this.isElapsed) {
            // Fini à cause de la limite de temps
            this._stopTimer()

            j3pElement(idPrefix + 'correction').innerHTML = tempsDepasse // Commentaires à droite

            j3pBarre(idPrefix + 'consigne1input1')

            this.typederreurs[10]++ //  Compte les temps dépassés
            this.cacheBoutonValider() //  Plus de réponse à donner

            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante() //  Passe à la question suivante
            // Fini à cause de la limite de temps
          } else {
            // Réponse fausse
            j3pElement(idPrefix + 'correction').innerHTML = cFaux // Commentaires à droite en rouge

            if (this.essaiCourant < ds.nbchances) {
              // Il reste des essais
              j3pElement(idPrefix + 'correction').innerHTML += '<br>' + essaieEncore // Commentaires à droite

              this.typederreurs[1]++ //  Compte les essais ratés

              afficheChances() //  affiche le nombre de chances qui restent

              // indication éventuelle ici
              afficheCorrection(false)
              // Il reste des essais
            } else {
              // Il ne reste plus d’essais
              this._stopTimer()
              this.cacheBoutonValider() //  Plus de réponse à donner

              j3pElement(idPrefix + 'correction').innerHTML += '<br>' + regardeCorrection // Commentaires à droite
              //
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              afficheCorrection(true)

              this.typederreurs[2]++ //  Compte les réponses fausses

              this.etat = 'navigation'
              this.sectionCourante() //  Passe à la question suivante
            }
            // Il ne reste plus d’essais
          }
          // Réponse fausse
        }
        // Mauvaise réponse
      }
      // Une réponse a été saisie

      // Obligatoire
      this.finCorrection()
      break // case "correction"
    }

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //           FIN Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'navigation':
      //  Si on a posé toutes les questions, on rajoute les erreurs et on continue
      if (me.storage.questions.length === 0) me.storage.questions = me.storage.questions.concat(me.storage.erreurs)

      //    Arrête les questions si toutes les réponses sont justes
      if (me.storage.questions.length === 0) ds.nbitems = me.questionCourante

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((this.typederreurs[1] === 0) && (this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) {
          //  Aucune erreur
          if (ds.limite) {
            this.parcours.pe = ds.pe_1
          } else {
            //  Aucune erreur en temps limité: Notion totalement maitrisée
            this.parcours.pe = ds.pe_2
          }
        } else if (
          //  A pu corriger ses erreurs
          ((this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) ||
          // ou une seule erreur s’il n’y a pas plusieurs chances
          ((ds.nbchances <= 1) && ((this.typederreurs[2] + this.typederreurs[10]) <= 1))
        ) {
          //  Aucune erreur sans temps limité: Notion comprise
          this.parcours.pe = ds.pe_3
        } else {
          //  Notion fragile
          this.parcours.pe = ds.pe_4
        } //  Notion non comprise si on n’a pas trouve au moins une réponse lorsqu’il y a plusieurs essais
        //     ou si on n’a pas trouve au moins deux réponses lorsqu’il y a plusieurs essais
        // console.log('pe à la fin de navigation : ', this.parcours.pe) // Pour pouvoir tester le message en développement

        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  } //  switch etat
} //  SectionPosition
